if exists( select NAME from SYSOBJECTS where NAME = 'VWSDIAGNOS' and XTYPE = 'V' )
	drop view VWSDIAGNOS
go

create view VWSDIAGNOS(
	FECHA,
	RELOJ,
	IP,
	DNS,
	HUELLAS,
	MEMORIA,
	CPU,
	LINUX,
	BITACORA)
as
	select DATEADD(dd, 0, DATEDIFF(dd, 0, WS_FECHA)) as FECHA, 
		CH_RELOJ as RELOJ,
		WS_DATOS.value('(/Diagnostico/IP)[1]', 'varchar(300)') as IP,
		WS_DATOS.value('(/Diagnostico/DNS)[1]', 'varchar(300)') as DNS,
		WS_DATOS.value('(/Diagnostico/FPFileCount)[1]', 'varchar(300)') as Huellas,
		WS_DATOS.value('(/Diagnostico/Memoria)[1]', 'varchar(300)') as Memoria,
		WS_DATOS.value('(/Diagnostico/CPU)[1]', 'varchar(300)') as CPU,
		WS_DATOS.value('(/Diagnostico/LinuxVersion)[1]', 'varchar(300)') as LinuxVersion,
		WS_DATOS.value('(/Diagnostico/Bitacora)[1]', 'varchar(900)') as Bitacora
	from #COMPARTE.dbo.WS_DIAGNOS
GO

if exists( select NAME from SYSCOLUMNS where NAME = 'EN_CODIGO' and ID = object_id(N'R_CLAS_ENT'))
BEGIN
	DELETE R_CLAS_ENT where EN_CODIGO = ( select EN_CODIGO from R_ENTIDAD where  en_tabla = 'VWSDIAGNOS' ) AND RC_CODIGO = 26
END
GO

if exists( select NAME from SYSCOLUMNS where NAME = 'AT_CAMPO' and ID = object_id(N'R_ATRIBUTO'))
BEGIN
	DELETE R_ATRIBUTO where AT_CAMPO = 'CH_RELOJ' and EN_CODIGO = ( select EN_CODIGO from R_ENTIDAD where  en_tabla = 'VWSDIAGNOS' )
END
GO

if exists( select NAME from SYSCOLUMNS where NAME = 'AT_CAMPO' and ID = object_id(N'R_ATRIBUTO'))
BEGIN
	DELETE R_ATRIBUTO where AT_CAMPO = 'WS_DATOS' and EN_CODIGO = ( select EN_CODIGO from R_ENTIDAD where  en_tabla = 'VWSDIAGNOS' )
END
GO

if exists( select NAME from SYSCOLUMNS where NAME = 'AT_CAMPO' and ID = object_id(N'R_ATRIBUTO'))
BEGIN
	DELETE R_ATRIBUTO where AT_CAMPO = 'WS_FECHA' and EN_CODIGO = ( select EN_CODIGO from R_ENTIDAD where  en_tabla = 'VWSDIAGNOS' )
END
GO

if exists( select NAME from SYSCOLUMNS where NAME = 'EN_TITULO' and ID = object_id(N'R_ENTIDAD'))
BEGIN
	UPDATE R_ENTIDAD SET EN_TITULO = 'Conciliación de timbrado', EN_TABLA = 'VTIMCONCIL', EN_DESCRIP = 'Conciliación de timbrado',
	EN_PRIMARY='PE_YEAR,PE_TIPO,PE_NUMERO,CB_CODIGO', EN_ATDESC = '', EN_ALIAS = '0', EN_ACTIVO = 'S', EN_VERSION = '476', US_CODIGO = '1', 
	EN_NIVEL0 = 'N' where EN_CODIGO = 405
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'SP_DISM_TASA_INFONAVIT') and xtype in (N'FN', N'IF', N'TF')) 
	drop function SP_DISM_TASA_INFONAVIT
GO 

create FUNCTION SP_DISM_TASA_INFONAVIT( @Empleado NumeroEmpleado, @Fecha Fecha  ) Returns NUMERIC(15, 2)
AS
BEGIN
  declare @SalarioIntegrado Numeric( 15, 2);
  declare @SalarioMinimo Numeric( 15, 2 );
  declare @FechaSalarioMinimo Fecha;
  declare @TasaIndex Numeric( 15, 2 );
  declare @ZonaGeografica Char( 1 );
  declare @Resultado SmallInt;
  declare @FechaIngreso Fecha;
  declare @AplicaDisminucion Booleano;
  declare @TipoCredito SmallInt;
  declare @FechaInicio Fecha;
  declare @Status SmallInt;

  declare @TasaNueva Numeric( 15, 2 );
  declare @FechaTasa Fecha;
  
  SET @TasaNueva = 0;
  set @FechaTasa = @Fecha

  select @FechaInicio = MIN(KI_FECHA) from KARINF where CB_CODIGO = @Empleado

  if ( @FechaInicio < @Fecha )
  begin
	   set @FechaInicio = @Fecha;
  end
  
  select @FechaIngreso = CB_FEC_ING from COLABORA where CB_CODIGO = @Empleado;


  select TOP 1 @TasaIndex = CB_INFTASA, @AplicaDisminucion = CB_INFDISM, @TipoCredito = CB_INFTIPO  
  from  KARINF
  where ( CB_CODIGO = @Empleado ) AND ( KI_FECHA <= @FechaInicio  ) order by KI_FECHA desc

  if ( @TasaIndex is not NULL ) and ( @TipoCredito = 1 ) and ( @AplicaDisminucion = 'S' )
  begin   
		    
		/*Obtener el status del empleado ala fecha de inicio del credito a la fecha de inicio del bimestre*/
        select @Status = DBO.SP_STATUS_ACT( @Fecha, @Empleado )

        if ( @Status <> 1 ) and (@FechaIngreso > @Fecha) 
		    begin
             
             /*Se trae de nuevo la fecha de ingreso debido a que puede haber mas de dos cortes*/
             select @FechaIngreso = ( select TOP 1 CB_FEC_ING from KARDEX where CB_CODIGO = @Empleado and
									                    ( CB_FECHA >= @Fecha) and ( CB_TIPO = 'ALTA') order by CB_FECHA asc)
             if ( @FechaIngreso > @Fecha )
             begin
                  set @FechaTasa = @FechaIngreso;
             end
		    end

        if ( @Status = 1 ) or ( @FechaTasa = @FechaIngreso )
        begin
              select @ZonaGeografica = CB_ZONA_GE
              from   COLABORA
              where ( CB_CODIGO = @Empleado );


              select @FechaSalarioMinimo = MAX( SM_FEC_INI )
              from SAL_MIN
              where ( SM_FEC_INI <= @FechaTasa )


              IF ( @ZonaGeografica = 'A' )
                select @SALARIOMINIMO = SM_ZONA_A
                from SAL_MIN
                where ( SM_FEC_INI = @FechaSalarioMinimo )

              ELSE IF ( @ZonaGeografica = 'B' )
                select @SalarioMinimo = SM_ZONA_B
                from SAL_MIN
                where ( SM_FEC_INI = @FechaSalarioMinimo )

              ELSE IF ( @ZonaGeografica = 'C' )
                select @SalarioMinimo = SM_ZONA_C
                from SAL_MIN
                where ( SM_FEC_INI = @FechaSalarioMinimo )
              ELSE
			  
              SET @SalarioMinimo = 0;

              if ( @SalarioMinimo > 0 )
              begin
                    select @SalarioIntegrado = CB_SAL_INT
                    FROM   DBO.SP_FECHA_KARDEX( @FechaTasa, @Empleado );

                    SET @SalarioIntegrado = @SalarioIntegrado / @SalarioMinimo;

                    SELECT @TasaNueva = Tasa
                    FROM   DBO.SP_A80_ESCALON( @TasaIndex, @SalarioIntegrado, @FechaTasa );
              end
        end
end

   if ( @TasaNueva = 0 ) or ( @TasaNueva is Null )
    set @TasaNueva = COALESCE( @TasaIndex, 0)

   Return @TasaNueva

END
go


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID('AuditoriaConceptosTimbradoTRESS') AND type IN ( N'P', N'PC' ))     
drop procedure AuditoriaConceptosTimbradoTRESS

go 
create procedure AuditoriaConceptosTimbradoTRESS
as
begin 
    Select 
	cast
	( 
	( 
		SELECT
		CO_NUMERO ConceptoID,
		CO_DESCRIP ConceptoDescripcion,
		case CO_CALCULA
			when 'S' then 1 
			else 0 
		end  ConceptoCalcular,
		case CO_ACTIVO 
			when 'S' then 1 
			else 0 
		end ConceptoActivo,
		CO_TIPO ConceptoTipo,
		CO_FORMULA ConceptoFormula,
		CO_FRM_ALT ConceptoFormulaAlterna,
		CO_IMP_CAL ConceptoISPTIndividual,
		case CO_G_ISPT
			when 'S' then 1 
			else 0 
		end ConceptoISPTGravar,
		CO_X_ISPT ConceptoISPTExentoFormula,
		CO_SAT_CLN ConceptoClaseMontoNegativo,
		CO_SAT_CLP ConceptoClaseMontoPositivo,
		CO_SAT_TPN ConceptoSATMontoNegativoClave,
		CO_SAT_TPP ConceptoSATMontoPositivoClave,
		CO_SAT_EXE ConceptoPercepcionExentaTratamiento,
		CO_SAT_CON ConceptoExentoConceptoId,
		case CO_TIMBRA
			when 'S' then 1 
			else 0 
		end  ConceptoTimbra
		FROM CONCEPTO
		ORDER BY 1
	FOR XML RAW ('ConceptosTRESS'), ROOT ('ConceptosSistemaTRESS'), ELEMENTS
	) as varchar(max) )  ConceptosXML  
end
go

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID('AFECTA_EMPLEADO') AND type IN ( N'P', N'PC' ))     
	drop procedure AFECTA_EMPLEADO  
go 

CREATE PROCEDURE AFECTA_EMPLEADO
    			@EMPLEADO INTEGER,
    			@ANIO SMALLINT,
    			@TIPO SMALLINT,
    			@NUMERO SMALLINT,
    			@MES SMALLINT,
    			@FACTOR INTEGER
AS
BEGIN
     SET NOCOUNT ON

	 	 DECLARE @RS_CODIGO Codigo 

	 select @RS_CODIGO = RSOCIAL.RS_CODIGO from NOMINA 
	 join RPATRON on NOMINA.CB_PATRON = RPATRON.TB_CODIGO 
	 join RSOCIAL on RSOCIAL.RS_CODIGO  = RPATRON.RS_CODIGO 
	 where NOMINA.PE_YEAR = @ANIO and NOMINA.PE_TIPO = @TIPO and NOMINA.PE_NUMERO = @NUMERO and NOMINA.CB_CODIGO = @EMPLEADO 

	 set @RS_CODIGO = coalesce( @RS_CODIGO, '' ) 

     DECLARE @CONCEPTO INTEGER;
     DECLARE @M1000 NUMERIC(15,2);
     DECLARE @M1001 NUMERIC(15,2);
     DECLARE @M1002 NUMERIC(15,2);
     DECLARE @M1003 NUMERIC(15,2);
     DECLARE @M1004 NUMERIC(15,2);
     DECLARE @M1005 NUMERIC(15,2);
     DECLARE @M1006 NUMERIC(15,2);
     DECLARE @M1007 NUMERIC(15,2);
     DECLARE @M1008 NUMERIC(15,2);
     DECLARE @M1009 NUMERIC(15,2);
     DECLARE @M1010 NUMERIC(15,2);
     DECLARE @M1011 NUMERIC(15,2);
     DECLARE @M1012 NUMERIC(15,2);
     DECLARE @M1013 NUMERIC(15,2);
     DECLARE @M1014 NUMERIC(15,2);
     DECLARE @M1015 NUMERIC(15,2);
     DECLARE @M1016 NUMERIC(15,2);
     DECLARE @M1017 NUMERIC(15,2);
     DECLARE @M1100 NUMERIC(15,2);
     DECLARE @M1101 NUMERIC(15,2);
     DECLARE @M1102 NUMERIC(15,2);
     DECLARE @M1103 NUMERIC(15,2);
     DECLARE @M1104 NUMERIC(15,2);
     DECLARE @M1105 NUMERIC(15,2);
     DECLARE @M1106 NUMERIC(15,2);
     DECLARE @M1107 NUMERIC(15,2);
     DECLARE @M1108 NUMERIC(15,2);
     DECLARE @M1109 NUMERIC(15,2);
     DECLARE @M1110 NUMERIC(15,2);
     DECLARE @M1111 NUMERIC(15,2);
     DECLARE @M1112 NUMERIC(15,2);
     DECLARE @M1113 NUMERIC(15,2);
     DECLARE @M1114 NUMERIC(15,2);
     DECLARE @M1115 NUMERIC(15,2);
     DECLARE @M1116 NUMERIC(15,2);
     DECLARE @M1117 NUMERIC(15,2);
     DECLARE @M1118 NUMERIC(15,2);
     DECLARE @M1119 NUMERIC(15,2);
     DECLARE @M1120 NUMERIC(15,2);
     DECLARE @M1121 NUMERIC(15,2);
     DECLARE @M1122 NUMERIC(15,2);
     DECLARE @M1123 NUMERIC(15,2);
     DECLARE @M1124 NUMERIC(15,2);
     DECLARE @M1125 NUMERIC(15,2);
     DECLARE @M1126 NUMERIC(15,2);
     DECLARE @M1127 NUMERIC(15,2);
     DECLARE @M1128 NUMERIC(15,2);
     DECLARE @M1129 NUMERIC(15,2);
     DECLARE @M1200 NUMERIC(15,2);
     DECLARE @M1201 NUMERIC(15,2);
     DECLARE @M1202 NUMERIC(15,2);
     DECLARE @M1203 NUMERIC(15,2);
     DECLARE @M1204 NUMERIC(15,2);
     DECLARE @M1205 NUMERIC(15,2);
     DECLARE @M1206 NUMERIC(15,2);
     DECLARE @M1207 NUMERIC(15,2);
     DECLARE @M1208 NUMERIC(15,2);
     DECLARE @M1209 NUMERIC(15,2);
     DECLARE @M1210 NUMERIC(15,2);
     DECLARE @M1211 NUMERIC(15,2);
     DECLARE @M1212 NUMERIC(15,2);
     DECLARE @M1213 NUMERIC(15,2);
     DECLARE @M1214 NUMERIC(15,2);
     DECLARE @M1215 NUMERIC(15,2);
     DECLARE @M1216 NUMERIC(15,2);
     DECLARE @TP_Clasif SMALLINT;

     select @M1000=N.NO_PERCEPC,
            @M1001=N.NO_DEDUCCI,
            @M1002=N.NO_NETO,
            @M1003=N.NO_X_ISPT,
            @M1005=N.NO_PER_MEN,
            @M1006=N.NO_X_MENS,
            @M1007=N.NO_PER_CAL,
            @M1008=N.NO_IMP_CAL,
            @M1009=N.NO_X_CAL,
            @M1010=N.NO_TOT_PRE,
            @M1011=N.NO_PREV_GR,
            @M1012=N.NO_PER_ISN,
            @M1013=N.NO_TOTAL_1,
            @M1014=N.NO_TOTAL_2,
            @M1015=N.NO_TOTAL_3,
            @M1016=N.NO_TOTAL_4,
            @M1017=N.NO_TOTAL_5,
            @M1100=N.NO_DIAS_FI,
            @M1101=N.NO_DIAS_FJ,
            @M1102=N.NO_DIAS_CG,
            @M1103=N.NO_DIAS_SG,
            @M1104=N.NO_DIAS_SU,
            @M1105=N.NO_DIAS_OT,
            @M1106=N.NO_DIAS_IN,
            @M1107=N.NO_DIAS * 7 / 6,
            @M1108=N.NO_DIAS_RE,
            @M1109=N.NO_D_TURNO,
            @M1110=N.NO_DIAS,
            @M1111=N.NO_DIAS_AS,
            @M1112=N.NO_DIAS_NT,
            @M1113=N.NO_DIAS_FV,
            @M1114=N.NO_DIAS_VA,
            @M1115=N.NO_DIAS_AG,
            @M1116=N.NO_DIAS_AJ,
            @M1117=N.NO_DIAS_SS,
            @M1118=N.NO_DIAS_EM,
            @M1119=N.NO_DIAS_SI,
            @M1120=N.NO_DIAS_BA,
            @M1121=N.NO_DIAS_PV,
            @M1122=N.NO_DIAS_VJ,
            @M1123=N.NO_DIAS_VJ * 7 / 6,
            @M1124=N.NO_DIAS_IH,
            @M1125=N.NO_DIAS_ID,
            @M1126=N.NO_DIAS_IT,
            @M1127=N.NO_DIAS_DT,
            @M1128=N.NO_DIAS_FS,
            @M1129=N.NO_DIAS_FT,
            @M1200=N.NO_JORNADA,
            @M1201=N.NO_HORAS,
            @M1202=N.NO_EXTRAS,
            @M1203=N.NO_DOBLES,
            @M1204=N.NO_TRIPLES,
            @M1205=N.NO_ADICION,
            @M1206=N.NO_TARDES,
            @M1207=N.NO_HORA_PD,
            @M1208=N.NO_HORA_CG,
            @M1209=N.NO_HORA_SG,
            @M1210=N.NO_FES_TRA,
            @M1211=N.NO_DES_TRA,
            @M1212=N.NO_VAC_TRA,
            @M1213=N.NO_FES_PAG,
            @M1214=N.NO_HORASNT,
            @M1215=N.NO_HORAPDT,
            @M1216=N.NO_PRE_EXT
            from NOMINA N where
            ( N.PE_YEAR = @Anio ) and
            ( N.PE_TIPO = @Tipo ) and
            ( N.PE_NUMERO = @Numero ) and
            ( N.CB_CODIGO = @Empleado )

     if ( @M1200 <> 0 )
		Select @M1200 = @M1201 / @M1200;
     Select @M1109 = @M1200 * @M1109;

     if ( @M1201 < 0 ) and ( @M1200 > 0 )
		set @M1200 = -@M1200;

     if ( @M1000 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1000, @Mes, @Factor, @M1000, @RS_CODIGO;
     if ( @M1001 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1001, @Mes, @Factor, @M1001, @RS_CODIGO;
     if ( @M1002 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1002, @Mes, @Factor, @M1002, @RS_CODIGO;
     if ( @M1003 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1003, @Mes, @Factor, @M1003, @RS_CODIGO;
     if ( @M1005 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1005, @Mes, @Factor, @M1005, @RS_CODIGO;
     if ( @M1006 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1006, @Mes, @Factor, @M1006, @RS_CODIGO;
     if ( @M1007 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1007, @Mes, @Factor, @M1007, @RS_CODIGO;
     if ( @M1008 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1008, @Mes, @Factor, @M1008, @RS_CODIGO;
     if ( @M1009 <> 0 )
       EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1009, @Mes, @Factor, @M1009, @RS_CODIGO;
     if ( @M1010 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1010, @Mes, @Factor, @M1010, @RS_CODIGO;
     if ( @M1011 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1011, @Mes, @Factor, @M1011, @RS_CODIGO;
  	 if ( @M1012 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1012, @Mes, @Factor, @M1012, @RS_CODIGO;
  	 if ( @M1013 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1013, @Mes, @Factor, @M1013, @RS_CODIGO;
  	 if ( @M1014 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1014, @Mes, @Factor, @M1014, @RS_CODIGO;
  	 if ( @M1015 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1015, @Mes, @Factor, @M1015, @RS_CODIGO;
  	 if ( @M1016 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1016, @Mes, @Factor, @M1016, @RS_CODIGO;
  	 if ( @M1017 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1017, @Mes, @Factor, @M1017, @RS_CODIGO;
     if ( @M1100 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1100, @Mes, @Factor, @M1100, @RS_CODIGO;
     if ( @M1101 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1101, @Mes, @Factor, @M1101, @RS_CODIGO;
     if ( @M1102 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1102, @Mes, @Factor, @M1102, @RS_CODIGO;
     if ( @M1103 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1103, @Mes, @Factor, @M1103, @RS_CODIGO;
     if ( @M1104 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1104, @Mes, @Factor, @M1104, @RS_CODIGO;
     if ( @M1105 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1105, @Mes, @Factor, @M1105, @RS_CODIGO;
     if ( @M1106 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1106, @Mes, @Factor, @M1106, @RS_CODIGO;
     if ( @M1107 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1107, @Mes, @Factor, @M1107, @RS_CODIGO;
     if ( @M1108 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1108, @Mes, @Factor, @M1108, @RS_CODIGO;
     if ( @M1109 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1109, @Mes, @Factor, @M1109, @RS_CODIGO;
     if ( @M1110 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1110, @Mes, @Factor, @M1110, @RS_CODIGO;
     if ( @M1111 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1111, @Mes, @Factor, @M1111, @RS_CODIGO;
     if ( @M1112 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1112, @Mes, @Factor, @M1112, @RS_CODIGO;
     if ( @M1113 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1113, @Mes, @Factor, @M1113, @RS_CODIGO;
     if ( @M1114 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1114, @Mes, @Factor, @M1114, @RS_CODIGO;
     if ( @M1115 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1115, @Mes, @Factor, @M1115, @RS_CODIGO;
     if ( @M1116 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1116, @Mes, @Factor, @M1116, @RS_CODIGO;
     if ( @M1117 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1117, @Mes, @Factor, @M1117, @RS_CODIGO;
     if ( @M1118 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1118, @Mes, @Factor, @M1118, @RS_CODIGO;
     if ( @M1119 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1119, @Mes, @Factor, @M1119, @RS_CODIGO;
     if ( @M1120 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1120, @Mes, @Factor, @M1120, @RS_CODIGO;
     if ( @M1121 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1121, @Mes, @Factor, @M1121, @RS_CODIGO;
     if ( @M1122 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1122, @Mes, @Factor, @M1122, @RS_CODIGO;
     if ( @M1123 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1123, @Mes, @Factor, @M1123, @RS_CODIGO;
     if ( @M1124 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1124, @Mes, @Factor, @M1124, @RS_CODIGO;
     if ( @M1125 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1125, @Mes, @Factor, @M1125, @RS_CODIGO;
     if ( @M1126 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1126, @Mes, @Factor, @M1126, @RS_CODIGO;
     if ( @M1127 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1127, @Mes, @Factor, @M1127, @RS_CODIGO;
     if ( @M1128 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1128, @Mes, @Factor, @M1128, @RS_CODIGO;
     if ( @M1129 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1129, @Mes, @Factor, @M1129, @RS_CODIGO;
     if ( @M1200 <> 0 )
     begin
          select @TP_Clasif = TP_CLAS from TPERIODO where TP_TIPO = @Tipo;

          if ( @TP_Clasif = 0 )
             Select @M1200 = 8 * @M1200;
          else
          if ( @TP_Clasif = 1 )
             Select @M1200 = 48 * @M1200;
          else
          if ( @TP_Clasif = 2 )
             Select @M1200 = 96 * @M1200;
          else
          if ( @TP_Clasif = 3 )
             Select @M1200 = 104 * @M1200;
          else
          if ( @TP_Clasif = 4 )
             Select @M1200 = 208 * @M1200;
          else
          if ( @TP_Clasif = 5 )
             Select @M1200 = 72 * @M1200;
          EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1200, @Mes, @Factor, @M1200, @RS_CODIGO;

     end
     if ( @M1201 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1201, @Mes, @Factor, @M1201, @RS_CODIGO;
     if ( @M1202 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1202, @Mes, @Factor, @M1202, @RS_CODIGO;
     if ( @M1203 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1203, @Mes, @Factor, @M1203, @RS_CODIGO;
     if ( @M1204 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1204, @Mes, @Factor, @M1204, @RS_CODIGO;
     if ( @M1205 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1205, @Mes, @Factor, @M1205, @RS_CODIGO;
     if ( @M1206 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1206, @Mes, @Factor, @M1206, @RS_CODIGO;
     if ( @M1207 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1207, @Mes, @Factor, @M1207, @RS_CODIGO;
     if ( @M1208 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1208, @Mes, @Factor, @M1208, @RS_CODIGO;
     if ( @M1209 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1209, @Mes, @Factor, @M1209, @RS_CODIGO;
     if ( @M1210 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1210, @Mes, @Factor, @M1210, @RS_CODIGO;
     if ( @M1211 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1211, @Mes, @Factor, @M1211, @RS_CODIGO;
     if ( @M1212 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1212, @Mes, @Factor, @M1212, @RS_CODIGO;
     if ( @M1213 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1213, @Mes, @Factor, @M1213, @RS_CODIGO;
     if ( @M1214 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1214, @Mes, @Factor, @M1214, @RS_CODIGO;
     if ( @M1215 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1215, @Mes, @Factor, @M1215, @RS_CODIGO;
	 if ( @M1216 <> 0 )
	    EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1216, @Mes, @Factor, @M1216, @RS_CODIGO;

     Declare TemporalMovimien Cursor for
        select M.CO_NUMERO, SUM( M.MO_PERCEPC + M.MO_DEDUCCI )
        from MOVIMIEN M where
               ( M.MO_ACTIVO = 'S' ) and
               ( M.PE_YEAR = @Anio ) and
               ( M.PE_TIPO = @Tipo ) and
               ( M.PE_NUMERO = @Numero ) and
               ( M.CB_CODIGO = @Empleado )
        group by M.CO_NUMERO
     open TemporalMovimien
     Fetch next	from TemporalMovimien
     into  @Concepto, @M1000
     WHILE @@FETCH_STATUS = 0
     begin
          EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, @Concepto, @Mes, @Factor, @M1000, @RS_CODIGO;
     	  Fetch next from TemporalMovimien
     	  into  @Concepto, @M1000
     end

     CLOSE TemporalMovimien
     DEALLOCATE TemporalMovimien


     select @M1004 = sum( M.MO_PERCEPC )
     from MOVIMIEN M left outer join CONCEPTO C on C.CO_NUMERO=M.CO_NUMERO
     where
               ( M.MO_ACTIVO = 'S' ) and
               ( M.PE_YEAR = @Anio ) and
               ( M.PE_TIPO = @Tipo ) and
               ( M.PE_NUMERO = @Numero ) and
               ( M.CB_CODIGO = @Empleado ) and
               ( C.CO_TIPO = 1 ) and
               ( C.CO_A_PTU = 'S' )

     if ( @M1004 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1004, @Mes, @Factor, @M1004,  @RS_CODIGO;


	EXECUTE AFECTA_EMPLEADO_PS_PREVISION_SOCIAL @Empleado, @Anio, @Tipo, @Numero, @Mes, @Factor, @RS_CODIGO
	EXECUTE AFECTA_EMPLEADO_PS_SALARIOS			@Empleado, @Anio, @Tipo, @Numero, @Mes, @Factor, @RS_CODIGO 

END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID('AFECTA_EMPLEADO_PS_PREVISION_SOCIAL') AND type IN ( N'P', N'PC' ))     
	drop procedure AFECTA_EMPLEADO_PS_PREVISION_SOCIAL  
go 

create PROCEDURE AFECTA_EMPLEADO_PS_PREVISION_SOCIAL( @EMPLEADO INTEGER,
    			@ANIO SMALLINT,
    			@TIPO SMALLINT,
    			@NUMERO SMALLINT,
    			@MES SMALLINT,
    			@FACTOR INTEGER, 
				@RS_CODIGO Codigo)
as
begin 
    DECLARE @M1018 NUMERIC(15,2); 
	
	select @M1018 = sum( M.MO_PERCEPC + M.MO_DEDUCCI )
    from MOVIMIEN M left outer join CONCEPTO C on C.CO_NUMERO=M.CO_NUMERO
    where
               ( M.MO_ACTIVO = 'S' ) and
               ( M.PE_YEAR = @Anio ) and
               ( M.PE_TIPO = @Tipo ) and
               ( M.PE_NUMERO = @Numero ) and
               ( M.CB_CODIGO = @Empleado ) and
               ( C.CO_TIPO in ( 1, 4)  ) and
               ( C.CO_PS_TIPO = 1 )

     if ( @M1018 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1018, @Mes, @Factor, @M1018, @RS_CODIGO ;

end 
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID('AFECTA_EMPLEADO_PS_SALARIOS') AND type IN ( N'P', N'PC' ))     
	drop procedure AFECTA_EMPLEADO_PS_SALARIOS
go

create PROCEDURE AFECTA_EMPLEADO_PS_SALARIOS( @EMPLEADO INTEGER,
    			@ANIO SMALLINT,
    			@TIPO SMALLINT,
    			@NUMERO SMALLINT,
    			@MES SMALLINT,
    			@FACTOR INTEGER, 
				@RS_CODIGO Codigo)
as
begin 
    DECLARE @M1019 NUMERIC(15,2); 
	
	select @M1019 = sum( M.MO_PERCEPC + M.MO_DEDUCCI )
    from MOVIMIEN M left outer join CONCEPTO C on C.CO_NUMERO=M.CO_NUMERO
    where
               ( M.MO_ACTIVO = 'S' ) and
               ( M.PE_YEAR = @Anio ) and
               ( M.PE_TIPO = @Tipo ) and
               ( M.PE_NUMERO = @Numero ) and
               ( M.CB_CODIGO = @Empleado ) and
               ( C.CO_TIPO in ( 1, 4)  ) and
               ( C.CO_PS_TIPO = 2 )

     if ( @M1019 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1019, @Mes, @Factor, @M1019, @RS_CODIGO;

end 
GO
 
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID('SP_FONACOT_RECALCULAR_INCIDENCIAS') AND type IN ( N'P', N'PC' ))     
	drop procedure SP_FONACOT_RECALCULAR_INCIDENCIAS
go

 create procedure SP_FONACOT_RECALCULAR_INCIDENCIAS(@FT_YEAR Anio, @FT_MONTH Mes) 
as 
begin 
	set nocount on 

	update FON_EMP set FE_INCIDE = VD.FE_INCIDE, FE_FECHA1 = VD.FE_FECHA1, FE_FECHA2 = VD.FE_FECHA2, FE_DIF_QTY = coalesce( VD.CT_DIF_QTY , 0 ) 
	from  FON_EMP, VCED_CALCULA VD
	where  VD.CB_CODIGO = FON_EMP.CB_CODIGO and  VD.PF_YEAR = FON_EMP.FT_YEAR and VD.PF_MES = FON_EMP.FT_MONTH and 
	VD.PF_YEAR = @FT_YEAR and VD.PF_MES = @FT_MONTH 


	update FON_CRE set FC_PR_STA = VD.PR_STATUS, FC_FON_MOT = VD.PR_FON_MOT 
	from FON_CRE, VCED_CALCULA VD 
	where  VD.CB_CODIGO = FON_CRE.CB_CODIGO and  VD.PF_YEAR = FON_CRE.FT_YEAR and VD.PF_MES = FON_CRE.FT_MONTH and VD.PR_REFEREN = FON_CRE.PR_REFEREN and VD.PR_TIPO = FON_CRE.PR_TIPO and 
	VD.PF_YEAR = @FT_YEAR and VD.PF_MES = @FT_MONTH 

	
end
GO

if exists (select * from dbo.sysobjects where id = object_id(N'FN_EsFalta') and xtype in (N'FN', N'IF', N'TF')) 
	drop function FN_EsFalta
GO 

create function  FN_EsFalta( @CB_CODIGO NumeroEmpleado, @AU_FECHA Fecha, @TU_CODIGO Codigo, @HO_CODIGO Codigo, @AU_STATUS status, @AU_TIPODIA status ) 
returns Status 
as 
begin 

	if ( select count(*) from CHECADAS where CB_CODIGO = @CB_CODIGO and AU_FECHA = @AU_FECHA and CH_TIPO in ( 1,2)   ) > 0 
	begin 
		return 0 
	end 

	return 1

end 
GO

IF exists( select * from sys.objects where object_id = object_id('VTIMCONCIL') )
    drop view VTIMCONCIL
GO
create view VTIMCONCIL
as
select 
	RSOCIAL.RS_CODIGO,
    NO.PE_YEAR, NO.PE_TIPO, NO.PE_NUMERO, 
	NO.CB_CODIGO, 
	EMP.PRETTYNAME, 	
	NO.NO_TIMBRO, 
	case 
		when NO.NO_TIMBRO = 0 then 'Pendiente' 
		when NO.NO_TIMBRO = 2 then 'Timbrada' 
		else 'Pendiente' 
	end  NO_TIM_DESC, 
	case 
		when NO.NO_TIMBRO = 0 then 'Pendiente' 
		when NO.NO_TIMBRO = 2 then 'Timbrada' 
		else 'Pendiente' 
	end  NO_TIM_DES, 
	case  
		when NO.NO_TIMBRO = 0 and NO.NO_CAN_UID = ''	then NO.NO_STATUS  
		when NO.NO_TIMBRO = 0 and NO.NO_CAN_UID <> ''   then NO.NO_STATUS 
		when NO.NO_TIMBRO = 2							then 8 
		else	
			NO.NO_STATUS 				
	end NC_STATUS_TRESS, 	
	coalesce(NC_STATUS,-1) NC_STATUS, 
	coalesce(NC_RESULT, -1 ) NC_RESULT, 
	coalesce(NC_RES_DESC, '' ) NC_RES_DESC, 
	coalesce(NC_RES_DESC, '' ) NC_RES_DES, 
    coalesce(NC_VER_COD, '' ) NC_VER_COD, 
	coalesce(NC_VERSION, '' ) NC_VERSION, 
	NO.NO_FACUUID,
	NO.NO_FACTURA, 
	coalesce(NC_FACTURA, 0 ) NC_FACTURA,
	coalesce(NC_UUID , '' ) NC_UUID, 
	coalesce(NC_FEC_TIM, '1899-12-30') NC_FEC_TIM,
	EMP.CB_CURP, 
	coalesce(NC_CURP, '') NC_CURP , 
	coalesce(NC_APLICADO, 'N' ) NC_APLICADO , 
	coalesce(NC_APLICADO, 'N' ) NC_APLICAD ,
	EMP.CB_RFC 
from NOMINA  NO
join RPATRON on RPATRON.TB_CODIGO = NO.CB_PATRON 
join RSOCIAL on RSOCIAL.RS_CODIGO = RPATRON.RS_CODIGO
left outer join V_EMP_TIMB_SIMPLE EMP on EMP.CB_CODIGO = NO.CB_CODIGO 
left outer join NOM_CONCIL NC on NC.PE_YEAR = NO.PE_YEAR and NC.PE_TIPO = NO.PE_TIPO and NC.PE_NUMERO = NO.PE_NUMERO  and  NC.CB_CODIGO = NO.CB_CODIGO 


GO

IF exists( select * from sys.objects where object_id = object_id('V_TOT_TIM') )
    drop view V_TOT_TIM
GO

CREATE VIEW V_TOT_TIM as
select RS_CODIGO, PE_YEAR, PE_TIPO , PE_NUMERO,  TOTAL TOT_EMPTRE, TIMBRADOS_TRESS TOT_TIMTRE,  CONCILIADOS TOT_CONCIL , APLICADOS TOT_APLICA, 
	cast( case 
	when TOTAL > 0 then ( CONCILIADOS*1.0 / TOTAL*1.0 ) * 100.00 
		else 0.00 
	end  as decimal(15,2) )  TOT_PORCON,  
	TIMBRADOS_SAT TOT_TIMSAT
from ( 

select NC.RS_CODIGO, NC.PE_YEAR, NC.PE_TIPO , NC.PE_NUMERO, PE.PE_TIMBRO, COUNT(*) TOTAL,  
SUM( case  
		when NC_STATUS_TRESS = 8 then 1 
		else 0 
	 end 
	 ) TIMBRADOS_TRESS, 

SUM( case  
		when NC_RESULT  = 0  and NC_STATUS_TRESS = 8 then 1 
		else 0 
	 end 
	 ) TIMBRADOS_SAT, 

SUM( case  
		when NC_RESULT = 0 then 1 
		else 0 
	 end 
	 ) CONCILIADOS, 
SUM( case  
		when NC_APLICAD = 'S' then 1 
		else 0 
	 end 
	 ) APLICADOS
from VTIMCONCIL NC
join PERIODO PE on  NC.PE_YEAR = PE.PE_YEAR and NC.PE_TIPO = PE.PE_TIPO and NC.PE_NUMERO = PE.PE_NUMERO 
group by NC.RS_CODIGO, NC.PE_YEAR, NC.PE_TIPO , NC.PE_NUMERO, PE.PE_TIMBRO

) Conciliacion 


go

update NOMINA set NO_VER_TIM = 1 where NO_VER_TIM is NULL

go 

alter table NOMINA alter column NO_VER_TIM FolioGrande Not NULL 

go

if (SELECT COUNT(*) FROM R_VALOR WHERE LV_CODIGO = 314 and VL_CODIGO = 4 and VL_DESCRIP = 'Otros') = 0 
BEGIN	
	INSERT INTO R_VALOR( LV_CODIGO,VL_CODIGO,VL_DESCRIP,VL_VERSION ) VALUES(314,4,'Otros',491)
END

GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID('LIMPIAR_DICCIONARIO_TIPOSSAT') AND type IN ( N'P', N'PC' ))     
	drop procedure LIMPIAR_DICCIONARIO_TIPOSSAT
go

create procedure LIMPIAR_DICCIONARIO_TIPOSSAT
as
begin 
	DELETE FROM R_FILTRO where EN_CODIGO = 367
	DELETE FROM R_DEFAULT where EN_CODIGO = 367
	DELETE FROM R_CLAS_ENT where EN_CODIGO = 367
	DELETE FROM R_MOD_ENT where EN_CODIGO = 367
	DELETE FROM R_RELACION where EN_CODIGO = 367
	DELETE FROM R_ATRIBUTO where EN_CODIGO = 367
	DELETE FROM R_ENTIDAD where EN_CODIGO = 367
end 

go 

DISABLE TRIGGER TD_R_ENTIDAD ON R_ENTIDAD
go 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID('LIMPIAR_DICCIONARIO_TIPOSSAT') AND type IN ( N'P', N'PC' ))     
	exec LIMPIAR_DICCIONARIO_TIPOSSAT
go

ENABLE TRIGGER TD_R_ENTIDAD ON R_ENTIDAD
go 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID('LIMPIAR_DICCIONARIO_TIPOSSAT') AND type IN ( N'P', N'PC' ))     
	drop procedure LIMPIAR_DICCIONARIO_TIPOSSAT
go
