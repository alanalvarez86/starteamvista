/*****************************************/
/**** Foreign Keys ***********************/
/*****************************************/

/*****************************************/
/**** Views ******************************/
/*****************************************/

/* ULTIMO_EMPLEADO: Averigua el �ltimo folio asignado a un empleado */
/* Patch 397 # Seq: 1 Agregado */
create view ULTIMO_EMPLEADO( CB_CODIGO ) as
select MAX( CB_CODIGO ) from COLABORA
GO

/* EMPLEADOS_X_VERIFICAR: Lista de empleados contra la cual se buscan empleados repetidos antes de dar de alta  */
/* Patch 397 # Seq: 2 Agregado */
create view EMPLEADOS_X_VERIFICAR(
        CB_CODIGO,
        CB_APE_PAT,
        CB_APE_MAT,
        CB_NOMBRES,
        CB_CURP,
        CB_RFC,
        CB_SEGSOC,
        CB_SEXO,
        CB_FEC_NAC,
        CB_ACTIVO,
        PRETTYNAME,
        FOTO,
        REFERENCIA )
as
     select C.CB_CODIGO, C.CB_APE_PAT, C.CB_APE_MAT, C.CB_NOMBRES, C.CB_CURP, C.CB_RFC, C.CB_SEGSOC,
            C.CB_SEXO, C.CB_FEC_NAC, C.CB_ACTIVO, C.PRETTYNAME, I.IM_BLOB as FOTO, CAST( '' as varchar(30) ) as REFERENCIA
     from COLABORA C
     left outer join IMAGEN I on ( I.CB_CODIGO = C.CB_CODIGO ) and ( I.IM_TIPO = 'FOTO' )
GO

/*****************************************/
/****  Triggers  *************************/
/*****************************************/

/**************************************************/
/* STORED PROCEDURES ******************************/
/**************************************************/

update GLOBAL set GL_FORMULA = '397' where ( GL_CODIGO = 133 )
GO
