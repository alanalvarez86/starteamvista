/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$   Base De Datos DATOS      $$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/

/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$   ESTRUCTURA DATOS      $$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/


/* SP_PATCH : CR:2460, Actualiza COLABORA con nuevo campo CB_ID_BIO */
/* Patch 430 #Seq: 1*/
if not exists( select NAME from SYSCOLUMNS where NAME = 'CB_ID_BIO' and ID = object_id(N'COLABORA'))
begin
	alter table COLABORA add CB_ID_BIO FolioGrande	
end
go

/* SP_PATCH : CR:2460, Actualiza COLABORA con nuevo campo de CB_GP_COD*/
/* Patch 430 #Seq: 2*/
if not exists( select NAME from SYSCOLUMNS where NAME = 'CB_GP_COD' and id=OBJECT_ID(N'COLABORA') )
begin
	alter table COLABORA add CB_GP_COD Codigo
end
go

/* SP_PATCH : CR:2460, Actualiza Colabora con id de Huella 0 */
/* Patch 430 #Seq: 3*/
update COLABORA set CB_ID_BIO = 0, CB_GP_COD = '' where coalesce(CB_ID_BIO, -1 ) = -1
go

/* SP_PATCH : CR:2460, Actualiza INVITA con nuevo campo IV_ID_BIO */
/* Patch 430 #Seq: 4*/
if not exists( select NAME from SYSCOLUMNS where NAME = 'IV_ID_BIO' and ID = object_id(N'INVITA'))
begin
	alter table INVITA add IV_ID_BIO FolioGrande	
end
go

/* SP_PATCH : CR:2460, Actualiza HUELLA con nuevo campo HU_SYNC */
/* Patch 430 #Seq: 5*/
if not exists( select NAME from SYSCOLUMNS where NAME = 'HU_SYNC' and ID = object_id(N'HUELLA'))
begin
	alter table HUELLA add HU_SYNC Fecha;
end
go

/* SP_PATCH : CR:2460, Actualiza la fecha de sincronizacion de huellas */
/* Patch 430 #Seq: 6*/
update HUELLA set HU_SYNC = '18991230' where coalesce(HU_SYNC, '18991230') = '18991230'
go

/* SP_PATCH : CR:2460, Se elimina Primary Key de tabla huella */
/* Patch 430 #Seq: 7*/
if exists( select NAME from sysobjects where NAME = 'PK_HUELLA' and XTYPE = 'PK')
begin
     alter table HUELLA drop CONSTRAINT PK_HUELLA 
end
go

/* SP_PATCH : CR:2460, Se agrega columna de tipo de huella en tabla HUELLA */
/* Patch 430 #Seq: 8*/
if not exists( select NAME from SYSCOLUMNS where NAME = 'HU_TIPO' and ID = object_id(N'HUELLA'))
begin
	 alter table HUELLA add HU_TIPO FolioChico 
end
go

/* SP_PATCH : CR:2460, Se actualiza columna de tipo de huella en tabla HUELLA */
/* Patch 430 #Seq: 9*/
update HUELLA set HU_TIPO = 0 where HU_TIPO is null
go
     
/* SP_PATCH : CR:2460, Se modifica columna de tipo de huella en tabla HUELLA */
/* Patch 430 #Seq: 10*/
if exists( select NAME from SYSCOLUMNS where NAME = 'HU_TIPO' and ID = object_id(N'HUELLA'))
begin
     alter table HUELLA alter column HU_TIPO FOLIOCHICO not null
end
go 

/* SP_PATCH : CR:2460, Primary key de tabla de Huella */
/* Patch 430 #Seq: 11*/
if not exists( select NAME from sysobjects where NAME = 'PK_HUELLA' and XTYPE = 'PK')
begin
     alter table HUELLA add constraint PK_HUELLA primary key ( CB_CODIGO, NO_HUELLA, HU_TIPO )
end
go

/* 2446 Error al Renumerar Empleados En Tabla DOCUMENTO */
/*Patch #430  Seq: 12*/
ALTER TABLE DOCUMENTO
       ADD CONSTRAINT FK_Documento_Colabora
       FOREIGN KEY (CB_CODIGO)
       REFERENCES COLABORA
       ON DELETE CASCADE
       ON UPDATE CASCADE;

GO 	    

/*2403  Mejoras en Admin. Vacaciones*/
/*Patch #430  Seq: 13*/

ALTER TABLE VACACION
       DROP CONSTRAINT PK_VACACION
GO

/* 2403  Mejoras en Admin. Vacaciones */
/*Patch #430  Seq: 14*/
ALTER TABLE VACACION
       ADD CONSTRAINT PK_VACACION PRIMARY KEY (CB_CODIGO, VA_FEC_INI, VA_TIPO, US_CODIGO)
GO

/* SP_PATCH : Tablas para almacenar alarmas de terminales Synel */
/*Patch #430  Seq: 15*/

if not exists( select NAME from SYSOBJECTS  where NAME='SY_ALARMA' and XTYPE='U' )
begin
	create table SY_ALARMA(
		RELOJ		RelojLinx not null,
		HORA		hora not null,
		LUNES		Booleano,
		MARTES		Booleano,
		MIERCOLES	Booleano,
		JUEVES		Booleano,
		VIERNES		Booleano,
		SABADO		Booleano,
		DOMINGO		Booleano,
		TPO_OPERA	Codigo1,
		RELAY		Numeroempleado,
		ACTIVO		FolioChico,
		INACTIVO	FolioChico,
		TOTAL		FolioChico
	)
end

Go
/* SP_PATCH : Tablas para almacenar alarmas de terminales Synel */
/*Patch #430  Seq: 16*/
if not exists( select NAME from SYSOBJECTS where NAME='PK_SY_ALARMA' and XTYPE='PK' )
begin
	alter table SY_ALARMA
		add constraint PK_SY_ALARMA primary key ( RELOJ, HORA )
end

GO

/* Prender Global de Vacaciones */
/* 2403  Mejoras en Admin. Vacaciones*/
/*Patch #430  Seq: 17*/
if not EXISTS ( select 1 from GLOBAL where GL_CODIGO = 296 )
   INSERT INTO GLOBAL (GL_CODIGO,GL_DESCRIP,GL_FORMULA,GL_TIPO,US_CODIGO,GL_CAPTURA,GL_NIVEL) 
   VALUES (296,'Ver Saldo de Vacaciones','S',0,0,GETDATE(),0)
GO 

/*2403  Mejoras en Admin. Vacaciones */
/*Patch #430  Seq: 18 */
create procedure ActualizaGlobalVencimiento
as
begin
     declare @DefaultSaldarVac varchar(30);
	 set @DefaultSaldarVac = '12';
	 if Exists( select 1 from GLOBAL where GL_CODIGO = 2062 )
	 begin
		  select @DefaultSaldarVac = GL_FORMULA from GLOBAL where GL_CODIGO = 2062;
	 end
	 
	if not EXISTS ( select 1 from GLOBAL where GL_CODIGO = 297 )
    begin
         INSERT INTO GLOBAL (GL_CODIGO,GL_DESCRIP,GL_FORMULA,GL_TIPO,US_CODIGO,GL_CAPTURA,GL_NIVEL)
         VALUES (297,'Vencimiento de Vacaciones',@DefaultSaldarVac,0,0,GETDATE(),0)
    end
end
GO 

/*2403  Mejoras en Admin. Vacaciones */
/*Patch #430  Seq: 19*/
exec  ActualizaGlobalVencimiento;
GO

/*2403  Mejoras en Admin. Vacaciones */
/*Patch #430  Seq: 20 */
Drop procedure ActualizaGlobalVencimiento;
Go

/*2479  Pension Alimenticia*/
/*Patch #430  Seq: 21*/
create table TPENSION
(
	TP_CODIGO Codigo,
	TP_NOMBRE Observaciones,
	TP_INGLES Observaciones,
	TP_NUMERO Pesos,
	TP_TEXTO Observaciones,
	TP_PERCEP Booleano,
	TP_PRESTA Booleano,
	TP_CO_PENS Formula,
	TP_CO_DESC Formula,
	TP_APL_NOR Booleano,
	TP_APL_LIQ Booleano,
	TP_APL_IND Booleano,
	US_CODIGO Usuario,
	LLAVE FolioGrande identity(1,1)
);
GO

/*2479  Pension Alimenticia*/
/*Patch #430  Seq: 22*/
alter table TPENSION add Constraint PK_TP_PENSION Primary KEY (TP_CODIGO);
GO

/*2479  Pension Alimenticia*/
/*Patch #430  Seq: 23*/
create table PENSION
(
	CB_CODIGO NumeroEmpleado,
	PS_FECHA Fecha,
	PS_ORDEN  FolioChico,	 	
	PS_EXPEDIE Descripcion,
	PS_BENEFIC Titulo,
	US_CODIGO Usuario,
	PS_CTA_1  DescLarga,
	PS_BANCO_1  Descripcion,
	PS_CTA_2  DescLarga,
	PS_BANCO_2  Descripcion,
	PS_OBSERVA	Formula,
	PS_FIJA	 Pesos,
	PS_B_EMAIL DescLarga,
	PS_B_DOM ReferenciaLarga,
	PS_B_TEL Descripcion,
	LLAVE FolioGrande identity(1,1)
);
GO

/*2479  Pension Alimenticia*/
/*Patch #430  Seq: 24*/
alter table PENSION add constraint PK_PENSION Primary KEY ( CB_CODIGO,PS_ORDEN );
GO

/*2479  Pension Alimenticia*/
/*Patch #430  Seq: 25*/
ALTER TABLE PENSION
       ADD CONSTRAINT FK_PENSIONES_COLABORA
       FOREIGN KEY (CB_CODIGO)
       REFERENCES COLABORA
       ON DELETE NO ACTION
       ON UPDATE CASCADE;
GO

/*2479  Pension Alimenticia*/
/*Patch #430  Seq: 26*/
create table PEN_PORCEN 
(
	TP_CODIGO Codigo,
	CB_CODIGO NumeroEmpleado,
	PS_ORDEN  FolioChico,
	PP_PORCENT Tasa,
	LLAVE FolioGrande identity(1,1)
);
GO

/*2479  Pension Alimenticia*/
/*Patch #430  Seq: 27*/
alter table PEN_PORCEN add Primary KEY (TP_CODIGO,CB_CODIGO,PS_ORDEN);
GO

/*2479  Pension Alimenticia*/
/*Patch #430  Seq: 28*/
ALTER TABLE PEN_PORCEN
       ADD CONSTRAINT FK_PEN_PORCEN_PENSIONES
       FOREIGN KEY (CB_CODIGO,PS_ORDEN)
       REFERENCES PENSION
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

/*2479  Pension Alimenticia*/
/*Patch #430  Seq: 29*/
alter table COLABORA add CB_TIE_PEN Booleano;
GO

/*2479  Pension Alimenticia*/
/*Patch #430  Seq: 30*/
update COLABORA set CB_TIE_PEN = 'N';
GO
   
/*2499  Administraci�n de Seguros de Gastos M�dicos*/
/*Patch #430  Seq: ???*/
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' 
    AND TABLE_NAME='POLIZA_MED') 
begin
	 IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' 
        AND TABLE_NAME='POL_VIGENC') 
		 Drop TABLE POL_VIGENC

	IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' 
	   AND TABLE_NAME='EMP_POLIZA')  
		 Drop TABLE EMP_POLIZA;

	 DROP TABLE POLIZA_MED;
end
GO
/*2499  Administraci�n de Seguros de Gastos M�dicos*/
/*Patch #430  Seq: 31 */
create table POLIZA_MED
(
	PM_CODIGO  Codigo,
	PM_NUMERO  Descripcion,
	PM_TIPO	   Status,
	PM_DESCRIP Titulo,
	PM_ASEGURA Descripcion,
	PM_BROKER  Descripcion,
	PM_TEL_BRK DescLarga,
	PM_NOM_CT1 Observaciones,
	PM_COR_CT1 Titulo,
	PM_TEL1CT1 DescLarga,
	PM_TEL2CT1 DescLarga,	
	PM_NOM_CT2 Observaciones,
	PM_COR_CT2 Titulo,
	PM_TEL1CT2 DescLarga,
	PM_TEL2CT2 DescLarga,	
	PM_OBSERVA Memo,
	LLAVE	   FolioGrande identity(1,1)
);
GO

/*2499  Administraci�n de Seguros de Gastos M�dicos*/
/*Patch #430  Seq: 32*/
alter table POLIZA_MED add Constraint PK_POLIZA_MED Primary KEY (PM_CODIGO);

/*2499  Administraci�n de Seguros de Gastos M�dicos*/
/*Patch #430  Seq: ?? */
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='POL_VIGENC') 
	Drop TABLE POL_VIGENC
GO
/*2499  Administraci�n de Seguros de Gastos M�dicos*/
/*Patch #430  Seq: 33*/
Create Table POL_VIGENC
(
	PM_CODIGO  Codigo,
	PV_REFEREN Referencia,
	PV_FEC_INI Fecha,
	PV_FEC_FIN Fecha,
	PV_CONDIC  Memo,
	PV_TEXTO   Descripcion,
	PV_NUMERO  Numerico,
	LLAVE	   FolioGrande identity(1,1)
);
GO

/*2499  Administraci�n de Seguros de Gastos M�dicos*/
/*Patch #430  Seq: 34*/
alter table POL_VIGENC add Constraint PK_POL_VIGENC Primary KEY (PM_CODIGO,PV_REFEREN);
GO

/*2499  Administraci�n de Seguros de Gastos M�dicos*/
/*Patch #430  Seq:35*/
ALTER TABLE POL_VIGENC
       ADD CONSTRAINT FK_POL_VIGENC_POLIZA_MED
       FOREIGN KEY (PM_CODIGO)
       REFERENCES POLIZA_MED
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

/*2499  Administraci�n de Seguros de Gastos M�dicos*/
/*Patch #430  Seq: ???*/
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='EMP_POLIZA') 
	Drop TABLE EMP_POLIZA
GO
/*2499  Administraci�n de Seguros de Gastos M�dicos*/
/*Patch #430  Seq: 36*/
Create Table EMP_POLIZA
(
	CB_CODIGO  NumeroEmpleado,
	PM_CODIGO  Codigo,
	PV_REFEREN Referencia,
	EP_TIPO    Status,
	PA_FOLIO   FolioChico,
	PA_RELACIO Status,
	EP_CERTIFI Descripcion,
	EP_FEC_INI Fecha,
	EP_FEC_FIN Fecha,
	EP_CFIJO   Pesos,
	EP_CPOLIZA Pesos,
	EP_CTITULA Pesos,
	EP_OBSERVA Formula,
	EP_CANCELA Booleano,
	EP_STATUS  as Case WHEN EP_CANCELA = 'S' THEN 2 WHEN EP_FEC_INI > GetDate() THEN 0 When ( EP_CANCELA = 'N' and ( GetDate() between EP_FEC_INI and EP_FEC_FIN ) )then 0 else 1 end,
	EP_CAN_FEC Fecha,
	EP_CAN_MON Pesos,
	EP_CAN_MOT DescLarga,	
	EP_GLOBAL  Booleano,
	US_CODIGO  Usuario,
	EP_FEC_REG Fecha,
	LLAVE	   FolioGrande identity(1,1)
);

/*2499  Administraci�n de Seguros de Gastos M�dicos*/
/*Patch #430  Seq: 37*/
alter table EMP_POLIZA add Constraint PK_EMP_POLIZA Primary KEY (CB_CODIGO,PM_CODIGO,PV_REFEREN,EP_TIPO,PA_FOLIO,PA_RELACIO,EP_FEC_INI);
GO

/*2499  Administraci�n de Seguros de Gastos M�dicos*/
/*Patch #430  Seq: 38*/
ALTER TABLE EMP_POLIZA
       ADD CONSTRAINT FK_EMP_POLIZA_POLIZA_MED
       FOREIGN KEY (PM_CODIGO)
       REFERENCES POLIZA_MED
       ON DELETE NO ACTION
       ON UPDATE CASCADE;
GO

/*2499  Administraci�n de Seguros de Gastos M�dicos*/
/*Patch #430  Seq: 39 */
ALTER TABLE EMP_POLIZA
       ADD CONSTRAINT FK_EMP_POLIZA_COLABORA
       FOREIGN KEY (CB_CODIGO)
       REFERENCES COLABORA
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

/*2499  Administraci�n de Seguros de Gastos M�dicos*/
/*Patch #430  Seq: 40*/
ALTER TABLE PARIENTE ADD PA_APE_PAT Descripcion;
GO 
/*2499  Administraci�n de Seguros de Gastos M�dicos*/
/*Patch #430  Seq: 41*/
ALTER TABLE PARIENTE ADD PA_APE_MAT Descripcion;
GO 

/*2499  Administraci�n de Seguros de Gastos M�dicos*/
/*Patch #430  Seq: 42*/
ALTER TABLE PARIENTE ADD PA_NOMBRES Descripcion;
GO

/*2499  Administraci�n de Seguros de Gastos M�dicos*/
/*Patch #430  Seq: 43*/

IF EXISTS (SELECT * FROM SYSOBJECTS WHERE name = 'TU_PARIENTE')
   DISABLE TRIGGER TU_PARIENTE ON PARIENTE;
GO
update PARIENTE set PA_APE_PAT = Coalesce((select CB_APE_PAT from Colabora where CB_CODIGO = PA_TRABAJA ),'') where PA_TRABAJA > 0;
GO
/*2499  Administraci�n de Seguros de Gastos M�dicos*/
/*Patch #430  Seq: 44*/
update PARIENTE set PA_APE_MAT = Coalesce((select CB_APE_MAT from Colabora where CB_CODIGO = PA_TRABAJA ),'') where PA_TRABAJA > 0;
GO
/*2499  Administraci�n de Seguros de Gastos M�dicos*/
/*Patch #430  Seq: 45*/
update PARIENTE set PA_NOMBRES = Coalesce((select CB_NOMBRES from Colabora where CB_CODIGO = PA_TRABAJA ),'') where PA_TRABAJA > 0;
GO
IF EXISTS (SELECT * FROM SYSOBJECTS WHERE name = 'TU_PARIENTE')
   ENABLE TRIGGER TU_PARIENTE ON PARIENTE;
GO
/*2424 Nuevo mecanismo para realizar ajuste de ISR utilizando tablas provisionales anualizadas*/
/*Patch #430  Seq: 46*/
CREATE TABLE ISR_AJUSTE (
	/*
			IS_YEAR              Anio,			 A�o en que se realiza el proceso
			IS_MES				 Mes, 			 Mes en que se realiza el proceso
			CB_CODIGO            NumeroEmpleado, N�mero de Empleado
			CB_SALARIO           PesosDiario,	 Salario Diario del empleado al final del mes de c�lculo
			IS_ING_BR            Pesos, 		 Resultado de la formula para el Ingreso Bruto
			IS_GRAVADO           Pesos, 		 Resultado de la Formula para el Ingreso Exento
			IS_RETENID           Pesos,			 Resultado de la Formula para el Impuesto Retenido
			IS_SUBE              Pesos,			 Resultado de la Formula para el SUBE Aplicado
			IS_IMPCALC           Pesos,			 Resultado de la Formula para el Impuesto Calculado
			IS_FAVOR             Pesos,			 Impuesto a favor, calculado por el proceso
			IS_CONTRA            Pesos,			 Impuesto a contra, calculado por el proceso
			US_CODIGO            Usuario		 Usuario que realiz� el proceso
	*/
			IS_YEAR              Anio,
			IS_MES				 Mes,
			CB_CODIGO            NumeroEmpleado,
			CB_SALARIO           PesosDiario,
			IS_ING_BR            Pesos,
			IS_GRAVADO           Pesos,
			IS_RETENID           Pesos,
			IS_SUBE              Pesos,
			IS_IMPCALC           Pesos,
			IS_FAVOR             Pesos,
			IS_CONTRA            Pesos,
			US_CODIGO            Usuario
)
GO

/*2424 Nuevo mecanismo para realizar ajuste de ISR utilizando tablas provisionales anualizadas*/
/*Patch #430  Seq: 47*/
ALTER TABLE ISR_AJUSTE
       ADD CONSTRAINT PK_ISR_AJUSTE PRIMARY KEY (IS_YEAR, IS_MES, CB_CODIGO)
GO

/*2384 - D�as IMSS cuando hay vacaciones*/
/*Patch #430  Seq: 48*/
/* Tabla N�mina: Dias vacaciones totales del periodo ( PRO D�as IMSS cuando hay vacaciones )*/
alter table NOMINA add NO_DIAS_VT Dias
GO

/*2384 - D�as IMSS cuando hay vacaciones*/
/*Patch #430  Seq: 49*/
/* Tabla N�mina: Inicializar Dias vacaciones en d�as no habiles ( PRO D�as IMSS cuando hay vacaciones )*/
update NOMINA set NO_DIAS_VT = 0 where ( NO_DIAS_VT is Null )
GO

/*2384 - D�as IMSS cuando hay vacaciones*/
/*Patch #430  Seq: 50*/
/* Tabla N�mina: D�as vacaciones pagados ( PRO D�as IMSS cuando hay vacaciones )*/
alter table NOMINA add NO_DIAS_VC Dias
GO

/*2384 - D�as IMSS cuando hay vacaciones*/
/*Patch #430  Seq: 51 */
/* Tabla N�mina: D�as vacaciones pagados ( PRO D�as IMSS cuando hay vacaciones )*/
update NOMINA set NO_DIAS_VC = 0 where ( NO_DIAS_VC is Null )
GO

/*2384 - D�as IMSS cuando hay vacaciones*/
/*Patch #430  Seq: 52*/
/* Agrega nuevos par�metros de d�as de vacaciones ( PRO D�as IMSS cuando hay vacaciones ) 1/3*/
CREATE PROCEDURE AGREGA_PARAM AS
BEGIN
    SET NOCOUNT ON;
    declare @Cuantos Integer;
    declare @Maximo int
    select @Cuantos = COUNT(*) from NOMPARAM where ( NP_NOMBRE = 'DIAS_VTOT' );
    if ( @Cuantos = 0 )
    begin
        select @Maximo = MAX(NP_FOLIO) from NOMPARAM
        insert into NOMPARAM (NP_FOLIO, NP_NOMBRE, NP_FORMULA, NP_TIPO, NP_DESCRIP, NP_ACTIVO )
        values (@Maximo+1, 'DIAS_VTOT', 'NO_DIAS_VT', 2, 'D�as de Vacaciones del periodo', 'S' )
    end
    select @Cuantos = COUNT(*) from NOMPARAM where ( NP_NOMBRE = 'DIAS_VCAL' );
    if ( @Cuantos = 0 )
    begin
        select @Maximo = MAX(NP_FOLIO) from NOMPARAM
        insert into NOMPARAM (NP_FOLIO, NP_NOMBRE, NP_FORMULA, NP_TIPO, NP_DESCRIP, NP_ACTIVO )
        values (@Maximo+1, 'DIAS_VCAL', 'NO_DIAS_VC', 2, 'D�as calendario de Vacaciones', 'S' )
    end
END
GO

/*2384 - D�as IMSS cuando hay vacaciones*/
/*Patch #430  Seq: 53*/
/* Agrega nuevos par�metros de d�as de vacaciones ( PRO D�as IMSS cuando hay vacaciones ) 2/3*/
EXECUTE AGREGA_PARAM
GO

/*2384 - D�as IMSS cuando hay vacaciones*/
/*Patch #430  Seq: 54*/
/* Agrega nuevos par�metros de d�as de vacaciones ( PRO D�as IMSS cuando hay vacaciones ) 3/3*/
DROP PROCEDURE AGREGA_PARAM
GO

/*2317 - Error en filtro: Numero 7 Campo no existe TMPROTA.CB_CODIGO*/
/*Patch #430  Seq: 55*/
/* */
update R_ENTIDAD set EN_NIVEL0='N'
where EN_CODIGO in(	106,109)
go

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 56*/
IF NOT EXISTS (SELECT * FROM systypes WHERE name = 'CODIGO10')
begin
	exec sp_addtype Codigo10, 'char(6)', 'NOT NULL'
	exec sp_bindefault CodigoVacio, Codigo10
end
go

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 57*/
/* Para eficiencia en JOIN de NOMINA hacia tablas hijas de Costeo*/
CREATE INDEX IE_NOMINA_LLAVE ON NOMINA (LLAVE )
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 58*/
IF NOT EXISTS (SELECT * FROM sysobjects WHERE type = 'U' AND name = 'T_CriteriosCosto')
begin
	CREATE TABLE T_CriteriosCosto
	(
		CritCostoID 		FolioGrande,
		CritCostoNombre 	Descripcion,
		CritCostoActivo 	Booleano
	);
	ALTER TABLE T_CriteriosCosto ADD CONSTRAINT PK_CriteriosCosto PRIMARY KEY (CritCostoID);
end
go

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 59*/
IF NOT EXISTS (SELECT * FROM sysobjects WHERE type = 'U' AND name = 'T_GruposCosto')
begin
	CREATE TABLE T_GruposCosto
	(
		GpoCostoID 		FolioGrande IDENTITY,
		GpoCostoCodigo 	Codigo10,
		GpoCostoNombre 	Observaciones,
		GpoCostoActivo 	Booleano,
	) ;
	ALTER TABLE T_GruposCosto ADD CONSTRAINT PK_GruposCosto PRIMARY KEY (GpoCostoID);
	ALTER TABLE T_GruposCosto ADD CONSTRAINT AK_GruposCosto_Codigo UNIQUE (GpoCostoCodigo);
end
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 60*/
IF NOT EXISTS (SELECT * FROM sysobjects WHERE type = 'U' AND name = 'T_ConceptosCosto')
begin
	CREATE TABLE T_ConceptosCosto
	(
		ConcepCostoID 	FolioGrande IDENTITY,
		CO_NUMERO 		Concepto,
		GpoCostoID 		FolioGrande,
		CritCostoID 	FolioGrande
	);
	ALTER TABLE T_ConceptosCosto ADD CONSTRAINT PK_ConceptosCosto PRIMARY KEY (ConcepCostoID)
	ALTER TABLE T_ConceptosCosto ADD CONSTRAINT AK_ConceptosCosto_Concepto UNIQUE (CO_NUMERO,GpoCostoID)
	CREATE INDEX IE_ConceptosCosto_Grupo ON T_ConceptosCosto (GpoCostoID ,CO_NUMERO )
end
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 61*/
/*Esta tabla tiene un join hacia nomina, es necesario cambiarlo hacia el formato PE_YEAR , PE_TIPO, PE_NUMERO*/
IF NOT EXISTS (SELECT * FROM sysobjects WHERE type = 'U' AND name = 'T_ProporcionTotales')
begin
	CREATE TABLE T_ProporcionTotales
	(
		PropTotalID 	FolioGrande IDENTITY,
		NominaID 		FolioGrande,
		CritCostoID 	FolioGrande,
		PropTotalConteo FolioGrande,
		PropTotalSuma 	Horas
	) ;
	ALTER TABLE T_ProporcionTotales ADD CONSTRAINT PK_ProporcionTotales PRIMARY KEY (PropTotalID)
	ALTER TABLE T_ProporcionTotales ADD CONSTRAINT AK_ProporcionTotales_Criterio UNIQUE (NominaID,CritCostoID)
end
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 62*/
/*Esta tabla tiene un join hacia nomina, es necesario cambiarlo hacia el formato PE_YEAR , PE_TIPO, PE_NUMERO*/
IF NOT EXISTS (SELECT * FROM sysobjects WHERE type = 'U' AND name = 'T_ProporcionCostos')
begin
	CREATE TABLE T_ProporcionCostos
	(
		PropCostoID 		FolioGrande IDENTITY,
		NominaID 			FolioGrande,
		CritCostoID 		FolioGrande,
		CC_CODIGO 			Codigo,
		PropCostoClasifi	OrdenTrabajo,
		PropCostoBase 		Horas
	) ;
	ALTER TABLE T_ProporcionCostos ADD CONSTRAINT PK_ProporcionCostos PRIMARY KEY (PropCostoID)
	CREATE INDEX IE_ProporcionCostos_Criterio ON T_ProporcionCostos (NominaID ,CritCostoID )
end
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 63 */
IF NOT EXISTS (SELECT * FROM sysobjects WHERE type = 'U' AND name = 'T_Costos')
begin
	CREATE TABLE T_Costos
	(
		CostoID 		FolioGrande IDENTITY,
		NominaID 		FolioGrande,
		CO_NUMERO 		Concepto,
		MO_REFEREN 		Referencia,
		GpoCostoID 		FolioGrande,
		CC_CODIGO 		Codigo,
		CostoClasifi 	OrdenTrabajo,
		CostoMonto 		Pesos
	) ;
	ALTER TABLE T_Costos ADD CONSTRAINT PK_Costos PRIMARY KEY (CostoID)
	CREATE INDEX IE_Costos_Concepto ON T_Costos (NominaID ,CO_NUMERO )
	CREATE INDEX IE_Costos_Grupo ON T_Costos (NominaID ,GpoCostoID )
end
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 64*/
IF NOT EXISTS (SELECT * FROM sysobjects WHERE type = 'U' AND name = 'TRANSFER')
begin
	CREATE TABLE TRANSFER(
		CB_CODIGO    	NumeroEmpleado,
		AU_FECHA        Fecha,
		TR_HORAS        Horas,
		TR_TIPO			Status,
		TR_MOTIVO		Codigo,
		CC_CODIGO		Codigo,
		TR_NUMERO       Pesos,
		TR_TEXTO        Descripcion,
		US_CODIGO       Usuario,
		TR_FECHA		Fecha,
		TR_GLOBAL		Booleano,
		TR_APRUEBA		Usuario,
		TR_FEC_APR		Fecha,
		TR_STATUS		Status,
		TR_TXT_APR		Observaciones,
		LLAVE FolioGrande IDENTITY(1,1)
	);
	ALTER TABLE TRANSFER ADD CONSTRAINT PK_TRANSFER PRIMARY KEY ( LLAVE )
	ALTER TABLE TRANSFER
       ADD CONSTRAINT FK_Transfer_Ausencia
       FOREIGN KEY (CB_CODIGO, AU_FECHA)
       REFERENCES AUSENCIA
       ON DELETE CASCADE
       ON UPDATE CASCADE
	CREATE INDEX IE_TRANSFER_TARJETA ON TRANSFER ( AU_FECHA ,CB_CODIGO )
end
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 65*/
IF NOT EXISTS (SELECT * FROM sysobjects WHERE type = 'U' AND name = 'MOT_TRANS')
begin
	CREATE TABLE MOT_TRANS(
		   TB_CODIGO            Codigo,
		   TB_ELEMENT           Descripcion,
		   TB_INGLES            Descripcion,
		   TB_NUMERO            Pesos,
		   TB_TEXTO             Descripcion,
		   TB_TIPO              Status,
		   LLAVE FolioGrande 	IDENTITY(1,1)
	);
	ALTER TABLE MOT_TRANS ADD CONSTRAINT PK_MOT_TRANS PRIMARY KEY (TB_CODIGO)
end
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 66*/
IF NOT EXISTS (SELECT * FROM sysobjects WHERE type = 'U' AND name = 'SUP_COSTEO')
begin
	CREATE TABLE SUP_COSTEO (
       US_CODIGO            Usuario,
       CB_NIVEL             Codigo,
	   LLAVE 			 	FolioGrande IDENTITY (1, 1) NOT NULL
	);
	ALTER TABLE SUP_COSTEO add CONSTRAINT PK_SUP_COSTEO PRIMARY KEY (US_CODIGO, CB_NIVEL)
end
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 67*/
IF NOT EXISTS (SELECT * FROM sysobjects WHERE type = 'U' AND name = 'T_Costos_SubCta')
begin
	CREATE TABLE T_Costos_SubCta
	(
		CostoID 		FolioGrande,
		NominaID 		FolioGrande,
		CuentaContable 	Formula,
		Tipo			Codigo1,
		Prorrat			Numerico,
		Comentario		Descripcion,
		Numero			Pesos,
		Monto			Pesos,
		LLAVE 			FolioGrande IDENTITY(1,1)
	) ;
	ALTER TABLE T_Costos_SubCta ADD CONSTRAINT PK_T_Costos_SubCta PRIMARY KEY (CostoID, NominaID, CuentaContable, Tipo)
end
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 68*/
IF NOT EXISTS ( SELECT * FROM sysobjects WHERE type = 'F' and name = 'FK_ConceptosCosto_CriteriosCosto')
	ALTER TABLE T_ConceptosCosto ADD CONSTRAINT FK_ConceptosCosto_CriteriosCosto FOREIGN KEY (CritCostoID) REFERENCES T_CriteriosCosto (CritCostoID)  ON UPDATE CASCADE ON DELETE CASCADE
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 69*/
IF NOT EXISTS ( SELECT * FROM sysobjects WHERE type = 'F' and name = 'FK_ConceptosCosto_GruposCosto')
	ALTER TABLE T_ConceptosCosto ADD CONSTRAINT FK_ConceptosCosto_GruposCosto FOREIGN KEY (GpoCostoID) REFERENCES T_GruposCosto (GpoCostoID)  ON UPDATE CASCADE ON DELETE CASCADE
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 70*/
IF NOT EXISTS ( SELECT * FROM sysobjects WHERE type = 'F' and name = 'FK_ConceptosCosto_CONCEPTO')
	ALTER TABLE T_ConceptosCosto ADD CONSTRAINT FK_ConceptosCosto_CONCEPTO FOREIGN KEY (CO_NUMERO) REFERENCES CONCEPTO (CO_NUMERO)  ON UPDATE CASCADE ON DELETE CASCADE
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 71*/
IF NOT EXISTS ( SELECT * FROM sysobjects WHERE type = 'F' and name = 'FK_ProporcionTotales_CriteriosCosto')
	ALTER TABLE T_ProporcionTotales ADD CONSTRAINT FK_ProporcionTotales_CriteriosCosto FOREIGN KEY (CritCostoID) REFERENCES T_CriteriosCosto (CritCostoID)  ON UPDATE NO ACTION ON DELETE NO ACTION
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 72*/
IF NOT EXISTS ( SELECT * FROM sysobjects WHERE type = 'F' and name = 'FK_ProporcionCostos_CriteriosCosto')
	ALTER TABLE T_ProporcionCostos ADD CONSTRAINT FK_ProporcionCostos_CriteriosCosto FOREIGN KEY (CritCostoID) REFERENCES T_CriteriosCosto (CritCostoID)  ON UPDATE CASCADE ON DELETE NO ACTION
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 73*/
IF NOT EXISTS ( SELECT * FROM sysobjects WHERE type = 'F' and name = 'FK_Costos_GruposCosto')
	ALTER TABLE T_Costos ADD CONSTRAINT FK_Costos_GruposCosto FOREIGN KEY (GpoCostoID) REFERENCES T_GruposCosto (GpoCostoID)  ON UPDATE NO ACTION ON DELETE NO ACTION
GO

/*2444 - Diccionario incorrecto en la tabla de Checada al usar las descripci�n de checadas*/
/*Patch #430  Seq: 74*/
delete from R_ATRIBUTO where EN_CODIGO = 8 and ( AT_CAMPO = 'CH_DES_CH' OR AT_CAMPO = 'CH_DESTPO' )
GO

/*2474 - CAS-137823- Error al agrupar sobre confidencialidad marca error de Collation*/
/*Patch #430  Seq: 75*/
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'V_NIVEL0')
DROP VIEW V_NIVEL0
GO

/*2474 - CAS-137823- Error al agrupar sobre confidencialidad marca error de Collation*/
/*Patch #430  Seq: 76*/
CREATE VIEW V_NIVEL0 ( TB_CODIGO, TB_ELEMENT ) AS
SELECT
  TB_CODIGO COLLATE DATABASE_DEFAULT as TB_CODIGO,
  TB_ELEMENT COLLATE DATABASE_DEFAULT as TB_ELEMENT

FROM #COMPARTE.dbo.NIVEL0
GO

/*2520 - Diccionario incorrecto en la tabla de Pariente, no existe el campo PA_EDAD*/
/*Patch #430  Seq: X*/
delete from R_ATRIBUTO where EN_CODIGO = 52 and AT_CAMPO = 'PA_EDAD'
GO

/****************************************/
/*************** VIEWS      *************/
/****************************************/

/* 2403  Mejoras en Admin. Vacaciones*/
/*Patch #430  Seq: 100*/
CREATE FUNCTION GetDerechoGozo( @Empleado NumeroEmpleado , @Anio Dias ,@FechaFinAniv Fecha,@UltimaTabla Codigo)
RETURNS @Tabla	TABLE (
	Gozo	    Numeric(15,2),
	Pago	    Numeric(15,2),
    Prima		Numeric(15,2) )
AS
BEGIN
	 declare @FechaCierre Fecha;
	 declare @GozoCierre DiasFrac;
	 declare @PagoCierre DiasFrac;
	 declare @PrimaCierre DiasFrac;
	 declare @DGozo DiasFrac;
	 declare @DPago DiasFrac;
	 declare @DPrima DiasFrac;
	 declare @DGozoPend DiasFrac;
	 declare @DPagoPend DiasFrac;
	 declare @DPrimaPend DiasFrac;
	 declare @Antiguedad Fecha;
	 declare @DiasTabla Dias;
	 declare @DiasTablaPrima Dias;

 
	 set @DGozo = 0;
	 set @DPago = 0;
	 set @DPrima = 0;

	 set @DGozoPend = 0;
	 set @DPagoPend = 0;
	 set @DPrimaPend = 0;

     select @Antiguedad = CB_FEC_ANT from COLABORA where CB_CODIGO = @Empleado;
     
	 
	 if ( Exists(select VA_FEC_INI from VACACION where CB_cODIGO = @Empleado and va_tipo = 0 and VA_YEAR = @Anio and VA_FEC_INI >= @Antiguedad  ) )
	 begin
          
		  SELECT @DGozo = Coalesce(Sum(VA_D_GOZO),0),@DPago = Coalesce(Sum(VA_D_PAGO),0),@DPrima = Coalesce(Sum(VA_D_PRIMA),0) from VACACION where CB_cODIGO = @Empleado and va_tipo = 0 and VA_YEAR = @Anio and VA_FEC_INI >= @Antiguedad;
		  select top 1 @FechaCierre = VA_FEC_INI,@GozoCierre = VA_D_GOZO, @PagoCierre = VA_D_PAGO,@PrimaCierre = VA_D_PRIMA from VACACION where CB_CODIGO = @Empleado and VA_TIPO = 0 and VA_YEAR = @Anio and VA_FEC_INI >= @Antiguedad order by VA_FEC_INI desc;  
		  if (@FechaCierre != @FechaFinAniv)
		  begin
		   	   if ((( DateDiff ( Day,@FechaCierre,@FechaFinAniv ))/365.00 ) < 1 )
			   begin
			    	set @FechaCierre = DateAdd(Day,1,@FechaCierre);
			   end
			   select Top 1 @DiasTablaPrima = (( PT_PRIMAVA / 100 )* PT_DIAS_VA),@DiasTabla = PT_DIAS_VA  from PRESTACI where TB_CODIGO = @UltimaTabla and PT_YEAR >= @Anio  order by PT_YEAR ASC     

			   set @DGozoPend = ( ( DateDiff ( Day,@FechaCierre,@FechaFinAniv ) / 365.00 ) * @DiasTabla); 	
			   set @DGozo = @GozoCierre + @DGozoPend;	 
	
			   set @DPagoPend = @DGozoPend; 	
			   set @DPago = @PagoCierre + @DPagoPend;	 
	
			   set @DPrimaPend = ( ( DateDiff ( Day,@FechaCierre,@FechaFinAniv ) / 365.00 ) * @DiasTablaPrima);
			   set @DPrima = @PrimaCierre + @DPrimaPend;
		  end
	 end
     else	
     begin	 
		  select top 1 @DPrima =  ( PT_PRIMAVA / 100 )* PT_DIAS_VA,@DGozo = PT_DIAS_VA,@DPago = PT_DIAS_VA from PRESTACI where TB_CODIGO = @UltimaTabla and PT_YEAR >= @Anio order by PT_YEAR ASC;
	     
     end;		   
	 insert into @Tabla (Gozo,Pago,Prima)
	 values (@DGozo,@DPago,@DPrima);
		
	 RETURN 
END
GO

/*2403  Mejoras en Admin. Vacaciones */
/*Patch #430  Seq: 101*/
CREATE FUNCTION Get_Proporcional( @FechaCierre Fecha,@Antig Fecha,@Anio Dias,@Empleado NumeroEmpleado,@UltimaTabla Codigo)
RETURNS @Tabla	TABLE (
	Gozo	Numeric(15,2),
	Pago	Numeric(15,2),
	Prima	Numeric(15,2)
   )
as 
begin
	 declare @DGozo DiasFrac;
	 declare @DPago DiasFrac;
	 declare @DPrima DiasFrac;
	 declare @DCierre Dias;
	 declare @FechaFinAniv Fecha;
	 declare @DiasTb Dias;
	 declare @DiasTbPrima DiasFrac;
	 declare @DiasDif Dias;
	 
     set @FechaFinAniv = DateAdd(Year,@Anio,@Antig);
     
	 if ( ( ( Month(@FechaCierre) = Month(@Antig) ) and ( Day(@FechaCierre) = Day(@Antig) ) ) or ( @FechaCierre is null ) )
     begin
		  select Top 1 @DPago = PT_DIAS_VA ,@DGozo = PT_DIAS_VA, @DPrima = ( ( PT_PRIMAVA / 100 )* PT_DIAS_VA )
					from PRESTACI where TB_CODIGO = @UltimaTabla and PT_YEAR >= @Anio order by PT_YEAR ASC	
	 end
	 else
	 begin
		  select Top 1 @DiasTbPrima = (( PT_PRIMAVA / 100 )* PT_DIAS_VA),@DiasTb = PT_DIAS_VA  
					from PRESTACI where TB_CODIGO = @UltimaTabla and PT_YEAR >= @Anio  order by PT_YEAR ASC 
    
		  set @DiasDif = DateDiff(Day,DateAdd(Day,1,@FechaCierre),@FechaFinAniv);
		  set @DGozo = ( @DiasDif / 365.00 )*@DiasTb;
		  set @DPago = @DGozo;
		  
		  set @DPrima = ( @DiasDif / 365.00 )*@DiasTbPrima;

	 end 
	 insert into @Tabla
	 ( Gozo,Pago,Prima)values( @DGozo,@DPago,@DPrima);
	return	
end
GO

/* 2403  Mejoras en Admin. Vacaciones */
/*Patch #430  Seq: 366*/
CREATE FUNCTION AniosVacCompletos( @FechaInicial Fecha,@FechaFinal Fecha )
RETURNS  integer
as
begin
declare @Anios integer;
	 if ( Month(@FechaInicial) = Month(@FechaFinal) and Day(@FechaInicial) = Day(@FechaFinal) )
	 begin
		  Set @Anios = DateDiff(Year,@FechaInicial,@FechaFinal)
	 end
	 else
	 begin
		  Set @Anios  =  (DateDiff( day, @FechaInicial,@FechaFinal ) + 1 )/365.25;
	 end
	 return @Anios;
end
GO

/* 2403  Mejoras en Admin. Vacaciones*/
/*Patch #430  Seq: 103*/
CREATE FUNCTION GET_SALDOS_VACACION( @Employee int)
RETURNS @Tabla	TABLE (
	VS_ANIV		int,
	CB_CODIGO	int,
	VS_D_GOZO	Numeric(15,2),
	VS_GOZO		Numeric(15,2),
	VS_S_GOZO	Numeric(15,2),
	VS_D_PAGO	Numeric(15,2),
	VS_PAGO		Numeric(15,2),
	VS_S_PAGO	Numeric(15,2),
	VS_D_PRIMA  Numeric(15,2),
	VS_PRIMA	Numeric(15,2),
	VS_S_PRIMA	Numeric(15,2),
	VS_FEC_VEN	DateTime,
	VS_CB_SAL	Numeric(15,2),CIERRE Numeric(15,2) )
AS
BEGIN
	declare @Empleado NumeroEmpleado;
	declare @Anios DiasFrac;
	declare @Anio Dias;

	declare @DerechoGozo DiasFrac;
	declare @GozadosDist DiasFrac;
	declare @GozadosRest DiasFrac;

    declare @GozoCierre DiasFrac;

	declare @DerechoPago DiasFrac;
	declare @PagadosDist DiasFrac;
	declare @PagadosRest DiasFrac;

	declare @PagoCierre DiasFrac;

	declare @DerechoPV DiasFrac;
	declare @PVDist DiasFrac;
	declare @PVRest DiasFrac;

	declare @PrimaCierre DiasFrac;
	declare @FechaCierre Fecha;

	declare @Antig Fecha;
	declare @AniosTotales Fecha;

	declare @Hoy Fecha;
	declare @YearCierre Dias;

	declare @DerechoGozoSum DiasFrac;
	declare @DerechoPagoSum DiasFrac;
	declare @DerechoPrimaSum DiasFrac;

    declare @FechaVencimiento Fecha;
    declare @Salario DiasFrac;
    declare @MesesGlobal Dias;

    declare @FechaFinAniv Fecha;
	declare @UltimaTabla Codigo;
	declare @FechaSalario Fecha;
    declare @FechaTabla Fecha;
    declare @Activo Booleano;
	declare @FechaBaja Fecha;


    if( @Employee > 0 )
    begin
		 declare EmpleadosVac CURSOR for
		   select CB_CODIGO,CB_FEC_ANT,CB_TABLASS,CB_SALARIO,CB_FEC_SAL,CB_ACTIVO,CB_FEC_BAJ from COLABORA where CB_CODIGO = @Employee
    end
    else
	begin
		 declare EmpleadosVac CURSOR for
		   select CB_CODIGO,CB_FEC_ANT,CB_TABLASS,CB_SALARIO,CB_FEC_SAL,CB_ACTIVO,CB_FEC_BAJ from COLABORA
    end;

	select @MesesGlobal = GL_FORMULA from GLOBAL where GL_CODIGO = 297;

	open EmpleadosVac;
     fetch NEXT from EmpleadosVac
     into @Empleado,@Antig,@UltimaTabla,@Salario,@FechaSalario,@Activo,@FechaBaja;
     while ( @@Fetch_Status = 0 )
     begin
		  -- Obtenemos los Gozados
		  select @GozadosRest = Coalesce(SUM(VA_GOZO),0),@PagadosRest = Coalesce(SUM(VA_PAGO),0),@PVRest = Coalesce(SUM(VA_P_PRIMA),0)
							from VACACION where CB_CODIGO = @Empleado and VA_TIPO = 1 and VA_FEC_INI >= @Antig;
		  --Empezamos
		  set @Anio = 1;
		  set @Anios = 0;
		  set @DerechoGozoSum = 0;
		  set @DerechoPagoSum = 0;
		  set @DerechoPrimaSum = 0;

		  Select @Hoy = gd FROM vCurrentDateTime;
		  if ( @Activo = 'N' )
		  begin
			   Set @Hoy = Coalesce(@FechaBaja,@Hoy);
		  end	
		  set @AniosTotales = dbo.AniosVacCompletos(@Antig,@Hoy );	
		  set @FechaCierre = @Antig;
		  
		  if ( ( @Antig is null )or (@Antig = '12/30/1899' ))
		  begin
		 	   select @Antig = CB_FEC_ING FROM COLABORA WHERE CB_CODIGO = @Empleado;  
	      end
			 
		  select Top 1 @FechaTabla = CB_FECHA FROM KARDEX where CB_CODIGO = @Empleado and CB_TIPO = 'PRESTA' order by CB_FECHA desc; 

		  -- Cursor para Cierres 
 	      declare CierresEmp CURSOR for
		    select VA_FEC_INI,VA_D_GOZO,VA_D_PAGO,VA_D_PRIMA,VA_YEAR from VACACION where CB_CODIGO = @Empleado and VA_TIPO = 0 and VA_FEC_INI >= @Antig order by VA_FEC_INI asc
		  
          open CierresEmp;
		  fetch NEXT from CierresEmp
		  into @FechaCierre,@GozoCierre,@PagoCierre,@PrimaCierre,@YearCierre;
		  while ( @@Fetch_Status = 0 )
		  begin 
			   	Set @Anios = dbo.AniosVacCompletos(@Antig,@FechaCierre );
			    			   -- Se recorre los anios que abarcan el cierre
		       if ( @Anios < @Anio )
		       begin
					set @DerechoGozoSum = @GozoCierre + @DerechoGozoSum;
					set @DerechoPagoSum = @PagoCierre + @DerechoPagoSum;
					set @DerechoPrimaSum = @PrimaCierre + @DerechoPrimaSum;
		       end
		       
		       While ( @Anios >= @Anio )
			   begin
					select @UltimaTabla = CB_TABLASS FROM COLABORA where CB_CODIGO = @Empleado; 
					set @FechaFinAniv = DateAdd( Year,@Anio,@Antig );
					if ( @FechaTabla != '12/30/1899' and ( not( @FechaTabla is null ) ) and ( @FechaFinAniv < @FechaTabla ) ) 
					begin
						select @UltimaTabla = dbo.SP_KARDEX_CB_TABLASS(@FechaFinAniv,@Empleado) ;
					end
					--Gozo
					if( @DerechoGozoSum > 0)
					begin
						 set @GozoCierre = @GozoCierre + @DerechoGozoSum;
						 set @DerechoGozoSum = 0;
					end
					--Pago
					if( @DerechoPagoSum > 0)
					begin
						 set @PagoCierre = @PagoCierre + @DerechoPagoSum;
						 set @DerechoPagoSum = 0;
					end
					--Prima
					if( @DerechoPrimaSum > 0)
					begin
						 set @PrimaCierre = @PrimaCierre + @DerechoPrimaSum;
						 set @DerechoPrimaSum = 0;
					end
					
					--select @DerechoGozo = Gozo,@DerechoPago = Pago,@DerechoPV = Prima
					--		from dbo.Get_Proporcional(@FechaCierre,@Antig,@Anio,@Empleado);
					select @DerechoGozo = Gozo,@DerechoPago = Pago,@DerechoPV = Prima 
								from dbo.GetDerechoGozo(@Empleado,@Anio,@FechaFinAniv,@UltimaTabla);
										
					-- Se resta del pendiente del Cierre
					--Gozo
					if ( @DerechoGozo <= @GozoCierre )
					begin
						 if ( @Anios = @Anio )
						 begin
							  set @DerechoGozo = @GozoCierre;
							  set @GozoCierre = 0;
						 end
						 else
						 begin
							  set @GozoCierre = @GozoCierre - @DerechoGozo;
						 end
					end
					else
					begin  
						 set @DerechoGozo = @GozoCierre;
						 set @GozoCierre = 0;
				    end
					--Pago
					if ( @DerechoPago <= @PagoCierre )
					begin
						 if ( @Anios = @Anio )
						 begin
							  set @DerechoPago = @PagoCierre;
							  set @PagoCierre = 0;
						 end
						 else
						 begin
							  set @PagoCierre = @PagoCierre - @DerechoPago;
						 end
					end
					else
					begin
						 set @DerechoPago = @PagoCierre;
						 set @PagoCierre = 0;
				    end
				    --Prima
					if ( @DerechoPV <= @PrimaCierre )
					begin
						 if ( @Anios = @Anio )
						 begin
							  set @DerechoPV = @PrimaCierre;
							  set @PrimaCierre = 0;
						 end
						 else
						 begin
							  set @PrimaCierre = @PrimaCierre - @DerechoPV;
						 end
					end
					else
					begin  
						 set @DerechoPV = @PrimaCierre;
						 set @PrimaCierre = 0;
				    end

					--Gozo
					if ( @GozadosRest > @DerechoGozo )
					begin
						set @GozadosDist = @DerechoGozo;
					    set @GozadosRest = @GozadosRest - @GozadosDist;
					end
					else
					begin
						 set @GozadosDist = @GozadosRest;
						 set @GozadosRest = 0;
					end
					--Pago
					if ( @PagadosRest > @DerechoPago )
					begin
						set @PagadosDist = @DerechoPago;
					    set @PagadosRest = @PagadosRest - @PagadosDist;
					end
					else
					begin
						 set @PagadosDist = @PagadosRest;
						 set @PagadosRest = 0;
					end
				    --Prima
					if ( @PVRest > @DerechoPV )
					begin
						set @PVDist = @DerechoPV;
					    set @PVRest = @PVRest - @PVDist;
					end
					else
					begin
						 set @PVDist = @PVRest;
						 set @PVRest = 0;
					end	
					-- Calcular Fecha de Vencimiento
					
					if ( ( ( @DerechoGozo - @GozadosDist ) > 0 ) and ( @MesesGlobal > 0 ) )
					begin
						 set @FechaVencimiento = DateAdd( Month,@MesesGlobal,@FechaFinAniv );
						 select @Salario = dbo.SP_KARDEX_CB_SALARIO ( @FechaFinAniv,@Empleado );
					end
					else
					begin
						 set @FechaVencimiento = null;
						 set @Salario = null;
					end

                    --insert tabla
					insert into @Tabla
					( VS_ANIV,
					  CB_CODIGO,
					  VS_D_GOZO,
					  VS_GOZO,
					  VS_S_GOZO,
					  VS_D_PAGO,
					  VS_PAGO,
					  VS_S_PAGO,
					  VS_D_PRIMA,
					  VS_PRIMA,
					  VS_S_PRIMA,
					  VS_FEC_VEN,
					  VS_CB_SAL)values
					(
					  @Anio,
					  @Empleado,
					  @DerechoGozo,
					  @GozadosDist,
					  @DerechoGozo - @GozadosDist,
					  @DerechoPago,
					  @PagadosDist,
					  @DerechoPago - @PagadosDist,
					  @DerechoPV,
					  @PVDist ,
					  @DerechoPV - @PVDist,
					  @FechaVencimiento,
					  @Salario		 
					 );
			 	
					 set @Anio = @Anio + 1;
			   end
			   fetch NEXT from CierresEmp
               into @FechaCierre,@GozoCierre,@PagoCierre,@PrimaCierre,@YearCierre;
		  end		
		  close CierresEmp;
	      deallocate CierresEmp;
		  
		  if ( @Anios != @AniosTotales )
		  begin
			   While (@AniosTotales >= @Anio)
			   begin
					set @FechaFinAniv = DateAdd( Year,@Anio,@Antig );
					select @UltimaTabla = CB_TABLASS FROM COLABORA where CB_CODIGO = @Empleado; 
					if ( @FechaTabla != '12/30/1899' and ( not( @FechaTabla is null ) ) and ( @FechaFinAniv < @FechaTabla ) )
					begin
						select @UltimaTabla = dbo.SP_KARDEX_CB_TABLASS(@FechaFinAniv,@Empleado);
					end
					
					select @DerechoGozo = Gozo,@DerechoPago = Pago,@DerechoPV = Prima
							from dbo.Get_Proporcional(@FechaCierre,@Antig,@Anio,@Empleado,@UltimaTabla);

					--Reiniciamos la fecha de Cierre para el siguiente aniversario
					set @FechaCierre = @FechaFinAniv;
					
					--Sumamos el Pendiente mas el proporcional calculado 
					--Gozo
					if( @DerechoGozoSum > 0)
					begin
						 set @DerechoGozo = @DerechoGozo + @DerechoGozoSum;
						 set @DerechoGozoSum = 0;
					end
					--Pago
					if( @DerechoPagoSum > 0)
					begin
						 set @DerechoPago = @DerechoPago + @DerechoPagoSum;
						 set @DerechoPagoSum = 0;
					end
					--Prima
					if( @DerechoPrimaSum > 0)
					begin
						 set @DerechoPV = @DerechoPV + @DerechoPrimaSum;
						 set @DerechoPrimaSum = 0;
					end
					

					--Repartir los dias con lo restante del saldo
					--Gozo
					if ( @GozadosRest > @DerechoGozo )
					begin
						set @GozadosDist = @DerechoGozo;
					    set @GozadosRest = @GozadosRest - @GozadosDist;
					end
					else
					begin
						 set @GozadosDist = @GozadosRest;
						 set @GozadosRest = 0;
					end	
					--Pago
					if ( @PagadosRest > @DerechoPago )
					begin
						set @PagadosDist = @DerechoPago;
					    set @PagadosRest = @PagadosRest - @PagadosDist;
					end
					else
					begin
						 set @PagadosDist = @PagadosRest;
						 set @PagadosRest = 0;
					end	
				    --Prima
					if ( @PVRest > @DerechoPV )
					begin
						set @PVDist = @DerechoPV;
					    set @PVRest = @PVRest - @PVDist;
					end
					else
					begin
						 set @PVDist = @PVRest;
						 set @PVRest = 0;
					end	
					
					if ( ( ( @DerechoGozo - @GozadosDist ) > 0 ) and ( @MesesGlobal > 0 ) )
					begin
						 set @FechaVencimiento = DateAdd( Month,@MesesGlobal,@FechaFinAniv );
						 select @Salario = dbo.SP_KARDEX_CB_SALARIO ( @FechaFinAniv,@Empleado );
					end
					else
					begin
						 set @FechaVencimiento = null;
						 set @Salario = null;
					end
					
					
					--Agregar el registro al resultado	
					insert into @Tabla 
					( VS_ANIV,
					  CB_CODIGO,
					  VS_D_GOZO,
					  VS_GOZO,
					  VS_S_GOZO,
					  VS_D_PAGO,
					  VS_PAGO,
					  VS_S_PAGO,
					  VS_D_PRIMA,
					  VS_PRIMA,
					  VS_S_PRIMA,
					  VS_FEC_VEN,
					  VS_CB_SAL)values
					(
					  @Anio,
					  @Empleado,
					  @DerechoGozo,
					  @GozadosDist,
					  @DerechoGozo - @GozadosDist,
					  @DerechoPago,
					  @PagadosDist,
					  @DerechoPago - @PagadosDist,
					  @DerechoPV,
					  @PVDist ,
					  @DerechoPV - @PVDist,
					  @FechaVencimiento,
					  @Salario 
					 );

					 set @Anio = @Anio + 1;
			   end				
		  end
		 	
		  fetch NEXT from EmpleadosVac
          into @Empleado,@Antig,@UltimaTabla,@Salario,@FechaSalario,@Activo,@FechaBaja;
     end;
	 close EmpleadosVac;
	 deallocate EmpleadosVac;
	 	
	return 
END
GO

/*Vista dependiente de  Funcion */
/*2403  Mejoras en Admin. Vacaciones */
/*Patch #430  Seq: 104*/
Create View V_SALD_VAC
(
	CB_CODIGO,
	VS_ANIV,
	VS_D_GOZO,
	VS_GOZO,
	VS_S_GOZO,
	VS_D_PAGO,
	VS_PAGO,
	VS_S_PAGO,
	VS_D_PRIMA,
	VS_PRIMA,
	VS_S_PRIMA,
	VS_FEC_VEN,
	VS_CB_SAL
)AS
	Select CB_CODIGO,VS_ANIV,VS_D_GOZO,VS_GOZO,VS_S_GOZO,VS_D_PAGO,VS_PAGO,VS_S_PAGO,VS_D_PRIMA,VS_PRIMA,VS_S_PRIMA,VS_FEC_VEN,VS_CB_SAL from GET_SALDOS_VACACION(-1)
go

/* 2403  Mejoras en Admin. Vacaciones*/
/*Patch #430  Seq: 105*/
drop function GetDerechoGozo;
GO
/* 2403  Mejoras en Admin. Vacaciones*/
/*Patch #430  Seq: 106*/
drop function Get_Proporcional;
GO
/* 2403  Mejoras en Admin. Vacaciones*/
/*Patch #430  Seq: 107*/
drop function YearsCompletos;
GO
/* 2403  Mejoras en Admin. Vacaciones*/
/*Patch #430  Seq: 108 */
DROP function GET_SALDOS_VACACION;
GO
/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
/*    Comienza: Cambios para  CR:2199  , Error de Linked Servers en Minusculas     */
/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */


/* V_USUARIO:  CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 109*/
IF EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'V_USUARIO' AND XTYPE = 'V' )
	DROP VIEW V_USUARIO
GO

/* Comienza: View de V_USUARIO , necesario regenerar para cubrir CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 110*/
CREATE VIEW V_USUARIO( US_CODIGO,
				  US_CORTO ,
 				  GR_CODIGO,
                      US_NIVEL ,
                      US_NOMBRE,
                      US_BLOQUEA,
                      US_CAMBIA,
                      US_FEC_IN,
                      US_FEC_OUT,
                      US_DENTRO,
                      US_ARBOL,
                      US_FORMA,
                      US_BIT_LST,
                      US_BIO_ID,
                      US_FEC_SUS,
                      US_FALLAS,
                      US_MAQUINA,
                      US_FEC_PWD,
                      US_EMAIL,
                      US_FORMATO,
                      US_LUGAR,
                      US_PAGINAS,
                      CM_CODIGO,
                      CB_CODIGO,
                      US_ANFITRI,
                      US_PORTAL,
                      US_ACTIVO,
                      US_DOMAIN,
                      US_JEFE
                               )

as

      SELECT      US_CODIGO,
                  US_CORTO ,
                  GR_CODIGO,
                  US_NIVEL ,
                  US_NOMBRE,
                  US_BLOQUEA,
                  US_CAMBIA ,
                  US_FEC_IN ,
                  US_FEC_OUT,
                  US_DENTRO ,
                  US_ARBOL  ,
                  US_FORMA  ,
                  US_BIT_LST,
                  US_BIO_ID ,
                  US_FEC_SUS,
                  US_FALLAS ,
                  US_MAQUINA,
                  US_FEC_PWD,
                  US_EMAIL  ,
                  US_FORMATO,
                  US_LUGAR  ,
                  US_PAGINAS,
                  CM_CODIGO ,
                  CB_CODIGO ,
                  US_ANFITRI,
                  US_PORTAL,
                  US_ACTIVO,
                  US_DOMAIN,
                  US_JEFE

      FROM #COMPARTE.dbo.USUARIO
GO

/* V_GRUPO: CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 111*/
IF EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'V_GRUPO' AND XTYPE = 'V' )
	DROP VIEW V_GRUPO
GO

/* Comienza: View de V_GRUPO , necesario regenerar para cubrir CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 112 */
CREATE VIEW V_GRUPO(
				GR_CODIGO,
				GR_DESCRIP,
				GR_PADRE )
as
	SELECT 	GR_CODIGO,
			GR_DESCRIP,
			GR_PADRE
	FROM #COMPARTE.dbo.GRUPO
GO

/* V_PRINTER: CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 113*/
IF EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'V_PRINTER' AND XTYPE = 'V' )
	DROP VIEW V_PRINTER

GO 

/* Comienza: View de V_PRINTER , necesario regenerar para cubrir CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq:  114*/
CREATE VIEW V_PRINTER (
				       PI_NOMBRE ,
				       PI_EJECT  ,
				       PI_CHAR_10,
				       PI_CHAR_12,
				       PI_CHAR_17,
				       PI_EXTRA_1,
				       PI_EXTRA_2,
				       PI_RESET  ,
				       PI_UNDE_ON,
				       PI_UNDE_OF,
				       PI_BOLD_ON,
				       PI_BOLD_OF,
				       PI_6_LINES,
				       PI_8_LINES,
				       PI_ITAL_ON,
				       PI_ITAL_OF,
				       PI_LANDSCA )
as
	Select PI_NOMBRE ,
		   PI_EJECT  ,
		   PI_CHAR_10,
		   PI_CHAR_12,
		   PI_CHAR_17,
		   PI_EXTRA_1,
		   PI_EXTRA_2,
		   PI_RESET  ,
		   PI_UNDE_ON,
		   PI_UNDE_OF,
		   PI_BOLD_ON,
		   PI_BOLD_OF,
		   PI_6_LINES,
		   PI_8_LINES,
		   PI_ITAL_ON,
		   PI_ITAL_OF,
		   PI_LANDSCA
	FROM #COMPARTE.dbo.PRINTER
GO

/* V_POLL: CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 115*/
IF EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'V_POLL' AND XTYPE = 'V' )
	DROP VIEW V_POLL
GO

/* Comienza: View de V_POLL , necesario regenerar para cubrir CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 116 */
CREATE VIEW V_POLL(
				PO_LINX,
				PO_EMPRESA,
				PO_NUMERO,
				PO_FECHA,
				PO_HORA,
				PO_LETRA )
AS
SELECT 	PO_LINX,
		PO_EMPRESA,
		PO_NUMERO,
		PO_FECHA,
		PO_HORA,
		PO_LETRA
FROM #COMPARTE.dbo.POLL
GO

/* V_NIVEL0: CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 117*/
IF EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'V_NIVEL0' AND XTYPE = 'V' )
	DROP VIEW V_NIVEL0
GO

/* Comienza: View de V_NIVEL0 , necesario regenerar para cubrir CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 118*/
CREATE VIEW V_NIVEL0(
					TB_CODIGO,
					TB_ELEMENT )
as
SELECT 	TB_CODIGO,
		TB_ELEMENT
FROM #COMPARTE.dbo.NIVEL0
GO

/* V_COMPANY: CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 119*/
IF EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'V_COMPANY' AND XTYPE = 'V' )
	DROP VIEW V_COMPANY
GO

/* Comienza: View de V_COMPANY , necesario regenerar para cubrir CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 120 */
CREATE VIEW V_COMPANY(
					CM_CODIGO,
					CM_NOMBRE,
					CM_ACUMULA,
					CM_CONTROL,
					CM_EMPATE,
					CM_ALIAS,
					CM_USRNAME,
					CM_USACAFE,
					CM_NIVEL0,
					CM_DATOS,
					CM_DIGITO,
					CM_KCLASIFI,
          CM_KCONFI,
          CM_KUSERS
					 )
AS
SELECT
		CM_CODIGO COLLATE DATABASE_DEFAULT as CM_CODIGO,
		CM_NOMBRE,
		CM_ACUMULA,
		CM_CONTROL,
		CM_EMPATE,
		CM_ALIAS,
		CM_USRNAME,
		CM_USACAFE,
		CM_NIVEL0,
		CM_DATOS,
		CM_DIGITO,
		CM_KCLASIFI,
    CM_KCONFI,
    CM_KUSERS
FROM #COMPARTE.dbo.COMPANY
GO

/* V_BITACORA: CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 121*/
IF EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'V_BITACORA' AND XTYPE = 'V' )
	DROP VIEW V_BITACORA
GO

/* Comienza: View de V_BITACORA , necesario regenerar para cubrir CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 122 */
CREATE VIEW V_BITACORA(
					US_CODIGO,
					BI_FECHA,
					BI_HORA,
					BI_PROC_ID,
					BI_TIPO,
					BI_NUMERO,
					BI_TEXTO,
					CB_CODIGO,
					BI_DATA,
					BI_CLASE,
					BI_FEC_MOV )
as
SELECT 	US_CODIGO,
		BI_FECHA,
		BI_HORA,
		BI_PROC_ID,
		BI_TIPO,
		BI_NUMERO,
		BI_TEXTO,
		CB_CODIGO,
		BI_DATA,
		BI_CLASE,
		BI_FEC_MOV
FROM #COMPARTE.dbo.BITACORA
GO

/* V_ACCESO: CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 123*/
IF EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'V_ACCESO' AND XTYPE = 'V' )
	DROP VIEW V_ACCESO
GO

/* Comienza: View de V_ACCESO , necesario regenerar para cubrir CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 124 */
CREATE VIEW V_ACCESO(
					GR_CODIGO,
					CM_CODIGO,
					AX_NUMERO,
					AX_DERECHO )
AS
SELECT 	GR_CODIGO,
		CM_CODIGO,
		AX_NUMERO,
		AX_DERECHO
FROM #COMPARTE.dbo.ACCESO
GO

/* V_ACCESO: CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 125*/
IF EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'V_BITKIOSC' AND XTYPE = 'V' )
	DROP VIEW V_BITKIOSC
GO

/* Comienza: View de V_BITKIOSC , necesario regenerar para cubrir CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 126 */
CREATE VIEW V_BITKIOSC( CB_CODIGO,
                        CM_CODIGO,
                        BI_FECHA,
                        BI_HORA,
                        BI_TIPO,
                        BI_ACCION,
                        BI_NUMERO,
                        BI_TEXTO,
                        BI_UBICA,
                        BI_KIOSCO,
                        BI_FEC_MOV,
                        BI_FEC_HOR )
as
  SELECT           CB_CODIGO,
                   CM_CODIGO,
                   BI_FECHA,
                   BI_HORA,
                   BI_TIPO,
                   BI_ACCION,
                   BI_NUMERO,
                   BI_TEXTO,
                   BI_UBICA,
                   BI_KIOSCO,
                   BI_FEC_MOV,
                   BI_FEC_HORA

            FROM #COMPARTE.dbo.BITKIOSCO
GO

/* V_BITMISD: CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 127*/
IF EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'V_BITMISD' AND XTYPE = 'V' )
	DROP VIEW V_BITMISD
GO

/* Comienza: View de V_BITMISD , necesario regenerar para cubrir CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 128 */
CREATE VIEW V_BITMISD( CB_CODIGO,
                        CM_CODIGO,
                        BI_FECHA,
                        BI_HORA,
                        BI_TIPO,
                        BI_ACCION,
                        BI_NUMERO,
                        BI_TEXTO,
                        BI_UBICA,
                        BI_KIOSCO,
                        BI_FEC_MOV,
                        BI_FEC_HOR )
as
  SELECT           CB_CODIGO,
                   CM_CODIGO,
                   BI_FECHA,
                   BI_HORA,
                   BI_TIPO,
                   BI_ACCION,
                   BI_NUMERO,
                   BI_TEXTO,
                   BI_UBICA,
                   BI_KIOSCO,
                   BI_FEC_MOV,
                   BI_FEC_HORA

            FROM #COMPARTE.dbo.BITKIOSCO
GO

/* V_BITCAFE: CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 129*/
IF EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'V_BITCAFE' AND XTYPE = 'V' )
	DROP VIEW V_BITCAFE
GO

/* Comienza: View de V_BITCAFE , necesario regenerar para cubrir CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 130 */
CREATE VIEW V_BITCAFE(
       BC_FOLIO,
       CB_CODIGO,
       IV_CODIGO,
       BC_EMPRESA,
       BC_CREDENC,
       BC_FECHA,
       BC_HORA,
       BC_RELOJ,
       BC_TIPO,
       BC_COMIDAS,
       BC_EXTRAS,
       BC_REG_EXT,
       BC_MANUAL,
       CL_CODIGO,
       BC_TGAFETE,
       BC_TIEMPO,
       BC_STATUS,
       BC_MENSAJE,
      BC_RESPUES,
      BC_CHECADA
      )
as
 SELECT   BC_FOLIO,
    CB_CODIGO,
    IV_CODIGO,
    BC_EMPRESA,
    BC_CREDENC,
    BC_FECHA,
    BC_HORA,
    BC_RELOJ,
    BC_TIPO,
    BC_COMIDAS,
    BC_EXTRAS,
    BC_REG_EXT,
    BC_MANUAL,
    CL_CODIGO,
       BC_TGAFETE,
    BC_TIEMPO,
    BC_STATUS,
    BC_MENSAJE,
    BC_RESPUES,
    BC_CHECADA

 FROM #COMPARTE.dbo.BITCAFE
GO

/* V_BITCAFEE: CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 131*/
IF EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'V_BITCAFEE' AND XTYPE = 'V' )
	DROP VIEW V_BITCAFEE
GO

/* Comienza: View de V_BITCAFEE , necesario regenerar para cubrir CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 132 */
CREATE VIEW V_BITCAFEE(
       BC_FOLIO,
       CB_CODIGO,
       IV_CODIGO,
       BC_EMPRESA,
       BC_CREDENC,
       BC_FECHA,
       BC_HORA,
       BC_RELOJ,
       BC_TIPO,
       BC_COMIDAS,
       BC_EXTRAS,
       BC_REG_EXT,
       BC_MANUAL,
       CL_CODIGO,
       BC_TGAFETE,
       BC_TIEMPO,
       BC_STATUS,
       BC_MENSAJE,
      BC_RESPUES,
      BC_CHECADA
      )
as
 SELECT   BC_FOLIO,
    CB_CODIGO,
    IV_CODIGO,
    BC_EMPRESA,
    BC_CREDENC,
    BC_FECHA,
    BC_HORA,
    BC_RELOJ,
    BC_TIPO,
    BC_COMIDAS,
    BC_EXTRAS,
    BC_REG_EXT,
    BC_MANUAL,
    CL_CODIGO,
    BC_TGAFETE,
    BC_TIEMPO,
    BC_STATUS,
    BC_MENSAJE,
    BC_RESPUES,
    BC_CHECADA

 FROM #COMPARTE.dbo.BITCAFE BITCAFE
 /*WHERE BITCAFE.BC_EMPRESA = #DIGITO    Revisar como traducir el #DIGITO para el nuevo Motor de patch*/
GO

/* V_DISXCOM: CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 135*/
IF EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'V_DISXCOM' AND XTYPE = 'V' )
	DROP VIEW V_DISXCOM
GO

/* Comienza: View de V_DISXCOM , necesario regenerar para cubrir CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq:  136*/
CREATE VIEW V_DISXCOM(
		 CM_CODIGO,
		 DI_NOMBRE,
		 DI_TIPO )

as
      SELECT CM_CODIGO,
		DI_NOMBRE ,
		DI_TIPO

      FROM #COMPARTE.dbo.DISXCOM
GO

/* V_ACCARBOL: CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 137*/
IF EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'V_ACCARBOL' AND XTYPE = 'V' )
	DROP VIEW V_ACCARBOL
GO

/* Comienza: View de V_ACCARBOL , necesario regenerar para cubrir CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 138 */
CREATE view V_ACCARBOL (
	AA_SOURCE,
	AX_NUMERO ,
	AA_DESCRIP,
	AA_VERSION,
	AA_POSICIO,
	AA_MODULO ,
	AA_SCREEN ,
	CM_CONTROL
) AS
SELECT AA_SOURCE,
	AX_NUMERO ,
	AA_DESCRIP COLLATE DATABASE_DEFAULT as AA_DESCRIP,
	AA_VERSION,
	AA_POSICIO,
	AA_MODULO ,
	AA_SCREEN,
	(CASE AA_SOURCE WHEN 0 then '3DATOS' WHEN 3 then '3RECLUTA' WHEN 4 then '3VISITA' ELSE '3OTRO' END)
	FROM #COMPARTE.dbo.ACC_ARBOL
where ((select COUNT(*) from #COMPARTE.dbo.AUTOMOD )=0)
/* Este view revisa contra los modulos autorizados. */
/* Si la tabla AUTOMOD esta vacia, se muestran todos los derechos de acceso */
/* AUTOMOD estara vacia cuando nunca se haya autorizado el Sentinel.*/
/* Si el sentinel se autoriza o se renueva la autorizacion, la tabla AUTOMOD se actualiza.*/
/* Esta tabla la actualiza solamente el TressCFG */
OR AA_MODULO in (select AM_CODIGO from #COMPARTE.dbo.AUTOMOD )
union
/* Derechos de acceso de los grupos adicionales creados por el cliente */
/* Los grupos adicionales se agregan en Globales\Campos Adicionales*/
SELECT  	0,
	10000+LLAVE,
	'Empleados-Datos-Adicionales-' + GX_TITULO COLLATE DATABASE_DEFAULT,
	0,
	7+(GX_POSICIO*.10),
	0,
	4,
	'3DATOS'
FROM GRUPO_AD
UNION
/* Derechos de acceso por clasificacion de Reporteador, esta parte cambia para cada cliente*/
SELECT 	1,
	RC_CODIGO,
	'Reporteador-Clasificaciones-'+RC_NOMBRE COLLATE DATABASE_DEFAULT,
	RC_VERSION,
	RC_ORDEN,
	25 ,
	5 ,
	'3DATOS'
FROM R_CLASIFI
/* Derechos de acceso por entidad de reporteador, esta parte cambia para cada cliente */
/* ESta parte se trae ordenado por modulo y despues por la posicion de la tabla dentro del modulo */
union
SELECT
	2,
	R_ENTIDAD.EN_CODIGO,
	'Reporteador-Entidades-' +
	COALESCE(R_MODULO.MO_NOMBRE,'<Sin m�dulo>') +'-' +
	COALESCE(R_ENTIDAD.EN_TITULO,'<Sin entidad>' ) +
	'('+COALESCE(R_ENTIDAD.EN_TABLA,' ')+')',
	R_ENTIDAD.EN_VERSION,
	R_MODULO.MO_ORDEN*10000 + R_MOD_ENT.ME_ORDEN,
	25 ,
	6 ,
	'3DATOS'
FROM R_MOD_ENT
left outer join R_MODULO on R_MODULO.MO_CODIGO= R_MOD_ENT.MO_CODIGO
left outer join R_ENTIDAD on R_ENTIDAD.EN_CODIGO= R_MOD_ENT.EN_CODIGO
/* Esta parte se trae las tablas*/
union
select 	2,
	R_ENTIDAD.EN_CODIGO,
	'Reporteador-Entidades-<Sin m�dulo>' +'-' +
	COALESCE(R_ENTIDAD.EN_TITULO,'<Sin entidad>' ) +
	'('+COALESCE(R_ENTIDAD.EN_TABLA,' ')+')',
	R_ENTIDAD.EN_VERSION,
	9999999 ,
	25 ,
	6 ,
	'3DATOS'
from R_ENTIDAD
where EN_CODIGO not IN (select EN_CODIGO from R_MOD_ENT where R_MOD_ENT.EN_CODIGO = R_ENTIDAD.EN_CODIGO)
GO

/* V_ACC_DER: CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 139*/
IF EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'V_ACC_DER' AND XTYPE = 'V' )
	DROP VIEW V_ACC_DER
GO

/* Comienza: View de V_ACC_DER , necesario regenerar para cubrir CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 140 */
CREATE view V_ACC_DER (
	AA_SOURCE,
	AX_NUMERO,
	AD_TIPO,
	AD_ACCION,
	AD_IMPACTO,
	AD_POSICIO
)AS
SELECT
	AD.AA_SOURCE,
	AD.AX_NUMERO,
	AD_TIPO,
	AD_ACCION COLLATE DATABASE_DEFAULT as AD_ACCION,
	AD_IMPACTO,
	AD_POSICIO
FROM #COMPARTE.dbo.ACC_DER AD
left outer join V_ACCARBOL A
	on A.AA_SOURCE = AD.AA_SOURCE
	AND A.AX_NUMERO = AD.AX_NUMERO
where AA_MODULO IS not null
union
select  AA_SOURCE,
	AX_NUMERO ,
  	CASE
    		WHEN charIndex('-Registro', AA_DESCRIP) <>0 THEN 1
    		WHEN charIndex('-Procesos',AA_DESCRIP) <>0 THEN 2
    		WHEN charIndex('-Anuales',AA_DESCRIP) <>0 THEN 3
		else 0
  	END,
  	CASE
    		WHEN charIndex('-Registro', AA_DESCRIP) <>0 THEN 'Registrar'
    		WHEN charIndex('-Procesos',AA_DESCRIP) <>0 THEN 'Ejecutar'
    		WHEN charIndex('-Anuales',AA_DESCRIP) <>0 THEN 'Ejecutar'
		else 'Consultar'
  	END COLLATE DATABASE_DEFAULT,

	4,
	0 from #COMPARTE.dbo.acc_arbol A
where A.ax_numero not in ( select ax_numero from #COMPARTE.dbo.acc_der)
UNION
SELECT
	AA_SOURCE,
	AX_NUMERO,
	AD_TIPO,
	AD_ACCION COLLATE DATABASE_DEFAULT,
	AD_IMPACTO,
	AD_POSICIO
from V_ACC_AD
UNION
SELECT
	AA_SOURCE,
	AX_NUMERO,
	AD_TIPO,
	AD_ACCION COLLATE DATABASE_DEFAULT,
	AD_IMPACTO,
	AD_POSICIO
from V_ACC_RDD
GO

/* V_CO_X_GR: CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 141*/
IF EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'V_CO_X_GR' AND XTYPE = 'V' )
	DROP VIEW V_CO_X_GR
GO

/* Comienza: View de V_CO_X_GR , necesario regenerar para cubrir CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 142  */
create VIEW V_CO_X_GR (
	CM_CODIGO,
	CM_CONTROL,
	GR_CODIGO
)
AS
select  CM_CODIGO COLLATE DATABASE_DEFAULT,
	CASE CM_CONTROL
		WHEN '3DATOS' THEN 0
		WHEN '3RECLUTA' THEN 3
		WHEN '3VISITA' THEN 4
		ELSE 5 END,
	GR_CODIGO
from #COMPARTE.dbo.GRUPO, #COMPARTE.dbo.COMPANY
GO

/* V_ACCALL: CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 143 */
IF EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'V_ACCALL' AND XTYPE = 'V' )
	DROP VIEW V_ACCALL

GO

/* Comienza: View de V_ACCALL , necesario regenerar para cubrir CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 144 */
CREATE VIEW V_ACCALL(
		AA_SOURCE,
		CM_CODIGO,
		GR_CODIGO,
		AX_NUMERO
		 )
AS
SELECT 	CASE (SELECT CM_CONTROL FROM #COMPARTE.dbo.COMPANY C WHERE
		C.CM_CODIGO=A.CM_CODIGO)
	WHEN '3DATOS' THEN 0
	WHEN '3RECLUTA' THEN 3
	WHEN '3VISITA' THEN 4
	ELSE 5 END,
	CM_CODIGO COLLATE DATABASE_DEFAULT as CM_CODIGO,
	GR_CODIGO,
	AX_NUMERO
FROM #COMPARTE.dbo.ACCESO A
union
select    AA_SOURCE,
	V_CO_X_GR.CM_CODIGO COLLATE DATABASE_DEFAULT,
	V_CO_X_GR.GR_CODIGO,
	AX_NUMERO
from V_CO_X_GR, #COMPARTE.dbo.ACC_ARBOL
where V_CO_X_GR.CM_CONTROL = AA_SOURCE
GO


/* V_ACCEMP: CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 145 */
IF EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'V_ACCEMP' AND XTYPE = 'V' )
	DROP VIEW V_ACCEMP

GO 


/* Comienza: View de V_ACCEMP , necesario regenerar para cubrir CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 146 */ 
CREATE VIEW V_ACCEMP(
		AA_SOURCE,
		CM_CODIGO,
		GR_CODIGO,
		AX_NUMERO,
		AX_DERECHO )
AS
/* Estos son los derechos de acceso que ya estan grabados, no importa si el derecho esta prendido o no*/
/* A la tabla ACCESO llega la informacion cuando el usuario entra a editar derechos de acceso , si el usuario nunca entra
a esa ventana (para determinado grupo) , ese grupo no tendra informacion en esa tabla*/
SELECT 	CASE (SELECT CM_CONTROL FROM #COMPARTE.dbo.COMPANY C
		WHERE C.CM_CODIGO=V.CM_CODIGO COLLATE DATABASE_DEFAULT)
	WHEN '3DATOS' THEN 0
	WHEN '3RECLUTA' THEN 3
	WHEN '3VISITA' THEN 4
	ELSE 5 END,
	CM_CODIGO COLLATE DATABASE_DEFAULT as CM_CODIGO,
	GR_CODIGO,
	AX_NUMERO,
	CASE WHEN GR_CODIGO=1 THEN 4095 ELSE
	coalesce( (SELECT A.AX_DERECHO FROM #COMPARTE.dbo.ACCESO A
		WHERE A.CM_CODIGO COLLATE DATABASE_DEFAULT = V.CM_CODIGO COLLATE DATABASE_DEFAULT
		AND A.GR_CODIGO = V.GR_CODIGO
		AND A.AX_NUMERO =  V.AX_NUMERO ) ,0)
	END
FROM V_ACCALL V
UNION
select 	AA_SOURCE,
	CM_CODIGO COLLATE DATABASE_DEFAULT,
	GR_CODIGO,
	(select 10000+LLAVE from GRUPO_AD G WHERE G.GX_CODIGO=V_GRADALL.AX_NUMERO),
	CASE WHEN GR_CODIGO=1 THEN 4095 ELSE
	coalesce( (SELECT G.GX_DERECHO FROM GR_AD_ACC G
		WHERE G.CM_CODIGO = V_GRADALL.CM_CODIGO
		AND G.GR_CODIGO = V_GRADALL.GR_CODIGO
		AND G.GX_CODIGO =  V_GRADALL.AX_NUMERO ) ,0)
	END
FROM V_GRADALL
UNION
SELECT 	AA_SOURCE,
	CM_CODIGO COLLATE DATABASE_DEFAULT,
	GR_CODIGO,
	AX_NUMERO,
	CASE WHEN GR_CODIGO=1 THEN 4095 ELSE
	coalesce( (SELECT RA_DERECHO FROM R_CLAS_ACC R
		WHERE R.CM_CODIGO = V_RCLAALL.CM_CODIGO
		AND R.GR_CODIGO = V_RCLAALL.GR_CODIGO
		AND R.RC_CODIGO =  V_RCLAALL.AX_NUMERO ) ,0) END
FROM V_RCLAALL
UNION
SELECT 	AA_SOURCE,
	CM_CODIGO COLLATE DATABASE_DEFAULT,
	GR_CODIGO,
	AX_NUMERO,
	CASE WHEN GR_CODIGO=1 THEN 4095 ELSE
	coalesce( (SELECT RE_DERECHO FROM R_ENT_ACC R
		WHERE R.CM_CODIGO = V_RENTALL.CM_CODIGO
		AND R.GR_CODIGO   = V_RENTALL.GR_CODIGO
		AND R.EN_CODIGO   = V_RENTALL.AX_NUMERO ) ,0) END
FROM V_RENTALL
GO


/* V_ACCUSU: CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 147*/
IF EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'V_ACCUSU' AND XTYPE = 'V' )
	DROP VIEW V_ACCUSU

GO 


/* Comienza: View de V_ACCUSU , necesario regenerar para cubrir CR 2199 Error de Linked Servers en Minusculas */
/* Patch 430 # Seq: 148 */ 
CREATE VIEW V_ACCUSU( US_CODIGO,
		  US_CORTO ,
 		  GR_CODIGO,
                      US_NIVEL ,
                      US_NOMBRE,
                      US_BLOQUEA,
                      US_CAMBIA,
                      US_FEC_IN,
                      US_FEC_OUT,
                      US_DENTRO,
                      US_ARBOL,
                      US_FORMA,
                      US_BIT_LST,
                      US_BIO_ID,
                      US_FEC_SUS,
                      US_FALLAS,
                      US_MAQUINA,
                      US_FEC_PWD,
                      US_EMAIL,
                      US_FORMATO,
                      US_LUGAR,
                      US_PAGINAS,
                      US_ANFITRI,
                      US_PORTAL,
                      US_ACTIVO,
                      US_DOMAIN,
                      US_JEFE
                               )
as
      SELECT      US_CODIGO,
                  US_CORTO ,
                  GR_CODIGO,
                  US_NIVEL ,
                  US_NOMBRE,
                  US_BLOQUEA,
                  US_CAMBIA ,
                  US_FEC_IN ,
                  US_FEC_OUT,
                  US_DENTRO ,
                  US_ARBOL  ,
                  US_FORMA  ,
                  US_BIT_LST,
                  US_BIO_ID ,
                  US_FEC_SUS,
                  US_FALLAS ,
                  US_MAQUINA,
                  US_FEC_PWD,
                  US_EMAIL  ,
                  US_FORMATO,
                  US_LUGAR  ,
                  US_PAGINAS,
                  US_ANFITRI,
                  US_PORTAL,
                  US_ACTIVO,
                  US_DOMAIN,
                  US_JEFE
      FROM #COMPARTE.dbo.USUARIO
      
GO 


/* CR2403  Mejoras en Admin. Vacaciones */
/*Patch #430  Seq: 149*/
CREATE VIEW dbo.vCurrentDateTime 
AS  
  SELECT gd = GETDATE()
GO

/* SP_PATCH : CR:2460, Borra posible vista existente V_EMP_BIO hacia #COMPARTE..EMP_BIO */
/* Patch 430 Seq: 150*/
if exists( select NAME from SYSOBJECTS where NAME = 'V_EMP_BIO' and XTYPE = 'V' )
	drop view V_EMP_BIO
go

/* SP_PATCH : CR:2460, Crea vista V_EMP_BIO hacia #COMPARTE..EMP_BIO */
/* Patch 430 Seq: 151*/
/* Nota: Es necesario sustituir #COMPARTE con el nombre del COMPARTE local */
create view V_EMP_BIO as select ID_NUMERO, CM_CODIGO, CB_CODIGO, IV_CODIGO from #COMPARTE.dbo.EMP_BIO
go
/* SP_PATCH : CR:2460, Borra posible vista existente V_WS_BITAC hacia #COMPARTE..WS_BITAC */
/* Patch 430 Seq: 133*/
if exists( select NAME from SYSOBJECTS where NAME = 'V_DISPOSIT' and XTYPE = 'V' )
	drop view V_DISPOSIT
go

/* SP_PATCH : CR:2460, Crea vista V_DISPOSIT hacia #COMPARTE..DISPOSIT */
/* Patch 430 Seq: 134 */
/* Nota: Es necesario sustituir #COMPARTE con el nombre del COMPARTE local */
create view V_DISPOSIT(
		DI_NOMBRE,
		DI_TIPO,
 		DI_DESCRIP,
        DI_NOTA,
        DI_IP
		/*DI_SYNC*/
		)

as 
	select DI_NOMBRE,
	  DI_TIPO,
	  DI_DESCRIP,
	  DI_NOTA,
	  DI_IP
	  /*DI_SYNC*/
      from #COMPARTE.dbo.DISPOSIT
go

/* SP_PATCH : CR:2460, Borra posible vista existente V_WS_BITAC hacia #COMPARTE..WS_BITAC */
/* Patch 430 Seq: 152*/
if exists( select NAME from SYSOBJECTS where NAME = 'V_GRUPOTER' and XTYPE = 'V' )
	drop view V_GRUPOTER
go

/* SP_PATCH : CR:2460, Crea vista V_GRUPOTER hacia #COMPARTE..GRUPOTERM */
/* Patch 430 Seq: 153 */
/* Nota: Es necesario sustituir #COMPARTE con el nombre del COMPARTE local */
create view V_GRUPOTER(
	GP_CODIGO,
	GP_DESCRIP)
as
	select GP_CODIGO,
		GP_DESCRIP
	from #COMPARTE.dbo.GRUPOTERM
go

/* SP_PATCH : CR:2460, Borra posible vista existente V_WS_BITAC hacia #COMPARTE..WS_BITAC */
/* Patch 430 Seq: 154 */
if exists( select NAME from SYSOBJECTS where NAME = 'V_DISPOGRU' and XTYPE = 'V' )
	drop view V_DISPOGRU
go

/* SP_PATCH : CR:2460, Crea vista V_DISPOGRU hacia #COMPARTE..DISPOGRUPO */
/* Patch 430 Seq: 155 */
/* Nota: Es necesario sustituir #COMPARTE con el nombre del COMPARTE local 
create view V_DISPOGRU(
	DI_NOMBRE,
	DI_TIPO,
	GP_CODIGO)
as
	select DI_NOMBRE,
		DI_TIPO,
		GP_CODIGO
	from #COMPARTE.dbo.DISPOGRUPO
go
*/
/* SP_PATCH : CR:2460 */
/* REPETIDO */
/* Patch 430 Seq: 150 */
if exists( select NAME from SYSOBJECTS where NAME = 'V_EMP_BIO' and XTYPE = 'V' )
	drop view V_EMP_BIO
go

/* SP_PATCH : CR:2460, Crea vista V_EMP_BIO hacia #COMPARTE..EMP_BIO */
/* Patch 430 Seq: 151 */
/* Nota: Es necesario sustituir #COMPARTE con el nombre del COMPARTE local */
create view V_EMP_BIO(
	ID_NUMERO,
	CM_CODIGO,
	CB_CODIGO,
	GP_CODIGO,
	IV_CODIGO)
as
	select ID_NUMERO,
		CM_CODIGO,
		CB_CODIGO,
		GP_CODIGO,
		IV_CODIGO
	from #COMPARTE.dbo.EMP_BIO
go

/* SP_PATCH : CR:2460, Borra posible vista existente V_WS_BITAC hacia #COMPARTE..WS_BITAC */
/* Patch 430 Seq: 156 */
if exists( select NAME from SYSOBJECTS where NAME = 'V_WS_BITAC' and XTYPE = 'V' )
	drop view V_WS_BITAC
go

/* SP_PATCH : CR:2460, Crea vista V_WS_BITAC hacia #COMPARTE..WS_BITAC */
/* Patch 430 Seq: 157 */
/* Nota: Es necesario sustituir #COMPARTE con el nombre del COMPARTE local */
create view V_WS_BITAC(
	WS_FECHA,
	CH_RELOJ,
	WS_CHECADA,
	CB_CODIGO,
	WS_MENSAJE,
	CM_CODIGO,
	WS_TIPO)
as
	select WS_FECHA,
		CH_RELOJ,
		WS_CHECADA,
		CB_CODIGO,
		WS_MENSAJE,
		CM_CODIGO,
		WS_TIPO
	from #COMPARTE.dbo.WS_BITAC
go


/* SP_PATCH : CR:2460, Borra posible vista existente VWS_ACCESS hacia #COMPARTE..WS_ACCESS */
/* Patch 430 Seq: 158 */
if exists( select NAME from SYSOBJECTS where NAME='VWS_ACCESS' and XTYPE='V' )
	drop view VWS_ACCESS
go

/* SP_PATCH : CR:2460, Crea vista VWS_ACCESS hacia #COMPARTE..WS_ACCESS */
/* Patch 430 Seq: 159 */
/* Nota: Es necesario sustituir #COMPARTE con el nombre del COMPARTE local */
create view VWS_ACCESS
(
	WA_FECHA,
	WA_HORA,
	CM_CODIGO,
	CB_CODIGO,
	CH_RELOJ,
	WS_CHECADA 
)
as
	select WA_FECHA, convert(varchar(8), WA_FECHA, 108) as WA_HORA, CM_CODIGO, CB_CODIGO, CH_RELOJ, WS_CHECADA from #COMPARTE.dbo.WS_ACCESS
go

GO 

/* 2320 Fecha cursos programados cuando cambia de puesto */
/*Patch #430  Seq: 160*/
create VIEW CUR_PROG (
  CB_CODIGO, 
  CB_PUESTO, 
  CU_CODIGO, 
  KC_FEC_PRO, 
  KC_EVALUA, 
  KC_FEC_TOM, 
  KC_HORAS, 
  CU_HORAS, 
  EN_OPCIONA, 
  EN_LISTA, 
  MA_CODIGO, 
  KC_PROXIMO, 
  KC_REVISIO, 
  CU_REVISIO,
  CP_MANUAL,
  EP_GLOBAL
) AS
select
  C.CB_CODIGO,
  C.CB_PUESTO,
  CURSO.CU_CODIGO,
 ( Select case when (KARCURSO.KC_FEC_TOM IS NULL )then DATEADD( DAY, E.EN_DIAS, C.CB_FEC_PTO ) else KARCURSO.KC_FECPROG end )KC_FEC_PRO,
  KARCURSO.KC_EVALUA,
  KARCURSO.KC_FEC_TOM,
  KARCURSO.KC_HORAS,
  CU_HORAS,
  E.EN_OPCIONA,
  E.EN_LISTA,
  CURSO.MA_CODIGO,
  (SELECT MIN(SE_FEC_INI) AS Expr1 FROM  dbo.SESION WHERE (CU_CODIGO = E.CU_CODIGO) AND (SE_FEC_INI >= CAST(FLOOR(CAST(GETDATE() AS float)) AS Datetime))) AS KC_PROXIMO, 
  KARCURSO.KC_REVISIO,
  CURSO.CU_REVISIO,
  cast( 'N' as CHAR(1) ) CP_MANUAL,
  cast( 'N' as CHAR(1) ) EP_GLOBAL
  from COLABORA C
  join ENTRENA E on ( E.PU_CODIGO = C.CB_PUESTO )
  left join KARCURSO on ( KARCURSO.CB_CODIGO = C.CB_CODIGO ) and ( KARCURSO.CU_CODIGO = E.CU_CODIGO )
  left join CURSO on ( CURSO.CU_CODIGO = E.CU_CODIGO )
where ( CURSO.CU_ACTIVO = 'S' ) 
UNION ALL
select
  C.CB_CODIGO,
  C.CB_PUESTO,
  CU.CU_CODIGO,
  ( select case when K.KC_FEC_TOM is null then NULL
		        when ( ( E.EN_RE_DIAS > 0 AND E.EN_REPROG = 'N' ) OR ( ( E.EN_RE_DIAS > 0 AND E.EN_REPROG = 'S' )and ( K.KC_REVISIO = CU.CU_REVISIO ) ) )then DATEADD( DAY, E.EN_RE_DIAS, K.KC_FEC_TOM )
				when ( ( E.EN_RE_DIAS > 0 AND E.EN_REPROG = 'S' ) and ( K.KC_FEC_TOM > ( select max(CH_FECHA) from CUR_REV where( ( CUR_REV.CU_CODIGO = CU.CU_CODIGO ) AND ( CUR_REV.CH_REVISIO = CU.CU_REVISIO ) ) ) ) and (K.KC_REVISIO <> CU.CU_REVISIO ))then DATEADD( DAY, E.EN_RE_DIAS, K.KC_FEC_TOM )
				else (select max(CH_FECHA) from CUR_REV where( ( CUR_REV.CU_CODIGO = CU.CU_CODIGO ) AND ( CUR_REV.CH_REVISIO = CU.CU_REVISIO ) ) )End ) KC_FEC_PRO,
  cast(0 as Decimal(15,2)) KC_EVALUA,
  cast(NULL as DateTime) KC_FEC_TOM,
  cast(0 as Decimal(15,2)) KC_HORAS,
  CU.CU_HORAS,
  E.EN_OPCIONA,
  E.EN_LISTA,
  CU.MA_CODIGO,
  (SELECT MIN(SE_FEC_INI) AS Expr1 FROM  dbo.SESION WHERE (CU_CODIGO = E.CU_CODIGO) AND (SE_FEC_INI >= CAST(FLOOR(CAST(GETDATE() AS float)) AS DateTime))) KC_PROXIMO,
  cast(' ' as varchar(10))KC_REVISIO,               
  CU.CU_REVISIO,                  
  cast( 'N' as CHAR(1) ) CP_MANUAL,  
  cast( 'N' as CHAR(1) ) EP_GLOBAL
  from COLABORA C
  join ENTRENA E on ( E.PU_CODIGO = C.CB_PUESTO )
  left join KARCURSO K on ( K.CB_CODIGO = C.CB_CODIGO ) and ( K.CU_CODIGO = E.CU_CODIGO ) and ( K.KC_FEC_TOM = ( select MAX( KC_FEC_TOM ) from KARCURSO AS K1 where ( K1.CB_CODIGO = C.CB_CODIGO ) and ( K1.CU_CODIGO = E.CU_CODIGO ) ))
  left join CURSO CU on ( CU.CU_CODIGO = E.CU_CODIGO )
where ( Cu.CU_ACTIVO = 'S' ) and 
		( ( E.EN_RE_DIAS > 0 ) AND NOT ( K.KC_FEC_TOM is NULL) or
        ( ( E.EN_REPROG = 'S' ) and ( K.KC_REVISIO <> CU.CU_REVISIO ) and (
          ( select count(k2.KC_REVISIO) FROM karcurso k2 where k2.KC_REVISIO = CU.CU_REVISIO AND ( k2.CB_CODIGO = C.CB_CODIGO ) and ( k2.CU_CODIGO = E.CU_CODIGO ) ) = 0 ) ))
UNION ALL
select
  C.CB_CODIGO,
  C.CB_PUESTO,
  CURSO.CU_CODIGO,
  (select case when (EP.EP_PORDIAS = 'S' ) then C.CB_FEC_PTO + EP.EP_DIAS else EP.EP_FECHA end	)KC_FEC_PRO,
  K.KC_EVALUA,
  K.KC_FEC_TOM,
  K.KC_HORAS,
  CURSO.CU_HORAS,
  EP.EP_OPCIONA,
  cast( 'N' as CHAR(1) ) EN_LISTA,
  CURSO.MA_CODIGO,
  (SELECT MIN(SE_FEC_INI) AS Expr1 FROM dbo.SESION WHERE (CU_CODIGO = EP.CU_CODIGO) AND (SE_FEC_INI >= CAST(FLOOR(CAST(GETDATE() AS float)) AS Datetime))) AS KC_PROXIMO, 
  K.KC_REVISIO,
  CURSO.CU_REVISIO,
  cast( 'S' as CHAR(1) ) CP_MANUAL,	
  EP.EP_GLOBAL
  from COLABORA C
  join EMP_PROG EP on ( EP.CB_CODIGO = C.CB_CODIGO )
  left join CURSO on ( CURSO.CU_CODIGO = EP.CU_CODIGO )
  left join KARCURSO K on ( K.CU_CODIGO = EP.CU_CODIGO and EP.CB_CODIGO = K.CB_CODIGO and K.KC_FEC_TOM >= (select case when (EP.EP_PORDIAS = 'S' ) then C.CB_FEC_PTO + EP.EP_DIAS else EP.EP_FECHA end	) )
where ( CURSO.CU_ACTIVO = 'S' )
GO   
/* 2320 Fecha cursos programados cuando cambia de puesto */
/*Patch #430  Seq: 161*/
create view CER_PROG (
   CB_CODIGO, 
   CB_PUESTO, 
   CI_CODIGO, 
   KI_FEC_PRO, 
   KI_FEC_TOM,
   KI_CALIF_1, 
   KI_CALIF_2, 
   KI_CALIF_3, 
   PC_OPCIONA, 
   PC_LISTA 
   ) AS
select
  C.CB_CODIGO,
  C.CB_PUESTO,
  CERTIFIC.CI_CODIGO,
  DATEADD( DAY, E.PC_DIAS, C.CB_FEC_PTO ) KC_FEC_PRO,
  KAR_CERT.KI_FEC_CER,
  KAR_CERT.KI_CALIF_1,
  KAR_CERT.KI_CALIF_2,
  KAR_CERT.KI_CALIF_3,
  E.PC_OPCIONA,
  E.PC_LISTA
from COLABORA C
join PTO_CERT E on ( E.PU_CODIGO = C.CB_PUESTO )
left join KAR_CERT on ( KAR_CERT.CB_CODIGO = C.CB_CODIGO ) and ( KAR_CERT.CI_CODIGO = E.CI_CODIGO )
left join CERTIFIC on ( CERTIFIC.CI_CODIGO = E.CI_CODIGO )
where ( CERTIFIC.CI_ACTIVO = 'S' )
GO

/* 2303 Limite de Impresiones en KIOSCO */
/*Patch #430  Seq: 162*/
create view dbo.V_KIOS_IMP
( 
	CB_CODIGO ,
	CM_CODIGO ,
	PE_TIPO ,
	PE_YEAR ,
	PE_NUMERO ,
	KI_FECHA ,
	KI_KIOSCO ,
	KI_HORA ,
	RE_CODIGO 
)as
select 	
    CB_CODIGO ,
	CM_CODIGO ,
	PE_TIPO ,
	PE_YEAR ,
	PE_NUMERO ,
	KI_FECHA ,
	KI_KIOSCO ,
	KI_HORA ,
	RE_CODIGO 
from 
	#COMPARTE.dbo.KIOS_IMPR
GO
/* 2377 Encuestas de Kiosco*/
/*Patch #430  Seq: 164 */
CREATE VIEW V_ENCUESTA(
	   ENC_CODIGO
      ,ENC_NOMBRE
      ,ENC_PREGUN
      ,ENC_CONFID
      ,ENC_VOTOSE
      ,ENC_STATUS
      ,US_CODIGO
      ,ENC_FECINI
      ,ENC_FECFIN
      ,ENC_HORINI
      ,ENC_HORFIN
      ,ENC_MRESUL
      ,ENC_FILTRO
      ,ENC_COMPAN
      ,ENC_KIOSCS
      ,ENC_OPCION
      ,ENC_MSJCON
	  ,ENC_OPCGAN
	  ,ENC_VOTGAN
)
as
  SELECT ENC_CODIGO
      ,ENC_NOMBRE
      ,ENC_PREGUN
      ,ENC_CONFID
      ,ENC_VOTOSE
      ,ENC_STATUS
      ,US_CODIGO
      ,ENC_FECINI
      ,ENC_FECFIN
      ,ENC_HORINI
      ,ENC_HORFIN
      ,ENC_MRESUL
      ,ENC_FILTRO
      ,ENC_COMPAN
      ,ENC_KIOSCS
      ,ENC_OPCION
      ,ENC_MSJCON
	  ,#Comparte.[dbo].[SP_MAX_VOTOS_ENCUESTA](ENC_CODIGO,1)AS ENC_OPCGAN
	  ,#Comparte.[dbo].[SP_MAX_VOTOS_ENCUESTA](ENC_CODIGO,0)AS ENC_VOTGAN
      FROM #Comparte.dbo.KENCUESTAS
GO

/* 2377 Encuestas de Kiosco*/
/*Patch #430  Seq: 165*/
CREATE VIEW V_VOTOS(
	   ENC_CODIGO
      ,CB_CODIGO
      ,CM_CODIGO
      ,CB_CREDENC
      ,VO_VALOR
      ,VO_FECHA
      ,VO_HORA
      ,VO_KIOSCO)
as
  SELECT
       ENC_CODIGO
      ,CB_CODIGO
      ,CM_CODIGO
      ,CB_CREDENC
      ,VO_VALOR
      ,VO_FECHA
      ,VO_HORA
      ,VO_KIOSCO
   FROM #Comparte.dbo.KVOTOS 
GO

/* 2377 Encuestas de Kiosco*/
/*Patch #430  Seq: 166*/
CREATE VIEW V_OPC_ENCS(
	  OP_ORDEN
      ,OP_TITULO
      ,OP_DESCRIP
      ,ENC_CODIGO
      ,US_CODIGO)
as
  SELECT OP_ORDEN
      ,OP_TITULO
      ,OP_DESCRIP
      ,ENC_CODIGO
      ,US_CODIGO
  FROM #Comparte.dbo.KOPCIONES
GO

/* 2377 Encuestas de Kiosco*/
/*Patch #430  Seq: 167*/
Create View V_RES_ENCU( 
	ENC_CODIGO,
	OP_ORDEN,
	OP_TITULO,
	PORCENTAJE,
	TOTAL)
AS
	select V.ENC_CODIGO,VO_VALOR,O.OP_TITULO ,CONVERT( VARCHAR, ( CONVERT( decimal(4,1), Round((CAST( Count(vo_valor)* 100 AS dECIMAL(6,2)) / CAST( (Select Count(*) From #Comparte.dbo.KVOTOS where Enc_Codigo = V.ENC_CODIGO ) as Decimal (6,2) ) ),1) ) ) )+'%' as Opcion, Count(VO_VALOR)as Total from
	#Comparte.dbo.KVOTOS V left outer join #Comparte.dbo.KOPCIONES O on O.OP_ORDEN = V.VO_VALOR and O.ENC_CODIGO = V.ENC_CODIGO
	Group by O.OP_TITULO,V.ENC_CODIGO,VO_VALOR
GO

/*2499  Administraci�n de Seguros de Gastos M�dicos*/
/*Patch #430  Seq: 168*/
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].V_EMP_POL') AND type in (N'V'))
	Drop VIEW V_EMP_POL;
GO
/*2499  Administraci�n de Seguros de Gastos M�dicos*/
/*Patch #430  Seq: 169 */
CREATE VIEW V_EMP_POL(
    CB_CODIGO  ,
	PM_CODIGO  ,
	PV_REFEREN ,
	EP_TIPO    ,
	PA_FOLIO   ,
	PA_RELACIO ,
	EP_CERTIFI ,
	EP_FEC_INI ,
	EP_FEC_FIN ,
	EP_CFIJO   ,
	EP_CPOLIZA ,
	EP_CTITULA ,
	EP_OBSERVA ,
	EP_CANCELA ,
	EP_STATUS ,
	EP_CAN_FEC ,
	EP_CAN_MON ,
	EP_CAN_MOT ,
	EP_GLOBAL  ,
	US_CODIGO  ,
	EP_FEC_REG ,
	EP_DEPEND ,
	LLAVE
)
AS
SELECT E.CB_CODIGO  ,
	E.PM_CODIGO  ,
	E.PV_REFEREN ,
	E.EP_TIPO    ,
	E.PA_FOLIO   ,
	E.PA_RELACIO ,
	E.EP_CERTIFI ,
	E.EP_FEC_INI ,
	E.EP_FEC_FIN ,
	E.EP_CFIJO   ,
	E.EP_CPOLIZA ,
	E.EP_CTITULA ,
	E.EP_OBSERVA ,
	E.EP_CANCELA ,
	E.EP_STATUS,
	E.EP_CAN_FEC ,
	E.EP_CAN_MON ,
	E.EP_CAN_MOT ,
	E.EP_GLOBAL  ,
	E.US_CODIGO  ,
	E.EP_FEC_REG ,
    ( P.PA_NOMBRE + ' ( ' + dbo.TF(44,E.PA_RELACIO)+' )' ) as EP_DEPEND,
    E.LLAVE
from EMP_POLIZA E
LEFT OUTER JOIN PARIENTE P on P.PA_RELACIO = E.PA_RELACIO and P.PA_FOLIO = E.PA_FOLIO and P.CB_CODIGO = E.CB_CODIGO;
GO
/*2499  Administraci�n de Seguros de Gastos M�dicos*/
/*Patch #430  Seq: 170*/
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].V_POL_VIG') AND type in (N'V'))
	Drop VIEW V_POL_VIG;
GO
/*2499  Administraci�n de Seguros de Gastos M�dicos*/
/*Patch #430  Seq: 171*/
CREATE VIEW V_POL_VIG(
    PM_CODIGO  ,
	PV_REFEREN ,
	PV_FEC_INI ,
	PV_FEC_FIN ,
	PV_CONDIC  ,
	PV_TEXTO   ,
	PV_NUMERO  ,
	PV_STITULA  ,
	PV_SDEPEND
)
AS
SELECT
	P.PM_CODIGO  ,
	P.PV_REFEREN ,
	P.PV_FEC_INI ,
	P.PV_FEC_FIN ,
	P.PV_CONDIC  ,
	P.PV_TEXTO   ,
	P.PV_NUMERO  ,
	(select Count(CB_CODIGO) from EMP_POLIZA E where E.PM_CODIGO = P.PM_CODIGO and E.PV_REFEREN = P.PV_REFEREN and E.EP_TIPO = 0) PV_STITULA,
	(select Count(CB_CODIGO) from EMP_POLIZA E where E.PM_CODIGO = P.PM_CODIGO and E.PV_REFEREN = P.PV_REFEREN and E.EP_TIPO = 1) PV_SDEPEND
from POL_VIGENC P;
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 172*/
CREATE VIEW CRIT_COSTO
AS
	SELECT	CritCostoID		CR_ID,
			CritCostoNombre	CR_NOMBRE,
			CritCostoActivo	CR_ACTIVO
	FROM	T_CriteriosCosto
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 173*/
CREATE VIEW GPO_COSTO
AS
	SELECT	GpoCostoID		GC_ID,
			GpoCostoCodigo	GC_CODIGO,
			GpoCostoNombre	GC_NOMBRE,
			GpoCostoActivo	GC_ACTIVO
	FROM	T_GruposCosto
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 174 */
CREATE VIEW CONC_COSTO
AS
	SELECT	CC.CO_NUMERO	CO_NUMERO,
			CONCEPTO.CO_DESCRIP,
			CC.GpoCostoID	GC_ID,
			GpoCostoCodigo	GC_CODIGO,
			GpoCostoNombre	GC_NOMBRE,
			CC.CritCostoID	CR_ID,
			CritCostoNombre	CR_NOMBRE
	FROM	T_ConceptosCosto CC
				JOIN CONCEPTO ON CC.CO_NUMERO = CONCEPTO.CO_NUMERO
				JOIN T_GruposCosto ON CC.GpoCostoID = T_GruposCosto.GpoCostoID
				JOIN T_CriteriosCosto ON CC.CritCostoID = T_CriteriosCosto.CritCostoID
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 175*/
CREATE VIEW CCOSTO
AS
	SELECT	TB_CODIGO	CC_CODIGO,
			TB_ELEMENT	CC_NOMBRE,
			TB_INGLES	CC_INGLES,
			TB_NUMERO	CC_NUMERO,
			TB_TEXTO	CC_TEXTO,
			TB_SUB_CTA	CC_SUB_CTA,
			TB_ACTIVO	CC_ACTIVO,
			LLAVE
	FROM	NIVEL9
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 176*/
CREATE VIEW PROP_COSTO
AS
	SELECT	PE_YEAR,
			PE_TIPO,
			PE_NUMERO,
			CB_CODIGO,
			PC.CritCostoID CR_ID,
			CritCostoNombre	CR_NOMBRE,
			PC.CC_CODIGO,
			CC_NOMBRE,
			PropCostoClasifi PC_CLASIFI,
			PropCostoBase PC_BASE
	FROM	T_ProporcionCostos PC
				JOIN NOMINA ON PC.NominaID = NOMINA.LLAVE
				JOIN T_CriteriosCosto ON PC.CritCostoID = T_CriteriosCosto.CritCostoID
				LEFT JOIN CCOSTO ON PC.CC_CODIGO = CCOSTO.CC_CODIGO
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 177*/
CREATE VIEW COSTOS
AS
	SELECT	PE_YEAR,
			PE_TIPO,
			PE_NUMERO,
			CB_CODIGO,
			CT.CO_NUMERO,
			COALESCE( CO_DESCRIP, '' ) CO_DESCRIP,
			CT.MO_REFEREN,
			CT.GpoCostoID GC_ID,
			GpoCostoCodigo GC_CODIGO,
			GpoCostoNombre GC_NOMBRE,
			CT.CC_CODIGO,
			COALESCE( CC_NOMBRE, '' ) CC_NOMBRE,
			CostoClasifi CT_CLASIFI,
			CostoMonto CT_MONTO
	FROM	T_Costos CT
				JOIN NOMINA ON CT.NominaID = NOMINA.LLAVE
				JOIN T_GruposCosto ON CT.GpoCostoID = T_GruposCosto.GpoCostoID
				LEFT JOIN CONCEPTO ON CT.CO_NUMERO = CONCEPTO.CO_NUMERO
				LEFT JOIN CCOSTO ON CT.CC_CODIGO = CCOSTO.CC_CODIGO
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 178*/
CREATE FUNCTION SP_AUSENCIA_COSTEO( @Empleado NumeroEmpleado, @Fecha Fecha)
RETURNS Codigo
AS
BEGIN
	declare @NivelCosteo FolioChico
	declare @Resultado Codigo

    select   @NivelCosteo = dbo.SP_GET_NIVEL_COSTEO();

	if @NivelCosteo = 0
		select @Resultado = ''
	else
		select @Resultado =
					CASE  @NivelCosteo
						WHEN 1 THEN CB_NIVEL1
						WHEN 2 THEN CB_NIVEL2
						WHEN 3 THEN CB_NIVEL3
						WHEN 4 THEN CB_NIVEL4
						WHEN 5 THEN CB_NIVEL5
						WHEN 6 THEN CB_NIVEL6
						WHEN 7 THEN CB_NIVEL7
						WHEN 8 THEN CB_NIVEL8
						WHEN 9 THEN CB_NIVEL9
					/* ACS
						WHEN 10 THEN CB_NIVEL10
						WHEN 11  THEN CB_NIVEL11
						WHEN 12 THEN CB_NIVEL12
					*/
					end
		from AUSENCIA
		where CB_CODIGO = @Empleado
		and AU_FECHA = @Fecha

	RETURN  @Resultado
END
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 179*/
CREATE  VIEW V_TRANSFER(
CB_CODIGO,
AU_FECHA,
CC_ORIGEN,
CC_DESTINO,
TR_HORAS,
TR_TIPO,
TR_MOTIVO,
TR_NUMERO,
TR_TEXTO,
US_CODIGO,
TR_FECHA,
TR_GLOBAL,
TR_APRUEBA,
TR_FEC_APR,
TR_STATUS,
TR_TXT_APR,
LLAVE
)
as
select
T.CB_CODIGO,
T.AU_FECHA Fecha,
dbo.SP_AUSENCIA_COSTEO(T.CB_CODIGO, T.AU_FECHA),
T.CC_CODIGO,
T.TR_HORAS,
T.TR_TIPO,
T.TR_MOTIVO,
T.TR_NUMERO,
T.TR_TEXTO,
T.US_CODIGO,
T.TR_FECHA,
T.TR_GLOBAL,
T.TR_APRUEBA,
T.TR_FEC_APR,
T.TR_STATUS,
T.TR_TXT_APR,
T.LLAVE
from TRANSFER T
left outer join COLABORA on COLABORA.CB_CODIGO = T.CB_CODIGO
left outer join AUSENCIA on AUSENCIA.CB_CODIGO = T.CB_CODIGO
and AUSENCIA.AU_FECHA = T.AU_FECHA
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 180*/
CREATE VIEW CHORAS (
	AU_FECHA,
	CB_CODIGO,
	CC_CODIGO,
	CH_TIPO,
	CH_CLASIFI,
    CH_HORAS )
as
	-- 0 = Horas Ordinarias
	-- Ausencia
	select 	AU_FECHA, CB_CODIGO, dbo.SP_AUSENCIA_COSTEO(CB_CODIGO, AU_FECHA), 0, '', AU_HORAS
	from	AUSENCIA
	where	( AU_HORAS > 0 )
    union
	-- Transferidas
	select	T.AU_FECHA, T.CB_CODIGO, dbo.SP_AUSENCIA_COSTEO(T.CB_CODIGO, T.AU_FECHA), 0, 'Transferidas',
			case when A.AU_HORAS >= TR_HORAS then coalesce(-TR_HORAS,0) else -AU_HORAS end
	from	TRANSFER T
	join	AUSENCIA A on (A.AU_FECHA = T.AU_FECHA) and (A.CB_CODIGO = T.CB_CODIGO)
	where	( T.TR_TIPO = 0 )
	union
	-- Recibidas
	select	T.AU_FECHA, T.CB_CODIGO, T.CC_CODIGO, 0, 'Recibidas',
			case when A.AU_HORAS >= TR_HORAS then coalesce(TR_HORAS,0) else AU_HORAS end
	from	TRANSFER T
	join	AUSENCIA A on (A.AU_FECHA = T.AU_FECHA) and (A.CB_CODIGO = T.CB_CODIGO)
	where	( T.TR_TIPO = 0 )
	union
	-- 1 = Horas Extras
	-- Ausencia
	select 	AU_FECHA, CB_CODIGO, dbo.SP_AUSENCIA_COSTEO(CB_CODIGO, AU_FECHA), 1, '', AU_EXTRAS
	from	AUSENCIA
	where	( AU_EXTRAS > 0 )
    union
	-- Transferidas
	select	T.AU_FECHA, T.CB_CODIGO, dbo.SP_AUSENCIA_COSTEO(T.CB_CODIGO, T.AU_FECHA), 1, 'Transferidas',
			case when A.AU_EXTRAS >= TR_HORAS then coalesce(-TR_HORAS,0) else -AU_EXTRAS end
	from	TRANSFER T
	join	AUSENCIA A on (A.AU_FECHA = T.AU_FECHA) and (A.CB_CODIGO = T.CB_CODIGO)
	where	( T.TR_TIPO = 1 )
	union
	-- Recibidas
	select	T.AU_FECHA, T.CB_CODIGO, T.CC_CODIGO, 1, 'Recibidas',
			case when A.AU_EXTRAS >= TR_HORAS then coalesce(TR_HORAS,0) else AU_EXTRAS end
	from	TRANSFER T
	join	AUSENCIA A on (A.AU_FECHA = T.AU_FECHA) and (A.CB_CODIGO = T.CB_CODIGO)
	where	( T.TR_TIPO = 1 )
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 181*/
drop function SP_AUSENCIA_COSTEO
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 182*/
CREATE VIEW dbo.COSTOS_POL AS
	SELECT
			PE_YEAR,
			PE_TIPO,
			PE_NUMERO,
			CB_CODIGO,
			CT.CO_NUMERO,
			COALESCE( CO_DESCRIP, '' ) CO_DESCRIP,
			CT.MO_REFEREN,
			CT.GpoCostoID GC_ID,
			GpoCostoCodigo GC_CODIGO,
			GpoCostoNombre GC_NOMBRE,
			CT.CC_CODIGO,
			COALESCE( CC_NOMBRE, '' ) CC_NOMBRE,
			CostoClasifi CT_CLASIFI,
			CostoMonto * (case T_Costos_SubCta.Prorrat when 0 then 1 else Coalesce( T_Costos_SubCta.Prorrat, 1) end ) CT_MONTO,
			CostoMonto CT_MONTO_OR,
			T_Costos_SubCta.CuentaContable CT_SUB_CTA,
			T_Costos_SubCta.Tipo CT_CAR_ABO,
			case T_Costos_SubCta.Prorrat when 0 then 1 else Coalesce( T_Costos_SubCta.Prorrat, 1) end CT_PRORRAT,
			T_Costos_SubCta.Comentario CT_COMENTA,
			T_Costos_SubCta.Numero CT_NUMERO
	FROM T_Costos_SubCta
				left outer join T_Costos CT on CT.CostoID = T_Costos_SubCta.CostoID
				JOIN NOMINA ON CT.NominaID = NOMINA.LLAVE
				JOIN T_GruposCosto ON CT.GpoCostoID = T_GruposCosto.GpoCostoID
				LEFT OUTER JOIN CONCEPTO ON CT.CO_NUMERO = CONCEPTO.CO_NUMERO
				LEFT OUTER JOIN CCOSTO ON CT.CC_CODIGO = CCOSTO.CC_CODIGO
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 183*/
CREATE view TOT_COSTOS_POLIZA
as
SELECT PE_YEAR, PE_TIPO, PE_NUMERO, COSTOS_POL.CT_SUB_CTA,
	cast ( ( sum(CT_MONTO)) as NUMERIC(15,2)) CP_MONTO, COSTOS_POL.CT_CAR_ABO
FROM COSTOS_POL
group by PE_YEAR, PE_TIPO, PE_NUMERO, COSTOS_POL.CT_SUB_CTA, CT_CAR_ABO
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 184*/
create view COST_CRITE
as
     SELECT CritCostoID AS CC_CODIGO, CritCostoNombre AS CC_DESCRIP, CritCostoActivo AS CC_ACTIVO
     FROM T_CriteriosCosto
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 185*/
CREATE VIEW COST_GRUPO
AS
     SELECT GpoCostoID AS GC_LLAVE, GpoCostoCodigo AS GC_CODIGO, GpoCostoNombre AS GC_DESCRIP,GpoCostoActivo AS GC_ACTIVO
     FROM T_GruposCosto
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 186*/
CREATE VIEW COST_CON
AS
     SELECT ConcepCostoID AS CR_LLAVE, CO_NUMERO, GpoCostoID AS GC_LLAVE, CritCostoID AS CC_CODIGO
     FROM T_ConceptosCosto
GO

/* Tabla PARIENTE: Cambiar dominio de campo PA_NOMBRES ( Sug. 2499 Gastos Medicos ) */
/* Patch 430 # Seq: 187 Agregado */
IF EXISTS (SELECT * FROM SYSOBJECTS WHERE name = 'TU_PARIENTE')
   DISABLE TRIGGER TU_PARIENTE ON PARIENTE;
go   
update PARIENTE set PA_NOMBRE = '' WHERE PA_NOMBRE is null
GO
/* Tabla PARIENTE: Cambiar dominio de campo PA_NOMBRES ( Sug. 2499 Gastos Medicos ) */
/* Patch 430 # Seq: 188 Agregado */
alter table PARIENTE alter column PA_NOMBRE Titulo
GO
IF EXISTS (SELECT * FROM SYSOBJECTS WHERE name = 'TU_PARIENTE')
   DISABLE TRIGGER TU_PARIENTE ON PARIENTE;
go  

/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$   PROCEDURES DATOS      $$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/

/* 2403  Mejoras en Admin. Vacaciones */
/*Patch #430  Seq: 362*/
create procedure [dbo].[SP_WFGetPeriodoActualDatosNoAfectados](@YEAR Anio, @TPO_NOMINA NominaTipo )
as
begin
	DECLARE
	@MAX_PERIODO INT,
	@STATUS INT
	SET @STATUS = 0;

	SELECT @MAX_PERIODO= (SELECT ISNULL(MAX(PE_NUMERO),1)       
	FROM PERIODO 
	WHERE ( PE_YEAR = @YEAR ) 
		AND ( PE_TIPO = @TPO_NOMINA )
		AND ( PE_NUMERO < 200 )
		AND ( PE_STATUS <> @STATUS )
		AND ( PE_STATUS < 5 ) )

	SELECT PE_YEAR, PE_TIPO, PE_NUMERO, PE_DESCRIP, PE_USO, PE_STATUS, PE_INC_BAJ, 
		PE_SOLO_EX, PE_FEC_INI, PE_FEC_FIN, PE_FEC_PAG, PE_MES, PE_DIAS, PE_DIAS_AC, 
		PE_DIA_MES, PE_POS_MES, PE_PER_MES, PE_PER_TOT, PE_FEC_MOD, PE_AHORRO, PE_PRESTAM, 
		PE_ASI_INI, PE_ASI_FIN, PE_NUM_EMP, PE_TOT_PER, PE_TOT_NET, PE_TOT_DED, PE_LOG, 
		US_CODIGO, (PE_MES - 1) AS PE_MES_A ,
		PE_PERIODO_TEXTO = 'De ' + (case 
								when datepart(dd,PE_FEC_INI)< 10 then '0' + convert(varchar(2), datepart(dd,PE_FEC_INI) )
								else convert(varchar(2), datepart(dd,PE_FEC_INI))
								end) + '/'
							 + (case 
								when datepart(mm,PE_FEC_INI)< 10 then '0' + convert(varchar(2), datepart(mm,PE_FEC_INI) )
								else convert(varchar(2), datepart(mm,PE_FEC_INI))
								end)+ '/'
							 +            convert(varchar(4), datepart(yyyy,PE_FEC_INI)) +
								' a ' + (case 
								when datepart(dd,PE_FEC_FIN)< 10 then '0' + convert(varchar(2), datepart(dd,PE_FEC_FIN) )
								else convert(varchar(2), datepart(dd,PE_FEC_FIN))
								end) + '/'
							 + (case
								when datepart(mm,PE_FEC_FIN)< 10 then '0' + convert(varchar(2), datepart(mm,PE_FEC_FIN) )
								else convert(varchar(2), datepart(mm,PE_FEC_FIN))
								end)+ '/'
							 +            convert(varchar(4), datepart(yyyy,PE_FEC_FIN))
	FROM PERIODO 
	WHERE ( PE_YEAR = @YEAR ) 
		AND ( PE_TIPO = @TPO_NOMINA )
		AND ( PE_NUMERO =  @MAX_PERIODO ) 
		AND ( PE_STATUS <> @STATUS )
		AND ( PE_STATUS < 5 )
end

go

/* 2403  Mejoras en Admin. Vacaciones */
/*Patch #430  Seq: 363 */
create procedure [dbo].[SP_WFGetPeriodoDatosNoAfectados](@YEAR Anio, @TPO_NOMINA NominaTipo, @NUM_NOMINA NominaNumero)
as
begin
	SELECT PE_YEAR, PE_TIPO, PE_NUMERO, PE_DESCRIP, PE_USO, PE_STATUS, PE_INC_BAJ, 
		PE_SOLO_EX, PE_FEC_INI, PE_FEC_FIN, PE_FEC_PAG, PE_MES, PE_DIAS, PE_DIAS_AC, 
		PE_DIA_MES, PE_POS_MES, PE_PER_MES, PE_PER_TOT, PE_FEC_MOD, PE_AHORRO, PE_PRESTAM, 
		PE_ASI_INI, PE_ASI_FIN, PE_NUM_EMP, PE_TOT_PER, PE_TOT_NET, PE_TOT_DED, PE_LOG, 
		US_CODIGO, (PE_MES - 1) AS PE_MES_A ,
		PE_PERIODO_TEXTO = 'De ' + (case
								when datepart(dd,PE_FEC_INI)< 10 then '0' + convert(varchar(2), datepart(dd,PE_FEC_INI) )
								else convert(varchar(2), datepart(dd,PE_FEC_INI))
								end) + '/'
							  + (case 
								when datepart(mm,PE_FEC_INI)< 10 then '0' + convert(varchar(2), datepart(mm,PE_FEC_INI) )
								else convert(varchar(2), datepart(mm,PE_FEC_INI))
								end)+ '/'
							  +	convert(varchar(4), datepart(yyyy,PE_FEC_INI)) +
						' a ' + (case 
								when datepart(dd,PE_FEC_FIN)< 10 then '0' + convert(varchar(2), datepart(dd,PE_FEC_FIN) )
								else convert(varchar(2), datepart(dd,PE_FEC_FIN))
								end) + '/'
							  + (case
								when datepart(mm,PE_FEC_FIN)< 10 then '0' + convert(varchar(2), datepart(mm,PE_FEC_FIN) )
								else convert(varchar(2), datepart(mm,PE_FEC_FIN))
								end)+ '/'
							  +	convert(varchar(4), datepart(yyyy,PE_FEC_FIN))
	FROM PERIODO
	WHERE ( PE_YEAR = @YEAR )
		AND ( PE_TIPO = @TPO_NOMINA )
		AND ( PE_NUMERO = @NUM_NOMINA )
		AND ( PE_STATUS < 5 )
end
go

/* 2456  Al Afectar Nomina y Tener Global de Bloqueo Prenominas Autorizadas , no se puede afectar */
/*Patch #430  Seq:  158 Proc. Modificado */
ALTER PROCEDURE AFECTA_FALTAS
    				@ANIO SMALLINT,
    				@TIPO SMALLINT,
    				@NUMERO SMALLINT,
    				@FACTOR INTEGER
AS
        SET NOCOUNT ON
	declare @FechaIni   DateTime;
  	declare @Incidencia Char( 6 );
  	declare @TipoDia    SmallInt;
  	declare @Empleado   Integer;
  	declare @FEC_INI    DateTime;
  	declare @MOTIVO     SmallInt;
  	declare @DIAS       Numeric(15,2);
	Declare @Bloqueo    VARCHAR(1);
		
	select @Bloqueo = GL_FORMULA from GLOBAL where GL_CODIGO = 289;
	
	if( @Bloqueo = 'S' )
	begin
		 Update GLOBAL set GL_FORMULA = 'N' where GL_CODIGO = 289;
	end
  	
	select @FechaIni = P.PE_ASI_INI  /* Fecha de inicio de asistencia */
	from PERIODO P
	where
         ( P.PE_YEAR = @Anio ) and
         ( P.PE_TIPO = @Tipo ) and
         ( P.PE_NUMERO = @Numero )
	
	DECLARE TemporalFaltas CURSOR FOR 
  		select F.CB_CODIGO, F.FA_FEC_INI, F.FA_MOTIVO, F.FA_DIAS
  		from   FALTAS F
       	left outer join NOMINA N on 
         	( N.PE_YEAR = @Anio ) 
		and ( N.PE_TIPO = @Tipo ) 
		and ( N.PE_NUMERO = @Numero ) 
		and ( N.CB_CODIGO = F.CB_CODIGO )
  		where ( F.PE_YEAR = @Anio ) 
		and ( F.PE_TIPO = @Tipo ) 
		and ( F.PE_NUMERO = @Numero )
		and ( F.FA_DIA_HOR = 'D' ) 
		and ( F.FA_MOTIVO  IN (0,1,2,4,12))

   	OPEN TemporalFaltas
   	FETCH NEXT FROM TemporalFaltas
   	into @Empleado, @FEC_INI, @MOTIVO, @DIAS
    WHILE @@FETCH_STATUS = 0
   	BEGIN
        if ( @FEC_INI <= '12/30/1899' ) 
		begin
			select @FEC_INI = @FechaIni;
		end
     	if ( @MOTIVO = 0 ) 
     	begin
       		Select @TipoDia = 0, @Incidencia = 'FI'
     	end
     	else if ( @MOTIVO = 1 )
     	begin
       		Select @TipoDia = 5, @Incidencia = 'FJ'
     	end
     	else if ( @MOTIVO = 2 )  
     	begin
       		Select @TipoDia = 4, @Incidencia = 'FJ'
     	end
     	else if ( @MOTIVO = 4 ) 
     	begin
       		Select @TipoDia = 6, @Incidencia = 'FJ'
     	end
     	else
     	begin
       		Select @TipoDia = 0, @Incidencia = '';
     	end
     
    
     	while ( @DIAS > 0 ) 
     	begin
       		if ( @MOTIVO <> 12 ) 
         		execute AFECTA_1_FALTA @Empleado, @FEC_INI, @TipoDia, @Incidencia, @Factor 
       		else if ( @FEC_INI < @FechaIni )
         		execute AFECTA_1_AJUSTE @Empleado, @FEC_INI, @TipoDia, @Incidencia, @Factor 
       
       		select @DIAS = @DIAS - 1, @FEC_INI = @FEC_INI + 1
     	end
   		
		FETCH NEXT FROM TemporalFaltas
   		into @Empleado, @FEC_INI, @MOTIVO, @DIAS 
  	end

   CLOSE TemporalFaltas
   DEALLOCATE TemporalFaltas
   if (@Bloqueo = 'S')
   begin
		Update GLOBAL set GL_FORMULA = 'S' where GL_CODIGO = 289;
   end
GO

/* 2403  Mejoras en Admin. Vacaciones*/
/*Patch #430  Seq: */
CREATE FUNCTION GetDerechoGozo( @Empleado NumeroEmpleado , @Anio Dias ,@FechaFinAniv Fecha,@UltimaTabla Codigo)
RETURNS @Tabla	TABLE (
	Gozo	    Numeric(15,2),
	Pago	    Numeric(15,2),
    Prima		Numeric(15,2) )
AS
BEGIN
	 declare @FechaCierre Fecha;
	 declare @GozoCierre DiasFrac;
	 declare @PagoCierre DiasFrac;
	 declare @PrimaCierre DiasFrac;
	 declare @DGozo DiasFrac;
	 declare @DPago DiasFrac;
	 declare @DPrima DiasFrac;
	 declare @DGozoPend DiasFrac;
	 declare @DPagoPend DiasFrac;
	 declare @DPrimaPend DiasFrac;
	 declare @Antiguedad Fecha;
	 declare @DiasTabla Dias;
	 declare @DiasTablaPrima Dias;

 
	 set @DGozo = 0;
	 set @DPago = 0;
	 set @DPrima = 0;

	 set @DGozoPend = 0;
	 set @DPagoPend = 0;
	 set @DPrimaPend = 0;

     select @Antiguedad = CB_FEC_ANT from COLABORA where CB_CODIGO = @Empleado;
     
	 
	 if ( Exists(select VA_FEC_INI from VACACION where CB_cODIGO = @Empleado and va_tipo = 0 and VA_YEAR = @Anio and VA_FEC_INI >= @Antiguedad  ) )
	 begin
          
		  SELECT @DGozo = Coalesce(Sum(VA_D_GOZO),0),@DPago = Coalesce(Sum(VA_D_PAGO),0),@DPrima = Coalesce(Sum(VA_D_PRIMA),0) from VACACION where CB_cODIGO = @Empleado and va_tipo = 0 and VA_YEAR = @Anio and VA_FEC_INI >= @Antiguedad;	
		  select top 1 @FechaCierre = VA_FEC_INI,@GozoCierre = VA_D_GOZO, @PagoCierre = VA_D_PAGO,@PrimaCierre = VA_D_PRIMA from VACACION where CB_CODIGO = @Empleado and VA_TIPO = 0 and VA_YEAR = @Anio and VA_FEC_INI >= @Antiguedad order by VA_FEC_INI desc;  
		  if (@FechaCierre != @FechaFinAniv)
		  begin
		   	   if ((( DateDiff ( Day,@FechaCierre,@FechaFinAniv ))/365.00 ) < 1 )
			   begin
			    	set @FechaCierre = DateAdd(Day,1,@FechaCierre);
			   end
			   select Top 1 @DiasTablaPrima = (( PT_PRIMAVA / 100 )* PT_DIAS_VA),@DiasTabla = PT_DIAS_VA  from PRESTACI where TB_CODIGO = @UltimaTabla and PT_YEAR >= @Anio  order by PT_YEAR ASC

			   set @DGozoPend = ( ( DateDiff ( Day,@FechaCierre,@FechaFinAniv ) / 365.00 ) * @DiasTabla); 	
			   set @DGozo = @GozoCierre + @DGozoPend;	 
	
			   set @DPagoPend = @DGozoPend; 	
			   set @DPago = @PagoCierre + @DPagoPend;	 
	
			   set @DPrimaPend = ( ( DateDiff ( Day,@FechaCierre,@FechaFinAniv ) / 365.00 ) * @DiasTablaPrima);
			   set @DPrima = @PrimaCierre + @DPrimaPend;
		  end
	 end
     else	
     begin	 
		  select top 1 @DPrima =  ( PT_PRIMAVA / 100 )* PT_DIAS_VA,@DGozo = PT_DIAS_VA,@DPago = PT_DIAS_VA from PRESTACI where TB_CODIGO = @UltimaTabla and PT_YEAR >= @Anio order by PT_YEAR ASC;
	     
     end;		   
	 insert into @Tabla (Gozo,Pago,Prima)
	 values (@DGozo,@DPago,@DPrima);
		
	 RETURN
END
GO

/*2403  Mejoras en Admin. Vacaciones */
/*Patch #430  Seq: 365*/
CREATE FUNCTION Get_Proporcional( @FechaCierre Fecha,@Antig Fecha,@Anio Dias,@Empleado NumeroEmpleado,@UltimaTabla Codigo)
RETURNS @Tabla	TABLE (
	Gozo	Numeric(15,2),
	Pago	Numeric(15,2),
	Prima	Numeric(15,2)
   )
as
begin
	 declare @DGozo DiasFrac;
	 declare @DPago DiasFrac;
	 declare @DPrima DiasFrac;
	 declare @DCierre Dias;
	 declare @FechaFinAniv Fecha;
	 declare @DiasTb Dias;
	 declare @DiasTbPrima DiasFrac;
	 declare @DiasDif Dias;
	 
     set @FechaFinAniv = DateAdd(Year,@Anio,@Antig);
     
	 if ( ( ( Month(@FechaCierre) = Month(@Antig) ) and ( Day(@FechaCierre) = Day(@Antig) ) ) or ( @FechaCierre is null ) )
     begin
		  select Top 1 @DPago = PT_DIAS_VA ,@DGozo = PT_DIAS_VA, @DPrima = ( ( PT_PRIMAVA / 100 )* PT_DIAS_VA )
					from PRESTACI where TB_CODIGO = @UltimaTabla and PT_YEAR >= @Anio order by PT_YEAR ASC	
	 end
	 else
	 begin
		  select Top 1 @DiasTbPrima = (( PT_PRIMAVA / 100 )* PT_DIAS_VA),@DiasTb = PT_DIAS_VA  
					from PRESTACI where TB_CODIGO = @UltimaTabla and PT_YEAR >= @Anio  order by PT_YEAR ASC 
    
		  set @DiasDif = DateDiff(Day,DateAdd(Day,1,@FechaCierre),@FechaFinAniv);
		  set @DGozo = ( @DiasDif / 365.00 )*@DiasTb;
		  set @DPago = @DGozo;
		  
		  set @DPrima = ( @DiasDif / 365.00 )*@DiasTbPrima;

	 end 
	 insert into @Tabla
	 ( Gozo,Pago,Prima)values( @DGozo,@DPago,@DPrima);
	return	
end
GO

/* 2403  Mejoras en Admin. Vacaciones */
/*Patch #430  Seq: 366*/
CREATE FUNCTION YearsCompletos( @FechaInicial Fecha,@FechaFinal Fecha )
RETURNS  integer
as
begin
declare @Anios integer;
	 if ( Month(@FechaInicial) = Month(@FechaFinal) and Day(@FechaInicial) = Day(@FechaFinal) )
	 begin
		  Set @Anios = DateDiff(Year,@FechaInicial,@FechaFinal)
	 end
	 else
	 begin
		  Set @Anios  =  (DateDiff( day, @FechaInicial,@FechaFinal ) + 1 )/365.25;
	 end
	 return @Anios;
end
GO

/* 2403  Mejoras en Admin. Vacaciones */
/*Patch #430  Seq: 367*/
CREATE FUNCTION GET_SALDOS_VACACION( @Employee int)
RETURNS @Tabla	TABLE (
	VS_ANIV		int,
	CB_CODIGO	int,
	VS_D_GOZO	Numeric(15,2),
	VS_GOZO		Numeric(15,2),
	VS_S_GOZO	Numeric(15,2),
	VS_D_PAGO	Numeric(15,2),
	VS_PAGO		Numeric(15,2),
	VS_S_PAGO	Numeric(15,2),
	VS_D_PRIMA  Numeric(15,2),
	VS_PRIMA	Numeric(15,2),
	VS_S_PRIMA	Numeric(15,2),
	VS_FEC_VEN	DateTime,
	VS_CB_SAL	Numeric(15,2),CIERRE Numeric(15,2) ) 
AS
BEGIN
	declare @Empleado NumeroEmpleado;
	declare @Anios DiasFrac;
	declare @Anio Dias;
	
	declare @DerechoGozo DiasFrac;
	declare @GozadosDist DiasFrac;
	declare @GozadosRest DiasFrac;

    declare @GozoCierre DiasFrac;
	
	declare @DerechoPago DiasFrac;
	declare @PagadosDist DiasFrac;
	declare @PagadosRest DiasFrac;

	declare @PagoCierre DiasFrac;
	
	declare @DerechoPV DiasFrac;
	declare @PVDist DiasFrac;
	declare @PVRest DiasFrac;

	declare @PrimaCierre DiasFrac;
	declare @FechaCierre Fecha;

	declare @Antig Fecha;	
	declare @AniosTotales Fecha;
	
	declare @Hoy Fecha;
	declare @YearCierre Dias;

	declare @DerechoGozoSum DiasFrac;
	declare @DerechoPagoSum DiasFrac;
	declare @DerechoPrimaSum DiasFrac;

    declare @FechaVencimiento Fecha;
    declare @Salario DiasFrac;
    declare @MesesGlobal Dias;
    
    declare @FechaFinAniv Fecha;
	declare @UltimaTabla Codigo;
	declare @FechaSalario Fecha;
    declare @FechaTabla Fecha;
    declare @Activo Booleano;
	declare @FechaBaja Fecha;


    if( @Employee > 0 )
    begin
		 declare EmpleadosVac CURSOR for
		   select CB_CODIGO,CB_FEC_ANT,CB_TABLASS,CB_SALARIO,CB_FEC_SAL,CB_ACTIVO,CB_FEC_BAJ from COLABORA where CB_CODIGO = @Employee 	
    end
    else
	begin
		 declare EmpleadosVac CURSOR for
		   select CB_CODIGO,CB_FEC_ANT,CB_TABLASS,CB_SALARIO,CB_FEC_SAL,CB_ACTIVO,CB_FEC_BAJ from COLABORA 	
    end;

	select @MesesGlobal = GL_FORMULA from GLOBAL where GL_CODIGO = 297;	

	open EmpleadosVac;
     fetch NEXT from EmpleadosVac
     into @Empleado,@Antig,@UltimaTabla,@Salario,@FechaSalario,@Activo,@FechaBaja;
     while ( @@Fetch_Status = 0 )
     begin
		  -- Obtenemos los Gozados 
		  select @GozadosRest = Coalesce(SUM(VA_GOZO),0),@PagadosRest = Coalesce(SUM(VA_PAGO),0),@PVRest = Coalesce(SUM(VA_P_PRIMA),0) 
							from VACACION where CB_CODIGO = @Empleado and VA_TIPO = 1 and VA_FEC_INI >= @Antig;
		  --Empezamos
		  set @Anio = 1; 
		  set @Anios = 0;
		  set @DerechoGozoSum = 0;
		  set @DerechoPagoSum = 0;
		  set @DerechoPrimaSum = 0;
          
		  Select @Hoy = gd FROM vCurrentDateTime;
		  if ( @Activo = 'N' )
		  begin
			   Set @Hoy = Coalesce(@FechaBaja,@Hoy);
		  end	
		  set @AniosTotales = dbo.YearsCompletos(@Antig,@Hoy );	
		  set @FechaCierre = @Antig;
		  
		  if ( ( @Antig is null )or (@Antig = '12/30/1899' ))
		  begin
		 	   select @Antig = CB_FEC_ING FROM COLABORA WHERE CB_CODIGO = @Empleado;  
	      end
			 
		  select Top 1 @FechaTabla = CB_FECHA FROM KARDEX where CB_CODIGO = @Empleado and CB_TIPO = 'PRESTA' order by CB_FECHA desc; 
		  
		  -- Cursor para Cierres 
 	      declare CierresEmp CURSOR for
		    select VA_FEC_INI,VA_D_GOZO,VA_D_PAGO,VA_D_PRIMA,VA_YEAR from VACACION where CB_CODIGO = @Empleado and VA_TIPO = 0 and VA_FEC_INI >= @Antig order by VA_FEC_INI asc
		  
          open CierresEmp;
		  fetch NEXT from CierresEmp
		  into @FechaCierre,@GozoCierre,@PagoCierre,@PrimaCierre,@YearCierre;
		  while ( @@Fetch_Status = 0 )
		  begin 
			   	Set @Anios = dbo.YearsCompletos(@Antig,@FechaCierre );
			    			   -- Se recorre los anios que abarcan el cierre
		       if ( @Anios < @Anio )
		       begin
					set @DerechoGozoSum = @GozoCierre + @DerechoGozoSum;
					set @DerechoPagoSum = @PagoCierre + @DerechoPagoSum;
					set @DerechoPrimaSum = @PrimaCierre + @DerechoPrimaSum;
		       end
		       
		       While ( @Anios >= @Anio )
			   begin
					select @UltimaTabla = CB_TABLASS FROM COLABORA where CB_CODIGO = @Empleado;
					set @FechaFinAniv = DateAdd( Year,@Anio,@Antig );
					if ( @FechaTabla != '12/30/1899' and ( not( @FechaTabla is null ) ) and ( @FechaFinAniv < @FechaTabla ) ) 
					begin
						select @UltimaTabla = dbo.SP_KARDEX_CB_TABLASS(@FechaFinAniv,@Empleado) ;
					end
					--Gozo
					if( @DerechoGozoSum > 0)
					begin
						 set @GozoCierre = @GozoCierre + @DerechoGozoSum;
						 set @DerechoGozoSum = 0;
					end
					--Pago
					if( @DerechoPagoSum > 0)
					begin
						 set @PagoCierre = @PagoCierre + @DerechoPagoSum;
						 set @DerechoPagoSum = 0;
					end
					--Prima
					if( @DerechoPrimaSum > 0)
					begin
						 set @PrimaCierre = @PrimaCierre + @DerechoPrimaSum;
						 set @DerechoPrimaSum = 0;
					end
					
					--select @DerechoGozo = Gozo,@DerechoPago = Pago,@DerechoPV = Prima
					--		from dbo.Get_Proporcional(@FechaCierre,@Antig,@Anio,@Empleado);
					select @DerechoGozo = Gozo,@DerechoPago = Pago,@DerechoPV = Prima 
								from dbo.GetDerechoGozo(@Empleado,@Anio,@FechaFinAniv,@UltimaTabla);
										
					-- Se resta del pendiente del Cierre
					--Gozo
					if ( @DerechoGozo <= @GozoCierre )
					begin
						 if ( @Anios = @Anio )
						 begin
							  set @DerechoGozo = @GozoCierre;
							  set @GozoCierre = 0;
						 end
						 else
						 begin
							  set @GozoCierre = @GozoCierre - @DerechoGozo;
						 end
					end
					else
					begin  
						 set @DerechoGozo = @GozoCierre;
						 set @GozoCierre = 0;
				    end
					--Pago
					if ( @DerechoPago <= @PagoCierre )
					begin
						 if ( @Anios = @Anio )
						 begin
							  set @DerechoPago = @PagoCierre;
							  set @PagoCierre = 0;
						 end
						 else
						 begin
							  set @PagoCierre = @PagoCierre - @DerechoPago;
						 end
					end
					else
					begin
						 set @DerechoPago = @PagoCierre;
						 set @PagoCierre = 0;
				    end
				    --Prima
					if ( @DerechoPV <= @PrimaCierre )
					begin
						 if ( @Anios = @Anio )
						 begin
							  set @DerechoPV = @PrimaCierre;
							  set @PrimaCierre = 0;
						 end
						 else
						 begin
							  set @PrimaCierre = @PrimaCierre - @DerechoPV;
						 end
					end
					else
					begin  
						 set @DerechoPV = @PrimaCierre;
						 set @PrimaCierre = 0;
				    end

					--Gozo
					if ( @GozadosRest > @DerechoGozo )
					begin
						set @GozadosDist = @DerechoGozo;
					    set @GozadosRest = @GozadosRest - @GozadosDist;
					end
					else
					begin
						 set @GozadosDist = @GozadosRest;
						 set @GozadosRest = 0;
					end
					--Pago
					if ( @PagadosRest > @DerechoPago )
					begin
						set @PagadosDist = @DerechoPago;
					    set @PagadosRest = @PagadosRest - @PagadosDist;
					end
					else
					begin
						 set @PagadosDist = @PagadosRest;
						 set @PagadosRest = 0;
					end	
				    --Prima
					if ( @PVRest > @DerechoPV )
					begin
						set @PVDist = @DerechoPV;
					    set @PVRest = @PVRest - @PVDist;
					end
					else
					begin
						 set @PVDist = @PVRest;
						 set @PVRest = 0;
					end	
					-- Calcular Fecha de Vencimiento
					
					if ( ( ( @DerechoGozo - @GozadosDist ) > 0 ) and ( @MesesGlobal > 0 ) )
					begin
						 set @FechaVencimiento = DateAdd( Month,@MesesGlobal,@FechaFinAniv );
						 select @Salario = dbo.SP_KARDEX_CB_SALARIO ( @FechaFinAniv,@Empleado );
					end
					else
					begin
						 set @FechaVencimiento = null;
						 set @Salario = null;
					end

                    --insert tabla
					insert into @Tabla
					( VS_ANIV,
					  CB_CODIGO,
					  VS_D_GOZO,
					  VS_GOZO,
					  VS_S_GOZO,
					  VS_D_PAGO,
					  VS_PAGO,
					  VS_S_PAGO,
					  VS_D_PRIMA,
					  VS_PRIMA,
					  VS_S_PRIMA,
					  VS_FEC_VEN,
					  VS_CB_SAL)values
					(
					  @Anio,
					  @Empleado,
					  @DerechoGozo,
					  @GozadosDist,
					  @DerechoGozo - @GozadosDist,
					  @DerechoPago,
					  @PagadosDist,
					  @DerechoPago - @PagadosDist,
					  @DerechoPV,
					  @PVDist ,
					  @DerechoPV - @PVDist,
					  @FechaVencimiento,
					  @Salario		 
					 );
			 	
					 set @Anio = @Anio + 1;
			   end
			   fetch NEXT from CierresEmp
               into @FechaCierre,@GozoCierre,@PagoCierre,@PrimaCierre,@YearCierre;
		  end		
		  close CierresEmp;
	      deallocate CierresEmp;
		  
		  if ( @Anios != @AniosTotales )
		  begin
			   While (@AniosTotales >= @Anio)
			   begin
					set @FechaFinAniv = DateAdd( Year,@Anio,@Antig );
					select @UltimaTabla = CB_TABLASS FROM COLABORA where CB_CODIGO = @Empleado; 
					if ( @FechaTabla != '12/30/1899' and ( not( @FechaTabla is null ) ) and ( @FechaFinAniv < @FechaTabla ) )
					begin
						select @UltimaTabla = dbo.SP_KARDEX_CB_TABLASS(@FechaFinAniv,@Empleado);
					end
					
					select @DerechoGozo = Gozo,@DerechoPago = Pago,@DerechoPV = Prima
							from dbo.Get_Proporcional(@FechaCierre,@Antig,@Anio,@Empleado,@UltimaTabla);

					--Reiniciamos la fecha de Cierre para el siguiente aniversario
					set @FechaCierre = @FechaFinAniv;

					--Sumamos el Pendiente mas el proporcional calculado 
					--Gozo
					if( @DerechoGozoSum > 0)
					begin
						 set @DerechoGozo = @DerechoGozo + @DerechoGozoSum;
						 set @DerechoGozoSum = 0;
					end
					--Pago
					if( @DerechoPagoSum > 0)
					begin
						 set @DerechoPago = @DerechoPago + @DerechoPagoSum;
						 set @DerechoPagoSum = 0;
					end
					--Prima
					if( @DerechoPrimaSum > 0)
					begin
						 set @DerechoPV = @DerechoPV + @DerechoPrimaSum;
						 set @DerechoPrimaSum = 0;
					end
					

					--Repartir los dias con lo restante del saldo
					--Gozo
					if ( @GozadosRest > @DerechoGozo )
					begin
						set @GozadosDist = @DerechoGozo;
					    set @GozadosRest = @GozadosRest - @GozadosDist;
					end
					else
					begin
						 set @GozadosDist = @GozadosRest;
						 set @GozadosRest = 0;
					end	
					--Pago
					if ( @PagadosRest > @DerechoPago )
					begin
						set @PagadosDist = @DerechoPago;
					    set @PagadosRest = @PagadosRest - @PagadosDist;
					end
					else
					begin
						 set @PagadosDist = @PagadosRest;
						 set @PagadosRest = 0;
					end	
				    --Prima
					if ( @PVRest > @DerechoPV )
					begin
						set @PVDist = @DerechoPV;
					    set @PVRest = @PVRest - @PVDist;
					end
					else
					begin
						 set @PVDist = @PVRest;
						 set @PVRest = 0;
					end	
					
					if ( ( ( @DerechoGozo - @GozadosDist ) > 0 ) and ( @MesesGlobal > 0 ) )
					begin
						 set @FechaVencimiento = DateAdd( Month,@MesesGlobal,@FechaFinAniv );
						 select @Salario = dbo.SP_KARDEX_CB_SALARIO ( @FechaFinAniv,@Empleado );
					end
					else
					begin
						 set @FechaVencimiento = null;
						 set @Salario = null;
					end
					
					
					--Agregar el registro al resultado	
					insert into @Tabla 
					( VS_ANIV,
					  CB_CODIGO,
					  VS_D_GOZO,
					  VS_GOZO,
					  VS_S_GOZO,
					  VS_D_PAGO,
					  VS_PAGO,
					  VS_S_PAGO,
					  VS_D_PRIMA,
					  VS_PRIMA,
					  VS_S_PRIMA,
					  VS_FEC_VEN,
					  VS_CB_SAL)values
					(
					  @Anio,
					  @Empleado,
					  @DerechoGozo,
					  @GozadosDist,
					  @DerechoGozo - @GozadosDist,
					  @DerechoPago,
					  @PagadosDist,
					  @DerechoPago - @PagadosDist,
					  @DerechoPV,
					  @PVDist ,
					  @DerechoPV - @PVDist,
					  @FechaVencimiento,
					  @Salario 
					 );

					 set @Anio = @Anio + 1;
			   end
		  end

		  fetch NEXT from EmpleadosVac
          into @Empleado,@Antig,@UltimaTabla,@Salario,@FechaSalario,@Activo,@FechaBaja;
     end;
	 close EmpleadosVac;
	 deallocate EmpleadosVac;

	return
END
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 368*/
CREATE FUNCTION SP_GET_NIVEL_COSTEO() RETURNS FolioChico
AS
BEGIN
	DECLARE @GLOBAL_Nivel_de_Costeo FolioChico
	DECLARE @ResGlobal Formula
	DECLARE @Resultado FolioChico

	SET @GLOBAL_Nivel_de_Costeo = 294

	SELECT @ResGlobal = RTrim(LTrim(GL_FORMULA))  FROM GLOBAL WHERE  GL_CODIGO = @GLOBAL_Nivel_de_Costeo

	/* Es necesario ajustarlo para ACS*/
	if  @ResGlobal in ( '1','2','3','4','5','6','7','8','9' )
		select @Resultado = convert( smallint,  @ResGlobal )
	else
		select @Resultado =0;

	RETURN @Resultado
END
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 369 */
CREATE FUNCTION SP_AUSENCIA_COSTEO( @Empleado NumeroEmpleado, @Fecha Fecha)
RETURNS Codigo
AS
BEGIN
	declare @NivelCosteo FolioChico
	declare @Resultado Codigo

  select   @NivelCosteo = dbo.SP_GET_NIVEL_COSTEO();

	if @NivelCosteo = 0
		select @Resultado = ''
	else
		select @Resultado =
					CASE  @NivelCosteo
						WHEN 1 THEN CB_NIVEL1
						WHEN 2 THEN CB_NIVEL2
						WHEN 3 THEN CB_NIVEL3
						WHEN 4 THEN CB_NIVEL4
						WHEN 5 THEN CB_NIVEL5
						WHEN 6 THEN CB_NIVEL6
						WHEN 7 THEN CB_NIVEL7
						WHEN 8 THEN CB_NIVEL8
						WHEN 9 THEN CB_NIVEL9
					/* ACS
						WHEN 10 THEN CB_NIVEL10
						WHEN 11  THEN CB_NIVEL11
						WHEN 12 THEN CB_NIVEL12
					*/
					end
		from AUSENCIA
		where CB_CODIGO = @Empleado
		and AU_FECHA = @Fecha

	RETURN  @Resultado
END
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 370*/
CREATE FUNCTION SP_NOMINA_COSTEO( @Empleado NumeroEmpleado,
								  @Year Anio,
								  @Tipo NominaTipo,
								  @Numero NominaNumero )
RETURNS Codigo
AS
BEGIN
	declare @NivelCosteo FolioChico
	declare @Resultado Codigo

  select   @NivelCosteo = dbo.SP_GET_NIVEL_COSTEO();

	if @NivelCosteo = 0
		select @Resultado = ''
	else
		select @Resultado =
					CASE  @NivelCosteo
						WHEN 1 THEN CB_NIVEL1
						WHEN 2 THEN CB_NIVEL2
						WHEN 3 THEN CB_NIVEL3
						WHEN 4 THEN CB_NIVEL4
						WHEN 5 THEN CB_NIVEL5
						WHEN 6 THEN CB_NIVEL6
						WHEN 7 THEN CB_NIVEL7
						WHEN 8 THEN CB_NIVEL8
						WHEN 9 THEN CB_NIVEL9
					/* ACS
						WHEN 10 THEN CB_NIVEL10
						WHEN 11  THEN CB_NIVEL11
						WHEN 12 THEN CB_NIVEL12
					*/
					end
		from NOMINA
		where CB_CODIGO = @Empleado
		and PE_YEAR = @Year
		and PE_TIPO = @Tipo
		and PE_NUMERO = @Numero
	RETURN  @Resultado
END
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 371*/
CREATE PROCEDURE Costos_CalcProporciones_Hook(
	@NominaID	FolioGrande )
AS
BEGIN
	-- 0 = Centro de Costos Default
	INSERT INTO T_ProporcionCostos ( NominaID, CritCostoID, CC_CODIGO, PropCostoBase )
	SELECT	@NominaID, 0,
			dbo.SP_NOMINA_COSTEO( NOMINA.CB_CODIGO, NOMINA.PE_YEAR, NOMINA.PE_TIPO, NOMINA.PE_NUMERO ),
			1
	FROM	NOMINA
	WHERE	NOMINA.LLAVE = @NominaID

	-- 1 = Horas Ordinarias
	INSERT INTO T_ProporcionCostos ( NominaID, CritCostoID, CC_CODIGO, PropCostoBase )
	SELECT	@NominaID, 1, dbo.SP_AUSENCIA_COSTEO(AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA), SUM( AUSENCIA.AU_HORAS )
	FROM	NOMINA
				JOIN AUSENCIA ON NOMINA.CB_CODIGO = AUSENCIA.CB_CODIGO AND AUSENCIA.AU_FECHA BETWEEN NOMINA.NO_ASI_INI AND NOMINA.NO_ASI_FIN
	WHERE	NOMINA.LLAVE = @NominaID AND AUSENCIA.AU_HORAS > 0
	GROUP BY dbo.SP_AUSENCIA_COSTEO(AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA)

	-- 1.1 Horas Ordinarias Transferidas
	INSERT INTO T_ProporcionCostos ( NominaID, CritCostoID, CC_CODIGO, PropCostoClasifi, PropCostoBase )
	SELECT	@NominaID, 1, dbo.SP_AUSENCIA_COSTEO(AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA), 'Transferidas', SUM( -TR_HORAS )
	FROM	NOMINA
				JOIN AUSENCIA ON NOMINA.CB_CODIGO = AUSENCIA.CB_CODIGO AND AUSENCIA.AU_FECHA BETWEEN NOMINA.NO_ASI_INI AND NOMINA.NO_ASI_FIN
				JOIN TRANSFER ON AUSENCIA.CB_CODIGO = TRANSFER.CB_CODIGO AND AUSENCIA.AU_FECHA = TRANSFER.AU_FECHA AND TR_TIPO = 0 AND TR_STATUS = 1
	WHERE	NOMINA.LLAVE = @NominaID
	GROUP BY dbo.SP_AUSENCIA_COSTEO(AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA)

	-- 1.2 Horas Ordinarias Recibidas para el otro centro de Costo
	IF ( @@ROWCOUNT > 0 )
		INSERT INTO T_ProporcionCostos ( NominaID, CritCostoID, CC_CODIGO, PropCostoClasifi, PropCostoBase )
		SELECT	@NominaID, 1, TRANSFER.CC_CODIGO, 'Recibidas', SUM( TR_HORAS )
		FROM	NOMINA
					JOIN AUSENCIA ON NOMINA.CB_CODIGO = AUSENCIA.CB_CODIGO AND AUSENCIA.AU_FECHA BETWEEN NOMINA.NO_ASI_INI AND NOMINA.NO_ASI_FIN
					JOIN TRANSFER ON AUSENCIA.CB_CODIGO = TRANSFER.CB_CODIGO AND AUSENCIA.AU_FECHA = TRANSFER.AU_FECHA AND TR_TIPO = 0 AND TR_STATUS = 1
		WHERE	NOMINA.LLAVE = @NominaID
		GROUP BY TRANSFER.CC_CODIGO
	
	-- 2 = Horas Extras
	INSERT INTO T_ProporcionCostos ( NominaID, CritCostoID, CC_CODIGO, PropCostoBase )
	SELECT	@NominaID, 2, dbo.SP_AUSENCIA_COSTEO(AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA), SUM( AUSENCIA.AU_EXTRAS )
	FROM	NOMINA
				JOIN AUSENCIA ON NOMINA.CB_CODIGO = AUSENCIA.CB_CODIGO AND AUSENCIA.AU_FECHA BETWEEN NOMINA.NO_ASI_INI AND NOMINA.NO_ASI_FIN
	WHERE	NOMINA.LLAVE = @NominaID AND AUSENCIA.AU_EXTRAS > 0
	GROUP BY dbo.SP_AUSENCIA_COSTEO(AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA)

	
	-- 2.1 Horas Extras Transferidas
	INSERT INTO T_ProporcionCostos ( NominaID, CritCostoID, CC_CODIGO, PropCostoClasifi, PropCostoBase )
	SELECT	@NominaID, 2, dbo.SP_AUSENCIA_COSTEO(AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA), 'Transferidas', SUM( -TR_HORAS )
	FROM	NOMINA
				JOIN AUSENCIA ON NOMINA.CB_CODIGO = AUSENCIA.CB_CODIGO AND AUSENCIA.AU_FECHA BETWEEN NOMINA.NO_ASI_INI AND NOMINA.NO_ASI_FIN
				JOIN TRANSFER ON AUSENCIA.CB_CODIGO = TRANSFER.CB_CODIGO AND AUSENCIA.AU_FECHA = TRANSFER.AU_FECHA AND TR_TIPO = 1 AND TR_STATUS = 1
	WHERE	NOMINA.LLAVE = @NominaID

	GROUP BY dbo.SP_AUSENCIA_COSTEO(AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA)

	IF ( @@ROWCOUNT > 0 )

	-- 2.2 Horas Extras Recibidas para el otro centro de Costo

		INSERT INTO T_ProporcionCostos ( NominaID, CritCostoID, CC_CODIGO, PropCostoClasifi, PropCostoBase )
		SELECT	@NominaID, 2, TRANSFER.CC_CODIGO, 'Recibidas', SUM( TR_HORAS )
		FROM	NOMINA
					JOIN AUSENCIA ON NOMINA.CB_CODIGO = AUSENCIA.CB_CODIGO AND AUSENCIA.AU_FECHA BETWEEN NOMINA.NO_ASI_INI AND NOMINA.NO_ASI_FIN
					JOIN TRANSFER ON AUSENCIA.CB_CODIGO = TRANSFER.CB_CODIGO AND AUSENCIA.AU_FECHA = TRANSFER.AU_FECHA AND TR_TIPO = 1 AND TR_STATUS = 1
		WHERE	NOMINA.LLAVE = @NominaID

		GROUP BY TRANSFER.CC_CODIGO

/*
	-- 101 = Especial
	INSERT INTO T_ProporcionCostos ( NominaID, CritCostoID, CC_CODIGO, PropCostoBase ) VALUES ( @NominaID, 101, '1', 10 )
	INSERT INTO T_ProporcionCostos ( NominaID, CritCostoID, CC_CODIGO, PropCostoBase ) VALUES ( @NominaID, 101, '2', 10 )
	INSERT INTO T_ProporcionCostos ( NominaID, CritCostoID, CC_CODIGO, PropCostoBase ) VALUES ( @NominaID, 101, '3',  5 )
	INSERT INTO T_ProporcionCostos ( NominaID, CritCostoID, CC_CODIGO, PropCostoBase ) VALUES ( @NominaID, 101, '3',  5 )
*/
END
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 372 */
CREATE PROCEDURE Costos_CalcProporciones(
	@NominaID	FolioGrande )
AS
BEGIN
	-- Borra proporciones de corrida anterior
	DELETE	T_ProporcionTotales
	WHERE	NominaID = @NominaID

	DELETE	T_ProporcionCostos
	WHERE	NominaID = @NominaID

	-- Distribuci�n a criterio del Cliente
	EXEC Costos_CalcProporciones_Hook @NominaID

	-- Calcula Totales de cada Criterio
	INSERT INTO T_ProporcionTotales ( NominaID, CritCostoID, PropTotalConteo, PropTotalSuma )
	SELECT	@NominaID, CritCostoID, COUNT(*), SUM( PropCostoBase )
	FROM	T_ProporcionCostos
	WHERE	NominaID = @NominaID
	GROUP BY CritCostoID
END
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 373 */
CREATE PROCEDURE Costos_ReparteCriterio(
	@NominaID		FolioGrande,
	@CO_NUMERO		Concepto,
	@MO_REFEREN		Referencia,
	@GpoCostoID		FolioGrande,
	@CritCostoID	FolioGrande,
	@Monto			Pesos )
AS
BEGIN
	DECLARE @Conteo			FolioGrande
	DECLARE @SumaBase		Horas
	DECLARE @SumaMonto		Pesos
	DECLARE @Diferencia		Pesos
	DECLARE @CostoID		FolioGrande


	SELECT	@Conteo		= PropTotalConteo,
			@SumaBase	= PropTotalSuma
	FROM	T_ProporcionTotales
	WHERE	NominaID = @NominaID AND CritCostoID = @CritCostoID

	-- No hay horas base para ese Criterio
	IF ( @Conteo IS NULL OR @Conteo = 0 OR @SumaBase = 0 )
		RETURN 0

	IF ( @Conteo = 1 )	-- Hay un solo Centro de Costo, reparte el 100% del monto a ese registro
	BEGIN
		INSERT INTO T_Costos ( NominaID, CO_NUMERO, MO_REFEREN, GpoCostoID, CC_CODIGO, CostoClasifi, CostoMonto )
		SELECT	 @NominaID, @CO_NUMERO, @MO_REFEREN, @GpoCostoID, CC_CODIGO, PropCostoClasifi, @Monto
		FROM	T_ProporcionCostos
		WHERE	NominaID = @NominaID AND CritCostoID = @CritCostoID
	END
	ELSE	-- Varios centros de Costo, reparte proporcional
	BEGIN
		INSERT INTO T_Costos ( NominaID, CO_NUMERO, MO_REFEREN, GpoCostoID, CC_CODIGO, CostoClasifi, CostoMonto )
		SELECT	@NominaID, @CO_NUMERO, @MO_REFEREN, @GpoCostoID, CC_CODIGO, PropCostoClasifi, ROUND( @Monto * PropCostoBase / @SumaBase , 2 )
		FROM	T_ProporcionCostos
		WHERE	NominaID = @NominaID AND CritCostoID = @CritCostoID

		-- Asegurar que no se pierdan centavos en el redondeo
		-- Suma el monto efectivamente repartido
		-- Obtiene el ID del primer costo para enviar diferencia
		SELECT	@SumaMonto	= SUM( CostoMonto ),
				@CostoID	= MIN( CostoID )
		FROM	T_Costos
		WHERE	NominaID = @NominaID AND CO_NUMERO = @CO_NUMERO AND GpoCostoID = @GpoCostoID

		-- Lo compara contra el monto que ten�a que repartir
		SET @Diferencia = @Monto - @SumaMonto

		-- Si se perdieron se mandan arbitrariamente al primer rengl�n
		IF ( @Diferencia <> 0 )
			UPDATE	T_Costos
			SET		CostoMonto = CostoMonto + @Diferencia
			WHERE	CostoID = @CostoID
	END

	RETURN @Conteo
END
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 374*/
CREATE PROCEDURE Costos_CalculaFijos_Hook(
	@NominaID	FolioGrande )
AS
BEGIN
	SET NOCOUNT ON

	DECLARE	@GpoCostoID		FolioGrande
	DECLARE @CritCostoID	FolioGrande

	SET @CritCostoID	= 102	-- Transferidas
	SET @GpoCostoID		= 7		-- Transferidas Standard

	INSERT INTO T_Costos ( NominaID, CO_NUMERO, MO_REFEREN, GpoCostoID, CC_CODIGO, CostoClasifi, CostoMonto )
	SELECT	@NominaID, 0, '', @GpoCostoID, T_ProporcionCostos.CC_CODIGO, PropCostoClasifi, PropCostoBase * CAST( CC_SUB_CTA AS FLOAT ) / 1000.0
	FROM	T_ProporcionCostos
				JOIN CCOSTO ON T_ProporcionCostos.CC_CODIGO = CCOSTO.CC_CODIGO
	WHERE	NominaID = @NominaID AND CritCostoID = @CritCostoID

/*
	-- Tambi�n se puede directamente sin pasar por un Criterio, sin usar T_ProporcionCostos
	INSERT INTO T_Costos ( NominaID, GpoCostoID, CC_CODIGO, CostoReferen, CostoMonto )
	SELECT	@NominaID, @GpoCostoID, dbo.SP_AUSENCIA_COSTEO(AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA), 'Transferidas', SUM( -TransferHoras*CostoStandard )
	FROM	NOMINA
				JOIN AUSENCIA ON NOMINA.CB_CODIGO = AUSENCIA.CB_CODIGO AND AUSENCIA.AU_FECHA BETWEEN NOMINA.NO_ASI_INI AND NOMINA.NO_ASI_FIN
				JOIN T_Transferidas ON AUSENCIA.LLAVE = T_Transferidas.AusenciaID AND TransferTipo = 1
				JOIN ... Tabla de Costo Standard
	WHERE	NOMINA.LLAVE = @NominaID
	GROUP BY dbo.SP_AUSENCIA_COSTEO(AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA)
*/	

	-- O Cualquier cosa fija. Puede leer COLABORA, NOMINA, etc.
	/*
	INSERT INTO T_Costos ( NominaID, GpoCostoID, CC_CODIGO, CostoReferen, CostoMonto )
	VALUES ( @NominaID, @GpoCostoID, ...
	*/
END
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 375 */
create procedure COSTO_SUBCTA_CARGO_HIJO(
	@CostoID FolioGrande,
	@NominaID FolioGrande,
	@Concepto Concepto,
	@CostoClasifi OrdenTrabajo,
	@CC_codigo Codigo
	)
AS
BEGIN
	/* En este SP, se programan las reglas para crear las cuentas contables*/
	return 0
END
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 376 */
create procedure COSTO_SUBCTA_ABONO_hijo(
	@CostoID FolioGrande,
	@NominaID FolioGrande,
	@Concepto Concepto,
	@CostoClasifi OrdenTrabajo,
	@CC_codigo Codigo
	)
AS
BEGIN
	/* En este SP, se programan las reglas para crear las cuentas contables*/
	return 0
END
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 377*/
CREATE PROCEDURE dbo.Costos_CalculaCuentasContables_Hook( @NominaID	FolioGrande )
AS
BEGIN
	declare @CostoID FolioGrande;
	declare @Concepto Concepto;
	declare @CostoClasifi OrdenTrabajo;
	declare @CC_codigo Codigo;
	declare @Monto Pesos;


	DELETE	T_Costos_SubCta
	WHERE	NominaID = @NominaID

	DECLARE CursorCostos CURSOR LOCAL FOR
		select CostoID, CO_NUMERO, CostoClasifi, CC_CODIGO, CostoMonto
		from T_COSTOS
		where NominaID = @NominaID

	OPEN CursorCostos
	FETCH NEXT FROM CursorCostos
	INTO	@CostoID, @Concepto, @CostoClasifi, @CC_CODIGO, @Monto

	WHILE ( @@FETCH_STATUS = 0 )
	BEGIN
		exec COSTO_SUBCTA_Cargo_HIJO @CostoID, @NominaID, @Concepto, @CostoClasifi, @CC_CODIGO
		exec COSTO_SUBCTA_Abono_HIJO @CostoID, @NominaID, @Concepto, @CostoClasifi, @CC_CODIGO

		FETCH NEXT FROM CursorCostos
		INTO	@CostoID, @Concepto, @CostoClasifi, @CC_CODIGO, @Monto
	END

	CLOSE CursorCostos
	DEALLOCATE CursorCostos
END
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 378*/
CREATE PROCEDURE Costos_CalculaNomina(
	@NominaID	FolioGrande )
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @GpoCostoID		FolioGrande
	DECLARE @CritCostoID	FolioGrande
	DECLARE @Monto			Pesos
	DECLARE @Registros		FolioGrande
	DECLARE @CO_NUMERO		Concepto
	DECLARE @MO_REFEREN		Referencia

	BEGIN TRANSACTION

	-- Borra costos de corrida anterior
	DELETE	T_Costos
	WHERE	NominaID = @NominaID

	-- Calcula proporciones por cada Criterio
	EXEC Costos_CalcProporciones @NominaID
	---------------------------------------

	DECLARE CursorConceptosCosto CURSOR LOCAL FOR
		SELECT	MOVIMIEN.CO_NUMERO, MO_REFEREN, CC.GpoCostoID, CC.CritCostoID, ( MO_PERCEPC+MO_DEDUCCI )
		FROM	NOMINA
					JOIN MOVIMIEN ON NOMINA.PE_YEAR = MOVIMIEN.PE_YEAR
										AND NOMINA.PE_TIPO = MOVIMIEN.PE_TIPO
										AND NOMINA.PE_NUMERO = MOVIMIEN.PE_NUMERO
										AND NOMINA.CB_CODIGO = MOVIMIEN.CB_CODIGO
					JOIN T_ConceptosCosto CC ON MOVIMIEN.CO_NUMERO = CC.CO_NUMERO
					JOIN T_GruposCosto ON CC.GpoCostoID = T_GruposCosto.GpoCostoID
					JOIN T_CriteriosCosto ON CC.CritCostoID = T_CriteriosCosto.CritCostoID
		WHERE	NOMINA.LLAVE = @NominaID
				AND MOVIMIEN.MO_ACTIVO = 'S' AND ( MO_PERCEPC+MO_DEDUCCI ) <> 0
				AND GpoCostoActivo = 'S' AND CritCostoActivo = 'S'

	OPEN CursorConceptosCosto
	FETCH NEXT FROM CursorConceptosCosto
	INTO	@CO_NUMERO, @MO_REFEREN, @GpoCostoID, @CritCostoID, @Monto

	WHILE ( @@FETCH_STATUS = 0 )
	BEGIN
		----------------------------
		EXEC @Registros = Costos_ReparteCriterio @NominaID, @CO_NUMERO, @MO_REFEREN, @GpoCostoID, @CritCostoID, @Monto
		IF ( @Registros = 0 )	-- Si no hay proporci�n de ese criterio, se va por el criterio default
		BEGIN
			EXEC @Registros = Costos_ReparteCriterio @NominaID, @CO_NUMERO, @MO_REFEREN, @GpoCostoID, 0, @Monto
			IF ( @Registros = 0 )
			BEGIN
				RAISERROR( 'Falt� definir la proporci�n del Criterio CERO', 16, 1 )
				BREAK
			END
		END
		----------------------------
		FETCH NEXT FROM CursorConceptosCosto
		INTO	@CO_NUMERO, @MO_REFEREN, @GpoCostoID, @CritCostoID, @Monto
	END

	CLOSE CursorConceptosCosto
	DEALLOCATE CursorConceptosCosto


	--- Por si tiene Costos Fijos o Standard
	EXEC Costos_CalculaFijos_Hook @NominaID
	---------------------------------------

	--- Se ejecuta el SP que agrega las cuentas contables
	EXEC Costos_CalculaCuentasContables_Hook @NominaID
	---------------------------------------

	COMMIT TRANSACTION
END
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 379*/
CREATE PROCEDURE Costos_CalculaPeriodo(
	@PE_YEAR	Anio,
	@PE_TIPO	NominaTipo,
	@PE_NUMERO	NominaNumero )
AS
BEGIN
	DECLARE @NominaID	FolioGrande

	DECLARE CursorNOMINA CURSOR LOCAL FOR
		SELECT		LLAVE
		FROM		NOMINA
		WHERE		PE_YEAR = @PE_YEAR AND PE_TIPO = @PE_TIPO AND PE_NUMERO = @PE_NUMERO
		ORDER BY	CB_CODIGO

	OPEN CursorNOMINA
	FETCH NEXT FROM CursorNOMINA
	INTO	@NominaID

	WHILE ( @@FETCH_STATUS = 0 )
	BEGIN
		------------------------------------
		EXEC Costos_CalculaNomina @NominaID
		------------------------------------
		FETCH NEXT FROM CursorNOMINA
		INTO	@NominaID
	END

	CLOSE CursorNOMINA
	DEALLOCATE CursorNOMINA
END
GO

/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$   TRIGGERS DATOS      $$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/

/* 2465 Error al Renumerar Empleados */
/*Patch #430  Seq: 97 */
ALTER TRIGGER TR_AUSENCIA ON AUSENCIA AFTER UPDATE,INSERT,DELETE AS
BEGIN
	 SET NOCOUNT ON
	 Declare @Aprobado int;
	 Declare @AprobadoU int
	 Declare @Empleado int;
	 Declare @AnioPeriodo int;
	 Declare @TipoPeriodo int;
	 Declare @NumPeriodo int;
	 Declare @EmpleadoU int;
	 Declare @AnioPeriodoU int;	 
	 Declare @TipoPeriodoU int;
	 Declare @NumPeriodoU int;
	 Declare @Bloqueo VARCHAR(1);
		
     if (  Not Update( CB_CODIGO )  )
     begin
		  select @Bloqueo = GL_FORMULA from GLOBAL where GL_CODIGO = 289
		  if (@Bloqueo = 'S')
		  begin		  
			    select @Empleado = CB_CODIGO from Inserted;
				 
				select @NumPeriodo = PE_NUMERO from Inserted;
				select @TipoPeriodo = PE_TIPO from Inserted;
				select @AnioPeriodo = PE_YEAR from Inserted;
				 
				if ( @Empleado is null and @NumPeriodo is null )
				begin
					   select @Empleado = CB_CODIGO from Deleted;
					   select @NumPeriodo = PE_NUMERO from Deleted;
					   select @TipoPeriodo = PE_TIPO from Deleted;
					   select @AnioPeriodo = PE_YEAR from Deleted;	
				end	
				ELSE
				BEGIN
					   select @EmpleadoU = CB_CODIGO from Deleted;
					   select @NumPeriodoU = PE_NUMERO from Deleted;
					   select @TipoPeriodoU = PE_TIPO from Deleted;
					   select @AnioPeriodoU = PE_YEAR from Deleted;
				END

				 if ( (@NumPeriodo > 0 and @TipoPeriodo > 0 and @AnioPeriodo > 0 )OR (@NumPeriodoU > 0 and @TipoPeriodoU > 0 and @AnioPeriodoU > 0) )
				 begin
					  select @Aprobado = NO_SUP_OK  from Nomina N where N.CB_CODIGO = @Empleado and N.PE_YEAR = @AnioPeriodo and N.PE_TIPO = @TipoPeriodo and N.PE_NUMERO = @NumPeriodo
					  select @AprobadoU = NO_SUP_OK  from Nomina N where N.CB_CODIGO = @EmpleadoU and N.PE_YEAR = @AnioPeriodoU and N.PE_TIPO = @TipoPeriodoU and N.PE_NUMERO = @NumPeriodoU
								  
					  if ( @Aprobado > 0 or  @AprobadoU > 0 )
					  begin
							RAISERROR(' Tarjeta de Pre-N�mina Autorizada - No se pueden realizar cambios', 15, 2);
							ROLLBACK TRANSACTION;
					  end
				 end
		  end
	 end
END
GO

/* TR_CHECADAS: Excepcion para 1825 Autorizacion y Bloque de PreNomina*/
/* Patch 430 # Seq:  98*/
ALTER TRIGGER TR_CHECADAS ON CHECADAS AFTER UPDATE,INSERT,DELETE AS
BEGIN
	 SET NOCOUNT ON
	 Declare @Aprobado int;
	 Declare @Empleado int;
     Declare @FechaTarjeta DateTime;
	 Declare @AnioPeriodo int;
     Declare @TipoPeriodo int;
     Declare @NumPeriodo int;
	 Declare @Bloqueo CHAR(1);

	 if (  Not Update( CB_CODIGO )  )
     begin	 
		  select @Bloqueo = GL_FORMULA from GLOBAL where GL_CODIGO = 289	 
		 if ( @Bloqueo = 'S' ) 
		 begin 
			 select @Empleado = CB_CODIGO from Inserted;

			 select @FechaTarjeta = AU_FECHA from Inserted;
			 
			 if ( ( @Empleado is Null ) and ( @FechaTarjeta is null ) )     
			 begin
				  select @Empleado = CB_CODIGO from Deleted;
				  select @FechaTarjeta = AU_FECHA from Deleted;
			 end
			 
			 select @AnioPeriodo = PE_YEAR,@TipoPeriodo = PE_TIPO,@NumPeriodo = PE_NUMERO from AUSENCIA A where A.CB_CODIGO = @Empleado and A.AU_FECHA = @FechaTarjeta;
			 if (@NumPeriodo > 0 and @TipoPeriodo > 0 and @AnioPeriodo > 0 )
			 begin
				   select @Aprobado = NO_SUP_OK  from Nomina N where N.CB_CODIGO = @Empleado and N.PE_YEAR = @AnioPeriodo and N.PE_TIPO = @TipoPeriodo and N.PE_NUMERO = @NumPeriodo 
				   if ( @Aprobado > 0 )
				   begin
						RAISERROR(' Tarjeta de Pre-N�mina Autorizada - No se pueden realizar cambios', 15, 2);
						ROLLBACK TRANSACTION;
				   end
			 end    
		 end
	 end
END
GO

/* TI_KARCURSO: Excepcion para 1825 Autorizacion y Bloque de PreNomina*/
/* Patch 430 # Seq:  95*/
ALTER TRIGGER TI_KARCURSO ON KARCURSO AFTER INSERT AS
BEGIN
SET NOCOUNT ON;
DECLARE  @FechaProgram Fecha;
declare @ReprogDias Integer;
declare @DiasAntig Integer;
declare @ReprogVersion char(1);
declare @cu_revisio char(10);
declare @FechaAntig Fecha;
declare @FecTomada Fecha;
declare @NewCbCodigo Integer;
declare @NewKcFecTom Fecha;
declare @NewCuCodigo Codigo;
declare @NewKcRevisio Codigo;

	 select @NewCbCodigo = CB_CODIGO,@NewKcFecTom = KC_FEC_TOM ,@NewCuCodigo = CU_CODIGO,@NewKcRevisio = KC_REVISIO from Inserted;

     /* Obtener los datos de la matriz de entrenamiento */
	 select @ReprogDias = E.EN_RE_DIAS,@DiasAntig = E.EN_DIAS,@ReprogVersion = E.EN_REPROG,@Cu_Revisio = C.CU_REVISIO FROM ENTRENA E join CURSO C on E.CU_CODIGO = C.CU_CODIGO where E.CU_CODIGO = @NewCuCodigo and E.PU_CODIGO = (select CB_PUESTO from colabora where CB_CODIGO = @NewCbCodigo );

	  /* Obtener la fecha de ingreso del empleado*/
     select @FechaAntig = CB_FEC_PTO from COLABORA where CB_CODIGO = @NewCbCodigo;

	 /* Obtener la fecha del ultimo curso tomado en cuestion */
     Select @FecTomada = max(kc_fec_tom) from KARCURSO where (kc_fec_tom < @NewKcFecTom and CU_CODIGO = @NewCuCodigo and CB_CODIGO = @NewCbCodigo);

	 /* Obtener la ultima fecha de Programacion para el curso */
     Select @FechaProgram = max(KC_FECPROG) from KARCURSO where (kc_fec_tom < @NewKcFecTom and CU_CODIGO = @NewCuCodigo and CB_CODIGO = @NewCbCodigo);
	 /* Si aun no se ha tomado el curso , se toma la fecha de antiguedad mas los dias de la matriz ;
		En caso contrario se investiga cual era la fecha de programaci�n para ese curso */
     if (@FechaProgram is null)
     begin
          set @FechaProgram = DATEADD( DAY, @DiasAntig , @FechaAntig );
     end
     else
     begin
         set @FechaProgram =  ( select case when @FecTomada is null then @FechaAntig
		        when ( @ReprogDias > 0 AND @ReprogVersion = 'N'  )THEN (  DATEADD( DAY, @ReprogDias , @FecTomada ))
				when ( ( @ReprogDias > 0 AND @ReprogVersion = 'S' ) and ( @NewKcRevisio = @cu_revisio ) and (  not (@NewKcRevisio = '')  ) )then ( select max(CH_FECHA) from dbo.CUR_REV where( ( CUR_REV.CU_CODIGO = @NewCuCodigo ) AND ( CUR_REV.CH_REVISIO = @cu_revisio ) ) ) 
				when ( ( @ReprogDias > 0 AND @ReprogVersion = 'S' ) and ((( @NewKcRevisio = @cu_revisio ) and ( @NewKcRevisio = '' ))or ( ( @NewKcRevisio <> @cu_revisio ) ) ) ) then ( DATEADD( DAY, @ReprogDias , @FecTomada ) )              
				when ( @ReprogDias = 0 AND @ReprogVersion = 'N' )then ( DATEADD( DAY, @DiasAntig , @FechaAntig ))			    
				when ( @ReprogDias = 0 AND @ReprogVersion = 'S' and ( not( @NewKcRevisio = '' ) ) and ( (select Count(CH_REVISIO) FROM cur_rev cr where cr.CH_REVISIO = @NewKcRevisio ) > 0 ) )then ( select max(CH_FECHA) from dbo.CUR_REV where( ( CUR_REV.CU_CODIGO = @NewCuCodigo ) AND ( CUR_REV.CH_REVISIO = @NewKcRevisio ) ) )			    				
				else ( DATEADD( DAY, @DiasAntig , @FechaAntig ) )End );
     end

     if (@FechaProgram is null)
     begin
          set @FechaProgram = @FechaAntig;
     end
      /* Se actualiza la fecha de programacion para el curso en cuestion Este ya no se puede reprogramar */ 
	update KARCURSO set KC_FECPROG = @FechaProgram WHERE karcurso.cb_codigo = @NewCbCodigo and karcurso.kc_fec_tom = @NewKcFecTom and karcurso.cu_codigo = @NewCuCodigo;
END
GO
/* TU_KARCURSO: Excepcion para 1825 Autorizacion y Bloque de PreNomina*/
/* Patch 430 # Seq:  96*/
ALTER TRIGGER TU_KARCURSO ON KARCURSO AFTER UPDATE AS
BEGIN
SET NOCOUNT ON;
DECLARE  @FechaProgram Fecha;
declare @ReprogDias Integer;
declare @DiasAntig Integer;
declare @ReprogVersion char(1);
declare @cu_revisio char(10);
declare @FechaAntig Fecha;
declare @FecTomada Fecha;
declare @NewCbCodigo Integer;
declare @NewKcFecTom Fecha;
declare @NewCuCodigo Codigo;
declare @NewKcRevisio Codigo;
declare @FechaRevision Fecha;

	 select @NewCbCodigo = CB_CODIGO,@NewKcFecTom = KC_FEC_TOM ,@NewCuCodigo = CU_CODIGO,@NewKcRevisio = KC_REVISIO from Inserted;
 
     /* Obtener los datos de la matriz de entrenamiento */
	 select @ReprogDias = E.EN_RE_DIAS,@DiasAntig = E.EN_DIAS,@ReprogVersion = E.EN_REPROG,@Cu_Revisio = C.CU_REVISIO FROM ENTRENA E join CURSO C on E.CU_CODIGO = C.CU_CODIGO where E.CU_CODIGO = @NewCuCodigo and E.PU_CODIGO = (select CB_PUESTO from colabora where CB_CODIGO = @NewCbCodigo );
     
	 /* Obtener la fecha de ingreso del empleado*/
     select @FechaAntig = CB_FEC_PTO from colabora where CB_CODIGO = @NewCbCodigo;

	 /* Obtener la fecha del ultimo curso tomado en cuestion */
     Select @FecTomada = max(kc_fec_tom) from KARCURSO where (kc_fec_tom < @NewKcFecTom and CU_CODIGO = @NewCuCodigo and CB_CODIGO = @NewCbCodigo);
	 
	  /* Obtener la ultima fecha de Programacion para el curso */
     Select @FechaProgram = max(KC_FECPROG) from KARCURSO where (kc_fec_tom < @NewKcFecTom and CU_CODIGO = @NewCuCodigo and CB_CODIGO = @NewCbCodigo);
	  /* Si aun no se ha tomado el curso , se toma la fecha de antiguedad mas los dias de la matriz ;
		En caso contrario se investiga cual era la fecha de programaci�n para ese curso */
     select @FechaRevision = max(CH_FECHA) from dbo.CUR_REV where ( ( CUR_REV.CU_CODIGO = @NewCuCodigo ) AND ( CUR_REV.CH_REVISIO = @NewKcRevisio ) ); 
     if (@FechaProgram is null)
     begin
          set @FechaProgram = DATEADD( DAY, @DiasAntig , @FechaAntig );
     end;
    
     IF Update(CU_CODIGO)
	 begin
		  set @FechaProgram =  ( select case when @FecTomada is null then @FechaAntig
		        when ( @ReprogDias > 0 AND @ReprogVersion = 'N'  )THEN (  DATEADD( DAY, @ReprogDias , @FecTomada ))
				when ( ( @ReprogDias > 0 AND @ReprogVersion = 'S' ) and ( @NewKcRevisio = @cu_revisio ) and (  not (@NewKcRevisio = '')  ) )then ( select max(CH_FECHA) from dbo.CUR_REV where( ( CUR_REV.CU_CODIGO = @NewCuCodigo ) AND ( CUR_REV.CH_REVISIO = @cu_revisio ) ) ) 
				when ( ( @ReprogDias > 0 AND @ReprogVersion = 'S' ) and ((( @NewKcRevisio = @cu_revisio ) and ( @NewKcRevisio = '' ))or ( ( @NewKcRevisio <> @cu_revisio ) ) ) ) then ( DATEADD( DAY, @ReprogDias , @FecTomada ) )              
				when ( @ReprogDias = 0 AND @ReprogVersion = 'N' )then ( DATEADD( DAY, @DiasAntig , @FechaAntig ))			    
				when ( @ReprogDias = 0 AND @ReprogVersion = 'S' )then ( select max(CH_FECHA) from dbo.CUR_REV where( ( CUR_REV.CU_CODIGO = @NewCuCodigo ) AND ( CUR_REV.CH_REVISIO = @cu_revisio ) ) )
				else ( DATEADD( DAY, @DiasAntig , @FechaAntig ) )End );
		
		  update KARCURSO set KC_FECPROG = @FechaProgram WHERE karcurso.cb_codigo = @NewCbCodigo and karcurso.kc_fec_tom = @NewKcFecTom and karcurso.cu_codigo = @NewCuCodigo;
	 end;
	 if ( Update ( KC_REVISIO ) and @ReprogVersion = 'S'  )
     begin	
            If( (@FechaRevision is null) or (@FechaRevision = '12/30/1899') )
			begin 	
			       if (@ReprogDias > 0 )
				   begin
						Select @FechaProgram = max(KC_FECPROG) from KARCURSO where (kc_fec_tom = @NewKcFecTom and CU_CODIGO = @NewCuCodigo and CB_CODIGO = @NewCbCodigo);
				   end
				   else
						set @FechaProgram = DATEADD( DAY, @DiasAntig , @FechaAntig );
					
			end			
			else
			begin				
				set @FechaProgram = @FechaRevision;				
			end;
			
			update KARCURSO set KC_FECPROG = @FechaProgram WHERE karcurso.cb_codigo = @NewCbCodigo and karcurso.kc_fec_tom = @NewKcFecTom and karcurso.cu_codigo = @NewCuCodigo;
	 end        
END
GO

/* SP_PATCH : CR:2460, Se crea trigger TR_CB_GP_COD de actualizacion de timestamp de HUELLA */
/* Patch 430  #Seq: 117 */
create trigger TR_CB_GP_COD on COLABORA after update as
begin
	set NOCOUNT on;
	declare @OldCodigo NumeroEmpleado;
	select @OldCodigo = CB_CODIGO from Deleted;

	if update( CB_GP_COD )
		update HUELLA set HU_SYNC = getdate() where CB_CODIGO = @OldCodigo
end
GO

/*1904 - Costeo: Distribuci�n de n�mina en los centros de costo que trabaj� el empleado durante el periodo de n�mina*/
/*Patch #430  Seq: 118 */
CREATE TRIGGER TD_NOMINA_COSTEO ON NOMINA AFTER DELETE AS
BEGIN
  SET NOCOUNT ON
  DELETE	T_ProporcionCostos
  FROM	T_ProporcionCostos, DELETED
  WHERE	T_ProporcionCostos.NominaID = DELETED.LLAVE

  DELETE	T_ProporcionTotales
  FROM	T_ProporcionTotales, DELETED
  WHERE	T_ProporcionTotales.NominaID = DELETED.LLAVE

  DELETE	T_Costos
  FROM	T_Costos, DELETED
  WHERE	T_Costos.NominaID = DELETED.LLAVE

  DELETE	T_Costos_SubCta
  FROM	T_Costos_SubCta, DELETED
  WHERE	T_Costos_SubCta.NominaID = DELETED.LLAVE
END
GO


