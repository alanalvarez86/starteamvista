/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$   Base De Datos DATOS      $$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/

/*****************************************/
/****  Tablas Nuevas***********************/
/*****************************************/

/* View V_BITKIOSC: Vista de la tabla BITKIOSCO ( PRO:Bit�cora de Kiosco) */
/* Patch 410 # Seq: 1 Agregado */
CREATE VIEW V_BITKIOSC( CB_CODIGO,
                        CM_CODIGO,
                        BI_FECHA,
                        BI_HORA,
                        BI_TIPO,
                        BI_ACCION,
                        BI_NUMERO,
                        BI_TEXTO,
                        BI_UBICA,
                        BI_KIOSCO,
                        BI_FEC_MOV,
                        BI_FEC_HOR )
as
  SELECT           CB_CODIGO,
                   CM_CODIGO,
                   BI_FECHA,
                   BI_HORA,
                   BI_TIPO,
                   BI_ACCION,
                   BI_NUMERO,
                   BI_TEXTO,
                   BI_UBICA,
                   BI_KIOSCO,
                   BI_FEC_MOV,
                   BI_FEC_HORA

            FROM #COMPARTE.dbo.BITKIOSCO

GO

/*****************************************/
/**** M�ltiples Razones Sociales**********/
/*****************************************/


/* Agregar tabla RSOCIAL: Totales de empresa ( PRO: M�ltiples Razones Sociales ) */
/* Patch 410 # Seq: 2 Agregado */
CREATE TABLE RSOCIAL (
       RS_CODIGO            Codigo,           /* C�digo */
       RS_NOMBRE            Titulo,           /* Raz�n Social de Empresa */
       RS_CALLE             Titulo,           /* Calle de Empresa */
       RS_NUMEXT            Referencia,        /* Numero Exterior */
       RS_NUMINT            Referencia,        /* Numero Interior */
       RS_COLONIA           Titulo,           /* Colonia de Empresa */
       RS_CIUDAD            Titulo,           /* Ciudad de Empresa */
       RS_ENTIDAD           Codigo2,          /* Entidad de Empresa */
       RS_CODPOST           Referencia,        /* C�digo Postal Empresa */
       RS_TEL               Descripcion,       /* Tel�fono de Empresa */
       RS_FAX               Descripcion,       /* Fax de Empresa */
       RS_EMAIL             DescLarga,         /* Correo electr�nico de Empresa */
       RS_WEB               PathURL,           /* P�gina Internet de Empresa */
       RS_RFC               Descripcion,       /* R.F.C. de Empresa */
       RS_INFO              Descripcion,       /* # de INFONAVIT de Empresa */
       RS_GUIA              Descripcion,       /* # de Gu�a de IMSS */
       RS_RLEGAL            Titulo,            /* Representante Legal */
       RS_SUBSID            Tasa,              /* % Subsidio Acreditable */
       RS_GIRO              Formula,           /* Giro de la Empresa */
       RS_STPS              Descripcion,       /* # de Registro ante STPS */
       RS_STPS_R1           Titulo,            /* Representante de Empresa en STPS */
       RS_STPS_R2           Titulo,            /* Representante de Empleados en STPS */
       RS_NUMERO            Pesos,            /* N�mero de uso General */
       RS_TEXTO             Descripcion      /* Texto de uso General */
)
GO

/* Tabla RSOCIAL: Agregar llave primaria ( PRO: M�ltiples Razones Sociales ) */
/* Patch 410 # Seq: 3 Agregado */
ALTER TABLE RSOCIAL
       ADD CONSTRAINT PK_RSOCIAL PRIMARY KEY ( RS_CODIGO )
GO

/* Tabla RPATRON: 'Raz�n Social' ( PRO: M�ltiples Razones Sociales ) */
/* Patch 410 # Seq: 4 Agregado */
alter table RPATRON add RS_CODIGO Codigo
GO

/* Tabla RPATRON: Inicializar Raz�n Social ( PRO: M�ltiples Razones Sociales ) */
/* Patch 410 # Seq: 5 Agregado */
update RPATRON set RS_CODIGO = '' where ( RS_CODIGO is NULL )
GO


/* INIT_RSOCIAL: Agregar registro default de Raz�n Social (1/3) ( PRO: M�ltiples Razones Sociales ) */
/* Patch 410 # Seq: 6 Agregado */
create procedure INIT_RSOCIAL
as
begin
     SET NOCOUNT ON;
     declare @Cuantos Integer;
     select @Cuantos = COUNT(*) from RSOCIAL
     if ( @Cuantos = 0 )
     begin
            
          insert RSOCIAL SELECT 'RASDEF', /*RS_CODIGO*/
		( select SUBSTRING ( COALESCE( ( select GL_FORMULA from GLOBAL where GL_CODIGO = 1 ), '' ),1,100 ) ),  /*RS_NOMBRE*/
		( select SUBSTRING ( COALESCE( ( select GL_FORMULA from GLOBAL where GL_CODIGO = 2 ), '' ),1,100 ) ),  /*RS_CALLE*/
		( select SUBSTRING ( COALESCE( ( select GL_FORMULA from GLOBAL where GL_CODIGO = 208 ), '' ),1,8 ) ),   /*RS_NUMEXT*/
		( select SUBSTRING ( COALESCE( ( select GL_FORMULA from GLOBAL where GL_CODIGO = 209 ), '' ),1,8 ) ),  /*RS_NUMINT*/
		( select SUBSTRING ( COALESCE( ( select GL_FORMULA from GLOBAL where GL_CODIGO = 3 ), '' ),1,100 ) ), /*RS_COLONIA*/
		( select SUBSTRING ( COALESCE( ( select GL_FORMULA from GLOBAL where GL_CODIGO = 4 ), '' ),1,100 ) ),  /*RS_CIUDAD*/
		( select SUBSTRING ( COALESCE( ( select GL_FORMULA from GLOBAL where GL_CODIGO = 5 ), '' ) ,1,2 ) ), /*RS_ENTIDAD*/
		( select SUBSTRING ( COALESCE( ( select GL_FORMULA from GLOBAL where GL_CODIGO = 6 ), '' ) ,1,8 ) ), /*RS_CODPOST*/
		( select SUBSTRING ( COALESCE( ( select GL_FORMULA from GLOBAL where GL_CODIGO = 7 ), '' ) ,1,30 ) ), /*RS_TEL*/
		  ''  /*RS_FAX*/ , ''  /*RS_EMAIL*/, ''  /*RS_WEB*/,
		( select SUBSTRING ( COALESCE( ( select GL_FORMULA from GLOBAL where GL_CODIGO = 8 ), '' ),1,30 ) ),  /*RS_RFC*/
		( select SUBSTRING ( COALESCE( ( select GL_FORMULA from GLOBAL where GL_CODIGO = 9 ), '' ),1,30 ) ),  /*RS_INFO*/
		( select SUBSTRING ( COALESCE( ( select GL_FORMULA from GLOBAL where GL_CODIGO = 11 ), '' ),1,30 ) ),  /*RS_GUIA*/
		( select SUBSTRING ( COALESCE( ( select GL_FORMULA from GLOBAL where GL_CODIGO = 12 ), '' ),1,100) ),  /*RS_RLEGAL*/
		( COALESCE( ( select CONVERT( numeric(15,5), ( select GL_FORMULA from GLOBAL where GL_CODIGO = 43 ) ) ), 0.00000 )  ), /*RS_SUBSID*/
		( COALESCE( ( select GL_FORMULA from GLOBAL where GL_CODIGO = 80 ), '' ) ),  /*RS_GIRO*/
		( select SUBSTRING ( COALESCE( ( select GL_FORMULA from GLOBAL where GL_CODIGO = 81 ), '' ),1,30) ),  /*RS_STPS*/
		( select SUBSTRING ( COALESCE( ( select GL_FORMULA from GLOBAL where GL_CODIGO = 82 ), '' ),1,100 ) ),  /*RS_STPS_R1*/
		( select SUBSTRING ( COALESCE( ( select GL_FORMULA from GLOBAL where GL_CODIGO = 83 ), '' ),1,100 ) ),  /*RS_STPS_R2*/
		 0  /*RS_NUMERO*/, ''  /*RS_TEXTO*/
		
          update RPATRON set RS_CODIGO = 'RASDEF';
     end
end
GO


/* INIT_RSOCIAL: Agregar registro default de Raz�n Social (2/3) ( PRO: M�ltiples Razones Sociales ) */
/* Patch 410 # Seq: 7 Agregado */
execute INIT_RSOCIAL
GO

/* INIT_RSOCIAL: Agregar registro default de Raz�n Social (3/3) ( PRO: M�ltiples Razones Sociales ) */
/* Patch 410 # Seq: 8 Agregado */
drop procedure INIT_RSOCIAL
GO

/*INIT_RSOCIAL: Agregar Llave foranea default de Raz�n Social ( PRO: M�ltiples Razones Sociales ) */
/* Patch 410 # Seq: 9 Agregado */
ALTER TABLE RPATRON
       ADD CONSTRAINT FK_RPatron_RSocial
       FOREIGN KEY (RS_CODIGO)
       REFERENCES RSOCIAL
       ON DELETE NO ACTION
       ON UPDATE CASCADE;
GO 

/*****************************************/
/***** N�minas Complementarias************/
/*****************************************/

/* Agregar global 213: N�mero de l�mite de n�minas ordinarias con valor default de 200: Totales de empresa (PRO: N�minas Complementarias)(1/3) */
/* Patch 410 # Seq: 10 Agregado */
create procedure GLOBAL_AGREGAR( @Global INTEGER, @Descripcion VARCHAR(30), @Formula VARCHAR(255) )
as
begin
     SET NOCOUNT ON;
     declare @Cuantos Integer;
     select @Cuantos = COUNT(*) from GLOBAL where ( GL_CODIGO = @Global );
     if ( @Cuantos = 0 )
     begin
          insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA ) values ( @Global, @Descripcion, @Formula );
     end
end
GO

/* Agregar global 213: N�mero de l�mite de n�minas ordinarias con valor default de 200: Totales de empresa (PRO: N�minas Complementarias)(2/3) */
/* Patch 410 # Seq: 11 Agregado */
exec GLOBAL_AGREGAR 213, 'N�mero l�mite de n�minas ordinarias', '200';
GO

/* Agregar global 213: N�mero de l�mite de n�minas ordinarias con valor default de 200: Totales de empresa (PRO: N�minas Complementarias)(3/3) */
/* Patch 410 # Seq: 12 Agregado */
drop procedure GLOBAL_AGREGAR
GO

/*************************************************/
/*Nuevo Mecanismo para proceso de lectura de reloj*/
/*************************************************/


/* Tabla HORARIO: 'Revisa salida de d�a anterior' (PRO: Nuevo Mecanismo para proceso de lectura de reloj) */
/* Patch 410 # Seq: 13 Agregado */
alter table HORARIO add HO_PARES Booleano
GO

/* Tabla HORARIO: Inicializar Revisa salida de d�a anterior (PRO: Nuevo Mecanismo para proceso de lectura de reloj) */
/* Patch 410 # Seq: 14 Agregado */
update HORARIO set HO_PARES = 'N' where ( HO_PARES is NULL )
GO


/*************************************************/
/*************Cursos en Supervisores**************/
/*************************************************/


/* Tabla SESION: US_CODIGO (PRO: Cursos en Supervisores)*/
/* Patch 410 # Seq: 15 Agregado */
alter table SESION add US_CODIGO Usuario
GO

/* Tabla SESION: Inicializar US_CODIGO (PRO: Cursos en Supervisores)*/
/* Patch 410 # Seq: 16 Agregado */
update SESION set US_CODIGO = 0 where ( US_CODIGO is NULL )
GO

/* View V_CURSO: Vista de la tabla CURSO ( PRO:Cursos en supervisores ) */
/* Patch 410 # Seq: 17 Agregado */
CREATE VIEW  V_CURSO (
			CU_CODIGO,
			CU_FOLIO,
			CU_NOMBRE,
			CU_ACTIVO,
			MA_CODIGO,
			CU_HORAS,
			CU_REVISIO,
			CU_COSTO1,
			CU_COSTO2,
			CU_COSTO3 )
as
select  CU_CODIGO,CU_FOLIO, 
			 CU_NOMBRE,
			 CU_ACTIVO,
			 MA_CODIGO,
			 CU_HORAS,
			 CU_REVISIO,
			 CU_COSTO1,
			 CU_COSTO2,
			 CU_COSTO3
		 FROM CURSO
		 
		
GO


/*************************************************/
/*************Horas extras pactadas **************/
/*************************************************/

/*Tabla HORARIO: Texto (PRO:Horas extras pactadas) */
/* Patch 410 # Seq: 18 Agregado */
alter table HORARIO add HO_TEXTO  Descripcion
GO

/*Tabla HORARIO: Inicializar Texto de uso general (PRO:Horas extras pactadas) */
/* Patch 410 # Seq: 19 Agregado */
update HORARIO set HO_TEXTO = '' where ( HO_TEXTO is NULL )
GO

/*Tabla HORARIO: Texto Ingl�s (PRO:Horas extras pactadas) */
/* Patch 410 # Seq: 20 Agregado */
alter table HORARIO add HO_INGLES Descripcion
GO 

/*Tabla HORARIO: Inicializar Texto Ingl�s (PRO:Horas extras pactadas) */
/* Patch 410 # Seq: 21 Agregado */
update HORARIO set HO_INGLES = '' where ( HO_INGLES is NULL )
GO

/*Tabla HORARIO: N�mero de uso general (PRO:Horas extras pactadas) */
/* Patch 410 # Seq: 22 Agregado */
alter table HORARIO add HO_NUMERO	Pesos 
GO

/*Tabla HORARIO: Inicializar N�mero de uso general (PRO:Horas extras pactadas) */
/* Patch 410 # Seq: 23 Agregado */
update HORARIO set HO_NUMERO = 0 where ( HO_NUMERO is NULL )
GO

/*Tabla AUSENCIA: Horas extras pactadas ( PRO:Horas extras pactadas ) */
/* Patch 410 # Seq: 24 Agregado */
alter table AUSENCIA add AU_PRE_EXT Horas
GO

/*Tabla AUSENCIA: Inicializa Horas extras pactadas ( PRO:Horas extras pactadas ) */
/* Patch 410 # Seq: 25 Agregado */
update AUSENCIA set AU_PRE_EXT = 0 where ( AU_PRE_EXT is NULL )
GO


/*Tabla NOMINA: Horas extras pactadas ( PRO:Horas extras pactadas ) */
/* Patch 410 # Seq: 26 Agregado */
alter table NOMINA add NO_PRE_EXT Horas
GO

/*Tabla NOMINA: Inicializa Horas extras pactadas ( PRO:Horas extras pactadas ) */
/* Patch 410 # Seq: 27 Agregado */
update NOMINA set NO_PRE_EXT = 0 where ( NO_PRE_EXT is NULL )
GO

/* Tabla TMPNOM: Horas extras pactadas ( PRO:Horas extras pactadas ) */
/* Patch 410 # Seq: 28 Agregado */
alter table TMPNOM add NO_PRE_EXT PesosEmpresa
GO

/*Tabla TMPNOM: Inicializa Horas extras pactadas ( PRO:Horas extras pactadas ) */
/* Patch 410 # Seq: 29 Agregado */
update TMPNOM set NO_PRE_EXT = 0 where ( NO_PRE_EXT is NULL )
GO

/* AGREGA_CONCEPTO_SISTEMA: Agrega un concepto de sistema cuando no existe (PRO: Horas Pactadas) (1/3) */
/* Patch 410 # Seq: 30 Agregado */
create procedure AGREGA_CONCEPTO_SISTEMA( @Concepto Smallint, @Descripcion VARCHAR(30) )
as
begin
     SET NOCOUNT ON;
     declare @Cuantos Integer;
     select @Cuantos = COUNT(*) from CONCEPTO where( CO_NUMERO = @Concepto );
     if ( @Cuantos = 0 )
     begin
          insert into CONCEPTO( CO_NUMERO, CO_DESCRIP, CO_ACTIVO ) values ( @Concepto, @Descripcion, 'S' );
     end
end
GO

/* AGREGA_CONCEPTO_SISTEMA: Agrega el concepto 1216 (PRO: Horas Pactadas) (2/3)*/
/* Patch 410 # Seq: 31 Agregado */
execute AGREGA_CONCEPTO_SISTEMA 1216, 'Extras prepagadas';


/* AGREGA_CONCEPTO_SISTEMA: Borra procedure (PRO: Horas Pactadas) (3/3)*/
/* Patch 410 # Seq: 32 Agregado */
drop procedure AGREGA_CONCEPTO_SISTEMA
GO


/*****************************************/
/**** Excluir FI al exportar a SUA********/
/*****************************************/

/* Agregar global 214: SUA: �Incluir Incapacidadaes? (PRO: Excluir FI al exportar a SUA)*/
/* Patch 410 # Seq: 33 Agregado */
if not EXISTS ( select * from global where gl_codigo = 214 )
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA ) 
values ( 214, 'SUA: Incluir FI', 'S' )



/*****************************************/
/********** ENROLAMIENTO******************/
/*****************************************/

/* Tabla COLABORA: Usuario (PRO: Enrolamiento)*/
/* Patch 410 # Seq: 34 Agregado */
ALTER TABLE COLABORA ADD US_CODIGO Usuario
GO


/*****************************************/
/**************** PORTAL******************/
/*****************************************/


/* AGREGA_LLAVES: Agrega el campo LLAVE a Tablas de Tress (PORTAL)  ( 1/5 )*/
/* Patch 410 # Seq: 35 Agregado */
CREATE PROCEDURE AGREGA_LLAVES
AS
BEGIN
	SET NOCOUNT ON
	declare @NombreTabla Formula
	DECLARE @Comando NVarchar(4000)

	declare Temporal CURSOR FOR   
    	select table_name from information_schema.tables where 
	( TABLE_TYPE = 'BASE TABLE' ) and 
	( TABLE_NAME not like 'TMP_%' ) 
	order by TABLE_NAME
	OPEN Temporal
   	FETCH NEXT FROM Temporal INTO @NombreTabla

	WHILE @@FETCH_STATUS = 0
    	BEGIN
		if ( IDENT_CURRENT ( @NombreTabla ) is NULL ) 
		begin
			set @Comando = N'alter table #TABLA add LLAVE FolioGrande identity( 1, 1)'
		
			set @Comando = REPLACE( @Comando, '#TABLA', @NombreTabla )
   		
			EXEC SP_EXECUTESQL @Comando 
		end
		FETCH NEXT FROM Temporal INTO @NombreTabla
    	END
	close Temporal
     deallocate Temporal

END
GO

/* AGREGA_LLAVES: Agrega el campo LLAVE a Tablas de Tress (PORTAL)  ( 3/5 )*/
/* Patch 410 # Seq: 37 Agregado */
exec AGREGA_LLAVES
GO

/* AGREGA_LLAVES: Agrega el campo LLAVE a Tablas de Tress (PORTAL)  ( 4/5 )*/
/* Patch 410 # Seq: 39 Agregado */
drop procedure AGREGA_LLAVES
GO

/* TFIJAS:  Se elimina la tabla TFIJAS */
/* Patch 410 # Seq: 40 Agregado */
DROP TABLE TFIJAS
GO

/* TFIJAS:  View TFIJAS  ( Portal )*/
/* Patch 410 # Seq: 41 Agregado */
CREATE VIEW TFIJAS( TF_TABLA,
                    TF_CODIGO,
                    TF_DESCRIP )
as
SELECT              LV_CODIGO,
                    VL_CODIGO,
                    VL_DESCRIP 
FROM R_VALOR
GO


/* V_VPUNTOS: Define columnas en VPUNTOS (2/3) ( Portal )*/
/* Patch 410 # Seq: 42 Modificado */
ALTER VIEW V_VPUNTOS
AS
select
	VPUNTOS.VP_FOLIO,VPUNTOS.VT_ORDEN,VPUNTOS.VT_CUAL,VPUNTOS.VT_COMENTA, 
	COALESCE( VN_PUNTOS, 0 ) VT_PUNTOS,
	VN.*                                               
from
	VPUNTOS
		join VAL_PTO VP on VPUNTOS.VP_FOLIO = VP.VP_FOLIO
		left join VALNIVEL VN  on VN.VL_CODIGO = VP.VL_CODIGO and VN.VS_ORDEN = VPUNTOS.VT_ORDEN and VN.VN_ORDEN = VPUNTOS.VT_CUAL
GO





/***************************************************/
/***********Matriz de Certificaciones *****************/
/**************************************************/
/**************************************************/


/* Agregar Tabla PTO_CERT: Puesto por certificaci�n ( SUG 1019: Matriz de Certificaciones )*/
/* Patch 410 # Seq: 43 Agregado */

CREATE TABLE PTO_CERT (
	CI_CODIGO Codigo NOT NULL , /* Codigo de cetificacion */	
	PU_CODIGO Codigo NOT NULL , /* Codigo de Puesto */
	PC_DIAS Dias NOT NULL ,     /* Antig�edad en dias */ 	
	PC_OPCIONA Booleano NOT NULL , /* Certificacion opcional */
    PC_LISTA Booleano NOT NULL, /* �Tiene lista de c�digos? */
	LLAVE FolioGrande IDENTITY (1, 1) NOT NULL 
)
go


/* Tabla PTO_CERT: Llave Primaria ( SUG 1019: Matriz de Certificaciones )*/
/* Patch 410 # Seq: 44 Agregado */

ALTER TABLE PTO_CERT
       ADD CONSTRAINT PK_PTO_CERT PRIMARY KEY (CI_CODIGO, PU_CODIGO)
go


/* Tabla PTO_CERT: Llave for�nea hacia Certificaciones(SUG 1019: Matriz de Certificaciones)*/
/* Patch 410 # Seq: 45 Agregado */
ALTER TABLE PTO_CERT
       ADD CONSTRAINT FK_PTO_CERT_Certifica
       FOREIGN KEY (CI_CODIGO)
       REFERENCES CERTIFIC
       ON DELETE CASCADE
       ON UPDATE CASCADE;

go


/* Tabla PTO_CERT: Llave for�nea hacia puesto (SUG 1019: Matriz de Certificaciones)*/
/* Patch 410 # Seq: 46 Agregado */	   
ALTER TABLE PTO_CERT
       ADD CONSTRAINT FK_PTO_CERT_Puesto
       FOREIGN KEY (PU_CODIGO)
       REFERENCES PUESTO
       ON DELETE CASCADE
       ON UPDATE CASCADE;
go


/* Agregar Tabla CERNIVEL: Certificaciones por nivel de empresa (SUG 1019: Matriz de Certificaciones)*/
/* Patch 410 # Seq: 47 Agregado */	
CREATE TABLE CERNIVEL (
	PU_CODIGO Codigo NOT NULL ,
	CI_CODIGO Codigo NOT NULL ,
	CN_CODIGO Codigo NOT NULL ,
	LLAVE FolioGrande IDENTITY (1, 1) NOT NULL 
)

go

/* Tabla CERNIVEL: Certificaciones por nivel de empresa (SUG 1019: Matriz de Certificaciones)*/
/* Patch 410 # Seq: 48 Agregado */
ALTER TABLE CERNIVEL
       ADD CONSTRAINT PK_CERNIVEL PRIMARY KEY (PU_CODIGO, CI_CODIGO,CN_CODIGO)
go

/* Tabla CERNIVEL: Certificaciones por nivel de empresa (SUG 1019: Matriz de Certificaciones)*/
/* Patch 410 # Seq: 49 Agregado */
ALTER TABLE CERNIVEL
       ADD CONSTRAINT FK_CERNIVEL
       FOREIGN KEY (CI_CODIGO, PU_CODIGO)
       REFERENCES PTO_CERT
       ON DELETE CASCADE
       ON UPDATE CASCADE;

GO

/* View CER_PROG: Certificaciones programadas (SUG 1019: Matriz de Certificaciones)*/
/* Patch 410 # Seq: 50 Agregado */

create view CER_PROG (
   CB_CODIGO, /*Numero de empleado*/
   CB_PUESTO, /*Codigo de puesto*/
   CI_CODIGO, /*Codigo de certificacion*/
   KI_FEC_PRO, /*Fecha de la certificacion, KC_FEC_PRO*/
   KI_FEC_TOM,/*Fecha en que se tom� la certificaciones KC_FEC_TOM*/
   KI_CALIF_1, /*Calificacion1,*/
   KI_CALIF_2, /*Calificacion2,*/
   KI_CALIF_3, /*Calificacion3,*/
   PC_OPCIONA, /*Certificacion opcional?*/
   PC_LISTA /*Contiene niveles?,*/
   ) AS
select
  C.CB_CODIGO,
  C.CB_PUESTO,
  CERTIFIC.CI_CODIGO,
  DATEADD( DAY, E.PC_DIAS, C.CB_FEC_ING ) KC_FEC_PRO,
  KAR_CERT.KI_FEC_CER,
  KAR_CERT.KI_CALIF_1,
  KAR_CERT.KI_CALIF_2,
  KAR_CERT.KI_CALIF_3,
  E.PC_OPCIONA,
  E.PC_LISTA
from COLABORA C
join PTO_CERT E on ( E.PU_CODIGO = C.CB_PUESTO )
left join KAR_CERT on ( KAR_CERT.CB_CODIGO = C.CB_CODIGO ) and ( KAR_CERT.CI_CODIGO = E.CI_CODIGO )
left join CERTIFIC on ( CERTIFIC.CI_CODIGO = E.CI_CODIGO )
where ( CERTIFIC.CI_ACTIVO = 'S' )
go


/**************************************************/
/**SUG 756: Campos adicionales obligatorios********/
/**************************************************/

/* Tabla CAMPO_AD: � Campo Obligatorio ? (SUG 756: Campos adicionales obligatorios)*/
/* Patch 410 # Seq: 51 Agregado */

alter table CAMPO_AD add CX_OBLIGA Booleano
GO

/* Tabla CAMPO_AD: Inicializacion de CX_OBLIGA (SUG 756: Campos adicionales obligatorios)*/
/* Patch 410 # Seq: 52 Agregado */ 

update CAMPO_AD set CX_OBLIGA ='N'
go

/***************************************************/
/*SUG 858: Derechos de acceso en campos adicionales*/
/**************************************************/

/* Agregar Tabla GR_AD_ACC: Derechos de acceso de campos adicionales  (SUG 858: Derechos de acceso en campos adicionales)*/
/* Patch 410 # Seq: 53 Agregado */

CREATE TABLE GR_AD_ACC (
	CM_CODIGO CodigoEmpresa NOT NULL ,
	GR_CODIGO FolioChico NOT NULL ,
	GX_CODIGO Codigo NOT NULL ,
	GX_DERECHO FolioChico NOT NULL ,
	LLAVE FolioGrande IDENTITY (1, 1) NOT NULL 
	)
	
go


/* Tabla GR_AD_ACC: Llave Primaria  (SUG 858: Derechos de acceso en campos adicionales)*/
/* Patch 410 # Seq: 54 Agregado */
ALTER TABLE GR_AD_ACC
       ADD CONSTRAINT PK_GR_AD_ACC PRIMARY KEY (CM_CODIGO, GR_CODIGO, GX_CODIGO)
go


/* SP_CREA_DERECHOS_GRUPO_AD: Poblar Dicci�n ( 1/7 )( SUG 858: Derechos de acceso en campos adicionales )*/
/* Patch 410 # Seq: 55 Agregado */
CREATE PROCEDURE SP_CREA_DERECHOS_GRUPO_AD (@CM_CODIGO CodigoEmpresa, @GR_CODIGO FolioChico  ) as
BEGIN
  SET NOCOUNT ON;
	DECLARE @Cursor_ADC Cursor
	DECLARE @Codigo varchar(6)
	DECLARE @Derecho int
	SELECT @Derecho = AX_DERECHO FROM #COMPARTE.DBO.ACCESO 
		WHERE CM_CODIGO = @CM_CODIGO 
		and GR_CODIGO = @GR_CODIGO
		and AX_NUMERO = 8
		
	Set @Cursor_ADC = CURSOR  FORWARD_ONLY STATIC FOR
	Select GX_CODIGO from GRUPO_AD
	OPEN @Cursor_ADC
		FETCH NEXT FROM @Cursor_ADC INTO @Codigo
		while ( @@FETCH_STATUS = 0 )
		BEGIN
			if ( @Derecho is NOT NULL )
			begin
				Insert into GR_AD_ACC (GR_CODIGO, CM_CODIGO,GX_CODIGO, GX_DERECHO )
				VALUES (@GR_CODIGO, @CM_CODIGO, @Codigo, @Derecho);
			end
			FETCH NEXT FROM @Cursor_ADC INTO @Codigo
		END
	CLOSE @Cursor_ADC
	DEALLOCATE @Cursor_ADC
end
GO


/* SP_CREA_DERECHOS_GRUPO_AD_POR_GRUPOS: Poblar Dicci�n ( 2/7 )( SUG 858: Derechos de acceso en campos adicionales )*/
/* Patch 410 # Seq: 56 Agregado */ 

CREATE PROCEDURE SP_CREA_DERECHOS_GRUPO_AD_POR_GRUPOS (@CM_CODIGO CodigoEmpresa ) as
BEGIN
  SET NOCOUNT ON;
	DECLARE @Cursor_Grupo Cursor
	DECLARE @Codigo int
	Set @Cursor_Grupo = CURSOR FORWARD_ONLY STATIC FOR
	Select GR_CODIGO from #COMPARTE.DBO.GRUPO
	OPEN @Cursor_Grupo
		FETCH NEXT FROM @Cursor_Grupo INTO @Codigo
		while ( @@FETCH_STATUS = 0 )
		BEGIN
			EXECUTE SP_CREA_DERECHOS_GRUPO_AD @CM_CODIGO, @Codigo
			FETCH NEXT FROM @Cursor_Grupo INTO @Codigo
		END
	CLOSE @Cursor_Grupo
	DEALLOCATE @Cursor_Grupo
end
GO


/* SP_POBLAR_DICCION: Poblar Dicci�n ( 3/7 )( SUG 858: Derechos de acceso en campos adicionales )*/
/* Patch 410 # Seq: 57 Agregado */ 

CREATE PROCEDURE SP_POBLAR_DICCION(@EMPRESA CodigoEmpresa) AS
BEGIN
     SET NOCOUNT ON;
     DECLARE @Resultado int;
	 DECLARE @CursorCompany Cursor
	 DECLARE @CodigoConfidencial CodigoEmpresa
	 
	 /* Patch 410, se copian los derechos existentes al nuevo esquema*/
	 select @Resultado = count(*) from GR_AD_ACC;
	 
     if (@Resultado = 0 )
     begin
	        
			Set @CursorCompany = CURSOR  FORWARD_ONLY STATIC FOR
			select CM_CODIGO from #Comparte.dbo.COMPANY
			where CM_DATOS = ( SELECT CM_DATOS FROM #Comparte.dbo.COMPANY WHERE CM_CODIGO = @EMPRESA )
			OPEN @CursorCompany
			FETCH NEXT FROM @CursorCompany INTO @CodigoConfidencial
			while ( @@FETCH_STATUS = 0 )
			BEGIN
				 
				 /* Se crean los derechos sobre las entidades */
				 EXEC SP_CREA_DERECHOS_GRUPO_AD_POR_GRUPOS @CodigoConfidencial
		  
				FETCH NEXT FROM @CursorCompany INTO @CodigoConfidencial
			END
			CLOSE @CursorCompany
			DEALLOCATE @CursorCompany
			
	 end;
end
GO

/* SP_POBLAR_DICCION: Poblar Dicci�n ( 4/7 )( SUG 858: Derechos de acceso en campos adicionales )*/
/* Patch 410 # Seq: 58 Agregado */ 

EXEC SP_POBLAR_DICCION #Empresa
GO

/* SP_POBLAR_DICCION: Poblar Dicci�n ( 5/7 )( SUG 858: Derechos de acceso en campos adicionales )*/
/* Patch 410 # Seq: 59 Agregado */ 
DROP PROCEDURE SP_POBLAR_DICCION 
GO


/* SP_POBLAR_DICCION: Poblar Dicci�n ( 6/7 )( SUG 858: Derechos de acceso en campos adicionales )*/
/* Patch 410 # Seq: 60 Agregado */ 

DROP PROCEDURE SP_CREA_DERECHOS_GRUPO_AD_POR_GRUPOS
GO


/* SP_POBLAR_DICCION: Poblar Dicci�n ( 7/7 )( SUG 858: Derechos de acceso en campos adicionales )*/
/* Patch 410 # Seq: 61 Agregado */ 

DROP PROCEDURE SP_CREA_DERECHOS_GRUPO_AD
GO

/**************************************************/
/*****************PRO Patch RDD  *******************/
/**************************************************/

/* Tabla R_ATRIBUTO: Activo, Versi�n y Usuario( PRO Patch RDD  )*/
/* Patch 410 # Seq: 62 Agregado */ 
alter table R_ATRIBUTO add AT_ACTIVO Booleano, AT_VERSION FolioChico, US_CODIGO Usuario;
GO

/* Tabla R_ATRIBUTO: Inicializaci�n Activo, Versi�n y Usuario( PRO Patch RDD  )*/
/* Patch 410 # Seq: 63 Agregado */
update R_ATRIBUTO SET AT_ACTIVO = 'S', AT_VERSION = (select GL_FORMULA from GLOBAL where GL_CODIGO =133), US_CODIGO = 0	
GO


/* Tabla R_CLAS_ACC: Versi�n y Usuario( PRO Patch RDD  )*/
/* Patch 410 # Seq: 64 Agregado */ 
alter table R_CLAS_ACC add RA_VERSION FolioChico, US_CODIGO Usuario;
GO

/* Tabla R_CLAS_ACC: Inicializaci�n RA_VERSION( PRO Patch RDD  )*/
/* Patch 410 # Seq: 65 Agregado */
update R_CLAS_ACC SET RA_VERSION = (select GL_FORMULA from GLOBAL where GL_CODIGO =133), US_CODIGO = 0	
GO
	

/* Tabla R_CLAS_ENT: Versi�n y Usuario( PRO Patch RDD  )*/
/* Patch 410 # Seq: 66 Agregado */ 
alter table R_CLAS_ENT add CE_VERSION FolioChico, US_CODIGO Usuario;
GO

/* Tabla R_CLAS_ENT: Inicializaci�n CE_VERSION ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 67 Agregado */
update R_CLAS_ENT SET CE_VERSION = (select GL_FORMULA from GLOBAL where GL_CODIGO =133), US_CODIGO = 0	
GO
	

/* Tabla R_CLASIFI: Activo, Versi�n, Usuario y M�dulo ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 68 Agregado */ 
alter table R_CLASIFI add RC_ACTIVO Booleano, RC_VERSION FolioChico, RC_MODULO FolioChico, US_CODIGO Usuario;
GO

/* Tabla R_CLASIFI: Inicializaci�n RC_ACTIVO y RC_VERSION  ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 69 Agregado */
update R_CLASIFI SET RC_ACTIVO = 'S', RC_VERSION = (select GL_FORMULA from GLOBAL where GL_CODIGO =133), US_CODIGO = 0	
GO
	
/* Tabla R_DEFAULT:  Versi�n y Usuario ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 70 Agregado */ 
alter table R_DEFAULT add RD_VERSION FolioChico, US_CODIGO Usuario;
GO

/* Tabla R_DEFAULT: Inicializaci�n de RD_VERSION ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 71 Agregado */ 
update R_DEFAULT SET RD_VERSION = (select GL_FORMULA from GLOBAL where GL_CODIGO =133), US_CODIGO = 0	
GO
	
/* Tabla R_ENT_ACC: Versi�n y Usuario ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 72 Agregado */ 
alter table R_ENT_ACC add RE_VERSION FolioChico, US_CODIGO Usuario;
GO

/* Tabla R_ENT_ACC: Inicializaci�n de RE_VERSION ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 73 Agregado */ 
update R_ENT_ACC SET RE_VERSION = (select GL_FORMULA from GLOBAL where GL_CODIGO =133), US_CODIGO = 0	
GO
	
/* Tabla R_ENTIDAD: Activo, Versi�n y Usuario ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 74 Agregado */ 
alter table R_ENTIDAD add EN_ACTIVO Booleano, EN_VERSION FolioChico, US_CODIGO Usuario;
GO

/* Tabla R_ENTIDAD: Inicializaci�n de EN_ACTIVO, EN_VERSION ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 75 Agregado */ 
update R_ENTIDAD SET EN_ACTIVO = 'S', EN_VERSION = (select GL_FORMULA from GLOBAL where GL_CODIGO =133), US_CODIGO = 0	
GO

/* Tabla R_FILTRO: Versi�n y Usuario ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 76 Agregado */ 	
alter table R_FILTRO add RF_VERSION FolioChico, US_CODIGO Usuario;
GO


/* Tabla R_FILTRO: Inicializaci�n de RF_VERSION ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 77 Agregado */ 	
update R_FILTRO SET RF_VERSION = (select GL_FORMULA from GLOBAL where GL_CODIGO =133), US_CODIGO = 0	
GO
	
/* Tabla R_LISTAVAL: Activo, Versi�n y Usuario ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 78 Agregado */ 	
alter table R_LISTAVAL add LV_ACTIVO Booleano, LV_VERSION FolioChico, US_CODIGO Usuario;
GO

/* Tabla R_LISTAVAL: Inicializaci�n de LV_ACTIVO y LV_VERSION ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 79 Agregado */ 
update R_LISTAVAL SET LV_ACTIVO = 'S', LV_VERSION = (select GL_FORMULA from GLOBAL where GL_CODIGO =133), US_CODIGO = 0	
GO
	

/* Tabla R_MOD_ENT: Versi�n y Usuario ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 80 Agregado */ 
alter table R_MOD_ENT add ME_VERSION FolioChico, US_CODIGO Usuario;
GO

/* Tabla R_MOD_ENT: Inicializaci�n de ME_VERSION  ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 81 Agregado */
update R_MOD_ENT SET ME_VERSION = (select GL_FORMULA from GLOBAL where GL_CODIGO =133), US_CODIGO = 0	
GO
	

/* Tabla R_MODULO: Activo, Versi�n y Usuario ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 82 Agregado */ 
alter table R_MODULO add MO_ACTIVO Booleano, MO_VERSION FolioChico, US_CODIGO Usuario;
GO

/* Tabla R_MODULO: Inicializaci�n de MO_ACTIVO y MO_VERSION ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 83 Agregado */ 
update R_MODULO SET MO_ACTIVO = 'S', MO_VERSION = (select GL_FORMULA from GLOBAL where GL_CODIGO =133), US_CODIGO = 0	
GO
	
/* Tabla R_ORDEN: Versi�n ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 84 Agregado */ 
alter table R_ORDEN add RO_VERSION FolioChico, US_CODIGO Usuario;
GO

/* Tabla R_ORDEN: Inicializaci�n RO_VERSION ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 85 Agregado */ 
update R_ORDEN SET RO_VERSION = (select GL_FORMULA from GLOBAL where GL_CODIGO =133), US_CODIGO = 0	
GO
	

/* Tabla R_RELACION: Activo, Versi�n y Usuario( PRO Patch RDD  )*/
/* Patch 410 # Seq: 86 Agregado */ 
alter table R_RELACION add RL_ACTIVO Booleano, RL_VERSION FolioChico, US_CODIGO Usuario;
GO

/* Tabla R_RELACION: Inicializaci�n RL_ACTIVO, RL_VERSION ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 87 Agregado */ 
update R_RELACION SET RL_ACTIVO = 'S', RL_VERSION = (select GL_FORMULA from GLOBAL where GL_CODIGO =133), US_CODIGO = 0	
GO
	
/* Tabla R_VALOR:  Versi�n y Usuario( PRO Patch RDD  )*/
/* Patch 410 # Seq: 88 Agregado */ 
alter table R_VALOR add VL_VERSION FolioChico, US_CODIGO Usuario;
GO

/* Tabla R_VALOR: Inicializaci�n de VL_VERSION( PRO Patch RDD  )*/
/* Patch 410 # Seq: 89 Agregado */ 
update R_VALOR SET VL_VERSION = (select GL_FORMULA from GLOBAL where GL_CODIGO =133), US_CODIGO = 0	
GO


/**************************************************/
/*********** SUG: 753 - Vencimiento de Contrato ***/
/**************************************************/

/* Tabla COLABORA: 'Fecha de vencimiento de contrato'*/
/* Patch 410 # Seq: 90 Agregado */
alter table COLABORA add CB_FEC_COV Fecha
GO

/* Tabla COLABORA: Inicializar Fecha de vencimiento de contrato 1 de 2 */
/* Patch 410 # Seq: 91 Agregado */
update COLABORA set CB_FEC_COV = '12/30/1899' where ( CB_FEC_COV is NULL )
GO

/* Tabla COLABORA: Inicializar Fecha de vencimiento de contrato 2 de 2 */
/* Patch 410 # Seq: 92 Agregado */
update COLABORA
set CB_FEC_COV = ( CB_FEC_CON + TB_DIAS - 1 )
from COLABORA
left outer join CONTRATO on CONTRATO.TB_CODIGO = COLABORA.CB_CONTRAT
where CONTRATO.TB_DIAS > 0
GO

/* Tabla KARDEX: 'Fecha de vencimiento de contrato'*/
/* Patch 410 # Seq: 93 Agregado */
alter table KARDEX add CB_FEC_COV Fecha
GO

/* Tabla KARDEX: Inicializar Fecha de vencimiento de contrato 1 de 2 */
/* Patch 410 # Seq: 94 Agregado */
update KARDEX set CB_FEC_COV = '12/30/1899' where ( CB_FEC_COV is NULL )
GO

/* Tabla KARDEX: Inicializar Fecha de vencimiento de contrato 2 de 2 */
/* Patch 410 # Seq: 95 Agregado */
update KARDEX
set CB_FEC_COV = ( CB_FEC_CON + TB_DIAS - 1 )
from KARDEX
left outer join CONTRATO on CONTRATO.TB_CODIGO = KARDEX.CB_CONTRAT
where CONTRATO.TB_DIAS > 0
GO

/* View R_DATOSDEF: Incluir nuevos campos( PRO Patch RDD  )*/
/* Patch 410 # Seq: 96 Agregado */ 
create VIEW R_DATOSDEF( EN_CODIGO, RD_ORDEN , RD_ENTIDAD, AT_CAMPO, RD_VERSION, US_CODIGO )
AS
select EN_CODIGO, RD_ORDEN, RD_ENTIDAD, AT_CAMPO, RD_VERSION,US_CODIGO from r_default
union 
select EN_CODIGO, RO_ORDEN, RO_ENTIDAD, AT_CAMPO, RO_VERSION,US_CODIGO from r_orden
union 
select EN_CODIGO, RF_ORDEN, RF_ENTIDAD, AT_CAMPO, RF_VERSION,US_CODIGO from r_filtro
GO


/* V_ACUMULA:  View a Acumula por meses ( Portal ) */
/* Patch 410 # Seq: 97 Agregado */
CREATE VIEW V_ACUMULA (
  CB_CODIGO,
  CO_NUMERO,
  AC_YEAR,
  AC_MES,
  AC_MONTO
   ) as
select CB_CODIGO, CO_NUMERO, AC_YEAR, 1, AC_MES_01
from ACUMULA
union
select CB_CODIGO, CO_NUMERO, AC_YEAR, 2, AC_MES_02
from ACUMULA
union
select CB_CODIGO, CO_NUMERO, AC_YEAR, 3, AC_MES_03
from ACUMULA
union
select CB_CODIGO, CO_NUMERO, AC_YEAR, 4, AC_MES_04
from ACUMULA
union
select CB_CODIGO, CO_NUMERO, AC_YEAR, 5, AC_MES_05
from ACUMULA
union
select CB_CODIGO, CO_NUMERO, AC_YEAR, 6, AC_MES_06
from ACUMULA
union
select CB_CODIGO, CO_NUMERO, AC_YEAR, 7, AC_MES_07
from ACUMULA
union
select CB_CODIGO, CO_NUMERO, AC_YEAR, 8, AC_MES_08
from ACUMULA
union
select CB_CODIGO, CO_NUMERO, AC_YEAR, 9, AC_MES_09
from ACUMULA
union
select CB_CODIGO, CO_NUMERO, AC_YEAR, 10, AC_MES_10
from ACUMULA
union
select CB_CODIGO, CO_NUMERO, AC_YEAR, 11, AC_MES_11
from ACUMULA
union
select CB_CODIGO, CO_NUMERO, AC_YEAR, 12, AC_MES_12
from ACUMULA
union
select CB_CODIGO, CO_NUMERO, AC_YEAR, 13, AC_MES_13
from ACUMULA
GO

/**************************************************/
/*********** Mejoras Fonacot ***************** ***/
/**************************************************/

/* PRESTAMO: Observaciones (PRO - Mejoras Fonacot)*/
/* Patch 410 # Seq: 98 Agregado */
ALTER TABLE PRESTAMO ADD PR_OBSERVA ReferenciaLarga
GO

/* PRESTAMO: Inicializaci�n de PR_OBSERVA (PRO - Mejoras Fonacot)*/
/* Patch 410 # Seq: 99 Agregado */
UPDATE PRESTAMO set PR_OBSERVA = '' where ( PR_OBSERVA is Null )
GO

/* FON_TOT: Detalle de c�lculo (PRO - Mejoras Fonacot)*/
/* Patch 410 # Seq: 100 Agregado */
ALTER TABLE FON_TOT ADD FT_DETALLE MEMO
GO

/* FON_TOT: Inicializaci�n de FT_DETALLE (PRO - Mejoras Fonacot)*/
/* Patch 410 # Seq: 101 Agregado */
UPDATE FON_TOT SET FT_DETALLE = '' WHERE ( FT_DETALLE is Null )
GO


/**************************************************/
/*** PRO - GE Nuevos Campos Adicionales ************/
/**************************************************/

/* COLABORA: Campo Adicional Texto #20 ( PRO - GE Nuevos Campos Adicionales )*/
/* Patch 410 # Seq: 102 Agregado */
alter table COLABORA add CB_G_TEX20 Descripcion
go

/* COLABORA: Inicializar CB_G_TEXT20  ( PRO - GE Nuevos Campos Adicionales )*/
/* Patch 410 # Seq: 103 Agregado */
update COLABORA set CB_G_TEX20 = '' where ( CB_G_TEX20 is NULL )
GO

/* COLABORA: Campo Adicional Texto #21 ( PRO - GE Nuevos Campos Adicionales )*/
/* Patch 410 # Seq: 104 Agregado */
alter table COLABORA add CB_G_TEX21 Descripcion
go

/* COLABORA: Inicializar CB_G_TEXT21  ( PRO - GE Nuevos Campos Adicionales )*/
/* Patch 410 # Seq: 105 Agregado */
update COLABORA set CB_G_TEX21 = '' where ( CB_G_TEX21 is NULL )
GO

/* COLABORA: Campo Adicional Texto #22 ( PRO - GE Nuevos Campos Adicionales )*/
/* Patch 410 # Seq: 106 Agregado */
alter table COLABORA add CB_G_TEX22 Descripcion
go

/* COLABORA: Inicializar CB_G_TEXT22  ( PRO - GE Nuevos Campos Adicionales )*/
/* Patch 410 # Seq: 107 Agregado */
update COLABORA set CB_G_TEX22 = '' where ( CB_G_TEX22 is NULL )
GO

/* COLABORA: Campo Adicional Texto #23 ( PRO - GE Nuevos Campos Adicionales )*/
/* Patch 410 # Seq: 108 Agregado */
alter table COLABORA add CB_G_TEX23 Descripcion
go

/* COLABORA: Inicializar CB_G_TEXT23  ( PRO - GE Nuevos Campos Adicionales )*/
/* Patch 410 # Seq: 109 Agregado */
update COLABORA set CB_G_TEX23 = '' where ( CB_G_TEX23 is NULL )
GO

/* COLABORA: Campo Adicional Texto #24 ( PRO - GE Nuevos Campos Adicionales )*/
/* Patch 410 # Seq: 110 Agregado */
alter table COLABORA add CB_G_TEX24 Descripcion
go

/* COLABORA: Inicializar CB_G_TEXT24  ( PRO - GE Nuevos Campos Adicionales )*/
/* Patch 410 # Seq: 111 Agregado */
update COLABORA set CB_G_TEX24 = '' where ( CB_G_TEX24 is NULL )
GO

/* CREA_CAMPOS_EXPEDIEN: Crea campos de Expediente M�dicos EX_G_TEXT# (1/3)  ( PRO SUG #12 Agregar mas campos al expediente m�dico )*/
/* Patch 410 # Seq: 112 Agregado */
CREATE PROCEDURE CREA_CAMPOS_EXPEDIEN
AS
BEGIN
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_TEX01 Formula;						
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_TEX02 Formula; 	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_TEX03 Formula; 	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_TEX04 Formula; 	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_TEX05 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_TEX06 Formula; 	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_TEX07 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_TEX08 Formula; 	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_TEX09 Formula; 	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_TEX10 Formula; 	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_TEX11 Formula; 	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_TEX12 Formula; 	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_TEX13 Formula; 	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_TEX14 Formula; 	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_TEX15 Formula; 	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_TEX16 Formula; 	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_TEX17 Formula; 	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_TEX18 Formula; 	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_TEX19 Formula; 	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_TEX20 Formula; 	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_TEX21 Formula; 	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_TEX22 Formula; 	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_TEX23 Formula; 	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_TEX24 Formula; 	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_TEX25 Formula; 	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_TEX26 Formula; 	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_TEX27 Formula; 	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_TEX28 Formula; 	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_TEX29 Formula; 	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_TEX30 Formula; 
	
	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES01 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES02 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES03 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES04 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES05 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES06 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES07 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES08 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES09 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES10 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES11 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES12 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES13 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES14 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES15 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES16 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES17 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES18 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES19 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES20 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES21 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES22 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES23 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES24 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES25 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES26 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES27 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES28 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES29 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES30 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES31 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES32 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES33 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES34 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES35 Formula;
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES36 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES37 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES38 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES39 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES40 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES41 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES42 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES43 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES44 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES45 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES46 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES47 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES48 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES49 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES50 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES51 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES52 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES53 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES54 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES55 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES56 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES57 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES58 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES59 Formula;	
	ALTER TABLE EXPEDIEN ALTER COLUMN EX_G_DES60 Formula;
	
	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX31 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX32 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX33 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX34 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX35 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX36 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX37 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX38 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX39 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX40 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX41 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX42 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX43 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX44 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX45 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX46 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX47 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX48 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX49 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX50 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX51 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX52 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX53 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX54 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX55 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX56 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX57 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX58 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX59 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX60 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX61 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX62 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX63 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX64 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX65 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX66 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX67 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX68 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX69 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX70 Formula;	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX71 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX72 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX73 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX74 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX75 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX76 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX77 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX78 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX79 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX80 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX81 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX82 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX83 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX84 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX85 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX86 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX87 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX88 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX89 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX90 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX91 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX92 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX93 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX94 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX95 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX96 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX97 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX98 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_TEX99 Formula;


	ALTER TABLE EXPEDIEN ADD EX_G_LOG61 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG62 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG63 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG64 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG65 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG66 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG67 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG68 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG69 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG70 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG71 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG72 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG73 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG74 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG75 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG76 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG77 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG78 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG79 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG80 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG81 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG82 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG83 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG84 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG85 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG86 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG87 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG88 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG89 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG90 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG91 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG92 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG93 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG94 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG95 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG96 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG97 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG98 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_LOG99 Booleano;

	ALTER TABLE EXPEDIEN ADD EX_G_BOL61 Booleano;
	ALTER TABLE EXPEDIEN ADD EX_G_BOL62 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL63 Booleano;
	ALTER TABLE EXPEDIEN ADD EX_G_BOL64 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL65 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL66 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL67 Booleano; 
	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL68 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL69 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL70 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL71 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL72 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL73 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL74 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL75 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL76 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL77 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL78 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL79 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL80 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL81 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL82 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL83 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL84 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL85 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL86 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL87 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL88 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL89 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL90 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL91 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL92 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL93 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL94 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL95 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL96 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL97 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL98 Booleano; 	
	ALTER TABLE EXPEDIEN ADD EX_G_BOL99 Booleano; 	

	ALTER TABLE EXPEDIEN ADD EX_G_DES61 Formula;
	ALTER TABLE EXPEDIEN ADD EX_G_DES62 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES63 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES64 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES65 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES66 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES67 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES68 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES69 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES70 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES71 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES72 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES73 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES74 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES75 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES76 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES77 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES78 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES79 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES80 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES81 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES82 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES83 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES84 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES85 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES86 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES87 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES88 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES89 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES90 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES91 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES92 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES93 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES94 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES95 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES96 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES97 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES98 Formula; 	
	ALTER TABLE EXPEDIEN ADD EX_G_DES99 Formula; 	


	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX31', 'Texto # 31', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX32', 'Texto # 32', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX33', 'Texto # 33', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX34', 'Texto # 34', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX35', 'Texto # 35', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX36', 'Texto # 36', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX37', 'Texto # 37', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX38', 'Texto # 38', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX39', 'Texto # 39', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX40', 'Texto # 40', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX41', 'Texto # 41', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX42', 'Texto # 42', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX43', 'Texto # 43', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX44', 'Texto # 44', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX45', 'Texto # 45', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX46', 'Texto # 46', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX47', 'Texto # 47', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX48', 'Texto # 48', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX49', 'Texto # 49', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX50', 'Texto # 50', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX51', 'Texto # 51', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX52', 'Texto # 52', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX53', 'Texto # 53', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX54', 'Texto # 54', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX55', 'Texto # 55', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX56', 'Texto # 56', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX57', 'Texto # 57', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX58', 'Texto # 58', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX59', 'Texto # 59', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX60', 'Texto # 60', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX61', 'Texto # 61', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX62', 'Texto # 62', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX63', 'Texto # 63', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX64', 'Texto # 64', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX65', 'Texto # 65', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX66', 'Texto # 66', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX67', 'Texto # 67', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX68', 'Texto # 68', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX69', 'Texto # 69', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX70', 'Texto # 70', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX71', 'Texto # 71', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX72', 'Texto # 72', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX73', 'Texto # 73', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX74', 'Texto # 74', 0 );	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX75', 'Texto # 75', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX76', 'Texto # 76', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX77', 'Texto # 77', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX78', 'Texto # 78', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX79', 'Texto # 79', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX80', 'Texto # 80', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES    ( 'EX_G_TEX81', 'Texto # 81', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX82', 'Texto # 82', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX83', 'Texto # 83', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX84', 'Texto # 84', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX85', 'Texto # 85', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX86', 'Texto # 86', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX87', 'Texto # 87', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX88', 'Texto # 88', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX89', 'Texto # 89', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX90', 'Texto # 80', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX91', 'Texto # 91', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX92', 'Texto # 92', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX93', 'Texto # 93', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX94', 'Texto # 94', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX95', 'Texto # 95', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES ( 'EX_G_TEX96', 'Texto # 96', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX97', 'Texto # 97', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX98', 'Texto # 98', 0 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_TEX99', 'Texto # 99', 0 ); 	

	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG61', 'Si/No # 61', 2 );
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG62', 'Si/No # 62', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG63', 'Si/No # 63', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG64', 'Si/No # 64', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG65', 'Si/No # 65', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG66', 'Si/No # 66', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG67', 'Si/No # 67', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG68', 'Si/No # 68', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG69', 'Si/No # 69', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG70', 'Si/No # 70', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG71', 'Si/No # 71', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG72', 'Si/No # 72', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG73', 'Si/No # 73', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG74', 'Si/No # 74', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG75', 'Si/No # 75', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG76', 'Si/No # 76', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG77', 'Si/No # 77', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG78', 'Si/No # 78', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG79', 'Si/No # 79', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG80', 'Si/No # 80', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG81', 'Si/No # 81', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG82', 'Si/No # 82', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG83', 'Si/No # 83', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG84', 'Si/No # 84', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG85', 'Si/No # 85', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG86', 'Si/No # 86', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG87', 'Si/No # 87', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG88', 'Si/No # 88', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG89', 'Si/No # 89', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG90', 'Si/No # 90', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG91', 'Si/No # 91', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG92', 'Si/No # 92', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG93', 'Si/No # 93', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG94', 'Si/No # 94', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG95', 'Si/No # 95', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG96', 'Si/No # 96', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG97', 'Si/No # 97', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG98', 'Si/No # 98', 2 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_LOG99', 'Si/No # 99', 2 ); 	

	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL61', 'Si/No/Comentario # 61', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL62', 'Si/No/Comentario # 62', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL63', 'Si/No/Comentario # 63', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL64', 'Si/No/Comentario # 64', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL65', 'Si/No/Comentario # 65', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL66', 'Si/No/Comentario # 66', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL67', 'Si/No/Comentario # 67', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL68', 'Si/No/Comentario # 68', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL69', 'Si/No/Comentario # 69', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL70', 'Si/No/Comentario # 70', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL71', 'Si/No/Comentario # 71', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL72', 'Si/No/Comentario # 72', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL73', 'Si/No/Comentario # 73', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL74', 'Si/No/Comentario # 74', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL75', 'Si/No/Comentario # 75', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL76', 'Si/No/Comentario # 76', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL77', 'Si/No/Comentario # 77', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL78', 'Si/No/Comentario # 78', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL79', 'Si/No/Comentario # 79', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL80', 'Si/No/Comentario # 80', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL81', 'Si/No/Comentario # 81', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL82', 'Si/No/Comentario # 82', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL83', 'Si/No/Comentario # 83', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL84', 'Si/No/Comentario # 84', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL85', 'Si/No/Comentario # 85', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL86', 'Si/No/Comentario # 86', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL87', 'Si/No/Comentario # 87', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL88', 'Si/No/Comentario # 88', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL89', 'Si/No/Comentario # 89', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL90', 'Si/No/Comentario # 90', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL91', 'Si/No/Comentario # 91', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL92', 'Si/No/Comentario # 92', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL93', 'Si/No/Comentario # 93', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL94', 'Si/No/Comentario # 94', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL95', 'Si/No/Comentario # 95', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL96', 'Si/No/Comentario # 96', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL97', 'Si/No/Comentario # 97', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL98', 'Si/No/Comentario # 98', 3 ); 	
	INSERT INTO CAMPO_EX ( CX_NOMBRE, CX_TITULO, CX_TIPO ) VALUES	( 'EX_G_BOL99', 'Si/No/Comentario # 99', 3 ); 	


END
GO


/* CREA_CAMPOS_EXPEDIEN: Crea campos de Expediente M�dicos EX_G_TEXT# (2/3)  ( PRO SUG #12 Agregar mas campos al expediente m�dico )*/
/* Patch 410 # Seq: 113 Agregado */
EXECUTE CREA_CAMPOS_EXPEDIEN
GO 

/* CREA_CAMPOS_EXPEDIEN: Crea campos de Expediente M�dicos EX_G_TEXT# (3/3)  ( PRO SUG #12 Agregar mas campos al expediente m�dico )*/
/* Patch 410 # Seq: 114 Agregado */
DROP PROCEDURE CREA_CAMPOS_EXPEDIEN
GO


/* V_BITCAFE: Bit�cora completa de Cafeteria. Se muestra la bit�coras de todas las empresas (SUG # 1028 Bitacora de cafeteria en la Base de Datos)*/
/* Patch 410 # Seq: 115 Agregado */
CREATE VIEW V_BITCAFE(
       BC_FOLIO,
       CB_CODIGO,
       IV_CODIGO,
       BC_EMPRESA, 
       BC_CREDENC, 
       BC_FECHA,
       BC_HORA,     
       BC_RELOJ,    
       BC_TIPO,  
       BC_COMIDAS,  
       BC_EXTRAS,  
       BC_REG_EXT,  
       BC_MANUAL,  
       CL_CODIGO,  
       BC_TGAFETE,
       BC_TIEMPO,
       BC_STATUS,
       BC_MENSAJE,
      BC_RESPUES,
      BC_CHECADA      
      )
as
 SELECT   BC_FOLIO, 
    CB_CODIGO,   
    IV_CODIGO, 
    BC_EMPRESA, 
    BC_CREDENC,  
    BC_FECHA, 
    BC_HORA,
    BC_RELOJ,
    BC_TIPO,
    BC_COMIDAS,
    BC_EXTRAS,
    BC_REG_EXT,
    BC_MANUAL,
    CL_CODIGO,
       BC_TGAFETE,
    BC_TIEMPO,
    BC_STATUS,
    BC_MENSAJE,
    BC_RESPUES,
    BC_CHECADA

 FROM #COMPARTE.dbo.BITCAFE
GO

/* V_BITCAFEE: View para consultar la bit�cora Cafeteria de la empresa en la que se est� firmado. */
/* Patch 410 # Seq: 116 Agregado */
CREATE VIEW V_BITCAFEE(
       BC_FOLIO, 
       CB_CODIGO,   
       IV_CODIGO, 
       BC_EMPRESA, 
       BC_CREDENC, 
       BC_FECHA,
       BC_HORA,     
       BC_RELOJ,    
       BC_TIPO,  
       BC_COMIDAS,  
       BC_EXTRAS,  
       BC_REG_EXT,  
       BC_MANUAL,  
       CL_CODIGO,  
       BC_TGAFETE,  
       BC_TIEMPO,
       BC_STATUS,   
       BC_MENSAJE,
      BC_RESPUES,
      BC_CHECADA       
      )
as
 SELECT   BC_FOLIO, 
    CB_CODIGO,   
    IV_CODIGO, 
    BC_EMPRESA,
    BC_CREDENC,  
    BC_FECHA, 
    BC_HORA,
    BC_RELOJ,
    BC_TIPO,
    BC_COMIDAS,
    BC_EXTRAS,
    BC_REG_EXT,
    BC_MANUAL,
    CL_CODIGO,
    BC_TGAFETE,
    BC_TIEMPO,
    BC_STATUS,
    BC_MENSAJE,
    BC_RESPUES,
    BC_CHECADA     
 
 FROM #COMPARTE.dbo.BITCAFE BITCAFE
 WHERE BITCAFE.BC_EMPRESA = #DIGITO
GO

/* ADICIONAL_INIT2: Considerar nuevos campos texto adicionales del 20-24 ( 1/3) */
/* Patch 410 # Seq: 117 Agregado */
CREATE PROCEDURE DBO.ADICIONAL_INIT2
as
begin
     SET NOCOUNT ON;
     BEGIN TRANSACTION TX1
     
     IF NOT EXISTS ( select * from CAMPO_AD where CX_NOMBRE = 'CB_G_TEX20' )
     	INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX20', 0 )

     IF NOT EXISTS ( select * from CAMPO_AD where CX_NOMBRE = 'CB_G_TEX21' )
        INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX21', 0 )

     IF NOT EXISTS ( select * from CAMPO_AD where CX_NOMBRE = 'CB_G_TEX22' )
     	INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX22', 0 )

     IF NOT EXISTS ( select * from CAMPO_AD where CX_NOMBRE = 'CB_G_TEX23' )
     	INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX23', 0 )

	IF NOT EXISTS ( select * from CAMPO_AD where CX_NOMBRE = 'CB_G_TEX24' )
     	INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX24', 0 )

     COMMIT TRANSACTION TX1
end
GO

/* ADICIONAL_INIT2: Considerar nuevos campos texto adicionales del 20-24 ( 2/3) */
/* Patch 410 # Seq: 118 Agregado */
exec DBO.ADICIONAL_INIT2
GO
/* ADICIONAL_INIT2: Considerar nuevos campos texto adicionales del 20-24 ( 3/3) */
/* Patch 410 # Seq: 119 Agregado */
drop procedure DBO.ADICIONAL_INIT2
GO

/* Tabla MOT_AUTO: 'Tipo de Autorizaci�n'  ( SUG #115 Liga de permitos y clase ) */
/* Patch 410 # Seq: 120 Agregado */
alter table MOT_AUTO add TB_TIPO Status
GO

/* Tabla MOT_AUTO: Inicializar Tipo de Autorizaci�n: Valor <Todos> ( SUG #115 Liga de permitos y clase )*/
/* Patch 410 # Seq: 121 Agregado */
update MOT_AUTO set TB_TIPO = -1 where ( TB_TIPO is NULL )
GO

/* Tabla INCIDEN: 'Clase de Permiso'  ( SUG #115 Liga de permitos y clase ) */
/* Patch 410 # Seq: 122 Agregado */
alter table INCIDEN add TB_PERMISO Status
GO

/* Tabla INCIDEN: Inicializar Clase de Permiso: Valor <Todos>  ( SUG #115 Liga de permitos y clase ) */
/* Patch 410 # Seq: 123 Agregado */
update INCIDEN set TB_PERMISO = -1 where ( TB_PERMISO is NULL )
GO


/**************************************************/
/******************INFONAVIT ***********************/
/**************************************************/

/* Tabla LIQ_EMP: Acumulado por n�mina ( PRO 856 - Conciliaci�n Infonavit)*/
/* Patch 410 # Seq: 124 Agregado */
ALTER TABLE LIQ_EMP ADD LE_ACUMULA Pesos
GO

/* Tabla LIQ_EMP: Descuento bimestre anterior ( PRO 856 - Conciliaci�n Infonavit )*/
/* Patch 410 # Seq: 125 Agregado */
ALTER TABLE LIQ_EMP ADD LE_BIM_ANT Pesos
GO

/* Tabla LIQ_EMP: Descuento siguiente bimestre ( PRO 856 - Conciliaci�n Infonavit )*/
/* Patch 410 # Seq: 126 Agregado */
ALTER TABLE LIQ_EMP ADD LE_BIM_SIG Pesos
GO

/* Tabla LIQ_EMP: Vacaciones Pagadas ( PRO 856 - Conciliaci�n Infonavit  )*/
/* Patch 410 # Seq: 127 Agregado */
ALTER TABLE LIQ_EMP ADD LE_VA_PAGO Dias
GO

/* Tabla LIQ_EMP: Vacaciones Gozadas ( PRO 856 - Conciliaci�n Infonavit)*/
/* Patch 410 # Seq: 128 Agregado */
ALTER TABLE LIQ_EMP ADD LE_VA_GOZO Dias
GO

/* Tabla LIQ_EMP: Inicializa acumulado por n�mina ( PRO 856 - Conciliaci�n Infonavit)*/
/* Patch 410 # Seq: 129 Agregado */
UPDATE LIQ_EMP SET LE_ACUMULA = 0.00 where ( LE_ACUMULA is Null )
GO

/* Tabla LIQ_EMP: Inicializa Descuento bimestre anterior ( PRO 856 - Conciliaci�n Infonavit)*/
/* Patch 410 # Seq: 130 Agregado */
UPDATE LIQ_EMP SET LE_BIM_ANT = 0.00 where ( LE_BIM_ANT is Null )
GO

/* Tabla LIQ_EMP: Inicializa Descuento siguiente bimestre ( PRO 856 - Conciliaci�n Infonavit)*/
/* Patch 410 # Seq: 131 Agregado */
UPDATE LIQ_EMP SET LE_BIM_SIG = 0.00 where ( LE_BIM_SIG is Null )
GO

/* Tabla LIQ_EMP: Inicializa Vacaciones Pagadas ( PRO 856 - Conciliaci�n Infonavit)*/
/* Patch 410 # Seq: 132 Agregado */
UPDATE LIQ_EMP SET LE_VA_PAGO = 0 where ( LE_VA_PAGO is Null )
GO

/* Tabla LIQ_EMP: Inicializa Vacaciones Gozadas ( PRO 856 - Conciliaci�n Infonavit)*/
/* Patch 410 # Seq: 133 Agregado */
UPDATE LIQ_EMP SET LE_VA_GOZO = 0 where ( LE_VA_GOZO is Null )
GO

/* Tabla LIQ_EMP: Provisi�n de vacaciones( PRO 856 - Conciliaci�n Infonavit)*/
/* Patch 410 # Seq: 134 Agregado */
ALTER TABLE LIQ_EMP ADD LE_PROV Pesos
GO


/* Tabla LIQ_EMP: Provisi�n de vacaciones ( PRO 856 - Conciliaci�n Infonavit )*/
/* Patch 410 # Seq: 135 Agregado */
UPDATE LIQ_EMP SET LE_PROV = 0 where ( LE_PROV is Null )
GO

/* Tabla V_LIQ_EMP: Provisi�n de vacaciones ( PRO 856 - Conciliaci�n Infonavit )*/
/* Patch 410 # Seq: 136 Agregado */
CREATE VIEW V_LIQ_EMP
(
	 LS_YEAR,
	 LS_MONTH,
      LE_ACUMULA,
      LE_PROV,
      LE_VA_GOZO,
      LE_VA_PAGO,
	 LE_INF_PAT,
	 LE_INF_AMO,
	 CB_CODIGO,
	 LE_TOT_INF,
	LE_DIAS_BM,
     LE_BIM_SIG,
	LE_BIM_ANT
)
AS
SELECT
	LS_YEAR LS_YEAR,
	LS_MONTH LS_MONTH,
     MAX( LE_ACUMULA) LE_ACUMULA,
	MAX( LE_PROV ) LE_PROV,
	MAX( LE_VA_GOZO ) LE_VA_GOZO,
	MAX( LE_VA_PAGO ) LE_VA_PAGO,
	SUM(LE_INF_PAT) LE_INF_PAT,
	SUM(LE_INF_AMO) LE_INF_AMO,
	CB_CODIGO CB_CODIGO,
	SUM(LE_TOT_INF) LE_TOT_INF,
	SUM(LE_DIAS_BM) LE_DIAS_BM,
     SUM(LE_BIM_SIG) LE_BIM_SIG,
     SUM(LE_BIM_ANT) LE_BIM_ANT
FROM LIQ_EMP
GROUP BY CB_CODIGO, LS_YEAR, LS_MONTH
GO


/* Init Global 230: Conceptos de Infonavit */
/* Patch 410 # Seq: 137 Agregado */
if not EXISTS ( select * from global where gl_codigo = 230 ) 
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA ) values ( 230, 'Conceptos de Infonavit', '54,55' )
GO

/* Init Global 232: Tipo de Comparaci�n de Ajuste Infonavit */
/* Patch 410 # Seq: 138 Agregado */
if not EXISTS( select * from global where gl_codigo = 232 )
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA ) values ( 232, 'T. Comparaci�n Ajus. Infonavit', 0 )
GO

/* KARINF: Kardex de Infonavit ( SUG 904 - Historial Infonavit ) */
/* Patch 410 # Seq: 139 Agregado */
CREATE TABLE KARINF( 
	  CB_CODIGO NumeroEmpleado, /* N�mero de Empleado */
  	  KI_FECHA Fecha, /*Fecha de Movmiento*/
       KI_TIPO Status, /* Tipo de Movimiento */
	  CB_INFTIPO Status,  /* Tipo de pr�stamo */
	  CB_INFCRED Descripcion, /* No. de cr�dito de Infonavit */
	  CB_INFTASA DescINFO,  /* Amortizaci�n Infonavit */
	  CB_INFDISM  Booleano, /* Aplica Tabla de Disminuci�n de %?*/
	  KI_COMENTA Observaciones, /* Observaciones */
	  US_CODIGO Usuario, /* Usuario que modific� */
	  KI_CAPTURA Fecha /* Fecha de captura */
)
GO


/* KARINF: Llave Primaria ( SUG 904 - Historial Infonavit ) */
/* Patch 410 # Seq: 140 Agregado */
ALTER TABLE KARINF
       ADD CONSTRAINT PK_KARINF PRIMARY KEY (CB_CODIGO, KI_FECHA, KI_TIPO)
go


/* KARINF: Llave for�nea ( SUG 904 - Historial Infonavit ) */
/* Patch 410 # Seq: 141 Agregado */
ALTER TABLE KARINF
       ADD CONSTRAINT FK_KINF_Colabora
       FOREIGN KEY (CB_CODIGO)
       REFERENCES COLABORA
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

/* COLABORA: Aplica disminuci�n de porcentaje? ( SUG 904 - Historial de Infonavit )*/
/* Patch 410 # Seq: 142 Agregado */
ALTER TABLE COLABORA ADD CB_INFDISM Booleano
GO

/* Aplica disminuci�n de porcentaje si tiene tasa anterior ( SUG 904 - Historial de Infonavit ) */
/* Patch 410 # Seq: 143 Agregado */
UPDATE COLABORA SET CB_INFDISM = 'N'
GO


/* Aplica disminuci�n de porcentaje si tiene tasa anterior ( SUG 904 - Historial de Infonavit ) */
/* Patch 410 # Seq: 144 Agregado */
UPDATE COLABORA SET CB_INFDISM = 'S' where ( CB_INF_OLD <> 0 ) AND ( CB_INFTIPO = 1 )
GO


/* Inicializaci�n TASA ANTERIOR ( SUG 904 - Historial de Infonavit )*/
/* Patch 410 # Seq: 145 Agregado */
UPDATE COLABORA SET CB_INFTASA = CB_INF_OLD WHERE ( CB_INF_OLD <> 0 ) AND ( CB_INFTIPO = 1 )
GO

/* COLABORA: Cr�dito de Infonavit Activo? ( SUG 904 - Historial de Infonavit )*/
/* Patch 410 # Seq: 146 Agregado */
ALTER TABLE COLABORA ADD CB_INFACT Booleano
GO

/* Inicializaci�n CB_INFACT ( SUG 904 - Historial de Infonavit ) */
/* Patch 410 # Seq: 147 Agregado */
UPDATE COLABORA SET CB_INFACT = 'N'
GO

/* El cr�dito esta activo si el empleado tiene cr�dito de INFONAVIT ( SUG 904 - Historial de Infonavit ) */
/* Patch 410 # Seq: 148 Agregado */
UPDATE COLABORA SET CB_INFACT = 'S' where ( CB_INFTIPO <> 0 ) AND ( CB_INFCRED <> '' ) AND ( CB_INFTASA > 0 )
GO

/* AGREGA_HISTORIAL_INF: Inicializa Historial de Infonvait ( SUG 904 - Historial Infonvait ) ( 1/3)*/
/* Patch 410 # Seq: 149 Agregado */
CREATE PROCEDURE AGREGA_HISTORIAL_INF
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @CB_CODIGO NumeroEmpleado
 	DECLARE @CB_INFTIPO Status
	DECLARE @CB_INFCRED Descripcion
	DECLARE @CB_INFTASA DescINFO
	DECLARE @CB_INF_OLD DescINFO
	DECLARE @CB_INF_INI Fecha
	DECLARE @CB_INFDISM Booleano

	DECLARE @FechaActual Fecha
	set @FechaActual = GETDATE()

	
	
	/* Nos trae los empleados con cr�dito INFONAVIT */
	DECLARE EmpleadosInfonavit CURSOR FOR
     SELECT CB_CODIGO, CB_INFTIPO, CB_INFCRED, CB_INFTASA, CB_INF_OLD, CB_INF_INI, CB_INFDISM from COLABORA where 
		( CB_INFACT = 'S' ) order by CB_CODIGO
          open EmpleadosInfonavit;

     fetch NEXT from EmpleadosInfonavit into @CB_CODIGO, @CB_INFTIPO, @CB_INFCRED, @CB_INFTASA, @CB_INF_OLD, @CB_INF_INI, @CB_INFDISM;

	while ( @@Fetch_Status = 0 )
     begin 
		/* Esta condici�n es para protegernos de errores al correr el patch por 2da vez*/
		IF NOT EXISTS ( SELECT CB_CODIGO FROM KARINF WHERE ( CB_CODIGO = @CB_CODIGO ) ) 
		BEGIN
		

     		INSERT INTO KARINF( CB_CODIGO, CB_INFTIPO, CB_INFCRED, CB_INFTASA, KI_FECHA,
					CB_INFDISM, KI_TIPO, KI_COMENTA, US_CODIGO, KI_CAPTURA )
			VALUES(@CB_CODIGO,@CB_INFTIPO,@CB_INFCRED,@CB_INFTASA, @CB_INF_INI,
			   @CB_INFDISM,0,'Inicializaci�n de cr�dito Infonavit',0,@FechaActual ) 
	
		END
		fetch NEXT from EmpleadosInfonavit 
			 		into @CB_CODIGO, 
						@CB_INFTIPO, 
						@CB_INFCRED, 
						@CB_INFTASA, 
						@CB_INF_OLD, 
						@CB_INF_INI,
						@CB_INFDISM;
     end
	close EmpleadosInfonavit;
	deallocate EmpleadosInfonavit;
END
GO

/* AGREGA_HISTORIAL_INF: Ejecuta Procedure ( SUG 904 - Historial Infonvait ) ( 2/3)*/
/* Patch 410 # Seq: 150 Agregado */
exec AGREGA_HISTORIAL_INF
GO

/* AGREGA_HISTORIAL_INF: Elimina Procedure ( SUG 904 - Historial Infonvait ) ( 3/3)*/
/* Patch 410 # Seq: 151 Agregado */
drop procedure AGREGA_HISTORIAL_INF
GO

/* Tabla RPATRON: Campos TB_CALLE, TB_NUMEXT, TB_NUMINT, TB_COLONIA, 
					TB_CIUDAD, TB_ENTIDAD, TB_CODPOST, TB_TEL, TB_FAX, TB_EMAIL, TB_WEB */
/* Patch 410 # Seq: 152 Agregado */
ALTER TABLE RPATRON ADD
	TB_CALLE Titulo,
	TB_NUMEXT Referencia,
	TB_NUMINT Referencia,
	TB_COLONIA Titulo,
	TB_CIUDAD Titulo,
	TB_ENTIDAD Codigo2,
	TB_CODPOST Referencia,
	TB_TEL Descripcion,
	TB_FAX Descripcion,
	TB_EMAIL DescLarga,
	TB_WEB PathURL
GO

/* Tabla RPATRON: Init TB_CALLE */
/* Patch 410 # Seq: 153 Agregado */
UPDATE RPATRON SET TB_CALLE = '' where ( TB_CALLE is Null )
GO

/* Tabla RPATRON: Init TB_NUMEXT */
/* Patch 410 # Seq: 154 Agregado */
UPDATE RPATRON SET TB_NUMEXT = '' where ( TB_NUMEXT is Null )
GO

/* Tabla RPATRON: Init TB_NUMINT */
/* Patch 410 # Seq: 155 Agregado */
UPDATE RPATRON SET TB_NUMINT = '' where ( TB_NUMINT is Null )
GO

/* Tabla RPATRON: Init TB_COLONIA */
/* Patch 410 # Seq: 156 Agregado */
UPDATE RPATRON SET TB_COLONIA = '' where ( TB_COLONIA is Null )
GO

/* Tabla RPATRON: Init TB_CIUDAD */
/* Patch 410 # Seq: 157 Agregado */
UPDATE RPATRON SET TB_CIUDAD = '' where ( TB_CIUDAD is Null )
GO


/* Tabla RPATRON: Init TB_ENTIDAD */
/* Patch 410 # Seq: 158 Agregado */
UPDATE RPATRON SET TB_ENTIDAD = '' where ( TB_ENTIDAD is Null )
GO

/* Tabla RPATRON: Init TB_CODPOST */
/* Patch 410 # Seq: 159 Agregado */
UPDATE RPATRON SET TB_CODPOST = '' where ( TB_CODPOST is Null )
GO

/* Tabla RPATRON: Init TB_TEL */
/* Patch 410 # Seq: 160 Agregado */
UPDATE RPATRON SET TB_TEL = '' where ( TB_TEL is Null )
GO

/* Tabla RPATRON: Init TB_FAX */
/* Patch 410 # Seq: 161 Agregado */
UPDATE RPATRON SET TB_FAX = '' where ( TB_FAX is Null )
GO

/* Tabla RPATRON: Init TB_EMAIL */
/* Patch 410 # Seq: 162 Agregado */
UPDATE RPATRON SET TB_EMAIL = '' where ( TB_EMAIL is Null )
GO


/* Tabla RPATRON: Init TB_WEB */
/* Patch 410 # Seq: 163 Agregado */
UPDATE RPATRON SET TB_WEB = '' where ( TB_WEB is Null )
GO


/* View CUR_PROG: Considerar el nuevo campo LLAVE */
/* Patch 410 # Seq: 164 Agregado */
alter view CUR_PROG (
  CB_CODIGO,
  CB_PUESTO,
  CU_CODIGO,
  KC_FEC_PRO,
  KC_EVALUA,
  KC_FEC_TOM,
  KC_HORAS,
  CU_HORAS,
  LLAVE,
  EN_OPCIONA,
  EN_LISTA,
  MA_CODIGO,
  KC_PROXIMO,
  KC_REVISIO,
  CU_REVISIO,
  CP_INDIVID ) as
select
  C.CB_CODIGO,
  C.CB_PUESTO,
  CURSO.CU_CODIGO,
  DATEADD( DAY, E.EN_DIAS, C.CB_FEC_ING ) KC_FEC_PRO,
  KARCURSO.KC_EVALUA,
  KARCURSO.KC_FEC_TOM,
  KARCURSO.KC_HORAS,
  CURSO.CU_HORAS,
  CURSO.LLAVE,
  E.EN_OPCIONA,
  E.EN_LISTA,
  CURSO.MA_CODIGO,
  ( select MIN( SESION.SE_FEC_INI ) from SESION where ( SESION.CU_CODIGO = E.CU_CODIGO ) and ( SESION.SE_FEC_INI >= cast( floor( cast( GetDate() as float ) ) as Datetime ))) KC_PROXIMO,
  KARCURSO.KC_REVISIO,
  CURSO.CU_REVISIO,
  cast( 'N' as CHAR(1) ) CP_INDIVID
from COLABORA C
join ENTRENA E on ( E.PU_CODIGO = C.CB_PUESTO )
left join KARCURSO on ( KARCURSO.CB_CODIGO = C.CB_CODIGO ) and ( KARCURSO.CU_CODIGO = E.CU_CODIGO )
left join CURSO on ( CURSO.CU_CODIGO = E.CU_CODIGO )
where ( CURSO.CU_ACTIVO = 'S' )
union
select
  C.CB_CODIGO,
  C.CB_PUESTO,
  CURSO.CU_CODIGO,
  DATEADD( DAY, EP.EP_DIAS, C.CB_FEC_ING ) KC_FEC_PRO,
  KARCURSO.KC_EVALUA,
  KARCURSO.KC_FEC_TOM,
  KARCURSO.KC_HORAS,
  CURSO.CU_HORAS,
  CURSO.LLAVE,
  EP.EP_OPCIONA,
  cast( 'N' as CHAR(1) ) EN_LISTA,
  CURSO.MA_CODIGO,
  ( select MIN( SESION.SE_FEC_INI ) from SESION where ( SESION.CU_CODIGO = EP.CU_CODIGO ) and ( SESION.SE_FEC_INI >= cast( floor( cast( GetDate() as float ) ) as Datetime ))) KC_PROXIMO,
  KARCURSO.KC_REVISIO,
  CURSO.CU_REVISIO,
  cast( 'S' as CHAR(1) ) CP_INDIVID
from COLABORA C
join EMP_PROG EP on ( EP.CB_CODIGO = C.CB_CODIGO )
left join KARCURSO on ( KARCURSO.CB_CODIGO = C.CB_CODIGO ) and ( KARCURSO.CU_CODIGO = EP.CU_CODIGO )
left join CURSO on ( CURSO.CU_CODIGO = EP.CU_CODIGO )
where ( CURSO.CU_ACTIVO = 'S' )
GO


/* SP_INSERT_CLASIFI: Inserta Clasificaciones ( 1/3)*/
/* Patch 410 # Seq: 165 Agregado */
CREATE PROCEDURE SP_INSERT_CLASIFI as
BEGIN
     SET NOCOUNT ON;
     DECLARE @Resultado int;
     select @Resultado = count(*) from R_CLASIFI where RC_CODIGO = 999;

     IF (@Resultado = 0 )
     begin
	insert into R_CLASIFI(RC_CODIGO, RC_NOMBRE, RC_ORDEN, RC_ACTIVO, RC_VERSION)
		values(999,'Tablas sin Clasificaci�n',999,'S',410)
     end
END

go

/* SP_INSERT_CLASIFI: Inserta Clasificaciones ( 2/3)*/
/* Patch 410 # Seq: 166 Agregado */
execute SP_INSERT_CLASIFI
go

/* SP_INSERT_CLASIFI: Inserta Clasificaciones ( 3/3)*/
/* Patch 410 # Seq: 167 Agregado */
drop procedure SP_INSERT_CLASIFI
go


/* Drop FK_R_ATRIBUTO_LISTAVAL: Drop del constraint ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 168 Agregado */ 
ALTER TABLE R_ATRIBUTO DROP CONSTRAINT FK_R_ATRIBUTO_LISTAVAL
GO

/* Drop FK_R_ENT_ACC: Drop del constraint ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 169 Agregado */ 
ALTER TABLE R_ENT_ACC DROP CONSTRAINT FK_R_ENT_ACC
GO

/* Drop FK_R_CLAS_ENT_ENTIDAD: Drop del constraint ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 170 Agregado */ 
ALTER TABLE R_CLAS_ENT DROP CONSTRAINT FK_R_CLAS_ENT_ENTIDAD
GO

/* Drop FK_R_MOD_ENT_ENTIDAD: Drop del constraint ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 171 Agregado */ 
ALTER TABLE R_MOD_ENT DROP CONSTRAINT FK_R_MOD_ENT_ENTIDAD
GO

/* Drop FK_R_ORDEN: Drop del constraint ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 172 Agregado */ 
ALTER TABLE R_ORDEN DROP CONSTRAINT FK_R_ORDEN
GO

/* Drop FK_R_FILTRO: Drop del constraint ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 173 Agregado */ 
ALTER TABLE R_FILTRO DROP CONSTRAINT FK_R_FILTRO
GO

/* Drop FK_R_DEFAULT_RD_ENTIDAD: Drop del constraint ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 174 Agregado */ 
ALTER TABLE R_DEFAULT DROP CONSTRAINT FK_R_DEFAULT_RD_ENTIDAD
GO

/* Drop FK_REPORTE_R_ENTIDAD: Drop del constraint ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 175 Agregado */ 
ALTER TABLE REPORTE DROP CONSTRAINT FK_REPORTE_R_ENTIDAD
GO

/* Tabla R_FILTRO: Constraint FK_R_FILTRO_R_ENTIDAD ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 176 Agregado */ 
ALTER TABLE R_FILTRO
       ADD CONSTRAINT FK_R_FILTRO_R_ENTIDAD
       FOREIGN KEY (EN_CODIGO)
       REFERENCES R_ENTIDAD
       ON DELETE CASCADE
       ON UPDATE CASCADE
GO


/* Tabla R_ORDEN: Constraint FK_R_ORDEN_R_ENTIDAD ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 177 Agregado */ 
ALTER TABLE R_ORDEN
       ADD CONSTRAINT FK_R_ORDEN_R_ENTIDAD
       FOREIGN KEY (EN_CODIGO)
       REFERENCES R_ENTIDAD
       ON DELETE CASCADE
       ON UPDATE CASCADE
GO

/* Tabla R_DEFAULT: Constraint FK_R_DEFAULT_R_ENTIDAD ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 178 Agregado */ 
ALTER TABLE R_DEFAULT
       ADD CONSTRAINT FK_R_DEFAULT_R_ENTIDAD
       FOREIGN KEY (EN_CODIGO)
       REFERENCES R_ENTIDAD
       ON DELETE CASCADE
       ON UPDATE CASCADE
GO

/* Tabla V_USUARIO: Considerar nuevos campos ( PRO Portal  )*/
/* Patch 410 # Seq: 179 Agregado */ 
ALTER VIEW V_USUARIO( US_CODIGO,
				  US_CORTO ,
 				  GR_CODIGO,
                      US_NIVEL ,
                      US_NOMBRE,
                      US_BLOQUEA,
                      US_CAMBIA,
                      US_FEC_IN,
                      US_FEC_OUT,
                      US_DENTRO,
                      US_ARBOL,
                      US_FORMA,
                      US_BIT_LST,
                      US_BIO_ID,
                      US_FEC_SUS,
                      US_FALLAS,
                      US_MAQUINA,
                      US_FEC_PWD,
                      US_EMAIL,
                      US_FORMATO,
                      US_LUGAR,
                      US_PAGINAS,
                      CM_CODIGO,
                      CB_CODIGO,
                      US_ANFITRI,
                      US_PORTAL,
                      US_ACTIVO,
                      US_DOMAIN,
                      US_JEFE
                               )

as

      SELECT      US_CODIGO,
                  US_CORTO ,
                  GR_CODIGO,
                  US_NIVEL ,
                  US_NOMBRE,
                  US_BLOQUEA,
                  US_CAMBIA ,
                  US_FEC_IN ,
                  US_FEC_OUT,
                  US_DENTRO ,
                  US_ARBOL  ,
                  US_FORMA  ,
                  US_BIT_LST,
                  US_BIO_ID ,
                  US_FEC_SUS,
                  US_FALLAS ,
                  US_MAQUINA,
                  US_FEC_PWD,
                  US_EMAIL  ,
                  US_FORMATO,
                  US_LUGAR  ,
                  US_PAGINAS,
                  CM_CODIGO ,
                  CB_CODIGO ,
                  US_ANFITRI,
                  US_PORTAL,
                  US_ACTIVO,
                  US_DOMAIN,
                  US_JEFE

      FROM #COMPARTE.dbo.USUARIO
GO

/* Tabla V_COMPANY: Considerar nuevos campos ( PRO Bit�cora de Kiosco  )*/
/* Patch 410 # Seq: 180 Agregado */ 
ALTER VIEW V_COMPANY(
					CM_CODIGO,
					CM_NOMBRE,
					CM_ACUMULA,
					CM_CONTROL,
					CM_EMPATE,
					CM_ALIAS,
					CM_USRNAME,
					CM_USACAFE,
					CM_NIVEL0,
					CM_DATOS,
					CM_DIGITO,
					CM_KCLASIFI
					 )
AS
SELECT
		CM_CODIGO,
		CM_NOMBRE,
		CM_ACUMULA,
		CM_CONTROL,
		CM_EMPATE,
		CM_ALIAS,
		CM_USRNAME,
		CM_USACAFE,
		CM_NIVEL0,
		CM_DATOS,
		CM_DIGITO,
		CM_KCLASIFI
FROM #Comparte.dbo.COMPANY
GO

/**************************************************/
/*********TRIGGERS ***********************/
/**************************************************/

/* TU_R_LISTAVAL: Trigger ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 89 Agregado */
CREATE TRIGGER TU_R_LISTAVAL ON R_LISTAVAL AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON;
     IF UPDATE ( LV_CODIGO )
     BEGIN
          declare @NewCodigo Codigo, @OldCodigo Codigo;
          select @NewCodigo = LV_CODIGO from Inserted;
          select @OldCodigo = LV_CODIGO from Deleted;
          UPDATE R_ATRIBUTO SET LV_CODIGO = @NewCodigo WHERE LV_CODIGO = @OldCodigo;
     END
END
GO

/* TU_R_LISTAVAL: Trigger ( PRO Patch RDD  )*/
/* Patch 410 # Seq: 90 Modificado */
ALTER TRIGGER TU_R_ENTIDAD ON R_ENTIDAD AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON;
     IF UPDATE( EN_CODIGO )
     BEGIN
          declare @NewCodigo FolioChico, @OldCodigo FolioChico;
          select @NewCodigo = EN_CODIGO from Inserted;
          select @OldCodigo = EN_CODIGO from Deleted;

	      update R_ENTIDAD set EN_ALIAS = @NewCodigo where (EN_ALIAS = @OldCodigo)
          update R_ATRIBUTO set AT_ENTIDAD = @NewCodigo where ( AT_ENTIDAD = @OldCodigo );
          update R_RELACION set RL_ENTIDAD = @NewCodigo where ( RL_ENTIDAD = @OldCodigo );
          update R_DEFAULT set EN_CODIGO = @NewCodigo where (EN_CODIGO = @OldCodigo );
          update R_ORDEN set EN_CODIGO = @NewCodigo where (EN_CODIGO = @OldCodigo );
          update R_FILTRO set EN_CODIGO = @NewCodigo where (EN_CODIGO = @OldCodigo );
          UPDATE R_ENT_ACC SET EN_CODIGO = @NewCodigo WHERE EN_CODIGO = @OldCodigo;
		  UPDATE R_CLAS_ENT SET EN_CODIGO = @NewCodigo WHERE EN_CODIGO = @OldCodigo;
		  UPDATE R_MOD_ENT SET EN_CODIGO = @NewCodigo WHERE EN_CODIGO = @OldCodigo;
 	      update CAMPOREP set CR_TABLA = @NewCodigo where ( CR_TABLA = @OldCodigo );
     END
END
GO


/**************************************************/
/*********Stored Procedures ***********************/
/**************************************************/

/* SP_RA: Funci�n RA() */
/* Patch 410 # Seq: 274 Agregado (PRO: M�ltiples Razones Sociales)*/
CREATE FUNCTION SP_RA(
  				@Concepto SMALLINT,
  				@Mes SMALLINT,
				@Anio SMALLINT,
				@Empleado INTEGER,
  				@Referencia char(8),
				@RazonSocial char(6),
  				@TipoNomina SMALLINT,  				
  				@ValidaReingreso CHAR(1) )
RETURNS NUMERIC(15,2)
AS
BEGIN
	DECLARE @Resultado AS NUMERIC(15,2)

	select @Resultado = SUM(MO_PERCEPC+MO_DEDUCCI)
	from   MOVIMIEN
	left outer join NOMINA on NOMINA.PE_YEAR = MOVIMIEN.PE_YEAR AND NOMINA.PE_TIPO = MOVIMIEN.PE_TIPO AND NOMINA.PE_NUMERO = MOVIMIEN.PE_NUMERO AND NOMINA.CB_CODIGO = MOVIMIEN.CB_CODIGO
	left outer join PERIODO on PERIODO.PE_YEAR = NOMINA.PE_YEAR AND PERIODO.PE_TIPO = NOMINA.PE_TIPO AND PERIODO.PE_NUMERO = NOMINA.PE_NUMERO
	left outer join RPATRON on RPATRON.TB_CODIGO = NOMINA.CB_PATRON
	left outer join RSOCIAL on RSOCIAL.RS_CODIGO = RPATRON.RS_CODIGO
    left outer join COLABORA on COLABORA.CB_CODIGO = MOVIMIEN.CB_CODIGO
	where ( MOVIMIEN.PE_YEAR = @Anio ) AND
		 ( MOVIMIEN.CB_CODIGO = @Empleado ) AND 		  
		    ( MOVIMIEN.CO_NUMERO = @Concepto ) AND
		    ( MOVIMIEN.MO_ACTIVO = 'S' ) AND
		    ( PERIODO.PE_STATUS = 6 ) AND
		    ( PERIODO.PE_MES = @Mes ) AND
		    /* Si aplica cualquier tipo de n�mina pasar por param�tro el valor -1 */
		    ( ( @TipoNomina = -1 ) OR ( MOVIMIEN.PE_TIPO = @TipoNomina ) ) AND
		    /* La referencia es opcional */
		    ( ( @Referencia = '' ) OR ( MOVIMIEN.MO_REFEREN = @Referencia ) ) AND
		    /* La raz�n social es opcional */
		    ( ( @RazonSocial = '' ) OR ( RSOCIAL.RS_CODIGO = @RazonSocial ) ) AND
             /* la validaci�n de reingresos es opcional*/
              ( ( @ValidaReingreso <> 'S' ) OR (  COLABORA.CB_FEC_ING <= PERIODO.PE_FEC_FIN  ) )

 	if ( @Resultado is NULL )
        select @Resultado = 0
	RETURN @Resultado
END
GO


/* SP_VALOR_CONCEPTO: Acumulado de concepto por raz�n social */
/* Patch 410 # Seq: 275 Agregado (PRO: M�ltiples Razones Sociales)*/
CREATE  FUNCTION SP_VALOR_CONCEPTO(
 				@Concepto SMALLINT,
				@Mes SMALLINT,
				@Anio SMALLINT,
				@Empleado INTEGER,
				@Razonsocial CHAR(6),
				@TipoNomina SMALLINT,							
				@ValidaReingreso CHAR(1)
				 )

RETURNS NUMERIC(15,2)
AS
BEGIN
     DECLARE @M1000 NUMERIC(15,2);
     DECLARE @M1001 NUMERIC(15,2);
     DECLARE @M1002 NUMERIC(15,2);
     DECLARE @M1003 NUMERIC(15,2);
     DECLARE @M1004 NUMERIC(15,2);
     DECLARE @M1005 NUMERIC(15,2);
     DECLARE @M1006 NUMERIC(15,2);
     DECLARE @M1007 NUMERIC(15,2);
     DECLARE @M1008 NUMERIC(15,2);
     DECLARE @M1009 NUMERIC(15,2);
     DECLARE @M1010 NUMERIC(15,2);
     DECLARE @M1011 NUMERIC(15,2);
     DECLARE @M1100 NUMERIC(15,2);
     DECLARE @M1101 NUMERIC(15,2);
     DECLARE @M1102 NUMERIC(15,2);
     DECLARE @M1103 NUMERIC(15,2);
     DECLARE @M1104 NUMERIC(15,2);
     DECLARE @M1105 NUMERIC(15,2);
     DECLARE @M1106 NUMERIC(15,2);
     DECLARE @M1107 NUMERIC(15,2);
     DECLARE @M1108 NUMERIC(15,2);
     DECLARE @M1109 NUMERIC(15,2);
     DECLARE @M1110 NUMERIC(15,2);
     DECLARE @M1111 NUMERIC(15,2);
     DECLARE @M1112 NUMERIC(15,2);
     DECLARE @M1113 NUMERIC(15,2);
     DECLARE @M1114 NUMERIC(15,2);
     DECLARE @M1115 NUMERIC(15,2);
     DECLARE @M1116 NUMERIC(15,2);
     DECLARE @M1117 NUMERIC(15,2);
     DECLARE @M1118 NUMERIC(15,2);
     DECLARE @M1119 NUMERIC(15,2);
     DECLARE @M1120 NUMERIC(15,2);
     DECLARE @M1121 NUMERIC(15,2);
     DECLARE @M1122 NUMERIC(15,2);
     DECLARE @M1123 NUMERIC(15,2);
     DECLARE @M1200 NUMERIC(15,2);
     DECLARE @M1201 NUMERIC(15,2);
     DECLARE @M1202 NUMERIC(15,2);
     DECLARE @M1203 NUMERIC(15,2);
     DECLARE @M1204 NUMERIC(15,2);
     DECLARE @M1205 NUMERIC(15,2);
     DECLARE @M1206 NUMERIC(15,2);
     DECLARE @M1207 NUMERIC(15,2);
     DECLARE @M1208 NUMERIC(15,2);
     DECLARE @M1209 NUMERIC(15,2);
     DECLARE @M1210 NUMERIC(15,2);
     DECLARE @M1211 NUMERIC(15,2);
     DECLARE @M1212 NUMERIC(15,2);
     DECLARE @M1213 NUMERIC(15,2);
     DECLARE @M1214 NUMERIC(15,2);
     DECLARE @M1215 NUMERIC(15,2);
     DECLARE @TipoNom SMALLINT;
     DECLARE @Resultado NUMERIC(15,2);

select @Resultado = 0;

if ( @Concepto <> 1004 ) 
begin
	DECLARE TemporalCursorNom Cursor for
	
     select N.NO_PERCEPC,
            N.NO_DEDUCCI,
            N.NO_NETO,
            N.NO_X_ISPT,
            N.NO_PER_MEN,
            N.NO_X_MENS,
            N.NO_PER_CAL,
            N.NO_IMP_CAL,
            N.NO_X_CAL,
            N.NO_TOT_PRE,
	       N.NO_PREV_GR,
            N.NO_DIAS_FI,
            N.NO_DIAS_FJ,
            N.NO_DIAS_CG,
            N.NO_DIAS_SG,
            N.NO_DIAS_SU,
            N.NO_DIAS_OT,
            N.NO_DIAS_IN,
            N.NO_DIAS * 7 / 6,
            N.NO_DIAS_RE,
            N.NO_D_TURNO,
            N.NO_DIAS,
            N.NO_DIAS_AS,
            N.NO_DIAS_NT,
            N.NO_DIAS_FV,
            N.NO_DIAS_VA,
            N.NO_DIAS_AG,
            N.NO_DIAS_AJ,
            N.NO_DIAS_SS,
            N.NO_DIAS_EM,
            N.NO_DIAS_SI,
            N.NO_DIAS_BA,
            N.NO_DIAS_PV,
            N.NO_DIAS_VJ,
            N.NO_DIAS_VJ * 7 / 6,
            N.NO_JORNADA,         
            N.NO_HORAS,           
            N.NO_EXTRAS,          
            N.NO_DOBLES,
            N.NO_TRIPLES,
            N.NO_ADICION,
            N.NO_TARDES,
            N.NO_HORA_PD,
            N.NO_HORA_CG,
            N.NO_HORA_SG,
            N.NO_FES_TRA,
            N.NO_DES_TRA,
            N.NO_VAC_TRA,
            N.NO_FES_PAG,
            N.NO_HORASNT,
            N.NO_HORAPDT,
		        N.PE_TIPO

		  from NOMINA N 
		  LEFT OUTER JOIN PERIODO ON PERIODO.PE_YEAR = N.PE_YEAR AND PERIODO.PE_TIPO = N.PE_TIPO AND PERIODO.PE_NUMERO = N.PE_NUMERO
		  LEFT OUTER JOIN RPATRON ON RPATRON.TB_CODIGO = N.CB_PATRON
		  LEFT OUTER JOIN RSOCIAL ON RSOCIAL.RS_CODIGO = RPATRON.RS_CODIGO
		  LEFT OUTER JOIN COLABORA ON COLABORA.CB_CODIGO = N.CB_CODIGO
		where
			( N.PE_YEAR = @Anio ) and
			( N.CB_CODIGO = @Empleado ) and
			( PERIODO.PE_STATUS = 6 ) and
			( PERIODO.PE_MES = @Mes ) and
			( ( @TipoNomina = -1 ) or ( N.PE_TIPO = @TipoNomina ) ) and			 
			( ( @RazonSocial = '' ) or ( RSOCIAL.RS_CODIGO = @RazonSocial ) ) and			 
      		( ( @ValidaReingreso <> 'S' ) or ( COLABORA.CB_FEC_ING <= PERIODO.PE_FEC_FIN ) ) 
			 
			 
			
		open TemporalCursorNom
		Fetch next from TemporalCursorNom
		into 
        @M1000,
        @M1001,
        @M1002,
        @M1003,
        @M1005,
        @M1006,
        @M1007,
        @M1008,
        @M1009,
        @M1010,
        @M1011,
        @M1100,
        @M1101,
        @M1102,
        @M1103,
        @M1104,
        @M1105,
        @M1106,
        @M1107,
        @M1108,
        @M1109,
        @M1110,
        @M1111,
        @M1112,
        @M1113,
        @M1114,
        @M1115,
        @M1116,
        @M1117,
        @M1118,
        @M1119,
        @M1120,
        @M1121,
        @M1122,
        @M1123,
        @M1200,
        @M1201,
        @M1202,
        @M1203,
        @M1204,
        @M1205,
        @M1206,
        @M1207,
        @M1208,
        @M1209,
        @M1210,
        @M1211,
        @M1212,
        @M1213,
        @M1214,
        @M1215,
        @TipoNom

	WHILE @@FETCH_STATUS = 0
     begin
		
          if ( @Concepto = 1200 or @Concepto = 1109 ) and ( @M1200 <> 0 ) 
          begin
                Select @M1200 = @M1201 / @M1200;
          end
          if ( @Concepto = 1000 ) and (@M1000 <> 0)
            select @Resultado = @M1000 + @Resultado
          if ( @Concepto = 1001 ) and (@M1001 <> 0)
            select @Resultado = @M1001 + @Resultado
          if ( @Concepto = 1002 ) and (@M1002 <> 0)
            select @Resultado = @M1002 + @Resultado
          if ( @Concepto = 1003 ) and (@M1003 <> 0)
            select @Resultado = @M1003 + @Resultado
          if ( @Concepto = 1005 ) and (@M1005 <> 0)
            select @Resultado = @M1005 + @Resultado
          if ( @Concepto = 1006 ) and (@M1006 <> 0)
            select @Resultado = @M1006 + @Resultado
          if ( @Concepto = 1007 ) and (@M1007 <> 0)
            select @Resultado = @M1007 + @Resultado
          if ( @Concepto = 1008 ) and (@M1008 <> 0)
            select @Resultado = @M1008 + @Resultado
          if ( @Concepto = 1009 ) and (@M1009 <> 0)
            select @Resultado = @M1009 + @Resultado
          if ( @Concepto = 1010 ) and (@M1010 <> 0)
            select @Resultado = @M1010 + @Resultado
          if ( @Concepto = 1011 ) and (@M1011 <> 0)
            select @Resultado = @M1011 + @Resultado
          if ( @Concepto = 1100 ) and (@M1100 <> 0)
            select @Resultado = @M1100 + @Resultado
          if ( @Concepto = 1101 ) and (@M1101 <> 0)
            select @Resultado = @M1101 + @Resultado
          if ( @Concepto = 1102 ) and (@M1102 <> 0)
            select @Resultado = @M1102 + @Resultado
          if ( @Concepto = 1103 ) and (@M1103 <> 0)
            select @Resultado = @M1103 + @Resultado
          if ( @Concepto = 1104 ) and (@M1104 <> 0)
            select @Resultado = @M1104 + @Resultado
          if ( @Concepto = 1105 ) and (@M1105 <> 0)
            select @Resultado = @M1105 + @Resultado
          if ( @Concepto = 1106 ) and (@M1106 <> 0)
            select @Resultado = @M1106 + @Resultado
          if ( @Concepto = 1107 ) and (@M1107 <> 0)
            select @Resultado = @M1107 + @Resultado
          if ( @Concepto = 1108 ) and (@M1108 <> 0)
            select @Resultado = @M1108 + @Resultado
          if ( @Concepto = 1109 ) and (@M1109 <> 0) 
		begin
			select @M1109 = @M1200 * @M1109;
               select @Resultado = @M1109 + @Resultado
		end
          if ( @Concepto = 1110 ) and (@M1110 <> 0)
            select @Resultado = @M1110 + @Resultado
          if ( @Concepto = 1111 ) and (@M1111 <> 0)
            select @Resultado = @M1111 + @Resultado
          if ( @Concepto = 1112 ) and (@M1112 <> 0)
            select @Resultado = @M1112 + @Resultado
          if ( @Concepto = 1113 ) and (@M1113 <> 0)
            select @Resultado = @M1113 + @Resultado
          if ( @Concepto = 1114 ) and (@M1114 <> 0)
            select @Resultado = @M1114 + @Resultado
          if ( @Concepto = 1115 ) and (@M1115 <> 0)
            select @Resultado = @M1115 + @Resultado
          if ( @Concepto = 1116 ) and (@M1116 <> 0)
            select @Resultado = @M1116 + @Resultado
          if ( @Concepto = 1117 ) and (@M1117 <> 0)
            select @Resultado = @M1117 + @Resultado
          if ( @Concepto = 1118 ) and (@M1118 <> 0)
            select @Resultado = @M1118 + @Resultado
          if ( @Concepto = 1119 ) and (@M1119 <> 0)
              select @Resultado = @M1119 + @Resultado
          if ( @Concepto = 1120 ) and (@M1120 <> 0)
            select @Resultado = @M1120 + @Resultado
          if ( @Concepto = 1121 ) and (@M1121 <> 0)
            select @Resultado = @M1121 + @Resultado
          if ( @Concepto = 1122 ) and (@M1122 <> 0)
            select @Resultado = @M1122 + @Resultado
          if ( @Concepto = 1123 ) and (@M1123 <> 0)
            select @Resultado = @M1123 + @Resultado
          if ( @Concepto = 1200 ) and (@M1200 <> 0 )
     	    begin
              if ( @TipoNom = 0 )
                Select @M1200 = 8 * @M1200;
              else
              if ( ( @TipoNom = 1 ) or ( @TipoNom = 6 ) or ( @TipoNom = 7 ) )
                Select @M1200 = 48 * @M1200;
              else
              if ( ( @TipoNom = 2 ) or ( @TipoNom = 10 ) or ( @TipoNom = 11 ) )
                Select @M1200 = 96 * @M1200;
              else
              if ( ( @TipoNom = 3 ) or ( @TipoNom = 8 ) or ( @TipoNom = 9 ) )
                Select @M1200 = 104 * @M1200;
              else
              if ( @TipoNom = 4 )
                Select @M1200 = 208 * @M1200;
              else
              if ( @TipoNom = 5 )
                Select @M1200 = 72 * @M1200;
              select @Resultado = @M1200 + @Resultado
        end
     		if ( @Concepto = 1201 and @M1201 <> 0 )
          select @Resultado = @M1201 + @Resultado
        if ( @Concepto = 1202 and @M1202 <> 0 )
          select @Resultado = @M1202 + @Resultado
        if ( @Concepto = 1203 and @M1203 <> 0 )
          select @Resultado = @M1203 + @Resultado
        if ( @Concepto = 1204 and @M1204 <> 0 )
          select @Resultado = @M1204 + @Resultado
        if ( @Concepto = 1205 and @M1205 <> 0 )
          select @Resultado = @M1205 + @Resultado
        if ( @Concepto = 1206 and @M1206 <> 0 )
          select @Resultado = @M1206 + @Resultado
        if ( @Concepto = 1207 and @M1207 <> 0 )
          select @Resultado = @M1207 + @Resultado
        if ( @Concepto = 1208 and @M1208 <> 0 )
          select @Resultado = @M1208 + @Resultado
        if ( @Concepto = 1209 and @M1209 <> 0 )
          select @Resultado = @M1209 + @Resultado
        if ( @Concepto = 1210 and @M1210 <> 0 )
          select @Resultado = @M1210 + @Resultado
        if ( @Concepto = 1211 and @M1211 <> 0 )
          select @Resultado = @M1211 + @Resultado
        if ( @Concepto = 1212 and @M1212 <> 0 )
          select @Resultado = @M1212 + @Resultado
        if ( @Concepto = 1213 and @M1213 <> 0 )
          select @Resultado = @M1213 + @Resultado
        if ( @Concepto = 1214 and @M1214 <> 0 )
          select @Resultado = @M1214 + @Resultado
        if ( @Concepto = 1215 and @M1215 <> 0 )
          select @Resultado = @M1215 + @Resultado


			Fetch next from TemporalCursorNom
     	  into  
			      @M1000,
            @M1001,
            @M1002,
            @M1003,
            @M1005,
            @M1006,
            @M1007,
            @M1008,
            @M1009,
            @M1010,
	          @M1011,
            @M1100,
            @M1101,
            @M1102,
            @M1103,
            @M1104,
            @M1105,
            @M1106,
            @M1107,
            @M1108,
            @M1109,
            @M1110,
            @M1111,
            @M1112,
            @M1113,
            @M1114,
            @M1115,
            @M1116,
            @M1117,
            @M1118,
            @M1119,
            @M1120,
            @M1121,
            @M1122,
            @M1123,
            @M1200,         
            @M1201,           
            @M1202,          
            @M1203,
            @M1204,
            @M1205,
            @M1206,
            @M1207,
            @M1208,
            @M1209,
            @M1210,
            @M1211,
            @M1212,
            @M1213,
            @M1214,
            @M1215,
		        @TipoNom
 		
	end
  CLOSE TemporalCursorNom
  DEALLOCATE TemporalCursorNom
end
else
begin
  	select @M1004 = sum( M.MO_PERCEPC )
     from MOVIMIEN M
	left outer join CONCEPTO C on C.CO_NUMERO=M.CO_NUMERO 
	left outer join NOMINA on NOMINA.PE_YEAR = M.PE_YEAR AND NOMINA.PE_TIPO = M.PE_TIPO AND NOMINA.PE_NUMERO = M.PE_NUMERO AND NOMINA.CB_CODIGO = M.CB_CODIGO
	left outer join PERIODO on PERIODO.PE_YEAR = NOMINA.PE_YEAR AND PERIODO.PE_TIPO = NOMINA.PE_TIPO AND PERIODO.PE_NUMERO = NOMINA.PE_NUMERO
	left outer join RPATRON on RPATRON.TB_CODIGO = NOMINA.CB_PATRON
	left outer join RSOCIAL on RSOCIAL.RS_CODIGO = RPATRON.RS_CODIGO
     left outer join COLABORA on COLABORA.CB_CODIGO = M.CB_CODIGO 
     where
		( M.PE_YEAR = @Anio ) AND
		( M.CB_CODIGO = @Empleado ) AND  		
		( M.MO_ACTIVO = 'S' ) AND
		( PERIODO.PE_STATUS = 6 ) AND
		( PERIODO.PE_MES = @Mes ) AND
                /* Si aplica cualquier tipo de n�mina pasar por param�tro el valor -1 */
		( ( @TipoNomina = -1 ) or ( NOMINA.PE_TIPO = @TipoNomina ) ) and	
		    /* La raz�n social es opcional */
		( ( @RazonSocial = '' ) or ( RSOCIAL.RS_CODIGO = @RazonSocial ) ) and
             /* la validaci�n de reingresos es opcional*/
          ( ( @ValidaReingreso <> 'S' ) OR (  COLABORA.CB_FEC_ING <= PERIODO.PE_FEC_FIN  ) ) and
          ( C.CO_TIPO = 1 ) and
		( C.CO_A_PTU = 'S' ) 


     if ( @M1004 <> 0 )
        select @Resultado = @M1004
end

  RETURN @Resultado

END

GO

/* SP_RAS: Funci�n RAS() */
/* Patch 410 # Seq: 276 Agregado (PRO: M�ltiples Razones Sociales)*/
CREATE FUNCTION SP_RAS(
		     @Concepto SMALLINT,
    			@NumeroIni SMALLINT,
    			@Numerofin SMALLINT,
			@Anio SMALLINT,
			@Empleado INTEGER,
			@Referencia char(8),
			@RazonSocial CHAR(6),
    		     @TipoNomina SMALLINT,   		    		
			@ValidaReingreso CHAR(1))
RETURNS NUMERIC(15,2)
AS
begin
	declare @Temporal NUMERIC(15,2);
	declare @Resultado NUMERIC(15,2);

  select @Resultado = 0;
  while ( @NumeroIni <= @NumeroFin )
  begin
	  if @Concepto < 1000 
	  	select @Temporal = dbo.SP_RA ( @Concepto, @NumeroIni, @Anio, @Empleado, @Referencia, @RazonSocial, @TipoNomina, @ValidaReingreso )
	  else
		select @Temporal = dbo.SP_VALOR_CONCEPTO ( @Concepto, @NumeroIni, @Anio, @Empleado, @RazonSocial, @TipoNomina, @ValidaReingreso )
		  
	if ( @Temporal is NOT NULL )
	    select @Resultado = @Resultado + @Temporal;
	select @NumeroIni = @NumeroIni + 1;
  end
	Return @Resultado;
end
GO



/* SP_INVESTIGA_STATUS: L�mite de n�minas ordinarias configurable (PRO:N�minas complementarias)*/
/* Patch 410 # Seq: 227 Modificado */
ALTER FUNCTION SP_INVESTIGA_STATUS ( @Empleado INTEGER, @Fecha DATETIME ) RETURNS SMALLINT AS
BEGIN
	declare @Status SMALLINT
	declare @FechaTipNom DATETIME
	declare @TipoNomina SmallInt
	declare @LimiteNomina SmallInt

	--Obtener Limite de Nomina del Global #213 "Numero Limite de Nominas Ordinarias"
	select @LimiteNomina = cast(GL_FORMULA as SmallInt) from GLOBAL where GL_CODIGO = 213

	select @TipoNomina = CB_NOMINA, @FechaTipNom = CB_FEC_NOM from COLABORA where CB_CODIGO = @Empleado

	if (@FechaTipNom > @Fecha) 
	select @TipoNomina = dbo.SP_KARDEX_CB_NOMINA( @Fecha, @Empleado )

	select @Status = MAX(PE_STATUS) from PERIODO
	where PE_NUMERO < @LimiteNomina and PE_ASI_INI <= @Fecha and PE_ASI_FIN >= @Fecha and PE_TIPO = @TipoNomina

	if @Status is NULL Select @Status = 0;

	Return @Status
end
GO

/* AUTORIZACIONES_SELECT: Considerar horas pactadas (PRO:Horas pactadas)*/
/* Patch 410 # Seq: 78 Modificado */
ALTER FUNCTION AUTORIZACIONES_SELECT
(    @Empleado INTEGER,
    @Fecha DATETIME,
    @Tipo SMALLINT )
RETURNS @RegAuto TABLE (
    Resultado NUMERIC(15,2),
    Motivo CHAR(4),
    US_COD_OK SMALLINT,
    HRS_APROB NUMERIC(15,2) )
AS
BEGIN
	declare @Resultado NUMERIC(15,2)
	declare	@Motivo CHAR(4)
        declare @US_COD_OK SMALLINT
        declare @HRS_APROB  NUMERIC(15,2)
     /* 7: Horas Extras
	  11: Horas Extras Prepagadas fuera de jornada
	  12: Horas Extras Prepagadas dentro de jornada*/
	if ( @Tipo in (7,11,12) )
	begin
		select @Resultado = CH_HOR_EXT,
                       @Motivo = CH_RELOJ,
                       @US_COD_OK = US_COD_OK,
                       @HRS_APROB = CH_HOR_DES from CHECADAS
          	where 	( CB_CODIGO = @Empleado ) and
                	( AU_FECHA = @Fecha ) and
                	( CH_TIPO = @Tipo )
     	end
     	else
     	begin
          	select @Resultado = CH_HOR_ORD,
                       @Motivo = CH_RELOJ,
                       @US_COD_OK = US_COD_OK,
                       @HRS_APROB = CH_HOR_DES from CHECADAS
          	where 	( CB_CODIGO = @Empleado ) and
                	( AU_FECHA = @Fecha ) and
                	( CH_TIPO = @Tipo )
     	end
     	if ( @Resultado is Null )
     	begin
          	set @Resultado = 0
          	set @Motivo = ''
     	end

  	insert into @RegAuto
	values ( @Resultado, @Motivo, @US_COD_OK, @HRS_APROB )
	return
END
GO

/* AUTORIZACION_AGREGAR: Considerar horas pactadas (PRO:Horas pactadas)*/
/* Patch 410 # Seq: 171 Modificado */
ALTER PROCEDURE AUTORIZACION_AGREGAR 
    @CB_CODIGO INTEGER,
    @AU_FECHA DATETIME,
    @CH_TIPO SMALLINT,
    @CH_DESCRIP SMALLINT,
    @CH_HOR_EXT NUMERIC(15,2),
    @CH_HOR_ORD NUMERIC(15,2),
    @CH_RELOJ CHAR(4),
    @CH_GLOBAL CHAR(1),
    @US_CODIGO SMALLINT
AS
     SET NOCOUNT ON
     delete from CHECADAS where 
	( CB_CODIGO = @CB_CODIGO ) and
     	( AU_FECHA = @AU_FECHA ) and
     	( CH_TIPO = @CH_TIPO )
	/* 7: Horas Extras
	  11: Horas Extras Prepagadas fuera de jornada
	  12: Horas Extras Prepagadas dentro de jornada*/
     if ( ( ( @CH_TIPO in (7,11,12) ) and ( @CH_HOR_EXT > 0 ) ) or ( @CH_HOR_ORD > 0 ) ) 
     begin
       	insert into CHECADAS( 	CB_CODIGO,
                               	AU_FECHA,
                               	CH_H_REAL,
                               	CH_H_AJUS,
                               	CH_SISTEMA,
                               	CH_GLOBAL,
                               	CH_IGNORAR,
	                        CH_RELOJ,
                                US_CODIGO,
              	                CH_TIPO,
                       	        CH_DESCRIP,
                               	CH_HOR_EXT,
	                        CH_HOR_ORD )
        	values ( @CB_CODIGO,
	        	 @AU_FECHA,
        		 '----',
		         '----',
	                 'N',
        	         @CH_GLOBAL,
                	 'S',
	                 @CH_RELOJ,
        	         @US_CODIGO,
                	 @CH_TIPO,
	                 @CH_DESCRIP,
        	         @CH_HOR_EXT,
                	 @CH_HOR_ORD );
     end


GO

/* AUTORIZACIONES_CREAR_CHECADAS: Considerar horas pactadas (PRO:Horas pactadas)*/
/* Patch 410 # Seq: 173 Modificado */
ALTER PROCEDURE AUTORIZACIONES_CREAR_CHECADAS 
    @CB_CODIGO INTEGER,
    @AU_FECHA DATETIME,
    @AU_AUT_EXT NUMERIC(15,2),
    @M_AU_AUT_EXT CHAR(4),
    @AU_AUT_TRA NUMERIC(15,2),
    @M_AU_AUT_TRA CHAR(4),
    @AU_PER_CG NUMERIC(15,2),
    @M_AU_PER_CG CHAR(4),
    @AU_PCG_ENT NUMERIC(15,2),
    @M_AU_PCG_ENT CHAR(4),
    @AU_PER_SG NUMERIC(15,2),
    @M_AU_PER_SG CHAR(4),
    @AU_PSG_ENT NUMERIC(15,2),
    @M_AU_PSG_ENT CHAR(4),
    @AU_PRE_FUERA_JOR NUMERIC(15,2),
    @M_AU_PRE_FUERA_JOR CHAR(4),
    @AU_PRE_DENTRO_JOR NUMERIC(15,2),
    @M_AU_PRE_DENTRO_JOR CHAR(4),	
    @CH_GLOBAL CHAR(1),
    @US_CODIGO INTEGER
AS
        SET NOCOUNT ON
	DECLARE @HO_CODIGO CHAR(6)
	DECLARE @AU_HOR_MAN CHAR(1)
	DECLARE @AU_STATUS SMALLINT
	DECLARE @OLD_AU_AUT_EXT NUMERIC(15,2)
	DECLARE @OLD_M_AU_AUT_EXT CHAR(4)
	DECLARE @OLD_AU_AUT_TRA NUMERIC(15,2)
	DECLARE @OLD_M_AU_AUT_TRA CHAR(4)
	DECLARE @OLD_AU_PER_CG NUMERIC(15,2)
	DECLARE @OLD_M_AU_PER_CG CHAR(4)
	DECLARE @OLD_AU_PCG_ENT NUMERIC(15,2)
	DECLARE @OLD_M_AU_PCG_ENT CHAR(4)
	DECLARE @OLD_AU_PER_SG NUMERIC(15,2)
	DECLARE @OLD_M_AU_PER_SG CHAR(4)
	DECLARE @OLD_AU_PSG_ENT NUMERIC(15,2)
	DECLARE @OLD_M_AU_PSG_ENT CHAR(4)
	DECLARE @OLD_AU_PRE_FUERA_JOR NUMERIC(15,2)
	DECLARE @OLD_M_AU_PRE_FUERA_JOR CHAR(4)
	DECLARE @OLD_AU_PRE_DENTRO_JOR NUMERIC(15,2)
	DECLARE @OLD_M_AU_PRE_DENTRO_JOR CHAR(4)

	select @OLD_AU_AUT_EXT = Resultado, @OLD_M_AU_AUT_EXT = Motivo
	from dbo.AUTORIZACIONES_SELECT( @CB_CODIGO, @AU_FECHA, 7 )
	select @OLD_AU_AUT_TRA = Resultado, @OLD_M_AU_AUT_TRA = Motivo
	from dbo.AUTORIZACIONES_SELECT( @CB_CODIGO, @AU_FECHA, 8 )
	select @OLD_AU_PER_CG = Resultado, @OLD_M_AU_PER_CG = Motivo
	from dbo.AUTORIZACIONES_SELECT( @CB_CODIGO, @AU_FECHA, 5 )
	select @OLD_AU_PCG_ENT = Resultado, @OLD_M_AU_PCG_ENT = Motivo
	from dbo.AUTORIZACIONES_SELECT( @CB_CODIGO, @AU_FECHA, 9 )
	select @OLD_AU_PER_SG = Resultado, @OLD_M_AU_PER_SG = Motivo
	from dbo.AUTORIZACIONES_SELECT( @CB_CODIGO, @AU_FECHA, 6 )
	select @OLD_AU_PSG_ENT = Resultado, @OLD_M_AU_PSG_ENT = Motivo
	from dbo.AUTORIZACIONES_SELECT( @CB_CODIGO, @AU_FECHA, 10 )
	select @OLD_AU_PRE_FUERA_JOR = Resultado, @OLD_M_AU_PRE_FUERA_JOR = Motivo
	from dbo.AUTORIZACIONES_SELECT( @CB_CODIGO, @AU_FECHA, 11 )
	select @OLD_AU_PRE_DENTRO_JOR = Resultado, @OLD_M_AU_PRE_DENTRO_JOR = Motivo
	from dbo.AUTORIZACIONES_SELECT( @CB_CODIGO, @AU_FECHA, 12 )
	

	if ( (@OLD_AU_AUT_EXT <> @AU_AUT_EXT) or (@OLD_M_AU_AUT_EXT <> @M_AU_AUT_EXT) ) 
	begin
          	EXECUTE AUTORIZACION_AGREGAR @CB_CODIGO, @AU_FECHA, 7, 4, @AU_AUT_EXT,
			0, @M_AU_AUT_EXT, @CH_GLOBAL, @US_CODIGO
     	end
	if ( (@OLD_AU_AUT_TRA <> @AU_AUT_TRA) or (@OLD_M_AU_AUT_TRA <> @M_AU_AUT_TRA) ) 
	begin
          	EXECUTE AUTORIZACION_AGREGAR @CB_CODIGO, @AU_FECHA, 8, 8, 0,
			@AU_AUT_TRA, @M_AU_AUT_TRA, @CH_GLOBAL, @US_CODIGO
     	end
	if ( (@OLD_AU_PER_CG <> @AU_PER_CG) or (@OLD_M_AU_PER_CG <> @M_AU_PER_CG) ) 
	begin
          	EXECUTE AUTORIZACION_AGREGAR @CB_CODIGO, @AU_FECHA, 5, 1, 0,
			@AU_PER_CG, @M_AU_PER_CG, @CH_GLOBAL, @US_CODIGO
     	end
	if ( (@OLD_AU_PCG_ENT <> @AU_PCG_ENT) or (@OLD_M_AU_PCG_ENT <> @M_AU_PCG_ENT) ) 
	begin
          	EXECUTE AUTORIZACION_AGREGAR @CB_CODIGO, @AU_FECHA, 9, 9, 0,
			@AU_PCG_ENT, @M_AU_PCG_ENT, @CH_GLOBAL, @US_CODIGO
     	end
	if ( (@OLD_AU_PER_SG <> @AU_PER_SG) or (@OLD_M_AU_PER_SG <> @M_AU_PER_SG) ) 
	begin
          	EXECUTE AUTORIZACION_AGREGAR @CB_CODIGO, @AU_FECHA, 6, 1, 0,
			@AU_PER_SG, @M_AU_PER_SG, @CH_GLOBAL, @US_CODIGO
     	end
	if ( (@OLD_AU_PSG_ENT <> @AU_PSG_ENT) or (@OLD_M_AU_PSG_ENT <> @M_AU_PSG_ENT) ) 
	begin
          	EXECUTE AUTORIZACION_AGREGAR @CB_CODIGO, @AU_FECHA, 10, 9, 0,
			@AU_PSG_ENT, @M_AU_PSG_ENT, @CH_GLOBAL, @US_CODIGO
     	end

	if ( (@OLD_AU_PRE_FUERA_JOR <> @AU_PRE_FUERA_JOR) or (@OLD_M_AU_PRE_FUERA_JOR <> @M_AU_PRE_FUERA_JOR) ) 
	begin
          	EXECUTE AUTORIZACION_AGREGAR @CB_CODIGO, @AU_FECHA, 11, 4, @AU_PRE_FUERA_JOR,
			0, @M_AU_PRE_FUERA_JOR, @CH_GLOBAL, @US_CODIGO

     	end
	if ( (@OLD_AU_PRE_DENTRO_JOR <> @AU_PRE_DENTRO_JOR) or (@OLD_M_AU_PRE_DENTRO_JOR <> @M_AU_PRE_DENTRO_JOR) ) 
	begin
          	EXECUTE AUTORIZACION_AGREGAR @CB_CODIGO, @AU_FECHA, 12, 4, @AU_PRE_DENTRO_JOR,
			0, @M_AU_PRE_DENTRO_JOR, @CH_GLOBAL, @US_CODIGO
     	end


GO


/* ASISTENCIA_AGREGA: Considerar horas pactadas (PRO:Horas pactadas)*/
/* Patch 410 # Seq: 13 Modificado */
ALTER PROCEDURE ASISTENCIA_AGREGA
    @CB_CODIGO INTEGER,
    @AU_FECHA DATETIME,
    @HO_CODIGO CHAR(6),
    @AU_HOR_MAN CHAR(1),
    @AU_STATUS SMALLINT,
    @HRS_EXTRAS NUMERIC(15,2),
    @M_HRS_EXTRAS CHAR(4),
    @DESCANSO NUMERIC(15,2),
    @M_DESCANSO CHAR(4),
    @PER_CG NUMERIC(15,2),
    @M_PER_CG CHAR(4),
    @PER_CG_ENT NUMERIC(15,2),
    @M_PER_CG_ENT CHAR(4),
    @PER_SG NUMERIC(15,2),
    @M_PER_SG CHAR(4),
    @PER_SG_ENT NUMERIC(15,2),
    @M_PER_SG_ENT CHAR(4),
    @PRE_FUERA_JOR NUMERIC(15,2),
    @M_PRE_FUERA_JOR CHAR(4),
    @PRE_DENTRO_JOR NUMERIC(15,2),
    @M_PRE_DENTRO_JOR CHAR(4),
    @CHECADA1 CHAR(4),
    @CHECADA2 CHAR(4),
    @CHECADA3 CHAR(4),
    @CHECADA4 CHAR(4),
    @US_CODIGO SMALLINT,
    @AU_OUT2EAT SMALLINT
AS
BEGIN
    SET NOCOUNT ON
	DECLARE @CB_AUTOSAL CHAR(1)
	DECLARE @CB_CONTRAT CHAR(1)
	DECLARE @CB_TABLASS CHAR(1)
	DECLARE @CB_PUESTO CHAR(6)
	DECLARE @CB_CLASIFI CHAR(6)
	DECLARE @CB_TURNO CHAR(6)
	DECLARE @CB_NIVEL1 CHAR(6)
	DECLARE @CB_NIVEL2 CHAR(6)
	DECLARE @CB_NIVEL3 CHAR(6)
	DECLARE @CB_NIVEL4 CHAR(6)
	DECLARE @CB_NIVEL5 CHAR(6)
	DECLARE @CB_NIVEL6 CHAR(6)
	DECLARE @CB_NIVEL7 CHAR(6)
	DECLARE @CB_NIVEL8 CHAR(6)
	DECLARE @CB_NIVEL9 CHAR(6)

	execute SP_KARDEX_CLASIFICACION @CB_CODIGO, @AU_FECHA, @CB_AUTOSAL OUTPUT,
		@CB_CLASIFI OUTPUT, @CB_CONTRAT OUTPUT, @CB_TURNO OUTPUT,
		@CB_PUESTO OUTPUT, @CB_TABLASS OUTPUT, @CB_NIVEL1 OUTPUT,
		@CB_NIVEL2 OUTPUT, @CB_NIVEL3 OUTPUT, @CB_NIVEL4 OUTPUT,
		@CB_NIVEL5 OUTPUT, @CB_NIVEL6 OUTPUT, @CB_NIVEL7 OUTPUT,
		@CB_NIVEL8 OUTPUT, @CB_NIVEL9 OUTPUT

	insert into AUSENCIA
		( CB_CODIGO, AU_FECHA, AU_STATUS, HO_CODIGO, AU_HOR_MAN, AU_OUT2EAT,

		CB_PUESTO, CB_CLASIFI, CB_TURNO, CB_NIVEL1, CB_NIVEL2,
		CB_NIVEL3, CB_NIVEL4, CB_NIVEL5, CB_NIVEL6, CB_NIVEL7,
		CB_NIVEL8, CB_NIVEL9 )
	values	( @CB_CODIGO, @AU_FECHA, @AU_STATUS, @HO_CODIGO, @AU_HOR_MAN, @AU_OUT2EAT,
		@CB_PUESTO, @CB_CLASIFI, @CB_TURNO, @CB_NIVEL1, @CB_NIVEL2,
		@CB_NIVEL3, @CB_NIVEL4, @CB_NIVEL5, @CB_NIVEL6, @CB_NIVEL7,
		@CB_NIVEL8, @CB_NIVEL9 )

     	execute ASISTENCIA_CREAR_CHECADAS @CB_CODIGO, @AU_FECHA, @CHECADA1,
		@CHECADA2, @CHECADA3, @CHECADA4, 'N', @US_CODIGO

	execute AUTORIZACIONES_CREAR_CHECADAS @CB_CODIGO, @AU_FECHA, @HRS_EXTRAS,
		@M_HRS_EXTRAS, @DESCANSO, @M_DESCANSO, @PER_CG, @M_PER_CG, @PER_CG_ENT,
		@M_PER_CG_ENT, @PER_SG, @M_PER_SG, @PER_SG_ENT, @M_PER_SG_ENT, @PRE_FUERA_JOR,
		@M_PRE_FUERA_JOR, @PRE_DENTRO_JOR, @M_PRE_DENTRO_JOR,'N',
		@US_CODIGO
END

GO


/* ASISTENCIA_MODIFICA: Considerar horas pactadas (PRO:Horas pactadas)*/
/* Patch 410 # Seq: 15 Modificado */
ALTER PROCEDURE ASISTENCIA_MODIFICA
    @CB_CODIGO INTEGER,
    @AU_FECHA DATETIME,
    @HO_CODIGO CHAR(6),
    @AU_HOR_MAN CHAR(1),
    @AU_STATUS SMALLINT,
    @HRS_EXTRAS NUMERIC(15,2),
    @M_HRS_EXTRAS CHAR(4),
    @DESCANSO NUMERIC(15,2),
    @M_DESCANSO CHAR(4),
    @PER_CG NUMERIC(15,2),
    @M_PER_CG CHAR(4),
    @PER_CG_ENT NUMERIC(15,2),
    @M_PER_CG_ENT CHAR(4),
    @PER_SG NUMERIC(15,2),
    @M_PER_SG CHAR(4),
    @PER_SG_ENT NUMERIC(15,2),
    @M_PER_SG_ENT CHAR(4),
    @PRE_FUERA_JOR NUMERIC(15,2),
    @M_PRE_FUERA_JOR CHAR(4),
    @PRE_DENTRO_JOR NUMERIC(15,2),
    @M_PRE_DENTRO_JOR CHAR(4),
    @CHECADA1 CHAR(4),
    @CHECADA2 CHAR(4),
    @CHECADA3 CHAR(4),
    @CHECADA4 CHAR(4),
    @US_CODIGO SMALLINT,
    @AU_OUT2EAT SMALLINT
AS
BEGIN
    SET NOCOUNT ON
  	DECLARE @CB_AUTOSAL CHAR(1)
	DECLARE @CB_CONTRAT CHAR(1)
	DECLARE @CB_TABLASS CHAR(1)
	DECLARE @CB_PUESTO CHAR(6)
	DECLARE @CB_CLASIFI CHAR(6)
	DECLARE @CB_TURNO CHAR(6)
	DECLARE @CB_NIVEL1 CHAR(6)
	DECLARE @CB_NIVEL2 CHAR(6)
	DECLARE @CB_NIVEL3 CHAR(6)
	DECLARE @CB_NIVEL4 CHAR(6)
	DECLARE @CB_NIVEL5 CHAR(6)
	DECLARE @CB_NIVEL6 CHAR(6)
	DECLARE @CB_NIVEL7 CHAR(6)
	DECLARE @CB_NIVEL8 CHAR(6)
	DECLARE @CB_NIVEL9 CHAR(6)

	execute SP_KARDEX_CLASIFICACION @CB_CODIGO, @AU_FECHA, @CB_AUTOSAL OUTPUT, 
		@CB_CLASIFI OUTPUT, @CB_CONTRAT OUTPUT, @CB_TURNO OUTPUT,
		@CB_PUESTO OUTPUT, @CB_TABLASS OUTPUT, @CB_NIVEL1 OUTPUT, 
		@CB_NIVEL2 OUTPUT, @CB_NIVEL3 OUTPUT, @CB_NIVEL4 OUTPUT, 
		@CB_NIVEL5 OUTPUT, @CB_NIVEL6 OUTPUT, @CB_NIVEL7 OUTPUT,
		@CB_NIVEL8 OUTPUT, @CB_NIVEL9 OUTPUT

	update AUSENCIA set HO_CODIGO = @HO_CODIGO,
           AU_STATUS = @AU_STATUS,
		   AU_HOR_MAN = @AU_HOR_MAN,
           AU_OUT2EAT = @AU_OUT2EAT,
           CB_PUESTO = @CB_PUESTO,
		   CB_CLASIFI = @CB_CLASIFI,
           CB_TURNO = @CB_TURNO,
		   CB_NIVEL1 = @CB_NIVEL1,
           CB_NIVEL2 = @CB_NIVEL2,
		   CB_NIVEL3 = @CB_NIVEL3,
           CB_NIVEL4 = @CB_NIVEL4,
		   CB_NIVEL5 = @CB_NIVEL5,
           CB_NIVEL6 = @CB_NIVEL6,
		   CB_NIVEL7 = @CB_NIVEL7,
           CB_NIVEL8 = @CB_NIVEL8,
		   CB_NIVEL9 = @CB_NIVEL9
	where ( CB_CODIGO = @CB_CODIGO ) and ( AU_FECHA = @AU_FECHA )

     	execute ASISTENCIA_CREAR_CHECADAS @CB_CODIGO, @AU_FECHA, @CHECADA1,
		@CHECADA2, @CHECADA3, @CHECADA4, 'N', @US_CODIGO

	execute AUTORIZACIONES_CREAR_CHECADAS @CB_CODIGO, @AU_FECHA, @HRS_EXTRAS,
		@M_HRS_EXTRAS, @DESCANSO, @M_DESCANSO, @PER_CG, @M_PER_CG, @PER_CG_ENT,
		@M_PER_CG_ENT, @PER_SG, @M_PER_SG, @PER_SG_ENT, @M_PER_SG_ENT, @PRE_FUERA_JOR,
	 	@M_PRE_FUERA_JOR, @PRE_DENTRO_JOR, @M_PRE_DENTRO_JOR,'N',@US_CODIGO
END

GO





/* AFECTA_EMPLEADO: Considerar concepto nuevo 1216 (PRO: Horas Pactadas)*/
/* Patch 410 # Seq: 151 Modificado */
ALTER PROCEDURE AFECTA_EMPLEADO
    			@EMPLEADO INTEGER,
    			@ANIO SMALLINT,
    			@TIPO SMALLINT,
    			@NUMERO SMALLINT,
    			@MES SMALLINT,
    			@FACTOR INTEGER
AS
BEGIN
     SET NOCOUNT ON
     DECLARE @CONCEPTO INTEGER;
     DECLARE @M1000 NUMERIC(15,2);
     DECLARE @M1001 NUMERIC(15,2);
     DECLARE @M1002 NUMERIC(15,2);
     DECLARE @M1003 NUMERIC(15,2);
     DECLARE @M1004 NUMERIC(15,2);
     DECLARE @M1005 NUMERIC(15,2);
     DECLARE @M1006 NUMERIC(15,2);
     DECLARE @M1007 NUMERIC(15,2);
     DECLARE @M1008 NUMERIC(15,2);
     DECLARE @M1009 NUMERIC(15,2);
     DECLARE @M1010 NUMERIC(15,2);
     DECLARE @M1011 NUMERIC(15,2);
     DECLARE @M1100 NUMERIC(15,2);
     DECLARE @M1101 NUMERIC(15,2);
     DECLARE @M1102 NUMERIC(15,2);
     DECLARE @M1103 NUMERIC(15,2);
     DECLARE @M1104 NUMERIC(15,2);
     DECLARE @M1105 NUMERIC(15,2);
     DECLARE @M1106 NUMERIC(15,2);
     DECLARE @M1107 NUMERIC(15,2);
     DECLARE @M1108 NUMERIC(15,2);
     DECLARE @M1109 NUMERIC(15,2);
     DECLARE @M1110 NUMERIC(15,2);
     DECLARE @M1111 NUMERIC(15,2);
     DECLARE @M1112 NUMERIC(15,2);
     DECLARE @M1113 NUMERIC(15,2);
     DECLARE @M1114 NUMERIC(15,2);
     DECLARE @M1115 NUMERIC(15,2);
     DECLARE @M1116 NUMERIC(15,2);
     DECLARE @M1117 NUMERIC(15,2);
     DECLARE @M1118 NUMERIC(15,2);
     DECLARE @M1119 NUMERIC(15,2);
     DECLARE @M1120 NUMERIC(15,2);
     DECLARE @M1121 NUMERIC(15,2);
     DECLARE @M1122 NUMERIC(15,2);
     DECLARE @M1123 NUMERIC(15,2);
     DECLARE @M1200 NUMERIC(15,2);
     DECLARE @M1201 NUMERIC(15,2);
     DECLARE @M1202 NUMERIC(15,2);
     DECLARE @M1203 NUMERIC(15,2);
     DECLARE @M1204 NUMERIC(15,2);
     DECLARE @M1205 NUMERIC(15,2);
     DECLARE @M1206 NUMERIC(15,2);
     DECLARE @M1207 NUMERIC(15,2);
     DECLARE @M1208 NUMERIC(15,2);
     DECLARE @M1209 NUMERIC(15,2);
     DECLARE @M1210 NUMERIC(15,2);
     DECLARE @M1211 NUMERIC(15,2);
     DECLARE @M1212 NUMERIC(15,2);
     DECLARE @M1213 NUMERIC(15,2);
     DECLARE @M1214 NUMERIC(15,2);
     DECLARE @M1215 NUMERIC(15,2);
	DECLARE @M1216 NUMERIC(15,2);

     select @M1000=N.NO_PERCEPC,
            @M1001=N.NO_DEDUCCI,
            @M1002=N.NO_NETO,
            @M1003=N.NO_X_ISPT,
            @M1005=N.NO_PER_MEN,
            @M1006=N.NO_X_MENS,
            @M1007=N.NO_PER_CAL,
            @M1008=N.NO_IMP_CAL,
            @M1009=N.NO_X_CAL,
            @M1010=N.NO_TOT_PRE,
            @M1011=N.NO_PREV_GR,
            @M1100=N.NO_DIAS_FI,
            @M1101=N.NO_DIAS_FJ,
            @M1102=N.NO_DIAS_CG,
            @M1103=N.NO_DIAS_SG,
            @M1104=N.NO_DIAS_SU,
            @M1105=N.NO_DIAS_OT,
            @M1106=N.NO_DIAS_IN,
            @M1107=N.NO_DIAS * 7 / 6,
            @M1108=N.NO_DIAS_RE,
            @M1109=N.NO_D_TURNO,
            @M1110=N.NO_DIAS,
            @M1111=N.NO_DIAS_AS,
            @M1112=N.NO_DIAS_NT,
            @M1113=N.NO_DIAS_FV,
            @M1114=N.NO_DIAS_VA,         
            @M1115=N.NO_DIAS_AG,
            @M1116=N.NO_DIAS_AJ,         
            @M1117=N.NO_DIAS_SS,
            @M1118=N.NO_DIAS_EM,
            @M1119=N.NO_DIAS_SI,
            @M1120=N.NO_DIAS_BA,
            @M1121=N.NO_DIAS_PV,
            @M1122=N.NO_DIAS_VJ,
            @M1123=N.NO_DIAS_VJ * 7 / 6,
            @M1200=N.NO_JORNADA,         
            @M1201=N.NO_HORAS,           
            @M1202=N.NO_EXTRAS,          
            @M1203=N.NO_DOBLES,
            @M1204=N.NO_TRIPLES,         
            @M1205=N.NO_ADICION,
            @M1206=N.NO_TARDES,
            @M1207=N.NO_HORA_PD,
            @M1208=N.NO_HORA_CG,
            @M1209=N.NO_HORA_SG,
            @M1210=N.NO_FES_TRA,
            @M1211=N.NO_DES_TRA,
            @M1212=N.NO_VAC_TRA,
            @M1213=N.NO_FES_PAG,
            @M1214=N.NO_HORASNT,
            @M1215=N.NO_HORAPDT,
		  @M1216=N.NO_PRE_EXT
 
            from NOMINA N where
            ( N.PE_YEAR = @Anio ) and
            ( N.PE_TIPO = @Tipo ) and
            ( N.PE_NUMERO = @Numero ) and
            ( N.CB_CODIGO = @Empleado )

     if ( @M1200 <> 0 )
     begin
          Select @M1200 = @M1201 / @M1200;
     end
     Select @M1109 = @M1200 * @M1109;
     if ( @M1000 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1000, @Mes, @Factor, @M1000;
     if ( @M1001 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1001, @Mes, @Factor, @M1001;
     if ( @M1002 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1002, @Mes, @Factor, @M1002;
     if ( @M1003 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1003, @Mes, @Factor, @M1003;
     if ( @M1005 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1005, @Mes, @Factor, @M1005;
     if ( @M1006 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1006, @Mes, @Factor, @M1006;
     if ( @M1007 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1007, @Mes, @Factor, @M1007;
     if ( @M1008 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1008, @Mes, @Factor, @M1008;
     if ( @M1009 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1009, @Mes, @Factor, @M1009;
     if ( @M1010 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1010, @Mes, @Factor, @M1010;
     if ( @M1011 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1011, @Mes, @Factor, @M1011;
     if ( @M1100 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1100, @Mes, @Factor, @M1100;
     if ( @M1101 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1101, @Mes, @Factor, @M1101;
     if ( @M1102 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1102, @Mes, @Factor, @M1102;
     if ( @M1103 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1103, @Mes, @Factor, @M1103;
     if ( @M1104 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1104, @Mes, @Factor, @M1104;
     if ( @M1105 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1105, @Mes, @Factor, @M1105;
     if ( @M1106 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1106, @Mes, @Factor, @M1106;
     if ( @M1107 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1107, @Mes, @Factor, @M1107;
     if ( @M1108 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1108, @Mes, @Factor, @M1108;
     if ( @M1109 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1109, @Mes, @Factor, @M1109;
     if ( @M1110 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1110, @Mes, @Factor, @M1110;
     if ( @M1111 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1111, @Mes, @Factor, @M1111;
     if ( @M1112 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1112, @Mes, @Factor, @M1112;
     if ( @M1113 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1113, @Mes, @Factor, @M1113;
     if ( @M1114 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1114, @Mes, @Factor, @M1114;
     if ( @M1115 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1115, @Mes, @Factor, @M1115;
     if ( @M1116 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1116, @Mes, @Factor, @M1116;
     if ( @M1117 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1117, @Mes, @Factor, @M1117;
     if ( @M1118 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1118, @Mes, @Factor, @M1118;
     if ( @M1119 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1119, @Mes, @Factor, @M1119;
     if ( @M1120 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1120, @Mes, @Factor, @M1120;
     if ( @M1121 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1121, @Mes, @Factor, @M1121;
     if ( @M1122 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1122, @Mes, @Factor, @M1122;
     if ( @M1123 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1123, @Mes, @Factor, @M1123;
     if ( @M1200 <> 0 )
     begin
          if ( @Tipo = 0 )
             Select @M1200 = 8 * @M1200;
          else
          if ( ( @Tipo = 1 ) or ( @Tipo = 6 ) or ( @Tipo = 7 ) )
             Select @M1200 = 48 * @M1200;
          else
          if ( ( @Tipo = 2 ) or ( @Tipo = 10 ) or ( @Tipo = 11 ) )
             Select @M1200 = 96 * @M1200;
          else
          if ( ( @Tipo = 3 ) or ( @Tipo = 8 ) or ( @Tipo = 9 ) )
             Select @M1200 = 104 * @M1200;
          else
          if ( @Tipo = 4 )
             Select @M1200 = 208 * @M1200;
          else
          if ( @Tipo = 5 )
             Select @M1200 = 72 * @M1200;
          EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1200, @Mes, @Factor, @M1200;
     end
     if ( @M1201 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1201, @Mes, @Factor, @M1201;
     if ( @M1202 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1202, @Mes, @Factor, @M1202;
     if ( @M1203 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1203, @Mes, @Factor, @M1203;
     if ( @M1204 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1204, @Mes, @Factor, @M1204;
     if ( @M1205 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1205, @Mes, @Factor, @M1205;
     if ( @M1206 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1206, @Mes, @Factor, @M1206;
     if ( @M1207 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1207, @Mes, @Factor, @M1207;
     if ( @M1208 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1208, @Mes, @Factor, @M1208;
     if ( @M1209 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1209, @Mes, @Factor, @M1209;
     if ( @M1210 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1210, @Mes, @Factor, @M1210;
     if ( @M1211 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1211, @Mes, @Factor, @M1211;
     if ( @M1212 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1212, @Mes, @Factor, @M1212;
     if ( @M1213 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1213, @Mes, @Factor, @M1213;
     if ( @M1214 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1214, @Mes, @Factor, @M1214;
     if ( @M1215 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1215, @Mes, @Factor, @M1215;
	if ( @M1216 <> 0 )
	   EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1216, @Mes, @Factor, @M1216;

     Declare TemporalMovimien Cursor for
        select M.CO_NUMERO, SUM( M.MO_PERCEPC + M.MO_DEDUCCI )
        from MOVIMIEN M where
               ( M.MO_ACTIVO = 'S' ) and
               ( M.PE_YEAR = @Anio ) and
               ( M.PE_TIPO = @Tipo ) and
               ( M.PE_NUMERO = @Numero ) and
               ( M.CB_CODIGO = @Empleado )
        group by M.CO_NUMERO
     open TemporalMovimien
     Fetch next	from TemporalMovimien
     into  @Concepto, @M1000
     WHILE @@FETCH_STATUS = 0
     begin
          EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, @Concepto, @Mes, @Factor, @M1000;
     	  Fetch next from TemporalMovimien
     	  into  @Concepto, @M1000
     end

     CLOSE TemporalMovimien
     DEALLOCATE TemporalMovimien


     select @M1004 = sum( M.MO_PERCEPC )
     from MOVIMIEN M left outer join CONCEPTO C on C.CO_NUMERO=M.CO_NUMERO
     where
               ( M.MO_ACTIVO = 'S' ) and
               ( M.PE_YEAR = @Anio ) and
               ( M.PE_TIPO = @Tipo ) and
               ( M.PE_NUMERO = @Numero ) and
               ( M.CB_CODIGO = @Empleado ) and
               ( C.CO_TIPO = 1 ) and
               ( C.CO_A_PTU = 'S' )

     if ( @M1004 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1004, @Mes, @Factor, @M1004;
END
GO

/* SP_VALIDA_CURSO_ASIG: Validar curso asignado (PRO:Cursos en Supervisores)*/
/* Patch 410 # Seq: 277 Agregado */
CREATE PROCEDURE SP_VALIDA_CURSO_ASIG( @CB_CODIGO NumeroEmpleado, 
							   @CU_CODIGO Codigo,
							   @CB_PUESTO Codigo, 
							   @CB_CLASIFI Codigo, 
							   @CB_TURNO Codigo, 
							   @KC_EVALUA Pesos,
							   @KC_FEC_TOM Fecha,
							   @KC_HORAS Pesos, 							    
							   @CB_NIVEL1 Codigo, 
							   @CB_NIVEL2 Codigo,
							   @CB_NIVEL3 Codigo, 
							   @CB_NIVEL4 Codigo, 
							   @CB_NIVEL5 Codigo, 
							   @CB_NIVEL6 Codigo, 
						        @CB_NIVEL7 Codigo,
							   @CB_NIVEL8 Codigo, 
							   @CB_NIVEL9 Codigo, 
							   @MA_CODIGO Codigo, 
							   @KC_FEC_FIN Fecha, 
						        @KC_REVISIO NombreCampo, 
							   @SE_FOLIO Codigo, 
							   @MensajeError Formula OUTPUT,
							   @Status Codigo1 OUTPUT )
AS
BEGIN
	SET NOCOUNT ON	
	/* Siempre manda status 0 ( Proceder ) y ningun mensaje de error en la base instalada*/

	set @Status = 0
	set @MensajeError = ''
	
END
GO

/* SP_VALIDA_CAMPOS_SESION: Validar campos de sesi�n (PRO:Cursos en Supervisores)*/
/* Patch 410 # Seq: 278 Agregado */
CREATE PROCEDURE SP_VALIDA_CAMPOS_SESION( @MA_CODIGO Codigo,
								  @SE_LUGAR Observaciones,
								  @SE_FEC_INI Fecha,
								  @SE_HOR_INI Hora,
								  @SE_FEC_FIN Fecha,
								  @SE_HOR_FIN Hora,
								  @SE_HORAS Horas,
								  @SE_CUPO FolioChico,
								  @SE_STATUS Status,
								  @SE_COSTO1 Pesos,
								  @SE_COSTO2 Pesos,
								  @SE_COSTO3 Pesos,
								  @SE_COMENTA Observaciones, 
 								  @SE_REVISIO NombreCampo,	
							       @MensajeError Formula OUTPUT,
							       @Status Codigo1 OUTPUT )
AS
BEGIN
	SET NOCOUNT ON	
	/* Siempre manda status 0 ( Proceder ) y ningun mensaje de error en la base instalada*/

	set @Status = 0
	set @MensajeError = ''
	
END
GO

/* SP_KARDEX_CLASIFICACION : Obtener la clasificacion del empleado en una fecha dada , si no existe en esa fecha regresar 
la ultima clasificacion  registrada*/
/* Patch 410 # Seq: 169 Modificado */
ALTER PROCEDURE SP_KARDEX_CLASIFICACION
    @EMPLEADO INTEGER,
    @FECHA DATETIME,
    @CB_AUTOSAL CHAR(1) OUTPUT,
    @CB_CLASIFI CHAR(6) OUTPUT,
    @CB_CONTRAT CHAR(1) OUTPUT,
    @CB_TURNO CHAR(6) OUTPUT,
    @CB_PUESTO CHAR(6) OUTPUT,
    @CB_TABLASS CHAR(1) OUTPUT,
    @CB_NIVEL1 CHAR(6) OUTPUT,
    @CB_NIVEL2 CHAR(6) OUTPUT,
    @CB_NIVEL3 CHAR(6) OUTPUT,
    @CB_NIVEL4 CHAR(6) OUTPUT,
    @CB_NIVEL5 CHAR(6) OUTPUT,
    @CB_NIVEL6 CHAR(6) OUTPUT,
    @CB_NIVEL7 CHAR(6) OUTPUT,
    @CB_NIVEL8 CHAR(6) OUTPUT,
    @CB_NIVEL9 CHAR(6) OUTPUT
AS
    SET NOCOUNT ON

	declare Temporal CURSOR FOR
	select CB_AUTOSAL, CB_CLASIFI, CB_CONTRAT,
               CB_TURNO, CB_PUESTO, CB_TABLASS,
               CB_NIVEL1, CB_NIVEL2, CB_NIVEL3,
               CB_NIVEL4, CB_NIVEL5, CB_NIVEL6,
               CB_NIVEL7, CB_NIVEL8, CB_NIVEL9
 	from KARDEX where ( CB_CODIGO = @Empleado ) and
                          ( CB_FECHA <= @Fecha  )
 	order by CB_FECHA desc, CB_NIVEL desc

	open Temporal

	fetch next from Temporal
        into @CB_AUTOSAL, @CB_CLASIFI, @CB_CONTRAT,
             @CB_TURNO, @CB_PUESTO, @CB_TABLASS,
             @CB_NIVEL1, @CB_NIVEL2, @CB_NIVEL3,
             @CB_NIVEL4, @CB_NIVEL5, @CB_NIVEL6,
             @CB_NIVEL7, @CB_NIVEL8, @CB_NIVEL9

	close Temporal
	deallocate Temporal
	
	if ( @CB_PUESTO is null )
	BEGIN
		declare Temporal2 CURSOR FOR
		select  CB_AUTOSAL, CB_CLASIFI, CB_CONTRAT,
               CB_TURNO, CB_PUESTO, CB_TABLASS,
               CB_NIVEL1, CB_NIVEL2, CB_NIVEL3,
               CB_NIVEL4, CB_NIVEL5, CB_NIVEL6,
               CB_NIVEL7, CB_NIVEL8, CB_NIVEL9
 		from KARDEX where ( CB_CODIGO = @Empleado )
 		order by CB_FECHA desc, CB_NIVEL desc

		open Temporal2

		fetch next from Temporal2
        into @CB_AUTOSAL, @CB_CLASIFI, @CB_CONTRAT,
             @CB_TURNO, @CB_PUESTO, @CB_TABLASS,
             @CB_NIVEL1, @CB_NIVEL2, @CB_NIVEL3,
             @CB_NIVEL4, @CB_NIVEL5, @CB_NIVEL6,
             @CB_NIVEL7, @CB_NIVEL8, @CB_NIVEL9

		close Temporal2
		deallocate Temporal2	
	END

GO



/*****************************************/
/**************** PORTAL******************/
/*****************************************/


/* Get_CursosProgramados: Listado de cursos programados (PORTAL)*/
/* Patch 410 # Seq: 279 Agregado */

CREATE procedure Get_CursosProgramados 
@CB_CODIGO int
as

SELECT CP.CU_CODIGO, CP.KC_FEC_PRO, CP.KC_EVALUA, 
CP.KC_FEC_TOM, CP.KC_REVISIO, CP.KC_HORAS, 
CP.CU_HORAS, CP.EN_OPCIONA, CP.KC_PROXIMO, C.CU_NOMBRE
from CUR_PROG CP
left outer join CURSO C 
	on( CP.CU_CODIGO = C.CU_CODIGO ) 
left outer join COLABORA 
	on( CP.CB_CODIGO = COLABORA.CB_CODIGO ) 
where( CP.CB_CODIGO = @CB_CODIGO ) 
	and ((CP.EN_LISTA = 'N') 
	or (COLABORA.CB_CLASIFI in (select ET_CODIGO from ENTNIVEL 
					where (ENTNIVEL.PU_CODIGO = CP.CB_PUESTO) 
					and (ENTNIVEL.CU_CODIGO = CP.CU_CODIGO))))
					order by KC_FEC_PRO 

GO


/* GET_EDAD: Obtiene en rango de fechas (PORTAL)*/
/* Patch 410 # Seq: 280 Agregado */

CREATE FUNCTION dbo.GET_EDAD 
(@FechaIni DATETIME, 
@FechaHasta DATETIME ) 
RETURNS VARCHAR(18) 
AS 

BEGIN 

DECLARE 
@FechaHoy DATETIME, 
@mes1 INT, 
@mes2 INT, 
@anios INT, 
@meses INT, 
@Edad VARCHAR(18) 

SELECT @anios=DATEDIFF(month,@FechaIni, @FechaHasta )/12 

SELECT    @mes1=(DATEPART(month,@FechaIni)), 
   @mes2 =(DATEPART(month,@FechaHasta)) 

IF @mes1 > @mes2 
BEGIN 
   SELECT @meses = (12 -(@mes1 - DATEPART(month,@FechaHasta))) 
END 
ELSE 
BEGIN 
   SELECT @meses = ( @mes2 - DATEPART(month,@FechaIni)) 
END 

RETURN (CONVERT(VARCHAR(3),@anios) + ' A�os ' + CONVERT(varchar(2),@meses) + ' Meses ')    
END 

go

/* SP_STATUS_EVALUADORES: Evaluaci�n de Datos: Obtiene status de los evaluadores (PORTAL)*/
/* Patch 410 # Seq: 281 Agregado */

CREATE FUNCTION SP_STATUS_EVALUADORES (
  @ET_CODIGO FolioGrande
)
RETURNS @StatusEvaluadores TABLE
( US_CODIGO  Smallint,
  SE_TOTAL   Integer,
  SE_NUEVA   Integer,
  SE_TERMINA Integer,
  SE_PROCESO Integer,
  SE_STATUS  Smallint
) AS     
BEGIN
	INSERT INTO @StatusEvaluadores
	SELECT US_CODIGO, COUNT(*), 
               SUM( CASE WHEN EV_STATUS <= 2 THEN 1 ELSE 0 END ),
               SUM( CASE WHEN EV_STATUS >= 4 THEN 1 ELSE 0 END ),
               SUM( CASE WHEN EV_STATUS = 3 THEN 1 ELSE 0 END ),
               0
	FROM   EVALUA
	WHERE  ET_CODIGO = @ET_CODIGO
	GROUP BY US_CODIGO

	UPDATE @StatusEvaluadores
	SET    SE_STATUS = CASE WHEN SE_NUEVA = SE_TOTAL THEN 2
                                WHEN SE_TERMINA = SE_TOTAL THEN 4 ELSE 3 END


	RETURN
END
GO



/* GET_PORC_CUMP_PUESTO: Evaluaci�n: Porcentaje de un empleado en un puesto indicado (PORTAL)*/
/* Patch 410 # Seq: 282 Agregado */
CREATE FUNCTION GET_PORC_CUMP_PUESTO ( @EMPLEADO NUMEROEMPLEADO, @PUESTO CODIGO )
RETURNS FLOAT
AS
begin

     declare @ORDEN1 INTEGER;
     declare @ORDEN2 INTEGER;
     declare @CUANTOS INTEGER;
     declare @CONTADOR INTEGER;
     declare @CUMPLE FLOAT;

     select @CONTADOR = 0;
     select @CUANTOS = 0;

     declare COMPETENCIAS cursor for
     select C1.CA_ORDEN, C2.CA_ORDEN from COMP_PTO
     left outer join COMPETEN on ( COMPETEN.CM_CODIGO = COMP_PTO.CM_CODIGO  )
     left outer join EMP_COMP on ( EMP_COMP.CM_CODIGO = COMP_PTO.CM_CODIGO ) and ( EMP_COMP.CB_CODIGO = @EMPLEADO )
     left outer join CALIFICA C2 on ( C2.CA_CODIGO = COMP_PTO.CA_CODIGO )
     left outer join CALIFICA C1 on ( C1.CA_CODIGO = EMP_COMP.CA_CODIGO )
     where ( COMP_PTO.PU_CODIGO = @PUESTO );
     open COMPETENCIAS;
     fetch NEXT from COMPETENCIAS into @ORDEN1, @ORDEN2;
     while ( @@Fetch_Status = 0 )
     begin
          if ( @ORDEN2 is not NULL )
          begin
	       if ( @ORDEN1 >= @ORDEN2 )
	       begin
	            select @CONTADOR = @CONTADOR + 1;
           end
           
			select @CUANTOS = @CUANTOS + 1;
          end
          fetch NEXT from COMPETENCIAS into @ORDEN1, @ORDEN2;
     end;
     close COMPETENCIAS;
     deallocate COMPETENCIAS;


     if ( @CUANTOS > 0 )
     begin

          select @CUMPLE = ROUND( ( ( @CONTADOR * 100 / @CUANTOS )  ), 0, 1 );
     end
     else
     begin
          select @CUMPLE = 0;
     end

RETURN @CUMPLE
END
GO


/* EFICIENCIA_POR_AREA_POR_FECHA: Labor: Porcentaje de un empleado en un puesto indicado (PORTAL)*/
/* Patch 410 # Seq: 283 Agregado */

CREATE FUNCTION Eficiencia_Por_Area_Por_Fecha(
	@Area		VARCHAR(30)
,	@Inicial	Fecha
,	@Final		Fecha )
RETURNS @Tabla	TABLE (
	LlaveArea	INT
,	Codigo		CHAR(6)
,	Nombre		VARCHAR(30)
,	Fecha		DATETIME
,	Invertidas	NUMERIC(15,2)
,	Ganadas	NUMERIC(15,2)
,	Eficiencia	NUMERIC(15,2) DEFAULT 0 )
AS
BEGIN

	INSERT INTO @Tabla ( 
		LlaveArea
	,	Codigo
	,	Nombre
	,	Fecha
	,	Invertidas
	,	Ganadas 
	)
	SELECT	AREA.LLAVE
	,	CB_AREA
	,	TB_ELEMENT
	,	AU_FECHA
	,	SUM(WK_TIEMPO/60.0)
	,	ROUND(SUM(WK_PIEZAS / WK_STD_HR ),4 )
	FROM	WORKS 
		JOIN AREA 
			ON CB_AREA = TB_CODIGO
	WHERE	WK_TIPO	= 0 
	AND	AU_FECHA	BETWEEN @Inicial AND @Final 
	AND	CB_AREA	LIKE @Area
	GROUP BY 
		AREA.LLAVE
	,	CB_AREA
	,	TB_ELEMENT
	,	AU_FECHA
	ORDER BY
		CB_AREA

	/* Segunda pasada, calcula eficiancia sobre totales */
	UPDATE @Tabla
	SET	Eficiencia	= Ganadas/ Invertidas * 100.0
	WHERE	Invertidas	> 0

	RETURN
END
GO


/* TIEMPOMUERTO_POR_MOTIVO_DETALLE: Labor: Tiempo Muerto (PORTAL)*/
/* Patch 410 # Seq: 284 Agregado */

CREATE FUNCTION TiempoMuerto_Por_Motivo_Detalle(
	@Inicial	Fecha,
	@Final		Fecha )
RETURNS @Tabla	TABLE (
	Codigo		CHAR(15)
,	Nombre		VARCHAR(30)
,	HorasHombre	NUMERIC(15,2)
,	Area		VARCHAR(10)
,	NombreArea	VARCHAR(100)
,	Fecha		DATETIME
,	Porcentaje	NUMERIC(15,2) DEFAULT 0 )
AS
BEGIN
	INSERT INTO @Tabla ( Codigo, Nombre, Area, NombreArea, Fecha, HorasHombre )

	SELECT	W.WK_TMUERTO
	,	TM.TB_ELEMENT
	,	W.CB_AREA
	,	A.TB_ELEMENT
	,	W.AU_FECHA
	,	SUM(W.WK_TIEMPO/60.0)
	FROM	WORKS W 
			JOIN TMUERTO TM 
				ON TM.TB_CODIGO	= W.WK_TMUERTO
			JOIN AREA A
				ON A.TB_CODIGO	= W.CB_AREA
	WHERE	W.WK_TIPO = 2 
	AND	W.AU_FECHA BETWEEN @Inicial AND @Final
	GROUP BY
		W.WK_TMUERTO
	,	TM.TB_ELEMENT
	,	W.CB_AREA
	,	A.TB_ELEMENT
	,	W.AU_FECHA
	ORDER BY
		W.WK_TMUERTO
	,	W.CB_AREA
	,	W.AU_FECHA
	/* Segunda pasada, calcula porcentaje */
	UPDATE @Tabla
	SET	Porcentaje = HorasHombre / ( SELECT SUM(HorasHombre) FROM @Tabla ) * 100.0

	RETURN
END
GO


/* F_GRAF_EVALUACIONES: Evaluaci�n de Datos: Obtiene la gr�fica de Evaluaciones (PORTAL)*/
/* Patch 410 # Seq: 285 Agregado */
CREATE FUNCTION F_GRAF_EVALUACIONES (
  @ET_CODIGO FolioGrande
)
RETURNS @StatusGrafica2 TABLE
( 
  STATUS integer,
  CANTIDAD Integer

) AS     
BEGIN
	DECLARE @Valor Integer;
	DECLARE @Contador Integer;
	SET @Contador = 0

	WHILE @Contador <=4

	BEGIN	

	SELECT @Valor = ( SUM( CASE WHEN EV_STATUS = @Contador THEN 1 ELSE 0 END ) ) 
	FROM   EVALUA
	WHERE  ET_CODIGO = @ET_CODIGO

    INSERT INTO @StatusGrafica2 values ( @Contador,@Valor);
	SET @Contador = @Contador+1
	END

	RETURN
END
GO

/* F_GRAF_EVALUADORES: Evaluaci�n de Datos: Obtiene la gr�fica de Evaluadores (PORTAL)*/
/* Patch 410 # Seq: 286 Agregado */
CREATE  FUNCTION F_GRAF_EVALUADORES (
  @ET_CODIGO FolioGrande
)
RETURNS @StatusGrafica4 TABLE
( 
  STATUS integer,
  CANTIDAD Integer

) AS     
BEGIN
	DECLARE @Valor Integer;
	DECLARE @Contador Integer;
	DECLARE @Contador2 Integer;
	DECLARE @TValor Integer;
	DECLARE @Temporal Integer;

	SET @Contador = 0
	SET @TValor = 0
	SET @Temporal = 0

	WHILE @Contador <=4
	BEGIN
	
		IF @Contador=0 
		BEGIN
			select @Valor = SUM( CASE WHEN SE_STATUS <=2 THEN 1 ELSE 0 END )
			FROM DBO.SP_STATUS_EVALUADORES(@ET_CODIGO)
			SET @Contador2 = 0
			INSERT INTO @StatusGrafica4 values ( @Contador2,@Valor);
			SET @Contador=2		
		END
		
		IF @Contador>=4
		BEGIN
			select @Valor = SUM( CASE WHEN SE_STATUS >=4 THEN 1 ELSE 0 END )
			FROM   DBO.SP_STATUS_EVALUADORES(@ET_CODIGO)
			SET @Contador2 = @Contador
			INSERT INTO @StatusGrafica4 values ( @Contador2,@Valor);
		END

		IF @Contador=3
			BEGIN
			select @Valor = SUM( CASE WHEN SE_STATUS =3 THEN 1 ELSE 0 END )
			FROM   DBO.SP_STATUS_EVALUADORES(@ET_CODIGO)
			SET @Contador2 = @Contador
			INSERT INTO @StatusGrafica4 values ( @Contador2,@Valor);
		END
		SET @Contador = @Contador+1

	END

	RETURN
END
GO

/* F_GRAF_EVALUADOS: Evaluaci�n de Datos: Obtiene la gr�fica de los evaluados (PORTAL)*/
/* Patch 410 # Seq: 287 Agregado */
CREATE FUNCTION F_GRAF_EVALUADOS (
  @ET_CODIGO FolioGrande
)
RETURNS @StatusGrafica3 TABLE
( 
  STATUS integer,
  CANTIDAD Integer

) AS     
BEGIN
	DECLARE @Valor Integer;
	DECLARE @Contador Integer;
	DECLARE @Contador2 Integer;
	SET @Contador = 0

	WHILE @Contador <=4
	BEGIN	
		IF @Contador=0 
		BEGIN
			SELECT @Valor = ( SUM( CASE WHEN SJ_STATUS <= 2 THEN 1 ELSE 0 END ) ) 
			FROM   SUJETO
			WHERE  ET_CODIGO = @ET_CODIGO
			SET @Contador2 = 0
			INSERT INTO @StatusGrafica3 values ( @Contador2,@Valor);
			SET @Contador=2		
		END

		IF @Contador>=4 
		BEGIN

			SELECT @Valor = ( SUM( CASE WHEN SJ_STATUS >=4 THEN 1 ELSE 0 END ) ) 
			FROM   SUJETO
			WHERE  ET_CODIGO = @ET_CODIGO
			SET @Contador2 = @Contador
			INSERT INTO @StatusGrafica3 values ( @Contador2,@Valor);
		END

		IF @Contador=3
		BEGIN

			SELECT @Valor = ( SUM( CASE WHEN SJ_STATUS = 3 THEN 1 ELSE 0 END ) ) 
			FROM   SUJETO
			WHERE  ET_CODIGO = @ET_CODIGO
			SET @Contador2 = @Contador
			INSERT INTO @StatusGrafica3 values ( @Contador2,@Valor);
		END
		
		SET @Contador = @Contador+1
	END

	RETURN
END
GO

/* SP_GET_DETALLE_AHORROS: Pr�stamos y Ahorros: Obtiene detalle de ahorros de un empleado (PORTAL)*/
/* Patch 410 # Seq: 288 Agregado */

CREATE PROCEDURE SP_GET_DETALLE_AHORROS
@CB_CODIGO INT,
@AHORRO_TIPO INT
AS
BEGIN
	SET NOCOUNT ON
	
	/* CONSTANTES para los Querys*/
	DECLARE
	@TIPODENTRONOMINA 	CHAR(1), 	/* Indica si el movimiento se realizo por n�mina('M') */
	@TIPOFUERANOMINA 	CHAR(1),	/* Indica si el movimiento se realizo por fuera de la n�mina('A') */
	@PERIODOSTATUS		SMALLINT,	/* Valor del estatus del periodo de n�mina */
	@MOVIMIENTOACTIVO	CHAR(1),	/* Indica el tipo de movimientos que se validaran */
	@USUARIO_PORTAL		INT,		/* Usuario fijo para la consulta en la tabla de TMPESTAD, este usuario no existe en la tabla de Comparte.Usuario */
	@US_CODIGO			INT			/* Se utiliza -1 como constante, solo para realizar la consulta */

	/*Se inicializan las constantes*/
	SELECT 	@USUARIO_PORTAL	= 5000,
			@TIPODENTRONOMINA='M',
			@TIPOFUERANOMINA ='A',
			@PERIODOSTATUS =6,
			@MOVIMIENTOACTIVO='S',
			@US_CODIGO=-1

	/*VARIABLES en las que se almacena la informaci�n que se va a procesar en el cursor*/	
	DECLARE

	/*Variables del cursor LISTA_AHORROS_CURSOR*/
	@L_CB_CODIGO 	INT,
	@L_AH_TIPO 		CHAR(1),
	@L_AH_FECHA 	DATETIME,
	@TB_CONCEPT 	NUMERIC(15,2),
	@L_AH_SALDO_I 	NUMERIC(15,2),
	@L_AH_SALDO 	NUMERIC(15,2),
	@L_AH_MONTO 	NUMERIC(15,2),
	@L_AH_REFEREN 	VARCHAR(8),
	@L_US_CODIGO	SMALLINT,
	@L_AH_ABONOS	NUMERIC(15,2),
	@L_AH_CARGOS	NUMERIC(15,2),
	@L_AH_TOTAL		NUMERIC(15,2),
	@L_AH_NUMERO	SMALLINT,
	@L_AH_STATUS 	SMALLINT,
	@L_AH_STATUS_DES VARCHAR(8),
	
	/*Variables del cursor MOVIMIENTOS_AHORRO_CURSOR*/
	@D_CB_CODIGO	INT, 
	@D_TE_TIPO		CHAR(1),
	@D_TE_REFEREN	VARCHAR(8),
	@D_PE_FEC_INI	DATETIME,	
	@D_MO_PERCEPC	NUMERIC(15,2),
	@D_MO_DEDUCCI	NUMERIC(15,2),
	@D_US_CODIGO	SMALLINT,
	@D_PE_YEAR		SMALLINT,
	@D_PE_TIPO		CHAR(1),
	@D_PE_NUMERO	SMALLINT, 
	@D_CO_NUMERO	SMALLINT,
	@D_TIPO			CHAR(1),

	/*Variables utilizadas para los calculos*/	
	@TE_CODIGO		INT,
	@TE_CODIGO_DET	INT ,
	@SALDO			NUMERIC(15,2),
	@DESCRIPCION 	VARCHAR(30),
	@NOMINA_DESC	VARCHAR(30),
	@REFERENCIA		INT,
	@ALTERN_ROW		INT,
	@FECHA_PERIODO	DATETIME

	/*Se inicializan las variables de calculos*/
	SELECT	@TE_CODIGO=0,@TE_CODIGO_DET=0,@SALDO=0,@REFERENCIA=1,@ALTERN_ROW=0

/*----------------------------------------------------------------------------------
	1.- SE ELIMINAN TODOS LOS REGISTROS DE LA TABLA TEMPESTAD DEL USUARIO DE PORTAL 
------------------------------------------------------------------------------------*/
	DELETE FROM TMPESTAD WHERE TE_USER = @USUARIO_PORTAL
/*----------------------------------------------------------------------------------
	2.- SE CREA TABLA TEMPORAL 
------------------------------------------------------------------------------------*/
	CREATE TABLE #TMP_AHORROS
	(
		[TE_USER] 		SMALLINT NOT NULL ,
		[TE_CODIGO] 	SMALLINT NOT NULL ,
		[TE_FECHA] 		DATETIME NOT NULL ,
		[TE_CARGO] 		NUMERIC(15,2) NOT NULL ,
		[TE_ABONO] 		NUMERIC(15,2) NOT NULL ,
		[TE_SALDO] 		NUMERIC(15,2) NOT NULL ,
		[US_CODIGO] 	SMALLINT NOT NULL ,
		[TE_DESCRIP] 	VARCHAR(30) NOT NULL ,
		[CB_CODIGO] 	INT NOT NULL ,
		[TE_TIPO] 		CHAR(6) NOT NULL ,
		[TE_REFEREN] 	VARCHAR(8) NOT NULL,
		[A_ANIOS_ANT] 	NUMERIC(15,2) NOT NULL ,
		[OTROS_ABONOS] 	NUMERIC(15,2) NOT NULL ,
		[OTROS_CARGOS] 	NUMERIC(15,2) NOT NULL ,
		[SALDO_ACTUAL] 	NUMERIC(15,2) NOT NULL ,
		[AH_TOTAL] 		NUMERIC(15,2) NOT NULL ,
		[AH_NUMERO] 	SMALLINT NOT NULL ,
		[AH_STATUS] 	SMALLINT NOT NULL ,
		[AH_STATUS_DES] VARCHAR(10),
		[TIPO_REGISTRO] VARCHAR(10),
		[ALTERN_ROW] 	INT
	)
/*----------------------------------------------------------------------------
	3.- SE OBTIENE LA FECHA DEL ULTIMO PERIODO PARA EL EMPLEADO
------------------------------------------------------------------------------*/
		SELECT @FECHA_PERIODO=(SELECT MAX(PE.PE_FEC_FIN) MAXIMO 
								    FROM PERIODO PE
									INNER JOIN NOMINA N
										ON PE.PE_YEAR=N.PE_YEAR
										AND PE.PE_TIPO=N.PE_TIPO
										AND PE.PE_NUMERO=N.PE_NUMERO
									INNER JOIN TURNO T
										ON N.CB_TURNO=T.TU_CODIGO
									WHERE PE.PE_STATUS = @PERIODOSTATUS
										AND N.CB_CODIGO= @CB_CODIGO)
/*----------------------------------------------------------------------------
	4.- CURSOR PARA OBTENER EL LISTADO DE LOS AHORROS DEL EMPLEADO
------------------------------------------------------------------------------*/

	DECLARE  LISTA_AHORROS_CURSOR CURSOR FOR
		SELECT 	A.CB_CODIGO,		A.AH_TIPO,		A.AH_FECHA,
				T.TB_CONCEPT,	 	A.AH_SALDO_I,	A.AH_SALDO,	0 AS 'AH_MONTO', 
				'' AS AH_REFEREN,	A.US_CODIGO,	A.AH_ABONOS,
	        	A.AH_CARGOS,		A.AH_TOTAL,		A.AH_NUMERO,
				A.AH_STATUS,		
				'A.AH_STATUS_DES'=
				CASE
					WHEN A.AH_STATUS=0 THEN 'Activo'
					WHEN A.AH_STATUS=1 THEN 'Saldado'
					WHEN A.AH_STATUS=2 THEN 'Cancelado' 
				END 
		FROM AHORRO A
		INNER JOIN TAHORRO T
			ON  A.AH_TIPO= T.TB_CODIGO
		WHERE 	A.CB_CODIGO=@CB_CODIGO
			AND A.AH_TIPO=@AHORRO_TIPO
			AND A.AH_FECHA<(@FECHA_PERIODO)
			AND ((A.AH_ABONOS+AH_CARGOS<>0)
			OR (A.AH_NUMERO<>0 ))
		ORDER BY A.CB_CODIGO,
				 A.AH_TIPO

	OPEN LISTA_AHORROS_CURSOR

	FETCH NEXT FROM LISTA_AHORROS_CURSOR 
	INTO 	@L_CB_CODIGO ,	@L_AH_TIPO,		@L_AH_FECHA,	@TB_CONCEPT ,
			@L_AH_SALDO_I,	@L_AH_SALDO,	@L_AH_MONTO,  	@L_AH_REFEREN,
			@L_US_CODIGO,	@L_AH_ABONOS,	@L_AH_CARGOS,	@L_AH_TOTAL,
			@L_AH_NUMERO,	@L_AH_STATUS,	@L_AH_STATUS_DES

	WHILE @@FETCH_STATUS = 0
	BEGIN

		SELECT  @TE_CODIGO	=@TE_CODIGO	+ 1,
				@SALDO		=@L_AH_MONTO + @L_AH_SALDO_I

	/*Se inserta un registro por cada ahorro en la tabla TempStad*/
		INSERT INTO TMPESTAD (TE_CODIGO,	CB_CODIGO,		TE_TIPO,		TE_FECHA, 
							 TE_DESCRIP,	TE_CARGO,		TE_ABONO,		TE_SALDO, 
							 US_CODIGO,		TE_USER,		TE_REFEREN )
					VALUES 	(@TE_CODIGO, 	@L_CB_CODIGO,	@L_AH_TIPO,		@L_AH_FECHA, 
							'SALDO INICIAL',@TB_CONCEPT, 	@L_AH_SALDO_I,	@SALDO, 
							@US_CODIGO, 	@USUARIO_PORTAL, @REFERENCIA)

	/*Se inserta registro en la tabla temporal para reporte final, este registro contiene los totales del ahorro*/
	INSERT INTO #TMP_AHORROS(TE_CODIGO,		CB_CODIGO,		TE_TIPO,		TE_FECHA, 	 
							TE_DESCRIP, 	TE_CARGO, 		TE_ABONO,		TE_SALDO,	
							US_CODIGO,		TE_USER,		TE_REFEREN,		A_ANIOS_ANT,
							OTROS_ABONOS,	OTROS_CARGOS,	SALDO_ACTUAL,	AH_TOTAL,	
							AH_NUMERO,		AH_STATUS,		AH_STATUS_DES,	TIPO_REGISTRO,
							ALTERN_ROW)
					VALUES	(@TE_CODIGO, 	@L_CB_CODIGO, 	@L_AH_TIPO,		@L_AH_FECHA, 
							'SALDO INICIAL',@TB_CONCEPT, 	0,				@SALDO, 
							@US_CODIGO, 	@USUARIO_PORTAL,@REFERENCIA,	@L_AH_SALDO_I,
							@L_AH_ABONOS,	@L_AH_CARGOS,	@L_AH_SALDO,	@L_AH_TOTAL,	
							@L_AH_NUMERO,	@L_AH_STATUS ,	@L_AH_STATUS_DES,'ENCABEZADO', 
							@ALTERN_ROW)
	/*---------------------------------------------------------------------------------------
		5.- CURSOR PARA OBTENER EL DETALLE DEL AHORRO EN REVISIO(MOVIMIENTOS_AHORRO_CURSOR)
			EL listado obtenido son los movimientos que ha realizado el empleado en su ahorro
	------------------------------------------------------------------------------------------*/
		DECLARE MOVIMIENTOS_AHORRO_CURSOR CURSOR FOR 
			SELECT 	TMPESTAD.CB_CODIGO, 
					TMPESTAD.TE_TIPO,
					TMPESTAD.TE_REFEREN,
					PERIODO.PE_FEC_INI PE_FEC_INI,	
					MOVIMIEN.MO_PERCEPC MO_PERCEPC,
					MOVIMIEN.MO_DEDUCCI MO_DEDUCCI,
					MOVIMIEN.US_CODIGO,
					NOMINA.PE_YEAR,
					NOMINA.PE_TIPO,
					NOMINA.PE_NUMERO, 
					MOVIMIEN.CO_NUMERO,
					@TIPODENTRONOMINA AS TIPO
			FROM TMPESTAD
			LEFT OUTER JOIN NOMINA 
				ON NOMINA.CB_CODIGO = TMPESTAD.CB_CODIGO
			LEFT OUTER JOIN PERIODO 
				ON PERIODO.PE_YEAR = NOMINA.PE_YEAR 
				AND PERIODO.PE_TIPO = 	NOMINA.PE_TIPO 
				AND PERIODO.PE_NUMERO = NOMINA.PE_NUMERO
			LEFT OUTER JOIN MOVIMIEN 
				ON NOMINA.PE_YEAR = MOVIMIEN.PE_YEAR 
				AND NOMINA.PE_TIPO = 	MOVIMIEN.PE_TIPO 
				AND NOMINA.PE_NUMERO = MOVIMIEN.PE_NUMERO 
				AND NOMINA.CB_CODIGO = MOVIMIEN.CB_CODIGO
			WHERE ( TMPESTAD.TE_USER = @USUARIO_PORTAL)
			AND ( TMPESTAD.US_CODIGO = @US_CODIGO )
			AND ( MOVIMIEN.CB_CODIGO = @CB_CODIGO)
			AND  ( MOVIMIEN.CO_NUMERO = TMPESTAD.TE_CARGO )
			AND  ( NOMINA.NO_STATUS	=	@PERIODOSTATUS)
			AND ( MOVIMIEN.MO_ACTIVO = @MOVIMIENTOACTIVO)
			AND  ( PERIODO.PE_FEC_FIN >= TMPESTAD.TE_FECHA )
			AND TMPESTAD.TE_REFEREN=@REFERENCIA
			UNION ALL
            SELECT	TMPESTAD.CB_CODIGO,
                  	TMPESTAD.TE_TIPO,
					TMPESTAD.TE_REFEREN,
					ACAR_ABO.CR_FECHA PE_FEC_INI,
					ACAR_ABO.CR_CARGO MO_PERCEPC,
					ACAR_ABO.CR_ABONO MO_DEDUCCI,
					ACAR_ABO.US_CODIGO,
					CAST(0 AS SMALLINT) PE_YEAR,
					CAST(0 AS SMALLINT) PE_TIPO,
					CAST(0 AS SMALLINT) PE_NUMERO,
					CAST(0 AS SMALLINT) CO_NUMERO,
					@TIPOFUERANOMINA AS TIPO
            FROM TMPESTAD
            LEFT OUTER JOIN ACAR_ABO 
				ON ACAR_ABO.CB_CODIGO = TMPESTAD.CB_CODIGO
                AND ACAR_ABO.AH_TIPO = TMPESTAD.TE_TIPO 
			WHERE TMPESTAD.TE_USER = @USUARIO_PORTAL
            	AND TMPESTAD.US_CODIGO = @US_CODIGO
				AND (ACAR_ABO.CB_CODIGO=@CB_CODIGO)
                AND (ACAR_ABO.CR_CARGO +ACAR_ABO.CR_ABONO)>0 
                AND TMPESTAD.TE_FECHA <= (@FECHA_PERIODO)
            ORDER BY 1,2,3,4

		   	OPEN MOVIMIENTOS_AHORRO_CURSOR
		   	FETCH NEXT FROM MOVIMIENTOS_AHORRO_CURSOR 
			INTO 	@D_CB_CODIGO, 	@D_TE_TIPO,		@D_TE_REFEREN,	@D_PE_FEC_INI,	
					@D_MO_PERCEPC,	@D_MO_DEDUCCI,	@D_US_CODIGO,	@D_PE_YEAR,
					@D_PE_TIPO,		@D_PE_NUMERO, 	@D_CO_NUMERO,	@D_TIPO
		
			   WHILE @@FETCH_STATUS = 0
			   BEGIN

				/*Se genera la descripci�n del movimiento*/
				
				SELECT 	@DESCRIPCION='',
						@NOMINA_DESC=(select TP_NOMBRE from tperiodo WHERE TP_TIPO=@D_PE_TIPO)

				/*Si el movimiento fue por nomina se genera etiqueta indicando la n�mina y el a�o*/
				IF(@D_TIPO=@TIPODENTRONOMINA)
				BEGIN
					SELECT @DESCRIPCION =@NOMINA_DESC + ' # ' + 
										 CONVERT(VARCHAR(30),@D_PE_NUMERO)+ ' A�o: ' +
										 CONVERT(VARCHAR(30),@D_PE_YEAR)
				END
				ELSE
				BEGIN
					/*	Si el movimiento fue por fuera de nomina se genera etiqueta 
						indicando Cargo o Abono segun sea el caso */
					IF((@D_MO_DEDUCCI=NULL) OR (@D_MO_DEDUCCI=0))
					BEGIN
						SELECT @DESCRIPCION='Cargo'
					END
					ELSE
					BEGIN
						SELECT @DESCRIPCION='Abono'
					END
				END

				/*Se calcula el Saldo que se genera con el movimiento*/		
				if(@D_MO_DEDUCCI<>0)
				BEGIN
					SELECT @SALDO=(@SALDO+@D_MO_DEDUCCI)
				END
				ELSE
				BEGIN
					SELECT @SALDO=(@SALDO-@D_MO_PERCEPC)
				END
				
				/* Se incrementa para ordenar los registros */
				SELECT @TE_CODIGO=@TE_CODIGO+1
				
				/* Se utiliza para alternar los renglones del grid*/
				IF(@ALTERN_ROW=0)
				BEGIN
					SELECT @ALTERN_ROW=1
				END
				ELSE
				BEGIN
					SELECT @ALTERN_ROW=0
				END
				
				/*	Se inserta el registro del movimiento a la tabla temporal para el reporte final*/
				INSERT INTO #TMP_AHORROS(TE_CODIGO,		CB_CODIGO,		TE_TIPO,		TE_FECHA, 	 
										TE_DESCRIP, 	TE_CARGO, 		TE_ABONO,		TE_SALDO,	
										US_CODIGO,		TE_USER,		TE_REFEREN,		A_ANIOS_ANT,
										OTROS_ABONOS,	OTROS_CARGOS,	SALDO_ACTUAL,	AH_TOTAL,	
										AH_NUMERO,		AH_STATUS,		AH_STATUS_DES,	TIPO_REGISTRO,
										ALTERN_ROW)
								VALUES(	@TE_CODIGO,		@D_CB_CODIGO, 	@D_TE_TIPO,		@D_PE_FEC_INI, 
										@DESCRIPCION, 	@D_MO_PERCEPC, 	@D_MO_DEDUCCI,	@SALDO, 
										@US_CODIGO,		@USUARIO_PORTAL,@D_TE_REFEREN,	0,
										0,				0,				0,				0,	
										0,				0,				'',				'GRID', 
										@ALTERN_ROW)

			FETCH NEXT FROM MOVIMIENTOS_AHORRO_CURSOR 
			INTO 	@D_CB_CODIGO, 	@D_TE_TIPO,		@D_TE_REFEREN,	@D_PE_FEC_INI,	
					@D_MO_PERCEPC,	@D_MO_DEDUCCI,	@D_US_CODIGO,	@D_PE_YEAR,
					@D_PE_TIPO,		@D_PE_NUMERO, 	@D_CO_NUMERO,	@D_TIPO
			   
			END
			
			CLOSE MOVIMIENTOS_AHORRO_CURSOR
			DEALLOCATE MOVIMIENTOS_AHORRO_CURSOR

   /*-----------------Termina MOVIMIENTOS_AHORRO_CURSOR-------------------------------------*/

	SELECT @REFERENCIA=@REFERENCIA+1, @ALTERN_ROW=0;

			/*	Se inserta el registro para indicar que finaliza el detalle del prestamo en revision*/
			INSERT INTO #TMP_AHORROS(TE_CODIGO,		CB_CODIGO,		TE_TIPO,		TE_FECHA, 	 
									TE_DESCRIP, 	TE_CARGO, 		TE_ABONO,		TE_SALDO,	
									US_CODIGO,		TE_USER,		TE_REFEREN,		A_ANIOS_ANT,
									OTROS_ABONOS,	OTROS_CARGOS,	SALDO_ACTUAL,	AH_TOTAL,	
									AH_NUMERO,		AH_STATUS,		AH_STATUS_DES,	TIPO_REGISTRO,
									ALTERN_ROW)
	
							VALUES(@TE_CODIGO, 		@L_CB_CODIGO, 	@L_AH_TIPO,		@L_AH_FECHA, 
									'---------------', @TB_CONCEPT, 0,				0, 
									@US_CODIGO, 	@USUARIO_PORTAL,@REFERENCIA,	0,
									0,				0,				0,				0,	
									0,				0,				'',				'FIN_GRID', 
									@ALTERN_ROW)
   /* Get the next Ahorro. */
   	FETCH NEXT FROM LISTA_AHORROS_CURSOR 
	INTO 	@L_CB_CODIGO ,	@L_AH_TIPO,		@L_AH_FECHA,	@TB_CONCEPT ,
			@L_AH_SALDO_I,	@L_AH_SALDO,	@L_AH_MONTO,  	@L_AH_REFEREN,
			@L_US_CODIGO,	@L_AH_ABONOS,	@L_AH_CARGOS,	@L_AH_TOTAL,
			@L_AH_NUMERO,	@L_AH_STATUS,	@L_AH_STATUS_DES

	END

	/* Close the LISTA_AHORROS_CURSOR */
	CLOSE LISTA_AHORROS_CURSOR
	DEALLOCATE LISTA_AHORROS_CURSOR

	SELECT * FROM #TMP_AHORROS
	DROP TABLE #TMP_AHORROS
END
GO

/* SP_GET_DETALLES_PRESTAMOS: Pr�stamos y Ahorros: Obtiene detalle de pr�stamos de un empleado (PORTAL)*/
/* Patch 410 # Seq: 289 Agregado */

CREATE PROCEDURE SP_GET_DETALLES_PRESTAMOS
	@CB_CODIGO INT,
	@PRE_TIPO INT
AS
BEGIN
	SET NOCOUNT ON
	
	/* CONSTANTES para los Querys*/
	DECLARE
	@TIPODENTRONOMINA 	CHAR(1), 	/* Indica si el movimiento se realizo por n�mina('M') */
	@TIPOFUERANOMINA 	CHAR(1),	/* Indica si el movimiento se realizo por fuera de la n�mina('P') */
	@PERIODOSTATUS		SMALLINT,	/* Valor del estatus del periodo de n�mina */
	@MOVIMIENTOACTIVO	CHAR(1),	/* Indica el tipo de movimientos que se validaran */
	@USUARIO_PORTAL		INT,		/* Usuario fijo para la consulta en la tabla de TMPESTAD, este usuario no existe en la tabla de Comparte.Usuario */
	@US_CODIGO			INT			/* Se utiliza -1 como constante, solo para realizar la consulta */

	/*Se inicializan las constantes*/
	SELECT 	@USUARIO_PORTAL		= 5000,
			@TIPODENTRONOMINA	='M',
			@TIPOFUERANOMINA 	='P',
			@PERIODOSTATUS 		=6,
			@MOVIMIENTOACTIVO	='S',
			@US_CODIGO			=-1

	/* VARIABLES en las que se almacena la informaci�n que se va a procesar en el cursor*/	
	
	DECLARE
	/*Variables del cursor LISTA_PRESTAMOS_CURSOR*/
	@L_CB_CODIGO 	INT,
	@L_PR_TIPO 		CHAR(1),
	@L_PR_FECHA 	DATETIME,
	@TB_CONCEPT 	NUMERIC(15,2),
	@L_PR_SALDO_I 	NUMERIC(15,2),
	@L_PR_SALDO 	NUMERIC(15,2),
	@L_PR_MONTO 	NUMERIC(15,2),
	@L_PR_REFEREN 	VARCHAR(8),
	@L_US_CODIGO	SMALLINT,
	@L_PR_ABONOS	NUMERIC(15,2),
	@L_PR_CARGOS	NUMERIC(15,2),
	@L_PR_TOTAL		NUMERIC(15,2),
	@L_PR_NUMERO	SMALLINT,
	@L_PR_STATUS 	SMALLINT,
	@L_PR_STATUS_DES VARCHAR(8),

	/*Variables del cursor MOVIMIENTOS_PRESTAMO_CURSOR*/
	@D_CB_CODIGO	INT, 
	@D_TE_TIPO		CHAR(1),
	@D_TE_REFEREN	VARCHAR(8),
	@D_PE_FEC_INI	DATETIME,	
	@D_MO_PERCEPC	NUMERIC(15,2),
	@D_MO_DEDUCCI	NUMERIC(15,2),
	@D_US_CODIGO	SMALLINT,
	@D_PE_YEAR		SMALLINT,
	@D_PE_TIPO		CHAR(1),
	@D_PE_NUMERO	SMALLINT, 
	@D_CO_NUMERO	SMALLINT,
	@D_TIPO			CHAR(1),

	/*Variables utilizadas para los calculos*/	
	@TE_CODIGO		INT,
	@TE_CODIGO_DET	INT ,
	@SALDO			NUMERIC(15,2),
	@DESCRIPCION 	VARCHAR(30),
	@NOMINA_DESC	VARCHAR(30),
	@ALTERN_ROW		INT,
	@FECHA_PERIODO	DATETIME


	/*Se inicializan las variables de calculos*/
	SELECT	@TE_CODIGO=0,@TE_CODIGO_DET=0,@SALDO=0,@ALTERN_ROW=0
/*----------------------------------------------------------------------------------
	1.- SE ELIMINAN TODOS LOS REGISTROS DE LA TABLA TEMPESTAD DEL USUARIO DE PORTAL 
------------------------------------------------------------------------------------*/
	DELETE FROM TMPESTAD WHERE TE_USER = @USUARIO_PORTAL
/*----------------------------------------------------------------------------------
	2.- SE CREA TABLA TEMPORAL PARA REPORTE FINAL
------------------------------------------------------------------------------------*/

CREATE TABLE #TMP_PRESTAMOS 
(
	[TE_USER] 		SMALLINT NOT NULL ,
	[TE_CODIGO] 	SMALLINT NOT NULL ,
	[TE_FECHA] 		DATETIME NOT NULL ,
	[TE_CARGO] 		NUMERIC(15,2) NOT NULL ,
	[TE_ABONO] 		NUMERIC(15,2) NOT NULL ,
	[TE_SALDO] 		NUMERIC(15,2) NOT NULL ,
	[US_CODIGO] 	SMALLINT NOT NULL ,
	[TE_DESCRIP] 	VARCHAR(30) NOT NULL ,
	[CB_CODIGO] 	INT NOT NULL ,
	[TE_TIPO] 		CHAR(6) NOT NULL ,
	[TE_REFEREN]	VARCHAR(8) NOT NULL,
	[P_ANIOS_ANT] 	NUMERIC(15,2) NOT NULL ,
	[OTROS_ABONOS] 	NUMERIC(15,2) NOT NULL ,
	[OTROS_CARGOS] 	NUMERIC(15,2) NOT NULL ,
	[SALDO_ACTUAL] 	NUMERIC(15,2) NOT NULL ,
	[PR_TOTAL] 		NUMERIC(15,2) NOT NULL ,
	[PR_NUMERO] 	SMALLINT NOT NULL ,
	[PR_STATUS] 	SMALLINT NOT NULL ,
	[PR_STATUS_DES] VARCHAR(10),
	[TIPO_REGISTRO] VARCHAR(10),
	[ALTERN_ROW] 	INT
)
/*----------------------------------------------------------------------------
	3.- SE OBTIENE LA FECHA DEL ULTIMO PERIODO PARA EL EMPLEADO
------------------------------------------------------------------------------*/
		SELECT @FECHA_PERIODO=(SELECT MAX(PE.PE_FEC_FIN) MAXIMO 
								    FROM PERIODO PE
									INNER JOIN NOMINA N
										ON PE.PE_YEAR=N.PE_YEAR
										AND PE.PE_TIPO=N.PE_TIPO
										AND PE.PE_NUMERO=N.PE_NUMERO
									INNER JOIN TURNO T
										ON N.CB_TURNO=T.TU_CODIGO
									WHERE PE.PE_STATUS = @PERIODOSTATUS
										AND N.CB_CODIGO= @CB_CODIGO)
/*----------------------------------------------------------------------------
	4.- CURSOR PARA OBTENER EL LISTADO DE LOS PRESTAMOS DEL EMPLEADO
------------------------------------------------------------------------------*/
	DECLARE  LISTA_PRESTAMOS_CURSOR CURSOR FOR
		SELECT 	P.CB_CODIGO,	P.PR_TIPO,		P.PR_FECHA,		T.TB_CONCEPT,
				P.PR_SALDO_I,	P.PR_SALDO,		P.PR_MONTO,		P.PR_REFEREN,
				P.US_CODIGO,	P.PR_ABONOS,	P.PR_CARGOS,	P.PR_TOTAL,
				P.PR_NUMERO,	P.PR_STATUS,	
				'PR_STATUS_DES'=
				CASE
					WHEN PR_STATUS=0 THEN 'Activo'
					WHEN PR_STATUS=1 THEN 'Saldado'
					WHEN PR_STATUS=2 THEN 'Cancelado' 
				END 
		FROM PRESTAMO P
		INNER JOIN TPRESTA T
			ON  P.PR_TIPO= T.TB_CODIGO
		WHERE P.CB_CODIGO=@CB_CODIGO
			AND P.PR_TIPO=@PRE_TIPO
			AND PR_STATUS=0
			AND P.PR_FECHA<(@FECHA_PERIODO)
			AND ((P.PR_ABONOS+PR_CARGOS<>0)
			OR (P.PR_NUMERO<>0 ))
	OPEN LISTA_PRESTAMOS_CURSOR

	FETCH NEXT FROM LISTA_PRESTAMOS_CURSOR 
	INTO 	@L_CB_CODIGO,	@L_PR_TIPO ,	@L_PR_FECHA,	@TB_CONCEPT ,	@L_PR_SALDO_I ,
			@L_PR_SALDO ,	@L_PR_MONTO,	@L_PR_REFEREN,	@L_US_CODIGO,	@L_PR_ABONOS,
			@L_PR_CARGOS,	@L_PR_TOTAL,	@L_PR_NUMERO,	@L_PR_STATUS,	@L_PR_STATUS_DES 

	WHILE @@FETCH_STATUS = 0
	BEGIN

	SELECT  @TE_CODIGO=@TE_CODIGO+1,
			@SALDO=@L_PR_MONTO - @L_PR_SALDO_I

				
	/*Se inserta un registro por cada Prestamo del empleado en la tabla TempStad*/
	INSERT INTO TMPESTAD(TE_CODIGO,		CB_CODIGO,		TE_TIPO,	TE_FECHA,	
						TE_DESCRIP,		TE_CARGO, 		TE_ABONO,	TE_SALDO, 	
						US_CODIGO, 		TE_USER, 		TE_REFEREN)
				VALUES (@TE_CODIGO, 	@L_CB_CODIGO, 	@L_PR_TIPO,	@L_PR_FECHA, 
						'SALDO INICIAL',@TB_CONCEPT, 	0,			@SALDO, 
						@US_CODIGO, 	@USUARIO_PORTAL,@L_PR_REFEREN)

	/*Se inserta registro en la tabla temporal para reporte final, este registro contiene los totales del prestamo*/
	INSERT INTO #TMP_PRESTAMOS
						(TE_CODIGO,		CB_CODIGO,		TE_TIPO,		TE_FECHA, 	 
						TE_DESCRIP, 	TE_CARGO, 		TE_ABONO,		TE_SALDO,	
						US_CODIGO,		TE_USER,		TE_REFEREN,		P_ANIOS_ANT,
						OTROS_ABONOS,	OTROS_CARGOS,	SALDO_ACTUAL,	PR_TOTAL,	
						PR_NUMERO,		PR_STATUS,		PR_STATUS_DES,	TIPO_REGISTRO,
						ALTERN_ROW)
	
				VALUES(@TE_CODIGO, 		@L_CB_CODIGO,	@L_PR_TIPO,		@L_PR_FECHA, 
						'SALDO INICIAL',@TB_CONCEPT, 	0,				@SALDO, 
						@US_CODIGO,		@USUARIO_PORTAL,@L_PR_REFEREN,	@L_PR_SALDO_I,
						@L_PR_ABONOS,	@L_PR_CARGOS,	@L_PR_SALDO,	@L_PR_TOTAL,	
						@L_PR_NUMERO,	@L_PR_STATUS,	@L_PR_STATUS_DES,'ENCABEZADO', 
						@ALTERN_ROW)

	/*---------------------------------------------------------------------------------------
		4.- CURSOR PARA OBTENER EL DETALLE DEL PRESTAMO REVISADO(MOVIMIENTOS_PRESTAMO_CURSOR)
	-----------------------------------------------------------------------------------------*/
		DECLARE MOVIMIENTOS_PRESTAMO_CURSOR CURSOR FOR 
			SELECT 	TMPESTAD.CB_CODIGO, 
					TMPESTAD.TE_TIPO,
					TMPESTAD.TE_REFEREN,
					PERIODO.PE_FEC_INI PE_FEC_INI,	
					MOVIMIEN.MO_PERCEPC MO_PERCEPC,
					MOVIMIEN.MO_DEDUCCI MO_DEDUCCI,
					MOVIMIEN.US_CODIGO,
					NOMINA.PE_YEAR,
					NOMINA.PE_TIPO,
					NOMINA.PE_NUMERO, 
					MOVIMIEN.CO_NUMERO,
					@TIPODENTRONOMINA AS TIPO
			FROM TMPESTAD
			LEFT OUTER JOIN NOMINA 
				ON NOMINA.CB_CODIGO = TMPESTAD.CB_CODIGO
			LEFT OUTER JOIN PERIODO 
				ON PERIODO.PE_YEAR = NOMINA.PE_YEAR 
				AND PERIODO.PE_TIPO = 	NOMINA.PE_TIPO 
				AND PERIODO.PE_NUMERO = NOMINA.PE_NUMERO
			LEFT OUTER JOIN MOVIMIEN 
				ON NOMINA.PE_YEAR = MOVIMIEN.PE_YEAR 
				AND NOMINA.PE_TIPO = 	MOVIMIEN.PE_TIPO 
				AND NOMINA.PE_NUMERO = MOVIMIEN.PE_NUMERO 
				AND NOMINA.CB_CODIGO = MOVIMIEN.CB_CODIGO
			WHERE ( TMPESTAD.TE_USER = @USUARIO_PORTAL)
			AND ( TMPESTAD.US_CODIGO = @US_CODIGO )
			AND ( MOVIMIEN.CB_CODIGO = @CB_CODIGO)
			AND  ( MOVIMIEN.CO_NUMERO = TMPESTAD.TE_CARGO )
			AND  ( NOMINA.NO_STATUS	=	@PERIODOSTATUS)
			AND  ( MOVIMIEN.MO_REFEREN= TMPESTAD.TE_REFEREN )
			AND ( MOVIMIEN.MO_ACTIVO = @MOVIMIENTOACTIVO)
			AND  ( PERIODO.PE_FEC_FIN >= TMPESTAD.TE_FECHA )
			AND TMPESTAD.TE_REFEREN=@L_PR_REFEREN
			 UNION ALL
			SELECT TMPESTAD.CB_CODIGO,TMPESTAD.TE_TIPO,TMPESTAD.TE_REFEREN,
				PCAR_ABO.CR_FECHA PE_FEC_INI,
				PCAR_ABO.CR_CARGO MO_PERCEPC,
				PCAR_ABO.CR_ABONO MO_DEDUCCI,
				PCAR_ABO.US_CODIGO,
				CAST(0 AS SMALLINT) PE_YEAR,
				CAST(0 AS SMALLINT) PE_TIPO,
				CAST(0 AS SMALLINT) PE_NUMERO,
				CAST(0 AS SMALLINT) CO_NUMERO,
				@TIPOFUERANOMINA AS TIPO
			FROM TMPESTAD
			LEFT OUTER JOIN PCAR_ABO 
				ON PCAR_ABO.CB_CODIGO = TMPESTAD.CB_CODIGO
				AND PCAR_ABO.PR_TIPO = TMPESTAD.TE_TIPO
				AND PCAR_ABO.PR_REFEREN = TMPESTAD.TE_REFEREN
			WHERE TMPESTAD.TE_USER = @USUARIO_PORTAL
			AND TMPESTAD.US_CODIGO = @US_CODIGO
			AND (PCAR_ABO.CR_CARGO +PCAR_ABO.CR_ABONO)>0
			AND TMPESTAD.TE_FECHA <= (@FECHA_PERIODO)
			AND TMPESTAD.TE_REFEREN=@L_PR_REFEREN
			ORDER BY 1,2,3,4

		OPEN MOVIMIENTOS_PRESTAMO_CURSOR
		FETCH NEXT FROM MOVIMIENTOS_PRESTAMO_CURSOR 
		INTO 	@D_CB_CODIGO, 	@D_TE_TIPO,		@D_TE_REFEREN,	@D_PE_FEC_INI,	
				@D_MO_PERCEPC,	@D_MO_DEDUCCI,	@D_US_CODIGO,	@D_PE_YEAR,
				@D_PE_TIPO,		@D_PE_NUMERO, 	@D_CO_NUMERO,	@D_TIPO
		
			WHILE @@FETCH_STATUS = 0
			BEGIN

			/*Se valida que tipo de movimiento se realizo y se obtiene su descripcion*/
				
				SELECT 	@DESCRIPCION='',
						@NOMINA_DESC=(select TP_NOMBRE from tperiodo WHERE TP_TIPO=@D_PE_TIPO)

				if(@D_TIPO=@TipoDentroNomina)
				begin
					SELECT @DESCRIPCION =@NOMINA_DESC + ' # ' + 
										 CONVERT(VARCHAR(30),@D_PE_NUMERO)+ ' A�o: ' +
										 CONVERT(VARCHAR(4),@D_PE_YEAR)
				end
				else
				begin
					IF((@D_MO_DEDUCCI=NULL) OR (@D_MO_DEDUCCI=0))
					BEGIN
						SELECT @DESCRIPCION='Cargo'
					END
					ELSE
					BEGIN
						SELECT @DESCRIPCION='Abono'
					END
				end

				/*Se calcula el saldo del prestamo*/		
				if(@D_MO_DEDUCCI<>0)
				BEGIN
					SELECT @SALDO=(@SALDO-@D_MO_DEDUCCI)
				END
				ELSE
				BEGIN
					SELECT @SALDO=(@SALDO+@D_MO_PERCEPC)
				END
				
				/* Se incrementa para ordenar los registros */
				SELECT @TE_CODIGO=@TE_CODIGO+1

				/* Se indica valor para alternar los renglones del grid*/
				IF(@ALTERN_ROW=0)
				BEGIN
					SELECT @ALTERN_ROW=1
				END
				ELSE
				BEGIN
					SELECT @ALTERN_ROW=0
				END

				/*Se inserta un registro en la tabla TempStad por cada Movimiento del prestamo en revision*/
				INSERT INTO #TMP_PRESTAMOS
								(TE_CODIGO,		CB_CODIGO,		TE_TIPO,		TE_FECHA,
								TE_DESCRIP, 	TE_CARGO, 		TE_ABONO,		TE_SALDO,	
								US_CODIGO,		TE_USER,		TE_REFEREN,		P_ANIOS_ANT,
								OTROS_ABONOS,	OTROS_CARGOS,	SALDO_ACTUAL,	PR_TOTAL,	
								PR_NUMERO,		PR_STATUS,		PR_STATUS_DES,	TIPO_REGISTRO,
								ALTERN_ROW)			
						VALUES (@TE_CODIGO,		@D_CB_CODIGO,	@D_TE_TIPO,		@D_PE_FEC_INI, 
								@DESCRIPCION, 	@D_MO_PERCEPC, 	@D_MO_DEDUCCI,	@SALDO, 
								@US_CODIGO,		@USUARIO_PORTAL,@D_TE_REFEREN,	0,
								0,				0,				0,				0,		
								0,				0,				'',				'GRID',
								@ALTERN_ROW)
			
	    	FETCH NEXT FROM MOVIMIENTOS_PRESTAMO_CURSOR 
					INTO 	@D_CB_CODIGO, 	@D_TE_TIPO,		@D_TE_REFEREN,	@D_PE_FEC_INI,	
							@D_MO_PERCEPC,	@D_MO_DEDUCCI,	@D_US_CODIGO,	@D_PE_YEAR,
							@D_PE_TIPO,		@D_PE_NUMERO, 	@D_CO_NUMERO,	@D_TIPO
		   
		   END
			
			CLOSE MOVIMIENTOS_PRESTAMO_CURSOR
			DEALLOCATE MOVIMIENTOS_PRESTAMO_CURSOR
   /*-----------------Termina MOVIMIENTOS_PRESTAMO_CURSOR-------------------------------------*/

			SELECT @ALTERN_ROW=0

			INSERT INTO #TMP_PRESTAMOS
							(TE_CODIGO,		CB_CODIGO,		TE_TIPO,		TE_FECHA,
							TE_DESCRIP, 	TE_CARGO, 		TE_ABONO,		TE_SALDO,	
							US_CODIGO,		TE_USER,		TE_REFEREN,		P_ANIOS_ANT,
							OTROS_ABONOS,	OTROS_CARGOS,	SALDO_ACTUAL,	PR_TOTAL,	
							PR_NUMERO,		PR_STATUS,		PR_STATUS_DES,	TIPO_REGISTRO,
							ALTERN_ROW)	
	
					VALUES(@TE_CODIGO, 		@L_CB_CODIGO,	@L_PR_TIPO,		@L_PR_FECHA, 
						'----------------',	@TB_CONCEPT, 	0,				0, 
						@US_CODIGO,			@USUARIO_PORTAL,@L_PR_REFEREN,	0,
						0,					0,				0,				0,	
						0,					0,				'',				'FIN_GRID',
						@ALTERN_ROW)

   /* Get the next Prestamo.*/
	FETCH NEXT FROM LISTA_PRESTAMOS_CURSOR 
	INTO 	@L_CB_CODIGO,	@L_PR_TIPO ,	@L_PR_FECHA,	@TB_CONCEPT ,	@L_PR_SALDO_I ,
			@L_PR_SALDO ,	@L_PR_MONTO,	@L_PR_REFEREN,	@L_US_CODIGO,	@L_PR_ABONOS,
			@L_PR_CARGOS,	@L_PR_TOTAL,	@L_PR_NUMERO,	@L_PR_STATUS,	@L_PR_STATUS_DES 


	END

	/* Close the LISTA_PRESTAMOS_CURSOR */
	CLOSE LISTA_PRESTAMOS_CURSOR
	DEALLOCATE LISTA_PRESTAMOS_CURSOR

	/*Se envia el reporte de los prestamos con su detalle y se elimina la tabla temporal*/
	SELECT * FROM #TMP_PRESTAMOS
	DROP TABLE #TMP_PRESTAMOS
END
GO


/* SP_AVG_ENTREV_VACANT: Consultas selecci�n: Promedio de entrevistas por vacante (PORTAL)*/
/* Patch 410 # Seq: 290 Agregado */
CREATE PROCEDURE SP_AVG_ENTREV_VACANT 
(
	@CB_CLIENTE varchar(30),
	@CB_ESTATUS varchar(30),
	@FECHA_INI varchar(30),
	@FECHA_FIN varchar(30),
	@SERVIDOR VARCHAR(1000)
)
AS
BEGIN

	SET @CB_CLIENTE = CASE @CB_CLIENTE  WHEN '-100' THEN '%' WHEN '-1' THEN '%' WHEN null THEN '%' WHEN '' THEN '%' ELSE @CB_CLIENTE  END
	SET @CB_ESTATUS = CASE @CB_ESTATUS  WHEN '-100' THEN '%' WHEN '-1' THEN '%' WHEN null THEN '%' WHEN '' THEN '%' ELSE @CB_ESTATUS  END
	
	EXEC('
	Declare @Actualizar Table
	(
		PROMEDIO int,
		PROMEDIO2 int
	)
	Declare @Vacantes varchar(30)
	Declare @Entrevista varchar(30)

	declare TmpCosto cursor for
	select R.RQ_VACANTE,COUNT(E.RQ_FOLIO) Entrevista
	from '+@SERVIDOR+'.DBO.ENTREVIS E
	left outer join '+@SERVIDOR+'.DBO.REQUIERE R on (R.RQ_FOLIO=E.RQ_FOLIO)
	left outer join '+@SERVIDOR+'.DBO.PUESTO P on (P.PU_CODIGO=R.RQ_PUESTO)
	where ( RQ_CLIENTE LIKE LTRIM(RTRIM('''+@CB_CLIENTE+''')))
	and( RQ_STATUS LIKE LTRIM(RTRIM('''+@CB_ESTATUS+''')))
	and(RQ_FEC_INI >= '''+@FECHA_INI+''' AND RQ_FEC_FIN <= '''+@FECHA_FIN+''' ) 
	group by E.RQ_FOLIO,P.PU_DESCRIP,R.RQ_FEC_INI,R.RQ_VACANTE

	Open TmpCosto
		Fetch Next From TmpCosto Into 
		@Vacantes,
		@Entrevista

		while @@Fetch_Status = 0
		begin
				insert into @Actualizar (PROMEDIO,PROMEDIO2) values(@Vacantes,@Entrevista) 

			Fetch Next From TmpCosto Into
			@Vacantes,
			@Entrevista
		end
	  close TmpCosto
	  deallocate TmpCosto
		select sum(PROMEDIO2)/cast(sum(PROMEDIO)as decimal (5,2)) as PROMEDIO from @Actualizar
	')

END
GO

/* SP_MOTIVO_VACANTE: Consultas selecci�n: Regresa motivo por el cual se abre la vacante (PORTAL)*/
/* Patch 410 # Seq: 291 Agregado */
CREATE PROCEDURE SP_MOTIVO_VACANTE(
	@CB_CLIENTE varchar(30),
	@CB_PUESTO varchar(30),
	@SERVIDOR varchar(1000)
)
AS
BEGIN
	
	DECLARE @QUERY char(1000);


IF @CB_CLIENTE = '-100' AND @CB_PUESTO = '-100' 
	BEGIN
		SELECT @QUERY = '';
	END

ELSE IF @CB_CLIENTE <>'-100'
	BEGIN
		SET @QUERY = 'WHERE RQ_CLIENTE='''+@CB_CLIENTE+''' '
	END

ELSE IF @CB_PUESTO <> '-100' 
	BEGIN
		SET @QUERY = 'WHERE RQ_PUESTO='''+@CB_PUESTO+''' '
	END

ELSE
	BEGIN
		SET @QUERY = 'WHERE RQ_CLIENTE='''+@CB_CLIENTE+''' AND RQ_PUESTO='''+@CB_PUESTO+''''
	END

	exec('
	Declare @MotivoVacante Table
	(	
	MOTIVO integer,
	CANTIDAD Integer
	)

	DECLARE @Valor Integer;	
	DECLARE @Contador Integer;
	SET @Contador = 0

	WHILE @Contador <=1
	BEGIN
		SELECT @Valor = ( SUM( CASE WHEN RQ_MOTIVO = @Contador THEN 1 ELSE 0 END ) ) 
		FROM '+@SERVIDOR+'.DBO.REQUIERE
		'+@QUERY+' 

		INSERT INTO @MotivoVacante values ( @Contador,@Valor);
		SET @Contador = @Contador+1 
	END
		SELECT MV.*, DBO.TF( 94, MV.MOTIVO) DESCRIPCION from @MotivoVacante MV
')

END
GO


/* SP_VACANTES_AC: Consultas selecci�n: Cantidad de vacantes por su descripci�n (PORTAL)*/
/* Patch 410 # Seq: 292 Agregado */
CREATE  PROCEDURE SP_VACANTES_AC (
	@SERVIDOR varchar(1000)
)
AS
BEGIN


	exec('
	Declare @MotivoVacante Table
	(	
	MOTIVO integer,
	CANTIDAD Integer
	)

	DECLARE @Valor Integer;	
	DECLARE @Contador Integer;
	SET @Contador = 0

	WHILE @Contador <=2
	BEGIN
		SELECT @Valor = ( SUM( CASE WHEN RQ_STATUS = @Contador THEN 1 ELSE 0 END ) ) 
		FROM '+@SERVIDOR+'.DBO.REQUIERE

		INSERT INTO @MotivoVacante values ( @Contador,@Valor);
		SET @Contador = @Contador+1 
	END
		SELECT MV.*, DBO.TF( 93, MV.MOTIVO) DESCRIPCION from @MotivoVacante MV
')

END
GO


/* SP_EST_MOTIVOS_AUTORIZA_EXT: Asistencia:  Regresa estad�stica de motivos de autorizaci�n de extras (PORTAL)*/
/* Patch 410 # Seq: 293 Agregado */
CREATE PROCEDURE SP_EST_MOTIVOS_AUTORIZA_EXT (
@CLASIFI VARCHAR(30),
@NIVEL VARCHAR(30),
@LISTA VARCHAR(30),
@YEAR INT
)
AS  
BEGIN

	DECLARE @TMPCLAS char(30);
	DECLARE @QUERY char(1000);
	
	SELECT @TMPCLAS = MIN(TB_CODIGO) FROM CLASIFI;
	
	SET @CLASIFI = CASE @CLASIFI  WHEN '-100' THEN '%' WHEN '-1' THEN '%' WHEN null THEN '%' WHEN '' THEN '%' ELSE @CLASIFI END
	SET @NIVEL = CASE @NIVEL  WHEN '1' THEN 'CB_NIVEL1' WHEN '2' THEN 'CB_NIVEL2' WHEN '3' THEN 'CB_NIVEL3' WHEN '4' THEN 'CB_NIVEL4' WHEN '5' THEN 'CB_NIVEL5' WHEN '6' THEN 'CB_NIVEL6'
	ELSE @NIVEL END

IF @NIVEL <> '-100' AND @LISTA <> '-100' 
	BEGIN
		SELECT @QUERY = ' and AUSENCIA.'+@NIVEL+' = '''+@LISTA+''''
	END

ELSE
	BEGIN
		 SELECT @QUERY = '';
	END

	exec('
		select case when A.CH_RELOJ = '''' then ''Sin Motivo'' else M.TB_ELEMENT end Motivo,
		(
		select SUM(B.CH_HOR_EXT)
		from CHECADAS B
		left outer join AUSENCIA on (AUSENCIA.CB_CODIGO=B.CB_CODIGO and AUSENCIA.AU_FECHA=B.AU_FECHA)
		where B.CH_TIPO>=7
		and (datepart( yyyy, B.AU_FECHA ) = '''+@YEAR+''')
		and (datepart( mm, B.AU_FECHA ) = 1)
		and A.CH_RELOJ=B.CH_RELOJ
		and ( AUSENCIA.CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+''')))
		'+@QUERY+'
		) Enero,
		(
		select SUM(B.CH_HOR_EXT)
		from CHECADAS B
		left outer join AUSENCIA on (AUSENCIA.CB_CODIGO=B.CB_CODIGO and AUSENCIA.AU_FECHA=B.AU_FECHA)
		where B.CH_TIPO>=7
		and (datepart( yyyy, B.AU_FECHA ) = '''+@YEAR+''')
		and (datepart( mm, B.AU_FECHA ) = 2)
		and A.CH_RELOJ=B.CH_RELOJ
		and ( AUSENCIA.CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+''')))
		'+@QUERY+'
		) Febrero,
		(
		select SUM(B.CH_HOR_EXT)
		from CHECADAS B
		left outer join AUSENCIA on (AUSENCIA.CB_CODIGO=B.CB_CODIGO and AUSENCIA.AU_FECHA=B.AU_FECHA)
		where B.CH_TIPO>=7
		and (datepart( yyyy, B.AU_FECHA ) = '''+@YEAR+''')
		and (datepart( mm, B.AU_FECHA ) = 3)
		and A.CH_RELOJ=B.CH_RELOJ
		and ( AUSENCIA.CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+''')))
		'+@QUERY+'
		) Marzo,
		(
		select SUM(B.CH_HOR_EXT)
		from CHECADAS B
		left outer join AUSENCIA on (AUSENCIA.CB_CODIGO=B.CB_CODIGO and AUSENCIA.AU_FECHA=B.AU_FECHA)
		where B.CH_TIPO>=7
		and (datepart( yyyy, B.AU_FECHA ) = '''+@YEAR+''')
		and (datepart( mm, B.AU_FECHA ) = 4)
		and A.CH_RELOJ=B.CH_RELOJ
		and ( AUSENCIA.CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+''')))
		'+@QUERY+'
		) Abril,
		(
		select SUM(B.CH_HOR_EXT)
		from CHECADAS B
		left outer join AUSENCIA on (AUSENCIA.CB_CODIGO=B.CB_CODIGO and AUSENCIA.AU_FECHA=B.AU_FECHA)
		where B.CH_TIPO>=7
		and (datepart( yyyy, B.AU_FECHA ) = '''+@YEAR+''')
		and (datepart( mm, B.AU_FECHA ) = 5)
		and A.CH_RELOJ=B.CH_RELOJ
		and ( AUSENCIA.CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+''')))
		'+@QUERY+'
		) Mayo,
		(
		select SUM(B.CH_HOR_EXT)
		from CHECADAS B
		left outer join AUSENCIA on (AUSENCIA.CB_CODIGO=B.CB_CODIGO and AUSENCIA.AU_FECHA=B.AU_FECHA)
		where B.CH_TIPO>=7
		and (datepart( yyyy, B.AU_FECHA ) = '''+@YEAR+''' )
		and (datepart( mm, B.AU_FECHA ) = 6)
		and A.CH_RELOJ=B.CH_RELOJ
		and ( AUSENCIA.CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+''')))
		'+@QUERY+'
		) Junio,
		(
		select SUM(B.CH_HOR_EXT)
		from CHECADAS B
		left outer join AUSENCIA on (AUSENCIA.CB_CODIGO=B.CB_CODIGO and AUSENCIA.AU_FECHA=B.AU_FECHA)
		where B.CH_TIPO>=7
		and (datepart( yyyy, B.AU_FECHA ) = '''+@YEAR+''')
		and (datepart( mm, B.AU_FECHA ) = 7)
		and A.CH_RELOJ=B.CH_RELOJ
		and ( AUSENCIA.CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+''')))
		'+@QUERY+'
		) Julio,
		(
		select SUM(B.CH_HOR_EXT)
		from CHECADAS B
		left outer join AUSENCIA on (AUSENCIA.CB_CODIGO=B.CB_CODIGO and AUSENCIA.AU_FECHA=B.AU_FECHA)
		where B.CH_TIPO>=7
		and (datepart( yyyy, B.AU_FECHA ) = '''+@YEAR+''' )
		and (datepart( mm, B.AU_FECHA ) = 8)
		and A.CH_RELOJ=B.CH_RELOJ
		and ( AUSENCIA.CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+''')))
		'+@QUERY+'
		) Agosto,
		(
		select SUM(B.CH_HOR_EXT)
		from CHECADAS B
		left outer join AUSENCIA on (AUSENCIA.CB_CODIGO=B.CB_CODIGO and AUSENCIA.AU_FECHA=B.AU_FECHA)
		where B.CH_TIPO>=7
		and (datepart( yyyy, B.AU_FECHA ) = '''+@YEAR+''')
		and (datepart( mm, B.AU_FECHA ) = 9)
		and A.CH_RELOJ=B.CH_RELOJ
		and ( AUSENCIA.CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+''')))
		'+@QUERY+'
		) Septiembre,
		(
		select SUM(B.CH_HOR_EXT)
		from CHECADAS B
		left outer join AUSENCIA on (AUSENCIA.CB_CODIGO=B.CB_CODIGO and AUSENCIA.AU_FECHA=B.AU_FECHA)
		where B.CH_TIPO>=7
		and (datepart( yyyy, B.AU_FECHA ) = '''+@YEAR+''' )
		and (datepart( mm, B.AU_FECHA ) = 10)
		and A.CH_RELOJ=B.CH_RELOJ
		and ( AUSENCIA.CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+''')))
		'+@QUERY+'
		) Octubre,
		(
		select SUM(B.CH_HOR_EXT)
		from CHECADAS B
		left outer join AUSENCIA on (AUSENCIA.CB_CODIGO=B.CB_CODIGO and AUSENCIA.AU_FECHA=B.AU_FECHA)
		where B.CH_TIPO>=7
		and (datepart( yyyy, B.AU_FECHA ) = '''+@YEAR+''' )
		and (datepart( mm, B.AU_FECHA ) = 11)
		and A.CH_RELOJ=B.CH_RELOJ
		and ( AUSENCIA.CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+''')))
		'+@QUERY+'
		) Noviembre,
		(
		select SUM(B.CH_HOR_EXT)
		from CHECADAS B
		left outer join AUSENCIA on (AUSENCIA.CB_CODIGO=B.CB_CODIGO and AUSENCIA.AU_FECHA=B.AU_FECHA)
		where B.CH_TIPO>=7
		and (datepart( yyyy, B.AU_FECHA ) = '''+@YEAR+''' )
		and (datepart( mm, B.AU_FECHA ) = 12)
		and A.CH_RELOJ=B.CH_RELOJ
		and ( AUSENCIA.CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+''')))
		'+@QUERY+'
		) Diciembre

		from CHECADAS A
		left outer join MOT_AUTO M on (M.TB_CODIGO=A.CH_RELOJ)
		where A.CH_TIPO>=7
		and (datepart( yyyy, A.AU_FECHA ) = '''+@YEAR+''' )
		group by A.CH_RELOJ,M.TB_ELEMENT
		order by A.CH_RELOJ,M.TB_ELEMENT
	')
END
GO

/* SP_EST_TIEMPO_EXTRA: Asistencia:  Regresa estad�stica de tiempo extra (PORTAL)*/
/* Patch 410 # Seq: 294 Agregado */
CREATE PROCEDURE SP_EST_TIEMPO_EXTRA (
@CLASIFI VARCHAR(30),
@NIVEL VARCHAR(30),
@LISTA VARCHAR(30),
@YEAR INT
)
AS  
BEGIN

	DECLARE @TMPCLAS char(30);
	DECLARE @QUERY char(1000);
	
	SELECT @TMPCLAS = MIN(TB_CODIGO) FROM CLASIFI;
	
	SET @CLASIFI = CASE @CLASIFI  WHEN '-100' THEN '%' WHEN '-1' THEN '%' WHEN null THEN '%' WHEN '' THEN '%' ELSE @CLASIFI END
	SET @NIVEL = CASE @NIVEL  WHEN '1' THEN 'CB_NIVEL1' WHEN '2' THEN 'CB_NIVEL2' WHEN '3' THEN 'CB_NIVEL3' WHEN '4' THEN 'CB_NIVEL4' WHEN '5' THEN 'CB_NIVEL5' WHEN '6' THEN 'CB_NIVEL6'
	ELSE @NIVEL END

IF @NIVEL <> '-100' AND @LISTA <> '-100' 
	BEGIN
		SELECT @QUERY = ' and AUSENCIA.'+@NIVEL+' = '''+@LISTA+''''
	END

ELSE
	BEGIN
		 SELECT @QUERY = '';
	END

	exec('
		select 
		datepart( mm, au_fecha ) N�mero, 
		DBO.TF( 61, datepart( mm, au_fecha )-1 ) Mes,
		SUM(AU_NUM_EXT) Trabajadas, 
		SUM(AU_AUT_EXT) Autorizadas,
		SUM(AU_DOBLES) Dobles,
		SUM(AU_TRIPLES) Triples
		from AUSENCIA
		where (datepart( yyyy, AU_FECHA ) = '''+@YEAR+''')
		and ( CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+''')))
		'+@QUERY+'
		group by datepart( mm, AU_FECHA )
		order by datepart( mm, AU_FECHA )
	')
END
GO

/* SP_GRAF_TOTALES_HORAS_DIAS: Asistencia:  Costo total de tiempo por horas o por dias (PORTAL)*/
/* Patch 410 # Seq: 295 Agregado */
CREATE PROCEDURE SP_GRAF_TOTALES_HORAS_DIAS (
@CLASIFI VARCHAR(30),
@NIVEL VARCHAR(30),
@LISTA VARCHAR(30),
@YEAR INT,
@HORAS VARCHAR(30)
)
AS
BEGIN

	DECLARE @TMPCLAS char(30);
	DECLARE @QUERY char(1000);
	
	SELECT @TMPCLAS = MIN(TB_CODIGO) FROM CLASIFI;
	
	SET @CLASIFI = CASE @CLASIFI  WHEN '-100' THEN '%' WHEN '-1' THEN '%' WHEN null THEN '%' WHEN '' THEN '%' ELSE @CLASIFI END
	SET @NIVEL = CASE @NIVEL  WHEN '1' THEN 'CB_NIVEL1' WHEN '2' THEN 'CB_NIVEL2' WHEN '3' THEN 'CB_NIVEL3' WHEN '4' THEN 'CB_NIVEL4' WHEN '5' THEN 'CB_NIVEL5' WHEN '6' THEN 'CB_NIVEL6'
	ELSE @NIVEL END

IF @NIVEL <> '-100' AND @LISTA <> '-100' 
	BEGIN
		SELECT @QUERY = ' and COLABORA.'+@NIVEL+' = '''+@LISTA+''''
	END

ELSE
	BEGIN
		 SELECT @QUERY = '';
	END

	exec('
		select VA.AC_MES, DBO.TF( 61, VA.AC_MES-1 ) Mes,
			( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND VP.AC_MES = VA.AC_MES and (VP.CO_NUMERO LIKE LTRIM(RTRIM('''+@HORAS+'''))) and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) '+@QUERY+'  ) MONTO
		from V_ACUMULA VA
		where (VA.AC_YEAR = '''+@YEAR+''')
		and VA.AC_MES<>13
		group by VA.AC_YEAR, VA.AC_MES
	')
END
GO


/* SP_TOTALES_DIAS_AUSENTISMO: Asistencia:  Regresa dias trabajados y ausentismo (PORTAL)*/
/* Patch 410 # Seq: 296 Agregado */
CREATE PROCEDURE SP_TOTALES_DIAS_AUSENTISMO (
@CLASIFI VARCHAR(30),
@NIVEL VARCHAR(30),
@LISTA VARCHAR(30),
@YEAR INT
)
AS
BEGIN

	DECLARE @TMPCLAS char(30);
	DECLARE @QUERY char(1000);
	
	SELECT @TMPCLAS = MIN(TB_CODIGO) FROM CLASIFI;
	
	SET @CLASIFI = CASE @CLASIFI  WHEN '-100' THEN '%' WHEN '-1' THEN '%' WHEN null THEN '%' WHEN '' THEN '%' ELSE @CLASIFI END
	SET @NIVEL = CASE @NIVEL  WHEN '1' THEN 'CB_NIVEL1' WHEN '2' THEN 'CB_NIVEL2' WHEN '3' THEN 'CB_NIVEL3' WHEN '4' THEN 'CB_NIVEL4' WHEN '5' THEN 'CB_NIVEL5' WHEN '6' THEN 'CB_NIVEL6'
	ELSE @NIVEL END

IF @NIVEL <> '-100' AND @LISTA <> '-100' 
	BEGIN
		SELECT @QUERY = ' and COLABORA.'+@NIVEL+' = '''+@LISTA+''''
	END

ELSE
	BEGIN
		 SELECT @QUERY = '';
	END

	exec('
	select VA.AC_MES, DBO.TF( 61, VA.AC_MES-1 ) Mes,
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND VP.AC_MES = VA.AC_MES AND VP.CO_NUMERO = 1107 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) Trabajadas,
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND VP.AC_MES = VA.AC_MES AND VP.CO_NUMERO = 1100 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) FI,
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND VP.AC_MES = VA.AC_MES AND VP.CO_NUMERO = 1103 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) PermisoSG,
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND VP.AC_MES = VA.AC_MES AND VP.CO_NUMERO = 1102 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) PermisoCG,
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND VP.AC_MES = VA.AC_MES AND VP.CO_NUMERO = 1113 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) VAC,
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND VP.AC_MES = VA.AC_MES AND VP.CO_NUMERO = 1106 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) INC
	from V_ACUMULA VA
	where (VA.AC_YEAR  = '''+@YEAR+''' )
	and VA.AC_MES<>13
	group by VA.AC_YEAR, VA.AC_MES
	union
	select 14, ''Total'',
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND  VP.CO_NUMERO = 1107 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) Trabajadas,
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND  VP.CO_NUMERO = 1100 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) FI,
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND  VP.CO_NUMERO = 1103 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) PermisoSG,
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND  VP.CO_NUMERO = 1102 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) PermisoCG,
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND  VP.CO_NUMERO = 1113 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) VAC,
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND  VP.CO_NUMERO = 1106 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) INC
	from V_ACUMULA VA 
	left outer join COLABORA ON (COLABORA.CB_CODIGO=VA.CB_CODIGO)
	where (VA.AC_YEAR = '''+@YEAR+''')
	and VA.AC_MES<>13
	group by VA.AC_YEAR
	')
END
GO

/* SP_TOTALES_HORAS: Asistencia:  Regresa total de horas trabajadas (PORTAL)*/
/* Patch 410 # Seq: 297 Agregado */
CREATE PROCEDURE SP_TOTALES_HORAS (
@CLASIFI VARCHAR(30),
@NIVEL VARCHAR(30),
@LISTA VARCHAR(30),
@YEAR INT
)
AS  
BEGIN

	DECLARE @TMPCLAS char(30);
	DECLARE @QUERY char(1000);
	
	SELECT @TMPCLAS = MIN(TB_CODIGO) FROM CLASIFI;
	
	SET @CLASIFI = CASE @CLASIFI  WHEN '-100' THEN '%' WHEN '-1' THEN '%' WHEN null THEN '%' WHEN '' THEN '%' ELSE @CLASIFI END
	SET @NIVEL = CASE @NIVEL  WHEN '1' THEN 'CB_NIVEL1' WHEN '2' THEN 'CB_NIVEL2' WHEN '3' THEN 'CB_NIVEL3' WHEN '4' THEN 'CB_NIVEL4' WHEN '5' THEN 'CB_NIVEL5' WHEN '6' THEN 'CB_NIVEL6'
	ELSE @NIVEL END

IF @NIVEL <> '-100' AND @LISTA <> '-100' 
	BEGIN
		SELECT @QUERY = ' and COLABORA.'+@NIVEL+' = '''+@LISTA+''''
	END

ELSE
	BEGIN
		 SELECT @QUERY = '';
	END

	exec('
	select VA.AC_MES, DBO.TF( 61, VA.AC_MES-1 ) Mes,
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND VP.AC_MES = VA.AC_MES AND VP.CO_NUMERO = 1201 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) Trabajadas,
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND VP.AC_MES = VA.AC_MES AND VP.CO_NUMERO = 1206 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) Retardo,
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND VP.AC_MES = VA.AC_MES AND VP.CO_NUMERO = 1203 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) ExtDobles,
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND VP.AC_MES = VA.AC_MES AND VP.CO_NUMERO = 1204 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) ExtTriples,
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND VP.AC_MES = VA.AC_MES AND VP.CO_NUMERO = 1207 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) Descanso
	from V_ACUMULA VA
	where (VA.AC_YEAR = '''+@YEAR+''' )
	and VA.AC_MES<>13
	group by VA.AC_YEAR, VA.AC_MES
	union
	select 14, ''Total'',
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND  VP.CO_NUMERO = 1201 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) Trabajadas,
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND  VP.CO_NUMERO = 1206 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) Retardo,
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND  VP.CO_NUMERO = 1203 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) ExtDobles,
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND  VP.CO_NUMERO = 1204 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) ExtTriples,
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND  VP.CO_NUMERO = 1207 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) Descanso
	from V_ACUMULA VA 
	left outer join COLABORA ON (COLABORA.CB_CODIGO=VA.CB_CODIGO)
	where (VA.AC_YEAR = '''+@YEAR+''' )
	and VA.AC_MES<>13
	group by VA.AC_YEAR
	')
END
GO


/* SP_COMBO_LISTADINAMICA: N�mina:  Despliega combo din�mico de nivel posicionado (PORTAL)*/
/* Patch 410 # Seq: 298 Agregado */
CREATE PROCEDURE SP_COMBO_LISTADINAMICA
(
	@NIVEL VARCHAR(30)
)
AS
BEGIN

	IF @NIVEL<>'-100'
	BEGIN
		SET @NIVEL = CASE @NIVEL WHEN '1' THEN 'NIVEL1' WHEN '2' THEN 'NIVEL2' WHEN '3' THEN 'NIVEL3' WHEN '4' THEN 'NIVEL4' 
		WHEN '5' THEN 'NIVEL5' WHEN '6' THEN 'NIVEL6' ELSE @NIVEL END

		EXEC('SELECT TB_CODIGO, TB_ELEMENT FROM '+@NIVEL+' where TB_ACTIVO = ''S'' ')
	END
	ELSE
	BEGIN
		Select TB_CODIGO = -1 , TB_ELEMENT=''
	END

END
GO

/* SP_NIVELES_DINAMICOS: N�mina: Regresa total de Percepci�n y deducci�n (PORTAL)*/
/* Patch 410 # Seq: 299 Agregado */
CREATE PROCEDURE SP_NIVELES_DINAMICOS (
@CLASIFI VARCHAR(30),
@NIVEL VARCHAR(30),
@LISTA VARCHAR(30),
@NOMINA VARCHAR(30)
)
AS  
BEGIN

	DECLARE @TMPCLAS char(30);
	DECLARE @QUERY char(1000);
	
	SELECT @TMPCLAS = MIN(TB_CODIGO) FROM CLASIFI;
	
	SET @CLASIFI = CASE @CLASIFI  WHEN '-100' THEN @TMPCLAS WHEN '-1' THEN @TMPCLAS WHEN null THEN @TMPCLAS WHEN '' THEN @TMPCLAS ELSE @CLASIFI END
	SET @NIVEL = CASE @NIVEL  WHEN '1' THEN 'CB_NIVEL1' WHEN '2' THEN 'CB_NIVEL2' WHEN '3' THEN 'CB_NIVEL3' WHEN '4' THEN 'CB_NIVEL4' WHEN '5' THEN 'CB_NIVEL5' WHEN '6' THEN 'CB_NIVEL6'
	ELSE @NIVEL END


IF @NIVEL <> '-100' AND @LISTA <> '-100' 
	BEGIN
		SELECT @QUERY = ' and COLABORA.'+@NIVEL+' = '''+@LISTA+''''
	END

ELSE
	BEGIN
		 SELECT @QUERY = '';
	END

	exec('
	select ACUMULA.CO_NUMERO,CO_DESCRIP, sum(AC_ANUAL) as Percepciones, CAST(0 AS SMALLINT) as deducciones from ACUMULA
	left outer join CONCEPTO on (ACUMULA.CO_NUMERO= CONCEPTO.CO_NUMERO)
	left outer join COLABORA on (ACUMULA.CB_CODIGO=COLABORA.CB_CODIGO)
	where (AC_YEAR LIKE LTRIM(RTRIM('''+@NOMINA+'''))) and CONCEPTO.CO_TIPO=1
	and CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))
	'+@QUERY+' 
	group by ACUMULA.CO_NUMERO,CO_DESCRIP

	UNION ALL

	select ACUMULA.CO_NUMERO, CO_DESCRIP,CAST(0 AS SMALLINT),sum(AC_ANUAL) from ACUMULA
	left outer join CONCEPTO on (ACUMULA.CO_NUMERO= CONCEPTO.CO_NUMERO)
	left outer join COLABORA on (ACUMULA.CB_CODIGO=COLABORA.CB_CODIGO)
	where (AC_YEAR LIKE LTRIM(RTRIM('''+@NOMINA+'''))) and CONCEPTO.CO_TIPO=2
	and CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+''')) 
	'+@QUERY+'
	group by ACUMULA.CO_NUMERO,CO_DESCRIP

	order by 1
	')
END
GO


/* SP_TOTALES_NOMINA: N�mina: Totales de una n�mina (PORTAL)*/
/* Patch 410 # Seq: 300 Agregado */
CREATE PROCEDURE SP_TOTALES_NOMINA(
@CB_LIQUIDA VARCHAR(30),
@CB_TURNO VARCHAR(30),
@CB_CLASIFI VARCHAR(30),
@CB_NIVEL VARCHAR(30),
@CB_LISTA VARCHAR(30),
@YEAR VARCHAR(30),
@PERIODO VARCHAR(30),
@TIPO VARCHAR(30)
)
AS  
BEGIN

	DECLARE @TMPCLAS char(30);
	DECLARE @QUERY char(1000);
	
	SELECT @TMPCLAS = MIN(TB_CODIGO) FROM CLASIFI;
	
	SET @CB_LIQUIDA = CASE @CB_LIQUIDA  WHEN '-100' THEN 0 WHEN '-1' THEN 0 WHEN null THEN 0 WHEN '' THEN 0 ELSE @CB_LIQUIDA END
	SET @CB_TURNO = CASE @CB_TURNO  WHEN '-100' THEN '%' WHEN '-1' THEN '%' WHEN null THEN '%' WHEN '' THEN '%' ELSE @CB_TURNO END
	SET @CB_CLASIFI = CASE @CB_CLASIFI  WHEN '-100' THEN @TMPCLAS WHEN '-1' THEN @TMPCLAS WHEN null THEN @TMPCLAS WHEN '' THEN @TMPCLAS ELSE @CB_CLASIFI END
	SET @CB_NIVEL = CASE @CB_NIVEL  WHEN '1' THEN 'CB_NIVEL1' WHEN '2' THEN 'CB_NIVEL2' WHEN '3' THEN 'CB_NIVEL3' WHEN '4' THEN 'CB_NIVEL4' WHEN '5' THEN 'CB_NIVEL5' WHEN '6' THEN 'CB_NIVEL6'
	ELSE @CB_NIVEL END

IF @CB_NIVEL <> '-100' AND @CB_LISTA <> '-100' 
	BEGIN
		SELECT @QUERY = ' and COLABORA.'+@CB_NIVEL+' = '''+@CB_LISTA+''''
	END

ELSE
	BEGIN
		 SELECT @QUERY = '';
	END

	exec('
	select NOMINA.CB_CODIGO,PRETTYNAME,NOMINA.CB_SALARIO, NO_PERCEPC, NO_DEDUCCI, NO_NETO,NO_HORAS,
	NO_DOBLES, NO_TRIPLES, NO_DES_TRA, NO_DIAS_VA,NO_DIAS_IN, NO_DIAS_AG
	from NOMINA
	left outer join COLABORA on (NOMINA.CB_CODIGO=COLABORA.CB_CODIGO)
	where PE_YEAR='''+@YEAR+''' and PE_NUMERO='''+@PERIODO+''' and PE_TIPO='''+@TIPO+'''
	and ( NO_LIQUIDA LIKE LTRIM(RTRIM('''+@CB_LIQUIDA+''')))
	and  ( NOMINA.CB_TURNO LIKE LTRIM(RTRIM('''+@CB_TURNO+''' )))
	and  ( NOMINA.CB_CLASIFI LIKE LTRIM(RTRIM('''+@CB_CLASIFI+''' )))
	'+@QUERY+'
	')
END
GO


/* SP_EST_VISITAS_MENS: Visitantes: Estad�sticas de visitas mensuales (PORTAL)*/
/* Patch 410 # Seq: 301 Agregado */
CREATE PROCEDURE SP_EST_VISITAS_MENS (
@YEAR INT,
@SERVIDOR VARCHAR(1000)
)
AS
BEGIN


	exec('
		select 1 as NUMERO,''Enero'' as MES,count(*) as TOTAL from  '+@SERVIDOR+'.LIBRO B
		where (datepart( yyyy, B.LI_ENT_FEC ) >='''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC ) <= '''+@YEAR+''')
		and (datepart( mm, B.LI_ENT_FEC ) = 1)
		union all
		select 2,''Febrero'',count(*) from '+@SERVIDOR+'.LIBRO B
		where (datepart( yyyy, B.LI_ENT_FEC ) >='''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC ) <= '''+@YEAR+''')
		and (datepart( mm, B.LI_ENT_FEC ) = 2)
		union all
		select 3,''Marzo'',count(*) from '+@SERVIDOR+'.LIBRO B
		where (datepart( yyyy, B.LI_ENT_FEC ) >='''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC ) <= '''+@YEAR+''')
		and (datepart( mm, B.LI_ENT_FEC ) = 3)
		union all
		select 4,''Abril'',count(*) from '+@SERVIDOR+'.LIBRO B
		where (datepart( yyyy, B.LI_ENT_FEC ) >='''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC ) <= '''+@YEAR+''')
		and (datepart( mm, B.LI_ENT_FEC ) = 4)
		union all
		select 5,''Mayo'',count(*) from '+@SERVIDOR+'.LIBRO B
		where (datepart( yyyy, B.LI_ENT_FEC ) >='''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC ) <= '''+@YEAR+''')
		and (datepart( mm, B.LI_ENT_FEC ) = 5)
		union all
		select 6,''Junio'',count(*) from '+@SERVIDOR+'.LIBRO B
		where (datepart( yyyy, B.LI_ENT_FEC ) >='''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC ) <= '''+@YEAR+''')
		and (datepart( mm, B.LI_ENT_FEC ) = 6)
		union all
		select 7,''Julio'',count(*) from '+@SERVIDOR+'.LIBRO B
		where (datepart( yyyy, B.LI_ENT_FEC ) >='''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC ) <= '''+@YEAR+''')
		and (datepart( mm, B.LI_ENT_FEC ) = 7)
		union all
		select 8,''Agosto'',count(*) from '+@SERVIDOR+'.LIBRO B
		where (datepart( yyyy, B.LI_ENT_FEC ) >='''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC ) <= '''+@YEAR+''')
		and (datepart( mm, B.LI_ENT_FEC ) = 8)
		union all
		select 9,''Septiembre'',count(*) from '+@SERVIDOR+'.LIBRO B
		where (datepart( yyyy, B.LI_ENT_FEC ) >='''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC ) <= '''+@YEAR+''')
		and (datepart( mm, B.LI_ENT_FEC ) = 9)
		union all
		select 10,''Octubre'',count(*) from '+@SERVIDOR+'.LIBRO B
		where (datepart( yyyy, B.LI_ENT_FEC ) >='''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC ) <= '''+@YEAR+''')
		and (datepart( mm, B.LI_ENT_FEC ) = 10)
		union all
		select 11,''Noviembre'',count(*) from '+@SERVIDOR+'.LIBRO B
		where (datepart( yyyy, B.LI_ENT_FEC ) >='''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC ) <= '''+@YEAR+''')
		and (datepart( mm, B.LI_ENT_FEC ) = 11)
		union all
		select 12,''Diciembre'',count(*) from '+@SERVIDOR+'.LIBRO B
		where (datepart( yyyy, B.LI_ENT_FEC ) >='''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC ) <= '''+@YEAR+''')
		and (datepart( mm, B.LI_ENT_FEC ) = 12)
		')
END
GO


/* SP_EST_VISITAS_ASUNTO: Visitantes: Estad�sticas de visitas por asunto(PORTAL)*/
/* Patch 410 # Seq: 302 Agregado */
CREATE PROCEDURE SP_EST_VISITAS_ASUNTO (
@YEAR INT,
@SERVIDOR VARCHAR(1000)
)
AS
BEGIN

	exec('
		select A.LI_ASUNTO as Codigo,T.TB_ELEMENT as Asunto,
		(
			select COUNT(*)
			from '+@SERVIDOR+'.LIBRO B
 			where(datepart( yyyy, B.LI_ENT_FEC ) >= '''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC )  <= '''+@YEAR+''' )
			and (datepart( mm, B.LI_ENT_FEC ) = 1)
			and A.LI_ASUNTO=B.LI_ASUNTO
		) Enero,
		(
			select COUNT(*)
			from '+@SERVIDOR+'.LIBRO B
 			where(datepart( yyyy, B.LI_ENT_FEC ) >= '''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC )  <= '''+@YEAR+''' )
			and (datepart( mm, B.LI_ENT_FEC ) = 2)
			and A.LI_ASUNTO=B.LI_ASUNTO
		) Febrero,
		(
			select COUNT(*)
			from '+@SERVIDOR+'.LIBRO B
 			where(datepart( yyyy, B.LI_ENT_FEC ) >= '''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC )  <= '''+@YEAR+''' )
			and (datepart( mm, B.LI_ENT_FEC ) = 3)
			and A.LI_ASUNTO=B.LI_ASUNTO
		) Marzo,
		(
			select COUNT(*)
			from '+@SERVIDOR+'.LIBRO B
 			where(datepart( yyyy, B.LI_ENT_FEC ) >= '''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC )  <= '''+@YEAR+''' )
			and (datepart( mm, B.LI_ENT_FEC ) = 4)
			and A.LI_ASUNTO=B.LI_ASUNTO
		) Abril,
		(
			select COUNT(*)
			from '+@SERVIDOR+'.LIBRO B
 			where(datepart( yyyy, B.LI_ENT_FEC ) >= '''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC )  <= '''+@YEAR+''' )
			and (datepart( mm, B.LI_ENT_FEC ) = 5)
			and A.LI_ASUNTO=B.LI_ASUNTO
		) Mayo,
		(
			select COUNT(*)
			from '+@SERVIDOR+'.LIBRO B
 			where(datepart( yyyy, B.LI_ENT_FEC ) >= '''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC )  <= '''+@YEAR+''' )
			and (datepart( mm, B.LI_ENT_FEC ) = 6)
			and A.LI_ASUNTO=B.LI_ASUNTO
		) Junio,
		(
			select COUNT(*)
			from '+@SERVIDOR+'.LIBRO B
 			where(datepart( yyyy, B.LI_ENT_FEC ) >= '''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC )  <= '''+@YEAR+''' )
			and (datepart( mm, B.LI_ENT_FEC ) = 7)
			and A.LI_ASUNTO=B.LI_ASUNTO
		) Julio,
		(
			select COUNT(*)
			from '+@SERVIDOR+'.LIBRO B
 			where(datepart( yyyy, B.LI_ENT_FEC ) >= '''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC )  <= '''+@YEAR+''' )
			and (datepart( mm, B.LI_ENT_FEC ) = 8)
			and A.LI_ASUNTO=B.LI_ASUNTO
		) Agosto,
		(
			select COUNT(*)
			from '+@SERVIDOR+'.LIBRO B
 			where(datepart( yyyy, B.LI_ENT_FEC ) >= '''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC )  <= '''+@YEAR+''' )
			and (datepart( mm, B.LI_ENT_FEC ) = 9)
			and A.LI_ASUNTO=B.LI_ASUNTO
		) Septiembre,
		(
			select COUNT(*)
			from '+@SERVIDOR+'.LIBRO B
 			where(datepart( yyyy, B.LI_ENT_FEC ) >= '''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC )  <= '''+@YEAR+''' )
			and (datepart( mm, B.LI_ENT_FEC ) = 10)
			and A.LI_ASUNTO=B.LI_ASUNTO
		) Octubre,
		(
			select COUNT(*)
			from '+@SERVIDOR+'.LIBRO B
 			where(datepart( yyyy, B.LI_ENT_FEC ) >= '''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC )  <= '''+@YEAR+''' )
			and (datepart( mm, B.LI_ENT_FEC ) = 11)
			and A.LI_ASUNTO=B.LI_ASUNTO
		) Noviembre,
		(
			select COUNT(*)
			from '+@SERVIDOR+'.LIBRO B
 			where(datepart( yyyy, B.LI_ENT_FEC ) >= '''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC )  <= '''+@YEAR+''' )
			and (datepart( mm, B.LI_ENT_FEC ) = 12)
			and A.LI_ASUNTO=B.LI_ASUNTO
		) Diciembre
		from '+@SERVIDOR+'.LIBRO A
		left outer join '+@SERVIDOR+'.TASUNTO T on (T.TB_CODIGO=A.LI_ASUNTO)
		where (datepart( yyyy, A.LI_ENT_FEC ) >= '''+@YEAR+''' and datepart( yyyy, A.LI_ENT_FEC )  <= '''+@YEAR+''' )
		group by T.TB_ELEMENT,A.LI_ASUNTO
		')
END
GO


/* RECALCULA_KARDEX: Agregar CB_FEC_COV */
/* Patch 410 # Seq: 165 Modificado ( Sug. 753 )*/
ALTER PROCEDURE RECALCULA_KARDEX(@Empleado INTEGER) AS
BEGIN
   SET NOCOUNT ON
   declare @kAUTOSAL  CHAR(1);
   declare @kClasifi  CHAR(6);
   declare @kCONTRAT  CHAR(1);
   declare @kFAC_INT  NUMERIC(15,5);
   declare @kFEC_ANT  Fecha;
   declare @kFEC_CON  Fecha;
   declare @kFEC_ING  Fecha;
   declare @kFEC_INT  Fecha;
   declare @kFEC_REV  Fecha;
   declare @kFECHA_2  Fecha;
   declare @kFECHA    Fecha;
   declare @kMOT_BAJ  CHAR(3);
   declare @kNivel1   CHAR(6);
   declare @kNivel2   CHAR(6);
   declare @kNivel3   CHAR(6);
   declare @kNivel4   CHAR(6);
   declare @kNivel5   CHAR(6);
   declare @kNivel6   CHAR(6);
   declare @kNivel7   CHAR(6);
   declare @kNivel8   CHAR(6);
   declare @kNivel9   CHAR(6);
   declare @kNOMNUME  SMALLINT;
   declare @kNOMTIPO  SMALLINT;
   declare @kNOMYEAR  SMALLINT;
   declare @kPatron   CHAR(1);
   declare @kPER_VAR  NUMERIC(15,2);
   declare @kPuesto   CHAR(6);
   declare @kSAL_INT  NUMERIC(15,2);
   declare @kSALARIO  NUMERIC(15,2);
   declare @kTABLASS  CHAR(1);
   declare @kTurno    CHAR(6);
   declare @kZona_GE  CHAR(1);
   declare @kOLD_SAL  NUMERIC(15,2);
   declare @kOLD_INT  NUMERIC(15,2);
   declare @kOTRAS_P  NUMERIC(15,2);
   declare @kPRE_INT  NUMERIC(15,2);
   declare @kREINGRE  Char(1);
   declare @kSAL_TOT  NUMERIC(15,2);
   declare @kTOT_GRA  NUMERIC(15,2);
   declare @kTIPO     CHAR(6);
   declare @kRANGO_S  NUMERIC(15,2);
   declare @kMOD_NV1  CHAR(1);
   declare @kMOD_NV2  CHAR(1);
   declare @kMOD_NV3  CHAR(1);
   declare @kMOD_NV4  CHAR(1);
   declare @kMOD_NV5  CHAR(1);
   declare @kMOD_NV6  CHAR(1);
   declare @kMOD_NV7  CHAR(1);
   declare @kMOD_NV8  CHAR(1);
   declare @kMOD_NV9  CHAR(1);
   declare @kPLAZA    FolioGrande;
   declare @kNOMINA   SMALLINT;
   declare @kRECONTR  Booleano;
   declare @kFEC_COV  Fecha;

   declare @eACTIVO   CHAR(1);
   declare @eAUTOSAL  CHAR(1);
   declare @eClasifi  CHAR(6);
   declare @eCONTRAT  CHAR(1);
   declare @eFAC_INT  NUMERIC(15,5);
   declare @eFEC_ANT  Fecha;
   declare @eFEC_CON  Fecha;
   declare @eFEC_ING  Fecha;
   declare @eFEC_INT  Fecha;
   declare @eFEC_REV  Fecha;
   declare @eFEC_SAL  Fecha;
   declare @eFEC_BAJ  Fecha;
   declare @eFEC_BSS  Fecha;
   declare @eMOT_BAJ  CHAR(3);
   declare @eNivel1   CHAR(6);
   declare @eNivel2   CHAR(6);
   declare @eNivel3   CHAR(6);
   declare @eNivel4   CHAR(6);
   declare @eNivel5   CHAR(6);
   declare @eNivel6   CHAR(6);
   declare @eNivel7   CHAR(6);
   declare @eNivel8   CHAR(6);
   declare @eNivel9   CHAR(6);
   declare @eNOMNUME  SMALLINT;
   declare @eNOMTIPO  SMALLINT;
   declare @eNOMYEAR  SMALLINT;
   declare @ePatron   CHAR(1);
   declare @ePER_VAR  NUMERIC(15,2);
   declare @ePuesto   CHAR(6);
   declare @eSAL_INT  NUMERIC(15,2);
   declare @eSALARIO  NUMERIC(15,2);
   declare @eTABLASS  CHAR(1);
   declare @eTIP_REV  CHAR(6);
   declare @eTurno    CHAR(6);
   declare @eZona_GE  CHAR(1);
   declare @eOLD_SAL  NUMERIC(15,2);
   declare @eOLD_INT  NUMERIC(15,2);
   declare @eOTRAS_P  NUMERIC(15,2);
   declare @ePRE_INT  NUMERIC(15,2);
   declare @eREINGRE  Char(1);
   declare @eSAL_TOT  NUMERIC(15,2);
   declare @eTOT_GRA  NUMERIC(15,2);
   declare @eRANGO_S  NUMERIC(15,2);
   declare @eFEC_NIV  Fecha;
   declare @eFEC_PTO  Fecha;
   declare @eFEC_TUR  Fecha;
   declare @FechaVacia Fecha;
   declare @eFEC_KAR  Fecha;
   declare @ePLAZA    FolioGrande;
   declare @eFEC_PLA  Fecha;
   declare @eNOMINA   SMALLINT;
   declare @eFEC_NOM  Fecha;
   declare @eRECONTR  Booleano;
   declare @eFEC_COV  Fecha;

   declare CursorKardex CURSOR SCROLL_LOCKS FOR
   select	CB_AUTOSAL, CB_CLASIFI,   CB_CONTRAT, CB_FAC_INT, CB_FEC_ANT,
                CB_FEC_CON, CB_FEC_ING,   CB_FEC_INT, CB_FEC_REV, CB_FECHA,    CB_FECHA_2 ,
                CB_MOT_BAJ, CB_NIVEL1,    CB_NIVEL2 , CB_NIVEL3 , CB_NIVEL4,   CB_NIVEL5 ,   CB_NIVEL6 ,
                CB_NIVEL7 , CB_NIVEL8,    CB_NIVEL9 , CB_NOMNUME ,CB_NOMTIPO , CB_NOMYEAR ,
                CB_OTRAS_P, CB_PATRON ,   CB_PER_VAR ,CB_PRE_INT ,
                CB_PUESTO , CB_REINGRE ,  CB_SAL_INT ,CB_SAL_TOT, CB_SALARIO,  CB_TABLASS ,
                CB_TIPO ,   CB_TOT_GRA,   CB_TURNO ,  CB_ZONA_GE, CB_RANGO_S, CB_PLAZA,
                CB_NOMINA,  CB_RECONTR, CB_FEC_COV
   from Kardex
   where CB_CODIGO = @Empleado
   order by cb_fecha,cb_nivel

      	SET @FechaVacia = '12/30/1899';
        SET @eACTIVO  = 'S';
      	SET @eFEC_BAJ = @FechaVacia;
      	SET @eFEC_BSS = @FechaVacia;
      	SET @eMOT_BAJ = ' ';
      	SET @eNOMYEAR = 0;
      	SET @eNOMTIPO = 0;
      	SET @eNOMNUME = 0;
      	SET @eREINGRE = 'N';
      	SET @eSALARIO = 0;
      	SET @eSAL_INT = 0;
      	SET @eFAC_INT = 0;
      	SET @ePRE_INT = 0;
      	SET @ePER_VAR = 0;
      	SET @eSAL_TOT = 0;
      	SET @eAUTOSAL = 'N';
      	SET @eTOT_GRA = 0;
      	SET @eFEC_REV = @FechaVacia;
      	SET @eFEC_INT = @FechaVacia;
      	SET @eOTRAS_P = 0;
      	SET @eOLD_SAL = 0;
      	SET @eOLD_INT = 0;
      	SET @eRANGO_S = 0;
        SET @eFEC_PLA = @FechaVacia;
        SET @eFEC_NOM = @FechaVacia;
        SET @eRECONTR = 'S';
        SET @eFEC_COV = @FechaVacia;

	Open CursorKardex
	Fetch NEXT from CursorKardex
	into	@kAUTOSAL, @kCLASIFI, @kCONTRAT,  @kFAC_INT,  @kFEC_ANT ,
      @kFEC_CON, @kFEC_ING, @kFEC_INT , @kFEC_REV  ,@kFECHA,   @kFECHA_2,
      @kMOT_BAJ ,@kNivel1,  @kNivel2 ,  @kNivel3,   @kNivel4,  @kNivel5,  @kNivel6,
      @kNivel7,  @kNivel8,  @kNivel9,   @kNOMNUME , @kNOMTIPO, @kNOMYEAR,
      @kOTRAS_P, @kPatron,  @kPER_VAR,  @kPRE_INT,
      @kPuesto,  @kREINGRE, @kSAL_INT,  @kSAL_TOT,  @kSALARIO, @kTABLASS,
      @kTIPO,    @kTOT_GRA, @kTurno,    @kZONA_GE,  @kRANGO_S, @kPLAZA,
      @kNOMINA,  @kRECONTR, @kFEC_COV


	if ( @@FETCH_STATUS <> 0 )
	BEGIN
		Close CursorKardex
		Deallocate CursorKardex
		RETURN
	END

	
	if ( @kTIPO <> 'ALTA' )
	BEGIN
		Close CursorKardex
		Deallocate CursorKardex
		RAISERROR( 'Empleado #%d, ERROR en KARDEX: Movimiento(s) previo(s) a la ALTA', 16, 1, @Empleado )
		RETURN
	END

	while ( @@FETCH_STATUS = 0 )
	BEGIN
        	if ( @kTIPO = 'ALTA' OR @kTIPO = 'CAMBIO' OR @kTIPO = 'CIERRE' )
          	BEGIN
			if ABS( @kSALARIO - @eSALARIO ) >= 0.01
			BEGIN
				SET @kOLD_SAL = @eSALARIO;
				SET @kFEC_REV = @kFECHA;
				SET @eFEC_REV = @kFEC_REV;
				SET @eSALARIO = @kSALARIO;
				SET @eOLD_SAL = @kOLD_SAL;
			END
			else
			BEGIN
				SET @kOLD_SAL = @eOLD_SAL;
				SET @kFEC_REV = @eFEC_REV;
			END
			if ABS( @kSAL_INT - @eSAL_INT ) >= 0.01
			BEGIN
				SET @kOLD_INT = @eSAL_INT;
				if ( @kTIPO = 'CAMBIO' )
					SET @kFEC_INT = @kFECHA_2;
				else
					SET @kFEC_INT = @kFECHA;
				SET @eFEC_INT = @kFEC_INT;
				SET @eSAL_INT = @kSAL_INT;
				SET @eOLD_INT = @kOLD_INT;
			END
			else
			BEGIN
                             SET @kOLD_INT = @eOLD_INT;
	                     SET @kFEC_INT = @eFEC_INT;
			END
			SET @eFAC_INT = @kFAC_INT;
			SET @ePRE_INT = @kPRE_INT;
			SET @ePER_VAR = @kPER_VAR;
			SET @eSAL_TOT = @kSAL_TOT;
			SET @eTOT_GRA = @kTOT_GRA;
			SET @eOTRAS_P = @kOTRAS_P;
			SET @eZONA_GE = @kZONA_GE;
			SET @eTIP_REV = @kTIPO;
			SET @eFEC_SAL = @kFECHA;
			SET @eRANGO_S = @kRANGO_S;
		END
		else
		BEGIN
			SET @kSALARIO = @eSALARIO;
			SET @kOLD_SAL = @eOLD_SAL;
			SET @kFEC_REV = @eFEC_REV;
			SET @kSAL_INT = @eSAL_INT;
			SET @kOLD_INT = @eOLD_INT;
			SET @kFEC_INT = @eFEC_INT;
			SET @kFAC_INT = @eFAC_INT;
			SET @kPRE_INT = @ePRE_INT;
			SET @kPER_VAR = @ePER_VAR;
			SET @kSAL_TOT = @eSAL_TOT;
			SET @kTOT_GRA = @eTOT_GRA;
			SET @kOTRAS_P = @eOTRAS_P;
			SET @kZONA_GE = @eZONA_GE;
			SET @kRANGO_S = @eRANGO_S;
		END

		if ( @kTIPO = 'ALTA' or @kTIPO = 'PUESTO' or @kTIPO = 'CAMBIO' or @kTIPO = 'CIERRE' )
			SET @eAUTOSAL = @kAUTOSAL;
		else
			SET @kAUTOSAL = @eAUTOSAL;

		if ( @kAUTOSAL <> 'S' )
		BEGIN
			SET @kRANGO_S = 0;
			SET @eRANGO_S = 0;
		END

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'PUESTO' OR @kTIPO = 'CIERRE' OR @kTIPO = 'PLAZA' )
		BEGIN
			SET @ePUESTO   = @kPUESTO;
			SET @eFEC_PTO  = @kFECHA;
                        if ( @kTIPO = 'PLAZA' AND @eAUTOSAL = 'S' )
                             SET @kCLASIFI = @eCLASIFI;
                        else
			     SET @eCLASIFI  = @kCLASIFI;
		END
		else
		BEGIN
			SET @kPUESTO   = @ePUESTO;
			SET @kCLASIFI  = @eCLASIFI;
		END

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'AREA' OR @kTIPO = 'CIERRE' OR @kTIPO = 'PLAZA' )
		BEGIN
			if ( ( @kTIPO = 'AREA' ) OR ( @kTIPO = 'CIERRE' ) OR ( @kTIPO = 'PLAZA' ))
			BEGIN
				if ( @kNIVEL1 <> @eNIVEL1 )
     					SET @kMOD_NV1 = 'S';
            			else
                 			SET @kMOD_NV1 = 'N';
				if ( @kNIVEL2 <> @eNIVEL2 )
     					SET @kMOD_NV2 = 'S';
            			else
                 			SET @kMOD_NV2 = 'N';
				if ( @kNIVEL3 <> @eNIVEL3 )
     					SET @kMOD_NV3 = 'S';
            			else
                 			SET @kMOD_NV3 = 'N';
				if ( @kNIVEL4 <> @eNIVEL4 )
     					SET @kMOD_NV4 = 'S';
            			else
                 			SET @kMOD_NV4 = 'N';
				if ( @kNIVEL5 <> @eNIVEL5 )
     					SET @kMOD_NV5 = 'S';
            			else
                 			SET @kMOD_NV5 = 'N';
				if ( @kNIVEL6 <> @eNIVEL6 )
     					SET @kMOD_NV6 = 'S';
            			else
                 			SET @kMOD_NV6 = 'N';
				if ( @kNIVEL7 <> @eNIVEL7 )
     					SET @kMOD_NV7 = 'S';
            			else
                 			SET @kMOD_NV7 = 'N';
				if ( @kNIVEL8 <> @eNIVEL8 )
     					SET @kMOD_NV8 = 'S';
            			else
                 			SET @kMOD_NV8 = 'N';
				if ( @kNIVEL9 <> @eNIVEL9 )
     					SET @kMOD_NV9 = 'S';
            			else
                 			SET @kMOD_NV9 = 'N';
			END
			else
			BEGIN
                                if ( @kNIVEL1 <> '' )
                                    SET @kMOD_NV1 = 'S';
                                else
                                    SET @kMOD_NV1 = 'N';
                                if ( @kNIVEL2 <> '' )
                                    SET @kMOD_NV2 = 'S';
                                else
                                    SET @kMOD_NV2 = 'N';
                                if ( @kNIVEL3 <> '' )
                                    SET @kMOD_NV3 = 'S';
                                else
                                    SET @kMOD_NV3 = 'N';
                                if ( @kNIVEL4 <> '' )
                                    SET @kMOD_NV4 = 'S';
                                else
                                    SET @kMOD_NV4 = 'N';
                                if ( @kNIVEL5 <> '' )
                                    SET @kMOD_NV5 = 'S';
                                else
                                    SET @kMOD_NV5 = 'N';
                                if ( @kNIVEL6 <> '' )
                                    SET @kMOD_NV6 = 'S';
                                else
                                    SET @kMOD_NV6 = 'N';
                                if ( @kNIVEL7 <> '' )
                                    SET @kMOD_NV7 = 'S';
                                else
                                    SET @kMOD_NV7 = 'N';
                                if ( @kNIVEL8 <> '' )
                                    SET @kMOD_NV8 = 'S';
                                else
                                    SET @kMOD_NV8 = 'N';
                                if ( @kNIVEL9 <> '' )
                                    SET @kMOD_NV9 = 'S';
                                else
                                    SET @kMOD_NV9 = 'N';

			END
			SET @eNIVEL1 = @kNIVEL1;
			SET @eNIVEL2 = @kNIVEL2;
			SET @eNIVEL3 = @kNIVEL3;
			SET @eNIVEL4 = @kNIVEL4;
			SET @eNIVEL5 = @kNIVEL5;
			SET @eNIVEL6 = @kNIVEL6;
			SET @eNIVEL7 = @kNIVEL7;
			SET @eNIVEL8 = @kNIVEL8;
			SET @eNIVEL9 = @kNIVEL9;
			SET @eFEC_NIV = @kFECHA;
		END
		else
		BEGIN
			SET @kNIVEL1 = @eNIVEL1;
			SET @kNIVEL2 = @eNIVEL2;
			SET @kNIVEL3 = @eNIVEL3;
			SET @kNIVEL4 = @eNIVEL4;
			SET @kNIVEL5 = @eNIVEL5;
			SET @kNIVEL6 = @eNIVEL6;
			SET @kNIVEL7 = @eNIVEL7;
			SET @kNIVEL8 = @eNIVEL8;
			SET @kNIVEL9 = @eNIVEL9;
                        SET @kMOD_NV1 = 'N';
                        SET @kMOD_NV2 = 'N';
                        SET @kMOD_NV3 = 'N';
                        SET @kMOD_NV4 = 'N';
                        SET @kMOD_NV5 = 'N';
                        SET @kMOD_NV6 = 'N';
                        SET @kMOD_NV7 = 'N';
                        SET @kMOD_NV8 = 'N';
                        SET @kMOD_NV9 = 'N';
		END

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'PRESTA' OR @kTIPO = 'CIERRE' )
			SET @eTABLASS = @kTABLASS;
		else
			SET @kTABLASS = @eTABLASS;

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'TURNO' OR @kTIPO = 'CIERRE' OR @kTIPO = 'PLAZA' )
		BEGIN
			SET @eTURNO   = @kTURNO;
			SET @eFEC_TUR = @kFECHA;
		END
		else
			SET @kTURNO   = @eTURNO;

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'TIPNOM' OR @kTIPO = 'CIERRE' )
		BEGIN
			SET @eNOMINA  = @kNOMINA;
			SET @eFEC_NOM = @kFECHA;
		END
		else
			SET @kNOMINA  = @eNOMINA;

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'RENOVA' OR @kTIPO = 'CIERRE' )
		BEGIN
			SET @eCONTRAT = @kCONTRAT;
			SET @eFEC_CON = @kFEC_CON;
			SET @eFEC_COV = @kFEC_COV;
		END
		else
		BEGIN
			SET @kCONTRAT = @eCONTRAT;
			SET @kFEC_CON = @eFEC_CON;
			SET @kFEC_COV = @eFEC_COV;
		END

		if ( @kTIPO = 'ALTA' )
		BEGIN
			if ( @eACTIVO = 'S' )
				SET @kREINGRE = 'N';
            		else
			BEGIN
				SET @kREINGRE = 'S';
				SET @eREINGRE = 'S';
			END

			SET @eFEC_ING = @kFECHA;
			SET @eFEC_ANT = @kFEC_ANT;
			SET @eACTIVO  = 'S';
			SET @ePATRON  = @kPATRON;
			SET @kFEC_ING = @eFEC_ING;
			SET @kFEC_ANT = @eFEC_ANT;
		END
		else
		BEGIN
			SET @kREINGRE = @eREINGRE;
			SET @kPATRON  = @ePATRON;
			SET @kFEC_ING = @eFEC_ING;
			SET @kFEC_ANT = @eFEC_ANT;
		END

		if ( @kTIPO = 'BAJA' )
		BEGIN
			SET @eFEC_BAJ  =  @kFECHA;
			SET @eFEC_BSS  =  @kFECHA_2;
			SET @eMOT_BAJ  =  @kMOT_BAJ;
			SET @eACTIVO   =  'N';
			SET @eNOMYEAR  =  @kNOMYEAR;
			SET @eNOMTIPO  =  @kNOMTIPO;
			SET @eNOMNUME  =  @kNOMNUME;
			SET @ePLAZA    =  0;
		END
		else
		BEGIN
			SET @kMOT_BAJ = @eMOT_BAJ;
			SET @kNOMYEAR = @eNOMYEAR;
			SET @kNOMTIPO = @eNOMTIPO;
			SET @kNOMNUME = @eNOMNUME;
		END

		if ( @kTIPO = 'ALTA'  OR @kTIPO = 'CIERRE' OR @kTIPO = 'PLAZA' )
		BEGIN
			SET @ePLAZA    = @kPLAZA;
			SET @eFEC_PLA  = @kFECHA;
		END
		else
		BEGIN
			SET @kPLAZA    = @ePLAZA;
		END

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'BAJA' )
		BEGIN
			if ( @kTIPO = 'ALTA' )
                        begin
                             SET @kRECONTR = 'S';
                             SET @eRECONTR = 'S';
                        end
                        else
                        begin
                             SET @eRECONTR = @kRECONTR;
                        end
		END
		else
		BEGIN
			SET @kRECONTR = @eRECONTR;
		END

		update KARDEX
     		set 	CB_AUTOSAL = @kAUTOSAL,
			CB_CLASIFI = @kCLASIFI,
			CB_CONTRAT = @kCONTRAT,
			CB_FEC_ANT = @kFEC_ANT,
			CB_FEC_CON = @kFEC_CON,
			CB_FEC_ING = @kFEC_ING,
			CB_FEC_INT = @kFEC_INT,
			CB_FEC_REV = @kFEC_REV,
			CB_FECHA_2 = @kFECHA_2,
			CB_MOT_BAJ = @kMOT_BAJ,
			CB_NIVEL1  = @kNIVEL1,
			CB_NIVEL2  = @kNIVEL2,
			CB_NIVEL3  = @kNIVEL3,
			CB_NIVEL4  = @kNIVEL4,
			CB_NIVEL5  = @kNIVEL5,
			CB_NIVEL6  = @kNIVEL6,
			CB_NIVEL7  = @kNIVEL7,
			CB_NIVEL8  = @kNIVEL8,
			CB_NIVEL9  = @kNIVEL9,
			CB_MOD_NV1 = @kMOD_NV1,
			CB_MOD_NV2 = @kMOD_NV2,
			CB_MOD_NV3 = @kMOD_NV3,
			CB_MOD_NV4 = @kMOD_NV4,
			CB_MOD_NV5 = @kMOD_NV5,
			CB_MOD_NV6 = @kMOD_NV6,
			CB_MOD_NV7 = @kMOD_NV7,
			CB_MOD_NV8 = @kMOD_NV8,
			CB_MOD_NV9 = @kMOD_NV9,
			CB_NOMNUME = @kNOMNUME,
			CB_NOMTIPO = @kNOMTIPO,
			CB_NOMYEAR = @kNOMYEAR,
			CB_OTRAS_P = @kOTRAS_P,
			CB_PATRON  = @kPATRON,
			CB_PER_VAR = @kPER_VAR,
			CB_PRE_INT = @kPRE_INT,
			CB_PUESTO  = @kPUESTO,
			CB_REINGRE = @kREINGRE,
			CB_SAL_INT = @kSAL_INT,
			CB_SAL_TOT = @kSAL_TOT,
			CB_SALARIO = @kSALARIO,
			CB_TABLASS = @kTABLASS,
			CB_TOT_GRA = @kTOT_GRA,
			CB_TURNO   = @kTURNO,
			CB_ZONA_GE = @kZONA_GE,
			CB_OLD_SAL = @kOLD_SAL,
			CB_OLD_INT = @kOLD_INT,
			CB_FAC_INT = @kFAC_INT,
			CB_RANGO_S = @kRANGO_S,
      CB_PLAZA   = @kPLAZA,
      CB_NOMINA  = @kNOMINA,
      CB_RECONTR = @kRECONTR,
      CB_FEC_COV = @kFEC_COV
		where CURRENT OF CursorKardex

		Fetch NEXT from CursorKardex
		into	@kAUTOSAL, @kCLASIFI, @kCONTRAT,  @kFAC_INT,  @kFEC_ANT ,
                        @kFEC_CON, @kFEC_ING, @kFEC_INT , @kFEC_REV  ,@kFECHA,   @kFECHA_2,
                        @kMOT_BAJ ,@kNivel1,  @kNivel2 ,  @kNivel3,   @kNivel4,  @kNivel5,  @kNivel6,
                        @kNivel7,  @kNivel8,  @kNivel9,   @kNOMNUME , @kNOMTIPO, @kNOMYEAR,
                        @kOTRAS_P, @kPatron,  @kPER_VAR,  @kPRE_INT,
                        @kPuesto,  @kREINGRE, @kSAL_INT,  @kSAL_TOT,  @kSALARIO, @kTABLASS,
                        @kTIPO,    @kTOT_GRA, @kTurno,    @kZONA_GE,  @kRANGO_S, @kPLAZA,
                        @kNOMINA,  @kRECONTR, @kFEC_COV
	END

	Close CursorKardex
	Deallocate CursorKardex

	SET @eFEC_KAR = @eFEC_ING;
	if ( @eFEC_NIV > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_NIV;
	if ( @eFEC_SAL > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_SAL;
	if ( @eFEC_PTO > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_PTO;
	if ( @eFEC_TUR > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_TUR;
	if ( @eFEC_PLA > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_PLA;

	update COLABORA
	set CB_AUTOSAL = @eAUTOSAL,
            CB_ACTIVO  = @eACTIVO,
            CB_CLASIFI = @eCLASIFI,
            CB_CONTRAT = @eCONTRAT,
            CB_FAC_INT = @eFAC_INT,
            CB_FEC_ANT = @eFEC_ANT,
            CB_FEC_CON = @eFEC_CON,
            CB_FEC_ING = @eFEC_ING,
            CB_FEC_INT = @eFEC_INT,
            CB_FEC_REV = @eFEC_REV,
            CB_FEC_SAL = @eFEC_SAL,
            CB_FEC_BAJ = @eFEC_BAJ,
            CB_FEC_BSS = @eFEC_BSS,
            CB_MOT_BAJ = @eMOT_BAJ,
            CB_NIVEL1  = @eNIVEL1,
            CB_NIVEL2  = @eNIVEL2,
            CB_NIVEL3  = @eNIVEL3,
            CB_NIVEL4  = @eNIVEL4,
            CB_NIVEL5  = @eNIVEL5,
            CB_NIVEL6  = @eNIVEL6,
            CB_NIVEL7  = @eNIVEL7,
            CB_NIVEL8  = @eNIVEL8,
            CB_NIVEL9  = @eNIVEL9,
            CB_NOMNUME = @eNOMNUME,
            CB_NOMTIPO = @eNOMTIPO,
            CB_NOMYEAR = @eNOMYEAR,
            CB_PATRON  = @ePATRON,
            CB_PER_VAR = @ePER_VAR,
            CB_PUESTO  = @ePUESTO,
            CB_SAL_INT = @eSAL_INT,
            CB_SALARIO = @eSALARIO,
            CB_TABLASS = @eTABLASS,
            CB_TURNO   = @eTURNO,
            CB_ZONA_GE = @eZONA_GE,
            CB_OLD_SAL = @eOLD_SAL,
            CB_OLD_INT = @eOLD_INT,
            CB_PRE_INT = @ePRE_INT,
            CB_SAL_TOT = @eSAL_TOT,
            CB_TOT_GRA = @eTOT_GRA,
            CB_TIP_REV = @eTIP_REV,
            CB_RANGO_S = @eRANGO_S,
            CB_FEC_NIV = @eFEC_NIV,
            CB_FEC_PTO = @eFEC_PTO,
            CB_FEC_TUR = @eFEC_TUR,
            CB_FEC_KAR = @eFEC_KAR,
            CB_PLAZA   = @ePLAZA,
            CB_FEC_PLA = @eFEC_PLA,
            CB_NOMINA  = @eNOMINA,
            CB_FEC_NOM = @eFEC_NOM,
            CB_RECONTR = @eRECONTR,
            CB_FEC_COV = @eFEC_COV
	where ( CB_CODIGO = @Empleado );
END
GO


/* SP_FECHA_KARDEX: Agregar CB_FEC_COV */
/* Patch 410 # Seq: 174 Modificado ( Sug. 753 )*/
ALTER FUNCTION SP_FECHA_KARDEX (
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS @RegKardex TABLE
( CB_FECHA DATETIME,
  CB_TIPO CHAR(6),
  CB_AUTOSAL CHAR(1),
  CB_CLASIFI CHAR(6),
  CB_COMENTA VARCHAR(50),
  CB_CONTRAT CHAR(1),
  CB_FAC_INT NUMERIC(15, 5),
  CB_FEC_CAP DATETIME,
  CB_FEC_CON DATETIME,
  CB_FEC_INT DATETIME,
  CB_FEC_REV DATETIME,
  CB_FECHA_2 DATETIME,
  CB_GLOBAL CHAR(1),
  CB_TURNO CHAR(6),
  CB_MONTO NUMERIC(15, 2),
  CB_MOT_BAJ CHAR(3),
  CB_OLD_INT NUMERIC(15, 2),
  CB_OLD_SAL NUMERIC(15, 2),
  CB_OTRAS_P NUMERIC(15, 2),
  CB_PATRON CHAR(1),
  CB_PER_VAR NUMERIC(15, 2),
  CB_PRE_INT NUMERIC(15, 2),
  CB_PUESTO CHAR(6),
  CB_RANGO_S NUMERIC(15, 2),
  CB_SAL_INT NUMERIC(15, 2),
  CB_SAL_TOT NUMERIC(15, 2),
  CB_SALARIO NUMERIC(15, 2),
  CB_STATUS SMALLINT,
  CB_TABLASS CHAR(1),
  CB_TOT_GRA NUMERIC(15, 2),
  US_CODIGO SMALLINT,
  CB_ZONA_GE CHAR(1),
  CB_NIVEL1 CHAR(6),
  CB_NIVEL2 CHAR(6),
  CB_NIVEL3 CHAR(6),
  CB_NIVEL4 CHAR(6),
  CB_NIVEL5 CHAR(6),
  CB_NIVEL6 CHAR(6),
  CB_NIVEL7 CHAR(6),
  CB_NIVEL8 CHAR(6),
  CB_NIVEL9 CHAR(6),
  CB_NOMTIPO SMALLINT,
  CB_NOMYEAR SMALLINT,
  CB_NOMNUME SMALLINT,
  CB_REINGRE CHAR(1),
  CB_FEC_ING DATETIME,
  CB_FEC_ANT DATETIME,
  CB_TIP_REV CHAR(6),
  CB_FEC_SAL DATETIME,
  CB_PLAZA INTEGER,
  CB_NOMINA SMALLINT,
  CB_RECONTR CHAR(1),
  CB_FEC_COV DATETIME
) AS
begin
	INSERT INTO @RegKardex
	select TOP 1
	       CB_FECHA, CB_TIPO, CB_AUTOSAL, CB_CLASIFI, CB_COMENTA, CB_CONTRAT,
               CB_FAC_INT, CB_FEC_CAP, CB_FEC_CON, CB_FEC_INT, CB_FEC_REV, CB_FECHA_2,
       	       CB_GLOBAL, CB_TURNO, CB_MONTO, CB_MOT_BAJ, CB_OLD_INT, CB_OLD_SAL, CB_OTRAS_P,
               CB_PATRON, CB_PER_VAR, CB_PRE_INT, CB_PUESTO, CB_RANGO_S,
       	       CB_SAL_INT, CB_SAL_TOT, CB_SALARIO, CB_STATUS, CB_TABLASS, CB_TOT_GRA,
               US_CODIGO, CB_ZONA_GE, CB_NIVEL1, CB_NIVEL2, CB_NIVEL3,
       	       CB_NIVEL4, CB_NIVEL5, CB_NIVEL6, CB_NIVEL7, CB_NIVEL8, CB_NIVEL9,
               CB_NOMTIPO, CB_NOMYEAR, CB_NOMNUME, CB_REINGRE, CB_FEC_ING, CB_FEC_ANT,
               CB_TIP_REV, CB_FEC_SAL, CB_PLAZA, CB_NOMINA, CB_RECONTR, CB_FEC_COV
	from KARDEX
	WHERE ( CB_CODIGO = @Empleado ) and ( CB_FECHA <= @Fecha )
	order by CB_FECHA desc, CB_NIVEL desc

	if ( @@ROWCOUNT = 0 )
	BEGIN
		INSERT INTO @RegKardex
		select TOP 1
		       CB_FECHA, CB_TIPO, CB_AUTOSAL, CB_CLASIFI, CB_COMENTA, CB_CONTRAT,
	               CB_FAC_INT, CB_FEC_CAP, CB_FEC_CON, CB_FEC_INT, CB_FEC_REV, CB_FECHA_2,
       		       CB_GLOBAL, CB_TURNO, CB_MONTO, CB_MOT_BAJ, CB_OLD_INT, CB_OLD_SAL, CB_OTRAS_P,
	               CB_PATRON, CB_PER_VAR, CB_PRE_INT, CB_PUESTO, CB_RANGO_S,
       		       CB_SAL_INT, CB_SAL_TOT, CB_SALARIO, CB_STATUS, CB_TABLASS, CB_TOT_GRA,
	               US_CODIGO, CB_ZONA_GE, CB_NIVEL1, CB_NIVEL2, CB_NIVEL3,
       		       CB_NIVEL4, CB_NIVEL5, CB_NIVEL6, CB_NIVEL7, CB_NIVEL8, CB_NIVEL9,
	               CB_NOMTIPO, CB_NOMYEAR, CB_NOMNUME, CB_REINGRE, CB_FEC_ING, CB_FEC_ANT,
	               CB_TIP_REV, CB_FEC_SAL, CB_PLAZA, CB_NOMINA, CB_RECONTR, CB_FEC_COV
		from KARDEX
		WHERE ( CB_CODIGO = @Empleado )
		order by CB_FECHA, CB_NIVEL

		if ( @@ROWCOUNT = 0 )
		BEGIN
      		select @Fecha = '12/30/1899';
			INSERT INTO @RegKardex
			VALUES ( @Fecha, '', 'N', '', '', '',
	        	       0, @Fecha, @Fecha, @Fecha, @Fecha, @Fecha,
	        	       'N', '', 0, '', 0, 0, 0,
		               '', 0, 0, '', 0,
        		       0, 0, 0, 0, '', 0,
		               0, '', '', '', '',
        		       '', '', '', '', '', '',
	        	       0, 0, 0, 'N', @Fecha, @Fecha, '', @Fecha, 0, 0, 'S', @Fecha )
		END
	END

	RETURN
END
GO

/* SP_KARDEX_CB_FEC_COV: Regresar KARDEX.CB_FEC_COV */
/* Patch 410 # Seq: 303 Agregado ( Sug. 753 )*/
CREATE FUNCTION SP_KARDEX_CB_FEC_COV(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Fecha
AS
BEGIN
	declare @Regresa Fecha;
	set @Regresa = ( select TOP 1 CB_FEC_COV
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_FEC_COV
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '12/30/1899' )
END
GO

/* SP_REFRESH_ADICIONALES: Considerar los nuevos campos adicionales */
/* Patch 410 # Seq: 186 Modificado ( PRO - Nuevos Campos Adicionales GE )*/
ALTER PROCEDURE SP_REFRESH_ADICIONALES
as
begin
     SET NOCOUNT ON;
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional # 1', 'CB_G_TAB_1'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional # 2', 'CB_G_TAB_2'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional # 3', 'CB_G_TAB_3'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional # 4', 'CB_G_TAB_4'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional # 5', 'CB_G_TAB_5'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional # 6', 'CB_G_TAB_6'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional # 7', 'CB_G_TAB_7'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional # 8', 'CB_G_TAB_8'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional # 9', 'CB_G_TAB_9'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional #10', 'CB_G_TAB10'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional #11', 'CB_G_TAB11'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional #12', 'CB_G_TAB12'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional #13', 'CB_G_TAB13'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional #14', 'CB_G_TAB14'

     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Tabla Adicional # 1', 'CB_G_TAB_1', 35
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Tabla Adicional # 2', 'CB_G_TAB_2', 36
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Tabla Adicional # 3', 'CB_G_TAB_3', 37
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Tabla Adicional # 4', 'CB_G_TAB_4', 38
     execute SP_TITULO_CAMPO_EXTRA 'Tabla Adicional # 5', 'CB_G_TAB_5'
     execute SP_TITULO_CAMPO_EXTRA 'Tabla Adicional # 6', 'CB_G_TAB_6'
     execute SP_TITULO_CAMPO_EXTRA 'Tabla Adicional # 7', 'CB_G_TAB_7'
     execute SP_TITULO_CAMPO_EXTRA 'Tabla Adicional # 8', 'CB_G_TAB_8'
     execute SP_TITULO_CAMPO_EXTRA 'Tabla Adicional # 9', 'CB_G_TAB_9'
     execute SP_TITULO_CAMPO_EXTRA 'Tabla Adicional #10', 'CB_G_TAB10'
     execute SP_TITULO_CAMPO_EXTRA 'Tabla Adicional #11', 'CB_G_TAB11'
     execute SP_TITULO_CAMPO_EXTRA 'Tabla Adicional #12', 'CB_G_TAB12'
     execute SP_TITULO_CAMPO_EXTRA 'Tabla Adicional #13', 'CB_G_TAB13'
     execute SP_TITULO_CAMPO_EXTRA 'Tabla Adicional #14', 'CB_G_TAB14'

     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Fecha Adicional # 1', 'CB_G_FEC_1', 22
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Fecha Adicional # 2', 'CB_G_FEC_2', 23
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Fecha Adicional # 3', 'CB_G_FEC_3', 24
     execute SP_TITULO_CAMPO_EXTRA 'Fecha Adicional # 4', 'CB_G_FEC_4'
     execute SP_TITULO_CAMPO_EXTRA 'Fecha Adicional # 5', 'CB_G_FEC_5'
     execute SP_TITULO_CAMPO_EXTRA 'Fecha Adicional # 6', 'CB_G_FEC_6'
     execute SP_TITULO_CAMPO_EXTRA 'Fecha Adicional # 7', 'CB_G_FEC_7'
     execute SP_TITULO_CAMPO_EXTRA 'Fecha Adicional # 8', 'CB_G_FEC_8'

     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'L�gico Adicional # 1', 'CB_G_LOG_1', 32
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'L�gico Adicional # 2', 'CB_G_LOG_2', 33
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'L�gico Adicional # 3', 'CB_G_LOG_3', 34
     execute SP_TITULO_CAMPO_EXTRA 'L�gico Adicional # 4', 'CB_G_LOG_4'
     execute SP_TITULO_CAMPO_EXTRA 'L�gico Adicional # 5', 'CB_G_LOG_5'
     execute SP_TITULO_CAMPO_EXTRA 'L�gico Adicional # 6', 'CB_G_LOG_6'
     execute SP_TITULO_CAMPO_EXTRA 'L�gico Adicional # 7', 'CB_G_LOG_7'
     execute SP_TITULO_CAMPO_EXTRA 'L�gico Adicional # 8', 'CB_G_LOG_8'

     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Texto Adicional # 1', 'CB_G_TEX_1', 25
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Texto Adicional # 2', 'CB_G_TEX_2', 26
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Texto Adicional # 3', 'CB_G_TEX_3', 27
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Texto Adicional # 4', 'CB_G_TEX_4', 28
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional # 5', 'CB_G_TEX_5'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional # 6', 'CB_G_TEX_6'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional # 7', 'CB_G_TEX_7'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional # 8', 'CB_G_TEX_8'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional # 9', 'CB_G_TEX_9'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #10', 'CB_G_TEX10'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #11', 'CB_G_TEX11'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #12', 'CB_G_TEX12'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #13', 'CB_G_TEX13'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #14', 'CB_G_TEX14'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #15', 'CB_G_TEX15'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #16', 'CB_G_TEX16'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #17', 'CB_G_TEX17'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #18', 'CB_G_TEX18'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #19', 'CB_G_TEX19'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #20', 'CB_G_TEX20'
	execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #21', 'CB_G_TEX21'
	execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #22', 'CB_G_TEX22'
	execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #23', 'CB_G_TEX23'
	execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #24', 'CB_G_TEX24'


     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'N�mero Adicional # 1', 'CB_G_NUM_1', 29
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'N�mero Adicional # 2', 'CB_G_NUM_2', 30
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'N�mero Adicional # 3', 'CB_G_NUM_3', 31
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional # 4', 'CB_G_NUM_4'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional # 5', 'CB_G_NUM_5'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional # 6', 'CB_G_NUM_6'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional # 7', 'CB_G_NUM_7'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional # 8', 'CB_G_NUM_8'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional # 9', 'CB_G_NUM_9'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional #10', 'CB_G_NUM10'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional #11', 'CB_G_NUM11'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional #12', 'CB_G_NUM12'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional #13', 'CB_G_NUM13'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional #14', 'CB_G_NUM14'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional #15', 'CB_G_NUM15'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional #16', 'CB_G_NUM16'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional #17', 'CB_G_NUM17'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional #18', 'CB_G_NUM18'
end
GO


/* ADICIONAL_INIT: Considerar nuevos campos texto adicionales del 20-24 */
/* Patch 410 # Seq: 226 Modificado ( PRO - Nuevos Campos Adicionales GE )*/
ALTER PROCEDURE ADICIONAL_INIT
as
begin
     SET NOCOUNT ON;
     BEGIN TRANSACTION TX1
     delete from CAMPO_AD

     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_FEC_1', 4 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_FEC_2', 4 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_FEC_3', 4 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_FEC_4', 4 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_FEC_5', 4 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_FEC_6', 4 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_FEC_7', 4 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_FEC_8', 4 )

     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_LOG_1', 2 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_LOG_2', 2 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_LOG_3', 2 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_LOG_4', 2 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_LOG_5', 2 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_LOG_6', 2 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_LOG_7', 2 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_LOG_8', 2 )

     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB_1', 5, 19 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB_2', 5, 20 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB_3', 5, 21 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB_4', 5, 22 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB_5', 5, 223 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB_6', 5, 224 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB_7', 5, 225 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB_8', 5, 226 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB_9', 5, 227 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB10', 5, 228 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB11', 5, 229 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB12', 5, 230 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB13', 5, 231 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB14', 5, 232 )

     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX_1', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX_2', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX_3', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX_4', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX_5', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX_6', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX_7', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX_8', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX_9', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX10', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX11', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX12', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX13', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX14', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX15', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX16', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX17', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX18', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX19', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX20', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX21', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX22', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX23', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX24', 0 )

     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM_1', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM_2', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM_3', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM_4', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM_5', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM_6', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM_7', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM_8', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM_9', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM10', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM11', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM12', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM13', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM14', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM15', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM16', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM17', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM18', 1 )
     COMMIT TRANSACTION TX1
end
GO


/* GET_PORC_PLANACC: Tomar plan de acci�n */
/* Patch 410 # Seq: 304 Agregado ( PORTAL )*/
CREATE FUNCTION GET_PORC_PLANACC( @CB_CODIGO INT )
RETURNS FLOAT 
AS
BEGIN

/* INICIA SECCION PLAN DE ACCION */

DECLARE

@CM_DESCRIP VARCHAR(120),
@CUMPLE CHAR(1),
@TOTAL_S1 FLOAT,
@TOTAL_N1 FLOAT

 
SELECT @TOTAL_S1 =0,@TOTAL_N1=0

DECLARE CUMPLIDO_cursor CURSOR FOR 
select A.AN_CODIGO,E.EP_TERMINO
from EMP_PLAN E
left outer join ACCION A on ( A.AN_CODIGO = E.AN_CODIGO ) 
where ( E.CB_CODIGO = @CB_CODIGO ) order by E.EP_FEC_INI 

OPEN CUMPLIDO_cursor
FETCH NEXT FROM CUMPLIDO_cursor 
INTO @CM_DESCRIP,@CUMPLE

 

WHILE @@FETCH_STATUS = 0
BEGIN
      IF @CUMPLE = 'S'
            SELECT @TOTAL_S1 = @TOTAL_S1 + 1 

   
      SELECT @TOTAL_N1 = @TOTAL_N1 + 1
FETCH NEXT FROM CUMPLIDO_cursor 

INTO @CM_DESCRIP,@CUMPLE

END

CLOSE CUMPLIDO_cursor

DEALLOCATE CUMPLIDO_cursor

/*Termina cursor de Cumplidos */

 IF @TOTAL_N1 > 0

      SELECT @TOTAL_N1 = ROUND(@TOTAL_S1 / @TOTAL_N1 * 100 , 0) 

 

RETURN @TOTAL_N1
END
GO


/**************************************************/
/********SP INFONAVIT *****************************/
/**************************************************/

/* RECALCULA_INFONAVIT: Recalcula Infonvait ( SUG 904 - Historial Infonavit) */
/* Patch 410 # Seq: 309 Agregado */
CREATE PROCEDURE RECALCULA_INFONAVIT(@Empleado NumeroEmpleado) AS
BEGIN
	SET NOCOUNT ON
	declare @CB_INFTIPO Status
	declare @CB_INFCRED	Descripcion
	declare @CB_INFTASA	DescINFO	
	declare @CB_INFDISM Booleano
	declare @KI_FECHA Fecha
	declare @KI_TIPO Status
	
	
	declare @Old_InfTipo Status
	set @Old_InfTipo = 0

	declare @Old_Infcred Descripcion
	set @Old_Infcred = ''

	declare @Old_Inftasa DescINFO
	set @Old_Inftasa = 0
	
	declare @Old_AplicaDis Booleano
	set @Old_AplicaDis = 'N'

	declare @FecIniCredito Fecha
	set @FecIniCredito = '12/30/1899'

	declare @CB_INFACT Booleano
	set @CB_INFACT = 'N' 



	declare CursorInfonavit CURSOR FOR
	select CB_INFTIPO, CB_INFCRED, CB_INFTASA, CB_INFDISM,
		  KI_FECHA, KI_TIPO
 	from KARINF
	where CB_CODIGO = @Empleado
	order by KI_FECHA

	Open CursorInfonavit
	Fetch NEXT from CursorInfonavit
	into	@CB_INFTIPO, @CB_INFCRED, @CB_INFTASA, @CB_INFDISM, @KI_FECHA, @KI_TIPO
	

	if ( @KI_TIPO <> 0 )
	BEGIN
		Close CursorInfonavit
		Deallocate CursorInfonavit
		RAISERROR( 'Empleado #%d, ERROR en Kardex Infonavit: Movimiento(s) previo(s) a la alta de Infonavit', 16, 1, @Empleado )
		RETURN
	END

	while ( @@FETCH_STATUS = 0 )
	BEGIN
		/* Inicio */
		if ( @KI_TIPO in ( 0  ) ) 
		begin
			set @CB_INFACT = 'S'
			set @FecIniCredito = @KI_FECHA
			set @Old_Infcred = @CB_INFCRED
			set @Old_InfTipo = @CB_INFTIPO
			set @Old_Inftasa = @CB_INFTASA
			set @Old_AplicaDis = @CB_INFDISM
		
		end
		
		/* Reinicio */
		
		if ( @KI_TIPO = 2 ) 
		begin
			set @CB_INFACT = 'S'
			set @Old_AplicaDis = 'S'
			set @FecIniCredito = @KI_FECHA
			set @Old_InfTipo = @CB_INFTIPO
			set @Old_Inftasa = @CB_INFTASA
			set @Old_AplicaDis = @CB_INFDISM
		end
		 

		/* Suspensi�n */
		if ( @KI_TIPO = 1 )
		BEGIN
			set @CB_INFACT = 'N'
		END

		/* Cambios */
		
		/* Cambio de Tipo de Descuento */
		if ( @KI_TIPO = 3 )
		begin
			set @Old_InfTipo = @CB_INFTIPO
			set @Old_Inftasa = @CB_INFTASA
			set @Old_AplicaDis = @CB_INFDISM 
		end
		
		/* Cambio de Valor de Descuento */
		if ( @KI_TIPO = 4 )
		begin
			set @Old_Inftasa = @CB_INFTASA
			set @Old_AplicaDis = @CB_INFDISM
			
		end
	
		/* Cambio de # de Cr�dito */
		if ( @KI_TIPO = 5 ) 
		begin
			set @Old_Infcred = @CB_INFCRED
			set @Old_Inftasa = @CB_INFTASA
			set @Old_AplicaDis = @CB_INFDISM
		end
		
		
		update KARINF set CB_INFTIPO = @Old_inftipo,
					    CB_INFCRED = @Old_Infcred,	
	 			    	    CB_INFTASA = @Old_Inftasa,	
	 			         CB_INFDISM = @Old_AplicaDis
		where CURRENT OF CursorInfonavit 
	 	
		Fetch NEXT from CursorInfonavit
		into	@CB_INFTIPO, @CB_INFCRED, @CB_INFTASA, @CB_INFDISM, @KI_FECHA, @KI_TIPO				
	END

	update COLABORA set CB_INF_INI = @FecIniCredito,
					CB_INFTIPO = @Old_InfTipo,
					CB_INFCRED = @Old_InfCred,
					CB_INFTASA = @Old_InfTasa,
					CB_INFACT = @CB_INFACT,
					CB_INFDISM = @Old_AplicaDis
	where ( CB_CODIGO = @Empleado )

					
	
	close CursorInfonavit;
	deallocate CursorInfonavit;
END
GO

/* SP_DISM_TASA_INFONAVIT: Regresa la reducci�n de tasa Infonavit( SUG 904 - Historial Infonavit) */
/* Patch 410 # Seq: 310 Agregado */
CREATE  FUNCTION SP_DISM_TASA_INFONAVIT( @Empleado NumeroEmpleado, @Fecha Fecha  ) Returns NUMERIC(15, 2)
AS
BEGIN
  declare @SalarioIntegrado Numeric( 15, 2);
  declare @SalarioMinimo Numeric( 15, 2 );
  declare @FechaSalarioMinimo Fecha;
  declare @TasaIndex Numeric( 15, 2 );
  declare @ZonaGeografica Char( 1 );
  declare @Resultado SmallInt;
  declare @FechaIngreso Fecha;
  declare @AplicaDisminucion Booleano;
  declare @TipoCredito SmallInt; 
  declare @FechaInicio Fecha;
  declare @Status SmallInt;

 

  declare @TasaNueva Numeric( 15, 2 );
  SET @TasaNueva = 0;

  select @FechaInicio = MIN(KI_FECHA) from KARINF where CB_CODIGO = @Empleado 

  
  if ( @FechaInicio < @Fecha )
  begin
	set @FechaInicio = @Fecha
  end
	 

  select TOP 1 @TasaIndex = CB_INFTASA, @AplicaDisminucion = CB_INFDISM, @TipoCredito = CB_INFTIPO  
  from  KARINF
  where ( CB_CODIGO = @Empleado ) AND ( KI_FECHA <= @FechaInicio  ) order by KI_FECHA desc

  if ( @TasaIndex is not NULL ) and ( @TipoCredito = 1 ) and ( @AplicaDisminucion = 'S' )
  begin   
          
	
          select @Status = DBO.SP_STATUS_ACT( @Fecha, @Empleado )

          
          if ( @Status = 1 )
          begin

			select @ZonaGeografica = CB_ZONA_GE
			from   COLABORA
			where ( CB_CODIGO = @Empleado );
			
			
			select @FechaSalarioMinimo = MAX( SM_FEC_INI )
			from SAL_MIN
			where ( SM_FEC_INI <= @FECHA )
			
			
			IF ( @ZonaGeografica = 'A' )
				select @SALARIOMINIMO = SM_ZONA_A
				from SAL_MIN
				where ( SM_FEC_INI = @FechaSalarioMinimo )
	
			ELSE IF ( @ZonaGeografica = 'B' )
				select @SalarioMinimo = SM_ZONA_B
				from SAL_MIN
				where ( SM_FEC_INI = @FechaSalarioMinimo )
	
			ELSE IF ( @ZonaGeografica = 'C' )
				select @SalarioMinimo = SM_ZONA_C
				from SAL_MIN
				where ( SM_FEC_INI = @FechaSalarioMinimo )
			ELSE
	
			SET @SalarioMinimo = 0;
			
			if ( @SalarioMinimo > 0 )
			begin
			select @SalarioIntegrado = CB_SAL_INT
			FROM   DBO.SP_FECHA_KARDEX( @Fecha, @Empleado );
			
			SET @SalarioIntegrado = @SalarioIntegrado / @SalarioMinimo;
			
			SELECT @TasaNueva = Tasa
			FROM   DBO.SP_A80_ESCALON( @TasaIndex, @SalarioIntegrado, @Fecha );
	
			
			END
          end
end

   if ( @TasaNueva = 0 ) or ( @TasaNueva is Null )
    set @TasaNueva = COALESCE( @TasaIndex, 0)

   Return @TasaNueva

END

GO


/* SP_CALENDARIO_HORAS: Obtiene el calendario de Horas( Portal ) */
/* Patch 410 # Seq: 305 Agregado */
CREATE PROCEDURE SP_CALENDARIO_HORAS (
@FECHAINI VARCHAR(30),
@FECHAFIN VARCHAR(30),
@NUMERO VARCHAR(30),
@CB_DIA VARCHAR(30),
@CB_HORAS VARCHAR(30)
)
AS  
BEGIN
DECLARE @QUERY varchar(1000);
DECLARE @QUERY2 varchar(1000);
DECLARE @QUERY3 varchar(1000);

SET @QUERY = ' '
SET @QUERY2 = ' '
SET @QUERY3 = ' '

IF @CB_DIA ='' or @CB_DIA ='-100'
	BEGIN
		SELECT @QUERY3 = ''
	END

IF @CB_DIA <> '-100'
	BEGIN
		SELECT @QUERY3 = 'and( AU_STATUS LIKE LTRIM(RTRIM('''+@CB_DIA+'''))) '
	END


IF @CB_HORAS = '0'
	BEGIN
		SELECT @QUERY = 'and AU_HORASCK <> 0.00 ';
	END
IF @CB_HORAS = '1'
	BEGIN
		SELECT @QUERY = 'and AU_EXTRAS <> 0.00 ';
	END
IF @CB_HORAS = '2' 
	BEGIN
		SELECT @QUERY = 'and AU_PER_CG <> 0.00 ';
	END
IF @CB_HORAS = '3' 
	BEGIN
		SELECT @QUERY = 'and AU_PER_SG <> 0.00 ';
	END
IF @CB_HORAS = '4' 
	BEGIN
		SELECT @QUERY = 'and AU_DES_TRA <> 0.00 ';
	END
IF @CB_HORAS = '5' 
	BEGIN
		SELECT @QUERY = 'and AU_TARDES <> 0.00 ';
	END

IF @CB_HORAS ='' or @CB_HORAS ='-100'
	BEGIN
		SELECT @QUERY = ''
	END

IF @NUMERO ='' or @NUMERO ='-100'
	BEGIN
		SELECT @QUERY2 = ''
	END

IF @NUMERO <> '-100'
	BEGIN
		SELECT @QUERY2 = ' and ( AUSENCIA.CB_CODIGO = '''+@NUMERO+''') '
	END


	exec('
		select AUSENCIA.CB_CODIGO,
		PRETTYNAME,
		AUSENCIA.AU_FECHA,
		AUSENCIA.AU_STATUS,
		DBO.TF( 40, AUSENCIA.AU_STATUS ) Dia,
		AUSENCIA.AU_TIPODIA,
		DBO.TF( 41, AUSENCIA.AU_STATUS ) Tipo,
		AUSENCIA.AU_HORASCK,
		AUSENCIA.AU_HORAS,
		AUSENCIA.AU_EXTRAS,
		AUSENCIA.AU_PER_CG,
		AUSENCIA.AU_PER_SG,
		AUSENCIA.AU_DES_TRA,
		AUSENCIA.AU_TARDES
		from AUSENCIA

		left outer join COLABORA on(COLABORA.CB_CODIGO=AUSENCIA.CB_CODIGO)
		where ( AUSENCIA.AU_FECHA >='''+@FECHAINI+''' and AUSENCIA.AU_FECHA<='''+@FECHAFIN+''')
		'+@QUERY3+'
		'+@QUERY2+'
		'+@QUERY+'
		order by AUSENCIA.CB_CODIGO,AUSENCIA.AU_FECHA
	')
END
GO

/* SP_CALENDARIO_INCIDENCIAS: Obtiene el calendario de incidencias ( Portal ) */
/* Patch 410 # Seq: 306 Agregado ( PORTAL ) */
CREATE PROCEDURE SP_CALENDARIO_INCIDENCIAS (
@FECHAINI VARCHAR(30),
@FECHAFIN VARCHAR(30),
@NUMERO VARCHAR(30),
@TIPO VARCHAR(30)
)
AS  
BEGIN

SET @TIPO = CASE @TIPO  WHEN '-100' THEN '%' WHEN '-1' THEN '%' WHEN null THEN '%' WHEN '' THEN '%' ELSE @TIPO END


DECLARE @QUERY2 varchar(1000);

SET @QUERY2 = ''

IF @NUMERO = '' or @NUMERO ='-100'
	BEGIN
		SELECT @QUERY2 = ''
	END

IF @NUMERO <> -100
	BEGIN
		SELECT @QUERY2 = 'and ( AUSENCIA.CB_CODIGO LIKE LTRIM(RTRIM('''+@NUMERO+''' )))  '
	END

	exec('
		select 
		AUSENCIA.CB_CODIGO,
		PRETTYNAME,
		AUSENCIA.AU_FECHA,
		DBO.TF( 40, AUSENCIA.AU_STATUS ) Dia,
		DBO.TF( 41, AUSENCIA.AU_STATUS ) Tipo,
		AUSENCIA.AU_TIPO
		from AUSENCIA
		left outer join COLABORA on(COLABORA.CB_CODIGO=AUSENCIA.CB_CODIGO)
		where ( AUSENCIA.AU_FECHA >='''+@FECHAINI+''' and AUSENCIA.AU_FECHA<='''+@FECHAFIN+''')
		and ( AUSENCIA.AU_TIPO LIKE LTRIM(RTRIM('''+@TIPO+''' )))
		'+@QUERY2+'
		order by AUSENCIA.CB_CODIGO,AUSENCIA.AU_FECHA
	')
END
GO


/* GET_COMPARTE: Modificar el nombre de la base de datos de comparte a la que pertenece 
			  la base de datos (DATOS) ( PORTAL ) */
/* Patch 410 # Seq: 307 Agregado */
CREATE FUNCTION GET_COMPARTE ()
RETURNS VARCHAR(50)
AS 
BEGIN 
      
RETURN 'COMPARTE'
END 
GO

/* GET_PORC_COMPETE: Obtiene el porcentaje de cumplimiento en las competencias de un empleado ( PORTAL ) */
/* Patch 410 # Seq: 308 Agregado */
CREATE FUNCTION DBO.GET_PORC_COMPETE( @CB_CODIGO INT )
RETURNS FLOAT 
AS
BEGIN

DECLARE 
@CM_DESCRIP VARCHAR(120),
@CUMPLE CHAR(1),
@TOTAL_S FLOAT,
@TOTAL_N FLOAT
SELECT @TOTAL_S =0, @TOTAL_N=0

DECLARE CUMPLIDO_cursor CURSOR FOR 
select CM_DESCRIP,(select dbo.SP_STR_TO_BOOL( C1.CA_ORDEN - C2.CA_ORDEN )) as CUMPLE
from COMP_PTO 
left outer join COMPETEN on COMPETEN.CM_CODIGO = COMP_PTO.CM_CODIGO
left outer join EMP_COMP on ( EMP_COMP.CM_CODIGO = COMP_PTO.CM_CODIGO ) 
and ( EMP_COMP.CB_CODIGO = @CB_CODIGO )
left outer join CALIFICA C2 ON C2.CA_CODIGO = COMP_PTO.CA_CODIGO
left outer join CALIFICA C1 ON C1.CA_CODIGO = EMP_COMP.CA_CODIGO 				
where( COMP_PTO.PU_CODIGO = (SELECT CB_PUESTO FROM COLABORA WHERE CB_CODIGO = @CB_CODIGO) )
order by CM_DESCRIP


OPEN CUMPLIDO_cursor
FETCH NEXT FROM CUMPLIDO_cursor 
INTO @CM_DESCRIP,@CUMPLE

WHILE @@FETCH_STATUS = 0
BEGIN
	IF @CUMPLE = 'S'
		SELECT @TOTAL_S = @TOTAL_S + 1 
	
	SELECT @TOTAL_N = @TOTAL_N + 1

FETCH NEXT FROM CUMPLIDO_cursor 
INTO @CM_DESCRIP,@CUMPLE
   
END

CLOSE CUMPLIDO_cursor
DEALLOCATE CUMPLIDO_cursor

/* ---- Termina cursor de Cumplidos --------------------------------------*/

IF @TOTAL_N > 0
	SELECT @TOTAL_N = ROUND(@TOTAL_S / @TOTAL_N  * 100 , 0)

RETURN @TOTAL_N

END

GO

/* EFICIENCIA_POR_AREA: Labor: Regresa Tabla de Eficiencia por Area ( PORTAL )*/
/* Patch 410 # Seq: 311 Agregado */
CREATE FUNCTION EFICIENCIA_POR_AREA(
	@Inicial	Fecha,
	@Final		Fecha )
RETURNS @Tabla	TABLE (
	Codigo		CHAR(6),
	Nombre		VARCHAR(30),
	Invertidas	NUMERIC(15,2),
	Ganadas		NUMERIC(15,2),
	Eficiencia	NUMERIC(15,2) DEFAULT 0 )
AS
BEGIN

	INSERT INTO @Tabla ( Codigo, Nombre, Invertidas, Ganadas )
	SELECT CB_AREA, TB_ELEMENT, SUM(WK_TIEMPO/60.0), ROUND(SUM(WK_PIEZAS / WK_STD_HR ),4 )
	FROM WORKS JOIN AREA ON CB_AREA = TB_CODIGO
	WHERE WK_TIPO = 0 AND AU_FECHA BETWEEN @Inicial AND @Final
	GROUP BY CB_AREA, TB_ELEMENT
	ORDER BY 1, 2

	/* Segunda pasada, calcula eficiancia sobre totales */
	UPDATE @Tabla
	SET	Eficiencia = Ganadas/ Invertidas * 100.0
	WHERE Invertidas > 0

	RETURN
END
GO

/* EFICIENCIA_POR_PRODUCTO: Labor: Regresa Tabla de Eficiencia por Producto ( PORTAL )*/
/* Patch 410 # Seq: 312 Agregado */
CREATE FUNCTION EFICIENCIA_POR_PRODUCTO( 
	@Inicial	Fecha,
	@Final		Fecha )
RETURNS @Tabla	TABLE (
	Codigo		CHAR(15),
	Nombre		VARCHAR(30),
	Producidas	NUMERIC(15,2),
	Standard	NUMERIC(15,2),
	Eficiencia	NUMERIC(15,2) DEFAULT 0 )
AS
BEGIN

	INSERT INTO @Tabla ( Codigo, Nombre, Producidas, Standard )
	SELECT WORKS.AR_CODIGO, AR_NOMBRE, ROUND(SUM(WK_PIEZAS),2), SUM(WK_TIEMPO/60.0 * WK_STD_HR)
	FROM WORKS JOIN PARTES ON WORKS.AR_CODIGO = PARTES.AR_CODIGO
	WHERE WK_TIPO = 0 AND AU_FECHA BETWEEN @Inicial AND @Final
	GROUP BY WORKS.AR_CODIGO, AR_NOMBRE
	ORDER BY 1, 2

	/* Segunda pasada, calcula eficiancia sobre totales*/
	UPDATE @Tabla
	SET	Eficiencia = Producidas / Standard * 100.0
	WHERE Standard > 0

	RETURN
END
GO


/* TIEMPOMUERTO_POR_MOTIVO: Labor: Regresa Tiempo Muerto ( PORTAL )*/
/* Patch 410 # Seq: 313 Agregado */
CREATE FUNCTION TIEMPOMUERTO_POR_MOTIVO(
	@Inicial	Fecha,
	@Final		Fecha )
RETURNS @Tabla	TABLE (
	Codigo		CHAR(15),
	Nombre		VARCHAR(30),
	HorasHombre	NUMERIC(15,2),
	Porcentaje	NUMERIC(15,2) DEFAULT 0 )
AS
BEGIN
	INSERT INTO @Tabla ( Codigo, Nombre, HorasHombre )
	SELECT WK_TMUERTO, TB_ELEMENT, SUM(WK_TIEMPO/60.0)
	FROM WORKS JOIN TMUERTO ON WORKS.WK_TMUERTO = TB_CODIGO
	WHERE WK_TIPO = 2 AND AU_FECHA BETWEEN @Inicial AND @Final
	GROUP BY WK_TMUERTO, TB_ELEMENT
	ORDER BY 1


	/* Segunda pasada, calcula porcentaje */
	UPDATE @Tabla
	SET	Porcentaje = HorasHombre / ( SELECT SUM(HorasHombre) FROM @Tabla ) * 100.0

	RETURN
END
GO

/* DEFECTOS_POR_MOTIVO: Labor: Defectos ( PORTAL )*/
/* Patch 410 # Seq: 314 Agregado */
CREATE FUNCTION DEFECTOS_POR_MOTIVO(
	@Inicial	Fecha,
	@Final		Fecha )
RETURNS @Tabla	TABLE (
	Codigo		CHAR(15),
	Nombre		VARCHAR(30),
	Defectos	NUMERIC(15,2),
	Porcentaje	NUMERIC(15,2) DEFAULT 0 )
AS
BEGIN
	INSERT INTO @Tabla ( Codigo, Nombre, Defectos )
	SELECT TB_CODIGO, TB_ELEMENT, SUM(DE_PIEZAS)
	FROM DEFECTO 
		JOIN TDEFECTO ON DE_CODIGO = TB_CODIGO
		JOIN CED_INSP ON DEFECTO.CI_FOLIO = CED_INSP.CI_FOLIO
		JOIN CEDULA ON CED_INSP.CE_FOLIO = CEDULA.CE_FOLIO
	WHERE CE_FECHA BETWEEN @Inicial AND @Final
	GROUP BY TB_CODIGO, TB_ELEMENT
	ORDER BY 1


	/* Segunda pasada, calcula porcentaje */
	UPDATE @Tabla
	SET	Porcentaje = Defectos / ( SELECT SUM(Defectos) FROM @Tabla ) * 100.0

	RETURN
END
GO


/* SP_STR_TO_BOOL: Convierte un String a un booleano ( PORTAL )*/
/* Patch 410 # Seq: 315 Agregado */
CREATE FUNCTION SP_STR_TO_BOOL( @Valor INTEGER )
RETURNS CHAR(1)
AS
BEGIN
  if( @Valor >= 0 )
    Return 'S'
    Return 'N'
END
GO

/* SP_GETTIPONOMINA: Obtiene los datos de la nomina de un empleado ( KIOSCO )*/
/* Patch 410 # Seq: 316 Agregado */
CREATE PROCEDURE SP_GETTIPONOMINA
@CB_CODIGO INT,
@VERSIONTRESS VARCHAR(10)
AS
BEGIN
	IF (@VERSIONTRESS = '')
	BEGIN
		SELECT 'TIPO_NOMINA' = 0 
	END

	IF ( @VERSIONTRESS = '2006' )
	BEGIN

		SELECT 'TIPO_NOMINA' = TURNO.TU_NOMINA 
		FROM COLABORA 
		LEFT OUTER JOIN TURNO 
		ON ( TURNO.TU_CODIGO = ( SELECT DBO.SP_GET_TURNO( GETDATE(), COLABORA.CB_CODIGO ) ) ) 
		WHERE ( COLABORA.CB_CODIGO = @CB_CODIGO )

	END
	
	IF ( @VERSIONTRESS = '2007')
	BEGIN
	
		SELECT 'TIPO_NOMINA' = CB_NOMINA 
		FROM COLABORA
		WHERE CB_CODIGO = @CB_CODIGO
	
	END

END
GO

/* SP_STATUS_ACT: Arregla defecto #1021 ( Build #3 )*/
/* Patch 410 # Seq: 48 Modificado */
ALTER FUNCTION SP_STATUS_ACT (
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS SMALLINT
AS   
BEGIN
  declare @Tipo Varchar(6);
  declare @kFecha DATETIME;
  declare @FEC_ING DATETIME;
  declare @FEC_BAJ DATETIME;
  declare @ACTIVO Char(1);
  declare @Resultado Smallint;

  select @FEC_ING = CB_FEC_ING, @FEC_BAJ = CB_FEC_BAJ, @ACTIVO = CB_ACTIVO
  from   COLABORA
  where  CB_CODIGO = @Empleado

  set @Resultado = 0;

  /* Sino se resuelve con el �ltimo par de alta y baja de colabora, se investiga en Kardex */
  if  ( ( @FECHA < @FEC_ING ) AND ( @FEC_BAJ = '12/30/1899') )
    set @Resultado = 0
  else if ( ( @ACTIVO = 'N' ) AND ( @FECHA > @FEC_BAJ ))
    set @Resultado = 9
  else  if( ( @ACTIVO = 'S' ) AND ( @FECHA >= @FEC_ING )) 
    set @Resultado = 1 
  else
  begin

    select TOP 1
         @Tipo = CB_TIPO, @kFecha = CB_FECHA
    from KARDEX
    where ( CB_CODIGO = @Empleado ) AND 
          ( CB_FECHA <= @Fecha  ) AND
          ( CB_TIPO IN ( 'ALTA', 'BAJA' ))
    order by CB_FECHA DESC, CB_NIVEL DESC

    if ( @Tipo IS NULL )
        set @Resultado = 0
    else if (( @Tipo = 'BAJA' ) and ( @Fecha > @kFecha ))
    	set @Resultado = 9
    else 
    	set @Resultado = 1
  end

  RETURN @Resultado
END
GO

/* SP_AUTO_X_APROBAR : Autorizaci�nes de Horas extras por aprobar la ultima clasificacion  registrada*/
/* Patch 410 # Seq: 209 Modificado */
ALTER FUNCTION SP_AUTO_X_APROBAR(
  @Fecha Fecha,
  @Usuario Usuario )
RETURNS @HorasXAprobar TABLE (
  CB_CODIGO Integer,
  CB_NIVEL CHAR(6),
  CH_TIPO SmallInt,
  CH_HORAS Numeric( 15, 2 ),
  CH_MOTIVO VARCHAR(4),
  CH_MOT_DES VARCHAR(30),
  CH_HOR_OK Numeric( 15, 2 ),
  US_CODIGO SmallInt,
  US_COD_OK SmallInt )
as
begin
     declare @Extras Horas;
     declare @CB_CODIGO NumeroEmpleado;
     declare @CB_NIVEL Codigo;

     declare Empleados cursor for
     select CB_CODIGO, CB_NIVEL
     from SP_LISTA_EMPLEADOS( @Fecha, @Usuario );

     Open Empleados
     Fetch Next From Empleados into @CB_CODIGO, @CB_NIVEL;
     while ( @@Fetch_Status = 0 )
     begin
           insert into @HorasXAprobar
           select @CB_CODIGO, @CB_NIVEL, CH.CH_TIPO,( CH.CH_HOR_EXT + CH.CH_HOR_ORD ),
                  CH.CH_RELOJ, M.TB_ELEMENT, CH.CH_HOR_DES, CH.US_CODIGO, CH.US_COD_OK
           from CHECADAS CH
           left outer join MOT_AUTO M on ( M.TB_CODIGO = CH.CH_RELOJ )
           where ( CH.CB_CODIGO = @CB_CODIGO ) and
                 ( CH.AU_FECHA = @Fecha ) and
                 ( CH.CH_TIPO in ( 5, 6, 7, 8, 9, 10, 11, 12 ) )
           order by CH.CH_TIPO;
           Fetch Next From Empleados into @CB_CODIGO, @CB_NIVEL;
     end
     close Empleados;
     deallocate Empleados;
     RETURN
end
GO

/* SP_AUTO_X_APROBAR_TODOS : Autorizaci�nes de Horas extras por aprobar la ultima clasificacion  registrada*/
/* Patch 410 # Seq: 210 Modificado */
ALTER FUNCTION SP_AUTO_X_APROBAR_TODOS( @Fecha Fecha )
RETURNS @HorasXAprobar TABLE (
  CB_CODIGO Integer,
  CB_NIVEL CHAR(6),
  CH_TIPO SmallInt,
  CH_HORAS Numeric( 15, 2 ),
  CH_MOTIVO VARCHAR(4),
  CH_MOT_DES VARCHAR(30),
  CH_HOR_OK Numeric( 15, 2 ),
  US_CODIGO SmallInt,
  US_COD_OK SmallInt )
as
begin
     declare @Extras Horas;
     declare @CB_CODIGO NumeroEmpleado;
     declare @CB_NIVEL Codigo;
     declare @NumNivel smallint;
     declare @Nivel1 CHAR(6);
     declare @Nivel2 CHAR(6);
     declare @Nivel3 CHAR(6);
     declare @Nivel4 CHAR(6);
     declare @Nivel5 CHAR(6);
     declare @Nivel6 CHAR(6);
     declare @Nivel7 CHAR(6);
     declare @Nivel8 CHAR(6);
     declare @Nivel9 CHAR(6);
     declare @FechaNivel Fecha;
     declare @Anterior NumeroEmpleado;
     declare @CH_TIPO SmallInt;
     declare @CH_HORAS Numeric( 15, 2 );
     declare @CH_MOTIVO VARCHAR(4);
     declare @CH_MOT_DES VARCHAR(30);
     declare @CH_HOR_OK Numeric( 15, 2 );
     declare @US_CODIGO SmallInt;
     declare @US_COD_OK SmallInt;

     select @Anterior = -1;
     /* Nivel de Organigrama de Supervisores */
     select @NumNivel = ( select CAST( GL_FORMULA AS SMALLINT )
                          from GLOBAL
                          where GL_CODIGO = 120 )

     /* Checadas */
     declare Checadas cursor for
     select CH.CB_CODIGO, CH.CH_TIPO, ( CH.CH_HOR_EXT + CH.CH_HOR_ORD ), CH.CH_RELOJ, M.TB_ELEMENT, CH.CH_HOR_DES, CH.US_CODIGO, CH.US_COD_OK
     from CHECADAS CH
     left outer join MOT_AUTO M on ( M.TB_CODIGO = CH.CH_RELOJ )
     where ( CH.AU_FECHA = @Fecha ) and
     ( CH.CH_TIPO in ( 5, 6, 7, 8, 9, 10, 11, 12 ) )
     order by CH.CB_CODIGO, CH.CH_TIPO;

     Open Checadas
     Fetch Next From Checadas into
     @CB_CODIGO, @CH_TIPO, @CH_HORAS,@CH_MOTIVO,@CH_MOT_DES,@CH_HOR_OK, @US_CODIGO,@US_COD_OK;

     while ( @@Fetch_Status = 0 )
     begin
          if ( @CB_CODIGO <> @Anterior )
          begin
               select
               @FechaNivel = C.CB_FEC_NIV,
               @Nivel1 = C.CB_NIVEL1,
               @Nivel2 = C.CB_NIVEL2,
               @Nivel3 = C.CB_NIVEL3,
               @Nivel4 = C.CB_NIVEL4,
               @Nivel5 = C.CB_NIVEL5,
               @Nivel6 = C.CB_NIVEL6,
               @Nivel7 = C.CB_NIVEL7,
               @Nivel8 = C.CB_NIVEL8,
               @Nivel9 = C.CB_NIVEL9
               from COLABORA C
               where ( C.CB_CODIGO = @CB_CODIGO )

               if ( @Fecha < @FechaNivel )
                       select @CB_NIVEL = dbo.SP_FECHA_NIVEL( @Fecha, @CB_CODIGO )
               else if ( @NumNivel = 1 ) select @CB_NIVEL = @Nivel1;
               else if ( @NumNivel = 2 ) select @CB_NIVEL = @Nivel2;
               else if ( @NumNivel = 3 ) select @CB_NIVEL = @Nivel3;
               else if ( @NumNivel = 4 ) select @CB_NIVEL = @Nivel4;
               else if ( @NumNivel = 5 ) select @CB_NIVEL = @Nivel5;
               else if ( @NumNivel = 6 ) select @CB_NIVEL = @Nivel6;
               else if ( @NumNivel = 7 ) select @CB_NIVEL = @Nivel7;
               else if ( @NumNivel = 8 ) select @CB_NIVEL = @Nivel8;
               else select @CB_NIVEL = @Nivel9

               select @Anterior = @CB_CODIGO;
          end
            /* Lista de Checadas */
          insert into @HorasXAprobar ( CB_CODIGO, CB_NIVEL, CH_TIPO, CH_HORAS, CH_MOTIVO, CH_MOT_DES, CH_HOR_OK, US_CODIGO, US_COD_OK )
          values ( @CB_CODIGO, @CB_NIVEL,@CH_TIPO, @CH_HORAS,@CH_MOTIVO,@CH_MOT_DES,@CH_HOR_OK, @US_CODIGO,@US_COD_OK )

          Fetch Next From Checadas into
          @CB_CODIGO, @CH_TIPO, @CH_HORAS,@CH_MOTIVO,@CH_MOT_DES,@CH_HOR_OK, @US_CODIGO,@US_COD_OK;
     end
     close Checadas;
     deallocate Checadas;

     RETURN
end
GO


/****************************************/
/***** Actualizar versi�n de Datos ******/
/****************************************/

update GLOBAL set GL_FORMULA = '410' where ( GL_CODIGO = 133 )
GO
