/****************************************/
/****************************************/
/****** Patch para DATOS ****************/
/****************************************/
/****************************************/


IF not exists ( select * from dbo.sysindexes where name = 'IDX_NOMINA_PERIODO'  ) 
CREATE NONCLUSTERED INDEX IDX_NOMINA_PERIODO ON NOMINA
(
	PE_YEAR DESC,
	PE_TIPO DESC,
	PE_NUMERO DESC
)

GO 

-- Indice muy tardo  2.5 minutos en una BD de 61,827,266 registros
IF not exists ( select * from dbo.sysindexes where name = 'IDX_MOVIMIEN_PERIODO'  ) 
CREATE NONCLUSTERED INDEX IDX_MOVIMIEN_PERIODO ON MOVIMIEN
(
	PE_YEAR DESC,
	PE_TIPO DESC,
	PE_NUMERO DESC
)

GO 

if exists (select * from dbo.sysobjects where id = object_id(N'fnConvert_TitleCase') and xtype in (N'FN', N'IF', N'TF')) 
	drop function fnConvert_TitleCase
go 

CREATE FUNCTION fnConvert_TitleCase (@InputString VARCHAR(4000) )
RETURNS VARCHAR(4000)
AS
BEGIN
DECLARE @Index INT
DECLARE @Char CHAR(1)
DECLARE @OutputString VARCHAR(255)

SET @OutputString = LOWER(@InputString)
SET @Index = 2
SET @OutputString = STUFF(@OutputString, 1, 1,UPPER(SUBSTRING(@InputString,1,1)))

WHILE @Index <= LEN(@InputString)
BEGIN
    SET @Char = SUBSTRING(@InputString, @Index, 1)
    IF @Char IN (' ', ';', ':', '!', '?', ',', '.', '_', '-', '/', '&','''','(')
    IF @Index + 1 <= LEN(@InputString)
BEGIN
    IF @Char != ''''
    OR
    UPPER(SUBSTRING(@InputString, @Index + 1, 1)) != 'S'
    SET @OutputString =
    STUFF(@OutputString, @Index + 1, 1,UPPER(SUBSTRING(@InputString, @Index + 1, 1)))
END
    SET @Index = @Index + 1
END

RETURN ISNULL(@OutputString,'')
END

Go 

if exists (select * from dbo.sysobjects where id = object_id(N'SP_DASHLET_GET_MES_DESC') and xtype in (N'FN', N'IF', N'TF')) 
	drop function SP_DASHLET_GET_MES_DESC
go 

create function SP_DASHLET_GET_MES_DESC ( @iMes status ) 
returns Descripcion 
as
begin 
	declare @MesDesc Descripcion 
	select 
		@MesDesc = 
		case ( @iMes-1 ) 			
			when 0	then 'Enero'
			when 1 then 'Febrero' 
			when 2 then 'Marzo' 
			when 3 then 'Abril' 
			when 4 then 'Mayo' 
			when 5 then 'Junio' 
			when 6 then 'Julio' 
			when 7 then 'Agosto' 
			when 8 then 'Septiembre' 
			when 9 then 'Octubre' 
			when 10 then 'Noviembre' 
			when 11 then 'Diciembre' 
		else '' 
	end 

	return @MesDesc
end 

GO 

if exists (select * from dbo.sysobjects where id = object_id(N'GET_PERIODO_NOMBRE') and xtype in (N'FN', N'IF', N'TF')) 
	drop function GET_PERIODO_NOMBRE
go 

CREATE FUNCTION GET_PERIODO_NOMBRE (@PE_TIPO NominaTipo, @PE_NUMERO NominaNumero )
RETURNS Descripcion 
as
begin 
	declare @PeriodoNombre Descripcion 

	select @PeriodoNombre = TP_NOMBRE from TPERIODO  where TP_TIPO = @PE_TIPO  

	if ( @PE_NUMERO > 0 ) 
		set @PeriodoNombre = coalesce( @PeriodoNombre, '' ) + ' ' + cast( @PE_NUMERO as varchar(20)) 
	else
		set @PeriodoNombre = 'S/D' 

	return @PeriodoNombre 

end 

GO 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_INFO_ROTACION') AND type IN ( N'P', N'PC' ))
	drop procedure SP_INFO_ROTACION
go

CREATE PROCEDURE SP_INFO_ROTACION  
	( @FECHAINI FECHA,
	 @FECHAFIN FECHA, 
	 @CM_NIVEL0 Formula, 
	 @EmpInicio decimal(15,2)  output,
	 @EmpAltas decimal(15,2)  output,
	 @EmpBajas decimal(15,2)  output,
	 @EmpFinal decimal(15,2)  output,
	 @EmpPromedio decimal(15,2) output	  
	  )
AS
BEGIN
    SET NOCOUNT ON
	DECLARE @USUARIO int
	DECLARE @ESTEGRUPO CHAR(6);
	DECLARE @ESTADESCRIPCION VARCHAR(30);
	DECLARE @INICIO INTEGER;
	DECLARE @ALTAS  INTEGER;
	DECLARE @BAJAS  INTEGER;
	DECLARE @DIASACTIVOS INTEGER;  
  	DECLARE @GRUPO CHAR(6);
  	DECLARE @DESCRIPCION VARCHAR(30);
  	DECLARE @FECING DATETIME;
  	DECLARE @FECBAJ DATETIME;
  	DECLARE @DIASRANGO INTEGER;
  	DECLARE @NUMGRUPOS INTEGER;	

	set @USUARIO = 1 
	
	CREATE TABLE #TMPROTAI(
		TR_USER int ,
		CB_CODIGO int ,
		TR_GRUPO varchar(6) ,
		TR_DESCRIP varchar(30) ,
		CB_FEC_ING Datetime ,
		CB_FEC_BAJ Datetime
	) 

	CREATE TABLE #TMPROTA(
		TR_USER int ,
		TR_GRUPO varchar(6) ,
		TR_DESCRIP varchar(30) ,
		TR_INICIO int,
		TR_FINAL int ,
		TR_ALTAS int ,
		TR_BAJAS int ,
		TR_PROM Decimal(15,2) ,
		TR_FECHA Datetime 
	) 
 	

        SET @NumGrupos = 0
	SET @DiasRango = DATEDIFF(day, @FechaIni, @FechaFin )+1
	
	insert into #TMPROTAI (TR_USER, CB_CODIGO, TR_GRUPO, TR_DESCRIP, CB_FEC_ING, CB_FEC_BAJ) 
	select @USUARIO, COLABORA.CB_CODIGO, '', '', COLABORA.CB_FEC_ING, COLABORA.CB_FEC_BAJ from COLABORA 
	where
	(
	 (( COLABORA.CB_FEC_ING < @FECHAFIN ) AND 
      ( COLABORA.CB_FEC_BAJ < COLABORA.CB_FEC_ING OR COLABORA.CB_FEC_BAJ >= @FECHAINI )) OR 
	  ( COLABORA.CB_FEC_BAJ < COLABORA.CB_FEC_ING AND COLABORA.CB_FEC_BAJ>=@FECHAINI ) 
	)
	and (@CM_NIVEL0 = ''  or ( COLABORA.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and COLABORA.CB_NIVEL0=''))



	Declare Temporal CURSOR for
 		select TR_GRUPO, TR_DESCRIP, CB_FEC_ING, CB_FEC_BAJ
 		from   #TMPROTAI
 		where  TR_USER = @Usuario
        	order by TR_GRUPO
	OPEN TEMPORAL
	fetch NEXT FROM Temporal 
	into   @Grupo, @Descripcion, @FecIng, @FecBaj 
		WHILE @@FETCH_STATUS = 0 
		BEGIN
    			if (( @NumGrupos = 0 ) or ( @EsteGrupo <> @Grupo )) 
            		begin
               
               			if ( @NumGrupos > 0 ) 
                  		EXECUTE sp_SUBTOTAL_ROTACION 
                           		@Usuario, @EsteGrupo, @EstaDescripcion, @Inicio,
                           		@Altas, @Bajas, @DiasActivos, @DiasRango, @FechaIni 

	               		SELECT	@NumGrupos = @NumGrupos + 1, @EsteGrupo = @Grupo,
	               			@EstaDescripcion = @Descripcion,
               				@Inicio = 0,
               				@Altas = 0,
               				@Bajas = 0,            
               				@DiasActivos = 0
            		end

            		if (( @FecIng >= @FechaIni ) and ( @FecIng <= @FechaFin ))
               			SET @Altas = @Altas + 1;

            		if (( @FecBaj >= @FechaIni ) and ( @FecBaj <= @FechaFin ))
               			SET @Bajas = @Bajas + 1;

            		if (( @FecIng < @FechaIni ) or (( @FecBaj < @FecIng ) and ( @FecBaj >= @FechaIni )))
            			SELECT @Inicio = @Inicio + 1, @FecIng = @FechaIni

            		if (( @FecBaj > @FechaFin ) or ( @FecBaj < @FecIng )) 
               			SET @FecBaj = @FechaFin;

            		SET @DiasActivos = DATEDIFF(day, @FecIng,@FecBaj) + 1 + @DiasActivos ;
			fetch NEXT FROM Temporal 
 			into   @Grupo, @Descripcion, @FecIng, @FecBaj 
  		END;

   	close Temporal
    	deallocate Temporal
        

        if ( @NumGrupos > 0 ) 
           EXECUTE SP_DASHLET_ROTACION_SUBTOTAL 
                   @Usuario, @EsteGrupo, @EstaDescripcion, @Inicio,
                   @Altas, @Bajas, @DiasActivos, @DiasRango, @FechaIni 

  	
	select 
		@EmpInicio = TR_INICIO * 1.0, 
		@EmpAltas = TR_ALTAS * 1.0, 
		@EmpBajas = TR_BAJAS * 1.0, 
		@EmpFinal = TR_FINAL * 1.0, 
		@EmpPromedio= TR_PROM * 1.0
	from #TMPROTA 
	
	drop table #TMPROTA
	drop table #TMPROTAI 

END

GO 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_DASHLET_ROTACION_COMPARACION_MES') AND type IN ( N'P', N'PC' ))
	drop procedure SP_DASHLET_ROTACION_COMPARACION_MES
go


create procedure SP_DASHLET_ROTACION_COMPARACION_MES ( @FechaMesIni Fecha,  @FechaMesFin Fecha, @CM_NIVEL0 Formula ) 
as
begin 
	-- Mes actual 	
	DECLARE @EmpInicio decimal(15,2)
	DECLARE @EmpAltas decimal(15,2)
	DECLARE @EmpBajas decimal(15,2)
	DECLARE @EmpFinal decimal(15,2)
	DECLARE @EmpPromedio decimal(15,2)
	DECLARE @MesActualDesc Descripcion 
	DECLARE @MesAntDesc	Descripcion 

	-- Mes anterior 
	DECLARE @Ant_FechaMesIni Fecha	
	DECLARE @Ant_FechaMesFin Fecha 
	DECLARE @Ant_EmpInicio decimal(15,2)
	DECLARE @Ant_EmpAltas decimal(15,2)
	DECLARE @Ant_EmpBajas decimal(15,2)
	DECLARE @Ant_EmpFinal decimal(15,2)
	DECLARE @Ant_EmpPromedio decimal(15,2)

	EXEC SP_INFO_ROTACION    @FechaMesIni  ,@FechaMesFin  ,@CM_NIVEL0  ,@EmpInicio OUTPUT  ,@EmpAltas OUTPUT  ,@EmpBajas OUTPUT  ,@EmpFinal OUTPUT  ,@EmpPromedio OUTPUT

	set @Ant_FechaMesIni = dateadd( month, -1, @FechaMesIni ) 
	set @Ant_FechaMesFin = dateadd( day, -1, @FechaMesIni ) 

	EXEC SP_INFO_ROTACION    @Ant_FechaMesIni  ,@Ant_FechaMesFin  ,@CM_NIVEL0  ,@Ant_EmpInicio OUTPUT  ,@Ant_EmpAltas OUTPUT  ,@Ant_EmpBajas OUTPUT  ,@Ant_EmpFinal OUTPUT  ,@Ant_EmpPromedio OUTPUT
	
		
	set @MesActualDesc = dbo.SP_DASHLET_GET_MES_DESC( month(@FechaMesIni ) ) 
	set @MesAntDesc = dbo.SP_DASHLET_GET_MES_DESC( month(@Ant_FechaMesIni ) ) 
				
	select ID=1, Descripcion=@MesAntDesc  , Altas= @Ant_EmpAltas*1.0, Bajas= @Ant_EmpBajas*1.0
	union all 
	select ID=2, Descripcion=@MesActualDesc, Altas= @EmpAltas*1.0, Bajas= @EmpBajas*1.0

end
GO 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_INDICADOR_SHOW') AND type IN ( N'P', N'PC' ))
	drop procedure SP_INDICADOR_SHOW 
go


create procedure SP_INDICADOR_SHOW( @valor Pesos, @valorAnterior Pesos, @textoAdicional Formula= '') 
as
begin 
	declare @tendencia Pesos 
	set @valorAnterior = coalesce( @ValorAnterior, 0 ) 
	set @valor = coalesce( @valor, 0 ) 

	if ( @valorAnterior = 0.00 and @valor = 0.00 ) 
		set @tendencia = 0 
	else
	if ( @valorAnterior = 0.00 ) 
		set @tendencia = 100.00 
	else 
		set @tendencia =  ( @valor  - @valorAnterior  ) / @valorAnterior * 100.00 
	
	select valor=@valor , tendencia=@tendencia,  anterior = @valorAnterior, textoAdicional = @textoAdicional 
end 


GO 
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_INDICADOR_ROTACION_MES') AND type IN ( N'P', N'PC' ))
	drop procedure SP_INDICADOR_ROTACION_MES 
go


create procedure SP_INDICADOR_ROTACION_MES ( @FechaMesIni Fecha,  @FechaMesFin Fecha, @CM_NIVEL0 Formula, @iCual status ) 
as
begin 
	-- Mes actual 	
	DECLARE @EmpInicio decimal(15,2)
	DECLARE @EmpAltas decimal(15,2)
	DECLARE @EmpBajas decimal(15,2)
	DECLARE @EmpFinal decimal(15,2)
	DECLARE @EmpPromedio decimal(15,2)
	DECLARE @MesActualDesc Descripcion 
	DECLARE @MesAntDesc	Descripcion 

	-- Mes anterior 
	DECLARE @Ant_FechaMesIni Fecha	
	DECLARE @Ant_FechaMesFin Fecha 
	DECLARE @Ant_EmpInicio decimal(15,2)
	DECLARE @Ant_EmpAltas decimal(15,2)
	DECLARE @Ant_EmpBajas decimal(15,2)
	DECLARE @Ant_EmpFinal decimal(15,2)
	DECLARE @Ant_EmpPromedio decimal(15,2)

	EXEC SP_INFO_ROTACION    @FechaMesIni  ,@FechaMesFin  ,@CM_NIVEL0  ,@EmpInicio OUTPUT  ,@EmpAltas OUTPUT  ,@EmpBajas OUTPUT  ,@EmpFinal OUTPUT  ,@EmpPromedio OUTPUT

	set @Ant_FechaMesIni = dateadd( month, -1, @FechaMesIni ) 
	set @Ant_FechaMesFin = dateadd( day, -1, @FechaMesIni ) 

	EXEC SP_INFO_ROTACION    @Ant_FechaMesIni  ,@Ant_FechaMesFin  ,@CM_NIVEL0  ,@Ant_EmpInicio OUTPUT  ,@Ant_EmpAltas OUTPUT  ,@Ant_EmpBajas OUTPUT  ,@Ant_EmpFinal OUTPUT  ,@Ant_EmpPromedio OUTPUT

	declare @valor Pesos 
	declare @Ant_valor Pesos

	if ( @iCual  = 1 )
	begin 		
		set @valor = @EmpFinal
		set @Ant_valor = @Ant_EmpFinal			
	end 
	else
	if ( @iCual  = 2 )
	begin 		
		set @valor = @EmpPromedio
		set @Ant_valor = @Ant_EmpPromedio			
	end 
	else
	if ( @iCual  = 3 )
	begin 		
		set @valor = @EmpBajas
		set @Ant_valor = @Ant_EmpBajas			
	end 
	else 
	if ( @iCual  = 4 )
	begin 
		if ( @EmpPromedio > 0 ) 
			set @valor = ( @EmpBajas / @EmpPromedio ) * 100.00 
		else
			set @valor = 0.00 
				
		if ( @Ant_EmpPromedio > 0 ) 
			set @Ant_valor = ( @Ant_EmpBajas / @Ant_EmpPromedio ) * 100.00 
		else
			set @Ant_valor = 0.00 
	end 
	
	exec SP_INDICADOR_SHOW @valor, @Ant_valor

end
GO 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'Sesion_SetValues') AND type IN ( N'P', N'PC' ))
	drop procedure Sesion_SetValues
go


/*** Procedimiento que siempre se ejecutara al cargar un tablero en Tress.exe ***/ 
CREATE PROCEDURE Sesion_SetValues (
	@SesionIDActual	FolioGrande,
	@SesionID	FolioGrande OUTPUT,
	@US_CODIGO	Usuario, 
	@CM_CODIGO	CodigoEmpresa, 	
	@PE_TIPO	FolioChico, 
	@PE_YEAR	FolioChico, 
	@PE_NUMERO	FolioChico, 
	@FechaActiva	Fecha	
	 )
AS
BEGIN 
	declare @PE_MES Mes 
	declare @MesIni Fecha 
	declare @MesFin Fecha 

	set nocount on

	select @PE_MES = PE_MES from PERIODO where @PE_TIPO = PE_TIPO and  @PE_YEAR = PE_YEAR and  @PE_NUMERO = PE_NUMERO
	if ( @PE_MES > 12 ) set @PE_MES = 12 
	set @PE_MES = coalesce( @PE_MES, 1 )

	select @MesIni = cast(0 as datetime)
	declare @dt Varchar(20)	
	
	set @dt = '' 
	if (@PE_MES > 9 ) 
		set @dt = CAST(@PE_YEAR as varchar(4)) + CAST(@PE_MES as varchar(2)) + '01'
	else
		set @dt = CAST(@PE_YEAR as varchar(4)) + '0' + CAST(@PE_MES as varchar(2)) +'01' 
			
	set @MesIni = CONVERT(datetime, @dt, 112 ) 
	set @MesFin = dateadd(day, -1,  dateadd( month, 1, @MesIni ) );  
	set @SesionID = @SesionIDActual;
	exec #COMPARTE..Sesion_Update @SesionID output, @US_CODIGO, @CM_CODIGO, @PE_TIPO, @PE_YEAR, @PE_NUMERO, @PE_MES, @FechaActiva, @MesIni, @MesFin 

END 

GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'Sesion_Delete') AND type IN ( N'P', N'PC' ))
	drop procedure Sesion_Delete
go

CREATE PROCEDURE Sesion_Delete ( @SesionID FolioGrande ) 
AS
BEGIN 
	exec #COMPARTE..Sesion_Delete @SesionID 
END 
GO 

if exists (select * from dbo.sysobjects where id = object_id(N'Sesion_GetValues') and xtype in (N'FN', N'IF', N'TF')) 
	drop function Sesion_GetValues
go 

CREATE function Sesion_GetValues( @SesionID FolioGrande ) 
returns table 
as
return (  
select PE_TIPO, PE_YEAR, PE_NUMERO, PE_MES, FechaActiva, MesIni, MesFin, US_CODIGO, CM_CODIGO, CM_NIVEL0  from #COMPARTE..Sesion 
where SesionID = @SesionID 
) 
go 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_DASHLET_MOTIVOSBAJAS_MES') AND type IN ( N'P', N'PC' ))
	drop procedure SP_DASHLET_MOTIVOSBAJAS_MES
go

create procedure SP_DASHLET_MOTIVOSBAJAS_MES ( 
	@MesIni Fecha, 
	@MesFin Fecha, 
	@CM_NIVEL0 Formula) 
as
begin 
	set nocount on
	select dbo.fnConvert_TitleCase( MB.TB_ELEMENT) Motivo, count(*) Cantidad from KARDEX 
	join COLABORA CB on KARDEX.CB_CODIGO = CB.CB_CODIGO 
	join MOT_BAJA MB on MB.TB_CODIGO = KARDEX.CB_MOT_BAJ 
	where CB_FECHA between @MesIni and @MesFin and CB_TIPO = 'BAJA' 
	and  (@CM_NIVEL0 = ''  or ( CB.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB.CB_NIVEL0=''))
	group by MB.TB_ELEMENT 
	order by Cantidad desc 

end 

GO 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_DASHLET_RUNQUERY') AND type IN ( N'P', N'PC' ))
	drop procedure SP_DASHLET_RUNQUERY

GO

CREATE PROCEDURE SP_DASHLET_RUNQUERY( @SesionID FolioGrande, @Query nvarchar(max) )	
AS
BEGIN
	
	DECLARE @Params		NVARCHAR( MAX )
	
	-- Variables de sesion 
	declare @FechaActiva Fecha 
	declare @US_CODIGO Usuario 
	declare @PE_TIPO NominaTipo 
	declare @PE_YEAR Anio
	declare @PE_NUMERO NominaNumero 
	declare @PE_MES Mes 
	declare @MesIni Fecha 
	declare @MesFin Fecha 
	declare @CM_NIVEL0 Formula 

	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED


	select @FechaActiva = FechaActiva, @MesIni = MesIni, @MesFin = MesFin, @PE_MES = PE_MES, 
	 @PE_TIPO = PE_TIPO, @PE_YEAR = PE_YEAR, @PE_NUMERO = PE_NUMERO, @CM_NIVEL0 = CM_NIVEL0, 
	 @US_CODIGO = US_CODIGO from dbo.Sesion_GetValues( @SesionID ) 


	SET @Params	= N'@US_CODIGO int, @FechaActiva DATETIME, @MesIni DATETIME, @MesFin DATETIME, @PE_TIPO int,  @PE_YEAR int, @PE_NUMERO int, @PE_MES int, @CM_NIVEL0 varchar(256) '
	EXEC sp_executesql @Query, @Params, @US_CODIGO, @FechaActiva, @MesIni, @MesFin,  @PE_TIPO,  @PE_YEAR, @PE_NUMERO, @PE_MES, @CM_NIVEL0

END



go 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_DASHLET_RUN') AND type IN ( N'P', N'PC' ))
	drop procedure SP_DASHLET_RUN

GO



CREATE PROCEDURE SP_DASHLET_RUN( @SesionID FolioGrande, @DashletID  FolioGrande )	
AS
BEGIN	
	declare @Query nvarchar(max)
	
	select @Query = D.DashletQuery from #COMPARTE..Dashlets  D where D.DashletID = @DashletID 

	exec SP_DASHLET_RUNQUERY @SesionID, @Query 
END 

go 

if exists (select * from dbo.sysobjects where id = object_id(N'SP_DASHLET_GET_INCIDENCIA_DESC') and xtype in (N'FN', N'IF', N'TF')) 
	drop function SP_DASHLET_GET_INCIDENCIA_DESC
go 

create function SP_DASHLET_GET_INCIDENCIA_DESC( @Incidencia status) 
returns Descripcion 
as
begin 
	return (select case @Incidencia 
        when 2  then 'Vacaciones'
        when 3  then 'Incapacidad'         
        when 4  then 'Perm. C/Goce'  
        when 5  then 'Perm. S/Goce'  
        when 6  then 'FJ'  
        when 7  then 'Suspensi�n'  
        when 8  then 'Otros'  
    else 'Sin clasificar' 
	end 
	)
end 
go 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_DASHLET_INCIDENCIAS_RH') AND type IN ( N'P', N'PC' ))
	drop procedure SP_DASHLET_INCIDENCIAS_RH

GO

create procedure SP_DASHLET_INCIDENCIAS_RH ( @Fecha Fecha, @CM_NIVEL0 Formula ) 
as
begin 
select  
	Incidencia as ID, 
    dbo.SP_DASHLET_GET_INCIDENCIA_DESC(Incidencia)  Descripcion, 
    count(*) Cantidad
    from (
select CB_CODIGO Empleado, dbo.fnConvert_TitleCase(REPLACE(PRETTYNAME, ',', '')) Nombre, dbo.SP_STATUS_EMP(@Fecha, CB_CODIGO ) Incidencia from COLABORA 
where COLABORA.CB_ACTIVO = 'S'  and (@CM_NIVEL0 = ''  or ( COLABORA.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and COLABORA.CB_NIVEL0=''))
) Empleados 
where Incidencia > 1 and Incidencia <> 9 
group by Incidencia 
 order by Cantidad desc 

end 
GO 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_DASHLET_INCIDENCIAS_LISTA_RH') AND type IN ( N'P', N'PC' ))
	drop procedure SP_DASHLET_INCIDENCIAS_LISTA_RH

GO

create procedure SP_DASHLET_INCIDENCIAS_LISTA_RH ( @Fecha Fecha, @CM_NIVEL0 Formula ) 
as
begin 
	select dbo.SP_DASHLET_GET_INCIDENCIA_DESC(Incidencia)  Incidencia,  Empleado, Nombre from ( 
	select CB_CODIGO Empleado, dbo.fnConvert_TitleCase(REPLACE(PRETTYNAME, ',', '')) Nombre, dbo.SP_STATUS_EMP(@Fecha, CB_CODIGO ) Incidencia from COLABORA 
	where COLABORA.CB_ACTIVO = 'S'  and (@CM_NIVEL0 = ''  or ( COLABORA.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and COLABORA.CB_NIVEL0=''))	
	) Lista 
	where LIsta.Incidencia > 1 and Incidencia <> 9 
	order by Incidencia, Empleado  
end 
GO 


/* Indicadores de Nominas */ 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_GET_PERIODO_ANTERIOR') AND type IN ( N'P', N'PC' ))
	drop procedure SP_GET_PERIODO_ANTERIOR 
go


create procedure SP_GET_PERIODO_ANTERIOR ( 
	 @PE_TIPO NominaTipo, 
	 @PE_YEAR Anio,
	 @PE_NUMERO NominaNumero, 	 
	 @Ant_PE_YEAR Anio  output,
	 @Ant_PE_NUMERO NominaNumero  output)
as
begin 
	declare @Pe_Uso status 
	declare @Pe_Fec_Pag Fecha 
	select @Pe_Uso = PE_USO, @Pe_Fec_Pag = PE_FEC_PAG  from Periodo where Pe_tipo = @PE_TIPO and pe_year = @PE_YEAR and pe_numero = @PE_NUMERO 

	-- La anterior debera estar afectada 
	if ( @Pe_Uso = 0 ) 
		select top 1 @Ant_PE_YEAR = PE_YEAR, @Ant_PE_NUMERO = PE_NUMERO  from PERIODO  
		where PE_TIPO = @PE_TIPO and PE_YEAR <= @PE_YEAR and PE_USO = 0  and PE_STATUS >5  and PE_FEC_PAG < @PE_FEC_PAG
		order by PE_FEC_PAG desc 
	else
	begin 
		set @Ant_PE_NUMERO = 0 
		set @Ant_PE_YEAR = 0 
	end 

	set @Ant_PE_NUMERO = coalesce( @Ant_PE_NUMERO, 0 ) 
	set @Ant_PE_YEAR = coalesce( @Ant_PE_YEAR, 0 ) 
end 

go 


 
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_INDICADOR_NOMINA') AND type IN ( N'P', N'PC' ))
	drop procedure SP_INDICADOR_NOMINA
go


create procedure SP_INDICADOR_NOMINA ( 
	 @PE_TIPO NominaTipo, 
	 @PE_YEAR Anio,
	 @PE_NUMERO NominaNumero, 	 
	 @PE_MES	Status, 
	 @CM_NIVEL0 Formula, 
	 @iCual status
 ) 
as
begin 
	
	declare @Ant_PE_YEAR Anio
	declare @Ant_PE_NUMERO NominaNumero 

	declare @Valor Pesos 
	declare @ValorAnterior Pesos

	exec SP_GET_PERIODO_ANTERIOR @PE_TIPO, @PE_YEAR, @PE_NUMERO, @Ant_PE_YEAR output, @Ant_PE_NUMERO output 

	set @Valor = 0 
	set @ValorAnterior = 0
	
	-- Empleados Calculaods
	if ( @iCual = 1 ) 
	begin 
		select @Valor = count(*)*1.0 from NOMINA where PE_TIPO = @PE_TIPO and PE_YEAR = @PE_YEAR and PE_NUMERO = @PE_NUMERO and NO_STATUS >=4 
 		and (@CM_NIVEL0 = ''  or ( CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB_NIVEL0=''))

		select @ValorAnterior = count(*)*1.0 from NOMINA where PE_TIPO = @PE_TIPO and PE_YEAR = @Ant_PE_YEAR and PE_NUMERO = @Ant_PE_NUMERO and NO_STATUS > 5 
 		and (@CM_NIVEL0 = ''  or ( CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB_NIVEL0=''))
	end 
	else
	-- Neto a pagar 
	if ( @iCual = 2 ) 
	begin 		
		select  @Valor=  cast( SUM(NO_NETO)  as Decimal(15,2))  from NOMINA	NO
		where NO.PE_TIPO = @PE_TIPO and NO.PE_YEAR = @PE_YEAR and NO.PE_NUMERO = @PE_NUMERO and NO_STATUS >= 4 
 		and (@CM_NIVEL0 = ''  or ( CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB_NIVEL0=''))

		select  @ValorAnterior= cast( SUM(NO_NETO) as Decimal(15,2))  from NOMINA NO				
		where NO.PE_TIPO = @PE_TIPO and NO.PE_YEAR = @Ant_PE_YEAR and NO.PE_NUMERO = @Ant_PE_NUMERO and NO_STATUS > 5 
 		and (@CM_NIVEL0 = ''  or ( CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB_NIVEL0=''))
	end 
	else
	-- Recibos a Timbrar 
	if ( @iCual = 3 ) 
	begin 	
		select @Valor = count(*) from NOMINA NO
		join PERIODO PE on NO.PE_TIPO = PE.PE_TIPO and NO.PE_YEAR = PE.PE_YEAR and NO.PE_NUMERO = PE.PE_NUMERO 
		where  PE.PE_TIPO = @PE_TIPO and PE.PE_YEAR = @PE_YEAR and PE.PE_MES = @PE_MES and  NO_TIMBRO <> 2 and PE.PE_STATUS > 5		
		and (@CM_NIVEL0 = ''  or ( CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB_NIVEL0=''))

		set @ValorAnterior = @Valor 

	end 
	else
	-- Neto a pagar  extras
	if ( @iCual = 4 ) 
	begin 		
		select  @Valor=  cast( SUM(MO_PERCEPC+MO_DEDUCCI)  as Decimal(15,2))  from MOVIMIEN MO
		join CONCEPTO on MO.CO_NUMERO = CONCEPTO.CO_NUMERO
		join NOMINA NO on NO.PE_TIPO = MO.PE_TIPO and NO.PE_YEAR = MO.PE_YEAR and NO.PE_NUMERO = MO.PE_NUMERO and MO.CB_CODIGO = NO.CB_CODIGO 
		where CONCEPTO.CO_TIPO in (1,4)	and ( CONCEPTO.CO_SAT_TPP = 'P019' or  CONCEPTO.CO_SAT_TPN = 'P019'  )
		and NO.PE_TIPO = @PE_TIPO and NO.PE_YEAR = @PE_YEAR and NO.PE_NUMERO = @PE_NUMERO and NO_STATUS >= 4 
 		and (@CM_NIVEL0 = ''  or ( CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB_NIVEL0=''))

		select  @ValorAnterior= cast( SUM(MO_PERCEPC+MO_DEDUCCI)  as Decimal(15,2))  from MOVIMIEN MO
		join CONCEPTO on MO.CO_NUMERO = CONCEPTO.CO_NUMERO
		join NOMINA NO on NO.PE_TIPO = MO.PE_TIPO and NO.PE_YEAR = MO.PE_YEAR and NO.PE_NUMERO = MO.PE_NUMERO and MO.CB_CODIGO = NO.CB_CODIGO 
		where CONCEPTO.CO_TIPO in (1,4)	and ( CONCEPTO.CO_SAT_TPP = 'P019' or  CONCEPTO.CO_SAT_TPN = 'P019'  )
		and NO.PE_TIPO = @PE_TIPO and NO.PE_YEAR = @Ant_PE_YEAR and NO.PE_NUMERO = @Ant_PE_NUMERO and NO_STATUS > 5 
 		and (@CM_NIVEL0 = ''  or ( CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB_NIVEL0=''))
	end 

	exec SP_INDICADOR_SHOW @Valor, @ValorAnterior 

end

go 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_DASHLET_NOMINA_DIFERENCIAS_PERCEPCIONES') AND type IN ( N'P', N'PC' ))
	drop procedure SP_DASHLET_NOMINA_DIFERENCIAS_PERCEPCIONES 
go


create procedure SP_DASHLET_NOMINA_DIFERENCIAS_PERCEPCIONES ( @PE_TIPO NominaTipo, 
	 @PE_YEAR Anio,
	 @PE_NUMERO NominaNumero, 
	 @CM_NIVEL0 Formula ) 
as
begin 
		
	declare @Ant_PE_YEAR Anio
	declare @AnT_PE_NUMERO NominaNumero
	 
	exec SP_GET_PERIODO_ANTERIOR @PE_TIPO, @PE_YEAR, @PE_NUMERO, @Ant_PE_YEAR output, @Ant_PE_NUMERO output 
	
	
	select MO.PE_NUMERO, CONCEPTO.CO_NUMERO, CONCEPTO.CO_DESCRIP, SUM(MO_PERCEPC+MO_DEDUCCI) MONTO into #PeriodoActual from MOVIMIEN MO
	join CONCEPTO on MO.CO_NUMERO = CONCEPTO.CO_NUMERO
	join NOMINA NO on NO.PE_TIPO = MO.PE_TIPO and NO.PE_YEAR = MO.PE_YEAR and NO.PE_NUMERO = MO.PE_NUMERO and MO.CB_CODIGO = NO.CB_CODIGO  
	where 		
		MO.PE_NUMERO = @PE_NUMERO and MO.PE_TIPO = @PE_TIPO and MO.PE_YEAR = @PE_YEAR and (CONCEPTO.CO_SAT_CLP > 0  or CONCEPTO.CO_SAT_CLN > 0 or CONCEPTO.CO_SAT_EXE > 0  ) 
		and (@CM_NIVEL0 = ''  or ( NO.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and  NO.CB_NIVEL0=''))
	group BY MO.PE_NUMERO, CONCEPTO.CO_NUMERO,CONCEPTO.CO_DESCRIP
	order by MONTO desc, CONCEPTO.CO_NUMERO

	select 		
		MO.PE_NUMERO, CONCEPTO.CO_NUMERO, CONCEPTO.CO_DESCRIP, SUM(MO_PERCEPC+MO_DEDUCCI) MONTO into #PeriodoAnterior from MOVIMIEN MO
	join CONCEPTO on MO.CO_NUMERO = CONCEPTO.CO_NUMERO
	join NOMINA NO on NO.PE_TIPO = MO.PE_TIPO and NO.PE_YEAR = MO.PE_YEAR and NO.PE_NUMERO = MO.PE_NUMERO and MO.CB_CODIGO = NO.CB_CODIGO  
	where 		
		MO.PE_NUMERO = @Ant_PE_NUMERO and MO.PE_TIPO = @PE_TIPO and MO.PE_YEAR = @Ant_PE_YEAR and (CONCEPTO.CO_SAT_CLP > 0  or CONCEPTO.CO_SAT_CLN > 0 or CONCEPTO.CO_SAT_EXE > 0  ) 
		and (@CM_NIVEL0 = ''  or ( NO.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and  NO.CB_NIVEL0=''))
	group BY MO.PE_NUMERO, CONCEPTO.CO_NUMERO,CONCEPTO.CO_DESCRIP 
	order by MONTO desc, CONCEPTO.CO_NUMERO


	declare @Query nvarchar(1000) 
	
	set @Query = 
	'select * from ( 
	select top 5 ID=CO_NUMERO, Descripcion=''(''+ cast(CO_NUMERO as varchar(10))  + '') '' + LEFT( dbo.fnConvert_TitleCase(CO_DESCRIP), 20 ), 	
	cast( MontoAnterior as Decimal(15,2)) ['+  dbo.GET_PERIODO_NOMBRE(@PE_TIPO, @Ant_PE_NUMERO ) +'], 
	cast( Monto  as Decimal(15,2))  ['+ dbo.GET_PERIODO_NOMBRE(@PE_TIPO, @PE_NUMERO ) +']	
		 from (
		select Act.CO_NUMERO, Act.CO_DESCRIP, Act.MONTO, COALESCE(Ant.Monto,0) MontoAnterior , Act.Monto - COALESCE(Ant.Monto,0) Diferencia, ABS( Act.Monto - COALESCE(Ant.Monto,0) ) DiferenciaAbs from #PeriodoActual Act
		left outer join #PeriodoAnterior Ant on Act.CO_NUMERO = Ant.CO_NUMERO
		) Diferencias
	where MontoAnterior <> Monto 
	order by (
		case 
			when MontoAnterior <> 0.00 then ABS(  (Diferencia/MontoAnterior) * 100.00 ) 
			else 0.00 
		end 
	) desc ) Diffs 
	order by  ['+  dbo.GET_PERIODO_NOMBRE(@PE_TIPO, @Ant_PE_NUMERO ) +'] -  ['+ dbo.GET_PERIODO_NOMBRE(@PE_TIPO, @PE_NUMERO ) +'] '
	
	EXEC sp_executesql @Query 
	


	drop table #PeriodoActual 
	drop table #PeriodoAnterior 


end


go 




IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_DASHLET_NOMINA_DIFERENCIAS_TIEMPOEXTRA') AND type IN ( N'P', N'PC' ))
	drop procedure SP_DASHLET_NOMINA_DIFERENCIAS_TIEMPOEXTRA
go


create procedure SP_DASHLET_NOMINA_DIFERENCIAS_TIEMPOEXTRA ( @PE_TIPO NominaTipo, 
	 @PE_YEAR Anio,
	 @PE_NUMERO NominaNumero, 
	 @CM_NIVEL0 Formula ) 
as
begin 
		
	declare @Ant_PE_YEAR Anio
	declare @AnT_PE_NUMERO NominaNumero
	 
	exec SP_GET_PERIODO_ANTERIOR @PE_TIPO, @PE_YEAR, @PE_NUMERO, @Ant_PE_YEAR output, @Ant_PE_NUMERO output 
		
	select MO.PE_NUMERO, CONCEPTO.CO_NUMERO, CONCEPTO.CO_DESCRIP, SUM(MO_PERCEPC+MO_DEDUCCI) MONTO into #PeriodoActual from MOVIMIEN MO
	join CONCEPTO on MO.CO_NUMERO = CONCEPTO.CO_NUMERO
	join NOMINA NO on NO.PE_TIPO = MO.PE_TIPO and NO.PE_YEAR = MO.PE_YEAR and NO.PE_NUMERO = MO.PE_NUMERO and MO.CB_CODIGO = NO.CB_CODIGO  
	where 
		CONCEPTO.CO_TIPO in (1,4)	and ( CONCEPTO.CO_SAT_TPP = 'P019' or  CONCEPTO.CO_SAT_TPN = 'P019'  ) and
		MO.PE_NUMERO = @PE_NUMERO and MO.PE_TIPO = @PE_TIPO and MO.PE_YEAR = @PE_YEAR and CONCEPTO.CO_TIPO in (1,4)
		and (@CM_NIVEL0 = ''  or ( NO.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and  NO.CB_NIVEL0=''))
	group BY MO.PE_NUMERO, CONCEPTO.CO_NUMERO,CONCEPTO.CO_DESCRIP 
	order by MONTO desc, CONCEPTO.CO_NUMERO

	select MO.PE_NUMERO, CONCEPTO.CO_NUMERO, CONCEPTO.CO_DESCRIP, SUM(MO_PERCEPC+MO_DEDUCCI) MONTO into #PeriodoAnterior from MOVIMIEN MO
	join CONCEPTO on MO.CO_NUMERO = CONCEPTO.CO_NUMERO
	join NOMINA NO on NO.PE_TIPO = MO.PE_TIPO and NO.PE_YEAR = MO.PE_YEAR and NO.PE_NUMERO = MO.PE_NUMERO and MO.CB_CODIGO = NO.CB_CODIGO  
	where  
		CONCEPTO.CO_TIPO in (1,4)	and ( CONCEPTO.CO_SAT_TPP = 'P019' or  CONCEPTO.CO_SAT_TPN = 'P019'  ) and
		MO.PE_NUMERO = @Ant_PE_NUMERO and MO.PE_TIPO = @PE_TIPO and MO.PE_YEAR = @Ant_PE_YEAR and CONCEPTO.CO_TIPO in (1,4)
		and (@CM_NIVEL0 = ''  or ( NO.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and  NO.CB_NIVEL0=''))
	group BY MO.PE_NUMERO, CONCEPTO.CO_NUMERO,CONCEPTO.CO_DESCRIP 
	order by MONTO desc, CONCEPTO.CO_NUMERO
	
	declare @Query nvarchar(1000) 
	
	set @Query = 
	'select ID=CO_NUMERO, Descripcion=''(''+ cast(CO_NUMERO as varchar(10))  + '') '' + LEFT( dbo.fnConvert_TitleCase(CO_DESCRIP), 20 ), 	
	cast( MontoAnterior as Decimal(15,2)) ['+  dbo.GET_PERIODO_NOMBRE(@PE_TIPO, @Ant_PE_NUMERO ) +'], 
	cast( Monto as Decimal(15,2))  ['+ dbo.GET_PERIODO_NOMBRE(@PE_TIPO, @PE_NUMERO ) +'] 	
		 from (
		select Act.CO_NUMERO, Act.CO_DESCRIP, Act.MONTO, COALESCE(Ant.Monto,0) MontoAnterior , Act.Monto - COALESCE(Ant.Monto,0) Diferencia, ABS( Act.Monto - COALESCE(Ant.Monto,0) ) DiferenciaAbs from #PeriodoActual Act
		left outer join #PeriodoAnterior Ant on Act.CO_NUMERO = Ant.CO_NUMERO
		) Diferencias
	order by DiferenciaAbs desc'
	
	EXEC sp_executesql @Query
	
	drop table #PeriodoActual 
	drop table #PeriodoAnterior 


end

GO 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_DASHLET_NOMINA_SUPERVISORES_TIEMPOEXTRA') AND type IN ( N'P', N'PC' ))
	drop procedure SP_DASHLET_NOMINA_SUPERVISORES_TIEMPOEXTRA
go


create procedure SP_DASHLET_NOMINA_SUPERVISORES_TIEMPOEXTRA ( @PE_TIPO NominaTipo, 
	 @PE_YEAR Anio,
	 @PE_NUMERO NominaNumero, 
	 @CM_NIVEL0 Formula ) 
as
begin 
		
	declare @Ant_PE_YEAR Anio
	declare @AnT_PE_NUMERO NominaNumero
	
	declare @iNivelSupervisor status  
	declare @CampoSupervisor varchar(20) 
	declare @TablaSupervisor varchar(20) 
	declare @NombreNivel     varchar(20) 
		
	set @iNivelSupervisor = dbo.SP_GET_NIVEL_SUPERVISORES()

	if ( @iNivelSupervisor = 0 ) 
		set @iNivelSupervisor = 1; 

	set @CampoSupervisor = 'CB_NIVEL'+cast( @iNivelSupervisor as varchar(2) ) 
	set @TablaSupervisor = 'NIVEL'+cast( @iNivelSupervisor as varchar(2) ) 

	if (@iNivelSupervisor >=1 ) and ( @iNivelSupervisor <= 9 ) 
		select @NombreNivel = dbo.fnConvert_TitleCase(GL_FORMULA) from global where GL_CODIGO = 12 + @iNivelSupervisor
	else
		set @NombreNivel = 'Supervisor' 
	
	set @NombreNivel = REPLACE( REPLACE( @NombreNivel, '[', '') , ']' , '' ) 
	declare @Query nvarchar(1000) 
	declare @Params nvarchar(1000) 
	
	set @Query = 		
	'select  REPLACE(Supervisor,'','', '''') as ['+@NombreNivel+'], CAST( MontoTotal as Decimal(15,2)) as  [Monto] from ( 
	select SUM(MO_PERCEPC+MO_DEDUCCI) as MontoTotal, RTRIM(Nivel.TB_CODIGO) + '' - ''+ dbo.fnConvert_TitleCase(Nivel.TB_ELEMENT) as Supervisor   from MOVIMIEN MO
	join CONCEPTO on MO.CO_NUMERO = CONCEPTO.CO_NUMERO
	join NOMINA NO on NO.PE_TIPO = MO.PE_TIPO and NO.PE_YEAR = MO.PE_YEAR and NO.PE_NUMERO = MO.PE_NUMERO and MO.CB_CODIGO = NO.CB_CODIGO  
	join '+@TablaSupervisor+' Nivel on NO.'+@CampoSupervisor+' = Nivel.TB_CODIGO 
	where  
		CONCEPTO.CO_TIPO in (1,4)	and ( CONCEPTO.CO_SAT_TPP = ''P019'' or  CONCEPTO.CO_SAT_TPN = ''P019''  ) and
		MO.PE_NUMERO = @PE_NUMERO and MO.PE_TIPO = @PE_TIPO and MO.PE_YEAR = @PE_YEAR and CONCEPTO.CO_TIPO in (1,4)
		and (@CM_NIVEL0 = ''''  or ( NO.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, '','') )   )  or (@CM_NIVEL0 = '''' and  NO.CB_NIVEL0=''''))
	group BY Nivel.TB_ELEMENT, Nivel.TB_CODIGO ) Montos 
	order by MontoTotal desc'

	
	SET @Params	= N'@PE_TIPO int,  @PE_YEAR int, @PE_NUMERO int, @CM_NIVEL0 varchar(256) '
	EXEC sp_executesql @Query, @Params,  @PE_TIPO,  @PE_YEAR, @PE_NUMERO, @CM_NIVEL0	
	
end 

go 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_DASHLET_NOMINA_EMPLEADOS_DIFERENCIAS') AND type IN ( N'P', N'PC' ))
	drop procedure SP_DASHLET_NOMINA_EMPLEADOS_DIFERENCIAS
go


create procedure SP_DASHLET_NOMINA_EMPLEADOS_DIFERENCIAS ( @PE_TIPO NominaTipo, 
	 @PE_YEAR Anio,
	 @PE_NUMERO NominaNumero, 
	 @CM_NIVEL0 Formula ) 
as
begin 

declare @Query nvarchar(2000) 
declare @Params nvarchar(1000) 
declare @Ant_PE_YEAR Anio
declare @AnT_PE_NUMERO NominaNumero
	 
exec SP_GET_PERIODO_ANTERIOR @PE_TIPO, @PE_YEAR, @PE_NUMERO, @Ant_PE_YEAR output, @Ant_PE_NUMERO output 

set @Query = 
';with NominaActual
as
(select  NO_NETO  , CB.CB_CODIGO, CB.PRETTYNAME
from NOMINA NO 
join COLABORA CB on CB.CB_CODIGO = NO.CB_CODIGO 
where PE_YEAR = @Ant_PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO = @Ant_PE_NUMERO 
	  and (@CM_NIVEL0 = ''''  or ( NO.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, '','') )   )  or (@CM_NIVEL0 = '''' and  NO.CB_NIVEL0=''''))
)
select TOP 20 CAST( (NominaActual.NO_NETO - NominaAnterior.NO_NETO ) as Decimal(15,2) ) as Diferencia,  NominaAnterior.CB_CODIGO as [N�mero], dbo.fnConvert_TitleCase( NominaAnterior.PRETTYNAME ) as [Empleado] from ( 
select  NO_NETO, CB.CB_CODIGO, CB.PRETTYNAME  
from NOMINA NO 
join COLABORA CB on CB.CB_CODIGO = NO.CB_CODIGO 
where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO = @PE_NUMERO
and (@CM_NIVEL0 = ''''  or ( NO.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, '','') )   )  or (@CM_NIVEL0 = '''' and  NO.CB_NIVEL0=''''))
) NominaAnterior 
join NominaActual  on NominaActual.CB_CODIGO = NominaAnterior.CB_CODIGO 
order by ABS(NominaActual.NO_NETO - NominaAnterior.NO_NETO ) desc'
 

	SET @Params	= N'@PE_TIPO int,  @PE_YEAR int, @PE_NUMERO int, @Ant_PE_YEAR int, @Ant_PE_NUMERO int, @CM_NIVEL0 varchar(256) '
	EXEC sp_executesql @Query, @Params,  @PE_TIPO,  @PE_YEAR, @PE_NUMERO,@Ant_PE_YEAR,@AnT_PE_NUMERO,  @CM_NIVEL0

	
end 


go 


 
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_INDICADOR_ASISTENCIA') AND type IN ( N'P', N'PC' ))
	drop procedure SP_INDICADOR_ASISTENCIA
go


create procedure SP_INDICADOR_ASISTENCIA ( 
	 @PE_TIPO NominaTipo, 
	 @PE_YEAR Anio,
	 @PE_NUMERO NominaNumero, 	 
	 @PE_MES	Status, 
	 @FechaActiva Fecha,
	 @CM_NIVEL0 Formula, 
	 @iCual status
 ) 
as
begin 
	
	declare @Ant_PE_YEAR Anio
	declare @Ant_PE_NUMERO NominaNumero 

	declare @Valor Pesos 
	declare @ValorAnterior Pesos

	exec SP_GET_PERIODO_ANTERIOR @PE_TIPO, @PE_YEAR, @PE_NUMERO, @Ant_PE_YEAR output, @Ant_PE_NUMERO output 

	set @Valor = 0 
	set @ValorAnterior = 0
	
	-- Empleados Checaron  
	if ( @iCual = 1 ) 
	begin 
		select @Valor=count(*) from ( 
		select CHECADAS.CB_CODIGO, count(*)*1.0 Cuantas from CHECADAS 
		join COLABORA CB on CHECADAS.CB_CODIGO = CB.CB_CODIGO 
		where AU_FECHA = @FechaActiva 
			and (@CM_NIVEL0 = ''  or ( CB.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB.CB_NIVEL0=''))
		group by CHECADAS.CB_CODIGO 
		) EmpleadosChecaron  						

		set @ValorAnterior = @valor 
	end 
	else
	-- Tiempo extra autorizado 
	if ( @iCual = 2 ) 
	begin 		
		select @Valor = SUM(AU_EXTRAS) from AUSENCIA AU
		join COLABORA CB on AU.CB_CODIGO = CB.CB_CODIGO 
		join PERIODO PE on AU.PE_YEAR = PE.PE_YEAR and AU.PE_TIPO = PE.PE_TIPO and AU.PE_NUMERO = PE.PE_NUMERO 
		where PE.PE_YEAR = @PE_YEAR and PE.PE_TIPO = @PE_TIPO and PE.PE_NUMERO = @PE_NUMERO  
 		and (@CM_NIVEL0 = ''  or ( CB.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB.CB_NIVEL0=''))

		select @ValorAnterior = SUM(AU_EXTRAS) from AUSENCIA AU
		join COLABORA CB on AU.CB_CODIGO = CB.CB_CODIGO 
		join PERIODO PE on AU.PE_YEAR = PE.PE_YEAR and AU.PE_TIPO = PE.PE_TIPO and AU.PE_NUMERO = PE.PE_NUMERO 
		where PE.PE_YEAR = @Ant_PE_YEAR and PE.PE_TIPO = @PE_TIPO and PE.PE_NUMERO = @Ant_PE_NUMERO  
 		and (@CM_NIVEL0 = ''  or ( CB.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB.CB_NIVEL0=''))

	end 
	else
	-- Tiempo extra por autorizar 
	if ( @iCual = 3 ) 
	begin 	
		select @Valor = 
		 SUM ( case 
				when AU_NUM_EXT  >= AU_EXTRAS then AU_NUM_EXT - AU_EXTRAS 
				else  0 
			end 
			)		
		from AUSENCIA AU
		join COLABORA CB on AU.CB_CODIGO = CB.CB_CODIGO 
		join PERIODO PE on AU.PE_YEAR = PE.PE_YEAR and AU.PE_TIPO = PE.PE_TIPO and AU.PE_NUMERO = PE.PE_NUMERO 
		where PE.PE_YEAR = @PE_YEAR and PE.PE_TIPO = @PE_TIPO and PE.PE_NUMERO = @PE_NUMERO  
 		and (@CM_NIVEL0 = ''  or ( CB.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB.CB_NIVEL0=''))

		select @ValorAnterior = 
			SUM ( case 
					when  AU_NUM_EXT >= AU_EXTRAS then AU_NUM_EXT - AU_EXTRAS
					else  0 
				end 
			  )		
		from AUSENCIA AU
		join COLABORA CB on AU.CB_CODIGO = CB.CB_CODIGO 
		join PERIODO PE on AU.PE_YEAR = PE.PE_YEAR and AU.PE_TIPO = PE.PE_TIPO and AU.PE_NUMERO = PE.PE_NUMERO 
		where PE.PE_YEAR = @Ant_PE_YEAR and PE.PE_TIPO = @PE_TIPO and PE.PE_NUMERO = @Ant_PE_NUMERO  
 		and (@CM_NIVEL0 = ''  or ( CB.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB.CB_NIVEL0=''))

	end 


	exec SP_INDICADOR_SHOW @Valor, @ValorAnterior 

end

go 



if exists (select * from dbo.sysobjects where id = object_id(N'FN_MASK_PORC') and xtype in (N'FN', N'IF', N'TF')) 
	drop function FN_MASK_PORC
go 

create function FN_MASK_PORC( @valor Decimal(15,2) ) 
returns Varchar(10) 
as
begin 
	
	declare @valorStr varchar(10) 
	set @valorStr = '' 
	set @valorStr = convert( varchar, @valor, 1 ) 
	if ( @valor < 100.00 ) 
		set @valorStr= RIGHT('00000'+@valorStr, 5) 
	
	set @valorStr = @valorStr + '%' 

	return @valorStr 
end

GO 

-- Incidencias por Supervisor 
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_DASHLET_ASISTENCIA_SUPERVISORES_INCIDENCIAS') AND type IN ( N'P', N'PC' ))
       drop procedure SP_DASHLET_ASISTENCIA_SUPERVISORES_INCIDENCIAS
go


create procedure SP_DASHLET_ASISTENCIA_SUPERVISORES_INCIDENCIAS ( @FechaActiva Fecha,  @CM_NIVEL0 Formula ) 
as
begin 
             
       declare @Ant_PE_YEAR Anio
       declare @AnT_PE_NUMERO NominaNumero
       
       declare @iNivelSupervisor status  
       declare @CampoSupervisor varchar(20) 
       declare @TablaSupervisor varchar(20) 
       declare @NombreNivel     varchar(20) 
             
       set @iNivelSupervisor = dbo.SP_GET_NIVEL_SUPERVISORES()

       if ( @iNivelSupervisor = 0 ) 
             set @iNivelSupervisor = 1; 

       set @CampoSupervisor = 'CB_NIVEL'+cast( @iNivelSupervisor as varchar(2) ) 
       set @TablaSupervisor = 'NIVEL'+cast( @iNivelSupervisor as varchar(2) ) 

       if (@iNivelSupervisor >=1 ) and ( @iNivelSupervisor <= 9 ) 
             select @NombreNivel = dbo.fnConvert_TitleCase(GL_FORMULA) from global where GL_CODIGO = 12 + @iNivelSupervisor
       else
             set @NombreNivel = 'Supervisor' 
       
       set @NombreNivel = REPLACE( REPLACE( @NombreNivel, '[', '') , ']' , '' ) 
       declare @Query nvarchar(2500) 
       declare @Params nvarchar(1000) 
       
       set @Query =        
'
with Empleados as 
( 
select CB_CODIGO, dbo.SP_KARDEX_'+@CampoSupervisor+'(@FechaActiva, CB_CODIGO) as '+@CampoSupervisor+', CB_NIVEL0 from COLABORA CB
where dbo.SP_STATUS_ACT(@FechaActiva, CB_CODIGO )  > 0  
and (@CM_NIVEL0 = ''''  or ( CB.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, '','') )   )  or (@CM_NIVEL0 = '''' and  CB.CB_NIVEL0=''''))
)

select  '+@CampoSupervisor+', count(*) Activos into #ActivosFecha from Empleados CB
group by '+@CampoSupervisor+'; 

select AU.'+@CampoSupervisor+', count(*) Calculados into #CalculadosFecha from AUSENCIA AU
join #ActivosFecha on AU.'+@CampoSupervisor+' = #ActivosFecha.'+@CampoSupervisor+'
join COLABORA CB on AU.CB_CODIGO = CB.CB_CODIGO 
where  AU.AU_FECHA = @FechaActiva 
and (@CM_NIVEL0 = ''''  or ( CB.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, '','') )   )  or (@CM_NIVEL0 = '''' and  CB.CB_NIVEL0=''''))
group by AU.'+@CampoSupervisor+'; 



;with Incidentes as 
( 
select AU.'+@CampoSupervisor+' , count(*) as Incidencias    from AUSENCIA AU
       join COLABORA CB on AU.CB_CODIGO = CB.CB_CODIGO 
       where  AU_TIPO <> ''''  and AU_TIPO not in (''RE'' ) and AU_FECHA = @FechaActiva
                    and (@CM_NIVEL0 = ''''  or ( CB.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, '','') )   )  or (@CM_NIVEL0 = '''' and  CB.CB_NIVEL0=''''))
       group by AU.'+@CampoSupervisor+' ) 
       
select RTRIM(Nivel.TB_CODIGO)+ '' - ''+ REPLACE(Nivel.TB_ELEMENT, '','', '''' )  ['+@NombreNivel+'], 
             
             case                              
                    when Calculados > 0  then  dbo.FN_MASK_PORC( ((Calculados-coalesce(Incidencias,0.00)*1.0)/( Calculados*1.0)) *100.00   )    
                    else ''S/D''
             end [% Asistencia], 
             coalesce(Calculados, 0) as [Empleados asignados], coalesce(Incidencias,0) as Ausentismos
from #ActivosFecha 
 left outer join #CalculadosFecha on #ActivosFecha.'+@CampoSupervisor+' =  #CalculadosFecha.'+@CampoSupervisor+'   
 left outer join Incidentes  on #ActivosFecha.'+@CampoSupervisor+' = Incidentes.'+@CampoSupervisor+'  
join '+@TablaSupervisor+' Nivel on #ActivosFecha.'+@CampoSupervisor+' = Nivel.TB_CODIGO 
where Activos > 0  and Nivel.TB_ACTIVO = ''S''
order by ( case                                
                    when Calculados > 0  then ((Calculados-coalesce(Incidencias,0.00)*1.0)/( Calculados*1.0)) *100.00   
                    else 10000
             end ), [Empleados asignados] desc  ; 
drop table #ActivosFecha; 
drop table #CalculadosFecha '; 

         
             print @Query      
       SET @Params  = N'@FechaActiva Datetime, @CM_NIVEL0 varchar(256) '
       EXEC sp_executesql @Query, @Params, @FechaActiva, @CM_NIVEL0 
       
end 

go 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_DASHLET_INCIDENCIAS_PRENOMINA') AND type IN ( N'P', N'PC' ))
	drop procedure SP_DASHLET_INCIDENCIAS_PRENOMINA

GO

create procedure SP_DASHLET_INCIDENCIAS_PRENOMINA ( @Fecha Fecha, @CM_NIVEL0 Formula ) 
as
begin 

select row_number() over (order by SUM(Empleados) desc ) as ID , Tipo as Descripcion, SUM(Empleados) Cantidad from ( 
select 
case 
   when  AU_TIPO = ''  then 'Asistencia' 
   when  TB_INCIDEN =0 then  'Baja/Ingreso' 
   when  TB_INCIDEN =1 and AU_TIPO = 'FI' then 'Falta Injustificada' 
   when  TB_INCIDEN =1 and AU_TIPO <> 'FI' then 'Falta' 
   when  TB_INCIDEN =2 then 'Incapacidad'
   when  TB_INCIDEN =3 or AU_TIPO = 'VAC' then 'Vacaciones'
   when  TB_INCIDEN =4 then   INC.TB_ELEMENT     
   when  TB_INCIDEN =5 then 'Retardo'
   when  TB_INCIDEN is NULL then AU_TIPO 
   else 'Otros' 
  end Tipo,  count(*) Empleados from AUSENCIA 
  join COLABORA on AUSENCIA.CB_CODIGO = COLABORA.CB_CODIGO 
left outer join INCIDEN INC on INC.TB_CODIGO = AU_TIPO 
where  AU_FECHA =  @Fecha  and  AU_TIPO not in ('FSS', 'BAJ', 'IGR' ) 
and COLABORA.CB_ACTIVO = 'S'  and (@CM_NIVEL0 = ''  or ( COLABORA.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and COLABORA.CB_NIVEL0='')) 
group by INC.TB_INCIDEN, INC.TB_PERMISO, AU_TIPO, INC.TB_ELEMENT    
) ResumenPrenomina 
group by Tipo
order by  Cantidad desc 

end 
GO 



go 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_DASHLET_ASISTENCIA_SUPERVISORES_TIEMPOEXTRA') AND type IN ( N'P', N'PC' ))
	drop procedure SP_DASHLET_ASISTENCIA_SUPERVISORES_TIEMPOEXTRA
go

create procedure SP_DASHLET_ASISTENCIA_SUPERVISORES_TIEMPOEXTRA ( @PE_TIPO int, @PE_YEAR int, @PE_NUMERO int,  @CM_NIVEL0 Formula ) 
as
begin 
		
	declare @Ant_PE_YEAR Anio
	declare @AnT_PE_NUMERO NominaNumero
	
	declare @iNivelSupervisor status  
	declare @CampoSupervisor varchar(20) 
	declare @TablaSupervisor varchar(20) 
	declare @NombreNivel     varchar(20) 
		
	set @iNivelSupervisor = dbo.SP_GET_NIVEL_SUPERVISORES()

	if ( @iNivelSupervisor = 0 ) 
		set @iNivelSupervisor = 1; 

	set @CampoSupervisor = 'CB_NIVEL'+cast( @iNivelSupervisor as varchar(2) ) 
	set @TablaSupervisor = 'NIVEL'+cast( @iNivelSupervisor as varchar(2) ) 

	if (@iNivelSupervisor >=1 ) and ( @iNivelSupervisor <= 9 ) 
		select @NombreNivel = dbo.fnConvert_TitleCase(GL_FORMULA) from global where GL_CODIGO = 12 + @iNivelSupervisor
	else
		set @NombreNivel = 'Supervisor' 
	
	set @NombreNivel = REPLACE( REPLACE( @NombreNivel, '[', '') , ']' , '' ) 
	declare @Query nvarchar(2000) 
	declare @Params nvarchar(1000) 
	

	set @Query = 		
'	select  	 
	 RTRIM( Nivel.TB_CODIGO )  + '' - ''+ REPLACE(NIVEL.TB_ELEMENT, '','', '''' )  ['+@NombreNivel+'] , 
SUM(AU_EXTRAS) [Extras autorizadas], 
	 SUM ( case 
				when AU_NUM_EXT >= AU_EXTRAS then AU_NUM_EXT - AU_EXTRAS				
				else  0 
			end 
		)  [Por Autorizar]

	    from AUSENCIA AU
		join COLABORA CB on AU.CB_CODIGO = CB.CB_CODIGO 
		join PERIODO PE on AU.PE_YEAR = PE.PE_YEAR and AU.PE_TIPO = PE.PE_TIPO and AU.PE_NUMERO = PE.PE_NUMERO 
		join  '+@TablaSupervisor+'  Nivel on AU.'+@CampoSupervisor+' = Nivel.TB_CODIGO
	where  
		PE.PE_YEAR = @PE_YEAR and PE.PE_TIPO = @PE_TIPO and PE.PE_NUMERO = @PE_NUMERO  and Nivel.TB_ACTIVO = ''S''
 		and (@CM_NIVEL0 = ''''  or ( CB.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, '','') )   )  or (@CM_NIVEL0 = '''' and CB.CB_NIVEL0=''''))
	group by Nivel.TB_CODIGO, Nivel.TB_ELEMENT
	order by [Extras autorizadas] desc' 
	

	SET @Params	= N'@PE_YEAR int, @PE_TIPO int, @PE_NUMERO int, @CM_NIVEL0 varchar(256) '
	EXEC sp_executesql @Query, @Params, @PE_YEAR , @PE_TIPO , @PE_NUMERO, @CM_NIVEL0	
	

	
end 

go 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_DASHLET_TIEMPOEXTRA_PRENOMINA') AND type IN ( N'P', N'PC' ))
	drop procedure SP_DASHLET_TIEMPOEXTRA_PRENOMINA
go

create procedure SP_DASHLET_TIEMPOEXTRA_PRENOMINA(  @PE_TIPO int, @PE_YEAR int, @PE_NUMERO int,  @CM_NIVEL0 Formula ) 
as
begin 

	declare @Ant_PE_YEAR Anio
	declare @Ant_PE_NUMERO NominaNumero 

	declare @Dobles Pesos 
	declare @Triples Pesos
	declare @DT Pesos

	declare @Ant_Dobles Pesos 
	declare @Ant_Triples Pesos
	declare @Ant_DT Pesos

	exec SP_GET_PERIODO_ANTERIOR @PE_TIPO, @PE_YEAR, @PE_NUMERO, @Ant_PE_YEAR output, @Ant_PE_NUMERO output 

	select @Dobles = SUM(AU_DOBLES),  @Triples = SUM(AU_TRIPLES),  @DT = SUM(AU_DES_TRA)
	from AUSENCIA AU 
	join COLABORA CB on AU.CB_CODIGO = CB.CB_CODIGO 
	join PERIODO PE on AU.PE_YEAR = PE.PE_YEAR and AU.PE_TIPO = PE.PE_TIPO and AU.PE_NUMERO = PE.PE_NUMERO 
	where  
	PE.PE_YEAR = @PE_YEAR and PE.PE_TIPO = @PE_TIPO and PE.PE_NUMERO = @PE_NUMERO  
	and (@CM_NIVEL0 = ''  or ( CB.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB.CB_NIVEL0=''))


	select @Ant_Dobles = SUM(AU_DOBLES),  @Ant_Triples = SUM(AU_TRIPLES),  @Ant_DT = SUM(AU_DES_TRA)
	from AUSENCIA AU 
	join COLABORA CB on AU.CB_CODIGO = CB.CB_CODIGO 
	join PERIODO PE on AU.PE_YEAR = PE.PE_YEAR and AU.PE_TIPO = PE.PE_TIPO and AU.PE_NUMERO = PE.PE_NUMERO 
	where  
	PE.PE_YEAR = @Ant_PE_YEAR and PE.PE_TIPO = @PE_TIPO and PE.PE_NUMERO = @Ant_PE_NUMERO  
	and (@CM_NIVEL0 = ''  or ( CB.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB.CB_NIVEL0=''))


	declare @Query nvarchar(1000) 
	declare @Params nvarchar(500) 
	set @Params  = '@Ant_Dobles Decimal(15,2), @Ant_Triples Decimal(15,2),  @Ant_DT Decimal(15,2), @Dobles Decimal(15,2), @Triples Decimal(15,2),  @DT Decimal(15,2)' 

	set @Query = 
	'
	select ID=1, Descripcion=''Horas extras dobles''  , ['+  dbo.GET_PERIODO_NOMBRE(@PE_TIPO, @Ant_PE_NUMERO ) +']=@Ant_Dobles, ['+  dbo.GET_PERIODO_NOMBRE(@PE_TIPO, @PE_NUMERO ) +']=@Dobles 
	union all 
	select ID=2, Descripcion=''Horas extras triples''  , ['+  dbo.GET_PERIODO_NOMBRE(@PE_TIPO, @Ant_PE_NUMERO ) +']=@Ant_Triples, ['+  dbo.GET_PERIODO_NOMBRE(@PE_TIPO, @PE_NUMERO ) +']=@Triples 
	union all 
	select ID=3, Descripcion=''Descanso trabajado''  , ['+  dbo.GET_PERIODO_NOMBRE(@PE_TIPO, @Ant_PE_NUMERO ) +']=@Ant_DT, ['+  dbo.GET_PERIODO_NOMBRE(@PE_TIPO, @PE_NUMERO ) +']=@DT
	'
	EXEC sp_executesql @Query, @Params, @Ant_Dobles , @Ant_Triples,  @Ant_DT, @Dobles , @Triples ,  @DT
	

end 

go 
