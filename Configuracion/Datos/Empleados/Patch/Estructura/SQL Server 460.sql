/****************************************/
/****************************************/
/****** Patch para DATOS ****************/
/****************************************/
/****************************************/

/* Patch 460: #15378: Suscripci�n de reportes - Edici�n de usuarios (1/4) */
IF EXISTS(SELECT * FROM sys.objects WHERE object_id = object_id('V_SUSCRIP') )
	DROP VIEW V_SUSCRIP
GO

/* Patch 460: #15378: Suscripci�n de reportes - Edici�n de usuarios (2/4) */
CREATE view V_SUSCRIP  as SELECT CM_CODIGO, RE_CODIGO=CA_REPORT, US_CODIGO from  #COMPARTE..V_SUSCRIP
group by CM_CODIGO, CA_REPORT, US_CODIGO 
GO

/* Patch 460: #15378: Suscripci�n de reportes - Edici�n de usuarios (3/4) */
IF EXISTS(SELECT * FROM sys.objects WHERE object_id = object_id('V_SUSC_CAL') )
	DROP VIEW V_SUSC_CAL
GO

/* Patch 460: #15378: Suscripci�n de reportes - Edici�n de usuarios (4/4) */
create view V_SUSC_CAL  as SELECT 
CA_FOLIO, CA_NOMBRE, CA_DESCRIP, CA_ACTIVO, CM_CODIGO,  CA_FREC, CA_HORA, CA_ULT_FEC, CA_REPORT, VS_TIPO, RO_CODIGO, RO_NOMBRE, US_CODIGO, US_NOMBRE, US_EMAIL 
 from  #COMPARTE..V_SUSCRIP
GO

/* Patch 460: #15803: Vista a calendario de reportes (1/2) */
IF EXISTS(SELECT * FROM sys.objects WHERE object_id = object_id('V_CALENDAR') )
    DROP VIEW V_CALENDAR
GO

/* Patch 460: #15803: Vista a calendario de reportes (2/2) */
CREATE view V_CALENDAR  as 
    SELECT CA_FOLIO,CA_NOMBRE,CA_DESCRIP,CA_ACTIVO,CM_CODIGO,CA_REPORT,CA_REPEMP,CA_FECHA,CA_HORA,CA_FREC,
        CA_RECUR,CA_DOWS,CA_MESES,CA_MESDIAS,CA_MES_ON,CA_MESWEEK,CA_FEC_ESP,CA_TSALIDA,CA_FSALIDA,CA_ULT_FEC,CA_NX_FEC,
        CA_NX_EVA,CA_US_CHG,CA_CAPTURA  
    FROM #COMPARTE..CALENDARIO
GO

/* Patch 460: Retro cursos*/
if exists (select * from sys.objects where object_id = object_id('V_CUR_PROG') )
	drop view V_CUR_PROG
GO

create view V_CUR_PROG (
  CB_CODIGO,
  CB_PUESTO,
  CB_NIVEL0,
  CU_CODIGO,
  CU_NOMBRE,
  KC_FEC_PRO,
  TOMADO,
  KC_FEC_TOM,
  CU_HORAS,
  EN_OPCIONA,
  EN_LISTA,
  CU_STPS,
  MA_CODIGO,
  CU_REVISIO,
  CP_MANUAL,
  EP_GLOBAL,
  CU_CLASIFI,
  CU_CLASE
) AS
select
  C.CB_CODIGO,
  C.CB_PUESTO,
  C.CB_NIVEL0,
  CU.CU_CODIGO,
  CU.CU_NOMBRE,
  ( select case when ( K.KC_FEC_TOM is NULL ) then DATEADD( DAY, E.EN_DIAS, C.CB_FEC_PTO )
                when ( K.KC_FEC_TOM is not NULL ) and ( ( E.EN_RE_DIAS = 0 AND E.EN_REPROG = 'N' ) or ( ( E.EN_RE_DIAS = 0 AND E.EN_REPROG = 'S' ) and ( K.KC_REVISIO = CU.CU_REVISIO ) ) ) then K.KC_FECPROG
				when ( K.KC_FEC_TOM is not NULL ) and ( ( E.EN_RE_DIAS > 0 AND E.EN_REPROG = 'N' ) or ( ( E.EN_RE_DIAS > 0 AND E.EN_REPROG = 'S' ) and ( K.KC_REVISIO = CU.CU_REVISIO ) ) ) then DATEADD( DAY, E.EN_RE_DIAS, K.KC_FEC_TOM )
				when ( K.KC_FEC_TOM is not NULL ) and ( ( E.EN_RE_DIAS = 0 AND E.EN_REPROG = 'S' ) and ( ( K.KC_FEC_TOM < CU.CU_FEC_REV ) ) and (K.KC_REVISIO <> CU.CU_REVISIO )) then CU.CU_FEC_REV
				else CU.CU_FEC_REV end ) KC_FEC_PRO,
  ( case when ( K.KC_FEC_TOM is NOT NULL ) then 1 else 0 end ) TOMADO,
  ( select case when ( E.EN_RE_DIAS = 0 AND E.EN_REPROG = 'N' ) then K.KC_FEC_TOM
				when ( ( E.EN_RE_DIAS > 0 AND E.EN_REPROG = 'N' ) OR ( ( E.EN_RE_DIAS > 0 AND E.EN_REPROG = 'S' )and ( K.KC_REVISIO <> CU.CU_REVISIO ) ) ) then null
				when ( ( E.EN_RE_DIAS = 0 AND E.EN_REPROG = 'S' ) and ( K.KC_REVISIO <> CU.CU_REVISIO ) ) then null
				else K.KC_FEC_TOM End ) KC_FEC_TOM,
  CU.CU_HORAS,
  E.EN_OPCIONA,
  E.EN_LISTA,
  CU.CU_STPS,
  CU.MA_CODIGO,
  CU.CU_REVISIO,
  cast( 'N' as CHAR(1) ) CP_MANUAL,
  cast( 'N' as CHAR(1) ) EP_GLOBAL,
  CU.CU_CLASIFI,
  CU.CU_CLASE
  from COLABORA C
  join ENTRENA E on ( E.PU_CODIGO = C.CB_PUESTO )
  left join KARCURSO K on ( K.CB_CODIGO = C.CB_CODIGO ) and ( K.CU_CODIGO = E.CU_CODIGO ) and ( K.KC_FEC_TOM = ( select MAX( KC_FEC_TOM ) from KARCURSO AS K1 where ( K1.CB_CODIGO = C.CB_CODIGO ) and ( K1.CU_CODIGO = E.CU_CODIGO ) ))
  left join CURSO CU on ( CU.CU_CODIGO = E.CU_CODIGO )
  where ( CU.CU_ACTIVO = 'S' )

UNION ALL

select
  C.CB_CODIGO,
  C.CB_PUESTO,
  C.CB_NIVEL0,
  CURSO.CU_CODIGO,
  CURSO.CU_NOMBRE,
  (select case when (EP.EP_PORDIAS = 'S' ) then C.CB_FEC_PTO + EP.EP_DIAS else EP.EP_FECHA end) KC_FEC_PRO,
  ( case when ( K.KC_FEC_TOM is NOT NULL ) then 1 else 0 end ) TOMADO,
  K.KC_FEC_TOM,
  CURSO.CU_HORAS,
  EP.EP_OPCIONA,
  cast( 'N' as CHAR(1) ) EN_LISTA,
  CURSO.CU_STPS,
  CURSO.MA_CODIGO,
  CURSO.CU_REVISIO,
  cast( 'S' as CHAR(1) ) CP_MANUAL,
  EP.EP_GLOBAL,
  CURSO.CU_CLASIFI,
  CURSO.CU_CLASE
  from COLABORA C
  join EMP_PROG EP on ( EP.CB_CODIGO = C.CB_CODIGO )
  left join CURSO on ( CURSO.CU_CODIGO = EP.CU_CODIGO )
  left join KARCURSO K on ( K.CU_CODIGO = EP.CU_CODIGO and EP.CB_CODIGO = K.CB_CODIGO and K.KC_FEC_TOM >= (select case when (EP.EP_PORDIAS = 'S' ) then C.CB_FEC_PTO + EP.EP_DIAS else EP.EP_FECHA end	) )
where ( CURSO.CU_ACTIVO = 'S' )

GO

/* Patch 460: #15852:  (1/4) Tablero de Mi Empresa */
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_DASHLET_GENERAL_CUMPLEANOS') AND type IN ( N'P', N'PC' ))
	drop procedure SP_DASHLET_GENERAL_CUMPLEANOS

GO

create procedure SP_DASHLET_GENERAL_CUMPLEANOS ( @CM_NIVEL0 Formula ) 
as
begin 
	declare @Fecha Fecha 
	
	set @Fecha = getdate() 

	declare @MesVar varchar(20) 
	set @MesVar = dbo.SP_DASHLET_GET_MES_DESC( MONTH(@Fecha) ) 

	declare @FecIni Fecha 
	declare @FecFin Fecha 
	select @FecIni = DATEADD(dd, -(DATEPART(dw, @Fecha)-1), @Fecha) , 
		   @FecFin = DATEADD(dd, 7-(DATEPART(dw, @Fecha)), @Fecha) 


	select CB_CODIGO Empleado, dbo.fnConvert_TitleCase(REPLACE(PRETTYNAME, ',', '')) Nombre, dbo.SP_DASHLET_GET_MES_DESC( MONTH(CB_FEC_NAC) ) as Mes,  DAY(CB_FEC_NAC) as [D_ia] from COLABORA 
	where 
		dateadd( year, datediff( year, CB_FEC_NAC, @Fecha+7 ), CB_FEC_NAC)  between @FecIni  and @FecFin and 
		dbo.SP_STATUS_ACT(@Fecha, CB_CODIGO ) = 1   and (@CM_NIVEL0 = ''  or ( COLABORA.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  
		or (@CM_NIVEL0 = '' and COLABORA.CB_NIVEL0=''))	
	order by dateadd( year, datediff( year, CB_FEC_NAC, @Fecha+7 ), CB_FEC_NAC) 

end 

go 

/* Patch 460: #15852:  (2/4) Tablero de Mi Empresa */
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_DASHLET_GENERAL_ANIVERSARIOS') AND type IN ( N'P', N'PC' ))
	drop procedure SP_DASHLET_GENERAL_ANIVERSARIOS

GO
create procedure SP_DASHLET_GENERAL_ANIVERSARIOS ( @CM_NIVEL0 Formula ) 
as
begin 
	declare @Fecha Fecha 
	set @Fecha = getdate() 

	
	
	declare @FecIni Fecha 
	declare @FecFin Fecha 
	select @FecIni = DATEADD(dd, -(DATEPART(dw, @Fecha)-1), @Fecha) , 
		   @FecFin = DATEADD(dd, 7-(DATEPART(dw, @Fecha)), @Fecha) 
	
	
	select CB_CODIGO Empleado, dbo.fnConvert_TitleCase(REPLACE(PRETTYNAME, ',', '')) Nombre, dbo.SP_DASHLET_GET_MES_DESC( MONTH(CB_FEC_ANT) ) as Mes,  DAY(CB_FEC_ANT) as [D_ia], Datediff(year, CB_FEC_ANT, @Fecha) as [Antig__uedad]   from COLABORA 
	where 
		dateadd( year, datediff( year, CB_FEC_ANT, @Fecha+7 ), CB_FEC_ANT)  between @FecIni  and @FecFin and  Datediff(year, CB_FEC_ANT, @Fecha) > 0 and 
		dbo.SP_STATUS_ACT(@Fecha, CB_CODIGO ) = 1   and (@CM_NIVEL0 = ''  or ( COLABORA.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  
		or (@CM_NIVEL0 = '' and COLABORA.CB_NIVEL0=''))	
	order by dateadd( year, datediff( year, CB_FEC_ANT, @Fecha+7 ), CB_FEC_ANT) 
end 

Go 

/* Patch 460: #15852:  (3/4) Tablero de Mi Empresa */
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_DASHLET_GENERAL_DEMOGRAFICO_GENERO') AND type IN ( N'P', N'PC' ))
	drop procedure SP_DASHLET_GENERAL_DEMOGRAFICO_GENERO

GO

create procedure SP_DASHLET_GENERAL_DEMOGRAFICO_GENERO (  @CM_NIVEL0 Formula ) 
as
begin 

declare @Fecha Fecha 

set @Fecha = getdate() 

select row_number() over (order by SUM(Empleados) desc ) as ID , Tipo as Descripcion, SUM(Empleados) Cantidad from ( 
select 
case 
   when  CB_SEXO = 'M'  then 'Masculino' 
   when  CB_SEXO = 'F'  then 'Femenino' 
   else 'No especificado' 
  end Tipo,  count(*) Empleados from COLABORA  
  where dbo.SP_STATUS_ACT(@Fecha, CB_CODIGO ) = 1   and (@CM_NIVEL0 = ''  or ( COLABORA.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  
		or (@CM_NIVEL0 = '' and COLABORA.CB_NIVEL0=''))	
  group by CB_SEXO 
) ResumenGenero
group by Tipo
order by  Descripcion desc 

end 

GO 

/* Patch 460: #15852:  (4/4) Tablero de Mi Empresa */
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_DASHLET_GENERAL_DEMOGRAFICO_EDAD') AND type IN ( N'P', N'PC' ))
	drop procedure SP_DASHLET_GENERAL_DEMOGRAFICO_EDAD

GO

create procedure SP_DASHLET_GENERAL_DEMOGRAFICO_EDAD (  @CM_NIVEL0 Formula ) 
as
begin 


	declare @Fecha Datetime 
	set @Fecha = getdate() 

	select row_number() over ( order by Grupo) as Id, Grupo as Descripcion,  SUM(Hombre) Hombre, SUM(Mujer) Mujer 
	from ( 
	select 
	case 
	   when  datediff(year, CB_FEC_NAC, @Fecha ) between 1 and 14  then '14 o -' 
	   when  datediff(year, CB_FEC_NAC, @Fecha ) between 15 and 19  then '15 a 19'    
	   when  datediff(year, CB_FEC_NAC, @Fecha ) between 20 and 24  then '20 a 24'    
	   when  datediff(year, CB_FEC_NAC, @Fecha ) between 25 and 29  then '25 a 29'    
	   when  datediff(year, CB_FEC_NAC, @Fecha ) between 30 and 34  then '30 a 34'    
	   when  datediff(year, CB_FEC_NAC, @Fecha ) between 35 and 39  then '35 a 39'    
	   when  datediff(year, CB_FEC_NAC, @Fecha ) between 40 and 44  then '40 a 44'    
	   when  datediff(year, CB_FEC_NAC, @Fecha ) between 45 and 49  then '45 a 49'    
	   when  datediff(year, CB_FEC_NAC, @Fecha ) between 50 and 54  then '50 a 54'    
	   when  datediff(year, CB_FEC_NAC, @Fecha ) between 55 and 59  then '55 a 59'    
	   when  datediff(year, CB_FEC_NAC, @Fecha ) between 60 and 64  then '60 a 64'    
	   when  datediff(year, CB_FEC_NAC, @Fecha ) between 65 and 120 then '65 o +'       
	   else 'No especificado' 
	  end Grupo,  Case when CB_SEXO = 'M' then -1 else 0 end  as Hombre ,  Case when CB_SEXO = 'F' then 1 else 0 end  as Mujer  
	  from COLABORA  
		 where dbo.SP_STATUS_ACT(@Fecha, CB_CODIGO ) = 1   and (@CM_NIVEL0 = ''  or ( COLABORA.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  
		or (@CM_NIVEL0 = '' and COLABORA.CB_NIVEL0=''))	
	  union all 
	  select '14 o -' as Grupo , 0 as Hombre, 0 as Mujer
	  union all 
	  select '15 a 19' as Grupo , 0 as Hombre, 0 as Mujer
	  union all 
	  select '20 a 24' as Grupo , 0 as Hombre, 0 as Mujer
	  union all 
	  select '25 a 29'  as Grupo , 0 as Hombre, 0 as Mujer
	  union all 
	  select '30 a 34' as Grupo , 0 as Hombre, 0 as Mujer
	  union all 
	  select '35 a 39' as Grupo , 0 as Hombre, 0 as Mujer
	  union all 
	  select '40 a 44' as Grupo , 0 as Hombre, 0 as Mujer
	  union all 
	  select '45 a 49' as Grupo , 0 as Hombre, 0 as Mujer
	  union all 
	  select '50 a 54' as Grupo , 0 as Hombre, 0 as Mujer
	  union all 
	  select '55 a 59' as Grupo , 0 as Hombre, 0 as Mujer
	  union all 
	  select '60 a 64' as Grupo , 0 as Hombre, 0 as Mujer
	  union all 
	  select '65 o +' as Grupo , 0 as Hombre, 0 as Mujer
	) Emp
	Group By Grupo 
	order by Grupo 

end 

GO


create procedure REPLICA_R_ENT_ACC_ACUMULADOS_RS 
as
begin 
	-- Iguala derechos a la entidad de Acumula x RS <== Acumula
	update R_ENT_ACC set RE_DERECHO  = ANT.RE_DERECHO from R_ENT_ACC, R_ENT_ACC ANT 
	where ANT.EN_CODIGO = 1  and R_ENT_ACC.EN_CODIGO = 393 and
	R_ENT_ACC.CM_CODIGO = ANT.CM_CODIGO  and R_ENT_ACC.GR_CODIGO = ANT.GR_CODIGO

	-- Agregar derechos a la entidad de Acumula x RS <== Acumula
	insert into R_ENT_ACC (CM_CODIGO, GR_CODIGO, EN_CODIGO, RE_DERECHO ) 
	select R_ENT_ACC.CM_CODIGO, R_ENT_ACC.GR_CODIGO, 393, R_ENT_ACC.RE_DERECHO  from R_ENT_ACC
	left outer join R_ENT_ACC ARS on R_ENT_ACC.CM_CODIGO = ARS.CM_CODIGO  and R_ENT_ACC.GR_CODIGO = ARS.GR_CODIGO and  ARS.EN_CODIGO = 393 
	where R_ENT_ACC.EN_CODIGO = 1  and ARS.CM_CODIGO is NULL 	
end 

go 

exec REPLICA_R_ENT_ACC_ACUMULADOS_RS 

go 

drop procedure REPLICA_R_ENT_ACC_ACUMULADOS_RS 

go 



