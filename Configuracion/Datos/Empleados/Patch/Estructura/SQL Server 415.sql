/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$   Base De Datos DATOS      $$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/

/*****************************************/
/****  Tablas Nuevas***********************/
/*****************************************/

/* Tabla Ausencia: Tipo de n�mina ( PRO - Cambio de tipo de n�mina )*/
/* Patch 415 # Seq: X Agregado */
ALTER TABLE AUSENCIA ADD CB_NOMINA NominaTipo
GO

/* Inicializaci�n de tipo de n�mina en tarjeta ( PRO - Cambio de tipo de n�mina ) (1/5)*/
/* Patch 415 # Seq: X Agregado */
CREATE FUNCTION SP_KARDEX_CB_NOMINA( @FECHA DATETIME, @EMPLEADO INTEGER ) RETURNS NominaTipo AS
BEGIN
  declare @Regresa NominaTipo;
  set @Regresa = ( select TOP 1 CB_NOMINA from KARDEX WHERE CB_CODIGO = @Empleado and
                   CB_FECHA <= @Fecha order by CB_FECHA desc, CB_NIVEL desc )
  if ( @Regresa is NULL )
      set @Regresa = ( select TOP 1 CB_NOMINA from KARDEX WHERE CB_CODIGO = @Empleado
                       order by CB_FECHA, CB_NIVEL )
  RETURN COALESCE( @Regresa, 0)
END
GO

/* Inicializaci�n de tipo de n�mina en tarjeta ( PRO - Cambio de tipo de n�mina ) (2/5)*/
/* Patch 415 # Seq: X Agregado */
CREATE FUNCTION SP_GET_NOMTIPO( @Fecha DATETIME, @Empleado INTEGER ) returns NominaTipo as
begin
	declare @FechaKardex DATETIME;
	declare @NOMTIPO NominaTipo;
	select @NOMTIPO = CB_NOMINA, @FechaKardex = CB_FEC_NOM from COLABORA where ( CB_CODIGO = @Empleado );
	if ( @Fecha < @FechaKardex )
	begin
		select @NOMTIPO  = dbo.SP_KARDEX_CB_NOMINA( @Fecha, @Empleado );
    end
	return @NOMTIPO;
end
GO

/* Inicializaci�n de tipo de n�mina en tarjeta ( PRO - Cambio de tipo de n�mina ) (3/5)*/
/* Patch 415 # Seq: X Agregado */
UPDATE AUSENCIA SET CB_NOMINA = ( SELECT DBO.SP_GET_NOMTIPO( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO ) )
GO


/* Inicializaci�n de tipo de n�mina en tarjeta ( PRO - Cambio de tipo de n�mina ) (4/5)*/
/* Patch 415 # Seq: X Agregado */
drop function SP_GET_NOMTIPO
GO

/* Inicializaci�n de tipo de n�mina en tarjeta ( PRO - Cambio de tipo de n�mina ) (5/5)*/
/* Patch 415 # Seq: X Agregado */
drop function SP_KARDEX_CB_NOMINA
GO

/* Tabla N�mina: Dias Periodo Calendario ( PRO Cambio Tipo N�mina )*/
/* Patch 415 # Seq: X Agregado */
ALTER TABLE NOMINA ADD NO_DIAS_PE Dias 
GO

/* Tabla N�mina: Inicializar Dias Periodo Calendario ( PRO Cambio Tipo N�mina )*/
/* Patch 415 # Seq: X Agregado */
UPDATE NOMINA SET NO_DIAS_PE =  CONVERT( SmallInt, ( NO_ASI_FIN - NO_ASI_INI ) + 1 ) where ( NO_DIAS_PE is Null )
GO 

/* Tabla Colabora: Considerar el cambio de tipo de n�mina en la fecha del �ltimo kardex 
				( PRO - Cambio de tipo de n�mina )*/
/* Patch 415 # Seq: X Modificado*/
update COLABORA set CB_FEC_KAR = COALESCE ( ( select MAX(CB_FECHA) from KARDEX where KARDEX.CB_CODIGO = COLABORA.CB_CODIGO
							AND CB_TIPO in ('ALTA', 'TIPNOM','CIERRE') AND CB_FECHA > COLABORA.CB_FEC_KAR  ), COLABORA.CB_FEC_KAR )
GO


/*Inicializa descripci�n de perido con formato de periodicidad/periodo. (PRO Replicar nombre tipo nomina configurable) (1/3)*/
/* Patch 415 # Seq: X Agregado*/
CREATE PROCEDURE SP_INI_TPERIODO 
AS
BEGIN
	 Declare @TipoNomina varchar(30)
	 declare TmpCosto cursor for
	 select TP_DESCRIP from TPERIODO

	 Open TmpCosto
		Fetch Next From TmpCosto Into 
		@TipoNomina

		while @@Fetch_Status = 0
		begin
			if ( @TipoNomina = 'Diario' )
				update TPERIODO set TP_DESCRIP='Diario/D�a' where TP_DESCRIP = @TipoNomina
			else if ( @TipoNomina = 'Semanal')
				update TPERIODO set TP_DESCRIP='Semanal/Semana' where TP_DESCRIP = @TipoNomina				
			else if ( @TipoNomina = 'Catorcenal')
				update TPERIODO set TP_DESCRIP='Catorcenal/Catorcena' where TP_DESCRIP = @TipoNomina					
			else if ( @TipoNomina = 'Quincenal')
				update TPERIODO set TP_DESCRIP='Quincenal/Quincena' where TP_DESCRIP = @TipoNomina				
			else if ( @TipoNomina = 'Mensual')
				update TPERIODO set TP_DESCRIP='Mensual/Mes' where TP_DESCRIP = @TipoNomina	
			else if ( @TipoNomina = 'Decenal')
				update TPERIODO set TP_DESCRIP='Decenal/Decena' where TP_DESCRIP = @TipoNomina						
			else if ( @TipoNomina = 'Semanal A')
				update TPERIODO set TP_DESCRIP='Semanal A/Semana A' where TP_DESCRIP = @TipoNomina						
			else if ( @TipoNomina = 'Semanal B')
				update TPERIODO set TP_DESCRIP='Semanal B/Semana B' where TP_DESCRIP = @TipoNomina						
			else if ( @TipoNomina = 'Quincenal A')
				update TPERIODO set TP_DESCRIP='Quincenal A/Quincena A' where TP_DESCRIP = @TipoNomina						
			else if ( @TipoNomina = 'Quincenal B')
				update TPERIODO set TP_DESCRIP='Quincenal B/Quincena B' where TP_DESCRIP = @TipoNomina						
			else if ( @TipoNomina = 'Catorcenal A')
				update TPERIODO set TP_DESCRIP='Catorcenal A/Catorcena A' where TP_DESCRIP = @TipoNomina				
			else if ( @TipoNomina = 'Catorcenal B')
				update TPERIODO set TP_DESCRIP='Catorcenal B/Catorcena B' where TP_DESCRIP = @TipoNomina					
	 Fetch Next From TmpCosto Into
	 @TipoNomina

	 end
	 close TmpCosto
	 deallocate TmpCosto
END
GO

/*Inicializa descripci�n de peridoo con formato de periodicidad/periodo. (PRO Replicar nombre tipo nomina configurable) (2/3)*/
/*Patch 415 # Seq: X Agregado*/
EXEC SP_INI_TPERIODO
GO


/*Inicializa descripci�n de peridoo con formato de periodicidad/periodo. (PRO Replicar nombre tipo nomina configurable) (3/3)*/
/* Patch 415 # Seq: X Agregado*/
DROP PROCEDURE SP_INI_TPERIODO
GO


/* TAHORRO.TB_FEC_INI Agregar la fecha de rango de inicio ( Reglas en pr�stamos y ahorros )*/
/* Patch 415 # Seq: X Agregado*/
alter table TAHORRO add TB_FEC_INI Fecha
GO

/*TAHORRO.TB_FEC_FIN Agregar la fecha de rango del final ( Reglas en pr�stamos y ahorros )*/
/* Patch 415 # Seq: X Agregado*/
alter table TAHORRO add TB_FEC_FIN Fecha
GO

/*TAHORRO.TB_VAL_RAN Si se valida el rango de fechas ? ( Reglas en pr�stamos y ahorros )*/
/*Patch 415 # Seq: X Agregado*/
alter table TAHORRO add TB_VAL_RAN Booleano
GO


/*INIT TAHORRO.TB_VAL_RAN Si se valida el rango de fechas ? ( Reglas en pr�stamos y ahorros )*/
/*Patch 415 # Seq: X Agregado*/
UPDATE TAHORRO SET TB_VAL_RAN = 'N' where ( TB_VAL_RAN is Null )
GO

/* REGLAPRES.Crear tabla de reglas para la validaci�n de los pr�stamos ( Reglas en pr�stamos y ahorros )*/
/*Patch 415 # Seq: X Agregado*/
CREATE TABLE REGLAPREST 
(
      RP_CODIGO FolioChico,   /*Codigo de Regla*/
      RP_LETRERO DescLarga,   /*Mensaje de Error de la Regla*/
      RP_ACTIVO Booleano,     /*� Activa ?*/
      RP_PRE_STS FolioChico,  /*Status del Pr�stamo a validar */  
      RP_LIMITE FolioChico,   /*N�mero l�mite de Prestamos */
      RP_VAL_FEC Booleano,    /*Validar rango de fechas del ahorro ?*/
      RP_TOPE Formula,        /*F�rmula del Tope del monto prestado*/
      RP_EMP_STS FolioChico,  /*Status del Empleado a validar*/
      RP_ANT_INI FolioChico,  /*Antiguedad en D�as L�mite inicial*/
      RP_ANT_FIN FolioChico,  /*Antiguedad en D�as L�mite final*/
      QU_CODIGO Condicion ,  	/*Condici�n de empleados a validar*/
      RP_FILTRO Formula,      /*F�rmula del Filtro de empleados a validar */
      RP_LISTA Booleano, 	/*�Contiene Lista de Pr�stamos?*/
      RP_ORDEN FolioChico, 	/* Orden de prioridad */
      LLAVE Int IDENTITY (1, 1)
)

GO


/*PK REGLAPRES: Definir Llave primaria ( PRO Reglas en pr�stamos y ahorros )*/
/*Patch 415 # Seq: X Agregado*/
ALTER TABLE REGLAPREST
       ADD CONSTRAINT PK_REGLAPREST PRIMARY KEY (RP_CODIGO)
GO

/*PRESTAXREG: Tabla de Reglas y prestamos a validar */
/*Patch 415 # Seq: X Agregado*/
CREATE TABLE PRESTAXREG
(
      RP_CODIGO FolioChico,   /*Codigo de la regla*/
      TB_CODIGO Codigo1,      /*Codigo del Tipo de Pr�stamo*/
      LLAVE Int IDENTITY (1, 1)
)
GO

/*PK PRESTAXREG: Llave Primaria ( PRO Reglas en pr�stamos y ahorros ) */
/*Patch 415 # Seq: X Agregado*/
ALTER TABLE PRESTAXREG
       ADD CONSTRAINT PK_PRESTAXREG PRIMARY KEY (RP_CODIGO, TB_CODIGO)
go

/*PRESTAXREG: Llave foranea para borrar en cascada los tipos de prestamos asignados ( PRO Reglas en pr�stamos y ahorros )*/
/*Patch 415 # Seq: X Agregado*/
ALTER TABLE PRESTAXREG
       ADD CONSTRAINT FK_REGLA_REGLAPREST
       FOREIGN KEY (RP_CODIGO)
       REFERENCES REGLAPREST
       ON DELETE CASCADE
       ON UPDATE CASCADE
go


/* TU_TIP_JT: Tipo Jornada Trabajo ( PRO Festivo Trabajado )*/
/*Patch 415 # Seq: X Agregado*/
ALTER TABLE TURNO ADD TU_TIP_JT  Status
GO 

/* INIT_JOR_TRABAJO: Inicializa el tipo de jornada de trabajo (1/3) ( PRO Festivo Trabajado )*/
/*Patch 415 # Seq: X Agregado*/
CREATE PROCEDURE INIT_JOR_TRABAJO
AS
BEGIN
	declare @JornadaTrabajo Pesos
	declare @TipoJornadaTrabajo Status
	declare @Turno Codigo 

	declare Temporal CURSOR FOR   
     select CAST ( TURNO.TU_JORNADA / ( CASE ( TURNO.TU_DIAS * TURNO.TU_VACA_HA ) 
						WHEN 0 THEN 1
						ELSE
						TURNO.TU_DIAS  
						END ) AS  NUMERIC(15,2) ), TU_CODIGO from Turno 
	OPEN Temporal
   	FETCH NEXT FROM Temporal INTO @JornadaTrabajo, @Turno


	WHILE @@FETCH_STATUS = 0
    	BEGIN
		set @TipoJornadaTrabajo = CASE 
						   WHEN @JornadaTrabajo >= 8 THEN 0
						   WHEN @JornadaTrabajo >= 7.5 THEN 1
					        WHEN @JornadaTrabajo <= 7 THEN 2
						   END 

		UPDATE TURNO SET TU_TIP_JT = @TipoJornadaTrabajo where ( TU_CODIGO = @Turno ) 
		FETCH NEXT FROM Temporal INTO @JornadaTrabajo, @Turno
	END
	close Temporal
     deallocate Temporal
END
GO

/* INIT_JOR_TRABAJO: Inicializa el tipo de jornada de trabajo (2/3) ( PRO Festivo Trabajado )*/
/*Patch 415 # Seq: X Agregado*/
EXEC INIT_JOR_TRABAJO
GO

/* INIT_JOR_TRABAJO: Inicializa el tipo de jornada de trabajo (3/3) ( PRO Festivo Trabajado )*/
/*Patch 415 # Seq: X Agregado*/
DROP PROCEDURE INIT_JOR_TRABAJO
GO

/* EVENTO.EV_M_SVAC: Saldar Vacaciones? ( PRO Saldar vacaciones al reingreso )*/
/*Patch 415 # Seq: X Agregado*/
ALTER TABLE EVENTO ADD EV_M_SVAC Booleano
GO

/* Inicializaci�n de EVENTO.EV_M_SVAC ( PRO Saldar vacaciones al reingreso ) */
/*Patch 415 # Seq: X Agregado*/
UPDATE EVENTO SET EV_M_SVAC = 'N' WHERE ( EV_M_SVAC is Null )
GO

/* EVENTO.EV_M_TVAC: Tipo de Saldo de Vacaciones ( PRO Saldar vacaciones al reingreso ) */
/*Patch 415 # Seq: X Agregado*/
ALTER TABLE EVENTO ADD EV_M_TVAC Status
GO

/* Inicializaci�n de EVENTO.EV_M_TVAC ( PRO Saldar vacaciones al reingreso )*/
/*Patch 415 # Seq: X Agregado*/
UPDATE EVENTO SET EV_M_TVAC = 0 WHERE ( EV_M_TVAC is Null )
GO

/*COLABORA.CB_NUM_EXT y COLABORA.CB_NUM_INT:  N�mero exterior e Interior */
/*Patch 415 # Seq: X Agregado*/
ALTER TABLE COLABORA ADD CB_NUM_EXT NombreCampo, CB_NUM_INT NombreCampo
GO

/*Inicializaci�n de COLABORA.CB_NUM_EXT:  N�mero exterior */
/*Patch 415 # Seq: X Agregado*/
UPDATE COLABORA set CB_NUM_EXT='' where ( CB_NUM_EXT is null )
GO

/*Inicializaci�n de COLABORA.CB_NUM_INT:  N�mero interior */
/*Patch 415 # Seq: X Agregado*/
UPDATE COLABORA set CB_NUM_INT='' where ( CB_NUM_INT is null )
GO

/*COLABORA.CB_CALLE:  Cambiar dominio */
/*Patch 415 # Seq: X Agregado*/
ALTER TABLE COLABORA ALTER COLUMN CB_CALLE Observaciones not null
GO

/*EXPEDIEN.EX_NUM_EXT y EXPEDIEN.EX_NUM_INT: N�mero exterior e Interior*/
/*Patch 415 # Seq: X Agregado*/
ALTER TABLE EXPEDIEN ADD EX_NUM_EXT NombreCampo, EX_NUM_INT NombreCampo
GO

/*Inicializaci�n EXPEDIEN.EX_NUM_EXT: N�mero exterior*/
/*Patch 415 # Seq: X Agregado*/
UPDATE EXPEDIEN SET EX_NUM_EXT='' WHERE ( EX_NUM_EXT is null )
GO

/*Inicializaci�n EXPEDIEN.EX_NUM_INT: N�mero interior*/
/*Patch 415 # Seq: X Agregado*/
UPDATE EXPEDIEN SET EX_NUM_INT='' WHERE ( EX_NUM_INT is null )
GO

/*EXPEDIEN.EX_CALLE: Cambiar dominio*/
/*Patch 415 # Seq: X Agregado*/
ALTER TABLE EXPEDIEN ALTER COLUMN EX_CALLE Observaciones not null
GO

/* Tabla: NOMINA: Nominas */
/* Patch 415 # Seq: X */
/* Es Simulacion de Finiquito Global ? */
alter table NOMINA add NO_GLOBAL Booleano
GO

/* Tabla: NOMINA: Nominas */
/* Patch 415 # Seq: X */
/* Status de la  Simulacion de Finiquito: 0= Sin Aprobar 1= Aprobado 2= Aplicada  */
alter table NOMINA add NO_APROBA STATUS
GO

/*NOMINA.NO_GLOBAL: ACTUALIZAR*/
/*Patch 415 # Seq: X Agregado*/
UPDATE NOMINA SET NO_GLOBAL='N' where ( NO_GLOBAL is null )
GO

/*NOMINA.NO_APROBA: ACTUALIZAR*/
/*Patch 415 # Seq: X Agregado*/
UPDATE NOMINA SET NO_APROBA='1' where ( NO_APROBA is null )
GO

/* QUERYS: Agrega nuevos campos (PRO: Sugerencia 1308 Navegacion de Empleados) */
/* Patch 410 # Seq: X Agregado */
alter table QUERYS add QU_NAVEGA Booleano, QU_ORDEN FolioChico
GO

/*QUERYS.QU_NAVEGA: ACTUALIZAR*/
/*Patch 415 # Seq: X Agregado*/
update QUERYS set QU_NAVEGA='N' where ( QU_NAVEGA is null )
GO

/*QUERYS.QU_ORDEN: ACTUALIZAR*/
/*Patch 415 # Seq: X Agregado*/
update QUERYS set QU_ORDEN=0 where ( QU_ORDEN is null )
GO

/*QUERYS.QU_CANDADO: Agrega campo(SUG. SUG Navegacion de Empleados)*/
/*Patch 415 # Seq: X Agregado*/
alter table QUERYS add QU_CANDADO Booleano
GO

/*QUERYS.QU_CANDADO: ACTUALIZAR*/
/*Patch 415 # Seq: X Agregado*/
update QUERYS set QU_CANDADO='N' where ( QU_CANDADO is null )
GO

/* AGREGA_QUERYS_SISTEMA: Agrega filtros de Sistema para Navegaci�n (PRO: Sugerencia 1308 Navegacion de Empleados) (1/5) */
/* Patch 415 # Seq: X Agregado */
create procedure AGREGA_QUERYS_SISTEMA( @Codigo Condicion, @Descripcion Formula, @Filtro Formula )
as
begin
     SET NOCOUNT ON;
     declare @Cuantos Integer;
     select @Cuantos = COUNT(*) from QUERYS where( QU_CODIGO = @Codigo );
     if ( @Cuantos = 0 )
     begin
          insert into QUERYS( QU_CODIGO, QU_DESCRIP, QU_FILTRO, QU_NIVEL, US_CODIGO, QU_NAVEGA, QU_ORDEN )
          values ( @Codigo, @Descripcion, @Filtro, 7, 0, 'S', 0 );
     end
end
GO

/* AGREGA_QUERYS_SISTEMA: Agrega el filtro TRESSACT (PRO: Sugerencia 1308 Navegacion de Empleados) (2/5)*/
/* Patch 415 # Seq: 26 Agregado */
execute AGREGA_QUERYS_SISTEMA 'TRESSACT', 'Activos', 'COLABORA.CB_ACTIVO=''S'' '
GO

/* AGREGA_QUERYS_SISTEMA: Agrega el filtro TRESSREI (PRO: Sugerencia 1308 Navegacion de Empleados) (3/5)*/
/* Patch 415 # Seq: 26 Agregado */
execute AGREGA_QUERYS_SISTEMA 'TRESSREI', 'Reingreso', '@(COLABORA.CB_ACTIVO=''S'') and ( COLABORA.CB_FEC_BAJ<>(''1899-12-30'') ) and ( COLABORA.CB_FEC_BAJ < COLABORA.CB_FEC_ING  ) '
GO

/* AGREGA_QUERYS_SISTEMA: Agrega el filtro TRESSBAJ (PRO: Sugerencia 1308 Navegacion de Empleados) (4/5)*/
/* Patch 415 # Seq: 26 Agregado */
execute AGREGA_QUERYS_SISTEMA 'TRESSBAJ', 'Baja', 'COLABORA.CB_ACTIVO=''N'' '
GO

/* AGREGA_QUERYS_SISTEMA: Borra procedure (PRO: Sugerencia 1308 Navegacion de Empleados) (5/5)*/
/* Patch 415 # Seq: 27 Agregado */
drop procedure AGREGA_QUERYS_SISTEMA
GO

/*COLABORA.CB_INF_ANT: Se agrega la columna de fecha de otorgamiento de INFONAVIT*/
/* Patch 415 # Seq: X Agregado */
alter table COLABORA add CB_INF_ANT fecha
GO

/*KARINF.CB_INF_ANT:Se agrega la columna de fecha de otorgamiento de INFONAVIT*/
/* Patch 415 # Seq: X Agregado */
alter table KARINF add CB_INF_ANT fecha
GO

/*INIT COLABORA.CB_INF_ANT: Inicializaci�n de fecha de otorgamiento de INFONAVIT*/
/* Patch 415 # Seq: X Agregado */
update COLABORA set CB_INF_ANT= CB_INF_INI
GO

/*INIT KARINF.CB_INF_ANT: Inicializaci�n de fecha de otorgamiento de INFONAVIT*/
/* Patch 415 # Seq: X Agregado */
update KARINF set CB_INF_ANT = ( select CB_INF_ANT from COLABORA where COLABORA.CB_CODIGO=KARINF.CB_CODIGO)
GO

/* PRO - ReProgramacion de Cursos Fersinsa ***********************************/
/* Tabla: CUR_REV: Historial de revisiones de Cursos*/
/* Patch 415 # Seq: 1  */
CREATE TABLE CUR_REV(
      CU_CODIGO Codigo NOT NULL,		  /* C�digo de curso */
      CH_REVISIO NombreCampo NOT NULL,	  /* Revisi�n */
      CH_FECHA Fecha NOT NULL,			  /* Fecha de revisi�n */
      CH_OBSERVA Observaciones NOT NULL,  /* Observaciones */
      US_CODIGO Usuario NOT NULL,		  /* �M�difico? */
      LLAVE Int IDENTITY (1, 1)
);
GO

/*Tabla: CUR_REV: Llave primaria de revisiones por curso */
/* Patch 415 # Seq: 2  */
ALTER TABLE CUR_REV
       ADD CONSTRAINT PK_CUR_REV PRIMARY KEY (CU_CODIGO, CH_REVISIO, CH_FECHA);
GO

/* Tabla: CUR_REV: Llave for�nea hacia curso */
/* Patch 415 # Seq: 3 */
ALTER TABLE CUR_REV
       ADD CONSTRAINT FK_CUR_REV_CURSO
       FOREIGN KEY (CU_CODIGO)
       REFERENCES CURSO
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

/*Tabla: ENTRENA: Nuevo Campo para Reprogramar cursos por revisiones*/
/* Patch 415 # Seq: 4  */
alter table ENTRENA add EN_REPROG Booleano;
GO

/*Tabla: ENTRENA: Actualiza la reprogramacion por revision*/
/* Patch 415 # Seq: 5  */
update ENTRENA set EN_REPROG = 'N' where EN_REPROG is null;
GO

/*Tabla: ENTRENA: Dias de Reprogramacion por frecuencia*/
/* Patch 415 # Seq: 5  */
alter table ENTRENA add EN_RE_DIAS Dias;
GO

/*Tabla: ENTRENA: Nuevo Actualiza D�as de entrenamiento*/
/* Patch 415 # Seq: 5  */
update ENTRENA set EN_RE_DIAS = 0 where EN_RE_DIAS is null;
GO

/*Tabla: KARCURSO: Nuevo Campo guardar la fecha de programaci�n */
/* Patch 415 # Seq: 6  */
alter table KARCURSO add KC_FECPROG Fecha; /*Fecha de programaci�n del curso tomado */
GO

/*Tabla: ENTRENA: Nuevo Actualiza D�as de entrenamiento*/
/* Patch 415 # Seq: 5  */
update KARCURSO set KC_FECPROG = '12/30/1899' where KC_FECPROG is null;
GO

/* Tabla: EMP_PROG: Fecha de programaci�n espec�fica (PRO: Reprogramaci�n de Cursos: Fersinsa) */
/* Patch 415 # Seq: 7 Agregado */
alter table EMP_PROG Add EP_FECHA Fecha;
GO

/* Tabla: EMP_PROG: Inicializa EP_FECHA (PRO: Reprogramaci�n de Cursos: Fersinsa) */
/* Patch 415 # Seq: 7 Agregado */
update EMP_PROG set EP_FECHA = '12/30/1899' where EP_FECHA is null;
GO

/* Tabla: EMP_PROG: �Programacion Por Dias? (PRO: Reprogramacion de Cursos: Fersinsa) */
/* Patch 415 # Seq: 8 Agregado */
alter table EMP_PROG Add EP_PORDIAS Booleano;
GO

/* Tabla: EMP_PROG: Actualizar todos los cursos individuales para establecer que la base instalada esta por Dias(PRO: Reprogramacion de Cursos: Fersinsa) */
/* Patch 415 # Seq: 9 Agregado */
update EMP_PROG set EP_PORDIAS ='S';
GO

/* Tabla: EMP_PROG: �Programacion  Geneerad Globalmente? (PRO: Reprogramacion de Cursos: Fersinsa) */
/* Patch 415 # Seq: 8 Agregado */
alter table EMP_PROG Add EP_GLOBAL Booleano;
GO

/* Tabla: EMP_PROG: �Programacion  Geneerad Globalmente? (PRO: Reprogramacion de Cursos: Fersinsa) */
/* Patch 415 # Seq: 8 Agregado */
update EMP_PROG set EP_GLOBAL = 'N' where EP_GLOBAL is null;
GO

/* Tabla: KARCURSO: Pcedure Temporal para actualizar los cursos ya tomados con al fecha de programacion que aplicaba en ese curso(PRO: Reprogramacion de Cursos: Fersinsa) (1/3)*/
/* Patch 415 # Seq: 11 Agregado */
CREATE procedure [dbo].[ActualizaCursosTomados] 
AS
declare @Dias Integer;
declare @Ingreso Fecha;
declare @Empleado Integer;
declare @Curso Codigo;
declare @FecTomada Fecha;

	 Declare TempCursoTomados CURSOR for
        select E.EN_DIAS,C.CB_FEC_ING,C.CB_CODIGO,K.CU_CODIGO,K.KC_FEC_TOM  from KARCURSO K 
			   join COLABORA C on C.CB_CODIGO = K.CB_CODIGO and C.CB_FEC_ING IS NOT NULL
			   join ENTRENA E on K.CU_CODIGO = E.CU_CODIGO and C.CB_PUESTO = E.PU_CODIGO
	OPEN TempCursoTomados
	 fetch NEXT FROM TempCursoTomados
        INTO @Dias,@Ingreso ,@Empleado,@Curso,@FecTomada
    WHILE @@FETCH_STATUS = 0
	BEGIN
	      if (@Dias is null)
		  begin
		       SET @Dias = 0;
		  end
		  update KARCURSO set KC_FECPROG =DATEADD( DAY, @Dias , @Ingreso ) where CB_CODIGO = @Empleado and CU_CODIGO = @Curso and KC_FEC_TOM = @FecTomada;
	     
        fetch NEXT FROM TempCursoTomados
         INTO @Dias,@Ingreso ,@Empleado,@Curso,@FecTomada
 	end
	close TempCursoTomados; 
    deallocate TempCursoTomados;
GO

/* Tabla: KARCURSO: Ejecutamos procedure que actualiza la tabla de cursos tomados a guardar la Fecha de Programaci�n de los Cursos Tomados(PRO: Reprogramacion de Cursos: Fersinsa) (2/3) */
/* Patch 415 # Seq: 12 Agregado */
EXEC dbo.ActualizaCursosTomados;
GO

/* Tabla: KARCURSO: Borramos el procedure temporal(PRO: Reprogramacion de Cursos: Fersinsa) (3/3) */
/* Patch 1 # Seq: 13 Agregado */
drop procedure dbo.ActualizaCursosTomados ;
GO

/* Tabla: KARCURSO: Pcedure Temporal para actualizar los cursos ya tomados Individualmente (PRO: Reprogramacion de Cursos: Fersinsa) (1/3) */
/* Patch 415 # Seq: 11 Agregado */
CREATE procedure [dbo].[ActualizaCursosIndividuales]
AS
declare @Dias Integer;
declare @Ingreso Fecha;
declare @Empleado Integer;
declare @Curso Codigo;
declare @FecTomada Fecha;

	 Declare TempCursoTomados CURSOR for
        select EP.EP_DIAS,C.CB_FEC_ING,EP.CB_CODIGO,EP.CU_CODIGO from EMP_PROG EP
			   join COLABORA C on C.CB_CODIGO = EP.CB_CODIGO and C.CB_FEC_ING IS NOT NULL
	OPEN TempCursoTomados
	 fetch NEXT FROM TempCursoTomados
        INTO @Dias,@Ingreso ,@Empleado,@Curso
    WHILE @@FETCH_STATUS = 0
	BEGIN
	      if (@Dias is null)
		  begin
		       SET @Dias = 0;
		  end
		  update EMP_PROG set EP_FECHA = DATEADD( DAY, @Dias , @Ingreso ) where CB_CODIGO = @Empleado and CU_CODIGO = @Curso
	     
        fetch NEXT FROM TempCursoTomados
         INTO @Dias,@Ingreso ,@Empleado,@Curso
 	end
	close TempCursoTomados;
    deallocate TempCursoTomados;
GO

/* Tabla: KARCURSO: Ejecutamos procedure que actualiza la tabla de cursos tomados a guardar la Fecha de Programaci�n de los Cursos Tomados(PRO: Reprogramacion de Cursos: Fersinsa) (2/3)*/
/* Patch 415 # Seq: 12 Agregado */
EXEC dbo.ActualizaCursosIndividuales;
GO

/* Tabla: KARCURSO: Borramos el procedure temporal(PRO: Reprogramacion de Cursos: Fersinsa) (3/3)*/
/* Patch 415 # Seq: 13 Agregado */
drop procedure dbo.ActualizaCursosIndividuales ;
GO

/* Tabla: CUR_PROG: Borramos el view de CUR_PROG para actualizarlo(PRO: Reprogramacion de Cursos: Fersinsa) */
/* Patch 1 # Seq: 14 Agregado */
drop view CUR_PROG;
GO
/* Tabla: CUR_PROG: Actualizamos el view de CUR_PROG para que muestre las reprogramaciones por Revisiones o por Frecuencia(PRO: Reprogramacion de Cursos: Fersinsa) */
/* Patch 1 # Seq: 15 Agregado */
CREATE VIEW CUR_PROG (
  CB_CODIGO, 
  CB_PUESTO, 
  CU_CODIGO, 
  KC_FEC_PRO, 
  KC_EVALUA, 
  KC_FEC_TOM, 
  KC_HORAS, 
  CU_HORAS, 
  EN_OPCIONA, 
  EN_LISTA, 
  MA_CODIGO, 
  KC_PROXIMO, 
  KC_REVISIO, 
  CU_REVISIO,
  CP_MANUAL,
  EP_GLOBAL
) AS
select
  C.CB_CODIGO,
  C.CB_PUESTO,
  CURSO.CU_CODIGO,
 ( Select case when (KARCURSO.KC_FEC_TOM IS NULL )then DATEADD( DAY, E.EN_DIAS, C.CB_FEC_ING ) else KARCURSO.KC_FECPROG end )KC_FEC_PRO,
  KARCURSO.KC_EVALUA,
  KARCURSO.KC_FEC_TOM,
  KARCURSO.KC_HORAS,
  CU_HORAS,
  E.EN_OPCIONA,
  E.EN_LISTA,
  CURSO.MA_CODIGO,
  (SELECT MIN(SE_FEC_INI) AS Expr1 FROM  dbo.SESION WHERE (CU_CODIGO = E.CU_CODIGO) AND (SE_FEC_INI >= CAST(FLOOR(CAST(GETDATE() AS float)) AS Datetime))) AS KC_PROXIMO, 
  KARCURSO.KC_REVISIO,
  CURSO.CU_REVISIO,
  cast( 'N' as CHAR(1) ) CP_MANUAL,
  cast( 'N' as CHAR(1) ) EP_GLOBAL
  from COLABORA C
  join ENTRENA E on ( E.PU_CODIGO = C.CB_PUESTO )
  left join KARCURSO on ( KARCURSO.CB_CODIGO = C.CB_CODIGO ) and ( KARCURSO.CU_CODIGO = E.CU_CODIGO )
  left join CURSO on ( CURSO.CU_CODIGO = E.CU_CODIGO )
where ( CURSO.CU_ACTIVO = 'S' ) 
UNION ALL
select
  C.CB_CODIGO,
  C.CB_PUESTO,
  CU.CU_CODIGO,
  ( select case when K.KC_FEC_TOM is null then NULL
		        when ( ( E.EN_RE_DIAS > 0 AND E.EN_REPROG = 'N' ) OR ( ( E.EN_RE_DIAS > 0 AND E.EN_REPROG = 'S' )and ( K.KC_REVISIO = CU.CU_REVISIO ) ) )then DATEADD( DAY, E.EN_RE_DIAS, K.KC_FEC_TOM )
				when ( ( E.EN_RE_DIAS > 0 AND E.EN_REPROG = 'S' ) and ( K.KC_FEC_TOM > ( select max(CH_FECHA) from CUR_REV where( ( CUR_REV.CU_CODIGO = CU.CU_CODIGO ) AND ( CUR_REV.CH_REVISIO = CU.CU_REVISIO ) ) ) ) and (K.KC_REVISIO <> CU.CU_REVISIO ))then DATEADD( DAY, E.EN_RE_DIAS, K.KC_FEC_TOM )
				else (select max(CH_FECHA) from CUR_REV where( ( CUR_REV.CU_CODIGO = CU.CU_CODIGO ) AND ( CUR_REV.CH_REVISIO = CU.CU_REVISIO ) ) )End ) KC_FEC_PRO,
  cast(0 as Decimal(15,2)) KC_EVALUA,
  cast('12/30/1899' as DateTime) KC_FEC_TOM,
  cast(0 as Decimal(15,2)) KC_HORAS,
  CU.CU_HORAS,
  E.EN_OPCIONA,
  E.EN_LISTA,
  CU.MA_CODIGO,
  (SELECT MIN(SE_FEC_INI) AS Expr1 FROM  dbo.SESION WHERE (CU_CODIGO = E.CU_CODIGO) AND (SE_FEC_INI >= CAST(FLOOR(CAST(GETDATE() AS float)) AS DateTime))) KC_PROXIMO,
  cast(' ' as varchar(10))KC_REVISIO,               /* Patch 385: Se agreg� KARCURSO.KC_REVISIO ( # de Revisi�n ) */
  CU.CU_REVISIO,                  /*  Patch 385: Se agreg� CURSO.CU_REVISIO ( # de Revisi�n ) */
  cast( 'N' as CHAR(1) ) CP_MANUAL,  /* Patch 385: Se agreg� CUR_PROG.CP_INDIVID ( Curso Programado Individualmente ) Booleano */
  cast( 'N' as CHAR(1) ) EP_GLOBAL
  from COLABORA C
  join ENTRENA E on ( E.PU_CODIGO = C.CB_PUESTO )
  left join KARCURSO K on ( K.CB_CODIGO = C.CB_CODIGO ) and ( K.CU_CODIGO = E.CU_CODIGO ) and ( K.KC_FEC_TOM = ( select MAX( KC_FEC_TOM ) from KARCURSO AS K1 where ( K1.CB_CODIGO = C.CB_CODIGO ) and ( K1.CU_CODIGO = E.CU_CODIGO ) ))
  left join CURSO CU on ( CU.CU_CODIGO = E.CU_CODIGO )
where ( Cu.CU_ACTIVO = 'S' ) and 
		( ( E.EN_RE_DIAS > 0 ) AND NOT ( K.KC_FEC_TOM = '12/30/1899') or
        ( ( E.EN_REPROG = 'S' ) and ( K.KC_REVISIO <> CU.CU_REVISIO ) and ( 
          ( select count(k2.KC_REVISIO) FROM karcurso k2 where k2.KC_REVISIO = CU.CU_REVISIO AND ( k2.CB_CODIGO = C.CB_CODIGO ) and ( k2.CU_CODIGO = E.CU_CODIGO ) ) = 0 ) ))
UNION ALL
select
  C.CB_CODIGO,
  C.CB_PUESTO,
  CURSO.CU_CODIGO,
  (select case when (EP.EP_PORDIAS = 'S' ) then C.CB_FEC_ING + EP.EP_DIAS else EP.EP_FECHA end	)KC_FEC_PRO,
  K.KC_EVALUA,
  K.KC_FEC_TOM,
  K.KC_HORAS,
  CURSO.CU_HORAS,
  EP.EP_OPCIONA,
  cast( 'N' as CHAR(1) ) EN_LISTA,
  CURSO.MA_CODIGO,
  (SELECT MIN(SE_FEC_INI) AS Expr1 FROM dbo.SESION WHERE (CU_CODIGO = EP.CU_CODIGO) AND (SE_FEC_INI >= CAST(FLOOR(CAST(GETDATE() AS float)) AS Datetime))) AS KC_PROXIMO, 
  K.KC_REVISIO,
  CURSO.CU_REVISIO,
  cast( 'S' as CHAR(1) ) CP_MANUAL,	
  EP.EP_GLOBAL
  from COLABORA C
  join EMP_PROG EP on ( EP.CB_CODIGO = C.CB_CODIGO )
  left join CURSO on ( CURSO.CU_CODIGO = EP.CU_CODIGO )
  left join KARCURSO K on ( K.CU_CODIGO = EP.CU_CODIGO and EP.CB_CODIGO = K.CB_CODIGO and K.KC_FEC_TOM >= (select case when (EP.EP_PORDIAS = 'S' ) then C.CB_FEC_ING + EP.EP_DIAS else EP.EP_FECHA end	) )
where ( CURSO.CU_ACTIVO = 'S' );


/* Sugerencia : PRO - Tope de Excepciones*/

/*ListaContenido: Nuevo Tipo de Dato para almacenar codigos */
/* Patch 415 # Seq: X Agregado */
exec sp_addtype ListaContenido, 'varchar(1024)', 'NOT NULL'
exec sp_bindefault StringVacio, ListaContenido
GO

/*CONCEPTO.CO_LIM_SUP: L�mite Superior de Captura de Excepciones de Monto */
/* Patch 415 # Seq: X Agregado */
alter table CONCEPTO add CO_LIM_SUP Pesos;
GO

/*CONCEPTO.CO_LIM_INF: L�mite Inferior de Captura de Excepciones de Monto */
/* Patch 415 # Seq: X Agregado */
alter table CONCEPTO add CO_LIM_INF Pesos;
GO

/*CONCEPTO.CO_VER_INF: Verificar L�mite Inferior de Captura de Excepciones de Monto ?*/
/* Patch 415 # Seq: X Agregado */
alter table CONCEPTO add CO_VER_INF Booleano;
GO

/*CONCEPTO.CO_VER_SUP: Verificar L�mite Inferior de Captura de Excepciones de Monto ? */
/* Patch 415 # Seq: X Agregado */
alter table CONCEPTO add CO_VER_SUP Booleano;
GO

/*CONCEPTO.CO_VER_ACC: Verificar Acceso de Captura de Excepciones de Monto ? */
/* Patch 415 # Seq: X Agregado */
alter table CONCEPTO add CO_VER_ACC Booleano;
GO

/*CONCEPTO.CO_VER_ACC: Grupos que tienen acceso a Capturar Excepciones ? */
/* Patch 415 # Seq: X Agregado */
alter table CONCEPTO add CO_GPO_ACC ListaContenido;
GO

/*CONCEPTO.CO_LIM_SUP: Actualizar Campo */
/* Patch 415 # Seq: X Agregado */
update CONCEPTO set CO_LIM_SUP = 0 where (CO_LIM_SUP is Null);
GO

/*CONCEPTO.CO_LIM_INF: Actualizar Campo */
/* Patch 415 # Seq: X Agregado */
update CONCEPTO set CO_LIM_INF = 0 where (CO_LIM_INF is Null);
GO

/*CONCEPTO.CO_VER_SUP: Actualizar Campo */
/* Patch 415 # Seq: X Agregado */
update CONCEPTO set CO_VER_SUP = 'N'  where (CO_VER_SUP is Null); 
GO

/*CONCEPTO.CO_VER_INF: Actualizar Campo */
/* Patch 415 # Seq: X Agregado */
update CONCEPTO set CO_VER_INF = 'N' where (CO_VER_INF is Null); 
GO

/*CONCEPTO.CO_VER_ACC: Actualizar Campo */
/* Patch 415 # Seq: X Agregado */
update CONCEPTO set CO_VER_ACC = 'N' where (CO_VER_ACC is Null); 
GO

/*CONCEPTO.CO_GPO_ACC: Actualizar Campo */
/* Patch 415 # Seq: X Agregado */
update CONCEPTO set CO_GPO_ACC = '' where (CO_GPO_ACC is Null);
GO

/* Tabla Festivo: Vigencia de Festivo */
/* Patch 415 # Seq: X Agregado */
ALTER TABLE FESTIVO ADD FE_YEAR Anio 
GO

/* FESTIVO.FE_YEAR: Inicializaci�n de Vigencia de Festivo */
/* Patch 415 # Seq: X Agregado */
UPDATE FESTIVO SET FE_YEAR = 0 WHERE ( FE_TIPO = 0 ) and ( FE_YEAR is Null )
GO

/* FESTIVO.FE_YEAR: Inicializaci�n de Intercambio de Festivo */
/* Patch 415 # Seq: X Agregado */
UPDATE FESTIVO SET FE_YEAR = SUBSTRING(CONVERT(VARCHAR(4), FE_CAMBIO, 112), 1, 5)  WHERE FE_TIPO = 1
GO

/* FESTIVO.FE_YEAR: Volver not null */
/* Patch 415 # Seq: X Agregado */
ALTER TABLE FESTIVO ALTER COLUMN FE_YEAR Anio not null
GO

/* ALTER PK_FESTIVO: Agregar FE_YEAR a la llave primaria (1/2) */
/* Patch 415 # Seq: X Agregado */
ALTER TABLE FESTIVO 
	DROP CONSTRAINT PK_FESTIVO
GO

/* ALTER PK_FESTIVO: Agregar FE_YEAR a la llave primaria (1/2) */
/* Patch 415 # Seq: X Agregado */
ALTER TABLE FESTIVO 
	ADD CONSTRAINT PK_FESTIVO PRIMARY KEY (TU_CODIGO, FE_MES, FE_DIA, FE_YEAR )
GO

/* NOMINA.NO_RASTREO: Rastreo de n�mina */
/* Patch 415 # Seq: X Agregado */
ALTER TABLE NOMINA ADD NO_RASTREO Memo
GO

/* Inicializar NOMINA.NO_RASTREO: Rastreo de n�mina */
/* Patch 415 # Seq: X Agregado */
UPDATE NOMINA SET NO_RASTREO = ' ' where NO_RASTREO is Null
GO

/* Inicializar MOT_CHECA: Motivo de Checadas Manuales(1/2) */
/* Patch 415 # Seq: X Agregado */
CREATE TABLE MOT_CHECA(
       TB_CODIGO            Codigo4, /*C�digo de motivo */
       TB_ELEMENT           Descripcion, /* Descripci�n */
       TB_INGLES            Descripcion, /* Ingl�s */
       TB_NUMERO            Pesos,      /* N�mero */
       TB_TEXTO             Descripcion, /* Texto de uso general */
       LLAVE                Int IDENTITY (1, 1)
)
Go


/* ALTER MOT_CHECA: Motivo de Checadas Manuales(2/2) */
/* Patch 415 # Seq: X Agregado */
ALTER TABLE MOT_CHECA
       ADD CONSTRAINT PK_MOT_CHECA PRIMARY KEY (TB_CODIGO)
GO

/* V_DISPOSIT: Lista de Dispositivos*/
/* Patch 415 # Seq: X Agregado */
CREATE VIEW V_DISPOSIT(
		DI_NOMBRE,
		DI_TIPO,
 		DI_DESCRIP,
                DI_NOTA,
                DI_IP )

as SELECT DI_NOMBRE,
	  DI_TIPO ,
	  DI_DESCRIP,
	  DI_NOTA ,
	  DI_IP
      FROM #COMPARTE.dbo.DISPOSIT
GO

/* V_DISXCOM: Dispositivos por empresa*/
/* Patch 415 # Seq: X Agregado */
CREATE VIEW V_DISXCOM(
		 CM_CODIGO,
		 DI_NOMBRE,
		 DI_TIPO )

as
      SELECT CM_CODIGO,
		DI_NOMBRE ,
		DI_TIPO

      FROM #COMPARTE.dbo.DISXCOM
GO

/* INIT CUR_REV: Stored procedure para actualizar los cursos con revision (1/3)*/
/* Patch 415 # Seq: X  Agregado */
CREATE PROCEDURE SP_INI_CUR_REV
as
begin
	set NOCOUNT ON
	Declare @CU_CODIGO varchar(6)
	Declare @CU_REVISIO varchar(10)
	Declare @CU_FEC_REV datetime
	Declare @Cuantos Int
	
	declare TmpCosto cursor for
	select CU_CODIGO,
		   CU_REVISIO,
		   CU_FEC_REV 
		   from CURSO

	Open TmpCosto
	Fetch Next From TmpCosto Into 
		@CU_CODIGO,
		@CU_REVISIO,
		@CU_FEC_REV

	while @@Fetch_Status = 0
	begin
		if @CU_REVISIO<>''
		begin
			 select @Cuantos = count(*) from CUR_REV where ( @CU_CODIGO = CU_CODIGO and @CU_REVISIO = CH_REVISIO AND @CU_FEC_REV = CH_FECHA  )
			 IF @Cuantos = 0 
			 begin
				  insert into CUR_REV (CU_CODIGO, CH_REVISIO, CH_FECHA, CH_OBSERVA, US_CODIGO) values(@CU_CODIGO, @CU_REVISIO, @CU_FEC_REV, '', 0) 
			 end
		end
		else
		begin
			 if (@CU_FEC_REV <> '12/30/1899')
			 begin
				  select @Cuantos = count(*) from CUR_REV where ( @CU_CODIGO = CU_CODIGO and @CU_REVISIO = CH_REVISIO AND @CU_FEC_REV = CH_FECHA  )
				  IF @Cuantos = 0 
				  begin
						insert into CUR_REV (CU_CODIGO, CH_REVISIO, CH_FECHA, CH_OBSERVA, US_CODIGO) values(@CU_CODIGO,'1', @CU_FEC_REV, '', 0); 
						update CURSO set CU_REVISIO = '1' where CU_CODIGO = @CU_CODIGO;
				  end
			 end
	    end
		Fetch Next From TmpCosto Into
		@CU_CODIGO,
		@CU_REVISIO,
		@CU_FEC_REV
	end
  close TmpCosto
  deallocate TmpCosto

	return 
end
GO

/* INIT CUR_REV: Ejecutar el stored procedure para actualizar los cursos con revision (2/3)*/
/* Patch 415 # Seq: X Agregado */
exec SP_INI_CUR_REV;
Go

/* INIT CUR_REV: Borrar el stored procedure para actualizar los cursos con revision (3/3)*/
/* Patch 415 # Seq: X Agregado */
drop procedure SP_INI_CUR_REV;
GO


/*VACACION.VA_AJUSTE: �Es Ajuste de Sistema? */
/*Patch 415:  # Seq: X Agregado */
alter table VACACION add VA_AJUSTE Booleano
GO

/*Inicializaci�n de VACACION.VA_AJUSTE (1/2) */
/*Patch 415:  # Seq: X Agregado */
update VACACION set VA_AJUSTE = 'N' where ( VA_AJUSTE is Null )
GO

/*Inicializaci�n de VACACION.VA_AJUSTE (2/2) */
/*Patch 415:  # Seq: X Agregado */
update VACACION set VA_AJUSTE = 'S' where VA_COMENTA = ' AJUSTE DE SALDO AL REINGRESO'
GO

/* Tabla V_COMPANY: Considerar nuevos campos ( PRO Enviar cartas de Kiosco  )*/
/* Patch 415 # Seq: X Agregado */
ALTER VIEW V_COMPANY(
					CM_CODIGO,
					CM_NOMBRE,
					CM_ACUMULA,
					CM_CONTROL,
					CM_EMPATE,
					CM_ALIAS,
					CM_USRNAME,
					CM_USACAFE,
					CM_NIVEL0,
					CM_DATOS,
					CM_DIGITO,
					CM_KCLASIFI,
          CM_KCONFI,
          CM_KUSERS
					 )
AS
SELECT
		CM_CODIGO,
		CM_NOMBRE,
		CM_ACUMULA,
		CM_CONTROL,
		CM_EMPATE,
		CM_ALIAS,
		CM_USRNAME,
		CM_USACAFE,
		CM_NIVEL0,
		CM_DATOS,
		CM_DIGITO,
		CM_KCLASIFI,
    CM_KCONFI,
    CM_KUSERS
FROM #COMPARTE.dbo.COMPANY
GO

/* Tabla LIQ_IMSS: Importe de Parcialidad ( Decreto 20% SUA 3.2.6 )*/
/* Patch 415 # Seq: X Agregado */
alter table LIQ_IMSS add LS_PARCIAL PesosEmpresa
GO

/* Tabla LIQ_IMSS: Aplica Financiamiento ( Decreto 20% SUA 3.2.6 )*/
/* Patch 415 # Seq: X Agregado */
alter table LIQ_IMSS add LS_FINAN Booleano
GO

/* Tabla LIQ_IMSS: Se Aplico Decreto 20% ( Decreto 20% SUA 3.2.6 )*/
/* Patch 415 # Seq: X Agregado */
alter table LIQ_IMSS add LS_DES_20 Booleano
GO

/* LIQ_IMSS.LS_PARCIAL: Inicializaci�n ( Decreto 20% SUA 3.2.6 )*/
/* Patch 415 # Seq: X Agregado */
update LIQ_IMSS set LS_PARCIAL = 0 where ( LS_PARCIAL is Null )
GO

/* LIQ_IMSS.LS_FINAN: Inicializaci�n ( Decreto 20% SUA 3.2.6 )*/
/* Patch 415 # Seq: X Agregado */
update LIQ_IMSS set LS_FINAN = 'N' where ( LS_FINAN is Null )
GO

/* LIQ_IMSS.LS_DES_20: Inicializaci�n ( Decreto 20% SUA 3.2.6 )*/
/* Patch 415 # Seq: X Agregado */
update LIQ_IMSS set LS_DES_20 = 'N' where ( LS_DES_20 is Null )
GO

/* ********************* Tablas nuevas para la Version Fiscal 2010 ********************************* */

/*COMPARA.CP_CALCULO: Campo para Identificar si se realiz� el Calculo de Comparativo ISPT  ( Cierre declaracion anual )*/
/* Patch 415 # Seq: X Agregado */
alter table COMPARA ADD CP_CALCULO Booleano
GO

/* COMPARA.CP_CALCULO: Inicializaci�n ( Cierre declaracion anual )*/
/* Patch 415 # Seq: X Agregado */
update COMPARA set CP_CALCULO = 'N' where ( CP_CALCULO is Null )
GO

/*TMPDIMMTOT: Tabla para la sugerencia del Candado en la Declaracion Anual
/* NOTA: Falta agregar el constraint entre TMPDIMMTOT y TMPDIMM */
/* Patch 415 # Seq: X Agregado */
CREATE TABLE TMPDIMMTOT (
	US_CODIGO Usuario NOT NULL ,
	TD_YEAR Anio NOT NULL ,
	TD_STATUS Status NOT NULL,
  LLAVE     Int IDENTITY (1, 1))
GO

/* TMPDIMMTOT: Llave Primaria */
/* Patch 415 # Seq: X Agregado */
ALTER TABLE TMPDIMMTOT
       ADD CONSTRAINT PK_TMPDIMMTOT PRIMARY KEY (US_CODIGO, TD_YEAR)

GO

/*TMPDIMM.TD_DATA_A1: Parametro Opcional #4 */
/* Patch 415 # Seq: X Agregado */
alter table TMPDIMM ADD TD_DATA_A1 Pesos
GO

/*TMPDIMM.TD_DATA_A1: Inicializaci�n */
/* Patch 415 # Seq: X Agregado */
update TMPDIMM set TD_DATA_A1 = 0 where ( TD_DATA_A1 is Null )
GO

/*TMPDIMM.TD_DATA_A2: Parametro Opcional #5 */
/* Patch 415 # Seq: X Agregado */
alter table TMPDIMM ADD TD_DATA_A2 Pesos
GO

/*TMPDIMM.TD_DATA_A2: Inicializaci�n */
/* Patch 415 # Seq: X Agregado */
update TMPDIMM set TD_DATA_A2 = 0 where ( TD_DATA_A2 is Null )
GO

/*TMPDIMM.TD_TEXT_01: Apellido Paterno */
/* Patch 415 # Seq: X Agregado */
alter table TMPDIMM ADD TD_TEXT_01 Formula
GO

/*TMPDIMM.TD_TEXT_01: Inicializaci�n */
/* Patch 415 # Seq: X Agregado */
update TMPDIMM set TD_TEXT_01 = '' where ( TD_TEXT_01 is Null )
GO

/*TMPDIMM.TD_TEXT_02: Apellido Materno */
/* Patch 415 # Seq: X Agregado */
alter table TMPDIMM ADD TD_TEXT_02 Formula
GO

/*TMPDIMM.TD_TEXT_02: Inicializaci�n */
/* Patch 415 # Seq: X Agregado */
update TMPDIMM set TD_TEXT_02 = '' where ( TD_TEXT_02 is Null )
GO

/*TMPDIMM.TD_TEXT_03: Nombres */
/* Patch 415 # Seq: X Agregado */
alter table TMPDIMM ADD TD_TEXT_03 Formula
GO

/*TMPDIMM.TD_TEXT_03: Inicializaci�n */
/* Patch 415 # Seq: X Agregado */
update TMPDIMM set TD_TEXT_03 = '' where ( TD_TEXT_03 is Null )
GO

/*TMPDIMM.TD_TEXT_04: Texto Opcional #1 */
/* Patch 415 # Seq: X Agregado */
alter table TMPDIMM ADD TD_TEXT_04 Formula
GO

/*TMPDIMM.TD_TEXT_04: Inicializaci�n */
/* Patch 415 # Seq: X Agregado */
update TMPDIMM set TD_TEXT_04 = '' where ( TD_TEXT_04 is Null )
GO

/*TMPDIMM.TD_TEXT_05: Texto Opcional #2 */
/* Patch 415 # Seq: X Agregado */
alter table TMPDIMM ADD TD_TEXT_05 Formula
GO

/*TMPDIMM.TD_TEXT_05: Inicializaci�n */
/* Patch 415 # Seq: X Agregado */
update TMPDIMM set TD_TEXT_05 = '' where ( TD_TEXT_05 is Null )
GO

/*TMPDIMM.TD_SELLO: Sello Digital */
/* Patch 415 # Seq: X Agregado */
alter table TMPDIMM ADD TD_SELLO Memo
GO

/*TMPDIMM.TD_SELLO: Inicializaci�n */
/* Patch 415 # Seq: X Agregado */
update TMPDIMM set TD_SELLO = '' where ( TD_SELLO is Null )
GO

/* RPATRON.TB_CLASE: Clase de Riesgo */
/* Patch 415 # Seq: X Agregado */
alter table RPATRON add TB_CLASE Status
GO

/* RPATRON.TB_CLASE: Inicializaci�n */
/* Patch 415 # Seq: X Agregado */
update RPATRON set TB_CLASE = 0 where ( TB_CLASE is Null )
GO

/* RPATRON.TB_FRACCION: Fracci�n */
/* Patch 415 # Seq: X Agregado */
alter table RPATRON add TB_FRACCION FolioGrande
GO

/* RPATRON.TB_FRACCION: Inicializaci�n */
/* Patch 415 # Seq: X Agregado */
update RPATRON set TB_FRACCION = 0 where ( TB_FRACCION is Null )
GO

/* RPATRON.TB_STYPS: �Acreditaci�n por la secretar�a de trabajo y previsi�n social? */
/* Patch 415 # Seq: X Agregado */
alter table RPATRON add TB_STYPS Booleano
GO

/* RPATRON.TB_STYPS: Inicializaci�n */
/* Patch 415 # Seq: X Agregado */
update RPATRON set TB_STYPS = 'N' where ( TB_STYPS is Null )
GO

/* Tabla PRIESGO: Prima de Riesgo anterior */
/* Patch 415 # Seq: X Agregado */
alter table PRIESGO add RT_PTANT Tasa
GO

/* PRIESGO.RT_PTANT: Inicializaci�n */
/* Patch 415 # Seq: X Agregado */
update PRIESGO set RT_PTANT = 0 where ( RT_PTANT is Null )
GO

/* Tabla PRIESGO: D�as subsideados por incapacidad temporal */
/* Patch 415 # Seq: X Agregado */
alter table PRIESGO add RT_S DiasFrac
GO

/* PRIESGO.RT_S: Inicializaci�n */
/* Patch 415 # Seq: X Agregado */
update PRIESGO set RT_S = 0 where ( RT_S is Null )
GO

/* Tabla PRIESGO: Suma de Incapacidades Permanentes */
/* Patch 415 # Seq: X Agregado */
alter table PRIESGO add RT_I Tasa
GO

/* PRIESGO.RT_I: Inicializaci�n */
/* Patch 415 # Seq: X Agregado */
update PRIESGO set RT_I = 0 where ( RT_I is Null )
GO

/* Tabla PRIESGO: N�mero de Defunciones */
/* Patch 415 # Seq: X Agregado */
alter table PRIESGO add RT_D Numerico
GO

/* PRIESGO.RT_D: Inicializaci�n */
/* Patch 415 # Seq: X Agregado */
update PRIESGO set RT_D = 0 where ( RT_D is Null )
GO


/* Tabla PRIESGO: Trabajadores Promedio expuestos */
/* Patch 415 # Seq: X Agregado */
alter table PRIESGO add RT_N Tasa
GO

/* PRIESGO.RT_N: Inicializaci�n */
/* Patch 415 # Seq: X Agregado */
update PRIESGO set RT_N = 0 where ( RT_N is Null )
GO

/* Tabla PRIESGO: Prima de Riesgo M�nima */
/* Patch 415 # Seq: X Agregado */
alter table PRIESGO add RT_M Tasa
GO

/* PRIESGO.RT_M: Inicializaci�n */
/* Patch 415 # Seq: X Agregado */
update PRIESGO set RT_M = 0 where ( RT_M is Null )
GO


/* Tabla PRIESGO: Factor de Prima */
/* Patch 415 # Seq: X Agregado */
alter table PRIESGO add RT_F Numerico
GO

/* PRIESGO.RT_F: Inicializaci�n */
/* Patch 415 # Seq: X Agregado */
update PRIESGO set RT_F = 0 where ( RT_F is Null )
GO



/* ********************* Termina Cambios para la Version Fiscal 2010 ********************************* */

/*****************************************/
/**** Stored Procedures *******************/
/*****************************************/

/* SP_CLAS_AUSENCIA: Actualizar el tipo de n�mina en la tarjeta ( PRO - Cambio de tipo de n�mina )*/
/* Patch 415 # Seq: 153 Modificado*/
ALTER PROCEDURE SP_CLAS_AUSENCIA @Empleado Int, @Fecha DateTime AS
BEGIN
       SET NOCOUNT ON
	Declare @FEC_KAR DateTime
	Declare @PUESTO Char(6)
	Declare @CLASIFI Char(6)
	Declare @TURNO Char(6)
        Declare @NIVEL1 Char(6)
	Declare @NIVEL2 Char(6)
	Declare @NIVEL3 Char(6)
	Declare @NIVEL4 Char(6)
	Declare @NIVEL5 Char(6)
        Declare @NIVEL6 Char(6)
	Declare @NIVEL7 Char(6)
	Declare @NIVEL8 Char(6)
	Declare @NIVEL9 Char(6)
        Declare @SALARIO PesosDiario

       	declare @T_PUESTO Char(6)
	declare @T_CLASIFI Char(6)
	declare @T_TURNO Char(6)
        declare @T_NIVEL1 Char(6)
	declare @T_NIVEL2 Char(6)
	declare @T_NIVEL3 Char(6)
	declare @T_NIVEL4 Char(6)
	declare @T_NIVEL5 Char(6)
        declare @T_NIVEL6 Char(6)
	declare @T_NIVEL7 Char(6)
	declare @T_NIVEL8 Char(6)
	declare @T_NIVEL9 Char(6)
	declare @NOMINA smallint

 	select @FEC_KAR = CB_FEC_KAR,
		@PUESTO = CB_PUESTO,
		@CLASIFI = CB_CLASIFI,
		@TURNO = CB_TURNO,
         	@NIVEL1 = CB_NIVEL1,
		@NIVEL2 = CB_NIVEL2,
		@NIVEL3 = CB_NIVEL3,
		@NIVEL4 = CB_NIVEL4,
		@NIVEL5 = CB_NIVEL5,
         	@NIVEL6 = CB_NIVEL6,
		@NIVEL7 = CB_NIVEL7,
		@NIVEL8 = CB_NIVEL8,
		@NIVEL9 = CB_NIVEL9,
                @SALARIO = CB_SALARIO,
 		@NOMINA = CB_NOMINA
  	from   	COLABORA
  	where  CB_CODIGO = @Empleado

  	if ( @FECHA < @FEC_KAR )
  	begin
 		select @PUESTO = CB_PUESTO,
			@CLASIFI = CB_CLASIFI,
			@TURNO = CB_TURNO,
         		@NIVEL1 = CB_NIVEL1,
			@NIVEL2 = CB_NIVEL2, 
			@NIVEL3 = CB_NIVEL3, 
			@NIVEL4 = CB_NIVEL4, 
			@NIVEL5 = CB_NIVEL5,
         		@NIVEL6 = CB_NIVEL6, 
			@NIVEL7 = CB_NIVEL7, 
			@NIVEL8 = CB_NIVEL8, 
			@NIVEL9 = CB_NIVEL9,
               @SALARIO = CB_SALARIO,
			@NOMINA = CB_NOMINA
    		from   SP_FECHA_KARDEX( @Fecha, @Empleado )
  	end

 	select @FEC_KAR = AU_FECHA,
	       @T_PUESTO = CB_PUESTO,
	       @T_CLASIFI = CB_CLASIFI,
	       @T_TURNO = CB_TURNO,
               @T_NIVEL1 = CB_NIVEL1,
	       @T_NIVEL2 = CB_NIVEL2,
	       @T_NIVEL3 = CB_NIVEL3,
	       @T_NIVEL4 = CB_NIVEL4,
	       @T_NIVEL5 = CB_NIVEL5,
               @T_NIVEL6 = CB_NIVEL6,
	       @T_NIVEL7 = CB_NIVEL7,
	       @T_NIVEL8 = CB_NIVEL8,
	       @T_NIVEL9 = CB_NIVEL9
  	from CLASITMP where
        ( CB_CODIGO = @Empleado ) and
        ( AU_FECHA = @Fecha ) and
        ( US_COD_OK <> 0 );
        if ( @FEC_KAR is not NULL )
        begin
             if ( @T_PUESTO <> '' )
             begin
                  select @PUESTO = @T_PUESTO;
             end
             if ( @T_CLASIFI <> '' )
             begin
                  select @CLASIFI = @T_CLASIFI;
             end
             if ( @T_TURNO <> '' )
             begin
                  select @TURNO = @T_TURNO;
             end
             if ( @T_NIVEL1 <> '' )
             begin
                  select @NIVEL1 = @T_NIVEL1;
             end
             if ( @T_NIVEL2 <> '' )
             begin
                  select @NIVEL2 = @T_NIVEL2;
             end
             if ( @T_NIVEL3 <> '' )
             begin
                  select @NIVEL3 = @T_NIVEL3;
             end
             if ( @T_NIVEL4 <> '' )
             begin
                  select @NIVEL4 = @T_NIVEL4;
             end
             if ( @T_NIVEL5 <> '' )
             begin
                  select @NIVEL5 = @T_NIVEL5;
             end
             if ( @T_NIVEL6 <> '' )
             begin
                  select @NIVEL6 = @T_NIVEL6;
             end
             if ( @T_NIVEL7 <> '' )
             begin
                  select @NIVEL7 = @T_NIVEL7;
             end
             if ( @T_NIVEL8 <> '' )
             begin
                  select @NIVEL8 = @T_NIVEL8;
             end
             if ( @T_NIVEL9 <> '' )
             begin
                  select @NIVEL9 = @T_NIVEL9;
             end
        end

  	update AUSENCIA
  	set   CB_PUESTO  = @PUESTO,
        	CB_CLASIFI = @CLASIFI,
        	CB_TURNO   = @TURNO,
        	CB_NIVEL1  = @NIVEL1,
        	CB_NIVEL2  = @NIVEL2,
        	CB_NIVEL3  = @NIVEL3,
        	CB_NIVEL4  = @NIVEL4,
        	CB_NIVEL5  = @NIVEL5,
        	CB_NIVEL6  = @NIVEL6,
        	CB_NIVEL7  = @NIVEL7,
        	CB_NIVEL8  = @NIVEL8,
        	CB_NIVEL9  = @NIVEL9,
          CB_SALARIO = @SALARIO,
          CB_NOMINA = @NOMINA
  	where CB_CODIGO = @Empleado and AU_FECHA = @Fecha;

        EXECUTE SP_CLAS_AUSENCIA_ADICIONAL @Empleado, @Fecha;
END;
GO


/* RECALCULA_KARDEX: Considerar el cambio de tipo de n�mina en la fecha del �ltimo kardex 
				( PRO - Cambio de tipo de n�mina )*/
/* Patch 415 # Seq: 165 Modificado*/
ALTER PROCEDURE RECALCULA_KARDEX(@Empleado INTEGER) AS
BEGIN
   SET NOCOUNT ON
   declare @kAUTOSAL  CHAR(1);
   declare @kClasifi  CHAR(6);
   declare @kCONTRAT  CHAR(1);
   declare @kFAC_INT  NUMERIC(15,5);
   declare @kFEC_ANT  Fecha;
   declare @kFEC_CON  Fecha;
   declare @kFEC_ING  Fecha;
   declare @kFEC_INT  Fecha;
   declare @kFEC_REV  Fecha;
   declare @kFECHA_2  Fecha;
   declare @kFECHA    Fecha;
   declare @kMOT_BAJ  CHAR(3);
   declare @kNivel1   CHAR(6);
   declare @kNivel2   CHAR(6);
   declare @kNivel3   CHAR(6);
   declare @kNivel4   CHAR(6);
   declare @kNivel5   CHAR(6);
   declare @kNivel6   CHAR(6);
   declare @kNivel7   CHAR(6);
   declare @kNivel8   CHAR(6);
   declare @kNivel9   CHAR(6);
   declare @kNOMNUME  SMALLINT;
   declare @kNOMTIPO  SMALLINT;
   declare @kNOMYEAR  SMALLINT;
   declare @kPatron   CHAR(1);
   declare @kPER_VAR  NUMERIC(15,2);
   declare @kPuesto   CHAR(6);
   declare @kSAL_INT  NUMERIC(15,2);
   declare @kSALARIO  NUMERIC(15,2);
   declare @kTABLASS  CHAR(1);
   declare @kTurno    CHAR(6);
   declare @kZona_GE  CHAR(1);
   declare @kOLD_SAL  NUMERIC(15,2);
   declare @kOLD_INT  NUMERIC(15,2);
   declare @kOTRAS_P  NUMERIC(15,2);
   declare @kPRE_INT  NUMERIC(15,2);
   declare @kREINGRE  Char(1);
   declare @kSAL_TOT  NUMERIC(15,2);
   declare @kTOT_GRA  NUMERIC(15,2);
   declare @kTIPO     CHAR(6);
   declare @kRANGO_S  NUMERIC(15,2);
   declare @kMOD_NV1  CHAR(1);
   declare @kMOD_NV2  CHAR(1);
   declare @kMOD_NV3  CHAR(1);
   declare @kMOD_NV4  CHAR(1);
   declare @kMOD_NV5  CHAR(1);
   declare @kMOD_NV6  CHAR(1);
   declare @kMOD_NV7  CHAR(1);
   declare @kMOD_NV8  CHAR(1);
   declare @kMOD_NV9  CHAR(1);
   declare @kPLAZA    FolioGrande;
   declare @kNOMINA   SMALLINT;
   declare @kRECONTR  Booleano;
   declare @kFEC_COV  Fecha;

   declare @eACTIVO   CHAR(1);
   declare @eAUTOSAL  CHAR(1);
   declare @eClasifi  CHAR(6);
   declare @eCONTRAT  CHAR(1);
   declare @eFAC_INT  NUMERIC(15,5);
   declare @eFEC_ANT  Fecha;
   declare @eFEC_CON  Fecha;
   declare @eFEC_ING  Fecha;
   declare @eFEC_INT  Fecha;
   declare @eFEC_REV  Fecha;
   declare @eFEC_SAL  Fecha;
   declare @eFEC_BAJ  Fecha;
   declare @eFEC_BSS  Fecha;
   declare @eMOT_BAJ  CHAR(3);
   declare @eNivel1   CHAR(6);
   declare @eNivel2   CHAR(6);
   declare @eNivel3   CHAR(6);
   declare @eNivel4   CHAR(6);
   declare @eNivel5   CHAR(6);
   declare @eNivel6   CHAR(6);
   declare @eNivel7   CHAR(6);
   declare @eNivel8   CHAR(6);
   declare @eNivel9   CHAR(6);
   declare @eNOMNUME  SMALLINT;
   declare @eNOMTIPO  SMALLINT;
   declare @eNOMYEAR  SMALLINT;
   declare @ePatron   CHAR(1);
   declare @ePER_VAR  NUMERIC(15,2);
   declare @ePuesto   CHAR(6);
   declare @eSAL_INT  NUMERIC(15,2);
   declare @eSALARIO  NUMERIC(15,2);
   declare @eTABLASS  CHAR(1);
   declare @eTIP_REV  CHAR(6);
   declare @eTurno    CHAR(6);
   declare @eZona_GE  CHAR(1);
   declare @eOLD_SAL  NUMERIC(15,2);
   declare @eOLD_INT  NUMERIC(15,2);
   declare @eOTRAS_P  NUMERIC(15,2);
   declare @ePRE_INT  NUMERIC(15,2);
   declare @eREINGRE  Char(1);
   declare @eSAL_TOT  NUMERIC(15,2);
   declare @eTOT_GRA  NUMERIC(15,2);
   declare @eRANGO_S  NUMERIC(15,2);
   declare @eFEC_NIV  Fecha;
   declare @eFEC_PTO  Fecha;
   declare @eFEC_TUR  Fecha;
   declare @FechaVacia Fecha;
   declare @eFEC_KAR  Fecha;
   declare @ePLAZA    FolioGrande;
   declare @eFEC_PLA  Fecha;
   declare @eNOMINA   SMALLINT;
   declare @eFEC_NOM  Fecha;
   declare @eRECONTR  Booleano;
   declare @eFEC_COV  Fecha;

   declare CursorKardex CURSOR SCROLL_LOCKS FOR
   select	CB_AUTOSAL, CB_CLASIFI,   CB_CONTRAT, CB_FAC_INT, CB_FEC_ANT,
                CB_FEC_CON, CB_FEC_ING,   CB_FEC_INT, CB_FEC_REV, CB_FECHA,    CB_FECHA_2 ,
                CB_MOT_BAJ, CB_NIVEL1,    CB_NIVEL2 , CB_NIVEL3 , CB_NIVEL4,   CB_NIVEL5 ,   CB_NIVEL6 ,
                CB_NIVEL7 , CB_NIVEL8,    CB_NIVEL9 , CB_NOMNUME ,CB_NOMTIPO , CB_NOMYEAR ,
                CB_OTRAS_P, CB_PATRON ,   CB_PER_VAR ,CB_PRE_INT ,
                CB_PUESTO , CB_REINGRE ,  CB_SAL_INT ,CB_SAL_TOT, CB_SALARIO,  CB_TABLASS ,
                CB_TIPO ,   CB_TOT_GRA,   CB_TURNO ,  CB_ZONA_GE, CB_RANGO_S, CB_PLAZA,
                CB_NOMINA,  CB_RECONTR, CB_FEC_COV
   from Kardex
   where CB_CODIGO = @Empleado
   order by cb_fecha,cb_nivel

      	SET @FechaVacia = '12/30/1899';
        SET @eACTIVO  = 'S';
      	SET @eFEC_BAJ = @FechaVacia;
      	SET @eFEC_BSS = @FechaVacia;
      	SET @eMOT_BAJ = ' ';
      	SET @eNOMYEAR = 0;
      	SET @eNOMTIPO = 0;
      	SET @eNOMNUME = 0;
      	SET @eREINGRE = 'N';
      	SET @eSALARIO = 0;
      	SET @eSAL_INT = 0;
      	SET @eFAC_INT = 0;
      	SET @ePRE_INT = 0;
      	SET @ePER_VAR = 0;
      	SET @eSAL_TOT = 0;
      	SET @eAUTOSAL = 'N';
      	SET @eTOT_GRA = 0;
      	SET @eFEC_REV = @FechaVacia;
      	SET @eFEC_INT = @FechaVacia;
      	SET @eOTRAS_P = 0;
      	SET @eOLD_SAL = 0;
      	SET @eOLD_INT = 0;
      	SET @eRANGO_S = 0;
        SET @eFEC_PLA = @FechaVacia;
        SET @eFEC_NOM = @FechaVacia;
        SET @eRECONTR = 'S';
        SET @eFEC_COV = @FechaVacia;

	Open CursorKardex
	Fetch NEXT from CursorKardex
	into	@kAUTOSAL, @kCLASIFI, @kCONTRAT,  @kFAC_INT,  @kFEC_ANT ,
      @kFEC_CON, @kFEC_ING, @kFEC_INT , @kFEC_REV  ,@kFECHA,   @kFECHA_2,
      @kMOT_BAJ ,@kNivel1,  @kNivel2 ,  @kNivel3,   @kNivel4,  @kNivel5,  @kNivel6,
      @kNivel7,  @kNivel8,  @kNivel9,   @kNOMNUME , @kNOMTIPO, @kNOMYEAR,
      @kOTRAS_P, @kPatron,  @kPER_VAR,  @kPRE_INT,
      @kPuesto,  @kREINGRE, @kSAL_INT,  @kSAL_TOT,  @kSALARIO, @kTABLASS,
      @kTIPO,    @kTOT_GRA, @kTurno,    @kZONA_GE,  @kRANGO_S, @kPLAZA,
      @kNOMINA,  @kRECONTR, @kFEC_COV


	if ( @@FETCH_STATUS <> 0 )
	BEGIN
		Close CursorKardex
		Deallocate CursorKardex
		RETURN
	END

	
	if ( @kTIPO <> 'ALTA' )
	BEGIN
		Close CursorKardex
		Deallocate CursorKardex
		RAISERROR( 'Empleado #%d, ERROR en KARDEX: Movimiento(s) previo(s) a la ALTA', 16, 1, @Empleado )
		RETURN
	END

	while ( @@FETCH_STATUS = 0 )
	BEGIN
        	if ( @kTIPO = 'ALTA' OR @kTIPO = 'CAMBIO' OR @kTIPO = 'CIERRE' )
          	BEGIN
			if ABS( @kSALARIO - @eSALARIO ) >= 0.01
			BEGIN
				SET @kOLD_SAL = @eSALARIO;
				SET @kFEC_REV = @kFECHA;
				SET @eFEC_REV = @kFEC_REV;
				SET @eSALARIO = @kSALARIO;
				SET @eOLD_SAL = @kOLD_SAL;
			END
			else
			BEGIN
				SET @kOLD_SAL = @eOLD_SAL;
				SET @kFEC_REV = @eFEC_REV;
			END
			if ABS( @kSAL_INT - @eSAL_INT ) >= 0.01
			BEGIN
				SET @kOLD_INT = @eSAL_INT;
				if ( @kTIPO = 'CAMBIO' )
					SET @kFEC_INT = @kFECHA_2;
				else
					SET @kFEC_INT = @kFECHA;
				SET @eFEC_INT = @kFEC_INT;
				SET @eSAL_INT = @kSAL_INT;
				SET @eOLD_INT = @kOLD_INT;
			END
			else
			BEGIN
                             SET @kOLD_INT = @eOLD_INT;
	                     SET @kFEC_INT = @eFEC_INT;
			END
			SET @eFAC_INT = @kFAC_INT;
			SET @ePRE_INT = @kPRE_INT;
			SET @ePER_VAR = @kPER_VAR;
			SET @eSAL_TOT = @kSAL_TOT;
			SET @eTOT_GRA = @kTOT_GRA;
			SET @eOTRAS_P = @kOTRAS_P;
			SET @eZONA_GE = @kZONA_GE;
			SET @eTIP_REV = @kTIPO;
			SET @eFEC_SAL = @kFECHA;
			SET @eRANGO_S = @kRANGO_S;
		END
		else
		BEGIN
			SET @kSALARIO = @eSALARIO;
			SET @kOLD_SAL = @eOLD_SAL;
			SET @kFEC_REV = @eFEC_REV;
			SET @kSAL_INT = @eSAL_INT;
			SET @kOLD_INT = @eOLD_INT;
			SET @kFEC_INT = @eFEC_INT;
			SET @kFAC_INT = @eFAC_INT;
			SET @kPRE_INT = @ePRE_INT;
			SET @kPER_VAR = @ePER_VAR;
			SET @kSAL_TOT = @eSAL_TOT;
			SET @kTOT_GRA = @eTOT_GRA;
			SET @kOTRAS_P = @eOTRAS_P;
			SET @kZONA_GE = @eZONA_GE;
			SET @kRANGO_S = @eRANGO_S;
		END

		if ( @kTIPO = 'ALTA' or @kTIPO = 'PUESTO' or @kTIPO = 'CAMBIO' or @kTIPO = 'CIERRE' )
			SET @eAUTOSAL = @kAUTOSAL;
		else
			SET @kAUTOSAL = @eAUTOSAL;

		if ( @kAUTOSAL <> 'S' )
		BEGIN
			SET @kRANGO_S = 0;
			SET @eRANGO_S = 0;
		END

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'PUESTO' OR @kTIPO = 'CIERRE' OR @kTIPO = 'PLAZA' )
		BEGIN
			SET @ePUESTO   = @kPUESTO;
			SET @eFEC_PTO  = @kFECHA;
                        if ( @kTIPO = 'PLAZA' AND @eAUTOSAL = 'S' )
                             SET @kCLASIFI = @eCLASIFI;
                        else
			     SET @eCLASIFI  = @kCLASIFI;
		END
		else
		BEGIN
			SET @kPUESTO   = @ePUESTO;
			SET @kCLASIFI  = @eCLASIFI;
		END

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'AREA' OR @kTIPO = 'CIERRE' OR @kTIPO = 'PLAZA' )
		BEGIN
			if ( ( @kTIPO = 'AREA' ) OR ( @kTIPO = 'CIERRE' ) OR ( @kTIPO = 'PLAZA' ))
			BEGIN
				if ( @kNIVEL1 <> @eNIVEL1 )
     					SET @kMOD_NV1 = 'S';
            			else
                 			SET @kMOD_NV1 = 'N';
				if ( @kNIVEL2 <> @eNIVEL2 )
     					SET @kMOD_NV2 = 'S';
            			else
                 			SET @kMOD_NV2 = 'N';
				if ( @kNIVEL3 <> @eNIVEL3 )
     					SET @kMOD_NV3 = 'S';
            			else
                 			SET @kMOD_NV3 = 'N';
				if ( @kNIVEL4 <> @eNIVEL4 )
     					SET @kMOD_NV4 = 'S';
            			else
                 			SET @kMOD_NV4 = 'N';
				if ( @kNIVEL5 <> @eNIVEL5 )
     					SET @kMOD_NV5 = 'S';
            			else
                 			SET @kMOD_NV5 = 'N';
				if ( @kNIVEL6 <> @eNIVEL6 )
     					SET @kMOD_NV6 = 'S';
            			else
                 			SET @kMOD_NV6 = 'N';
				if ( @kNIVEL7 <> @eNIVEL7 )
     					SET @kMOD_NV7 = 'S';
            			else
                 			SET @kMOD_NV7 = 'N';
				if ( @kNIVEL8 <> @eNIVEL8 )
     					SET @kMOD_NV8 = 'S';
            			else
                 			SET @kMOD_NV8 = 'N';
				if ( @kNIVEL9 <> @eNIVEL9 )
     					SET @kMOD_NV9 = 'S';
            			else
                 			SET @kMOD_NV9 = 'N';
			END
			else
			BEGIN
                                if ( @kNIVEL1 <> '' )
                                    SET @kMOD_NV1 = 'S';
                                else
                                    SET @kMOD_NV1 = 'N';
                                if ( @kNIVEL2 <> '' )
                                    SET @kMOD_NV2 = 'S';
                                else
                                    SET @kMOD_NV2 = 'N';
                                if ( @kNIVEL3 <> '' )
                                    SET @kMOD_NV3 = 'S';
                                else
                                    SET @kMOD_NV3 = 'N';
                                if ( @kNIVEL4 <> '' )
                                    SET @kMOD_NV4 = 'S';
                                else
                                    SET @kMOD_NV4 = 'N';
                                if ( @kNIVEL5 <> '' )
                                    SET @kMOD_NV5 = 'S';
                                else
                                    SET @kMOD_NV5 = 'N';
                                if ( @kNIVEL6 <> '' )
                                    SET @kMOD_NV6 = 'S';
                                else
                                    SET @kMOD_NV6 = 'N';
                                if ( @kNIVEL7 <> '' )
                                    SET @kMOD_NV7 = 'S';
                                else
                                    SET @kMOD_NV7 = 'N';
                                if ( @kNIVEL8 <> '' )
                                    SET @kMOD_NV8 = 'S';
                                else
                                    SET @kMOD_NV8 = 'N';
                                if ( @kNIVEL9 <> '' )
                                    SET @kMOD_NV9 = 'S';
                                else
                                    SET @kMOD_NV9 = 'N';

			END
			SET @eNIVEL1 = @kNIVEL1;
			SET @eNIVEL2 = @kNIVEL2;
			SET @eNIVEL3 = @kNIVEL3;
			SET @eNIVEL4 = @kNIVEL4;
			SET @eNIVEL5 = @kNIVEL5;
			SET @eNIVEL6 = @kNIVEL6;
			SET @eNIVEL7 = @kNIVEL7;
			SET @eNIVEL8 = @kNIVEL8;
			SET @eNIVEL9 = @kNIVEL9;
			SET @eFEC_NIV = @kFECHA;
		END
		else
		BEGIN
			SET @kNIVEL1 = @eNIVEL1;
			SET @kNIVEL2 = @eNIVEL2;
			SET @kNIVEL3 = @eNIVEL3;
			SET @kNIVEL4 = @eNIVEL4;
			SET @kNIVEL5 = @eNIVEL5;
			SET @kNIVEL6 = @eNIVEL6;
			SET @kNIVEL7 = @eNIVEL7;
			SET @kNIVEL8 = @eNIVEL8;
			SET @kNIVEL9 = @eNIVEL9;
                        SET @kMOD_NV1 = 'N';
                        SET @kMOD_NV2 = 'N';
                        SET @kMOD_NV3 = 'N';
                        SET @kMOD_NV4 = 'N';
                        SET @kMOD_NV5 = 'N';
                        SET @kMOD_NV6 = 'N';
                        SET @kMOD_NV7 = 'N';
                        SET @kMOD_NV8 = 'N';
                        SET @kMOD_NV9 = 'N';
		END

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'PRESTA' OR @kTIPO = 'CIERRE' )
			SET @eTABLASS = @kTABLASS;
		else
			SET @kTABLASS = @eTABLASS;

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'TURNO' OR @kTIPO = 'CIERRE' OR @kTIPO = 'PLAZA' )
		BEGIN
			SET @eTURNO   = @kTURNO;
			SET @eFEC_TUR = @kFECHA;
		END
		else
			SET @kTURNO   = @eTURNO;

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'TIPNOM' OR @kTIPO = 'CIERRE' )
		BEGIN
			SET @eNOMINA  = @kNOMINA;
			SET @eFEC_NOM = @kFECHA;
		END
		else
			SET @kNOMINA  = @eNOMINA;

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'RENOVA' OR @kTIPO = 'CIERRE' )
		BEGIN
			SET @eCONTRAT = @kCONTRAT;
			SET @eFEC_CON = @kFEC_CON;
			SET @eFEC_COV = @kFEC_COV;
		END
		else
		BEGIN
			SET @kCONTRAT = @eCONTRAT;
			SET @kFEC_CON = @eFEC_CON;
			SET @kFEC_COV = @eFEC_COV;
		END

		if ( @kTIPO = 'ALTA' )
		BEGIN
			if ( @eACTIVO = 'S' )
				SET @kREINGRE = 'N';
            		else
			BEGIN
				SET @kREINGRE = 'S';
				SET @eREINGRE = 'S';
			END

			SET @eFEC_ING = @kFECHA;
			SET @eFEC_ANT = @kFEC_ANT;
			SET @eACTIVO  = 'S';
			SET @ePATRON  = @kPATRON;
			SET @kFEC_ING = @eFEC_ING;
			SET @kFEC_ANT = @eFEC_ANT;
		END
		else
		BEGIN
			SET @kREINGRE = @eREINGRE;
			SET @kPATRON  = @ePATRON;
			SET @kFEC_ING = @eFEC_ING;
			SET @kFEC_ANT = @eFEC_ANT;
		END

		if ( @kTIPO = 'BAJA' )
		BEGIN
			SET @eFEC_BAJ  =  @kFECHA;
			SET @eFEC_BSS  =  @kFECHA_2;
			SET @eMOT_BAJ  =  @kMOT_BAJ;
			SET @eACTIVO   =  'N';
			SET @eNOMYEAR  =  @kNOMYEAR;
			SET @eNOMTIPO  =  @kNOMTIPO;
			SET @eNOMNUME  =  @kNOMNUME;
			SET @ePLAZA    =  0;
		END
		else
		BEGIN
			SET @kMOT_BAJ = @eMOT_BAJ;
			SET @kNOMYEAR = @eNOMYEAR;
			SET @kNOMTIPO = @eNOMTIPO;
			SET @kNOMNUME = @eNOMNUME;
		END

		if ( @kTIPO = 'ALTA'  OR @kTIPO = 'CIERRE' OR @kTIPO = 'PLAZA' )
		BEGIN
			SET @ePLAZA    = @kPLAZA;
			SET @eFEC_PLA  = @kFECHA;
		END
		else
		BEGIN
			SET @kPLAZA    = @ePLAZA;
		END

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'BAJA' )
		BEGIN
			if ( @kTIPO = 'ALTA' )
                        begin
                             SET @kRECONTR = 'S';
                             SET @eRECONTR = 'S';
                        end
                        else
                        begin
                             SET @eRECONTR = @kRECONTR;
                        end
		END
		else
		BEGIN
			SET @kRECONTR = @eRECONTR;
		END

		update KARDEX
     		set 	CB_AUTOSAL = @kAUTOSAL,
			CB_CLASIFI = @kCLASIFI,
			CB_CONTRAT = @kCONTRAT,
			CB_FEC_ANT = @kFEC_ANT,
			CB_FEC_CON = @kFEC_CON,
			CB_FEC_ING = @kFEC_ING,
			CB_FEC_INT = @kFEC_INT,
			CB_FEC_REV = @kFEC_REV,
			CB_FECHA_2 = @kFECHA_2,
			CB_MOT_BAJ = @kMOT_BAJ,
			CB_NIVEL1  = @kNIVEL1,
			CB_NIVEL2  = @kNIVEL2,
			CB_NIVEL3  = @kNIVEL3,
			CB_NIVEL4  = @kNIVEL4,
			CB_NIVEL5  = @kNIVEL5,
			CB_NIVEL6  = @kNIVEL6,
			CB_NIVEL7  = @kNIVEL7,
			CB_NIVEL8  = @kNIVEL8,
			CB_NIVEL9  = @kNIVEL9,
			CB_MOD_NV1 = @kMOD_NV1,
			CB_MOD_NV2 = @kMOD_NV2,
			CB_MOD_NV3 = @kMOD_NV3,
			CB_MOD_NV4 = @kMOD_NV4,
			CB_MOD_NV5 = @kMOD_NV5,
			CB_MOD_NV6 = @kMOD_NV6,
			CB_MOD_NV7 = @kMOD_NV7,
			CB_MOD_NV8 = @kMOD_NV8,
			CB_MOD_NV9 = @kMOD_NV9,
			CB_NOMNUME = @kNOMNUME,
			CB_NOMTIPO = @kNOMTIPO,
			CB_NOMYEAR = @kNOMYEAR,
			CB_OTRAS_P = @kOTRAS_P,
			CB_PATRON  = @kPATRON,
			CB_PER_VAR = @kPER_VAR,
			CB_PRE_INT = @kPRE_INT,
			CB_PUESTO  = @kPUESTO,
			CB_REINGRE = @kREINGRE,
			CB_SAL_INT = @kSAL_INT,
			CB_SAL_TOT = @kSAL_TOT,
			CB_SALARIO = @kSALARIO,
			CB_TABLASS = @kTABLASS,
			CB_TOT_GRA = @kTOT_GRA,
			CB_TURNO   = @kTURNO,
			CB_ZONA_GE = @kZONA_GE,
			CB_OLD_SAL = @kOLD_SAL,
			CB_OLD_INT = @kOLD_INT,
			CB_FAC_INT = @kFAC_INT,
			CB_RANGO_S = @kRANGO_S,
      CB_PLAZA   = @kPLAZA,
      CB_NOMINA  = @kNOMINA,
      CB_RECONTR = @kRECONTR,
      CB_FEC_COV = @kFEC_COV
		where CURRENT OF CursorKardex

		Fetch NEXT from CursorKardex
		into	@kAUTOSAL, @kCLASIFI, @kCONTRAT,  @kFAC_INT,  @kFEC_ANT ,
                        @kFEC_CON, @kFEC_ING, @kFEC_INT , @kFEC_REV  ,@kFECHA,   @kFECHA_2,
                        @kMOT_BAJ ,@kNivel1,  @kNivel2 ,  @kNivel3,   @kNivel4,  @kNivel5,  @kNivel6,
                        @kNivel7,  @kNivel8,  @kNivel9,   @kNOMNUME , @kNOMTIPO, @kNOMYEAR,
                        @kOTRAS_P, @kPatron,  @kPER_VAR,  @kPRE_INT,
                        @kPuesto,  @kREINGRE, @kSAL_INT,  @kSAL_TOT,  @kSALARIO, @kTABLASS,
                        @kTIPO,    @kTOT_GRA, @kTurno,    @kZONA_GE,  @kRANGO_S, @kPLAZA,
                        @kNOMINA,  @kRECONTR, @kFEC_COV
	END

	Close CursorKardex
	Deallocate CursorKardex

	SET @eFEC_KAR = @eFEC_ING;
	if ( @eFEC_NIV > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_NIV;
	if ( @eFEC_SAL > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_SAL;
	if ( @eFEC_PTO > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_PTO;
	if ( @eFEC_TUR > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_TUR;
	if ( @eFEC_PLA > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_PLA;
	if ( @eFEC_NOM > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_NOM;

	update COLABORA
	set CB_AUTOSAL = @eAUTOSAL,
            CB_ACTIVO  = @eACTIVO,
            CB_CLASIFI = @eCLASIFI,
            CB_CONTRAT = @eCONTRAT,
            CB_FAC_INT = @eFAC_INT,
            CB_FEC_ANT = @eFEC_ANT,
            CB_FEC_CON = @eFEC_CON,
            CB_FEC_ING = @eFEC_ING,
            CB_FEC_INT = @eFEC_INT,
            CB_FEC_REV = @eFEC_REV,
            CB_FEC_SAL = @eFEC_SAL,
            CB_FEC_BAJ = @eFEC_BAJ,
            CB_FEC_BSS = @eFEC_BSS,
            CB_MOT_BAJ = @eMOT_BAJ,
            CB_NIVEL1  = @eNIVEL1,
            CB_NIVEL2  = @eNIVEL2,
            CB_NIVEL3  = @eNIVEL3,
            CB_NIVEL4  = @eNIVEL4,
            CB_NIVEL5  = @eNIVEL5,
            CB_NIVEL6  = @eNIVEL6,
            CB_NIVEL7  = @eNIVEL7,
            CB_NIVEL8  = @eNIVEL8,
            CB_NIVEL9  = @eNIVEL9,
            CB_NOMNUME = @eNOMNUME,
            CB_NOMTIPO = @eNOMTIPO,
            CB_NOMYEAR = @eNOMYEAR,
            CB_PATRON  = @ePATRON,
            CB_PER_VAR = @ePER_VAR,
            CB_PUESTO  = @ePUESTO,
            CB_SAL_INT = @eSAL_INT,
            CB_SALARIO = @eSALARIO,
            CB_TABLASS = @eTABLASS,
            CB_TURNO   = @eTURNO,
            CB_ZONA_GE = @eZONA_GE,
            CB_OLD_SAL = @eOLD_SAL,
            CB_OLD_INT = @eOLD_INT,
            CB_PRE_INT = @ePRE_INT,
            CB_SAL_TOT = @eSAL_TOT,
            CB_TOT_GRA = @eTOT_GRA,
            CB_TIP_REV = @eTIP_REV,
            CB_RANGO_S = @eRANGO_S,
            CB_FEC_NIV = @eFEC_NIV,
            CB_FEC_PTO = @eFEC_PTO,
            CB_FEC_TUR = @eFEC_TUR,
            CB_FEC_KAR = @eFEC_KAR,
            CB_PLAZA   = @ePLAZA,
            CB_FEC_PLA = @eFEC_PLA,
            CB_NOMINA  = @eNOMINA,
            CB_FEC_NOM = @eFEC_NOM,
            CB_RECONTR = @eRECONTR,
            CB_FEC_COV = @eFEC_COV
	where ( CB_CODIGO = @Empleado );
END
GO

/* SP_KARDEX_CLASIFICACION: Considerar CB_SALARIO y CB_NOMINA ( PRO - Cambio de tipo de n�mina ) */
/* Patch 415 # Seq: 169 Modificado*/
ALTER PROCEDURE DBO.SP_KARDEX_CLASIFICACION
    @EMPLEADO INTEGER,
    @FECHA DATETIME,
    @CB_AUTOSAL CHAR(1) OUTPUT,
    @CB_CLASIFI CHAR(6) OUTPUT,
    @CB_CONTRAT CHAR(1) OUTPUT,
    @CB_TURNO CHAR(6) OUTPUT,
    @CB_PUESTO CHAR(6) OUTPUT,
    @CB_TABLASS CHAR(1) OUTPUT,
    @CB_NIVEL1 CHAR(6) OUTPUT,
    @CB_NIVEL2 CHAR(6) OUTPUT,
    @CB_NIVEL3 CHAR(6) OUTPUT,
    @CB_NIVEL4 CHAR(6) OUTPUT,
    @CB_NIVEL5 CHAR(6) OUTPUT,
    @CB_NIVEL6 CHAR(6) OUTPUT,
    @CB_NIVEL7 CHAR(6) OUTPUT,
    @CB_NIVEL8 CHAR(6) OUTPUT,
    @CB_NIVEL9 CHAR(6) OUTPUT,
    @CB_SALARIO PesosDiario OUTPUT,
    @CB_NOMINA SmallInt OUTPUT
AS
        SET NOCOUNT ON
  	declare Temporal CURSOR FOR
	select CB_AUTOSAL, CB_CLASIFI, CB_CONTRAT,
               CB_TURNO, CB_PUESTO, CB_TABLASS,
               CB_NIVEL1, CB_NIVEL2, CB_NIVEL3,
               CB_NIVEL4, CB_NIVEL5, CB_NIVEL6,
               CB_NIVEL7, CB_NIVEL8, CB_NIVEL9,
			CB_SALARIO, CB_NOMINA
 	from KARDEX where ( CB_CODIGO = @Empleado ) and
                          ( CB_FECHA <= @Fecha  )
 	order by CB_FECHA desc, CB_NIVEL desc

	open Temporal

	fetch next from Temporal
        into @CB_AUTOSAL, @CB_CLASIFI, @CB_CONTRAT,
             @CB_TURNO, @CB_PUESTO, @CB_TABLASS,
             @CB_NIVEL1, @CB_NIVEL2, @CB_NIVEL3,
             @CB_NIVEL4, @CB_NIVEL5, @CB_NIVEL6,
             @CB_NIVEL7, @CB_NIVEL8, @CB_NIVEL9,
		   @CB_SALARIO, @CB_NOMINA

	close Temporal
	deallocate Temporal
	
	if ( @CB_PUESTO is null )
	BEGIN
		declare Temporal2 CURSOR FOR
		select  CB_AUTOSAL, CB_CLASIFI, CB_CONTRAT,
               CB_TURNO, CB_PUESTO, CB_TABLASS,
               CB_NIVEL1, CB_NIVEL2, CB_NIVEL3,
               CB_NIVEL4, CB_NIVEL5, CB_NIVEL6,
               CB_NIVEL7, CB_NIVEL8, CB_NIVEL9,
			CB_SALARIO, CB_NOMINA
 		from KARDEX where ( CB_CODIGO = @Empleado )
 		order by CB_FECHA desc, CB_NIVEL desc

		open Temporal2

		fetch next from Temporal2
        into @CB_AUTOSAL, @CB_CLASIFI, @CB_CONTRAT,
             @CB_TURNO, @CB_PUESTO, @CB_TABLASS,
             @CB_NIVEL1, @CB_NIVEL2, @CB_NIVEL3,
             @CB_NIVEL4, @CB_NIVEL5, @CB_NIVEL6,
             @CB_NIVEL7, @CB_NIVEL8, @CB_NIVEL9,
		   @CB_SALARIO, @CB_NOMINA

		close Temporal2
		deallocate Temporal2	
	END

GO

/* ASISTENCIA_AGREGA: Considerar CB_SALARIO y CB_NOMINA ( PRO - Cambio de tipo de n�mina ) */
/* Motivo de checadas manuales, motivos del 1 al 4 */
/* Patch 415 # Seq: 173 Modificado*/
ALTER PROCEDURE ASISTENCIA_AGREGA
    @CB_CODIGO INTEGER,
    @AU_FECHA DATETIME,
    @HO_CODIGO CHAR(6),
    @AU_HOR_MAN CHAR(1),
    @AU_STATUS SMALLINT,
    @HRS_EXTRAS NUMERIC(15,2),
    @M_HRS_EXTRAS CHAR(4),
    @DESCANSO NUMERIC(15,2),
    @M_DESCANSO CHAR(4),
    @PER_CG NUMERIC(15,2),
    @M_PER_CG CHAR(4),
    @PER_CG_ENT NUMERIC(15,2),
    @M_PER_CG_ENT CHAR(4),
    @PER_SG NUMERIC(15,2),
    @M_PER_SG CHAR(4),
    @PER_SG_ENT NUMERIC(15,2),
    @M_PER_SG_ENT CHAR(4),
    @PRE_FUERA_JOR NUMERIC(15,2),
    @M_PRE_FUERA_JOR CHAR(4),
    @PRE_DENTRO_JOR NUMERIC(15,2),
    @M_PRE_DENTRO_JOR CHAR(4),
    @CHECADA1 CHAR(4),
    @CHECADA2 CHAR(4),
    @CHECADA3 CHAR(4),
    @CHECADA4 CHAR(4),
    @MC_CHECADA1 CHAR(4),
    @MC_CHECADA2 CHAR(4),
    @MC_CHECADA3 CHAR(4),
    @MC_CHECADA4 CHAR(4),
    @US_CODIGO SMALLINT,
    @AU_OUT2EAT SMALLINT
AS
BEGIN
    SET NOCOUNT ON
	DECLARE @CB_AUTOSAL CHAR(1)
	DECLARE @CB_CONTRAT CHAR(1)
	DECLARE @CB_TABLASS CHAR(1)
	DECLARE @CB_PUESTO CHAR(6)
	DECLARE @CB_CLASIFI CHAR(6)
	DECLARE @CB_TURNO CHAR(6)
	DECLARE @CB_NIVEL1 CHAR(6)
	DECLARE @CB_NIVEL2 CHAR(6)
	DECLARE @CB_NIVEL3 CHAR(6)
	DECLARE @CB_NIVEL4 CHAR(6)
	DECLARE @CB_NIVEL5 CHAR(6)
	DECLARE @CB_NIVEL6 CHAR(6)
	DECLARE @CB_NIVEL7 CHAR(6)
	DECLARE @CB_NIVEL8 CHAR(6)
	DECLARE @CB_NIVEL9 CHAR(6)
	DECLARE @CB_SALARIO PesosDiario
	DECLARE @CB_NOMINA SmallInt

	execute SP_KARDEX_CLASIFICACION @CB_CODIGO, @AU_FECHA, @CB_AUTOSAL OUTPUT,
		@CB_CLASIFI OUTPUT, @CB_CONTRAT OUTPUT, @CB_TURNO OUTPUT,
		@CB_PUESTO OUTPUT, @CB_TABLASS OUTPUT, @CB_NIVEL1 OUTPUT,
		@CB_NIVEL2 OUTPUT, @CB_NIVEL3 OUTPUT, @CB_NIVEL4 OUTPUT,
		@CB_NIVEL5 OUTPUT, @CB_NIVEL6 OUTPUT, @CB_NIVEL7 OUTPUT,
		@CB_NIVEL8 OUTPUT, @CB_NIVEL9 OUTPUT, @CB_SALARIO OUTPUT,
		@CB_NOMINA OUTPUT

	insert into AUSENCIA
		( CB_CODIGO, AU_FECHA, AU_STATUS, HO_CODIGO, AU_HOR_MAN, AU_OUT2EAT,

		CB_PUESTO, CB_CLASIFI, CB_TURNO, CB_NIVEL1, CB_NIVEL2,
		CB_NIVEL3, CB_NIVEL4, CB_NIVEL5, CB_NIVEL6, CB_NIVEL7,
		CB_NIVEL8, CB_NIVEL9, CB_SALARIO, CB_NOMINA )
	values	( @CB_CODIGO, @AU_FECHA, @AU_STATUS, @HO_CODIGO, @AU_HOR_MAN, @AU_OUT2EAT,
		@CB_PUESTO, @CB_CLASIFI, @CB_TURNO, @CB_NIVEL1, @CB_NIVEL2,
		@CB_NIVEL3, @CB_NIVEL4, @CB_NIVEL5, @CB_NIVEL6, @CB_NIVEL7,
		@CB_NIVEL8, @CB_NIVEL9, @CB_SALARIO, @CB_NOMINA  )

     	execute ASISTENCIA_CREAR_CHECADAS @CB_CODIGO, @AU_FECHA, @CHECADA1,
		@CHECADA2, @CHECADA3, @CHECADA4, @MC_CHECADA1, @MC_CHECADA2, @MC_CHECADA3,
    @MC_CHECADA4, 'N', @US_CODIGO

	execute AUTORIZACIONES_CREAR_CHECADAS @CB_CODIGO, @AU_FECHA, @HRS_EXTRAS,
		@M_HRS_EXTRAS, @DESCANSO, @M_DESCANSO, @PER_CG, @M_PER_CG, @PER_CG_ENT,
		@M_PER_CG_ENT, @PER_SG, @M_PER_SG, @PER_SG_ENT, @M_PER_SG_ENT, @PRE_FUERA_JOR,
		@M_PRE_FUERA_JOR, @PRE_DENTRO_JOR, @M_PRE_DENTRO_JOR,'N',
		@US_CODIGO
END

GO

/* ASISTENCIA_MODIFICA: Considerar CB_SALARIO y CB_NOMINA ( PRO - Cambio de tipo de n�mina ) */
/* Motivos de checadas manuales considerar motivos del 1 al 4 */
/* Patch 415 # Seq: 15 Modificado*/
ALTER PROCEDURE ASISTENCIA_MODIFICA
    @CB_CODIGO INTEGER,
    @AU_FECHA DATETIME,
    @HO_CODIGO CHAR(6),
    @AU_HOR_MAN CHAR(1),
    @AU_STATUS SMALLINT,
    @HRS_EXTRAS NUMERIC(15,2),
    @M_HRS_EXTRAS CHAR(4),
    @DESCANSO NUMERIC(15,2),
    @M_DESCANSO CHAR(4),
    @PER_CG NUMERIC(15,2),
    @M_PER_CG CHAR(4),
    @PER_CG_ENT NUMERIC(15,2),
    @M_PER_CG_ENT CHAR(4),
    @PER_SG NUMERIC(15,2),
    @M_PER_SG CHAR(4),
    @PER_SG_ENT NUMERIC(15,2),
    @M_PER_SG_ENT CHAR(4),
    @PRE_FUERA_JOR NUMERIC(15,2),
    @M_PRE_FUERA_JOR CHAR(4),
    @PRE_DENTRO_JOR NUMERIC(15,2),
    @M_PRE_DENTRO_JOR CHAR(4),  
    @CHECADA1 CHAR(4),
    @CHECADA2 CHAR(4),
    @CHECADA3 CHAR(4),
    @CHECADA4 CHAR(4),
    @MC_CHECADA1 CHAR(4),
    @MC_CHECADA2 CHAR(4),
    @MC_CHECADA3 CHAR(4),
    @MC_CHECADA4 CHAR(4),
    @US_CODIGO SMALLINT,
    @AU_OUT2EAT SMALLINT
AS
BEGIN
    SET NOCOUNT ON
  	DECLARE @CB_AUTOSAL CHAR(1)
	DECLARE @CB_CONTRAT CHAR(1)
	DECLARE @CB_TABLASS CHAR(1)
	DECLARE @CB_PUESTO CHAR(6)
	DECLARE @CB_CLASIFI CHAR(6)
	DECLARE @CB_TURNO CHAR(6)
	DECLARE @CB_NIVEL1 CHAR(6)
	DECLARE @CB_NIVEL2 CHAR(6)
	DECLARE @CB_NIVEL3 CHAR(6)
	DECLARE @CB_NIVEL4 CHAR(6)
	DECLARE @CB_NIVEL5 CHAR(6)
	DECLARE @CB_NIVEL6 CHAR(6)
	DECLARE @CB_NIVEL7 CHAR(6)
	DECLARE @CB_NIVEL8 CHAR(6)
	DECLARE @CB_NIVEL9 CHAR(6)
	DECLARE @CB_SALARIO PesosDiario
	DECLARE @CB_NOMINA SmallInt

	execute SP_KARDEX_CLASIFICACION @CB_CODIGO, @AU_FECHA, @CB_AUTOSAL OUTPUT, 
		@CB_CLASIFI OUTPUT, @CB_CONTRAT OUTPUT, @CB_TURNO OUTPUT,
		@CB_PUESTO OUTPUT, @CB_TABLASS OUTPUT, @CB_NIVEL1 OUTPUT, 
		@CB_NIVEL2 OUTPUT, @CB_NIVEL3 OUTPUT, @CB_NIVEL4 OUTPUT, 
		@CB_NIVEL5 OUTPUT, @CB_NIVEL6 OUTPUT, @CB_NIVEL7 OUTPUT,
		@CB_NIVEL8 OUTPUT, @CB_NIVEL9 OUTPUT, @CB_SALARIO OUTPUT,
		@CB_NOMINA OUTPUT

	update AUSENCIA set HO_CODIGO = @HO_CODIGO,
           AU_STATUS = @AU_STATUS,
		   AU_HOR_MAN = @AU_HOR_MAN,
           AU_OUT2EAT = @AU_OUT2EAT,
           CB_PUESTO = @CB_PUESTO,
		   CB_CLASIFI = @CB_CLASIFI,
           CB_TURNO = @CB_TURNO,
		   CB_NIVEL1 = @CB_NIVEL1,
           CB_NIVEL2 = @CB_NIVEL2,
		   CB_NIVEL3 = @CB_NIVEL3,
           CB_NIVEL4 = @CB_NIVEL4,
		   CB_NIVEL5 = @CB_NIVEL5,
           CB_NIVEL6 = @CB_NIVEL6,
		   CB_NIVEL7 = @CB_NIVEL7,
           CB_NIVEL8 = @CB_NIVEL8,
		   CB_NIVEL9 = @CB_NIVEL9,
		 CB_SALARIO = @CB_SALARIO,
		 CB_NOMINA = @CB_NOMINA
	where ( CB_CODIGO = @CB_CODIGO ) and ( AU_FECHA = @AU_FECHA )

     	execute ASISTENCIA_CREAR_CHECADAS @CB_CODIGO, @AU_FECHA, @CHECADA1,
		@CHECADA2, @CHECADA3, @CHECADA4, @MC_CHECADA1, @MC_CHECADA2, @MC_CHECADA3,
    @MC_CHECADA4,'N', @US_CODIGO

	execute AUTORIZACIONES_CREAR_CHECADAS @CB_CODIGO, @AU_FECHA, @HRS_EXTRAS,
		@M_HRS_EXTRAS, @DESCANSO, @M_DESCANSO, @PER_CG, @M_PER_CG, @PER_CG_ENT,
		@M_PER_CG_ENT, @PER_SG, @M_PER_SG, @PER_SG_ENT, @M_PER_SG_ENT, @PRE_FUERA_JOR,
	 	@M_PRE_FUERA_JOR, @PRE_DENTRO_JOR, @M_PRE_DENTRO_JOR,'N',@US_CODIGO
END

GO

/* SP_CLAS_NOMINA: Considerar fecha de inicio o fecha fin segun el corte ( PRO - Cambio Tipo Nomina )*/
/* Patch 415 # Seq: 27 Modificado*/
ALTER PROCEDURE SP_CLAS_NOMINA
    	@ANIO SMALLINT,
    	@TIPO SMALLINT,
    	@NUMERO SMALLINT,
    	@EMPLEADO INTEGER,
        @ROTATIVO CHAR(6) OUTPUT
AS
    SET NOCOUNT ON
	declare @FechaIni DATETIME;
 	declare @FechaFin DATETIME;
 	declare @ZONA_GE  CHAR(1);
 	declare @PUESTO   CHAR(6);
 	declare @CLASIFI  CHAR(6);
 	declare @TURNO    CHAR(6);
 	declare @PATRON   CHAR(1);
 	declare @NIVEL1   CHAR(6);
 	declare @NIVEL2   CHAR(6);
 	declare @NIVEL3   CHAR(6);
 	declare @NIVEL4   CHAR(6);
 	declare @NIVEL5   CHAR(6);
 	declare @NIVEL6   CHAR(6);
 	declare @NIVEL7   CHAR(6);
 	declare @NIVEL8   CHAR(6);
 	declare @NIVEL9   CHAR(6);
 	declare @SALARIO  NUMERIC(15,2);
 	declare @SAL_INT  NUMERIC(15,2);
 	declare @PROMEDIA CHAR(1);
 	declare @JORNADA  NUMERIC(15,2);
 	declare @D_TURNO  SMALLINT;
 	declare @ES_ROTA  CHAR(1);
 	declare @FEC_KAR  DATETIME;
 	declare @FEC_SAL  DATETIME;
	declare @BAN_ELE  VARCHAR(30);
	declare @NIVEL0   CHAR(6);
	declare @PLAZA	  INTEGER;	
	declare @CB_NOMINA	SMALLINT;
	declare @FEC_ING	DATETIME;
	declare @FEC_BAJ	DATETIME;
	declare @FEC_NOM	DATETIME;
	declare @Fecha		DATETIME;
	declare @k_NOMINA		SMALLINT;

  	select @FechaIni = PE_FEC_INI,
               @FechaFin = PE_FEC_FIN
  	from   PERIODO
  	where  PE_YEAR = @Anio 
	and PE_TIPO = @Tipo
	and PE_NUMERO = @Numero

	select 
		@CB_NOMINA = CB_NOMINA
        from   SP_FECHA_KARDEX( @FechaIni, @Empleado )     

	
	SELECT @FEC_ING = CB_FEC_ING, 
		  @FEC_BAJ = CB_FEC_BAJ, 
		  @FEC_NOM = CB_FEC_NOM,
		  @k_NOMINA = CB_NOMINA		 
		  from COLABORA where CB_CODIGO = @Empleado

	/* Si existe cualquier corte en la n�mina ya sea por ingreso o cambio de tipo de n�mina
	   se determina a que fecha va investigar el turno.

	   1. Si se esta calculando la n�mina anterior se toma la fecha anterior al cambio.
	   2. Si se esta calculando la n�mina con el cambio se toma la fecha posterior al cambio. */

	IF ( @FEC_NOM between @FechaIni and @FechaFin ) and ( @CB_NOMINA <> @k_NOMINA )
	begin
		if ( @CB_NOMINA = @Tipo ) 
		begin
			set @Fecha = @FechaIni
		end
		else
		begin
			set @Fecha = @FechaFin
		end 
	end
	else
	begin
		set @Fecha = @FechaFin
	end  
	

  	select @ZONA_GE = CB_ZONA_GE,
		@PUESTO = CB_PUESTO,
		@CLASIFI = CB_CLASIFI,
		@TURNO = CB_TURNO,
		@PATRON = CB_PATRON,
                @NIVEL1 = CB_NIVEL1,
		@NIVEL2 = CB_NIVEL2, 
		@NIVEL3 = CB_NIVEL3,
		@NIVEL4 = CB_NIVEL4, 
		@NIVEL5 = CB_NIVEL5,
                @NIVEL6 = CB_NIVEL6,
		@NIVEL7 = CB_NIVEL7, 
		@NIVEL8 = CB_NIVEL8, 
		@NIVEL9 = CB_NIVEL9,
                @SALARIO = CB_SALARIO,
		@SAL_INT = CB_SAL_INT, 
		@FEC_KAR = CB_FEC_KAR, 
		@FEC_SAL = CB_FEC_SAL,
                @BAN_ELE = CB_BAN_ELE,
                @NIVEL0 = CB_NIVEL0,
		@PLAZA	= CB_PLAZA
  	from   COLABORA
  	where  CB_CODIGO = @Empleado

  	if ( @Fecha < @FEC_KAR ) 
  	begin
    		select 
			@ZONA_GE = CB_ZONA_GE,
			@PUESTO = CB_PUESTO,
			@CLASIFI = CB_CLASIFI, 
			@TURNO = CB_TURNO, 
			@PATRON = CB_PATRON,
         	        @NIVEL1 = CB_NIVEL1,
			@NIVEL2 = CB_NIVEL2, 
			@NIVEL3 = CB_NIVEL3, 
			@NIVEL4 = CB_NIVEL4,
			@NIVEL5 = CB_NIVEL5,
         	        @NIVEL6 = CB_NIVEL6,
			@NIVEL7 = CB_NIVEL7, 
			@NIVEL8 = CB_NIVEL8,
			@NIVEL9 = CB_NIVEL9,
         	        @SALARIO = CB_SALARIO,
			@SAL_INT = CB_SAL_INT,
			@PLAZA	= CB_PLAZA
            from   SP_FECHA_KARDEX( @Fecha, @Empleado )
  	end

  	select @PROMEDIA = GL_FORMULA
  	from   GLOBAL
  	where  GL_CODIGO = 42

  	if (( @PROMEDIA = 'S' ) and ( @FEC_SAL > @FechaIni ))
        EXECUTE SP_RANGO_SALARIO @FechaIni, @FechaFin, @Empleado,
                                 @SALARIO OUTPUT, @SAL_INT OUTPUT

	EXECUTE SP_DIAS_JORNADA @Turno, @FechaIni, @FechaFin,
    				@D_Turno OUTPUT, @Jornada OUTPUT, @Es_Rota OUTPUT

  	if ( @ES_ROTA = 'S' )
    		SET @Rotativo = @Turno
  	else
    		SET @Rotativo = '';

  	update NOMINA
  	set     CB_ZONA_GE = @ZONA_GE,
        	CB_PUESTO  = @PUESTO,
        	CB_CLASIFI = @CLASIFI,
        	CB_TURNO   = @TURNO,
        	CB_PATRON  = @PATRON,
        	CB_NIVEL1  = @NIVEL1,
        	CB_NIVEL2  = @NIVEL2,
        	CB_NIVEL3  = @NIVEL3,
        	CB_NIVEL4  = @NIVEL4,
        	CB_NIVEL5  = @NIVEL5,
        	CB_NIVEL6  = @NIVEL6,
        	CB_NIVEL7  = @NIVEL7,
        	CB_NIVEL8  = @NIVEL8,
        	CB_NIVEL9  = @NIVEL9,
        	CB_SALARIO = @SALARIO,
        	CB_SAL_INT = @SAL_INT,
        	NO_JORNADA = @JORNADA,
        	NO_D_TURNO = @D_TURNO,
                CB_BAN_ELE = @BAN_ELE,
                CB_NIVEL0  = @NIVEL0,
                CB_PLAZA   = @PLAZA
  	where PE_YEAR = @Anio
	and PE_TIPO = @Tipo
	and PE_NUMERO = @Numero
	and CB_CODIGO = @Empleado;

GO

/*SP_CAMBIA_REGLAPRESTA: Stored Procedure para cambiar el orden de las reglas de los prestamos ( PRO Reglas en pr�stamos y ahorros )*/
/* Patch 415 # Seq: X Agregado */
CREATE PROCEDURE SP_CAMBIA_REGLAPRESTA(@CODIG FolioChico, @Actual FolioChico, @Nuevo FolioChico )
AS
BEGIN
SET NOCOUNT ON;
	declare @Maximo FolioChico;
	if ( ( @Nuevo < 1 ) or ( @Actual < 1 ) or ( @Actual = @Nuevo ) )
		RETURN;
	select @Maximo = MAX( RP_ORDEN ) from REGLAPREST;
	if ( @Nuevo > @Maximo ) or ( @Actual > @Maximo )
		RETURN;
	update REGLAPREST	set RP_ORDEN = 0 where ( RP_ORDEN = @Actual );
	update REGLAPREST	set RP_ORDEN = @Actual where ( RP_ORDEN = @Nuevo );
	update REGLAPREST  set RP_ORDEN = @Nuevo where ( RP_ORDEN = 0 );
END
GO


/*SP_BORRA_REGLAPREST: Stored Procedure para cambiar el orden de las reglas de los prestamos al borrar ( PRO Reglas en pr�stamos y ahorros )*/
/* Patch 415 # Seq: X Agregado */
CREATE PROCEDURE SP_BORRA_REGLAPREST( @RP_CODIGO Codigo, @Actual FolioChico )
AS
BEGIN
	SET NOCOUNT ON;
	delete from REGLAPREST where ( RP_CODIGO = @RP_CODIGO ) and ( RP_ORDEN = @Actual );
	if ( @@ROWCOUNT > 0 )
    	begin
	     update REGLAPREST set RP_ORDEN = RP_ORDEN - 1
             where ( RP_ORDEN > @Actual );
        end
END
GO

/*RECALCULA_INFONAVIT: Considerar fecha de otorgamiento de Infonavit*/
/* Patch 415 # Seq: 309 Modificado */
ALTER PROCEDURE RECALCULA_INFONAVIT(@Empleado NumeroEmpleado) AS
BEGIN
	SET NOCOUNT ON
	declare @CB_INFTIPO Status
	declare @CB_INFCRED	Descripcion
	declare @CB_INFTASA	DescINFO	
	declare @CB_INFDISM Booleano
	declare @KI_FECHA Fecha
	declare @KI_TIPO Status
	declare @CB_INF_ANT Fecha	
	
	declare @Old_InfTipo Status
	set @Old_InfTipo = 0

	declare @Old_Infcred Descripcion
	set @Old_Infcred = ''

	declare @Old_Inftasa DescINFO
	set @Old_Inftasa = 0
	
	declare @Old_AplicaDis Booleano
	set @Old_AplicaDis = 'N'

	declare @FecIniCredito Fecha
	set @FecIniCredito = '12/30/1899'

	declare @CB_INFACT Booleano
	set @CB_INFACT = 'N' 

	declare @Old_INF_ANT Fecha
	set @Old_INF_ANT = '12/30/1899'


	declare CursorInfonavit CURSOR FOR
	select CB_INFTIPO, CB_INFCRED, CB_INFTASA, CB_INFDISM,
		  KI_FECHA, KI_TIPO, CB_INF_ANT
 	from KARINF
	where CB_CODIGO = @Empleado
	order by KI_FECHA

	Open CursorInfonavit
	Fetch NEXT from CursorInfonavit
	into	@CB_INFTIPO, @CB_INFCRED, @CB_INFTASA, @CB_INFDISM, @KI_FECHA, @KI_TIPO, @CB_INF_ANT
	

	if ( @KI_TIPO <> 0 )
	BEGIN
		Close CursorInfonavit
		Deallocate CursorInfonavit
		RAISERROR( 'Empleado #%d, ERROR en Kardex Infonavit: Movimiento(s) previo(s) a la alta de Infonavit', 16, 1, @Empleado )
		RETURN
	END

	while ( @@FETCH_STATUS = 0 )
	BEGIN
		/* Inicio */
		if ( @KI_TIPO in ( 0  ) ) 
		begin
			set @CB_INFACT = 'S'
			set @FecIniCredito = @KI_FECHA
			set @Old_Infcred = @CB_INFCRED
			set @Old_InfTipo = @CB_INFTIPO
			set @Old_Inftasa = @CB_INFTASA
			set @Old_AplicaDis = @CB_INFDISM
			set @Old_INF_ANT = @CB_INF_ANT
		
		end
		
		/* Reinicio */
		
		if ( @KI_TIPO = 2 ) 
		begin
			set @CB_INFACT = 'S'
			set @Old_AplicaDis = 'S'
			set @FecIniCredito = @KI_FECHA
			set @Old_InfTipo = @CB_INFTIPO
			set @Old_Inftasa = @CB_INFTASA
			set @Old_AplicaDis = @CB_INFDISM
			set @Old_INF_ANT = @CB_INF_ANT
		end
		 

		/* Suspensi�n */
		if ( @KI_TIPO = 1 )
		BEGIN
			set @CB_INFACT = 'N'
		END

		/* Cambios */
		
		/* Cambio de Tipo de Descuento */
		if ( @KI_TIPO = 3 )
		begin
			set @Old_InfTipo = @CB_INFTIPO
			set @Old_Inftasa = @CB_INFTASA
			set @Old_AplicaDis = @CB_INFDISM 
		end
		
		/* Cambio de Valor de Descuento */
		if ( @KI_TIPO = 4 )
		begin
			set @Old_Inftasa = @CB_INFTASA
			set @Old_AplicaDis = @CB_INFDISM
			
		end
	
		/* Cambio de # de Cr�dito */
		if ( @KI_TIPO = 5 ) 
		begin
			set @Old_Infcred = @CB_INFCRED
      set @Old_InfTipo = @CB_INFTIPO
			set @Old_Inftasa = @CB_INFTASA
			set @Old_AplicaDis = @CB_INFDISM
		end
		
		
		update KARINF set CB_INFTIPO = @Old_inftipo,
					    CB_INFCRED = @Old_Infcred,	
	 			    	    CB_INFTASA = @Old_Inftasa,	
	 			         CB_INFDISM = @Old_AplicaDis,
						 CB_INF_ANT = @Old_INF_ANT
		where CURRENT OF CursorInfonavit 
	 	
		Fetch NEXT from CursorInfonavit
		into	@CB_INFTIPO, @CB_INFCRED, @CB_INFTASA, @CB_INFDISM, @KI_FECHA, @KI_TIPO, @CB_INF_ANT
	END

	update COLABORA set CB_INF_INI = @FecIniCredito,
					CB_INFTIPO = @Old_InfTipo,
					CB_INFCRED = @Old_InfCred,
					CB_INFTASA = @Old_InfTasa,
					CB_INFACT = @CB_INFACT,
					CB_INFDISM = @Old_AplicaDis,
					CB_INF_ANT = @Old_INF_ANT
	where ( CB_CODIGO = @Empleado )

					
	
	close CursorInfonavit;
	deallocate CursorInfonavit;
END
GO

/*SP_CHECADA_MOT: Considerar el motivo de la checada*/
/* Patch 415 # Seq: 309 Modificado */
ALTER FUNCTION SP_CHECADA_MOT
	( @FECHA DATETIME, @EMPLEADO INTEGER, @NUMERO SMALLINT )
RETURNS CHAR(4)
AS
BEGIN
	declare @Resultado CHAR(4)
	declare @Ciclo SMALLINT
  	SET @Ciclo = 1
  	declare Temporal cursor READ_ONLY STATIC for
     		select CH_MOTIVO from CHECADAS
		where ( CB_CODIGO = @EMPLEADO ) and ( AU_FECHA = @FECHA ) and
		( CH_TIPO < 5 ) and ( CH_SISTEMA <> 'S' )
		order by CH_H_REAL
	Open Temporal
	if ( @@CURSOR_ROWS >= @NUMERO )
	begin
		Fetch Next From Temporal Into @Resultado
		while ( @@Fetch_Status = 0 ) and ( @Ciclo <> @NUMERO )
		begin
			SET @Ciclo = @Ciclo + 1
			Fetch Next From Temporal Into @Resultado
		end
	end
	if ( @Resultado is Null ) or ( @Ciclo <> @NUMERO )
		SET @Resultado = ''

	close Temporal
	deallocate Temporal

	RETURN @Resultado
END
GO
/*SP_CHECADA_USUARIO: Obtener el usuario de la checada*/
/* Patch 415 # Seq: 309 Modificado */
ALTER FUNCTION SP_CHECADA_USUARIO
	( @FECHA DATETIME, @EMPLEADO INTEGER, @NUMERO SMALLINT )
RETURNS Int
AS
BEGIN
	declare @Resultado Int
	declare @Ciclo SMALLINT
  	SET @Ciclo = 1
  	declare Temporal cursor READ_ONLY STATIC for
     		select US_CODIGO from CHECADAS
		where ( CB_CODIGO = @EMPLEADO ) and ( AU_FECHA = @FECHA ) and
		( CH_TIPO < 5 ) and ( CH_SISTEMA <> 'S' )
		order by CH_H_REAL
	Open Temporal
	if ( @@CURSOR_ROWS >= @NUMERO )
	begin
		Fetch Next From Temporal Into @Resultado
		while ( @@Fetch_Status = 0 ) and ( @Ciclo <> @NUMERO )
		begin
			SET @Ciclo = @Ciclo + 1
			Fetch Next From Temporal Into @Resultado
		end
	end
	if ( @Resultado is Null ) or ( @Ciclo <> @NUMERO )
		SET @Resultado = 0

	close Temporal
	deallocate Temporal
	
	RETURN @Resultado
END
GO
/*ASISTENCIA_AGREGAR: Considerar le motivo de la checada*/
/* Patch 415 # Seq: 10 Modificado */
ALTER PROCEDURE ASISTENCIA_AGREGAR 
    @CB_CODIGO INTEGER,
    @AU_FECHA DATETIME,
    @CH_REAL CHAR(4),
    @CH_GLOBAL CHAR(1),
    @US_CODIGO SMALLINT,
	@CH_MOTIVO CHAR(4)
AS
     SET NOCOUNT ON
     insert into CHECADAS( CB_CODIGO,
                           AU_FECHA,
                           CH_H_REAL,
                           CH_H_AJUS,
                           CH_SISTEMA,
                           CH_GLOBAL,
                           CH_RELOJ,
                           US_CODIGO,
                           CH_MOTIVO )
	values ( @CB_CODIGO,
                 @AU_FECHA,
                 @CH_REAL,
                 @CH_REAL,
                 'N',
                 @CH_GLOBAL,
                 '',
                 @US_CODIGO,
				        @CH_MOTIVO )
GO

/*ASISTENCIA_CREAR_CHECADAS: Considerar le motivo de la checada*/
/* Patch 415 # Seq: 11 Modificado */
ALTER PROCEDURE ASISTENCIA_CREAR_CHECADAS
    @CB_CODIGO INTEGER,
    @AU_FECHA DATETIME,
    @CHECADA1 CHAR(4),
    @CHECADA2 CHAR(4),
    @CHECADA3 CHAR(4),
    @CHECADA4 CHAR(4),
    @MC_CHECADA1 CHAR(4),
    @MC_CHECADA2 CHAR(4),
    @MC_CHECADA3 CHAR(4),
    @MC_CHECADA4 CHAR(4),
    @CH_GLOBAL CHAR(1),
    @US_CODIGO SMALLINT

AS
        SET NOCOUNT ON
	DECLARE @OLD_CHECADA1 CHAR(4)
	DECLARE @OLD_CHECADA2 CHAR(4)
	DECLARE @OLD_CHECADA3 CHAR(4)
	DECLARE @OLD_CHECADA4 CHAR(4)
  DECLARE @OLD_MC_CHECADA1 CHAR(4)
	DECLARE @OLD_MC_CHECADA2 CHAR(4)
	DECLARE @OLD_MC_CHECADA3 CHAR(4)
	DECLARE @OLD_MC_CHECADA4 CHAR(4)


     	select @OLD_CHECADA1 = dbo.SP_CHECADAS( @AU_FECHA, @CB_CODIGO, 1 )
     	select @OLD_CHECADA2 = dbo.SP_CHECADAS( @AU_FECHA, @CB_CODIGO, 2 )
     	select @OLD_CHECADA3 = dbo.SP_CHECADAS( @AU_FECHA, @CB_CODIGO, 3 )
     	select @OLD_CHECADA4 = dbo.SP_CHECADAS( @AU_FECHA, @CB_CODIGO, 4 )

      select @OLD_MC_CHECADA1 = dbo.SP_CHECADA_MOT( @AU_FECHA, @CB_CODIGO, 1 )
      select @OLD_MC_CHECADA2 = dbo.SP_CHECADA_MOT( @AU_FECHA, @CB_CODIGO, 2 )
      select @OLD_MC_CHECADA3 = dbo.SP_CHECADA_MOT( @AU_FECHA, @CB_CODIGO, 3 )
      select @OLD_MC_CHECADA4 = dbo.SP_CHECADA_MOT( @AU_FECHA, @CB_CODIGO, 3 )

	if ( @OLD_CHECADA1 is not null ) and ( ( @CHECADA1 <> @OLD_CHECADA1 ) or
                                         ( @MC_CHECADA1 <> @OLD_MC_CHECADA1 ) )
     	begin
          	delete from CHECADAS where
          	( CB_CODIGO = @CB_CODIGO ) and
          	( AU_FECHA = @AU_FECHA ) and
          	( CH_H_REAL = @OLD_CHECADA1 )
     	end
	if ( @OLD_CHECADA2 is not null ) and ( ( @CHECADA2 <> @OLD_CHECADA2 ) or
                                         ( @MC_CHECADA2 <> @OLD_MC_CHECADA2 ) )
     	begin
          	delete from CHECADAS where
          	( CB_CODIGO = @CB_CODIGO ) and
          	( AU_FECHA = @AU_FECHA ) and
          	( CH_H_REAL = @OLD_CHECADA2 )
     	end
	if ( @OLD_CHECADA3 is not null ) and ( ( @CHECADA3 <> @OLD_CHECADA3 ) or
                                         ( @MC_CHECADA3 <> @OLD_MC_CHECADA3 ) )
     	begin
          	delete from CHECADAS where
          	( CB_CODIGO = @CB_CODIGO ) and
          	( AU_FECHA = @AU_FECHA ) and
          	( CH_H_REAL = @OLD_CHECADA3 )
     	end
	if ( @OLD_CHECADA4 is not null ) and ( ( @CHECADA4 <> @OLD_CHECADA4 ) or
                                         ( @MC_CHECADA4 <> @OLD_MC_CHECADA4 ) )
     	begin
          	delete from CHECADAS where
          	( CB_CODIGO = @CB_CODIGO ) and
          	( AU_FECHA = @AU_FECHA ) and
          	( CH_H_REAL = @OLD_CHECADA4 )
     	end

	if ( ( @OLD_CHECADA1 is null or ( ( @CHECADA1 <> @OLD_CHECADA1 ) or ( @MC_CHECADA1 <> @OLD_MC_CHECADA1 ) )  ) and
                                  ( @CHECADA1 <> '' and @CHECADA1 <> '0000' ) )
	begin
		EXECUTE ASISTENCIA_AGREGAR @CB_CODIGO, @AU_FECHA, @CHECADA1, @CH_GLOBAL, @US_CODIGO,@MC_CHECADA1
     	end
	if ( ( @OLD_CHECADA2 is null or ( ( @CHECADA2 <> @OLD_CHECADA2 ) or ( @MC_CHECADA2 <> @OLD_MC_CHECADA2 ) ) ) and
                                  ( @CHECADA2 <> '' and @CHECADA2 <> '0000' ) )
	begin
		EXECUTE ASISTENCIA_AGREGAR @CB_CODIGO, @AU_FECHA, @CHECADA2, @CH_GLOBAL, @US_CODIGO,@MC_CHECADA2
     	end
	if ( ( @OLD_CHECADA3 is null or ( ( @CHECADA3 <> @OLD_CHECADA3 ) or ( @MC_CHECADA3 <> @OLD_MC_CHECADA3 )  ) ) and
                                  ( @CHECADA3 <> '' and @CHECADA3 <> '0000' ) )
	begin
		EXECUTE ASISTENCIA_AGREGAR @CB_CODIGO, @AU_FECHA, @CHECADA3, @CH_GLOBAL, @US_CODIGO,@MC_CHECADA3
     	end
	if ( ( @OLD_CHECADA4 is null or ( ( @CHECADA4 <> @OLD_CHECADA4 ) or ( @MC_CHECADA4 <> @OLD_MC_CHECADA4 ) ) ) and
                                  ( @CHECADA4 <> '' and @CHECADA4 <> '0000' ) )
	begin
		EXECUTE ASISTENCIA_AGREGAR @CB_CODIGO, @AU_FECHA, @CHECADA4, @CH_GLOBAL, @US_CODIGO,@MC_CHECADA4
     	end
GO

/*****************************************/
/************ TRIGGERS *******************/
/*****************************************/

/* Tabla: EMP_PROG: Trigger para actualizar la fecha de programacion del curso Tomado (PRO: Reprogramacion de Cursos: Fersinsa) */
/* Patch 415 # Seq: 10 Agregado */
CREATE TRIGGER TI_KARCURSO ON KARCURSO AFTER INSERT AS
BEGIN
SET NOCOUNT ON;
DECLARE  @FechaProgram Fecha;
declare @ReprogDias Integer;
declare @DiasAntig Integer;
declare @ReprogVersion char(1);
declare @cu_revisio char(10);
declare @FechaAntig Fecha;
declare @FecTomada Fecha;
declare @NewCbCodigo Integer;
declare @NewKcFecTom Fecha;
declare @NewCuCodigo Codigo;
declare @NewKcRevisio Codigo;

	 select @NewCbCodigo = CB_CODIGO,@NewKcFecTom = KC_FEC_TOM ,@NewCuCodigo = CU_CODIGO,@NewKcRevisio = KC_REVISIO from Inserted;

     /* Obtener los datos de la matriz de entrenamiento */
	 select @ReprogDias = E.EN_RE_DIAS,@DiasAntig = E.EN_DIAS,@ReprogVersion = E.EN_REPROG,@Cu_Revisio = C.CU_REVISIO FROM ENTRENA E join CURSO C on E.CU_CODIGO = C.CU_CODIGO where E.CU_CODIGO = @NewCuCodigo and E.PU_CODIGO = (select CB_PUESTO from colabora where CB_CODIGO = @NewCbCodigo );

	 /* Obtener la fecha de ingreso del empleado*/
     select @FechaAntig = CB_FEC_ING from colabora where CB_CODIGO = @NewCbCodigo;

	 /* Obtener la fecha del ultimo curso tomado en cuestion */
     Select @FecTomada = max(kc_fec_tom) from KARCURSO where (kc_fec_tom < @NewKcFecTom and CU_CODIGO = @NewCuCodigo and CB_CODIGO = @NewCbCodigo);

	 /* Obtener la ultima fecha de Programacion para el curso */
     Select @FechaProgram = max(KC_FECPROG) from KARCURSO where (kc_fec_tom < @NewKcFecTom and CU_CODIGO = @NewCuCodigo and CB_CODIGO = @NewCbCodigo);
	 /* Si aun no se ha tomado el curso , se toma la fecha de antiguedad mas los dias de la matriz ;
		En caso contrario se investiga cual era la fecha de programaci�n para ese curso */
     if (@FechaProgram is null)
     begin
          set @FechaProgram = DATEADD( DAY, @DiasAntig , @FechaAntig );
     end
     else
     begin
         set @FechaProgram =  ( select case when @FecTomada is null then '12/30/2009'
		        when ( @ReprogDias > 0 AND @ReprogVersion = 'N'  )THEN (  DATEADD( DAY, @ReprogDias , @FecTomada ))
				when ( ( @ReprogDias > 0 AND @ReprogVersion = 'S' ) and ( @NewKcRevisio = @cu_revisio ) and (  not (@NewKcRevisio = '')  ) )then ( select max(CH_FECHA) from dbo.CUR_REV where( ( CUR_REV.CU_CODIGO = @NewCuCodigo ) AND ( CUR_REV.CH_REVISIO = @cu_revisio ) ) ) 
				when ( ( @ReprogDias > 0 AND @ReprogVersion = 'S' ) and ((( @NewKcRevisio = @cu_revisio ) and ( @NewKcRevisio = '' ))or ( ( @NewKcRevisio <> @cu_revisio ) ) ) ) then ( DATEADD( DAY, @ReprogDias , @FecTomada ) )              
				when ( @ReprogDias = 0 AND @ReprogVersion = 'N' )then ( DATEADD( DAY, @DiasAntig , @FechaAntig ))			    
				when ( @ReprogDias = 0 AND @ReprogVersion = 'S' and ( not( @NewKcRevisio = '' ) ) and ( (select Count(CH_REVISIO) FROM cur_rev cr where cr.CH_REVISIO = @NewKcRevisio ) > 0 ) )then ( select max(CH_FECHA) from dbo.CUR_REV where( ( CUR_REV.CU_CODIGO = @NewCuCodigo ) AND ( CUR_REV.CH_REVISIO = @NewKcRevisio ) ) )			    				
				else ( DATEADD( DAY, @DiasAntig , @FechaAntig ) )End );
     end

     if (@FechaProgram is null)
     begin
          set @FechaProgram = '12/30/1899';
     end
     /* Se actualiza la fecha de programacion para el curso en cuestion Este ya no se puede reprogramar */ 
	update KARCURSO set KC_FECPROG = @FechaProgram WHERE karcurso.cb_codigo = @NewCbCodigo and karcurso.kc_fec_tom = @NewKcFecTom and karcurso.cu_codigo = @NewCuCodigo;
END
GO

/* Tabla: KARCURSO: Trigger para actualizar la fecha de programacion del curso Tomado (PRO: Reprogramacion de Cursos: Fersinsa) */
/* Patch 415 # Seq: 10 Agregado */
CREATE TRIGGER TU_KARCURSO ON KARCURSO AFTER UPDATE AS
BEGIN
SET NOCOUNT ON;
DECLARE  @FechaProgram Fecha;
declare @ReprogDias Integer;
declare @DiasAntig Integer;
declare @ReprogVersion char(1);
declare @cu_revisio char(10);
declare @FechaAntig Fecha;
declare @FecTomada Fecha;
declare @NewCbCodigo Integer;
declare @NewKcFecTom Fecha;
declare @NewCuCodigo Codigo;
declare @NewKcRevisio Codigo;
declare @FechaRevision Fecha;

	 select @NewCbCodigo = CB_CODIGO,@NewKcFecTom = KC_FEC_TOM ,@NewCuCodigo = CU_CODIGO,@NewKcRevisio = KC_REVISIO from Inserted;
 
     /* Obtener los datos de la matriz de entrenamiento */
	 select @ReprogDias = E.EN_RE_DIAS,@DiasAntig = E.EN_DIAS,@ReprogVersion = E.EN_REPROG,@Cu_Revisio = C.CU_REVISIO FROM ENTRENA E join CURSO C on E.CU_CODIGO = C.CU_CODIGO where E.CU_CODIGO = @NewCuCodigo and E.PU_CODIGO = (select CB_PUESTO from colabora where CB_CODIGO = @NewCbCodigo );
     
	 /* Obtener la fecha de ingreso del empleado*/
     select @FechaAntig = CB_FEC_ING from colabora where CB_CODIGO = @NewCbCodigo;

	 /* Obtener la fecha del ultimo curso tomado en cuestion */
     Select @FecTomada = max(kc_fec_tom) from KARCURSO where (kc_fec_tom < @NewKcFecTom and CU_CODIGO = @NewCuCodigo and CB_CODIGO = @NewCbCodigo);
	 
	 /* Obtener la ultima fecha de Programacion para el curso */
     Select @FechaProgram = max(KC_FECPROG) from KARCURSO where (kc_fec_tom < @NewKcFecTom and CU_CODIGO = @NewCuCodigo and CB_CODIGO = @NewCbCodigo);
	 /* Si aun no se ha tomado el curso , se toma la fecha de antiguedad mas los dias de la matriz ;
		En caso contrario se investiga cual era la fecha de programaci�n para ese curso */
     select @FechaRevision = max(CH_FECHA) from dbo.CUR_REV where ( ( CUR_REV.CU_CODIGO = @NewCuCodigo ) AND ( CUR_REV.CH_REVISIO = @NewKcRevisio ) ); 
     if (@FechaProgram is null)
     begin
          set @FechaProgram = DATEADD( DAY, @DiasAntig , @FechaAntig );
     end;
    
     IF Update(CU_CODIGO)
	 begin
		  set @FechaProgram =  ( select case when @FecTomada is null then '12/30/2009'
		        when ( @ReprogDias > 0 AND @ReprogVersion = 'N'  )THEN (  DATEADD( DAY, @ReprogDias , @FecTomada ))
				when ( ( @ReprogDias > 0 AND @ReprogVersion = 'S' ) and ( @NewKcRevisio = @cu_revisio ) and (  not (@NewKcRevisio = '')  ) )then ( select max(CH_FECHA) from dbo.CUR_REV where( ( CUR_REV.CU_CODIGO = @NewCuCodigo ) AND ( CUR_REV.CH_REVISIO = @cu_revisio ) ) ) 
				when ( ( @ReprogDias > 0 AND @ReprogVersion = 'S' ) and ((( @NewKcRevisio = @cu_revisio ) and ( @NewKcRevisio = '' ))or ( ( @NewKcRevisio <> @cu_revisio ) ) ) ) then ( DATEADD( DAY, @ReprogDias , @FecTomada ) )              
				when ( @ReprogDias = 0 AND @ReprogVersion = 'N' )then ( DATEADD( DAY, @DiasAntig , @FechaAntig ))			    
				when ( @ReprogDias = 0 AND @ReprogVersion = 'S' )then ( select max(CH_FECHA) from dbo.CUR_REV where( ( CUR_REV.CU_CODIGO = @NewCuCodigo ) AND ( CUR_REV.CH_REVISIO = @cu_revisio ) ) )			    				
				else ( DATEADD( DAY, @DiasAntig , @FechaAntig ) )End );
		
		  update KARCURSO set KC_FECPROG = @FechaProgram WHERE karcurso.cb_codigo = @NewCbCodigo and karcurso.kc_fec_tom = @NewKcFecTom and karcurso.cu_codigo = @NewCuCodigo;
	 end;
	 if ( Update ( KC_REVISIO ) and @ReprogVersion = 'S'  )
     begin	
            If( (@FechaRevision is null) or (@FechaRevision = '12/30/1899') )
			begin 	
			       if (@ReprogDias > 0 )
				   begin
						Select @FechaProgram = max(KC_FECPROG) from KARCURSO where (kc_fec_tom = @NewKcFecTom and CU_CODIGO = @NewCuCodigo and CB_CODIGO = @NewCbCodigo);
				   end
				   else
						set @FechaProgram = DATEADD( DAY, @DiasAntig , @FechaAntig );
					
			end			
			else
			begin				
				set @FechaProgram = @FechaRevision;				
			end;
			
			update KARCURSO set KC_FECPROG = @FechaProgram WHERE karcurso.cb_codigo = @NewCbCodigo and karcurso.kc_fec_tom = @NewKcFecTom and karcurso.cu_codigo = @NewCuCodigo;
	 end        
END
GO

