/************************************************/
/************************************************/
/**********   Base De Datos DATOS      **********/
/************************************************/
/************************************************/

/************************************************/
/***** Tablas Modificadas ***********************/
/************************************************/

/* Tabla AUSENCIA: Horas no trabajadas en un d�a ( Sugerencia QUINCENALES ) */
/* Patch 395 # Seq: 24 Agregado */
alter table AUSENCIA add AU_HORASNT Horas
GO

/* Tabla PERIODO: Fecha de inicio del per�odo de Asistencia ( Sugerencia QUINCENALES ) */
/* Patch 395 # Seq: 25 Agregado */
alter table PERIODO add PE_ASI_INI Fecha
GO

/* Tabla PERIODO: Fecha final del per�odo de Asistencia ( Sugerencia QUINCENALES ) */
/* Patch 395 # Seq: 26 Agregado */
alter table PERIODO add PE_ASI_FIN Fecha
GO

/* Tabla NOMINA: Horas no trabajadas en un per�odo ( Sugerencia QUINCENALES ) */
/* Patch 395 # Seq: 27 Agregado */
alter table NOMINA add NO_HORASNT Horas
GO

/**********************************/
/* Inicializar Columnas Agregadas */
/**********************************/

/* Tabla AUSENCIA: Inicializar horas no trabajadas en un d�a ( Sugerencia QUINCENALES ) */
/* Patch 395 # Seq: 41 Agregado */
update AUSENCIA set AU_HORASNT = 0 where ( AU_HORASNT is NULL )
GO

/* Tabla PERIODO: Inicializaci�n del per�odo de Asistencia ( Sugerencia QUINCENALES ) */
/* Patch 395 # Seq: 42 Agregado */
update PERIODO set PE_ASI_INI = PE_FEC_INI where ( PE_ASI_INI <= '12/30/1899' ) or ( PE_ASI_INI is NULL )
GO

/* Tabla PERIODO: Inicializaci�n del per�odo de Asistencia ( Sugerencia QUINCENALES ) */
/* Patch 395 # Seq: 43 Agregado */
update PERIODO set PE_ASI_FIN = PE_FEC_FIN where ( PE_ASI_FIN <= '12/30/1899' ) or ( PE_ASI_FIN is NULL )
GO

/* Tabla NOMINA: Inicializar horas no trabajadas en un per�odo ( Sugerencia QUINCENALES ) */
/* Patch 395 # Seq: 44 Agregado */
update NOMINA set NO_HORASNT = 0 where ( NO_HORASNT is NULL )
GO

/*********************************************/
/******** # de VERSION ***********************/
/*********************************************/

update GLOBAL set GL_FORMULA = '394' where ( GL_CODIGO = 133 )
GO
