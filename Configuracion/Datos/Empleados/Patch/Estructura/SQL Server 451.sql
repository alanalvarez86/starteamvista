/****************************************/
/****************************************/
/****** Patch para DATOS ****************/
/****************************************/
/****************************************/

/* Patch 451: #13786: Guardar en tabla NOMINA la tabla de prestaciones del empleado (1/9) */
IF not EXISTS (select * from syscolumns where id=object_id('NOMINA') and name='CB_TABLASS')
	alter table NOMINA add CB_TABLASS Codigo1 null
GO
 

/* Patch 451: #13786: Guardar en tabla NOMINA la tabla de prestaciones del empleado (2/9) */
update NOMINA set NOMINA.CB_TABLASS = ''
from NOMINA where NOMINA.CB_TABLASS is null
GO
 
/* Patch 451: #13786: Guardar en tabla NOMINA la tabla de prestaciones del empleado (3/9) */
alter table NOMINA alter column CB_TABLASS Codigo1 not null
GO

/* Patch 451: #13786: Guardar en tabla NOMINA la tabla de prestaciones del empleado (4/9) */
IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_CLAS_NOMINA') AND type IN ( N'P', N'PC' ))
	drop procedure SP_CLAS_NOMINA
GO

/* Patch 451: #13786: Guardar en tabla NOMINA la tabla de prestaciones del empleado (5/9) */
CREATE PROCEDURE SP_CLAS_NOMINA
    	@ANIO SMALLINT,
    	@TIPO SMALLINT,
    	@NUMERO SMALLINT,
    	@EMPLEADO INTEGER,
        @ROTATIVO CHAR(6) OUTPUT
AS
    SET NOCOUNT ON
	declare @FechaIni DATETIME;
 	declare @FechaFin DATETIME;
 	declare @ZONA_GE  CHAR(1);
 	declare @TABLA    CHAR(1);
 	declare @PUESTO   CHAR(6);
 	declare @CLASIFI  CHAR(6);
 	declare @TURNO    CHAR(6);
 	declare @PATRON   CHAR(1);
 	declare @NIVEL1   CHAR(6);
 	declare @NIVEL2   CHAR(6);
 	declare @NIVEL3   CHAR(6);
 	declare @NIVEL4   CHAR(6);
 	declare @NIVEL5   CHAR(6);
 	declare @NIVEL6   CHAR(6);
 	declare @NIVEL7   CHAR(6);
 	declare @NIVEL8   CHAR(6);
 	declare @NIVEL9   CHAR(6);
 	declare @SALARIO  NUMERIC(15,2);
 	declare @SAL_INT  NUMERIC(15,2);
 	declare @PROMEDIA CHAR(1);
 	declare @JORNADA  NUMERIC(15,2);
 	declare @D_TURNO  SMALLINT;
 	declare @ES_ROTA  CHAR(1);
 	declare @FEC_KAR  DATETIME;
 	declare @FEC_SAL  DATETIME;
	declare @BAN_ELE  VARCHAR(30);
	declare @NIVEL0   CHAR(6);
	declare @PLAZA	  INTEGER;	
	declare @CB_NOMINA	SMALLINT;
	declare @FEC_ING	DATETIME;
	declare @FEC_BAJ	DATETIME;
	declare @FEC_NOM	DATETIME;
	declare @Fecha		DATETIME;
	declare @k_NOMINA		SMALLINT;
	declare @CTA_GAS  VARCHAR(30);
	declare @CTA_VAL  VARCHAR(30);
	declare @PAGO	SMALLINT; 
	
  	select @FechaIni = PE_FEC_INI,
               @FechaFin = PE_FEC_FIN
  	from   PERIODO
  	where  PE_YEAR = @Anio 
	and PE_TIPO = @Tipo
	and PE_NUMERO = @Numero

	select 
		@CB_NOMINA = CB_NOMINA
        from   SP_FECHA_KARDEX( @FechaIni, @Empleado )     

	
	SELECT @FEC_ING = CB_FEC_ING, 
		  @FEC_BAJ = CB_FEC_BAJ,
		  @FEC_NOM = CB_FEC_NOM,
		  @k_NOMINA = CB_NOMINA		 
		  from COLABORA where CB_CODIGO = @Empleado

	

	IF ( @FEC_NOM between @FechaIni and @FechaFin ) and ( @CB_NOMINA <> @k_NOMINA )
	begin
		if ( @CB_NOMINA = @Tipo ) 
		begin
			set @Fecha = @FechaIni
		end
		else
		begin
			set @Fecha = @FechaFin
		end 
	end
	else
	begin
		set @Fecha = @FechaFin
	end  
	
	
  	select @ZONA_GE = CB_ZONA_GE,
  		@TABLA = CB_TABLASS,
		@PUESTO = CB_PUESTO,
		@CLASIFI = CB_CLASIFI,
		@TURNO = CB_TURNO,
		@PATRON = CB_PATRON,
                @NIVEL1 = CB_NIVEL1,
		@NIVEL2 = CB_NIVEL2, 
		@NIVEL3 = CB_NIVEL3,
		@NIVEL4 = CB_NIVEL4, 
		@NIVEL5 = CB_NIVEL5,
                @NIVEL6 = CB_NIVEL6,
		@NIVEL7 = CB_NIVEL7, 
		@NIVEL8 = CB_NIVEL8, 
		@NIVEL9 = CB_NIVEL9,
                @SALARIO = CB_SALARIO,
		@SAL_INT = CB_SAL_INT, 
		@FEC_KAR = CB_FEC_KAR, 
		@FEC_SAL = CB_FEC_SAL,
                @BAN_ELE = CB_BAN_ELE,
                @NIVEL0 = CB_NIVEL0,
		@PLAZA	= CB_PLAZA,
		@CTA_GAS = CB_CTA_GAS,
        @CTA_VAL = CB_CTA_VAL
        
  	from   COLABORA
  	where  CB_CODIGO = @Empleado

  	if ( @Fecha < @FEC_KAR ) 
  	begin
    		select 
			@ZONA_GE = CB_ZONA_GE,
			@TABLA = CB_TABLASS,
			@PUESTO = CB_PUESTO,
			@CLASIFI = CB_CLASIFI, 
			@TURNO = CB_TURNO, 
			@PATRON = CB_PATRON,
         	        @NIVEL1 = CB_NIVEL1,
			@NIVEL2 = CB_NIVEL2, 
			@NIVEL3 = CB_NIVEL3, 
			@NIVEL4 = CB_NIVEL4,
			@NIVEL5 = CB_NIVEL5,
         	        @NIVEL6 = CB_NIVEL6,
			@NIVEL7 = CB_NIVEL7, 
			@NIVEL8 = CB_NIVEL8,
			@NIVEL9 = CB_NIVEL9,
         	        @SALARIO = CB_SALARIO,
			@SAL_INT = CB_SAL_INT,
			@PLAZA	= CB_PLAZA
            from   SP_FECHA_KARDEX( @Fecha, @Empleado )
  	end

  	select @PROMEDIA = GL_FORMULA
  	from   GLOBAL
  	where  GL_CODIGO = 42

  	if (( @PROMEDIA = 'S' ) and ( @FEC_SAL > @FechaIni ))
        EXECUTE SP_RANGO_SALARIO @FechaIni, @FechaFin, @Empleado,
                                 @SALARIO OUTPUT, @SAL_INT OUTPUT

	EXECUTE SP_DIAS_JORNADA @Turno, @FechaIni, @FechaFin,
    				@D_Turno OUTPUT, @Jornada OUTPUT, @Es_Rota OUTPUT

	if ( @ES_ROTA = 'S' )
    		SET @Rotativo = @Turno
  	else
    		SET @Rotativo = '';

	set @PAGO = dbo.SP_GET_METODO_PAGO_DEFAULT() 

  	update NOMINA
  	set     CB_ZONA_GE = @ZONA_GE,
			CB_TABLASS = @TABLA,
        	CB_PUESTO  = @PUESTO,
        	CB_CLASIFI = @CLASIFI,
        	CB_TURNO   = @TURNO,
        	CB_PATRON  = @PATRON,
        	CB_NIVEL1  = @NIVEL1,
        	CB_NIVEL2  = @NIVEL2,
        	CB_NIVEL3  = @NIVEL3,
        	CB_NIVEL4  = @NIVEL4,
        	CB_NIVEL5  = @NIVEL5,
        	CB_NIVEL6  = @NIVEL6,
        	CB_NIVEL7  = @NIVEL7,
        	CB_NIVEL8  = @NIVEL8,
        	CB_NIVEL9  = @NIVEL9,
        	CB_SALARIO = @SALARIO,
        	CB_SAL_INT = @SAL_INT,
        	NO_JORNADA = @JORNADA,
        	NO_D_TURNO = @D_TURNO,
                CB_BAN_ELE = @BAN_ELE,
                CB_NIVEL0  = @NIVEL0,
                CB_PLAZA   = @PLAZA,
				CB_CTA_GAS = @CTA_GAS,
                CB_CTA_VAL = @CTA_VAL,
			NO_PAGO = case  when  NO_PAGO > 0  then NO_PAGO else @PAGO  end 

  	where PE_YEAR = @Anio
	and PE_TIPO = @Tipo
	and PE_NUMERO = @Numero
	and CB_CODIGO = @Empleado;
GO


/* Patch 451: #13786: Guardar en tabla NOMINA la tabla de prestaciones del empleado (6/9) */
IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_COPIA_NOMINA') AND type IN ( N'P', N'PC' ))
	drop procedure SP_COPIA_NOMINA
go

/* Patch 451: #13786: Guardar en tabla NOMINA la tabla de prestaciones del empleado (7/9) */
CREATE procedure SP_COPIA_NOMINA(
@YEARORIGINAL SMALLINT,
@TIPOORIGINAL SMALLINT,
@NUMEROORIGINAL SMALLINT,
@YEARNUEVO SMALLINT,
@TIPONUEVO SMALLINT,
@NUMERONUEVO SMALLINT,
@EMPLEADO INTEGER )
as
begin
     SET NOCOUNT ON
     declare @Status SmallInt;
     select @Status = N.NO_STATUS from DBO.NOMINA as N
     where ( N.PE_YEAR = @YEARORIGINAL ) and
           ( N.PE_TIPO = @TIPOORIGINAL ) and
           ( N.PE_NUMERO = @NUMEROORIGINAL ) and
           ( N.CB_CODIGO = @EMPLEADO );
     if ( @Status is not Null )
     begin
          if ( @Status > 4 )
          begin
               set @Status = 4;
          end;
          insert into DBO.NOMINA( PE_YEAR,
                                  PE_TIPO,
                                  PE_NUMERO,
                                  CB_CODIGO,
                                  CB_CLASIFI,
                                  CB_TURNO,
                                  CB_PATRON,
                                  CB_PUESTO,
                                  CB_ZONA_GE,
                                  NO_FOLIO_1,
                                  NO_FOLIO_2,
                                  NO_FOLIO_3,
                                  NO_FOLIO_4,
                                  NO_FOLIO_5,
                                  NO_OBSERVA,
                                  NO_STATUS,
                                  US_CODIGO,
                                  NO_USER_RJ,
                                  CB_NIVEL1,
                                  CB_NIVEL2,
                                  CB_NIVEL3,
                                  CB_NIVEL4,
                                  CB_NIVEL5,
                                  CB_NIVEL6,
                                  CB_NIVEL7,
                                  CB_NIVEL8,
                                  CB_NIVEL9,
                                  CB_SAL_INT,
                                  CB_SALARIO,
                                  NO_DIAS,
                                  NO_ADICION,
                                  NO_DEDUCCI,
                                  NO_NETO,
                                  NO_DES_TRA,
                                  NO_DIAS_AG,
                                  NO_DIAS_VA,
                                  NO_DOBLES,
                                  NO_EXTRAS,
                                  NO_FES_PAG,
                                  NO_FES_TRA,
                                  NO_HORA_CG,
                                  NO_HORA_SG,
                                  NO_HORAS,
                                  NO_IMP_CAL,
                                  NO_JORNADA,
                                  NO_PER_CAL,
                                  NO_PER_MEN,
                                  NO_PERCEPC,
                                  NO_TARDES,
                                  NO_TRIPLES,
                                  NO_TOT_PRE,
                                  NO_VAC_TRA,
                                  NO_X_CAL,
                                  NO_X_ISPT,
                                  NO_X_MENS,
                                  NO_D_TURNO,
                                  NO_DIAS_AJ,
                                  NO_DIAS_AS,
                                  NO_DIAS_CG,
                                  NO_DIAS_EM,
                                  NO_DIAS_FI,
                                  NO_DIAS_FJ,
                                  NO_DIAS_FV,
                                  NO_DIAS_IN,
                                  NO_DIAS_NT,
                                  NO_DIAS_OT,
                                  NO_DIAS_RE,
                                  NO_DIAS_SG,
                                  NO_DIAS_SS,
                                  NO_DIAS_SU,
                                  NO_HORA_PD,
                                  NO_LIQUIDA,
                                  NO_FUERA,
                                  NO_EXENTAS,
                                  NO_FEC_PAG,
                                  NO_USR_PAG,
								  CB_NIVEL0,
								  CB_BAN_ELE,
								  NO_HORAPDT,
							      NO_HORASNT,
								  NO_DIAS_SI,
								  NO_DIAS_VJ,
							      NO_DIAS_PV,
								  NO_SUP_OK,
								  NO_FEC_OK,
								  NO_HOR_OK,
								  NO_ASI_INI,
								  NO_ASI_FIN,
								  NO_DIAS_BA,
								  NO_PREV_GR,
								  CB_PLAZA,
								  NO_FEC_LIQ,
								  CB_CTA_GAS,
								  CB_CTA_VAL,
								  NO_PER_ISN,
                  NO_DIAS_IH,
                  NO_DIAS_ID,
                  NO_DIAS_IT,
                  NO_TOTAL_1,
                  NO_TOTAL_2,
                  NO_TOTAL_3,
                  NO_TOTAL_4,
                  NO_TOTAL_5,
                  NO_FACT_BA,
                  NO_DIAS_DT,
                  NO_DIAS_FS,
                  NO_DIAS_FT,
				  CB_TABLASS )
          select @YEARNUEVO,
                 @TIPONUEVO,
                 @NUMERONUEVO,
                 N2.CB_CODIGO,
                 N2.CB_CLASIFI,
                 N2.CB_TURNO,
                 N2.CB_PATRON,
                 N2.CB_PUESTO,
                 N2.CB_ZONA_GE,
                 N2.NO_FOLIO_1,
                 N2.NO_FOLIO_2,
                 N2.NO_FOLIO_3,
                 N2.NO_FOLIO_4,
                 N2.NO_FOLIO_5,
                 N2.NO_OBSERVA,
                 @Status,
                 N2.US_CODIGO,
                 N2.NO_USER_RJ,
                 N2.CB_NIVEL1,
                 N2.CB_NIVEL2,
                 N2.CB_NIVEL3,
                 N2.CB_NIVEL4,
                 N2.CB_NIVEL5,
                 N2.CB_NIVEL6,
                 N2.CB_NIVEL7,
                 N2.CB_NIVEL8,
                 N2.CB_NIVEL9,
                 N2.CB_SAL_INT,
                 N2.CB_SALARIO,
                 N2.NO_DIAS,
                 N2.NO_ADICION,
                 N2.NO_DEDUCCI,
                 N2.NO_NETO,
                 N2.NO_DES_TRA,
                 N2.NO_DIAS_AG,
                 N2.NO_DIAS_VA,
                 N2.NO_DOBLES,
                 N2.NO_EXTRAS,
                 N2.NO_FES_PAG,
                 N2.NO_FES_TRA,
                 N2.NO_HORA_CG,
                 N2.NO_HORA_SG,
                 N2.NO_HORAS,
                 N2.NO_IMP_CAL,
                 N2.NO_JORNADA,
                 N2.NO_PER_CAL,
                 N2.NO_PER_MEN,
                 N2.NO_PERCEPC,
                 N2.NO_TARDES,
                 N2.NO_TRIPLES,
                 N2.NO_TOT_PRE,
                 N2.NO_VAC_TRA,
                 N2.NO_X_CAL,
                 N2.NO_X_ISPT,
                 N2.NO_X_MENS,
                 N2.NO_D_TURNO,
                 N2.NO_DIAS_AJ,
                 N2.NO_DIAS_AS,
                 N2.NO_DIAS_CG,
                 N2.NO_DIAS_EM,
                 N2.NO_DIAS_FI,
                 N2.NO_DIAS_FJ,
                 N2.NO_DIAS_FV,
                 N2.NO_DIAS_IN,
                 N2.NO_DIAS_NT,
                 N2.NO_DIAS_OT,
                 N2.NO_DIAS_RE,
                 N2.NO_DIAS_SG,
                 N2.NO_DIAS_SS,
                 N2.NO_DIAS_SU,
                 N2.NO_HORA_PD,
                 N2.NO_LIQUIDA,
                 N2.NO_FUERA,
                 N2.NO_EXENTAS,
                 N2.NO_FEC_PAG,
                 N2.NO_USR_PAG,
				 N2.CB_NIVEL0,
				 N2.CB_BAN_ELE,
				 N2.NO_HORAPDT,
				 N2.NO_HORASNT,
				 N2.NO_DIAS_SI,
				 N2.NO_DIAS_VJ,
				 N2.NO_DIAS_PV,
				 N2.NO_SUP_OK,
				 N2.NO_FEC_OK,
				 N2.NO_HOR_OK,
				 N2.NO_ASI_INI,
				 N2.NO_ASI_FIN,
				 N2.NO_DIAS_BA,
				 N2.NO_PREV_GR,
				 N2.CB_PLAZA,
				 N2.NO_FEC_LIQ,
				 N2.CB_CTA_GAS,
				 N2.CB_CTA_VAL,
				 N2.NO_PER_ISN,
         N2.NO_DIAS_IH,
         N2.NO_DIAS_ID,
         N2.NO_DIAS_IT,
         N2.NO_TOTAL_1,
         N2.NO_TOTAL_2,
         N2.NO_TOTAL_3,
         N2.NO_TOTAL_4,
         N2.NO_TOTAL_5,
         N2.NO_FACT_BA,
         N2.NO_DIAS_DT,
         N2.NO_DIAS_FS,
         N2.NO_DIAS_FT,
		 N2.CB_TABLASS
          from DBO.NOMINA as N2 where ( N2.PE_YEAR = @YEARORIGINAL ) and
                                      ( N2.PE_TIPO = @TIPOORIGINAL ) and
                                      ( N2.PE_NUMERO = @NUMEROORIGINAL ) and
                                      ( N2.CB_CODIGO = @EMPLEADO );
          insert into DBO.MOVIMIEN( PE_YEAR,
                                    PE_TIPO,
                                    PE_NUMERO,
                                    CB_CODIGO,
                                    MO_ACTIVO,
                                    CO_NUMERO,
                                    MO_IMP_CAL,
                                    US_CODIGO,
                                    MO_PER_CAL,
                                    MO_REFEREN,
                                    MO_X_ISPT,
                                    MO_PERCEPC,
                                    MO_DEDUCCI )
          select @YEARNUEVO,
                 @TIPONUEVO,
                 @NUMERONUEVO,
                 M2.CB_CODIGO,
                 M2.MO_ACTIVO,
                 M2.CO_NUMERO,
                 M2.MO_IMP_CAL,
                 M2.US_CODIGO,
                 M2.MO_PER_CAL,
                 M2.MO_REFEREN,
                 M2.MO_X_ISPT,
                 M2.MO_PERCEPC,
                 M2.MO_DEDUCCI
          from DBO.MOVIMIEN as M2 where ( M2.PE_YEAR = @YEARORIGINAL ) and
                                        ( M2.PE_TIPO = @TIPOORIGINAL ) and
                                        ( M2.PE_NUMERO = @NUMEROORIGINAL ) and
                                        ( M2.CB_CODIGO = @EMPLEADO );
          insert into DBO.FALTAS( PE_YEAR,
                                  PE_TIPO,
                                  PE_NUMERO,
                                  CB_CODIGO,
                                  FA_FEC_INI,
                                  FA_DIA_HOR,
                                  FA_MOTIVO,
                                  FA_DIAS,
                                  FA_HORAS )
          select @YEARNUEVO,
                 @TIPONUEVO,
                 @NUMERONUEVO,
                 F2.CB_CODIGO,
                 F2.FA_FEC_INI,
                 F2.FA_DIA_HOR,
                 F2.FA_MOTIVO,
                 F2.FA_DIAS,
                 F2.FA_HORAS
          from DBO.FALTAS as F2 where ( F2.PE_YEAR = @YEARORIGINAL ) and
                                      ( F2.PE_TIPO = @TIPOORIGINAL ) and
                                      ( F2.PE_NUMERO = @NUMEROORIGINAL ) and
                                      ( F2.CB_CODIGO = @EMPLEADO );
     end;
end
GO

/* Patch 451: #13786: Guardar en tabla NOMINA la tabla de prestaciones del empleado (8/9) */
IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_CANCELA_NOMINA') AND type IN ( N'P', N'PC' ))
	drop procedure SP_CANCELA_NOMINA
GO

/* Patch 451: #13786: Guardar en tabla NOMINA la tabla de prestaciones del empleado (9/9) */
CREATE procedure SP_CANCELA_NOMINA(
@YEARORIGINAL SMALLINT,
@TIPOORIGINAL SMALLINT,
@NUMEROORIGINAL SMALLINT,
@YEARNUEVO SMALLINT,
@TIPONUEVO SMALLINT,
@NUMERONUEVO SMALLINT,
@EMPLEADO INTEGER,
@USUARIO SMALLINT )
as
begin
     SET NOCOUNT ON
     declare @StatusNuevo SmallInt;
     select @StatusNuevo = N.NO_STATUS from DBO.NOMINA as N
     where ( N.PE_YEAR = @YEARORIGINAL ) and
           ( N.PE_TIPO = @TIPOORIGINAL ) and
           ( N.PE_NUMERO = @NUMEROORIGINAL ) and
           ( N.CB_CODIGO = @EMPLEADO );
     if ( @StatusNuevo is not Null )
     begin
          if ( @StatusNuevo > 4 )
             set @StatusNuevo = 4;
          insert into DBO.NOMINA( PE_YEAR,
                                  PE_TIPO,
                                  PE_NUMERO,
                                  CB_CODIGO,
                                  CB_CLASIFI,
                                  CB_TURNO,
                                  CB_PATRON,
                                  CB_PUESTO,
                                  CB_ZONA_GE,
                                  NO_FOLIO_1,
                                  NO_FOLIO_2,
                                  NO_FOLIO_3,
                                  NO_FOLIO_4,
                                  NO_FOLIO_5,
                                  NO_OBSERVA,
                                  NO_STATUS,
                                  US_CODIGO,
                                  NO_USER_RJ,
                                  CB_NIVEL1,
                                  CB_NIVEL2,
                                  CB_NIVEL3,
                                  CB_NIVEL4,
                                  CB_NIVEL5,
                                  CB_NIVEL6,
                                  CB_NIVEL7,
                                  CB_NIVEL8,
                                  CB_NIVEL9,
                                  CB_SAL_INT,
                                  CB_SALARIO,
                                  NO_DIAS,
                                  NO_ADICION,
                                  NO_DEDUCCI,
                                  NO_NETO,
                                  NO_DES_TRA,
                                  NO_DIAS_AG,
                                  NO_DIAS_VA,
                                  NO_DOBLES,
                                  NO_EXTRAS,
                                  NO_FES_PAG,
                                  NO_FES_TRA,
                                  NO_HORA_CG,
                                  NO_HORA_SG,
                                  NO_HORAS,
                                  NO_IMP_CAL,
                                  NO_JORNADA,
                                  NO_PER_CAL,
                                  NO_PER_MEN,
                                  NO_PERCEPC,
                                  NO_TARDES,
                                  NO_TRIPLES,
                                  NO_TOT_PRE,
                                  NO_VAC_TRA,
                                  NO_X_CAL,
                                  NO_X_ISPT,
                                  NO_X_MENS,
                                  NO_D_TURNO,
                                  NO_DIAS_AJ,
                                  NO_DIAS_AS,
                                  NO_DIAS_CG,
                                  NO_DIAS_EM,
                                  NO_DIAS_FI,
                                  NO_DIAS_FJ,
                                  NO_DIAS_FV,
                                  NO_DIAS_IN,
                                  NO_DIAS_NT,
                                  NO_DIAS_OT,
                                  NO_DIAS_RE,
                                  NO_DIAS_SG,
                                  NO_DIAS_SS,
                                  NO_DIAS_SU,
                                  NO_HORA_PD,
                                  NO_LIQUIDA,
                                  NO_FUERA,
                                  NO_EXENTAS,
                                  NO_FEC_PAG,
                                  NO_USR_PAG,
								  CB_NIVEL0,
								  CB_BAN_ELE,
								  NO_HORAPDT,
								  NO_HORASNT,
								  NO_DIAS_SI,
								  NO_DIAS_VJ,
							      NO_DIAS_PV,
								  NO_SUP_OK,
								  NO_FEC_OK,
								  NO_HOR_OK,
								  NO_ASI_INI,
								  NO_ASI_FIN,
								  NO_DIAS_BA,
								  NO_PREV_GR,
								  CB_PLAZA,
								  NO_FEC_LIQ,
								  CB_CTA_GAS,
								  CB_CTA_VAL,
								  NO_PER_ISN,
                  NO_DIAS_IH,
                  NO_DIAS_ID,
                  NO_DIAS_IT,
                  NO_TOTAL_1,
                  NO_TOTAL_2,
                  NO_TOTAL_3,
                  NO_TOTAL_4,
                  NO_TOTAL_5,
                  NO_FACT_BA,
                  NO_DIAS_DT,
                  NO_DIAS_FS,
                  NO_DIAS_FT,
				  CB_TABLASS
								  )
          select @YEARNUEVO,
                 @TIPONUEVO,
                 @NUMERONUEVO,
                 N1.CB_CODIGO,
                 N1.CB_CLASIFI,
                 N1.CB_TURNO,
                 N1.CB_PATRON,
                 N1.CB_PUESTO,
                 N1.CB_ZONA_GE,
                 N1.NO_FOLIO_1,
                 N1.NO_FOLIO_2,
                 N1.NO_FOLIO_3,
                 N1.NO_FOLIO_4,
                 N1.NO_FOLIO_5,
                 N1.NO_OBSERVA,
                 @StatusNuevo,
                 N1.US_CODIGO,
                 N1.NO_USER_RJ,
                 N1.CB_NIVEL1,
                 N1.CB_NIVEL2,
                 N1.CB_NIVEL3,
                 N1.CB_NIVEL4,
                 N1.CB_NIVEL5,
                 N1.CB_NIVEL6,
                 N1.CB_NIVEL7,
                 N1.CB_NIVEL8,
                 N1.CB_NIVEL9,
                 N1.CB_SAL_INT,
                 N1.CB_SALARIO,
                 -N1.NO_DIAS,
                 -N1.NO_ADICION,
                 -N1.NO_DEDUCCI,
                 -N1.NO_NETO,
                 -N1.NO_DES_TRA,
                 -N1.NO_DIAS_AG,
                 -N1.NO_DIAS_VA,
                 -N1.NO_DOBLES,
                 -N1.NO_EXTRAS,
                 -N1.NO_FES_PAG,
                 -N1.NO_FES_TRA,
                 -N1.NO_HORA_CG,
                 -N1.NO_HORA_SG,
                 -N1.NO_HORAS,
                 -N1.NO_IMP_CAL,
                 -N1.NO_JORNADA,
                 -N1.NO_PER_CAL,
                 -N1.NO_PER_MEN,
                 -N1.NO_PERCEPC,
                 -N1.NO_TARDES,
                 -N1.NO_TRIPLES,
                 -N1.NO_TOT_PRE,
                 -N1.NO_VAC_TRA,
                 -N1.NO_X_CAL,
                 -N1.NO_X_ISPT,
                 -N1.NO_X_MENS,
                 -N1.NO_D_TURNO,
                 -N1.NO_DIAS_AJ,
                 -N1.NO_DIAS_AS,
                 -N1.NO_DIAS_CG,
                 -N1.NO_DIAS_EM,
                 -N1.NO_DIAS_FI,
                 -N1.NO_DIAS_FJ,
                 -N1.NO_DIAS_FV,
                 -N1.NO_DIAS_IN,
                 -N1.NO_DIAS_NT,
                 -N1.NO_DIAS_OT,
                 -N1.NO_DIAS_RE,
                 -N1.NO_DIAS_SG,
                 -N1.NO_DIAS_SS,
                 -N1.NO_DIAS_SU,
                 -N1.NO_HORA_PD,
                 N1.NO_LIQUIDA,
                 N1.NO_FUERA,
                 -N1.NO_EXENTAS,
                 N1.NO_FEC_PAG,
                 N1.NO_USR_PAG,
				 N1.CB_NIVEL0,
				 N1.CB_BAN_ELE,
				 -N1.NO_HORAPDT,
				 -N1.NO_HORASNT,
				 -N1.NO_DIAS_SI,
				 -N1.NO_DIAS_VJ,
				 -N1.NO_DIAS_PV,
				 N1.NO_SUP_OK,
				 N1.NO_FEC_OK,
				 N1.NO_HOR_OK,
				 N1.NO_ASI_INI,
				 N1.NO_ASI_FIN,
				 -N1.NO_DIAS_BA,
				 -N1.NO_PREV_GR,
				 N1.CB_PLAZA,
				 N1.NO_FEC_LIQ,
				 N1.CB_CTA_GAS,
				 N1.CB_CTA_VAL,
				 -N1.NO_PER_ISN,
         -N1.NO_DIAS_IH,
         -N1.NO_DIAS_ID,
         -N1.NO_DIAS_IT,
         -N1.NO_TOTAL_1,
         -N1.NO_TOTAL_2,
         -N1.NO_TOTAL_3,
         -N1.NO_TOTAL_4,
         -N1.NO_TOTAL_5,
         -N1.NO_FACT_BA,
         -N1.NO_DIAS_DT,
         -N1.NO_DIAS_FS,
         -N1.NO_DIAS_FT,
		 N1.CB_TABLASS
          from DBO.NOMINA as N1 where
          ( N1.PE_YEAR = @YEARORIGINAL ) and
          ( N1.PE_TIPO = @TIPOORIGINAL ) and
          ( N1.PE_NUMERO = @NUMEROORIGINAL ) and
          ( N1.CB_CODIGO = @EMPLEADO );
          insert into DBO.MOVIMIEN( PE_YEAR,
                                    PE_TIPO,
                                    PE_NUMERO,
                                    CB_CODIGO,
                                    MO_ACTIVO,
                                    CO_NUMERO,
                                    MO_IMP_CAL,
                                    US_CODIGO,
                                    MO_PER_CAL,
                                    MO_REFEREN,
                                    MO_X_ISPT,
                                    MO_PERCEPC,
                                    MO_DEDUCCI )
          select @YEARNUEVO,
                 @TIPONUEVO,
                 @NUMERONUEVO,
                 M1.CB_CODIGO,
                 M1.MO_ACTIVO,
                 M1.CO_NUMERO,
                 -M1.MO_IMP_CAL,
                 @USUARIO,
                 M1.MO_PER_CAL,
                 M1.MO_REFEREN,
                 -M1.MO_X_ISPT,
                 -M1.MO_PERCEPC,
                 -M1.MO_DEDUCCI
          from DBO.MOVIMIEN as M1 where
          ( M1.PE_YEAR = @YEARORIGINAL ) and
          ( M1.PE_TIPO = @TIPOORIGINAL ) and
          ( M1.PE_NUMERO = @NUMEROORIGINAL ) and
          ( M1.CB_CODIGO = @EMPLEADO );
          insert into DBO.FALTAS( PE_YEAR,
                              PE_TIPO,
                              PE_NUMERO,
                              CB_CODIGO,
                              FA_FEC_INI,
                              FA_DIA_HOR,
                              FA_MOTIVO,
                              FA_DIAS,
                              FA_HORAS )
          select @YEARNUEVO,
                 @TIPONUEVO,
                 @NUMERONUEVO,
                 F1.CB_CODIGO,
                 F1.FA_FEC_INI,
                 F1.FA_DIA_HOR,
                 F1.FA_MOTIVO,
                 -F1.FA_DIAS,
                 -F1.FA_HORAS
          from DBO.FALTAS as F1 where
          ( F1.PE_YEAR = @YEARORIGINAL ) and
          ( F1.PE_TIPO = @TIPOORIGINAL ) and
          ( F1.PE_NUMERO = @NUMEROORIGINAL ) and
          ( F1.CB_CODIGO = @EMPLEADO );
     end
end
GO

/* Patch 451: #13263: Pensi�n alimenticia (1/3) */
if not exists (SELECT * FROM syscolumns WHERE id=object_id('PENSION') AND name='PS_ACTIVA'    )
	 alter table PENSION add PS_ACTIVA Codigo1 NULL
go 

/* Patch 451: #13263: Pensi�n alimenticia (2/3) */
update PENSION set PS_ACTIVA = 'S' where PS_ACTIVA is null 
go 

/* Patch 451: #13263: Pensi�n alimenticia (3/3) */
alter table PENSION alter column PS_ACTIVA Codigo1 NOT NULL 
go


IF EXISTS ( SELECT *  FROM SYSOBJECTS WHERE NAME = 'SP_DASHLET_ROTACION_SUBTOTAL'  AND XTYPE = 'P' )
DROP procedure SP_DASHLET_ROTACION_SUBTOTAL

GO 

create PROCEDURE SP_DASHLET_ROTACION_SUBTOTAL
	(@USUARIO SMALLINT,
	 @ESTEGRUPO CHAR(6),
	 @ESTADESCRIPCION VARCHAR(30),
	 @INICIO INTEGER,
	 @ALTAS INTEGER,
 	 @BAJAS INTEGER,
         @DIASACTIVOS NUMERIC(15,2),
	 @DIASRANGO NUMERIC(15,2),
	 @FECHAINI DATETIME	 )
AS
BEGIN
     SET NOCOUNT ON
     declare @Promedio NUMERIC(15,2);
     if ( @DiasRAngo = 0 )
     begin
          Set @Promedio = 0;
     end
     else
     begin
          Set @Promedio = @DiasActivos / @DiasRango;
     end
     insert into #TMPROTA
     (TR_GRUPO, TR_DESCRIP, TR_INICIO, TR_FINAL, TR_ALTAS, TR_BAJAS, TR_PROM, TR_FECHA, TR_USER ) values
     (@EsteGrupo, @EstaDescripcion, @Inicio, @Inicio+@Altas-@Bajas, @Altas, @Bajas, @Promedio, @FECHAINI, @USUARIO );
end

GO

IF EXISTS ( SELECT *  FROM SYSOBJECTS WHERE NAME = 'SP_DASHLET_ROTACION'  AND XTYPE = 'P' )
DROP procedure SP_DASHLET_ROTACION

GO 

CREATE PROCEDURE SP_DASHLET_ROTACION 
	( @FECHAINI FECHA,
	 @FECHAFIN FECHA, 
	 @CM_NIVEL0 Formula )
AS
BEGIN
    SET NOCOUNT ON
	DECLARE @USUARIO int
	DECLARE @ESTEGRUPO CHAR(6);
	DECLARE @ESTADESCRIPCION VARCHAR(30);
	DECLARE @INICIO INTEGER;
	DECLARE @ALTAS  INTEGER;
	DECLARE @BAJAS  INTEGER;
	DECLARE @DIASACTIVOS INTEGER;  
  	DECLARE @GRUPO CHAR(6);
  	DECLARE @DESCRIPCION VARCHAR(30);
  	DECLARE @FECING DATETIME;
  	DECLARE @FECBAJ DATETIME;
  	DECLARE @DIASRANGO INTEGER;
  	DECLARE @NUMGRUPOS INTEGER;	

	set @USUARIO = 1 
	
	CREATE TABLE #TMPROTAI(
		TR_USER int ,
		CB_CODIGO int ,
		TR_GRUPO varchar(6) ,
		TR_DESCRIP varchar(30) ,
		CB_FEC_ING Datetime ,
		CB_FEC_BAJ Datetime
	) 

	CREATE TABLE #TMPROTA(
		TR_USER int ,
		TR_GRUPO varchar(6) ,
		TR_DESCRIP varchar(30) ,
		TR_INICIO int,
		TR_FINAL int ,
		TR_ALTAS int ,
		TR_BAJAS int ,
		TR_PROM Decimal(15,2) ,
		TR_FECHA Datetime 
	) 
 	

        SET @NumGrupos = 0
	SET @DiasRango = DATEDIFF(day, @FechaIni, @FechaFin )+1
	
	insert into #TMPROTAI (TR_USER, CB_CODIGO, TR_GRUPO, TR_DESCRIP, CB_FEC_ING, CB_FEC_BAJ) 
	select @USUARIO, COLABORA.CB_CODIGO, '', '', COLABORA.CB_FEC_ING, COLABORA.CB_FEC_BAJ from COLABORA 
	where
	(
	 (( COLABORA.CB_FEC_ING < @FECHAFIN ) AND 
      ( COLABORA.CB_FEC_BAJ < COLABORA.CB_FEC_ING OR COLABORA.CB_FEC_BAJ >= @FECHAINI )) OR 
	  ( COLABORA.CB_FEC_BAJ < COLABORA.CB_FEC_ING AND COLABORA.CB_FEC_BAJ>=@FECHAINI ) 
	)
	and (@CM_NIVEL0 = ''  or ( COLABORA.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and COLABORA.CB_NIVEL0=''))



	Declare Temporal CURSOR for
 		select TR_GRUPO, TR_DESCRIP, CB_FEC_ING, CB_FEC_BAJ
 		from   #TMPROTAI
 		where  TR_USER = @Usuario
        	order by TR_GRUPO
	OPEN TEMPORAL
	fetch NEXT FROM Temporal 
	into   @Grupo, @Descripcion, @FecIng, @FecBaj 
		WHILE @@FETCH_STATUS = 0 
		BEGIN
    			if (( @NumGrupos = 0 ) or ( @EsteGrupo <> @Grupo )) 
            		begin
               
               			if ( @NumGrupos > 0 ) 
                  		EXECUTE sp_SUBTOTAL_ROTACION 
                           		@Usuario, @EsteGrupo, @EstaDescripcion, @Inicio,
                           		@Altas, @Bajas, @DiasActivos, @DiasRango, @FechaIni 

	               		SELECT	@NumGrupos = @NumGrupos + 1, @EsteGrupo = @Grupo,
	               			@EstaDescripcion = @Descripcion,
               				@Inicio = 0,
               				@Altas = 0,
               				@Bajas = 0,            
               				@DiasActivos = 0
            		end

            		if (( @FecIng >= @FechaIni ) and ( @FecIng <= @FechaFin ))
               			SET @Altas = @Altas + 1;

            		if (( @FecBaj >= @FechaIni ) and ( @FecBaj <= @FechaFin ))
               			SET @Bajas = @Bajas + 1;

            		if (( @FecIng < @FechaIni ) or (( @FecBaj < @FecIng ) and ( @FecBaj >= @FechaIni )))
            			SELECT @Inicio = @Inicio + 1, @FecIng = @FechaIni

            		if (( @FecBaj > @FechaFin ) or ( @FecBaj < @FecIng )) 
               			SET @FecBaj = @FechaFin;

            		SET @DiasActivos = DATEDIFF(day, @FecIng,@FecBaj) + 1 + @DiasActivos ;
			fetch NEXT FROM Temporal 
 			into   @Grupo, @Descripcion, @FecIng, @FecBaj 
  		END;

   	close Temporal
    	deallocate Temporal
        

        if ( @NumGrupos > 0 ) 
           EXECUTE SP_DASHLET_ROTACION_SUBTOTAL 
                   @Usuario, @EsteGrupo, @EstaDescripcion, @Inicio,
                   @Altas, @Bajas, @DiasActivos, @DiasRango, @FechaIni 

  	
	/*select ID=1, Descripcion='Inicio' , Cantidad= TR_INICIO*1.0 from #TMPROTA 
	union all */
	select ID=2, Descripcion='Altas' , Cantidad= TR_ALTAS*1.0 from #TMPROTA 
	union all 
	select ID=3, Descripcion='Bajas' , Cantidad= TR_BAJAS*1.0 from #TMPROTA 
	/*union all 
	select ID=4, Descripcion='Final' , Cantidad= TR_FINAL*1.0 from #TMPROTA 
	union all 
	select ID=4, Descripcion='Promedio' , Cantidad= TR_PROM from #TMPROTA */
	
	drop table #TMPROTA
	drop table #TMPROTAI 

END

go 

IF EXISTS ( SELECT *  FROM SYSOBJECTS WHERE NAME = 'FN_Hour2DateTime'  AND XTYPE = 'FN' )
DROP FUNCTION FN_Hour2DateTime
go

create function FN_Hour2DateTime(@Fecha Fecha, @Hora Hora )
returns Fecha 
as
begin 
	declare @FechaTmp Fecha
	declare @HoraTmp Hora
	declare @iHora smallint 
	declare @sHora varchar(2) 
	set @HoraTmp = coalesce( @Hora , '' ) 	
	
	if ( @Hora <> '') 
	begin  
		if ( @Hora >= '2400' ) 
		begin 
			set @Fecha = dateadd( day, 1, @Fecha ) 
			set @iHora = cast( SUBSTRING(@Hora, 1, 2)  as smallint ) - 24 
			set @sHora = ltrim( str( @iHora ) )
			set @FechaTmp =  cast ( (  @sHora + ':'+  SUBSTRING(@Hora, 3, 2) ) as datetime ) 	
			set @FechaTmp = dateadd(dd, datediff(dd,0, @Fecha), 0)  + @FechaTmp
		end 
		else
		begin 
			set @FechaTmp =  cast ( (  SUBSTRING(@Hora, 1, 2) + ':'+  SUBSTRING(@Hora, 3, 2) ) as datetime ) 	
			set @FechaTmp = dateadd(dd, datediff(dd,0, @Fecha), 0)  + @FechaTmp
		end 
	end 
	else
		set @FechaTmp =  null 		

	return @FechaTmp 
end 

go 

IF EXISTS ( SELECT *  FROM SYSOBJECTS WHERE NAME = 'FN_GeneraRitmo'  AND XTYPE = 'TF' )
DROP FUNCTION FN_GeneraRitmo
go

CREATE  FUNCTION FN_GeneraRitmo(@patron AS Varchar(255), @horario1 Codigo, @horario2 Codigo, @horario3 Codigo )
RETURNS
 @Result TABLE(
	        iDia int identity(0,1), 
			HO_CODIGO  varchar(20), 
			TipoDia		int			
			 )      
AS
BEGIN
	  DECLARE @ritmos as table (
	        i int identity(0,1), 
			Patron varchar(20), 			
			HO_CODIGO  varchar(20), 
			TipoDia		int,
			Dias		int			
			 )
	
      DECLARE @str VARCHAR(20)
      DECLARE @ind Int
	  DECLARE @StatusDia int 

	  DECLARE @auHabil int 
	  DECLARE @auSabado int 
	  DECLARE @auDescanso int 
	  set  @AuHabil = 0 
	  set  @AuSabado = 1 
	  set  @AuDescanso = 2 
	  
	  DECLARE @lUSaSabados bit 

	  if charindex('#', @patron ) > 0 
	  begin 
		set @lUSaSabados = 1
		set @patron = SUBSTRING(@patron, 2, LEN(@patron)) 
	  end
	  else
	    set @lUSaSabados= 0; 



	  set @StatusDia = @AuHabil 

      IF(@patron is not null)
      BEGIN
            SET @ind = CharIndex(',',@patron)
            WHILE @ind > 0
            BEGIN
                  SET @str = SUBSTRING(@patron,1,@ind-1)
                  SET @patron = SUBSTRING(@patron,@ind+1,LEN(@patron)-@ind)
                  INSERT INTO @ritmos (Patron, TipoDia) values (@str, @StatusDia)
                  SET @ind = CharIndex(',',@patron)


				  SET @StatusDia = case 
									when @StatusDia = @AuHabil and @lUsaSabados = 1  then @AuSabado
									when @StatusDia = @AuHabil and @lUsaSabados = 0  then @AuDescanso
									when @StatusDia = @AuSabado then @AuDescanso 
									when @StatusDia = @AuDescanso then @AuHabil
								else 
									@AuHabil 
							end 
            END
            SET @str = @patron
            INSERT INTO @ritmos (Patron, TipoDia) values (@str, @StatusDia)
      END



	  
	update @ritmos  set Patron = case when charindex('%', Patron) > 0 then  SUBSTRING( Patron, 1,  charindex('%', Patron)-1) else Patron end 
	update @ritmos  set Ho_Codigo = SUBSTRING( Patron, charindex(':', Patron)+1,  len( Patron)- charindex(':', Patron)+1  )  where Patron like '%:%' 
	update @ritmos  set Ho_Codigo = case 
									when TipoDia = @auHabil then @Horario1 
									when TipoDia = @auSabado then @Horario2 
									when TipoDia = @auDescanso and @lUSaSabados = 1  then @Horario3
									when TipoDia = @auDescanso and @lUSaSabados = 0  then @Horario2
								   else 
								    @Horario1 
								  end	
					where Ho_Codigo = '' or Ho_Codigo is NULL  
	update @ritmos  set Dias = cast( case  when charindex(':', Patron) > 0  then SUBSTRING( Patron, 1, charindex(':', Patron)-1 ) 
								   else  Patron 
							 end  as int ) 



	declare @iRitmo int 
	declare @nRitmos int 
	declare @nDias  int 
	declare @jRitmo int 
	set @iRitmo = 0 
	select @nRitmos = count(*) from @Ritmos
	while @iRitmo < @nRitmos 
	begin 
		select @nDias = Dias from @Ritmos where i= @iRitmo 
		set @jRitmo = 0 
		while @jRitmo < @nDias 
		begin 
			insert into @Result ( HO_CODIGO , TipoDia ) 
			select HO_CODIGO, TipoDia from @Ritmos where i = @iRitmo 
			 
			set @jRitmo = @jRitmo +1 
		end
		
		set @iRitmo = @iRitmo +1 
	end 


      RETURN
END

go 


IF EXISTS ( SELECT *  FROM SYSOBJECTS WHERE NAME = 'FN_GetFestivos'  AND XTYPE = 'TF' )
DROP FUNCTION dbo.FN_GetFestivos
go
	
create function FN_GetFestivos( @TU_CODIGO Codigo, @Fecha Fecha) 
RETURNS @Festivos
  TABLE  (
	FE_MES integer, 
	FE_DIA integer, 
	FECHA datetime, 
	TU_CODIGO char(6)
		)	
as
begin 

insert into @Festivos ( FE_MES, FE_DIA, FECHA, TU_CODIGO ) 
select FE_MES, FE_DIA, FE_CAMBIO, TU_CODIGO  from FESTIVO where FE_TIPO = 1 and FE_YEAR = YEAR(@Fecha) and  TU_CODIGO =  @TU_CODIGO 

insert into @Festivos ( FE_MES, FE_DIA, FECHA, TU_CODIGO ) 
select Fe.FE_MES, Fe.FE_DIA, Fe.FE_CAMBIO, Fe.TU_CODIGO  from FESTIVO FE
left outer join @Festivos ft  on  ft.FE_MES = FE.FE_MES and ft.FE_DIA = FE.FE_DIA
where FE_TIPO = 0 and Fe.TU_CODIGO = @TU_CODIGO and FE_YEAR = YEAR( @Fecha ) and ft.FE_DIA is null 

insert into @Festivos ( FE_MES, FE_DIA, FECHA,TU_CODIGO )
select Fe.FE_MES, Fe.FE_DIA, Fe.FE_CAMBIO, Fe.TU_CODIGO    from FESTIVO FE
left outer join @Festivos ft  on  ft.FE_MES = FE.FE_MES and ft.FE_DIA = FE.FE_DIA
	where FE_TIPO = 0 and Fe.TU_CODIGO = @TU_CODIGO  and ft.FE_DIA is null 

insert into @Festivos ( FE_MES, FE_DIA, FECHA ) 
select Fe.FE_MES, Fe.FE_DIA, Fe.FE_CAMBIO   from FESTIVO FE 
left outer join @Festivos ft  on  ft.FE_MES = FE.FE_MES and ft.FE_DIA = FE.FE_DIA
where FE_TIPO = 1 and Fe.FE_CAMBIO =  @Fecha  and  Fe.TU_CODIGO = '!!!'  and ft.FE_DIA is null 

insert into @Festivos ( FE_MES, FE_DIA, FECHA ) 
select Fe.FE_MES, Fe.FE_DIA, Fe.FE_CAMBIO   from FESTIVO FE
left outer join @Festivos ft  on  ft.FE_MES = FE.FE_MES and ft.FE_DIA = FE.FE_DIA
 where FE_TIPO = 0 and FE_YEAR = YEAR( @Fecha )  and  Fe.TU_CODIGO = '!!!' and ft.FE_DIA is null 

insert into @Festivos ( FE_MES, FE_DIA, FECHA ) 
select Fe.FE_MES, Fe.FE_DIA, Fe.FE_CAMBIO   from FESTIVO FE 
left outer join @Festivos ft  on  ft.FE_MES = FE.FE_MES and ft.FE_DIA = FE.FE_DIA
where FE_TIPO = 0  and  Fe.TU_CODIGO = '!!!' and ft.FE_DIA is null 


update @Festivos set TU_CODIGO = '!!!' where TU_CODIGO is null 

update @Festivos set FECHA =  DATEADD(year, YEAR(@Fecha)-1900, DATEADD(month, FE_MES-1, DATEADD(day, FE_DIA-1, 0))) where FECHA = '1899-12-30' 

return 

end 



go 



IF EXISTS ( SELECT *  FROM SYSOBJECTS WHERE NAME = 'FN_EsFalta'  AND XTYPE = 'FN' )
DROP FUNCTION FN_EsFalta
go

create function  FN_EsFalta( @CB_CODIGO NumeroEmpleado, @AU_FECHA Fecha, @TU_CODIGO Codigo, @HO_CODIGO Codigo, @AU_STATUS status, @AU_TIPODIA status ) 
returns Status 
as 
begin 

	if ( select count(*) from CHECADAS where CB_CODIGO = @CB_CODIGO and AU_FECHA = @AU_FECHA and CH_TIPO in ( 1,2)   ) > 0 
	begin 
		return 0 
	end 

	return 1

end 


GO 

IF EXISTS ( SELECT *  FROM SYSOBJECTS WHERE NAME = 'SP_Dashlet_Indicador_EmpleadosActivos'  AND XTYPE = 'P' )
DROP procedure SP_Dashlet_Indicador_EmpleadosActivos
GO 

create procedure SP_Dashlet_Indicador_EmpleadosActivos( @Fecha Fecha,  @IndicadorValor int output, @FechaAnterior fecha, @IndicadorTendencia Pesos output, @CM_NIVEL0 Formula = ''  ) 
as
begin 
	set nocount on 

	declare  @IndicadorValorAnterior int 

	select 
		@IndicadorValor = SUM( 	
		case 
			when dbo.SP_STATUS_ACT( @Fecha, CB_CODIGO )= 1 then 1
			else 0 
		end 
		), 
		@IndicadorValorAnterior = SUM( 	
		case 
			when dbo.SP_STATUS_ACT( @FechaAnterior, CB_CODIGO )= 1 then 1
			else 0 
		end 
		) 
	from COLABORA 
	where @CM_NIVEL0 = ''  or ( CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB_NIVEL0='')

	if (@IndicadorValor > 0  )
		set @IndicadorTendencia =  ( ( @IndicadorValor*1.0 - @IndicadorValorAnterior*1.0 ) / @IndicadorValor*1.0 ) * 100.00
	else 
		set @IndicadorTendencia = 0; 

end 
go 


IF EXISTS ( SELECT *  FROM SYSOBJECTS WHERE NAME = 'SP_Dashlet_Indicador_ContratosRenovar'  AND XTYPE = 'P' )
DROP procedure SP_Dashlet_Indicador_ContratosRenovar
GO 

create procedure SP_Dashlet_Indicador_ContratosRenovar( @Fecha Fecha,  @IndicadorValor int output, @FechaAnterior fecha, @IndicadorTendencia Pesos output, @CM_NIVEL0 Formula = '' ) 
as
begin 
	set nocount on 

	declare  @IndicadorValorAnterior int 

	declare @FinDeMes Fecha 
	Select @FinDeMes =  DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@Fecha)+1,0))
	declare @FinDeMesAnterior  Fecha 
	SELECT @FinDeMesAnterior = DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@Fecha),0))

	select 
		@IndicadorValor = SUM( 	
		   case 
				when ( CB_FEC_COV between '2000-01-01' and @FinDeMes  )and dbo.SP_STATUS_ACT( @FinDeMes, CB_CODIGO )= 1 then 1 
				else 0 
		   end 
		), 
		@IndicadorValorAnterior = SUM( 	
		case 
			when ( dbo.SP_KARDEX_CB_FEC_COV( @FinDeMesAnterior, CB_CODIGO ) between '2000-01-01' and @FinDeMesAnterior ) and dbo.SP_STATUS_ACT( @FinDeMesAnterior, CB_CODIGO )= 1 then 1  
			else 0 
		end 
		) 
	from COLABORA 
	where @CM_NIVEL0 = ''  or ( CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB_NIVEL0='')

	if (@IndicadorValor > 0  )
		set @IndicadorTendencia =  ( ( @IndicadorValor*1.0 - @IndicadorValorAnterior*1.0 ) / @IndicadorValor*1.0 ) * 100.00
	else 
		set @IndicadorTendencia = 0; 


end 
GO 

if exists (select * from sysobjects where id = object_id(N'FN_GetNominasSinTimbrar') and xtype in (N'FN', N'IF', N'TF')) 
	drop function FN_GetNominasSinTimbrar
go


CREATE function FN_GetNominasSinTimbrar (@Pe_year int, @Pe_mes int, @CM_NIVEL0 Formula )
returns integer
as
begin
declare @Total int 

  set @Total = 0 
  select 
	@Total = count(*)
  from NOMINA NO 
  join PERIODO PE on PE.PE_YEAR = NO.PE_YEAR and PE.PE_TIPO = NO.PE_TIPO and PE.PE_NUMERO = NO.PE_NUMERO   
  where NO.NO_STATUS > 5 and NO.NO_TIMBRO < 2  
  and PE.PE_YEAR = @Pe_year and PE.PE_MES = @Pe_mes and PE.PE_STATUS > 5 
  and (  @CM_NIVEL0 = ''  or ( CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB_NIVEL0='') )
  return @Total 

end 


go 



IF EXISTS ( SELECT *  FROM SYSOBJECTS WHERE NAME = 'SP_Dashlet_Indicador_SinTimbrar'  AND XTYPE = 'P' )
DROP procedure SP_Dashlet_Indicador_SinTimbrar
GO 

create procedure SP_Dashlet_Indicador_SinTimbrar(@Fecha Fecha, @PE_TIPO int, @PE_NUMERO int, @PE_YEAR int,  @IndicadorValor int output, @IndicadorTendencia Pesos output, @CM_NIVEL0 Formula = '' ) 
as
begin 
	set nocount on 

	declare  @IndicadorValorAnterior int 
	declare  @PeMes int 
	declare  @PeMesAnterior int 
	declare  @PeYearAnterior int 

	select @PeMEs = PE_MES from PERIODO where PE_TIPO = @PE_TIPO and PE_NUMERO = @PE_NUMERO and PE_YEAR = @PE_YEAR 
	set @IndicadorValor = dbo.FN_GetNominasSinTimbrar(@PE_YEAR, @PeMEs, @CM_NIVEL0 ) 

	if ( @PeMes = 1 ) 
	begin 
		set @PeMesAnterior = 12 
		set @PeYearAnterior = @Pe_YEAR -1 
	end 
	else
	begin 
		set @PeMesAnterior =  @PeMes - 1  
		set @PeYearAnterior = @Pe_YEAR 
	end

	set @IndicadorValorAnterior = dbo.FN_GetNominasSinTimbrar(@PeYearAnterior, @PeMesAnterior, @CM_NIVEL0 ) 
	
	
	if (@IndicadorValor > 0  )
		set @IndicadorTendencia =  ( ( @IndicadorValor*1.0 - @IndicadorValorAnterior*1.0 ) / @IndicadorValor*1.0 ) * 100.00
	else 
		set @IndicadorTendencia = 0; 

end 


GO 



/* ACUMULADOS DE NOMINA */

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id=object_id('ACUMULA_RS'))
CREATE TABLE ACUMULA_RS (
	AC_ID int IDENTITY(1,1),
	AC_YEAR Anio ,
	CB_CODIGO NumeroEmpleado ,			
	CO_NUMERO Concepto ,
	RS_CODIGO Codigo , 	
	AC_MES_01 Pesos ,	
	AC_MES_02 Pesos ,	
	AC_MES_03 Pesos ,
	AC_MES_04 Pesos ,
	AC_MES_05 Pesos ,
	AC_MES_06 Pesos ,
	AC_MES_07 Pesos ,
	AC_MES_08 Pesos ,
	AC_MES_09 Pesos ,
	AC_MES_10 Pesos ,
	AC_MES_11 Pesos ,
	AC_MES_12 Pesos ,
	AC_MES_13 Pesos ,	
	AC_ANUAL Pesos 
)
GO

IF NOT EXISTS( SELECT NAME FROM sys.sysobjects WHERE NAME = 'PK_ACUMULA_RS' and XTYPE = 'PK' )
ALTER TABLE ACUMULA_RS ADD  CONSTRAINT PK_ACUMULA_RS PRIMARY KEY CLUSTERED 
(
	AC_YEAR ASC,
	CB_CODIGO ASC,
	CO_NUMERO ASC,
	RS_CODIGO ASC
)
GO

IF NOT EXISTS( SELECT NAME from sys.sysobjects WHERE NAME = 'FK_Acumula_RS_Colabora' and XTYPE = 'F' )
ALTER TABLE ACUMULA_RS  WITH NOCHECK ADD  CONSTRAINT FK_Acumula_RS_Colabora FOREIGN KEY(CB_CODIGO)
	REFERENCES COLABORA (CB_CODIGO)
	ON UPDATE CASCADE
	ON DELETE CASCADE
GO

IF EXISTS( select NAME from sys.sysobjects WHERE NAME = 'FK_Acumula_RS_Colabora' and XTYPE = 'F' )
ALTER TABLE ACUMULA_RS CHECK CONSTRAINT FK_Acumula_RS_Colabora
GO

IF NOT EXISTS( SELECT NAME FROM sys.sysobjects WHERE NAME = 'FK_Acumula_RS_Concepto' and XTYPE = 'F' )
ALTER TABLE ACUMULA_RS  WITH NOCHECK ADD  CONSTRAINT FK_Acumula_RS_Concepto FOREIGN KEY(CO_NUMERO)
	REFERENCES CONCEPTO (CO_NUMERO)
	ON UPDATE CASCADE
GO

IF EXISTS( SELECT NAME from sys.sysobjects WHERE NAME = 'FK_Acumula_RS_Concepto' and XTYPE = 'F' )
ALTER TABLE ACUMULA_RS CHECK CONSTRAINT FK_Acumula_RS_Concepto
GO

IF NOT EXISTS ( SELECT * FROM sys.indexes WHERE name='IDX_ACUMULA_RS' AND object_id = OBJECT_ID('ACUMULA_RS') ) 
Create Index IDX_ACUMULA_RS on ACUMULA_RS 
(
	AC_YEAR ASC,
	CB_CODIGO ASC,
	CO_NUMERO ASC
)
GO

IF NOT EXISTS ( SELECT * FROM sys.indexes WHERE name='IDX_ACUMULA_RS_AC_ID' AND object_id = OBJECT_ID('ACUMULA_RS') ) 
CREATE UNIQUE NONCLUSTERED INDEX IDX_ACUMULA_RS_AC_ID ON ACUMULA_RS
(
	AC_ID ASC
)

GO 

IF NOT EXISTS ( SELECT * FROM sys.indexes WHERE name='IDX_NOMINA_NO_STATUS' AND object_id = OBJECT_ID('NOMINA') ) 
CREATE NONCLUSTERED INDEX IDX_NOMINA_NO_STATUS ON NOMINA
(
	PE_YEAR ASC,
	PE_TIPO ASC,
	PE_NUMERO ASC,
	NO_STATUS ASC
)
INCLUDE ( CB_CODIGO, NO_TIMBRO) 

GO 

IF EXISTS(SELECT * FROM sys.objects WHERE object_id = object_id('VACUMRSTOT') )
	DROP VIEW VACUMRSTOT
GO

CREATE VIEW VACUMRSTOT WITH SCHEMABINDING 
AS
	SELECT  AC_YEAR  ,
			CB_CODIGO ,			
			CO_NUMERO  ,
			AC_MES_01  = coalesce( SUM(AR.AC_MES_01), 0 ) ,
			AC_MES_02  = coalesce( SUM(AR.AC_MES_02), 0 ) ,
			AC_MES_03  = coalesce( SUM(AR.AC_MES_03), 0 ) , 
			AC_MES_04  = coalesce( SUM(AR.AC_MES_04), 0 ) ,
			AC_MES_05  = coalesce( SUM(AR.AC_MES_05), 0 ) ,
			AC_MES_06  = coalesce( SUM(AR.AC_MES_06), 0 ) ,
			AC_MES_07  = coalesce( SUM(AR.AC_MES_07), 0 ) ,
			AC_MES_08  = coalesce( SUM(AR.AC_MES_08), 0 ) ,
			AC_MES_09  = coalesce( SUM(AR.AC_MES_09), 0 ) ,
			AC_MES_10  = coalesce( SUM(AR.AC_MES_10), 0 ) ,
			AC_MES_11  = coalesce( SUM(AR.AC_MES_11), 0 ) ,
			AC_MES_12  = coalesce( SUM(AR.AC_MES_12), 0 ) ,
			AC_MES_13  = coalesce( SUM(AR.AC_MES_13), 0 ) ,
			AC_ANUAL   = coalesce( SUM(AR.AC_ANUAL), 0 )
	FROM	dbo.ACUMULA_RS AR
	GROUP BY AC_YEAR, CB_CODIGO, CO_NUMERO
GO

IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'AFECTA_UN_CONCEPTO_RAZON_SOCIAL') AND type IN ( N'P', N'PC' ))
	DROP PROCEDURE AFECTA_UN_CONCEPTO_RAZON_SOCIAL
GO

CREATE  PROCEDURE AFECTA_UN_CONCEPTO_RAZON_SOCIAL ( 
    				@ANIO SMALLINT,
    				@EMPLEADO INTEGER,
    				@CONCEPTO INTEGER,
    				@MES SMALLINT,
    				@FACTOR INTEGER,
    				@MONTO NUMERIC(15,2), 				
					@RS_CODIGO Codigo				
					)
AS
BEGIN 
	Select @Monto = @Factor * @Monto
    if ( @Monto <> 0 ) 
    begin        
		select @FACTOR = COUNT(*) from ACUMULA_RS A where			 	
				( RS_CODIGO = @RS_CODIGO ) and 
                ( AC_YEAR = @Anio ) and
                ( CB_CODIGO = @Empleado ) and
                ( CO_NUMERO = @Concepto ) 
        if ( @Factor = 0 ) 
		begin 
            insert into ACUMULA_RS (RS_CODIGO, AC_YEAR, CB_CODIGO, CO_NUMERO )  values ( @RS_CODIGO, @Anio, @Empleado, @Concepto );				
		end 


        if ( @Mes = 1 ) 
            update ACUMULA_RS  
		set AC_MES_01 = AC_MES_01 + @Monto 
		where		
					( RS_CODIGO = @RS_CODIGO ) and 
					( AC_YEAR = @Anio ) and
					( CB_CODIGO = @Empleado ) and
					( CO_NUMERO = @Concepto );
			else
			if ( @Mes = 2 ) 
				update ACUMULA_RS set AC_MES_02 = AC_MES_02 + @Monto 
		where		
					( RS_CODIGO = @RS_CODIGO ) and 	
					( AC_YEAR = @Anio ) and
					( CB_CODIGO = @Empleado ) and
					( CO_NUMERO = @Concepto );
			else
			if ( @Mes = 3 ) 
				update ACUMULA_RS set AC_MES_03 = AC_MES_03 + @Monto 
		where		
					( RS_CODIGO = @RS_CODIGO ) and 
					( AC_YEAR = @Anio ) and
					( CB_CODIGO = @Empleado ) and
					( CO_NUMERO = @Concepto );
			else
			if ( @Mes = 4 ) 
				update ACUMULA_RS set AC_MES_04 = AC_MES_04 + @Monto 
		where		
					( RS_CODIGO = @RS_CODIGO ) and 
					( AC_YEAR = @Anio ) and
					( CB_CODIGO = @Empleado ) and
					( CO_NUMERO = @Concepto );
			else
			if ( @Mes = 5 ) 
				update ACUMULA_RS set AC_MES_05 = AC_MES_05 + @Monto 
		where		
					( RS_CODIGO = @RS_CODIGO ) and 
					( AC_YEAR = @Anio ) and
					( CB_CODIGO = @Empleado ) and
					( CO_NUMERO = @Concepto );
			else
			if ( @Mes = 6 ) 
				update ACUMULA_RS set AC_MES_06 = AC_MES_06 + @Monto 
		where		
					( RS_CODIGO = @RS_CODIGO ) and 
					( AC_YEAR = @Anio ) and
					( CB_CODIGO = @Empleado ) and
					( CO_NUMERO = @Concepto );
			else
			if ( @Mes = 7 ) 
				update ACUMULA_RS set AC_MES_07 = AC_MES_07 + @Monto 
		where		
					( RS_CODIGO = @RS_CODIGO ) and 
					( AC_YEAR = @Anio ) and
					( CB_CODIGO = @Empleado ) and
					( CO_NUMERO = @Concepto );
			else
			if ( @Mes = 8 ) 
				update ACUMULA_RS set AC_MES_08 = AC_MES_08 + @Monto 
		where		
					( RS_CODIGO = @RS_CODIGO ) and 
					( AC_YEAR = @Anio ) and
					( CB_CODIGO = @Empleado ) and
					( CO_NUMERO = @Concepto );
			else
			if ( @Mes = 9 ) 
				update ACUMULA_RS set AC_MES_09 = AC_MES_09 + @Monto 
		where		
					( RS_CODIGO = @RS_CODIGO ) and 
					( AC_YEAR = @Anio ) and
					( CB_CODIGO = @Empleado ) and
					( CO_NUMERO = @Concepto );
			else
			if ( @Mes = 10 ) 
				update ACUMULA_RS set AC_MES_10 = AC_MES_10 + @Monto 
		where		
					( RS_CODIGO = @RS_CODIGO ) and 
					( AC_YEAR = @Anio ) and
					( CB_CODIGO = @Empleado ) and
					( CO_NUMERO = @Concepto );
			else
			if ( @Mes = 11 ) 
				update ACUMULA_RS set AC_MES_11 = AC_MES_11 + @Monto 
		where		
					( RS_CODIGO = @RS_CODIGO ) and 
					( AC_YEAR = @Anio ) and
					( CB_CODIGO = @Empleado ) and
					( CO_NUMERO = @Concepto );
			else
			if ( @Mes = 12 ) 
				update ACUMULA_RS set AC_MES_12 = AC_MES_12 + @Monto 
		where		
					( RS_CODIGO = @RS_CODIGO ) and 
					( AC_YEAR = @Anio ) and
					( CB_CODIGO = @Empleado ) and
					( CO_NUMERO = @Concepto );
			else
			if ( @Mes = 13 ) 
				update ACUMULA_RS set AC_MES_13 = AC_MES_13 + @Monto 
		where		
					( RS_CODIGO = @RS_CODIGO ) and 
					( AC_YEAR = @Anio ) and
					( CB_CODIGO = @Empleado ) and
					( CO_NUMERO = @Concepto );

		/* No filtra por Razon Social para limpiar AC_ANUAL de las razones sociales no asociadas a la nomina del empleados en caso de datos sucios */ 
		update ACUMULA_RS set AC_ANUAL = (AC_MES_01 +
										AC_MES_02 +
										AC_MES_03 +
										AC_MES_04 +
										AC_MES_05 +
										AC_MES_06 +
										AC_MES_07 +
										AC_MES_08 +
										AC_MES_09 +
										AC_MES_10 +
										AC_MES_11 +
										AC_MES_12 +
										AC_MES_13)
		where ( AC_YEAR = @Anio ) and
			( CB_CODIGO = @Empleado ) and
			( CO_NUMERO = @Concepto );
    end
END 
GO

IF EXISTS ( SELECT * FROM sys.objects WHERE   object_id = OBJECT_ID(N'CALCULA_ACUMULADOS_TOTALES') AND type IN ( N'P', N'PC' ))
	DROP PROCEDURE CALCULA_ACUMULADOS_TOTALES
GO

CREATE  PROCEDURE CALCULA_ACUMULADOS_TOTALES ( 
    				@ANIO SMALLINT,
    				@EMPLEADO INTEGER,
    				@CONCEPTO INTEGER,
    				@MES SMALLINT    			
					)
AS
BEGIN 
	declare @MontoTotal Pesos 
	declare @iExisteAcumula int 

	select @iExisteAcumula = COUNT(*) from ACUMULA A where
			( AC_YEAR = @Anio ) and
			( CB_CODIGO = @Empleado ) and
			( CO_NUMERO = @Concepto ) 

	if ( @iExisteAcumula = 0 ) 
	begin 
		insert into ACUMULA (AC_YEAR, CB_CODIGO, CO_NUMERO )  values ( @Anio, @Empleado, @Concepto );				
	end 
		  
	if ( @Mes = 1 ) 
	begin 
		select   @MontoTotal = coalesce( SUM(AR.AC_MES_01), 0 ) 
		from ACUMULA_RS AR 
		where ( AR.AC_YEAR = @Anio ) and ( AR.CB_CODIGO = @Empleado ) and   ( AR.CO_NUMERO = @Concepto );	
															
		update ACUMULA  set ACUMULA.AC_MES_01 = @MontoTotal 
		from ACUMULA 				
		where( AC_YEAR = @Anio ) and  ( CB_CODIGO = @Empleado ) and  ( CO_NUMERO = @Concepto ); 
	end 
	else
	if ( @Mes = 2 ) 
	begin 
		select   @MontoTotal = coalesce( SUM(AR.AC_MES_02), 0 ) 
		from ACUMULA_RS AR 
		where ( AR.AC_YEAR = @Anio ) and ( AR.CB_CODIGO = @Empleado ) and   ( AR.CO_NUMERO = @Concepto );	
															
		update ACUMULA  set ACUMULA.AC_MES_02 = @MontoTotal 
		from ACUMULA 				
		where( AC_YEAR = @Anio ) and  ( CB_CODIGO = @Empleado ) and  ( CO_NUMERO = @Concepto ); 
	end 
	else
	if ( @Mes = 3 ) 
	begin 
		select   @MontoTotal = coalesce( SUM(AR.AC_MES_03), 0 ) 
		from ACUMULA_RS AR 
		where ( AR.AC_YEAR = @Anio ) and ( AR.CB_CODIGO = @Empleado ) and   ( AR.CO_NUMERO = @Concepto );	
															
		update ACUMULA  set ACUMULA.AC_MES_03 = @MontoTotal 
		from ACUMULA 				
		where( AC_YEAR = @Anio ) and  ( CB_CODIGO = @Empleado ) and  ( CO_NUMERO = @Concepto ); 
	end 
	else
	if ( @Mes = 4 ) 
	begin 
		select   @MontoTotal = coalesce( SUM(AR.AC_MES_04), 0 ) 
		from ACUMULA_RS AR 
		where ( AR.AC_YEAR = @Anio ) and ( AR.CB_CODIGO = @Empleado ) and   ( AR.CO_NUMERO = @Concepto );	
															
		update ACUMULA  set ACUMULA.AC_MES_04 = @MontoTotal 
		from ACUMULA 				
		where( AC_YEAR = @Anio ) and  ( CB_CODIGO = @Empleado ) and  ( CO_NUMERO = @Concepto ); 
	end 
	else
	if ( @Mes = 5 ) 
	begin 
		select   @MontoTotal = coalesce( SUM(AR.AC_MES_05), 0 ) 
		from ACUMULA_RS AR 
		where ( AR.AC_YEAR = @Anio ) and ( AR.CB_CODIGO = @Empleado ) and   ( AR.CO_NUMERO = @Concepto );	
															
		update ACUMULA  set ACUMULA.AC_MES_05 = @MontoTotal 
		from ACUMULA 				
		where( AC_YEAR = @Anio ) and  ( CB_CODIGO = @Empleado ) and  ( CO_NUMERO = @Concepto ); 
	end 
	else
	if ( @Mes = 6 ) 
	begin 
		select   @MontoTotal = coalesce( SUM(AR.AC_MES_06), 0 ) 
		from ACUMULA_RS AR 
		where ( AR.AC_YEAR = @Anio ) and ( AR.CB_CODIGO = @Empleado ) and   ( AR.CO_NUMERO = @Concepto );	
															
		update ACUMULA  set ACUMULA.AC_MES_06 = @MontoTotal 
		from ACUMULA 				
		where( AC_YEAR = @Anio ) and  ( CB_CODIGO = @Empleado ) and  ( CO_NUMERO = @Concepto ); 
	end 
	else
	if ( @Mes = 7 ) 
	begin 
		select   @MontoTotal = coalesce( SUM(AR.AC_MES_07), 0 ) 
		from ACUMULA_RS AR 
		where ( AR.AC_YEAR = @Anio ) and ( AR.CB_CODIGO = @Empleado ) and   ( AR.CO_NUMERO = @Concepto );	
															
		update ACUMULA  set ACUMULA.AC_MES_07 = @MontoTotal 
		from ACUMULA 				
		where( AC_YEAR = @Anio ) and  ( CB_CODIGO = @Empleado ) and  ( CO_NUMERO = @Concepto ); 
	end 
	else
	if ( @Mes = 8 ) 
	begin 
		select   @MontoTotal = coalesce( SUM(AR.AC_MES_08), 0 ) 
		from ACUMULA_RS AR 
		where ( AR.AC_YEAR = @Anio ) and ( AR.CB_CODIGO = @Empleado ) and   ( AR.CO_NUMERO = @Concepto );	
															
		update ACUMULA  set ACUMULA.AC_MES_08 = @MontoTotal 
		from ACUMULA 				
		where( AC_YEAR = @Anio ) and  ( CB_CODIGO = @Empleado ) and  ( CO_NUMERO = @Concepto ); 
	end 
	else
	if ( @Mes = 9 ) 
	begin 
		select   @MontoTotal = coalesce( SUM(AR.AC_MES_09), 0 ) 
		from ACUMULA_RS AR 
		where ( AR.AC_YEAR = @Anio ) and ( AR.CB_CODIGO = @Empleado ) and   ( AR.CO_NUMERO = @Concepto );
																
		update ACUMULA  set ACUMULA.AC_MES_09 = @MontoTotal 
		from ACUMULA 				
		where( AC_YEAR = @Anio ) and  ( CB_CODIGO = @Empleado ) and  ( CO_NUMERO = @Concepto ); 
	end 
	else
	if ( @Mes = 10 ) 
	begin 
		select   @MontoTotal = coalesce( SUM(AR.AC_MES_10), 0 ) 
		from ACUMULA_RS AR 
		where ( AR.AC_YEAR = @Anio ) and ( AR.CB_CODIGO = @Empleado ) and   ( AR.CO_NUMERO = @Concepto );	
															
		update ACUMULA  set ACUMULA.AC_MES_10 = @MontoTotal 
		from ACUMULA 				
		where( AC_YEAR = @Anio ) and  ( CB_CODIGO = @Empleado ) and  ( CO_NUMERO = @Concepto ); 
	end 
	else
	if ( @Mes = 11 ) 
	begin 
		select   @MontoTotal = coalesce( SUM(AR.AC_MES_11), 0 ) 
		from ACUMULA_RS AR 
		where ( AR.AC_YEAR = @Anio ) and ( AR.CB_CODIGO = @Empleado ) and   ( AR.CO_NUMERO = @Concepto );	
															
		update ACUMULA  set ACUMULA.AC_MES_11 = @MontoTotal 
		from ACUMULA 				
		where( AC_YEAR = @Anio ) and  ( CB_CODIGO = @Empleado ) and  ( CO_NUMERO = @Concepto ); 
	end 
	else
	if ( @Mes = 12 ) 
	begin 
		select   @MontoTotal = coalesce( SUM(AR.AC_MES_12), 0 ) 
		from ACUMULA_RS AR 
		where ( AR.AC_YEAR = @Anio ) and ( AR.CB_CODIGO = @Empleado ) and   ( AR.CO_NUMERO = @Concepto );	
															
		update ACUMULA  set ACUMULA.AC_MES_12 = @MontoTotal 
		from ACUMULA 				
		where( AC_YEAR = @Anio ) and  ( CB_CODIGO = @Empleado ) and  ( CO_NUMERO = @Concepto ); 
	end 
	else
	if ( @Mes = 13 ) 
	begin 
		select   @MontoTotal = coalesce( SUM(AR.AC_MES_13), 0 ) 
		from ACUMULA_RS AR 
		where ( AR.AC_YEAR = @Anio ) and ( AR.CB_CODIGO = @Empleado ) and   ( AR.CO_NUMERO = @Concepto );		
														
		update ACUMULA  set ACUMULA.AC_MES_13 = @MontoTotal 
		from ACUMULA 				
		where( AC_YEAR = @Anio ) and  ( CB_CODIGO = @Empleado ) and  ( CO_NUMERO = @Concepto ); 
	end 
	        		                
	update ACUMULA set AC_ANUAL = (AC_MES_01 +
									AC_MES_02 +
									AC_MES_03 +
									AC_MES_04 +
									AC_MES_05 +
									AC_MES_06 +
									AC_MES_07 +
									AC_MES_08 +
									AC_MES_09 +
									AC_MES_10 +
									AC_MES_11 +
									AC_MES_12 +
									AC_MES_13)
	where ( AC_YEAR = @Anio ) and
		( CB_CODIGO = @Empleado ) and
		( CO_NUMERO = @Concepto );    
END
GO

IF EXISTS ( SELECT * FROM sys.objects WHERE   object_id = OBJECT_ID(N'CALCULA_ACUMULADOS_TOTALES_ANUAL') AND type IN ( N'P', N'PC' ))
	DROP PROCEDURE CALCULA_ACUMULADOS_TOTALES_ANUAL
GO

CREATE  PROCEDURE CALCULA_ACUMULADOS_TOTALES_ANUAL ( 
    				@AC_YEAR SMALLINT,
    				@CB_CODIGO INTEGER,
    				@CO_NUMERO INTEGER    						
					)
AS
BEGIN 
	set nocount on 
	exec CALCULA_ACUMULADOS_TOTALES @AC_YEAR, @CB_CODIGO, @CO_NUMERO, 1
	exec CALCULA_ACUMULADOS_TOTALES @AC_YEAR, @CB_CODIGO, @CO_NUMERO, 2
	exec CALCULA_ACUMULADOS_TOTALES @AC_YEAR, @CB_CODIGO, @CO_NUMERO, 3
	exec CALCULA_ACUMULADOS_TOTALES @AC_YEAR, @CB_CODIGO, @CO_NUMERO, 4
	exec CALCULA_ACUMULADOS_TOTALES @AC_YEAR, @CB_CODIGO, @CO_NUMERO, 5
	exec CALCULA_ACUMULADOS_TOTALES @AC_YEAR, @CB_CODIGO, @CO_NUMERO, 6
	exec CALCULA_ACUMULADOS_TOTALES @AC_YEAR, @CB_CODIGO, @CO_NUMERO, 7
	exec CALCULA_ACUMULADOS_TOTALES @AC_YEAR, @CB_CODIGO, @CO_NUMERO, 8
	exec CALCULA_ACUMULADOS_TOTALES @AC_YEAR, @CB_CODIGO, @CO_NUMERO, 9
	exec CALCULA_ACUMULADOS_TOTALES @AC_YEAR, @CB_CODIGO, @CO_NUMERO, 10
	exec CALCULA_ACUMULADOS_TOTALES @AC_YEAR, @CB_CODIGO, @CO_NUMERO, 11
	exec CALCULA_ACUMULADOS_TOTALES @AC_YEAR, @CB_CODIGO, @CO_NUMERO, 12
	exec CALCULA_ACUMULADOS_TOTALES @AC_YEAR, @CB_CODIGO, @CO_NUMERO, 13
END 
GO

IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'AFECTA_UN_CONCEPTO') AND type IN ( N'P', N'PC' ))
	DROP PROCEDURE AFECTA_UN_CONCEPTO
GO

CREATE  PROCEDURE AFECTA_UN_CONCEPTO 
    				@ANIO SMALLINT,
    				@EMPLEADO INTEGER,
    				@CONCEPTO INTEGER,
    				@MES SMALLINT,
    				@FACTOR INTEGER,
    				@MONTO NUMERIC(15,2), 				
					@RS_CODIGO Codigo = ''
AS
BEGIN 
     SET NOCOUNT ON
	 EXEC AFECTA_UN_CONCEPTO_RAZON_SOCIAL @ANIO, @EMPLEADO, @CONCEPTO, @MES, @FACTOR, @MONTO, @RS_CODIGO
	 EXEC CALCULA_ACUMULADOS_TOTALES  @ANIO, @EMPLEADO, @CONCEPTO, @MES
END 
GO

IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'AFECTA_EMPLEADO') AND type IN ( N'P', N'PC' ))
	DROP PROCEDURE AFECTA_EMPLEADO
GO

CREATE  PROCEDURE AFECTA_EMPLEADO
    				@EMPLEADO INTEGER,
    				@ANIO SMALLINT,
    				@TIPO SMALLINT,
    				@NUMERO SMALLINT,
    				@MES SMALLINT,
    				@FACTOR INTEGER
AS
BEGIN
     SET NOCOUNT ON

	 DECLARE @RS_CODIGO Codigo 

	 select @RS_CODIGO = RSOCIAL.RS_CODIGO from NOMINA 
	 join RPATRON on NOMINA.CB_PATRON = RPATRON.TB_CODIGO 
	 join RSOCIAL on RSOCIAL.RS_CODIGO  = RPATRON.RS_CODIGO 
	 where NOMINA.PE_YEAR = @ANIO and NOMINA.PE_TIPO = @TIPO and NOMINA.PE_NUMERO = @NUMERO and NOMINA.CB_CODIGO = @EMPLEADO 

	 set @RS_CODIGO = coalesce( @RS_CODIGO, '' ) 

     DECLARE @CONCEPTO INTEGER;
     DECLARE @M1000 NUMERIC(15,2);
     DECLARE @M1001 NUMERIC(15,2);
     DECLARE @M1002 NUMERIC(15,2);
     DECLARE @M1003 NUMERIC(15,2);
     DECLARE @M1004 NUMERIC(15,2);
     DECLARE @M1005 NUMERIC(15,2);
     DECLARE @M1006 NUMERIC(15,2);
     DECLARE @M1007 NUMERIC(15,2);
     DECLARE @M1008 NUMERIC(15,2);
     DECLARE @M1009 NUMERIC(15,2);
     DECLARE @M1010 NUMERIC(15,2);
     DECLARE @M1011 NUMERIC(15,2);
     DECLARE @M1012 NUMERIC(15,2);
     DECLARE @M1013 NUMERIC(15,2);
     DECLARE @M1014 NUMERIC(15,2);
     DECLARE @M1015 NUMERIC(15,2);
     DECLARE @M1016 NUMERIC(15,2);
     DECLARE @M1017 NUMERIC(15,2);
     DECLARE @M1100 NUMERIC(15,2);
     DECLARE @M1101 NUMERIC(15,2);
     DECLARE @M1102 NUMERIC(15,2);
     DECLARE @M1103 NUMERIC(15,2);
     DECLARE @M1104 NUMERIC(15,2);
     DECLARE @M1105 NUMERIC(15,2);
     DECLARE @M1106 NUMERIC(15,2);
     DECLARE @M1107 NUMERIC(15,2);
     DECLARE @M1108 NUMERIC(15,2);
     DECLARE @M1109 NUMERIC(15,2);
     DECLARE @M1110 NUMERIC(15,2);
     DECLARE @M1111 NUMERIC(15,2);
     DECLARE @M1112 NUMERIC(15,2);
     DECLARE @M1113 NUMERIC(15,2);
     DECLARE @M1114 NUMERIC(15,2);
     DECLARE @M1115 NUMERIC(15,2);
     DECLARE @M1116 NUMERIC(15,2);
     DECLARE @M1117 NUMERIC(15,2);
     DECLARE @M1118 NUMERIC(15,2);
     DECLARE @M1119 NUMERIC(15,2);
     DECLARE @M1120 NUMERIC(15,2);
     DECLARE @M1121 NUMERIC(15,2);
     DECLARE @M1122 NUMERIC(15,2);
     DECLARE @M1123 NUMERIC(15,2);
     DECLARE @M1124 NUMERIC(15,2);
     DECLARE @M1125 NUMERIC(15,2);
     DECLARE @M1126 NUMERIC(15,2);
     DECLARE @M1127 NUMERIC(15,2);
     DECLARE @M1128 NUMERIC(15,2);
     DECLARE @M1129 NUMERIC(15,2);
     DECLARE @M1200 NUMERIC(15,2);
     DECLARE @M1201 NUMERIC(15,2);
     DECLARE @M1202 NUMERIC(15,2);
     DECLARE @M1203 NUMERIC(15,2);
     DECLARE @M1204 NUMERIC(15,2);
     DECLARE @M1205 NUMERIC(15,2);
     DECLARE @M1206 NUMERIC(15,2);
     DECLARE @M1207 NUMERIC(15,2);
     DECLARE @M1208 NUMERIC(15,2);
     DECLARE @M1209 NUMERIC(15,2);
     DECLARE @M1210 NUMERIC(15,2);
     DECLARE @M1211 NUMERIC(15,2);
     DECLARE @M1212 NUMERIC(15,2);
     DECLARE @M1213 NUMERIC(15,2);
     DECLARE @M1214 NUMERIC(15,2);
     DECLARE @M1215 NUMERIC(15,2);
     DECLARE @M1216 NUMERIC(15,2);

     select @M1000=N.NO_PERCEPC,
            @M1001=N.NO_DEDUCCI,
            @M1002=N.NO_NETO,
            @M1003=N.NO_X_ISPT,
            @M1005=N.NO_PER_MEN,
            @M1006=N.NO_X_MENS,
            @M1007=N.NO_PER_CAL,
            @M1008=N.NO_IMP_CAL,
            @M1009=N.NO_X_CAL,
            @M1010=N.NO_TOT_PRE,
            @M1011=N.NO_PREV_GR,
            @M1012=N.NO_PER_ISN,
            @M1013=N.NO_TOTAL_1,
            @M1014=N.NO_TOTAL_2,
            @M1015=N.NO_TOTAL_3,
            @M1016=N.NO_TOTAL_4,
            @M1017=N.NO_TOTAL_5,
            @M1100=N.NO_DIAS_FI,
            @M1101=N.NO_DIAS_FJ,
            @M1102=N.NO_DIAS_CG,
            @M1103=N.NO_DIAS_SG,
            @M1104=N.NO_DIAS_SU,
            @M1105=N.NO_DIAS_OT,
            @M1106=N.NO_DIAS_IN,
            @M1107=N.NO_DIAS * 7 / 6,
            @M1108=N.NO_DIAS_RE,
            @M1109=N.NO_D_TURNO,
            @M1110=N.NO_DIAS,
            @M1111=N.NO_DIAS_AS,
            @M1112=N.NO_DIAS_NT,
            @M1113=N.NO_DIAS_FV,
            @M1114=N.NO_DIAS_VA,
            @M1115=N.NO_DIAS_AG,
            @M1116=N.NO_DIAS_AJ,
            @M1117=N.NO_DIAS_SS,
            @M1118=N.NO_DIAS_EM,
            @M1119=N.NO_DIAS_SI,
            @M1120=N.NO_DIAS_BA,
            @M1121=N.NO_DIAS_PV,
            @M1122=N.NO_DIAS_VJ,
            @M1123=N.NO_DIAS_VJ * 7 / 6,
            @M1124=N.NO_DIAS_IH,
            @M1125=N.NO_DIAS_ID,
            @M1126=N.NO_DIAS_IT,
            @M1127=N.NO_DIAS_DT,
            @M1128=N.NO_DIAS_FS,
            @M1129=N.NO_DIAS_FT,
            @M1200=N.NO_JORNADA,
            @M1201=N.NO_HORAS,
            @M1202=N.NO_EXTRAS,
            @M1203=N.NO_DOBLES,
            @M1204=N.NO_TRIPLES,
            @M1205=N.NO_ADICION,
            @M1206=N.NO_TARDES,
            @M1207=N.NO_HORA_PD,
            @M1208=N.NO_HORA_CG,
            @M1209=N.NO_HORA_SG,
            @M1210=N.NO_FES_TRA,
            @M1211=N.NO_DES_TRA,
            @M1212=N.NO_VAC_TRA,
            @M1213=N.NO_FES_PAG,
            @M1214=N.NO_HORASNT,
            @M1215=N.NO_HORAPDT,
            @M1216=N.NO_PRE_EXT
            from NOMINA N where
            ( N.PE_YEAR = @Anio ) and
            ( N.PE_TIPO = @Tipo ) and
            ( N.PE_NUMERO = @Numero ) and
            ( N.CB_CODIGO = @Empleado )

     if ( @M1200 <> 0 )
		Select @M1200 = @M1201 / @M1200;
     Select @M1109 = @M1200 * @M1109;

     if ( @M1201 < 0 ) and ( @M1200 > 0 )
		set @M1200 = -@M1200;

     if ( @M1000 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1000, @Mes, @Factor, @M1000, @RS_CODIGO;
     if ( @M1001 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1001, @Mes, @Factor, @M1001, @RS_CODIGO;
     if ( @M1002 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1002, @Mes, @Factor, @M1002, @RS_CODIGO;
     if ( @M1003 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1003, @Mes, @Factor, @M1003, @RS_CODIGO;
     if ( @M1005 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1005, @Mes, @Factor, @M1005, @RS_CODIGO;
     if ( @M1006 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1006, @Mes, @Factor, @M1006, @RS_CODIGO;
     if ( @M1007 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1007, @Mes, @Factor, @M1007, @RS_CODIGO;
     if ( @M1008 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1008, @Mes, @Factor, @M1008, @RS_CODIGO;
     if ( @M1009 <> 0 )
       EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1009, @Mes, @Factor, @M1009, @RS_CODIGO;
     if ( @M1010 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1010, @Mes, @Factor, @M1010, @RS_CODIGO;
     if ( @M1011 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1011, @Mes, @Factor, @M1011, @RS_CODIGO;
  	 if ( @M1012 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1012, @Mes, @Factor, @M1012, @RS_CODIGO;
  	 if ( @M1013 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1013, @Mes, @Factor, @M1013, @RS_CODIGO;
  	 if ( @M1014 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1014, @Mes, @Factor, @M1014, @RS_CODIGO;
  	 if ( @M1015 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1015, @Mes, @Factor, @M1015, @RS_CODIGO;
  	 if ( @M1016 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1016, @Mes, @Factor, @M1016, @RS_CODIGO;
  	 if ( @M1017 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1017, @Mes, @Factor, @M1017, @RS_CODIGO;
     if ( @M1100 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1100, @Mes, @Factor, @M1100, @RS_CODIGO;
     if ( @M1101 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1101, @Mes, @Factor, @M1101, @RS_CODIGO;
     if ( @M1102 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1102, @Mes, @Factor, @M1102, @RS_CODIGO;
     if ( @M1103 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1103, @Mes, @Factor, @M1103, @RS_CODIGO;
     if ( @M1104 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1104, @Mes, @Factor, @M1104, @RS_CODIGO;
     if ( @M1105 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1105, @Mes, @Factor, @M1105, @RS_CODIGO;
     if ( @M1106 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1106, @Mes, @Factor, @M1106, @RS_CODIGO;
     if ( @M1107 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1107, @Mes, @Factor, @M1107, @RS_CODIGO;
     if ( @M1108 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1108, @Mes, @Factor, @M1108, @RS_CODIGO;
     if ( @M1109 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1109, @Mes, @Factor, @M1109, @RS_CODIGO;
     if ( @M1110 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1110, @Mes, @Factor, @M1110, @RS_CODIGO;
     if ( @M1111 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1111, @Mes, @Factor, @M1111, @RS_CODIGO;
     if ( @M1112 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1112, @Mes, @Factor, @M1112, @RS_CODIGO;
     if ( @M1113 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1113, @Mes, @Factor, @M1113, @RS_CODIGO;
     if ( @M1114 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1114, @Mes, @Factor, @M1114, @RS_CODIGO;
     if ( @M1115 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1115, @Mes, @Factor, @M1115, @RS_CODIGO;
     if ( @M1116 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1116, @Mes, @Factor, @M1116, @RS_CODIGO;
     if ( @M1117 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1117, @Mes, @Factor, @M1117, @RS_CODIGO;
     if ( @M1118 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1118, @Mes, @Factor, @M1118, @RS_CODIGO;
     if ( @M1119 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1119, @Mes, @Factor, @M1119, @RS_CODIGO;
     if ( @M1120 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1120, @Mes, @Factor, @M1120, @RS_CODIGO;
     if ( @M1121 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1121, @Mes, @Factor, @M1121, @RS_CODIGO;
     if ( @M1122 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1122, @Mes, @Factor, @M1122, @RS_CODIGO;
     if ( @M1123 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1123, @Mes, @Factor, @M1123, @RS_CODIGO;
     if ( @M1124 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1124, @Mes, @Factor, @M1124, @RS_CODIGO;
     if ( @M1125 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1125, @Mes, @Factor, @M1125, @RS_CODIGO;
     if ( @M1126 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1126, @Mes, @Factor, @M1126, @RS_CODIGO;
     if ( @M1127 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1127, @Mes, @Factor, @M1127, @RS_CODIGO;
     if ( @M1128 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1128, @Mes, @Factor, @M1128, @RS_CODIGO;
     if ( @M1129 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1129, @Mes, @Factor, @M1129, @RS_CODIGO;
     if ( @M1200 <> 0 )
     begin
          if ( @Tipo = 0 )
             Select @M1200 = 8 * @M1200;
          else
          if ( ( @Tipo = 1 ) or ( @Tipo = 6 ) or ( @Tipo = 7 ) )
             Select @M1200 = 48 * @M1200;
          else
          if ( ( @Tipo = 2 ) or ( @Tipo = 10 ) or ( @Tipo = 11 ) )
             Select @M1200 = 96 * @M1200;
          else
          if ( ( @Tipo = 3 ) or ( @Tipo = 8 ) or ( @Tipo = 9 ) )
             Select @M1200 = 104 * @M1200;
          else
          if ( @Tipo = 4 )
             Select @M1200 = 208 * @M1200;
          else
          if ( @Tipo = 5 )
             Select @M1200 = 72 * @M1200;
          EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1200, @Mes, @Factor, @M1200, @RS_CODIGO;

     end
     if ( @M1201 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1201, @Mes, @Factor, @M1201, @RS_CODIGO;
     if ( @M1202 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1202, @Mes, @Factor, @M1202, @RS_CODIGO;
     if ( @M1203 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1203, @Mes, @Factor, @M1203, @RS_CODIGO;
     if ( @M1204 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1204, @Mes, @Factor, @M1204, @RS_CODIGO;
     if ( @M1205 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1205, @Mes, @Factor, @M1205, @RS_CODIGO;
     if ( @M1206 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1206, @Mes, @Factor, @M1206, @RS_CODIGO;
     if ( @M1207 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1207, @Mes, @Factor, @M1207, @RS_CODIGO;
     if ( @M1208 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1208, @Mes, @Factor, @M1208, @RS_CODIGO;
     if ( @M1209 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1209, @Mes, @Factor, @M1209, @RS_CODIGO;
     if ( @M1210 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1210, @Mes, @Factor, @M1210, @RS_CODIGO;
     if ( @M1211 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1211, @Mes, @Factor, @M1211, @RS_CODIGO;
     if ( @M1212 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1212, @Mes, @Factor, @M1212, @RS_CODIGO;
     if ( @M1213 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1213, @Mes, @Factor, @M1213, @RS_CODIGO;
     if ( @M1214 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1214, @Mes, @Factor, @M1214, @RS_CODIGO;
     if ( @M1215 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1215, @Mes, @Factor, @M1215, @RS_CODIGO;
	 if ( @M1216 <> 0 )
	    EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1216, @Mes, @Factor, @M1216, @RS_CODIGO;

     Declare TemporalMovimien Cursor for
        select M.CO_NUMERO, SUM( M.MO_PERCEPC + M.MO_DEDUCCI )
        from MOVIMIEN M where
               ( M.MO_ACTIVO = 'S' ) and
               ( M.PE_YEAR = @Anio ) and
               ( M.PE_TIPO = @Tipo ) and
               ( M.PE_NUMERO = @Numero ) and
               ( M.CB_CODIGO = @Empleado )
        group by M.CO_NUMERO
     open TemporalMovimien
     Fetch next	from TemporalMovimien
     into  @Concepto, @M1000
     WHILE @@FETCH_STATUS = 0
     begin
          EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, @Concepto, @Mes, @Factor, @M1000, @RS_CODIGO;
     	  Fetch next from TemporalMovimien
     	  into  @Concepto, @M1000
     end

     CLOSE TemporalMovimien
     DEALLOCATE TemporalMovimien


     select @M1004 = sum( M.MO_PERCEPC )
     from MOVIMIEN M left outer join CONCEPTO C on C.CO_NUMERO=M.CO_NUMERO
     where
               ( M.MO_ACTIVO = 'S' ) and
               ( M.PE_YEAR = @Anio ) and
               ( M.PE_TIPO = @Tipo ) and
               ( M.PE_NUMERO = @Numero ) and
               ( M.CB_CODIGO = @Empleado ) and
               ( C.CO_TIPO = 1 ) and
               ( C.CO_A_PTU = 'S' )

     if ( @M1004 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1004, @Mes, @Factor, @M1004,  @RS_CODIGO;
END
GO

IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'LIMPIA_ACUMULADOS_TOTALES') AND type IN ( N'P', N'PC' ))
	DROP PROCEDURE LIMPIA_ACUMULADOS_TOTALES
GO

CREATE  PROCEDURE LIMPIA_ACUMULADOS_TOTALES( 	
					@ANIO SMALLINT,
    				@EMPLEADO INTEGER,
    				@MES SMALLINT
					) 
AS
BEGIN
	set nocount on 

     if ( @Mes = 1 )
        update ACUMULA set AC_MES_01 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 2 )
        update ACUMULA set AC_MES_02 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 3 )
        update ACUMULA set AC_MES_03 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 4 )
        update ACUMULA set AC_MES_04 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 5 )
        update ACUMULA set AC_MES_05 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 6 )
        update ACUMULA set AC_MES_06 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 7 )
        update ACUMULA set AC_MES_07 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 8 )
        update ACUMULA set AC_MES_08 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 9 )
        update ACUMULA set AC_MES_09 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 10 )
        update ACUMULA set AC_MES_10 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 11 )
        update ACUMULA set AC_MES_11 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 12 )
        update ACUMULA set AC_MES_12 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 13 )
        update ACUMULA set AC_MES_13 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );

END
GO

IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'LIMPIA_ACUMULADOS_RS') AND type IN ( N'P', N'PC' ))
	DROP PROCEDURE LIMPIA_ACUMULADOS_RS
GO

CREATE  PROCEDURE LIMPIA_ACUMULADOS_RS(
					@ANIO SMALLINT,
					@EMPLEADO INTEGER,
					@MES SMALLINT
					) 
AS
BEGIN
	set nocount on 	
	 if ( @Mes = 1 )
        update ACUMULA_RS set AC_MES_01 = 0  where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 2 )
        update ACUMULA_RS set AC_MES_02 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 3 )
        update ACUMULA_RS set AC_MES_03 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 4 )
        update ACUMULA_RS set AC_MES_04 = 0  where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 5 )
        update ACUMULA_RS set AC_MES_05 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 6 )
        update ACUMULA_RS set AC_MES_06 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 7 )
        update ACUMULA_RS set AC_MES_07 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 8 )
        update ACUMULA_RS set AC_MES_08 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 9 )
        update ACUMULA_RS set AC_MES_09 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 10 )
        update ACUMULA_RS set AC_MES_10 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 11 )
        update ACUMULA_RS set AC_MES_11 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 12 )
        update ACUMULA_RS set AC_MES_12 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 13 )
        update ACUMULA_RS set AC_MES_13 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
END
GO

IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_AcumuladosRazonSocial_SumarAnual') AND type IN ( N'P', N'PC' ))
	DROP PROCEDURE SP_AcumuladosRazonSocial_SumarAnual
GO

CREATE  PROCEDURE SP_AcumuladosRazonSocial_SumarAnual( 	
					@AC_YEAR Anio ,
					@CB_CODIGO NumeroEmpleado
					)
AS
BEGIN
	set nocount on 
	
	-- Suma de ACUMULA_RS
	update ACUMULA_RS set AC_ANUAL = (AC_MES_01 +
                                        AC_MES_02 +
                                        AC_MES_03 +
                                        AC_MES_04 +
                                        AC_MES_05 +
                                        AC_MES_06 +
                                        AC_MES_07 +
                                        AC_MES_08 +
                                        AC_MES_09 +
                                        AC_MES_10 +
                                        AC_MES_11 +
                                        AC_MES_12 +
                                        AC_MES_13)
    where ( AC_YEAR = @AC_YEAR  ) and  ( CB_CODIGO = @CB_CODIGO  ) 
	-- Recalculo de Totales en ACUMULA
	update ACUMULA 
	set 
	     ACUMULA.AC_MES_01  = AR.AC_MES_01,
		  ACUMULA.AC_MES_02  = AR.AC_MES_02,
		  ACUMULA.AC_MES_03  = AR.AC_MES_03, 
		  ACUMULA.AC_MES_04  = AR.AC_MES_04,
		  ACUMULA.AC_MES_05  = AR.AC_MES_05,
		  ACUMULA.AC_MES_06  = AR.AC_MES_06,
		  ACUMULA.AC_MES_07  = AR.AC_MES_07,
		  ACUMULA.AC_MES_08  = AR.AC_MES_08,
		  ACUMULA.AC_MES_09  = AR.AC_MES_09,
		  ACUMULA.AC_MES_10  = AR.AC_MES_10,
		  ACUMULA.AC_MES_11  = AR.AC_MES_11,
		  ACUMULA.AC_MES_12  = AR.AC_MES_12,
		  ACUMULA.AC_MES_13  = AR.AC_MES_13,
		  ACUMULA.AC_ANUAL   = AR.AC_ANUAL
	from ACUMULA
	left outer join VACUMRSTOT AR  on ACUMULA.CB_CODIGO = AR.CB_CODIGO and ACUMULA.AC_YEAR = AR.AC_YEAR and ACUMULA.CO_NUMERO = AR.CO_NUMERO 
	where ( AR.AC_YEAR = @AC_YEAR ) and ( AR.CB_CODIGO = @CB_CODIGO )
END
GO 

IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'RECALCULA_ACUMULADOS') AND type IN ( N'P', N'PC' ))
	DROP PROCEDURE RECALCULA_ACUMULADOS
GO

CREATE  PROCEDURE RECALCULA_ACUMULADOS
					@ANIO SMALLINT,
					@EMPLEADO INTEGER,
					@MES SMALLINT,
					@STATUS SMALLINT
AS
BEGIN
    SET NOCOUNT ON
	DECLARE @TIPO SMALLINT;
	DECLARE @NUMERO SMALLINT;
	
	exec LIMPIA_ACUMULADOS_RS		@ANIO, @EMPLEADO, @MES  
	exec LIMPIA_ACUMULADOS_TOTALES	@ANIO, @EMPLEADO, @MES  
	-- Borra 

	Declare TemporalAcumula Cursor for
		select P.PE_TIPO, P.PE_NUMERO from NOMINA N join PERIODO P on
               ( P.PE_YEAR = N.PE_YEAR ) and
               ( P.PE_TIPO = N.PE_TIPO ) and
               ( P.PE_NUMERO = N.PE_NUMERO )
        where
             ( N.NO_STATUS = @Status ) and
             ( N.CB_CODIGO = @Empleado ) and
             ( N.PE_YEAR = @Anio ) and
             ( P.PE_MES = @Mes )

	OPEN TemporalAcumula
	FETCH NEXT FROM TemporalAcumula
	INTO @Tipo, @Numero 
    WHILE @@FETCH_STATUS = 0
	begin
		EXECUTE AFECTA_EMPLEADO @Empleado, @Anio, @Tipo, @Numero, @Mes, 1;
		FETCH NEXT FROM TemporalAcumula
		INTO @Tipo, @Numero 
    end
    Close TemporalAcumula
    Deallocate TemporalAcumula
	exec SP_AcumuladosRazonSocial_SumarAnual @ANIO, @EMPLEADO
END
GO

IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_AcumuladosRazonSocial_Create') AND type IN ( N'P', N'PC' ))
	DROP PROCEDURE SP_AcumuladosRazonSocial_Create
GO

CREATE  PROCEDURE SP_AcumuladosRazonSocial_Create( 
	@AC_ID	FolioGrande OUTPUT, 
	@AC_YEAR Anio ,
	@CB_CODIGO NumeroEmpleado ,			
	@CO_NUMERO Concepto ,
	@RS_CODIGO Codigo , 	
	@AC_MES_01 Pesos ,	
	@AC_MES_02 Pesos ,	
	@AC_MES_03 Pesos ,
	@AC_MES_04 Pesos ,
	@AC_MES_05 Pesos ,
	@AC_MES_06 Pesos ,
	@AC_MES_07 Pesos ,
	@AC_MES_08 Pesos ,
	@AC_MES_09 Pesos ,
	@AC_MES_10 Pesos ,
	@AC_MES_11 Pesos ,
	@AC_MES_12 Pesos ,
	@AC_MES_13 Pesos ,	
	@AC_ANUAL Pesos )
AS
BEGIN
	set nocount on 
		
	set @AC_ANUAL = @AC_MES_01+ @AC_MES_02+ @AC_MES_03+ @AC_MES_04+ @AC_MES_05+ @AC_MES_06+ @AC_MES_07+ 
	@AC_MES_08+ @AC_MES_09+ @AC_MES_10+ @AC_MES_11+ @AC_MES_12+ @AC_MES_13; 

	insert into ACUMULA_RS ( AC_YEAR, CB_CODIGO, CO_NUMERO, RS_CODIGO, 
	AC_MES_01, AC_MES_02, AC_MES_03, AC_MES_04, AC_MES_05, AC_MES_06, AC_MES_07, 
	AC_MES_08, AC_MES_09, AC_MES_10, AC_MES_11, AC_MES_12, AC_MES_13, AC_ANUAL ) 
	values  ( @AC_YEAR, @CB_CODIGO, @CO_NUMERO, @RS_CODIGO, 
	@AC_MES_01, @AC_MES_02, @AC_MES_03, @AC_MES_04, @AC_MES_05, @AC_MES_06, @AC_MES_07, 
	@AC_MES_08, @AC_MES_09, @AC_MES_10, @AC_MES_11, @AC_MES_12, @AC_MES_13,	@AC_ANUAL ) 
	
	SET @AC_ID	= SCOPE_IDENTITY()
	exec CALCULA_ACUMULADOS_TOTALES_ANUAL @AC_YEAR, @CB_CODIGO, @CO_NUMERO
	RETURN 0 
END
GO

IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_AcumuladosRazonSocial_Update') AND type IN ( N'P', N'PC' ))
	DROP PROCEDURE SP_AcumuladosRazonSocial_Update
GO

CREATE  PROCEDURE SP_AcumuladosRazonSocial_Update( 
					@AC_ID	FolioGrande, 
					@AC_YEAR Anio ,
					@CB_CODIGO NumeroEmpleado ,			
					@CO_NUMERO Concepto ,
					@RS_CODIGO Codigo , 	
					@AC_MES_01 Pesos ,	
					@AC_MES_02 Pesos ,	
					@AC_MES_03 Pesos ,
					@AC_MES_04 Pesos ,
					@AC_MES_05 Pesos ,
					@AC_MES_06 Pesos ,
					@AC_MES_07 Pesos ,
					@AC_MES_08 Pesos ,
					@AC_MES_09 Pesos ,
					@AC_MES_10 Pesos ,
					@AC_MES_11 Pesos ,
					@AC_MES_12 Pesos ,
					@AC_MES_13 Pesos ,	
					@AC_ANUAL Pesos
					)
AS
BEGIN
	set nocount on 
	
	set @AC_ANUAL = @AC_MES_01+ @AC_MES_02+ @AC_MES_03+ @AC_MES_04+ @AC_MES_05+ @AC_MES_06+ @AC_MES_07+ 
	@AC_MES_08+ @AC_MES_09+ @AC_MES_10+ @AC_MES_11+ @AC_MES_12+ @AC_MES_13; 

	update ACUMULA_RS 
		set AC_YEAR = @AC_YEAR, 
			CB_CODIGO = @CB_CODIGO, 
			CO_NUMERO = @CO_NUMERO, 
			RS_CODIGO = @RS_CODIGO, 
			AC_MES_01 = @AC_MES_01, 
			AC_MES_02 = @AC_MES_02, 
			AC_MES_03 = @AC_MES_03, 
			AC_MES_04 = @AC_MES_04, 
			AC_MES_05 = @AC_MES_05, 
			AC_MES_06 = @AC_MES_06, 
			AC_MES_07 = @AC_MES_07, 
			AC_MES_08 = @AC_MES_08, 
			AC_MES_09 = @AC_MES_09, 
			AC_MES_10 = @AC_MES_10,
			AC_MES_11 = @AC_MES_11, 
			AC_MES_12 = @AC_MES_12, 
			AC_MES_13 = @AC_MES_13, 
			AC_ANUAL  = @AC_ANUAL 
	where  AC_ID = @AC_ID

	exec CALCULA_ACUMULADOS_TOTALES_ANUAL @AC_YEAR, @CB_CODIGO, @CO_NUMERO 
END
GO

IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_AcumuladosRazonSocial_Delete') AND type IN ( N'P', N'PC' ))
	DROP PROCEDURE SP_AcumuladosRazonSocial_Delete
GO

CREATE  PROCEDURE SP_AcumuladosRazonSocial_Delete(
					@AC_ID	FolioGrande,
					@AC_YEAR Anio,
					@CB_CODIGO NumeroEmpleado,
					@CO_NUMERO Concepto,
					@RS_CODIGO Codigo
					)
AS
BEGIN
	set nocount on 
		
	delete from ACUMULA_RS 
	where  ( AC_ID = @AC_ID )

	exec CALCULA_ACUMULADOS_TOTALES_ANUAL @AC_YEAR, @CB_CODIGO, @CO_NUMERO 

END
GO

IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_INICIAR_DATOS_ACUMULADOS_RS') AND type IN ( N'P', N'PC' ))
	DROP PROCEDURE SP_INICIAR_DATOS_ACUMULADOS_RS
GO

CREATE  PROCEDURE SP_INICIAR_DATOS_ACUMULADOS_RS 
AS
BEGIN
	set nocount on 
	if ( select count(*) from ACUMULA_RS )  = 0 
	begin 
		insert into ACUMULA_RS
			(AC_YEAR  ,
			AC.CB_CODIGO  ,			
			CO_NUMERO  ,
			RS_CODIGO  , 	
			AC_MES_01  ,	
			AC_MES_02  ,	
			AC_MES_03  ,
			AC_MES_04  ,
			AC_MES_05  ,
			AC_MES_06  ,
			AC_MES_07  ,
			AC_MES_08  ,
			AC_MES_09  ,
			AC_MES_10  ,
			AC_MES_11  ,
			AC_MES_12  ,
			AC_MES_13  ,	
			AC_ANUAL)  
		select 	
			AC_YEAR  ,
			AC.CB_CODIGO  ,			
			CO_NUMERO  ,
			RS_CODIGO= RP.RS_CODIGO   , 	
			AC_MES_01  ,	
			AC_MES_02  ,	
			AC_MES_03  ,
			AC_MES_04  ,
			AC_MES_05  ,
			AC_MES_06  ,
			AC_MES_07  ,
			AC_MES_08  ,
			AC_MES_09  ,
			AC_MES_10  ,
			AC_MES_11  ,
			AC_MES_12  ,
			AC_MES_13  ,	
			AC_ANUAL  
		from ACUMULA AC
		join COLABORA CB on AC.CB_CODIGO = CB.CB_CODIGO 
		join RPATRON RP on RP.TB_CODIGO  = CB.CB_PATRON 

	end 
END
GO

EXEC SP_INICIAR_DATOS_ACUMULADOS_RS 
GO

DROP PROCEDURE SP_INICIAR_DATOS_ACUMULADOS_RS
GO

IF EXISTS ( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'SP_A2'  AND XTYPE = 'FN' )
	DROP function SP_A2
GO

CREATE FUNCTION SP_A2(
  					@Concepto Concepto,
  					@Mes SMALLINT,
					@Anio Anio,
					@Empleado NumeroEmpleado,
					@RazonSocial Codigo
			   )
RETURNS Pesos 
AS
BEGIN
	DECLARE @Resultado AS Pesos
	  
	select @Resultado =  
	SUM( 
	case ( @Mes ) 
		when 1 then AC_MES_01
		when 2 then AC_MES_02
		when 3 then AC_MES_03
		when 4 then AC_MES_04
		when 5 then AC_MES_05
		when 6 then AC_MES_06
		when 7 then AC_MES_07
		when 8 then AC_MES_08
		when 9 then AC_MES_09
		when 10 then AC_MES_10
		when 11 then AC_MES_11
		when 12 then AC_MES_12
		when 13 then AC_MES_13
		else 0 
	end
	)
	from ACUMULA_RS where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado ) and ( RS_CODIGO = @RazonSocial  or @RazonSocial = '' ) and ( CO_NUMERO = @Concepto )

	set @Resultado = coalesce( @Resultado, 0 ) 

	RETURN @Resultado
END

GO

IF EXISTS ( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'SP_AS2'  AND XTYPE = 'FN' )
	DROP function SP_AS2
GO

CREATE FUNCTION SP_AS2(
					@Concepto Concepto,
    				@NumeroIni SMALLINT,
    				@Numerofin SMALLINT,
					@Anio Anio,
					@Empleado NumeroEmpleado,			
					@RazonSocial Codigo
				)
RETURNS Pesos
AS
BEGIN
	declare @Temporal Pesos; 
	declare @Resultado Pesos; 

	select @Resultado = 0;
	while ( @NumeroIni <= @NumeroFin )
	begin		
		select @Temporal = dbo.SP_A2 ( @Concepto, @NumeroIni, @Anio, @Empleado, @RazonSocial )	  
		  
		if ( @Temporal is NOT NULL )
			select @Resultado = @Resultado + @Temporal;
		select @NumeroIni = @NumeroIni + 1;
	end

	set @Resultado = coalesce( @Resultado, 0 ) 
	Return @Resultado;
END

GO

