/* #20056 */
IF NOT EXISTS (SELECT GL_CODIGO FROM GLOBAL WHERE GL_CODIGO = 328) 
BEGIN
	INSERT INTO GLOBAL (GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO, US_CODIGO, GL_CAPTURA, GL_NIVEL) 
		VALUES (328, 'Fonacot por c�dula mensual', 'N', 0, 0, GETDATE(), 0)
END
ELSE IF EXISTS (SELECT GL_CODIGO FROM GLOBAL WHERE GL_CODIGO = 328 AND GL_FORMULA NOT IN ('S', 'N') )
BEGIN
	UPDATE GLOBAL SET GL_DESCRIP = 'Fonacot por c�dula mensual', GL_FORMULA = 'N', GL_CAPTURA =  GETDATE() WHERE GL_CODIGO = 328
END
GO

/* US #20101 */
/* Percepciones */
IF NOT EXISTS (SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_SAT_NUM BETWEEN 51 AND 53 AND TB_SAT_CLA = 1)
BEGIN
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO)
	VALUES
		('P051', 'Pagos por gratificaciones, primas, compensaciones, recompensas u otros a extrabajadores derivados de jubilaci�n en parcialidades', 1, 51, 'S'),
		('P052', 'Pagos que se realicen a extrabajadores que obtengan una jubilaci�n en parcialidades derivados de la ejecuci�n de resoluci�n judicial o de un laudo', 1, 52, 'S'),
		('P053', 'Pagos que se realicen a extrabajadores que obtengan una jubilaci�n en una sola exhibici�n derivados de la ejecuci�n de resoluci�n judicial o de un laudo', 1, 53, 'S')
END
/* Deducciones */
IF NOT EXISTS (SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_SAT_NUM BETWEEN 102 AND 106 AND TB_SAT_CLA = 2)
BEGIN
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO)
	VALUES
		('D102', 'Ajuste a pagos por gratificaciones, primas, compensaciones, recompensas u otros a extrabajadores derivados de jubilaci�n en parcialidades, gravados', 2, 102, 'S'),
		('D103', 'Ajuste a pagos que se realicen a extrabajadores que obtengan una jubilaci�n en parcialidades derivados de la ejecuci�n de una resoluci�n judicial o de un laudo gravados', 2, 103, 'S'),
		('D104', 'Ajuste a pagos que se realicen a extrabajadores que obtengan una jubilaci�n en parcialidades derivados de la ejecuci�n de una resoluci�n judicial o de un laudo exentos', 2, 104, 'S'),
		('D105', 'Ajuste a pagos que se realicen a extrabajadores que obtengan una jubilaci�n en una sola exhibici�n derivados de la ejecuci�n de una resoluci�n judicial o de un laudo gravados', 2, 105, 'S'),
		('D106', 'Ajuste a pagos que se realicen a extrabajadores que obtengan una jubilaci�n en una sola exhibici�n derivados de la ejecuci�n de una resoluci�n judicial o de un laudo exentos', 2, 106, 'S')
END

GO