/****************************************/
/****************************************/
/****** Patch para DATOS ****************/
/****************************************/
/****************************************/

/* Patch 450: Mejoras FONACOT */

/*Fix Diccionario campos*/
-- PCAR_ABO.CR_OBSERVA

/* Definici�n correcta para campo PCAR_ABO.CR_OBSERVA */
ALTER TABLE PCAR_ABO ALTER COLUMN CR_OBSERVA DescLarga NOT NULL
GO

IF exists (SELECT * FROM sysobjects where id = object_id('VTRESSFILE') )
	DROP VIEW VTRESSFILE
GO

CREATE VIEW VTRESSFILE(
				TF_PATH, 
				TF_EXT, 
				TF_FILE, 
				TF_DESCR, 
				TF_VERSION, 
				TF_BUILD, 
				TF_FEC_MOD, 
				TF_CALLS, 
				TF_FEC_ACC, 
				TF_HOST, 
				TF_FEC_UPD)
as
		SELECT 	TF_PATH, 
				TF_EXT, 
				TF_FILE, 
				TF_DESCR, 
				TF_VERSION, 
				TF_BUILD, 
				TF_FEC_MOD, 
				TF_CALLS, 
				TF_FEC_ACC, 
				TF_HOST, 
				TF_FEC_UPD
	FROM #COMPARTE.dbo.TRESSFILES
go
	
	
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id=object_id('PUESTO') AND name='PU_SAT_RSG'    )
     alter table PUESTO add PU_SAT_RSG Codigo NULL 
go 

update PUESTO set PU_SAT_RSG = '' where PU_SAT_RSG is null 
go 

alter table PUESTO alter column PU_SAT_RSG Codigo NOT NULL 
go
	
	
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id=object_id('TURNO') AND name='TU_SAT_JOR'    )
    alter table TURNO add TU_SAT_JOR Codigo NULL 
go 

update TURNO set TU_SAT_JOR = '' where TU_SAT_JOR is null 
go 

alter table TURNO alter column TU_SAT_JOR Codigo NOT NULL 
go

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id=object_id('CONTRATO') AND name='TB_SAT_CON'    )
    alter table CONTRATO add TB_SAT_CON Codigo NULL 
go 

update CONTRATO set TB_SAT_CON = '' where TB_SAT_CON is null 
go 

alter table CONTRATO alter column TB_SAT_CON Codigo NOT NULL 
go

/* 12206 Timbrado - Tipo de Jornada SAT */ 
IF NOT EXISTS (SELECT * FROM sysobjects WHERE id=object_id('TSATJORNAD')   )
create table TSATJORNAD
(
 TB_CODIGO    Codigo PRIMARY KEY ,
 TB_ELEMENT   DescLarga,
 TB_COD_SAT   DescLarga, 
 TB_INGLES    Descripcion,
 TB_NUMERO    Pesos,
 TB_TEXTO     Descripcion,
 TB_ACTIVO    Booleano
)
go 

/* 12162 Timbrado - Tipo de Riesgo de Puesto SAT  */ 
IF NOT EXISTS (SELECT * FROM sysobjects WHERE id=object_id('TSATRIESGO')   )
create table TSATRIESGO
(
 TB_CODIGO    Codigo PRIMARY KEY , 
 TB_ELEMENT   DescLarga,
 TB_COD_SAT   DescLarga, 
 TB_INGLES    Descripcion,
 TB_NUMERO    Pesos,
 TB_TEXTO     Descripcion,
 TB_ACTIVO    Booleano
)
go 

/* 12205 Timbrado - Tipo de Contrato SAT  */ 
IF NOT EXISTS (SELECT * FROM sysobjects WHERE id=object_id('TSATCONTRA')   )
create table TSATCONTRA
(
 TB_CODIGO    Codigo PRIMARY KEY , --Codigo
 TB_ELEMENT   DescLarga, --Descripcion
 TB_COD_SAT   DescLarga,  --Codigo SAT
 TB_INGLES    Descripcion,
 TB_NUMERO    Pesos,
 TB_TEXTO     Descripcion,
 TB_ACTIVO    Booleano
)
go 

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id=object_id('VALOR_UMA')   )
CREATE TABLE VALOR_UMA (
       UM_FEC_INI           Fecha  PRIMARY KEY,
       UM_VALOR             PesosDiario,
)
GO

/* #13392 Global para indicar tipo de tope de amortizaci�n INFONAVIT */
if not EXISTS ( select 1 from VALOR_UMA where UM_FEC_INI = '01/01/2016' )
   INSERT INTO VALOR_UMA (UM_FEC_INI,UM_VALOR) VALUES ( '01/01/2016', 73.04 )
GO

/* #10985 */

ALTER TABLE TRANSFER DROP CONSTRAINT FK_Transfer_Ausencia
GO
ALTER TABLE CHECADAS DROP CONSTRAINT FK_Checadas_Ausencia
GO
DROP INDEX XIE1AUSENCIA ON AUSENCIA
GO
ALTER TABLE AUSENCIA DROP CONSTRAINT PK_AUSENCIA
GO

/* Crear llave primaria de AUSENCIA como CLUSTERED (default) y cambiando el orden de los campos */
ALTER TABLE AUSENCIA
       ADD CONSTRAINT PK_AUSENCIA PRIMARY KEY (AU_FECHA, CB_CODIGO)
GO

CREATE INDEX XIE1AUSENCIA ON AUSENCIA
(
       CB_CODIGO,
       AU_FECHA
)
GO

/* Crear llave foranea hacia AUSENCIA con el cambio en la llave primaria */
ALTER TABLE CHECADAS
       ADD CONSTRAINT FK_Checadas_Ausencia
       FOREIGN KEY (AU_FECHA, CB_CODIGO)
       REFERENCES AUSENCIA
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

ALTER TABLE TRANSFER
       ADD CONSTRAINT FK_Transfer_Ausencia
       FOREIGN KEY (AU_FECHA, CB_CODIGO)
       REFERENCES AUSENCIA
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

/* Timbrado de Nomina */
IF not EXISTS (select * from syscolumns where id=object_id('NOMINA') and name='NO_CAN_UID')
alter table NOMINA add NO_CAN_UID  DescLarga  null

go

update NOMINA 
set NOMINA.NO_CAN_UID  = '' 
from NOMINA NO 
join PERIODO PE on NO.PE_TIPO = PE.PE_TIPO and NO.PE_YEAR = PE.PE_YEAR and NO.PE_NUMERO = PE.PE_NUMERO
where NO.NO_CAN_UID is null and PE.PE_FEC_PAG >= '2013-11-01'
go 

/* Patch 450: Agregar procedure SP_TIMBRAR_EMPLEADO (1/2) */
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_TIMBRAR_EMPLEADO') AND type IN ( N'P', N'PC' ))
	drop procedure SP_TIMBRAR_EMPLEADO
go

/* Patch 450: Agregar procedure SP_TIMBRAR_EMPLEADO (2/2) */
CREATE procedure SP_TIMBRAR_EMPLEADO( @PE_YEAR Anio, @PE_TIPO NominaTipo, @PE_NUMERO NominaNumero, @CB_CODIGO NumeroEmpleado, @CB_RFC Descripcion, @Status status, @Timbre Formula, @FolioTimbramex FolioGrande, @Usuario usuario, @FacturaUUID  DescLarga = '' )  
as  
begin  
 set nocount on  
 declare @StatusTimbrado status  
 set @StatusTimbrado = 2  
  
 if ( @Status = @StatusTimbrado )  
  update NOMINA set NO_TIMBRO = @Status, NO_SELLO = @Timbre , NO_FACTURA = @FolioTimbramex  , NO_FACUUID = @FacturaUUID
  where  CB_CODIGO = @CB_CODIGO and PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO = @PE_NUMERO  
      and NO_STATUS = 6  
   else  
  update NOMINA set NO_TIMBRO = @Status, NO_SELLO = '',  NO_FACTURA = 0, NO_FACUUID = '', NO_CAN_UID = NO_FACUUID   
  where  CB_CODIGO = @CB_CODIGO and PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO = @PE_NUMERO  
      and NO_STATUS = 6  
end  
go

/* Horario */
IF not EXISTS (select * from syscolumns where id=object_id('HORARIO') and name='HO_TIP_JT')
	alter table HORARIO add HO_TIP_JT Pesos null
GO
update HORARIO set HO_TIP_JT = 0.0 where HO_TIP_JT is null
GO
alter table HORARIO alter column HO_TIP_JT Pesos NOT NULL 
GO

/* CR:, Timbrado Actualizar Derechos Datos\Otros\Timbrado 1/3 */
/* Patch 450 #Seq: 1*/
create procedure ACTUALIZA_GLOBALES_TIMBRADO
as
begin
    SET NOCOUNT ON;
    
    if ( select count(*) from GLOBAL where ( GL_CODIGO = 275 ) )= 0  and  ( select count(*) from GLOBAL where ( GL_CODIGO = 257 ) ) >0 
       INSERT INTO GLOBAL(GL_CODIGO, GL_FORMULA)
        SELECT 275, GL_FORMULA from GLOBAL where (GL_CODIGO = 257 ) 
end
GO
/* CR:, Timbrado Actualizar Derechos Datos\Otros\Timbrado 2/3 */
/* Patch 450 #Seq: 1*/
execute ACTUALIZA_GLOBALES_TIMBRADO
GO
/* CR:, Timbrado Actualizar Derechos Datos\Otros\Timbrado 3/3 */
/* Patch 450 #Seq: 1*/
DROP PROCEDURE ACTUALIZA_GLOBALES_TIMBRADO
GO

/* Timbrado de Nomina Notificaciones y advertencias */
if exists (select * from sysobjects where id = object_id(N'FN_GetGlobalBooleano') and xtype in (N'FN', N'IF', N'TF')) 
	drop function FN_GetGlobalBooleano
go
create function FN_GetGlobalBooleano ( @GlobalNo int ) 
returns Booleano 
as 
begin
     return coalesce( ( select top 1 SUBSTRING(GL_FORMULA,1,1) from GLOBAL where GL_CODIGO = @GlobalNo ) , 'N' ) 
end 
go 

if exists (select * from sysobjects where id = object_id(N'FN_GetGlobalInteger') and xtype in (N'FN', N'IF', N'TF')) 
	drop function FN_GetGlobalInteger
go
create function FN_GetGlobalInteger ( @GlobalNo int ) 
returns integer  
as 
begin
     declare @Number int 
	 declare @ValueStr Formula 
	 set @ValueStr =  coalesce( ( select top 1 GL_FORMULA  from GLOBAL where GL_CODIGO = @GlobalNo ) , '0' )
	 
	 if ISNUMERIC ( @ValueStr )> 0  
		return cast( @ValueStr as int ) 
	  
	return 0 
end 
go 

if exists (select * from sysobjects where id = object_id(N'FN_NotificacionTimbrado_GetNominasPendientes') and xtype in (N'FN', N'IF', N'TF')) 
	drop function FN_NotificacionTimbrado_GetNominasPendientes
go
create function FN_NotificacionTimbrado_GetNominasPendientes(@CM_CODIGO CodigoEmpresa, @Fecha Fecha) 
returns @Periodos TABLE  (
	[Fecha de pago] datetime, 
	[A�o] varchar(50) default '', 	
	[Tipo] varchar(50) default '', 
	[Periodo] varchar(50) default '', 	
	[Estatus] varchar(50) default '', 	
	[Recibos sin timbrar] int
) 
as 
begin 

 if dbo.FN_GetGlobalBooleano( 314 )  = 'N' 
    return 

 declare @PE_USO int 
 
 if dbo.FN_GetGlobalBooleano( 325 )  = 'S' 
	set @PE_USO = 0 
 else
	set @PE_USO = -1 

 declare @FechaMin Fecha 
 declare @iDias int 

 set @iDias = dbo.FN_GetGlobalInteger( 315) 
 if  @iDias >0 
	set @FechaMin = dateadd( day , -1* @iDias, @Fecha ) 
 else
	set @FechaMin ='2013-01-01' 



 insert into @Periodos 
 (  [Fecha de pago], [A�o], [Tipo],[Periodo],  [Estatus], [Recibos sin timbrar] ) 
 select cast( PE_FEC_PAG as varchar(50) ), 
        LTRIM( str(PE_YEAR) ), 
		TPERIODO.TP_DESCRIP, 
		LTRIM( str(PE_NUMERO) ) , 
    PE_StatusDesc = case PE_TIMBRO   
						when 0 then 'Pendiente' 
						when 1 then 'Timbrada Parcial' 
						when 2 then 'Timbrada' 
					end , 
     ( select count(*) from NOMINA NO where NO.NO_TIMBRO = 0  and NO.PE_YEAR = PERIODO.PE_YEAR and NO.PE_TIPO = PERIODO.PE_TIPO and NO.PE_NUMERO = PERIODO.PE_NUMERO ) as PE_SinTimbrar
 from PERIODO 
 join TPERIODO on PERIODO.PE_TIPO = TPERIODO.TP_TIPO 
 where PE_YEAR >= 2014 and PERIODO.PE_TIMBRO in (0,1) and PERIODO.PE_STATUS > 5  and ( PERIODO.PE_USO = @PE_USO or @PE_USO = -1 )  and PERIODO.PE_FEC_PAG between @FechaMin and @Fecha

return 
end 
go


/* Notificacion Pendientes de Timbrado Globales*/
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'ACTUALIZA_GLOBALES_NOTIFICACION_TIMBRADO') AND type IN ( N'P', N'PC' ))
	drop procedure GLOBALES_NOTIFICACION_TIMBRADO
go
create procedure GLOBALES_NOTIFICACION_TIMBRADO
as
begin
    SET NOCOUNT ON;    
    if ( select count(*) from GLOBAL where ( GL_CODIGO = 314 ) )= 0 
       INSERT INTO GLOBAL(GL_CODIGO, GL_FORMULA, GL_DESCRIP) values ( 314, 'N', 'Notificaci�n Activo')        
	if ( select count(*) from GLOBAL where ( GL_CODIGO = 315 ) )= 0 
       INSERT INTO GLOBAL(GL_CODIGO, GL_FORMULA, GL_DESCRIP) values ( 315, 7, 'Notificaci�n D�as') 
	if ( select count(*) from GLOBAL where ( GL_CODIGO = 325 ) )= 0 
       INSERT INTO GLOBAL(GL_CODIGO, GL_FORMULA, GL_DESCRIP) values ( 325, 'N', 'Notificaci�n solo ordinarias') 

end
GO

execute GLOBALES_NOTIFICACION_TIMBRADO
GO

DROP PROCEDURE GLOBALES_NOTIFICACION_TIMBRADO
GO 

/* #13392 Global para indicar tipo de tope de amortizaci�n INFONAVIT */ 
if not EXISTS ( select 1 from GLOBAL where GL_CODIGO = 326 )
   INSERT INTO GLOBAL (GL_CODIGO,GL_DESCRIP,GL_FORMULA,GL_TIPO,US_CODIGO,GL_CAPTURA,GL_NIVEL) 
   VALUES (326,'Tipo de tope de amortizaci�n','0',0,0,GETDATE(),0)
GO 

/* 13142 : GLOBAL DE # VECES UMA */
if not EXISTS ( select 1 from GLOBAL where GL_CODIGO = 327 )
   INSERT INTO GLOBAL (GL_CODIGO,GL_DESCRIP,GL_FORMULA,GL_TIPO,US_CODIGO,GL_CAPTURA,GL_NIVEL) 
   VALUES (327,'Valor de Tipo de Cr�dito UMA','4',2,0,GETDATE(),0)
GO 

/* #13089 : Reporte de costeo pueda presentar los criterios de costeo */
ALTER VIEW COSTOS
AS
    SELECT    PE_YEAR,
            PE_TIPO,
            PE_NUMERO,
            CB_CODIGO,
            CT.CO_NUMERO,
            COALESCE( CO_DESCRIP, '' ) CO_DESCRIP,
            CT.MO_REFEREN,
            CT.GpoCostoID GC_ID,
            GpoCostoCodigo GC_CODIGO,
            GpoCostoNombre GC_NOMBRE,
            CT.CC_CODIGO,
            COALESCE( CC_NOMBRE, '' ) CC_NOMBRE,
            CostoClasifi CT_CLASIFI,
            CostoMonto CT_MONTO,
            T_ConceptosCosto.CritCostoID CR_ID,
            CritCostoNombre CR_NOMBRE
    FROM    T_Costos CT
                JOIN NOMINA ON CT.NominaID = NOMINA.LLAVE
                JOIN T_GruposCosto ON CT.GpoCostoID = T_GruposCosto.GpoCostoID
                JOIN T_ConceptosCosto ON CT.CO_NUMERO = T_ConceptosCosto.CO_NUMERO and CT.GpoCostoID = T_ConceptosCosto.GpoCostoID
                JOIN T_CriteriosCosto ON T_CriteriosCosto.CritCostoID = T_ConceptosCosto.CritCostoID
                LEFT JOIN CONCEPTO ON CT.CO_NUMERO = CONCEPTO.CO_NUMERO
                LEFT JOIN CCOSTO ON CT.CC_CODIGO = CCOSTO.CC_CODIGO
GO

/* #13400 */
ALTER VIEW CHORAS (
	AU_FECHA,
	CB_CODIGO,
	CC_CODIGO,
	CH_TIPO,
	CH_CLASIFI,
    CH_HORAS )
as
	select 	AU_FECHA, CB_CODIGO, dbo.SP_AUSENCIA_COSTEO(CB_CODIGO, AU_FECHA), 0, '', AU_HORAS
	from	AUSENCIA
	where	( AU_HORAS > 0 )
    union

	select	T.AU_FECHA, T.CB_CODIGO, dbo.SP_AUSENCIA_COSTEO(T.CB_CODIGO, T.AU_FECHA), 0, 'Transferidas',
			case when A.AU_HORAS >= TR_HORAS then coalesce(-TR_HORAS,0) else -AU_HORAS end
	from	TRANSFER T
	join	AUSENCIA A on (A.AU_FECHA = T.AU_FECHA) and (A.CB_CODIGO = T.CB_CODIGO)
	where	( T.TR_TIPO = 0 )
	union

	select	T.AU_FECHA, T.CB_CODIGO, T.CC_CODIGO, 0, 'Recibidas',
			case when A.AU_HORAS >= TR_HORAS then coalesce(TR_HORAS,0) else AU_HORAS end
	from	TRANSFER T
	join	AUSENCIA A on (A.AU_FECHA = T.AU_FECHA) and (A.CB_CODIGO = T.CB_CODIGO)
	where	( T.TR_TIPO = 0 )
	union

	select 	AU_FECHA, CB_CODIGO, dbo.SP_AUSENCIA_COSTEO(CB_CODIGO, AU_FECHA), 1, '', AU_EXTRAS + AU_DES_TRA
	from	AUSENCIA
	where	( AU_EXTRAS > 0 ) or ( AU_DES_TRA > 0 )
    union

	select	T.AU_FECHA, T.CB_CODIGO, dbo.SP_AUSENCIA_COSTEO(T.CB_CODIGO, T.AU_FECHA), 1, 'Transferidas',
			case when A.AU_EXTRAS + A.AU_DES_TRA >= TR_HORAS then coalesce(-TR_HORAS,0) else -AU_EXTRAS + -AU_DES_TRA end
	from	TRANSFER T
	join	AUSENCIA A on (A.AU_FECHA = T.AU_FECHA) and (A.CB_CODIGO = T.CB_CODIGO)
	where	( T.TR_TIPO = 1 )
	union

	select	T.AU_FECHA, T.CB_CODIGO, T.CC_CODIGO, 1, 'Recibidas',
			case when A.AU_EXTRAS + A.AU_DES_TRA >= TR_HORAS then coalesce(TR_HORAS,0) else AU_EXTRAS + AU_DES_TRA end
	from	TRANSFER T
	join	AUSENCIA A on (A.AU_FECHA = T.AU_FECHA) and (A.CB_CODIGO = T.CB_CODIGO)
	where	( T.TR_TIPO = 1 )
GO
