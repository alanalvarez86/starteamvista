/****************************************/
/****************************************/
/****** Patch para DATOS ****************/
/****************************************/
/****************************************/

/* Patch 471: #16467  */

/* Nuevo campo en tabla PCED_FON: 1/3 */
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id=object_id('PCED_FON') AND name='PF_REUBICA'    )
     alter table PCED_FON add PF_REUBICA Codigo1 NULL 
go 

/* Nuevo campo en tabla PCED_FON: 2/3 */
update PCED_FON set PF_REUBICA = '' where PF_REUBICA is null 
go 

/* Nuevo campo en tabla PCED_FON: 3/3 */
alter table PCED_FON alter column PF_REUBICA Codigo1 NOT NULL 
go

/* Vista VCED_FON, agregar campo PF_REUBICA: 1/2 */
IF exists( select * from sys.objects where object_id = object_id('VCED_FON') )
    drop view VCED_FON
GO

/* Vista VCED_FON, agregar campo PF_REUBICA: 1/2 */
create view VCED_FON(
    CB_CODIGO,
    PR_TIPO,
    PR_REFEREN,
    PF_YEAR,
    PF_MES,
    PF_PAGO,
    PF_PLAZO,
    PF_PAGADAS,
    PF_CAPTURA,
	PF_REUBICA,
    US_CODIGO,
    PF_NOMINA, 
    PF_NOM_QTY )
as
select
    PCED_FON.CB_CODIGO,
    PR_TIPO,
    PR_REFEREN,
    PF_YEAR,
    PF_MES,
    PF_PAGO,
    PF_PLAZO,
    PF_PAGADAS,
    PF_CAPTURA,
	PF_REUBICA,
    US_CODIGO,
    coalesce( Afectadas.PF_Monto, 0.00 ) , 
    coalesce( Afectadas.PF_Cuantos, 0 ) 
from PCED_FON
left outer join 
(
    select M.PE_YEAR, P.PE_MES,  NO.CB_CODIGO, M.MO_REFEREN, coalesce( SUM( M.MO_PERCEPC + M.MO_DEDUCCI ), 0.00 )  PF_Monto, count(*) PF_Cuantos from MOVIMIEN M 
      join NOMINA NO on 
              ( NO.PE_YEAR = M.PE_YEAR ) and
               ( NO.PE_TIPO = M.PE_TIPO ) and
               ( NO.PE_NUMERO = M.PE_NUMERO ) and 
               ( NO.CB_CODIGO  = M.CB_CODIGO ) 
      join PERIODO P on
               ( P.PE_YEAR = NO.PE_YEAR ) and
               ( P.PE_TIPO = NO.PE_TIPO ) and
               ( P.PE_NUMERO = NO.PE_NUMERO )
      where ( M.MO_ACTIVO = 'S' ) and ( NO.NO_STATUS >= 6 ) and M.CO_NUMERO = dbo.FN_GetConceptoFonacot()
    group by  M.PE_YEAR, P.PE_MES,  NO.CB_CODIGO, M.MO_REFEREN
) Afectadas on Afectadas.PE_YEAR = PF_YEAR and Afectadas.PE_MES = PCED_FON.PF_MES and  Afectadas.CB_CODIGO = PCED_FON.CB_CODIGO and Afectadas.MO_REFEREN = PCED_FON.PR_REFEREN 
GO

/* Funci�n para obtener la c�dula de pago Fonacot: 1/2 */
if exists (select * from sysobjects where id = object_id(N'FONACOT_CEDULA') and xtype in (N'FN', N'IF', N'TF')) 
	drop function FONACOT_CEDULA
go

/* Funci�n para obtener la c�dula de pago Fonacot: 2/2 */
CREATE FUNCTION FONACOT_CEDULA
(	@YEAR SMALLINT,
    @MES SMALLINT,
	@RAZON_SOCIAL CHAR(6) )
RETURNS @CEDULA TABLE
	(
    NO_FONACOT varchar(30),
    RFC varchar(30),
	NOMBRE VARCHAR(250),
	NO_CREDITO varchar (8),
	RETENCION_MENSUAL numeric(15,2),
	CLAVE_EMPLEADO int,
	PLAZO smallint,
	MESES smallint,
	RETENCION_REAL numeric (15,2),
	INCIDENCIA char(1)	,
	FECHA_INI_BAJA varchar(10),
	FECHA_FIN varchar(10),
	REUBICADO CHAR(1)
	)
AS
BEGIN
	insert into @CEDULA
	( NO_FONACOT, RFC, NOMBRE, NO_CREDITO, RETENCION_MENSUAL, CLAVE_EMPLEADO, PLAZO, MESES, RETENCION_REAL,
		INCIDENCIA, FECHA_INI_BAJA, FECHA_FIN, REUBICADO)	

		SELECT C.CB_FONACOT, C.CB_RFC, UPPER (C.CB_APE_PAT + ' ' + C.CB_APE_MAT +' ' + C.CB_NOMBRES) AS PRETTYNAME, F.PR_REFEREN, F.PF_PAGO, C.CB_CODIGO, F.PF_PLAZO, F.PF_PAGADAS,	
	FE.FE_NOMINA, FE.FE_INCIDE, case when CONVERT (VARCHAR(10), FE.FE_FECHA1, 103) = '30/12/1899' then '' else CONVERT (VARCHAR(10), FE.FE_FECHA1, 103) end AS FECHA_INI_BAJA, 
	case when CONVERT (VARCHAR(10), FE.FE_FECHA2, 103) = '30/12/1899' then '' else CONVERT (VARCHAR(10), FE.FE_FECHA2, 103) end AS FECHA_FIN, F.PF_REUBICA	
	FROM PCED_FON F
		JOIN COLABORA C ON F.CB_CODIGO = C.CB_CODIGO
		JOIN FON_EMP FE ON FE.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = FE.FT_YEAR AND F.PF_MES = FE.FT_MONTH
		--LEFT OUTER JOIN FON_CRE FC ON FC.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = FC.FT_YEAR AND F.PF_MES = FC.FT_MONTH AND F.PR_TIPO = FC.PR_TIPO  AND F.PR_REFEREN = FC.PR_REFEREN
	WHERE 
		F.PF_YEAR = @YEAR
		AND F.PF_MES = @MES
		AND C.CB_PATRON IN (SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RAZON_SOCIAL or @RAZON_SOCIAL = '')

	return
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'FN_Fonacot_Get_Saldo_UltimaCedula') and xtype in (N'FN', N'IF', N'TF')) 
       drop function FN_Fonacot_Get_Saldo_UltimaCedula
go 

create function FN_Fonacot_Get_Saldo_UltimaCedula(@CB_CODIGO NumeroEmpleado, @PR_REFEREN Referencia, @PR_TIPO Codigo1)
returns Pesos 
as
begin 

       declare @PR_MONTO Pesos 

       select top 1  @PR_MONTO = PF_PAGO from PCED_FON where CB_CODIGO = @CB_CODIGO and  PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN order by PF_YEAR desc, PF_MES desc 
       set @PR_MONTO = coalesce( @PR_MONTO , 0.000 ) 

       return @PR_MONTO 
       
end
go 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_FONACOT_ACTUALIZAR_UN_PRESTAMO') AND type IN ( N'P', N'PC' ))
    drop procedure SP_FONACOT_ACTUALIZAR_UN_PRESTAMO
go

create procedure SP_FONACOT_ACTUALIZAR_UN_PRESTAMO (@CB_CODIGO NumeroEmpleado, @PR_REFEREN Referencia, @PR_TIPO Codigo1,  @iPR_STATUS status, @iUS_CODIGO Usuario, @lGrabarBitacora Booleano = 'S' ) 
as
begin 
    set nocount on 
    DECLARE @BI_TEXTO Observaciones
    DECLARE @BI_DATA NVARCHAR(MAX)
    DECLARE @BI_FEC_MOV Fecha
       DECLARE @PR_MONTO Pesos 


       set @PR_MONTO = dbo.FN_Fonacot_Get_Saldo_UltimaCedula( @CB_CODIGO, @PR_REFEREN, @PR_TIPO ) 

       update PRESTAMO set PR_STATUS = @iPR_STATUS,  US_CODIGO= @iUS_CODIGO ,  PR_MONTO = @PR_MONTO, PR_TOTAL = @PR_MONTO, PR_SALDO = @PR_MONTO, PR_PAG_PER = @PR_MONTO  
       where CB_CODIGO = @CB_CODIGO and PR_REFEREN = @PR_REFEREN and PR_TIPO = @PR_TIPO                     

       if ( @lGrabarBitacora  = 'S' ) 
       begin 
             SET @BI_TEXTO =  'Modificaci�n desde C�dula Fonacot: Ref:' + @PR_REFEREN;
             SET @BI_DATA = 'Pr�stamo: Empl.:' + cast (@CB_CODIGO as varchar) + ' Tipo:' + @PR_TIPO + ' Ref:' + @PR_REFEREN + CHAR(13)+CHAR(10) + 'PR_STATUS' + CHAR(13)+CHAR(10) + ' A : ' + cast (@iPR_STATUS as varchar)
    
             SELECT @BI_FEC_MOV = GETDATE();
             exec SP_INSERTAR_BITACORA @iUS_CODIGO, 0, 0, 0, @BI_TEXTO, @CB_CODIGO, @BI_DATA, 62, @BI_FEC_MOV;
       end; 

       
end
go 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_FONACOT_AGREGAR_UN_PRESTAMO') AND type IN ( N'P', N'PC' ))
    drop procedure SP_FONACOT_AGREGAR_UN_PRESTAMO
go
 
create procedure SP_FONACOT_AGREGAR_UN_PRESTAMO (@CB_CODIGO NumeroEmpleado, @PR_TIPO Codigo1, @PR_REFEREN Referencia, @PR_FECHA Fecha, @PR_STATUS status, @PR_OBSERVA ReferenciaLarga,  @US_CODIGO Usuario, @PF_PAGO Pesos) 
as
begin 
	set nocount on 

	if ( select count(*) from PRESTAMO where CB_CODIGO = @CB_CODIGO and PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN ) = 0
	begin 
		INSERT INTO PRESTAMO (CB_CODIGO, PR_TIPO, PR_REFEREN, PR_FECHA, PR_STATUS, PR_OBSERVA, US_CODIGO, PR_MONTO, PR_TOTAL, PR_SALDO, PR_PAG_PER) 
		VALUES ( @CB_CODIGO, @PR_TIPO, @PR_REFEREN, @PR_FECHA, @PR_STATUS, @PR_OBSERVA, @US_CODIGO, @PF_PAGO, @PF_PAGO, @PF_PAGO , @PF_PAGO )
	end

end 
GO 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_FONACOT_CANCELAR_PRESTAMOS') AND type IN ( N'P', N'PC' ))
    drop procedure SP_FONACOT_CANCELAR_PRESTAMOS
go

create procedure SP_FONACOT_CANCELAR_PRESTAMOS (@iUS_CODIGO Usuario, @RS_CODIGO Codigo = '') 
as
begin
    set nocount on 
    DECLARE @CONCEPTO_FONACOT Codigo1;
    DECLARE @Prestamos TABLE
    (
        i int identity(1,1),
        iCB_CODIGO int, 
        sPR_REFEREN varchar(8) 
    )
    SELECT @CONCEPTO_FONACOT = GL_FORMULA FROM GLOBAL WHERE GL_CODIGO = 211
    insert into @Prestamos ( iCB_CODIGO , sPR_REFEREN ) 
        select CB.CB_CODIGO, PR_REFEREN  from PRESTAMO PR
		join COLABORA CB on PR.CB_CODIGO = CB.CB_CODIGO 
		 where PR_TIPO = @CONCEPTO_FONACOT and PR_STATUS in (0) and CB.CB_PATRON in ( select TB_CODIGO from RPATRON where RS_CODIGO = @RS_CODIGO  or @RS_CODIGO = '' ) 
    declare @i int = 1 
    declare @n int 
    declare @iCB_CODIGO NumeroEmpleado 
    declare @sPR_REFEREN Referencia 
    select @n = count(*) from @Prestamos 
    while @i <= @n 
    begin          select @iCB_CODIGO = iCB_CODIGO , @sPR_REFEREN = sPR_REFEREN from @Prestamos where i = @i 
        
        exec SP_FONACOT_ACTUALIZAR_UN_PRESTAMO @iCB_CODIGO, @sPR_REFEREN, @CONCEPTO_FONACOT, 1, @iUS_CODIGO 
                
        set @i = @i + 1 
    end 
end
GO 