/*
	Patch Datos 464.
	SUA 2018.

	Tabla VALOR_UMA.
	Nuevo campo para almacenar el factor de descuento.
		UM_FDESC de tipo PesosDiario.

	Jueves 1ro de marzo de 2018.
	*/

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id=object_id('VALOR_UMA') AND name='UM_FDESC '    )
     alter table VALOR_UMA add UM_FDESC PesosDiario NULL 
go    

if ( select COUNT(*) from VALOR_UMA where UM_FEC_INI = '2018-01-01' ) = 0 
begin 
	insert into VALOR_UMA (UM_FEC_INI, UM_VALOR, UM_FDESC) values ( '2018-01-01', 75.49, 78.43 ) 
end 

go 
UPDATE VALOR_UMA SET UM_FDESC = case 
					when UM_FEC_INI >= '2018-01-01' then 78.43 
					else 0.00 
				end
where UM_FDESC is null 
go 

alter table VALOR_UMA alter column UM_FDESC PesosDiario NOT NULL 
go

