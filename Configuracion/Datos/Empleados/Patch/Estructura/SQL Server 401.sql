/*****************************************/
/****  Tablas ****************************/
/*****************************************/

/* Tabla COLABORA: Tipo de n�mina ( Tipo N�mina ) */
/* Patch 401 # Seq: 1 Agregado */
ALTER TABLE COLABORA
Add CB_NOMINA NominaTipo
GO

/* Tabla COLABORA: Fecha �ltimo cambio de n�mina ( Tipo N�mina ) */
/* Patch 401 # Seq: 2 Agregado */
ALTER TABLE COLABORA
Add CB_FEC_NOM Fecha
GO

/* Tabla KARDEX: Tipo de n�mina ( Tipo N�mina ) */
/* Patch 401 # Seq: 3 Agregado */
ALTER TABLE KARDEX
Add CB_NOMINA NominaTipo
GO

/* Tabla PLAZA: Tipo de n�mina ( Tipo N�mina ) */
/* Patch 401 # Seq: 4 Agregado */
ALTER TABLE PLAZA
Add PL_NOMINA NominaTipo
GO

/* TIPNOM_INIT_DATOS: Agregar nuevo tipo TIPNOM (1/3) ( Tipo N�mina ) */
/* Patch 401 # Seq: 5 Agregado */
create procedure TIPNOM_INIT_DATOS
as
begin
     SET NOCOUNT ON;
     declare @Cuantos Integer;
     select @Cuantos = COUNT(*) from TKARDEX where
     ( TB_CODIGO = 'TIPNOM' );
     if ( @Cuantos = 0 )
     begin
          insert into TKARDEX( TB_CODIGO, TB_ELEMENT, TB_INGLES, TB_NIVEL, TB_SISTEMA )
          values( 'TIPNOM', 'Cambio de Tipo de N�mina', 'Payroll Change', 4, 'S' );
     end
end
GO

/* TIPNOM_INIT_DATOS: Agregar nuevo tipo TIPNOM (2/3) ( Tipo N�mina ) */
/* Patch 401 # Seq: 6 Agregado */
exec TIPNOM_INIT_DATOS
GO

/* TIPNOM_INIT_DATOS: Agregar nuevo tipo TIPNOM (3/3) ( Tipo N�mina ) */
/* Patch 401 # Seq: 7 Agregado */
drop procedure TIPNOM_INIT_DATOS
GO

/* RECALCULA_KARDEX: Actualizar KARDEX.CB_NOMINA y COLABORA.CB_NOMINA ( 1/5 ) */
/* Patch 401 # Seq: 8 Modificado */
ALTER PROCEDURE RECALCULA_KARDEX(@Empleado INTEGER) AS
BEGIN
   SET NOCOUNT ON
   declare @kAUTOSAL  CHAR(1);
   declare @kClasifi  CHAR(6);
   declare @kCONTRAT  CHAR(1);
   declare @kFAC_INT  NUMERIC(15,5);
   declare @kFEC_ANT  Fecha;
   declare @kFEC_CON  Fecha;
   declare @kFEC_ING  Fecha;
   declare @kFEC_INT  Fecha;
   declare @kFEC_REV  Fecha;
   declare @kFECHA_2  Fecha;
   declare @kFECHA    Fecha;
   declare @kMOT_BAJ  CHAR(3);
   declare @kNivel1   CHAR(6);
   declare @kNivel2   CHAR(6);
   declare @kNivel3   CHAR(6);
   declare @kNivel4   CHAR(6);
   declare @kNivel5   CHAR(6);
   declare @kNivel6   CHAR(6);
   declare @kNivel7   CHAR(6);
   declare @kNivel8   CHAR(6);
   declare @kNivel9   CHAR(6);
   declare @kNOMNUME  SMALLINT;
   declare @kNOMTIPO  SMALLINT;
   declare @kNOMYEAR  SMALLINT;
   declare @kPatron   CHAR(1);
   declare @kPER_VAR  NUMERIC(15,2);
   declare @kPuesto   CHAR(6);
   declare @kSAL_INT  NUMERIC(15,2);
   declare @kSALARIO  NUMERIC(15,2);
   declare @kTABLASS  CHAR(1);
   declare @kTurno    CHAR(6);
   declare @kZona_GE  CHAR(1);
   declare @kOLD_SAL  NUMERIC(15,2);
   declare @kOLD_INT  NUMERIC(15,2);
   declare @kOTRAS_P  NUMERIC(15,2);
   declare @kPRE_INT  NUMERIC(15,2);
   declare @kREINGRE  Char(1);
   declare @kSAL_TOT  NUMERIC(15,2);
   declare @kTOT_GRA  NUMERIC(15,2);
   declare @kTIPO     CHAR(6);
   declare @kRANGO_S  NUMERIC(15,2);
   declare @kMOD_NV1  CHAR(1);
   declare @kMOD_NV2  CHAR(1);
   declare @kMOD_NV3  CHAR(1);
   declare @kMOD_NV4  CHAR(1);
   declare @kMOD_NV5  CHAR(1);
   declare @kMOD_NV6  CHAR(1);
   declare @kMOD_NV7  CHAR(1);
   declare @kMOD_NV8  CHAR(1);
   declare @kMOD_NV9  CHAR(1);
   declare @kPLAZA    FolioGrande;
   declare @kNOMINA   SMALLINT;

   declare @eACTIVO   CHAR(1);
   declare @eAUTOSAL  CHAR(1);
   declare @eClasifi  CHAR(6);
   declare @eCONTRAT  CHAR(1);
   declare @eFAC_INT  NUMERIC(15,5);
   declare @eFEC_ANT  Fecha;
   declare @eFEC_CON  Fecha;
   declare @eFEC_ING  Fecha;
   declare @eFEC_INT  Fecha;
   declare @eFEC_REV  Fecha;
   declare @eFEC_SAL  Fecha;
   declare @eFEC_BAJ  Fecha;
   declare @eFEC_BSS  Fecha;
   declare @eMOT_BAJ  CHAR(3);
   declare @eNivel1   CHAR(6);
   declare @eNivel2   CHAR(6);
   declare @eNivel3   CHAR(6);
   declare @eNivel4   CHAR(6);
   declare @eNivel5   CHAR(6);
   declare @eNivel6   CHAR(6);
   declare @eNivel7   CHAR(6);
   declare @eNivel8   CHAR(6);
   declare @eNivel9   CHAR(6);
   declare @eNOMNUME  SMALLINT;
   declare @eNOMTIPO  SMALLINT;
   declare @eNOMYEAR  SMALLINT;
   declare @ePatron   CHAR(1);
   declare @ePER_VAR  NUMERIC(15,2);
   declare @ePuesto   CHAR(6);
   declare @eSAL_INT  NUMERIC(15,2);
   declare @eSALARIO  NUMERIC(15,2);
   declare @eTABLASS  CHAR(1);
   declare @eTIP_REV  CHAR(6);
   declare @eTurno    CHAR(6);
   declare @eZona_GE  CHAR(1);
   declare @eOLD_SAL  NUMERIC(15,2);
   declare @eOLD_INT  NUMERIC(15,2);
   declare @eOTRAS_P  NUMERIC(15,2);
   declare @ePRE_INT  NUMERIC(15,2);
   declare @eREINGRE  Char(1);
   declare @eSAL_TOT  NUMERIC(15,2);
   declare @eTOT_GRA  NUMERIC(15,2);
   declare @eRANGO_S  NUMERIC(15,2);
   declare @eFEC_NIV  Fecha;
   declare @eFEC_PTO  Fecha;
   declare @eFEC_TUR  Fecha;
   declare @FechaVacia Fecha;
   declare @eFEC_KAR  Fecha;
   declare @ePLAZA    FolioGrande;
   declare @eFEC_PLA  Fecha;
   declare @eNOMINA   SMALLINT;
   declare @eFEC_NOM  Fecha;
   declare CursorKardex CURSOR SCROLL_LOCKS FOR
   select	CB_AUTOSAL, CB_CLASIFI,   CB_CONTRAT, CB_FAC_INT, CB_FEC_ANT,
    CB_FEC_CON, CB_FEC_ING,   CB_FEC_INT, CB_FEC_REV, CB_FECHA,    CB_FECHA_2 ,
    CB_MOT_BAJ, CB_NIVEL1,    CB_NIVEL2 , CB_NIVEL3 , CB_NIVEL4,   CB_NIVEL5 ,   CB_NIVEL6 ,
    CB_NIVEL7 , CB_NIVEL8,    CB_NIVEL9 , CB_NOMNUME ,CB_NOMTIPO , CB_NOMYEAR ,
    CB_OTRAS_P, CB_PATRON ,   CB_PER_VAR ,CB_PRE_INT ,
    CB_PUESTO , CB_REINGRE ,  CB_SAL_INT ,CB_SAL_TOT, CB_SALARIO,  CB_TABLASS ,
    CB_TIPO ,   CB_TOT_GRA,   CB_TURNO ,  CB_ZONA_GE, CB_RANGO_S, CB_PLAZA,
    CB_NOMINA
   from Kardex
   where CB_CODIGO = @Empleado
   order by cb_fecha,cb_nivel

      	SET @FechaVacia = '12/30/1899';
	      SET @eACTIVO  = 'S';
      	SET @eFEC_BAJ = @FechaVacia;
      	SET @eFEC_BSS = @FechaVacia;
      	SET @eMOT_BAJ = ' ';
      	SET @eNOMYEAR = 0;
      	SET @eNOMTIPO = 0;
      	SET @eNOMNUME = 0;
      	SET @eREINGRE = 'N';
      	SET @eSALARIO = 0;
      	SET @eSAL_INT = 0;
      	SET @eFAC_INT = 0;
      	SET @ePRE_INT = 0;
      	SET @ePER_VAR = 0;
      	SET @eSAL_TOT = 0;
      	SET @eAUTOSAL = 'N';
      	SET @eTOT_GRA = 0;
      	SET @eFEC_REV = @FechaVacia;
      	SET @eFEC_INT = @FechaVacia;
      	SET @eOTRAS_P = 0;
      	SET @eOLD_SAL = 0;
      	SET @eOLD_INT = 0;
      	SET @eRANGO_S = 0;
        SET @eFEC_PLA = @FechaVacia;
        SET @eFEC_NOM = @FechaVacia;

	Open CursorKardex
	Fetch NEXT from CursorKardex
	into	@kAUTOSAL, @kCLASIFI, @kCONTRAT,  @kFAC_INT,  @kFEC_ANT ,
      @kFEC_CON, @kFEC_ING, @kFEC_INT , @kFEC_REV  ,@kFECHA,   @kFECHA_2,
      @kMOT_BAJ ,@kNivel1,  @kNivel2 ,  @kNivel3,   @kNivel4,  @kNivel5,  @kNivel6,
      @kNivel7,  @kNivel8,  @kNivel9,   @kNOMNUME , @kNOMTIPO, @kNOMYEAR,
      @kOTRAS_P, @kPatron,  @kPER_VAR,  @kPRE_INT,
      @kPuesto,  @kREINGRE, @kSAL_INT,  @kSAL_TOT,  @kSALARIO, @kTABLASS,
      @kTIPO,    @kTOT_GRA, @kTurno,    @kZONA_GE,  @kRANGO_S, @kPLAZA, 
      @kNOMINA


	if ( @@FETCH_STATUS <> 0 )
	BEGIN
		Close CursorKardex
		Deallocate CursorKardex
		RETURN
	END

	
	if ( @kTIPO <> 'ALTA' )
	BEGIN
		Close CursorKardex
		Deallocate CursorKardex
		/*RAISERROR( 'Empleado #%d, ERROR en KARDEX: Movimiento(s) previo(s) a la ALTA', 16, 1, @Empleado ) No se levanta la excepcion del SP normal, para no detener el patch*/
		RETURN
	END

	while ( @@FETCH_STATUS = 0 )
	BEGIN
        	if ( @kTIPO = 'ALTA' OR @kTIPO = 'CAMBIO' OR @kTIPO = 'CIERRE' )
          	BEGIN
			if ABS( @kSALARIO - @eSALARIO ) >= 0.01
			BEGIN
				SET @kOLD_SAL = @eSALARIO;
				SET @kFEC_REV = @kFECHA;
				SET @eFEC_REV = @kFEC_REV;
				SET @eSALARIO = @kSALARIO;
				SET @eOLD_SAL = @kOLD_SAL;
			END
			else
			BEGIN
				SET @kOLD_SAL = @eOLD_SAL;
				SET @kFEC_REV = @eFEC_REV;
			END
			if ABS( @kSAL_INT - @eSAL_INT ) >= 0.01
			BEGIN
				SET @kOLD_INT = @eSAL_INT;
				if ( @kTIPO = 'CAMBIO' )
					SET @kFEC_INT = @kFECHA_2;
				else
					SET @kFEC_INT = @kFECHA;
				SET @eFEC_INT = @kFEC_INT;
				SET @eSAL_INT = @kSAL_INT;
				SET @eOLD_INT = @kOLD_INT;
			END
			else
			BEGIN
        SET @kOLD_INT = @eOLD_INT;
	       SET @kFEC_INT = @eFEC_INT;
			END
			SET @eFAC_INT = @kFAC_INT;
			SET @ePRE_INT = @kPRE_INT;
			SET @ePER_VAR = @kPER_VAR;
			SET @eSAL_TOT = @kSAL_TOT;
			SET @eTOT_GRA = @kTOT_GRA;
			SET @eOTRAS_P = @kOTRAS_P;
			SET @eZONA_GE = @kZONA_GE;
			SET @eTIP_REV = @kTIPO;
			SET @eFEC_SAL = @kFECHA;
			SET @eRANGO_S = @kRANGO_S;
		END
		else
		BEGIN
			SET @kSALARIO = @eSALARIO;
			SET @kOLD_SAL = @eOLD_SAL;
			SET @kFEC_REV = @eFEC_REV;
			SET @kSAL_INT = @eSAL_INT;
			SET @kOLD_INT = @eOLD_INT;
			SET @kFEC_INT = @eFEC_INT;
			SET @kFAC_INT = @eFAC_INT;
			SET @kPRE_INT = @ePRE_INT;
			SET @kPER_VAR = @ePER_VAR;
			SET @kSAL_TOT = @eSAL_TOT;
			SET @kTOT_GRA = @eTOT_GRA;
			SET @kOTRAS_P = @eOTRAS_P;
			SET @kZONA_GE = @eZONA_GE;
			SET @kRANGO_S = @eRANGO_S;
		END

		if ( @kTIPO = 'ALTA' or @kTIPO = 'PUESTO' or @kTIPO = 'CAMBIO' or @kTIPO = 'CIERRE' )
			SET @eAUTOSAL = @kAUTOSAL;
		else
			SET @kAUTOSAL = @eAUTOSAL;

		if ( @kAUTOSAL <> 'S' )
		BEGIN
			SET @kRANGO_S = 0;
			SET @eRANGO_S = 0;
		END

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'PUESTO' OR @kTIPO = 'CIERRE' OR @kTIPO = 'PLAZA' )
		BEGIN
			SET @ePUESTO   = @kPUESTO;
			SET @eFEC_PTO  = @kFECHA;
                        if ( @kTIPO = 'PLAZA' AND @eAUTOSAL = 'S' )
                             SET @kCLASIFI = @eCLASIFI;
                        else
			     SET @eCLASIFI  = @kCLASIFI;
		END
		else
		BEGIN
			SET @kPUESTO   = @ePUESTO;
			SET @kCLASIFI  = @eCLASIFI;
		END

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'AREA' OR @kTIPO = 'CIERRE' OR @kTIPO = 'PLAZA' )
		BEGIN
			if ( ( @kTIPO = 'AREA' ) OR ( @kTIPO = 'CIERRE' ) OR ( @kTIPO = 'PLAZA' ))
			BEGIN
				if ( @kNIVEL1 <> @eNIVEL1 )
     					SET @kMOD_NV1 = 'S';
            			else
                 			SET @kMOD_NV1 = 'N';
				if ( @kNIVEL2 <> @eNIVEL2 )
     					SET @kMOD_NV2 = 'S';
            			else
                 			SET @kMOD_NV2 = 'N';
				if ( @kNIVEL3 <> @eNIVEL3 )
     					SET @kMOD_NV3 = 'S';
            			else
                 			SET @kMOD_NV3 = 'N';
				if ( @kNIVEL4 <> @eNIVEL4 )
     					SET @kMOD_NV4 = 'S';
            			else
                 			SET @kMOD_NV4 = 'N';
				if ( @kNIVEL5 <> @eNIVEL5 )
     					SET @kMOD_NV5 = 'S';
            			else
                 			SET @kMOD_NV5 = 'N';
				if ( @kNIVEL6 <> @eNIVEL6 )
     					SET @kMOD_NV6 = 'S';
            			else
                 			SET @kMOD_NV6 = 'N';
				if ( @kNIVEL7 <> @eNIVEL7 )
     					SET @kMOD_NV7 = 'S';
            			else
                 			SET @kMOD_NV7 = 'N';
				if ( @kNIVEL8 <> @eNIVEL8 )
     					SET @kMOD_NV8 = 'S';
            			else
                 			SET @kMOD_NV8 = 'N';
				if ( @kNIVEL9 <> @eNIVEL9 )
     					SET @kMOD_NV9 = 'S';
            			else
                 			SET @kMOD_NV9 = 'N';
			END
			else
			BEGIN
                 if ( @kNIVEL1 <> '' )
                     SET @kMOD_NV1 = 'S';
                 else
                     SET @kMOD_NV1 = 'N';
                 if ( @kNIVEL2 <> '' )
                     SET @kMOD_NV2 = 'S';
                 else
                     SET @kMOD_NV2 = 'N';
                 if ( @kNIVEL3 <> '' )
                     SET @kMOD_NV3 = 'S';
                 else
                     SET @kMOD_NV3 = 'N';
                 if ( @kNIVEL4 <> '' )
                     SET @kMOD_NV4 = 'S';
                 else
                     SET @kMOD_NV4 = 'N';
                 if ( @kNIVEL5 <> '' )
                     SET @kMOD_NV5 = 'S';
                 else
                     SET @kMOD_NV5 = 'N';
                 if ( @kNIVEL6 <> '' )
                     SET @kMOD_NV6 = 'S';
                 else
                     SET @kMOD_NV6 = 'N';
                 if ( @kNIVEL7 <> '' )
                     SET @kMOD_NV7 = 'S';
                 else
                     SET @kMOD_NV7 = 'N';
                 if ( @kNIVEL8 <> '' )
                     SET @kMOD_NV8 = 'S';
                 else
                     SET @kMOD_NV8 = 'N';
                 if ( @kNIVEL9 <> '' )
                     SET @kMOD_NV9 = 'S';
                 else
                     SET @kMOD_NV9 = 'N';

			END
			SET @eNIVEL1 = @kNIVEL1;
			SET @eNIVEL2 = @kNIVEL2;
			SET @eNIVEL3 = @kNIVEL3;
			SET @eNIVEL4 = @kNIVEL4;
			SET @eNIVEL5 = @kNIVEL5;
			SET @eNIVEL6 = @kNIVEL6;
			SET @eNIVEL7 = @kNIVEL7;
			SET @eNIVEL8 = @kNIVEL8;
			SET @eNIVEL9 = @kNIVEL9;
			SET @eFEC_NIV = @kFECHA;
		END
		else
		BEGIN
			SET @kNIVEL1 = @eNIVEL1;
			SET @kNIVEL2 = @eNIVEL2;
			SET @kNIVEL3 = @eNIVEL3;
			SET @kNIVEL4 = @eNIVEL4;
			SET @kNIVEL5 = @eNIVEL5;
			SET @kNIVEL6 = @eNIVEL6;
			SET @kNIVEL7 = @eNIVEL7;
			SET @kNIVEL8 = @eNIVEL8;
			SET @kNIVEL9 = @eNIVEL9;
            SET @kMOD_NV1 = 'N';
            SET @kMOD_NV2 = 'N';
            SET @kMOD_NV3 = 'N';
            SET @kMOD_NV4 = 'N';
            SET @kMOD_NV5 = 'N';
            SET @kMOD_NV6 = 'N';
            SET @kMOD_NV7 = 'N';
            SET @kMOD_NV8 = 'N';
            SET @kMOD_NV9 = 'N';
		END

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'PRESTA' OR @kTIPO = 'CIERRE' )
			SET @eTABLASS = @kTABLASS;
		else
			SET @kTABLASS = @eTABLASS;

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'TURNO' OR @kTIPO = 'CIERRE' OR @kTIPO = 'PLAZA' )
		BEGIN
			SET @eTURNO   = @kTURNO;
			SET @eFEC_TUR = @kFECHA;
		END
		else
			SET @kTURNO   = @eTURNO;

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'TIPNOM' OR @kTIPO = 'CIERRE' )
		BEGIN
			SET @eNOMINA  = @kNOMINA;
			SET @eFEC_NOM = @kFECHA;
		END
		else
			SET @kNOMINA  = @eNOMINA;

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'RENOVA' OR @kTIPO = 'CIERRE' )
		BEGIN
			SET @eCONTRAT = @kCONTRAT;
			SET @eFEC_CON = @kFEC_CON;
		END
		else
		BEGIN
			SET @kCONTRAT = @eCONTRAT;
			SET @kFEC_CON = @eFEC_CON;
		END

		if ( @kTIPO = 'ALTA' )
		BEGIN
			if ( @eACTIVO = 'S' )
				SET @kREINGRE = 'N';
            		else
			BEGIN
				SET @kREINGRE = 'S';
				SET @eREINGRE = 'S';
			END

			SET @eFEC_ING = @kFECHA;
			SET @eFEC_ANT = @kFEC_ANT;
			SET @eACTIVO  = 'S';
			SET @ePATRON  = @kPATRON;
			SET @kFEC_ING = @eFEC_ING;
			SET @kFEC_ANT = @eFEC_ANT;
		END
		else
		BEGIN
			SET @kREINGRE = @eREINGRE;
			SET @kPATRON  = @ePATRON;
			SET @kFEC_ING = @eFEC_ING;
			SET @kFEC_ANT = @eFEC_ANT;
		END

		if ( @kTIPO = 'BAJA' )
		BEGIN
			SET @eFEC_BAJ  =  @kFECHA;
			SET @eFEC_BSS  =  @kFECHA_2;
			SET @eMOT_BAJ  =  @kMOT_BAJ;
			SET @eACTIVO   =  'N';
			SET @eNOMYEAR  =  @kNOMYEAR;
			SET @eNOMTIPO  =  @kNOMTIPO;
			SET @eNOMNUME  =  @kNOMNUME;
			SET @ePLAZA    =  0;
		END
		else
		BEGIN
			SET @kMOT_BAJ = @eMOT_BAJ;
			SET @kNOMYEAR = @eNOMYEAR;
			SET @kNOMTIPO = @eNOMTIPO;
			SET @kNOMNUME = @eNOMNUME;
		END


		if ( @kTIPO = 'ALTA'  OR @kTIPO = 'CIERRE' OR @kTIPO = 'PLAZA' )
		BEGIN
			SET @ePLAZA    = @kPLAZA;
			SET @eFEC_PLA  = @kFECHA;
		END
		else
		BEGIN
			SET @kPLAZA    = @ePLAZA;
		END

		update KARDEX
     		set 	CB_AUTOSAL = @kAUTOSAL,
			CB_CLASIFI = @kCLASIFI,
			CB_CONTRAT = @kCONTRAT,
			CB_FEC_ANT = @kFEC_ANT,
			CB_FEC_CON = @kFEC_CON,
			CB_FEC_ING = @kFEC_ING,
			CB_FEC_INT = @kFEC_INT,
			CB_FEC_REV = @kFEC_REV,
			CB_FECHA_2 = @kFECHA_2,
			CB_MOT_BAJ = @kMOT_BAJ,
			CB_NIVEL1  = @kNIVEL1,
			CB_NIVEL2  = @kNIVEL2,
			CB_NIVEL3  = @kNIVEL3,
			CB_NIVEL4  = @kNIVEL4,
			CB_NIVEL5  = @kNIVEL5,
			CB_NIVEL6  = @kNIVEL6,
			CB_NIVEL7  = @kNIVEL7,
			CB_NIVEL8  = @kNIVEL8,
			CB_NIVEL9  = @kNIVEL9,
			CB_MOD_NV1 = @kMOD_NV1,
			CB_MOD_NV2 = @kMOD_NV2,
			CB_MOD_NV3 = @kMOD_NV3,
			CB_MOD_NV4 = @kMOD_NV4,
			CB_MOD_NV5 = @kMOD_NV5,
			CB_MOD_NV6 = @kMOD_NV6,
			CB_MOD_NV7 = @kMOD_NV7,
			CB_MOD_NV8 = @kMOD_NV8,
			CB_MOD_NV9 = @kMOD_NV9,
			CB_NOMNUME = @kNOMNUME,
			CB_NOMTIPO = @kNOMTIPO,
			CB_NOMYEAR = @kNOMYEAR,
			CB_OTRAS_P = @kOTRAS_P,
			CB_PATRON  = @kPATRON,
			CB_PER_VAR = @kPER_VAR,
			CB_PRE_INT = @kPRE_INT,
			CB_PUESTO  = @kPUESTO,
			CB_REINGRE = @kREINGRE,
			CB_SAL_INT = @kSAL_INT,
			CB_SAL_TOT = @kSAL_TOT,
			CB_SALARIO = @kSALARIO,
			CB_TABLASS = @kTABLASS,
			CB_TOT_GRA = @kTOT_GRA,
			CB_TURNO   = @kTURNO,
			CB_ZONA_GE = @kZONA_GE,
			CB_OLD_SAL = @kOLD_SAL,
			CB_OLD_INT = @kOLD_INT,
			CB_FAC_INT = @kFAC_INT,
			CB_RANGO_S = @kRANGO_S,
      CB_PLAZA   = @kPLAZA,
      CB_NOMINA  = @kNOMINA
		where CURRENT OF CursorKardex


		Fetch NEXT from CursorKardex
		into	@kAUTOSAL, @kCLASIFI, @kCONTRAT,  @kFAC_INT,  @kFEC_ANT ,
       @kFEC_CON, @kFEC_ING, @kFEC_INT , @kFEC_REV  ,@kFECHA,   @kFECHA_2,
       @kMOT_BAJ ,@kNivel1,  @kNivel2 ,  @kNivel3,   @kNivel4,  @kNivel5,  @kNivel6,
       @kNivel7,  @kNivel8,  @kNivel9,   @kNOMNUME , @kNOMTIPO, @kNOMYEAR,
       @kOTRAS_P, @kPatron,  @kPER_VAR,  @kPRE_INT,
       @kPuesto,  @kREINGRE, @kSAL_INT,  @kSAL_TOT,  @kSALARIO, @kTABLASS,
       @kTIPO,    @kTOT_GRA, @kTurno,    @kZONA_GE,  @kRANGO_S, @kPLAZA,
       @kNOMINA
	END

	Close CursorKardex
	Deallocate CursorKardex

	
	SET @eFEC_KAR = @eFEC_ING;
	if ( @eFEC_NIV > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_NIV;
	if ( @eFEC_SAL > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_SAL;
	if ( @eFEC_PTO > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_PTO;
	if ( @eFEC_TUR > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_TUR;
	if ( @eFEC_PLA > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_PLA;

	update COLABORA
	set CB_AUTOSAL = @eAUTOSAL,
		CB_ACTIVO  = @eACTIVO,
		CB_CLASIFI = @eCLASIFI,
		CB_CONTRAT = @eCONTRAT,
		CB_FAC_INT = @eFAC_INT,
		CB_FEC_ANT = @eFEC_ANT,
		CB_FEC_CON = @eFEC_CON,
		CB_FEC_ING = @eFEC_ING,
		CB_FEC_INT = @eFEC_INT,
		CB_FEC_REV = @eFEC_REV,
		CB_FEC_SAL = @eFEC_SAL,
		CB_FEC_BAJ = @eFEC_BAJ,
		CB_FEC_BSS = @eFEC_BSS,
		CB_MOT_BAJ = @eMOT_BAJ,
		CB_NIVEL1  = @eNIVEL1,
		CB_NIVEL2  = @eNIVEL2,
		CB_NIVEL3  = @eNIVEL3,
		CB_NIVEL4  = @eNIVEL4,
		CB_NIVEL5  = @eNIVEL5,
		CB_NIVEL6  = @eNIVEL6,
		CB_NIVEL7  = @eNIVEL7,
    CB_NIVEL8  = @eNIVEL8,
		CB_NIVEL9  = @eNIVEL9,
		CB_NOMNUME = @eNOMNUME,
		CB_NOMTIPO = @eNOMTIPO,
		CB_NOMYEAR = @eNOMYEAR,
		CB_PATRON  = @ePATRON,
		CB_PER_VAR = @ePER_VAR,
		CB_PUESTO  = @ePUESTO,
		CB_SAL_INT = @eSAL_INT,
		CB_SALARIO = @eSALARIO,
		CB_TABLASS = @eTABLASS,
		CB_TURNO   = @eTURNO,
		CB_ZONA_GE = @eZONA_GE,
		CB_OLD_SAL = @eOLD_SAL,
		CB_OLD_INT = @eOLD_INT,
		CB_PRE_INT = @ePRE_INT,
		CB_SAL_TOT = @eSAL_TOT,
		CB_TOT_GRA = @eTOT_GRA,
		CB_TIP_REV = @eTIP_REV,
		CB_RANGO_S = @eRANGO_S,
		CB_FEC_NIV = @eFEC_NIV,
		CB_FEC_PTO = @eFEC_PTO,
		CB_FEC_TUR = @eFEC_TUR,
		CB_FEC_KAR = @eFEC_KAR,
                CB_PLAZA   = @ePLAZA,
                CB_FEC_PLA = @eFEC_PLA,
                CB_NOMINA  = @eNOMINA,
                CB_FEC_NOM = @eFEC_NOM
	where ( CB_CODIGO = @Empleado );
END
GO

/* INSERTA_KARDEX_TIPNOM: Actualizar KARDEX.CB_NOMINA y COLABORA.CB_NOMINA ( 2/5 ) */
/* Patch 401 # Seq: 9 Agregado */
CREATE PROCEDURE INSERTA_KARDEX_TIPNOM AS
BEGIN
	SET NOCOUNT ON
	declare @Empleado integer;
	declare @Turno char(6);
	declare @Tipo char(6);
	declare @Fecha DateTime;
  declare @FechaCap DateTime;
  declare @Usuario Usuario;
	declare @TipoNomina integer;
	declare @TipoNominaAnterior integer;

  declare CursorColabora CURSOR SCROLL_LOCKS FOR
	select	CB_CODIGO from COLABORA
	order by CB_CODIGO

  Open CursorColabora
	Fetch NEXT from CursorColabora
	into	@Empleado

	if ( @@FETCH_STATUS <> 0 )
	BEGIN
		Close CursorColabora
		Deallocate CursorColabora
		RETURN
	END

	while ( @@FETCH_STATUS = 0 )
	BEGIN
		SET @TipoNominaAnterior = 0;

		declare Cursor_Kardex CURSOR SCROLL_LOCKS FOR
		select CB_TURNO, CB_TIPO, CB_FECHA, CB_FEC_CAP, US_CODIGO, TU_NOMINA
                from Kardex
		left outer join TURNO on TURNO.TU_CODIGO = KARDEX.CB_TURNO
		where CB_CODIGO = @Empleado and (CB_TIPO = 'ALTA' OR CB_TIPO ='TURNO' OR CB_TIPO='CIERRE' OR CB_TIPO='PLAZA')
		and (TU_NOMINA>0)
		order by CB_FECHA, CB_TIPO

		Open Cursor_Kardex
		Fetch NEXT from Cursor_Kardex
		into @Turno, @Tipo, @Fecha, @FechaCap, @Usuario, @TipoNomina

   	if ( @@FETCH_STATUS <> 0 )
  	BEGIN
		  UPDATE COLABORA set CB_NOMINA = ( select TU_NOMINA from TURNO where TURNO.TU_CODIGO = COLABORA.CB_TURNO ),
                          CB_FEC_NOM = CB_FEC_TUR
      where COLABORA.CB_CODIGO = @Empleado
	  END

		while ( @@FETCH_STATUS = 0 )
		begin
			if ( @Tipo = 'ALTA' ) or ( @Tipo = 'CIERRE' )
			begin
				update KARDEX
				set CB_NOMINA = @TipoNomina
				where CURRENT OF Cursor_Kardex
			end

			else if ( ( @Tipo = 'TURNO' ) or ( @Tipo = 'PLAZA' ) ) and ( @TipoNominaAnterior <> @TipoNomina )
			begin
        delete from KARDEX
        where ( CB_CODIGO = @Empleado ) and ( CB_FECHA = @Fecha ) and ( CB_TIPO = 'TIPNOM' );

				insert into KARDEX ( CB_CODIGO, CB_FECHA, CB_FECHA_2, CB_TIPO, CB_NOMINA,
                             CB_COMENTA, CB_NIVEL, CB_FEC_CAP, US_CODIGO )
				VALUES( @Empleado, @Fecha, @Fecha, 'TIPNOM', @TipoNomina,
                'Cambio de Tipo de N�mina', 4, @FechaCap, @Usuario );
			end

			SET @TipoNominaAnterior = @TipoNomina;

			Fetch NEXT from Cursor_Kardex
			into @Turno, @Tipo, @Fecha, @FechaCap, @Usuario, @TipoNomina
		end;


		EXECUTE RECALCULA_KARDEX @Empleado
		if ( @@ERROR <> 0 )
			BREAK

		Close Cursor_Kardex
		Deallocate Cursor_Kardex

		Fetch NEXT from CursorColabora
		into @Empleado
	END

	Close CursorColabora
	Deallocate CursorColabora
END
GO

/* INSERTA_KARDEX_TIPNOM: Actualizar KARDEX.CB_NOMINA y COLABORA.CB_NOMINA ( 3/5 ) */
/* Patch 401 # Seq: 10 Agregado */
EXECUTE INSERTA_KARDEX_TIPNOM
GO

/* INSERTA_KARDEX_TIPNOM: Actualizar KARDEX.CB_NOMINA y COLABORA.CB_NOMINA ( 4/5 ) */
/* Patch 401 # Seq: 11 Agregado */
DROP PROCEDURE INSERTA_KARDEX_TIPNOM
GO

/* RECALCULA_KARDEX: Actualizar KARDEX.CB_NOMINA y COLABORA.CB_NOMINA ( 5/5 ) */
/* Patch 401 # Seq: 12 Agregado */
DROP PROCEDURE RECALCULA_KARDEX
GO

/*****************************************/
/**** Foreign Keys ***********************/
/*****************************************/

/*****************************************/
/**** Views ******************************/
/*****************************************/

/*****************************************/
/****  Triggers  *************************/
/*****************************************/

/**************************************************/
/* STORED PROCEDURES ******************************/
/**************************************************/

/* SP_KARDEX_CB_NOMINA: Obtener campo KARDEX.CB_NOMINA */
/* Patch 401 # Seq: 253 Agregado */
CREATE FUNCTION SP_KARDEX_CB_NOMINA( @FECHA DATETIME, @EMPLEADO INTEGER ) RETURNS NominaTipo AS
BEGIN
  declare @Regresa NominaTipo;
  set @Regresa = ( select TOP 1 CB_NOMINA from KARDEX WHERE CB_CODIGO = @Empleado and
                   CB_FECHA <= @Fecha order by CB_FECHA desc, CB_NIVEL desc )
  if ( @Regresa is NULL )
      set @Regresa = ( select TOP 1 CB_NOMINA from KARDEX WHERE CB_CODIGO = @Empleado
                       order by CB_FECHA, CB_NIVEL )
  RETURN COALESCE( @Regresa, 0)
END
GO

/* SP_KARDEX_CB_PLAZA: Obtener campo KARDEX.CB_PLAZA */
/* Patch 401 # Seq: 254 Agregado */
CREATE FUNCTION SP_KARDEX_CB_PLAZA( @FECHA DATETIME, @EMPLEADO INTEGER ) RETURNS FolioGrande AS
BEGIN
  declare @Regresa FolioGrande;
  set @Regresa = ( select TOP 1 CB_PLAZA from KARDEX WHERE CB_CODIGO = @Empleado and
                   CB_FECHA <= @Fecha order by CB_FECHA desc, CB_NIVEL desc )
  if ( @Regresa is NULL )
      set @Regresa = ( select TOP 1 CB_PLAZA from KARDEX WHERE CB_CODIGO = @Empleado
                       order by CB_FECHA, CB_NIVEL )
  RETURN COALESCE( @Regresa, 0 )
END
GO

/* SP_FECHA_KARDEX: Obtener campos KARDEX.CB_PLAZA, KARDEX.CB_NOMINA */
/* Patch 401 # Seq: 174 Modificado */
ALTER FUNCTION SP_FECHA_KARDEX (
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS @RegKardex TABLE
( CB_FECHA DATETIME,
  CB_TIPO CHAR(6),
  CB_AUTOSAL CHAR(1),
  CB_CLASIFI CHAR(6),
  CB_COMENTA VARCHAR(50),
  CB_CONTRAT CHAR(1),
  CB_FAC_INT NUMERIC(15, 5),
  CB_FEC_CAP DATETIME,
  CB_FEC_CON DATETIME,
  CB_FEC_INT DATETIME,
  CB_FEC_REV DATETIME,
  CB_FECHA_2 DATETIME,
  CB_GLOBAL CHAR(1),
  CB_TURNO CHAR(6),
  CB_MONTO NUMERIC(15, 2),
  CB_MOT_BAJ CHAR(3),
  CB_OLD_INT NUMERIC(15, 2),
  CB_OLD_SAL NUMERIC(15, 2),
  CB_OTRAS_P NUMERIC(15, 2),
  CB_PATRON CHAR(1),
  CB_PER_VAR NUMERIC(15, 2),
  CB_PRE_INT NUMERIC(15, 2),
  CB_PUESTO CHAR(6),
  CB_RANGO_S NUMERIC(15, 2),
  CB_SAL_INT NUMERIC(15, 2),
  CB_SAL_TOT NUMERIC(15, 2),
  CB_SALARIO NUMERIC(15, 2),
  CB_STATUS SMALLINT,
  CB_TABLASS CHAR(1),
  CB_TOT_GRA NUMERIC(15, 2),
  US_CODIGO SMALLINT,
  CB_ZONA_GE CHAR(1),
  CB_NIVEL1 CHAR(6),
  CB_NIVEL2 CHAR(6),
  CB_NIVEL3 CHAR(6),
  CB_NIVEL4 CHAR(6),
  CB_NIVEL5 CHAR(6),
  CB_NIVEL6 CHAR(6),
  CB_NIVEL7 CHAR(6),
  CB_NIVEL8 CHAR(6),
  CB_NIVEL9 CHAR(6),
  CB_NOMTIPO SMALLINT,
  CB_NOMYEAR SMALLINT,
  CB_NOMNUME SMALLINT,
  CB_REINGRE CHAR(1),
  CB_FEC_ING DATETIME,
  CB_FEC_ANT DATETIME,
  CB_TIP_REV CHAR(6),
  CB_FEC_SAL DATETIME,
  CB_PLAZA INTEGER, 
  CB_NOMINA SMALLINT
) AS     
begin
	INSERT INTO @RegKardex
	select TOP 1
	       CB_FECHA, CB_TIPO, CB_AUTOSAL, CB_CLASIFI, CB_COMENTA, CB_CONTRAT,
               CB_FAC_INT, CB_FEC_CAP, CB_FEC_CON, CB_FEC_INT, CB_FEC_REV, CB_FECHA_2,
       	       CB_GLOBAL, CB_TURNO, CB_MONTO, CB_MOT_BAJ, CB_OLD_INT, CB_OLD_SAL, CB_OTRAS_P,
               CB_PATRON, CB_PER_VAR, CB_PRE_INT, CB_PUESTO, CB_RANGO_S,
       	       CB_SAL_INT, CB_SAL_TOT, CB_SALARIO, CB_STATUS, CB_TABLASS, CB_TOT_GRA,
               US_CODIGO, CB_ZONA_GE, CB_NIVEL1, CB_NIVEL2, CB_NIVEL3,
       	       CB_NIVEL4, CB_NIVEL5, CB_NIVEL6, CB_NIVEL7, CB_NIVEL8, CB_NIVEL9,
               CB_NOMTIPO, CB_NOMYEAR, CB_NOMNUME, CB_REINGRE, CB_FEC_ING, CB_FEC_ANT, 
               CB_TIP_REV, CB_FEC_SAL, CB_PLAZA, CB_NOMINA  
	from KARDEX
	WHERE ( CB_CODIGO = @Empleado ) and ( CB_FECHA <= @Fecha )
	order by CB_FECHA desc, CB_NIVEL desc

	if ( @@ROWCOUNT = 0 )
	BEGIN
		INSERT INTO @RegKardex
		select TOP 1
		       CB_FECHA, CB_TIPO, CB_AUTOSAL, CB_CLASIFI, CB_COMENTA, CB_CONTRAT,
	               CB_FAC_INT, CB_FEC_CAP, CB_FEC_CON, CB_FEC_INT, CB_FEC_REV, CB_FECHA_2,
       		       CB_GLOBAL, CB_TURNO, CB_MONTO, CB_MOT_BAJ, CB_OLD_INT, CB_OLD_SAL, CB_OTRAS_P,
	               CB_PATRON, CB_PER_VAR, CB_PRE_INT, CB_PUESTO, CB_RANGO_S,
       		       CB_SAL_INT, CB_SAL_TOT, CB_SALARIO, CB_STATUS, CB_TABLASS, CB_TOT_GRA,
	               US_CODIGO, CB_ZONA_GE, CB_NIVEL1, CB_NIVEL2, CB_NIVEL3,
       		       CB_NIVEL4, CB_NIVEL5, CB_NIVEL6, CB_NIVEL7, CB_NIVEL8, CB_NIVEL9,
	               CB_NOMTIPO, CB_NOMYEAR, CB_NOMNUME, CB_REINGRE, CB_FEC_ING, CB_FEC_ANT, 
	               CB_TIP_REV, CB_FEC_SAL, CB_PLAZA, CB_NOMINA
		from KARDEX
		WHERE ( CB_CODIGO = @Empleado )
		order by CB_FECHA, CB_NIVEL

		if ( @@ROWCOUNT = 0 )
		BEGIN
      		select @Fecha = '12/30/1899';
			INSERT INTO @RegKardex
			VALUES ( @Fecha, '', 'N', '', '', '',
	        	       0, @Fecha, @Fecha, @Fecha, @Fecha, @Fecha,
	        	       'N', '', 0, '', 0, 0, 0,
		               '', 0, 0, '', 0,
        		       0, 0, 0, 0, '', 0,
		               0, '', '', '', '',
        		       '', '', '', '', '', '',
	        	       0, 0, 0, 'N', @Fecha, @Fecha, '', @Fecha, 0, 0 )
		END
	END

	RETURN
END
GO

/* SP_GET_NOMTIPO: Obtener Tipo de N�mina */
/* Patch 401 # Seq: 255 Agregado */
CREATE FUNCTION SP_GET_NOMTIPO( @Fecha DATETIME, @Empleado INTEGER ) returns NominaTipo as
begin
	declare @FechaKardex DATETIME;
	declare @NOMTIPO NominaTipo;
	select @NOMTIPO = CB_NOMINA, @FechaKardex = CB_FEC_NOM from COLABORA where ( CB_CODIGO = @Empleado );
	if ( @Fecha < @FechaKardex )
	begin
		select @NOMTIPO  = dbo.SP_KARDEX_CB_NOMINA( @Fecha, @Empleado );
    end
	return @NOMTIPO;
end
GO

/* SP_INVESTIGA_STATUS: Investigar status de n�mina del empleado */
/* Patch 401 # Seq: 227 Modificado */
ALTER FUNCTION SP_INVESTIGA_STATUS ( @Empleado INTEGER, @Fecha DATETIME ) RETURNS SMALLINT AS
BEGIN
	 declare @Status SMALLINT
	 declare @FechaTipNom DATETIME
	 declare @TipoNomina SmallInt

	 select @TipoNomina = CB_NOMINA, @FechaTipNom = CB_FEC_NOM from COLABORA where CB_CODIGO = @Empleado

	 if (@FechaTipNom > @Fecha)
		select @TipoNomina = dbo.SP_KARDEX_CB_NOMINA( @Fecha, @Empleado )

     select @Status = MAX(PE_STATUS) from PERIODO
     where PE_NUMERO < 200 and PE_ASI_INI <= @Fecha and PE_ASI_FIN >= @Fecha and PE_TIPO = @TipoNomina

     if @Status is NULL Select @Status = 0;
	 Return @Status
end
GO

/* AFECTA_EMPLEADO: Agregar nuevos tipos de n�mina */
/* Patch 401 # Seq: 151 Modificado */
ALTER PROCEDURE AFECTA_EMPLEADO
    			@EMPLEADO INTEGER,
    			@ANIO SMALLINT,
    			@TIPO SMALLINT,
    			@NUMERO SMALLINT,
    			@MES SMALLINT,
    			@FACTOR INTEGER
AS
BEGIN
     SET NOCOUNT ON
     DECLARE @CONCEPTO INTEGER;
     DECLARE @M1000 NUMERIC(15,2);
     DECLARE @M1001 NUMERIC(15,2);
     DECLARE @M1002 NUMERIC(15,2);
     DECLARE @M1003 NUMERIC(15,2);
     DECLARE @M1004 NUMERIC(15,2);
     DECLARE @M1005 NUMERIC(15,2);
     DECLARE @M1006 NUMERIC(15,2);
     DECLARE @M1007 NUMERIC(15,2);
     DECLARE @M1008 NUMERIC(15,2);
     DECLARE @M1009 NUMERIC(15,2);
     DECLARE @M1010 NUMERIC(15,2);
     DECLARE @M1011 NUMERIC(15,2);
     DECLARE @M1100 NUMERIC(15,2);
     DECLARE @M1101 NUMERIC(15,2);
     DECLARE @M1102 NUMERIC(15,2);
     DECLARE @M1103 NUMERIC(15,2);
     DECLARE @M1104 NUMERIC(15,2);
     DECLARE @M1105 NUMERIC(15,2);
     DECLARE @M1106 NUMERIC(15,2);
     DECLARE @M1107 NUMERIC(15,2);
     DECLARE @M1108 NUMERIC(15,2);
     DECLARE @M1109 NUMERIC(15,2);
     DECLARE @M1110 NUMERIC(15,2);
     DECLARE @M1111 NUMERIC(15,2);
     DECLARE @M1112 NUMERIC(15,2);
     DECLARE @M1113 NUMERIC(15,2);
     DECLARE @M1114 NUMERIC(15,2);
     DECLARE @M1115 NUMERIC(15,2);
     DECLARE @M1116 NUMERIC(15,2);
     DECLARE @M1117 NUMERIC(15,2);
     DECLARE @M1118 NUMERIC(15,2);
     DECLARE @M1119 NUMERIC(15,2);
     DECLARE @M1120 NUMERIC(15,2);
     DECLARE @M1121 NUMERIC(15,2);
     DECLARE @M1122 NUMERIC(15,2);
     DECLARE @M1123 NUMERIC(15,2);
     DECLARE @M1200 NUMERIC(15,2);
     DECLARE @M1201 NUMERIC(15,2);
     DECLARE @M1202 NUMERIC(15,2);
     DECLARE @M1203 NUMERIC(15,2);
     DECLARE @M1204 NUMERIC(15,2);
     DECLARE @M1205 NUMERIC(15,2);
     DECLARE @M1206 NUMERIC(15,2);
     DECLARE @M1207 NUMERIC(15,2);
     DECLARE @M1208 NUMERIC(15,2);
     DECLARE @M1209 NUMERIC(15,2);
     DECLARE @M1210 NUMERIC(15,2);
     DECLARE @M1211 NUMERIC(15,2);
     DECLARE @M1212 NUMERIC(15,2);
     DECLARE @M1213 NUMERIC(15,2);
     DECLARE @M1214 NUMERIC(15,2);
     DECLARE @M1215 NUMERIC(15,2);

     select @M1000=N.NO_PERCEPC,
            @M1001=N.NO_DEDUCCI,
            @M1002=N.NO_NETO,
            @M1003=N.NO_X_ISPT,
            @M1005=N.NO_PER_MEN,
            @M1006=N.NO_X_MENS,
            @M1007=N.NO_PER_CAL,
            @M1008=N.NO_IMP_CAL,
            @M1009=N.NO_X_CAL,
            @M1010=N.NO_TOT_PRE,
	          @M1011=N.NO_PREV_GR,
            @M1100=N.NO_DIAS_FI,
            @M1101=N.NO_DIAS_FJ,
            @M1102=N.NO_DIAS_CG,
            @M1103=N.NO_DIAS_SG,
            @M1104=N.NO_DIAS_SU,
            @M1105=N.NO_DIAS_OT,
            @M1106=N.NO_DIAS_IN,
            @M1107=N.NO_DIAS * 7 / 6,
            @M1108=N.NO_DIAS_RE,
            @M1109=N.NO_D_TURNO,
            @M1110=N.NO_DIAS,
            @M1111=N.NO_DIAS_AS,
            @M1112=N.NO_DIAS_NT,
            @M1113=N.NO_DIAS_FV,
            @M1114=N.NO_DIAS_VA,
            @M1115=N.NO_DIAS_AG,
            @M1116=N.NO_DIAS_AJ,
            @M1117=N.NO_DIAS_SS,
            @M1118=N.NO_DIAS_EM,
            @M1119=N.NO_DIAS_SI,
            @M1120=N.NO_DIAS_BA,
            @M1121=N.NO_DIAS_PV,
            @M1122=N.NO_DIAS_VJ,
            @M1123=N.NO_DIAS_VJ * 7 / 6,
            @M1200=N.NO_JORNADA,         
            @M1201=N.NO_HORAS,           
            @M1202=N.NO_EXTRAS,          
            @M1203=N.NO_DOBLES,
            @M1204=N.NO_TRIPLES,
            @M1205=N.NO_ADICION,
            @M1206=N.NO_TARDES,
            @M1207=N.NO_HORA_PD,
            @M1208=N.NO_HORA_CG,
            @M1209=N.NO_HORA_SG,
            @M1210=N.NO_FES_TRA,
            @M1211=N.NO_DES_TRA,
            @M1212=N.NO_VAC_TRA,
            @M1213=N.NO_FES_PAG,
            @M1214=N.NO_HORASNT,
            @M1215=N.NO_HORAPDT
            from NOMINA N where
            ( N.PE_YEAR = @Anio ) and
            ( N.PE_TIPO = @Tipo ) and
            ( N.PE_NUMERO = @Numero ) and
            ( N.CB_CODIGO = @Empleado )

     if ( @M1200 <> 0 )
     begin
          Select @M1200 = @M1201 / @M1200;
     end
     Select @M1109 = @M1200 * @M1109;
     if ( @M1000 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1000, @Mes, @Factor, @M1000;
     if ( @M1001 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1001, @Mes, @Factor, @M1001;
     if ( @M1002 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1002, @Mes, @Factor, @M1002;
     if ( @M1003 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1003, @Mes, @Factor, @M1003;
     if ( @M1005 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1005, @Mes, @Factor, @M1005;
     if ( @M1006 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1006, @Mes, @Factor, @M1006;
     if ( @M1007 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1007, @Mes, @Factor, @M1007;
     if ( @M1008 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1008, @Mes, @Factor, @M1008;
     if ( @M1009 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1009, @Mes, @Factor, @M1009;
     if ( @M1010 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1010, @Mes, @Factor, @M1010;
     if ( @M1011 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1011, @Mes, @Factor, @M1011;
     if ( @M1100 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1100, @Mes, @Factor, @M1100;
     if ( @M1101 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1101, @Mes, @Factor, @M1101;
     if ( @M1102 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1102, @Mes, @Factor, @M1102;
     if ( @M1103 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1103, @Mes, @Factor, @M1103;
     if ( @M1104 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1104, @Mes, @Factor, @M1104;
     if ( @M1105 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1105, @Mes, @Factor, @M1105;
     if ( @M1106 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1106, @Mes, @Factor, @M1106;
     if ( @M1107 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1107, @Mes, @Factor, @M1107;
     if ( @M1108 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1108, @Mes, @Factor, @M1108;
     if ( @M1109 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1109, @Mes, @Factor, @M1109;
     if ( @M1110 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1110, @Mes, @Factor, @M1110;
     if ( @M1111 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1111, @Mes, @Factor, @M1111;
     if ( @M1112 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1112, @Mes, @Factor, @M1112;
     if ( @M1113 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1113, @Mes, @Factor, @M1113;
     if ( @M1114 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1114, @Mes, @Factor, @M1114;
     if ( @M1115 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1115, @Mes, @Factor, @M1115;
     if ( @M1116 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1116, @Mes, @Factor, @M1116;
     if ( @M1117 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1117, @Mes, @Factor, @M1117;
     if ( @M1118 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1118, @Mes, @Factor, @M1118;
     if ( @M1119 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1119, @Mes, @Factor, @M1119;
     if ( @M1120 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1120, @Mes, @Factor, @M1120;
     if ( @M1121 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1121, @Mes, @Factor, @M1121;
     if ( @M1122 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1122, @Mes, @Factor, @M1122;
     if ( @M1123 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1123, @Mes, @Factor, @M1123;
     if ( @M1200 <> 0 )
     begin
          if ( @Tipo = 0 )
             Select @M1200 = 8 * @M1200;
          else
          if ( ( @Tipo = 1 ) or ( @Tipo = 6 ) or ( @Tipo = 7 ) )
             Select @M1200 = 48 * @M1200;
          else
          if ( ( @Tipo = 2 ) or ( @Tipo = 10 ) or ( @Tipo = 11 ) )
             Select @M1200 = 96 * @M1200;
          else
          if ( ( @Tipo = 3 ) or ( @Tipo = 8 ) or ( @Tipo = 9 ) )
             Select @M1200 = 104 * @M1200;
          else
          if ( @Tipo = 4 )
             Select @M1200 = 208 * @M1200;
          else
          if ( @Tipo = 5 )
             Select @M1200 = 72 * @M1200;
          EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1200, @Mes, @Factor, @M1200;
     end
     if ( @M1201 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1201, @Mes, @Factor, @M1201;
     if ( @M1202 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1202, @Mes, @Factor, @M1202;
     if ( @M1203 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1203, @Mes, @Factor, @M1203;
     if ( @M1204 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1204, @Mes, @Factor, @M1204;
     if ( @M1205 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1205, @Mes, @Factor, @M1205;
     if ( @M1206 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1206, @Mes, @Factor, @M1206;
     if ( @M1207 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1207, @Mes, @Factor, @M1207;
     if ( @M1208 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1208, @Mes, @Factor, @M1208;
     if ( @M1209 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1209, @Mes, @Factor, @M1209;
     if ( @M1210 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1210, @Mes, @Factor, @M1210;
     if ( @M1211 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1211, @Mes, @Factor, @M1211;
     if ( @M1212 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1212, @Mes, @Factor, @M1212;
     if ( @M1213 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1213, @Mes, @Factor, @M1213;
     if ( @M1214 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1214, @Mes, @Factor, @M1214;
     if ( @M1215 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1215, @Mes, @Factor, @M1215;

     Declare TemporalMovimien Cursor for
        select M.CO_NUMERO, SUM( M.MO_PERCEPC + M.MO_DEDUCCI )
        from MOVIMIEN M where
               ( M.MO_ACTIVO = 'S' ) and
               ( M.PE_YEAR = @Anio ) and
               ( M.PE_TIPO = @Tipo ) and
               ( M.PE_NUMERO = @Numero ) and
               ( M.CB_CODIGO = @Empleado )
        group by M.CO_NUMERO
     open TemporalMovimien
     Fetch next	from TemporalMovimien
     into  @Concepto, @M1000
     WHILE @@FETCH_STATUS = 0
     begin
          EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, @Concepto, @Mes, @Factor, @M1000;
     	  Fetch next from TemporalMovimien
     	  into  @Concepto, @M1000
     end

     CLOSE TemporalMovimien
     DEALLOCATE TemporalMovimien


     select @M1004 = sum( M.MO_PERCEPC )
     from MOVIMIEN M left outer join CONCEPTO C on C.CO_NUMERO=M.CO_NUMERO
     where
               ( M.MO_ACTIVO = 'S' ) and
               ( M.PE_YEAR = @Anio ) and
               ( M.PE_TIPO = @Tipo ) and
               ( M.PE_NUMERO = @Numero ) and
               ( M.CB_CODIGO = @Empleado ) and
               ( C.CO_TIPO = 1 ) and
               ( C.CO_A_PTU = 'S' )

     if ( @M1004 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1004, @Mes, @Factor, @M1004;
END
GO

/* RECALCULA_KARDEX: Agregar KARDEX.CB_NOMINA y COLABORA.CB_NOMINA */
/* Patch 401 # Seq: 165 Modificado */
ALTER PROCEDURE RECALCULA_KARDEX(@Empleado INTEGER) AS
BEGIN
   SET NOCOUNT ON
   declare @kAUTOSAL  CHAR(1);
   declare @kClasifi  CHAR(6);
   declare @kCONTRAT  CHAR(1);
   declare @kFAC_INT  NUMERIC(15,5);
   declare @kFEC_ANT  Fecha;
   declare @kFEC_CON  Fecha;
   declare @kFEC_ING  Fecha;
   declare @kFEC_INT  Fecha;
   declare @kFEC_REV  Fecha;
   declare @kFECHA_2  Fecha;
   declare @kFECHA    Fecha;
   declare @kMOT_BAJ  CHAR(3);
   declare @kNivel1   CHAR(6);
   declare @kNivel2   CHAR(6);
   declare @kNivel3   CHAR(6);
   declare @kNivel4   CHAR(6);
   declare @kNivel5   CHAR(6);
   declare @kNivel6   CHAR(6);
   declare @kNivel7   CHAR(6);
   declare @kNivel8   CHAR(6);
   declare @kNivel9   CHAR(6);
   declare @kNOMNUME  SMALLINT;
   declare @kNOMTIPO  SMALLINT;
   declare @kNOMYEAR  SMALLINT;
   declare @kPatron   CHAR(1);
   declare @kPER_VAR  NUMERIC(15,2);
   declare @kPuesto   CHAR(6);
   declare @kSAL_INT  NUMERIC(15,2);
   declare @kSALARIO  NUMERIC(15,2);
   declare @kTABLASS  CHAR(1);
   declare @kTurno    CHAR(6);
   declare @kZona_GE  CHAR(1);
   declare @kOLD_SAL  NUMERIC(15,2);
   declare @kOLD_INT  NUMERIC(15,2);
   declare @kOTRAS_P  NUMERIC(15,2);
   declare @kPRE_INT  NUMERIC(15,2);
   declare @kREINGRE  Char(1);
   declare @kSAL_TOT  NUMERIC(15,2);
   declare @kTOT_GRA  NUMERIC(15,2);
   declare @kTIPO     CHAR(6);
   declare @kRANGO_S  NUMERIC(15,2);
   declare @kMOD_NV1  CHAR(1);
   declare @kMOD_NV2  CHAR(1);
   declare @kMOD_NV3  CHAR(1);
   declare @kMOD_NV4  CHAR(1);
   declare @kMOD_NV5  CHAR(1);
   declare @kMOD_NV6  CHAR(1);
   declare @kMOD_NV7  CHAR(1);
   declare @kMOD_NV8  CHAR(1);
   declare @kMOD_NV9  CHAR(1);
   declare @kPLAZA    FolioGrande;
   declare @kNOMINA   SMALLINT;

   declare @eACTIVO   CHAR(1);
   declare @eAUTOSAL  CHAR(1);
   declare @eClasifi  CHAR(6);
   declare @eCONTRAT  CHAR(1);
   declare @eFAC_INT  NUMERIC(15,5);
   declare @eFEC_ANT  Fecha;
   declare @eFEC_CON  Fecha;
   declare @eFEC_ING  Fecha;
   declare @eFEC_INT  Fecha;
   declare @eFEC_REV  Fecha;
   declare @eFEC_SAL  Fecha;
   declare @eFEC_BAJ  Fecha;
   declare @eFEC_BSS  Fecha;
   declare @eMOT_BAJ  CHAR(3);
   declare @eNivel1   CHAR(6);
   declare @eNivel2   CHAR(6);
   declare @eNivel3   CHAR(6);
   declare @eNivel4   CHAR(6);
   declare @eNivel5   CHAR(6);
   declare @eNivel6   CHAR(6);
   declare @eNivel7   CHAR(6);
   declare @eNivel8   CHAR(6);
   declare @eNivel9   CHAR(6);
   declare @eNOMNUME  SMALLINT;
   declare @eNOMTIPO  SMALLINT;
   declare @eNOMYEAR  SMALLINT;
   declare @ePatron   CHAR(1);
   declare @ePER_VAR  NUMERIC(15,2);
   declare @ePuesto   CHAR(6);
   declare @eSAL_INT  NUMERIC(15,2);
   declare @eSALARIO  NUMERIC(15,2);
   declare @eTABLASS  CHAR(1);
   declare @eTIP_REV  CHAR(6);
   declare @eTurno    CHAR(6);
   declare @eZona_GE  CHAR(1);
   declare @eOLD_SAL  NUMERIC(15,2);
   declare @eOLD_INT  NUMERIC(15,2);
   declare @eOTRAS_P  NUMERIC(15,2);
   declare @ePRE_INT  NUMERIC(15,2);
   declare @eREINGRE  Char(1);
   declare @eSAL_TOT  NUMERIC(15,2);
   declare @eTOT_GRA  NUMERIC(15,2);
   declare @eRANGO_S  NUMERIC(15,2);
   declare @eFEC_NIV  Fecha;
   declare @eFEC_PTO  Fecha;
   declare @eFEC_TUR  Fecha;
   declare @FechaVacia Fecha;
   declare @eFEC_KAR  Fecha;
   declare @ePLAZA    FolioGrande;
   declare @eFEC_PLA  Fecha;
   declare @eNOMINA   SMALLINT;
   declare @eFEC_NOM  Fecha;
   declare CursorKardex CURSOR SCROLL_LOCKS FOR
   select	CB_AUTOSAL, CB_CLASIFI,   CB_CONTRAT, CB_FAC_INT, CB_FEC_ANT,
    CB_FEC_CON, CB_FEC_ING,   CB_FEC_INT, CB_FEC_REV, CB_FECHA,    CB_FECHA_2 ,
    CB_MOT_BAJ, CB_NIVEL1,    CB_NIVEL2 , CB_NIVEL3 , CB_NIVEL4,   CB_NIVEL5 ,   CB_NIVEL6 ,
    CB_NIVEL7 , CB_NIVEL8,    CB_NIVEL9 , CB_NOMNUME ,CB_NOMTIPO , CB_NOMYEAR ,
    CB_OTRAS_P, CB_PATRON ,   CB_PER_VAR ,CB_PRE_INT ,
    CB_PUESTO , CB_REINGRE ,  CB_SAL_INT ,CB_SAL_TOT, CB_SALARIO,  CB_TABLASS ,
    CB_TIPO ,   CB_TOT_GRA,   CB_TURNO ,  CB_ZONA_GE, CB_RANGO_S, CB_PLAZA,
    CB_NOMINA
   from Kardex
   where CB_CODIGO = @Empleado
   order by cb_fecha,cb_nivel

      	SET @FechaVacia = '12/30/1899';
	      SET @eACTIVO  = 'S';
      	SET @eFEC_BAJ = @FechaVacia;
      	SET @eFEC_BSS = @FechaVacia;
      	SET @eMOT_BAJ = ' ';
      	SET @eNOMYEAR = 0;
      	SET @eNOMTIPO = 0;
      	SET @eNOMNUME = 0;
      	SET @eREINGRE = 'N';
      	SET @eSALARIO = 0;
      	SET @eSAL_INT = 0;
      	SET @eFAC_INT = 0;
      	SET @ePRE_INT = 0;
      	SET @ePER_VAR = 0;
      	SET @eSAL_TOT = 0;
      	SET @eAUTOSAL = 'N';
      	SET @eTOT_GRA = 0;
      	SET @eFEC_REV = @FechaVacia;
      	SET @eFEC_INT = @FechaVacia;
      	SET @eOTRAS_P = 0;
      	SET @eOLD_SAL = 0;
      	SET @eOLD_INT = 0;
      	SET @eRANGO_S = 0;
        SET @eFEC_PLA = @FechaVacia;
        SET @eFEC_NOM = @FechaVacia;

	Open CursorKardex
	Fetch NEXT from CursorKardex
	into	@kAUTOSAL, @kCLASIFI, @kCONTRAT,  @kFAC_INT,  @kFEC_ANT ,
      @kFEC_CON, @kFEC_ING, @kFEC_INT , @kFEC_REV  ,@kFECHA,   @kFECHA_2,
      @kMOT_BAJ ,@kNivel1,  @kNivel2 ,  @kNivel3,   @kNivel4,  @kNivel5,  @kNivel6,
      @kNivel7,  @kNivel8,  @kNivel9,   @kNOMNUME , @kNOMTIPO, @kNOMYEAR,
      @kOTRAS_P, @kPatron,  @kPER_VAR,  @kPRE_INT,
      @kPuesto,  @kREINGRE, @kSAL_INT,  @kSAL_TOT,  @kSALARIO, @kTABLASS,
      @kTIPO,    @kTOT_GRA, @kTurno,    @kZONA_GE,  @kRANGO_S, @kPLAZA,
      @kNOMINA


	if ( @@FETCH_STATUS <> 0 )
	BEGIN
		Close CursorKardex
		Deallocate CursorKardex
		RETURN
	END

	
	if ( @kTIPO <> 'ALTA' )
	BEGIN
		Close CursorKardex
		Deallocate CursorKardex
		RAISERROR( 'Empleado #%d, ERROR en KARDEX: Movimiento(s) previo(s) a la ALTA', 16, 1, @Empleado )
		RETURN
	END

	while ( @@FETCH_STATUS = 0 )
	BEGIN
        	if ( @kTIPO = 'ALTA' OR @kTIPO = 'CAMBIO' OR @kTIPO = 'CIERRE' )
          	BEGIN
			if ABS( @kSALARIO - @eSALARIO ) >= 0.01
			BEGIN
				SET @kOLD_SAL = @eSALARIO;
				SET @kFEC_REV = @kFECHA;
				SET @eFEC_REV = @kFEC_REV;
				SET @eSALARIO = @kSALARIO;
				SET @eOLD_SAL = @kOLD_SAL;
			END
			else
			BEGIN
				SET @kOLD_SAL = @eOLD_SAL;
				SET @kFEC_REV = @eFEC_REV;
			END
			if ABS( @kSAL_INT - @eSAL_INT ) >= 0.01
			BEGIN
				SET @kOLD_INT = @eSAL_INT;
				if ( @kTIPO = 'CAMBIO' )
					SET @kFEC_INT = @kFECHA_2;
				else
					SET @kFEC_INT = @kFECHA;
				SET @eFEC_INT = @kFEC_INT;
				SET @eSAL_INT = @kSAL_INT;
				SET @eOLD_INT = @kOLD_INT;
			END
			else
			BEGIN
        SET @kOLD_INT = @eOLD_INT;
	       SET @kFEC_INT = @eFEC_INT;
			END
			SET @eFAC_INT = @kFAC_INT;
			SET @ePRE_INT = @kPRE_INT;
			SET @ePER_VAR = @kPER_VAR;
			SET @eSAL_TOT = @kSAL_TOT;
			SET @eTOT_GRA = @kTOT_GRA;
			SET @eOTRAS_P = @kOTRAS_P;
			SET @eZONA_GE = @kZONA_GE;
			SET @eTIP_REV = @kTIPO;
			SET @eFEC_SAL = @kFECHA;
			SET @eRANGO_S = @kRANGO_S;
		END
		else
		BEGIN
			SET @kSALARIO = @eSALARIO;
			SET @kOLD_SAL = @eOLD_SAL;
			SET @kFEC_REV = @eFEC_REV;
			SET @kSAL_INT = @eSAL_INT;
			SET @kOLD_INT = @eOLD_INT;
			SET @kFEC_INT = @eFEC_INT;
			SET @kFAC_INT = @eFAC_INT;
			SET @kPRE_INT = @ePRE_INT;
			SET @kPER_VAR = @ePER_VAR;
			SET @kSAL_TOT = @eSAL_TOT;
			SET @kTOT_GRA = @eTOT_GRA;
			SET @kOTRAS_P = @eOTRAS_P;
			SET @kZONA_GE = @eZONA_GE;
			SET @kRANGO_S = @eRANGO_S;
		END

		if ( @kTIPO = 'ALTA' or @kTIPO = 'PUESTO' or @kTIPO = 'CAMBIO' or @kTIPO = 'CIERRE' )
			SET @eAUTOSAL = @kAUTOSAL;
		else
			SET @kAUTOSAL = @eAUTOSAL;

		if ( @kAUTOSAL <> 'S' )
		BEGIN
			SET @kRANGO_S = 0;
			SET @eRANGO_S = 0;
		END

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'PUESTO' OR @kTIPO = 'CIERRE' OR @kTIPO = 'PLAZA' )
		BEGIN
			SET @ePUESTO   = @kPUESTO;
			SET @eFEC_PTO  = @kFECHA;
                        if ( @kTIPO = 'PLAZA' AND @eAUTOSAL = 'S' )
                             SET @kCLASIFI = @eCLASIFI;
                        else
			     SET @eCLASIFI  = @kCLASIFI;
		END
		else
		BEGIN
			SET @kPUESTO   = @ePUESTO;
			SET @kCLASIFI  = @eCLASIFI;
		END

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'AREA' OR @kTIPO = 'CIERRE' OR @kTIPO = 'PLAZA' )
		BEGIN
			if ( ( @kTIPO = 'AREA' ) OR ( @kTIPO = 'CIERRE' ) OR ( @kTIPO = 'PLAZA' ))
			BEGIN
				if ( @kNIVEL1 <> @eNIVEL1 )
     					SET @kMOD_NV1 = 'S';
            			else
                 			SET @kMOD_NV1 = 'N';
				if ( @kNIVEL2 <> @eNIVEL2 )
     					SET @kMOD_NV2 = 'S';
            			else
                 			SET @kMOD_NV2 = 'N';
				if ( @kNIVEL3 <> @eNIVEL3 )
     					SET @kMOD_NV3 = 'S';
            			else
                 			SET @kMOD_NV3 = 'N';
				if ( @kNIVEL4 <> @eNIVEL4 )
     					SET @kMOD_NV4 = 'S';
            			else
                 			SET @kMOD_NV4 = 'N';
				if ( @kNIVEL5 <> @eNIVEL5 )
     					SET @kMOD_NV5 = 'S';
            			else
                 			SET @kMOD_NV5 = 'N';
				if ( @kNIVEL6 <> @eNIVEL6 )
     					SET @kMOD_NV6 = 'S';
            			else
                 			SET @kMOD_NV6 = 'N';
				if ( @kNIVEL7 <> @eNIVEL7 )
     					SET @kMOD_NV7 = 'S';
            			else
                 			SET @kMOD_NV7 = 'N';
				if ( @kNIVEL8 <> @eNIVEL8 )
     					SET @kMOD_NV8 = 'S';
            			else
                 			SET @kMOD_NV8 = 'N';
				if ( @kNIVEL9 <> @eNIVEL9 )
     					SET @kMOD_NV9 = 'S';
            			else
                 			SET @kMOD_NV9 = 'N';
			END
			else
			BEGIN
                 if ( @kNIVEL1 <> '' )
                     SET @kMOD_NV1 = 'S';
                 else
                     SET @kMOD_NV1 = 'N';
                 if ( @kNIVEL2 <> '' )
                     SET @kMOD_NV2 = 'S';
                 else
                     SET @kMOD_NV2 = 'N';
                 if ( @kNIVEL3 <> '' )
                     SET @kMOD_NV3 = 'S';
                 else
                     SET @kMOD_NV3 = 'N';
                 if ( @kNIVEL4 <> '' )
                     SET @kMOD_NV4 = 'S';
                 else
                     SET @kMOD_NV4 = 'N';
                 if ( @kNIVEL5 <> '' )
                     SET @kMOD_NV5 = 'S';
                 else
                     SET @kMOD_NV5 = 'N';
                 if ( @kNIVEL6 <> '' )
                     SET @kMOD_NV6 = 'S';
                 else
                     SET @kMOD_NV6 = 'N';
                 if ( @kNIVEL7 <> '' )
                     SET @kMOD_NV7 = 'S';
                 else
                     SET @kMOD_NV7 = 'N';
                 if ( @kNIVEL8 <> '' )
                     SET @kMOD_NV8 = 'S';
                 else
                     SET @kMOD_NV8 = 'N';
                 if ( @kNIVEL9 <> '' )
                     SET @kMOD_NV9 = 'S';
                 else
                     SET @kMOD_NV9 = 'N';

			END
			SET @eNIVEL1 = @kNIVEL1;
			SET @eNIVEL2 = @kNIVEL2;
			SET @eNIVEL3 = @kNIVEL3;
			SET @eNIVEL4 = @kNIVEL4;
			SET @eNIVEL5 = @kNIVEL5;
			SET @eNIVEL6 = @kNIVEL6;
			SET @eNIVEL7 = @kNIVEL7;
			SET @eNIVEL8 = @kNIVEL8;
			SET @eNIVEL9 = @kNIVEL9;
			SET @eFEC_NIV = @kFECHA;
		END
		else
		BEGIN
			SET @kNIVEL1 = @eNIVEL1;
			SET @kNIVEL2 = @eNIVEL2;
			SET @kNIVEL3 = @eNIVEL3;
			SET @kNIVEL4 = @eNIVEL4;
			SET @kNIVEL5 = @eNIVEL5;
			SET @kNIVEL6 = @eNIVEL6;
			SET @kNIVEL7 = @eNIVEL7;
			SET @kNIVEL8 = @eNIVEL8;
			SET @kNIVEL9 = @eNIVEL9;
            SET @kMOD_NV1 = 'N';
            SET @kMOD_NV2 = 'N';
            SET @kMOD_NV3 = 'N';
            SET @kMOD_NV4 = 'N';
            SET @kMOD_NV5 = 'N';
            SET @kMOD_NV6 = 'N';
            SET @kMOD_NV7 = 'N';
            SET @kMOD_NV8 = 'N';
            SET @kMOD_NV9 = 'N';
		END

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'PRESTA' OR @kTIPO = 'CIERRE' )
			SET @eTABLASS = @kTABLASS;
		else
			SET @kTABLASS = @eTABLASS;

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'TURNO' OR @kTIPO = 'CIERRE' OR @kTIPO = 'PLAZA' )
		BEGIN
			SET @eTURNO   = @kTURNO;
			SET @eFEC_TUR = @kFECHA;
		END
		else
			SET @kTURNO   = @eTURNO;

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'TIPNOM' OR @kTIPO = 'CIERRE' )
		BEGIN
			SET @eNOMINA  = @kNOMINA;
			SET @eFEC_NOM = @kFECHA;
		END
		else
			SET @kNOMINA  = @eNOMINA;

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'RENOVA' OR @kTIPO = 'CIERRE' )
		BEGIN
			SET @eCONTRAT = @kCONTRAT;
			SET @eFEC_CON = @kFEC_CON;
		END
		else
		BEGIN
			SET @kCONTRAT = @eCONTRAT;
			SET @kFEC_CON = @eFEC_CON;
		END

		if ( @kTIPO = 'ALTA' )
		BEGIN
			if ( @eACTIVO = 'S' )
				SET @kREINGRE = 'N';
            		else
			BEGIN
				SET @kREINGRE = 'S';
				SET @eREINGRE = 'S';
			END

			SET @eFEC_ING = @kFECHA;
			SET @eFEC_ANT = @kFEC_ANT;
			SET @eACTIVO  = 'S';
			SET @ePATRON  = @kPATRON;
			SET @kFEC_ING = @eFEC_ING;
			SET @kFEC_ANT = @eFEC_ANT;
		END
		else
		BEGIN
			SET @kREINGRE = @eREINGRE;
			SET @kPATRON  = @ePATRON;
			SET @kFEC_ING = @eFEC_ING;
			SET @kFEC_ANT = @eFEC_ANT;
		END

		if ( @kTIPO = 'BAJA' )
		BEGIN
			SET @eFEC_BAJ  =  @kFECHA;
			SET @eFEC_BSS  =  @kFECHA_2;
			SET @eMOT_BAJ  =  @kMOT_BAJ;
			SET @eACTIVO   =  'N';
			SET @eNOMYEAR  =  @kNOMYEAR;
			SET @eNOMTIPO  =  @kNOMTIPO;
			SET @eNOMNUME  =  @kNOMNUME;
			SET @ePLAZA    =  0;
		END
		else
		BEGIN
			SET @kMOT_BAJ = @eMOT_BAJ;
			SET @kNOMYEAR = @eNOMYEAR;
			SET @kNOMTIPO = @eNOMTIPO;
			SET @kNOMNUME = @eNOMNUME;
		END


		if ( @kTIPO = 'ALTA'  OR @kTIPO = 'CIERRE' OR @kTIPO = 'PLAZA' )
		BEGIN
			SET @ePLAZA    = @kPLAZA;
			SET @eFEC_PLA  = @kFECHA;
		END
		else
		BEGIN
			SET @kPLAZA    = @ePLAZA;
		END

		update KARDEX
    set 	CB_AUTOSAL = @kAUTOSAL,
			CB_CLASIFI = @kCLASIFI,
			CB_CONTRAT = @kCONTRAT,
			CB_FEC_ANT = @kFEC_ANT,
			CB_FEC_CON = @kFEC_CON,
			CB_FEC_ING = @kFEC_ING,
			CB_FEC_INT = @kFEC_INT,
			CB_FEC_REV = @kFEC_REV,
			CB_FECHA_2 = @kFECHA_2,
			CB_MOT_BAJ = @kMOT_BAJ,
			CB_NIVEL1  = @kNIVEL1,
			CB_NIVEL2  = @kNIVEL2,
			CB_NIVEL3  = @kNIVEL3,
			CB_NIVEL4  = @kNIVEL4,
			CB_NIVEL5  = @kNIVEL5,
			CB_NIVEL6  = @kNIVEL6,
			CB_NIVEL7  = @kNIVEL7,
			CB_NIVEL8  = @kNIVEL8,
			CB_NIVEL9  = @kNIVEL9,
			CB_MOD_NV1 = @kMOD_NV1,
			CB_MOD_NV2 = @kMOD_NV2,
			CB_MOD_NV3 = @kMOD_NV3,
			CB_MOD_NV4 = @kMOD_NV4,
			CB_MOD_NV5 = @kMOD_NV5,
			CB_MOD_NV6 = @kMOD_NV6,
			CB_MOD_NV7 = @kMOD_NV7,
			CB_MOD_NV8 = @kMOD_NV8,
			CB_MOD_NV9 = @kMOD_NV9,
			CB_NOMNUME = @kNOMNUME,
			CB_NOMTIPO = @kNOMTIPO,
			CB_NOMYEAR = @kNOMYEAR,
			CB_OTRAS_P = @kOTRAS_P,
			CB_PATRON  = @kPATRON,
			CB_PER_VAR = @kPER_VAR,
			CB_PRE_INT = @kPRE_INT,
			CB_PUESTO  = @kPUESTO,
			CB_REINGRE = @kREINGRE,
			CB_SAL_INT = @kSAL_INT,
			CB_SAL_TOT = @kSAL_TOT,
			CB_SALARIO = @kSALARIO,
			CB_TABLASS = @kTABLASS,
			CB_TOT_GRA = @kTOT_GRA,
			CB_TURNO   = @kTURNO,
			CB_ZONA_GE = @kZONA_GE,
			CB_OLD_SAL = @kOLD_SAL,
			CB_OLD_INT = @kOLD_INT,
			CB_FAC_INT = @kFAC_INT,
			CB_RANGO_S = @kRANGO_S,
      CB_PLAZA   = @kPLAZA,
      CB_NOMINA  = @kNOMINA
		where CURRENT OF CursorKardex


		Fetch NEXT from CursorKardex
		into	@kAUTOSAL, @kCLASIFI, @kCONTRAT,  @kFAC_INT,  @kFEC_ANT ,
       @kFEC_CON, @kFEC_ING, @kFEC_INT , @kFEC_REV  ,@kFECHA,   @kFECHA_2,
       @kMOT_BAJ ,@kNivel1,  @kNivel2 ,  @kNivel3,   @kNivel4,  @kNivel5,  @kNivel6,
       @kNivel7,  @kNivel8,  @kNivel9,   @kNOMNUME , @kNOMTIPO, @kNOMYEAR,
       @kOTRAS_P, @kPatron,  @kPER_VAR,  @kPRE_INT,
       @kPuesto,  @kREINGRE, @kSAL_INT,  @kSAL_TOT,  @kSALARIO, @kTABLASS,
       @kTIPO,    @kTOT_GRA, @kTurno,    @kZONA_GE,  @kRANGO_S, @kPLAZA,
       @kNOMINA
	END

	Close CursorKardex
	Deallocate CursorKardex

	
	SET @eFEC_KAR = @eFEC_ING;
	if ( @eFEC_NIV > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_NIV;
	if ( @eFEC_SAL > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_SAL;
	if ( @eFEC_PTO > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_PTO;
	if ( @eFEC_TUR > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_TUR;
	if ( @eFEC_PLA > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_PLA;

	update COLABORA
	set CB_AUTOSAL = @eAUTOSAL,
		CB_ACTIVO  = @eACTIVO,
		CB_CLASIFI = @eCLASIFI,
		CB_CONTRAT = @eCONTRAT,
		CB_FAC_INT = @eFAC_INT,
		CB_FEC_ANT = @eFEC_ANT,
		CB_FEC_CON = @eFEC_CON,
		CB_FEC_ING = @eFEC_ING,
		CB_FEC_INT = @eFEC_INT,
		CB_FEC_REV = @eFEC_REV,
		CB_FEC_SAL = @eFEC_SAL,
		CB_FEC_BAJ = @eFEC_BAJ,
		CB_FEC_BSS = @eFEC_BSS,
		CB_MOT_BAJ = @eMOT_BAJ,
		CB_NIVEL1  = @eNIVEL1,
		CB_NIVEL2  = @eNIVEL2,
		CB_NIVEL3  = @eNIVEL3,
		CB_NIVEL4  = @eNIVEL4,
		CB_NIVEL5  = @eNIVEL5,
		CB_NIVEL6  = @eNIVEL6,
		CB_NIVEL7  = @eNIVEL7,
    CB_NIVEL8  = @eNIVEL8,
		CB_NIVEL9  = @eNIVEL9,
		CB_NOMNUME = @eNOMNUME,
		CB_NOMTIPO = @eNOMTIPO,
		CB_NOMYEAR = @eNOMYEAR,
		CB_PATRON  = @ePATRON,
		CB_PER_VAR = @ePER_VAR,
		CB_PUESTO  = @ePUESTO,
		CB_SAL_INT = @eSAL_INT,
		CB_SALARIO = @eSALARIO,
		CB_TABLASS = @eTABLASS,
		CB_TURNO   = @eTURNO,
		CB_ZONA_GE = @eZONA_GE,
		CB_OLD_SAL = @eOLD_SAL,
		CB_OLD_INT = @eOLD_INT,
		CB_PRE_INT = @ePRE_INT,
		CB_SAL_TOT = @eSAL_TOT,
		CB_TOT_GRA = @eTOT_GRA,
		CB_TIP_REV = @eTIP_REV,
		CB_RANGO_S = @eRANGO_S,
		CB_FEC_NIV = @eFEC_NIV,
		CB_FEC_PTO = @eFEC_PTO,
		CB_FEC_TUR = @eFEC_TUR,
		CB_FEC_KAR = @eFEC_KAR,
    CB_PLAZA   = @ePLAZA,
    CB_FEC_PLA = @eFEC_PLA,
    CB_NOMINA  = @eNOMINA,
    CB_FEC_NOM = @eFEC_NOM
	where ( CB_CODIGO = @Empleado );
END
GO

/****************************************/
/***** Actualizar versi�n de Datos ******/
/****************************************/

update GLOBAL set GL_FORMULA = '401' where ( GL_CODIGO = 133 )
GO
