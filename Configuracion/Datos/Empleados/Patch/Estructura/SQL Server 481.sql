/****************************************/
/****************************************/
/****** Patch para DATOS ****************/
/****************************************/
/****************************************/


/*SYNC*/
GO 


IF not exists (SELECT * FROM systypes where name like N'FechaWFN')
begin
                exec sp_addtype FechaWFN, 'datetime', 'NOT NULL'
                exec sp_bindefault Cero, FechaWFN
end
go

IF not exists (SELECT * FROM systypes where name like N'BooleanoWFN')
begin
                exec sp_addtype BooleanoWFN, 'bit', 'NOT NULL'
                exec sp_bindefault Cero, BooleanoWFN
end
go

GO 
IF ( not EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE  TABLE_NAME = 'T_WFTablas')) 
begin
	CREATE TABLE T_WFTablas  (
		TablaID FolioGrande,
		TablaNombre Observaciones,
		TablaOrden FolioChico,
		TablaActivo BooleanoWFN,
		TablaInsertGet varchar(max),
		TablaInsertPut Formula,
		TablaInsertLast FechaWFN,
		TablaUpdateGet varchar(max),
		TablaUpdatePut Formula,
		TablaUpdateLast FechaWFN,
		TablaRango		Status, 
		TablaRangoIni	Dias,
		TablaRangoFin	Dias, 
		TablaGrupo		FolioGrande,
	CONSTRAINT PK_WFTablas PRIMARY KEY CLUSTERED 
	(
		TablaID ASC
	)
	) 
end

go 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTabla_SetLastUpdate') AND type IN ( N'P', N'PC' ))
	drop procedure WFTabla_SetLastUpdate
go
CREATE PROCEDURE WFTabla_SetLastUpdate(
	@TablaID	INT,
	@Last		DATETIME )
AS
BEGIN
	UPDATE	T_WFTablas
	SET		TablaUpdateLast	= @Last
	WHERE	TablaID = @TablaID
END
GO
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTabla_SetLastInsert') AND type IN ( N'P', N'PC' ))
	drop procedure WFTabla_SetLastInsert
go
CREATE PROCEDURE WFTabla_SetLastInsert(
	@TablaID	INT,
	@Last		DATETIME )
AS
BEGIN
	UPDATE	T_WFTablas
	SET		TablaInsertLast	= @Last
	WHERE	TablaID = @TablaID
END
GO
if exists (select * from sysobjects where id = object_id(N'PE_INSERTAR_BITACORA') and OBJECTPROPERTY(id, N'IsProcedure') = 1) 
 drop procedure PE_INSERTAR_BITACORA 
GO 
CREATE PROCEDURE PE_INSERTAR_BITACORA
 ( 
        @US_CODIGO Usuario, 
	@BI_PROC_ID Status, 
	@BI_TIPO Status,
        @BI_NUMERO FolioGrande, 
	@BI_TEXTO Observaciones, 
	@CB_CODIGO NumeroEmpleado, 
        @BI_DATA Memo, 
	@BI_CLASE Status, 
	@BI_FEC_MOV Fecha 
) 
AS 
BEGIN 
	DECLARE @BI_FECHA Fecha
	DECLARE @BI_HORA HoraMinutosSegundos
	SET @BI_FECHA = getdate()
	SET @BI_HORA = SUBSTRING( CONVERT(CHAR(24),@BI_FECHA,113), 13,8 ) 
	SET @BI_FECHA = CONVERT(CHAR(10),@BI_FECHA,110) 

INSERT INTO BITACORA (US_CODIGO, BI_FECHA, BI_HORA, BI_PROC_ID, BI_TIPO, 
         BI_NUMERO, BI_TEXTO, CB_CODIGO, BI_DATA, BI_CLASE, BI_FEC_MOV)
VALUES ( @US_CODIGO, @BI_FECHA, @BI_HORA, @BI_PROC_ID, @BI_TIPO, @BI_NUMERO, @BI_TEXTO, @CB_CODIGO,
        @BI_DATA, @BI_CLASE, @BI_FEC_MOV ) 
END
GO


IF NOT EXISTS (SELECT * FROM syscolumns WHERE id=object_id('T_WFTablas') AND name='TablaRango'	) 
	alter table T_WFTablas add TablaRango		Status		 null 
go 
	update T_WFTablas set TablaRango = 0  where TablaRango is null 
go 
	alter table T_WFTablas alter column TablaRango Status not null 
go 
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id=object_id('T_WFTablas') AND name='TablaRangoIni'	) 
	alter table T_WFTablas add TablaRangoIni	Dias		 null 
go 
	update T_WFTablas set TablaRangoIni = 0  where TablaRangoIni is null 
go 
	alter table T_WFTablas alter column TablaRangoIni Dias not null 

go 
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id=object_id('T_WFTablas') AND name='TablaRangoFin'	) 
	alter table T_WFTablas add TablaRangoFin	Dias		 null 
go 
	update T_WFTablas set TablaRangoFin = 0  where TablaRangoFin is null 
go 
	alter table T_WFTablas alter column TablaRangoFin Dias not null 

go 
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id=object_id('T_WFTablas') AND name='TablaGrupo'	) 
	alter table T_WFTablas add TablaGrupo		FolioGrande	 null 
go 
	update T_WFTablas set TablaGrupo = 0  where TablaGrupo is null 
go 
	alter table T_WFTablas alter column TablaGrupo FolioGrande not null 
go 


-- Se cambia por GetRows
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTabla_GetRegistros') AND type IN ( N'P', N'PC' ))
	drop procedure WFTabla_GetRegistros
go

-- Se cambia por GetRows
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTabla_GetUpdate') AND type IN ( N'P', N'PC' ))
	drop procedure WFTabla_ForzarUpdate
go

-- No se usaba 
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTabla_GetInsert') AND type IN ( N'P', N'PC' ))
	drop procedure WFTabla_GetInsert
go

-- No se usaba 
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTabla_GetUpdate') AND type IN ( N'P', N'PC' ))
	drop procedure WFTabla_GetUpdate
go

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTabla_Consulta') AND type IN ( N'P', N'PC' ))
	drop procedure WFTabla_Consulta
go

CREATE PROCEDURE WFTabla_Consulta(
       @Grupo        FolioGrande = 0,
       @TablaID      FolioGrande = 0 )
AS
BEGIN
       SET NOCOUNT ON
       SELECT TablaID, TablaNombre, TablaInsertGet, TablaInsertPut, TablaInsertLast, TablaUpdateGet, TablaUpdatePut, TablaUpdateLast
       FROM   T_WFTablas
       WHERE  TablaActivo = 'TRUE' AND ( @TablaID = 0 AND TablaGrupo = @Grupo OR TablaID = @TablaID )
       ORDER BY TablaOrden, TablaID
END
go 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'BORRAR_INDICES_LLAVE') AND type IN ( N'P', N'PC' ))
	drop procedure BORRAR_INDICES_LLAVE

go

create procedure BORRAR_INDICES_LLAVE ( @tabla Formula ) 
as
begin 

	declare @qry nvarchar(max);
	select @qry = (
	select 
	  'IF EXISTS(SELECT * FROM sys.indexes WHERE name='''+ i.name +''' AND object_id = OBJECT_ID(''['+s.name+'].['+o.name+']''))      drop index ['+i.name+'] ON ['+s.name+'].['+o.name+'];  '
	from sys.indexes i 
			inner join sys.objects o on  i.object_id=o.object_id
			inner join sys.schemas s on o.schema_id = s.schema_id
			inner join sys.index_columns ic on ic.object_id = i.object_id  and ic.index_id = i.index_id
			inner join sys.columns c on c.object_id =  i.object_id  and c.column_id = ic.column_id
		where o.type<>'S' and is_primary_key<>1 and i.index_id>0
		and s.name!='sys' and s.name!='sys' and is_unique_constraint=0 and o.name = @tabla and c.name = 'LLAVE' 
		for xml path('')) ;
	
		exec sp_executesql @qry

end 
go 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'REGENERAR_LLAVE') AND type IN ( N'P', N'PC' ))
	drop procedure REGENERAR_LLAVE
go
create procedure REGENERAR_LLAVE ( @tabla Formula ) 
as
begin 
	declare @campo nvarchar(100)
	declare @sql nvarchar(1024)

	set @campo = @tabla +'.LLAVE'

	exec BORRAR_INDICES_LLAVE @tabla 

	if ( select count(*) from Syscolumns  where id = OBJECT_ID( @tabla ) and name = 'LLAVE' and cdefault > 0  ) > 0 
		   exec sp_unbindefault @campo

	if ( select count(*) from Syscolumns  where id = OBJECT_ID( @tabla ) and name = 'LLAVE' ) > 0 
	begin 
		   set @sql = 'alter table '+@tabla+' drop column LLAVE'
		   EXECUTE  sp_executesql  @sql 
	end 

	set @sql = 'alter table '+@tabla+' add LLAVE int identity(1,1) '
	EXECUTE  sp_executesql  @sql 

end 
go


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTabla_PreparaCampoLlave') AND type IN ( N'P', N'PC' ))
	drop procedure WFTabla_PreparaCampoLlave
go
create procedure WFTabla_PreparaCampoLlave ( @tabla Formula ) 
as
begin 

	DECLARE @SQLString  NVARCHAR( MAX )  
	declare @FolioGrandeType int 
	declare @intType int 
	select @FolioGrandeType = user_type_id  from sys.types where name='FolioGrande' 
	select @intType = user_type_id  from sys.types where name='int' 


    if ( left(@tabla, 3) = 'S3_' ) 
		set @tabla = replace( @tabla, 'S3_', '' ) 



	IF ((SELECT OBJECTPROPERTY( OBJECT_ID(@Tabla), 'TableHasIdentity')) = 1) and 
		   (( select count(*) from Syscolumns  where id = OBJECT_ID( @tabla ) and name = 'LLAVE' and xusertype <> @intType ))>0 
	begin 		
		 SET @SQLString =  
			N'IF  EXISTS (SELECT * FROM sysobjects WHERE xtype = ''UQ'' AND parent_obj=object_id(''' + @tabla + ''') AND name = ''SYNC_AK_'+@tabla+'_LLAVE'' ) 
					ALTER TABLE '+@tabla+' DROP CONSTRAINT SYNC_AK_'+@tabla+'_LLAVE' 
		
		EXECUTE sp_executesql @SQLString 
		
		exec REGENERAR_LLAVE @Tabla  
	end 

end 
go 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTabla_GetRows') AND type IN ( N'P', N'PC' ))
	drop procedure WFTabla_GetRows
go
CREATE PROCEDURE WFTabla_GetRows(
	@TablaID	INT,
	@SincTime	DATETIME,
	@Cual		SMALLINT,
	@Forzar		SMALLINT )
AS
BEGIN
	DECLARE	@Query		NVARCHAR( MAX )
	DECLARE @Params		NVARCHAR( MAX )
	DECLARE @LastSinc	DATETIME
	DECLARE @Rango		SMALLINT
	DECLARE @RangoIni	SMALLINT
	DECLARE @RangoFin	SMALLINT
	DECLARE @Inicial	DATETIME
	DECLARE @Final		DATETIME
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT	@Query		= CASE WHEN @Cual = 1 THEN TablaInsertGet ELSE TablaUpdateGet END,
			@LastSinc	= CASE WHEN @Forzar = 1 THEN '1899-12-30' WHEN @Cual = 1 THEN TablaInsertLast ELSE TablaUpdateLast END,
			@Rango		= TablaRango,
			@RangoIni	= TablaRangoIni,
			@RangoFin	= TablaRangoFin
	FROM	T_WFTablas
	WHERE	TablaID = @TablaID
	
	-- TablaRango: 0=Sin rango, 1=Segundos, 2=Minutos, 3=Horas, 4=Dias
	IF ( @Rango = 0 )
	BEGIN
		SET @Inicial	= 0
		SET @Final		= @SincTime
	END
	ELSE IF ( @Rango = 1 )
	BEGIN
		SET @Inicial	= DATEADD( ss, @RangoIni, @SincTime )
		SET @Final		= DATEADD( ss, @RangoFin, @SincTime )
	END
	ELSE IF ( @Rango = 2 )
	BEGIN
		SET @Inicial	= DATEADD( mi, @RangoIni, @SincTime )
		SET @Final		= DATEADD( mi, @RangoFin, @SincTime )
	END
	ELSE IF ( @Rango = 3 )
	BEGIN
		SET @Inicial	= DATEADD( hh, @RangoIni, @SincTime )
		SET @Final		= DATEADD( hh, @RangoFin, @SincTime )
	END
	ELSE IF ( @Rango = 4 )
	BEGIN
		SET @Inicial	= DATEADD( dd, @RangoIni, FLOOR( CAST( @SincTime AS FLOAT )))	-- Elimina hora, solo fecha
		SET @Final		= DATEADD( dd, @RangoFin, FLOOR( CAST( @SincTime AS FLOAT )))
	END

	SET @Params	= N'@LastSinc DATETIME, @SincTime DATETIME, @Inicial DATETIME, @Final DATETIME'
	EXEC sp_executesql @Query, @Params, @LastSinc, @SincTime, @Inicial, @Final

END

go 



IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTabla_PreparaTriggers') AND type IN ( N'P', N'PC' ))
	drop procedure WFTabla_PreparaTriggers

go 

CREATE PROCEDURE WFTabla_PreparaTriggers(  
 @TablaID FolioGrande )  
AS  
BEGIN  
 DECLARE @TablaNombre Observaciones  
 DECLARE @SQLString  NVARCHAR( MAX )  
 DECLARE @TriggerName Observaciones  
   
 SELECT @TablaNombre = TablaNombre  
 FROM T_WFTablas  
 WHERE TablaID = @TablaID  

 if ( left(@TablaNombre, 3) = 'S3_' ) 
	set @TablaNombre = replace( @TablaNombre, 'S3_', '' ) 
  
 -- 1) Agrega Timestamps de Created/Updated  
 SET @SQLString =   
N'IF NOT EXISTS (SELECT * FROM syscolumns WHERE id=object_id(''' + @TablaNombre + ''') AND name=''SINC_CREATED'')   
 ALTER TABLE ' + @TablaNombre + ' ADD SINC_CREATED Fecha NULL, SINC_UPDATED Fecha NULL'  
 EXECUTE sp_executesql @SQLString  
  
 -- 2) TimeStamps pone CERO para que se agreguen la primera sincronizaci�n  
 SET @SQLString = N'UPDATE ' + @TablaNombre + ' SET SINC_CREATED = 0, SINC_UPDATED = 0 WHERE SINC_CREATED IS NULL'  
 EXECUTE sp_executesql @SQLString  
    
 SET @SQLString = N'ALTER TABLE ' + @TablaNombre + ' ALTER COLUMN SINC_CREATED Fecha NOT NULL'  
 EXECUTE sp_executesql @SQLString  
 
 SET @SQLString = N'ALTER TABLE ' + @TablaNombre + ' ALTER COLUMN SINC_UPDATED Fecha NOT NULL'  
 EXECUTE sp_executesql @SQLString  
  
 SET @SQLString =  
  N'IF NOT EXISTS (SELECT * FROM sysobjects WHERE xtype = ''UQ'' AND parent_obj=object_id(''' + @TablaNombre + ''') AND name = ''SYNC_AK_'+@TablaNombre+'_LLAVE'' ) 
	ALTER TABLE '+@TablaNombre+' ADD CONSTRAINT SYNC_AK_'+@TablaNombre+'_LLAVE UNIQUE ( LLAVE )' 
	
 EXECUTE sp_executesql @SQLString 	
	
   -- 3) Crea Trigger de Insert  
 SET @TriggerName = 'SINC_TI_' + @TablaNombre  
 SET @SQLString =   
N'IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id( ''' + @TriggerName + ''') AND xtype in (''TR''))  
  DROP TRIGGER ' + @TriggerName  
 EXECUTE sp_executesql @SQLString  
  
 SET @SQLString =   
N'CREATE TRIGGER ' + @TriggerName + ' ON ' + @TablaNombre + ' AFTER INSERT AS  
BEGIN  
 SET NOCOUNT ON 
 UPDATE ' + @TablaNombre + '  
 SET  SINC_CREATED = GETDATE()  
 FROM ' + @TablaNombre + '  
    JOIN Inserted ON ' + @TablaNombre + '.LLAVE = Inserted.LLAVE  
END'  
 EXECUTE sp_executesql @SQLString  
  
 -- 4) Crea trigger de Update  
 SET @TriggerName = 'SINC_TU_' + @TablaNombre  
 SET @SQLString =   
N'IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id( ''' + @TriggerName + ''') AND xtype in (''TR''))  
  DROP TRIGGER ' + @TriggerName  
 EXECUTE sp_executesql @SQLString  
  
 SET @SQLString =   
N'CREATE TRIGGER ' + @TriggerName + ' ON ' + @TablaNombre + ' AFTER UPDATE AS  
BEGIN  
 SET NOCOUNT ON  
 IF ( TRIGGER_NESTLEVEL() = 1 )  
  UPDATE ' + @TablaNombre + '  
  SET  SINC_UPDATED = GETDATE()  
  FROM ' + @TablaNombre + '  
     JOIN Inserted ON ' + @TablaNombre + '.LLAVE = Inserted.LLAVE  
END'  
 EXECUTE sp_executesql @SQLString  
   
 -- 5) Crea trigger de Delete  
 SET @TriggerName = 'SINC_TD_' + @TablaNombre  
 SET @SQLString =   
N'IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id( ''' + @TriggerName + ''') AND xtype in (''TR''))  
  DROP TRIGGER ' + @TriggerName  
 EXECUTE sp_executesql @SQLString  
  
SET @SQLString =   
N'CREATE TRIGGER ' + @TriggerName + ' ON ' + @TablaNombre + ' AFTER DELETE AS  
BEGIN  
 SET NOCOUNT ON
 INSERT INTO T_WFBorrados ( TablaID, BorradoLlave, BorradoDeleted )  
 SELECT  ' + CAST( @TablaID AS VARCHAR( 10 )) + ', LLAVE, GETDATE()  
 FROM   Deleted  
END'  
 EXECUTE sp_executesql @SQLString  
END  

GO 



IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTabla_Prepara') AND type IN ( N'P', N'PC' ))
	drop procedure WFTabla_Prepara

GO 
CREATE PROCEDURE WFTabla_Prepara (
	@TablaID	INT,
	@Nombre		VARCHAR( 50 ),
	@Orden		INT,
	@Activo		BIT,
	@InsertGet	VARCHAR( MAX ),
	@InsertPut	VARCHAR( 250 ),
	@UpdateGet	VARCHAR( MAX ),
	@UpdatePut	VARCHAR( 250 ),
	@Rango		SMALLINT,
	@RangoIni	SMALLINT,
	@RangoFin	SMALLINT,
	@Grupo		INT )
AS
BEGIN
	-- Si no existe,la agrega y crea sus Triggers, else la actualiza
	IF ( SELECT COUNT(*) FROM T_WFTablas WHERE TablaID = @TablaID ) = 0
	BEGIN
	    BEGIN TRAN 
			-- Agrega el registro
			INSERT INTO T_WFTablas ( TablaID, TablaNombre, TablaOrden, TablaActivo,
				TablaInsertGet, TablaInsertPut, TablaInsertLast, TablaUpdateGet, TablaUpdatePut, TablaUpdateLast,
				TablaRango, TablaRangoIni, TablaRangoFin, TablaGrupo )
			VALUES ( @TablaID, @Nombre, @Orden, @Activo,
				@InsertGet, @InsertPut, 0, @UpdateGEt, @UpdatePut, 0,
				@Rango, @RangoIni, @RangoFin, @Grupo )
			-- Prepara los Triggers	
	
			if ( LEFT( @Nombre, 2 ) <> 'V_' )
			begin 				    
				EXEC WFTabla_PreparaCampoLlave @Nombre
				EXEC WFTabla_PreparaTriggers @TablaID
			end 
		
		IF @@ERROR = 0 
		   COMMIT TRAN 
		ELSE 
			ROLLBACK TRANSACTION 

	END
	ELSE
	BEGIN 
		UPDATE	T_WFTablas
		SET		TablaOrden		= @Orden,
				TablaActivo		= @Activo,
				TablaInsertGet	= @InsertGet,
				TablaInsertPut	= @InsertPut,
				TablaUpdateGet	= @UpdateGet,
				TablaUpdatePut	= @UpdatePut,
				TablaRango		= @Rango,
				TablaRangoIni	= @RangoIni,
				TablaRangoFin	= @RangoFin,
				TablaGrupo		= @Grupo
		WHERE	TablaID = @TablaID

		if ( LEFT( @Nombre, 2 ) <> 'V_' )
		begin 				    			
			EXEC WFTabla_PreparaTriggers @TablaID
		end 
	END
END

go 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFSync_GetSincTime') AND type IN ( N'P', N'PC' ))
	drop procedure WFSync_GetSincTime

go 

create procedure  WFSync_GetSincTime ( @SincTime Datetime OUTPUT ) 
as
begin 
	set @SincTime = getdate() 
	return 
end 
GO

IF ( not EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE  TABLE_NAME = 'T_WFBorrados')) 
begin

CREATE TABLE T_WFBorrados (
	BorradoID FolioGrande IDENTITY(1,1),
	TablaID FolioGrande,
	BorradoLlave FolioGrande,
	BorradoDeleted FechaWFN,
 CONSTRAINT [PK_WFBorrados] PRIMARY KEY CLUSTERED 
(
	BorradoID ASC
)
)

end 

GO
	
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFBorrados_GetLastSync') AND type IN ( N'P', N'PC' ))
	drop procedure WFBorrados_GetLastSync
go

CREATE PROCEDURE WFBorrados_GetLastSync
as
BEGIN
	DECLARE @GLOBAL_WFN_SYNC_BORRADO FolioChico
	DECLARE @Resultado Formula
	DECLARE @Fecha Fecha 
	
	SET @GLOBAL_WFN_SYNC_BORRADO = 313
		
	SELECT @Resultado = GL_FORMULA  FROM GLOBAL WHERE  GL_CODIGO = @GLOBAL_WFN_SYNC_BORRADO 
	
	SET @Resultado = COALESCE( @Resultado, '1899-12-30');
	
	select convert( Datetime, @Resultado, 120 ) as LastSinc
	
END

GO

	
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFBorrados_SetLastSync') AND type IN ( N'P', N'PC' ))
	drop procedure WFBorrados_SetLastSync
go

CREATE PROCEDURE WFBorrados_SetLastSync( @SincTime	DATETIME ) 
as
BEGIN
	set nocount on 
	DECLARE @GLOBAL_WFN_SYNC_BORRADO FolioChico
	DECLARE @SincTimeStr Formula
	DECLARE @Fecha Fecha 
	
	SET @GLOBAL_WFN_SYNC_BORRADO = 313
	
	set @SincTimeStr = convert(varchar(255), @SincTime, 120 )
		 
	if ( select COUNT(1) from GLOBAL where  GL_CODIGO = @GLOBAL_WFN_SYNC_BORRADO ) = 0 
	begin 
		insert into GLOBAL (GL_CODIGO, GL_DESCRIP, GL_FORMULA ) 
		values (@GLOBAL_WFN_SYNC_BORRADO, 'Ultima sinc. borrado en WFN', @SincTimeStr) 
	end
	else
	begin 
		update GLOBAL set GL_FORMULA = @SincTimeStr where  GL_CODIGO = @GLOBAL_WFN_SYNC_BORRADO  
	end; 		
	
END

go 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFBorrados_GetRegistros') AND type IN ( N'P', N'PC' ))
	drop procedure WFBorrados_GetRegistros
go

CREATE PROCEDURE WFBorrados_GetRegistros(
	@LastSinc	DATETIME,
	@SincTime	DATETIME )
AS
BEGIN
	SELECT	TablaID, BorradoLlave
	FROM	T_WFBorrados
	WHERE	BorradoDeleted >= @LastSinc AND BorradoDeleted < @SincTime
	ORDER BY BorradoID
END
go

IF ( not EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE  TABLE_NAME = 'T_WFTransacciones')) 
begin
	CREATE TABLE T_WFTRANSACCIONES(
		TRANSID FolioGrande IDENTITY(1,1),
		TRANSNOMBRE Formula,
		TAREAID FolioGrande,
		OBJETOID FolioGrande,
		VERBOID FolioGrande,
		TRANSCARGO XML NULL,
		ERRORID FolioGrande,
		TRANSERROR Varchar(max),
		TRANSINICIAL FechaWFN,
		TRANSFINAL FechaWFN,
	CONSTRAINT [PK_WFTRANSACCIONES] PRIMARY KEY CLUSTERED 
	(
		TRANSID ASC
	)
	)
end
GO

if exists (select * from sysobjects where id = object_id(N'WFConfig_GetUsuarioTRESS') and xtype in (N'FN', N'IF', N'TF')) 
	drop function WFConfig_GetUsuarioTRESS
go 

CREATE function WFConfig_GetUsuarioTRESS() 
returns Usuario 
as 
begin 
	
		declare @US_CODIGO Usuario 
		declare @XMLConfig XML 

		select @XMLConfig = ConfigXML from #COMPARTE..T_WFConfig 

		set @US_CODIGO= @XMLConfig.value('(/ControladorConfig/UsuarioSistema/.)[1]', 'INT' )

		set @US_CODIGO = coalesce( @US_CODIGO, 0 ) 

		return @US_CODIGO 
        
end

GO 


IF NOT EXISTS (SELECT * FROM syscolumns WHERE id=object_id('T_WFTransacciones') AND name='TransCargoRespuesta'	) 
	alter table T_WFTransacciones  add TransCargoRespuesta XML null 
go 

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id=object_id('T_WFTransacciones') AND name='TransCargoWFTress'	) 
	alter table T_WFTransacciones  add TransCargoWFTress XML null 
go 

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id=object_id('T_WFTransacciones') AND name='TransCargoRespuestaTress'	) 
	alter table T_WFTransacciones  add TransCargoRespuestaTress XML null 
go 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_SetError') AND type IN ( N'P', N'PC' ))
	drop procedure WFTransaccion_SetError

go 


CREATE PROCEDURE WFTransaccion_SetError (
	@TransID	INT,
	@ErrorID	INT,
	@Error		VARCHAR( max ))
AS
BEGIN
	UPDATE	T_WFTransacciones
	SET		ErrorID		= @ErrorID,
			TransError	= @Error,
			TransFinal	= GETDATE()
	WHERE	TransID = @TransID
END

GO 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_Vacaciones') AND type IN ( N'P', N'PC' ))
	drop procedure WFTransaccion_Vacaciones
go
CREATE PROCEDURE WFTransaccion_Vacaciones(
	@TransID	INT )
AS
BEGIN
	DECLARE @Cargo	XML
	
	SELECT	@Cargo	= TransCargo
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID	
END
GO 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_Permisos') AND type IN ( N'P', N'PC' ))
	drop procedure WFTransaccion_Permisos
go
CREATE PROCEDURE WFTransaccion_Permisos(
	@TransID	INT )
AS
BEGIN
	DECLARE @Cargo	XML
	
	SELECT	@Cargo	= TransCargo
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID
	
END


GO 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'Certificacion_Agregar') AND type IN ( N'P', N'PC' ))
	drop procedure Certificacion_Agregar
go

create procedure Certificacion_Agregar ( 
@CB_CODIGO	NumeroEmpleado,
@KI_FEC_CER	Fecha,
@CI_CODIGO	Codigo,
@KI_FOLIO	Descripcion,
@KI_RENOVAR	Dias,
@KI_APROBO	Booleano,
@KI_CALIF_1	Numerico,
@KI_CALIF_2	Numerico,
@KI_CALIF_3	Numerico,
@KI_SINOD_1	DescLarga,
@KI_SINOD_2	DescLarga,
@KI_SINOD_3	DescLarga,
@KI_OBSERVA	Memo,
@US_CODIGO	Usuario
)
as 
begin 
	set nocount on 
	
	insert into KAR_CERT (
		CB_CODIGO,
		KI_FEC_CER,
		CI_CODIGO,
		KI_FOLIO,
		KI_RENOVAR,
		KI_APROBO,
		KI_CALIF_1,
		KI_CALIF_2,
		KI_CALIF_3,
		KI_SINOD_1,
		KI_SINOD_2,
		KI_SINOD_3,
		KI_OBSERVA,
		US_CODIGO) 
	values (
		@CB_CODIGO,
		@KI_FEC_CER,
		@CI_CODIGO,
		@KI_FOLIO,
		@KI_RENOVAR,
		@KI_APROBO,
		@KI_CALIF_1,
		@KI_CALIF_2,
		@KI_CALIF_3,
		@KI_SINOD_1,
		@KI_SINOD_2,
		@KI_SINOD_3,
		@KI_OBSERVA,
		@US_CODIGO
	)
end 

go 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'Certificacion_Actualizar') AND type IN ( N'P', N'PC' ))
	drop procedure Certificacion_Actualizar
go


create procedure Certificacion_Actualizar(
@CB_CODIGO	NumeroEmpleado,
@KI_FEC_CER	Fecha,
@CI_CODIGO	Codigo,
@KI_FOLIO	Descripcion,
@KI_RENOVAR	Dias,
@KI_APROBO	Booleano,
@KI_CALIF_1	Numerico,
@KI_CALIF_2	Numerico,
@KI_CALIF_3	Numerico,
@KI_SINOD_1	DescLarga,
@KI_SINOD_2	DescLarga,
@KI_SINOD_3	DescLarga,
@KI_OBSERVA	Memo,
@US_CODIGO	Usuario
)
as 
begin 
set nocount on 
	
	update KAR_CERT 
	set		
		KI_FOLIO = @KI_FOLIO,
		KI_RENOVAR = @KI_RENOVAR, 
		KI_APROBO  = @KI_APROBO,
		KI_CALIF_1 = @KI_CALIF_1,
		KI_CALIF_2 = @KI_CALIF_2,
		KI_CALIF_3 = @KI_CALIF_3,
		KI_SINOD_1 = @KI_SINOD_1,
		KI_SINOD_2 = @KI_SINOD_2,
		KI_SINOD_3 = @KI_SINOD_3,
		KI_OBSERVA = @KI_OBSERVA,
		US_CODIGO = @US_CODIGO
	where CB_CODIGO = @CB_CODIGO and KI_FEC_CER = @KI_FEC_CER and CI_CODIGO = @CI_CODIGO


end 

go 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_Certificaciones') AND type IN ( N'P', N'PC' ))
	drop procedure WFTransaccion_Certificaciones
go
CREATE PROCEDURE WFTransaccion_Certificaciones(
	@TransID	INT )
AS
BEGIN 
	DECLARE @ErrorID	INT
	DECLARE @Cargo	XML
	
	SELECT	@Cargo	= TransCargo
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID	
	
	DECLARE @EMPRESA  varchar(10) 
	DECLARE @CB_CODIGO	INT
	DECLARE @NOMBRE		Formula 
	
	DECLARE @PermisoInicial Fecha 

    DECLARE @CertificaFecha Fecha -- ="2015/02/21"
    DECLARE @CompetenCodigo Codigo --- ="ABC1"
    DECLARE @CertificaFolio Descripcion -- ="001"
    DECLARE @CertificaRenovar Foliochico -- ="365"
    DECLARE @CertificaAprobado Status  -- ="1"
	DECLARE @CertificaAprobadoBool  Booleano   -- ="1"
    DECLARE @CertificaCalif1 Numerico -- ="85.5"
    DECLARE @CertificaCalif2 Numerico -- ="100"
    DECLARE @CertificaCalif3 Numerico --="0"
    DECLARE @CertificaSinod1 DescLarga -- ="Juan Sinodal"
    DECLARE @CertificaSinod2 DescLarga --=" "
    DECLARE @CertificaSinod3 DescLarga --=" "
    DECLARE @CertificaObserva varchar(max) -- ="Buen esfuerzo"
	DECLARE @US_CODIGO Usuario 
	
	DECLARE @XMLTress  Varchar(max) 

	declare @ClienteID FolioGrande 
			
	SET	@ClienteID	= coalesce(  @Cargo.value('(//REGISTRO/@ClienteID)[1]', 'INT' ), 0) 
	SET @EMPRESA	= dbo.WFEmpresas_GetCodigoEmpresa( @ClienteID ) ; 
	SET	@CB_CODIGO	= coalesce(  @Cargo.value('(//REGISTRO/@EmpNumero)[1]', 'INT' ), 0) 	
	SET @CertificaFecha   = coalesce(@Cargo.value('(//REGISTRO/@CertificaFecha)[1]', 'DateTime' ), '1899-12-30' ) 	
	SET	@CompetenCodigo		= coalesce( @Cargo.value('(//REGISTRO/@CompetenCodigo)[1]', 'Varchar(6)' ), '' ) 	
	SET	@CertificaFolio		= coalesce( @Cargo.value('(//REGISTRO/@CertificaFolio)[1]', 'Varchar(256)' ), '' ) 	
	
	SET	@CertificaRenovar	= coalesce(@Cargo.value('(//REGISTRO/@CertificaRenovar)[1]', 'INT' ), 0)	
	SET	@CertificaAprobado	= coalesce(@Cargo.value('(//REGISTRO/@CertificaAprobado)[1]', 'INT' ), 0)	

	if ( @CertificaAprobado = 1 ) 
		Set @CertificaAprobadoBool = 'S' 
	else
		Set @CertificaAprobadoBool = 'N' 

	SET	@CertificaCalif1	= coalesce(@Cargo.value('(//REGISTRO/@CertificaCalif1)[1]', 'DECIMAL(15,2)' ), 0.00)
	SET	@CertificaCalif2	= coalesce(@Cargo.value('(//REGISTRO/@CertificaCalif2)[1]', 'DECIMAL(15,2)' ), 0.00)
	SET	@CertificaCalif3	= coalesce(@Cargo.value('(//REGISTRO/@CertificaCalif3)[1]', 'DECIMAL(15,2)' ), 0.00)

	SET	@CertificaSinod1		= coalesce( @Cargo.value('(//REGISTRO/@CertificaSinod1)[1]', 'Varchar(256)' ), '' ) 		
	SET	@CertificaSinod2		= coalesce( @Cargo.value('(//REGISTRO/@CertificaSinod2)[1]', 'Varchar(256)' ), '' ) 		
	SET	@CertificaSinod3		= coalesce( @Cargo.value('(//REGISTRO/@CertificaSinod3)[1]', 'Varchar(256)' ), '' ) 		
	SET	@CertificaObserva	= coalesce( @Cargo.value('(//REGISTRO/@CertificaObserva)[1]', 'Varchar(max)' ), '' ) 			
	
	--- SET	@FileID	= coalesce(@Cargo.value('(//REGISTRO/@FileID)[1]', 'INT' ), 0)	

	set @US_CODIGO = dbo.WFConfig_GetUsuarioTRESS(); 

	if ( select COUNT(*) from KAR_CERT where CB_CODIGO = @CB_CODIGO and KI_FEC_CER = @CertificaFecha and CI_CODIGO = @CompetenCodigo ) > 0 
	begin 
		exec Certificacion_Actualizar @CB_CODIGO, @CertificaFecha, @CompetenCodigo, @CertificaFolio, @CertificaRenovar, @CertificaAprobadoBool, 
			@CertificaCalif1, @CertificaCalif2, @CertificaCalif3, @CertificaSinod1, @CertificaSinod2, @CertificaSinod3, @CertificaObserva , @US_CODIGO 
		
	end 
	else
	begin 
		exec Certificacion_Agregar @CB_CODIGO, @CertificaFecha, @CompetenCodigo, @CertificaFolio, @CertificaRenovar, @CertificaAprobadoBool, 
			@CertificaCalif1, @CertificaCalif2, @CertificaCalif3, @CertificaSinod1, @CertificaSinod2, @CertificaSinod3, @CertificaObserva , @US_CODIGO 
	end; 
		
end 


GO 

if exists (select * from sysobjects where id = object_id(N'WFEmpresas_GetCodigoEmpresa') and xtype in (N'FN', N'IF', N'TF')) 
	drop function WFEmpresas_GetCodigoEmpresa
go 

CREATE function WFEmpresas_GetCodigoEmpresa( @ClienteID FolioGrande ) returns Varchar(20) 
as 
begin 

        declare @Cm_Codigo Varchar(20) 
        
        set @Cm_Codigo = '' 
        
        if @ClienteID > 0 
                select @Cm_Codigo = CM_CODIGO from #COMPARTE..COMPANY where CM_WFEMPID = @ClienteID 
         
        return RTRIM( @Cm_Codigo) 
end

go 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_Infonavit') AND type IN ( N'P', N'PC' ))
	drop procedure WFTransaccion_Infonavit
go
CREATE PROCEDURE WFTransaccion_Infonavit(
	@TransID	INT )
AS
BEGIN
	DECLARE @Cargo	XML
	
	SELECT	@Cargo	= TransCargo
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID
	
					
	update KARINF set KI_NOTA = InfoNota  		
	from KARINF KI, ( 
	select  		
			ClienteID	= coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
			InfoTipo	= coalesce(t.c.value('(@InfoTipo)', 'INT' ), 0),		
			EMPRESA	= dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
			EmpNumero	= coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,			
			InfoFecha   = coalesce(t.c.value('(@InfoFecha)', 'Datetime' ), '1899-12-30' ) ,			
			InfoNota	= coalesce(t.c.value('(@InfoNota)', 'Varchar(max)' ), '' ) 					
	 from 
	 @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
	 ) CargoTabla
	where KI.CB_CODIGO = CargoTabla.EmpNumero and KI.KI_FECHA = CargoTabla.InfoFecha  	 

	return 0 

END

GO
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_Create') AND type IN ( N'P', N'PC' ))
	drop procedure WFTransaccion_Create
go
CREATE PROCEDURE WFTransaccion_Create(
	@TransID	INT OUTPUT,
	@Nombre		VARCHAR( 255 ),
	@TareaID	INT,
	@ObjetoID	INT,
	@VerboID	INT,
	@Cargo		XML )
AS
BEGIN
	INSERT INTO T_WFTransacciones ( TransNombre, TareaID, ObjetoID, VerboID, TransCargo, ErrorID, TransInicial, TransFinal )
	VALUES ( @Nombre, @TareaID, @ObjetoID, @VerboID, @Cargo, 0, GETDATE(), 0 )
	
	SET @TransID	= SCOPE_IDENTITY()
END
GO
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_Aplica') AND type IN ( N'P', N'PC' ))
	drop procedure WFTransaccion_Aplica
go


CREATE PROCEDURE WFTransaccion_Aplica(
	@TransID	INT,
	@Error		VARCHAR( 255 ) OUTPUT )
AS
BEGIN
	DECLARE	@VerboID	INT
	DECLARE	@ObjetoID	INT
	DECLARE @ErrorID	INT
	
	SELECT	@VerboID	= VerboID, @ObjetoID = ObjetoID 
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID
	

	IF ( @VerboID = 1312 )
		EXEC @ErrorID = WFTransaccion_Vacaciones @TransID
	ELSE 
	IF ( @VerboID = 1304 )
		EXEC @ErrorID = WFTransaccion_Permisos @TransID		
	ELSE
	IF ( @VerboID = 67 ) and ( @ObjetoID = 1090 ) 
	begin 
		-- No tiene programacion adicional 
		set @ErrorID = 0 
	end 
	ELSE	
	IF ( @VerboID = 1239 ) and ( @ObjetoID = 1060 )    --- Guardar documento(s)
	begin 
		-- No tiene programacion adicional 
		set @ErrorID = 0 
	end 
	ELSE 	
	IF ( @VerboID = 1341 ) and ( @ObjetoID = 1068 )   --- Cambiar datos de empleado
	begin 
		-- No tiene programacion adicional 
		set @ErrorID = 0 
	end 
	ELSE 	
	IF ( @VerboID = 1456 ) and ( @ObjetoID = 1096 )   --- Guardar Excepciones
	begin 		
		-- No tiene programacion adicional 
		set @ErrorID = 0 
	end 
	ELSE 	
	IF ( @VerboID = 1077 ) and ( @ObjetoID = 1308 )   --- Guardar Incapacidades
	begin 		
		-- No tiene programacion adicional 
		set @ErrorID = 0 
	end 
	ELSE 	
	IF ( @VerboID = 1469 ) and ( @ObjetoID = 1075)   --- Guardar Prestamos
	begin 		
		-- No tiene programacion adicional 
		set @ErrorID = 0 
	end 
	ELSE 	
	IF ( @VerboID = 86) and ( @ObjetoID = 1068)   --- Cambio de Puesto
	begin 		
		-- No tiene programacion adicional 
		set @ErrorID = 0 
	end 
	ELSE 	
	IF ( @VerboID = 94) and ( @ObjetoID = 1068)   --- Cambio de Turno
	begin 		
		-- No tiene programacion adicional 
		set @ErrorID = 0 
	end 
	ELSE 	
	IF ( @VerboID = 150) and ( @ObjetoID = 1068)   --- Cambio de Equipo
	begin 		
		-- No tiene programacion adicional 
		set @ErrorID = 0 
	end 
	ELSE 	
	IF ( @VerboID = 1282) and ( @ObjetoID = 1068)   --- Baja de Empleado
	begin 		
		-- No tiene programacion adicional 
		set @ErrorID = 0 
	end 
	ELSE 	
	IF ( @VerboID = 90) and ( @ObjetoID = 1068)   --- Cambio de Salario
	begin 		
		-- No tiene programacion adicional 
		set @ErrorID = 0 
	end 
	ELSE 	
	IF ( @VerboID = 106) and ( @ObjetoID = 1068)   --- Cambio de Area
	begin 		
		-- No tiene programacion adicional 
		set @ErrorID = 0 
	end 
	ELSE
	IF ( @VerboID = 98) and ( @ObjetoID = 1068)   --- Cambio de Contrato
	begin 		
		-- No tiene programacion adicional 
		set @ErrorID = 0 
	end 
	ELSE
	IF ( @VerboID = 1285) and ( @ObjetoID = 1071)   --- CRUD Parientes
	begin 		
		-- No tiene programacion adicional 
		set @ErrorID = 0 
	end 	
	ELSE 	
	IF ( @VerboID = 1490 ) and ( @ObjetoID = 1107 )   --- Guardar cambios Infonavit 
	begin 	
		-- No tiene programacion adicional 
		set @ErrorID = 0 
		EXEC @ErrorID = WFTransaccion_Infonavit @TransID		

	end 
	ELSE
    IF ( @VerboID = 2085 ) and ( @ObjetoID = 1213 )   --- Guardar Seguro de Gastos Medicos Empleado
    begin 
         -- No tiene programacion adicional 
         set @ErrorID = 0 
    end 
	ELSE
	IF ( @VerboID = 100269 ) and ( @ObjetoID = 1051 )   --- Realiza intercambio de festivos
    begin 
         -- No tiene programacion adicional 
         set @ErrorID = 0 
    end 
	ELSE 
	IF ( @VerboID = 1281 ) and ( @ObjetoID = 1068 )   --- Realiza Alta de Empleado oficial 
    begin 
         -- No tiene programacion adicional 
         set @ErrorID = 0 
    end 
    ELSE
    IF ( @VerboID = 80 ) and ( @ObjetoID = 1068 )   --- Realiza Reingreso de Empleado 
    begin 
         -- No tiene programacion adicional 
         set @ErrorID = 0 
    end   
    ELSE
    IF ( @VerboID = 2138 ) and ( @ObjetoID = 1222 )   --- Realiza Checada Manual
    begin 
         -- No tiene programacion adicional 
         set @ErrorID = 0 
    end   
	ELSE
	BEGIN
		SET @ErrorID	= -1
		EXEC WFTransaccion_SetError @TransID, @ErrorID, 'Tipo de Transacci�n no es v�lido'
	END
	
	IF ( @ErrorID <> 0 )
		SELECT	@Error	= TransError
		FROM	T_WFTransacciones
		WHERE	TransID = @TransID
	ELSE
	BEGIN 
		SELECT	@Error	= TransError, @ErrorID =  ErrorID
		FROM	T_WFTransacciones
		WHERE	TransID = @TransID				
	END 

	set @Error   = coalesce(@Error, '')
	set @ErrorID = coalesce(@ErrorID, 0 ) 

	RETURN @ErrorID
END

GO
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_GetXMLVacaciones') AND type IN ( N'P', N'PC' ))
	drop procedure WFTransaccion_GetXMLVacaciones
go

CREATE PROCEDURE WFTransaccion_GetXMLVacaciones( 
	@TransID	INT,
	@Xml		XML OUTPUT ,
	@FileID		INT OUTPUT, 
	@Error		VARCHAR( 255 ) OUTPUT)
AS
begin     
    DECLARE @ErrorID    INT
    DECLARE @Cargo    XML
    
    SELECT    @Cargo    = TransCargo
    FROM    T_WFTransacciones
    WHERE    TransID = @TransID    
    
    DECLARE @EMPRESA  varchar(10) 
    DECLARE @CB_CODIGO    INT
    DECLARE @NOMBRE        Formula 
    DECLARE @VaFecIni  Fecha 
    DECLARE @VaFecFin  Fecha 
    DECLARE @VaDias    Pesos
    DECLARE @VaDiasGozo Pesos 
    DECLARE @VaDiasPago Pesos 
    DECLARE @VaPrima     Pesos
    DECLARE @PeriodoID    FolioGrande  
    DECLARE @VaNomYear Anio 
    DECLARE @VaNomTipo NominaTipo
    DECLARE @VaNomNum  NominaNumero
    DECLARE @VaPeriodo  Descripcion 
    DECLARE @VaComenta    Titulo 
	DECLARE @UsarVaGozo status 
    DECLARE @XMLTress  Varchar(max)     

    declare @ClienteID FolioGrande 
    set @FileID = 0         

    select  
            ClienteID    = coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
            EMPRESA    = dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
            CB_CODIGO    = coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,
            NOMBRE        = coalesce(  t.c.value('(@PersonaName)', 'Varchar(256)' ), '' ),    
            VA_FEC_INI   = coalesce(t.c.value('(@VacacionInicial)', 'DateTime' ), '1899-12-30' ) ,
            VA_FEC_FIN  = coalesce(t.c.value('(@VacacionFinal)', 'DateTime' ), '1899-12-30' ) ,
            VaDias        = coalesce(t.c.value('(@VacacionDias)', 'DECIMAL(15,2)' ), 0.00),
            VA_GOZO    = coalesce(t.c.value('(@VacacionGozo)', 'DECIMAL(15,2)' ), 0.00),
            VA_PAGO    = coalesce(t.c.value('(@VacacionPago)', 'DECIMAL(15,2)' ), 0.00),
            VA_PRIMA    =      coalesce(t.c.value('(@VacacionPrima)', 'DECIMAL(15,2)' ), 0.00),
            VA_NOMYEAR    = coalesce(t.c.value('(@PeriodoYear)', 'INT' ), 0),
            VA_NOMTIPO    = coalesce(t.c.value('(@TipoNominaCodigo)', 'INT' ), 0),
            VA_NOMNUME    = coalesce(t.c.value('(@PeriodoNumero)', 'INT' ), 0),
            VA_PERIODO    = coalesce(t.c.value('(@VacacionPeriodo)', 'Varchar(256)' ), '' ) ,
            VA_COMENTA  = coalesce(t.c.value('(@VacacionDescrip)', 'Varchar(255)' ), '' ), 
			USAR_VA_GOZO = coalesce(tr.cr.value('(@VacacionGozoEditar)', 'INT' ), 0),
            FileID    = coalesce(t.c.value('(@FileID)', 'INT' ), 0)    
        
     into #VACA_TEMP
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
	 cross apply @Cargo.nodes('//REGISTROS') tr(cr)
     

    select TOP 1 
        @VaPeriodo = VA_PERIODO, 
        @VaComenta = VA_COMENTA, 
        @VaNomYear = VA_NOMYEAR, 
        @VaNomTipo = VA_NOMTIPO, 
        @VaNomNum = VA_NOMNUME, 
		@UsarVaGozo = USAR_VA_GOZO
    from #VACA_TEMP order by VA_FEC_INI
     

         declare @XML_Mult XML 

         set @XML_Mult = (     
           select 
                EMPRESA, 
                'VACA' as MOV3,
				CB_CODIGO as EMPLEADO, 
                VA_FEC_INI = convert( varchar(20), MIN(VA_FEC_INI), 112 ), 
                VA_FEC_FIN = convert( varchar(20), MAX(VA_FEC_FIN), 112  ),
				USAR_VA_GOZO = @UsarVaGozo, 
                VA_GOZO = SUM(VA_GOZO), 
                VA_PAGO = SUM(VA_PAGO), 
                VA_P_PRIMA = SUM( VA_PRIMA ),
                VA_PERIODO = MAX(VA_PERIODO),
                VA_COMENTA  = MAX(VA_COMENTA),  
                (     
            select  
                convert( varchar(20), VA_FEC_INI , 112 )   as VA_FEC_INI, 
                convert( varchar(20), VA_FEC_FIN , 112 )   as VA_FEC_FIN,   
				USAR_VA_GOZO = @UsarVaGozo,       
                VA_GOZO, 
                VA_PAGO, 
                VA_NOMTIPO, 
                VA_NOMNUME, 
                VA_NOMYEAR 
            from #VACA_TEMP VTU
            where VTG.CB_CODIGO  = VTU.CB_CODIGO and VTG.EMPRESA = VTU.EMPRESA 
            order by VA_FEC_INI 
            FOR XML PATH('VACACION'), ROOT('GENERAR_MULTIPLE'), TYPE )
           from #VACA_TEMP VTG
            group by EMPRESA, CB_CODIGO 
            FOR XML PATH('DATOS'), ROOT('MULTIPLE'), TYPE 
         )
                     
        set @XML = @XML_Mult

    
    SET @ErrorID = 0 
    set @Error = '' 
    
    drop table #VACA_TEMP     
    
end

GO
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_GetXMLReingresoEmpleado') AND type IN ( N'P', N'PC' ))
	drop procedure WFTransaccion_GetXMLReingresoEmpleado
go


create  PROCEDURE WFTransaccion_GetXMLReingresoEmpleado( 
	@TransID	INT,
	@Xml		XML OUTPUT ,
	@FileID		INT OUTPUT, 
	@Error		VARCHAR( 255 ) OUTPUT)
AS
begin 	
	DECLARE @ErrorID	INT
	DECLARE @Cargo	XML
	
	SELECT	@Cargo	= TransCargo
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID	
	
	DECLARE @EMPRESA			varchar(10) 
	DECLARE @kCB_CODIGO			INT
	DECLARE @NOMBRE				Formula 
	DECLARE @KarFecha			Fecha 
	
	DECLARE @PersonaID			FolioGrande 		
	DECLARE @EquipoID			FolioGrande  	
	DECLARE @PersonaTipo		int 
	DECLARE @KarObservaciones	Titulo  
	DECLARE @kCB_AUTOSAL		Booleano
	DECLARE @kCB_AUTOSAL_GLOBAL Booleano

	DECLARE @kCB_APE_MAT	Descripcion
	DECLARE @kCB_APE_PAT	Descripcion
	DECLARE @kCB_NOMBRES	Descripcion


	DECLARE @kCB_BAN_ELE	Descripcion
	DECLARE @kCB_CALLE		Observaciones
	DECLARE @kCB_CARRERA	DescLarga
	DECLARE @kCB_CHECA		Booleano
	DECLARE @kCB_CIUDAD		Descripcion
	DECLARE @kCB_CLASIFI	Codigo
	DECLARE @kCB_CODPOST	Referencia
	DECLARE @kCB_COLONIA	Descripcion
	DECLARE @kCB_CONTRAT	Codigo1
	DECLARE @kCB_CREDENC	Codigo1
	DECLARE @kCB_CTA_VAL	Descripcion
	DECLARE @kCB_CURP		Descripcion
	DECLARE @kCB_E_MAIL		Formula
	DECLARE @kCB_ESTADO		Codigo2
	DECLARE @kCB_ESTUDIO	Codigo2
	DECLARE @kCB_FEC_ANT	Fecha
	DECLARE @kCB_FEC_CON	Fecha
	DECLARE @kCB_FEC_ING	Fecha
	DECLARE @kCB_FEC_NAC	Fecha
	DECLARE @kCB_LUG_NAC	Descripcion
	DECLARE @kCB_MUNICIP	Codigo
	DECLARE @kCB_NACION		Descripcion
	DECLARE @kCB_NIVEL0		Codigo
	DECLARE @kCB_NIVEL1		Codigo
	DECLARE @kCB_NIVEL2		Codigo
	DECLARE @kCB_NIVEL3		Codigo
	DECLARE @kCB_NIVEL4		Codigo
	DECLARE @kCB_NIVEL5		Codigo
	DECLARE @kCB_NIVEL6		Codigo
	DECLARE @kCB_NIVEL7		Codigo
	DECLARE @kCB_NIVEL8		Codigo
	DECLARE @kCB_NIVEL9		Codigo
	DECLARE @kCB_PATRON		RegPatronal
	DECLARE @kCB_PUESTO		Codigo
	DECLARE @kCB_RFC		Descripcion
	DECLARE @kCB_SALARIO	PesosDiario
	DECLARE @PersonaSalario PesosDiario
	DECLARE @kCB_SALARIO_BRUTO	PesosDiario
	DECLARE @kCB_SEGSOC		Descripcion
	DECLARE @kCB_SEXO		Codigo1
	DECLARE @kCB_TABLASS	Codigo1
	DECLARE @kCB_TEL		Descripcion
	DECLARE @kCB_TURNO		Codigo
	DECLARE @kCB_ZONA_GE	ZonaGeo

	---PADRES
	DECLARE @kNOM_PADRE	Descripcion
	DECLARE @kNOM_MADRE	Descripcion

	---- CAMPOS NUEVOS A MAPEAR
	DECLARE @kCB_AR_FEC as Fecha
	DECLARE @kCB_AR_HOR as Hora
	DECLARE @kCB_EDO_CIV as Codigo1
	DECLARE @kCB_FEC_RES as Fecha
	DECLARE @kCB_EST_HOR as Descripcion
	DECLARE @kCB_EST_HOY as Booleano
	DECLARE @kCB_EST_HOY_INT as Status
	DECLARE @kCB_EVALUA as DiasFrac
	DECLARE @kCB_EXPERIE as Titulo
	DECLARE @kCB_FEC_BAJ as Fecha
	DECLARE @kCB_FEC_BSS as Fecha
	DECLARE @kCB_FEC_INT as Fecha
	DECLARE @kCB_FEC_REV as Fecha
	DECLARE @kCB_FEC_VAC as Fecha
	DECLARE @kCB_G_FEC_1 as Fecha
	DECLARE @kCB_G_FEC_2 as Fecha
	DECLARE @kCB_G_FEC_3 as Fecha
	DECLARE @kCB_G_FEC_4 as Fecha
	DECLARE @kCB_G_FEC_5 as Fecha
	DECLARE @kCB_G_FEC_6 as Fecha
	DECLARE @kCB_G_FEC_7 as Fecha
	DECLARE @kCB_G_FEC_8 as Fecha
	DECLARE @kCB_G_LOG_1 as Booleano
	DECLARE @kCB_G_LOG_1_INT as Status
	DECLARE @kCB_G_LOG_2 as Booleano
	DECLARE @kCB_G_LOG_2_INT as Status
	DECLARE @kCB_G_LOG_3 as Booleano
	DECLARE @kCB_G_LOG_3_INT as Status
	DECLARE @kCB_G_LOG_4 as Booleano
	DECLARE @kCB_G_LOG_4_INT as Status
	DECLARE @kCB_G_LOG_5 as Booleano
	DECLARE @kCB_G_LOG_5_INT as Status
	DECLARE @kCB_G_LOG_6 as Booleano
	DECLARE @kCB_G_LOG_6_INT as Status
	DECLARE @kCB_G_LOG_7 as Booleano
	DECLARE @kCB_G_LOG_7_INT as Status
	DECLARE @kCB_G_LOG_8 as Booleano
	DECLARE @kCB_G_LOG_8_INT as Status
	DECLARE @kCB_G_NUM_1 as Pesos
	DECLARE @kCB_G_NUM_2 as Pesos
	DECLARE @kCB_G_NUM_3 as Pesos
	DECLARE @kCB_G_NUM_4 as Pesos
	DECLARE @kCB_G_NUM_5 as Pesos
	DECLARE @kCB_G_NUM_6 as Pesos
	DECLARE @kCB_G_NUM_7 as Pesos
	DECLARE @kCB_G_NUM_8 as Pesos
	DECLARE @kCB_G_NUM_9 as Pesos
	DECLARE @kCB_G_NUM10 as Pesos
	DECLARE @kCB_G_NUM11 as Pesos
	DECLARE @kCB_G_NUM12 as Pesos
	DECLARE @kCB_G_NUM13 as Pesos
	DECLARE @kCB_G_NUM14 as Pesos
	DECLARE @kCB_G_NUM15 as Pesos
	DECLARE @kCB_G_NUM16 as Pesos
	DECLARE @kCB_G_NUM17 as Pesos
	DECLARE @kCB_G_NUM18 as Pesos
	DECLARE @kCB_G_TAB_1 as Codigo
	DECLARE @kCB_G_TAB_2 as Codigo
	DECLARE @kCB_G_TAB_3 as Codigo
	DECLARE @kCB_G_TAB_4 as Codigo
	DECLARE @kCB_G_TAB_5 as Codigo
	DECLARE @kCB_G_TAB_6 as Codigo
	DECLARE @kCB_G_TAB_7 as Codigo
	DECLARE @kCB_G_TAB_8 as Codigo
	DECLARE @kCB_G_TAB_9 as Codigo
	DECLARE @kCB_G_TAB10 as Codigo
	DECLARE @kCB_G_TAB11 as Codigo
	DECLARE @kCB_G_TAB12 as Codigo
	DECLARE @kCB_G_TAB13 as Codigo
	DECLARE @kCB_G_TAB14 as Codigo
	DECLARE @kCB_G_TEX_1 as Descripcion
	DECLARE @kCB_G_TEX_2 as Descripcion
	DECLARE @kCB_G_TEX_3 as Descripcion
	DECLARE @kCB_G_TEX_4 as Descripcion
	DECLARE @kCB_G_TEX_5 as Descripcion
	DECLARE @kCB_G_TEX_6 as Descripcion
	DECLARE @kCB_G_TEX_7 as Descripcion
	DECLARE @kCB_G_TEX_8 as Descripcion
	DECLARE @kCB_G_TEX_9 as Descripcion
	DECLARE @kCB_G_TEX10 as Descripcion
	DECLARE @kCB_G_TEX11 as Descripcion
	DECLARE @kCB_G_TEX12 as Descripcion
	DECLARE @kCB_G_TEX13 as Descripcion
	DECLARE @kCB_G_TEX14 as Descripcion
	DECLARE @kCB_G_TEX15 as Descripcion
	DECLARE @kCB_G_TEX16 as Descripcion
	DECLARE @kCB_G_TEX17 as Descripcion
	DECLARE @kCB_G_TEX18 as Descripcion
	DECLARE @kCB_G_TEX19 as Descripcion
	DECLARE @kCB_HABLA as Booleano
	DECLARE @kCB_HABLA_INT as Status
	DECLARE @kCB_IDIOMA as Descripcion
	DECLARE @kCB_INFCRED as Descripcion
	DECLARE @kCB_INFMANT as Booleano
	DECLARE @kCB_INFMANT_INT as Status
	DECLARE @kCB_INFTASA as DescINFO
	DECLARE @kCB_LA_MAT as Descripcion
	DECLARE @kCB_LAST_EV as Fecha
	DECLARE @kCB_MAQUINA as Titulo
	DECLARE @kCB_MED_TRA as Codigo1
	DECLARE @kCB_PASAPOR as Booleano
	DECLARE @kCB_PASAPOR_INT as Status
	DECLARE @kCB_FEC_INC as Fecha
	DECLARE @kCB_FEC_PER as Fecha
	DECLARE @kCB_NOMYEAR as Anio
	DECLARE @kCB_NOMTIPO as NominaTipo
	DECLARE @kCB_NEXT_EV as Fecha
	DECLARE @kCB_NOMNUME as NominaNumero
	DECLARE @kCB_SAL_INT as PesosDiario
	DECLARE @kCB_VIVECON as Codigo1
	DECLARE @kCB_VIVEEN as Codigo2
	DECLARE @kCB_ZONA as Referencia
	DECLARE @kCB_INFTIPO as Status
	DECLARE @kCB_DER_FEC as Fecha
	DECLARE @kCB_DER_PAG as DiasFrac
	DECLARE @kCB_V_PAGO as DiasFrac
	DECLARE @kCB_DER_GOZ as DiasFrac
	DECLARE @kCB_V_GOZO as DiasFrac
	DECLARE @kCB_TIP_REV as Codigo
	DECLARE @kCB_MOT_BAJ as Codigo3
	DECLARE @kCB_FEC_SAL as Fecha
	DECLARE @kCB_INF_INI as Fecha
	DECLARE @kCB_INF_OLD as DescINFO
	DECLARE @kCB_OLD_SAL as PesosDiario
	DECLARE @kCB_OLD_INT as PesosDiario
	DECLARE @kCB_PRE_INT as PesosDiario
	DECLARE @kCB_PER_VAR as PesosDiario
	DECLARE @kCB_SAL_TOT as PesosDiario
	DECLARE @kCB_TOT_GRA as PesosDiario
	DECLARE @kCB_FAC_INT as Tasa
	DECLARE @kCB_RANGO_S as Tasa
	DECLARE @kCB_CLINICA as Codigo3
	DECLARE @kCB_FEC_NIV as Fecha
	DECLARE @kCB_FEC_PTO as Fecha
	DECLARE @kCB_AREA as Codigo
	DECLARE @kCB_FEC_TUR as Fecha
	DECLARE @kCB_FEC_KAR as Fecha
	DECLARE @kCB_PENSION as Status
	DECLARE @kCB_CANDIDA as FolioGrande
	DECLARE @kCB_ID_NUM as Descripcion
	DECLARE @kCB_ENT_NAC as Codigo2
	DECLARE @kCB_COD_COL as Codigo
	DECLARE @kCB_PASSWRD as Passwd
	DECLARE @kCB_PLAZA as FolioGrande
	DECLARE @kCB_FEC_PLA as Fecha
	DECLARE @kCB_DER_PV as DiasFrac
	DECLARE @kCB_V_PRIMA as DiasFrac
	DECLARE @kCB_SUB_CTA as Descripcion
	DECLARE @kCB_NETO as Pesos
	DECLARE @kCB_PORTAL as Booleano
	DECLARE @kCB_PORTAL_INT as Status
	DECLARE @kCB_DNN_OK as Booleano
	DECLARE @kCB_DNN_OK_INT as Status
	DECLARE @kCB_USRNAME as DescLarga
	DECLARE @kCB_NOMINA as NominaTipo
	DECLARE @kCB_FEC_NOM as Fecha
	DECLARE @kCB_RECONTR as Booleano
	DECLARE @kCB_RECONTR_INT as Status
	DECLARE @kCB_DISCAPA as Booleano
	DECLARE @kCB_DISCAPA_INT as Status
	DECLARE @kCB_INDIGE as Booleano
	DECLARE @kCB_INDIGE_INT as Status
	DECLARE @kCB_FONACOT as Descripcion
	DECLARE @kCB_EMPLEO as Booleano
	DECLARE @kCB_EMPLEO_INT as Status
	DECLARE @kUS_CODIGO as Usuario
	DECLARE @kCB_FEC_COV as Fecha
	DECLARE @kCB_G_TEX20 as Descripcion
	DECLARE @kCB_G_TEX21 as Descripcion
	DECLARE @kCB_G_TEX22 as Descripcion
	DECLARE @kCB_G_TEX23 as Descripcion
	DECLARE @kCB_G_TEX24 as Descripcion
	DECLARE @kCB_INFDISM as Booleano
	DECLARE @kCB_INFDISM_INT as Status
	DECLARE @kCB_INFACT as Booleano
	DECLARE @kCB_INFACT_INT as Status
	DECLARE @kCB_NUM_EXT as NombreCampo 
	DECLARE @kCB_NUM_INT as NombreCampo
	DECLARE @kCB_INF_ANT as Fecha
	DECLARE @kCB_TDISCAP as Status
	DECLARE @kCB_ESCUELA as DescLarga
	DECLARE @kCB_TESCUEL as Status
	DECLARE @kCB_TITULO as Status
	DECLARE @kCB_YTITULO as Anio
	DECLARE @kCB_CTA_GAS as Descripcion
	DECLARE @kCB_ID_BIO as FolioGrande
	DECLARE @kCB_GP_COD as Codigo
	DECLARE @kCB_TIE_PEN as Booleano
	DECLARE @kCB_TIE_PEN_INT as Status
	DECLARE @kCB_TSANGRE as Descripcion
	DECLARE @kCB_ALERGIA as Formula
	DECLARE @kCB_BRG_ACT as Booleano
	DECLARE @kCB_BRG_ACT_INT as Status
	DECLARE @kCB_BRG_TIP as Status
	DECLARE @kCB_BRG_ROL as Booleano
	DECLARE @kCB_BRG_ROL_INT as Status
	DECLARE @kCB_BRG_JEF as Booleano
	DECLARE @kCB_BRG_JEF_INT as Status
	DECLARE @kCB_BRG_CON as Booleano
	DECLARE @kCB_BRG_CON_INT as Status
	DECLARE @kCB_BRG_PRA as Booleano
	DECLARE @kCB_BRG_PRA_INT as Status
	DECLARE @kCB_BRG_NOP as Descripcion
	DECLARE @kCB_BANCO as Codigo
	DECLARE @kCB_REGIMEN as Status
	----- FIN	
	
	DECLARE @XMLTress  Varchar(max) 
	

	declare @ClienteID FolioGrande 
	set @FileID = 0 
	SET @ErrorID = 0 
	set @ERROR = '' 

	--Toma globales de Empleado 		
	select 
		@kCB_Ciudad = Ciudad , 
		@kCB_Estado = Estado , 
		@kCB_MUNICIP = Municipio , 
		@kCB_Checa = Checa , 
		@kCB_Nacion = Nacion , 
		@kCB_Sexo = Sexo , 
		@kCB_Credenc = Credencial , 
		@kCB_EDO_CIV = EstadoCivil , 
		@kCB_ViveEn = ViveEn , 
		@kCB_ViveCon = ViveCon , 
		@kCB_MED_TRA = MedioTransporte , 
		@kCB_Estudio = Estudio , 
		@kCB_AUTOSAL_GLOBAL = AutoSal , 
		@kCB_Zona = ZonaGeografica , 
		@kCB_Patron = Patron , 
		@kCB_Salario = Salario , 
		@kCB_Contrat = Contrato , 
		@kCB_Puesto = Puesto , 
		@kCB_Clasifi = Clasifi , 
		@kCB_Turno = Turno , 
		@kCB_TablaSS = TablaSS , 
		@kCB_Nivel1 = Nivel1 , 
		@kCB_Nivel2 = Nivel2 , 
		@kCB_Nivel3 = Nivel3 , 
		@kCB_Nivel4 = Nivel4 , 
		@kCB_Nivel5 = Nivel5 , 
		@kCB_Nivel6 = Nivel6 , 
		@kCB_Nivel7 = Nivel7 , 
		@kCB_Nivel8 = Nivel8 , 
		@kCB_Nivel9 = Nivel9 , 
		@kCB_Regimen = Regimen , 
		@KCB_Banco = Banco 
	from dbo.WF_GetGlobalesEmpleado() 

	SET	@ClienteID	= coalesce(  @Cargo.value('(//REGISTRO/@ClienteID)[1]', 'INT' ), 0) 
	SET @EMPRESA	= dbo.WFEmpresas_GetCodigoEmpresa( @ClienteID ) ; 
	
	SET	@PersonaID	= coalesce(  @Cargo.value('(//REGISTRO/@PersonaID)[1]', 'INT' ),  @TransID )     	
	
	--- IDENTIFICACION DEL EMPLEADO 
	SET	@KCB_APE_PAT		= coalesce( @Cargo.value('(//REGISTRO/@PersonaApePat)[1]', 'Varchar(256)' ), '' ) 
	SET	@KCB_APE_MAT		= coalesce( @Cargo.value('(//REGISTRO/@PersonaApeMat)[1]', 'Varchar(256)' ), '' ) 
	SET	@KCB_NOMBRES		= coalesce( @Cargo.value('(//REGISTRO/@PersonaNombres)[1]', 'Varchar(256)' ), '' ) 	
	SET @kCB_FEC_NAC  = coalesce(@Cargo.value('(//REGISTRO/@PersonaNacFecha)[1]', 'Date' ), '1899-12-30' ) 	
	SET	@KCB_RFC		= coalesce( @Cargo.value('(//REGISTRO/@PersonaRFC)[1]', 'Varchar(256)' ), '' ) 
	SET	@KCB_CURP		= coalesce( @Cargo.value('(//REGISTRO/@PersonaCURP)[1]', 'Varchar(256)' ), '' ) 
	SET	@kCB_SEGSOC		= coalesce( @Cargo.value('(//REGISTRO/@PersonaIMSS)[1]', 'Varchar(256)' ), '' ) 	
	SET	@KCB_SEXO		=case ( coalesce( @Cargo.value('(//REGISTRO/@PersonaGenero)[1]', 'int' ), -1 ) ) 
									when  1 then 'M' 
	                                when  2 then 'F'
									else  @KCB_SEXO 
						end;  
	SET @kCB_ENT_NAC = coalesce( @Cargo.value('(//REGISTRO/@NacEstadoCodigo)[1]', 'Varchar(2)' ), '' ) -- as Codigo2
	
	-- PERSONALES  
	SET @kCB_LUG_NAC   = coalesce(@Cargo.value('(//REGISTRO/@PersonaNacCiudad)[1]', 'Varchar(30)' ), '' ) 
	SET @kCB_NACION    = coalesce(@Cargo.value('(//REGISTRO/@PersonaNacionalidad)[1]', 'Varchar(30)' ), '' ) 	
	SET	@kCB_PASAPOR_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaPasaporte)[1]', 'INT' ), 0)	
	if ( @kCB_PASAPOR_INT = 1 ) 
		Set @kCB_PASAPOR = 'S' 
	else
		Set @kCB_PASAPOR = 'N'
	SET @kCB_EDO_CIV = coalesce( @Cargo.value('(//REGISTRO/@EdoCivilCodigo)[1]', 'Varchar(1)' ), @kCB_EDO_CIV ) --Estado Civil
	SET @kCB_LA_MAT = coalesce( @Cargo.value('(//REGISTRO/@PersonaLugarMat)[1]', 'Varchar(30)' ), '' ) --Lugar y Fecha de matrimonio
	SET @kNOM_PADRE = coalesce( @Cargo.value('(//REGISTRO/@PersonaNomPad)[1]', 'Varchar(30)' ), '' )
	SET @kNOM_MADRE = coalesce( @Cargo.value('(//REGISTRO/@PersonaNomMad)[1]', 'Varchar(30)' ), '' )
	SET @kCB_MED_TRA = coalesce( @Cargo.value('(//REGISTRO/@TransporteCodigo)[1]', 'Varchar(1)' ), @kCB_MED_TRA  ) --Transporte	
	SET	@kCB_DISCAPA_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaSufreDiscapa)[1]', 'INT' ), 0)	
	if ( @kCB_DISCAPA_INT = 1 ) 
		Set @kCB_DISCAPA = 'S' 
	else
		Set @kCB_DISCAPA = 'N'
	SET @kCB_TDISCAP = coalesce(@Cargo.value('(//REGISTRO/@PersonaDiscapa)[1]', 'INT' ), 0) -- as Status		
	--SET @kCB_INDIGE as Booleano
	SET	@kCB_INDIGE_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaIndigena)[1]', 'INT' ), 0)	
	if ( @kCB_INDIGE_INT = 1 ) 
		Set @kCB_INDIGE = 'S' 
	else
		Set @kCB_INDIGE = 'N'
		
	-- DOMICILIO 
	SET @kCB_CALLE = coalesce( @Cargo.value('(//REGISTRO/@DomicilioCalle)[1]', 'Varchar(50)' ), '' ) 
	SET @kCB_NUM_EXT = coalesce( @Cargo.value('(//REGISTRO/@DomicilioNumExt)[1]', 'Varchar(10)' ), '' ) 
	SET @kCB_NUM_INT = coalesce( @Cargo.value('(//REGISTRO/@DomicilioNumInt)[1]', 'Varchar(10)' ), '' ) 
	SET @kCB_COLONIA = coalesce( @Cargo.value('(//REGISTRO/@DomicilioColonia)[1]', 'Varchar(30)' ), '' ) 
	SET @kCB_CODPOST = coalesce( @Cargo.value('(//REGISTRO/@DomicilioCodPost)[1]', 'Varchar(8)' ), '' ) 
	SET @kCB_COD_COL = coalesce( @Cargo.value('(//REGISTRO/@ColoniaCodigo)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_CLINICA = coalesce( @Cargo.value('(//REGISTRO/@Clinica)[1]', 'Varchar(3)' ), '' ) -- as Codigo3
	SET @kCB_ZONA 	 =  dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@DomicilioZona)[1]', 'Varchar(8)' ), @kCB_ZONA )  -- as Referencia	
	SET	@KCB_CIUDAD	 =  dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@DomicilioCiudad)[1]', 'Varchar(30)' ), @kCB_Ciudad ) 	
	SET	@KCB_ESTADO	 =  dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@EstadoCodigo)[1]', 'Varchar(6)' ), @kCB_ESTADO ) 
	SET @kCB_MUNICIP =  dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@MunicipioCodigo)[1]', 'Varchar(6)' ), @kCB_MUNICIP ) 
	
	-- CONTACTO 
	SET @kCB_TEL = coalesce( @Cargo.value('(//REGISTRO/@PersonaTelefono)[1]', 'Varchar(30)' ), '' ) 
	SET @kCB_E_MAIL = coalesce( @Cargo.value('(//REGISTRO/@PersonaEmail)[1]', 'Varchar(255)' ), '' ) 
	SET @kCB_VIVECON = coalesce( @Cargo.value('(//REGISTRO/@ViveConCodigo)[1]', 'Varchar(1)' ), @kCB_ViveCon  )  -- as Codigo1
	SET @kCB_VIVEEN = coalesce( @Cargo.value('(//REGISTRO/@ViveEnCodigo)[1]', 'Varchar(2)' ),  @kCB_VIVEEN )  -- as Codigo2
	SET @kCB_FEC_RES = coalesce(@Cargo.value('(//REGISTRO/@ResidenciaFecha)[1]', 'Date' ), '1899-12-30' ) --Fecha Residencia 	

	--- EXPERIENCIA  
	-- Exp. Academica 
	SET @kCB_ESTUDIO =  dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@EstudioCodigo)[1]', 'Varchar(2)' ),  @kCB_VIVEEN )  -- as Codigo2
	SET @kCB_ESCUELA =  coalesce( @Cargo.value('(//REGISTRO/@PersonaEscuela)[1]', 'Varchar(40)' ), '' )	
	SET @kCB_TESCUEL = coalesce(@Cargo.value('(//REGISTRO/@PersonaEstTipo)[1]', 'INT' ), 0) -- as Status
	SET @kCB_CARRERA  = coalesce( @Cargo.value('(//REGISTRO/@PersonaCarrera)[1]', 'Varchar(40)' ), '' ) 		
	SET @kCB_TITULO = coalesce(@Cargo.value('(//REGISTRO/@PersonaTitulo)[1]', 'INT' ), 0) -- as Anio
	SET @kCB_YTITULO = coalesce(@Cargo.value('(//REGISTRO/@PersonaEstYear)[1]', 'INT' ), 0) -- as Anio
	SET @kCB_EST_HOR = coalesce( @Cargo.value('(//REGISTRO/@PersonaEstudiaHorario)[1]', 'Varchar(30)' ), '' ) --Carrera y Horario
	---DECLARE @kCB_EST_HOY as Booleano
	SET	@kCB_EST_HOY_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaEstudia)[1]', 'INT' ), 0)	
	if ( @kCB_EST_HOY_INT = 1 ) 
		Set @kCB_EST_HOY = 'S' 
	else
		Set @kCB_EST_HOY = 'N'

	-- Habilidades 
	SET	@kCB_HABLA_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaOtroIdioma)[1]', 'INT' ), 0)	
	if ( @kCB_HABLA_INT = 1 ) 
		Set @kCB_HABLA = 'S' 
	else
		Set @kCB_HABLA = 'N'
	SET @kCB_IDIOMA = coalesce( @Cargo.value('(//REGISTRO/@PersonaIdiomas)[1]', 'Varchar(30)' ), '' ) --Idioma o dominio
	SET @kCB_MAQUINA = coalesce( @Cargo.value('(//REGISTRO/@PersonaMaquinas)[1]', 'Varchar(100)' ), '' ) --Maquinas que conoce
	SET @kCB_EXPERIE = coalesce( @Cargo.value('(//REGISTRO/@PersonaExperiencia)[1]', 'Varchar(100)' ), '' )--Experiencia
	
	
	-- Contratacion 	
	SET @kCB_FEC_ING  = coalesce(@Cargo.value('(//REGISTRO/@EmpIngresoFecha)[1]', 'Date' ), '1899-12-30' ) 	
	SET @kCB_FEC_ANT  = coalesce(@Cargo.value('(//REGISTRO/@EmpAntigFecha)[1]', 'Date' ), '1899-12-30' ) 	
	
	-- Contrato 
	SET @kCB_FEC_CON  = coalesce(@Cargo.value('(//REGISTRO/@EmpContratoIni)[1]', 'Date' ), @kCB_FEC_ING ) 	
	SET	@KCB_CONTRAT  =  dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@ContratoCodigo)[1]', 'Varchar(6)' ), @KCB_CONTRAT ) 
		--Calcular el numero de dias para la fecha de vencimiento del contrato
		declare @DiasContrato as FolioChico
		SELECT @DiasContrato = TB_DIAS FROM CONTRATO WHERE TB_CODIGO = @kCB_CONTRAT
		if ( @DiasContrato > 0 )
		begin		 
			set @kCB_FEC_COV = cast( dateadd( day, @DiasContrato -1, @kCB_FEC_CON ) as Datetime ) 
		end
	
		
	SET	@KCB_PUESTO		= coalesce( @Cargo.value('(//REGISTRO/@PuestoCodigo)[1]', 'Varchar(6)' ), '' ) 
	SET @kCB_NOMINA 	= coalesce(@Cargo.value('(//REGISTRO/@TipoNominaCodigo)[1]', 'INT' ), @kCB_NOMINA) -- as NominaTipo
	
	-- Tomados del puesto en caso de que no esten definidos 
	----------------- TOMADOS DEL PUESTO  --------------------------------
	declare @kPU_PATRON Codigo1 
	declare @kPU_TURNO Codigo 
	declare @kPU_AUTOSAL Codigo 
	declare @kPU_TABLASS Codigo 
	declare @KPU_CLASIFI Codigo 
	
	select @KPU_CLASIFI = PU_CLASIFI, @kPU_PATRON = PU_PATRON, @kPU_TURNO = PU_TURNO,  @kPU_AUTOSAL  = PU_AUTOSAL, @kPU_TABLASS = PU_TABLASS from PUESTO where PU_CODIGO = @kCB_PUESTO 

	-- Si no estan definidas en el puesto tomas las default 
	set @kCB_AUTOSAL_GLOBAL = @KCB_AUTOSAL;
    set @kCB_PATRON  = case  coalesce(@kPU_PATRON,'')   when '' then @kCB_PATRON   else @kPU_PATRON  end
	set @kCB_TURNO = case  coalesce(@kPU_TURNO,'')  when '' then @kCB_TURNO  else @kPU_TURNO end	
	set @kCB_TABLASS = case  coalesce(@kPU_TABLASS,'')  when '' then @kCB_TABLASS  else @kPU_TABLASS end
	set @kCB_CLASIFI = case  coalesce(@KPU_CLASIFI,'')  when '' then @kCB_CLASIFI  else @KPU_CLASIFI end
	set @kCB_CLASIFI = coalesce( @kCB_CLASIFI, '' ); 

    -- Al ultimo lee los del Cargo 	
	SET	@KCB_CLASIFI 	= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@TabuladorCodigo)[1]', 'Varchar(6)' ), @KCB_CLASIFI ) 
	SET	@KCB_TURNO		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@TurnoCodigo)[1]', 'Varchar(6)' ), @KCB_TURNO ) 
	SET	@KCB_PATRON		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@RegPatronCodigo)[1]', 'Varchar(1)' ), @KCB_PATRON ) 	
		
	-- TimbradoNomina 
	SET @kCB_REGIMEN = coalesce(@Cargo.value('(//REGISTRO/@RegimenSAT)[1]', 'INT' ), @kCB_REGIMEN) -- as Status
	
	
	-- AREA 
	SET	@kCB_NIVEL1		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel1Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL1 ) 
	SET	@kCB_NIVEL2		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel2Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL2 ) 
	SET	@kCB_NIVEL3		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel3Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL3 ) 
	SET	@kCB_NIVEL4		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel4Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL4 ) 
	SET	@kCB_NIVEL5		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel5Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL5 ) 
	SET	@kCB_NIVEL6		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel6Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL6 ) 
	SET	@kCB_NIVEL7		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel7Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL7 ) 
	SET	@kCB_NIVEL8		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel8Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL8 ) 
	SET	@kCB_NIVEL9		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel9Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL9 ) 
	
	
    -- SALARIO 	
	SET	@KCB_TABLASS		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@PrestaLeyCodigo)[1]', 'Varchar(6)' ), @KCB_TABLASS ) 	
	SET	@KCB_AUTOSAL	=case coalesce( @Cargo.value('(//REGISTRO/@SalTabula)[1]', 'int' ),  -1 ) 
								    when  0 then 'N'
									when  1 then 'S'
									else @KCB_AUTOSAL
						end;	
	SET	@kCB_SALARIO	= coalesce( @Cargo.value('(//REGISTRO/@SalDiario)[1]', 'Decimal(15,2)' ), @kCB_SALARIO ) 
	SET @kCB_PER_VAR    = coalesce( @Cargo.value('(//REGISTRO/@PromVar)[1]', 'Decimal(15,2)' ), @kCB_SALARIO ) 
	SET	@KCB_ZONA_GE	= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@ZonaGeo)[1]', 'Varchar(1)' ), 'A' ) 	
	
	
	-- Datos del movimiento 
	SET @KarFecha  = coalesce(@Cargo.value('(//REGISTRO/@EmpIngresoFecha)[1]', 'Date' ), '1899-12-30' ) 		
	SET @KarObservaciones  = coalesce(@Cargo.value('(//REGISTRO/@KarObservaciones)[1]', 'Varchar(255)' ), '' ) 
	SET	@FileID	= coalesce(@Cargo.value('(//REGISTRO/@FileID)[1]', 'INT' ), 0)	
	
	SET	@KCB_CHECA		=case ( coalesce( @Cargo.value('(//REGISTRO/@PersonaCheca)[1]', 'int' ), -1 ) ) 
	                                when  0 then 'N'
									when  1 then 'S'
									else @KCB_CHECA
						end;  




	
	
	-----Campos que faltaban de mapear del XML "CargoAltaEmpleado.xml"  -------
	SET @kCB_CTA_VAL = coalesce( @Cargo.value('(//REGISTRO/@PersonaVale)[1]', 'Varchar(30)' ), '' )
	

	---- Campos Faltantes que no se encontraban en el XML "CargoAltaEmpleado.xml" para futuras Altas
	SET @kCB_AR_FEC = coalesce(@Cargo.value('(//REGISTRO/@PersonaArFec)[1]', 'Date' ), '1899-12-30' ) 	
	SET @kCB_AR_HOR = coalesce( @Cargo.value('(//REGISTRO/@PersonaArHor)[1]', 'Varchar(4)' ), '' )
	
	
	
	SET @kCB_EVALUA = coalesce( @Cargo.value('(//REGISTRO/@PersonaEvalua)[1]', 'Decimal(15,2)' ), 0.00 ) --Resultado Evaluacion
	
	SET @kCB_FEC_BAJ = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecBaj)[1]', 'Date' ), '1899-12-30' ) --Fecha Baja	
	SET @kCB_FEC_BSS = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecBss)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_FEC_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecInt)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_FEC_REV = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecRev)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_FEC_VAC = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecVac)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_1 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec1)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_2 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec2)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_3 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec3)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_4 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec4)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_5 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec5)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_6 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec6)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_7 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec7)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_8 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec8)[1]', 'Date' ), '1899-12-30' )
	
	----------------------------------------- VALORES ADICIONALES DE TRESS ---------------------------	
	--DECLARE @kCB_G_LOG_1 as Booleano
	SET	@kCB_G_LOG_1_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog1)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_1_INT = 1 ) 
		Set @kCB_G_LOG_1 = 'S' 
	else
		Set @kCB_G_LOG_1 = 'N'
	--DECLARE @kCB_G_LOG_2 as Booleano
	SET	@kCB_G_LOG_2_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog2)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_2_INT = 1 ) 
		Set @kCB_G_LOG_2 = 'S' 
	else
		Set @kCB_G_LOG_2 = 'N' 
	--DECLARE @kCB_G_LOG_3 as Booleano
	SET	@kCB_G_LOG_3_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog3)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_3_INT = 1 ) 
		Set @kCB_G_LOG_3 = 'S' 
	else
		Set @kCB_G_LOG_3 = 'N' 
	--DECLARE @kCB_G_LOG_4 as Booleano
	SET	@kCB_G_LOG_4_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog4)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_4_INT = 1 ) 
		Set @kCB_G_LOG_4 = 'S' 
	else
		Set @kCB_G_LOG_4 = 'N' 
	--DECLARE @kCB_G_LOG_5 as Booleano
	SET	@kCB_G_LOG_5_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog5)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_5_INT = 1 ) 
		Set @kCB_G_LOG_5 = 'S' 
	else
		Set @kCB_G_LOG_5 = 'N' 
	--DECLARE @kCB_G_LOG_6 as Booleano
	SET	@kCB_G_LOG_6_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog6)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_6_INT = 1 ) 
		Set @kCB_G_LOG_6 = 'S' 
	else
		Set @kCB_G_LOG_6 = 'N' 
	--DECLARE @kCB_G_LOG_7 as Booleano
	SET	@kCB_G_LOG_7_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog7)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_7_INT = 1 ) 
		Set @kCB_G_LOG_7 = 'S' 
	else
		Set @kCB_G_LOG_7 = 'N' 
	--DECLARE @kCB_G_LOG_8 as Booleano
	SET	@kCB_G_LOG_8_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog8)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_8_INT = 1 ) 
		Set @kCB_G_LOG_8 = 'S' 
	else
		Set @kCB_G_LOG_8 = 'N' 

	SET @kCB_G_NUM_1 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum1)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_2 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum2)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_3 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum3)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_4 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum4)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_5 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum5)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_6 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum6)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_7 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum7)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_8 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum8)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_9 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum9)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM10 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum10)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM11 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum11)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM12 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum12)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM13 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum13)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM14 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum14)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM15 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum15)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM16 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum16)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM17 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum17)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM18 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum18)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_TAB_1 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab1)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_2 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab2)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_3 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab3)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_4 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab4)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_5 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab5)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_6 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab6)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_7 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab7)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_8 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab8)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_9 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab9)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB10 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab10)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB11 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab11)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB12 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab12)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB13 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab13)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB14 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab14)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TEX_1  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText1)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_2  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText2)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_3  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText3)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_4  = coalesce( @Cargo.value('(//REGISTRO/@PersonaCLABE)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_5  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText5)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_6  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText6)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_7  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText7)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_8  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText8)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_9  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText9)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX10  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText10)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX11  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText11)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX12  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText12)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX13  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText13)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX14  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText14)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX15  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText15)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX16  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText16)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX17  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText17)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX18  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText18)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX19  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText19)[1]', 'Varchar(30)' ), '' ) -- as Descripcion		
	SET @kCB_G_TEX20  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText20)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX21  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText21)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX22  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText22)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX23  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText23)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX24  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText24)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	
	SET @kCB_INFCRED = coalesce( @Cargo.value('(//REGISTRO/@PersonaInfCred)[1]', 'Varchar(30)' ), '' ) --Credito Infonavit 
	---DECLARE @kCB_INFMANT as Booleano 
	SET	@kCB_INFMANT_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaInfMant)[1]', 'INT' ), 0)	
	if ( @kCB_INFMANT_INT = 1 ) 
		Set @kCB_INFMANT = 'S' 
	else
		Set @kCB_INFMANT = 'N'

	SET @kCB_INFTASA = coalesce( @Cargo.value('(//REGISTRO/@PersonaInfTasa)[1]', 'Decimal(15,2)' ), 0.00 ) --Valor Descuento Infonavit 
	
	SET @kCB_LAST_EV = coalesce(@Cargo.value('(//REGISTRO/@PersonaLastEv)[1]', 'Date' ), '1899-12-30' ) --Fecha de ultima evaluacion
	
	
	SET @kCB_FEC_INC = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecInc)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_FEC_PER = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecPer)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_NOMYEAR = coalesce(@Cargo.value('(//REGISTRO/@PersonaNomYear)[1]', 'INT' ), 0)
	SET @kCB_NOMTIPO = coalesce(@Cargo.value('(//REGISTRO/@PersonaNomTipo)[1]', 'INT' ), 0)
	

	SET @kCB_NEXT_EV = coalesce(@Cargo.value('(//REGISTRO/@PersonaNextEv)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_NOMNUME = coalesce(@Cargo.value('(//REGISTRO/@PersonaNomNume)[1]', 'INT' ), 0) -- as NominaNumero
	SET @kCB_SAL_INT = coalesce( @Cargo.value('(//REGISTRO/@PersonaSalInt)[1]', 'Decimal(15,2)' ), 0.00 ) -- as PesosDiario
	
	SET @kCB_INFTIPO = coalesce(@Cargo.value('(//REGISTRO/@PersonaNomNume)[1]', 'INT' ), 0)  -- as Status
	SET @kCB_DER_FEC = coalesce(@Cargo.value('(//REGISTRO/@PersonaDerFec)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_DER_PAG = coalesce( @Cargo.value('(//REGISTRO/@PersonaDerPag)[1]', 'Decimal(15,2)' ), 0.00 )  -- as DiasFrac
	SET @kCB_V_PAGO = coalesce( @Cargo.value('(//REGISTRO/@PersonaVPago)[1]', 'Decimal(15,2)' ), 0.00 )  -- as DiasFrac
	SET @kCB_DER_GOZ = coalesce( @Cargo.value('(//REGISTRO/@PersonaDerGoz)[1]', 'Decimal(15,2)' ), 0.00 )  -- as DiasFrac
	SET @kCB_V_GOZO = coalesce( @Cargo.value('(//REGISTRO/@PersonaVGozo)[1]', 'Decimal(15,2)' ), 0.00 )  -- as DiasFrac


	SET @kCB_TIP_REV = coalesce( @Cargo.value('(//REGISTRO/@PersonaTipRev)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_MOT_BAJ = coalesce( @Cargo.value('(//REGISTRO/@PersonaMotBaj)[1]', 'Varchar(3)' ), '' ) -- as Codigo3
	SET @kCB_FEC_SAL = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecSal)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_INF_INI = coalesce(@Cargo.value('(//REGISTRO/@PersonaInfIni)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_INF_OLD = coalesce( @Cargo.value('(//REGISTRO/@PersonaInfOld)[1]', 'Decimal(15,2)' ), 0.00 )  -- as DescINFO
	SET @kCB_OLD_SAL = coalesce( @Cargo.value('(//REGISTRO/@PersonaOldSal)[1]', 'Decimal(15,2)' ), 0.00 )  -- as PesosDiario
	SET @kCB_OLD_INT = coalesce( @Cargo.value('(//REGISTRO/@PersonaOldInt)[1]', 'Decimal(15,2)' ), 0.00 )  -- as PesosDiario
	SET @kCB_PRE_INT = coalesce( @Cargo.value('(//REGISTRO/@PersonaPreInt)[1]', 'Decimal(15,2)' ), 0.00 )  -- as PesosDiario
	SET @kCB_SAL_TOT = coalesce( @Cargo.value('(//REGISTRO/@PersonaSalTot)[1]', 'Decimal(15,2)' ), 0.00 )  -- as PesosDiario
	SET @kCB_TOT_GRA = coalesce( @Cargo.value('(//REGISTRO/@PersonaTotGra)[1]', 'Decimal(15,2)' ), 0.00 )  -- as PesosDiario
	SET @kCB_FAC_INT = coalesce( @Cargo.value('(//REGISTRO/@PersonaFacInt)[1]', 'Decimal(15,2)' ), 0.00 )  -- as Tasa
	SET @kCB_RANGO_S = coalesce( @Cargo.value('(//REGISTRO/@PersonaRangoS)[1]', 'Decimal(15,2)' ), 0.00 )  -- as Tasa
	
	SET @kCB_FEC_NIV = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecNiv)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_FEC_PTO = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecPto)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_AREA = coalesce( @Cargo.value('(//REGISTRO/@PersonaArea)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_FEC_TUR = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecTur)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_FEC_KAR = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecKar)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_PENSION = coalesce(@Cargo.value('(//REGISTRO/@PersonaPension)[1]', 'INT' ), 0) -- as Status

	SET @kCB_CANDIDA = coalesce(@Cargo.value('(//REGISTRO/@PersonaCandida)[1]', 'INT' ), 0) -- as FolioGrande
	SET @kCB_ID_NUM = coalesce( @Cargo.value('(//REGISTRO/@PersonaIdNum)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	
	
	SET @kCB_PASSWRD = coalesce( @Cargo.value('(//REGISTRO/@PersonaPasswrd)[1]', 'Varchar(30)' ), '' ) -- as Passwd
	SET @kCB_PLAZA = coalesce(@Cargo.value('(//REGISTRO/@PersonaPlaza)[1]', 'INT' ), 0) -- as FolioGrande
	SET @kCB_FEC_PLA = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecPla)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_DER_PV = coalesce( @Cargo.value('(//REGISTRO/@PersonaDerPv)[1]', 'Decimal(15,2)' ), 0.00 ) -- as DiasFrac
	SET @kCB_V_PRIMA = coalesce( @Cargo.value('(//REGISTRO/@PersonaVPrima)[1]', 'Decimal(15,2)' ), 0.00 ) -- as DiasFrac
	SET @kCB_SUB_CTA = coalesce( @Cargo.value('(//REGISTRO/@PersonaSubCta)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_NETO = coalesce( @Cargo.value('(//REGISTRO/@PersonaNeto)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	
	--SET @kCB_PORTAL as Booleano
	SET	@kCB_PORTAL_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaPortal)[1]', 'INT' ), 0)	
	if ( @kCB_PORTAL_INT = 1 ) 
		Set @kCB_PORTAL = 'S' 
	else
		Set @kCB_PORTAL = 'N' 
	--SET @kCB_DNN_OK as Booleano
	SET	@kCB_DNN_OK_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaDnnOk)[1]', 'INT' ), 0)	
	if ( @kCB_DNN_OK_INT = 1 ) 
		Set @kCB_DNN_OK = 'S' 
	else
		Set @kCB_DNN_OK = 'N' 

	SET @kCB_USRNAME = coalesce( @Cargo.value('(//REGISTRO/@PersonaUsrname)[1]', 'Varchar(40)' ), '' ) -- as DescLarga
	
	SET @kCB_FEC_NOM = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecNom)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	--SET @kCB_RECONTR as Booleano
	SET	@kCB_RECONTR_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaRecontr)[1]', 'INT' ), 0)	
	if ( @kCB_RECONTR_INT = 1 ) 
		Set @kCB_RECONTR = 'S' 
	else
		Set @kCB_RECONTR = 'N'

		
	SET @kCB_FONACOT = coalesce( @Cargo.value('(//REGISTRO/@PersonaFonacot)[1]', 'Varchar(30)' ), '' ) -- as Descripcion

	--SET @kCB_EMPLEO as Booleano
	SET	@kCB_EMPLEO_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaEmpleo)[1]', 'INT' ), 0)	
	if ( @kCB_EMPLEO_INT = 1 ) 
		Set @kCB_EMPLEO = 'S' 
	else
		Set @kCB_EMPLEO = 'N'

	SET @kUS_CODIGO = coalesce(@Cargo.value('(//REGISTRO/@PersonaUsCodigo)[1]', 'INT' ), 0) -- as Usuario
	
	--SET @kCB_INFDISM as Booleano
	SET	@kCB_INFDISM_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaInfDism)[1]', 'INT' ), 0)	
	if ( @kCB_INFDISM_INT = 1 ) 
		Set @kCB_INFDISM = 'S' 
	else
		Set @kCB_INFDISM = 'N'

	--SET @kCB_INFACT as Booleano
	SET	@kCB_INFACT_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaInfAct)[1]', 'INT' ), 0)	
	if ( @kCB_INFACT_INT = 1 ) 
		Set @kCB_INFACT = 'S' 
	else
		Set @kCB_INFACT = 'N'
	
	SET @kCB_INF_ANT = coalesce(@Cargo.value('(//REGISTRO/@PersonaInfAnt)[1]', 'Date' ), '1899-12-30' ) -- as Fecha 
	
	SET @kCB_CTA_GAS = coalesce( @Cargo.value('(//REGISTRO/@PersonaCtaGas)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_ID_BIO = coalesce(@Cargo.value('(//REGISTRO/@PersonaIdBio)[1]', 'INT' ), 0) -- as FolioGrande
	SET @kCB_GP_COD = coalesce( @Cargo.value('(//REGISTRO/@PersonaGpCod)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	--SET @kCB_TIE_PEN as Booleano
	SET	@kCB_TIE_PEN_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaTiePen)[1]', 'INT' ), 0)	
	if ( @kCB_TIE_PEN_INT = 1 ) 
		Set @kCB_TIE_PEN = 'S' 
	else
		Set @kCB_TIE_PEN = 'N'

	SET @kCB_TSANGRE = coalesce( @Cargo.value('(//REGISTRO/@PersonaTSangre)[1]', 'Varchar(6)' ), '' ) -- as Descripcion
	SET @kCB_ALERGIA = coalesce( @Cargo.value('(//REGISTRO/@PersonaAlergia)[1]', 'Varchar(255)' ), '' ) -- as Formula

	--SET @kCB_BRG_ACT as Booleano
	SET	@kCB_BRG_ACT_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaBrgAct)[1]', 'INT' ), 0)	
	if ( @kCB_BRG_ACT_INT = 1 ) 
		Set @kCB_BRG_ACT = 'S' 
	else
		Set @kCB_BRG_ACT = 'N'

	SET @kCB_BRG_TIP = coalesce(@Cargo.value('(//REGISTRO/@PersonaBgrTip)[1]', 'INT' ), 0) -- as Status
	--SET @kCB_BRG_ROL as Booleano
	SET	@kCB_BRG_ROL_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaBrgRol)[1]', 'INT' ), 0)	
	if ( @kCB_BRG_ROL_INT = 1 ) 
		Set @kCB_BRG_ROL = 'S' 
	else
		Set @kCB_BRG_ROL = 'N'

	--SET @kCB_BRG_JEF as Booleano
	SET	@kCB_BRG_JEF_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaBrgJef)[1]', 'INT' ), 0)	
	if ( @kCB_BRG_JEF_INT = 1 ) 
		Set @kCB_BRG_JEF = 'S' 
	else
		Set @kCB_BRG_JEF = 'N'

	--SET @kCB_BRG_CON as Booleano
	SET	@kCB_BRG_CON_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaBrgCon)[1]', 'INT' ), 0)	
	if ( @kCB_BRG_CON_INT = 1 ) 
		Set @kCB_BRG_CON = 'S' 
	else
		Set @kCB_BRG_CON = 'N'
	--SET @kCB_BRG_PRA as Booleano
	SET	@kCB_BRG_PRA_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaBrgPra)[1]', 'INT' ), 0)	
	if ( @kCB_BRG_PRA_INT = 1 ) 
		Set @kCB_BRG_PRA = 'S' 
	else
		Set @kCB_BRG_PRA = 'N'

	SET @kCB_BRG_NOP = coalesce( @Cargo.value('(//REGISTRO/@PersonaBgrNop)[1]', 'Varchar(6)' ), '' ) -- as Descripcion
	SET @kCB_BANCO = coalesce( @Cargo.value('(//REGISTRO/@PersonaBanco)[1]', 'Varchar(6)' ), @kCB_BANCO ) -- as Codigo
	
	
    if ( @ErrorID = 0 ) 
    begin 

	
	-- Si ya existe el empleado con los mismos nombres, rfc, fecha de Persona y correo  define el mismo numero de empleado 
	-- para que la validacion la realice el WFTress.dll 

	set @kCB_CODIGO = 0 
	set @kCB_SALARIO_BRUTO = 0 

	select @kCB_CODIGO = coalesce(@Cargo.value('(//REGISTRO/@EmpNumero)[1]', 'INT' ), 0)	

	--coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) 


	set @kCB_CODIGO = coalesce (@kCB_CODIGO ,  0 ) 

	-- Se obtiene el numero de empleado mayor + 1 
	if ( @kCB_CODIGO = 0 ) 
		SELECT @kCB_CODIGO = CB_CODIGO + 1  FROM dbo.ULTIMO_EMPLEADO

	
	 set @Xml =(	 

	   select MOV3= 'REINGRESO_EMPLEADO'		, 
			@EMPRESA as EMPRESA,  ( 
	   select * from  ( 
		 SELECT 		
		        @PersonaID  as IngresoID , 
				@kCB_CODIGO  as CB_CODIGO  , 
				--'S'  as CB_ACTIVO,		
				@kCB_AUTOSAL as CB_AUTOSAL , --Booleano
				@kCB_APE_MAT as CB_APE_MAT , --Descripcion
				@kCB_APE_PAT as CB_APE_PAT , --Descripcion
				@kCB_NOMBRES as CB_NOMBRES , --Descripcion
				--@kCB_BAN_ELE as CB_BAN_ELE , --Descripcion Se Quita por peticion de Roch
				@kCB_CALLE as CB_CALLE , --Observaciones
				@kCB_CARRERA as CB_CARRERA , --DescLarga
				@kCB_CHECA as CB_CHECA , --Booleano
				@kCB_CIUDAD as CB_CIUDAD , --Descripcion
				@kCB_CLASIFI as CB_CLASIFI , --Codigo
				@kCB_CODPOST as CB_CODPOST , --Referencia
				@kCB_COLONIA as CB_COLONIA , --Descripcion
				@kCB_CONTRAT as CB_CONTRAT , --Codigo1
				@kCB_CREDENC as CB_CREDENC , --Codigo1
				@kCB_CTA_VAL as CB_CTA_VAL , --Descripcion
				@kCB_CURP as CB_CURP , --Descripcion
				@kCB_E_MAIL as CB_E_MAIL , --Formula
				@kCB_ESTADO as CB_ESTADO , --Codigo2
				@kCB_ESTUDIO as CB_ESTUDIO , --Codigo2
				convert( varchar(20), @kCB_FEC_ANT , 112 )  as CB_FEC_ANT , --Fecha
				convert( varchar(20), @kCB_FEC_CON , 112 )  as CB_FEC_CON , --Fecha
				convert( varchar(20), @kCB_FEC_COV , 112 )  as CB_FEC_COV , --Fecha
				convert( varchar(20), @kCB_FEC_ING , 112 )  as CB_FEC_ING , --Fecha
				convert( varchar(20), @kCB_FEC_NAC , 112 )  as CB_FEC_NAC , --Fecha
				@kCB_LUG_NAC as CB_LUG_NAC , --Descripcion
				@kCB_MUNICIP as CB_MUNICIP , --Codigo
				@kCB_NACION as CB_NACION , --Descripcion
				@kCB_NIVEL0 as CB_NIVEL0 , --Codigo
				@kCB_NIVEL1 as CB_NIVEL1 , --Codigo
				@kCB_NIVEL2 as CB_NIVEL2 , --Codigo
				@kCB_NIVEL3 as CB_NIVEL3 , --Codigo
				@kCB_NIVEL4 as CB_NIVEL4 , --Codigo
				@kCB_NIVEL5 as CB_NIVEL5 , --Codigo
				@kCB_NIVEL6 as CB_NIVEL6 , --Codigo
				@kCB_NIVEL7 as CB_NIVEL7 , --Codigo
				@kCB_NIVEL8 as CB_NIVEL8 , --Codigo
				@kCB_NIVEL9 as CB_NIVEL9 , --Codigo
				@kCB_PATRON as CB_PATRON , --RegPatronal
				@kCB_PUESTO as CB_PUESTO , --Codigo
				@kCB_RFC as CB_RFC , --Descripcion
				@kCB_SALARIO as CB_SALARIO , --PesosDiario
				@kCB_SALARIO_BRUTO as CB_SALARIO_BRUTO , --PesosDiario Salario Bruto Mensual
				@kCB_SEGSOC as CB_SEGSOC , --Descripcion
				@kCB_SEXO as CB_SEXO , --Codigo1
				@kCB_TABLASS as CB_TABLASS , --Codigo1
				@kCB_TEL as CB_TEL , --Descripcion
				@kCB_TURNO as CB_TURNO , --Codigo
				@kCB_ZONA_GE as CB_ZONA_GE , --ZonaGeo										
				---Nuevos Campos Mapeados
				@kCB_EDO_CIV as CB_EDO_CIV , -- Codigo1
				convert( varchar(20), @kCB_FEC_RES , 112 )  as CB_FEC_RES , -- Fecha
				@kCB_EST_HOR as CB_EST_HOR , -- Descripcion
				@kCB_EST_HOY as CB_EST_HOY , -- Booleano
				@kCB_EVALUA as CB_EVALUA , -- DiasFrac
				@kCB_EXPERIE as CB_EXPERIE , -- Titulo
				convert( varchar(20), @kCB_FEC_BAJ , 112 )    as CB_FEC_BAJ , -- Fecha
				convert( varchar(20), @kCB_FEC_BSS , 112 )    as CB_FEC_BSS , -- Fecha
				convert( varchar(20), @kCB_FEC_INT , 112 )    as CB_FEC_INT , -- Fecha
				convert( varchar(20), @kCB_FEC_REV , 112 )    as CB_FEC_REV , -- Fecha
				convert( varchar(20), @kCB_FEC_VAC , 112 )    as CB_FEC_VAC , -- Fecha
				@kCB_G_FEC_1 as CB_G_FEC_1 , -- Fecha
				@kCB_G_FEC_2 as CB_G_FEC_2 , -- Fecha
				@kCB_G_FEC_3 as CB_G_FEC_3 , -- Fecha
				@kCB_G_FEC_4 as CB_G_FEC_4 , -- Fecha
				@kCB_G_FEC_5 as CB_G_FEC_5 , -- Fecha
				@kCB_G_FEC_6 as CB_G_FEC_6 , -- Fecha
				@kCB_G_FEC_7 as CB_G_FEC_7 , -- Fecha
				@kCB_G_FEC_8 as CB_G_FEC_8 , -- Fecha
				@kCB_G_LOG_1 as CB_G_LOG_1 , -- Booleano
				@kCB_G_LOG_2 as CB_G_LOG_2 , -- Booleano
				@kCB_G_LOG_3 as CB_G_LOG_3 , -- Booleano
				@kCB_G_LOG_4 as CB_G_LOG_4 , -- Booleano
				@kCB_G_LOG_5 as CB_G_LOG_5 , -- Booleano
				@kCB_G_LOG_6 as CB_G_LOG_6 , -- Booleano
				@kCB_G_LOG_7 as CB_G_LOG_7 , -- Booleano
				@kCB_G_LOG_8 as CB_G_LOG_8 , -- Booleano
				@kCB_G_NUM_1 as CB_G_NUM_1 , -- Pesos
				@kCB_G_NUM_2 as CB_G_NUM_2 , -- Pesos
				@kCB_G_NUM_3 as CB_G_NUM_3 , -- Pesos
				@kCB_G_NUM_4 as CB_G_NUM_4 , -- Pesos
				@kCB_G_NUM_5 as CB_G_NUM_5 , -- Pesos
				@kCB_G_NUM_6 as CB_G_NUM_6 , -- Pesos
				@kCB_G_NUM_7 as CB_G_NUM_7 , -- Pesos
				@kCB_G_NUM_8 as CB_G_NUM_8 , -- Pesos
				@kCB_G_NUM_9 as CB_G_NUM_9 , -- Pesos
				@kCB_G_NUM10 as CB_G_NUM10 , -- Pesos
				@kCB_G_NUM11 as CB_G_NUM11 , -- Pesos
				@kCB_G_NUM12 as CB_G_NUM12 , -- Pesos
				@kCB_G_NUM13 as CB_G_NUM13 , -- Pesos
				@kCB_G_NUM14 as CB_G_NUM14 , -- Pesos
				@kCB_G_NUM15 as CB_G_NUM15 , -- Pesos
				@kCB_G_NUM16 as CB_G_NUM16 , -- Pesos
				@kCB_G_NUM17 as CB_G_NUM17 , -- Pesos
				@kCB_G_NUM18 as CB_G_NUM18 , -- Pesos
				@kCB_G_TAB_1 as CB_G_TAB_1 , -- Codigo
				@kCB_G_TAB_2 as CB_G_TAB_2 , -- Codigo
				@kCB_G_TAB_3 as CB_G_TAB_3 , -- Codigo
				@kCB_G_TAB_4 as CB_G_TAB_4 , -- Codigo
				@kCB_G_TAB_5 as CB_G_TAB_5 , -- Codigo
				@kCB_G_TAB_6 as CB_G_TAB_6 , -- Codigo
				@kCB_G_TAB_7 as CB_G_TAB_7 , -- Codigo
				@kCB_G_TAB_8 as CB_G_TAB_8 , -- Codigo
				@kCB_G_TAB_9 as CB_G_TAB_9 , -- Codigo
				@kCB_G_TAB10 as CB_G_TAB10 , -- Codigo
				@kCB_G_TAB11 as CB_G_TAB11 , -- Codigo
				@kCB_G_TAB12 as CB_G_TAB12 , -- Codigo
				@kCB_G_TAB13 as CB_G_TAB13 , -- Codigo
				@kCB_G_TAB14 as CB_G_TAB14 , -- Codigo
				@kCB_G_TEX_1 as CB_G_TEX_1 , -- Descripcion
				@kCB_G_TEX_2 as CB_G_TEX_2 , -- Descripcion
				@kCB_G_TEX_3 as CB_G_TEX_3 , -- Descripcion
				@kCB_G_TEX_4 as CB_G_TEX_4 , -- Descripcion
				@kCB_G_TEX_5 as CB_G_TEX_5 , -- Descripcion
				@kCB_G_TEX_6 as CB_G_TEX_6 , -- Descripcion
				@kCB_G_TEX_7 as CB_G_TEX_7 , -- Descripcion
				@kCB_G_TEX_8 as CB_G_TEX_8 , -- Descripcion
				@kCB_G_TEX_9 as CB_G_TEX_9 , -- Descripcion
				@kCB_G_TEX10 as CB_G_TEX10 , -- Descripcion
				@kCB_G_TEX11 as CB_G_TEX11 , -- Descripcion
				@kCB_G_TEX12 as CB_G_TEX12 , -- Descripcion
				@kCB_G_TEX13 as CB_G_TEX13 , -- Descripcion
				@kCB_G_TEX14 as CB_G_TEX14 , -- Descripcion
				@kCB_G_TEX15 as CB_G_TEX15 , -- Descripcion
				@kCB_G_TEX16 as CB_G_TEX16 , -- Descripcion
				@kCB_G_TEX17 as CB_G_TEX17 , -- Descripcion
				@kCB_G_TEX18 as CB_G_TEX18 , -- Descripcion
				@kCB_G_TEX19 as CB_G_TEX19 , -- Descripcion
				@kCB_HABLA as CB_HABLA , -- Booleano
				@kCB_IDIOMA as CB_IDIOMA , -- Descripcion
				@kCB_INFCRED as CB_INFCRED , -- Descripcion
				@kCB_INFMANT as CB_INFMANT , -- Booleano
				@kCB_INFTASA as CB_INFTASA , -- DescINFO
				@kCB_LA_MAT as CB_LA_MAT , -- Descripcion
				@kCB_LAST_EV as CB_LAST_EV , -- Fecha
				@kCB_MAQUINA as CB_MAQUINA , -- Titulo
				@kCB_MED_TRA as CB_MED_TRA , -- Codigo1
				@kCB_PASAPOR as CB_PASAPOR , -- Booleano				
				@kCB_VIVECON as CB_VIVECON , -- Codigo1
				@kCB_VIVEEN as CB_VIVEEN , -- Codigo2
				@kCB_ZONA as CB_ZONA , -- Referencia
				@kCB_TIP_REV as CB_TIP_REV , -- Codigo				
				@kCB_INF_INI as CB_INF_INI , -- Fecha
				@kCB_INF_OLD as CB_INF_OLD , -- DescINFO
				@kCB_OLD_SAL as CB_OLD_SAL , -- PesosDiario
				@kCB_OLD_INT as CB_OLD_INT , -- PesosDiario
				@kCB_PRE_INT as CB_PRE_INT , -- PesosDiario
				@kCB_PER_VAR as CB_PER_VAR , -- PesosDiario
				@kCB_SAL_TOT as CB_SAL_TOT , -- PesosDiario
				@kCB_TOT_GRA as CB_TOT_GRA , -- PesosDiario
				@kCB_FAC_INT as CB_FAC_INT , -- Tasa
				@kCB_RANGO_S as CB_RANGO_S , -- Tasa
				@kCB_CLINICA as CB_CLINICA , -- Codigo3																
				@kCB_CANDIDA as CB_CANDIDA , -- FolioGrande
				@kCB_ID_NUM as CB_ID_NUM , -- Descripcion
				@kCB_ENT_NAC as CB_ENT_NAC , -- Codigo2
				@kCB_COD_COL as CB_COD_COL , -- Codigo								
				@kCB_DER_PV as CB_DER_PV , -- DiasFrac
				@kCB_V_PRIMA as CB_V_PRIMA , -- DiasFrac
				@kCB_SUB_CTA as CB_SUB_CTA , -- Descripcion
				@kCB_NETO as CB_NETO , -- Pesos
				@kCB_NOMINA as CB_NOMINA , -- NominaTipo
				@kCB_RECONTR as CB_RECONTR , -- Booleano
				@kCB_DISCAPA as CB_DISCAPA , -- Booleano
				@kCB_INDIGE as CB_INDIGE , -- Booleano
				@kCB_FONACOT as CB_FONACOT , -- Descripcion
				@kCB_EMPLEO as CB_EMPLEO , -- Booleano								
				@kCB_G_TEX20 as CB_G_TEX20 , -- Descripcion
				@kCB_G_TEX21 as CB_G_TEX21 , -- Descripcion
				@kCB_G_TEX22 as CB_G_TEX22 , -- Descripcion
				@kCB_G_TEX23 as CB_G_TEX23 , -- Descripcion
				@kCB_G_TEX24 as CB_G_TEX24 , -- Descripcion
				@kCB_INFDISM as CB_INFDISM , -- Booleano
				@kCB_INFACT as CB_INFACT , -- Booleano
				@kCB_NUM_EXT as CB_NUM_EXT , -- NombreCampo
				@kCB_NUM_INT as CB_NUM_INT , -- NombreCampo
				@kCB_INF_ANT as CB_INF_ANT , -- Fecha
				@kCB_TDISCAP as CB_TDISCAP , -- Status
				@kCB_ESCUELA as CB_ESCUELA , -- DescLarga
				@kCB_TESCUEL as CB_TESCUEL , -- Status
				@kCB_TITULO as CB_TITULO , -- Status
				@kCB_YTITULO as CB_YTITULO , -- Anio
				@kCB_CTA_GAS as CB_CTA_GAS , -- Descripcion
				@kCB_ID_BIO as CB_ID_BIO , -- FolioGrande
				@kCB_GP_COD as CB_GP_COD , -- Codigo				
				@kCB_TSANGRE as CB_TSANGRE , -- Descripcion
				@kCB_ALERGIA as CB_ALERGIA , -- Formula
				@kCB_BRG_ACT as CB_BRG_ACT , -- Booleano
				@kCB_BRG_TIP as CB_BRG_TIP , -- Status
				@kCB_BRG_ROL as CB_BRG_ROL , -- Booleano
				@kCB_BRG_JEF as CB_BRG_JEF , -- Booleano
				@kCB_BRG_CON as CB_BRG_CON , -- Booleano
				@kCB_BRG_PRA as CB_BRG_PRA , -- Booleano
				@kCB_BRG_NOP as CB_BRG_NOP , -- Descripcion
				@kCB_BANCO as CB_BANCO ,	-- Codigo 
				@kCB_REGIMEN as CB_REGIMEN -- Status

		   )  REGISTRO    FOR XML AUTO, TYPE 
		   )   REGISTROS  FOR XML PATH('DATOS'), TYPE  
		   )  
		 	
		SET @ErrorID = 0 
		set @Error = '' 
	end
	
	-- Solo pruebas
	UPDATE T_Wftransacciones set TransCargoWFTress = @Xml where TransID = @transID 

	return @ErrorID
end



go 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_GetXMLPermisos') AND type IN ( N'P', N'PC' ))
	drop procedure WFTransaccion_GetXMLPermisos
go

CREATE PROCEDURE WFTransaccion_GetXMLPermisos( 
	@TransID	INT,
	@Xml		XML OUTPUT ,
	@FileID		INT OUTPUT, 
	@Error		VARCHAR( 255 ) OUTPUT)
AS
begin 	
	DECLARE @ErrorID	INT
	DECLARE @Cargo	XML
	
	SELECT	@Cargo	= TransCargo
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID	
	
	DECLARE @EMPRESA  varchar(10) 
	DECLARE @CB_CODIGO	INT
	DECLARE @NOMBRE		Formula 
	
	DECLARE @PermisoInicial Fecha 
	DECLARE @PermisoFinal   Fecha 
	DECLARE @PermisoDias    Pesos
	DECLARE @TipoPermisoClase Status 
	DECLARE @PermisoReferen Descripcion 
	DECLARE @PermisoDescrip	Titulo 
	DECLARE @TipoPermisoCodigo Codigo 
	DECLARE @XMLTress  Varchar(max) 

	declare @ClienteID FolioGrande 
	set @FileID = 0 
	
	select  
			ClienteID	= coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
			EMPRESA	= dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
			CB_CODIGO	= coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,
			NOMBRE		= coalesce(  t.c.value('(@PersonaName)', 'Varchar(256)' ), '' ),	

			INICIO   = coalesce(t.c.value('(@PermisoInicial)', 'DateTime' ), '1899-12-30' ) ,
			RETORNO  = coalesce(t.c.value('(@PermisoFinal)', 'DateTime' ), '1899-12-30' ) ,
			DIAS		= coalesce(t.c.value('(@PermisoDias)', 'DECIMAL(15,2)' ), 0.00),
			CLASE	= coalesce(t.c.value('(@TipoPermisoClase)', 'INT' ), 0),
			TIPO	= coalesce(t.c.value('(@TipoPermisoCodigo)', 'Varchar(256)' ), '' ) ,
			REFERENCIA  = coalesce(t.c.value('(@PermisoReferen)', 'Varchar(255)' ), '' ), 
			OBSERVACIONES  = coalesce(t.c.value('(@PermisoDescrip)', 'Varchar(255)' ), '' ), 
			FileID	= coalesce(t.c.value('(@FileID)', 'INT' ), 0)	
		
	 into #PERMI_TEMP 
	 from 
	 @Cargo.nodes('//REGISTROS/REGISTRO') t(c) 
		declare @XML_DatosMult XML 
		set @XML_DatosMult = ( 		
			select 					
				EMPRESA,
				'PERM' as MOV3,
				CB_CODIGO as EMPLEADO,
				convert( varchar(20), INICIO , 112 )   as INICIO, 
				convert( varchar(20), RETORNO , 112 )   as RETORNO, 	
				DIAS, 
				CLASE, 						
				TIPO, 
				REFERENCIA, 
				OBSERVACIONES				 
			from #PERMI_TEMP PT
			--group by PT.EMPRESA, PT.CB_CODIGO, INICIO, RETORNO 
			order by INICIO 
			FOR XML PATH('DATOS'), ROOT('MULTIPLE'), TYPE	
			)	
		declare @XMlDatosMult_varchar varchar(max)			
		set @XMlDatosMult_varchar = cast( @XML_DatosMult as varchar(max))	

		set @XmlTress = @XMlDatosMult_varchar

	set @Xml = @XMLTress 
	SET @ErrorID = 0 
	set @Error = '' 
	
	drop table #PERMI_TEMP

end
go 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_GetXMLAutorizarExtras') AND type IN ( N'P', N'PC' ))
	drop procedure WFTransaccion_GetXMLAutorizarExtras
go

CREATE PROCEDURE WFTransaccion_GetXMLAutorizarExtras( 
	@TransID	INT,
	@Xml		XML OUTPUT ,
	@FileID		INT OUTPUT, 
	@Error		VARCHAR( 255 ) OUTPUT)
AS
begin 	
 	
set nocount on 
DECLARE @ErrorID	INT
declare  @Cargo XML 

	SELECT	@Cargo	= TransCargo
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID	

DECLARE @XMLTress  Varchar(max) 	

	select  
			ClienteID	= coalesce(t.c.value('(@ClienteID)', 'INT' ), 0), 
			EMPRESA	= dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
			EMPLEADO	= coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,
			FECHAAUTO   = coalesce(r.r.value('(@EmpCalFecha)', 'DateTime' ), t.c.value('(@EmpCalFecha)', 'DateTime' ), '1899-12-30' ) ,
			NOMBRE		= coalesce(  t.c.value('(@PersonaName)', 'Varchar(256)' ), '' ),	
			HORAS		= coalesce(t.c.value('(@AutCalHoras)', 'DECIMAL(15,2)' ), 0.00),
			MOTIVO = coalesce(  r.r.value('(@MotivoCalCodigo)', 'Varchar(256)' ), '' ),	
			TIPOAUTO = coalesce(  r.r.value('(@MotivoCalClase)', 'INT' ), 0 ),	
			MOTIVOPREVIA = coalesce(  r.r.value('(@MotivoCalPrevia)', 'INT' ), 1),	
			FileID	= coalesce(t.c.value('(@FileID)', 'INT' ), 0)	
		
	 into #AUTORIZACIONES
	 from 
	 @Cargo.nodes('//REGISTROS/REGISTRO') t(c), 
	 @Cargo.nodes('//REGISTROS') r(r)

  declare @XML_Mult XML 

  -- SUSTITUIR, SUMAR, DEJAR_ANTERIOR 

  set @XMLTress = ( 
		   select 
			 
			 'AUTO3MULT' as MOV3,
			 1 as US_CODIGO, 
			 EMPRESA, 
			 ACCION_SI_EXISTE = 
					case MOTIVOPREVIA 
						when 1 then 'SUSTITUIR'
						when 2 then 'SUMAR' 
						when 3 then 'DEJAR_ANTERIOR' 
					else 
						'SUSTITUIR' 
					end, 
			 SUM( HORAS ) as TOTALHORAS, 
			 COUNT( EMPLEADO )  as TOTALREG 			  
			from #AUTORIZACIONES 
			group by EMPRESA,MOTIVOPREVIA
		   FOR XML PATH('')
		   
		   )	
	
		 set @XML_Mult = ( 		
			select  
				EMPRESA, 
				EMPLEADO, 
				MOV3 = 'AUTO3MULT', 
				TIPOSOLICITUD='AUTORIZACION', 
				ACCION_SI_EXISTE =
					case MOTIVOPREVIA 
						when 1 then 'SUSTITUIR'
						when 2 then 'SUMAR' 
						when 3 then 'DEJAR_ANTERIOR' 
					else 
						'SUSTITUIR' 
					end, 
				FECHAAUTO = convert( varchar(20), FECHAAUTO , 112 ) , 
				TIPOAUTO,
				HORAS,
				MOTIVO,
				EMPRESA_TXT = EMPRESA  

			from #AUTORIZACIONES 			
			FOR XML PATH('INFO'), ROOT('EMPLEADOS') 
		 )
		 		 
		declare @XMlMult_varchar varchar(max)
			
		set @XMlMult_varchar = cast( @XMl_Mult as varchar(max))	
		set @XmlTress = '<DATOS>' + @XmlTress + @XMlMult_varchar + '</DATOS>' 
		set @XML = @XmlTress
	 
	drop table #AUTORIZACIONES 

	set @FileID = 0 
	SET @ErrorID = 0 
	set @Error = '' 

end

go 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_GetXMLExcepciones') AND type IN ( N'P', N'PC' ))
	drop procedure WFTransaccion_GetXMLExcepciones
go

CREATE PROCEDURE WFTransaccion_GetXMLExcepciones( 
	@TransID	INT,
	@Xml		XML OUTPUT ,
	@FileID		INT OUTPUT, 
	@Error		VARCHAR( 255 ) OUTPUT)
AS
begin 	
	DECLARE @ErrorID	INT
	DECLARE @Cargo	XML
	
	SELECT	@Cargo	= TransCargo
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID	
	
	declare @ClienteID FolioGrande 
	set @FileID = 0 		

	select * into #EXCP_TEMP 
	from ( 
	select  		
			ClienteID	= coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
			EMPRESA	= dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
			EmpNumero	= coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,
			PersonaName		= coalesce(  t.c.value('(@PersonaName)', 'Varchar(256)' ), '' ),	
			PeriodoYear	= coalesce(t.c.value('(@PeriodoYear)', 'INT' ), 0),
			TipoNominaCodigo	= coalesce(t.c.value('(@TipoNominaCodigo)', 'INT' ), 0),
			PeriodoNumero	= coalesce(t.c.value('(@PeriodoNumero)', 'INT' ), 0),
			ConceptoNumero	= coalesce(t.c.value('(@ConceptoID)', 'INT' ), 0),
			TipoOperacion	= coalesce(t.c.value('(@TipoOperacion)', 'INT' ), 0),
			RevisarIncapacidad= coalesce(t.c.value('(@RevisarIncapacidad)', 'INT'  ), 0 ),			
			MovimientoMonto	= coalesce(t.c.value('(@ExcepcionMonto)', 'DECIMAL(15,2)' ), 0.00),
			MovimientoReferencia	= coalesce(t.c.value('(@ExcepcionObserva)', 'Varchar(256)' ), '' ) ,						
			VA_COMENTA  = coalesce(t.c.value('(@VacacionDescrip)', 'Varchar(255)' ), '' ), 
			FileID	= coalesce(t.c.value('(@FileID)', 'INT' ), 0), 
			FileID2	= coalesce(t.c.value('(@FileID2)', 'INT' ), 0)	
			 
	 from 
	 @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
	 ) CargoTabla
	 
	
	 if ( select count(*) from MOVIMIEN MO
	 join  #EXCP_TEMP ET on MO.CO_NUMERO = ET.ConceptoNumero 
	 and MO.CB_CODIGO = ET.EmpNumero and MO.PE_YEAR = ET.PeriodoYear and MO.PE_TIPO = ET.TipoNominaCodigo and MO.PE_NUMERO = ET.PeriodoNumero
	 where MO.US_CODIGO > 0 ) > 0 
	 begin 
		SET @ErrorID = 500; 
		SET @Error = 'Ya existen montos registrados previamente en el sistema para los empleados solicitados.' 
	 end  
	 else
	 begin 
		 set @Xml = ( 
			 select 		
				MOV3='EXNOMINA', 
				EMPRESA, 
 			( select 
				 EMPRESA, 
				 TipoNominaCodigo,PeriodoYear,PeriodoNumero,TipoOperacion, 
					case 
						when RevisarIncapacidad= 0 then 'False' 
						when RevisarIncapacidad= 1 then 'True' 
					else 
						'False' 
					end RevisarIncapacidad, 
				 count(*) Cuantos, 

					( SELECT ClienteId, ConceptoNumero, MovimientoMonto, MovimientoReferencia, EmpNumero, PersonaName FROM #EXCP_TEMP REGISTRO 
					     where REGISTROS.EMPRESA = REGISTRO.EMPRESA and REGISTROS.PeriodoNumero = REGISTRO.PeriodoNumero and
						       REGISTROS.PeriodoYear = REGISTRO.PeriodoYear and REGISTROS.TipoNominaCodigo = REGISTRO.TipoNominaCodigo   FOR XML AUTO, TYPE) 
			 from #EXCP_TEMP REGISTROS 
			 where DATOS.EMPRESA = REGISTROS.EMPRESA and DATOS.TipoOperacion = REGISTROS.TipoOperacion and DATOS.RevisarIncapacidad = REGISTROS.RevisarIncapacidad and 
				   DATOS.TipoNominaCodigo = REGISTROS.TipoNominaCodigo and  DATOS.PeriodoYear = REGISTROS.PeriodoYear and DATOS.PeriodoNumero = REGISTROS.PeriodoNumero 
				   			
			group by EMPRESA,TipoNominaCodigo,PeriodoYear,PeriodoNumero,TipoOperacion,RevisarIncapacidad 
			 FOR XML AUTO, TYPE 
			 ) 

			 from #EXCP_TEMP DATOS 
			group by EMPRESA,TipoNominaCodigo,PeriodoYear,PeriodoNumero,TipoOperacion,RevisarIncapacidad 
			 FOR XML PATH('DATOS'), ROOT('MULTIPLE'), TYPE 
		) 
					
		SET @ErrorID = 0 
		set @Error = '' 

	end 
	
	drop table #EXCP_TEMP 	
	
end



GO 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_GetXMLDocumentos') AND type IN ( N'P', N'PC' ))
	drop procedure WFTransaccion_GetXMLDocumentos
go

CREATE PROCEDURE WFTransaccion_GetXMLDocumentos( 
	@TransID	INT,
	@Xml		XML OUTPUT ,
	@FileID		INT OUTPUT, 
	@Error		VARCHAR( 255 ) OUTPUT, 
	@MultiplesFileID  VARCHAR(max) OUTPUT)
AS
begin 	
	DECLARE @ErrorID	INT
	DECLARE @Cargo	XML
	
	SELECT	@Cargo	= TransCargo
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID	
	
	DECLARE @EMPRESA  varchar(10) 
	DECLARE @CB_CODIGO	INT

	declare @ClienteID FolioGrande 
	set @FileID = 0 

	set @MultiplesFileID = '' 
	select @MultiplesFileID  = COALESCE(@MultiplesFileID+',' , '' ) + RTRIM( LTRIM( STR( FileID ) ) )
	from ( 
	select  		
			/*ClienteID	= coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
			EMPRESA	= dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 			*/
			FileID	= coalesce(t.c.value('(@FileID)', 'INT' ), 0)						 
	 from 
	 @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
	 ) CargoTabla
	 

	set @Xml = '' 	
	SET @ErrorID = 0 
	set @Error = '' 


end
GO 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_GetXMLDatosEmpleado') AND type IN ( N'P', N'PC' ))
	drop procedure WFTransaccion_GetXMLDatosEmpleado
go

CREATE  PROCEDURE WFTransaccion_GetXMLDatosEmpleado ( 
       @TransID     INT,
       @Xml         XML OUTPUT ,
       @FileID             INT OUTPUT, 
       @Error       VARCHAR( 255 ) OUTPUT)
AS
begin 
       DECLARE @ErrorID    INT
       DECLARE @Cargo      XML
       
       SELECT @Cargo = TransCargo
       FROM   T_WFTransacciones
       WHERE  TransID = @TransID  
       
       DECLARE @EMPRESA  varchar(10) 
       DECLARE @CB_CODIGO  INT
       DECLARE @NOMBRE            Formula 


       declare @ClienteID FolioGrande 
       set @FileID = 0             

       select * into #EXCP_TEMP 
       from ( 
       select              
                    ClienteID    = coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
                    EMPRESA      = dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
                    EmpNumero    = coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,
                    Campo        = coalesce(  t.c.value('(@Campo)', 'Varchar(256)' ), NULL ), 
                    valor        = coalesce(  t.c.value('(@Valor)', 'Varchar(256)' ),  NULL )                                                                   
        from 
        @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
       ) CargoTabla
       

       set @Xml = ( 
              select             
                    MOV3='MODCOLABORA', 
                    EMPRESA, 
             ( select 
                     EMPRESA,
                    count(*) Cuantos, 

                           ( SELECT ClienteId, EmpNumero, Campo, Valor  FROM #EXCP_TEMP REGISTRO where REGISTROS.EMPRESA = REGISTRO.EMPRESA  FOR XML AUTO, TYPE) 
              from #EXCP_TEMP REGISTROS 
              where DATOS.EMPRESA = REGISTROS.EMPRESA                
                    
             group by EMPRESA
             FOR XML AUTO, TYPE 
              ) 

             from #EXCP_TEMP DATOS 
             group by EMPRESA
             FOR XML PATH('DATOS'), ROOT('MULTIPLE'), TYPE 
       ) 
                           
       
       SET @ErrorID = 0 
       set @Error = '' 
       
       drop table #EXCP_TEMP      
       
end


GO 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_GetXMLInfonavit') AND type IN ( N'P', N'PC' ))
	drop procedure WFTransaccion_GetXMLInfonavit
go

CREATE PROCEDURE WFTransaccion_GetXMLInfonavit( 
	@TransID	INT,
	@Xml		XML OUTPUT ,
	@FileID		INT OUTPUT, 
	@Error		VARCHAR( 255 ) OUTPUT, 
	@MultiplesFileID Varchar(max) output )
AS
begin 	
	DECLARE @ErrorID	INT
	DECLARE @Cargo	XML
	
	SELECT	@Cargo	= TransCargo
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID	
	
	declare @ClienteID FolioGrande 
	set @FileID = 0 
		

	set @MultiplesFileID = '' 
	select @MultiplesFileID  = COALESCE(@MultiplesFileID+',' , '' ) + RTRIM( LTRIM( STR( FileID ) ) )
	from ( 
	select  					
			FileID	= coalesce(t.c.value('(@FileID)', 'INT' ), 0)						 
	 from 
	 @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
	 ) CargoTabla  where FileID > 0 
	 


	select * into #EXCP_TEMP 
	from ( 
	select  		
			ClienteID	= coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
			InfoTipo	= coalesce(t.c.value('(@InfoTipo)', 'INT' ), 0),		
			EMPRESA	= dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
			EmpNumero	= coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,			
			InfoFecha   = coalesce(t.c.value('(@InfoFecha)', 'Datetime' ), '1899-12-30' ) ,
			InfoNumero	= coalesce(t.c.value('(@InfoNumero)', 'Varchar(256)' ), '' ) ,		
			InfoClase	= coalesce(t.c.value('(@InfoClase)', 'INT' ), 0),		
			InfoValor	= coalesce(t.c.value('(@InfoValor)', 'DECIMAL(15,4)' ), 0.0000)			
	 from 
	 @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
	 ) CargoTabla
	 

	 set @Xml = ( 
		 select 		
			MOV3= 
			case InfoTipo 
				when 0 then 'INFONAVIT_AGREGAR'
				when 1 then 'INFONAVIT_MODIFICAR'
				when 2 then 'INFONAVIT_SUSPENDER'
				when 3 then 'INFONAVIT_REINICIAR'
			else 
				'INFONAVIT_AGREGAR' 
			end
				, 
			EMPRESA, 
 		( select 
			 EMPRESA, 
			 InfoTipo, 				
			 count(*) Cuantos, 

				( SELECT ClienteId,  EmpNumero, FECHA= convert( varchar(20), InfoFecha , 112 ), CB_INF_INI = convert( varchar(20), InfoFecha , 112 ), CB_INF_ANT = convert( varchar(20), InfoFecha , 112 ) , CB_INFCRED = InfoNumero, CB_INFTIPO=InfoClase, CB_INFTASA=InfoValor FROM #EXCP_TEMP REGISTRO where REGISTROS.EMPRESA = REGISTRO.EMPRESA and REGISTROS.InfoTipo = REGISTRO.InfoTipo  FOR XML AUTO, TYPE) 
		 from #EXCP_TEMP REGISTROS 
		 where DATOS.EMPRESA = REGISTROS.EMPRESA and DATOS.InfoTipo = REGISTROS.InfoTipo
			  
			
		group by EMPRESA,InfoTipo
		 FOR XML AUTO, TYPE 
		 ) 

		 from #EXCP_TEMP DATOS 
		group by EMPRESA,InfoTipo
		 FOR XML PATH('DATOS'), ROOT('MULTIPLE'), TYPE 
	) 
				
	
	SET @ErrorID = 0 
	set @Error = '' 
	
	drop table #EXCP_TEMP 	
	
end

GO 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_GetXMLIncapacidades') AND type IN ( N'P', N'PC' ))
	drop procedure WFTransaccion_GetXMLIncapacidades
go

CREATE PROCEDURE WFTransaccion_GetXMLIncapacidades( 
	@TransID	INT,
	@Xml		XML OUTPUT ,
	@FileID		INT OUTPUT, 
	@Error		VARCHAR( 255 ) OUTPUT, 
	@MultiplesFileID Varchar(max) output )
AS
begin 	
	DECLARE @ErrorID	INT
	DECLARE @Cargo	XML
	
	SELECT	@Cargo	= TransCargo
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID	
	
	declare @ClienteID FolioGrande 
	set @FileID = 0 		

	set @MultiplesFileID = '' 
	select @MultiplesFileID  = COALESCE(@MultiplesFileID+',' , '' ) + RTRIM( LTRIM( STR( FileID ) ) )
	from ( 
	select  					
			FileID	= coalesce(t.c.value('(@FileID)', 'INT' ), 0)						 
	 from 
	 @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
	 ) CargoTabla
	 

	select * into #INCAPAS_TEMP 
	from ( 
	select  		
			ClienteID	= coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 			
			EMPRESA	= dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
			EmpNumero	= coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,			
			IncapacidadInicial   = convert( varchar(20),coalesce(t.c.value('(@IncapaInicial)', 'Datetime' ), '1899-12-30' ) , 112 ) , 
			IncapacidadFechaRH   =  convert( varchar(20),coalesce(t.c.value('(@IncapaFechaRH)', 'Datetime' ), '1899-12-30' ) , 112 ) , 
			IncapacidadSUAInicial   =  convert( varchar(20),coalesce(t.c.value('(@IncapaSUAInicial)', 'Datetime' ), '1899-12-30' ) , 112 ) , 
			IncapacidadSUAFinal   =  convert( varchar(20),coalesce(t.c.value('(@IncapaSUAFinal)', 'Datetime' ), '1899-12-30' ) , 112 ) , 
			IncapacidadDias	= coalesce(t.c.value('(@IncapaDias)', 'INT' ), 0),		
			IncapacidadNumero	= coalesce(t.c.value('(@IncapaNumero)', 'Varchar(256)' ), '' ) ,		
			IncapacidadTipo	=  coalesce(t.c.value('(@TipoIncapaCodigo)', 'Varchar(10)' ), '' ) ,		
			IncapacidadObserva	=  coalesce(t.c.value('(@IncapaDescrip)', 'Varchar(256)' ), '' ) ,	
			IncapacidadClaseMotivo = coalesce(t.c.value('(@IncapaControl)', 'INT' ), 0),		
			IncapacidadTipoFin = coalesce(t.c.value('(@IncapaTipoFin)', 'INT' ), 0),	
			IncapacidadPermanente  = coalesce(t.c.value('(@IncapaPermanente)', 'Decimal(15,4)' ), 0.0000),
			IncapacidadDiasSubsidiar =  coalesce(t.c.value('(@IncapaDiasSubsidiar)', 'Decimal(15,2)' ), 0.00),		
			IncapacidadAjustarFecha = coalesce(t.c.value('(@IncapaAjustarFecha)', 'INT' ), 0),	
			-- PeriodoYear="2015" TipoNominaCodigo="1" PeriodoNumero="54" 	
			PeriodoYear = coalesce(t.c.value('(@PeriodoYear)', 'INT' ), 0),
			TipoNominaCodigo = coalesce(t.c.value('(@TipoNominaCodigo)', 'INT' ), 0),
			PeriodoNumero = coalesce(t.c.value('(@PeriodoNumero)', 'INT' ), 0)
	 from 
	 @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
	 ) CargoTabla
	 

	 set @Xml = ( 
		 select 		
			MOV3= 'INCAPACIDAD_AGREGAR',			
			EMPRESA, 
 		( select 
			 EMPRESA, 			 
			 count(*) Cuantos, 

				( SELECT ClienteId,  EmpNumero, IncapacidadInicial,
						case IncapacidadFechaRH
							when '18991230' then IncapacidadInicial 
							else IncapacidadFechaRH 
						end IncapacidadFechaRH, 						
						case IncapacidadSUAInicial
							when '18991230' then IncapacidadInicial 
							else IncapacidadSUAInicial 
						end IncapacidadSUAInicial, 
						case IncapacidadSUAFinal
							when '18991230' then convert( varchar(20), cast(  dateadd( day, IncapacidadDias, IncapacidadInicial ) as Datetime )  , 112 ) 
							else IncapacidadSUAFinal 
						end IncapacidadSUAFinal, 						
						IncapacidadDias,
						IncapacidadNumero,
						IncapacidadTipo,
						IncapacidadObserva,
						IncapacidadClaseMotivo,												
						IncapacidadTipoFin,
						IncapacidadPermanente, 
						IncapacidadDiasSubsidiar,
						IncapacidadAjustarFecha,
						PeriodoYear,
						TipoNominaCodigo,
						PeriodoNumero				
						FROM #INCAPAS_TEMP REGISTRO where REGISTROS.EMPRESA = REGISTRO.EMPRESA   FOR XML AUTO, TYPE) 
		 from #INCAPAS_TEMP REGISTROS 
		 where DATOS.EMPRESA = REGISTROS.EMPRESA 
			  
			
		group by EMPRESA
		 FOR XML AUTO, TYPE 
		 ) 

		 from #INCAPAS_TEMP DATOS 
		group by EMPRESA
		 FOR XML PATH('DATOS'), ROOT('MULTIPLE'), TYPE 
	) 
				
	
	SET @ErrorID = 0 
	set @Error = '' 
	
	drop table #INCAPAS_TEMP 	
	
end


GO 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_GetXMLPrestamos') AND type IN ( N'P', N'PC' ))
	drop procedure WFTransaccion_GetXMLPrestamos
go

CREATE PROCEDURE WFTransaccion_GetXMLPrestamos( 
	@TransID	INT,
	@Xml		XML OUTPUT ,
	@FileID		INT OUTPUT, 
	@Error		VARCHAR( 255 ) OUTPUT, 
	@MultiplesFileID Varchar(max) output )
AS
begin 	
	DECLARE @ErrorID	INT
	DECLARE @Cargo	XML
	
	SELECT	@Cargo	= TransCargo
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID	
	
	declare @ClienteID FolioGrande 
	set @FileID = 0 
		


	set @MultiplesFileID = '' 
	select @MultiplesFileID  = COALESCE(@MultiplesFileID+',' , '' ) + RTRIM( LTRIM( STR( FileID ) ) )
	from ( 
	select  					
			FileID	= coalesce(t.c.value('(@FileID)', 'INT' ), 0)						 
	 from 
	 @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
	 ) CargoTabla
	 

	select * into #PREST_TEMP 
	from ( 
	select  		
			ClienteID	= coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
			PersonaID	= coalesce(  t.c.value('(@PersonaID)', 'INT' ), 0), 			
			EMPRESA	= dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
			EmpNumero	= coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,	
			TipoDesCodigo	= coalesce(t.c.value('(@TipoDesCodigo)', 'Varchar(256)' ), '' ) ,	
			DescuentoReferencia	= coalesce(t.c.value('(@DescuentoReferencia)', 'Varchar(256)' ), '' ) ,	
			DescuentoInicial   = convert( varchar(20),coalesce(t.c.value('(@DescuentoInicial)', 'Datetime' ), '1899-12-30' ) , 112 ) , 
			DescuentoMonto =  coalesce(t.c.value('(@DescuentoMonto)', 'Decimal(15,2)' ), 0.00),
			DescuentoFormula	= coalesce(t.c.value('(@DescuentoFormula)', 'Varchar(256)' ), '' ) ,
			DescuentoSubCta	= coalesce(t.c.value('(@DescuentoSubCta)', 'Varchar(256)' ), '' ) ,
			DescuentoObserva	= coalesce(t.c.value('(@DescuentoObserva)', 'Varchar(256)' ), '' ) ,
			DescuentoSolicita =  coalesce(t.c.value('(@DescuentoSolicita)', 'Decimal(15,2)' ), 0.00),
			DescuentoInteres =  coalesce(t.c.value('(@DescuentoInteres)', 'Decimal(15,2)' ), 0.00),
			DescuentoMeses =  coalesce(t.c.value('(@DescuentoMeses)', 'Decimal(15,2)' ), 0.00),
			DescuentoPagPer =  coalesce(t.c.value('(@DescuentoPagPer)', 'Decimal(15,2)' ), 0.00),
			DescuentoPagos =  coalesce(t.c.value('(@DescuentoPagos)', 'Decimal(15,2)' ), 0.00),
			DescuentoTasa =  coalesce(t.c.value('(@DescuentoTasa)', 'Decimal(15,2)' ), 0.00)
	 from 
	 @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
	 ) CargoTabla
	 

	 set @Xml = ( 
		 select 		
			MOV3= 'PRESTAMO_AGREGAR',			
			EMPRESA, 
 		( select 
			 EMPRESA, 			 
			 count(*) Cuantos, 

				( SELECT ClienteId, 
						PersonaID, 
						EmpNumero, 
						TipoDesCodigo,
						DescuentoReferencia,
						DescuentoInicial,
						DescuentoMonto,												
						DescuentoFormula,
						DescuentoSubCta,
						DescuentoObserva,
						DescuentoSolicita,
						DescuentoInteres,
						DescuentoMeses,
						DescuentoPagPer,
						DescuentoPagos,
						DescuentoTasa				
						FROM #PREST_TEMP REGISTRO where REGISTROS.EMPRESA = REGISTRO.EMPRESA   FOR XML AUTO, TYPE) 
		 from #PREST_TEMP REGISTROS 
		 where DATOS.EMPRESA = REGISTROS.EMPRESA 
			  
			
		group by EMPRESA
		 FOR XML AUTO, TYPE 
		 ) 

		 from #PREST_TEMP DATOS 
		group by EMPRESA
		 FOR XML PATH('DATOS')--, ROOT('MULTIPLE'), TYPE 
	) 
				
	
	SET @ErrorID = 0 
	set @Error = '' 
	
	drop table #PREST_TEMP 	
	
end


GO 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_GetXMLRespuesta') AND type IN ( N'P', N'PC' ))
	drop procedure WFTransaccion_GetXMLRespuesta
go

CREATE PROCEDURE WFTransaccion_GetXMLRespuesta(
       @TransID            INT,
       @Xml                XML,
       @HayXML                    bit OUTPUT, 
       @XmlRespuesta XML OUTPUT , 
       @Error       VARCHAR( 255 ) OUTPUT)
AS
begin 

       DECLARE      @VerboID     INT
       DECLARE      @ObjetoID    INT
       DECLARE @ErrorID    INT
       DECLARE @XmlCargo   XML 
       
       --SET ARITHABORT OFF
       
       SELECT @VerboID     = VerboID,  @ObjetoID = ObjetoID, @XmlCargo = TransCargo
       FROM   T_WFTransacciones
       WHERE  TransID = @TransID
       
       set @HayXML = 0 

	   update T_WFTransacciones set TransCargoRespuestaTress = @Xml where TransID = @TransID 


       IF ( @VerboID = 100198) and ( @ObjetoID = 100033)  
       begin 
             set @HayXML = 1 
             set @XmlRespuesta = @XmlCargo  

             --Parametro="8719" Clave1="No Maestro C1" Clave2="No Maestro C2"
             declare @i int 
             declare @iMax int          
             select ROW_NUMBER() over (order by Numero) as I , Numero, Parametro, Clave1, Clave2  into #Autorizados 
             from ( 
             select                                         
                           Numero = coalesce(  t.c.value('(@Numero)', 'INT' ), 0),      
                           Parametro  =  coalesce(  t.c.value('(@Parametro)', 'INT' ), 0),           
                           Clave1  =  coalesce(  t.c.value('(@Clave1)', 'varchar(255)' ), ''),              
                           Clave2  =  coalesce(  t.c.value('(@Clave2)', 'varchar(255)' ), '')
             from 
              @Xml.nodes('//Respuesta/Sentinel') t(c)
             ) XmlTabla

             select @iMax = count(*) from #Autorizados
             declare @Parametro int 
             declare @Numero int 
             declare @Clave1 varchar(255) 
             declare @Clave2 varchar(255) 
             set @i= 1 
             while ( @i <= @iMax ) 
             begin 

                    select @Parametro = Parametro , @Numero = Numero ,  @Clave1 = Clave1 , @Clave2 = Clave2 from #Autorizados where i =@i                  
                    SET @XmlRespuesta.modify( 'insert attribute SentinelParametroNew {sql:variable("@Parametro") } into (/REGISTROS/REGISTRO[@SentinelSerie=sql:variable("@Numero")])[1]'  )
                    SET @XmlRespuesta.modify( 'insert attribute SentinelClave1New {sql:variable("@Clave1") } into (/REGISTROS/REGISTRO[@SentinelSerie=sql:variable("@Numero")])[1]'  )
                    SET @XmlRespuesta.modify( 'insert attribute SentinelClave2New {sql:variable("@Clave2") } into (/REGISTROS/REGISTRO[@SentinelSerie=sql:variable("@Numero")])[1]'  ) 
                    set @i= @i +1 
             end 
       
             drop table #Autorizados
       end
	   else
	   IF ( @VerboID = 1281 ) and ( @ObjetoID = 1068 )  -- Alta de empleado 
       begin 
             set @HayXML = 1 
             set @XmlRespuesta = @XmlCargo  
			
             declare @CB_CODIGO int 
			 declare @EmpNumero int 
			 
             select                                                                    
                 @CB_CODIGO  =  coalesce(  t.c.value('(@CB_CODIGO)', 'INT' ), 0)                                      
             from 
              @Xml.nodes('//DATOS/REGISTROS/REGISTRO') t(c)

			 set @EmpNumero = -1 
			 select                                                                    
                 @EmpNumero  = coalesce( t.c.value('(@EmpNumero)', 'INT' ), -1 )                                     
             from 
              @XmlRespuesta.nodes('//REGISTROS/REGISTRO') t(c)
             			            			 
                                                       
             -- Se modifica el cargo para 
             if ( @EmpNumero < 0 )                    
                 SET @XmlRespuesta.modify( 'insert attribute EmpNumero {sql:variable("@CB_CODIGO") } into (//REGISTROS/REGISTRO)[1]'  )                      
             else
              SET @XmlRespuesta.modify( 'replace value of (//REGISTROS/REGISTRO/@EmpNumero)[1]  with sql:variable("@CB_CODIGO") '  )                                    

       end
       
	   update T_WFTransacciones set TransCargoRespuesta = @XmlRespuesta where TransID = @TransID 
	   
       
       return 0 
end

GO 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_GetXMLPuestoClasificacion') AND type IN ( N'P', N'PC' ))
       drop procedure WFTransaccion_GetXMLPuestoClasificacion
GO
 
 
CREATE PROCEDURE WFTransaccion_GetXMLPuestoClasificacion(
       @TransID     INT,
       @Xml         XML OUTPUT ,
       @FileID             INT OUTPUT,
       @Error       VARCHAR( 255 ) OUTPUT,
       @MultiplesFileID Varchar(max) output )
AS
begin
       DECLARE @ErrorID    INT
       DECLARE @Cargo      XML
      
       SELECT @Cargo = TransCargo
       FROM   T_WFTransacciones
       WHERE  TransID = @TransID 
      
       declare @ClienteID FolioGrande
       set @FileID = 0
            
 
 
       set @MultiplesFileID = ''
       select @MultiplesFileID  = COALESCE(@MultiplesFileID+',' , '' ) + RTRIM( LTRIM( STR( FileID ) ) )
       from (
       select                                 
                    FileID = coalesce(t.c.value('(@FileID)', 'INT' ), 0)                                   
        from
        @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
       ) CargoTabla
      
 
       select * into #PUESTO_TEMP
       from (
       select             
                    ClienteID    = coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0),
                    PersonaID    = coalesce(  t.c.value('(@PersonaID)', 'INT' ), 0),               
                    EMPRESA      = dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ),
                    EMPLEADO     = coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) , 
                    CB_PUESTO    = coalesce(t.c.value('(@PuestoCodigo)', 'Varchar(256)' ), '' ) ,  
                    CB_CLASIFI   = coalesce(t.c.value('(@ClasificacionCodigo)', 'Varchar(256)' ), '' ) ,   
                    CB_FECHAREG   = convert( varchar(20),coalesce(t.c.value('(@KarFecha)', 'Datetime' ), '1899-12-30' ) , 112 ) ,                   
            	    DESCRIPCION     = coalesce(t.c.value('(@MotKarNombre)', 'Varchar(256)' ), '' )     , 
                    OBSERVACIONES     = coalesce(t.c.value('(@KarDescrip)', 'Varchar(256)' ), '' )                       
        from
        @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
       ) CargoTabla
      
 
       set @Xml = (
              select            
                    MOV3= 'CAMBIOPUESTO',                  
                    EMPRESA,
             ( select
                     EMPRESA,                   
                     count(*) Cuantos,
 
                           ( SELECT ClienteId,
                                        PersonaID,
                                        EMPLEADO,
                                        CB_CLASIFI,
                                        CB_PUESTO,                                    
                                        CB_FECHAREG,
                                        DESCRIPCION,
                                        OBSERVACIONES             
                                        FROM #PUESTO_TEMP  REGISTRO  where EMPLEADOS.EMPRESA = REGISTRO.EMPRESA   FOR XML PATH('INFO'), TYPE )
              from #PUESTO_TEMP EMPLEADOS
              where DATOS.EMPRESA = EMPLEADOS.EMPRESA
                      
                   
             group by EMPRESA
             FOR XML AUTO, TYPE
              )
 
             from #PUESTO_TEMP DATOS
             group by EMPRESA
             FOR XML PATH('DATOS')--, ROOT('MULTIPLE'), TYPE
       )
                          
      
       SET @ErrorID = 0
       set @Error = ''
      
       drop table #PUESTO_TEMP   
      
end

GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_GetXMLCambioTurno') AND type IN ( N'P', N'PC' ))
    drop procedure WFTransaccion_GetXMLCambioTurno
GO
CREATE PROCEDURE WFTransaccion_GetXMLCambioTurno( 
    @TransID    INT,
    @Xml        XML OUTPUT ,
    @FileID        INT OUTPUT, 
    @Error        VARCHAR( 255 ) OUTPUT, 
    @MultiplesFileID Varchar(max) output )
AS
begin     
    DECLARE @ErrorID    INT
    DECLARE @Cargo    XML
    
    SELECT    @Cargo    = TransCargo
    FROM    T_WFTransacciones
    WHERE    TransID = @TransID    
    
    declare @ClienteID FolioGrande 
    set @FileID = 0 
        
    set @MultiplesFileID = '' 
    select @MultiplesFileID  = COALESCE(@MultiplesFileID+',' , '' ) + RTRIM( LTRIM( STR( FileID ) ) )
    from ( 
    select                      
            FileID    = coalesce(t.c.value('(@FileID)', 'INT' ), 0)                         
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
    select * into #TURNO_TEMP 
    from ( 
    select          
            ClienteID    = coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
            PersonaID    = coalesce(  t.c.value('(@PersonaID)', 'INT' ), 0),             
            EMPRESA    = dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
            EMPLEADO    = coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,    
            CB_TURNO    = coalesce(t.c.value('(@TurnoCodigo)', 'Varchar(256)' ), '' ) ,                
            CB_FECHAREG   = convert( varchar(20),coalesce(t.c.value('(@KarFecha)', 'Datetime' ), '1899-12-30' ) , 112 ) ,             
            DESCRIPCION     = coalesce(t.c.value('(@MotKarNombre)', 'Varchar(256)' ), '' )     , 
            OBSERVACIONES     = coalesce(t.c.value('(@KarDescrip)', 'Varchar(256)' ), '' )               
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
     set @Xml = ( 
         select         
            MOV3= 'CAMBIOTURNO',            
            EMPRESA, 
         ( select 
             EMPRESA,              
             count(*) Cuantos, 
                ( SELECT ClienteId, 
                        PersonaID, 
                        EMPLEADO, 
                        CB_TURNO, 
                        CB_FECHAREG,
                        DESCRIPCION,
                        OBSERVACIONES            
                        FROM #TURNO_TEMP  REGISTRO  where EMPLEADOS.EMPRESA = REGISTRO.EMPRESA   FOR XML PATH('INFO'), TYPE ) 
         from #TURNO_TEMP EMPLEADOS 
         where DATOS.EMPRESA = EMPLEADOS.EMPRESA 
              
            
        group by EMPRESA
         FOR XML AUTO, TYPE 
         ) 
         from #TURNO_TEMP DATOS 
        group by EMPRESA
         FOR XML PATH('DATOS')--, ROOT('MULTIPLE'), TYPE 
    ) 
                
    
    SET @ErrorID = 0 
    set @Error = '' 
    
    drop table #TURNO_TEMP     
    
end

GO 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_GetXMLCambioEquipo') AND type IN ( N'P', N'PC' ))
    drop procedure WFTransaccion_GetXMLCambioEquipo 
GO

CREATE PROCEDURE WFTransaccion_GetXMLCambioEquipo( 
    @TransID    INT,
    @Xml        XML OUTPUT ,
    @FileID        INT OUTPUT, 
    @Error        VARCHAR( 255 ) OUTPUT, 
    @MultiplesFileID Varchar(max) output )
AS
begin     
    DECLARE @ErrorID    INT
    DECLARE @Cargo    XML
    
    SELECT    @Cargo    = TransCargo
    FROM    T_WFTransacciones
    WHERE    TransID = @TransID    
    
    declare @ClienteID FolioGrande 
    set @FileID = 0 
        

    set @MultiplesFileID = '' 
    select @MultiplesFileID  = COALESCE(@MultiplesFileID+',' , '' ) + RTRIM( LTRIM( STR( FileID ) ) )
    from ( 
    select                      
            FileID    = coalesce(t.c.value('(@FileID)', 'INT' ), 0)                         
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
    select * into #EQUIPO_TEMP 
    from ( 
    select          
            ClienteID    = coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
            PersonaID    = coalesce(  t.c.value('(@PersonaID)', 'INT' ), 0),             
            EMPRESA    = dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
            EMPLEADO    = coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,    
            CB_EQUIPO    = coalesce(t.c.value('(@EquipoCodigo)', 'Varchar(256)' ), '' ) ,                
            CB_FECHAREG   = convert( varchar(20),coalesce(t.c.value('(@KarFecha)', 'Datetime' ), '1899-12-30' ) , 112 ) ,             
            DESCRIPCION     = coalesce(t.c.value('(@MotKarNombre)', 'Varchar(256)' ), '' )     , 
            OBSERVACIONES     = coalesce(t.c.value('(@KarDescrip)', 'Varchar(256)' ), '' )             
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
     set @Xml = ( 
         select         
            MOV3= 'CAMBIOEQUIPO',            
            EMPRESA, 
         ( select 
             EMPRESA,              
             count(*) Cuantos, 
                ( SELECT ClienteId, 
                        PersonaID, 
                        EMPLEADO, 
                        CB_EQUIPO, 
                        CB_FECHAREG,
                        DESCRIPCION,
                        OBSERVACIONES            
                        FROM #EQUIPO_TEMP  REGISTRO  where EMPLEADOS.EMPRESA = REGISTRO.EMPRESA   FOR XML PATH('INFO'), TYPE ) 
         from #EQUIPO_TEMP EMPLEADOS 
         where DATOS.EMPRESA = EMPLEADOS.EMPRESA 
              
            
        group by EMPRESA
         FOR XML AUTO, TYPE 
         ) 
         from #EQUIPO_TEMP DATOS 
        group by EMPRESA
         FOR XML PATH('DATOS')--, ROOT('MULTIPLE'), TYPE 
    ) 
                
    
    SET @ErrorID = 0 
    set @Error = '' 
    
    drop table #EQUIPO_TEMP     
    
end

GO


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_GetXMLBajaEmpleado') AND type IN ( N'P', N'PC' ))
    drop procedure WFTransaccion_GetXMLBajaEmpleado 
GO

CREATE PROCEDURE WFTransaccion_GetXMLBajaEmpleado( 
    @TransID    INT,
    @Xml        XML OUTPUT ,
    @FileID        INT OUTPUT, 
    @Error        VARCHAR( 255 ) OUTPUT, 
    @MultiplesFileID Varchar(max) output )
AS
begin     
    DECLARE @ErrorID    INT
    DECLARE @Cargo    XML
    
    SELECT    @Cargo    = TransCargo
    FROM    T_WFTransacciones
    WHERE    TransID = @TransID    
    
    declare @ClienteID FolioGrande 
    set @FileID = 0 
        

    set @MultiplesFileID = '' 
    select @MultiplesFileID  = COALESCE(@MultiplesFileID+',' , '' ) + RTRIM( LTRIM( STR( FileID ) ) )
    from ( 
    select                      
            FileID    = coalesce(t.c.value('(@FileID)', 'INT' ), 0)                         
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
    select * into #EQUIPO_TEMP 
    from ( 
    select          
            ClienteID    = coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
            PersonaID    = coalesce(  t.c.value('(@PersonaID)', 'INT' ), 0),             
            EMPRESA    = dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
            EMPLEADO    = coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,    
            PE_YEAR    = coalesce(  t.c.value('(@PeriodoYear)', 'INT' ), 0) ,    
            PE_TIPO    = coalesce(  t.c.value('(@TipoNominaCodigo)', 'INT' ), 0) ,    
            PE_NUMERO    = coalesce(  t.c.value('(@PeriodoNumero)', 'INT' ), 0) ,    
            CB_MOTIVO    = coalesce(t.c.value('(@MotKarCodigo)', 'Varchar(256)' ), '' ) ,                
            CB_FECHAREG = convert( varchar(20),coalesce(t.c.value('(@KarFecha)', 'Datetime' ), '1899-12-30' ) , 112 ) ,             
            CB_FECHASS   = convert( varchar(20),coalesce(t.c.value('(@KarFechaSS)', 'Datetime' ), '1899-12-30' ) , 112 ) ,             
            DESCRIPCION     = coalesce(t.c.value('(@MotKarNombre)', 'Varchar(256)' ), '' )     , 
            OBSERVACIONES     = coalesce(t.c.value('(@KarDescrip)', 'Varchar(256)' ), '' ),
			CB_RECONTR    = coalesce(t.c.value('(@KarReingreso)', 'Varchar(256)' ), '' )
             
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
     set @Xml = ( 
         select         
            MOV3= 'BAJA',            
            EMPRESA, 
         ( select 
             EMPRESA,              
             count(*) Cuantos, 
                ( SELECT ClienteId, 
                        PersonaID, 
                        EMPLEADO, 
                        CB_MOTIVO, 
                        CB_FECHAREG,
                        CB_FECHASS,
                        DESCRIPCION,
                        OBSERVACIONES, 
                        PE_YEAR, 
                        PE_TIPO, 
                        PE_NUMERO,
						CB_RECONTR = 
							case CB_RECONTR 
								when 'true' then 'S'
								when 'false' then 'N'
							else 
								'N' 
							end						          
                        FROM #EQUIPO_TEMP  REGISTRO  where EMPLEADOS.EMPRESA = REGISTRO.EMPRESA   FOR XML PATH('INFO'), TYPE ) 
         from #EQUIPO_TEMP EMPLEADOS 
         where DATOS.EMPRESA = EMPLEADOS.EMPRESA 
              
            
        group by EMPRESA
         FOR XML AUTO, TYPE 
         ) 
         from #EQUIPO_TEMP DATOS 
        group by EMPRESA
         FOR XML PATH('DATOS')--, ROOT('MULTIPLE'), TYPE 
    ) 
                
    
    SET @ErrorID = 0 
    set @Error = '' 
    
    drop table #EQUIPO_TEMP     
    
end
GO



IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_GetXMLCambioSalario') AND type IN ( N'P', N'PC' ))
    drop procedure WFTransaccion_GetXMLCambioSalario
go
CREATE PROCEDURE WFTransaccion_GetXMLCambioSalario( 
    @TransID    INT,
    @Xml        XML OUTPUT ,
    @FileID        INT OUTPUT, 
    @Error        VARCHAR( 255 ) OUTPUT, 
    @MultiplesFileID Varchar(max) output )
AS
begin     
    DECLARE @ErrorID    INT
    DECLARE @Cargo    XML
    
    SELECT    @Cargo    = TransCargo
    FROM    T_WFTransacciones
    WHERE    TransID = @TransID    
    
    declare @ClienteID FolioGrande 
    set @FileID = 0 
        
    set @MultiplesFileID = '' 
    select @MultiplesFileID  = COALESCE(@MultiplesFileID+',' , '' ) + RTRIM( LTRIM( STR( FileID ) ) )
    from ( 
    select                      
            FileID    = coalesce(t.c.value('(@FileID)', 'INT' ), 0)                         
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
    select * into #SALARIO_TEMP 
    from ( 
    select          
            ClienteID    = coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
            PersonaID    = coalesce(  t.c.value('(@PersonaID)', 'INT' ), 0),             
            EMPRESA    = dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
            EMPLEADO    = coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,               
            CB_SALARIO   = coalesce(  t.c.value('(@CompSalario)', 'Numeric(15,2)' ), 0.00) ,               
            CB_FECHAREG = convert( varchar(20),coalesce(t.c.value('(@KarFecha)', 'Datetime' ), '1899-12-30' ) , 112 ) ,             
            CB_FECHASS   = convert( varchar(20),coalesce(t.c.value('(@KarFechaSS)', 'Datetime' ), '1899-12-30' ) , 112 ) ,             
            KARAUTOSAL = coalesce(t.c.value('(@KarAutoSal )', 'INT' ), 0 )     , 
            DESCRIPCION     = coalesce(t.c.value('(@KarDescrip)', 'Varchar(256)' ), '' )     , 
            OBSERVACIONES     = coalesce(t.c.value('(@KarObserva)', 'Varchar(256)' ), '' )             
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
     set @Xml = ( 
         select         
            MOV3= 'CAMBIOSALARIO',            
            EMPRESA, 
         ( select 
             EMPRESA,              
             count(*) Cuantos, 
                ( SELECT ClienteId, 
                        PersonaID, 
                        EMPLEADO, 
                        CB_SALARIO, 
                        CB_FECHAREG,
                        CB_FECHASS,
                        CB_AUTOSAL = case  KARAUTOSAL  when 0 then 'N'  else 'S'  end , 
                        DESCRIPCION,
                        OBSERVACIONES
                        FROM #SALARIO_TEMP REGISTRO  where EMPLEADOS.EMPRESA = REGISTRO.EMPRESA   FOR XML PATH('INFO'), TYPE ) 
         from #SALARIO_TEMP EMPLEADOS 
         where DATOS.EMPRESA = EMPLEADOS.EMPRESA 
              
            
        group by EMPRESA
         FOR XML AUTO, TYPE 
         ) 
         from #SALARIO_TEMP DATOS 
        group by EMPRESA
         FOR XML PATH('DATOS')--, ROOT('MULTIPLE'), TYPE 
    ) 
                
    
    SET @ErrorID = 0 
    set @Error = '' 
    
    drop table #SALARIO_TEMP     
    
end
go 




IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_GetXMLCambioAreas') AND type IN ( N'P', N'PC' ))
    drop procedure WFTransaccion_GetXMLCambioAreas
go
CREATE PROCEDURE WFTransaccion_GetXMLCambioAreas( 
    @TransID    INT,
    @Xml        XML OUTPUT ,
    @FileID        INT OUTPUT, 
    @Error        VARCHAR( 255 ) OUTPUT, 
    @MultiplesFileID Varchar(max) output )
AS
begin     
    DECLARE @ErrorID    INT
    DECLARE @Cargo    XML
    
    SELECT    @Cargo    = TransCargo
    FROM    T_WFTransacciones
    WHERE    TransID = @TransID    
    
    declare @ClienteID FolioGrande 
    set @FileID = 0 
        
    set @MultiplesFileID = '' 
    select @MultiplesFileID  = COALESCE(@MultiplesFileID+',' , '' ) + RTRIM( LTRIM( STR( FileID ) ) )
    from ( 
    select                      
            FileID    = coalesce(t.c.value('(@FileID)', 'INT' ), 0)                         
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
    select * into #EQUIPO_TEMP 
    from ( 
    select          
            ClienteID    = coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
            PersonaID    = coalesce(  t.c.value('(@PersonaID)', 'INT' ), 0),             
            EMPRESA    = dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
            EMPLEADO    = coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,    
            CB_NIVEL1    = coalesce(t.c.value('(@Nivel1Codigo)', 'Varchar(256)' ), '' ) ,                
            CB_NIVEL2    = coalesce(t.c.value('(@Nivel2Codigo)', 'Varchar(256)' ), '' ) ,                
            CB_NIVEL3    = coalesce(t.c.value('(@Nivel3Codigo)', 'Varchar(256)' ), '' ) ,                
            CB_NIVEL4    = coalesce(t.c.value('(@Nivel4Codigo)', 'Varchar(256)' ), '' ) ,                
            CB_NIVEL5    = coalesce(t.c.value('(@Nivel5Codigo)', 'Varchar(256)' ), '' ) ,                
            CB_NIVEL6    = coalesce(t.c.value('(@Nivel6Codigo)', 'Varchar(256)' ), '' ) ,                
            CB_NIVEL7    = coalesce(t.c.value('(@Nivel7Codigo)', 'Varchar(256)' ), '' ) ,                
            CB_NIVEL8    = coalesce(t.c.value('(@Nivel8Codigo)', 'Varchar(256)' ), '' ) ,                
            CB_NIVEL9    = coalesce(t.c.value('(@Nivel9Codigo)', 'Varchar(256)' ), '' ) ,               
		    CB_PUESTO    = coalesce(t.c.value('(@PuestoCodigo)', 'Varchar(256)' ), '' ) ,               
			CB_TURNO    = coalesce(t.c.value('(@TurnoCodigo)', 'Varchar(256)' ), '' ) ,               
            CB_FECHAREG   = convert( varchar(20),coalesce(t.c.value('(@KarFecha)', 'Datetime' ), '1899-12-30' ) , 112 ) ,             
            DESCRIPCION     = coalesce(t.c.value('(@KarDescrip)', 'Varchar(256)' ), '' )     , 
            OBSERVACIONES     = coalesce(t.c.value('(@KarObserva)', 'Varchar(256)' ), '' )             
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
     set @Xml = ( 
         select         
            MOV3= 'CAMBIOAREAS',            
            EMPRESA, 
         ( select 
             EMPRESA,              
             count(*) Cuantos, 
                ( SELECT ClienteId, 
                        PersonaID, 
                        EMPLEADO, 
                        CB_NIVEL1, 
                        CB_NIVEL2, 
                        CB_NIVEL3, 
                        CB_NIVEL4, 
                        CB_NIVEL5, 
                        CB_NIVEL6, 
                        CB_NIVEL7, 
                        CB_NIVEL8, 
                        CB_NIVEL9, 
						CB_PUESTO, 
						CB_TURNO,
                        CB_FECHAREG,
                        DESCRIPCION,
                        OBSERVACIONES            
                        FROM #EQUIPO_TEMP  REGISTRO  where EMPLEADOS.EMPRESA = REGISTRO.EMPRESA   FOR XML PATH('INFO'), TYPE ) 
         from #EQUIPO_TEMP EMPLEADOS 
         where DATOS.EMPRESA = EMPLEADOS.EMPRESA 
              
            
        group by EMPRESA
         FOR XML AUTO, TYPE 
         ) 
         from #EQUIPO_TEMP DATOS 
        group by EMPRESA
         FOR XML PATH('DATOS')--, ROOT('MULTIPLE'), TYPE 
    ) 
                
    
    SET @ErrorID = 0 
    set @Error = '' 
    
    drop table #EQUIPO_TEMP     
    
end
go 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_GetXMLCambioContrato') AND type IN ( N'P', N'PC' ))
    drop procedure WFTransaccion_GetXMLCambioContrato
go
CREATE PROCEDURE WFTransaccion_GetXMLCambioContrato( 
    @TransID    INT,
    @Xml        XML OUTPUT ,
    @FileID        INT OUTPUT, 
    @Error        VARCHAR( 255 ) OUTPUT, 
    @MultiplesFileID Varchar(max) output )
AS
begin     
    DECLARE @ErrorID    INT
    DECLARE @Cargo    XML
    
    SELECT    @Cargo    = TransCargo
    FROM    T_WFTransacciones
    WHERE    TransID = @TransID    
    
    declare @ClienteID FolioGrande 
    set @FileID = 0 
        
    set @MultiplesFileID = '' 
    select @MultiplesFileID  = COALESCE(@MultiplesFileID+',' , '' ) + RTRIM( LTRIM( STR( FileID ) ) )
    from ( 
    select                      
            FileID    = coalesce(t.c.value('(@FileID)', 'INT' ), 0)                         
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
    select * into #EQUIPO_TEMP 
    from ( 
    select          
            ClienteID    = coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
            PersonaID    = coalesce(  t.c.value('(@PersonaID)', 'INT' ), 0),             
            EMPRESA    = dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
            EMPLEADO    = coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,    
            CB_CONTRAT    = coalesce(t.c.value('(@ContratoCodigo)', 'Varchar(256)' ), '' ) ,                                        
            CB_FECHA   = convert( varchar(20),coalesce(t.c.value('(@KarFecha)', 'Datetime' ), '1899-12-30' ) , 112 ) ,             
            CB_FEC_VEC   = convert( varchar(20),coalesce(t.c.value('(@EmpContratoFin)', 'Datetime' ), '1899-12-30' ) , 112 ) ,             
            CB_NOTA     = coalesce(t.c.value('(@KarDescrip)', 'Varchar(256)' ), '' )     , 
            CB_COMENTA     = coalesce(t.c.value('(@MotKarNombre)', 'Varchar(256)' ), '' )             
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
     set @Xml = ( 
         select         
            MOV3= 'CAMBIOCONTRATO',            
            EMPRESA, 
         ( select 
             EMPRESA,              
             count(*) Cuantos, 
                ( SELECT ClienteId, 
                        PersonaID, 
                        EMPLEADO, 
                        CB_CONTRAT,                         
                        CB_FECHA,
                        CB_FEC_CON=CB_FECHA, 
                        CB_FEC_VEC=case when CB_FEC_VEC ='2999-12-31' then '1899-12-30' else CB_FEC_VEC end , 
                        CB_COMENTA,
                        CB_NOTA             
                        FROM #EQUIPO_TEMP  REGISTRO  where EMPLEADOS.EMPRESA = REGISTRO.EMPRESA   FOR XML PATH('INFO'), TYPE ) 
         from #EQUIPO_TEMP EMPLEADOS 
         where DATOS.EMPRESA = EMPLEADOS.EMPRESA 
              
            
        group by EMPRESA
         FOR XML AUTO, TYPE 
         ) 
         from #EQUIPO_TEMP DATOS 
        group by EMPRESA
         FOR XML PATH('DATOS')--, ROOT('MULTIPLE'), TYPE 
    ) 
                
    
    SET @ErrorID = 0 
    set @Error = '' 
    
    drop table #EQUIPO_TEMP     
    
end
go 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_GetXMLCRUDParientes') AND type IN ( N'P', N'PC' ))
    drop procedure WFTransaccion_GetXMLCRUDParientes
go
CREATE PROCEDURE WFTransaccion_GetXMLCRUDParientes( 
    @TransID    INT,
    @Xml        XML OUTPUT ,
    @FileID        INT OUTPUT, 
    @Error        VARCHAR( 255 ) OUTPUT, 
    @MultiplesFileID Varchar(max) output )
AS
begin     
    DECLARE @ErrorID    INT
    DECLARE @Cargo    XML
    
    SELECT    @Cargo    = TransCargo
    FROM    T_WFTransacciones
    WHERE    TransID = @TransID    
    
    declare @ClienteID FolioGrande 
    set @FileID = 0 
        
    set @MultiplesFileID = '' 
    select @MultiplesFileID  = COALESCE(@MultiplesFileID+',' , '' ) + RTRIM( LTRIM( STR( FileID ) ) )
    from ( 
    select                      
            FileID    = coalesce(t.c.value('(@FileID)', 'INT' ), 0)                         
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
    select * into #EQUIPO_TEMP 
    from ( 
    select          
            ClienteID    = coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
            PersonaID    = coalesce(  t.c.value('(@PersonaID)', 'INT' ), 0),             
            EMPRESA    = dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
            EMPLEADO    = coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,    
			PA_FEC_NAC  = convert( varchar(20),coalesce(t.c.value('(@ParienteNacFecha)', 'Datetime' ), '1899-12-30' ) , 112 ), 
			ParienteTipo =   coalesce(  t.c.value('(@ParienteTipo)', 'INT' ), 0),          
			PA_FOLIO =   coalesce(  t.c.value('(@ParienteFolio)', 'INT' ), 0),          
			PA_RELACIO =   coalesce(  t.c.value('(@ParentescoID)', 'INT' ), 0),          
			PA_SEXO =  case  coalesce(  t.c.value('(@ParienteGenero)', 'INT' ), 0)
							when 2 then 'F' 
							else 'M' 
						end, 
			PA_APE_PAT    = coalesce(t.c.value('(@ParienteApePat)', 'Varchar(256)' ), '' ) ,                                        
			PA_APE_MAT    = coalesce(t.c.value('(@ParienteApeMat)', 'Varchar(256)' ), '' ) ,                                        
			PA_NOMBRES    = coalesce(t.c.value('(@ParienteNombres)', 'Varchar(256)' ), '' ) ,          								                             
			LLAVE =   coalesce(  t.c.value('(@Llave)', 'INT' ), 0)         
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla

	 delete from #EQUIPO_TEMP where ParienteTipo = 0 
     
     set @Xml = ( 
         select         
            MOV3= case ParienteTipo when 1 then 'PARIENTE_AGREGAR'
									when 2 then 'PARIENTE_MODIFICAR' 
									when 3 then 'PARIENTE_BORRAR' 
									ELSE 'PARIENTE_AGREGAR' 
					end,  
            EMPRESA, 
         ( select 
             EMPRESA,              
             count(*) Cuantos, 
                ( SELECT ClienteId, 
                        PersonaID, 
                        EMPLEADO, 
						PA_FEC_NAC , 
						PA_FOLIO	, 
						PA_RELACIO ,  
						PA_SEXO    ,  
						PA_APE_PAT,  
						PA_APE_MAT,  
						PA_NOMBRES,  
						LLAVE
                        FROM #EQUIPO_TEMP  REGISTRO  where EMPLEADOS.EMPRESA = REGISTRO.EMPRESA  and  REGISTRO.ParienteTipo = DATOS.ParienteTipo  FOR XML PATH('INFO'), TYPE ) 
         from #EQUIPO_TEMP EMPLEADOS 
         where DATOS.EMPRESA = EMPLEADOS.EMPRESA and DATOS.ParienteTipo = EMPLEADOS.ParienteTipo  and DATOS.ParienteTipo >0 
              
            
        group by EMPRESA, ParienteTipo
         FOR XML AUTO, TYPE 
         ) 
         from #EQUIPO_TEMP DATOS 
        group by EMPRESA, ParienteTipo
         FOR XML PATH('DATOS')--, ROOT('MULTIPLE'), TYPE 
    ) 
                
    
    SET @ErrorID = 0 
    set @Error = '' 
    
    drop table #EQUIPO_TEMP     
    
end
go 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_GetXMLAgregarAsegurado') AND type IN ( N'P', N'PC' ))
    drop procedure WFTransaccion_GetXMLAgregarAsegurado 
go
CREATE PROCEDURE WFTransaccion_GetXMLAgregarAsegurado( 
    @TransID    INT,
    @Xml        XML OUTPUT ,
    @FileID        INT OUTPUT, 
    @Error        VARCHAR( 255 ) OUTPUT, 
    @MultiplesFileID Varchar(max) output )
AS
begin     
    DECLARE @ErrorID    INT
    DECLARE @Cargo    XML
    
    SELECT    @Cargo    = TransCargo
    FROM    T_WFTransacciones
    WHERE    TransID = @TransID    
    
    declare @ClienteID FolioGrande 
    set @FileID = 0 
        
    set @MultiplesFileID = '' 
    select @MultiplesFileID  = COALESCE(@MultiplesFileID+',' , '' ) + RTRIM( LTRIM( STR( FileID ) ) )
    from ( 
    select                      
            FileID    = coalesce(t.c.value('(@FileID)', 'INT' ), 0)                         
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
    select * into #EQUIPO_TEMP 
    from ( 
    select          
            ClienteID    = coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
            PersonaID    = coalesce(  t.c.value('(@PersonaID)', 'INT' ), 0),             
            EMPRESA    = dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
                    EMPLEADO    = coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,    

                    PM_CODIGO    = coalesce(  t.c.value('(@SeguroCodigo)', 'varchar(6)' ), '') ,    
                    PV_REFEREN    = coalesce(  t.c.value('(@SeguVigReferen)', 'varchar(20)' ), '') ,    
            EP_ASEGURA    = coalesce(  t.c.value('(@AseguraTipo)', 'INT' ), 0) ,    
                    PA_FOLIO =   coalesce(  t.c.value('(@ParienteFolio)', 'INT' ), 0),      
                    PA_RELACIO =   coalesce(  t.c.value('(@ParentescoID)', 'INT' ), 0),          
                    EP_TIPO =   coalesce(  t.c.value('(@AseguraTipoPol)', 'INT' ), 0),          
                    EP_CFIJO =   coalesce(  t.c.value('(@AseguraCFijo)', 'numeric(15,2)' ), 0.00),          
                    EP_CERTIFI    = coalesce(  t.c.value('(@AseguraCertifi)', 'varchar(30)' ), '') ,                            
                    EP_FEC_INI  = convert( varchar(20),coalesce(t.c.value('(@AseguraInicial)', 'Datetime' ), '1899-12-30' ) , 112 ), 
                    EP_FEC_FIN  = convert( varchar(20),coalesce(t.c.value('(@AseguraFinal)', 'Datetime' ), '1899-12-30' ) , 112 ), 
                    EP_OBSERVA    = coalesce(  t.c.value('(@AseguraObserva)', 'varchar(255)' ), '') ,    
                    EP_END_ALT    = coalesce(  t.c.value('(@AseguraEndoso)', 'varchar(30)' ), '') ,                                            
                    LLAVE =   coalesce(  t.c.value('(@Llave)', 'INT' ), 0)         
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
        select * from #EQUIPO_TEMP 

     set @Xml = ( 
         select         
            MOV3= 'EMPSEGURO_AGREGAR',  
            EMPRESA, 
         ( select 
             EMPRESA,              
             count(*) Cuantos, 
                ( SELECT ClienteId, 
                        PersonaID, 
                        EMPLEADO,                           
                                        PM_CODIGO, 
                                        PV_REFEREN, 
                                        EP_ASEGURA, 
                                        PA_FOLIO, 
                                        PA_RELACIO, 
                                        EP_TIPO, 
                                        EP_CFIJO, 
                                        EP_CERTIFI, 
                                        EP_FEC_INI, 
                                        EP_FEC_FIN, 
                                        EP_OBSERVA, 
                                        EP_END_ALT,  
                                        LLAVE
                        FROM #EQUIPO_TEMP  REGISTRO  where EMPLEADOS.EMPRESA = REGISTRO.EMPRESA   FOR XML PATH('INFO'), TYPE ) 
         from #EQUIPO_TEMP EMPLEADOS 
         where DATOS.EMPRESA = EMPLEADOS.EMPRESA 
              
            
        group by EMPRESA
         FOR XML AUTO, TYPE 
         ) 
         from #EQUIPO_TEMP DATOS 
        group by EMPRESA
         FOR XML PATH('DATOS')--, ROOT('MULTIPLE'), TYPE 
    ) 
                
    
    SET @ErrorID = 0 
    set @Error = '' 
    
    drop table #EQUIPO_TEMP     
    
end
go

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_GetXMLChecadas') AND type IN ( N'P', N'PC' ))
    drop procedure WFTransaccion_GetXMLChecadas
go

CREATE PROCEDURE WFTransaccion_GetXMLChecadas( 
    @TransID    INT,
    @Xml        XML OUTPUT ,
    @FileID        INT OUTPUT, 
    @Error        VARCHAR( 255 ) OUTPUT, 
    @MultiplesFileID Varchar(max) output )
AS
begin     
    DECLARE @ErrorID    INT
    DECLARE @Cargo    XML
    
    SELECT    @Cargo    = TransCargo
    FROM    T_WFTransacciones
    WHERE    TransID = @TransID    
    
    declare @ClienteID FolioGrande 
    set @FileID = 0 
        
    set @MultiplesFileID = '' 
    select @MultiplesFileID  = COALESCE(@MultiplesFileID+',' , '' ) + RTRIM( LTRIM( STR( FileID ) ) )
    from ( 
    select                      
            FileID    = coalesce(t.c.value('(@FileID)', 'INT' ), 0)                         
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
    select * into #CHECADA_TEMP 
    from ( 
    select    
			      
            ClienteID    = coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
			Fecha   = convert( varchar(20),coalesce(t.c.value('(@Fecha)', 'Datetime' ), '1899-12-30' ) , 112 ) ,  
			--Fecha   = coalesce(t.c.value('(@Fecha)', 'Date' ), '1899-12-30' ) ,  
			EMPRESA    = dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
			Empleado    = coalesce(  t.c.value('(@Empleado)', 'INT' ), 0) ,    
            Hora    = coalesce(t.c.value('(@Hora)', 'Varchar(10)' ), '') ,
			Credencial  = coalesce(t.c.value('(@Credencial)', 'Varchar(10)' ), '') ,
			Reloj  =  coalesce(t.c.value('(@Reloj)', 'Varchar(10)' ), ''),
			Motivo    = coalesce(t.c.value('(@Motivo)', 'Varchar(10)' ), '')         
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
     set @Xml = ( 
         select         
            MOV3= 'CHECADASIMPLE',            
            EMPRESA, 
         ( select 
             EMPRESA,              
             count(*) Cuantos, 
                ( SELECT ClienteId, 
                        Fecha, 
                        Empleado, 
                        Hora, 
                        Credencial,
                        Reloj,
						Motivo      
                        FROM #CHECADA_TEMP  REGISTRO  where EMPLEADOS.EMPRESA = REGISTRO.EMPRESA   FOR XML PATH('INFO'), TYPE ) 
         from #CHECADA_TEMP EMPLEADOS 
         where DATOS.EMPRESA = EMPLEADOS.EMPRESA 
              
            
        group by EMPRESA
         FOR XML AUTO, TYPE 
         ) 
         from #CHECADA_TEMP DATOS 
        group by EMPRESA
         FOR XML PATH('DATOS')--, ROOT('MULTIPLE'), TYPE 
    ) 
                
    
    SET @ErrorID = 0 
    set @Error = '' 
    
    drop table #CHECADA_TEMP     
    
end
go

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_GetXMLIntercambioFestivo') AND type IN ( N'P', N'PC' ))
    drop procedure WFTransaccion_GetXMLIntercambioFestivo
go
CREATE PROCEDURE WFTransaccion_GetXMLIntercambioFestivo( 
    @TransID    INT,
    @Xml        XML OUTPUT ,
    @FileID        INT OUTPUT, 
    @Error        VARCHAR( 255 ) OUTPUT, 
    @MultiplesFileID Varchar(max) output )
AS
begin     
    DECLARE @ErrorID    INT
    DECLARE @Cargo    XML
    
    SELECT    @Cargo    = TransCargo
    FROM    T_WFTransacciones
    WHERE    TransID = @TransID    
    
    declare @ClienteID FolioGrande 
    set @FileID = 0 
        
    set @MultiplesFileID = '' 
    select @MultiplesFileID  = COALESCE(@MultiplesFileID+',' , '' ) + RTRIM( LTRIM( STR( FileID ) ) )
    from ( 
    select                      
            FileID    = coalesce(t.c.value('(@FileID)', 'INT' ), 0)                         
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
    select * into #EQUIPO_TEMP 
    from ( 
    select          
            ClienteID    = coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
            PersonaID    = coalesce(  t.c.value('(@PersonaID)', 'INT' ), 0),             
            EMPRESA    = dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
            EMPLEADO    = coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,                
            FECHAORIGINAL   = convert( varchar(20),coalesce(t.c.value('(@InterOriginal)', 'Datetime' ), '1899-12-30' ) , 112 ) ,             
            FECHANUEVA   = convert( varchar(20),coalesce(t.c.value('(@InterNueva)', 'Datetime' ), '1899-12-30' ) , 112 ) ,             
            NOTA     = coalesce(t.c.value('(@Nota)', 'Varchar(256)' ), '' )     , 
            COMENTA     = coalesce(t.c.value('(@Comentario)', 'Varchar(256)' ), '' )             
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
     set @Xml = ( 
         select         
            MOV3= 'INTERCAMBIOFESTIVO',            
            EMPRESA, 
         ( select 
             EMPRESA,              
             count(*) Cuantos, 
                ( SELECT ClienteId, 
                        PersonaID, 
                        EMPLEADO,                         
                        FECHAORIGINAL, 
                        FECHANUEVA, 
                        COMENTA,
                        NOTA             
                        FROM #EQUIPO_TEMP  REGISTRO  where EMPLEADOS.EMPRESA = REGISTRO.EMPRESA   FOR XML PATH('INFO'), TYPE ) 
         from #EQUIPO_TEMP EMPLEADOS 
         where DATOS.EMPRESA = EMPLEADOS.EMPRESA 
              
            
        group by EMPRESA
         FOR XML AUTO, TYPE 
         ) 
         from #EQUIPO_TEMP DATOS 
        group by EMPRESA
         FOR XML PATH('DATOS')
    ) 
                
    
    SET @ErrorID = 0 
    set @Error = '' 
    
    drop table #EQUIPO_TEMP     
    
end
go 
 

if exists (select * from sysobjects where id = object_id(N'WF_GetGlobalString') and xtype in (N'FN', N'IF', N'TF')) 
	drop function WF_GetGlobalString
go 

create function WF_GetGlobalString ( @GlobalNo int ) 
returns Formula 
as 
begin
     return coalesce( ( select  top 1 GL_FORMULA from GLOBAL where GL_CODIGO = @GlobalNo ) , '' ) 
end 

go 
if exists (select * from sysobjects where id = object_id(N'WF_GetGlobalBooleano') and xtype in (N'FN', N'IF', N'TF')) 
	drop function WF_GetGlobalBooleano
go 

create function WF_GetGlobalBooleano ( @GlobalNo int ) 
returns Booleano 
as 
begin
     return coalesce( ( select top 1 SUBSTRING(GL_FORMULA,1,1) from GLOBAL where GL_CODIGO = @GlobalNo ) , 'N' ) 
end 

go 

if exists (select * from sysobjects where id = object_id(N'WF_GetGlobalInteger') and xtype in (N'FN', N'IF', N'TF')) 
	drop function WF_GetGlobalInteger
go 

create function WF_GetGlobalInteger ( @GlobalNo int ) 
returns integer  
as 
begin
     declare @Number int 
	 declare @ValueStr Formula 
	 set @ValueStr =  coalesce( ( select top 1 GL_FORMULA  from GLOBAL where GL_CODIGO = @GlobalNo ) , '0' )
	 
	 if ISNUMERIC ( @ValueStr )> 0  
		return cast( @ValueStr as int ) 
	  
	return 0 
end 

go 


if exists (select * from sysobjects where id = object_id(N'WF_GetGlobalReal') and xtype in (N'FN', N'IF', N'TF')) 
	drop function WF_GetGlobalReal
go 

create function WF_GetGlobalReal ( @GlobalNo int ) 
returns Pesos  
as 
begin
     
	 declare @ValueStr Formula 
	 set @ValueStr =  coalesce( ( select top 1 GL_FORMULA  from GLOBAL where GL_CODIGO = @GlobalNo ) , '0' )
	 
	 if ISNUMERIC ( @ValueStr )> 0  
		return cast( @ValueStr as Decimal(15,2)  ) 
	  
	return 0 
end 
go 

IF EXISTS ( SELECT *  FROM SYSOBJECTS WHERE NAME = 'WF_GetGlobalesEmpleado'  AND XTYPE = 'TF' )
DROP FUNCTION dbo.WF_GetGlobalesEmpleado
go

create function  WF_GetGlobalesEmpleado() 
RETURNS @GlobalesEmpleado
  TABLE 
(
	Ciudad varchar(255) ,
	Estado varchar(255) ,
	Municipio varchar(255) ,
	Checa char(1),
	Nacion varchar(255) ,
	Sexo varchar(255) ,
	Credencial varchar(255) ,
	EstadoCivil varchar(255) ,
	ViveEn varchar(255) ,
	ViveCon varchar(255) ,
	MedioTransporte varchar(255) ,
	Estudio varchar(255) ,
	AutoSal char(1),
	ZonaGeografica varchar(1) ,
	Patron varchar(255) ,
	Salario Numeric(15,2) ,
	Contrato varchar(255) ,
	Puesto varchar(255) ,
	Clasifi varchar(255) ,
	Turno varchar(255) ,
	TablaSS varchar(255) ,
	Nivel1 varchar(255) ,
	Nivel2 varchar(255) ,
	Nivel3 varchar(255) ,
	Nivel4 varchar(255) ,
	Nivel5 varchar(255) ,
	Nivel6 varchar(255) ,
	Nivel7 varchar(255) ,
	Nivel8 varchar(255) ,
	Nivel9 varchar(255) ,
	Siguiente char(1) ,
	Regimen int ,
	Banco varchar(255) 
)  
as 
begin 

declare @K_GLOBAL_CIUDAD_EMPRESA  int 
declare @K_GLOBAL_ENTIDAD_EMPRESA  int 
declare @K_GLOBAL_DEF_MUNICIPIO  int 
declare @K_GLOBAL_DEF_CHECA_TARJETA int 
declare @K_GLOBAL_DEF_NACIONALIDAD  int 
declare @K_GLOBAL_DEF_SEXO  int 
declare @K_GLOBAL_DEF_LETRA_GAFETE  int 
declare @K_GLOBAL_DEF_ESTADO_CIVIL  int 
declare @K_GLOBAL_DEF_VIVE_EN   int 
declare @K_GLOBAL_DEF_VIVE_CON  int 
declare @K_GLOBAL_DEF_MEDIO_TRANSPORTE  int 
declare @K_GLOBAL_DEF_ESTUDIOS  int 
declare @K_GLOBAL_DEF_SALARIO_TAB  int 
declare @K_GLOBAL_ZONAS_GEOGRAFICAS  int 
declare @K_GLOBAL_DEF_REGISTRO_PATRONAL  int 
declare @K_GLOBAL_DEF_SALARIO_DIARIO  int 
declare @K_GLOBAL_DEF_CONTRATO  int 
declare @K_GLOBAL_DEF_PUESTO  int 
declare @K_GLOBAL_DEF_CLASIFICACION  int 
declare @K_GLOBAL_DEF_TURNO  int 
declare @K_GLOBAL_DEF_PRESTACIONES  int 
declare @K_GLOBAL_DEF_NIVEL_1  int 
declare @K_GLOBAL_DEF_NIVEL_2  int 
declare @K_GLOBAL_DEF_NIVEL_3  int 
declare @K_GLOBAL_DEF_NIVEL_4  int 
declare @K_GLOBAL_DEF_NIVEL_5  int 
declare @K_GLOBAL_DEF_NIVEL_6  int 
declare @K_GLOBAL_DEF_NIVEL_7  int 
declare @K_GLOBAL_DEF_NIVEL_8  int 
declare @K_GLOBAL_DEF_NIVEL_9  int 
declare @K_GLOBAL_NUM_EMP_AUTOMATICO  int 
declare @K_GLOBAL_DEF_REGIMEN  int 
declare @K_GLOBAL_DEF_BANCO  int 
    
set @K_GLOBAL_CIUDAD_EMPRESA           = 4; -- {Ciudad de Empresa}
set @K_GLOBAL_ENTIDAD_EMPRESA          = 5; -- {Entidad de Empresa}
set @K_GLOBAL_DEF_CHECA_TARJETA        = 89; -- { Default: Checa Tarjeta }
set @K_GLOBAL_DEF_NACIONALIDAD         = 90; -- { Default: Nacionalidad }
set @K_GLOBAL_DEF_SEXO                 = 91; -- { Default: Sexo }
set @K_GLOBAL_DEF_SALARIO_TAB          = 92; -- { Default: Salario por Tabulador }
set @K_GLOBAL_DEF_SALARIO_DIARIO       = 93; -- { Default: Salario Diario }
set @K_GLOBAL_DEF_LETRA_GAFETE         = 94; -- { Default: Letra de Gafete }
set @K_GLOBAL_DEF_ESTADO_CIVIL         = 95; -- { Default: C�digo de Estado Civil }
set @K_GLOBAL_DEF_VIVE_EN              = 96; -- { Default: C�digo de Vive En }
set @K_GLOBAL_DEF_VIVE_CON             = 97; -- { Default: C�digo de Vive Con }
set @K_GLOBAL_DEF_MEDIO_TRANSPORTE     = 98; -- { Default: C�digo de Medio de Transporte }
set @K_GLOBAL_DEF_ESTUDIOS             = 99; -- { Default: C�digo de Grado de Estudios }
set @K_GLOBAL_DEF_CONTRATO             = 100; --{ Default: Tipo de Contrato }
set @K_GLOBAL_DEF_PUESTO               = 101; --{ Default: C�digo de Puesto }
set @K_GLOBAL_DEF_CLASIFICACION        = 102; --{ Default: Clasificaci�n / Tabulador }
set @K_GLOBAL_DEF_TURNO                = 103; --{ Default: C�digo de Turno }
set @K_GLOBAL_DEF_PRESTACIONES         = 104; --{ Default: C�digo Tabla Prestaciones }
set @K_GLOBAL_DEF_REGISTRO_PATRONAL    = 105; --{ Default: C�digo Registro Patronal }
set @K_GLOBAL_DEF_NIVEL_1              = 106; --{ Default: C�digo de Nivel #1 }
set @K_GLOBAL_DEF_NIVEL_2              = 107; --{ Default: C�digo de Nivel #2 }
set @K_GLOBAL_DEF_NIVEL_3              = 108; --{ Default: C�digo de Nivel #3 }
set @K_GLOBAL_DEF_NIVEL_4              = 109; --{ Default: C�digo de Nivel #4 }
set @K_GLOBAL_DEF_NIVEL_5              = 110; --{ Default: C�digo de Nivel #5 }
set @K_GLOBAL_DEF_NIVEL_6              = 111; --{ Default: C�digo de Nivel #6 }
set @K_GLOBAL_DEF_NIVEL_7              = 112; --{ Default: C�digo de Nivel #7 }
set @K_GLOBAL_DEF_NIVEL_8              = 113; --{ Default: C�digo de Nivel #8 }
set @K_GLOBAL_DEF_NIVEL_9              = 114; --{ Default: C�digo de Nivel #9 }
set @K_GLOBAL_NUM_EMP_AUTOMATICO       = 116; -- { SI/NO se desea generar el # de Empleado autom�ticamente}
set @K_GLOBAL_DEF_MUNICIPIO            = 247;
set @K_GLOBAL_DEF_BANCO                =  311;
set @K_GLOBAL_DEF_REGIMEN              =  312;

insert into @GlobalesEmpleado 
select 
Ciudad            = dbo.WF_GetGlobalString( @K_GLOBAL_CIUDAD_EMPRESA  ), 
Estado            = dbo.WF_GetGlobalString( @K_GLOBAL_ENTIDAD_EMPRESA  ), 
Municipio         = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_MUNICIPIO  ), 
Checa             = dbo.WF_GetGlobalBooleano( @K_GLOBAL_DEF_CHECA_TARJETA ), 
Nacion            = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_NACIONALIDAD  ), 
Sexo              = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_SEXO  ), 
Credencial        = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_LETRA_GAFETE  ), 
EstadoCivil       = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_ESTADO_CIVIL  ), 
ViveEn            = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_VIVE_EN   ), 
ViveCon           = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_VIVE_CON  ), 
MedioTransporte   = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_MEDIO_TRANSPORTE  ), 
Estudio           = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_ESTUDIOS  ), 
AutoSal           = dbo.WF_GetGlobalBooleano( @K_GLOBAL_DEF_SALARIO_TAB  ), 
ZonaGeografica    = SUBSTRING( dbo.WF_GetGlobalString( @K_GLOBAL_ZONAS_GEOGRAFICAS),1,1 ), 
Patron            = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_REGISTRO_PATRONAL  ), 
Salario           = dbo.WF_GetGlobalReal( @K_GLOBAL_DEF_SALARIO_DIARIO  ), 
Contrato          = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_CONTRATO  ), 
Puesto            = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_PUESTO  ), 
Clasifi           = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_CLASIFICACION  ), 
Turno             = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_TURNO  ), 
TablaSS           = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_PRESTACIONES  ), 
Nivel1            = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_NIVEL_1  ), 
Nivel2            = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_NIVEL_2  ), 
Nivel3            = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_NIVEL_3  ), 
Nivel4            = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_NIVEL_4  ), 
Nivel5            = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_NIVEL_5  ), 
Nivel6            = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_NIVEL_6  ), 
Nivel7            = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_NIVEL_7  ), 
Nivel8            = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_NIVEL_8  ), 
Nivel9            = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_NIVEL_9  ), 
Siguiente         = dbo.WF_GetGlobalBooleano( @K_GLOBAL_NUM_EMP_AUTOMATICO  ), 
Regimen           = dbo.WF_GetGlobalInteger( @K_GLOBAL_DEF_REGIMEN  ), 
Banco             = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_BANCO  )


return 
end 

go 

if exists (select * from dbo.sysobjects where id = object_id(N'WF_isEmpty') and xtype in (N'FN', N'IF', N'TF')) 
	drop function WF_isEmpty 

go 

CREATE function WF_isEmpty( @var1 varchar(256),  @var2 varchar(256)  ) 
returns Varchar(256) 
as 
begin 	
	
	set @var1 = coalesce( @var1, '' ) 
	if ( @var1 = '' ) 
		return @var2 
	
	return @var1 

end

go 

if exists (select * from dbo.sysobjects where id = object_id(N'WF_GetCodigoEscolaridad') and xtype in (N'FN', N'IF', N'TF')) 
	drop function WF_GetCodigoEscolaridad
go 

CREATE function WF_GetCodigoEscolaridad( @iEscolaridad int ) 
returns Codigo2
as 
begin 	
  declare @Codigo Codigo2 
  declare @Numero Pesos 

  set @Numero = @iEscolaridad

  select  @Codigo = TB_CODIGO from  ESTUDIOS  where TB_NUMERO = @Numero 
  return @Codigo 

end
go 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_GetXMLAltaEmpleadoGenerica') AND type IN ( N'P', N'PC' ))
	drop procedure WFTransaccion_GetXMLAltaEmpleadoGenerica
go


CREATE  PROCEDURE WFTransaccion_GetXMLAltaEmpleadoGenerica( 
	@TransID	INT,
	@Xml		XML OUTPUT ,
	@FileID		INT OUTPUT, 
	@Error		VARCHAR( 255 ) OUTPUT)
AS
begin 	
	DECLARE @ErrorID	INT
	DECLARE @Cargo	XML
	
	SELECT	@Cargo	= TransCargo
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID	
	
	DECLARE @EMPRESA			varchar(10) 
	DECLARE @kCB_CODIGO			INT
	DECLARE @NOMBRE				Formula 
	DECLARE @KarFecha			Fecha 
	
	DECLARE @PersonaID			FolioGrande 		
	DECLARE @EquipoID			FolioGrande  	
	DECLARE @PersonaTipo		int 
	DECLARE @KarObservaciones	Titulo  
	DECLARE @kCB_AUTOSAL		Booleano
	DECLARE @kCB_AUTOSAL_GLOBAL Booleano

	DECLARE @kCB_APE_MAT	Descripcion
	DECLARE @kCB_APE_PAT	Descripcion
	DECLARE @kCB_NOMBRES	Descripcion


	DECLARE @kCB_BAN_ELE	Descripcion
	DECLARE @kCB_CALLE		Observaciones
	DECLARE @kCB_CARRERA	DescLarga
	DECLARE @kCB_CHECA		Booleano
	DECLARE @kCB_CIUDAD		Descripcion
	DECLARE @kCB_CLASIFI	Codigo
	DECLARE @kCB_CODPOST	Referencia
	DECLARE @kCB_COLONIA	Descripcion
	DECLARE @kCB_CONTRAT	Codigo1
	DECLARE @kCB_CREDENC	Codigo1
	DECLARE @kCB_CTA_VAL	Descripcion
	DECLARE @kCB_CURP		Descripcion
	DECLARE @kCB_E_MAIL		Formula
	DECLARE @kCB_ESTADO		Codigo2
	DECLARE @kCB_ESTUDIO	Codigo2
	DECLARE @kCB_FEC_ANT	Fecha
	DECLARE @kCB_FEC_CON	Fecha
	DECLARE @kCB_FEC_ING	Fecha
	DECLARE @kCB_FEC_NAC	Fecha
	DECLARE @kCB_LUG_NAC	Descripcion
	DECLARE @kCB_MUNICIP	Codigo
	DECLARE @kCB_NACION		Descripcion
	DECLARE @kCB_NIVEL0		Codigo
	DECLARE @kCB_NIVEL1		Codigo
	DECLARE @kCB_NIVEL2		Codigo
	DECLARE @kCB_NIVEL3		Codigo
	DECLARE @kCB_NIVEL4		Codigo
	DECLARE @kCB_NIVEL5		Codigo
	DECLARE @kCB_NIVEL6		Codigo
	DECLARE @kCB_NIVEL7		Codigo
	DECLARE @kCB_NIVEL8		Codigo
	DECLARE @kCB_NIVEL9		Codigo
	DECLARE @kCB_PATRON		RegPatronal
	DECLARE @kCB_PUESTO		Codigo
	DECLARE @kCB_RFC		Descripcion
	DECLARE @kCB_SALARIO	PesosDiario
	DECLARE @PersonaSalario PesosDiario
	DECLARE @kCB_SALARIO_BRUTO	PesosDiario
	DECLARE @kCB_SEGSOC		Descripcion
	DECLARE @kCB_SEXO		Codigo1
	DECLARE @kCB_TABLASS	Codigo1
	DECLARE @kCB_TEL		Descripcion
	DECLARE @kCB_TURNO		Codigo
	DECLARE @kCB_ZONA_GE	ZonaGeo

	---PADRES
	DECLARE @kNOM_PADRE	Descripcion
	DECLARE @kNOM_MADRE	Descripcion

	---- CAMPOS NUEVOS A MAPEAR
	DECLARE @kCB_AR_FEC as Fecha
	DECLARE @kCB_AR_HOR as Hora
	DECLARE @kCB_EDO_CIV as Codigo1
	DECLARE @kCB_FEC_RES as Fecha
	DECLARE @kCB_EST_HOR as Descripcion
	DECLARE @kCB_EST_HOY as Booleano
	DECLARE @kCB_EST_HOY_INT as Status
	DECLARE @kCB_EVALUA as DiasFrac
	DECLARE @kCB_EXPERIE as Titulo
	DECLARE @kCB_FEC_BAJ as Fecha
	DECLARE @kCB_FEC_BSS as Fecha
	DECLARE @kCB_FEC_INT as Fecha
	DECLARE @kCB_FEC_REV as Fecha
	DECLARE @kCB_FEC_VAC as Fecha
	DECLARE @kCB_G_FEC_1 as Fecha
	DECLARE @kCB_G_FEC_2 as Fecha
	DECLARE @kCB_G_FEC_3 as Fecha
	DECLARE @kCB_G_FEC_4 as Fecha
	DECLARE @kCB_G_FEC_5 as Fecha
	DECLARE @kCB_G_FEC_6 as Fecha
	DECLARE @kCB_G_FEC_7 as Fecha
	DECLARE @kCB_G_FEC_8 as Fecha
	DECLARE @kCB_G_LOG_1 as Booleano
	DECLARE @kCB_G_LOG_1_INT as Status
	DECLARE @kCB_G_LOG_2 as Booleano
	DECLARE @kCB_G_LOG_2_INT as Status
	DECLARE @kCB_G_LOG_3 as Booleano
	DECLARE @kCB_G_LOG_3_INT as Status
	DECLARE @kCB_G_LOG_4 as Booleano
	DECLARE @kCB_G_LOG_4_INT as Status
	DECLARE @kCB_G_LOG_5 as Booleano
	DECLARE @kCB_G_LOG_5_INT as Status
	DECLARE @kCB_G_LOG_6 as Booleano
	DECLARE @kCB_G_LOG_6_INT as Status
	DECLARE @kCB_G_LOG_7 as Booleano
	DECLARE @kCB_G_LOG_7_INT as Status
	DECLARE @kCB_G_LOG_8 as Booleano
	DECLARE @kCB_G_LOG_8_INT as Status
	DECLARE @kCB_G_NUM_1 as Pesos
	DECLARE @kCB_G_NUM_2 as Pesos
	DECLARE @kCB_G_NUM_3 as Pesos
	DECLARE @kCB_G_NUM_4 as Pesos
	DECLARE @kCB_G_NUM_5 as Pesos
	DECLARE @kCB_G_NUM_6 as Pesos
	DECLARE @kCB_G_NUM_7 as Pesos
	DECLARE @kCB_G_NUM_8 as Pesos
	DECLARE @kCB_G_NUM_9 as Pesos
	DECLARE @kCB_G_NUM10 as Pesos
	DECLARE @kCB_G_NUM11 as Pesos
	DECLARE @kCB_G_NUM12 as Pesos
	DECLARE @kCB_G_NUM13 as Pesos
	DECLARE @kCB_G_NUM14 as Pesos
	DECLARE @kCB_G_NUM15 as Pesos
	DECLARE @kCB_G_NUM16 as Pesos
	DECLARE @kCB_G_NUM17 as Pesos
	DECLARE @kCB_G_NUM18 as Pesos
	DECLARE @kCB_G_TAB_1 as Codigo
	DECLARE @kCB_G_TAB_2 as Codigo
	DECLARE @kCB_G_TAB_3 as Codigo
	DECLARE @kCB_G_TAB_4 as Codigo
	DECLARE @kCB_G_TAB_5 as Codigo
	DECLARE @kCB_G_TAB_6 as Codigo
	DECLARE @kCB_G_TAB_7 as Codigo
	DECLARE @kCB_G_TAB_8 as Codigo
	DECLARE @kCB_G_TAB_9 as Codigo
	DECLARE @kCB_G_TAB10 as Codigo
	DECLARE @kCB_G_TAB11 as Codigo
	DECLARE @kCB_G_TAB12 as Codigo
	DECLARE @kCB_G_TAB13 as Codigo
	DECLARE @kCB_G_TAB14 as Codigo
	DECLARE @kCB_G_TEX_1 as Descripcion
	DECLARE @kCB_G_TEX_2 as Descripcion
	DECLARE @kCB_G_TEX_3 as Descripcion
	DECLARE @kCB_G_TEX_4 as Descripcion
	DECLARE @kCB_G_TEX_5 as Descripcion
	DECLARE @kCB_G_TEX_6 as Descripcion
	DECLARE @kCB_G_TEX_7 as Descripcion
	DECLARE @kCB_G_TEX_8 as Descripcion
	DECLARE @kCB_G_TEX_9 as Descripcion
	DECLARE @kCB_G_TEX10 as Descripcion
	DECLARE @kCB_G_TEX11 as Descripcion
	DECLARE @kCB_G_TEX12 as Descripcion
	DECLARE @kCB_G_TEX13 as Descripcion
	DECLARE @kCB_G_TEX14 as Descripcion
	DECLARE @kCB_G_TEX15 as Descripcion
	DECLARE @kCB_G_TEX16 as Descripcion
	DECLARE @kCB_G_TEX17 as Descripcion
	DECLARE @kCB_G_TEX18 as Descripcion
	DECLARE @kCB_G_TEX19 as Descripcion
	DECLARE @kCB_HABLA as Booleano
	DECLARE @kCB_HABLA_INT as Status
	DECLARE @kCB_IDIOMA as Descripcion
	DECLARE @kCB_INFCRED as Descripcion
	DECLARE @kCB_INFMANT as Booleano
	DECLARE @kCB_INFMANT_INT as Status
	DECLARE @kCB_INFTASA as DescINFO
	DECLARE @kCB_LA_MAT as Descripcion
	DECLARE @kCB_LAST_EV as Fecha
	DECLARE @kCB_MAQUINA as Titulo
	DECLARE @kCB_MED_TRA as Codigo1
	DECLARE @kCB_PASAPOR as Booleano
	DECLARE @kCB_PASAPOR_INT as Status
	DECLARE @kCB_FEC_INC as Fecha
	DECLARE @kCB_FEC_PER as Fecha
	DECLARE @kCB_NOMYEAR as Anio
	DECLARE @kCB_NOMTIPO as NominaTipo
	DECLARE @kCB_NEXT_EV as Fecha
	DECLARE @kCB_NOMNUME as NominaNumero
	DECLARE @kCB_SAL_INT as PesosDiario
	DECLARE @kCB_VIVECON as Codigo1
	DECLARE @kCB_VIVEEN as Codigo2
	DECLARE @kCB_ZONA as Referencia
	DECLARE @kCB_INFTIPO as Status
	DECLARE @kCB_DER_FEC as Fecha
	DECLARE @kCB_DER_PAG as DiasFrac
	DECLARE @kCB_V_PAGO as DiasFrac
	DECLARE @kCB_DER_GOZ as DiasFrac
	DECLARE @kCB_V_GOZO as DiasFrac
	DECLARE @kCB_TIP_REV as Codigo
	DECLARE @kCB_MOT_BAJ as Codigo3
	DECLARE @kCB_FEC_SAL as Fecha
	DECLARE @kCB_INF_INI as Fecha
	DECLARE @kCB_INF_OLD as DescINFO
	DECLARE @kCB_OLD_SAL as PesosDiario
	DECLARE @kCB_OLD_INT as PesosDiario
	DECLARE @kCB_PRE_INT as PesosDiario
	DECLARE @kCB_PER_VAR as PesosDiario
	DECLARE @kCB_SAL_TOT as PesosDiario
	DECLARE @kCB_TOT_GRA as PesosDiario
	DECLARE @kCB_FAC_INT as Tasa
	DECLARE @kCB_RANGO_S as Tasa
	DECLARE @kCB_CLINICA as Codigo3
	DECLARE @kCB_FEC_NIV as Fecha
	DECLARE @kCB_FEC_PTO as Fecha
	DECLARE @kCB_AREA as Codigo
	DECLARE @kCB_FEC_TUR as Fecha
	DECLARE @kCB_FEC_KAR as Fecha
	DECLARE @kCB_PENSION as Status
	DECLARE @kCB_CANDIDA as FolioGrande
	DECLARE @kCB_ID_NUM as Descripcion
	DECLARE @kCB_ENT_NAC as Codigo2
	DECLARE @kCB_COD_COL as Codigo
	DECLARE @kCB_PASSWRD as Passwd
	DECLARE @kCB_PLAZA as FolioGrande
	DECLARE @kCB_FEC_PLA as Fecha
	DECLARE @kCB_DER_PV as DiasFrac
	DECLARE @kCB_V_PRIMA as DiasFrac
	DECLARE @kCB_SUB_CTA as Descripcion
	DECLARE @kCB_NETO as Pesos
	DECLARE @kCB_PORTAL as Booleano
	DECLARE @kCB_PORTAL_INT as Status
	DECLARE @kCB_DNN_OK as Booleano
	DECLARE @kCB_DNN_OK_INT as Status
	DECLARE @kCB_USRNAME as DescLarga
	DECLARE @kCB_NOMINA as NominaTipo
	DECLARE @kCB_FEC_NOM as Fecha
	DECLARE @kCB_RECONTR as Booleano
	DECLARE @kCB_RECONTR_INT as Status
	DECLARE @kCB_DISCAPA as Booleano
	DECLARE @kCB_DISCAPA_INT as Status
	DECLARE @kCB_INDIGE as Booleano
	DECLARE @kCB_INDIGE_INT as Status
	DECLARE @kCB_FONACOT as Descripcion
	DECLARE @kCB_EMPLEO as Booleano
	DECLARE @kCB_EMPLEO_INT as Status
	DECLARE @kUS_CODIGO as Usuario
	DECLARE @kCB_FEC_COV as Fecha
	DECLARE @kCB_G_TEX20 as Descripcion
	DECLARE @kCB_G_TEX21 as Descripcion
	DECLARE @kCB_G_TEX22 as Descripcion
	DECLARE @kCB_G_TEX23 as Descripcion
	DECLARE @kCB_G_TEX24 as Descripcion
	DECLARE @kCB_INFDISM as Booleano
	DECLARE @kCB_INFDISM_INT as Status
	DECLARE @kCB_INFACT as Booleano
	DECLARE @kCB_INFACT_INT as Status
	DECLARE @kCB_NUM_EXT as NombreCampo 
	DECLARE @kCB_NUM_INT as NombreCampo
	DECLARE @kCB_INF_ANT as Fecha
	DECLARE @kCB_TDISCAP as Status
	DECLARE @kCB_ESCUELA as DescLarga
	DECLARE @kCB_TESCUEL as Status
	DECLARE @kCB_TITULO as Status
	DECLARE @kCB_YTITULO as Anio
	DECLARE @kCB_CTA_GAS as Descripcion
	DECLARE @kCB_ID_BIO as FolioGrande
	DECLARE @kCB_GP_COD as Codigo
	DECLARE @kCB_TIE_PEN as Booleano
	DECLARE @kCB_TIE_PEN_INT as Status
	DECLARE @kCB_TSANGRE as Descripcion
	DECLARE @kCB_ALERGIA as Formula
	DECLARE @kCB_BRG_ACT as Booleano
	DECLARE @kCB_BRG_ACT_INT as Status
	DECLARE @kCB_BRG_TIP as Status
	DECLARE @kCB_BRG_ROL as Booleano
	DECLARE @kCB_BRG_ROL_INT as Status
	DECLARE @kCB_BRG_JEF as Booleano
	DECLARE @kCB_BRG_JEF_INT as Status
	DECLARE @kCB_BRG_CON as Booleano
	DECLARE @kCB_BRG_CON_INT as Status
	DECLARE @kCB_BRG_PRA as Booleano
	DECLARE @kCB_BRG_PRA_INT as Status
	DECLARE @kCB_BRG_NOP as Descripcion
	DECLARE @kCB_BANCO as Codigo
	DECLARE @kCB_REGIMEN as Status
	----- FIN	
	
	DECLARE @XMLTress  Varchar(max) 
	

	declare @ClienteID FolioGrande 
	set @FileID = 0 
	SET @ErrorID = 0 
	set @ERROR = '' 

	--Toma globales de Empleado 		
	select 
		@kCB_Ciudad = Ciudad , 
		@kCB_Estado = Estado , 
		@kCB_MUNICIP = Municipio , 
		@kCB_Checa = Checa , 
		@kCB_Nacion = Nacion , 
		@kCB_Sexo = Sexo , 
		@kCB_Credenc = Credencial , 
		@kCB_EDO_CIV = EstadoCivil , 
		@kCB_ViveEn = ViveEn , 
		@kCB_ViveCon = ViveCon , 
		@kCB_MED_TRA = MedioTransporte , 
		@kCB_Estudio = Estudio , 
		@kCB_AUTOSAL_GLOBAL = AutoSal , 
		@kCB_Zona = ZonaGeografica , 
		@kCB_Patron = Patron , 
		@kCB_Salario = Salario , 
		@kCB_Contrat = Contrato , 
		@kCB_Puesto = Puesto , 
		@kCB_Clasifi = Clasifi , 
		@kCB_Turno = Turno , 
		@kCB_TablaSS = TablaSS , 
		@kCB_Nivel1 = Nivel1 , 
		@kCB_Nivel2 = Nivel2 , 
		@kCB_Nivel3 = Nivel3 , 
		@kCB_Nivel4 = Nivel4 , 
		@kCB_Nivel5 = Nivel5 , 
		@kCB_Nivel6 = Nivel6 , 
		@kCB_Nivel7 = Nivel7 , 
		@kCB_Nivel8 = Nivel8 , 
		@kCB_Nivel9 = Nivel9 , 
		@kCB_Regimen = Regimen , 
		@KCB_Banco = Banco 
	from dbo.WF_GetGlobalesEmpleado() 

	SET	@ClienteID	= coalesce(  @Cargo.value('(//REGISTRO/@ClienteID)[1]', 'INT' ), 0) 
	SET @EMPRESA	= dbo.WFEmpresas_GetCodigoEmpresa( @ClienteID ) ; 
	
	SET	@PersonaID	= coalesce(  @Cargo.value('(//REGISTRO/@PersonaID)[1]', 'INT' ),  @TransID )     	
	
	--- IDENTIFICACION DEL EMPLEADO 
	SET	@KCB_APE_PAT		= coalesce( @Cargo.value('(//REGISTRO/@PersonaApePat)[1]', 'Varchar(256)' ), '' ) 
	SET	@KCB_APE_MAT		= coalesce( @Cargo.value('(//REGISTRO/@PersonaApeMat)[1]', 'Varchar(256)' ), '' ) 
	SET	@KCB_NOMBRES		= coalesce( @Cargo.value('(//REGISTRO/@PersonaNombres)[1]', 'Varchar(256)' ), '' ) 	
	SET @kCB_FEC_NAC  = coalesce(@Cargo.value('(//REGISTRO/@PersonaNacFecha)[1]', 'Date' ), '1899-12-30' ) 	
	SET	@KCB_RFC		= coalesce( @Cargo.value('(//REGISTRO/@PersonaRFC)[1]', 'Varchar(256)' ), '' ) 
	SET	@KCB_CURP		= coalesce( @Cargo.value('(//REGISTRO/@PersonaCURP)[1]', 'Varchar(256)' ), '' ) 
	SET	@kCB_SEGSOC		= coalesce( @Cargo.value('(//REGISTRO/@PersonaIMSS)[1]', 'Varchar(256)' ), '' ) 	
	SET	@KCB_SEXO		=case ( coalesce( @Cargo.value('(//REGISTRO/@PersonaGenero)[1]', 'int' ), -1 ) ) 
									when  1 then 'M' 
	                                when  2 then 'F'
									else  @KCB_SEXO 
						end;  
	SET @kCB_ENT_NAC = coalesce( @Cargo.value('(//REGISTRO/@NacEstadoCodigo)[1]', 'Varchar(2)' ), '' ) -- as Codigo2
	
	-- PERSONALES  
	SET @kCB_LUG_NAC   = coalesce(@Cargo.value('(//REGISTRO/@PersonaNacCiudad)[1]', 'Varchar(30)' ), '' ) 
	SET @kCB_NACION    = coalesce(@Cargo.value('(//REGISTRO/@PersonaNacionalidad)[1]', 'Varchar(30)' ), '' ) 	
	SET	@kCB_PASAPOR_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaPasaporte)[1]', 'INT' ), 0)	
	if ( @kCB_PASAPOR_INT = 1 ) 
		Set @kCB_PASAPOR = 'S' 
	else
		Set @kCB_PASAPOR = 'N'
	SET @kCB_EDO_CIV = coalesce( @Cargo.value('(//REGISTRO/@EdoCivilCodigo)[1]', 'Varchar(1)' ), @kCB_EDO_CIV ) --Estado Civil
	SET @kCB_LA_MAT = coalesce( @Cargo.value('(//REGISTRO/@PersonaLugarMat)[1]', 'Varchar(30)' ), '' ) --Lugar y Fecha de matrimonio
	SET @kNOM_PADRE = coalesce( @Cargo.value('(//REGISTRO/@PersonaNomPad)[1]', 'Varchar(30)' ), '' )
	SET @kNOM_MADRE = coalesce( @Cargo.value('(//REGISTRO/@PersonaNomMad)[1]', 'Varchar(30)' ), '' )
	SET @kCB_MED_TRA = coalesce( @Cargo.value('(//REGISTRO/@TransporteCodigo)[1]', 'Varchar(1)' ), @kCB_MED_TRA  ) --Transporte	
	SET	@kCB_DISCAPA_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaSufreDiscapa)[1]', 'INT' ), 0)	
	if ( @kCB_DISCAPA_INT = 1 ) 
		Set @kCB_DISCAPA = 'S' 
	else
		Set @kCB_DISCAPA = 'N'
	SET @kCB_TDISCAP = coalesce(@Cargo.value('(//REGISTRO/@PersonaDiscapa)[1]', 'INT' ), 0) -- as Status		
	--SET @kCB_INDIGE as Booleano
	SET	@kCB_INDIGE_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaIndigena)[1]', 'INT' ), 0)	
	if ( @kCB_INDIGE_INT = 1 ) 
		Set @kCB_INDIGE = 'S' 
	else
		Set @kCB_INDIGE = 'N'
		
	-- DOMICILIO 
	SET @kCB_CALLE = coalesce( @Cargo.value('(//REGISTRO/@DomicilioCalle)[1]', 'Varchar(50)' ), '' ) 
	SET @kCB_NUM_EXT = coalesce( @Cargo.value('(//REGISTRO/@DomicilioNumExt)[1]', 'Varchar(10)' ), '' ) 
	SET @kCB_NUM_INT = coalesce( @Cargo.value('(//REGISTRO/@DomicilioNumInt)[1]', 'Varchar(10)' ), '' ) 
	SET @kCB_COLONIA = coalesce( @Cargo.value('(//REGISTRO/@DomicilioColonia)[1]', 'Varchar(30)' ), '' ) 
	SET @kCB_CODPOST = coalesce( @Cargo.value('(//REGISTRO/@DomicilioCodPost)[1]', 'Varchar(8)' ), '' ) 
	SET @kCB_COD_COL = coalesce( @Cargo.value('(//REGISTRO/@ColoniaCodigo)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_CLINICA = coalesce( @Cargo.value('(//REGISTRO/@Clinica)[1]', 'Varchar(3)' ), '' ) -- as Codigo3
	SET @kCB_ZONA 	 =  dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@DomicilioZona)[1]', 'Varchar(8)' ), @kCB_ZONA )  -- as Referencia	
	SET	@KCB_CIUDAD	 =  dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@DomicilioCiudad)[1]', 'Varchar(30)' ), @kCB_Ciudad ) 	
	SET	@KCB_ESTADO	 =  dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@EstadoCodigo)[1]', 'Varchar(6)' ), @kCB_ESTADO ) 
	SET @kCB_MUNICIP =  dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@MunicipioCodigo)[1]', 'Varchar(6)' ), @kCB_MUNICIP ) 
	
	-- CONTACTO 
	SET @kCB_TEL = coalesce( @Cargo.value('(//REGISTRO/@PersonaTelefono)[1]', 'Varchar(30)' ), '' ) 
	SET @kCB_E_MAIL = coalesce( @Cargo.value('(//REGISTRO/@PersonaEmail)[1]', 'Varchar(255)' ), '' ) 
	SET @kCB_VIVECON = coalesce( @Cargo.value('(//REGISTRO/@ViveConCodigo)[1]', 'Varchar(1)' ), @kCB_ViveCon  )  -- as Codigo1
	SET @kCB_VIVEEN = coalesce( @Cargo.value('(//REGISTRO/@ViveEnCodigo)[1]', 'Varchar(2)' ),  @kCB_VIVEEN )  -- as Codigo2
	SET @kCB_FEC_RES = coalesce(@Cargo.value('(//REGISTRO/@ResidenciaFecha)[1]', 'Date' ), '1899-12-30' ) --Fecha Residencia 	

	--- EXPERIENCIA  
	-- Exp. Academica 
	SET @kCB_ESTUDIO =  dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@EstudioCodigo)[1]', 'Varchar(2)' ),  @kCB_VIVEEN )  -- as Codigo2
	SET @kCB_ESCUELA =  coalesce( @Cargo.value('(//REGISTRO/@PersonaEscuela)[1]', 'Varchar(40)' ), '' )	
	SET @kCB_TESCUEL = coalesce(@Cargo.value('(//REGISTRO/@PersonaEstTipo)[1]', 'INT' ), 0) -- as Status
	SET @kCB_CARRERA  = coalesce( @Cargo.value('(//REGISTRO/@PersonaCarrera)[1]', 'Varchar(40)' ), '' ) 		
	SET @kCB_TITULO = coalesce(@Cargo.value('(//REGISTRO/@PersonaTitulo)[1]', 'INT' ), 0) -- as Anio
	SET @kCB_YTITULO = coalesce(@Cargo.value('(//REGISTRO/@PersonaEstYear)[1]', 'INT' ), 0) -- as Anio
	SET @kCB_EST_HOR = coalesce( @Cargo.value('(//REGISTRO/@PersonaEstudiaHorario)[1]', 'Varchar(30)' ), '' ) --Carrera y Horario
	---DECLARE @kCB_EST_HOY as Booleano
	SET	@kCB_EST_HOY_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaEstudia)[1]', 'INT' ), 0)	
	if ( @kCB_EST_HOY_INT = 1 ) 
		Set @kCB_EST_HOY = 'S' 
	else
		Set @kCB_EST_HOY = 'N'

	-- Habilidades 
	SET	@kCB_HABLA_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaOtroIdioma)[1]', 'INT' ), 0)	
	if ( @kCB_HABLA_INT = 1 ) 
		Set @kCB_HABLA = 'S' 
	else
		Set @kCB_HABLA = 'N'
	SET @kCB_IDIOMA = coalesce( @Cargo.value('(//REGISTRO/@PersonaIdiomas)[1]', 'Varchar(30)' ), '' ) --Idioma o dominio
	SET @kCB_MAQUINA = coalesce( @Cargo.value('(//REGISTRO/@PersonaMaquinas)[1]', 'Varchar(100)' ), '' ) --Maquinas que conoce
	SET @kCB_EXPERIE = coalesce( @Cargo.value('(//REGISTRO/@PersonaExperiencia)[1]', 'Varchar(100)' ), '' )--Experiencia
	
	
	-- Contratacion 	
	SET @kCB_FEC_ING  = coalesce(@Cargo.value('(//REGISTRO/@EmpIngresoFecha)[1]', 'Date' ), '1899-12-30' ) 	
	SET @kCB_FEC_ANT  = coalesce(@Cargo.value('(//REGISTRO/@EmpAntigFecha)[1]', 'Date' ), '1899-12-30' ) 	
	
	-- Contrato 
	SET @kCB_FEC_CON  = coalesce(@Cargo.value('(//REGISTRO/@EmpContratoIni)[1]', 'Date' ), @kCB_FEC_ING ) 	
	SET	@KCB_CONTRAT  =  dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@ContratoCodigo)[1]', 'Varchar(6)' ), @KCB_CONTRAT ) 
		--Calcular el numero de dias para la fecha de vencimiento del contrato
		declare @DiasContrato as FolioChico
		SELECT @DiasContrato = TB_DIAS FROM CONTRATO WHERE TB_CODIGO = @kCB_CONTRAT
		if ( @DiasContrato > 0 )
		begin		 
			set @kCB_FEC_COV = cast( dateadd( day, @DiasContrato -1, @kCB_FEC_CON ) as Datetime ) 
		end
	
		
	SET	@KCB_PUESTO		= coalesce( @Cargo.value('(//REGISTRO/@PuestoCodigo)[1]', 'Varchar(6)' ), '' ) 
	SET @kCB_NOMINA 	= coalesce(@Cargo.value('(//REGISTRO/@TipoNominaCodigo)[1]', 'INT' ), @kCB_NOMINA) -- as NominaTipo
	
	-- Tomados del puesto en caso de que no esten definidos 
	----------------- TOMADOS DEL PUESTO  --------------------------------
	declare @kPU_PATRON Codigo1 
	declare @kPU_TURNO Codigo 
	declare @kPU_AUTOSAL Codigo 
	declare @kPU_TABLASS Codigo 
	declare @KPU_CLASIFI Codigo 
	
	select @KPU_CLASIFI = PU_CLASIFI, @kPU_PATRON = PU_PATRON, @kPU_TURNO = PU_TURNO,  @kPU_AUTOSAL  = PU_AUTOSAL, @kPU_TABLASS = PU_TABLASS from PUESTO where PU_CODIGO = @kCB_PUESTO 

	-- Si no estan definidas en el puesto tomas las default 
	set @kCB_AUTOSAL_GLOBAL = @KCB_AUTOSAL;
    set @kCB_PATRON  = case  coalesce(@kPU_PATRON,'')   when '' then @kCB_PATRON   else @kPU_PATRON  end
	set @kCB_TURNO = case  coalesce(@kPU_TURNO,'')  when '' then @kCB_TURNO  else @kPU_TURNO end	
	set @kCB_TABLASS = case  coalesce(@kPU_TABLASS,'')  when '' then @kCB_TABLASS  else @kPU_TABLASS end
	set @kCB_CLASIFI = case  coalesce(@KPU_CLASIFI,'')  when '' then @kCB_CLASIFI  else @KPU_CLASIFI end
	set @kCB_CLASIFI = coalesce( @kCB_CLASIFI, '' ); 

    -- Al ultimo lee los del Cargo 	
	SET	@KCB_CLASIFI 	= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@TabuladorCodigo)[1]', 'Varchar(6)' ), @KCB_CLASIFI ) 
	SET	@KCB_TURNO		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@TurnoCodigo)[1]', 'Varchar(6)' ), @KCB_TURNO ) 
	SET	@KCB_PATRON		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@RegPatronCodigo)[1]', 'Varchar(1)' ), @KCB_PATRON ) 	
		
	-- TimbradoNomina 
	SET @kCB_REGIMEN = coalesce(@Cargo.value('(//REGISTRO/@RegimenSAT)[1]', 'INT' ), @kCB_REGIMEN) -- as Status
	
	
	-- AREA 
	SET	@kCB_NIVEL1		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel1Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL1 ) 
	SET	@kCB_NIVEL2		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel2Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL2 ) 
	SET	@kCB_NIVEL3		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel3Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL3 ) 
	SET	@kCB_NIVEL4		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel4Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL4 ) 
	SET	@kCB_NIVEL5		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel5Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL5 ) 
	SET	@kCB_NIVEL6		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel6Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL6 ) 
	SET	@kCB_NIVEL7		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel7Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL7 ) 
	SET	@kCB_NIVEL8		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel8Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL8 ) 
	SET	@kCB_NIVEL9		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel9Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL9 ) 
	
	
    -- SALARIO 	
	SET	@KCB_TABLASS		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@PrestaLeyCodigo)[1]', 'Varchar(6)' ), @KCB_TABLASS ) 	
	SET	@KCB_AUTOSAL	=case coalesce( @Cargo.value('(//REGISTRO/@SalTabula)[1]', 'int' ),  -1 ) 
								    when  0 then 'N'
									when  1 then 'S'
									else @KCB_AUTOSAL
						end;	
	SET	@kCB_SALARIO	= coalesce( @Cargo.value('(//REGISTRO/@SalDiario)[1]', 'Decimal(15,2)' ), @kCB_SALARIO ) 
	SET @kCB_PER_VAR    = coalesce( @Cargo.value('(//REGISTRO/@PromVar)[1]', 'Decimal(15,2)' ), @kCB_SALARIO ) 
	SET	@KCB_ZONA_GE	= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@ZonaGeo)[1]', 'Varchar(1)' ), 'A' ) 	
	
	
	-- Datos del movimiento 
	SET @KarFecha  = coalesce(@Cargo.value('(//REGISTRO/@EmpIngresoFecha)[1]', 'Date' ), '1899-12-30' ) 		
	SET @KarObservaciones  = coalesce(@Cargo.value('(//REGISTRO/@KarObservaciones)[1]', 'Varchar(255)' ), '' ) 
	SET	@FileID	= coalesce(@Cargo.value('(//REGISTRO/@FileID)[1]', 'INT' ), 0)	
	
	SET	@KCB_CHECA		=case ( coalesce( @Cargo.value('(//REGISTRO/@PersonaCheca)[1]', 'int' ), -1 ) ) 
	                                when  0 then 'N'
									when  1 then 'S'
									else @KCB_CHECA
						end;  




	
	
	-----Campos que faltaban de mapear del XML "CargoAltaEmpleado.xml"  -------
	SET @kCB_CTA_VAL = coalesce( @Cargo.value('(//REGISTRO/@PersonaVale)[1]', 'Varchar(30)' ), '' )
	

	---- Campos Faltantes que no se encontraban en el XML "CargoAltaEmpleado.xml" para futuras Altas
	SET @kCB_AR_FEC = coalesce(@Cargo.value('(//REGISTRO/@PersonaArFec)[1]', 'Date' ), '1899-12-30' ) 	
	SET @kCB_AR_HOR = coalesce( @Cargo.value('(//REGISTRO/@PersonaArHor)[1]', 'Varchar(4)' ), '' )
	
	
	
	SET @kCB_EVALUA = coalesce( @Cargo.value('(//REGISTRO/@PersonaEvalua)[1]', 'Decimal(15,2)' ), 0.00 ) --Resultado Evaluacion
	
	SET @kCB_FEC_BAJ = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecBaj)[1]', 'Date' ), '1899-12-30' ) --Fecha Baja	
	SET @kCB_FEC_BSS = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecBss)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_FEC_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecInt)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_FEC_REV = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecRev)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_FEC_VAC = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecVac)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_1 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec1)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_2 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec2)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_3 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec3)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_4 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec4)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_5 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec5)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_6 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec6)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_7 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec7)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_8 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec8)[1]', 'Date' ), '1899-12-30' )
	
	----------------------------------------- VALORES ADICIONALES DE TRESS ---------------------------	
	--DECLARE @kCB_G_LOG_1 as Booleano
	SET	@kCB_G_LOG_1_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog1)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_1_INT = 1 ) 
		Set @kCB_G_LOG_1 = 'S' 
	else
		Set @kCB_G_LOG_1 = 'N'
	--DECLARE @kCB_G_LOG_2 as Booleano
	SET	@kCB_G_LOG_2_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog2)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_2_INT = 1 ) 
		Set @kCB_G_LOG_2 = 'S' 
	else
		Set @kCB_G_LOG_2 = 'N' 
	--DECLARE @kCB_G_LOG_3 as Booleano
	SET	@kCB_G_LOG_3_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog3)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_3_INT = 1 ) 
		Set @kCB_G_LOG_3 = 'S' 
	else
		Set @kCB_G_LOG_3 = 'N' 
	--DECLARE @kCB_G_LOG_4 as Booleano
	SET	@kCB_G_LOG_4_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog4)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_4_INT = 1 ) 
		Set @kCB_G_LOG_4 = 'S' 
	else
		Set @kCB_G_LOG_4 = 'N' 
	--DECLARE @kCB_G_LOG_5 as Booleano
	SET	@kCB_G_LOG_5_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog5)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_5_INT = 1 ) 
		Set @kCB_G_LOG_5 = 'S' 
	else
		Set @kCB_G_LOG_5 = 'N' 
	--DECLARE @kCB_G_LOG_6 as Booleano
	SET	@kCB_G_LOG_6_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog6)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_6_INT = 1 ) 
		Set @kCB_G_LOG_6 = 'S' 
	else
		Set @kCB_G_LOG_6 = 'N' 
	--DECLARE @kCB_G_LOG_7 as Booleano
	SET	@kCB_G_LOG_7_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog7)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_7_INT = 1 ) 
		Set @kCB_G_LOG_7 = 'S' 
	else
		Set @kCB_G_LOG_7 = 'N' 
	--DECLARE @kCB_G_LOG_8 as Booleano
	SET	@kCB_G_LOG_8_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog8)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_8_INT = 1 ) 
		Set @kCB_G_LOG_8 = 'S' 
	else
		Set @kCB_G_LOG_8 = 'N' 

	SET @kCB_G_NUM_1 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum1)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_2 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum2)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_3 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum3)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_4 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum4)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_5 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum5)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_6 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum6)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_7 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum7)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_8 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum8)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_9 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum9)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM10 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum10)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM11 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum11)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM12 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum12)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM13 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum13)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM14 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum14)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM15 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum15)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM16 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum16)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM17 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum17)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM18 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum18)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_TAB_1 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab1)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_2 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab2)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_3 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab3)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_4 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab4)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_5 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab5)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_6 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab6)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_7 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab7)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_8 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab8)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_9 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab9)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB10 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab10)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB11 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab11)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB12 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab12)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB13 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab13)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB14 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab14)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TEX_1  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText1)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_2  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText2)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_3  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText3)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_4  = coalesce( @Cargo.value('(//REGISTRO/@PersonaCLABE)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_5  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText5)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_6  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText6)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_7  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText7)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_8  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText8)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_9  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText9)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX10  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText10)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX11  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText11)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX12  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText12)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX13  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText13)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX14  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText14)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX15  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText15)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX16  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText16)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX17  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText17)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX18  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText18)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX19  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText19)[1]', 'Varchar(30)' ), '' ) -- as Descripcion		
	SET @kCB_G_TEX20  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText20)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX21  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText21)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX22  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText22)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX23  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText23)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX24  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText24)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	
	SET @kCB_INFCRED = coalesce( @Cargo.value('(//REGISTRO/@PersonaInfCred)[1]', 'Varchar(30)' ), '' ) --Credito Infonavit 
	---DECLARE @kCB_INFMANT as Booleano 
	SET	@kCB_INFMANT_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaInfMant)[1]', 'INT' ), 0)	
	if ( @kCB_INFMANT_INT = 1 ) 
		Set @kCB_INFMANT = 'S' 
	else
		Set @kCB_INFMANT = 'N'

	SET @kCB_INFTASA = coalesce( @Cargo.value('(//REGISTRO/@PersonaInfTasa)[1]', 'Decimal(15,2)' ), 0.00 ) --Valor Descuento Infonavit 
	
	SET @kCB_LAST_EV = coalesce(@Cargo.value('(//REGISTRO/@PersonaLastEv)[1]', 'Date' ), '1899-12-30' ) --Fecha de ultima evaluacion
	
	
	SET @kCB_FEC_INC = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecInc)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_FEC_PER = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecPer)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_NOMYEAR = coalesce(@Cargo.value('(//REGISTRO/@PersonaNomYear)[1]', 'INT' ), 0)
	SET @kCB_NOMTIPO = coalesce(@Cargo.value('(//REGISTRO/@PersonaNomTipo)[1]', 'INT' ), 0)
	

	SET @kCB_NEXT_EV = coalesce(@Cargo.value('(//REGISTRO/@PersonaNextEv)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_NOMNUME = coalesce(@Cargo.value('(//REGISTRO/@PersonaNomNume)[1]', 'INT' ), 0) -- as NominaNumero
	SET @kCB_SAL_INT = coalesce( @Cargo.value('(//REGISTRO/@PersonaSalInt)[1]', 'Decimal(15,2)' ), 0.00 ) -- as PesosDiario
	
	SET @kCB_INFTIPO = coalesce(@Cargo.value('(//REGISTRO/@PersonaNomNume)[1]', 'INT' ), 0)  -- as Status
	SET @kCB_DER_FEC = coalesce(@Cargo.value('(//REGISTRO/@PersonaDerFec)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_DER_PAG = coalesce( @Cargo.value('(//REGISTRO/@PersonaDerPag)[1]', 'Decimal(15,2)' ), 0.00 )  -- as DiasFrac
	SET @kCB_V_PAGO = coalesce( @Cargo.value('(//REGISTRO/@PersonaVPago)[1]', 'Decimal(15,2)' ), 0.00 )  -- as DiasFrac
	SET @kCB_DER_GOZ = coalesce( @Cargo.value('(//REGISTRO/@PersonaDerGoz)[1]', 'Decimal(15,2)' ), 0.00 )  -- as DiasFrac
	SET @kCB_V_GOZO = coalesce( @Cargo.value('(//REGISTRO/@PersonaVGozo)[1]', 'Decimal(15,2)' ), 0.00 )  -- as DiasFrac


	SET @kCB_TIP_REV = coalesce( @Cargo.value('(//REGISTRO/@PersonaTipRev)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_MOT_BAJ = coalesce( @Cargo.value('(//REGISTRO/@PersonaMotBaj)[1]', 'Varchar(3)' ), '' ) -- as Codigo3
	SET @kCB_FEC_SAL = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecSal)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_INF_INI = coalesce(@Cargo.value('(//REGISTRO/@PersonaInfIni)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_INF_OLD = coalesce( @Cargo.value('(//REGISTRO/@PersonaInfOld)[1]', 'Decimal(15,2)' ), 0.00 )  -- as DescINFO
	SET @kCB_OLD_SAL = coalesce( @Cargo.value('(//REGISTRO/@PersonaOldSal)[1]', 'Decimal(15,2)' ), 0.00 )  -- as PesosDiario
	SET @kCB_OLD_INT = coalesce( @Cargo.value('(//REGISTRO/@PersonaOldInt)[1]', 'Decimal(15,2)' ), 0.00 )  -- as PesosDiario
	SET @kCB_PRE_INT = coalesce( @Cargo.value('(//REGISTRO/@PersonaPreInt)[1]', 'Decimal(15,2)' ), 0.00 )  -- as PesosDiario
	SET @kCB_SAL_TOT = coalesce( @Cargo.value('(//REGISTRO/@PersonaSalTot)[1]', 'Decimal(15,2)' ), 0.00 )  -- as PesosDiario
	SET @kCB_TOT_GRA = coalesce( @Cargo.value('(//REGISTRO/@PersonaTotGra)[1]', 'Decimal(15,2)' ), 0.00 )  -- as PesosDiario
	SET @kCB_FAC_INT = coalesce( @Cargo.value('(//REGISTRO/@PersonaFacInt)[1]', 'Decimal(15,2)' ), 0.00 )  -- as Tasa
	SET @kCB_RANGO_S = coalesce( @Cargo.value('(//REGISTRO/@PersonaRangoS)[1]', 'Decimal(15,2)' ), 0.00 )  -- as Tasa
	
	SET @kCB_FEC_NIV = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecNiv)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_FEC_PTO = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecPto)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_AREA = coalesce( @Cargo.value('(//REGISTRO/@PersonaArea)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_FEC_TUR = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecTur)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_FEC_KAR = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecKar)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_PENSION = coalesce(@Cargo.value('(//REGISTRO/@PersonaPension)[1]', 'INT' ), 0) -- as Status

	SET @kCB_CANDIDA = coalesce(@Cargo.value('(//REGISTRO/@PersonaCandida)[1]', 'INT' ), 0) -- as FolioGrande
	SET @kCB_ID_NUM = coalesce( @Cargo.value('(//REGISTRO/@PersonaIdNum)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	
	
	SET @kCB_PASSWRD = coalesce( @Cargo.value('(//REGISTRO/@PersonaPasswrd)[1]', 'Varchar(30)' ), '' ) -- as Passwd
	SET @kCB_PLAZA = coalesce(@Cargo.value('(//REGISTRO/@PersonaPlaza)[1]', 'INT' ), 0) -- as FolioGrande
	SET @kCB_FEC_PLA = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecPla)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_DER_PV = coalesce( @Cargo.value('(//REGISTRO/@PersonaDerPv)[1]', 'Decimal(15,2)' ), 0.00 ) -- as DiasFrac
	SET @kCB_V_PRIMA = coalesce( @Cargo.value('(//REGISTRO/@PersonaVPrima)[1]', 'Decimal(15,2)' ), 0.00 ) -- as DiasFrac
	SET @kCB_SUB_CTA = coalesce( @Cargo.value('(//REGISTRO/@PersonaSubCta)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_NETO = coalesce( @Cargo.value('(//REGISTRO/@PersonaNeto)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	
	--SET @kCB_PORTAL as Booleano
	SET	@kCB_PORTAL_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaPortal)[1]', 'INT' ), 0)	
	if ( @kCB_PORTAL_INT = 1 ) 
		Set @kCB_PORTAL = 'S' 
	else
		Set @kCB_PORTAL = 'N' 
	--SET @kCB_DNN_OK as Booleano
	SET	@kCB_DNN_OK_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaDnnOk)[1]', 'INT' ), 0)	
	if ( @kCB_DNN_OK_INT = 1 ) 
		Set @kCB_DNN_OK = 'S' 
	else
		Set @kCB_DNN_OK = 'N' 

	SET @kCB_USRNAME = coalesce( @Cargo.value('(//REGISTRO/@PersonaUsrname)[1]', 'Varchar(40)' ), '' ) -- as DescLarga
	
	SET @kCB_FEC_NOM = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecNom)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	--SET @kCB_RECONTR as Booleano
	SET	@kCB_RECONTR_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaRecontr)[1]', 'INT' ), 0)	
	if ( @kCB_RECONTR_INT = 1 ) 
		Set @kCB_RECONTR = 'S' 
	else
		Set @kCB_RECONTR = 'N'

		
	SET @kCB_FONACOT = coalesce( @Cargo.value('(//REGISTRO/@PersonaFonacot)[1]', 'Varchar(30)' ), '' ) -- as Descripcion

	--SET @kCB_EMPLEO as Booleano
	SET	@kCB_EMPLEO_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaEmpleo)[1]', 'INT' ), 0)	
	if ( @kCB_EMPLEO_INT = 1 ) 
		Set @kCB_EMPLEO = 'S' 
	else
		Set @kCB_EMPLEO = 'N'

	SET @kUS_CODIGO = coalesce(@Cargo.value('(//REGISTRO/@PersonaUsCodigo)[1]', 'INT' ), 0) -- as Usuario
	
	--SET @kCB_INFDISM as Booleano
	SET	@kCB_INFDISM_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaInfDism)[1]', 'INT' ), 0)	
	if ( @kCB_INFDISM_INT = 1 ) 
		Set @kCB_INFDISM = 'S' 
	else
		Set @kCB_INFDISM = 'N'

	--SET @kCB_INFACT as Booleano
	SET	@kCB_INFACT_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaInfAct)[1]', 'INT' ), 0)	
	if ( @kCB_INFACT_INT = 1 ) 
		Set @kCB_INFACT = 'S' 
	else
		Set @kCB_INFACT = 'N'
	
	SET @kCB_INF_ANT = coalesce(@Cargo.value('(//REGISTRO/@PersonaInfAnt)[1]', 'Date' ), '1899-12-30' ) -- as Fecha 
	
	SET @kCB_CTA_GAS = coalesce( @Cargo.value('(//REGISTRO/@PersonaCtaGas)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_ID_BIO = coalesce(@Cargo.value('(//REGISTRO/@PersonaIdBio)[1]', 'INT' ), 0) -- as FolioGrande
	SET @kCB_GP_COD = coalesce( @Cargo.value('(//REGISTRO/@PersonaGpCod)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	--SET @kCB_TIE_PEN as Booleano
	SET	@kCB_TIE_PEN_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaTiePen)[1]', 'INT' ), 0)	
	if ( @kCB_TIE_PEN_INT = 1 ) 
		Set @kCB_TIE_PEN = 'S' 
	else
		Set @kCB_TIE_PEN = 'N'

	SET @kCB_TSANGRE = coalesce( @Cargo.value('(//REGISTRO/@PersonaTSangre)[1]', 'Varchar(6)' ), '' ) -- as Descripcion
	SET @kCB_ALERGIA = coalesce( @Cargo.value('(//REGISTRO/@PersonaAlergia)[1]', 'Varchar(255)' ), '' ) -- as Formula

	--SET @kCB_BRG_ACT as Booleano
	SET	@kCB_BRG_ACT_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaBrgAct)[1]', 'INT' ), 0)	
	if ( @kCB_BRG_ACT_INT = 1 ) 
		Set @kCB_BRG_ACT = 'S' 
	else
		Set @kCB_BRG_ACT = 'N'

	SET @kCB_BRG_TIP = coalesce(@Cargo.value('(//REGISTRO/@PersonaBgrTip)[1]', 'INT' ), 0) -- as Status
	--SET @kCB_BRG_ROL as Booleano
	SET	@kCB_BRG_ROL_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaBrgRol)[1]', 'INT' ), 0)	
	if ( @kCB_BRG_ROL_INT = 1 ) 
		Set @kCB_BRG_ROL = 'S' 
	else
		Set @kCB_BRG_ROL = 'N'

	--SET @kCB_BRG_JEF as Booleano
	SET	@kCB_BRG_JEF_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaBrgJef)[1]', 'INT' ), 0)	
	if ( @kCB_BRG_JEF_INT = 1 ) 
		Set @kCB_BRG_JEF = 'S' 
	else
		Set @kCB_BRG_JEF = 'N'

	--SET @kCB_BRG_CON as Booleano
	SET	@kCB_BRG_CON_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaBrgCon)[1]', 'INT' ), 0)	
	if ( @kCB_BRG_CON_INT = 1 ) 
		Set @kCB_BRG_CON = 'S' 
	else
		Set @kCB_BRG_CON = 'N'
	--SET @kCB_BRG_PRA as Booleano
	SET	@kCB_BRG_PRA_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaBrgPra)[1]', 'INT' ), 0)	
	if ( @kCB_BRG_PRA_INT = 1 ) 
		Set @kCB_BRG_PRA = 'S' 
	else
		Set @kCB_BRG_PRA = 'N'

	SET @kCB_BRG_NOP = coalesce( @Cargo.value('(//REGISTRO/@PersonaBgrNop)[1]', 'Varchar(6)' ), '' ) -- as Descripcion
	SET @kCB_BANCO = coalesce( @Cargo.value('(//REGISTRO/@PersonaBanco)[1]', 'Varchar(6)' ), @kCB_BANCO ) -- as Codigo
	
	
    if ( @ErrorID = 0 ) 
    begin 

	
	-- Si ya existe el empleado con los mismos nombres, rfc, fecha de Persona y correo  define el mismo numero de empleado 
	-- para que la validacion la realice el WFTress.dll 

	set @kCB_CODIGO = 0 
	set @kCB_SALARIO_BRUTO = 0 

	select @kCB_CODIGO = CB_CODIGO from COLABORA  where CB_NOMBRES = @kCB_NOMBRES and CB_APE_MAT = @kCB_APE_MAT and CB_APE_PAT = @kCB_APE_PAT 
													and CB_FEC_ING = @kCB_FEC_ING and CB_E_MAIL  = @kCB_E_MAIL  and CB_RFC     = @kCB_RFC


	set @kCB_CODIGO = coalesce (@kCB_CODIGO ,  0 ) 

	-- Se obtiene el numero de empleado mayor + 1 
	if ( @kCB_CODIGO = 0 ) 
		SELECT @kCB_CODIGO = CB_CODIGO + 1  FROM dbo.ULTIMO_EMPLEADO

	
	 set @Xml =(	 

	   select MOV3= 'ALTA_EMPLEADO'		, 
			@EMPRESA as EMPRESA,  ( 
	   select * from  ( 
		 SELECT 		
		        @PersonaID  as IngresoID , 
				@kCB_CODIGO  as CB_CODIGO  , 
				'S'  as CB_ACTIVO,		
				@kCB_AUTOSAL as CB_AUTOSAL , --Booleano
				@kCB_APE_MAT as CB_APE_MAT , --Descripcion
				@kCB_APE_PAT as CB_APE_PAT , --Descripcion
				@kCB_NOMBRES as CB_NOMBRES , --Descripcion
				--@kCB_BAN_ELE as CB_BAN_ELE , --Descripcion Se Quita por peticion de Roch
				@kCB_CALLE as CB_CALLE , --Observaciones
				@kCB_CARRERA as CB_CARRERA , --DescLarga
				@kCB_CHECA as CB_CHECA , --Booleano
				@kCB_CIUDAD as CB_CIUDAD , --Descripcion
				@kCB_CLASIFI as CB_CLASIFI , --Codigo
				@kCB_CODPOST as CB_CODPOST , --Referencia
				@kCB_COLONIA as CB_COLONIA , --Descripcion
				@kCB_CONTRAT as CB_CONTRAT , --Codigo1
				@kCB_CREDENC as CB_CREDENC , --Codigo1
				@kCB_CTA_VAL as CB_CTA_VAL , --Descripcion
				@kCB_CURP as CB_CURP , --Descripcion
				@kCB_E_MAIL as CB_E_MAIL , --Formula
				@kCB_ESTADO as CB_ESTADO , --Codigo2
				@kCB_ESTUDIO as CB_ESTUDIO , --Codigo2
				convert( varchar(20), @kCB_FEC_ANT , 112 )  as CB_FEC_ANT , --Fecha
				convert( varchar(20), @kCB_FEC_CON , 112 )  as CB_FEC_CON , --Fecha
				convert( varchar(20), @kCB_FEC_COV , 112 )  as CB_FEC_COV , --Fecha
				convert( varchar(20), @kCB_FEC_ING , 112 )  as CB_FEC_ING , --Fecha
				convert( varchar(20), @kCB_FEC_NAC , 112 )  as CB_FEC_NAC , --Fecha
				@kCB_LUG_NAC as CB_LUG_NAC , --Descripcion
				@kCB_MUNICIP as CB_MUNICIP , --Codigo
				@kCB_NACION as CB_NACION , --Descripcion
				@kCB_NIVEL0 as CB_NIVEL0 , --Codigo
				@kCB_NIVEL1 as CB_NIVEL1 , --Codigo
				@kCB_NIVEL2 as CB_NIVEL2 , --Codigo
				@kCB_NIVEL3 as CB_NIVEL3 , --Codigo
				@kCB_NIVEL4 as CB_NIVEL4 , --Codigo
				@kCB_NIVEL5 as CB_NIVEL5 , --Codigo
				@kCB_NIVEL6 as CB_NIVEL6 , --Codigo
				@kCB_NIVEL7 as CB_NIVEL7 , --Codigo
				@kCB_NIVEL8 as CB_NIVEL8 , --Codigo
				@kCB_NIVEL9 as CB_NIVEL9 , --Codigo
				@kCB_PATRON as CB_PATRON , --RegPatronal
				@kCB_PUESTO as CB_PUESTO , --Codigo
				@kCB_RFC as CB_RFC , --Descripcion
				@kCB_SALARIO as CB_SALARIO , --PesosDiario
				@kCB_SALARIO_BRUTO as CB_SALARIO_BRUTO , --PesosDiario Salario Bruto Mensual
				@kCB_SEGSOC as CB_SEGSOC , --Descripcion
				@kCB_SEXO as CB_SEXO , --Codigo1
				@kCB_TABLASS as CB_TABLASS , --Codigo1
				@kCB_TEL as CB_TEL , --Descripcion
				@kCB_TURNO as CB_TURNO , --Codigo
				@kCB_ZONA_GE as CB_ZONA_GE , --ZonaGeo										
				---Nuevos Campos Mapeados
				@kCB_EDO_CIV as CB_EDO_CIV , -- Codigo1
				convert( varchar(20), @kCB_FEC_RES , 112 )  as CB_FEC_RES , -- Fecha
				@kCB_EST_HOR as CB_EST_HOR , -- Descripcion
				@kCB_EST_HOY as CB_EST_HOY , -- Booleanos
				@kCB_EVALUA as CB_EVALUA , -- DiasFrac
				@kCB_EXPERIE as CB_EXPERIE , -- Titulo
				convert( varchar(20), @kCB_FEC_BAJ , 112 )    as CB_FEC_BAJ , -- Fecha
				convert( varchar(20), @kCB_FEC_BSS , 112 )    as CB_FEC_BSS , -- Fecha
				convert( varchar(20), @kCB_FEC_INT , 112 )    as CB_FEC_INT , -- Fecha
				convert( varchar(20), @kCB_FEC_REV , 112 )    as CB_FEC_REV , -- Fecha
				convert( varchar(20), @kCB_FEC_VAC , 112 )    as CB_FEC_VAC , -- Fecha
				@kCB_G_FEC_1 as CB_G_FEC_1 , -- Fecha
				@kCB_G_FEC_2 as CB_G_FEC_2 , -- Fecha
				@kCB_G_FEC_3 as CB_G_FEC_3 , -- Fecha
				@kCB_G_FEC_4 as CB_G_FEC_4 , -- Fecha
				@kCB_G_FEC_5 as CB_G_FEC_5 , -- Fecha
				@kCB_G_FEC_6 as CB_G_FEC_6 , -- Fecha
				@kCB_G_FEC_7 as CB_G_FEC_7 , -- Fecha
				@kCB_G_FEC_8 as CB_G_FEC_8 , -- Fecha
				@kCB_G_LOG_1 as CB_G_LOG_1 , -- Booleano
				@kCB_G_LOG_2 as CB_G_LOG_2 , -- Booleano
				@kCB_G_LOG_3 as CB_G_LOG_3 , -- Booleano
				@kCB_G_LOG_4 as CB_G_LOG_4 , -- Booleano
				@kCB_G_LOG_5 as CB_G_LOG_5 , -- Booleano
				@kCB_G_LOG_6 as CB_G_LOG_6 , -- Booleano
				@kCB_G_LOG_7 as CB_G_LOG_7 , -- Booleano
				@kCB_G_LOG_8 as CB_G_LOG_8 , -- Booleano
				@kCB_G_NUM_1 as CB_G_NUM_1 , -- Pesos
				@kCB_G_NUM_2 as CB_G_NUM_2 , -- Pesos
				@kCB_G_NUM_3 as CB_G_NUM_3 , -- Pesos
				@kCB_G_NUM_4 as CB_G_NUM_4 , -- Pesos
				@kCB_G_NUM_5 as CB_G_NUM_5 , -- Pesos
				@kCB_G_NUM_6 as CB_G_NUM_6 , -- Pesos
				@kCB_G_NUM_7 as CB_G_NUM_7 , -- Pesos
				@kCB_G_NUM_8 as CB_G_NUM_8 , -- Pesos
				@kCB_G_NUM_9 as CB_G_NUM_9 , -- Pesos
				@kCB_G_NUM10 as CB_G_NUM10 , -- Pesos
				@kCB_G_NUM11 as CB_G_NUM11 , -- Pesos
				@kCB_G_NUM12 as CB_G_NUM12 , -- Pesos
				@kCB_G_NUM13 as CB_G_NUM13 , -- Pesos
				@kCB_G_NUM14 as CB_G_NUM14 , -- Pesos
				@kCB_G_NUM15 as CB_G_NUM15 , -- Pesos
				@kCB_G_NUM16 as CB_G_NUM16 , -- Pesos
				@kCB_G_NUM17 as CB_G_NUM17 , -- Pesos
				@kCB_G_NUM18 as CB_G_NUM18 , -- Pesos
				@kCB_G_TAB_1 as CB_G_TAB_1 , -- Codigo
				@kCB_G_TAB_2 as CB_G_TAB_2 , -- Codigo
				@kCB_G_TAB_3 as CB_G_TAB_3 , -- Codigo
				@kCB_G_TAB_4 as CB_G_TAB_4 , -- Codigo
				@kCB_G_TAB_5 as CB_G_TAB_5 , -- Codigo
				@kCB_G_TAB_6 as CB_G_TAB_6 , -- Codigo
				@kCB_G_TAB_7 as CB_G_TAB_7 , -- Codigo
				@kCB_G_TAB_8 as CB_G_TAB_8 , -- Codigo
				@kCB_G_TAB_9 as CB_G_TAB_9 , -- Codigo
				@kCB_G_TAB10 as CB_G_TAB10 , -- Codigo
				@kCB_G_TAB11 as CB_G_TAB11 , -- Codigo
				@kCB_G_TAB12 as CB_G_TAB12 , -- Codigo
				@kCB_G_TAB13 as CB_G_TAB13 , -- Codigo
				@kCB_G_TAB14 as CB_G_TAB14 , -- Codigo
				@kCB_G_TEX_1 as CB_G_TEX_1 , -- Descripcion
				@kCB_G_TEX_2 as CB_G_TEX_2 , -- Descripcion
				@kCB_G_TEX_3 as CB_G_TEX_3 , -- Descripcion
				@kCB_G_TEX_4 as CB_G_TEX_4 , -- Descripcion
				@kCB_G_TEX_5 as CB_G_TEX_5 , -- Descripcion
				@kCB_G_TEX_6 as CB_G_TEX_6 , -- Descripcion
				@kCB_G_TEX_7 as CB_G_TEX_7 , -- Descripcion
				@kCB_G_TEX_8 as CB_G_TEX_8 , -- Descripcion
				@kCB_G_TEX_9 as CB_G_TEX_9 , -- Descripcion
				@kCB_G_TEX10 as CB_G_TEX10 , -- Descripcion
				@kCB_G_TEX11 as CB_G_TEX11 , -- Descripcion
				@kCB_G_TEX12 as CB_G_TEX12 , -- Descripcion
				@kCB_G_TEX13 as CB_G_TEX13 , -- Descripcion
				@kCB_G_TEX14 as CB_G_TEX14 , -- Descripcion
				@kCB_G_TEX15 as CB_G_TEX15 , -- Descripcion
				@kCB_G_TEX16 as CB_G_TEX16 , -- Descripcion
				@kCB_G_TEX17 as CB_G_TEX17 , -- Descripcion
				@kCB_G_TEX18 as CB_G_TEX18 , -- Descripcion
				@kCB_G_TEX19 as CB_G_TEX19 , -- Descripcion
				@kCB_HABLA as CB_HABLA , -- Booleano
				@kCB_IDIOMA as CB_IDIOMA , -- Descripcion
				@kCB_INFCRED as CB_INFCRED , -- Descripcion
				@kCB_INFMANT as CB_INFMANT , -- Booleano
				@kCB_INFTASA as CB_INFTASA , -- DescINFO
				@kCB_LA_MAT as CB_LA_MAT , -- Descripcion
				@kCB_LAST_EV as CB_LAST_EV , -- Fecha
				@kCB_MAQUINA as CB_MAQUINA , -- Titulo
				@kCB_MED_TRA as CB_MED_TRA , -- Codigo1
				@kCB_PASAPOR as CB_PASAPOR , -- Booleano				
				@kCB_VIVECON as CB_VIVECON , -- Codigo1
				@kCB_VIVEEN as CB_VIVEEN , -- Codigo2
				@kCB_ZONA as CB_ZONA , -- Referencia
				@kCB_TIP_REV as CB_TIP_REV , -- Codigo				
				@kCB_INF_INI as CB_INF_INI , -- Fecha
				@kCB_INF_OLD as CB_INF_OLD , -- DescINFO
				@kCB_OLD_SAL as CB_OLD_SAL , -- PesosDiario
				@kCB_OLD_INT as CB_OLD_INT , -- PesosDiario
				@kCB_PRE_INT as CB_PRE_INT , -- PesosDiario
				@kCB_PER_VAR as CB_PER_VAR , -- PesosDiario
				@kCB_SAL_TOT as CB_SAL_TOT , -- PesosDiario
				@kCB_TOT_GRA as CB_TOT_GRA , -- PesosDiario
				@kCB_FAC_INT as CB_FAC_INT , -- Tasa
				@kCB_RANGO_S as CB_RANGO_S , -- Tasa
				@kCB_CLINICA as CB_CLINICA , -- Codigo3																
				@kCB_CANDIDA as CB_CANDIDA , -- FolioGrande
				@kCB_ID_NUM as CB_ID_NUM , -- Descripcion
				@kCB_ENT_NAC as CB_ENT_NAC , -- Codigo2
				@kCB_COD_COL as CB_COD_COL , -- Codigo								
				@kCB_DER_PV as CB_DER_PV , -- DiasFrac
				@kCB_V_PRIMA as CB_V_PRIMA , -- DiasFrac
				@kCB_SUB_CTA as CB_SUB_CTA , -- Descripcion
				@kCB_NETO as CB_NETO , -- Pesos
				@kCB_NOMINA as CB_NOMINA , -- NominaTipo
				@kCB_RECONTR as CB_RECONTR , -- Booleano
				@kCB_DISCAPA as CB_DISCAPA , -- Booleano
				@kCB_INDIGE as CB_INDIGE , -- Booleano
				@kCB_FONACOT as CB_FONACOT , -- Descripcion
				@kCB_EMPLEO as CB_EMPLEO , -- Booleano								
				@kCB_G_TEX20 as CB_G_TEX20 , -- Descripcion
				@kCB_G_TEX21 as CB_G_TEX21 , -- Descripcion
				@kCB_G_TEX22 as CB_G_TEX22 , -- Descripcion
				@kCB_G_TEX23 as CB_G_TEX23 , -- Descripcion
				@kCB_G_TEX24 as CB_G_TEX24 , -- Descripcion
				@kCB_INFDISM as CB_INFDISM , -- Booleano
				@kCB_INFACT as CB_INFACT , -- Booleano
				@kCB_NUM_EXT as CB_NUM_EXT , -- NombreCampo
				@kCB_NUM_INT as CB_NUM_INT , -- NombreCampo
				@kCB_INF_ANT as CB_INF_ANT , -- Fecha
				@kCB_TDISCAP as CB_TDISCAP , -- Status
				@kCB_ESCUELA as CB_ESCUELA , -- DescLarga
				@kCB_TESCUEL as CB_TESCUEL , -- Status
				@kCB_TITULO as CB_TITULO , -- Status
				@kCB_YTITULO as CB_YTITULO , -- Anio
				@kCB_CTA_GAS as CB_CTA_GAS , -- Descripcion
				@kCB_ID_BIO as CB_ID_BIO , -- FolioGrande
				@kCB_GP_COD as CB_GP_COD , -- Codigo				
				@kCB_TSANGRE as CB_TSANGRE , -- Descripcion
				@kCB_ALERGIA as CB_ALERGIA , -- Formula
				@kCB_BRG_ACT as CB_BRG_ACT , -- Booleano
				@kCB_BRG_TIP as CB_BRG_TIP , -- Status
				@kCB_BRG_ROL as CB_BRG_ROL , -- Booleano
				@kCB_BRG_JEF as CB_BRG_JEF , -- Booleano
				@kCB_BRG_CON as CB_BRG_CON , -- Booleano
				@kCB_BRG_PRA as CB_BRG_PRA , -- Booleano
				@kCB_BRG_NOP as CB_BRG_NOP , -- Descripcion
				@kCB_BANCO as CB_BANCO ,	-- Codigo 
				@kCB_REGIMEN as CB_REGIMEN -- Status

		   )  REGISTRO    FOR XML AUTO, TYPE 
		   )   REGISTROS  FOR XML PATH('DATOS'), TYPE  
		   )  
		 	
		SET @ErrorID = 0 
		set @Error = '' 
	end
	
	-- Solo pruebas
	UPDATE T_Wftransacciones set TransCargoWFTress = @Xml where TransID = @transID 

	return @ErrorID
end


go 
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_GetXMLTRESS') AND type IN ( N'P', N'PC' ))
	drop procedure WFTransaccion_GetXMLTRESS
go


CREATE PROCEDURE WFTransaccion_GetXMLTRESS(
	@TransID	INT,
	@Xml		XML OUTPUT , 
	@FileID		INT OUTPUT , 	
	@Error		VARCHAR( 255 ) OUTPUT , 
	@MultiplesFileID  VARCHAR(Max) OUTPUT )
AS
BEGIN
	DECLARE	@VerboID	INT
	DECLARE	@ObjetoID	INT
	DECLARE @ErrorID	INT
	
	SET ARITHABORT OFF
	
	SELECT	@VerboID	= VerboID, @ObjetoID  = ObjetoID 
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID
	
	SET @ErrorID = 0 
	SET @FileID = 0 
	SET @MultiplesFileID  = '' 

	IF ( @VerboID = 1312 )
		EXEC @ErrorID = WFTransaccion_GetXMLVacaciones @TransID, @Xml output , @FileID output,  @Error output 
	ELSE
	IF ( @VerboID = 1304 )
		EXEC @ErrorID = WFTransaccion_GetXMLPermisos @TransID, @Xml output , @FileID output,  @Error output 		
	Else
	IF ( @VerboID = 67 ) and ( @ObjetoID = 1090 ) 
		EXEC @ErrorID = WFTransaccion_GetXMLAutorizarExtras @TransID, @Xml output , @FileID output,  @Error output 		
	Else	
	IF ( @VerboID = 1239 ) and ( @ObjetoID = 1060 ) 
		EXEC @ErrorID = WFTransaccion_GetXMLDocumentos @TransID, @Xml output , @FileID output,  @Error output , @MultiplesFileID output 	
	Else
	IF ( @VerboID = 1341 ) and ( @ObjetoID = 1068 ) 
		EXEC @ErrorID =  WFTransaccion_GetXMLDatosEmpleado @TransID, @Xml output , @FileID output,  @Error output 	
	Else	
	IF ( @VerboID = 1490 ) and ( @ObjetoID = 1107 ) 
		EXEC @ErrorID =  WFTransaccion_GetXMLInfonavit @TransID, @Xml output , @FileID output,  @Error output  , @MultiplesFileID output 		
	Else	
	IF ( @VerboID = 1077 ) and ( @ObjetoID = 1308 ) 
		EXEC @ErrorID =  WFTransaccion_GetXMLIncapacidades @TransID, @Xml output , @FileID output,  @Error output  , @MultiplesFileID output 		
	Else	
	IF ( @VerboID = 1469 ) and ( @ObjetoID = 1075 ) 
		EXEC @ErrorID =  WFTransaccion_GetXMLPrestamos @TransID, @Xml output , @FileID output,  @Error output  , @MultiplesFileID output 		
	Else	
	IF ( @VerboID = 86) and ( @ObjetoID = 1068) 
		EXEC @ErrorID =  WFTransaccion_GetXMLPuestoClasificacion @TransID, @Xml output , @FileID output,  @Error output  , @MultiplesFileID output 		
	Else	
	IF ( @VerboID = 94) and ( @ObjetoID = 1068) 
		EXEC @ErrorID =  WFTransaccion_GetXMLCambioTurno @TransID, @Xml output , @FileID output,  @Error output  , @MultiplesFileID output 		
	Else	
	IF ( @VerboID = 150) and ( @ObjetoID = 1068) 
		EXEC @ErrorID =  WFTransaccion_GetXMLCambioEquipo @TransID, @Xml output , @FileID output,  @Error output  , @MultiplesFileID output 		
	Else	
	IF ( @VerboID = 1282) and ( @ObjetoID = 1068) 
		EXEC @ErrorID =  WFTransaccion_GetXMLBajaEmpleado @TransID, @Xml output , @FileID output,  @Error output  , @MultiplesFileID output 		
	Else	
	IF ( @VerboID = 90) and ( @ObjetoID = 1068) 
		EXEC @ErrorID =  WFTransaccion_GetXMLCambioSalario @TransID, @Xml output , @FileID output,  @Error output  , @MultiplesFileID output 		
	Else
	IF ( @VerboID = 106) and ( @ObjetoID = 1068) 
		EXEC @ErrorID =  WFTransaccion_GetXMLCambioAreas @TransID, @Xml output , @FileID output,  @Error output  , @MultiplesFileID output 		
	Else
	IF ( @VerboID = 98) and ( @ObjetoID = 1068) 
		EXEC @ErrorID =  WFTransaccion_GetXMLCambioContrato @TransID, @Xml output , @FileID output,  @Error output  , @MultiplesFileID output 		
	Else
	IF ( @VerboID = 1285) and ( @ObjetoID = 1071) 
		EXEC @ErrorID =  WFTransaccion_GetXMLCRUDParientes @TransID, @Xml output , @FileID output,  @Error output  , @MultiplesFileID output 		
	Else	
	IF ( @VerboID = 1456 ) and ( @ObjetoID = 1096 ) 
		EXEC @ErrorID = WFTransaccion_GetXMLExcepciones @TransID, @Xml output , @FileID output,  @Error output 	
	Else
	IF ( @VerboID = 2085) and ( @ObjetoID = 1213) 
            EXEC @ErrorID =  WFTransaccion_GetXMLAgregarAsegurado @TransID, @Xml output , @FileID output,  @Error output  , @MultiplesFileID output           
        Else
	IF ( @VerboID = 100269) and ( @ObjetoID = 1051) 
            EXEC @ErrorID =  WFTransaccion_GetXMLIntercambioFestivo @TransID, @Xml output , @FileID output,  @Error output  , @MultiplesFileID output        
	Else 
 	IF ( @VerboID = 2138) and ( @ObjetoID = 1222) 
            EXEC @ErrorID =  WFTransaccion_GetXMLChecadas @TransID, @Xml output , @FileID output,  @Error output  , @MultiplesFileID output        
	Else  
	IF ( @VerboID = 1281) and ( @ObjetoID = 1068 ) 
            EXEC @ErrorID =  WFTransaccion_GetXMLAltaEmpleadoGenerica @TransID, @Xml output , @FileID output,  @Error output 
        Else
        IF ( @VerboID = 80) and ( @ObjetoID = 1068 ) 
            EXEC @ErrorID =  WFTransaccion_GetXMLReingresoEmpleado @TransID, @Xml output , @FileID output,  @Error output 
        Else    
	begin 
		set @Xml = '' -- No aplica para el Transaccion / no ES ERROR 
		set @Error = '' 
	end 
	

	RETURN @ErrorID
END

GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFFile_GrabaDocumentoInfonavit') AND type IN ( N'P', N'PC' ))
	drop procedure WFFile_GrabaDocumentoInfonavit
go



create procedure WFFile_GrabaDocumentoInfonavit(@File Imagen, @Nombre Descripcion , @Ext Codigo, @CB_CODIGO NumeroEmpleado, @KI_FECHA Fecha, @KI_TIPO Status,  @Usuario Usuario) 
as 
begin 
	set nocount on 
					
	update KARINF set KI_D_EXT = @Ext, KI_D_NOM = @Nombre, KI_D_BLOB = @File 
	where CB_CODIGO = @CB_CODIGO and KI_FECHA = @KI_FECHA  
		
end

go 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFFile_GrabaFoto') AND type IN ( N'P', N'PC' ))
	drop procedure WFFile_GrabaFoto
go

create procedure WFFile_GrabaFoto(@File Imagen, @CB_CODIGO NumeroEmpleado, @Usuario Usuario) 
as 
begin 
	set nocount on 		
	
	update IMAGEN set IM_BLOB = @File, IM_OBSERVA = 'Actualizada por WFN' where CB_CODIGO = @CB_CODIGO  and IM_TIPO = 'FOTO' 

	if ( @@ROWCOUNT = 0  ) 
	begin 
		insert into IMAGEN ( CB_CODIGO,  IM_TIPO, IM_BLOB, IM_OBSERVA ) values ( @CB_CODIGO, 'FOTO' , @File, 'Ingresada por WFN' ) 
	end 	
		
end

go 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFFile_GrabaTRESS') AND type IN ( N'P', N'PC' ))
	drop procedure WFFile_GrabaTRESS
go

CREATE PROCEDURE WFFile_GrabaTRESS(
	@TransID	INT,	
	@FileID		INT, 	
	@File		Imagen, 	
	@Nombre		Formula, 
	@Ext		Codigo,	
	@Usuario	Usuario, 
	@Error		VARCHAR( 255 ) OUTPUT)
AS
BEGIN
	DECLARE	@VerboID	INT
	DECLARE	@ObjetoID	INT
	DECLARE @ErrorID	INT
	DECLARE @Cargo	XML

	DECLARE @EMPRESA  varchar(10) 
	DECLARE @CB_CODIGO	INT	
	DECLARE @DocumentoNombre Varchar(255)
	declare @ClienteID FolioGrande 
	
	SET ARITHABORT OFF
	
	
	SELECT	@VerboID	= VerboID, @ObjetoID  = ObjetoID,  
	@Cargo	= TransCargo
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID
	
	SET @ErrorID = 0 	
	
	SET @Ext =  RTRIM( REPLACE( @Ext, '.', '' ) ) 

	IF ( @VerboID = 1312 )
	begin 
	
		SET	@ClienteID	= coalesce(  @Cargo.value('(//REGISTRO/@ClienteID)[1]', 'INT' ), 0) 
		SET @EMPRESA	= dbo.WFEmpresas_GetCodigoEmpresa( @ClienteID ) ; 
		SET	@CB_CODIGO	= coalesce(  @Cargo.value('(//REGISTRO/@EmpNumero)[1]', 'INT' ), 0) 
		
		declare @hoy Fecha 
		declare @fecha Fecha 
		declare @hora Hora
		declare @ClaseDoc Codigo 
		
		set @ClaseDoc = '017'
		set @hoy = GETDATE()
		set @fecha = DATEADD(dd, 0, DATEDIFF(dd, 0,@hoy))
		set @hora =  REPLACE( convert(varchar(5), @hoy , 108), ':', '' ) 		
		
		insert into DOCUMENTO (CB_CODIGO, DO_TIPO, DO_BLOB, DO_NOMBRE, DO_EXT ) 
		values ( @CB_CODIGO, 'WF'+@EMPRESA, @File, @Nombre, @Ext ) 
										
	end 	
	else
	if ( @VerboID = 1490 ) and ( @ObjetoID = 1107 ) 
	begin 

		declare @InfoFecha Fecha 
		declare @InfoTipo  Status 

		SET	@CB_CODIGO	= coalesce(  @Cargo.value('(//REGISTRO/@EmpNumero)[1]', 'INT' ), 0) 					
		select 
				@CB_CODIGO		 = x.value(N'@EmpNumero', N'int') , 				
				@ClienteID		 = x.value(N'@ClienteID', N'int'), 
				@InfoFecha        = x.value(N'@InfoFecha', N'Datetime'), 
				@InfoTipo =		   x.value(N'@InfoTipo', N'int')
		from @Cargo.nodes(N'//REGISTRO[@FileID=sql:variable("@FileID")]') t(x)						

		SET @EMPRESA	= dbo.WFEmpresas_GetCodigoEmpresa( @ClienteID ) ; 

		set @Nombre = coalesce( @Nombre, '' ) 
		set @Nombre = left( @Nombre, 30 ) 
	
		exec WFFile_GrabaDocumentoInfonavit @File, @Nombre, @Ext, @CB_CODIGO, @InfoFecha, @InfoTipo, @Usuario 		
	end 
	else
	begin
		-- Se asume que es el cargo de carga de documento  
		SET	@CB_CODIGO	= coalesce(  @Cargo.value('(//REGISTRO/@EmpNumero)[1]', 'INT' ), 0) 	
		declare @TipoDocCodigo varchar(10) 
		
		select 
				@CB_CODIGO		 = x.value(N'@EmpNumero', N'int') , 
				@TipoDocCodigo	 = x.value(N'@TipoDocCodigo', N'varchar(10)') , 
				@ClienteID		 = x.value(N'@ClienteID', N'int'), 
				@Ext             = x.value(N'@DocumentoExt', N'varchar(6)'), 
				@DocumentoNombre = x.value(N'@DocumentoNombre', N'varchar(255)')
		from @Cargo.nodes(N'//REGISTRO[@FileID=sql:variable("@FileID")]') t(x)
						

		SET @EMPRESA	= dbo.WFEmpresas_GetCodigoEmpresa( @ClienteID ) ; 


		        
		if ( @TipoDocCodigo = 'FOTO'  ) 
		begin 
			exec WFFile_GrabaFoto @File, @CB_CODIGO, @Usuario
		end
		else			
		begin
			insert into DOCUMENTO (CB_CODIGO, DO_TIPO, DO_BLOB, DO_NOMBRE, DO_EXT ) 
			values ( @CB_CODIGO, @TipoDocCodigo, @File, @DocumentoNombre, @Ext ) 
		end
			
	end 

	RETURN @ErrorID
END


GO 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_GetXMLPlugin') AND type IN ( N'P', N'PC' ))
	drop procedure WFTransaccion_GetXMLPlugin
go


CREATE PROCEDURE WFTransaccion_GetXMLPlugin(
	@TransID	INT,
	@Xml		XML OUTPUT , 
	@Plugin		VARCHAR(1024) OUTPUT, 
	@FileID		INT OUTPUT , 	
	@Error		VARCHAR( 255 ) OUTPUT)
AS
BEGIN
	DECLARE	@VerboID	INT
	DECLARE @ErrorID	INT
	
	SET ARITHABORT OFF
	
	SELECT	@VerboID	= VerboID,  @Xml = TransCargo
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID
	
	SET @ErrorID = 0 
	SET @FileID = 0 
	SET @Error = '' 

    IF ( @VerboID = 9998 )	
		set  @Plugin = 'WFTest.dll'
	ELSE
	IF ( @VerboID = 10000 )
		set  @Plugin = 'WFCompaq.dll'
	Else	
	begin 
		set @Xml = '<DATOS/>'
		set @Plugin = '' 
		set @Error = 'No existe plugin correspondiente al Verbo  #'+ RTRIM ( LTRIM( STR( @VerboID ) ) ) 
		set @ErrorID = 100
	end
	

	RETURN @ErrorID
END
go

/*Version de conceptos*/
IF ( not EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE  TABLE_NAME = 'KAR_CONCEP')) 
BEGIN
	/* Historial de Cambios a conceptos  */ 
	CREATE TABLE KAR_CONCEP(	
	CO_NUMERO dbo.Concepto ,
	KC_VERSION FolioGrande,
	US_FEC_MOD Fecha, 
	US_CODIGO  Usuario, 
	CO_A_PTU dbo.Booleano ,
	CO_ACTIVO dbo.Booleano ,
	CO_CALCULA dbo.Booleano ,
	CO_DESCRIP dbo.Descripcion ,
	CO_FORMULA dbo.Formula ,
	CO_G_IMSS dbo.Booleano ,
	CO_G_ISPT dbo.Booleano ,
	CO_IMP_CAL dbo.Formula ,
	CO_IMPRIME dbo.Booleano ,
	CO_LISTADO dbo.Booleano ,
	CO_MENSUAL dbo.Booleano ,		
	CO_QUERY dbo.Condicion ,
	CO_RECIBO dbo.Formula ,
	CO_TIPO dbo.Status ,
	CO_X_ISPT dbo.Formula ,
	CO_SUB_CTA dbo.Descripcion ,
	CO_CAMBIA dbo.Booleano ,
	CO_D_EXT dbo.Codigo ,
	CO_D_NOM dbo.Descripcion ,
	CO_LIM_SUP dbo.Pesos ,
	CO_LIM_INF dbo.Pesos ,
	CO_VER_INF dbo.Booleano ,
	CO_VER_SUP dbo.Booleano ,
	CO_VER_ACC dbo.Booleano ,
	CO_GPO_ACC dbo.ListaContenido ,
	CO_ISN dbo.Booleano ,
	CO_SUMRECI dbo.Booleano ,
	CO_FRM_ALT dbo.Formula ,
	CO_USO_NOM dbo.Status ,
	CO_SAT_CLP dbo.Status ,
	CO_SAT_TPP dbo.Codigo ,
	CO_SAT_CLN dbo.Status ,
	CO_SAT_TPN dbo.Codigo ,
	CO_SAT_EXE dbo.Status ,
	CO_SAT_CON dbo.Concepto ,	
	CO_PS_TIPO dbo.Status ,
 CONSTRAINT PK_KAR_CONCEP PRIMARY KEY CLUSTERED 
(
	CO_NUMERO ASC, 
	KC_VERSION ASC
)
)
END
GO

IF ( not EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE  TABLE_NAME = 'CONCEPTOVR')) 
BEGIN
	/* Versiones de Concepto - tabla hermana de Conceptos  */ 
	CREATE TABLE CONCEPTOVR(
		CO_NUMERO Concepto, 
		KC_VERSION FolioGrande,
		US_FEC_MOD Fecha, 
		US_CODIGO  Usuario, 

		CONSTRAINT PK_CONCEPTOVR PRIMARY KEY CLUSTERED 
		(
		CO_NUMERO ASC 
		)
	)
END
GO

IF ( not EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE  TABLE_NAME = 'CONCEPTOVT')) 
BEGIN
	/* Version Total de Conceptos  */ 
	CREATE TABLE CONCEPTOVT(
		KT_VERSION FolioGrande, 
		CO_NUMERO Concepto, 
		KC_VERSION FolioGrande,
		KT_FECHA Fecha, 
		CONSTRAINT PK_CONCEPTOVT PRIMARY KEY CLUSTERED 
		(
		KT_VERSION ASC,
		CO_NUMERO ASC, 
		KC_VERSION ASC
		)
	)
END
GO

IF exists (select * from sysobjects where id = object_id(N'KardexConceptos_GetVersionActual') and xtype in (N'FN', N'IF', N'TF')) 
	drop function KardexConceptos_GetVersionActual
GO

create function KardexConceptos_GetVersionActual() 
returns FolioGrande 
as
begin 
declare @verTotal FolioGrande 
	
	select @verTotal = max(KT_VERSION) from CONCEPTOVT 	
	set @verTotal = coalesce( @VerTotal, 0 ) 	
	
	return @verTotal 
	
end
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'KardexConceptos_TomarFoto') AND type IN ( N'P', N'PC' ))
	drop procedure KardexConceptos_TomarFoto
GO

/* FK a Concepto de CONCEPTOVR */ 
CREATE procedure KardexConceptos_TomarFoto
as
begin 
	set nocount on 
	declare @verTotal FolioGrande 
	
	set @verTotal = dbo.KardexConceptos_GetVersionActual() 

	set @verTotal = @verTotal + 1 
	
	declare @hoy Fecha 
	set @hoy = GETDATE() 
			
	insert into CONCEPTOVT (KT_VERSION, CO_NUMERO, KC_VERSION, KT_FECHA ) 
	select @verTotal, CO_NUMERO, KC_VERSION, @hoy from CONCEPTOVR

end
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'KardexConceptos_NotifyUpdate') AND type IN ( N'P', N'PC' ))
	drop procedure KardexConceptos_NotifyUpdate
GO

CREATE procedure KardexConceptos_NotifyUpdate( @CO_NUMERO Concepto, @US_CODIGO Usuario, @US_FEC_MOD Fecha , @lTomarFoto Booleano = 'S' ) 
as
begin 
	set nocount on 
	
	declare @KC_VERSION FolioGrande 

	if (@CO_NUMERO >= 1000 ) 
		return 
	

	select @KC_VERSION = KC_VERSION from CONCEPTOVR where CO_NUMERO = @CO_NUMERO 
	set @KC_VERSION = coalesce( @KC_VERSION, 0 ) 
	
	set @KC_VERSION = @KC_VERSION + 1 
		
	insert into KAR_CONCEP ( CO_NUMERO, KC_VERSION,  US_CODIGO, US_FEC_MOD
	  ,CO_A_PTU      ,CO_ACTIVO      ,CO_CALCULA      ,CO_DESCRIP      ,CO_FORMULA
      ,CO_G_IMSS      ,CO_G_ISPT      ,CO_IMP_CAL      ,CO_IMPRIME      ,CO_LISTADO
      ,CO_MENSUAL      ,CO_QUERY      ,CO_RECIBO      ,CO_TIPO      ,CO_X_ISPT
      ,CO_SUB_CTA      ,CO_CAMBIA      ,CO_D_EXT      ,CO_D_NOM      ,CO_LIM_SUP
      ,CO_LIM_INF      ,CO_VER_INF      ,CO_VER_SUP      ,CO_VER_ACC      ,CO_GPO_ACC
      ,CO_ISN      ,CO_SUMRECI      ,CO_FRM_ALT      ,CO_USO_NOM      ,CO_SAT_CLP
      ,CO_SAT_TPP      ,CO_SAT_CLN      ,CO_SAT_TPN      ,CO_SAT_EXE      ,CO_SAT_CON
      ,CO_PS_TIPO)
	select 
	   CO_NUMERO, @KC_VERSION,  @US_CODIGO, @US_FEC_MOD
	  ,CO_A_PTU      ,CO_ACTIVO      ,CO_CALCULA      ,CO_DESCRIP      ,CO_FORMULA
      ,CO_G_IMSS      ,CO_G_ISPT      ,CO_IMP_CAL      ,CO_IMPRIME      ,CO_LISTADO
      ,CO_MENSUAL      ,CO_QUERY      ,CO_RECIBO      ,CO_TIPO      ,CO_X_ISPT
      ,CO_SUB_CTA      ,CO_CAMBIA      ,CO_D_EXT      ,CO_D_NOM      ,CO_LIM_SUP
      ,CO_LIM_INF      ,CO_VER_INF      ,CO_VER_SUP      ,CO_VER_ACC      ,CO_GPO_ACC
      ,CO_ISN      ,CO_SUMRECI      ,CO_FRM_ALT      ,CO_USO_NOM      ,CO_SAT_CLP
      ,CO_SAT_TPP      ,CO_SAT_CLN      ,CO_SAT_TPN      ,CO_SAT_EXE      ,CO_SAT_CON
      ,CO_PS_TIPO
	from CONCEPTO where CO_NUMERO = @CO_NUMERO 

	update CONCEPTOVR set KC_VERSION = @KC_VERSION, US_CODIGO = @US_CODIGO, US_FEC_MOD = @US_FEC_MOD where CO_NUMERO = @CO_NUMERO 

	if @@ROWCOUNT = 0 
		insert into CONCEPTOVR (CO_NUMERO, KC_VERSION , US_CODIGO, US_FEC_MOD) values ( @CO_NUMERO, @KC_VERSION, @US_CODIGO, @US_FEC_MOD)  



	if @lTomarFoto = 'S' 
		exec KardexConceptos_TomarFoto
end;

go

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_INICIAR_KARDEX_CONCEPTOS') AND type IN ( N'P', N'PC' ))
	drop procedure SP_INICIAR_KARDEX_CONCEPTOS
GO

CREATE  PROCEDURE SP_INICIAR_KARDEX_CONCEPTOS
AS
BEGIN 
		select row_number() over ( order by CO_NUMERO ) i, CO_NUMERO into #ListaConceptos from CONCEPTO where CO_NUMERO < 1000 
		declare @i int 
		declare @n  int 
		declare @CO_NUMERO Concepto 
		declare @Fecha Fecha 

		set @i = 1 

		select @n= COUNT(*) from #ListaConceptos

		set @Fecha = GETDATE() 

		while @i <=  @n 
		begin 
			select @CO_NUMERO = CO_NUMERO from #ListaConceptos where i = @i 
			if (@CO_NUMERO > 0 ) 
				exec KardexConceptos_NotifyUpdate @CO_NUMERO, 1, @Fecha, 'N' 	

			set @i = @i  + 1 
		end 

		exec KardexConceptos_TomarFoto; 
END
GO


exec SP_INICIAR_KARDEX_CONCEPTOS; 

GO 

if not exists( select NAME from SYSCOLUMNS where NAME = 'NO_VER_CON' and ID = object_id(N'NOMINA'))
BEGIN
	alter table NOMINA add NO_VER_CON FolioGrande NULL 
END
GO

IF ( EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE  TABLE_NAME = 'NOMINA')) 
BEGIN
	update NOMINA set NO_VER_CON = 1 where NO_VER_CON is NULL
END
GO

if exists( select NAME from SYSCOLUMNS where NAME = 'NO_VER_CON' and ID = object_id(N'NOMINA'))
BEGIN
	alter table NOMINA alter column NO_VER_CON FolioGrande Not NULL 
END
GO

if not exists( select NAME from SYSCOLUMNS where NAME = 'NO_VER_TIM' and ID = object_id(N'NOMINA'))
BEGIN
	alter table NOMINA add NO_VER_TIM FolioGrande NULL
END
GO

if exists( select NAME from SYSCOLUMNS where NAME = 'NO_VER_TIM' and ID = object_id(N'NOMINA'))
BEGIN
	update NOMINA set NO_VER_TIM = 1 where NO_VER_TIM is NULL
END
GO

IF EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'V_CONCEPVT' AND XTYPE = 'V' )
	DROP VIEW V_CONCEPVT
GO

create view V_CONCEPVT
as
select KT.KT_VERSION, KT.KT_FECHA, KT.CO_NUMERO, KT.KC_VERSION, KC.US_CODIGO, KC.US_FEC_MOD
	  ,CO_A_PTU      ,CO_ACTIVO      ,CO_CALCULA      ,CO_DESCRIP      ,CO_FORMULA
      ,CO_G_IMSS      ,CO_G_ISPT      ,CO_IMP_CAL      ,CO_IMPRIME      ,CO_LISTADO
      ,CO_MENSUAL      ,CO_QUERY      ,CO_RECIBO      ,CO_TIPO      ,CO_X_ISPT
      ,CO_SUB_CTA      ,CO_CAMBIA      ,CO_D_EXT      ,CO_D_NOM      ,CO_LIM_SUP
      ,CO_LIM_INF      ,CO_VER_INF      ,CO_VER_SUP      ,CO_VER_ACC      ,CO_GPO_ACC
      ,CO_ISN      ,CO_SUMRECI      ,CO_FRM_ALT      ,CO_USO_NOM      ,CO_SAT_CLP
      ,CO_SAT_TPP      ,CO_SAT_CLN      ,CO_SAT_TPN      ,CO_SAT_EXE      ,CO_SAT_CON
      ,CO_PS_TIPO
 from CONCEPTOVT KT
left outer join KAR_CONCEP  KC on KC.CO_NUMERO = KT.CO_NUMERO and KC.KC_VERSION = KT.KC_VERSION

go

IF EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'V_CONTNOM' AND XTYPE = 'V' )
	DROP VIEW V_CONTNOM
GO

create view V_CONTNOM
as
	select NO.PE_YEAR, NO.PE_TIPO, NO.PE_NUMERO, NO.CB_CODIGO,  KT.CO_NUMERO, KC.KC_VERSION
		  ,CO_A_PTU      ,CO_ACTIVO      ,CO_CALCULA      ,CO_DESCRIP      ,CO_FORMULA
		  ,CO_G_IMSS      ,CO_G_ISPT      ,CO_IMP_CAL      ,CO_IMPRIME      ,CO_LISTADO
		  ,CO_MENSUAL      ,CO_QUERY      ,CO_RECIBO      ,CO_TIPO      ,CO_X_ISPT
		  ,CO_SUB_CTA      ,CO_CAMBIA      ,CO_D_EXT      ,CO_D_NOM      ,CO_LIM_SUP
		  ,CO_LIM_INF      ,CO_VER_INF      ,CO_VER_SUP      ,CO_VER_ACC      ,CO_GPO_ACC
		  ,CO_ISN      ,CO_SUMRECI      ,CO_FRM_ALT      ,CO_USO_NOM      ,CO_SAT_CLP
		  ,CO_SAT_TPP      ,CO_SAT_CLN      ,CO_SAT_TPN      ,CO_SAT_EXE      ,CO_SAT_CON
		  ,CO_PS_TIPO, NO_VER_TIM
	 from NOMINA NO
	left outer join CONCEPTOVT  KT on KT.KT_VERSION = NO.NO_VER_TIM 
	left outer join KAR_CONCEP  KC on KC.CO_NUMERO = KT.CO_NUMERO and KC.KC_VERSION = KT.KC_VERSION
go

IF EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'VMONTOSTIM' AND XTYPE = 'V' )
	DROP VIEW VMONTOSTIM
GO

create view VMONTOSTIM
as
	select MO.PE_YEAR, MO.PE_TIPO, MO.PE_NUMERO, MO.CB_CODIGO, MO.MO_ACTIVO, MO.CO_NUMERO, MO.MO_IMP_CAL, MO.US_CODIGO, MO.MO_PER_CAL, MO.MO_REFEREN, MO.MO_X_ISPT, MO.MO_PERCEPC, 
		   MO.MO_DEDUCCI, CN.CO_SAT_CLP, CN.CO_SAT_TPP, CN.CO_SAT_CLN, CN.CO_SAT_TPN, CN.CO_SAT_EXE, CN.CO_SAT_CON, CN.NO_VER_TIM,	   
		   ( CASE WHEN( MO.MO_PERCEPC + MO.MO_DEDUCCI < 0 ) THEN CN.CO_SAT_CLN ELSE CN.CO_SAT_CLP END ) AS CLASE,
		   ( select TB_SAT_NUM from TIPO_SAT where TB_CODIGO = CN.CO_SAT_TPN ) AS TPN,
		   ( select TB_SAT_NUM from TIPO_SAT where TB_CODIGO = CN.CO_SAT_TPP ) AS TPP,	   
		   CASE WHEN( MO.MO_PERCEPC + MO.MO_DEDUCCI < 0 ) THEN (( select TB_SAT_NUM from TIPO_SAT where TB_CODIGO = CN.CO_SAT_TPN )) ELSE ( select TB_SAT_NUM from TIPO_SAT where TB_CODIGO = CN.CO_SAT_TPP ) END AS TIPO_SAT,
		   CASE WHEN( MO.MO_PERCEPC + MO.MO_DEDUCCI < 0 ) THEN ( CN.CO_SAT_TPN ) ELSE ( CN.CO_SAT_TPP ) END AS VT_TIP_SAT,
		   ( 	   
			CASE
				WHEN ( ( CASE WHEN( MO.MO_PERCEPC + MO.MO_DEDUCCI < 0 ) THEN CN.CO_SAT_CLN ELSE CN.CO_SAT_CLP END )  = 4 )
				THEN ( CASE WHEN ( MO.MO_PERCEPC+MO.MO_DEDUCCI<0) THEN (-(MO.MO_PERCEPC+MO_DEDUCCI)) ELSE (MO.MO_PERCEPC+MO.MO_DEDUCCI) END  )
				ELSE ( CASE WHEN ( MO.MO_PERCEPC+MO.MO_DEDUCCI<0 AND CN.CO_SAT_CLP<>CN.CO_SAT_CLN ) THEN (-(MO_PERCEPC+MO_DEDUCCI)) ELSE (MO_PERCEPC+MO_DEDUCCI) END ) END
		   ) AS MONTO,
		   (
			CASE
				WHEN ( ( CASE WHEN( MO.MO_PERCEPC + MO.MO_DEDUCCI < 0 ) THEN CN.CO_SAT_CLN ELSE CN.CO_SAT_CLP END ) = 1 )
				THEN (MO.MO_X_ISPT)
				ELSE (0) END
			) AS EXENTO
		   from MOVIMIEN MO
	join V_CONTNOM CN on CN.PE_YEAR = MO.PE_YEAR and CN.PE_TIPO = MO.PE_TIPO and CN.PE_NUMERO = MO.PE_NUMERO and CN.CB_CODIGO = MO.CB_CODIGO and CN.CO_NUMERO = MO.CO_NUMERO
	join NOMINA NO on  NO.PE_YEAR = MO.PE_YEAR and NO.PE_TIPO = MO.PE_TIPO and NO.PE_NUMERO = MO.PE_NUMERO and NO.CB_CODIGO = MO.CB_CODIGO  
	where NO.NO_STATUS > 4 and NO.NO_TIMBRO = 2 

go

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_TIMBRAR_EMPLEADO') AND type IN ( N'P', N'PC' ))
	drop procedure SP_TIMBRAR_EMPLEADO
GO

CREATE procedure SP_TIMBRAR_EMPLEADO( @PE_YEAR Anio, @PE_TIPO NominaTipo, @PE_NUMERO NominaNumero, @CB_CODIGO NumeroEmpleado, @CB_RFC Descripcion, @Status status, @Timbre Formula, @FolioTimbramex FolioGrande, @Usuario usuario, @FacturaUUID  DescLarga = '' )
as 
begin 
set nocount on 
declare @StatusTimbrado status 
set @StatusTimbrado = 2 
if ( @Status = @StatusTimbrado )
begin 
	declare @verTotal FolioGrande 	
	set @verTotal = dbo.KardexConceptos_GetVersionActual() 

	update NOMINA set NO_TIMBRO = @Status, NO_SELLO = @Timbre , NO_FACTURA = @FolioTimbramex  , NO_FACUUID = @FacturaUUID, NO_VER_TIM  = @verTotal 
	where  CB_CODIGO = @CB_CODIGO and PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO = @PE_NUMERO 
	and NO_STATUS = 6  
end
else
	update NOMINA set NO_TIMBRO = @Status, NO_SELLO = '',  NO_FACTURA = 0, NO_FACUUID = '', NO_CAN_UID = NO_FACUUID 
	where  CB_CODIGO = @CB_CODIGO and PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO = @PE_NUMERO  
	and NO_STATUS = 6 
end
GO

/* #17452: Importar c�dula Fonacot por empleado, un solo pr�stamo suma todos sus cr�ditos 1/7 */
if not exists( select NAME from SYSOBJECTS  where NAME='PCED_ENC' and XTYPE='U' )
begin
    CREATE TABLE PCED_ENC (
        PE_TIPO        Codigo1,
        PE_NO_CT    Empleados,
        RS_CODIGO    Codigo,
        PE_YEAR        Anio,
        PE_MES        Mes,
        PE_ARCHIVO    PathArchivo
    )
    
    ALTER TABLE PCED_ENC
    ADD CONSTRAINT PK_PCED_ENC PRIMARY KEY (PE_YEAR, PE_MES)    
end
GO

/*	#17725: No se puede generar c�dula de pago cuando las c�dulas fueron cargadas en alg�n <=Braavos1.
	Script para incializar tabla PCED_ENC	
*/
IF (select count(PE_YEAR) FROM PCED_ENC) = 0
	INSERT INTO PCED_ENC
		SELECT DISTINCT 0, 0, '', PF_YEAR, PF_MES, '' FROM PCED_FON
GO
 
/* #17452: Importar c�dula Fonacot por empleado, un solo pr�stamo suma todos sus cr�ditos 2/7 */
IF not EXISTS (select * from syscolumns where id=object_id('PCED_FON') and name='PF_CTD')
    ALTER TABLE PCED_FON  ADD PF_CTD  Empleados NULL 
go 

/* #17452: Importar c�dula Fonacot por empleado, un solo pr�stamo suma todos sus cr�ditos 3/7 */
update PCED_FON set PF_CTD =  0 where PF_CTD is null 
go 

/* #17452: Importar c�dula Fonacot por empleado, un solo pr�stamo suma todos sus cr�ditos 4/7 */
ALTER TABLE PCED_FON ALTER COLUMN PF_CTD Empleados NOT NULL 
go 

/* #17452: Importar c�dula Fonacot por empleado, un solo pr�stamo suma todos sus cr�ditos 5/7 */
IF not EXISTS (select * from syscolumns where id=object_id('PCED_FON') and name='PF_NO_SS')
    ALTER TABLE PCED_FON  ADD PF_NO_SS  Descripcion NULL 
go 

/* #17452: Importar c�dula Fonacot por empleado, un solo pr�stamo suma todos sus cr�ditos 6/7 */
update PCED_FON set PF_NO_SS =  '' where PF_NO_SS is null 
go 

/* #17452: Importar c�dula Fonacot por empleado, un solo pr�stamo suma todos sus cr�ditos 7/7 */
ALTER TABLE PCED_FON ALTER COLUMN PF_NO_SS Descripcion NOT NULL 
go 

IF exists (select * from sysobjects where id = object_id(N'FONACOT_GET_FECHA_INICIO') and xtype in (N'FN', N'IF', N'TF')) 
	drop function FONACOT_GET_FECHA_INICIO
GO

CREATE FUNCTION FONACOT_GET_FECHA_INICIO( @FT_YEAR INT, @FT_MONTH INT, @CB_NOMINA INT) RETURNS FECHA 
AS
BEGIN 
	DECLARE @FECHA FECHA

	SELECT @FECHA = PE_FEC_INI FROM PERIODO P INNER JOIN	FN_FONACOT_PERIODOS (@FT_YEAR, @FT_MONTH, @CB_NOMINA) FP
			ON P.PE_NUMERO = FP.PE_NUMERO
			where
				PE_YEAR = @FT_YEAR and PE_MES_FON = @FT_MONTH 	AND PE_TIPO =@CB_NOMINA		AND PE_POS_FON = 1

	RETURN COALESCE( @FECHA, '2018-01-01' ) 
END 
GO

IF EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'VCED_CALCULA' AND XTYPE = 'V' )
	DROP VIEW VCED_CALCULA
GO

CREATE view VCED_CALCULA
as
	select
	PF_YEAR, PF_MES, CB_CODIGO, PR_REFEREN,  PR_TIPO, PF_PAGO, PF_PLAZO, PF_PAGADAS, FC_NOMINA, FC_AJUSTE,
	PF_PAGO - (FC_NOMINA + FC_AJUSTE )  as DIFERENCIA,
	FE_INCIDE,
	PR_STATUS,
	PR_FON_MOT,
	FECHA_INI_BAJA as FE_FECHA1,
	FECHA_FIN as FE_FECHA2,
	PF_REUBICA,
	CT_DIF_QTY,
	FE_CUANTOS
	from (

		SELECT F.PF_YEAR, F.PF_MES, C.CB_CODIGO, F.PR_REFEREN,  F.PR_TIPO, F.PF_PAGO, F.PF_PLAZO, F.PF_PAGADAS,
			coalesce(FC.FC_NOMINA,0.00) FC_NOMINA,  coalesce(FC.FC_AJUSTE,0.00) FC_AJUSTE ,
						coalesce(case
								when FE.FE_INCIDE = 'B'  then 'B'
								when FE.FE_INCIDE is null  and C.CB_ACTIVO = 'N' and C.CB_FEC_BAJ < dbo.FONACOT_GET_FECHA_INICIO(F.PF_YEAR, F.PF_MES, C.CB_NOMINA )	then 'B'
								when ((FE.FE_INCIDE is null or FE.FE_INCIDE = '' or FE.FE_INCIDE = '0') AND P.PR_STATUS = 0) then '0'
								else FE.FE_INCIDE
						end, '')   FE_INCIDE ,
						coalesce(case
							when FE.FE_INCIDE = 'B'  then   FE.FE_FECHA1
								when FE.FE_INCIDE is null  and C.CB_ACTIVO = 'N' and C.CB_FEC_BAJ < dbo.FONACOT_GET_FECHA_INICIO(F.PF_YEAR, F.PF_MES, C.CB_NOMINA )	then CB_FEC_BAJ
							else FE.FE_FECHA1
						end , '' ) AS FECHA_INI_BAJA,
						coalesce( case
							when FE.FE_INCIDE = 'B'  then  '1899-12-30'
							else FE.FE_FECHA2
						end, ''  ) AS FECHA_FIN,
							F.PF_REUBICA,
						coalesce(PR_STATUS,0) PR_STATUS,
						coalesce(PR_FON_MOT,0) PR_FON_MOT,
						coalesce(CT.CT_DIF_QTY, 0) CT_DIF_QTY,
						coalesce(FE.FE_CUANTOS, 0 ) FE_CUANTOS
			FROM PCED_FON F
				LEFT JOIN COLABORA C ON F.CB_CODIGO = C.CB_CODIGO
				LEFT JOIN FON_EMP FE ON FE.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = FE.FT_YEAR AND F.PF_MES = FE.FT_MONTH
				LEFT JOIN FON_CRE FC ON FC.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = FC.FT_YEAR AND F.PF_MES = FC.FT_MONTH AND F.PR_REFEREN = FC.PR_REFEREN AND F.PR_TIPO = FC.PR_TIPO
				LEFT JOIN VCEDTFON CT  ON CT.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = CT.PF_YEAR AND F.PF_MES = CT.PF_MES
				LEFT JOIN PRESTAMO P ON P.PR_REFEREN = F.PR_REFEREN AND P.PR_TIPO = F.PR_TIPO AND P.CB_CODIGO = F.CB_CODIGO
		) Cedula
GO

IF exists( select * from sys.objects where object_id = object_id('VCED_CONSULTA') )
    drop view VCED_CONSULTA
GO

CREATE view VCED_CONSULTA
as
	select 
	PF_YEAR, PF_MES, CB_CODIGO, PR_REFEREN,  PR_TIPO, PF_PAGO, PF_PLAZO, PF_PAGADAS, FC_NOMINA, FC_AJUSTE, 
	PF_PAGO - (FC_NOMINA + FC_AJUSTE )  as DIFERENCIA, 
	FE_INCIDE, 
	PR_STATUS,
	PR_FON_MOT,
	FECHA_INI_BAJA as FE_FECHA1, 
	FECHA_FIN as FE_FECHA2, 
	PF_REUBICA, 
	FE_DIF_QTY, 
	FE_CUANTOS,
	PF_CTD
	from ( 

	SELECT F.PF_YEAR, F.PF_MES, C.CB_CODIGO, F.PR_REFEREN,  F.PR_TIPO, F.PF_PAGO, F.PF_PLAZO, F.PF_PAGADAS,
		coalesce(FC.FC_NOMINA,0.00) FC_NOMINA,  coalesce(FC.FC_AJUSTE,0.00) FC_AJUSTE ,				
					case
					 when (FE.FE_INCIDE is null or FE.FE_INCIDE = '0' ) and  C.CB_ACTIVO = 'N' and C.CB_FEC_BAJ < dbo.FONACOT_GET_FECHA_INICIO(F.PF_YEAR, F.PF_MES, C.CB_NOMINA )	then 'B' 	
					 else coalesce(FE_INCIDE, '0') 
					end 					 
					AS FE_INCIDE,
					case
					 when (FE.FE_INCIDE is null or FE.FE_INCIDE = '0' ) and  C.CB_ACTIVO = 'N' and C.CB_FEC_BAJ < dbo.FONACOT_GET_FECHA_INICIO(F.PF_YEAR, F.PF_MES, C.CB_NOMINA )	then CB_FEC_BAJ	
					 else coalesce(FE.FE_FECHA1, '1899-12-30') 
					end 					 
					AS FECHA_INI_BAJA, 
					coalesce(FE.FE_FECHA2, '1899-12-30') AS FECHA_FIN, 
					F.PF_REUBICA, 
					FC.FC_PR_STA AS PR_STATUS, 
					FC.FC_FON_MOT AS PR_FON_MOT, 
					FE.FE_DIF_QTY, 
					FE.FE_CUANTOS,
					PF_CTD				
		FROM PCED_FON F
			LEFT JOIN COLABORA C ON F.CB_CODIGO = C.CB_CODIGO
			LEFT JOIN FON_EMP FE ON FE.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = FE.FT_YEAR AND F.PF_MES = FE.FT_MONTH
			LEFT JOIN FON_CRE FC ON FC.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = FC.FT_YEAR AND F.PF_MES = FC.FT_MONTH AND F.PR_REFEREN = FC.PR_REFEREN  AND F.PR_TIPO = FC.PR_TIPO 
			LEFT JOIN VCEDTFON CT  ON CT.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = CT.PF_YEAR AND F.PF_MES = CT.PF_MES				
	
		) Cedula

GO

IF exists( select * from sys.objects where object_id = object_id('VCED_DET') )
    drop view VCED_DET
GO

CREATE view VCED_DET
as
select
PF_YEAR, PF_MES, RS_CODIGO, CB_FONACOT, CB_RFC, CB_SEGSOC, CB_CODIGO, PRETTYNAME, PR_REFEREN, PR_TIPO,  PF_PAGO, PF_PLAZO, PF_PAGADAS, FC_NOMINA, FC_AJUSTE,
PF_PAGO - (FC_NOMINA + FC_AJUSTE )  as DIFERENCIA,
FE_INCIDE,
case FE_INCIDE
	when '0' then ''
	when 'B' then 'Baja IMSS'
	when 'I' then 'Incapacidad'
	when 'C' then 'Pr�stamo cancelado'
	when 'M' then 'Pago de m�s'
	when 'L' then 'Pago de menos'
	else  'Otro'
end FE_IN_DESC,
case FE_INCIDE
	when '0' then ''
	when 'C' then PR_FON_MOT
	else  ''
end PR_FON_MOT,
FECHA_INI_BAJA as FE_FECHA1, FECHA_FIN as FE_FECHA2, PF_REUBICA, FE_DIF_QTY, FE_CUANTOS, PF_CTD
from ( 
SELECT VC.PF_YEAR, VC.PF_MES,  coalesce(RP.RS_CODIGO,'') RS_CODIGO,  C.CB_FONACOT, C.CB_RFC, C.CB_SEGSOC, coalesce(UPPER (C.CB_APE_PAT + ' ' + C.CB_APE_MAT +' ' + C.CB_NOMBRES), '')  AS PRETTYNAME, VC.PR_REFEREN, VC.PR_TIPO,  VC.PF_PAGO, coalesce(C.CB_CODIGO,0) CB_CODIGO, VC.PF_PLAZO, VC.PF_PAGADAS,
	coalesce(VC.FC_NOMINA,0.00) FC_NOMINA,  coalesce(VC.FC_AJUSTE,0.00) FC_AJUSTE ,
				
				case
					when coalesce( FE_INCIDE, '0')  = '0'  and PR_STATUS = 1  then 'C'
				    when coalesce( FE_INCIDE, '0')  = '0' AND ((FC_NOMINA+FC_AJUSTE)>PF_PAGO) then 'M'
				    when coalesce( FE_INCIDE, '0')  = '0' AND ((FC_NOMINA+FC_AJUSTE)<PF_PAGO) then 'L'
					else coalesce( FE_INCIDE, '0' )
				end AS FE_INCIDE,
				case
					when CONVERT (VARCHAR(10), VC.FE_FECHA1, 103) = '30/12/1899'  then ''
					else CONVERT (VARCHAR(10), VC.FE_FECHA1, 103)
				end AS FECHA_INI_BAJA,
				case
					when CONVERT (VARCHAR(10), VC.FE_FECHA2, 103) = '30/12/1899' then ''
					else CONVERT (VARCHAR(10), VC.FE_FECHA2, 103)
				end AS FECHA_FIN, 
			    VC.PF_REUBICA,				 
				case PR_FON_MOT 
					when 0 then 'No aplica' 
					when 1 then 'Pago directo en Fonacot' 
					when 2 then 'Confirmaci�n de Fonacot' 
					when 3 then 'Ausente en C�dula'
					when 4 then 'CanceladoOtro'
					else  '' 
				end PR_FON_MOT, VC.FE_DIF_QTY, VC.FE_CUANTOS, PF_CTD
	FROM VCED_CONSULTA VC
		LEFT JOIN COLABORA C ON VC.CB_CODIGO = C.CB_CODIGO
		LEFT JOIN RPATRON RP on CB_PATRON = RP.TB_CODIGO
	) Cedula 

GO 

if exists (select * from dbo.sysobjects where id = object_id(N'FONACOT_CEDULA_TRABAJADOR') and xtype in (N'FN', N'IF', N'TF')) 
	drop function FONACOT_CEDULA_TRABAJADOR
GO 

create function FONACOT_CEDULA_TRABAJADOR
(	@YEAR SMALLINT,
    @MES SMALLINT,
	@RAZON_SOCIAL CHAR(6), 
	@IgnorarIncapacidades Booleano)
RETURNS @CEDULA TABLE
(
	NO_CT int,
	ANIO_EMISION smallint,
	MES_EMISION smallint,
    NO_FONACOT varchar(30),
    RFC varchar(30),
    NO_SS varchar(11),
	NOMBRE VARCHAR(250),
	CLAVE_EMPLEADO int,
	CTD_CREDITOS int,
	RETENCION_MENSUAL numeric(15,2),
	TIPO char(1),
	RETENCION_REAL numeric (15,2),
	FECHA_BAJA varchar(10),
	FECHA_INI varchar(10),
	FECHA_FIN varchar(10)
	)
AS
BEGIN
	insert into @CEDULA
	( NO_CT, ANIO_EMISION, MES_EMISION, NO_FONACOT, RFC, NO_SS, NOMBRE, CLAVE_EMPLEADO, CTD_CREDITOS, RETENCION_MENSUAL, TIPO, RETENCION_REAL,
		FECHA_BAJA, FECHA_INI, FECHA_FIN)	

	select PE.PE_NO_CT, @YEAR ANIO_EMISION, @MES MES_EMISION, CB_FONACOT, CB_RFC, CB_SEGSOC, PRETTYNAME, CB_CODIGO, PF_CTD, PF_PAGO,			
			case
				when FE_INCIDE = 'B' then FE_INCIDE
			    when @IgnorarIncapacidades = 'S' and FE_INCIDE = 'I' and FE_DIF_QTY < FE_CUANTOS  then  '0'				
				when FE_INCIDE not in ('B', '0', 'I', 'L', 'M' ) then '0' 				
				else FE_INCIDE 				
			end as 
			FE_INCIDE,
			FC_NOMINA+FC_AJUSTE AS RETENCION_REAL,
			case 
				when FE_INCIDE = 'B' then FE_FECHA1 
				when @IgnorarIncapacidades = 'S' and FE_INCIDE = 'I' and FE_DIF_QTY < FE_CUANTOS  then  '' 		
				else '' 
			end as 
			FECHA_BAJA,
			case 
				when FE_INCIDE = 'B' then '' 
				when @IgnorarIncapacidades = 'S' and FE_INCIDE = 'I' and FE_DIF_QTY < FE_CUANTOS  then  '' 			
				else FE_FECHA1 
			end  as 
			FECHA_INI,
			case 
				when FE_INCIDE = 'B' then '' 
				when @IgnorarIncapacidades = 'S' and FE_INCIDE = 'I' and FE_DIF_QTY < FE_CUANTOS  then  '' 			
				else FE_FECHA2
			end  as 
			FECHA_FIN
	from VCED_DET VC, PCED_ENC PE
	WHERE 	PF_YEAR = @YEAR AND PF_MES = @MES	AND ( VC.RS_CODIGO = @RAZON_SOCIAL or @RAZON_SOCIAL = '')
	AND VC.PF_YEAR = PE.PE_YEAR AND VC.PF_MES = PE.PE_MES
		
	return
END
GO 

if exists (select * from dbo.sysobjects where id = object_id(N'PrestamoFonacot_PuedeActivar') and xtype in (N'FN', N'IF', N'TF')) 
	drop function PrestamoFonacot_PuedeActivar
GO

CREATE function PrestamoFonacot_PuedeActivar(@PF_YEAR int, @PF_MES Status, @CB_CODIGO NumeroEmpleado, @PR_TIPO Codigo1, @PR_REFEREN Referencia ) 
returns Booleano 
as
begin 
    declare @FechaIni Fecha
    declare @CB_FEC_BAJ Fecha 
    declare @CB_FEC_ING Fecha 
    declare @CB_NOMINA NominaTipo 

    select @CB_FEC_BAJ = CB_FEC_BAJ, @CB_NOMINA = CB_NOMINA, @CB_FEC_ING = CB_FEC_ING from COLABORA where CB_CODIGO = @CB_CODIGO 
    select @FechaIni = MIN(PE_FEC_INI) from PERIODO where PE_MES_FON = @PF_MES and PE_YEAR = @PF_YEAR and PE_TIPO = @CB_NOMINA 

	if ( @CB_FEC_ING > @CB_FEC_BAJ  or @CB_FEC_BAJ >= @FechaIni )  
    begin 
        return 'S' 
    end 

    return 'N' 
end
GO

if exists( select NAME from SYSCOLUMNS where NAME = 'at_campo' and ID = object_id(N'R_ATRIBUTO'))
BEGIN
	DELETE R_ATRIBUTO where at_campo = 'STATUS_PR' and en_codigo = ( select EN_CODIGO from R_ENTIDAD where  en_tabla = 'PRESTAMO' )
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'Fon_Diferencias_Totales') and xtype in (N'FN', N'IF', N'TF'))
	DROP FUNCTION Fon_Diferencias_Totales
go

CREATE FUNCTION Fon_Diferencias_Totales( @Year int, @Mes int, @RSCodigo Codigo)
RETURNS TABLE
AS RETURN
		select
				Coalesce ([Baja IMSS], 0) as BajaIMSS, Coalesce (Incapacidad, 0) as Incapacidad, Coalesce ([Pr�stamo cancelado], 0) as PrestamoCancelado,
				Coalesce ([Pago de m�s], 0) as PagoMas, Coalesce ([Pago de menos], 0) as PagoMenos, 
				Coalesce (Otro, 0) as Otro, coalesce (Total, 0) Total,
				Coalesce ([Ausente en C�dula], 0) as Ausente, Coalesce ([Confirmaci�n de Fonacot], 0) as Confirmacion,
				Coalesce (CanceladoOtro, 0) as CanceladoOtro, Coalesce ([Pago directo en Fonacot], 0) as PagoDirecto,  coalesce (SubTotal, 0) SubTotal
			from
			(
				  SELECT FE_IN_DESC as columnname, SUM (DIFERENCIA) AS value
					  FROM VCED_DET WHERE PF_YEAR = @Year AND PF_MES = @Mes AND RS_CODIGO = @RSCodigo
					  AND FE_IN_DESC <> '' AND FE_IN_DESC <> 'Otro'
					  GROUP BY FE_IN_DESC
				UNION
					  SELECT 'Otro' as columnname, SUM (DIFERENCIA) AS value
						  FROM VCED_DET WHERE PF_YEAR = @Year AND PF_MES = @Mes AND RS_CODIGO = @RSCodigo		  
						  AND (FE_INCIDE = '0' OR FE_INCIDE = '')
				UNION
					  SELECT 'Total' as columnname, SUM (DIFERENCIA) AS value
						  FROM VCED_DET WHERE PF_YEAR = @Year AND PF_MES = @Mes AND RS_CODIGO = @RSCodigo
				UNION
					 SELECT 
						PR_FON_MOT as columnname, SUM (DIFERENCIA) AS value
						FROM VCED_DET WHERE PF_YEAR = @Year AND PF_MES = @Mes AND RS_CODIGO = @RSCodigo
						AND FE_IN_DESC <> '' AND FE_IN_DESC <> 'Otro'
						AND FE_INCIDE = 'C'
						GROUP BY PR_FON_MOT
				UNION
					  SELECT 'SubTotal' as columnname, SUM (DIFERENCIA) AS value
						  FROM VCED_DET WHERE PF_YEAR = @Year AND PF_MES = @Mes AND RS_CODIGO = @RSCodigo
						AND FE_IN_DESC <> '' AND FE_IN_DESC <> 'Otro'
						AND FE_INCIDE = 'C'
			  ) d
			pivot
			(
			  max(value)
			  for columnname in ([Baja IMSS], Incapacidad, [Pr�stamo cancelado], [Pago de m�s], [Pago de menos], Otro, Total,
				[Ausente en C�dula], [Confirmaci�n de Fonacot],  CanceladoOtro, [Pago directo en Fonacot], SubTotal)
			) piv
GO 

if exists (select * from dbo.sysobjects where id = object_id(N'VHGTI') and OBJECTPROPERTY(id, N'IsView') = 1)
	DROP VIEW VHGTI
GO

CREATE VIEW VHGTI
AS
SELECT
	BIO.ID_NUMERO,
	COALESCE (HU_ID, 0) AS HU_ID,
	COALESCE (H.CM_CODIGO, '') AS CM_CODIGO,
	H.CB_CODIGO,
	COALESCE (HU_INDICE, 0) AS HU_INDICE,
	COALESCE (H.IV_CODIGO, 0) AS IV_CODIGO,
	H.HU_ID AS HU_CNT,
	H.HU_HUELLA AS HUELLA,
	CO.CM_DIGITO + REPLICATE('0', 9 - LEN( H.CB_CODIGO ) ) + cast ( H.CB_CODIGO as Varchar) + 'A' as CHECADA
FROM #COMPARTE..HUELLAGTI H
left outer join #COMPARTE..EMP_BIO BIO on H.CB_CODIGO = BIO.CB_CODIGO and H.CM_CODIGO = BIO.CM_CODIGO
left outer join #COMPARTE..COMPANY CO on H.CM_CODIGO = CO.CM_CODIGO
WHERE ( BIO.CB_ACTIVO = 'S' ) 
or ( ( BIO.CB_ACTIVO = 'N' ) and ( BIO.CB_FEC_BAJ >= Getdate() ) )
and ( BIO.IV_CODIGO = 0 )
GO

if exists (select * from dbo.sysobjects where id = object_id(N'VPGTI') and OBJECTPROPERTY(id, N'IsView') = 1)
	DROP VIEW VPGTI
GO

CREATE VIEW VPGTI
AS
SELECT
	EI.ID_NUMERO,
	COALESCE (EI.CM_CODIGO, '') AS CM_CODIGO,
	EI.CB_CODIGO,
	CO.CM_DIGITO + REPLICATE('0', 9 - LEN( EI.CB_CODIGO ) ) + cast ( EI.CB_CODIGO as Varchar) + 'A' as CHECADA
FROM #COMPARTE..EMP_ID EI
left outer join #COMPARTE..COMPANY CO on EI.CM_CODIGO = CO.CM_CODIGO
GO
