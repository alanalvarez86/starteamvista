/*****************************/
/* Creaci�n de tablas nuevas */
/*****************************/

/* Table: SUP_AREA, Owner: SYSDBA */
/* Se usa para indicar �reas administradas por usuarios supervisores */
/* Patch 375 # Seq 1: agregado */
CREATE TABLE SUP_AREA ( US_CODIGO USUARIO, CB_AREA CODIGO )
GO

/* Patch 375 # Seq 2: agregado */
ALTER TABLE SUP_AREA ADD CONSTRAINT PK_SUP_AREA PRIMARY KEY
	(
		US_CODIGO,
		CB_AREA
	)
GO

/* Table: KAR_AREA, Owner: SYSDBA */
/* Se usa para guardar los cambios de �reas de empleados */
/* Patch 375 # Seq 3: agregado */
CREATE TABLE KAR_AREA ( CB_CODIGO NUMEROEMPLEADO,
        		KA_FECHA FECHA,
        		KA_HORA HORA,
        		CB_AREA CODIGO,
        		US_CODIGO USUARIO,
       	 		KA_FEC_MOV FECHA,
        		KA_HOR_MOV HORA )
GO

/* Patch 375 # Seq 4: agregado */
ALTER TABLE KAR_AREA ADD CONSTRAINT PK_KAR_AREA PRIMARY KEY
	(
		CB_CODIGO,
		KA_FECHA,
		KA_HORA
	)
GO

/* Temporal para indicar los empleados sujetos de Labor de un d�a */
/* Unicamente se usa en el c�lculo de tiempos */
/* No es necesario incluir en DICCION */
/* Patch 375 # Seq 5: agregado */
CREATE TABLE TMPLABOR ( US_CODIGO USUARIO,
       			CB_CODIGO NUMEROEMPLEADO,
       			CB_AREA CODIGO,
       			CB_CHECA BOOLEANO,
       			CB_TURNO CODIGO,
       			TL_STATUS STATUS,
       			TL_APLICA BOOLEANO,
       			TL_CALCULA BOOLEANO)
G0

/* Patch 375 # Seq 6: agregado */
ALTER TABLE TMPLABOR ADD CONSTRAINT PK_TMPLABOR PRIMARY KEY
	(
		US_CODIGO,
		CB_CODIGO
	)
GO

/* Table TMPBALA2, Owner: SYSDBA*/
/* Temporal para manejar el listado de n�mina */
/* Patch 375 # Seq 7: agregado */
CREATE TABLE TMPBALA2 (
  TP_USER USUARIO,
  TP_NIVEL1 CODIGO,
  TP_NIVEL2 CODIGO,
  TP_NIVEL3 CODIGO,
  TP_NIVEL4 CODIGO,
  TP_NIVEL5 CODIGO,
  TP_ORDEN FOLIOCHICO,
  CB_CODIGO NUMEROEMPLEADO,
  TP_NUM_PER CONCEPTO,
  TP_DES_PER DESCRIPCION,
  TP_MON_PER PESOS,
  TP_HOR_PER HORAS,
  TP_REF_PER REFERENCIA,
  TP_NUM_DED CONCEPTO,
  TP_DES_DED DESCRIPCION,
  TP_MON_DED PESOS,
  TP_REF_DED REFERENCIA,
  TP_HOR_DED HORAS,
  TP_TOTAL NUMEROEMPLEADO,
  PE_YEAR ANIO,
  PE_TIPO NOMINATIPO,
  PE_NUMERO NOMINANUMERO,
  TP_EMPLEAD OBSERVACIONES
)
GO

/* Patch 375 # Seq 8: agregado */
ALTER TABLE TMPBALA2
  ADD PRIMARY KEY (TP_USER, TP_NIVEL1, TP_NIVEL2, TP_NIVEL3, TP_NIVEL4, TP_NIVEL5, CB_CODIGO, TP_ORDEN)
GO

/***************************************/
/* Alteraciones a tablas ya existentes */
/***************************************/

/* Table CEDULA, Owner: SYSDBA */
/* Crea un �ndice �nico para evitar c�dulas con la misma fecha y hora */
/* Patch 375 # Seq 9: agregado */
CREATE UNIQUE INDEX XAK1CEDULA ON CEDULA
(
       CE_AREA,
       CE_FECHA,
       CE_HORA
)
GO

/* Table: COLABORA, Owner: SYSDBA */
/* Guarda en COLABORA las fechas del �ltimo cambio de �rea */
/* Mejora eficiencia de la base de datos pero disminuye su normalidad */
/* Patch 375 # Seq 10: agregado */
ALTER TABLE COLABORA ADD CB_AR_FEC FECHA
GO

/* Patch 375 # Seq 11: agregado */
ALTER TABLE COLABORA ADD CB_AR_HOR HORA
GO

/* Para dejar datos de �rea en COLABORA correctamente */
/* Patch 375 # Seq 12: agregado */
update COLABORA set CB_AR_FEC = CB_FEC_ING, CB_AR_HOR = '0000'
where ( CB_AR_FEC <= '12/30/1899' )
GO

/* Table: AREA, Owner: SYSDBA */
/* AREA.TB_OP_UNI indica que la operaci�n default del �rea es la �nica que es capturable en las c�dulas */
/* Patch 375 # Seq 13: agregado */
ALTER TABLE AREA ADD TB_OP_UNI BOOLEANO
GO

/* Table: AREA, Owner: SYSDBA */
/* AREA.AR_TIPO indica el tipo del �rea: 0 = Directo, 1 = Indirecto, 2 = Administrativo */
/* Patch 375 # Seq 14: agregado */
ALTER TABLE AREA ADD AR_TIPO STATUS
GO

/* Table: AREA, Owner: SYSDBA */
/* AREA.AR_SHIFT indica el turno del �rea: 0 = Otros, 1 = Matutino, 2 = Vestpertino, 3 = Nocturno */
/* Patch 375 # Seq 15: agregado */
ALTER TABLE AREA ADD AR_SHIFT STATUS
GO

/* Table: AREA, Owner: SYSDBA */
/* AREA.AR_PRI_HOR: indica la primera hora del �rea */
/* Patch 375 # Seq 16: agregado */
ALTER TABLE AREA ADD AR_PRI_HOR HORA
GO

/* Table: AREA, Owner: SYSDBA */
/* Inicializa Operaci�n Unica */
/* Patch 375 # Seq 17: agregado */
UPDATE AREA SET TB_OP_UNI = 'N' WHERE ( TB_OP_UNI is NULL ) OR ( TB_OP_UNI = '' );
GO

/* Table: AREA, Owner: SYSDBA */
/* Inicializa Primera Hora */
/* Patch 375 # Seq 18: agregado */
UPDATE AREA SET AR_PRI_HOR = '0700' WHERE ( AR_PRI_HOR is NULL ) OR ( AR_PRI_HOR = '' );
GO

/* Table: PROCESO, Owner: SYSDBA */
/* PROCESO.PC_PARAMS guarda los par�metros con los que fu� ejecutado el proceso. Defecto #278 */
/* Patch 375 # Seq 19: agregado */
ALTER TABLE PROCESO ADD PC_PARAMS FORMULA
GO

/* Table: POLIZA, Owner: SYSDBA */
/* POLIZA.TP_TEXTO: Para almacenar el comentario de una p�liza */
/* Patch 375 # Seq 20: agregado */
ALTER TABLE POLIZA ADD TP_TEXTO NOMBRECAMPO
GO

/* Table: POLIZA, Owner: SYSDBA */
/* POLIZA.TP_NUMERO: Para almacenar el comentario num�rico de una p�liza */
/* Patch 375 # Seq 21: agregado */
ALTER TABLE POLIZA ADD TP_NUMERO STATUS
GO

/* Table: CURSO, Owner: SYSDBA */
/* Mismo sentido que TB_NUMERO en otras tablas */
/* Patch 375 # Seq 22: agregado */
ALTER TABLE CURSO ADD CU_NUMERO PESOS
GO

/* Table: CURSO, Owner: SYSDBA */
/* Mismo sentido que TB_TEXTO en otras tablas */
/* Patch 375 # Seq 23: agregado */
ALTER TABLE CURSO ADD CU_TEXTO DESCLARGA
GO

/* Table: MAESTRO, Owner: SYSDBA */
/* Mismo sentido que TB_NUMERO en otras tablas */
/* Patch 375 # Seq 24: agregado */
ALTER TABLE MAESTRO ADD MA_NUMERO PESOS
GO

/* Table: MAESTRO, Owner: SYSDBA */
/* Mismo sentido que TB_TEXTO en otras tablas */
/* Patch 375 # Seq 25: agregado */
ALTER TABLE MAESTRO ADD MA_TEXTO DESCRIPCION
GO

/* Table: MAESTRO, Owner: SYSDBA */
/* Empresa donde trabaja el maestro */
/* Patch 375 # Seq 26: agregado */
ALTER TABLE MAESTRO ADD MA_EMPRESA DESCRIPCION
GO

/* Table: MAESTRO, Owner: SYSDBA */
/* Direcci�n del Maestro o Empresa */
/* Patch 375 # Seq 27: agregado */
ALTER TABLE MAESTRO ADD MA_DIRECCI DESCRIPCION
GO

/* Table: MAESTRO, Owner: SYSDBA */
/* Ciudad de la Direcci�n del Maestro o Empresa */
/* Patch 375 # Seq 28: agregado */
ALTER TABLE MAESTRO ADD MA_CIUDAD DESCRIPCION
GO

/* Table: MAESTRO, Owner: SYSDBA */
/* Tel�fono del Maestro o Empresa */
/* Patch 375 # Seq 29: agregado */
ALTER TABLE MAESTRO ADD MA_TEL DESCRIPCION
GO

/* Table: WORKS, Owner: SYSDBA */
/* Para almacenar la duraci�n de las checadas precalculadas */
/* Patch 375 # Seq 30: agregado */
alter table WORKS add WK_PRE_CAL TIEMPO

/* Table: WORKS, Owner: SYSDBA */
/* Para almacenar la hora de inicio de la checada */
/* Patch 375 # Seq : 31 agregado */
alter table WORKS add WK_HORA_I Hora;

/* Table: WORKS, Owner: SYSDBA */
/* Para almacenar el tiempo estandard de la checada */
/* Patch 375 # Seq : agregado */
alter table WORKS add WK_STD_HR Tiempo;

/* Corrige problema con inicializaci�n de registros de */
/* Cierre de vacaciones en el cliente: se requiere que */
/* estos registros no tengan fecha de fin */
/* Patch 375 # Seq 34: agregado */
update VACACION set VA_FEC_FIN = '12/30/1899' where ( VA_TIPO = 0 ) and ( VA_FEC_FIN > '12/30/1899' )

/*************************************/
/* Stored Procedures del nuevo Labor */
/*************************************/

/* Recalcula el �rea de un empleado */
/* Patch 375 # Seq 51: agregado */
CREATE  PROCEDURE DBO.RECALCULA_AREA @EMPLEADO INTEGER
AS
DECLARE @VKA_FECHA DATETIME,
	@VKA_HORA CHAR(4),
	@VCB_AREA CHAR(6),
	@VUS_CODIGO SMALLINT,
	@VKA_FEC_MOV DATETIME,
	@VKA_HOR_MOV CHAR(4),
	@VKA_ACTIVO CHAR(1)
begin
	SET NOCOUNT ON;
        DECLARE Cur_RecalculaArea CURSOR FOR
  		SELECT TOP 1
			   KA_FECHA,
			   KA_HORA,
			   CB_AREA,
			   US_CODIGO,
			   KA_FEC_MOV,
			   KA_HOR_MOV
      		FROM 	   KAR_AREA
      		WHERE 	   CB_CODIGO = @EMPLEADO
      		ORDER BY   KA_FECHA desc,
			   KA_HORA  desc

	OPEN Cur_RecalculaArea

	FETCH NEXT FROM Cur_RecalculaArea
      	INTO 	@vKA_FECHA,
		@vKA_HORA,
		@vCB_AREA,
		@vUS_CODIGO,
		@vKA_FEC_MOV,
		@vKA_HOR_MOV
	IF ( @@ROWCOUNT  <> 0 )
	BEGIN
		UPDATE 	COLABORA
		SET 	CB_AREA = @VCB_AREA,
	    		CB_AR_FEC = @VKA_FECHA,
	    		CB_AR_HOR = @VKA_HORA
		WHERE  	CB_CODIGO = @EMPLEADO
	END
	ELSE
	BEGIN
  		SELECT  @vKA_ACTIVO = CB_ACTIVO,
			@vKA_FECHA = CB_FEC_ING,
			@vCB_AREA = CB_AREA
		FROM 	COLABORA
		WHERE 	CB_CODIGO = @Empleado

		IF ( @vCB_AREA <> '' ) and ( @vKA_ACTIVO = 'S' )    /* Est� Activo y tiene Area Asignada */
  		BEGIN
      			SELECT @vKA_HORA = '0001'
      			SELECT @vKA_FEC_MOV = Getdate()
      			SELECT @VKA_HOR_MOV = '0001'
			INSERT INTO KAR_AREA (  CB_CODIGO,
						KA_FECHA,
						KA_HORA,
						CB_AREA,
						KA_FEC_MOV,
						KA_HOR_MOV )
      				VALUES ( @Empleado,
					 @vKA_FECHA,
					 @vKA_HORA,
					 @vCB_AREA,
					 @vKA_FEC_MOV,
					 @vKA_HOR_MOV )

			UPDATE 	COLABORA
			SET 	CB_AR_FEC = @vKA_FECHA,
				CB_AR_HOR = @vKA_HORA
      			WHERE ( CB_CODIGO = @Empleado )
  		END
	END

	CLOSE Cur_RecalculaArea
	DEALLOCATE Cur_RecalculaArea
END
GO

/* Recalcula el �rea de todos los empleados */
/* Patch 375 # Seq 33: agregado */
CREATE  PROCEDURE DBO.RECALCULA_AREA_TODOS
AS
DECLARE @CB_CODIGO INTEGER
begin
	SET NOCOUNT ON
        DECLARE Cursor_AreaTodos CURSOR FOR
		select CB_CODIGO
		from COLABORA
		order by CB_CODIGO
	OPEN Cursor_AreaTodos
	FETCH NEXT FROM Cursor_AreaTodos INTO	@CB_CODIGO
	WHILE @@FETCH_STATUS = 0
     	begin
          	EXEC RECALCULA_AREA @CB_CODIGO

		FETCH NEXT FROM Cursor_AreaTodos
		INTO	@CB_CODIGO
     	end
	CLOSE Cursor_AreaTodos
	DEALLOCATE Cursor_AreaTodos
END
GO

/* Para dejar datos de �rea en COLABORA correctamente */
/* Patch 375 # Seq 500: agregado */
EXEC RECALCULA_AREA_TODOS
GO

/* Determina el �rea a la que pertenece un empleado en una fecha / hora usando el Kardex */
/* Patch 375 # Seq 55: agregado */
CREATE FUNCTION DBO.SP_FECHA_AREA_KARDEX (
	@EMPLEADO INTEGER,
	@FECHA DATETIME,
	@HORA CHAR(4) )
RETURNS @RegKardex TABLE (
      	KA_FECHA DATETIME,
	KA_HORA CHAR(4),
	CB_AREA CHAR(6),
	US_CODIGO SMALLINT,
	KA_FEC_MOV DATETIME,
	KA_HOR_MOV CHAR(4) )
AS
BEGIN
     insert into @RegKardex
             select TOP 1 KA_FECHA, KA_HORA, CB_AREA, US_CODIGO, KA_FEC_MOV, KA_HOR_MOV
             from KAR_AREA
             where ( CB_CODIGO = @Empleado ) and ( ( KA_FECHA < @Fecha ) or ( KA_FECHA = @Fecha and KA_HORA <= @Hora ) )
             order by KA_FECHA desc, KA_HORA desc
     if ( @@ROWCOUNT = 0 )
     begin
             insert into @RegKardex
                     select TOP 1 KA_FECHA, KA_HORA, CB_AREA, US_CODIGO, KA_FEC_MOV, KA_HOR_MOV
                     from KAR_AREA
                     where ( CB_CODIGO = @Empleado )
                     order by KA_FECHA, KA_HORA
             if ( @@ROWCOUNT = 0 )
             begin
                     set @Fecha = '12/30/1899'
                     insert into @RegKardex
                     values ( @Fecha, '0000', '', 0, @Fecha, '0000' )
             end
     end
     return
END
GO

/* Determina el �rea a la que pertenece un empleado en una fecha / hora usando COLABORA y/o Kardex */
/* Patch 375 # Seq 57: agregado */
CREATE FUNCTION DBO.SP_FECHA_AREA
	( @EMPLEADO INTEGER,
    	  @FECHA DATETIME,
    	  @HORA CHAR(4) )
RETURNS CHAR(6)
AS
BEGIN
     DECLARE @AREA CHAR(6);
     select @AREA = CB_AREA from COLABORA where
     ( CB_CODIGO = @EMPLEADO ) and
     ( CB_FEC_ING <= @FECHA ) and
     ( ( CB_AR_FEC < @FECHA ) or
     ( ( CB_AR_FEC = @FECHA ) and ( CB_AR_HOR <= @HORA ) ) );
     IF ( @AREA IS NULL )
     BEGIN
       	  SELECT @AREA =( SELECT CB_AREA FROM DBO.SP_FECHA_AREA_KARDEX( @EMPLEADO, @FECHA, @HORA ) );
     END
     IF( @AREA IS NULL )
     BEGIN
    	  SELECT @AREA = '';
     END
     RETURN @AREA
END
GO

/* Determina la lista de empleados de un supervisor y al �rea que pertenecen */
/* cada uno de acuerdo a la fecha y hora */
/* Patch 375 # Seq 59: agregado */
CREATE FUNCTION DBO.SP_LISTA_AREA
	( @FECHA DATETIME,
          @HORA CHAR(4),
    	  @USUARIO SMALLINT )
RETURNS @RegListaArea TABLE
	( CB_CODIGO INTEGER,
          CB_AREA CHAR(6))
AS
BEGIN
     DECLARE @MATCHES INTEGER,
	     @MATCHES2 INTEGER,
	     @CB_CODIGO INTEGER,
	     @CB_AREA CHAR(6);
     /* Cuando no hay ningun �rea especificada se aplican todas */
     SELECT @MATCHES = (SELECT COUNT(*) FROM SUP_AREA WHERE ( US_CODIGO = @USUARIO ));
     DECLARE Temporal CURSOR for
        select COLABORA.CB_CODIGO, ( select DBO.SP_FECHA_AREA( COLABORA.CB_CODIGO, @FECHA, @HORA ) ) CB_AREA from COLABORA
        where ( ( select DBO.SP_STATUS_ACT ( @FECHA, COLABORA.CB_CODIGO ) ) = 1 )
        order by COLABORA.CB_CODIGO
     OPEN Temporal
     FETCH NEXT FROM Temporal INTO
     @CB_CODIGO,
     @CB_AREA
     WHILE @@FETCH_STATUS = 0
     BEGIN
        /* Si no hay �rea � se aplican todas las �reas al usuario */
   	IF ( ( @CB_AREA = '' ) or ( @Matches = 0 ) )
   	BEGIN
      		INSERT INTO @RegListaArea VALUES( @CB_CODIGO, @CB_AREA );
   	END
        ELSE
   	BEGIN
             SELECT @MATCHES2 = (SELECT COUNT(*) FROM SUP_AREA
             WHERE ( US_CODIGO = @USUARIO ) AND ( CB_AREA = @CB_AREA ));
             IF ( @MATCHES2 > 0 )
             BEGIN
                  INSERT INTO @RegListaArea VALUES( @CB_CODIGO, @CB_AREA );
             END
        END
	FETCH NEXT FROM Temporal INTO
     	@CB_CODIGO,
     	@CB_AREA
     END
     CLOSE Temporal
     DEALLOCATE Temporal
     RETURN
END
GO

/* Determina los empleados pertenecientes a un �rea en una fecha / hora */
/* Patch 375 # Seq 61: agregado */
CREATE  FUNCTION DBO.SP_LISTA_AREA_EMPLEADOS
	( @FECHA DATETIME,
    	  @HORA CHAR(4),
          @AREA CHAR(6) )
RETURNS @RegListaEmpleados TABLE (
	CB_CODIGO Integer )
AS
BEGIN
     DECLARE @Empleado INTEGER;
     DECLARE Temporal CURSOR for
        select COLABORA.CB_CODIGO from COLABORA where
        ( ( select DBO.SP_STATUS_ACT ( @FECHA, COLABORA.CB_CODIGO ) ) = 1 ) and
        ( ( select DBO.SP_FECHA_AREA( COLABORA.CB_CODIGO, @FECHA, @HORA ) ) = @AREA )
        order by COLABORA.CB_CODIGO
     OPEN Temporal;
     FETCH NEXT FROM Temporal INTO @Empleado;
     WHILE @@FETCH_STATUS = 0
     BEGIN
          INSERT INTO @RegListaEmpleados VALUES ( @Empleado );
          FETCH NEXT FROM Temporal INTO @Empleado;
     END;
     CLOSE Temporal
     DEALLOCATE Temporal
     RETURN
END
GO

/* Determina los empleados que pertenecen a una c�dula */
/* Patch 375 # Seq 63: agregado */
CREATE FUNCTION DBO.SP_CEDULA_EMPLEADOS
	( @CEDULA INTEGER,
          @FECHA DATETIME,
          @HORA CHAR(4),
	  @USUARIO INTEGER )
RETURNS @RegListaEmpleados TABLE (
	CB_CODIGO Integer,
        CAPTURADO Integer )
AS
BEGIN
     DECLARE @CUANTOS INTEGER;
     DECLARE @EMPLEADO INTEGER;
     DECLARE @HAY_EMPLEADOS INTEGER;
     DECLARE @AREA CHAR(6);
     SELECT @AREA = CE_AREA, @CUANTOS = ( select COUNT(*) from CED_EMP CE where ( CE.CE_FOLIO = C.CE_FOLIO ) )
     FROM CEDULA C where ( CE_FOLIO = @CEDULA )
     IF( @AREA IS NOT NULL )
     BEGIN
          IF ( @CUANTOS = 0 )
          BEGIN
               DECLARE Temporal CURSOR for
                     SELECT CB_CODIGO FROM SP_LISTA_AREA_EMPLEADOS( @FECHA, @HORA, @AREA )
               OPEN Temporal;
               FETCH NEXT FROM Temporal INTO
               @EMPLEADO;
               WHILE @@FETCH_STATUS = 0
               BEGIN
                      SELECT @HAY_EMPLEADOS = COUNT(*) FROM TMPLABOR T WHERE
                     ( T.US_CODIGO = @USUARIO ) and ( T.CB_CODIGO = @EMPLEADO ) and ( T.TL_APLICA = 'S' )
                     if ( @HAY_EMPLEADOS > 0 )
                     begin
                          INSERT INTO @RegListaEmpleados VALUES ( @EMPLEADO, @CUANTOS );
                     end
                      FETCH NEXT FROM Temporal INTO
                      @EMPLEADO;
               END
               CLOSE Temporal
               DEALLOCATE Temporal
          END
     END
     ELSE
     BEGIN
          DECLARE Temporal CURSOR for
             SELECT CB_CODIGO FROM CED_EMP CE WHERE ( CE.CE_FOLIO = @CEDULA ) and
             ( ( SELECT COUNT(*) FROM TMPLABOR T WHERE ( T.US_CODIGO = @USUARIO ) and ( T.CB_CODIGO = CE.CB_CODIGO ) and ( T.CB_AREA <> '' ) and ( T.TL_APLICA = 'S' ) ) > 0 )
          OPEN Temporal;
          FETCH NEXT FROM Temporal INTO
          @EMPLEADO;
          WHILE @@FETCH_STATUS = 0
          BEGIN
               INSERT INTO @RegListaEmpleados VALUES ( @EMPLEADO, @CUANTOS );
               FETCH NEXT FROM Temporal INTO
               @EMPLEADO;
          END
          CLOSE Temporal
          DEALLOCATE Temporal
     END
     RETURN
END
GO

/* Proratea las piezas entre los WORKS de una c�dula */
/* Patch 375 # Seq 65: agregado */
CREATE PROCEDURE DBO.SP_CEDULA_PIEZAS( @CEDULA INTEGER )
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @PIEZAS NUMERIC( 15, 4 );
    DECLARE @TIEMPO NUMERIC( 15, 4 );
    SELECT @PIEZAS = CE_PIEZAS FROM CEDULA WHERE ( CE_FOLIO = @CEDULA );
    IF ( ( @PIEZAS IS NOT  NULL  ) AND ( @PIEZAS > 0 ) )
     BEGIN
          SELECT @TIEMPO = SUM( WK_TIEMPO ) FROM WORKS WHERE ( WK_CEDULA = @CEDULA );
          IF ( ( @TIEMPO IS  NOT NULL ) AND ( @TIEMPO > 0 ) )
          BEGIN
               UPDATE WORKS SET WK_PIEZAS = @PIEZAS * ( WK_TIEMPO / @TIEMPO )
               WHERE ( WK_CEDULA = @CEDULA );

          END
     END
END
GO

/* Determina las �reas a las que perteneci� un empleado en un per�odo de tiempo */
/* Patch 375 # Seq 67: agregado */
CREATE FUNCTION DBO.SP_EMPLEADO_AREAS
	( @EMPLEADO INTEGER,
    	  @FECHAINICIAL DATETIME,
    	  @HORAINICIAL CHAR(4),
    	  @FECHAFINAL DATETIME,
    	  @HORAFINAL CHAR(4) )
RETURNS @RegEmpleadoAreas TABLE
	( CB_AREA CHAR(6),
    	  KA_FECHA DATETIME,
    	  KA_HORA CHAR(4))
AS
BEGIN
     DECLARE @CB_AREA CHAR(6),
             @KA_FECHA DATETIME,
	     @KA_HORA CHAR(4)

     SELECT @CB_AREA = CB_AREA, @KA_FECHA = CB_AR_FEC, @KA_HORA = CB_AR_HOR
     FROM COLABORA WHERE ( CB_CODIGO = @EMPLEADO )
     IF ( ( @FECHAINICIAL > @KA_FECHA ) OR ( ( @FECHAINICIAL = @KA_FECHA ) AND ( @HORAINICIAL > @KA_HORA ) ) )
     BEGIN
          SELECT @KA_FECHA = @FECHAINICIAL
          SELECT @KA_HORA = @HORAINICIAL
          INSERT INTO @RegEmpleadoAreas VALUES ( @CB_AREA, @KA_FECHA, @KA_HORA );
     END
     ELSE
     BEGIN
          SELECT @CB_AREA = (SELECT DBO.SP_FECHA_AREA( @EMPLEADO, @FECHAINICIAL, @HORAINICIAL ) );
          SELECT @KA_FECHA = @FECHAINICIAL;
          SELECT @KA_HORA = @HORAINICIAL;
          INSERT INTO @RegEmpleadoAreas VALUES ( @CB_AREA, @KA_FECHA, @KA_HORA )
          DECLARE CursorTemporalEmp CURSOR for
             SELECT CB_AREA, KA_FECHA, KA_HORA FROM KAR_AREA WHERE
             ( CB_CODIGO = @EMPLEADO ) AND
             ( ( KA_FECHA > @FECHAINICIAL ) OR ( ( KA_FECHA = @FECHAINICIAL ) AND ( KA_HORA > @HORAINICIAL ) ) ) AND
             ( ( KA_FECHA < @FECHAFINAL ) OR ( ( KA_FECHA = @FECHAFINAL ) AND ( KA_HORA <= @HORAFINAL ) ) )
             ORDER BY KA_FECHA, KA_HORA
          OPEN CursorTemporalEmp
          FETCH NEXT FROM CursorTemporalEmp INTO @CB_AREA,
	                                         @KA_FECHA,
                                                 @KA_HORA
          WHILE @@FETCH_STATUS = 0
     	  BEGIN
               INSERT INTO @RegEmpleadoAreas VALUES ( @CB_AREA, @KA_FECHA, @KA_HORA );
               FETCH NEXT FROM CursorTemporalEmp INTO @CB_AREA,
                                                      @KA_FECHA,
                                                      @KA_HORA
          END
          CLOSE CursorTemporalEmp
          DEALLOCATE CursorTemporalEmp
     END
     RETURN
END
GO

/* Determina el area default de un empleado a una fecha */
/* Patch 375 # Seq 69: agregado */
CREATE FUNCTION DBO.SP_FECHA_AREA_DEFAULT( @EMPLEADO INTEGER, @FECHA DATETIME )
RETURNS CHAR(6)
as
begin
     declare @AREA CHAR(6);
     SELECT @AREA = CB_AREA FROM SP_FECHA_AREA_KARDEX( @EMPLEADO, @FECHA, '0000' );
     /* Si no hay area inicial, hay que preguntar por una asignaci�n en la fecha */
     if ( ( @AREA is NULL ) or ( @AREA = '' ) )
     begin
          select @AREA = K1.CB_AREA from KAR_AREA K1 where
          ( K1.CB_CODIGO = @EMPLEADO ) and
          ( K1.KA_FECHA = @FECHA ) and
          ( K1.KA_HORA = ( select MIN( K2.KA_HORA ) from KAR_AREA K2 where
          ( K2.CB_CODIGO = K1.CB_CODIGO ) and
          ( K2.KA_FECHA = K1.KA_FECHA ) ) );
          if ( @AREA is NULL )
          begin
               select @AREA = '';
          end
     end
     return @AREA;
end

GO

/* Inicializa el temporal que indica los empleados sujetos de Labor en una fecha */
/* Patch 375 # Seq 71: agregado */
CREATE PROCEDURE DBO.SP_TMPLABOR_START( @USUARIO INTEGER, @FECHA DATETIME)
AS
  DECLARE @STATUS_EMP SMALLINT;
  DECLARE @AU_CHECADAS INTEGER;
  DECLARE @CB_CODIGO INTEGER;
  DECLARE @CB_FEC_KAR DATETIME;
  DECLARE @CB_TURNO CHAR(6);
  DECLARE @CB_AREA CHAR(6);
BEGIN
     SET NOCOUNT ON;
     /* Este SP tiene implementadas algunas de las reglas con las cuales se determina */
     /* si los empleados son sujetos de Labor */
     UPDATE TMPLABOR SET TL_APLICA = 'N', TL_CALCULA = 'N' WHERE ( US_CODIGO = @USUARIO );
     DECLARE Temporal CURSOR FOR
        SELECT TMPLABOR.CB_CODIGO, COLABORA.CB_FEC_KAR, COLABORA.CB_TURNO FROM TMPLABOR
        left outer join COLABORA ON ( COLABORA.CB_CODIGO = TMPLABOR.CB_CODIGO )
        WHERE ( TMPLABOR.US_CODIGO = @USUARIO ) order by TMPLABOR.CB_CODIGO
     OPEN Temporal
     FETCH NEXT FROM Temporal INTO @CB_CODIGO, @CB_FEC_KAR, @CB_TURNO;
     WHILE @@FETCH_STATUS = 0
     BEGIN
       SELECT @CB_AREA = ( SELECT DBO.SP_FECHA_AREA_DEFAULT( @CB_CODIGO, @FECHA ) );
       SELECT @AU_CHECADAS = COUNT(*) from CHECADAS where
                     ( CB_CODIGO = @CB_CODIGO ) and
                     ( AU_FECHA = @FECHA ) and
                     ( CH_TIPO in ( 1, 2 ) );
       IF ( @AU_CHECADAS = 0 )
       BEGIN
            /* Pregunta si hay horas por checadas ( AU_HORASCK + AU_NUM_EXT ) */
            /* � si hay horas a pagar ( AU_HORAS ) en d�as que no son permisos con goce ni festivos */
            /* � si hay horas extras a pagar o descansos trabajados */
            SELECT @AU_CHECADAS = COUNT(*) from AUSENCIA where
            ( CB_CODIGO = @CB_CODIGO ) and
            ( AU_FECHA = @FECHA ) and
            ( ( AU_HORASCK + AU_NUM_EXT > 0 ) or
            ( ( AU_HORAS > 0 ) and not ( AU_TIPODIA in ( 3, 8 ) ) ) or
            ( ( AU_EXTRAS + AU_DES_TRA ) > 0 ) );
       END
       IF ( @AU_CHECADAS = 0 )
       BEGIN
            /* Solo vale la pena preguntar si no hay asistencia */
            /* Peligro cuando cambia el criterio para seleccionar empleados sujetos a LABOR */
            SELECT  @STATUS_EMP = ( SELECT DBO.SP_STATUS_EMP( @FECHA, @CB_CODIGO ) as RESULTADO );
            IF ( @FECHA < @CB_FEC_KAR )
            BEGIN
                 SELECT @CB_TURNO = CB_TURNO FROM SP_FECHA_KARDEX( @FECHA, @CB_CODIGO );
            END
            UPDATE TMPLABOR SET
            CB_AREA = @CB_AREA,
            CB_TURNO = @CB_TURNO,
            TL_STATUS = @STATUS_EMP
            WHERE ( US_CODIGO = @USUARIO ) and ( CB_CODIGO = @CB_CODIGO );
       END
       ELSE
       BEGIN
            UPDATE TMPLABOR SET
            CB_AREA = @CB_AREA,
            CB_TURNO = '',
            TL_STATUS = 0,
            TL_APLICA = 'S'
            WHERE ( US_CODIGO = @USUARIO ) and ( CB_CODIGO = @CB_CODIGO );
       END
       /* Lee el area inicial del empleado en la fecha a las 00:00 hrs */
       FETCH NEXT FROM Temporal INTO @CB_CODIGO, @CB_FEC_KAR, @CB_TURNO;
     END
     CLOSE Temporal
     DEALLOCATE Temporal
END
GO

/* NUEVO: se usa para generar reportes de balanza de n�mina */
/* Patch 375 # Seq 49: agregado */
CREATE PROCEDURE DBO.SP_LISTADO_BALANZA (
  @USUARIO SMALLINT,
  @YEAR SMALLINT,
  @TIPO SMALLINT,
  @NUMERO SMALLINT,
  @EMPLEADO INTEGER,
  @CONCEPTO SMALLINT,
  @COLUMNA SMALLINT,
  @DESCRIPCION CHAR(30),
  @MONTO NUMERIC(15, 2),
  @HORAS NUMERIC(15, 2),
  @REFERENCIA VARCHAR(8),
  @TP_NIVEL1 CHAR(6),
  @TP_NIVEL2 CHAR(6),
  @TP_NIVEL3 CHAR(6),
  @TP_NIVEL4 CHAR(6),
  @TP_NIVEL5 CHAR(6)
)  AS
BEGIN
     SET NOCOUNT ON
     DECLARE @PERCEPCION SMALLINT;
     DECLARE @DEDUCCION SMALLINT;

     SELECT @PERCEPCION = COUNT(*) FROM DBO.TMPBALA2
     WHERE TP_USER = @USUARIO AND
           CB_CODIGO = @EMPLEADO AND
           TP_NIVEL1 = @TP_NIVEL1 AND
           TP_NIVEL2 = @TP_NIVEL2 AND
           TP_NIVEL3 = @TP_NIVEL3 AND
           TP_NIVEL4 = @TP_NIVEL4 AND
           TP_NIVEL5 = @TP_NIVEL5 AND
           TP_NUM_PER > 0

     SELECT @DEDUCCION=COUNT(*) FROM TMPBALA2
     WHERE TP_USER = @USUARIO AND
           CB_CODIGO = @EMPLEADO AND
           TP_NIVEL1 = @TP_NIVEL1 AND
           TP_NIVEL2 = @TP_NIVEL2 AND
           TP_NIVEL3 = @TP_NIVEL3 AND
           TP_NIVEL4 = @TP_NIVEL4 AND
           TP_NIVEL5 = @TP_NIVEL5 AND
           TP_NUM_DED > 0

     IF (@COLUMNA = 1) 
     BEGIN
          IF (@PERCEPCION >= @DEDUCCION)
          BEGIN
               INSERT INTO TMPBALA2 ( TP_ORDEN,
                                      CB_CODIGO,
                                      PE_YEAR,
                                      PE_TIPO,
                                      PE_NUMERO,
                                      TP_USER,
                                      TP_NUM_PER,
                                      TP_MON_PER,
                                      TP_DES_PER,
                                      TP_HOR_PER,
                                      TP_REF_PER,
                                      TP_NIVEL1,
                                      TP_NIVEL2,
                                      TP_NIVEL3,
                                      TP_NIVEL4,
                                      TP_NIVEL5)
                            VALUES( @PERCEPCION,
                                    @EMPLEADO,
                                    @YEAR,
                                    @TIPO,
                                    @NUMERO,
                                    @USUARIO,
                                    @CONCEPTO,
                                    @MONTO,
                                    @DESCRIPCION,
                                    @HORAS,
                                    @REFERENCIA,
                                    @TP_NIVEL1,
                                    @TP_NIVEL2,
                                    @TP_NIVEL3,
                                    @TP_NIVEL4,
                                    @TP_NIVEL5 );

          END
          ELSE
          BEGIN
               UPDATE DBO.TMPBALA2 SET
                      TP_NUM_PER=@CONCEPTO,
                      TP_MON_PER=@MONTO,
                      TP_DES_PER=@DESCRIPCION,
                      TP_HOR_PER=@HORAS,
                      TP_REF_PER=@REFERENCIA
               WHERE TP_USER=@USUARIO AND
                     CB_CODIGO=@EMPLEADO AND
                     TP_NIVEL1 = @TP_NIVEL1 AND
                     TP_NIVEL2 = @TP_NIVEL2 AND
                     TP_NIVEL3 = @TP_NIVEL3 AND
                     TP_NIVEL4 = @TP_NIVEL4 AND
                     TP_NIVEL5 = @TP_NIVEL5 AND
                     TP_ORDEN=@PERCEPCION;
          END
     END
     ELSE
     BEGIN
          IF (@DEDUCCION >= @PERCEPCION)
          BEGIN
               INSERT INTO DBO.TMPBALA2 ( TP_ORDEN,
                                      CB_CODIGO,
                                      PE_YEAR,
                                      PE_TIPO,
                                      PE_NUMERO,
                                      TP_USER,
                                      TP_NUM_DED,
                                      TP_MON_DED,
                                      TP_DES_DED,
                                      TP_HOR_DED,
                                      TP_REF_DED,
                                      TP_NIVEL1,
                                      TP_NIVEL2,
                                      TP_NIVEL3,
                                      TP_NIVEL4,
                                      TP_NIVEL5)
                            VALUES( @DEDUCCION,
                                    @EMPLEADO,
                                    @YEAR,
                                    @TIPO,
                                    @NUMERO,
                                    @USUARIO,
                                    @CONCEPTO,
                                    @MONTO,
                                    @DESCRIPCION,
                                    @HORAS,
                                    @REFERENCIA,
                                    @TP_NIVEL1,
                                    @TP_NIVEL2,
                                    @TP_NIVEL3,
                                    @TP_NIVEL4,
                                    @TP_NIVEL5 );
          END
          ELSE
          BEGIN
               UPDATE DBO.TMPBALA2 SET
                      TP_NUM_DED=@CONCEPTO,
                      TP_MON_DED=@MONTO,
                      TP_DES_DED=@DESCRIPCION,
                      TP_HOR_DED=@HORAS,
                      TP_REF_DED=@REFERENCIA
               WHERE TP_USER=@USUARIO AND
                     CB_CODIGO=@EMPLEADO AND
                     TP_NIVEL1 = @TP_NIVEL1 AND
                     TP_NIVEL2 = @TP_NIVEL2 AND
                     TP_NIVEL3 = @TP_NIVEL3 AND
                     TP_NIVEL4 = @TP_NIVEL4 AND
                     TP_NIVEL5 = @TP_NIVEL5 AND
                     TP_ORDEN=@DEDUCCION;
          END;
     END;
END

GO

/************************/
/* Triggers modificados */
/************************/

/* Ya se tiene un foreign key constraint entre DEFSTEPS y PARTES */
/* Hay que modificar TU_PARTES */

/* Patch 375 # Seq 36: modificado */
ALTER TRIGGER TU_PARTES ON PARTES AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( AR_CODIGO )
  BEGIN
	declare @NewCodigo CodigoParte, @OldCodigo CodigoParte;

	select @NewCodigo = AR_CODIGO from   Inserted;
        select @OldCodigo = AR_CODIGO from   Deleted;

	UPDATE WORKS SET AR_CODIGO = @NewCodigo WHERE AR_CODIGO = @OldCodigo;
	UPDATE WORDER SET AR_CODIGO = @NewCodigo WHERE AR_CODIGO = @OldCodigo;
	UPDATE CEDULA SET AR_CODIGO = @NewCodigo WHERE AR_CODIGO = @OldCodigo;
  END
END

/* Eliminar WOFIJA y STEPS del trigger TU_WORDER de MSSQL ( se hace por FKEYS ) */
/* Patch 375 # Seq 39: modificado */
ALTER TRIGGER TU_WORDER ON WORDER AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( WO_NUMBER )
  BEGIN
	declare @NewCodigo OrdenTrabajo, @OldCodigo OrdenTrabajo;

	select @NewCodigo = WO_NUMBER from   Inserted;
        select @OldCodigo = WO_NUMBER from   Deleted;

   UPDATE WORKS SET WO_NUMBER = @NewCodigo WHERE WO_NUMBER = @OldCodigo;
   UPDATE CEDULA SET WO_NUMBER = @NewCodigo WHERE WO_NUMBER = @OldCodigo;
   UPDATE CED_WORD SET WO_NUMBER = @NewCodigo WHERE WO_NUMBER=@OldCodigo;
  END
END

/* Actualizar KAR_AREA cuando cambia el c�digo del empleado */
/* Patch 375 # Seq 35: agregado */
ALTER TABLE KAR_AREA
       ADD CONSTRAINT FK_KAR_AREA_COLABORA
       FOREIGN KEY (CB_CODIGO)
       REFERENCES COLABORA
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

/* Patch 375 # Seq 47: agregado */
CREATE TRIGGER TU_AREA ON AREA AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo CODIGO, @OldCodigo CODIGO;

	select @NewCodigo = CB_CODIGO from Inserted;
        select @OldCodigo = CB_CODIGO from Deleted;
        update SUP_AREA set CB_AREA = @NewCodigo where ( CB_AREA = @OldCodigo );
        update KAR_AREA set CB_AREA = @NewCodigo where ( CB_AREA = @OldCodigo );
        update COLABORA set CB_AREA = @NewCodigo where ( CB_AREA = @OldCodigo );
  END
END
GO

/***********************************************************/
/* Agregar a todos los triggers el comando: SET NOCOUNT ON */
/* al comienzo del trigger.                                */
/* Ejemplo:                                                */
/*
CREATE TRIGGER TU_TURNO ON TURNO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON
  IF UPDATE( TU_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TU_CODIGO from   Inserted;
        select @OldCodigo = TU_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_TURNO = @NewCodigo WHERE CB_TURNO = @OldCodigo;
	UPDATE KARDEX	SET CB_TURNO = @NewCodigo WHERE CB_TURNO = @OldCodigo;
	UPDATE NOMINA	SET CB_TURNO = @NewCodigo WHERE CB_TURNO = @OldCodigo;
	UPDATE AUSENCIA	SET CB_TURNO = @NewCodigo WHERE CB_TURNO = @OldCodigo;
	UPDATE KARCURSO	SET CB_TURNO = @NewCodigo WHERE CB_TURNO = @OldCodigo;
  END
END
/***********************************************************/

/***************************************************************************/
/* Stored procedures modificados como resultado de cambios en programaci�n */
/***************************************************************************/

/* No actualizaba antes el usuario quien modific� la n�mina */
/* Patch 375 # Seq 146: modificado */
alter procedure DBO.SET_STATUS_PERIODO(
@ANIO SMALLINT,
@TIPO SMALLINT,
@NUMERO SMALLINT,
@USUARIO SMALLINT,
@MAXSTATUS SMALLINT OUTPUT )
as
begin
     SET NOCOUNT ON
     declare @Diferentes SmallInt;
     declare @Status SmallInt;
     declare @Empleados Integer;
     declare @Percepciones NUMERIC( 15, 2 );
     declare @Deducciones NUMERIC( 15, 2 );
     declare @Netos NUMERIC( 15, 2 );
     declare @TotEmpleados Integer;
     declare @TotPercepciones NUMERIC( 15, 2 );
     declare @TotDeducciones NUMERIC( 15, 2 );
     declare @TotNetos NUMERIC( 15, 2 );
     declare TemporalNomina CURSOR for
        select N.NO_STATUS, COUNT(*), SUM( N.NO_PERCEPC ), SUM( N.NO_DEDUCCI ), SUM( N.NO_NETO )
        from DBO.NOMINA as N
        where  ( N.PE_YEAR = @Anio ) and
               ( N.PE_TIPO = @Tipo ) and
               ( N.PE_NUMERO = @Numero )
        group by N.NO_STATUS
        order by N.NO_STATUS;
     set @Diferentes = 0;
     set @MaxStatus = 0;
     set @TotEmpleados = 0;
     set @TotPercepciones = 0;
     set @TotDeducciones = 0;
     set @TotNetos = 0;
	 open TemporalNomina;
     fetch NEXT from TemporalNomina
     into @Status,
          @Empleados,
          @Percepciones,
          @Deducciones,
          @Netos;
     while ( @@Fetch_Status = 0 )
     begin
          set @Diferentes = @Diferentes + 1;
          set @MaxStatus = @Status;
          set @TotEmpleados = @TotEmpleados + @Empleados;
          set @TotPercepciones = @TotPercepciones + @Percepciones;
          set @TotDeducciones = @TotDeducciones + @Deducciones;
          set @TotNetos = @TotNetos + @Netos;
          fetch NEXT from TemporalNomina
          into @Status,
               @Empleados,
               @Percepciones,
               @Deducciones,
               @Netos;
     end;
	 close TemporalNomina;
	 deallocate TemporalNomina;
     if ( @Diferentes > 0 )
     begin
          if ( @Diferentes > 1 )
          begin
               if ( @MaxStatus = 4 )
                  set @MaxStatus = 3
               else
                   if ( @MaxStatus = 6 )
                      set @MaxStatus = 5;
               set @TotPercepciones = 0;
               set @TotDeducciones = 0;
               set @TotNetos = 0;
          end;
          if ( @MaxStatus = 1 )
             set @MaxStatus = 2;
     end;
     update DBO.PERIODO set
            PE_STATUS = @MaxStatus,
            PE_NUM_EMP = @TotEmpleados,
            PE_TOT_PER = @TotPercepciones,
            PE_TOT_DED = @TotDeducciones,
            PE_TOT_NET = @TotNetos,
            US_CODIGO = @Usuario
     where ( PE_YEAR = @Anio ) and
           ( PE_TIPO = @Tipo ) and
           ( PE_NUMERO = @Numero );
end
GO

/* Cambi� llamada a SET_STATUS_PERIODO */
/* Patch 375 # Seq 155: modificado */
ALTER PROCEDURE DBO.AFECTA_PERIODO(
    			@ANIO SMALLINT,
    			@TIPO SMALLINT,
    			@NUMERO SMALLINT,
			@USUARIO SMALLINT,
    			@STATUSVIEJO SMALLINT,
    			@STATUSNUEVO SMALLINT,
    			@FACTOR INTEGER )

AS
	SET NOCOUNT ON
	DECLARE @CONCEPTO INTEGER;
	DECLARE @MES SMALLINT;
	DECLARE @CONTADOR INTEGER;
	DECLARE @TOTAL NUMERIC(15,2);
	DECLARE @TIPOAHORRO CHAR(1);
	DECLARE @TIPOPRESTAMO CHAR(1);
	DECLARE @REFERENCIA VARCHAR(8);
        DECLARE @EMPLEADO INTEGER;

     	select @MES = P.PE_MES from PERIODO P 
	where ( P.PE_YEAR = @Anio ) 
	and ( P.PE_TIPO = @Tipo ) 
	and ( P.PE_NUMERO = @Numero ) 
     
	Declare TempColabora CURSOR for
        	select N.CB_CODIGO from NOMINA N
        	left outer join COLABORA C 
		on ( C.CB_CODIGO = N.CB_CODIGO )
        	where ( N.PE_YEAR = @Anio ) 
		and ( N.PE_TIPO = @Tipo )
		and ( N.PE_NUMERO = @Numero )
		and ( N.NO_STATUS = @StatusViejo )
		and not ( C.CB_CODIGO is NULL )

	OPEN TempColabora
	fetch NEXT FROM TempColabora
	INTO @EMPLEADO 
		
	set @Contador = 0
	WHILE @@FETCH_STATUS = 0
	BEGIN

		EXECUTE AFECTA_EMPLEADO @Empleado, @Anio, @Tipo, @Numero, @Mes, @Factor;
		Declare TempAhorro CURSOR for
       			select A.AH_TIPO, T.TB_CONCEPT 
			from AHORRO A join TAHORRO T on ( T.TB_CODIGO = A.AH_TIPO )
       			where ( A.CB_CODIGO = @Empleado )
			and ( A.AH_STATUS = 0 )

		Open TempAhorro
		fetch NEXT FROM TempAhorro
		into @TipoAhorro, @Concepto 
		while @@FETCH_STATUS = 0 
       		begin
       			select @Contador = COUNT(*), @Total = SUM( M.MO_DEDUCCI ) 
			from MOVIMIEN M 
			where ( M.PE_YEAR = @Anio )
			and ( M.PE_TIPO = @Tipo ) 
			and ( M.PE_NUMERO = @Numero ) 
			and ( M.CB_CODIGO= @Empleado ) 
			and ( M.CO_NUMERO = @Concepto )
			and ( M.MO_ACTIVO = 'S' )

			if ( @Contador > 0 )
       			begin
               			Select @Total = @Factor * @Total;
               			update AHORRO  set
                      		AH_NUMERO = AH_NUMERO + @Factor,
                       		AH_TOTAL  = AH_TOTAL + @Total,
                       		AH_SALDO = AH_SALDO + @Total
               			where ( CB_CODIGO = @Empleado )
				and ( AH_TIPO = @TipoAhorro );
        		end
			fetch NEXT FROM TempAhorro
 			into   @TipoAhorro, @Concepto
		end /* While TempoAhorro */
			
		close TempAhorro /* LO CIERRA */
    		deallocate TempAhorro /* LO LIBERA */
          
		Declare TempPrestamo CURSOR for
             		select P.PR_TIPO, P.PR_REFEREN, T.TB_CONCEPT 
			from PRESTAMO P join TPRESTA T on
                    	( T.TB_CODIGO = P.PR_TIPO )
             		where ( P.CB_CODIGO = @Empleado ) 
			and ( P.PR_STATUS in ( 0, 2 ) )

		Open TempPrestamo
		fetch NEXT FROM TempPrestamo 
		into @TipoPrestamo, @Referencia, @Concepto 

		while @@FETCH_STATUS = 0
       		begin
       			select @Contador = COUNT(*), @Total = SUM( M.MO_DEDUCCI )
			from MOVIMIEN M 
			where ( M.PE_YEAR = @Anio ) 
			and ( M.PE_TIPO = @Tipo ) 
			and ( M.PE_NUMERO = @Numero )
			and ( M.CB_CODIGO= @Empleado ) 
			and ( M.CO_NUMERO = @Concepto ) 
			and ( M.MO_ACTIVO = 'S' ) 
			and ( M.MO_REFEREN = @Referencia )
               			
			if ( @Contador > 0 ) 
       			begin
               			Select @Total = @Factor * @Total;
               			update PRESTAMO set
                       		PR_NUMERO = PR_NUMERO + @Factor,
                       		PR_TOTAL  = PR_TOTAL + @Total,
                       		PR_SALDO = PR_SALDO - @Total
               			where ( CB_CODIGO = @Empleado ) 
				and ( PR_TIPO = @TipoPrestamo )
				and ( PR_REFEREN = @Referencia );
       			end
			fetch NEXT FROM TempPrestamo 
			into @TipoPrestamo, @Referencia, @Concepto 
        	end /* While TempPrestamo */

		close TempPrestamo /* LO CIERRA */
    		deallocate TempPrestamo /* LO LIBERA */
          	
		EXECUTE SET_STATUS_PRESTAMOS @Empleado;

          	if ( @StatusNuevo > 0 )
          	begin
               		update NOMINA set NO_STATUS = @StatusNuevo 
			where ( PE_YEAR = @Anio ) 
			and ( PE_TIPO = @Tipo ) 
			and ( PE_NUMERO = @Numero )
			and ( CB_CODIGO = @Empleado );
          	end
		fetch NEXT FROM TempColabora 
		INTO @EMPLEADO
		set @Contador = @Contador + 1
	end /* While TempColabora */

	close TempColabora /* LO CIERRA */
    	deallocate TempColabora /* LO LIBERA */

     	EXECUTE AFECTA_FALTAS @Anio, @Tipo, @Numero, @Factor
     	if ( @StatusNuevo > 0 ) 
     	begin

          	update NOMINA set NO_STATUS = @StatusNuevo
          	where ( PE_YEAR = @Anio ) 
		and ( PE_TIPO = @Tipo ) 
		and ( PE_NUMERO = @Numero ) 
		and ( ( select COUNT(*) from COLABORA C where ( C.CB_CODIGO = Nomina.CB_CODIGO ) ) = 0 )

          	EXECUTE SET_STATUS_PERIODO @Anio, @Tipo, @Numero, @Usuario, @StatusNuevo
     	end

	RETURN @Contador

GO

/*--Se le agrega un parametro mas al procedure para que acepte la lista de los parametros Defecto 278 */
/* Patch 375 # Seq 39: modificado */
ALTER PROCEDURE DBO.INIT_PROCESS_LOG(
  @Proceso Integer,
  @Usuario Smallint,
  @Fecha   DateTime,
  @Hora    Varchar(8),
  @Maximo  Integer,
  @Params   varchar(255),
  @Folio   Integer OUTPUT )
AS
  SET NOCOUNT ON
  select @Folio = MAX( PC_NUMERO ) from PROCESO

  if ( @Folio is NULL )
    select @Folio = 1
  else
    select @Folio = @Folio + 1

  insert into PROCESO ( PC_PROC_ID,
                        PC_NUMERO,
                        US_CODIGO,
                        PC_FEC_INI,
                        PC_HOR_INI,
                        PC_MAXIMO,
                        PC_ERROR,
			PC_PARAMS ) values
                         ( @Proceso,
                           @Folio,
                           @Usuario,
                           @Fecha,
                           @Hora,
                           @Maximo,
                           'A',
			   @Params )
GO

/*******************************************************************/
/** Cambios al refrescado del diccionario de datos *****************/
/*******************************************************************/

/* CV: 17-julio-2002 */
/* Este stored procedure se utiliza para refrescar el diccionario de datos*/
/* Se cambio este stored procedure para que tambien cambie los datos  */
/* del m�dulo de Presupuesto de Personal */
/* Patch 375 # Seq 163: modificado */
ALTER PROCEDURE SP_TITULO_CAMPO(
	@NUMGLOBAL SMALLINT,
	@TITULODEFAULT VARCHAR(30),
	@NOMBRE VARCHAR(30))
AS
begin
        SET NOCOUNT ON
  	DECLARE @TITULO VARCHAR(30)

	select @Titulo = GL_FORMULA
  	from GLOBAL
  	where GL_CODIGO = @NumGlobal

  	if (( @Titulo is Null ) or ( @Titulo = '' ))
    		SET @Titulo = @TituloDefault;

   	update DICCION
  	set DI_TITULO = @Titulo, DI_TCORTO= @Titulo, DI_CLAVES = ''
  	where DI_CLASIFI IN (5,10,18,32,33,50,124,141,142,143,144,145,146,147,148,149)
	and DI_NOMBRE = @Nombre;
end

/* CV: 17-julio-2002 */
/* Este stored procedure se utiliza para refrescar el diccionario de datos*/
/* Los datos que se actualizan son los de la clasificacion 124 la cual corresponde*/
/* a la entidad enConteo */
/* Se manda llamar desdes el stored procedure SP_REFRECH_DICCION */
/* Patch 375 # Seq 166: agregado */
CREATE PROCEDURE SP_TITULO_CONTEO (
  @NUMGLOBAL SMALLINT,
  @TITULODEFAULT VARCHAR(30),
  @NOMBRE VARCHAR(10)
)  AS
begin
     SET NOCOUNT ON
     declare @Formula Varchar(30);
     declare @Titulo Varchar(30);
     declare @GlobalNivel SmallInt;
     declare @LookUp SmallInt;

     set @Titulo = '';
     set @Lookup = 86;

     select @Formula = GL_FORMULA
     from GLOBAL
     where ( GL_CODIGO = @NumGlobal )

     if (( @Formula is Null ) or ( @Formula = '' ) or ( @Formula = '0' ))
     begin
          set @Titulo = @TituloDefault;
     end
     else
     begin
          if ( @Formula = '1' )
          begin
               set @Titulo = 'Puesto';
               set @Lookup = 59;
          end
          else if ( @Formula = '2' )
          begin
               set @Titulo = 'Turno';
               set @Lookup = 69;
          end
          else if ( @Formula = '3' ) 
          begin
               set @Titulo = 'Clasificaci�n';
               set @Lookup = 9;
          end
          else if ( @Formula = '4' ) 
          begin
               set @Titulo = 'Confidencialidad';
               set @Lookup = 157;
          end
          else
          begin
               /*Quiere decir que es un nivel de organigrama*/
               if ( @Formula = '5' ) 
               begin
                    set @GlobalNivel = 13;
                    set @Lookup = 41;
               end
               else if ( @Formula = '6' ) 
               begin
                    set @GlobalNivel = 14;
                    set @Lookup = 42;
               end
               else if ( @Formula = '7' ) 
               begin
                    set @GlobalNivel = 15;
                    set @Lookup = 43;
               end
               else if ( @Formula = '8' )
               begin
                    set @GlobalNivel = 16;
                    set @Lookup = 44;
               end
               else if ( @Formula = '9' ) 
               begin
                    set @GlobalNivel = 17;
                    set @Lookup = 45;
               end
               else if ( @Formula = '10' ) 
               begin
                    set @GlobalNivel = 18;
                    set @Lookup = 46;
               end
               else if ( @Formula = '11' ) 
               begin
                    set @GlobalNivel = 19;
                    set @Lookup = 47;
               end
               else if ( @Formula = '12' ) 
               begin
                    set @GlobalNivel = 20;
                    set @Lookup = 48;
               end
               else if ( @Formula = '13' )
               begin
                    set @GlobalNivel = 21;
                    set @Lookup = 49;
               end
               else
               begin
                    set @Titulo = @TituloDefault;
               end

               if ( @Titulo = '' )
               begin
                    /*El titulo viene vacio, porque el Sp_titulo_campo, lo investiga*/
                    execute SP_TITULO_CAMPO @GlobalNivel, @TituloDefault, @NOMBRE ;

                    update DICCION
                    set    DI_NUMERO = @LookUp
                    where ( DI_CLASIFI = 124 ) and
                          ( DI_NOMBRE = @NOMBRE );
               end;
          end;
     end;

     if (@Titulo <> '')
     begin
          update DICCION
          set    DI_TITULO = @Titulo,
                 DI_TCORTO = @Titulo,
                 DI_CLAVES = '',
                 DI_NUMERO = @Lookup
          where ( DI_CLASIFI = 124 ) and
                ( DI_NOMBRE = @NOMBRE );
     end;
end;

/* CV: 17-julio-2002 */
/* Este stored procedure se utiliza para refrescar el diccionario de datos*/
/* Se cambio este stored procedure para que tambien cambie los datos  */
/* del m�dulo de Presupuesto de Personal */
/* Patch 375 # Seq 168: modificado */
ALTER PROCEDURE SP_REFRESH_DICCION
as
begin
  SET NOCOUNT ON
  execute  SP_TITULO_TABLA 13, 'Nivel de Organigrama #1', 41 ;
  execute  SP_TITULO_TABLA 14, 'Nivel de Organigrama #2', 42 ;
  execute  SP_TITULO_TABLA 15, 'Nivel de Organigrama #3', 43 ;
  execute  SP_TITULO_TABLA 16, 'Nivel de Organigrama #4', 44 ;
  execute  SP_TITULO_TABLA 17, 'Nivel de Organigrama #5', 45 ;
  execute  SP_TITULO_TABLA 18, 'Nivel de Organigrama #6', 46 ;
  execute  SP_TITULO_TABLA 19, 'Nivel de Organigrama #7', 47 ;
  execute  SP_TITULO_TABLA 20, 'Nivel de Organigrama #8', 48 ;
  execute  SP_TITULO_TABLA 21, 'Nivel de Organigrama #9', 49 ;

  execute  SP_TITULO_TABLA 35, 'Tabla Adicional #1', 19
  execute  SP_TITULO_TABLA 36, 'Tabla Adicional #2', 20
  execute  SP_TITULO_TABLA 37, 'Tabla Adicional #3', 21
  execute  SP_TITULO_TABLA 38, 'Tabla Adicional #4', 22

  execute  SP_TITULO_TABLA 134, 'Orden de Trabajo', 149
  execute  SP_TITULO_TABLA 136, 'Cat�logo de Partes', 141
  execute  SP_TITULO_TABLA 138, 'Cat�logo de Area', 148
  execute  SP_TITULO_TABLA 140, 'Cat�logo de Operaciones', 142 
  execute  SP_TITULO_TABLA 142, 'Cat�logo de Tiempo Muerto', 138
  execute  SP_TITULO_TABLA 143, 'Modulador #1', 135 
  execute  SP_TITULO_TABLA 144, 'Modulador #2', 136 
  execute  SP_TITULO_TABLA 145, 'Modulador #3', 137

  execute  SP_TITULO_CAMPO 13, 'C�digo de Nivel #1', 'CB_NIVEL1'
  execute  SP_TITULO_CAMPO 14, 'C�digo de Nivel #2', 'CB_NIVEL2' 
  execute  SP_TITULO_CAMPO 15, 'C�digo de Nivel #3', 'CB_NIVEL3' 
  execute  SP_TITULO_CAMPO 16, 'C�digo de Nivel #4', 'CB_NIVEL4'
  execute  SP_TITULO_CAMPO 17, 'C�digo de Nivel #5', 'CB_NIVEL5'
  execute  SP_TITULO_CAMPO 18, 'C�digo de Nivel #6', 'CB_NIVEL6' 
  execute  SP_TITULO_CAMPO 19, 'C�digo de Nivel #7', 'CB_NIVEL7' 
  execute  SP_TITULO_CAMPO 20, 'C�digo de Nivel #8', 'CB_NIVEL8' 
  execute  SP_TITULO_CAMPO 21, 'C�digo de Nivel #9', 'CB_NIVEL9' 

  execute  SP_TITULO_CAMPO 13, 'Nuevo Nivel #1', 'EV_NIVEL1' 
  execute  SP_TITULO_CAMPO 14, 'Nuevo Nivel #2', 'EV_NIVEL2' 
  execute  SP_TITULO_CAMPO 15, 'Nuevo Nivel #3', 'EV_NIVEL3' 
  execute  SP_TITULO_CAMPO 16, 'Nuevo Nivel #4', 'EV_NIVEL4' 
  execute  SP_TITULO_CAMPO 17, 'Nuevo Nivel #5', 'EV_NIVEL5' 
  execute  SP_TITULO_CAMPO 18, 'Nuevo Nivel #6', 'EV_NIVEL6'
  execute  SP_TITULO_CAMPO 19, 'Nuevo Nivel #7', 'EV_NIVEL7' 
  execute  SP_TITULO_CAMPO 20, 'Nuevo Nivel #8', 'EV_NIVEL8' 
  execute  SP_TITULO_CAMPO 21, 'Nuevo Nivel #9', 'EV_NIVEL9' 

  execute  SP_TITULO_CAMPO 22, 'Fecha Adicional #1', 'CB_G_FEC_1' 
  execute  SP_TITULO_CAMPO 23, 'Fecha Adicional #2', 'CB_G_FEC_2' 
  execute  SP_TITULO_CAMPO 24, 'Fecha Adicional #3', 'CB_G_FEC_3' 
  execute  SP_TITULO_CAMPO 25, 'Texto Adicional #1', 'CB_G_TEX_1' 
  execute  SP_TITULO_CAMPO 26, 'Texto Adicional #2', 'CB_G_TEX_2'
  execute  SP_TITULO_CAMPO 27, 'Texto Adicional #3', 'CB_G_TEX_3' 
  execute  SP_TITULO_CAMPO 28, 'Texto Adicional #4', 'CB_G_TEX_4'
  execute  SP_TITULO_CAMPO 29, 'N�mero Adicional #1', 'CB_G_NUM_1' 
  execute  SP_TITULO_CAMPO 30, 'N�mero Adicional #2', 'CB_G_NUM_2' 
  execute  SP_TITULO_CAMPO 31, 'N�mero Adicional #3', 'CB_G_NUM_3'
  execute  SP_TITULO_CAMPO 32, 'L�gico Adicional #1', 'CB_G_LOG_1' 
  execute  SP_TITULO_CAMPO 33, 'L�gico Adicional #2', 'CB_G_LOG_2'
  execute  SP_TITULO_CAMPO 34, 'L�gico Adicional #3', 'CB_G_LOG_3' 
  execute  SP_TITULO_CAMPO 35, 'Tabla Adicional #1','CB_G_TAB_1' 
  execute  SP_TITULO_CAMPO 36, 'Tabla Adicional #2','CB_G_TAB_2'
  execute  SP_TITULO_CAMPO 37, 'Tabla Adicional #3','CB_G_TAB_3'
  execute  SP_TITULO_CAMPO 38, 'Tabla Adicional #4','CB_G_TAB_4' 

  execute  SP_TITULO_CAMPO 134, 'Orden de Trabajo','WO_NUMBER' 
  execute  SP_TITULO_CAMPO 136, 'Parte','AR_CODIGO' 
  execute  SP_TITULO_CAMPO 138, 'Area','CB_AREA' 
  execute  SP_TITULO_CAMPO 140, 'Operaci�n','OP_NUMBER' 
  execute  SP_TITULO_CAMPO 142, 'Tiempo Muerto','WK_TMUERTO' 
  execute  SP_TITULO_CAMPO 142, 'Tiempo Muerto','LX_TMUERTO' 
  execute  SP_TITULO_CAMPO 143, 'Modulador #1','LX_MODULA1' 
  execute  SP_TITULO_CAMPO 144, 'Modulador #2','LX_MODULA2' 
  execute  SP_TITULO_CAMPO 145, 'Modulador #3','LX_MODULA3'
  execute  SP_TITULO_CAMPO 143, 'Modulador #1','WK_MOD_1'
  execute  SP_TITULO_CAMPO 144, 'Modulador #2','WK_MOD_2'
  execute  SP_TITULO_CAMPO 145, 'Modulador #3','WK_MOD_3'

  execute  SP_TITULO_CONTEO  161, 'Criterio #1',  'CT_NIVEL_1'
  execute  SP_TITULO_CONTEO  162, 'Criterio #2',  'CT_NIVEL_2'
  execute  SP_TITULO_CONTEO  163, 'Criterio #3',  'CT_NIVEL_3'
  execute  SP_TITULO_CONTEO  164, 'Criterio #4',  'CT_NIVEL_4'
  execute  SP_TITULO_CONTEO  165, 'Criterio #5',  'CT_NIVEL_5'

end
go

/* Evita que se haga un UPDATE cuando el mes no tiene datos */
/* Patch 375 # Seq 176: modificado */
ALTER procedure DBO.IMSS_CALCULA_RECARGOS(
@PATRON CHAR( 1 ),
@YEAR SMALLINT,
@MES SMALLINT,
@TIPO SMALLINT,
@FACTOR NUMERIC( 15, 5 ),
@TASA NUMERIC( 15, 5 ) )
as
begin
     SET NOCOUNT ON
     declare @rTotImss Numeric( 15, 2 );
     declare @rTotRetiro Numeric( 15, 2 );
     declare @rTotInfo Numeric( 15, 2 );
     declare @rTotAmort Numeric( 15, 2 );
     declare @rTotApoVol Numeric( 15, 2 );
     declare @rContraFactor Numeric( 15, 9 );
     declare @rContraTasa Numeric( 15, 9 );
     declare @rActIMSS Numeric( 15, 2 );
     declare @rRecIMSS Numeric( 15, 2 );
     declare @rActRet Numeric( 15, 2 );
     declare @rRecRet Numeric( 15, 2 );
     declare @rActINFO Numeric( 15, 2 );
     declare @rRecINFO Numeric( 15, 2 );
     declare @Cuantos Integer;
     select @rTotImss = SUM( L.LE_TOT_IMS ),
            @rTotRetiro = SUM( L.LE_TOT_RET - L.LE_APO_VOL ),
            @rTotInfo = SUM( L.LE_INF_PAT ),
            @rTotAmort = SUM( L.LE_INF_AMO ),
            @rTotApoVol = SUM( L.LE_APO_VOL ),
            @Cuantos = COUNT(*)
     from DBO.LIQ_EMP as L where ( L.LS_PATRON = @PATRON ) and
                                 ( L.LS_YEAR = @YEAR ) and
                                 ( L.LS_MONTH = @MES ) and
                                 ( L.LS_TIPO = @TIPO );
     set @rContraFactor = ( @FACTOR - 1 );
     set @rContraTasa = @FACTOR * ( @TASA / 100 );
     if ( @Cuantos > 0 )
     begin
          set @rActIMSS = @rTotIMSS * @rContraFactor;
          set @rRecIMSS = @rTotIMSS * @rContraTasa;
          set @rActRET = @rTotRetiro * @rContraFactor;
          set @rRecRET = @rTotRetiro * @rContraTasa;
          set @rTotInfo = @rTotInfo + @rTotAmort;
          set @rActINFO = @rTotInfo * @rContraFactor;
          set @rRecINFO = @rTotInfo * @rContraTasa;
     end
     else
     begin
          set @rActIMSS = 0;
          set @rRecIMSS = 0;
          set @rActRET = 0;
          set @rRecRET = 0;
          set @rTotInfo = 0;
          set @rActINFO = 0;
          set @rRecINFO = 0;
     end
     set @rTotIMSS = @rActIMSS + @rRecIMSS;
     set @rTotRetiro = @rActRET + @rRecRET;
     set @rTotINFO = @rActINFO + @rRecINFO;
     update DBO.LIQ_IMSS set LS_ACT_IMS = @rActIMSS,
                             LS_REC_IMS = @rRecIMSS,
                             LS_ACT_RET = @rActRet,
                             LS_REC_RET = @rRecRet,
                             LS_ACT_INF = @rActINFO,
                             LS_REC_INF = @rRecINFO,
                             LS_ACT_AMO = @rTotAmort * @rContraFactor,
                             LS_REC_AMO = @rTotAmort * @rContraTasa,
                             LS_ACT_APO = @rTotApoVol * @rContraFactor,
                             LS_REC_APO = @rTotApoVol * @rContraTasa,
                             LS_FAC_ACT = @Factor,
                             LS_FAC_REC = @Tasa,
                             LS_TOT_IMS = LS_SUB_IMS + @rTotIMSS,
                             LS_TOT_RET = LS_SUB_RET + LS_APO_VOL + @rTotRetiro,
                             LS_TOT_INF = LS_SUB_INF + @rTotINFO,
                             LS_TOT_MES = LS_SUB_IMS + @rTotIMSS + LS_SUB_RET + LS_APO_VOL + @rTotRetiro +
                                          LS_SUB_INF + @rTotINFO
     where ( LS_PATRON = @PATRON ) and
           ( LS_YEAR = @YEAR ) and
           ( LS_MONTH = @MES ) and
           ( LS_TIPO = @TIPO );
end
GO

/* Considerar la fecha de baja del empleado */
/* Esto ya est� en \3Win_13\Datos\SQLServer\3DatosFunciones.sql */
/* Aparentemente no hay que cambiar dicho script */
/* Patch 375 # Seq 87: modificado */
ALTER FUNCTION DBO.SP_CAFE_AUTORIZA_COMIDA(
@EMPLEADO INTEGER,
@DIA DATETIME,
@HORA CHAR(4),
@CREDENCIAL CHAR(1) )
RETURNS @RESULTADO TABLE ( AUTORIZACION SMALLINT NOT NULL, FECHABAJA DATETIME NULL )
AS
BEGIN
     DECLARE @cActivo Char( 1 )
     DECLARE @cCredencial Char( 1 )
     DECLARE @iRegistros Integer
     DECLARE @ValorAuto Smallint
     DECLARE @dBaja DATETIME
     DECLARE @iStatus SmallInt
     select @cActivo = C.CB_ACTIVO,
            @dBaja = C.CB_FEC_BAJ,
            @cCredencial = C.CB_CREDENC,
            @iStatus = ( select DBO.SP_STATUS_ACT ( @DIA, C.CB_CODIGO ) )
     from DBO.COLABORA as C where ( C.CB_CODIGO = @EMPLEADO )
     if @cActivo Is Null
        set @ValorAuto = 1
     else
         if ( @iStatus <> 1 )
            set @ValorAuto = 2
         else
             if ( ( @CREDENCIAL <> ' ' ) and ( @cCredencial <> @CREDENCIAL ) )
                set @ValorAuto = 3
             else
             begin
                  select @iRegistros = COUNT(*)
                  from DBO.CAF_COME C where
                  ( C.CB_CODIGO = @EMPLEADO ) and
                  ( C.CF_FECHA = @DIA ) and
                  ( C.CF_HORA = @HORA )
                  if ( @iRegistros > 0 )
                     set @ValorAuto = 4
                  else
                      set @ValorAuto = 0
             end
     insert into @RESULTADO values( @ValorAuto, @dBaja )
     RETURN
end
GO

/* Eduardo: Hay que considerar  VACACION.VA_TIPO = 1 */
/* Patch 375 # Seq 75: modificado */
ALTER FUNCTION SP_STATUS_CONFLICTO
( @EMPLEADO INTEGER,
  @FECHAORIGINAL FECHA,
  @FECHAINICIAL FECHA,
  @FECHAFINAL FECHA,
  @ENTIDAD SMALLINT )
returns @VALORES table (
  RESULTADO INTEGER not null,
  INICIO DATETIME,
  FIN DATETIME )
as
begin
     declare @iClase Integer;
     declare @sActivo Char( 1 );
     declare @Valor Integer;
     declare @Inicial FECHA;
     declare @Final FECHA;
     set @Valor = 1;
     select @Inicial = C.CB_FEC_ING,
            @Final = C.CB_FEC_BAJ,
            @sActivo = C.CB_ACTIVO from DBO.COLABORA as C
     where ( C.CB_CODIGO = @EMPLEADO );
     if ( @FECHAINICIAL < @Inicial )
     begin
          set @Valor = 0;
     end
     else
     if ( ( @sActivo <> 'S' ) and ( @FECHAFINAL > @Final ) )
     begin
          set @Valor = 9;
     end
     else
     begin
          if ( @ENTIDAD = 1 )
          begin
               declare Incapacidades cursor for
                  select I.IN_FEC_INI, I.IN_FEC_FIN from DBO.INCAPACI as I where
                  ( I.CB_CODIGO = @EMPLEADO ) and
                  ( I.IN_FEC_INI <> @FECHAORIGINAL ) and
                  ( I.IN_FEC_INI <= @FECHAFINAL ) and
                  ( I.IN_FEC_FIN > @FECHAINICIAL );
          end
          else
          begin
               declare Incapacidades cursor for
                  select I.IN_FEC_INI, I.IN_FEC_FIN from DBO.INCAPACI as I where
                  ( I.CB_CODIGO = @EMPLEADO ) and
                  ( I.IN_FEC_INI <= @FECHAFINAL ) and
                  ( I.IN_FEC_FIN > @FECHAINICIAL );
          end
          open Incapacidades;
          fetch NEXT from Incapacidades into @Inicial, @Final;
          while @@Fetch_Status = 0
          begin
	  	set @Valor = 3;
               	break;
          end;          
	  close Incapacidades;
          deallocate Incapacidades;
          if ( @ENTIDAD = 2 )
          begin
               declare Vacaciones cursor for
                  select V.VA_FEC_INI, V.VA_FEC_FIN from DBO.VACACION as V where
                  ( V.CB_CODIGO = @EMPLEADO ) and
                  ( V.VA_TIPO = 1 ) and
                  ( V.VA_FEC_INI <> @FECHAORIGINAL ) and
                  ( V.VA_FEC_INI <= @FECHAFINAL ) and
                  ( V.VA_FEC_FIN > @FECHAINICIAL );
          end
          else
          begin
               declare Vacaciones cursor for
                  select V.VA_FEC_INI, V.VA_FEC_FIN from DBO.VACACION as V where
                  ( V.CB_CODIGO = @EMPLEADO ) and
                  ( V.VA_TIPO = 1 ) and
                  ( V.VA_FEC_INI <= @FECHAFINAL ) and
                  ( V.VA_FEC_FIN > @FECHAINICIAL );
          end;
          open Vacaciones;
          fetch NEXT from Vacaciones into @Inicial, @Final;
          while @@Fetch_Status = 0
          begin
	  	set @Valor = 2;
		break;
          end;
          close Vacaciones;
          deallocate Vacaciones;
          if ( @ENTIDAD = 3 )
          begin
               declare Permisos cursor for
                  select P.PM_CLASIFI, P.PM_FEC_INI, P.PM_FEC_FIN from DBO.PERMISO as P where
                  ( P.CB_CODIGO = @EMPLEADO ) and
                  ( P.PM_FEC_INI <> @FECHAORIGINAL ) and
                  ( P.PM_FEC_INI <= @FECHAFINAL ) and
                  ( P.PM_FEC_FIN > @FECHAINICIAL );
          end
          else
          begin
               declare Permisos cursor for
                  select P.PM_CLASIFI, P.PM_FEC_INI, P.PM_FEC_FIN from DBO.PERMISO as P where
                  ( P.CB_CODIGO = @EMPLEADO ) and
                  ( P.PM_FEC_INI <= @FECHAFINAL ) and
                  ( P.PM_FEC_FIN > @FECHAINICIAL );
          end;
          open Permisos;
          fetch NEXT from Permisos into @iClase, @Inicial, @Final;
          while @@Fetch_Status = 0
          begin
	  	set @Valor = @iClase + 4;
		break;
          end;
          close Permisos;
          deallocate Permisos;
     end;
     insert into @VALORES( RESULTADO, INICIO, FIN ) values ( @Valor, @Inicial, @Final );
     RETURN
end
GO

/* Eduardo: Se debe usar SP_STATUS_ACT para determinar si el empleado est� activo */
/* Patch 375 # Seq : modificado */
ALTER FUNCTION SP_LISTA_KARDEX (
  @Fecha DATETIME,
  @Usuario SMALLINT,
  @NumNivel SMALLINT
)
RETURNS @RegKardex TABLE
(      CB_CODIGO            Integer,
       CB_NIVEL             CHAR(6) )
AS
BEGIN
  declare @CB_CODIGO Integer;
  declare @CB_NIVEL  CHAR(6);
  declare @Matches Integer;
  declare @Matches2 Integer;
  declare @Nivel1 CHAR(6);
  declare @Nivel2 CHAR(6);
  declare @Nivel3 CHAR(6);
  declare @Nivel4 CHAR(6);
  declare @Nivel5 CHAR(6);
  declare @Nivel6 CHAR(6);
  declare @Nivel7 CHAR(6);
  declare @Nivel8 CHAR(6);
  declare @Nivel9 CHAR(6);
  declare @FechaNivel DATETIME;
  declare Temporal CURSOR for
   select CB_CODIGO, CB_FEC_NIV, CB_NIVEL1, CB_NIVEL2, CB_NIVEL3, CB_NIVEL4, CB_NIVEL5, CB_NIVEL6, CB_NIVEL7, CB_NIVEL8, CB_NIVEL9
   from COLABORA
   where ( ( select dbo.SP_STATUS_ACT( @Fecha, COLABORA.CB_CODIGO ) ) = 1 )

	open Temporal

	fetch NEXT FROM Temporal INTO
	@CB_CODIGO, @FechaNivel, @Nivel1, @Nivel2, @Nivel3, @Nivel4, @Nivel5, @Nivel6, @Nivel7, @Nivel8, @Nivel9

	while @@Fetch_Status = 0
	BEGIN
		if ( @Fecha < @FechaNivel )
			select @CB_NIVEL = dbo.SP_FECHA_NIVEL( @Fecha, @CB_CODIGO )
     		else if ( @NumNivel = 1 ) select @CB_NIVEL = @Nivel1
		else if ( @NumNivel = 2 ) select @CB_NIVEL = @Nivel2
     		else if ( @NumNivel = 3 ) select @CB_NIVEL = @Nivel3
     		else if ( @NumNivel = 4 ) select @CB_NIVEL = @Nivel4
     		else if ( @NumNivel = 5 ) select @CB_NIVEL = @Nivel5
     		else if ( @NumNivel = 6 ) select @CB_NIVEL = @Nivel6
     		else if ( @NumNivel = 7 ) select @CB_NIVEL = @Nivel7
     		else if ( @NumNivel = 8 ) select @CB_NIVEL = @Nivel8
     		else select @CB_NIVEL = @Nivel9

		if ( @CB_NIVEL <> '' )
		BEGIN
			select @Matches = ( select COUNT(*) from SUPER where ( US_CODIGO = @USUARIO ) and ( CB_NIVEL = @CB_NIVEL ))
       			if ( @Matches > 0 )
			BEGIN
           			select @Matches2 = ( select COUNT(*) from ASIGNA where ( AS_FECHA = @FECHA ) and ( CB_CODIGO = @CB_CODIGO ))
				if ( @Matches2 = 0 )
				BEGIN
					INSERT INTO @RegKardex
					VALUES ( @CB_CODIGO, @CB_NIVEL )
				END
			END
		END

		fetch NEXT FROM Temporal INTO
		@CB_CODIGO, @FechaNivel, @Nivel1, @Nivel2, @Nivel3, @Nivel4, @Nivel5, @Nivel6, @Nivel7, @Nivel8, @Nivel9
	END

	close Temporal
	deallocate Temporal
	
	RETURN
END
GO

/* Eduardo: Se debe usar SP_STATUS_ACT para determinar si el empleado est� activo */
/* Patch 375 # Seq : modificado */
ALTER FUNCTION SP_LISTA_EMPLEADOS (
  @FECHA DATETIME,
  @USUARIO SMALLINT
) RETURNS @RegLista TABLE (
  CB_CODIGO INTEGER,
  CB_NIVEL CHAR(6),
  CB_TIPO INTEGER )
AS
BEGIN
  declare @NumNivel SMALLINT;

  	/* Nivel de Organigrama de Supervisores */
	select @NumNivel = ( select CAST( GL_FORMULA AS SMALLINT )
  			from GLOBAL
  			where GL_CODIGO = 120 )

	insert into @RegLista
  	select CB_CODIGO, CB_NIVEL, 1
  	from   SP_LISTA_KARDEX( @FECHA, @USUARIO, @NumNivel )

  	insert into @RegLista
  	select CB_CODIGO, CB_NIVEL, 2
  	from   ASIGNA
  	where  AS_FECHA = @FECHA and
         ( ( select count(*) from SUPER where US_CODIGO = @USUARIO and CB_NIVEL = ASIGNA.CB_NIVEL ) > 0 ) and
         ( ( select DBO.SP_STATUS_ACT( @FECHA, ASIGNA.CB_CODIGO ) ) = 1 )

	RETURN
END
GO

