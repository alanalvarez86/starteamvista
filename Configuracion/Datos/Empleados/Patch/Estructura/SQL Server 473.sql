
/* Nuevo campo en tabla PERIODO: 1/3 */
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id=object_id('PERIODO') AND name='PE_MES_FON'    )
     alter table PERIODO add PE_MES_FON Mes NULL 
go    
/* Nuevo campo en tabla PERIODO: 2/3 */
update PERIODO set PE_MES_FON = case  when PE_USO = 0 then PE_MES else 0 end where PE_MES_FON is null 
go 
/* Nuevo campo en tabla PERIODO: 3/3 */
alter table PERIODO alter column PE_MES_FON Mes NOT NULL 
go

/* Patch 470: #16471 */
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id=object_id('CONCEPTO') AND name='CO_PS_TIPO'    )
    alter table CONCEPTO add CO_PS_TIPO Status NULL
go

IF EXISTS (SELECT * FROM syscolumns WHERE id=object_id('CONCEPTO') AND name='CO_PS_TIPO'    )
	update CONCEPTO set CO_PS_TIPO = 
			case 
				when CO_G_ISPT  = 'S' then 2
				else 0 
			end
	 where CO_PS_TIPO is NULL
go

IF EXISTS (SELECT * FROM syscolumns WHERE id=object_id('CONCEPTO') AND name='CO_PS_TIPO'    )
    alter table CONCEPTO alter column CO_PS_TIPO Status not null
go

/**Limpieza de procedures que ya no se ocupan para Prevision Social */
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'Conceptos_PrevisionSocial_CalcularPrioridad') AND type IN ( N'P', N'PC' ))
    drop procedure Conceptos_PrevisionSocial_CalcularPrioridad
go

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'Conceptos_PrevisionSocial_BajaPrioridad') AND type IN ( N'P', N'PC' ))
    drop procedure Conceptos_PrevisionSocial_BajaPrioridad
go

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'Conceptos_PrevisionSocial_SubePrioridad') AND type IN ( N'P', N'PC' ))
    drop procedure Conceptos_PrevisionSocial_SubePrioridad
go

/* #16874: El usuario necesita administrar en el cat�logo de conceptos si Aplica para Suma de Salarios o es de Previsi�n Social. 1/1 */
DELETE FROM R_ATRIBUTO where en_codigo = (SELECT EN_CODIGO FROM R_ENTIDAD WHERE EN_TABLA = 'CONCEPTO') and at_campo = 'CO_PS_PRIO'

go 

IF EXISTS (SELECT * FROM syscolumns WHERE id=object_id('CONCEPTO') AND name='CO_PS_PRIO')    
begin 	 	 
	EXEC sp_unbindefault 'CONCEPTO.CO_PS_PRIO'
	alter table CONCEPTO  drop column CO_PS_PRIO       
end 

go 

/* Agregar Conceptos de Acumuados de Prevision Social (1/3)*/
/* Patch 473 */
create procedure AGREGA_CONCEPTOS_SISTEMA
as
begin
     SET NOCOUNT ON;
     declare @Cuantos Integer;
     select @Cuantos = COUNT(*) from CONCEPTO where( CO_NUMERO = 1018 );
     if ( @Cuantos = 0 )
     begin
          insert into CONCEPTO( CO_NUMERO, CO_DESCRIP, CO_ACTIVO ) values ( 1018, 'Total de Prevision Social', 'S' );
     end
     select @Cuantos = COUNT(*) from CONCEPTO where( CO_NUMERO = 1019 );
     if ( @Cuantos = 0 )
     begin
          insert into CONCEPTO( CO_NUMERO, CO_DESCRIP, CO_ACTIVO ) values ( 1019,  'Total de Salario (P.Social)', 'S' );
     end   
end
GO

/*  Agregar Conceptos de Acumuados de Prevision Social (2/3)*/
/* Patch 473 #Seq: X*/
execute AGREGA_CONCEPTOS_SISTEMA
GO

/* Agregar Conceptos de Acumuados de Prevision Social (3/3)*/
/* Patch 473 #Seq: X*/
drop procedure AGREGA_CONCEPTOS_SISTEMA;

GO 
/* Funci�n que toma el conteo de elementos de una Cedula   */
if exists (select * from sysobjects where id = object_id(N'FN_FONACOT_GET_CONTEO_CEDULA') and xtype in (N'FN', N'IF', N'TF')) 
	drop function FN_FONACOT_GET_CONTEO_CEDULA
go

create function FN_FONACOT_GET_CONTEO_CEDULA(  @PF_YEAR Anio, @PF_MES Mes, @RS_SOCIAL Codigo )
returns int 
as
begin 
	declare @Conteo int 
	select  @Conteo = count(*) from PCED_FON PF
		left outer join COLABORA CB on PF.CB_CODIGO = CB.CB_CODIGO 
		left outer join RPATRON RP on CB.CB_PATRON = RP.TB_CODIGO 
		where PF_YEAR = @PF_YEAR and PF_MES = @PF_MES  and ( RP.RS_CODIGO = @RS_SOCIAL or @RS_SOCIAL = '' ) 

	return @Conteo 
end 
GO 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'AFECTA_EMPLEADO_PS_PREVISION_SOCIAL') AND type IN ( N'P', N'PC' ))
	drop procedure AFECTA_EMPLEADO_PS_PREVISION_SOCIAL
GO 

create PROCEDURE AFECTA_EMPLEADO_PS_PREVISION_SOCIAL( @EMPLEADO INTEGER,
    			@ANIO SMALLINT,
    			@TIPO SMALLINT,
    			@NUMERO SMALLINT,
    			@MES SMALLINT,
    			@FACTOR INTEGER, 
				@RS_CODIGO Codigo)
as
begin 
    DECLARE @M1018 NUMERIC(15,2); 
	
	select @M1018 = sum( M.MO_PERCEPC + M.MO_DEDUCCI )
    from MOVIMIEN M left outer join CONCEPTO C on C.CO_NUMERO=M.CO_NUMERO
    where
               ( M.MO_ACTIVO = 'S' ) and
               ( M.PE_YEAR = @Anio ) and
               ( M.PE_TIPO = @Tipo ) and
               ( M.PE_NUMERO = @Numero ) and
               ( M.CB_CODIGO = @Empleado ) and
               ( C.CO_TIPO in ( 1, 4)  ) and
               ( C.CO_PS_TIPO = 1 )

     if ( @M1018 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1018, @Mes, @Factor, @M1018, @RS_CODIGO ;

end 
GO 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'AFECTA_EMPLEADO_PS_SALARIOS') AND type IN ( N'P', N'PC' ))
	drop procedure AFECTA_EMPLEADO_PS_SALARIOS
GO 

create PROCEDURE AFECTA_EMPLEADO_PS_SALARIOS( @EMPLEADO INTEGER,
    			@ANIO SMALLINT,
    			@TIPO SMALLINT,
    			@NUMERO SMALLINT,
    			@MES SMALLINT,
    			@FACTOR INTEGER, 
				@RS_CODIGO Codigo)
as
begin 
    DECLARE @M1019 NUMERIC(15,2); 
	
	select @M1019 = sum( M.MO_PERCEPC + M.MO_DEDUCCI )
    from MOVIMIEN M left outer join CONCEPTO C on C.CO_NUMERO=M.CO_NUMERO
    where
               ( M.MO_ACTIVO = 'S' ) and
               ( M.PE_YEAR = @Anio ) and
               ( M.PE_TIPO = @Tipo ) and
               ( M.PE_NUMERO = @Numero ) and
               ( M.CB_CODIGO = @Empleado ) and
               ( C.CO_TIPO in ( 1, 4)  ) and
               ( C.CO_PS_TIPO = 2 )

     if ( @M1019 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1019, @Mes, @Factor, @M1019, @RS_CODIGO;

end 
GO 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'AFECTA_EMPLEADO') AND type IN ( N'P', N'PC' ))
	drop procedure AFECTA_EMPLEADO
GO

CREATE  PROCEDURE AFECTA_EMPLEADO
    				@EMPLEADO INTEGER,
    				@ANIO SMALLINT,
    				@TIPO SMALLINT,
    				@NUMERO SMALLINT,
    				@MES SMALLINT,
    				@FACTOR INTEGER
AS
BEGIN
     SET NOCOUNT ON

	 DECLARE @RS_CODIGO Codigo 

	 select @RS_CODIGO = RSOCIAL.RS_CODIGO from NOMINA 
	 join RPATRON on NOMINA.CB_PATRON = RPATRON.TB_CODIGO 
	 join RSOCIAL on RSOCIAL.RS_CODIGO  = RPATRON.RS_CODIGO 
	 where NOMINA.PE_YEAR = @ANIO and NOMINA.PE_TIPO = @TIPO and NOMINA.PE_NUMERO = @NUMERO and NOMINA.CB_CODIGO = @EMPLEADO 

	 set @RS_CODIGO = coalesce( @RS_CODIGO, '' ) 

     DECLARE @CONCEPTO INTEGER;
     DECLARE @M1000 NUMERIC(15,2);
     DECLARE @M1001 NUMERIC(15,2);
     DECLARE @M1002 NUMERIC(15,2);
     DECLARE @M1003 NUMERIC(15,2);
     DECLARE @M1004 NUMERIC(15,2);
     DECLARE @M1005 NUMERIC(15,2);
     DECLARE @M1006 NUMERIC(15,2);
     DECLARE @M1007 NUMERIC(15,2);
     DECLARE @M1008 NUMERIC(15,2);
     DECLARE @M1009 NUMERIC(15,2);
     DECLARE @M1010 NUMERIC(15,2);
     DECLARE @M1011 NUMERIC(15,2);
     DECLARE @M1012 NUMERIC(15,2);
     DECLARE @M1013 NUMERIC(15,2);
     DECLARE @M1014 NUMERIC(15,2);
     DECLARE @M1015 NUMERIC(15,2);
     DECLARE @M1016 NUMERIC(15,2);
     DECLARE @M1017 NUMERIC(15,2);
     DECLARE @M1100 NUMERIC(15,2);
     DECLARE @M1101 NUMERIC(15,2);
     DECLARE @M1102 NUMERIC(15,2);
     DECLARE @M1103 NUMERIC(15,2);
     DECLARE @M1104 NUMERIC(15,2);
     DECLARE @M1105 NUMERIC(15,2);
     DECLARE @M1106 NUMERIC(15,2);
     DECLARE @M1107 NUMERIC(15,2);
     DECLARE @M1108 NUMERIC(15,2);
     DECLARE @M1109 NUMERIC(15,2);
     DECLARE @M1110 NUMERIC(15,2);
     DECLARE @M1111 NUMERIC(15,2);
     DECLARE @M1112 NUMERIC(15,2);
     DECLARE @M1113 NUMERIC(15,2);
     DECLARE @M1114 NUMERIC(15,2);
     DECLARE @M1115 NUMERIC(15,2);
     DECLARE @M1116 NUMERIC(15,2);
     DECLARE @M1117 NUMERIC(15,2);
     DECLARE @M1118 NUMERIC(15,2);
     DECLARE @M1119 NUMERIC(15,2);
     DECLARE @M1120 NUMERIC(15,2);
     DECLARE @M1121 NUMERIC(15,2);
     DECLARE @M1122 NUMERIC(15,2);
     DECLARE @M1123 NUMERIC(15,2);
     DECLARE @M1124 NUMERIC(15,2);
     DECLARE @M1125 NUMERIC(15,2);
     DECLARE @M1126 NUMERIC(15,2);
     DECLARE @M1127 NUMERIC(15,2);
     DECLARE @M1128 NUMERIC(15,2);
     DECLARE @M1129 NUMERIC(15,2);
     DECLARE @M1200 NUMERIC(15,2);
     DECLARE @M1201 NUMERIC(15,2);
     DECLARE @M1202 NUMERIC(15,2);
     DECLARE @M1203 NUMERIC(15,2);
     DECLARE @M1204 NUMERIC(15,2);
     DECLARE @M1205 NUMERIC(15,2);
     DECLARE @M1206 NUMERIC(15,2);
     DECLARE @M1207 NUMERIC(15,2);
     DECLARE @M1208 NUMERIC(15,2);
     DECLARE @M1209 NUMERIC(15,2);
     DECLARE @M1210 NUMERIC(15,2);
     DECLARE @M1211 NUMERIC(15,2);
     DECLARE @M1212 NUMERIC(15,2);
     DECLARE @M1213 NUMERIC(15,2);
     DECLARE @M1214 NUMERIC(15,2);
     DECLARE @M1215 NUMERIC(15,2);
     DECLARE @M1216 NUMERIC(15,2);
	 DECLARE @TP_Clasif SMALLINT;

     select @M1000=N.NO_PERCEPC,
            @M1001=N.NO_DEDUCCI,
            @M1002=N.NO_NETO,
            @M1003=N.NO_X_ISPT,
            @M1005=N.NO_PER_MEN,
            @M1006=N.NO_X_MENS,
            @M1007=N.NO_PER_CAL,
            @M1008=N.NO_IMP_CAL,
            @M1009=N.NO_X_CAL,
            @M1010=N.NO_TOT_PRE,
            @M1011=N.NO_PREV_GR,
            @M1012=N.NO_PER_ISN,
            @M1013=N.NO_TOTAL_1,
            @M1014=N.NO_TOTAL_2,
            @M1015=N.NO_TOTAL_3,
            @M1016=N.NO_TOTAL_4,
            @M1017=N.NO_TOTAL_5,
            @M1100=N.NO_DIAS_FI,
            @M1101=N.NO_DIAS_FJ,
            @M1102=N.NO_DIAS_CG,
            @M1103=N.NO_DIAS_SG,
            @M1104=N.NO_DIAS_SU,
            @M1105=N.NO_DIAS_OT,
            @M1106=N.NO_DIAS_IN,
            @M1107=N.NO_DIAS * 7 / 6,
            @M1108=N.NO_DIAS_RE,
            @M1109=N.NO_D_TURNO,
            @M1110=N.NO_DIAS,
            @M1111=N.NO_DIAS_AS,
            @M1112=N.NO_DIAS_NT,
            @M1113=N.NO_DIAS_FV,
            @M1114=N.NO_DIAS_VA,
            @M1115=N.NO_DIAS_AG,
            @M1116=N.NO_DIAS_AJ,
            @M1117=N.NO_DIAS_SS,
            @M1118=N.NO_DIAS_EM,
            @M1119=N.NO_DIAS_SI,
            @M1120=N.NO_DIAS_BA,
            @M1121=N.NO_DIAS_PV,
            @M1122=N.NO_DIAS_VJ,
            @M1123=N.NO_DIAS_VJ * 7 / 6,
            @M1124=N.NO_DIAS_IH,
            @M1125=N.NO_DIAS_ID,
            @M1126=N.NO_DIAS_IT,
            @M1127=N.NO_DIAS_DT,
            @M1128=N.NO_DIAS_FS,
            @M1129=N.NO_DIAS_FT,
            @M1200=N.NO_JORNADA,
            @M1201=N.NO_HORAS,
            @M1202=N.NO_EXTRAS,
            @M1203=N.NO_DOBLES,
            @M1204=N.NO_TRIPLES,
            @M1205=N.NO_ADICION,
            @M1206=N.NO_TARDES,
            @M1207=N.NO_HORA_PD,
            @M1208=N.NO_HORA_CG,
            @M1209=N.NO_HORA_SG,
            @M1210=N.NO_FES_TRA,
            @M1211=N.NO_DES_TRA,
            @M1212=N.NO_VAC_TRA,
            @M1213=N.NO_FES_PAG,
            @M1214=N.NO_HORASNT,
            @M1215=N.NO_HORAPDT,
            @M1216=N.NO_PRE_EXT
            from NOMINA N where
            ( N.PE_YEAR = @Anio ) and
            ( N.PE_TIPO = @Tipo ) and
            ( N.PE_NUMERO = @Numero ) and
            ( N.CB_CODIGO = @Empleado )

     if ( @M1200 <> 0 )
		Select @M1200 = @M1201 / @M1200;
     Select @M1109 = @M1200 * @M1109;

     if ( @M1201 < 0 ) and ( @M1200 > 0 )
		set @M1200 = -@M1200;

     if ( @M1000 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1000, @Mes, @Factor, @M1000, @RS_CODIGO;
     if ( @M1001 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1001, @Mes, @Factor, @M1001, @RS_CODIGO;
     if ( @M1002 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1002, @Mes, @Factor, @M1002, @RS_CODIGO;
     if ( @M1003 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1003, @Mes, @Factor, @M1003, @RS_CODIGO;
     if ( @M1005 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1005, @Mes, @Factor, @M1005, @RS_CODIGO;
     if ( @M1006 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1006, @Mes, @Factor, @M1006, @RS_CODIGO;
     if ( @M1007 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1007, @Mes, @Factor, @M1007, @RS_CODIGO;
     if ( @M1008 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1008, @Mes, @Factor, @M1008, @RS_CODIGO;
     if ( @M1009 <> 0 )
       EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1009, @Mes, @Factor, @M1009, @RS_CODIGO;
     if ( @M1010 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1010, @Mes, @Factor, @M1010, @RS_CODIGO;
     if ( @M1011 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1011, @Mes, @Factor, @M1011, @RS_CODIGO;
  	 if ( @M1012 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1012, @Mes, @Factor, @M1012, @RS_CODIGO;
  	 if ( @M1013 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1013, @Mes, @Factor, @M1013, @RS_CODIGO;
  	 if ( @M1014 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1014, @Mes, @Factor, @M1014, @RS_CODIGO;
  	 if ( @M1015 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1015, @Mes, @Factor, @M1015, @RS_CODIGO;
  	 if ( @M1016 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1016, @Mes, @Factor, @M1016, @RS_CODIGO;
  	 if ( @M1017 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1017, @Mes, @Factor, @M1017, @RS_CODIGO;
     if ( @M1100 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1100, @Mes, @Factor, @M1100, @RS_CODIGO;
     if ( @M1101 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1101, @Mes, @Factor, @M1101, @RS_CODIGO;
     if ( @M1102 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1102, @Mes, @Factor, @M1102, @RS_CODIGO;
     if ( @M1103 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1103, @Mes, @Factor, @M1103, @RS_CODIGO;
     if ( @M1104 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1104, @Mes, @Factor, @M1104, @RS_CODIGO;
     if ( @M1105 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1105, @Mes, @Factor, @M1105, @RS_CODIGO;
     if ( @M1106 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1106, @Mes, @Factor, @M1106, @RS_CODIGO;
     if ( @M1107 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1107, @Mes, @Factor, @M1107, @RS_CODIGO;
     if ( @M1108 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1108, @Mes, @Factor, @M1108, @RS_CODIGO;
     if ( @M1109 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1109, @Mes, @Factor, @M1109, @RS_CODIGO;
     if ( @M1110 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1110, @Mes, @Factor, @M1110, @RS_CODIGO;
     if ( @M1111 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1111, @Mes, @Factor, @M1111, @RS_CODIGO;
     if ( @M1112 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1112, @Mes, @Factor, @M1112, @RS_CODIGO;
     if ( @M1113 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1113, @Mes, @Factor, @M1113, @RS_CODIGO;
     if ( @M1114 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1114, @Mes, @Factor, @M1114, @RS_CODIGO;
     if ( @M1115 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1115, @Mes, @Factor, @M1115, @RS_CODIGO;
     if ( @M1116 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1116, @Mes, @Factor, @M1116, @RS_CODIGO;
     if ( @M1117 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1117, @Mes, @Factor, @M1117, @RS_CODIGO;
     if ( @M1118 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1118, @Mes, @Factor, @M1118, @RS_CODIGO;
     if ( @M1119 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1119, @Mes, @Factor, @M1119, @RS_CODIGO;
     if ( @M1120 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1120, @Mes, @Factor, @M1120, @RS_CODIGO;
     if ( @M1121 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1121, @Mes, @Factor, @M1121, @RS_CODIGO;
     if ( @M1122 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1122, @Mes, @Factor, @M1122, @RS_CODIGO;
     if ( @M1123 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1123, @Mes, @Factor, @M1123, @RS_CODIGO;
     if ( @M1124 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1124, @Mes, @Factor, @M1124, @RS_CODIGO;
     if ( @M1125 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1125, @Mes, @Factor, @M1125, @RS_CODIGO;
     if ( @M1126 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1126, @Mes, @Factor, @M1126, @RS_CODIGO;
     if ( @M1127 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1127, @Mes, @Factor, @M1127, @RS_CODIGO;
     if ( @M1128 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1128, @Mes, @Factor, @M1128, @RS_CODIGO;
     if ( @M1129 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1129, @Mes, @Factor, @M1129, @RS_CODIGO;
     if ( @M1200 <> 0 )
     begin
          select @TP_Clasif = TP_CLAS from TPERIODO where TP_TIPO = @Tipo;

          if ( @TP_Clasif = 0 )
             Select @M1200 = 8 * @M1200;
          else
          if ( @TP_Clasif = 1 )
             Select @M1200 = 48 * @M1200;
          else
          if ( @TP_Clasif = 2 )
             Select @M1200 = 96 * @M1200;
          else
          if ( @TP_Clasif = 3 )
             Select @M1200 = 104 * @M1200;
          else
          if ( @TP_Clasif = 4 )
             Select @M1200 = 208 * @M1200;
          else
          if ( @TP_Clasif = 5 )
             Select @M1200 = 72 * @M1200;
          EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1200, @Mes, @Factor, @M1200, @RS_CODIGO;

     end
     if ( @M1201 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1201, @Mes, @Factor, @M1201, @RS_CODIGO;
     if ( @M1202 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1202, @Mes, @Factor, @M1202, @RS_CODIGO;
     if ( @M1203 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1203, @Mes, @Factor, @M1203, @RS_CODIGO;
     if ( @M1204 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1204, @Mes, @Factor, @M1204, @RS_CODIGO;
     if ( @M1205 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1205, @Mes, @Factor, @M1205, @RS_CODIGO;
     if ( @M1206 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1206, @Mes, @Factor, @M1206, @RS_CODIGO;
     if ( @M1207 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1207, @Mes, @Factor, @M1207, @RS_CODIGO;
     if ( @M1208 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1208, @Mes, @Factor, @M1208, @RS_CODIGO;
     if ( @M1209 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1209, @Mes, @Factor, @M1209, @RS_CODIGO;
     if ( @M1210 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1210, @Mes, @Factor, @M1210, @RS_CODIGO;
     if ( @M1211 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1211, @Mes, @Factor, @M1211, @RS_CODIGO;
     if ( @M1212 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1212, @Mes, @Factor, @M1212, @RS_CODIGO;
     if ( @M1213 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1213, @Mes, @Factor, @M1213, @RS_CODIGO;
     if ( @M1214 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1214, @Mes, @Factor, @M1214, @RS_CODIGO;
     if ( @M1215 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1215, @Mes, @Factor, @M1215, @RS_CODIGO;
	 if ( @M1216 <> 0 )
	    EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1216, @Mes, @Factor, @M1216, @RS_CODIGO;

     Declare TemporalMovimien Cursor for
        select M.CO_NUMERO, SUM( M.MO_PERCEPC + M.MO_DEDUCCI )
        from MOVIMIEN M where
               ( M.MO_ACTIVO = 'S' ) and
               ( M.PE_YEAR = @Anio ) and
               ( M.PE_TIPO = @Tipo ) and
               ( M.PE_NUMERO = @Numero ) and
               ( M.CB_CODIGO = @Empleado )
        group by M.CO_NUMERO
     open TemporalMovimien
     Fetch next	from TemporalMovimien
     into  @Concepto, @M1000
     WHILE @@FETCH_STATUS = 0
     begin
          EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, @Concepto, @Mes, @Factor, @M1000, @RS_CODIGO;
     	  Fetch next from TemporalMovimien
     	  into  @Concepto, @M1000
     end

     CLOSE TemporalMovimien
     DEALLOCATE TemporalMovimien


     select @M1004 = sum( M.MO_PERCEPC )
     from MOVIMIEN M left outer join CONCEPTO C on C.CO_NUMERO=M.CO_NUMERO
     where
               ( M.MO_ACTIVO = 'S' ) and
               ( M.PE_YEAR = @Anio ) and
               ( M.PE_TIPO = @Tipo ) and
               ( M.PE_NUMERO = @Numero ) and
               ( M.CB_CODIGO = @Empleado ) and
               ( C.CO_TIPO = 1 ) and
               ( C.CO_A_PTU = 'S' )

     if ( @M1004 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1004, @Mes, @Factor, @M1004,  @RS_CODIGO;


	EXECUTE AFECTA_EMPLEADO_PS_PREVISION_SOCIAL @Empleado, @Anio, @Tipo, @Numero, @Mes, @Factor, @RS_CODIGO
	EXECUTE AFECTA_EMPLEADO_PS_SALARIOS			@Empleado, @Anio, @Tipo, @Numero, @Mes, @Factor, @RS_CODIGO 

END
GO 

/* #17022: Funci�n CALC_FON y Vista V_CED_FON. Modificar funci�n y vista para considerar los cambios a la tabla PERIODO. Tomar el mes al cual se le asignar� el descuento Fonacot: 1/2 */
if exists (select * from dbo.sysobjects where id = object_id(N'FN_FONACOT_PERIODOS') and xtype in (N'FN', N'IF', N'TF')) 
	DROP FUNCTION FN_FONACOT_PERIODOS
GO

/* #17022: Funci�n CALC_FON y Vista V_CED_FON. Modificar funci�n y vista para considerar los cambios a la tabla PERIODO. Tomar el mes al cual se le asignar� el descuento Fonacot: 1/2 */
CREATE FUNCTION FN_FONACOT_PERIODOS 
	(@AnioPeriodo Anio, @MesPeriodo Mes, @TipoPeriodo NominaTipo)
RETURNS @Valores TABLE
	(
	   PE_NUMERO Smallint,
	   PE_POS_FON Smallint
	)
AS
BEGIN
	DECLARE @PE_POS_FON Dias

	insert into @Valores (PE_NUMERO, PE_POS_FON)
	SELECT PE_NUMERO, ROW_NUMBER () OVER (ORDER BY PE_NUMERO ASC)
	FROM PERIODO WHERE PE_YEAR = @AnioPeriodo AND PE_MES_FON = @MesPeriodo  AND PE_TIPO = @TipoPeriodo
	GROUP BY PE_NUMERO
	ORDER BY PE_NUMERO ASC
	
	RETURN 
END
GO


/* #16904: Realizar el ajuste suavizado en la funcion CALC_FON para asi no deducir cantidades fuertes en los periodos contiguos: 1/2 */
/* #17022: Funci�n CALC_FON y Vista V_CED_FON. Modificar funci�n y vista para considerar los cambios a la tabla PERIODO. Tomar el mes al cual se le asignar� el descuento Fonacot: 1/2 */
if exists (select * from dbo.sysobjects where id = object_id(N'FN_CALCULA_FON') and xtype in (N'FN', N'IF', N'TF')) 
       drop function FN_CALCULA_FON
go 

/* #16904: Realizar el ajuste suavizado en la funcion CALC_FON para asi no deducir cantidades fuertes en los periodos contiguos: 2/2 */
/* #17022: Funci�n CALC_FON y Vista V_CED_FON. Modificar funci�n y vista para considerar los cambios a la tabla PERIODO. Tomar el mes al cual se le asignar� el descuento Fonacot: 2/2 */
create function FN_CALCULA_FON 
	(@CB_CODIGO NumeroEmpleado, @PR_REFEREN Referencia, @PR_TIPO Codigo1, @TipoAjuste NumeroEmpleado, @AjusteMesAnterior Booleano,
	@AnioPeriodo Anio, @MesPeriodoFon Mes, @NumPeriodo NominaNumero, @TipoPeriodo NominaTipo)
RETURNS @Valores TABLE
	(
	   RETENCION_PERIODO Decimal(15,2), 
	   MONTO_CEDULA   Decimal(15,2), 
	   MONTO_RETENIDO Decimal(15,2), 
       MONTO_ESPERADO Decimal(15,2), 
       AJUSTE Decimal(15,2), 
       MONTO_CONCEPTO Decimal(15,2) ,
       DEUDA_MA Decimal(15,2) ,
	   NUM_PERIODO_MES int,
	   POS_PERIODO_MES int,
	   MES_ANTERIOR    int, 
	   YEAR_ANTERIOR   int 
	)
as
begin 
		declare @RETENCION_PERIODO Pesos 
		declare @MONTO_CEDULA   Pesos 
		declare @MONTO_RETENIDO Pesos 
		declare @MONTO_ESPERADO Pesos 
		declare @AJUSTE Pesos 
		declare @MONTO_CONCEPTO Pesos 
		declare @DEUDA_MA Pesos 
		DECLARE @NUM_PERIODO_MES Empleados
		DECLARE @POS_PERIODO_MES Empleados

		DECLARE @MES_ANTERIOR Status
		DECLARE @YEAR_ANTERIOR Anio
	   	   
		SET @AJUSTE = 0;		

		-- OBTENER N�MERO DE PERIODO Y LA CANTIDAD DE PERIODOS DEL MES.	   		
		SELECT @POS_PERIODO_MES = PE_POS_FON FROM FN_FONACOT_PERIODOS (@AnioPeriodo, @MesPeriodoFon, @TipoPeriodo)
		WHERE PE_NUMERO = @NumPeriodo
		-- Cantidad de periodos Fonacot.
		-- Considerar cambio de a�o.
		IF (@MesPeriodoFon <> 13) AND (@MesPeriodoFon <> 1)
		BEGIN
			SELECT @NUM_PERIODO_MES = COUNT(PE_NUMERO) FROM PERIODO
			WHERE PE_YEAR = @AnioPeriodo AND PE_MES_FON = @MesPeriodoFon AND PE_TIPO = @TipoPeriodo
		END
		ELSE IF @MesPeriodoFon = 13
		BEGIN
			SELECT  @NUM_PERIODO_MES = 
				(SELECT COUNT(PE_NUMERO) CANTIDAD FROM PERIODO
				WHERE PE_YEAR = @AnioPeriodo AND PE_MES_FON = @MesPeriodoFon AND PE_TIPO = @TipoPeriodo)
				+
				(SELECT COUNT(PE_NUMERO) CANTIDAD FROM PERIODO
				WHERE PE_YEAR = (@AnioPeriodo+1) AND PE_MES_FON = 1 AND PE_TIPO = @TipoPeriodo) 
		END
		ELSE IF @MesPeriodoFon = 1
		BEGIN
			SELECT  @NUM_PERIODO_MES = 
				(SELECT COUNT(PE_NUMERO) CANTIDAD FROM PERIODO
				WHERE PE_YEAR = @AnioPeriodo AND PE_MES_FON = @MesPeriodoFon AND PE_TIPO = @TipoPeriodo)
				+
				(SELECT COUNT(PE_NUMERO) CANTIDAD FROM PERIODO
				WHERE PE_YEAR = (@AnioPeriodo-1) AND PE_MES_FON = 13 AND PE_TIPO = @TipoPeriodo) 
		END

		IF @MesPeriodoFon <> 13
		BEGIN
			SELECT @MONTO_CEDULA = PF_PAGO,  @MONTO_RETENIDO = PF_NOMINA FROM VCED_FON
			WHERE CB_CODIGO = @CB_CODIGO and PF_YEAR = @AnioPeriodo AND PF_MES = @MesPeriodoFon AND PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN
		END
		ELSE
		BEGIN
			SELECT @MONTO_CEDULA = PF_PAGO,  @MONTO_RETENIDO = PF_NOMINA FROM VCED_FON
			WHERE CB_CODIGO = @CB_CODIGO and PF_YEAR = (@AnioPeriodo+1) AND PF_MES = 1 AND PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN
		END
		set @MONTO_RETENIDO = coalesce( @MONTO_RETENIDO, 0.00 ) 


		if (@MesPeriodoFon = 1 ) 
		begin 
			set @MES_ANTERIOR = 12 
			set @YEAR_ANTERIOR = @AnioPeriodo - 1; 
		end
		else
		begin 
			set @MES_ANTERIOR = @MesPeriodoFon - 1 ; 
			set @YEAR_ANTERIOR = @AnioPeriodo; 
		end
				
		-- SI EL TIPO DE AJUSTE ES  1 (DURANTE EL MES, ES DECIR, SUAVIZADO) � 2 (AJUSTAR CADA PERIODO) Y SE PIDE AJUSTAR EL MES ANTERIOR,
		-- OBTENER AJUSTE DEL MES ANTERIOR.
		-- IF (@TipoAjuste = 1 OR @TipoAjuste = 2) AND (@AjusteMesAnterior = 'S')
		IF @AjusteMesAnterior = 'S'
		BEGIN
			SELECT @DEUDA_MA = (PF_PAGO-PF_NOMINA) FROM VCED_FON
				WHERE CB_CODIGO = @CB_CODIGO and PF_YEAR = @YEAR_ANTERIOR AND PF_MES = @MES_ANTERIOR  AND PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN 
			
			SET @DEUDA_MA = COALESCE (@DEUDA_MA, 0.000);
		END
		ELSE
			SET @DEUDA_MA = 0.000;
	    
		-- AJUSTE.
		-- 1. SUAVIZADO, ES DECIR, DURANTE EL MES.
		-- 2. AJUSTA CADA PER�ODO.
		-- 3. AJUSTA AL FINAL DE MES (�LTIMO PER�ODO).
		-- IF (@TipoAjuste = 1) OR (@TipoAjuste = 2)
	   
		-- OBTENER LA RETENCI�N POR PERIODO.
		IF @NUM_PERIODO_MES != 0
		BEGIN
			IF @MesPeriodoFon <> 13
			BEGIN
				SELECT @RETENCION_PERIODO = ((PF_PAGO+@DEUDA_MA)/@NUM_PERIODO_MES) FROM PCED_FON
					WHERE CB_CODIGO = @CB_CODIGO and PF_YEAR = @AnioPeriodo AND PF_MES = @MesPeriodoFon AND PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN;
			END
			ELSE
			BEGIN
				SELECT @RETENCION_PERIODO = ((PF_PAGO+@DEUDA_MA)/@NUM_PERIODO_MES) FROM PCED_FON
					WHERE CB_CODIGO = @CB_CODIGO and PF_YEAR = (@AnioPeriodo+1) AND PF_MES = 1 AND PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN;		
			END
			SET @RETENCION_PERIODO = coalesce( @RETENCION_PERIODO , 0.000 );
		END
		ELSE
		BEGIN
			SET @RETENCION_PERIODO = 0.0;
		END

		-- OBTENER MONTO ESPERADO.
		-- IF (@TipoAjuste = 1 OR @TipoAjuste = 2) OR (@TipoAjuste = 3 AND @POS_PERIODO_MES = @NUM_PERIODO_MES)
		IF (@TipoAjuste = 1 OR @TipoAjuste = 2) OR (@TipoAjuste = 3 AND @POS_PERIODO_MES = @NUM_PERIODO_MES AND @MesPeriodoFon <> 13)
		BEGIN
			SET @MONTO_ESPERADO = (@POS_PERIODO_MES - 1)*@RETENCION_PERIODO;
			-- OBTENER EL AJUSTE,
			-- ES DECIR, LA DIFERENCIA ENTRE LO QUE DEBER�A LLEVAR DESCONTADO HASTA EL MOMENTO (@MONTO_ESPERADO)
			-- Y LO QUE EN REALIDAD LLEVA (VCED_FON.PF_NOMINA).
			-- AJUSTE SUAVIZADO, ES DECIR, DURANTE LOS PER�ODOS QUE RESTAN EN EL MES.

			IF (@TipoAjuste = 1)
			BEGIN
				SET @AJUSTE = (@MONTO_ESPERADO - @MONTO_RETENIDO ) / ((@NUM_PERIODO_MES-@POS_PERIODO_MES)+1)
			END
			-- AJUSTE CADA PER�ODO O AL FINAL DEL MES.
			ELSE IF (@TipoAjuste = 2 OR @TipoAjuste = 3)
			BEGIN
				SET @AJUSTE = (@MONTO_ESPERADO - @MONTO_RETENIDO ) 
			END

			SET @AJUSTE = COALESCE (@AJUSTE, 0.000);
		END
		ELSE
		BEGIN
			SET @MONTO_ESPERADO = 0.000;
			SET @AJUSTE = 0.000;
		END
	   
		SET @MONTO_CONCEPTO = @RETENCION_PERIODO + @AJUSTE

		IF (@POS_PERIODO_MES = @NUM_PERIODO_MES) AND (@MesPeriodoFon <> 13)
		BEGIN
			IF ABS (@MONTO_CEDULA - (@MONTO_RETENIDO + @MONTO_CONCEPTO)) < 1
			BEGIN
				SET @MONTO_CONCEPTO = @MONTO_CONCEPTO + (@MONTO_CEDULA - (@MONTO_RETENIDO + @MONTO_CONCEPTO))				
			END
		END

		insert into @Valores (
				RETENCION_PERIODO, 
				MONTO_CEDULA, 
				MONTO_RETENIDO, 
				MONTO_ESPERADO, 
				AJUSTE, 
				MONTO_CONCEPTO ,
				DEUDA_MA, 
				NUM_PERIODO_MES,
				POS_PERIODO_MES,
				MES_ANTERIOR ,  
				YEAR_ANTERIOR
		) values ( 
				@RETENCION_PERIODO, 
				@MONTO_CEDULA, 
				@MONTO_RETENIDO, 
				@MONTO_ESPERADO, 
				@AJUSTE, 
				@MONTO_CONCEPTO ,
				@DEUDA_MA, 
				@NUM_PERIODO_MES,
				@POS_PERIODO_MES,
				@MES_ANTERIOR ,  
				@YEAR_ANTERIOR
		)

		RETURN 
end
GO

/* 16988 : Eliminar registro de c�dula fonacot */
if exists (select * from sysobjects where id = object_id(N'FN_FONACOT_CONTEO_CEDULA_AFECTADA') and xtype in (N'FN', N'IF', N'TF')) 
	drop function FN_FONACOT_CONTEO_CEDULA_AFECTADA
go

create function FN_FONACOT_CONTEO_CEDULA_AFECTADA (@YEAR SMALLINT, @MES SMALLINT, @RAZON_SOCIAL CHAR(6)) returns Integer 
as
begin
	 DECLARE @Return Integer

	 SET @Return = (SELECT Count(*)
     FROM VCED_FON V JOIN COLABORA C ON V.CB_CODIGO = C.CB_CODIGO
     WHERE V.PF_YEAR = @YEAR
	       AND V.PF_MES = @MES 
	       AND C.CB_PATRON IN (SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RAZON_SOCIAL or @RAZON_SOCIAL = '')		   
		   and V.PF_NOM_QTY > 0 )

	 Return @Return
end
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_FONACOT_ELIMINAR_CEDULA') AND type IN ( N'P', N'PC' ))
    drop procedure SP_FONACOT_ELIMINAR_CEDULA
go

create procedure SP_FONACOT_ELIMINAR_CEDULA (@YEAR SMALLINT, @MES SMALLINT, @RAZON_SOCIAL CHAR(6), @US_CODIGO SMALLINT, @PROC_ID INTEGER, @BI_NUMERO INTEGER) 
as
begin
	 set nocount on
	 DECLARE @NOMINAS_AFECTADAS INTEGER;
	 DECLARE @BI_TEXTO Observaciones
	 DECLARE @BI_DATA NVARCHAR(MAX)
	 DECLARE @BI_FEC_MOV Fecha
	 DECLARE @PR_TIPO Codigo1
	 DECLARE @CONTADOR_ELIMINADO INTEGER
	 DECLARE @EMPLEADO int;
	 DECLARE @REFERENCIA VARCHAR(8)


	 select @PR_TIPO = LEFT( LTRIM(GL_FORMULA) , 1)  from GLOBAL where GL_CODIGO = 211 
	 set @PR_TIPO = COALESCE( @PR_TIPO, '' ) 

	 SELECT @NOMINAS_AFECTADAS = dbo.FN_FONACOT_CONTEO_CEDULA_AFECTADA(@YEAR, @MES, @RAZON_SOCIAL)  
	 
	 if @NOMINAS_AFECTADAS = 0
	 begin
		  SELECT @BI_FEC_MOV = GETDATE();
		  		  
		  Declare Temporal CURSOR for
 		  SELECT PR.CB_CODIGO, PR.PR_REFEREN from  PRESTAMO PR JOIN COLABORA C ON PR.CB_CODIGO = C.CB_CODIGO
			WHERE PR.CB_CODIGO IN (SELECT CB_CODIGO FROM PCED_FON WHERE PF_YEAR = @YEAR AND PF_MES =@MES AND  PR_TIPO = @PR_TIPO) AND PR.PR_TIPO = @PR_TIPO
			AND C.CB_PATRON IN (SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RAZON_SOCIAL or @RAZON_SOCIAL = '') 

		  OPEN TEMPORAL
		     fetch NEXT FROM Temporal 
	         into  @EMPLEADO, @REFERENCIA
		  WHILE @@FETCH_STATUS = 0 
		  BEGIN
				UPDATE PRESTAMO SET PR_STATUS = 1, PR_FON_MOT = 4 WHERE CB_CODIGO = @EMPLEADO AND PR_REFEREN = @REFERENCIA
				SET @BI_TEXTO = 'Se cancel� pr�stamo Fonacot con referencia '  + @REFERENCIA; 
				exec SP_INSERTAR_BITACORA @US_CODIGO, @PROC_ID, 0, @BI_NUMERO, @BI_TEXTO, @EMPLEADO, '', 62, @BI_FEC_MOV;
			    fetch NEXT FROM Temporal 
 			    into  @EMPLEADO, @REFERENCIA
  		   END;

   	       close Temporal
    	   deallocate Temporal

		   DELETE PF FROM PCED_FON PF JOIN COLABORA C ON PF.CB_CODIGO = C.CB_CODIGO
		  WHERE PF.PF_YEAR = @YEAR AND PF.PF_MES = @MES AND C.CB_PATRON IN (SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RAZON_SOCIAL or @RAZON_SOCIAL = '') 
			   AND PF.PR_TIPO = @PR_TIPO; 

		  SELECT @CONTADOR_ELIMINADO = @@ROWCOUNT; 
		  
		  if @CONTADOR_ELIMINADO = 0 
			 SET @BI_TEXTO = 'No se eliminaron registros de c�dula Fonacot.';
		  else
			  SET @BI_TEXTO = 'Se eliminaron: ' + cast (@CONTADOR_ELIMINADO as varchar) + ' registro(s) de c�dula Fonacot.'; 		  
		  
		  exec SP_INSERTAR_BITACORA @US_CODIGO, @PROC_ID, 0, @BI_NUMERO, @BI_TEXTO, 0, '', 62, @BI_FEC_MOV;


		  /*DELETE PR FROM PRESTAMO PR JOIN COLABORA C ON PR.CB_CODIGO = C.CB_CODIGO
		  WHERE PR.CB_CODIGO NOT IN ( SELECT PC.CB_CODIGO FROM PCED_FON PC WHERE PC.CB_CODIGO IS NOT NULL)
		    AND PR.PR_TIPO = @PR_TIPO 
			AND C.CB_PATRON IN (SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RAZON_SOCIAL or @RAZON_SOCIAL = '') 
			AND PR_OBSERVA LIKE 'Carga inicial en la c�dula Fonacot%'

		  SELECT @CONTADOR_ELIMINADO = @@ROWCOUNT; 
		  
		  if @CONTADOR_ELIMINADO = 0 
			 SET @BI_TEXTO = 'No se eliminaron pr�stamos Fonacot.';
		  else
			  SET @BI_TEXTO = 'Se eliminaron: ' + cast (@CONTADOR_ELIMINADO as varchar) + ' pr�stamos(s) Fonacot.'; 		  
		  
		  exec SP_INSERTAR_BITACORA @US_CODIGO, @PROC_ID, 0, @BI_NUMERO, @BI_TEXTO, 0, '', 62, @BI_FEC_MOV;
		  */
	 end;
end
GO

/* Vista VCED_FON, consultar el nuevo campo de Mes en Per�odo (PF_MES_FON): 1/2 */
IF exists( select * from sys.objects where object_id = object_id('VCED_FON') )
    drop view VCED_FON
GO

/* Vista VCED_FON, consultar el nuevo campo de Mes en Per�odo (PF_MES_FON): 2/2 */
create view VCED_FON(
    CB_CODIGO,
    PR_TIPO,
    PR_REFEREN,
    PF_YEAR,
    PF_MES,
    PF_PAGO,
    PF_PLAZO,
    PF_PAGADAS,
    PF_CAPTURA,
	PF_REUBICA,
    US_CODIGO,
    PF_NOMINA, 
    PF_NOM_QTY, 
	PERIODO_INICIAL, 
	PERIODO_FINAL )
as
select
    PCED_FON.CB_CODIGO,
    PR_TIPO,
    PR_REFEREN,
    PF_YEAR,
    PF_MES,
    PF_PAGO,
    PF_PLAZO,
    PF_PAGADAS,
    PF_CAPTURA,
	PF_REUBICA,
    US_CODIGO,
    coalesce( Afectadas.PF_Monto, 0.00 ) , 
    coalesce( Afectadas.PF_Cuantos, 0 ) , 
	coalesce( Afectadas.PERIODO_INICIAL, 0 ) , 
	coalesce( Afectadas.PERIODO_FINAL, 0 )  
from PCED_FON
left outer join 
(
    select M.PE_YEAR, P.PE_MES_FON,  NO.CB_CODIGO, M.MO_REFEREN, coalesce( SUM( M.MO_PERCEPC + M.MO_DEDUCCI ), 0.00 )  PF_Monto, count(*) PF_Cuantos, MIN(P.PE_NUMERO) PERIODO_INICIAL, MAX(P.PE_NUMERO) PERIODO_FINAL from MOVIMIEN M 
      join NOMINA NO on 
              ( NO.PE_YEAR = M.PE_YEAR ) and
               ( NO.PE_TIPO = M.PE_TIPO ) and
               ( NO.PE_NUMERO = M.PE_NUMERO ) and 
               ( NO.CB_CODIGO  = M.CB_CODIGO ) 
      join PERIODO P on
               ( P.PE_YEAR = NO.PE_YEAR ) and
               ( P.PE_TIPO = NO.PE_TIPO ) and
               ( P.PE_NUMERO = NO.PE_NUMERO )
      where ( M.MO_ACTIVO = 'S' ) and ( NO.NO_STATUS >= 6 ) and M.CO_NUMERO = dbo.FN_GetConceptoFonacot()
    group by  M.PE_YEAR, P.PE_MES_FON,  NO.CB_CODIGO, M.MO_REFEREN
) Afectadas on Afectadas.PE_YEAR = PF_YEAR and Afectadas.PE_MES_FON = PCED_FON.PF_MES and  Afectadas.CB_CODIGO = PCED_FON.CB_CODIGO and Afectadas.MO_REFEREN = PCED_FON.PR_REFEREN 
GO

/* #16982 Fecha de inicio de pr�stamo debe ser al inicio del periodo del mes y no al inicio del mes. 1/4 */
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_FONACOT_AGREGAR_UN_PRESTAMO') AND type IN ( N'P', N'PC' ))
    DROP PROCEDURE SP_FONACOT_AGREGAR_UN_PRESTAMO
go
 
/* #16982 Fecha de inicio de pr�stamo debe ser al inicio del periodo del mes y no al inicio del mes. 2/4 */
CREATE PROCEDURE SP_FONACOT_AGREGAR_UN_PRESTAMO (@CB_CODIGO NumeroEmpleado, @PR_TIPO Codigo1, @PR_REFEREN Referencia, @PR_FECHA Fecha, @PR_STATUS status, @PR_OBSERVA ReferenciaLarga,  @US_CODIGO Usuario, @PF_PAGO Pesos) 
as
begin 
	set nocount on 
	declare @Fecha_Inicio Fecha 
	
	SELECT @Fecha_Inicio = PE_FEC_INI FROM PERIODO P INNER JOIN	FN_FONACOT_PERIODOS (year( @PR_FECHA), month( @PR_FECHA ), (SELECT CB_NOMINA FROM COLABORA WHERE CB_CODIGO = @CB_CODIGO)) FP
		ON P.PE_NUMERO = FP.PE_NUMERO
		where
			PE_YEAR = year( @PR_FECHA) and PE_MES_FON = month( @PR_FECHA ) 
			AND PE_TIPO = (SELECT CB_NOMINA FROM COLABORA WHERE CB_CODIGO = @CB_CODIGO)
			AND PE_POS_FON = 1


	set @Fecha_Inicio = coalesce( @Fecha_Inicio, @PR_FECHA ) 


	if ( select count(*) from PRESTAMO where CB_CODIGO = @CB_CODIGO and PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN ) = 0
	begin 
		INSERT INTO PRESTAMO (CB_CODIGO, PR_TIPO, PR_REFEREN, PR_FECHA, PR_STATUS, PR_OBSERVA, US_CODIGO, PR_MONTO, PR_TOTAL, PR_SALDO, PR_PAG_PER, PR_FON_MOT) 
		VALUES ( @CB_CODIGO, @PR_TIPO, @PR_REFEREN, @Fecha_Inicio, @PR_STATUS, @PR_OBSERVA, @US_CODIGO, @PF_PAGO, @PF_PAGO, @PF_PAGO , @PF_PAGO, 0 )
	end

end
go

/* #16982 Fecha de inicio de pr�stamo debe ser al inicio del periodo del mes y no al inicio del mes. 3/4 */
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_FONACOT_ACTUALIZAR_UN_PRESTAMO') AND type IN ( N'P', N'PC' ))
    DROP PROCEDURE SP_FONACOT_ACTUALIZAR_UN_PRESTAMO
go

/* #16982 Fecha de inicio de pr�stamo debe ser al inicio del periodo del mes y no al inicio del mes. 4/4 */
CREATE PROCEDURE SP_FONACOT_ACTUALIZAR_UN_PRESTAMO (@CB_CODIGO NumeroEmpleado, @PR_REFEREN Referencia, @PR_TIPO Codigo1,  @iPR_STATUS status, @iUS_CODIGO Usuario, @lGrabarBitacora Booleano = 'S' ) 
as
begin 
    set nocount on 
       DECLARE @BI_TEXTO Observaciones
       DECLARE @BI_DATA NVARCHAR(MAX)
          DECLARE @BI_FEC_MOV Fecha
       DECLARE @PR_MONTO Pesos 
          DECLARE @PR_PAG_PER Pesos 

    declare @PR_FECHA Fecha 
	select @PR_FECHA = PR_FECHA from PRESTAMO where CB_CODIGO = @CB_CODIGO and PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN 
	
	declare @year int 
	declare @mes int 

	select top 1  @year = PF_YEAR , @mes = PF_MES  from PCED_FON where CB_CODIGO = @CB_CODIGO and PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN 
	order by PF_YEAR, PF_MES 

	declare @Fecha_Inicio Fecha 

	SELECT @Fecha_Inicio = PE_FEC_INI FROM PERIODO P INNER JOIN	FN_FONACOT_PERIODOS (@Year, @Mes, (SELECT CB_NOMINA FROM COLABORA WHERE CB_CODIGO = @CB_CODIGO)) FP
		ON P.PE_NUMERO = FP.PE_NUMERO
		where
			PE_YEAR = @Year and PE_MES_FON = @Mes 
			AND PE_TIPO = (SELECT CB_NOMINA FROM COLABORA WHERE CB_CODIGO = @CB_CODIGO)
			AND PE_POS_FON = 1

	set @Fecha_Inicio = coalesce( @Fecha_Inicio, @PR_FECHA ) 

    set @PR_PAG_PER = dbo.FN_Fonacot_Get_Saldo_UltimaCedula( @CB_CODIGO, @PR_REFEREN, @PR_TIPO ) 
    SELECT @PR_MONTO = SUM(PF_PAGO) FROM PCED_FON WHERE CB_CODIGO = @CB_CODIGO AND PR_REFEREN = @PR_REFEREN AND PR_TIPO = @PR_TIPO 
    SET @PR_MONTO = COALESCE(@PR_MONTO, 0.00)        

    update PRESTAMO set PR_FECHA = @Fecha_Inicio, PR_STATUS = @iPR_STATUS,  US_CODIGO= @iUS_CODIGO ,  PR_MONTO = @PR_MONTO, PR_TOTAL = @PR_MONTO, PR_SALDO = @PR_MONTO, PR_PAG_PER = @PR_PAG_PER , 
        PR_FON_MOT =  case                                       
                    when @iPR_STATUS = 1 
					then 3 
                    else
						0 
					end                                               
    where CB_CODIGO = @CB_CODIGO and PR_REFEREN = @PR_REFEREN and PR_TIPO = @PR_TIPO                     

    if ( @lGrabarBitacora  = 'S' ) 
    begin 
            SET @BI_TEXTO =  'Modificaci�n desde C�dula Fonacot: Ref:' + @PR_REFEREN;
            SET @BI_DATA = 'Pr�stamo: Empl.:' + cast (@CB_CODIGO as varchar) + ' Tipo:' + @PR_TIPO + ' Ref:' + @PR_REFEREN + CHAR(13)+CHAR(10) + 'PR_STATUS' + CHAR(13)+CHAR(10) + ' A : ' + cast (@iPR_STATUS as varchar)
    
            SELECT @BI_FEC_MOV = GETDATE();
            exec SP_INSERTAR_BITACORA @iUS_CODIGO, 0, 0, 0, @BI_TEXTO, @CB_CODIGO, @BI_DATA, 62, @BI_FEC_MOV;
    end;
end
GO