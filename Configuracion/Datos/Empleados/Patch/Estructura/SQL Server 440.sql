/****************************************/
/****************************************/
/******* Patch para DATOS  **************/
/****************************************/
/****************************************/

/* Deshabilitar Triggers */
 DISABLE TRIGGER ALL on DBO.EXPEDIEN;
 GO
 DISABLE TRIGGER ALL on DBO.COLABORA;
 GO
/* Patch 440: Vista V_DB_INFO (1/2) */
IF exists (SELECT * FROM dbo.sysobjects where id = object_id(N'[dbo].[V_DB_INFO]') AND xtype in (N'V'))
	DROP VIEW V_DB_INFO
GO

/* Patch 440: Vista V_DB_INFO (2/2) */
create view V_DB_INFO (
	DB_CODIGO,
	DB_DESCRIP,
	DB_CONTROL,
	DB_TIPO,
	DB_DATOS,
	DB_USRNAME,
	DB_PASSWRD,
	DB_CTRL_RL,
	DB_CHKSUM
   ) as
select
	DB_CODIGO,
	DB_DESCRIP,
	DB_CONTROL,
	DB_TIPO,
	DB_DATOS,
	DB_USRNAME,
	DB_PASSWRD,
	DB_CTRL_RL,
	DB_CHKSUM
from #COMPARTE.dbo.DB_INFO
GO

/* Patch 440: Quitar Campo llave C_T_NACOMP (1/2) */
if exists( select NAME from SYSCOLUMNS where NAME = 'Llave' and ID = object_id(N'C_T_NACOMP') and CDEFAULT > 0 )
exec sp_unbindefault 'C_T_NACOMP.Llave'
GO

/* Patch 440: Quitar Campo llave C_T_NACOMP (2/2) */
if exists( select NAME from SYSCOLUMNS where NAME = 'Llave' and ID = object_id(N'C_T_NACOMP'))
alter table C_T_NACOMP drop column Llave
GO

/* Patch 440: Quitar Campo llave C_PER_COMP (1/2) */
if exists( select NAME from SYSCOLUMNS where NAME = 'Llave' and ID = object_id(N'C_PER_COMP')and CDEFAULT > 0)
exec sp_unbindefault 'C_PER_COMP.Llave'
GO

/* Patch 440: Quitar Campo llave C_PER_COMP (2/2) */
if exists( select NAME from SYSCOLUMNS where NAME = 'Llave' and ID = object_id(N'C_PER_COMP'))
alter table C_PER_COMP drop column Llave
GO

/* Patch 440: Quitar Campo llave C_PERF_PTO (1/2) */
if exists( select NAME from SYSCOLUMNS where NAME = 'Llave' and ID = object_id(N'C_PERF_PTO') and CDEFAULT > 0)
exec sp_unbindefault 'C_PERF_PTO.Llave'
GO

/* Patch 440: Quitar Campo llave C_PERF_PTO (2/2) */
if exists( select NAME from SYSCOLUMNS where NAME = 'Llave' and ID = object_id(N'C_PERF_PTO'))
alter table C_PERF_PTO drop column Llave
GO

/* Patch 440: Quitar Campo llave C_EMP_COMP (1/2) */
if exists( select NAME from SYSCOLUMNS where NAME = 'Llave' and ID = object_id(N'C_EMP_COMP') and CDEFAULT > 0)
exec sp_unbindefault 'C_EMP_COMP.Llave'
GO

/* Patch 440: Quitar Campo llave C_EMP_COMP (2/2) */
if exists( select NAME from SYSCOLUMNS where NAME = 'Llave' and ID = object_id(N'C_EMP_COMP'))
alter table C_EMP_COMP drop column Llave
GO

/* Patch 440: Quitar Campo llave C_EVA_COMP (1/2) */
if exists( select NAME from SYSCOLUMNS where NAME = 'Llave' and ID = object_id(N'C_EVA_COMP') and CDEFAULT > 0)
exec sp_unbindefault 'C_EVA_COMP.Llave'
GO

/* Patch 440: Quitar Campo llave C_EVA_COMP (2/2) */
if exists( select NAME from SYSCOLUMNS where NAME = 'Llave' and ID = object_id(N'C_EVA_COMP'))
alter table C_EVA_COMP drop column Llave
GO

/* Patch 440: Defecto SOP-5300 - Labor no permite la captura de 5 decimales en Tiempo Standar */
alter table PARTES alter column AR_STD_HR Tasa
GO

/* Patch 440: Agregar campo COLABORA.CB_BANCO (1/3) */
IF NOT EXISTS( SELECT * FROM syscolumns WHERE NAME = 'CB_BANCO' AND id = object_id('COLABORA'))
	alter table COLABORA add CB_BANCO Codigo null
GO
/* Patch 440: Agregar campo COLABORA.CB_BANCO (2/3) */
update COLABORA set CB_BANCO = ''  where CB_BANCO is null
GO
/* Patch 440: Agregar campo COLABORA.CB_BANCO (3/3) */
alter table COLABORA alter column CB_BANCO Codigo not null
GO

IF NOT EXISTS( SELECT * FROM syscolumns WHERE NAME = 'CB_REGIMEN' AND id = object_id('COLABORA'))
	alter table COLABORA add CB_REGIMEN Status null 
GO 
update COLABORA set CB_REGIMEN = 0  where CB_REGIMEN is null 
GO 
alter table COLABORA alter column CB_REGIMEN Status not null 
GO 

/* Patch 440: Agregar campo RSOCIAL.CT_CODIGO (1/3) */
IF NOT EXISTS( SELECT * FROM syscolumns WHERE NAME = 'CT_CODIGO' AND id = object_id('RSOCIAL'))
	alter table RSOCIAL add CT_CODIGO Codigo null 
GO

/* Patch 440: Agregar campo RSOCIAL.CT_CODIGO (2/3) */
update RSOCIAL set CT_CODIGO = ''  where CT_CODIGO is null
GO

/* Patch 440: Agregar campo RSOCIAL.CT_CODIGO (3/3) */
alter table RSOCIAL alter column CT_CODIGO Codigo not null
GO

/* Patch 440: Agregar campo RSOCIAL.RS_CONTID (1/3) */
IF NOT EXISTS( SELECT * FROM syscolumns WHERE NAME = 'RS_CONTID' AND id = object_id('RSOCIAL'))
	alter table RSOCIAL add RS_CONTID FolioGrande  null 
GO

/* Patch 440: Agregar campo RSOCIAL.RS_CONTID (2/3) */
update RSOCIAL set RS_CONTID = 0  where RS_CONTID is null
GO

/* Patch 440: Agregar campo RSOCIAL.RS_CONTID (3/3) */
alter table RSOCIAL alter column RS_CONTID FolioGrande  not null
GO

/* Patch 440: Status del timbre - Agregar campo PERIODO.PE_TIMBRO (1/3) */
IF NOT EXISTS( SELECT * FROM syscolumns WHERE NAME = 'PE_TIMBRO' AND id = object_id('PERIODO'))
	alter table PERIODO add PE_TIMBRO Status  null
GO

/* Patch 440: Status del timbre - Agregar campo PERIODO.PE_TIMBRO (2/3) */
update PERIODO set PE_TIMBRO  = 0 where PE_TIMBRO is null
GO

/* Patch 440: Status del timbre - Agregar campo PERIODO.PE_TIMBRO (3/3) */
alter table PERIODO alter column PE_TIMBRO Status not  null
GO

/* Patch 440: Status del timbre - Agregar campo NOMINA.NO_TIMBRO (1/3) */
IF NOT EXISTS( SELECT * FROM syscolumns WHERE NAME = 'NO_TIMBRO' AND id = object_id('NOMINA'))
	alter table NOMINA add NO_TIMBRO   Status  null
GO

/* Patch 440: Status del timbre - Agregar campo NOMINA.NO_SELLO (1/3) */
IF NOT EXISTS( SELECT * FROM syscolumns WHERE NAME = 'NO_SELLO' AND id = object_id('NOMINA'))
	alter table NOMINA add NO_SELLO Formula  null
GO

/* Patch 440: Status del timbre - Agregar campo NOMINA.NO_FACTURA (1/3) */
IF NOT EXISTS( SELECT * FROM syscolumns WHERE NAME = 'NO_FACTURA' AND id = object_id('NOMINA'))
	alter table NOMINA add NO_FACTURA  FolioGrande  null
GO

/* Patch 440: Status del timbre - Agregar campo NOMINA.NO_PAGO(1/3) */
IF NOT EXISTS( SELECT * FROM syscolumns WHERE NAME = 'NO_PAGO' AND id = object_id('NOMINA'))
	alter table NOMINA add NO_PAGO Status  null 

go 


/* Patch 440: Status del timbre - Agregar campo NOMINA.NO_TIMBRO (2/3) */
update NOMINA set NO_TIMBRO  = 0 where NO_TIMBRO is null
GO

/* Patch 440: Status del timbre - Agregar campo NOMINA.NO_SELLO (2/3) */
update NOMINA set NO_SELLO  = '' where NO_SELLO is null
GO

/* Patch 440: Status del timbre - Agregar campo NOMINA.NO_FACTURA (2/3) */
update NOMINA set NO_FACTURA  = 0  where NO_FACTURA is null
GO

/* Patch 440: Status del timbre - Agregar campo NOMINA.NO_TIMBRO (3/3) */
alter table NOMINA alter column NO_TIMBRO   Status  not null
GO

/* Patch 440: Status del timbre - Agregar campo NOMINA.NO_SELLO (3/3) */
alter table NOMINA alter column NO_SELLO Formula  not null
GO

/* Patch 440: Status del timbre - Agregar campo NOMINA.NO_FACTURA (3/3) */
IF NOT EXISTS( SELECT * FROM syscolumns WHERE NAME = 'NO_FACUUID' AND id = object_id('NOMINA'))
	alter table NOMINA alter column NO_FACTURA  FolioGrande  not null
GO

/* Patch 440: Agregar tabla TIPO_SAT */
IF NOT EXISTS( select * from sysobjects where NAME = 'TIPO_SAT' and type = 'U' )
begin
	create table TIPO_SAT (
		TB_CODIGO	Codigo PRIMARY KEY,
		TB_ELEMENT	Formula,
		TB_SAT_CLA	Status,
		TB_SAT_NUM	FolioChico,
		TB_INGLES	Descripcion,
		TB_NUMERO	Pesos,
		TB_TEXTO	Descripcion,
		TB_ACTIVO	Booleano
	)
end
GO

/* Patch 440: Agregar tabla BANCO */
IF NOT EXISTS( select * from sysobjects where NAME = 'BANCO' and type = 'U' )
begin
	create table BANCO (
		TB_CODIGO	Codigo PRIMARY KEY,
		TB_ELEMENT	DescLarga,
		TB_NOMBRE	Formula,
		TB_SAT_BAN	FolioChico,
		TB_INGLES	Descripcion,
		TB_NUMERO	Pesos,
		TB_TEXTO	Descripcion,
		TB_ACTIVO	Booleano
	)
end
GO

/* Patch 440: Agregar campos a tabla CONCEPTO (1/3) */
IF NOT EXISTS( SELECT * FROM syscolumns WHERE NAME = 'CO_SAT_CON' AND id = object_id('CONCEPTO'))
begin
alter table CONCEPTO
add
	CO_SAT_CLP	Status  NULL,
	CO_SAT_TPP	Codigo  NULL ,
	CO_SAT_CLN	Status  NULL,
	CO_SAT_TPN	Codigo  Null,
	CO_SAT_EXE	Status  NULL,
	CO_SAT_CON	Concepto  NULL
end
GO

/* Patch 440: Agregar campos a tabla CONCEPTO (2/3) */
update CONCEPTO set CO_SAT_CLP = 0 where CO_SAT_CLP is null
GO

update CONCEPTO set  CO_SAT_TPP = '' where  CO_SAT_TPP is null
GO

update CONCEPTO set	CO_SAT_CLN = 0	where CO_SAT_CLN is null
GO

update CONCEPTO set  CO_SAT_TPN = '' where CO_SAT_TPN is null
GO

update CONCEPTO set CO_SAT_EXE = 0 where CO_SAT_EXE is null
GO

update CONCEPTO set  CO_SAT_CON = '' where CO_SAT_CON is null
GO

/* Patch 440: Agregar campos a tabla CONCEPTO (3/3) */
alter table CONCEPTO alter column  CO_SAT_CLP	Status not null
GO

alter table CONCEPTO alter column  CO_SAT_TPP	Codigo not null
GO

alter table CONCEPTO alter column  CO_SAT_CLN	Status not null
GO

alter table CONCEPTO alter column  CO_SAT_TPN	Codigo not null
GO

alter table CONCEPTO alter column  CO_SAT_EXE	Status not null
GO

alter table CONCEPTO alter column  CO_SAT_CON	Concepto not null
GO

/* Patch 440: Inicializar cat�logos de Timbrado: TIPO_SAT */
IF  ( SELECT count(*)  FROM TIPO_SAT ) = 0  
begin 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( '0000', 'Desconocido', 0, 0, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P001', 'Sueldos, Salarios Rayas y Jornales ', 1, 1, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P012', 'Seguro de Gastos M�dicos Mayores ', 1, 12, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P013', 'Cuotas Sindicales Pagadas por el Patr�n ', 1, 13, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P014', 'Subsidios por incapacidad ', 1, 14, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P015', 'Becas para trabajadores y/o hijos ', 1, 15, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P016', 'Otros ', 1, 16, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P017', 'Subsidio para el empleo ', 1, 17, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P019', 'Horas extra ', 1, 19, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P020', 'Prima dominical ', 1, 20, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P021', 'Prima vacacional ', 1, 21, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P022', 'Prima por antig�edad ', 1, 22, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P002', 'Gratificaci�n Anual (Aguinaldo) ', 1, 2, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P023', 'Pagos por separaci�n ', 1, 23, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P024', 'Seguro de retiro ', 1, 24, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P025', 'Indemnizaciones ', 1, 25, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P026', 'Reembolso por funeral ', 1, 26, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P027', 'Cuotas de seguridad social pagadas por el patr�n ', 1, 27, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P028', 'Comisiones ', 1, 28, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P029', 'Vales de despensa ', 1, 29, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P030', 'Vales de restaurante ', 1, 30, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P031', 'Vales de gasolina ', 1, 31, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P032', 'Vales de ropa ', 1, 32, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P003', 'Participaci�n de los Trabajadores en Utilidades', 1, 3, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P033', 'Ayuda para renta ', 1, 33, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P034', 'Ayuda para art�culos escolares ', 1, 34, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P035', 'Ayuda para anteojos ', 1, 35, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P036', 'Ayuda para transporte ', 1, 36, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P037', 'Ayuda para gastos de funeral ', 1, 37, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P038', 'Otros ingresos por salarios ', 1, 38, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P039', 'Jubilaciones, pensiones o haberes de retiro', 1, 39, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P004', 'Reembolso de Gastos M�dicos Dentales y Hospital', 1, 4, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P005', 'Fondo de Ahorro ', 1, 5, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P006', 'Caja de ahorro ', 1, 6, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P009', 'Contribuciones del Trabajador Pagadas por Patr�n ', 1, 9, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P010', 'Premios por puntualidad ', 1, 10, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P011', 'Prima de Seguro de vida ', 1, 11, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'D001', 'Seguridad social ', 2, 1, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'D002', 'ISR ', 2, 2, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'D003', 'Aportaciones a retiro, cesant�a y vejez', 2, 3, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'D004', 'Otros ', 2, 4, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'D005', 'Aportaciones a Fondo de vivienda ', 2, 5, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'D006', 'Descuento por incapacidad', 2, 6, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'D007', 'Pensi�n alimenticia ', 2, 7, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'D008', 'Renta ', 2, 8, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'D009', 'Pr�stamos provenientes del INFONAVIT', 2, 9, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'D010', 'Pago por cr�dito de vivienda ', 2, 10, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'D011', 'Pago de abonos INFONACOT ', 2, 11, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'D012', 'Anticipo de salarios ', 2, 12, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'D013', 'Pagos hechos con exceso al trabajador ', 2, 13, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'D014', 'Errores ', 2, 14, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'D015', 'P�rdidas ', 2, 15, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'D016', 'Aver�as ', 2, 16, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'D017', 'Adquisici�n de art�culos producidos por la empresa', 2, 17, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'D018', 'Cuotas para cooperativas y cajas de ahorro', 2, 18, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'D019', 'Cuotas sindicales ', 2, 19, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'D020', 'Ausencia (Ausentismo) ', 2, 20, 'S') 
	INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'D021', 'Cuotas obrero patronales ', 2, 21, 'S') 
end 
GO

/* Patch 440: Inicializar cat�logos de Timbrado: BANCO */
IF  ( SELECT count(*)  FROM BANCO ) = 0  
begin 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '002', 'BANAMEX',  'Banco Nacional de M�xico, S.A., Instituci�n de Banca M�ltiple, Grupo Financiero Banamex', 2, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '006', 'BANCOMEXT',  'Banco Nacional de Comercio Exterior, Sociedad Nacional de Cr�dito, Instituci�n de Banca de Desarrollo', 6, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '009', 'BANOBRAS',  'Banco Nacional de Obras y Servicios P�blicos, Sociedad Nacional de Cr�dito, Instituci�n de Banca de Desarrollo', 9, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '012', 'BBVA BANCOMER',  'BBVA Bancomer, S.A., Instituci�n de Banca M�ltiple, Grupo Financiero BBVA Bancomer', 12, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '014', 'SANTANDER',  'Banco Santander (M�xico), S.A., Instituci�n de Banca M�ltiple, Grupo Financiero Santander', 14, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '019', 'BANJERCITO',  'Banco Nacional del Ej�rcito, Fuerza A�rea y Armada, Sociedad Nacional de Cr�dito, Instituci�n de Banca de	Desarrollo', 19, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '021', 'HSBC',  'HSBC M�xico, S.A., instituci�n De Banca M�ltiple, Grupo Financiero HSBC', 21, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '030', 'BAJIO',  'Banco del Baj�o, S.A., Instituci�n de Banca M�ltiple', 30, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '032', 'IXE',  'IXE Banco, S.A., Instituci�n de Banca M�ltiple, IXE Grupo Financiero', 32, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '036', 'INBURSA',  'Banco Inbursa, S.A., Instituci�n de Banca M�ltiple, Grupo Financiero Inbursa', 36, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '037', 'INTERACCIONES',  'Banco Interacciones, S.A., Instituci�n de Banca M�ltiple', 37, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '042', 'MIFEL',  'Banca Mifel, S.A., Instituci�n de Banca M�ltiple, Grupo Financiero Mifel', 42, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '044', 'SCOTIABANK',  'Scotiabank Inverlat, S.A.', 44, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '058', 'BANREGIO',  'Banco Regional de Monterrey, S.A., Instituci�n de Banca M�ltiple, Banregio Grupo Financiero', 58, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '059', 'INVEX',  'Banco Invex, S.A., Instituci�n de Banca M�ltiple, Invex Grupo Financiero', 59, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '060', 'BANSI',  'Bansi, S.A., Instituci�n de Banca M�ltiple', 60, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '062', 'AFIRME',  'Banca Afirme, S.A., Instituci�n de Banca M�ltiple', 62, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '072', 'BANORTE',  'Banco Mercantil del Norte, S.A., Instituci�n de Banca M�ltiple, Grupo Financiero Banorte', 72, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '102', 'THE ROYAL BANK',  'The Royal Bank of Scotland M�xico, S.A., Instituci�n de Banca M�ltiple', 102, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '103', 'AMERICAN EXPRESS',  'American Express Bank (M�xico), S.A.,  Instituci�n de Banca M�ltiple', 103, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '106', 'BAMSA',  'Bank of America M�xico, S.A.,  Instituci�n de Banca M�ltiple, Grupo Financiero Bank of America', 106, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '108', 'TOKYO',  'Bank of Tokyo-Mitsubishi UFJ (M�xico), S.A.', 108, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '110', 'JP MORGAN',  'Banco J.P. Morgan, S.A., Instituci�n de Banca M�ltiple, J.P. Morgan Grupo Financiero', 110, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '112', 'BMONEX',  'Banco Monex, S.A., Instituci�n de Banca M�ltiple', 112, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '113', 'VE POR MAS',  'Banco Ve Por Mas, S.A.  Instituci�n de Banca M�ltiple', 113, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '116', 'ING',  'ING Bank (M�xico), S.A., Instituci�n de Banca M�ltiple, ING Grupo Financiero', 116, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '124', 'DEUTSCHE',  'Deutsche Bank M�xico, S.A., Instituci�n de Banca M�ltiple', 124, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '126', 'CREDIT SUISSE',  'Banco Credit Suisse (M�xico), S.A. Instituci�n de Banca M�ltiple, Grupo Financiero Credit Suisse (M�xico)', 126, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '127', 'AZTECA',  'Banco Azteca, S.A. Instituci�n de Banca M�ltiple.', 127, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '128', 'AUTOFIN',  'Banco Autofin M�xico, S.A. Instituci�n de Banca M�ltiple', 128, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '129', 'BARCLAYS',  'Barclays Bank M�xico, S.A., Instituci�n de Banca M�ltiple, Grupo Financiero Barclays M�xico', 129, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '130', 'COMPARTAMOS',  'Banco Compartamos, S.A., Instituci�n de Banca M�ltiple', 130, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '131', 'BANCO FAMSA',  'Banco Ahorro Famsa, S.A., Instituci�n de Banca M�ltiple', 131, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '132', 'BMULTIVA',  'Banco Multiva, S.A., Instituci�n de Banca M�ltiple,	Multivalores Grupo Financiero', 132, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '133', 'ACTINVER',  'Banco Actinver, S.A. Instituci�n de Banca M�ltiple, Grupo Financiero Actinver', 133, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '134', 'WAL-MART',  'Banco Wal-Mart de M�xico Adelante, S.A., Instituci�n de Banca M�ltiple', 134, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '135', 'NAFIN',  'Nacional Financiera, Sociedad Nacional de Cr�dito, Instituci�n de Banca de Desarrollo', 135, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '136', 'INTERBANCO',  'Inter Banco, S.A. Instituci�n de Banca M�ltiple', 136, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '137', 'BANCOPPEL',  'BanCoppel, S.A., Instituci�n de Banca M�ltiple', 137, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '138', 'ABC CAPITAL',  'ABC Capital, S.A., Instituci�n de Banca M�ltiple', 138, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '139', 'UBS BANK',  'UBS Bank M�xico, S.A., Instituci�n de Banca M�ltiple, UBS Grupo Financiero', 139, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '140', 'CONSUBANCO',  'Consubanco, S.A. Instituci�n de Banca M�ltiple', 140, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '141', 'VOLKSWAGEN',  'Volkswagen Bank, S.A., Instituci�n de Banca M�ltiple', 141, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '143', 'CIBANCO',  'CIBanco, S.A.', 143, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '145', 'BBASE',  'Banco Base, S.A., Instituci�n de Banca M�ltiple', 145, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '166', 'BANSEFI',  'Banco del Ahorro Nacional y Servicios Financieros, Sociedad Nacional de Cr�dito, Instituci�n de Banca de Desarrollo', 166, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '168', 'HIPOTECARIA FEDERAL',  'Sociedad Hipotecaria Federal, Sociedad Nacional de Cr�dito, Instituci�n de Banca de Desarrollo', 168, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '600', 'MONEXCB',  'Monex Casa de Bolsa, S.A. de C.V. Monex Grupo Financiero', 600, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '601', 'GBM',  'GBM Grupo Burs�til Mexicano, S.A. de C.V. Casa de Bolsa', 601, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '602', 'MASARI',  'Masari Casa de Bolsa, S.A.', 602, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '605', 'VALUE',  'Value, S.A. de C.V. Casa de Bolsa', 605, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '606', 'ESTRUCTURADORES',  'Estructuradores del Mercado de Valores Casa de Bolsa, S.A. de C.V.', 606, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '607', 'TIBER',  'Casa de Cambio Tiber, S.A. de C.V.', 607, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '608', 'VECTOR',  'Vector Casa de Bolsa, S.A. de C.V.', 608, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '610', 'B&B',  'B y B, Casa de Cambio, S.A. de C.V.', 610, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '614', 'ACCIVAL',  'Acciones y Valores Banamex, S.A. de C.V., Casa de Bolsa', 614, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '615', 'MERRILL LYNCH',  'Merrill Lynch M�xico, S.A. de C.V. Casa de Bolsa', 615, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '616', 'FINAMEX',  'Casa de Bolsa Finamex, S.A. de C.V.', 616, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '617', 'VALMEX',  'Valores Mexicanos Casa de Bolsa, S.A. de C.V.', 617, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '618', 'UNICA',  'Unica Casa de Cambio, S.A. de C.V.', 618, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '619', 'MAPFRE',  'MAPFRE Tepeyac, S.A.', 619, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '620', 'PROFUTURO',  'Profuturo G.N.P., S.A. de C.V., Afore', 620, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '621', 'CB ACTINVER',  'Actinver Casa de Bolsa, S.A. de C.V.', 621, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '622', 'OACTIN',  'OPERADORA ACTINVER, S.A. DE C.V.', 622, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '623', 'SKANDIA',  'Skandia Vida, S.A. de C.V.', 623, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '626', 'CBDEUTSCHE',  'Deutsche Securities, S.A. de C.V. CASA DE BOLSA', 626, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '627', 'ZURICH',  'Zurich Compa��a de Seguros, S.A.', 627, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '628', 'ZURICHVI',  'Zurich Vida, Compa��a de Seguros, S.A.', 628, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '629', 'SU CASITA',  'Hipotecaria Su Casita, S.A. de C.V. SOFOM ENR', 629, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '630', 'CB INTERCAM',  'Intercam Casa de Bolsa, S.A. de C.V.', 630, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '631', 'CI BOLSA',  'CI Casa de Bolsa, S.A. de C.V.', 631, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '632', 'BULLTICK CB',  'Bulltick Casa de Bolsa, S.A., de C.V.', 632, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '633', 'STERLING',  'Sterling Casa de Cambio, S.A. de C.V.', 633, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '634', 'FINCOMUN',  'Fincom�n, Servicios Financieros Comunitarios, S.A. de C.V.', 634, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '636', 'HDI SEGUROS',  'HDI Seguros, S.A. de C.V.', 636, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '637', 'ORDER',  'Order Express Casa de Cambio, S.A. de C.V', 637, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '638', 'AKALA',  'Akala, S.A. de C.V., Sociedad Financiera Popular', 638, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '640', 'CB JPMORGAN',  'J.P. Morgan Casa de Bolsa, S.A. de C.V. J.P. Morgan Grupo Financiero', 640, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '642', 'REFORMA',  'Operadora de Recursos Reforma, S.A. de C.V., S.F.P.', 642, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '646', 'STP',  'Sistema de Transferencias y Pagos STP, S.A. de C.V.SOFOM ENR', 646, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '647', 'TELECOMM',  'Telecomunicaciones de M�xico', 647, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '648', 'EVERCORE',  'Evercore Casa de Bolsa, S.A. de C.V.', 648, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '649', 'SKANDIA',  'Skandia Operadora de Fondos, S.A. de C.V.', 649, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '651', 'SEGMTY',  'Seguros Monterrey New York Life, S.A de C.V', 651, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '652', 'ASEA',  'Soluci�n Asea, S.A. de C.V., Sociedad Financiera Popular', 652, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '653', 'KUSPIT',  'Kuspit Casa de Bolsa, S.A. de C.V.', 653, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '655', 'SOFIEXPRESS',  'J.P. SOFIEXPRESS, S.A. de C.V., S.F.P.', 655, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '656', 'UNAGRA',  'UNAGRA, S.A. de C.V., S.F.P.', 656, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '659', 'OPCIONES EMPRESARIALES DEL NOROESTE',  'OPCIONES EMPRESARIALES DEL NORESTE, S.A. DE C.V., S.F.P.', 659, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '901', 'CLS',  'Cls Bank International', 901, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '902', 'INDEVAL',  'SD. Indeval, S.A. de C.V.', 902, 'S') 
	INSERT INTO BANCO (TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN,  TB_ACTIVO ) VALUES ( '670', 'LIBERTAD',  'Libertad Servicios Financieros, S.A. De C.V.', 670, 'S') 

end
GO

/* Patch 440: Agregar procedure SP_TIMBRAR_EMPLEADO (1/2) */
if exists (select * from sysobjects where id = object_id(N'SP_TIMBRAR_EMPLEADO') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
 drop procedure SP_TIMBRAR_EMPLEADO
GO

/* Patch 440: Agregar procedure SP_TIMBRAR_EMPLEADO (2/2) */
CREATE procedure SP_TIMBRAR_EMPLEADO( @PE_YEAR Anio, @PE_TIPO NominaTipo, @PE_NUMERO NominaNumero, @CB_CODIGO NumeroEmpleado, @CB_RFC Descripcion, @Status status, @Timbre Formula, @FolioTimbramex FolioGrande, @Usuario usuario )   
as  
begin  
 set nocount on   
 declare @StatusTimbrado status     
 set @StatusTimbrado = 2   
   
 if ( @Status = @StatusTimbrado )   
  update NOMINA set NO_TIMBRO = @Status, NO_SELLO = @Timbre , NO_FACTURA = @FolioTimbramex  
  where  CB_CODIGO = @CB_CODIGO and PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO = @PE_NUMERO   
      and NO_STATUS = 6   
   else  
  update NOMINA set NO_TIMBRO = @Status, NO_SELLO = '',  NO_FACTURA = 0    
  where  CB_CODIGO = @CB_CODIGO and PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO = @PE_NUMERO   
      and NO_STATUS = 6   
end    
GO

/* Patch 440: Agregar procedure AFECTAR_STATUS_TIMBRADO (1/2) */
if exists (select * from sysobjects where id = object_id(N'AFECTAR_STATUS_TIMBRADO') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
 drop procedure AFECTAR_STATUS_TIMBRADO
GO

/* Patch 440: Agregar procedure AFECTAR_STATUS_TIMBRADO (2/2) */
CREATE procedure AFECTAR_STATUS_TIMBRADO( @PE_YEAR Anio, @PE_TIPO NominaTipo, @PE_NUMERO NominaNumero, @StatusActual status, @StatusNuevo status )   
as  
begin  
 set nocount on   
 declare @Total int   
 declare @Timbrados int   
 declare @StatusTimbradoPendiente status    
 declare @StatusTimbradoParcial status    
 declare @StatusTimbradoTotal status    
 declare @NominaAfectada status   
   
   
 set @StatusTimbradoPendiente = 0   
 set @StatusTimbradoParcial = 1   
 set @StatusTimbradoTotal = 2   
 set @NominaAfectada = 6   
   
 --Conteo total   
 select @Total = count(*) from NOMINA where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and NO_STATUS = @NominaAfectada   
   
 if @Total > 0   
 begin   
  --Conteo timbrados  
  select @Timbrados = count(*) from NOMINA where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and  NO_STATUS = @NominaAfectada  and NO_TIMBRO = @StatusTimbradoTotal  
  if ( @StatusNuevo = @StatusTimbradoTotal )    
  begin  
    if ( @Total = @Timbrados )  
    update PERIODO set PE_TIMBRO = @StatusTimbradoTotal  where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and  PE_STATUS = @NominaAfectada   
   else  
    update PERIODO set PE_TIMBRO = @StatusTimbradoParcial where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and  PE_STATUS = @NominaAfectada      
  end  
  else  
  begin  
   if ( @Timbrados = 0 )   
    update PERIODO set PE_TIMBRO = @StatusTimbradoPendiente where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and PE_STATUS = @NominaAfectada   
   else  
    update PERIODO set PE_TIMBRO = @StatusTimbradoParcial where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and  PE_STATUS = @NominaAfectada      
  end;   
 end;  
    
end
GO

/* Patch 440: Modificar procedure AFECTAR_STATUS_TIMBRADO (1/2) */
if exists (select * from sysobjects where id = object_id(N'AFECTA_PERIODO') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
 drop procedure AFECTA_PERIODO
GO

/* Patch 440: Modificar procedure AFECTAR_STATUS_TIMBRADO (2/2) */
CREATE PROCEDURE AFECTA_PERIODO(  
       @ANIO SMALLINT,  
       @TIPO SMALLINT,  
       @NUMERO SMALLINT,  
   @USUARIO SMALLINT,  
       @STATUSVIEJO SMALLINT,  
       @STATUSNUEVO SMALLINT,  
       @FACTOR INTEGER )  
  
AS  
 SET NOCOUNT ON  
 DECLARE @CONCEPTO INTEGER;  
 DECLARE @MES SMALLINT;  
 DECLARE @CONTADOR INTEGER;  
 DECLARE @TOTAL NUMERIC(15,2);  
 DECLARE @TIPOAHORRO CHAR(1);  
 DECLARE @TIPOPRESTAMO CHAR(1);  
 DECLARE @REFERENCIA VARCHAR(8);  
        DECLARE @EMPLEADO INTEGER;  
       
      select @MES = P.PE_MES from PERIODO P   
 where ( P.PE_YEAR = @Anio )   
 and ( P.PE_TIPO = @Tipo )   
 and ( P.PE_NUMERO = @Numero )   
   
 -- Validacion a mirar en Nomina.dll  
 if ( select count(*) from NOMINA N   
  left outer join COLABORA C   
  on ( C.CB_CODIGO = N.CB_CODIGO )  
 where   
  ( N.PE_YEAR = @Anio )   
  and ( N.PE_TIPO = @Tipo )  
  and ( N.PE_NUMERO = @Numero )  
  and not ( C.CB_CODIGO is NULL )  
  and ( N.NO_TIMBRO > 0 )  ) > 0   
 begin   
   RAISERROR( 'La nomina esta Timbrada', 16, 1 )  
   return 0  
 end   
    
    -- Filtro NO_TIMBRO  se queda para la version 2014  
 Declare TempColabora CURSOR for  
         select N.CB_CODIGO from NOMINA N  
         left outer join COLABORA C   
  on ( C.CB_CODIGO = N.CB_CODIGO )  
         where ( N.PE_YEAR = @Anio )   
  and ( N.PE_TIPO = @Tipo )  
  and ( N.PE_NUMERO = @Numero )  
  and ( N.NO_STATUS = @StatusViejo )  
  and not ( C.CB_CODIGO is NULL )  
  and ( N.NO_TIMBRO = 0 )    
  
 OPEN TempColabora  
 fetch NEXT FROM TempColabora  
 INTO @EMPLEADO   
    
 set @Contador = 0  
 WHILE @@FETCH_STATUS = 0  
 BEGIN  
  
  EXECUTE AFECTA_EMPLEADO @Empleado, @Anio, @Tipo, @Numero, @Mes, @Factor;  
  Declare TempAhorro CURSOR for  
          select A.AH_TIPO, T.TB_CONCEPT   
   from AHORRO A join TAHORRO T on ( T.TB_CODIGO = A.AH_TIPO )  
          where ( A.CB_CODIGO = @Empleado )  
   and ( A.AH_STATUS = 0 )  
  
  Open TempAhorro  
  fetch NEXT FROM TempAhorro  
  into @TipoAhorro, @Concepto   
  while @@FETCH_STATUS = 0   
         begin  
          select @Contador = COUNT(*), @Total = SUM( M.MO_DEDUCCI )   
   from MOVIMIEN M   
   where ( M.PE_YEAR = @Anio )   
   and ( M.PE_TIPO = @Tipo )   
   and ( M.PE_NUMERO = @Numero )   
   and ( M.CB_CODIGO= @Empleado )   
   and ( M.CO_NUMERO = @Concepto )  
   and ( M.MO_ACTIVO = 'S' )  
  
   if ( @Contador > 0 )  
          begin  
                  Select @Total = @Factor * @Total;  
                  update AHORRO  set  
                        AH_NUMERO = AH_NUMERO + @Factor,  
                         AH_TOTAL  = AH_TOTAL + @Total,  
                         AH_SALDO = AH_SALDO + @Total  
                  where ( CB_CODIGO = @Empleado )  
    and ( AH_TIPO = @TipoAhorro );  
          end  
   fetch NEXT FROM TempAhorro  
    into   @TipoAhorro, @Concepto  
  end   
     
  close TempAhorro   
      deallocate TempAhorro   
            
  Declare TempPrestamo CURSOR for  
               select P.PR_TIPO, P.PR_REFEREN, T.TB_CONCEPT   
   from PRESTAMO P join TPRESTA T on  
                     ( T.TB_CODIGO = P.PR_TIPO )  
               where ( P.CB_CODIGO = @Empleado )   
   and ( P.PR_STATUS in ( 0, 2 ) )  
  
  Open TempPrestamo  
  fetch NEXT FROM TempPrestamo   
  into @TipoPrestamo, @Referencia, @Concepto   
  
  while @@FETCH_STATUS = 0  
         begin  
          select @Contador = COUNT(*), @Total = SUM( M.MO_DEDUCCI )  
   from MOVIMIEN M   
   where ( M.PE_YEAR = @Anio )   
   and ( M.PE_TIPO = @Tipo )   
   and ( M.PE_NUMERO = @Numero )  
   and ( M.CB_CODIGO= @Empleado )   
   and ( M.CO_NUMERO = @Concepto )   
   and ( M.MO_ACTIVO = 'S' )   
   and ( M.MO_REFEREN = @Referencia )  
                    
   if ( @Contador > 0 )   
          begin  
                  Select @Total = @Factor * @Total;  
                  update PRESTAMO set  
                         PR_NUMERO = PR_NUMERO + @Factor,  
                         PR_TOTAL  = PR_TOTAL + @Total,  
                         PR_SALDO = PR_SALDO - @Total  
                  where ( CB_CODIGO = @Empleado )   
    and ( PR_TIPO = @TipoPrestamo )   
    and ( PR_REFEREN = @Referencia );  
          end  
   fetch NEXT FROM TempPrestamo   
   into @TipoPrestamo, @Referencia, @Concepto   
         end   
  
  close TempPrestamo   
      deallocate TempPrestamo   
             
  EXECUTE SET_STATUS_PRESTAMOS @Empleado;  
  
           if ( @StatusNuevo > 0 )  
           begin  
                 update NOMINA set NO_STATUS = @StatusNuevo  
   where ( PE_YEAR = @Anio )   
   and ( PE_TIPO = @Tipo )   
   and ( PE_NUMERO = @Numero )  
   and ( CB_CODIGO = @Empleado );  
           end  
  fetch NEXT FROM TempColabora   
  INTO @EMPLEADO  
  set @Contador = @Contador + 1  
 end   
  
 close TempColabora   
     deallocate TempColabora   
  
      EXECUTE AFECTA_FALTAS @Anio, @Tipo, @Numero, @Factor  
      if ( @StatusNuevo > 0 )   
      begin  
            
           update NOMINA set NO_STATUS = @StatusNuevo  
           where ( PE_YEAR = @Anio )   
  and ( PE_TIPO = @Tipo )   
  and ( PE_NUMERO = @Numero )   
  and ( ( select COUNT(*) from COLABORA C where ( C.CB_CODIGO = Nomina.CB_CODIGO ) ) = 0 )  
  
           EXECUTE SET_STATUS_PERIODO @Anio, @Tipo, @Numero, @Usuario, @StatusNuevo  
      end  
  
 RETURN @Contador  
 GO

if exists (select * from dbo.sysobjects where id = object_id(N'SP_GET_METODO_PAGO_DEFAULT') and xtype in (N'FN', N'IF', N'TF')) 
 drop function SP_GET_METODO_PAGO_DEFAULT  

go 

CREATE FUNCTION SP_GET_METODO_PAGO_DEFAULT() RETURNS Status
AS
BEGIN
	DECLARE @GLOBAL_Nomina_MetodoPago FolioChico
	DECLARE @Resultado Formula
	DECLARE @ResultadoInt  Status 
	
	SET @GLOBAL_Nomina_MetodoPago = 310
		
	SELECT @Resultado = GL_FORMULA  FROM GLOBAL WHERE  GL_CODIGO = @GLOBAL_Nomina_MetodoPago 
	
	SET @Resultado = COALESCE( @Resultado, '0')	
	set  @ResultadoInt = cast( @Resultado as smallint )

	RETURN @ResultadoInt 
END


go 

if exists (select * from sysobjects where id = object_id(N'SP_CLAS_NOMINA') and OBJECTPROPERTY(id, N'IsProcedure') = 1) 
 drop procedure SP_CLAS_NOMINA 
GO 


CREATE PROCEDURE SP_CLAS_NOMINA
    	@ANIO SMALLINT,
    	@TIPO SMALLINT,
    	@NUMERO SMALLINT,
    	@EMPLEADO INTEGER,
        @ROTATIVO CHAR(6) OUTPUT
AS
    SET NOCOUNT ON
	declare @FechaIni DATETIME;
 	declare @FechaFin DATETIME;
 	declare @ZONA_GE  CHAR(1);
 	declare @PUESTO   CHAR(6);
 	declare @CLASIFI  CHAR(6);
 	declare @TURNO    CHAR(6);
 	declare @PATRON   CHAR(1);
 	declare @NIVEL1   CHAR(6);
 	declare @NIVEL2   CHAR(6);
 	declare @NIVEL3   CHAR(6);
 	declare @NIVEL4   CHAR(6);
 	declare @NIVEL5   CHAR(6);
 	declare @NIVEL6   CHAR(6);
 	declare @NIVEL7   CHAR(6);
 	declare @NIVEL8   CHAR(6);
 	declare @NIVEL9   CHAR(6);
 	declare @SALARIO  NUMERIC(15,2);
 	declare @SAL_INT  NUMERIC(15,2);
 	declare @PROMEDIA CHAR(1);
 	declare @JORNADA  NUMERIC(15,2);
 	declare @D_TURNO  SMALLINT;
 	declare @ES_ROTA  CHAR(1);
 	declare @FEC_KAR  DATETIME;
 	declare @FEC_SAL  DATETIME;
	declare @BAN_ELE  VARCHAR(30);
	declare @NIVEL0   CHAR(6);
	declare @PLAZA	  INTEGER;	
	declare @CB_NOMINA	SMALLINT;
	declare @FEC_ING	DATETIME;
	declare @FEC_BAJ	DATETIME;
	declare @FEC_NOM	DATETIME;
	declare @Fecha		DATETIME;
	declare @k_NOMINA		SMALLINT;
	declare @CTA_GAS  VARCHAR(30);
	declare @CTA_VAL  VARCHAR(30);
	declare @PAGO	SMALLINT; 
	
  	select @FechaIni = PE_FEC_INI,
               @FechaFin = PE_FEC_FIN
  	from   PERIODO
  	where  PE_YEAR = @Anio 
	and PE_TIPO = @Tipo
	and PE_NUMERO = @Numero

	select 
		@CB_NOMINA = CB_NOMINA
        from   SP_FECHA_KARDEX( @FechaIni, @Empleado )     

	
	SELECT @FEC_ING = CB_FEC_ING, 
		  @FEC_BAJ = CB_FEC_BAJ,
		  @FEC_NOM = CB_FEC_NOM,
		  @k_NOMINA = CB_NOMINA		 
		  from COLABORA where CB_CODIGO = @Empleado

	

	IF ( @FEC_NOM between @FechaIni and @FechaFin ) and ( @CB_NOMINA <> @k_NOMINA )
	begin
		if ( @CB_NOMINA = @Tipo ) 
		begin
			set @Fecha = @FechaIni
		end
		else
		begin
			set @Fecha = @FechaFin
		end 
	end
	else
	begin
		set @Fecha = @FechaFin
	end  
	

  	select @ZONA_GE = CB_ZONA_GE,
		@PUESTO = CB_PUESTO,
		@CLASIFI = CB_CLASIFI,
		@TURNO = CB_TURNO,
		@PATRON = CB_PATRON,
                @NIVEL1 = CB_NIVEL1,
		@NIVEL2 = CB_NIVEL2, 
		@NIVEL3 = CB_NIVEL3,
		@NIVEL4 = CB_NIVEL4, 
		@NIVEL5 = CB_NIVEL5,
                @NIVEL6 = CB_NIVEL6,
		@NIVEL7 = CB_NIVEL7, 
		@NIVEL8 = CB_NIVEL8, 
		@NIVEL9 = CB_NIVEL9,
                @SALARIO = CB_SALARIO,
		@SAL_INT = CB_SAL_INT, 
		@FEC_KAR = CB_FEC_KAR, 
		@FEC_SAL = CB_FEC_SAL,
                @BAN_ELE = CB_BAN_ELE,
                @NIVEL0 = CB_NIVEL0,
		@PLAZA	= CB_PLAZA,
		@CTA_GAS = CB_CTA_GAS,
        @CTA_VAL = CB_CTA_VAL
        
  	from   COLABORA
  	where  CB_CODIGO = @Empleado

  	if ( @Fecha < @FEC_KAR ) 
  	begin
    		select 
			@ZONA_GE = CB_ZONA_GE,
			@PUESTO = CB_PUESTO,
			@CLASIFI = CB_CLASIFI, 
			@TURNO = CB_TURNO, 
			@PATRON = CB_PATRON,
         	        @NIVEL1 = CB_NIVEL1,
			@NIVEL2 = CB_NIVEL2, 
			@NIVEL3 = CB_NIVEL3, 
			@NIVEL4 = CB_NIVEL4,
			@NIVEL5 = CB_NIVEL5,
         	        @NIVEL6 = CB_NIVEL6,
			@NIVEL7 = CB_NIVEL7, 
			@NIVEL8 = CB_NIVEL8,
			@NIVEL9 = CB_NIVEL9,
         	        @SALARIO = CB_SALARIO,
			@SAL_INT = CB_SAL_INT,
			@PLAZA	= CB_PLAZA
            from   SP_FECHA_KARDEX( @Fecha, @Empleado )
  	end

  	select @PROMEDIA = GL_FORMULA
  	from   GLOBAL
  	where  GL_CODIGO = 42

  	if (( @PROMEDIA = 'S' ) and ( @FEC_SAL > @FechaIni ))
        EXECUTE SP_RANGO_SALARIO @FechaIni, @FechaFin, @Empleado,
                                 @SALARIO OUTPUT, @SAL_INT OUTPUT

	EXECUTE SP_DIAS_JORNADA @Turno, @FechaIni, @FechaFin,
    				@D_Turno OUTPUT, @Jornada OUTPUT, @Es_Rota OUTPUT

	if ( @ES_ROTA = 'S' )
    		SET @Rotativo = @Turno
  	else
    		SET @Rotativo = '';

	set @PAGO = dbo.SP_GET_METODO_PAGO_DEFAULT() 

  	update NOMINA
  	set     CB_ZONA_GE = @ZONA_GE,
        	CB_PUESTO  = @PUESTO,
        	CB_CLASIFI = @CLASIFI,
        	CB_TURNO   = @TURNO,
        	CB_PATRON  = @PATRON,
        	CB_NIVEL1  = @NIVEL1,
        	CB_NIVEL2  = @NIVEL2,
        	CB_NIVEL3  = @NIVEL3,
        	CB_NIVEL4  = @NIVEL4,
        	CB_NIVEL5  = @NIVEL5,
        	CB_NIVEL6  = @NIVEL6,
        	CB_NIVEL7  = @NIVEL7,
        	CB_NIVEL8  = @NIVEL8,
        	CB_NIVEL9  = @NIVEL9,
        	CB_SALARIO = @SALARIO,
        	CB_SAL_INT = @SAL_INT,
        	NO_JORNADA = @JORNADA,
        	NO_D_TURNO = @D_TURNO,
                CB_BAN_ELE = @BAN_ELE,
                CB_NIVEL0  = @NIVEL0,
                CB_PLAZA   = @PLAZA,
				CB_CTA_GAS = @CTA_GAS,
                CB_CTA_VAL = @CTA_VAL,
			NO_PAGO = @PAGO 

  	where PE_YEAR = @Anio
	and PE_TIPO = @Tipo
	and PE_NUMERO = @Numero
	and CB_CODIGO = @Empleado;
GO


/* Global Regimen SAT  = 2 Sueldos y Salarios  */
/*Patch #440  Seq: */
if not EXISTS ( select 1 from GLOBAL where GL_CODIGO = 312 )
   INSERT INTO GLOBAL (GL_CODIGO,GL_DESCRIP,GL_FORMULA,GL_TIPO,US_CODIGO,GL_CAPTURA,GL_NIVEL) 
   VALUES (312,'R�gimen de SAT predeterminado','2',0,0,GETDATE(),0)
GO 

/* Global Metodo de pato SAT = 2 Transferencia Electronica  */
/*Patch #440  Seq: */
if not EXISTS ( select 1 from GLOBAL where GL_CODIGO = 310 )
   INSERT INTO GLOBAL (GL_CODIGO,GL_DESCRIP,GL_FORMULA,GL_TIPO,US_CODIGO,GL_CAPTURA,GL_NIVEL) 
   VALUES (310,'M�todo de Pago de SAT','2',0,0,GETDATE(),0)
GO 

CREATE FUNCTION ANIOSCOMPLETOS
(
        @FromDate DATETIME,
        @ToDate DATETIME
)
RETURNS INT
AS
BEGIN
        RETURN  CASE
                       WHEN @FromDate > @ToDate THEN NULL
                       WHEN DATEPART(day, @FromDate) > DATEPART(day, @ToDate) THEN DATEDIFF(month, @FromDate, @ToDate) - 1
                       ELSE DATEDIFF(month, @FromDate, @ToDate)
               END / 12
END
GO

ALTER FUNCTION SP_IMSS_V( @TABLA CHAR(1),
  		           @INICIAL DATETIME,
  		           @FINAL DATETIME,
		           @RESULTADO INTEGER,
                           @EMPLEADO INTEGER )
RETURNS NUMERIC(15, 5)
AS
BEGIN
     declare @DiasAntig NUMERIC(15,2);
     declare @Anio INTEGER;
     declare @LastAniv DATETIME;
     if ( @Final < @Inicial )
     begin
          select @Anio = 0
     end
     else
     begin
		  select @Anio = dbo.AniosCompletos( @Inicial, @Final )
          select @LastAniv = DATEADD(year, @Anio, @Inicial)
          if ( @LastAniv < @Final )
          begin
               select @Anio = @Anio + 1
          end
     end
     RETURN DBO.SP_IMSS_YEAR_V( @Tabla, @Anio, @RESULTADO, @EMPLEADO )
END
GO

/* Habilitar Triggers */
ENABLE TRIGGER ALL on DBO.EXPEDIEN;
GO
ENABLE TRIGGER ALL on DBO.COLABORA;
GO