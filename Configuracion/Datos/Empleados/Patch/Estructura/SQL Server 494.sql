IF EXISTS ( SELECT  * FROM  sys.objects WHERE   object_id = OBJECT_ID(N'SP_FONACOT_ACTUALIZAR_UN_PRESTAMO') AND type IN ( N'P', N'PC' ))
    drop procedure SP_FONACOT_ACTUALIZAR_UN_PRESTAMO
go

CREATE PROCEDURE SP_FONACOT_ACTUALIZAR_UN_PRESTAMO (@CB_CODIGO NumeroEmpleado, @PR_REFEREN Referencia, @PR_TIPO Codigo1,  @iPR_STATUS status, @iUS_CODIGO Usuario, @PR_PAG_PER Pesos, @lGrabarBitacora Booleano = 'S' ) 
as
begin 
    set nocount on 
    DECLARE @BI_TEXTO Observaciones
    DECLARE @BI_DATA NVARCHAR(MAX)
    DECLARE @BI_FEC_MOV Fecha
    DECLARE @PR_MONTO Pesos 
	declare @PR_SALDO_DEF Pesos	
    declare @PR_FECHA Fecha 
	declare @FormulaAnterior Observaciones
	declare @FormulaActual Observaciones

	set @PR_SALDO_DEF = 9999999.00

	select @PR_FECHA = PR_FECHA from PRESTAMO where CB_CODIGO = @CB_CODIGO and PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN 
	
	declare @year int 
	declare @mes int 

	select top 1  @year = PF_YEAR , @mes = PF_MES  from PCED_FON where CB_CODIGO = @CB_CODIGO and PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN 
	order by PF_YEAR, PF_MES 

	declare @Fecha_Inicio Fecha 

	SELECT @Fecha_Inicio = PE_FEC_INI FROM PERIODO P INNER JOIN	FN_FONACOT_PERIODOS (@Year, @Mes, (SELECT CB_NOMINA FROM COLABORA WHERE CB_CODIGO = @CB_CODIGO)) FP
		ON P.PE_NUMERO = FP.PE_NUMERO
		where
			PE_YEAR = @Year and PE_MES_FON = @Mes 
			AND PE_TIPO = (SELECT CB_NOMINA FROM COLABORA WHERE CB_CODIGO = @CB_CODIGO)
			AND PE_POS_FON = 1

	set @Fecha_Inicio = coalesce( @Fecha_Inicio, @PR_FECHA ) 
 
	set @PR_MONTO = @PR_SALDO_DEF

	set @FormulaAnterior = (select PR_FORMULA from PRESTAMO  where CB_CODIGO = @CB_CODIGO and PR_REFEREN = @PR_REFEREN and PR_TIPO = @PR_TIPO);  
 
    update PRESTAMO set PR_FECHA = @Fecha_Inicio, PR_STATUS = @iPR_STATUS,  US_CODIGO= @iUS_CODIGO ,  PR_MONTO = @PR_MONTO, PR_TOTAL = @PR_MONTO, PR_SALDO = @PR_MONTO, PR_PAG_PER = @PR_PAG_PER , PR_FORMULA = '', 
        PR_FON_MOT =  case                                       
                    when @iPR_STATUS = 1 
					then 3 
                    else
						0 
					end                                               
    where CB_CODIGO = @CB_CODIGO and PR_REFEREN = @PR_REFEREN and PR_TIPO = @PR_TIPO 
                    
	set @FormulaActual = (select PR_FORMULA from PRESTAMO  where CB_CODIGO = @CB_CODIGO and PR_REFEREN = @PR_REFEREN and PR_TIPO = @PR_TIPO); 
    if ( @lGrabarBitacora  = 'S' ) 
    begin 
            SET @BI_TEXTO =  'Modificaci�n desde C�dula Fonacot: Ref:' + @PR_REFEREN;
            SET @BI_DATA = 'Pr�stamo: Empl.:' + cast (@CB_CODIGO as varchar) + ' Tipo:' + @PR_TIPO + ' Ref:' + @PR_REFEREN + CHAR(13)+CHAR(10) + 'PR_STATUS' + CHAR(13)+CHAR(10) + ' A : ' + cast (@iPR_STATUS as varchar)
    
            SELECT @BI_FEC_MOV = GETDATE();
            exec SP_INSERTAR_BITACORA @iUS_CODIGO, 0, 0, 0, @BI_TEXTO, @CB_CODIGO, @BI_DATA, 62, @BI_FEC_MOV;

	        if (@FormulaAnterior <> '')
			begin
				--PARA ESPECIFICAR ANTES Y DESPUES DE FORMULA
				SET @BI_TEXTO =  'Modificaci�n de Pr�stamo: Empl.:'+cast (@CB_CODIGO as varchar) +' Tipo:'+@PR_TIPO+' Ref:' + @PR_REFEREN;
				SET @BI_DATA = 'Pr�stamo: Empl.:' + cast (@CB_CODIGO as varchar) + ' Tipo:' + @PR_TIPO + ' Ref:' + @PR_REFEREN + CHAR(13)+CHAR(10) + ' PR_FORMULA : ' + CHAR(13)+CHAR(10) + ' De: '+ @FormulaAnterior + CHAR(13)+CHAR(10) +' A: '+@FormulaActual   
				SELECT @BI_FEC_MOV = GETDATE();
				exec SP_INSERTAR_BITACORA @iUS_CODIGO, 0, 0, 0, @BI_TEXTO, @CB_CODIGO, @BI_DATA, 62, @BI_FEC_MOV;
			end;
    end;
end
GO

IF EXISTS ( SELECT  * FROM  sys.objects WHERE   object_id = OBJECT_ID(N'FN_FONACOT_PERIODOS') AND type IN ( N'TF' ))
    drop function FN_FONACOT_PERIODOS
go

CREATE FUNCTION FN_FONACOT_PERIODOS
	(@AnioPeriodo Anio, @MesPeriodo Mes, @TipoPeriodo NominaTipo)
RETURNS @Valores TABLE
	(
	   PE_NUMERO Smallint,
	   PE_POS_FON Smallint,	   
       PE_USO Smallint 
	)
AS
BEGIN
	DECLARE @PE_POS_FON Dias

	insert into @Valores (PE_NUMERO, PE_POS_FON, PE_USO)
	SELECT PE_NUMERO, ROW_NUMBER () OVER (ORDER BY PE_NUMERO ASC), PE_USO
	FROM PERIODO WHERE PE_YEAR = @AnioPeriodo AND PE_MES_FON = @MesPeriodo  AND PE_TIPO = @TipoPeriodo AND PE_PRESTAM = 'S'
	GROUP BY PE_NUMERO, PE_USO
	ORDER BY PE_NUMERO ASC
	
	RETURN 
END
GO

IF EXISTS ( SELECT  * FROM  sys.objects WHERE   object_id = OBJECT_ID(N'FN_CALCULA_FON') AND type IN ( N'TF' ))
    drop function FN_CALCULA_FON
go

CREATE FUNCTION FN_CALCULA_FON
	(@CB_CODIGO NumeroEmpleado, @PR_REFEREN Referencia, @PR_TIPO Codigo1, 
	@TipoAjuste NumeroEmpleado, @AustarIncluyendoIncapacitados Booleano, 
	@SaldoPrestamoAjuste Decimal(15,2),
	@IncapacidadDias NumeroEmpleado, @IncapacidadesMes NumeroEmpleado, @AjusteMesAnterior Booleano,
	@AnioPeriodo Anio, @MesPeriodoFon Mes, @NumPeriodo NominaNumero, @TipoPeriodo NominaTipo)
RETURNS @Valores TABLE
	(
	   RETENCION_PERIODO Decimal(15,2), 
	   MONTO_CEDULA   Decimal(15,2), 
	   MONTO_RETENIDO Decimal(15,2), 
       MONTO_ESPERADO Decimal(15,2), 
       AJUSTE Decimal(15,2), 
       MONTO_CONCEPTO Decimal(15,2) ,
       DEUDA_MA Decimal(15,2) ,
	   NUM_PERIODO_MES int,
	   POS_PERIODO_MES int,
	   MES_ANTERIOR    int, 
	   YEAR_ANTERIOR   int 
	)
as
begin 
		declare @RETENCION_PERIODO Pesos 
		declare @MONTO_CEDULA   Pesos 
		declare @MONTO_RETENIDO Pesos 
		declare @MONTO_ESPERADO Pesos 
		declare @AJUSTE Pesos 
		declare @AJUSTE_BASE Pesos 
		declare @MONTO_CONCEPTO Pesos 
		declare @DEUDA_MA Pesos 
		DECLARE @NUM_PERIODO_MES Empleados
		DECLARE @POS_PERIODO_MES Empleados
		DECLARE @MES_ANTERIOR Status
		DECLARE @YEAR_ANTERIOR Anio
		DECLARE @PE_FEC_INI Fecha
		DECLARE @PE_FEC_FIN Fecha
		DECLARE @PE_USO Status
   	   
		SET @AJUSTE = 0;		
		SET @AJUSTE_BASE = 0;

		-- OBTENER INCAPACIDADES.
		IF @AustarIncluyendoIncapacitados = 'S' OR @TipoAjuste = 0
		BEGIN
			SET @IncapacidadesMes = 0
			SET @IncapacidadDias = 0
		END
		ELSE IF @AustarIncluyendoIncapacitados = 'N' AND @TipoAjuste > 0
		BEGIN
			-- OBTENER LOS D�AS DE INCAPACIDAD DE LOS PER�ODOS ANTERIORES			
			IF @IncapacidadDias < 0
			BEGIN
				SELECT @IncapacidadDias = NO_DIAS_IN FROM NOMINA WHERE CB_CODIGO = @CB_CODIGO
					AND PE_YEAR = @AnioPeriodo AND PE_NUMERO = @NumPeriodo AND PE_TIPO = @TipoPeriodo
			END

			IF @IncapacidadesMes < 0
			BEGIN
				SELECT @PE_FEC_INI = (MIN (PE_FEC_INI)), @PE_FEC_FIN =  (MAX (PE_FEC_FIN)) FROM PERIODO
					WHERE PE_NUMERO IN (SELECT PE_NUMERO FROM FN_FONACOT_PERIODOS (@AnioPeriodo, @MesPeriodoFon, @TipoPeriodo))
						AND PE_YEAR = @AnioPeriodo AND PE_MES_FON = @MesPeriodoFon AND PE_TIPO = @TipoPeriodo AND PE_PRESTAM = 'S'
			
				SELECT @IncapacidadesMes = COALESCE (SUM (IN_DIAS), 0) FROM INCAPACI WHERE CB_CODIGO = @CB_CODIGO					
					AND ((IN_FEC_INI >= @PE_FEC_INI AND IN_FEC_INI <= @PE_FEC_FIN ) OR (IN_FEC_FIN <= @PE_FEC_FIN AND IN_FEC_FIN > @PE_FEC_INI ))
			END				
		END
		-- ====================================================================================================================================

		-- OBTENER N�MERO DE PERIODO Y LA CANTIDAD DE PERIODOS DEL MES.
		SELECT @POS_PERIODO_MES = PE_POS_FON, @PE_USO = PE_USO FROM FN_FONACOT_PERIODOS (@AnioPeriodo, @MesPeriodoFon, @TipoPeriodo)
			WHERE PE_NUMERO = @NumPeriodo

		IF @PE_USO = 1
		BEGIN
			SELECT @MONTO_CEDULA = PF_PAGO,  @MONTO_RETENIDO = PF_NOMINA, @POS_PERIODO_MES = PF_NOM_QTY + 1 FROM VCED_FON WHERE CB_CODIGO = @CB_CODIGO and PF_YEAR = @AnioPeriodo AND PF_MES = @MesPeriodoFon AND PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN
		END
		ELSE
		BEGIN
			SELECT @MONTO_CEDULA = PF_PAGO,  @MONTO_RETENIDO = PF_NOMINA FROM VCED_FON WHERE CB_CODIGO = @CB_CODIGO and PF_YEAR = @AnioPeriodo AND PF_MES = @MesPeriodoFon AND PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN
			set @MONTO_RETENIDO = coalesce( @MONTO_RETENIDO, 0.00 )
		END

		-- Cantidad de periodos Fonacot.
		SELECT @NUM_PERIODO_MES = COUNT(PE_NUMERO) FROM PERIODO
		WHERE PE_YEAR = @AnioPeriodo AND PE_MES_FON = @MesPeriodoFon AND PE_TIPO = @TipoPeriodo AND PE_PRESTAM = 'S' AND PE_USO = 0

		if (@MesPeriodoFon = 1 )
		begin
			set @MES_ANTERIOR = 12
			set @YEAR_ANTERIOR = @AnioPeriodo - 1;
		end
		else
		begin
			set @MES_ANTERIOR = @MesPeriodoFon - 1;
			set @YEAR_ANTERIOR = @AnioPeriodo;
		end		

		-- SI EL TIPO DE AJUSTE ES  1 (DURANTE EL MES, ES DECIR, SUAVIZADO) � 2 (AJUSTAR CADA PERIODO) Y SE PIDE AJUSTAR EL MES ANTERIOR,
		-- OBTENER AJUSTE DEL MES ANTERIOR.
		IF @AjusteMesAnterior = 'S'
		BEGIN
			SELECT @DEUDA_MA = (PF_PAGO-PF_NOMINA) FROM VCED_FON
				WHERE CB_CODIGO = @CB_CODIGO and PF_YEAR = @YEAR_ANTERIOR AND PF_MES = @MES_ANTERIOR  AND PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN 
			
			SET @DEUDA_MA = COALESCE (@DEUDA_MA, 0.000);
		END
		ELSE
			SET @DEUDA_MA = 0.000;
	    
		-- AJUSTE.
		-- 1. SUAVIZADO, ES DECIR, DURANTE EL MES.
		-- 2. AJUSTA CADA PER�ODO.
		-- 3. AJUSTA AL FINAL DE MES (�LTIMO PER�ODO).
	   
		-- OBTENER LA RETENCI�N POR PERIODO.
		IF @NUM_PERIODO_MES != 0
		BEGIN
			SELECT @RETENCION_PERIODO = ((PF_PAGO+@DEUDA_MA)/@NUM_PERIODO_MES) FROM PCED_FON
				WHERE CB_CODIGO = @CB_CODIGO and PF_YEAR = @AnioPeriodo AND PF_MES = @MesPeriodoFon AND PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN ;
			SET @RETENCION_PERIODO = coalesce( @RETENCION_PERIODO , 0.000 );
		END
		ELSE
		BEGIN
			SET @RETENCION_PERIODO = 0.0;
		END
		
		-- OBTENER MONTO ESPERADO.		
		IF ((@TipoAjuste = 1 OR @TipoAjuste = 2) OR (@TipoAjuste = 3 AND @POS_PERIODO_MES = @NUM_PERIODO_MES)) 			
			AND (@IncapacidadDias = 0) AND (@IncapacidadesMes = 0 )
		BEGIN
			SET @MONTO_ESPERADO = (@POS_PERIODO_MES - 1)*@RETENCION_PERIODO;			

			-- OBTENER EL AJUSTE,
			-- ES DECIR, LA DIFERENCIA ENTRE LO QUE DEBER�A LLEVAR DESCONTADO HASTA EL MOMENTO (@MONTO_ESPERADO)
			-- Y LO QUE EN REALIDAD LLEVA (VCED_FON.PF_NOMINA)	

			-- AJUSTE SUAVIZADO, ES DECIR, DURANTE LOS PER�ODOS QUE RESTAN EN EL MES.
			SET @AJUSTE_BASE = @MONTO_ESPERADO - @MONTO_RETENIDO;

			IF ( @SaldoPrestamoAjuste > 0 ) 
			BEGIN
			  SET @AJUSTE_BASE =  @AJUSTE_BASE - @SaldoPrestamoAjuste 
			END
 

            IF (@TipoAjuste = 1)
            BEGIN
                SET @AJUSTE = (@AJUSTE_BASE ) / ((@NUM_PERIODO_MES-@POS_PERIODO_MES)+1)
            END
            -- AJUSTE CADA PER�ODO O AL FINAL DEL MES.
            ELSE IF (@TipoAjuste = 2 OR @TipoAjuste = 3)
            BEGIN
                SET @AJUSTE = (@AJUSTE_BASE ) 
            END

			SET @AJUSTE = COALESCE (@AJUSTE, 0.000);
		END
		ELSE
		BEGIN
			SET @MONTO_ESPERADO = 0.000;
			SET @AJUSTE = 0.000;
		END	   
	   
		SET @MONTO_CONCEPTO = @RETENCION_PERIODO + @AJUSTE		 

		IF @POS_PERIODO_MES = @NUM_PERIODO_MES
		BEGIN
			IF ABS (@MONTO_CEDULA - (@MONTO_RETENIDO + @MONTO_CONCEPTO)) < 1
			BEGIN
				SET @MONTO_CONCEPTO = @MONTO_CONCEPTO + (@MONTO_CEDULA - (@MONTO_RETENIDO + @MONTO_CONCEPTO))				
			END
		END

		insert into @Valores (
				RETENCION_PERIODO, 
				MONTO_CEDULA, 
				MONTO_RETENIDO, 
				MONTO_ESPERADO, 
				AJUSTE, 
				MONTO_CONCEPTO ,
				DEUDA_MA, 
				NUM_PERIODO_MES,
				POS_PERIODO_MES,
				MES_ANTERIOR ,  
				YEAR_ANTERIOR
		) values ( 
				@RETENCION_PERIODO, 
				@MONTO_CEDULA, 
				@MONTO_RETENIDO, 
				@MONTO_ESPERADO, 
				@AJUSTE, 
				@MONTO_CONCEPTO ,
				@DEUDA_MA, 
				@NUM_PERIODO_MES,
				@POS_PERIODO_MES,
				@MES_ANTERIOR ,  
				@YEAR_ANTERIOR
		)

		RETURN 
end
GO

IF EXISTS ( SELECT  * FROM  sys.objects WHERE   object_id = OBJECT_ID(N'SP_DASHLET_GENERAL_CUMPLEANOS') AND type IN ( N'P', N'PC' ))
    drop procedure SP_DASHLET_GENERAL_CUMPLEANOS
go

create procedure SP_DASHLET_GENERAL_CUMPLEANOS ( @CM_NIVEL0 Formula ) 
as
begin 
    declare @Fecha Fecha 
    
    set @Fecha = getdate()         
    set @Fecha = DATEADD(dd, 0, DATEDIFF(dd, 0,@Fecha))

      declare @MesVar varchar(20) 
    set @MesVar = dbo.SP_DASHLET_GET_MES_DESC( MONTH(@Fecha) ) 

    declare @lunes datetime
    set @lunes = '17530101'

    declare @FecIni Fecha 
    declare @FecFin Fecha 

    set @FecIni = @lunes + datediff(day,@lunes,@Fecha)/7*7 
    set @FecFin = dateadd(day, 6, @FecIni)

    select CB_CODIGO Empleado, dbo.fnConvert_TitleCase(REPLACE(PRETTYNAME, ',', '')) Nombre, dbo.SP_DASHLET_GET_MES_DESC( MONTH(CB_FEC_NAC) ) as Mes,  DAY(CB_FEC_NAC) as [D_ia] from COLABORA 
    where 
        dateadd( year, datediff( year, CB_FEC_NAC, @Fecha+7 ), CB_FEC_NAC)  between @FecIni  and @FecFin and 
        dbo.SP_STATUS_ACT(@Fecha, CB_CODIGO ) = 1   and (@CM_NIVEL0 = ''  or ( COLABORA.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  
        or (@CM_NIVEL0 = '' and COLABORA.CB_NIVEL0=''))    
    order by dateadd( year, datediff( year, CB_FEC_NAC, @Fecha+7 ), CB_FEC_NAC) 

end
go 

IF EXISTS ( SELECT  * FROM  sys.objects WHERE   object_id = OBJECT_ID(N'SP_DASHLET_GENERAL_ANIVERSARIOS') AND type IN ( N'P', N'PC' ))
    drop procedure SP_DASHLET_GENERAL_ANIVERSARIOS
go

create procedure SP_DASHLET_GENERAL_ANIVERSARIOS ( @CM_NIVEL0 Formula ) 
as
begin 
    declare @Fecha Fecha 
    set @Fecha = getdate() 
    set @Fecha = DATEADD(dd, 0, DATEDIFF(dd, 0,@Fecha))
    
    
    declare @lunes datetime
    set @lunes = '17530101'

    declare @FecIni Fecha 
    declare @FecFin Fecha 

    set @FecIni = @lunes + datediff(day,@lunes,@Fecha)/7*7 
    set @FecFin = dateadd(day, 6, @FecIni)
    
    
    select CB_CODIGO Empleado, dbo.fnConvert_TitleCase(REPLACE(PRETTYNAME, ',', '')) Nombre, dbo.SP_DASHLET_GET_MES_DESC( MONTH(CB_FEC_ANT) ) as Mes,  DAY(CB_FEC_ANT) as [D_ia], Datediff(year, CB_FEC_ANT, @Fecha) as [Antig__uedad]   from COLABORA 
    where 
        dateadd( year, datediff( year, CB_FEC_ANT, @Fecha+7 ), CB_FEC_ANT)  between @FecIni  and @FecFin and  Datediff(year, CB_FEC_ANT, @Fecha) > 0 and 
        dbo.SP_STATUS_ACT(@Fecha, CB_CODIGO ) = 1   and (@CM_NIVEL0 = ''  or ( COLABORA.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  
        or (@CM_NIVEL0 = '' and COLABORA.CB_NIVEL0=''))    
    order by dateadd( year, datediff( year, CB_FEC_ANT, @Fecha+7 ), CB_FEC_ANT) 
end
go