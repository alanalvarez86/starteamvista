/***************************************/
/* Creaci�n de tablas nuevas Patch 382 */
/***************************************/

/***************************************/
/****  Tablas  *************************/
/***************************************/

/* Tabla MOTSCRAP: Motivos de Scrap */
/* Patch 382 # Seq: 1 Agregado */
CREATE TABLE MOTSCRAP (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion )
GO

/* PK MOTSCRAP: Nuevo */
/* Patch 382 # Seq: 2 Agregado */
ALTER TABLE MOTSCRAP
      ADD CONSTRAINT PK_MOTSCRAP PRIMARY KEY (TB_CODIGO)
GO

/* Tabla COMPONEN: Componentes (materiales para PARTES) */
/* Patch 382 # Seq: 3 Agregado */
CREATE TABLE COMPONEN (
       CN_CODIGO            CodigoParte,
       CN_NOMBRE            DescLarga,
       CN_SHORT             DescCorta,
       CN_BARCODE           DescCorta,
       CN_INGLES            Descripcion,
       CN_COSTO             Pesos,
       CN_UNIDAD            Referencia,
       CN_DETALLE           Formula,
       CN_NUMERO1           Pesos,
       CN_NUMERO2           Pesos,
       CN_TEXTO1            Descripcion,
       CN_TEXTO2            Descripcion )
GO

/* PK COMPONEN: Nuevo */
/* Patch 382 # Seq: 4 Agregado */
ALTER TABLE COMPONEN
      ADD CONSTRAINT PK_COMPONEN PRIMARY KEY (CN_CODIGO)
GO

/* Tabla CEDSCRAP: C�dulas de Scrap */
/* Patch 382 # Seq: 5 Agregado */
CREATE TABLE CEDSCRAP (
       CS_FOLIO             FolioGrande,
       CS_AREA              Codigo,
       CS_FECHA             Fecha,
       CS_HORA              Hora,
       WO_NUMBER            OrdenTrabajo,
       AR_CODIGO            CodigoParte,
       OP_NUMBER            Operacion,
       CS_FEC_FAB           Fecha,
       CS_TAMLOTE           Piezas,
       CS_COMENTA           DescLarga,
       CS_OBSERVA           Memo,
       CS_NUMERO1           Pesos,
       CS_NUMERO2           Pesos,
       CS_TEXTO1            Descripcion,
       CS_TEXTO2            Descripcion,
       US_CODIGO            Usuario )
GO

/* PK CEDSCRAP: Nuevo */
/* Patch 382 # Seq: 6 Agregado */
ALTER TABLE CEDSCRAP
      ADD CONSTRAINT PK_CEDSCRAP PRIMARY KEY (CS_FOLIO)
GO

/* Tabla SCRAP: Material de Scrap: Detalle de la c�dula de Scrap */
/* Patch 382 # Seq: 7 Agregado */
CREATE TABLE SCRAP (
       CS_FOLIO             FolioGrande,
       SC_FOLIO             FolioChico,
       CN_CODIGO            CodigoParte,
       SC_MOTIVO            Codigo,
       SC_PIEZAS            Piezas,
       SC_COMENTA           DescLarga )
GO

/* PK SCRAP: Nuevo */
/* Patch 382 # Seq: 8 Agregado */
ALTER TABLE SCRAP
      ADD CONSTRAINT PK_SCRAP PRIMARY KEY (CS_FOLIO, SC_FOLIO)
GO

/* FK SCRAP -> CEDSCRAP: Nuevo */
/* Patch 382 # Seq: 9 Agregado */
ALTER TABLE SCRAP
       ADD CONSTRAINT FK_SCRAP_CEDSCRAP
       FOREIGN KEY (CS_FOLIO)
       REFERENCES CEDSCRAP
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

/* FK SCRAP -> MOTSCRAP: Nuevo */
/* Patch 382 # Seq: 10 Agregado */
ALTER TABLE SCRAP
       ADD CONSTRAINT FK_SCRAP_MOTSCRAP
       FOREIGN KEY (SC_MOTIVO)
       REFERENCES MOTSCRAP
       ON DELETE NO ACTION
       ON UPDATE CASCADE;
GO

/* FK SCRAP -> COMPONEN: Nuevo */
/* Patch 382 # Seq: 11 Agregado */
ALTER TABLE SCRAP
       ADD CONSTRAINT FK_SCRAP_COMPONEN
       FOREIGN KEY (CN_CODIGO)
       REFERENCES COMPONEN
       ON DELETE NO ACTION
       ON UPDATE CASCADE;
GO

/*****************************************/
/**** Views ******************************/
/*****************************************/

/* VIEW DEFECT_V: Nuevo */
/* Patch 382 # Seq: 12 Agregado */
CREATE VIEW DEFECT_V(
       CI_FOLIO,
       DE_FOLIO,
       DE_CODIGO,
       DE_PIEZAS,
       DE_COMENTA,
       CI_AREA,
       CE_FOLIO,
       WO_NUMBER,
       AR_CODIGO,
       CI_FECHA,
       CI_HORA,
       US_CODIGO,
       CI_TIPO,
       CI_RESULT,
       CI_COMENTA,
       CI_OBSERVA,
       CI_TAMLOTE,
       CI_MUESTRA,
       CI_TIEMPO,
       CI_NUMERO1,
       CI_NUMERO2 ) as
select D.CI_FOLIO,
       D.DE_FOLIO,
       D.DE_CODIGO,
       D.DE_PIEZAS,
       D.DE_COMENTA,
       C.CI_AREA,
       C.CE_FOLIO,
       C.WO_NUMBER,
       C.AR_CODIGO,
       C.CI_FECHA,
       C.CI_HORA,
       C.US_CODIGO,
       C.CI_TIPO,
       C.CI_RESULT,
       C.CI_COMENTA,
       C.CI_OBSERVA,                
       C.CI_TAMLOTE,
       C.CI_MUESTRA,
       C.CI_TIEMPO,
       C.CI_NUMERO1,
       C.CI_NUMERO2
from DEFECTO D left outer join CED_INSP C on ( C.CI_FOLIO = D.CI_FOLIO )
GO

/* VIEW SCRAP_V: Nuevo */
/* Patch 382 # Seq: 13 Agregado */
create view SCRAP_V(
       CS_FOLIO,
       SC_FOLIO,
       CN_CODIGO,
       SC_MOTIVO,
       DE_PIEZAS,
       DE_COMENTA,
       CS_AREA,
       CS_FECHA,
       CS_HORA,
       WO_NUMBER,
       AR_CODIGO,
       OP_NUMBER,
       CS_FEC_FAB,
       CS_TAMLOTE,
       CS_COMENTA,
       CS_OBSERVA,
       CS_NUMERO1,
       CS_NUMERO2,
       CS_TEXTO1,
       CS_TEXTO2,
       US_CODIGO  ) as
select S.CS_FOLIO,
       S.SC_FOLIO,
       S.CN_CODIGO,
       S.SC_MOTIVO,
       S.SC_PIEZAS,
       S.SC_COMENTA,
       C.CS_AREA,
       C.CS_FECHA,
       C.CS_HORA,
       C.WO_NUMBER,
       C.AR_CODIGO,
       C.OP_NUMBER,
       C.CS_FEC_FAB,
       C.CS_TAMLOTE,
       C.CS_COMENTA,
       C.CS_OBSERVA,
       C.CS_NUMERO1,
       C.CS_NUMERO2,
       C.CS_TEXTO1,
       C.CS_TEXTO2,
       C.US_CODIGO
from SCRAP S left outer join CEDSCRAP C on ( C.CS_FOLIO = S.CS_FOLIO )
GO

/*****************************************/
/****  Triggers  *************************/
/*****************************************/

/* Hay que cambiar triggers TU_PARTES, TU_AREA, TU_WORDER, TU_OPERA  para considerar CEDSCRAP */

/* TU_PARTES: Considerar tabla CEDSCRAP */
/* Patch 382 # Seq: 36 Modificado */
ALTER TRIGGER TU_PARTES ON PARTES AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( AR_CODIGO )
  BEGIN
	declare @NewCodigo CodigoParte, @OldCodigo CodigoParte;

	select @NewCodigo = AR_CODIGO from   Inserted;
    select @OldCodigo = AR_CODIGO from   Deleted;

	UPDATE WORKS	SET AR_CODIGO = @NewCodigo WHERE ( AR_CODIGO = @OldCodigo );
	UPDATE WORDER	SET AR_CODIGO = @NewCodigo WHERE ( AR_CODIGO = @OldCodigo );
	UPDATE CEDULA	SET AR_CODIGO = @NewCodigo WHERE ( AR_CODIGO = @OldCodigo );
	UPDATE CED_INSP	SET AR_CODIGO = @NewCodigo WHERE ( AR_CODIGO = @OldCodigo );
    UPDATE CEDSCRAP SET AR_CODIGO = @NewCodigo WHERE ( AR_CODIGO = @OldCodigo );
  END
END
GO

/* TU_AREA: Considerar tabla CEDSCRAP */
/* Patch 382 # Seq: 47 Modificado */
ALTER TRIGGER TU_AREA ON AREA AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo CODIGO, @OldCodigo CODIGO;

	select @NewCodigo = TB_CODIGO from Inserted;
    select @OldCodigo = TB_CODIGO from Deleted;

    update SUP_AREA set CB_AREA = @NewCodigo where ( CB_AREA = @OldCodigo );
    update KAR_AREA set CB_AREA = @NewCodigo where ( CB_AREA = @OldCodigo );
    update COLABORA set CB_AREA = @NewCodigo where ( CB_AREA = @OldCodigo );
    update WORKS    set CB_AREA = @NewCodigo where ( CB_AREA = @OldCodigo );
    update CEDULA   set CE_AREA = @NewCodigo where ( CE_AREA = @OldCodigo );
    update CED_INSP set CI_AREA = @NewCodigo where ( CI_AREA = @OldCodigo );
	update CEDSCRAP set CS_AREA = @NewCodigo where ( CS_AREA = @OldCodigo );
  END
END
GO

/* TU_WORDER: Considerar tabla CEDSCRAP */
/* Patch 382 # Seq: 39 Modificado */
ALTER TRIGGER TU_WORDER ON WORDER AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( WO_NUMBER )
  BEGIN
	declare @NewCodigo OrdenTrabajo, @OldCodigo OrdenTrabajo;

	select @NewCodigo = WO_NUMBER from   Inserted;
    select @OldCodigo = WO_NUMBER from   Deleted;

	UPDATE WORKS    SET WO_NUMBER = @NewCodigo WHERE ( WO_NUMBER = @OldCodigo );
	UPDATE CEDULA   SET WO_NUMBER = @NewCodigo WHERE ( WO_NUMBER = @OldCodigo );
	UPDATE CED_WORD SET WO_NUMBER = @NewCodigo WHERE ( WO_NUMBER = @OldCodigo );
	UPDATE CED_INSP	SET WO_NUMBER = @NewCodigo WHERE ( WO_NUMBER = @OldCodigo );
  	update CEDSCRAP SET WO_NUMBER = @NewCodigo WHERE ( WO_NUMBER = @OldCodigo );
  END
END
GO

/* TU_OPERA: Considerar tabla CEDSCRAP */
/* Patch 382 # Seq: 38 Modificado */
ALTER TRIGGER TU_OPERA ON OPERA AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( OP_NUMBER )
  BEGIN
	declare @NewCodigo Operacion, @OldCodigo Operacion;

	select @NewCodigo = OP_NUMBER from   Inserted;
    select @OldCodigo = OP_NUMBER from   Deleted;

	UPDATE WORKS	SET OP_NUMBER = @NewCodigo WHERE ( OP_NUMBER = @OldCodigo );
	UPDATE STEPS	SET OP_NUMBER = @NewCodigo WHERE ( OP_NUMBER = @OldCodigo );
	UPDATE DEFSTEPS	SET OP_NUMBER = @NewCodigo WHERE ( OP_NUMBER = @OldCodigo );
	UPDATE CEDULA	SET OP_NUMBER = @NewCodigo WHERE ( OP_NUMBER = @OldCodigo );
	UPDATE AREA	    SET TB_OPERA  = @NewCodigo WHERE ( TB_OPERA  = @OldCodigo );
   	update CEDSCRAP SET OP_NUMBER = @NewCodigo WHERE ( OP_NUMBER = @OldCodigo );
  END
END
GO

/**************************************************/
/* STORED PROCEDURES ******************************/
/**************************************************/

/* Patch 382: Agregar SCRAP ( 191 ) a lista de Entidades */
/* Patch 382: # Seq: 185 Modificado */
ALTER PROCEDURE DBO.SP_TITULO_CAMPO(
	@NUMGLOBAL SMALLINT,
	@TITULODEFAULT VARCHAR(30),
	@NOMBRE VARCHAR(30))
AS
BEGIN
     SET NOCOUNT ON;
     DECLARE @TITULO VARCHAR(30);
     select @Titulo = GL_FORMULA from GLOBAL where ( GL_CODIGO = @NumGlobal );
     if ( ( @Titulo is Null ) or ( @Titulo = '' ) )
        SET @Titulo = @TituloDefault;
     update DICCION set DI_TITULO = @Titulo,
                        DI_TCORTO= @Titulo,
                        DI_CLAVES = ''
     where ( DI_CLASIFI in ( 5, 10, 18, 32, 33, 50, 59, 124, 141, 142, 143, 144, 145, 146, 147, 148, 149, 152, 186, 187, 191 ) )
     and ( DI_NOMBRE = @Nombre );
END
GO

/* Patch 382: Agregar llamadas a SP_TITULO_CAMPO por campo de SCRAP.CS_AREA */
/* Patch 382 # Seq: 195 Modificado */
ALTER PROCEDURE DBO.SP_REFRESH_DICCION
as
begin
  SET NOCOUNT ON
  execute  SP_TITULO_TABLA 13, 'Nivel de Organigrama #1', 41 ;
  execute  SP_TITULO_TABLA 14, 'Nivel de Organigrama #2', 42 ;
  execute  SP_TITULO_TABLA 15, 'Nivel de Organigrama #3', 43 ;
  execute  SP_TITULO_TABLA 16, 'Nivel de Organigrama #4', 44 ;
  execute  SP_TITULO_TABLA 17, 'Nivel de Organigrama #5', 45 ;
  execute  SP_TITULO_TABLA 18, 'Nivel de Organigrama #6', 46 ;
  execute  SP_TITULO_TABLA 19, 'Nivel de Organigrama #7', 47 ;
  execute  SP_TITULO_TABLA 20, 'Nivel de Organigrama #8', 48 ;
  execute  SP_TITULO_TABLA 21, 'Nivel de Organigrama #9', 49 ;

  execute  SP_TITULO_TABLA 35, 'Tabla Adicional #1', 19
  execute  SP_TITULO_TABLA 36, 'Tabla Adicional #2', 20
  execute  SP_TITULO_TABLA 37, 'Tabla Adicional #3', 21
  execute  SP_TITULO_TABLA 38, 'Tabla Adicional #4', 22

  execute  SP_TITULO_TABLA 134, 'Orden de Trabajo', 149
  execute  SP_TITULO_TABLA 136, 'Cat�logo de Partes', 141
  execute  SP_TITULO_TABLA 138, 'Cat�logo de Area', 148
  execute  SP_TITULO_TABLA 140, 'Cat�logo de Operaciones', 142
  execute  SP_TITULO_TABLA 142, 'Cat�logo de Tiempo Muerto', 138
  execute  SP_TITULO_TABLA 176, 'Lista de Defectos', 187
  execute  SP_TITULO_TABLA 143, 'Modulador #1', 135
  execute  SP_TITULO_TABLA 144, 'Modulador #2', 136
  execute  SP_TITULO_TABLA 145, 'Modulador #3', 137

  execute  SP_TITULO_CAMPO 13, 'C�digo de Nivel #1', 'CB_NIVEL1'
  execute  SP_TITULO_CAMPO 14, 'C�digo de Nivel #2', 'CB_NIVEL2'
  execute  SP_TITULO_CAMPO 15, 'C�digo de Nivel #3', 'CB_NIVEL3'
  execute  SP_TITULO_CAMPO 16, 'C�digo de Nivel #4', 'CB_NIVEL4'
  execute  SP_TITULO_CAMPO 17, 'C�digo de Nivel #5', 'CB_NIVEL5'
  execute  SP_TITULO_CAMPO 18, 'C�digo de Nivel #6', 'CB_NIVEL6'
  execute  SP_TITULO_CAMPO 19, 'C�digo de Nivel #7', 'CB_NIVEL7'
  execute  SP_TITULO_CAMPO 20, 'C�digo de Nivel #8', 'CB_NIVEL8'
  execute  SP_TITULO_CAMPO 21, 'C�digo de Nivel #9', 'CB_NIVEL9'

  execute  SP_TITULO_CAMPO 13, 'Nuevo Nivel #1', 'EV_NIVEL1'
  execute  SP_TITULO_CAMPO 14, 'Nuevo Nivel #2', 'EV_NIVEL2'
  execute  SP_TITULO_CAMPO 15, 'Nuevo Nivel #3', 'EV_NIVEL3'
  execute  SP_TITULO_CAMPO 16, 'Nuevo Nivel #4', 'EV_NIVEL4'
  execute  SP_TITULO_CAMPO 17, 'Nuevo Nivel #5', 'EV_NIVEL5'
  execute  SP_TITULO_CAMPO 18, 'Nuevo Nivel #6', 'EV_NIVEL6'
  execute  SP_TITULO_CAMPO 19, 'Nuevo Nivel #7', 'EV_NIVEL7'
  execute  SP_TITULO_CAMPO 20, 'Nuevo Nivel #8', 'EV_NIVEL8'
  execute  SP_TITULO_CAMPO 21, 'Nuevo Nivel #9', 'EV_NIVEL9'

  execute  SP_TITULO_CAMPO 13, 'C�digo de Nivel #1', 'PU_NIVEL1'
  execute  SP_TITULO_CAMPO 14, 'C�digo de Nivel #2', 'PU_NIVEL2'
  execute  SP_TITULO_CAMPO 15, 'C�digo de Nivel #3', 'PU_NIVEL3'
  execute  SP_TITULO_CAMPO 16, 'C�digo de Nivel #4', 'PU_NIVEL4'
  execute  SP_TITULO_CAMPO 17, 'C�digo de Nivel #5', 'PU_NIVEL5'
  execute  SP_TITULO_CAMPO 18, 'C�digo de Nivel #6', 'PU_NIVEL6'
  execute  SP_TITULO_CAMPO 19, 'C�digo de Nivel #7', 'PU_NIVEL7'
  execute  SP_TITULO_CAMPO 20, 'C�digo de Nivel #8', 'PU_NIVEL8'
  execute  SP_TITULO_CAMPO 21, 'C�digo de Nivel #9', 'PU_NIVEL9'

  execute  SP_TITULO_CAMPO 22, 'Fecha Adicional #1', 'CB_G_FEC_1'
  execute  SP_TITULO_CAMPO 23, 'Fecha Adicional #2', 'CB_G_FEC_2'
  execute  SP_TITULO_CAMPO 24, 'Fecha Adicional #3', 'CB_G_FEC_3'
  execute  SP_TITULO_CAMPO 25, 'Texto Adicional #1', 'CB_G_TEX_1'
  execute  SP_TITULO_CAMPO 26, 'Texto Adicional #2', 'CB_G_TEX_2'
  execute  SP_TITULO_CAMPO 27, 'Texto Adicional #3', 'CB_G_TEX_3'
  execute  SP_TITULO_CAMPO 28, 'Texto Adicional #4', 'CB_G_TEX_4'
  execute  SP_TITULO_CAMPO 29, 'N�mero Adicional #1', 'CB_G_NUM_1'
  execute  SP_TITULO_CAMPO 30, 'N�mero Adicional #2', 'CB_G_NUM_2'
  execute  SP_TITULO_CAMPO 31, 'N�mero Adicional #3', 'CB_G_NUM_3'
  execute  SP_TITULO_CAMPO 32, 'L�gico Adicional #1', 'CB_G_LOG_1'
  execute  SP_TITULO_CAMPO 33, 'L�gico Adicional #2', 'CB_G_LOG_2'
  execute  SP_TITULO_CAMPO 34, 'L�gico Adicional #3', 'CB_G_LOG_3'
  execute  SP_TITULO_CAMPO 35, 'Tabla Adicional #1','CB_G_TAB_1'
  execute  SP_TITULO_CAMPO 36, 'Tabla Adicional #2','CB_G_TAB_2'
  execute  SP_TITULO_CAMPO 37, 'Tabla Adicional #3','CB_G_TAB_3'
  execute  SP_TITULO_CAMPO 38, 'Tabla Adicional #4','CB_G_TAB_4'

  execute  SP_TITULO_CAMPO 134, 'Orden de Trabajo','WO_NUMBER'
  execute  SP_TITULO_CAMPO 136, 'Parte','AR_CODIGO'
  execute  SP_TITULO_CAMPO 138, 'Area','CB_AREA'
  execute  SP_TITULO_CAMPO 138, 'Area','CI_AREA'
  execute  SP_TITULO_CAMPO 138, 'Area','CE_AREA'
  execute  SP_TITULO_CAMPO 138, 'Area','CS_AREA'
  execute  SP_TITULO_CAMPO 140, 'Operaci�n','OP_NUMBER'
  execute  SP_TITULO_CAMPO 142, 'Tiempo Muerto','WK_TMUERTO'
  execute  SP_TITULO_CAMPO 142, 'Tiempo Muerto','LX_TMUERTO'
  execute  SP_TITULO_CAMPO 176, 'Defecto','DE_CODIGO'
  execute  SP_TITULO_CAMPO 143, 'Modulador #1','LX_MODULA1'
  execute  SP_TITULO_CAMPO 144, 'Modulador #2','LX_MODULA2'
  execute  SP_TITULO_CAMPO 145, 'Modulador #3','LX_MODULA3'
  execute  SP_TITULO_CAMPO 143, 'Modulador #1','WK_MOD_1'
  execute  SP_TITULO_CAMPO 144, 'Modulador #2','WK_MOD_2'
  execute  SP_TITULO_CAMPO 145, 'Modulador #3','WK_MOD_3'

  execute  SP_TITULO_CONTEO  161, 'Criterio #1',  'CT_NIVEL_1'
  execute  SP_TITULO_CONTEO  162, 'Criterio #2',  'CT_NIVEL_2'
  execute  SP_TITULO_CONTEO  163, 'Criterio #3',  'CT_NIVEL_3'
  execute  SP_TITULO_CONTEO  164, 'Criterio #4',  'CT_NIVEL_4'
  execute  SP_TITULO_CONTEO  165, 'Criterio #5',  'CT_NIVEL_5'

  execute  SP_TITULO_EXPEDIEN_BOL  'EX_G_BOL', 'Si/No/Texto'
  execute  SP_TITULO_EXPEDIEN  'EX_G_LOG', 'Si/No'
  execute  SP_TITULO_EXPEDIEN  'EX_G_NUM', 'Numero'
  execute  SP_TITULO_EXPEDIEN  'EX_G_TEX', 'Texto'

  execute  SP_CUSTOM_DICCION

end
