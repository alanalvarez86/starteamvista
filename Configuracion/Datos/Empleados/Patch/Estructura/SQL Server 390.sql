/************************************************/
/************************************************/
/**********   Base De Datos DATOS      **********/
/************************************************/
/************************************************/

/************************************************/
/***** Tablas Modificadas ***********************/
/************************************************/

/* Tabla ENTIDAD: Siglas/Clave De Entidad Para CURP( Sugerencia 622 ) */
/* Patch 390 # Seq: 1 Agregado */
alter table ENTIDAD add TB_CURP Codigo2
GO

/* Tabla COLABORA: Entidad de Nacimiento ( Sugerencia 622 ) */
/* Patch 390 # Seq: 2 Agregado */
alter table COLABORA add CB_ENT_NAC Codigo2
GO

/* Tabla COLABORA: Colonia Donde Vive El Empleado ( Sugerencia 233 ) */
/* Patch 390 # Seq: 3 Agregado */
alter table COLABORA add CB_COD_COL Codigo
GO

/* Tabla COLABORA: Contrase�a Del Empleado ( Mejoras Al Kiosko ) */
/* Patch 390 # Seq: 4 Agregado */
alter table COLABORA add CB_PASSWRD Passwrd
GO

/* Tabla COLABORA: Campo Adicional Tipo Fecha # 4 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 5 Agregado */
alter table COLABORA add CB_G_FEC_4 Fecha           /* Campo Fecha Adicional # 4 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Fecha # 5 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 6 Agregado */
alter table COLABORA add CB_G_FEC_5 Fecha           /* Campo Fecha Adicional # 5 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Fecha # 6 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 7 Agregado */
alter table COLABORA add CB_G_FEC_6 Fecha           /* Campo Fecha Adicional # 6 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Fecha # 7 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 8 Agregado */
alter table COLABORA add CB_G_FEC_7 Fecha           /* Campo Fecha Adicional # 7 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Fecha # 8 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 9 Agregado */
alter table COLABORA add CB_G_FEC_8 Fecha           /* Campo Fecha Adicional # 8 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Booleano # 4 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 10 Agregado */
alter table COLABORA add CB_G_LOG_4 Booleano        /* Campo L�gico Adicional # 4 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Booleano # 5 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 11 Agregado */
alter table COLABORA add CB_G_LOG_5 Booleano        /* Campo L�gico Adicional # 5 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Booleano # 6 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 12 Agregado */
alter table COLABORA add CB_G_LOG_6 Booleano        /* Campo L�gico Adicional # 6 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Booleano # 7 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 13 Agregado */
alter table COLABORA add CB_G_LOG_7 Booleano        /* Campo L�gico Adicional # 7 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Booleano # 8 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 14 Agregado */
alter table COLABORA add CB_G_LOG_8 Booleano        /* Campo L�gico Adicional # 8 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Tabla # 5 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 15 Agregado */
alter table COLABORA add CB_G_TAB_5 Codigo          /* Campo Tabla Adicional # 5 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Tabla # 6 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 16 Agregado */
alter table COLABORA add CB_G_TAB_6 Codigo          /* Campo Tabla Adicional # 6 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Tabla # 7 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 17 Agregado */
alter table COLABORA add CB_G_TAB_7 Codigo          /* Campo Tabla Adicional # 7 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Tabla # 8 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 18 Agregado */
alter table COLABORA add CB_G_TAB_8 Codigo          /* Campo Tabla Adicional # 8 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Tabla # 9 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 19 Agregado */
alter table COLABORA add CB_G_TAB_9 Codigo          /* Campo Tabla Adicional # 9 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Tabla # 10 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 20 Agregado */
alter table COLABORA add CB_G_TAB10 Codigo          /* Campo Tabla Adicional # 10 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Tabla # 11 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 21 Agregado */
alter table COLABORA add CB_G_TAB11 Codigo          /* Campo Tabla Adicional # 11 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Tabla # 12 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 22 Agregado */
alter table COLABORA add CB_G_TAB12 Codigo          /* Campo Tabla Adicional # 12 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Tabla # 13 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 23 Agregado */
alter table COLABORA add CB_G_TAB13 Codigo          /* Campo Tabla Adicional # 13 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Tabla # 14 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 24 Agregado */
alter table COLABORA add CB_G_TAB14 Codigo          /* Campo Tabla Adicional # 14 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Texto # 5 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 25 Agregado */
alter table COLABORA add CB_G_TEX_5 Descripcion     /* Campo Texto Adicional # 5 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Texto # 6 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 26 Agregado */
alter table COLABORA add CB_G_TEX_6 Descripcion     /* Campo Texto Adicional # 6 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Texto # 7 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 27 Agregado */
alter table COLABORA add CB_G_TEX_7 Descripcion     /* Campo Texto Adicional # 7 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Texto # 8 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 28 Agregado */
alter table COLABORA add CB_G_TEX_8 Descripcion     /* Campo Texto Adicional # 8 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Texto # 9 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 29 Agregado */
alter table COLABORA add CB_G_TEX_9 Descripcion     /* Campo Texto Adicional # 9 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Texto # 10 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 30 Agregado */
alter table COLABORA add CB_G_TEX10 Descripcion     /* Campo Texto Adicional # 10 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Texto # 11 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 31 Agregado */
alter table COLABORA add CB_G_TEX11 Descripcion     /* Campo Texto Adicional # 11 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Texto # 12 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 32 Agregado */
alter table COLABORA add CB_G_TEX12 Descripcion     /* Campo Texto Adicional # 12 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Texto # 13 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 33 Agregado */
alter table COLABORA add CB_G_TEX13 Descripcion     /* Campo Texto Adicional # 13 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Texto # 14 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 34 Agregado */
alter table COLABORA add CB_G_TEX14 Descripcion     /* Campo Texto Adicional # 14 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Texto # 15 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 35 Agregado */
alter table COLABORA add CB_G_TEX15 Descripcion     /* Campo Texto Adicional # 15 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Texto # 16 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 36 Agregado */
alter table COLABORA add CB_G_TEX16 Descripcion     /* Campo Texto Adicional # 16 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Texto # 17 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 37 Agregado */
alter table COLABORA add CB_G_TEX17 Descripcion     /* Campo Texto Adicional # 17 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Texto # 18 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 38 Agregado */
alter table COLABORA add CB_G_TEX18 Descripcion     /* Campo Texto Adicional # 18 */
GO

/* Tabla COLABORA: Campo Adicional Tipo Texto # 19 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 39 Agregado */
alter table COLABORA add CB_G_TEX19 Descripcion     /* Campo Texto Adicional # 19 */
GO

/* Tabla COLABORA: Campo Adicional Tipo N�mero # 4 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 40 Agregado */
alter table COLABORA add CB_G_NUM_4 Pesos           /* Campo Num�rico Adicional # 4 */
GO

/* Tabla COLABORA: Campo Adicional Tipo N�mero # 5 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 41 Agregado */
alter table COLABORA add CB_G_NUM_5 Pesos           /* Campo Num�rico Adicional # 5 */
GO

/* Tabla COLABORA: Campo Adicional Tipo N�mero # 6 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 42 Agregado */
alter table COLABORA add CB_G_NUM_6 Pesos           /* Campo Num�rico Adicional # 6 */
GO

/* Tabla COLABORA: Campo Adicional Tipo N�mero # 7 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 43 Agregado */
alter table COLABORA add CB_G_NUM_7 Pesos           /* Campo Num�rico Adicional # 7 */
GO

/* Tabla COLABORA: Campo Adicional Tipo N�mero # 8 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 44 Agregado */
alter table COLABORA add CB_G_NUM_8 Pesos           /* Campo Num�rico Adicional # 8 */
GO

/* Tabla COLABORA: Campo Adicional Tipo N�mero # 9 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 45 Agregado */
alter table COLABORA add CB_G_NUM_9 Pesos           /* Campo Num�rico Adicional # 9 */
GO

/* Tabla COLABORA: Campo Adicional Tipo N�mero # 10 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 46 Agregado */
alter table COLABORA add CB_G_NUM10 Pesos           /* Campo Num�rico Adicional # 10 */
GO

/* Tabla COLABORA: Campo Adicional Tipo N�mero # 11 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 47 Agregado */
alter table COLABORA add CB_G_NUM11 Pesos           /* Campo Num�rico Adicional # 11 */
GO

/* Tabla COLABORA: Campo Adicional Tipo N�mero # 12 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 48 Agregado */
alter table COLABORA add CB_G_NUM12 Pesos           /* Campo Num�rico Adicional # 12 */
GO

/* Tabla COLABORA: Campo Adicional Tipo N�mero # 13 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 49 Agregado */
alter table COLABORA add CB_G_NUM13 Pesos           /* Campo Num�rico Adicional # 13 */
GO

/* Tabla COLABORA: Campo Adicional Tipo N�mero # 14 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 50 Agregado */
alter table COLABORA add CB_G_NUM14 Pesos           /* Campo Num�rico Adicional # 14 */
GO

/* Tabla COLABORA: Campo Adicional Tipo N�mero # 15 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 51 Agregado */
alter table COLABORA add CB_G_NUM15 Pesos           /* Campo Num�rico Adicional # 15 */
GO

/* Tabla COLABORA: Campo Adicional Tipo N�mero # 16 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 52 Agregado */
alter table COLABORA add CB_G_NUM16 Pesos           /* Campo Num�rico Adicional # 16 */
GO

/* Tabla COLABORA: Campo Adicional Tipo N�mero # 17 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 53 Agregado */
alter table COLABORA add CB_G_NUM17 Pesos           /* Campo Num�rico Adicional # 17 */
GO

/* Tabla COLABORA: Campo Adicional Tipo N�mero # 18 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 54 Agregado */
alter table COLABORA add CB_G_NUM18 Pesos           /* Campo Num�rico Adicional # 18 */
GO

/* Tabla CONCEPTO: Sub-Cuenta Para P�liza Contable ( Sugerencia 629 ) */
/* Patch 390 # Seq: 55 Agregado */
alter table CONCEPTO add CO_SUB_CTA Descripcion
GO

/* Tabla CLASIFI: Sub-Cuenta Para P�liza Contable ( Sugerencia 629 ) */
/* Patch 390 # Seq: 56 Agregado */
alter table CLASIFI add TB_SUB_CTA Descripcion
GO

/* Tabla TURNO: Sub-Cuenta Para P�liza Contable ( Sugerencia 629 ) */
/* Patch 390 # Seq: 57 Agregado */
alter table TURNO add TU_SUB_CTA Descripcion
GO

/* Tabla TURNO: Campo Temporal Para Almacenar Los Ritmos ( Sugerencia 683 ) */
/* Patch 390 # Seq: 58 Agregado */
alter table TURNO add TU_RIT_TMP Formula
GO

/* Tabla TURNO: Pasar de TURNO.TU_RIT_PAT hacia TURNO.TU_RIT_TMP ( Sugerencia 683 ) */
/* Patch 390 # Seq: 59 Agregado */
update TURNO set TU_RIT_TMP = TU_RIT_PAT
GO

/* Tabla TURNO: Borrar Columna TURNO.TU_RIT_PAT ( Sugerencia 683 ) */
/* Patch 390 # Seq: 60 Agregado */
exec SP_UNBINDEFAULT 'TURNO.TU_RIT_PAT'
alter table TURNO drop column TU_RIT_PAT
GO

/* Tabla TURNO: Cambiar Ancho del Ritmo De Un Turno a 255 ( Sugerencia 683 ) */
/* Patch 390 # Seq: 61 Agregado */
alter table TURNO add TU_RIT_PAT Formula
GO

/* Tabla TURNO: Pasar de TURNO.TU_RIT_TMP hacia TURNO.TU_RIT_PAT ( Sugerencia 683 ) */
/* Patch 390 # Seq: 62 Agregado */
update TURNO set TU_RIT_PAT = TU_RIT_TMP
GO

/* Tabla TURNO: Borrar Columna TURNO.TU_RIT_TMP ( Sugerencia 683 ) */
/* Patch 390 # Seq: 63 Agregado */
exec SP_UNBINDEFAULT 'TURNO.TU_RIT_TMP'
alter table TURNO drop column TU_RIT_TMP
GO

/* Tabla PUESTO: Sub-Cuenta Para P�liza Contable ( Sugerencia 629 ) */
/* Patch 390 # Seq: 64 Agregado */
alter table PUESTO add PU_SUB_CTA Descripcion
GO

/* Tabla NIVEL1: Sub-Cuenta Para P�liza Contable ( Sugerencia 629 ) */
/* Patch 390 # Seq: 65 Agregado */
alter table NIVEL1 add TB_SUB_CTA Descripcion
GO

/* Tabla NIVEL2: Sub-Cuenta Para P�liza Contable ( Sugerencia 629 ) */
/* Patch 390 # Seq: 66 Agregado */
alter table NIVEL2 add TB_SUB_CTA Descripcion
GO

/* Tabla NIVEL3: Sub-Cuenta Para P�liza Contable ( Sugerencia 629 ) */
/* Patch 390 # Seq: 67 Agregado */
alter table NIVEL3 add TB_SUB_CTA Descripcion
GO

/* Tabla NIVEL4: Sub-Cuenta Para P�liza Contable ( Sugerencia 629 ) */
/* Patch 390 # Seq: 68 Agregado */
alter table NIVEL4 add TB_SUB_CTA Descripcion
GO

/* Tabla NIVEL5: Sub-Cuenta Para P�liza Contable ( Sugerencia 629 ) */
/* Patch 390 # Seq: 69 Agregado */
alter table NIVEL5 add TB_SUB_CTA Descripcion
GO

/* Tabla NIVEL6: Sub-Cuenta Para P�liza Contable ( Sugerencia 629 ) */
/* Patch 390 # Seq: 70 Agregado */
alter table NIVEL6 add TB_SUB_CTA Descripcion
GO

/* Tabla NIVEL7: Sub-Cuenta Para P�liza Contable ( Sugerencia 629 ) */
/* Patch 390 # Seq: 71 Agregado */
alter table NIVEL7 add TB_SUB_CTA Descripcion
GO

/* Tabla NIVEL8: Sub-Cuenta Para P�liza Contable ( Sugerencia 629 ) */
/* Patch 390 # Seq: 72 Agregado */
alter table NIVEL8 add TB_SUB_CTA Descripcion
GO

/* Tabla NIVEL9: Sub-Cuenta Para P�liza Contable ( Sugerencia 629 ) */
/* Patch 390 # Seq: 73 Agregado */
alter table NIVEL9 add TB_SUB_CTA Descripcion
GO

/* Tabla CURSO: Costo # 3 ( Sugerencia 587 ) */
/* Patch 390 # Seq: 74 Agregado */
alter table CURSO add CU_COSTO3 Pesos
GO

/* Tabla SESION: Costo # 1 ( Sugerencia 587 ) */
/* Patch 390 # Seq: 75 Agregado */
alter table SESION add SE_COSTO1 Pesos
GO

/* Tabla SESION: Costo # 2 ( Sugerencia 587 ) */
/* Patch 390 # Seq: 76 Agregado */
alter table SESION add SE_COSTO2 Pesos
GO

/* Tabla SESION: Costo # 3 ( Sugerencia 587 ) */
/* Patch 390 # Seq: 77 Agregado */
alter table SESION add SE_COSTO3 Pesos
GO

/**********************************/
/* Inicializar Columnas Agregadas */
/**********************************/

/* Tabla ENTIDAD: Inicializar Siglas/Clave De Entidad Para CURP( Sugerencia 622 ) */
/* Patch 390 # Seq: 78 Agregado */
update ENTIDAD set TB_CURP = '' where ( TB_CURP is NULL )
GO

/* Tabla COLABORA: Inicializar Entidad de Nacimiento ( Sugerencia 622 ) */
/* Patch 390 # Seq: 79 Agregado */
update COLABORA set CB_ENT_NAC = '' where ( CB_ENT_NAC is NULL )
GO

/* Tabla COLABORA: Inicializar Colonia Donde Vive El Empleado ( Sugerencia 233 ) */
/* Patch 390 # Seq: 80 Agregado */
update COLABORA set CB_COD_COL = '' where ( CB_COD_COL is NULL )
GO

/* Tabla COLABORA: Inicializar Contrase�a Del Empleado */
/* Patch 390 # Seq: 81 Agregado */
update COLABORA set CB_PASSWRD = '' where ( CB_PASSWRD is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Fecha # 4 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 82 Agregado */
update COLABORA set CB_G_FEC_4 = '12/30/1899' where ( CB_G_FEC_4 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Fecha # 5 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 83 Agregado */
update COLABORA set CB_G_FEC_5 = '12/30/1899' where ( CB_G_FEC_5 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Fecha # 6 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 84 Agregado */
update COLABORA set CB_G_FEC_6 = '12/30/1899' where ( CB_G_FEC_6 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Fecha # 7 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 85 Agregado */
update COLABORA set CB_G_FEC_7 = '12/30/1899' where ( CB_G_FEC_7 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Fecha # 8 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 86 Agregado */
update COLABORA set CB_G_FEC_8 = '12/30/1899' where ( CB_G_FEC_8 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Booleano # 4 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 87 Agregado */
update COLABORA set CB_G_LOG_4 = 'N' where ( CB_G_LOG_4 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Booleano # 5 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 88 Agregado */
update COLABORA set CB_G_LOG_5 = 'N' where ( CB_G_LOG_5 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Booleano # 6 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 89 Agregado */
update COLABORA set CB_G_LOG_6 = 'N' where ( CB_G_LOG_6 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Booleano # 7 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 90 Agregado */
update COLABORA set CB_G_LOG_7 = 'N' where ( CB_G_LOG_7 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Booleano # 8 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 91 Agregado */
update COLABORA set CB_G_LOG_8 = 'N' where ( CB_G_LOG_8 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Tabla # 5 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 92 Agregado */
update COLABORA set CB_G_TAB_5 = '' where ( CB_G_TAB_5 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Tabla # 6 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 93 Agregado */
update COLABORA set CB_G_TAB_6 = '' where ( CB_G_TAB_6 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Tabla # 7 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 94 Agregado */
update COLABORA set CB_G_TAB_7 = '' where ( CB_G_TAB_7 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Tabla # 8 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 95 Agregado */
update COLABORA set CB_G_TAB_8 = '' where ( CB_G_TAB_8 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Tabla # 9 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 96 Agregado */
update COLABORA set CB_G_TAB_9 = '' where ( CB_G_TAB_9 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Tabla # 10 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 97 Agregado */
update COLABORA set CB_G_TAB10 = '' where ( CB_G_TAB10 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Tabla # 11 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 98 Agregado */
update COLABORA set CB_G_TAB11 = '' where ( CB_G_TAB11 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Tabla # 12 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 99 Agregado */
update COLABORA set CB_G_TAB12 = '' where ( CB_G_TAB12 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Tabla # 13 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 100 Agregado */
update COLABORA set CB_G_TAB13 = '' where ( CB_G_TAB13 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Tabla # 14 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 101 Agregado */
update COLABORA set CB_G_TAB14 = '' where ( CB_G_TAB14 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Texto # 5 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 102 Agregado */
update COLABORA set CB_G_TEX_5 = '' where ( CB_G_TEX_5 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Texto # 6 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 103 Agregado */
update COLABORA set CB_G_TEX_6 = '' where ( CB_G_TEX_6 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Texto # 7 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 104 Agregado */
update COLABORA set CB_G_TEX_7 = '' where ( CB_G_TEX_7 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Texto # 8 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 105 Agregado */
update COLABORA set CB_G_TEX_8 = '' where ( CB_G_TEX_8 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Texto # 9 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 106 Agregado */
update COLABORA set CB_G_TEX_9 = '' where ( CB_G_TEX_9 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Texto # 10 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 107 Agregado */
update COLABORA set CB_G_TEX10 = '' where ( CB_G_TEX10 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Texto # 11 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 108 Agregado */
update COLABORA set CB_G_TEX11 = '' where ( CB_G_TEX11 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Texto # 12 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 109 Agregado */
update COLABORA set CB_G_TEX12 = '' where ( CB_G_TEX12 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Texto # 13 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 110 Agregado */
update COLABORA set CB_G_TEX13 = '' where ( CB_G_TEX13 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Texto # 14 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 111 Agregado */
update COLABORA set CB_G_TEX14 = '' where ( CB_G_TEX14 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Texto # 15 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 112 Agregado */
update COLABORA set CB_G_TEX15 = '' where ( CB_G_TEX15 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Texto # 16 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 113 Agregado */
update COLABORA set CB_G_TEX16 = '' where ( CB_G_TEX16 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Texto # 17 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 114 Agregado */
update COLABORA set CB_G_TEX17 = '' where ( CB_G_TEX17 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Texto # 18 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 115 Agregado */
update COLABORA set CB_G_TEX18 = '' where ( CB_G_TEX18 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo Texto # 19 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 116 Agregado */
update COLABORA set CB_G_TEX19 = '' where ( CB_G_TEX19 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo N�mero # 4 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 117 Agregado */
update COLABORA set CB_G_NUM_4 = 0 where ( CB_G_NUM_4 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo N�mero # 5 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 118 Agregado */
update COLABORA set CB_G_NUM_5 = 0 where ( CB_G_NUM_5 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo N�mero # 6 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 119 Agregado */
update COLABORA set CB_G_NUM_6 = 0 where ( CB_G_NUM_6 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo N�mero # 7 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 120 Agregado */
update COLABORA set CB_G_NUM_7 = 0 where ( CB_G_NUM_7 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo N�mero # 8 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 121 Agregado */
update COLABORA set CB_G_NUM_8 = 0 where ( CB_G_NUM_8 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo N�mero # 9 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 122 Agregado */
update COLABORA set CB_G_NUM_9 = 0 where ( CB_G_NUM_9 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo N�mero # 10 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 123 Agregado */
update COLABORA set CB_G_NUM10 = 0 where ( CB_G_NUM10 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo N�mero # 11 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 124 Agregado */
update COLABORA set CB_G_NUM11 = 0 where ( CB_G_NUM11 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo N�mero # 12 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 125 Agregado */
update COLABORA set CB_G_NUM12 = 0 where ( CB_G_NUM12 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo N�mero # 13 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 126 Agregado */
update COLABORA set CB_G_NUM13 = 0 where ( CB_G_NUM13 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo N�mero # 14 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 127 Agregado */
update COLABORA set CB_G_NUM14 = 0 where ( CB_G_NUM14 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo N�mero # 15 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 128 Agregado */
update COLABORA set CB_G_NUM15 = 0 where ( CB_G_NUM15 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo N�mero # 16 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 129 Agregado */
update COLABORA set CB_G_NUM16 = 0 where ( CB_G_NUM16 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo N�mero # 17 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 130 Agregado */
update COLABORA set CB_G_NUM17 = 0 where ( CB_G_NUM17 is NULL )
GO

/* Tabla COLABORA: Inicializar Campo Adicional Tipo N�mero # 18 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 131 Agregado */
update COLABORA set CB_G_NUM18 = 0 where ( CB_G_NUM18 is NULL )
GO

/* Tabla CONCEPTO: Inicializar Sub-Cuenta Para P�liza Contable ( Sugerencia 629 ) */
/* Patch 390 # Seq: 132 Agregado */
update CONCEPTO set CO_SUB_CTA = '' where ( CO_SUB_CTA is NULL )
GO

/* Tabla CLASIFI: Inicializar Sub-Cuenta Para P�liza Contable ( Sugerencia 629 ) */
/* Patch 390 # Seq: 133 Agregado */
update CLASIFI set TB_SUB_CTA = '' where ( TB_SUB_CTA is NULL )
GO

/* Tabla TURNO: Inicializar Sub-Cuenta Para P�liza Contable ( Sugerencia 629 ) */
/* Patch 390 # Seq: 134 Agregado */
update TURNO set TU_SUB_CTA = '' where ( TU_SUB_CTA is NULL )
GO

/* Tabla PUESTO: Inicializar Sub-Cuenta Para P�liza Contable ( Sugerencia 629 ) */
/* Patch 390 # Seq: 135 Agregado */
update PUESTO set PU_SUB_CTA = '' where ( PU_SUB_CTA is NULL )
GO

/* Tabla NIVEL1: Inicializar Sub-Cuenta Para P�liza Contable ( Sugerencia 629 ) */
/* Patch 390 # Seq: 136 Agregado */
update NIVEL1 set TB_SUB_CTA = '' where ( TB_SUB_CTA is NULL )
GO

/* Tabla NIVEL2: Sub-Cuenta Para P�liza Contable ( Sugerencia 629 ) */
/* Patch 390 # Seq: 137 Agregado */
update NIVEL2 set TB_SUB_CTA = '' where ( TB_SUB_CTA is NULL )
GO

/* Tabla NIVEL3: Sub-Cuenta Para P�liza Contable ( Sugerencia 629 ) */
/* Patch 390 # Seq: 138 Agregado */
update NIVEL3 set TB_SUB_CTA = '' where ( TB_SUB_CTA is NULL )
GO

/* Tabla NIVEL4: Sub-Cuenta Para P�liza Contable ( Sugerencia 629 ) */
/* Patch 390 # Seq: 139 Agregado */
update NIVEL4 set TB_SUB_CTA = '' where ( TB_SUB_CTA is NULL )
GO

/* Tabla NIVEL5: Sub-Cuenta Para P�liza Contable ( Sugerencia 629 ) */
/* Patch 390 # Seq: 140 Agregado */
update NIVEL5 set TB_SUB_CTA = '' where ( TB_SUB_CTA is NULL )
GO

/* Tabla NIVEL6: Sub-Cuenta Para P�liza Contable ( Sugerencia 629 ) */
/* Patch 390 # Seq: 141 Agregado */
update NIVEL6 set TB_SUB_CTA = '' where ( TB_SUB_CTA is NULL )
GO

/* Tabla NIVEL7: Sub-Cuenta Para P�liza Contable ( Sugerencia 629 ) */
/* Patch 390 # Seq: 142 Agregado */
update NIVEL7 set TB_SUB_CTA = '' where ( TB_SUB_CTA is NULL )
GO

/* Tabla NIVEL8: Sub-Cuenta Para P�liza Contable ( Sugerencia 629 ) */
/* Patch 390 # Seq: 143 Agregado */
update NIVEL8 set TB_SUB_CTA = '' where ( TB_SUB_CTA is NULL )
GO

/* Tabla NIVEL9: Sub-Cuenta Para P�liza Contable ( Sugerencia 629 ) */
/* Patch 390 # Seq: 144 Agregado */
update NIVEL9 set TB_SUB_CTA = '' where ( TB_SUB_CTA is NULL )
GO

/* Tabla CURSO: Inicializar Costo # 3 ( Sugerencia 587 ) */
/* Patch 390 # Seq: 145 Agregado */
update CURSO set CU_COSTO3 = 0 where ( CU_COSTO3 is NULL )
GO

/* Tabla SESION: Inicializar Costo # 1 ( Sugerencia 587 ) */
/* Patch 390 # Seq: 146 Agregado */
update SESION set SE_COSTO1 = 0 where ( SE_COSTO1 is NULL )
GO

/* Tabla SESION: Costo # 2 ( Sugerencia 587 ) */
/* Patch 390 # Seq: 147 Agregado */
update SESION set SE_COSTO2 = 0 where ( SE_COSTO2 is NULL )
GO

/* Tabla SESION: Costo # 3 ( Sugerencia 587 ) */
/* Patch 390 # Seq: 148 Agregado */
update SESION set SE_COSTO3 = 0 where ( SE_COSTO3 is NULL )
GO

/************************************************/
/****** Tablas Nuevas ***************************/
/************************************************/

/* Tabla COLONIA: Cat�logo de Colonias / Fraccionamientos ( Sugerencia 233 ) */
/* Patch 390 # Seq: 149 Agregado */
CREATE TABLE COLONIA (
       TB_CODIGO            Codigo,         /* C�digo */
       TB_ELEMENT           Descripcion,    /* Nombre De La Colonia */
       TB_CODPOST           Referencia,     /* C�digo Postal */
       TB_CLINICA           Codigo3,        /* # Cl�nica Del IMSS */
       TB_ZONA	            Referencia,     /* Zona de la Ciudad */
       TB_INGLES            Descripcion,    /* Descripci�n En Ingl�s */
       TB_NUMERO            Pesos,          /* N�mero Libre */
       TB_TEXTO             Descripcion     /* Texto Libre */
)
GO

/* Llave Primaria COLONIA: Cat�logo de Colonias / Fraccionamientos ( Sugerencia 233 ) */
/* Patch 390 # Seq: 150 Agregado */
ALTER TABLE COLONIA
       ADD CONSTRAINT PK_COLONIA PRIMARY KEY(TB_CODIGO)
GO

/* Tabla GRUPO_AD: Agrupaciones De Campos Adicionales De COLABORA ( Sugerencia 119 ) */
/* Patch 390 # Seq: 151 Agregado */
CREATE TABLE GRUPO_AD (
       GX_CODIGO            Codigo,
       GX_TITULO            Observaciones,
       GX_POSICIO           FolioChico
)
GO

/* Llave Primaria GRUPO_AD: Agrupaciones De Campos Adicionales De COLABORA ( Sugerencia 119 ) */
/* Patch 390 # Seq: 152 Agregado */
ALTER TABLE GRUPO_AD
       ADD CONSTRAINT PK_GRUPO_AD PRIMARY KEY (GX_CODIGO)
GO

/* Tabla CAMPO_AD: Contenedor De Campos Adicionales De COLABORA ( Sugerencia 119 ) */
/* Patch 390 # Seq: 153 Agregado */
CREATE TABLE CAMPO_AD (
       CX_NOMBRE            NombreCampo,
       GX_CODIGO            Codigo,
       CX_POSICIO           FolioChico,
       CX_TITULO            Descripcion,
       CX_TIPO              Status,
       CX_DEFAULT           Formula,
       CX_MOSTRAR           Status,
       CX_ENTIDAD           Status
)
GO

/* Llave Primaria GRUPO_AD: Contenedor De Campos Adicionales De COLABORA ( Sugerencia 119 ) */
/* Patch 390 # Seq: 154 Agregado */
ALTER TABLE CAMPO_AD
       ADD CONSTRAINT PK_CAMPO_AD PRIMARY KEY (CX_NOMBRE)
GO

/* Tabla EXTRA5: Tabla Extra # 5 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 155 Agregado */
CREATE TABLE EXTRA5 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
GO

/* Llave Primaria EXTRA5: Tabla Extra # 5 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 156 Agregado */
ALTER TABLE EXTRA5
       ADD CONSTRAINT PK_EXTRA5 PRIMARY KEY(TB_CODIGO)
GO

/* Tabla EXTRA6: Tabla Extra # 6 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 157 Agregado */
CREATE TABLE EXTRA6 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
GO

/* Llave Primaria EXTRA6: Tabla Extra # 6 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 158 Agregado */
ALTER TABLE EXTRA6
       ADD CONSTRAINT PK_EXTRA6 PRIMARY KEY(TB_CODIGO)
GO

/* Tabla EXTRA7: Tabla Extra # 7 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 159 Agregado */
CREATE TABLE EXTRA7 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
GO

/* Llave Primaria EXTRA7: Tabla Extra # 7 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 160 Agregado */
ALTER TABLE EXTRA7
       ADD CONSTRAINT PK_EXTRA7 PRIMARY KEY(TB_CODIGO)
GO

/* Tabla EXTRA8: Tabla Extra # 8 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 161 Agregado */
CREATE TABLE EXTRA8 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
GO

/* Llave Primaria EXTRA8: Tabla Extra # 8 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 162 Agregado */
ALTER TABLE EXTRA8
       ADD CONSTRAINT PK_EXTRA8 PRIMARY KEY(TB_CODIGO)
GO

/* Tabla EXTRA9: Tabla Extra # 9 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 163 Agregado */
CREATE TABLE EXTRA9 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
GO

/* Llave Primaria EXTRA9: Tabla Extra # 9 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 164 Agregado */
ALTER TABLE EXTRA9
       ADD CONSTRAINT PK_EXTRA9 PRIMARY KEY(TB_CODIGO)
GO

/* Tabla EXTRA10: Tabla Extra # 10 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 165 Agregado */
CREATE TABLE EXTRA10 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
GO

/* Llave Primaria EXTRA10: Tabla Extra # 10 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 166 Agregado */
ALTER TABLE EXTRA10
       ADD CONSTRAINT PK_EXTRA10 PRIMARY KEY(TB_CODIGO)
GO

/* Tabla EXTRA11: Tabla Extra # 11 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 167 Agregado */
CREATE TABLE EXTRA11 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
GO

/* Llave Primaria EXTRA11: Tabla Extra # 11 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 168 Agregado */
ALTER TABLE EXTRA11
       ADD CONSTRAINT PK_EXTRA11 PRIMARY KEY(TB_CODIGO)
GO

/* Tabla EXTRA12: Tabla Extra # 12 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 169 Agregado */
CREATE TABLE EXTRA12 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
GO

/* Llave Primaria EXTRA12: Tabla Extra # 12 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 170 Agregado */
ALTER TABLE EXTRA12
       ADD CONSTRAINT PK_EXTRA12 PRIMARY KEY(TB_CODIGO)
GO

/* Tabla EXTRA13: Tabla Extra # 13 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 171 Agregado */
CREATE TABLE EXTRA13 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
GO

/* Llave Primaria EXTRA13: Tabla Extra # 13 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 172 Agregado */
ALTER TABLE EXTRA13
       ADD CONSTRAINT PK_EXTRA13 PRIMARY KEY(TB_CODIGO)
GO

/* Tabla EXTRA14: Tabla Extra # 14 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 173 Agregado */
CREATE TABLE EXTRA14 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
GO

/* Llave Primaria EXTRA14: Tabla Extra # 14 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 174 Agregado */
ALTER TABLE EXTRA14
       ADD CONSTRAINT PK_EXTRA14 PRIMARY KEY(TB_CODIGO)
GO

/*****************************************/
/**************** Views ******************/
/*****************************************/

/* Crear VIEW ENT_NAC Para Poder Hacer Joins Con COLABORA y Entidad De Nacimiento ( Sugerencia 622 ) */
/* Patch 390 # Seq: 175 Agregado */
create view ENT_NAC( TB_CODIGO, TB_ELEMENT, TB_INGLES, TB_NUMERO, TB_TEXTO, TB_CURP )
as
select TB_CODIGO, TB_ELEMENT, TB_INGLES, TB_NUMERO, TB_TEXTO, TB_CURP from ENTIDAD
GO

/*****************************************/
/**** Foreign Keys ***********************/
/*****************************************/

/*****************************************/
/****  Triggers  *************************/
/*****************************************/

/* Trigger TU_COLONIA */
/* Patch 390 # Seq: 69 Agregado */
CREATE TRIGGER TU_COLONIA ON COLONIA AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;
	select @NewCodigo = TB_CODIGO from Inserted;
        select @OldCodigo = TB_CODIGO from Deleted;
	update COLABORA set CB_COD_COL = @NewCodigo where ( COLABORA.CB_COD_COL = @OldCodigo );
  END
END
GO

/* Trigger TD_GRUPO_AD */
/* Patch 390 # Seq: 70 Agregado */
CREATE TRIGGER TD_GRUPO_AD ON GRUPO_AD AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
  update CAMPO_AD set CAMPO_AD.GX_CODIGO = '' from CAMPO_AD, Deleted where ( CAMPO_AD.GX_CODIGO = Deleted.GX_CODIGO );
END
GO

/* Trigger TU_GRUPO_AD */
/* Patch 390 # Seq: 71 Agregado */
CREATE TRIGGER TU_GRUPO_AD ON GRUPO_AD AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( GX_CODIGO )
  BEGIN
       declare @NewCodigo Codigo
       declare @OldCodigo Codigo;
       select @NewCodigo = GX_CODIGO from Inserted;
       select @OldCodigo = GX_CODIGO from Deleted;
       update CAMPO_AD set CAMPO_AD.GX_CODIGO = @NewCodigo
       where ( CAMPO_AD.GX_CODIGO = @OldCodigo );
  END
END
GO

/* Trigger TU_EXTRA5 */
/* Patch 390 # Seq: 72 Agregado */
CREATE TRIGGER TU_EXTRA5 ON EXTRA5 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;
	select @NewCodigo = TB_CODIGO from Inserted;
        select @OldCodigo = TB_CODIGO from Deleted;
	update COLABORA set CB_G_TAB_5 = @NewCodigo where ( COLABORA.CB_G_TAB_5 = @OldCodigo );
  END
END
GO

/* Trigger TU_EXTRA6 */
/* Patch 390 # Seq: 73 Agregado */
CREATE TRIGGER TU_EXTRA6 ON EXTRA6 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;
	select @NewCodigo = TB_CODIGO from Inserted;
        select @OldCodigo = TB_CODIGO from Deleted;
	update COLABORA set CB_G_TAB_6 = @NewCodigo where ( COLABORA.CB_G_TAB_6 = @OldCodigo );
  END
END
GO

/* Trigger TU_EXTRA7 */
/* Patch 390 # Seq: 74 Agregado */
CREATE TRIGGER TU_EXTRA7 ON EXTRA7 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;
	select @NewCodigo = TB_CODIGO from Inserted;
        select @OldCodigo = TB_CODIGO from Deleted;
	update COLABORA set CB_G_TAB_7 = @NewCodigo where ( COLABORA.CB_G_TAB_7 = @OldCodigo );
  END
END
GO

/* Trigger TU_EXTRA8 */
/* Patch 390 # Seq: 75 Agregado */
CREATE TRIGGER TU_EXTRA8 ON EXTRA8 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;
	select @NewCodigo = TB_CODIGO from Inserted;
        select @OldCodigo = TB_CODIGO from Deleted;
	update COLABORA set CB_G_TAB_8 = @NewCodigo where ( COLABORA.CB_G_TAB_8 = @OldCodigo );
  END
END
GO

/* Trigger TU_EXTRA9 */
/* Patch 390 # Seq: 76 Agregado */
CREATE TRIGGER TU_EXTRA9 ON EXTRA9 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;
	select @NewCodigo = TB_CODIGO from Inserted;
        select @OldCodigo = TB_CODIGO from Deleted;
	update COLABORA set CB_G_TAB_9 = @NewCodigo where ( COLABORA.CB_G_TAB_9 = @OldCodigo );
  END
END
GO

/* Trigger TU_EXTRA10 */
/* Patch 390 # Seq: 77 Agregado */
CREATE TRIGGER TU_EXTRA10 ON EXTRA10 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;
	select @NewCodigo = TB_CODIGO from Inserted;
        select @OldCodigo = TB_CODIGO from Deleted;
	update COLABORA set CB_G_TAB10 = @NewCodigo where ( COLABORA.CB_G_TAB10 = @OldCodigo );
  END
END
GO

/* Trigger TU_EXTRA11 */
/* Patch 390 # Seq: 78 Agregado */
CREATE TRIGGER TU_EXTRA11 ON EXTRA11 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;
	select @NewCodigo = TB_CODIGO from Inserted;
        select @OldCodigo = TB_CODIGO from Deleted;
	update COLABORA set CB_G_TAB11 = @NewCodigo where ( COLABORA.CB_G_TAB11 = @OldCodigo );
  END
END
GO

/* Trigger TU_EXTRA12 */
/* Patch 390 # Seq: 79 Agregado */
CREATE TRIGGER TU_EXTRA12 ON EXTRA12 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;
	select @NewCodigo = TB_CODIGO from Inserted;
        select @OldCodigo = TB_CODIGO from Deleted;
	update COLABORA set CB_G_TAB12 = @NewCodigo where ( COLABORA.CB_G_TAB12 = @OldCodigo );
  END
END
GO

/* Trigger TU_EXTRA13 */
/* Patch 390 # Seq: 80 Agregado */
CREATE TRIGGER TU_EXTRA13 ON EXTRA13 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;
	select @NewCodigo = TB_CODIGO from Inserted;
        select @OldCodigo = TB_CODIGO from Deleted;
	update COLABORA set CB_G_TAB13 = @NewCodigo where ( COLABORA.CB_G_TAB13 = @OldCodigo );
  END
END
GO

/* Trigger TU_EXTRA14 */
/* Patch 390 # Seq: 81 Agregado */
CREATE TRIGGER TU_EXTRA14 ON EXTRA14 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;
	select @NewCodigo = TB_CODIGO from Inserted;
        select @OldCodigo = TB_CODIGO from Deleted;
	update COLABORA set CB_G_TAB14 = @NewCodigo where ( COLABORA.CB_G_TAB14 = @OldCodigo );
  END
END
GO

/**************************************************/
/******** STORED PROCEDURES ***********************/
/**************************************************/

/* SP_GET_TURNO: Averiguar El Turno De Un Empleado A Una Fecha( Defecto 111 ) */
/* Patch 390 # Seq: 220 Agregado */
create function DBO.SP_GET_TURNO( @Fecha DATETIME, @Empleado INTEGER ) returns CHAR(6)
as
begin
     declare @FechaKardex DATETIME;
     declare @Turno CHAR(6);
     select @Turno = CB_TURNO, @FechaKardex = CB_FEC_KAR from COLABORA where ( CB_CODIGO = @Empleado );
     if ( @Fecha < @FechaKardex )
     begin
          select @TURNO = CB_TURNO from SP_FECHA_KARDEX( @Fecha, @Empleado );
     end
     return @Turno;
end
GO

/* EMPLEADO_ADENTRO: Determina Si Un Empleado Est� Dentro De La Empresa */
/* Patch 390 # Seq: 221 Agregado */
CREATE FUNCTION DBO.EMPLEADO_ADENTRO( @Empleado NumeroEmpleado, @Fecha Fecha ) RETURNS SmallInt
AS
BEGIN
     declare @iChecadas as INTEGER;
     select @iChecadas = COUNT(*) from ACCESLOG where
     ( ACCESLOG.CB_CODIGO = @Empleado ) and
     ( ACCESLOG.AL_FECHA = @Fecha ) and
     ( ( ( US_CODIGO = 0 ) and ( AL_OK_SIST = 'S' ) ) or ( ( US_CODIGO <> 0 ) and ( AL_OK_MAN = 'S' ) ) );
     RETURN ( @iChecadas % 2 );
END
GO

/* EMPLEADO_ULTACCESO: Determina El Ultimo Acceso De  Un Empleado En Una Fecha */
/* Patch 390 # Seq: 222 Agregado */
CREATE FUNCTION DBO.EMPLEADO_ULTACCESO( @Empleado NumeroEmpleado, @Fecha Fecha ) RETURNS HORA
AS
BEGIN
     declare @Checada as HORA;
     select TOP 1 @Checada = AL_HORA from ACCESLOG
     where ( ACCESLOG.CB_CODIGO = @Empleado ) and
           ( ACCESLOG.AL_FECHA = @Fecha ) and
           ( ( ( US_CODIGO = 0 ) and ( AL_OK_SIST = 'S' ) ) or ( ( US_CODIGO <> 0 ) and ( AL_OK_MAN = 'S' ) ) )
     order by AL_HORA desc;
     RETURN @Checada;
END
GO

/* EMPLEADO_ULTCASETA: Determina La Caseta Del El Ultimo Acceso De  Un Empleado En Una Fecha */
/* Patch 390 # Seq: 223 Agregado */
CREATE FUNCTION DBO.EMPLEADO_ULTCASETA( @Empleado NumeroEmpleado, @Fecha Fecha ) RETURNS Codigo
AS
BEGIN
     declare @Caseta as Codigo;
     select TOP 1 @Caseta = AL_CASETA from ACCESLOG
     where ( ACCESLOG.CB_CODIGO = @Empleado ) and
           ( ACCESLOG.AL_FECHA = @Fecha ) and
           ( ( ( US_CODIGO = 0 ) and ( AL_OK_SIST = 'S' ) ) or ( ( US_CODIGO <> 0 ) and ( AL_OK_MAN = 'S' ) ) )
     order by AL_HORA desc;
     RETURN @Caseta;
END
GO

/* Tabla ENTIDAD: Para Actualizar Siglas/Clave De Entidad Para CURP( Sugerencia 622 ) */
/* Patch 390 # Seq: 200 Agregado */
create procedure DBO.INIT_CURP_ENTIDAD
AS
BEGIN
     SET NOCOUNT ON;
     BEGIN TRANSACTION TX1
     update ENTIDAD set TB_CURP = 'DF' where ( TB_CODIGO = '01' ); /* DISTRITO FEDERAL */
     update ENTIDAD set TB_CURP = 'AS' where ( TB_CODIGO = '02' ); /* AGUASCALIENTES */
     update ENTIDAD set TB_CURP = 'BC' where ( TB_CODIGO = '03' ); /* BAJA CALIFORNIA */
     update ENTIDAD set TB_CURP = 'BS' where ( TB_CODIGO = '04' ); /* BAJA CALIFORNIA SUR */
     update ENTIDAD set TB_CURP = 'CC' where ( TB_CODIGO = '05' ); /* CAMPECHE */
     update ENTIDAD set TB_CURP = 'CL' where ( TB_CODIGO = '06' ); /* COAHUILA */
     update ENTIDAD set TB_CURP = 'CM' where ( TB_CODIGO = '07' ); /* COLIMA */
     update ENTIDAD set TB_CURP = 'CS' where ( TB_CODIGO = '08' ); /* CHIAPAS */
     update ENTIDAD set TB_CURP = 'CH' where ( TB_CODIGO = '09' ); /* CHIHUAHUA */
     update ENTIDAD set TB_CURP = 'DG' where ( TB_CODIGO = '10' ); /* DURANGO */
     update ENTIDAD set TB_CURP = 'GT' where ( TB_CODIGO = '11' ); /* GUANAJUATO */
     update ENTIDAD set TB_CURP = 'GR' where ( TB_CODIGO = '12' ); /* GUERRERO */
     update ENTIDAD set TB_CURP = 'HG' where ( TB_CODIGO = '13' ); /* HIDALGO */
     update ENTIDAD set TB_CURP = 'JC' where ( TB_CODIGO = '14' ); /* JALISCO */
     update ENTIDAD set TB_CURP = 'MC' where ( TB_CODIGO = '15' ); /* MEXICO */
     update ENTIDAD set TB_CURP = 'MN' where ( TB_CODIGO = '16' ); /* MICHOACAN */
     update ENTIDAD set TB_CURP = 'MS' where ( TB_CODIGO = '17' ); /* MORELOS */
     update ENTIDAD set TB_CURP = 'NT' where ( TB_CODIGO = '18' ); /* NAYARIT */
     update ENTIDAD set TB_CURP = 'NL' where ( TB_CODIGO = '19' ); /* NUEVO LEON */
     update ENTIDAD set TB_CURP = 'OC' where ( TB_CODIGO = '20' ); /* OAXACA */
     update ENTIDAD set TB_CURP = 'PL' where ( TB_CODIGO = '21' ); /* PUEBLA */
     update ENTIDAD set TB_CURP = 'QT' where ( TB_CODIGO = '22' ); /* QUERETARO */
     update ENTIDAD set TB_CURP = 'QR' where ( TB_CODIGO = '23' ); /* QUINTANA ROO */
     update ENTIDAD set TB_CURP = 'SP' where ( TB_CODIGO = '24' ); /* SAN LUIS POTOSI */
     update ENTIDAD set TB_CURP = 'SL' where ( TB_CODIGO = '25' ); /* SINALOA */
     update ENTIDAD set TB_CURP = 'SR' where ( TB_CODIGO = '26' ); /* SONORA */
     update ENTIDAD set TB_CURP = 'TC' where ( TB_CODIGO = '27' ); /* TABASCO */
     update ENTIDAD set TB_CURP = 'TS' where ( TB_CODIGO = '28' ); /* TAMAULIPAS */
     update ENTIDAD set TB_CURP = 'TL' where ( TB_CODIGO = '29' ); /* TLAXCALA */
     update ENTIDAD set TB_CURP = 'VZ' where ( TB_CODIGO = '30' ); /* VERACRUZ */
     update ENTIDAD set TB_CURP = 'YN' where ( TB_CODIGO = '31' ); /* YUCAT�N */
     update ENTIDAD set TB_CURP = 'ZS' where ( TB_CODIGO = '32' ); /* ZACATECAS */
     update ENTIDAD set TB_CURP = 'NE' where ( TB_CODIGO = '99' ); /* NACIDO EXTRANJERO */
     COMMIT TRANSACTION TX1
END
GO

/* Tabla ENTIDAD: Llamar INIT_CURP_ENTIDAD ( Sugerencia 622 ) */
/* Patch 390 # Seq: 201 Agregado */
exec DBO.INIT_CURP_ENTIDAD
GO

/* Tabla ENTIDAD: Borrar INIT_CURP_ENTIDAD ( Sugerencia 622 ) */
/* Patch 390 # Seq: 202 Agregado */
drop procedure DBO.INIT_CURP_ENTIDAD
GO

/* ADICIONAL_INIT: Inicializa Campos Adicionales De Empleados ( Sugerencia 119 ) */
/* Patch 390 # Seq: 224 Agregado */
create procedure DBO.ADICIONAL_INIT
as
begin
     SET NOCOUNT ON;
     BEGIN TRANSACTION TX1
     delete from CAMPO_AD

     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_FEC_1', 4 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_FEC_2', 4 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_FEC_3', 4 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_FEC_4', 4 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_FEC_5', 4 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_FEC_6', 4 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_FEC_7', 4 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_FEC_8', 4 )

     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_LOG_1', 2 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_LOG_2', 2 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_LOG_3', 2 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_LOG_4', 2 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_LOG_5', 2 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_LOG_6', 2 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_LOG_7', 2 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_LOG_8', 2 )

     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB_1', 5, 19 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB_2', 5, 20 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB_3', 5, 21 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB_4', 5, 22 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB_5', 5, 223 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB_6', 5, 224 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB_7', 5, 225 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB_8', 5, 226 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB_9', 5, 227 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB10', 5, 228 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB11', 5, 229 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB12', 5, 230 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB13', 5, 231 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB14', 5, 232 )

     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX_1', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX_2', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX_3', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX_4', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX_5', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX_6', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX_7', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX_8', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX_9', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX10', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX11', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX12', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX13', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX14', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX15', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX16', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX17', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX18', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX19', 0 )

     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM_1', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM_2', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM_3', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM_4', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM_5', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM_6', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM_7', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM_8', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM_9', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM10', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM11', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM12', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM13', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM14', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM15', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM16', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM17', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM18', 1 )
     COMMIT TRANSACTION TX1
end
GO

/* ADICIONAL_CONFIG: Inicializa Un Campo Adicional Ya Existente Previo AL Patch 390 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 204 Agregado */
create procedure DBO.ADICIONAL_CONFIG( @Global Integer, @Campo NombreCampo  )
as
begin
     SET NOCOUNT ON;
     declare @Grupo Codigo;
     declare @Titulo Formula;
     declare @Posicion Integer;
     select @Titulo = GL_FORMULA from GLOBAL where ( GL_CODIGO = @Global );
     if ( ( @Titulo is Null ) or ( @Titulo = '' ) )
     begin
          select @Posicion = 0;
          select @Grupo = '';
          select @Titulo = '';
     end
     else
     begin
          select @Grupo = 'EXTRA1';
          select @Posicion = COUNT(*) from GRUPO_AD;
          if ( @Posicion = 0 )
          begin
               insert into GRUPO_AD( GX_CODIGO, GX_TITULO, GX_POSICIO ) values ( @Grupo, 'Adicionales 1', 1 );
          end
          select @Posicion = MAX( CX_POSICIO ) from CAMPO_AD where ( GX_CODIGO = @Grupo );
          if ( @Posicion is Null )
             select @Posicion = 0;
          if ( @Posicion >= 9 )
          begin
               select @Grupo = 'EXTRA2';
               select @Posicion = COUNT(*) from GRUPO_AD where ( GX_CODIGO = @Grupo );
               if ( @Posicion = 0 )
               begin
                    insert into GRUPO_AD( GX_CODIGO, GX_TITULO, GX_POSICIO ) values ( @Grupo, 'Adicionales 2', 2 );
               end
               select @Posicion = MAX( CX_POSICIO ) from CAMPO_AD where ( GX_CODIGO = @Grupo );
               if ( @Posicion is Null )
                  select @Posicion = 0;
          end
          select @Posicion = @Posicion + 1;
     end
     update CAMPO_AD set GX_CODIGO = @Grupo,
                         CX_POSICIO = @Posicion,
                         CX_TITULO = @Titulo,
                         CX_DEFAULT = '',
                         CX_MOSTRAR = 0
     where ( CX_NOMBRE = @Campo );
end
GO

/* ADICIONAL_START: Inicializa Campos Adicionales Ya Existentes Previos AL Patch 390 ( Sugerencia 119 ) */
/* Patch 390 # Seq: 205 Agregado */
create procedure DBO.ADICIONAL_START
as
begin
     SET NOCOUNT ON;
     BEGIN TRANSACTION TX1

     execute ADICIONAL_CONFIG 25, 'CB_G_TEX_1'
     execute ADICIONAL_CONFIG 26, 'CB_G_TEX_2'
     execute ADICIONAL_CONFIG 27, 'CB_G_TEX_3'
     execute ADICIONAL_CONFIG 28, 'CB_G_TEX_4'

     execute ADICIONAL_CONFIG 29, 'CB_G_NUM_1'
     execute ADICIONAL_CONFIG 30, 'CB_G_NUM_2'
     execute ADICIONAL_CONFIG 31, 'CB_G_NUM_3'

     execute ADICIONAL_CONFIG 22, 'CB_G_FEC_1'
     execute ADICIONAL_CONFIG 23, 'CB_G_FEC_2'
     execute ADICIONAL_CONFIG 24, 'CB_G_FEC_3'

     execute ADICIONAL_CONFIG 35, 'CB_G_TAB_1'
     execute ADICIONAL_CONFIG 36, 'CB_G_TAB_2'
     execute ADICIONAL_CONFIG 37, 'CB_G_TAB_3'
     execute ADICIONAL_CONFIG 38, 'CB_G_TAB_4'
     
     execute ADICIONAL_CONFIG 32, 'CB_G_LOG_1'
     execute ADICIONAL_CONFIG 33, 'CB_G_LOG_2'
     execute ADICIONAL_CONFIG 34, 'CB_G_LOG_3'

     COMMIT TRANSACTION TX1

end
GO

/* Llamar ADICIONAL_INIT2( Sugerencia 119 ) */
/* Patch 390 # Seq: 206 Agregado */
exec DBO.ADICIONAL_INIT2
GO

/* Llamar ADICIONAL_START ( Sugerencia 119 ) */
/* Patch 390 # Seq: 207 Agregado */
exec DBO.ADICIONAL_START
GO

/* Borrar ADICIONAL_START ( Sugerencia 119 ) */
/* Patch 390 # Seq: 208 Agregado */
drop procedure DBO.ADICIONAL_START
GO

/* Borrar ADICIONAL_CONFIG ( Sugerencia 119 ) */
/* Patch 390 # Seq: 209 Agregado */
drop procedure DBO.ADICIONAL_CONFIG
GO

/* Borrar ADICIONAL_CONFIG ( Sugerencia 119 ) */
/* Patch 390 # Seq: 210 Agregado */
drop procedure DBO.ADICIONAL_INIT2
GO

/* SP_TITULO_TABLA_DICCION: Agregar Para Poder Refrescar Diccionario Con El T�tulo De Una Tabla */
/* Patch 390 # Seq: 180 Agregado */
CREATE PROCEDURE DBO.SP_TITULO_TABLA_DICCION( @Titulo VARCHAR(30), @TituloDefault VARCHAR(30), @Tabla SMALLINT)
as
begin
     SET NOCOUNT ON
     if ( ( @Titulo is NULL ) or ( @Titulo = '' ) )
         set @Titulo = @TituloDefault;
     update DICCION set DI_TITULO = @Titulo, DI_TCORTO = @Titulo, DI_CLAVES = ''
     where ( DI_CLASIFI = -1 ) and ( DI_CALC = @Tabla );
end
GO

/* SP_TITULO_TABLA_EXTRA: Determinar El Nombre De Una Tabla Adicional */
/* Patch 390 # Seq: 181 Agregado */
CREATE PROCEDURE DBO.SP_TITULO_TABLA_EXTRA( @TituloDefault VARCHAR(30), @Nombre VARCHAR(10) )
AS
begin
     SET NOCOUNT ON
     declare @Titulo Varchar(30);
     declare @Tabla SMALLINT;
     select @Titulo = CX_TITULO, @Tabla = CX_ENTIDAD from CAMPO_AD where ( CX_NOMBRE = @Nombre );
     if ( ( @Titulo is NOT NULL ) and ( @Tabla is NOT NULL ) )
     begin
          execute SP_TITULO_TABLA_DICCION @Titulo, @TituloDefault, @Tabla;
     end
end
GO

/* SP_TITULO_TABLA: Cambiar para extraer la l�gica que �nicamente refresca el DICCIONARIO, sin leer el GLOBAL */
/* Patch 390 # Seq: 187 Modificado */
ALTER PROCEDURE DBO.SP_TITULO_TABLA( @NumGlobal SMALLINT, @TituloDefault VARCHAR(30), @Tabla SMALLINT)
as
begin
     SET NOCOUNT ON
      declare @Titulo VARCHAR(30);
     select @Titulo = GL_FORMULA from GLOBAL where ( GL_CODIGO = @NumGlobal );
     execute SP_TITULO_TABLA_DICCION @Titulo, @TituloDefault, @Tabla
end
GO

/* SP_TITULO_CAMPO_DICCION: Agregar Para Poder Refrescar Diccionario Con El T�tulo De Un Campo */
/* Patch 390 # Seq: 182 Agregado */
CREATE  PROCEDURE DBO.SP_TITULO_CAMPO_DICCION( @Titulo VARCHAR(30), @TituloDefault VARCHAR(30), @Prefijo VARCHAR(30), @Campo VARCHAR(30) )
AS
BEGIN
     SET NOCOUNT ON;
     if ( ( @Titulo is NULL ) or ( @Titulo = '' ) )
        set @Titulo = @TituloDefault;
     set @Titulo = SUBSTRING ( @Prefijo + @Titulo , 1 , 30 ) ;
     update DICCION set DI_TITULO = @Titulo,
                        DI_TCORTO= @Titulo,
                        DI_CLAVES = ''
     where ( DI_CLASIFI in ( 5, 10, 18, 32, 33, 50, 59, 124, 141, 142, 143, 144, 145, 146, 147, 148, 149, 152, 186, 187, 191, 218 ) )
     and ( DI_NOMBRE = @Campo );
END
GO

/* SP_TITULO_CAMPO_EXTRA: Determinar El Nombre De Un Campo Adicional */
/* Patch 390 # Seq: 183 Agregado */
CREATE PROCEDURE DBO.SP_TITULO_CAMPO_EXTRA( @TituloDefault VARCHAR(30), @Campo VARCHAR(10) )
AS
begin
     SET NOCOUNT ON;
     declare @Titulo Varchar(30);
     select @Titulo = CX_TITULO from CAMPO_AD where ( CX_NOMBRE = @Campo );
     if ( @Titulo is NOT NULL )
     begin
          execute SP_TITULO_CAMPO_DICCION @Titulo, @TituloDefault, '', @Campo
     end
end
GO

/* SP_TITULO_CAMPO_EXTRA_GLOBAL: Determinar El Nombre De Un Campo Adicional */
/* Patch 390 # Seq: 182 Agregado */
CREATE PROCEDURE DBO.SP_TITULO_CAMPO_EXTRA_GLOBAL( @TituloDefault VARCHAR(30), @Campo VARCHAR(10), @Global Integer )
AS
begin
     SET NOCOUNT ON;
     declare @Titulo Varchar(30);
     select @Titulo = CX_TITULO from CAMPO_AD where ( CX_NOMBRE = @Campo );
     if ( @Titulo is NOT NULL )
     begin
          execute SP_TITULO_CAMPO_DICCION @Titulo, @TituloDefault, '', @Campo;
          update GLOBAL set GL_FORMULA = @Titulo where ( GL_CODIGO = @Global );
     end
end
GO

/* SP_TITULO_CAMPO_PLUS: Cambiar para extraer la l�gica que �nicamente refresca el DICCIONARIO, sin leer el GLOBAL */
/* Patch 390 # Seq: 185 Modificado */
ALTER PROCEDURE DBO.SP_TITULO_CAMPO_PLUS( @NUMGLOBAL SMALLINT, @TITULODEFAULT VARCHAR(30), @PREFIJO VARCHAR(30), @NOMBRE VARCHAR(30) )
AS
BEGIN
     SET NOCOUNT ON;
     DECLARE @TITULO VARCHAR(30);
     select @Titulo = GL_FORMULA from GLOBAL where ( GL_CODIGO = @NumGlobal );
     execute SP_TITULO_CAMPO_DICCION @Titulo, @TituloDefault, @Prefijo, @Nombre
END
GO

/* SP_REFRESH_ADICIONALES: Para Refrescar Diccionario Con Los Campos Adicionales De Empleados */
/* Patch 390 # Seq: 184 Agregado */
CREATE PROCEDURE DBO.SP_REFRESH_ADICIONALES
as
begin
     SET NOCOUNT ON;
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional # 1', 'CB_G_TAB_1'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional # 2', 'CB_G_TAB_2'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional # 3', 'CB_G_TAB_3'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional # 4', 'CB_G_TAB_4'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional # 5', 'CB_G_TAB_5'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional # 6', 'CB_G_TAB_6'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional # 7', 'CB_G_TAB_7'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional # 8', 'CB_G_TAB_8'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional # 9', 'CB_G_TAB_9'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional #10', 'CB_G_TAB10'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional #11', 'CB_G_TAB11'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional #12', 'CB_G_TAB12'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional #13', 'CB_G_TAB13'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional #14', 'CB_G_TAB14'

     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Tabla Adicional # 1', 'CB_G_TAB_1', 35
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Tabla Adicional # 2', 'CB_G_TAB_2', 36
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Tabla Adicional # 3', 'CB_G_TAB_3', 37
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Tabla Adicional # 4', 'CB_G_TAB_4', 38
     execute SP_TITULO_CAMPO_EXTRA 'Tabla Adicional # 5', 'CB_G_TAB_5'
     execute SP_TITULO_CAMPO_EXTRA 'Tabla Adicional # 6', 'CB_G_TAB_6'
     execute SP_TITULO_CAMPO_EXTRA 'Tabla Adicional # 7', 'CB_G_TAB_7'
     execute SP_TITULO_CAMPO_EXTRA 'Tabla Adicional # 8', 'CB_G_TAB_8'
     execute SP_TITULO_CAMPO_EXTRA 'Tabla Adicional # 9', 'CB_G_TAB_9'
     execute SP_TITULO_CAMPO_EXTRA 'Tabla Adicional #10', 'CB_G_TAB10'
     execute SP_TITULO_CAMPO_EXTRA 'Tabla Adicional #11', 'CB_G_TAB11'
     execute SP_TITULO_CAMPO_EXTRA 'Tabla Adicional #12', 'CB_G_TAB12'
     execute SP_TITULO_CAMPO_EXTRA 'Tabla Adicional #13', 'CB_G_TAB13'
     execute SP_TITULO_CAMPO_EXTRA 'Tabla Adicional #14', 'CB_G_TAB14'

     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Fecha Adicional # 1', 'CB_G_FEC_1', 22
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Fecha Adicional # 2', 'CB_G_FEC_2', 23
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Fecha Adicional # 3', 'CB_G_FEC_3', 24
     execute SP_TITULO_CAMPO_EXTRA 'Fecha Adicional # 4', 'CB_G_FEC_4'
     execute SP_TITULO_CAMPO_EXTRA 'Fecha Adicional # 5', 'CB_G_FEC_5'
     execute SP_TITULO_CAMPO_EXTRA 'Fecha Adicional # 6', 'CB_G_FEC_6'
     execute SP_TITULO_CAMPO_EXTRA 'Fecha Adicional # 7', 'CB_G_FEC_7'
     execute SP_TITULO_CAMPO_EXTRA 'Fecha Adicional # 8', 'CB_G_FEC_8'

     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'L�gico Adicional # 1', 'CB_G_LOG_1', 32
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'L�gico Adicional # 2', 'CB_G_LOG_2', 33
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'L�gico Adicional # 3', 'CB_G_LOG_3', 34
     execute SP_TITULO_CAMPO_EXTRA 'L�gico Adicional # 4', 'CB_G_LOG_4'
     execute SP_TITULO_CAMPO_EXTRA 'L�gico Adicional # 5', 'CB_G_LOG_5'
     execute SP_TITULO_CAMPO_EXTRA 'L�gico Adicional # 6', 'CB_G_LOG_6'
     execute SP_TITULO_CAMPO_EXTRA 'L�gico Adicional # 7', 'CB_G_LOG_7'
     execute SP_TITULO_CAMPO_EXTRA 'L�gico Adicional # 8', 'CB_G_LOG_8'

     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Texto Adicional # 1', 'CB_G_TEX_1', 25
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Texto Adicional # 2', 'CB_G_TEX_2', 26
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Texto Adicional # 3', 'CB_G_TEX_3', 27
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Texto Adicional # 4', 'CB_G_TEX_4', 28
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional # 5', 'CB_G_TEX_5'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional # 6', 'CB_G_TEX_6'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional # 7', 'CB_G_TEX_7'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional # 8', 'CB_G_TEX_8'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional # 9', 'CB_G_TEX_9'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #10', 'CB_G_TEX10'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #11', 'CB_G_TEX11'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #12', 'CB_G_TEX12'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #13', 'CB_G_TEX13'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #14', 'CB_G_TEX14'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #15', 'CB_G_TEX15'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #16', 'CB_G_TEX16'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #17', 'CB_G_TEX17'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #18', 'CB_G_TEX18'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #19', 'CB_G_TEX19'

     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'N�mero Adicional # 1', 'CB_G_NUM_1', 29
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'N�mero Adicional # 2', 'CB_G_NUM_2', 30
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'N�mero Adicional # 3', 'CB_G_NUM_3', 31
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional # 4', 'CB_G_NUM_4'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional # 5', 'CB_G_NUM_5'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional # 6', 'CB_G_NUM_6'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional # 7', 'CB_G_NUM_7'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional # 8', 'CB_G_NUM_8'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional # 9', 'CB_G_NUM_9'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional #10', 'CB_G_NUM10'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional #11', 'CB_G_NUM11'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional #12', 'CB_G_NUM12'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional #13', 'CB_G_NUM13'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional #14', 'CB_G_NUM14'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional #15', 'CB_G_NUM15'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional #16', 'CB_G_NUM16'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional #17', 'CB_G_NUM17'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional #18', 'CB_G_NUM18'
end
GO

/* SP_REFRESH_DICCION: Modificado para que invoque el nuevo SP_REFRESH_ADICIONALES */
/* Patch 390 # Seq: 195 Modificado */
ALTER PROCEDURE DBO.SP_REFRESH_DICCION
as
begin
  SET NOCOUNT ON
  execute  SP_TITULO_TABLA 13, 'Nivel de Organigrama #1', 41 ;
  execute  SP_TITULO_TABLA 14, 'Nivel de Organigrama #2', 42 ;
  execute  SP_TITULO_TABLA 15, 'Nivel de Organigrama #3', 43 ;
  execute  SP_TITULO_TABLA 16, 'Nivel de Organigrama #4', 44 ;
  execute  SP_TITULO_TABLA 17, 'Nivel de Organigrama #5', 45 ;
  execute  SP_TITULO_TABLA 18, 'Nivel de Organigrama #6', 46 ;
  execute  SP_TITULO_TABLA 19, 'Nivel de Organigrama #7', 47 ;
  execute  SP_TITULO_TABLA 20, 'Nivel de Organigrama #8', 48 ;
  execute  SP_TITULO_TABLA 21, 'Nivel de Organigrama #9', 49 ;

  execute  SP_TITULO_TABLA 134, 'Orden de Trabajo', 149
  execute  SP_TITULO_TABLA 136, 'Cat�logo de Partes', 141
  execute  SP_TITULO_TABLA 138, 'Cat�logo de Area', 148
  execute  SP_TITULO_TABLA 140, 'Cat�logo de Operaciones', 142
  execute  SP_TITULO_TABLA 142, 'Cat�logo de Tiempo Muerto', 138
  execute  SP_TITULO_TABLA 176, 'Lista de Defectos', 187
  execute  SP_TITULO_TABLA 143, 'Modulador #1', 135
  execute  SP_TITULO_TABLA 144, 'Modulador #2', 136
  execute  SP_TITULO_TABLA 145, 'Modulador #3', 137

  execute  SP_TITULO_CAMPO 13, 'C�digo de Nivel #1', 'CB_NIVEL1'
  execute  SP_TITULO_CAMPO 14, 'C�digo de Nivel #2', 'CB_NIVEL2'
  execute  SP_TITULO_CAMPO 15, 'C�digo de Nivel #3', 'CB_NIVEL3'
  execute  SP_TITULO_CAMPO 16, 'C�digo de Nivel #4', 'CB_NIVEL4'
  execute  SP_TITULO_CAMPO 17, 'C�digo de Nivel #5', 'CB_NIVEL5'
  execute  SP_TITULO_CAMPO 18, 'C�digo de Nivel #6', 'CB_NIVEL6'
  execute  SP_TITULO_CAMPO 19, 'C�digo de Nivel #7', 'CB_NIVEL7'
  execute  SP_TITULO_CAMPO 20, 'C�digo de Nivel #8', 'CB_NIVEL8'
  execute  SP_TITULO_CAMPO 21, 'C�digo de Nivel #9', 'CB_NIVEL9'

  execute  SP_TITULO_CAMPO 13, 'Nuevo Nivel #1', 'EV_NIVEL1'
  execute  SP_TITULO_CAMPO 14, 'Nuevo Nivel #2', 'EV_NIVEL2'
  execute  SP_TITULO_CAMPO 15, 'Nuevo Nivel #3', 'EV_NIVEL3'
  execute  SP_TITULO_CAMPO 16, 'Nuevo Nivel #4', 'EV_NIVEL4'
  execute  SP_TITULO_CAMPO 17, 'Nuevo Nivel #5', 'EV_NIVEL5'
  execute  SP_TITULO_CAMPO 18, 'Nuevo Nivel #6', 'EV_NIVEL6'
  execute  SP_TITULO_CAMPO 19, 'Nuevo Nivel #7', 'EV_NIVEL7'
  execute  SP_TITULO_CAMPO 20, 'Nuevo Nivel #8', 'EV_NIVEL8'
  execute  SP_TITULO_CAMPO 21, 'Nuevo Nivel #9', 'EV_NIVEL9'

  execute  SP_TITULO_CAMPO 13, 'C�digo de Nivel #1', 'PU_NIVEL1'
  execute  SP_TITULO_CAMPO 14, 'C�digo de Nivel #2', 'PU_NIVEL2'
  execute  SP_TITULO_CAMPO 15, 'C�digo de Nivel #3', 'PU_NIVEL3'
  execute  SP_TITULO_CAMPO 16, 'C�digo de Nivel #4', 'PU_NIVEL4'
  execute  SP_TITULO_CAMPO 17, 'C�digo de Nivel #5', 'PU_NIVEL5'
  execute  SP_TITULO_CAMPO 18, 'C�digo de Nivel #6', 'PU_NIVEL6'
  execute  SP_TITULO_CAMPO 19, 'C�digo de Nivel #7', 'PU_NIVEL7'
  execute  SP_TITULO_CAMPO 20, 'C�digo de Nivel #8', 'PU_NIVEL8'
  execute  SP_TITULO_CAMPO 21, 'C�digo de Nivel #9', 'PU_NIVEL9'

  execute  SP_TITULO_CAMPO 134, 'Orden de Trabajo','WO_NUMBER'
  execute  SP_TITULO_CAMPO 136, 'Parte','AR_CODIGO'
  execute  SP_TITULO_CAMPO 138, 'Area','CB_AREA'
  execute  SP_TITULO_CAMPO 138, 'Area','CI_AREA'
  execute  SP_TITULO_CAMPO 138, 'Area','CE_AREA'
  execute  SP_TITULO_CAMPO 138, 'Area','CS_AREA'
  execute  SP_TITULO_CAMPO 140, 'Operaci�n','OP_NUMBER'
  execute  SP_TITULO_CAMPO 142, 'Tiempo Muerto','WK_TMUERTO'
  execute  SP_TITULO_CAMPO 142, 'Tiempo Muerto','LX_TMUERTO'
  execute  SP_TITULO_CAMPO 176, 'Defecto','DE_CODIGO'
  execute  SP_TITULO_CAMPO 143, 'Modulador #1','LX_MODULA1'
  execute  SP_TITULO_CAMPO 144, 'Modulador #2','LX_MODULA2'
  execute  SP_TITULO_CAMPO 145, 'Modulador #3','LX_MODULA3'
  execute  SP_TITULO_CAMPO 143, 'Modulador #1','WK_MOD_1'
  execute  SP_TITULO_CAMPO 144, 'Modulador #2','WK_MOD_2'
  execute  SP_TITULO_CAMPO 145, 'Modulador #3','WK_MOD_3'

  execute  SP_TITULO_CONTEO  161, 'Criterio #1',  'CT_NIVEL_1'
  execute  SP_TITULO_CONTEO  162, 'Criterio #2',  'CT_NIVEL_2'
  execute  SP_TITULO_CONTEO  163, 'Criterio #3',  'CT_NIVEL_3'
  execute  SP_TITULO_CONTEO  164, 'Criterio #4',  'CT_NIVEL_4'
  execute  SP_TITULO_CONTEO  165, 'Criterio #5',  'CT_NIVEL_5'

  execute  SP_TITULO_EXPEDIEN_BOL  'EX_G_BOL', 'Si/No/Texto'
  execute  SP_TITULO_EXPEDIEN  'EX_G_LOG', 'Si/No'
  execute  SP_TITULO_EXPEDIEN  'EX_G_NUM', 'Numero'
  execute  SP_TITULO_EXPEDIEN  'EX_G_TEX', 'Texto'

  execute  SP_TITULO_CAMPO_PLUS 13, 'Nivel #1', 'Mod. ', 'CB_MOD_NV1'
  execute  SP_TITULO_CAMPO_PLUS 14, 'Nivel #2', 'Mod. ', 'CB_MOD_NV2'
  execute  SP_TITULO_CAMPO_PLUS 15, 'Nivel #3', 'Mod. ', 'CB_MOD_NV3'
  execute  SP_TITULO_CAMPO_PLUS 16, 'Nivel #4', 'Mod. ', 'CB_MOD_NV4'
  execute  SP_TITULO_CAMPO_PLUS 17, 'Nivel #5', 'Mod. ', 'CB_MOD_NV5'
  execute  SP_TITULO_CAMPO_PLUS 18, 'Nivel #6', 'Mod. ', 'CB_MOD_NV6'
  execute  SP_TITULO_CAMPO_PLUS 19, 'Nivel #7', 'Mod. ', 'CB_MOD_NV7'
  execute  SP_TITULO_CAMPO_PLUS 20, 'Nivel #8', 'Mod. ', 'CB_MOD_NV8'
  execute  SP_TITULO_CAMPO_PLUS 21, 'Nivel #9', 'Mod. ', 'CB_MOD_NV9'

  execute SP_REFRESH_ADICIONALES

  execute  SP_CUSTOM_DICCION

end
GO

/*****************************************************/
/** D:\3Win_13\Datos\SQLServer\3DatosProcedures.sql **/
/*****************************************************/

/* AFECTA_FALTAS: Cambiado para quitar los par�metros YEAR */
/* Patch 390 # Seq: 154 Modificado */
ALTER PROCEDURE AFECTA_FALTAS
    				@ANIO SMALLINT,
    				@TIPO SMALLINT,
    				@NUMERO SMALLINT,
    				@FACTOR INTEGER
AS
        SET NOCOUNT ON
	declare @FechaIni   DateTime;
  	declare @Incidencia Char( 6 );
  	declare @TipoDia    SmallInt;
  	declare @Empleado   Integer;
  	declare @FEC_INI    DateTime;
  	declare @MOTIVO     SmallInt;
  	declare @DIAS       Numeric(15,2);

  	select @FechaIni = P.PE_FEC_INI 
	from PERIODO P 
	where
         ( P.PE_YEAR = @Anio ) and
         ( P.PE_TIPO = @Tipo ) and
         ( P.PE_NUMERO = @Numero )

	DECLARE TemporalFaltas CURSOR FOR
  		select F.CB_CODIGO, F.FA_FEC_INI, F.FA_MOTIVO, F.FA_DIAS
  		from   FALTAS F
       	left outer join NOMINA N on 
         	( N.PE_YEAR = @Anio )
		and ( N.PE_TIPO = @Tipo ) 
		and ( N.PE_NUMERO = @Numero ) 
		and ( N.CB_CODIGO = F.CB_CODIGO )
  		where ( F.PE_YEAR = @Anio ) 
		and ( F.PE_TIPO = @Tipo ) 
		and ( F.PE_NUMERO = @Numero )
		and ( F.FA_DIA_HOR = 'D' ) 
		and ( F.FA_MOTIVO  IN (0,1,2,4,12))

   	OPEN TemporalFaltas
   	FETCH NEXT FROM TemporalFaltas
   	into @Empleado, @FEC_INI, @MOTIVO, @DIAS 
    WHILE @@FETCH_STATUS = 0
   	BEGIN
        if ( @FEC_INI <= '12/30/1899' ) 
		begin
			select @FEC_INI = @FechaIni;
		end
     	if ( @MOTIVO = 0 ) 
     	begin
       		Select @TipoDia = 0, @Incidencia = 'FI'
     	end
     	else if ( @MOTIVO = 1 ) 
     	begin
       		Select @TipoDia = 5, @Incidencia = 'FJ'
     	end
     	else if ( @MOTIVO = 2 )
     	begin
       		Select @TipoDia = 4, @Incidencia = 'FJ'
     	end
     	else if ( @MOTIVO = 4 ) 
     	begin
       		Select @TipoDia = 6, @Incidencia = 'FJ'
     	end
     	else  
     	begin
       		Select @TipoDia = 0, @Incidencia = '';
     	end


     	while ( @DIAS > 0 )
     	begin
       		if ( @MOTIVO <> 12 )
         		execute AFECTA_1_FALTA @Empleado, @FEC_INI, @TipoDia, @Incidencia, @Factor
       		else if ( @FEC_INI < @FechaIni )
         		execute AFECTA_1_AJUSTE @Empleado, @FEC_INI, @TipoDia, @Incidencia, @Factor

       		select @DIAS = @DIAS - 1, @FEC_INI = @FEC_INI + 1
     	end

		FETCH NEXT FROM TemporalFaltas
   		into @Empleado, @FEC_INI, @MOTIVO, @DIAS
  	end

   CLOSE TemporalFaltas
   DEALLOCATE TemporalFaltas
GO

/* SP_TOPE_PTU: Cambiado para quitar los par�metros YEAR */
/* Patch 390 # Seq: 159 Modificado */
ALTER procedure DBO.SP_TOPE_PTU(
@ANIO SMALLINT,
@UTILIDADES NUMERIC( 15, 2 ),
@TOPESALARIO NUMERIC( 15, 2 ) )
as
begin
     SET NOCOUNT ON
     declare @Empleado Integer;
     declare @iEmpleados Integer;
     declare @nDias SmallInt;
     declare @nTotalDias Integer;
     declare @nFactorDias NUMERIC( 15, 5 );
     declare @nMonto NUMERIC( 15, 2 );
     declare @nTotalMonto NUMERIC( 15, 2 );
     declare @nFactorMonto NUMERIC( 15, 5 );
     set @nTotalDias = 0;
     set @nTotalMonto = 0;
     set @iEmpleados = 0;
     declare Temporal cursor for
        select R.CB_CODIGO, R.RU_DIAS, R.RU_MONTO from DBO.REP_PTU as R where ( R.RU_YEAR = @ANIO );
     open Temporal;
     fetch NEXT from Temporal into @Empleado, @nDias, @nMonto;
     while @@Fetch_Status = 0
     begin
          if ( @nMonto > @TOPESALARIO )
          begin
               set @nMonto = @TOPESALARIO;
               update DBO.REP_PTU set RU_MONTO = @TopeSalario where
               ( RU_YEAR = @ANIO ) and ( CB_CODIGO = @Empleado );
          end;
          set @nTotalMonto = @nTotalMonto + @nMonto;
          set @nTotalDias = @nTotalDias + @nDias;
          set @iEmpleados = @iEmpleados + 1;
          fetch NEXT from Temporal into @Empleado, @nDias, @nMonto;
     end;
     close Temporal;
     if ( @iEmpleados > 0 )
     begin
          set @nFactorDias = ( ( @UTILIDADES / 2.0 ) / @nTotalDias );
          set @nFactorMonto = ( ( @UTILIDADES / 2.0 ) / @nTotalMonto );
          open Temporal;
          fetch NEXT from Temporal into @Empleado, @nDias, @nMonto;
          while @@Fetch_Status = 0
          begin
               update DBO.REP_PTU set RU_M_DIAS = ROUND( @nDias * @nFactorDias, 2 ),
                                      RU_M_MONTO = ROUND( @nMonto * @nFactorMonto, 2 )
               where ( CB_CODIGO = @Empleado ) and ( RU_YEAR = @ANIO );
               fetch NEXT from Temporal into @Empleado, @nDias, @nMonto;
          end;
          close Temporal;
     end;
     deallocate Temporal;
end
GO

/* IMSS_CALCULA_RECARGOS: Cambiado para quitar los par�metros YEAR */
/* Patch 390 # Seq: 172 Modificado */
ALTER procedure DBO.IMSS_CALCULA_RECARGOS(
@PATRON CHAR( 1 ),
@ANIO SMALLINT,
@MES SMALLINT,
@TIPO SMALLINT,
@FACTOR NUMERIC( 15, 5 ),
@TASA NUMERIC( 15, 5 ) )
as
begin
     SET NOCOUNT ON
     declare @rTotImss Numeric( 15, 2 );
     declare @rTotRetiro Numeric( 15, 2 );
     declare @rTotInfo Numeric( 15, 2 );
     declare @rTotAmort Numeric( 15, 2 );
     declare @rTotApoVol Numeric( 15, 2 );
     declare @rContraFactor Numeric( 15, 9 );
     declare @rContraTasa Numeric( 15, 9 );
     declare @rActIMSS Numeric( 15, 2 );
     declare @rRecIMSS Numeric( 15, 2 );
     declare @rActRet Numeric( 15, 2 );
     declare @rRecRet Numeric( 15, 2 );
     declare @rActINFO Numeric( 15, 2 );
     declare @rRecINFO Numeric( 15, 2 );
     declare @Cuantos Integer;
     select @rTotImss = SUM( L.LE_TOT_IMS ),
            @rTotRetiro = SUM( L.LE_TOT_RET - L.LE_APO_VOL ),
            @rTotInfo = SUM( L.LE_INF_PAT ),
            @rTotAmort = SUM( L.LE_INF_AMO ),
            @rTotApoVol = SUM( L.LE_APO_VOL ),
            @Cuantos = COUNT(*)
     from DBO.LIQ_EMP as L where ( L.LS_PATRON = @PATRON ) and
                                 ( L.LS_YEAR = @ANIO ) and
                                 ( L.LS_MONTH = @MES ) and
                                 ( L.LS_TIPO = @TIPO );
     set @rContraFactor = ( @FACTOR - 1 );
     set @rContraTasa = @FACTOR * ( @TASA / 100 );
     if ( @Cuantos > 0 )
     begin
          set @rActIMSS = @rTotIMSS * @rContraFactor;
          set @rRecIMSS = @rTotIMSS * @rContraTasa;
          set @rActRET = @rTotRetiro * @rContraFactor;
          set @rRecRET = @rTotRetiro * @rContraTasa;
          set @rTotInfo = @rTotInfo + @rTotAmort;
          set @rActINFO = @rTotInfo * @rContraFactor;
          set @rRecINFO = @rTotInfo * @rContraTasa;
     end
     else
     begin
          set @rActIMSS = 0;
          set @rRecIMSS = 0;
          set @rActRET = 0;
          set @rRecRET = 0;
          set @rTotInfo = 0;
          set @rActINFO = 0;
          set @rRecINFO = 0;
     end
     set @rTotIMSS = @rActIMSS + @rRecIMSS;
     set @rTotRetiro = @rActRET + @rRecRET;
     set @rTotINFO = @rActINFO + @rRecINFO;
     update DBO.LIQ_IMSS set LS_ACT_IMS = @rActIMSS,
                             LS_REC_IMS = @rRecIMSS,
                             LS_ACT_RET = @rActRet,
                             LS_REC_RET = @rRecRet,
                             LS_ACT_INF = @rActINFO,
                             LS_REC_INF = @rRecINFO,
                             LS_ACT_AMO = @rTotAmort * @rContraFactor,
                             LS_REC_AMO = @rTotAmort * @rContraTasa,
                             LS_ACT_APO = @rTotApoVol * @rContraFactor,
                             LS_REC_APO = @rTotApoVol * @rContraTasa,
                             LS_FAC_ACT = @Factor,
                             LS_FAC_REC = @Tasa,
                             LS_TOT_IMS = LS_SUB_IMS + @rTotIMSS,
                             LS_TOT_RET = LS_SUB_RET + LS_APO_VOL + @rTotRetiro,
                             LS_TOT_INF = LS_SUB_INF + @rTotINFO,
                             LS_TOT_MES = LS_SUB_IMS + @rTotIMSS + LS_SUB_RET + LS_APO_VOL + @rTotRetiro +
                                          LS_SUB_INF + @rTotINFO
     where ( LS_PATRON = @PATRON ) and
           ( LS_YEAR = @ANIO ) and
           ( LS_MONTH = @MES ) and
           ( LS_TIPO = @TIPO );
end
GO

/* SP_UPDATE_AUSENCIA: Cambiado para quitar los par�metros YEAR */
/* Patch 390 # Seq: 19 Modificado */
ALTER PROCEDURE SP_UPDATE_AUSENCIA
    				@EMPLEADO INTEGER,
    				@FECHA DATETIME,
    				@HORARIO CHAR(6),
    				@STATUS SMALLINT,
    				@ANIO SMALLINT,
    				@TIPO SMALLINT,
    				@NUMERO SMALLINT,
    				@POSICION SMALLINT,
    				@INCIDENCIA CHAR(3),
    				@TIPODIA SMALLINT,
    				@ORDINARIAS NUMERIC(15,2),
    				@DOBLES NUMERIC(15,2),
    				@TRIPLES NUMERIC(15,2),
    				@DESCANSO NUMERIC(15,2),
    				@PERMISOCG NUMERIC(15,2),
    				@PERMISOSG NUMERIC(15,2)
AS
        SET NOCOUNT ON
  	declare @Usuario SmallInt;
  	declare @Extras Numeric( 15, 2 );
  	declare @Tardes Numeric( 15, 2 );
    select @Extras = @Dobles + @Triples;

    select @Usuario = US_CODIGO, @Tardes = AU_TARDES
    from AUSENCIA
    where ( CB_CODIGO = @Empleado ) and ( AU_FECHA = @Fecha )
    
	if ( @Usuario is NULL ) 
    begin
		insert into AUSENCIA( CB_CODIGO,
                              AU_FECHA,
                              HO_CODIGO,
                              AU_STATUS,
                              PE_YEAR,
                              PE_TIPO,
                              PE_NUMERO,
                              AU_POSICIO,
                              AU_TIPO,
                              AU_TIPODIA,
                              AU_HORAS,
                              AU_DOBLES,
                              AU_TRIPLES,
                              AU_EXTRAS,
                              AU_DES_TRA,
                              AU_PER_CG,
                              AU_PER_SG )
         values ( @Empleado,
                  @Fecha,
                  @Horario,
                  @Status,
                  @Anio,
                  @Tipo,
                  @Numero,
                  @Posicion,
                  @Incidencia,
                  @TipoDia,
                  @Ordinarias,
                  @Dobles,
                  @Triples,
				  @Extras,
                  @Descanso,
                  @PermisoCG,
                  @PermisoSG );
    end
    else
    begin
          if ( @Usuario = 0 ) 
          begin
               
               if (( @TipoDia = 8 ) and ( @Tardes > 0 )) 
               begin
                 	Select @Tardes = 0;
                 	if ( @Incidencia = 'RE' ) Select @Incidencia = '';
               end
               update AUSENCIA set PE_YEAR = @Anio,
                                   PE_TIPO = @Tipo,
                                   PE_NUMERO = @Numero,
                                   AU_POSICIO = @Posicion,
                                   AU_TIPO = @Incidencia,
                                   AU_TIPODIA = @TipoDia,
                                   AU_HORAS = @Ordinarias,
                                   AU_DOBLES = @Dobles,
                                   AU_TRIPLES = @Triples,
                                   AU_EXTRAS = @Extras,
                                   AU_DES_TRA = @Descanso,
                                   AU_PER_CG = @PermisoCG,
                                   AU_PER_SG = @PermisoSG,
                                   AU_TARDES = @Tardes
               where ( CB_CODIGO = @Empleado ) and ( AU_FECHA = @Fecha );
          end
          else
               update AUSENCIA set PE_YEAR = @Anio,
                                   PE_TIPO = @Tipo,
                                   PE_NUMERO = @Numero,
                                   AU_DOBLES = @Dobles,
                                   AU_TRIPLES = @Triples,
                                   AU_EXTRAS = @Extras,
                                   AU_POSICIO = @Posicion
               where ( CB_CODIGO = @Empleado ) and ( AU_FECHA = @Fecha );
     end
     execute SP_CLAS_AUSENCIA @Empleado, @Fecha ;
GO

/* SP_CLAS_NOMINA: Cambiado para quitar los par�metros YEAR */
/* Patch 390 # Seq: 25 Modificado */
ALTER PROCEDURE SP_CLAS_NOMINA
    		@ANIO SMALLINT,
    		@TIPO SMALLINT,
    		@NUMERO SMALLINT,
    		@EMPLEADO INTEGER,
		@ROTATIVO CHAR(6) OUTPUT
AS
        SET NOCOUNT ON
	declare @FechaIni DATETIME;
 	declare @FechaFin DATETIME;
 	declare @ZONA_GE  CHAR(1);
 	declare @PUESTO   CHAR(6);
 	declare @CLASIFI  CHAR(6);
 	declare @TURNO    CHAR(6);
 	declare @PATRON   CHAR(1);
 	declare @NIVEL1   CHAR(6);
 	declare @NIVEL2   CHAR(6);
 	declare @NIVEL3   CHAR(6);
 	declare @NIVEL4   CHAR(6);
 	declare @NIVEL5   CHAR(6);
 	declare @NIVEL6   CHAR(6);
 	declare @NIVEL7   CHAR(6);
 	declare @NIVEL8   CHAR(6);
 	declare @NIVEL9   CHAR(6);
 	declare @SALARIO  NUMERIC(15,2);
 	declare @SAL_INT  NUMERIC(15,2);
 	declare @PROMEDIA CHAR(1);
 	declare @JORNADA  NUMERIC(15,2);
 	declare @D_TURNO  SMALLINT;
 	declare @ES_ROTA  CHAR(1);
 	declare @FEC_KAR  DATETIME;
 	declare @FEC_SAL  DATETIME;

  	select @FechaIni = PE_FEC_INI, 
		@FechaFin = PE_FEC_FIN
  	from   PERIODO
  	where  PE_YEAR = @Anio 
	and PE_TIPO = @Tipo 
	and PE_NUMERO = @Numero

  	select @ZONA_GE = CB_ZONA_GE, 
		@PUESTO = CB_PUESTO, 
		@CLASIFI = CB_CLASIFI, 
		@TURNO = CB_TURNO, 
		@PATRON = CB_PATRON,
         	@NIVEL1 = CB_NIVEL1, 
		@NIVEL2 = CB_NIVEL2, 
		@NIVEL3 = CB_NIVEL3, 
		@NIVEL4 = CB_NIVEL4, 
		@NIVEL5 = CB_NIVEL5,
         	@NIVEL6 = CB_NIVEL6, 
		@NIVEL7 = CB_NIVEL7, 
		@NIVEL8 = CB_NIVEL8, 
		@NIVEL9 = CB_NIVEL9,
         	@SALARIO = CB_SALARIO, 
		@SAL_INT = CB_SAL_INT, 
		@FEC_KAR = CB_FEC_KAR, 
		@FEC_SAL = CB_FEC_SAL
  	from   COLABORA
  	where  CB_CODIGO = @Empleado

  	if ( @FechaFin < @FEC_KAR )
  	begin
    		select
			@ZONA_GE = CB_ZONA_GE, 
			@PUESTO = CB_PUESTO, 
			@CLASIFI = CB_CLASIFI, 
			@TURNO = CB_TURNO, 
			@PATRON = CB_PATRON,
         		@NIVEL1 = CB_NIVEL1, 
			@NIVEL2 = CB_NIVEL2, 
			@NIVEL3 = CB_NIVEL3, 
			@NIVEL4 = CB_NIVEL4, 
			@NIVEL5 = CB_NIVEL5,
         		@NIVEL6 = CB_NIVEL6, 
			@NIVEL7 = CB_NIVEL7, 
			@NIVEL8 = CB_NIVEL8, 
			@NIVEL9 = CB_NIVEL9,
         		@SALARIO = CB_SALARIO, 
			@SAL_INT = CB_SAL_INT
    		from   SP_FECHA_KARDEX( @FechaFin, @Empleado )
  	end

  	select @PROMEDIA = GL_FORMULA
  	from   GLOBAL
  	where  GL_CODIGO = 42

  	if (( @PROMEDIA = 'S' ) and ( @FEC_SAL > @FechaIni )) 
        EXECUTE SP_RANGO_SALARIO @FechaIni, @FechaFin, @Empleado,
                                 @SALARIO OUTPUT, @SAL_INT OUTPUT

	EXECUTE SP_DIAS_JORNADA @Turno, @FechaIni, @FechaFin,
    				@D_Turno OUTPUT, @Jornada OUTPUT, @Es_Rota OUTPUT

  	if ( @ES_ROTA = 'S' )
    		SET @Rotativo = @Turno
  	else
    		SET @Rotativo = '';

  	update NOMINA
  	set   CB_ZONA_GE = @ZONA_GE,
        	CB_PUESTO  = @PUESTO,
        	CB_CLASIFI = @CLASIFI,
        	CB_TURNO   = @TURNO,
        	CB_PATRON  = @PATRON,
        	CB_NIVEL1  = @NIVEL1,
        	CB_NIVEL2  = @NIVEL2,
        	CB_NIVEL3  = @NIVEL3,
        	CB_NIVEL4  = @NIVEL4,
        	CB_NIVEL5  = @NIVEL5,
        	CB_NIVEL6  = @NIVEL6,
        	CB_NIVEL7  = @NIVEL7,
        	CB_NIVEL8  = @NIVEL8,
        	CB_NIVEL9  = @NIVEL9,
        	CB_SALARIO = @SALARIO,
        	CB_SAL_INT = @SAL_INT,
        	NO_JORNADA = @JORNADA,
        	NO_D_TURNO = @D_TURNO
  	where PE_YEAR = @Anio
	and PE_TIPO = @Tipo
	and PE_NUMERO = @Numero
	and CB_CODIGO = @Empleado;
GO

/* SP_ADD_NOMINA: Cambiado para quitar los par�metros YEAR */
/* Patch 390 # Seq: 27 Modificado */
ALTER PROCEDURE SP_ADD_NOMINA
    			@ANIO SMALLINT,
    			@TIPO SMALLINT,
    			@NUMERO SMALLINT,
    			@EMPLEADO INTEGER,
    			@ROTATIVO CHAR(6) OUTPUT
AS
  SET NOCOUNT ON
  INSERT INTO NOMINA
  ( PE_YEAR, PE_TIPO, PE_NUMERO, CB_CODIGO )
  VALUES ( @Anio, @Tipo, @Numero, @Empleado );


  exec SP_CLAS_NOMINA @Anio, @Tipo, @Numero, @Empleado, @ROTATIVO OUTPUT
GO

/* SP_CLAS_NOMINA_VACA: Cambiado para quitar los par�metros YEAR */
/* Patch 390 # Seq: 29 Modificado */
ALTER PROCEDURE SP_CLAS_NOMINA_VACA
    		@ANIO SMALLINT,
    		@TIPO SMALLINT,
    		@NUMERO SMALLINT,
    		@EMPLEADO INTEGER,
    		@ROTATIVO CHAR(6) OUTPUT,
    		@DIASVACA PESOS OUTPUT
AS
        SET NOCOUNT ON
  	declare @Existe INTEGER;

  	select @DiasVaca=sum( VA_PAGO )
  	from   VACACION
  	where ( CB_CODIGO = @Empleado  )
	and ( VA_TIPO = 1 )
	and ( VA_NOMYEAR = @Anio )
	and ( VA_NOMTIPO = @Tipo )
	and ( VA_NOMNUME = @Numero )

  	if ( @DiasVaca > 0 )
  	begin
	    	select @Existe = CB_CODIGO
    		from   NOMINA
    		where  ( CB_CODIGO = @Empleado ) 
		and ( PE_YEAR = @Anio ) 
		and ( PE_TIPO = @Tipo ) 
		and ( PE_NUMERO = @Numero )

    		if ( @Existe is NULL ) 
      			execute SP_ADD_NOMINA  @Anio, @Tipo, @Numero, @Empleado, @Rotativo

    		update NOMINA
    		set    NO_DIAS_VA = @DiasVaca
    		where  ( CB_CODIGO = @Empleado )
		and ( PE_YEAR = @Anio )
		and ( PE_TIPO = @Tipo )
		and ( PE_NUMERO = @Numero );
  	end

  	execute SP_CLAS_NOMINA @Anio, @Tipo, @Numero, @Empleado, @Rotativo OUTPUT;
GO

/* SP_BALANZA: Cambiado para quitar los par�metros YEAR */
/* Patch 390 # Seq: 35 Modificado */
ALTER procedure DBO.SP_BALANZA(
@USUARIO SMALLINT,
@ANIO SMALLINT,
@TIPO SMALLINT,
@NUMERO SMALLINT,
@EMPLEADO INTEGER,
@CONCEPTO SMALLINT,
@COLUMNA SMALLINT,
@DESCRIPCION CHAR( 30 ),
@MONTO NUMERIC( 15, 2 ),
@HORAS NUMERIC( 15, 2 ),
@REFERENCIA VARCHAR( 8 ) )
as
begin
     SET NOCOUNT ON
     declare @Percepcion SMALLINT;
     declare @Deduccion SMALLINT;
     select @Percepcion = COUNT(*) from DBO.TMPBALAN as T
     where ( T.TZ_USER = @USUARIO ) and
           ( T.CB_CODIGO = @EMPLEADO ) and
           ( T.TZ_NUM_PER > 0 );
     select @Deduccion = COUNT(*) from DBO.TMPBALAN as T
     where ( T.TZ_USER = @USUARIO ) and
           ( T.CB_CODIGO = @EMPLEADO ) and
           ( T.TZ_NUM_DED > 0 );
     if ( @COLUMNA = 1 )
     begin
          if ( @Percepcion >= @Deduccion )
          begin
               insert into DBO.TMPBALAN( TZ_CODIGO,
                                         CB_CODIGO,
                                         PE_YEAR,
                                         PE_TIPO,
                                         PE_NUMERO,
                                         TZ_USER,
                                         TZ_NUM_PER,
                                         TZ_MON_PER,
                                         TZ_DES_PER,
                                         TZ_HOR_PER,
                                         TZ_REF_PER ) values (
                                        @PERCEPCION,
                                        @EMPLEADO,
                                        @ANIO,
                                        @TIPO,
                                        @NUMERO,
                                        @USUARIO,
                                        @CONCEPTO,
                                        @MONTO,
                                        @DESCRIPCION,
                                        @HORAS,
                                        @REFERENCIA );
          end
          else
          begin
               update DBO.TMPBALAN set TZ_NUM_PER = @CONCEPTO,
                                       TZ_MON_PER = @MONTO,
                                       TZ_DES_PER = @DESCRIPCION,
                                       TZ_HOR_PER = @HORAS,
                                       TZ_REF_PER = @REFERENCIA
               where ( TZ_USER = @USUARIO ) and
                     ( CB_CODIGO = @EMPLEADO ) and
                     ( TZ_CODIGO = @PERCEPCION );
          end
     end
     else
     begin
          if ( @Deduccion >= @Percepcion  )
          begin
               insert into DBO.TMPBALAN( TZ_CODIGO,
                                         CB_CODIGO,
                                         PE_YEAR,
                                         PE_TIPO,
                                         PE_NUMERO,
                                         TZ_USER,
                                         TZ_NUM_DED,
                                         TZ_MON_DED,
                                         TZ_DES_DED,
                                         TZ_HOR_DED,
                                         TZ_REF_DED ) values (
                                        @DEDUCCION,
                                        @EMPLEADO,
                                        @ANIO,
                                        @TIPO,
                                        @NUMERO,
                                        @USUARIO,
                                        @CONCEPTO,
                                        @MONTO,
                                        @DESCRIPCION,
                                        @HORAS,
                                        @REFERENCIA );
          end
          else
          begin
               update DBO.TMPBALAN set TZ_NUM_DED = @CONCEPTO,
                                       TZ_MON_DED = @MONTO,
                                       TZ_DES_DED = @DESCRIPCION,
                                       TZ_HOR_DED = @HORAS,
                                       TZ_REF_DED = @REFERENCIA
               where ( TZ_USER = @USUARIO ) and
                     ( CB_CODIGO = @EMPLEADO ) and
                     ( TZ_CODIGO = @DEDUCCION );
          end;
     end;
end
GO

/* SP_LISTADO_BALANZA: Cambiado para quitar los par�metros YEAR */
/* Patch 390 # Seq: 49 Modificado */
ALTER PROCEDURE DBO.SP_LISTADO_BALANZA (
  @USUARIO SMALLINT,
  @ANIO SMALLINT,
  @TIPO SMALLINT,
  @NUMERO SMALLINT,
  @EMPLEADO INTEGER,
  @CONCEPTO SMALLINT,
  @COLUMNA SMALLINT,
  @DESCRIPCION CHAR(30),
  @MONTO NUMERIC(15, 2),
  @HORAS NUMERIC(15, 2),
  @REFERENCIA VARCHAR(8),
  @TP_NIVEL1 CHAR(6),
  @TP_NIVEL2 CHAR(6),
  @TP_NIVEL3 CHAR(6),
  @TP_NIVEL4 CHAR(6),
  @TP_NIVEL5 CHAR(6)
)  AS
BEGIN
     SET NOCOUNT ON
     DECLARE @PERCEPCION SMALLINT;
     DECLARE @DEDUCCION SMALLINT;

     SELECT @PERCEPCION = COUNT(*) FROM DBO.TMPBALA2
     WHERE TP_USER = @USUARIO AND
           CB_CODIGO = @EMPLEADO AND
           TP_NIVEL1 = @TP_NIVEL1 AND
           TP_NIVEL2 = @TP_NIVEL2 AND
           TP_NIVEL3 = @TP_NIVEL3 AND
           TP_NIVEL4 = @TP_NIVEL4 AND
           TP_NIVEL5 = @TP_NIVEL5 AND
           TP_NUM_PER > 0

     SELECT @DEDUCCION=COUNT(*) FROM TMPBALA2
     WHERE TP_USER = @USUARIO AND
           CB_CODIGO = @EMPLEADO AND
           TP_NIVEL1 = @TP_NIVEL1 AND
           TP_NIVEL2 = @TP_NIVEL2 AND
           TP_NIVEL3 = @TP_NIVEL3 AND
           TP_NIVEL4 = @TP_NIVEL4 AND
           TP_NIVEL5 = @TP_NIVEL5 AND
           TP_NUM_DED > 0

     IF (@COLUMNA = 1) 
     BEGIN
          IF (@PERCEPCION >= @DEDUCCION)
          BEGIN
               INSERT INTO TMPBALA2 ( TP_ORDEN,
                                      CB_CODIGO,
                                      PE_YEAR,
                                      PE_TIPO,
                                      PE_NUMERO,
                                      TP_USER,
                                      TP_NUM_PER,
                                      TP_MON_PER,
                                      TP_DES_PER,
                                      TP_HOR_PER,
                                      TP_REF_PER,
                                      TP_NIVEL1,
                                      TP_NIVEL2,
                                      TP_NIVEL3,
                                      TP_NIVEL4,
                                      TP_NIVEL5)
                            VALUES( @PERCEPCION,
                                    @EMPLEADO,
                                    @ANIO,
                                    @TIPO,
                                    @NUMERO,
                                    @USUARIO,
                                    @CONCEPTO,
                                    @MONTO,
                                    @DESCRIPCION,
                                    @HORAS,
                                    @REFERENCIA,
                                    @TP_NIVEL1,
                                    @TP_NIVEL2,
                                    @TP_NIVEL3,
                                    @TP_NIVEL4,
                                    @TP_NIVEL5 );

          END
          ELSE
          BEGIN
               UPDATE DBO.TMPBALA2 SET
                      TP_NUM_PER=@CONCEPTO,
                      TP_MON_PER=@MONTO,
                      TP_DES_PER=@DESCRIPCION,
                      TP_HOR_PER=@HORAS,
                      TP_REF_PER=@REFERENCIA
               WHERE TP_USER=@USUARIO AND
                     CB_CODIGO=@EMPLEADO AND
                     TP_NIVEL1 = @TP_NIVEL1 AND
                     TP_NIVEL2 = @TP_NIVEL2 AND
                     TP_NIVEL3 = @TP_NIVEL3 AND
                     TP_NIVEL4 = @TP_NIVEL4 AND
                     TP_NIVEL5 = @TP_NIVEL5 AND
                     TP_ORDEN=@PERCEPCION;
          END
     END
     ELSE
     BEGIN
          IF (@DEDUCCION >= @PERCEPCION)
          BEGIN
               INSERT INTO DBO.TMPBALA2 ( TP_ORDEN,
                                      CB_CODIGO,
                                      PE_YEAR,
                                      PE_TIPO,
                                      PE_NUMERO,
                                      TP_USER,
                                      TP_NUM_DED,
                                      TP_MON_DED,
                                      TP_DES_DED,
                                      TP_HOR_DED,
                                      TP_REF_DED,
                                      TP_NIVEL1,
                                      TP_NIVEL2,
                                      TP_NIVEL3,
                                      TP_NIVEL4,
                                      TP_NIVEL5)
                            VALUES( @DEDUCCION,
                                    @EMPLEADO,
                                    @ANIO,
                                    @TIPO,
                                    @NUMERO,
                                    @USUARIO,
                                    @CONCEPTO,
                                    @MONTO,
                                    @DESCRIPCION,
                                    @HORAS,
                                    @REFERENCIA,
                                    @TP_NIVEL1,
                                    @TP_NIVEL2,
                                    @TP_NIVEL3,
                                    @TP_NIVEL4,
                                    @TP_NIVEL5 );
          END
          ELSE
          BEGIN
               UPDATE DBO.TMPBALA2 SET
                      TP_NUM_DED=@CONCEPTO,
                      TP_MON_DED=@MONTO,
                      TP_DES_DED=@DESCRIPCION,
                      TP_HOR_DED=@HORAS,
                      TP_REF_DED=@REFERENCIA
               WHERE TP_USER=@USUARIO AND
                     CB_CODIGO=@EMPLEADO AND
                     TP_NIVEL1 = @TP_NIVEL1 AND
                     TP_NIVEL2 = @TP_NIVEL2 AND
                     TP_NIVEL3 = @TP_NIVEL3 AND
                     TP_NIVEL4 = @TP_NIVEL4 AND
                     TP_NIVEL5 = @TP_NIVEL5 AND
                     TP_ORDEN=@DEDUCCION;
          END;
     END;
END
GO

/* PREPARA_CALENDARIO: Cambiado para quitar los par�metros YEAR */
/* Patch 390 # Seq: 37 Modificado */
ALTER procedure DBO.PREPARA_CALENDARIO(
@USUARIO SMALLINT,
@EMPLEADO INTEGER,
@ANIO SMALLINT,
@MES SMALLINT,
@FECHA_MES DATETIME,
@FECHA_INI DATETIME,
@FECHA_FIN DATETIME )
as
begin
     SET NOCOUNT ON
     declare @CUANTOS Numeric( 15, 2 )
     declare @Fecha_Min DATETIME;
     declare @Fecha_Max DATETIME;
     declare @FechaActual DATETIME;
     declare @Posicion SMALLINT;
     declare @Fecha01 DATETIME;
     declare @Fecha02 DATETIME;
     declare @Fecha03 DATETIME;
     declare @Fecha04 DATETIME;
     declare @Fecha05 DATETIME;
     declare @Fecha06 DATETIME;
     declare @Fecha07 DATETIME;
     declare @Fecha08 DATETIME;
     declare @Fecha09 DATETIME;
     declare @Fecha10 DATETIME;
     declare @Fecha11 DATETIME;
     declare @Fecha12 DATETIME;
     declare @Fecha13 DATETIME;
     declare @Fecha14 DATETIME;
     declare @Fecha15 DATETIME;
     declare @Fecha16 DATETIME;
     declare @Fecha17 DATETIME;
     declare @Fecha18 DATETIME;
     declare @Fecha19 DATETIME;
     declare @Fecha20 DATETIME;
     declare @Fecha21 DATETIME;
     declare @Fecha22 DATETIME;
     declare @Fecha23 DATETIME;
     declare @Fecha24 DATETIME;
     declare @Fecha25 DATETIME;
     declare @Fecha26 DATETIME;
     declare @Fecha27 DATETIME;
     declare @Fecha28 DATETIME;
     declare @Fecha29 DATETIME;
     declare @Fecha30 DATETIME;
     declare @Fecha31 DATETIME;
     declare @STATUS SMALLINT;
     declare @TIPODIA SMALLINT;
     declare @HORAS   NUMERIC( 15, 2 );
     declare @EXTRAS  NUMERIC( 15, 2 );
     declare @DOBLES  NUMERIC( 15, 2 );
     declare @TARDES  NUMERIC( 15, 2 );
     declare @DES_TRA NUMERIC( 15, 2 );
     declare @PER_CG  NUMERIC( 15, 2 );
     declare @PER_SG  NUMERIC( 15, 2 );
     declare @TotHabil   SMALLINT;
     declare @TotSabado  SMALLINT;
     declare @TotDescan  SMALLINT;
     declare @TotNormal  SMALLINT;
     declare @TotIncapa  SMALLINT;
     declare @TotVaca    SMALLINT;
     declare @TotPer_CG  SMALLINT;
     declare @TotPer_SG  SMALLINT;
     declare @TotPer_FJ  SMALLINT;
     declare @TotSuspen  SMALLINT;
     declare @TotPer_OT  SMALLINT;
     declare @TotFestivo SMALLINT;
     declare @TotNo_Trab SMALLINT;
     declare @TotTrabajo SMALLINT;
     declare @TotFaltas  SMALLINT;
     declare @TotRetardo SMALLINT;
     declare @TotHoras   NUMERIC( 15, 2 );
     declare @TotExtras  NUMERIC( 15, 2 );
     declare @TotDobles  NUMERIC( 15, 2 );
     declare @TotDes_Tra NUMERIC( 15, 2 );
     declare @TotHor_CG  NUMERIC( 15, 2 );
     declare @TotHor_SG  NUMERIC( 15, 2 );
     declare @TotTardes  NUMERIC( 15, 2 );
     declare Temporal CURSOR for
        select A.AU_FECHA,
               A.AU_STATUS,
               A.AU_TIPODIA,
               A.AU_HORAS,
               A.AU_EXTRAS,
               A.AU_DOBLES,
               A.AU_TARDES,
               A.AU_DES_TRA,
               A.AU_PER_CG,
               A.AU_PER_SG
        from DBO.AUSENCIA as A where
        ( A.CB_CODIGO = @Empleado ) and
        ( A.AU_FECHA between @Fecha_Ini and @Fecha_Fin )
        order by A.AU_FECHA
     set @Cuantos = 0;
     set @Fecha01 = '12/30/1899';
     set @Fecha02 = @Fecha01;
     set @Fecha03 = @Fecha01;
     set @Fecha04 = @Fecha01;
     set @Fecha05 = @Fecha01;
     set @Fecha06 = @Fecha01;
     set @Fecha07 = @Fecha01;
     set @Fecha08 = @Fecha01;
     set @Fecha09 = @Fecha01;
     set @Fecha10 = @Fecha01;
     set @Fecha11 = @Fecha01;
     set @Fecha12 = @Fecha01;
     set @Fecha13 = @Fecha01;
     set @Fecha14 = @Fecha01;
     set @Fecha15 = @Fecha01;
     set @Fecha16 = @Fecha01;
     set @Fecha17 = @Fecha01;
     set @Fecha18 = @Fecha01;
     set @Fecha19 = @Fecha01;
     set @Fecha20 = @Fecha01;
     set @Fecha21 = @Fecha01;
     set @Fecha22 = @Fecha01;
     set @Fecha23 = @Fecha01;
     set @Fecha24 = @Fecha01;
     set @Fecha25 = @Fecha01;
     set @Fecha26 = @Fecha01;
     set @Fecha27 = @Fecha01;
     set @Fecha28 = @Fecha01;
     set @Fecha29 = @Fecha01;
     set @Fecha30 = @Fecha01;
     set @Fecha31 = @Fecha01;
     set @TotHabil   = 0;
     set @TotSabado  = 0;
     set @TotDescan  = 0;
     set @TotNormal  = 0;
     set @TotIncapa  = 0;
     set @TotVaca    = 0;
     set @TotPer_CG  = 0;
     set @TotPer_SG  = 0;
     set @TotPer_FJ  = 0;
     set @TotSuspen  = 0;
     set @TotPer_OT  = 0;
     set @TotFestivo = 0;
     set @TotNo_Trab = 0;
     set @TotTrabajo = 0;
     set @TotFaltas  = 0;
     set @TotRetardo = 0;
     set @TotHoras   = 0;
     set @TotExtras  = 0;
     set @TotDobles  = 0;
     set @TotDes_Tra = 0;
     set @TotHor_CG  = 0;
     set @TotHor_SG  = 0;
     set @TotTardes  = 0;
     Open Temporal;
     fetch NEXT from Temporal
     into @FechaActual,
          @Status,
          @TipoDia,
          @Horas,
          @Extras,
          @Dobles,
          @Tardes,
          @Des_Tra,
          @Per_CG,
          @Per_SG;
     while ( @@Fetch_Status = 0 )
     begin
          if ( @TipoDia = 9 )
             set @TotNo_Trab = @TotNo_Trab + 1;
          else
          begin
               if ( @Cuantos = 0 )
                  set @Fecha_Min = @FechaActual;
               set @Fecha_Max = @FechaActual;
               set @Cuantos = @Cuantos + 1;
               if ( @Status = 0 )
                  set @TotHabil = @TotHabil + 1
               else
                   if ( @Status = 1 )
                      set @TotSabado = @TotSabado + 1;
                   else
                       set @TotDescan = @TotDescan + 1;
               if ( @TipoDia = 0 ) set @TotNormal = @TotNormal + 1
               else
               if ( @TipoDia = 1 ) set @TotIncapa = @TotIncapa + 1
               else
               if ( @TipoDia = 2 ) set @TotVaca = @TotVaca + 1
               else
               if ( @TipoDia = 3 ) set @TotPer_CG = @TotPer_CG + 1
               else
               if ( @TipoDia = 4 ) set @TotPer_SG = @TotPer_SG + 1
               else
               if ( @TipoDia = 5 ) set @TotPer_FJ = @TotPer_FJ + 1
               else
               if ( @TipoDia = 6 ) set @TotSuspen = @TotSuspen + 1
               else
               if ( @TipoDia = 7 ) set @TotPer_OT = @TotPer_OT + 1
               else
                   set @TotFestivo = @TotFestivo + 1;
               if ( @Horas > 0 )
                  set @TotTrabajo = @TotTrabajo + 1
               else
                   if ( ( @Status = 0 ) and ( @TipoDia = 0 ) )
                      set @TotFaltas = @TotFaltas + 1;
               if ( @Tardes > 0 )
                  set @TotRetardo = @TotRetardo + 1;
               set @TotHoras   = @TotHoras + @Horas;
               set @TotExtras  = @TotExtras + @Extras;
               set @TotDobles  = @TotDobles + @Dobles;
               set @TotDes_Tra = @TotDes_Tra + @Des_Tra;
               set @TotHor_CG  = @TotHor_CG + @Per_CG;
               set @TotHor_SG  = @TotHor_SG + @Per_SG;
               set @TotTardes  = @TotTardes + @Tardes;
          end
          set @Posicion = CONVERT( SmallInt, @FechaActual - @Fecha_Mes ) + 1;
          if ( @Posicion =  1 )
             set @Fecha01 = @FechaActual
          else
          if ( @Posicion =  2 )
             set @Fecha02 = @FechaActual
          else
          if ( @Posicion =  3 )
             set @Fecha03 = @FechaActual
          else
          if ( @Posicion =  4 )
             set @Fecha04 = @FechaActual
          else
          if ( @Posicion =  5 )
             set @Fecha05 = @FechaActual
          else
          if ( @Posicion =  6 )
             set @Fecha06 = @FechaActual
          else
          if ( @Posicion =  7 )
             set @Fecha07 = @FechaActual
          else
          if ( @Posicion =  8 )
             set @Fecha08 = @FechaActual
          else
          if ( @Posicion =  9 )
             set @Fecha09 = @FechaActual
          else
          if ( @Posicion = 10 )
             set @Fecha10 = @FechaActual
          else
          if ( @Posicion = 11 )
             set @Fecha11 = @FechaActual
          else
          if ( @Posicion = 12 )
             set @Fecha12 = @FechaActual
          else
          if ( @Posicion = 13 )
             set @Fecha13 = @FechaActual
          else
          if ( @Posicion = 14 )
             set @Fecha14 = @FechaActual
          else
          if ( @Posicion = 15 )
             set @Fecha15 = @FechaActual
          else
          if ( @Posicion = 16 )
             set @Fecha16 = @FechaActual
          else
          if ( @Posicion = 17 )
             set @Fecha17 = @FechaActual
          else
          if ( @Posicion = 18 )
             set @Fecha18 = @FechaActual
          else
          if ( @Posicion = 19 )
             set @Fecha19 = @FechaActual
          else
          if ( @Posicion = 20 )
             set @Fecha20 = @FechaActual
          else
          if ( @Posicion = 21 )
             set @Fecha21 = @FechaActual
          else
          if ( @Posicion = 22 )
             set @Fecha22 = @FechaActual
          else
          if ( @Posicion = 23 )
             set @Fecha23 = @FechaActual
          else
          if ( @Posicion = 24 )
             set @Fecha24 = @FechaActual
          else
          if ( @Posicion = 25 )
             set @Fecha25 = @FechaActual
          else
          if ( @Posicion = 26 )
             set @Fecha26 = @FechaActual
          else
          if ( @Posicion = 27 )
             set @Fecha27 = @FechaActual
          else
          if ( @Posicion = 28 )
             set @Fecha28 = @FechaActual
          else
          if ( @Posicion = 29 )
             set @Fecha29 = @FechaActual
          else
          if ( @Posicion = 30 )
             set @Fecha30 = @FechaActual
          else
          if ( @Posicion = 31 )
             set @Fecha31 = @FechaActual;
          fetch NEXT from Temporal
          into @FechaActual,
               @Status,
               @TipoDia,
               @Horas,
               @Extras,
               @Dobles,
               @Tardes,
               @Des_Tra,
               @Per_CG,
               @Per_SG;
     end;
	 close Temporal;
	 deallocate Temporal;
     if ( @Cuantos > 0 )
     begin
          insert into DBO.TMPCALEN
            ( US_CODIGO, CB_CODIGO, TL_YEAR, TL_MES, TL_FEC_MIN, TL_FEC_MAX, TL_NUMERO,
              TL_FECHA01, TL_FECHA02, TL_FECHA03, TL_FECHA04, TL_FECHA05, TL_FECHA06, TL_FECHA07, TL_FECHA08, TL_FECHA09, TL_FECHA10,
              TL_FECHA11, TL_FECHA12, TL_FECHA13, TL_FECHA14, TL_FECHA15, TL_FECHA16, TL_FECHA17, TL_FECHA18, TL_FECHA19, TL_FECHA20,
              TL_FECHA21, TL_FECHA22, TL_FECHA23, TL_FECHA24, TL_FECHA25, TL_FECHA26, TL_FECHA27, TL_FECHA28, TL_FECHA29, TL_FECHA30, TL_FECHA31,
              TL_HABIL, TL_SABADO, TL_DESCAN, TL_NORMAL, TL_INCAPA, TL_VACA, TL_PER_CG, TL_PER_SG, TL_PER_FJ, TL_SUSPEN, TL_PER_OT, TL_FESTIVO,
              TL_NO_TRAB, TL_TRABAJO, TL_FALTAS, TL_RETARDO, TL_HORAS, TL_EXTRAS, TL_DOBLES, TL_DES_TRA, TL_HOR_CG, TL_HOR_SG, TL_TARDES )
          values
            ( @USUARIO, @Empleado, @Anio, @Mes, @Fecha_Min, @Fecha_Max, @Cuantos,
              @Fecha01, @Fecha02, @Fecha03, @Fecha04, @Fecha05, @Fecha06, @Fecha07, @Fecha08, @Fecha09, @Fecha10,
              @Fecha11, @Fecha12, @Fecha13, @Fecha14, @Fecha15, @Fecha16, @Fecha17, @Fecha18, @Fecha19, @Fecha20,
              @Fecha21, @Fecha22, @Fecha23, @Fecha24, @Fecha25, @Fecha26, @Fecha27, @Fecha28, @Fecha29, @Fecha30, @Fecha31,
              @TotHabil, @TotSabado, @TotDescan, @TotNormal, @TotIncapa, @TotVaca, @TotPer_CG, @TotPer_SG, @TotPer_FJ, @TotSuspen, @TotPer_OT, @TotFestivo,
              @TotNo_Trab, @TotTrabajo, @TotFaltas, @TotRetardo, @TotHoras, @TotExtras, @TotDobles, @TotDes_Tra, @TotHor_CG, @TotHor_SG, @TotTardes );
     end;
end
GO

/* PREPARA_CALENDARIO_HORAS: Cambiado para quitar los par�metros YEAR */
/* Patch 390 # Seq: 45 Modificado */
ALTER procedure DBO.PREPARA_CALENDARIO_HORAS(
@USUARIO SMALLINT,
@EMPLEADO INTEGER,
@ANIO SMALLINT,
@MES SMALLINT,
@FECHA_MES DATETIME,
@FECHA_INI DATETIME,
@FECHA_FIN DATETIME,
@CAMPO SMALLINT,
@PRIMERO SMALLINT )
as
begin
     SET NOCOUNT ON
     declare @Cuantos NUMERIC(15, 2);
     declare @Fecha_Min DATETIME;
     declare @Fecha_Max DATETIME;
     declare @FechaActual DATETIME;
     declare @Posicion SMALLINT;
     declare @Tipo CHAR(3);
     declare @AuTipo01 CHAR(3);
     declare @AuTipo02 CHAR(3);
     declare @AuTipo03 CHAR(3);
     declare @AuTipo04 CHAR(3);
     declare @AuTipo05 CHAR(3);
     declare @AuTipo06 CHAR(3);
     declare @AuTipo07 CHAR(3);
     declare @AuTipo08 CHAR(3);
     declare @AuTipo09 CHAR(3);
     declare @AuTipo10 CHAR(3);
     declare @AuTipo11 CHAR(3);
     declare @AuTipo12 CHAR(3);
     declare @AuTipo13 CHAR(3);
     declare @AuTipo14 CHAR(3);
     declare @AuTipo15 CHAR(3);
     declare @AuTipo16 CHAR(3);
     declare @AuTipo17 CHAR(3);
     declare @AuTipo18 CHAR(3);
     declare @AuTipo19 CHAR(3);
     declare @AuTipo20 CHAR(3);
     declare @AuTipo21 CHAR(3);
     declare @AuTipo22 CHAR(3);
     declare @AuTipo23 CHAR(3);
     declare @AuTipo24 CHAR(3);
     declare @AuTipo25 CHAR(3);
     declare @AuTipo26 CHAR(3);
     declare @AuTipo27 CHAR(3);
     declare @AuTipo28 CHAR(3);
     declare @AuTipo29 CHAR(3);
     declare @AuTipo30 CHAR(3);
     declare @AuTipo31 CHAR(3);
     declare @Monto NUMERIC( 15, 2 );
     declare @Monto01 NUMERIC( 15, 2 );
     declare @Monto02 NUMERIC( 15, 2 );
     declare @Monto03 NUMERIC( 15, 2 );
     declare @Monto04 NUMERIC( 15, 2 );
     declare @Monto05 NUMERIC( 15, 2 );
     declare @Monto06 NUMERIC( 15, 2 );
     declare @Monto07 NUMERIC( 15, 2 );
     declare @Monto08 NUMERIC( 15, 2 );
     declare @Monto09 NUMERIC( 15, 2 );
     declare @Monto10 NUMERIC( 15, 2 );
     declare @Monto11 NUMERIC( 15, 2 );
     declare @Monto12 NUMERIC( 15, 2 );
     declare @Monto13 NUMERIC( 15, 2 );
     declare @Monto14 NUMERIC( 15, 2 );
     declare @Monto15 NUMERIC( 15, 2 );
     declare @Monto16 NUMERIC( 15, 2 );
     declare @Monto17 NUMERIC( 15, 2 );
     declare @Monto18 NUMERIC( 15, 2 );
     declare @Monto19 NUMERIC( 15, 2 );
     declare @Monto20 NUMERIC( 15, 2 );
     declare @Monto21 NUMERIC( 15, 2 );
     declare @Monto22 NUMERIC( 15, 2 );
     declare @Monto23 NUMERIC( 15, 2 );
     declare @Monto24 NUMERIC( 15, 2 );
     declare @Monto25 NUMERIC( 15, 2 );
     declare @Monto26 NUMERIC( 15, 2 );
     declare @Monto27 NUMERIC( 15, 2 );
     declare @Monto28 NUMERIC( 15, 2 );
     declare @Monto29 NUMERIC( 15, 2 );
     declare @Monto30 NUMERIC( 15, 2 );
     declare @Monto31 NUMERIC( 15, 2 );
     declare @STATUS SMALLINT;
     declare @TIPODIA SMALLINT;
     declare @HORAS   NUMERIC( 15, 2 );
     declare @EXTRAS  NUMERIC( 15, 2 );
     declare @DOBLES  NUMERIC( 15, 2 );
     declare @TRIPLES NUMERIC( 15, 2 );
     declare @TARDES  NUMERIC( 15, 2 );
     declare @DES_TRA NUMERIC( 15, 2 );
     declare @PER_CG  NUMERIC( 15, 2 );
     declare @PER_SG  NUMERIC( 15, 2 );
     declare @TotHabil   SMALLINT;
     declare @TotSabado  SMALLINT;
     declare @TotDescan  SMALLINT;
     declare @TotNormal  SMALLINT;
     declare @TotIncapa  SMALLINT;
     declare @TotVaca    SMALLINT;
     declare @TotPer_CG  SMALLINT;
     declare @TotPer_SG  SMALLINT;
     declare @TotPer_FJ  SMALLINT;
     declare @TotSuspen  SMALLINT;
     declare @TotPer_OT  SMALLINT;
     declare @TotFestivo SMALLINT;
     declare @TotNo_Trab SMALLINT;
     declare @TotTrabajo SMALLINT;
     declare @TotFaltas  SMALLINT;
     declare @TotRetardo SMALLINT;
     declare @TotHoras   NUMERIC( 15, 2 );
     declare @TotExtras  NUMERIC( 15, 2 );
     declare @TotDobles  NUMERIC( 15, 2 );
     declare @TotDes_Tra NUMERIC( 15, 2 );
     declare @TotHor_CG  NUMERIC( 15, 2 );
     declare @TotHor_SG  NUMERIC( 15, 2 );
     declare @TotTardes  NUMERIC( 15, 2 );
     declare Temporal CURSOR for
        select A.AU_FECHA,
               A.AU_STATUS,
               A.AU_TIPODIA,
               A.AU_HORAS,
               A.AU_EXTRAS,
               A.AU_DOBLES,
               A.AU_TRIPLES,
               A.AU_TIPO,
               A.AU_TARDES,
               A.AU_DES_TRA,
               A.AU_PER_CG,
               A.AU_PER_SG
        from DBO.AUSENCIA as A where
        ( A.CB_CODIGO = @Empleado ) and
        ( A.AU_FECHA between @FECHA_INI and @FECHA_FIN )
        order by A.AU_FECHA

     set @Cuantos = 0;
     set @Monto01 = 0;
     set @Monto02 = @Monto01;
     set @Monto03 = @Monto01;
     set @Monto04 = @Monto01;
     set @Monto05 = @Monto01;
     set @Monto06 = @Monto01;
     set @Monto07 = @Monto01;
     set @Monto08 = @Monto01;
     set @Monto09 = @Monto01;
     set @Monto10 = @Monto01;
     set @Monto11 = @Monto01;
     set @Monto12 = @Monto01;
     set @Monto13 = @Monto01;
     set @Monto14 = @Monto01;
     set @Monto15 = @Monto01;
     set @Monto16 = @Monto01;
     set @Monto17 = @Monto01;
     set @Monto18 = @Monto01;
     set @Monto19 = @Monto01;
     set @Monto20 = @Monto01;
     set @Monto21 = @Monto01;
     set @Monto22 = @Monto01;
     set @Monto23 = @Monto01;
     set @Monto24 = @Monto01;
     set @Monto25 = @Monto01;
     set @Monto26 = @Monto01;
     set @Monto27 = @Monto01;
     set @Monto28 = @Monto01;
     set @Monto29 = @Monto01;
     set @Monto30 = @Monto01;
     set @Monto31 = @Monto01;
     set @AuTipo01 = '';
     set @AuTipo02 = @AuTipo01;
     set @AuTipo03 = @AuTipo01;
     set @AuTipo04 = @AuTipo01;
     set @AuTipo05 = @AuTipo01;
     set @AuTipo06 = @AuTipo01;
     set @AuTipo07 = @AuTipo01;
     set @AuTipo08 = @AuTipo01;
     set @AuTipo09 = @AuTipo01;
     set @AuTipo10 = @AuTipo01;
     set @AuTipo11 = @AuTipo01;
     set @AuTipo12 = @AuTipo01;
     set @AuTipo13 = @AuTipo01;
     set @AuTipo14 = @AuTipo01;
     set @AuTipo15 = @AuTipo01;
     set @AuTipo16 = @AuTipo01;
     set @AuTipo17 = @AuTipo01;
     set @AuTipo18 = @AuTipo01;
     set @AuTipo19 = @AuTipo01;
     set @AuTipo20 = @AuTipo01;
     set @AuTipo21 = @AuTipo01;
     set @AuTipo22 = @AuTipo01;
     set @AuTipo23 = @AuTipo01;
     set @AuTipo24 = @AuTipo01;
     set @AuTipo25 = @AuTipo01;
     set @AuTipo26 = @AuTipo01;
     set @AuTipo27 = @AuTipo01;
     set @AuTipo28 = @AuTipo01;
     set @AuTipo29 = @AuTipo01;
     set @AuTipo30 = @AuTipo01;
     set @AuTipo31 = @AuTipo01;
     set @TotHabil   = 0;
     set @TotSabado  = 0;
     set @TotDescan  = 0;
     set @TotNormal  = 0;
     set @TotIncapa  = 0;
     set @TotVaca    = 0;
     set @TotPer_CG  = 0;
     set @TotPer_SG  = 0;
     set @TotPer_FJ  = 0;
     set @TotSuspen  = 0;
     set @TotPer_OT  = 0;
     set @TotFestivo = 0;
     set @TotNo_Trab = 0;
     set @TotTrabajo = 0;
     set @TotFaltas  = 0;
     set @TotRetardo = 0;
     set @TotHoras   = 0;
     set @TotExtras  = 0;
     set @TotDobles  = 0;
     set @TotDes_Tra = 0;
     set @TotHor_CG  = 0;
     set @TotHor_SG  = 0;
     set @TotTardes  = 0;
     Open Temporal;
     fetch NEXT from Temporal
     into @FechaActual,
          @Status,
          @TipoDia,
          @Horas,
          @Extras,
          @Dobles,
          @Triples,
          @Tipo,
          @Tardes,
          @Des_Tra,
          @Per_CG,
          @Per_SG;
     while ( @@Fetch_Status = 0 )
     begin
          if ( @TipoDia = 9 )
          begin
               if ( @Primero = 1 )
                  set @TotNo_Trab = @TotNo_Trab + 1;
          end
          else
          begin
               if ( @Cuantos = 0 )
                  set @Fecha_Min = @FechaActual;
               set @Fecha_Max = @FechaActual;
               set @Cuantos = @Cuantos + 1;
               if ( @Primero = 1 )
               begin
                    if ( @Status = 0 )
                       set @TotHabil = @TotHabil + 1
                    else
                        if ( @Status = 1 )
                            set @TotSabado = @TotSabado + 1
                        else
                            set @TotDescan = @TotDescan + 1;
                    if ( @TipoDia = 0 )
                       set @TotNormal = @TotNormal + 1
                    else
                    if ( @TipoDia = 1 )
                       set @TotIncapa = @TotIncapa + 1
                    else
                    if ( @TipoDia = 2 )
                       set @TotVaca = @TotVaca + 1
                    else
                    if ( @TipoDia = 3 )
                       set @TotPer_CG = @TotPer_CG + 1
                    else
                    if ( @TipoDia = 4 )
                       set @TotPer_SG = @TotPer_SG + 1
                    else
                    if ( @TipoDia = 5 )
                       set @TotPer_FJ = @TotPer_FJ + 1
                    else
                    if ( @TipoDia = 6 )
                       set @TotSuspen = @TotSuspen + 1
                    else
                    if ( @TipoDia = 7 )
                       set @TotPer_OT = @TotPer_OT + 1
                    else
                        set @TotFestivo = @TotFestivo + 1;
                    if ( @Horas > 0 )
                       set @TotTrabajo = @TotTrabajo + 1
                    else
                        if ( ( @Status = 0 ) AND ( @TipoDia = 0 ) )
                           set @TotFaltas = @TotFaltas + 1;
                    if ( @Tardes > 0 )
                       set @TotRetardo = @TotRetardo + 1;
                    set @TotHoras   = @TotHoras + @Horas;
                    set @TotExtras  = @TotExtras + @Extras;
                    set @TotDobles  = @TotDobles + @Dobles;
                    set @TotDes_Tra = @TotDes_Tra + @Des_Tra;
                    set @TotHor_CG  = @TotHor_CG + @Per_CG;
                    set @TotHor_SG  = @TotHor_SG + @Per_SG;
                    set @TotTardes  = @TotTardes + @Tardes;
               end;
          end;
          if ( @Campo = 0 )
             set @Monto = @Horas
          else
          if ( @Campo = 1 )
             set @Monto = @Extras
          else
          if ( @Campo = 2 )
             set @Monto = @Dobles
          else
          if ( @Campo = 3 )
             set @Monto = @Triples
          else
          if ( @Campo = 4 )
             set @Monto = @Tardes
          else
          if ( @Campo = 5 )
             set @Monto = @Des_Tra
          else
          if ( @Campo = 6 )
             set @Monto = @Per_CG
          else
          if ( @Campo = 7 )
             set @Monto = @Per_SG
          else
          if ( @Campo = 8 )
             set @Monto = @Status
          else
          if ( @Campo = 9 )
             set @Monto = @TipoDia
          else
              set @Monto = 0;
          set @Posicion = CONVERT( SmallInt, @FechaActual - @Fecha_Mes ) + 1;
          if ( @Posicion =  1 )
          begin
               set @AuTipo01 = @Tipo;
               set @Monto01 = @Monto;
          end
          else if ( @Posicion =  2 )
          begin
               set @AuTipo02 = @Tipo;
               set @Monto02 = @Monto;
          end
          else if ( @Posicion =  3 )
          begin
               set @AuTipo03 = @Tipo;
               set @Monto03 = @Monto;
          end
          else if ( @Posicion =  4 )
          begin
               set @AuTipo04 = @Tipo;
               set @Monto04 = @Monto;
          end
          else if ( @Posicion =  5 )
          begin
               set @AuTipo05 = @Tipo;
               set @Monto05 = @Monto;
          end
          else if ( @Posicion =  6 )
          begin
               set @AuTipo06 = @Tipo;
               set @Monto06 = @Monto;
          end
          else if ( @Posicion =  7 )
          begin
               set @AuTipo07 = @Tipo;
               set @Monto07 = @Monto;
          end
          else if ( @Posicion =  8 )
          begin
               set @AuTipo08 = @Tipo;
               set @Monto08 = @Monto;
          end
          else if ( @Posicion =  9 )
          begin
               set @AuTipo09 = @Tipo;
               set @Monto09 = @Monto;
          end
          else if ( @Posicion = 10 )
          begin
               set @AuTipo10 = @Tipo;
               set @Monto10 = @Monto;
          end
          else if ( @Posicion = 11 )
          begin
               set @AuTipo11 = @Tipo;
               set @Monto11 = @Monto;
          end
          else if ( @Posicion = 12 )
          begin
               set @AuTipo12 = @Tipo;
               set @Monto12 = @Monto;
          end
          else if ( @Posicion = 13 )
          begin
               set @AuTipo13 = @Tipo;
               set @Monto13 = @Monto;
          end
          else if ( @Posicion = 14 )
          begin
               set @AuTipo14 = @Tipo;
               set @Monto14 = @Monto;
          end
          else if ( @Posicion = 15 )
          begin
               set @AuTipo15 = @Tipo;
               set @Monto15 = @Monto;
          end
          else if ( @Posicion = 16 )
          begin
               set @AuTipo16 = @Tipo;
               set @Monto16 = @Monto;
          end
          else if ( @Posicion = 17 )
          begin
               set @AuTipo17 = @Tipo;
               set @Monto17 = @Monto;
          end
          else if ( @Posicion = 18 )
          begin
               set @AuTipo18 = @Tipo;
               set @Monto18 = @Monto;
          end
          else if ( @Posicion = 19 )
          begin
               set @AuTipo19 = @Tipo;
               set @Monto19 = @Monto;
          end
          else if ( @Posicion = 20 )
          begin
               set @AuTipo20 = @Tipo;
               set @Monto20 = @Monto;
          end
          else if ( @Posicion = 21 )
          begin
               set @AuTipo21 = @Tipo;
               set @Monto21 = @Monto;
          end
          else if ( @Posicion = 22 )
          begin
               set @AuTipo22 = @Tipo;
               set @Monto22 = @Monto;
          end
          else if ( @Posicion = 23 )
          begin
               set @AuTipo23 = @Tipo;
               set @Monto23 = @Monto;
          end
          else if ( @Posicion = 24 )
          begin
               set @AuTipo24 = @Tipo;
               set @Monto24 = @Monto;
          end
          else if ( @Posicion = 25 )
          begin
               set @AuTipo25 = @Tipo;
               set @Monto25 = @Monto;
          end
          else if ( @Posicion = 26 )
          begin
               set @AuTipo26 = @Tipo;
               set @Monto26 = @Monto;
          end
          else if ( @Posicion = 27 )
          begin
               set @AuTipo27 = @Tipo;
               set @Monto27 = @Monto;
          end
          else if ( @Posicion = 28 )
          begin
               set @AuTipo28 = @Tipo;
               set @Monto28 = @Monto;
          end
          else if ( @Posicion = 29 )
          begin
               set @AuTipo29 = @Tipo;
               set @Monto29 = @Monto;
          end
          else if ( @Posicion = 30 )
          begin
               set @AuTipo30 = @Tipo;
               set @Monto30 = @Monto;
          end
          else if ( @Posicion = 31 )
          begin
               set @AuTipo31 = @Tipo;
               set @Monto31 = @Monto;
          end
          fetch NEXT from Temporal
          into @FechaActual,
               @Status,
               @TipoDia,
               @Horas,
               @Extras,
               @Dobles,
               @Triples,
               @Tipo,
               @Tardes,
               @Des_Tra,
               @Per_CG,
               @Per_SG;
     end
	 close Temporal;
	 deallocate Temporal;
     if ( @Cuantos > 0 )
     begin
          insert into DBO.TMPCALHR
         ( US_CODIGO, CB_CODIGO, TL_YEAR, TL_MES, TL_CAMPO, TL_FEC_MIN, TL_FEC_MAX, TL_NUMERO,
           TL_CAMPO01, TL_CAMPO02, TL_CAMPO03, TL_CAMPO04, TL_CAMPO05, TL_CAMPO06, TL_CAMPO07, TL_CAMPO08, TL_CAMPO09, TL_CAMPO10,
           TL_CAMPO11, TL_CAMPO12, TL_CAMPO13, TL_CAMPO14, TL_CAMPO15, TL_CAMPO16, TL_CAMPO17, TL_CAMPO18, TL_CAMPO19, TL_CAMPO20,
           TL_CAMPO21, TL_CAMPO22, TL_CAMPO23, TL_CAMPO24, TL_CAMPO25, TL_CAMPO26, TL_CAMPO27, TL_CAMPO28, TL_CAMPO29, TL_CAMPO30, TL_CAMPO31,
           TL_TIPO01, TL_TIPO02, TL_TIPO03, TL_TIPO04, TL_TIPO05, TL_TIPO06, TL_TIPO07, TL_TIPO08, TL_TIPO09, TL_TIPO10,
           TL_TIPO11, TL_TIPO12, TL_TIPO13, TL_TIPO14, TL_TIPO15, TL_TIPO16, TL_TIPO17, TL_TIPO18, TL_TIPO19, TL_TIPO20,
           TL_TIPO21, TL_TIPO22, TL_TIPO23, TL_TIPO24, TL_TIPO25, TL_TIPO26, TL_TIPO27, TL_TIPO28, TL_TIPO29, TL_TIPO30, TL_TIPO31,
           TL_HABIL, TL_SABADO, TL_DESCAN, TL_NORMAL, TL_INCAPA, TL_VACA, TL_PER_CG, TL_PER_SG, TL_PER_FJ, TL_SUSPEN, TL_PER_OT, TL_FESTIVO,
           TL_NO_TRAB, TL_TRABAJO, TL_FALTAS, TL_RETARDO, TL_HORAS, TL_EXTRAS, TL_DOBLES, TL_DES_TRA, TL_HOR_CG, TL_HOR_SG, TL_TARDES )
       values
         ( @Usuario, @Empleado, @Anio, @Mes, @Campo, @Fecha_Min, @Fecha_Max, @Cuantos,
           @Monto01, @Monto02, @Monto03, @Monto04, @Monto05, @Monto06, @Monto07, @Monto08, @Monto09, @Monto10,
           @Monto11, @Monto12, @Monto13, @Monto14, @Monto15, @Monto16, @Monto17, @Monto18, @Monto19, @Monto20,
           @Monto21, @Monto22, @Monto23, @Monto24, @Monto25, @Monto26, @Monto27, @Monto28, @Monto29, @Monto30, @Monto31,
           @AuTipo01, @AuTipo02, @AuTipo03, @AuTipo04, @AuTipo05, @AuTipo06, @AuTipo07, @AuTipo08, @AuTipo09, @AuTipo10,
           @AuTipo11, @AuTipo12, @AuTipo13, @AuTipo14, @AuTipo15, @AuTipo16, @AuTipo17, @AuTipo18, @AuTipo19, @AuTipo20,
           @AuTipo21, @AuTipo22, @AuTipo23, @AuTipo24, @AuTipo25, @AuTipo26, @AuTipo27, @AuTipo28, @AuTipo29, @AuTipo30, @AuTipo31,
           @TotHabil, @TotSabado, @TotDescan, @TotNormal, @TotIncapa, @TotVaca, @TotPer_CG, @TotPer_SG, @TotPer_FJ, @TotSuspen, @TotPer_OT, @TotFestivo,
           @TotNo_Trab, @TotTrabajo, @TotFaltas, @TotRetardo, @TotHoras, @TotExtras, @TotDobles, @TotDes_Tra, @TotHor_CG, @TotHor_SG, @TotTardes );
     end;
end
GO

/**************************************************/
/* D:\3Win_13\Datos\SQLServer\3DatosFunciones.sql */
/**************************************************/

/* SP_AS: Cambiado para quitar los par�metros YEAR */
/* Patch 390 # Seq: 163 Modificado */
ALTER FUNCTION SP_AS (
  @Concepto SMALLINT,
  @MesIni SMALLINT,
  @MesFin SMALLINT,
  @Anio SMALLINT,
  @Empleado INTEGER )
RETURNS NUMERIC(15,2)
AS   
BEGIN
  declare @Mes_01 NUMERIC(15,2)
  declare @Mes_02 NUMERIC(15,2)
  declare @Mes_03 NUMERIC(15,2)
  declare @Mes_04 NUMERIC(15,2)
  declare @Mes_05 NUMERIC(15,2)
  declare @Mes_06 NUMERIC(15,2)
  declare @Mes_07 NUMERIC(15,2)
  declare @Mes_08 NUMERIC(15,2)
  declare @Mes_09 NUMERIC(15,2)
  declare @Mes_10 NUMERIC(15,2)
  declare @Mes_11 NUMERIC(15,2)
  declare @Mes_12 NUMERIC(15,2)
  declare @Mes_13 NUMERIC(15,2)
  declare @Resultado Pesos

  select @Mes_01 = AC_MES_01, @Mes_02 = AC_MES_02, @Mes_03 = AC_MES_03, @Mes_04 = AC_MES_04,
         @Mes_05 = AC_MES_05, @Mes_06 = AC_MES_06, @Mes_07 = AC_MES_07, @Mes_08 = AC_MES_08,
         @Mes_09 = AC_MES_09, @Mes_10 = AC_MES_10, @Mes_11 = AC_MES_11, @Mes_12 = AC_MES_12, @Mes_13 = AC_MES_13
  from ACUMULA
  where ( CB_CODIGO = @Empleado ) AND 
        ( AC_YEAR = @Anio  ) AND 
        ( CO_NUMERO = @Concepto )

  select @Resultado = 0
  if ( @MesIni <= 1  and  @MesFin >= 1 ) select @Resultado=@Resultado+@Mes_01
  if ( @MesIni <= 2  and  @MesFin >= 2 ) select @Resultado=@Resultado+@Mes_02
  if ( @MesIni <= 3  and  @MesFin >= 3 ) select @Resultado=@Resultado+@Mes_03
  if ( @MesIni <= 4  and  @MesFin >= 4 ) select @Resultado=@Resultado+@Mes_04
  if ( @MesIni <= 5  and  @MesFin >= 5 ) select @Resultado=@Resultado+@Mes_05
  if ( @MesIni <= 6  and  @MesFin >= 6 ) select @Resultado=@Resultado+@Mes_06
  if ( @MesIni <= 7  and  @MesFin >= 7 ) select @Resultado=@Resultado+@Mes_07
  if ( @MesIni <= 8  and  @MesFin >= 8 ) select @Resultado=@Resultado+@Mes_08
  if ( @MesIni <= 9  and  @MesFin >= 9 ) select @Resultado=@Resultado+@Mes_09
  if ( @MesIni <= 10 and  @MesFin >= 10 ) select @Resultado=@Resultado+@Mes_10
  if ( @MesIni <= 11 and  @MesFin >= 11 ) select @Resultado=@Resultado+@Mes_11
  if ( @MesIni <= 12 and  @MesFin >= 12 ) select @Resultado=@Resultado+@Mes_12
  if ( @MesIni <= 13 and  @MesFin >= 13 ) select @Resultado=@Resultado+@Mes_13
 
  if @Resultado IS NULL select @Resultado = 0
  RETURN @Resultado
END
GO

/* SP_C: Cambiado para quitar los par�metros YEAR */
/* Patch 390 # Seq: 164 Modificado */
ALTER FUNCTION SP_C(
  @Concepto SMALLINT,
  @Numero SMALLINT,
  @Tipo SMALLINT,
  @Anio SMALLINT,
  @Empleado INTEGER )
RETURNS NUMERIC(15,2)
AS
BEGIN
	DECLARE @Resultado AS NUMERIC(15,2)
	select @Resultado = SUM(MO_PERCEPC+MO_DEDUCCI)
	from   MOVIMIEN
	where ( CB_CODIGO = @Empleado ) AND
  		( PE_YEAR = @Anio ) AND
		( PE_TIPO = @Tipo ) AND
		( PE_NUMERO = @Numero ) AND
		( CO_NUMERO = @Concepto ) AND
		( MO_ACTIVO = 'S' )
      	if ( @Resultado is NULL )
       		select @Resultado = 0
	RETURN @Resultado
END
GO

/* SP_CM: Cambiado para quitar los par�metros YEAR */
/* Patch 390 # Seq: 165 Modificado */
ALTER FUNCTION SP_CM
				( 	@CONCEPTO SMALLINT,
    				@REFERENCIA VARCHAR(8),
    				@NUMERO SMALLINT,
    				@TIPO SMALLINT,
    				@ANIO SMALLINT,
    				@EMPLEADO INTEGER)

RETURNS NUMERIC(15,2)
AS
begin
	Declare @Resultado NUMERIC(15,2)
    if ( @Referencia = '' ) 
    begin
    	Select @Resultado = dbo.SP_C( @Concepto, @Numero, @Tipo, @Anio, @Empleado )
    end
    else
    begin
 		select @Resultado = SUM(MO_PERCEPC+MO_DEDUCCI)
 		from   MOVIMIEN
        where ( CB_CODIGO = @Empleado )
		AND ( PE_YEAR = @Anio )
		AND ( PE_TIPO = @Tipo )
		AND ( PE_NUMERO = @Numero )
		AND ( CO_NUMERO = @Concepto )
		AND ( MO_REFEREN = @Referencia )
		AND ( MO_ACTIVO = 'S' )
    end
	RETURN @Resultado
end
GO

/* SP_CS: Cambiado para quitar los par�metros YEAR */
/* Patch 390 # Seq: 169 Modificado */
ALTER FUNCTION SP_CS(
		@CONCEPTO SMALLINT,
    		@NUMEROINI SMALLINT,
    		@NUMEROFIN SMALLINT,
    		@TIPO SMALLINT,
    		@ANIO SMALLINT,
    		@EMPLEADO INTEGER)
RETURNS NUMERIC(15,2)
AS
begin
	declare @Temporal NUMERIC(15,2);
	declare @Resultado NUMERIC(15,2);

  	select @Resultado = 0;
  	while ( @NumeroIni <= @NumeroFin )
  	begin
	    Select @Temporal = dbo.SP_C ( @Concepto, @NumeroIni, @Tipo, @Anio, @Empleado )
    	if ( @Temporal is NOT NULL )
      		select @Resultado = @Resultado + @Temporal;
    	Select @NumeroIni = @NumeroIni + 1;
  	end
	Return @Resultado;
end
GO

/* SP_CAFE_SEMANAL: Cambiado para quitar los par�metros YEAR */
/* Patch 390 # Seq: 85 Modificado */
ALTER function DBO.SP_CAFE_SEMANAL(
@EMPLEADO INTEGER,
@FECHA DATETIME,
@ANIO SMALLINT,
@TIPOCOMIDA SMALLINT,
@TIPORESULTADO SMALLINT,
@TIPOPERIODO SMALLINT )
RETURNS Integer
as
begin
     declare @FechaInicial DateTime;
     declare @FechaFinal DateTime;
     declare @Resultado Integer;
     if ( @Empleado is Null )
        set @Resultado = 0
     else
     begin
          select @FechaInicial = P.PE_FEC_INI, @FechaFinal = P.PE_FEC_FIN from DBO.PERIODO as P
          where ( P.PE_YEAR = @Anio ) and
                ( P.PE_TIPO = @TipoPeriodo ) and
                ( P.PE_FEC_INI <= @Fecha ) and
                ( P.PE_FEC_FIN >= @Fecha );
          if ( @FechaInicial is Null )
          begin
               set @FechaInicial = @Fecha;
               set @FechaFinal = @Fecha;
          end;
          select @Resultado = DBO.SP_CAFE_SUMA_COMIDAS( @Empleado, @FechaInicial, @FechaFinal, @TipoComida, @TipoResultado );
     end
     Return( @Resultado );
end
GO

/* SP_CAFE_AL_DIA: Cambiado para quitar los par�metros YEAR */
/* Patch 390 # Seq: 86 Modificado */
ALTER function DBO.SP_CAFE_AL_DIA(
@EMPLEADO INTEGER,
@FECHA DATETIME,
@ANIO SMALLINT,
@TIPOCOMIDA SMALLINT,
@TIPORESULTADO SMALLINT,
@TIPOPERIODO SMALLINT )
RETURNS Integer
as
begin
     declare @FechaInicial DateTime;
     declare @Resultado Integer;
     if ( @Empleado is Null )
        set @Resultado = 0
     else
     begin
          select @FechaInicial = P.PE_FEC_INI from DBO.PERIODO as P
          where ( P.PE_YEAR = @Anio ) and
                ( P.PE_TIPO = @TipoPeriodo ) and
                ( P.PE_FEC_INI <= @Fecha ) and
                ( P.PE_FEC_FIN >= @Fecha );
          if ( @FechaInicial is Null )
             set @FechaInicial = @Fecha;
          select @Resultado = DBO.SP_CAFE_SUMA_COMIDAS( @Empleado, @FechaInicial, @Fecha, @TipoComida, @TipoResultado );
     end
     Return( @Resultado );
end
GO

/* SP_INVESTIGA_STATUS: Averigua El Status Del Per�odo Al Que Pertenece Un Empleado En Una Fecha ( Defecto 181 ) */
/* Patch 390 # Seq: 225 Agregado */
CREATE FUNCTION SP_INVESTIGA_STATUS (
  @Empleado INTEGER,
  @Fecha DATETIME
)
RETURNS SMALLINT
AS
BEGIN
     declare @Status SMALLINT
     declare @Turno CHAR(6)
     declare @FechaTurno DATETIME
     declare @TipoNomina SmallInt

     select @Turno = CB_TURNO, @FechaTurno = CB_FEC_TUR
     from COLABORA
     where CB_CODIGO = @Empleado

     if (@FechaTurno > @Fecha)
        select @Turno = dbo.SP_KARDEX_CB_TURNO( @Fecha, @Empleado )

     select @TipoNomina=TU_NOMINA
     from TURNO
     where TU_CODIGO = @Turno

     select @Status = MAX(PE_STATUS)
     from PERIODO
     where PE_NUMERO < 200
     and PE_FEC_INI <= @Fecha
     and PE_FEC_FIN >= @Fecha
     and PE_TIPO = @TipoNomina

     if @Status is NULL Select @Status = 0;
     Return @Status
end
GO

/* SP_STATUS_TARJETA: Averigua El Status Del Per�odo Al Que Pertenece Una Tarjeta De Asistencia ( Defecto 181 ) */
/* Patch 390 # Seq: 226 Agregado */
CREATE FUNCTION SP_STATUS_TARJETA (
  @Empleado INTEGER,
  @Fecha DATETIME
)
RETURNS SMALLINT
AS
BEGIN
     declare @Status SMALLINT

     select @Status = PE_STATUS
     from AUSENCIA
     left outer join PERIODO on  PERIODO.PE_YEAR = AUSENCIA.PE_YEAR
                             and PERIODO.PE_TIPO = AUSENCIA.PE_TIPO
                             and PERIODO.PE_NUMERO = AUSENCIA.PE_NUMERO
     where AUSENCIA.CB_CODIGO = @Empleado
     and   AUSENCIA.AU_FECHA = @Fecha

     if (@Status is NULL)
     begin
          Select @Status = dbo.SP_INVESTIGA_STATUS( @Empleado, @Fecha )
     end;

     Return @Status
END
GO

/* TURNO_EMPLEADO: Ampliar el ancho del Ritmo ( Sugerencia 683 ) */
/* Patch 390 # Seq: 95 Modificado */
ALTER FUNCTION TURNO_EMPLEADO(
  @Fecha Datetime,
  @Empleado Integer )
RETURNS @TablaTurno TABLE
(      TU_RIT_PAT           Varchar(255),
       TU_HOR_1             Char(6),
       TU_HOR_2             Char(6),
       TU_HOR_3             Char(6),
       TU_HOR_4             Char(6),
       TU_HOR_5             Char(6),
       TU_HOR_6             Char(6),
       TU_HOR_7             Char(6),
       TU_TIP_1             Smallint,
       TU_TIP_2             Smallint,
       TU_TIP_3             Smallint,
       TU_TIP_4             Smallint,
       TU_TIP_5             Smallint,
       TU_TIP_6             Smallint,
       TU_TIP_7             Smallint,
       TU_CODIGO            Char(6),
       TU_RIT_INI           Datetime,
       TU_HORARIO           Smallint,
       TU_HOR_FES	    Char(6),
       TU_VACA_HA           Numeric(15,2),
       TU_VACA_SA           Numeric(15,2),
       TU_VACA_DE           Numeric(15,2),
       CB_CREDENC           Char(1) )
AS
BEGIN
     INSERT @TablaTurno
     SELECT TU_RIT_PAT,
            TU_HOR_1,
            TU_HOR_2,
            TU_HOR_3,
            TU_HOR_4,
            TU_HOR_5,
            TU_HOR_6,
            TU_HOR_7,
            TU_TIP_1,
            TU_TIP_2,
            TU_TIP_3,
            TU_TIP_4,
            TU_TIP_5,
            TU_TIP_6,
            TU_TIP_7,
            TU_CODIGO,
            TU_RIT_INI,
            TU_HORARIO,
            TU_HOR_FES,
            TU_VACA_HA,
            TU_VACA_SA,
            TU_VACA_DE, 
            ( select C.CB_CREDENC from COLABORA C where ( C.CB_CODIGO = @Empleado ) ) CB_CREDENC
     FROM TURNO WHERE ( TU_CODIGO = dbo.SP_KARDEX_CB_TURNO( @Fecha, @Empleado ) );
     RETURN
END
GO

/* SP_DIAS_JORNADA: Ampliar el ancho del Ritmo ( Sugerencia 683 ) */
/* Patch 390 # Seq: 21 Modificado */
ALTER PROCEDURE SP_DIAS_JORNADA(
		@TURNO CHAR(6),
    		@FECHAINI DATETIME,
    		@FECHAFIN DATETIME,
    		@TU_DIAS SMALLINT OUTPUT,
    		@TU_JORNADA NUMERIC(15,2) OUTPUT,
    		@ES_ROTA CHAR(1)OUTPUT)
AS
BEGIN
     set NOCOUNT ON
     select @TU_DIAS = TU_DIAS,
            @TU_JORNADA = TU_JORNADA,
            @ES_ROTA = ( case when ( ( TU_RIT_PAT is NULL ) or ( TU_RIT_PAT = '' ) ) then 'N' else 'S' end )
     from TURNO
     where ( TU_CODIGO = @Turno );
     if ( @TU_DIAS is NULL )
     begin
          select @TU_DIAS = 0, @TU_JORNADA = 0, @ES_ROTA = 'N'
     end
END
GO

update GLOBAL set GL_FORMULA = '390' where ( GL_CODIGO = 133 )
GO
