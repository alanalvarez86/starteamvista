/****************************************/
/****************************************/
/****** Patch para DATOS ****************/
/****************************************/
/****************************************/

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_TIMBRAR_EMPLEADO') AND type IN ( N'P', N'PC' ))
	drop procedure SP_TIMBRAR_EMPLEADO
GO

CREATE procedure SP_TIMBRAR_EMPLEADO( @PE_YEAR Anio, @PE_TIPO NominaTipo, @PE_NUMERO NominaNumero, @CB_CODIGO NumeroEmpleado, @CB_RFC Descripcion, @Status status, @Timbre Formula, @FolioTimbramex FolioGrande, @Usuario usuario, @FacturaUUID  DescLarga = '' )
as 
begin 
	declare @StatusTimbrado status 
	declare @StatusTimbradoPendiente status    
	declare @StatusTimbradoParcial status    
	declare @StatusTimbradoTotal status    
	declare @StatusTimbradoPendienteNuevo status
	declare @StatusCancelacionPendienteNuevo status
	declare @StatusTimbradoPendienteLimpiar status
	declare @StatusCancelacionPendienteLimpiar status
   
	set @StatusTimbradoPendiente = 0
	set @StatusTimbradoParcial = 1   
	set @StatusTimbradoTotal = 2   
	set @StatusTimbradoPendienteNuevo = 3
	set @StatusCancelacionPendienteNuevo = 4
	set @StatusTimbradoPendienteLimpiar = 5
	set @StatusCancelacionPendienteLimpiar = 6

	if ( @Status = @StatusTimbradoPendienteLimpiar )
	begin 
		   update NOMINA set NO_TIMBRO = @StatusTimbradoPendiente
		   where  CB_CODIGO = @CB_CODIGO and PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO = @PE_NUMERO  
		   and NO_STATUS = 6 and NO_TIMBRO = @StatusTimbradoPendienteNuevo   
	end	
	else
	if ( @Status = @StatusCancelacionPendienteLimpiar )
	begin 
		   update NOMINA set NO_TIMBRO = @StatusTimbradoTotal
		   where  CB_CODIGO = @CB_CODIGO and PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO = @PE_NUMERO  
		   and NO_STATUS = 6 and NO_TIMBRO = @StatusCancelacionPendienteNuevo   
	end
	else
	if ( @Status = @StatusTimbradoPendiente )
	begin 
		   update NOMINA set NO_TIMBRO = @Status, NO_SELLO = '',  NO_FACTURA = 0, NO_FACUUID = '', NO_CAN_UID = NO_FACUUID 
		   where  CB_CODIGO = @CB_CODIGO and PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO = @PE_NUMERO  
		   and NO_STATUS = 6 
	end
	else
	if ( @Status = @StatusTimbradoTotal )
	begin 
		   declare @verTotal FolioGrande     
		   set @verTotal = dbo.KardexConceptos_GetVersionActual() 

		   update NOMINA set NO_TIMBRO = @Status, NO_SELLO = @Timbre , NO_FACTURA = @FolioTimbramex  , NO_FACUUID = @FacturaUUID, NO_VER_TIM  = @verTotal 
		   where  CB_CODIGO = @CB_CODIGO and PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO = @PE_NUMERO 
		   and NO_STATUS = 6  
	end
	else
	if ( @Status = @StatusTimbradoPendienteNuevo ) or( @Status = @StatusCancelacionPendienteNuevo )
	begin 
		   update NOMINA set NO_TIMBRO = @Status
		   where  CB_CODIGO = @CB_CODIGO and PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO = @PE_NUMERO  
		   and NO_STATUS = 6 
	end
end
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'[AFECTAR_STATUS_TIMBRADO]') AND type IN ( N'P', N'PC' ))
    DROP PROCEDURE AFECTAR_STATUS_TIMBRADO
go
 
CREATE procedure AFECTAR_STATUS_TIMBRADO( @PE_YEAR Anio, @PE_TIPO NominaTipo, @PE_NUMERO NominaNumero, @StatusActual status, @StatusNuevo status )   
as  
begin  
	exec TimbradoConcilia_CalcularStatusPeriodo @PE_YEAR, @PE_TIPO, @PE_NUMERO, @StatusActual, @StatusNuevo	        
end

GO

if exists (select * from sysobjects where id = object_id(N'Periodo_GetStatusTimbrado') and xtype in (N'FN', N'IF', N'TF')) 
	drop function Periodo_GetStatusTimbrado
go

CREATE FUNCTION Periodo_GetStatusTimbrado(@PE_YEAR int,  @PE_TIPO int, @PE_NUMERO int,  @CM_NIVEL0 Formula = '' ) 
RETURNS TABLE 
AS RETURN
( 
select PE_YEAR, PE_TIPO, PE_NUMERO, PE_STATUS, PE_TIMBRO,  coalesce( [0], 0)  as Pendientes,  coalesce([2], 0) as Timbradas,  coalesce( [3], 0)  as TimbradoPendientes,  coalesce([4], 0) as CancelacionPendiente,  coalesce([0], 0)+coalesce([3], 0) as  PendientesTotal,  coalesce([2], 0)+coalesce([4], 0) as TimbradasTotal from 
  (
  select 
    PE.PE_YEAR,
    PE.PE_TIPO,
    PE.PE_STATUS, 
    PE.PE_TIMBRO,
    PE.PE_NUMERO,
    NO_TIMBRO, 
    COUNT(*) Empleados
  from PERIODO PE 
  left outer join NOMINA NO on PE.PE_YEAR = NO.PE_YEAR and PE.PE_TIPO = NO.PE_TIPO and PE.PE_NUMERO = NO.PE_NUMERO   
  where 
  PE.PE_YEAR = @PE_YEAR and PE.PE_TIPO = @PE_TIPO and PE.PE_NUMERO = @PE_NUMERO 
  and (  @CM_NIVEL0 = ''  or ( CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB_NIVEL0='') )
  group by PE.PE_YEAR, PE.PE_TIPO, PE.PE_NUMERO,PE.PE_STATUS, PE.PE_TIMBRO, NO_TIMBRO 
  ) Timbs 
pivot 
(
    SUM(Empleados) 
    for NO_TIMBRO in ( [0], [1], [2], [3], [4] ) 
)  pvt 

)

GO

if exists (select * from sysobjects where id = object_id(N'Periodo_GetStatusTimbrado_RazonSocial') and xtype in (N'FN', N'IF', N'TF')) 
	drop function Periodo_GetStatusTimbrado_RazonSocial
go

CREATE FUNCTION Periodo_GetStatusTimbrado_RazonSocial(@PE_YEAR int,  @PE_TIPO int, @PE_NUMERO int,  @CM_NIVEL0 Formula = '',  @RAZON_SOCIAL Codigo ) 
RETURNS TABLE 
AS RETURN
( 
select PE_YEAR, PE_TIPO, PE_NUMERO, PE_STATUS, PE_TIMBRO,  coalesce( [0], 0)  as Pendientes,  coalesce([2], 0) as Timbradas,  coalesce( [3], 0)  as TimbradoPendientes,  coalesce([4], 0) as CancelacionPendiente,  coalesce([0], 0)+coalesce([3], 0) as  PendientesTotal,  coalesce([2], 0)+coalesce([4], 0) as TimbradasTotal from 
  (
  select 
    PE.PE_YEAR,
    PE.PE_TIPO,
    PE.PE_STATUS, 
    PE.PE_TIMBRO,
    PE.PE_NUMERO,
    NO_TIMBRO, 
    COUNT(*) Empleados
  from PERIODO PE 
  left outer join NOMINA NO on PE.PE_YEAR = NO.PE_YEAR and PE.PE_TIPO = NO.PE_TIPO and PE.PE_NUMERO = NO.PE_NUMERO   
  left outer join RPATRON on RPATRON.TB_CODIGO = NO.CB_PATRON
  left outer join RSOCIAL on RSOCIAL.RS_CODIGO = RPATRON.RS_CODIGO
  where 
  PE.PE_YEAR = @PE_YEAR and PE.PE_TIPO = @PE_TIPO and PE.PE_NUMERO = @PE_NUMERO 
  and (  @CM_NIVEL0 = ''  or ( CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB_NIVEL0='') )
  and ( RSOCIAL.RS_CODIGO = @RAZON_SOCIAL )
  group by PE.PE_YEAR, PE.PE_TIPO, PE.PE_NUMERO,PE.PE_STATUS, PE.PE_TIMBRO, NO_TIMBRO 
  ) Timbs 
pivot 
(
    SUM(Empleados) 
    for NO_TIMBRO in ( [0], [1], [2], [3], [4] ) 
)  pvt 

)

GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'TimbradoConcilia_CalcularStatusPeriodo') AND type IN ( N'P', N'PC' ))
    drop procedure TimbradoConcilia_CalcularStatusPeriodo
go

CREATE procedure TimbradoConcilia_CalcularStatusPeriodo( @PE_YEAR Anio, @PE_TIPO NominaTipo, @PE_NUMERO NominaNumero, @StatusActual status, @StatusNuevo status)   
as  
begin  
	set nocount on   
	declare @Total int   
	declare @TotalAfectados int   
	declare @TotalTimbradoPendiente int   
	declare @TotalCancelacionPendiente int   

	declare @Timbrados int   
	declare @StatusTimbradoPendiente status    
	declare @StatusTimbradoParcial status    
	declare @StatusTimbradoTotal status    
	declare @NominaAfectada status   
	declare @NominaAfectadaParcial status      
	declare @StatusTimbradoPendienteNuevo status
	declare @StatusCancelacionPendienteNuevo status
   
	set @StatusTimbradoPendiente = 0
	set @StatusTimbradoParcial = 1   
	set @StatusTimbradoTotal = 2   
	set @NominaAfectada = 6   
	set @NominaAfectadaParcial = 5
	set @StatusTimbradoPendienteNuevo = 3
	set @StatusCancelacionPendienteNuevo = 4
   
 	select @Total                     = count(*) from NOMINA where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  
	select @TotalAfectados            = count(*) from NOMINA where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and NO_STATUS = @NominaAfectada   
	select @TotalTimbradoPendiente    = count(*) from NOMINA where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and NO_TIMBRO = @StatusTimbradoPendienteNuevo   
	select @TotalCancelacionPendiente = count(*) from NOMINA where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and NO_TIMBRO = @StatusCancelacionPendienteNuevo   
   
	 if @Total > 0   
	 begin     
		  select @Timbrados = count(*) from NOMINA where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and  NO_STATUS = @NominaAfectada  and NO_TIMBRO = @StatusTimbradoTotal  

		  if ( @TotalAfectados > 0 ) 
		  begin 
			  if ( @TotalTimbradoPendiente > 0 )              
				update PERIODO set PE_TIMBRO = @StatusTimbradoPendienteNuevo where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and PE_STATUS >= @NominaAfectadaParcial                      
			  else 
				if ( @TotalCancelacionPendiente > 0 )          
					update PERIODO set PE_TIMBRO = @StatusCancelacionPendienteNuevo where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and PE_STATUS >= @NominaAfectadaParcial          
				else                  
					if ( @Total = @Timbrados )  
						update PERIODO set PE_TIMBRO = @StatusTimbradoTotal  where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and  PE_STATUS >= @NominaAfectadaParcial                            
					else  
					   if ( @Timbrados > 0 ) 
							update PERIODO set PE_TIMBRO = @StatusTimbradoParcial where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and  PE_STATUS >= @NominaAfectadaParcial      
					   else 
							update PERIODO set PE_TIMBRO = @StatusTimbradoPendiente where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and PE_STATUS >= @NominaAfectadaParcial                                                                    
		  end
		  else
		  begin
			update PERIODO set PE_TIMBRO = @StatusTimbradoPendiente where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and PE_STATUS >= @NominaAfectadaParcial   
		  end; 
	end;    
end
GO

IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_COMPLEMENTO_SAT_XML') AND type IN ( N'P', N'PC' ))
	DROP PROCEDURE SP_COMPLEMENTO_SAT_XML 
GO

create procedure SP_COMPLEMENTO_SAT_XML
as 
begin 
	set nocount on

	IF EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO = 'P016' ) BEGIN
		update TIPO_SAT set TB_ACTIVO = 'N' where TB_CODIGO = 'P016' AND TB_SAT_CLA = 1 
	END

	IF EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO = 'P017' ) BEGIN
		update TIPO_SAT set TB_ACTIVO = 'N' where TB_CODIGO = 'P017' AND TB_SAT_CLA = 1 
	END

	IF EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO = 'P003' ) BEGIN
		update TIPO_SAT set TB_ELEMENT = 'Participaci�n de los Trabajadores en las Utilidades PTU' where TB_CODIGO = 'P003' AND TB_SAT_CLA = 1 
	END

	IF EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO = 'P004' ) BEGIN
		update TIPO_SAT set TB_ELEMENT = 'Reembolso de Gastos M�dicos Dentales y Hospitalarios' where TB_CODIGO = 'P004' AND TB_SAT_CLA = 1 
	END
	
	IF EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO = 'P009' ) BEGIN
		update TIPO_SAT set TB_ELEMENT = 'Contribuciones a Cargo del Trabajador Pagadas por el Patr�n' where TB_CODIGO = 'P009' AND TB_SAT_CLA = 1 
	END

	IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO = 'D101' ) BEGIN
		INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO ) VALUES ( 'D101', 'ISR Retenido de ejercicio anterior', 101, 2, 'S');
	END

	IF EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO = 'D080' ) BEGIN
		update TIPO_SAT set TB_ELEMENT = 'Ajuste en Vi�ticos gravados' where TB_CODIGO = 'D080' AND TB_SAT_CLA = 2 
	END

	IF EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO = 'D081' ) BEGIN
		update TIPO_SAT set TB_ELEMENT = 'Ajuste en Vi�ticos' where TB_CODIGO = 'D081' AND TB_SAT_CLA = 2 
	END

	IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO = 'OP05' ) BEGIN
		INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO ) VALUES ( 'OP05', 'Reintegro de ISR retenido en exceso de ejercicio anterior (siempre que no haya sido enterado al SAT)', 5, 4, 'S');
	END

	IF EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO = 'OP01' ) BEGIN
		update TIPO_SAT set TB_ELEMENT = 'Reintegro de ISR pagado en exceso (siempre que no haya sido enterado al SAT)' where TB_CODIGO = 'OP01' AND TB_SAT_CLA = 4
	END

	IF EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO = 'OP02' ) BEGIN
		update TIPO_SAT set TB_ELEMENT = 'Subsidio para el empleo (efectivamente entregado al trabajador)' where TB_CODIGO = 'OP02' AND TB_SAT_CLA = 4
	END

	IF EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO = 'OP03' ) BEGIN
		update TIPO_SAT set TB_ELEMENT = 'Vi�ticos (entregados al trabajador)' where TB_CODIGO = 'OP03' AND TB_SAT_CLA = 4
	END

	IF EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO = 'OP999' ) BEGIN
		update TIPO_SAT set TB_ELEMENT = 'Pagos distintos a los listados y que no deben considerarse como ingreso por sueldos, salarios o ingresos asimilados' where TB_CODIGO = 'OP999' AND TB_SAT_CLA = 4
	END
END
GO

IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_COMPLEMENTO_SAT_XML') AND type IN ( N'P', N'PC' ))
	EXECUTE dbo.SP_COMPLEMENTO_SAT_XML;
GO

IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_COMPLEMENTO_SAT_XML') AND type IN ( N'P', N'PC' ))
	DROP PROCEDURE SP_COMPLEMENTO_SAT_XML 
GO

IF exists( select * from sys.objects where object_id = object_id('VCED_CONSULTA') )
    drop view VCED_CONSULTA
GO

CREATE view VCED_CONSULTA
as
	select 
	PF_YEAR, PF_MES, RS_CODIGO, CB_CODIGO, PR_REFEREN,  PR_TIPO, PF_PAGO, PF_PLAZO, PF_PAGADAS, FC_NOMINA, FC_AJUSTE, 
	PF_PAGO - (FC_NOMINA + FC_AJUSTE )  as DIFERENCIA, 
	FE_INCIDE, 
	PR_STATUS,
	PR_FON_MOT,
	FECHA_INI_BAJA as FE_FECHA1, 
	FECHA_FIN as FE_FECHA2, 
	PF_REUBICA, 
	FE_DIF_QTY, 
	FE_CUANTOS,
	PF_CTD
	from ( 

	SELECT F.PF_YEAR, F.PF_MES, F.RS_CODIGO, C.CB_CODIGO, F.PR_REFEREN,  F.PR_TIPO, F.PF_PAGO, F.PF_PLAZO, F.PF_PAGADAS,
		coalesce(FC.FC_NOMINA,0.00) FC_NOMINA,  coalesce(FC.FC_AJUSTE,0.00) FC_AJUSTE ,				
					case
					 when (FE.FE_INCIDE is null or FE.FE_INCIDE = '0' ) and  C.CB_ACTIVO = 'N' and C.CB_FEC_BAJ < dbo.FONACOT_GET_FECHA_INICIO(F.PF_YEAR, F.PF_MES, C.CB_NOMINA )	then 'B' 	
					 else coalesce(FE_INCIDE, '0') 
					end 					 
					AS FE_INCIDE,
					case
					 when (FE.FE_INCIDE is null or FE.FE_INCIDE = '0' ) and C.CB_ACTIVO = 'N' and C.CB_FEC_BAJ < dbo.FONACOT_GET_FECHA_INICIO(F.PF_YEAR, F.PF_MES, C.CB_NOMINA )	then CB_FEC_BAJ	
					 when  FE.FE_INCIDE = 'B' then CB_FEC_BAJ
					 else coalesce(FE.FE_FECHA1, '1899-12-30') 
					end 					 
					AS FECHA_INI_BAJA, 
					coalesce(FE.FE_FECHA2, '1899-12-30') AS FECHA_FIN, 
					F.PF_REUBICA, 
					FC.FC_PR_STA AS PR_STATUS,
					FC.FC_FON_MOT AS PR_FON_MOT, 
					FE.FE_DIF_QTY, 
					(SELECT COUNT(PCF1.CB_CODIGO) FROM PCED_FON PCF1
					WHERE F.CB_CODIGO = PCF1.CB_CODIGO AND F.PF_YEAR = PCF1.PF_YEAR
					AND F.PF_MES = PCF1.PF_MES AND F.PR_TIPO = PCF1.PR_TIPO) FE_CUANTOS,
					PF_CTD				
		FROM PCED_FON F
			LEFT JOIN COLABORA C ON F.CB_CODIGO = C.CB_CODIGO
			LEFT JOIN FON_EMP FE ON FE.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = FE.FT_YEAR AND F.PF_MES = FE.FT_MONTH
			LEFT JOIN FON_CRE FC ON FC.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = FC.FT_YEAR AND F.PF_MES = FC.FT_MONTH AND F.PR_REFEREN = FC.PR_REFEREN  AND F.PR_TIPO = FC.PR_TIPO 
			LEFT JOIN VCEDTFON CT  ON CT.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = CT.PF_YEAR AND F.PF_MES = CT.PF_MES				
	
		) Cedula

GO

IF exists( select * from sys.objects where object_id = object_id('VCED_DET') )
    drop view VCED_DET
GO

CREATE view VCED_DET
as
select
PF_YEAR, PF_MES, RS_CODIGO, CB_FONACOT, CB_RFC, CB_SEGSOC, CB_CODIGO, PRETTYNAME, PR_REFEREN, PR_TIPO, PR_STATUS,  PF_PAGO, PF_PLAZO, PF_PAGADAS, FC_NOMINA, FC_AJUSTE,
PF_PAGO - (FC_NOMINA + FC_AJUSTE )  as DIFERENCIA,
FE_INCIDE,
case FE_INCIDE
	when '0' then ''
	when 'B' then 'Baja'
	when 'I' then 'Incapacidad'
	when 'C' then 'Pr�stamo cancelado'
	when 'M' then 'Pago de m�s'
	when 'L' then 'Pago de menos'
	else  'Otro'
end FE_IN_DESC,
CASE PR_STATUS
	when 0 then ''
	else PR_FON_MOT
END PR_FON_MOT,
FECHA_INI_BAJA as FE_FECHA1, FECHA_FIN as FE_FECHA2, PF_REUBICA, FE_DIF_QTY, FE_CUANTOS, PF_CTD
from ( 
SELECT VC.PF_YEAR, VC.PF_MES,  coalesce(VC.RS_CODIGO,'') RS_CODIGO,  C.CB_FONACOT, C.CB_RFC, C.CB_SEGSOC, coalesce(UPPER (C.CB_APE_PAT + ' ' + C.CB_APE_MAT +' ' + C.CB_NOMBRES), '')  AS PRETTYNAME, VC.PR_REFEREN, VC.PR_TIPO, VC.PR_STATUS,  VC.PF_PAGO, coalesce(C.CB_CODIGO,0) CB_CODIGO, VC.PF_PLAZO, VC.PF_PAGADAS,
	coalesce(VC.FC_NOMINA,0.00) FC_NOMINA,  coalesce(VC.FC_AJUSTE,0.00) FC_AJUSTE ,
				
				case
					when coalesce( FE_INCIDE, '0')  = '0'  and PR_STATUS = 1  then 'C'
				    when coalesce( FE_INCIDE, '0')  = '0' AND ((FC_NOMINA+FC_AJUSTE)>PF_PAGO) then 'M'
				    when coalesce( FE_INCIDE, '0')  = '0' AND ((FC_NOMINA+FC_AJUSTE)<PF_PAGO) then 'L'
					else coalesce( FE_INCIDE, '0' )
				end AS FE_INCIDE,
				case
					when CONVERT (VARCHAR(10), VC.FE_FECHA1, 103) = '30/12/1899'  then ''
					else CONVERT (VARCHAR(10), VC.FE_FECHA1, 103)
				end AS FECHA_INI_BAJA,
				case
					when CONVERT (VARCHAR(10), VC.FE_FECHA2, 103) = '30/12/1899' then ''
					else CONVERT (VARCHAR(10), VC.FE_FECHA2, 103)
				end AS FECHA_FIN, 
			    VC.PF_REUBICA,				 
				case PR_FON_MOT 
					when 0 then 'No aplica' 
					when 1 then 'Pago directo en Fonacot' 
					when 2 then 'Confirmaci�n de Fonacot' 
					when 3 then 'Ausente en C�dula'
					when 4 then 'CanceladoOtro'
					else  '' 
				end PR_FON_MOT, VC.FE_DIF_QTY, VC.FE_CUANTOS, PF_CTD
	FROM VCED_CONSULTA VC
		LEFT JOIN COLABORA C ON VC.CB_CODIGO = C.CB_CODIGO
	) Cedula 

GO 

if exists (select * from dbo.sysobjects where id = object_id(N'FN_FONACOT_MONTO_PRESTAMO') and xtype in (N'FN', N'IF', N'TF')) 
       drop function FN_FONACOT_MONTO_PRESTAMO
go 

CREATE FUNCTION FN_FONACOT_MONTO_PRESTAMO (@CB_CODIGO NumeroEmpleado, @PR_REFEREN Referencia, @PR_TIPO Codigo1, @YEAR SMALLINT, @MES SMALLINT) returns Pesos 
as
begin
	 DECLARE @Return Pesos

	IF (SELECT GL_FORMULA FROM GLOBAL WHERE GL_CODIGO = 328 ) = 'N'
	BEGIN
		SELECT @Return = PR_PAG_PER FROM PRESTAMO WHERE CB_CODIGO = @CB_CODIGO AND PR_TIPO = @PR_TIPO AND PR_REFEREN = @PR_REFEREN
	END
	ELSE
	BEGIN
		SELECT @Return = COALESCE (PF_PAGO, 0) FROM PCED_FON WHERE CB_CODIGO = @CB_CODIGO AND PR_TIPO = @PR_TIPO AND PR_REFEREN = @PR_REFEREN
			AND PF_YEAR = @YEAR AND PF_MES = @MES

		IF @Return IS NULL
			SET @Return = 0
	END

	Return @Return

end
go

IF exists( select * from sys.objects where object_id = object_id('V_TERMINAL') )
    drop view V_TERMINAL
GO

create view V_TERMINAL
as
	select TE_CODIGO, TE_NOMBRE, TE_NUMERO, TE_TEXTO, TE_MAC, TE_APP, TE_BEAT,TE_ZONA, TE_ACTIVO, TE_XML,TE_LOGO,TE_FONDO,TE_ZONA_ES, TE_VERSION, TE_IP,TE_HUELLAS, TE_ID from #COMPARTE.dbo.TERMINAL
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_FONACOT_CANCELAR_PRESTAMOS') AND type IN ( N'P', N'PC' ))
    DROP PROCEDURE SP_FONACOT_CANCELAR_PRESTAMOS
GO

CREATE procedure SP_FONACOT_CANCELAR_PRESTAMOS (@YEAR SMALLINT, @MES SMALLINT, @RAZON_SOCIAL CHAR(6), @US_CODIGO SMALLINT, @PROC_ID INTEGER, @BI_NUMERO INTEGER) 
as
begin
    set nocount on 
    DECLARE @PR_TIPO Codigo1;
    DECLARE @Prestamos TABLE
    (
        i int identity(1,1),
        iCB_CODIGO int, 
        sPR_REFEREN varchar(8) 
    )
    SELECT @PR_TIPO = GL_FORMULA FROM GLOBAL WHERE GL_CODIGO = 211
    insert into @Prestamos ( iCB_CODIGO , sPR_REFEREN )
		 select CB.CB_CODIGO, PR.PR_REFEREN  from PRESTAMO PR
			join COLABORA CB on PR.CB_CODIGO = CB.CB_CODIGO 
			join PCED_FON PF on PR.PR_REFEREN = PF.PR_REFEREN AND PR.PR_TIPO = PF.PR_TIPO
			 where PR.PR_TIPO = @PR_TIPO and PR_STATUS in (0)
			 AND (PF.RS_CODIGO = @RAZON_SOCIAL OR @RAZON_SOCIAL = '')
			 AND CB.CB_CODIGO = PF.CB_CODIGO

    declare @i int = 1 
    declare @n int 
    declare @iCB_CODIGO NumeroEmpleado 
    declare @sPR_REFEREN Referencia 
    select @n = count(*) from @Prestamos 
    while @i <= @n 
    begin          select @iCB_CODIGO = iCB_CODIGO , @sPR_REFEREN = sPR_REFEREN from @Prestamos where i = @i 
        
		if ( dbo.PrestamoFonacot_PuedeCancelar( @YEAR, @MES, @iCB_CODIGO, @PR_TIPO,  @sPR_REFEREN) = 'S' ) 
			exec SP_FONACOT_ACTUALIZAR_UN_PRESTAMO @iCB_CODIGO, @sPR_REFEREN, @PR_TIPO, 1, @US_CODIGO, 0
                
        set @i = @i + 1 
    end 
end
GO

if exists (select * from sysobjects where id = object_id(N'FK_PENSIONES_COLABORA') and xtype in (N'F'))
BEGIN
    ALTER TABLE PENSION DROP CONSTRAINT FK_PENSIONES_COLABORA
END

ALTER TABLE PENSION
       ADD CONSTRAINT FK_PENSIONES_COLABORA
       FOREIGN KEY (CB_CODIGO)
       REFERENCES COLABORA
       ON DELETE CASCADE
       ON UPDATE CASCADE	   
GO

IF exists( select * from sys.objects where object_id = object_id('V_EMP_TIMB_SIMPLE') )
    drop view V_EMP_TIMB_SIMPLE
go 
create view V_EMP_TIMB_SIMPLE
as
select distinct CB_CODIGO, PRETTYNAME, CB_CURP, CB_RFC  FROM V_EMP_TIMB EMP 

Go 

IF exists( select * from sys.objects where object_id = object_id('VTIMCONCIL') )
    drop view VTIMCONCIL
GO
create view VTIMCONCIL
as
select 
	RSOCIAL.RS_CODIGO,
    NO.PE_YEAR, NO.PE_TIPO, NO.PE_NUMERO, 
	NO.CB_CODIGO, 
	EMP.PRETTYNAME, 	
	NO.NO_TIMBRO, 
	case 
		when NO.NO_TIMBRO = 0 then 'Pendiente' 
		when NO.NO_TIMBRO = 2 then 'Timbrada' 
		else 'Pendiente' 
	end  NO_TIM_DESC, 
	case  
		when NO.NO_TIMBRO = 0 and NO.NO_CAN_UID = ''	then NO.NO_STATUS  
		when NO.NO_TIMBRO = 0 and NO.NO_CAN_UID <> ''   then NO.NO_STATUS 
		when NO.NO_TIMBRO = 2							then 8 
		else	
			NO.NO_STATUS 				
	end NC_STATUS_TRESS, 	
	coalesce(NC_STATUS,-1) NC_STATUS, 
	coalesce(NC_RESULT, -1 ) NC_RESULT, 
	coalesce(NC_RES_DESC, '' ) NC_RES_DESC, 
	coalesce(NC_RES_DESC, '' ) NC_RES_DES, 
    coalesce(NC_VER_COD, '' ) NC_VER_COD, 
	coalesce(NC_VERSION, '' ) NC_VERSION, 
	NO.NO_FACUUID,
	NO.NO_FACTURA, 
	coalesce(NC_FACTURA, 0 ) NC_FACTURA,
	coalesce(NC_UUID , '' ) NC_UUID, 
	coalesce(NC_FEC_TIM, '1899-12-30') NC_FEC_TIM,
	EMP.CB_CURP, 
	coalesce(NC_CURP, '') NC_CURP , 
	coalesce(NC_APLICADO, 'N' ) NC_APLICADO , 
	coalesce(NC_APLICADO, 'N' ) NC_APLICAD , 
	EMP.CB_RFC 
from NOMINA  NO
join RPATRON on RPATRON.TB_CODIGO = NO.CB_PATRON 
join RSOCIAL on RSOCIAL.RS_CODIGO = RPATRON.RS_CODIGO
left outer join V_EMP_TIMB_SIMPLE EMP on EMP.CB_CODIGO = NO.CB_CODIGO 
left outer join NOM_CONCIL NC on NC.PE_YEAR = NO.PE_YEAR and NC.PE_TIPO = NO.PE_TIPO and NC.PE_NUMERO = NO.PE_NUMERO  and  NC.CB_CODIGO = NO.CB_CODIGO 


GO


if exists (select * from dbo.sysobjects where id = object_id(N'FONACOT_CEDULA_TRABAJADOR') and xtype in (N'FN', N'IF', N'TF')) 
	drop function FONACOT_CEDULA_TRABAJADOR
GO 

create function FONACOT_CEDULA_TRABAJADOR
(	@YEAR SMALLINT,
    @MES SMALLINT,
	@RAZON_SOCIAL CHAR(6), 
	@IgnorarIncapacidades Booleano)
RETURNS @CEDULA TABLE
(
	NO_CT int,
	ANIO_EMISION smallint,
	MES_EMISION smallint,
    NO_FONACOT varchar(30),
    RFC varchar(30),
    NO_SS varchar(11),
	NOMBRE VARCHAR(250),
	CLAVE_EMPLEADO int,
	CTD_CREDITOS int,
	RETENCION_MENSUAL numeric(15,2),
	TIPO char(1),
	RETENCION_REAL numeric (15,2),
	FECHA_BAJA varchar(10),
	FECHA_INI varchar(10),
	FECHA_FIN varchar(10)
	)
AS
BEGIN
	insert into @CEDULA
	( NO_CT, ANIO_EMISION, MES_EMISION, NO_FONACOT, RFC, NO_SS, NOMBRE, CLAVE_EMPLEADO, CTD_CREDITOS, RETENCION_MENSUAL, TIPO, RETENCION_REAL,
		FECHA_BAJA, FECHA_INI, FECHA_FIN)	

	select PE.PE_NO_CT, @YEAR ANIO_EMISION, @MES MES_EMISION, CB_FONACOT, CB_RFC, CB_SEGSOC, PRETTYNAME, CB_CODIGO, PF_CTD, PF_PAGO,			
			case
				when FE_INCIDE = 'B' then FE_INCIDE
			    when @IgnorarIncapacidades = 'S' and FE_INCIDE = 'I' and FE_DIF_QTY < FE_CUANTOS  then  '0'				
				when FE_INCIDE not in ('B', '0', 'I', 'L', 'M' ) then '0' 				
				else FE_INCIDE 				
			end as 
			FE_INCIDE,
			FC_NOMINA+FC_AJUSTE AS RETENCION_REAL,
			case 
				when FE_INCIDE = 'B' then FE_FECHA1 
				when @IgnorarIncapacidades = 'S' and FE_INCIDE = 'I' and FE_DIF_QTY < FE_CUANTOS  then  '' 		
				else '' 
			end as 
			FECHA_BAJA,
			case 
				when FE_INCIDE = 'B' then '' 
				when @IgnorarIncapacidades = 'S' and FE_INCIDE = 'I' and FE_DIF_QTY < FE_CUANTOS  then  '' 			
				else FE_FECHA1 
			end  as 
			FECHA_INI,
			case 
				when FE_INCIDE = 'B' then '' 
				when @IgnorarIncapacidades = 'S' and FE_INCIDE = 'I' and FE_DIF_QTY < FE_CUANTOS  then  '' 			
				else FE_FECHA2
			end  as 
			FECHA_FIN
	from VCED_DET VC, PCED_ENC PE
	WHERE 	PF_YEAR = @YEAR AND PF_MES = @MES	AND ( VC.RS_CODIGO = @RAZON_SOCIAL or @RAZON_SOCIAL = '')
	AND VC.PF_YEAR = PE.PE_YEAR AND VC.PF_MES = PE.PE_MES
	AND VC.RS_CODIGO = PE.RS_CODIGO
		
	return
END
GO 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFTransaccion_GetXMLAltaEmpleadoGenerica') AND type IN ( N'P', N'PC' ))
	drop procedure WFTransaccion_GetXMLAltaEmpleadoGenerica
go

CREATE  PROCEDURE WFTransaccion_GetXMLAltaEmpleadoGenerica( 
	@TransID	INT,
	@Xml		XML OUTPUT ,
	@FileID		INT OUTPUT, 
	@Error		VARCHAR( 255 ) OUTPUT)
AS
begin 	
	DECLARE @ErrorID	INT
	DECLARE @Cargo	XML
	
	SELECT	@Cargo	= TransCargo
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID	
	
	DECLARE @EMPRESA			varchar(10) 
	DECLARE @kCB_CODIGO			INT
	DECLARE @NOMBRE				Formula 
	DECLARE @KarFecha			Fecha 
	
	DECLARE @PersonaID			FolioGrande 		
	DECLARE @EquipoID			FolioGrande  	
	DECLARE @PersonaTipo		int 
	DECLARE @KarObservaciones	Titulo  
	DECLARE @kCB_AUTOSAL		Booleano
	DECLARE @kCB_AUTOSAL_GLOBAL Booleano

	DECLARE @kCB_APE_MAT	Descripcion
	DECLARE @kCB_APE_PAT	Descripcion
	DECLARE @kCB_NOMBRES	Descripcion


	DECLARE @kCB_BAN_ELE	Descripcion
	DECLARE @kCB_CALLE		Observaciones
	DECLARE @kCB_CARRERA	DescLarga
	DECLARE @kCB_CHECA		Booleano
	DECLARE @kCB_CIUDAD		Descripcion
	DECLARE @kCB_CLASIFI	Codigo
	DECLARE @kCB_CODPOST	Referencia
	DECLARE @kCB_COLONIA	Descripcion
	DECLARE @kCB_CONTRAT	Codigo1
	DECLARE @kCB_CREDENC	Codigo1
	DECLARE @kCB_CTA_VAL	Descripcion
	DECLARE @kCB_CURP		Descripcion
	DECLARE @kCB_E_MAIL		Formula
	DECLARE @kCB_ESTADO		Codigo2
	DECLARE @kCB_ESTUDIO	Codigo2
	DECLARE @kCB_FEC_ANT	Fecha
	DECLARE @kCB_FEC_CON	Fecha
	DECLARE @kCB_FEC_ING	Fecha
	DECLARE @kCB_FEC_NAC	Fecha
	DECLARE @kCB_LUG_NAC	Descripcion
	DECLARE @kCB_MUNICIP	Codigo
	DECLARE @kCB_NACION		Descripcion
	DECLARE @kCB_NIVEL0		Codigo
	DECLARE @kCB_NIVEL1		Codigo
	DECLARE @kCB_NIVEL2		Codigo
	DECLARE @kCB_NIVEL3		Codigo
	DECLARE @kCB_NIVEL4		Codigo
	DECLARE @kCB_NIVEL5		Codigo
	DECLARE @kCB_NIVEL6		Codigo
	DECLARE @kCB_NIVEL7		Codigo
	DECLARE @kCB_NIVEL8		Codigo
	DECLARE @kCB_NIVEL9		Codigo
	DECLARE @kCB_PATRON		RegPatronal
	DECLARE @kCB_PUESTO		Codigo
	DECLARE @kCB_RFC		Descripcion
	DECLARE @kCB_SALARIO	PesosDiario
	DECLARE @PersonaSalario PesosDiario
	DECLARE @kCB_SALARIO_BRUTO	PesosDiario
	DECLARE @kCB_SEGSOC		Descripcion
	DECLARE @kCB_SEXO		Codigo1
	DECLARE @kCB_TABLASS	Codigo1
	DECLARE @kCB_TEL		Descripcion
	DECLARE @kCB_TURNO		Codigo
	DECLARE @kCB_ZONA_GE	ZonaGeo

	---PADRES
	DECLARE @kNOM_PADRE	Descripcion
	DECLARE @kNOM_MADRE	Descripcion

	---- CAMPOS NUEVOS A MAPEAR
	DECLARE @kCB_AR_FEC as Fecha
	DECLARE @kCB_AR_HOR as Hora
	DECLARE @kCB_EDO_CIV as Codigo1
	DECLARE @kCB_FEC_RES as Fecha
	DECLARE @kCB_EST_HOR as Descripcion
	DECLARE @kCB_EST_HOY as Booleano
	DECLARE @kCB_EST_HOY_INT as Status
	DECLARE @kCB_EVALUA as DiasFrac
	DECLARE @kCB_EXPERIE as Titulo
	DECLARE @kCB_FEC_BAJ as Fecha
	DECLARE @kCB_FEC_BSS as Fecha
	DECLARE @kCB_FEC_INT as Fecha
	DECLARE @kCB_FEC_REV as Fecha
	DECLARE @kCB_FEC_VAC as Fecha
	DECLARE @kCB_G_FEC_1 as Fecha
	DECLARE @kCB_G_FEC_2 as Fecha
	DECLARE @kCB_G_FEC_3 as Fecha
	DECLARE @kCB_G_FEC_4 as Fecha
	DECLARE @kCB_G_FEC_5 as Fecha
	DECLARE @kCB_G_FEC_6 as Fecha
	DECLARE @kCB_G_FEC_7 as Fecha
	DECLARE @kCB_G_FEC_8 as Fecha
	DECLARE @kCB_G_LOG_1 as Booleano
	DECLARE @kCB_G_LOG_1_INT as Status
	DECLARE @kCB_G_LOG_2 as Booleano
	DECLARE @kCB_G_LOG_2_INT as Status
	DECLARE @kCB_G_LOG_3 as Booleano
	DECLARE @kCB_G_LOG_3_INT as Status
	DECLARE @kCB_G_LOG_4 as Booleano
	DECLARE @kCB_G_LOG_4_INT as Status
	DECLARE @kCB_G_LOG_5 as Booleano
	DECLARE @kCB_G_LOG_5_INT as Status
	DECLARE @kCB_G_LOG_6 as Booleano
	DECLARE @kCB_G_LOG_6_INT as Status
	DECLARE @kCB_G_LOG_7 as Booleano
	DECLARE @kCB_G_LOG_7_INT as Status
	DECLARE @kCB_G_LOG_8 as Booleano
	DECLARE @kCB_G_LOG_8_INT as Status
	DECLARE @kCB_G_NUM_1 as Pesos
	DECLARE @kCB_G_NUM_2 as Pesos
	DECLARE @kCB_G_NUM_3 as Pesos
	DECLARE @kCB_G_NUM_4 as Pesos
	DECLARE @kCB_G_NUM_5 as Pesos
	DECLARE @kCB_G_NUM_6 as Pesos
	DECLARE @kCB_G_NUM_7 as Pesos
	DECLARE @kCB_G_NUM_8 as Pesos
	DECLARE @kCB_G_NUM_9 as Pesos
	DECLARE @kCB_G_NUM10 as Pesos
	DECLARE @kCB_G_NUM11 as Pesos
	DECLARE @kCB_G_NUM12 as Pesos
	DECLARE @kCB_G_NUM13 as Pesos
	DECLARE @kCB_G_NUM14 as Pesos
	DECLARE @kCB_G_NUM15 as Pesos
	DECLARE @kCB_G_NUM16 as Pesos
	DECLARE @kCB_G_NUM17 as Pesos
	DECLARE @kCB_G_NUM18 as Pesos
	DECLARE @kCB_G_TAB_1 as Codigo
	DECLARE @kCB_G_TAB_2 as Codigo
	DECLARE @kCB_G_TAB_3 as Codigo
	DECLARE @kCB_G_TAB_4 as Codigo
	DECLARE @kCB_G_TAB_5 as Codigo
	DECLARE @kCB_G_TAB_6 as Codigo
	DECLARE @kCB_G_TAB_7 as Codigo
	DECLARE @kCB_G_TAB_8 as Codigo
	DECLARE @kCB_G_TAB_9 as Codigo
	DECLARE @kCB_G_TAB10 as Codigo
	DECLARE @kCB_G_TAB11 as Codigo
	DECLARE @kCB_G_TAB12 as Codigo
	DECLARE @kCB_G_TAB13 as Codigo
	DECLARE @kCB_G_TAB14 as Codigo
	DECLARE @kCB_G_TEX_1 as Descripcion
	DECLARE @kCB_G_TEX_2 as Descripcion
	DECLARE @kCB_G_TEX_3 as Descripcion
	DECLARE @kCB_G_TEX_4 as Descripcion
	DECLARE @kCB_G_TEX_5 as Descripcion
	DECLARE @kCB_G_TEX_6 as Descripcion
	DECLARE @kCB_G_TEX_7 as Descripcion
	DECLARE @kCB_G_TEX_8 as Descripcion
	DECLARE @kCB_G_TEX_9 as Descripcion
	DECLARE @kCB_G_TEX10 as Descripcion
	DECLARE @kCB_G_TEX11 as Descripcion
	DECLARE @kCB_G_TEX12 as Descripcion
	DECLARE @kCB_G_TEX13 as Descripcion
	DECLARE @kCB_G_TEX14 as Descripcion
	DECLARE @kCB_G_TEX15 as Descripcion
	DECLARE @kCB_G_TEX16 as Descripcion
	DECLARE @kCB_G_TEX17 as Descripcion
	DECLARE @kCB_G_TEX18 as Descripcion
	DECLARE @kCB_G_TEX19 as Descripcion
	DECLARE @kCB_HABLA as Booleano
	DECLARE @kCB_HABLA_INT as Status
	DECLARE @kCB_IDIOMA as Descripcion
	DECLARE @kCB_INFCRED as Descripcion
	DECLARE @kCB_INFMANT as Booleano
	DECLARE @kCB_INFMANT_INT as Status
	DECLARE @kCB_INFTASA as DescINFO
	DECLARE @kCB_LA_MAT as Descripcion
	DECLARE @kCB_LAST_EV as Fecha
	DECLARE @kCB_MAQUINA as Titulo
	DECLARE @kCB_MED_TRA as Codigo1
	DECLARE @kCB_PASAPOR as Booleano
	DECLARE @kCB_PASAPOR_INT as Status
	DECLARE @kCB_FEC_INC as Fecha
	DECLARE @kCB_FEC_PER as Fecha
	DECLARE @kCB_NOMYEAR as Anio
	DECLARE @kCB_NOMTIPO as NominaTipo
	DECLARE @kCB_NEXT_EV as Fecha
	DECLARE @kCB_NOMNUME as NominaNumero
	DECLARE @kCB_SAL_INT as PesosDiario
	DECLARE @kCB_VIVECON as Codigo1
	DECLARE @kCB_VIVEEN as Codigo2
	DECLARE @kCB_ZONA as Referencia
	DECLARE @kCB_INFTIPO as Status
	DECLARE @kCB_DER_FEC as Fecha
	DECLARE @kCB_DER_PAG as DiasFrac
	DECLARE @kCB_V_PAGO as DiasFrac
	DECLARE @kCB_DER_GOZ as DiasFrac
	DECLARE @kCB_V_GOZO as DiasFrac
	DECLARE @kCB_TIP_REV as Codigo
	DECLARE @kCB_MOT_BAJ as Codigo3
	DECLARE @kCB_FEC_SAL as Fecha
	DECLARE @kCB_INF_INI as Fecha
	DECLARE @kCB_INF_OLD as DescINFO
	DECLARE @kCB_OLD_SAL as PesosDiario
	DECLARE @kCB_OLD_INT as PesosDiario
	DECLARE @kCB_PRE_INT as PesosDiario
	DECLARE @kCB_PER_VAR as PesosDiario
	DECLARE @kCB_SAL_TOT as PesosDiario
	DECLARE @kCB_TOT_GRA as PesosDiario
	DECLARE @kCB_FAC_INT as Tasa
	DECLARE @kCB_RANGO_S as Tasa
	DECLARE @kCB_CLINICA as Codigo3
	DECLARE @kCB_FEC_NIV as Fecha
	DECLARE @kCB_FEC_PTO as Fecha
	DECLARE @kCB_AREA as Codigo
	DECLARE @kCB_FEC_TUR as Fecha
	DECLARE @kCB_FEC_KAR as Fecha
	DECLARE @kCB_PENSION as Status
	DECLARE @kCB_CANDIDA as FolioGrande
	DECLARE @kCB_ID_NUM as Descripcion
	DECLARE @kCB_ENT_NAC as Codigo2
	DECLARE @kCB_COD_COL as Codigo
	DECLARE @kCB_PASSWRD as Passwd
	DECLARE @kCB_PLAZA as FolioGrande
	DECLARE @kCB_FEC_PLA as Fecha
	DECLARE @kCB_DER_PV as DiasFrac
	DECLARE @kCB_V_PRIMA as DiasFrac
	DECLARE @kCB_SUB_CTA as Descripcion
	DECLARE @kCB_NETO as Pesos
	DECLARE @kCB_PORTAL as Booleano
	DECLARE @kCB_PORTAL_INT as Status
	DECLARE @kCB_DNN_OK as Booleano
	DECLARE @kCB_DNN_OK_INT as Status
	DECLARE @kCB_USRNAME as DescLarga
	DECLARE @kCB_NOMINA as NominaTipo
	DECLARE @kCB_FEC_NOM as Fecha
	DECLARE @kCB_RECONTR as Booleano
	DECLARE @kCB_RECONTR_INT as Status
	DECLARE @kCB_DISCAPA as Booleano
	DECLARE @kCB_DISCAPA_INT as Status
	DECLARE @kCB_INDIGE as Booleano
	DECLARE @kCB_INDIGE_INT as Status
	DECLARE @kCB_FONACOT as Descripcion
	DECLARE @kCB_EMPLEO as Booleano
	DECLARE @kCB_EMPLEO_INT as Status
	DECLARE @kUS_CODIGO as Usuario
	DECLARE @kCB_FEC_COV as Fecha
	DECLARE @kCB_G_TEX20 as Descripcion
	DECLARE @kCB_G_TEX21 as Descripcion
	DECLARE @kCB_G_TEX22 as Descripcion
	DECLARE @kCB_G_TEX23 as Descripcion
	DECLARE @kCB_G_TEX24 as Descripcion
	DECLARE @kCB_INFDISM as Booleano
	DECLARE @kCB_INFDISM_INT as Status
	DECLARE @kCB_INFACT as Booleano
	DECLARE @kCB_INFACT_INT as Status
	DECLARE @kCB_NUM_EXT as NombreCampo 
	DECLARE @kCB_NUM_INT as NombreCampo
	DECLARE @kCB_INF_ANT as Fecha
	DECLARE @kCB_TDISCAP as Status
	DECLARE @kCB_ESCUELA as DescLarga
	DECLARE @kCB_TESCUEL as Status
	DECLARE @kCB_TITULO as Status
	DECLARE @kCB_YTITULO as Anio
	DECLARE @kCB_CTA_GAS as Descripcion
	DECLARE @kCB_ID_BIO as FolioGrande
	DECLARE @kCB_GP_COD as Codigo
	DECLARE @kCB_TIE_PEN as Booleano
	DECLARE @kCB_TIE_PEN_INT as Status
	DECLARE @kCB_TSANGRE as Descripcion
	DECLARE @kCB_ALERGIA as Formula
	DECLARE @kCB_BRG_ACT as Booleano
	DECLARE @kCB_BRG_ACT_INT as Status
	DECLARE @kCB_BRG_TIP as Status
	DECLARE @kCB_BRG_ROL as Booleano
	DECLARE @kCB_BRG_ROL_INT as Status
	DECLARE @kCB_BRG_JEF as Booleano
	DECLARE @kCB_BRG_JEF_INT as Status
	DECLARE @kCB_BRG_CON as Booleano
	DECLARE @kCB_BRG_CON_INT as Status
	DECLARE @kCB_BRG_PRA as Booleano
	DECLARE @kCB_BRG_PRA_INT as Status
	DECLARE @kCB_BRG_NOP as Descripcion
	DECLARE @kCB_BANCO as Codigo
	DECLARE @kCB_REGIMEN as Status

	DECLARE @MOV3 as Descripcion
	DECLARE @EmpNumero as Codigo
	DECLARE @IngresoReingreso as Codigo
	----- FIN	
	
	DECLARE @XMLTress  Varchar(max) 
	

	declare @ClienteID FolioGrande 
	set @FileID = 0 
	SET @ErrorID = 0 
	set @ERROR = '' 

	--Toma globales de Empleado 		
	select 
		@kCB_Ciudad = Ciudad , 
		@kCB_Estado = Estado , 
		@kCB_MUNICIP = Municipio , 
		@kCB_Checa = Checa , 
		@kCB_Nacion = Nacion , 
		@kCB_Sexo = Sexo , 
		@kCB_Credenc = Credencial , 
		@kCB_EDO_CIV = EstadoCivil , 
		@kCB_ViveEn = ViveEn , 
		@kCB_ViveCon = ViveCon , 
		@kCB_MED_TRA = MedioTransporte , 
		@kCB_Estudio = Estudio , 
		@kCB_AUTOSAL_GLOBAL = AutoSal , 
		@kCB_Zona = ZonaGeografica , 
		@kCB_Patron = Patron , 
		@kCB_Salario = Salario , 
		@kCB_Contrat = Contrato , 
		@kCB_Puesto = Puesto , 
		@kCB_Clasifi = Clasifi , 
		@kCB_Turno = Turno , 
		@kCB_TablaSS = TablaSS , 
		@kCB_Nivel1 = Nivel1 , 
		@kCB_Nivel2 = Nivel2 , 
		@kCB_Nivel3 = Nivel3 , 
		@kCB_Nivel4 = Nivel4 , 
		@kCB_Nivel5 = Nivel5 , 
		@kCB_Nivel6 = Nivel6 , 
		@kCB_Nivel7 = Nivel7 , 
		@kCB_Nivel8 = Nivel8 , 
		@kCB_Nivel9 = Nivel9 , 
		@kCB_Regimen = Regimen , 
		@KCB_Banco = Banco 
	from dbo.WF_GetGlobalesEmpleado() 

	SET	@ClienteID	= coalesce(  @Cargo.value('(//REGISTRO/@ClienteID)[1]', 'INT' ), 0)
	SET	@EmpNumero	= coalesce(  @Cargo.value('(//REGISTRO/@EmpNumero)[1]', 'INT' ), 0)  
	SET	@IngresoReingreso	= coalesce(  @Cargo.value('(//REGISTRO/@IngresoReingreso)[1]', 'INT' ), 0) 
	SET @EMPRESA	= dbo.WFEmpresas_GetCodigoEmpresa( @ClienteID ) ; 
	
	SET	@PersonaID	= coalesce(  @Cargo.value('(//REGISTRO/@PersonaID)[1]', 'INT' ),  @TransID )     	
	
	--- IDENTIFICACION DEL EMPLEADO 
	SET	@KCB_APE_PAT		= coalesce( @Cargo.value('(//REGISTRO/@PersonaApePat)[1]', 'Varchar(256)' ), '' ) 
	SET	@KCB_APE_MAT		= coalesce( @Cargo.value('(//REGISTRO/@PersonaApeMat)[1]', 'Varchar(256)' ), '' ) 
	SET	@KCB_NOMBRES		= coalesce( @Cargo.value('(//REGISTRO/@PersonaNombres)[1]', 'Varchar(256)' ), '' ) 	
	SET @kCB_FEC_NAC  = coalesce(@Cargo.value('(//REGISTRO/@PersonaNacFecha)[1]', 'Date' ), '1899-12-30' ) 	
	SET	@KCB_RFC		= coalesce( @Cargo.value('(//REGISTRO/@PersonaRFC)[1]', 'Varchar(256)' ), '' ) 
	SET	@KCB_CURP		= coalesce( @Cargo.value('(//REGISTRO/@PersonaCURP)[1]', 'Varchar(256)' ), '' ) 
	SET	@kCB_SEGSOC		= coalesce( @Cargo.value('(//REGISTRO/@PersonaIMSS)[1]', 'Varchar(256)' ), '' ) 	
	SET	@KCB_SEXO		=case ( coalesce( @Cargo.value('(//REGISTRO/@PersonaGenero)[1]', 'int' ), -1 ) ) 
									when  1 then 'M' 
	                                when  2 then 'F'
									else  @KCB_SEXO 
						end;  
	SET @kCB_ENT_NAC = coalesce( @Cargo.value('(//REGISTRO/@NacEstadoCodigo)[1]', 'Varchar(2)' ), '' ) -- as Codigo2
	
	-- PERSONALES  
	SET @kCB_LUG_NAC   = coalesce(@Cargo.value('(//REGISTRO/@PersonaNacCiudad)[1]', 'Varchar(30)' ), '' ) 
	SET @kCB_NACION    = coalesce(@Cargo.value('(//REGISTRO/@PersonaNacionalidad)[1]', 'Varchar(30)' ), '' ) 	
	SET	@kCB_PASAPOR_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaPasaporte)[1]', 'INT' ), 0)	
	if ( @kCB_PASAPOR_INT = 1 ) 
		Set @kCB_PASAPOR = 'S' 
	else
		Set @kCB_PASAPOR = 'N'
	SET @kCB_EDO_CIV = coalesce( @Cargo.value('(//REGISTRO/@EdoCivilCodigo)[1]', 'Varchar(1)' ), @kCB_EDO_CIV ) --Estado Civil
	SET @kCB_LA_MAT = coalesce( @Cargo.value('(//REGISTRO/@PersonaLugarMat)[1]', 'Varchar(30)' ), '' ) --Lugar y Fecha de matrimonio
	SET @kNOM_PADRE = coalesce( @Cargo.value('(//REGISTRO/@PersonaNomPad)[1]', 'Varchar(30)' ), '' )
	SET @kNOM_MADRE = coalesce( @Cargo.value('(//REGISTRO/@PersonaNomMad)[1]', 'Varchar(30)' ), '' )
	SET @kCB_MED_TRA = coalesce( @Cargo.value('(//REGISTRO/@TransporteCodigo)[1]', 'Varchar(1)' ), @kCB_MED_TRA  ) --Transporte	
	SET	@kCB_DISCAPA_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaSufreDiscapa)[1]', 'INT' ), 0)	
	if ( @kCB_DISCAPA_INT = 1 ) 
		Set @kCB_DISCAPA = 'S' 
	else
		Set @kCB_DISCAPA = 'N'
	SET @kCB_TDISCAP = coalesce(@Cargo.value('(//REGISTRO/@PersonaDiscapa)[1]', 'INT' ), 0) -- as Status		
	--SET @kCB_INDIGE as Booleano
	SET	@kCB_INDIGE_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaIndigena)[1]', 'INT' ), 0)	
	if ( @kCB_INDIGE_INT = 1 ) 
		Set @kCB_INDIGE = 'S' 
	else
		Set @kCB_INDIGE = 'N'
		
	-- DOMICILIO 
	SET @kCB_CALLE = coalesce( @Cargo.value('(//REGISTRO/@DomicilioCalle)[1]', 'Varchar(50)' ), '' ) 
	SET @kCB_NUM_EXT = coalesce( @Cargo.value('(//REGISTRO/@DomicilioNumExt)[1]', 'Varchar(10)' ), '' ) 
	SET @kCB_NUM_INT = coalesce( @Cargo.value('(//REGISTRO/@DomicilioNumInt)[1]', 'Varchar(10)' ), '' ) 
	SET @kCB_COLONIA = coalesce( @Cargo.value('(//REGISTRO/@DomicilioColonia)[1]', 'Varchar(30)' ), '' ) 
	SET @kCB_CODPOST = coalesce( @Cargo.value('(//REGISTRO/@DomicilioCodPost)[1]', 'Varchar(8)' ), '' ) 
	SET @kCB_COD_COL = coalesce( @Cargo.value('(//REGISTRO/@ColoniaCodigo)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_CLINICA = coalesce( @Cargo.value('(//REGISTRO/@Clinica)[1]', 'Varchar(3)' ), '' ) -- as Codigo3
	SET @kCB_ZONA 	 =  dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@DomicilioZona)[1]', 'Varchar(8)' ), @kCB_ZONA )  -- as Referencia	
	SET	@KCB_CIUDAD	 =  dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@DomicilioCiudad)[1]', 'Varchar(30)' ), @kCB_Ciudad ) 	
	SET	@KCB_ESTADO	 =  dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@EstadoCodigo)[1]', 'Varchar(6)' ), @kCB_ESTADO ) 
	SET @kCB_MUNICIP =  dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@MunicipioCodigo)[1]', 'Varchar(6)' ), @kCB_MUNICIP ) 
	
	-- CONTACTO 
	SET @kCB_TEL = coalesce( @Cargo.value('(//REGISTRO/@PersonaTelefono)[1]', 'Varchar(30)' ), '' ) 
	SET @kCB_E_MAIL = coalesce( @Cargo.value('(//REGISTRO/@PersonaEmail)[1]', 'Varchar(255)' ), '' ) 
	SET @kCB_VIVECON = coalesce( @Cargo.value('(//REGISTRO/@ViveConCodigo)[1]', 'Varchar(1)' ), @kCB_ViveCon  )  -- as Codigo1
	SET @kCB_VIVEEN = coalesce( @Cargo.value('(//REGISTRO/@ViveEnCodigo)[1]', 'Varchar(2)' ),  @kCB_VIVEEN )  -- as Codigo2
	SET @kCB_FEC_RES = coalesce(@Cargo.value('(//REGISTRO/@ResidenciaFecha)[1]', 'Date' ), '1899-12-30' ) --Fecha Residencia 	

	--- EXPERIENCIA  
	-- Exp. Academica 
	SET @kCB_ESTUDIO =  dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@EstudioCodigo)[1]', 'Varchar(2)' ),  @kCB_VIVEEN )  -- as Codigo2
	SET @kCB_ESCUELA =  coalesce( @Cargo.value('(//REGISTRO/@PersonaEscuela)[1]', 'Varchar(40)' ), '' )	
	SET @kCB_TESCUEL = coalesce(@Cargo.value('(//REGISTRO/@PersonaEstTipo)[1]', 'INT' ), 0) -- as Status
	SET @kCB_CARRERA  = coalesce( @Cargo.value('(//REGISTRO/@PersonaCarrera)[1]', 'Varchar(40)' ), '' ) 		
	SET @kCB_TITULO = coalesce(@Cargo.value('(//REGISTRO/@PersonaTitulo)[1]', 'INT' ), 0) -- as Anio
	SET @kCB_YTITULO = coalesce(@Cargo.value('(//REGISTRO/@PersonaEstYear)[1]', 'INT' ), 0) -- as Anio
	SET @kCB_EST_HOR = coalesce( @Cargo.value('(//REGISTRO/@PersonaEstudiaHorario)[1]', 'Varchar(30)' ), '' ) --Carrera y Horario
	---DECLARE @kCB_EST_HOY as Booleano
	SET	@kCB_EST_HOY_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaEstudia)[1]', 'INT' ), 0)	
	if ( @kCB_EST_HOY_INT = 1 ) 
		Set @kCB_EST_HOY = 'S' 
	else
		Set @kCB_EST_HOY = 'N'

	-- Habilidades 
	SET	@kCB_HABLA_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaOtroIdioma)[1]', 'INT' ), 0)	
	if ( @kCB_HABLA_INT = 1 ) 
		Set @kCB_HABLA = 'S' 
	else
		Set @kCB_HABLA = 'N'
	SET @kCB_IDIOMA = coalesce( @Cargo.value('(//REGISTRO/@PersonaIdiomas)[1]', 'Varchar(30)' ), '' ) --Idioma o dominio
	SET @kCB_MAQUINA = coalesce( @Cargo.value('(//REGISTRO/@PersonaMaquinas)[1]', 'Varchar(100)' ), '' ) --Maquinas que conoce
	SET @kCB_EXPERIE = coalesce( @Cargo.value('(//REGISTRO/@PersonaExperiencia)[1]', 'Varchar(100)' ), '' )--Experiencia
	
	
	-- Contratacion 	
	SET @kCB_FEC_ING  = coalesce(@Cargo.value('(//REGISTRO/@EmpIngresoFecha)[1]', 'Date' ), '1899-12-30' ) 	
	SET @kCB_FEC_ANT  = coalesce(@Cargo.value('(//REGISTRO/@EmpAntigFecha)[1]', 'Date' ), '1899-12-30' ) 	
	
	-- Contrato 
	SET @kCB_FEC_CON  = coalesce(@Cargo.value('(//REGISTRO/@EmpContratoIni)[1]', 'Date' ), @kCB_FEC_ING ) 	
	SET	@KCB_CONTRAT  =  dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@ContratoCodigo)[1]', 'Varchar(6)' ), @KCB_CONTRAT ) 
		--Calcular el numero de dias para la fecha de vencimiento del contrato
		declare @DiasContrato as FolioChico
		SELECT @DiasContrato = TB_DIAS FROM CONTRATO WHERE TB_CODIGO = @kCB_CONTRAT
		if ( @DiasContrato > 0 )
		begin		 
			set @kCB_FEC_COV = cast( dateadd( day, @DiasContrato -1, @kCB_FEC_CON ) as Datetime ) 
		end
	
		
	SET	@KCB_PUESTO		= coalesce( @Cargo.value('(//REGISTRO/@PuestoCodigo)[1]', 'Varchar(6)' ), '' ) 
	SET @kCB_NOMINA 	= coalesce(@Cargo.value('(//REGISTRO/@TipoNominaCodigo)[1]', 'INT' ), @kCB_NOMINA) -- as NominaTipo
	
	-- Tomados del puesto en caso de que no esten definidos 
	----------------- TOMADOS DEL PUESTO  --------------------------------
	declare @kPU_PATRON Codigo1 
	declare @kPU_TURNO Codigo 
	declare @kPU_AUTOSAL Codigo 
	declare @kPU_TABLASS Codigo 
	declare @KPU_CLASIFI Codigo 
	
	select @KPU_CLASIFI = PU_CLASIFI, @kPU_PATRON = PU_PATRON, @kPU_TURNO = PU_TURNO,  @kPU_AUTOSAL  = PU_AUTOSAL, @kPU_TABLASS = PU_TABLASS from PUESTO where PU_CODIGO = @kCB_PUESTO 

	-- Si no estan definidas en el puesto tomas las default 
	set @kCB_AUTOSAL_GLOBAL = @KCB_AUTOSAL;
    set @kCB_PATRON  = case  coalesce(@kPU_PATRON,'')   when '' then @kCB_PATRON   else @kPU_PATRON  end
	set @kCB_TURNO = case  coalesce(@kPU_TURNO,'')  when '' then @kCB_TURNO  else @kPU_TURNO end	
	set @kCB_TABLASS = case  coalesce(@kPU_TABLASS,'')  when '' then @kCB_TABLASS  else @kPU_TABLASS end
	set @kCB_CLASIFI = case  coalesce(@KPU_CLASIFI,'')  when '' then @kCB_CLASIFI  else @KPU_CLASIFI end
	set @kCB_CLASIFI = coalesce( @kCB_CLASIFI, '' ); 

    -- Al ultimo lee los del Cargo 	
	SET	@KCB_CLASIFI 	= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@TabuladorCodigo)[1]', 'Varchar(6)' ), @KCB_CLASIFI ) 
	SET	@KCB_TURNO		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@TurnoCodigo)[1]', 'Varchar(6)' ), @KCB_TURNO ) 
	SET	@KCB_PATRON		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@RegPatronCodigo)[1]', 'Varchar(1)' ), @KCB_PATRON ) 	
		
	-- TimbradoNomina 
	SET @kCB_REGIMEN = coalesce(@Cargo.value('(//REGISTRO/@RegimenSAT)[1]', 'INT' ), @kCB_REGIMEN) -- as Status
	
	
	-- AREA 
	SET	@kCB_NIVEL1		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel1Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL1 ) 
	SET	@kCB_NIVEL2		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel2Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL2 ) 
	SET	@kCB_NIVEL3		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel3Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL3 ) 
	SET	@kCB_NIVEL4		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel4Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL4 ) 
	SET	@kCB_NIVEL5		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel5Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL5 ) 
	SET	@kCB_NIVEL6		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel6Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL6 ) 
	SET	@kCB_NIVEL7		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel7Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL7 ) 
	SET	@kCB_NIVEL8		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel8Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL8 ) 
	SET	@kCB_NIVEL9		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel9Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL9 ) 
	
	
    -- SALARIO 	
	SET	@KCB_TABLASS		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@PrestaLeyCodigo)[1]', 'Varchar(6)' ), @KCB_TABLASS ) 	
	SET	@KCB_AUTOSAL	=case coalesce( @Cargo.value('(//REGISTRO/@SalTabula)[1]', 'int' ),  -1 ) 
								    when  0 then 'N'
									when  1 then 'S'
									else @KCB_AUTOSAL
						end;	
	SET	@kCB_SALARIO	= coalesce( @Cargo.value('(//REGISTRO/@SalDiario)[1]', 'Decimal(15,2)' ), @kCB_SALARIO ) 
	SET @kCB_PER_VAR    = coalesce( @Cargo.value('(//REGISTRO/@PromVar)[1]', 'Decimal(15,2)' ), @kCB_SALARIO ) 
	SET	@KCB_ZONA_GE	= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@ZonaGeo)[1]', 'Varchar(1)' ), 'A' ) 	
	
	
	-- Datos del movimiento 
	SET @KarFecha  = coalesce(@Cargo.value('(//REGISTRO/@EmpIngresoFecha)[1]', 'Date' ), '1899-12-30' ) 		
	SET @KarObservaciones  = coalesce(@Cargo.value('(//REGISTRO/@KarObservaciones)[1]', 'Varchar(255)' ), '' ) 
	SET	@FileID	= coalesce(@Cargo.value('(//REGISTRO/@FileID)[1]', 'INT' ), 0)	
	
	SET	@KCB_CHECA		=case ( coalesce( @Cargo.value('(//REGISTRO/@PersonaCheca)[1]', 'int' ), -1 ) ) 
	                                when  0 then 'N'
									when  1 then 'S'
									else @KCB_CHECA
						end;  




	
	
	-----Campos que faltaban de mapear del XML "CargoAltaEmpleado.xml"  -------
	SET @kCB_CTA_VAL = coalesce( @Cargo.value('(//REGISTRO/@PersonaVale)[1]', 'Varchar(30)' ), '' )
	

	---- Campos Faltantes que no se encontraban en el XML "CargoAltaEmpleado.xml" para futuras Altas
	SET @kCB_AR_FEC = coalesce(@Cargo.value('(//REGISTRO/@PersonaArFec)[1]', 'Date' ), '1899-12-30' ) 	
	SET @kCB_AR_HOR = coalesce( @Cargo.value('(//REGISTRO/@PersonaArHor)[1]', 'Varchar(4)' ), '' )
	
	
	
	SET @kCB_EVALUA = coalesce( @Cargo.value('(//REGISTRO/@PersonaEvalua)[1]', 'Decimal(15,2)' ), 0.00 ) --Resultado Evaluacion
	
	SET @kCB_FEC_BAJ = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecBaj)[1]', 'Date' ), '1899-12-30' ) --Fecha Baja	
	SET @kCB_FEC_BSS = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecBss)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_FEC_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecInt)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_FEC_REV = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecRev)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_FEC_VAC = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecVac)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_1 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec1)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_2 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec2)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_3 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec3)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_4 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec4)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_5 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec5)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_6 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec6)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_7 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec7)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_8 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec8)[1]', 'Date' ), '1899-12-30' )
	
	----------------------------------------- VALORES ADICIONALES DE TRESS ---------------------------	
	--DECLARE @kCB_G_LOG_1 as Booleano
	SET	@kCB_G_LOG_1_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog1)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_1_INT = 1 ) 
		Set @kCB_G_LOG_1 = 'S' 
	else
		Set @kCB_G_LOG_1 = 'N'
	--DECLARE @kCB_G_LOG_2 as Booleano
	SET	@kCB_G_LOG_2_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog2)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_2_INT = 1 ) 
		Set @kCB_G_LOG_2 = 'S' 
	else
		Set @kCB_G_LOG_2 = 'N' 
	--DECLARE @kCB_G_LOG_3 as Booleano
	SET	@kCB_G_LOG_3_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog3)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_3_INT = 1 ) 
		Set @kCB_G_LOG_3 = 'S' 
	else
		Set @kCB_G_LOG_3 = 'N' 
	--DECLARE @kCB_G_LOG_4 as Booleano
	SET	@kCB_G_LOG_4_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog4)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_4_INT = 1 ) 
		Set @kCB_G_LOG_4 = 'S' 
	else
		Set @kCB_G_LOG_4 = 'N' 
	--DECLARE @kCB_G_LOG_5 as Booleano
	SET	@kCB_G_LOG_5_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog5)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_5_INT = 1 ) 
		Set @kCB_G_LOG_5 = 'S' 
	else
		Set @kCB_G_LOG_5 = 'N' 
	--DECLARE @kCB_G_LOG_6 as Booleano
	SET	@kCB_G_LOG_6_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog6)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_6_INT = 1 ) 
		Set @kCB_G_LOG_6 = 'S' 
	else
		Set @kCB_G_LOG_6 = 'N' 
	--DECLARE @kCB_G_LOG_7 as Booleano
	SET	@kCB_G_LOG_7_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog7)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_7_INT = 1 ) 
		Set @kCB_G_LOG_7 = 'S' 
	else
		Set @kCB_G_LOG_7 = 'N' 
	--DECLARE @kCB_G_LOG_8 as Booleano
	SET	@kCB_G_LOG_8_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog8)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_8_INT = 1 ) 
		Set @kCB_G_LOG_8 = 'S' 
	else
		Set @kCB_G_LOG_8 = 'N' 

	SET @kCB_G_NUM_1 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum1)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_2 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum2)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_3 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum3)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_4 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum4)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_5 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum5)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_6 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum6)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_7 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum7)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_8 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum8)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_9 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum9)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM10 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum10)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM11 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum11)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM12 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum12)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM13 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum13)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM14 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum14)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM15 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum15)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM16 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum16)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM17 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum17)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM18 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum18)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_TAB_1 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab1)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_2 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab2)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_3 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab3)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_4 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab4)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_5 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab5)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_6 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab6)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_7 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab7)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_8 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab8)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_9 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab9)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB10 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab10)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB11 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab11)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB12 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab12)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB13 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab13)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB14 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab14)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TEX_1  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText1)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_2  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText2)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_3  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText3)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_4  = coalesce( @Cargo.value('(//REGISTRO/@PersonaCLABE)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_5  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText5)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_6  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText6)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_7  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText7)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_8  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText8)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_9  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText9)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX10  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText10)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX11  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText11)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX12  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText12)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX13  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText13)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX14  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText14)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX15  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText15)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX16  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText16)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX17  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText17)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX18  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText18)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX19  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText19)[1]', 'Varchar(30)' ), '' ) -- as Descripcion		
	SET @kCB_G_TEX20  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText20)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX21  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText21)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX22  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText22)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX23  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText23)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX24  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText24)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	
	SET @kCB_INFCRED = coalesce( @Cargo.value('(//REGISTRO/@PersonaInfCred)[1]', 'Varchar(30)' ), '' ) --Credito Infonavit 
	---DECLARE @kCB_INFMANT as Booleano 
	SET	@kCB_INFMANT_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaInfMant)[1]', 'INT' ), 0)	
	if ( @kCB_INFMANT_INT = 1 ) 
		Set @kCB_INFMANT = 'S' 
	else
		Set @kCB_INFMANT = 'N'

	SET @kCB_INFTASA = coalesce( @Cargo.value('(//REGISTRO/@PersonaInfTasa)[1]', 'Decimal(15,2)' ), 0.00 ) --Valor Descuento Infonavit 
	
	SET @kCB_LAST_EV = coalesce(@Cargo.value('(//REGISTRO/@PersonaLastEv)[1]', 'Date' ), '1899-12-30' ) --Fecha de ultima evaluacion
	
	
	SET @kCB_FEC_INC = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecInc)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_FEC_PER = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecPer)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_NOMYEAR = coalesce(@Cargo.value('(//REGISTRO/@PersonaNomYear)[1]', 'INT' ), 0)
	SET @kCB_NOMTIPO = coalesce(@Cargo.value('(//REGISTRO/@PersonaNomTipo)[1]', 'INT' ), 0)
	

	SET @kCB_NEXT_EV = coalesce(@Cargo.value('(//REGISTRO/@PersonaNextEv)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_NOMNUME = coalesce(@Cargo.value('(//REGISTRO/@PersonaNomNume)[1]', 'INT' ), 0) -- as NominaNumero
	SET @kCB_SAL_INT = coalesce( @Cargo.value('(//REGISTRO/@PersonaSalInt)[1]', 'Decimal(15,2)' ), 0.00 ) -- as PesosDiario
	
	SET @kCB_INFTIPO = coalesce(@Cargo.value('(//REGISTRO/@PersonaNomNume)[1]', 'INT' ), 0)  -- as Status
	SET @kCB_DER_FEC = coalesce(@Cargo.value('(//REGISTRO/@PersonaDerFec)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_DER_PAG = coalesce( @Cargo.value('(//REGISTRO/@PersonaDerPag)[1]', 'Decimal(15,2)' ), 0.00 )  -- as DiasFrac
	SET @kCB_V_PAGO = coalesce( @Cargo.value('(//REGISTRO/@PersonaVPago)[1]', 'Decimal(15,2)' ), 0.00 )  -- as DiasFrac
	SET @kCB_DER_GOZ = coalesce( @Cargo.value('(//REGISTRO/@PersonaDerGoz)[1]', 'Decimal(15,2)' ), 0.00 )  -- as DiasFrac
	SET @kCB_V_GOZO = coalesce( @Cargo.value('(//REGISTRO/@PersonaVGozo)[1]', 'Decimal(15,2)' ), 0.00 )  -- as DiasFrac


	SET @kCB_TIP_REV = coalesce( @Cargo.value('(//REGISTRO/@PersonaTipRev)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_MOT_BAJ = coalesce( @Cargo.value('(//REGISTRO/@PersonaMotBaj)[1]', 'Varchar(3)' ), '' ) -- as Codigo3
	SET @kCB_FEC_SAL = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecSal)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_INF_INI = coalesce(@Cargo.value('(//REGISTRO/@PersonaInfIni)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_INF_OLD = coalesce( @Cargo.value('(//REGISTRO/@PersonaInfOld)[1]', 'Decimal(15,2)' ), 0.00 )  -- as DescINFO
	SET @kCB_OLD_SAL = coalesce( @Cargo.value('(//REGISTRO/@PersonaOldSal)[1]', 'Decimal(15,2)' ), 0.00 )  -- as PesosDiario
	SET @kCB_OLD_INT = coalesce( @Cargo.value('(//REGISTRO/@PersonaOldInt)[1]', 'Decimal(15,2)' ), 0.00 )  -- as PesosDiario
	SET @kCB_PRE_INT = coalesce( @Cargo.value('(//REGISTRO/@PersonaPreInt)[1]', 'Decimal(15,2)' ), 0.00 )  -- as PesosDiario
	SET @kCB_SAL_TOT = coalesce( @Cargo.value('(//REGISTRO/@PersonaSalTot)[1]', 'Decimal(15,2)' ), 0.00 )  -- as PesosDiario
	SET @kCB_TOT_GRA = coalesce( @Cargo.value('(//REGISTRO/@PersonaTotGra)[1]', 'Decimal(15,2)' ), 0.00 )  -- as PesosDiario
	SET @kCB_FAC_INT = coalesce( @Cargo.value('(//REGISTRO/@PersonaFacInt)[1]', 'Decimal(15,2)' ), 0.00 )  -- as Tasa
	SET @kCB_RANGO_S = coalesce( @Cargo.value('(//REGISTRO/@PersonaRangoS)[1]', 'Decimal(15,2)' ), 0.00 )  -- as Tasa
	
	SET @kCB_FEC_NIV = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecNiv)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_FEC_PTO = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecPto)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_AREA = coalesce( @Cargo.value('(//REGISTRO/@PersonaArea)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_FEC_TUR = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecTur)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_FEC_KAR = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecKar)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_PENSION = coalesce(@Cargo.value('(//REGISTRO/@PersonaPension)[1]', 'INT' ), 0) -- as Status

	SET @kCB_CANDIDA = coalesce(@Cargo.value('(//REGISTRO/@PersonaCandida)[1]', 'INT' ), 0) -- as FolioGrande
	SET @kCB_ID_NUM = coalesce( @Cargo.value('(//REGISTRO/@PersonaIdNum)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	
	
	SET @kCB_PASSWRD = coalesce( @Cargo.value('(//REGISTRO/@PersonaPasswrd)[1]', 'Varchar(30)' ), '' ) -- as Passwd
	SET @kCB_PLAZA = coalesce(@Cargo.value('(//REGISTRO/@PersonaPlaza)[1]', 'INT' ), 0) -- as FolioGrande
	SET @kCB_FEC_PLA = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecPla)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_DER_PV = coalesce( @Cargo.value('(//REGISTRO/@PersonaDerPv)[1]', 'Decimal(15,2)' ), 0.00 ) -- as DiasFrac
	SET @kCB_V_PRIMA = coalesce( @Cargo.value('(//REGISTRO/@PersonaVPrima)[1]', 'Decimal(15,2)' ), 0.00 ) -- as DiasFrac
	SET @kCB_SUB_CTA = coalesce( @Cargo.value('(//REGISTRO/@PersonaSubCta)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_NETO = coalesce( @Cargo.value('(//REGISTRO/@PersonaNeto)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	
	--SET @kCB_PORTAL as Booleano
	SET	@kCB_PORTAL_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaPortal)[1]', 'INT' ), 0)	
	if ( @kCB_PORTAL_INT = 1 ) 
		Set @kCB_PORTAL = 'S' 
	else
		Set @kCB_PORTAL = 'N' 
	--SET @kCB_DNN_OK as Booleano
	SET	@kCB_DNN_OK_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaDnnOk)[1]', 'INT' ), 0)	
	if ( @kCB_DNN_OK_INT = 1 ) 
		Set @kCB_DNN_OK = 'S' 
	else
		Set @kCB_DNN_OK = 'N' 

	SET @kCB_USRNAME = coalesce( @Cargo.value('(//REGISTRO/@PersonaUsrname)[1]', 'Varchar(40)' ), '' ) -- as DescLarga
	
	SET @kCB_FEC_NOM = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecNom)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	--SET @kCB_RECONTR as Booleano
	SET	@kCB_RECONTR_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaRecontr)[1]', 'INT' ), 0)	
	if ( @kCB_RECONTR_INT = 1 ) 
		Set @kCB_RECONTR = 'S' 
	else
		Set @kCB_RECONTR = 'N'

		
	SET @kCB_FONACOT = coalesce( @Cargo.value('(//REGISTRO/@PersonaFonacot)[1]', 'Varchar(30)' ), '' ) -- as Descripcion

	--SET @kCB_EMPLEO as Booleano
	SET	@kCB_EMPLEO_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaEmpleo)[1]', 'INT' ), 0)	
	if ( @kCB_EMPLEO_INT = 1 ) 
		Set @kCB_EMPLEO = 'S' 
	else
		Set @kCB_EMPLEO = 'N'

	SET @kUS_CODIGO = coalesce(@Cargo.value('(//REGISTRO/@PersonaUsCodigo)[1]', 'INT' ), 0) -- as Usuario
	
	--SET @kCB_INFDISM as Booleano
	SET	@kCB_INFDISM_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaInfDism)[1]', 'INT' ), 0)	
	if ( @kCB_INFDISM_INT = 1 ) 
		Set @kCB_INFDISM = 'S' 
	else
		Set @kCB_INFDISM = 'N'

	--SET @kCB_INFACT as Booleano
	SET	@kCB_INFACT_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaInfAct)[1]', 'INT' ), 0)	
	if ( @kCB_INFACT_INT = 1 ) 
		Set @kCB_INFACT = 'S' 
	else
		Set @kCB_INFACT = 'N'
	
	SET @kCB_INF_ANT = coalesce(@Cargo.value('(//REGISTRO/@PersonaInfAnt)[1]', 'Date' ), '1899-12-30' ) -- as Fecha 
	
	SET @kCB_CTA_GAS = coalesce( @Cargo.value('(//REGISTRO/@PersonaCtaGas)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_ID_BIO = coalesce(@Cargo.value('(//REGISTRO/@PersonaIdBio)[1]', 'INT' ), 0) -- as FolioGrande
	SET @kCB_GP_COD = coalesce( @Cargo.value('(//REGISTRO/@PersonaGpCod)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	--SET @kCB_TIE_PEN as Booleano
	SET	@kCB_TIE_PEN_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaTiePen)[1]', 'INT' ), 0)	
	if ( @kCB_TIE_PEN_INT = 1 ) 
		Set @kCB_TIE_PEN = 'S' 
	else
		Set @kCB_TIE_PEN = 'N'

	SET @kCB_TSANGRE = coalesce( @Cargo.value('(//REGISTRO/@PersonaTSangre)[1]', 'Varchar(6)' ), '' ) -- as Descripcion
	SET @kCB_ALERGIA = coalesce( @Cargo.value('(//REGISTRO/@PersonaAlergia)[1]', 'Varchar(255)' ), '' ) -- as Formula

	--SET @kCB_BRG_ACT as Booleano
	SET	@kCB_BRG_ACT_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaBrgAct)[1]', 'INT' ), 0)	
	if ( @kCB_BRG_ACT_INT = 1 ) 
		Set @kCB_BRG_ACT = 'S' 
	else
		Set @kCB_BRG_ACT = 'N'

	SET @kCB_BRG_TIP = coalesce(@Cargo.value('(//REGISTRO/@PersonaBgrTip)[1]', 'INT' ), 0) -- as Status
	--SET @kCB_BRG_ROL as Booleano
	SET	@kCB_BRG_ROL_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaBrgRol)[1]', 'INT' ), 0)	
	if ( @kCB_BRG_ROL_INT = 1 ) 
		Set @kCB_BRG_ROL = 'S' 
	else
		Set @kCB_BRG_ROL = 'N'

	--SET @kCB_BRG_JEF as Booleano
	SET	@kCB_BRG_JEF_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaBrgJef)[1]', 'INT' ), 0)	
	if ( @kCB_BRG_JEF_INT = 1 ) 
		Set @kCB_BRG_JEF = 'S' 
	else
		Set @kCB_BRG_JEF = 'N'

	--SET @kCB_BRG_CON as Booleano
	SET	@kCB_BRG_CON_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaBrgCon)[1]', 'INT' ), 0)	
	if ( @kCB_BRG_CON_INT = 1 ) 
		Set @kCB_BRG_CON = 'S' 
	else
		Set @kCB_BRG_CON = 'N'
	--SET @kCB_BRG_PRA as Booleano
	SET	@kCB_BRG_PRA_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaBrgPra)[1]', 'INT' ), 0)	
	if ( @kCB_BRG_PRA_INT = 1 ) 
		Set @kCB_BRG_PRA = 'S' 
	else
		Set @kCB_BRG_PRA = 'N'

	SET @kCB_BRG_NOP = coalesce( @Cargo.value('(//REGISTRO/@PersonaBgrNop)[1]', 'Varchar(6)' ), '' ) -- as Descripcion
	SET @kCB_BANCO = coalesce( @Cargo.value('(//REGISTRO/@PersonaBanco)[1]', 'Varchar(6)' ), @kCB_BANCO ) -- as Codigo
	
	
    if ( @ErrorID = 0 ) 
    begin 

	
	-- Si ya existe el empleado con los mismos nombres, rfc, fecha de Persona y correo  define el mismo numero de empleado 
	-- para que la validacion la realice el WFTress.dll 

	set @kCB_SALARIO_BRUTO = 0 
    if ( (@EmpNumero <> 0) and (@IngresoReingreso = 1)) 
    begin 
         set @kCB_CODIGO = @EmpNumero
         set @MOV3 = 'REINGRESO_EMPLEADO' 
    end
    else 
    begin 
        set @MOV3 = 'ALTA_EMPLEADO' 
		set @kCB_CODIGO = 0 

		select @kCB_CODIGO = CB_CODIGO from COLABORA  where CB_NOMBRES = @kCB_NOMBRES and CB_APE_MAT = @kCB_APE_MAT and CB_APE_PAT = @kCB_APE_PAT 
													and CB_FEC_ING = @kCB_FEC_ING and CB_E_MAIL  = @kCB_E_MAIL  and CB_RFC     = @kCB_RFC

		set @kCB_CODIGO = coalesce (@kCB_CODIGO ,  0 ) 

		-- Se obtiene el numero de empleado mayor + 1 
		if ( @kCB_CODIGO = 0 ) 
			SELECT @kCB_CODIGO = CB_CODIGO + 1  FROM dbo.ULTIMO_EMPLEADO
	 end 

	 set @Xml =(	 

	   select @MOV3 as MOV3, 
			@EMPRESA as EMPRESA,  ( 
	   select * from  ( 
		 SELECT 		
		        @PersonaID  as IngresoID , 
				@kCB_CODIGO  as CB_CODIGO  , 
				'S'  as CB_ACTIVO,		
				@kCB_AUTOSAL as CB_AUTOSAL , --Booleano
				@kCB_APE_MAT as CB_APE_MAT , --Descripcion
				@kCB_APE_PAT as CB_APE_PAT , --Descripcion
				@kCB_NOMBRES as CB_NOMBRES , --Descripcion
				--@kCB_BAN_ELE as CB_BAN_ELE , --Descripcion Se Quita por peticion de Roch
				@kCB_CALLE as CB_CALLE , --Observaciones
				@kCB_CARRERA as CB_CARRERA , --DescLarga
				@kCB_CHECA as CB_CHECA , --Booleano
				@kCB_CIUDAD as CB_CIUDAD , --Descripcion
				@kCB_CLASIFI as CB_CLASIFI , --Codigo
				@kCB_CODPOST as CB_CODPOST , --Referencia
				@kCB_COLONIA as CB_COLONIA , --Descripcion
				@kCB_CONTRAT as CB_CONTRAT , --Codigo1
				@kCB_CREDENC as CB_CREDENC , --Codigo1
				@kCB_CTA_VAL as CB_CTA_VAL , --Descripcion
				@kCB_CURP as CB_CURP , --Descripcion
				@kCB_E_MAIL as CB_E_MAIL , --Formula
				@kCB_ESTADO as CB_ESTADO , --Codigo2
				@kCB_ESTUDIO as CB_ESTUDIO , --Codigo2
				convert( varchar(20), @kCB_FEC_ANT , 112 )  as CB_FEC_ANT , --Fecha
				convert( varchar(20), @kCB_FEC_CON , 112 )  as CB_FEC_CON , --Fecha
				convert( varchar(20), @kCB_FEC_COV , 112 )  as CB_FEC_COV , --Fecha
				convert( varchar(20), @kCB_FEC_ING , 112 )  as CB_FEC_ING , --Fecha
				convert( varchar(20), @kCB_FEC_NAC , 112 )  as CB_FEC_NAC , --Fecha
				@kCB_LUG_NAC as CB_LUG_NAC , --Descripcion
				@kCB_MUNICIP as CB_MUNICIP , --Codigo
				@kCB_NACION as CB_NACION , --Descripcion
				@kCB_NIVEL0 as CB_NIVEL0 , --Codigo
				@kCB_NIVEL1 as CB_NIVEL1 , --Codigo
				@kCB_NIVEL2 as CB_NIVEL2 , --Codigo
				@kCB_NIVEL3 as CB_NIVEL3 , --Codigo
				@kCB_NIVEL4 as CB_NIVEL4 , --Codigo
				@kCB_NIVEL5 as CB_NIVEL5 , --Codigo
				@kCB_NIVEL6 as CB_NIVEL6 , --Codigo
				@kCB_NIVEL7 as CB_NIVEL7 , --Codigo
				@kCB_NIVEL8 as CB_NIVEL8 , --Codigo
				@kCB_NIVEL9 as CB_NIVEL9 , --Codigo
				@kCB_PATRON as CB_PATRON , --RegPatronal
				@kCB_PUESTO as CB_PUESTO , --Codigo
				@kCB_RFC as CB_RFC , --Descripcion
				@kCB_SALARIO as CB_SALARIO , --PesosDiario
				@kCB_SALARIO_BRUTO as CB_SALARIO_BRUTO , --PesosDiario Salario Bruto Mensual
				@kCB_SEGSOC as CB_SEGSOC , --Descripcion
				@kCB_SEXO as CB_SEXO , --Codigo1
				@kCB_TABLASS as CB_TABLASS , --Codigo1
				@kCB_TEL as CB_TEL , --Descripcion
				@kCB_TURNO as CB_TURNO , --Codigo
				@kCB_ZONA_GE as CB_ZONA_GE , --ZonaGeo										
				---Nuevos Campos Mapeados
				@kCB_EDO_CIV as CB_EDO_CIV , -- Codigo1
				convert( varchar(20), @kCB_FEC_RES , 112 )  as CB_FEC_RES , -- Fecha
				@kCB_EST_HOR as CB_EST_HOR , -- Descripcion
				@kCB_EST_HOY as CB_EST_HOY , -- Booleanos
				@kCB_EVALUA as CB_EVALUA , -- DiasFrac
				@kCB_EXPERIE as CB_EXPERIE , -- Titulo
				convert( varchar(20), @kCB_FEC_BAJ , 112 )    as CB_FEC_BAJ , -- Fecha
				convert( varchar(20), @kCB_FEC_BSS , 112 )    as CB_FEC_BSS , -- Fecha
				convert( varchar(20), @kCB_FEC_INT , 112 )    as CB_FEC_INT , -- Fecha
				convert( varchar(20), @kCB_FEC_REV , 112 )    as CB_FEC_REV , -- Fecha
				convert( varchar(20), @kCB_FEC_VAC , 112 )    as CB_FEC_VAC , -- Fecha
				@kCB_G_FEC_1 as CB_G_FEC_1 , -- Fecha
				@kCB_G_FEC_2 as CB_G_FEC_2 , -- Fecha
				@kCB_G_FEC_3 as CB_G_FEC_3 , -- Fecha
				@kCB_G_FEC_4 as CB_G_FEC_4 , -- Fecha
				@kCB_G_FEC_5 as CB_G_FEC_5 , -- Fecha
				@kCB_G_FEC_6 as CB_G_FEC_6 , -- Fecha
				@kCB_G_FEC_7 as CB_G_FEC_7 , -- Fecha
				@kCB_G_FEC_8 as CB_G_FEC_8 , -- Fecha
				@kCB_G_LOG_1 as CB_G_LOG_1 , -- Booleano
				@kCB_G_LOG_2 as CB_G_LOG_2 , -- Booleano
				@kCB_G_LOG_3 as CB_G_LOG_3 , -- Booleano
				@kCB_G_LOG_4 as CB_G_LOG_4 , -- Booleano
				@kCB_G_LOG_5 as CB_G_LOG_5 , -- Booleano
				@kCB_G_LOG_6 as CB_G_LOG_6 , -- Booleano
				@kCB_G_LOG_7 as CB_G_LOG_7 , -- Booleano
				@kCB_G_LOG_8 as CB_G_LOG_8 , -- Booleano
				@kCB_G_NUM_1 as CB_G_NUM_1 , -- Pesos
				@kCB_G_NUM_2 as CB_G_NUM_2 , -- Pesos
				@kCB_G_NUM_3 as CB_G_NUM_3 , -- Pesos
				@kCB_G_NUM_4 as CB_G_NUM_4 , -- Pesos
				@kCB_G_NUM_5 as CB_G_NUM_5 , -- Pesos
				@kCB_G_NUM_6 as CB_G_NUM_6 , -- Pesos
				@kCB_G_NUM_7 as CB_G_NUM_7 , -- Pesos
				@kCB_G_NUM_8 as CB_G_NUM_8 , -- Pesos
				@kCB_G_NUM_9 as CB_G_NUM_9 , -- Pesos
				@kCB_G_NUM10 as CB_G_NUM10 , -- Pesos
				@kCB_G_NUM11 as CB_G_NUM11 , -- Pesos
				@kCB_G_NUM12 as CB_G_NUM12 , -- Pesos
				@kCB_G_NUM13 as CB_G_NUM13 , -- Pesos
				@kCB_G_NUM14 as CB_G_NUM14 , -- Pesos
				@kCB_G_NUM15 as CB_G_NUM15 , -- Pesos
				@kCB_G_NUM16 as CB_G_NUM16 , -- Pesos
				@kCB_G_NUM17 as CB_G_NUM17 , -- Pesos
				@kCB_G_NUM18 as CB_G_NUM18 , -- Pesos
				@kCB_G_TAB_1 as CB_G_TAB_1 , -- Codigo
				@kCB_G_TAB_2 as CB_G_TAB_2 , -- Codigo
				@kCB_G_TAB_3 as CB_G_TAB_3 , -- Codigo
				@kCB_G_TAB_4 as CB_G_TAB_4 , -- Codigo
				@kCB_G_TAB_5 as CB_G_TAB_5 , -- Codigo
				@kCB_G_TAB_6 as CB_G_TAB_6 , -- Codigo
				@kCB_G_TAB_7 as CB_G_TAB_7 , -- Codigo
				@kCB_G_TAB_8 as CB_G_TAB_8 , -- Codigo
				@kCB_G_TAB_9 as CB_G_TAB_9 , -- Codigo
				@kCB_G_TAB10 as CB_G_TAB10 , -- Codigo
				@kCB_G_TAB11 as CB_G_TAB11 , -- Codigo
				@kCB_G_TAB12 as CB_G_TAB12 , -- Codigo
				@kCB_G_TAB13 as CB_G_TAB13 , -- Codigo
				@kCB_G_TAB14 as CB_G_TAB14 , -- Codigo
				@kCB_G_TEX_1 as CB_G_TEX_1 , -- Descripcion
				@kCB_G_TEX_2 as CB_G_TEX_2 , -- Descripcion
				@kCB_G_TEX_3 as CB_G_TEX_3 , -- Descripcion
				@kCB_G_TEX_4 as CB_G_TEX_4 , -- Descripcion
				@kCB_G_TEX_5 as CB_G_TEX_5 , -- Descripcion
				@kCB_G_TEX_6 as CB_G_TEX_6 , -- Descripcion
				@kCB_G_TEX_7 as CB_G_TEX_7 , -- Descripcion
				@kCB_G_TEX_8 as CB_G_TEX_8 , -- Descripcion
				@kCB_G_TEX_9 as CB_G_TEX_9 , -- Descripcion
				@kCB_G_TEX10 as CB_G_TEX10 , -- Descripcion
				@kCB_G_TEX11 as CB_G_TEX11 , -- Descripcion
				@kCB_G_TEX12 as CB_G_TEX12 , -- Descripcion
				@kCB_G_TEX13 as CB_G_TEX13 , -- Descripcion
				@kCB_G_TEX14 as CB_G_TEX14 , -- Descripcion
				@kCB_G_TEX15 as CB_G_TEX15 , -- Descripcion
				@kCB_G_TEX16 as CB_G_TEX16 , -- Descripcion
				@kCB_G_TEX17 as CB_G_TEX17 , -- Descripcion
				@kCB_G_TEX18 as CB_G_TEX18 , -- Descripcion
				@kCB_G_TEX19 as CB_G_TEX19 , -- Descripcion
				@kCB_HABLA as CB_HABLA , -- Booleano
				@kCB_IDIOMA as CB_IDIOMA , -- Descripcion
				@kCB_INFCRED as CB_INFCRED , -- Descripcion
				@kCB_INFMANT as CB_INFMANT , -- Booleano
				@kCB_INFTASA as CB_INFTASA , -- DescINFO
				@kCB_LA_MAT as CB_LA_MAT , -- Descripcion
				@kCB_LAST_EV as CB_LAST_EV , -- Fecha
				@kCB_MAQUINA as CB_MAQUINA , -- Titulo
				@kCB_MED_TRA as CB_MED_TRA , -- Codigo1
				@kCB_PASAPOR as CB_PASAPOR , -- Booleano				
				@kCB_VIVECON as CB_VIVECON , -- Codigo1
				@kCB_VIVEEN as CB_VIVEEN , -- Codigo2
				@kCB_ZONA as CB_ZONA , -- Referencia
				@kCB_TIP_REV as CB_TIP_REV , -- Codigo				
				@kCB_INF_INI as CB_INF_INI , -- Fecha
				@kCB_INF_OLD as CB_INF_OLD , -- DescINFO
				@kCB_OLD_SAL as CB_OLD_SAL , -- PesosDiario
				@kCB_OLD_INT as CB_OLD_INT , -- PesosDiario
				@kCB_PRE_INT as CB_PRE_INT , -- PesosDiario
				@kCB_PER_VAR as CB_PER_VAR , -- PesosDiario
				@kCB_SAL_TOT as CB_SAL_TOT , -- PesosDiario
				@kCB_TOT_GRA as CB_TOT_GRA , -- PesosDiario
				@kCB_FAC_INT as CB_FAC_INT , -- Tasa
				@kCB_RANGO_S as CB_RANGO_S , -- Tasa
				@kCB_CLINICA as CB_CLINICA , -- Codigo3																
				@kCB_CANDIDA as CB_CANDIDA , -- FolioGrande
				@kCB_ID_NUM as CB_ID_NUM , -- Descripcion
				@kCB_ENT_NAC as CB_ENT_NAC , -- Codigo2
				@kCB_COD_COL as CB_COD_COL , -- Codigo								
				@kCB_DER_PV as CB_DER_PV , -- DiasFrac
				@kCB_V_PRIMA as CB_V_PRIMA , -- DiasFrac
				@kCB_SUB_CTA as CB_SUB_CTA , -- Descripcion
				@kCB_NETO as CB_NETO , -- Pesos
				@kCB_NOMINA as CB_NOMINA , -- NominaTipo
				@kCB_RECONTR as CB_RECONTR , -- Booleano
				@kCB_DISCAPA as CB_DISCAPA , -- Booleano
				@kCB_INDIGE as CB_INDIGE , -- Booleano
				@kCB_FONACOT as CB_FONACOT , -- Descripcion
				@kCB_EMPLEO as CB_EMPLEO , -- Booleano								
				@kCB_G_TEX20 as CB_G_TEX20 , -- Descripcion
				@kCB_G_TEX21 as CB_G_TEX21 , -- Descripcion
				@kCB_G_TEX22 as CB_G_TEX22 , -- Descripcion
				@kCB_G_TEX23 as CB_G_TEX23 , -- Descripcion
				@kCB_G_TEX24 as CB_G_TEX24 , -- Descripcion
				@kCB_INFDISM as CB_INFDISM , -- Booleano
				@kCB_INFACT as CB_INFACT , -- Booleano
				@kCB_NUM_EXT as CB_NUM_EXT , -- NombreCampo
				@kCB_NUM_INT as CB_NUM_INT , -- NombreCampo
				@kCB_INF_ANT as CB_INF_ANT , -- Fecha
				@kCB_TDISCAP as CB_TDISCAP , -- Status
				@kCB_ESCUELA as CB_ESCUELA , -- DescLarga
				@kCB_TESCUEL as CB_TESCUEL , -- Status
				@kCB_TITULO as CB_TITULO , -- Status
				@kCB_YTITULO as CB_YTITULO , -- Anio
				@kCB_CTA_GAS as CB_CTA_GAS , -- Descripcion
				@kCB_ID_BIO as CB_ID_BIO , -- FolioGrande
				@kCB_GP_COD as CB_GP_COD , -- Codigo				
				@kCB_TSANGRE as CB_TSANGRE , -- Descripcion
				@kCB_ALERGIA as CB_ALERGIA , -- Formula
				@kCB_BRG_ACT as CB_BRG_ACT , -- Booleano
				@kCB_BRG_TIP as CB_BRG_TIP , -- Status
				@kCB_BRG_ROL as CB_BRG_ROL , -- Booleano
				@kCB_BRG_JEF as CB_BRG_JEF , -- Booleano
				@kCB_BRG_CON as CB_BRG_CON , -- Booleano
				@kCB_BRG_PRA as CB_BRG_PRA , -- Booleano
				@kCB_BRG_NOP as CB_BRG_NOP , -- Descripcion
				@kCB_BANCO as CB_BANCO ,	-- Codigo 
				@kCB_REGIMEN as CB_REGIMEN -- Status

		   )  REGISTRO    FOR XML AUTO, TYPE 
		   )   REGISTROS  FOR XML PATH('DATOS'), TYPE  
		   )  
		 	
		SET @ErrorID = 0 
		set @Error = '' 
	end
	
	-- Solo pruebas
	UPDATE T_Wftransacciones set TransCargoWFTress = @Xml where TransID = @transID 

	return @ErrorID
end
GO

