/* #17111: Cancelar n�mina timbrada con status Afectada Parcial. 1/2 */
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'[AFECTAR_STATUS_TIMBRADO]') AND type IN ( N'P', N'PC' ))
    DROP PROCEDURE AFECTAR_STATUS_TIMBRADO
go
 
/* #17111: Cancelar n�mina timbrada con status Afectada Parcial. 2/2 */
CREATE procedure AFECTAR_STATUS_TIMBRADO( @PE_YEAR Anio, @PE_TIPO NominaTipo, @PE_NUMERO NominaNumero, @StatusActual status, @StatusNuevo status )   
as  
begin  
 set nocount on   
 declare @Total int   
 declare @TotalAfectados int   
 declare @Timbrados int   
 declare @StatusTimbradoPendiente status    
 declare @StatusTimbradoParcial status    
 declare @StatusTimbradoTotal status    
 declare @NominaAfectada status   
 declare @NominaAfectadaParcial status      
   
 set @StatusTimbradoPendiente = 0   
 set @StatusTimbradoParcial = 1   
 set @StatusTimbradoTotal = 2   
 set @NominaAfectada = 6   
 set @NominaAfectadaParcial = 5
   
 
 select @Total = count(*) from NOMINA where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  
 select @TotalAfectados = count(*) from NOMINA where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and NO_STATUS = @NominaAfectada   
   
 if @Total > 0   
 begin     
      select @Timbrados = count(*) from NOMINA where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and  NO_STATUS = @NominaAfectada  and NO_TIMBRO = @StatusTimbradoTotal  

      if ( @TotalAfectados > 0 ) 
      begin 
          if ( @StatusNuevo = @StatusTimbradoTotal )    
          begin  
                if ( @Total = @Timbrados )  
                    update PERIODO set PE_TIMBRO = @StatusTimbradoTotal  where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and  PE_STATUS >= @NominaAfectadaParcial   
               else  
                    update PERIODO set PE_TIMBRO = @StatusTimbradoParcial where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and  PE_STATUS >= @NominaAfectadaParcial      
          end  
          else  
          begin  
               if ( @Timbrados = 0 )   
                    update PERIODO set PE_TIMBRO = @StatusTimbradoPendiente where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and PE_STATUS >= @NominaAfectadaParcial   
               else  
                    update PERIODO set PE_TIMBRO = @StatusTimbradoParcial where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and  PE_STATUS >= @NominaAfectadaParcial      
          end;   
      end
      else
      begin
             update PERIODO set PE_TIMBRO = @StatusTimbradoPendiente where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and PE_STATUS >= @NominaAfectadaParcial   
      end; 
end;  
    
end

GO

/* #17358: Usuario podr� identificar el status de una nomina Afectada Parcial y Timbrada Parcial en Sistema TRESS y Timbrado de Nomina. 1/2 */
if exists (select * from sysobjects where id = object_id(N'Periodo_GetStatusTimbrado') and xtype in (N'FN', N'IF', N'TF')) 
	drop function Periodo_GetStatusTimbrado
go

/* #17358: Usuario podr� identificar el status de una nomina Afectada Parcial y Timbrada Parcial en Sistema TRESS y Timbrado de Nomina. 1/2 */
CREATE FUNCTION Periodo_GetStatusTimbrado(@PE_YEAR int,  @PE_TIPO int, @PE_NUMERO int,  @CM_NIVEL0 Formula = '' ) 
RETURNS TABLE 
AS RETURN
( 
select PE_YEAR, PE_TIPO, PE_NUMERO, PE_STATUS, PE_TIMBRO,  coalesce( [0], 0)  as Pendientes,  coalesce([2], 0) as Timbradas  from 
  (
  select 
    PE.PE_YEAR,
    PE.PE_TIPO,
    PE.PE_NUMERO,
    PE.PE_STATUS, 
    PE.PE_TIMBRO,
    NO_TIMBRO, 
    COUNT(*) Empleados
  from PERIODO PE 
  left outer join NOMINA NO on PE.PE_YEAR = NO.PE_YEAR and PE.PE_TIPO = NO.PE_TIPO and PE.PE_NUMERO = NO.PE_NUMERO   
  where 
  PE.PE_YEAR = @PE_YEAR and PE.PE_TIPO = @PE_TIPO and PE.PE_NUMERO = @PE_NUMERO 
  and (  @CM_NIVEL0 = ''  or ( CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB_NIVEL0='') )
  group by PE.PE_YEAR, PE.PE_TIPO, PE.PE_NUMERO,PE.PE_STATUS, PE.PE_TIMBRO, NO_TIMBRO 
  ) Timbs 
pivot 
(
    SUM(Empleados) 
    for NO_TIMBRO in ( [0], [1], [2]  ) 
)  pvt 

)

GO

/* SUA */
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id=object_id('VALOR_UMA') AND name='UM_FDESC '    )
     alter table VALOR_UMA add UM_FDESC PesosDiario NULL 
go    

if ( select COUNT(*) from VALOR_UMA where UM_FEC_INI = '2018-01-01' ) = 0 
begin 
	insert into VALOR_UMA (UM_FEC_INI, UM_VALOR, UM_FDESC) values ( '2018-01-01', 75.49, 78.43 ) 
end 

go 
UPDATE VALOR_UMA SET UM_FDESC = case 
					when UM_FEC_INI >= '2018-01-01' then 78.43 
					else 0.00 
				end
where UM_FDESC is null 
go 

alter table VALOR_UMA alter column UM_FDESC PesosDiario NOT NULL 
go

