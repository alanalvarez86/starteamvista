/* Vista VCED_FON, consultar el nuevo campo de Mes en Per�odo (VMOV_FON): 1/2 */
IF exists( select * from sys.objects where object_id = object_id('VMOV_FON') )
    drop view VMOV_FON
GO


create view VMOV_FON 
as
select M.PE_YEAR, P.PE_MES_FON,  NO.CB_CODIGO, M.MO_REFEREN, ( M.MO_PERCEPC + M.MO_DEDUCCI ) MF_MONTO, P.PE_NUMERO, P.PE_FEC_PAG, P.PE_MES, P.PE_POS_MES  from MOVIMIEN M 
      join NOMINA NO on 
              ( NO.PE_YEAR = M.PE_YEAR ) and
               ( NO.PE_TIPO = M.PE_TIPO ) and
               ( NO.PE_NUMERO = M.PE_NUMERO ) and 
               ( NO.CB_CODIGO  = M.CB_CODIGO ) 
      join PERIODO P on
               ( P.PE_YEAR = NO.PE_YEAR ) and
               ( P.PE_TIPO = NO.PE_TIPO ) and
               ( P.PE_NUMERO = NO.PE_NUMERO )
      where ( M.MO_ACTIVO = 'S' ) and ( NO.NO_STATUS >= 6 ) and M.CO_NUMERO = dbo.FN_GetConceptoFonacot()

GO 

if exists (select * from sysobjects where id = object_id(N'MovFon_GetPeriodoInicial') and xtype in (N'FN', N'IF', N'TF')) 
	drop function MovFon_GetPeriodoInicial
go

create function MovFon_GetPeriodoInicial(@PE_YEAR int, @PE_MES_FON int, @CB_CODIGO NumeroEmpleado, @MO_REFEREN Referencia ) 
returns int 
as
begin 

	declare @NumeroPeriodo int 


	select TOP 1 @NumeroPeriodo = PE_NUMERO from VMOV_FON where PE_YEAR = @PE_YEAR and  PE_MES_FON = @PE_MES_FON	 and CB_CODIGO = @CB_CODIGO and MO_REFEREN = @MO_REFEREN 
	order by PE_FEC_PAG 

	set @NumeroPeriodo = coalesce( @NumeroPeriodo, 0 ) 

	return @NumeroPeriodo 
	
end

GO 


if exists (select * from sysobjects where id = object_id(N'MovFon_GetPeriodoFinal') and xtype in (N'FN', N'IF', N'TF')) 
	drop function MovFon_GetPeriodoFinal
go

create function MovFon_GetPeriodoFinal(@PE_YEAR int, @PE_MES_FON int, @CB_CODIGO NumeroEmpleado, @MO_REFEREN Referencia ) 
returns int 
as
begin 

	declare @NumeroPeriodo int 


	select TOP 1 @NumeroPeriodo = PE_NUMERO from VMOV_FON where PE_YEAR = @PE_YEAR and  PE_MES_FON = @PE_MES_FON	 and CB_CODIGO = @CB_CODIGO and MO_REFEREN = @MO_REFEREN 
	order by PE_FEC_PAG desc 

	set @NumeroPeriodo = coalesce( @NumeroPeriodo, 0 ) 

	return @NumeroPeriodo 
	
end  


GO 


if exists (select * from sysobjects where id = object_id(N'CedFon_GetStatus') and xtype in (N'FN', N'IF', N'TF')) 
	drop function CedFon_GetStatus
go

create function CedFon_GetStatus(@PE_YEAR int, @PE_MES_FON int, @CB_CODIGO NumeroEmpleado, @MO_REFEREN Referencia, @FT_STATUS Status ) 
returns int 
as
begin 

	declare @NumeroPeriodo int 
	declare @StatusPeriodo int 
	declare @TipoNomina	   int 
	
	set @FT_STATUS = coalesce( @FT_STATUS, 0 ) 
	
	-- Si el pago fonacot no se ha calculado total , entonces la cedula esta abierta
	if ( @FT_STATUS < 2 ) 
		return 1 

	-- Si el pago fonacot esta cerrado, entonces tambien la c�dula 
	if ( @FT_STATUS >= 3 ) 
		return 2 
			
	-- Si el usuario no acostumbra a cerrar la nomina entonces debemos buscar en los periodos del mes fonacot del tipo de nomina del empleado 	
	select @TipoNomina = CB_NOMINA from COLABORA where CB_CODIGO = @CB_CODIGO 

	
	-- Si hay al menos una nomina sin afectar entonces arroja que todavia hay pendientes
	if ( select COUNT(*) from PERIODO where PE_MES_FON = @PE_MES_FON  and PE_YEAR = @PE_YEAR  and PE_TIPO = @TipoNomina and PE_STATUS < 6  ) = 0  
		return 1
	else
		return 2
		


	return 0 
	
end  


GO 


/* Vista VCED_FON, consultar el nuevo campo de Mes en Per�odo (PF_MES_FON): 1/2 */
IF exists( select * from sys.objects where object_id = object_id('VCED_FON') )
    drop view VCED_FON
GO

/* Vista VCED_FON, consultar el nuevo campo de Mes en Per�odo (PF_MES_FON): 2/2 */
CREATE view VCED_FON(
    CB_CODIGO,
    PR_TIPO,
    PR_REFEREN,
    PF_YEAR,
    PF_MES,
    PF_PAGO,
    PF_PLAZO,
    PF_PAGADAS,
    PF_CAPTURA,
	PF_REUBICA,
    US_CODIGO,
    PF_NOMINA, 
    PF_NOM_QTY, 
	PF_STATUS,
	PERIODO_INICIAL, 
	PERIODO_FINAL )
as
select
    PF.CB_CODIGO,
    PF.PR_TIPO,
    PF.PR_REFEREN,
    PF_YEAR,
    PF_MES,
    PF_PAGO,
    PF_PLAZO,
    PF_PAGADAS,
    PF_CAPTURA,
	PF_REUBICA,
    US_CODIGO,
    coalesce( Afectadas.PF_Monto, 0.00 ) , 
    coalesce( Afectadas.PF_Cuantos, 0 ) , 
	coalesce(dbo.CedFon_GetStatus(PE_YEAR, PE_MES_FON, PF.CB_CODIGO, MO_REFEREN, FT.FT_STATUS ), 0), 
	coalesce(dbo.MovFon_GetPeriodoInicial(PE_YEAR, PE_MES_FON, PF.CB_CODIGO, MO_REFEREN) , 0 ) , 
	coalesce( dbo.MovFon_GetPeriodoFinal(PE_YEAR, PE_MES_FON, PF.CB_CODIGO, MO_REFEREN)  , 0 )  
from PCED_FON PF
left outer join 
(
    select M.PE_YEAR, PE_MES_FON,  CB_CODIGO, M.MO_REFEREN, coalesce( SUM( MF_MONTO ) , 0.00 )  PF_Monto, count(*) PF_Cuantos from 
	VMOV_FON M
    group by  PE_YEAR, PE_MES_FON,  CB_CODIGO, MO_REFEREN
) Afectadas on Afectadas.PE_YEAR = PF_YEAR and Afectadas.PE_MES_FON = PF.PF_MES and  Afectadas.CB_CODIGO = PF.CB_CODIGO and Afectadas.MO_REFEREN = PF.PR_REFEREN 
LEFT OUTER JOIN FON_TOT FT on FT.FT_YEAR = PF.PF_YEAR and FT.FT_MONTH = PF.PF_MES 

GO

if exists (select * from sysobjects where id = object_id(N'PrestamoFonacot_PuedeCancelar') and xtype in (N'FN', N'IF', N'TF')) 
	drop function PrestamoFonacot_PuedeCancelar 
go

create function PrestamoFonacot_PuedeCancelar(@PF_YEAR int, @PF_MES Status, @CB_CODIGO NumeroEmpleado, @PR_TIPO Codigo1, @PR_REFEREN Referencia ) 
returns Booleano 
as
begin 

	declare @PF_STATUS Status     
	declare @PF_YEAR_Anterior int 
	declare @PF_MES_Anterior int 

	if ( @PF_MES = 1 ) 
	begin 
		set @PF_YEAR_Anterior = @PF_YEAR - 1 
		set @PF_MES_Anterior = 12
	end 
	else
	begin
		set @PF_YEAR_Anterior = @PF_YEAR 
		set @PF_MES_Anterior = @PF_MES - 1 
	end 	
	

	if ( select COUNT(*) from PCED_FON where CB_CODIGO = @CB_CODIGO and PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN and PF_YEAR >= @PF_YEAR and PF_MES > @PF_MES ) > 0 
		return 'N' 

	if ( select COUNT(*) from PCED_FON where CB_CODIGO = @CB_CODIGO and PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN and PF_YEAR >= @PF_YEAR+1 and PF_MES > 0  ) > 0 
		return 'N' 


	select  @PF_STATUS = PF_STATUS from VCED_FON where CB_CODIGO = @CB_CODIGO and PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN and PF_YEAR = @PF_YEAR_Anterior and PF_MES = @PF_MES_Anterior

	set @PF_STATUS = coalesce( @PF_STATUS, 0 ) 

	if ( @PF_STATUS in (0,2) ) 
		return 'S' 
	
	return 'N' 

end 

Go 


/* Vista VCED_DET, consultar el detalle de pago de cedula : 1/2 */
IF exists( select * from sys.objects where object_id = object_id('VCED_DET') )
    drop view VCED_DET
GO

/* Vista VCED_DET, consultar el detalle de pago de cedula : 2/2 */
CREATE view VCED_DET
as
select 
PF_YEAR, PF_MES, CB_FONACOT, CB_RFC, CB_CODIGO, PRETTYNAME, PR_REFEREN, PF_PAGO, PF_PLAZO, PF_PAGADAS, FC_NOMINA, FC_AJUSTE, 
PF_PAGO - (FC_NOMINA + FC_AJUSTE )  as DIFERENCIA, 
FE_INCIDE, 
case FE_INCIDE 
	when '0' then '' 
	when 'B' then 'Baja IMSS' 
	when 'I' then 'Incapacidad' 	
	else  'Otro' 
end FE_IN_DESC, 
FECHA_INI_BAJA as FE_FECHA1, FECHA_FIN as FE_FECHA2, PF_REUBICA, RS_CODIGO 
from ( 

SELECT F.PF_YEAR, F.PF_MES, C.CB_FONACOT, C.CB_RFC, coalesce(UPPER (C.CB_APE_PAT + ' ' + C.CB_APE_MAT +' ' + C.CB_NOMBRES), '')  AS PRETTYNAME, F.PR_REFEREN, F.PF_PAGO, coalesce(C.CB_CODIGO,0) CB_CODIGO, F.PF_PLAZO, F.PF_PAGADAS,
	coalesce(FC.FC_NOMINA,0.00) FC_NOMINA,  coalesce(FC.FC_AJUSTE,0.00) FC_AJUSTE ,
				coalesce(case			
					when  C.CB_ACTIVO = 'N'  then 'B' 		 
					 when FE.FE_INCIDE = 'I' and CT.CT_DIF_QTY < CT.CT_CRE_QTY  then  '0' 	
					 when FE.FE_INCIDE is null or FE.FE_INCIDE = ''  then '0' 								  
					 else FE.FE_INCIDE   
				end, '')   FE_INCIDE , 	
				coalesce(case 
					when C.CB_ACTIVO = 'N'  then  CONVERT (VARCHAR(10), C.CB_FEC_BAJ, 103)  				  					
					when CONVERT (VARCHAR(10), FE.FE_FECHA1, 103) = '30/12/1899'  then '' 
					when FE.FE_INCIDE = 'I' and CT.CT_DIF_QTY < CT.CT_CRE_QTY  then  '' 						
					else CONVERT (VARCHAR(10), FE.FE_FECHA1, 103) 
				end , '' ) AS FECHA_INI_BAJA, 
				coalesce( case 
					when CONVERT (VARCHAR(10), FE.FE_FECHA2, 103) = '30/12/1899' then '' 
					when FE.FE_INCIDE = 'I' and CT.CT_DIF_QTY < CT.CT_CRE_QTY then  '' 					  					
					else CONVERT (VARCHAR(10), FE.FE_FECHA2, 103) 
				end, ''  ) AS FECHA_FIN, 
			     F.PF_REUBICA, 
				 coalesce(RP.RS_CODIGO, '' ) RS_CODIGO
	FROM PCED_FON F
		LEFT JOIN COLABORA C ON F.CB_CODIGO = C.CB_CODIGO
		LEFT JOIN FON_EMP FE ON FE.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = FE.FT_YEAR AND F.PF_MES = FE.FT_MONTH
		LEFT JOIN FON_CRE FC ON FC.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = FC.FT_YEAR AND F.PF_MES = FC.FT_MONTH AND F.PR_REFEREN = FC.PR_REFEREN
		LEFT JOIN VCEDTFON CT  ON CT.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = CT.PF_YEAR AND F.PF_MES = CT.PF_MES		
		LEFT JOIN RPATRON RP on CB_PATRON = RP.TB_CODIGO 
	
	) Cedula 


GO 

if exists (select * from sysobjects where id = object_id(N'FONACOT_CEDULA') and xtype in (N'FN', N'IF', N'TF')) 
	drop function FONACOT_CEDULA
go

create function FONACOT_CEDULA
(	@YEAR SMALLINT,
    @MES SMALLINT,
	@RAZON_SOCIAL CHAR(6), 
	@IgnorarIncapacidades Booleano)
RETURNS @CEDULA TABLE
	(
    NO_FONACOT varchar(30),
    RFC varchar(30),
	NOMBRE VARCHAR(250),
	NO_CREDITO varchar (8),
	RETENCION_MENSUAL numeric(15,2),
	CLAVE_EMPLEADO int,
	PLAZO smallint,
	MESES smallint,
	RETENCION_REAL numeric (15,2),
	INCIDENCIA char(1)	,
	FECHA_INI_BAJA varchar(10),
	FECHA_FIN varchar(10),
	REUBICADO CHAR(1)
	)
AS
BEGIN
/** TODO: Basarse en la vista VCED_DET para poder tener la logica en una sola vista **/
	insert into @CEDULA
	( NO_FONACOT, RFC, NOMBRE, NO_CREDITO, RETENCION_MENSUAL, CLAVE_EMPLEADO, PLAZO, MESES, RETENCION_REAL,
		INCIDENCIA, FECHA_INI_BAJA, FECHA_FIN, REUBICADO)	

		SELECT C.CB_FONACOT, C.CB_RFC, coalesce(UPPER (C.CB_APE_PAT + ' ' + C.CB_APE_MAT +' ' + C.CB_NOMBRES), '')  AS PRETTYNAME, F.PR_REFEREN, F.PF_PAGO, coalesce(C.CB_CODIGO,0), F.PF_PLAZO, F.PF_PAGADAS,
		coalesce(FC.FC_NOMINA,0.00) + coalesce(FC.FC_AJUSTE,0.00),
				coalesce(case			
					when  C.CB_ACTIVO = 'N'  then 'B' 		 
					 when @IgnorarIncapacidades = 'S' and FE.FE_INCIDE = 'I' and CT.CT_DIF_QTY < CT.CT_CRE_QTY  then  '0' 	
					 when FE.FE_INCIDE is null or FE.FE_INCIDE = ''  then '0' 								  
					 else FE.FE_INCIDE   
				end, '')   FE_INCIDE , 	
				coalesce(case 
					when C.CB_ACTIVO = 'N'  then  CONVERT (VARCHAR(10), C.CB_FEC_BAJ, 103)  				  					
					when CONVERT (VARCHAR(10), FE.FE_FECHA1, 103) = '30/12/1899'  then '' 
					when @IgnorarIncapacidades = 'S' and FE.FE_INCIDE = 'I' and CT.CT_DIF_QTY < CT.CT_CRE_QTY  then  '' 						
					else CONVERT (VARCHAR(10), FE.FE_FECHA1, 103) 
				end , '' ) AS FECHA_INI_BAJA, 
				coalesce( case 
					when CONVERT (VARCHAR(10), FE.FE_FECHA2, 103) = '30/12/1899' then '' 
					when @IgnorarIncapacidades = 'S' and FE.FE_INCIDE = 'I' and CT.CT_DIF_QTY < CT.CT_CRE_QTY then  '' 					  					
					else CONVERT (VARCHAR(10), FE.FE_FECHA2, 103) 
				end, ''  ) AS FECHA_FIN, 
			     F.PF_REUBICA
	FROM PCED_FON F
		LEFT JOIN COLABORA C ON F.CB_CODIGO = C.CB_CODIGO
		LEFT JOIN FON_EMP FE ON FE.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = FE.FT_YEAR AND F.PF_MES = FE.FT_MONTH
		LEFT JOIN FON_CRE FC ON FC.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = FC.FT_YEAR AND F.PF_MES = FC.FT_MONTH AND F.PR_REFEREN = FC.PR_REFEREN
		LEFT JOIN VCEDTFON CT  ON CT.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = CT.PF_YEAR AND F.PF_MES = CT.PF_MES
		
	WHERE 
		F.PF_YEAR = @YEAR
		AND F.PF_MES = @MES
		AND C.CB_PATRON IN (SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RAZON_SOCIAL or @RAZON_SOCIAL = '')

	return
END

GO 

/* #16982  1/4 */
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_FONACOT_AGREGAR_UN_PRESTAMO') AND type IN ( N'P', N'PC' ))
    DROP PROCEDURE SP_FONACOT_AGREGAR_UN_PRESTAMO
go
 
/* #16982  2/4 */
CREATE PROCEDURE SP_FONACOT_AGREGAR_UN_PRESTAMO (@CB_CODIGO NumeroEmpleado, @PR_TIPO Codigo1, @PR_REFEREN Referencia, @PR_FECHA Fecha, @PR_STATUS status, @PR_OBSERVA ReferenciaLarga,  @US_CODIGO Usuario, @PF_PAGO Pesos) 
as
begin 
	set nocount on 
	declare @Fecha_Inicio Fecha 
	declare @PR_SALDO_DEF Pesos
	
	set @PR_SALDO_DEF = 9999999.0
	
	SELECT @Fecha_Inicio = PE_FEC_INI FROM PERIODO P INNER JOIN	FN_FONACOT_PERIODOS (year( @PR_FECHA), month( @PR_FECHA ), (SELECT CB_NOMINA FROM COLABORA WHERE CB_CODIGO = @CB_CODIGO)) FP
		ON P.PE_NUMERO = FP.PE_NUMERO
		where
			PE_YEAR = year( @PR_FECHA) and PE_MES_FON = month( @PR_FECHA ) 
			AND PE_TIPO = (SELECT CB_NOMINA FROM COLABORA WHERE CB_CODIGO = @CB_CODIGO)
			AND PE_POS_FON = 1


	set @Fecha_Inicio = coalesce( @Fecha_Inicio, @PR_FECHA ) 


	if ( select count(*) from PRESTAMO where CB_CODIGO = @CB_CODIGO and PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN ) = 0
	begin 
		INSERT INTO PRESTAMO (CB_CODIGO, PR_TIPO, PR_REFEREN, PR_FECHA, PR_STATUS, PR_OBSERVA, US_CODIGO, PR_MONTO, PR_TOTAL, PR_SALDO, PR_PAG_PER, PR_FON_MOT) 
		VALUES ( @CB_CODIGO, @PR_TIPO, @PR_REFEREN, @Fecha_Inicio, @PR_STATUS, @PR_OBSERVA, @US_CODIGO, @PR_SALDO_DEF, @PR_SALDO_DEF, @PR_SALDO_DEF , @PF_PAGO, 0 )
	end

end

GO 


/* #16982 3/4 */
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_FONACOT_ACTUALIZAR_UN_PRESTAMO') AND type IN ( N'P', N'PC' ))
    DROP PROCEDURE SP_FONACOT_ACTUALIZAR_UN_PRESTAMO
go

/* #16982 4/4 */
CREATE PROCEDURE SP_FONACOT_ACTUALIZAR_UN_PRESTAMO (@CB_CODIGO NumeroEmpleado, @PR_REFEREN Referencia, @PR_TIPO Codigo1,  @iPR_STATUS status, @iUS_CODIGO Usuario, @PR_PAG_PER Pesos, @lGrabarBitacora Booleano = 'S' ) 
as
begin 
    set nocount on 
    DECLARE @BI_TEXTO Observaciones
    DECLARE @BI_DATA NVARCHAR(MAX)
    DECLARE @BI_FEC_MOV Fecha
    DECLARE @PR_MONTO Pesos 
	declare @PR_SALDO_DEF Pesos	
    declare @PR_FECHA Fecha 

	set @PR_SALDO_DEF = 9999999.00

	select @PR_FECHA = PR_FECHA from PRESTAMO where CB_CODIGO = @CB_CODIGO and PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN 
	
	declare @year int 
	declare @mes int 

	select top 1  @year = PF_YEAR , @mes = PF_MES  from PCED_FON where CB_CODIGO = @CB_CODIGO and PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN 
	order by PF_YEAR, PF_MES 

	declare @Fecha_Inicio Fecha 

	SELECT @Fecha_Inicio = PE_FEC_INI FROM PERIODO P INNER JOIN	FN_FONACOT_PERIODOS (@Year, @Mes, (SELECT CB_NOMINA FROM COLABORA WHERE CB_CODIGO = @CB_CODIGO)) FP
		ON P.PE_NUMERO = FP.PE_NUMERO
		where
			PE_YEAR = @Year and PE_MES_FON = @Mes 
			AND PE_TIPO = (SELECT CB_NOMINA FROM COLABORA WHERE CB_CODIGO = @CB_CODIGO)
			AND PE_POS_FON = 1

	set @Fecha_Inicio = coalesce( @Fecha_Inicio, @PR_FECHA ) 
 
	set @PR_MONTO = @PR_SALDO_DEF
 
    update PRESTAMO set PR_FECHA = @Fecha_Inicio, PR_STATUS = @iPR_STATUS,  US_CODIGO= @iUS_CODIGO ,  PR_MONTO = @PR_MONTO, PR_TOTAL = @PR_MONTO, PR_SALDO = @PR_MONTO, PR_PAG_PER = @PR_PAG_PER , 
        PR_FON_MOT =  case                                       
                    when @iPR_STATUS = 1 
					then 3 
                    else
						0 
					end                                               
    where CB_CODIGO = @CB_CODIGO and PR_REFEREN = @PR_REFEREN and PR_TIPO = @PR_TIPO                     

    if ( @lGrabarBitacora  = 'S' ) 
    begin 
            SET @BI_TEXTO =  'Modificaci�n desde C�dula Fonacot: Ref:' + @PR_REFEREN;
            SET @BI_DATA = 'Pr�stamo: Empl.:' + cast (@CB_CODIGO as varchar) + ' Tipo:' + @PR_TIPO + ' Ref:' + @PR_REFEREN + CHAR(13)+CHAR(10) + 'PR_STATUS' + CHAR(13)+CHAR(10) + ' A : ' + cast (@iPR_STATUS as varchar)
    
            SELECT @BI_FEC_MOV = GETDATE();
            exec SP_INSERTAR_BITACORA @iUS_CODIGO, 0, 0, 0, @BI_TEXTO, @CB_CODIGO, @BI_DATA, 62, @BI_FEC_MOV;
    end;
end
GO


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_FONACOT_CANCELAR_PRESTAMOS') AND type IN ( N'P', N'PC' ))
    DROP PROCEDURE SP_FONACOT_CANCELAR_PRESTAMOS
GO

CREATE procedure SP_FONACOT_CANCELAR_PRESTAMOS (@YEAR SMALLINT, @MES SMALLINT, @RAZON_SOCIAL CHAR(6), @US_CODIGO SMALLINT, @PROC_ID INTEGER, @BI_NUMERO INTEGER) 
as
begin
    set nocount on 
    DECLARE @PR_TIPO Codigo1;
    DECLARE @Prestamos TABLE
    (
        i int identity(1,1),
        iCB_CODIGO int, 
        sPR_REFEREN varchar(8) 
    )
    SELECT @PR_TIPO = GL_FORMULA FROM GLOBAL WHERE GL_CODIGO = 211
    insert into @Prestamos ( iCB_CODIGO , sPR_REFEREN ) 
        select CB.CB_CODIGO, PR_REFEREN  from PRESTAMO PR
		join COLABORA CB on PR.CB_CODIGO = CB.CB_CODIGO 
		 where PR_TIPO = @PR_TIPO and PR_STATUS in (0) and CB.CB_PATRON in ( select TB_CODIGO from RPATRON where RS_CODIGO = @RAZON_SOCIAL  or @RAZON_SOCIAL = '' ) 
    declare @i int = 1 
    declare @n int 
    declare @iCB_CODIGO NumeroEmpleado 
    declare @sPR_REFEREN Referencia 
    select @n = count(*) from @Prestamos 
    while @i <= @n 
    begin          select @iCB_CODIGO = iCB_CODIGO , @sPR_REFEREN = sPR_REFEREN from @Prestamos where i = @i 
        
		-- @PF_YEAR int, @PF_MES Status, @CB_CODIGO NumeroEmpleado, @PR_TIPO Codigo1, @PR_REFEREN Referencia
		if ( dbo.PrestamoFonacot_PuedeCancelar( @YEAR, @MES, @iCB_CODIGO, @PR_TIPO,  @sPR_REFEREN) = 'S' ) 
			exec SP_FONACOT_ACTUALIZAR_UN_PRESTAMO @iCB_CODIGO, @sPR_REFEREN, @PR_TIPO, 1, @US_CODIGO, 0
                
        set @i = @i + 1 
    end 
end
GO 



IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_FONACOT_ELIMINAR_CEDULA') AND type IN ( N'P', N'PC' ))
    DROP PROCEDURE SP_FONACOT_ELIMINAR_CEDULA
GO

CREATE procedure SP_FONACOT_ELIMINAR_CEDULA (@YEAR SMALLINT, @MES SMALLINT, @RAZON_SOCIAL CHAR(6), @US_CODIGO SMALLINT, @PROC_ID INTEGER, @BI_NUMERO INTEGER) 
as
begin
	 set nocount on
	 DECLARE @NOMINAS_AFECTADAS INTEGER;
	 DECLARE @BI_TEXTO Observaciones
	 DECLARE @BI_DATA NVARCHAR(MAX)
	 DECLARE @BI_FEC_MOV Fecha
	 DECLARE @PR_TIPO Codigo1
	 DECLARE @CONTADOR_ELIMINADO INTEGER
	 DECLARE @EMPLEADO int;
	 DECLARE @REFERENCIA VARCHAR(8)


	 select @PR_TIPO = LEFT( LTRIM(GL_FORMULA) , 1)  from GLOBAL where GL_CODIGO = 211 
	 set @PR_TIPO = COALESCE( @PR_TIPO, '' ) 

	 SELECT @NOMINAS_AFECTADAS = dbo.FN_FONACOT_CONTEO_CEDULA_AFECTADA(@YEAR, @MES, @RAZON_SOCIAL)  
	 
	 if @NOMINAS_AFECTADAS = 0
	 begin
		  SELECT @BI_FEC_MOV = GETDATE();
		  		  
		  Declare Temporal CURSOR for
 		  SELECT PR.CB_CODIGO, PR.PR_REFEREN from  PRESTAMO PR JOIN COLABORA C ON PR.CB_CODIGO = C.CB_CODIGO
			WHERE PR.CB_CODIGO IN (SELECT CB_CODIGO FROM PCED_FON WHERE PF_YEAR = @YEAR AND PF_MES =@MES AND  PR_TIPO = @PR_TIPO) AND PR.PR_TIPO = @PR_TIPO
			AND C.CB_PATRON IN (SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RAZON_SOCIAL or @RAZON_SOCIAL = '') 

		  OPEN TEMPORAL
		     fetch NEXT FROM Temporal 
	         into  @EMPLEADO, @REFERENCIA
		  WHILE @@FETCH_STATUS = 0 
		  BEGIN
				if ( dbo.PrestamoFonacot_PuedeCancelar( @YEAR, @MES, @EMPLEADO, @PR_TIPO, @REFERENCIA) = 'S' ) 
				begin 
					UPDATE PRESTAMO SET PR_STATUS = 1, PR_FON_MOT = 4 WHERE CB_CODIGO = @EMPLEADO AND PR_REFEREN = @REFERENCIA AND PR_TIPO = @PR_TIPO 
					SET @BI_TEXTO = 'Se cancel� pr�stamo Fonacot con referencia '  + @REFERENCIA; 
					exec SP_INSERTAR_BITACORA @US_CODIGO, @PROC_ID, 0, @BI_NUMERO, @BI_TEXTO, @EMPLEADO, '', 62, @BI_FEC_MOV;
				end; 

				fetch NEXT FROM Temporal 
 					into  @EMPLEADO, @REFERENCIA
  		   END;

   	       close Temporal
    	   deallocate Temporal

		   DELETE PF FROM PCED_FON PF JOIN COLABORA C ON PF.CB_CODIGO = C.CB_CODIGO
		  WHERE PF.PF_YEAR = @YEAR AND PF.PF_MES = @MES AND C.CB_PATRON IN (SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RAZON_SOCIAL or @RAZON_SOCIAL = '') 
			   AND PF.PR_TIPO = @PR_TIPO; 

		  SELECT @CONTADOR_ELIMINADO = @@ROWCOUNT; 
		  
		  if @CONTADOR_ELIMINADO = 0 
			 SET @BI_TEXTO = 'No se eliminaron registros de c�dula Fonacot.';
		  else
			  SET @BI_TEXTO = 'Se eliminaron: ' + cast (@CONTADOR_ELIMINADO as varchar) + ' registro(s) de c�dula Fonacot.'; 		  
		  
		  exec SP_INSERTAR_BITACORA @US_CODIGO, @PROC_ID, 0, @BI_NUMERO, @BI_TEXTO, 0, '', 62, @BI_FEC_MOV;
		
	 end;
end

GO 

/* #17092  1/2 */
if exists (select * from dbo.sysobjects where id = object_id(N'FN_CALCULA_FON') and xtype in (N'FN', N'IF', N'TF')) 
       drop function FN_CALCULA_FON
go 

/* #17092: Tener un mecanismo para reservar el pago de cr�ditos Fonacot cuando las Vacaciones
	se pagan por adelantado para as� saldarlos en la siguiente nomina o al calcular pago Fonacot 2/2 */
CREATE FUNCTION FN_CALCULA_FON
	(@CB_CODIGO NumeroEmpleado, @PR_REFEREN Referencia, @PR_TIPO Codigo1, @TipoAjuste NumeroEmpleado, @AjusteMesAnterior Booleano,
	@AnioPeriodo Anio, @MesPeriodoFon Mes, @NumPeriodo NominaNumero, @TipoPeriodo NominaTipo)
RETURNS @Valores TABLE
	(
	   RETENCION_PERIODO Decimal(15,2), 
	   MONTO_CEDULA   Decimal(15,2), 
	   MONTO_RETENIDO Decimal(15,2), 
       MONTO_ESPERADO Decimal(15,2), 
       AJUSTE Decimal(15,2), 
       MONTO_CONCEPTO Decimal(15,2) ,
       DEUDA_MA Decimal(15,2) ,
	   NUM_PERIODO_MES int,
	   POS_PERIODO_MES int,
	   MES_ANTERIOR    int, 
	   YEAR_ANTERIOR   int 
	)
as
begin 
		declare @RETENCION_PERIODO Pesos 
		declare @MONTO_CEDULA   Pesos 
		declare @MONTO_RETENIDO Pesos 
		declare @MONTO_ESPERADO Pesos 
		declare @AJUSTE Pesos 
		declare @MONTO_CONCEPTO Pesos 
		declare @DEUDA_MA Pesos 
		DECLARE @NUM_PERIODO_MES Empleados
		DECLARE @POS_PERIODO_MES Empleados
		DECLARE @MES_ANTERIOR Status
		DECLARE @YEAR_ANTERIOR Anio
	   	   
		SET @AJUSTE = 0;		

		-- OBTENER N�MERO DE PERIODO Y LA CANTIDAD DE PERIODOS DEL MES.	   		
		SELECT @POS_PERIODO_MES = PE_POS_FON FROM FN_FONACOT_PERIODOS (@AnioPeriodo, @MesPeriodoFon, @TipoPeriodo)
		WHERE PE_NUMERO = @NumPeriodo
		-- Cantidad de periodos Fonacot.
		SELECT @NUM_PERIODO_MES = COUNT(PE_NUMERO) FROM PERIODO
		WHERE PE_YEAR = @AnioPeriodo AND PE_MES_FON = @MesPeriodoFon AND PE_TIPO = @TipoPeriodo

		SELECT @MONTO_CEDULA = PF_PAGO,  @MONTO_RETENIDO = PF_NOMINA FROM VCED_FON WHERE CB_CODIGO = @CB_CODIGO and PF_YEAR = @AnioPeriodo AND PF_MES = @MesPeriodoFon AND PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN
		set @MONTO_RETENIDO = coalesce( @MONTO_RETENIDO, 0.00 ) 


		if (@MesPeriodoFon = 1 ) 
		begin 
			set @MES_ANTERIOR = 12 
			set @YEAR_ANTERIOR = @AnioPeriodo - 1; 
		end
		else
		begin 
			set @MES_ANTERIOR = @MesPeriodoFon - 1 ; 
			set @YEAR_ANTERIOR = @AnioPeriodo; 
		end 
		

		-- SI EL TIPO DE AJUSTE ES  1 (DURANTE EL MES, ES DECIR, SUAVIZADO) � 2 (AJUSTAR CADA PERIODO) Y SE PIDE AJUSTAR EL MES ANTERIOR,
		-- OBTENER AJUSTE DEL MES ANTERIOR.
		-- IF (@TipoAjuste = 1 OR @TipoAjuste = 2) AND (@AjusteMesAnterior = 'S')
		IF @AjusteMesAnterior = 'S'
		BEGIN
			SELECT @DEUDA_MA = (PF_PAGO-PF_NOMINA) FROM VCED_FON
				WHERE CB_CODIGO = @CB_CODIGO and PF_YEAR = @YEAR_ANTERIOR AND PF_MES = @MES_ANTERIOR  AND PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN 
			
			SET @DEUDA_MA = COALESCE (@DEUDA_MA, 0.000);
		END
		ELSE
			SET @DEUDA_MA = 0.000;
	    
		-- AJUSTE.
		-- 1. SUAVIZADO, ES DECIR, DURANTE EL MES.
		-- 2. AJUSTA CADA PER�ODO.
		-- 3. AJUSTA AL FINAL DE MES (�LTIMO PER�ODO).
		-- IF (@TipoAjuste = 1) OR (@TipoAjuste = 2)
	   
		-- OBTENER LA RETENCI�N POR PERIODO.
		IF @NUM_PERIODO_MES != 0
		BEGIN
			SELECT @RETENCION_PERIODO = ((PF_PAGO+@DEUDA_MA)/@NUM_PERIODO_MES) FROM PCED_FON
				WHERE CB_CODIGO = @CB_CODIGO and PF_YEAR = @AnioPeriodo AND PF_MES = @MesPeriodoFon AND PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN ;
			SET @RETENCION_PERIODO = coalesce( @RETENCION_PERIODO , 0.000 );
		END
		ELSE
		BEGIN
			SET @RETENCION_PERIODO = 0.0;
		END
		
		-- OBTENER MONTO ESPERADO.
		IF (@TipoAjuste = 1 OR @TipoAjuste = 2) OR (@TipoAjuste = 3 AND @POS_PERIODO_MES = @NUM_PERIODO_MES)
		BEGIN
			SET @MONTO_ESPERADO = (@POS_PERIODO_MES - 1)*@RETENCION_PERIODO;			
			-- SET @MONTO_ESPERADO = ((@POS_PERIODO_MES - 1)*@RETENCION_PERIODO) + @MONTO_PAJUSTE_CARGO + @MONTO_PAJUSTE_ABONO;

			-- OBTENER EL AJUSTE,
			-- ES DECIR, LA DIFERENCIA ENTRE LO QUE DEBER�A LLEVAR DESCONTADO HASTA EL MOMENTO (@MONTO_ESPERADO)
			-- Y LO QUE EN REALIDAD LLEVA (VCED_FON.PF_NOMINA)	

			-- AJUSTE SUAVIZADO, ES DECIR, DURANTE LOS PER�ODOS QUE RESTAN EN EL MES.
			IF (@TipoAjuste = 1)
			BEGIN
				SET @AJUSTE = (@MONTO_ESPERADO - @MONTO_RETENIDO ) / ((@NUM_PERIODO_MES-@POS_PERIODO_MES)+1)
			END
			-- AJUSTE CADA PER�ODO O AL FINAL DEL MES.
			ELSE IF (@TipoAjuste = 2 OR @TipoAjuste = 3)
			BEGIN
				SET @AJUSTE = (@MONTO_ESPERADO - @MONTO_RETENIDO ) 
			END

			SET @AJUSTE = COALESCE (@AJUSTE, 0.000);
		END
		ELSE
		BEGIN
			SET @MONTO_ESPERADO = 0.000;
			SET @AJUSTE = 0.000;
		END	   
	   
		SET @MONTO_CONCEPTO = @RETENCION_PERIODO + @AJUSTE		 

		IF @POS_PERIODO_MES = @NUM_PERIODO_MES
		BEGIN
			IF ABS (@MONTO_CEDULA - (@MONTO_RETENIDO + @MONTO_CONCEPTO)) < 1
			BEGIN
				SET @MONTO_CONCEPTO = @MONTO_CONCEPTO + (@MONTO_CEDULA - (@MONTO_RETENIDO + @MONTO_CONCEPTO))				
			END
		END

		insert into @Valores (
				RETENCION_PERIODO, 
				MONTO_CEDULA, 
				MONTO_RETENIDO, 
				MONTO_ESPERADO, 
				AJUSTE, 
				MONTO_CONCEPTO ,
				DEUDA_MA, 
				NUM_PERIODO_MES,
				POS_PERIODO_MES,
				MES_ANTERIOR ,  
				YEAR_ANTERIOR
		) values ( 
				@RETENCION_PERIODO, 
				@MONTO_CEDULA, 
				@MONTO_RETENIDO, 
				@MONTO_ESPERADO, 
				@AJUSTE, 
				@MONTO_CONCEPTO ,
				@DEUDA_MA, 
				@NUM_PERIODO_MES,
				@POS_PERIODO_MES,
				@MES_ANTERIOR ,  
				@YEAR_ANTERIOR
		)

		RETURN 
end
GO

/* #16906: 1/2 */
if exists (select * from dbo.sysobjects where id = object_id(N'Fon_Diferencias_Totales') and xtype in (N'FN', N'IF', N'TF')) 
	DROP FUNCTION Fon_Diferencias_Totales
go 

/* #16906 2/2 */
CREATE FUNCTION Fon_Diferencias_Totales( @Year int, @Mes int, @Patron char(1)) 
RETURNS TABLE 
AS RETURN
(
select  Coalesce ([Baja IMSS], 0) as BajaIMSS, Coalesce (Incapacidad, 0) as Incapacidad, Coalesce (Otro, 0) as Otro, coalesce (Total, 0) Total
from
(
  SELECT FE_IN_DESC as columnname, SUM (DIFERENCIA) AS value
      FROM VCED_DET WHERE PF_YEAR = @Year AND PF_MES = @Mes AND RS_CODIGO =
	  (SELECT RS_CODIGO FROM RPATRON WHERE TB_CODIGO = @Patron)	
	  AND FE_IN_DESC <> '' AND FE_IN_DESC <> 'Otro'
	  GROUP BY FE_IN_DESC
UNION
  SELECT 'Otro' as columnname, SUM (DIFERENCIA) AS value
      FROM VCED_DET WHERE PF_YEAR = @Year AND PF_MES = @Mes AND RS_CODIGO =
	  (SELECT RS_CODIGO FROM RPATRON WHERE TB_CODIGO = @Patron)	
	  AND (FE_INCIDE = '0' OR FE_INCIDE = '')
UNION
  SELECT 'Total' as columnname, SUM (DIFERENCIA) AS value
      FROM VCED_DET WHERE PF_YEAR = @Year AND PF_MES = @Mes AND RS_CODIGO =
	  (SELECT RS_CODIGO FROM RPATRON WHERE TB_CODIGO = @Patron)	
  ) d
pivot
(
  max(value)
  for columnname in ([Baja IMSS], Incapacidad, Otro, Total)
) piv
)
GO 

IF exists( select * from sys.objects where object_id = object_id('VHUELLAGTI') )
    drop view VHUELLAGTI
GO

CREATE VIEW VHUELLAGTI
AS
--> Siempre traer todas las huellas, no filtrar por IV_CODIGO.
SELECT COALESCE (HU_ID, 0) AS HU_ID,
	COALESCE (H.CM_CODIGO, '') AS CM_CODIGO,
	H.CB_CODIGO,
	COALESCE (HU_INDICE, 0) AS HU_INDICE,
	COALESCE (H.IV_CODIGO, 0) AS IV_CODIGO,
	COUNT(HU_ID) AS HU_CNT
FROM #COMPARTE.DBO.HUELLAGTI AS H, #COMPARTE.DBO.EMP_BIO AS BIO, COLABORA AS C

WHERE H.CB_CODIGO = BIO.CB_CODIGO
		AND H.CM_CODIGO = BIO.CM_CODIGO
		AND H.IV_CODIGO = BIO.IV_CODIGO
		AND H.HU_INDICE < 11

GROUP BY HU_ID, H.CM_CODIGO, H.CB_CODIGO, HU_INDICE, H.IV_CODIGO
GO


IF exists( select * from sys.objects where object_id = object_id('VFACIALGTI') )
    drop view VFACIALGTI
GO

CREATE VIEW VFACIALGTI
AS
SELECT COALESCE (HU_ID, 0) AS HU_ID,
	COALESCE (H.CM_CODIGO, '') AS CM_CODIGO,
	H.CB_CODIGO,
	COALESCE (HU_INDICE, 0) AS HU_INDICE,
	COALESCE (H.IV_CODIGO, 0) AS IV_CODIGO,
	COUNT(HU_ID) AS HU_CNT
FROM #COMPARTE.DBO.HUELLAGTI AS H, #COMPARTE.DBO.EMP_BIO AS BIO, COLABORA AS C

WHERE H.CB_CODIGO = BIO.CB_CODIGO
		AND H.CM_CODIGO = BIO.CM_CODIGO
		AND H.IV_CODIGO = BIO.IV_CODIGO
		AND H.HU_INDICE = 11

GROUP BY HU_ID, H.CM_CODIGO, H.CB_CODIGO, HU_INDICE, H.IV_CODIGO
GO
