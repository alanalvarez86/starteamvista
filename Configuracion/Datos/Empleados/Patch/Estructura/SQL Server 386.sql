/* Tabla ACCREGLA: Regla para Control de Acceso */
/* Patch 386 # Seq: Agregado */
CREATE TABLE ACCREGLA (
       AE_CODIGO            FolioChico,  /* C�digo De La Regla */
       AE_TIPO              Status,      /* Tipo ( eAccesoRegla = ( arInformativo, arEvalEntrada, arEvalSalida, arEvaluacion ) ) */
       AE_LETRERO           DescLarga,   /* INF: T�tulo EVAL: Mensaje */
       AE_FORMULA           Formula,     /* F�rmula ( INF: Regresa Texto EVAL: Regresa Booleano ) */
       AE_QUERY             Condicion,   /* Condici�n -> QUERYS.QU_CODIGO */
       AE_SEVERI            Status,      /* Severidad ( Usar eTipoBitacora ) */
       AE_ACTIVO            Booleano,    /* Activo / Inactivo */
       US_CODIGO            Usuario      /* Ultimo Usuario En Modificar Regla */
)
GO

/* PK ACCREGLA: Llave Primaria de la Tabla ACCREGLA */
/* Patch 386 # Seq: Agregado */
ALTER TABLE ACCREGLA ADD CONSTRAINT PK_ACCREGLA PRIMARY KEY (AE_CODIGO)

/* Tabla ACCESLOG: Bit�cora De Entrada / Salida En Caseta */
/* Patch 386 # Seq: Agregado */
CREATE TABLE ACCESLOG (
       CB_CODIGO            NumeroEmpleado, /* Empleado */
       AL_FECHA             Fecha,          /* Fecha */
       AL_HORA              Hora,           /* Hora */
       AL_ENTRADA           Booleano,       /* Si es Entrada */
       AL_OK_SIST           Booleano,       /* OK Del Sistema */
       AL_OK_MAN            Booleano,       /* OK Del Guardia */
       AL_OK_FEC            Fecha,          /* Fecha Del OK Del Guardia */
       AL_OK_HOR            Hora,           /* Hora Del OK Del Guardia */
       AL_COMENT            Comentario,     /* Comentarios */
       AE_CODIGO            Codigo,         /* Regla Que Rechaza */
       AL_CASETA            Codigo,         /* Caseta Usada */
       US_CODIGO            Usuario         /* Usuario */
)
GO

/* PK ACCESLOG: Llave Primaria de la Tabla ACCESLOG */
/* Patch 386 # Seq: Agregado */
ALTER TABLE ACCESLOG ADD CONSTRAINT PK_ACCESLOG PRIMARY KEY (CB_CODIGO, AL_FECHA, AL_HORA)
GO

/*****************************************/
/**** Foreign Keys ***********************/
/*****************************************/

ALTER TABLE ACCESLOG
       ADD CONSTRAINT FK_ACCESLOG_COLABORA
       FOREIGN KEY (CB_CODIGO)
       REFERENCES COLABORA
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

/*****************************************/
/****  Triggers  *************************/
/*****************************************/

/* Trigger sobre ACCREGLA Agregado */
/* Patch 386 # Seq: Agregado */
CREATE TRIGGER TU_ACCREGLA ON ACCREGLA AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( AE_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;
	select @NewCodigo = AE_CODIGO from Inserted;
        select @OldCodigo = AE_CODIGO from Deleted;
        update ACCESLOG set AE_CODIGO = @NewCodigo where ( AE_CODIGO = @OldCodigo );
  END
END
GO

/* Trigger sobre QUERYS modificado */
/* Patch 386 # Seq: Modificado */
ALTER TRIGGER TU_QUERYS ON QUERYS AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( QU_CODIGO )
  BEGIN
	declare @NewCodigo Condicion, @OldCodigo Condicion;

	select @NewCodigo = QU_CODIGO from   Inserted;
        select @OldCodigo = QU_CODIGO from   Deleted;

	IF ( @OldCodigo <> '' ) AND ( @NewCodigo <> '' )
	BEGIN
		UPDATE CONCEPTO SET CO_QUERY  = @NewCodigo WHERE CO_QUERY = @OldCodigo;
		UPDATE EVENTO   SET EV_QUERY  = @NewCodigo WHERE EV_QUERY = @OldCodigo;
		UPDATE REPORTE  SET QU_CODIGO = @NewCodigo WHERE QU_CODIGO = @OldCodigo;
		UPDATE FOLIO    SET QU_CODIGO = @NewCodigo WHERE QU_CODIGO = @OldCodigo;
		UPDATE ACCREGLA SET QU_CODIGO = @NewCodigo WHERE QU_CODIGO = @OldCodigo;
	END
  END
END
GO

/**************************************************/
/* STORED PROCEDURES ******************************/
/**************************************************/

update GLOBAL set GL_FORMULA = '386' where ( GL_CODIGO = 133 )
GO





