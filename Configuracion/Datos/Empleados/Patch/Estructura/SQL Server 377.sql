/*****************************/
/* Creaci�n de campos nuevos */
/*****************************/

/* Creaci�n de la Tabla CAUSACCI */
/* Patch 377 # Seq: 1 Agregado */
CREATE TABLE CAUSACCI (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_TIPO              Status
)
GO

/* Llave Primaria de la Tabla CAUSACCI */
/* Patch 377 # Seq: 2 Agregado */
ALTER TABLE CAUSACCI
       ADD CONSTRAINT PK_CAUSACCI PRIMARY KEY (TB_CODIGO)
GO

/* Creaci�n de la Tabla MOTACCI */
/* Patch 377 # Seq: 3 Agregado */
CREATE TABLE MOTACCI (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_TIPO              Status
)
GO

/* Llave Primaria de la Tabla MOTACCI */
/* Patch 377 # Seq: 4 Agregado */
ALTER TABLE MOTACCI
       ADD CONSTRAINT PK_MOTACCI PRIMARY KEY (TB_CODIGO)
GO

/* Creaci�n de la Tabla TACCIDEN */
/* Patch 377 # Seq: 5 Agregado */
CREATE TABLE TACCIDEN (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_TIPO              Status
)
GO

/* Llave Primaria de la Tabla TACCIDEN */
/* Patch 377 # Seq: 6 Agregado */
ALTER TABLE TACCIDEN
       ADD CONSTRAINT PK_TACCIDEN PRIMARY KEY (TB_CODIGO)
GO

/* Creaci�n de la Tabla EXPEDIEN */
/* Patch 377 # Seq: 7 Agregado */
CREATE TABLE EXPEDIEN (
       EX_CODIGO  NumeroEmpleado,
       EX_TIPO    Status,
       EX_NOMBRES Descripcion,
       EX_APE_PAT Descripcion,
       EX_APE_MAT Descripcion,
       EX_SEXO    Codigo1,
       EX_FEC_NAC Fecha,
       EX_CALLE   Descripcion,
       EX_COLONIA Descripcion,
       EX_CIUDAD  Descripcion,
       EX_CODPOST Referencia,
       EX_ESTADO  Codigo2,
       EX_TEL     Descripcion,
       EX_FEC_INI Fecha,
       EX_EMB_INI Fecha,
       EX_EMB_FIN Fecha,
       EX_OBSERVA Memo,
       CB_CODIGO  NumeroEmpleado,
       US_CODIGO  Usuario,

       EX_G_TEX01 Observaciones,
       EX_G_TEX02 Observaciones,
       EX_G_TEX03 Observaciones,
       EX_G_TEX04 Observaciones,
       EX_G_TEX05 Observaciones,
       EX_G_TEX06 Observaciones,
       EX_G_TEX07 Observaciones,
       EX_G_TEX08 Observaciones,
       EX_G_TEX09 Observaciones,
       EX_G_TEX10 Observaciones,
       EX_G_TEX11 Observaciones,
       EX_G_TEX12 Observaciones,
       EX_G_TEX13 Observaciones,
       EX_G_TEX14 Observaciones,
       EX_G_TEX15 Observaciones,
       EX_G_TEX16 Observaciones,
       EX_G_TEX17 Observaciones,
       EX_G_TEX18 Observaciones,
       EX_G_TEX19 Observaciones,
       EX_G_TEX20 Observaciones,
       EX_G_TEX21 Observaciones,
       EX_G_TEX22 Observaciones,
       EX_G_TEX23 Observaciones,
       EX_G_TEX24 Observaciones,
       EX_G_TEX25 Observaciones,
       EX_G_TEX26 Observaciones,
       EX_G_TEX27 Observaciones,
       EX_G_TEX28 Observaciones,
       EX_G_TEX29 Observaciones,
       EX_G_TEX30 Observaciones,

       EX_G_LOG01 Booleano,
       EX_G_LOG02 Booleano,
       EX_G_LOG03 Booleano,
       EX_G_LOG04 Booleano,
       EX_G_LOG05 Booleano,
       EX_G_LOG06 Booleano,
       EX_G_LOG07 Booleano,
       EX_G_LOG08 Booleano,
       EX_G_LOG09 Booleano,
       EX_G_LOG10 Booleano,
       EX_G_LOG11 Booleano,
       EX_G_LOG12 Booleano,
       EX_G_LOG13 Booleano,
       EX_G_LOG14 Booleano,
       EX_G_LOG15 Booleano,
       EX_G_LOG16 Booleano,
       EX_G_LOG17 Booleano,
       EX_G_LOG18 Booleano,
       EX_G_LOG19 Booleano,
       EX_G_LOG20 Booleano,
       EX_G_LOG21 Booleano,
       EX_G_LOG22 Booleano,
       EX_G_LOG23 Booleano,
       EX_G_LOG24 Booleano,
       EX_G_LOG25 Booleano,
       EX_G_LOG26 Booleano,
       EX_G_LOG27 Booleano,
       EX_G_LOG28 Booleano,
       EX_G_LOG29 Booleano,
       EX_G_LOG30 Booleano,
       EX_G_LOG31 Booleano,
       EX_G_LOG32 Booleano,
       EX_G_LOG33 Booleano,
       EX_G_LOG34 Booleano,
       EX_G_LOG35 Booleano,
       EX_G_LOG36 Booleano,
       EX_G_LOG37 Booleano,
       EX_G_LOG38 Booleano,
       EX_G_LOG39 Booleano,
       EX_G_LOG40 Booleano,
       EX_G_LOG41 Booleano,
       EX_G_LOG42 Booleano,
       EX_G_LOG43 Booleano,
       EX_G_LOG44 Booleano,
       EX_G_LOG45 Booleano,
       EX_G_LOG46 Booleano,
       EX_G_LOG47 Booleano,
       EX_G_LOG48 Booleano,
       EX_G_LOG49 Booleano,
       EX_G_LOG50 Booleano,
       EX_G_LOG51 Booleano,
       EX_G_LOG52 Booleano,
       EX_G_LOG53 Booleano,
       EX_G_LOG54 Booleano,
       EX_G_LOG55 Booleano,
       EX_G_LOG56 Booleano,
       EX_G_LOG57 Booleano,
       EX_G_LOG58 Booleano,
       EX_G_LOG59 Booleano,
       EX_G_LOG60 Booleano,

       EX_G_NUM01 Pesos,
       EX_G_NUM02 Pesos,
       EX_G_NUM03 Pesos,
       EX_G_NUM04 Pesos,
       EX_G_NUM05 Pesos,
       EX_G_NUM06 Pesos,
       EX_G_NUM07 Pesos,
       EX_G_NUM08 Pesos,
       EX_G_NUM09 Pesos,
       EX_G_NUM10 Pesos,
       EX_G_NUM11 Pesos,
       EX_G_NUM12 Pesos,
       EX_G_NUM13 Pesos,
       EX_G_NUM14 Pesos,
       EX_G_NUM15 Pesos,
       EX_G_NUM16 Pesos,
       EX_G_NUM17 Pesos,
       EX_G_NUM18 Pesos,
       EX_G_NUM19 Pesos,
       EX_G_NUM20 Pesos,
       EX_G_NUM21 Pesos,
       EX_G_NUM22 Pesos,
       EX_G_NUM23 Pesos,
       EX_G_NUM24 Pesos,
       EX_G_NUM25 Pesos,
       EX_G_NUM26 Pesos,
       EX_G_NUM27 Pesos,
       EX_G_NUM28 Pesos,
       EX_G_NUM29 Pesos,
       EX_G_NUM30 Pesos,

       EX_G_BOL01 Booleano,
       EX_G_BOL02 Booleano,
       EX_G_BOL03 Booleano,
       EX_G_BOL04 Booleano,
       EX_G_BOL05 Booleano,
       EX_G_BOL06 Booleano,
       EX_G_BOL07 Booleano,
       EX_G_BOL08 Booleano,
       EX_G_BOL09 Booleano,
       EX_G_BOL10 Booleano,
       EX_G_BOL11 Booleano,
       EX_G_BOL12 Booleano,
       EX_G_BOL13 Booleano,
       EX_G_BOL14 Booleano,
       EX_G_BOL15 Booleano,
       EX_G_BOL16 Booleano,
       EX_G_BOL17 Booleano,
       EX_G_BOL18 Booleano,
       EX_G_BOL19 Booleano,
       EX_G_BOL20 Booleano,
       EX_G_BOL21 Booleano,
       EX_G_BOL22 Booleano,
       EX_G_BOL23 Booleano,
       EX_G_BOL24 Booleano,
       EX_G_BOL25 Booleano,
       EX_G_BOL26 Booleano,
       EX_G_BOL27 Booleano,
       EX_G_BOL28 Booleano,
       EX_G_BOL29 Booleano,
       EX_G_BOL30 Booleano,
       EX_G_BOL31 Booleano,
       EX_G_BOL32 Booleano,
       EX_G_BOL33 Booleano,
       EX_G_BOL34 Booleano,
       EX_G_BOL35 Booleano,
       EX_G_BOL36 Booleano,
       EX_G_BOL37 Booleano,
       EX_G_BOL38 Booleano,
       EX_G_BOL39 Booleano,
       EX_G_BOL40 Booleano,
       EX_G_BOL41 Booleano,
       EX_G_BOL42 Booleano,
       EX_G_BOL43 Booleano,
       EX_G_BOL44 Booleano,
       EX_G_BOL45 Booleano,
       EX_G_BOL46 Booleano,
       EX_G_BOL47 Booleano,
       EX_G_BOL48 Booleano,
       EX_G_BOL49 Booleano,
       EX_G_BOL50 Booleano,
       EX_G_BOL51 Booleano,
       EX_G_BOL52 Booleano,
       EX_G_BOL53 Booleano,
       EX_G_BOL54 Booleano,
       EX_G_BOL55 Booleano,
       EX_G_BOL56 Booleano,
       EX_G_BOL57 Booleano,
       EX_G_BOL58 Booleano,
       EX_G_BOL59 Booleano,
       EX_G_BOL60 Booleano,

       EX_G_DES01 Observaciones,
       EX_G_DES02 Observaciones,
       EX_G_DES03 Observaciones,
       EX_G_DES04 Observaciones,
       EX_G_DES05 Observaciones,
       EX_G_DES06 Observaciones,
       EX_G_DES07 Observaciones,
       EX_G_DES08 Observaciones,
       EX_G_DES09 Observaciones,
       EX_G_DES10 Observaciones,
       EX_G_DES11 Observaciones,
       EX_G_DES12 Observaciones,
       EX_G_DES13 Observaciones,
       EX_G_DES14 Observaciones,
       EX_G_DES15 Observaciones,
       EX_G_DES16 Observaciones,
       EX_G_DES17 Observaciones,
       EX_G_DES18 Observaciones,
       EX_G_DES19 Observaciones,
       EX_G_DES20 Observaciones,
       EX_G_DES21 Observaciones,
       EX_G_DES22 Observaciones,
       EX_G_DES23 Observaciones,
       EX_G_DES24 Observaciones,
       EX_G_DES25 Observaciones,
       EX_G_DES26 Observaciones,
       EX_G_DES27 Observaciones,
       EX_G_DES28 Observaciones,
       EX_G_DES29 Observaciones,
       EX_G_DES30 Observaciones,
       EX_G_DES31 Observaciones,
       EX_G_DES32 Observaciones,
       EX_G_DES33 Observaciones,
       EX_G_DES34 Observaciones,
       EX_G_DES35 Observaciones,
       EX_G_DES36 Observaciones,
       EX_G_DES37 Observaciones,
       EX_G_DES38 Observaciones,
       EX_G_DES39 Observaciones,
       EX_G_DES40 Observaciones,
       EX_G_DES41 Observaciones,
       EX_G_DES42 Observaciones,
       EX_G_DES43 Observaciones,
       EX_G_DES44 Observaciones,
       EX_G_DES45 Observaciones,
       EX_G_DES46 Observaciones,
       EX_G_DES47 Observaciones,
       EX_G_DES48 Observaciones,
       EX_G_DES49 Observaciones,
       EX_G_DES50 Observaciones,
       EX_G_DES51 Observaciones,
       EX_G_DES52 Observaciones,
       EX_G_DES53 Observaciones,
       EX_G_DES54 Observaciones,
       EX_G_DES55 Observaciones,
       EX_G_DES56 Observaciones,
       EX_G_DES57 Observaciones,
       EX_G_DES58 Observaciones,
       EX_G_DES59 Observaciones,
       EX_G_DES60 Observaciones
)
GO

/* Llave Primaria de la Tabla EXPEDIEN */
/* Patch 377 # Seq: 8 Agregado */
ALTER TABLE EXPEDIEN
       ADD CONSTRAINT PK_EXPEDIEN PRIMARY KEY (EX_CODIGO)
GO

/* Creaci�n de la Tabla ACCIDENT */
/* Patch 377 # Seq: 9 Agregado */
CREATE TABLE ACCIDENT (
       EX_CODIGO            NumeroEmpleado,
       AX_FECHA             Fecha,
       AX_FEC_REG           Fecha,
       AX_CAUSA             Codigo,
       AX_MOTIVO            Codigo,
       AX_HORA              Hora,
       AX_TIP_ACC           Codigo,
       AX_FEC_SUS           Fecha,
       AX_HOR_SUS           Hora,
       AX_PER_REG           Descripcion,
       AX_TIP_LES           Status,
       AX_DESCRIP           Memo,
       AX_ATENDIO           Status,
       AX_OBSERVA           Observaciones,
       AX_NUM_INC           Descripcion,
       US_CODIGO            Usuario,
       AX_TIPO              Status,
       AX_FEC_COM           Fecha,
       AX_HOR_COM           Hora,
       AX_RIESGO            Booleano,
       AX_INCAPA            Booleano,
       AX_DAN_MAT           Booleano,
       AX_INF_ACC           Memo,
       AX_INF_TES           Memo,
       AX_INF_SUP           Memo,
       AX_NUM_TES           NumeroEmpleado,
       AX_NUMERO            Descripcion
)
GO

/* Llave Primaria de la Tabla ACCIDENT */
/* Patch 377 # Seq: 10 Agregado */
ALTER TABLE ACCIDENT
       ADD CONSTRAINT PK_ACCIDENT PRIMARY KEY (EX_CODIGO, AX_FECHA)
GO

/* Llave For�nea de la Tabla ACCIDENT hacia EXPEDIEN */
/* Patch 377 # Seq: 11 Agregado */
ALTER TABLE ACCIDENT
       ADD CONSTRAINT FK_ACCIDENT_EXPEDIEN
       FOREIGN KEY (EX_CODIGO)
       REFERENCES EXPEDIEN
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

/* Creaci�n de la Tabla EMBARAZO */
/* Patch 377 # Seq: 12 Agregado */
CREATE TABLE EMBARAZO (
       EX_CODIGO            NumeroEmpleado,
       EM_FEC_UM            Fecha,
       EM_FEC_PP            Fecha,
       EM_FINAL             Booleano,
       EM_FEC_FIN           Fecha,
       EM_TERMINO           Status,
       EM_NORMAL            Booleano,
       EM_MORTAL            Booleano,
       EM_COMENTA           Observaciones,
       EM_PRENAT            Fecha,
       EM_POSNAT            Fecha,
       EM_RIESGO            Booleano,
       EM_INC_INI           Fecha,
       EM_INC_FIN           Fecha,
       EM_OBS_RIE           Observaciones,
       US_CODIGO            Usuario
)
GO

/* Llave Primaria de la Tabla EMBARAZO */
/* Patch 377 # Seq: 13 Agregado */
ALTER TABLE EMBARAZO
       ADD CONSTRAINT PK_EMBARAZO PRIMARY KEY (EX_CODIGO, EM_FEC_UM)
GO

/* Llave For�nea de la Tabla EMBARAZO hacia EXPEDIEN*/
/* Patch 377 # Seq: 14 Agregado */
ALTER TABLE EMBARAZO
       ADD CONSTRAINT FK_EMBARAZO_EXPEDIEN
       FOREIGN KEY (EX_CODIGO)
       REFERENCES EXPEDIEN
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

/* Creaci�n de la Tabla MEDICINA */
/* Patch 377 # Seq: 15 Agregado */
CREATE TABLE MEDICINA (
       ME_CODIGO            Codigo,
       ME_NOMBRE            Descripcion,
       ME_TIPO              Status,
       ME_INGLES            Descripcion,
       ME_NUMERO            Pesos,
       ME_TEXTO             Descripcion,
       ME_MEDIDA            Referencia,
       ME_DESCRIP           Observaciones
)
GO

/* Llave Primaria de la Tabla MEDICINA */
/* Patch 377 # Seq: 16 Agregado */
ALTER TABLE MEDICINA
       ADD CONSTRAINT PK_MEDICINA PRIMARY KEY (ME_CODIGO)
GO

/* Creaci�n de la Tabla MED_ENTR */
/* Patch 377 # Seq: 17 Agregado */
CREATE TABLE MED_ENTR (
       ME_CODIGO            Codigo,
       EX_CODIGO            NumeroEmpleado,
       MT_FECHA             Fecha,
       MT_CANTIDA           Pesos,
       MT_COMENTA           Observaciones,
       US_CODIGO            Usuario
)
GO

/* Llave Primaria de la Tabla MED_ENTR */
/* Patch 377 # Seq: 18 Agregado */
ALTER TABLE MED_ENTR
       ADD CONSTRAINT PK_MED_ENTR PRIMARY KEY (ME_CODIGO, MT_FECHA, EX_CODIGO)
GO

/* Llave For�nea de la Tabla MED_ENTR hacia EXPEDIEN */
/* Patch 377 # Seq: 19 Agregado */
ALTER TABLE MED_ENTR
       ADD CONSTRAINT FK_MED_ENTR_EXPEDIEN
       FOREIGN KEY (EX_CODIGO)
       REFERENCES EXPEDIEN
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

/* Llave For�nea de la Tabla MED_ENTR hacia MEDICINA */
/* Patch 377 # Seq: 20 Agregado */
ALTER TABLE MED_ENTR
       ADD CONSTRAINT FK_MED_ENTR_MEDICINA
       FOREIGN KEY (ME_CODIGO)
       REFERENCES MEDICINA
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

/* Creaci�n de la Tabla TESTUDIO */
/* Patch 377 # Seq: 21 Agregado */
CREATE TABLE TESTUDIO (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_TIPO              Status
)
GO

/* Llave Primaria de la Tabla TESTUDIO */
/* Patch 377 # Seq: 22 Agregado */
ALTER TABLE TESTUDIO
       ADD CONSTRAINT PK_TESTUDIO PRIMARY KEY (TB_CODIGO)
GO

/* Creaci�n de la Tabla DIAGNOST */
/* Patch 377 # Seq: 23 Agregado */
CREATE TABLE DIAGNOST (
       DA_CODIGO            Codigo,
       DA_NOMBRE            Descripcion,
       DA_TIPO              Status,
       DA_INGLES            Descripcion,
       DA_NUMERO            Pesos,
       DA_TEXTO             Descripcion
)
GO

/* Llave Primaria de la Tabla DIAGNOST */
/* Patch 377 # Seq: 24 Agregado */
ALTER TABLE DIAGNOST
       ADD CONSTRAINT PK_DIAGNOST PRIMARY KEY (DA_CODIGO)
GO

/* Creaci�n de la Tabla TCONSLTA */
/* Patch 377 # Seq: 25 Agregado */
CREATE TABLE TCONSLTA (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_TIPO              Status
)
GO

/* Llave Primaria de la Tabla TCONSLTA */
/* Patch 377 # Seq: 26 Agregado */
ALTER TABLE TCONSLTA
       ADD CONSTRAINT PK_TCONSLTA PRIMARY KEY (TB_CODIGO)
GO

/* Creaci�n de la Tabla CONSULTA */
/* Patch 377 # Seq: 27 Agregado */
CREATE TABLE CONSULTA (
       EX_CODIGO            NumeroEmpleado,
       CN_FECHA             Fecha,
       CN_HOR_INI           Hora,
       DA_CODIGO            Codigo,
       CN_TIPO              Codigo,
       CN_MOTIVO            Observaciones,
       CN_SINTOMA           Memo,
       CN_EXPLORA           Memo,
       CN_OBSERVA           Memo,
       CN_DIAGNOS           Memo,
       CN_PULSO             FolioChico,
       CN_RESPIRA           FolioChico,
       CN_PESO              Pesos,
       CN_TEMPERA           FolioChico,
       CN_ALTURA            Pesos,
       CN_PRE_SIS           FolioChico,
       CN_PRE_DIA           FolioChico,
       CN_CUIDADO           Memo,
       CN_RECETA            Memo,
       CN_AV_IZQ            FolioChico,
       CN_AV_DER            FolioChico,
       CN_EST_TIP           Codigo,
       CN_EST_DES           Observaciones,
       CN_EST_HAY           Booleano,
       CN_EST_FEC           Fecha,
       CN_EST_RES           Memo,
       CN_EST_OBS           Memo,
       CN_HOR_FIN           Hora,
       CN_IMSS              Booleano,
       US_CODIGO            Usuario,
       CN_FEC_MOD           Fecha,
       CN_SUB_SEC           Booleano,
       CN_D_EXT             Codigo,
       CN_D_BLOB            Imagen,
       CN_D_OBS             Descripcion
)
GO

/* Llave Primaria de la Tabla CONSULTA */
/* Patch 377 # Seq: 28 Agregado */
ALTER TABLE CONSULTA
       ADD CONSTRAINT PK_CONSULTA PRIMARY KEY (EX_CODIGO, CN_FECHA, CN_TIPO)
GO

/* Llave For�nea de la Tabla CONSULTA hacia EXPEDIEN */
/* Patch 377 # Seq: 29 Agregado */
ALTER TABLE CONSULTA
       ADD CONSTRAINT FK_CONSULTA_EXPEDIEN
       FOREIGN KEY (EX_CODIGO)
       REFERENCES EXPEDIEN
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

/* Creaci�n de la Tabla GRUPO_EX */
/* Patch 377 # Seq: 30 Agregado */
CREATE TABLE GRUPO_EX (
       GX_CODIGO            Codigo,
       GX_TITULO            Observaciones,
       GX_POSICIO           FolioChico
)
GO

/* Llave Primaria de la Tabla GRUPO_EX */
/* Patch 377 # Seq: 31 Agregado */
ALTER TABLE GRUPO_EX
       ADD CONSTRAINT PK_GRUPO_EX PRIMARY KEY (GX_CODIGO)
GO

/* Creaci�n de la Tabla CAMPO_EX */
/* Patch 377 # Seq: 32 Agregado */
CREATE TABLE CAMPO_EX (
       CX_NOMBRE            NombreCampo,
       GX_CODIGO            Codigo,
       CX_POSICIO           FolioChico,
       CX_TITULO            Descripcion,
       CX_TIPO              Status,
       CX_DEFAULT           Formula,
       CX_MOSTRAR           Status
)
GO

/* Llave Primaria de la Tabla CAMPO_EX */
/* Patch 377 # Seq: 33 Agregado */
ALTER TABLE CAMPO_EX
       ADD CONSTRAINT PK_CAMPO_EX PRIMARY KEY (CX_NOMBRE)
GO

/*****************************************/
/****  Triggers  *************************/
/*****************************************/

/* Modificar Trigger de Borrado para la Tabla COLABORA para considerar EXPEDIEN */
/* Patch 380 # Seq: 45 Modificado */
ALTER TRIGGER TD_COLABORA ON COLABORA AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
  declare @OldCodigo NumeroEmpleado;

  select @OldCodigo = CB_CODIGO from Deleted;

  delete NOMINA	from NOMINA where ( NOMINA.CB_CODIGO = @OldCodigo ) and ( NOMINA.NO_STATUS <= 4 )
  if ( @OldCodigo > 0 )
  begin
       delete from EXPEDIEN where ( EXPEDIEN.CB_CODIGO = @OldCodigo )
  end
END
GO

/* Modificar Trigger de Cambio para la Tabla COLABORA para considerar EXPEDIEN */
/* Patch 380 # Seq: 46 Modificado */
ALTER TRIGGER TU_COLABORA ON COLABORA AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( CB_CODIGO )
  BEGIN
	declare @NewCodigo NumeroEmpleado, @OldCodigo NumeroEmpleado;

	select @NewCodigo = CB_CODIGO from Inserted;
        select @OldCodigo = CB_CODIGO from Deleted;

	UPDATE NOMINA	SET CB_CODIGO = @NewCodigo WHERE CB_CODIGO = @OldCodigo;
	UPDATE LIQ_EMP	SET CB_CODIGO = @NewCodigo WHERE CB_CODIGO = @OldCodigo;
	UPDATE WORKS	SET CB_CODIGO = @NewCodigo WHERE CB_CODIGO = @OldCodigo;
	UPDATE CED_EMP	SET CB_CODIGO = @NewCodigo WHERE CB_CODIGO = @OldCodigo;
        if ( @OldCodigo > 0 )
        begin
             update EXPEDIEN set EXPEDIEN.CB_CODIGO = @NewCodigo
             where ( EXPEDIEN.CB_CODIGO = @OldCodigo )
        end
  END
END
GO

/* Trigger de Cambio para la Tabla CAUSACCI */
/* Patch 380 # Seq: 48 Agregado */
CREATE TRIGGER TU_CAUSACCI ON CAUSACCI AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo
        declare @OldCodigo Codigo;
	select @NewCodigo = TB_CODIGO from Inserted;
        select @OldCodigo = TB_CODIGO from Deleted;
        update ACCIDENT set ACCIDENT.AX_CAUSA = @NewCodigo
        where ( ACCIDENT.AX_CAUSA = @OldCodigo );
  END
END
GO

/* Trigger de Cambio para la Tabla MOTACCI */
/* Patch 380 # Seq: 49 Agregado */
CREATE TRIGGER TU_MOTACCI ON MOTACCI AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
 	declare @NewCodigo Codigo;
	declare @OldCodigo Codigo;
	select @NewCodigo = TB_CODIGO from Inserted;
	select @OldCodigo = TB_CODIGO from Deleted;
	update ACCIDENT set ACCIDENT.AX_MOTIVO = @NewCodigo
	where ( ACCIDENT.AX_MOTIVO = @OldCodigo );
  END
END
GO

/* Trigger de Cambio para la Tabla TACCIDEN */
/* Patch 380 # Seq: 50 Agregado */
CREATE TRIGGER TU_TACCIDEN ON TACCIDEN AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo
        declare @OldCodigo Codigo;
	select @NewCodigo = TB_CODIGO from Inserted;
        select @OldCodigo = TB_CODIGO from Deleted;
        update ACCIDENT set ACCIDENT.AX_TIP_ACC = @NewCodigo
        where ( ACCIDENT.AX_TIP_ACC = @OldCodigo );
  END
END
GO

/* Trigger de Cambio para la Tabla TESTUDIO */
/* Patch 380 # Seq: 51 Agregado */
CREATE TRIGGER TU_TESTUDIO ON TESTUDIO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo
        declare @OldCodigo Codigo;
	select @NewCodigo = TB_CODIGO from Inserted;
        select @OldCodigo = TB_CODIGO from Deleted;
        update CONSULTA set CONSULTA.CN_EST_TIP = @NewCodigo
        where ( CONSULTA.CN_EST_TIP = @OldCodigo );
  END
END
GO

/* Trigger de Cambio para la Tabla DIAGNOST */
/* Patch 380 # Seq: 52 Agregado */
CREATE TRIGGER TU_DIAGNOST ON DIAGNOST AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( DA_CODIGO )
  BEGIN
	declare @NewCodigo Codigo
        declare @OldCodigo Codigo;
	select @NewCodigo = DA_CODIGO from Inserted;
        select @OldCodigo = DA_CODIGO from Deleted;
        update CONSULTA set CONSULTA.DA_CODIGO = @NewCodigo
        where ( CONSULTA.DA_CODIGO = @OldCodigo );
  END
END
GO

/* Trigger de Cambio para la Tabla TCONSLTA */
/* Patch 380 # Seq: 53 Agregado */
CREATE TRIGGER TU_TCONSLTA ON TCONSLTA AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo
        declare @OldCodigo Codigo;
	select @NewCodigo = TB_CODIGO from Inserted;
        select @OldCodigo = TB_CODIGO from Deleted;
        update CONSULTA set CONSULTA.CN_TIPO = @NewCodigo
        where ( CONSULTA.CN_TIPO = @OldCodigo );
  END
END
GO

/* Trigger de Borrado para la Tabla GRUPO_EX */
/* Patch 380 # Seq: 54 Agregado */
CREATE TRIGGER TD_GRUPO_EX ON GRUPO_EX AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
  update CAMPO_EX set CAMPO_EX.GX_CODIGO = '' from CAMPO_EX, Deleted where ( CAMPO_EX.GX_CODIGO = Deleted.GX_CODIGO );
END
GO

/* Trigger de Cambio para la Tabla GRUPO_EX */
/* Patch 380 # Seq: 55 Agregado */
CREATE TRIGGER TU_GRUPO_EX ON GRUPO_EX AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( GX_CODIGO )
  BEGIN
	declare @NewCodigo Codigo
        declare @OldCodigo Codigo;
	select @NewCodigo = GX_CODIGO from Inserted;
        select @OldCodigo = GX_CODIGO from Deleted;
        update CAMPO_EX set CAMPO_EX.GX_CODIGO = @NewCodigo
        where ( CAMPO_EX.GX_CODIGO = @OldCodigo );
  END
END
GO

/**************************************************/
/* STORED PROCEDURES ******************************/
/**************************************************/

/* Agregar SP para diccionario de Expediente*/
/* Patch 380 # Seq: 188 Agregado */
create PROCEDURE SP_TITULO_EXPEDIEN(
  @Nombre VarChar(10),
  @Titulo VarChar( 30)
) as
begin
     SET NOCOUNT ON
     declare @NombreEx Varchar(10);
     declare @TituloEx Varchar(30);


     declare TemporalDiccion CURSOR for
             select DI_NOMBRE from DICCION
             where DI_CLASIFI = 169
             and DI_NOMBRE like @Nombre+'%'
             order by DI_NOMBRE

     open TemporalDiccion;
     fetch NEXT from TemporalDiccion
     into @NombreEx

     while ( @@Fetch_Status = 0 )
     begin

          select @TituloEx = CX_TITULO from CAMPO_EX
          where CX_NOMBRE = @NombreEx

          if (( @TituloEx is Null ) or ( @TituloEx = '' ))
          begin
	       set @TituloEx = @Titulo + ' #' + SUBSTRING( @NombreEx , 9 , 2 );	
          end

          Update DICCION
          set DI_TITULO = @TituloEx,
              DI_TCORTO = @TituloEx,
              DI_CLAVES = @TituloEx
          where DI_CLASIFI = 169 /*Clasificacion de Expediente*/
          and DI_NOMBRE = @NombreEx;

          fetch NEXT from TemporalDiccion
          into @NombreEx
     end

     close TemporalDiccion;
     deallocate TemporalDiccion;
end
GO

/* Agregar SP para diccionario de Expediente*/
/* Patch 380 # Seq: 189 Agregado */
CREATE PROCEDURE SP_TITULO_EXPEDIEN_BOL(
  @Nombre VarChar(10),
  @Titulo VarChar( 30)
) as
begin
     SET NOCOUNT ON
     declare @NombreEx Varchar(10);
     declare @TituloEx Varchar(30);


     EXECUTE SP_TITULO_EXPEDIEN @NOMBRE, @TITULO ;

     declare TemporalDiccion CURSOR for
             select DI_TITULO, DI_NOMBRE from DICCION
             where DI_CLASIFI = 169
             and DI_NOMBRE like @Nombre+'%'
             order by DI_NOMBRE;

     open TemporalDiccion;
     fetch NEXT from TemporalDiccion
     into @TituloEx, @NombreEx;

     while ( @@Fetch_Status = 0 )
     begin
 
	  select @TituloEx = 'D:'+ SUBSTRING( @TituloEx, 1, 28 );
          

	  set @NombreEx = 'EX_G_DES'+ SUBSTRING( @NombreEx , 9 , 2 );

          Update DICCION
          set DI_TITULO = @TituloEx,
              DI_TCORTO = @TituloEx,
              DI_CLAVES = @TituloEx
          where DI_CLASIFI = 169 /*Clasificacion de Expediente*/
          and DI_NOMBRE = @NombreEx;

          fetch NEXT from TemporalDiccion
          into @TituloEx, @NombreEx
     end

     close TemporalDiccion;
     deallocate TemporalDiccion;
end

/* Define el ancho del c�digo de empleado en Diccionario*/
/* Patch 380 # Seq: 196 Agregado */
CREATE PROCEDURE SP_ANCHO_EMPLEADO @Ancho SmallInt AS
begin
     SET NOCOUNT ON
     update DICCION set DI_ANCHO = @Ancho
     where ( DI_NOMBRE = 'CB_CODIGO' )
end
GO

/* Define el ancho del c�digo de empleado en Reportes*/
/* Patch 380 # Seq: 197 Agregado */
CREATE PROCEDURE SP_ANCHO_EMPLEADO_REPORTES @Ancho SmallInt AS
begin
     SET NOCOUNT ON
     update CAMPOREP set CR_ANCHO = @Ancho where
     ( CR_TIPO = 0 ) and
     ( CR_FORMULA like '%CB_CODIGO' ) and
     ( CR_FORMULA not like '%@%' ) and
     ( CR_ANCHO < @Ancho );
end
GO

/* SP_CUSTOM_DICCION es un HOOK para hacer trabajo extra */
/* Patch 380 # Seq: 190 Agregado */
CREATE PROCEDURE DBO.SP_CUSTOM_DICCION  AS

/* Cambiar SP para diccionario de Expediente y para HOOK de SP_CUSTOM_DICCION */
/* Patch 380 # Seq: 195 Modificado */
ALTER PROCEDURE DBO.SP_REFRESH_DICCION
as
begin
  SET NOCOUNT ON
  execute  SP_TITULO_TABLA 13, 'Nivel de Organigrama #1', 41 ;
  execute  SP_TITULO_TABLA 14, 'Nivel de Organigrama #2', 42 ;
  execute  SP_TITULO_TABLA 15, 'Nivel de Organigrama #3', 43 ;
  execute  SP_TITULO_TABLA 16, 'Nivel de Organigrama #4', 44 ;
  execute  SP_TITULO_TABLA 17, 'Nivel de Organigrama #5', 45 ;
  execute  SP_TITULO_TABLA 18, 'Nivel de Organigrama #6', 46 ;
  execute  SP_TITULO_TABLA 19, 'Nivel de Organigrama #7', 47 ;
  execute  SP_TITULO_TABLA 20, 'Nivel de Organigrama #8', 48 ;
  execute  SP_TITULO_TABLA 21, 'Nivel de Organigrama #9', 49 ;

  execute  SP_TITULO_TABLA 35, 'Tabla Adicional #1', 19
  execute  SP_TITULO_TABLA 36, 'Tabla Adicional #2', 20
  execute  SP_TITULO_TABLA 37, 'Tabla Adicional #3', 21
  execute  SP_TITULO_TABLA 38, 'Tabla Adicional #4', 22

  execute  SP_TITULO_TABLA 134, 'Orden de Trabajo', 149
  execute  SP_TITULO_TABLA 136, 'Cat�logo de Partes', 141
  execute  SP_TITULO_TABLA 138, 'Cat�logo de Area', 148
  execute  SP_TITULO_TABLA 140, 'Cat�logo de Operaciones', 142
  execute  SP_TITULO_TABLA 142, 'Cat�logo de Tiempo Muerto', 138
  execute  SP_TITULO_TABLA 176, 'Lista de Defectos', 187
  execute  SP_TITULO_TABLA 143, 'Modulador #1', 135
  execute  SP_TITULO_TABLA 144, 'Modulador #2', 136 
  execute  SP_TITULO_TABLA 145, 'Modulador #3', 137

  execute  SP_TITULO_CAMPO 13, 'C�digo de Nivel #1', 'CB_NIVEL1'
  execute  SP_TITULO_CAMPO 14, 'C�digo de Nivel #2', 'CB_NIVEL2'
  execute  SP_TITULO_CAMPO 15, 'C�digo de Nivel #3', 'CB_NIVEL3'
  execute  SP_TITULO_CAMPO 16, 'C�digo de Nivel #4', 'CB_NIVEL4'
  execute  SP_TITULO_CAMPO 17, 'C�digo de Nivel #5', 'CB_NIVEL5'
  execute  SP_TITULO_CAMPO 18, 'C�digo de Nivel #6', 'CB_NIVEL6'
  execute  SP_TITULO_CAMPO 19, 'C�digo de Nivel #7', 'CB_NIVEL7'
  execute  SP_TITULO_CAMPO 20, 'C�digo de Nivel #8', 'CB_NIVEL8'
  execute  SP_TITULO_CAMPO 21, 'C�digo de Nivel #9', 'CB_NIVEL9' 

  execute  SP_TITULO_CAMPO 13, 'Nuevo Nivel #1', 'EV_NIVEL1'
  execute  SP_TITULO_CAMPO 14, 'Nuevo Nivel #2', 'EV_NIVEL2' 
  execute  SP_TITULO_CAMPO 15, 'Nuevo Nivel #3', 'EV_NIVEL3' 
  execute  SP_TITULO_CAMPO 16, 'Nuevo Nivel #4', 'EV_NIVEL4' 
  execute  SP_TITULO_CAMPO 17, 'Nuevo Nivel #5', 'EV_NIVEL5'
  execute  SP_TITULO_CAMPO 18, 'Nuevo Nivel #6', 'EV_NIVEL6' 
  execute  SP_TITULO_CAMPO 19, 'Nuevo Nivel #7', 'EV_NIVEL7' 
  execute  SP_TITULO_CAMPO 20, 'Nuevo Nivel #8', 'EV_NIVEL8' 
  execute  SP_TITULO_CAMPO 21, 'Nuevo Nivel #9', 'EV_NIVEL9'

  execute  SP_TITULO_CAMPO 22, 'Fecha Adicional #1', 'CB_G_FEC_1' 
  execute  SP_TITULO_CAMPO 23, 'Fecha Adicional #2', 'CB_G_FEC_2'
  execute  SP_TITULO_CAMPO 24, 'Fecha Adicional #3', 'CB_G_FEC_3' 
  execute  SP_TITULO_CAMPO 25, 'Texto Adicional #1', 'CB_G_TEX_1' 
  execute  SP_TITULO_CAMPO 26, 'Texto Adicional #2', 'CB_G_TEX_2'
  execute  SP_TITULO_CAMPO 27, 'Texto Adicional #3', 'CB_G_TEX_3' 
  execute  SP_TITULO_CAMPO 28, 'Texto Adicional #4', 'CB_G_TEX_4'
  execute  SP_TITULO_CAMPO 29, 'N�mero Adicional #1', 'CB_G_NUM_1'
  execute  SP_TITULO_CAMPO 30, 'N�mero Adicional #2', 'CB_G_NUM_2' 
  execute  SP_TITULO_CAMPO 31, 'N�mero Adicional #3', 'CB_G_NUM_3'
  execute  SP_TITULO_CAMPO 32, 'L�gico Adicional #1', 'CB_G_LOG_1' 
  execute  SP_TITULO_CAMPO 33, 'L�gico Adicional #2', 'CB_G_LOG_2'
  execute  SP_TITULO_CAMPO 34, 'L�gico Adicional #3', 'CB_G_LOG_3'
  execute  SP_TITULO_CAMPO 35, 'Tabla Adicional #1','CB_G_TAB_1'
  execute  SP_TITULO_CAMPO 36, 'Tabla Adicional #2','CB_G_TAB_2'
  execute  SP_TITULO_CAMPO 37, 'Tabla Adicional #3','CB_G_TAB_3'
  execute  SP_TITULO_CAMPO 38, 'Tabla Adicional #4','CB_G_TAB_4'

  execute  SP_TITULO_CAMPO 134, 'Orden de Trabajo','WO_NUMBER'
  execute  SP_TITULO_CAMPO 136, 'Parte','AR_CODIGO' 
  execute  SP_TITULO_CAMPO 138, 'Area','CB_AREA' 
  execute  SP_TITULO_CAMPO 138, 'Area','CI_AREA' 
  execute  SP_TITULO_CAMPO 138, 'Area','CE_AREA' 
  execute  SP_TITULO_CAMPO 140, 'Operaci�n','OP_NUMBER'
  execute  SP_TITULO_CAMPO 142, 'Tiempo Muerto','WK_TMUERTO'
  execute  SP_TITULO_CAMPO 142, 'Tiempo Muerto','LX_TMUERTO' 
  execute  SP_TITULO_CAMPO 176, 'Defecto','DE_CODIGO' 
  execute  SP_TITULO_CAMPO 143, 'Modulador #1','LX_MODULA1' 
  execute  SP_TITULO_CAMPO 144, 'Modulador #2','LX_MODULA2'
  execute  SP_TITULO_CAMPO 145, 'Modulador #3','LX_MODULA3' 
  execute  SP_TITULO_CAMPO 143, 'Modulador #1','WK_MOD_1' 
  execute  SP_TITULO_CAMPO 144, 'Modulador #2','WK_MOD_2' 
  execute  SP_TITULO_CAMPO 145, 'Modulador #3','WK_MOD_3'

  execute  SP_TITULO_CONTEO  161, 'Criterio #1',  'CT_NIVEL_1'
  execute  SP_TITULO_CONTEO  162, 'Criterio #2',  'CT_NIVEL_2'
  execute  SP_TITULO_CONTEO  163, 'Criterio #3',  'CT_NIVEL_3'
  execute  SP_TITULO_CONTEO  164, 'Criterio #4',  'CT_NIVEL_4'
  execute  SP_TITULO_CONTEO  165, 'Criterio #5',  'CT_NIVEL_5'

  execute  SP_TITULO_EXPEDIEN_BOL  'EX_G_BOL', 'Si/No/Texto'
  execute  SP_TITULO_EXPEDIEN  'EX_G_LOG', 'Si/No'
  execute  SP_TITULO_EXPEDIEN  'EX_G_NUM', 'Numero'
  execute  SP_TITULO_EXPEDIEN  'EX_G_TEX', 'Texto'

  execute  SP_CUSTOM_DICCION

end
GO

/* SP para averiguar status de accidente de un expediente*/
/* Patch 380 # Seq: 174 Agregado */
CREATE FUNCTION SP_STATUS_ACC (
  @EXPEDIENTE INTEGER,
  @FECHAINICIAL DATETIME,
  @FECHAFINAL DATETIME
) RETURNS CHAR(1) AS
begin

     declare @Cuantos INTEGER;
     declare @Resultado Char(1);

     select @Cuantos = COUNT(*) from ACCIDENT where
     ( EX_CODIGO = @EXPEDIENTE ) and
     ( AX_FECHA = @FechaInicial ) and
     ( AX_FECHA <= @FechaFinal )

     if ( @Cuantos > 0 )
        Set @Resultado = 'S';
     else
         Set @Resultado = 'N';

     Return @Resultado
end
GO

/* SP para averiguar status de embarazo de un expediente*/
/* Patch 380 # Seq: 175 Agregado */
CREATE FUNCTION SP_STATUS_EMB (
  @EXPEDIENTE INTEGER,
  @FECHAINICIAL DATETIME,
  @FECHAFINAL DATETIME
) RETURNS CHAR(1) AS
begin
     declare @Cuantos INTEGER;
     declare @Resultado Char(1);

     select @Cuantos = COUNT(*) from EMBARAZO where
     ( EX_CODIGO = @EXPEDIENTE ) and
     ( ( @FechaInicial between EM_FEC_UM and EM_FEC_PP ) or
     ( @FechaFinal between EM_FEC_UM and EM_FEC_PP ) )

     if ( @Cuantos > 0 )
        Set @Resultado = 'S';
     else
         Set @Resultado = 'N';

     Return @Resultado;

end
GO

/* SP para inicializar campos disponibles para configurar el Expediente*/
/* Patch 380 # Seq: 176 Agregado */
CREATE PROCEDURE DBO.INIT_EXPEDIENTE
as
begin
     BEGIN TRANSACTION TX1
     delete from CAMPO_EX
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX01', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX02', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX03', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX04', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX05', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX06', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX07', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX08', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX09', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX10', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX11', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX12', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX13', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX14', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX15', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX16', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX17', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX18', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX19', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX20', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX21', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX22', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX23', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX24', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX25', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX26', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX27', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX28', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX29', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX30', 0 )

     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG01', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG02', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG03', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG04', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG05', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG06', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG07', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG08', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG09', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG10', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG11', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG12', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG13', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG14', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG15', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG16', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG17', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG18', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG19', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG20', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG21', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG22', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG23', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG24', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG25', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG26', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG27', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG28', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG29', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG30', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG31', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG32', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG33', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG34', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG35', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG36', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG37', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG38', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG39', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG40', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG41', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG42', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG43', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG44', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG45', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG46', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG47', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG48', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG49', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG50', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG51', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG52', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG53', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG54', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG55', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG56', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG57', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG58', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG59', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG60', 2 )

     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM01', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM02', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM03', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM04', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM05', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM06', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM07', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM08', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM09', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM10', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM11', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM12', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM13', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM14', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM15', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM16', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM17', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM18', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM19', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM20', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM21', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM22', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM23', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM24', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM25', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM26', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM27', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM28', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM29', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM30', 1 )

     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL01', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL02', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL03', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL04', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL05', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL06', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL07', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL08', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL09', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL10', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL11', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL12', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL13', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL14', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL15', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL16', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL17', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL18', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL19', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL20', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL21', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL22', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL23', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL24', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL25', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL26', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL27', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL28', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL29', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL30', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL31', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL32', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL33', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL34', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL35', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL36', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL37', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL38', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL39', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL40', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL41', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL42', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL43', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL44', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL45', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL46', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL47', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL48', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL49', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL50', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL51', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL52', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL53', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL54', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL55', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL56', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL57', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL58', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL59', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL60', 3 )
     COMMIT TRANSACTION TX1
end
GO

/* Inicializa los campos disponibles para el Expediente */
/* Patch 380 # Seq: 500 Agregado */
exec DBO.INIT_EXPEDIENTE
GO

/* Agregar verificaciones de d�as h�biles por pren�mina*/
/* Patch 380 # Seq: 71 Modificado */
ALTER PROCEDURE DBO.SP_TMPLABOR_START( @USUARIO INTEGER, @FECHA DATETIME)
AS
  DECLARE @STATUS_EMP SMALLINT;
  DECLARE @AU_CHECADAS INTEGER;
  DECLARE @CB_CODIGO INTEGER;
  DECLARE @CB_FEC_KAR DATETIME;
  DECLARE @CB_TURNO CHAR(6);
  DECLARE @CB_AREA CHAR(6);
BEGIN
     SET NOCOUNT ON;
     UPDATE TMPLABOR SET TL_APLICA = 'N', TL_CALCULA = 'N' WHERE ( US_CODIGO = @USUARIO );
     DECLARE Temporal CURSOR FOR
        SELECT TMPLABOR.CB_CODIGO, COLABORA.CB_FEC_KAR, COLABORA.CB_TURNO FROM TMPLABOR
        left outer join COLABORA ON ( COLABORA.CB_CODIGO = TMPLABOR.CB_CODIGO )
        WHERE ( TMPLABOR.US_CODIGO = @USUARIO ) order by TMPLABOR.CB_CODIGO
     OPEN Temporal
     FETCH NEXT FROM Temporal INTO @CB_CODIGO, @CB_FEC_KAR, @CB_TURNO;
     WHILE @@FETCH_STATUS = 0
     BEGIN
       SELECT @CB_AREA = ( SELECT DBO.SP_FECHA_AREA_DEFAULT( @CB_CODIGO, @FECHA ) );
       SELECT @AU_CHECADAS = COUNT(*) from CHECADAS where
                     ( CB_CODIGO = @CB_CODIGO ) and
                     ( AU_FECHA = @FECHA ) and
                     ( CH_TIPO in ( 1, 2 ) );
       IF ( @AU_CHECADAS = 0 )
       BEGIN
            SELECT @AU_CHECADAS = COUNT(*) from AUSENCIA where
            ( CB_CODIGO = @CB_CODIGO ) and
            ( AU_FECHA = @FECHA ) and
            ( ( AU_HORASCK + AU_NUM_EXT > 0 ) or
            ( ( AU_HORAS > 0 ) and not ( AU_TIPODIA in ( 3, 8 ) ) ) or
            ( ( AU_EXTRAS + AU_DES_TRA ) > 0 ) );
       END
       IF ( @AU_CHECADAS = 0 )
       BEGIN
            SELECT  @STATUS_EMP = ( SELECT DBO.SP_STATUS_EMP( @FECHA, @CB_CODIGO ) as RESULTADO );
            IF ( @FECHA < @CB_FEC_KAR )
            BEGIN
                 SELECT @CB_TURNO = CB_TURNO FROM SP_FECHA_KARDEX( @FECHA, @CB_CODIGO );
            END
            UPDATE TMPLABOR SET
            CB_AREA = @CB_AREA,
            CB_TURNO = @CB_TURNO,
            TL_STATUS = @STATUS_EMP
            WHERE ( US_CODIGO = @USUARIO ) and ( CB_CODIGO = @CB_CODIGO );
       END
       ELSE
       BEGIN
            UPDATE TMPLABOR SET
            CB_AREA = @CB_AREA,
            CB_TURNO = '',
            TL_STATUS = 0,
            TL_APLICA = 'S'
            WHERE ( US_CODIGO = @USUARIO ) and ( CB_CODIGO = @CB_CODIGO );
       END
       FETCH NEXT FROM Temporal INTO @CB_CODIGO, @CB_FEC_KAR, @CB_TURNO;
     END
     CLOSE Temporal
     DEALLOCATE Temporal
END
GO

update GLOBAL set GL_FORMULA = '377' where ( GL_CODIGO = 133 )
GO
