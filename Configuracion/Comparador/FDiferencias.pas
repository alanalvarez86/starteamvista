unit FDiferencias;

interface

uses Windows, Messages, SysUtils,
     Classes, Graphics, Controls, DB, DBTables,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Grids;

type
  TDiferencias = class(TForm)
    Panel: TPanel;
    Salir: TBitBtn;
    StringGrid: TStringGrid;
  private
    procedure AgregaDiferencia(Maestro, Prueba: TField);
    { Private declarations }
  public
    { Public declarations }
    function Conectar: Boolean;
  end;

var
  Diferencias: TDiferencias;

procedure MostrarDiferencias;

implementation

uses ZetaCommonTools,
     DComparador;

{$R *.DFM}

procedure MostrarDiferencias;
begin
     if not Assigned( Diferencias ) then
        Diferencias := TDiferencias.Create( Application );
     with Diferencias do
     begin
          if Conectar then
             ShowModal
          else
              ShowMessage( '� No Hay Diferencias ' );
     end;
end;

{ ************** TDiferencias *********** }

function TDiferencias.Conectar: Boolean;
begin
     with StringGrid do
     begin
          RowCount := 2; { Inicializacion }
          FixedRows := 1;
          Cells[ 0, 0 ] := 'Campo';
          Cells[ 1, 0 ] := 'Valor Maestro';
          Cells[ 2, 0 ] := 'Valor Prueba';
          Cells[ 3, 0 ] := 'Diferencia';
          Result := dmComparador.GetDiferences( AgregaDiferencia );
          RowCount := RowCount - 1;
     end;
end;

procedure TDiferencias.AgregaDiferencia( Maestro, Prueba: TField );

function GetValueAsString( Campo: TField ): String;
begin
     with Campo do
     begin
          case DataType of
               ftString: Result := AsString;
               ftSmallint: Result := IntToStr( AsInteger );
               ftInteger: Result := IntToStr( AsInteger );
               ftWord: Result := IntToStr( AsInteger );
               ftBoolean: Result := ZetaCommonTools.zBoolToStr( AsBoolean );
               ftFloat: Result := FormatFloat( '#,##0.0000;(#,##0.0000)', AsFloat );
               ftCurrency: Result := FormatFloat( '#,##0.00;(#,##0.00)', AsCurrency );
               ftDate: Result := FormatDateTime( 'dd/mmm/yyyy', AsDateTime );
               ftTime: Result := FormatDateTime( 'hh:nn', AsDateTime );
               ftDateTime: Result := FormatDateTime( 'dd/mmm/yyyy hh:nn', AsDateTime );
          else
              Result := '???';
          end;
     end;
end;

function GetDiferenciaAsString: String;
begin
     with Maestro do
     begin
          case DataType of
               ftSmallint: Result := IntToStr( AsInteger - Prueba.AsInteger );
               ftInteger: Result := IntToStr( AsInteger - Prueba.AsInteger );
               ftWord: Result := IntToStr( AsInteger - Prueba.AsInteger );
               ftFloat: Result := FormatFloat( '#,##0.0000;(#,##0.0000)', AsFloat - Prueba.AsFloat );
               ftCurrency: Result := FormatFloat( '#,##0.00;(#,##0.00)', AsCurrency - Prueba.AsCurrency );
          else
              Result := '';
          end;
     end;
end;

begin
     with StringGrid do
     begin
          with Maestro do
          begin
               Cells[ 0, ( RowCount - 1 ) ] := FieldName;
               Cells[ 1, ( RowCount - 1 ) ] := GetValueAsString( Maestro );
               Cells[ 2, ( RowCount - 1 ) ] := GetValueAsString( Prueba );
               Cells[ 3, ( RowCount - 1 ) ] := GetDiferenciaAsString;
          end;
          RowCount := RowCount + 1;
     end;
end;

end.
