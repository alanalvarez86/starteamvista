unit DComparador;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Db, DBTables;

type
  TAddDiference = procedure( Maestro, Prueba: TField ) of object;
  TdmComparador = class(TDataModule)
    dbPrueba: TDatabase;
    tqMaestro: TQuery;
    tqPrueba: TQuery;
    dbMaestro: TDatabase;
    procedure SetDisplayFormats(DataSet: TDataSet);
  private
    { Private declarations }
    function BuscaDiferencia( const lAdelante: Boolean ): Boolean;
    function Conectar( BaseDeDatos: TDatabase; const sAlias, sUsuario, sPassword: String ): Boolean;
    function CamposSonDiferentes( Maestro: TField ): Boolean;
  public
    { Public declarations }
    function ConectarMaestro( const sAlias, sUsuario, sPassword: String ): Boolean;
    function ConectarPrueba( const sAlias, sUsuario, sPassword: String ): Boolean;
    function Comparar( const sScript: String ): Boolean; overload;
    function Comparar( const sScript, sQuery: String ): Boolean; overload;
    function BuscaSiguienteDiferencia: Boolean;
    function BuscaAnteriorDiferencia: Boolean;
    function GetDiferences( Metodo: TAddDiference ): Boolean;
  end;

var
  dmComparador: TdmComparador;

implementation

uses ZetaCommonTools;

{$R *.DFM}

procedure TdmComparador.SetDisplayFormats(DataSet: TDataSet);
var
   i: Integer;

procedure MaskNumerico( const Campo: TNumericField );
begin
     Campo.DisplayFormat := '#,0.0000';
end;

begin
     with Dataset do
     begin
          for i := 0 to ( FieldCount - 1 ) do
          begin
               case Fields[ i ].DataType of
                    ftFloat: MaskNumerico( TNumericField( Fields[ i ] ) );
                    ftCurrency: MaskNumerico( TNumericField( Fields[ i ] ) );
               end;
          end;
     end;
end;

function TdmComparador.Conectar( BaseDeDatos: TDatabase; const sAlias, sUsuario, sPassword: String ): Boolean;
begin
     with BaseDeDatos do
     begin
          try
             Connected := False;
             AliasName := sAlias;
             with Params do
             begin
                  Values[ 'USER NAME' ] := sUsuario;
                  Values[ 'PASSWORD' ] := sPassword;
             end;
             Connected := True;
             Result := True;
          except
                on Error: Exception do
                begin
                     Connected := False;
                     Application.HandleException( Error );
                     Result := False;
                end;
          end;
     end;
end;

function TdmComparador.ConectarMaestro( const sAlias, sUsuario, sPassword: String ): Boolean;
begin
     Result := Conectar( dbMaestro, sAlias, sUsuario, sPassword );
end;

function TdmComparador.ConectarPrueba( const sAlias, sUsuario, sPassword: String ): Boolean;
begin
     Result := Conectar( dbPrueba, sAlias, sUsuario, sPassword );
end;

function TdmComparador.Comparar( const sScript, sQuery: String ): Boolean;

procedure ConectaQuery( Query: TQuery; const sSQL: String );
begin
     with Query do
     begin
          Active := False;
          with SQL do
          begin
               Clear;
               Text := sSQL;
          end;
          Active := True;
     end;
end;

begin
     ConectaQuery( tqMaestro, sScript );
     ConectaQuery( tqPrueba, sQuery );
     with tqPrueba do
     begin
          First;
          DisableControls;
     end;
     with tqMaestro do
     begin
          DisableControls;
          First;
          Result := BuscaDiferencia( True );
          EnableControls;
          tqPrueba.EnableControls;
     end;
end;

function TdmComparador.Comparar( const sScript: String ): Boolean;
begin
     Result := Comparar( sScript, sScript );
end;

function TdmComparador.BuscaAnteriorDiferencia: Boolean;
begin
     with tqPrueba do
     begin
          DisableControls;
          Prior;
     end;
     with tqMaestro do
     begin
          DisableControls;
          Prior;
          Result := BuscaDiferencia( False );
          EnableControls;
     end;
     with tqPrueba do
     begin
          EnableControls;
     end;
end;

function TdmComparador.BuscaSiguienteDiferencia: Boolean;
begin
     with tqPrueba do
     begin
          DisableControls;
          Next;
     end;
     with tqMaestro do
     begin
          DisableControls;
          Next;
          Result := BuscaDiferencia( True );
          EnableControls;
     end;
     with tqPrueba do
     begin
          EnableControls;
     end;
end;

function TdmComparador.BuscaDiferencia( const lAdelante: Boolean ): Boolean;
var
   i: Integer;
begin
     Result := False;
     with tqMaestro do
     begin
          while not ( ( lAdelante and Eof ) or ( not lAdelante and Bof ) ) and not Result do
          begin
               for i := 0 to ( FieldCount - 1 ) do
               begin
                    if CamposSonDiferentes( Fields[ i ] ) then
                    begin
                         Result := True;
                         Break;
                    end;
               end;
               if not Result then
               begin
                    if lAdelante then
                    begin
                         Next;
                         tqPrueba.Next;
                         if Eof or tqPrueba.Eof then
                            Result := ( Eof <> tqPrueba.Eof );
                    end
                    else
                    begin
                         Prior;
                         tqPrueba.Prior;
                         if Bof or tqPrueba.Bof then
                            Result := ( Bof <> tqPrueba.Bof );
                    end;
               end;
          end;
     end;
end;

function TdmComparador.CamposSonDiferentes( Maestro: TField ): Boolean;
var
   Prueba: TField;

function Iguales( const rValor1, rValor2: Extended ): Boolean;
begin
     Result := ( Abs( rValor1 - rValor2 ) < 0.0001 );
end;

begin
     Prueba := tqPrueba.FindField( Maestro.FieldName );
     if ( Prueba = nil ) then
        Result := False
     else
     begin
          with Maestro do
          begin
               case DataType of
                    ftString: Result := ( AsString <> Prueba.AsString );
                    ftSmallint: Result := ( AsInteger <> Prueba.AsInteger );
                    ftInteger: Result := ( AsInteger <> Prueba.AsInteger );
                    ftWord: Result := ( AsInteger <> Prueba.AsInteger );
                    ftBoolean: Result := ( AsBoolean <> Prueba.AsBoolean );
                    ftFloat: Result := not Iguales( AsFloat, Prueba.AsFloat );
                    ftCurrency: Result := not Iguales( AsCurrency, Prueba.AsCurrency );
                    ftDate: Result := ( AsDateTime <> Prueba.AsDateTime );
                    ftTime: Result := ( AsDateTime <> Prueba.AsDateTime );
                    ftDateTime: Result := ( AsDateTime <> Prueba.AsDateTime );
               else
                   Result := False;
               end;
          end;
     end;
end;

function TdmComparador.GetDiferences( Metodo: TAddDiference ): Boolean;
var
   i: Integer;

begin
     Result := False;
     with tqMaestro do
     begin
          if Active then
          begin
               for i := 0 to ( FieldCount - 1 ) do
               begin
                    if CamposSonDiferentes( Fields[ i ] ) then
                    begin
                         Metodo( Fields[ i ], tqPrueba.FindField( Fields[ i ].FieldName ) );
                         Result := True;
                    end;
               end;
          end
          else
              Result := False;
     end;
end;

end.
