object Diferencias: TDiferencias
  Left = 280
  Top = 215
  Width = 482
  Height = 256
  Caption = 'Campos Diferentes'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel: TPanel
    Left = 0
    Top = 195
    Width = 474
    Height = 34
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Salir: TBitBtn
      Left = 394
      Top = 5
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Salir'
      TabOrder = 0
      Kind = bkClose
    end
  end
  object StringGrid: TStringGrid
    Left = 0
    Top = 0
    Width = 474
    Height = 195
    Align = alClient
    ColCount = 4
    DefaultColWidth = 110
    FixedCols = 0
    RowCount = 2
    TabOrder = 1
  end
end
