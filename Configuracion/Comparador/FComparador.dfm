object Comparator: TComparator
  Left = 504
  Top = 10
  Width = 472
  Height = 430
  Caption = 'Comparar Querys en Dos Bases De Datos '
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Wizard: TZetaWizard
    Left = 0
    Top = 343
    Width = 464
    Height = 34
    Align = alBottom
    TabOrder = 0
    AlEjecutar = WizardAlEjecutar
    AlCancelar = WizardAlCancelar
    AfterMove = WizardAfterMove
    BeforeMove = WizardBeforeMove
    PageControl = PageControl
    Reejecutar = True
    object Anterior: TZetaWizardButton
      Left = 5
      Top = 5
      Width = 86
      Height = 25
      Hint = 'Regresar al Paso Anterior'
      Caption = '&Anterior'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333333333333333333333333333333333333333333333
        3333333333333FF3333333333333003333333333333F77F33333333333009033
        333333333F7737F333333333009990333333333F773337FFFFFF330099999000
        00003F773333377777770099999999999990773FF33333FFFFF7330099999000
        000033773FF33777777733330099903333333333773FF7F33333333333009033
        33333333337737F3333333333333003333333333333377333333333333333333
        3333333333333333333333333333333333333333333333333333333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 2
      Tipo = bwAnterior
      Wizard = Wizard
    end
    object Siguiente: TZetaWizardButton
      Left = 95
      Top = 5
      Width = 86
      Height = 25
      Hint = 'Avanzar Hacia El Siguiente Paso'
      Caption = '&Siguiente'
      Enabled = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333333333333333333333333333333333333333333333
        3333333333333333333333333333333333333333333FF3333333333333003333
        3333333333773FF3333333333309003333333333337F773FF333333333099900
        33333FFFFF7F33773FF30000000999990033777777733333773F099999999999
        99007FFFFFFF33333F7700000009999900337777777F333F7733333333099900
        33333333337F3F77333333333309003333333333337F77333333333333003333
        3333333333773333333333333333333333333333333333333333333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 2
      Tipo = bwSiguiente
      Wizard = Wizard
    end
    object Comparar: TZetaWizardButton
      Left = 271
      Top = 5
      Width = 86
      Height = 25
      Hint = 'Efectuar Comparación'
      Anchors = [akTop, akRight]
      Caption = 'C&omparar'
      ModalResult = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
      Tipo = bwEjecutar
      Wizard = Wizard
    end
    object Cancelar: TZetaWizardButton
      Left = 361
      Top = 5
      Width = 86
      Height = 25
      Hint = 'Cancelar y Salir'
      Anchors = [akTop, akRight]
      Caption = '&Cancelar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Kind = bkCancel
      Tipo = bwCancelar
      Wizard = Wizard
    end
  end
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 464
    Height = 343
    ActivePage = Parametros
    Align = alClient
    TabOrder = 1
    object Parametros: TTabSheet
      Caption = 'Parametros'
      TabVisible = False
      object Splitter1: TSplitter
        Left = 217
        Top = 0
        Width = 3
        Height = 333
        Cursor = crHSplit
      end
      object WinDatosGB: TGroupBox
        Left = 220
        Top = 0
        Width = 236
        Height = 333
        Align = alClient
        Caption = ' Base de Datos Prueba '
        TabOrder = 0
        object ScriptTestGB: TGroupBox
          Left = 2
          Top = 108
          Width = 232
          Height = 223
          Align = alClient
          Caption = ' Query Prueba '
          TabOrder = 0
          object ScriptTest: TMemo
            Left = 2
            Top = 15
            Width = 228
            Height = 206
            Align = alClient
            ScrollBars = ssBoth
            TabOrder = 0
            OnChange = ScriptMasterChange
          end
        end
        object PanelPrueba: TPanel
          Left = 2
          Top = 15
          Width = 232
          Height = 93
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object AliasLBL: TLabel
            Left = 43
            Top = 8
            Width = 25
            Height = 13
            Caption = '&Alias:'
            FocusControl = Alias
          end
          object UsuarioLBL: TLabel
            Left = 29
            Top = 32
            Width = 39
            Height = 13
            Caption = '&Usuario:'
            FocusControl = Usuario
          end
          object PasswordLBL: TLabel
            Left = 19
            Top = 56
            Width = 49
            Height = 13
            Caption = '&Password:'
            FocusControl = Password
          end
          object Alias: TComboBox
            Left = 72
            Top = 4
            Width = 157
            Height = 21
            Style = csDropDownList
            ItemHeight = 13
            TabOrder = 0
            OnClick = PruebaChange
          end
          object Usuario: TEdit
            Left = 72
            Top = 28
            Width = 157
            Height = 21
            TabOrder = 1
            Text = 'SYSDBA'
            OnClick = PruebaChange
          end
          object Password: TEdit
            Left = 72
            Top = 52
            Width = 157
            Height = 21
            PasswordChar = '*'
            TabOrder = 2
            Text = 'm'
            OnClick = PruebaChange
          end
          object SameScript: TCheckBox
            Left = 2
            Top = 76
            Width = 83
            Height = 18
            Alignment = taLeftJustify
            Caption = 'Igualar Query:'
            TabOrder = 3
            OnClick = SameScriptClick
          end
        end
      end
      object DOSDatosGB: TGroupBox
        Left = 0
        Top = 0
        Width = 217
        Height = 333
        Align = alLeft
        Caption = ' Base de Datos Maestra '
        TabOrder = 1
        object ScriptMasterGB: TGroupBox
          Left = 2
          Top = 108
          Width = 213
          Height = 223
          Align = alClient
          Caption = ' Query Maestro '
          TabOrder = 0
          object ScriptMaster: TMemo
            Left = 2
            Top = 15
            Width = 209
            Height = 206
            Align = alClient
            ScrollBars = ssBoth
            TabOrder = 0
            OnChange = ScriptMasterChange
          end
        end
        object PanelMaster: TPanel
          Left = 2
          Top = 15
          Width = 213
          Height = 93
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object AliasMaestroLBL: TLabel
            Left = 26
            Top = 7
            Width = 25
            Height = 13
            Caption = '&Alias:'
            FocusControl = AliasMaestro
          end
          object UsuarioMaestroLBL: TLabel
            Left = 12
            Top = 31
            Width = 39
            Height = 13
            Caption = '&Usuario:'
            FocusControl = UsuarioMaestro
          end
          object PasswordMaestroLBL: TLabel
            Left = 2
            Top = 55
            Width = 49
            Height = 13
            Caption = '&Password:'
            FocusControl = PasswordMaestro
          end
          object AliasMaestro: TComboBox
            Left = 55
            Top = 3
            Width = 153
            Height = 21
            Style = csDropDownList
            ItemHeight = 13
            TabOrder = 0
            OnClick = MaestroChange
          end
          object UsuarioMaestro: TEdit
            Left = 55
            Top = 27
            Width = 153
            Height = 21
            TabOrder = 1
            Text = 'SYSDBA'
            OnChange = MaestroChange
          end
          object PasswordMaestro: TEdit
            Left = 55
            Top = 51
            Width = 153
            Height = 21
            PasswordChar = '*'
            TabOrder = 2
            Text = 'm'
            OnChange = MaestroChange
          end
        end
      end
    end
    object Comparacion: TTabSheet
      Caption = 'Comparacion'
      TabVisible = False
      object Splitter: TSplitter
        Left = 0
        Top = 152
        Width = 456
        Height = 3
        Cursor = crVSplit
        Align = alTop
      end
      object PanelResultados: TPanel
        Left = 0
        Top = 302
        Width = 456
        Height = 31
        Align = alBottom
        TabOrder = 0
        object Diferencias: TBitBtn
          Left = 176
          Top = 3
          Width = 88
          Height = 25
          Hint = 'Mostrar Campos Diferentes'
          Caption = '&Diferencias'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = DiferenciasClick
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000010000000000000000000
            BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777770000077
            777777700AAAAA007777770AAAAAAAAA077770AAAAAAAAAAA07770AA0AAAAA0A
            A0770AAA00AAA00AAA070AAAA00000AAAA070AAAAAAAAAAAAA070AAAAAAAAAAA
            AA070AAA00AAA00AAA0770AA00AAA00AA07770AAAAAAAAAAA077770AAAAAAAAA
            077777700AAAAA00777777777000007777777777777777777777}
        end
        object SiguienteDiferencia: TBitBtn
          Left = 268
          Top = 3
          Width = 88
          Height = 25
          Hint = 'Buscar Siguiente Diferencia'
          Caption = 'Si&guiente'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SiguienteDiferenciaClick
          Glyph.Data = {
            42010000424D4201000000000000760000002800000011000000110000000100
            040000000000CC00000000000000000000001000000010000000000000000000
            BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777700000007777477777777777700000007777447777777777700000007744
            4447777777777000000074774477777777777000000074774777000000007000
            0000747777777777777770000000747777777700000070000000747777777777
            7777700000007477777777000000700000007477777777777777700000007477
            7777770000007000000074777777777777777000000074777777000000007000
            0000747777777777777770000000774444477777777770000000777777777777
            777770000000}
        end
        object AnteriorDiferencia: TBitBtn
          Left = 83
          Top = 3
          Width = 88
          Height = 25
          Hint = 'Buscar Diferencia Anterior'
          Caption = 'Pre&via'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = AnteriorDiferenciaClick
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F000000000000000000000001000000000000000000000000000
            BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777700007FFFFFFFFFFFFFFFFFF700008777777777777777777F00008777
            777777777777777F00008777777777770777777F00008777777777700777777F
            00008777777777000777777F00008777777770000777777F0000877777770000
            0777777F00008777777000000777777F00008777777700000777777F00008777
            777770000777777F00008777777777000777777F00008777777777700777777F
            00008777777777770777777F00008777777777777777777F0000877777777777
            7777777F00007888888888888888888700007777777777777777777700007777
            77777777777777770000}
        end
      end
      object QueryMaestroGB: TGroupBox
        Left = 0
        Top = 0
        Width = 456
        Height = 152
        Align = alTop
        Caption = ' Query Maestro '
        TabOrder = 1
        object DBGridMaestro: TDBGrid
          Left = 2
          Top = 15
          Width = 452
          Height = 135
          Align = alClient
          DataSource = dsMaestro
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
      object QueryPruebaGB: TGroupBox
        Left = 0
        Top = 155
        Width = 456
        Height = 147
        Align = alClient
        Caption = ' Query Prueba '
        TabOrder = 2
        object DBGridPrueba: TDBGrid
          Left = 2
          Top = 15
          Width = 452
          Height = 130
          Align = alClient
          DataSource = dsPrueba
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 377
    Width = 464
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object dsMaestro: TDataSource
    Left = 224
    Top = 349
  end
  object dsPrueba: TDataSource
    Left = 192
    Top = 349
  end
end
