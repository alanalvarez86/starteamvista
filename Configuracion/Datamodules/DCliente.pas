unit DCliente;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,
     DBasicoCliente,
{$ifdef DOS_CAPAS}
{$endif}
     Login_TLB,
     ZetaClientDataSet,
     ZetaCommonLists,
     ZetaCommonClasses;

type
  TdmCliente = class(TBasicoCliente)
    cdsTPeriodos: TZetaLookupDataSet;
    cdsEmpleadoLookup: TZetaLookupDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsTPeriodosAlAdquirirDatos(Sender: TObject);
  private
    FArregloPeriodo: TStrings; //acl
    FEmpleado: TNumEmp;
    FFechaAsistencia: TDate;
    FPeriodoTipo: eTipoPeriodo;
    FPeriodoNumero: Integer;
    FIMSSPatron: String;
    FIMSSYear: Integer;
    FIMSSTipo: eTipoLiqIMSS;
    FIMSSMes: Integer;
    FMatsushita : Boolean;
    FEmpresaActiva : OleVariant;

    procedure CargaActivosSistema(Parametros: TZetaParams);
    { Private declarations }
  protected
  {$ifdef DOS_CAPAS}
      FServerGlobal: TdmServerGlobal;
      FServerReporteador: TdmServerReporting;
      FServerReportes : TdmServerReportes;
      FServerSistema: TdmServerSistema;
      FServerConsultas: TdmServerConsultas;
  {$endif}
  public

    property Empleado: TNumEmp read FEmpleado;
    property FechaAsistencia: TDate read FFechaAsistencia write FFechaAsistencia;
    property PeriodoTipo: eTipoPeriodo read FPeriodoTipo write FPeriodoTipo;
    property PeriodoNumero: Integer read FPeriodoNumero;
    property IMSSPatron: String read FIMSSPatron write FIMSSPatron;
    property IMSSYear: Integer read FIMSSYear write FIMSSYear;
    property IMSSTipo: eTipoLiqIMSS read FIMSSTipo write FIMSSTipo;
    property IMSSMes: Integer read FIMSSMes write FIMSSMes;
    property Matsushita: Boolean read FMatsushita;
    {$ifdef DOS_CAPAS}
    property ServerGlobal: TdmServerGlobal read FServerGlobal;
    property ServerReporteador: TdmServerReporting read FServerReporteador;
    property ServerReportes: TdmServerReportes read FServerReportes;
    property ServerSistema: TdmServerSistema read FServerSistema;
    property ServerConsultas: TdmServerConsultas read FServerConsultas;
    {$endif}
    //property EmpresaSeleccion: Variant read GetEmpresaSeleccion;

    { Public declarations }
    function BuscaCompany( const sCodigo: String ): Boolean;
    function Empresa: OleVariant;
    function GetValorActivoStr( const eTipo: TipoEstado ): String; override;
    function GetAuto: OleVariant;
    procedure CargaActivosTodos( Parametros: TZetaParams ); override;
    procedure LeeTiposNomina(Lista: TStrings; const lTodosPeriodos: Boolean = FALSE );//acl
    procedure InitArrayTPeriodo;
    function GetDatosPeriodoActivo: TDatosPeriodo;
     procedure GetEmpleadosBuscados(const sPaterno, sMaterno, sNombre, sRFC, sNSS, sBanca: String; const oEmpresa: OleVariant; const oDataSet: TZetaClientDataSet);
    procedure SetEmpresaActiva( EmpresaActiva : OleVariant );
  end;

var
  dmCliente: TdmCliente;

implementation

{$R *.DFM}

uses ZetaRegistryCliente,
     ZetaCommonTools;

procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
     inherited;
     InitClientRegistry;
     SetUsuario( 1 );
     FArregloPeriodo:= TStringList.Create;//acl
{$ifdef DOS_CAPAS}
{$endif}
end;

//acl
procedure TdmCliente.InitArrayTPeriodo;
begin
     { Llena el arreglo eTipoPeriodo y aTipoPeriodo }
     LeeTiposNomina( FArregloPeriodo, TRUE ); //acl
     LlenaTipoPeriodo( FArregloPeriodo ); //acl
end;

procedure TdmCliente.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil(FArregloPeriodo);  //acl
     ClearClientRegistry;
{$ifdef DOS_CAPAS}
{$endif}
     inherited;
end;

//acl
procedure TdmCliente.LeeTiposNomina( Lista: TStrings; const lTodosPeriodos: Boolean );
begin
     with Lista do
     begin
          BeginUpdate;
          Clear;
          with cdsTPeriodos do
          begin
               Refrescar;
               First;
               while ( not EOF ) do
               begin
                    if strLleno( FieldByName( 'TP_DESCRIP' ).AsString ) or ( lTodosPeriodos ) then
                       Add( FieldByName( 'TP_TIPO' ).AsString + '=' + FieldByName( 'TP_DESCRIP' ).AsString);
                    Next;
               end;
          end;
          EndUpdate;
     end;
end;


function TdmCliente.BuscaCompany( const sCodigo: String ): Boolean;
begin
     with cdsCompany do
     begin
          Data := Servidor.GetCompanys( 0, Ord( tc3Datos ) );
          Result := Locate( 'CM_CODIGO', sCodigo, [] );
     end;
end;

function TdmCliente.Empresa: OleVariant;
begin
     Result := FEmpresaActiva;
end;

function TdmCliente.GetAuto: OleVariant;
begin
     Result := Servidor.GetAuto;
end;

procedure TdmCliente.CargaActivosSistema( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddDate( 'FechaAsistencia', Date );
          AddDate( 'FechaDefault', Date );
          AddInteger( 'YearDefault', TheYear( Date ) );
          AddInteger( 'EmpleadoActivo', 0 );
          AddString( 'NombreUsuario', VACIO );
          AddString( 'CodigoEmpresa', VACIO );
     end;
end;

procedure TdmCliente.CargaActivosTodos(Parametros: TZetaParams);
begin
     CargaActivosSistema(Parametros);
end;

function TdmCliente.GetValorActivoStr(const eTipo: TipoEstado): String;
begin
end;

procedure TdmCliente.cdsTPeriodosAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     cdsTPeriodos.Data := Servidor.GetTipoPeriodo( Empresa );
end;

function TdmCliente.GetDatosPeriodoActivo: TDatosPeriodo;
begin
     ///
end;

procedure TdmCliente.GetEmpleadosBuscados(const sPaterno, sMaterno,
  sNombre, sRFC, sNSS, sBanca: String; const oEmpresa: OleVariant;
  const oDataSet: TZetaClientDataSet);
begin
//
end;


procedure TdmCliente.SetEmpresaActiva(EmpresaActiva: OleVariant);
var
sCM_CODIGO : string;
begin
     FEmpresaActiva := EmpresaActiva;
     sCM_CODIGO :=  EmpresaActiva[P_CODIGO] ;
end;

end.
