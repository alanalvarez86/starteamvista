unit DSistema;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,
     DBaseSistema,
     ZetaClientDataSet, ZetaServerDataSet;

type
  TdmSistema = class(TdmBaseSistema)
    cdsArbol: TZetaLookupDataSet;
    cdsEmpleadosPortal: TZetaLookupDataSet;
    cdsEmpresasPortal: TZetaLookupDataSet;
    cdsNivel0: TZetaLookupDataSet;
    procedure cdsUsuariosGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
    function CargaListaSupervisores( Lista: TStrings; MostrarActivos: Boolean  ): String;
    procedure DescargaListaSupervisores( Lista: TStrings );
    procedure CargaListaAreas(Lista: TStrings);
    procedure DescargaListaAreas(Lista: TStrings);
    function cdsAdicionalesAccesos: TZetaClientDataset;
    function cdsGruposAdic: TZetaClientDataset;

  end;

var
  dmSistema: TdmSistema;

implementation

{$R *.DFM}



procedure TdmSistema.cdsUsuariosGetRights(Sender: TZetaClientDataSet;
          const iRight: Integer; var lHasRights: Boolean);
begin
     //CV: Siempre es FALSO, porque en el caso de Labor, no se puede
     //editar la forma de usuarios. AL ponerlo como falso, desaparece el boton de
     //modificar del ZetaBusqueda.
     lHasRights := FALSE;
end;

function TdmSistema.CargaListaSupervisores(Lista: TStrings;  MostrarActivos: Boolean): String;
begin

end;
procedure TdmSistema.DescargaListaSupervisores(Lista: TStrings);
begin

end;



procedure TdmSistema.CargaListaAreas(Lista: TStrings);
begin

end;

procedure TdmSistema.DescargaListaAreas(Lista: TStrings);
begin

end;

function TdmSistema.cdsAdicionalesAccesos: TZetaClientDataset;
begin
     Result:=NIL;
end;

function TdmSistema.cdsGruposAdic: TZetaClientDataset;
begin
     Result:=NIL;
end;
end.
