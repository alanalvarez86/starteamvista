object dmBaseSistema: TdmBaseSistema
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  object cdsEmpresas: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    BeforeInsert = cdsEmpresasBeforeInsert
    BeforePost = cdsEmpresasBeforePost
    AfterDelete = cdsEmpresasAfterDelete
    OnNewRecord = cdsEmpresasNewRecord
    OnReconcileError = GenericReconcileError
    AlAdquirirDatos = cdsEmpresasAlAdquirirDatos
    AlEnviarDatos = cdsEmpresasAlEnviarDatos
    LookupName = 'Empresas'
    LookupDescriptionField = 'CM_NOMBRE'
    LookupKeyField = 'CM_CODIGO'
    OnGetRights = cdsEmpresasGetRights
    Left = 104
    Top = 8
  end
  object cdsGrupos: TZetaLookupDataSet
    Aggregates = <
      item
        Active = True
        AggregateName = 'MaxCodigo'
        Expression = 'Max( GR_CODIGO ) + 0'
        Visible = False
      end>
    AggregatesActive = True
    Params = <>
    BeforeInsert = cdsGruposBeforeInsert
    BeforePost = cdsGruposBeforePost
    AfterDelete = cdsGruposAfterDelete
    OnNewRecord = cdsGruposNewRecord
    OnReconcileError = GenericReconcileError
    AlAdquirirDatos = cdsGruposAlAdquirirDatos
    AlEnviarDatos = cdsGruposAlEnviarDatos
    UsaCache = True
    LookupName = 'Grupos de Usuarios'
    LookupDescriptionField = 'GR_DESCRIP'
    LookupKeyField = 'GR_CODIGO'
    OnGetRights = cdsGruposGetRights
    Left = 176
    Top = 8
  end
  object cdsUsuarios: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsUsuariosAfterOpen
    BeforePost = cdsUsuariosBeforePost
    AfterDelete = cdsUsuariosAfterDelete
    OnNewRecord = cdsUsuariosNewRecord
    OnReconcileError = cdsUsuariosReconcileError
    AlAdquirirDatos = cdsUsuariosAlAdquirirDatos
    AlEnviarDatos = cdsUsuariosAlEnviarDatos
    AlCrearCampos = cdsUsuariosAlCrearCampos
    AlModificar = cdsUsuariosAlModificar
    LookupName = 'Usuarios'
    LookupDescriptionField = 'US_NOMBRE'
    LookupKeyField = 'US_CODIGO'
    OnGetRights = cdsUsuariosGetRights
    Left = 24
    Top = 8
  end
  object cdsImpresoras: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterDelete = cdsImpresorasAfterDelete
    OnReconcileError = GenericReconcileError
    AlAdquirirDatos = cdsImpresorasAlAdquirirDatos
    AlEnviarDatos = cdsImpresorasAlEnviarDatos
    UsaCache = True
    Left = 104
    Top = 64
  end
  object cdsAccesosBase: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = GenericReconcileError
    AlEnviarDatos = cdsAccesosBaseAlEnviarDatos
    Left = 176
    Top = 64
  end
  object cdsEmpresasLookUp: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsEmpresasLookUpAlAdquirirDatos
    LookupName = 'Empresas'
    LookupDescriptionField = 'CM_NOMBRE'
    LookupKeyField = 'CM_CODIGO'
    OnGetRights = cdsEmpresasGetRights
    Left = 24
    Top = 64
  end
  object cdsSuscrip: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    BeforePost = cdsSuscripBeforePost
    OnNewRecord = cdsSuscripNewRecord
    OnReconcileError = GenericReconcileError
    AlAdquirirDatos = cdsSuscripAlAdquirirDatos
    AlEnviarDatos = cdsSuscripAlEnviarDatos
    AlCrearCampos = cdsSuscripAlCrearCampos
    Left = 248
    Top = 8
  end
  object cdsCopiarAccesos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 328
    Top = 8
  end
  object cdsGruposLookup: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsGruposLookupAlAdquirirDatos
    LookupName = 'Grupos de Usuarios'
    LookupDescriptionField = 'GR_DESCRIP'
    LookupKeyField = 'GR_CODIGO'
    Left = 264
    Top = 72
  end
  object cdsEmpresasAccesos: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsEmpresasAccesosAlAdquirirDatos
    AlCrearCampos = cdsEmpresasAccesosAlCrearCampos
    Left = 368
    Top = 72
  end
  object cdsDerechos: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 24
    Top = 136
  end
  object cdsUsuariosLookup: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    LookupName = 'Usuarios'
    LookupDescriptionField = 'US_NOMBRE'
    LookupKeyField = 'US_CODIGO'
    LookupActivoField = 'US_ACTIVO'
    OnGetRights = cdsUsuariosLookupGetRights
    Left = 432
    Top = 8
  end
  object cdsClasifiRepEmp: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'RC_ORDEN'
    Params = <>
    LookupName = 'Clasificaciones'
    LookupDescriptionField = 'RC_NOMBRE'
    LookupKeyField = 'RC_CODIGO'
    Left = 112
    Top = 136
  end
  object cdsRoles: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = GenericReconcileError
    Left = 216
    Top = 136
  end
  object cdsUserRoles: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    Left = 304
    Top = 136
  end
  object cdsGruposTodos: TZetaLookupDataSet
    Aggregates = <
      item
        Active = True
        AggregateName = 'MaxCodigo'
        Expression = 'Max( GR_CODIGO ) + 0'
        Visible = False
      end>
    AggregatesActive = True
    Params = <>
    BeforeInsert = cdsGruposBeforeInsert
    BeforePost = cdsGruposBeforePost
    AfterDelete = cdsGruposAfterDelete
    OnNewRecord = cdsGruposNewRecord
    OnReconcileError = GenericReconcileError
    AlAdquirirDatos = cdsGruposTodosAlAdquirirDatos
    AlEnviarDatos = cdsGruposAlEnviarDatos
    UsaCache = True
    LookupName = 'Grupos de Usuarios'
    LookupDescriptionField = 'GR_DESCRIP'
    LookupKeyField = 'GR_CODIGO'
    OnGetRights = cdsGruposGetRights
    Left = 24
    Top = 192
  end
end
