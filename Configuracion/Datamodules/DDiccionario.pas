unit DDiccionario;

interface

{$INCLUDE DEFINES.INC}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, DBClient,
{$ifdef DOS_CAPAS}
     {$ifdef RDDTRESSCFG}
        DServerReporteadorDD,
     {$else}
        DServerDiccionario,
     {$endif}
{$else}
     {$ifdef RDDTRESSCFG}
     ReporteadorDD_TLB,
     {$else}
     Diccionario_TLB,
     {$endif}
{$endif}
{$ifdef RDDTRESSCFG}
{$else}
     DEntidadesTress,
{$endif}
     ZetaTipoEntidad,
     ZetaTipoEntidadTools,
     ZetaCommonLists,
     ZetaClientDataSet;

const
     K_DICCION_SISTEMA = 10000;

type



  {$ifdef RDDTRESSCFG}
    ListaClasificaciones= integer;
    {$ifdef DOS_CAPAS}
       TdmServerDiccionario = TdmServerReporteadorDD;
    {$else}
       IdmServerDiccionarioDisp = IdmServerReporteadorDDDisp;
    {$endif}
  {$else}
  ListaClasificaciones= set of eClasifiReporte;
  {$endif}

  TObjetoEntidad = class( TObject )
  private
    { Private declarations }
  public
    { Public declarations }
    Entidad: TipoEntidad;
  end;
  TObjetoString = class( TObject )
  private
    { Private declarations }
  public
    { Public declarations }
    Campo: String[ 30 ];
    Entidad: TipoEntidad;
  end;

  //TClasifiReporte = Array[Ord(Low(eClasifiReporte)) .. Ord( High(eClasifiReporte) )] of eClasifiReporte;

  TdmDiccionario = class(TDataModule)
    cdsGlobal: TZetaClientDataSet;
    cdsBuscaPorCampo: TZetaClientDataSet;
    cdsBuscaPorTabla: TZetaClientDataSet;
    cdsDiccion: TZetaClientDataSet;
    cdsClasificaciones: TZetaLookupDataSet;
    cdsTablasPorClasificacion: TZetaClientDataSet;
    cdsRelaciones: TZetaClientDataSet;
    cdsCamposPorTabla: TZetaClientDataSet;
    cdsLookupGeneral: TZetaLookupDataSet;
    cdsClasifiDerechos: TZetaClientDataSet;
    cdsDatosDefault: TZetaClientDataSet;
    cdsListasFijas: TZetaLookupDataSet;
    cdsModulos: TZetaLookupDataSet;
    cdsListasFijasValores: TZetaClientDataSet;
    cdsFunciones: TZetaClientDataSet;
    cdsEntidades: TZetaLookupDataSet;
    cdsEntidadesLookup: TZetaLookupDataSet;
    cdsModulosLookup: TZetaLookupDataSet;
    cdsEntidadesPorModulo: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure cdsClasificacionesAlAdquirirDatos(Sender: TObject);
    procedure cdsClasifiDerechosAlAdquirirDatos(Sender: TObject);
    procedure cdsEntidadesAlAdquirirDatos(Sender: TObject);
    procedure cdsEntidadesPorModuloAlAdquirirDatos(Sender: TObject);
  private
    { Private declarations }
    lVerConfidencial: Boolean;
    lRevisaConfidencial: Boolean;
    lArregloVacio: Boolean;

{$ifdef DOS_CAPAS}
{$else}
    FServidor: IdmServerDiccionarioDisp;
    function GetServerDiccionario: IdmServerDiccionarioDisp;
{$endif}

{$ifdef RDDTRESSCFG}
{$else}
{$endif}
  protected
    { Protected declarations }

    sFiltroConfi: String;

    function GetConfidencial: Boolean;

{$ifdef DOS_CAPAS}
{$else}
    property ServerDiccionario: IdmServerDiccionarioDisp read GetServerDiccionario;
{$endif}


  protected
    FClasificacion : Variant;
  public
    { Public declarations }
    property VerConfidencial : Boolean read GetConfidencial;
    {$ifdef RDDTRESSCFG}
    procedure LlenaArregloEntidades;
    procedure   SetLookupNames;
    procedure CambioGlobales;
    {$endif}
  end;

var
  dmDiccionario: TdmDiccionario;

implementation

uses ZGlobalTress,
     ZAccesosMgr,
     ZAccesosTress,
     ZetaDialogo,
     ZetaCommonTools,
     ZetaCommonClasses,
     DCliente;

{$R *.DFM}

{ TdmBaseDiccionario }

procedure TdmDiccionario.cdsClasificacionesAlAdquirirDatos(
  Sender: TObject);
begin
{$ifdef RDDTRESSCFG}
     with dmCliente do
          cdsClasificaciones.Data := ServerDiccionario.GetListaClasifi( Empresa );
                                                                        //dmSistema.GetEmpresaAccesos
     {$else}
     {$endif}
end;

procedure TdmDiccionario.cdsClasifiDerechosAlAdquirirDatos(
  Sender: TObject);
begin
 {$ifdef RDDTRESSCFG}
     with dmCliente do
          cdsClasifiDerechos.Data := ServerDiccionario.GetDerechosClasifi(Empresa, GetGrupoActivo);
     {$else}
     {$endif}
end;

procedure TdmDiccionario.cdsEntidadesAlAdquirirDatos(Sender: TObject);
begin
      {$ifdef RDDTRESSCFG}
     //cdsEntidades.Data := ServerDiccionario.GetEntidades( dmSistema.GetEmpresaAccesos );
     cdsEntidades.Data := ServerDiccionario.GetEntidades( dmCliente.Empresa );
     {$else}
     {$endif}
     cdsEntidadesLookup.Data := cdsEntidades.Data;
end;

procedure TdmDiccionario.cdsEntidadesPorModuloAlAdquirirDatos(
  Sender: TObject);
begin
 //
end;

procedure TdmDiccionario.DataModuleCreate(Sender: TObject);
begin
     lVerConfidencial := FALSE;
     lRevisaConfidencial := FALSE;
     sFiltroConfi := '';
     lArregloVacio := TRUE;
end;

function TdmDiccionario.GetConfidencial: Boolean;
begin
     if NOT lRevisaConfidencial then
     begin
          lVerConfidencial :=  TRUE;
          lRevisaConfidencial := TRUE;
     end;
     Result := lVerConfidencial;
end;


function TdmDiccionario.GetServerDiccionario: IdmServerDiccionarioDisp;
begin
     Result := IdmServerDiccionarioDisp(dmCliente.CreaServidor( {$ifdef RDDTRESSCFG}CLASS_dmServerReporteadorDD{$else}CLASS_dmServerDiccionario{$endif}, FServidor ));
end;

procedure TdmDiccionario.LlenaArregloEntidades;
begin

end;



procedure TdmDiccionario.SetLookupNames;
begin

end;

procedure TdmDiccionario.CambioGlobales;
begin
     cdsDiccion.Refrescar;
     SetLookupNames;
end;


end.



