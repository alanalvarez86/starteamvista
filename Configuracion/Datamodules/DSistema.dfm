inherited dmSistema: TdmSistema
  OldCreateOrder = True
  Left = 246
  Top = 255
  inherited cdsGrupos: TZetaLookupDataSet
    Aggregates = <
      item
        Active = True
        Expression = 'Max( GR_CODIGO ) + 0'
        Visible = False
      end>
  end
  inherited cdsGruposTodos: TZetaLookupDataSet
    Aggregates = <
      item
        Active = True
        Expression = 'Max( GR_CODIGO ) + 0'
        Visible = False
      end>
  end
  object cdsArbol: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    Left = 328
    Top = 192
  end
  object cdsEmpleadosPortal: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    Left = 328
    Top = 248
  end
  object cdsEmpresasPortal: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    Left = 328
    Top = 304
  end
  object cdsNivel0: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    Left = 328
    Top = 376
  end
end
