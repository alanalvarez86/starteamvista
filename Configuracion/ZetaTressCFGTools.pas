unit ZetaTressCFGTools;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, ShellAPI, DB, DBTables;

const
     K_RENGLON = '< RENGLON NUEVO >';
     K_RENGLON_TITULO = '==============================';
     K_RENGLON_NUEVO = -2;
     {NOMBRE DE VARIABLES @ALGO DE SCRIPTS}
     ARROBA = '@';

     ARROBA_YEAR = '@YEAR';
     ARROBA_YEAR_MAS = '@YEAR_MAS';
     ARROBA_YEAR_MENOS = '@YEAR_MENOS';
     ARROBA_TIPO = '@TIPO';
     ARROBA_NUMERO = '@NUMERO';
     ARROBA_MES = '@MES';
     ARROBA_AC_MES = '@AC_MES';
     ARROBA_AC_MES_MAS = '@AC_MES_MAS';
     ARROBA_AC_MES_MENOS = '@AC_MES_MENOS';
     ARROBA_AC_BIMESTRE = '@AC_BIMESTRE';
     ARROBA_EMPLEADO = '@EMPLEADO';
     ARROBA_FECHA = '@FECHA';
     ARROBA_FECHA_SIST = '@FECHA_SIS';
     ARROBA_FECHA_INICIAL = '@INICIO';
     ARROBA_FECHA_INICIAL_MAS = '@INICIO_MAS';
     ARROBA_FECHA_FINAL = '@FIN';
     ARROBA_CONCEPTO = '@CONCEPTO';
     ARROBA_CONCEPTO_REDONDEO = '@REDONDEO';        //'@C_RED'//
     ARROBA_CONCEPTO_ISPT = '@ISPT';            //'@C_ISPT'//
     ARROBA_CONCEPTO_CREDITO = '@CREDITO';      //'@C_CREDITO'//
     ARROBA_CAMPOS = '@CAMPOS';
     ARROBA_CAMPO = '@CAMPO';
     ARROBA_OPERACION = '@OPERACION';
     ARROBA_STATUS = '@STATUS';
     ARROBA_INICIO = '@INICIO';
     ARROBA_FIN = '@FIN';
     ARROBA_USUARIO = '@USUARIO';
     ARROBA_DES_USUARIO = '@D_USUARIO';
     ARROBA_CONDICION = '@CONDICION';
     ARROBA_POSICION = '@POSICION';
     ARROBA_CODIGO = '@CODIGO';
//     ARROBA_TABLA = '@TABLA';

     ARROBA_IMSS = '@IMSS';
     ARROBA_IMSS_PATRON = '@PATRON';
     ARROBA_IMSS_YEAR = '@IMSS_YEAR';
     ARROBA_IMSS_MES = '@IMSS_MES';
     ARROBA_IMSS_TIPO = '@IMSS_TIPO';

     ARROBA_FECHA_ASIST = '@FEC_ASIST';

     ARROBA_REP_FILTRO = '@FILTRO';
     ARROBA_REP_CONDICION = '@REPORTECONDICION';
     ARROBA_REP_NOMBRE = '@NOMBREREPORTE';
     ARROBA_REP_TITULO = '@TITULO';
     ARROBA_REP_SUBTITULO = '@SUBTITULO';
     ARROBA_REP_DESCRIPCION = '@DESCRIPCION';
     ARROBA_REP_CODIGO = '@CODIGO';
     ARROBA_CONFIDENCIAL = '@CONFIDENCIAL';
     ARROBA_NIVEL_PROG   = '@NIVEL_PROG';

function SetFileNameDefaultPath( const sFile: String ): String;
function SetFileNamePath( const sPath, sFile: String ): String;
function CTOD( const sFecha: String ): TDate;
function STOD( const sFecha: String ): TDate;
function ExecuteFile( const FileName, Params, DefaultDir: String; ShowCmd: Integer ): THandle;
function UpperLower( const sValor: String ): String;
function YearsCompletos( const dInicial, dFinal: TDate; var lAniv: Boolean ): Currency;
procedure ParamAsInteger( const sName: String; iValue: Integer; Query: TQuery );
procedure ParamAsFloat( const sName: String; rValue: Extended; Query: TQuery );
procedure ParamAsDate( const sName: String; dValue: TDate; Query: TQuery );
procedure ParamAsString( const sName, sValue: String; Query: TQuery );
procedure SetInsertScript( Tabla: String; Source: TDataset; Target: TQuery );
procedure TransferDatasets( Source: TDataset; Target: TQuery );
procedure TransfiereDatasets( sTabla: String; qSource, qTarget: TQuery);

implementation

uses ZetaServerTools,
     ZetaCommonTools,
     ZetaCommonClasses;

function SetFileNamePath( const sPath, sFile: String ): String;
begin
     Result := sPath;
     if ( Result[ Length( Result ) ] <> '\' ) then
        Result := Result + '\';
     Result := Result + sFile;
end;

function SetFileNameDefaultPath( const sFile: String ): String;
begin
     Result := SetFileNamePath( ExtractFileDir( Application.ExeName ), sFile );
end;

function ExecuteFile( const FileName, Params, DefaultDir: String; ShowCmd: Integer ): THandle;
var
   zFileName, zParams, zDir: array[0..79] of Char;
begin
     Result := ShellExecute( Application.MainForm.Handle,
                             nil,
                             StrPCopy( zFileName, FileName ),
                             StrPCopy( zParams, Params ),
                             StrPCopy( zDir, DefaultDir ),
                             ShowCmd );
end;

function UpperLower( const sValor: String ): String;
var
   i: Integer;
   sUno: String;
   bValor: Boolean;
begin
     bValor := True;
     Result := '';
     for i := 1 to Length( sValor) do
     begin
          sUno := Copy( sValor, i, 1 );
          if bValor then
          begin
               Result := Result + UpperCase( sUno );
               bValor := False;
          end
          else
              Result := Result + LowerCase( sUno );
          if ( sUno = ' ' ) then
             bValor:= True;
     end;
end;

function STOD( const sFecha: String ): TDate;
begin
     Result := ZetaCommonTools.CodificaFecha( StrToIntDef( Copy( sFecha, 1, 4 ), 0 ),
                                              StrToIntDef( Copy( sFecha, 5, 2 ), 0 ),
                                              StrToIntDef( Copy( sFecha, 7, 2 ), 0 ) );
end;

function CTOD( const sFecha: String ): TDate;
var
   iYear: Word;
begin
     if ( Length( sFecha ) = 8 ) then
        iYear := StrToIntDef( Copy( sFecha, 7, 2 ), 0 )
     else
         iYear := StrToIntDef( Copy( sFecha, 7, 4 ), 0 );
     Result := ZetaCommonTools.CodificaFecha( iYear,
                                              StrToIntDef( Copy( sFecha, 4, 2 ), 0 ),
                                              StrToIntDef( Copy( sFecha, 1, 2 ), 0 ) );
end;

procedure ParamAsInteger( const sName: String; iValue: Integer; Query: TQuery );
var
   i: Word;
begin
     with Query do
     begin
          //cv
          if Params.Count > 0 then
          begin
               for i := 0 to ( Params.Count - 1 ) do
               begin
                    if ( AnsiCompareText( Params[ i ].Name, sName ) = 0 ) then
                       Params[ i ].AsInteger := iValue;
               end;
          end;
     end;
end;

procedure ParamAsFloat( const sName: String; rValue: Extended; Query: TQuery );
var
   i: Word;
begin
     with Query do
     begin
          for i := 0 to ( Params.Count - 1 ) do
          begin
               if ( AnsiCompareText( Params[ i ].Name, sName ) = 0 ) then
                  Params[ i ].AsFloat := rValue;
          end;
     end;
end;

procedure ParamAsDate( const sName: String; dValue: TDate; Query: TQuery );
var
   i: Word;
begin
     with Query do
     begin
          for i := 0 to ( Params.Count - 1 ) do
          begin
               if ( AnsiCompareText( Params[ i ].Name, sName ) = 0 ) then
                  Params[ i ].AsDate := dValue;
          end;
     end;
end;

procedure ParamAsString( const sName, sValue: String; Query: TQuery );
var
   i: Word;
begin
     with Query do
     begin
          for i := 0 to ( Params.Count - 1 ) do
          begin
               if ( AnsiCompareText( Params[ i ].Name, sName ) = 0 ) then
                  Params[ i ].AsString := sValue;
          end;
     end;
end;

function YearsCompletos( const dInicial, dFinal: TDate; var lAniv: Boolean ): Currency;
var
   iDayIni, iMonthIni, iYearIni: Word;
   iDayFin, iMonthFin, iYearFin: Word;
begin
     lAniv := False;
     if ( dFinal > dInicial ) then
     begin
          DecodeDate( dInicial, iYearIni, iMonthIni, iDayIni );
          DecodeDate( dFinal, iYearFin, iMonthFin, iDayFin );
          // Si coincide con en el aniversario
          lAniv :=( iMonthIni = iMonthFin ) and ( iDayIni = iDayFin );
          if lAniv then   // Resta de a�os enteros
             Result := iYearFin - iYearIni
          else  // Hay una parte proporcional
              Result := ( ( dFinal - dInicial ) + 1 ) / 365.25;
     end
     else
         Result := 0;
end;

function DefineTabla( sTabla: String; oFields: TField ): Boolean;
begin
     Result := False;
     with oFields do
     begin
          {$ifdef MSSQL}
          if NoEsCampoLLavePortal( oFields ) then
          begin
               if ( FieldKind = fkData ) then
               begin
                    if ( ( AnsiUpperCase( sTabla ) <> 'COLABORA' ) or ( FieldName <> K_PRETTYNAME ) ) then
                    begin
                         if ( ( AnsiUpperCase( sTabla ) <> 'KARDEX' ) or ( ( FieldName <> 'CB_TIP_REV' ) and ( FieldName <> 'CB_FEC_SAL' ) ) ) then
                              Result := True;
                    end;
               end;
          end;
          {$else}
          if ( FieldKind = fkData ) then
             Result := True;
          {$endif}
     end;
end;

procedure TransfiereDatasets( sTabla: String; qSource, qTarget: TQuery);
var
   i,j: Integer;
   //oParam: TParam;
begin
     //Checar ZetaTressCFGTools el m�todo SetInsertScript donde se valida que si es SQL no mande
     //el Prettyname
     {
     ZetaCommonTools.TransferDatasets( tqSource, tqTarget );
     }
     j := 0;
     with qSource do
     begin
          for i := 0 to ( FieldCount - 1 ) do
          begin
               //MA: Soluci�n #1 Protegiendote si la posicion de los campos cambian
               {oParam :=  tqTarget.Params.FindParam( Fields[i].FieldName );
               if ( oParam <> nil ) then
                  oParam.AssignField( Fields[ i ] );}
               //MA: Soluci�n #2 Solo te valida que el numero de parametros coincida con el numero de campos
               //pero no te protege si los campos cambiaran de posicion.
{              if ( tqTarget.ParamCount > i ) then
                  tqTarget.Params[ i ].AssignField( Fields[ i ] ); }
               if DefineTabla( sTabla, Fields[ i ] ) then
               begin
                    qTarget.Params[ j ].AssignField( Fields[ i ] );
                    j := j + 1;
               end;
          end;
     end;
end;


procedure SetInsertScript( Tabla: String; Source: TDataset; Target: TQuery );
var
   i: Integer;
   sScript, sCampos: String;
begin
     sScript := 'insert into ' + Tabla + ' ( ';
     sCampos := ' ) values ( ';
     with Source do
     begin
          for i := 0 to ( FieldCount - 1 ) do
          begin
               //with Fields[ i ] do
               //begin
                    if DefineTabla( Tabla, Fields[ i ] ) then
                    begin
                         sScript := sScript + Fields[ i ].FieldName + ',';
                         sCampos := sCampos + ':' + Fields[ i ].FieldName + ',';
                    end;
               //end;
          end;
     end;
     sScript := ZetaCommonTools.CortaUltimo( sScript ) + ZetaCommonTools.CortaUltimo( sCampos ) + ' )';
     with Target do
     begin
          Active := False;
          with SQL do
          begin
               Clear;
               Add( sScript );
          end;
     end;
end;

procedure TransferDatasets( Source: TDataset; Target: TQuery );
var
   i: Integer;
begin
     with Source do
     begin
          for i := 0 to ( FieldCount - 1 ) do
          begin
               with Fields[ i ] do
               begin
                    if NoEsCampoLLavePortal(Fields[i]) then
                    begin
                         if ( FieldKind = fkData ) then
                         begin
                              {$ifdef INTERBASE}
                              if ( Fields[ i ] is TBlobField ) and ( TBlobField( Fields[ i ] ).BlobSize = 0 ) then
                                 Target.ParamByName( FieldName ).AsBlob := TBlobData( '' )
                              else
                                  Target.ParamByName( FieldName ).AssignField( Fields[ i ] );
                              {$endif}
                              {$ifdef MSSQL}
                              if ( Fields[ i ] is TBlobField ) then
                                 Target.ParamByName( FieldName ).AsString := Fields[ i ].AsString
                              else
                                  Target.ParamByName( FieldName ).AssignField( Fields[ i ] );
                              {$endif}
                         end;
                    end;
               end;
          end;
     end;
     with Target do
     begin
          for i := 0 to ( Params.Count - 1 ) do
          begin
               with Params[ i ] do
               begin
                    if IsNull then
                    begin
                         case DataType of
                              ftString: AsString := '';
                              ftDate, ftTime, ftDateTime: AsDateTime := NullDateTime;
                              ftInteger, ftSmallInt, ftWord: AsInteger := 0;
                              ftFloat, ftCurrency: AsFloat := 0;
                              ftBlob: AsBlob := TBlobData( '' );
                              ftMemo: AsString := '';
                         end;
                    end;
               end;
          end;
     end;
end;

end.
