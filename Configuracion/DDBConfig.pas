unit DDBConfig;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, BDE, DB, DBTables, FileCtrl, DBClient,
     {$ifndef VER130}Variants,{$endif}
     ZetaLicenseMgr,
     ZetaAliasManager,
     ZetaRegistryServer,
     DZetaServerProvider,
     FAutoServer,
     DMigrar,
     ZetaCommonLists,
     ZetaLicenseClasses,
     {$ifdef MSSQL}
     DMigrarDatosIB,
     {$endif}
     ZetaServerDataSet;

const
     K_INTRBASE = 'INTRBASE';
{$ifdef MSSQL}
     K_MSSQL_COMPARTE_BLOB_SIZE = '200';
{$endif}

     K_200_ESPACIOS = '                                                                                                                                                                                                        ';

     Q_QUERY_EMPRESAS_ANTES = 'select CM_CODIGO, CM_NOMBRE,'+
                        ' CM_ALIAS, CM_USRNAME, CM_PASSWRD,'+
                        ' CM_EMPATE, CM_ACUMULA, CM_DATOS,'+
                        ' CM_NIVEL0, CM_USACAFE, CM_USACASE, CM_ACTIVOS,'+
                        ' CM_CONTROL, CM_DIGITO, CM_KCLASIFI, CM_CONTROL AS CM_CTRL_RL, '''+K_200_ESPACIOS+''' AS CM_CHKSUM'+
                        ' from COMPANY'+
                        ' order by CM_NOMBRE';

     Q_QUERY_EMPRESAS = 'select CM_CODIGO, CM_NOMBRE,'+
                        ' CM_ALIAS, CM_USRNAME, CM_PASSWRD,'+
                        ' CM_EMPATE, CM_ACUMULA, CM_DATOS,'+
                        ' CM_NIVEL0, CM_USACAFE, CM_USACASE, CM_ACTIVOS,'+
                        ' CM_CONTROL, CM_DIGITO, CM_KCLASIFI, CM_CTRL_RL, CM_CHKSUM'+
                        ' from COMPANY'+
                        ' order by CM_NOMBRE';

 K_QTY_DB_KEYS = 11;

 DB_KEYS : array[0..10] of string =
     ( 'GET_MINUTOS_LOG_S', 'FN_HORAS_MINUTOS', 'GET_TIMESTAMP_TRESS', 'GET_TRESS_AROUND' , 'SP_COMPARTE_INVESTIGA',
       'FN_JEFE_US_CODIGO', 'SP_HORAS_NT_COMPARTE', 'SP_AS_C',  'FN_CONFLICTO_EMPRESA' , 'FN_US_CODIGO_JEFE', 'GET_REPORTE_NIVEL');

{$ifdef MSSQL}
     Q_AGREGAR_CM_CTRL_RL = 'ALTER TABLE COMPANY ADD CM_CTRL_RL Referencia NULL';
     Q_AGREGAR_CM_CHKSUM  = 'ALTER TABLE COMPANY ADD CM_CHKSUM Accesos NULL';
     Q_INIT_CM_CTRL_RL_1  = 'UPDATE COMPANY SET CM_CTRL_RL = CM_CONTROL where CM_CTRL_RL is NULL or CM_CTRL_RL = ''''';
     Q_INIT_CM_CTRL_RL_2  = 'UPDATE COMPANY SET CM_CTRL_RL  = ''3PRUEBA'',  CM_CONTROL=''3DATOS'' where CM_CTRL_RL= ''3COMEXT''';
     Q_INIT_CM_CHKSUM_3   = 'UPDATE COMPANY SET CM_CHKSUM  = '''' where CM_CHKSUM is NULL';
     Q_CREATE_KEY         = 'create function %s() returns Accesos WITH ENCRYPTION as begin  return ''%s''  end ';

     Q_AGREGAR_CM_CTRL_RL_AFTER = 'ALTER TABLE COMPANY ADD CM_CTRL_RL Referencia NOT NULL';
     Q_AGREGAR_CM_CHKSUM_AFTER  = 'ALTER TABLE COMPANY ADD CM_CHKSUM Accesos NOT NULL';

     Q_CREATE_KEY_REPORTE_CRYPT  = 'D0598A95B37AEF63828E91B550FC3E9CCFA492BA4ED76FE607253CE942E055DB609592F16BC674A75B92CE9DAB62AB54C174C6DF73D832FA2ED90925A8BB49C1598786B74B5B081424151C46CE4FA2'+
                            '87AB82BB44E97DCF01076CB770967AAD9F98719CC580E15883BE0519CE97B998C553CD3FFE18202A0B513F0D265C8593659B74A795C95F85CA7F85A346C13BFA1B16081A030A0B62B24D0C583938E4'+
                            '1909422910133DC96A8494A289C963E940D173AAB992C28FD9984EE166A850C78AEB2AC96BA99CA178ADBD6AEE105FDF62C97287AC453F580B5FB175D486E376D44D88946CCD71D4A9C87C8EA89B9D'+
                            'BE9BE923307202193FC97ABE1D4E25F03EEC61ED618597A090B0B4728396D357FD2B10141D350F085B2FE12504563321BF5A9B7889E85C393245FB002A1323301567A0B3F77887AD5888A827341F46'+
                            'C76BA08B80F539D351CE55D26390E147DD4AF57BE14F2A7AF0110E432A3035F16B92BC501D1B412D2B44E16CD496F762CEBCF58D4BC9538981AA9888DB0B14EE5CD56390E340C66EC254D25DEE0825'+
                            '300C32C76BF76BA3BF94E423C892FF728283A767A14F151765BC63F33DEF0350350700531759A64DBB74B413402B32FB1FF466FE7DAFB28EB9BF65FF0150E07BC67D85AE93EE3F3BEF2F220B51D411'+
                            '4AB352C78A86DC41DA1D1810200C72E84C2777F3B753D50BFA28C4D487EC28E7163ED477E65CE04AE67BA0AB5281B74CE05CDC55EC60B892F97A8EB5AF92E0110C3ECD5E8BD14A386FCC54DA7286B3'+
                            '9FED3E147FF1974BCA7ABD001D76EF7DA079AD89EC4EE651C968D1BCB7E783A777FA62F87CB812FB2AEF7DFB66C85E9D98A791BEA4BE9B9686FC4DC641F410C764B4A78C8986B4989BBB2B1D270C3F'+
                            'CA6EF41B5ACA5BD14FE05CEA7EAEA585DC88B06DC8A2F240E536C166A88D9888DC69CF4DD867CBB9E960CBADDB0F0EC76FD13318410C31E91C186ECF64C07999A6B0A6DF54E831161A1A1A10095A31'+
                            '43D87F89BB7CDA89FB5AF8580267D5C596FD67EA4A1075C7D784EF69E4441E7BC9D98AE17BF6560C69DBCB42FB19';
{
     Q_CREATE_KEY_REPORTE = 'create function  GET_REPORTE_NIVEL( @nivel Codigo )  returns Formula  WITH ENCRYPTION '+
                            'as '+
                            'begin '+
                            '	if ( @nivel = ''PLANTA''  ) '+
                            '		return dbo.GET_MINUTOS_LOG_S() '+
                            '	if ( @nivel = ''DEPTO''  ) '+
                            '		return  dbo.FN_HORAS_MINUTOS() '+
                            '	if ( @nivel = ''OFICNA''  ) '+
                            '		return  dbo.GET_TIMESTAMP_TRESS() '+
                            '	if ( @nivel = ''CCOSTO''  ) '+
                            '		return   dbo.GET_TRESS_AROUND () '+
                            '	if ( @nivel = ''LINEA''  ) '+
                            '		return   dbo.SP_COMPARTE_INVESTIGA() '+
                            '	if ( @nivel = ''SUPER''  ) '+
                            '		return   dbo.FN_JEFE_US_CODIGO() '+
                            '	if ( @nivel = ''GRNCIA''  ) '+
                            '		return   dbo.SP_HORAS_NT_COMPARTE() '+
                            '	if ( @nivel = ''PRODUC''  ) '+
                            '		return   dbo.SP_AS_C() '+
                            '	if ( @nivel = ''CONTAB''  ) '+
                            '		return   dbo.FN_CONFLICTO_EMPRESA () '+
                            '	if ( @nivel = ''EMPLD''  ) '+
                            '		return   dbo.FN_US_CODIGO_JEFE()  '+
                            '	return '''';                                '+
                            'end';
 }
     Q_DROP_KEY = 'drop function %s';
     Q_GET_KEY_REPORTE = 'select VALOR=dbo.%s(%s)';
     DB_KEYS_REPORTE : array[0..9] of string = ( 'PLANTA', 'DEPTO', 'OFICNA', 'CCOSTO' , 'LINEA',  'SUPER', 'GRNCIA', 'PRODUC',  'CONTAB' , 'EMPLD');
     Q_EXISTE_KEY = 'select count(*) as CUANTOS from sysobjects where id = object_id(N''%s'') and xtype in (N''FN'', N''IF'', N''TF'')';
     N_EXISTE_KEY_REPORTE = 10 ;
     Q_EXISTE_KEY_REPORTE = 'select count(*) as CUANTOS from sysobjects where id = object_id(N''GET_REPORTE_NIVEL'') and xtype in (N''FN'', N''IF'', N''TF'')';

{$endif}
{$ifdef INTERBASE}
     Q_AGREGAR_CM_CTRL_RL = 'ALTER TABLE COMPANY ADD CM_CTRL_RL Referencia;';
     Q_AGREGAR_CM_CHKSUM  = 'ALTER TABLE COMPANY ADD CM_CHKSUM Accesos;';
     Q_INIT_CM_CTRL_RL_1  = 'UPDATE COMPANY SET CM_CTRL_RL = CM_CONTROL where CM_CTRL_RL is NULL or CM_CTRL_RL = '''' ;';
     Q_INIT_CM_CTRL_RL_2  = 'UPDATE COMPANY SET CM_CTRL_RL  = ''3PRUEBA'',  CM_CONTROL=''3DATOS'' where CM_CTRL_RL= ''3COMEXT'';';
     Q_CREATE_KEY = 'create procedure %s returns ( Valor varchar(255) ) as begin  Valor = ''%s'';  SUSPEND; end;';

//     Q_CREATE_KEY = 'CREATE PROCEDURE %s RETURNS (  VALOR VARCHAR(255) ) AS BEGIN VALOR = ''%s''; suspend; END';


     Q_DROP_KEY = 'drop procedure %s;';
     Q_GET_KEY = 'select Valor as VALOR from %s';
     Q_EXISTE_KEY = 'select count(*) as CUANTOS from rdb$procedures  where 	rdb$procedure_name = ''%s''';
{$endif}

     Q_CUENTA_CONFIG_BD_PROD = 'SELECT count(*) AS CUANTOS  from COMPANY where UPPER(CM_DATOS) = UPPER( ''%s'' ) and ( CM_CONTROL = ''3DATOS''  or CM_CONTROL = '''' )  and ( CM_CTRL_RL = ''3DATOS'' or CM_CTRL_RL = ''''  ) ';
     Q_VALIDA_CONFIG_BD_PROD = 'SELECT CM_CODIGO, CM_CONTROL, CM_DATOS, CM_CTRL_RL, CM_CHKSUM  from COMPANY where UPPER(CM_DATOS) = UPPER( ''%s'' ) and ( CM_CONTROL = ''3DATOS''  or CM_CONTROL = '''' )  and ( CM_CTRL_RL = ''3DATOS'' or CM_CTRL_RL = '''' ) ';
     Q_CUENTA_DUPLICIDAD_BD = 'SELECT count(*) AS CUANTOS  from COMPANY where UPPER(CM_DATOS) = UPPER( ''%s'' ) and CM_CONTROL in (%s) and ( CM_CTRL_RL not in (%s) ) and CM_CODIGO <> ''%s'' ';
type
  TCallBack = procedure( const sMsg: String ) of object;
  TCallBackError = procedure( const sMsg: String; Error: Exception ) of object;
  TdmDBConfig = class(TDataModule, IDBConfigStoreHandler)
    dbEmpresas: TDatabase;
    tqList: TQuery;
    tqCompany: TQuery;
    upCompany: TUpdateSQL;
    tqCompanyCM_CODIGO: TStringField;
    tqCompanyCM_NOMBRE: TStringField;
    tqCompanyCM_ALIAS: TStringField;
    tqCompanyCM_USRNAME: TStringField;
    tqCompanyCM_PASSWRD: TStringField;
    tqCompanyCM_EMPATE: TIntegerField;
    tqCompanyCM_ACUMULA: TStringField;
    tqCompanyCM_DATOS: TStringField;
    tqCompanyCM_NIVEL0: TStringField;
    tqCompanyCM_USACAFE: TStringField;
    tqCompanyCM_ACTIVOS: TIntegerField;
    tqCompanyCM_TIPO_E: TStringField;
    tqCompanyCM_CONTROL: TStringField;
    tqCompanyCM_TIPO: TSmallintField;
    tqCompanyCM_DIGITO: TStringField;
    cdsEmpresas: TServerDataSet;
    tqCompanyCM_KCLASIFI: TSmallintField;
    dbEmpresa: TDatabase;
    cdsEmpresasActualizar: TServerDataSet;
    tqSPPatch: TQuery;
    tqSPPatchSE_TIPO: TIntegerField;
    tqSPPatchSE_SEQ_NUM: TIntegerField;
    tqSPPatchSE_APLICA: TStringField;
    tqSPPatchSE_NOMBRE: TStringField;
    tqSPPatchSE_DESCRIP: TStringField;
    upSPPatch: TUpdateSQL;
    tqActualizaBlob: TQuery;
    tqSPPatchSE_DATA: TMemoField;
    tqSPPatchToApply: TQuery;
    tqSPPatchSE_ARCHIVO: TStringField;
    tqCompanyCM_CTRL_RL: TStringField;
    tqCompanyCM_CHKSUM: TStringField;
    tqExisteChecksum: TQuery;
    tqActualizaChecksum: TQuery;
    tqScriptComparteAux: TQuery;
    tqValidaDuplicidadBD: TQuery;
    tqCompanyCM_USACASE: TStringField;
    procedure cdsEmpresasAfterOpen(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure tqCompanyAfterOpen(DataSet: TDataSet);
    procedure tqCompanyUpdateRecord(DataSet: TDataSet; UpdateKind: TUpdateKind; var UpdateAction: TUpdateAction);
    procedure tqCompanyNewRecord(DataSet: TDataSet);
    procedure tqCompanyCalcFields(DataSet: TDataSet);
    procedure tqCompanyBeforeInsert(DataSet: TDataSet);
    procedure tqCompanyBeforePost(DataSet: TDataSet);
    procedure cdsEmpresasActualizarAfterOpen(DataSet: TDataSet);
    procedure tqSPPatchUpdateRecord(DataSet: TDataSet;
      UpdateKind: TUpdateKind; var UpdateAction: TUpdateAction);
    procedure tqSPPatchNewRecord(DataSet: TDataSet);
    procedure tqSPPatchAfterOpen(DataSet: TDataSet);
    procedure tqActualizaBlobUpdateError(DataSet: TDataSet;
      E: EDatabaseError; UpdateKind: TUpdateKind;
      var UpdateAction: TUpdateAction);
  private
    { Private declarations }
    FAutoServer : TAutoServer;
    FAliasManager: TZetaAliasManager;
    FRegistry: TZetaRegistryServer;
    FGlobalLicense  : TGlobalLicenseValues;
    FDBConfigMgr    : TDbConfigManager;
    FTipos: TStrings;
    FNombres: TStrings;
    FMaxDigito: Char;
    FInicializaComparte: Boolean;
    FInsert : Boolean;
    FAcumula : Boolean;
    FEsCheckSumInicial : Boolean;
    procedure ConectaBaseDeDatos(DataBase: TDatabase; const sAlias, sUserName, sPassword: String);
    procedure ConectaComparte( const sAlias, sUserName, sPassword: String );
    procedure DecodePassword(Sender: TField; var Text: string; DisplayText: Boolean);
    procedure DesConectaBaseDeDatos(DataBase: TDatabase);
    procedure EncodePassword(Sender: TField; const Text: String );
    procedure MaskNumerico(const Campo: String);
    procedure CM_ACTUALIZARGetText(Sender: TField; var Text: string; DisplayText: Boolean);
    procedure CM_VERSIONGetText(Sender: TField; var Text: string; DisplayText: Boolean);
    procedure OnChangeCM_CTRL_RL(Sender: TField);

    procedure tqSPPatch_SE_APLICA_GetText(Sender: TField; var Text: string; DisplayText: Boolean);
    procedure tqSPPatch_SE_TIPOGetText(Sender: TField; var Text: String; DisplayText: Boolean );
    procedure tqSPPatch_SE_TIPOOnChange(Sender: TField);



  public
    { Public declarations }
    property AutoServer : TAutoServer read FAutoServer;
    property AliasManager: TZetaAliasManager read FAliasManager;
    property Registry: TZetaRegistryServer read FRegistry;
    property InicializaComparte: Boolean read FInicializaComparte write FInicializaComparte;
    function BuildEmpresa(DataSet: TDataSet): OleVariant;
    function GetEmpresaActualizar : OleVariant;

    function Connected: Boolean;
    function DesbloqueaAdministradores: Boolean;
    function GetCompany: String;
    function GetCompanyAlias: String;
    function GetCompanyDatabase: String;
    function GetCompanyName: String;
    function GetCompanyPassword: String;
    function GetCompanyUserName: String;
    function GetDigito: string;
    function CheckRegistry: Boolean;
    function GetTipoCompanyName( const sValor: String ): eTipoCompany;
    function GetTipoEmpresa: String;
    function GetTipoEmpresaEdit: String;
    function GetTipoEmpresaReal : eTipoCompany;
    function LlenaListaCompanias( Lista: TStrings ): Boolean;
    function LlenaListaCompaniasFuente( Lista: TCompanyList; lPresupuesto : Boolean  = FALSE  ): Integer;
    function LlenaListaCompaniasAplica( Lista: TStrings; const tcTipo : eTipoCompany  ): Boolean;
    function UsuariosContar: Integer;

    function GetDBConfigMgr : TDbConfigManager;

    procedure SetAutoServer( value : TAutoServer );

    procedure AbrirCompany;
    procedure CerrarCompany;
    procedure RefrescarCompany;

    procedure CambiaTipo(const sValor: String);
    procedure ConectarComparte;
    procedure ConectarCompany( DataSource: TDataSource );
    procedure DesconectaComparte;
    procedure DesconectarCompany( DataSource: TDataSource );
    procedure EmpresasConectar( const lTodas: Boolean  ; const eTipo  : eTipoCompany = tc3Datos);
    procedure EmpresasActualizarConectar( const lTodas: Boolean; const tcTipo : eTipoCompany  );
    procedure AgregaCompany;
    procedure BorraCompany;
    procedure ModificaCompany;
    procedure GrabaCompany;
    procedure CancelaCambiosCompany;

    procedure LlenaListaAlias( Lista: TStrings );
    procedure LlenaListaConsolidadas( Lista: TCompanyList );
    procedure LlenaListaNivel0( Lista: TStrings );
    procedure LeeListaNivel0(Lista: TStrings);
    procedure SetCompanyPassword( const Value: String );
    procedure SetEditMode;
    procedure LicenciasLeer( Manager: TLicenseMgr; Provider: TdmZetaServerProvider; Respuesta: TCallBack; RespuestaError: TCallBackError );
    function SeleccionLeer( Manager: TLicenseMgr; Provider: TdmZetaServerProvider; Respuesta: TCallBack; RespuestaError: TCallBackError ) : TDatosSeleccion;
    function VisitantesLeer( Manager: TLicenseMgr; Provider: TdmZetaServerProvider; Respuesta: TCallBack; RespuestaError: TCallBackError ) : TDatosVisitantes;
    procedure EscribeAUTOMOD( oAutoServer: TAutoServer );

    function GrabaSPPatch : boolean;
    function BorraSPPatch : boolean;
    function AgregaSPPatch : boolean;
    function ConsultarSPPatch(iSE_TIPO: integer; sCM_CODIGO : string ) : boolean;
    function CuentaSPPatch(iSE_TIPO: integer; sCM_CODIGO : string ) : integer;
    function ExisteChecksum : boolean;
    function SetChecksum( lPuedeCorregir : Boolean; var lCorrigio : Boolean ) : boolean ;
    function EsValidoOldChecksum : Boolean;
    function ExisteKeyEnBD( iKey : integer ) : Boolean;
    function ExistenKeys : Boolean;
    function EjecutaScriptComparte(sScript : string ) : boolean ;

    function CrearEstructurasChecksum : boolean;
    function ActualizarCheckSumTodasCompanys : boolean;
    function EsDbConfigInicial : boolean;
    function ReadDBConfigFromDatabase( iKey : integer) : string;
    function WriteDBConfigFromDatabase( iKey : integer; encryptedValue : string ) : boolean;
    function ReadRegistryComparteBD : string;
    function ReadRegistryChecksum : string;
    function WriteRegistryChecksum( encryptedValue : string ) : boolean;

    function ValidarAltaBDProduccion : boolean;
    function IncrementarAltaBDProduccion : boolean;
    function ContarDuplicidadBD : integer;
    function ContarConfiguracionBDProduccion: integer;

  end;

var
  dmDBConfig: TdmDBConfig;

function GetCommandLineParameter( const iParameter: Integer ): String;
procedure ActivaSesion;

implementation

uses ZetaTressCFGTools,
     ZetaCommonTools,
     ZetaServerTools,
     ZetaCommonClasses,
     ZAccesosTress,
     ZetaDataSetTools,
     ZetaDialogo,
     FAutoClasses;

const
     K_ANCHO_ULTIMA_NOMINA = 40;

{$R *.DFM}

function GetCommandLineParameter( const iParameter: Integer ): String;
begin
     if ( ParamCount() < iParameter ) then
        Result := VACIO
     else
         Result := ParamStr( iParameter );
end;

procedure ActivaSesion;
var
   sFolder: String;
begin
     with Session do
     begin
          if not Active then
          begin
               sFolder := GetCommandLineParameter( 1 );
               if ( sFolder <> VACIO ) and FileCtrl.DirectoryExists( sFolder ) then
                  PrivateDir := sFolder;
               NetFileDir := ZetaTressCFGTools.SetFileNameDefaultPath( VACIO );
               Active := True;
          end;
     end;
end;

{ ******** TdmDBConfig ********** }

procedure TdmDBConfig.DataModuleCreate(Sender: TObject);
var
   eCiclo: eTipoCompany;
begin
     ActivaSesion;
     FAliasManager := TZetaAliasManager.Create;
     FRegistry := TZetaRegistryServer.Create;
     FTipos := TStringList.Create;
     FNombres := TStringList.Create;
     FInicializaComparte:= FALSE;
     FEsCheckSumInicial := TRUE;

     FGlobalLicense := TGlobalLicenseValues.Create( Self.FAutoServer );
     FGlobalLicense.FechaActual := Date;
     FDBConfigMgr := TDbConfigManager.Create(FGlobalLicense, IDBConfigStoreHandler( Self )  );

     
     {$ifdef INTERBASE}
     dmMigrar := TdmMigrar(Self);
     {$endif}
     for eCiclo := Low( eTipoCompany ) to High( eTipoCompany ) do
     begin
          FTipos.Add( Format( '%s=%s', [ ZetaCommonLists.ObtieneElemento( lfTipoCompany, Ord( eCiclo ) ), ZetaCommonLists.ObtieneElemento( lfTipoCompanyName, Ord( eCiclo ) ) ] ) );
          FNombres.Add( Format( '%s=%s', [ AnsiUpperCase( ZetaCommonLists.ObtieneElemento( lfTipoCompanyName, Ord( eCiclo ) ) ), ZetaCommonLists.ObtieneElemento( lfTipoCompany, Ord( eCiclo ) ) ] ) );
     end;
end;

procedure TdmDBConfig.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FNombres );
     FreeAndNil( FTipos );
     FRegistry.Free;
     FAliasManager.Free;
     
end;

procedure TdmDBConfig.MaskNumerico( const Campo: String );
var
   oCampo: TField;
begin
     with dmDBConfig.cdsEmpresas do
     begin
          oCampo := FindField( Campo );
          if ( oCampo <> nil ) and ( oCampo is TNumericField ) then
          begin
               TNumericField( oCampo ).DisplayFormat := '#,0';
          end;
     end;
end;

function TdmDBConfig.Connected: Boolean;
begin
     Result := dbEmpresas.Connected;
end;

procedure TdmDBConfig.ConectaBaseDeDatos( DataBase: TDatabase; const sAlias, sUserName, sPassword: String );
begin
     with Database do
     begin
          Connected := False;
          AliasName := sAlias;
          {
          AliasName := VACIO;
          DriverName := K_INTRBASE;
          }
          with Params do
          begin
               {
               Values[ 'SERVER NAME' ] := sDatabase;
               }
               Values[ 'USER NAME' ] := sUserName;
               Values[ 'PASSWORD' ] := sPassword;
          end;
          Connected := True;
          Session.NetFileDir := ZetaTressCFGTools.SetFileNameDefaultPath( VACIO ); { Para Manejar Cached Updates en Paradox }
     end;
end;

procedure TdmDBConfig.DesConectaBaseDeDatos( DataBase: TDatabase );
begin
     with Database do
     begin
          Connected := False;
          AliasName := VACIO;
          DriverName := VACIO;
          with Params do
          begin
               Values[ 'SERVER NAME' ] := VACIO;
               Values[ 'USER NAME' ] := VACIO;
               Values[ 'PASSWORD' ] := VACIO;
          end;
     end;
end;

procedure TdmDBConfig.ConectarComparte;
begin
     {
     Shutdown the database using gbak utility with "force:" option.
     All the user currently connected will be out of connection.
     Then connect to database yourself.
     Shutdown database form IB server manager using "Deny new connections"
     option, what will protect you from new connections.
     Then do your metadat update.
     Then bring database back online with IB server manager.
     }
     DesconectaComparte;
     with Registry do
     begin
          if IsValid then
             ConectaComparte( AliasComparte, UserName, Password )
          else
              raise Exception.Create( 'Esta Estaci�n No Ha Sido Inicializada' )
     end;
end;

procedure TdmDBConfig.ConectaComparte( const sAlias, sUserName, sPassword: String );
begin
     {$ifdef MSSQL}
     dbEmpresas.Params.Values[ 'BLOB SIZE' ] := K_MSSQL_COMPARTE_BLOB_SIZE;
     {$endif}
     ConectaBaseDeDatos( dbEmpresas, sAlias, sUserName, sPassword );
end;

procedure TdmDBConfig.DesconectaComparte;
begin
     DesconectaBaseDeDatos( dbEmpresas );
end;

procedure TdmDBConfig.LlenaListaAlias( Lista: TStrings );
begin
     AliasManager.PrepareAliasList( Lista );
end;

function TdmDBConfig.LlenaListaCompanias( Lista: TStrings ): Boolean;
begin
     with Lista do
     begin
          try
             BeginUpdate;
             Clear;
             with tqList do
             begin
                  try
                     Active := False;
                     with SQL do
                     begin
                          Clear;
                          Add( 'select CM_CODIGO, CM_NOMBRE from COMPANY order by CM_CODIGO' );
                     end;
                     Active := True;
                     while not Eof do
                     begin
                          Add( FieldByName( 'CM_CODIGO' ).AsString + '=' + FieldByName( 'CM_NOMBRE' ).AsString );
                          Next;
                     end;
                  finally
                         Active := False;
                  end;
             end;
             Add( '   =No Acumular' );
          finally
                 EndUpdate;
          end;
          Result := ( Count > 0 );
     end;
end;

function TdmDBConfig.LlenaListaCompaniasAplica( Lista: TStrings; const tcTipo : eTipoCompany  ): Boolean;
var
   sCM_CONTROL : string;
begin
     with Lista do
     begin
          try
             BeginUpdate;
             Clear;
             with tqList do
             begin
                  try
                     Active := False;
                     with SQL do
                     begin
                          Clear;
                          sCM_CONTROL := ZetaCommonLists.ObtieneElemento( lfTipoCompany, Ord( tcTipo ) );
                          Add( Format(  'select CM_CODIGO, CM_NOMBRE from COMPANY where CM_NIVEL0='''' and CM_CONTROL=%s order by CM_CODIGO',[ EntreComillas( sCM_CONTROL )]) );
                     end;
                     Active := True;
                     while not Eof do
                     begin
                          Add( FieldByName( 'CM_CODIGO' ).AsString + '=' + FieldByName( 'CM_NOMBRE' ).AsString );
                          Next;
                     end;
                  finally
                         Active := False;
                  end;
             end;
             Add( '   =Todas' );
          finally
                 EndUpdate;
          end;
          Result := ( Count > 0 );
     end;
end;

function TdmDBConfig.LlenaListaCompaniasFuente( Lista: TCompanyList ; lPresupuesto : Boolean ): Integer;
begin
     Result := -1;
     Lista.Clear;
     with tqList do
     begin
          try
             Active := False;
             with SQL do
             begin
                  Clear;
                  Add( 'select CM_CODIGO, CM_NOMBRE, CM_ALIAS, CM_USRNAME, CM_PASSWRD, CM_EMPATE, CM_DATOS' );

                  if ( not lPresupuesto ) then
                      Add( Format( 'from COMPANY where ( %s ) order by CM_CODIGO', [ ZetaServerTools.GetTipoCompany( Ord( tc3Datos ) ) ] ) )
                  else
                      Add( Format( 'from COMPANY where ( %s ) order by CM_CODIGO', [ ZetaServerTools.GetTipoCompanyMasPresupuestos ] ) )

             end;
             Active := True;
             while not Eof do
             begin
                  with Lista.AgregaEmpresa do
                  begin
                       Codigo := FieldByName( 'CM_CODIGO' ).AsString;
                       Nombre := FieldByName( 'CM_NOMBRE' ).AsString;
                       Alias := FieldByName( 'CM_ALIAS' ).AsString;
                       Datos := FieldByName( 'CM_DATOS' ).AsString;
                       UserName := FieldByName( 'CM_USRNAME' ).AsString;
                       Password := ZetaServerTools.Decrypt( FieldByName( 'CM_PASSWRD' ).AsString );
                       Empate := FieldByName( 'CM_EMPATE' ).AsInteger;
                       if ( GetCompany = Codigo ) then
                          Result := Lista.Count - 1;
                  end;
                  Next;
             end;
          finally
                 Active := False;
          end;
     end;
end;

procedure TdmDBConfig.LeeListaNivel0( Lista: TStrings );
begin
     with Lista do
     begin
          try
             BeginUpdate;
             Clear;
             with tqList do
             begin
                  try
                     Active := False;
                     with SQL do
                     begin
                          Clear;
                          Add( 'select TB_CODIGO, TB_ELEMENT from NIVEL0 order by TB_CODIGO' );
                     end;
                     Active := True;
                     while not Eof do
                     begin
                          Add( FieldByName( 'TB_CODIGO' ).AsString + '=' + FieldByName( 'TB_ELEMENT' ).AsString );
                          Next;
                     end;
                  finally
                         Active := False;
                  end;
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TdmDBConfig.LlenaListaNivel0( Lista: TStrings );
begin
     LeeListaNivel0( Lista );
     with Lista do
     begin
          try
             BeginUpdate;
             Add( '   =Sin Confidencialidad' );
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TdmDBConfig.LlenaListaConsolidadas( Lista: TCompanyList );
begin
     Lista.Clear;
     with tqList do
     begin
          try
             Active := False;
             with SQL do
             begin
                  Clear;
                  Add( 'select CM_CODIGO, CM_NOMBRE, CM_ALIAS, CM_USRNAME, CM_PASSWRD,' );
                  Add( 'CM_EMPATE, CM_DATOS from COMPANY where ');
                  Add( Format( '( CM_ACUMULA = ''%s'' ) order by CM_CODIGO', [ GetCompany ] ) );
             end;
             Active := True;
             while not Eof do
             begin
                  with Lista.AgregaEmpresa do
                  begin
                       Codigo := FieldByName( 'CM_CODIGO' ).AsString;
                       Nombre := FieldByName( 'CM_NOMBRE' ).AsString;
                       Alias := FieldByName( 'CM_ALIAS' ).AsString;
                       UserName := FieldByName( 'CM_USRNAME' ).AsString;
                       Password := ZetaServerTools.Decrypt( FieldByName( 'CM_PASSWRD' ).AsString );
                       Empate := FieldByName( 'CM_EMPATE' ).AsInteger;
                  end;
                  Next;
             end;
          finally
                 Active := False;
          end;
     end;
end;

function TdmDBConfig.GetCompany: String;
begin
     Result := tqCompany.FieldByName( 'CM_CODIGO' ).AsString;
end;

function TdmDBConfig.GetCompanyName: String;
begin
     Result := tqCompany.FieldByName( 'CM_NOMBRE' ).AsString;
end;

function TdmDBConfig.GetCompanyAlias: String;
begin
     Result := tqCompany.FieldByName( 'CM_ALIAS' ).AsString;
end;

function TdmDBConfig.GetCompanyDatabase: String;
begin
     Result := tqCompany.FieldByName( 'CM_DATOS' ).AsString;
end;

function TdmDBConfig.GetCompanyUserName: String;
begin
     Result := tqCompany.FieldByName( 'CM_USRNAME' ).AsString;
end;

function TdmDBConfig.GetDigito: string;
begin
     Result := tqCompany.FieldByName( 'CM_DIGITO' ).AsString;
end;

function TdmDBConfig.GetCompanyPassword: String;
begin
     Result := Decrypt( tqCompany.FieldByName( 'CM_PASSWRD' ).AsString );
end;

procedure TdmDBConfig.SetCompanyPassword( const Value: String );
begin
     tqCompany.FieldByName( 'CM_PASSWRD' ).AsString := Encrypt( Value );
end;

function TdmDBConfig.CheckRegistry: Boolean;
begin
     with Registry do
     begin
          Result := Registry.IsValid;
     end;
end;

procedure TdmDBConfig.DecodePassword(Sender: TField; var Text: string; DisplayText: Boolean);
begin
     Text := Decrypt( Sender.AsString );
end;

procedure TdmDBConfig.EncodePassword(Sender: TField; const Text: String );
begin
     Sender.AsString := Encrypt( Text );
end;

function TdmDBConfig.GetTipoCompanyName( const sValor: String ): eTipoCompany;
var
   iValor: Integer;
begin
     Result := tc3Datos;                        // El default siempre es 3Datos
     if strLleno( sValor ) then
     begin
          iValor := FTipos.IndexOfName( AnsiUpperCase( sValor ) );
          if ( iValor >= 0 ) then
             Result := eTipoCompany( iValor );
     end;
end;

function TdmDBConfig.GetTipoEmpresa: String;
var
   sValor: String;
begin
     with tqCompany do
     begin
          sValor := FieldByName( 'CM_CONTROL' ).AsString;
          if ZetaCommonTools.StrVacio( sValor ) then
             Result := ZetaCommonLists.ObtieneElemento( lfTipoCompanyName, Ord( tc3Datos ) )
          else
              if ( FTipos.IndexOfName( AnsiUpperCase( sValor ) ) < 0 ) then
                 Result := sValor
              else
                  Result := FTipos.Values[ sValor ];
     end;
end;


function TdmDBConfig.GetTipoEmpresaEdit: String;
var
   sValor: String;
begin
     with tqCompany do
     begin
          sValor := FieldByName( 'CM_CTRL_RL' ).AsString;
          if ZetaCommonTools.StrVacio( sValor ) then
             Result := ZetaCommonLists.ObtieneElemento( lfTipoCompanyName, Ord( tc3Datos ) )
          else
              if ( FTipos.IndexOfName( AnsiUpperCase( sValor ) ) < 0 ) then
                 Result := sValor
              else
                  Result := FTipos.Values[ sValor ];
     end;
end;


procedure TdmDBConfig.SetEditMode;
begin
     with tqCompany do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
     end;
end;

procedure TdmDBConfig.CambiaTipo( const sValor: String );
begin
     with tqCompany do
     begin
          SetEditMode;
          with FieldByName( 'CM_CTRL_RL' ) do
          begin
               if ( FNombres.IndexOfName( AnsiUpperCase( sValor ) ) < 0 ) then
                  AsString := sValor
               else
                   if ZetaCommonTools.StrVacio( sValor ) then
                      AsString := ZetaCommonLists.ObtieneElemento( lfTipoCompany, Ord( tc3Datos ) )
                   else
                       AsString := FNombres.Values[ sValor ];
          end;
     end;
end;

procedure TdmDBConfig.tqCompanyAfterOpen(DataSet: TDataSet);
begin
     inherited;
     with Dataset do
     begin
          with FieldByName( 'CM_PASSWRD' ) do
          begin
               OnGetText := DecodePassword;
               OnSetText := EncodePassword;
          end;

          with FieldByName( 'CM_CTRL_RL' ) do
          begin
               OnChange :=  OnChangeCM_CTRL_RL;
          end;
     end;
end;

procedure TdmDBConfig.tqCompanyCalcFields(DataSet: TDataSet);
var
   sControl: String;
   eTipo, eCiclo: eTipoCompany;
begin
     with Dataset do
     begin
          sControl := FieldByName( 'CM_CTRL_RL' ).AsString;
          eTipo := tc3Datos;
          for eCiclo := Low( eTipoCompany ) to High( eTipoCompany ) do
          begin
               if ( sControl = ZetaCommonLists.ObtieneElemento( lfTipoCompany, Ord( eCiclo ) ) ) then
               begin
                    eTipo := eCiclo;
                    Break;
               end;
          end;
          FieldByName( 'CM_TIPO' ).AsInteger := Ord( eTipo );
          FieldByName( 'CM_TIPO_E' ).AsString := ZetaCommonLists.ObtieneElemento( lfTipoCompanyName, Ord( eTipo ) );
     end;
end;

procedure TdmDBConfig.tqCompanyBeforeInsert(DataSet: TDataSet);
begin
     FMaxDigito := ZetaDataSetTools.GetNextCaracter( DataSet, 'CM_DIGITO' );
     if EsMinuscula( FMaxDigito ) then
        FMaxDigito := #0;               // La 'a' minuscula es mayor que 'Z' y el m�ximo valor es 'Z'
end;

procedure TdmDBConfig.tqCompanyNewRecord(DataSet: TDataSet);
const
     K_SIN_RESTRICCION = -1;
begin
     with tqCompany do
     begin
          FieldByName( 'CM_CODIGO' ).AsString := VACIO;
          FieldByName( 'CM_NOMBRE' ).AsString := VACIO;
          {$ifdef INTERBASE}
          FieldByName( 'CM_USRNAME' ).AsString := 'SYSDBA';
          FieldByName( 'CM_PASSWRD' ).AsString := 'masterkey';
          {$endif}
          {$ifdef MSSQL}
          FieldByName( 'CM_USRNAME' ).AsString := 'sa';
          FieldByName( 'CM_PASSWRD' ).AsString := VACIO;
          {$endif}
          FieldByName( 'CM_CONTROL' ).AsString := ZetaCommonLists.ObtieneElemento( lfTipoCompany, Ord( tc3Datos ) );
          FieldByName( 'CM_CTRL_RL' ).AsString := FieldByName( 'CM_CONTROL' ).AsString;
          FieldByName( 'CM_EMPATE' ).AsInteger := 0;
          FieldByName( 'CM_ACUMULA' ).AsString := VACIO;
          FieldByName( 'CM_DATOS' ).AsString := VACIO;
          FieldByName( 'CM_NIVEL0' ).AsString := VACIO;
          FieldByName( 'CM_ALIAS' ).AsString := VACIO;
          FieldByName( 'CM_USACAFE' ).AsString := K_GLOBAL_NO;
          FieldByName( 'CM_USACASE' ).AsString := K_GLOBAL_NO;          
          FieldByName( 'CM_ACTIVOS' ).AsInteger := 0;
          FieldByName( 'CM_DIGITO' ).AsString := FMaxDigito;
          FieldByName( 'CM_KCLASIFI').AsInteger:= K_SIN_RESTRICCION;
          FInsert := True;
          FAcumula := FALSE;
     end;
end;


function TdmDBConfig.ValidarAltaBDProduccion : boolean;
var
    sMensajeAdv : string;
begin
     SetAutoServer( Self.AutoServer );
     FDBConfigMgr.LoadDbConfig;
     Result := FDBConfigMgr.PuedeAgregarDB;
     if ( Result ) and ( FDBConfigMgr.AdvertirAgregarDB( sMensajeAdv ) ) then
     begin
          ZWarning('Advertencia', sMensajeAdv, 0, mbOK );
     end;

end;

function TdmDBConfig.IncrementarAltaBDProduccion: boolean;
begin
     Result := True;
     with tqCompany do
     begin
        if StrLleno( FieldByName( 'CM_DATOS').AsString  ) and ( FieldByName( 'CM_CTRL_RL').AsString = '3DATOS' ) and ( ContarConfiguracionBDProduccion < 2 )   then
        begin
              FDBConfigMgr.LoadDbConfig;
              FDBConfigMgr.IncrementarDB;
              Result := FDBConfigMGr.SaveDbConfig;
        end;
     end;
end;

procedure TdmDBConfig.tqCompanyBeforePost(DataSet: TDataSet);
    procedure setCheckSumInternal;
    begin
         with tqCompany do
          FieldByName( 'CM_CHKSUM' ).AsString  := ZetaServerTools.GetCompanyChecksumFromData( FieldByName( 'CM_CODIGO' ).AsString, FieldByName( 'CM_CTRL_RL' ).AsString, FieldByName( 'CM_CONTROL' ).AsString, FieldByName( 'CM_DATOS'  ).AsString , Now);
    end;
begin
     FAcumula := FALSE;   //Si Es nuevo Acumula

     with tqCompany do
     begin
          if ( GetTipoCompanyName( FieldByName( 'CM_CONTROL' ).AsString ) <> tc3Datos ) then
          begin
             FieldByName( 'CM_DIGITO' ).AsString := VACIO;
             setCheckSumInternal;
          end
          else
          begin
             if ( FInsert  ) or  ( EsValidoOldChecksum ) then
                setCheckSumInternal
             else
             begin
                  FieldByName( 'CM_CTRL_RL').AsString := '3PRUEBA';
                  setCheckSumInternal
             end;

             { Se comento por cuestiones de usabilidad AV:Marzo/9/2012
             if ContarDuplicidadBD > 0 then
             begin
                  if  ( FieldByName( 'CM_CTRL_RL').AsString = '3DATOS' ) then
                      DatabaseError( 'Ya existe(n) empresa(s) configurada(s) de Pruebas con la misma Base de Datos' );

                  if  ( FieldByName( 'CM_CTRL_RL').AsString = '3PRUEBA' ) then
                      DatabaseError( 'Ya existe(n) empresa(s) configurada(s) de Producci�n con la misma Base de Datos' );
             end;
             }


             {
              Si es Ingreso de nueva empresa, o
              Si cambi� el tipo de Empresa o
              Si cambio de Base de datos

              Y si La BD esta asignada

             }
             if  (FInsert) or ( FieldByName( 'CM_CTRL_RL').OldValue <> FieldByName( 'CM_CTRL_RL').NewValue  )  or
                 ( UpperCase( FieldByName('CM_DATOS').OldValue )  <> UpperCase( FieldByName( 'CM_DATOS').NewValue)   )  then
             begin
                  if (  FieldByName( 'CM_CTRL_RL').AsString = '3DATOS' ) then
                     FAcumula := TRUE;
                  
                  if  StrLleno( FieldByName( 'CM_DATOS').AsString ) and  ( ContarConfiguracionBDProduccion = 0 ) then
                  begin
                     if  ( FieldByName( 'CM_CTRL_RL').AsString = '3DATOS' )  and ( not ValidarAltaBDProduccion ) then
                     begin
                         if ( AutoServer.EsDemo ) then
                            DatabaseError( 'No es posible agregar Empresas de Producci�n en modo DEMO')                         
                         else
                             DatabaseError( 'Seg�n los t�rminos de su Licencia se ha agotado el n�mero de altas permitidas de Bases de Datos de Producci�n');
                     end;
                  end;
             end;
          end;
     end;
end;

procedure TdmDBConfig.tqCompanyUpdateRecord(DataSet: TDataSet; UpdateKind: TUpdateKind; var UpdateAction: TUpdateAction);
begin
     case UpdateKind of
          ukModify:
          begin
               with upCompany do
               begin
                    SetParams( ukModify );
                    Apply( ukModify );
               end;
               UpdateAction := uaApplied;
          end;
          ukInsert:
          begin
               with upCompany do
               begin
                    SetParams( ukInsert );
                    Apply( ukInsert );
               end;
               UpdateAction := uaApplied;
          end;
     end;
end;

procedure TdmDBConfig.AbrirCompany;
begin
     tqCompany.SQL.Clear;
     if ( ExisteCheckSum ) then
     begin
        tqCompany.SQL.Add( Q_QUERY_EMPRESAS );
     end
     else
     begin
          tqCompany.SQL.Add( Q_QUERY_EMPRESAS_ANTES );
     end;

     tqCompany.Active := True;
end;

procedure TdmDBConfig.CerrarCompany;
begin
     tqCompany.Active := False;
end;

procedure TdmDBConfig.RefrescarCompany;
begin
     CerrarCompany;
     AbrirCompany;
end;

procedure TdmDBConfig.ConectarCompany( DataSource: TDataSource );
begin
     DataSource.Dataset := tqCompany;
end;

procedure TdmDBConfig.DesconectarCompany( DataSource: TDataSource );
begin
     DataSource.Dataset := nil;
end;

procedure TdmDBConfig.ModificaCompany;
begin
     tqCompany.Edit;
end;

procedure TdmDBConfig.AgregaCompany;
begin
     tqCompany.Insert;
end;

procedure TdmDBConfig.BorraCompany;
var
   oCursor: TCursor;
   {$ifdef MSSQL}
   dmMigra : TdmMigrarDatosIB;
   {$endif}
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dbEmpresas do
        begin
             try
                {$ifdef MSSQL}
                dmMigra := TdmMigrarDatosIB.Create( Self );
                dmMigra.ConectaEmpresa;
                {$endif}
                 StartTransaction;
                 try
                    with upCompany do
                    begin
                         SetParams( ukDelete );
                         ExecSQL( ukDelete );
                    end;
                    Commit;
                 except
                        on Error: Exception do
                        begin
                             RollBack;
                             Application.HandleException( Error );
                        end;
                 end;

                 with tqCompany do
                 begin
                      if FieldByName( 'CM_TIPO' ).AsInteger = Ord(tc3Datos) then
                      begin
                            {$ifdef MSSQL}
                               dmMigra.ActualizaCodigoEmpresa(VACIO,FieldByName('CM_CODIGO').AsString,False );
                            {$endif}
                            {$ifdef INTERBASE}
                               dmMigrar.OpenDatabase(dbEmpresa,GetCompanyDatabase,GetCompanyUserName,GetCompanyPassword );
                               dmMigrar.ActualizaDerechosAdicionales(dbEmpresa,False,FieldByName('CM_CODIGO').AsString,VACIO );
                            {$endif}
                      end;
                 end;
             finally
                   {$ifdef MSSQL}
                   FreeAndNil(dmMigra);
                   {$endif}
             end;
        end;
        with tqCompany do
        begin
             Active := False;
             Active := True;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TdmDBConfig.GrabaCompany;
var
   oCursor: TCursor;
   {$ifdef MSSQL}
   dmMigra : TdmMigrarDatosIB;
   {$endif}

begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        {$ifdef MSSQL}
        dmMigra := TdmMigrarDatosIB.Create( Self );
        {$endif}
        {$ifdef INTERBASE}
        dmMigrar.OpenDatabase(dbEmpresa,GetCompanyDatabase,GetCompanyUserName,GetCompanyPassword );
        {$endif}
        with tqCompany do
        begin
             Post;
             if ( FieldByName( 'CM_TIPO' ).AsInteger = Ord(tc3Datos) ) and ( Not FInsert ) then
             begin
                   {$ifdef MSSQL}
                      dmMigra.ActualizaCodigoEmpresa( FieldByName( 'CM_CODIGO' ).OldValue, FieldByName( 'CM_CODIGO' ).AsString,True);
                   {$endif}
                   {$ifdef INTERBASE}
                      dmMigrar.ActualizaDerechosAdicionales( dbEmpresa,True, FieldByName( 'CM_CODIGO' ).AsString,FieldByName( 'CM_CODIGO' ).OldValue );
                   {$endif}
             end;
             dbEmpresas.ApplyUpdates( [ tqCompany ] );

             if FAcumula then
                IncrementarAltaBDProduccion;
        end;
     finally
            {$ifdef MSSQL}
            FreeAndNil(dmMigra);
            {$endif}
            Screen.Cursor := oCursor;
            dbEmpresa.Connected := False;
     end;
     FInsert := False;
     FAcumula := False;
end;

procedure TdmDBConfig.CancelaCambiosCompany;
begin
     with tqCompany do
     begin
          Cancel;
          CancelUpdates;
     end;
end;

function TdmDBConfig.DesbloqueaAdministradores: Boolean;
begin
     Result := True;
     with dbEmpresas do
     begin
          StartTransaction;
          try
             with tqList do
             begin
                  with SQL do
                  begin
                       Clear;
                       Add( Format( 'update USUARIO set US_BLOQUEA = %s where ( GR_CODIGO = %d )', [ EntreComillas( ZetaCommonClasses.K_GLOBAL_NO ), D_GRUPO_SIN_RESTRICCION ] ) );
                  end;
                  ExecSQL;
             end;
             Commit;
          except
                on Error: Exception do
                begin
                     Rollback;
                     Result := False;
                     Application.HandleException( Error );
                end;
          end;
     end;
end;

{ Asignaci�n de Licencias de Empleados }

procedure TdmDBConfig.cdsEmpresasAfterOpen(DataSet: TDataSet);
begin
     inherited;
     MaskNumerico( 'CM_ACTIVOS' );
     MaskNumerico( 'CM_INACTIV' );
     MaskNumerico( 'CM_TOTAL' );
     MaskNumerico( 'CM_ASIGNED' );
end;

function TdmDBConfig.BuildEmpresa(DataSet : TDataSet):OleVariant;
{
Las posiciones de este arreglo se encuentran en ZetaCommonClasses:

P_ALIAS = 0;
P_DATABASE = 0;
P_USER_NAME = 1;
P_PASSWORD = 2;
P_USUARIO = 3;
P_NIVEL_0 = 4;
P_CODIGO = 5;
}
begin
     with DataSet do
     begin
          Result := VarArrayOf( [ FieldByName( 'CM_DATOS' ).AsString,
                                  FieldByName( 'CM_USRNAME' ).AsString,
                                  FieldByName( 'CM_PASSWRD' ).AsString,
                                  0,
                                  FieldByName( 'CM_NIVEL0' ).AsString,
                                  FieldByName( 'CM_CODIGO' ).AsString ] );
     end;
end;


function TdmDBConfig.GetEmpresaActualizar:OleVariant;
{
Las posiciones de este arreglo se encuentran en ZetaCommonClasses:

P_ALIAS = 0;
P_DATABASE = 0;
P_USER_NAME = 1;
P_PASSWORD = 2;
P_USUARIO = 3;
P_NIVEL_0 = 4;
P_CODIGO = 5;

P_GRUPO = 6;
P_APPID = 7;
}

const
K_USUARIO_ADMINISTRADOR = 1;

begin
     with cdsEmpresasActualizar do
     begin
          Result := VarArrayOf( [ FieldByName( 'CM_DATOS' ).AsString,
                                  FieldByName( 'CM_USRNAME' ).AsString,
                                  Encrypt(  FieldByName( 'CM_PASSWRD' ).AsString ) ,
                                  K_USUARIO_ADMINISTRADOR,
                                  FieldByName( 'CM_NIVEL0' ).AsString,
                                  FieldByName( 'CM_CODIGO' ).AsString,
                                  D_GRUPO_SIN_RESTRICCION,
                                  0 ] );
     end;
end;

//    eTipoCompany = ( tc3Datos, tcRecluta, tcVisitas, tcOtro, tc3Prueba, tcPresupuesto );
procedure TdmDBConfig.EmpresasConectar( const lTodas: Boolean  ; const eTipo  : eTipoCompany = tc3Datos);
begin
     { Primero llenar la lista de empresas }
     try
        with tqList do
        begin
             Active := False;
             with SQL do
             begin
                  Clear;
                  Add( 'select CM_CODIGO, CM_NOMBRE, CM_ACTIVOS, CM_ALIAS, CM_USRNAME, CM_PASSWRD, CM_DATOS, CM_NIVEL0, CM_KCLASIFI, CM_CTRL_RL, CM_CHKSUM from COMPANY where ' );
                  if not lTodas then
                  begin
                       Add( '( CM_NIVEL0 = '''' ) and ' );
                  end;

                 Add( Format( '( %s ) order by CM_CODIGO', [ ZetaServerTools.GetTipoCompany( Ord( eTipo ) ) ] ) )
             end;
             Active := True;
             with cdsEmpresas do
             begin
                  InitTempDataSet;
                  AddStringField( 'CM_CODIGO', tqList.FieldByName( 'CM_CODIGO' ).DataSize );
                  AddStringField( 'CM_NOMBRE', tqList.FieldByName( 'CM_NOMBRE' ).DataSize );
                  AddStringField( 'CM_ALIAS', tqList.FieldByName( 'CM_ALIAS' ).DataSize );
                  AddStringField( 'CM_USRNAME', tqList.FieldByName( 'CM_USRNAME' ).DataSize );
                  AddStringField( 'CM_PASSWRD', tqList.FieldByName( 'CM_PASSWRD' ).DataSize );
                  AddStringField( 'CM_DATOS', tqList.FieldByName( 'CM_DATOS' ).DataSize );
                  AddStringField( 'CM_NIVEL0', tqList.FieldByName( 'CM_NIVEL0' ).DataSize );
                  AddStringField( 'CM_CTRL_RL', tqList.FieldByName( 'CM_CTRL_RL' ).DataSize );
                  AddStringField( 'CM_CHKSUM', tqList.FieldByName( 'CM_CHKSUM' ).DataSize );
                  AddIntegerField( 'CM_ASIGNED' );
                  AddIntegerField( 'CM_ACTIVOS' );
                  AddIntegerField( 'CM_INACTIV' );
                  AddIntegerField( 'CM_TOTAL' );
                  AddDateField( 'CM_ULT_ALT' );
                  AddDateField( 'CM_ULT_BAJ' );
                  AddDateField( 'CM_ULT_TAR' );
                  AddStringField( 'CM_ULT_NOM', K_ANCHO_ULTIMA_NOMINA );
                  AddIntegerField( 'PE_YEAR' );
                  AddIntegerField( 'PE_TIPO' );
                  AddIntegerField( 'PE_NUMERO' );
                  AddDateField( 'PE_FEC_INI' );
                  AddStringField( 'CM_CONNECT', 1 );
                  AddIntegerField( 'PC_SI_QTY' );
                  AddDateField( 'PC_SI_FEC' );
                  AddIntegerField( 'PC_AN_QTY' );
                  AddDateField( 'PC_AN_FEC' );
                  AddIntegerField( 'CM_KCLASIFI');
                  AddFloatField( 'PC_NOMLAVG');
                  AddFloatField( 'PC_NOM_AVG');

                  AddIntegerField( 'PC_FOA_QTY' );
                  AddDateField( 'PC_FOA_FEC' );
                  AddIntegerField( 'PC_FON_QTY' );
                  AddDateField( 'PC_FON_FEC' );
                  AddIntegerField( 'PC_FOC_QTY' );
                  AddDateField( 'PC_FOC_FEC' );
                  AddIntegerField( 'PC_LB_QTY' );
                  AddDateField( 'PC_LB_FEC' );
                  AddIntegerField( 'PC_EM_QTY' );
                  AddDateField( 'PC_EM_FEC' );
                  AddIntegerField( 'RE_CU_QTY' );
                  AddIntegerField( 'RE_CF_QTY' );
                  AddDateField( 'RE_CF_FEC' );
                  AddIntegerField( 'RE_RG_QTY' );
                  AddDateField( 'RE_RG_FEC' );
                  AddIntegerField( 'RE_SM_QTY' );
                  AddDateField( 'RE_SM_FEC' );
                  AddIntegerField( 'RE_PC_QTY' );
                  AddDateField( 'RE_PC_FEC' );
                  AddIntegerField( 'RE_EV_QTY' );
                  AddDateField( 'RE_EV_FEC' );

                  AddFloatField( 'DB_SIZE');
                  AddStringField( 'MS_SQL_VER', 256 );
                  AddStringField( 'MS_SQL_LEV', 256 );
                  AddStringField( 'MS_SQL_EDT', 256 );

                  //NOMINA
//                  AddStringField(  'COMPANYXMLDATA', 18000 );
                  AddMemoField('COMPANYXMLDATA', 32000);
                  {
                  AddStringField(  'ISPT', 512 );
                  AddStringField(  'INFONAVITAPORTA', 512 );
                  AddStringField(  'INFONAVITAPROV', 512 );
                  AddStringField(  'PERIODOS', 2048 );

                  //ASISTENCIA
                  AddStringField(  'RELOJES', 1024 );

                  //FONACOT
                  AddStringField(  'FONACOT_AJUSTE', 256 );
                  AddStringField(  'FONACOT_CALCULO', 256 );

                  //USOMODULOS
                  AddStringField(  'CURSOS', 200 );
                  AddStringField(  'CAFETERIA', 200 );
                  AddStringField(  'LABOR', 200 );
                  AddStringField(  'REPORTESE', 200 );
                  AddStringField(  'RESGUARDO', 200 );
                  AddStringField(  'MEDICOS', 200 );
                  AddStringField(  'PLANCARRERA', 200 );
                  AddStringField(  'EVALUACION', 200 );}



                  CreateTempDataset;
                  LogChanges := False;
             end;
             while not Eof do
             begin
                  cdsEmpresas.Append;
                  cdsEmpresas.FieldByName( 'CM_CODIGO' ).AsString := FieldByName( 'CM_CODIGO' ).AsString;
                  cdsEmpresas.FieldByName( 'CM_NOMBRE' ).AsString := FieldByName( 'CM_NOMBRE' ).AsString;
                  cdsEmpresas.FieldByName( 'CM_ALIAS' ).AsString := FieldByName( 'CM_ALIAS' ).AsString;
                  cdsEmpresas.FieldByName( 'CM_USRNAME' ).AsString := FieldByName( 'CM_USRNAME' ).AsString;
                  cdsEmpresas.FieldByName( 'CM_PASSWRD' ).AsString := FieldByName( 'CM_PASSWRD' ).AsString;
                  cdsEmpresas.FieldByName( 'CM_DATOS' ).AsString := FieldByName( 'CM_DATOS' ).AsString;
                  cdsEmpresas.FieldByName( 'CM_NIVEL0' ).AsString := FieldByName( 'CM_NIVEL0' ).AsString;
                  cdsEmpresas.FieldByName( 'CM_CTRL_RL' ).AsString := FieldByName( 'CM_CTRL_RL' ).AsString;
                  cdsEmpresas.FieldByName( 'CM_CHKSUM' ).AsString := FieldByName( 'CM_CHKSUM' ).AsString;
                  cdsEmpresas.FieldByName( 'CM_CONNECT' ).AsString := K_GLOBAL_NO;
                  cdsEmpresas.FieldByName('CM_KCLASIFI').AsInteger:= FieldByName('CM_KCLASIFI').AsInteger;
                  cdsEmpresas.FieldByName( 'COMPANYXMLDATA' ).AsString := VACIO;
                  cdsEmpresas.FieldByName( 'DB_SIZE' ).AsFloat := 0;
                  cdsEmpresas.Post;
                  Next;
             end;
             Active := False;
        end;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                { Para que no haya nada que modificar }
                cdsEmpresas.EmptyDataset;
           end;
     end;
end;


// Consulta de Empresas por Tipo de bases de Datos
procedure TdmDBConfig.EmpresasActualizarConectar( const lTodas: Boolean; const tcTipo : eTipoCompany  );

    function FiltroTipo : string;
    begin

    end;
begin
     { Primero llenar la lista de empresas }
     try
        with tqList do
        begin
             Active := False;
             with SQL do
             begin
                  Clear;
                  Add( 'select CM_CODIGO, CM_NOMBRE, CM_ACTIVOS, CM_ALIAS, CM_USRNAME, CM_PASSWRD, CM_DATOS, CM_NIVEL0, CM_KCLASIFI from COMPANY where ' );
                  if not lTodas then
                  begin
                       Add( '( CM_NIVEL0 = '''' ) and ' );
                  end;

                  if ( tcTipo = tc3Datos ) then
                     Add( Format( '( %s ) order by CM_CODIGO', [ ZetaServerTools.GetTipoCompanyMasPresupuestos ] ) )
                  else
                      Add( Format( '( %s ) order by CM_CODIGO', [ ZetaServerTools.GetTipoCompany( Ord( tcTipo ) ) ] ) );
             end;
             Active := True;
             with cdsEmpresasActualizar do
             begin
                  InitTempDataSet;
                  AddStringField( 'CM_CODIGO', tqList.FieldByName( 'CM_CODIGO' ).DataSize );
                  AddStringField( 'CM_NOMBRE', tqList.FieldByName( 'CM_NOMBRE' ).DataSize );
                  AddStringField( 'CM_ALIAS', tqList.FieldByName( 'CM_ALIAS' ).DataSize );
                  AddStringField( 'CM_USRNAME', tqList.FieldByName( 'CM_USRNAME' ).DataSize );
                  AddStringField( 'CM_PASSWRD', tqList.FieldByName( 'CM_PASSWRD' ).DataSize );
                  AddStringField( 'CM_DATOS', tqList.FieldByName( 'CM_DATOS' ).DataSize );
{$ifdef MSSQL}
                  AddStringField( 'CM_NIVEL0', tqList.FieldByName( 'CM_NIVEL0' ).DataSize );
{$else}
                  AddStringField( 'CM_NIVEL0', 255 {tqList.FieldByName( 'CM_NIVEL0' ).DataSize} );
{$endif}
                  AddIntegerField( 'CM_ASIGNED' );
                  AddIntegerField( 'CM_ACTIVOS' );
                  AddIntegerField( 'CM_INACTIV' );
                  AddIntegerField( 'CM_TOTAL' );
                  AddDateField( 'CM_ULT_ALT' );
                  AddDateField( 'CM_ULT_BAJ' );
                  AddDateField( 'CM_ULT_TAR' );
                  AddStringField( 'CM_ULT_NOM', K_ANCHO_ULTIMA_NOMINA );
                  AddIntegerField( 'PE_YEAR' );
                  AddIntegerField( 'PE_TIPO' );
                  AddIntegerField( 'PE_NUMERO' );
                  AddDateField( 'PE_FEC_INI' );
                  AddStringField( 'CM_CONNECT', 1 );
                  AddIntegerField( 'PC_SI_QTY' );
                  AddDateField( 'PC_SI_FEC' );
                  AddIntegerField( 'PC_AN_QTY' );
                  AddDateField( 'PC_AN_FEC' );
                  AddIntegerField( 'CM_KCLASIFI');
                  AddIntegerField( 'CM_VERSION');
                  AddIntegerField( 'CM_VERSION_NUEVA');
                  AddStringField( 'CM_ACTUALIZAR', 1);
                  AddStringField( 'CM_AVANCE_PATCH', 100);
                  AddStringField( 'CM_AVANCE_DICCIONARIO', 100);
                  AddStringField( 'CM_AVANCE_REPORTES', 100);
                  AddStringField( 'CM_EXCEPCION', 500);
                  AddFloatField( 'PC_NOMLAVG');
                  AddFloatField( 'PC_NOM_AVG');
                  CreateTempDataset;
                  LogChanges := False;
             end;
             while not Eof do
             begin
                  cdsEmpresasActualizar.Append;
                  cdsEmpresasActualizar.FieldByName( 'CM_CODIGO' ).AsString := FieldByName( 'CM_CODIGO' ).AsString;
                  cdsEmpresasActualizar.FieldByName( 'CM_NOMBRE' ).AsString := FieldByName( 'CM_NOMBRE' ).AsString;
                  cdsEmpresasActualizar.FieldByName( 'CM_ALIAS' ).AsString := FieldByName( 'CM_ALIAS' ).AsString;
                  cdsEmpresasActualizar.FieldByName( 'CM_USRNAME' ).AsString := FieldByName( 'CM_USRNAME' ).AsString;
                  cdsEmpresasActualizar.FieldByName( 'CM_PASSWRD' ).AsString :=  ZetaServerTools.Decrypt( FieldByName( 'CM_PASSWRD' ).AsString ); //FieldByName( 'CM_PASSWRD' ).AsString;
                  cdsEmpresasActualizar.FieldByName( 'CM_DATOS' ).AsString := FieldByName( 'CM_DATOS' ).AsString;
                  cdsEmpresasActualizar.FieldByName( 'CM_NIVEL0' ).AsString := FieldByName( 'CM_NIVEL0' ).AsString;
                  cdsEmpresasActualizar.FieldByName( 'CM_CONNECT' ).AsString := K_GLOBAL_NO;
                  cdsEmpresasActualizar.FieldByName('CM_KCLASIFI').AsInteger:= FieldByName('CM_KCLASIFI').AsInteger;
                  cdsEmpresasActualizar.FieldByName( 'CM_VERSION' ).AsInteger := 0 ;
                  cdsEmpresasActualizar.FieldByName( 'CM_VERSION_NUEVA' ).AsInteger := 0 ;
                  cdsEmpresasActualizar.FieldByName( 'CM_ACTUALIZAR' ).AsString := K_GLOBAL_SI;
                  cdsEmpresasActualizar.FieldByName( 'CM_AVANCE_PATCH' ).AsString := VACIO;
                  cdsEmpresasActualizar.FieldByName( 'CM_AVANCE_DICCIONARIO' ).AsString := VACIO;
                  cdsEmpresasActualizar.FieldByName( 'CM_AVANCE_REPORTES' ).AsString := VACIO;
                  cdsEmpresasActualizar.FieldByName( 'CM_EXCEPCION' ).AsString := VACIO;
                  cdsEmpresasActualizar.Post;
                  Next;
             end;
             Active := False;
        end;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                { Para que no haya nada que modificar }
                cdsEmpresasActualizar.EmptyDataset;
           end;
     end;
end;



procedure TdmDBConfig.LicenciasLeer( Manager: TLicenseMgr; Provider: TdmZetaServerProvider; Respuesta: TCallBack; RespuestaError: TCallBackError );
const
     K_MESES_3DT_HISTORIA = 3;
var
   lOk : Boolean;
   iEmpleados, iDesempleados, iAsignados: Integer;
   sEmpresa, sRazonSocial, sUltimaNomina, sCompanyXMLData, sBD : String;
   dUltimaAlta, dUltimaBaja, dUltimaTarjeta: TDate;
   fNominaDuracionAvgDias, fNominaDuracionAvgTotal, fDBSize : Double;
   FPeriodo: TDatosPeriodo;
   FProcesos: TDatosUltimosProcesos;
   FRegistros : TDatosUltimosRegistros;
   FVerSQLServer : TSQLServerVersion;
begin
     {$ifdef VALIDA_EMPLEADOS}
     lOk := True;
     {$endif}
     iAsignados := 0;
     iEmpleados := 0;
     iDesempleados := 0;
     dUltimaTarjeta := NullDateTime;
     dUltimaAlta := NullDateTime;
     dUltimaBaja := NullDateTime;
     fNominaDuracionAvgDias :=0.00;
     fNominaDuracionAvgTotal := 0.00;
     fDBSize := -1;
     sCompanyXMLData := VACIO;
     with cdsEmpresas do
     begin
          First;
          {$ifdef VALIDA_EMPLEADOS}
          while not Eof and lOk do
          {$else}
          while not Eof do
          {$endif}
          begin
               sEmpresa := FieldByName( 'CM_CODIGO' ).AsString;
               sRazonSocial := FieldByName( 'CM_NOMBRE' ).AsString;
               sBD := FieldByName( 'CM_DATOS' ).AsString;

               {$ifdef VALIDA_EMPLEADOS}
               {$else}
               lOk := True;
               {$endif}
               try
                  Respuesta( Format( 'Leyendo %s', [ sEmpresa ] ) );
                  with Provider do
                  begin
                       Deactivate;     //Se agrega para descontaminar los accesos pasados
                       Activate;
                       EmpresaActiva := BuildEmpresa( dmDBConfig.cdsEmpresas );
                       with Manager do
                       begin
                            iAsignados := AsignadosGet;
                            iEmpleados := EmpleadosGet;
                            iDesempleados := DesempleadosGet;
                            dUltimaAlta := UltimaAlta;
                            dUltimaBaja := UltimaBaja;
                            dUltimaTarjeta := UltimaTarjeta;
                            FPeriodo := UltimaNomina;
                            fNominaDuracionAvgDias := DuracionNomina(K_MESES_3DT_HISTORIA);
                            fNominaDuracionAvgTotal := DuracionNomina(660);  //-55 years
                            fDBSize :=  GetDatabaseSize( sBD );
                            with FPeriodo do
                            begin
                                 sUltimaNomina := Format( '%s ( %s )', [ ZetaCommonTools.GetPeriodoInfo( Year, Numero, Tipo ), ZetaCommonTools.FechaCorta( Inicio ) ] );
                            end;
                            FProcesos := UltimosProcesos;
                            FRegistros := UltimosRegistros;
                            FVerSQLServer := GetSQLServerVersion;
                            sCompanyXMLData := GetCompanyXMLStr;
                       end;
                       Deactivate;
                  end;
               except
                     on Error: Exception do
                     begin
                          RespuestaError( Format( 'No Fu� Posible Leer Los Empleados Asignados A La Empresa %s: ' + CR_LF + '%s', [ sEmpresa, sRazonSocial ] ), Error );
                          lOk := False;
                     end;
               end;
               if lOk then
               begin
                    Edit;
                    FieldByName( 'CM_ACTIVOS' ).AsInteger := iEmpleados;
                    FieldByName( 'CM_INACTIV' ).AsInteger := iDesempleados;
                    FieldByName( 'CM_TOTAL' ).AsInteger := iEmpleados + iDesempleados;
                    FieldByName( 'CM_ASIGNED' ).AsInteger := iAsignados;
                    FieldByName( 'CM_ULT_ALT' ).AsDateTime := dUltimaAlta;
                    FieldByName( 'CM_ULT_BAJ' ).AsDateTime := dUltimaBaja;
                    FieldByName( 'CM_ULT_TAR' ).AsDateTime := dUltimaTarjeta;
                    FieldByName( 'CM_ULT_NOM' ).AsString := Copy( sUltimaNomina, 1, K_ANCHO_ULTIMA_NOMINA );
                    FieldByName( 'DB_SIZE' ).AsFloat := fDBSize;

                    with FVerSQLServer do
                    begin
                        FieldByName( 'MS_SQL_VER' ).AsString := Version;
                        FieldByName( 'MS_SQL_LEV' ).AsString := Nivel;
                        FieldByName( 'MS_SQL_EDT' ).AsString := Edicion;
                    end;

                    //Datos de Modulos
                    with FRegistros do
                    begin
                        FieldByName( 'RE_CU_QTY' ).AsInteger := CursosEntrenaCuantos;
                        FieldByName( 'RE_CF_QTY' ).AsInteger := CafeteriaCuantos;
                        FieldByName( 'RE_CF_FEC' ).AsDateTime:= CafeteriaUltimo;
                        FieldByName( 'RE_RG_QTY' ).AsInteger := ResguardoCuantos;
                        FieldByName( 'RE_RG_FEC' ).AsDateTime:= ResguardoUltimo;
                        FieldByName( 'RE_SM_QTY' ).AsInteger := SMedicosCuantos;
                        FieldByName( 'RE_SM_FEC' ).AsDateTime:= SMedicosUltimo;
                        FieldByName( 'RE_PC_QTY' ).AsInteger := PlanCarreraCuantos;
                        FieldByName( 'RE_PC_FEC' ).AsDateTime:= PlanCarreraUltimo;
                        FieldByName( 'RE_EV_QTY' ).AsInteger := EvaluacionCuantos;
                        FieldByName( 'RE_EV_FEC' ).AsDateTime:= EvaluacionUltimo;
                    end;

                    with FPeriodo do
                    begin
                         FieldByName( 'PE_YEAR' ).AsInteger := Year;
                         FieldByName( 'PE_TIPO' ).AsInteger := Ord( Tipo );
                         FieldByName( 'PE_NUMERO' ).AsInteger := Numero;
                         FieldByName( 'PE_FEC_INI' ).AsDateTime := Inicio;
                    end;
                    FieldByName( 'CM_CONNECT' ).AsString := K_GLOBAL_SI;

                    with FProcesos do
                    begin
                         FieldByName( 'PC_SI_QTY' ).AsInteger := CalculoIntegradosCuantos;
                         FieldByName( 'PC_SI_FEC' ).AsDateTime := CalculoIntegradosUltimo;
                         FieldByName( 'PC_AN_QTY' ).AsInteger := AfectarNominaCuantos;
                         FieldByName( 'PC_AN_FEC' ).AsDateTime := AfectarNominaUltimo;
                         FieldByName( 'PC_FOA_QTY' ).AsInteger := AjusteRetFonacotCuantos;
                         FieldByName( 'PC_FOA_FEC' ).AsDateTime:= AjusteRetFonacotUltimo;
                         FieldByName( 'PC_FON_QTY' ).AsInteger:= CalcularPagoFonacotCuantos;
                         FieldByName( 'PC_FON_FEC' ).AsDateTime:= CalcularPagoFonacotUltimo;
                         FieldByName( 'PC_LB_QTY' ).AsInteger := LabCalcularTiemposCuantos;
                         FieldByName( 'PC_LB_FEC' ).AsDateTime:= LabCalcularTiemposUltimo;
                         FieldByName( 'PC_FOC_QTY' ).AsInteger:= ConciliacionFonacotCuantos;
                         FieldByName( 'PC_FOC_FEC' ).AsDateTime:= ConciliacionFonacotUltimo;
                         FieldByName( 'PC_EM_QTY' ).AsInteger := ReportesEmailCuantos;
                         FieldByName( 'PC_EM_FEC' ).AsDateTime:= ReportesEmailUltimo;

                    end;

                    FieldByName( 'PC_NOMLAVG' ).AsFloat := fNominaDuracionAvgDias;
                    FieldByName( 'PC_NOM_AVG' ).AsFloat := fNominaDuracionAvgTotal;
                    FieldByName( 'COMPANYXMLDATA' ).AsString := sCompanyXMLData;
                    Post;
               end
               else
               begin
               end;
               Next;
          end;
          First;
     end;
end;

function TdmDBConfig.UsuariosContar: Integer;
begin
     try
        with tqList do
        begin
             Active := False;
             with SQL do
             begin
                  Clear;
                  Add( 'select COUNT(*) as CUANTOS from USUARIO' );
             end;
             Active := True;
             Result := Fields[ 0 ].AsInteger;
             Active := False;
        end;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                Result := 0;
           end;
     end;
end;


procedure TdmDBConfig.EscribeAUTOMOD( oAutoServer: TAutoServer );
{$ifdef MSSQL}
const
      Q_VA_INSERTA_CLAVES= 'insert into AUTOMOD ( AM_CODIGO, AM_NOMBRE,AM_FECHA ) ' +
                                         'values ( %d, ''%s'', ''%s'' )';

 var
    eModulo : TModulos;
 {$endif}
begin
     {$ifdef MSSQL}
     with tqList do
     begin
          try
             try
                Active := False;
                with SQL do
                begin
                     Clear;
                     Add( 'DELETE FROM AUTOMOD' );
                end;
                ExecSQL;

                for eModulo := Low( TModulos ) to High( TModulos ) do
                    if oAutoServer.OkModulo( eModulo, FALSE ) or oAutoServer.EsDemo then
                    begin
                         //Active := False;
                         SQL.Clear;
                         SQL.Add(  Format( Q_VA_INSERTA_CLAVES, [ Ord(eModulo), FAutoClasses.GetModuloName( eModulo), DateToStrSQL(Now) ] )  );
                         ExecSQL;
                    end;
                    //Los siguientes modulos van fijos:
                    //Reporteador
                         SQL.Clear;
                         SQL.Add(  Format( Q_VA_INSERTA_CLAVES, [ 25, 'Reporteador', DateToStrSQL(Now) ] )  );
                         ExecSQL;
                    //Catalogos y Tablas
                         SQL.Clear;
                         SQL.Add(  Format( Q_VA_INSERTA_CLAVES, [ 26, 'Catalogos y tablas', DateToStrSQL(Now) ] )  );
                         ExecSQL;
                    //Caja de Ahorro
                         SQL.Clear;
                         SQL.Add(  Format( Q_VA_INSERTA_CLAVES, [ 27, 'Caja de Ahorro', DateToStrSQL(Now) ] )  );
                         ExecSQL;
                except
                end;
          finally
                 Active := False;
          end;
     end;
     {$endif}
end;

procedure TdmDBConfig.cdsEmpresasActualizarAfterOpen(DataSet: TDataSet);
begin
     inherited;
     with Dataset do
     begin
          with FieldByName( 'CM_ACTUALIZAR' ) do
          begin
               OnGetText := CM_ACTUALIZARGetText;
          end;
          with FieldByName( 'CM_VERSION' ) do
          begin
               OnGetText := CM_VERSIONGetText;
          end;
     end;
 end;

procedure TdmDBConfig.CM_ACTUALIZARGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
     Text := VACIO;
end;

procedure TdmDBConfig.tqSPPatchUpdateRecord(DataSet: TDataSet;
  UpdateKind: TUpdateKind; var UpdateAction: TUpdateAction);
var
   sIsEqual : string;
begin
     try
         case UpdateKind of
              ukDelete:
              begin
              with upSPPatch do
                   begin
                        SetParams( ukDelete );
                        Apply( ukDelete );
                   end;
                   UpdateAction := uaApplied;
              end;

              ukModify:
              begin
                   with upSPPatch do
                   begin
                       {if ( tqSPPatch.FieldByName('SE_APLICA').IsNull ) then
                        begin}
                            upSPPatch.Query[ukModify].SQL.Clear;

                            if (   VarIsNull( tqSPPatch.FieldByName('SE_APLICA').OldValue ) ) then
                               sIsEqual := 'is'
                            else
                                sIsEqual := '=';

                             upSPPatch.Query[ukModify].SQL.Add(Format( 'update SP_PATCH set '+
                                                               ' SE_TIPO =:SE_TIPO, '+
                                                               ' SE_SEQ_NUM =:SE_SEQ_NUM, '+
                                                               ' SE_APLICA =:SE_APLICA, '+
                                                               ' SE_NOMBRE =:SE_NOMBRE, '+
                                                               ' SE_DESCRIP =:SE_DESCRIP, '+
                                                               ' SE_DATA = :SE_DATA, '+
                                                               ' SE_ARCHIVO = :SE_ARCHIVO '+
                                                               ' where'+
                                                               ' (SE_TIPO = :OLD_SE_TIPO  and SE_SEQ_NUM = :OLD_SE_SEQ_NUM and '+
                                                               ' SE_APLICA %s :OLD_SE_APLICA)', [sIsEqual]) );


                        SetParams( ukModify );
                        Apply( ukModify );
                   end;

                   UpdateAction := uaApplied;
              end;

              ukInsert:
              begin
                   UpdateAction := uaApplied;
              end;
         end;
     except
            on Error: Exception do
            begin
                 //Application.HandleException( Error );
                 UpdateAction := uaFail;
            end;
     end;

end;

procedure TdmDBConfig.tqSPPatchNewRecord(DataSet: TDataSet);
begin
   with Dataset do
  begin
       FieldByName( 'SE_TIPO' ).AsInteger := 0;
       FieldByName( 'SE_SEQ_NUM' ).AsInteger := 0;
       FieldByName( 'SE_ARCHIVO' ).AsString := VACIO;
  end;
end;

function  TdmDBConfig.GrabaSPPatch : boolean;
begin
     Result := False;
     try
        try
            with tqSPPatch do
            begin
                 dbEmpresas.ApplyUpdates( [ tqSPPatch ] );
                 Result := True;
                 tqSPPatch.Active := False;
                 tqSPPatch.Active := True;
            end;
        except
               on Error: Exception do
               begin
                   if StrVacio ( tqSPPatch.FieldByName( 'SE_APLICA' ).AsString ) then
                      Raise Exception.Create( 'No fue posible modificar el SP ,'+CR_LF+' �Ya existe un Stored Procedure adicional asociado al mismo Tipo y # de Secuencia!')
                   else
                       Raise Exception.Create( 'No fue posible modificar el SP ,'+CR_LF+' �Ya existe un Stored Procedure adicional asociado  al mismo Tipo , # de  Secuencia  y Empresa');


               end;
        end;
     finally

     end;
end;

function TdmDBConfig.AgregaSPPatch : boolean;
const
       Q_EXISTE_SP_PATCH = 'select COUNT(*) as CUANTOS from SP_PATCH where SE_TIPO = %d and SE_SEQ_NUM = %d and SE_APLICA = %s ';
       Q_INSERT_SP_PATCH= 'insert into SP_PATCH (SE_SEQ_NUM, SE_TIPO, SE_APLICA, SE_NOMBRE, SE_DESCRIP, SE_DATA, SE_ARCHIVO) VALUES '+
                 '( :SE_SEQ_NUM, :SE_TIPO, :SE_APLICA, :SE_NOMBRE, :SE_DESCRIP, :SE_DATA, :SE_ARCHIVO ) ';
var

   iCuantos : integer;

  procedure DespreparaQuery( Query: TQuery );
  begin
       with Query do
       begin
            Active := False;
            if Prepared then
               Unprepare;
       end;
  end;

  procedure PreparaQuery( Query: TQuery; const sSQL: String );
  begin
       DespreparaQuery( Query );
       with Query do
       begin
            with SQL do
            begin
                 Clear;
                 Text := sSQL;
            end;
            Prepare;
       end;
  end;

  function ExisteRegistroSpPatch( iSE_TIPO, iSE_SEQ_NUM : integer ; sSE_APLICA : string )  : boolean;
  begin
     try
        with tqList do
        begin
             Active := False;
             with SQL do
             begin
                  Clear;
                  Add(  Format ( Q_EXISTE_SP_PATCH , [iSE_TIPO, iSE_SEQ_NUM, EntreComillas( sSE_APLICA ) ] ) );
             end;
             Active := True;
             iCuantos := Fields[ 0 ].AsInteger;
             Result := (iCuantos > 0 );
             Active := False;
        end;
     except
           on Error: Exception do
           begin
                Raise Exception.Create( 'No fue posible consultar SP_PATCH');
                Result := False;
           end;
     end;
  end;

begin
     Result := False;

     if not ExisteRegistroSpPatch( tqSPPatch.FieldByName( 'SE_TIPO' ).AsInteger,  tqSPPatch.FieldByName( 'SE_SEQ_NUM' ).AsInteger, tqSPPatch.FieldByName( 'SE_APLICA' ).AsString ) then
     begin
        try
           with dbEmpresas do
           begin
              StartTransaction;
              try
                 PreparaQuery( tqActualizaBlob ,Q_INSERT_SP_PATCH );
                 tqActualizaBlob.ParamByName( 'SE_DATA' ).AsMemo :=  tqSPPatch.FieldByName( 'SE_DATA' ).AsString;
                 tqActualizaBlob.ParamByName( 'SE_TIPO' ).AsInteger := tqSPPatch.FieldByName( 'SE_TIPO' ).AsInteger;
                 tqActualizaBlob.ParamByName( 'SE_SEQ_NUM' ).AsInteger := tqSPPatch.FieldByName( 'SE_SEQ_NUM' ).AsInteger;

                 if ( tqSPPatch.FieldByName( 'SE_APLICA' ).IsNull ) then
                 begin
                    with tqActualizaBlob.ParamByName( 'SE_APLICA' )do
                    begin
                         Clear;
                         DataType := ftString;
                         Bound := True;
                    end;
                 end
                 else
                     tqActualizaBlob.ParamByName( 'SE_APLICA' ).AsString := tqSPPatch.FieldByName( 'SE_APLICA' ).AsString;
                 tqActualizaBlob.ParamByName( 'SE_NOMBRE' ).AsString := tqSPPatch.FieldByName( 'SE_NOMBRE' ).AsString;
                 tqActualizaBlob.ParamByName( 'SE_DESCRIP' ).AsString := tqSPPatch.FieldByName( 'SE_DESCRIP' ).AsString;
                 tqActualizaBlob.ParamByName( 'SE_ARCHIVO').AsString := tqSPPatch.FieldByName( 'SE_ARCHIVO' ).AsString;
                 tqActualizaBlob.ExecSQL;
                 Commit;
                 Result := True;
                 tqSPPatch.Active := False;
                 tqSPPatch.Active := True;
              except
                     on Error: Exception do
                     begin
                          RollBack;
                          Raise Exception.Create( 'No fue posible agregar el SP, '+ Error.Message );
                          //Application.HandleException( Error );
                     end;
              end;

           end;
        finally

        end;
     end
     else
     begin
           if StrVacio ( tqSPPatch.FieldByName( 'SE_APLICA' ).AsString ) then
              Raise Exception.Create( 'No fue posible agregar el SP ,'+CR_LF +'�Ya existe un Stored Procedure adicional asociado al mismo Tipo y # de Secuencia!')
           else
               Raise Exception.Create( 'No fue posible agregar el SP ,'+CR_LF +'�Ya existe un Stored Procedure adicional asociado  al mismo Tipo , # de  Secuencia  y Empresa');

           Result := False;
     end;
end;


function TdmDBConfig.BorraSPPatch : boolean;
var
   oCursor: TCursor;
   sIsEqual : string;
begin
oCursor := Screen.Cursor;
Screen.Cursor := crHourglass;
Result := False;
with dbEmpresas do
begin
     try

         StartTransaction;
         try
            with upSPPatch do
            begin

                 sIsEqual := '= :SE_APLICA ' ;
                 if ( tqSPPatch.FieldByName('SE_APLICA').IsNull ) then
                 begin
                     sIsEqual := 'is NULL ';
                 end;

                 Query[ukDelete].SQL.Clear;
                 Query[ukDelete].SQL.Add(Format('delete from SP_PATCH ' +
                                   ' where'+
                                   ' (SE_TIPO = :SE_TIPO  and SE_SEQ_NUM = :SE_SEQ_NUM and '+
                                   ' SE_APLICA %s)', [sIsEqual]) );

                 SetParams( ukDelete );
                 ExecSQL( ukDelete );
            end;
            Commit;
            Result := True;
            tqSPPatch.Active := False;
            tqSPPatch.Active := True;
         except
                on Error: Exception do
                begin
                     RollBack;
                     DatabaseError( 'No fue posible borrar el SP: '+ Error.Message );
                     //Application.HandleException( Error );
                end;
         end;
     finally

     end;



end;

Screen.Cursor := oCursor;
end;

procedure TdmDBConfig.tqSPPatchAfterOpen(DataSet: TDataSet);
begin
     inherited;
     with Dataset do
     begin

          with FieldByName( 'SE_APLICA' ) do
          begin
               OnGetText := tqSPPatch_SE_APLICA_GetText;
               Required := False;
          end;
          FieldByName('SE_TIPO').OnGetText :=  tqSPPatch_SE_TIPOGetText;
          FieldByName('SE_TIPO').OnChange := tqSPPatch_SE_TIPOOnChange;
     end;
end;

procedure TdmDBConfig.tqSPPatch_SE_TIPOGetText(Sender: TField; var Text: String; DisplayText: Boolean );
var
   iValue: Integer;
begin
     if ( not  Sender.DataSet.IsEmpty ) and  ( DisplayText ) then
     begin
          iValue := Sender.AsInteger;
          if ( iValue = 0 ) then
             Text := 'Comparte'
          else
              Text := ZetaCommonLists.ObtieneElemento( lfTipoCompanyName, iValue - 1 );
     end;
end;


procedure TdmDBConfig.tqSPPatch_SE_APLICA_GetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
     if not  Sender.DataSet.IsEmpty then
     begin
        if ( Sender.DataSet.FieldByName('SE_TIPO').AsInteger <> 0 ) then
        begin
             if StrVacio( Sender.AsString ) then
                Text := 'Todas'
             else
                 Text  :=  Sender.AsString;
        end;
     end;
end;

procedure TdmDBConfig.tqActualizaBlobUpdateError(DataSet: TDataSet;
  E: EDatabaseError; UpdateKind: TUpdateKind;
  var UpdateAction: TUpdateAction);
begin
DatabaseError(E.Message);
end;

procedure TdmDBConfig.CM_VERSIONGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
     if not  Sender.DataSet.IsEmpty then
     begin
        if ( Sender.AsInteger < 0 ) then
        begin
                Text := '???';
        end
        else
            Text := Sender.AsString;
     end;
end;

function TdmDBConfig.ConsultarSPPatch(iSE_TIPO: integer ; sCM_CODIGO : string ): boolean;
const
     Q_GET_PATCH_QUERY = 'select SE_NOMBRE, SE_SEQ_NUM, SE_DATA, SE_ARCHIVO from SP_PATCH where ( SE_TIPO = %d ) and ( SE_APLICA = '''' or SE_APLICA is NULL or SE_APLICA=%s)  order by SE_SEQ_NUM, SE_APLICA' ;
begin
     Result := False;
     with tqSPPatchToApply do
     begin
          try
             Active := False;
             with SQL do
             begin
                  Clear;
                  Add( Format(Q_GET_PATCH_QUERY, [iSE_TIPO, EntreComillas( sCM_CODIGO ) ])  );
             end;
             Active := True;
          finally

          end;
     end;
end;

procedure TdmDBConfig.tqSPPatch_SE_TIPOOnChange(Sender: TField);
begin
     with Sender do
     begin
          //if ( DataSet.FieldByName('SE_TIPO').AsInteger = 0 ) then
          DataSet.FieldByName('SE_APLICA').AsString := VACIO;
     end;
end;

function TdmDBConfig.CuentaSPPatch(iSE_TIPO: integer;
  sCM_CODIGO: string): integer;
const
     Q_GET_PATCH_QUERY_CUANTOS = 'select count(*) as CUANTOS from SP_PATCH where ( SE_TIPO = %d ) and ( SE_APLICA = '''' or SE_APLICA is NULL or SE_APLICA=%s)' ;
begin
     with tqSPPatchToApply do
     begin
          try
             Active := False;
             try
                with SQL do
                begin
                     Clear;
                     Add( Format(Q_GET_PATCH_QUERY_CUANTOS, [iSE_TIPO, EntreComillas( sCM_CODIGO ) ])  );
                end;
                Active := True;
                Result := FieldByName('CUANTOS').AsInteger;
             except
                   on Error: Exception do
                   begin
                       Result := -1;
                   end;
             end;
          finally
             Active := False;
          end;
     end;
end;

procedure TdmDBConfig.OnChangeCM_CTRL_RL(Sender: TField);
begin
     with Sender do
     begin
          if ( DataSet.FieldByName('CM_CTRL_RL').AsString = '3PRUEBA' ) then
             DataSet.FieldByName('CM_CONTROL').AsString := '3DATOS'
          else
             DataSet.FieldByName('CM_CONTROL').AsString := DataSet.FieldByName('CM_CTRL_RL').AsString;


     end;
end;

function TdmDBConfig.ExisteChecksum: boolean;
const
{$ifdef INTERBASE}
 Q_EXISTE_CHECKSUM = 'select count(*) as CUANTOS from RDB$RELATION_FIELDS where RDB$RELATION_Name = ''COMPANY'' and RDB$Field_name = ''CM_CHKSUM'' ';
{$endif}
{$ifdef MSSQL}
 Q_EXISTE_CHECKSUM = 'select count(*) as CUANTOS from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME=''COMPANY'' and COLUMN_NAME=''CM_CHKSUM'' ';
{$endif}
begin
   with tqSPPatchToApply do
   begin
        try
           Active := False;
           try
              with SQL do
              begin
                   Clear;
                   Add( Q_EXISTE_CHECKSUM );
              end;
              Active := True;
              Result := FieldByName('CUANTOS').AsInteger > 0 ;
           except
                 on Error: Exception do
                 begin
                     Result := False;
                 end;
           end;
        finally
           Active := False;
        end;
   end;
end;

function TdmDBConfig.EsValidoOldChecksum : Boolean;
var
  sCompany, sControlReal, sControl, sDatos : String;
  dFecha : TDate;
  sCheckSum : string;
begin
     with dbEmpresas do
     begin
          sCheckSum := tqCompany.FieldByName( 'CM_CHKSUM' ).OldValue;
          ZetaServerTools.GetCompanyDataFromChecksum( sCheckSum, sCompany, sControlReal, sControl, sDatos, dFecha );

          with tqCompany do
          begin
               Result := ( sCompany = FieldByName('CM_CODIGO').OldValue  ) and
                         ( sControlReal = FieldByName('CM_CTRL_RL').OldValue  ) and
                         ( sControl = FieldByName('CM_CONTROL').OldValue ) and
                         ( sDatos = FieldByName('CM_DATOS').OldValue );

          end;

     end;
end;

function TdmDBConfig.SetChecksum( lPuedeCorregir : Boolean; var lCorrigio : Boolean )  : Boolean;
var
   sCM_CODIGO, sCheckSum , sCM_CTRL_RL, sCM_CONTROL, sCM_CHKSUM: string;
   lGeneraCheckSum : boolean;
begin

    Result := TRUE;
    lGeneraCheckSum := FALSE;

     with dbEmpresas do
     begin
          sCM_CODIGO := GetCompany;

          with tqCompany do
          begin
              sCM_CTRL_RL := FieldByName( 'CM_CTRL_RL' ).AsString;
              sCM_CONTROL := FieldByName( 'CM_CONTROL' ).AsString;
              sCM_CHKSUM := FieldByName( 'CM_CHKSUM' ).AsString;

              if ( FEsCheckSumInicial ) then
              begin
                   lGeneraCheckSum := TRUE;
              end;
              if ( ( strVacio( sCM_CTRL_RL ) and strVacio( sCM_CONTROL ) ) or strVacio( sCM_CHKSUM)  ) and ( lPuedeCorregir ) then
              begin
                   lGeneraCheckSum := TRUE;
                   sCM_CTRL_RL := '3DATOS';
                   sCM_CONTROL := '3DATOS';
                   lCorrigio := True;
               end;

          end;


          if lGeneraCheckSum  then
          begin
              with tqCompany do
              begin
                  sCheckSum := ZetaServerTools.GetCompanyChecksumFromData( sCM_CODIGO,sCM_CTRL_RL, sCM_CONTROL, FieldByName( 'CM_DATOS'  ).AsString , Now);
              end;

              StartTransaction;
              try
                 with tqActualizaChecksum do
                 begin
                      with SQL do
                      begin
                           Clear;
                           Add( Format( 'update COMPANY set CM_CHKSUM = %s, CM_CONTROL = %s , CM_CTRL_RL = %s where ( CM_CODIGO = %s )', [ EntreComillas( sCheckSum), EntreComillas( sCM_CONTROL ), EntreComillas( sCM_CTRL_RL ), EntreComillas( sCM_CODIGO ) ] ) );
                      end;
                      ExecSQL;
                 end;
                 Result := True;
                 Commit;
              except
                    on Error: Exception do
                    begin
                         Rollback;
                         Result := False;
                         Application.HandleException( Error );
                    end;
              end;
          end;
     end;
end;

function TdmDBConfig.EjecutaScriptComparte(sScript: string): boolean;
begin
     with dbEmpresas do
     begin
          StartTransaction;
          try
             with tqScriptComparteAux do
             begin
                  with SQL do
                  begin
                       Clear;
                       Add( sScript );
                  end;
                  ExecSQL;
             end;
             Result := True;
             Commit;
          except
                on Error: Exception do
                begin
                     Rollback;
                     Result := False;
                end;
          end;
     end;
end;

function TdmDBConfig.CrearEstructurasChecksum: boolean;
begin
   with dmDBCOnfig do
   begin
       if ( not ExisteChecksum ) then
       begin
            EjecutaScriptComparte( Q_AGREGAR_CM_CTRL_RL );
            EjecutaScriptComparte( Q_AGREGAR_CM_CHKSUM );
            EjecutaScriptComparte( Q_INIT_CM_CTRL_RL_1 );
            EjecutaScriptComparte( Q_INIT_CM_CTRL_RL_2 );
            {$ifdef MSSQL}
            EjecutaScriptComparte( Q_INIT_CM_CHKSUM_3 );
            EjecutaScriptComparte( Q_AGREGAR_CM_CTRL_RL_AFTER );
            EjecutaScriptComparte( Q_AGREGAR_CM_CHKSUM_AFTER );
            {$endif}
            FEsCheckSumInicial := TRUE;
       end
       else
           FEsCheckSumInicial := FALSE;
   end;
   Result := dmDBCOnfig.ExisteChecksum;
end;

function TdmDBConfig.ActualizarCheckSumTodasCompanys : boolean;
var
   lCorrigio, lPuedeCorregir : boolean;
begin
   Result := True;


   SetAutoServer( Self.AutoServer );
   FDBConfigMgr.LoadDbConfig;
   lPuedeCorregir := FDBConfigMgr.PuedeAgregarDB;
   lCorrigio := FALSE;

   with tqCompany do
   begin
       DisableControls;
       First;
       while not Eof do
       begin
           if not SetChecksum(lPuedeCorregir,  lCorrigio )  then
               Result := False;
            Next;
       end;
       First;
       EnableControls;
   end;

   if ( lPuedeCorregir  ) and ( lCorrigio ) and ( not FEsCheckSumInicial )  then
   begin
       FDBConfigMgr.IncrementarDB;
   end;
   FDBConfigMgr.SaveDbConfig;
end;

function TdmDBConfig.EsDbConfigInicial: boolean;
begin
     Result := {FEsCheckSumInicial and} not ExistenKeys;

     //Situacioens que pueden apoyar
     //0 un registro especial generado por el instalador

     //Situaciones que refutan el caso de que sea Inicial
     //1 Existe el Patch 430 instalado
     //2 Existe una BD de empresas con patch > 430
end;

function TdmDBConfig.ReadDBConfigFromDatabase(
  iKey: integer): string;
begin
//Ejecuta un Selecto from Funcion
   Result := VACIO;

   try
       with tqScriptComparteAux do
      begin
           try
              Active := False;
              with SQL do
              begin
                   Clear;
                   {$ifdef MSSQL}
                   Add( Format(Q_GET_KEY_REPORTE, [ 'GET_REPORTE_NIVEL', EntreComillas( DB_KEYS_REPORTE[iKey] ) ] )  );
                   {$else}
                   Add( Format(Q_GET_KEY, [ DB_KEYS[iKey] ] )  );
                   {$endif}
              end;
              Active := True;
              while not Eof do
              begin
                   Result := FieldByName( 'VALOR' ).AsString;
                   Next;
              end;
           finally
                  Active := False;
           end;
      end;

   except
         on Error: Exception do
         begin
              tqScriptComparteAux.Active := False;
              Result := VACIO;
         end;
   end;

end;

function TdmDBConfig.ReadRegistryChecksum: string;
begin
  try
     Result := dmDBConfig.Registry.ChecksumRaw;
  except
      on Error: Exception do
      begin
           Result := VACIO;
      end;
  end;
end;

function TdmDBConfig.ReadRegistryComparteBD: string;
begin
  try
     Result := UpperCase(dmDBConfig.Registry.Database );
  except
      on Error: Exception do
      begin
           Result := VACIO;
      end;
  end;
end;


function TdmDBConfig.WriteDBConfigFromDatabase(
  iKey: integer; encryptedValue: string): boolean;
begin
   //Borra la funcion anterior
   if ( iKey >= 0 ) and ( iKey <= 9 ) then
   begin
      if ExisteKeyEnBD( iKey ) then
            EjecutaScriptComparte( Format ( Q_DROP_KEY , [DB_KEYS[iKey]] ));
      Result := EjecutaScriptComparte( Format ( Q_CREATE_KEY , [DB_KEYS[iKey], encryptedValue] ))
   end
   else
       Result := False;

   {$ifdef MSSQL}
   if not ExisteKeyEnBD( N_EXISTE_KEY_REPORTE ) then
      EjecutaScriptComparte( Decrypt( Q_CREATE_KEY_REPORTE_CRYPT ) )
   {$endif}
end;

function TdmDBConfig.WriteRegistryChecksum(
  encryptedValue: string): boolean;
begin
     try
        dmDBConfig.Registry.ChecksumRaw := encryptedValue;
        Result := True;
     except
         on Error: Exception do
         begin
              Result := False;
         end;
     end;
end;

procedure TdmDBConfig.SetAutoServer(value: TAutoServer);
begin
     Self.FAutoServer := value;
     Self.FGlobalLicense.AutoServer := value;
end;

function TdmDBConfig.ExisteKeyEnBD( iKey : integer ) : Boolean;
begin
   Result := False;
   try
      with tqScriptComparteAux do
      begin
           try
              Active := False;
              with SQL do
              begin
                   Clear;
                   {$ifdef MSSQL}
                   if ( iKey = N_EXISTE_KEY_REPORTE ) then
                      Add( Q_EXISTE_KEY_REPORTE  )
                   else
                   {$endif}
                       Add( Format(Q_EXISTE_KEY, [ DB_KEYS[iKey] ] )  );
              end;
              Active := True;
              while not Eof do
              begin
                   Result := FieldByName( 'CUANTOS' ).AsInteger > 0 ;
                   Next;
              end;
           finally
                  Active := False;
           end;
      end;

   except
         on Error: Exception do
         begin
              Result := False;
         end;
   end;
end;



function TdmDBConfig.ExistenKeys: Boolean;
var
    iKey : integer;
begin
   //Borra la funcion anterior
   Result := False;
   for iKey:= 0 to 9 do
   begin
      if ExisteKeyEnBD( iKey ) then
         Result := True;
   end;
end;

function TdmDBConfig.GetTipoEmpresaReal: eTipoCompany;
var
   sValor: String;
   iValor: integer;
begin
     with tqCompany do
     begin
          sValor := FieldByName( 'CM_CTRL_RL' ).AsString;
          if ZetaCommonTools.StrVacio( sValor ) then
             Result :=  tc3Datos
          else
          begin
              iValor := FTipos.IndexOfName( AnsiUpperCase( sValor ) );
              if ( iValor  < 0 ) then
                 Result := tc3Datos
              else
                  Result := eTipoCompany( iValor );
          end;
     end;
end;


function TdmDBConfig.ContarDuplicidadBD: integer;
var
   sCM_CODIGO : string;
   sCM_DATOS : string;
   sCM_CONTROL : string;
   sCM_CTRL_RL : string;
   sCM_CTRL_RL_Contrario : string;
begin
   Result := 0;

   sCM_CODIGO := tqCompany.FieldByName('CM_CODIGO').AsString;
   sCM_DATOS :=  tqCompany.FieldByName('CM_DATOS').AsString;
   sCM_CONTROL := '3DATOS';
   sCM_CTRL_RL := tqCompany.FieldByName('CM_CTRL_RL').AsString;

   if ( sCM_CTRL_RL = '3DATOS' ) then
      sCM_CTRL_RL_Contrario := '3PRUEBA'
   else
   if ( sCM_CTRL_RL = '3PRUEBA' ) then
      sCM_CTRL_RL_Contrario := '3DATOS'
   else
       sCM_CTRL_RL_Contrario := VACIO;

   if StrLleno ( sCM_CTRL_RL_Contrario)  then
   begin
      try
         with tqValidaDuplicidadBD do
         begin
              try
                 Active := False;
                 with SQL do
                 begin
                      Clear;
                      Add( Format(Q_CUENTA_DUPLICIDAD_BD, [ sCM_DATOS, sCM_CONTROL, sCM_CTRL_RL, sCM_CODIGO ] )  );
                 end;
                 Active := True;
                 while not Eof do
                 begin
                      Result := FieldByName( 'CUANTOS' ).AsInteger;
                      Next;
                 end;
              finally
                     Active := False;
              end;
         end;

      except
            on Error: Exception do
            begin
                 Result := 999;
            end;
      end;
   end;
end;



{$ifdef ANTES}
function TdmDBConfig.ContarConfiguracionBDProduccion: integer;
var
   sCM_DATOS : string;
begin
   Result := 0;

   sCM_DATOS :=  tqCompany.FieldByName('CM_DATOS').AsString;

   try
      with tqValidaDuplicidadBD do
      begin
           try
              Active := False;
              with SQL do
              begin
                   Clear;
                   Add( Format(Q_CUENTA_CONFIG_BD_PROD, [ sCM_DATOS ] )  );
              end;
              Active := True;
              while not Eof do
              begin
                   Result := FieldByName( 'CUANTOS' ).AsInteger;
                   Next;
              end;
           finally
                  Active := False;
           end;
      end;

   except
         on Error: Exception do
         begin
              Result := 999;
         end;
   end;
end;
{$else}
function TdmDBConfig.ContarConfiguracionBDProduccion: integer;
var
   sCM_DATOS : string;
   sCompany, sControlReal, sControl, sDatos : String;
   dFecha : TDate;
   sCheckSum : string;
begin
   Result := 0;

   sCM_DATOS :=  tqCompany.FieldByName('CM_DATOS').AsString;

   // Q_VALIDA_CONFIG_BD_PROD = 'SELECT CM_CODIGO, CM_CONTROL, CM_DATOS, CM_CTRL_RL  from COMPANY where UPPER(CM_DATOS) = UPPER( ''%s'' ) and CM_CONTROL = ''3DATOS'' and CM_CTRL_RL = ''3DATOS''  ';

   if StrVacio( sCM_DATOS ) then
      Result := 1  //Para que no sea tomado en cuenta 
   else
   begin
      try
         with tqValidaDuplicidadBD do
         begin
              try
                 Active := False;
                 with SQL do
                 begin
                      Clear;
                      Add( Format(Q_VALIDA_CONFIG_BD_PROD, [ sCM_DATOS ] )  );
                 end;
                 Active := True;
                 while not Eof do
                 begin
                      sCheckSum := FieldByName( 'CM_CHKSUM' ).AsString;

                      ZetaServerTools.GetCompanyDataFromChecksum( sCheckSum, sCompany, sControlReal, sControl, sDatos, dFecha );
                      if  ( sCompany = FieldByName('CM_CODIGO').AsString  ) and ( sControlReal = FieldByName('CM_CTRL_RL').AsString  ) and
                          ( sControl = FieldByName('CM_CONTROL').AsString ) and ( sDatos = FieldByName('CM_DATOS').AsString ) then
                      begin
                           Inc(Result);
                      end;

                      Next;
                 end;
              finally
                     Active := False;
              end;
         end;

      except
            on Error: Exception do
            begin
                 Result := 0;
            end;
      end;
   end;
end;
{$endif}





function TdmDBConfig.SeleccionLeer(Manager: TLicenseMgr;
  Provider: TdmZetaServerProvider; Respuesta: TCallBack;
  RespuestaError: TCallBackError) : TDatosSeleccion;
var
   FDatosSeleccion: TDatosSeleccion;
   sEmpresa, sRazonSocial, sBD : string;
   lOk : boolean;
begin

     with Result do
     begin
          BDCuantos := 0;
          RequisicionCuantos := 0;
          RequisicionUltimo := 0;
     end;

     with cdsEmpresas do
     begin
          First;
          {$ifdef VALIDA_EMPLEADOS}
          while not Eof and lOk do
          {$else}
          while not Eof do
          {$endif}
          begin
               sEmpresa := FieldByName( 'CM_CODIGO' ).AsString;
               sRazonSocial := FieldByName( 'CM_NOMBRE' ).AsString;
               sBD := FieldByName( 'CM_DATOS' ).AsString;

               lOk := True;

               try
                  Respuesta( Format( 'Leyendo %s', [ sEmpresa ] ) );
                  with Provider do
                  begin
                       Deactivate;     //Se agrega para descontaminar los accesos pasados
                       Activate;
                       EmpresaActiva := BuildEmpresa( dmDBConfig.cdsEmpresas );
                       with Manager do
                       begin
                            FDatosSeleccion := RegistrosSeleccion;
                       end;
                       Deactivate;
                  end;
               except
                     on Error: Exception do
                     begin
                          RespuestaError( Format( 'No Fu� Posible Leer La BD de Seleccion de Personal %s: ' + CR_LF + '%s', [ sEmpresa, sRazonSocial ] ), Error );
                          lOk := False;
                     end;
               end;
               if lOk then
               begin
                     with Result do
                     begin
                         BDCuantos := BDCuantos + 1;
                         RequisicionCuantos := RequisicionCuantos + FDatosSeleccion.RequisicionCuantos;
                         if ( FDatosSeleccion.RequisicionUltimo > RequisicionUltimo ) then
                            RequisicionUltimo := FDatosSeleccion.RequisicionUltimo;
                     end;
               end
               else
               begin
               end;
               Next;
          end;
          First;
     end;
end;

function TdmDBConfig.VisitantesLeer(Manager: TLicenseMgr;
  Provider: TdmZetaServerProvider; Respuesta: TCallBack;
  RespuestaError: TCallBackError) : TDatosVisitantes;
var
   FDatosVisitantes: TDatosVisitantes;
   sEmpresa, sRazonSocial, sBD : string;
   lOk : boolean;
begin

     with Result do
     begin
          BDCuantos := 0;
          LibroCuantos := 0;
          LibroUltimo := 0;
     end;

     with cdsEmpresas do
     begin
          First;
          {$ifdef VALIDA_EMPLEADOS}
          while not Eof and lOk do
          {$else}
          while not Eof do
          {$endif}
          begin
               sEmpresa := FieldByName( 'CM_CODIGO' ).AsString;
               sRazonSocial := FieldByName( 'CM_NOMBRE' ).AsString;
               sBD := FieldByName( 'CM_DATOS' ).AsString;

               lOk := True;

               try
                  Respuesta( Format( 'Leyendo %s', [ sEmpresa ] ) );
                  with Provider do
                  begin
                       Deactivate;     //Se agrega para descontaminar los accesos pasados
                       Activate;
                       EmpresaActiva := BuildEmpresa( dmDBConfig.cdsEmpresas );
                       with Manager do
                       begin
                            FDatosVisitantes := RegistrosVisitantes;
                       end;
                       Deactivate;
                  end;
               except
                     on Error: Exception do
                     begin
                          RespuestaError( Format( 'No Fu� Posible Leer La BD de Visitantes %s: ' + CR_LF + '%s', [ sEmpresa, sRazonSocial ] ), Error );
                          lOk := False;
                     end;
               end;
               if lOk then
               begin
                     with Result do
                     begin
                         BDCuantos := BDCuantos + 1;
                         LibroCuantos := LibroCuantos + FDatosVisitantes.LibroCuantos;
                         if ( FDatosVisitantes.LibroUltimo > LibroUltimo ) then
                            LibroUltimo := FDatosVisitantes.LibroUltimo;
                     end;
               end
               else
               begin
               end;
               Next;
          end;
          First;
     end;
end;

function TdmDBConfig.GetDBConfigMgr: TDbConfigManager;
begin
     SetAutoServer( Self.AutoServer );
     FDBConfigMgr.LoadDbConfig;
     Result := FDBConfigMgr;
end;

end.
