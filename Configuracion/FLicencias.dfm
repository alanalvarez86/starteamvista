inherited LicenseAllocation: TLicenseAllocation
  Left = 248
  Top = 178
  Width = 627
  Height = 364
  BorderIcons = [biSystemMenu, biMaximize]
  BorderStyle = bsSizeable
  Caption = 'Asignar Licencias de Empleados a Empresas'
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 273
    Width = 611
    Height = 34
    BevelOuter = bvNone
    inherited OK: TBitBtn
      Left = 448
      Top = 3
      Width = 79
      Height = 29
      ModalResult = 0
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 537
      Top = 3
      Width = 79
      Height = 29
    end
  end
  object ZetaDBGrid: TZetaDBGrid
    Left = 0
    Top = 0
    Width = 520
    Height = 273
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection]
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = ZetaDBGridDblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'CM_CODIGO'
        Title.Caption = 'C'#243'digo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CM_NOMBRE'
        Title.Caption = 'Nombre'
        Width = 260
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CM_ASIGNED'
        Title.Caption = 'Licencias'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CM_ACTIVOS'
        Title.Caption = 'Empleados'
        Width = 62
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CM_INACTIV'
        Title.Caption = 'Inactivos'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CM_TOTAL'
        Title.Caption = 'Total'
        Visible = True
      end>
  end
  object PanelSuperior: TPanel
    Left = 520
    Top = 0
    Width = 91
    Height = 273
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 2
    object LicenciasGB: TGroupBox
      Left = 0
      Top = 67
      Width = 91
      Height = 206
      Align = alBottom
      Caption = ' Licencias  '
      TabOrder = 0
      object LicenciaTotalLBL: TLabel
        Left = 8
        Top = 17
        Width = 27
        Height = 13
        Alignment = taRightJustify
        Caption = 'Total:'
      end
      object AsignadasLBL: TLabel
        Left = 8
        Top = 50
        Width = 64
        Height = 13
        Alignment = taRightJustify
        Caption = '(-) Asignadas:'
      end
      object Asignadas: TZetaTextBox
        Left = 8
        Top = 67
        Width = 72
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Asignadas'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object LicenciaTotal: TZetaTextBox
        Left = 8
        Top = 31
        Width = 72
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'LicenciaTotal'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object DisponiblesLBL: TLabel
        Left = 8
        Top = 86
        Width = 72
        Height = 13
        Alignment = taRightJustify
        Caption = '(=) Disponibles:'
      end
      object Disponibles: TZetaTextBox
        Left = 8
        Top = 101
        Width = 72
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Disponibles'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
    end
    object Asignar: TBitBtn
      Left = 5
      Top = 5
      Width = 83
      Height = 25
      Hint = 'Asignar Licencias A La Empresa Seleccionada'
      Caption = 'Asignar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = AsignarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
        000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
        00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
        F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
        0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
        FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
        FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
        0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
        00333377737FFFFF773333303300000003333337337777777333}
      NumGlyphs = 2
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 307
    Width = 611
    Height = 19
    Panels = <
      item
        Text = 'Instalaci'#243'n # 999'
        Width = 100
      end
      item
        Text = 'Intentos: 99'
        Width = 100
      end
      item
        Width = 50
      end>
  end
  object DataSource: TDataSource
    Left = 112
    Top = 73
  end
end
