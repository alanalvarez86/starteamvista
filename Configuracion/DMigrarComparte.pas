unit DMigrarComparte;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBTables,
     DMigrar,
     ZetaAliasManager,
     ZetaMigrar,
     ZetaCommonLists;

type
  TdmMigrarComparte = class(TdmMigrar)
    Grupos: TZetaMigrar;
    Usuario: TZetaMigrar;
    tqUno: TQuery;
    Company: TZetaMigrar;
    procedure dmMigrarCreate(Sender: TObject);
    procedure dmMigrarDestroy(Sender: TObject);
    procedure GruposAdditionalRows(Sender: TObject);
    procedure UsuarioOpenDatasets(Sender: TObject);
    procedure UsuarioMoveExcludedFields(Sender: TObject);
    procedure UsuarioMoveOtherTables(Sender: TObject);
    procedure UsuarioCloseDatasets(Sender: TObject);
    procedure CompanyFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure CompanyMoveExcludedFields(Sender: TObject);
  private
    { Private declarations }
    FCompanyList: TStrings;
    FInterbaseFile: String;
    FDataPath: String;
    FComputerName: String;
    function GetAliasManager: TZetaAliasManager;
    procedure SetMigrarUsuarios( const lValue: Boolean );
  public
    { Public declarations }
    property AliasManager: TZetaAliasManager read GetAliasManager;
    property CompanyList: TStrings read FCompanyList;
    property InterbaseFile: String read FInterbaseFile write FInterbaseFile;
    property DataPath: String read FDataPath write FDataPath;
    property ComputerName: String read FComputerName write FComputerName;
    property MigrarUsuarios: Boolean write SetMigrarUsuarios;
  end;

var
  dmMigrarComparte: TdmMigrarComparte;

implementation

uses DDBConfig,
     ZetaAliasCrear,
     ZetaServerTools,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaODBCTools;

{$R *.DFM}

function FileCopy( const sSource, sDestination: String; var sError: String ): Boolean;
var
   iCode: DWORD;
   pMessage: Pointer;
begin
     if Windows.CopyFile( {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}( sSource ), {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}( sDestination ), False ) then
        Result := True
     else
     begin
          Result := False;
          iCode := GetLastError;
          Windows.FormatMessage( FORMAT_MESSAGE_ALLOCATE_BUFFER or FORMAT_MESSAGE_FROM_SYSTEM,
                                 nil,
                                 iCode,
                                 0,
                                 @pMessage,
                                 0,
                                 nil );
          sError := String( {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}( pMessage ) );
          LocalFree( hLocal( pMessage ) );
     end;
end;

{ ********* Eventos Para Base de Datos Compartida ********* }

procedure TdmMigrarComparte.dmMigrarCreate(Sender: TObject);
begin
     inherited;
     FCompanyList := TStringList.Create;
end;

procedure TdmMigrarComparte.dmMigrarDestroy(Sender: TObject);
begin
     FCompanyList.Free;
     inherited;
end;

function TdmMigrarComparte.GetAliasManager: TZetaAliasManager;
begin
     Result := dmDBConfig.AliasManager;
end;

procedure TdmMigrarComparte.SetMigrarUsuarios( const lValue: Boolean );
begin
     with Usuario do
     begin
          Enabled := lValue;
          EmptyTargetTable := lValue;
     end;
     with Grupos do
     begin
          Enabled := lValue;
          EmptyTargetTable := lValue;
     end;
end;

procedure TdmMigrarComparte.GruposAdditionalRows(Sender: TObject);
begin
     inherited;
     case TablaMigrada.Counter of
          1:
          with TargetQuery do
          begin
               ParamByName( 'GR_CODIGO' ).AsInteger := 1;
               ParamByName( 'GR_DESCRIP' ).AsString := 'Administradores';
          end;
          2:
          with TargetQuery do
          begin
               ParamByName( 'GR_CODIGO' ).AsInteger := 2;
               ParamByName( 'GR_DESCRIP' ).AsString := 'Usuarios';
          end;
     end;
end;

procedure TdmMigrarComparte.UsuarioOpenDatasets(Sender: TObject);
begin
     inherited;
     with TablaMigrada do
     begin
          PrepareTargetQuery( tqUno, 'insert into USER_BIT( US_CODIGO, UB_CODIGO ) values ( :US_CODIGO, :UB_CODIGO )' );
     end;
end;

procedure TdmMigrarComparte.UsuarioMoveExcludedFields(Sender: TObject);
var
   sNombre, sValor, sBitLst: String;
   iNivel, iValor: Integer;
begin
     with Source do
     begin
          sNombre := FieldByName( 'US_NOMBRE' ).AsString;
          sValor := FieldByName( 'US_HASH' ).AsString;
          iNivel := FieldByName( 'US_NIVEL' ).AsInteger;
          if EsVer260 then
             sBitLst := ZetaCommonTools.zBoolToStr( StrLleno( FieldByName( 'US_SUPERS' ).AsString ) )
          else
              sBitLst := K_GLOBAL_NO;
     end;
     if ( iNivel = 1 ) then
        iValor := 1
     else
         iValor := 2;
     with TargetQuery do
     begin
          ParamByName( 'US_CODIGO' ).AsInteger := StrAsInteger( sValor );
          ParamByName( 'US_CORTO' ).AsString := sValor;
          ParamByName( 'US_NOMBRE' ).AsString := sNombre;
          ParamByName( 'US_NIVEL' ).AsInteger := iNivel;
          ParamByName( 'GR_CODIGO' ).AsInteger := iValor;
          ParamByName( 'US_PASSWRD' ).AsString := ZetaServerTools.Encrypt( sValor );
          ParamByName( 'US_BLOQUEA' ).AsString := zBoolToStr( False );
          ParamByName( 'US_CAMBIA' ).AsString := zBoolToStr( True );
          ParamByName( 'US_DENTRO' ).AsString := zBoolToStr( False );
          ParamByName( 'US_BIT_LST' ).AsString := sBitLst;
     end;
     Bitacora.AgregaMensaje( Format( 'Usuario %s: Nombre Corto y Password: %s', [ sNombre, sValor ] ) );
end;

procedure TdmMigrarComparte.UsuarioMoveOtherTables(Sender: TObject);
var
   lLista: Boolean;
   i, iUsuario: Integer;
begin
     inherited;
     if EsVer260 then
     begin
          with TargetQuery do
          begin
               lLista := ZetaCommonTools.zStrToBool( ParamByName( 'US_BIT_LST' ).AsString );
               iUsuario := ParamByName( 'US_CODIGO' ).AsInteger;
          end;
          if lLista then
          begin
               with FLista do
               begin
                    CommaText := Source.FieldByName( 'US_SUPERS' ).AsString;
                    for i := 0 to ( Count - 1 ) do
                    begin
                         with tqUno do
                         begin
                              ParamByName( 'US_CODIGO' ).AsInteger := iUsuario;
                              ParamByName( 'UB_CODIGO' ).AsInteger := StrAsInteger( Strings[ i ] );
                              ExecSQL;
                         end;
                    end;
               end;
          end;
     end;
end;

procedure TdmMigrarComparte.UsuarioCloseDatasets(Sender: TObject);
begin
     inherited;
     with TablaMigrada do
     begin
          CloseQuery( tqUno );
     end;
end;

procedure TdmMigrarComparte.CompanyFilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
     inherited;
     Accept := ( FCompanyList.IndexOf( Dataset.FieldByName( 'CM_NOMBRE' ).AsString ) >= 0 );
end;

procedure TdmMigrarComparte.CompanyMoveExcludedFields(Sender: TObject);
var
   iCodigo, iAcumula: Integer;
   sNombre, sAlias, sDatabase, sFileName, sError: String;
begin
     inherited;
     with Source do
     begin
          iCodigo := StrAsInteger( FieldByName( 'CM_NUMERO' ).AsString );
          iAcumula := StrAsInteger( FieldByName( 'CM_ACUMULA' ).AsString );
          sNombre := FieldByName( 'CM_NOMBRE' ).AsString;
          sAlias := Format( 'Tress %d', [ iCodigo ] );
          sFileName := Format( '%s%s.gdb', [ FDataPath, sAlias ] );
          sDatabase := ZetaAliasManager.BuildFullDatabaseName( sFileName, FComputerName );
     end;
     if not FileCopy( FInterbaseFile, sFileName, sError ) then
        Bitacora.AgregaError( Format( 'Error Al Copiar %s Hacia %s: %s', [ FInterbaseFile, sFileName, sError ] ) );
     with AliasManager do
     begin
          if CreateAlias( sAlias, sDatabase, TargetUserName, sError ) then
          begin
               if not ZetaODBCTools.SetIntersolvDataSource( sAlias, sAlias, sDatabase, sError ) then
                  Bitacora.AgregaError( Format( 'Error Al Crear ODBC Datasource %s: %s', [ sAlias, sError ] ) );
          end
          else
              Bitacora.AgregaError( Format( 'Error Al Crear Alias %s: %s', [ sAlias, sError ] ) );
     end;
     with TargetQuery do
     begin
          ParamByName( 'CM_CODIGO' ).AsInteger := iCodigo;
          ParamByName( 'CM_NOMBRE' ).AsString := sNombre;
          ParamByName( 'CM_ACUMULA' ).AsInteger := iAcumula;
          ParamByName( 'CM_ALIAS' ).AsString := sAlias;
          ParamByName( 'CM_DATOS' ).AsString := sDatabase;
          ParamByName( 'CM_USRNAME' ).AsString := TargetUserName;
          ParamByName( 'CM_PASSWRD' ).AsString := ZetaServerTools.Encrypt( TargetPassword );
     end;
end;

end.
