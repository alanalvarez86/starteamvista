object SentinelWrite: TSentinelWrite
  Left = 690
  Top = 273
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Actualizar Sentinel'
  ClientHeight = 305
  ClientWidth = 320
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 320
    Height = 265
    ActivePage = tabAutorizacionVirtual
    Align = alClient
    TabOrder = 0
    object tabAutorizacion: TTabSheet
      Caption = 'Autorizaci'#243'n'
      object EmpresaLBL: TLabel
        Left = 21
        Top = 23
        Width = 54
        Height = 13
        Alignment = taRightJustify
        Caption = '# Empresa:'
      end
      object Label1: TLabel
        Left = 37
        Top = 43
        Width = 38
        Height = 13
        Alignment = taRightJustify
        Caption = '&Versi'#243'n:'
        FocusControl = Version
      end
      object UsuariosLBL: TLabel
        Left = 31
        Top = 65
        Width = 44
        Height = 13
        Alignment = taRightJustify
        Caption = '&Usuarios:'
        FocusControl = Usuarios
      end
      object EmpleadosLBL: TLabel
        Left = 20
        Top = 87
        Width = 55
        Height = 13
        Alignment = taRightJustify
        Caption = '&Empleados:'
        FocusControl = Empleados
      end
      object ModulosLBL: TLabel
        Left = 24
        Top = 153
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = '&Par'#225'metro:'
        FocusControl = Parametro
      end
      object Clave1LBL: TLabel
        Left = 29
        Top = 175
        Width = 46
        Height = 13
        Caption = 'Clave #&1:'
        FocusControl = Clave1
      end
      object Clave2LBL: TLabel
        Left = 29
        Top = 198
        Width = 46
        Height = 13
        Caption = 'Clave #&2:'
        FocusControl = Clave2
      end
      object SentinelLBL: TLabel
        Left = 24
        Top = 4
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Sentinel #:'
      end
      object Sentinel: TZetaTextBox
        Left = 77
        Top = 2
        Width = 65
        Height = 18
        AutoSize = False
        Caption = 'Sentinel'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object Empresa: TZetaTextBox
        Left = 77
        Top = 21
        Width = 65
        Height = 17
        AutoSize = False
        Caption = 'Empresa'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object SQLEngineLBL: TLabel
        Left = 2
        Top = 131
        Width = 73
        Height = 13
        Alignment = taRightJustify
        Caption = 'Base de Datos:'
      end
      object PlataformaLBL: TLabel
        Left = 22
        Top = 109
        Width = 53
        Height = 13
        Alignment = taRightJustify
        Caption = 'Plataforma:'
      end
      object Clave1: TEdit
        Left = 77
        Top = 171
        Width = 149
        Height = 21
        TabOrder = 6
      end
      object Clave2: TEdit
        Left = 77
        Top = 194
        Width = 149
        Height = 21
        TabOrder = 7
      end
      object Version: TComboBox
        Left = 77
        Top = 39
        Width = 83
        Height = 21
        ItemHeight = 13
        TabOrder = 0
        Text = 'Version'
        Items.Strings = (
          '2.0'
          '1.3')
      end
      object Usuarios: TZetaNumero
        Left = 77
        Top = 61
        Width = 83
        Height = 21
        Mascara = mnEmpleado
        TabOrder = 1
      end
      object Empleados: TZetaNumero
        Left = 77
        Top = 83
        Width = 83
        Height = 21
        Mascara = mnEmpleado
        TabOrder = 2
      end
      object Parametro: TZetaNumero
        Left = 77
        Top = 149
        Width = 83
        Height = 21
        Mascara = mnEmpleado
        TabOrder = 5
      end
      object Plataforma: TComboBox
        Left = 77
        Top = 105
        Width = 150
        Height = 21
        Style = csDropDownList
        ItemHeight = 0
        TabOrder = 3
      end
      object SQLEngine: TComboBox
        Left = 77
        Top = 127
        Width = 150
        Height = 21
        Style = csDropDownList
        ItemHeight = 0
        TabOrder = 4
      end
    end
    object tabAutorizacionVirtual: TTabSheet
      Caption = 'Autorizaci'#243'n Sentinel Virtual'
      ImageIndex = 2
      object VirtualUsuariosLBL: TLabel
        Left = 31
        Top = 29
        Width = 44
        Height = 13
        Alignment = taRightJustify
        Caption = '&Usuarios:'
        FocusControl = VirtualUsuarios
      end
      object VirtualEmpleadosLBL: TLabel
        Left = 20
        Top = 51
        Width = 55
        Height = 13
        Alignment = taRightJustify
        Caption = '&Empleados:'
        FocusControl = VirtualEmpleados
      end
      object VirtualModulosLBL: TLabel
        Left = 24
        Top = 97
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = '&Par'#225'metro:'
        FocusControl = VirtualParametro
      end
      object VirtualClave1LBL: TLabel
        Left = 29
        Top = 119
        Width = 46
        Height = 13
        Caption = 'Clave #&1:'
        FocusControl = VirtualClave1
      end
      object VirtualClave2LBL: TLabel
        Left = 29
        Top = 141
        Width = 46
        Height = 13
        Caption = 'Clave #&2:'
        FocusControl = VirtualClave2
      end
      object Label14: TLabel
        Left = 85
        Top = 164
        Width = 116
        Height = 13
        Alignment = taCenter
        Caption = 'Servidor de Pruebas'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
        WordWrap = True
      end
      object Label15: TLabel
        Left = 25
        Top = 5
        Width = 52
        Height = 13
        Alignment = taRightJustify
        Caption = 'Servidor:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblNombreServer: TLabel
        Left = 79
        Top = 5
        Width = 5
        Height = 13
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 22
        Top = 74
        Width = 53
        Height = 13
        Alignment = taRightJustify
        Caption = 'Plataforma:'
      end
      object VirtualClave1: TEdit
        Left = 77
        Top = 115
        Width = 149
        Height = 21
        TabOrder = 3
      end
      object VirtualClave2: TEdit
        Left = 77
        Top = 137
        Width = 149
        Height = 21
        TabOrder = 4
      end
      object VirtualUsuarios: TZetaNumero
        Left = 77
        Top = 25
        Width = 83
        Height = 21
        Mascara = mnEmpleado
        TabOrder = 0
      end
      object VirtualEmpleados: TZetaNumero
        Left = 77
        Top = 47
        Width = 83
        Height = 21
        Mascara = mnEmpleado
        TabOrder = 1
      end
      object VirtualParametro: TZetaNumero
        Left = 77
        Top = 93
        Width = 83
        Height = 21
        Mascara = mnEmpleado
        TabOrder = 2
      end
      object PlataformaVirtual: TComboBox
        Left = 77
        Top = 70
        Width = 150
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 5
      end
    end
  end
  object panelBotones: TPanel
    Left = 0
    Top = 265
    Width = 320
    Height = 40
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      320
      40)
    object ReadOnlyLBL: TLabel
      Left = 8
      Top = 8
      Width = 123
      Height = 26
      Alignment = taCenter
      AutoSize = False
      Caption = 'No tiene derechos de administrador'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
      WordWrap = True
    end
    object Cancelar: TBitBtn
      Left = 236
      Top = 8
      Width = 75
      Height = 25
      Hint = 'Salir Sin Grabar Sentinela'
      Anchors = [akRight, akBottom]
      Caption = '&Cancelar'
      TabOrder = 0
      Kind = bkCancel
    end
    object OK: TBitBtn
      Left = 157
      Top = 8
      Width = 75
      Height = 25
      Hint = 'Grabar Sentinela'
      Anchors = [akRight, akBottom]
      Caption = '&OK'
      Default = True
      TabOrder = 1
      OnClick = OKClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
  end
end
