unit FConsultor;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, OVCL, OCL, ExtCtrls, Grids,
     DBGrids, Db, ODSI, AstaDrv2, ComCtrls;

type
  TConsultorODBC = class(TForm)
    Hdbc: THdbc;
    PanelSuperior: TPanel;
    DatasorceLBL: TLabel;
    DSComboBox: TDSComboBox;
    Conectar: TBitBtn;
    Desconectar: TBitBtn;
    UsuarioLBL: TLabel;
    Usuario: TEdit;
    Clave: TEdit;
    ClaveLBL: TLabel;
    DataSource: TDataSource;
    DBGrid: TDBGrid;
    OEDataSet: TOEDataSet;
    StatusBar: TStatusBar;
    QueryGB: TGroupBox;
    PanelSQLButtons: TPanel;
    Ejecutar: TSpeedButton;
    BorrarTodo: TSpeedButton;
    Script: TMemo;
    Splitter1: TSplitter;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ConectarClick(Sender: TObject);
    procedure HdbcAfterConnect(Sender: TObject);
    procedure HdbcAfterDisconnect(Sender: TObject);
    procedure DesconectarClick(Sender: TObject);
    procedure EjecutarClick(Sender: TObject);
    procedure ScriptChange(Sender: TObject);
    procedure BorrarTodoClick(Sender: TObject);
  private
    { Private declarations }
    procedure SetControls(const lConnected: Boolean);
    procedure ManejaExcepcion(Sender: TObject; E: Exception);
  public
    { Public declarations }
  end;

var
  ConsultorODBC: TConsultorODBC;

implementation

uses ZetaCommonTools;

{$R *.DFM}

procedure TConsultorODBC.FormCreate(Sender: TObject);
begin
     Application.OnException := ManejaExcepcion;
end;

procedure TConsultorODBC.FormShow(Sender: TObject);
begin
     with DSComboBox do
     begin
          Populate;
     end;
     SetControls( False );
end;

procedure TConsultorODBC.FormDestroy(Sender: TObject);
begin
     DesconectarClick( Self );
end;

procedure TConsultorODBC.SetControls( const lConnected: Boolean );
begin
     Conectar.Enabled := not lConnected;
     Desconectar.Enabled := lConnected;
     Ejecutar.Enabled := lConnected and ( Script.Lines.Count > 0 );
     BorrarTodo.Enabled := Ejecutar.Enabled;
end;

{$define USE_DSN}
//{$undef USE_DSN}

{$define EASYSOFT_IB7}
{$undef EASYSOFT_IB7}

{$define FIREBIRD}
{$undef FIREBIRD}

procedure TConsultorODBC.ConectarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with HDBC do
        begin
             Connected := False;
             {$ifdef USE_DSN}
             Datasource := DSComboBox.Datasource;
             {$else}
             {$ifdef SQLSERVER}
             Driver := 'SQL Server';
             with Attributes do
             begin
                  Clear;
                  Add( Format( 'Server=%s', [ 'TRESS_2001' ] ) );     // Este dato tambi�n deber� viajar
                  Add( Format( 'Database=%s', [ 'GermanPrueba' ] ) );
                  Add( Format( 'Language=%s', [ 'us_english' ] ) );
             end;
             {$endif}
             {$ifdef EASYSOFT_IB7}
             Driver := 'Easysoft IB7 ODBC';
             with Attributes do
             begin
                  Clear;
                  //Add( Format( 'Database=%s', [ 'DELL-GERMAN:D:\3Win_13\Datos\datos.gdb' ] ) );
                  Add( Format( 'Database=%s', [ 'DELL-MARCO:D:\Datos\mycomparte.gdb' ] ) );
                  Add( Format( 'With_Schema=%d', [ 0 ] ) );
                  Add( Format( 'Nowait=%d', [ 1  ] ) );      { 0 = Off, 1 = On }
                  Add( Format( 'Dialect=%d', [ 3 ] ) );      { 1, 2, 3 }
                  Add( Format( 'OldMetaData=%d', [ 1  ] ) ); { 0 = Off, 1 = On }
                  Add( Format( 'ExecProc=%d', [ 0  ] ) );    { 0 = Off, 1 = On }
                  Add( Format( 'DQuote=%d', [ 0  ] ) );      { 0 = Off, 1 = On }
                  Add( Format( 'ReadOnly=%d', [ 0 ] ) );     { 0 = Off, 1 = On }
                  Add( Format( 'Charset=%s', [ '' ] ) );
                  Add( Format( 'CommitSelect=%d', [ 0 ] ) ); { 0 = Off, 1 = On }
                  Add( Format( 'WithDefault=%d', [ 0 ] ) );  { 0 = Off, 1 = On }
                  Add( Format( 'FlushCommit=%d', [ 0 ] ) );     { 0 = Off, 1 = On }
                  Add( Format( 'PadVarchar=%d', [ 0 ] ) );      { 0 = Off, 1 = On }
             end;
             {$endif}
             {$ifdef FIREBIRD}
             Driver := 'Firebird/Interbase(r) driver';
             with Attributes do
             begin
                  Clear;
                  Add( Format( 'dbname=%s', [ 'DELL-GERMAN:D:\3Win_13\Datos\datos.gdb' ] ) );
             end;
             {$endif}
             {$endif}
             UserName := Usuario.Text;
             Password := Clave.Text;
             Connected := True;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TConsultorODBC.DesconectarClick(Sender: TObject);
begin
     with HDBC do
     begin
          Connected := False;
     end;
end;

procedure TConsultorODBC.HdbcAfterConnect(Sender: TObject);
begin
     SetControls( True );
end;

procedure TConsultorODBC.HdbcAfterDisconnect(Sender: TObject);
begin
     SetControls( False );
end;

procedure TConsultorODBC.ScriptChange(Sender: TObject);
begin
     SetControls( HDBC.Connected );
end;

procedure TConsultorODBC.BorrarTodoClick(Sender: TObject);
begin
     Script.Lines.Clear;
end;

procedure TConsultorODBC.EjecutarClick(Sender: TObject);
var
   oCursor: TCursor;
   dStart: TDateTime;
   sScript: String;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     dStart := Now;
     try
        with OEDataset do
        begin
             try
                Active := False;
                SQL := Script.Lines.Text;
                Prepare;
                {
                SQL.Assign( Script.Lines );
                }
                sScript := UpperCase( SQL );
                if ( AnsiPos( 'INSERT', sScript ) > 0 ) or
                   ( Pos( 'DELETE', sScript ) > 0 ) or
                   ( Pos( 'UPDATE', sScript ) > 0 ) then
                begin
                     Prepare;
                     {
                     ParamByName( 'NO_HORAS' ).AsFloat := 13.245;
                     ParamByName( 'NO_HORAS' ).AsFloat := ZetaCommonTools.MiRound( 13.245, Params.DefPrecision[ 'NO_HORAS' ] );
                     }
                     ExecSQL;
                end
                else
                begin
                     Prepare;
                     Active := True;
                end;
             except
                   on Error: Exception do
                   begin
                        Application.HandleException( Error );
                        Active := False;
                   end;
             end;
        end;
     finally
            StatusBar.SimpleText := FormatDateTime( 'hh:nn:ss:zz', ( Now - dStart ) );
            Screen.Cursor := oCursor;
     end;
end;

procedure TConsultorODBC.ManejaExcepcion( Sender: TObject; E: Exception );
begin
     if E is EODBC then
        EODBC( E ).Detail( 'Error En Base De Datos' )
     else
         ShowMessage( E.Message );
end;

end.


