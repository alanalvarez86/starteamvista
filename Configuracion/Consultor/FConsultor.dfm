object ConsultorODBC: TConsultorODBC
  Left = 192
  Top = 107
  Width = 592
  Height = 406
  ActiveControl = DSComboBox
  Caption = 'Consultor de ODBC'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 213
    Width = 584
    Height = 3
    Cursor = crVSplit
    Align = alTop
  end
  object PanelSuperior: TPanel
    Left = 0
    Top = 0
    Width = 584
    Height = 108
    Align = alTop
    TabOrder = 0
    object DatasorceLBL: TLabel
      Left = 5
      Top = 9
      Width = 63
      Height = 13
      Alignment = taRightJustify
      Caption = '&Data Source:'
      FocusControl = DSComboBox
    end
    object UsuarioLBL: TLabel
      Left = 28
      Top = 31
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = '&Usuario:'
      FocusControl = Usuario
    end
    object ClaveLBL: TLabel
      Left = 10
      Top = 53
      Width = 57
      Height = 13
      Alignment = taRightJustify
      Caption = '&Contraseņa:'
      FocusControl = Clave
    end
    object DSComboBox: TDSComboBox
      Left = 70
      Top = 6
      Width = 228
      Height = 21
      ItemHeight = 13
      TabOrder = 0
    end
    object Conectar: TBitBtn
      Left = 302
      Top = 8
      Width = 100
      Height = 25
      Caption = '&Conectar'
      TabOrder = 3
      OnClick = ConectarClick
      Kind = bkOK
    end
    object Desconectar: TBitBtn
      Left = 303
      Top = 40
      Width = 99
      Height = 25
      Caption = '&Desconectar'
      TabOrder = 4
      OnClick = DesconectarClick
      Kind = bkAbort
    end
    object Usuario: TEdit
      Left = 70
      Top = 28
      Width = 228
      Height = 21
      TabOrder = 1
      Text = 'SYSDBA'
    end
    object Clave: TEdit
      Left = 70
      Top = 50
      Width = 228
      Height = 21
      PasswordChar = '*'
      TabOrder = 2
      Text = 'm'
    end
  end
  object DBGrid: TDBGrid
    Left = 0
    Top = 216
    Width = 584
    Height = 137
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 353
    Width = 584
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object QueryGB: TGroupBox
    Left = 0
    Top = 108
    Width = 584
    Height = 105
    Align = alTop
    Caption = ' SQL '
    TabOrder = 3
    object PanelSQLButtons: TPanel
      Left = 549
      Top = 15
      Width = 33
      Height = 88
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object Ejecutar: TSpeedButton
        Left = 5
        Top = 29
        Width = 23
        Height = 22
        Hint = 'Ejecutar SQL'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
          555555555555555555555555555555555555555555FF55555555555559055555
          55555555577FF5555555555599905555555555557777F5555555555599905555
          555555557777FF5555555559999905555555555777777F555555559999990555
          5555557777777FF5555557990599905555555777757777F55555790555599055
          55557775555777FF5555555555599905555555555557777F5555555555559905
          555555555555777FF5555555555559905555555555555777FF55555555555579
          05555555555555777FF5555555555557905555555555555777FF555555555555
          5990555555555555577755555555555555555555555555555555}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = EjecutarClick
      end
      object BorrarTodo: TSpeedButton
        Left = 5
        Top = 4
        Width = 23
        Height = 22
        Hint = 'Borrar Todo El SQL'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00500005000555
          555557777F777555F55500000000555055557777777755F75555005500055055
          555577F5777F57555555005550055555555577FF577F5FF55555500550050055
          5555577FF77577FF555555005050110555555577F757777FF555555505099910
          555555FF75777777FF555005550999910555577F5F77777775F5500505509990
          3055577F75F77777575F55005055090B030555775755777575755555555550B0
          B03055555F555757575755550555550B0B335555755555757555555555555550
          BBB35555F55555575F555550555555550BBB55575555555575F5555555555555
          50BB555555555555575F555555555555550B5555555555555575}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = BorrarTodoClick
      end
    end
    object Script: TMemo
      Left = 2
      Top = 15
      Width = 547
      Height = 88
      Align = alClient
      Lines.Strings = (
        'select NO_HORAS from NOMINA where'
        '( PE_YEAR = 2000 ) and'
        '( PE_TIPO = 1 ) and'
        '( PE_NUMERO = 1 ) and'
        '( CB_CODIGO = 30 )')
      ScrollBars = ssVertical
      TabOrder = 1
      OnChange = ScriptChange
    end
  end
  object Hdbc: THdbc
    ConnectionPooling = cpDefault
    Version = '5.06'
    AfterConnect = HdbcAfterConnect
    AfterDisconnect = HdbcAfterDisconnect
    Left = 432
    Top = 16
  end
  object DataSource: TDataSource
    AutoEdit = False
    DataSet = OEDataSet
    Left = 512
    Top = 16
  end
  object OEDataSet: TOEDataSet
    hDbc = Hdbc
    hStmt.SkipByPosition = True
    hStmt.SkipByCursor = True
    hStmt.StringTrimming = stTrimNone
    hStmt.Target.Target = (
      'OE5.06'
      ''
      ''
      ''
      '')
    SQL = 
      'select NO_HORAS from NOMINA where'#13#10'( PE_YEAR = 2000 ) and'#13#10'( PE_' +
      'TIPO = 1 ) and'#13#10'( PE_NUMERO = 1 ) and'#13#10'( CB_CODIGO = 30 )'
    ParamCheck = False
    Params = <>
    Cached = True
    CachedUpdates = True
    EmptyFieldToNull = True
    AutoCalcFields = False
    Left = 472
    Top = 16
  end
end
