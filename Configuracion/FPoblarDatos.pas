unit FPoblarDatos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, ComCtrls, Checklst, ExtCtrls, Buttons, FileCtrl, Mask,
     FMigrar,
     ZetaWizard,
     ZetaFecha, jpeg;

type
  TPoblarDatos = class(TMigrar)
    Defaults: TTabSheet;
    DefaultsGB: TGroupBox;
    ListaDefaults: TCheckListBox;
    PathLBL: TLabel;
    Path: TEdit;
    PathSeek: TSpeedButton;
    TablasDefaultsGB: TGroupBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
    procedure WizardAfterMove(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    function CrearPath: String;
  protected
    { Protected declarations }
  public
    { Public declarations }
  end;

var
  PoblarDatos: TPoblarDatos;

implementation

uses ZetaCommonTools,
     ZetaDialogo,
     ZetaTressCFGTools,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaMigrar,
     FHelpContext,
     DMigrar,
     DMigrarDatos;

{$R *.DFM}

{ ********** TPoblarDatos ************ }

procedure TPoblarDatos.FormCreate(Sender: TObject);
begin
     FParadoxPath := Path;
     FListaDefaults := ListaDefaults;
     dmMigracion := TdmMigrarDatos.Create( Self );
     //Path.Text := ZetaTressCFGTools.SetFileNameDefaultPath( CrearPath );
     LogFile.Text := ZetaTressCFGTools.SetFileNameDefaultPath( 'Poblar.log' );
     HelpContext := H00023_Poblando_una_base_de_datos;
     inherited;
end;

procedure TPoblarDatos.FormShow(Sender: TObject);
begin
     inherited;
     Path.Text := ZetaTressCFGTools.SetFileNameDefaultPath( CrearPath );
end;

function TPoblarDatos.CrearPath: String;
var
   sPath, sTipo: String;
begin
     sPath  := '%sConfig';
     case CompanyType of
          tcRecluta: sTipo := 'Seleccion\';
          tcVisitas: sTipo := 'Visitas\';
     else
         sTipo := 'Fuentes\';
     end;
     Result := Format( sPath, [ sTipo ] );
end;

procedure TPoblarDatos.FormDestroy(Sender: TObject);
begin
     inherited;
     dmMigracion.Free;
end;

procedure TPoblarDatos.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     if CanMove then
     begin
          if Wizard.Adelante then
          begin
               if Wizard.EsPaginaActual( DirectorioDOS ) then
                  CanMove := ValidaDirectorioParadox;
          end;
     end;
end;

procedure TPoblarDatos.WizardAfterMove(Sender: TObject);
begin
     inherited;
     if Wizard.EsPaginaActual( Defaults ) then
     begin
          InitListaTablas;
     end;
end;

procedure TPoblarDatos.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
begin
     StartLog;
     lOk := PoblarDatos;
     EndLog;
     inherited;
end;

end.
