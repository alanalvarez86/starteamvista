unit FListaMigracion;

interface

uses  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
      StdCtrls, Buttons, ExtCtrls, Checklst,
      DMigrar;

type
  TListaMigracion = class(TForm)
    PanelInferior: TPanel;
    OK: TBitBtn;
    Cancelar: TBitBtn;
    Population: TGroupBox;
    Migration: TGroupBox;
    Poblables: TCheckListBox;
    Migrables: TCheckListBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
    FMigrarCFG: TMigrarCFG;
  public
    { Public declarations }
  end;

var
  ListaMigracion: TListaMigracion;

implementation

{$R *.DFM}

procedure TListaMigracion.FormCreate(Sender: TObject);
begin
     FMigrarCFG := TMigrarCFG.Create;
end;

procedure TListaMigracion.FormDestroy(Sender: TObject);
begin
     FMigrarCFG.Free;
end;

procedure TListaMigracion.FormShow(Sender: TObject);
begin
     with FMigrarCFG do
     begin
          LoadCheckListBoxPoblar( Poblables );
          LoadCheckListBoxMigrar( Migrables );
     end;
end;

procedure TListaMigracion.OKClick(Sender: TObject);
begin
     with FMigrarCFG do
     begin
          SaveCheckListBoxPoblar( Poblables );
          SaveCheckListBoxMigrar( Migrables );
     end;
end;

end.
