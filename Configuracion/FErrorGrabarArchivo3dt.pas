unit FErrorGrabarArchivo3dt;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  Vcl.Menus, cxShellBrowserDialog, Vcl.StdCtrls, cxButtons, cxTextEdit, cxMemo, Vcl.ExtCtrls, dxSkinsCore, TressMorado2013,
  Vcl.ImgList, cxClasses, dxSkinBlack, dxSkinBlue, dxSkinBlueprint,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue;

type
  TErrorGrabarArchivo3dt = class(TForm)
    memAdvertencia: TcxMemo;
    lblGuardar: TLabel;
    txtArchivo3dt: TcxTextEdit;
    btArchivo3dt: TcxButton;
    cxOpenDialog: TcxShellBrowserDialog;
    panelBotones: TPanel;
    ImageButtons: TcxImageList;
    btOk: TcxButton;
    procedure btArchivo3dtClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btOKClick(Sender: TObject);
  private
    { Private declarations }
    FFileName: String;
  public
    { Public declarations }
    property FileName: String read FFileName write FFileName;
  end;


function EditarArchivo (const sFileName: String): String;

var
  ErrorGrabarArchivo3dt: TErrorGrabarArchivo3dt;

implementation

{$R *.dfm}

function EditarArchivo (const sFileName: String): String;
begin
     with TErrorGrabarArchivo3dt.Create( Application ) do
     begin
          try
             FileName := sFileName;
             ShowModal;
             Result := txtArchivo3dt.Text;
          finally
            Free;
          end;
     end;
end;

procedure TErrorGrabarArchivo3dt.btOKClick(Sender: TObject);
begin
    Close();
end;

procedure TErrorGrabarArchivo3dt.FormShow(Sender: TObject);
begin
    memAdvertencia.Lines.Add(FileName);
    txtArchivo3dt.Text := FileName;
    txtArchivo3dt.SetFocus;
end;

procedure TErrorGrabarArchivo3dt.btArchivo3dtClick(Sender: TObject);
var sFileName: String;
begin
      sFileName := ExtractFileName(txtArchivo3dt.Text);
      cxOpenDialog.Path := ExtractFilePath( Application.ExeName );
      if cxOpenDialog.Execute then
      begin
           FileName := cxOpenDialog.Path + '\' + sFileName;
           txtArchivo3dt.Text := FileName;
      end;
end;


end.
