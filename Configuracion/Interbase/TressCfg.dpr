program TressCfg;

uses
  Forms,
  FMigrar in '..\FMigrar.pas' {Migrar},
  DMigrar in '..\DMigrar.pas' {dmMigrar: TDataModule},
  FAcercaDe in '..\..\Linx.5\LinxBase\FAcercaDe.pas' {AcercaDe},
  ZBaseDlgModal in '..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  FEmpresas in '..\FEmpresas.pas' {Empresas},
  ZetaLicenseMgr in '..\..\Servidor\ZetaLicenseMgr.pas',
  FDatosImportReporte in '..\Datamodules\FDatosImportReporte.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Configurador de Tress';
  Application.HelpFile := 'Tresscfg.chm';
  Application.CreateForm(TEmpresas, Empresas);
  Application.Run;
end.
