inherited DBChecker: TDBChecker
  Left = 309
  Top = 212
  Width = 507
  Height = 289
  BorderIcons = [biSystemMenu, biMaximize]
  BorderStyle = bsSizeable
  Caption = 'Probar Acceso A Bases De Datos'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 201
    Width = 499
    Height = 54
    inherited OK: TBitBtn
      Left = 340
      Top = 9
      Height = 36
      Hint = 'Empezar La Prueba'
      Caption = '&Probar'
      ModalResult = 0
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 420
      Top = 9
      Height = 36
      Hint = 'Abandonar Esta Pantalla'
      Caption = '&Salir'
      Kind = bkClose
    end
    object DirectTest: TCheckBox
      Left = 150
      Top = 2
      Width = 129
      Height = 17
      Caption = 'Probar Acceso Dire&cto'
      Checked = True
      ParentShowHint = False
      ShowHint = False
      State = cbChecked
      TabOrder = 2
      OnClick = DirectTestClick
    end
    object ODBCTest: TCheckBox
      Left = 150
      Top = 32
      Width = 147
      Height = 17
      Caption = 'Probar Acceso Con &ODBC'
      Checked = True
      State = cbChecked
      TabOrder = 3
      OnClick = DirectTestClick
    end
    object Autoconfigurar: TBitBtn
      Left = 5
      Top = 9
      Width = 122
      Height = 36
      Hint = 'Configurar Acceso Usando Alias de BDE'
      Anchors = [akTop, akRight]
      Caption = '&AutoConfigurar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = AutoconfigurarClick
      Glyph.Data = {
        76020000424D7602000000000000760000002800000020000000200000000100
        0400000000000002000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888888888888888888888888888888888888888888000000888888888888888
        888888888807888F800888888888888888888888007888F88808888888888888
        8888888807888F8FFFF000000000000000000000080008F0000008777787878F
        8FF8F88F887F0F8088880F0F788888F8FFF88F88F807788088880F0077878F8F
        FF88F88F880F088088880FFFFFFFFFFFFFFFFFFFF80008800000800000000000
        000000000F88887878F02220080BBBBBBBBB300000F887878F02990800BBBBBB
        BBB3B300F80FFFFFF099990FBBBB000BBB3B3B3BF880000009999990FBB0FF00
        B3B3B3BF07099999999999970BB0FF000B3B3BB07770999999999000BBB0F700
        00B3BBBB0009999999998FBBBBBB0F80000BBBBBBBB0000000008FFFBBBBB0F8
        000BBBBBFFF0788888808888FBBBBB0F800BBBBF00000788888082888BBBBBB0
        00BBBBB0788F788888808888BBBBBBBBBBBBBBBB0788F8888880888FFFBBBBBB
        BBBBBBBF000000000000228F00FBBBBBBBBBBB00F007222222229990770FBBBB
        BBBBB00F0F00999999999999070FB0BBBB0FB000F0F099999999999990FF070B
        B070FF0000099999999999999900F70FB00F00F00999999999999999999F8F0F
        F000000F0999999999990000000888F88F0077F8000000000000888888888888
        88FF888888888888888888888888888888888888888888888888}
    end
    object ckDigito: TCheckBox
      Left = 150
      Top = 17
      Width = 159
      Height = 17
      Caption = 'Actualizar &D�gito de Empresa'
      TabOrder = 5
      OnClick = DirectTestClick
    end
  end
  object ResultadoGB: TGroupBox
    Left = 0
    Top = 0
    Width = 499
    Height = 201
    Align = alClient
    Caption = ' Resultados '
    TabOrder = 1
    object Resultados: TMemo
      Left = 2
      Top = 15
      Width = 495
      Height = 184
      Align = alClient
      BorderStyle = bsNone
      Color = clBtnFace
      ReadOnly = True
      ScrollBars = ssBoth
      TabOrder = 0
    end
  end
  object HdbcComparte: THdbc
    ConnectionPooling = cpDefault
    Version = '5.06'
    Left = 33
    Top = 94
  end
  object HdbcEmpresa: THdbc
    ConnectionPooling = cpDefault
    Version = '5.06'
    Left = 95
    Top = 94
  end
  object oeCompany: TOEQuery
    hDbc = HdbcComparte
    hStmt.CursorType = 0
    hStmt.Target.Target = (
      'OE5.06'
      ''
      ''
      ''
      '')
    UniDirectional = True
    Params = <>
    Left = 64
    Top = 94
  end
  object dbComparte: TIB_Connection
    Left = 33
    Top = 125
  end
  object dbEmpresa: TIB_Connection
    Left = 96
    Top = 124
  end
  object IB_Tx_Comparte: TIB_Transaction
    Isolation = tiConcurrency
    Left = 33
    Top = 155
  end
  object tqQuery: TIB_Cursor
    IB_Connection = dbComparte
    IB_Transaction = IB_Tx_Comparte
    ColorScheme = False
    MasterSearchFlags = [msfOpenMasterOnOpen, msfSearchAppliesToMasterOnly]
    Left = 65
    Top = 155
  end
  object tqCompany: TIB_Cursor
    IB_Connection = dbComparte
    ColorScheme = False
    MasterSearchFlags = [msfOpenMasterOnOpen, msfSearchAppliesToMasterOnly]
    Left = 65
    Top = 124
  end
  object oeVersion: TOEQuery
    hDbc = HdbcComparte
    hStmt.CursorType = 0
    hStmt.Target.Target = (
      'OE5.06'
      ''
      ''
      ''
      '')
    UniDirectional = True
    Params = <>
    Left = 96
    Top = 158
  end
  object tqVersion: TIB_Cursor
    IB_Connection = dbComparte
    ColorScheme = False
    MasterSearchFlags = [msfOpenMasterOnOpen, msfSearchAppliesToMasterOnly]
    Left = 129
    Top = 156
  end
  object tqUpdateCompany: TIB_Cursor
    IB_Connection = dbComparte
    ColorScheme = False
    MasterSearchFlags = [msfOpenMasterOnOpen, msfSearchAppliesToMasterOnly]
    Left = 129
    Top = 124
  end
  object tqDigito: TIB_Cursor
    IB_Connection = dbEmpresa
    ColorScheme = False
    MasterSearchFlags = [msfOpenMasterOnOpen, msfSearchAppliesToMasterOnly]
    Left = 161
    Top = 156
  end
end
