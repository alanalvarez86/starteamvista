unit FRegistryServerEditor;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ExtCtrls, FileCtrl, DB, DBTables,
     ZetaAliasManager,
     ZetaRegistryServer,
     ZBaseDlgModal, ZetaDBTextBox, ZetaKeyCombo;

type
  TServerRegistryEditor = class(TZetaDlgModal)
    UserName: TEdit;
    UserNameLBL: TLabel;
    PasswordLBL: TLabel;
    Password: TEdit;
    PasswordCheck: TEdit;
    PasswordCheckLBL: TLabel;
    DatabaseLBL: TLabel;
    AliasNameLBL: TLabel;
    CrearAlias: TSpeedButton;
    AliasName: TZetaTextBox;
    Database: TZetaTextBox;
    Label6: TLabel;
    TipoLogin: TZetaKeyCombo;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure PasswordChange(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure CrearAliasClick(Sender: TObject);
  private
    { Private declarations }
    FAliasManager: TZetaAliasManager;
    FRegistry: TZetaRegistryServer;
  protected
    { Protected declarations }
    property Registry: TZetaRegistryServer read FRegistry;
    procedure LoadControls; virtual;
    procedure UnloadControls; virtual;
  public
    { Public declarations }
    function IsValid: Boolean;
  end;

var
  ServerRegistryEditor: TServerRegistryEditor;

function EspecificaComparte: Boolean;

implementation

uses
     FHelpContext,
     ZetaDialogo,
     ZetaAliasCrear,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaWinAPITools;

{$R *.DFM}

function EspecificaComparte: Boolean;
begin
     with TServerRegistryEditor.Create( Application ) do
     begin
          try
             ShowModal;
             Result := ( ModalResult = mrOK );
          finally
                 Free;
          end;
     end;
end;

{ ************* TServerRegistryEditor ******** }

procedure TServerRegistryEditor.FormCreate(Sender: TObject);
begin
     inherited;
     FRegistry := TZetaRegistryServer.Create;
     FAliasManager := TZetaAliasManager.Create;
     TipoLogin.ListaFija := lfTipoLogin;
end;

procedure TServerRegistryEditor.FormShow(Sender: TObject);
begin
     inherited;
     LoadControls;
     HelpContext := H00011_Indicando_la_base_de_datos;
end;

procedure TServerRegistryEditor.FormDestroy(Sender: TObject);
begin
     FAliasManager.Free;
     FRegistry.Free;
     inherited;
end;

function TServerRegistryEditor.IsValid: Boolean;
begin
     Result := FRegistry.IsValid;
end;

procedure TServerRegistryEditor.LoadControls;
begin
     with Registry do
     begin
          Self.AliasName.Caption := AliasComparte;
          Self.Database.Caption := Database;
          Self.UserName.Text := UserName;
          Self.Password.Text := Password;
          Self.PasswordCheck.Text := Password;
          Self.TipoLogin.Valor := TipoLogin;
     end;
end;

procedure TServerRegistryEditor.UnloadControls;
begin
     with Registry do
     begin
          AliasComparte := Self.AliasName.Caption;
          Database := Self.Database.Caption;
          UserName := Self.UserName.Text;
          Password := Self.Password.Text;
          TipoLogin:= Self.TipoLogin.Valor;
     end;
end;

procedure TServerRegistryEditor.PasswordChange(Sender: TObject);
begin
     inherited;
     PasswordCheck.Text := '';
end;

procedure TServerRegistryEditor.CrearAliasClick(Sender: TObject);
var
   sAliasName, sDatabase: String;
begin
     inherited;
     with AliasName do
     begin
          sAliasName := Caption;
          if ZetaAliasCrear.CreaUnAliasDatabase( FAliasManager, sAliasName, sDatabase ) then
          begin
               Caption := sAliasName;
               Database.Caption := sDatabase;
          end;
     end;
end;

procedure TServerRegistryEditor.OKClick(Sender: TObject);
begin
     inherited;
     if ( Password.Text = PasswordCheck.Text ) then
     begin
          UnloadControls;
          ModalResult := mrOk;
     end
     else
     begin
          ZetaDialogo.zError( Caption, 'Clave de Acceso No Fu� Capturada Correctamente', 0 );
          ActiveControl := Password;
     end;
end;

procedure TServerRegistryEditor.CancelarClick(Sender: TObject);
begin
     inherited;
     Close;
end;

end.
