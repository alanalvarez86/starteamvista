unit ZFuncsGenerales;

interface

uses Classes, Sysutils, Mask, Controls,
     {$ifndef VER130}MaskUtils,{$endif}
     ZetaQRExpr,
     ZEvaluador;

procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );

implementation

uses ZetaCommonTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     DZetaServerProvider;

{ ******** Funciones Internas ****************}

function Aniversario( dReferencia, dInicial, dFinal: TDate ): Boolean;
var
   iYear: Word;
begin
     if ( dReferencia >= dFinal ) then
        Result := False
     else
         begin
              iYear := TheYear( dFinal );
              dReferencia := DateTheYear( dReferencia, iYear );
	      if ( dReferencia > dFinal ) then
                 dReferencia := DateTheYear( dReferencia, ( iYear - 1 ) );
              Result := ( dReferencia >= dInicial ) and ( dReferencia <= dFinal );
         end;
end;

function Asc( const sValor: String ): Integer;
begin
     if StrLleno( sValor ) then
        Result := Ord( sValor[ 1 ] )
     else
        Result := 0;
end;


{TZetaASC}
type
TZetaASC = class( TZetaFunc )
public
  function Calculate: TQREvResult; override;
end;

function TZetaASC.Calculate: TQREvResult;
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resInt;
          intResult := Asc( ParamString( 0 ) );
     end;
end;


function MesConLetra( const dReferencia: TDateTime ): String;
begin
     Result := MesConLetraMes( TheMonth( dReferencia ) );
end;

function TransformNumero( const iEnteros, iDecimales: Integer; const rValor: Extended ): String;
var
   iPos: Integer;
   sDecimales: String;
begin
     Result := Format( '%' + IntToStr( iEnteros ) + '.' + IntToStr( iDecimales ) + 'n', [ rValor ] );
     iPos := Pos( '.', Result );
     if ( iPos > 0 ) then
     begin
          sDecimales := Copy( Result, iPos, ( Length( Result ) - iPos + 1 ) );
          Result := Copy( Result, 1, ( iPos - 1 ) );
     end
     else
         sDecimales := '';
     iPos := iEnteros + Trunc( ( iEnteros - 1 ) / 3 ) ;
     Result := PadL( StrRight( Result, iPos ), iPos ) + sDecimales;
end;

{******************** Funciones Globales ********************* }
type
TZetaABS = class( TZetaFunc )
public
  function Calculate: TQREvResult; override;
end;

function TZetaABS.Calculate: TQREvResult;
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resDouble;
          dblResult := Abs( ParamTasa( 0 ) );
     end;
end;

{TZetaALLTRIM}

type
TZetaALLTRIM = class( TZetaFunc )
public
  function Calculate : TQREvResult; override;
end;

function TZetaALLTRIM.Calculate: TQREvResult; // Sustituye a la funci�n 'ALLTRIM' de Clipper //
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resString;
          strResult := Trim( ParamString( 0 ) );
     end
end;

{TZetaANIV}

type
TZetaANIV = class( TZetaFunc )
public
  function Calculate: TQREvResult; override;
end;

function TZetaANIV.Calculate: TQREvResult;
begin
     ParametrosFijos( 3 );
     with Result do
     begin
          Kind := resBool;
          booResult := Aniversario( ParamDate( 0 ), ParamDate( 1 ), ParamDate( 2 ) );
     end;
end;

{TZetaCTOD}

type
TZetaCTOD = class( TZetaConstante )
public
  function Calculate: TQREvResult; override;
end;

function TZetaCTOD.Calculate: TQREvResult;
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resDouble;
          EsFecha := TRUE;
          dblResult := ParamDate( 0 );
     end;
end;


{TZetaAT}
type
TZetaAT = class( TZetaFunc )
public
  function Calculate: TQREvResult; override;
end;

function TZetaAT.Calculate: TQREvResult;
begin
     ParametrosFijos( 2 );
     with Result do
     begin
          Kind := resInt;
          intResult := Pos( ParamString( 0 ), ParamString( 1 ) );
     end;
end;

{TZetaA_HORAS}
type
TZetaA_HORAS = class( TZetaFunc )
public
  function Calculate: TQREvResult; override;
end;

function TZetaA_HORAS.Calculate: TQREvResult;
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resDouble;
          dblResult := aHoras( ParamInteger( 0 ) );
     end;
end;

{TZetaA_HORA_STR}
type
TZetaA_HORA_STR = class( TZetaFunc )
public
  function Calculate: TQREvResult; override;
end;

function TZetaA_HORA_STR.Calculate: TQREvResult;
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resString;
          strResult := aHoraString( ParamInteger( 0 ) );
     end;
end;

{TZetaA_MINUTOS}
type
TZetaA_MINUTOS = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaA_MINUTOS.Calculate: TQREvResult;
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resInt;
          intResult := aMinutos( ParamString( 0 ) );
     end;
end;

{TZetaCASE}
type
TZetaCASE = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaCASE.Calculate: TQREvResult;     // Sustituye a la funci�n CASE( valor, c1, r1, c2, r2, ..., cN, rN ) de TRESS //
var
   i, iPos, iElementos, iPares: Integer;
begin
     ParametrosFijos( 2 );
     iElementos := ( ArgList.Count - 1 );
     iPares := Trunc( iElementos / 2 );
     iPos := 1;
     for i := 1 to iPares do
     begin
          if Iguales( Argument( 0 ), Argument( iPos ) ) then
          begin
               Result := Argument( iPos + 1 );
               Exit;
          end;
          iPos := iPos + 2;  // Pasa al siguiente Par //
     end;
     // En este punto, ninguna comparaci�n tuvo �xito //
     // En caso de haber un n�mero NON de elementos, el �ltimo es el ELSE //
     if ( iElementos > ( iPares * 2 ) ) then
        Result := Argument( iElementos );
end;


{TZetaCHR}
type
TZetaCHR = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaCHR.Calculate: TQREvResult;
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resString;
          strResult := Chr( ParamInteger( 0 ) );
     end;
end;


{TZetaDATE}
type
TZetaDATE = class( TZetaConstante )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaDATE.Calculate: TQREvResult;
begin
     with Result do
     begin
          Kind := resDouble;
          EsFecha := TRUE;
          dblResult := Date;
     end;
end;


{TZetaDAY}
type
  TZetaDAY = class( TZetaConstante )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaDAY.Calculate: TQREvResult;
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resInt;
          intResult := TheDay( ParamDate( 0 ) );
     end;
end;

{TZetaDENTRO}
type
TZetaDENTRO = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaDENTRO.Calculate: TQREvResult; // Sustituye a la funci�n '$' de Clipper //
var
   sTarget, sSearch: String;

function NextToken( var sTarget: String ): String;
var
   i: Integer;
begin
     i := Pos( ',', sTarget );
     if ( i <> 0 ) then
     begin
          Result := System.Copy( sTarget, 1, ( i - 1 ) );
          System.Delete( sTarget, 1, i );
     end
     else
     begin
          Result := sTarget;
          sTarget := '';
     end;
end;

begin
     ParametrosFijos( 2 );
     sTarget := ParamString( 1 );
     sSearch := ParamString( 0 );
     with Result do
     begin
          Kind := resBool;
          booResult := False;
          while not booResult and StrLleno( sTarget ) do
          begin
               booResult := ( sSearch = NextToken( sTarget ) );
          end;
     end;
end;

{TZetaDIA_DEL_MES}
type
TZetaDIA_DEL_MES = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaDIA_DEL_MES.Calculate: TQREvResult; // Fecha Inicial y Final del Mes de una Fecha //
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resDouble;
          EsFecha:= True;
          dblResult := DiaDelMes( DefaultDate( 1, Date ), ( ParamInteger( 0 ) = 1 ) );
     end;
end;

{TZetaDIF_MINUTOS}
type
TZetaDIF_MINUTOS = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaDIF_MINUTOS.Calculate: TQREvResult; // Diferencia en Minutos entre dos horas //
var
   sHora1, sHora2: String;
begin
     ParametrosFijos( 2 );
     sHora1 := ParamString( 0 );
     sHora2 := ParamString( 1 );
     with Result do
     begin
          Kind := resInt;
          if StrLleno( sHora1 ) and StrLleno( sHora2 ) then
          begin
     	    intResult := aMinutos( sHora2 ) - aMinutos( sHora1 );
     	    if not DefaultBoolean( 3, False ) then
               intResult := iMax( intResult, 0 );
          end
          else
     	   intResult := DefaultInteger( 2, 0 );
     end;
end;

{TZetaDOW}
type
TZetaDOW = class( TZetaConstante )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaDOW.Calculate: TQREvResult; // Sustituye DOW ( Day of Week ) de Clipper //
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resInt;
          intResult := DayOfWeek( ParamDate( 0 ) );
     end;

end;

{TZetaDTOC}
type
TZetaDTOC = class( TZetaConstante )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaDTOC.Calculate: TQREvResult; // Sustituye DOW ( Day of Week ) de Clipper //
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resString;
          strResult := FormatDateTime( DefaultString( 1, 'dd/mm/yyyy' ), ParamDate( 0 ) );
     end;
end;

{TZetaDTOS}
type
TZetaDTOS = class( TZetaConstante )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaDTOS.Calculate: TQREvResult; // Sustituye DOW ( Day of Week ) de Clipper //
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resString;
          strResult := FechaToStr( ParamDate( 0 ) );
     end;
end;

{TZetaEMPTY}
type
TZetaEMPTY = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaEMPTY.Calculate: TQREvResult; // Sustituye DOW ( Day of Week ) de Clipper //
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resBool;
          case Argument( 0 ).Kind of
               resString: booResult := not StrLleno( ParamString( 0 ) );
               resInt: booResult := ( ParamInteger( 0 ) = 0 );
               resDouble: booResult := ( PesosIguales( ParamTasa( 0 ), 0.0 ) );
          else
              booResult := False;
          end;
     end;
end;

{TZetaFALSE}
type
TZetaFALSE = class( TZetaConstante )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaFALSE.Calculate: TQREvResult;
begin
     with Result do
     begin
          Kind := resBool;
          booResult := False;
     end;
end;

{TZetaFECHA_CORTA}
type
TZetaFECHA_CORTA = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaFECHA_CORTA.Calculate: TQREvResult; // Sustituye DOW ( Day of Week ) de Clipper //
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resString;
          strResult := FechaCorta( ParamDate( 0 ) );
     end;
end;

{TZetaFECHA_DIA}
type
TZetaFECHA_DIA = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaFECHA_DIA.Calculate: TQREvResult; // Sustituye DOW ( Day of Week ) de Clipper //
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resString;
          strResult := FormatDateTime( 'dddd', ParamDate( 0 ) );
     end;
end;

{TZetaFECHA_STR}
type
TZetaFECHA_STR = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaFECHA_STR.Calculate: TQREvResult; // Sustituye DOW ( Day of Week ) de Clipper //
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resString;
          strResult := FechaLarga( ParamDate( 0 ) );
     end;
end;


{TZetaHORA24}
type
TZetaHORA24 = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaHORA24.Calculate: TQREvResult; // Funci�n HORA24( sHora: String ) de Tress //
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resString;
          strResult := aHora24( ParamString( 0 ) );
     end
end;

{TZetaIIF}
{ Version utilizada hasta el 2.0.85
type
TZetaIIF = class( TZetaFunc )
public
  function Calculate: TQREvResult; override;
end;

function TZetaIIF.Calculate: TQREvResult; // Funci�n IIF( Bool, exp1, exp2 ) //
begin
     ParametrosFijos( 3 );
     if ParamBoolean( 0 ) then
     begin
          Result := Argument( 1 );
          EsFecha := Argument( 1 ).resFecha;
     end
     else
     begin
          Result := Argument( 2 );
          EsFecha := Argument( 2 ).resFecha;
     end;
end; }

{TZetaIIF}
type
TZetaIIF = class( TZetaFunc )
public
  { Public Declarations }
  function Value( FIFO : TQRFiFo ) : TQREvResult; override;
end;

function TZetaIIF.Value(FiFo : TQRFiFo) : TQREvResult;
var
  aArgument : TQREvElement;
  aCondicion, aTercero : TQREvResult;


    procedure ValidaParametro;
    begin
        if ( aArgument is TQREvElementArgumentEnd ) then
            ErrorParametro( 'Tienen que ser 3 par�metros', '' );
    end;

    procedure ObtieneParametro;
    begin
        aArgument := TQREvElement(FiFo.Get);
        ValidaParametro;
    end;

    function BrincaParam( aParam : TQREvElement ) : TQREvElement; forward;

    procedure LimpiaParametros( aParam : TQREvElement );
    begin
        while not ( aParam is TQREvElementArgumentEnd ) do
            aParam := BrincaParam( aParam );
    end;

    function BrincaParam( aParam : TQREvElement ) : TQREvElement;
    begin
        Result := TQREvElement( FiFo.Get );
        if ( aParam is TQREvElementOperator ) then
        begin
            Result := BrincaParam( Result );    // OP1
            Result := BrincaParam( Result );   // OP2
        end
        else if ( aParam is TQREvElementFunction ) then
        begin
            LimpiaParametros( Result );
            Result := TQREvElement( FiFo.Get ); // Brinca ArgumentEnd
        end;
    end;

    procedure LimpiaResto;
    begin
        if not ( aArgument is TQREvElementArgumentEnd ) then
        begin
            aArgument := TQREvElement(FiFo.Get);
            LimpiaParametros( aArgument );
        end;
    end;


begin
    try
        ObtieneParametro;   // Primero
        // El primer par�metro, es la condici�n del IF( condidion,RVerdadero, RFalso )
        aCondicion := aArgument.Value( FiFo );

        if ( aCondicion.Kind = resBool ) then
        begin
            ObtieneParametro;   // Segundo
            // Si estoy buscando requeridos, eval�a ambos
            if ( Evaluador.BuscandoRequeridos ) then
            begin
                 Result := aArgument.Value( Fifo );
                 aArgument := TQREvElement(FiFo.Get);
                 if not ( aArgument is TQREvElementArgumentEnd ) then
                    aTercero := aArgument.Value( Fifo )
                 else
                    aTercero.Kind := resError;

                // ej. IF( condicion, 0, cb_salario ) antes regresaba ENTERO
                if ( Result.Kind = resInt ) and ( aTercero.Kind = resDouble ) then
                begin
                    Result.Kind := resDouble;
                    Result.resFecha  := FALSE;
                end;
            end
            // Si la condici�n es verdadera, eval�a el segundo par�metro
            else if ( aCondicion.booResult ) then
                 Result := aArgument.Value( FiFo )
            else  // Si la condici�n es FALSA, eval�a el tercer par�metro
            begin
                aArgument := BrincaParam( aArgument );
                ValidaParametro;   // Tercero, S�lo en caso de ser necesario
                // ObtieneParametro;
                Result := aArgument.Value( FiFo );

                if ( Result.Kind = resError ) and ( Result.strResult ='NIL' ) then
                   ErrorParametro( 'Tercer Par�metro no puede quedar vac�o', '' );
            end;
        end
        else if ( aCondicion.Kind = resError ) then
            Result := aCondicion
        else
            ErrorParametro( 'Primer par�metro No es L�gico', '' );


        EsFecha := Result.resFecha;
    except
        on E : EErrorEvaluador do
        begin
          Result.Kind := resError;
          Result.strResult := E.Message;
        end;
        on E : Exception do
        begin
          Result.Kind := resError;
          Result.strResult := ErrorEvaluador( eeFuncion, E.Message,
'Funci�n:' + Nombre );
        end;
    end;
    LimpiaResto;

    if Evaluador.BuscandoRequeridos then
         case ( Result.Kind ) of
              resInt        : Result.intResult := 1;
              resDouble     : Result.dblResult := 1;
              resString     : Result.strResult := '';
         end;
end;

{TZetaINT}
type
TZetaINT = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaINT.Calculate: TQREvResult; // Funci�n INT de Clipper //
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resInt;
          intResult := Trunc( ParamTasa( 0 ) );
     end
end;


{TZetaLEFT}
type
TZetaLEFT = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaLEFT.Calculate: TQREvResult; // Funci�n LEFT de Clipper //
begin
     ParametrosFijos( 2 );
     with Result do
     begin
          Kind := resString;
          strResult := StrLeft( ParamString( 0 ), ParamInteger( 1 ) );
     end
end;


{TZetaLEN}
type
TZetaLEN = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaLEN.Calculate: TQREvResult; // Funci�n LEN de Clipper //
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resInt;
          intResult := Length( ParamString( 0 ) );
     end
end;

{TZetaLOWER}
type
TZetaLOWER = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaLOWER.Calculate: TQREvResult; // Funci�n LOWER de Clipper //
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resString;
          strResult := AnsiLowerCase( ParamString( 0 ) );
     end
end;

{TZetaLTRIM}
type
TZetaLTRIM = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaLTRIM.Calculate: TQREvResult; // Funci�n LTRIM de Clipper //
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resString;
          strResult := TrimLeft( ParamString( 0 ) );
     end
end;

{TZetaMAX}
type
TZetaMAX = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaMAX.Calculate: TQREvResult; // Funci�n MAX() de Clipper //
begin
     {
     ParametrosFijos( 2 );
     with Result do
     begin
          Kind := resDouble;
          dblResult := rMax( ParamTasa( 0 ), ParamTasa( 1 ) );
     end
     }
     ParametrosFijos( 2 );
     with Result do
     begin
          Kind := resDouble;
          EsFecha := Argument( 0 ).resFecha;
          if Kind in [ resInt, resDouble ] then
          begin
               if EsFecha then dblResult := rMax( ParamDate( 0 ), ParamDate( 1 ) )
               else dblResult := rMax( ParamTasa( 0 ), ParamTasa( 1 ) );
          end
          else Kind := resError;
     end
end;

{TZetaMIN}
type
TZetaMIN = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaMIN.Calculate: TQREvResult; // Funci�n MIN() de Clipper //
begin
     ParametrosFijos( 2 );
     with Result do
     begin
          Kind := resDouble;
          EsFecha := Argument( 0 ).resFecha;
          if Kind in [ resInt, resDouble ] then
          begin
               if EsFecha then dblResult := rMin( ParamDate( 0 ), ParamDate( 1 ) )
               else dblResult := rMin( ParamTasa( 0 ), ParamTasa( 1 ) );
          end
          else Kind := resError;
     end
end;

{TZetaMONTH}
type
TZetaMONTH = class( TZetaConstante )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaMONTH.Calculate: TQREvResult; // Funci�n MONTH de Clipper //
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resInt;
          intResult := TheMonth( ParamDate( 0 ) );
     end;
end;

{TZetaPADC}
type
TZetaPADC = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaPADC.Calculate: TQREvResult; // Funci�n PADC de Clipper //
 var sTexto, s : string;
     sRelleno : Char;
begin
     ParametrosFijos( 2 );
     with Result do
     begin
          Kind := resString;
          if Argument( 0 ).Kind = resInt then sTexto := IntToStr( ParamInteger( 0 ) )
          else sTexto := ParamString( 0 );

          s := DefaultString( 2, ' ' );
          sRelleno := ' ';
          if StrLleno(s ) then sRelleno := s[ 1 ];

          if Length(sTexto) >  ParamInteger( 1 ) then
             sTexto:= Copy(sTexto, 1, ParamInteger( 1 ));

          strResult := PadCCar( sTexto, ParamInteger( 1 ), sRelleno );
     end
end;

{TZetaPADL}
type
TZetaPADL = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaPADL.Calculate: TQREvResult; // Funci�n PADL de Clipper //
 var sTexto, s: string;
     sRelleno : Char;
begin
     ParametrosFijos( 2 );
     with Result do
     begin
          Kind := resString;
          if Argument( 0 ).Kind = resInt then sTexto := IntToStr( ParamInteger( 0 ) )
          else sTexto := ParamString( 0 );

          s := DefaultString( 2, ' ' );
          sRelleno := ' ';
          if StrLleno(s ) then sRelleno := s[ 1 ];

          if Length(sTexto) >  ParamInteger( 1 ) then
             sTexto:= Copy(sTexto, 1, ParamInteger( 1 ));

          strResult := PadLCar( sTexto, ParamInteger( 1 ), sRelleno );
     end;
end;

{TZetaPADR}
type
TZetaPADR = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaPADR.Calculate: TQREvResult; // Funci�n PADR de Clipper //
 var sTexto,s : String;
     sRelleno : Char;
begin
     ParametrosFijos( 2 );
     with Result do
     begin
          Kind := resString;
          if Argument( 0 ).Kind = resInt then sTexto := IntToStr( ParamInteger( 0 ) )
          else sTexto := ParamString( 0 );

          s := DefaultString( 2, ' ' );
          sRelleno := ' ';
          if StrLleno(s ) then sRelleno := s[ 1 ];

          if Length(sTexto) >  ParamInteger( 1 ) then
             sTexto:= Copy(sTexto, 1, ParamInteger( 1 ));

          strResult := PadRCar( sTexto, ParamInteger( 1 ), sRelleno );
     end;
end;

{TZetaPROTECCION}
type
TZetaPROTECCION = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaPROTECCION.Calculate: TQREvResult; // Funci�n PROTECCION de Tress //
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resString;
          strResult := MontoConLetra( ParamFloat( 0 ),
                                      DefaultString( 1, 'Pesos' ),
                                      DefaultBoolean( 2, True ) );
     end;
end;

{TZetaRANGO}
type
TZetaRANGO = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaRANGO.Calculate: TQREvResult; // Funci�n RANGO de Tress //
var
   lOk: Boolean;
   rValor: TPesos;
begin
     ParametrosFijos( 3 );
     if ( Argument( 0 ).Kind = resString ) then
        lOk := ( ( ParamString( 0 ) >= ParamString( 1 ) ) and ( ParamString( 0 ) <= ParamString( 2 ) ) )
     else
     begin
          rValor := ParamTasa( 0 );
          lOk := ( ( rValor >= ParamTasa( 1 ) ) and ( rValor <= ParamTasa( 2 ) ) )
     end;
     with Result do
     begin
          Kind := resBool;
          booResult := lOk;
     end
end;

{TZetaREPLICATE}
type
TZetaREPLICATE = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaREPLICATE.Calculate: TQREvResult; // Funci�n REPLICATE de Clipper //
begin
     ParametrosFijos( 2 );
     Result.Kind := resString;
     Result.strResult := Replicate( ParamString( 0 ), ParamInteger( 1 ) )
end;

{TZetaRIGHT}
type
TZetaRIGHT = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaRIGHT.Calculate: TQREvResult; // Funci�n RIGHT de Clipper //
begin
     ParametrosFijos( 2 );
     with Result do
     begin
          Kind := resString;
          strResult := StrRight( ParamString( 0 ), ParamInteger( 1 ) );
     end;
end;

{TZetaROUND}
type
TZetaROUND = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaROUND.Calculate: TQREvResult; // Funci�n ROUND( x: Float, decimales) de Clipper //
begin
     ParametrosFijos( 2 );
     with Result do
     begin
          Kind := resDouble;
          dblResult := MiRound( ParamTasa( 0 ), ParamInteger( 1 ) );
     end
end;

{TZetaMXR}
type
TZetaMaxRound = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaMaxRound.Calculate: TQREvResult; // Funci�n ROUND( x: Float, decimales) de Clipper //
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resDouble;
          dblResult := miRound( rMax( ParamTasa( 0 ), 0 ), 0 );
     end
end;

{TZetaRTRIM}
type
TZetaRTRIM = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaRTRIM.Calculate: TQREvResult; // Funci�n RTRIM de Clipper //
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resString;
          strResult := TrimRight( ParamString( 0 ) );
     end
end;

{TZetaSPACE}
type
TZetaSPACE = class( TZetaConstante )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaSPACE.Calculate: TQREvResult; // Funci�n SPACE de Clipper //
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resString;
          strResult := Space( ParamInteger( 0 ) );
     end;
end;

{TZetaSTR}
type
TZetaSTR = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaSTR.Calculate: TQREvResult; // Funci�n STR de Clipper
var
   rValor : Currency;
   nl, nd, nEnteros : Integer;
begin
     ParametrosFijos( 1 );
     if Argument( 0 ).Kind = resInt then
     begin
          rValor := ParamInteger(0);
          nl := DefaultInteger( 1, 10 );
          nd := DefaultInteger( 2, 0 );
     end
     else
     begin
          rValor := ParamTasa(0); //(JB) Funcion STR no respeta la generacion del valor numerico con decimales indicados CR1780 T1139
          nl := DefaultInteger( 1, 13 );
          nd := DefaultInteger( 2, 2 );
     end;

     if ( nd > 0 ) then
        nEnteros := nl - nd - 1
     else
        nEnteros := nl;

     with Result do
     begin
          Kind := resString;
          Str( rValor:nEnteros:0, strResult );
          if length( strResult ) > nEnteros then
             strResult := Replicate( '*', nl )
          else
             Str( rValor:nl:nd, strResult );
     end;
end;

{TZetaSTRTRAN}
type
TZetaSTRTRAN = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaSTRTRAN.Calculate: TQREvResult; // Funci�n STRTRAN de Clipper //
var
   iCtr, iPos, iCuantos: Integer;
   sBuffer, sResult, sSearch, sReplace: String;
begin
     ParametrosFijos( 2 );
     sResult := ParamString( 0 );
     sSearch := ParamString( 1 );
     sReplace := DefaultString( 2, '' );
     iPos := DefaultInteger( 3, 1 );
     iCuantos := DefaultInteger( 4, Length( sResult ) );

     if ( iPos > 1 ) then
     begin
          sBuffer := Copy( sResult, 1, ( iPos - 1 ) );
          sResult := Copy( sResult, iPos, ( Length( sResult ) - iPos + 1 ) );
     end
     else
         sBuffer := '';
     with Result do
     begin
          Kind := resString;
          strResult := sBuffer;
     end;
     iCtr := 1;
     iPos := Pos( sSearch, sResult );
     while ( iPos > 0 ) and ( iCtr <= iCuantos ) do
     begin
          iCtr := iCtr + 1;
          sResult := StrTransform( sResult, sSearch, sReplace );
          iPos := Pos( sSearch, sResult );
     end;
     with Result do
     begin
          Kind := resString;
          strResult := sBuffer + sResult;
     end;
end;

{TZetaSUBSTR}
type
TZetaSUBSTR = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaSUBSTR.Calculate: TQREvResult; // Funci�n SUBSTR de Clipper //
var
   iStart, iCount: Word;
begin
     ParametrosFijos( 2 );
     with Result do
     begin
          Kind := resString;
          strResult := ParamString( 0 );
          iStart := ParamInteger( 1 );
          iCount := DefaultInteger( 2, ( Length( strResult ) - iStart ) );
          strResult := Copy( strResult, iStart, iCount );
     end;
end;


{TZetaTH}
type
TZetaTH = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaTH.Calculate: TQREvResult; // Funci�n TH de Tress //
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resString;
          strResult := ParamString( 0 );
          strResult := Copy( strResult, 1, 2 ) + ':' + Copy( strResult, 3, 2 );
     end;
end;


{TZetaTIME}
type
TZetaTIME = class( TZetaConstante )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaTIME.Calculate: TQREvResult; // Funci�n TIME de Clipper //
begin
     with Result do
     begin
          Kind := resString;
          strResult := FormatDateTime( 'hh:nn:ss', Now );
     end;
end;


{TZetaTRANSFORM}
type
TZetaTRANSFORM = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaTRANSFORM.Calculate: TQREvResult;
   procedure SeparaMascara( var sMascara, sFuncion, sResto : String );
   var nPos : Integer;
   begin
        sMascara := AnsiUpperCase( sMascara );
        if Copy( sMascara, 1, 1 ) = '@' then
        begin
             nPos := Pos( ' ', sMascara );
             if ( nPos = 0 ) then
                nPos := Length( sMascara ) + 1;
             sFuncion := Copy( sMascara, 2, nPos-2 );
             sResto   := Copy( sMascara, nPos+1, MAXINT );
        end
        else
        begin
             sFuncion := '';
             sResto := sMascara;
        end;
   end;

   function TransformNumero( sMascara : String; nMonto : Extended ) : String;
   var
     sFuncion, sResto : String;
     nPos : Integer;
     sParte1, sParte2, sParte3 : String;
     sEnteros, sDecimales : String;
     nPad : Integer;
     lPesos, lStars : Boolean;
   begin
        if ( Pos( ';', sMascara ) > 0 ) then
        begin
             Result := FormatFloat( sMascara, nMonto );
             Exit;
        end;

        sParte1 := '';
        sParte2 := '';
        sParte3 := '';

        SeparaMascara( sMascara, sFuncion, sResto );

        lPesos := ( Copy( sResto, 1, 1 ) = '$' );
        if ( lPesos ) then
           sResto := Copy( sResto, 2, MAXINT );
        lStars := ( Pos( '*', sResto ) > 0 );
        if ( lStars ) then
           sResto := StrTransAll( sResto, '*', '9' );


        // Construye partes para Delphi
        if ( Pos( '9', sResto ) > 0 ) then
        begin
             nPos := Pos( '.', sResto );
             if ( nPos > 0 ) then
             begin
                  sEnteros := Copy( sResto, 1, nPos-1 );
                  sDecimales := Copy( sResto, nPos, MAXINT );
                  sDecimales := StrTransAll( sDecimales, '9', '0' );
             end
             else
             begin
                  sEnteros := sResto;
                  sDecimales := '';
                  sResto := Trim( sResto );
                  for nPos := 1 to Length( sResto ) do
                      if not ( sResto[ nPos ] in [ '9', ',' ] ) then
                      begin
                           sDecimales := Copy( sResto, nPos, MAXINT );
                           break;
                      end;
             end;
             if ( Pos( ',', sEnteros ) > 0 ) then
                sEnteros := ',0'
             else
                 sEnteros := '0';
             sParte1 := sEnteros + sDecimales;
        end;

        if ( Pos( 'Z', sFuncion ) > 0 ) then
           sParte3 := '#';

        sMascara := sParte1 + ';' + sParte2 + ';' + sParte3;
        Result := FormatFloat( sMascara, nMonto );

        if ( nMonto < 0 ) then
             if ( Pos( '(', sFuncion ) > 0 ) or ( Pos( ')', sFuncion ) > 0 ) then
                Result := '(' + StrTransform( Result, '-', '' ) + ')';

        nPad := Length( sResto );
        if ( nPad > 0 ) then
           if ( Length( Result ) > nPad ) then
              Result := Replicate( '*', nPad )
           else if ( lStars ) then
               Result := PadLCar( Result, nPad, '*' )
           else
               Result := PadLCar( Result, nPad, ' ' );

        if ( lPesos ) and ( Length( Trim( Result )) > 0 ) then
           Result := '$' + Result;
   end;

   function TransformTexto( sMascara, sTexto : String ) : String;
   var
      sFuncion, sResto : String;
      sParte1, sParte2, sParte3 : String;
   begin
        if ( Pos( ';', sMascara ) > 0 ) then
        begin
             Result := FormatMaskText( sMascara, sTexto );
             Exit;
        end;

        sParte1 := '';
        sParte2 := '';
        sParte3 := '';
        SeparaMascara( sMascara, sFuncion, sResto );

        if ( Pos( '!', sMascara ) > 0 ) then
           sTexto := AnsiUpperCase( sTexto );
        if ( Pos( 'R', sFuncion ) > 0 ) then
        begin
           sParte1 := sResto;
           sParte1 := StrTransAll( sParte1, '!', 'c' );
           sParte1 := StrTransAll( sParte1, '#', 'c' );
           sParte1 := StrTransAll( sParte1, 'N', 'c' );
           sParte1 := StrTransAll( sParte1, 'X', 'c' );
           sParte2 := '0';
        end;


        sMascara := sParte1 + ';' + sParte2 + ';' + sParte3;
        if Length( sMascara ) > 2 then
           Result := FormatMaskText( sMascara, sTexto )
        else
            Result := sTexto;
   end;

begin
     ParametrosFijos( 2 );
     with Result do
     begin
          Kind := resString;
          case Argument( 0 ).Kind of
               resInt: strResult    := TransformNumero( ParamString(1), ParamInteger( 0 ) );
               resString: strResult := TransformTexto(  ParamString(1), ParamString( 0 ) );
               resDouble:
               begin
                    if Argument( 0 ).resFecha then
                       strResult := FormatDateTime( ParamString(1), ParamDate( 0 ) )
                    else
                        strResult := TransformNumero( ParamString(1), ParamTasa( 0 ) );
               end
          else
              strResult := '';
          end;
     end;
end;


{TZetaTRUE}
type
TZetaTRUE = class( TZetaConstante )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaTRUE.Calculate: TQREvResult;
begin
     with Result do
     begin
          Kind := resBool;
          booResult := True;
     end;
end;


{TZetaUPPER}
type
TZetaUPPER = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaUPPER.Calculate : TQREvResult; // Funci�n UPPER de Clipper //
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resString;
          strResult := AnsiUpperCase( ParamString( 0 ) );
     end;
end;

{TZetaVAL}
type
TZetaVAL = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaVAL.Calculate: TQREvResult; // Funci�n TH de Tress //
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resDouble;
          dblResult := StrToReal( ParamString( 0 ) );
     end;
end;


{TZetaYEAR}
type
TZetaYEAR = class( TZetaConstante )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaYEAR.Calculate: TQREvResult; // Funci�n MONTH de Clipper //
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resInt;
          intResult := TheYear( ParamDate( 0 ) );
     end;
end;


{TZetaAYER}
type
TZetaAYER = class( TZetaConstante )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaAYER.Calculate: TQREvResult; // Funci�n MONTH de Clipper //
 var dReferencia: TDate;
begin
     with Result do
     begin
           Kind := resDouble;
           EsFecha := TRUE;
           dReferencia:= DefaultDate( 0, Date() );
           if ( DayOfWeek( dReferencia ) = 2 ) then
             dblResult := dReferencia - 3
           else
	      dblResult := dReferencia - 1;
     end;
end;


{TZetaLCERO}
type
TZetaLCERO = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaLCERO.Calculate: TQrEvResult;
begin
     ParametrosFijos( 2 ); //????
     with Result do
     begin
          Kind := resString;
          strResult := ParamString( 0 );
          if ParamFloat( 2 )= 0 then
             strResult := ''
          else
             strResult := PadR( strResult, DefaultInteger( 1, Length( strResult ) ) ) +
                          TransformNumero( DefaultInteger( 3, 12 ), DefaultInteger( 4, 0 ), ParamFloat( 2 ) );
     end;
end;

{TZetaCero}
type
TZetaCero = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaCero.Calculate: TQrEvResult;
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resString;
          if ( ParamFloat( 0 ) = 0 ) then
             strResult := ''
          else
              strResult := TransformNumero( DefaultInteger( 1, 12 ), DefaultInteger( 2, 0 ), ParamFloat( 0 ) );
     end;
end;


{TZetaDIASENCURSO}
type
TZetaDIASENCURSO = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaDIASENCURSO.Calculate: TQrEvResult;
var
   dReferencia, dTope, dNewYear: TDate;
begin
     dReferencia := DefaultDate( 0, Date() );   //x
     dTope := DefaultDate( 1, Date() );         // y
     dNewYear := FirstDayOfYear( TheYear( dTope ) );
     if ( dReferencia < dNewYear ) then
        dReferencia := dNewYear;
     with Result do
     begin
          Kind := resInt;
//          IntResult := DaysBetween( dReferencia , dTope);
          IntResult := Trunc( dTope - dReferencia + 1 );
     end;
end;

{TZetaLASTMONDAY}
type
TZetaLASTMONDAY = class( TZetaConstante )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaLASTMONDAY.Calculate: TQrEvResult;
begin
     with Result do
     begin
          Kind := resDouble;
          EsFecha := TRUE;
          dblResult := LastMonday(DefaultDate( 0, Date() ));
     end;
end;

{TZetaNPESOS}
type
TZetaNPESOS = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaNPESOS.Calculate: TQrEvResult;
var
   rMoneda: Extended;
begin
     ParametrosFijos( 1 );
     rMoneda := DefaultFloat( 1, 0.05 );
     with Result do
     begin
          Kind := resDouble;
          dblResult := MiRound( MiRound( ParamFloat( 0 ) / rMoneda, 0 ) * rMoneda, 2 );
     end;
end;

{TZetaQUITAACENTOS}
type
TZetaQUITAACENTOS = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaQUITAACENTOS.Calculate: TQrEvResult;
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resString;
          strResult := ZetaCommonTools.QuitaAcentos( ParamString( 0 ));
     end;
end;

{TZetaSTRZERO}
type
TZetaSTRZERO = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaSTRZERO.Calculate: TQrEvResult;
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resString;
          //Antes tenia ArgList.Count >1 ???? -CV
          if ( Argument( 0 ).Kind = resDouble  ) then
                StrResult := StrToZero( ParamFloat( 0 ), DefaultInteger( 1, 10 ), DefaultInteger( 2, 2 ) )
          else
                StrResult := StrToZero( ParamFloat( 0 ), DefaultInteger( 1, 10 ), DefaultInteger( 2, 0 ) );
     end;
end;

{TZetaSTUFF}
type
TZetaSTUFF = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

{function TZetaSTUFF.Calculate : TQREvResult;
begin
     ParametrosFijos( 3 );
     with Result do
     begin
          Kind:= resString;
          StrResult:= StrTransAll( ParamString(0), Copy( ParamString(0), ParamInteger(1),ParamInteger(2)), DefaultString(3, '') );
     end;
end;
}

function TZetaSTUFF.Calculate: TQrEvResult;
var
   sOriginal, sReplace: String;
   iInicio, iCuantos: Integer;
begin
     ParametrosFijos( 3 );
     sOriginal := ParamString( 0 );
     iInicio := ParamInteger( 1 );
     iCuantos := ParamInteger( 2 );
     if ( iCuantos > 0 ) then
        Delete( sOriginal, iInicio, iCuantos );
     sReplace := ParamString( 3 );
     if StrLleno( sReplace ) then
        Insert( sReplace, sOriginal, iInicio );
     with Result do
     begin
          Kind := resString;
          strResult := sOriginal;
     end;
end;

{TZetaTIEMPO}
type
TZetaTIEMPO = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaTIEMPO.Calculate: TQrEvResult;
 var eTipo : eTiempoDias;
     sTiempo : string;
begin
     ParametrosFijos( 2 );
     with Result do
     begin
          Kind := resString;
          sTiempo := AnsiUpperCase( DefaultString( 2, 'D' ) );
          if sTiempo = 'A' then
             eTipo := etYear
          else if sTiempo = 'M' then
               eTipo := etMeses
          else eTipo := etDias;
          strResult := Tiempo( ParamDate( 0 ), ParamDate( 1 ), eTipo );
     end;

     {ParametrosFijos( 2 );
     with Result do
     begin
          Kind := resString;
          strResult := Tiempo( ParamDate( 0 ), ParamDate( 1 ), ( AnsiUpperCase( DefaultString( 2, 'D' ) ) = 'D' ) );
     end;}
end;


{TZetaYEARLETRA}
type
TZetaYEARLETRA = class( TZetaConstante )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaYEARLETRA.Calculate: TQrEvResult;
var
   iYear, iRegresa: Integer;
begin
     iYear := TheYear( DefaultDate( 0, Date() ) );
     with Result do
     begin
          iRegresa := DefaultInteger( 1, 1 );
          Kind := resString;
          case iRegresa of
               1: strResult := IntToStr( iYear mod 10 );
               2: strResult := IntToStr( iYear mod 100 );
               3: strResult := IntToStr( iYear mod 1000 );
               4: strResult := IntToStr( iYear );
               11: strResult := MontoConLetra( iYear mod 10, '', False );
               12: strResult := MontoConLetra( iYear mod 100, '', False );
               13: strResult := MontoConLetra( iYear mod 1000, '', False );
               14: strResult := MontoConLetra( iYear, '', False );
               else
                   strResult := IntToStr( iYear );
          end;
     end;
end;


{TZetaCAJA}
type
TZetaCAJA = class( TZetaConstante )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaCAJA.Calculate: TQrEvResult;
var
   i: Integer;
   sGuiones: String;
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          case ParamInteger( 0 ) of
               0: strResult := '';
               1: strResult := '[';
               2: strResult := '[]';
          else
              begin
                   for i:= 0 to ( ParamInteger( 0 ) - 3 ) do
                       sGuiones:= sGuiones + '_';
                   strResult:= '[' + sGuiones + ']';
              end;
          end;
     end;
end;

{TZetaSALTA}
type
TZetaSALTA = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaSALTA.Calculate: TQrEvResult;
begin
     with Result do
     begin
          Kind := resString;
          strResult := CR_LF;
     end;
end;

{TZetaMESMES}
type
TZetaMESMES = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaMESMES.Calculate: TQrEvResult;
var
   iMes: Integer;
begin
     ParametrosFijos(1);

     iMes := ParamInteger( 0 );

     if ( iMes < 1 ) OR ( iMes > 12 ) then
     begin
          raise EErrorEvaluador.Create( ErrorEvaluador( eeSintaxis,
                                      '# de Mes fuera de Rango 1..12',
                                      'Error en la Expresi�n: MES_MES( )' ) )
     end
     else
          with Result do
          begin
               Kind := resString;
               StrResult:= MesConLetraMes( iMes );
         end;
end;

{TZetaMES}
type
TZetaMES = class( TZetaFunc )
public
  { Public Declarations }
  function Calculate: TQREvResult; override;
end;

function TZetaMES.Calculate: TQREvResult; // Funci�n MES de Tress: Mes con Letras //
begin
     ParametrosFijos( 1 );
     with Result do
     begin
          Kind := resString;
          strResult := MesConLetra( ParamDate( 0 ) );
     end
end;

{*********************** TZetaDIASLETRA *********************}

type
  TZetaDIASLETRA = class( TZetaFunc )
  public
    function Calculate: TQREvResult; override;
  end;

function TZetaDIASLETRA.Calculate: TQREvResult;
 var rValor : Real;
     eTipo : eTiempoDias;
begin
     if ParamVacio( 0 ) then
        rValor := 0
     else
        rValor := ParamFloat( 0 );
     if ParamVacio( 1 ) then
        eTipo := etDias
     else
        if ParamBoolean(1) then
           eTipo := etDias
        else eTipo := etMeses;

     with Result do
     begin
          Kind := resString;
          strResult := TiempoDias( rValor, eTipo );
     end;
end;

type
  TZetaRAYA = class( TZetaFunc )
  public
    { Public Declarations }
    function Calculate: TQREvResult; override;
  end;

function TZetaRAYA.Calculate: TQrEvResult;
const
     GUION = '_';
var
   iAncho : Integer;

   function ObtieneCaracter( const i: Byte ): Char;
   var
      sTexto: String;
   begin
        sTexto := DefaultString( i, GUION );
        Result:= sTexto[1];
   end;

begin
     ParametrosFijos( 1 );
     iAncho := ParamInteger( 0 );
     with Result do
     begin
          Kind := resString;
          strResult := copy( ObtieneCaracter( 1 ) +
                             Replicate( ObtieneCaracter( 3 ), iAncho - 2 ) +
                             ObtieneCaracter( 2 ), 1, iAncho );
     end;
end;

type
  TZetaLIMPIASTR = class( TZetaFunc )
  public
    { Public Declarations }
    function Calculate: TQREvResult; override;
  end;

function TZetaLIMPIASTR.Calculate: TQrEvResult;
const
    tLetras = [ 'A'..'Z' ];
var
   i, Ancho : Integer;
   sLimpia, sOrigen: String;
   Caracter: Char;
begin
     ParametrosFijos( 1 );
     sLimpia:= VACIO;
     sOrigen:= Trim( ParamString(0) );
     Ancho:= Length( sOrigen );
     for i:= 1 to Ancho do
     begin
          Caracter:= Copy( sOrigen, i, 1 )[1];
          if Caracter in tLetras then
             sLimpia:= sLimpia + Caracter;
     end;
     with Result do
     begin
          Kind := resString;
          if StrVacio( sLimpia ) then
             strResult:= SPACE( Ancho )
          else
             strResult := sLimpia;
     end;
end;

{****************   TZetaSumFunction **************************}
type
  TZetaSumFunction = class(TZetaFunc)
  private
    SumResult : TQREvResult;
    ResAssigned : boolean;
  public
    constructor Create(oEvaluador : TQREvaluator); override;
    procedure Aggregate; override;
    function Calculate : TQREvResult; override;
    procedure Reset; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;

  end;

constructor TZetaSumFunction.Create(oEvaluador : TQREvaluator);
begin
  inherited Create(oEvaluador);
  ResAssigned := false;
  IsAggreg := true;
end;

procedure TZetaSumFunction.Reset;
begin
  ResAssigned := false;
end;

procedure TZetaSumFunction.Aggregate;
var
  aValue : TQREvResult;
begin
  if ArgList.Count = 1 then
  begin
    aValue := Argument(0);
    if ResAssigned then
    begin
      case aValue.Kind of
        resInt    : SumResult.dblResult := SumResult.dblResult + aValue.IntResult;
        resDouble : SumResult.dblResult := SumResult.dblResult + aValue.dblResult;
        else
            SumResult.Kind := resError
      end;
    end else
    begin
      SumResult.Kind := resDouble;
      case aValue.Kind of
        resInt    : SumResult.dblResult := aValue.IntResult;
        resDouble : SumResult.dblResult := aValue.dblResult;
      else
        SumResult.Kind := resError;
      end;
    end;
    ResAssigned := true;
    if ( SumResult.Kind = resError ) then
       SumResult.strResult := ErrorEvaluador( eeTipos, 'S�lo se permiten N�meros', 'Funci�n SUM()' );
  end else
    SumResult := ErrorCreate( ErrorEvaluador( eeFuncionParam, 'Falta par�metro Num�rico', 'Funci�n SUM()' ));
end;

function TZetaSumFunction.Calculate : TQREvResult;
begin
  Aggregate; { EZM }
  if ResAssigned then
    Result := SumResult
  else
    Result := ErrorCreate( ErrorEvaluador( eeFuncion, 'No ha sido inicializado', 'Funci�n SUM()' ));
end;

procedure TZetaSumFunction.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ParametrosFijos( 1 );
     TipoRegresa := resDouble;
end;


{******************** TZetaAverageFunction ***************}

type
  TZetaAverageFunction = class(TZetaSumFunction)
  private
    Count : longint;
    aResult : TQREvResult;
  public
    function Calculate : TQREvResult; override;
    procedure Aggregate; override;
    procedure Reset; override;
  end;

procedure TZetaAverageFunction.Reset;
begin
  inherited Reset;
  aResult := ErrorCreate( ErrorEvaluador( eeFuncion, 'No hay registros', 'Funci�n AVERAGE()' ));
  Count := 0;
  IsAggreg := true;
end;

procedure TZetaAverageFunction.Aggregate;
var
  aValue : TQREvResult;
begin
  inherited Aggregate;
  inc(Count);
  aValue := SumResult;  // EZM inherited Calculate;
  aResult.Kind := resDouble;
  case aValue.Kind of
    ResInt : aResult.DblResult := aValue.IntResult / Count;
    ResDouble : aResult.DblResult := aValue.DblResult / Count;
  else
    aResult := aValue;            // Recibe el error
  end;
end;

function TZetaAverageFunction.Calculate : TQREvResult;
begin
     Aggregate; { EZM }
     if ResAssigned then
       Result := aResult
     else
       Result := ErrorCreate( ErrorEvaluador( eeFuncion, 'No ha sido inicializado', 'Funci�n AVERAGE()' ));
end;

{******************** TZetaMaxFunction ***********************}

type
  TZetaMaxFunction = class(TZetaFunc)
  private
    MaxResult : TQREvResult;
    ResAssigned : boolean;
  public
    constructor Create(oEvaluador : TQREvaluator); override;
    function Calculate : TQREvResult; override;
    procedure Aggregate; override;
    procedure Reset; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

constructor TZetaMaxFunction.Create(oEvaluador : TQREvaluator);
begin
    inherited Create(oEvaluador);
    ResAssigned := false;
    IsAggreg := true;
end;

procedure TZetaMaxFunction.Reset;
begin
  ResAssigned := false;
end;

procedure TZetaMaxFunction.Aggregate;
var
  aValue : TQREvResult;
begin
  if ArgList.Count = 1 then
  begin
    aValue := Argument(0);
    if ResAssigned then
    begin
      case aValue.Kind of
        resInt : if aValue.IntResult > MaxResult.dblResult then
                   MaxResult.dblResult := aValue.IntResult;
        resDouble : if aValue.DblResult > MaxResult.DblResult then
                      MaxResult.DblResult := aValue.DblResult;
        resString : if aValue.StrResult > MaxResult.StrResult then
                      MaxResult.StrResult := aValue.StrResult;
        resBool : if aValue.booResult > MaxResult.BooResult then
                    MaxResult.BooResult := aValue.BooResult;
      else
        MaxResult := aValue;            // Pasa el Error
      end
    end else
    begin
      // Obliga a DOUBLE
      if ( aValue.Kind = resInt ) then
      begin
           MaxResult.Kind := resDouble;
           MaxResult.dblResult := aValue.intResult;
      end
      else
          MaxResult := aValue;
      ResAssigned := true;
    end
  end else
    MaxResult.Kind := resError;
end;

function TZetaMaxFunction.Calculate : TQREvResult;
begin
  Aggregate; { EZM }
  if ResAssigned then
    Result := MaxResult
  else
    Result := ErrorCreate( ErrorEvaluador( eeFuncion, 'No ha sido inicializado', 'Funci�n MAXIMO()' ));

end;

procedure TZetaMaxFunction.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ParametrosFijos( 1 );
     TipoRegresa := Argument(0).Kind;
end;

{******************* TZetaMinFunction ******************}

type
  TZetaMinFunction = class(TZetaFunc)
  private
    MinResult : TQREvResult;
    ResAssigned : boolean;
  public
    constructor Create(oEvaluador : TQREvaluator); override;
    function Calculate : TQREvResult; override;
    procedure Aggregate; override;
    procedure Reset; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

constructor TZetaMinFunction.Create(oEvaluador : TQREvaluator);
begin
     inherited Create(oEvaluador);
     ResAssigned := false;
     IsAggreg := true;
end;

procedure TZetaMinFunction.Reset;
begin
  ResAssigned := false;
end;

procedure TZetaMinFunction.Aggregate;
var
  aValue : TQREvResult;
begin
  if ArgList.Count = 1 then
  begin
    aValue := Argument(0);
    if ResAssigned then
    begin
      case aValue.Kind of
        resInt : if aValue.IntResult < MinResult.dblResult then
                   MinResult.dblResult := aValue.IntResult;
        resDouble : if aValue.DblResult < MinResult.DblResult then
                      MinResult.DblResult := aValue.DblResult;
        resString : if aValue.StrResult < MinResult.StrResult then
                      MinResult.StrResult := aValue.StrResult;
        resBool : if aValue.booResult > MinResult.BooResult then
                    MinResult.BooResult := aValue.BooResult;
      else
        MinResult := aValue; // Pasa El error
      end
    end else
    begin
      ResAssigned := true;
      // Obliga a DOUBLE
      if ( aValue.Kind = resInt ) then
      begin
           MinResult.Kind := resDouble;
           MinResult.dblResult := aValue.intResult;
      end
      else
          MinResult := aValue;
    end
  end else
    MinResult.Kind := resError;
end;

function TZetaMinFunction.Calculate : TQREvResult;
begin
  Aggregate; { EZM }
  if ResAssigned then
    Result := MinResult
  else
    Result := ErrorCreate( ErrorEvaluador( eeFuncion, 'No ha sido inicializado', 'Funci�n MINIMO()' ));
end;

procedure TZetaMinFunction.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ParametrosFijos( 1 );
     TipoRegresa := Argument(0).Kind;
end;

{******* TZetaCountFunction ***************}

type
  TZetaCountFunction = class(TZetaFunc)
  private
    FCount : integer;
  public
    constructor Create(oEvaluador : TQREvaluator); override;
    function Value(FiFo : TQRFiFo) : TQREvResult; override;
    procedure Reset; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

constructor TZetaCountFunction.Create(oEvaluador : TQREvaluator);
begin
     inherited Create(oEvaluador);
     FCount := 0;
     IsAggreg := true;
end;

function TZetaCountFunction.Value(FiFo : TQRFiFo) : TQREvResult;
begin
     GetArguments(FiFo);
     { EZM }
     Result.resFecha := FALSE;
     //if FiFo.Aggreg then
     // EZM: if IsAggreg then
        inc(FCount);
     Result.Kind := resInt;
     Result.intResult := FCount;
     FreeArguments;
end;

procedure TZetaCountFunction.Reset;
begin
  FCount := 0;
end;

procedure TZetaCountFunction.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resInt;
end;


{******* TZetaVarAux ***************}
type
  TZetaVARAUX = class(TZetaFunc)
  public
    function Calculate: TQREvResult; override;
  end;

function TZetaVARAUX.Calculate: TQREvResult;
begin
     // Si viene el par�metro se Almacena
     if ( ArgList.Count > 0 ) then
        ZetaEvaluador.VarAux := ParamFloat( 0 );

     // Siempre regresa el ultimo valor almacenado en VarAux
     with Result do
     begin
        Kind      := resDouble;
        resFecha  := FALSE;
        dblResult := ZetaEvaluador.VarAux;
     end;
end;

type
TZetaTF = class ( TZetaRegresa )
public
 procedure GetRequeridos( var TipoRegresa : TQREvResultType ); Override;
 function Calculate: TQREvResult; override;

end;

procedure TZetaTF.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resString;
end;

function TZetaTF.Calculate : TQREvResult;
 var oLista : ListasFijas;
begin
     with Result do
     begin
          Kind := resString;
          EsFecha := FALSE;
          oLista := ListasFijas(ParamInteger(0));
          strResult := ZetaCommonLists.ObtieneElemento( oLista,
                                                        ParamInteger(1)-GetOffSet(oLista));
     end;
end;


{*********************** REGISTER *********************}

procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );
begin
     with FunctionLibrary do
     begin
          {Funciones de Totalizacion}
          RegisterFunction( TZetaCOUNTFunction, 'COUNT','N�mero de Registros');
          RegisterFunction( TZetaSUMFunction, 'SUM','Suma');
          RegisterFunction( TZetaMINFunction, 'MINIMO','Valor M�nimo');
          RegisterFunction( TZetaMAXFunction, 'MAXIMO','Valor M�ximo');
          RegisterFunction( TZetaAVERAGEFunction, 'AVERAGE','Valor Promedio');

          {Funciones de Uso Generales}                                                     
          RegisterFunction( TZetaABS, 'ABS', 'Valor Absoluto de un N�mero' );              
          RegisterFunction( TZetaALLTRIM, 'ALLTRIM', 'Elimina Espacios' );
          RegisterFunction( TZetaANIV, 'ANIV', 'Aniversario en Rango de Fechas' );         
          RegisterFunction( TZetaCTOD, 'CTOD' , 'Formato ''DD/MM/AA'' a Fecha' );
          RegisterFunction( TZetaASC, 'ASC', 'Valor ASCII de un Caracter' );               
          RegisterFunction( TZetaAT, 'AT', 'Posici�n de Texto dentro Otro' );              
          RegisterFunction( TZetaA_HORAS, 'A_HORAS', 'Minutos a N�mero de Horas' );        
          RegisterFunction( TZetaA_HORA_STR, 'A_HORA_STR', 'Minutos a Formato: ''HHMM''' );
          RegisterFunction( TZetaA_MINUTOS, 'A_MINUTOS', 'Formato ''HHMM'' a Minutos' );
          RegisterFunction( TZetaCASE, 'CASE', 'Case' );
          RegisterFunction( TZetaCHR, 'CHR', 'Caracter de Un Ascii');
          RegisterFunction( TZetaDATE, 'DATE', 'Fecha del Sistema' );
          RegisterFunction( TZetaDAY, 'DAY' , 'N�mero de D�a de una Fecha' );
          RegisterFunction( TZetaDENTRO, 'DENTRO', 'Texto DENTRO de Varios Textos' );
          RegisterFunction( TZetaDIA_DEL_MES, 'DIA_DEL_ME', 'Primer o Ultimo D�a del Mes');
          RegisterFunction( TZetaDIF_MINUTOS, 'DIF_MINUTO', 'Diferencias entre Checadas' );          
          RegisterFunction( TZetaDOW, 'DOW', 'Dia de la Semana de una Fecha');
          RegisterFunction( TZetaDTOC, 'DTOC', 'Fecha a Caracter');
          RegisterFunction( TZetaDTOS, 'DTOS', 'Fecha a Formato YYYYMMDD');
          RegisterFunction( TZetaEMPTY, 'EMPTY', 'Si un Texto est� Vacio' );
          RegisterFunction( TZetaFALSE, 'FALSE', 'Constante FALSE');
          RegisterFunction( TZetaFECHA_CORTA, 'FECHA_CORTA', 'Fecha Corta');
          RegisterFunction( TZetaFECHA_DIA, 'FECHA_DIA', 'Nombre del D�a de una Fecha');
          RegisterFunction( TZetaFECHA_STR, 'FECHA_STR', 'Fecha en Palabras');
          RegisterFunction( TZetaHORA24, 'HORA24','Resta 24 a Horas Mayores a 24');
          RegisterFunction( TZetaIIF, 'IF', 'Expresi�n IF');
          RegisterFunction( TZetaINT, 'INT', 'Parte Entera de un N�mero');
          RegisterFunction( TZetaLEFT, 'LEFT', 'Parte Izquierda de un Texto');
          RegisterFunction( TZetaLEN, 'LEN', 'Longitud de un Texto');
          RegisterFunction( TZetaLOWER, 'LOWER', 'Covierte a Min�sculas');
          RegisterFunction( TZetaLTRIM, 'LTRIM', 'Quita Espacios de la Izquierda');
          RegisterFunction( TZetaMAX, 'MAX', 'M�ximo entre 2 N�meros');
          RegisterFunction( TZetaMIN, 'MIN', 'M�nimo entre 2 N�meros');
          RegisterFunction( TZetaMONTH, 'MONTH', 'N�mero del Mes de una Fecha');
          RegisterFunction( TZetaPADC, 'PADC', 'Agrega Caracteres a un Texto');                      
          RegisterFunction( TZetaPADL, 'PADL', 'Agrega Carac. a la Izquierda');
          RegisterFunction( TZetaPADR, 'PADR', 'Agrega Carac. a la Derecha');
          RegisterFunction( TZetaPROTECCION, 'PROTECCION', 'Monto en Letra en un Rengl�n');
          RegisterFunction( TZetaRANGO, 'RANGO', 'Valor en un Rango');
          RegisterFunction( TZetaREPLICATE, 'REPLICATE', 'Texto Repetido Varias Veces');
          RegisterFunction( TZetaRIGHT, 'RIGHT', 'Parte Derecha de un Texto');                    
          RegisterFunction( TZetaROUND, 'ROUND', 'Redondeo de un N�mero');
          RegisterFunction( TZetaMaxRound, 'MXR', 'Redondeo Topado a Cero' ); //CV: Se agrego para la Declaracion Anual
          RegisterFunction( TZetaRTRIM, 'RTRIM', 'Elimina Espacios de la Derecha');
          RegisterFunction( TZetaSPACE, 'SPACE', 'Espacio Relleno');
          RegisterFunction( TZetaSTR, 'STR', 'Numero a Texto');
          RegisterFunction( TZetaSTRTRAN, 'STRTRAN', 'Sustituci�n un Texto por Otro');
          RegisterFunction( TZetaSUBSTR, 'SUBSTR', 'Parte de Otro');
          RegisterFunction( TZetaTH, 'TH', 'Agrega Puntos (:) a la Hora');
          RegisterFunction( TZetaTIME, 'TIME', 'Hora Actual' );
          RegisterFunction( TZetaTRANSFORM, 'TRANSFORM', 'Formatea un Texto');
          RegisterFunction( TZetaTRUE, 'TRUE', 'Constante TRUE');
          RegisterFunction( TZetaUPPER, 'UPPER', 'Convierte a May�sculas');
          RegisterFunction( TZetaVAL, 'VAL', 'Texto a N�mero' );
          RegisterFunction( TZetaYEAR, 'YEAR', 'N�mero del A�o de una Fecha');
          RegisterFunction( TZetaAYER, 'AYER', 'Fecha del Ultimo Viernes');
          RegisterFunction( TZetaLCERO, 'LCERO', 'Monto en Letras' );
          RegisterFunction( TZetaCero, 'CERO','Numero a Texto' );
          RegisterFunction( TZetaDIASENCURSO, 'DIAS_EN_CU','D�as Transcurridos');
          RegisterFunction( TZetaLASTMONDAY, 'LAST_MONDAY', 'Ultimo Lunes');
          RegisterFunction( TZetaMESMES, 'MES_MES', 'Mes en Letras');
          RegisterFunction( TZetaNPESOS, 'NPESOS', 'Redondeo del Monto a la Moneda');
          RegisterFunction( TZetaQUITAACENTOS, 'QUITA_ACEN', 'Quita Acentos');
          RegisterFunction( TZetaSTRZERO, 'STRZERO', 'Numero a Texto');
          RegisterFunction( TZetaSTUFF, 'STUFF', 'Sustituci�n un Texto por Otro');
          RegisterFunction( TZetaTIEMPO, 'TIEMPO', 'Tiempo Trancurrido');
          RegisterFunction( TZetaYEARLETRA, 'YEAR_LETRA', 'Un A�o en Letras');
          RegisterFunction( TZetaCAJA, 'CAJA', '[______]');
          RegisterFunction( TZetaSALTA, 'SALTA', 'Brinco de Linea');
          RegisterFunction( TZetaMES, 'MES','Mes en Espa�ol');
          RegisterFunction( TZetaDIASLETRA, 'DIAS_LETRA','Equivalente de D�as en Letra');
          RegisterFunction( TZetaRAYA, 'RAYA','Repite caracter' );
          RegisterFunction( TZetaLIMPIASTR, 'LIMPIA_STR','Elimina n�meros en texto' );
          RegisterFunction( TZetaVARAUX, 'VX','Almacena valor temp' );

          {$ifdef DOS_CAPAS}
          RegisterFunction( TZetaTF,'TF', 'Info de Tabla Fija' );
          {$else}
            {$ifndef TRESS}
            RegisterFunction( TZetaTF,'TF', 'Info de Tabla Fija' );
            {$endif}
          {$endif}
     end;
end;
{$ifdef FALSE}



{$endif}

                                                                     
end.


