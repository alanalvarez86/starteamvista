unit FEmpresasEdit;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Mask, DBCtrls, ExtCtrls, Buttons, Db,
     ZBaseDlgModal,
     ZetaKeyCombo,
     ZetaNumero,
     DDBConfig, ZetaDBTextBox, ZetaEdit, FSeleccionarConfidencialidad;

type
  TEmpresaEdit = class(TZetaDlgModal)
    DBGroupBox: TGroupBox;
    CM_ALIASlbl: TLabel;
    CM_USRNAMElbl: TLabel;
    CM_PASSWRDlbl: TLabel;
    PasswordCheckLBL: TLabel;
    CrearAlias: TSpeedButton;
    CM_DATOSlbl: TLabel;
    CM_USRNAME: TDBEdit;
    PasswordCheck: TEdit;
    CM_PASSWRD: TDBEdit;
    CM_CODIGOlbl: TLabel;
    CM_NOMBRElbl: TLabel;
    CM_ACUMULAlbl: TLabel;
    CM_EMPATElbl: TLabel;
    CM_NOMBRE: TDBEdit;
    CM_EMPATE: TZetaDBNumero;
    CM_ACUMULA: TZetaDBKeyCombo;
    CM_USACAFE: TDBCheckBox;
    DBNavigator: TDBNavigator;
    DataSource: TDataSource;
    CM_TIPO_Elbl: TLabel;
    CM_TIPO_E: TComboBox;
    CM_ALIAS: TZetaDBTextBox;
    CM_DATOS: TZetaDBTextBox;
    DigitoGafetesLBL: TLabel;
    CM_DIGITO: TDBEdit;
    CM_CODIGO: TZetaDBEdit;
    gbConfidencialidad: TGroupBox;
    btSeleccionarConfiden: TSpeedButton;
    listaConfidencialidad: TListBox;
    rbConfidenNinguna: TRadioButton;
    rbConfidenAlgunas: TRadioButton;
    CM_USACASE: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure DataSourceStateChange(Sender: TObject);
    procedure CrearAliasClick(Sender: TObject);
    procedure CM_PASSWRDChange(Sender: TObject);
    procedure CM_TIPO_EChange(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure CM_DIGITOKeyPress(Sender: TObject; var Key: Char);
    procedure SetControlesDigito;
    procedure FormDestroy(Sender: TObject);
    procedure rbConfidenNingunaClick(Sender: TObject);
    procedure rbConfidenAlgunasClick(Sender: TObject);
    procedure btSeleccionarConfidenClick(Sender: TObject);
  private
    { Private declarations }
    dmDBConfig: TdmDBConfig;
    FModo: TDataSetState;
    lValoresConfidencialidad : TStringList;

    procedure SetControls( const lEnabled: Boolean );
    procedure SetModo( Value: TDataSetState );
    procedure SetDBConfig( Value: TdmDBConfig );

    procedure SetListConfidencialidad( sValores : string );
    procedure GetConfidencialidad;


  public
    { Public declarations }
    property DBConfig: TdmDBConfig write SetDBConfig;
    property Modo: TDataSetState read FModo write SetModo;
  end;

var
  EmpresaEdit: TEmpresaEdit;

implementation

uses ZetaAliasCrear,
     FHelpContext,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaDialogo;

{$R *.DFM}

procedure TEmpresaEdit.FormCreate(Sender: TObject);
begin
     inherited;
     ZetaCommonLists.LlenaLista( lfTipoCompanyName, CM_TIPO_E.Items );
     HelpContext := H00020_Agregar_y_borrar_empresas;
     lValoresConfidencialidad := TStringList.Create;
end;

procedure TEmpresaEdit.SetDBConfig( Value: TdmDBConfig );
begin
     if ( dmDBConfig <> Value ) then
     begin
          dmDBConfig := Value;
          if ( dmDBConfig <> nil ) then
          begin
               with dmDBConfig do
               begin
                    //LlenaListaAlias( CM_ALIAS.Lista );
                    LlenaListaCompanias( CM_ACUMULA.Lista );
                    //LlenaListaNivel0( CM_NIVEL0.Lista );
                    LeeListaNivel0( lValoresConfidencialidad );
                    ConectarCompany( DataSource );
               end;
          end;
     end;
end;

procedure TEmpresaEdit.SetModo( Value: TDataSetState );
begin
     if ( FModo <> Value ) then
     begin
          FModo := Value;
          with dmDBConfig do
          begin
               case Modo of
                    dsEdit: ModificaCompany;
                    dsInsert: AgregaCompany;
               end;
          end;
     end;
end;

procedure TEmpresaEdit.SetControls( const lEnabled: Boolean );
begin
     with PasswordCheck do
     begin
          if lEnabled then
             Text := '';
          Enabled := lEnabled;
          PasswordCheckLBL.Enabled := Enabled;
     end;
end;

procedure TEmpresaEdit.DataSourceDataChange(Sender: TObject; Field: TField);
var
   eTipo : eTipoCompany;
begin
     inherited;
     if ( Field = nil ) then
     begin
          PasswordCheck.Text := CM_PASSWRD.Text;
          CM_TIPO_E.Text := dmDBConfig.GetTipoEmpresaEdit;
          SetControls( False );
          SetControlesDigito;
          with DataSource.DataSet do
          begin          
               CM_TIPO_E.Enabled := True;

               if ( State in [dsBrowse] ) then
                  GetConfidencialidad
               else if (State in [dsInsert]) then
                    rbConfidenNinguna.Checked := TRUE;

               if ( State in [dsBrowse, dsEdit] ) then
               begin
                   eTipo := dmDBConfig.GetTipoEmpresaReal;
                   CM_TIPO_E.Enabled := ( eTipo <> tc3Prueba );
               end;
          end;
     end;
end;

procedure TEmpresaEdit.DataSourceStateChange(Sender: TObject);
var
   ModalResultBuffer: TModalResult;
   lEdit: Boolean;
begin
     inherited;
     with DataSource do
     begin
          case State of
               dsEdit: Caption := 'Modificar ' + dmDBConfig.GetCompanyName;
               dsInsert: Caption := 'Agregar Compa��a';
          else
              Caption := 'Consultar Compa��a';
          end;
          lEdit := ( State in [ dsEdit, dsInsert ] );
     end;
     OK.Enabled := lEdit;
     DBNavigator.Enabled := not lEdit;
     with Cancelar do
     begin
          ModalResultBuffer := ModalResult;
          if lEdit then
          begin
               Kind := bkCancel;
               Cancel := False;
               Caption := '&Cancelar';
          end
          else
          begin
               Kind := bkClose;
               Cancel := True;
               Caption := '&Salir';
          end;
          ModalResult := ModalResultBuffer;
     end;
end;

procedure TEmpresaEdit.CM_PASSWRDChange(Sender: TObject);
begin
     inherited;
     SetControls( True );
end;

procedure TEmpresaEdit.CM_TIPO_EChange(Sender: TObject);
begin
     inherited;
     dmDBConfig.CambiaTipo( CM_TIPO_E.Text );
     SetControlesDigito;
end;

procedure TEmpresaEdit.OKClick(Sender: TObject);
begin
     inherited;
     if not ( ( UpperCase( CM_TIPO_E.Text ) = 'PRESUPUESTO') ) then
     begin
          if ( CM_PASSWRD.Text <> PasswordCheck.Text ) then
             ZetaDialogo.zError( Caption, 'Clave De Acceso No Fu� Capturada Correctamente', 0 )
          else
          begin
               try
                  with dmDBConfig do
                  begin
                       SetCompanyPassword( CM_PASSWRD.Text );
                       GrabaCompany;
                  end;
               except
                     on Error: Exception do
                     begin
                          ZetaDialogo.zExcepcion( Caption, 'Error Al Agregar Empresa', Error, 0 );
                     end;
               end;
          end;
     end
     else
     begin
          ZetaDialogo.zError( Caption, 'No est� permitido seleccionar una Empresa de tipo Presupuesto en Profesional.', 0 );
          CM_TIPO_E.SetFocus;
     end;
end;

procedure TEmpresaEdit.CancelarClick(Sender: TObject);
begin
     inherited;
     case DataSource.State of
          dsEdit, dsInsert:
          begin
           dmDBConfig.CancelaCambiosCompany;
           GetConfidencialidad;
          end;
     else
         Close;
     end;
end;

procedure TEmpresaEdit.CrearAliasClick(Sender: TObject);
var
   sAliasName, sDatabaseName: String;
begin
     inherited;
     with CM_ALIAS do
     begin
          sAliasName := Field.AsString;
          if CreaUnAliasDatabase( dmDBConfig.AliasManager, sAliasName, sDatabaseName ) then
          begin
               dmDBConfig.SetEditMode;
               CM_DATOS.Field.AsString := sDatabaseName;
               Field.AsString := sAliasName;
          end;
     end;
end;

procedure TEmpresaEdit.CM_DIGITOKeyPress(Sender: TObject; var Key: Char);
begin
     inherited;
     if ZetaCommonTools.EsMinuscula( Key ) then     // Solo acepta may�sculas
        Key := ZetaCommonTools.aMayuscula( Key );
     if ( Key >= #32 ) and
        ( ( not ( ZetaCommonTools.EsDigito( Key ) ) ) and ( not ( ZetaCommonTools.EsMayuscula( Key ) ) ) ) then
        Key := #0;       // Si no es caracter de control y no es D�gito o Letra May�scula se ignora
end;

procedure TEmpresaEdit.SetControlesDigito;
var
   lEnabled : Boolean;
begin
     lEnabled := ( dmDBConfig.GetTipoCompanyName( dmDBConfig.tqCompany.FieldByName( 'CM_CONTROL' ).AsString ) = tc3Datos );
     DigitoGafetesLBL.Enabled := lEnabled;
     CM_DIGITO.Enabled := lEnabled;

     gbConfidencialidad.Enabled := lEnabled;

     rbConfidenNinguna.Enabled := lEnabled;
     rbConfidenAlgunas.Enabled := lEnabled;
     listaConfidencialidad.Enabled := lEnabled;
     btSeleccionarConfiden.Enabled := lEnabled;



end;

procedure TEmpresaEdit.FormDestroy(Sender: TObject);
begin
  inherited;
  FreeAndNil( lValoresConfidencialidad ) ;
end;


procedure TEmpresaEdit.SetListConfidencialidad( sValores : string );
var
   Lista : TStringList;
   i, j : integer;
begin
    Lista := TStringList.Create;

    sValores := Trim( sValores );
    if StrLleno( sValores ) then
        Lista.CommaText  := sValores;

    listaConfidencialidad.Items.Clear;

    rbConfidenNinguna.OnClick := nil;
    rbConfidenAlgunas.OnClick := nil;
    rbConfidenNinguna.Checked := False;
    rbConfidenAlgunas.Checked := False;

    for i:= 0 to lValoresConfidencialidad.Count - 1 do
    begin
        if ( Lista.Count > 0 ) then
        begin
                j := Lista.IndexOf( Trim( lValoresConfidencialidad.Names[i] )  );

                if ( j >= 0 ) then
                   listaConfidencialidad.Items.Add( lValoresConfidencialidad[i] )

                //CM_NIVEL0.Checked[i] :=  ( j >= 0 );
        end
    end;

    rbConfidenNinguna.Checked :=   listaConfidencialidad.Items.Count = 0;
    rbConfidenAlgunas.Checked := not rbConfidenNinguna.Checked;

    if ( gbConfidencialidad.Enabled ) then
    begin
        listaConfidencialidad.Enabled := rbConfidenAlgunas.Checked;
        btSeleccionarConfiden.Enabled := rbConfidenAlgunas.Checked;
    end;

    rbConfidenNinguna.OnClick := rbConfidenNingunaClick;
    rbConfidenAlgunas.OnClick := rbConfidenAlgunasClick;

    FreeAndNil( Lista );

end;

procedure TEmpresaEdit.GetConfidencialidad;
begin
   if (  DataSource.DataSet  <> nil ) then
   with DataSource.DataSet do
   begin
        SetListConfidencialidad(  FieldByName('CM_NIVEL0').AsString );
   end;
end;


procedure TEmpresaEdit.btSeleccionarConfidenClick(Sender: TObject);
var
   selConfi : TSeleccionarConfidencialidad;
   sCM_NIVEL0Nuevo : string;
begin
  inherited;

  selConfi := TSeleccionarConfidencialidad.Create( Self );
  sCM_NIVEL0Nuevo := selConfi.GetConfidencialidad( DataSource.DataSet.FieldByName('CM_NIVEL0').AsString, lValoresConfidencialidad );

  if (  sCM_NIVEL0Nuevo <>  DataSource.DataSet.FieldByName('CM_NIVEL0').AsString ) then
  begin
     with DataSource.DataSet do
     begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

           FieldByName('CM_NIVEL0').AsString  := sCM_NIVEL0Nuevo;
           GetConfidencialidad;
     end;
  end;

  FreeAndNil ( selConfi ) ;

end;

procedure TEmpresaEdit.rbConfidenNingunaClick(Sender: TObject);
begin
  inherited;

   with DataSource.DataSet do
   begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

        FieldByName('CM_NIVEL0').AsString  :=  '';
   end;
   GetConfidencialidad;
end;

procedure TEmpresaEdit.rbConfidenAlgunasClick(Sender: TObject);
begin
  inherited;
     if ( rbConfidenAlgunas.Checked ) then
     begin
        btSeleccionarConfidenClick( Sender );
        GetConfidencialidad;
     end;

end;

end.
