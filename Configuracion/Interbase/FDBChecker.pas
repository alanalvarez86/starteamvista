unit FDBChecker;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ExtCtrls, DB, DBTables,
     ODSI, OCL, IB_Components,
     FAutoServer,
     ZetaRegistryServer,
     ZBaseDlgModal;

type
  TDBChecker = class(TZetaDlgModal)
    ResultadoGB: TGroupBox;
    Resultados: TMemo;
    DirectTest: TCheckBox;
    ODBCTest: TCheckBox;
    HdbcComparte: THdbc;
    HdbcEmpresa: THdbc;
    oeCompany: TOEQuery;
    dbComparte: TIB_Connection;
    dbEmpresa: TIB_Connection;
    Autoconfigurar: TBitBtn;
    IB_Tx_Comparte: TIB_Transaction;
    tqQuery: TIB_Cursor;
    tqCompany: TIB_Cursor;
    oeVersion: TOEQuery;
    tqVersion: TIB_Cursor;
    ckDigito: TCheckBox;
    tqUpdateCompany: TIB_Cursor;
    tqDigito: TIB_Cursor;
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure DirectTestClick(Sender: TObject);
    procedure AutoconfigurarClick(Sender: TObject);
  private
    { Private declarations }
    FAutoServer: TAutoServer;
    FError: Boolean;
    FRefrescarShell: Boolean;
    function DirectOpenDB( Database: TIB_Connection; const sNombre, sDatabase, sUserName, sPassword: String; const lComparte: Boolean = FALSE ): Boolean;
    function GetRegistry: TZetaRegistryServer;
    function ODBCOpenDB(Database: THdbc; const sNombre, sDatabase, sUserName, sPassword: String; const lComparte: Boolean = FALSE ): Boolean;
    function TestDirecto: Boolean;
    function TestODBC: Boolean;
    procedure AgregaTexto(Lista: TStrings; sTexto: String);
    procedure SetControls;
    procedure WriteLog(const sMensaje: String);
    procedure WriteError(const sMensaje: String; Error: Exception);
    procedure CleanResultados;
    function GetVersionODBC(Database: THdbc; const lComparte: Boolean ): String;
    function GetVersionDirect(Database: TIB_Connection; const lComparte: Boolean ): String;
    function ActualizarDigitoEmpresa: Boolean;
    procedure SetDigitoEmpresa( const sEmpresa: String );
    procedure PreparaQuery( IBCursor: TIB_Cursor; const sScript: String );
  protected
    { Protected declarations }
    property Registry: TZetaRegistryServer read GetRegistry;
  public
    { Public declarations }
    property AutoServer: TAutoServer read FAutoServer write FAutoServer;
    property RefrescarShell: Boolean read FRefrescarShell;
  end;

var
  DBChecker: TDBChecker;

implementation

uses DDBConfig,
     FHelpContext,
     FAutoClasses,
     ZetaWinAPITools,
     ZetaAliasManager,
     ZetaServerTools,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZGlobalTress,
     ZetaCommonTools,
     ZetaDialogo;

{$R *.DFM}

const
     SQL_COMPANYS = 'select CM_CODIGO, CM_NOMBRE, CM_ALIAS, CM_PASSWRD, CM_USRNAME, CM_DATOS, CM_DIGITO, CM_CONTROL, CM_NIVEL0 from COMPANY order by CM_NOMBRE';
     SQL_DATABASE = 'update COMPANY set CM_DATOS = :CM_DATOS where ( CM_CODIGO = :CM_CODIGO )';
     SQL_DIGITO_EMPRESA = 'select GL_FORMULA as DIGITO from GLOBAL where ( GL_CODIGO = %d )';
     UPDATE_DIGITO = 'update COMPANY set CM_DIGITO = :CM_DIGITO where CM_CODIGO = :Codigo';
     aVersionScripts: array[ FALSE..TRUE ] of pChar = ( 'select GL_FORMULA as VERSION from GLOBAL where ( GL_CODIGO = %d )',
                                                        'select CL_TEXTO as VERSION from CLAVES where ( CL_NUMERO = %d )' );
     aVersionPosicion: array[ FALSE..TRUE ] of Integer = ( K_GLOBAL_VERSION_DATOS, K_POS_CLAVES_VERSION );

procedure TDBChecker.FormShow(Sender: TObject);

procedure EnableCheckBox( Control: TCheckBox; const lChecked, lEnabled: Boolean );
begin
     with Control do
     begin
          Checked := lChecked;
          Visible := lEnabled;
     end;
end;

begin
     inherited;
     case AutoServer.PlataformaAuto of
          ptCorporativa:
          begin
               EnableCheckBox( ODBCTest, True, True );
               EnableCheckBox( DirectTest, True, True );
          end;
          ptProfesional:
          begin
               EnableCheckBox( ODBCTest, False, False );
               EnableCheckBox( DirectTest, True, False );
          end;
     end;
     EnableCheckBox( ckDigito, False, True );      // Independientemente de que DirectTest est� visible
     FRefrescarShell := FALSE;
     HelpContext := H00012_Probando_el_acceso;
end;

function TDBChecker.GetRegistry: TZetaRegistryServer;
begin
     Result := DDBConfig.dmDBConfig.Registry;
end;

procedure TDBChecker.CleanResultados;
begin
     with Resultados.Lines do
     begin
          BeginUpdate;
          Clear;
          EndUpdate;
     end;
end;
procedure TDBChecker.AgregaTexto( Lista: TStrings; sTexto: String );
var
   pValor, pStart: PChar;
   sValor: String;
begin
     with Lista do
     begin
          BeginUpdate;
          try
             pValor := Pointer( sTexto );
             if ( pValor <> nil ) then
             begin
                  while ( pValor^ <> #0 ) do
                  begin
                       pStart := pValor;
                       while not ( pValor^ in [ #0, #10, #13 ] ) do
                       begin
                            Inc( pValor );
                       end;
                       SetString( sValor, pStart, ( pValor - pStart ) );
                       Add( sValor );
                       if ( pValor^ = #13 ) then
                          Inc( pValor );
                       if ( pValor^ = #10 ) then
                          Inc( pValor );
                  end;
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TDBChecker.WriteLog( const sMensaje: String );
begin
     with Resultados.Lines do
     begin
          BeginUpdate;
          Add( sMensaje );
          EndUpdate;
     end;
end;

procedure TDBChecker.WriteError( const sMensaje: String; Error: Exception );
begin
     FError := True;
     WriteLog( StringOfChar( '-', 40 ) );
     WriteLog( sMensaje );
     AgregaTexto( Resultados.Lines, ZetaCommonTools.GetExceptionInfo( Error ) );
end;

function TDBChecker.GetVersionDirect(Database: TIB_Connection; const lComparte: Boolean): String;
const
     MENSAJE_VERSION = '( V. %s )';
begin
     try
        with tqVersion do
        begin
             Active := False;
             IB_Connection := Database;
             with SQL do
             begin
                  Clear;
                  Add( Format( aVersionScripts[ lComparte ], [ aVersionPosicion[ lComparte ] ] ) );
             end;
             Active := True;
             if not EOF then
                Result := Format( MENSAJE_VERSION, [ FieldByName( 'VERSION' ).AsString ] )
             else
                Result := Format( MENSAJE_VERSION, [ 'Indefinida' ] );
        end;
     except
           on Error: Exception do
           begin
                Result := '...ERROR Al Leer Versi�n de Datos Por ODBC';
           end;
     end;
end;

function TDBChecker.DirectOpenDB( Database: TIB_Connection; const sNombre, sDatabase, sUserName, sPassword: String; const lComparte: Boolean = FALSE  ): Boolean;
begin
     Result := False;
     try
        with Database do
        begin
             Connected := False;
             DatabaseName := sDatabase;
             UserName := sUserName;
             Password := sPassword;
             Connected := True;
             Result := Connected;
        end;
//        WriteLog( 'OK por Acceso Directo -> ' + sNombre );
        WriteLog( 'OK por Acceso Directo -> ' + sNombre + '  ' + GetVersionDirect( DataBase, lComparte ) );
     except
           on Error: Exception do
           begin
                WriteError( 'ERROR Acceso Directo: Empresa ' + sNombre + ' ( ' + sDatabase + ' )', Error );
           end;
     end;
end;

function TDBChecker.GetVersionODBC( Database: THdbc; const lComparte: Boolean ): String;
const
     MENSAJE_VERSION = '( V. %s )';
begin
     try
        with oeVersion do
        begin
             Active := False;
             Hdbc := DataBase;
             with SQL do
             begin
                  Clear;
                  Add( Format( aVersionScripts[ lComparte ], [ aVersionPosicion[ lComparte ] ] ) );
             end;
             Active := True;
             if not IsEmpty then
                Result := Format( MENSAJE_VERSION, [ FieldByName( 'VERSION' ).AsString ] )
             else
                Result := Format( MENSAJE_VERSION, [ 'Indefinida' ] );
        end;
     except
           on Error: Exception do
           begin
                Result := '...ERROR Al Leer Versi�n de Datos Por ODBC';
           end;
     end;
end;

function TDBChecker.ODBCOpenDB(Database: THdbc; const sNombre, sDatabase, sUserName, sPassword: String; const lComparte: Boolean = FALSE ): Boolean;
begin
     Result := False;
     try
        with Database do
        begin
             Connected := False;
             Driver := 'INTERSOLV InterBase ODBC Driver (*.gdb)';
             with Attributes do
             begin
                  Clear;
                  Add( Format( 'Database=%s', [ sDatabase ] ) );
                  Add( 'ApplicationUsingThreads=1' );
                  Add( 'LockTimeOut=0' );
                  Add( 'WorkArounds=1' );
             end;
             UserName := sUserName;
             Password := sPassword;
             Connected := True;
             Result := Connected;
        end;
//        WriteLog( 'OK por ODBC -> ' + sNombre )
        WriteLog( 'OK por ODBC -> ' + sNombre + '  ' + GetVersionODBC( DataBase, lComparte ) );
     except
           on Error: Exception do
           begin
                WriteError( 'ERROR ODBC: Empresa ' + sNombre + ' ( ' + sDatabase + ' )', Error );
           end;
     end;
end;

procedure TDBChecker.SetControls;
begin
     OK.Enabled := DirectTest.Checked or ODBCTest.Checked;
     ckDigito.Enabled := DirectTest.Checked;
end;

procedure TDBChecker.DirectTestClick(Sender: TObject);
begin
     inherited;
     SetControls;
end;

function TDBChecker.ActualizarDigitoEmpresa: Boolean;
begin
     Result := ckDigito.Checked;
end;

procedure TDBChecker.SetDigitoEmpresa( const sEmpresa: String );
var
   sDigito: String;
begin
     sDigito := VACIO;
     with tqDigito do
     begin
          Active := True;
          try
             if not EOF then
                sDigito := FieldByName( 'DIGITO' ).AsString;
          finally
             Active := False;
          end;
     end;
     if strLleno( sDigito ) then
     begin
          with IB_Tx_Comparte do
          begin
               if not InTransaction then
                  StartTransaction;
          end;
          try
             with tqUpdateCompany do
             begin
                  ParamByName( 'Codigo' ).AsString := sEmpresa;
                  ParamByName( 'CM_DIGITO' ).AsString := sDigito;
                  ExecSQL;
             end;
             IB_Tx_Comparte.Commit;
             WriteLog( Format( 'Se Actualiz� D�gito En Gafetes -> %s', [ sDigito ] ) );
             FRefrescarShell := TRUE;
          except
                on Error: Exception do
                begin
                     IB_Tx_Comparte.Rollback;
                     WriteError( Format( 'ERROR Al Configurar D�gito de Empresa a %s', [ sEmpresa ] ), Error );
                end;
          end;
     end;
end;

procedure TDBChecker.PreparaQuery( IBCursor: TIB_Cursor; const sScript: String );
begin
     with IBCursor do
     begin
          Active := False;
          with SQL do
          begin
               Clear;
               Add( sScript );
          end;
     end;
end;

function TDBChecker.TestDirecto: Boolean;
var
   lOk : Boolean;
begin
     if DirectTest.Checked then
     begin
          WriteLog( '------- Probando Acceso Directo ------- ' );
          with Registry do
          begin
               Result := DirectOpenDB( dbComparte, 'Comparte', Database, UserName, Password, TRUE );
          end;
          if Result then
          begin
               try
                  PreparaQuery( tqCompany, SQL_COMPANYS );
                  tqCompany.Active := True;
                  WriteLog( 'Lectura De Empresas Por Acceso Directo Tuvo Exito' );
                  if ActualizarDigitoEmpresa then
                  begin
                       PreparaQuery( tqUpdateCompany, UPDATE_DIGITO );
                       PreparaQuery( tqDigito, Format( SQL_DIGITO_EMPRESA, [ ZGlobalTress.K_GLOBAL_DIGITO_EMPRESA ] ) );
                  end;
               except
                     on Error: Exception do
                     begin
                          WriteError( 'ERROR Al Leer Empresas Por Acceso Directo', Error );
                          Result := False;
                     end;
               end;
               if Result then
               begin
                    with tqCompany do
                    begin
                         while not Eof do
                         begin
                              lOk := DirectOpenDB( dbEmpresa,
                                                   FieldByName( 'CM_NOMBRE' ).AsString,
                                                   FieldByName( 'CM_DATOS' ).AsString,
                                                   FieldByName( 'CM_USRNAME' ).AsString,
                                                   ZetaServerTools.Decrypt( FieldByName( 'CM_PASSWRD' ).AsString ) );
                              if lOk and ActualizarDigitoEmpresa and
                                 strVacio( FieldByName( 'CM_DIGITO' ).AsString ) and
                                 strVacio( FieldByName( 'CM_NIVEL0' ).AsString ) and
                                 ( DDBConfig.dmDBConfig.GetTipoCompanyName( FieldByName( 'CM_CONTROL' ).AsString ) = tc3Datos ) then    // Solo Empresas de Datos de Tress
                              begin
                                   SetDigitoEmpresa( FieldByName( 'CM_CODIGO' ).AsString );    // Ya est�n abiertas las bases de datos de Comparte y Empresa
                              end;
                              Result := lOk and Result;
                              Next;
                         end;
                    end;
               end;
          end;
          if Result then
             WriteLog( '** Acceso A Bases De Datos Por Acceso Directo Tuvo Exito **' );
     end
     else
         Result := True;
end;

function TDBChecker.TestODBC: Boolean;
begin
     if ODBCTest.Checked then
     begin
          WriteLog( '------- Probando ODBC ------- ' );
          with Registry do
          begin
               Result := ODBCOpenDB( hdbcComparte, 'Comparte', Database, UserName, Password, TRUE );
          end;
          if Result then
          begin
               try
                  with oeCompany do
                  begin
                       Active := False;
                       with SQL do
                       begin
                            Clear;
                            Add( SQL_COMPANYS );
                       end;
                       Active := True;
                  end;
                  WriteLog( 'Lectura De Empresas Por ODBC Tuvo Exito' );
               except
                     on Error: Exception do
                     begin
                          WriteError( 'ERROR Al Leer Empresas Por ODBC', Error );
                          Result := False;
                     end;
               end;
               if Result then
               begin
                    with oeCompany do
                    begin
                         while not Eof do
                         begin
                              Result := ODBCOpenDB( hdbcEmpresa,
                                                    FieldByName( 'CM_NOMBRE' ).AsString,
                                                    FieldByName( 'CM_DATOS' ).AsString,
                                                    FieldByName( 'CM_USRNAME' ).AsString,
                                                    ZetaServerTools.Decrypt( FieldByName( 'CM_PASSWRD' ).AsString ) ) and Result;
                              Next;
                         end;
                    end;
               end;
          end;
          if Result then
             WriteLog( '** Acceso A Bases De Datos Por ODBC Tuvo Exito **' );
     end
     else
         Result := True;
end;

procedure TDBChecker.OKClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        CleanResultados;
        FError := False;
        TestDirecto;
        TestODBC;
        if FError then
           ZetaDialogo.zError( '� Hay Problemas !', 'Hay Problemas En El Acceso A Las Bases De Datos', 0 )
        else
        begin
             ZetaDialogo.zInformation( '� Exito !', 'El Acceso A Las Bases De Datos Tuvo Exito', 0 );
             Cancelar.SetFocus;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TDBChecker.AutoconfigurarClick(Sender: TObject);
var
   oCursor: TCursor;
   FManager: TZetaAliasManager;
   sEmpresa, sAlias, sDatabase: String;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     FManager := TZetaAliasManager.Create;
     try
        CleanResultados;
        FError := False;
        try
           with Registry do
           begin
                WriteLog( '------- Configurando Comparte ------- ' );
                Database := FManager.GetServerName( AliasComparte );
                WriteLog( Format( 'Alias "%s" = %s', [ AliasComparte, Database ] ) );
                WriteLog( '------- Configurando Empresas ------- ' );
                if DirectOpenDB( dbComparte, 'Comparte', Database, UserName, Password ) then
                begin
                     WriteLog( 'Comparte Fu� Abierta' );
                     try
                        with tqQuery do
                        begin
                             Active := False;
                             with SQL do
                             begin
                                  Clear;
                                  Add( SQL_DATABASE );
                             end;
                        end;
                        with tqCompany do
                        begin
                             Active := False;
                             with SQL do
                             begin
                                  Clear;
                                  Add( SQL_COMPANYS );
                             end;
                             Active := True;
                             WriteLog( 'Empresas Fueron Le�das' );
                             while not Eof do
                             begin
                                  sEmpresa := FieldByName( 'CM_CODIGO' ).AsString;
                                  sAlias := FieldByName( 'CM_ALIAS' ).AsString;
                                  sDatabase := '';
                                  if ( sAlias = '' ) then
                                     WriteLog( Format( 'Empresa %s: Alias NO ESPECIFICADO', [ sEmpresa ] ) )
                                  else
                                  begin
                                       try
                                          sDatabase := FManager.GetServerName( sAlias );
                                       except
                                             on Error: Exception do
                                             begin
                                                  WriteError( Format( 'Empresa %s: Alias "%s" Inexistente � Inv�lido', [ sEmpresa, sAlias ] ), Error );
                                             end;
                                       end;
                                  end;
                                  if ( sDatabase <> '' ) then
                                  begin
                                       with IB_Tx_Comparte do
                                       begin
                                            if not InTransaction then
                                               StartTransaction;
                                       end;
                                       try
                                          with tqQuery do
                                          begin
                                               ParamByName( 'CM_CODIGO' ).AsString := sEmpresa;
                                               ParamByName( 'CM_DATOS' ).AsString := sDatabase;
                                               ExecSQL;
                                          end;
                                          IB_Tx_Comparte.Commit;
                                          WriteLog( Format( 'Empresa %s: Alias "%s" = %s', [ sEmpresa, sAlias, sDatabase ] ) );
                                          FRefrescarShell := TRUE;             // Se va a refrescar el Shell
                                       except
                                             on Error: Exception do
                                             begin
                                                  IB_Tx_Comparte.Rollback;
                                                  WriteError( Format( 'ERROR Al Configurar %s', [ sEmpresa ] ), Error );
                                             end;
                                       end;
                                  end;
                                  Next;
                             end;
                             Active := False;
                        end;
                     except
                           on Error: Exception do
                           begin
                                WriteError( 'ERROR Al Leer Empresas', Error );
                           end;
                     end;
                end;
           end;
        except
              on Error: Exception do
              begin
                   WriteError( 'Error Al Autoconfigurar', Error );
              end;
        end;
        if FError then
           ZetaDialogo.zError( '� Hay Problemas !', 'Hay Problemas Al AutoConfigurar Acceso A Las Bases De Datos', 0 )
        else
            ZetaDialogo.zInformation( '� Exito !', 'El Acceso A Las Bases De Datos Fu� Configurado', 0 );
     finally
            FManager.Free;
            Screen.Cursor := oCursor;
     end;
end;

end.
