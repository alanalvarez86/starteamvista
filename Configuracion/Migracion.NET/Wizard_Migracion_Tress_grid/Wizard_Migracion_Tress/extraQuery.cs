﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wizard_Migracion_Tress
{
    class extraQuery //clase para guardar los datos dentro de un diccionario
    {
        public string Nombre { get; set; }
        public string Columnas {get;set; }
        public string Condiciones {get; set; }
        public string posicion { get; set; }
        public string tipo { get; set; }
        public bool Migrar { get; set; }
        public string query { get; set; }
        public string fbrows { get; set; }
    }

    class diccionario //diccionario de datos de condiciones y tablas 
    {
        public Dictionary<string,extraQuery> elemento = new Dictionary<string, extraQuery>(); //diccionario
        
        public void Agregar(string tabla,string columnas,string condicion) // agregaelemento al diccionario
        {
        AddToDictionary(elemento,tabla, columnas, condicion);
        }
        public void Agregar(bool omitir, string posicion, string tipo, string tabla, string Columnas, string Condicion, string Query)
        {
            AddToDictionary(elemento, omitir, posicion, tipo, tabla, Columnas, Condicion, Query);
        }
        public void Agregar(bool omitir, string posicion, string tipo, string tabla, string Columnas, string Condicion, string Query,string rows)
        {
            AddToDictionary(elemento, omitir, posicion, tipo, tabla, Columnas, Condicion, Query, rows);
        }
        private void AddToDictionary(Dictionary<string, extraQuery> Querys,string Tabla, string Columnas, string Condiciones) //se asignan los elementos
        {
            extraQuery Query = new extraQuery();

            Query.Nombre = Tabla;
            Query.Columnas = Columnas;
            Query.Condiciones = Condiciones;
            Querys.Add(key: Query.Nombre, value: Query);
        }
        private void AddToDictionary(Dictionary<string, extraQuery> Querys,bool omitir,string posicion, string tipo,   string Tabla, string Columnas, string Condiciones,string query) //se asignan los elementos
        {
            extraQuery Query = new extraQuery();

            Query.Nombre = Tabla;
            Query.Columnas = Columnas;
            Query.Condiciones = Condiciones;
            Query.Migrar = omitir;
            Query.posicion = posicion;
            Query.query=query;
            Query.tipo = tipo;
            Querys.Add(key: Query.Nombre, value: Query);
        }
        private void AddToDictionary(Dictionary<string, extraQuery> Querys,bool omitir,string posicion, string tipo,   string Tabla, string Columnas, string Condiciones,string query,string rows) //se asignan los elementos
        {
            extraQuery Query = new extraQuery();

            Query.Nombre = Tabla;
            Query.Columnas = Columnas;
            Query.Condiciones = Condiciones;
            Query.Migrar = omitir;
            Query.posicion = posicion;
            Query.query=query;
            Query.tipo = tipo;
            Query.fbrows = rows;
            Querys.Add(key: Query.Nombre, value: Query);
        }

        public  string encuentracondiciones(string tabla)//encuentra las condiciones de la tabla
        {
            string condiciones;

            if (elemento.ContainsKey(tabla) == false)
            {
                condiciones = ""; // devuelve cadena vacia si no hay tabla registrada en el diccionario
            }
            else
            {
                extraQuery encontrado = elemento[tabla];
                condiciones = encontrado.Condiciones;
            }
            return condiciones;
        }
        public string encuentracolumnas(string tabla)//encuentra las columnas de la tabla
        {
            string columnas;
            if (elemento.ContainsKey(tabla) == false)
            {
                columnas = "*"; //devuelve "todos" si no encuentra columnas 
            }
            else
            {
                extraQuery encontrado = elemento[tabla];
                columnas = encontrado.Condiciones;
            }
            return columnas;
        
        }
       
    }
}
