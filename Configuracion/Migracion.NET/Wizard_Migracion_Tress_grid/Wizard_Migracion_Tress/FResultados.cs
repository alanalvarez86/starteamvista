﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;

namespace Wizard_Migracion_Tress
{
    public partial class FResultados : Form
    {
        public FResultados()
        {
            InitializeComponent();
        }
        public string bitacora;
        public string detalle;
        public string conexionquery;
        private void button4_Click_1(object sender, EventArgs e)
        {
            Process p = new Process();
            p.StartInfo.FileName = bitacora;
            p.StartInfo.Arguments = "ProcessStart.cs";
            try
            {
                p.Start();
            }
            catch
            {
                MessageBox.Show(this, "Bitacora No localizada, Asegurate que se guardo y que no ha sido movida a otra hubicacion", "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Question);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Process p = new Process();
            p.StartInfo.FileName = detalle;
            p.StartInfo.Arguments = "ProcessStart.cs";
            try
            {
                p.Start();
            }
            catch
            {
                MessageBox.Show("Detalle No localizado, Asegurate que se guardo y que no ha sido movido a otra hubicacion");
            }
        }

        private void btnReportes_Click(object sender, EventArgs e)
        {
            
            
            saveFileDialog1.ShowDialog();
        }
        public void creareporte(SqlCommand query, string rutareporte)
        {
           while(query.Connection.State == ConnectionState.Closed)
           {
            query.Connection.Open();
           }
            SqlDataReader lector = query.ExecuteReader();
            using (StreamWriter outfile = new StreamWriter(rutareporte, true))
            {
                while (lector.Read())
                {

                    for (int i = 0; i < lector.FieldCount; i++)
                    {
                        outfile.Write(lector[i].ToString() + ",");
                    }
                    outfile.Write(Environment.NewLine);
                }
            }
            query.Connection.Close();
        }
        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            string filename = "";
             filename = saveFileDialog1.FileName;
            SqlConnection conexion = new SqlConnection(conexionquery);
            SqlCommand query = new SqlCommand();
            query.Connection = conexion;
            
            query.CommandText = @"select R.RE_CODIGO, R.RE_NOMBRE from REPORTE R
where ( ( select count(*) from CAMPOREP C where C.RE_CODIGO = R.RE_CODIGO and UPPER(C.CR_FORMULA) like '%SELECT%' ) > 0 )
";

            using (StreamWriter outfile = new StreamWriter(filename, false))
            {
                outfile.Write("Reportes con Formulas Diferente Sintaxis" + Environment.NewLine + Environment.NewLine);
            }
            creareporte(query,filename);
            query.CommandText = @"select CO_NUMERO, CO_DESCRIP from CONCEPTO
where ( UPPER(CO_FORMULA) like '%SELECT%' )
";


            using (StreamWriter outfile = new StreamWriter(filename, true))
            {
                outfile.Write(Environment.NewLine + Environment.NewLine + "Conceptos con Formulas Diferente Sintaxis" + Environment.NewLine);
            }
            creareporte(query,filename);
            query.CommandText = @"select NP_FOLIO, NP_NOMBRE from NOMPARAM
where ( UPPER( cast(NP_FORMULA as varchar(255) ) ) like '%SELECT%' )
";

            using (StreamWriter outfile = new StreamWriter(filename, true))
            {
                outfile.Write(Environment.NewLine + Environment.NewLine + "Parametros con Formulas Diferente Sintaxis" + Environment.NewLine);
            }
            creareporte(query,filename);
            Process p = new Process();
            p.StartInfo.FileName = filename;
            p.StartInfo.Arguments = "ProcessStart.cs";
            p.Start();

        
        }
    }
}
