﻿namespace Wizard_Migracion_Tress
{
    partial class FPrepara
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FPrepara));
            this.GridPreparar = new System.Windows.Forms.DataGridView();
            this.Omitir = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Posicion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tipo = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Campos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Condicion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Query = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RowsFB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RowsSql = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.LblTstimado = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.BtnVolver = new System.Windows.Forms.Button();
            this.BtnCalcula = new System.Windows.Forms.Button();
            this.BtnMigra = new System.Windows.Forms.Button();
            this.BtnOrdena = new System.Windows.Forms.Button();
            this.BtnCarga = new System.Windows.Forms.Button();
            this.BtnGuarda = new System.Windows.Forms.Button();
            this.guardarBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.MarcMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.marcarTodos = new System.Windows.Forms.ToolStripMenuItem();
            this.desmarcartodos = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.marcar1 = new System.Windows.Forms.ToolStripMenuItem();
            this.demarcar = new System.Windows.Forms.ToolStripMenuItem();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.GridPreparar)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.MarcMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // GridPreparar
            // 
            this.GridPreparar.AllowDrop = true;
            this.GridPreparar.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.GridPreparar.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.GridPreparar.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridPreparar.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.GridPreparar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GridPreparar.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Omitir,
            this.Posicion,
            this.Tipo,
            this.Nombre,
            this.Campos,
            this.Condicion,
            this.Query,
            this.RowsFB,
            this.RowsSql});
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.GridPreparar.DefaultCellStyle = dataGridViewCellStyle10;
            this.GridPreparar.Location = new System.Drawing.Point(12, 85);
            this.GridPreparar.Name = "GridPreparar";
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridPreparar.RowHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.GridPreparar.RowHeadersVisible = false;
            this.GridPreparar.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridPreparar.Size = new System.Drawing.Size(757, 342);
            this.GridPreparar.TabIndex = 45;
            this.GridPreparar.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.GridPreparar_CellBeginEdit);
            this.GridPreparar.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridPreparar_CellClick);
            this.GridPreparar.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewPreparacion_CellContentClick);
            this.GridPreparar.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DGVPreparar_RowHeaderMouseDoubleClick);
            this.GridPreparar.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridPreparar_CellValueChanged);
            this.GridPreparar.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DGVPreparar_RowHeaderMouseDoubleClick);
            this.GridPreparar.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.DGVPreparar_RowsAdded);
            this.GridPreparar.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.DGVPreparar_UserDeletedRow);
            this.GridPreparar.Click += new System.EventHandler(this.GridPreparar_Click);
            this.GridPreparar.DragDrop += new System.Windows.Forms.DragEventHandler(this.DGVPreparar_DragDrop);
            this.GridPreparar.DragOver += new System.Windows.Forms.DragEventHandler(this.DGVPreparar_DragOver);
            this.GridPreparar.MouseClick += new System.Windows.Forms.MouseEventHandler(this.GridPreparar_MouseClick);
            this.GridPreparar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DGVPreparar_MouseDown);
            this.GridPreparar.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DGVPreparar_Menupopup);
            // 
            // Omitir
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle2.NullValue = false;
            this.Omitir.DefaultCellStyle = dataGridViewCellStyle2;
            this.Omitir.HeaderText = "Transferir";
            this.Omitir.Name = "Omitir";
            this.Omitir.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Posicion
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N0";
            dataGridViewCellStyle3.NullValue = null;
            this.Posicion.DefaultCellStyle = dataGridViewCellStyle3;
            this.Posicion.HeaderText = "Posición";
            this.Posicion.Name = "Posicion";
            this.Posicion.ReadOnly = true;
            // 
            // Tipo
            // 
            this.Tipo.HeaderText = "Tipo de Instrucción";
            this.Tipo.Items.AddRange(new object[] {
            "Tabla",
            "Proceso",
            "Query"});
            this.Tipo.Name = "Tipo";
            this.Tipo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Nombre
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Nombre.DefaultCellStyle = dataGridViewCellStyle4;
            this.Nombre.HeaderText = "Nombre";
            this.Nombre.Name = "Nombre";
            this.Nombre.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Campos
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Campos.DefaultCellStyle = dataGridViewCellStyle5;
            this.Campos.HeaderText = "Campos";
            this.Campos.Name = "Campos";
            this.Campos.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Condicion
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Condicion.DefaultCellStyle = dataGridViewCellStyle6;
            this.Condicion.HeaderText = "Condición";
            this.Condicion.Name = "Condicion";
            this.Condicion.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Query
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Query.DefaultCellStyle = dataGridViewCellStyle7;
            this.Query.HeaderText = "Instrucción ";
            this.Query.Name = "Query";
            // 
            // RowsFB
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N0";
            dataGridViewCellStyle8.NullValue = null;
            this.RowsFB.DefaultCellStyle = dataGridViewCellStyle8;
            this.RowsFB.HeaderText = "Registros(FB)";
            this.RowsFB.Name = "RowsFB";
            this.RowsFB.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // RowsSql
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.Format = "N0";
            dataGridViewCellStyle9.NullValue = null;
            this.RowsSql.DefaultCellStyle = dataGridViewCellStyle9;
            this.RowsSql.HeaderText = "Registros(MSSQL)";
            this.RowsSql.Name = "RowsSql";
            this.RowsSql.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // checkBox1
            // 
            this.checkBox1.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.checkBox1.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.Location = new System.Drawing.Point(112, 62);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(17, 20);
            this.checkBox1.TabIndex = 52;
            this.checkBox1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // LblTstimado
            // 
            this.LblTstimado.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LblTstimado.Location = new System.Drawing.Point(444, 489);
            this.LblTstimado.Name = "LblTstimado";
            this.LblTstimado.Size = new System.Drawing.Size(399, 24);
            this.LblTstimado.TabIndex = 54;
            this.LblTstimado.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LblTstimado.Click += new System.EventHandler(this.LblTstimado_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel1.Controls.Add(this.BtnVolver);
            this.panel1.Controls.Add(this.BtnCalcula);
            this.panel1.Controls.Add(this.BtnMigra);
            this.panel1.Controls.Add(this.BtnOrdena);
            this.panel1.Controls.Add(this.BtnCarga);
            this.panel1.Controls.Add(this.BtnGuarda);
            this.panel1.Location = new System.Drawing.Point(0, 516);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(901, 46);
            this.panel1.TabIndex = 56;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // BtnVolver
            // 
            this.BtnVolver.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnVolver.Image = global::Wizard_Migracion_Tress.Properties.Resources.Back;
            this.BtnVolver.Location = new System.Drawing.Point(586, 0);
            this.BtnVolver.Name = "BtnVolver";
            this.BtnVolver.Size = new System.Drawing.Size(133, 42);
            this.BtnVolver.TabIndex = 60;
            this.BtnVolver.Text = "Volver";
            this.BtnVolver.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.BtnVolver, "Volver");
            this.BtnVolver.UseVisualStyleBackColor = true;
            this.BtnVolver.Click += new System.EventHandler(this.button2_Click);
            // 
            // BtnCalcula
            // 
            this.BtnCalcula.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCalcula.Image = global::Wizard_Migracion_Tress.Properties.Resources.Extract_object_icon;
            this.BtnCalcula.Location = new System.Drawing.Point(419, 0);
            this.BtnCalcula.Name = "BtnCalcula";
            this.BtnCalcula.Size = new System.Drawing.Size(133, 42);
            this.BtnCalcula.TabIndex = 59;
            this.BtnCalcula.Text = "Calcular Registros";
            this.BtnCalcula.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.BtnCalcula, "Calculo de Registros a Transferir Por tabla y tiempo estimado de Transferencia");
            this.BtnCalcula.UseVisualStyleBackColor = true;
            this.BtnCalcula.Click += new System.EventHandler(this.Calcular_filas_Click);
            // 
            // BtnMigra
            // 
            this.BtnMigra.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnMigra.Image = global::Wizard_Migracion_Tress.Properties.Resources.data_transfer_icon5;
            this.BtnMigra.Location = new System.Drawing.Point(725, 1);
            this.BtnMigra.Name = "BtnMigra";
            this.BtnMigra.Size = new System.Drawing.Size(133, 42);
            this.BtnMigra.TabIndex = 58;
            this.BtnMigra.Text = "Iniciar";
            this.BtnMigra.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.BtnMigra, "Iniciar Transferencia");
            this.BtnMigra.UseVisualStyleBackColor = true;
            this.BtnMigra.Click += new System.EventHandler(this.Migrar_click);
            // 
            // BtnOrdena
            // 
            this.BtnOrdena.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnOrdena.Image = global::Wizard_Migracion_Tress.Properties.Resources.gtk_sort_descending2;
            this.BtnOrdena.Location = new System.Drawing.Point(14, 0);
            this.BtnOrdena.Name = "BtnOrdena";
            this.BtnOrdena.Size = new System.Drawing.Size(133, 42);
            this.BtnOrdena.TabIndex = 56;
            this.BtnOrdena.Text = "Ordenar Tablas";
            this.BtnOrdena.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.BtnOrdena, "Ordenar tablas por orden de Transferencia");
            this.BtnOrdena.UseVisualStyleBackColor = true;
            this.BtnOrdena.Click += new System.EventHandler(this.Ordenarbtn_Click);
            // 
            // BtnCarga
            // 
            this.BtnCarga.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCarga.Image = global::Wizard_Migracion_Tress.Properties.Resources.Stock_Index_Up;
            this.BtnCarga.Location = new System.Drawing.Point(284, 0);
            this.BtnCarga.Name = "BtnCarga";
            this.BtnCarga.Size = new System.Drawing.Size(133, 42);
            this.BtnCarga.TabIndex = 55;
            this.BtnCarga.Text = "Cargar Estructura";
            this.BtnCarga.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.BtnCarga, "Cargar estructura desde Archivo(xml)");
            this.BtnCarga.UseVisualStyleBackColor = true;
            this.BtnCarga.Click += new System.EventHandler(this.carga_estructura_click);
            // 
            // BtnGuarda
            // 
            this.BtnGuarda.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnGuarda.Image = global::Wizard_Migracion_Tress.Properties.Resources.Save_icon;
            this.BtnGuarda.Location = new System.Drawing.Point(149, 0);
            this.BtnGuarda.Name = "BtnGuarda";
            this.BtnGuarda.Size = new System.Drawing.Size(133, 42);
            this.BtnGuarda.TabIndex = 54;
            this.BtnGuarda.Text = "Guardar Estructura";
            this.BtnGuarda.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.BtnGuarda, "Guardar Estructura a Archivo(xml)");
            this.BtnGuarda.UseVisualStyleBackColor = true;
            this.BtnGuarda.Click += new System.EventHandler(this.guarda_estructura_click);
            // 
            // guardarBtn
            // 
            this.guardarBtn.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guardarBtn.Location = new System.Drawing.Point(808, 442);
            this.guardarBtn.Name = "guardarBtn";
            this.guardarBtn.Size = new System.Drawing.Size(72, 44);
            this.guardarBtn.TabIndex = 57;
            this.guardarBtn.Text = "Guardar Orden";
            this.guardarBtn.UseVisualStyleBackColor = true;
            this.guardarBtn.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 57;
            this.label1.Text = "Transferir Todo";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(901, 46);
            this.panel2.TabIndex = 58;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(30, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(308, 29);
            this.label2.TabIndex = 58;
            this.label2.Text = "5.   Orden de Transferencia";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "Estructura.XML";
            this.openFileDialog1.Filter = "Archivo XML(*.XML)|*.XML";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.FileName = "Estructura.XML";
            this.saveFileDialog1.Filter = "Archivo XML(*.XML)|*.XML";
            // 
            // MarcMenuStrip
            // 
            this.MarcMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.marcarTodos,
            this.desmarcartodos,
            this.toolStripSeparator1,
            this.marcar1,
            this.demarcar});
            this.MarcMenuStrip.Name = "contextMenuStrip1";
            this.MarcMenuStrip.Size = new System.Drawing.Size(223, 98);
            this.MarcMenuStrip.Text = "Marcar/Desmarcar";
            // 
            // marcarTodos
            // 
            this.marcarTodos.Name = "marcarTodos";
            this.marcarTodos.Size = new System.Drawing.Size(222, 22);
            this.marcarTodos.Text = "Marcar Todo Hasta Aqui";
            this.marcarTodos.Click += new System.EventHandler(this.marcarTodos_Click);
            // 
            // desmarcartodos
            // 
            this.desmarcartodos.Name = "desmarcartodos";
            this.desmarcartodos.Size = new System.Drawing.Size(222, 22);
            this.desmarcartodos.Text = "Desmarcar Todo Hasta Aqui";
            this.desmarcartodos.Click += new System.EventHandler(this.desmarcartodos_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(219, 6);
            // 
            // marcar1
            // 
            this.marcar1.Name = "marcar1";
            this.marcar1.Size = new System.Drawing.Size(222, 22);
            this.marcar1.Text = "Marcar 1";
            this.marcar1.Click += new System.EventHandler(this.marcar1_Click);
            // 
            // demarcar
            // 
            this.demarcar.Name = "demarcar";
            this.demarcar.Size = new System.Drawing.Size(222, 22);
            this.demarcar.Text = "Desmarcar 1";
            this.demarcar.Click += new System.EventHandler(this.demarcar_Click);
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(655, 53);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(136, 23);
            this.textBox1.TabIndex = 59;
            this.toolTip1.SetToolTip(this.textBox1, "Buscar Nombre de Tabla");
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
            this.textBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(594, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 18);
            this.label3.TabIndex = 60;
            this.label3.Text = "Buscar :";
            // 
            // FPrepara
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(857, 502);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.guardarBtn);
            this.Controls.Add(this.LblTstimado);
            this.Controls.Add(this.GridPreparar);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(873, 540);
            this.Name = "FPrepara";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Transferencia de Datos";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FPrepara_FormClosing);
            this.Load += new System.EventHandler(this.FPrepara_Load);
            this.Shown += new System.EventHandler(this.FPrepara_Shown);
            this.SizeChanged += new System.EventHandler(this.FPrepara_SizeChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FPrepara_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.GridPreparar)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.MarcMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.DataGridView GridPreparar;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label LblTstimado;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button BtnVolver;
        private System.Windows.Forms.Button BtnCalcula;
        private System.Windows.Forms.Button BtnMigra;
        private System.Windows.Forms.Button guardarBtn;
        private System.Windows.Forms.Button BtnOrdena;
        private System.Windows.Forms.Button BtnCarga;
        private System.Windows.Forms.Button BtnGuarda;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ContextMenuStrip MarcMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem marcarTodos;
        private System.Windows.Forms.ToolStripMenuItem desmarcartodos;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem marcar1;
        private System.Windows.Forms.ToolStripMenuItem demarcar;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Omitir;
        private System.Windows.Forms.DataGridViewTextBoxColumn Posicion;
        private System.Windows.Forms.DataGridViewComboBoxColumn Tipo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn Campos;
        private System.Windows.Forms.DataGridViewTextBoxColumn Condicion;
        private System.Windows.Forms.DataGridViewTextBoxColumn Query;
        private System.Windows.Forms.DataGridViewTextBoxColumn RowsFB;
        private System.Windows.Forms.DataGridViewTextBoxColumn RowsSql;
    }
}