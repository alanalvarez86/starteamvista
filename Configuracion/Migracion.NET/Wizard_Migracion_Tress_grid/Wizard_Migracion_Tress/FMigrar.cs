﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FirebirdSql.Data.Client;
using FirebirdSql.Data.FirebirdClient;
using System.Threading;
using System.Diagnostics;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Xml.Linq;
using System.Xml;
namespace Wizard_Migracion_Tress
{
    public partial class FMigrar : Form
    {
        public FMigrar()
        {
            InitializeComponent();

        }


        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }
        bool filled = false;
        Thread HiloMigracion;
        double filastotales;
        DataTable dt = new DataTable();
        public Thread gridloader;
        DataGridView grid;
        public Point punto = new Point();

        private void Form1_Shown(object sender, EventArgs e)
        {
            if (Program.MigrarComparte)
            {
                BtnTodo.Visible = false;
                BtResumen.Visible = false;
            }
            dataGridViewButtonColumn1.Visible = !Program.MigrarComparte;
            loadbounds();
            this.WindowState = FormWindowState.Maximized;
            gridloader = new Thread(() => loadgrid());
            gridloader.Start();

            this.Cursor = Cursors.WaitCursor;
            /*
            FbConnection conexionfb = new FbConnection(Program.Prepara.conexionstringfb);
            FbCommand queryfb = new FbCommand();
            queryfb.Connection = conexionfb;
            
            extraQuery[] ContenidoDiccionario = Program.Migrar.DiccionarioMigracion.elemento.Values.ToArray<extraQuery>();
            dGridMigracion.Rows.Add();
            dGridMigracion[0, 0].Value = 0;
            dGridMigracion[1, 0].Value = "Preparando Tablas";
            dGridMigracion[8, 0].Value = playimage;
          
            for (int i = 0; i < ContenidoDiccionario.Length; i++)
            {
                queryfb.Connection.Open();
                int j = i + 1;
                dGridMigracion.Rows.Add();
                dGridMigracion[0, j].Value = dGridMigracion.RowCount - 1;
                dGridMigracion[1, j].Value = ContenidoDiccionario[i].Nombre;
                if (ContenidoDiccionario[i].Migrar == true)
                {
                    if (ContenidoDiccionario[i].tipo == "Tabla" || ContenidoDiccionario[i].Nombre=="TipoKardex")
                    {
                        queryfb.CommandText = "select count(*) from " + ContenidoDiccionario[i].Nombre + " " + ContenidoDiccionario[i].Condiciones;
                        if (ContenidoDiccionario[i].Nombre == "TipoKardex")
                        {
                            queryfb.CommandText = "select count(Distinct(CB_TIPO)) from Kardex where CB_TIPO not in (select tb_Codigo from TKARDEX)";
                        }


                        try
                        {
                            dGridMigracion[2, j].Value = queryfb.ExecuteScalar();
                             queryfb.CommandText="select count(rdb$field_name) from rdb$relation_fields where rdb$relation_name='"+ContenidoDiccionario[i].Nombre+"'";
                           
                            filastotales = filastotales + double.Parse(dGridMigracion[2, j].Value.ToString());
                            queryfb.Connection.Close();
                        }
                        catch
                        {
                            dGridMigracion[2, j].Value = 0;
                            queryfb.Connection.Close();
                        }
                    }
                    else
                    {
                        if (ContenidoDiccionario[i].tipo == "Proceso")
                        {
                            if (ContenidoDiccionario[i].Nombre == "TipoKardex")
                            {
                                queryfb.CommandText = "select count(Distinct(CB_TIPO)) from Kardex where CB_TIPO not in (select tb_Codigo from TKARDEX)";
                                try
                                {
                                    dGridMigracion[2, j].Value = queryfb.ExecuteScalar();
                                   
                                }
                                catch
                                {
                                    dGridMigracion[2, j].Value = 0;
                                }
                            }
                        }
                    }
                }
                else
                {
                    dGridMigracion.Rows[j].DefaultCellStyle.BackColor = Color.LightGray;
                }
            }
            
            finalizogrid = true;
           
            */
            this.Cursor = Cursors.Default;
        }
        public string nombretabla;
        public void loadgrid()
        {
            string tabla = "";
            try
            {
                grid = new DataGridView();
                foreach (DataGridViewColumn col in dGridMigracion.Columns)
                {
                    grid.Columns.Add((DataGridViewColumn)col.Clone());
                }
                FbConnection conexionfb = new FbConnection(Program.Prepara.conexionstringfb);
                FbCommand queryfb = new FbCommand();
                queryfb.Connection = conexionfb;
                FbDataReader reader;
                extraQuery[] ContenidoDiccionario = Program.Migrar.DiccionarioMigracion.elemento.Values.ToArray<extraQuery>();
                grid.Rows.Clear();
                grid.Rows.Add();
                grid[0, 0].Value = 0;
                grid[1, 0].Value = "Preparando Tablas";
                grid[8, 0].Value = playimage;

                for (int i = 0; i < ContenidoDiccionario.Length; i++)
                {
                    while (conexionfb.State == ConnectionState.Closed)
                    {
                        try
                        {
                            queryfb.Connection.Open();
                        }
                        catch { }
                    }
                    int j = i + 1;
                    grid.Rows.Add();
                    grid[0, j].Value = j;
                    grid[1, j].Value = ContenidoDiccionario[i].Nombre;
                    nombretabla = ContenidoDiccionario[i].Nombre;
                    tabla = nombretabla+" Provoco el error";
                    if (ContenidoDiccionario[i].Migrar == true)
                    {
                        if (ContenidoDiccionario[i].tipo == "Tabla" || ContenidoDiccionario[i].tipo == "Proceso")
                        {
                            if (ContenidoDiccionario[i].fbrows == "" || ContenidoDiccionario[i].fbrows == null)
                            {
                                queryfb.CommandText = "select count(*) from " + ContenidoDiccionario[i].Nombre + " " + ContenidoDiccionario[i].Condiciones;
                                if (ContenidoDiccionario[i].Nombre == "TipoKardex")
                                {
                                    queryfb.CommandText = "select count(Distinct(CB_TIPO)) from Kardex where CB_TIPO not in (select tb_Codigo from TKARDEX)";
                                }
                                if (ContenidoDiccionario[i].Nombre == "TipoPrestamo")
                                {
                                    queryfb.CommandText = "select count(Distinct(PR_TIPO)) from Prestamo where PR_TIPO not in (select TB_CODIGO from TPRESTA)";
                                }
                                if (ContenidoDiccionario[i].Nombre == "BITACORA" || ContenidoDiccionario[i].Nombre == "AUSENCIA" || ContenidoDiccionario[i].Nombre == "MOVIMIEN" || ContenidoDiccionario[i].Nombre == "CHECADAS")
                                {

                                    Program.reiniciaServidor(ref conexionfb);
                                }
                                try
                                {
                                    int rows;

                                    reader = queryfb.ExecuteReader();
                                    reader.Read();

                                    rows = int.Parse(reader[0].ToString());


                                    grid[2, j].Value = rows;
                                    ContenidoDiccionario[i].fbrows = rows.ToString();
                                    // grid[2, j].Value = 11000000;
                                    filastotales = filastotales + double.Parse(grid[2, j].Value.ToString());

                                    reader.Close();
                                    if (ContenidoDiccionario[i].Nombre == "MOVIMIEN")
                                    {
                                        Program.reiniciaServidor(ref conexionfb);
                                    }
                                    conexionfb.Close();

                                }
                                catch
                                {
                                    grid[2, j].Value = 0;
                                    conexionfb.Close();
                                    if (ContenidoDiccionario[i].tipo == "Tabla")
                                    {
                                        ContenidoDiccionario[i].Migrar = false;
                                    }
                                }

                            }
                            else
                            {
                                grid[2, j].Value = int.Parse(ContenidoDiccionario[i].fbrows.ToString());
                                filastotales = filastotales + double.Parse(ContenidoDiccionario[i].fbrows.ToString());
                            }
                        }


                    }
                    else
                    {
                        grid.Rows[j].DefaultCellStyle.BackColor = Color.LightGray;
                    }

                    //   queryfb.Dispose();


                }
                // Thread.Sleep(60000);

                Program.reiniciaServidor(ref conexionfb);
                XElement tablas = new XElement("Transferencia");
                for (int i = 0; i < ContenidoDiccionario.Length; i++)
                {
                    tablas.Add(new XElement("Tabla",
                        new XElement("Omitir", ContenidoDiccionario[i].Migrar.ToString()),
                        new XElement("Posicion", double.Parse(ContenidoDiccionario[i].posicion)),
                        new XElement("Tipo", ContenidoDiccionario[i].tipo + ""),
                        new XElement("Nombre", ContenidoDiccionario[i].Nombre + ""),
                        new XElement("Campos", ContenidoDiccionario[i].Columnas + ""),
                        new XElement("Condicion", ContenidoDiccionario[i].Condiciones + ""),
                        new XElement("Query", ContenidoDiccionario[i].query + ""),
                        new XElement("fbrows", ContenidoDiccionario[i].fbrows + "")));

                    ;
                }
                tablas.Save(Program.Prepara.nombrearchivo);
                finalizopregrid = true;

                conexionfb.Close();

            }
            catch(Exception error) {
                MessageBox.Show(error.ToString());
            }
        }
        bool finalizopregrid = false;

        public void loadMigracion()
        {
            Type officeType = Type.GetTypeFromProgID("Excel.Application");
            if (officeType == null)
            {
                existeexcel = false;
            }
            else
            {
                existeexcel = true;
            }
            extraQuery[] ContenidoDiccionario = Program.Migrar.DiccionarioMigracion.elemento.Values.ToArray<extraQuery>();
            dGridMigracion.Rows.Clear();
            foreach (DataGridViewRow row in grid.Rows)
            {
                if (row.Index < grid.RowCount - 1)
                {
                    dGridMigracion.Rows.Add(row.Cells[0].Value, row.Cells[1].Value, row.Cells[2].Value, row.Cells[3].Value, row.Cells[4].Value, row.Cells[5].Value, row.Cells[6].Value, row.Cells[7].Value);
                    if (row.Index > 0)
                    {
                        if (ContenidoDiccionario[row.Index - 1].Migrar == false)
                        {
                            dGridMigracion.Rows[row.Index].DefaultCellStyle.BackColor = Color.LightGray;
                        }
                    }
                }
            }

            grid = null;
            gridloader.Join();
            GC.Collect();
            GC.WaitForPendingFinalizers();

            HiloMigracion = new Thread(() => Program.Migrar.iterador());
            HiloMigracion.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            HiloMigracion.CurrentUICulture = new System.Globalization.CultureInfo("en-US");
            HiloMigracion.Start();
            progressBar1.Maximum = (int)filastotales;
            this.Cursor = Cursors.Default;
            finalizogrid = true;

        }
        public void showhelp()
        {
            if (tabResumen.SelectedIndex == 0)
            {
                System.Windows.Forms.Help.ShowHelp(this, @"Transferencia.chm", HelpNavigator.Topic, "proceso_de_Transferencia.htm");
            }
            else
            {
                System.Windows.Forms.Help.ShowHelp(this, @"Transferencia.chm", HelpNavigator.Topic, "proceso_de_Transferencia2.htm");
            }

        }
        private void Form1_Load(object sender, EventArgs e)
        {
           
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en-US");
            if (BtnTodo.Visible)
            {
                BtnTodo.Enabled = false;

            }
            BtnMigrarOtro.Enabled = false;
            loadbounds();
            Program.horaini = DateTime.Now;
            timer1.Start();
            reportnom = Program.reporte;
            saveFileDialog1.FileName = reportnom;
            saveFileDialog1.Filter = "Archivo de Texto(*.CSV)|*.CSV";
            saveFileDialog1.InitialDirectory = Path.GetDirectoryName(reportnom);
            foreach (DataGridViewColumn columna in dGridMigracion.Columns)
            {
                columna.DefaultCellStyle.Font = Program.Prepara.Font;
                columna.HeaderCell.Style.Font = Program.Prepara.Font;
                if (columna.Index != 1)
                {
                    columna.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                }
                columna.SortMode = DataGridViewColumnSortMode.NotSortable;

            }
            dGridMigracion.AutoGenerateColumns = false;
        }
        int i = 0;
        int errores;
        int exitos;
        int suma;
        int porciento;
        int total;
        int resto;

        Image playimage = global::Wizard_Migracion_Tress.Properties.Resources.play;
        Image exitoimage = global::Wizard_Migracion_Tress.Properties.Resources.Agt_Action_Success;
        Image waringinimage = global::Wizard_Migracion_Tress.Properties.Resources.Warning;
        Image errorimage = global::Wizard_Migracion_Tress.Properties.Resources.Cancel;
        int lasti = 0;

        double velocidad;
        double finalizar;
        string tiempoEstimado;
        double tiempo;

        int filasrecorridas;
        int advertencias;
        int sumerrores;
        bool finalizogrid = false;
        int filastemp = 0; int ttemp = 0;
        int veltemp = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            
            if (dGridMigracion.ColumnCount > 6)
            {
                if (dGridMigracion.RowCount > 0)
                {
                    dGridMigracion[6, 0].Value = Program.Migrar.borradas;
                }
            }
            tiempotranscurrido=(int)((DateTime.Now - Program.horaini).TotalSeconds *10);
            if (Program.Migrar.borrandotabla != "")
            {
                LblTrestanteCalc.Text = "Preparando Tabla " + Program.Migrar.borrandotabla;

            }
            if (Program.Migrar.pausa == true)
            {
                tiempo--;
                tiempotranscurrido--;
            }
            if (finalizogrid == true)
            {

                int limite = Program.Migrar.recorridoTablas;
                if (Program.Migrar.finalizoPreparacion == true)
                {
                    tiempo++;
                }
                if (Program.Migrar.finalizoPreparacion == true && tiempo == 0)
                {
                    tiempo++;
                }
                filasrecorridas = 0;
                sumerrores = 0;

                for (int i = 0; i <= Program.Migrar.recorridoTablas; i++)
                {

                    if (Program.Migrar.tablasrecorridas > 0 && i < Program.Migrar.tablaresulados.GetLength(0) && Program.Migrar.tablaresulados[i, 0] != null && Program.Migrar.tablaresulados[i, 1] != null)
                    {

                        filasrecorridas += int.Parse(Program.Migrar.tablaresulados[i, 0]) + int.Parse(Program.Migrar.tablaresulados[i, 1]);
                        sumerrores += int.Parse(Program.Migrar.tablaresulados[i, 1]);

                    }
                }
                if (Program.Migrar.finalizoPreparacion == true)
                {

                    for (i = i; i <= limite; i++)
                    {

                        if (lasti != i)
                        {
                            ttemp = 0;
                            if (i > 10)
                            {
                                dGridMigracion.FirstDisplayedScrollingRowIndex = (i + 1) - 10;
                            }
                        }
                        if (i + 1 < dGridMigracion.RowCount && dGridMigracion.Rows[i + 1].DefaultCellStyle.BackColor != Color.LightGray)
                        {

                            if (int.TryParse(dGridMigracion[2, i + 1].Value.ToString(), out total))
                            {

                                if (Program.Migrar.tablaresulados[i, 1] == null || Program.Migrar.tablaresulados[i, 1] == "x")
                                {
                                    errores = 0;
                                }
                                else
                                {
                                    errores = int.Parse(Program.Migrar.tablaresulados[i, 1]);
                                }
                                //  exitos = int.Parse(Program.Migrar.tablaresulados[i, 0]);
                                if (Program.Migrar.tablaresulados[i, 0] == null || Program.Migrar.tablaresulados[i, 0] == "x")
                                {
                                    exitos = 0;

                                }
                                else
                                {
                                    exitos = int.Parse(Program.Migrar.tablaresulados[i, 0]);

                                }

                                total = int.Parse(dGridMigracion[2, i + 1].Value.ToString());
                                if (total > 0)
                                {
                                    porciento = (int)(((double)(exitos) / (double)total) * 100);
                                }
                                else
                                {
                                    porciento = 100;
                                }
                                if (Program.Migrar.tablaresulados[i, 2] == null)
                                {
                                    advertencias = 0;
                                }
                                else
                                {
                                    advertencias = int.Parse(Program.Migrar.tablaresulados[i, 2].ToString());
                                }
                                suma = exitos + errores;
                                resto = total - suma;
                                dGridMigracion[3, i + 1].Value = exitos;
                                dGridMigracion[4, i + 1].Value = errores;
                                dGridMigracion[5, i + 1].Value = advertencias;
                                dGridMigracion[6, i + 1].Value = porciento;
                                dGridMigracion[7, i + 1].Value = resto;
                                filastemp = exitos + errores;
                                if (resto == 0)
                                {
                                    if (errores > 0)
                                    {
                                        dGridMigracion.Rows[i + 1].DefaultCellStyle.BackColor = Color.LightYellow;
                                        dGridMigracion[8, i + 1].Value = waringinimage;
                                        if (exitos == 0)
                                        {
                                            dGridMigracion.Rows[i + 1].DefaultCellStyle.BackColor = Color.Red;
                                            dGridMigracion[8, i + 1].Value = errorimage;
                                        }
                                    }
                                    else
                                    {
                                        dGridMigracion[8, i + 1].Value = exitoimage;
                                    }
                                }
                                else
                                {
                                    if (errores > 0)
                                    {
                                        dGridMigracion.Rows[i + 1].DefaultCellStyle.BackColor = Color.LightYellow;
                                        dGridMigracion[8, i + 1].Value = waringinimage;
                                    }
                                    else
                                    {
                                        dGridMigracion[8, i + 1].Value = playimage;
                                    }
                                }

                            }
                            else
                            {
                                if (i < Program.Migrar.recorridoTablas)
                                {
                                    dGridMigracion[2, i + 1].Value = 100;
                                    dGridMigracion[8, i + 1].Value = exitoimage;
                                }
                                else
                                {
                                    if (errores == 0)
                                    {
                                        dGridMigracion[8, i + 1].Value = playimage;

                                    }
                                    else
                                    {

                                    }
                                }
                            }

                        }
                        if (Program.Migrar.finalizoPreparacion == false)
                        {
                            i--;
                        }
                        if (tiempo > 0)
                        {

                            ttemp++;


                            veltemp = (filastemp * 10) / ttemp;



                            for (int x = 0; x < i; x++)
                            {

                            }
                            velocidad = ((filasrecorridas) / tiempo) * 10;
                            if (i < 180 && tiempo > 0 && veltemp > 0 && veltemp > 0)
                            {
                                try
                                {
                                    finalizar = ((int.Parse(dGridMigracion[2, i + 1].Value.ToString()) / veltemp) - (ttemp / 10)) + ((filastotales - filasrecorridas) / 4500);
                                }
                                catch { }
                            }
                            else
                            {
                                finalizar = (filastotales / velocidad) - (tiempo / 10);
                            }
                            tiempoEstimado = ((int)(finalizar / 3600)).ToString() + ":" + ((int)((int)((int)(finalizar) % 3600) / 60)).ToString("00") + ":" + (((int)finalizar) % 60).ToString("00"); ;
                            LblTrestanteCalc.Text = tiempoEstimado;
                        }

                    }
                    if (progressBar1.Maximum < filasrecorridas)
                    {
                        progressBar1.Value = progressBar1.Maximum;
                    }
                    else
                    {
                        progressBar1.Value = filasrecorridas;

                    }
                    double progreso = ((double)(progressBar1.Value) * 100) / progressBar1.Maximum;
                    lblProgresoP.Text = ((int)(progreso)).ToString() + "%";
                    double porcenerrores = (double)((((double)progressBar1.Value - sumerrores)) * 100) / progressBar1.Maximum;
                    if (sumerrores > 0 && porcenerrores == 100)
                    {
                        porcenerrores = 99.99;
                    }
                    LblTotMigradoP.Text = porcenerrores.ToString("00.00") + "%";
                    if (resto != 0)
                    {
                        i--;
                    }
                    lasti = i;
                }
                else
                {
                    dGridMigracion[8, 0].Value = exitoimage;

                }
                if (i == dGridMigracion.RowCount && Program.Migrar.finalizoProceso)
                {
                    timer1.Stop();
                    dt.Columns.Add("No.");

                    dt.Columns.Add("Nombre");
                    dt.Columns.Add("Registros A Transferir");
                    dt.Columns.Add("Registros Transferidos");
                    dt.Columns.Add("Registros Ignorados");
                    dt.Columns.Add("Advertencias");
                    dt.Columns.Add("Porcentaje Transferido");
                    dt.Columns[0].DataType = typeof(int);
                    dt.Columns[2].DataType = typeof(int);
                    dt.Columns[3].DataType = typeof(int);
                    dt.Columns[4].DataType = typeof(int);
                    dt.Columns[5].DataType = typeof(int);
                    dt.Columns[6].DataType = typeof(int);

                    foreach (DataGridViewRow row in dGridMigracion.Rows)
                    {
                        if (row.Index != 0 && row.DefaultCellStyle.BackColor != Color.LightGray)
                        {
                            DataRow dRow = dt.NewRow();
                            foreach (DataGridViewCell cell in row.Cells)
                            {

                                switch (cell.ColumnIndex)
                                {
                                    case 8:
                                        break;
                                    case 1:
                                        dRow[cell.ColumnIndex] = cell.Value;
                                        break;
                                    case 7:
                                        break;
                                    default:
                                        if (cell.Value != null)
                                        {
                                            dRow[cell.ColumnIndex] = int.Parse(cell.Value.ToString());
                                        }
                                        else
                                        {
                                            dRow[cell.ColumnIndex] = 0;
                                        }
                                        break;


                                }
                            }

                            dt.Rows.Add(dRow);
                        }
                    }

                    dGridResultados.DataSource = new DataView(dt, "", "", DataViewRowState.CurrentRows);
                    fillgridresumen();
                    if (!Program.MigrarComparte)
                    {
                    BtResumen.Enabled = true;
                    }
                        filtrar();
                    filled = true;
                    Application.DoEvents();
                    filtrar();
                    foreach (DataGridViewColumn columna in dGridMigracion.Columns)
                    {
                        columna.SortMode = DataGridViewColumnSortMode.Automatic;

                        if (columna.HeaderText.Contains("Procesado"))
                        {
                        }
                        else
                        {
                            columna.HeaderText = columna.HeaderText.Trim();
                        }
                    }
                    filtrarAdvertencias.Enabled = true;
                    FiltrarErrores.Enabled = true;
                    FiltrarMigradas.Enabled = true;
                    BtnMigrarOtro.Enabled = true;
                    if (BtnTodo.Visible)
                    {
                        BtnTodo.Enabled = true;
                    }
                    int val = 0;
                    int.TryParse(LblTotMigradoP.Text, out val);
                    StringBuilder messagemaker = new StringBuilder();
                    loadbounds();
                    BtnStop.Enabled = false;
                    BtnPausa.Enabled = false;
                    if (sumerrores == 0)
                    {

                        messagemaker.Append("El proceso de transferencia terminó con éxito." + Environment.NewLine + Environment.NewLine +
"Analice la información de las pestañas Resultados y Resumen, así como los diferentes reportes generados.");
                        MessageBox.Show(this, messagemaker.ToString(), "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    }
                    else
                    {
                        messagemaker.Append(" El proceso de transferencia ha terminado." + Environment.NewLine + Environment.NewLine +
"Analice la información de las pestañas Resultados y Resumen, así como los diferentes reportes generados.");
                        MessageBox.Show(this, messagemaker.ToString(), "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    }

                    tabResumen.SelectTab(1);
                }

            }
            else
            {
                LblTrestanteCalc.Text = "Calculando " + nombretabla;
                if (finalizopregrid == true)
                {
                    finalizopregrid = false;
                    loadMigracion();
                }
                tiempo++;
            }
            loadbounds();

            lbltTranscurridoN.Text = ((int)(tiempotranscurrido / (10) / 3600)).ToString() + ":" + ((int)((int)(((int)(tiempotranscurrido) / (10)) % 3600) / 60)).ToString("00") + ":" + (((int)tiempotranscurrido / 10) % 60).ToString("00");

            if (Program.Migrar.pausa == true && Program.Migrar.pausado == true)
            {
                if (mensajePausado == true)
                {
                    mensajePausado = false;
                    MessageBox.Show("Transferencia Pausada");

                } BtnPausa.Enabled = true;
            }
            else
            {
                if (Program.Migrar.pausa == true && Program.Migrar.pausado == false)
                {
                    if (mensajePausa == true)
                    {
                        mensajePausa = false;
                        mensajePausado = true;
                        MessageBox.Show("Esperando a llegar a punto de control para pausar la transferencia");
                    }
                    BtnPausa.Enabled = false;
                }
                else
                {

                }

            }
        }
        bool mensajePausado = false;
        bool existeexcel = false;
        bool mensajePausa = false;
        private void button1_Click(object sender, EventArgs e)
        {
            if (Program.Migrar.bitacorainicia == true)
            {
                if (existeexcel == true)
                {
                    var app = new Microsoft.Office.Interop.Excel.Application();
                    bool visible = true;
                    try
                    {
                        app.Workbooks.Open(Program.Principal.rutadetalle, ReadOnly: true);

                    }
                    catch
                    {
                        MessageBox.Show(this, "Detalle de bitácora no localizado, Asegurate que se guardo y que no ha sido movida a otra ubicacion", "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        app.Workbooks.Close();
                        app.Quit();
                        visible = false;
                    }
                    app.Visible = visible;

                }
                else
                {
                    try
                    {
                        Process p = Process.Start("notepad.exe", Program.Principal.rutadetalle);


                    }
                    catch
                    {
                        MessageBox.Show(this, "Detalle de bitácora no localizado, Asegurate que se guardo y que no ha sido movida a otra ubicación", "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }



                }
            }
        }
        private void fillgridresumen()
        {
            if (Program.MigrarComparte == false)
            {
                for (int i = 0; i < Program.Migrar.ResumenMigracion.GetLength(0); i++)
                {

                    dgridresumen2.Rows.Add();
                    for (int j = 0; j < Program.Migrar.ResumenMigracion.GetLength(1) - 1; j++)
                    {

                        dgridresumen2[j, i].Value = Program.Migrar.ResumenMigracion[i, j];
                    }
                    if (Program.Migrar.ResumenMigracion[i, 2].ToUpper().Contains("NO MOSTRAR"))
                    {

                        dgridresumen2.Rows[i].Cells[2] = new DataGridViewTextBoxCell();
                        dgridresumen2.Rows[i].Cells[2].Value = "No hay Reporte";
                    }
                    else
                    {
                        DataGridViewButtonCell btn = new DataGridViewButtonCell();
                        btn.Value = "Ver Reporte";

                        DataGridViewButtonCell btn2 = new DataGridViewButtonCell();
                        btn2.Value = "Ver Reporte";
                        dgridresumen2.Rows[i].Cells[2] = btn2;
                    }

                }


                foreach (DataGridViewColumn columna in dgridresumen2.Columns)
                {
                    columna.DefaultCellStyle.Font = Program.Prepara.Font;
                    columna.HeaderCell.Style.Font = Program.Prepara.Font;
                    columna.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                }
            }
            else
            {
                for (int i = 0; i < dGridMigracion.RowCount - 1; i++)
                {
                    if (dGridMigracion[Nombre.Index, i].Value.ToString() == "COMPANY" || dGridMigracion[Nombre.Index, i].Value.ToString() == "GRUPO" || dGridMigracion[Nombre.Index, i].Value.ToString() == "USUARIO")
                    {
                        if ((int.Parse(dGridMigracion[FilasMigrar.Index, i].Value.ToString())) - int.Parse(dGridMigracion[FilasMigradas.Index, i].Value.ToString()) ==0)
                        {
                            switch ((dGridMigracion[Nombre.Index, i].Value.ToString()))
                            {
                                case "COMPANY":
                                    dgridresumen2.Rows.Add("Registros de compañias", "Todos se Transfirieron");
                                    break;
                                case "GRUPO":
                                    dgridresumen2.Rows.Add("Registros de grupos de usuarios", "Todos se Transfirieron");
                                    break;
                                case "USUARIO":
                                    dgridresumen2.Rows.Add("Registros de usuarios", "Todos se Transfirieron");
                                    break;

                            }
                        }
                        else
                        {
                            int faltantes =(int.Parse(dGridMigracion[FilasMigrar.Index, i].Value.ToString())) - int.Parse(dGridMigracion[FilasMigradas.Index, i].Value.ToString()); 
                            switch ((dGridMigracion[Nombre.Index, i].Value.ToString()))
                            {
                                case "COMPANY":
                                    dgridresumen2.Rows.Add("Registros de compañias", faltantes.ToString() + " registros faltantes");
                                    break;
                                case "GRUPO":
                                    dgridresumen2.Rows.Add("Registros de grupos de usuarios", faltantes.ToString() + " registros faltantes");
                                    break;
                                case "USUARIO":
                                    dgridresumen2.Rows.Add("Registros de usuarios", faltantes.ToString() + " registros faltantes");
                                    break;

                            }
                        }
                    }

                }
            
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            if (Program.Migrar.bitacorainicia == true)
            {
                if (existeexcel == true)
                {
                    Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
                    bool visible = true;


                    try
                    {
                        app.Workbooks.Open(Program.Prepara.rutabitacora, ReadOnly: true);
                    }


                    catch
                    {
                        MessageBox.Show(this, "Bitácora no localizada, Asegurate que se guardo y que no ha sido movida a otra ubicación", "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        app.Workbooks.Close();
                        app.Quit();
                        visible = false;
                    }
                    app.Visible = visible;
                }
                else
                {
                    try
                    {
                        Process p = Process.Start("notepad.exe", Program.Prepara.rutabitacora);
                    }
                    catch
                    {
                        MessageBox.Show(this, "Bitácora no localizada, Asegurate que se guardo y que no ha sido movida a otra ubicación", "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }


                }
            }

        }
        Image pausedimage = global::Wizard_Migracion_Tress.Properties.Resources.media_pause;
        private void button1_Click_1(object sender, EventArgs e)
        {

            if (Program.Migrar.pausa == false)
            {
                Program.Migrar.pausa = true;
                mensajePausa = true;
                BtnPausa.BackgroundImage = playimage;

            }
            else
            {
                Program.Migrar.pausa = false;
                BtnPausa.BackgroundImage = pausedimage;

            }


        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }
        bool volver = false;
        private void FMigrar_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (volver == false)
            {
                if (MessageBox.Show(this, "¿Seguro que deseas Salir?", "Transferencia de Datos", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    Environment.Exit(Environment.ExitCode);
                }
                else
                { e.Cancel = true; }
            }
            else
            {
                volver = false;
                if (MessageBox.Show(this, "¿Seguro que deseas terminar este proceso para iniciar otra transferencia?", "Transferencia de Datos", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    //Program.Principal.wizardPages1.SelectedIndex = 1;
                    while (Program.Principal.WizardPages1.SelectedIndex != 0)
                    {
                        Program.Principal.MoverHoja(-1);
                    }
                    Program.Principal.MoverHoja(+1);
                    Program.Principal.nuevo();
                    Program.Principal.Show();
                }
                else
                {
                    e.Cancel = true;
                }


            }
        }
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            filtrar();
        }

        int filtro = 0;

        public void filtrar()
        {
            string filtroE = "";
            string filtroM = "";
            string FiltroA = "";
            string or = "";
            if (FiltrarErrores.Checked == true)
            {
                filtroE = "[Registros Ignorados]>'0'";
                or = "or";
            }
            if (FiltrarMigradas.Checked == true)
            {
                filtroM = or + " [Registros Transferidos]<>'0'";
                or = "or";

            }
            if (filtrarAdvertencias.Checked == true)
            {
                FiltroA = or + " [Advertencias] <>'0'";
            }
            dGridResultados.DataSource = dt.DefaultView.RowFilter = (filtroE + filtroM + FiltroA);
            DataView view = new DataView(dt);
            view.RowFilter = filtroE + filtroM + FiltroA;
            dGridResultados.DataSource = view;


            foreach (DataGridViewColumn columna in dGridResultados.Columns)
            {

                columna.DefaultCellStyle.Font = Program.Prepara.Font;
                columna.HeaderCell.Style.Font = Program.Prepara.Font;
                columna.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;


                if (columna.Index == 6)
                {
                    columna.DefaultCellStyle.Format = "0\\%";
                }

                if (columna.Index != 1)
                {
                    columna.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                }
                if (columna.Index != 1 && columna.Index != 6)
                {
                    columna.DefaultCellStyle.Format = "N0";
                }

            }

        }
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            filtrar();
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            filtrar();
        }

        private void GridMigracion_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            if (e.Column.Index == 6)
            {

            }
            if (e.Column.Index == 8)
            {
                e.Column.CellTemplate = new DataGridViewImageCell();
            }
            if (e.Column.Index == 2)
            {

            }
            if (e.Column.Index == 3)
            {

            }
            if (e.Column.Index == 4)
            {

            }
            if (e.Column.Index == 5)
            {

            }
            if (e.Column.Index == 6)
            {

            }
            if (e.Column.Index == 7)
            {

            }
            if (e.Column.Index == 8)
            {

            } if (e.Column.Index == 9)
            {

            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            volver = true;
            this.Close();
        }
        int tiempotranscurrido = 0;
        public void loadbounds()
        {

            foreach (DataGridViewColumn columna in dgridresumen2.Columns)
            {
                columna.DefaultCellStyle.Font = Program.Prepara.Font;
                columna.HeaderCell.Style.Font = Program.Prepara.Font;
                columna.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }
            /* foreach (DataGridViewColumn column in dGridMigracion.Columns)
             {

                 width += column.Width;
             }
             p
             */
            panel4.Width = this.Width;

            tabResumen.Width = this.Width;
            tabResumen.Height = this.Height - 170;
            tabPage1.Height = this.Height - 170;
            tabPage1.Width = this.Width;
            tabPage2.Height = this.Height - 170;
            tabPage2.Width = this.Width;
            dGridMigracion.Left = 0;
            dGridMigracion.Height = tabPage1.Height - 41;
            dGridResultados.Left = 0;

            dGridResultados.Height = ((tabResumen.Height - 100));

            dGridMigracion.Width = this.ClientSize.Width - 7;
            dGridResultados.Width = this.ClientSize.Width - 7;
            // dGridResultados.Top = panel3.Bottom + 10;
            panel3.Left = (this.Width / 2) - (panel3.Width / 2);
            BtnPausa.Left = dGridMigracion.Right - BtnPausa.Width;
            BtnPausa.Top = dGridMigracion.Bottom + 1;
            panel1.Width = this.Width;
            panel2.Width = this.Width;
            panel2.Top = this.Height - 86;
            panel4.Top = panel2.Top - 26;

            LblTrestanteCalc.Left = this.Width - (20 + LblTrestanteCalc.Width);
            LblTrestante.Left = LblTrestanteCalc.Left - (10 + LblTrestante.Width);
            LblTotMigrado.Left = lblProgresoP.Right + 30;
            LblTotMigradoP.Left = LblTotMigrado.Right + 10;

            btnSalir.Left = this.Width - 133;
            BtnDetalle.Left = btnSalir.Left - 117;
            BtnBitacora.Left = BtnDetalle.Left - 117;


            if (Program.MigrarComparte)
            {
                BtnMigrarOtro.Left = BtnBitacora.Left - 142;
                dgridresumen2.Height = 89; 
            }
            else
            {
                BtnTodo.Left = BtnBitacora.Left - 117;
                BtnMigrarOtro.Left = BtnTodo.Left - 142;
            }
            lbltTranscurridoN.Left = LblTrestante.Left - (30 + lbltTranscurridoN.Width);
            lblttranscurrido.Left = lbltTranscurridoN.Left - (10 + lblttranscurrido.Width);
            progressBar1.Width = this.Width - ((this.Width - BtnMigrarOtro.Left) + progressBar1.Left + 10);
            BtnStop.Top = BtnPausa.Top;
            BtnStop.Left = dGridMigracion.Right - BtnPausa.Width;
            dgridresumen2.Top = dGridResultados.Top;
            dgridresumen2.Left = dGridResultados.Left;
            dgridresumen2.Width = dGridResultados.Width;
            if (!Program.MigrarComparte)
            {
                if (dgridresumen2.Parent.Height < 250)
                {
                    dgridresumen2.Height = dgridresumen2.Parent.Height - 100;

                }
                else
                {

                    dgridresumen2.Height = 133;
                }
            }
            panelresumen.Left = panel3.Left;
            panelresumen.Width = panel3.Width;
            panelresumen.Top = panel3.Top;
            if (BtResumen.Visible)
            {
                BtResumen.Left = FiltrarErrores.Left;
            }
        }

        private void FMigrar_SizeChanged(object sender, EventArgs e)
        {
            loadbounds();
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
        bool saved = false;
        public string reportnom = "";
        private void BtnTodo_Click(object sender, EventArgs e)
        {


            if (existeexcel == true)
            {
                //abre el ya guardado
                bool visible = true;
                var app = new Microsoft.Office.Interop.Excel.Application();
                try
                {
                    var workbook = app.Workbooks.Open(Program.Migrar.reportnom, ReadOnly: true);

                }
                catch 
                {
                    MessageBox.Show(this, "Resultados de reportes no localizados, asegurate que se guardo y que no ha sido movida a otra ubicación", "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    app.Workbooks.Close();
                    app.Quit();
                    visible = false;
                }

                app.Visible = visible;
            }
            else
            {
                try
                {
                    Process p = Process.Start("notepad.exe", Program.Migrar.reportnom);

                }
                catch
                {
                    MessageBox.Show(this, "Bitacora no localizada, asegurate que se guardo y que no ha sido movida a otra ubicación", "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Question);
                }






            }








        }




        public void generareportes()
        {
            for (int i = 0; i < 3; i++)
            {

                escribereportes(i);

            }

        }
        public void escribereportes(int reporte)
        {
            string[] commandtext = new string[3];
            commandtext[0] = @"select R.RE_CODIGO, R.RE_NOMBRE from REPORTE R
where ( ( select count(*) from CAMPOREP C where C.RE_CODIGO = R.RE_CODIGO and UPPER(C.CR_FORMULA) like '%SELECT%' ) > 0 )
";
            commandtext[1] = @"select CO_NUMERO, CO_DESCRIP from CONCEPTO
where ( UPPER(CO_FORMULA) like '%SELECT%' )
";

            commandtext[2] = @"select NP_FOLIO, NP_NOMBRE from NOMPARAM
where ( UPPER( cast(NP_FORMULA as varchar(255) ) ) like '%SELECT%' )
";

            string[] text = new string[3];
            text[0] = "Reportes con fórmulas que consultan querys directos a la BD con @select";
            text[1] = "Conceptos con fórmulas que consultan querys directos a la BD con @select";
            text[2] = "Parametros con fórmulas que consultan querys directos a la BD con @select";
            SqlConnection conexionsql = new SqlConnection(Program.Prepara.conexionstringsql);
            SqlCommand query = new SqlCommand();
            query.Connection = conexionsql;
            query.CommandText = commandtext[reporte];

            conexionsql.Open();
            SqlDataReader reader = query.ExecuteReader();
            bool header = false;
            while (reader.Read())
            {
                if (header == false)
                {
                    using (StreamWriter outfile = new StreamWriter(reportnom, true, System.Text.Encoding.Default)) // se guardan en la bitacora los resultados 
                    {
                        string encabezado = "";
                        for (int columnas = 0; columnas < reader.FieldCount; columnas++)
                        {
                            encabezado += reader.GetName(columnas);
                            if (columnas < reader.FieldCount - 1)
                            {
                                encabezado += ",";
                            }
                        }
                        outfile.Write(Environment.NewLine + text[reporte] + Environment.NewLine + encabezado + Environment.NewLine);
                    }
                    header = true;
                }
                using (StreamWriter outfile = new StreamWriter(reportnom, true, System.Text.Encoding.Default)) // se guardan en la bitacora los resultados 
                {
                    string encabezado = "";
                    for (int columnas = 0; columnas < reader.FieldCount; columnas++)
                    {

                        encabezado += reader[columnas];
                        if (columnas < reader.FieldCount - 1)
                        {
                            encabezado += ",";
                        }

                    }
                    outfile.Write(encabezado + Environment.NewLine);
                }
            }
            reader.Close();
            conexionsql.Close();
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "¿Estas seguro que deseas detener la transferencia a mitad del proceso?", "Transferencia de Datos", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
            {
                this.Cursor = Cursors.WaitCursor;
                Program.Migrar.detenerMigracion = true;
                Program.Migrar.pausa = false;

                BtnPausa.Enabled = false;
                try
                {
                    HiloMigracion.Join();
                }
                catch { }

                timer1.Stop();
                MessageBox.Show(this, "Transferencia detenida con éxito, Ver bitácoras para Analizar Resultados", "Transferencia de Datos");
                BtnMigrarOtro.Enabled = true;


                this.Cursor = Cursors.Default;

                BtnStop.Enabled = false;
                BtnPausa.Enabled = false;

            }
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_3(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "¿Seguro que deseas Salir?", "Transferencia de Datos", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                Environment.Exit(Environment.ExitCode);
            }
            else
            {

            }
        }

        private void FMigrar_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.F1)
            {
                showhelp();
            }
            if (e.KeyData == Keys.F12)
            {

                Program.Acercadefrm = new Acercadefrm();
                Program.Acercadefrm.ShowDialog();
            }

        }

        private void dGridMigracion_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            /*
            int width = 0;
            foreach (DataGridViewColumn column in dGridMigracion.Columns)
            {

                width += column.Width;
            }

            dGridMigracion.Width = width + 20;
            BtnPausa.Left = dGridMigracion.Right - BtnPausa.Width;
            BtnPausa.Top = dGridMigracion.Bottom + 1;
            BtnStop.Top = BtnPausa.Top;
            BtnStop.Left = BtnPausa.Left - 45;
             * */
        }

        private void dGridResultados_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void BtnStop_ChangeUICues(object sender, UICuesEventArgs e)
        {

        }

        private void dgridresumen_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try{
            if (dgridresumen2.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Contains("Ver Reporte") && e.ColumnIndex == 2)
            {
                if (Program.Migrar.bitacorainicia == true)
                {
                    if (existeexcel == true)
                    {
                        var app = new Microsoft.Office.Interop.Excel.Application();
                        bool visible = true;
                        try
                        {
                            app.Workbooks.Open(Program.Migrar.ResumenMigracion[e.RowIndex, 2], ReadOnly: true);

                        }
                        catch
                        {
                            MessageBox.Show(this, "Reporte de " + dgridresumen2.Rows[e.RowIndex].Cells[0].Value + " no localizado, Asegurate que se guardo y que no ha sido movida a otra ubicacion", "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            app.Workbooks.Close();
                            app.Quit();
                            visible = false;
                        }
                        app.Visible = visible;

                    }
                    else
                    {
                        try
                        {
                            Process p = Process.Start("notepad.exe", Program.Migrar.ResumenMigracion[e.RowIndex, 2]);


                        }
                        catch
                        {
                            MessageBox.Show(this, "Reporte de " + dgridresumen2.Rows[e.RowIndex].Cells[0].Value + " no localizado, Asegurate que se guardo y que no ha sido movida a otra ubicación", "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }



                    }
                }
                }
            }
            catch { }
        }

        private void resumen_Click(object sender, EventArgs e)
        {

        }

        private void BtResumen_Click(object sender, EventArgs e)
        {
            if (existeexcel == true)
            {
                var app = new Microsoft.Office.Interop.Excel.Application();
                bool visible = true;
                try
                {
                    app.Workbooks.Open(Program.Migrar.RutaResumen, ReadOnly: true);

                }
                catch
                {
                    MessageBox.Show(this, "Resumen no localizado, Asegurate que se guardo y que no ha sido movida a otra ubicacion", "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    app.Workbooks.Close();
                    app.Quit();
                    visible = false;
                }
                app.Visible = visible;

            }
            else
            {
                try
                {
                    Process p = Process.Start("notepad.exe", Program.Migrar.RutaResumen);


                }
                catch
                {
                    MessageBox.Show(this, "Resumen no localizado, Asegurate que se guardo y que no ha sido movida a otra ubicación", "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }


        }

       
    }
}
