﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FirebirdSql.Data.FirebirdClient;
using FirebirdSql.Data.Client;

namespace Wizard_Migracion_Tress
{
    public partial class FTableSelector : Form
    {
        public FTableSelector()
        {
            InitializeComponent();
        }
        string selectorcommand = "select rdb$field_name from rdb$relation_fields where rdb$relation_name=";
        public string columnas="";
        public string conexionstring;
        public string tabla;
        FbConnection conexion = new FbConnection();

        private void FTableSelector_Load(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            conexion.ConnectionString = conexionstring;
        }

        private void FTableSelector_Shown(object sender, EventArgs e)
        {
            FbCommand query = new FbCommand();
            query.Connection = conexion;
            query.Connection.Open();
            
            query.CommandText = selectorcommand + tabla;
            FbDataReader reader = query.ExecuteReader();
            while (reader.Read())
            {
                listBox1.Items.Add(reader[0].ToString().Trim());

            }
            reader.Close();
            query.Connection.Close();
            string[] columnasarray = columnas.Split(',');
            for (int i = 0; i < columnasarray.Length; i++)
            {
                for (int j = 0; j < listBox1.Items.Count; j++)
                {
                    if (listBox1.Items[j].ToString() == columnasarray[i]) 
                    {
                        listBox2.Items.Add(listBox1.Items[j]);
                        listBox1.Items.Remove(listBox1.Items[j]);
                    
                    }

                }
            
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

          int index=0;
          try
          {
              index = listBox1.SelectedIndex;
              listBox2.Items.Add(listBox1.SelectedItem);
            
          }
          catch
          {
              try
              {
                  listBox1.SelectedIndex = 0;
                  listBox2.Items.Add(listBox1.SelectedItem);
                  index = 0;
              }
              catch
              { }
          }
          try
          {
              listBox1.Items.Remove(listBox1.SelectedItem);
          }
          catch { }
                 columnas = "";
              foreach (var listboxitem in listBox2.Items)
              {
                  columnas += listboxitem.ToString() + ",";
              }
              if (columnas.Length > 0)
              {
                 columnas= columnas.Remove(columnas.Length - 1,1);
              }
                try
              {
                  listBox1.SelectedIndex = index;
              }
              catch
              { 
              
              }
              disableenable();
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            FbCommand query = new FbCommand();
            query.Connection = conexion;
            query.Connection.Open();

            query.CommandText = selectorcommand + tabla;
            FbDataReader reader = query.ExecuteReader();
            while (reader.Read())
            {
                listBox2.Items.Add(reader[0].ToString().Trim());

            }
            reader.Close();
            query.Connection.Close();
            columnas = "*";
            disableenable();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            listBox2.Items.Clear();
            columnas = "";
            FbCommand query = new FbCommand();
            query.Connection = conexion;
            query.Connection.Open();
            query.CommandText = selectorcommand + tabla;
            FbDataReader reader = query.ExecuteReader();
            while (reader.Read())
            {
                listBox1.Items.Add(reader[0].ToString().Trim());

            }
            reader.Close();
            query.Connection.Close();
            disableenable();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int index;
            try
            {
                index = listBox2.SelectedIndex;
                listBox1.Items.Add(listBox2.SelectedItem);
            }
            catch
            {
                index = 0;
                try
                {
                    listBox2.SelectedIndex = 0;
                }
                catch { }
            }
            try
            {
                listBox2.Items.Remove(listBox2.SelectedItem);
                listBox2.SelectedIndex = index;
            }
            catch { }
            columnas = "";
            foreach (var listboxitem in listBox2.Items)
            {
                  columnas += listboxitem.ToString() + ",";
            }

            if (columnas.Length > 0)
            {
                columnas = columnas.Remove(columnas.Length - 1);
            }
            
            
            disableenable();
        }

        private void FTableSelector_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (columnas == "")
            {
                MessageBox.Show(this, "Tienes que Seleccionar Minimo Una Columna", "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            Program.Prepara.tempcolumnas = columnas;
            
        }
        public void enablepass()
        {
            if (listBox1.Items.Count > 0)
            {
                pas1.Enabled = true;
                pastodo.Enabled = true;
            }
            else
            {
                pas1.Enabled = false;
                pastodo.Enabled = false;
                columnas = "*";
            }
            }
        public void enablevolver()
        {
            if (listBox2.Items.Count > 0)
            {
                volver1.Enabled = true;
                volvertodo.Enabled = true;
            }
            else
            {
                volver1.Enabled = false;
                volvertodo.Enabled = false;
            }
        
        }
        public void disableenable()
        {
            enablepass();
            enablevolver();
        }
        
    }
}
