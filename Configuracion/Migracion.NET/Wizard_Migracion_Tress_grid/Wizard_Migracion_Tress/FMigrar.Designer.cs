﻿namespace Wizard_Migracion_Tress
{
    partial class FMigrar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FMigrar));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSalir = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.BtnTodo = new System.Windows.Forms.Button();
            this.BtnMigrarOtro = new System.Windows.Forms.Button();
            this.BtnBitacora = new System.Windows.Forms.Button();
            this.BtnDetalle = new System.Windows.Forms.Button();
            this.lbltTranscurridoN = new System.Windows.Forms.Label();
            this.lblttranscurrido = new System.Windows.Forms.Label();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.dataGridViewProgressColumn1 = new ProgressBarInDataGridView.DataGridViewProgressColumn();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewProgressColumn2 = new ProgressBarInDataGridView.DataGridViewProgressColumn();
            this.dataGridViewProgressColumn3 = new ProgressBarInDataGridView.DataGridViewProgressColumn();
            this.dataGridViewProgressColumn4 = new ProgressBarInDataGridView.DataGridViewProgressColumn();
            this.dataGridViewProgressColumn5 = new ProgressBarInDataGridView.DataGridViewProgressColumn();
            this.dataGridViewProgressColumn6 = new ProgressBarInDataGridView.DataGridViewProgressColumn();
            this.dataGridViewProgressColumn7 = new ProgressBarInDataGridView.DataGridViewProgressColumn();
            this.dataGridViewProgressColumn8 = new ProgressBarInDataGridView.DataGridViewProgressColumn();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.BtnPausa = new System.Windows.Forms.Button();
            this.BtnStop = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.LblTotMigradoP = new System.Windows.Forms.Label();
            this.LblTotMigrado = new System.Windows.Forms.Label();
            this.lblProgresoP = new System.Windows.Forms.Label();
            this.lblProgreso = new System.Windows.Forms.Label();
            this.LblTrestanteCalc = new System.Windows.Forms.Label();
            this.LblTrestante = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dGridResultados = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.FiltrarMigradas = new System.Windows.Forms.CheckBox();
            this.FiltrarErrores = new System.Windows.Forms.CheckBox();
            this.filtrarAdvertencias = new System.Windows.Forms.CheckBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dGridMigracion = new System.Windows.Forms.DataGridView();
            this.Numero = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FilasMigrar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FilasMigradas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.filasErrores = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Advert = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotProceso = new ProgressBarInDataGridView.DataGridViewProgressColumn();
            this.FilasRestantes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewImageColumn();
            this.tabResumen = new System.Windows.Forms.TabControl();
            this.resumen = new System.Windows.Forms.TabPage();
            this.labelPrestamos = new System.Windows.Forms.Label();
            this.panelresumen = new System.Windows.Forms.Panel();
            this.BtResumen = new System.Windows.Forms.Button();
            this.dgridresumen2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewButtonColumn1 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dGridResultados)).BeginInit();
            this.panel3.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dGridMigracion)).BeginInit();
            this.tabResumen.SuspendLayout();
            this.resumen.SuspendLayout();
            this.panelresumen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgridresumen2)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(10, 12);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(302, 20);
            this.progressBar1.TabIndex = 6;
            this.progressBar1.Click += new System.EventHandler(this.progressBar1_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1157, 46);
            this.panel1.TabIndex = 7;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(30, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(323, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "6.   Proceso de transferencia";
            // 
            // btnSalir
            // 
            this.btnSalir.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.Image = global::Wizard_Migracion_Tress.Properties.Resources.Cancel;
            this.btnSalir.Location = new System.Drawing.Point(797, 4);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(115, 42);
            this.btnSalir.TabIndex = 1;
            this.btnSalir.Text = "Salir";
            this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.btnSalir, "Salir");
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.button1_Click_3);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.Controls.Add(this.BtnTodo);
            this.panel2.Controls.Add(this.btnSalir);
            this.panel2.Controls.Add(this.BtnMigrarOtro);
            this.panel2.Controls.Add(this.progressBar1);
            this.panel2.Controls.Add(this.BtnBitacora);
            this.panel2.Controls.Add(this.BtnDetalle);
            this.panel2.Location = new System.Drawing.Point(0, 587);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1157, 46);
            this.panel2.TabIndex = 8;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // BtnTodo
            // 
            this.BtnTodo.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnTodo.Image = global::Wizard_Migracion_Tress.Properties.Resources.todoicon;
            this.BtnTodo.Location = new System.Drawing.Point(487, 4);
            this.BtnTodo.Name = "BtnTodo";
            this.BtnTodo.Size = new System.Drawing.Size(115, 42);
            this.BtnTodo.TabIndex = 14;
            this.BtnTodo.Text = "Extras";
            this.BtnTodo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.BtnTodo, "Generar Lista de Reportes");
            this.BtnTodo.UseVisualStyleBackColor = true;
            this.BtnTodo.Click += new System.EventHandler(this.BtnTodo_Click);
            // 
            // BtnMigrarOtro
            // 
            this.BtnMigrarOtro.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnMigrarOtro.Image = global::Wizard_Migracion_Tress.Properties.Resources.Play_Mode_Repeat_All_Hot_icon;
            this.BtnMigrarOtro.Location = new System.Drawing.Point(357, 4);
            this.BtnMigrarOtro.Name = "BtnMigrarOtro";
            this.BtnMigrarOtro.Size = new System.Drawing.Size(140, 42);
            this.BtnMigrarOtro.TabIndex = 13;
            this.BtnMigrarOtro.Text = "Transferencia Nueva";
            this.BtnMigrarOtro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.BtnMigrarOtro, "Transferir Nuevamente");
            this.BtnMigrarOtro.UseVisualStyleBackColor = true;
            this.BtnMigrarOtro.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // BtnBitacora
            // 
            this.BtnBitacora.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnBitacora.Image = global::Wizard_Migracion_Tress.Properties.Resources.iconodetalle;
            this.BtnBitacora.Location = new System.Drawing.Point(698, 4);
            this.BtnBitacora.Name = "BtnBitacora";
            this.BtnBitacora.Size = new System.Drawing.Size(115, 42);
            this.BtnBitacora.TabIndex = 3;
            this.BtnBitacora.Text = "Detalle";
            this.BtnBitacora.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.BtnBitacora, "Mostrar Detalles de la Bitácora");
            this.BtnBitacora.UseVisualStyleBackColor = true;
            this.BtnBitacora.Click += new System.EventHandler(this.button1_Click);
            // 
            // BtnDetalle
            // 
            this.BtnDetalle.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDetalle.Image = global::Wizard_Migracion_Tress.Properties.Resources.resumen_icono;
            this.BtnDetalle.Location = new System.Drawing.Point(586, 4);
            this.BtnDetalle.Name = "BtnDetalle";
            this.BtnDetalle.Size = new System.Drawing.Size(115, 42);
            this.BtnDetalle.TabIndex = 4;
            this.BtnDetalle.Text = "Bitácora";
            this.BtnDetalle.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.BtnDetalle, "Mostrar Bitácora");
            this.BtnDetalle.UseVisualStyleBackColor = true;
            this.BtnDetalle.Click += new System.EventHandler(this.button2_Click);
            // 
            // lbltTranscurridoN
            // 
            this.lbltTranscurridoN.AutoSize = true;
            this.lbltTranscurridoN.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltTranscurridoN.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbltTranscurridoN.Location = new System.Drawing.Point(665, 0);
            this.lbltTranscurridoN.Name = "lbltTranscurridoN";
            this.lbltTranscurridoN.Size = new System.Drawing.Size(70, 18);
            this.lbltTranscurridoN.TabIndex = 16;
            this.lbltTranscurridoN.Text = "Calculando";
            this.lbltTranscurridoN.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblttranscurrido
            // 
            this.lblttranscurrido.AutoSize = true;
            this.lblttranscurrido.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblttranscurrido.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblttranscurrido.Location = new System.Drawing.Point(524, 0);
            this.lblttranscurrido.Name = "lblttranscurrido";
            this.lblttranscurrido.Size = new System.Drawing.Size(135, 18);
            this.lblttranscurrido.TabIndex = 15;
            this.lblttranscurrido.Text = "Tiempo Transcurrido:";
            this.lblttranscurrido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblttranscurrido.Click += new System.EventHandler(this.label3_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // dataGridViewProgressColumn1
            // 
            this.dataGridViewProgressColumn1.FillWeight = 300.868F;
            this.dataGridViewProgressColumn1.HeaderText = "Total Procesado                                           ";
            this.dataGridViewProgressColumn1.Name = "dataGridViewProgressColumn1";
            this.dataGridViewProgressColumn1.ReadOnly = true;
            this.dataGridViewProgressColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewProgressColumn1.Width = 239;
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.FillWeight = 20.30457F;
            this.dataGridViewImageColumn1.HeaderText = "Status";
            this.dataGridViewImageColumn1.Image = global::Wizard_Migracion_Tress.Properties.Resources.Agt_Action_Success;
            this.dataGridViewImageColumn1.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.ReadOnly = true;
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewImageColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewImageColumn1.Width = 50;
            // 
            // dataGridViewProgressColumn2
            // 
            this.dataGridViewProgressColumn2.FillWeight = 657.868F;
            this.dataGridViewProgressColumn2.HeaderText = "Total Procesado                                                                  " +
                "                          ";
            this.dataGridViewProgressColumn2.Name = "dataGridViewProgressColumn2";
            this.dataGridViewProgressColumn2.ReadOnly = true;
            this.dataGridViewProgressColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewProgressColumn2.Width = 386;
            // 
            // dataGridViewProgressColumn3
            // 
            this.dataGridViewProgressColumn3.FillWeight = 657.868F;
            this.dataGridViewProgressColumn3.HeaderText = "Total Procesado                                                                  " +
                "                          ";
            this.dataGridViewProgressColumn3.Name = "dataGridViewProgressColumn3";
            this.dataGridViewProgressColumn3.ReadOnly = true;
            this.dataGridViewProgressColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewProgressColumn3.Width = 386;
            // 
            // dataGridViewProgressColumn4
            // 
            this.dataGridViewProgressColumn4.FillWeight = 657.868F;
            this.dataGridViewProgressColumn4.HeaderText = "Total Procesado                                                                  " +
                "                          ";
            this.dataGridViewProgressColumn4.Name = "dataGridViewProgressColumn4";
            this.dataGridViewProgressColumn4.ReadOnly = true;
            this.dataGridViewProgressColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewProgressColumn4.Width = 386;
            // 
            // dataGridViewProgressColumn5
            // 
            this.dataGridViewProgressColumn5.FillWeight = 657.868F;
            this.dataGridViewProgressColumn5.HeaderText = "Total Procesado                                                                  " +
                "                          ";
            this.dataGridViewProgressColumn5.Name = "dataGridViewProgressColumn5";
            this.dataGridViewProgressColumn5.ReadOnly = true;
            this.dataGridViewProgressColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewProgressColumn5.Width = 386;
            // 
            // dataGridViewProgressColumn6
            // 
            this.dataGridViewProgressColumn6.FillWeight = 657.868F;
            this.dataGridViewProgressColumn6.HeaderText = "Total Procesado                                                                  " +
                "                          ";
            this.dataGridViewProgressColumn6.Name = "dataGridViewProgressColumn6";
            this.dataGridViewProgressColumn6.ReadOnly = true;
            this.dataGridViewProgressColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewProgressColumn6.Width = 386;
            // 
            // dataGridViewProgressColumn7
            // 
            this.dataGridViewProgressColumn7.FillWeight = 657.868F;
            this.dataGridViewProgressColumn7.HeaderText = "Total Procesado                                                                  " +
                "                          ";
            this.dataGridViewProgressColumn7.Name = "dataGridViewProgressColumn7";
            this.dataGridViewProgressColumn7.ReadOnly = true;
            this.dataGridViewProgressColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewProgressColumn7.Width = 386;
            // 
            // dataGridViewProgressColumn8
            // 
            this.dataGridViewProgressColumn8.FillWeight = 657.868F;
            this.dataGridViewProgressColumn8.HeaderText = "Total Procesado                                                                  " +
                "                          ";
            this.dataGridViewProgressColumn8.Name = "dataGridViewProgressColumn8";
            this.dataGridViewProgressColumn8.ReadOnly = true;
            this.dataGridViewProgressColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewProgressColumn8.Width = 386;
            // 
            // BtnPausa
            // 
            this.BtnPausa.BackgroundImage = global::Wizard_Migracion_Tress.Properties.Resources.media_pause;
            this.BtnPausa.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnPausa.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPausa.Location = new System.Drawing.Point(954, 403);
            this.BtnPausa.Name = "BtnPausa";
            this.BtnPausa.Size = new System.Drawing.Size(39, 32);
            this.BtnPausa.TabIndex = 5;
            this.toolTip1.SetToolTip(this.BtnPausa, "Pausar/Continuar Transferencia");
            this.BtnPausa.UseVisualStyleBackColor = true;
            this.BtnPausa.Visible = false;
            this.BtnPausa.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // BtnStop
            // 
            this.BtnStop.BackgroundImage = global::Wizard_Migracion_Tress.Properties.Resources.stop;
            this.BtnStop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.BtnStop.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnStop.Location = new System.Drawing.Point(909, 403);
            this.BtnStop.Name = "BtnStop";
            this.BtnStop.Size = new System.Drawing.Size(39, 32);
            this.BtnStop.TabIndex = 6;
            this.toolTip1.SetToolTip(this.BtnStop, "Detener Transferencia");
            this.BtnStop.UseVisualStyleBackColor = true;
            this.BtnStop.Click += new System.EventHandler(this.button1_Click_2);
            this.BtnStop.ChangeUICues += new System.Windows.Forms.UICuesEventHandler(this.BtnStop_ChangeUICues);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lblttranscurrido);
            this.panel4.Controls.Add(this.lbltTranscurridoN);
            this.panel4.Controls.Add(this.LblTotMigradoP);
            this.panel4.Controls.Add(this.LblTotMigrado);
            this.panel4.Controls.Add(this.lblProgresoP);
            this.panel4.Controls.Add(this.lblProgreso);
            this.panel4.Controls.Add(this.LblTrestanteCalc);
            this.panel4.Controls.Add(this.LblTrestante);
            this.panel4.Location = new System.Drawing.Point(0, 556);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(930, 25);
            this.panel4.TabIndex = 14;
            // 
            // LblTotMigradoP
            // 
            this.LblTotMigradoP.AutoSize = true;
            this.LblTotMigradoP.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTotMigradoP.Location = new System.Drawing.Point(293, 0);
            this.LblTotMigradoP.Name = "LblTotMigradoP";
            this.LblTotMigradoP.Size = new System.Drawing.Size(23, 18);
            this.LblTotMigradoP.TabIndex = 18;
            this.LblTotMigradoP.Text = "0%";
            // 
            // LblTotMigrado
            // 
            this.LblTotMigrado.AutoSize = true;
            this.LblTotMigrado.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTotMigrado.Location = new System.Drawing.Point(189, 0);
            this.LblTotMigrado.Name = "LblTotMigrado";
            this.LblTotMigrado.Size = new System.Drawing.Size(119, 18);
            this.LblTotMigrado.TabIndex = 17;
            this.LblTotMigrado.Text = "Total Transferido :";
            // 
            // lblProgresoP
            // 
            this.lblProgresoP.AutoSize = true;
            this.lblProgresoP.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProgresoP.Location = new System.Drawing.Point(95, 0);
            this.lblProgresoP.Name = "lblProgresoP";
            this.lblProgresoP.Size = new System.Drawing.Size(23, 18);
            this.lblProgresoP.TabIndex = 16;
            this.lblProgresoP.Text = "0%";
            this.lblProgresoP.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProgreso
            // 
            this.lblProgreso.AutoSize = true;
            this.lblProgreso.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProgreso.Location = new System.Drawing.Point(7, 0);
            this.lblProgreso.Name = "lblProgreso";
            this.lblProgreso.Size = new System.Drawing.Size(72, 18);
            this.lblProgreso.TabIndex = 15;
            this.lblProgreso.Text = "Progreso : ";
            this.lblProgreso.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LblTrestanteCalc
            // 
            this.LblTrestanteCalc.AutoSize = true;
            this.LblTrestanteCalc.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTrestanteCalc.Location = new System.Drawing.Point(926, 0);
            this.LblTrestanteCalc.Name = "LblTrestanteCalc";
            this.LblTrestanteCalc.Size = new System.Drawing.Size(70, 18);
            this.LblTrestanteCalc.TabIndex = 14;
            this.LblTrestanteCalc.Text = "Calculando";
            // 
            // LblTrestante
            // 
            this.LblTrestante.AutoSize = true;
            this.LblTrestante.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTrestante.Location = new System.Drawing.Point(798, 0);
            this.LblTrestante.Name = "LblTrestante";
            this.LblTrestante.Size = new System.Drawing.Size(121, 18);
            this.LblTrestante.TabIndex = 13;
            this.LblTrestante.Text = "Tiempo Restante : ";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.White;
            this.tabPage2.Controls.Add(this.dGridResultados);
            this.tabPage2.Controls.Add(this.panel3);
            this.tabPage2.Location = new System.Drawing.Point(4, 31);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(385, 387);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Resultados";
            this.tabPage2.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // dGridResultados
            // 
            this.dGridResultados.AllowUserToAddRows = false;
            this.dGridResultados.AllowUserToDeleteRows = false;
            this.dGridResultados.AllowUserToResizeColumns = false;
            this.dGridResultados.AllowUserToResizeRows = false;
            this.dGridResultados.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dGridResultados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGridResultados.Location = new System.Drawing.Point(173, 59);
            this.dGridResultados.Name = "dGridResultados";
            this.dGridResultados.RowHeadersVisible = false;
            this.dGridResultados.Size = new System.Drawing.Size(781, 104);
            this.dGridResultados.TabIndex = 13;
            this.dGridResultados.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dGridResultados_CellValueChanged);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.FiltrarMigradas);
            this.panel3.Controls.Add(this.FiltrarErrores);
            this.panel3.Controls.Add(this.filtrarAdvertencias);
            this.panel3.Location = new System.Drawing.Point(173, 6);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(692, 47);
            this.panel3.TabIndex = 12;
            // 
            // FiltrarMigradas
            // 
            this.FiltrarMigradas.AutoSize = true;
            this.FiltrarMigradas.Enabled = false;
            this.FiltrarMigradas.Location = new System.Drawing.Point(3, 3);
            this.FiltrarMigradas.Name = "FiltrarMigradas";
            this.FiltrarMigradas.Size = new System.Drawing.Size(112, 17);
            this.FiltrarMigradas.TabIndex = 9;
            this.FiltrarMigradas.Text = "Filtrar Transferidas";
            this.FiltrarMigradas.UseVisualStyleBackColor = true;
            this.FiltrarMigradas.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // FiltrarErrores
            // 
            this.FiltrarErrores.AutoSize = true;
            this.FiltrarErrores.Enabled = false;
            this.FiltrarErrores.Location = new System.Drawing.Point(290, 3);
            this.FiltrarErrores.Name = "FiltrarErrores";
            this.FiltrarErrores.Size = new System.Drawing.Size(87, 17);
            this.FiltrarErrores.TabIndex = 10;
            this.FiltrarErrores.Text = "Filtrar Errores";
            this.FiltrarErrores.UseVisualStyleBackColor = true;
            this.FiltrarErrores.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // filtrarAdvertencias
            // 
            this.filtrarAdvertencias.AutoSize = true;
            this.filtrarAdvertencias.Enabled = false;
            this.filtrarAdvertencias.Location = new System.Drawing.Point(520, 3);
            this.filtrarAdvertencias.Name = "filtrarAdvertencias";
            this.filtrarAdvertencias.Size = new System.Drawing.Size(116, 17);
            this.filtrarAdvertencias.TabIndex = 11;
            this.filtrarAdvertencias.Text = "Filtrar Advertencias";
            this.filtrarAdvertencias.UseVisualStyleBackColor = true;
            this.filtrarAdvertencias.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.Controls.Add(this.BtnStop);
            this.tabPage1.Controls.Add(this.dGridMigracion);
            this.tabPage1.Controls.Add(this.BtnPausa);
            this.tabPage1.Location = new System.Drawing.Point(4, 31);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(385, 387);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Transferencia";
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // dGridMigracion
            // 
            this.dGridMigracion.AllowUserToAddRows = false;
            this.dGridMigracion.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dGridMigracion.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dGridMigracion.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Numero,
            this.Nombre,
            this.FilasMigrar,
            this.FilasMigradas,
            this.filasErrores,
            this.Advert,
            this.TotProceso,
            this.FilasRestantes,
            this.Status});
            this.dGridMigracion.Location = new System.Drawing.Point(6, 6);
            this.dGridMigracion.Name = "dGridMigracion";
            this.dGridMigracion.RowHeadersVisible = false;
            this.dGridMigracion.Size = new System.Drawing.Size(334, 331);
            this.dGridMigracion.TabIndex = 0;
            this.dGridMigracion.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dGridMigracion.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dGridMigracion_CellValueChanged);
            this.dGridMigracion.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.GridMigracion_ColumnAdded);
            // 
            // Numero
            // 
            this.Numero.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N0";
            dataGridViewCellStyle2.NullValue = null;
            this.Numero.DefaultCellStyle = dataGridViewCellStyle2;
            this.Numero.FillWeight = 20.30457F;
            this.Numero.HeaderText = "No.";
            this.Numero.Name = "Numero";
            this.Numero.ReadOnly = true;
            this.Numero.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Nombre
            // 
            this.Nombre.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Nombre.FillWeight = 20.30457F;
            this.Nombre.HeaderText = "Nombre";
            this.Nombre.Name = "Nombre";
            this.Nombre.ReadOnly = true;
            this.Nombre.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // FilasMigrar
            // 
            this.FilasMigrar.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N0";
            dataGridViewCellStyle3.NullValue = null;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.FilasMigrar.DefaultCellStyle = dataGridViewCellStyle3;
            this.FilasMigrar.FillWeight = 20.30457F;
            this.FilasMigrar.HeaderText = "Registros Por Transferir";
            this.FilasMigrar.Name = "FilasMigrar";
            this.FilasMigrar.ReadOnly = true;
            this.FilasMigrar.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.FilasMigrar.Width = 202;
            // 
            // FilasMigradas
            // 
            this.FilasMigradas.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N0";
            dataGridViewCellStyle4.NullValue = null;
            this.FilasMigradas.DefaultCellStyle = dataGridViewCellStyle4;
            this.FilasMigradas.FillWeight = 20.30457F;
            this.FilasMigradas.HeaderText = "Transferidos";
            this.FilasMigradas.Name = "FilasMigradas";
            this.FilasMigradas.ReadOnly = true;
            this.FilasMigradas.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // filasErrores
            // 
            this.filasErrores.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N0";
            dataGridViewCellStyle5.NullValue = null;
            this.filasErrores.DefaultCellStyle = dataGridViewCellStyle5;
            this.filasErrores.FillWeight = 20.30457F;
            this.filasErrores.HeaderText = "Ignorados";
            this.filasErrores.Name = "filasErrores";
            // 
            // Advert
            // 
            this.Advert.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N0";
            dataGridViewCellStyle6.NullValue = null;
            this.Advert.DefaultCellStyle = dataGridViewCellStyle6;
            this.Advert.FillWeight = 20.30457F;
            this.Advert.HeaderText = "Advertencias";
            this.Advert.Name = "Advert";
            // 
            // TotProceso
            // 
            this.TotProceso.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.TotProceso.FillWeight = 20.30457F;
            this.TotProceso.HeaderText = "Total Procesado";
            this.TotProceso.Name = "TotProceso";
            this.TotProceso.ReadOnly = true;
            this.TotProceso.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.TotProceso.Width = 149;
            // 
            // FilasRestantes
            // 
            this.FilasRestantes.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N0";
            dataGridViewCellStyle7.NullValue = null;
            this.FilasRestantes.DefaultCellStyle = dataGridViewCellStyle7;
            this.FilasRestantes.FillWeight = 20.30457F;
            this.FilasRestantes.HeaderText = "Registros Restantes";
            this.FilasRestantes.Name = "FilasRestantes";
            this.FilasRestantes.ReadOnly = true;
            this.FilasRestantes.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.FilasRestantes.Width = 171;
            // 
            // Status
            // 
            this.Status.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Status.FillWeight = 20.30457F;
            this.Status.HeaderText = "Status";
            this.Status.Image = global::Wizard_Migracion_Tress.Properties.Resources.media_pause;
            this.Status.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            this.Status.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Status.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // tabResumen
            // 
            this.tabResumen.Controls.Add(this.tabPage1);
            this.tabResumen.Controls.Add(this.tabPage2);
            this.tabResumen.Controls.Add(this.resumen);
            this.tabResumen.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabResumen.Location = new System.Drawing.Point(0, 52);
            this.tabResumen.Name = "tabResumen";
            this.tabResumen.SelectedIndex = 0;
            this.tabResumen.Size = new System.Drawing.Size(393, 422);
            this.tabResumen.TabIndex = 13;
            // 
            // resumen
            // 
            this.resumen.Controls.Add(this.labelPrestamos);
            this.resumen.Controls.Add(this.panelresumen);
            this.resumen.Controls.Add(this.dgridresumen2);
            this.resumen.Location = new System.Drawing.Point(4, 31);
            this.resumen.Name = "resumen";
            this.resumen.Padding = new System.Windows.Forms.Padding(3);
            this.resumen.Size = new System.Drawing.Size(385, 387);
            this.resumen.TabIndex = 2;
            this.resumen.Text = "Resumen";
            this.resumen.UseVisualStyleBackColor = true;
            this.resumen.Click += new System.EventHandler(this.resumen_Click);
            // 
            // labelPrestamos
            // 
            this.labelPrestamos.Location = new System.Drawing.Point(2, 220);
            this.labelPrestamos.Name = "labelPrestamos";
            this.labelPrestamos.Size = new System.Drawing.Size(310, 86);
            this.labelPrestamos.TabIndex = 17;
            this.labelPrestamos.Text = "Se generaron nuevos tipos de prestamos, favor de verificar los conceptos para hac" +
                "er las modificaciones necesarias";
            this.labelPrestamos.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelPrestamos.Visible = false;
            // 
            // panelresumen
            // 
            this.panelresumen.Controls.Add(this.BtResumen);
            this.panelresumen.Location = new System.Drawing.Point(-66, 170);
            this.panelresumen.Name = "panelresumen";
            this.panelresumen.Size = new System.Drawing.Size(692, 47);
            this.panelresumen.TabIndex = 16;
            // 
            // BtResumen
            // 
            this.BtResumen.Enabled = false;
            this.BtResumen.Location = new System.Drawing.Point(266, 3);
            this.BtResumen.Name = "BtResumen";
            this.BtResumen.Size = new System.Drawing.Size(178, 41);
            this.BtResumen.TabIndex = 0;
            this.BtResumen.Text = "Resumen";
            this.BtResumen.UseVisualStyleBackColor = true;
            this.BtResumen.Click += new System.EventHandler(this.BtResumen_Click);
            // 
            // dgridresumen2
            // 
            this.dgridresumen2.AllowUserToAddRows = false;
            this.dgridresumen2.AllowUserToDeleteRows = false;
            this.dgridresumen2.AllowUserToResizeColumns = false;
            this.dgridresumen2.AllowUserToResizeRows = false;
            this.dgridresumen2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgridresumen2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgridresumen2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewButtonColumn1});
            this.dgridresumen2.Location = new System.Drawing.Point(8, 59);
            this.dgridresumen2.Name = "dgridresumen2";
            this.dgridresumen2.RowHeadersVisible = false;
            this.dgridresumen2.Size = new System.Drawing.Size(781, 104);
            this.dgridresumen2.TabIndex = 15;
            this.dgridresumen2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgridresumen_CellClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Concepto";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Resultado";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewButtonColumn1
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewButtonColumn1.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewButtonColumn1.HeaderText = "Reporte";
            this.dataGridViewButtonColumn1.Name = "dataGridViewButtonColumn1";
            this.dataGridViewButtonColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewButtonColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // FMigrar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1346, 741);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.tabResumen);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(873, 461);
            this.Name = "FMigrar";
            this.Text = "Transferencia de Datos";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FMigrar_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.SizeChanged += new System.EventHandler(this.FMigrar_SizeChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FMigrar_KeyDown);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dGridResultados)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dGridMigracion)).EndInit();
            this.tabResumen.ResumeLayout(false);
            this.resumen.ResumeLayout(false);
            this.panelresumen.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgridresumen2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ProgressBarInDataGridView.DataGridViewProgressColumn dataGridViewProgressColumn1;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button BtnBitacora;
        private System.Windows.Forms.Button BtnDetalle;
        private System.Windows.Forms.ProgressBar progressBar1;
        private ProgressBarInDataGridView.DataGridViewProgressColumn dataGridViewProgressColumn2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private ProgressBarInDataGridView.DataGridViewProgressColumn dataGridViewProgressColumn3;
        private ProgressBarInDataGridView.DataGridViewProgressColumn dataGridViewProgressColumn4;
        private ProgressBarInDataGridView.DataGridViewProgressColumn dataGridViewProgressColumn5;
        private ProgressBarInDataGridView.DataGridViewProgressColumn dataGridViewProgressColumn6;
        private ProgressBarInDataGridView.DataGridViewProgressColumn dataGridViewProgressColumn7;
        private ProgressBarInDataGridView.DataGridViewProgressColumn dataGridViewProgressColumn8;
        private System.Windows.Forms.Button BtnMigrarOtro;
        private System.Windows.Forms.Button BtnTodo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbltTranscurridoN;
        private System.Windows.Forms.Label lblttranscurrido;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label LblTotMigradoP;
        private System.Windows.Forms.Label LblTotMigrado;
        private System.Windows.Forms.Label lblProgresoP;
        private System.Windows.Forms.Label lblProgreso;
        private System.Windows.Forms.Label LblTrestanteCalc;
        private System.Windows.Forms.Label LblTrestante;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dGridResultados;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.CheckBox FiltrarMigradas;
        private System.Windows.Forms.CheckBox FiltrarErrores;
        private System.Windows.Forms.CheckBox filtrarAdvertencias;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button BtnStop;
        private System.Windows.Forms.DataGridView dGridMigracion;
        private System.Windows.Forms.DataGridViewTextBoxColumn Numero;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn FilasMigrar;
        private System.Windows.Forms.DataGridViewTextBoxColumn FilasMigradas;
        private System.Windows.Forms.DataGridViewTextBoxColumn filasErrores;
        private System.Windows.Forms.DataGridViewTextBoxColumn Advert;
        private ProgressBarInDataGridView.DataGridViewProgressColumn TotProceso;
        private System.Windows.Forms.DataGridViewTextBoxColumn FilasRestantes;
        private System.Windows.Forms.DataGridViewImageColumn Status;
        private System.Windows.Forms.Button BtnPausa;
        private System.Windows.Forms.TabControl tabResumen;
        private System.Windows.Forms.TabPage resumen;
        private System.Windows.Forms.Panel panelresumen;
        private System.Windows.Forms.Button BtResumen;
        private System.Windows.Forms.DataGridView dgridresumen2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewButtonColumn dataGridViewButtonColumn1;
        private System.Windows.Forms.Label labelPrestamos;
    }
}