﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ServiceProcess;
namespace Wizard_Migracion_Tress
{
    public partial class FormImpersonateUser : Form
    {
        public FormImpersonateUser()
        {
            InitializeComponent();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                textBox1.Text = "";
                textBox2.Text = "";
                textBox1.Enabled = false;
                textBox2.Enabled = false;
                comboBox1.Enabled = false;
            }
            else
            {
                textBox1.Enabled = true;
                textBox2.Enabled = true ;
                comboBox1.Enabled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Program.usuario = null;
            Program.password = null;
            Program.fbserverfound = true;
            if (!checkBox1.Checked)
            {

                try
                {
                    Impersonator impersonate = new Impersonator(textBox1.Text, comboBox1.Text, textBox2.Text);
                    Program.usuario = textBox1.Text;
                    Program.domain = comboBox1.Text;
                    Program.password = textBox2.Text;
                }
                catch
                {
                    MessageBox.Show(this, "Credenciales Invalidas, Verifica tus parametros de Conexion", "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }


                using (new Impersonator(Program.usuario, Program.domain, Program.password))
                {
                    ServiceController ctl = new ServiceController();
                    try
                    {
                        try
                        {
                            ctl = ServiceController.GetServices(Program.server).Where(s => s.DisplayName.Contains("Firebird Guardian")).FirstOrDefault();
                        }
                        catch{}
                        if (ctl == null)
                        {
                            MessageBox.Show(this, "Servidor de Firebird no Encontrado en servidor: " + Program.server + "", "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            Program.fbserverfound = false;
                            this.Close();
                     
                        }
                        else
                        {
                            if (ctl.Status != ServiceControllerStatus.Running)
                            {
                                if (MessageBox.Show(this, "Servicio de Firebird Sin Iniciar, ¿Deseas Iniciarlo?", "Transferencia de Datos", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                {
                                    ctl.Start();
                                    ctl.WaitForStatus(ServiceControllerStatus.Running);
                                    this.Cursor = Cursors.Default;
                                }
                            }
                            else
                            {
                                ctl.Stop();
                                ctl.WaitForStatus(ServiceControllerStatus.Stopped);
                                ctl.Start();
                                ctl.WaitForStatus(ServiceControllerStatus.Running);

                            }
                            Program.validremoteuser = true;
                        }
                    }
                    catch
                    {
                        MessageBox.Show(this, "Error, El usuario no cuenta con los privilegios suficientes, registrese con otro usuario para Continuar", "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    
                }
                


            }
            else
            {
                using (new Impersonator(Program.usuario, Program.domain, Program.password))
                {
                   ServiceController ctl = new ServiceController();
                   try
                    {
                        try
                        {
                            ctl = ServiceController.GetServices(Program.server).Where(s => s.DisplayName.Contains("Firebird Guardianr")).FirstOrDefault();

                        }
                        catch { }
                        if (ctl == null)
                        {
                            MessageBox.Show(this, "Servidor de Firebird no Encontrado", "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            Program.fbserverfound = false;
                        }
                        else
                        {
                            if (ctl.Status != ServiceControllerStatus.Running)
                            {
                                if (MessageBox.Show(this, "Servicio de Firebird Sin Iniciar, ¿Deseas Iniciarlo?", "Transferencia de Datos", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                {
                                    ctl.Start();
                                    ctl.WaitForStatus(ServiceControllerStatus.Running);
                                    this.Cursor = Cursors.Default;
                                }
                            }
                            else
                            {
                                ctl.Stop();
                                ctl.WaitForStatus(ServiceControllerStatus.Stopped);
                                ctl.Start();
                                ctl.WaitForStatus(ServiceControllerStatus.Running);

                            }
                        }
                        Program.validremoteuser = true;
                    }
                    catch
                    {
                        MessageBox.Show(this, "Error, El usuario no cuenta con los privilegios suficientes, registrese con otro usuario para Continuar", "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    
                }

                
            }
            this.Close();
        }

        private void FormImpersonateUser_Load(object sender, EventArgs e)
        {
            Program.validremoteuser = false;
        }
    }
}
