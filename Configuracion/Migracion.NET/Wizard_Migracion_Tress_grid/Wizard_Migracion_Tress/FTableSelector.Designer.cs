﻿namespace Wizard_Migracion_Tress
{
    partial class FTableSelector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FTableSelector));
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.pas1 = new System.Windows.Forms.Button();
            this.volvertodo = new System.Windows.Forms.Button();
            this.pastodo = new System.Windows.Forms.Button();
            this.volver1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 22;
            this.listBox1.Location = new System.Drawing.Point(12, 78);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(285, 400);
            this.listBox1.TabIndex = 0;
            // 
            // listBox2
            // 
            this.listBox2.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBox2.FormattingEnabled = true;
            this.listBox2.ItemHeight = 22;
            this.listBox2.Location = new System.Drawing.Point(467, 78);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(285, 400);
            this.listBox2.TabIndex = 1;
            // 
            // pas1
            // 
            this.pas1.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pas1.Location = new System.Drawing.Point(351, 78);
            this.pas1.Name = "pas1";
            this.pas1.Size = new System.Drawing.Size(62, 73);
            this.pas1.TabIndex = 2;
            this.pas1.Text = ">";
            this.pas1.UseVisualStyleBackColor = true;
            this.pas1.Click += new System.EventHandler(this.button1_Click);
            // 
            // volvertodo
            // 
            this.volvertodo.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.volvertodo.Location = new System.Drawing.Point(351, 405);
            this.volvertodo.Name = "volvertodo";
            this.volvertodo.Size = new System.Drawing.Size(62, 73);
            this.volvertodo.TabIndex = 3;
            this.volvertodo.Text = "<<";
            this.volvertodo.UseVisualStyleBackColor = true;
            this.volvertodo.Click += new System.EventHandler(this.button2_Click);
            // 
            // pastodo
            // 
            this.pastodo.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pastodo.Location = new System.Drawing.Point(351, 157);
            this.pastodo.Name = "pastodo";
            this.pastodo.Size = new System.Drawing.Size(62, 73);
            this.pastodo.TabIndex = 4;
            this.pastodo.Text = ">>";
            this.pastodo.UseVisualStyleBackColor = true;
            this.pastodo.Click += new System.EventHandler(this.button3_Click);
            // 
            // volver1
            // 
            this.volver1.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.volver1.Location = new System.Drawing.Point(351, 326);
            this.volver1.Name = "volver1";
            this.volver1.Size = new System.Drawing.Size(62, 73);
            this.volver1.TabIndex = 5;
            this.volver1.Text = "<";
            this.volver1.UseVisualStyleBackColor = true;
            this.volver1.Click += new System.EventHandler(this.button4_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(94, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 21);
            this.label1.TabIndex = 6;
            this.label1.Text = "Fuente";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(564, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 21);
            this.label2.TabIndex = 7;
            this.label2.Text = "Destino";
            // 
            // FTableSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(764, 571);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.volver1);
            this.Controls.Add(this.pastodo);
            this.Controls.Add(this.volvertodo);
            this.Controls.Add(this.pas1);
            this.Controls.Add(this.listBox2);
            this.Controls.Add(this.listBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FTableSelector";
            this.Text = "Transferencia de Datos";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FTableSelector_FormClosing);
            this.Load += new System.EventHandler(this.FTableSelector_Load);
            this.Shown += new System.EventHandler(this.FTableSelector_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.Button pas1;
        private System.Windows.Forms.Button volvertodo;
        private System.Windows.Forms.Button pastodo;
        private System.Windows.Forms.Button volver1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}