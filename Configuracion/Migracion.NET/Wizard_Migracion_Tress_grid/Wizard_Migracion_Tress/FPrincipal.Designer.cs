﻿namespace Wizard_Migracion_Tress
{
    partial class FPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TabPage tabPage3;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FPrincipal));
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.CmBComparte = new System.Windows.Forms.ComboBox();
            this.CmBEmpresa = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label28 = new System.Windows.Forms.Label();
            this.TxtBaseDatosSQL = new System.Windows.Forms.ComboBox();
            this.checkWSA = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtContrasenaSQL = new System.Windows.Forms.TextBox();
            this.TxtUsuarioSQl = new System.Windows.Forms.TextBox();
            this.TxtServidorSQL = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnProbarSql = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.LabelTipoMigracion = new System.Windows.Forms.Label();
            this.lblAccion = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnFin = new System.Windows.Forms.Button();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.btnSiguiente = new System.Windows.Forms.Button();
            this.btAtrass = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.OpenFDFirebird = new System.Windows.Forms.OpenFileDialog();
            this.SaveFDRuta = new System.Windows.Forms.SaveFileDialog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.BtnMigrarDatos = new System.Windows.Forms.Button();
            this.BtnMigrarComparte = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.TxtBaseDatosFB = new System.Windows.Forms.TextBox();
            this.TxtContrasenaFB = new System.Windows.Forms.TextBox();
            this.TxtUsuarioFB = new System.Windows.Forms.TextBox();
            this.TxtServidorFB = new System.Windows.Forms.TextBox();
            this.BtnProbarFirebird = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.TxtRuta = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.txtfbBaseComparte = new System.Windows.Forms.TextBox();
            this.TxtFbPassComparte = new System.Windows.Forms.TextBox();
            this.txtfbUserComparte = new System.Windows.Forms.TextBox();
            this.txtfbServerComparte = new System.Windows.Forms.TextBox();
            this.btnConeccionComparte = new System.Windows.Forms.Button();
            this.DropDownSqlBaseComparte = new System.Windows.Forms.ComboBox();
            this.TxtsqlPassComparte = new System.Windows.Forms.TextBox();
            this.TxtsqlUsuarioComparte = new System.Windows.Forms.TextBox();
            this.txtSqlServerComparte = new System.Windows.Forms.TextBox();
            this.btnConeccionSqlComparte = new System.Windows.Forms.Button();
            this.label44 = new System.Windows.Forms.Label();
            this.ButtonBitacoraComparte = new System.Windows.Forms.Button();
            this.TxtRutaBitacoraComparte = new System.Windows.Forms.TextBox();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.WizardPages1 = new Wizard_Migracion_Tress.WizardPages();
            this.step1 = new System.Windows.Forms.TabPage();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.step2 = new System.Windows.Forms.TabPage();
            this.label27 = new System.Windows.Forms.Label();
            this.localcheck = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label23 = new System.Windows.Forms.Label();
            this.TxtTablaInicial = new System.Windows.Forms.TextBox();
            this.RadBtnMigracionParcial = new System.Windows.Forms.RadioButton();
            this.RadBtnMigracionCompleta = new System.Windows.Forms.RadioButton();
            this.label25 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.CheckPrestamosAgregar = new System.Windows.Forms.CheckBox();
            this.CheckKardexAgregar = new System.Windows.Forms.CheckBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.FechaFinAsistencia = new System.Windows.Forms.DateTimePicker();
            this.RadBtnAsistenciaRango = new System.Windows.Forms.RadioButton();
            this.FechaIniAsistencia = new System.Windows.Forms.DateTimePicker();
            this.RadBtnAsistenciaCompleta = new System.Windows.Forms.RadioButton();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.FechaFinBitacora = new System.Windows.Forms.DateTimePicker();
            this.FechaIniBitacora = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.RadBtnBitacoraRango = new System.Windows.Forms.RadioButton();
            this.radBtnBitacoraCompleta = new System.Windows.Forms.RadioButton();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.lblCodigo = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.lblRuta = new System.Windows.Forms.TextBox();
            this.bdCorpText = new System.Windows.Forms.TextBox();
            this.bdProtext = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.lblKardex = new System.Windows.Forms.Label();
            this.lblTarjetas = new System.Windows.Forms.Label();
            this.lblBitacora = new System.Windows.Forms.Label();
            this.lblDestino = new System.Windows.Forms.Label();
            this.lblOrigen = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.TabComparteFb = new System.Windows.Forms.TabPage();
            this.label39 = new System.Windows.Forms.Label();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.TabComparteSql = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.TabComparteParametros = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label45 = new System.Windows.Forms.Label();
            this.TabComparteResumen = new System.Windows.Forms.TabPage();
            this.txtInfoBitacoraRuta = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.txtInfoBdFirebird = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.txtInfoBdMSSQL = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            tabPage3 = new System.Windows.Forms.TabPage();
            tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.WizardPages1.SuspendLayout();
            this.step1.SuspendLayout();
            this.step2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.TabComparteFb.SuspendLayout();
            this.TabComparteSql.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.TabComparteParametros.SuspendLayout();
            this.panel3.SuspendLayout();
            this.TabComparteResumen.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage3
            // 
            tabPage3.BackColor = System.Drawing.Color.White;
            tabPage3.Controls.Add(this.pictureBox4);
            tabPage3.Controls.Add(this.pictureBox2);
            tabPage3.Controls.Add(this.CmBComparte);
            tabPage3.Controls.Add(this.CmBEmpresa);
            tabPage3.Controls.Add(this.label32);
            tabPage3.Controls.Add(this.label31);
            tabPage3.Controls.Add(this.checkBox1);
            tabPage3.Controls.Add(this.pictureBox1);
            tabPage3.Controls.Add(this.label28);
            tabPage3.Controls.Add(this.TxtBaseDatosSQL);
            tabPage3.Controls.Add(this.checkWSA);
            tabPage3.Controls.Add(this.label5);
            tabPage3.Controls.Add(this.label6);
            tabPage3.Controls.Add(this.label7);
            tabPage3.Controls.Add(this.TxtContrasenaSQL);
            tabPage3.Controls.Add(this.TxtUsuarioSQl);
            tabPage3.Controls.Add(this.TxtServidorSQL);
            tabPage3.Controls.Add(this.label8);
            tabPage3.Controls.Add(this.btnProbarSql);
            tabPage3.Location = new System.Drawing.Point(4, 22);
            tabPage3.Name = "tabPage3";
            tabPage3.Padding = new System.Windows.Forms.Padding(3);
            tabPage3.Size = new System.Drawing.Size(851, 403);
            tabPage3.TabIndex = 2;
            tabPage3.Click += new System.EventHandler(this.tabPage3_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Enabled = false;
            this.pictureBox4.Location = new System.Drawing.Point(555, 242);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(28, 26);
            this.pictureBox4.TabIndex = 64;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Visible = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Enabled = false;
            this.pictureBox2.Location = new System.Drawing.Point(555, 210);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(28, 26);
            this.pictureBox2.TabIndex = 63;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // CmBComparte
            // 
            this.CmBComparte.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmBComparte.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmBComparte.FormattingEnabled = true;
            this.CmBComparte.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.CmBComparte.Location = new System.Drawing.Point(283, 210);
            this.CmBComparte.Name = "CmBComparte";
            this.CmBComparte.Size = new System.Drawing.Size(266, 26);
            this.CmBComparte.TabIndex = 59;
            this.toolTip1.SetToolTip(this.CmBComparte, "Selecciona Una Base de Datos");
            this.CmBComparte.DropDown += new System.EventHandler(this.CmBComparte_DropDown);
            this.CmBComparte.SelectedIndexChanged += new System.EventHandler(this.CmBComparte_SelectedIndexChanged);
            // 
            // CmBEmpresa
            // 
            this.CmBEmpresa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmBEmpresa.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmBEmpresa.FormattingEnabled = true;
            this.CmBEmpresa.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.CmBEmpresa.Location = new System.Drawing.Point(283, 242);
            this.CmBEmpresa.Name = "CmBEmpresa";
            this.CmBEmpresa.Size = new System.Drawing.Size(266, 26);
            this.CmBEmpresa.TabIndex = 60;
            this.toolTip1.SetToolTip(this.CmBEmpresa, "Selecciona Una Base de Datos");
            this.CmBEmpresa.DropDown += new System.EventHandler(this.CmBEmpresa_DropDown);
            this.CmBEmpresa.SelectedIndexChanged += new System.EventHandler(this.CmBEmpresa_SelectedIndexChanged_1);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(112, 250);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(157, 18);
            this.label32.TabIndex = 62;
            this.label32.Text = "Código de Base de Datos:";
            this.label32.Click += new System.EventHandler(this.label32_Click);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(95, 218);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(174, 18);
            this.label31.TabIndex = 61;
            this.label31.Text = "Base de Datos de Comparte:";
            this.label31.Click += new System.EventHandler(this.label31_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.Font = new System.Drawing.Font("Trebuchet MS", 9.75F);
            this.checkBox1.Location = new System.Drawing.Point(646, 6);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox1.Size = new System.Drawing.Size(10, 24);
            this.checkBox1.TabIndex = 57;
            this.checkBox1.Text = "Transferir Derechos de Acceso";
            this.checkBox1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.Visible = false;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged_1);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Enabled = false;
            this.pictureBox1.Location = new System.Drawing.Point(555, 175);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(28, 26);
            this.pictureBox1.TabIndex = 53;
            this.pictureBox1.TabStop = false;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(277, 36);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(220, 18);
            this.label28.TabIndex = 48;
            this.label28.Text = "Seleccionar la base de datos destino";
            // 
            // TxtBaseDatosSQL
            // 
            this.TxtBaseDatosSQL.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TxtBaseDatosSQL.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBaseDatosSQL.FormattingEnabled = true;
            this.TxtBaseDatosSQL.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.TxtBaseDatosSQL.Location = new System.Drawing.Point(283, 175);
            this.TxtBaseDatosSQL.Name = "TxtBaseDatosSQL";
            this.TxtBaseDatosSQL.Size = new System.Drawing.Size(266, 26);
            this.TxtBaseDatosSQL.TabIndex = 36;
            this.toolTip1.SetToolTip(this.TxtBaseDatosSQL, "Selecciona Una Base de Datos");
            this.TxtBaseDatosSQL.DropDown += new System.EventHandler(this.TxtBaseDatosSQL_DropDown);
            this.TxtBaseDatosSQL.SelectedIndexChanged += new System.EventHandler(this.TxtBaseDatosSQL_SelectedIndexChanged);
            this.TxtBaseDatosSQL.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtBaseDatosSQL_KeyDown);
            // 
            // checkWSA
            // 
            this.checkWSA.AutoSize = true;
            this.checkWSA.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkWSA.Location = new System.Drawing.Point(607, 314);
            this.checkWSA.Name = "checkWSA";
            this.checkWSA.Size = new System.Drawing.Size(238, 21);
            this.checkWSA.TabIndex = 44;
            this.checkWSA.Text = "Windows Security Authentication";
            this.checkWSA.UseVisualStyleBackColor = true;
            this.checkWSA.Visible = false;
            this.checkWSA.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(88, 183);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(181, 18);
            this.label5.TabIndex = 40;
            this.label5.Text = "Base de Datos de la Empresa:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(190, 153);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 18);
            this.label6.TabIndex = 38;
            this.label6.Text = "Contraseña:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(212, 125);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 18);
            this.label7.TabIndex = 37;
            this.label7.Text = "Usuario:";
            // 
            // TxtContrasenaSQL
            // 
            this.TxtContrasenaSQL.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtContrasenaSQL.Location = new System.Drawing.Point(283, 147);
            this.TxtContrasenaSQL.Name = "TxtContrasenaSQL";
            this.TxtContrasenaSQL.PasswordChar = '*';
            this.TxtContrasenaSQL.Size = new System.Drawing.Size(266, 23);
            this.TxtContrasenaSQL.TabIndex = 35;
            this.toolTip1.SetToolTip(this.TxtContrasenaSQL, "Contraseña de MSSQL");
            // 
            // TxtUsuarioSQl
            // 
            this.TxtUsuarioSQl.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUsuarioSQl.Location = new System.Drawing.Point(283, 120);
            this.TxtUsuarioSQl.Name = "TxtUsuarioSQl";
            this.TxtUsuarioSQl.Size = new System.Drawing.Size(266, 23);
            this.TxtUsuarioSQl.TabIndex = 34;
            this.toolTip1.SetToolTip(this.TxtUsuarioSQl, "Usuario para Acceder a MSSQL");
            this.TxtUsuarioSQl.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // TxtServidorSQL
            // 
            this.TxtServidorSQL.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtServidorSQL.Location = new System.Drawing.Point(283, 91);
            this.TxtServidorSQL.Name = "TxtServidorSQL";
            this.TxtServidorSQL.Size = new System.Drawing.Size(266, 23);
            this.TxtServidorSQL.TabIndex = 33;
            this.toolTip1.SetToolTip(this.TxtServidorSQL, "Nombre del servidor e instancia de MSSQL");
            this.TxtServidorSQL.TextChanged += new System.EventHandler(this.TxtServidorSQL_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(207, 96);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 18);
            this.label8.TabIndex = 34;
            this.label8.Text = "Servidor:";
            // 
            // btnProbarSql
            // 
            this.btnProbarSql.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProbarSql.Image = global::Wizard_Migracion_Tress.Properties.Resources.connect_to_database;
            this.btnProbarSql.Location = new System.Drawing.Point(283, 274);
            this.btnProbarSql.Name = "btnProbarSql";
            this.btnProbarSql.Size = new System.Drawing.Size(150, 37);
            this.btnProbarSql.TabIndex = 39;
            this.btnProbarSql.Text = "Probar Conexión";
            this.btnProbarSql.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.toolTip1.SetToolTip(this.btnProbarSql, "Probar que los parametros de conexión sean correctos");
            this.btnProbarSql.UseVisualStyleBackColor = true;
            this.btnProbarSql.Click += new System.EventHandler(this.btnProbarSql_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.LabelTipoMigracion);
            this.panel1.Controls.Add(this.lblAccion);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(863, 46);
            this.panel1.TabIndex = 1;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // LabelTipoMigracion
            // 
            this.LabelTipoMigracion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelTipoMigracion.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelTipoMigracion.Location = new System.Drawing.Point(651, 8);
            this.LabelTipoMigracion.Name = "LabelTipoMigracion";
            this.LabelTipoMigracion.Size = new System.Drawing.Size(199, 29);
            this.LabelTipoMigracion.TabIndex = 1;
            this.LabelTipoMigracion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblAccion
            // 
            this.lblAccion.AutoSize = true;
            this.lblAccion.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAccion.Location = new System.Drawing.Point(30, 8);
            this.lblAccion.Name = "lblAccion";
            this.lblAccion.Size = new System.Drawing.Size(599, 29);
            this.lblAccion.TabIndex = 0;
            this.lblAccion.Text = "Asistente de Transferencia de Datos de Sistema TRESS";
            this.lblAccion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnFin);
            this.panel2.Controls.Add(this.BtnCancel);
            this.panel2.Controls.Add(this.btnSiguiente);
            this.panel2.Controls.Add(this.btAtrass);
            this.panel2.Controls.Add(this.button5);
            this.panel2.Location = new System.Drawing.Point(0, 465);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(901, 48);
            this.panel2.TabIndex = 2;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // btnFin
            // 
            this.btnFin.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFin.Location = new System.Drawing.Point(229, 1);
            this.btnFin.Name = "btnFin";
            this.btnFin.Size = new System.Drawing.Size(133, 42);
            this.btnFin.TabIndex = 7;
            this.btnFin.Text = "Finalizar";
            this.toolTip1.SetToolTip(this.btnFin, "Finalizar");
            this.btnFin.UseVisualStyleBackColor = true;
            this.btnFin.Visible = false;
            this.btnFin.Click += new System.EventHandler(this.btnFin_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Image = global::Wizard_Migracion_Tress.Properties.Resources.Cancel;
            this.BtnCancel.Location = new System.Drawing.Point(455, 1);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(133, 42);
            this.BtnCancel.TabIndex = 1001;
            this.BtnCancel.Text = "Cancelar";
            this.BtnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.toolTip1.SetToolTip(this.BtnCancel, "Cancelar y Salir");
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnSiguiente
            // 
            this.btnSiguiente.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSiguiente.Image = global::Wizard_Migracion_Tress.Properties.Resources.Actions_go_next_icon2;
            this.btnSiguiente.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSiguiente.Location = new System.Drawing.Point(725, 1);
            this.btnSiguiente.Name = "btnSiguiente";
            this.btnSiguiente.Size = new System.Drawing.Size(133, 42);
            this.btnSiguiente.TabIndex = 1003;
            this.btnSiguiente.Text = "Siguiente";
            this.btnSiguiente.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.toolTip1.SetToolTip(this.btnSiguiente, "Avanzar");
            this.btnSiguiente.UseVisualStyleBackColor = true;
            this.btnSiguiente.Click += new System.EventHandler(this.button3_Click);
            // 
            // btAtrass
            // 
            this.btAtrass.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAtrass.Image = global::Wizard_Migracion_Tress.Properties.Resources.Back;
            this.btAtrass.Location = new System.Drawing.Point(590, 1);
            this.btAtrass.Name = "btAtrass";
            this.btAtrass.Size = new System.Drawing.Size(133, 42);
            this.btAtrass.TabIndex = 1002;
            this.btAtrass.Text = "       Atras";
            this.btAtrass.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.btAtrass, "Regresar ");
            this.btAtrass.UseVisualStyleBackColor = true;
            this.btAtrass.Visible = false;
            this.btAtrass.Click += new System.EventHandler(this.button2_Click);
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Image = global::Wizard_Migracion_Tress.Properties.Resources.Cute_Ball_Help_icon;
            this.button5.Location = new System.Drawing.Point(25, 1);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(133, 42);
            this.button5.TabIndex = 1004;
            this.button5.Text = "Ayuda";
            this.button5.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.toolTip1.SetToolTip(this.button5, "Ver Ayuda");
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // imageList2
            // 
            this.imageList2.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList2.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // OpenFDFirebird
            // 
            this.OpenFDFirebird.FileOk += new System.ComponentModel.CancelEventHandler(this.OpenFDFirebird_FileOk);
            // 
            // SaveFDRuta
            // 
            this.SaveFDRuta.DefaultExt = "Txt";
            this.SaveFDRuta.FileName = "Bitacora.Txt";
            // 
            // BtnMigrarDatos
            // 
            this.BtnMigrarDatos.Location = new System.Drawing.Point(324, 194);
            this.BtnMigrarDatos.Name = "BtnMigrarDatos";
            this.BtnMigrarDatos.Size = new System.Drawing.Size(176, 42);
            this.BtnMigrarDatos.TabIndex = 3;
            this.BtnMigrarDatos.Text = "Transferir datos de empresas";
            this.toolTip1.SetToolTip(this.BtnMigrarDatos, "Transferir base de datos de una empresa");
            this.BtnMigrarDatos.UseVisualStyleBackColor = true;
            this.BtnMigrarDatos.Click += new System.EventHandler(this.BtnMigrarDatos_Click);
            // 
            // BtnMigrarComparte
            // 
            this.BtnMigrarComparte.Location = new System.Drawing.Point(324, 146);
            this.BtnMigrarComparte.Name = "BtnMigrarComparte";
            this.BtnMigrarComparte.Size = new System.Drawing.Size(176, 42);
            this.BtnMigrarComparte.TabIndex = 2;
            this.BtnMigrarComparte.Text = "Transferir datos de Comparte";
            this.toolTip1.SetToolTip(this.BtnMigrarComparte, "Transferir base de datos de comparte");
            this.BtnMigrarComparte.UseVisualStyleBackColor = true;
            this.BtnMigrarComparte.Click += new System.EventHandler(this.BtnMigrarComparte_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(556, 92);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(32, 25);
            this.button1.TabIndex = 24;
            this.button1.Text = "...";
            this.button1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.toolTip1.SetToolTip(this.button1, "Buscar Archivo");
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // TxtBaseDatosFB
            // 
            this.TxtBaseDatosFB.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBaseDatosFB.Location = new System.Drawing.Point(283, 91);
            this.TxtBaseDatosFB.Name = "TxtBaseDatosFB";
            this.TxtBaseDatosFB.Size = new System.Drawing.Size(266, 23);
            this.TxtBaseDatosFB.TabIndex = 23;
            this.toolTip1.SetToolTip(this.TxtBaseDatosFB, "Ruta a la base de datos de FireBird");
            this.TxtBaseDatosFB.TextChanged += new System.EventHandler(this.TxtBaseDatosFB_TextChanged);
            // 
            // TxtContrasenaFB
            // 
            this.TxtContrasenaFB.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtContrasenaFB.Location = new System.Drawing.Point(283, 147);
            this.TxtContrasenaFB.Name = "TxtContrasenaFB";
            this.TxtContrasenaFB.PasswordChar = '*';
            this.TxtContrasenaFB.Size = new System.Drawing.Size(266, 23);
            this.TxtContrasenaFB.TabIndex = 26;
            this.toolTip1.SetToolTip(this.TxtContrasenaFB, "Contraseña de FireBird");
            this.TxtContrasenaFB.TextChanged += new System.EventHandler(this.TxtContrasenaFB_TextChanged);
            // 
            // TxtUsuarioFB
            // 
            this.TxtUsuarioFB.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUsuarioFB.Location = new System.Drawing.Point(283, 120);
            this.TxtUsuarioFB.Name = "TxtUsuarioFB";
            this.TxtUsuarioFB.Size = new System.Drawing.Size(266, 23);
            this.TxtUsuarioFB.TabIndex = 25;
            this.TxtUsuarioFB.Text = "SYSDBA";
            this.toolTip1.SetToolTip(this.TxtUsuarioFB, "Usuario de FireBird");
            this.TxtUsuarioFB.TextChanged += new System.EventHandler(this.TxtUsuarioFB_TextChanged);
            // 
            // TxtServidorFB
            // 
            this.TxtServidorFB.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtServidorFB.Location = new System.Drawing.Point(283, 91);
            this.TxtServidorFB.Name = "TxtServidorFB";
            this.TxtServidorFB.Size = new System.Drawing.Size(266, 23);
            this.TxtServidorFB.TabIndex = 22;
            this.toolTip1.SetToolTip(this.TxtServidorFB, "Servidor donde esta instalado el motor de FireBird");
            this.TxtServidorFB.Visible = false;
            this.TxtServidorFB.TextChanged += new System.EventHandler(this.TxtServidorFB_TextChanged);
            // 
            // BtnProbarFirebird
            // 
            this.BtnProbarFirebird.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnProbarFirebird.Image = global::Wizard_Migracion_Tress.Properties.Resources.connect_to_database;
            this.BtnProbarFirebird.Location = new System.Drawing.Point(284, 176);
            this.BtnProbarFirebird.Name = "BtnProbarFirebird";
            this.BtnProbarFirebird.Size = new System.Drawing.Size(150, 37);
            this.BtnProbarFirebird.TabIndex = 27;
            this.BtnProbarFirebird.Text = "Probar Conexión";
            this.BtnProbarFirebird.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.toolTip1.SetToolTip(this.BtnProbarFirebird, "Probar que los datos de conexión sean correctos");
            this.BtnProbarFirebird.UseVisualStyleBackColor = true;
            this.BtnProbarFirebird.Click += new System.EventHandler(this.BtnProbarFirebird_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(10, 4);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(296, 18);
            this.label30.TabIndex = 39;
            this.label30.Tag = "";
            this.label30.Text = "Bitácora del proceso de transferencia de datos";
            this.toolTip1.SetToolTip(this.label30, "Seleccion de ruta para guardar la bitácora de los procesos de la transferencia(La" +
        " ruta predeterminada es la ruta donde se encuentra el ejecutable)");
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.White;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(646, 24);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(33, 23);
            this.button2.TabIndex = 38;
            this.button2.Text = "...";
            this.button2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.toolTip1.SetToolTip(this.button2, "Seleccionar ruta donde guardar la bitácora");
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // TxtRuta
            // 
            this.TxtRuta.Enabled = false;
            this.TxtRuta.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRuta.Location = new System.Drawing.Point(55, 25);
            this.TxtRuta.Name = "TxtRuta";
            this.TxtRuta.Size = new System.Drawing.Size(585, 23);
            this.TxtRuta.TabIndex = 37;
            this.toolTip1.SetToolTip(this.TxtRuta, "Ruta donde se guardara la bitácora");
            this.TxtRuta.TextChanged += new System.EventHandler(this.TxtRuta_TextChanged_1);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(10, 59);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(71, 18);
            this.label29.TabIndex = 3;
            this.label29.Tag = "";
            this.label29.Text = "Préstamos";
            this.toolTip1.SetToolTip(this.label29, "Opcion para Reconstruir los tipos de Prestamos que hayan sido Eliminados\r\n(el no " +
        "Reconstruirlos Impedira que los Prestamos Almacenadso con el tipo Eliminado no s" +
        "ean Transferidos)");
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(10, 4);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(139, 18);
            this.label11.TabIndex = 1;
            this.label11.Tag = "";
            this.label11.Text = "Kardex de Empleados";
            this.toolTip1.SetToolTip(this.label11, "Opcion para Reconstruir los tipos de Kardex que hayan sido Eliminados\r\n(el no Rec" +
        "onstruirlos Impedira que Kardex Almacenadso con el tipo Eliminado no sean Transf" +
        "eridos)");
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(10, 4);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(144, 18);
            this.label10.TabIndex = 1;
            this.label10.Tag = "";
            this.label10.Text = "Tarjetas de Asistencia";
            this.toolTip1.SetToolTip(this.label10, "Opcion para Limitar Los registros de Asistencia de los Empleados");
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(10, 4);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(133, 18);
            this.label9.TabIndex = 0;
            this.label9.Tag = "";
            this.label9.Text = "Bitácora del Sistema";
            this.toolTip1.SetToolTip(this.label9, "Opcion para limitar la Bitácora de Asistencia del Sistema");
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.White;
            this.button3.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(555, 90);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(32, 23);
            this.button3.TabIndex = 34;
            this.button3.Text = "...";
            this.button3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.toolTip1.SetToolTip(this.button3, "Buscar Archivo");
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click_2);
            // 
            // txtfbBaseComparte
            // 
            this.txtfbBaseComparte.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfbBaseComparte.Location = new System.Drawing.Point(283, 90);
            this.txtfbBaseComparte.Name = "txtfbBaseComparte";
            this.txtfbBaseComparte.Size = new System.Drawing.Size(266, 23);
            this.txtfbBaseComparte.TabIndex = 33;
            this.toolTip1.SetToolTip(this.txtfbBaseComparte, "Ruta a la base de datos de FireBird");
            // 
            // TxtFbPassComparte
            // 
            this.TxtFbPassComparte.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFbPassComparte.Location = new System.Drawing.Point(283, 148);
            this.TxtFbPassComparte.Name = "TxtFbPassComparte";
            this.TxtFbPassComparte.PasswordChar = '*';
            this.TxtFbPassComparte.Size = new System.Drawing.Size(266, 23);
            this.TxtFbPassComparte.TabIndex = 36;
            this.toolTip1.SetToolTip(this.TxtFbPassComparte, "Contraseña de FireBird");
            // 
            // txtfbUserComparte
            // 
            this.txtfbUserComparte.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfbUserComparte.Location = new System.Drawing.Point(283, 119);
            this.txtfbUserComparte.Name = "txtfbUserComparte";
            this.txtfbUserComparte.Size = new System.Drawing.Size(266, 23);
            this.txtfbUserComparte.TabIndex = 35;
            this.txtfbUserComparte.Text = "SYSDBA";
            this.toolTip1.SetToolTip(this.txtfbUserComparte, "Usuario de FireBird");
            // 
            // txtfbServerComparte
            // 
            this.txtfbServerComparte.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfbServerComparte.Location = new System.Drawing.Point(283, 91);
            this.txtfbServerComparte.Name = "txtfbServerComparte";
            this.txtfbServerComparte.Size = new System.Drawing.Size(266, 23);
            this.txtfbServerComparte.TabIndex = 30;
            this.toolTip1.SetToolTip(this.txtfbServerComparte, "Servidor donde esta instalado el motor de FireBird");
            this.txtfbServerComparte.Visible = false;
            // 
            // btnConeccionComparte
            // 
            this.btnConeccionComparte.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConeccionComparte.Image = global::Wizard_Migracion_Tress.Properties.Resources.connect_to_database;
            this.btnConeccionComparte.Location = new System.Drawing.Point(283, 181);
            this.btnConeccionComparte.Name = "btnConeccionComparte";
            this.btnConeccionComparte.Size = new System.Drawing.Size(150, 37);
            this.btnConeccionComparte.TabIndex = 37;
            this.btnConeccionComparte.Text = "Probar Conexión";
            this.btnConeccionComparte.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.toolTip1.SetToolTip(this.btnConeccionComparte, "Probar que los datos de conexión sean correctos");
            this.btnConeccionComparte.UseVisualStyleBackColor = true;
            this.btnConeccionComparte.Click += new System.EventHandler(this.btnConeccionComparte_Click);
            // 
            // DropDownSqlBaseComparte
            // 
            this.DropDownSqlBaseComparte.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.DropDownSqlBaseComparte.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DropDownSqlBaseComparte.FormattingEnabled = true;
            this.DropDownSqlBaseComparte.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.DropDownSqlBaseComparte.Location = new System.Drawing.Point(283, 176);
            this.DropDownSqlBaseComparte.Name = "DropDownSqlBaseComparte";
            this.DropDownSqlBaseComparte.Size = new System.Drawing.Size(266, 26);
            this.DropDownSqlBaseComparte.TabIndex = 60;
            this.toolTip1.SetToolTip(this.DropDownSqlBaseComparte, "Selecciona Una Base de Datos");
            this.DropDownSqlBaseComparte.DropDown += new System.EventHandler(this.DropDownSqlBaseComparte_DropDown);
            this.DropDownSqlBaseComparte.SelectedIndexChanged += new System.EventHandler(this.DropDownSqlBaseComparte_SelectedIndexChanged);
            // 
            // TxtsqlPassComparte
            // 
            this.TxtsqlPassComparte.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtsqlPassComparte.Location = new System.Drawing.Point(283, 147);
            this.TxtsqlPassComparte.Name = "TxtsqlPassComparte";
            this.TxtsqlPassComparte.PasswordChar = '*';
            this.TxtsqlPassComparte.Size = new System.Drawing.Size(266, 23);
            this.TxtsqlPassComparte.TabIndex = 58;
            this.toolTip1.SetToolTip(this.TxtsqlPassComparte, "Contraseña de MSSQL");
            // 
            // TxtsqlUsuarioComparte
            // 
            this.TxtsqlUsuarioComparte.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtsqlUsuarioComparte.Location = new System.Drawing.Point(283, 120);
            this.TxtsqlUsuarioComparte.Name = "TxtsqlUsuarioComparte";
            this.TxtsqlUsuarioComparte.Size = new System.Drawing.Size(266, 23);
            this.TxtsqlUsuarioComparte.TabIndex = 56;
            this.toolTip1.SetToolTip(this.TxtsqlUsuarioComparte, "Usuario para Acceder a MSSQL");
            // 
            // txtSqlServerComparte
            // 
            this.txtSqlServerComparte.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSqlServerComparte.Location = new System.Drawing.Point(283, 91);
            this.txtSqlServerComparte.Name = "txtSqlServerComparte";
            this.txtSqlServerComparte.Size = new System.Drawing.Size(266, 23);
            this.txtSqlServerComparte.TabIndex = 55;
            this.toolTip1.SetToolTip(this.txtSqlServerComparte, "Nombre del servidor e instancia de MSSQL");
            // 
            // btnConeccionSqlComparte
            // 
            this.btnConeccionSqlComparte.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConeccionSqlComparte.Image = global::Wizard_Migracion_Tress.Properties.Resources.connect_to_database;
            this.btnConeccionSqlComparte.Location = new System.Drawing.Point(283, 208);
            this.btnConeccionSqlComparte.Name = "btnConeccionSqlComparte";
            this.btnConeccionSqlComparte.Size = new System.Drawing.Size(150, 37);
            this.btnConeccionSqlComparte.TabIndex = 62;
            this.btnConeccionSqlComparte.Text = "Probar Conexión";
            this.btnConeccionSqlComparte.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.toolTip1.SetToolTip(this.btnConeccionSqlComparte, "Probar que los parametros de conexión sean correctos");
            this.btnConeccionSqlComparte.UseVisualStyleBackColor = true;
            this.btnConeccionSqlComparte.Click += new System.EventHandler(this.btnConeccionSqlComparte_Click);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(10, 4);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(296, 18);
            this.label44.TabIndex = 39;
            this.label44.Tag = "";
            this.label44.Text = "Bitácora del proceso de transferencia de datos";
            this.toolTip1.SetToolTip(this.label44, "Seleccion de ruta para guardar la bitácora de los procesos de la transferencia(La" +
        " ruta predeterminada es la ruta donde se encuentra el ejecutable)");
            // 
            // ButtonBitacoraComparte
            // 
            this.ButtonBitacoraComparte.BackColor = System.Drawing.Color.White;
            this.ButtonBitacoraComparte.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.ButtonBitacoraComparte.FlatAppearance.BorderSize = 0;
            this.ButtonBitacoraComparte.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.ButtonBitacoraComparte.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.ButtonBitacoraComparte.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ButtonBitacoraComparte.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonBitacoraComparte.Location = new System.Drawing.Point(646, 24);
            this.ButtonBitacoraComparte.Name = "ButtonBitacoraComparte";
            this.ButtonBitacoraComparte.Size = new System.Drawing.Size(33, 23);
            this.ButtonBitacoraComparte.TabIndex = 38;
            this.ButtonBitacoraComparte.Text = "...";
            this.ButtonBitacoraComparte.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ButtonBitacoraComparte.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.toolTip1.SetToolTip(this.ButtonBitacoraComparte, "Seleccionar ruta donde guardar la bitácora");
            this.ButtonBitacoraComparte.UseVisualStyleBackColor = false;
            this.ButtonBitacoraComparte.Click += new System.EventHandler(this.ButtonBitacoraComparte_Click);
            // 
            // TxtRutaBitacoraComparte
            // 
            this.TxtRutaBitacoraComparte.Enabled = false;
            this.TxtRutaBitacoraComparte.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRutaBitacoraComparte.Location = new System.Drawing.Point(55, 25);
            this.TxtRutaBitacoraComparte.Name = "TxtRutaBitacoraComparte";
            this.TxtRutaBitacoraComparte.Size = new System.Drawing.Size(585, 23);
            this.TxtRutaBitacoraComparte.TabIndex = 37;
            this.toolTip1.SetToolTip(this.TxtRutaBitacoraComparte, "Ruta donde se guardara la bitácora");
            // 
            // helpProvider1
            // 
            this.helpProvider1.HelpNamespace = "Transferencia.chm";
            // 
            // WizardPages1
            // 
            this.WizardPages1.Controls.Add(this.step1);
            this.WizardPages1.Controls.Add(this.step2);
            this.WizardPages1.Controls.Add(tabPage3);
            this.WizardPages1.Controls.Add(this.tabPage4);
            this.WizardPages1.Controls.Add(this.tabPage6);
            this.WizardPages1.Controls.Add(this.tabPage7);
            this.WizardPages1.Controls.Add(this.TabComparteFb);
            this.WizardPages1.Controls.Add(this.TabComparteSql);
            this.WizardPages1.Controls.Add(this.TabComparteParametros);
            this.WizardPages1.Controls.Add(this.TabComparteResumen);
            this.WizardPages1.Location = new System.Drawing.Point(0, 41);
            this.WizardPages1.Multiline = true;
            this.WizardPages1.Name = "WizardPages1";
            this.WizardPages1.SelectedIndex = 0;
            this.WizardPages1.Size = new System.Drawing.Size(859, 429);
            this.WizardPages1.TabIndex = 0;
            this.WizardPages1.SelectedIndexChanged += new System.EventHandler(this.wizardPages1_SelectedIndexChanged);
            // 
            // step1
            // 
            this.step1.BackColor = System.Drawing.Color.White;
            this.step1.Controls.Add(this.BtnMigrarDatos);
            this.step1.Controls.Add(this.BtnMigrarComparte);
            this.step1.Controls.Add(this.textBox1);
            this.step1.Location = new System.Drawing.Point(4, 22);
            this.step1.Name = "step1";
            this.step1.Padding = new System.Windows.Forms.Padding(3);
            this.step1.Size = new System.Drawing.Size(851, 403);
            this.step1.TabIndex = 0;
            this.step1.Text = "Step 1";
            this.step1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.White;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Enabled = false;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(73, 63);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(681, 44);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "Seleccione el tipo de Transferencia a realizar";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // step2
            // 
            this.step2.BackColor = System.Drawing.Color.White;
            this.step2.Controls.Add(this.label27);
            this.step2.Controls.Add(this.button1);
            this.step2.Controls.Add(this.localcheck);
            this.step2.Controls.Add(this.label4);
            this.step2.Controls.Add(this.TxtBaseDatosFB);
            this.step2.Controls.Add(this.TxtContrasenaFB);
            this.step2.Controls.Add(this.TxtUsuarioFB);
            this.step2.Controls.Add(this.TxtServidorFB);
            this.step2.Controls.Add(this.label3);
            this.step2.Controls.Add(this.label2);
            this.step2.Controls.Add(this.BtnProbarFirebird);
            this.step2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.step2.Location = new System.Drawing.Point(4, 22);
            this.step2.Name = "step2";
            this.step2.Padding = new System.Windows.Forms.Padding(3);
            this.step2.Size = new System.Drawing.Size(851, 403);
            this.step2.TabIndex = 1;
            this.step2.Text = "tabPage2";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(281, 38);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(287, 18);
            this.label27.TabIndex = 46;
            this.label27.Text = "Seleccionar la base de datos fuente de Empresa";
            this.label27.Click += new System.EventHandler(this.label27_Click);
            // 
            // localcheck
            // 
            this.localcheck.AutoSize = true;
            this.localcheck.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.localcheck.Location = new System.Drawing.Point(594, 95);
            this.localcheck.Name = "localcheck";
            this.localcheck.Size = new System.Drawing.Size(87, 22);
            this.localcheck.TabIndex = 27;
            this.localcheck.Text = "Localhost:";
            this.localcheck.UseVisualStyleBackColor = true;
            this.localcheck.Visible = false;
            this.localcheck.CheckedChanged += new System.EventHandler(this.localcheck_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(174, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 18);
            this.label4.TabIndex = 29;
            this.label4.Text = "Base de Datos:";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(190, 153);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 18);
            this.label3.TabIndex = 27;
            this.label3.Text = "Contraseña:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(212, 125);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 18);
            this.label2.TabIndex = 26;
            this.label2.Text = "Usuario:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.White;
            this.tabPage4.Controls.Add(this.panel8);
            this.tabPage4.Controls.Add(this.panel7);
            this.tabPage4.Controls.Add(this.panel6);
            this.tabPage4.Controls.Add(this.panel5);
            this.tabPage4.Controls.Add(this.panel4);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(851, 403);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "tabPage4";
            this.tabPage4.Click += new System.EventHandler(this.tabPage4_Click);
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.label30);
            this.panel8.Controls.Add(this.button2);
            this.panel8.Controls.Add(this.TxtRuta);
            this.panel8.Controls.Add(this.label16);
            this.panel8.Location = new System.Drawing.Point(75, 310);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(703, 67);
            this.panel8.TabIndex = 36;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(13, 25);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(40, 18);
            this.label16.TabIndex = 36;
            this.label16.Text = "Ruta:";
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.label23);
            this.panel7.Controls.Add(this.TxtTablaInicial);
            this.panel7.Controls.Add(this.RadBtnMigracionParcial);
            this.panel7.Controls.Add(this.RadBtnMigracionCompleta);
            this.panel7.Controls.Add(this.label25);
            this.panel7.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel7.Location = new System.Drawing.Point(6, 6);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(10, 10);
            this.panel7.TabIndex = 3;
            this.panel7.Visible = false;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(194, 56);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(73, 16);
            this.label23.TabIndex = 4;
            this.label23.Text = "Tabla Inicial";
            // 
            // TxtTablaInicial
            // 
            this.TxtTablaInicial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtTablaInicial.Enabled = false;
            this.TxtTablaInicial.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTablaInicial.Location = new System.Drawing.Point(283, 45);
            this.TxtTablaInicial.Name = "TxtTablaInicial";
            this.TxtTablaInicial.Size = new System.Drawing.Size(214, 27);
            this.TxtTablaInicial.TabIndex = 3;
            this.TxtTablaInicial.Leave += new System.EventHandler(this.TxtTablaInicial_Leave);
            this.TxtTablaInicial.MouseEnter += new System.EventHandler(this.TxtTablaInicial_MouseEnter);
            // 
            // RadBtnMigracionParcial
            // 
            this.RadBtnMigracionParcial.AutoSize = true;
            this.RadBtnMigracionParcial.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadBtnMigracionParcial.Location = new System.Drawing.Point(97, 41);
            this.RadBtnMigracionParcial.Name = "RadBtnMigracionParcial";
            this.RadBtnMigracionParcial.Size = new System.Drawing.Size(63, 20);
            this.RadBtnMigracionParcial.TabIndex = 2;
            this.RadBtnMigracionParcial.Text = "Parcial";
            this.RadBtnMigracionParcial.UseVisualStyleBackColor = true;
            this.RadBtnMigracionParcial.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged_1);
            // 
            // RadBtnMigracionCompleta
            // 
            this.RadBtnMigracionCompleta.AutoSize = true;
            this.RadBtnMigracionCompleta.Checked = true;
            this.RadBtnMigracionCompleta.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadBtnMigracionCompleta.Location = new System.Drawing.Point(13, 41);
            this.RadBtnMigracionCompleta.Name = "RadBtnMigracionCompleta";
            this.RadBtnMigracionCompleta.Size = new System.Drawing.Size(81, 20);
            this.RadBtnMigracionCompleta.TabIndex = 1;
            this.RadBtnMigracionCompleta.TabStop = true;
            this.RadBtnMigracionCompleta.Text = "Completa";
            this.RadBtnMigracionCompleta.UseVisualStyleBackColor = true;
            this.RadBtnMigracionCompleta.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(10, 4);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(148, 16);
            this.label25.TabIndex = 0;
            this.label25.Text = "Tipo de Transferencia";
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.CheckPrestamosAgregar);
            this.panel6.Controls.Add(this.label29);
            this.panel6.Controls.Add(this.CheckKardexAgregar);
            this.panel6.Controls.Add(this.label11);
            this.panel6.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel6.Location = new System.Drawing.Point(75, 184);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(703, 120);
            this.panel6.TabIndex = 2;
            // 
            // CheckPrestamosAgregar
            // 
            this.CheckPrestamosAgregar.AutoSize = true;
            this.CheckPrestamosAgregar.Checked = true;
            this.CheckPrestamosAgregar.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckPrestamosAgregar.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CheckPrestamosAgregar.Location = new System.Drawing.Point(14, 80);
            this.CheckPrestamosAgregar.Name = "CheckPrestamosAgregar";
            this.CheckPrestamosAgregar.Size = new System.Drawing.Size(216, 20);
            this.CheckPrestamosAgregar.TabIndex = 4;
            this.CheckPrestamosAgregar.Text = "Agregar Tipos de Préstamos Faltantes";
            this.CheckPrestamosAgregar.UseVisualStyleBackColor = true;
            // 
            // CheckKardexAgregar
            // 
            this.CheckKardexAgregar.AutoSize = true;
            this.CheckKardexAgregar.Checked = true;
            this.CheckKardexAgregar.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckKardexAgregar.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CheckKardexAgregar.Location = new System.Drawing.Point(13, 25);
            this.CheckKardexAgregar.Name = "CheckKardexAgregar";
            this.CheckKardexAgregar.Size = new System.Drawing.Size(199, 20);
            this.CheckKardexAgregar.TabIndex = 2;
            this.CheckKardexAgregar.Text = "Agregar Tipos de Kardex Faltantes";
            this.CheckKardexAgregar.UseVisualStyleBackColor = true;
            this.CheckKardexAgregar.CheckedChanged += new System.EventHandler(this.CheckKardexAgregar_CheckedChanged);
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.FechaFinAsistencia);
            this.panel5.Controls.Add(this.RadBtnAsistenciaRango);
            this.panel5.Controls.Add(this.FechaIniAsistencia);
            this.panel5.Controls.Add(this.RadBtnAsistenciaCompleta);
            this.panel5.Controls.Add(this.label14);
            this.panel5.Controls.Add(this.label10);
            this.panel5.Controls.Add(this.label15);
            this.panel5.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel5.Location = new System.Drawing.Point(75, 118);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(703, 60);
            this.panel5.TabIndex = 1;
            // 
            // FechaFinAsistencia
            // 
            this.FechaFinAsistencia.CustomFormat = "  dd/MM/yyyy";
            this.FechaFinAsistencia.Enabled = false;
            this.FechaFinAsistencia.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FechaFinAsistencia.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.FechaFinAsistencia.Location = new System.Drawing.Point(534, 23);
            this.FechaFinAsistencia.Name = "FechaFinAsistencia";
            this.FechaFinAsistencia.Size = new System.Drawing.Size(145, 20);
            this.FechaFinAsistencia.TabIndex = 9;
            this.FechaFinAsistencia.ValueChanged += new System.EventHandler(this.FechaFinAsistencia_ValueChanged);
            // 
            // RadBtnAsistenciaRango
            // 
            this.RadBtnAsistenciaRango.AutoSize = true;
            this.RadBtnAsistenciaRango.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadBtnAsistenciaRango.Location = new System.Drawing.Point(97, 25);
            this.RadBtnAsistenciaRango.Name = "RadBtnAsistenciaRango";
            this.RadBtnAsistenciaRango.Size = new System.Drawing.Size(109, 20);
            this.RadBtnAsistenciaRango.TabIndex = 3;
            this.RadBtnAsistenciaRango.Text = "Rango de Fechas";
            this.RadBtnAsistenciaRango.UseVisualStyleBackColor = true;
            this.RadBtnAsistenciaRango.CheckedChanged += new System.EventHandler(this.radioButton4_CheckedChanged);
            // 
            // FechaIniAsistencia
            // 
            this.FechaIniAsistencia.CustomFormat = "  dd/MM/yyyy";
            this.FechaIniAsistencia.Enabled = false;
            this.FechaIniAsistencia.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FechaIniAsistencia.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.FechaIniAsistencia.Location = new System.Drawing.Point(299, 23);
            this.FechaIniAsistencia.Name = "FechaIniAsistencia";
            this.FechaIniAsistencia.Size = new System.Drawing.Size(142, 20);
            this.FechaIniAsistencia.TabIndex = 8;
            this.FechaIniAsistencia.ValueChanged += new System.EventHandler(this.FechaIniAsistencia_ValueChanged);
            // 
            // RadBtnAsistenciaCompleta
            // 
            this.RadBtnAsistenciaCompleta.AutoSize = true;
            this.RadBtnAsistenciaCompleta.Checked = true;
            this.RadBtnAsistenciaCompleta.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadBtnAsistenciaCompleta.Location = new System.Drawing.Point(10, 25);
            this.RadBtnAsistenciaCompleta.Name = "RadBtnAsistenciaCompleta";
            this.RadBtnAsistenciaCompleta.Size = new System.Drawing.Size(72, 20);
            this.RadBtnAsistenciaCompleta.TabIndex = 2;
            this.RadBtnAsistenciaCompleta.TabStop = true;
            this.RadBtnAsistenciaCompleta.Text = "Completa";
            this.RadBtnAsistenciaCompleta.UseVisualStyleBackColor = true;
            this.RadBtnAsistenciaCompleta.CheckedChanged += new System.EventHandler(this.RadBtnAsistenciaCompleta_CheckedChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.label14.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(489, 23);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(20, 16);
            this.label14.TabIndex = 7;
            this.label14.Text = "Al:";
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(243, 27);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(27, 16);
            this.label15.TabIndex = 6;
            this.label15.Text = "Del:";
            this.label15.Click += new System.EventHandler(this.label15_Click);
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.FechaFinBitacora);
            this.panel4.Controls.Add(this.FechaIniBitacora);
            this.panel4.Controls.Add(this.label13);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.RadBtnBitacoraRango);
            this.panel4.Controls.Add(this.radBtnBitacoraCompleta);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel4.Location = new System.Drawing.Point(75, 52);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(703, 60);
            this.panel4.TabIndex = 0;
            this.panel4.Paint += new System.Windows.Forms.PaintEventHandler(this.panel4_Paint);
            // 
            // FechaFinBitacora
            // 
            this.FechaFinBitacora.CustomFormat = "  dd/MM/yyyy";
            this.FechaFinBitacora.Enabled = false;
            this.FechaFinBitacora.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FechaFinBitacora.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.FechaFinBitacora.Location = new System.Drawing.Point(534, 25);
            this.FechaFinBitacora.Name = "FechaFinBitacora";
            this.FechaFinBitacora.Size = new System.Drawing.Size(145, 20);
            this.FechaFinBitacora.TabIndex = 5;
            this.FechaFinBitacora.ValueChanged += new System.EventHandler(this.FechaFinBitacora_ValueChanged);
            // 
            // FechaIniBitacora
            // 
            this.FechaIniBitacora.CustomFormat = "  dd/MM/yyyy";
            this.FechaIniBitacora.Enabled = false;
            this.FechaIniBitacora.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FechaIniBitacora.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.FechaIniBitacora.Location = new System.Drawing.Point(299, 23);
            this.FechaIniBitacora.Name = "FechaIniBitacora";
            this.FechaIniBitacora.Size = new System.Drawing.Size(142, 20);
            this.FechaIniBitacora.TabIndex = 4;
            this.FechaIniBitacora.ValueChanged += new System.EventHandler(this.FechaIniBitacora_ValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.label13.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(489, 29);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(20, 16);
            this.label13.TabIndex = 4;
            this.label13.Text = "Al:";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(243, 29);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(27, 16);
            this.label12.TabIndex = 3;
            this.label12.Text = "Del:";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // RadBtnBitacoraRango
            // 
            this.RadBtnBitacoraRango.AutoSize = true;
            this.RadBtnBitacoraRango.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadBtnBitacoraRango.Location = new System.Drawing.Point(97, 25);
            this.RadBtnBitacoraRango.Name = "RadBtnBitacoraRango";
            this.RadBtnBitacoraRango.Size = new System.Drawing.Size(112, 20);
            this.RadBtnBitacoraRango.TabIndex = 2;
            this.RadBtnBitacoraRango.Text = "Rango de Fechas ";
            this.RadBtnBitacoraRango.UseVisualStyleBackColor = true;
            this.RadBtnBitacoraRango.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radBtnBitacoraCompleta
            // 
            this.radBtnBitacoraCompleta.AutoSize = true;
            this.radBtnBitacoraCompleta.Checked = true;
            this.radBtnBitacoraCompleta.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radBtnBitacoraCompleta.Location = new System.Drawing.Point(10, 25);
            this.radBtnBitacoraCompleta.Name = "radBtnBitacoraCompleta";
            this.radBtnBitacoraCompleta.Size = new System.Drawing.Size(72, 20);
            this.radBtnBitacoraCompleta.TabIndex = 1;
            this.radBtnBitacoraCompleta.TabStop = true;
            this.radBtnBitacoraCompleta.Text = "Completa";
            this.radBtnBitacoraCompleta.UseVisualStyleBackColor = true;
            this.radBtnBitacoraCompleta.CheckedChanged += new System.EventHandler(this.radBtnBitacoraCompleta_CheckedChanged);
            // 
            // tabPage6
            // 
            this.tabPage6.BackColor = System.Drawing.Color.White;
            this.tabPage6.Controls.Add(this.lblCodigo);
            this.tabPage6.Controls.Add(this.label33);
            this.tabPage6.Controls.Add(this.lblRuta);
            this.tabPage6.Controls.Add(this.bdCorpText);
            this.tabPage6.Controls.Add(this.bdProtext);
            this.tabPage6.Controls.Add(this.label24);
            this.tabPage6.Controls.Add(this.label26);
            this.tabPage6.Controls.Add(this.lblKardex);
            this.tabPage6.Controls.Add(this.lblTarjetas);
            this.tabPage6.Controls.Add(this.lblBitacora);
            this.tabPage6.Controls.Add(this.lblDestino);
            this.tabPage6.Controls.Add(this.lblOrigen);
            this.tabPage6.Controls.Add(this.label22);
            this.tabPage6.Controls.Add(this.label21);
            this.tabPage6.Controls.Add(this.label20);
            this.tabPage6.Controls.Add(this.label19);
            this.tabPage6.Controls.Add(this.label18);
            this.tabPage6.Controls.Add(this.label17);
            this.tabPage6.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(851, 403);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Pendiente";
            this.tabPage6.Click += new System.EventHandler(this.tabPage6_Click);
            // 
            // lblCodigo
            // 
            this.lblCodigo.AutoSize = true;
            this.lblCodigo.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(235, 357);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(31, 18);
            this.lblCodigo.TabIndex = 48;
            this.lblCodigo.Tag = "";
            this.lblCodigo.Text = "N/A";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
            this.label33.Location = new System.Drawing.Point(0, 343);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(229, 44);
            this.label33.TabIndex = 44;
            this.label33.Tag = "";
            this.label33.Text = "Origen de Derechos a \r\nClasificaciones de Reportes :";
            this.label33.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblRuta
            // 
            this.lblRuta.BackColor = System.Drawing.Color.White;
            this.lblRuta.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblRuta.Enabled = false;
            this.lblRuta.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRuta.ForeColor = System.Drawing.Color.Black;
            this.lblRuta.Location = new System.Drawing.Point(235, 276);
            this.lblRuta.Multiline = true;
            this.lblRuta.Name = "lblRuta";
            this.lblRuta.Size = new System.Drawing.Size(598, 59);
            this.lblRuta.TabIndex = 40;
            // 
            // bdCorpText
            // 
            this.bdCorpText.BackColor = System.Drawing.Color.White;
            this.bdCorpText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.bdCorpText.Enabled = false;
            this.bdCorpText.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bdCorpText.ForeColor = System.Drawing.Color.Black;
            this.bdCorpText.Location = new System.Drawing.Point(235, 61);
            this.bdCorpText.Multiline = true;
            this.bdCorpText.Name = "bdCorpText";
            this.bdCorpText.Size = new System.Drawing.Size(598, 49);
            this.bdCorpText.TabIndex = 39;
            this.bdCorpText.WordWrap = false;
            // 
            // bdProtext
            // 
            this.bdProtext.BackColor = System.Drawing.Color.White;
            this.bdProtext.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.bdProtext.Enabled = false;
            this.bdProtext.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bdProtext.ForeColor = System.Drawing.Color.Black;
            this.bdProtext.Location = new System.Drawing.Point(238, 6);
            this.bdProtext.Multiline = true;
            this.bdProtext.Name = "bdProtext";
            this.bdProtext.Size = new System.Drawing.Size(595, 49);
            this.bdProtext.TabIndex = 38;
            this.bdProtext.WordWrap = false;
            this.bdProtext.Enter += new System.EventHandler(this.textBox2_Enter);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(594, 390);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(75, 17);
            this.label24.TabIndex = 37;
            this.label24.Text = "Completa";
            this.label24.Visible = false;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(435, 391);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(156, 16);
            this.label26.TabIndex = 36;
            this.label26.Text = "Tipo de Transferencia: ";
            this.label26.Visible = false;
            // 
            // lblKardex
            // 
            this.lblKardex.AutoSize = true;
            this.lblKardex.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKardex.Location = new System.Drawing.Point(235, 243);
            this.lblKardex.Name = "lblKardex";
            this.lblKardex.Size = new System.Drawing.Size(94, 18);
            this.lblKardex.TabIndex = 34;
            this.lblKardex.Text = "No Hacer Nada";
            // 
            // lblTarjetas
            // 
            this.lblTarjetas.AutoSize = true;
            this.lblTarjetas.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTarjetas.Location = new System.Drawing.Point(232, 185);
            this.lblTarjetas.Name = "lblTarjetas";
            this.lblTarjetas.Size = new System.Drawing.Size(66, 18);
            this.lblTarjetas.TabIndex = 33;
            this.lblTarjetas.Text = "Pendiente";
            // 
            // lblBitacora
            // 
            this.lblBitacora.AutoSize = true;
            this.lblBitacora.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBitacora.Location = new System.Drawing.Point(232, 132);
            this.lblBitacora.Name = "lblBitacora";
            this.lblBitacora.Size = new System.Drawing.Size(66, 18);
            this.lblBitacora.TabIndex = 32;
            this.lblBitacora.Text = "Pendiente";
            // 
            // lblDestino
            // 
            this.lblDestino.AutoSize = true;
            this.lblDestino.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDestino.Location = new System.Drawing.Point(497, 132);
            this.lblDestino.Name = "lblDestino";
            this.lblDestino.Size = new System.Drawing.Size(73, 17);
            this.lblDestino.TabIndex = 31;
            this.lblDestino.Text = "Pendiente";
            this.lblDestino.Visible = false;
            // 
            // lblOrigen
            // 
            this.lblOrigen.AutoSize = true;
            this.lblOrigen.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrigen.Location = new System.Drawing.Point(486, 149);
            this.lblOrigen.Name = "lblOrigen";
            this.lblOrigen.Size = new System.Drawing.Size(73, 17);
            this.lblOrigen.TabIndex = 30;
            this.lblOrigen.Text = "Pendiente";
            this.lblOrigen.Visible = false;
            this.lblOrigen.Click += new System.EventHandler(this.lblOrigen_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(81, 297);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(148, 22);
            this.label22.TabIndex = 29;
            this.label22.Text = "Ruta de Bitácora :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(48, 239);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(181, 22);
            this.label21.TabIndex = 28;
            this.label21.Text = "Kardex de Empleados :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(40, 185);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(189, 22);
            this.label20.TabIndex = 27;
            this.label20.Text = "Tarjetas de Asistencia :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(57, 129);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(172, 22);
            this.label19.TabIndex = 26;
            this.label19.Text = "Bitácora de Sistema :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(50, 68);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(179, 22);
            this.label18.TabIndex = 25;
            this.label18.Text = "Base de Datos MSSQL :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(37, 19);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(192, 22);
            this.label17.TabIndex = 24;
            this.label17.Text = "Base de Datos FireBird :";
            // 
            // tabPage7
            // 
            this.tabPage7.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(851, 403);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "tabPage7";
            this.tabPage7.Click += new System.EventHandler(this.tabPage7_Click);
            // 
            // TabComparteFb
            // 
            this.TabComparteFb.Controls.Add(this.label39);
            this.TabComparteFb.Controls.Add(this.button3);
            this.TabComparteFb.Controls.Add(this.checkBox2);
            this.TabComparteFb.Controls.Add(this.label35);
            this.TabComparteFb.Controls.Add(this.txtfbBaseComparte);
            this.TabComparteFb.Controls.Add(this.TxtFbPassComparte);
            this.TabComparteFb.Controls.Add(this.txtfbUserComparte);
            this.TabComparteFb.Controls.Add(this.txtfbServerComparte);
            this.TabComparteFb.Controls.Add(this.label36);
            this.TabComparteFb.Controls.Add(this.label37);
            this.TabComparteFb.Controls.Add(this.btnConeccionComparte);
            this.TabComparteFb.Location = new System.Drawing.Point(4, 22);
            this.TabComparteFb.Name = "TabComparteFb";
            this.TabComparteFb.Padding = new System.Windows.Forms.Padding(3);
            this.TabComparteFb.Size = new System.Drawing.Size(851, 403);
            this.TabComparteFb.TabIndex = 7;
            this.TabComparteFb.Text = "tabPage1";
            this.TabComparteFb.UseVisualStyleBackColor = true;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(305, 35);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(215, 18);
            this.label39.TabIndex = 47;
            this.label39.Text = "Seleccionar la base de datos fuente";
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox2.Location = new System.Drawing.Point(593, 90);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(87, 22);
            this.checkBox2.TabIndex = 37;
            this.checkBox2.Text = "Localhost:";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.Visible = false;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(182, 94);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(95, 18);
            this.label35.TabIndex = 40;
            this.label35.Text = "Base de Datos:";
            this.label35.Click += new System.EventHandler(this.label35_Click);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(198, 148);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(79, 18);
            this.label36.TabIndex = 38;
            this.label36.Text = "Contraseña:";
            this.label36.Click += new System.EventHandler(this.label36_Click);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(220, 121);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(57, 18);
            this.label37.TabIndex = 35;
            this.label37.Text = "Usuario:";
            this.label37.Click += new System.EventHandler(this.label37_Click);
            // 
            // TabComparteSql
            // 
            this.TabComparteSql.Controls.Add(this.label1);
            this.TabComparteSql.Controls.Add(this.DropDownSqlBaseComparte);
            this.TabComparteSql.Controls.Add(this.pictureBox3);
            this.TabComparteSql.Controls.Add(this.label40);
            this.TabComparteSql.Controls.Add(this.label41);
            this.TabComparteSql.Controls.Add(this.label42);
            this.TabComparteSql.Controls.Add(this.TxtsqlPassComparte);
            this.TabComparteSql.Controls.Add(this.TxtsqlUsuarioComparte);
            this.TabComparteSql.Controls.Add(this.txtSqlServerComparte);
            this.TabComparteSql.Controls.Add(this.label43);
            this.TabComparteSql.Controls.Add(this.btnConeccionSqlComparte);
            this.TabComparteSql.Location = new System.Drawing.Point(4, 22);
            this.TabComparteSql.Name = "TabComparteSql";
            this.TabComparteSql.Padding = new System.Windows.Forms.Padding(3);
            this.TabComparteSql.Size = new System.Drawing.Size(851, 403);
            this.TabComparteSql.TabIndex = 8;
            this.TabComparteSql.Text = "tabPage2";
            this.TabComparteSql.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(305, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(220, 18);
            this.label1.TabIndex = 65;
            this.label1.Text = "Seleccionar la base de datos destino";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Enabled = false;
            this.pictureBox3.Location = new System.Drawing.Point(555, 176);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(28, 26);
            this.pictureBox3.TabIndex = 64;
            this.pictureBox3.TabStop = false;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(95, 184);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(174, 18);
            this.label40.TabIndex = 63;
            this.label40.Text = "Base de Datos de Comparte:";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(190, 153);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(79, 18);
            this.label41.TabIndex = 61;
            this.label41.Text = "Contraseña:";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(212, 125);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(57, 18);
            this.label42.TabIndex = 59;
            this.label42.Text = "Usuario:";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(207, 96);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(62, 18);
            this.label43.TabIndex = 57;
            this.label43.Text = "Servidor:";
            // 
            // TabComparteParametros
            // 
            this.TabComparteParametros.Controls.Add(this.panel3);
            this.TabComparteParametros.Location = new System.Drawing.Point(4, 22);
            this.TabComparteParametros.Name = "TabComparteParametros";
            this.TabComparteParametros.Padding = new System.Windows.Forms.Padding(3);
            this.TabComparteParametros.Size = new System.Drawing.Size(851, 403);
            this.TabComparteParametros.TabIndex = 9;
            this.TabComparteParametros.Text = "tabPage5";
            this.TabComparteParametros.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label44);
            this.panel3.Controls.Add(this.ButtonBitacoraComparte);
            this.panel3.Controls.Add(this.TxtRutaBitacoraComparte);
            this.panel3.Controls.Add(this.label45);
            this.panel3.Location = new System.Drawing.Point(66, 52);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(703, 67);
            this.panel3.TabIndex = 37;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(13, 25);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(40, 18);
            this.label45.TabIndex = 36;
            this.label45.Text = "Ruta:";
            // 
            // TabComparteResumen
            // 
            this.TabComparteResumen.Controls.Add(this.txtInfoBitacoraRuta);
            this.TabComparteResumen.Controls.Add(this.label48);
            this.TabComparteResumen.Controls.Add(this.txtInfoBdFirebird);
            this.TabComparteResumen.Controls.Add(this.label46);
            this.TabComparteResumen.Controls.Add(this.txtInfoBdMSSQL);
            this.TabComparteResumen.Controls.Add(this.label47);
            this.TabComparteResumen.Location = new System.Drawing.Point(4, 22);
            this.TabComparteResumen.Name = "TabComparteResumen";
            this.TabComparteResumen.Padding = new System.Windows.Forms.Padding(3);
            this.TabComparteResumen.Size = new System.Drawing.Size(851, 403);
            this.TabComparteResumen.TabIndex = 10;
            this.TabComparteResumen.Text = "tabPage1";
            this.TabComparteResumen.UseVisualStyleBackColor = true;
            // 
            // txtInfoBitacoraRuta
            // 
            this.txtInfoBitacoraRuta.BackColor = System.Drawing.Color.White;
            this.txtInfoBitacoraRuta.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtInfoBitacoraRuta.Enabled = false;
            this.txtInfoBitacoraRuta.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInfoBitacoraRuta.ForeColor = System.Drawing.Color.Black;
            this.txtInfoBitacoraRuta.Location = new System.Drawing.Point(226, 204);
            this.txtInfoBitacoraRuta.Multiline = true;
            this.txtInfoBitacoraRuta.Name = "txtInfoBitacoraRuta";
            this.txtInfoBitacoraRuta.Size = new System.Drawing.Size(595, 59);
            this.txtInfoBitacoraRuta.TabIndex = 45;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(69, 204);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(148, 22);
            this.label48.TabIndex = 44;
            this.label48.Text = "Ruta de Bitácora :";
            // 
            // txtInfoBdFirebird
            // 
            this.txtInfoBdFirebird.BackColor = System.Drawing.Color.White;
            this.txtInfoBdFirebird.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtInfoBdFirebird.Enabled = false;
            this.txtInfoBdFirebird.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInfoBdFirebird.ForeColor = System.Drawing.Color.Black;
            this.txtInfoBdFirebird.Location = new System.Drawing.Point(226, 94);
            this.txtInfoBdFirebird.Multiline = true;
            this.txtInfoBdFirebird.Name = "txtInfoBdFirebird";
            this.txtInfoBdFirebird.Size = new System.Drawing.Size(595, 49);
            this.txtInfoBdFirebird.TabIndex = 43;
            this.txtInfoBdFirebird.WordWrap = false;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(25, 94);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(192, 22);
            this.label46.TabIndex = 42;
            this.label46.Text = "Base de Datos FireBird :";
            // 
            // txtInfoBdMSSQL
            // 
            this.txtInfoBdMSSQL.BackColor = System.Drawing.Color.White;
            this.txtInfoBdMSSQL.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtInfoBdMSSQL.Enabled = false;
            this.txtInfoBdMSSQL.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInfoBdMSSQL.ForeColor = System.Drawing.Color.Black;
            this.txtInfoBdMSSQL.Location = new System.Drawing.Point(226, 149);
            this.txtInfoBdMSSQL.Multiline = true;
            this.txtInfoBdMSSQL.Name = "txtInfoBdMSSQL";
            this.txtInfoBdMSSQL.Size = new System.Drawing.Size(595, 49);
            this.txtInfoBdMSSQL.TabIndex = 41;
            this.txtInfoBdMSSQL.WordWrap = false;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(38, 149);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(179, 22);
            this.label47.TabIndex = 40;
            this.label47.Text = "Base de Datos MSSQL :";
            // 
            // FPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(863, 508);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.WizardPages1);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(910, 680);
            this.MinimumSize = new System.Drawing.Size(873, 32);
            this.Name = "FPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Transferencia de Datos";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FPrincipal_FormClosing);
            this.Load += new System.EventHandler(this.FPrincipal_Load);
            this.Shown += new System.EventHandler(this.FPrincipal_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FPrincipal_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FPrincipal_KeyPress);
            tabPage3.ResumeLayout(false);
            tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.WizardPages1.ResumeLayout(false);
            this.step1.ResumeLayout(false);
            this.step1.PerformLayout();
            this.step2.ResumeLayout(false);
            this.step2.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.TabComparteFb.ResumeLayout(false);
            this.TabComparteFb.PerformLayout();
            this.TabComparteSql.ResumeLayout(false);
            this.TabComparteSql.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.TabComparteParametros.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.TabComparteResumen.ResumeLayout(false);
            this.TabComparteResumen.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.Button BtnCancel;
        private System.Windows.Forms.Button btAtrass;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.OpenFileDialog OpenFDFirebird;
        private System.Windows.Forms.SaveFileDialog SaveFDRuta;
        private System.Windows.Forms.Label lblAccion;
        private System.Windows.Forms.Button btnFin;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnSiguiente;
        private System.Windows.Forms.HelpProvider helpProvider1;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TextBox lblRuta;
        private System.Windows.Forms.TextBox bdCorpText;
        private System.Windows.Forms.TextBox bdProtext;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label lblKardex;
        private System.Windows.Forms.Label lblTarjetas;
        private System.Windows.Forms.Label lblBitacora;
        private System.Windows.Forms.Label lblDestino;
        private System.Windows.Forms.Label lblOrigen;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox TxtRuta;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox TxtTablaInicial;
        private System.Windows.Forms.RadioButton RadBtnMigracionParcial;
        private System.Windows.Forms.RadioButton RadBtnMigracionCompleta;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.CheckBox CheckPrestamosAgregar;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.CheckBox CheckKardexAgregar;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.DateTimePicker FechaFinAsistencia;
        private System.Windows.Forms.RadioButton RadBtnAsistenciaRango;
        private System.Windows.Forms.DateTimePicker FechaIniAsistencia;
        private System.Windows.Forms.RadioButton RadBtnAsistenciaCompleta;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DateTimePicker FechaFinBitacora;
        private System.Windows.Forms.DateTimePicker FechaIniBitacora;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.RadioButton RadBtnBitacoraRango;
        private System.Windows.Forms.RadioButton radBtnBitacoraCompleta;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox TxtBaseDatosSQL;
        private System.Windows.Forms.CheckBox checkWSA;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TxtContrasenaSQL;
        private System.Windows.Forms.TextBox TxtUsuarioSQl;
        private System.Windows.Forms.TextBox TxtServidorSQL;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnProbarSql;
        private System.Windows.Forms.TabPage step2;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox localcheck;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtBaseDatosFB;
        private System.Windows.Forms.TextBox TxtContrasenaFB;
        private System.Windows.Forms.TextBox TxtUsuarioFB;
        private System.Windows.Forms.TextBox TxtServidorFB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button BtnProbarFirebird;
        private System.Windows.Forms.TabPage step1;
        private System.Windows.Forms.TextBox textBox1;
        public WizardPages WizardPages1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label lblCodigo;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Button BtnMigrarDatos;
        private System.Windows.Forms.Button BtnMigrarComparte;
        private System.Windows.Forms.TabPage TabComparteFb;
        private System.Windows.Forms.TabPage TabComparteSql;
        private System.Windows.Forms.TabPage TabComparteParametros;
        private System.Windows.Forms.TabPage TabComparteResumen;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtfbBaseComparte;
        private System.Windows.Forms.TextBox TxtFbPassComparte;
        private System.Windows.Forms.TextBox txtfbUserComparte;
        private System.Windows.Forms.TextBox txtfbServerComparte;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Button btnConeccionComparte;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox DropDownSqlBaseComparte;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox TxtsqlPassComparte;
        private System.Windows.Forms.TextBox TxtsqlUsuarioComparte;
        private System.Windows.Forms.TextBox txtSqlServerComparte;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Button btnConeccionSqlComparte;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Button ButtonBitacoraComparte;
        private System.Windows.Forms.TextBox TxtRutaBitacoraComparte;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox txtInfoBitacoraRuta;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox txtInfoBdFirebird;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox txtInfoBdMSSQL;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label LabelTipoMigracion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.ComboBox CmBComparte;
        private System.Windows.Forms.ComboBox CmBEmpresa;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
    }
}

