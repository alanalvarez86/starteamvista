﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.ServiceProcess;
using System.Diagnostics;
using FirebirdSql.Data.FirebirdClient;
using FirebirdSql.Data.Client;
using System.Data.SqlClient;
using System.Data.Sql;
namespace Wizard_Migracion_Tress
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 
        public static FPrincipal Principal;
        public static Migrar Migrar;
        public static FResultados Resultados;
        public static FPrepara Prepara;
        public static FMigrar Form1;
        public static Acercadefrm Acercadefrm;
        public static string reporte;
        public static string server;
        public static string usuario;
        public static string password;
        public static bool validremoteuser;
        public static string domain;
        public static bool fbserverfound;
        public static DateTime horaini;
        public static DateTime horafin;
        public static bool migrarDerechos = false;
        public static string ConexionMaster = "";
        public static string CDEmpresa = "";
        public static string NombreComparte = "";
        public static string nombrebaseDatos = "";
        public static bool MigrarComparte;
        public static string Compañia = "";
        public static string VersionCompartesql = "";
        [STAThread]
        static void Main()
        {
             
            
            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");

            System.Threading.Thread.CurrentThread.CurrentCulture = culture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = culture;
            Principal = new FPrincipal();
            Principal.StartPosition = FormStartPosition.CenterScreen;
            Application.EnableVisualStyles();
           // Application.SetCompatibleTextRenderingDefault(false);
            try{

            Application.Run(Principal);
            }
            catch(Exception e){MessageBox.Show(e.ToString());}
        }

      


        public static void reiniciaServidor(ref FbConnection conexion)
        {
            ServiceController fbguard = new ServiceController();
            fbguard.DisplayName = "Firebird Guardian - DefaultInstance";
            fbguard.MachineName = Program.server;
            string text = fbguard.Status.ToString();
            do{
               
            using (new Impersonator(Program.usuario, Program.domain, Program.password))
            {
                try
                {
                    
                    
                    conexion.Close();
                    fbguard.Stop();
                    fbguard.WaitForStatus(ServiceControllerStatus.Stopped);
                 //   fbservice.Stop();
                //    fbservice.WaitForStatus(ServiceControllerStatus.Stopped);
                    //  System.Windows.Forms.MessageBox.Show(fbservice.Status.ToString());
                    System.Threading.Thread.Sleep(2000);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();

                    do
                    {
                        try
                        {

                            conexion.Open();
                        }
                        catch
                        {
                            if (fbguard.Status != ServiceControllerStatus.Running)
                            {
                                try
                                {

                                    fbguard.Start();
                                    fbguard.WaitForStatus(ServiceControllerStatus.Running, new TimeSpan(0, 0, 5));
                                    
                                }
                                catch
                                {


                                }
                            }
                            else
                            {



                            }
                        }
                    } while (conexion.State == System.Data.ConnectionState.Closed || fbguard.Status != ServiceControllerStatus.Running);
                }
                catch { }
            }
            } while (conexion.State == System.Data.ConnectionState.Closed || fbguard.Status != ServiceControllerStatus.Running);
        }
        public static void reiniciaServidor(ref FbConnection conexion, ref FbConnection conexion2, ref SqlConnection conexionsql)
        {
            ServiceController fbguard = new ServiceController();
            fbguard.DisplayName = "Firebird Guardian - DefaultInstance";
            fbguard.MachineName = Program.server;
            do{
                
            using (new Impersonator(Program.usuario, Program.domain, Program.password))
            {
                try
                {
                    
                    conexionsql.Close();
                    conexion.Close();
                    conexion2.Close();
                    fbguard.Stop();
                    fbguard.WaitForStatus(ServiceControllerStatus.Stopped);
                    //  System.Windows.Forms.MessageBox.Show(fbservice.Status.ToString());
                    System.Threading.Thread.Sleep(2000);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();

                    do
                    {
                        try
                        {

                            if (conexion.State != System.Data.ConnectionState.Open)
                            {
                                conexion.Open();
                            }
                            if (conexion2.State != System.Data.ConnectionState.Open)
                            {
                                conexion2.Open();
                            }
                            if (conexionsql.State != System.Data.ConnectionState.Open)
                            {
                                conexionsql.Open();
                            }
                            SqlCommand query = new SqlCommand(@"
DBCC FREEPROCCACHE
DBCC FREESYSTEMCACHE('ALL')
DBCC FREESESSIONCACHE", conexionsql);
                            query.ExecuteNonQuery();
                        }
                        catch
                        {
                            if (fbguard.Status != ServiceControllerStatus.Running)
                            {
                                try
                                {

                                    fbguard.Start();
                                    fbguard.WaitForStatus(ServiceControllerStatus.Running, new TimeSpan(0, 0, 5));

                                }
                                catch
                                {


                                }
                            }
                            else
                            {



                            }
                        }
                    } while (conexion.State == System.Data.ConnectionState.Closed || conexion2.State == System.Data.ConnectionState.Closed || fbguard.Status != ServiceControllerStatus.Running);

                }catch{}
            }
            } while (conexion.State == System.Data.ConnectionState.Closed || conexion2.State == System.Data.ConnectionState.Closed || fbguard.Status != ServiceControllerStatus.Running);
        
        }
        public static string[] array = {    "GLOBAL",  "COLABORA",
                                "TAHORRO",
                                "AHORRO",
                                "ACAR_ABO",
                                "ACCESLOG",
                                "EXPEDIEN",
                                "ACCIDENT",
                                "CURSO",
                                "ACCION",
                                "ACCREGLA",
                                "CONCEPTO",
                                "ACUMULA",
                                "AGUINAL",
                                "ANTESCUR",
                                "ANTESPTO",
                                "AREA",
                                "NUMERICA",
                                "T_ART_80",
                                "ART_80",
                                "AR_TEM_CUR",
                                "ASIGNA",
                                "AULA",
                                "AUSENCIA",
                                "BITACORA",
                                "BREAKS",
                                "BRK_HORA",
                                "CAFREGLA",
                                "CAF_COME",
                                "INVITA",
                                "CAF_INV",
                                "CALCURSO",
                                "CALIFICA",
                                "REPORTE",
                                "CAMPOREP",
                                "CAMPO_AD",
                                "CAMPO_EX",
                                "CAUSACCI",
                                "CCURSO",
                                "CEDSCRAP",
                                "CEDULA",
                                "CED_EMP",
                                "CED_INSP",
                                "CED_WORD",
                                "CERTIFIC",
                                "PUESTO",
                                "PTO_CERT",
                                "CERNIVEL",
                                "CHECADAS",
                                "CLASIFI",
                                "CLASITMP",
                                "COLONIA",
                                "COMPARA",
                                "COMPETEN",
                                "COMPONEN",
                                "COMP_CAL",
                                "FAM_PTO",
                                "COMP_FAM",
                                "COMP_MAP",
                                "COMP_PTO",
                                "CONSULTA",
                                "CONTEO",
                                "CONTRATO",
                                "CTABANCO",
                                "CTA_MOVS",
                                "CURSOPRE",
                                "SESION",
                                "INSCRITO",
                                "RESERVA",
                                "CUR_ASIS",
                                "CUR_REV",
                                "DECLARA",
                                "TDEFECTO",
                                "DEFECTO",
                                "PARTES",
                                "DEFSTEPS",
                                "DESCTIPO",
                                "DESC_FLD",
                                "PERFIL",
                                "DESC_PTO",
                                "DIAGNOST",
                                "DICCION",
                                "DIMENSIO",
                                "DOCUMENTO",
                                "EDOCIVIL",
                                "EMBARAZO",
                                "EMP_COMP",
                                "EMP_PLAN",
                                "POLIZA_MED",
                                "EMP_POLIZA",
                                "EMP_PROG",
                                "ENTIDAD",
                                "ENTRENA",
                                "ENTNIVEL",
                                "ESTABLEC",
                                "ESTUDIOS",
                                "EVENTO",
                                "EXTRA1",
                                "EXTRA10",
                                "EXTRA11",
                                "EXTRA12",
                                "EXTRA13",
                                "EXTRA14",
                                "EXTRA2",
                                "EXTRA3",
                                "EXTRA4",
                                "EXTRA5",
                                "EXTRA6",
                                "EXTRA7",
                                "EXTRA8",
                                "EXTRA9",
                                "PERIODO",
                                "NOMINA",
                                "FALTAS",
                                "TURNO",
                                "FESTIVO",
                                "FOLIO",
                                "FON_TOT",
                                "FON_EMP",
                                "FON_CRE",
                            
                                "GRUPO_AD",
                                "GRUPO_EX",
                                "GR_AD_ACC",
                                "HORARIO",
                                "HUELLA",
                                "IMAGEN",
                                "INCAPACI",
                                "INCIDEN",
                                "ISR_AJUSTE",
                                "KARCURSO",
                                "TKARDEX",
                                "KARDEX",
                                "KARINF",
                                "KAR_AREA",
                                "KAR_CERT",
                                "KAR_EMPSIL",
                                "KAR_FIJA",
                                "TOOL",
                                "KAR_TOOL",
                                "LAY_PRO_CE",
                                "MAQUINA",
                                "LAY_MAQ",
                                "LECTURAS",
                                "LEY_IMSS",
                                "LIQ_IMSS",
                                "LIQ_EMP",
                                "LIQ_MOV",
                                "MAESTRO",
                                "MAQ_CERT",
                                "MEDICINA",
                                "MED_ENTR",
                                "MISREPOR",
                                "MODULA1",
                                "MODULA2",
                                "MODULA3",
                                "MONEDA",
                                "MOTACCI",
                                "MOTSCRAP",
                                "MOT_AUTO",
                                "MOT_BAJA",
                                "MOT_CHECA",
                                "MOT_TOOL",
                                "MOVIMIEN",
                                "MOV_GRAL",
                                "MUNICIPIO",
                                "NIVEL1",
                                "NIVEL2",
                                "NIVEL3",
                                "NIVEL4",
                                "NIVEL5",
                                "NIVEL6",
                                "NIVEL7",
                                "NIVEL8",
                                "NIVEL9",
                                "NIV_PTO",
                                "NIV_DIME",
                                "NOMPARAM",
                                "OCUPA_NAC",
                                "OPERA",
                                "ORDFOLIO",
                                "OTRASPER",
                                "PARIENTE",
                                "TPRESTA",
                                "PRESTAMO",
                                "PCAR_ABO",
                                "PENSION",
                                "PEN_PORCEN",
                                "PERMISO",
                                "PLAZA",
                                "POL_TIPO",
                                "POL_HEAD",
                                "POLIZA",
                                "POL_VIGENC",
                                "SSOCIAL",
                                "PRESTACI",
                                "REGLAPREST",
                                "PRESTAXREG",
                                "RSOCIAL",
                                "RPATRON",
                                "PRIESGO",
                                "PROCESO",
                                "PROV_CAP",
                                "PTOFIJAS",
                                "PTOTOOLS",
                                "PTO_DIME",
                                "QUERYS",
                                "REP_AHO",
                                "REP_EMPS",
                                "REP_PTU",
                                "RIESGO",
                                "SAL_MIN",
                                "SCRAP",
                                "SILLA_LAY",
                                "SILLA_MAQ",
                                "WORDER",
                                "STEPS",
                                "SUPER",
                                "SUP_AREA",
                                "SUSCRIP",
                                "SY_ALARMA",
                                "TACCIDEN",
                                "TALLA",
                                "TCAMBIO",
                                "TCOMPETE",
                                "TCONSLTA",
                                "TCTAMOVS",
                                "TCURSO",
                                "TESTUDIO",
                                "TMAQUINA",
                                "TMPBALA2",
                                "TMPBALAMES",
                                "TMPBALAN",
                                "TMPCALEN",
                                "TMPCALHR",
                                "TMPDEMO",
                                "TMPDIMM",
                                "TMPDIMMTOT",
                                "TMPESTAD",
                                "TMPEVENT",
                                "TMPFOLIO",
                                "TMPHORAS",
                                "TMPLABOR",
                                "TMPLISTA",
                                "TMPMOVS",
                                "TMPNOM",
                                "TMPPROMVAR",
                                "TMPROTA",
                                "TMPROTAI",
                                "TMPSALAR",
                                "TMPTOTAL",
                                "TMUERTO",
                                "TOPERA",
                                "TPARTE",
                                "TPENSION",
                                "TPERIODO",
                                "TRANSPOR",
                                "VACACION",
                                "VACAPLAN",
                                "VIVE_CON",
                                "VIVE_EN",
                                "WOFIJA",
                                "WORKS"};    
    }
}
