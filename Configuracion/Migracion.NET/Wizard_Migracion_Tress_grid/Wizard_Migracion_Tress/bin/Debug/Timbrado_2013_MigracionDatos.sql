IF NOT EXISTS( SELECT * FROM syscolumns WHERE NAME = 'CT_CODIGO' AND id = object_id('RSOCIAL'))
	alter table RSOCIAL add CT_CODIGO Codigo null 
go 
update RSOCIAL set CT_CODIGO = ''  where CT_CODIGO is null 
go 
alter table RSOCIAL alter column CT_CODIGO Codigo not null 
go 
IF NOT EXISTS( SELECT * FROM syscolumns WHERE NAME = 'RS_CONTID' AND id = object_id('RSOCIAL'))
	alter table RSOCIAL add RS_CONTID FolioGrande  null 
go 
update RSOCIAL set RS_CONTID = 0  where RS_CONTID is null 
go 
alter table RSOCIAL alter column RS_CONTID FolioGrande  not null 
go 

/* Status del timbre */ 
IF NOT EXISTS( SELECT * FROM syscolumns WHERE NAME = 'PE_TIMBRO' AND id = object_id('PERIODO'))
	alter table PERIODO add PE_TIMBRO Status  null

go 
update PERIODO set PE_TIMBRO  = 0 where PE_TIMBRO is null 
go 

alter table PERIODO alter column PE_TIMBRO Status not  null

go 

 /* Status del timbre */ 
IF NOT EXISTS( SELECT * FROM syscolumns WHERE NAME = 'NO_TIMBRO' AND id = object_id('NOMINA'))
	alter table NOMINA add NO_TIMBRO   Status  null 
go 
IF NOT EXISTS( SELECT * FROM syscolumns WHERE NAME = 'NO_SELLO' AND id = object_id('NOMINA'))
	alter table NOMINA add NO_SELLO Formula  null

go 
IF NOT EXISTS( SELECT * FROM syscolumns WHERE NAME = 'NO_FACTURA' AND id = object_id('NOMINA'))
	alter table NOMINA add NO_FACTURA  FolioGrande  null 

go 
  
 
IF NOT EXISTS( SELECT * FROM syscolumns WHERE NAME = 'CO_SAT_CON' AND id = object_id('CONCEPTO'))
begin 

alter table CONCEPTO 
add 
	CO_SAT_CLP	Status  NULL,
	CO_SAT_TPP	Codigo  NULL ,
	CO_SAT_CLN	Status  NULL,
	CO_SAT_TPN	Codigo  Null,
	CO_SAT_EXE	Status  NULL, 
	CO_SAT_CON	Concepto  NULL 
	
end 

go 

update CONCEPTO set CO_SAT_CLP = 0
where CO_SAT_CLP is null 

go 
update CONCEPTO set  CO_SAT_TPP = '' where  CO_SAT_TPP is null 

go 
update CONCEPTO set	CO_SAT_CLN = 0	where CO_SAT_CLN is null

go 
update CONCEPTO set  CO_SAT_TPN = '' where CO_SAT_TPN is null

go 
update CONCEPTO set CO_SAT_EXE = 0 where CO_SAT_EXE is null 

go 
update CONCEPTO set  CO_SAT_CON = '' where CO_SAT_CON is null

go 
alter table CONCEPTO 
alter column  CO_SAT_CLP	Status not null  
go 
alter table CONCEPTO 
alter column  CO_SAT_TPP	Codigo not null  
go 
alter table CONCEPTO 
alter column  CO_SAT_CLN	Status not null 
go 
alter table CONCEPTO 
alter column  CO_SAT_TPN	Codigo not null 
go 
alter table CONCEPTO 
alter column  CO_SAT_EXE	Status not null  
go 
alter table CONCEPTO 
alter column  CO_SAT_CON	Concepto not null 
	

go 

