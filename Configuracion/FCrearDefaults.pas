unit FCrearDefaults;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ComCtrls, Buttons, ExtCtrls, Checklst,
     FMigrar,
     DMigrar,
     ZetaWizard, jpeg;

type
  TCrearDefaults = class(TMigrar)
    TablasDefaultsGB: TGroupBox;
    ListaDefaults: TCheckListBox;
    DirectorioGB: TGroupBox;
    PathLBL: TLabel;
    Path: TEdit;
    PathSeek: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
  private
    { Private declarations }
    procedure SetUpLista;
  public
    { Public declarations }
  end;

var
  CrearDefaults: TCrearDefaults;

implementation

uses ZetaFunciones,
     ZetaMigrar,
     DMigrarDatos;

{$R *.DFM}

procedure TCrearDefaults.FormCreate(Sender: TObject);
begin
     FParadoxPath := Path;
     FListaDefaults := ListaDefaults;
     dmMigracion := TdmMigrarDatos.Create( Self );
     Path.Text := SetFileNameDefaultPath( 'Fuentes' );
     LogFile.Text := SetFileNameDefaultPath( 'Defaults.log' );
     InitListaTablas;
     SetUpLista;
     inherited;
end;

procedure TCrearDefaults.FormDestroy(Sender: TObject);
begin
     dmMigracion.Free;
     inherited;
end;

procedure TCrearDefaults.SetUpLista;
var
   i: Integer;
begin
     with ListaDefaults do
     begin
          for i := 0 to ( Items.Count - 1 ) do
              Checked[ i ] := TZetaMigrar( Items.Objects[ i ] ).Poblar;
     end;
end;

procedure TCrearDefaults.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     if CanMove then
     begin
          if Wizard.Adelante then
          begin
               if Wizard.EsPaginaActual( DirectorioDOS ) then
                  CanMove := ValidaDirectorioParadox;
          end;
     end;
end;

procedure TCrearDefaults.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
begin
     StartLog;
     lOk := CrearDefaults;
     EndLog;
     inherited;
end;

end.
