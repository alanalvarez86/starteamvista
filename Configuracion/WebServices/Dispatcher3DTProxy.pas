// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : http://10.10.10.26/3DT/Dispatcher/Dispatcher.asmx?WSDL
// Encoding : utf-8
// Version  : 1.0
// (01/04/2014 10:00:52 a.m. - 1.33.2.5)
// ************************************************************************ //

unit Dispatcher3DTProxy;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns, System.Classes, Winapi.WinInet, Soap.SOAPHTTPTrans, Soap.OpConvertOptions;

const
K_TIMEOUT_CONNECT = 5000;
K_TIMEOUT_TRANSACTION = 300000;

type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"
  // !:base64Binary    - "http://www.w3.org/2001/XMLSchema"

   TWebServiceConfigSOAP = class( TObject )
public
  FHTTPRIO: THTTPRIO;

  procedure BeforeExecute(const MethodName: string; SOAPRequest: TStream);
end;



  TSOAPDataSet = class; { "urn:ServidorSOAPIntf"[GblCplx] }

// ************************************************************************ //
  // XML       : TSOAPDataSet, global, <complexType>
  // Namespace : urn:ServidorSOAPIntf
  // ************************************************************************ //
  TSOAPDataSet = class(TRemotable)
  private
    FMsgID: Integer;
    FParams: string;
    FDataSet: string;
  published
    property MsgID:   Integer  read FMsgID write FMsgID;
    property Params:  string   read FParams write FParams;
    property DataSet: string   read FDataSet write FDataSet;
  end;

  // ************************************************************************ //
  // Namespace : http://tress.com.mx/
  // soapAction: http://tress.com.mx/%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // binding   : Despachador_x0020_3DTSoap
  // service   : Despachador_x0020_3DT
  // port      : Despachador_x0020_3DTSoap
  // URL       : http://10.10.10.26/3DT/Dispatcher/Dispatcher.asmx
  // ************************************************************************ //
  Despachador_x0020_3DTSoap = interface(IInvokable)
  ['{2E906926-71D4-6692-FA1E-9ECBB0564736}']
    function  Echo: WideString; stdcall;
    function  Upload(const archivo: TByteDynArray; const xml: WideString): WideString; stdcall;
    function  UploadBase64(const archivo: WideString; const xml: WideString): WideString; stdcall;
    function  UploadRepLicBase64 (const archivo: WideString; const xml: WideString): WideString; stdcall; //(@am): metodo agregado a la interfaz;
  end;

function GetDespachador_x0020_3DTSoap(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): Despachador_x0020_3DTSoap;


implementation

function GetDespachador_x0020_3DTSoap(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): Despachador_x0020_3DTSoap;
const
  defWSDL = 'http://10.10.10.26/3DT/Dispatcher/Dispatcher.asmx?WSDL';
  defURL  = 'http://10.10.10.26/3DT/Dispatcher/Dispatcher.asmx';
  defSvc  = 'Despachador_x0020_3DT';
  defPrt  = 'Despachador_x0020_3DTSoap';
var
  RIO: THTTPRIO;
  wsConfig : TWebServiceConfigSOAP;
begin
  Result := nil;
  wsConfig  :=   TWebServiceConfigSOAP.Create;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try

    RIO.HTTPWebNode.InvokeOptions := RIO.HTTPWebNode.InvokeOptions + [ soIgnoreInvalidCerts, soAutoCheckAccessPointViaUDDI ];
    RIO.Converter.Options := RIO.Converter.Options + [soUTF8InHeader];
    RIO.HTTPWebNode.UseUTF8InHeader := True;

    Result := (RIO as Despachador_x0020_3DTSoap);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;

    wsConfig.FHTTPRIO := RIO;
    RIO.OnBeforeExecute := wsConfig.BeforeExecute;

  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;

{ TWebServiceConfigSOAP }

procedure TWebServiceConfigSOAP.BeforeExecute(const MethodName: string; SOAPRequest: TStream);
var iTimeOUt, iTimeOutTx : integer;
begin
    iTimeOut := K_TIMEOUT_CONNECT;
    iTimeOutTx := K_TIMEOUT_TRANSACTION;

    InternetSetOption(nil, INTERNET_OPTION_CONNECT_TIMEOUT, Pointer(@iTimeOut),
    SizeOf(iTimeOut));

    InternetSetOption(nil, INTERNET_OPTION_SEND_TIMEOUT, Pointer(@iTimeOutTx),
    SizeOf(iTimeOutTx));

    InternetSetOption(nil, INTERNET_OPTION_RECEIVE_TIMEOUT, Pointer(@iTimeOutTx),
    SizeOf(iTimeOutTx));
end;

initialization
  InvRegistry.RegisterInterface(TypeInfo(Despachador_x0020_3DTSoap), 'http://tress.com.mx/', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(Despachador_x0020_3DTSoap), 'http://tress.com.mx/%operationName%');
  InvRegistry.RegisterInvokeOptions(TypeInfo(Despachador_x0020_3DTSoap), ioDocument);
  // RemClassRegistry.RegisterXSClass(TSOAPDataSet, 'urn:ServidorSOAPIntf', 'TSOAPDataSet');
  RemClassRegistry.RegisterXSClass(TSOAPDataSet, 'http://tress.com.mx/', 'TSOAPDataSet');

end.