object HayProblemas: THayProblemas
  Left = 319
  Top = 198
  ActiveControl = Configurar
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Error Al Abrir Base De Datos Comparte'
  ClientHeight = 133
  ClientWidth = 313
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Imagen: TImage
    Left = 0
    Top = 0
    Width = 65
    Height = 92
    Align = alLeft
    Center = True
  end
  object PanelBotones: TPanel
    Left = 0
    Top = 92
    Width = 313
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Reintentar: TBitBtn
      Left = 8
      Top = 8
      Width = 88
      Height = 29
      Hint = 'Reintentar Abrir Base de Datos'
      Caption = '&Reintentar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Kind = bkRetry
    end
    object Configurar: TBitBtn
      Left = 112
      Top = 8
      Width = 88
      Height = 29
      Hint = 'Configurar Acceso a Base de Datos'
      Caption = '&Configurar'
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = ConfigurarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555550FF0559
        1950555FF75F7557F7F757000FF055591903557775F75557F77570FFFF055559
        1933575FF57F5557F7FF0F00FF05555919337F775F7F5557F7F700550F055559
        193577557F7F55F7577F07550F0555999995755575755F7FFF7F5570F0755011
        11155557F755F777777555000755033305555577755F75F77F55555555503335
        0555555FF5F75F757F5555005503335505555577FF75F7557F55505050333555
        05555757F75F75557F5505000333555505557F777FF755557F55000000355557
        07557777777F55557F5555000005555707555577777FF5557F55553000075557
        0755557F7777FFF5755555335000005555555577577777555555}
      NumGlyphs = 2
    end
    object Cancelar: TBitBtn
      Left = 216
      Top = 8
      Width = 88
      Height = 29
      Caption = '&Cancelar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Kind = bkCancel
    end
  end
  object Info: TMemo
    Left = 65
    Top = 0
    Width = 248
    Height = 92
    Align = alClient
    Alignment = taCenter
    BorderStyle = bsNone
    Color = clBtnFace
    ReadOnly = True
    TabOrder = 1
  end
end
