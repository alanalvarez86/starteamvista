unit FHayProblemas;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ExtCtrls, StdCtrls, Buttons, Registry;

type
  THayProblemas = class(TForm)
    Imagen: TImage;
    PanelBotones: TPanel;
    Reintentar: TBitBtn;
    Configurar: TBitBtn;
    Cancelar: TBitBtn;
    Info: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure ConfigurarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetError( Value: Exception );
  end;

var
  HayProblemas: THayProblemas;

function EditarRegistry: Boolean;
function ConnectionProblemDialog( Error: Exception ): Boolean;
procedure InvocarBDE;

implementation

uses FRegistryServerEditor,
     ZetaRegistryServer,
     ZetaTressCFGTools,
     ZetaCommonTools,
     ZetaDialogo,
     ZetaCommonClasses;

{$R *.DFM}

function EditarRegistry: Boolean;
begin
     Result := FRegistryServerEditor.EspecificaComparte;
end;

function ConnectionProblemDialog( Error: Exception ): Boolean;
begin
     with THayProblemas.Create( Application ) do
     begin
          try
             SetError( Error );
             Chicharra;
             ShowModal;
             Result := ( ModalResult = mrRetry );
          finally
                 Free;
          end;
     end;
end;

procedure InvocarBDE;
const
     K_BDE_EXE = 'BDEADMIN.EXE';
     K_BDE_KEY = 'Software\Borland\Database Engine';
     K_BDE_DLL_PATH = 'DLLPATH';
var
   sDirectory: String;
   lOk: Boolean;
begin
     lOk := False;
     with TRegistry.Create do
     begin
          try
             RootKey := HKEY_LOCAL_MACHINE;
             if OpenKey( K_BDE_KEY, False ) then
             begin
                  sDirectory := VerificaDir( ReadString( K_BDE_DLL_PATH ) );
                  lOk := True;
             end
             else
                 zError( '� Error En Registry !', 'La Llave Del BDE' + CR_LF + 'HKEY_LOCAL_MACHINE\' + K_BDE_KEY + CR_LF + 'No Existe', 0 );
          finally
                 Free;
          end;
     end;
     if lOk then
     begin
          if FileExists( sDirectory + K_BDE_EXE ) then
             ExecuteFile( K_BDE_EXE, '', sDirectory, SW_SHOWDEFAULT )
          else
              zError( '� Error Al Invocar BDE !', 'El Administrador Del BDE' + CR_LF + sDirectory + K_BDE_EXE + CR_LF + 'No Fu� Encontrado', 0 );
     end;
end;

{ ************** THayProblemas *************** }

procedure THayProblemas.FormCreate(Sender: TObject);
begin
     with Imagen do
     begin
          Picture.Icon.Handle := LoadIcon( 0, IDI_HAND );
     end;
end;

procedure THayProblemas.SetError( Value: Exception );
begin
     Info.Lines.Text := Value.Message;
end;

procedure THayProblemas.ConfigurarClick(Sender: TObject);
begin
     EditarRegistry;
end;

end.
