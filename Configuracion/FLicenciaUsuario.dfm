inherited LicenciaUsuario: TLicenciaUsuario
  Left = 290
  Top = 184
  Caption = 'Licencia de Usuario'
  ClientHeight = 373
  ClientWidth = 464
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 337
    Width = 464
    inherited OK: TBitBtn
      Left = 296
      Caption = '&Guardar'
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 381
    end
  end
  object redLicencia: TRichEdit
    Left = 0
    Top = 73
    Width = 464
    Height = 264
    Align = alClient
    Color = clBtnFace
    Lines.Strings = (
      
        'CONTRATO DE LICENCIA DE USO NO EXCLUSIVA PARA USUARIO FINAL RESP' +
        'ECTO '
      
        'AL PROGRAMA DE C'#211'MPUTO QUE  CELEBRAN POR UNA PARTE  '#39#39'GRUPO TRES' +
        'S '
      
        'INTERNACIONAL, S.A. DE C.V.'#39#39', REPRESENTADA POR EL C. JULIO CESA' +
        'R ANDRADE '
      
        'BUSTAMANTE, A QUIEN  EN LO SUCESIVO  PARA LOS EFECTOS DE ESTE CO' +
        'NTRATO '
      'SE DENOMINAR'#193' '#39#39'GRUPO TRESS'#39#39', RESPECTO AL PROGRAMA DE C'#211'MPUTO '
      
        #39#39'SISTEMA TRESS'#39#39', EN LO SUCESIVO EL SISTEMA, REGISTRADO ANTE EL' +
        ' INSTITUTO '
      
        'NACIONAL DEL DERECHO DE AUTOR, Y PROTEGIDO INTERNACIONALMENTE DE' +
        ' '
      
        'ACUERDO A LAS LEYES INTERNACIONALES, Y POR LA OTRA EL USUARIO FI' +
        'NAL DEL '
      
        'SISTEMA QUE ES LA PERSONA FISICA O MORAL QUE ADQUIRIO ESTA LICEN' +
        'CIA DE '
      
        'USO NO EXCLUSIVA DE GRUPO TRESS O SUS DISTRIBUIDORES AUTORIZADOS' +
        '. '
      ''
      ''
      'DECLARACIONES:'
      'I.-  GRUPO TRESS'
      
        'Que es una sociedad mercantil, constituida bajo las Leyes de los' +
        ' Estados Unidos Mexicanos, '
      
        'con Registro Federal de Contribuyentes GTI-950629-JR8, constitui' +
        'da en t'#233'rminos de las '
      
        'Escritura P'#250'blica N'#250'mero 5178, de fecha 27 de junio de 1995, pas' +
        'ado ante la fe del Notario '
      
        'P'#250'blico N'#250'm. 12, Lic. Lamberto Morera Mezquita, e inscrita en el' +
        ' Registro P'#250'blico de la '
      
        'Propiedad y del Comercio, bajo el n'#250'mero 5178. Que su representa' +
        'nte acredita su '
      
        'personalidad mediante instrumento p'#250'blico n'#250'mero 5178 y su objet' +
        'o es entre otros an'#225'lisis, '
      
        'dise'#241'o desarrollo, implantaci'#243'n, capacitaci'#243'n y asesor'#237'a de sist' +
        'emas; entre otros.'
      ''
      'II.- EL USUARIO FINAL.'
      
        'Para la prestaci'#243'n del servicio El Usuario Final es la persona q' +
        'ue se describe en el apartado '
      
        'de definiciones cuyo nombre consta en el proemio  del presente c' +
        'ontrato.'
      ''
      'DEFINICIONES'
      ''
      
        'I. CONSENTIMIENTO. Antes de adquirir la licencia o autorizar la ' +
        'instalaci'#243'n de EL SISTEMA '
      
        'en su equipo, se le pedir'#225' que revise y en su caso acepte los t'#233 +
        'rminos y condiciones '
      'estipulados en el presente contrato. '
      ''
      
        'Por ello, desde el momento en que sea adquirido o instalado EL S' +
        'ISTEMA en su equipo, se '
      
        'tendr'#225' por otorgado el consentimiento en la celebraci'#243'n del pres' +
        'ente contrato de licencia, y '
      
        'por consecuencia aceptados todos los t'#233'rminos contenidos en el m' +
        'ismo.'
      ''
      
        'II. DEFINICIONES. Salvo que se indique lo contrario en el presen' +
        'te Contrato de licencia, los '
      
        'siguientes t'#233'rminos tendr'#225'n los significados que a continuaci'#243'n ' +
        'se establecen:'
      ''
      
        'USUARIO FINAL: Persona f'#237'sica o moral que obtiene '#250'nicamente der' +
        'echos de uso de  EL '
      'SISTEMA.'
      ''
      
        'DOCUMENTACI'#211'N: Se refiere a la gu'#237'a del usuario y dem'#225's informac' +
        'i'#243'n otorgada al Usuario '
      'Final conjuntamente con  EL SISTEMA.'
      ''
      'EL SISTEMA: '#39#39'SISTEMA TRESS'#39#39'.'
      ''
      
        'SENTINEL: Dispositivo electr'#243'nico que tiene la funci'#243'n de contro' +
        'lar el n'#250'mero de accesos a '
      'EL SISTEMA en la red interna de computadoras.'
      ''
      
        'DISTRIBUIDOR: Persona f'#237'sica o moral autorizada por GRUPO TRESS ' +
        'para promover y '
      
        'comercializar EL SISTEMA, mediante el uso correcto de sus marcas' +
        ', en una determinada '
      'delimitaci'#243'n territorial.'
      ''
      
        'DERECHOS DE PROPIEDAD INDUSTRIAL E INTELECTUAL: Todos los signos' +
        ' distintivos '
      
        '(marcas, avisos comerciales, nombres comerciales), secretos indu' +
        'striales, derechos autorales '
      
        'sobre las diversas obras, as'#237' como el patentamiento de los proce' +
        'sos internos del programa de '
      'c'#243'mputo '#39#39'SISTEMA TRESS'#39#39'.'
      ''
      
        'INFORMACI'#211'N  CONFIDENCIAL: Toda aquella informaci'#243'n de aplicaci'#243 +
        'n industrial  y/o '
      
        'comercial  que  guarden GRUPO TRESS con car'#225'cter confidencial en' +
        ' soportes materiales de '
      
        'diversa naturaleza tangible, y que sea proporcionada al USUARIO ' +
        'FINAL.'
      ''
      
        'REVELACI'#211'N: A la difusi'#243'n, comunicaci'#243'n, publicaci'#243'n y divulgaci' +
        #243'n, en cualquier forma y a '
      
        'cualquier persona, de la informaci'#243'n confidencial recibida o a l' +
        'a que haya tenido acceso El '
      
        'Usuario Final, sea en forma directa o indirecta y sin consentimi' +
        'ento expreso de GRUPO '
      'TRESS.'
      ''
      
        'APODERAMIENTO: Que El Usuario Final se haga due'#241'o, ocupe para su' +
        ' beneficio o para el '
      
        'de un tercero o ponga bajo su poder o el de un tercero, la infor' +
        'maci'#243'n de car'#225'cter '
      
        'confidencial que haya recibido o a la que tenga o haya tenido ac' +
        'ceso, sea en forma directa o '
      'indirecta y sin consentimiento expreso de GRUPO TRESS.'
      ''
      
        'USO INDEBIDO: Se entender'#225' el empleo de la informaci'#243'n de car'#225'ct' +
        'er confidencial que '
      
        'haya recibido o a la que tenga o haya tenido acceso El Usuario F' +
        'inal, sea en forma directa o '
      
        'indirecta y sin consentimiento de  GRUPO TRESS, con fines divers' +
        'os para los cuales le fue '
      'entregada.'
      ''
      
        'III. EFECTOS. El presente Contrato empezar'#225' a surtir efectos a p' +
        'artir de la fecha en que '
      
        'primeramente se presente cualesquiera de los siguientes supuesto' +
        's:'
      ''
      
        '1. Que EL SISTEMA sea instalado en el dispositivo electr'#243'nico qu' +
        'e el Usuario Final haya '
      'determinado.'
      ''
      
        '2. Que el Usuario Final haya adquirido la licencia de uso de EL ' +
        'SISTEMA.'
      ''
      
        'Cualquier convenio o acuerdo relativo a este Contrato que haya s' +
        'ido celebrado con '
      
        'anterioridad a la fecha en que surta efectos el presente, dejar'#225 +
        ' de tener efectividad y por lo '
      
        'tanto las relaciones jur'#237'dicas que deriven del uso de EL SISTEMA' +
        ', se regir'#225'n por el presente '
      'Contrato de licencia.'
      ''
      'CLAUSULAS'
      ''
      
        'I. OTORGAMIENTO DE LICENCIA. GRUPO TRESS le otorga una licencia ' +
        'de uso NO '
      'EXCLUSIVA de EL SISTEMA para ser instalado en su servidor.'
      ''
      
        'Ambas partes acuerdan que EL SISTEMA se considerar'#225' que est'#225' en ' +
        'uso cuando se '
      
        'encuentre cargado temporalmente en memoria RAM del servidor, o i' +
        'nstalado en memorias '
      
        'permanentes, como es el caso de discos duros, discos compactos (' +
        'CD'#8217's), o cualquier otro '
      'dispositivo espec'#237'fico. '
      ''
      
        'Todos los dem'#225's derechos patrimoniales de EL SISTEMA no otorgado' +
        's en el presente, '
      'quedan reservados en favor de GRUPO TRESS.'
      ''
      
        'GRUPO TRESS entrega los discos y Documentaci'#243'n para la instalaci' +
        #243'n y operaci'#243'n  de '#39#39'El '
      
        'SISTEMA'#39#39', as'#237' como el '#39#39'SENTINEL'#39#39' para el control correspondie' +
        'nte.'
      ''
      
        'II. OBLIGACIONES DEL USUARIO FINAL.  Se obliga a usar el sistema' +
        ' en los t'#233'rminos, '
      
        'condiciones y limitaciones establecidas en el presente instrumen' +
        'to.'
      ''
      
        'No podr'#225' modificar las leyendas relacionadas con la titularidad ' +
        'de los derechos morales y '
      'patrimoniales de EL SISTEMA, incluido  la Documentaci'#243'n.'
      'B'
      
        'No est'#225' facultado para descompilar, desmembrar o desensamblar lo' +
        's programas de EL '
      
        'SISTEMA, en el caso del c'#243'digo fuente u objeto, en caso de que t' +
        'uviese acceso, o realizar '
      'cualquier proceso  para revertir la ingenier'#237'a de EL SISTEMA.'
      ''
      
        'III. LIMITACIONES DE LA LICENCIA. La licencia de uso no exclusiv' +
        'a que se otorgan '
      
        'mediante el presente contrato NO podr'#225' transmitirse, cederse, en' +
        'ajenarse, arrendarse,  de '
      
        'manera alguna por parte de EL USUARIO FINAL a favor de cualquier' +
        ' tercero. '
      ''
      
        'Asimismo, tampoco podr'#225' transmitirse electr'#243'nicamente, copiar, r' +
        'eproducir, modificar, adaptar '
      'o crear alguna obra derivada basada en  EL SISTEMA, '
      ''
      
        'IV. GARANT'#205'A. La garant'#237'a otorgada por el presente instrumento, ' +
        'estar'#225' vigente por un '
      
        't'#233'rmino que no exceder'#225' en ning'#250'n caso de noventa d'#237'as contados ' +
        'a partir de que inicien los '
      
        'efectos del presente instrumento, y en que dicha garant'#237'a quedar' +
        #225' sin efecto alguno en caso '
      
        'de que EL USUARIO FINAL no cumpla cabalmente  con todos y cada u' +
        'no de los requisitos y '
      
        'limitaciones que se establecen en la Documentaci'#243'n que le sea en' +
        'tregada.'
      ''
      
        'GRUPO TRESS garantiza que el funcionamiento en condiciones norma' +
        'les de uso y '
      
        'operaci'#243'n,   EL SISTEMA se encuentra libre de errores. En caso d' +
        'e presentarse alg'#250'n error '
      
        'relacionado con el funcionamiento ordinario de EL SISTEMA, GRUPO' +
        ' TRESS llevar'#225' a cabo '
      'la reposici'#243'n del mismo. '
      ''
      
        'GRUPO TRESS renuncia al otorgamiento de cualquier otra garant'#237'a ' +
        'o condiciones, ya sea '
      
        'de manera expresa, impl'#237'cita o prevista en la legislaci'#243'n aplica' +
        'ble, en caso de ser '
      
        'procedente, presentadas  en este caso de manera enunciativa pero' +
        ' no limitativa: idoneidad '
      
        'de EL SISTEMA para un determinado fin no establecido en el prese' +
        'nte contrato, ausencia de '
      
        'virus inform'#225'ticos, ausencia de negligencia o falta de esfuerzo ' +
        'razonable. Adem'#225's no existe '
      
        'garant'#237'a sobre la condici'#243'n del derecho de uso de EL SISTEMA, po' +
        'r lo cual las infracciones '
      
        'cometidas por el uso indebido, son responsabilidad del Usuario F' +
        'inal. Lo anterior a'#250'n y '
      
        'cuando el Distribuidor hubiese declarado lo contrario o reconoci' +
        'do alguna de estas garant'#237'as, '
      'salvo que le sea fehacientemente imputable a GRUPO TRESS'
      ''
      
        'El Usuario final asume todos los riesgos que surjan por la utili' +
        'zaci'#243'n y/o rendimiento de EL '
      
        'SISTEMA en cada una de sus funciones, de acuerdo a las actividad' +
        'es y procesos que se '
      'realizan con los m'#243'dulos que lo componen.'
      ''
      
        'V. EXCLUSI'#211'N DE LOS DA'#209'OS. En ning'#250'n caso, de acuerdo a la legis' +
        'laci'#243'n vigente, '
      
        'GRUPO TRESS o sus DISTRIBUIDORES ser'#225'n responsables por NING'#218'N d' +
        'a'#241'o que sufra '
      
        'el Usuario Final, ya sea directo, incidental, consecuencial, san' +
        'cionador, personal, por '
      
        'p'#233'rdidas de beneficios, por pago indebido de n'#243'minas, por interr' +
        'upci'#243'n de procesos de '
      
        'negocios, por p'#233'rdida de informaci'#243'n confidencial y no confidenc' +
        'ial, por p'#233'rdida de '
      
        'privacidad de acuerdo a la Ley Federal de Protecci'#243'n al Consumid' +
        'or, por incumplimiento de '
      
        'obligaciones de la empresa con sus empleados y con terceros ajen' +
        'os a la misma, ya sea de '
      
        'buena fe y por incompetencia, por negligencia o por cualquier ot' +
        'ra raz'#243'n que pudiese surgir '
      
        'por el USO o IMPOSIBILIDAD DE USO de EL SISTEMA, a'#250'n en el caso ' +
        'de que se hubiera '
      
        'informado a GRUPO TRESS o a cualquier DISTRIBUIDOR de la posibil' +
        'idad de dichos '
      
        'da'#241'os. Dicha exclusi'#243'n de da'#241'os ser'#225' efectiva hasta en los casos' +
        ' de que alguna parte de EL '
      
        'SISTEMA presente o se descubran errores, ya que la obligaci'#243'n de' +
        ' GRUPO TRESS es '
      
        'reponer EL SISTEMA, pero en ning'#250'n caso hacerse responsable de l' +
        'as consecuencias que '
      
        'pudiera ocasionar cualesquiera de estos errores, salvo que le se' +
        'a imputable.'
      ''
      
        'VI. El sistema queda instalado dentro de los primeros cinco d'#237'as' +
        ' posteriores a la firma del '
      
        'contrato, para lo cual el Usuario Final deber'#225' pagar el costo es' +
        'tablecido en la cl'#225'usula  '
      'anterior  del presente contrato.'
      ''
      
        'VII. LIMITACI'#211'N Y LIBERACI'#211'N DE RESPONSABILIDAD. En este acto, G' +
        'RUPO TRESS, '
      
        'por este medio se libera de todas las responsabilidades que surj' +
        'an o se relacionen con todas '
      
        'las reclamaciones concernientes a EL SISTEMA, a su USO o a su IM' +
        'POSIBILIDAD DE USO, '
      
        'independientemente de los compromisos adquiridos por el Distribu' +
        'idor con el Usuario Final, '
      'salvo que le sea imputable a GRUPO TRESS.'
      ''
      
        'VIII. ACTUALIZACIONES. Usted como Usuario Final reconoce y convi' +
        'ene que la licencia no '
      
        'exclusiva concedida por GRUPO TRESS, '#250'nica y exclusivamente se r' +
        'efiere a EL SISTEMA, '
      
        'que actualmente es conocido en el mercado como Sistema TRESS. Po' +
        'r lo que usted '
      
        'reconoce y acepta que la licencia no exclusiva aqu'#237' otorgada no ' +
        'incluye ninguna '
      'actualizaci'#243'n o modificaci'#243'n del Sistema TRESS.'
      ''
      
        'GRUPO TRESS se reserva el derecho a reemplazar, actualizar o mod' +
        'ificar  EL SISTEMA, '
      
        'para en su caso informarle oportunamente acerca de cualquier act' +
        'ualizaci'#243'n, modificaci'#243'n o '
      
        'nueva versi'#243'n del Sistema TRESS que desarrolle, por escrito dent' +
        'ro de los quince d'#237'as '
      
        'siguientes a su modificaci'#243'n para efecto de que Usted adquiera o' +
        ' suscriba un nuevo contrato '
      
        'de licencia de uso no exclusivo con GRUPO TRESS bajo el cual la ' +
        'contraprestaci'#243'n '
      
        'respectiva pagadera a GRUPO TRESS, o a quien sus derechos repres' +
        'ente, sea equivalente '
      
        'a un valor justo de mercado,  mismo precio que estar'#225' a la vista' +
        ' del p'#250'blico  Usuario en la '
      'oficinas de GRUPO TRESS.'
      ''
      
        'En el caso de que GRUPO TRESS ofrezca un reemplazo o versi'#243'n mod' +
        'ificada o cualquier '
      
        'actualizaci'#243'n de EL SISTEMA, la continuidad en su uso (de EL SIS' +
        'TEMA) se condiciona a '
      
        'su aceptaci'#243'n de dicho reemplazo o versi'#243'n modificada o actualiz' +
        'aci'#243'n por cualquier nuevo '
      
        'contrato que lo acompa'#241'e que reemplace y anule '#233'ste, as'#237' como se' +
        ' dar'#225' por terminada la '
      'autorizaci'#243'n del uso de las versiones anteriores de EL SISTEMA.'
      ''
      
        'IX. EJECUTABILIDAD DE DERECHOS Y OBLIGACIONES. Se reconoce y  se' +
        ' acepta que la '
      
        'presente licencia de Usuario Final se apega a lo dispuesto por l' +
        'a Ley Federal del Derecho de '
      
        'Autor, por lo cual existe la obligaci'#243'n por parte del Usuario Fi' +
        'nal de no realizar ninguna de las '
      
        'acciones establecidas por el art'#237'culo 231 de la Ley en comento y' +
        ' por el C'#243'digo Penal '
      
        'Federal, caso contrario se estar'#225' en un evidente acto de mala fe' +
        '.'
      ''
      
        'X. TERMINACI'#211'N DE LA LICENCIA. La presente licencia de uso NO EX' +
        'CLUSIVA, podr'#225' '
      
        'darse por terminada en forma anticipada cuando se incumpla cuale' +
        'squiera de las '
      'obligaciones del presente contrato.'
      ''
      
        'En caso de incumplimiento por parte del Usuario Final en alguna ' +
        'de las obligaciones '
      
        'contenidas en  el presente contrato, se establece la pena conven' +
        'cional consistente en el '
      
        'equivalente al 90% (Noventa por ciento) del valor de mercado de ' +
        'EL SISTEMA. Lo anterior, '
      
        'independientemente de las acciones compensatorias que se juzguen' +
        ' convenientes '
      
        'relacionadas con infracciones administrativas o denuncias o quer' +
        'ellas penales.'
      ''
      
        'GRUPO TRESS puede dar por terminado este Contrato, al ofrecerle ' +
        'una nueva licencia de '
      
        'Usuario Final que sustituya y anule EL SISTEMA, para el caso de ' +
        'cualquier reemplazo o '
      
        'versi'#243'n modificada o actualizaci'#243'n de EL SISTEMA y condicionar l' +
        'a continuidad de su '
      
        'utilizaci'#243'n o de dicho reemplazo, modificaci'#243'n o versi'#243'n actuali' +
        'zada a su aceptaci'#243'n de dicho '
      'Contrato que lo sustituya y anule. '
      ''
      
        'En este supuesto, GRUPO TRESS puede dar por terminado este Contr' +
        'ato en el caso de que '
      
        'el Usuario Final reciba una notificaci'#243'n que establezca que qued' +
        'a prohibida la continuaci'#243'n '
      
        'del uso de EL SISTEMA. En el caso de que GRUPO TRESS d'#233' por term' +
        'inado este Contrato, '
      
        'debe dejar de utilizar EL SISTEMA inmediatamente y en su caso re' +
        'gresar a GRUPO TRESS, '
      
        'o destruir, todas las copias de EL SISTEMA as'#237' como sus partes q' +
        'ue lo integran.'
      ''
      
        'XI. LEYENDAS DE DERECHOS DE AUTOR. Las leyendas, DERECHO RESERVA' +
        'DO, D.R., '
      
        'nombre  del titular, domicilio, a'#241'o de primera publicaci'#243'n y a'#241'o' +
        ' actual, que aparezcan en EL '
      
        'SISTEMA deber'#225'n ser respetadas y en ning'#250'n momento borradas, cas' +
        'o contrario se estar'#237'a '
      
        'incurriendo en infracciones administrativa en materia de derecho' +
        ' de autor, contempladas por '
      'la Ley Federal del Derecho de Autor.'
      ''
      
        'XII. ACCIONES EJERCITADAS POR TERCEROS.  GRUPO TRESS se obliga a' +
        ' realizar '
      
        'todas las actividades necesarias para proteger al Usuario Final ' +
        'de las acciones ejercitadas '
      
        'por terceros que aleguen la TITULARIDAD sobre EL SISTEMA. De est' +
        'a manera, GRUPO '
      
        'TRESS se obliga a cubrir los gastos generados por la defensa de ' +
        'la titularidad de los '
      
        'derechos de EL SISTEMA y erogados por el Usuario Final. Se estab' +
        'lece que GRUPO '
      
        'TRESS en caso de que incumpla los t'#233'rminos del presente contrato' +
        ', independientemente de '
      
        'devolver las cantidades pagadas por adelantado deber'#225' pagar la p' +
        'ena convencional '
      'se'#241'alada en la cl'#225'usula XI del presente contrato.'
      ''
      
        'En ning'#250'n otro supuesto, GRUPO TRESS tiene la obligaci'#243'n de apoy' +
        'ar o proteger al Usuario '
      
        'Final de acciones de terceros, sino '#250'nicamente en los casos que ' +
        'se relacionen con '
      'TITULARIDAD DE DERECHOS AUTORALES.'
      ''
      
        'En el caso de que el Usuario Final, cometa, est'#233' cometiendo o ha' +
        'ya cometido, algunas de las '
      
        'infracciones contempladas por la Ley Federal del Derecho de Auto' +
        'r, para el caso de las '
      
        'infracciones en materia de comercio y en materia de derechos de ' +
        'autor, o por la Ley de la '
      
        'Propiedad Industrial, para el caso de las infracciones en materi' +
        'a de propiedad industrial, '
      
        'GRUPO TRESS se desliga de cualquier responsabilidad legal y que ' +
        'est'#233' debidamente '
      'comprobado.'
      ''
      
        'XIII. REGAL'#205'AS.  El Usuario Final se obliga a pagar por concepto' +
        ' de contraprestaci'#243'n a la '
      
        'licencia de uso no exclusiva, la cantidad acordada en los T'#201'RMIN' +
        'OS Y CONDICIONES DE '
      'LA VENTA.'
      ''
      
        'XIV. VIGENCIA,  RESCISI'#211'N Y REVOCACI'#211'N DE LA LICENCIA. La licenc' +
        'ia de uso no '
      
        'exclusiva otorgada al Usuario Final, estar'#225' vigente hasta en tan' +
        'to no sea revocada, '
      'rescindida o modificada por parte de GRUPO TRESS.'
      ''
      
        'GRUPO TRESS tiene el derecho de rescindir el contrato y por cons' +
        'ecuencia revocar la '
      
        'licencia, en cualquier momento que considere que el Usuario Fina' +
        'l ha incumplido con alguna '
      
        'de las obligaciones establecidas en el presente contrato, sin ne' +
        'cesidad de la declaraci'#243'n '
      
        'previa del Juez con solo notificar dicho incumplimiento al Usuar' +
        'io Final. Lo anterior '
      
        'independientemente de las acciones legales a que den lugar por e' +
        'l incumplimiento.'
      ''
      
        'XV. DERECHOS DE PROPIEDAD INDUSTRIAL E INTELECTUAL. El usuario f' +
        'inal reconoce '
      
        'que GRUPO TRESS es el '#250'nico y leg'#237'timo titular de los derechos p' +
        'atrimoniales de la obra de '
      
        'c'#243'mputo '#39#39'SISTEMA TRESS'#39#39', el cual se encuentra registrada ante ' +
        'el Instituto Nacional del '
      
        'Derecho de Autor, en la Direcci'#243'n de registro correspondiente,  ' +
        'y que en el caso de la '
      
        'licencia de uso limitada que se otorga en el presente, el resto ' +
        'de los derechos patrimoniales '
      'son exclusivos de '#39#39'SISTEMA TRESS'#39#39'.'
      ''
      
        'Asimismo, todos los signos distintivos utilizados para identific' +
        'ar EL SISTEMA, como es el caso '
      
        'de la marca registrada TRESS, TRESS Y DISE'#209'O, entre otros, son p' +
        'ropiedad de GRUPO '
      
        'TRESS, por ello el Usuario Final se obliga a llevar a cabo el us' +
        'o correcto de los signos '
      
        'distintivos en menci'#243'n, sin realizar modificaciones que alteren ' +
        'el car'#225'cter distintivo de los '
      'mismos.'
      ''
      
        'XVI. SECRETOS INDUSTRIALES. El Usuario Final se obliga a no divu' +
        'lgar, revelar, utilizar '
      
        'indebidamente, por cualquier medio, en beneficio propio de terce' +
        'ras personas, la informaci'#243'n '
      
        'confidencial, propiedad de GRUPO TRESS, que '#233'sta le transmita en' +
        ' la celebraci'#243'n del '
      
        'presente contrato o a la que por alg'#250'n otro motivo, causa o circ' +
        'unstancia tenga o haya tenido '
      
        'acceso, como pudiera ser el caso de la Documentaci'#243'n, C'#243'digo Fue' +
        'nte, as'#237' como el '
      'funcionamiento de EL SISTEMA.'
      ''
      
        'Por ello, El Usuario Final se obliga a mantener en absoluto secr' +
        'eto y completa '
      
        'confidencialidad, toda  la informaci'#243'n  que GRUPO TRESS le trans' +
        'mita o proporcione para el '
      'desempe'#241'o correcto de EL SISTEMA. '
      ''
      
        'Asimismo, el Usuario Final tiene obligaci'#243'n de celebrar contrato' +
        's de confidencialidad con los '
      
        'empleados que tengan acceso a la informaci'#243'n confidencial. Caso ' +
        'contrario estaremos en '
      
        'presencia de un evidente incumplimiento por parte del Usuario Fi' +
        'nal.'
      ''
      
        'El simple inicio de una relaci'#243'n comercial o contractual de cual' +
        'quier naturaleza, por parte de '
      
        'usted o cualesquiera de sus empleados, con un tercero que se ded' +
        'ique a igual o similar giro '
      
        'que GRUPO TRESS, har'#225' presumir salvo prueba en contrario, que El' +
        ' Usuario Final, por si '
      
        'mismo o trav'#233's de sus empleados comunic'#243' la informaci'#243'n confiden' +
        'cial referida en los '
      'p'#225'rrafos precedentes.'
      ''
      'XVII. DOMICILIOS:'
      
        'GRUPO TRESS establece como domicilio para o'#237'r y recibir notifica' +
        'ciones el ubicado en '
      
        'CARRETERA AL AEROPUERTO n'#250'mero 1900 LOCAL M-2, CENTRO COMERCIAL ' +
        'OTAY, '
      'TIJUANA BAJA CALIFORNIA, MEXICO.'
      ''
      ''
      
        'EL USUARIO FINAL establece como domicilio para o'#237'r y recibir not' +
        'ificaciones el que se '
      
        'establezca en la factura emitida por GRUPO TRES INTERNACIONAL a ' +
        'favor del Usuario '
      'Final. '
      ''
      
        'XVIII. Para efectos de prevenir una controversia futura, se conv' +
        'iene en otorgarse las '
      'siguientes concesiones:'
      ''
      
        '1. Al momento de ser instalado EL SISTEMA en los dispositivos el' +
        'ectr'#243'nicos de su empresa; '
      
        'al ser entregados los elementos para su instalaci'#243'n; o al ser ad' +
        'quirida la licencia de uso no '
      'exclusiva, se tendr'#225' por otorgado el consentimiento.'
      ''
      
        '2. La recepci'#243'n de este documento y la entrega del sistema impli' +
        'ca la aceptaci'#243'n del '
      'presente contrato.'
      ''
      
        '3. Cualquier uso indebido de EL SISTEMA llevado a cabo por emple' +
        'ados del usuario final, se '
      
        'tendr'#225' por realizado por parte del mismo Usuario Final, para los' +
        ' efectos legales a que haya '
      'lugar.'
      ''
      
        'XIX. COMPETENCIA Y JURISDICCI'#211'N. Ambas partes acuerdan que para ' +
        'cualquier '
      
        'controversia que pudiera surgir con motivo de la interpretaci'#243'n ' +
        'y ejecuci'#243'n del presente '
      
        'contrato y licencia de uso y garant'#237'a de sistema, se someter'#225'n a' +
        ' la Competencia de la '
      
        'Procuradur'#237'a Federal del Consumidor o autoridad administrativa q' +
        'ue corresponda por la Ley '
      
        'de la Propiedad Industrial, y en caso de susbsistir controversia' +
        ' se someten a la jurisdicci'#243'n de '
      
        'los tribunales de la ciudad de Tijuana, B.C., renunciando a aque' +
        'llos domicilios  que pudieran '
      'corresponderles por raz'#243'n de sus domicilios presentes o futuros.'
      ''
      ''
      
        'Con base en lo establecido en el Cap'#237'tulo X de la Ley Federal de' +
        ' Protecci'#243'n al Consumidor, '
      
        'el presente contrato ha quedado debidamente registrado  ante la ' +
        'Procuradur'#237'a Federal de '
      
        'Protecci'#243'n al Consumidor con fecha 14 de diciembre de 2001, bajo' +
        ' el n'#250'mero 44333, libro  1, '
      
        'volumen 14 a fojas  104, surtiendo sus efectos en todos sus t'#233'rm' +
        'inos.')
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 1
  end
  object PanelUsuario: TPanel
    Left = 0
    Top = 0
    Width = 464
    Height = 73
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object gbusuario: TGroupBox
      Left = 0
      Top = 0
      Width = 464
      Height = 73
      Align = alClient
      Caption = ' Informaci'#243'n Registrada por la Instalaci'#243'n '
      TabOrder = 0
      object lblUsuario: TLabel
        Left = 38
        Top = 17
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Usuario:'
      end
      object lblPlataforma: TLabel
        Left = 24
        Top = 33
        Width = 53
        Height = 13
        Alignment = taRightJustify
        Caption = 'Plataforma:'
      end
      object lblFecha: TLabel
        Left = 10
        Top = 49
        Width = 67
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha y Hora:'
      end
      object Usuariolbl: TLabel
        Left = 80
        Top = 17
        Width = 3
        Height = 13
      end
      object Fechalbl: TLabel
        Left = 80
        Top = 49
        Width = 3
        Height = 13
      end
      object Plataformalbl: TLabel
        Left = 80
        Top = 33
        Width = 3
        Height = 13
      end
    end
  end
  object SaveDialog: TSaveDialog
    DefaultExt = '.doc'
    FileName = 'Licencia'
    Filter = 
      'DOC Texto ( *.doc )|*.doc|RTF Texto ( *.rtf )|*.rtf|Todos ( *.* ' +
      ')|*.*'
    InitialDir = 'C:\'
    Options = [ofOverwritePrompt, ofPathMustExist, ofFileMustExist]
    Left = 8
    Top = 341
  end
end
