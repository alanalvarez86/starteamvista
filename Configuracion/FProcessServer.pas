unit FProcessServer;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, ExtCtrls,
     Controls, Forms, Dialogs, FileCtrl, StdCtrls, Buttons,
     ZetaRegistryServer,
     ZBaseDlgModal,
     ZetaDBTextBox;

type
  TProcessServer = class(TZetaDlgModal)
    BinariosLBL: TLabel;
    Binarios: TEdit;
    BinariosSeek: TSpeedButton;
    VersionLBL: TLabel;
    Bevel: TBevel;
    AsistenciaLBL: TLabel;
    Asistencia: TZetaTextBox;
    Cafeteria: TZetaTextBox;
    CafeteriaLBL: TLabel;
    CafeteraLBL: TLabel;
    Cafetera: TZetaTextBox;
    Catalogos: TZetaTextBox;
    CatalogosLBL: TLabel;
    ConsultasLBL: TLabel;
    Consultas: TZetaTextBox;
    Diccionario: TZetaTextBox;
    DiccionarioLBL: TLabel;
    GlobalesLBL: TLabel;
    Globales: TZetaTextBox;
    IMSS: TZetaTextBox;
    IMSSlbl: TLabel;
    LaborLBL: TLabel;
    Labor: TZetaTextBox;
    Login: TZetaTextBox;
    LoginLBL: TLabel;
    NominaLBL: TLabel;
    Nomina: TZetaTextBox;
    NominaAnuales: TZetaTextBox;
    NominaAnualesLBL: TLabel;
    NominaCalculoLBL: TLabel;
    NominaCalculo: TZetaTextBox;
    Recursos: TZetaTextBox;
    RecursosLBL: TLabel;
    ReportesLBL: TLabel;
    Reportes: TZetaTextBox;
    Sistema: TZetaTextBox;
    SistemaLBL: TLabel;
    SupervisoresLBL: TLabel;
    Supervisores: TZetaTextBox;
    Tablas: TZetaTextBox;
    TablasLBL: TLabel;
    PortalTress: TZetaTextBox;
    PortalTressLBL: TLabel;
    GlobalSelLBL: TLabel;
    GlobalSeleccion: TZetaTextBox;
    Seleccion: TZetaTextBox;
    SeleccionLBL: TLabel;
    SelReportesLBL: TLabel;
    SelReportes: TZetaTextBox;
    ServicioMedico: TZetaTextBox;
    ServicioMedicoLBL: TLabel;
    CajaAhorroLBL: TLabel;
    CajaAhorro: TZetaTextBox;
    GlobalVisLBL: TLabel;
    GlobalVisitantes: TZetaTextBox;
    PresupuestosLBL: TLabel;
    Presupuestos: TZetaTextBox;
    ReporteadorLBL: TLabel;
    Reporteador: TZetaTextBox;
    VisitantesLBL: TLabel;
    Visitantes: TZetaTextBox;
    VisReportesLBL: TLabel;
    VisReportes: TZetaTextBox;
    procedure BinariosSeekClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BinariosChange(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure BinariosExit(Sender: TObject);
  private
    function GetRegistry: TZetaRegistryServer;
    function GetBinaryDirectory: String;
  private
    { Private declarations }
    FChanged: Boolean;
    FPath: String;
    property Registry: TZetaRegistryServer read GetRegistry;
    procedure GetDLLVersions;
  public
    { Public declarations }
  end;

var
  ProcessServer: TProcessServer;

procedure ShowProcessServerInfo;

implementation

uses DDBConfig,
     FHelpContext,
     ZetaCommonClasses,
     ZetaWinAPITools;

{$R *.DFM}

procedure ShowProcessServerInfo;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with TProcessServer.Create( Application ) do
        begin
             try
                ShowModal;
             finally
                    Free;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

{ ****** TProcessServer ********* }

procedure TProcessServer.FormShow(Sender: TObject);
begin
     inherited;
     with Registry do
     begin
          Self.Binarios.Text := Binarios;
     end;
     FChanged := False;
     FPath := '';
     with OK do
     begin
          Enabled := False;
          Visible := False;
     end;
     with Cancelar do
     begin
          Kind := bkClose;
          Hint := 'Salir De Esta Ventana';
          Caption := '&Salir';
     end;
     GetDLLVersions;
     HelpContext := H00016_Servidor_de_procesos;
end;

function TProcessServer.GetRegistry: TZetaRegistryServer;
begin
     Result := dmDBConfig.Registry;
end;

function TProcessServer.GetBinaryDirectory: String;
begin
     Result := Self.Binarios.Text;
end;

procedure TProcessServer.GetDLLVersions;
var
   oCursor: TCursor;
   sPath: String;

procedure GetTressVersion( const sFileName: String; Control, Letrero: TLabel );
var
   sHint, sVersion: String;
begin
     sHint := ZetaWinAPITools.FormatDirectory( sPath ) + sFileName;
     sVersion := ZetaWinAPITools.GetVersionInfoComments( sHint );
     with Control do
     begin
          Caption := sVersion;
          Hint := sHint;
     end;
     with Letrero do
     begin
          Hint := sHint;
     end;
end;

begin
     //MV-29/Nov/2002: Se incluyeron las Dlls de Selecci�n y de Servicio M�dico
     sPath := GetBinaryDirectory;
     if ( sPath <> FPath ) then
     begin
          FPath := sPath;
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             GetTressVersion( 'Asistencia.dll', Asistencia, AsistenciaLBL );
             GetTressVersion( 'Cafeteria.dll', Cafeteria, CafeteriaLBL );
             GetTressVersion( 'Cafetera.exe', Cafetera, CafeteraLBL );
             GetTressVersion( 'CajaAhorro.dll', CajaAhorro, CajaAhorroLBL );
             GetTressVersion( 'Catalogos.dll', Catalogos, CatalogosLBL );
             GetTressVersion( 'Consultas.dll', Consultas, ConsultasLBL );
             GetTressVersion( 'Diccionario.dll', Diccionario, DiccionarioLBL );
             GetTressVersion( 'Global.dll', Globales, GlobalesLBL );
             GetTressVersion( 'GlobalSeleccion.dll', GlobalSeleccion, GlobalSelLBL );
             GetTressVersion( 'GlobalVisitantes.dll', GlobalVisitantes, GlobalVisLBL );
             GetTressVersion( 'Imss.dll', IMSS, IMSSLBL );
             GetTressVersion( 'Labor.dll', Labor, LaborLBL );
             GetTressVersion( 'Login.dll', Login, LoginLBL );
             GetTressVersion( 'Nomina.dll', Nomina, NominaLBL );
             GetTressVersion( 'DAnualNomina.dll', NominaAnuales, NominaAnualesLBL );
             GetTressVersion( 'DCalcNomina.dll', NominaCalculo, NominaCalculoLBL );
             GetTressVersion( 'PortalTress.dll', PortalTress, PortalTressLBL );
             GetTressVersion( 'Presupuestos.dll', Presupuestos, PresupuestosLBL );
             GetTressVersion( 'Recursos.dll', Recursos, RecursosLBL );
             GetTressVersion( 'Reportes.dll', Reportes, ReportesLBL );
             GetTressVersion( 'Reporteador.dll', Reporteador, ReporteadorLBL );
             GetTressVersion( 'Seleccion.dll', Seleccion, SeleccionLBL );
             GetTressVersion( 'SelReportes.dll', SelReportes, SelReportesLBL );
             GetTressVersion( 'ServicioMedico.dll', ServicioMedico, ServicioMedicoLBL );
             GetTressVersion( 'Sistema.dll', Sistema, SistemaLBL );
             GetTressVersion( 'Super.dll', Supervisores, SupervisoresLBL );
             GetTressVersion( 'Tablas.dll', Tablas, TablasLBL );
             GetTressVersion( 'Visitantes.dll', Visitantes, VisitantesLBL );
             GetTressVersion( 'VisReportes.dll', VisReportes, VisReportesLBL );
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TProcessServer.BinariosChange(Sender: TObject);
begin
     inherited;
     if not FChanged then
     begin
          FChanged := True;
          with OK do
          begin
               Enabled := True;
               Visible := True;
          end;
          with Cancelar do
          begin
               Kind := bkCancel;
               Hint := 'Cancelar Cambios y Salir De Esta Ventana';
               Caption := '&Cancelar';
          end;
     end;
end;

procedure TProcessServer.BinariosExit(Sender: TObject);
begin
     inherited;
     GetDLLVersions;
end;

procedure TProcessServer.BinariosSeekClick(Sender: TObject);
var
   sArchivo: String;
begin
     inherited;
     with Binarios do
     begin
          sArchivo := Text;
          if FileCtrl.SelectDirectory( sArchivo, [], 0 ) then
          begin
               Text := sArchivo;
               GetDLLVersions;
          end;
     end;
end;

procedure TProcessServer.OKClick(Sender: TObject);
begin
     inherited;
     with Registry do
     begin
          Binarios := GetBinaryDirectory;
     end;
end;

end.
