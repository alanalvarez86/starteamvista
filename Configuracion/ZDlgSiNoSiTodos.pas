unit ZDlgSiNoSiTodos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls;

type
  TZDlgSiNoSiATodos = class(TForm)
    Label1: TLabel;
    Image1: TImage;
    OK: TBitBtn;
    Cancelar: TBitBtn;
    Todos: TBitBtn;
    Label2: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
   ZDlgSiNoSiATodos: TZDlgSiNoSiATodos;

function SiNoSiTodos: TModalResult;

implementation

{$R *.DFM}

function SiNoSiTodos: TModalResult;
begin
     if ( ZDlgSiNoSiATodos = nil ) then
     begin
          ZDlgSiNoSiATodos := TZDlgSiNoSiATodos.Create( Application.Mainform ); // Se destruye al Salir del Programa //
     end;
     with ZDlgSiNoSiATodos do
     begin
          ShowModal;
          Result := ModalResult;
     end;
end;

end.
