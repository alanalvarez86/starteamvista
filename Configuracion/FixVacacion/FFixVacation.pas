unit FFixVacation;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, ShellApi,
     Forms, Dialogs, Buttons, StdCtrls, FileCtrl, DB, DBTables, ComCtrls;

type
  TFixVacacion = class(TForm)
    Arreglar: TBitBtn;
    Salir: TBitBtn;
    TressDOS: TGroupBox;
    DOSPathLBL: TLabel;
    Path: TEdit;
    PathSeek: TSpeedButton;
    TressWindows: TGroupBox;
    AliasLBL: TLabel;
    Alias: TComboBox;
    UserNameLBL: TLabel;
    Usuario: TEdit;
    Password: TEdit;
    PasswordLBL: TLabel;
    dbTress: TDatabase;
    dbDOS: TDatabase;
    Source: TQuery;
    TargetQuery: TQuery;
    tqUno: TQuery;
    tqDos: TQuery;
    tqTres: TQuery;
    tqCuatro: TQuery;
    tqCinco: TQuery;
    tqSeis: TQuery;
    StatusBar: TStatusBar;
    Cierres: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure PathSeekClick(Sender: TObject);
    procedure ArreglarClick(Sender: TObject);
    procedure SalirClick(Sender: TObject);
    procedure CierresClick(Sender: TObject);
  private
    { Private declarations }
    FLog: TStrings;
    FErrores: Integer;
    function ConectarDOS( const sPath: String ): Boolean;
    function ConectarWindows( const sAlias, sUsuario, sPassword: String ): Boolean;
    procedure ArreglaColabora;
    procedure BorraVacaciones;
    procedure MigraVacaciones;
    procedure CierraVacaciones;
    procedure DesconectarDOS;
    procedure DesconectarWindows;
    procedure MigrarFechaQuery( Query: TQuery; const sTargetField: String; const Value: TDate );
    procedure MigrarFecha( const sTargetField: String; const Value: TDate );
    procedure MuestraError( Sender: TObject; Error: Exception );
    procedure ClearParameters( Query: TQuery );
    procedure PreparaQuery( Query: TQuery; const sScript: String );
    procedure ShowStatus( const sValue: String );
    procedure EmpezarTransaccion;
    procedure TerminarTransaccion( const lCommit: Boolean );
    procedure InitLog;
    procedure LogError( const sMensaje: String; Error: Exception );
    procedure LogEvent( const sMensaje: String );
    procedure CloseLog( sFileName: String );
  public
    { Public declarations }
  end;

var
  FixVacacion: TFixVacacion;

implementation

uses ZetaCommonTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaDialogo;

{$R *.DFM}

procedure SetToNull( Parametro: TParam; Tipo: TFieldType );
begin
     with Parametro do
     begin
          DataType := Tipo;
          Clear;
          Bound := True;
     end;
end;

function SetFileNamePath( const sPath, sFile: String ): String;
begin
     Result := sPath;
     if ( Result[ Length( Result ) ] <> '\' ) then
        Result := Result + '\';
     Result := Result + sFile;
end;

function SetFileNameDefaultPath( const sFile: String ): String;
begin
     Result := SetFileNamePath( ExtractFileDir( Application.ExeName ), sFile );
end;

function ExecuteFile( const FileName, Params, DefaultDir: String; ShowCmd: Integer ): THandle;
var
   zFileName, zParams, zDir: array[0..79] of Char;
begin
     Result := ShellApi.ShellExecute( Application.MainForm.Handle,
                             nil,
                             StrPCopy( zFileName, FileName ),
                             StrPCopy( zParams, Params ),
                             StrPCopy( zDir, DefaultDir ),
                             ShowCmd );
end;

{ ******************* }

procedure TFixVacacion.FormCreate(Sender: TObject);
var
   OldConfigMode: TConfigMode;
begin
     Application.OnException := MuestraError;
     with Alias do
     begin
          with Session do
          begin
               OldConfigMode := ConfigMode;
               ConfigMode := [ cfmPersistent ];
               if not Active then
                  Active := True;
               NetFileDir := ExtractFilePath( Application.ExeName );
               PrivateDir := ExtractFilePath( Application.ExeName );
               GetAliasNames( Items );
               ConfigMode := OldConfigMode;
          end;
          ItemIndex := 0;
     end;
     FLog := TStringList.Create;
end;

procedure TFixVacacion.FormDestroy(Sender: TObject);
begin
     FLog.Free;
end;

procedure TFixVacacion.InitLog;
begin
     FErrores := 0;
     with FLog do
     begin
          BeginUpdate;
          Clear;
     end;
end;

procedure TFixVacacion.LogError( const sMensaje: String; Error: Exception );
var
   i, iLastCategory, iLastErrorCode: Integer;
begin
     with FLog do
     begin
          BeginUpdate;
          Add( sMensaje + ':' );
          if Assigned( Error ) then
          begin
               if ( Error is EDBEngineError ) then
               begin
                    with Error as EDBEngineError do
                    begin
                         iLastCategory := 0;
                         iLastErrorCode := 0;
                         for i := 0 to ( ErrorCount - 1 ) do
                         begin
                              with Errors[ i ] do
                              begin
                                   if ( iLastCategory = Category ) and ( iLastErrorCode = ErrorCode ) then
                                      Add( Message )
                                   else
                                       Add( IntToStr( i + 1 ) +
                                            ': Categor�a = ' +
                                            IntToStr( Category ) +
                                            ', Error = ' +
                                            IntToStr( ErrorCode ) +
                                            CR_LF +
                                            Message );
                                   iLastCategory := Category;
                                   iLastErrorCode := ErrorCode;
                              end;
                         end;
                    end;
               end
               else
               begin
                    with Error as Exception do
                    begin
                         Add( ClassName + ' = ' + Message );
                    end;
               end;
          end;
          Add( StringOfChar( '-', 20 ) );
          EndUpdate;
     end;
     FErrores := FErrores + 1;
end;

procedure TFixVacacion.LogEvent( const sMensaje: String );
begin
     with FLog do
     begin
          Add( StringOfChar( '*', 5 ) + ' ' + sMensaje + ' ' + StringOfChar( '*', 5 ) );
     end;
end;

procedure TFixVacacion.CloseLog( sFileName: String );
begin
     sFileName := SetFileNameDefaultPath( sFileName );
     with FLog do
     begin
          EndUpdate;
          SaveToFile( sFileName );
     end;
     if ( FErrores > 0 ) then
     begin
          if zErrorConfirm( '� El Proceso Ha Terminado Con Errores !',
                            'Se Encontraron ' + Trim( Format( '%10.0n', [ FErrores / 1 ] ) ) + ' Errores' +
                            CR_LF +
                            CR_LF +
                            'La Bit�cora Ha Sido Guardada En El Archivo' +
                            CR_LF +
                            sFileName +
                            CR_LF +
                            CR_LF +
                            '� Desea Ver La Bit�cora Del Proceso ?', 0, mbOk ) then
          begin
               ExecuteFile( 'NOTEPAD.EXE', ExtractFileName( sFileName ), ExtractFilePath( sFileName ), SW_SHOWDEFAULT );
          end;
     end
     else
         zInformation( Caption, 'Proceso Terminado Con Exito', 0 );
end;

procedure TFixVacacion.MuestraError( Sender: TObject; Error: Exception );
begin
     zExcepcion( '� Error En ' + Caption + ' !', '� Se Encontr� Un Error !', Error, 0 );
end;

procedure TFixVacacion.PathSeekClick(Sender: TObject);
var
   sPath: String;
begin
     with Path do
     begin
          sPath := Text;
          if SelectDirectory( sPath, [], 0 ) then
             Text := sPath;
     end;
end;

function TFixVacacion.ConectarDOS( const sPath: String ): Boolean;
begin
     try
        with dbDOS do
        begin
             Connected := False;
             with Params do
             begin
                  Values[ 'PATH' ] := sPath;
             end;
             Connected := True;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                DesconectarDOS;
                Application.HandleException( Error );
                Result := False;
           end;
     end;
end;

procedure TFixVacacion.DesconectarDOS;
begin
     with dbTress do
     begin
          Connected := False;
     end;
end;

function TFixVacacion.ConectarWindows( const sAlias, sUsuario, sPassword: String ): Boolean;
begin
     try
        with dbTress do
        begin
             Connected := False;
             AliasName := sAlias;
             with Params do
             begin
                  Values[ 'USER NAME' ] := sUsuario;
                  Values[ 'PASSWORD' ] := sPassword;
             end;
             Connected := True;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                DesconectarWindows;
                Application.HandleException( Error );
                Result := False;
           end;
     end;
end;

procedure TFixVacacion.DesconectarWindows;
begin
     with dbTress do
     begin
          Connected := False;
     end;
end;

procedure TFixVacacion.EmpezarTransaccion;
begin
     with dbTress do
     begin
          if not InTransaction then
             StartTransaction;
     end;
end;

procedure TFixVacacion.TerminarTransaccion( const lCommit: Boolean );
begin
     with dbTress do
     begin
          if InTransaction then
          begin
               if lCommit then
                  Commit
               else
                   RollBack;
          end;
     end;
end;

procedure TFixVacacion.ClearParameters( Query: TQuery );
var
   i: Integer;
begin
     with Query do
     begin
          for i := ( Params.Count - 1 ) downto 0 do
          begin
               SetToNull( Params.Items[ i ], Params.Items[ i ].DataType );
          end;
     end;
end;

procedure TFixVacacion.MigrarFechaQuery( Query: TQuery; const sTargetField: String; const Value: TDate );
begin
     with Query do
     begin
          ParamByName( sTargetField ).AsDateTime := Value;
     end;
end;

procedure TFixVacacion.MigrarFecha( const sTargetField: String; const Value: TDate );
begin
     MigrarFechaQuery( TargetQuery, sTargetField, Value );
end;

procedure TFixVacacion.PreparaQuery( Query: TQuery; const sScript: String );
begin
     with Query do
     begin
          Active := False;
          with SQL do
          begin
               Clear;
               Add( sScript );
          end;
     end;
end;

procedure TFixVacacion.ShowStatus( const sValue: String );
begin
     StatusBar.SimpleText := sValue;
     Application.ProcessMessages;
end;

procedure TFixVacacion.ArreglaColabora;
var
   iCtr, iEmpleado: Integer;
   rValor: Extended;
begin
     PreparaQuery( Source, 'select CB_CODIGO, CB_V_PA_0, CB_V_PA_1, CB_V_PA_2 from COLABORA.DBF' );
     PreparaQuery( TargetQuery, 'update COLABORA set CB_V_PAGO = :CB_V_PAGO where ( CB_CODIGO = :CB_CODIGO )' );
     with Source do
     begin
          iCtr := 0;
          Active := True;
          while not EOF do
          begin
               iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
               rValor := FieldByName( 'CB_V_PA_0' ).AsFloat +
                         FieldByName( 'CB_V_PA_1' ).AsFloat +
                         FieldByName( 'CB_V_PA_2' ).AsFloat;
               EmpezarTransaccion;
               try
                  ClearParameters( TargetQuery );
                  with TargetQuery do
                  begin
                       ParamByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
                       ParamByName( 'CB_V_PAGO' ).AsFloat := rValor;
                       ExecSQL;
                  end;
                  TerminarTransaccion( True );
               except
                     on Error: Exception do
                     begin
                          TerminarTransaccion( False );
                          LogError( 'Error al Corregir Empleado ' + IntToStr( iEmpleado ), Error );
                     end;
               end;
               if ( iCtr > 10 ) then
               begin
                    ShowStatus( 'Empleado # ' + IntToStr( iEmpleado ) );
                    iCtr := 0;
               end;
               iCtr := iCtr + 1;
               Next;
          end;
          Active := False;
     end;
end;

procedure TFixVacacion.BorraVacaciones;
begin
     try
        PreparaQuery( TargetQuery, 'delete from VACACION' );
        with TargetQuery do
        begin
             ExecSQL;
        end;
     except
           on Error: Exception do
           begin
                LogError( 'Error al Borrar VACACION', Error );
           end;
     end;
end;

procedure TFixVacacion.MigraVacaciones;
var
   sCargo: String;
   iCtr, iYear, iNumero: Integer;
   rValor, rTotal: Extended;

procedure MigrarStringKey( const sTargetField: String; const sValor: String );
begin
     with TargetQuery.ParamByName( sTargetField ) do
     begin
          if StrLleno( sValor ) then
             AsString := sValor
          else
              AsString := '';
     end;
end;

procedure MigrarCodigoEmpleado;
begin
     TargetQuery.ParamByName( 'CB_CODIGO' ).AsInteger := Source.FieldByName( 'CB_CODIGO' ).AsInteger;
end;

function Limita( const sValor: String; iLen: Word ): String;
begin
     Result := Copy( Trim( sValor ), 1, iLen );
end;

begin
     PreparaQuery( Source, 'select * from INC_VACA.DBF' );
     PreparaQuery( TargetQuery, 'insert into VACACION (CB_CODIGO, VA_FEC_INI, VA_TIPO, '+
                          'VA_FEC_FIN, VA_COMENTA, VA_CAPTURA, US_CODIGO, VA_D_PAGO, VA_PAGO, VA_S_PAGO, '+
                          'VA_D_GOZO, VA_GOZO, CB_SALARIO, CB_TABLASS, VA_NOMYEAR, VA_NOMTIPO, '+
                          'VA_NOMNUME, VA_YEAR, VA_MONTO, VA_SEVEN, VA_TASA_PR, VA_PRIMA, VA_OTROS, '+
                          'VA_TOTAL, VA_PERIODO, VA_GLOBAL ) values ( '+
                          ':CB_CODIGO, :VA_FEC_INI, :VA_TIPO, '+
                          ':VA_FEC_FIN, :VA_COMENTA, :VA_CAPTURA, :US_CODIGO, :VA_D_PAGO, :VA_PAGO, :VA_S_PAGO, '+
                          ':VA_D_GOZO, :VA_GOZO, :CB_SALARIO, :CB_TABLASS, :VA_NOMYEAR, :VA_NOMTIPO, '+
                          ':VA_NOMNUME, :VA_YEAR, :VA_MONTO, :VA_SEVEN, :VA_TASA_PR, :VA_PRIMA, :VA_OTROS, '+
                          ':VA_TOTAL, :VA_PERIODO, :VA_GLOBAL )' );
     with Source do
     begin
          iCtr := 0;
          Active := True;
          while not EOF do
          begin
               ClearParameters( TargetQuery );
               sCargo := FieldByName( 'IN_CARGO' ).AsString;
               MigrarFecha( 'VA_FEC_INI', FieldByName( 'IN_FEC_INI' ).AsDateTime );
               MigrarFecha( 'VA_FEC_FIN', FieldByName( 'IN_FEC_FIN' ).AsDateTime );
               MigrarFecha( 'VA_CAPTURA', NullDateTime );
               MigrarStringKey( 'CB_TABLASS', FieldByName( 'CB_TABLASS' ).AsString );
               iYear := StrToIntDef( Copy( sCargo, 1, 4 ), 0 );
               iNumero := StrToIntDef( Copy( sCargo, 5, 3 ), 0 );
               rValor := FieldByName( 'IN_TASA_IP' ).AsFloat;
               MigrarCodigoEmpleado;
               EmpezarTransaccion;
               try
                  with TargetQuery do
                  begin
                       ParamByName( 'VA_TIPO' ).AsInteger := Ord( tvVacaciones );
                       ParamByName( 'US_CODIGO' ).AsInteger := 0;
                       ParamByName( 'VA_PAGO' ).AsFloat := rValor;
                       ParamByName( 'VA_S_PAGO' ).AsInteger := 0;
                       ParamByName( 'VA_D_PAGO' ).AsInteger := 0;
                       ParamByName( 'VA_D_GOZO' ).AsInteger := 0;
                       ParamByName( 'VA_GOZO' ).AsFloat := Source.FieldByName( 'IN_DIAS' ).AsFloat;
                       ParamByName( 'VA_NOMYEAR' ).AsInteger := iYear;
                       ParamByName( 'VA_NOMTIPO' ).AsInteger := Ord( tpSemanal );
                       ParamByName( 'VA_NOMNUME' ).AsInteger := iNumero;
                       ParamByName( 'VA_YEAR' ).AsInteger := 0;
                       ParamByName( 'VA_GLOBAL' ).AsString := K_GLOBAL_NO;
                       ParamByName( 'VA_TASA_PR' ).AsFloat := Source.FieldByName( 'VA_TASA_PR' ).AsFloat;
                       rValor := Source.FieldByName( 'VA_MONTO' ).AsFloat;
                       rTotal := rValor;
                       ParamByName( 'VA_MONTO' ).AsFloat := rValor;
                       rValor := Source.FieldByName( 'VA_SEVEN' ).AsFloat;
                       rTotal := rTotal + rValor;
                       ParamByName( 'VA_SEVEN' ).AsFloat := rValor;
                       rValor := Source.FieldByName( 'VA_PRIMA' ).AsFloat;
                       rTotal := rTotal + rValor;
                       ParamByName( 'VA_PRIMA' ).AsFloat := rValor;
                       rValor := Source.FieldByName( 'VA_OTROS_M' ).AsFloat;
                       rTotal := rTotal + rValor;
                       ParamByName( 'VA_OTROS' ).AsFloat := rValor;
                       ParamByName( 'VA_TOTAL' ).AsFloat := rTotal;
                       ParamByName( 'VA_PERIODO' ).AsString := Source.FieldByName( 'VA_TRA_DEL' ).AsString +
                                                               ' A ' +
                                                               Source.FieldByName( 'VA_TRA_AL' ).AsString;
                       ParamByName( 'VA_COMENTA' ).AsString := Limita( Source.FieldByName( 'IN_COMENTA' ).AsString, 30 );
                       ParamByName( 'CB_SALARIO' ).AsFloat := Source.FieldByName( 'CB_SALARIO' ).AsFloat;
                       ExecSQL;
                  end;
                  TerminarTransaccion( True );
               except
                     on Error: Exception do
                     begin
                          TerminarTransaccion( False );
                          LogError( 'Error al Migrar Vacaci�n', Error );
                     end;
               end;
               if ( iCtr > 10 ) then
               begin
                    ShowStatus( 'Vacaci�n # ' + IntToStr( iCtr ) );
                    iCtr := 0;
               end;
               iCtr := iCtr + 1;
               Next;
          end;
     end;
end;

procedure TFixVacacion.CierraVacaciones;
var
   rSaldoPago, rSumaGozo, rDerecho: Extended;
   dPrimero, dCierre, dDerecho: TDate;
   iCtr, iCodigo: Integer;
begin
     PreparaQuery( tqUno, 'select SUM( V.VA_PAGO ) as VA_PAGO, ' +
                          'SUM( V.VA_GOZO ) as VA_GOZO, ' +
                          'MIN( V.VA_FEC_INI ) as VA_FEC_INI, ' +
                          'V.CB_CODIGO as CB_CODIGO, ' +
                          'C.CB_V_PAGO as CB_V_PAGO, ' +
                          'C.CB_V_GOZO as CB_V_GOZO, ' +
                          'C.CB_DER_PAG as CB_DER_PAG, ' +
                          'C.CB_DER_FEC as CB_DER_FEC ' +
                          'from VACACION V, COLABORA C where ' +
                          '( V.VA_TIPO = 1 ) and ' +
                          '( C.CB_CODIGO = V.CB_CODIGO ) and ' +
                          '( V.VA_FEC_INI >= ( select C2.CB_FEC_ANT from COLABORA C2 where ( C2.CB_CODIGO = V.CB_CODIGO ) ) ) ' +
                          'group by V.CB_CODIGO, C.CB_V_PAGO, C.CB_V_GOZO, C.CB_DER_PAG, C.CB_DER_FEC' );
     PreparaQuery( tqDos, 'insert into VACACION ' +
                          '( CB_CODIGO, VA_FEC_INI, VA_TIPO, VA_COMENTA, VA_D_PAGO, VA_D_GOZO ) values ' +
                          '(:CB_CODIGO,:VA_FEC_INI,:VA_TIPO,:VA_COMENTA,:VA_D_PAGO,:VA_D_GOZO )' );
     PreparaQuery( tqTres, 'insert into VACACION ' +
                           '( CB_CODIGO, VA_FEC_INI, VA_FEC_FIN, VA_TIPO, VA_COMENTA, VA_PAGO, VA_GOZO ) values ' +
                           '(:CB_CODIGO,:VA_FEC_INI,:VA_FEC_FIN,:VA_TIPO,:VA_COMENTA,:VA_PAGO,:VA_GOZO )' );
     PreparaQuery( tqCuatro, 'update COLABORA set CB_V_PAGO = :CB_V_PAGO ' +
                             'where ( CB_CODIGO = :CB_CODIGO )' );
     PreparaQuery( tqCinco, 'update COLABORA set CB_V_GOZO = :CB_V_GOZO ' +
                            'where ( CB_CODIGO = :CB_CODIGO )' );
     PreparaQuery( tqSeis, 'select V.VA_FEC_INI from VACACION V where ' +
                           '( V.CB_CODIGO = :CB_CODIGO ) and ' +
                           '( V.VA_TIPO = 1 ) and ' +
                           '( V.VA_FEC_INI >= ( select C.CB_FEC_ANT from COLABORA C where ( C.CB_CODIGO = V.CB_CODIGO ) ) ) ' +
                           'order by V.VA_FEC_INI' );
     with tqUno do
     begin
          iCtr := 0;
          Active := True;
          while not Eof do
          begin
               iCodigo := FieldByName( 'CB_CODIGO' ).AsInteger;
               EmpezarTransaccion;
               try
                  dPrimero := FieldByName( 'VA_FEC_INI' ).AsDateTime;
	          rSaldoPago := ( FieldByName( 'CB_V_PAGO' ).AsFloat - FieldByName( 'VA_PAGO' ).AsFloat );
                  rSumaGozo := ( FieldByName( 'CB_V_GOZO' ).AsFloat - FieldByName( 'VA_GOZO' ).AsFloat );
	          rDerecho := FieldByName( 'CB_DER_PAG' ).AsFloat;
                  dDerecho := FieldByName( 'CB_DER_FEC' ).AsDateTime;
	          if ( rDerecho > 0 ) then	// Agrega registro de cierre //
	          begin
                       ClearParameters( tqSeis );
                       with tqSeis do
                       begin
                            Active := False;
		            ParamByName( 'CB_CODIGO' ).AsInteger := iCodigo;
                            Active := True;
                            if IsEmpty then
                               dCierre := NullDateTime
                            else
                                dCierre := ( Fields[ 0 ].AsDateTime - 1 );
                            Active := False;
                       end;
                       if ( dCierre > 0 ) then  { Se hacen 2 cierres: uno un dia antes de la primera vacacion }
                       begin                    { con los valores; otro en la fecha deseado pero con valores en 0 };
                            ClearParameters( tqDos );
                            MigrarFechaQuery( tqDos, 'VA_FEC_INI', dCierre - 1 );
                            with tqDos do
                            begin
		                 ParamByName( 'CB_CODIGO' ).AsInteger := iCodigo;
		                 ParamByName( 'VA_TIPO' ).AsInteger := Ord( tvCierre );
		                 ParamByName( 'VA_COMENTA' ).AsString := 'MIGRACION';
		                 ParamByName( 'VA_D_PAGO' ).AsFloat := rDerecho;
		                 ParamByName( 'VA_D_GOZO' ).AsFloat := rDerecho;
                                 ExecSQL;
                            end;
                            if ( dDerecho <> dCierre ) then
                            begin
                                 ClearParameters( tqDos );
		                 MigrarFechaQuery( tqDos, 'VA_FEC_INI', dDerecho );
                                 with tqDos do
                                 begin
		                      ParamByName( 'CB_CODIGO' ).AsInteger := iCodigo;
		                      ParamByName( 'VA_TIPO' ).AsInteger := Ord( tvCierre );
		                      ParamByName( 'VA_COMENTA' ).AsString := 'MIGRACION';
		                      ParamByName( 'VA_D_PAGO' ).AsFloat := 0;
		                      ParamByName( 'VA_D_GOZO' ).AsFloat := 0;
                                      ExecSQL;
                                 end;
                            end;
                       end
                       else
                       begin    { Solo un movimiento de cierre }
                            ClearParameters( tqDos );
		            MigrarFechaQuery( tqDos, 'VA_FEC_INI', dDerecho );
                            with tqDos do
                            begin
		                 ParamByName( 'CB_CODIGO' ).AsInteger := iCodigo;
		                 ParamByName( 'VA_TIPO' ).AsInteger := Ord( tvCierre );
		                 ParamByName( 'VA_COMENTA' ).AsString := 'MIGRACION';
		                 ParamByName( 'VA_D_PAGO' ).AsFloat := rDerecho;
		                 ParamByName( 'VA_D_GOZO' ).AsFloat := rDerecho;
                                 ExecSQL;
                            end;
                       end;
                  end;
                  if ( rSaldoPago > 0.5 ) or ( rSumaGozo > 0.5 ) then
	          begin
                       ClearParameters( tqTres );
		       MigrarFechaQuery( tqTres, 'VA_FEC_INI', dPrimero - 1 );
		       MigrarFechaQuery( tqTres, 'VA_FEC_FIN', dPrimero );
                       with tqTres do
                       begin
		            ParamByName( 'CB_CODIGO' ).AsInteger := iCodigo;
		            ParamByName( 'VA_TIPO' ).AsInteger := Ord( tvVacaciones );
		            ParamByName( 'VA_COMENTA' ).AsString := 'MIGRACION';
		            ParamByName( 'VA_PAGO' ).AsFloat := rMax( rSaldoPago, 0 );
		            ParamByName( 'VA_GOZO' ).AsFloat := rMax( rSumaGozo, 0 );
                            ExecSQL;
                       end;
                  end;
                  if ( rSaldoPago < 0 ) then
                  begin
                       rSaldoPago := FieldByName( 'VA_PAGO' ).AsFloat;
                       with tqCuatro do
                       begin
		            ParamByName( 'CB_CODIGO' ).AsInteger := iCodigo;
		            ParamByName( 'CB_V_PAGO' ).AsFloat := rSaldoPago;
                            ExecSQL;
                       end;
                  end;
                  if ( rSumaGozo < 0 ) then
                  begin
                       rSumaGozo := FieldByName( 'VA_GOZO' ).AsFloat;
                       with tqCinco do
                       begin
		            ParamByName( 'CB_CODIGO' ).AsInteger := iCodigo;
		            ParamByName( 'CB_V_GOZO' ).AsFloat := rSumaGozo;
                            ExecSQL;
                       end;
                  end;
                  TerminarTransaccion( True );
               except
                     on Error: Exception do
                     begin
                          TerminarTransaccion( False );
                          LogError( 'Error al Cerrar Vacaciones', Error );
                     end;
               end;
               Next;
               if ( iCtr > 10 ) then
               begin
                    ShowStatus( 'Cierre # ' + IntToStr( iCtr ) );
                    iCtr := 0;
               end;
               iCtr := iCtr + 1;
               Next;
          end;
     end;
end;

procedure TFixVacacion.ArreglarClick(Sender: TObject);
var
   sPath: String;
   oCursor: TCursor;

procedure CheckFile( const sPath, sFileName: String );
begin
     if not FileExists( sPath + sFileName ) then
        raise Exception.Create( 'Archivo ' + sPath + sFileName + ' No Existe !' );
end;

begin
     sPath := VerificaDir( Path.Text );
     CheckFile( sPath, 'COLABORA.DBF' );
     CheckFile( sPath, 'INC_VACA.DBF' );
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        InitLog;
        if ConectarDOS( sPath ) then
        begin
             if ConectarWindows( Alias.Items.Strings[ Alias.ItemIndex ], Usuario.Text, Password.Text ) then
             begin
                  ShowStatus( 'Arreglando Vacaciones...' );
                  LogEvent( 'Inicializando COLABORA' );
                  ArreglaColabora;
                  LogEvent( 'Borrando VACACION' );
                  BorraVacaciones;
                  LogEvent( 'Migrando Vacaciones' );
                  MigraVacaciones;
                  LogEvent( 'Cerrando Vacaciones' );
                  CierraVacaciones;
                  LogEvent( 'Proceso Terminado' );
                  DesconectarWindows;
                  ShowStatus( 'Las Vacaciones Fueron Arregladas' );
             end;
             DesconectarDOS;
        end;
        CloseLog( 'MIGRACION.LOG' );
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TFixVacacion.CierresClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        InitLog;
        if ConectarWindows( Alias.Items.Strings[ Alias.ItemIndex ], Usuario.Text, Password.Text ) then
        begin
             ShowStatus( 'Cerrando Vacaciones...' );
             LogEvent( 'Cerrando Vacaciones' );
             CierraVacaciones;
             LogEvent( 'Proceso Terminado' );
             DesconectarWindows;
             ShowStatus( 'Los Cierres De Vacaciones Fueron Efectuados' );
        end;
        CloseLog( 'CIERRE.LOG' );
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TFixVacacion.SalirClick(Sender: TObject);
begin
     Close;
end;

end.
