unit DMigrar;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, DB, DBTables, BDE,
     {$ifdef MSSQL}
     ODSI, OCL, OCI,
     {$endif}
     ZetaRegistryServer,
     ZetaAliasManager,
     ZetaMigrar;

type
  TStartCallBack = procedure( Sender: TObject; const sMsg: String ) of object;
  TEndCallBack = procedure( Sender: TObject; var Continue: Boolean ) of object;
  eDriverTypes = ( dtInterbase, dtOracle, dtSQLServer );
  TCompanyData = class( TObject )
  private
    { Private declarations }
    FCodigo: String;
    FNombre: String;
    FAlias: String;
    FDatos: String;
    FUserName: String;
    FPassword: String;
    FEmpate: Integer;
  public
    { Public declarations }
    property Codigo: String read FCodigo write FCodigo;
    property Nombre: String read FNombre write FNombre;
    property Alias: String read FAlias write FAlias;
    property Datos: String read FDatos write FDatos;
    property UserName: String read FUserName write FUserName;
    property Password: String read FPassword write FPassword;
    property Empate: Integer read FEmpate write FEmpate;
  end;
  TCompanyList = class( TObject )
  private
    { Private declarations }
    FLista: TList;
    function GetEmpresa( Index: Integer ): TCompanyData;
  public
    { Public declarations }
    property Empresa[ Index: Integer ]: TCompanyData read GetEmpresa;
    constructor Create;
    destructor Destroy; override;
    function AgregaEmpresa: TCompanyData;
    function Count: Integer;
    procedure Clear;
    procedure LlenaLista( Lista: TStrings );
  end;
  TCompanyInfo = class( TObject )
  private
    { Private declarations }
    FName: String;
    FPath: String;
    FRaiz: Boolean;
  public
    { Public declarations }
    property Name: String read FName;
    property Path: String read FPath;
    property Raiz: Boolean read FRaiz;
    constructor Create( const sName, sPath: String; lRaiz: Boolean );
  end;
  TdmMigrar = class( TDataModule )
    dbSourceDatos: TDatabase;
    dbTarget: TDatabase;
    dbSourceShared: TDatabase;
    tqSource: TQuery;
    tqTarget: TQuery;
    tqDelete: TQuery;
    ttSource: TTable;
    ttTarget: TTable;
    BatchMove: TBatchMove;
    procedure dmMigrarCreate(Sender: TObject);
    procedure dmMigrarDestroy(Sender: TObject);
    procedure DoCallBack( Sender: TObject; var lOk: Boolean );
  private
    { Private declarations }
    FEsVer260: Boolean;
    FRegistry: TZetaRegistryServer;
    FAliasManager: TZetaAliasManager;
    FSourceDatosPath: String;
    FSourceSharedPath: String;
    FTargetAlias: String;
    FTargetUserName: String;
    FTargetPassword: String;
    FBitacora: TZetaMigrarLog;
    FSource: TQuery;
    FTargetQuery: TQuery;
    FTablaMigrada: TZetaMigrar;
    FStartCallBack: TStartCallBack;
    FCallBack: TEndCallBack;
    FEndCallBack: TEndCallBack;
    FErrorFound: Boolean;
    FErrorCount: Integer;
    FDriverType: eDriverTypes;
    FItemName: String;
    function AlteraIndices( const lActive: Boolean ): Boolean;
    function Pertenece( Component: TComponent ): Boolean;
    function PuedePoblar( Component: TComponent ): Boolean;
    function PuedeMigrar( Component: TComponent ): Boolean;
    procedure OpenDBase( Database: TDatabase; const sDirectory: String );
  protected
    { Protected declarations }
    FLista: TStrings;
    function DoEndCallBack: Boolean;
    function GetTimeStamp: String;
    procedure DoStartCallBack( const sMsg: String );
    procedure ActivaSesion;
    procedure CloseDatabase( Database: TDatabase );
    procedure ConnectQuery( oDatabase: TDatabase; Query: TQuery; const Script: String );
    procedure OpenAlias( Database: TDatabase; const sAlias, sUserName, sPassword: String );
    procedure OpenDBSourceDatos;
    procedure OpenDBSourceDatosAlternate( const sPath: String );
    procedure OpenDBSourceShared;
    procedure OpenDBTarget;
    procedure OpenAllDatabases;
    procedure AttachTargetQuery( Query: TQuery; const Script: String );
    procedure PrepareTargetQuery( const Script: String );
    procedure CloseDBTarget;
    procedure CloseDBSourceDatos;
    procedure CloseDBSourceShared;
    procedure CloseAllDatabases;
    procedure ClearErrorFound;
    procedure SetErrorFound( const lValue: Boolean );
    procedure SetTablaMigrada( Tabla: TZetaMigrar );
    procedure EmpezarTransaccion;
    procedure MigrarAdicionales; virtual;
    procedure TerminarTransaccion( const lCommit: Boolean );
    procedure SetEsVer260( const lValue: Boolean ); virtual;
  public
    { Public declarations }
    property Bitacora: TZetaMigrarLog read FBitacora write FBitacora;
    property CallBack: TEndCallBack read FCallBack write FCallBack;
    property DriverType: eDriverTypes read FDriverType;
    property EndCallBack: TEndCallBack read FEndCallBack write FEndCallBack;
    property ErrorFound: Boolean read FErrorFound;
    property ErrorCount: Integer read FErrorCount;
    property EsVer260: Boolean read FEsVer260 write SetEsVer260;
    property ItemName: String read FItemName write FItemName;
    property Registry: TZetaRegistryServer read FRegistry;
    property Source: TQuery read FSource;
    property SourceDatosPath: String read FSourceDatosPath write FSourceDatosPath;
    property SourceSharedPath: String read FSourceSharedPath write FSourceSharedPath;
    property StartCallBack: TStartCallBack read FStartCallBack write FStartCallBack;
    property TablaMigrada: TZetaMigrar read FTablaMigrada;
    property TargetAlias: String read FTargetAlias write FTargetAlias;
    property TargetPassword: String read FTargetPassword write FTargetPassword;
    property TargetQuery: TQuery read FTargetQuery;
    property TargetUserName: String read FTargetUserName write FTargetUserName;
    function DbaseLanguageDriverNameOK( const sDriver: String ): Boolean;
    function DriveOK( var sValor: String ): Boolean;
    function Pasos: Integer;
    function PasosDefault: Integer;
    function Steps: Integer;
    procedure GetPasosList( Lista: TStrings );
    procedure GetStepList( Lista: TStrings );
    procedure GetAliasNames( Lista: TStrings );
    procedure ClearBitacora;
    procedure ClearBitacoraErrores;
    procedure CierraBitacora;
    procedure InitBitacora( Errores: TAsciiLog; Resumen: TStrings );
    procedure LlenaListaEmpresas( Lista: TStrings; const sDirectory: String );
    procedure Poblar;
    procedure PoblarDefault;
    procedure Migrar( Mensajes: TStrings );
    procedure ResetAllDatabases;
    {$ifdef MSSQL}
    procedure HdbcBeforeConnect(Sender: TObject);
    {$endif}
    procedure OpenDatabase( Database: TDatabase; const sDatabase, sUserName, sPassword: String );
    procedure ActualizaDerechosAdicionales(const dbEmpresa:TDataBase;const Actualiza:Boolean;const CodigoNuevo,CodigoAnterior:string);
  end;

var
  dmMigrar: TdmMigrar;

function PK_Violation( Error: Exception ): Boolean;

implementation

uses DDBConfig,
     ZetaCommonTools,
     ZetaTressCFGTools;

{$R *.DFM}

function PK_Violation( Error: Exception ): Boolean;
begin
    Result := ( Pos( 'VIOLATION OF PRIMARY', UpperCase( Error.Message ) ) > 0 );
end;

{ ********* TCompanyList ********** }

constructor TCompanyList.Create;
begin
     FLista := TList.Create;
end;

destructor TCompanyList.Destroy;
begin
     Clear;
     FLista.Free;
     inherited Destroy;
end;

procedure TCompanyList.Clear;
var
   i: Integer;
begin
     with FLista do
     begin
          for i := ( Count - 1 ) downto 0 do
          begin
               Empresa[ i ].Free;
          end;
          Clear;
     end;
end;

function TCompanyList.Count: Integer;
begin
     Result := FLista.Count;
end;

function TCompanyList.GetEmpresa( Index: Integer ): TCompanyData;
begin
     with FLista do
     begin
          if ( Index >= 0 ) and ( Index < Count ) then
             Result := TCompanyData( Items[ Index ] )
          else
              Result := nil;
     end;
end;

function TCompanyList.AgregaEmpresa: TCompanyData;
var
   oEmpresa: TCompanyData;
begin
     oEmpresa := TCompanyData.Create;
     FLista.Add( oEmpresa );
     Result := oEmpresa;
end;

procedure TCompanyList.LlenaLista( Lista: TStrings );
var
   i: Integer;
begin
     try
        with Lista do
        begin
             BeginUpdate;
             Clear;
        end;
        for i := 0 to ( FLista.Count - 1 ) do
        begin
             with Empresa[ i ] do
             begin
                  Lista.Add( Nombre );
             end;
        end;
     finally
            Lista.EndUpdate;
     end;
end;

{ ************* TCompanyInfo *********** }

constructor TCompanyInfo.Create( const sName, sPath: String; lRaiz: Boolean );
begin
     FName := sName;
     FPath := sPath;
     FRaiz := lRaiz;
end;

{ ************* TdmMigrar *************** }

procedure TdmMigrar.dmMigrarCreate(Sender: TObject);
begin
     FEsVer260 := False;
     FAliasManager := TZetaAliasManager.Create;
     FRegistry := TZetaRegistryServer.Create;
     FBitacora := TZetaMigrarLog.Create;
     FLista := TStringList.Create;
     ResetAllDatabases;
end;

procedure TdmMigrar.dmMigrarDestroy(Sender: TObject);
begin
     FLista.Free;
     FreeAndNil( FBitacora );
     FRegistry.Free;
     FAliasManager.Free;
end;

procedure TdmMigrar.InitBitacora( Errores: TAsciiLog; Resumen: TStrings );
begin
     with FBitacora do
     begin
          SetErrores( Errores );
          SetResumen( Resumen );
     end;
end;

procedure TdmMigrar.CierraBitacora;
begin
     FBitacora.Close;
end;

procedure TdmMigrar.ClearBitacora;
begin
     with FBitacora do
     begin
          SetErrores( nil );
          SetResumen( nil );
     end;
end;

procedure TdmMigrar.ClearBitacoraErrores;
begin
     with FBitacora do
     begin
          SetErrores( nil );
     end;
end;


{$ifdef MSSQL}
procedure TdmMigrar.HdbcBeforeConnect(Sender: TObject);
const
     SQL_PRESERVE_CURSORS = 1204;
     SQL_PC_ON = 1;
begin
     SQLSetConnectAttr(THdbc( Sender ).Handle, SQL_PRESERVE_CURSORS, Pointer(SQL_PC_ON), 0);
end;
{$endif}

procedure TdmMigrar.SetEsVer260( const lValue: Boolean );
begin
     FEsVer260 := lValue;
end;

function TdmMigrar.DriveOK( var sValor: String ): Boolean;
var
   sDrive: String;
   sDrivePath: Array[ 0..3 ] of Char;     // holds the root directory to query //
begin
     sDrive := ExtractFileDrive( sValor );
     if ( Length( sDrive ) <> 2 ) then
        sValor := 'No Se Aceptan Directorios de Red'
     else
     begin
          StrPCopy( sDrivePath, sDrive + '\' );
          case GetDriveType( sDrivePath ) of
               DRIVE_UNKNOWN: sValor := 'Disco Desconocido';
               DRIVE_NO_ROOT_DIR: sValor := 'Directorio Ra�z No Existe';
               DRIVE_REMOVABLE: sValor := '';
               DRIVE_FIXED: sValor := '';
               DRIVE_REMOTE: sValor := 'No Se Aceptan Directorios de Red';
               DRIVE_CDROM: sValor := '';
               DRIVE_RAMDISK: sValor := 'No Se Aceptan Directorios en Discos RAM';
          else
              sValor := 'Disco Indeterminado';
          end;
     end;
     Result := not StrLleno( sValor );
end;

procedure TdmMigrar.ActivaSesion;
begin
     with Session do
     begin
          if not Active then
             Active := True;
          NetFileDir := SetFileNameDefaultPath( '' );
     end;
end;

function TdmMigrar.DbaseLanguageDriverNameOK( const sDriver: String ): Boolean;
var
   pValue: {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif};
begin
     ActivaSesion;
     pValue := StrAlloc( dbiMaxNameLen + 1 );
     try
        Check( BDE.DbiGetLDName( szDBASE, nil, pValue ) );
        Result := ( StrPas( pValue ) = sDriver );
     finally
            StrDispose( pValue );
     end;
end;

procedure TdmMigrar.EmpezarTransaccion;
begin
     with dbTarget do
     begin
          if InTransaction then
             Commit;
          if not InTransaction then
             StartTransaction;
     end;
end;

procedure TdmMigrar.TerminarTransaccion( const lCommit: Boolean );
begin
     with dbTarget do
     begin
          if InTransaction then
          begin
               if lCommit then
                  Commit
               else
                   RollBack;
          end;
     end;
end;

procedure TdmMigrar.ClearErrorFound;
begin
     FErrorFound := False;
     FErrorCount := 0;
end;

procedure TdmMigrar.SetErrorFound( const lValue: Boolean );
begin
     if lValue then
     begin
          FErrorFound := True;
          FErrorCount := FErrorCount + 1;
     end;
end;

procedure TdmMigrar.ResetAllDatabases;
begin
     SourceDatosPath := '';
     SourceSharedPath := '';
     TargetAlias := '';
     TargetUserName := '';
     TargetPassword := '';
end;

procedure TdmMigrar.OpenAllDatabases;
begin
     OpenDBSourceShared;
     OpenDBSourceDatos;
     OpenDBTarget;
end;

procedure TdmMigrar.OpenAlias( Database: TDatabase; const sAlias, sUserName, sPassword: String );
begin
     if ZetaCommonTools.StrLleno( sAlias ) then
     begin
          with Database do
          begin
               Connected := False;
               AliasName := sAlias;
               {
               AliasName := '';
               DriverName := K_INTERBASE;
               }
               with Params do
               begin
                    {
                    Values[ 'SERVER NAME' ] := sDatabase;
                    }
                    Values[ 'USER NAME' ] := sUserName;
                    Values[ 'PASSWORD' ] := sPassword;
               end;
               Connected := True;
          end;
     end;
end;

procedure TdmMigrar.OpenDatabase( Database: TDatabase; const sDatabase, sUserName, sPassword: String );
begin
     if ZetaCommonTools.StrLleno( sDatabase ) then
     begin
          with Database do
          begin
               Connected := False;
               AliasName := '';
               DriverName := K_INTERBASE;
               with Params do
               begin
                    Values[ 'SERVER NAME' ] := sDatabase;
                    Values[ 'USER NAME' ] := sUserName;
                    Values[ 'PASSWORD' ] := sPassword;
               end;
               Connected := True;
          end;
     end;
end;

procedure TdmMigrar.OpenDBase( Database: TDatabase; const sDirectory: String );
begin
     if StrLleno( sDirectory ) then
     begin
          with Database do
          begin
               Connected := False;
               with Params do
               begin
                    Values[ 'PATH' ] := sDirectory;
               end;
               Connected := True;
          end;
     end;
end;

procedure TdmMigrar.OpenDBTarget;
begin
     OpenAlias( dbTarget, FTargetAlias, FTargetUserName, FTargetPassword );
end;

procedure TdmMigrar.OpenDBSourceShared;
begin
     OpenDBase( dbSourceShared, FSourceSharedPath );
end;

procedure TdmMigrar.OpenDBSourceDatos;
begin
     OpenDBase( dbSourceDatos, FSourceDatosPath );
end;

procedure TdmMigrar.OpenDBSourceDatosAlternate( const sPath: String );
begin
     OpenDBase( dbSourceDatos, sPath );
end;

procedure TdmMigrar.CloseDatabase( Database: TDatabase );
begin
     Database.Connected := False;
end;

procedure TdmMigrar.CloseDBTarget;
begin
     CloseDatabase( dbTarget );
end;

procedure TdmMigrar.CloseDBSourceShared;
begin
     CloseDatabase( dbSourceShared );
end;

procedure TdmMigrar.CloseDBSourceDatos;
begin
     CloseDatabase( dbSourceDatos );
end;

procedure TdmMigrar.CloseAllDatabases;
begin
     CloseDBTarget;
     CloseDBSourceDatos;
     CloseDBSourceShared;
end;

procedure TdmMigrar.GetAliasNames( Lista: TStrings );
begin
     FAliasManager.GetAliasList( Lista );
end;

procedure TdmMigrar.SetTablaMigrada( Tabla: TZetaMigrar );
begin
     FTablaMigrada := Tabla;
     with TablaMigrada do
     begin
          FSource := SourceQuery;
          FTargetQuery := TargetQuery;
     end;
end;

procedure TdmMigrar.ConnectQuery( oDatabase: TDatabase; Query: TQuery; const Script: String );
begin
     with Query do
     begin
          Active := False;
          DatabaseName := oDatabase.DatabaseName;
          with SQL do
          begin
               Clear;
               if StrLleno( Script ) then
               begin
                    Add( Script );
                    Prepare;
               end;
          end;
     end;
end;

procedure TdmMigrar.AttachTargetQuery( Query: TQuery; const Script: String );
begin
     ConnectQuery( dbTarget, Query, Script );
end;

procedure TdmMigrar.PrepareTargetQuery( const Script: String );
begin
     AttachTargetQuery( tqTarget, Script );
end;

procedure TdmMigrar.LlenaListaEmpresas( Lista: TStrings; const sDirectory: String );
var
   sName, sPath: String;
begin
     SourceSharedPath := sDirectory;
     OpenDBSourceShared;
     with tqSource do
     begin
          DatabaseName := dbSourceShared.DatabaseName;
          Active := False;
          with SQL do
          begin
               Clear;
               Add( 'select CM_NOMBRE, CM_DRIVE, CM_PATH from COMPANYS.DBF' );
          end;
     end;
     with Lista do
     begin
          try
             BeginUpdate;
             Clear;
             with tqSource do
             begin
                  Active := True;
                  First;
                  while not Eof do
                  begin
                       sName := FieldByName( 'CM_NOMBRE' ).AsString;
                       sPath := FieldByName( 'CM_DRIVE' ).AsString + ':' + FieldByName( 'CM_PATH' ).AsString;
                       AddObject( sName, TCompanyInfo.Create( sName, sPath, False ) );
                       Next;
                  end;
                  Active := False;
             end;
          finally
                 CloseDBSourceShared;
                 EndUpdate;
          end;
     end;
end;

function TdmMigrar.Pertenece( Component: TComponent ): Boolean;
begin
     Result := ( Component is TZetaMigrar );
end;

function TdmMigrar.PuedePoblar( Component: TComponent ): Boolean;
begin
     Result := Pertenece( Component ) and TZetaMigrar( Component ).PuedePoblar( SourceDatosPath );
end;

function TdmMigrar.PuedeMigrar( Component: TComponent ): Boolean;
begin
     Result := Pertenece( Component ) and TZetaMigrar( Component ).Enabled;
end;

function TdmMigrar.Pasos: Integer;
begin
     Result := 2 * PasosDefault;
end;

function TdmMigrar.PasosDefault: Integer;
var
   i: Integer;
begin
     Result := 0;
     for i := 0 to ( ComponentCount - 1 ) do
     begin
          if PuedePoblar( Components[ i ] ) then
             Result := Result + 1;
     end;
end;

function TdmMigrar.Steps: Integer;
var
   i: Integer;
begin
     Result := 0;
     for i := 0 to ( ComponentCount - 1 ) do
         if PuedeMigrar( Components[ i ] ) then
            Result := Result + 1;
     Result := 2 * Result;  // Para contar los borrados de tablas //
end;

procedure TdmMigrar.GetPasosList( Lista: TStrings );
var
   i: Integer;
   ZetaMigrar: TZetaMigrar;
begin
     with Lista do
     begin
          try
             BeginUpdate;
             Clear;
             for i := 0 to ( Self.ComponentCount - 1 ) do
             begin
                  if PuedePoblar( Self.Components[ i ] ) then
                  begin
                       ZetaMigrar := TZetaMigrar( Self.Components[ i ] );
                       AddObject( ZetaMigrar.StartMessage + ' ( ' + ZetaMigrar.TargetTableName + ' )', ZetaMigrar );
                  end;
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TdmMigrar.GetStepList( Lista: TStrings );
var
   i: Integer;
begin
     with Lista do
     begin
          try
             BeginUpdate;
             Clear;
             for i := 0 to ( Self.ComponentCount - 1 ) do
                 if PuedeMigrar( Self.Components[ i ] ) then
                    AddObject( TZetaMigrar( Self.Components[ i ] ).StartMessage + ' ( ' + TZetaMigrar( Self.Components[ i ] ).TargetTableName + ' )', TZetaMigrar( Self.Components[ i ] ) );
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TdmMigrar.Poblar;
var
   i: Integer;
begin
     if ( Pasos > 0 ) then
     begin
          ClearErrorFound;
          with FBitacora do
          begin
               AgregaResumen( StringOfChar( '=', 60 ) );
               AgregaResumen( 'Poblaci�n De Base De Datos' );
               AgregaResumen( 'Base de Datos A Poblar: ' + TargetAlias );
               AgregaResumen( 'Directorio De Tablas Fuente PARADOX: ' + SourceDatosPath );
               AgregaResumen( StringOfChar( '=', 60 ) );
               AgregaResumen( GetTimeStamp + ': Iniciando Poblaci�n De Base De Datos' );
          end;
          ActivaSesion;
          OpenDBTarget;
          OpenDBSourceDatos;
          for i := ( ComponentCount - 1 ) downto 0 do
          begin
               if PuedePoblar( Components[ i ] ) then
               begin
                    with TZetaMigrar( Components[ i ] ) do
                    begin
                         DoStartCallBack( 'Borrando ' + TargetTableName );
                         BorraTabla( FBitacora );
                         SetErrorFound( HayError );
                         if not DoEndCallBack then
                            Break;
                    end;
               end;
          end;
          for i := 0 to ( ComponentCount - 1 ) do
          begin
               if PuedePoblar( Components[ i ] ) then
               begin
                    with TZetaMigrar( Components[ i ] ) do
                    begin
                         DoStartCallBack( 'Poblando ' + TargetTableName );
                         PoblarTabla( FBitacora );
                         SetErrorFound( HayError );
                         if not DoEndCallBack then
                            Break;
                    end;
               end;
          end;
          CloseDBSourceDatos;
          CloseDBTarget;
          FBitacora.AgregaResumen( GetTimeStamp + ': Poblaci�n De Base De Datos Terminada' );
     end;
end;

procedure TdmMigrar.PoblarDefault;
var
   i: Integer;
begin
     if ( Pasos > 0 ) then
     begin
          ClearErrorFound;
          with FBitacora do
          begin
               AgregaResumen( StringOfChar( '=', 60 ) );
               AgregaResumen( 'Creaci�n de Tablas Default' );
               AgregaResumen( 'Base de Datos Modelo: ' + TargetAlias );
               AgregaResumen( 'Directorio De Tablas Fuente PARADOX: ' + SourceDatosPath );
               AgregaResumen( StringOfChar( '=', 60 ) );
               AgregaResumen( GetTimeStamp + ': Iniciando Creaci�n de Tablas Default' );
          end;
          ActivaSesion;
          OpenDBTarget;
          OpenDBSourceDatos;
          for i := 0 to ( ComponentCount - 1 ) do
          begin
               if PuedePoblar( Components[ i ] ) then
               begin
                    with TZetaMigrar( Components[ i ] ) do
                    begin
                         DoStartCallBack( 'Creando ' + TargetTableName + '.DB' );
                         PoblarTablaDefault( FBitacora );
                         SetErrorFound( HayError );
                         if not DoEndCallBack then
                            Break;
                    end;
               end;
          end;
          CloseDBSourceDatos;
          CloseDBTarget;
          FBitacora.AgregaResumen( GetTimeStamp + ': Creaci�n de Tablas Default Terminada' );
     end;
end;

function TdmMigrar.AlteraIndices( const lActive: Boolean ): Boolean;
const
     aBoolToInt: array[ False..True ] of Byte = ( 0, 1 );
     aPrefijo: array[ False..True ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'in', '' );
     aAccion: array[ False..True ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'Desactivado', 'Activado' );
     K_LIST_ACTIVES = 'select RDB$INDEX_NAME INDICE from RDB$INDICES where '+
                      '( ( RDB$INDEX_INACTIVE is NULL ) or ( RDB$INDEX_INACTIVE = 0 ) ) and '+
                      '( ( RDB$SYSTEM_FLAG is NULL ) or ( RDB$SYSTEM_FLAG <> 1 ) ) and '+
                      '( RDB$INDEX_NAME not like "RDB$PRIMARY%" ) and '+
                      '( RDB$INDEX_NAME not like "RDB$FOREIGN%" ) '+
                      'order by RDB$RELATION_NAME, RDB$INDEX_ID';
     K_LIST_INACTIVES = 'select RDB$INDEX_NAME INDICE from RDB$INDICES where '+
                        '( ( RDB$INDEX_INACTIVE is not NULL ) and ( RDB$INDEX_INACTIVE = 1 ) ) and '+
                        '( ( RDB$SYSTEM_FLAG is NULL ) or ( RDB$SYSTEM_FLAG <> 1 ) ) and '+
                        '( RDB$INDEX_NAME not like "RDB$PRIMARY%" ) and '+
                        '( RDB$INDEX_NAME not like "RDB$FOREIGN%" ) '+
                        'order by RDB$RELATION_NAME, RDB$INDEX_ID';
{
select RDB$INDEX_NAME INDICE from RDB$INDICES where
( ( RDB$INDEX_INACTIVE is NULL ) or ( RDB$INDEX_INACTIVE = 0 ) )and
( ( RDB$SYSTEM_FLAG is NULL ) or ( RDB$SYSTEM_FLAG <> 1 ) ) and
( RDB$INDEX_NAME not like "RDB$PRIMARY%" ) and
( RDB$INDEX_NAME not like "RDB$FOREIGN%" )
order by RDB$RELATION_NAME, RDB$INDEX_ID
}
     K_ALTER_INDEX = 'alter index %s %sactive';
var
   FIndices: TStrings;
   i: Integer;
   sIndex: String;
begin
     Result := False;
     FIndices := TStringList.Create;
     try
        try
           if lActive then
              PrepareTargetQuery( K_LIST_INACTIVES )
           else
               PrepareTargetQuery( K_LIST_ACTIVES );
           with tqTarget do
           begin
                Active := True;
                while not Eof do
                begin
                     FIndices.Add( FieldByName( 'INDICE' ).AsString );
                     Next;
                end;
                Active := False;
           end;
           with FIndices do
           begin
                for i := 0 to ( Count - 1 ) do
                begin
                     sIndex := Strings[ i ];
                     PrepareTargetQuery( Format( K_ALTER_INDEX, [ sIndex, aPrefijo[ lActive ] ] ) );
                     with dbTarget do
                     begin
                          StartTransaction;
                          try
                             tqTarget.ExecSQL;
                             Commit;
                          except
                                on Error: Exception do
                                begin
                                     RollBack;
                                     FBitacora.HandleException( Format( 'Indice %s No Pudo Ser %s', [ sIndex, aAccion[ lActive ] ] ), Error );
                                end;
                          end;
                     end;
                end;
           end;
           Result := True;
        except
              on Error: Exception do
              begin
                   FBitacora.HandleException( 'Error Al Alterar Indices', Error );
              end;
        end;
     finally
            FIndices.Free;
     end;
     with FBitacora do
     begin
          if Result then
             AgregaMensaje( Format( 'Indices Fueron %ss', [ aAccion[ lActive ] ] ) )
          else
              AgregaResumen( Format( 'Indices No Pudieron Ser %ss', [ aAccion[ lActive ] ] ) );
     end;
end;

procedure TdmMigrar.Migrar( Mensajes: TStrings );
var
   i: Integer;
   sMensaje: String;
begin
     if ( Steps > 0 ) then
     begin
          ClearErrorFound;
          with FBitacora do
          begin
               AgregaResumen( StringOfChar( '=', 60 ) );
               AgregaResumen( 'Migraci�n De Tress DOS a Tress Windows' );
               AgregaResumen( 'Base de Datos Tress Windows: ' + TargetAlias );
               AgregaResumen( 'Directorio De Tablas Tress DOS: ' + SourceDatosPath );
               with Mensajes do
               begin
                    for i := 0 to ( Count - 1 ) do
                        AgregaResumen( Strings[ i ] );
               end;
               AgregaResumen( StringOfChar( '=', 60 ) );
               AgregaResumen( GetTimeStamp + ': Empezando Migraci�n De Tress DOS a Tress Windows' );
          end;
          ActivaSesion;
          OpenAllDatabases;
          AlteraIndices( False ); { Desactiva Indices No Primarios Ni For�neos }
          for i := ( ComponentCount - 1 ) downto 0 do
          begin
               if PuedeMigrar( Components[ i ] ) then
               begin
                    with TZetaMigrar( Components[ i ] ) do
                    begin
                         DoStartCallBack( 'Borrando ' + TargetTableName );
                         BorraTabla( FBitacora );
                         SetErrorFound( HayError );
                         if not DoEndCallBack then
                            Break;
                    end;
               end;
          end;
          for i := 0 to ( ComponentCount - 1 ) do
          begin
               if PuedeMigrar( Components[ i ] ) then
               begin
                    SetTablaMigrada( TZetaMigrar( Components[ i ] ) );
                    with TablaMigrada do
                    begin
                         DoStartCallBack( 'Migrando ' + StartMessage + '( ' + TargetTableName + ' )' );
                         Migrar( Bitacora );
                         SetErrorFound( HayError );
                         if not DoEndCallBack then
                            Break;
                    end;
               end;
          end;
          MigrarAdicionales;
          AlteraIndices( True ); { Activa Indices No Primarios Ni For�neos }
          CloseAllDatabases;
          with FBitacora do
          begin
               sMensaje := GetTimeStamp + ': Migraci�n De Tress DOS a Tress Windows Terminada';
               AgregaResumenOnly( '' );
               AgregaResumenOnly( sMensaje );
               AgregaMensaje( sMensaje );
          end;
     end;
end;

procedure TdmMigrar.MigrarAdicionales;
begin
end;

function TdmMigrar.GetTimeStamp: String;
begin
     Result := FormatDateTime( 'hh:nn dd/mmm/yy', Now );
end;

procedure TdmMigrar.DoStartCallBack( const sMsg: String );
begin
     if Assigned( FStartCallBack ) then
        FStartCallBack( Self, sMsg );
end;

procedure TdmMigrar.DoCallBack( Sender: TObject; var lOk: Boolean );
begin
     if Assigned( FCallBack ) then
        FCallBack( Sender, lOk );
     if not lOk then
        FBitacora.AgregaResumen( '*** Interrupci�n del Usuario ***' );
end;

function TdmMigrar.DoEndCallBack: Boolean;
var
   Continue: Boolean;
begin
     Continue := True;
     if Assigned( FEndCallBack ) then
        FEndCallBack( Self, Continue );
     Result := Continue;
     if not Result then
        FBitacora.AgregaResumen( '*** Interrupci�n del Usuario ***' );
end;

procedure TdmMigrar.ActualizaDerechosAdicionales(const dbEmpresa:TDataBase;const Actualiza:Boolean;const CodigoNuevo,CodigoAnterior:string);
var
   tqQuery :TQuery;
const
     K_UPD_GR = 'update GR_AD_ACC set CM_CODIGO = ''%s'' where CM_CODIGO = ''%s''  ';
     K_DEL_GR = 'delete from GR_AD_ACC where CM_CODIGO = ''%s'' ';

begin
     tqQuery := TQuery.Create(Self);
     tqQuery.UniDirectional := True;
     try
         try
         		if Actualiza then
             	  ConnectQuery( dbEmpresa,tqQuery,Format( K_UPD_GR,[CodigoNuevo,CodigoAnterior] ))
         		else
                ConnectQuery( dbEmpresa,tqQuery,Format( K_DEL_GR,[CodigoNuevo,CodigoAnterior] ));
            with dbEmpresa do
            begin
              	 StartTransaction;
                 tqQuery.ExecSQL;
                 Commit;
         		end;
         except
              on Error: Exception do
              begin
         			//dbEmpresa.RollBack;
              //FBitacora.HandleException( Format( 'No se pudo Modificar los Derechos en Campos Adicionales en %s',[CodigoNuevo] ), Error );
              end;
         end;
     finally
            FreeAndNil(tqQuery);
     end;
end;

end.
