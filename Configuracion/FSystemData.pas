unit FSystemData;

{$ifdef MSSQL}
{$define DEBUGSENTINEL}
{$define SENTINELVIRTUAL}
{$endif}

{.$define DEBUG_XML_3DT}

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls,
     FAutoClasses,
     FAutoServer,
     FAutoServerLoader;

type
  TSystemData = class(TForm)
    Panelnferior: TPanel;
    Salir: TBitBtn;
    PanelSuperior: TPanel;
    ServerInfo: TMemo;
    Exportar: TBitBtn;
    SaveDialog: TSaveDialog;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ExportarClick(Sender: TObject);
  private
    { Private declarations }
    FAutoServer: TAutoServer;
    FDatos: TStrings;
    function BoolToAttBoolean( lValue : boolean ) : String;
    function ExportData( const sFileName: String ): Boolean;
    function ModuloOK(const eModulo: TModulos): String;
    function PrestamoOK(const eModulo: TModulos): String;
    function ReportaFecha( const dValue: TDate ): String;

    function LoadCompanyData : string;
    procedure AddText( const sText : string ) ;
    procedure LoadCompanyDataCallBack(const sMsg: String);
    procedure LoadCompanyDataCallBackError(const sMsg: String; Error: Exception);
    procedure LoadSentinelData;
    procedure LoadUserData;
  public
    { Public declarations }
    property AutoServer: TAutoServer read FAutoServer write FAutoServer;
  end;

var
  SystemData: TSystemData;

implementation

uses DZetaServerProvider,
     DDBConfig,
     ZetaServerTools,
     ZetaDialogo,
     ZetaLicenseMgr,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaLicenseClasses,
{$ifdef SENTINELVIRTUAL}
     FSentinelRegistry,
{$endif}
     ZetaWinAPITools;

{$R *.DFM}

{ TSystemData }

const
     K_FECHA = 'yyyy-mm-dd';

procedure TSystemData.FormCreate(Sender: TObject);
begin
     HelpContext := H00015_Datos_del_sistema;
end;

procedure TSystemData.FormShow(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        ZetaWinAPITools.WriteSystemInfo( ServerInfo.Lines, False);
     finally
            Screen.Cursor := oCursor;
     end;
end;


function TSystemData.ReportaFecha( const dValue: TDate ): String;
begin
     if ( dValue > NullDateTime ) then
        Result := FormatDateTime( K_FECHA, dValue )
     else
         Result := '1899-12-30';
end;



function TSystemData.ModuloOK( const eModulo: TModulos ): String;
begin
     Result := BoolToAttBoolean( FAutoClasses.BitSet( FAutoServer.Modulos, Ord( eModulo ) ) );
end;

function TSystemData.PrestamoOK( const eModulo: TModulos ): String;
begin
     Result := BoolToAttBoolean( FAutoClasses.BitSet( FAutoServer.Prestamos, Ord( eModulo ) ) );
end;


procedure TSystemData.LoadSentinelData;
const
     K_MODULO = '%25.25s         %s       %s';
     K_MODULO_XML = '<Modulo Numero="%d" Nombre="%s" Autorizado="%s" Prestado="%s"/>';
var
   eModulo: TModulos;
begin
     with FDatos do
     begin
          {$ifdef SENTINELVIRTUAL}
          FSentinelRegistry.FDesactivarConteoVirtual := TRUE;
          {$endif}
          TAutoServerLoader.LoadSentinelInfo( FAutoServer );

          {$ifdef ANTES}
          with FAutoServer do
          begin
               Add( '' );
               Add( 'Fecha:               ' + FormatDateTime( K_FECHA, Now ) );
               Add( '-------- Datos Del Sentinel -----------------------' );
               Add( '' );
               Add( 'Empresa #:           ' + IntToStr( Empresa ) );
               Add( 'Empleados:           ' + IntToStr( Empleados ) );
               Add( 'Licencias:           ' + IntToStr( Usuarios ) );
               Add( '# de Sentinel        ' + IntToStr( NumeroSerie ) );
               Add( 'Version:             ' + Version );
               Add( 'Kit Distribuidor:    ' + ZetaCommonTools.BoolToSiNo( EsKit ) );
               Add( 'Plataforma:          ' + PlataformaAutoStr );
               Add( 'Base De Datos:       ' + GetSQLEngineStr );
               Add( 'Vencimiento:         ' + GetVencimientoStr );
               Add( 'Caducidad Pr�stamos: ' + GetCaducidadStr );
               Add( '' );
               Add( 'M�dulo                    Autorizado Prestado' );
               Add( '------------------------- ---------- --------' );
               Add( '' );
               for eModulo := Low( TModulos ) to High( TModulos ) do
               begin
                    Add( Format( K_MODULO, [ FAutoServer.GetModuloStr( eModulo ), ModuloOk( eModulo ), PrestamoOk( eModulo ) ] ) );
               end;
          end;
          {$else}
          with FAutoServer do
          begin
               Add( '<Sentinel ' );
               Add( 'Empresa="' + IntToStr( Empresa ) +'" ');
               Add( 'Empleados="' + IntToStr( Empleados ) +'" ');
               Add( 'Licencias="' + IntToStr( Usuarios ) +'" ');
               Add( 'Numero="' + IntToStr( NumeroSerie ) +'" ');
               Add( 'LimiteConfigBD="' + IntToStr( ZetaLicenseClasses.GetIntervaloLimiteBDConfig(LimiteConfigBD) ) +'" ');
               Add( 'IniciaValidacion="' + IntToStr( IniciaValidacion ) +'" ');
               Add( 'Version="' + Version  +'" ' );
               Add( 'EsKitDistribuidor="' + BoolToAttBoolean( EsKit )+'" ' );
               Add( 'Plataforma="' + PlataformaAutoStr +'" ');
               Add( 'BaseDeDatos="' + GetSQLEngineStr +'" ');
               Add( 'Vencimiento="' + ReportaFecha( FAutoServer.Vencimiento ) +'" ');
               Add( 'CaducidadPrestamos="' + ReportaFecha( FAutoServer.Caducidad )  +'" ');
               Add( '>' );
               for eModulo := Low( TModulos ) to High( TModulos ) do
               begin
                    Add( Format( K_MODULO_XML, [ Ord( eModulo ),  FAutoServer.GetModuloStr( eModulo ), ModuloOk( eModulo ), PrestamoOk( eModulo ) ] ) );
               end;
               Add( '</Sentinel>');
          end;
         {$endif}
         {$ifdef SENTINELVIRTUAL}
         FSentinelRegistry.FDesactivarConteoVirtual := FALSE;
         {$endif}
     end;
end;

procedure TSystemData.LoadCompanyDataCallBack( const sMsg: String );
begin
end;

procedure TSystemData.LoadCompanyDataCallBackError( const sMsg: String; Error: Exception );
begin
     {$ifdef ANTES}
          AddText( Format( '%s: %s', [ sMsg, Error.Message ] ) );
     {$else}
          AddText( Format( '<Error><Mensaje><![CDATA[%s]]></Mensaje><Excepcion><![CDATA[%s]]></Excepcion></Error>', [ sMsg, Error.Message ] ) );
     {$endif}
end;

procedure TSystemData.LoadUserData;
begin
     with FDatos do
     begin
     {$ifdef ANTES}
          Add( '' );
          Add( '----------------------------------------------------' );
          Add( 'Usuarios Totales: ' + Trim( Format( '%9.0n', [ dmDBConfig.UsuariosContar / 1 ] ) ) );
          Add( '----------------------------------------------------' );
          Add( '' );
     {$else}
          Add( '<UserData UsuariosTotales="' + Trim( Format( '%9.0n', [ dmDBConfig.UsuariosContar / 1 ] ) ) +'" />');
     {$endif}
     end;
end;

procedure TSystemData.AddText( const sText : string ) ;
begin
     FDatos.Add( sText );
end;

function TSystemData.LoadCompanyData : string;
const
     K_COMPANY = '%-10.10s %-50.50s %9.0n %9.0n %9.0n %-6.6s %-50.50s %-11.11s %-11.11s %-11.11s %-40.40s %4.4d %4.4d %6.6d %-11.11s %2.2s %6.6d %-11.11s %6.6d %-11.11s';
     K_TOTALES = '%-10.10s %-50.50s %9.0n %9.0n %9.0n';
     K_COMPANY_ATTS = 'CM_3_PROD="%s" '+
                      'CM_CTRL_RL="%s" CM_CODIGO="%s" CM_NOMBRE="%s"  CM_ACTIVOS="%d"  CM_INACTIV="%d" CM_TOTAL="%d" CM_NIVEL0="%s"  CM_DATOS="%s" DB_SIZE="%f" MS_SQL_VER="%s" MS_SQL_LEV="%s" MS_SQL_EDT="%s" CM_ULT_ALT="%s" CM_ULT_BAJ="%s" '+
                      'CM_ULT_TAR="%s" PE_YEAR="%d"  PE_TIPO="%d" PE_NUMERO="%d" PE_FEC_INI="%s" CM_CONNECT="%s"  PC_SI_QTY="%d" PC_SI_FEC="%s" PC_AN_QTY="%d" PC_AN_FEC="%s" '+
                      'PC_NOMLAVG="%f" PC_NOM_AVG="%f" ';

     K_NUEVOS_ATTS = 'PC_FOA_QTY="%d"  PC_FOA_FEC="%s" PC_FON_QTY="%d"  PC_FON_FEC="%s" PC_FOC_QTY="%d"  PC_FOC_FEC="%s" PC_LB_QTY="%d" PC_LB_FEC="%s" PC_EM_QTY="%d" '+
                     'PC_EM_FEC="%s" RE_CU_QTY="%d" RE_CF_QTY="%d" RE_CF_FEC="%s" RE_RG_QTY="%d" RE_RG_FEC="%s" RE_SM_QTY="%d" RE_SM_FEC="%s" RE_PC_QTY="%d" RE_PC_FEC="%s" RE_EV_QTY="%d" RE_EV_FEC="%s" ';

     K_OTROS_ATTS = '<Otros K_MDAT_QTY="%d" K_MDAT_FEC="%s" K_KIOS_QTY="%d" K_KIOS_FEC="%s" K_WF_QTY="%d" K_WF_FEC="%s" K_SEL_BD="%d" K_SEL_QTY="%d" K_SEL_FEC="%s" K_VIS_BD="%d" K_VIS_QTY="%d" K_VIS_FEC="%s" />';

     K_TOTALES_ATTS = '<Totales TotalActivos="%d" ActivosProduccion="%d" ActivosPruebas="%d" TotalBajas="%d" BajasProduccion="%d" BajasPruebas="%d"  CantAltasBD="%d"  UltimaAltaBD="%s"/>';

var
   oZetaProvider: TdmZetaServerProvider;
   FManager: TLicenseMgr;
   rActivos, rInactivos : Integer;
   rActivosTest, rInactivosTest : Integer;
   sBaseDeDatos, sDatabase, sNivel0: String;
   FSeleccionDatos : TDatosSeleccion;
   FVisitantesDatos : TDatosVisitantes;
   FModulosComparteDatos : TDatosModulosComparte;
   FDbConfigManager : TDbConfigManager;

   function EsTressProduccion( sCM_CTRL_RL : string ) : boolean;
   begin
        Result :=  StrVacio(sCM_CTRL_RL) or  ( sCM_CTRL_RL = '3DATOS' );

   end;


   function HTMLEncode(const Data: string): string;
   var
    i: Integer;
   begin

    result := '';
    for i := 1 to length(Data) do
      case Data[i] of
        '<': result := result + '&lt;';
        '>': result := result + '&gt;';
        '&': result := result + '&amp;';
        '"': result := result + '&quot;';
      else
        result := result + Data[i];
      end;
   end;



begin
     oZetaProvider := TdmZetaServerProvider.Create( Self );
     try
        FManager := TLicenseMgr.Create( oZetaProvider );
        try
           with FManager do
           begin
                AutoServer := Self.AutoServer;
           end;
           with dmDBConfig do
           begin
                FDbConfigManager := GetDBConfigMgr;
                FModulosComparteDatos :=  FManager.RegistrosModulosComparte;
                EmpresasConectar(True, tcRecluta );
                FSeleccionDatos := SeleccionLeer(FManager, oZetaProvider, LoadCompanyDataCallBack, LoadCompanyDataCallBackError );
                EmpresasConectar(True, tcVisitas );
                FVisitantesDatos := VisitantesLeer(FManager, oZetaProvider, LoadCompanyDataCallBack, LoadCompanyDataCallBackError );
                EmpresasConectar( {$ifdef VALIDA_EMPLEADOS}False{$else}True{$endif} );
                LicenciasLeer( FManager, oZetaProvider, LoadCompanyDataCallBack, LoadCompanyDataCallBackError );
                rActivos := 0;
                rInactivos := 0;
                rActivosTest := 0;
                rInactivosTest := 0;
                //rTotal := 0;
                with FDatos do
                begin
                     with cdsEmpresas do
                     begin
                          IndexFieldNames := 'CM_DATOS';
                          First;
                          sDatabase := VACIO;

                          AddText( Format( '<Companies Cantidad="%d">', [cdsEmpresas.RecordCount] ));
                          while not Eof do
                          begin
                               AddText( '<Company ' );
                               sBaseDeDatos := UpperCase( Trim( FieldByName( 'CM_DATOS' ).AsString ) );
                               sNivel0 := FieldByName( 'CM_NIVEL0' ).AsString;
                               AddText( Format( K_COMPANY_ATTS, [
                                                         BoolToAttBoolean(  EsTressProduccion( FieldByName( 'CM_CTRL_RL' ).AsString ) ),
                                                         FieldByName( 'CM_CTRL_RL' ).AsString,
                                                         HTMLEncode( FieldByName( 'CM_CODIGO' ).AsString ),
                                                         HTMLEncode( FieldByName( 'CM_NOMBRE' ).AsString ),
                                                         FieldByName( 'CM_ACTIVOS' ).AsInteger,
                                                         FieldByName( 'CM_INACTIV' ).AsInteger,
                                                         FieldByName( 'CM_TOTAL' ).AsInteger,
                                                         HTMLEncode( sNivel0 ),
                                                         HTMLEncode( sBaseDeDatos ),
                                                         FieldByName( 'DB_SIZE' ).AsFloat,
                                                         HTMLEncode(FieldByName( 'MS_SQL_VER' ).AsString),
                                                         HTMLEncode(FieldByName( 'MS_SQL_LEV' ).AsString),
                                                         HTMLEncode(FieldByName( 'MS_SQL_EDT' ).AsString),
                                                         ReportaFecha( FieldByName( 'CM_ULT_ALT' ).AsDateTime ),
                                                         ReportaFecha( FieldByName( 'CM_ULT_BAJ' ).AsDateTime ),
                                                         ReportaFecha( FieldByName( 'CM_ULT_TAR' ).AsDateTime ),
                                                         //FieldByName( 'CM_ULT_NOM' ).AsString,
                                                         FieldByName( 'PE_YEAR' ).AsInteger,
                                                         FieldByName( 'PE_TIPO' ).AsInteger,
                                                         FieldByName( 'PE_NUMERO' ).AsInteger,
                                                         ReportaFecha( FieldByName( 'PE_FEC_INI' ).AsDateTime ),
                                                         HTMLEncode( FieldByName( 'CM_CONNECT' ).AsString ),
                                                         FieldByName( 'PC_SI_QTY' ).AsInteger,
                                                         ReportaFecha( FieldByName( 'PC_SI_FEC' ).AsDateTime ),
                                                         FieldByName( 'PC_AN_QTY' ).AsInteger,
                                                         ReportaFecha( FieldByName( 'PC_AN_FEC' ).AsDateTime ),
                                                         FieldByName( 'PC_NOMLAVG' ).AsFloat,
                                                         FieldByName( 'PC_NOM_AVG' ).AsFloat
                                                         ] ) );
                               AddText( Format( K_NUEVOS_ATTS,
                                    [
                                       FieldByName( 'PC_FOA_QTY' ).AsInteger,
                                       ReportaFecha( FieldByName( 'PC_FOA_FEC' ).AsDateTime ),
                                       FieldByName( 'PC_FON_QTY' ).AsInteger,
                                       ReportaFecha( FieldByName( 'PC_FON_FEC' ).AsDateTime ),
                                       FieldByName( 'PC_FOC_QTY' ).AsInteger,
                                       ReportaFecha( FieldByName( 'PC_FOC_FEC' ).AsDateTime ),
                                       FieldByName( 'PC_LB_QTY' ).AsInteger,
                                       ReportaFecha( FieldByName( 'PC_LB_FEC' ).AsDateTime ),
                                       FieldByName( 'PC_EM_QTY' ).AsInteger,
                                       ReportaFecha( FieldByName( 'PC_EM_FEC' ).AsDateTime ),
                                       FieldByName( 'RE_CU_QTY' ).AsInteger,
                                       FieldByName( 'RE_CF_QTY' ).AsInteger,
                                       ReportaFecha( FieldByName( 'RE_CF_FEC' ).AsDateTime ),
                                       FieldByName( 'RE_RG_QTY' ).AsInteger,
                                       ReportaFecha( FieldByName( 'RE_RG_FEC' ).AsDateTime ),
                                       FieldByName( 'RE_SM_QTY' ).AsInteger,
                                       ReportaFecha( FieldByName( 'RE_SM_FEC' ).AsDateTime ),
                                       FieldByName( 'RE_PC_QTY' ).AsInteger,
                                       ReportaFecha( FieldByName( 'RE_PC_FEC' ).AsDateTime ),
                                       FieldByName( 'RE_EV_QTY' ).AsInteger,
                                       ReportaFecha( FieldByName( 'RE_EV_FEC' ).AsDateTime )
                                    ] ) );

                               AddText('>');
                               if ( sDatabase <> sBaseDeDatos )  then
                               begin
                                    if EsTressProduccion( FieldByName( 'CM_CTRL_RL' ).AsString )  then
                                    begin
                                       rActivos := rActivos + FieldByName( 'CM_ACTIVOS' ).AsInteger;
                                       rInactivos := rInactivos + FieldByName( 'CM_INACTIV' ).AsInteger;
                                    end
                                    else
                                    if ( FieldByName( 'CM_CTRL_RL' ).AsString = '3PRUEBA' ) then
                                    begin
                                       rActivosTest := rActivosTest + FieldByName( 'CM_ACTIVOS' ).AsInteger;
                                       rInactivosTest := rInactivosTest + FieldByName( 'CM_INACTIV' ).AsInteger;
                                    end;
                               end;

                               sDatabase := sBaseDeDatos;
                               AddText(FieldByName( 'COMPANYXMLDATA' ).AsString);
                               AddText('</Company>');
                               Next;
                          end;
                     end;
                     AddText( '</Companies>' );

//                     FModulosComparteDatos.KioscoCuantos
                     //K_OTROS_ATTS = '<Otros K_MDAT_QTY="%d" K_MDAT_FEC="%s" K_KIOS_QTY="%d" K_KIOS_FEC="%s" K_WF_QTY="%d" K_WF_FEC="%s" K_SEL_BD="%d" K_SEL_QTY="%d" K_SEL_FEC="%s" K_VIS_BD="%d" K_VIS_QTY="%d" K_VIS_FEC="%s" />';
                     with  FModulosComparteDatos, FSeleccionDatos, FVisitantesDatos do
                     begin
                         AddText( Format( K_OTROS_ATTS, [MisDatosCuantos, ReportaFecha(MisDatosUltimo),
                                  KioscoCuantos, ReportaFecha(KioscoUltimo),
                                  WorkflowCuantos, ReportaFecha(WorkflowUltimo),
                                  FSeleccionDatos.BDCuantos,  RequisicionCuantos, ReportaFecha(RequisicionUltimo),
                                  FVisitantesDatos.BDCuantos, LibroCuantos, ReportaFecha( LibroUltimo )   ] )  );
                     end;

                     //FDbConfigManager.
                     //K_TOTALES_ATTS = '<Totales TotalActivos="%d" ActivosProduccion="%d" ActivosPruebas="%d" TotalBajas="%d" BajasProduccion="%d" BajasPruebas="%d"  CantAltasBD="%d"  UltimaAltaBD="%s" />';
                     AddText( Format( K_TOTALES_ATTS, [rActivos + rActivosTest, rActivos, rActivosTest, rInactivos+rInactivosTest,rInactivos, rInactivosTest, FDbConfigManager.GetDbConfigRec.Cantidad,  ReportaFecha( FDbConfigManager.GetDbConfigRec.Ultima )  ] ) );



                end;
           end;
        finally

               FreeAndNil( FManager );
        end;
     finally
            FreeAndNil( oZetaProvider );
     end;
end;

{$ifdef ANTES}
procedure TSystemData.LoadCompanyData;
const
     K_COMPANY = '%-10.10s %-50.50s %9.0n %9.0n %9.0n %-6.6s %-50.50s %-11.11s %-11.11s %-11.11s %-40.40s %4.4d %4.4d %6.6d %-11.11s %2.2s %6.6d %-11.11s %6.6d %-11.11s';
     K_TOTALES = '%-10.10s %-50.50s %9.0n %9.0n %9.0n';
var
   oZetaProvider: TdmZetaServerProvider;
   FManager: TLicenseMgr;
   rActivos, rInactivos, rTotal: Extended;
   sBaseDeDatos, sDatabase, sNivel0: String;

function ReportaFecha( const dValue: TDate ): String;
begin
     if ( dValue > NullDateTime ) then
        Result := FormatDateTime( K_FECHA, dValue )
     else
         Result := VACIO;
end;

begin
     oZetaProvider := TdmZetaServerProvider.Create( Self );
     try
        FManager := TLicenseMgr.Create( oZetaProvider );
        try
           with FManager do
           begin
                AutoServer := Self.AutoServer;
           end;
           with dmDBConfig do
           begin
                EmpresasConectar( {$ifdef VALIDA_EMPLEADOS}False{$else}True{$endif} );
                LicenciasLeer( FManager, oZetaProvider, LoadCompanyDataCallBack, LoadCompanyDataCallBackError );
                rActivos := 0;
                rInactivos := 0;
                rTotal := 0;
                with FDatos do
                begin
                     Add( '' );
                     Add( 'C�digo     Empresa                                              Activos Inactivos     Total Nivel0 Base De Datos                                      Ult. Alta   Ult. Baja   U. Tarjeta  Ultima N�mina Calculada Total            '+
                          'Year Tipo N�mero Fech Inicio OK SI QTY SI ULT FECH AN QTY AN ULT FECH' );
                     Add( '---------- -------------------------------------------------- --------- --------- --------- ------ -------------------------------------------------- ----------- ----------- ----------- ---------------------------------------- '+
                          '---- ---- ------ ----------- -- ------ ----------- ------ -----------' );
                     with cdsEmpresas do
                     begin
                          IndexFieldNames := 'CM_DATOS';
                          First;
                          sDatabase := VACIO;
                          while not Eof do
                          begin
                               sBaseDeDatos := UpperCase( Trim( FieldByName( 'CM_DATOS' ).AsString ) );
                               sNivel0 := FieldByName( 'CM_NIVEL0' ).AsString;
                               Add( Format( K_COMPANY, [ FieldByName( 'CM_CODIGO' ).AsString,
                                                         FieldByName( 'CM_NOMBRE' ).AsString,
                                                         FieldByName( 'CM_ACTIVOS' ).AsFloat,
                                                         FieldByName( 'CM_INACTIV' ).AsFloat,
                                                         FieldByName( 'CM_TOTAL' ).AsFloat,
                                                         sNivel0,
                                                         sBaseDeDatos,
                                                         ReportaFecha( FieldByName( 'CM_ULT_ALT' ).AsDateTime ),
                                                         ReportaFecha( FieldByName( 'CM_ULT_BAJ' ).AsDateTime ),
                                                         ReportaFecha( FieldByName( 'CM_ULT_TAR' ).AsDateTime ),
                                                         FieldByName( 'CM_ULT_NOM' ).AsString,
                                                         FieldByName( 'PE_YEAR' ).AsInteger,
                                                         FieldByName( 'PE_TIPO' ).AsInteger,
                                                         FieldByName( 'PE_NUMERO' ).AsInteger,
                                                         ReportaFecha( FieldByName( 'PE_FEC_INI' ).AsDateTime ),
                                                         FieldByName( 'CM_CONNECT' ).AsString,
                                                         FieldByName( 'PC_SI_QTY' ).AsInteger,
                                                         ReportaFecha( FieldByName( 'PC_SI_FEC' ).AsDateTime ),
                                                         FieldByName( 'PC_AN_QTY' ).AsInteger,
                                                         ReportaFecha( FieldByName( 'PC_AN_FEC' ).AsDateTime )
                                                         ] ) );
                               if ( sDatabase <> sBaseDeDatos ) then
                               begin
                                    rActivos := rActivos + FieldByName( 'CM_ACTIVOS' ).AsInteger;
                                    rInactivos := rInactivos + FieldByName( 'CM_INACTIV' ).AsInteger;
                                    rTotal := rTotal + FieldByName( 'CM_TOTAL' ).AsInteger;
                               end;
                               sDatabase := sBaseDeDatos;
                               Next;
                          end;
                     end;
                     Add( '                                                              --------- --------- ---------' );
                     Add( Format( K_TOTALES, [ '', '', rActivos, rInactivos, rTotal ] ) );
                end;
           end;
        finally
               FreeAndNil( FManager );
        end;
     finally
            FreeAndNil( oZetaProvider );
     end;
end;
{$endif}

function TSystemData.ExportData( const sFileName: String ): Boolean;
var
   i: Integer;
begin
     Result := False;
     try
        FDatos := TStringList.Create;
        try
           FDatos.Add('<?xml version="1.0" encoding="ISO-8859-1" ?>');
           FDatos.Add( Format( '<TressDT Fecha="%s">', [ FormatDateTime( K_FECHA, Now )] ) );
           ZetaWinAPITools.WriteSystemInfo( FDatos, True);
           LoadSentinelData;
           LoadUserData;
           LoadCompanyData;
           AddText('</TressDT>');
           with FDatos do
           begin
          {$ifdef DEBUG_XML_3DT}
                SaveToFile( sFileName  + '.xml' );
          {$endif}
                for i := 0 to ( Count - 1 ) do
                begin
                     Strings[ i ] := ZetaServerTools.Encrypt( Strings[ i ] );
                end;

                SaveToFile( sFileName );
           end;
        finally
               FreeAndNil( FDatos );
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
end;

procedure TSystemData.ExportarClick(Sender: TObject);
var
   oCursor: TCursor;
   sFileName: String;
begin
     with SaveDialog do
     begin
          FileName := Format( '%sDatosSistema.3dt', [ ExtractFilePath( Application.ExeName ) ] );
          if Execute then
          begin
               sFileName := FileName;
               oCursor := Screen.Cursor;
               Screen.Cursor := crHourglass;
               try
                  if  Strvacio( ExtractFileExt( sFileName ) ) then
                    sFileName := sFileName + '.3dt';

                  if ExportData( sFileName ) then
                     ZetaDialogo.zInformation( 'Los Datos Han Sido Exportados',
                                               'Los Datos Del Sistema Han Sido Exportados Hacia El Archivo' + CR_LF + CR_LF + sFileName,
                                               0 );
               finally
                      Screen.Cursor := oCursor;
               end;
          end;
     end;
end;

function TSystemData.BoolToAttBoolean(lValue: boolean): String;
begin
     if ( lValue ) then
        Result := 'true'
     else
         Result := 'false';
end;

end.
