unit FSPConsulta;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Db, Grids, DBGrids,
     FSPEdit,
     ZBaseDlgModal,
     ZetaDBGrid;

type
  TSPConsulta = class(TZetaDlgModal)
    ZetaDBGrid: TZetaDBGrid;
    DataSource: TDataSource;
    Agregar: TSpeedButton;
    Borrar: TSpeedButton;
    Modificar: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure AgregarClick(Sender: TObject);
    procedure BorrarClick(Sender: TObject);
    procedure ModificarClick(Sender: TObject);
    procedure DataSourceStateChange(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure ZetaDBGridDblClick(Sender: TObject);
  private
    { Private declarations }
    FSPEdit: TSPEdit;
    function Dataset: TDataset;
    procedure SetControls;
    procedure Editar;
    procedure Refrescar;
  public
    { Public declarations }
    function Init: Boolean;
  end;

var
  SPConsulta: TSPConsulta;

implementation

uses ZetaDialogo,
     ZetaCommonClasses,
     FHelpContext,
     DReportes;

{$R *.DFM}

{ TSPConsulta }

procedure TSPConsulta.FormCreate(Sender: TObject);
begin
     inherited;
     dmReportes := TdmReportes.Create( Self );
     HelpContext := H00027_Stored_Procedures_Adicionales;
end;

procedure TSPConsulta.FormShow(Sender: TObject);
begin
     inherited;
     Datasource.Dataset := Self.Dataset;
end;

procedure TSPConsulta.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FSPEdit );
     FreeAndNil( dmReportes );
     inherited;
end;

function TSPConsulta.Dataset: TDataset;
begin
     Result := dmReportes.GetPatchSPDataSet;
end;

function TSPConsulta.Init: Boolean;
begin
     try
        with dmReportes do
        begin
             Result := AbreSPExtras;
        end;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                Result := False;
           end;
     end;


end;

procedure TSPConsulta.SetControls;
var
   lHay, lEdit: Boolean;
begin
     with Dataset do
     begin
          lHay := not IsEmpty;
          lEdit := ( State in [ dsEdit, dsInsert ] );
     end;
     Agregar.Enabled := not lEdit;
     Borrar.Enabled := lHay and not lEdit;
     Modificar.Enabled := lHay and not lEdit;
end;

procedure TSPConsulta.Editar;
var
   Pos : TBookMark;
begin

     Pos :=  Dataset.GetBookmark;
     if not Assigned( SPEdit ) then
        FSPEdit := TSPEdit.Create( Self );
     with FSPEdit do
     begin
          Datasource.Dataset := Self.Dataset;
          ShowModal;

          DataSet.DisableControls;
          Refrescar;

          if ( not Agrego ) and (Pos <> nil)  then
          begin
             with Self.DataSet do
             begin
                if BookMarkValid( Pos ) then
                        GotoBookMark( Pos );
                 FreeBookMark( Pos );
             end;
          end;
          DataSet.EnableControls;

     end;
end;

procedure TSPConsulta.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     SetControls;
end;

procedure TSPConsulta.DataSourceStateChange(Sender: TObject);
begin
     inherited;
     SetControls;
end;

procedure TSPConsulta.ZetaDBGridDblClick(Sender: TObject);
begin
     inherited;
     Modificar.Click;
end;

procedure TSPConsulta.AgregarClick(Sender: TObject);
begin
     inherited;
     with Dataset do
     begin
          Append;
     end;
     Editar;
end;

procedure TSPConsulta.BorrarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     if ZetaDialogo.zConfirm( '� Atenci�n !', '� Desea Borrar Este Stored Procedure ?', 0, mbNo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
            // Dataset.Delete;
             dmReportes.BorraSPPatch;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TSPConsulta.ModificarClick(Sender: TObject);
begin
     inherited;
     Dataset.Edit;
     Editar;
end;

procedure TSPConsulta.FormClose(Sender: TObject; var Action: TCloseAction);

const
     K_MENSAJE = 'Los Stored Procedures Adicionales est�n almacenados en la Base de Datos de Comparte.' + CR_LF +
                 'Estos se conservar�n en la tabla SP_PATCH';

begin
     inherited;
     ZetaDialogo.zInformation( '� Atenci�n !', K_MENSAJE, 0 );
end;

procedure TSPConsulta.Refrescar;
begin
    dmReportes.RefrescaPatchSPDataSet;
end;

end.
