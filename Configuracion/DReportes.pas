unit DReportes;

interface
                                                                                                                                                                                   
uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBTables, DBGrids, BDE, TMultiP,
     DMigrar,
     FAutoServer,
     ZetaCommonLists,
     ZetaMigrar, DBClient, ZetaServerDataSet;

type
  TSQLAgenteClient = class(TObject);
  eFormatoFecha = ( ffDDMMYY, ffDDMMYYYY, ffMMDDYY, ffMMDDYYYY, ffYYMMDD, ffYYYYMMDD );
  ERangoScript = ( esTodos, esRango, esLista );
  TModoPatch = record
    Habilitado: Boolean;
    Filtro: ERangoScript;
    Inicial: Integer;
    Final: Integer;
    Lista: String;
  end;
  TdmReportes = class(TdmMigrar)
    tqRead: TQuery;
    dbFuente: TDatabase;
    tqReporte: TQuery;
    tqCamposRep: TQuery;
    procedure dmMigrarCreate(Sender: TObject);
    procedure dmMigrarDestroy(Sender: TObject);
    procedure SE_TIPOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure ttSourceNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
    FLista1: TStrings;
    FLista2: TStrings;
    FVerActual: String;
    FExiste_VA: Boolean;
    FTxCounter: Integer;
    FCompanyType: eTipoCompany;
    function AbreTabla( Tabla: TTable; const sTabla: String ): Boolean;
    function BorrarStoredProcedures( const lEnabled: Boolean; BaseDatos: TDatabase ): Boolean;
    function BorrarFunciones( const lEnabled: Boolean; BaseDatos: TDatabase ): Boolean;
    function BorraTablas: Boolean;
    function BorrarTriggers( const lEnabled: Boolean; BaseDatos: TDatabase ): Boolean;
    function CrearTriggers( const lEnabled: Boolean; const sPatchFile, sVerActual, sVersion: String; BaseDatos: TDatabase ): Boolean;
    function CrearStoredProcedures( const lEnabled: Boolean; const sPatchFile, sVerActual, sVersion, sSupNivel: String; BaseDatos: TDatabase ): Boolean;
    {$ifdef ANTES}
    function CrearStoredProceduresExtras( const lEnabled: Boolean; const sPatchFile: String; const iTipo: Integer; BaseDatos: TDatabase ): Boolean;
    {$endif}
    function CrearStoredProceduresExtras( const lEnabled: Boolean; const iTipo: Integer; BaseDatos: TDatabase): Boolean;
    function EjecutaScript( const Modo: TModoPatch; const sPatchFile, sVerActual, sVersion, sTitulo, sToken, sValor: String; const iClase: Integer; BaseDatos: TDatabase ): Boolean; overload;
    function EjecutaScript( const Modo: TModoPatch; const sPatchFile, sVerActual, sVersion, sTitulo, sToken, sValor: String; const iClase: Integer; BaseDatos: TDatabase;  var lHuboAdvertencias : boolean): Boolean;overload;
    function FormatScript( const sScript: String ): String;
    function GetAccion( const lValor: Boolean ): String;
    function GetGlobalVersion: Integer;
    function GetModo( const Valor: TModoPatch ): String;
    function GetModoPatchDefault: TModoPatch;
    function GetRealTableName(const sTable: String): String;
    function GetScriptFieldName: String;
    function GetScriptDropTrigger: String;
    function GetScriptDropProcedure: String;
    function GetGlobalDirectorioPlantillas : String;
    function LeeTablas( Empresa: TCompanyData; const iYear: Integer; const dInicial, dFinal: TDate; const iMaxEmpleados: Integer; const iMaxEmpDBSource: Integer ): Boolean;
    function ListStoredProcedures( BaseDatos: TDatabase ): Boolean;
    function PK_Violation( Error: Exception ): Boolean;
    function AbreArchivoSPExtras: Boolean;
    {$ifdef MSSQL}
    function ListarProcesos( BaseDatos: TDatabase; const sScript, sNombre: String ): Boolean;
    function ListFunciones( BaseDatos: TDatabase ): Boolean;
    {$endif}
    function Patch( const Modo: TModoPatch; const sPatchFile, sVerActual, sVersion: String; BaseDatos: TDatabase ): Boolean; overload;
    function Patch( const Modo: TModoPatch; const sPatchFile, sVerActual, sVersion: String; BaseDatos: TDatabase;  var lHuboAdvertencias : boolean ): Boolean; overload;
    function RecalcularKardex( BaseDatos: TDatabase ): Boolean;
    {$ifdef INTERBASE}
    function CopiaDerechosAdicionales( BaseDatos: TDatabase ): Boolean;
    function RestructurarDerechosOtros : Boolean;
    {$ENDIF}
    procedure PrepararEstructura(const sVersion : string);
    function TerminaConsolidacion( const sTableName: String; const lExito: Boolean ): Boolean;
    function TransactionCommit( const sMensaje: String ): Boolean;
    procedure AttachQuery( BaseDatos: TDatabase; Query: TQuery; const sScript: String );
    procedure CambiaGlobalVersion( sVersion: String );
    procedure CreateInsertScript( const sTableName: String; Target: TQuery; const BaseDatos: TDataBase = NIL   ); overload;
    procedure CreateInsertScript( const sTableName: String; Target: TQuery; Campos: TStrings; const BaseDatos: TDataBase = NIL  ); overload;
    procedure DespreparaQuery( Query: TQuery );
    procedure DeterminaDependencias( sValor: String );
    procedure EjecutaDDL( BaseDatos: TDatabase; Query: TQuery; const sSQL: String );
    procedure PreparaQuery( Query: TQuery; const sScript: String; const BaseDatos: TDataBase = NIL  );
    procedure PreparaQueryParadox( Query: TQuery; const sScript: String );
    procedure PreparaTabla( Tabla: TTable; const sTabla: String );
    procedure TransactionEnd;
    procedure TransactionStart;
    //procedure TransfiereDatasets( sTabla: String);
    procedure InitTableFieldLists(const sTableName: String{$ifdef INTERBASE};const lExcluirBlobs: Boolean = FALSE{$endif});
    procedure CambiaClavesVersion(const sVersion: String);
    function GetTipoBitacora(const sMensaje: String): eTipoBitacora;
    {$ifdef MSSQL}
    function ExcluirProdecure (const sItemName:string) :Boolean;
    {$endif}
  public
    { Public declarations }

    property CompanyType: eTipoCompany read FCompanyType write FCompanyType;
    property Existe_VA: Boolean read FExiste_VA;
    property Lista1: TStrings read FLista1;
    property VerActual: String read FVerActual;
    function AbreArchivosReportes: Boolean;
    function AbreArchivoConceptos( DataSource: TDataSource ): Boolean;
    function AbreArchivoParadox( const sFileName: String ): Boolean;

    function AbreSPExtras : Boolean;
    function CrearTablaEspeciales(var nProcedures : integer; const BaseDatos: TDataBase) : Boolean;
    function ImportarArchivoSPExtras ( const BaseDatos: TDataBase ): Boolean;

    function AbreBDReceptora: Boolean; overload;
    function AbreBDReceptora(lMostrarError : boolean) : Boolean; overload;
    function CierraBDReceptora: Boolean;
    function ActualizarDatos( const sPatchFile, sVerActual, sVersion: String; const lTriggers, lSP, lKardex, lDiccionario, lReportes: Boolean; const ModoEstructuras: TModoPatch ): Boolean; overload;
    function ActualizarDatos( const sPatchFile, sVerActual, sVersion: String; const lTriggers, lSP, lKardex, lDiccionario, lReportes: Boolean; const ModoEstructuras: TModoPatch; var lHuboErrores, lHuboAdvertencias : boolean; const lTratarAdvertencias : boolean = FALSE ): Boolean; overload;
    function ActualizarComparte( const sPatchFile, sVerActual, sVersion: String; const lTriggers, lSP: Boolean; const ModoEstructuras: TModoPatch ): Boolean;
    {$ifdef MSSQL}
    procedure TransformaScriptMSSQL(var  sScript : string; const sServer, sDatabaseEmpresa, sDatabaseComparte : string );
    {$endif}
    function ConfiguraBaseDeDatos( const sFileName, sDatabase: String; const iTipo: Integer ): Boolean;
    function GetConsolidationSteps( Lista: TCompanyList ): Integer;
    function GetCuantosConceptos: Integer;
    function GetExportSteps: Integer;
    function GetImportSteps: Integer;
    function GetImportTableSteps: Integer;
    function GetPatchSteps: Integer;
    function GetConfigureBDSteps: Integer;
    function GetSPFileName( const sExtension: String ): String;
    function GetVerActual: String;
    function GetVerActualComparte: String;
    function GetVersionList( Lista: TStrings; const sPatchFile: String ): Boolean;
    function ExportarReportes( Incluidos: TBookMarkList ): Boolean;
    function ExportarTablas: Boolean;
    function ImportarASCII( const lValidate: Boolean; const sTableName, sArchivo: String; Campos: TStrings; const eFormato: eFormatoFecha ): Boolean;
    function ImportarASCIIInit( const sArchivo: String ): Integer;
    function ImportarConceptos( Incluidos: TBookMarkList ): Boolean;
    function ImportarReportes: Boolean;
    function ImportarReportesRP1(sDirectorioReportes :  string; listaReportes : TStringList;   lImportarPlantilla,  lSustituirPlantilla, lContinuarNoExistePlantilla: boolean) : Integer;
    function ImportarTabla( const sTableName: String; Mode: TBatchMode ): Boolean;
    function ImportarImagenes( const sTipo, sFileSpec: String; lConservarImagen : Boolean = FALSE ): Boolean;
    function InicializaBaseDeDatos( ListaTablas: TStrings; const lEsComparte: Boolean = FALSE ): Boolean;
    function TablaExiste( const sTableName: String ): Boolean;
    function GetGlobalConservarImagen: Boolean;
    procedure ConfiguraBaseDeDatosMsg( const sMsg: String );
    procedure ConfiguraBaseDeDatosError( const sMsg: String; Error: Exception );
    procedure Consolidar( const sNombre: String; Lista: TCompanyList; const iYear: Integer; const dInicial, dFinal: TDate; const lBorrarTablas: Boolean; const iMaxEmpleados: Integer );
    procedure GetDescripcionCampos(const sTabla: String; const iClase: Integer; Campos: TStrings);
    procedure GetDescripcionTablas(Lista: TStrings);
    procedure GetListaTablas( ListaTablas: TStrings );
    procedure OpenReportTable( DataSource: TDataSource );
    function GetListaClasifiEmpresa( oLista : TStrings ): Boolean;
    function GetPosClasifiEmp( oLista: TStrings; const eTipo: eClasifiReporte ):Integer;
    function GetPatchSPDataSet : TDataSet;
    procedure RefrescaPatchSPDataSet;
    function GrabaSPPatch : boolean;
    function BorraSPPatch : boolean;
    function AgregaSPPatch : boolean;
    function LlenaListaCompaniasAplica( Lista: TStrings; const tcTipo : eTipoCompany ): Boolean;


    {Funciones Dummy para tener compatiblidad con codigo TRESS}
    function EvaluaParametros( oSQLAgente : TSQLAgenteClient; Parametros : TStrings;var sError : wideString;var oParams : OleVariant;const lMuestraDialogo : Boolean ) : Boolean;
  end;

var
  aFormatoFecha: array[ eFormatoFecha ] of pChar = ( 'DD/MM/YY', 'DD/MM/YYYY', 'MM/DD/YY', 'MM/DD/YYYY', 'YYMMDD', 'YYYYMMDD' );
  dmReportes: TdmReportes;

implementation

uses DDBConfig,
{$ifdef MSSQL}
     DConfigurarEspeciales,
{$endif}
     BDEFunctions,
     ZAsciiTools,
     ZGlobalTress,
     ZetaAsciiFile,
     ZetaTressCFGTools,
     ZetaCommonTools,
     ZetaServerTools,
     ZetaCommonClasses,
     FDatosImportReporte,
     ZetaTipoEntidad,
     ZetaTipoEntidadTools,
     ZetaDialogo,
     FToolsFoto;

const
     UnaComilla = Chr( 39 ); { ' }
     K_TRIGGER = 0;
     K_STORED_PROCEDURE = 1;
     K_SQL = 2;
     K_NOMBRE_EMPRESA = '#EMPRESA';
     K_NOMBRE_COMPARTE = '#COMPARTE';
     K_DIGITO_EMPRESA = '#DIGITO';
     K_PUNTO = '.';

     K_OBJETO_EXISTE = 'ALREADY EXISTS';

     K_RENGLON_NUEVO = -2;
      K_TEMPLATE_EXT = '.QR2';
      K_TEMPLATE_LAND = 'DEFAULT LANDSCAPE';
      K_TEMPLATE = 'DEFAULT';
      K_TEMPLATE_HTM = 'DEFAULT.HTM';
      K_IMP_DEFAULT  = 'IMPRESORA DEFAULT:';
      K_DOC_DEFAULT = 'CONTRATO.DOC';
      K_BLOBFIELD = 'IM_BLOB';
            K_NO_REQUIERE = 'NO REQUIERE';
      K_GENERAL = '[GENERALES]';
      K_REPORTE = '[REPORTE]';

{$ifdef INTERBASE}
      K_KARCURSO_CONSTRAINT = 0;
      K_CAF_COME_CONSTRAINT = 1;
      K_CAF_INV_CONSTRAINT = 2;
      K_VACACION_CONSTRAINT = 3;
      K_EMP_POLIZA_CONSTRAINT = 4;
{$endif}

{$ifdef INTERBASE}
     K_EXISTE_GRAL =  'ATTEMPT TO STORE DUPLICATE VALUE';
{$else}
     K_PK_EXISTE =  'ALREADY HAS';
     K_FK_EXISTE = 'INTRODUCING FOREIGN KEY CONSTRAINT';
     K_COLUMNA_EXISTE = 'COLUMN NAMES IN EACH TABLE MUST BE UNIQUE';
{$endif}

   {$ifdef INTERBASE}
     Q_MAX_NUMEMP = 'select COUNT ( * ) as CUANTOS from COLABORA '+
                       'where ( ( select RESULTADO from SP_STATUS_ACT ( %s, COLABORA.CB_CODIGO ) ) = 1 ) ';
   {$else}
     Q_MAX_NUMEMP = 'select COUNT( * ) as CUANTOS from COLABORA '+
                       'where ( ( select DBO.SP_STATUS_ACT ( %s, COLABORA.CB_CODIGO ) ) = 1 ) ';
   {$endif}

   {$ifdef INTERBASE}
     K_QTY_SP_DEPENDIENTES = 16;
     aSPDependientes: array [ 1..K_QTY_SP_DEPENDIENTES ] of PChar =  ( 'GET_FEC_PROG_TOMADOS',
                                                                       'GET_FEC_PROG_INDIVID',
                                                                       'GET_FEC_PROG',
                                                                       'GET_FEC_PROG_REV',
                                                                       'GET_FEC_PROGT',
                                                                       'SP_FECHA_PATRON_CURSO',
                                                                       'SP_CHECA_PRENOM_AUTO_CHECADAS',
                                                                       'SP_CHECA_PRENOM_AUTO_AUSENCIA',
                                                                       'TF',
                                                                       'GET_STATUS_POLIZA',
                                                                       'SP_STATUS_ACT',
                                                                       'SP_STATUS_VACA',
                                                                       'SP_STATUS_INCA',
                                                                       'SP_STATUS_PERM',
                                                                       'SP_STATUS_EMP',
                                                                       'SP_JORNADA_DIARIA'
        );
   {$endif}
   {$ifdef MSSQL}
    K_QTY_SP_DEPENDIENTES = 6;
     aSPDependientes: array [ 1..K_QTY_SP_DEPENDIENTES ] of PChar =  ( 'PASOS_MODELO',
                                                                       'SP_STATUS_EMP',
                                                                       'SP_KARDEX_CB_TABLASS',
                                                                       'SP_JORNADA_DIARIA',
                                                                       'GETNOMBREUSUARIO',
                                                                       'F_GET_MTRZ_HAB'
      );
   {$endif}
                                              
   Q_SP_PATCH_COMPARTE_EXISTE  =  'select count(*) as CUANTOS from SP_PATCH';
   Q_SP_PATCH_COMPARTE_SELECT  = 'select *  from SP_PATCH order by SE_TIPO, SE_SEQ_NUM, SE_APLICA';

   {$ifdef INTERBASE}
   Q_EXISTE_TABLA_SP_ESPECIALES  = 'SELECT count(*) as CUANTOS FROM RDB$RELATIONS WHERE RDB$RELATION_NAME = ''SP_PATCH''';

   Q_CREA_TABLA_SP_ESPECIALES  =
   'CREATE TABLE SP_PATCH ( '+
   '    SE_TIPO STATUS, '+
   '    SE_SEQ_NUM STATUS, '+
   '    SE_APLICA CODIGOEMPRESA, '+
   '    SE_NOMBRE DESCRIPCION, '+
   '    SE_DESCRIP DESCLARGA, '+
   '    SE_DATA MEMO,'+
   '    SE_ARCHIVO RUTA_URL'+
   '   );';

   Q_CREA_TABLA_SP_ESPECIALES_PK =
   'ALTER TABLE SP_PATCH ' +
   '    ADD CONSTRAINT PK_SP_PATCH PRIMARY KEY (SE_TIPO, SE_SEQ_NUM, SE_APLICA );' ;

   Q_CREA_TD_COMPANY_SP_PATCH =
   'CREATE TRIGGER TD_COMPANY_SP_PATCH FOR COMPANY AFTER DELETE AS '+
   'BEGIN '+
   '     delete from SP_PATCH where ( SP_PATCH.SE_APLICA = OLD.CM_CODIGO );'+
   'END';

   Q_CREA_TU_COMPANY_SP_PATCH =
   'CREATE TRIGGER TU_COMPANY_SP_PATCH FOR COMPANY AFTER UPDATE AS '+
   'BEGIN '+
   ' IF ( OLD.CM_CODIGO <> NEW.CM_CODIGO ) THEN '+
   '  BEGIN '+
   '       update SP_PATCH set SP_PATCH.SE_APLICA = NEW.CM_CODIGO where ( SP_PATCH.SE_APLICA = OLD.CM_CODIGO ); '+
   '  END '+
   'END';
   {$endif}

   {$ifdef MSSQL}
   Q_EXISTE_TABLA_SP_ESPECIALES  ='select count(*) as CUANTOS from SYSOBJECTS where NAME = ''SP_PATCH'' and XTYPE = ''U'' ';

   Q_CREA_TABLA_SP_ESPECIALES  =
   'CREATE TABLE SP_PATCH ( '+
   '    SE_TIPO STATUS, '+
   '    SE_SEQ_NUM STATUS, '+
   '    SE_APLICA CODIGOEMPRESA, '+
   '    SE_NOMBRE DESCRIPCION, '+
   '    SE_DESCRIP DESCLARGA, '+
   '    SE_DATA MEMO, '+
   '    SE_ARCHIVO RUTA_URL)';

   Q_CREA_TABLA_SP_ESPECIALES_PK =
   'ALTER TABLE SP_PATCH ' +
   '    ADD CONSTRAINT PK_SP_PATCH PRIMARY KEY (SE_TIPO, SE_SEQ_NUM, SE_APLICA)' ;

   Q_CREA_TD_COMPANY_SP_PATCH =
   'CREATE TRIGGER TD_COMPANY_SP_PATCH ON COMPANY AFTER DELETE AS '+
   'begin '+
   '	set NOCOUNT ON; '+
   '	declare @OldCodigo CodigoEmpresa; '+
   '	select @OldCodigo = CM_CODIGO from Deleted; '+
   '	delete from SP_PATCH where SP_PATCH.SE_APLICA = @OldCodigo ; '+
   'END ';

   Q_CREA_TU_COMPANY_SP_PATCH =
   'CREATE TRIGGER TU_COMPANY_SP_PATCH ON COMPANY AFTER UPDATE AS '+
   'BEGIN '+
   '	SET NOCOUNT ON; '+
   '	IF UPDATE( CM_CODIGO ) '+
   '	BEGIN '+
   '		declare @NewCodigo CodigoEmpresa; '+
   '		declare @OldCodigo CodigoEmpresa; '+
   '		select @NewCodigo = CM_CODIGO from Inserted; '+
   '		select @OldCodigo = CM_CODIGO from Deleted; '+
   '	    update SP_PATCH set SP_PATCH.SE_APLICA = @NewCodigo where ( SP_PATCH.SE_APLICA = @OldCodigo ); '+
   '	END '+
   'END ';

   {$endif}

{$R *.DFM}

procedure TdmReportes.dmMigrarCreate(Sender: TObject);
begin
     inherited;
     FLista1 := TStringList.Create;
     FLista2 := TStringList.Create;
     FCompanyType := tc3Datos;

end;

procedure TdmReportes.dmMigrarDestroy(Sender: TObject);
begin
     FLista2.Free;
     FLista1.Free;
     inherited;
end;

procedure TdmReportes.OpenReportTable( DataSource: TDataSource );
begin
     OpenDBTarget;
     PreparaQuery( tqTarget, 'select RE_CODIGO, RE_NOMBRE, RE_TITULO from REPORTE order by RE_CODIGO' );
     tqTarget.Active := True;
     DataSource.DataSet := tqTarget;
end;

procedure TdmReportes.PreparaTabla( Tabla: TTable; const sTabla: String );
begin
     with Tabla do
     begin
          Active := False;
          DatabaseName := dbSourceShared.DatabaseName;
          TableType := ttDefault;
          TableName := sTabla;
     end;
end;

procedure TdmReportes.AttachQuery( BaseDatos: TDatabase; Query: TQuery; const sScript: String );
begin
     with Query do
     begin
          Active := False;
          DatabaseName := BaseDatos.DatabaseName;
          with SQL do
          begin
               Clear;
               Add( sScript );
          end;
     end;
end;

procedure TdmReportes.PreparaQueryParadox( Query: TQuery; const sScript: String );
begin
     AttachQuery( dbSourceShared, Query, sScript );
end;

procedure TdmReportes.PreparaQuery( Query: TQuery; const sScript: String; const BaseDatos: TDataBase  );
begin
     if ( BaseDatos = NIL ) then
        AttachQuery( dbTarget, Query, sScript )
     else
         AttachQuery( BaseDatos, Query, sScript )
end;

procedure TdmReportes.DespreparaQuery( Query: TQuery );
begin
     with Query do
     begin
          Active := False;
          while FieldCount > 0 do
          begin
               Fields[ FieldCount - 1 ].DataSet := nil;
          end;
          FieldDefs.Clear;
          if Prepared then
             Unprepare;
     end;
end;

procedure TdmReportes.EjecutaDDL( BaseDatos: TDatabase; Query: TQuery; const sSQL: String );

function BorraComentarios: String;
var
   iStart, iEnd: Integer;
   lLoop: Boolean;
begin
     Result := sSQL;
     lLoop := True;
     while lLoop do
     begin
          iStart := Pos( '/*', Result );
          if ( iStart > 0 ) then
          begin
               iEnd := Pos( '*/', Result );
               if ( iEnd = 0 ) then
                  Result := Copy( Result, 1, ( iStart - 1 ) )
               else
               begin
                    iEnd := iEnd + 3;
                    Result := Copy( Result, 1, ( iStart - 1 ) ) + Copy( Result, iEnd, ( Length( Result ) - iEnd + 1 ) );
               end;
          end
          else
              lLoop := False;
     end;
end;

begin
     DespreparaQuery( Query );
     with Query do
     begin
          DatabaseName := BaseDatos.DatabaseName;
          ParamCheck := False;
          with SQL do
          begin
               Clear;
               Text := BorraComentarios;
          end;
           ExecSQL;
          ParamCheck := True;
     end;                                
end;

procedure TdmReportes.GetListaTablas( ListaTablas: TStrings );
begin
     AbreBDReceptora;
     BDEFunctions.BDEOpenTableList( dbTarget.Handle, ListaTablas );
end;

procedure TdmReportes.GetDescripcionTablas( Lista: TStrings );
{$ifdef INTERBASE}
const
     K_MAX_INTERBASE_TABLE_NAME = 10;
{$endif}
var
   i: Integer;
   sTabla: String;
begin
     GetListaTablas( Lista );
     PreparaQuery( tqTarget, 'select DI_TITULO, DI_CALC from DICCION where ( DI_CLASIFI = -1 ) and ( DI_NOMBRE = :Tabla )' );
     with Lista do
     begin
          BeginUpdate;
          try
             for i := 0 to ( Count - 1 ) do
             begin
                  sTabla := Strings[ i ];
                  {$ifdef INTERBASE}
                  if ( Length( sTabla ) > K_MAX_INTERBASE_TABLE_NAME ) then
                  begin
                       Strings[ i ] := Format( '%s=%s', [ sTabla, sTabla ] );
                       Objects[ i ] := TObject( 0 );
                  end
                  else
                  {$endif}
                  with tqTarget do
                  begin
                       Active := False;
                       ParamByName( 'Tabla' ).AsString := sTabla;
                       Active := True;
                       if IsEmpty then
                       begin
                            Strings[ i ] := Format( '%s=%s', [ sTabla, sTabla ] );
                            Objects[ i ] := TObject( 0 );
                       end
                       else
                       begin
                            Strings[ i ] := Format( '%s=%s: %s', [ sTabla, sTabla, FieldByName( 'DI_TITULO' ).AsString ] );
                            Objects[ i ] := TObject( FieldByName( 'DI_CALC' ).AsInteger );
                       end;
                       Active := False;
                  end;
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

function TdmReportes.GetRealTableName( const sTable: String ): String;
begin
     {$ifdef INTERBASE}
     Result := sTable;
     {$endif}
     {$ifdef MSSQL}
     Result := 'DBO.' + sTable;
     {$endif}
end;

procedure TdmReportes.GetDescripcionCampos( const sTabla: String; const iClase: Integer; Campos: TStrings );
var
   i: Integer;
   sCampo: String;
begin
     with dbTarget do
     begin
          BDEFunctions.BDEOpenFieldList( Handle, GetRealTableName( sTabla ), Campos );
     end;
     if ( iClase > 0 ) then
     begin
          PreparaQuery( tqTarget, Format( 'select DI_TITULO from DICCION where ( DI_NOMBRE = :Campo ) and ( DI_CLASIFI = %d )', [ iClase ] ) );
     end;
     with Campos do
     begin
          BeginUpdate;
          try
             for i := 0 to ( Count - 1 ) do
             begin
                  sCampo := Strings[ i ];
                  if ( iClase > 0 ) then
                  begin
                       with tqTarget do
                       begin
                            Active := False;
                            ParamByName( 'Campo' ).AsString := sCampo;
                            Active := True;
                            if IsEmpty then
                               Strings[ i ] := Format( '%s=%s', [ sCampo, sCampo ] )
                            else
                                Strings[ i ] := Format( '%s=%s: %s', [ sCampo, sCampo, FieldByName( 'DI_TITULO' ).AsString ] );
                            Active := False;
                       end;
                  end
                  else
                      Strings[ i ] := Format( '%s=%s', [ sCampo, sCampo ] )
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

function TdmReportes.GetExportSteps: Integer;
begin
     Result := 3; // Llamadas a CALLBACK() en TdmReportes.ExportarReportes //
end;

function TdmReportes.TablaExiste( const sTableName: String ): Boolean;
begin
     Result := BDEFunctions.BDECheckTableExists( dbTarget.Handle, GetRealTableName( sTableName ) );
end;

function TdmReportes.AbreTabla( Tabla: TTable; const sTabla: String ): Boolean;
begin
     PreparaTabla( Tabla, sTabla );
     Tabla.Active := True;
     Result := True;
end;

function TdmReportes.AbreBDReceptora: Boolean;
begin
     Result :=   AbreBDReceptora( True );
end;

function TdmReportes.AbreBDReceptora(lMostrarError : boolean) : Boolean;
begin
     Result := False;
     try
        OpenDBTarget;
        Result := True;
     except
           on Error: Exception do
           begin
                if ( lMostrarError ) then
                begin
                    Bitacora.HandleException( '� Error Al Abrir Base De Datos !', Error );
                end
                else
                begin
                    Raise Exception.CreateFmt( '� Error Al Abrir Base De Datos ( Alias %s )!', [ TargetAlias ]);
                end;
           end;
     end;
end;




function TdmReportes.CierraBDReceptora: Boolean;
begin
     Result := False;
     try
        DespreparaQuery( tqTarget );
        CloseDBTarget;
        CloseAllDatabases;
        Result := True;
     except
           on Error: Exception do
           begin
                Bitacora.HandleException( '� Error Al Cerrar Base De Datos !', Error );
           end;
     end;
end;


function TdmReportes.ExportarReportes( Incluidos: TBookMarkList ): Boolean;
var
   i, iCount: Integer;
   sValor: String;
   {$ifdef MSSQL}
   oField: TFieldDef;
   {$endif}

begin
     sValor := '';
     Result := False;
     ClearErrorFound;
     with Bitacora do
     begin
          AgregaResumen( 'Exportaci�n de Reportes' );
          AgregaResumen( 'Base de Datos Exportadora: ' + TargetAlias );
          AgregaResumen( 'Directorio Hacia Donde Se Exporta: ' + SourceSharedPath );
          AgregaResumen( GetTimeStamp + ': Iniciando Exportaci�n De Reportes' );
     end;
     with tqTarget do
     begin
          try
             DisableControls;
             iCount := Incluidos.Count;
             for i := 0 to ( iCount - 1 ) do
             begin
                  GotoBookMark( TBookMark( Incluidos.Items[ i ] ) );
                  sValor := sValor + IntToStr( FieldByName( 'RE_CODIGO' ).AsInteger ) + ',';
             end;
             First;
          finally
                 EnableControls;
          end;
     end;
     sValor := CortaUltimo( sValor );
     Bitacora.AgregaResumen( 'Reportes a Exportar: ' + IntToStr( iCount ) );
     if StrLleno( sValor ) then
     begin
          try
             try
                OpenDBSourceShared;
                if DoEndCallBack then
                begin
                     Bitacora.AgregaResumen( 'Leyendo Reportes a Exportar' );
                     PreparaTabla( ttTarget, 'REPORTE.DB' );
                     PreparaQuery( tqSource, 'select * from REPORTE where RE_CODIGO in ( ' + sValor + ' ) order by RE_CODIGO' );
                     tqSource.Active := True;

                     {$ifdef MSSQL}
                     //Version 2008: Eliminar el campo LLAVE, para que no lo exporte.
                     oField:= tqSource.FieldDefs.Find( K_CAMPO_LLAVE_PORTAL );
                     if (oField <> NIL) then
                     begin
                          tqSource.FieldDefs.Delete(oField.Index);
                     end;
                     {$endif}

                     Bitacora.AgregaResumen( 'Exportando Reportes' );
                     with BatchMove do
                     begin
                          Mode := batCopy;
                          Source := tqSource;
                          Destination := ttTarget;
                          Execute;
                     end;
                     if DoEndCallBack then  // Tabla CAMPOREP //
                     begin
                          Bitacora.AgregaResumen( 'Leyendo Campos de Reportes a Exportar' );
                          PreparaTabla( ttTarget, 'CAMPOREP.DB' );
                          PreparaQuery( tqSource, 'select * from CAMPOREP where RE_CODIGO in ( ' + sValor + ' ) order by RE_CODIGO, CR_TIPO, CR_POSICIO' );
                          tqSource.Active := True;

                          {$ifdef MSSQL}
                          //Version 2008: Eliminar el campo LLAVE, para que no lo exporte.
                          oField:= tqSource.FieldDefs.Find( K_CAMPO_LLAVE_PORTAL );
                          if (oField <> NIL) then
                          begin
                               tqSource.FieldDefs.Delete(oField.Index);
                          end;
                          {$endif}

                          Bitacora.AgregaResumen( 'Exportando Campos de Reportes' );
                          with BatchMove do
                          begin
                               Execute;
                          end;
                          if DoEndCallBack then
                             Result := True;
                     end;
                end;
             except
                   on Error: Exception do
                   begin
                        SetErrorFound( True );
                        Bitacora.HandleException( 'Error Al Exportar Reportes', Error );
                   end;
             end;
          finally
                 CloseDBSourceShared;
          end;
     end
     else
         Bitacora.AgregaError( '*** No Se Escogi� Ning�n Reporte Para Exportar ***' );
     with Bitacora do
     begin
          sValor := GetTimeStamp + ': Exportaci�n De Reportes Terminada %s';
          if Result then
             AgregaResumen( Format( sValor, [ 'Con Exito' ] ) )
          else
              AgregaResumen( Format( sValor, [ 'CON ERRORES' ] ) );
     end;
     SetErrorFound( not Result );
end;

function TdmReportes.GetPatchSteps: Integer;
begin
     Result := 6;
end;

function TdmReportes.GetImportTableSteps: Integer;
begin
     Result := ttSource.RecordCount;
end;

function TdmReportes.GetImportSteps: Integer;
begin
     Result := ttSource.RecordCount + ttTarget.RecordCount;
end;

function TdmReportes.GetCuantosConceptos: Integer;
begin
     Result := tqRead.RecordCount;
end;

function TdmReportes.AbreArchivosReportes: Boolean;
begin
     OpenDBSourceShared;
     Result := AbreTabla( ttSource, 'REPORTE.DB' ) and AbreTabla( ttTarget, 'CAMPOREP.DB' );
end;

function TdmReportes.AbreArchivoConceptos( DataSource: TDataSource ): Boolean;
begin
     with DataSource do
     begin
          Dataset := nil;
          OpenDBSourceShared;
          with tqRead do
          begin
               PreparaQueryParadox( tqRead, 'select * from CONCEPTO.DB order by CO_NUMERO' );
               Active := True;
          end;
          Dataset := tqRead;
          Result := True;
     end;
end;

function TdmReportes.AbreArchivoParadox( const sFileName: String ): Boolean;
begin
     OpenDBSourceShared;
     Result := AbreTabla( ttSource, sFileName );
end;

function TdmReportes.AbreArchivoSPExtras: Boolean;
var
   sFileName: String;
begin
     sFileName := GetSPFileName( 'DB' );
     SourceSharedPath := ExtractFilePath( sFileName );
     OpenDBSourceShared;
     if FileExists( sFileName ) then
        PreparaTabla( ttSource, ExtractFileName( sFileName ) )
     else
     begin
          with ttSource do
          begin
               Active := False;
               Filtered := FALSE;
               Filter := '';
               IndexFieldNames := '';
               while ( FieldCount > 0 ) do
               begin
                    Fields[ FieldCount - 1 ].DataSet := nil;
               end;
               DatabaseName := dbSourceShared.DatabaseName;
               TableType := ttParadox;
               TableName := ExtractFileName( sFileName );
               with FieldDefs do
               begin
                    Clear;
                    Add( 'SE_TIPO', ftInteger, 0, True );
                    Add( 'SE_SEQ_NUM', ftInteger, 0, True );
                    Add( 'SE_NOMBRE', ftString, 10, False );
                    Add( 'SE_DESCRIP', ftString, 50, False );
                    Add( 'SE_DATA', ftBlob, 1, False );
               end;
               CreateTable;
          end;
     end;
     with ttSource do
     begin
          Exclusive := True;
          Active := True;
          Result := Active;
     end;
end;

procedure TdmReportes.ttSourceNewRecord(DataSet: TDataSet);
begin
     inherited;
     with Dataset do
     begin
          FieldByName( 'SE_TIPO' ).AsInteger := 0;
          FieldByName( 'SE_SEQ_NUM' ).AsInteger := 0;
     end;
end;

procedure TdmReportes.SE_TIPOGetText(Sender: TField; var Text: String; DisplayText: Boolean );
var
   iValue: Integer;
begin
     if DisplayText then
     begin
          iValue := Sender.AsInteger;
          if ( iValue = 0 ) then
             Text := 'Comparte'
          else
              Text := ZetaCommonLists.ObtieneElemento( lfTipoCompanyName, iValue - 1 );
     end;
end;

function TdmReportes.ImportarConceptos( Incluidos: TBookMarkList ): Boolean;
var
   lTodos, lExiste, lLoop: Boolean;
   sValor: String;
   i, iCount, iConcepto: Integer;

procedure SetUpdateScript( const sTabla, sLlave: String; Source: TDataset; Target: TQuery );
var
   i: Integer;
   sScript: String;
begin
     sScript := 'update ' + sTabla + ' set ';
     with Source do
     begin
          for i := 0 to ( FieldCount - 1 ) do
          begin
               with Fields[ i ] do
               begin
                    if ( FieldKind = fkData ) and ( FieldName <> sLlave ) then
                    begin
                         sScript := sScript + FieldName + '= :' + FieldName + ',';
                    end;
               end;
          end;
     end;
     sScript := CortaUltimo( sScript ) + ' where ( ' + sLlave + ' = :' + sLlave + ' )';
     with Target do
     begin
          Active := False;
          with SQL do
          begin
               Clear;
               Add( sScript );
          end;
     end;
end;

begin
     lLoop := True;
     Result := False;
     ClearErrorFound;
     with Bitacora do
     begin
          AgregaResumen( 'Importaci�n de Conceptos de N�mina' );
          AgregaResumen( 'Base de Datos Receptora: ' + TargetAlias );
          AgregaResumen( 'Archivo Donde Residen Los Conceptos a Importar: ' + SourceSharedPath );
          AgregaResumen( GetTimeStamp + ': Iniciando Importaci�n De Conceptos' );
     end;
     if AbreBDReceptora then
     begin
          PreparaQuery( tqTarget, '' );
          PreparaQuery( tqDelete, '' );
          PreparaQuery( tqSource, 'select CO_FORMULA from CONCEPTO where ( CO_NUMERO = :CONCEPTO )' );
          ZetaTressCFGTools.SetInsertScript( 'CONCEPTO', tqRead, tqTarget );
          SetUpdateScript( 'CONCEPTO', 'CO_NUMERO', tqRead, tqDelete );
          iConcepto := 0;
          with tqRead do
          begin
               DisableControls;
               EmpezarTransaccion;
               try
                  with FLista1 do
                  begin
                       Clear;
                       iCount := Incluidos.Count;
                       if ( iCount > 0 ) then
                       begin
                            lTodos := False;
                            for i := 0 to ( iCount - 1 ) do
                            begin
                                 GotoBookMark( TBookMark( Incluidos.Items[ i ] ) );
                                 Add( IntToStr( FieldByName( 'CO_NUMERO' ).AsInteger ) );
                            end;
                       end
                       else
                           lTodos := True;
                  end;
                  First;
                  while not Eof and lLoop do
                  begin
                       iConcepto := FieldByName( 'CO_NUMERO' ).AsInteger;
                       if lTodos or ( FLista1.IndexOf( IntToStr( iConcepto ) ) >= 0 ) then
                       begin
                            with tqSource do
                            begin
                                 Active := False;
                                 ParamAsInteger( 'CONCEPTO', iConcepto, tqSource );
                                 Active := True;
                                 if IsEmpty then
                                 begin
                                      lExiste := False;
                                      sValor := '';
                                 end
                                 else
                                 begin
                                      lExiste := True;
                                      sValor := FieldByName( 'CO_FORMULA' ).AsString;
                                 end;
                                 Active := False;
                            end;
                            if lExiste then
                            begin
                                 TransferDatasets( tqRead, tqDelete ); // Update CONCEPTO.DB -> CONCEPTO //
                                 tqDelete.ExecSQL;
                                 Bitacora.AgregaError( 'Concepto ' + IntToStr( iConcepto ) + ' Fu� Sobreescrito' + CR_LF + 'F�rmula Original: ' + sValor );
                            end
                            else
                            begin
                                 TransferDatasets( tqRead, tqTarget ); // Insert CONCEPTO.DB -> CONCEPTO //
                                 tqTarget.ExecSQL;
                                 Bitacora.AgregaError( 'Concepto ' + IntToStr( iConcepto ) + ' Fu� Agregado' );
                            end;
                       end;
                       lLoop := DoEndCallBack;
                       Next;
                  end;
                  TerminarTransaccion( lLoop );
                  Result := True;
               except
                     on Error: Exception do
                     begin
                          TerminarTransaccion( False );
                          SetErrorFound( True );
                          Bitacora.HandleException( 'Error Al Importar Concepto ' + IntToStr( iConcepto ), Error );
                     end;
               end;
               EnableControls;
          end;
     end;
     with Bitacora do
     begin
          sValor := GetTimeStamp + ': Importaci�n De Conceptos de N�mina Terminada %s';
          if Result then
             AgregaResumen( Format( sValor, [ 'Con Exito' ] ) )
          else
              AgregaResumen( Format( sValor, [ 'CON ERRORES' ] ) );
     end;
     SetErrorFound( not Result );
end;

function TdmReportes.ImportarReportes: Boolean;
var
   iNuevo, iViejo: Integer;
   lLoop: Boolean;
   sValor: String;

function GetNewCodigo( var iFolio: Integer ): Boolean;
begin
     Result := False;
     try
        PreparaQuery( tqTarget, 'select MAX( RE_CODIGO ) from REPORTE' );
        with tqTarget do
        begin
             Active := True;
             if IsEmpty then
                iFolio := 1
             else
                 iFolio := Fields[ 0 ].AsInteger + 1;
             Active := False;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                SetErrorFound( True );
                Bitacora.HandleException( 'Error Al Determinar C�digo De Reportes Importados', Error );
           end;
     end;
end;

function ImportaCampos( const iViejo, iNuevo: Integer; var lLoop: Boolean ): Boolean;
begin
     Result := False;
     with ttTarget do
     begin
          try
             Filtered := False;
             Filter := 'RE_CODIGO = ' + IntToStr( iViejo );
             Filtered := True;
             First;
             while not Eof and lLoop do
             begin
                  try
                     TransferDatasets( ttTarget, tqTarget ); // CAMPOREP.DBF -> CAMPOREP //
                     with tqTarget do
                     begin
                          ParamByName( 'RE_CODIGO' ).AsInteger := iNuevo;
                          ExecSQL;
                     end;
                  except
                        on Error: Exception do
                        begin
                             SetErrorFound( True );
                             Bitacora.HandleException( 'Error Al Transferir Campos De Reporte ' + IntToStr( iViejo ), Error );
                        end;
                  end;
                  lLoop := DoEndCallBack;
                  Next;
             end;
             Result := True;
          except
                on Error: Exception do
                begin
                     SetErrorFound( True );
                     Bitacora.HandleException( 'Error Al Filtrar Campos De Reporte ' + IntToStr( iViejo ), Error );
                end;
          end;
     end;
end;

begin
     iViejo := 0;
     iNuevo := 0;
     lLoop := True;
     Result := False;
     ClearErrorFound;
     with Bitacora do
     begin
          AgregaResumen( 'Importaci�n de Reportes' );
          AgregaResumen( 'Base de Datos Receptora: ' + TargetAlias );
          AgregaResumen( 'Directorio Donde Residen Los Reportes a Importar: ' + SourceSharedPath );
          AgregaResumen( GetTimeStamp + ': Iniciando Importaci�n De Reportes' );
     end;
     if AbreBDReceptora and GetNewCodigo( iNuevo ) then
     begin
          PreparaQuery( tqSource, '' );
          PreparaQuery( tqTarget, '' );
          ZetaTressCFGTools.SetInsertScript( 'REPORTE', ttSource, tqSource );
          ZetaTressCFGTools.SetInsertScript( 'CAMPOREP', ttTarget, tqTarget );
          with ttSource do
          begin
               while not Eof and lLoop do
               begin
                    EmpezarTransaccion;
                    Result := True;
                    try
                       TransferDatasets( ttSource, tqSource ); // REPORTE.DBF -> REPORTE //
                       with tqSource do
                       begin
                            with ParamByName( 'RE_CODIGO' ) do
                            begin
                                 iViejo := AsInteger;
                                 AsInteger := iNuevo;
                            end;
                            with ParamByName( 'RE_NOMBRE' ) do
                            begin
                                 AsString := Copy( 'IMP: ' + AsString, 1, 30 );
                            end;
                            ExecSQL;
                       end;
                       lLoop := DoEndCallBack;
                       if lLoop then
                       begin
                            ImportaCampos( iViejo, iNuevo, lLoop );
                            if lLoop then
                               Bitacora.AgregaResumen( 'Reporte ' + IntToStr( iViejo ) + ' Importado Como ' + IntToStr( iNuevo ) );
                       end;
                       TerminarTransaccion( lLoop );
                    except
                          on Error: Exception do
                          begin
                               TerminarTransaccion( False );
                               SetErrorFound( True );
                               Bitacora.HandleException( 'Error En Reporte ' + IntToStr( iViejo ), Error );
                               Result := False;
                          end;
                    end;
                    iNuevo := iNuevo + 1;
                    Next;
               end;
          end;
     end;
     with Bitacora do
     begin
          sValor := GetTimeStamp + ': Importaci�n De Reportes Terminada %s';
          if Result then
             AgregaResumen( Format( sValor, [ 'Con Exito' ] ) )
          else
              AgregaResumen( Format( sValor, [ 'CON ERRORES' ] ) );
     end;
     SetErrorFound( not Result );
end;


procedure TdmReportes.InitTableFieldLists( const sTableName: String{$ifdef INTERBASE};const lExcluirBlobs: Boolean{$endif} );
begin
     with dbTarget do
     begin
          BDEFunctions.BDEOpenFieldList( Handle, GetRealTableName( sTableName ), FLista1{$ifdef INTERBASE},lExcluirBlobs{$endif} );
          BDEFunctions.BDEBuildPrimaryKeyFieldList( Handle, GetRealTableName( sTableName ), FLista1, FLista2 );
     end;
end;

procedure TdmReportes.CreateInsertScript( const sTableName: String; Target: TQuery; Campos: TStrings; const BaseDatos: TDataBase );
var
   i: Integer;
   sCampos, sValues: String;
begin
     sCampos := '';
     sValues := '';
     with Campos do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               sCampos := sCampos + Strings[ i ] + ',';
               sValues := sValues + ':' + Strings[ i ] + ',';
          end;
     end;
     PreparaQuery( Target, Format( 'insert into %s ( %s ) values ( %s )', [ sTableName, ZetaCommonTools.CortaUltimo( sCampos ), ZetaCommonTools.CortaUltimo( sValues ) ] ),  BaseDatos );
end;

procedure TdmReportes.CreateInsertScript( const sTableName: String; Target: TQuery; const BaseDatos: TDataBase );
begin
     CreateInsertScript( sTableName, Target, FLista1, BaseDatos );
end;

function TdmReportes.ImportarImagenes( const sTipo, sFileSpec: String ; lConservarImagen : Boolean): Boolean;
var
   sValor: String;
   lLoop: Boolean;
   Archivo: TSearchRec;
   sBuffer: String;
   PMultiImage: TPMultiImage;

procedure ImportaImagen( const sFileName: String );
var
   iEmpleado: Integer;
   lExiste, lHayImagen: Boolean;
begin
     try
        iEmpleado := StrToIntDef( ChangeFileExt( ExtractFileName( sFileName ), '' ), 0 );
        if ( iEmpleado > 0 ) then
        begin
             lExiste := False;
             lHayImagen := False;
             with tqDelete do
             begin
                  Active := False;
                  ParamByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
                  Active := True;
                  if not IsEmpty then
                  begin
                       lExiste := True;
                       lHayImagen := ( FieldByName( 'IMAGENES' ).AsInteger > 0 );
                  end;
                  Active := False;
             end;
             if lExiste then
             begin
                  if lHayImagen then
                     Bitacora.AgregaError( Format( 'Error Al Importar Im�gen %s: Empleado %d Ya Tiene Una %s', [ sFileName, iEmpleado, sTipo ] ) )
                  else
                  begin
                       with PMultiImage do
                       begin
                            ImageName := sFileName;
                            if ( Picture.Bitmap.Width > 300 ) and ( not lConservarImagen )  then
                               FToolsFoto.ResizePMultiImageBestFit( PMultiImage, 300, 300 );

                            SaveAsJpg( sBuffer );
                       end;
                       if FileExists( sBuffer ) then
                       begin
                            with tqTarget do
                            begin
                                 ParamByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
                                 ParamByName( 'IM_BLOB' ).LoadFromFile( sBuffer, ftBlob );
                                 ExecSQL;
                            end;
                            DeleteFile( sBuffer );
                       end
                       else
                           Bitacora.AgregaError( Format( 'Error Al Importar Im�gen %s: No se pudo leer el archivo', [ sFileName ] ) );
                  end;
             end
             else
                 Bitacora.AgregaError( Format( 'Error Al Importar Im�gen %s: Empleado %d No Existe', [ sFileName, iEmpleado ] ) );
        end;
     except
           on Error: Exception do
           begin
                SetErrorFound( True );
                Bitacora.HandleException( Format( 'Error Al Importar Im�gen %s', [ sFileName ] ), Error );
           end;
     end;
end;

begin
     Result := False;
     lLoop := True;
     ClearErrorFound;
     with Bitacora do
     begin
          AgregaResumen( 'Importaci�n de Im�genes' );
          AgregaResumen( 'Base de Datos Receptora: ' + TargetAlias );
          AgregaResumen( 'Directorio De Im�genes: ' + sFileSpec );
          AgregaResumen( 'Tipo de Imagen: ' + sTipo );
          AgregaResumen( GetTimeStamp + ': Iniciando Importaci�n De Im�genes' );
     end;
     sBuffer := ZetaTressCFGTools.SetFileNameDefaultPath( 'IMAGEBUF.JPG' );
     PMultiImage := TPMultiImage.Create( Owner );
     try
        try
           with PMultiImage do
           begin
                Parent := TForm( Owner );
                Name := 'PMultiImage';
                Visible := False;
                JPegSaveQuality := 75;
           end;
           PreparaQuery( tqTarget, Format( 'insert into IMAGEN( CB_CODIGO, IM_TIPO, IM_BLOB ) values ( :CB_CODIGO, ''%s'', :IM_BLOB )', [ sTipo ] ) );
           PreparaQuery( tqDelete, Format( 'select C.CB_CODIGO, ( select COUNT(*) from IMAGEN I where ( I.CB_CODIGO = C.CB_CODIGO ) and ( I.IM_TIPO = ''%s'' ) ) IMAGENES from COLABORA C where ( C.CB_CODIGO = :CB_CODIGO )', [ sTipo ] ) );
           try
              if ( FindFirst( sFileSpec, faAnyFile, Archivo ) = 0 ) then
              begin
                   ImportaImagen( Archivo.Name );
                   while ( FindNext( Archivo ) = 0 ) do
                   begin
                        ImportaImagen( Archivo.Name );
                   end;
                   FindClose( Archivo );
              end;
              Result := True;
           except
                 on Error: Exception do
                 begin
                      SetErrorFound( True );
                      Bitacora.HandleException( 'Error Al Importar Im�genes', Error );
                 end;
           end;
           DespreparaQuery( tqTarget );
           DespreparaQuery( tqDelete );
        except
              on Error: Exception do
              begin
                   SetErrorFound( True );
                   Bitacora.HandleException( 'Error Al Preparar Importaci�n de Im�genes', Error );
              end;
        end;
     finally
            FreeAndNil( PMultiImage );
     end;
     with Bitacora do
     begin
          sValor := GetTimeStamp + ': Importaci�n De Im�genes Tipo ' + sTipo + ' Terminada %s';
          if lLoop then
          begin
               if ErrorFound then
                  AgregaResumen( Format( sValor, [ 'CON ERRORES' ] ) )
               else
                   AgregaResumen( Format( sValor, [ 'Con Exito' ] ) );
          end
          else
              AgregaResumen( Format( sValor, [ 'POR INTERRUPCION DEL USUARIO' ] ) );
     end;
end;

function TdmReportes.ImportarASCIIInit( const sArchivo: String ): Integer;
var
   AsciiData: TAsciiServer;
begin
     Result := 0;
     AsciiData := TAsciiServer.Create;
     try
        with AsciiData do
        begin
             if Open( sArchivo ) then
             begin
                  Result := RecordCount;
                  Close;
             end;
        end;
     finally
            FreeAndNil( AsciiData );
     end;
end;

function TdmReportes.ImportarASCII( const lValidate: Boolean; const sTableName, sArchivo: String; Campos: TStrings; const eFormato: eFormatoFecha ): Boolean;
var
   i: Integer;
   lLoop: Boolean;
   sValor, sDateFormat: String;
   cSeparator: Char;
   AsciiData: TAsciiServer;

procedure PrepareParameter( const iPtr: Integer; const sValue: String );
begin
     with tqTarget do
     begin
          with Params[ iPtr ] do
          begin
               case tqDelete.Fields[ iPtr ].DataType of
                    ftString: AsString := sValue;
                    ftSmallInt, ftInteger, ftWord: AsInteger := StrToInt( sValue );
                    ftFloat, ftCurrency: AsFloat := StrToFloat( sValue );
                    ftDate: AsDate := StrToDate( sValue );
                    ftTime: AsTime := StrToTime( sValue );
                    ftDateTime: AsDateTime := StrToDateTime( sValue );
               end;
          end;
     end;
end;
begin
     lLoop := True;
     ClearErrorFound;
     with Bitacora do
     begin
          AgregaResumen( 'Importaci�n de Tabla Por Archivo ASCII CSV' );
          AgregaResumen( 'Base de Datos Receptora: ' + TargetAlias );
          AgregaResumen( 'Archivo Importado: ' + sArchivo );
          AgregaResumen( 'Tabla Importada: ' + sTableName );
          if lValidate then
             AgregaResumen( 'Solo Validar Archivo' );
          AgregaResumen( GetTimeStamp + ': Iniciando Importaci�n De Tabla' );
     end;
     cSeparator := DateSeparator;
     DateSeparator := '/';
     sDateFormat := ShortDateFormat;
     case eFormato of
          ffDDMMYY: ShortDateFormat := 'dd/mm/yy';
          ffDDMMYYYY: ShortDateFormat := 'dd/mm/yyyy';
          ffMMDDYY: ShortDateFormat := 'mm/dd/yy';
          ffMMDDYYYY: ShortDateFormat := 'mm/dd/yyyyy';
          ffYYMMDD: ShortDateFormat := 'yymmdd';
          ffYYYYMMDD: ShortDateFormat := 'yyyymmdd';
     end;
     AsciiData := TAsciiServer.Create;
     try
        try
           PreparaQuery( tqDelete, Format( 'select %s from %s', [ Campos.CommaText, sTableName ] ) );
           with tqDelete do
           begin
                with FieldDefs do
                begin
                     Update;
                     for i := 0 to ( Count - 1 ) do
                         FieldDefs[ i ].CreateField( tqDelete );
                end;
           end;
           CreateInsertScript( sTableName, tqTarget, Campos );
           with AsciiData do
           begin
                if Open( sArchivo ) then
                begin
                     repeat
                           sValor := Data;
                           if ZetaCommonTools.StrLleno( sValor ) then
                           begin
                                EmpezarTransaccion;
                                try
                                   with tqTarget do
                                   begin
                                        with Campos do
                                        begin
                                             CommaText := sValor;
                                             if ( Count = ParamCount ) then
                                             begin
                                                  for i := 0 to ( Count - 1 ) do
                                                  begin
                                                       PrepareParameter( i, Strings[ i ] );
                                                  end;
                                                  if not lValidate then
                                                     ExecSQL;
                                             end
                                             else
                                                 Bitacora.AgregaError( Format( 'Rengl�n Incompleto: %s', [ sValor ] ) );
                                        end;
                                   end;
                                   TerminarTransaccion( True );
                                except
                                      on Error: Exception do
                                      begin
                                           TerminarTransaccion( False );
                                           Bitacora.HandleException( Format( 'Error Al Importar Tabla %s ( Datos: %s )', [ sTableName, sValor ] ), Error );
                                      end;
                                end;
                           end;
                           lLoop := DoEndCallBack;
                     until not Read;
                     Close;
                end;
           end;
           Result := lLoop;
        except
              on Error: Exception do
              begin
                   Bitacora.HandleException( 'Error Al Importar Tabla ' + sTableName, Error );
                   Result := False;
              end;
        end;
     finally
            FreeAndNil( AsciiData );
            DateSeparator := cSeparator;
            ShortDateFormat := sDateFormat;
            DespreparaQuery( tqDelete );
            DespreparaQuery( tqTarget );
     end;
     with Bitacora do
     begin
          sValor := GetTimeStamp + ': Importaci�n De Tabla ' + sTableName + ' Terminada %s';
          if lLoop then
          begin
               if ErrorFound then
                  AgregaResumen( Format( sValor, [ 'CON ERRORES' ] ) )
               else
                   AgregaResumen( Format( sValor, [ 'Con Exito' ] ) );
          end
          else
              AgregaResumen( Format( sValor, [ 'POR INTERRUPCION DEL USUARIO' ] ) );
          if lValidate then
          begin
               if ErrorFound then
                  AgregaResumen( 'El Archivo A Importar Tiene Problemas' )
               else
                   AgregaResumen( 'OK: El Archivo A Importar Es Correcto' );
          end;
     end;
end;

function TdmReportes.ImportarTabla( const sTableName: String; Mode: TBatchMode ): Boolean;
var
   lLoop: Boolean;
   sValor: String;

function ConstruyeFiltro: String;
var
   i: Integer;
begin
     Result := '';
     with FLista2 do
     begin
          for i := 0 to ( Count - 1 ) do
              Result := ConcatFiltros( Result, '( ' + Strings[ i ] + ' = :' + Strings[ i ] + ')' );
     end;
end;

procedure AsignaLlaves( Source: TDataSet; Target: TQuery );
var
   i: Integer;
   sCampo: String;
begin
     with FLista2 do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               sCampo := Strings[ i ];
               Target.ParamByName( sCampo ).AssignField( Source.FieldByName( sCampo ) );
          end;
     end;
end;

function SeekRecord( Source: TDataSet; Target: TQuery ): Boolean;
begin
     AsignaLlaves( Source, Target );
     with Target do
     begin
          Active := True;
          if IsEmpty then
             Result := False
          else
              Result := ( Fields[ 0 ].AsInteger > 0 );
          Active := False;
     end;
end;

procedure SetUpdateScript( Target: TQuery );
var
   i: Integer;
   sCampo, sCampos: String;
begin
     sCampos := '';
     with FLista1 do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               sCampo := Strings[ i ];
               if ( FLista2.IndexOf( sCampo ) < 0 ) then { No Se Encuentra En Los Campos De La Llave Primaria }
                  sCampos := sCampos + sCampo + '= :' + sCampo + ',';
          end;
     end;
     with Target do
     begin
          Active := False;
          with SQL do
          begin
               Clear;
               Add( Format( 'update %s set %s where %s', [ sTableName, CortaUltimo( sCampos ), ConstruyeFiltro ] ) );
          end;
     end;
end;

procedure SetSeekScript( Target: TQuery );
begin
     with Target do
     begin
          Active := False;
          with SQL do
          begin
               Clear;
               Add( Format( 'select COUNT(*) from %s where %s', [ sTableName, ConstruyeFiltro ] ) );
          end;
     end;
end;

begin
     lLoop := True;
     ClearErrorFound;
     with Bitacora do
     begin
          AgregaResumen( 'Importaci�n de Tabla' );
          AgregaResumen( 'Base de Datos Receptora: ' + TargetAlias );
          AgregaResumen( 'Directorio Donde Reside La Tabla Importada: ' + SourceSharedPath );
          AgregaResumen( 'Tabla Importada: ' + sTableName );
          AgregaResumen( GetTimeStamp + ': Iniciando Importaci�n De Tabla' );
     end;
     InitTableFieldLists( sTableName{$ifdef INTERBASE}, TRUE {$endif});
     PreparaQuery( tqSource, '' );
     PreparaQuery( tqTarget, '' );
     PreparaQuery( tqDelete, '' );
     CreateInsertScript( sTableName, tqSource );
     SetUpdateScript( tqTarget );
     SetSeekScript( tqDelete );
     EmpezarTransaccion;
     try
        with ttSource do
        begin
             while not Eof and lLoop do
             begin
                  try
                     case Mode of
                          batAppend:
                          begin
                               if not SeekRecord( ttSource, tqDelete ) then
                               begin
                                    TransferDatasets( ttSource, tqSource );
                                    tqSource.ExecSQL;
                               end;
                          end;
                          batUpdate:
                          begin
                               if SeekRecord( ttSource, tqDelete ) then
                               begin
                                    TransferDatasets( ttSource, tqTarget );
                                    AsignaLlaves( ttSource, tqTarget );
                                    tqTarget.ExecSQL;
                               end
                               else
                               begin
                                    TransferDatasets( ttSource, tqSource );
                                    tqSource.ExecSQL;
                               end;
                          end;
                     end;
                  except
                        on Error: Exception do
                        begin
                             SetErrorFound( True );
                             Bitacora.HandleException( 'Error Al Importar Tabla ' + sTableName, Error );
                        end;
                  end;
                  lLoop := DoEndCallBack;
                  Next;
             end;
        end;
        TerminarTransaccion( lLoop );
        Result := lLoop;
     except
           on Error: Exception do
           begin
                TerminarTransaccion( False );
                Bitacora.HandleException( 'Error Al Importar Tabla ' + sTableName, Error );
                Result := False;
           end;
     end;
     SetErrorFound( not Result );
     with Bitacora do
     begin
          sValor := GetTimeStamp + ': Importaci�n De Tabla ' + sTableName + ' Terminada %s';
          if lLoop then
          begin
               if ErrorFound then
                  AgregaResumen( Format( sValor, [ 'CON ERRORES' ] ) )
               else
                   AgregaResumen( Format( sValor, [ 'Con Exito' ] ) );
          end
          else
              AgregaResumen( Format( sValor, [ 'POR INTERRUPCION DEL USUARIO' ] ) );
     end;
end;

function TdmReportes.ExportarTablas: Boolean;
var
   i: Integer;
   lLoop: Boolean;
   sValor: String;

procedure ExportaTabla( const sTableName: String );
var
   i: Integer;
   sCampos: String;
{$ifdef MSSQL}
   lHayGUID: Boolean;
{$endif}
begin
     try
        PreparaQuery( tqSource, 'select * from ' + sTableName );
        {$ifdef MSSQL}
        lHayGUID := False;
        sCampos := '';
        with tqSource do
        begin
             Active := True;
             {
             Es necesario barrer FieldDefs[] debido a que la propiedad Fields[]
             no contiene las columnas tipo ftGUID
             }
             for i := 0 to ( FieldDefs.Count - 1 ) do
             begin
                  //Version 2008 2.12, se tiene que ignorar el campo LLAVE
                  if NoEsCampoLLavePortal( FieldDefs[ i ].Name ) then
                  begin
                       {
                       Las columnas tipo GUID no son reportadas por FieldDefs[]
                       como ftGUID sino como ftUnknown
                       }
                       if ( FieldDefs[ i ].DataType = ftUnknown ) then
                       begin
                            sCampos := sCampos + Format( 'CAST( %0:s as VARCHAR( 36 ) ) as %0:s', [ FieldDefs[ i ].Name ] );
                            lHayGUID := True;
                       end
                       else
                           sCampos := sCampos + FieldDefs[ i ].Name;
                       sCampos := sCampos + ',';
                  end
                  else
                      lHayGUID := True;
             end;
             {
             Con esto se asegura que �nicamente se cambia el query con el
             que se lee la tabla a exportar cuando hay campos GUID's. De
             otra manera se utiliza el 'select * from <tabla>'
             }
             if lHayGUID then
                sCampos := ZetaCommonTools.CortaUltimo( sCampos )
             else
                 sCampos := '*';
             Active := False;
        end;
        PreparaQuery( tqSource, Format( 'select %s from ' + sTableName, [ sCampos ] ) );
        {$endif}
        {$ifdef INTERBASE}
        {En interbase se excluyen los blobs ya que no se pueden importar}
        with tqSource do
        begin
             Active := True;
             for i := 0 to ( FieldDefs.Count - 1 ) do
             begin
                  if not ( FieldDefs[ i ].DataType in [ftMemo,ftBlob] ) then
                  begin
                       sCampos := sCampos + FieldDefs[ i ].Name + ',';
                  end;
             end;
             Active := False;
             sCampos := ZetaCommonTools.CortaUltimo( sCampos );
             PreparaQuery( tqSource, Format( 'select %s from ' + sTableName, [ sCampos ] ) );
        end;
        {$endif}
        tqSource.Active := True;
        try
           PreparaTabla( ttTarget, sTableName + '.DB' );
           with BatchMove do
           begin
                Source := tqSource;
                Destination := ttTarget;
                Mode := batCopy;
                Execute;
           end;
        except
              on Error: Exception do
              begin
                   with Error do
                   begin
                        Message := 'Error Al Crear Tabla Default ' + sTableName + '.DB' + CR_LF + Message;
                        raise
                   end;
              end;
        end;
     except
           on Error: Exception do
           begin
                with Error do
                begin
                     Message := 'Error Al Abrir Tabla Fuente ' + sTableName + '.DB' + CR_LF + Message;
                     raise
                end;
           end;
     end;
end;

begin
     lLoop := True;
     ClearErrorFound;
     with Bitacora do
     begin
          AgregaResumen( 'Exportaci�n de Tablas' );
          AgregaResumen( 'Base de Datos Fuente: ' + TargetAlias );
          AgregaResumen( 'Directorio Hacia Donde Exportar: ' + SourceSharedPath );
          AgregaResumen( GetTimeStamp + ': Iniciando Exportaci�n De Tablas' );
     end;
     try
        OpenDBSourceShared;
        try
           with FLista1 do
           begin
                try
                   for i := 0 to ( Count - 1 ) do
                   begin
                        sValor := Strings[ i ];
                        ExportaTabla( sValor );
                        Bitacora.AgregaResumen( 'Tabla ' + sValor + ' Fu� Exportada' );
                        lLoop := DoEndCallBack;
                        if not lLoop then
                           Break;
                   end;
                   Result := lLoop;
                except
                      on Error: Exception do
                      begin
                           Bitacora.HandleException( 'Error Al Exportar Tablas', Error );
                           Result := False;
                      end;
                end;
           end;
        finally
               CloseDBSourceShared;
        end;
     except
           on Error: Exception do
           begin
                Bitacora.HandleException( 'Error Al Accesar Directorio Destino', Error );
                Result := False;
           end;
     end;
     with Bitacora do
     begin
          sValor := GetTimeStamp + ': Exportaci�n De Tablas Terminada %s';
          if lLoop then
          begin
               if Result then
                  AgregaResumen( Format( sValor, [ 'Con Exito' ] ) )
               else
                   AgregaResumen( Format( sValor, [ 'CON ERRORES' ] ) );
          end
          else
              AgregaResumen( Format( sValor, [ 'POR INTERRUPCION DEL USUARIO' ] ) );
     end;
     SetErrorFound( not Result );
end;

{ ************* Patch de Datos / Comparte ************ }

function TdmReportes.GetScriptFieldName;
begin
     {$ifdef Interbase}
     case DriverType of
          dtInterbase: Result := 'SP_INTRBASE';
          dtOracle: Result := 'SP_ORACLE';
     end;
     {$endif}
     {$ifdef MsSql}
     Result := 'SP_ORACLE';
     {$endif}
end;

function TdmReportes.GetScriptDropTrigger;
begin
     {$ifdef MsSql}
          Result := 'DROP TRIGGER %s';
     {$endif}

     {$ifdef Interbase}
     case DriverType of
          dtInterbase: Result := 'delete from RDB$TRIGGERS where ( RDB$TRIGGER_NAME = ' + EntreComillas( '%s' ) + ' )';
          dtOracle: Result := 'drop trigger %s';
     end;
     {$endif}
end;

function TdmReportes.GetScriptDropProcedure;
begin
     {$ifdef Interbase}
     case DriverType of
          dtInterbase: Result := 'drop procedure %s';
          dtOracle: Result := 'drop procedure %s';
     end;
     {$endif}
     {$ifdef MsSql}
     Result := 'drop procedure %s';
     {$endif}
end;

procedure TdmReportes.DeterminaDependencias( sValor: String );
var
   FDependencias: TStrings;
   sDependencia: String;
   i: Integer;
begin
     FDependencias := TStringList.Create;
     try
        try
           with TqTarget do
           begin
                Active := False;
                ParamByName( 'SP_NAME' ).AsString := sValor;
                Active := True;
                while not Eof do
                begin
                     sDependencia := FieldByName( 'SP_DEPEND' ).AsString;
                     if ( sDependencia <> sValor ) then                      // Protecci�n con M�todos Recursivos a un solo nivel ( SP1 -> SP1 ) no resuelve casos como ( SP1 -> SP2 -> SP1 )
                        FDependencias.Add( sDependencia );
                     Next;
                end;
                Active := False;
           end;
        except
              on Error: Exception do
              begin
                   FDependencias.Clear;
                   SetErrorFound( True );
                   Bitacora.HandleException( 'ERROR Al Determinar Dependencias', Error );
              end;
        end;
        with FDependencias do
        begin
             for i := 0 to ( Count - 1 ) do
             begin
                  DeterminaDependencias( Strings[ i ] );
             end;
        end;
        with FLista1 do
        begin
             if ( IndexOf( sValor ) < 0 ) then  { No Existe en la Lista }
                Append( sValor );               { Agrega al Final }
        end;
     finally
            FDependencias.Free;
     end;
end;

function TdmReportes.FormatScript( const sScript: String ): String;
begin
     Result := CR_LF +
               StringOfChar( '=', 6 ) +
               ' SCRIPT ' +
               StringOfChar( '=', 6 ) +
               CR_LF +
               sScript +
               CR_LF +
               StringOfChar( '=', 20 )
end;

{$ifdef MSSQL}
function TdmReportes.ListarProcesos( BaseDatos: TDatabase; const sScript, sNombre: String ): Boolean;
var
   lOk: Boolean;
begin
     lOk := True;
     Bitacora.AgregaResumen( Format( 'Ennumerando %s', [ sNombre ] ) );
     DoStartCallBack( sNombre );
     try
        AttachQuery( BaseDatos, tqTarget, sScript );
        with FLista1 do
        begin
             BeginUpdate;
             try
                Clear;
                with tqTarget do
                begin
                     Open;
                     while not Eof do
                     begin
                          Add( Fields[ 0 ].AsString );
                          Next;
                     end;
                     Active := False;
                end;
             finally
                    EndUpdate;
             end;
        end;
     except
           lOk := False;
     end;
     Result := lOk and DoEndCallBack;
end;

function TdmReportes.ListFunciones( BaseDatos: TDatabase ): Boolean;
begin
     { Estos querys requieren el cast, por que si no regresan puros nulos }
     Result := ListarProcesos( BaseDatos, 'select CAST(  SYSOBJECTS.NAME as VARCHAR( 50 ) ) from SYSOBJECTS where SYSOBJECTS.TYPE in ( ''FN'', ''IF'', ''TF'' )', 'Funciones' );
end;

function TdmReportes.ListStoredProcedures( BaseDatos: TDatabase ): Boolean;
begin
     { Estos querys requieren el cast, por que si no regresan puros nulos }
     Result := ListarProcesos( BaseDatos, 'select CAST(  SYSOBJECTS.NAME as VARCHAR( 50 ) ) from SYSOBJECTS where ( SYSOBJECTS.TYPE = ''P'' ) and ( SYSOBJECTS.CATEGORY = 0 )', 'SP' );
end;
{$endif}

{$ifdef INTERBASE}
function TdmReportes.ListStoredProcedures( BaseDatos: TDatabase ): Boolean;
var
   lOk: Boolean;
   i: Integer;
   sProcedure: String;
   FProcList: TStrings;

   function EsExcepcionDependiente( const sStoredProcedure: String ): Boolean;
   var
      i: Byte;
   begin
        Result:= FALSE;
        for i:= 1 to K_QTY_SP_DEPENDIENTES do
            if ( aSPDependientes[i] = sStoredProcedure ) then
            begin
                 Result:= TRUE;
                 Break;
            end;

        for i:=0  to K_QTY_DB_KEYS-1 do
         if ( DB_KEYS[i] = sStoredProcedure ) then
         begin
              Result:= TRUE;
              Break;
         end;

   end;

begin
     lOk := True;
     Bitacora.AgregaResumen( 'Determinando Dependencias' );
     DoStartCallBack( 'Dependencias' );


     AttachQuery( BaseDatos, TqTarget, 'select RDB$DEPENDED_ON_NAME SP_DEPEND from RDB$DEPENDENCIES where ' +
                                       '( RDB$DEPENDENT_NAME = :SP_NAME ) and ' +
                                       '( RDB$DEPENDENT_TYPE = 5 ) and ' +
                                       '( RDB$DEPENDED_ON_TYPE = 5 ) and ' +
                                       '( RDB$FIELD_NAME is NULL )' );
     with FLista1 do
     begin
          BeginUpdate;
          Clear;
     end;
     FProcList := TStringList.Create;
     BDEFunctions.BDEGetSPList( BaseDatos.Handle, FProcList );
     with FProcList do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               sProcedure := Strings[ i ];
               if not EsExcepcionDependiente(sProcedure)  then
               begin
                    DeterminaDependencias( sProcedure );
                    ItemName := sProcedure;
                    DoCallBack( Self, lOk );
                    if not lOk then
                       Break;
               end;
          end;
     end;
     with FLista1 do
     begin
          EndUpdate;
     end;
     FProcList.Free;
     Result := lOk and DoEndCallBack;
end;
{$endif}

{$ifdef MSSQL}
function TdmReportes.ExcluirProdecure (const sItemName:string): Boolean;
var
	i: Byte;
begin
     Result:= FALSE;
     for i:= 1 to K_QTY_SP_DEPENDIENTES do
         if ( aSPDependientes[i] = sItemName ) then
         begin
              Result:= TRUE;
              Break;
         end;

     for i:=0  to K_QTY_DB_KEYS-1 do
         if ( DB_KEYS[i] = sItemName ) then
         begin
              Result:= TRUE;
              Break;
         end;
end;
{$endif}

function TdmReportes.BorrarFunciones( const lEnabled: Boolean; BaseDatos: TDatabase ): Boolean;
{$ifdef MSSQL}
var
   lOk: Boolean;
   i: Integer;
   sScript: String;
{$endif}
begin
     if lEnabled then
     begin
          {$ifdef MSSQL}
          try
             BaseDatos.Connected := True;
             lOk := ListFunciones( BaseDatos );
             if lOk then
             begin
                  Bitacora.AgregaResumen( 'Borrando Funciones' );
                  DoStartCallBack( 'Borrar Funciones' );
                  with FLista1 do
                  begin
                       for i := ( Count - 1 ) downto 0 do
                       begin
                            ItemName := Strings[ i ];
                            if Not ExcluirProdecure( ItemName ) then
                            begin
                                 sScript := Format( 'drop function %s', [ ItemName ] );
                                 with BaseDatos do
                                 begin
                                      StartTransaction;
                                      try
                                         EjecutaDDL( BaseDatos, tqDelete, sScript );
                                         Commit;
                                      except
                                            on Error: Exception do
                                            begin
                                                 RollBack;
                                                 SetErrorFound( True );
                                                 Bitacora.HandleException( 'ERROR Al Borrar Stored Procedure ' + ItemName + FormatScript( sScript ), Error );
                                            end;
                                      end;
                                 end;
                                 DoCallBack( Self, lOk );
                            end;
                       end;
                  end;
                  lOk := lOk and DoEndCallBack;
             end;
          except
                on Error: Exception do
                begin
                     SetErrorFound( True );
                     Bitacora.HandleException( 'ERROR al Borrar Funciones', Error );
                end;
          end;
          BaseDatos.Connected := False;
          Result := lOk;
          {$endif}
          {$ifdef INTERBASE}
          Result := True;
          {$endif}
     end
     else
         Result := True;
end;

function TdmReportes.BorrarStoredProcedures( const lEnabled: Boolean; BaseDatos: TDatabase ): Boolean;
var
   lOk: Boolean;
   i: Integer;
   sScript: String;
begin
     if lEnabled then
     begin
          try
             BaseDatos.Connected := True;
             lOk := ListStoredProcedures( BaseDatos );
             if lOk then
             begin
                  Bitacora.AgregaResumen( 'Borrando Stored Procedures' );
                  DoStartCallBack( 'Borrar SP' );
                  with FLista1 do
                  begin
                       for i := ( Count - 1 ) downto 0 do
                       begin
                            ItemName := Strings[ i ];
                            sScript := Format( GetScriptDropProcedure, [ ItemName ] );
                            with BaseDatos do
                            begin
                                 StartTransaction;
                                 try
                                    EjecutaDDL( BaseDatos, tqDelete, sScript );
                                    Commit;
                                 except
                                       on Error: Exception do
                                       begin
                                            RollBack;
                                            SetErrorFound( True );
                                            Bitacora.HandleException( 'ERROR Al Borrar Stored Procedure ' + ItemName + FormatScript( sScript ), Error );
                                       end;
                                 end;
                            end;
                            DoCallBack( Self, lOk );
                       end;
                  end;
                  lOk := lOk and DoEndCallBack;
             end;
          except
                on Error: Exception do
                begin
                     SetErrorFound( True );
                     Bitacora.HandleException( 'ERROR al Borrar Stored Procedures', Error );
                end;
          end;
          BaseDatos.Connected := False;
          Result := lOk;
     end
     else
         Result := True;
end;

function TdmReportes.BorrarTriggers( const lEnabled: Boolean; BaseDatos: TDatabase ): Boolean;
const
     {$ifdef MSSQL}
     K_LIST_TRIGGERS = 'select CAST( SYSOBJECTS.NAME as VARCHAR( 50 ) ) SP_NAME from SYSOBJECTS where ( SYSOBJECTS.TYPE = ''TR'' )';
     {$endif}
     {$ifdef INTERBASE}
     K_LIST_TRIGGERS = 'select RDB$TRIGGER_NAME SP_NAME from RDB$TRIGGERS where ( RDB$SYSTEM_FLAG is NULL ) or ( RDB$SYSTEM_FLAG = 0 )';
     {$endif}

var
   lCached, lOk: Boolean;
   sScript: String;
begin
     lOk := True;
     if lEnabled then
     begin
          try
             BaseDatos.Connected := True;
             Bitacora.AgregaResumen( 'Borrando Triggers' );
             DoStartCallBack( 'Borrar Triggers' );
             AttachQuery( BaseDatos, tqTarget, K_LIST_TRIGGERS );
             with tqTarget do
             begin
                  lCached := CachedUpdates;
                  CachedUpdates := True;
                  Active := True;
                  FetchAll; { Para que se traiga todo ANTES de empezar a borrar uno por uno }
                  First;
                  while not Eof and lOk do
                  begin
                       ItemName := FieldByName( 'SP_NAME' ).AsString;
                       sScript := Format( GetScriptDropTrigger, [ ItemName ] );
                       with BaseDatos do
                       begin
                            StartTransaction;
                            try
                               EjecutaDDL( BaseDatos, tqDelete, sScript );
                               Commit;
                            except
                                  on Error: Exception do
                                  begin
                                       Rollback;
                                       SetErrorFound( True );
                                       Bitacora.HandleException( 'ERROR Al Borrar Trigger ' + ItemName + FormatScript( sScript ), Error );
                                  end;
                            end;
                       end;
                       DoCallBack( Self, lOk );
                       Next;
                  end;
                  Active := False;
                  CachedUpdates := lCached;
             end;
          except
                on Error: Exception do
                begin
                     SetErrorFound( True );
                     Bitacora.HandleException( 'ERROR al Borrar Triggers', Error );
                end;
          end;
          BaseDatos.Connected := False;
          DoEndCallBack;
     end;
     Result := lOk;
end;

function TdmReportes.GetVersionList( Lista: TStrings; const sPatchFile: String ): Boolean;
begin
     Result := False;
     with Lista do
     begin
          BeginUpdate;
          Clear;
          try
             PreparaQueryParadox( tqSource, Format( 'select DISTINCT( SP_VERSION ) from %s order by SP_VERSION desc', [ sPatchFile ] ) );
             with tqSource do
             begin
                  Active := True;
                  while not Eof do
                  begin
                       Add( FieldByName( 'SP_VERSION' ).AsString );
                       Result := True;
                       Next;
                  end;
                  Active := False;
             end;
          except
          end;
          EndUpdate;
     end;
end;

function TdmReportes.EjecutaScript( const Modo: TModoPatch; const sPatchFile, sVerActual, sVersion, sTitulo, sToken, sValor: String; const iClase: Integer; BaseDatos: TDatabase): Boolean;
var
 lHuboAdvertencias : boolean;
begin
     Result :=EjecutaScript( Modo, sPatchFile, sVerActual, sVersion, sTitulo, sToken, sValor, iClase, BaseDatos, lHuboAdvertencias);
end;


function TdmReportes.EjecutaScript( const Modo: TModoPatch; const sPatchFile, sVerActual, sVersion, sTitulo, sToken, sValor: String; const iClase: Integer; BaseDatos: TDatabase; var lHuboAdvertencias : boolean): Boolean;
const
     K_ERROR_MSG = 'ERROR Al %s: Script #%s: %s';
     K_WARNING_MSG = 'Script: #%s Estructura: %s ';

var
   sScript, sCampo, sListaWarnings: String;
   sServer, sDatabase: String;
   iSeqNum: Integer;
   lOk: Boolean;

function GetFiltro: String;
begin
     with Modo do
     begin
          case Filtro of
               esTodos: Result := '';
               esRango: Result := 'and ( SP_SEQ_NUM >= ' + IntToStr( Inicial ) + ' ) and ( SP_SEQ_NUM <= ' + IntToStr( Final ) + ' )';
               esLista: Result := '';
          end;
     end;
end;

function GetVersion: String;
begin
     if ZetaCommonTools.StrVacio( sVerActual ) or ( Modo.Filtro = esRango ) and ( sVerActual = sVersion ) then
        Result:= Format( '( SP_VERSION = %s )', [ EntreComillas( sVersion ) ] )
     else
         Result := Format( '( SP_VERSION > %s ) and ( SP_VERSION <= %s )', [ EntreComillas( sVerActual ), EntreComillas( sVersion ) ] );
end;

{$ifdef INTERBASE}
procedure DropConstraintsSinNombre(const iOpcion:Integer);
const
      K_ScriptKarcurso = 'select ''ALTER TABLE ''||r.rdb$relation_name ||'' DROP CONSTRAINT ''||r.rdb$constraint_name||'';''' +
                                 'AS COMANDO from rdb$relation_constraints r ' +
                                 'left outer join RDB$INDEX_SEGMENTS ON ' +
                                 '( rdb$relation_constraints.RDB$INDEX_NAME = RDB$INDEX_SEGMENTS.RDB$INDEX_NAME ) '+
                                 'where ( r.rdb$relation_name = ''KARCURSO'') and (r.rdb$constraint_type=''FOREIGN KEY'') AND '+
                                 'RDB$INDEX_SEGMENTS.RDB$FIELD_NAME = ''CU_CODIGO''';
      K_ScriptCafeCome = 'select ''ALTER TABLE ''||r.rdb$relation_name ||'' DROP CONSTRAINT ''||r.rdb$constraint_name||'';''' +
                                 'AS COMANDO from rdb$relation_constraints r ' +
                                 'left outer join RDB$INDEX_SEGMENTS ON ' +
                                 '( rdb$relation_constraints.RDB$INDEX_NAME = RDB$INDEX_SEGMENTS.RDB$INDEX_NAME ) '+
                                 'where ( r.rdb$relation_name = ''CAF_COME'') and (r.rdb$constraint_type=''PRIMARY KEY'') AND '+
                                 'RDB$INDEX_SEGMENTS.RDB$FIELD_NAME = ''CB_CODIGO''';
      K_ScriptCafeInv = 'select ''ALTER TABLE ''||r.rdb$relation_name ||'' DROP CONSTRAINT ''||r.rdb$constraint_name||'';''' +
                                 'AS COMANDO from rdb$relation_constraints r ' +
                                 'left outer join RDB$INDEX_SEGMENTS ON ' +
                                 '( rdb$relation_constraints.RDB$INDEX_NAME = RDB$INDEX_SEGMENTS.RDB$INDEX_NAME ) '+
                                 'where ( r.rdb$relation_name = ''CAF_INV'') and (r.rdb$constraint_type=''PRIMARY KEY'') AND '+
                                 'RDB$INDEX_SEGMENTS.RDB$FIELD_NAME = ''IV_CODIGO''';
      K_ScriptVacacion = 'select ''ALTER TABLE ''||r.rdb$relation_name ||'' DROP CONSTRAINT ''||r.rdb$constraint_name||'';''' +
                                 'AS COMANDO from rdb$relation_constraints r ' +
                                 'left outer join RDB$INDEX_SEGMENTS ON ' +
                                 '( rdb$relation_constraints.RDB$INDEX_NAME = RDB$INDEX_SEGMENTS.RDB$INDEX_NAME ) '+
                                 'where ( r.rdb$relation_name = ''VACACION'') and (r.rdb$constraint_type=''PRIMARY KEY'') AND '+
                                 'RDB$INDEX_SEGMENTS.RDB$FIELD_NAME = ''CB_CODIGO''';
      K_ScriptEmpPoliza = 'select ''ALTER TABLE ''||r.rdb$relation_name ||'' DROP CONSTRAINT ''||r.rdb$constraint_name||'';''' +
                                 'AS COMANDO from rdb$relation_constraints r ' +
                                 'left outer join RDB$INDEX_SEGMENTS ON ' +
                                 '( rdb$relation_constraints.RDB$INDEX_NAME = RDB$INDEX_SEGMENTS.RDB$INDEX_NAME ) '+
                                 'where ( r.rdb$relation_name = ''EMP_POLIZA'') and (r.rdb$constraint_type=''PRIMARY KEY'') AND '+
                                 'RDB$INDEX_SEGMENTS.RDB$FIELD_NAME = ''CB_CODIGO''';

      procedure EjecutarScriptDeletePK;
      begin
          with tqTarget do
          begin
             AttachQuery( BaseDatos, tqTarget, sScript );
             Active:= True;
             if (not isEmpty) then //Nada mas existe una referencia a KARCURSO
             begin
                  with BaseDatos do
                  begin
                       StartTransaction;
                        try
                           EjecutaDDL( BaseDatos, tqTarget, tqTarget.FieldByName('COMANDO').AsString );
                           Commit;
                        except
                              on Error: Exception do
                              begin
                                   Rollback;
                                   SetErrorFound( True );
                                   Bitacora.HandleException( Format( K_ERROR_MSG, [ sTitulo, IntToStr( iSeqNum ), ItemName ] ) + FormatScript( sScript ), Error );
                              end;
                        end;
                  end;
             end;
             DoCallBack( Self, lOk );
             Active:= False;
             AttachQuery( BaseDatos, tqTarget, '' );
          end;
      end;
begin
     if (sVersion = '420') then
     begin
          case iOpcion of
              K_KARCURSO_CONSTRAINT: sScript := K_ScriptKarcurso;
              K_CAF_COME_CONSTRAINT: sScript :=  K_ScriptCafeCome;
              K_CAF_INV_CONSTRAINT: sScript := K_ScriptCafeInv;
          end;
     end;
     if (sVersion = '430') and ( iOpcion = K_VACACION_CONSTRAINT ) then
     begin
          sScript := K_ScriptVacacion;
     end;
     if (sVersion = '435') and ( iOpcion = K_EMP_POLIZA_CONSTRAINT ) then
     begin
          sScript := K_ScriptEmpPoliza;
     end;

     if strLleno( sScript ) then
        EjecutarScriptDeletePK;
end;
{$endif}

begin
     lOk := True;
     lHuboAdvertencias := FALSE;
     BaseDatos.Connected := True;
     AttachQuery( BaseDatos, tqTarget, '' );
     try
        sCampo := GetScriptFieldName;
        Bitacora.AgregaResumen( sTitulo );
        DoStartCallBack( sTitulo );
        {$ifdef INTERBASE}
        if iClase = K_SQL then
        begin
             DropConstraintsSinNombre(K_KARCURSO_CONSTRAINT);
             DropConstraintsSinNombre(K_CAF_COME_CONSTRAINT);
             DropConstraintsSinNombre(K_CAF_INV_CONSTRAINT);
             DropConstraintsSinNombre(K_VACACION_CONSTRAINT);
             DropConstraintsSinNombre(K_EMP_POLIZA_CONSTRAINT);
        end;
        {$endif}                                                                                                                                                  // ( SP_VERSION = %s )  - EntreComillas( sVersion )
        PreparaQueryParadox( tqSource, Format( 'select SP_NAME, SP_SEQ_NUM, %s from %s where %s and ( SP_TIPO = ' + IntToStr( iClase ) + ' ) %s order by SP_SEQ_NUM', [ sCampo, sPatchFile, GetVersion, GetFiltro ] ) );
        ZetaServerTools.GetServerDatabase( Registry.Database, sServer, sDatabase );
        with tqSource do
        begin
             Active := True;
             while not Eof and lOk do
             begin
                  ItemName := FieldByName( 'SP_NAME' ).AsString;
                  iSeqNum := FieldByName( 'SP_SEQ_NUM' ).AsInteger;
                  sScript := FieldByName( sCampo ).AsString;
                  if strLleno( sScript ) then                    // Si no est� lleno el Script no ejecuta el SQL
                  begin
                       if StrLleno( sToken ) then
                          sScript := StrTransAll( sScript, sToken, sValor );

                       //* Las siguientes lineas es para cambiar el #COMPARTE y #EMPRESA por los codigos correspondientes *//
                       //* Solo aplica para el TressCFG de SQL Server *//

                       {$ifdef MSSQL}
                       if ( Pos( UpperCase( sServer ), UpperCase( dmDBConfig.GetCompanyDataBase )  ) <= 0 ) then
                          sScript := StrTransAll( sScript, K_NOMBRE_COMPARTE, Comilla + sServer + Comilla + K_PUNTO + sDataBase )
                       else
                           sScript := StrTransAll( sScript, K_NOMBRE_COMPARTE, sDatabase );

                       sScript := StrTransAll( sScript, K_NOMBRE_EMPRESA, dmDBConfig.GetCompany );

                       sScript := StrTransAll( sScript,K_DIGITO_EMPRESA, EntreComillas(dmDBConfig.GetDigito) );
                       {$endif}
                       with BaseDatos do
                       begin
                            StartTransaction;
                            try
                               EjecutaDDL( BaseDatos, tqTarget, sScript );
                               Commit;
                            except
                                  on Error: Exception do
                                  begin
                                       Rollback;
                                       if ( GetTipoBitacora( Error.Message ) = tbAdvertencia ) then
                                       begin
                                            sListaWarnings:=  Format ( K_WARNING_MSG, [ IntToStr( iSeqNum ), ItemName ] ) + CR_LF + sListaWarnings
                                       end
                                       else
                                       begin
                                            SetErrorFound( True );
                                            Bitacora.HandleException( Format( K_ERROR_MSG, [ sTitulo, IntToStr( iSeqNum ), ItemName ] ) + FormatScript( sScript ), Error );
                                       end
                                  end;
                            end;
                       end;
                  end;
                  DoCallBack( Self, lOk );
                  Next;
             end;
             Active := False;
        end;
     except
           on Error: Exception do
           begin
                SetErrorFound( True );
                Bitacora.HandleException( 'ERROR al ' + sTitulo, Error );
           end;
     end;
     BaseDatos.Connected := False;
     if StrLleno ( sListaWarnings ) then
     begin
          lHuboAdvertencias := TRUE;
          Bitacora.AddWarningList( ' Advertencia: No se complet� el cambio de estructuras ','Objetos previamente creados en '+  dmDBConfig.GetCompany + CR_LF, sListaWarnings );
     end;
     Result := lOk and DoEndCallBack;
end;

function TdmReportes.GetModoPatchDefault: TModoPatch;
begin
     with Result do
     begin
          Habilitado := True;
          Filtro := esTodos;
          Inicial := 1;
          Final := 999;
          Lista := '';
     end;
end;

function TdmReportes.Patch( const Modo: TModoPatch; const sPatchFile, sVerActual, sVersion: String; BaseDatos: TDatabase): Boolean;
var
   lHuboAdvertencias : boolean;
begin
     Result := Patch( Modo, sPatchFile, sVerActual, sVersion, BaseDatos, lHuboAdvertencias );
end;

function TdmReportes.Patch( const Modo: TModoPatch; const sPatchFile, sVerActual, sVersion: String; BaseDatos: TDatabase; var lHuboAdvertencias : boolean ): Boolean;
begin
     with Modo do
     begin
          if Habilitado then
             Result := EjecutaScript( Modo, sPatchFile, sVerActual, sVersion, 'Cambiar Estructuras', '', '', K_SQL, BaseDatos, lHuboAdvertencias )
          else
              Result := True;
     end;
end;

function TdmReportes.CrearTriggers( const lEnabled: Boolean; const sPatchFile, sVerActual, sVersion: String; BaseDatos: TDatabase ): Boolean;
begin
     if lEnabled then
        Result := EjecutaScript( GetModoPatchDefault, sPatchFile, sVerActual, sVersion, 'Crear Triggers', '', '', K_TRIGGER, BaseDatos )
     else
         Result := True;
end;

function TdmReportes.CrearStoredProcedures( const lEnabled: Boolean; const sPatchFile, sVerActual, sVersion, sSupNivel: String; BaseDatos: TDatabase ): Boolean;
begin
     if lEnabled then
        Result := EjecutaScript( GetModoPatchDefault, sPatchFile, sVerActual, sVersion, 'Crear Stored Procedures', 'CB_NIVEL_X', sSupNivel, K_STORED_PROCEDURE, BaseDatos )
     else
         Result := True;
end;

{$ifdef ANTES}
function TdmReportes.CrearStoredProceduresExtras( const lEnabled: Boolean; const sPatchFile: String; const iTipo: Integer; BaseDatos: TDatabase ): Boolean;
const
     K_ERROR_MSG = 'ERROR Al %s: Script #%s: %s';
     K_TITULO = 'Agregar Stored Procedures Adicionales';
var
   sScript: String;
   iSeqNum: Integer;
   lOk: Boolean;
begin
     if lEnabled and FileExists( sPatchFile ) then
     begin
          CloseDBSourceShared;
          SourceSharedPath := ExtractFilePath( sPatchFile );
          OpenDBSourceShared;
          lOk := True;
          BaseDatos.Connected := True;
          AttachQuery( BaseDatos, tqTarget, '' );
          try
             Bitacora.AgregaResumen( K_TITULO );
             DoStartCallBack( K_TITULO );
             PreparaQueryParadox( tqSource, Format( 'select SE_NOMBRE, SE_SEQ_NUM, SE_DATA from %s where ( SE_TIPO = %d ) order by SE_SEQ_NUM', [ ExtractFileName( sPatchFile ), iTipo + 1 ] ) );
             with tqSource do
             begin
                  Active := True;
                  while not Eof and lOk do
                  begin
                       ItemName := FieldByName( 'SE_NOMBRE' ).AsString;
                       iSeqNum := FieldByName( 'SE_SEQ_NUM' ).AsInteger;
                       sScript := FieldByName( 'SE_DATA' ).AsString;
                       with BaseDatos do
                       begin
                            StartTransaction;
                            try
                               EjecutaDDL( BaseDatos, tqTarget, sScript );
                               Commit;
                            except
                                  on Error: Exception do
                                  begin
                                       Rollback;
                                       SetErrorFound( True );
                                       Bitacora.HandleException( Format( K_ERROR_MSG, [ K_TITULO, IntToStr( iSeqNum ), ItemName ] ) + FormatScript( sScript ), Error );
                                  end;
                            end;
                       end;
                       DoCallBack( Self, lOk );
                       Next;
                  end;
                  Active := False;
             end;
          except
                on Error: Exception do
                begin
                     SetErrorFound( True );
                     Bitacora.HandleException( 'ERROR al ' + K_TITULO, Error );
                end;
          end;
          BaseDatos.Connected := False;
          Result := lOk and DoEndCallBack;
     end
     else
         Result := True;
end;
{$endif}

function TdmReportes.CrearStoredProceduresExtras( const lEnabled: Boolean; const iTipo: Integer; BaseDatos: TDatabase): Boolean;
const
     K_ERROR_MSG = 'ERROR Al %s: Script #%s: %s';
     K_TITULO = 'Agregar Stored Procedures Adicionales';
{$ifdef MSSQL}
     K_TITULO_SCRIPT  = 'Ejecutar Script Especial Adicional';
{$endif}
var
   sScript: String;
   {$ifdef MSSQL}
   sScriptFile : String;
   dmConfiguraScripts : TdmConfigurarEspeciales;
   {$endif}
   iSeqNum: Integer;
   lOk: Boolean;

   function  CuentaSPExtras : integer;
   begin
        if ( iTipo < 0 ) then
            Result := dmDBConfig.CuentaSPPatch( iTipo + 1 , VACIO )
         else
             Result := dmDBConfig.CuentaSPPatch( iTipo + 1 , dmDBConfig.GetCompany );
   end;

   function CargaSPExtras : boolean;
   begin
        if ( iTipo < 0 ) then
           Result:= dmDBConfig.ConsultarSPPatch( iTipo + 1 , VACIO )
        else
            Result := dmDBConfig.ConsultarSPPatch( iTipo + 1 , dmDBConfig.GetCompany );
   end;
{$ifdef MSSQL}

   procedure ConfigurarConexionScripts;
   begin
        dmConfiguraScripts :=  TdmConfigurarEspeciales.Create(nil);
        dmConfiguraScripts.DmMigracion := Self;
        dmConfiguraScripts.Tipo := iTipo;
   end;
{$endif}
begin
     if ( lEnabled ) and ( CuentaSPExtras > 0 ) then
     begin
          lOk := True;
          BaseDatos.Connected := True;
          {$ifdef MSSQL}
          ConfigurarConexionScripts;
          {$endif}

          AttachQuery( BaseDatos, tqTarget, '' );
          try
             Bitacora.AgregaResumen( K_TITULO );
             DoStartCallBack( K_TITULO );

             CargaSPExtras;

             with dmDBConfig.tqSPPatchToApply do
             begin
                  if ( not IsEmpty ) then
                  begin
                       while not Eof and lOk do
                       begin
                           ItemName := FieldByName( 'SE_NOMBRE' ).AsString;
                           iSeqNum := FieldByName( 'SE_SEQ_NUM' ).AsInteger;
                           sScript := FieldByName( 'SE_DATA' ).AsString;
                           {$ifdef MSSQL}
                           sScriptFile := FieldByName( 'SE_ARCHIVO' ).AsString;
                           {$endif}

                           if StrLLeno( sScript ) then
                           with BaseDatos do
                           begin
                                StartTransaction;
                                try
                                   EjecutaDDL( BaseDatos, tqTarget, sScript );
                                   Commit;
                                except
                                      on Error: Exception do
                                      begin
                                           Rollback;
                                           SetErrorFound( True );
                                           Bitacora.HandleException( Format( K_ERROR_MSG, [ K_TITULO, IntToStr( iSeqNum ), ItemName ] ) + FormatScript( sScript ), Error );
                                      end;
                                end;
                           end;

                           {$ifdef MSSQL}
                           if StrLLeno( sScriptFile ) then
                           begin
                               with dmConfiguraScripts do
                               begin
                                    try
                                       if Conectar then
                                       begin
                                            dmConfiguraScripts.ScriptFile :=  sScriptFile;
                                            dmConfiguraScripts.ProcesaScript;
                                       end
                                    except
                                      on Error: Exception do
                                      begin
                                           SetErrorFound( True );
                                           Bitacora.HandleException( Format( K_ERROR_MSG, [ K_TITULO, IntToStr( iSeqNum ), ItemName ] ) + ScriptFile, Error );
                                      end;
                                    end;
                                    Desconectar;
                               end;
                           end;
                           {$endif}

                           DoCallBack( Self, lOk );
                           Next;
                       end;
                  end;

                  Active := False;
             end;
          except
                on Error: Exception do
                begin
                     SetErrorFound( True );
                     Bitacora.HandleException( 'ERROR al ' + K_TITULO, Error );
                end;
          end;
          BaseDatos.Connected := False;
          Result := lOk and DoEndCallBack;
     end
     else
         Result := True;
end;


function TdmReportes.GetAccion( const lValor: Boolean ): String;
begin
     if lValor then
        Result := 'ACTIVADA'
     else
         Result := 'DESACTIVADA';
end;

function TdmReportes.GetModo( const Valor: TModoPatch ): String;
begin
     with Valor do
     begin
          if Habilitado then
          begin
               Result := 'ACTIVADA';
               case Filtro of
                    esTodos: Result := Result + ' ( TODOS LOS SCRIPTS )';
                    esRango: Result := Result + ' ( SCRIPTS DEL ' + IntToStr( Inicial ) + ' AL ' + IntToStr( Final ) + ' )';
                    esLista: Result := Result + ' ( SCRIPTS ' + Lista + ' )';
               end;
          end
          else
              Result := 'DESACTIVADA';
     end;
end;

function TdmReportes.GetSPFileName( const sExtension: String ): String;
begin
     Result := Format( '%sSP_EXTRA.%s', [ ExtractFilePath( Application.ExeName ), sExtension ] );
end;

procedure TdmReportes.PrepararEstructura(const sVersion : string);
begin
     {$ifdef INTERBASE}
     if sVersion = '420' then
     begin
          Bitacora.AgregaResumen( 'Revisando view CONTA_KC' );
          PreparaQuery( tqTarget, 'DROP PROCEDURE SET_KARCURSO_CONTADOR' );
          try
             tqTarget.ExecSQL;
          except
          end;

          PreparaQuery( tqTarget, 'DROP PROCEDURE SET_KARCURSO_CONTADOR_RP' );
          try
             tqTarget.ExecSQL;
          except
          end;

          if BDEFunctions.BDECheckTableExists( dbTarget.Handle, 'CONTA_KC' ) then
          begin
               PreparaQuery( tqTarget, 'DROP VIEW CONTA_KC' );
               tqTarget.ExecSQL;
          end;
     end;
     {$endif}
end;


function TdmReportes.ActualizarDatos( const sPatchFile, sVerActual, sVersion: String; const lTriggers, lSP, lKardex, lDiccionario, lReportes: Boolean; const ModoEstructuras: TModoPatch): Boolean;
var
   lHuboErrores, lHuboAdvertencias : boolean;
begin
   Result := ActualizarDatos( sPatchFile, sVerActual, sVersion, lTriggers, lSP, lKardex, lDiccionario, lReportes, ModoEstructuras, lHuboErrores, lHuboAdvertencias );
end;


function TdmReportes.ActualizarDatos( const sPatchFile, sVerActual, sVersion: String; const lTriggers, lSP, lKardex, lDiccionario, lReportes: Boolean; const ModoEstructuras: TModoPatch; var lHuboErrores, lHuboAdvertencias : boolean; const lTratarAdvertencias : boolean = FALSE ): Boolean;
var
   sValor {$ifdef ANTES}, sSpExtra {$endif} : String;
begin
{$ifdef ANTES}
     sSpExtra := GetSPFileName( 'DB' );
{$endif}
     case CompanyType of
          tcRecluta: sValor := 'Selecci�n';
     else
         sValor := 'Empresa';
     end;
     with Bitacora do
     begin
          AgregaResumen( Format( 'Actualizaci�n de Base de Datos de %s', [ sValor ] ) );
          AgregaResumen( 'Base de Datos:                     ' + TargetAlias );
          AgregaResumen( 'Versi�n Actual:                    ' + sVerActual );
          AgregaResumen( 'Versi�n Deseada:                   ' + sVersion );
          AgregaResumen( 'Actualizaci�n de Triggers:         ' + GetAccion( lTriggers ) );
          AgregaResumen( 'Actualizaci�n de StoredProcedures: ' + GetAccion( lSP ) );
     {$ifdef ANTES}
          if FileExists( sSpExtra ) then
          begin
               AgregaResumen( 'Archivo de Stored Procedures Adicionales: ' + sSpExtra );
          end;
     {$endif}
          AgregaResumen( 'Actualizaci�n de Estructuras:      ' + GetModo( ModoEstructuras ) );
          AgregaResumen( 'Actualizaci�n de Diccionario:      ' + GetAccion( lDiccionario ) );
          AgregaResumen( 'Importaci�n de Reportes:           ' + GetAccion( lReportes ) );

          AgregaResumen( GetTimeStamp + ': Iniciando Actualizaci�n' );

     end;
     ClearErrorFound;
     if CompanyType = tc3Datos then
     begin
          PreparaQuery( tqTarget, 'select GL_FORMULA from GLOBAL where ( GL_CODIGO = ' + IntToStr( K_GLOBAL_NIVEL_SUPERVISOR ) + ' )' );
          with tqTarget do
          begin
               Active := True;
               if IsEmpty or ( StrToInt( Trim(Fields[ 0 ].AsString ) ) <= 0 ) then
                  sValor := 'CB_NIVEL9'
               else
                   sValor := 'CB_NIVEL' + Trim( Fields[ 0 ].AsString );
               Active := False;
          end;
     end
     else
         sValor := 'CB_NIVEL9';


     PrepararEstructura(sVersion);

     Result := BorrarStoredProcedures( lSP, dbTarget ) and
               BorrarFunciones( lSP, dbTarget ) and
               BorrarTriggers( lTriggers, dbTarget ) and
               Patch( ModoEstructuras, sPatchFile, sVerActual, sVersion, dbTarget, lHuboAdvertencias ) and
               CrearTriggers( lTriggers, sPatchFile, '', sVersion, dbTarget ) and
               CrearStoredProcedures( lSP, sPatchFile, '', sVersion, sValor, dbTarget ) and
               CrearStoredProceduresExtras( lSP, Ord( CompanyType ), dbTarget );
     {$ifdef ANTES}
               CrearStoredProceduresExtras( lSP, sSpExtra, Ord( CompanyType ), dbTarget );
     {$endif}
     with Bitacora do
     begin
          sValor := GetTimeStamp + ': Actualizaci�n Terminada %s';

          lHuboErrores :=  ErrorFound;
          if Result and not ErrorFound then
          begin
             CambiaGlobalVersion( sVersion );
             if ( lTratarAdvertencias ) then
             begin
                  if ( lHuboAdvertencias ) then
                     AgregaResumen( Format( sValor, [ 'Con ADVERTENCIAS' ] ) )
                  else
                      AgregaResumen( Format( sValor, [ 'Con Exito' ] ) );

             end
             else
                 AgregaResumen( Format( sValor, [ 'Con Exito' ] ) );
          end
          else
              AgregaResumen( Format( sValor, [ 'CON ERRORES' ] ) );
     end;
     if lKardex then
     begin
          with Bitacora do
          begin
               AgregaResumen( 'Iniciando Rec�lculo de Kardex' );
               if RecalcularKardex( dbTarget ) then
               begin
                    AgregaResumen( Format( '%s: Rec�lculo de Kardex Terminado Con Exito', [ GetTimeStamp ] ) );
               end
               else
               begin
                    AgregaResumen( Format( '%s: Hubo Problemas Al Recalcular Kardex', [ GetTimeStamp ] ) );
               end;
          end;
     end;
     {$ifdef INTERBASE}
     if sVersion = '410' then
     begin
          with Bitacora do
          begin
               AgregaResumen( 'Iniciando Copia de Derechos de Acceso de Datos Adicionales' );
               if CopiaDerechosAdicionales( dbTarget ) then
               begin
                    AgregaResumen( Format( '%s: Copia de Derechos de Acceso de Datos Adicionales Terminado Con Exito', [ GetTimeStamp ] ) );
               end
               else
               begin
                    AgregaResumen( Format( '%s: Hubo Problemas Al Copiar Derechos de Acceso de Datos Adicionales', [ GetTimeStamp ] ) );
               end;
          end;
     end;
     {$endif}
end;

function TdmReportes.ActualizarComparte( const sPatchFile, sVerActual, sVersion: String; const lTriggers, lSP: Boolean; const ModoEstructuras: TModoPatch ): Boolean;
var
   sValor, sSpExtra: String;
begin
     sSpExtra := GetSPFileName( 'DB' );
     with dmDBConfig do
     begin
          ConectarComparte;
          with Bitacora do
          begin
               AgregaResumen( 'Actualizaci�n de Base de Datos Compartida' );
               AgregaResumen( 'Base de Datos :                    ' + dbEmpresas.AliasName );
               AgregaResumen( 'Versi�n Actual:                    ' + sVerActual );
               AgregaResumen( 'Versi�n Deseada:                   ' + sVersion );
               AgregaResumen( 'Actualizaci�n de Triggers:         ' + GetAccion( lTriggers ) );
               AgregaResumen( 'Actualizaci�n de StoredProcedures: ' + GetAccion( lSP ) );
          {$ifdef ANTES}
               if FileExists( sSpExtra ) then
               begin
                    AgregaResumen( 'Archivo de Stored Procedures Adicionales: ' + sSpExtra );
               end;
          {$endif}
               AgregaResumen( 'Actualizaci�n de Estructuras: ' + GetModo( ModoEstructuras ) );
               AgregaResumen( GetTimeStamp + ': Iniciando Actualizaci�n' );
          end;
          ClearErrorFound;
          Result := BorrarStoredProcedures( lSP, dbEmpresas ) and
                    BorrarFunciones( lSP, dbEmpresas ) and
                    BorrarTriggers( lTriggers, dbEmpresas ) and
                    Patch( ModoEstructuras, sPatchFile, sVerActual, sVersion, dbEmpresas ) and
                    CrearTriggers( lTriggers, sPatchFile, '', sVersion, dbEmpresas ) and
                    CrearStoredProcedures( lSP, sPatchFile, '', sVersion, sValor, dbEmpresas ) and
                    CrearStoredProceduresExtras( lSP,  -1, dbEmpresas );
{$ifdef ANTES}
                    CrearStoredProceduresExtras( lSP, sSpExtra, -1, dbEmpresas );
{$endif}
          with Bitacora do
          begin
               sValor := GetTimeStamp + ': Actualizaci�n Terminada %s';
               if Result and not ErrorFound then
               begin
                    CambiaClavesVersion( sVersion );
                    AgregaResumen( Format( sValor, [ 'Con Exito' ] ) );
               end
               else
                   AgregaResumen( Format( sValor, [ 'CON ERRORES' ] ) );
          end;

          {$ifdef INTERBASE}
          // if sVersion = '430' then  // CUANTO SE SUBA SERA 435
          if sVersion = '435' then
          begin
               with Bitacora do
               begin
                    AgregaResumen( 'Iniciando Restructura de Derechos de Acceso de Datos Otros ' );
                    if RestructurarDerechosOtros then
                    begin
                         AgregaResumen( Format( '%s: Restructura de Derechos de Acceso de Datos Otros Terminado Con Exito', [ GetTimeStamp ] ) );
                    end
                    else
                    begin
                         AgregaResumen( Format( '%s: Hubo Problemas Al Restructurar Derechos de Acceso de Datos Otros', [ GetTimeStamp ] ) );
                    end;
               end;
          end;
          {$endif}
          DesconectaComparte;
     end;
end;

function TdmReportes.GetVerActualComparte: String;
const
     Q_VA_SELECT_CLAVES = 'select CL_TEXTO from CLAVES where ( CL_NUMERO = %d )';
begin
     FVerActual:= '0';
     with dmDBConfig do
     begin
          ConectarComparte;
          AttachQuery( dbEmpresas, tqTarget, Format( Q_VA_SELECT_CLAVES, [ K_POS_CLAVES_VERSION ] ) );
          with tqTarget do
          begin
               Active := True;
               FExiste_VA:= not IsEmpty;
               if FExiste_VA then
                  FVerActual:= FieldByName( 'CL_TEXTO' ).AsString
               else
                   FVerActual := '370'; { GA 09/Abr/2003: Este debe ser un muy buen default }
               Active := False;
          end;
          DesconectaComparte;
     end;
     Result:= FVerActual;
end;

procedure TdmReportes.CambiaClavesVersion( const sVersion: String );
const
     Q_VA_UPDATE_CLAVES = 'update CLAVES set CL_TEXTO = %s where ( CL_NUMERO = %d )';
     Q_VA_INSERTA_CLAVES = 'insert into CLAVES ( CL_NUMERO, CL_TEXTO ) values ( %d, %s )';
begin
     if FExiste_VA then
     begin
          if ( FVerActual <> sVersion ) then
          begin
               AttachQuery( dmDBConfig.dbEmpresas, tqTarget, Format( Q_VA_UPDATE_CLAVES, [ EntreComillas( sVersion ), K_POS_CLAVES_VERSION ] ) );
               tqTarget.ExecSQL;
          end;
     end
     else
     begin
          AttachQuery( dmDBConfig.dbEmpresas, tqTarget, Format( Q_VA_INSERTA_CLAVES, [ K_POS_CLAVES_VERSION, EntreComillas( sVersion ) ] ) );
          tqTarget.ExecSQL;
     end;
end;

function TdmReportes.GetGlobalVersion: Integer;
const
     K_GLOBAL_VERSION_TRESS = 133;
     K_GLOBAL_VERSION_SELECCION = 78;
begin
     case CompanyType of
          tcRecluta: Result := K_GLOBAL_VERSION_SELECCION;
     else
         Result := K_GLOBAL_VERSION_TRESS;
     end;
end;

function TdmReportes.GetVerActual: String;
const
     K_VERSION_INICIAL = '300';
begin
     FVerActual:= K_VERSION_INICIAL;
     PreparaQuery( tqTarget, Format( 'select GL_FORMULA from GLOBAL where ( GL_CODIGO = %d )', [ GetGlobalVersion ] ) );
     with tqTarget do
     begin
          Active := True;
          FExiste_VA:= not IsEmpty;
          if FExiste_VA then
          begin
             FVerActual:= FieldByName( 'GL_FORMULA' ).AsString;
             FVerActual := Trim(FVerActual);
          end;
          Active := False;
     end;
     Result := FVerActual;
end;

procedure TdmReportes.CambiaGlobalVersion( sVersion: String );
begin
     if FExiste_VA then
     begin
        if ( FVerActual <> sVersion ) then
        begin
             PreparaQuery( tqTarget, Format( 'update GLOBAL set GL_FORMULA = ''%s'' where ( GL_CODIGO = %d )', [ sVersion, GetGlobalVersion ] ) );
             tqTarget.ExecSQL;
        end;
     end
     else
     begin
          PreparaQuery( tqTarget, Format( 'insert into GLOBAL ( GL_CODIGO, GL_FORMULA ) values ( %d, ''%s'' )', [ GetGlobalVersion, sVersion ] ) );
          tqTarget.ExecSQL;
     end;
end;

{$ifdef INTERBASE}
function TdmReportes.RestructurarDerechosOtros: Boolean;
const
     K_ACCESO_NIVELO_OTROS =' INSERT INTO ACCESO(GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO) ' +
                            ' VALUES (%s, %s, %d, %d)' ;
     D_689 = 689;
var
   AX_DERECHO : integer;
begin
     Result := False;

      {         if ( :Cuantos = 0 )then begin INSERT INTO ACCESO(GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO)
                 SELECT GR_CODIGO, CM_CODIGO, 689, AX_DERECHO FROM ACCESO  WHERE AX_NUMERO = 7;
                   end }
    try
       AttachQuery( dmDBConfig.dbEmpresas, tqTarget, Format ('select  COUNT(*) cuantos from ACCESO where ( AX_NUMERO = %d)', [D_689]));

       tqTarget.Active:= TRUE;
       if ( tqTarget.FieldByName('Cuantos').AsInteger = 0 ) then
       begin
             with dmDBConfig do
             begin
                  with tqList do
                  begin
                       SQL.Clear;
                       SQL.Add('select * from acceso a where ax_Numero = 7 ');
                       Active := TRUE;
                  end;
             end;
             while NOT dmDBConfig.tqList.Eof do
             begin
                  AX_DERECHO := dmDBConfig.tqList.FieldByName('AX_DERECHO').AsInteger;

                  if ( AX_DERECHO AND 128  > 0 ) then
                  begin
                          AX_DERECHO := AX_DERECHO OR 8;
                  end
                  else
                  begin
                          AX_DERECHO := AX_DERECHO AND NOT 8;
                  end;

                  with  dmDBConfig.tqList do
                    AttachQuery( dmDBConfig.dbEmpresas, tqTarget, Format( K_ACCESO_NIVELO_OTROS, [EntreComillas(FieldByName('GR_CODIGO').AsString), EntreComillas(FieldByName('CM_CODIGO').AsString), D_689, AX_DERECHO ] ) );
                  tqTarget.ExecSQL;
                  dmDBConfig.tqList.Next;
             end
       end;
       Result := True;
    except
          on Error: Exception do
          begin
               Bitacora.HandleException( 'ERROR Al Restructurar Derechos de Datos Otros' , Error );
          end;
    end;
end;

function TdmReportes.CopiaDerechosAdicionales( BaseDatos: TDatabase ): Boolean;
 var
    sScript: string;
begin
     Result := False;
     try
        PreparaQuery( tqTarget, 'select count(*) cuantos from GR_AD_ACC ' );
        tqTarget.Active:= TRUE;
        if ( tqTarget.FieldByName('Cuantos').AsInteger = 0 ) then
        begin

             with dmDBConfig do
             begin
                  //ConectarComparte;
                  with tqList do
                  begin
                       SQL.Clear;
                       SQL.Add(Format( 'select * from acceso a where ax_Numero = 8 ' +
                                       'and a.cm_codigo in '+
                                       '(select c.cm_codigo from company c WHERE CM_DATOS = %s) ', [Comillas( GetCompanyDatabase )] ));
                       Active := TRUE;
                  end;
             end;

             BaseDatos.Connected := True;
             with BaseDatos do
             begin
                  StartTransaction;
                  try

                     PreparaQuery( tqTarget, 'insert into GR_AD_ACC (GX_CODIGO,GR_CODIGO,CM_CODIGO,GX_DERECHO) '+
                                             'select GX_CODIGO,%d,%s,%d from GRUPO_AD' );
                     while NOT dmDBConfig.tqList.Eof do
                     begin
                          sScript := Format( 'insert into GR_AD_ACC (GX_CODIGO,GR_CODIGO,CM_CODIGO,GX_DERECHO) '+
                                             'select GX_CODIGO,%d,%s,%d from GRUPO_AD',
                                             [ dmDBConfig.tqList.FieldByName('GR_CODIGO').AsInteger,
                                               Comillas( dmDBConfig.tqList.FieldByName('CM_CODIGO').AsString ),
                                               dmDBConfig.tqList.FieldByName('AX_DERECHO').AsInteger ] );

                          PreparaQuery( tqTarget, sScript );
                          tqTarget.ExecSQL;
                          dmDBConfig.tqList.Next;
                     end;
                     Commit;
                     Result := True;
                  except
                        on Error: Exception do
                        begin
                             Rollback;
                             Bitacora.HandleException( 'ERROR Al Copiar Derechos de Acceso Adicionales', Error );
                        end;
                  end;
             end;
             dmDBConfig.tqList.Active := False;
             BaseDatos.Connected := False;
        end
        else
        begin
             Result := True;
             tqTarget.Active := FALSE;
        end;
     except
           on Error: Exception do
           begin
                Bitacora.HandleException( 'ERROR Al Iniciar Copia de Derechos de Acceso Adicionales', Error );
           end;
     end;

end;
{$ENDIF}

function TdmReportes.RecalcularKardex( BaseDatos: TDatabase ): Boolean;
const
     {$ifdef INTERBASE}
     K_RECALCULA_KARDEX = 'execute procedure KARDEX_TODOS';
     {$endif}
     {$ifdef MSSQL}
     K_RECALCULA_KARDEX = 'exec RECALCULA_TODOS';
     {$endif}
begin
     Result := False;
     try
        BaseDatos.Connected := True;
        with BaseDatos do
        begin
             StartTransaction;
             try
                PreparaQuery( tqTarget, K_RECALCULA_KARDEX );
                tqTarget.ExecSQL;
                Commit;
                Result := True;
             except
                   on Error: Exception do
                   begin
                        Rollback;
                        Bitacora.HandleException( 'ERROR Al Recalcular Kardex', Error );
                   end;
             end;
        end;
     except
           on Error: Exception do
           begin
                Bitacora.HandleException( 'ERROR Al Iniciar Rec�lculo De Kardex', Error );
           end;
     end;
     BaseDatos.Connected := False;
end;

{ ************* Consolidaci�n de Empresas **************** }

procedure TdmReportes.TransactionStart;
begin
     EmpezarTransaccion;
     FTxCounter := 1;
end;

function TdmReportes.TransactionCommit( const sMensaje: String ): Boolean;
begin
     ItemName := sMensaje;
     if ( FTxCounter > 99 ) then
     begin
          DoCallBack( Self, Result );
          TerminarTransaccion( True );
          TransactionStart;
     end
     else
     begin
          FTxCounter := FTxCounter + 1;
          Result := True;
     end;
end;

procedure TdmReportes.TransactionEnd;
begin
     TerminarTransaccion( True );
     FTxCounter := 1;
end;

function TdmReportes.GetConsolidationSteps( Lista: TCompanyList ): Integer;
begin
     Result := 8 + ( 9 * Lista.Count ); // Llamadas a CALLBACK() en TdmReportes.ExportarReportes //
end;

function TdmReportes.TerminaConsolidacion( const sTableName: String; const lExito: Boolean ): Boolean;
begin
     if lExito then
     begin
          Bitacora.AgregaResumen( GetTimeStamp + ' Tabla ' + sTableName + ' Fu� Consolidada' );
          Result := DoEndCallBack;
     end
     else
     begin
          Bitacora.AgregaResumen( GetTimeStamp + ' Tabla ' + sTableName + ' Fu� Parcialmente Consolidada' );
          Result := False;
     end;
end;

procedure TdmReportes.Consolidar( const sNombre: String; Lista: TCompanyList; const iYear: Integer; const dInicial, dFinal: TDate; const lBorrarTablas: Boolean; const iMaxEmpleados: Integer );
var
   lOk: Boolean;
   i, iNumEmpDBSource: Integer;
   sValor: String;
begin
     with Bitacora do
     begin
          AgregaResumen( 'Consolidaci�n de Empresas' );
          AgregaResumen( 'Empresa Consolidadora: ' + sNombre );
          AgregaResumen( 'Base de Datos: ' + TargetAlias );
          AgregaResumen( GetTimeStamp + ': Iniciando Consolidaci�n' );
     end;
     lOk := False;
     ClearErrorFound;
     try
        OpenDBTarget;
        if lBorrarTablas then
           lOk := BorraTablas
        else
            lOk := True;
        if lOk then
        begin
             PreparaQuery( tqSource, Format( Q_MAX_NUMEMP, [ DatetoStrSQLC( dInicial ) ] ) );
             with tqSource do
             begin
                  Active := True;
                  iNumEmpDBSource := FieldByName('CUANTOS').AsInteger;
             end;
             with Lista do
             begin
                  for i := 0 to ( Count - 1 ) do
                  begin
                       lOk := LeeTablas( Empresa[ i ], iYear, dInicial, dFinal, iMaxEmpleados, iNumEmpDBSource );
                       if not lOk then
                          Break;
                  end;
             end;
        end;
     except
           on Error: Exception do
           begin
                SetErrorFound( True );
                Bitacora.HandleException( 'ERROR al Abrir Empresa Consolidadora', Error );
           end;
     end;
     CloseDBTarget;
     with Bitacora do
     begin
          sValor := GetTimeStamp + ': Consolidaci�n Terminada %s';
          if lOk and not ErrorFound then
             AgregaResumen( Format( sValor, [ 'Con Exito' ] ) )
          else
              AgregaResumen( Format( sValor, [ 'CON ERRORES' ] ) );
     end;
end;

function TdmReportes.BorraTablas: Boolean;

procedure BorraTabla( const sTableName: String );
begin
     try
        PreparaQuery( tqTarget, 'delete from ' + sTableName );
        tqTarget.ExecSQL;
        Bitacora.AgregaResumen( GetTimeStamp + ' Tabla ' + sTableName + ' Fue Borrada' );
        if not DoEndCallBack then
           raise EAbort.Create( 'Proceso Interrumpido por el Usuario' );
     except
           on Error: Exception do
           begin
                Error.Message := 'Tabla: ' + sTableName + CR_LF + Error.Message;
                raise;
           end;
     end;
end;

begin
     with dbTarget do
     begin
          StartTransaction;
          try
             BorraTabla( 'INCAPACI' );
             BorraTabla( 'VACACION' );
             BorraTabla( 'PERMISO' );
             BorraTabla( 'AUSENCIA' );
             BorraTabla( 'ACUMULA' );
             BorraTabla( 'KAR_FIJA' );
             BorraTabla( 'KARDEX' );
             BorraTabla( 'COLABORA' );
             Commit;
             Result := True;
          except
                on Error: EAbort do
                begin
                     RollBack;
                     SetErrorFound( True );
                     Result := False;
                end;
                on Error: Exception do
                begin
                     RollBack;
                     SetErrorFound( True );
                     Bitacora.HandleException( 'ERROR al Borrar Tablas', Error );
                     Result := False;
                end;
          end;
     end;
end;

function TdmReportes.LeeTablas( Empresa: TCompanyData; const iYear: Integer; const dInicial, dFinal: TDate; const iMaxEmpleados: Integer; const iMaxEmpDBSource: Integer ): Boolean;
var
   iAcumulaEmpresas: Integer;
   lOk: Boolean;
function LeeUnaTabla( const sTableName, sFiltro, sOrden: String ): Boolean;
var
   lHayCBCodigo: Boolean;
   iCounter: Integer;
   sQuery: String;
begin
     Result := True;
     sQuery := 'select * from ' + sTableName;
     if ZetaCommonTools.StrLleno( sFiltro ) then
        sQuery := sQuery + ' where ' + sFiltro;
     if ZetaCommonTools.StrLleno( sOrden ) then
        sQuery := sQuery + ' order by ' + sOrden;
     try
        AttachQuery( dbFuente, tqSource, sQuery );
        AttachQuery( dbTarget, tqTarget, '' );
        with tqSource do
        begin
             Active := True;
             lHayCBCodigo := ( FindField( 'CB_CODIGO' ) <> nil );
             ZetaTressCFGTools.SetInsertScript( sTableName, tqSource, tqTarget );
             iCounter := 0;
             TransactionStart;
             while not Eof and Result do
             begin
                  try
                     ZetaTressCFGTools.TransfiereDatasets( sTableName, tqSource, tqTarget );
                     with tqTarget do
                     begin
                          if lHayCbCodigo then
                          begin
                               with ParamByName( 'CB_CODIGO' ) do
                               begin
                                    AsInteger := AsInteger + Empresa.Empate;
                               end;
                          end;
                          ExecSQL;
                     end;
                     iCounter := iCounter + 1;
                  except
                        on Error: Exception do
                        begin
                             SetErrorFound( True );
                             Bitacora.HandleException( 'ERROR al Transferir Tabla ' + sTableName, Error );
                        end;
                  end;
                  Result := TransactionCommit( sTableName + ' ' + IntToStr( iCounter ) );
                  Next;
             end;
             TransactionEnd;
             Active := False;
        end;
        Result := TerminaConsolidacion( sTableName, Result );
     except
           on Error: Exception do
           begin
                Bitacora.HandleException( 'ERROR al Consolidar Tabla ' + sTableName, Error );
                Result := False;
           end;
     end;
end;

function MergeTable( const sTableName, sKeyName, sFiltro, sOrden: String ): Boolean;
var
   lHayCbCodigo, lYaExiste: Boolean;
   sQuery: String;
   iCounter: Integer;
begin
     sQuery := 'select * from ' + sTableName;
     if ZetaCommonTools.StrLleno( sFiltro ) then
        sQuery := sQuery + ' where ' + sFiltro;
     if ZetaCommonTools.StrLleno( sOrden ) then
        sQuery := sQuery + ' order by ' + sOrden;
     try
        AttachQuery( dbFuente, tqSource, sQuery );
        AttachQuery( dbTarget, tqTarget, '' );
        AttachQuery( dbTarget, tqDelete, Format( 'select COUNT(*) from %s where ( %s = :%s )', [ sTableName, sKeyName, sKeyName ] ) );
        Result := True;
        iCounter := 0;
        TransactionStart;
        with tqSource do
        begin
             Active := True;
             lHayCbCodigo := ( FindField( 'CB_CODIGO' ) <> nil );
             ZetaTressCFGTools.SetInsertScript( sTableName, tqSource, tqTarget );
             while not Eof and Result do
             begin
                  tqDelete.ParamByName( sKeyName ).AssignField( FieldByName( sKeyName ) );
                  with tqDelete do
                  begin
                       Active := True;
                       if IsEmpty then
                          lYaExiste := False
                       else
                           lYaExiste := ( Fields[ 0 ].AsInteger > 0 );
                       Active := False;
                  end;
                  if not lYaExiste then
                  begin
                       try
                          ZetaTressCFGTools.TransfiereDatasets( sTableName, tqSource, tqTarget );
                          with tqTarget do
                          begin
                               if lHayCbCodigo then
                               begin
                                    with ParamByName( 'CB_CODIGO' ) do
                                    begin
                                         AsInteger := AsInteger + Empresa.Empate;
                                    end;
                               end;
                               ExecSQL;
                          end;
                       except
                             on Error: Exception do
                             begin
                                  SetErrorFound( True );
                                  Bitacora.HandleException( 'ERROR al Transferir Tabla ' + sTableName, Error );
                             end;
                       end;
                  end;
                  iCounter := iCounter + 1;
                  Result := TransactionCommit( sTableName + ' ' + IntToStr( iCounter ) );
                  Next;
             end;
             Active := False;
        end;
        Result := TerminaConsolidacion( sTableName, Result );
     except
           on Error: Exception do
           begin
                Bitacora.HandleException( 'ERROR al Consolidar Tabla ' + sTableName, Error );
                Result := False;
           end;
     end;
end;

   function ObtenerNumEmpleados: Integer;
   begin
        AttachQuery( dbFuente, tqTarget, Format( Q_MAX_NUMEMP, [ DatetoStrSQLC( dInicial ) ] ) );
        with tqTarget do
        begin
             Active := True;
             Result := FieldByName('CUANTOS').AsInteger;
        end;
   end;

begin
     Result := False;
     with Bitacora do
     begin
          AgregaResumen( StringOfChar( '*', 50 ) );
          AgregaResumen( GetTimeStamp + ': Consolidando ' + Empresa.Nombre );
     end;
     try
        with Empresa do
        begin
             OpenAlias( dbFuente, Alias, UserName, Password );
        end;
        //iNumEmpleados := ObtenerNumEmpleados;
        iAcumulaEmpresas := ObtenerNumEmpleados + iMaxEmpDBSource;
        if ( iMaxEmpleados >= iAcumulaEmpresas ) then
        begin
             lOk := LeeUnaTabla( 'COLABORA', '', 'CB_CODIGO' );
             if lOk then
                lOk := LeeUnaTabla( 'KARDEX', '', 'CB_CODIGO, CB_FECHA, CB_TIPO' );
             if lOk then
                lOk := LeeUnaTabla( 'KAR_FIJA', '', 'CB_CODIGO, CB_FECHA, CB_TIPO, KF_FOLIO' );
             if lOk then
                lOk := LeeUnaTabla( 'INCAPACI', '', 'CB_CODIGO, IN_FEC_INI' );
             if lOk then
                lOk := LeeUnaTabla( 'VACACION', '', 'CB_CODIGO, VA_FEC_INI, VA_TIPO' );
             if lOk then
                lOk := LeeUnaTabla( 'PERMISO', '', 'CB_CODIGO, PM_FEC_INI' );
             if lOk then
                lOk := LeeUnaTabla( 'AUSENCIA', Format( '( AU_FECHA >= %s ) and ( AU_FECHA <= %s )', [ DateToStrSQLC( dInicial ), DateToStrSQLC( dFinal ) ] ), 'CB_CODIGO, AU_FECHA' );
             if lOk then
                lOk := MergeTable( 'CONCEPTO', 'CO_NUMERO', '', 'CO_NUMERO' );
             if lOk then
                lOk := LeeUnaTabla( 'ACUMULA', Format( '( AC_YEAR = %d )', [ iYear ] ), 'AC_YEAR, CB_CODIGO, CO_NUMERO' );
             //Result := True;
             Result := lOk;
        end
        else
        begin
             Bitacora.AgregaError( Format( 'Intentando proceso para %d Empleados.' + CR_LF + 'Se excedi� el l�mite de %d Empleados Autorizados', [ iAcumulaEmpresas, iMaxEmpleados ] ) );
        end;
     except
           on Error: Exception do
           begin
                SetErrorFound( True );
                Bitacora.HandleException( 'ERROR al Abrir Empresa A Consolidar', Error );
                Result := False;
           end;
     end;
     CloseDatabase( dbFuente );
end;

{ ********** Configuraci�n de Bases de Datos ********** }

function TdmReportes.GetConfigureBDSteps: Integer;
begin
     Result := FTxCounter;
end;

procedure TdmReportes.ConfiguraBaseDeDatosMsg( const sMsg: String );
begin
     Bitacora.AgregaMensaje( sMsg );
end;

procedure TdmReportes.ConfiguraBaseDeDatosError( const sMsg: String; Error: Exception );
begin
     Bitacora.HandleException( sMsg, Error );
     SetErrorFound( True );
end;

{$ifdef MSSQL}
procedure TdmReportes.TransformaScriptMSSQL(var  sScript : string ; const sServer, sDatabaseEmpresa, sDatabaseComparte : string );
begin
   if Pos( K_NOMBRE_COMPARTE, sScript ) > 0 then
   begin
        if ( Pos( UpperCase( sServer ), UpperCase( sDatabaseEmpresa ) ) > 0 ) then
            sScript := StrTransAll( sScript, K_NOMBRE_COMPARTE, sDatabaseComparte )
        else
            sScript := StrTransAll( sScript, K_NOMBRE_COMPARTE, Comilla + sServer + Comilla + K_PUNTO + sDatabaseComparte );
   end;

   if Pos( K_NOMBRE_EMPRESA, sScript ) > 0 then
   begin
        sScript := StrTransAll( sScript, K_NOMBRE_EMPRESA, dmDBConfig.GetCompany );
   end;

   if Pos( K_DIGITO_EMPRESA, sScript ) > 0 then
   begin
        sScript := StrTransAll( sScript,K_DIGITO_EMPRESA, EntreComillas(dmDBConfig.GetDigito) );
   end;
end;
{$endif}

function TdmReportes.ConfiguraBaseDeDatos( const sFileName, sDatabase: String; const iTipo: Integer ): Boolean;
var
   lOk: Boolean;
   sValor: String;
   {$ifdef MSSQL}
   oListaScript: TStringList;
   sScript: String;
   {$endif}
   sServer, sDatabaseComparte: String;
begin
     with Bitacora do
     begin
          AgregaResumen( 'Configuraci�n de Base De Datos' );
          AgregaResumen( 'Base de Datos: ' + sDatabase );
          AgregaResumen( 'Archivo de Scripts: ' + sFileName );
          AgregaResumen( GetTimeStamp + ': Iniciando Configuraci�n' );
     end;
     lOk := True;
     sValor := ZetaTressCFGTools.SetFileNameDefaultPath( 'Script.sql' );
     CloseDBSourceShared;
     SourceSharedPath := ExtractFilePath( sFileName );
     OpenDBSourceShared;
     PreparaQueryParadox( tqSource, Format( 'select COUNT(*) from %s where ( SE_TIPO = %d )', [ ExtractFileName( sFileName ), iTipo ] ) );
     ZetaServerTools.GetServerDatabase( Registry.Database, sServer, sDatabaseComparte );
     with tqSource do
     begin
          Active := True;
          FTxCounter := Fields[ 0 ].AsInteger;
          Active := False;
     end;
     if ( FTxCounter > 0 ) then
     begin
          ClearErrorFound;
          DoStartCallBack( 'Iniciando' );
          PreparaQueryParadox( tqSource, Format( 'select SE_NOMBRE, SE_DESCRIP, SE_SEQ_NUM, SE_DATA from %s where ( SE_TIPO = %d ) order by SE_SEQ_NUM', [ ExtractFileName( sFileName ), iTipo ] ) );
          with tqSource do
          begin
               Active := True;
               while not Eof and lOk do
               begin
                    //* Las siguientes lineas es para cambiar el #COMPARTE y #EMPRESA por los codigos correspondientes *//
                    //* Solo aplica para el TressCFG de SQL Server *//
                    {$ifdef MSSQL}
                    sScript := FieldByName( 'SE_DATA' ).AsString;
                    if Pos( K_NOMBRE_COMPARTE, sScript ) > 0 then
                    begin
                         if ( Pos( UpperCase( sServer ), UpperCase( sDatabase ) ) > 0 ) then
                             sScript := StrTransAll( sScript, K_NOMBRE_COMPARTE, sDatabaseComparte )
                         else
                             sScript := StrTransAll( sScript, K_NOMBRE_COMPARTE, Comilla + sServer + Comilla + K_PUNTO + sDatabaseComparte );
                    end;

                    if Pos( K_NOMBRE_EMPRESA, sScript ) > 0 then
                    begin
                         sScript := StrTransAll( sScript, K_NOMBRE_EMPRESA, dmDBConfig.GetCompany );
                    end;

                    if Pos( K_DIGITO_EMPRESA, sScript ) > 0 then
                    begin
                         sScript := StrTransAll( sScript,K_DIGITO_EMPRESA, EntreComillas(dmDBConfig.GetDigito) );
                    end;

                    if ( sScript <> FieldByName( 'SE_DATA' ).AsString ) then
                    begin
                         oListaScript := TStringList.Create;
                         try
                            oListaScript.Text := sScript;
                            oListaScript.SaveToFile( sValor );
                         finally
                                FreeAndNil( oListaScript );
                         end;
                    end
                    else
                    {$endif}
                        TMemoField( FieldByName( 'SE_DATA' ) ).SaveToFile( sValor );
                    FVerActual := FieldByName( 'SE_DESCRIP' ).AsString;
                    ItemName := sValor;
                    DoCallBack( Self, lOk );
                    Next;
               end;
               Active := False;
          end;
          lOk := DoEndCallBack;
     end
     else
         DatabaseError( 'No Hay Scripts Para Ejecutar' );
     Result := not ErrorFound;
     with Bitacora do
     begin
          sValor := GetTimeStamp + ': Configuraci�n Terminada %s';
          if lOk and not ErrorFound then
             AgregaResumen( Format( sValor, [ 'Con Exito' ] ) )
          else
              AgregaResumen( Format( sValor, [ 'CON ERRORES' ] ) );
     end;
end;

{ ********** Inicializar Datos De Una Base De Datos ********** }

function TdmReportes.InicializaBaseDeDatos( ListaTablas: TStrings; const lEsComparte: Boolean ): Boolean;
var
   i, j: Integer;
   lOk: Boolean;
   sValor: String;

procedure SwapPositions( const sParentTable, sChildTable: String );
var
   i, j: Integer;
begin
     with ListaTablas do
     begin
          i := IndexOf( sParentTable );
          j := IndexOf( sChildTable );
          if ( i >= 0 ) and ( j >= 0 ) and ( i > j ) then
             Exchange( i, j );
     end;
end;

function GetBaseDeDatos: TDataBase;
begin
     if ( lEsComparte ) then
     begin
          Result:= dmDBConfig.dbEmpresas;
     end
     else
         Result:= dbTarget;
end;

begin
     with Bitacora do
     begin
          AgregaResumen( 'Inicializaci�n de Base De Datos' );
          AgregaResumen( 'Base de Datos: ' + TargetAlias );
          AgregaResumen( 'Directorio de Archivos Fuente: ' + SourceSharedPath );
          AgregaResumen( GetTimeStamp + ': Empezando Inicializaci�n' );
     end;
     lOk := True;
     ClearErrorFound;
     DoStartCallBack( 'Iniciando' );
     PreparaQuery( tqTarget, '', GetBaseDeDatos );



     with ListaTablas do
     begin
          if ( lEsComparte ) then
          begin
               {$ifdef MSSQL}
               {ORDEN:
               'K_ESTILO'
               'K_SCREEN'
               'K_BOTON'
               'K_SHOW
               'K_SHOWINF
                }
               SwapPositions('KESTILO.DB','KSCREEN.DB');
               SwapPositions('KESTILO.DB','KBOTON.DB');
               SwapPositions('KESTILO.DB','KSHOW.DB');
               SwapPositions('KSCREEN.DB','KBOTON.DB');
               SwapPositions('KSCREEN.DB','KSHOW.DB');
               SwapPositions('KSHOW.DB', 'KSHOWINF.DB');
               {$endif}
          end
          else
          begin
               {$ifdef MSSQL}
               SwapPositions( 'R_CLASIFI.DB', 'R_CLAS_ENT.DB' );
               SwapPositions( 'R_CLASIFI.DB', 'CAMPOREP.DB' );
               SwapPositions( 'R_CLASIFI.DB', 'REPORTE.DB' );
               SwapPositions( 'R_MODULO.DB', 'R_MOD_ENT.DB' );
               SwapPositions( 'R_LISTAVAL.DB', 'R_VALOR.DB' );
               SwapPositions( 'R_LISTAVAL.DB', 'R_ORDEN.DB' );
               SwapPositions( 'R_LISTAVAL.DB', 'R_FILTRO.DB' );
               SwapPositions( 'R_LISTAVAL.DB', 'R_DEFAULT.DB' );
               SwapPositions( 'R_LISTAVAL.DB', 'R_ATRIBUTO.DB' );
               SwapPositions( 'R_ENTIDAD.DB', 'R_ORDEN.DB' );
               SwapPositions( 'R_ENTIDAD.DB', 'R_FILTRO.DB' );
               SwapPositions( 'R_ENTIDAD.DB', 'R_DEFAULT.DB' );
               SwapPositions( 'R_ENTIDAD.DB', 'R_ATRIBUTO.DB' );
               SwapPositions( 'R_ENTIDAD.DB', 'R_CLAS_ENT.DB' );
               SwapPositions( 'R_ENTIDAD.DB', 'R_RELACION.DB' );
               SwapPositions( 'R_ENTIDAD.DB', 'R_MOD_ENT.DB' );
               SwapPositions( 'R_ENTIDAD.DB', 'CAMPOREP.DB' );
               SwapPositions( 'R_ENTIDAD.DB', 'REPORTE.DB' );
               SwapPositions( 'R_ATRIBUTO.DB', 'R_ORDEN.DB' );
               SwapPositions( 'R_ATRIBUTO.DB', 'R_FILTRO.DB' );
               SwapPositions( 'R_ATRIBUTO.DB', 'R_DEFAULT.DB' );
               {$endif}
               SwapPositions( 'REPORTE.DB', 'CAMPOREP.DB' );
               SwapPositions( 'NUMERICA.DB', 'T_ART_80.DB' );
               SwapPositions( 'NUMERICA.DB', 'ART_80.DB' );
               SwapPositions( 'T_ART_80.DB', 'ART_80.DB' );
               SwapPositions( 'TURNO.DB', 'FESTIVO.DB' );
               SwapPositions( 'SSOCIAL.DB', 'PRESTACI.DB' );
               SwapPositions( 'RSOCIAL.DB', 'RPATRON.DB' );
          end;

          for i := 0 to ( Count - 1 ) do
          begin
               ItemName := Strings[ i ];
               sValor := ChangeFileExt( ItemName, '' );
               try
                  if AbreArchivoParadox( ItemName ) then
                  begin
                       with GetBaseDeDatos do
                       begin
                            StartTransaction;
                       end;
                       try
                          with ttSource do
                          begin
                               with FLista1 do
                               begin
                                    Clear;
                                    for j := 0 to ( FieldCount - 1 ) do
                                    begin
                                         Add( Fields[ j ].FieldName );
                                    end;
                               end;
                               CreateInsertScript( sValor, tqTarget, GetBaseDeDatos );
                               First;
                               while not Eof do
                               begin
                                    try
                                       ZetaTressCFGTools.TransferDatasets( ttSource, tqTarget );

                                       tqTarget.ExecSQL;
                                    except
                                          on Error: Exception do
                                          begin
                                               //MA:25-Mar-04: Se agreg� esta validaci�n ya que al Crear la estructura de la BD. (DB_MSSQSL.DB)
                                               //              se agrega el global 133 que es el que contiene la version de la BD.
                                               if( not( ( (sValor = 'GLOBAL' ) or (sValor = 'ACC_DER') OR (sValor = 'ACC_ARBOL') )
                                                   and ( PK_VIOLATION( Error ) ) ) )then
                                               begin
                                                    Bitacora.HandleException( Format( 'Error Al Inicializar Datos de Tabla %s', [ ItemName ] ), Error );
                                                    SetErrorFound( True );
                                               end;
                                          end;
                                    end;
                                    Next;
                               end;
                          end;
                       finally
                              with GetBaseDeDatos do
                              begin
                                   Commit;
                              end;
                       end;
                  end;
               except
                     on Error: Exception do
                     begin
                          Bitacora.HandleException( Format( 'Error Al Inicializar Tabla %s', [ ItemName ] ), Error );
                          SetErrorFound( True );
                     end;
               end;
               DoCallBack( Self, lOk );
               if not lOk then
                  Break;
          end;
     end;
     lOk := DoEndCallBack;
     Result := not ErrorFound;
     with Bitacora do
     begin
          sValor := GetTimeStamp + ': Inicializaci�n Terminada %s';
          if lOk and not ErrorFound then
             AgregaResumen( Format( sValor, [ 'Con Exito' ] ) )
          else
              AgregaResumen( Format( sValor, [ 'CON ERRORES' ] ) );
     end;
end;

function TdmReportes.PK_Violation( Error: Exception ): Boolean;
begin
    Result := DMigrar.PK_Violation(Error);
end;

function TdmReportes.GetListaClasifiEmpresa(oLista: TStrings): Boolean;
const
     K_MEN_SIN_RESTRICCION = 'Sin Restricci�n';
     crSinRestriccion = MaxInt;
begin
     Result:= TRUE;
     with oLista do
     begin
          Clear;
          AddObject( K_MEN_SIN_RESTRICCION , TObject( crSinRestriccion ));
     end;

     try
        with dmDBConfig do
        begin
             TargetAlias:= GetCompanyAlias;
             TargetUserName:= GetCompanyUserName;
             TargetPassword:= GetCompanyPassword;

             if ( tqCompany.State <> dsInsert  ) and ( eTipoCompany( tqCompany.FieldByName('CM_TIPO').AsInteger ) = tc3Datos ) then
             begin
                  OpenDBTarget;
                  PreparaQuery( tqTarget, 'select RC_CODIGO, RC_ORDEN, RC_NOMBRE from R_CLASIFI order by RC_ORDEN' );
                  try
                     with oLista do
                     begin
                          with tqTarget do
                          begin
                               Active:= True;
                               First;
                               Result:= not IsEmpty;
                               if Result then
                               begin
                                    while NOT EOF do
                                    begin
                                         oLista.AddObject( FieldByName('RC_NOMBRE').AsString, TObject( FieldByName('RC_CODIGO').AsInteger ) );
                                         Next;
                                    end;
                               end;
                          end;
                     end;
                  finally
                         DespreparaQuery( tqTarget );
                         CloseDBTarget;
                  end;
             end;
        end;
     except
           on Error: Exception do
           begin
                {No es deseable que muestre el error de que no pudo conectar la empresa.
                 Va a dejar el combo vac�o, solamente con el index de sin restricci�n}
           end;
     end;
end;

function TdmReportes.GetPosClasifiEmp( oLista: TStrings; const eTipo: eClasifiReporte ):Integer;
 var
    i: integer;
begin
     Result := -1;
     for i:= 0 to oLista.Count -1 do
     begin
          if eClasifiReporte( oLista.Objects[i] ) = eTipo then
          begin
               Result := i;
               Break;
          end;
     end;
end;

function TdmReportes.GetTipoBitacora(const sMensaje: String): eTipoBitacora;
begin
     if ( Pos ( K_OBJETO_EXISTE, UpperCase( sMensaje ) ) > 0 ) or
     {$ifdef INTERBASE }
        ( Pos ( K_EXISTE_GRAL, UpperCase( sMensaje ) ) > 0 )
     {$else}
        ( Pos ( K_PK_EXISTE, UpperCase( sMensaje ) ) > 0  ) or
        ( Pos ( K_FK_EXISTE, UpperCase( sMensaje ) )  > 0 ) or
        ( Pos ( K_COLUMNA_EXISTE, UpperCase( sMensaje ) )  > 0 ) {$endif} then
        Result:= tbAdvertencia
     else
         Result:= tbError
end;

function TdmReportes.EvaluaParametros(oSQLAgente: TSQLAgenteClient;
  Parametros: TStrings; var sError: wideString; var oParams: OleVariant;
  const lMuestraDialogo: Boolean): Boolean;
begin
     Result := False;
end;





function TdmReportes.ImportarReportesRP1(sDirectorioReportes : string; listaReportes : TStringList;   lImportarPlantilla,  lSustituirPlantilla, lContinuarNoExistePlantilla: boolean) : integer;
const K_ERROR = 'ERROR';
       K_NO_VALIDO = 'El archivo de importaci�n no es v�lido';
       K_IMPORTA = 'Importaci�n de Reportes';

var
   iNuevo, iViejo, iReporte, iExitos: Integer;
   lLoop: Boolean;
   FDirectorioPlantillas: String;
   cdsReportes: TServerDataSet;
   cdsCamposRep: TServerDataSet;
   dsReportes, dsCamposRep : TDataSet;
   oEncabezado,oImpReporte : TStringList;



function GetNewCodigo( var iFolio: Integer ): Boolean;
begin
     Result := False;
     try
        PreparaQuery( tqTarget, 'select MAX( RE_CODIGO ) from REPORTE' );
        with tqTarget do
        begin
             Active := True;
             if IsEmpty then
                iFolio := 1
             else
                 iFolio := Fields[ 0 ].AsInteger + 1;
             Active := False;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                SetErrorFound( True );
                Bitacora.HandleException( 'Error Al Determinar C�digo De Reportes Importados', Error );
           end;
     end;
end;

function ImportaCampos( const iViejo, iNuevo: Integer; var lLoop: Boolean ): Boolean;
begin
     Result := False;
     with dsCamposRep do
     begin
          try
             Filtered := False;
             Filter := 'RE_CODIGO = ' + IntToStr( iViejo );
             Filtered := True;
             First;
             while not Eof and lLoop do
             begin
                  try
                     TransferDatasets( dsCamposRep, tqTarget ); // CAMPOREP.DBF -> CAMPOREP //
                     with tqTarget do
                     begin
                          ParamByName( 'RE_CODIGO' ).AsInteger := iNuevo;
                          ExecSQL;
                     end;
                  except
                        on Error: Exception do
                        begin
                             SetErrorFound( True );
                             Bitacora.HandleException( 'Error Al Transferir Campos De Reporte ' +  listaReportes[iViejo-1], Error );
                        end;
                  end;
                  lLoop := DoEndCallBack;
                  Next;
             end;
             Result := True;
          except
                on Error: Exception do
                begin
                     //SetErrorFound( True );
                     Bitacora.HandleException( 'Error Al Filtrar Campos De Reporte ' + listaReportes[iViejo-1] , Error );
                end;
          end;
     end;
end;

function ObtieneEntidad( const Index: TipoEntidad ): String;
begin
     {$ifdef RDDTRESSCFG}
     {$else}
     Result := ZetaTipoEntidadTools.ObtieneEntidad( TipoEntidad( Index ) );
     {$endif}

end;

function ObtieneClasificacion( const Index: eClasifiReporte ): String;
begin
     {$ifdef RDDTRESSCFG}
     {$else}
     Result := ObtieneElemento(lfClasifiReporte,Ord(Index) );
     {$endif}
end;

function ParseReporteRP1( sArchivo : string; iReporte :integer)  : boolean;
var i{ j {, iReporte} : integer;
      sFuente, sNombreReporte: string;
      lExistePlantilla : Boolean;
      oDatosImport : TDatosImportReporte;
      iCount : integer;


      function LeeSeccion( var i : integer;
                      DataSet : TDataSet ): Boolean;
  var sRenglon,sField,sFormat,sValue : string;
      IgualPos : integer;

  function EsCorchete : Boolean;
  begin
       Result := Copy(oImpReporte[i],1,1)='[' ;
  end;
  function QuitaComillas : string;
  begin
       Result := sValue;
       if Copy(Result,1,1) = UnaCOMILLA then
       begin
            Result := Copy(Result,2,Length(Result));
            if Copy(Result,Length(Result),1) = UnaCOMILLA then
               Result := Copy(Result,1,Length(Result)-1);
       end;
  end;
  var oField : TField;
 begin
      Result := TRUE;
      if EsCorchete then
         i := i+1;

      while (i<iCount) AND (NOT EsCorchete) do
      begin
           with DataSet do
           begin
                sRenglon := oImpReporte[i];
                if StrLleno(sRenglon) then
                begin
                     IgualPos := Pos('=',sRenglon);
                     sField := Copy(sRenglon,1,IgualPos-1);
                     sValue := Copy(sRenglon,IgualPos+1,Length(sRenglon));
                     oField := FindField(sField);
                     if oField <> NIL then
                     begin
                          with oField do
                          begin
                               if DataType in [ftDate, ftDateTime] then
                               try
                                  sFormat := ShortDateFormat;
                                  ShortDateFormat := 'mm/dd/yyyy';
                                  AsString := sValue;
                               finally
                                      ShortDateFormat:=sFormat;
                               end
                               else if oField is TNumericField then
                                    AsString := sValue
                               else AsString := QuitaComillas;
                          end;
                     end
                     else Result := FALSE;
                end;
                i := i+1;
           end;
      end;
 end;


function GetPlantilla(const Value: string) : string;
begin

     if ( Value = '' ) OR (Value=(K_TEMPLATE+'.QR2')) then
     begin
          lImportarPlantilla := False;
          Result := K_TEMPLATE+'.QR2';
     end
     else if Value = K_NO_REQUIERE then
     begin
          lImportarPlantilla := False;
          Result :=  Value;
     end
     else
     begin
          lImportarPlantilla := True;
          Result := Value;
     end;
end;


 function ImportaPlantilla : Boolean;
  var sFuente, sDestino : string;
 begin
      Result := TRUE;
      if lImportarPlantilla then
      begin
           sFuente := VerificaDir(ExtractFilePath(sArchivo));
           sDestino := FDirectorioPlantillas;

           if sFuente <> sDestino then
           begin
                sFuente := sFuente + oDatosImport.Plantilla;
                sDestino := sDestino + oDatosImport.Plantilla;
                if FileExists(sDestino) then
                begin
                   if lSustituirPlantilla then
                      CopyFile(PChar(sFuente),PChar(sDestino),FALSE)
                   else
                       Result := FALSE;
                end
                else
                    CopyFile(PChar(sFuente),PChar(sDestino),FALSE);
           end;
      end;
 end;

begin

     oImpReporte := TStringList.Create;
     oEncabezado := TStringList.Create;
     try
        oImpReporte.LoadFromFile(sArchivo);
        iCount := oImpReporte.Count;
        if Pos('TOTAL=',UpperCase(oImpReporte[iCount-1]))>0 then
           oImpReporte.Delete(iCount-1);
        iCount := oImpReporte.Count;
        if Pos('[TOTAL_DETALLE]',UpperCase(oImpReporte[iCount-1]))>0  then
           oImpReporte.Delete(iCount-1);
        iCount := oImpReporte.Count;
        oDatosImport := TDatosImportReporte.Create;

        if oImpReporte[0] = K_GENERAL then i :=1
        else i :=0;

        while oImpReporte[i] <> K_REPORTE DO
        begin
             oEncabezado.Add(oImpReporte[i]);
             i := i+1;
        end;
        oEncabezado.Add('RE_CLASIFI='+oImpReporte.Values['RE_CLASIFI']);
        with oEncabezado do
        begin
             sNombreReporte := Values['NOMBRE'];

             if StrVacio( sNombreReporte ) then
             begin
                  Result := FALSE;
                  Bitacora.AgregaError (  K_NO_VALIDO ) ;
                  Exit;
             end;
             with oDatosImport do
             begin
                  Empresa:= Values['EMPRESA'];
                  Usuario := Values['USUARIO'];
                  Fecha := Values['FECHA'];
                  Nombre := sNombreReporte;
                  Tipo := ObtieneElemento(lfTipoReporte,StrToInt(Values['TIPO']));
                  Tabla := ObtieneEntidad(TipoEntidad(StrToInt(Values['TABLA'])));
                  NombreNuevo := sNombreReporte;
                  Plantilla := Values['PLANTILLA'];
                  Clasificacion := eClasifiReporte(StrToInt(Values['RE_CLASIFI']));
                  Result := True
             end;
        end;

        oDatosImport.ImportarPlantilla := lImportarPlantilla;

        if ( oDatosImport.ImportarPlantilla ) AND
           ( oDatosImport.Plantilla <> K_NO_REQUIERE )  AND
           ( oDatosImport.Plantilla <> K_TEMPLATE+'.QR2' ) then
        begin
             sFuente := VerificaDir(ExtractFilePath(sArchivo));
             lExistePlantilla := FileExists( sFuente + oDatosImport.Plantilla);

             // Si no existe plantilla
             if NOT lExistePlantilla then
             begin

                  Result := lContinuarNoExistePlantilla; //zConfirm( K_IMPORTA, 'La Plantilla  ' + Comillas(sFuente + oDatosImport.Plantilla) + ' No Existe,'+CR_LF+  '�Desea Continuar con la Importaci�n? ',0,mbNo );
                  if (  Result ) then
                        Bitacora.AgregaMensaje(  Format( 'La Plantilla %s no existe en el directorio fuente. El reporte se importar� sin plantilla', [oDatosImport.Plantilla] ) );
             end
             else
                ImportaPlantilla;
        end;

        if Result then
        begin
             with dsReportes do
             begin
                  Append;
                  LeeSeccion(i,dsReportes);
                  if Result then
                  begin
                       FieldByName('RE_CODIGO').AsInteger := iReporte;
                       FieldByName('RE_NOMBRE').AsString := oDatosImport.NombreNuevo;
                       FieldByName('RE_FECHA').AsDateTime := DATE;
                       FieldByName('US_CODIGO').AsInteger := 1;
                       // ExisteClasificacion(oDatosImport.Clasificacion) then
                       FieldByName('RE_CLASIFI').AsInteger := Ord(oDatosImport.Clasificacion);
                       while i < iCount do
                       begin
                            dsCamposRep.Append;
                            if  LeeSeccion(i,dsCamposRep) then
                            begin
                               dsCamposRep.FieldByName( 'RE_CODIGO').AsInteger := iReporte;
                               dsCamposRep.Post;
                            end
                            else
                                dsCamposRep.Cancel;
                       end;

                       Post;
                  end;
             end
        end;
     finally
            FreeAndNil( oEncabezado );
            FreeAndNil( oImpReporte );
     end;
end;

    procedure PrepararQuerys;
    begin
          PreparaQuery( tqSource, '' );
          PreparaQuery( tqTarget, '' );
          PreparaQuery( tqReporte, 'select * from REPORTE where RE_CODIGO = 0' );
          PreparaQuery( tqCamposRep, 'select * from CAMPOREP where RE_CODIGO = 0' );
          tqReporte.Active := True;
          tqCamposRep.Active := True;
          dsReportes := TDataSet( tqReporte );
          dsCamposRep := TDataSet( tqCamposRep );
    end;


    procedure AbrirDataSetsReportes;
    begin
          cdsReportes :=  TServerDataSet.Create( Self );
          cdsReportes.FieldDefs.Assign( TDataSet( tqReporte ).FieldDefs );
          cdsReportes.CreateDataSet;
          dsReportes := cdsReportes;

          cdsCamposRep :=  TServerDataSet.Create( Self );
          cdsCamposRep.FieldDefs.Assign( TDataSet( tqCamposRep ).FieldDefs );
          cdsCamposRep.CreateDataSet;
          dsCamposRep := cdsCamposRep;
    end;

    procedure DesprepararQuerys;
    begin
         DespreparaQuery( tqSource );
         DespreparaQuery( tqTarget );
         tqReporte.Active := False;
         tqCamposRep.Active := False;
         DespreparaQuery( tqReporte );
         DespreparaQuery( tqCamposRep );
         FreeAndNil( cdsReportes );
         FreeAndNil( cdsCamposRep );
    end;

const
     K_IMPORTA_REP_EXITO  = 0;
     K_IMPORTA_REP_ERROR = -1;
     K_IMPORTA_REP_ADVERTENCIAS = 1;

begin
     iExitos := 0;
     iNuevo := 0;
     lLoop := True;
     ClearErrorFound;
     with Bitacora do
     begin
          AgregaResumen( 'Importaci�n de Reportes' );
          AgregaResumen( 'Base de Datos Receptora: ' + TargetAlias );
          AgregaResumen( 'Directorio Donde Residen Los Reportes a Importar: ' + sDirectorioReportes );
          AgregaResumen( GetTimeStamp + ': Iniciando Importaci�n De Reportes' );
     end;
     if AbreBDReceptora and GetNewCodigo( iNuevo ) then
     begin
          FDirectorioPlantillas := GetGlobalDirectorioPlantillas;
          PrepararQuerys;
          AbrirDataSetsReportes;

          try
             // En este punto deber� agregarse los nuevos reportes y sus respectivos campos

              for iReporte := 0 to listaReportes.Count-1 do
              begin
                   Bitacora.AgregaMensaje( 'Reporte a Importar: '+ listaReportes[iReporte] );
                   ParseReporteRP1( IncludeTrailingBackslash( sDirectorioReportes ) + listaReportes[iReporte], iReporte+1);
              end;


             ZetaTressCFGTools.SetInsertScript( 'REPORTE', dsReportes, tqSource );
             ZetaTressCFGTools.SetInsertScript( 'CAMPOREP', dsCamposRep, tqTarget );

             with dsReportes do
             begin
                  First;
                  iViejo := 1;
                  while not Eof and lLoop do
                  begin
                       EmpezarTransaccion;
                       try
                          TransferDatasets( dsReportes, tqSource );
                          with tqSource do
                          begin
                               with ParamByName( 'RE_CODIGO' ) do
                               begin
                                    iViejo := AsInteger;
                                    AsInteger := iNuevo;
                               end;
                               with ParamByName( 'RE_NOMBRE' ) do
                               begin
                                    AsString := Copy( AsString, 1, 30 );
                               end;
                               ExecSQL;
                          end;
                          lLoop := DoEndCallBack;
                          if lLoop then
                          begin
                               ImportaCampos( iViejo, iNuevo, lLoop );
                               if lLoop then
                               begin
                                  Bitacora.AgregaMensaje( 'Reporte ' + dsReportes.FieldByName( 'RE_NOMBRE' ).AsString  + ' Importado Como ' + IntToStr( iNuevo ) );
                                  Inc(iExitos)
                               end;

                          end;
                          TerminarTransaccion( lLoop );
                       except
                             on Error: Exception do
                             begin
                                  TerminarTransaccion( False );
                                  //SetErrorFound( True );
                                  Bitacora.HandleException( 'No Fue Posible Importar Reporte  ' +listaReportes[iViejo-1], Error );
                             end;
                       end;
                       iNuevo := iNuevo + 1;
                       Next;
                  end;
             end;
          finally
                 DesprepararQuerys;
          end;

     end;


     if ( iExitos =   listaReportes.Count ) then
        Result := K_IMPORTA_REP_EXITO
     else
        Result := K_IMPORTA_REP_ADVERTENCIAS
end;



function TdmReportes.GetGlobalDirectorioPlantillas: String;
const
     K_GLOBAL_DIR_PLANT  = 61;
     K_DIR_DEFAULT = '';
begin
     Result := K_DIR_DEFAULT;

     if ( CompanyType = tc3Datos ) then
     begin
         PreparaQuery( tqTarget, Format( 'select GL_FORMULA from GLOBAL where ( GL_CODIGO = %d )', [ K_GLOBAL_DIR_PLANT ] ) );
         with tqTarget do
         begin
              Active := True;
              if not IsEmpty then
                 Result:= VerificaDir ( FieldByName( 'GL_FORMULA' ).AsString );
              Active := False;
         end;
     end;
end;

function TdmReportes.GetGlobalConservarImagen: Boolean;
const
     K_GLOBAL_CONSERVAR_TAM_FOTOS      = 229;
     K_CONSERVAR_DEFAULT = False;
begin
     Result := K_CONSERVAR_DEFAULT;

     if ( CompanyType = tc3Datos ) then
     begin
         PreparaQuery( tqTarget, Format( 'select GL_FORMULA from GLOBAL where ( GL_CODIGO = %d )', [ K_GLOBAL_CONSERVAR_TAM_FOTOS ] ) );
         with tqTarget do
         begin
              Active := True;
              if not IsEmpty then
                 Result:= zStrToBool( FieldByName( 'GL_FORMULA' ).AsString );
              Active := False;
         end;
     end;
end;



function TdmReportes.CrearTablaEspeciales(var nProcedures : integer; const BaseDatos : TDataBase) : Boolean;
 var
    lExisteTabla : boolean;
begin
     nProcedures := 0;
     try
          with BaseDatos do
          begin
               AttachQuery( BaseDatos, tqTarget, Q_EXISTE_TABLA_SP_ESPECIALES );
               tqTarget.Active := TRUE;
               with tqTarget do
               begin
                    lExisteTabla := FieldByName('CUANTOS').AsInteger > 0 ;
               end;
               tqTarget.Active := FALSE;

               if not lExisteTabla then
               begin
                    StartTransaction;
                    try
                       AttachQuery( BaseDatos, tqTarget, Q_CREA_TABLA_SP_ESPECIALES );
                       tqTarget.ExecSQL;
                       AttachQuery( BaseDatos, tqTarget, Q_CREA_TABLA_SP_ESPECIALES_PK );
                       tqTarget.ExecSQL;
                       AttachQuery( BaseDatos, tqTarget, Q_CREA_TD_COMPANY_SP_PATCH );
                       tqTarget.ExecSQL;
                       AttachQuery( BaseDatos, tqTarget, Q_CREA_TU_COMPANY_SP_PATCH );
                       tqTarget.ExecSQL;
                       Commit;
                       lExisteTabla := True;
                    except
                          on Error: Exception do
                          begin
                               Rollback;
                               lExisteTabla := ( GetTipoBitacora( Error.Message ) = tbAdvertencia );
                          end;
                    end;
                    tqTarget.Active := FALSE;
               end;

               if ( lExisteTabla ) then
               begin
                    AttachQuery( BaseDatos, tqTarget, Q_SP_PATCH_COMPARTE_EXISTE );
                    tqTarget.Active := TRUE;

                    with tqTarget do
                    begin
                         nProcedures := FieldByName('CUANTOS').AsInteger;
                    end;
                    Result :=TRUE;
               end
               else
                   Result := FALSE;

                tqTarget.Active := FALSE;
          end;
     except
           on Error: Exception do
           begin
                Result := False;
                //( 'ERROR Al Intentar Leer la Tabla de SP Especiales', Error );
           end;
     end;
end;

function TdmReportes.AbreSPExtras: Boolean;
var
   nSPs : integer;
   BaseDatos : TDataBase;



begin
    try
       with dmDBConfig do
       begin
            //ConectarComparte;
            BaseDatos := dbEmpresas;
       end;

       BaseDatos.Connected := True;

       Result := CrearTablaEspeciales( nSPs, BaseDatos );

       if ( Result ) then
       begin
           //Si no tiene procedures y existen un archivo Importa la informaci�n a la Tabla Si el Usuario lo desea
           if ( nSPs = 0 ) then
           begin
              if FileExists( GetSPFileName('DB') ) then
              begin
                 if ZConfirm( 'Importaci�n de SP Extras', '� Desea Importar los Stored Procedures Extras en '+CR_LF+ GetSPFileName('DB') + '?', 0, mbYes) then
                 begin
                    if AbreArchivoSPExtras then
                          ImportarArchivoSPExtras( BaseDatos);
                 end;
              end;
           end;

       end;

    except
          on Error: Exception do
          begin
             Result := False;
          end;
    end;

    if ( Result ) then
    begin
           with dmDBConfig do
           begin
                tqSPPatch.Active := False;
                tqSPPatch.CachedUpdates := True;
                tqSPPatch.Active := True;
           end;
    end;


end;

function TdmReportes.ImportarArchivoSPExtras(const BaseDatos : TDataBase): Boolean;
var
     ms: TMemoryStream;

procedure BDEmpezarTransaccion;
begin
     with BaseDatos do
     begin
          if InTransaction then
             Commit;
          if not InTransaction then
             StartTransaction;
     end;
end;

procedure BDTerminarTransaccion( const lCommit: Boolean );
begin
     with BaseDatos do
     begin
          if InTransaction then
          begin
               if lCommit then
                  Commit
               else
                   RollBack;
          end;
     end;
end;



begin

     BaseDatos.Connected := True;
     PreparaQuery( tqTarget, '', BaseDatos );
     ZetaTressCFGTools.SetInsertScript( 'SP_PATCH', ttSource, tqTarget );

     with ttSource do
     begin
          DisableControls;
          BDEmpezarTransaccion;

          try
             First;
             ms := TMemoryStream.Create;
             while not Eof  do
             begin
                  TransferDatasets( ttSource, tqTarget );
                  tqTarget.ParamByName('SE_DATA').AsMemo := ttSource.FieldByName( 'SE_DATA').AsString;
                  tqTarget.ExecSQL;
                  Next;
             end;
             BDTerminarTransaccion( True );
             FreeAndNil( ms );
             Result := True;
          except
                on Error: Exception do
                begin
                     BDTerminarTransaccion( False );
                     Result := False;
                end;
          end;
          EnableControls;
    end;
end;


function TdmReportes.GetPatchSPDataSet: TDataSet;
begin
     Result :=  dmDBConfig.tqSPPatch;
end;

procedure  TdmReportes.RefrescaPatchSPDataSet;
begin
      dmDBConfig.tqSPPatch.Active := False;
      dmDBConfig.tqSPPatch.Active := True;
end;


function TdmReportes.GrabaSPPatch: boolean;
begin
     Result := dmDBConfig.GrabaSPPatch;
end;

function TdmReportes.BorraSPPatch: boolean;
begin
     Result := dmDBConfig.BorraSPPatch;
end;

function TdmReportes.AgregaSPPatch : boolean;
begin
      Result := dmDBConfig.AgregaSPPatch;
end;

function TdmReportes.LlenaListaCompaniasAplica(Lista: TStrings; const tcTipo : eTipoCompany ): Boolean;
begin
     Result := dmDBConfig.LlenaListaCompaniasAplica( Lista, tcTipo );
end;

end.
