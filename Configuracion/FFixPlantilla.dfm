object Form1: TForm1
  Left = 192
  Top = 116
  Width = 696
  Height = 480
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 64
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Load Report'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Bitacora: TListBox
    Left = 40
    Top = 88
    Width = 345
    Height = 321
    ItemHeight = 13
    TabOrder = 1
  end
  object Reporte: TDesignQuickReport
    Left = 520
    Top = 16
    Width = 816
    Height = 1056
    Frame.Color = clBlack
    Frame.DrawTop = False
    Frame.DrawBottom = False
    Frame.DrawLeft = False
    Frame.DrawRight = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    Functions.Strings = (
      'PAGENUMBER'
      'COLUMNNUMBER'
      'REPORTTITLE')
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    Options = [FirstPageHeader, LastPageFooter]
    Page.Columns = 1
    Page.Orientation = poPortrait
    Page.PaperSize = Letter
    Page.Values = (
      127
      2794
      127
      2159
      127
      127
      0)
    PrinterSettings.Copies = 1
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.OutputBin = Auto
    PrintIfEmpty = True
    SnapToGrid = True
    Units = Inches
    Zoom = 100
    DesignBackgroundDX = 0
    DesignBackgroundDY = 0
    object QRDesignBand1: TQRDesignBand
      Left = 48
      Top = 48
      Width = 720
      Height = 40
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      AlignToBottom = False
      Color = clWhite
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        105.833333333333
        1905)
      BandType = rbTitle
      EditorEnabled = True
      QRDesigner = FDesigner
      BandNum = 0
    end
  end
  object FDesigner: TQRepDesigner
    SaveLoadPrinterSetup = False
    UseDataModules = False
    Version = '1.22'
    QReport = Reporte
    ScriptsEnabled = True
    AddCursor = crDefault
    AllowBlockEdit = False
    AllowNewCalcFields = False
    AllowSQLEdit = True
    AutoEditAfterInsert = True
    BackupFileExtension = '.BAK'
    BandNameFont.Charset = DEFAULT_CHARSET
    BandNameFont.Color = clSilver
    BandNameFont.Height = 20
    BandNameFont.Name = 'MS Sans Serif'
    BandNameFont.Style = []
    ComponentFrameColor = clGray
    ComponentFrameStyle = psDot
    DefaultDatabaseOnly = False
    GridSizeX = 1
    GridSizeY = 1
    KeyboardMoveX = 1
    KeyboardMoveY = 1
    MinElementWidth = 5
    MinElementHeight = 5
    QueriesOnlyInExpert = False
    QueriesOnlyInDBSetup = False
    SaveBackupFiles = False
    SaveImagesInReportfile = False
    ScriptEditByUser = True
    ShowBandNames = True
    ShowComponentFrames = False
    ShowDatafieldListbox = True
    UndoEnabled = False
    DatafieldsSorted = False
    SQLSettings.DelimiterType = delNoQuotes
    SQLSettings.SQLStringDelimiterLeft = #39
    SQLSettings.SQLStringDelimiterRight = #39
    SQLSettings.SQLTableDelimiterLeft = #39
    SQLSettings.SQLTableDelimiterRight = #39
    SQLSettings.SQLDateDelimiterLeft = #39
    SQLSettings.SQLDateDelimiterRight = #39
    SQLSettings.SQLWildcard = '%'
    Left = 16
    Top = 8
  end
end
