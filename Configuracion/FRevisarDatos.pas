unit FRevisarDatos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, ComCtrls, Checklst, ExtCtrls, Buttons, FileCtrl, Mask,
     FMigrar,
     ZetaWizard,
     ZetaFecha, jpeg;

type
  TRevisarDatos = class(TMigrar)
    Empresas: TTabSheet;
    BaseDatosFuente: TGroupBox;
    ListaFuente: TListBox;
    PanelDatos: TPanel;
    NombreLBL: TLabel;
    Nombre: TLabel;
    DirectorioLBL: TLabel;
    Directorio: TLabel;
    TressDOS: TGroupBox;
    TressDOSPathSeek: TSpeedButton;
    TressDOSPathLBL: TLabel;
    TressDOSPath: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
    procedure WizardAfterMove(Sender: TObject);
    procedure ListaFuenteClick(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
  end;

var
  RevisarDatos: TRevisarDatos;

implementation

uses ZetaCommonTools,
     ZetaTressCFGTools,
     ZetaDialogo,
     ZetaCommonClasses,
     ZetaMigrar,
     FHelpContext,
     DMigrar,
     DMigrarDatos;

{$R *.DFM}

{ ********** TMigrarDatos ************ }

procedure TRevisarDatos.FormCreate(Sender: TObject);
begin
     FListaFuente := ListaFuente;
     FNombre := Nombre;
     FDirectorio := Directorio;
     FDOSPath := TressDOSPath;
     dmMigracion := TdmMigrar.Create( Self );
     LogFile.Text := ZetaTressCFGTools.SetFileNameDefaultPath( 'Revision.log' );
     inherited;
     HelpContext := H00002_Revisar_DOS;
end;

procedure TRevisarDatos.FormDestroy(Sender: TObject);
begin
     inherited;
     dmMigracion.Free;
end;

procedure TRevisarDatos.ListaFuenteClick(Sender: TObject);
begin
     inherited;
     SetControls;
end;

procedure TRevisarDatos.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     if CanMove then
     begin
          if Wizard.Adelante then
          begin
               if Wizard.EsPaginaActual( DirectorioDOS ) then
                  CanMove := ValidaDirectorioDOS and LlenaListaCompanys
               else
                   if Wizard.EsPaginaActual( Empresas ) then
                      CanMove := ValidaEmpresa;
          end;
     end;
end;

procedure TRevisarDatos.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual( Empresas ) then
     begin
          with ListaFuente do
               if ( Items.Count > 0 ) and ( ItemIndex < 0 ) then
                  ItemIndex := 0;
          SetControls;
     end;
end;

procedure TRevisarDatos.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
begin
     lOk := RevisarArchivos;
end;

end.
