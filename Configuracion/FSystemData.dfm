object SystemData: TSystemData
  Left = 183
  Top = 179
  Width = 459
  Height = 420
  Caption = 'Datos del Sistema'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panelnferior: TPanel
    Left = 0
    Top = 350
    Width = 443
    Height = 32
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      443
      32)
    object Salir: TBitBtn
      Left = 363
      Top = 3
      Width = 77
      Height = 27
      Anchors = [akTop, akRight]
      Caption = '&Salir'
      TabOrder = 0
      Kind = bkClose
    end
    object Exportar: TBitBtn
      Left = 2
      Top = 3
      Width = 77
      Height = 27
      Hint = 'Exportar Datos A Un Archivo'
      Caption = 'E&xportar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = ExportarClick
      Glyph.Data = {
        4E010000424D4E01000000000000760000002800000012000000120000000100
        040000000000D800000000000000000000001000000000000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777000000
        0000000000007777770788888888800000007888870F77777777800000008FFF
        F80F77700777800000008F11F80F70000007800000008F11F80F777777778000
        00008FFFF80FFFFFFFFF7000000078888F0000000070000000008FFFF8FFFF87
        7717770000008F33F8F22F877111770000008F33F8F22F877717770000008FFF
        F8FFFF8777177700000078888F8888777717770000008FFFF8FFFF8777177700
        00008FDDF8FCCF111117770000008FDDF8FCCF877777770000008FFFF8FFFF87
        777777000000788887888877777777000000}
    end
  end
  object PanelSuperior: TPanel
    Left = 0
    Top = 0
    Width = 443
    Height = 350
    Align = alClient
    TabOrder = 1
    object ServerInfo: TMemo
      Left = 1
      Top = 1
      Width = 441
      Height = 348
      Align = alClient
      Color = clBtnFace
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier'
      Font.Style = []
      Lines.Strings = (
        ''
        'Servidor de Windows NT')
      ParentFont = False
      ScrollBars = ssVertical
      TabOrder = 0
    end
  end
  object SaveDialog: TSaveDialog
    DefaultExt = '3dt'
    Filter = 'Datos Del Sistema (*.3dt)|*.3dt|Todos (*.*)|*.*'
    FilterIndex = 0
    Title = 'Archivo De Exportaci'#243'n'
    Left = 104
    Top = 112
  end
end
