unit FFixes;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Buttons, StdCtrls, FileCtrl, DB, DBTables, ComCtrls, ImgList, ExtCtrls;

type
  TEmpresaData = class(TObject)
  private
    { Private declarations }
    FOwner: TList;
    FAlias: String;
    FCodigo: String;
    FNombre: String;
    FPassword: String;
    FUserName: String;
  public
    { Public declarations }
    constructor Create( Owner: TList; const sCodigo, sNombre, sAlias, sUserName, sPassword: String );
    destructor Destroy; override;
    property Alias: String read FAlias;
    property Codigo: String read FCodigo;
    property Nombre: String read FNombre;
    property Password: String read FPassword;
    property UserName: String read FUserName;
  end;
  TUpgrader = class(TForm)
    dbTress: TDatabase;
    tqMaster: TQuery;
    StatusBar: TStatusBar;
    SmallIconList: TImageList;
    EmpresasGB: TGroupBox;
    Companias: TListView;
    PanelBotones: TPanel;
    OK: TBitBtn;
    Salir: TBitBtn;
    PruebasGB: TGroupBox;
    DoDefaults: TCheckBox;
    DoDecimales: TCheckBox;
    DoCheckReports: TCheckBox;
    DoCheckKardex: TCheckBox;
    tqUno: TQuery;
    tqDos: TQuery;
    tqTres: TQuery;
    tqCuatro: TQuery;
    tqCinco: TQuery;
    tqSeis: TQuery;
    DoCheckPlantillas: TCheckBox;
    BitBtn1: TBitBtn;
    lbAyuda: TLabel;
    DoCheckConceptos: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure SalirClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    FLog: TStrings;
    FCompanies: TList;
    FErrores: Integer;
    FLengthMensaje : integer;
    function ConectarComparte: Boolean;
    function ConectarWindows( const sAlias, sUsuario, sPassword: String ): Boolean;
    function GetCompanies(Index: Integer): TEmpresaData;
    function GetCompaniesCount: Integer;
    procedure ActualizaDecimales(const sScript, sTabla: string);
    procedure ActualizaParametrosReportes(const sScript, sTipo: string);
    procedure Actualizar(Empresa: TEmpresaData);
    procedure ClearCompanies;
    procedure CloseLog( sFileName: String );
    procedure DesconectarWindows;
    procedure EmpezarTransaccion;
    procedure InitLog;
    procedure LogError( const sMensaje: String; Error: Exception );
    procedure LogEvent( const sMensaje: String );
    procedure LogEvent2(const sMensaje: String);
    procedure LogEvent3;
    procedure LogIt(const sMensaje: String);
    procedure MasterEjecuta(const sScript: String);
    procedure MasterPrepara(const sScript: String);
    procedure MuestraError( Sender: TObject; Error: Exception );
    procedure PreparaQuery(Query: TQuery; const sScript: String);
    procedure RevisarPlantillas;
    procedure RevisarAhorros;
    procedure RevisarConceptos(const sID, sCampo: String);
    procedure RevisarPrestamos;
    procedure RevisarReportes(const sID, sCount, sList: String);
    procedure ShowStatus( const sMsg: String );
    procedure TerminarTransaccion( const lCommit: Boolean );
    procedure VerificaKardex;
  public
    { Public declarations }
    property Companies[ Index: Integer ]: TEmpresaData read GetCompanies;
  end;

var
  Upgrader: TUpgrader;

implementation

uses ZetaMessages,
     ZetaRegistryServer,
     ZetaServerTools,
     ZetaClientTools,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaDialogo,
     ZGlobalTress,
     FAyuda,
     FPlantillas;

{$R *.DFM}

const
     K_DIVISION = 75;

{ ******** TEmpresaData ********* }

constructor TEmpresaData.Create( Owner: TList; const sCodigo, sNombre, sAlias, sUserName, sPassword: String );
begin
     FOwner := Owner;
     FCodigo := sCodigo;
     FNombre := sNombre;
     FAlias := sAlias;
     FPassword := sPassword;
     FUserName := sUserName;
     FOwner.Add( Self );
end;

destructor TEmpresaData.Destroy;
begin
     FOwner.Remove( Self );
     inherited Destroy;
end;

{ ********* TUpgrader ************ }

procedure TUpgrader.FormCreate(Sender: TObject);
begin
     ZetaCommonTools.Environment;
     Application.OnException := MuestraError;
     FLog := TStringList.Create;
     FCompanies := TList.Create;
     FLengthMensaje := 0;
     lbAyuda.Caption := 'Recuerde dar Click en el bot�n de ''Ayuda'','+ CR_LF +
                        'para ver informaci�n m�s detallada de cada uno'+ CR_LF +
                        'de los procesos que se van a realizar.'; 
end;

procedure TUpgrader.FormShow(Sender: TObject);
var
   i: Integer;
   ListItem: TListItem;
begin
     ConectarComparte;
     for i := 0 to ( GetCompaniesCount - 1 ) do
     begin
          with Companias.Items do
          begin
               ListItem := Add;
               with ListItem do
               begin
                    StateIndex := i;
               end;
               Item[ ListItem.Index ].Caption := Companies[ i ].Nombre;
          end;
     end;
     with Companias do
     begin
          if ( Selected = nil ) then
             Selected := Items[ 0 ];
     end;
end;

procedure TUpgrader.FormDestroy(Sender: TObject);
begin
     ClearCompanies;
     FCompanies.Free;
     FLog.Free;
end;

{ ****** Bit�cora ********* }

procedure TUpgrader.InitLog;
begin
     FErrores := 0;
     with FLog do
     begin
          BeginUpdate;
          Clear;
     end;
end;

procedure TUpgrader.LogIt( const sMensaje: String );
begin
     FLog.Add( sMensaje );
end;

procedure TUpgrader.LogError( const sMensaje: String; Error: Exception );
var
   i, iLastCategory, iLastErrorCode: Integer;
begin
     with FLog do
     begin
          BeginUpdate;
          LogIt( sMensaje + ':' );
          if Assigned( Error ) then
          begin
               if ( Error is EDBEngineError ) then
               begin
                    with Error as EDBEngineError do
                    begin
                         iLastCategory := 0;
                         iLastErrorCode := 0;
                         for i := 0 to ( ErrorCount - 1 ) do
                         begin
                              with Errors[ i ] do
                              begin
                                   if ( iLastCategory = Category ) and ( iLastErrorCode = ErrorCode ) then
                                      LogIt( Message )
                                   else
                                       LogIt( IntToStr( i + 1 ) +
                                              ': Categor�a = ' +
                                              IntToStr( Category ) +
                                              ', Error = ' +
                                              IntToStr( ErrorCode ) +
                                              CR_LF +
                                              Message );
                                   iLastCategory := Category;
                                   iLastErrorCode := ErrorCode;
                              end;
                         end;
                    end;
               end
               else
               begin
                    with Error as Exception do
                    begin
                         LogIt( ClassName + ' = ' + Message );
                    end;
               end;
          end;
          LogIt( StringOfChar( '-', 20 ) );
          EndUpdate;
     end;
     FErrores := FErrores + 1;
end;

procedure TUpgrader.LogEvent2( const sMensaje: String );
begin
     LogIt( StringOfChar( '=', 80 ) );
     LogIt('');
     LogIt('');
     LogIt( StringOfChar( '=', 80 ) );
     LogIt('');
     LogEvent( sMensaje );
end;

procedure TUpgrader.LogEvent3;
begin
     //LogEvent( StringOfChar( '*', 40 ) );
end;

procedure TUpgrader.LogEvent( const sMensaje: String );
begin
     LogIt( StringOfChar( '*', 5 ) + ' ' + sMensaje + ' ' + StringOfChar( '*', 5 ) );
end;

procedure TUpgrader.CloseLog( sFileName: String );
begin
     sFileName := ZetaMessages.SetFileNameDefaultPath( sFileName );
     with FLog do
     begin
          EndUpdate;
          SaveToFile( sFileName );
     end;
     if ( FErrores > 0 ) then
     begin
          if zErrorConfirm( '� El Proceso Ha Terminado Con Errores !',
                            'Se Encontraron ' + Trim( Format( '%10.0n', [ FErrores / 1 ] ) ) + ' Errores' +
                            CR_LF +
                            CR_LF +
                            'La Bit�cora Ha Sido Guardada En El Archivo' +
                            CR_LF +
                            sFileName +
                            CR_LF +
                            CR_LF +
                            '� Desea Ver La Bit�cora Del Proceso ?', 0, mbOk ) then
          begin
               ExecuteFile( 'NOTEPAD.EXE', ExtractFileName( sFileName ), ExtractFilePath( sFileName ), SW_SHOWDEFAULT );
          end;
     end
     else
         if ZetaDialogo.zConfirm( '� El Proceso Ha Terminado Con Exito !', '� Desea Ver La Bit�cora Del Proceso ?', 0, mbOk ) then
         begin
              ExecuteFile( 'NOTEPAD.EXE', ExtractFileName( sFileName ), ExtractFilePath( sFileName ), SW_SHOWDEFAULT );
         end;
end;

procedure TUpgrader.MuestraError( Sender: TObject; Error: Exception );
begin
     ZetaDialogo.zExcepcion( '� Error En ' + Caption + ' !', '� Se Encontr� Un Error !', Error, 0 );
end;

function TUpgrader.GetCompanies( Index: Integer ): TEmpresaData;
begin
     Result := TEmpresaData( FCompanies.Items[ Index ] );
end;

function TUpgrader.GetCompaniesCount: Integer;
begin
     Result := FCompanies.Count;
end;

procedure TUpgrader.ClearCompanies;
var
   i: Integer;
begin
     for i := ( GetCompaniesCount - 1 ) downto 0 do
     begin
          Companies[ i ].Free;
     end;
end;

{ ************* Bases de Datos *********** }

function TUpgrader.ConectarWindows( const sAlias, sUsuario, sPassword: String ): Boolean;
begin
     try
        with dbTress do
        begin
             Connected := False;
             AliasName := sAlias;
             with Params do
             begin
                  Values[ 'USER NAME' ] := sUsuario;
                  Values[ 'PASSWORD' ] := sPassword;
             end;
             Connected := True;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                DesconectarWindows;
                Application.HandleException( Error );
                Result := False;
           end;
     end;
end;

procedure TUpgrader.DesconectarWindows;
begin
     with dbTress do
     begin
          Connected := False;
     end;
end;

procedure TUpgrader.EmpezarTransaccion;
begin
     with dbTress do
     begin
          if not InTransaction then
             StartTransaction;
     end;
end;

procedure TUpgrader.TerminarTransaccion( const lCommit: Boolean );
begin
     with dbTress do
     begin
          if InTransaction then
          begin
               if lCommit then
                  Commit
               else
                   RollBack;
          end;
     end;
end;

procedure TUpgrader.PreparaQuery( Query: TQuery; const sScript: String );
begin
     with Query do
     begin
          Active := False;
          with SQL do
          begin
               Clear;
               Text := sScript;
          end;
          Prepare;
     end;
end;

procedure TUpgrader.MasterPrepara( const sScript: String );
begin
     PreparaQuery( tqMaster, sScript );
end;

procedure TUpgrader.MasterEjecuta( const sScript: String );
begin
     PreparaQuery( tqMaster, sScript );
     with tqMaster do
     begin
          ExecSQL;
     end;
end;
{ ********* Procesos ********* }

function TUpgrader.ConectarComparte: Boolean;
begin
     with TZetaRegistryServer.Create do
     begin
          try
             Result := ConectarWindows( AliasComparte, UserName, Password );
          finally
                 Free;
          end;
     end;
     if Result then
     begin
          try
             MasterPrepara( Format( 'update COMPANY set CM_CONTROL = ''%s'' where ( CM_CONTROL is NULL )', [ ZetaCommonLists.ObtieneElemento( lfTipoCompany, Ord( tc3Datos ) ) ] ) );
          except
                on Error: Exception do
                begin
                     Application.HandleException( Error );
                     Result := False;
                end;
          end;
     end;
     if Result then
     begin
          ClearCompanies;
          try
             MasterPrepara( 'select CM_CODIGO, CM_NOMBRE, CM_ALIAS, CM_USRNAME, CM_PASSWRD from COMPANY order by CM_NOMBRE' );
             with tqMaster do
             begin
                  Active := True;
                  while not Eof do
                  begin
                       TEmpresaData.Create( FCompanies,
                                            FieldByName( 'CM_CODIGO' ).AsString,
                                            FieldByName( 'CM_NOMBRE' ).AsString,
                                            FieldByName( 'CM_ALIAS' ).AsString,
                                            FieldByName( 'CM_USRNAME' ).AsString,
                                            ZetaServerTools.Decrypt( FieldByName( 'CM_PASSWRD' ).AsString ) );
                       Next;
                  end;
                  Active := False;
             end;
          except
                on Error: Exception do
                begin
                     Application.HandleException( Error );
                     Result := False;
                end;
          end;
     end;
     DesconectarWindows;
end;

procedure TUpgrader.RevisarReportes( const sID, sCount, sList: String );
var
   iCuantos, iReporte: Integer;
   sSugerida: String;

function GetTipoDato( const eTipo: eTipoCampo; const iPosicion: Integer ): String;
begin
     Result := ZetaCommonLists.ObtieneElemento( lfTipoCampo, Ord( eTipo ) );
     case eTipo of
          tcEncabezado, tcPieGrupo:
          begin
               if ( iPosicion = 0 ) then
                  Result := Format( '%s - NivelEmpresa', [ Result ] )
               else
                   Result := Format( '%s - Grupo %d', [ Result, iPosicion ] );
          end;
     end;
end;

begin
     try
        MasterPrepara( sCount );
        with tqMaster do
        begin
             Active := True;
             iCuantos := Fields[ 0 ].AsInteger;
             Active := False;
        end;
        if ( iCuantos > 0 ) then
        begin
             LogEvent2( Format('Revision de Reporte con %s', [sID]) );
             LogIt( Format( 'Hay Reportes %s ( %d Ocurrencias )', [ sID, iCuantos ] ) );
             LogIt( 'Reportes Por Revisar:' );
             LogIt( '' );
             MasterPrepara( sList );
             with tqMaster do
             begin
                  Active := True;
                  iReporte := -1;
                  while not Eof do
                  begin
                       if ( iReporte <> FieldByName( 'RE_CODIGO' ).AsInteger ) then
                       begin
                            iReporte := FieldByName( 'RE_CODIGO' ).AsInteger;
                            LogIt( '' );
                            LogIt( StringOfChar( '-', K_DIVISION ) );
                            LogIt( Format( 'Reporte %d = %s ( %s ) Tipo %s:', [ iReporte,
                                                                                FieldByName( 'RE_NOMBRE' ).AsString,
                                                                                ZetaCommonLists.ObtieneElemento( lfClasifiReporte, FieldByName( 'RE_CLASIFI' ).AsInteger ),
                                                                                ZetaCommonLists.ObtieneElemento( lfTipoReporte, FieldByName( 'RE_TIPO' ).AsInteger ) ] ) );
                       end;
                       LogIt( Format( '%s: Columna %s, F�rmula %s', [ GetTipoDato( eTipoCampo( FieldByName( 'CR_TIPO' ).AsInteger ), FieldByName( 'CR_POSICIO' ).AsInteger ),
                                                                      FieldByName( 'CR_TITULO' ).AsString,
                                                                      Trim(FieldByName( 'CR_FORMULA' ).AsString) ] ) );
                       sSugerida := FieldByName( 'SUGERIDA' ).AsString;
                       if ZetaCommonTools.StrLleno( sSugerida ) then
                          LogIt( Format( 'F�rmula Sugerida: %s', [ sSugerida ] ) );
                       Next;
                  end;
                  Active := False;
             end;
        end;
     except
           on Error: Exception do
           begin
                LogError( Format( 'Error Al Buscar Reportes %s', [ sID ] ), Error );
           end;
     end;
end;

procedure TUpgrader.RevisarConceptos( const sID, sCampo: String );
const
     Q_FILTRO_IF_CASE = '( UPPER( @CAMPO ) like ''%IF(%'' ) or ( UPPER( @CAMPO ) like ''%IF (%'' ) or '+
                        '( UPPER( @CAMPO ) like ''%CASE(%'' ) or ( UPPER( @CAMPO ) like ''%CASE (%'' )';
     Q_FILTRO_C = '( UPPER( @CAMPO ) like ''% C(%'' ) or '+
                  '( UPPER( @CAMPO ) like ''%(C(%'' ) or '+
                  '( UPPER( @CAMPO ) like ''%+C(%'' ) or '+
                  '( UPPER( @CAMPO ) like ''%-C(%'' ) or '+
                  '( UPPER( @CAMPO ) like ''%*C(%'' ) or '+
                  '( UPPER( @CAMPO ) like ''%/C(%'' ) or '+
                  '( UPPER( @CAMPO ) like ''%.C(%'' ) or '+
                  '( UPPER( @CAMPO ) like ''%,C(%'' )';

procedure CheckConceptos( const sFiltro, sFuncion: String );
const
     ARROBA_CAMPO = '@CAMPO';
     Q_CONCEPTO_COUNT = 'select COUNT(*) from CONCEPTO where %s order by CO_NUMERO';
     Q_CONCEPTO_LIST = 'select CO_NUMERO, CO_DESCRIP, %s from CONCEPTO where %s order by CO_NUMERO';
var
   iCuantos: Integer;
   sFilter: String;
begin
     try
        sFilter := ZetaCommonTools.StrTransAll( sFiltro, ARROBA_CAMPO, sCampo );
        MasterPrepara( Format( Q_CONCEPTO_COUNT, [ sFilter ] ) );
        with tqMaster do
        begin
             Active := True;
             iCuantos := Fields[ 0 ].AsInteger;
             Active := False;
        end;
        if ( iCuantos > 0 ) then
        begin
             LogEvent2( Format('Revision De %s De Conceptos de N�mina ( CONCEPTO.%s )', [ sID, sCampo ] ) );
             LogIt( Format( 'Hay %d Conceptos De N�mina Con %s Que Usan %s', [ iCuantos, sID, sFuncion ] ) );
             LogIt( 'Conceptos Por Revisar:' );
             LogIt( '' );
             MasterPrepara( Format( Q_CONCEPTO_LIST, [ sCampo, sFilter ] ) );
             with tqMaster do
             begin
                  Active := True;
                  while not Eof do
                  begin
                       LogIt( Format( 'Concepto # %d: %s', [ FieldByName( 'CO_NUMERO' ).AsInteger, FieldByName( 'CO_DESCRIP' ).AsString ] ) );
                       LogIt( Format( 'F�rmula de %s: %s', [ sID, FieldByName( sCampo ).AsString ] ) );
                       LogIt( '' );
                       Next;
                  end;
                  Active := False;
             end;
        end;
     except
           on Error: Exception do
           begin
                LogError( Format( 'Error Al Revisar %s De Conceptos', [ sID ] ), Error );
           end;
     end;
end;

begin
     CheckConceptos( Q_FILTRO_C, 'La Funci�n C()' );
     CheckConceptos( Q_FILTRO_IF_CASE, 'Las Funciones IF() y/o CASE()' );
end;

procedure TUpgrader.RevisarAhorros;
const
     Q_FILTRO_IF_CASE = '( UPPER( AH_FORMULA ) like ''%IF(%'' ) or ( UPPER( AH_FORMULA ) like ''%IF (%'' ) or '+
                        '( UPPER( AH_FORMULA ) like ''%CASE(%'' ) or ( UPPER( AH_FORMULA ) like ''%CASE (%'' )';
     Q_FILTRO_C = '( UPPER( AH_FORMULA ) like ''% C(%'' ) or '+
                  '( UPPER( AH_FORMULA ) like ''%(C(%'' ) or '+
                  '( UPPER( AH_FORMULA ) like ''%+C(%'' ) or '+
                  '( UPPER( AH_FORMULA ) like ''%-C(%'' ) or '+
                  '( UPPER( AH_FORMULA ) like ''%*C(%'' ) or '+
                  '( UPPER( AH_FORMULA ) like ''%/C(%'' ) or '+
                  '( UPPER( AH_FORMULA ) like ''%.C(%'' ) or '+
                  '( UPPER( AH_FORMULA ) like ''%,C(%'' )';

procedure CheckAhorros( const sFiltro, sFuncion: String );
const
     Q_COUNT = 'select COUNT(*) from AHORRO where %s order by CB_CODIGO, AH_TIPO';
     Q_LIST = 'select CB_CODIGO, AH_TIPO, AH_FORMULA from AHORRO where %s order by CB_CODIGO, AH_TIPO';
var
   iCuantos: Integer;
begin
     try
        MasterPrepara( Format( Q_COUNT, [ sFiltro ] ) );
        with tqMaster do
        begin
             Active := True;
             iCuantos := Fields[ 0 ].AsInteger;
             Active := False;
        end;
        if ( iCuantos > 0 ) then
        begin
             LogEvent2( 'Revision De F�rmulas De Ahorros ( AHORRO.AH_FORMULA )' );
             LogIt( Format( 'Hay %d F�rmulas de Ahorros Que Usan %s', [ iCuantos, sFuncion ] ) );
             LogIt( 'Ahorros Por Revisar:' );
             LogIt( '' );
             MasterPrepara( Format( Q_LIST, [ sFiltro ] ) );
             with tqMaster do
             begin
                  Active := True;
                  while not Eof do
                  begin
                       LogIt( Format( 'Empleado # %d, Tipo De Ahorro: %s', [ FieldByName( 'CB_CODIGO' ).AsInteger, FieldByName( 'AH_TIPO' ).AsString ] ) );
                       LogIt( Format( 'F�rmula: %s', [ FieldByName( 'AH_FORMULA' ).AsString ] ) );
                       Next;
                  end;
                  Active := False;
             end;
        end;
     except
           on Error: Exception do
           begin
                LogError( 'Error Al Revisar F�rmulas De Ahorros', Error );
           end;
     end;
end;

begin
     CheckAhorros( Q_FILTRO_C, 'La Funci�n C()' );
     CheckAhorros( Q_FILTRO_IF_CASE, 'Las Funciones IF() y/o CASE()' );
end;

procedure TUpgrader.RevisarPrestamos;
const
     Q_FILTRO_IF_CASE = '( UPPER( PR_FORMULA ) like ''%IF(%'' ) or ( UPPER( PR_FORMULA ) like ''%IF (%'' ) or '+
                        '( UPPER( PR_FORMULA ) like ''%CASE(%'' ) or ( UPPER( PR_FORMULA ) like ''%CASE (%'' )';
     Q_FILTRO_C = '( UPPER( PR_FORMULA ) like ''% C(%'' ) or '+
                  '( UPPER( PR_FORMULA ) like ''%(C(%'' ) or '+
                  '( UPPER( PR_FORMULA ) like ''%+C(%'' ) or '+
                  '( UPPER( PR_FORMULA ) like ''%-C(%'' ) or '+
                  '( UPPER( PR_FORMULA ) like ''%*C(%'' ) or '+
                  '( UPPER( PR_FORMULA ) like ''%/C(%'' ) or '+
                  '( UPPER( PR_FORMULA ) like ''%.C(%'' ) or '+
                  '( UPPER( PR_FORMULA ) like ''%,C(%'' )';

procedure CheckPrestamos( const sFiltro, sFuncion: String );
const
     Q_COUNT = 'select COUNT(*) from PRESTAMO where %s order by CB_CODIGO, PR_TIPO, PR_REFEREN';
     Q_LIST = 'select CB_CODIGO, PR_TIPO, PR_REFEREN, PR_FORMULA from PRESTAMO where %s order by CB_CODIGO, PR_TIPO, PR_REFEREN';
var
   iCuantos: Integer;
begin
     try
        MasterPrepara( Format( Q_COUNT, [ sFiltro ] ) );
        with tqMaster do
        begin
             Active := True;
             iCuantos := Fields[ 0 ].AsInteger;
             Active := False;
        end;
        if ( iCuantos > 0 ) then
        begin
             LogEvent2( 'Revision De F�rmulas De Prestamos ( PRESTAMO.PR_FORMULA )' );
             LogIt( Format( 'Hay %d F�rmulas de Prestamos Que Usan %s', [ iCuantos, sFuncion ] ) );
             LogIt( 'Prestamos Por Revisar:' );
             LogIt( '' );
             MasterPrepara( Format( Q_LIST, [ sFiltro ] ) );
             with tqMaster do
             begin
                  Active := True;
                  while not Eof do
                  begin
                       LogIt( Format( 'Empleado # %d, Tipo De Pr�stamo: %s, Referencia: %s', [ FieldByName( 'CB_CODIGO' ).AsInteger, FieldByName( 'PR_TIPO' ).AsString, FieldByName( 'PR_REFEREN' ).AsString ] ) );
                       LogIt( Format( 'F�rmula: %s', [ FieldByName( 'PR_FORMULA' ).AsString ] ) );
                       Next;
                  end;
                  Active := False;
             end;
        end;
     except
           on Error: Exception do
           begin
                LogError( 'Error Al Revisar F�rmulas De Prestamos', Error );
           end;
     end;
end;

begin
     CheckPrestamos( Q_FILTRO_C, 'La Funci�n C()' );
     CheckPrestamos( Q_FILTRO_IF_CASE, 'Las Funciones IF() y/o CASE()' );
end;

procedure TUpgrader.Actualizar( Empresa: TEmpresaData );
const
     Q_SET_DEFAULT_BOOL = 'update CAMPOREP set CR_FORMULA = ''TRUE'' where ( CR_TIPO = 6 ) and ( CR_TFIELD = 0 ) and ( CR_FORMULA = '''' )';
     Q_SET_DEFAULT_FLOAT = 'update CAMPOREP set CR_FORMULA = ''0.0'' where ( CR_TIPO = 6 ) and ( CR_TFIELD = 1 ) and ( CR_FORMULA = '''' )';
     Q_SET_DEFAULT_INT = 'update CAMPOREP set CR_FORMULA = ''0'' where ( CR_TIPO = 6 ) and ( CR_TFIELD = 2 ) and ( CR_FORMULA = '''' )';
     Q_SET_DEFAULT_DATE = 'update CAMPOREP set CR_FORMULA = ''DATE()'' where ( CR_TIPO = 6 ) and ( CR_TFIELD = 3 ) and ( CR_FORMULA = '''' )';
     Q_SET_DEFAULT_TEXT = 'update CAMPOREP set CR_FORMULA = '''' where ( CR_TIPO = 6 ) and ( CR_TFIELD in ( 4, 5 ) ) and ( CR_FORMULA = '''' )';
     Q_DECIMALES_COLABORA = 'update COLABORA set '+
                            'CB_OLD_INT = cast( CB_OLD_INT * 100 as INTEGER ) / 100,'+
                            'CB_OLD_SAL = cast( CB_OLD_SAL * 100 as INTEGER ) / 100,'+
                            'CB_PER_VAR = cast( CB_PER_VAR * 100 as INTEGER ) / 100,'+
                            'CB_PRE_INT = cast( CB_PRE_INT * 100 as INTEGER ) / 100,'+
                            'CB_SAL_INT = cast( CB_SAL_INT * 100 as INTEGER ) / 100,'+
                            'CB_SAL_TOT = cast( CB_SAL_TOT * 100 as INTEGER ) / 100,'+
                            'CB_SALARIO = cast( CB_SALARIO * 100 as INTEGER ) / 100,'+
                            'CB_TOT_GRA = cast( CB_TOT_GRA * 100 as INTEGER ) / 100';
     Q_DECIMALES_KARDEX = 'update KARDEX set '+
                          'CB_TOT_GRA = cast( CB_TOT_GRA * 100 as INTEGER ) / 100,'+
                          'CB_SALARIO = cast( CB_SALARIO * 100 as INTEGER ) / 100,'+
                          'CB_SAL_TOT = cast( CB_SAL_TOT * 100 as INTEGER ) / 100,'+
                          'CB_SAL_INT = cast( CB_SAL_INT * 100 as INTEGER ) / 100,'+
                          'CB_PRE_INT = cast( CB_PRE_INT * 100 as INTEGER ) / 100,'+
                          'CB_PER_VAR = cast( CB_PER_VAR * 100 as INTEGER ) / 100,'+
                          'CB_OTRAS_P = cast( CB_OTRAS_P * 100 as INTEGER ) / 100,'+
                          'CB_OLD_SAL = cast( CB_OLD_SAL * 100 as INTEGER ) / 100,'+
                          'CB_OLD_INT = cast( CB_OLD_INT * 100 as INTEGER ) / 100';
     Q_DECIMALES_NOMINA = 'update NOMINA set '+
                          'NO_X_MENS = cast( NO_X_MENS * 100 as INTEGER ) / 100,'+
                          'NO_X_ISPT = cast( NO_X_ISPT * 100 as INTEGER ) / 100,'+
                          'NO_X_CAL = cast( NO_X_CAL * 100 as INTEGER ) / 100,'+
                          'NO_TOT_PRE = cast( NO_TOT_pre * 100 as INTEGER ) / 100,'+
                          'NO_PERCEPC = cast( NO_PERCEPC * 100 as INTEGER ) / 100,'+
                          'NO_PER_MEN = cast( NO_PER_MEN * 100 as INTEGER ) / 100,'+
                          'NO_PER_CAL = cast( NO_PER_CAL * 100 as INTEGER ) / 100,'+
                          'NO_NETO = cast( NO_NETO * 100 as INTEGER ) / 100,'+
                          'NO_DEDUCCI = cast( NO_DEDUCCI * 100 as INTEGER ) / 100,'+
                          'CB_SALARIO = cast( CB_SALARIO * 100 as INTEGER ) / 100,'+
                          'CB_SAL_INT = cast( CB_SAL_INT * 100 as INTEGER ) / 100,'+
                          'NO_IMP_CAL = cast( NO_IMP_CAL * 100 as INTEGER ) / 100';
     Q_DECIMALES_PRESTA = 'update PRESTACI set PT_FACTOR = cast( PT_FACTOR * 10000 as INTEGER ) / 10000';
     Q_POLIZAS_COUNT = 'select COUNT(*) CUANTOS from CAMPOREP left outer join REPORTE on ( REPORTE.RE_CODIGO = CAMPOREP.RE_CODIGO ) where '+
                       '( ( UPPER( CAMPOREP.CR_FORMULA ) like ''%PERIODO.%'' ) or ( UPPER( CAMPOREP.CR_FORMULA ) like ''%NOMINA.%'' ) or ( UPPER( CAMPOREP.CR_FORMULA ) like ''%COLABORA.%'' ) ) and '+
                       '( REPORTE.RE_TIPO = 3 ) and ( CAMPOREP.CR_TIPO = 0 )';
     Q_POLIZAS_LIST = 'select REPORTE.RE_CLASIFI, REPORTE.RE_CODIGO, REPORTE.RE_NOMBRE, CAMPOREP.CR_TITULO, CAMPOREP.CR_FORMULA '+
                      'from CAMPOREP left outer join REPORTE on ( REPORTE.RE_CODIGO = CAMPOREP.RE_CODIGO ) where '+
                      '( ( UPPER( CAMPOREP.CR_FORMULA ) like ''%PERIODO.%'' ) or '+
                      '  ( UPPER( CAMPOREP.CR_FORMULA ) like ''%NOMINA.%'' ) or '+
                      '  ( UPPER( CAMPOREP.CR_FORMULA ) like ''%COLABORA.%'' ) )  and '+
                      '( REPORTE.RE_TIPO = 3 ) and ( CAMPOREP.CR_TIPO = 0 ) '+
                      'order by REPORTE.RE_CLASIFI, REPORTE.RE_CODIGO';
     Q_RAPIDAS_COUNT = 'select COUNT(*) CUANTOS from CAMPOREP left outer join REPORTE on ( REPORTE.RE_CODIGO = CAMPOREP.RE_CODIGO ) where '+
                       '( ( ( ( ( CAMPOREP.CR_COLOR = 0 ) and ( CAMPOREP.CR_SUBPOS > RE_RENESPA ) or ( ( CAMPOREP.CR_COLOR = 2 ) and ( CAMPOREP.CR_SUBPOS < ( RE_RENESPA + RE_COLESPA ) ) ) ) and '+
                       'not ( ( REPORTE.RE_NOMBRE like ''M_A%'' ) or '+
                       '( REPORTE.RE_NOMBRE like ''M_C%'' ) or '+
                       '( REPORTE.RE_NOMBRE like ''M_F%'' ) or '+
                       '( REPORTE.RE_NOMBRE like ''M_I%'' ) or '+
                       '( REPORTE.RE_NOMBRE like ''M_R%'' ) or '+
                       '( REPORTE.RE_NOMBRE like ''M_N%'' ) ) ) ) ) and '+
                       '( REPORTE.RE_TIPO = 5 ) and '+
                       '( CAMPOREP.CR_TIPO = 0 ) and '+
                       '( REPORTE.RE_CLASIFI in ( 0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 11 ) )';
     Q_RAPIDAS_LIST = 'select REPORTE.RE_CLASIFI, REPORTE.RE_CODIGO, REPORTE.RE_TIPO, REPORTE.RE_NOMBRE, REPORTE.RE_FECHA, REPORTE.RE_ANCHO LINEAS_X_FORMA, '+
                      'REPORTE.RE_RENESPA LINEAS_ENCABEZADO, REPORTE.RE_COLESPA LINEAS_DETALLE, REPORTE.RE_ANCHO - REPORTE.RE_RENESPA - REPORTE.RE_COLESPA LINEAS_PIE, '+
                      'CAMPOREP.CR_COLOR BANDA, CAMPOREP.CR_SUBPOS RENGLON, CAMPOREP.CR_SHOW COLUMNA, CAMPOREP.CR_TITULO, CAMPOREP.CR_FORMULA from CAMPOREP '+
                      'left outer join REPORTE on ( REPORTE.RE_CODIGO = CAMPOREP.RE_CODIGO ) where '+
                      '( ( ( ( ( CAMPOREP.CR_COLOR = 0 ) and ( CAMPOREP.CR_SUBPOS > RE_RENESPA ) or ( ( CAMPOREP.CR_COLOR = 2 ) and ( CAMPOREP.CR_SUBPOS < ( RE_RENESPA + RE_COLESPA ) ) ) ) and '+
                      'not ( ( REPORTE.RE_NOMBRE like ''M_A%'' ) or '+
                      '( REPORTE.RE_NOMBRE like ''M_C%'' ) or '+
                      '( REPORTE.RE_NOMBRE like ''M_F%'' ) or '+
                      '( REPORTE.RE_NOMBRE like ''M_I%'' ) or '+
                      '( REPORTE.RE_NOMBRE like ''M_R%'' ) or '+
                      '( REPORTE.RE_NOMBRE like ''M_N%'' ) ) ) ) ) and '+
                      '( REPORTE.RE_TIPO = 5 ) and '+
                      '( CAMPOREP.CR_TIPO = 0 ) and '+
                      '( REPORTE.RE_CLASIFI in ( 0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 11 ) ) '+
                      'order by REPORTE.RE_CLASIFI, REPORTE.RE_CODIGO, CAMPOREP.CR_TIPO, CAMPOREP.CR_POSICIO, CAMPOREP.CR_SUBPOS, CAMPOREP.CR_SHOW';
     Q_RAPIDAS_RENGLON_COUNT = 'select COUNT(CAMPOREP.CR_SUBPOS) '+
                               'from CAMPOREP '+
                               'left outer join REPORTE on ( REPORTE.RE_CODIGO = CAMPOREP.RE_CODIGO ) '+
                               'where '+
                               '( REPORTE.RE_TIPO = 5 ) and '+
                               '( REPORTE.RE_CLASIFI in ( 0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 11 ) ) and '+
                               '( CAMPOREP.CR_TIPO = 0 ) and '+
                               '( CAMPOREP.CR_COLOR = 1 ) and '+
                               '( CAMPOREP.CR_SUBPOS > 1 ) and '+
                               'NOT ( ( REPORTE.RE_NOMBRE like ''M_A%'' ) or '+
                               '( REPORTE.RE_NOMBRE like ''M_C%'' ) or '+
                               '( REPORTE.RE_NOMBRE like ''M_F%'' ) or '+
                               '( REPORTE.RE_NOMBRE like ''M_I%'' ) or '+
                               '( REPORTE.RE_NOMBRE like ''M_R%'' ) or '+
                               '( REPORTE.RE_NOMBRE like ''M_N%'' ) ) '+
                               'order by REPORTE.RE_CLASIFI, REPORTE.RE_CODIGO, CAMPOREP.CR_TIPO, CAMPOREP.CR_POSICIO, CAMPOREP.CR_SUBPOS ';
     Q_RAPIDAS_RENGLON = 'select DISTINCT(CAMPOREP.CR_SUBPOS), '+
                         'REPORTE.RE_CLASIFI, REPORTE.RE_CODIGO, REPORTE.RE_TIPO, REPORTE.RE_NOMBRE, REPORTE.RE_FECHA '+
                         'from CAMPOREP '+
                         'left outer join REPORTE on ( REPORTE.RE_CODIGO = CAMPOREP.RE_CODIGO ) '+
                         'where '+
                         '( REPORTE.RE_TIPO = 5 ) and '+
                         '( REPORTE.RE_CLASIFI in ( 0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 11 ) ) and '+
                         '( CAMPOREP.CR_TIPO = 0 ) and '+
                         '( CAMPOREP.CR_COLOR = 1 ) and '+
                         '( CAMPOREP.CR_SUBPOS > 1 ) and '+
                         'NOT ( ( REPORTE.RE_NOMBRE like ''M_A%'' ) or '+
                         '( REPORTE.RE_NOMBRE like ''M_C%'' ) or '+
                         '( REPORTE.RE_NOMBRE like ''M_F%'' ) or '+
                         '( REPORTE.RE_NOMBRE like ''M_I%'' ) or '+
                         '( REPORTE.RE_NOMBRE like ''M_R%'' ) or '+
                         '( REPORTE.RE_NOMBRE like ''M_N%'' ) ) '+
                         'order by REPORTE.RE_CLASIFI, REPORTE.RE_CODIGO, CAMPOREP.CR_TIPO, CAMPOREP.CR_POSICIO, CAMPOREP.CR_SUBPOS ';

     Q_ARROBA_COUNT = 'select COUNT(*) CUANTOS from CAMPOREP where '+
                      '( UPPER( CAMPOREP.CR_FORMULA ) like ''%@FILTRO%'' )';
     Q_ARROBA_LIST = 'select REPORTE.RE_CLASIFI, REPORTE.RE_NOMBRE, REPORTE.RE_TIPO, CAMPOREP.RE_CODIGO, CAMPOREP.CR_TIPO, CAMPOREP.CR_POSICIO, '+
                     'CAMPOREP.CR_TITULO, CAMPOREP.CR_FORMULA, CAST( '''' AS CHAR( 1 ) ) SUGERIDA from CAMPOREP left outer join REPORTE on ( REPORTE.RE_CODIGO = CAMPOREP.RE_CODIGO ) where '+
                     '( UPPER( CAMPOREP.CR_FORMULA ) like ''%@FILTRO%'' ) '+
                     'order by REPORTE.RE_CLASIFI, CAMPOREP.RE_CODIGO, CAMPOREP.CR_TIPO, CAMPOREP.CR_POSICIO, CAMPOREP.CR_SUBPOS';
     Q_IFCASE_COUNT = 'select COUNT(*) CUANTOS from CAMPOREP where '+
                      '( UPPER( CAMPOREP.CR_FORMULA ) like ''%IF(%'' ) or ( UPPER( CAMPOREP.CR_FORMULA ) like ''%IF (%'' ) or '+
                      '( UPPER( CAMPOREP.CR_FORMULA ) like ''%CASE(%'' ) or ( UPPER( CAMPOREP.CR_FORMULA ) like ''%CASE (%'' )';
     Q_IFCASE_LIST = 'select REPORTE.RE_CLASIFI, REPORTE.RE_TIPO, REPORTE.RE_NOMBRE, '+
                     'CAMPOREP.RE_CODIGO, CAMPOREP.CR_TIPO, CAMPOREP.CR_POSICIO, CAMPOREP.CR_TITULO, CAMPOREP.CR_FORMULA, CAST( '''' AS CHAR( 1 ) ) SUGERIDA from CAMPOREP '+
                     'left outer join REPORTE on ( REPORTE.RE_CODIGO = CAMPOREP.RE_CODIGO ) where '+
                     '( UPPER( CAMPOREP.CR_FORMULA ) like ''%IF(%'' ) or ( UPPER( CAMPOREP.CR_FORMULA ) like ''%IF (%'' ) or '+
                     '( UPPER( CAMPOREP.CR_FORMULA ) like ''%CASE(%'' ) or ( UPPER( CAMPOREP.CR_FORMULA ) like ''%CASE (%'' ) '+
                     'order by REPORTE.RE_CLASIFI, CAMPOREP.RE_CODIGO, CAMPOREP.CR_TIPO, CAMPOREP.CR_POSICIO, CAMPOREP.CR_SUBPOS';
     Q_SINTABLA_COUNT = 'select COUNT(*) CUANTOS from CAMPOREP left outer join REPORTE on ( REPORTE.RE_CODIGO = CAMPOREP.RE_CODIGO ) where '+
                        '( ( ( REPORTE.RE_ENTIDAD <> CAMPOREP.CR_TABLA ) and '+
                        '( CAMPOREP.CR_TIPO <> 6 )  and '+
                        '( ( ( CAMPOREP.CR_TIPO <> 2 ) and ( CAMPOREP.CR_CALC = 0 ) ) or '+
                        '( ( CAMPOREP.CR_TIPO = 2 ) and ( CAMPOREP.CR_COLOR = 0 ) ) or '+
                        '( ( CAMPOREP.CR_TIPO = 1 ) and ( CAMPOREP.CR_ALINEA = 0 ) ) ) and '+
                        '( CAMPOREP.CR_FORMULA <> '''' ) and not ( UPPER( CAMPOREP.CR_FORMULA ) like ''%.%'' ) ) ) and '+
                        '( ( REPORTE.RE_TIPO = 0 ) or ( REPORTE.RE_TIPO = 5 ) )';
     Q_SINTABLA_LIST = 'select REPORTE.RE_CLASIFI, REPORTE.RE_NOMBRE, REPORTE.RE_TIPO, CAMPOREP.RE_CODIGO, CAMPOREP.CR_TIPO, '+
                       'CAMPOREP.CR_POSICIO, CAMPOREP.CR_TITULO, CAMPOREP.CR_FORMULA, '+
                       '( select D.DI_NOMBRE || ''.''|| CAMPOREP.CR_FORMULA from DICCION D where ( D.DI_CLASIFI = -1 ) and ( D.DI_CALC = CAMPOREP.CR_TABLA ) ) SUGERIDA '+
                       'from CAMPOREP left outer join REPORTE on ( REPORTE.RE_CODIGO = CAMPOREP.RE_CODIGO ) where '+
                       '( ( ( REPORTE.RE_ENTIDAD <> CAMPOREP.CR_TABLA ) and '+
                       '( CAMPOREP.CR_TIPO <> 6 )  and '+
                       '( ( ( CAMPOREP.CR_TIPO <> 2 ) and ( CAMPOREP.CR_CALC = 0 ) ) or '+
                       '( ( CAMPOREP.CR_TIPO = 2 ) and ( CAMPOREP.CR_COLOR = 0 ) ) or '+
                       '( ( CAMPOREP.CR_TIPO = 1 ) and ( CAMPOREP.CR_ALINEA = 0 ) ) ) and '+
                       '( CAMPOREP.CR_FORMULA <> '''' ) and not ( UPPER( CAMPOREP.CR_FORMULA ) like ''%.%'' ) ) ) and '+
                       '( ( REPORTE.RE_TIPO = 0 ) or ( REPORTE.RE_TIPO = 5 ) ) '+
                       'order by REPORTE.RE_CLASIFI, CAMPOREP.RE_CODIGO, CAMPOREP.CR_TIPO';
var
   iReporte, iCuantos: Integer;
   sMensaje : string;
begin
     with Empresa do
     begin
          LogEvent2( Format( 'Actualizar Empresa: %s', [ Nombre ] ) );
          LogIt( Format( 'Base De Datos: %s', [ Alias ] ) );
          LogEvent3;
     end;
     ShowStatus( 'Conectando...' );
     LogIt('');
     try
        try
           with Empresa do
           begin
                ConectarWindows( Alias, UserName, Password );
           end;

           {************ REVISION DE DECIMALES EN LA BASE DE DATOS **************}
           if DoDecimales.Checked then
           begin
                EmpezarTransaccion;
                try
                   LogEvent2( 'Revisi�n y Correci�n de Campos Decimales en las');
                   LogEvent( 'Tablas de COLABORA, KARDEX, NOMINA y PRESTACI');
                   ActualizaDecimales( Q_DECIMALES_COLABORA, 'COLABORA' );
                   ActualizaDecimales( Q_DECIMALES_KARDEX, 'KARDEX' );
                   ActualizaDecimales( Q_DECIMALES_NOMINA, 'NOMINA' );
                   ActualizaDecimales( Q_DECIMALES_PRESTA, 'PRESTACIONES' );
                   LogEvent3;
                   TerminarTransaccion( True );
                except
                      on Error: Exception do
                      begin
                           LogError( 'Error Al Eliminar Decimales', Error );
                           TerminarTransaccion( False );
                      end;
                end;
           end;

           if DoCheckPlantillas.Checked then
           begin
                {************ REVISION DE LA PLANTILLAS USADAS EN LISTADOS **************}
                RevisarPlantillas;
           end;

           {********************* REVISION DE PARAMETROS DE REPORTE **********************}
           if DoDefaults.Checked then
           begin
                EmpezarTransaccion;
                try
                   LogEvent2( 'Revisi�n y Correci�n de Par�metros de Reportes');
                   ActualizaParametrosReportes( Q_SET_DEFAULT_BOOL, 'Booleanos' );
                   ActualizaParametrosReportes( Q_SET_DEFAULT_FLOAT, 'Flotantes' );
                   ActualizaParametrosReportes( Q_SET_DEFAULT_INT, 'Enteros' );
                   ActualizaParametrosReportes( Q_SET_DEFAULT_DATE, 'Fechas' );
                   ActualizaParametrosReportes( Q_SET_DEFAULT_TEXT, 'Textos' );
                   LogEvent3;
                   TerminarTransaccion( True );
                except
                      on Error: Exception do
                      begin
                           LogError( 'Error Al Actualizar Defaults de Columnas de Reportes', Error );
                           TerminarTransaccion( False );
                      end;
                end;
           end;

           {********************* REVISION DE MOVTOS DE KARDEX **********************}
           if DoCheckKardex.Checked then
           begin
                VerificaKardex;
           end;

           {************ REVISION DE REPORTES Y POLIZAS CONTABLES **************}
           if DoCheckReports.Checked then
           begin
                {************ REVISION DE POLIZAS CONTABLES **************}
                try
                   MasterPrepara( Q_POLIZAS_COUNT );
                   with tqMaster do
                   begin
                        Active := True;
                        iCuantos := Fields[ 0 ].AsInteger;
                        Active := False;
                   end;
                   if ( iCuantos > 0 ) then
                   begin
                        LogEvent2( 'P�lizas Contables con Datos Inv�lidos' );
                        LogIt( Format( 'Hay P�lizas con Datos Inv�lidos En %d F�rmulas', [ iCuantos ] ) );
                        LogIt( 'Estas P�lizas tienen F�rmulas Con Referencias a COLABORA, NOMINA, PERIODO' );
                        LogIt( 'P�lizas Por Corregir:' );
                        LogIt( '' );
                        MasterPrepara( Q_POLIZAS_LIST );
                        with tqMaster do
                        begin
                             Active := True;
                             iReporte := -1;
                             while not Eof do
                             begin
                                  if ( iReporte <> FieldByName( 'RE_CODIGO' ).AsInteger ) then
                                  begin
                                       iReporte := FieldByName( 'RE_CODIGO' ).AsInteger;
                                       LogIt( '' );
                                       LogIt( Format( 'POLIZA %d = %s ( Clasificaci�n %s ):', [ iReporte,
                                                                                                FieldByName( 'RE_NOMBRE' ).AsString,
                                                                                                ZetaCommonLists.ObtieneElemento( lfClasifiReporte, FieldByName( 'RE_CLASIFI' ).AsInteger ) ] ) );
                                  end;
                                  LogIt( Format( 'Cuenta %s, F�rmula %s', [ FieldByName( 'CR_TITULO' ).AsString,
                                                                            FieldByName( 'CR_FORMULA' ).AsString ] ) );
                                  Next;
                             end;
                             LogEvent3;
                             Active := False;
                        end;
                   end;
                except
                      on Error: Exception do
                      begin
                           LogError( 'Error Al Buscar P�lizas Con Datos Inv�lidos', Error );
                      end;
                end;
                {************ REVISION DE IMPRESION RAPIDA **************}
                { RENGLONES QUE SE SALEN DEL LIMITE ESPECIFICADO }
                try
                   MasterPrepara( Q_RAPIDAS_COUNT );
                   with tqMaster do
                   begin
                        Active := True;
                        iCuantos := Fields[ 0 ].AsInteger;
                        Active := False;
                   end;
                   if ( iCuantos > 0 ) then
                   begin
                        LogEvent2( 'Reportes de Impresi�n R�pida Con Problemas' );
                        LogIt( 'Estos Reportes De Impresi�n R�pida Tienen Renglones Traslapados' );
                        LogIt( 'En Las Bandas de Encabezado, Detalle � Pie De P�gina' );
                        LogIt( 'Reportes Por Corregir:' );
                        MasterPrepara( Q_RAPIDAS_LIST );
                        with tqMaster do
                        begin
                             Active := True;
                             iReporte := -1;
                             while not Eof do
                             begin
                                  if ( iReporte <> FieldByName( 'RE_CODIGO' ).AsInteger ) then
                                  begin
                                       iReporte := FieldByName( 'RE_CODIGO' ).AsInteger;
                                       LogIt( '' );
                                       LogIt( StringOfChar( '-', K_DIVISION ) );
                                       LogIt( Format( 'Reporte %d = %s ( Clasificaci�n %s )', [ iReporte,
                                                                                  FieldByName( 'RE_NOMBRE' ).AsString,
                                                                                  ZetaCommonLists.ObtieneElemento( lfClasifiReporte, FieldByName( 'Re_CLASIFI' ).AsInteger ) ] ) );
                                       LogIt( Format( 'Pesta�a Impresora: Forma = %d L�neas, Encabezado = %d L�neas, Detalle = %d L�neas, Pie = %d L�neas ',
                                                      [ FieldByName( 'LINEAS_X_FORMA' ).AsInteger,
                                                        FieldByName( 'LINEAS_ENCABEZADO' ).AsInteger,
                                                        FieldByName( 'LINEAS_DETALLE' ).AsInteger,
                                                        FieldByName( 'LINEAS_PIE' ).AsInteger ] ) );
                                  end;
                                  LogIt( Format( '%s - Columna: %s = %s, Rengl�n: %d, Columna: %d ', [ ZetaCommonLists.ObtieneElemento( lfTipoBanda, FieldByName( 'BANDA' ).AsInteger ),
                                                                                                       FieldByName( 'CR_TITULO' ).AsString,
                                                                                                       FieldByName( 'CR_FORMULA' ).AsString,
                                                                                                       FieldByName( 'RENGLON' ).AsInteger,
                                                                                                       FieldByName( 'COLUMNA' ).AsInteger ] ) );
                                  Next;
                             end;
                             LogEvent3;
                             Active := False;
                        end;
                   end;
                except
                      on Error: Exception do
                      begin
                           LogError( 'Error Al Buscar Reportes de Impresi�n R�pida Con Problemas', Error );
                      end;
                end;
                {************ REVISION DE IMPRESION RAPIDA **************}
                { RENGLONES DEL DETALLE QUE EMPIEZAN A PARTIR DEL RENGLON 2 }
                try
                   MasterPrepara( Q_RAPIDAS_RENGLON_COUNT );
                   with tqMaster do
                   begin
                        Active := True;
                        iCuantos := Fields[ 0 ].AsInteger;
                        Active := False;
                   end;
                   if ( iCuantos > 0 ) then
                   begin
                        LogEvent2( 'Reportes de Impresi�n R�pida Con Problemas' );
                        LogIt( 'Estos Reportes De Impresi�n R�pida Tienen Renglones' );
                        LogIt( 'En La Banda de Detalle Que NO Comienzan en la Posici�n 1' );
                        LogIt( 'Reportes Por Corregir:' );
                        LogIt('');
                        MasterPrepara( Q_RAPIDAS_RENGLON );
                        with tqMaster do
                        begin
                             Active := True;
                             iReporte := -1;
                             while not Eof do
                             begin
                                  if ( iReporte <> FieldByName( 'RE_CODIGO' ).AsInteger ) then
                                  begin
                                       iReporte := FieldByName( 'RE_CODIGO' ).AsInteger;
                                       sMensaje := Format( 'Reporte %d = %s ( Clasificaci�n %s )', [ iReporte,
                                                                                       FieldByName( 'RE_NOMBRE' ).AsString,
                                                                                       ZetaCommonLists.ObtieneElemento( lfClasifiReporte, FieldByName( 'Re_CLASIFI' ).AsInteger ) ] );
                                  end;
                                  Next;
                                  if iReporte <> FieldByName( 'RE_CODIGO' ).AsInteger then
                                  begin
                                       LogIt( sMensaje );
                                  end;
                             end;
                             LogEvent3;
                             Active := False;
                        end;
                   end;
                except
                      on Error: Exception do
                      begin
                           LogError( 'Error Al Buscar Reportes de Impresi�n R�pida Con Problemas', Error );
                      end;
                end;
                RevisarReportes( 'Con @FILTRO', Q_ARROBA_COUNT, Q_ARROBA_LIST );
                RevisarReportes( 'Con IF() y/o CASE()', Q_IFCASE_COUNT, Q_IFCASE_LIST );
                RevisarReportes( 'Sin Tabla Principal', Q_SINTABLA_COUNT, Q_SINTABLA_LIST );
           end;

           { ********** REVISION DE CONCEPTOS DE NOMINA ************* }
           if DoCheckConceptos.Checked then
           begin
                RevisarConceptos( 'F�rmula De C�lculo', 'CO_FORMULA' );
                RevisarConceptos( 'F�rmula Exento ISPT', 'CO_X_ISPT' );
                RevisarConceptos( 'F�rmula ISPT Individual', 'CO_IMP_CAL' );
                RevisarConceptos( 'F�rmula Del Recibo', 'CO_RECIBO' );
                RevisarAhorros;
                RevisarPrestamos;
           end;
        except
              on Error: Exception do
              begin
                   LogError( Format( 'Error Al Actualizar Empresa: %s', [ Empresa.Nombre ] ), Error );
              end;
        end;
        ShowStatus( '' );
     finally
            DesconectarWindows;
     end;
end;

procedure TUpgrader.ActualizaParametrosReportes(const sScript, sTipo : string);
begin
     ShowStatus( Format('Defaults %s',[sTipo]) );
     MasterEjecuta( sScript );
     LogIt( Format('Los Reportes que Contienen Par�metros %s han Sido Corregidos', [sTipo]));
end;

procedure TUpgrader.ActualizaDecimales(const sScript, sTabla : string);
begin
     ShowStatus( Format('Decimales de %s', [sTabla] ));
     MasterEjecuta( sScript );
     LogIt( Format('Los Campos con Decimales de la Tabla %s ha Sido Corregida', [sTabla] ) );
end;

procedure TUpgrader.RevisarPlantillas;
 const
     Q_PLANTILLAS_GLOBAL = 'SELECT GL_FORMULA FROM GLOBAL WHERE GL_CODIGO = %d';
     Q_PLANTILLAS_COUNT = 'SELECT COUNT(*) FROM REPORTE WHERE RE_REPORTE IS NOT NULL AND RE_TIPO = 0 ';
     Q_PLANTILLAS = 'SELECT DISTINCT(UPPER(RE_REPORTE)) RE_REPORTE FROM REPORTE ' +
                    'WHERE RE_REPORTE IS NOT NULL AND RE_TIPO = 0 ';

 var Plantilla : TPlantillas;
     iCuantos: integer;
     lPrimerMSg : Boolean;
     sMsg : string;
begin
     try
        Plantilla := TPlantillas.Create( self );
        try
           MasterPrepara( Q_PLANTILLAS_COUNT );
           with tqMaster do
           begin
                Active := True;
                iCuantos := Fields[ 0 ].AsInteger;
                Active := False;
           end;

           if ( iCuantos > 0 ) then
           begin
                LogEvent2( 'Revisi�n de Plantillas que NO Contienen la Banda de Detalle' );
                Logit( Format( 'Hay %d Listados Que Usan Diferentes Plantillas', [ iCuantos ] ) );
                LogIt( 'A Estos Listados Se Les Revisar� Que Su Plantilla Contenga La Banda De Detalle' );
                LogIt( '' );
                MasterPrepara( Format( Q_PLANTILLAS_GLOBAL, [ZGlobalTress.K_GLOBAL_DIR_PLANT]) );
                with tqMaster do
                begin
                     Active := True;
                     Plantilla.Directorio := Fields[ 0 ].AsString;
                     Active := False;
                end;

                Plantilla.BackupDir := 'BackupOriginales';
                MasterPrepara( Q_PLANTILLAS );
                lPrimerMSg := FALSE;
                with tqMaster do
                begin
                     Active := True;
                     while not Eof do
                     begin
                          sMsg := Plantilla.CargaPlantilla( FieldByName( 'RE_REPORTE' ).AsString );
                          if StrLleno( sMsg ) then
                          begin
                               if NOT lPrimerMSg then
                               begin
                                    LogIt( 'Plantillas Corregidas:' );
                                    LogIt( '' );
                                    lPrimerMSg := TRUE;
                               end;
                               LogIt( sMsg );
                          end;
                          Next;
                     end;
                     Active := False;
                     if NOT lPrimerMSg then
                        LogIt( 'No se Encontr� Ninguna Plantilla con Errores' );
                end;
                LogEvent3;
           end;
        except
              on Error: Exception do
              begin
                   LogError( 'Error Al Revisar las Plantillas', Error );
              end;
        end;
     finally
            FreeAndNil( Plantilla );
     end;
end;

procedure TUpgrader.VerificaKardex;
const
     K_MARCA = 'MIGRACION 1.3.x a 2.x';
     K_CAMPOS = 'CB_CODIGO, CB_ACTIVO, CB_AUTOSAL, CB_CLASIFI, CB_CONTRAT, '+
                'CB_FEC_ANT, CB_FEC_CON, CB_FEC_ING, CB_FEC_INT, '+
                'CB_FEC_REV, CB_FEC_BAJ, CB_FEC_BSS, CB_MOT_BAJ, '+
                'CB_NIVEL1, CB_NIVEL2, CB_NIVEL3, CB_NIVEL4, CB_NIVEL5, CB_NIVEL6, CB_NIVEL7, CB_NIVEL8, CB_NIVEL9, '+
                'CB_NOMNUME, CB_NOMTIPO, CB_NOMYEAR, CB_PATRON, '+
                'CB_PER_VAR, CB_PRE_INT, CB_PUESTO, CB_SAL_INT, '+
                'CB_SAL_TOT, CB_SALARIO, CB_TABLASS, CB_TOT_GRA, CB_TURNO, '+
                'CB_ZONA_GE, CB_OLD_SAL, CB_OLD_INT, CB_FAC_INT';
var
   dAlta, dBaja: TDate;
   lActivo: Boolean;

function DeterminaAltaBaja: Boolean;
begin
     with tqUno do
     begin
          dAlta := FieldByName( 'CB_FEC_ING' ).AsDateTime;
          dBaja := FieldByName( 'CB_FEC_BAJ' ).AsDateTime;
          lActivo := ZetaCommonTools.zStrToBool( FieldByName( 'CB_ACTIVO' ).AsString );
     end;
     if not lActivo then
     begin
          if ( dBaja = NullDateTime ) then
             dBaja := dAlta;
     end;
     Result := ( dAlta <> NullDateTime );
end;

function ConvierteNivelKardex( const sTipo: String ): Integer;
begin
     if ( sTipo = K_T_ALTA ) then
        Result := 0
     else
     if ( sTipo = K_T_TAS_INF ) then
        Result := 1
     else
     if ( sTipo = K_T_EVENTO ) then
        Result := 1
     else
     if ( sTipo = K_T_PUESTO ) then
        Result := 2
     else
     if ( sTipo = K_T_AREA ) then
        Result := 3
     else
     if ( sTipo = K_T_TURNO ) then
        Result := 4
     else
     if ( sTipo = K_T_RENOVA ) then
        Result := 5
     else
     if ( sTipo = K_T_PRESTA ) then
        Result := 6
     else
     if ( sTipo = K_T_CAMBIO ) then
        Result := 7
     else
     if ( sTipo = K_T_CIERRA ) then
        Result := 8
     else
     if ( sTipo = K_T_BAJA ) then
        Result := 9
     else
         Result := 1;
end;

procedure AgregaBaja( const iEmpleado: TNumEmp );
begin
     with tqTres do
     begin
          ParamByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
          ParamByName( 'CB_TIPO' ).AsString := K_T_BAJA;
          ParamByName( 'CB_NIVEL' ).AsInteger := ConvierteNivelKardex( K_T_BAJA );
          ParamByName( 'CB_FECHA' ).AsDate := dBaja;
          ParamByName( 'CB_FECHA_2' ).AsDate := tqUno.FieldByName( 'CB_FEC_BSS' ).AsDateTime;
          ParamByName( 'CB_NOMYEAR' ).AsInteger := tqUno.FieldByName( 'CB_NOMYEAR' ).AsInteger;
          ParamByName( 'CB_NOMTIPO' ).AsInteger := tqUno.FieldByName( 'CB_NOMTIPO' ).AsInteger;
          ParamByName( 'CB_NOMNUME' ).AsInteger := tqUno.FieldByName( 'CB_NOMNUME' ).AsInteger;
          ParamByName( 'CB_MOT_BAJ' ).AsString := tqUno.FieldByName( 'CB_MOT_BAJ' ).AsString;
          ParamByName( 'CB_COMENTA' ).AsString := K_MARCA;
          ExecSQL;
     end;
     LogIt( 'Kardex: BAJA Agregada Al Empleado ' + IntToStr( iEmpleado ) );
end;

procedure AgregaAlta( const iEmpleado: Integer );
begin
     with tqDos do
     begin
          ParamByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
          ParamByName( 'CB_TIPO' ).AsString := K_T_ALTA;
          ParamByName( 'CB_NIVEL' ).AsInteger := ConvierteNivelKardex( K_T_ALTA );
          ParamByName( 'CB_FECHA' ).AsDate := dAlta;
          ParamByName( 'CB_AUTOSAL' ).AsString := tqUno.FieldByName( 'CB_AUTOSAL' ).AsString;
          ParamByName( 'CB_CLASIFI' ).AsString := tqUno.FieldByName( 'CB_CLASIFI' ).AsString;
          ParamByName( 'CB_CONTRAT' ).AsString := tqUno.FieldByName( 'CB_CONTRAT' ).AsString;
          ParamByName( 'CB_FEC_ANT' ).AsDate := tqUno.FieldByName( 'CB_FEC_ANT' ).AsDateTime;
          ParamByName( 'CB_FEC_CON' ).AsDate := tqUno.FieldByName( 'CB_FEC_CON' ).AsDateTime;
          ParamByName( 'CB_FEC_ING' ).AsDate := dAlta;
          ParamByName( 'CB_FEC_INT' ).AsDate := tqUno.FieldByName( 'CB_FEC_INT' ).AsDateTime;
          ParamByName( 'CB_FEC_REV' ).AsDate := tqUno.FieldByName( 'CB_FEC_REV' ).AsDateTime;
          ParamByName( 'CB_NIVEL1' ).AsString := tqUno.FieldByName( 'CB_NIVEL1' ).AsString;
          ParamByName( 'CB_NIVEL2' ).AsString := tqUno.FieldByName( 'CB_NIVEL2' ).AsString;
          ParamByName( 'CB_NIVEL3' ).AsString := tqUno.FieldByName( 'CB_NIVEL3' ).AsString;
          ParamByName( 'CB_NIVEL4' ).AsString := tqUno.FieldByName( 'CB_NIVEL4' ).AsString;
          ParamByName( 'CB_NIVEL5' ).AsString := tqUno.FieldByName( 'CB_NIVEL5' ).AsString;
          ParamByName( 'CB_NIVEL6' ).AsString := tqUno.FieldByName( 'CB_NIVEL6' ).AsString;
          ParamByName( 'CB_NIVEL7' ).AsString := tqUno.FieldByName( 'CB_NIVEL7' ).AsString;
          ParamByName( 'CB_NIVEL8' ).AsString := tqUno.FieldByName( 'CB_NIVEL8' ).AsString;
          ParamByName( 'CB_NIVEL9' ).AsString := tqUno.FieldByName( 'CB_NIVEL9' ).AsString;
          ParamByName( 'CB_PATRON' ).AsString := tqUno.FieldByName( 'CB_PATRON' ).AsString;
          ParamByName( 'CB_PER_VAR' ).AsFloat := tqUno.FieldByName( 'CB_PER_VAR' ).AsFloat;
          ParamByName( 'CB_PRE_INT' ).AsFloat := tqUno.FieldByName( 'CB_PRE_INT' ).AsFloat;
          ParamByName( 'CB_PUESTO' ).AsString := tqUno.FieldByName( 'CB_PUESTO' ).AsString;
          ParamByName( 'CB_SAL_INT' ).AsFloat := tqUno.FieldByName( 'CB_SAL_INT' ).AsFloat;
          ParamByName( 'CB_SAL_TOT' ).AsFloat := tqUno.FieldByName( 'CB_SAL_TOT' ).AsFloat;
          ParamByName( 'CB_SALARIO' ).AsFloat := tqUno.FieldByName( 'CB_SALARIO' ).AsFloat;
          ParamByName( 'CB_TABLASS' ).AsString := tqUno.FieldByName( 'CB_TABLASS' ).AsString;
          ParamByName( 'CB_TOT_GRA' ).AsFloat := tqUno.FieldByName( 'CB_TOT_GRA' ).AsFloat;
          ParamByName( 'CB_TURNO' ).AsString := tqUno.FieldByName( 'CB_TURNO' ).AsString;
          ParamByName( 'CB_ZONA_GE' ).AsString := tqUno.FieldByName( 'CB_ZONA_GE' ).AsString;
          ParamByName( 'CB_OLD_SAL' ).AsFloat := tqUno.FieldByName( 'CB_OLD_SAL' ).AsFloat;
          ParamByName( 'CB_OLD_INT' ).AsFloat := tqUno.FieldByName( 'CB_OLD_INT' ).AsFloat;
          ParamByName( 'CB_FAC_INT' ).AsFloat := tqUno.FieldByName( 'CB_FAC_INT' ).AsFloat;
          ParamByName( 'CB_COMENTA' ).AsString := K_MARCA;
          ExecSQL;
     end;
     LogIt( 'Kardex: ALTA Agregada Al Empleado ' + IntToStr( iEmpleado ) );
end;

procedure CreaAltaBaja( const iEmpleado: Integer );
begin
     if DeterminaAltaBaja then
     begin
          EmpezarTransaccion;
          try
             AgregaAlta( iEmpleado );
             if not lActivo then
                AgregaBaja( iEmpleado );
             TerminarTransaccion( True );
          except
                on Error: Exception do
                begin
                     TerminarTransaccion( False );
                     LogError( 'Error Al Crear Alta/Baja de Kardex Empleado ' + IntToStr( iEmpleado ), Error );
                end;
          end;
     end
     else
     begin
          LogIt( 'Empleado ' + IntToStr( iEmpleado ) + ' Sin Kardex: Fecha De Ingreso Vac�a' );
     end;
end;

procedure AgregaAltaBaja( const iEmpleado: Integer );
var
   dKardexAlta, dKardexBaja: TDate;
   lUltimoEsBaja: Boolean;
begin
     with tqUno do
     begin
          Active := False;
          ParamByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
          Active := True;
          if DeterminaAltaBaja then
          begin
               dKardexAlta := NullDateTime;
               dKardexBaja := NullDateTime;
               lUltimoEsBaja := False;
               with tqSeis do
               begin
                    Active := False;
                    ParamByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
                    Active := True;
                    if not IsEmpty then
                    begin
                         dKardexAlta := FieldByName( 'CB_FECHA' ).AsDateTime;
                         while not Eof do
                         begin
                              lUltimoEsBaja := ( FieldByName( 'CB_TIPO' ).AsString = K_T_BAJA );
                              dKardexBaja := FieldByName( 'CB_FECHA' ).AsDateTime;
                              Next;
                         end;
                    end;
                    Active := False;
               end;
               if ( dAlta > dKardexAlta ) then
               begin
                    LogIt( 'Empleado ' + IntToStr( iEmpleado ) + ' Sin ALTA Inicial: Fecha De Ingreso Posterior Al Primer Movimiento en KARDEX' )
               end
               else
                   if not lActivo and not lUltimoEsBaja and ( dBaja < dKardexBaja ) then
                   begin
                        LogIt( 'Empleado ' + IntToStr( iEmpleado ) + ' Sin BAJA Final: Fecha De Baja Anterior Al Ultimo Movimiento en KARDEX' )
                   end
                   else
                   begin
                        EmpezarTransaccion;
                        try
                           AgregaAlta( iEmpleado );
                           if not lActivo and not lUltimoEsBaja then
                              AgregaBaja( iEmpleado );
                           with tqCuatro do
                           begin
                                ParamByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
                                ExecSQL;
                           end;
                           TerminarTransaccion( True );
                        except
                              on Error: Exception do
                              begin
                                   TerminarTransaccion( False );
                                   LogError( 'Error Al Crear Alta/Baja de Kardex Empleado ' + IntToStr( iEmpleado ), Error );
                              end;
                        end;
                   end;
          end
          else
          begin
               LogIt( 'Empleado ' + IntToStr( iEmpleado ) + ' Sin ALTA Inicial: Fecha De Ingreso Vac�a' );
          end;
          Active := False;
     end;
end;

procedure CloseQuery( Query: TQuery );
begin
     if ( Query <> nil ) then
     begin
          with Query do
          begin
               Active := False;
               if Prepared then
                  UnPrepare;
          end;
     end;
end;

 var iCount : integer;
begin

     inherited;
     LogEvent2('Verificaci�n de los Movimientos de Kardex');
     ShowStatus('Verificaci�n de los Movimientos de Kardex');
     CloseQuery( tqUno );
     PreparaQuery( tqUno, 'select ' + K_CAMPOS + ' from COLABORA where ( ( select COUNT(*) from KARDEX K where ( K.CB_CODIGO = COLABORA.CB_CODIGO ) ) = 0 )' );
     PreparaQuery( tqDos, 'insert into KARDEX( CB_CODIGO, '+
                                              'CB_TIPO, '+
                                              'CB_NIVEL, '+
                                              'CB_FECHA, '+
                                              'CB_AUTOSAL, '+
                                              'CB_CLASIFI, '+
                                              'CB_CONTRAT, '+
                                              'CB_FEC_ANT, '+
                                              'CB_FEC_CON, '+
                                              'CB_FEC_ING, '+
                                              'CB_FEC_INT, '+
                                              'CB_FEC_REV, '+
                                              'CB_NIVEL1, '+
                                              'CB_NIVEL2, '+
                                              'CB_NIVEL3, '+
                                              'CB_NIVEL4, '+
                                              'CB_NIVEL5, '+
                                              'CB_NIVEL6, '+
                                              'CB_NIVEL7, '+
                                              'CB_NIVEL8, '+
                                              'CB_NIVEL9, '+
                                              'CB_PATRON, '+
                                              'CB_PER_VAR, '+
                                              'CB_PRE_INT, '+
                                              'CB_PUESTO, '+
                                              'CB_SAL_INT, '+
                                              'CB_SAL_TOT, '+
                                              'CB_SALARIO, '+
                                              'CB_TABLASS, '+
                                              'CB_TOT_GRA, '+
                                              'CB_TURNO, '+
                                              'CB_ZONA_GE, '+
                                              'CB_OLD_SAL, '+
                                              'CB_OLD_INT, '+
                                              'CB_FAC_INT, '+
                                              'CB_COMENTA ) values ('+
                                              ':CB_CODIGO, '+
                                              ':CB_TIPO, '+
                                              ':CB_NIVEL, '+
                                              ':CB_FECHA, '+
                                              ':CB_AUTOSAL, '+
                                              ':CB_CLASIFI, '+
                                              ':CB_CONTRAT, '+
                                              ':CB_FEC_ANT, '+
                                              ':CB_FEC_CON, '+
                                              ':CB_FEC_ING, '+
                                              ':CB_FEC_INT, '+
                                              ':CB_FEC_REV, '+
                                              ':CB_NIVEL1, '+
                                              ':CB_NIVEL2, '+
                                              ':CB_NIVEL3, '+
                                              ':CB_NIVEL4, '+
                                              ':CB_NIVEL5, '+
                                              ':CB_NIVEL6, '+
                                              ':CB_NIVEL7, '+
                                              ':CB_NIVEL8, '+
                                              ':CB_NIVEL9, '+
                                              ':CB_PATRON, '+
                                              ':CB_PER_VAR, '+
                                              ':CB_PRE_INT, '+
                                              ':CB_PUESTO, '+
                                              ':CB_SAL_INT, '+
                                              ':CB_SAL_TOT, '+
                                              ':CB_SALARIO, '+
                                              ':CB_TABLASS, '+
                                              ':CB_TOT_GRA, '+
                                              ':CB_TURNO, '+
                                              ':CB_ZONA_GE, '+
                                              ':CB_OLD_SAL, '+
                                              ':CB_OLD_INT, '+
                                              ':CB_FAC_INT, '+
                                              ':CB_COMENTA )' );
     PreparaQuery( tqTres, 'insert into KARDEX( CB_CODIGO, '+
                                               'CB_TIPO, '+
                                               'CB_NIVEL, '+
                                               'CB_FECHA, '+
                                               'CB_FECHA_2, '+
                                               'CB_MOT_BAJ, '+
                                               'CB_NOMYEAR, '+
                                               'CB_NOMTIPO, '+
                                               'CB_NOMNUME, '+
                                               'CB_COMENTA ) values ( '+
                                               ':CB_CODIGO, '+
                                               ':CB_TIPO, '+
                                               ':CB_NIVEL, '+
                                               ':CB_FECHA, '+
                                               ':CB_FECHA_2, '+
                                               ':CB_MOT_BAJ, '+
                                               ':CB_NOMYEAR, '+
                                               ':CB_NOMTIPO, '+
                                               ':CB_NOMNUME, '+
                                               ':CB_COMENTA )' );
     PreparaQuery( tqCuatro, 'execute procedure RECALCULA_KARDEX( :CB_CODIGO )' );

     iCount := FLog.Count;
     { Agregar movimiento de Alta a aquellos empleados SIN registros en la tabla KARDEX }
     try
        ShowStatus('Agregando movimientos de Alta a aquellos empleados SIN registros en la tabla KARDEX ');
        with tqUno do
        begin
             Active := True;
             while not Eof do
             begin
                  CreaAltaBaja( FieldByName( 'CB_CODIGO' ).AsInteger );
                  Next;
             end;
             Active := False;
        end;
      except
           on Error: Exception do
           begin
                LogError( 'Error Al Examinar Kardex', Error );
           end;
      end;
      CloseQuery( tqUno );

      { Al recalcular el Kardex, el StoredProcedure reporta los empleados }
      { quienes, teniendo registros en la tabla KARDEX, el primer movimiento }
      { NO es KARDEX.CB_TIPO = 'ALTA' }
      PreparaQuery( tqUno, 'select ' + K_CAMPOS + ' from COLABORA where ( CB_CODIGO = :CB_CODIGO )' );
      PreparaQuery( tqCinco, 'select CB_CODIGO, PROBLEMA from RECALCULA_TODOS' );
      PreparaQuery( tqSeis, 'select CB_FECHA, CB_TIPO from KARDEX where ( CB_CODIGO = :CB_CODIGO ) order by CB_FECHA, CB_NIVEL' );
      try
        ShowStatus('Ejecutando RECALCULA_TODOS, este proceso tomar� varios minutos...');
        with tqCinco do
        begin
             Active := True;
             while not Eof do
             begin
                  ShowStatus('Agregando movimientos de a aquellos empleados con problemas en la tabla KARDEX ');
                  AgregaAltaBaja( FieldByName( 'CB_CODIGO' ).AsInteger );
                  Next;
             end;
             Active := False;
        end;
      except
           on Error: Exception do
           begin
                LogError( 'Error Al Recalcular Kardex', Error );
           end;
      end;

      if iCount = FLog.Count then
         LogIt('No se Encontr� Ning�n Problema con el Kardex de los Empleados');

      CloseQuery( tqSeis );
      CloseQuery( tqCinco );
      CloseQuery( tqCuatro );
      CloseQuery( tqTres );
      CloseQuery( tqDos );
      CloseQuery( tqUno );
end;

{ ********* Eventos ********** }

procedure TUpgrader.ShowStatus( const sMsg: String );
begin
     StatusBar.SimpleText := sMsg;
end;

procedure TUpgrader.OKClick(Sender: TObject);
var
   iPtr: Integer;
   oCursor: TCursor;
begin
     with Companias do
     begin
          if ( SelCount > 0 ) then
             iPtr := Selected.StateIndex
          else
              iPtr := -1;
     end;
     if ( iPtr < 0 ) then
        ZetaDialogo.zError( '� Error !', 'No Hay Ninguna Empresa Seleccionada', 0 )
     else
     begin
          if ZetaDialogo.zConfirm( '� Atenci�n !', Format( '� Desea Actualizar La Empresa "%s" ?', [ Companies[ iPtr ].Nombre ] ), 0, mbNo ) then
          begin
               oCursor := Screen.Cursor;
               Screen.Cursor := crHourglass;
               try
                  InitLog;
                  Actualizar( Companies[ iPtr ] );
                  CloseLog( 'ACTUALIZADOR.LOG' );
               finally
                      Screen.Cursor := oCursor;
               end;
          end
     end;
end;

procedure TUpgrader.SalirClick(Sender: TObject);
begin
     Close;
end;

procedure TUpgrader.BitBtn1Click(Sender: TObject);
begin
     if Ayuda = NIL then
        Ayuda := TAyuda.Create( self );
        
     Ayuda.ShowModal;
end;

end.
