unit FAyuda;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, Buttons, ExtCtrls, ZBaseDlgModal ;

type
  TAyuda = class(TZetaDlgModal)
    Panel2: TPanel;
    RichEdit: TRichEdit;
    Imprimir: TBitBtn;
    PrintDialog: TPrintDialog;
    procedure FormCreate(Sender: TObject);
    procedure ImprimirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure rtfIntroduccion;
    procedure rtfDecimales;
    procedure rtfKardex;
    procedure rtfParametrosReportes;
    procedure rtfRevisarReportes;
    procedure rtfRevisarPlantillas;
    procedure rtfRevisarConceptos;
    procedure Agrega(const sTexto: string);
    procedure Bold;
    procedure Normal;
    procedure TamanoLetra(const iLetra: integer);
    procedure SubTitulo(const sTexto: string);
    procedure Titulo(const sTexto: string);
    procedure SubTitulo2(const sTexto: string);
    procedure Reminder;
    procedure SetColor(oColor: TColor);
    procedure Asesor;
    procedure Underline;
  public
    { Public declarations }
  end;

var
  Ayuda: TAyuda;

implementation

uses ZetaCommonClasses;

{$R *.DFM}

procedure TAyuda.FormCreate(Sender: TObject);
begin
     with RichEdit.Lines do
     begin
          BeginUpdate;
          Clear;
          rtfIntroduccion;
          rtfDecimales;
          rtfKardex;
          rtfParametrosReportes;
          rtfRevisarReportes;
          rtfRevisarPlantillas;
          rtfRevisarConceptos;
          EndUpdate;
     end;
end;

procedure TAyuda.FormShow(Sender: TObject);
begin
     inherited;
     ActiveControl := RichEdit;
     RichEdit.SelStart := 0;
     RichEdit.SelLength := 0;
end;

procedure TAyuda.SetColor( oColor: TColor );
begin
     RichEdit.SelAttributes.Color := oColor;
end;

procedure TAyuda.Underline;
begin
     RichEdit.SelAttributes.Style := [ fsUnderline ];
end;

procedure TAyuda.Bold;
begin
     RichEdit.SelAttributes.Style := [ fsBold ];
end;

procedure TAyuda.Normal;
begin
     with RichEdit.SelAttributes do
     begin
          Style := [];
          Size := 8;
     end;
end;

procedure TAyuda.TamanoLetra(const iLetra : integer);
begin
     RichEdit.SelAttributes.Size := iLetra;
end;

procedure TAyuda.Agrega(const sTexto : string);
begin
     RichEdit.Lines.Add(sTexto);
end;

procedure TAyuda.Titulo(const sTexto : string);
begin
     Agrega('');
     Agrega('');
     SetColor( clNavy );
     RichEdit.SelAttributes.Style := [fsBold,fsUnderline];
     TamanoLetra(10);
     Agrega(sTexto);
     SetColor( clBlack );
     Normal;
     Agrega('');
end;

procedure TAyuda.SubTitulo(const sTexto : string);
begin
     Bold;
     Agrega(sTexto);
     Normal;
end;

procedure TAyuda.SubTitulo2(const sTexto : string);
begin
     Agrega('');
     RichEdit.SelAttributes.Style := [fsBold,fsUnderline];
     Agrega(sTexto);
     Normal;
end;


procedure TAyuda.Asesor;
begin
     RichEdit.SelAttributes.Style := [ fsBold, fsUnderline ];
     SetColor( clPurple );
     Agrega( 'Actividades que Debe de Realizar el Asesor' );
     SetColor( clBlack );
     Normal;
end;

procedure TAyuda.Reminder;
begin
     Agrega( '' );
     SetColor( clRed );
     Agrega('Recordar que NO TODAS las f�rmulas reportadas tienen alg�n error');
     SetColor( clBlack );
     Agrega( '' );
end;

procedure TAyuda.rtfIntroduccion;
begin
     RichEdit.SelAttributes.Style := [fsBold,fsUnderline];
     TamanoLetra(12);
     Agrega('Actualizador de Tress 1.3.x a Tress 2.x');
     Normal;
     Titulo('Introducci�n');
     Agrega('Este programa ayuda a mininizar los cambios manuales que se tienen que realizar en una migraci�n de la versi�n 1.3.x a la versi�n 2.x; para lograr tal objetivo se llevan acabo los siguientes procesos:');
     Agrega('');
     Agrega('1.- Eliminaci�n de Decimales De Salarios y Otros Datos');
     Agrega('2.- Rec�lculo y Verificaci�n Kardex De Empleados');
     Agrega('3.- Especificar Defaults De Par�metros De Reportes');
     Agrega('4.- Revisar Reportes');
     Agrega('5.- Revisar Plantillas');
     Agrega('');
     Agrega('A continuaci�n se explica cada uno de los puntos mencionados anteriormente,'+
            ' en algunos casos se explica a detalle el proceso que se va a realizar.');
     Agrega('Para algunos puntos, sobre todo aquellos que involucran al reporteador,' +
            'es necesario que el asesor haga algunas modificaciones, si esto es necesario,' +
            'se indica bajo el rublo de' );
     Asesor;
end;

procedure TAyuda.rtfDecimales;
begin
     Titulo('Eliminaci�n de Decimales De Salarios y Otros Datos');
     SubTitulo('Objetivo:');
     Agrega('Revisar los campos que contienen decimales de las tablas: COLABORA, KARDEX, NOMINA y PRESTACI, para asegurar que todos los datos num�ricos de la base de datos, est�n redondeados a dos decimales.');
     Agrega('Si este proceso no se ejecuta, puede haber diferencias de redondeos en c�lculos como Promediar Variables o Rec�lculo de Salarios Integrados (por mencionar algunos).');
end;

procedure TAyuda.rtfKardex;
begin
     Titulo('Rec�lculo y Verificaci�n Kardex De Empleados');
     SubTitulo('Objetivo:');
     Agrega('Revisar en el kardex de todos los empleados que se tengan los movimientos de ALTA y/o BAJA.');
     Agrega('Para lograr esto, se siguen las siguientes reglas:');
     Agrega('a) Si el empleado NO tiene movimiento de ALTA, se le agregar� el registro en base a su fecha de ingreso (COLABORA.CB_FEC_ING).');
     Agrega('b) Si el empleado aparece como Baja y NO tiene su registro de BAJA en Kardex, �ste ser� agregado en base a su fecha de baja (COLABORA.CB_FEC_BAJ).');
     Agrega('c) Si la fecha de ingreso del empleado es mayor a la fecha del primer movimiento del kardex, se asentar� un error en la bit�cora, en este caso no se agrega ning�n registro.');
     Agrega('d) Si la fecha de baja del empleado es menor a la fecha del �ltimo movimiento del kardex, se asentar� un error en la bit�cora, en este caso no se agrega ning�n registro.');
     Agrega('');
     Bold;
     SetColor( clRed );
     Subtitulo('NOTA IMPORTANTE');
     Agrega('Se tiene que considerar que este proceso puede tomar varios minutos, incluso puede durar horas, pues se est� realizando el Rec�lculo de Kardex para cada uno de los empleados.');
     SetColor( clBlack );
     Normal;
     Agrega('');
     Asesor;
     Agrega('Revisar los casos de los empleados reportados en bit�cora en los puntos 3 y 4 y agregar manualmente los movimientos de ALTA y/o BAJA seg�n sea el caso.');
end;

procedure TAyuda.rtfParametrosReportes;
begin
     Titulo('Defaults De Par�metros De Reportes');
     SubTitulo('Objetivo:');
     Agrega('Actualizar los par�metros de los reportes a que tengan las f�rmulas defaults correspondientes. Este cambio es necesario, debido a que cambi� el uso de los par�metros de la versi�n 1.3 a la 2.x');
     Agrega('El realizar este proceso elimina el tener que revisar los reportes que usan par�metros y certificar que funcionen correctamente.');
end;

procedure TAyuda.rtfRevisarPlantillas;
begin
     Titulo('Revisi�n de Plantillas');
     SubTitulo('Objetivo:');
     Agrega('Revisar aquellas plantillas que estan siendo usadas por lo menos por un listado.');
     Agrega( 'Esta revisi�n consiste en validar que la plantilla '+
             'contenga su banda de DETALLE, si la plantilla no contiene esta banda,'+
             ' se le agregar�, usando el tipo y tama�o de letra que se usa a nivel'+
             ' plantilla y se grabar� en el directorio de plantillas bajo el mismo nombre.');
     Agrega( 'La plantilla original (la que no contiene la banda de detalle)'+
             ' se guardar� en un folder llamado BackupOriginales que lo podr�'+
             ' encontrar en el mismo directorio de plantillas.');
end;

procedure TAyuda.rtfRevisarReportes;

procedure EjemploNotOK( const sMensaje: String );
begin
     Agrega( '' );
     SetColor( clRed );
     Agrega( sMensaje );
     SetColor( clBlack );
end;

procedure EjemploOK( const sMensaje: String );
begin
     Agrega( '' );
     SetColor( clGreen );
     Agrega( sMensaje );
     SetColor( clBlack );
end;

begin
     Titulo('Revisi�n de Reportes');
     SubTitulo('Objetivo:');
     Agrega('Revisar los cambios de funcionalidad en los reportes.');
     Agrega('Debido a que la versi�n 2.x utiliza nuevas herramientas en la generaci�n de reportes, se ha requerido detectar cuales de los reportes existentes en el sistema tienen alguna incompatibilidad con la nueva versi�n.');

     SubTitulo2('P�lizas con datos inv�lidos');
     Agrega('Se revisar� si las p�lizas en sus cuentas contables contienen alg�n dato incompatible con la nueva versi�n.');
     Agrega('En bit�cora se registrar�:');
     Agrega('1.- N�mero y nombre de la p�liza');
     Agrega('2.- Clasificaci�n en donde se encuentra');
     Agrega('3.- La lista de las cuentas contables con problemas');
     Agrega('');
     Asesor;
     Agrega('Para corregir estas p�lizas, SOLAMENTE se pueden usar alguna de las siguientes funciones:');
     Agrega('SUMA(), NSUMA(), CUADRE(), CPOLIZA(), GLOBAL() y/o C().');
     Agrega('Para informaci�n sobre el uso de estas funciones, consulte el Index de la ayuda en l�nea de Tress.');

     SubTitulo2('Reportes De Impresi�n R�pida que Tienen Renglones Traslapados');
     Agrega('Se requiere revisar aquellos reportes de Impresi�n R�pida '+
            'cuyos datos en el Encabezado o Pi� de P�gina hacen referencia a '+
            'renglones fuera de los l�mites definidos para Encabezado y Pi� de P�gina '+
            'respectivamente, invadiendo por lo tanto el Detalle.');
     Agrega('Por ejemplo, si el encabezado est� definido como de 15 renglones y tiene un dato '+
            'configurado para imprimirse en el rengl�n 16, el listado es incorrecto y aparecer� '+
            'en la lista de reportes que hay que corregir.');
     Agrega('NOTA: Una excepci�n es que los datos definidos en el Pi� de P�gina -exclusivamente - '+
            'pueden usar un n�mero de rengl�n que corresponda al detalle siempre y cuando la columna'+
            ' a usar sea superior a la mayor que se est� usando en el propio detalle. Estos casos, '+
            'aunque son v�lidos en 2.x, tambi�n son incluidos en la bit�cora.');
     Agrega('En bit�cora se registrar�:');
     Agrega(' 1.- N�mero y nombre del reporte');
     Agrega(' 2.- Clasificaci�n en donde se encuentra el reporte');
     Agrega(' 3.- N�mero de l�neas por forma (total del reporte)');
     Agrega(' 4.- N�mero de l�neas definidas para el Encabezado');
     Agrega(' 6.- N�mero de l�neas definidas para el Detalle');
     Agrega(' 7.- N�mero de l�neas definidas para el Pi� de P�gina');
     Agrega(' 8.- Ubicaci�n de la f�rmula incompatible. Puede ser en un campo/columna, en un encabezado o en un pie de grupo.');
     Agrega(' 9.- T�tulo de la columna que contiene la f�rmula incompatible');
     Agrega('10.- F�rmula que hay que modificar');
     Agrega('');
     Asesor;
     Agrega('Para corregir estos reportes, es necesario modificar la posici�n de los datos con el problema de manera que se respete el l�mite definido para el encabezado y el pi� de p�gina respectivamente, de manera que no invadan el detalle del reporte.');

     SubTitulo2('Reportes De Impresi�n R�pida que en su Detalle Contienen Renglones en Posiciones Diferentes a 1.');
     Agrega('Se requiere revisar aquellos reportes de Impresi�n R�pida cuyos datos en el Detalle hacen referencia a renglones con posici�n mayor a 1.');
     Agrega('Por ejemplo, en la versi�n 1.3 se pod�a tener un reporte en donde todos los datos del '+
            'detalle ten�an en el rengl�n la posici�n 2, en la versi�n anterior el detalle sal�a a'+
            ' rengl�n seguido, ahora en la versi�n 2.x sale a doble rengl�n, pues una de las'+
            ' caracter�sticas es que ya se pueden imprimir reportes con detalle en varios renglones.');
     Agrega('En bit�cora se registrar�:');
     Agrega('a) N�mero y nombre del reporte');
     Agrega('b) Clasificaci�n en donde se encuentra el reporte');
     Agrega('');
     Asesor;
     Agrega('Para corregir estos reportes, es necesario modificar la posici�n de los datos que se encuentran en el detalle, en la mayor�a de los casos se corregir� poniendo todos los renglones en la posici�n 1.');

     SubTitulo2('Reportes con @FILTRO');
     Agrega('Se revisar� cuales reportes contienen en alguna de sus f�rmulas la expresi�n @FILTRO, la cual es incompatible con la versi�n 2.x.');
     Agrega('En bit�cora se registrar�:');
     Agrega('1.- N�mero y nombre del reporte');
     Agrega('2.- Clasificaci�n en donde se encuentra el reporte');
     Agrega('3.- Tipo de reporte (Listado, Forma, Impresi�n R�pida)');
     Agrega('4.- Ubicaci�n de la f�rmula incompatible. Puede ser en un campo/columna, en un encabezado o en un pie de grupo.');
     Agrega('5.- T�tulo de la columna que contiene la f�rmula incompatible');
     Agrega('6.- F�rmula que hay que modificar');
     Agrega('');
     Asesor;
     Agrega('En general, @FILTRO se usaba principalmente en los reportes tipo ASCII y para corregirlos hay que sustituir toda la expresi�n por la funci�n SUM( X ), donde X es el monto o expresi�n que se quiere totalizar.');
     Agrega('Es importante que adem�s de la expresi�n SUM (X), se incluyan las operaciones adicionales de la f�rmula original, por ejemplo, la expresi�n');
     Agrega( '' );
     Agrega('@select SUM( NO_NETO ) ) * 100 from NOMINA where @FILTRO');
     Agrega( '' );
     Agrega('quedar�a de la siguiente manera:');
     Agrega( '' );
     Agrega('SUM( NO_NETO ) * 100' );
     Agrega( '' );
     Agrega('Para informaci�n sobre el uso de la funci�n SUM(), consulte el Index de la ayuda en l�nea de Tress.');

     SubTitulo2('Reportes sin tabla principal');
     Agrega('Se revisar�n los reportes para garantizar que todos los campos que lo '+
            'requieran contengan indicada la tabla a la que corresponde, es decir, que cumplan'+
            ' con el formato TABLA.CAMPO, por ejemplo Colabora.Cb_Checa.');
     Agrega('No todos los campos sin tabla marcan error, de manera que se registrar�n en bit�cora'+
            ' �nicamente los campos que pueden marcar error por falta de la tabla correspondiente.');
     Agrega('En bit�cora se registrar�:');
     Agrega('1.- N�mero y nombre del reporte');
     Agrega('2.- Clasificaci�n en donde se encuentra el reporte');
     Agrega('3.- Tipo de reporte (Listado, Forma, Impresi�n Rapida)');
     Agrega('4.- Ubicaci�n de la f�rmula incompatible. Puede ser en un campo/columna, en un encabezado o en un pie de grupo.');
     Agrega('5.- T�tulo de la columna que contiene la f�rmula incompatible');
     Agrega('6.- F�rmula que hay que modificar');
     Agrega('7.- F�rmula sugerida que corrige el error.');
     Agrega('');
     Asesor;
     Agrega('Para corregir estos reportes se tienen dos opciones:');
     Agrega('a) Borrar el campo, grupo o filtro que tenga el problema y volverlo a  agregar. Esta es la soluci�n m�s recomendada.');
     Agrega('b) Modificar manualmente la f�rmula bas�ndose en la f�rmula sugerida que se incluye en el reporte especial');

     SubTitulo2('Reportes con IF() y/o CASE()');
     Agrega('Se revisar�n los reportes y se reportar�n todas las f�rmulas que contengan la funci�n IF() o la funci�n CASE().');
     Agrega('En bit�cora se registrar�:');
     Agrega('   - N�mero y nombre del reporte');
     Agrega('   - Clasificaci�n en donde se encuentra');
     Agrega('   - Tipo de reporte (Listado, Forma, Impresi�n Rapida, P�liza Contable)');
     Agrega('   - Ubicaci�n de la f�rmula. Puede ser en un campo/columna, en un encabezado o en un pie de grupo.');
     Agrega('   - T�tulo de la columna que contiene la f�rmula');
     Agrega('   - F�rmula que hay que revisar');
     Agrega('');
     Asesor;
     Agrega('De las incidencias reportadas, revisar que tanto las funciones IF() y CASE() regresen siempre para todas sus condiciones el mismo tipo de dato');
     Reminder;
     Underline;
     Agrega('Ejemplos de la funci�n IF():');
     Normal;
     EjemploOK('IF(COLABORA.CB_ACTIVO="S", "Activo", "Baja") <-- CORRECTO, siempre regresa un dato de tipo TEXTO');
     EjemploOK('IF(COLABORA.CB_ACTIVO="S", COLABORA.CB_SALARIO/9.5, 0.00 ) <-- CORRECTO, siempre regresa un dato de tipo NUMERICO CON DECIMALES');
     EjemploOK('IF(COLABORA.CB_ACTIVO="S", COLABORA.CB_FEC_ING, COLABORA.CB_FEC_BAJ ) <-- CORRECTO, siempre regresa un dato de tipo FECHA');
     EjemploNotOK('IF(COLABORA.CB_ACTIVO="S", COLABORA.CB_SALARIO/9.5, '' ) <-- INCORRECTO, porque para los empleados activos regresa un NUMERO CON DECIMALES y para los que est�n dados de baja regresa un TEXTO');
     EjemploNotOK('IF(COLABORA.CB_ACTIVO="S", COLABORA.CB_SALARIO/9.5, 0 ) <-- INCORRECTO, porque para los empleados activos regresa un NUMERO CON DECIMALES y para los que est�n dados de baja regresa un NUMERO ENTERO (SIN DECIMALES)');
     EjemploOK('F�rmula CORRECTA para ambos casos --> IF(COLABORA.CB_ACTIVO="S", COLABORA.CB_SALARIO/9.5, 0.00 )');
     Agrega('');
     SetColor( clRed );
     Underline;
     Agrega('NOTA IMPORTANTE');
     Normal;
     SetColor( clBlack );
     Agrega('Es muy com�n que se usen expresiones como la siguiente:');
     Agrega('');
     Agrega('IF(COLABORA.CB_ACTIVO="S", COLABORA.CB_SALARIO/9.5, "" )');
     Agrega('');
     Agrega('en donde se quiere que aparezca vac�o en el caso de que la expresi�n sea falsa, como ya se menciona en los ejemplos anteriores, esto es INCORRECTO, en lugar de �sta expresi�n debe utilizarse');
     Agrega('');
     Agrega('IF(COLABORA.CB_ACTIVO="S", COLABORA.CB_SALARIO/9.5, 0.00 )');
     Agrega('');
     Agrega('y usar la m�scara');
     Agrega('');
     Agrega('''#,0.00;-#,0.00;#''  �  ''$#,0.00;-$#,0.00;#'' ');
     Agrega('');
     Agrega('para que deje vac�o el dato en caso de ser cero. Estas m�scaras est�n disponibles en el reporteador en la lista de m�scaras para datos NUMERICOS CON DECIMALES');
     Agrega('');
     Underline;
     Agrega('Ejemplos de la funci�n CASE():');
     Normal;
     EjemploOK('CASE( COLABORA.CB_EDO_CIV,"C","1","S","2","V","3","U","5" ) <-- CORRECTO, siempre regresa un dato de tipo TEXTO');
     EjemploOK('CASE( COLABORA.CB_EDO_CIV,"C",1,"S",2,"V",3,"U",5 ) <-- CORRECTO, siempre regresa un dato de tipo NUMERICO');
     EjemploNotOK('CASE( COLABORA.CB_EDO_CIV,"C",1,"S",2,"V","3","U","5" ) <-- INCORRECTO, porque cuando el estado civil es "C" � "S" regresa un NUMERO y cuando es "U" � "V" regresa un TEXTO.');
end;

procedure TAyuda.rtfRevisarConceptos;
begin
     Titulo('Revisi�n de Conceptos De N�mina, Ahorros y Pr�stamos' );
     Agrega('Se revisar�n los conceptos de n�mina, f�rmulas de ahorros y pr�stamos para reportar' );
     Agrega('las ocurrencias de las funciones C(), IF() y/o CASE()' );
     Agrega('En bit�cora se registrar�:');
     Agrega('1) Datos del concepto, ahorro � pr�stamos');
     Agrega('2) F�rmula que hay que revisar');
     Agrega('');
     Asesor;
     Reminder;
     Agrega('Revisar que la funci�n C() en Tress 1.3 se ejecuta igual que la funci�n CS()' );
     Agrega('mientras que en Tress 2.x se ejecuta igual que la funci�n CC()' );
     Bold;
     Agrega('por lo que hay que evaluar si requiere ser cambiada la f�rmula donde se usa C()' );
     Normal;
     Agrega('');
     Agrega('Revisar que tanto las funciones IF() y CASE() regresen siempre' );
     Agrega('para todas sus condiciones el mismo tipo de dato');

end;

procedure TAyuda.ImprimirClick(Sender: TObject);
begin
     inherited;
     PrintDialog.Execute;
     RichEdit.Print('Archivo de Ayuda');
end;

end.
