object ListaMigracion: TListaMigracion
  Left = 200
  Top = 108
  AutoScroll = False
  Caption = 'Configurar Lista De Tablas A Migrar'
  ClientHeight = 322
  ClientWidth = 455
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanelInferior: TPanel
    Left = 0
    Top = 288
    Width = 455
    Height = 34
    Align = alBottom
    TabOrder = 0
    object OK: TBitBtn
      Left = 290
      Top = 5
      Width = 75
      Height = 25
      Hint = 'Grabar Lista De Tablas A Migrar'
      Caption = '&OK'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = OKClick
      Kind = bkOK
    end
    object Cancelar: TBitBtn
      Left = 374
      Top = 5
      Width = 75
      Height = 25
      Hint = 'Salir Sin Grabar Nada'
      Caption = '&Cancelar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Kind = bkCancel
    end
  end
  object Population: TGroupBox
    Left = 0
    Top = 0
    Width = 227
    Height = 288
    Align = alLeft
    Caption = ' TablasPoblar '
    TabOrder = 1
    object Poblables: TCheckListBox
      Left = 2
      Top = 15
      Width = 223
      Height = 271
      Align = alClient
      BorderStyle = bsNone
      Color = clBtnFace
      ItemHeight = 13
      TabOrder = 0
    end
  end
  object Migration: TGroupBox
    Left = 227
    Top = 0
    Width = 228
    Height = 288
    Align = alClient
    Caption = ' Tablas A Migrar '
    TabOrder = 2
    object Migrables: TCheckListBox
      Left = 2
      Top = 15
      Width = 224
      Height = 271
      Align = alClient
      BorderStyle = bsNone
      Color = clBtnFace
      ItemHeight = 13
      TabOrder = 0
    end
  end
end
