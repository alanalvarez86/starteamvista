object Empresas: TEmpresas
  Left = 0
  Top = 0
  Width = 689
  Height = 385
  Caption = 'Empresas'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanelEmpresas: TPanel
    Left = 0
    Top = 30
    Width = 673
    Height = 278
    Align = alClient
    TabOrder = 0
    object DBGrid: TDBGrid
      Left = 1
      Top = 1
      Width = 671
      Height = 276
      Align = alClient
      DataSource = dsCompany
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ReadOnly = True
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = _EmpresaModificarExecute
      Columns = <
        item
          Expanded = False
          FieldName = 'CM_CODIGO'
          Title.Caption = 'C'#243'digo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CM_NOMBRE'
          Title.Caption = 'Nombre'
          Width = 233
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CM_TIPO_E'
          Title.Caption = 'Tipo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CM_DIGITO'
          Title.Caption = 'D'#237'gito'
          Width = 35
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CM_DATOS'
          Title.Caption = 'Base de Datos'
          Width = 245
          Visible = True
        end>
    end
  end
  object PanelToolBar: TPanel
    Left = 0
    Top = 0
    Width = 673
    Height = 30
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object BtnPrimero: TZetaSpeedButton
      Left = 5
      Top = 3
      Width = 25
      Height = 25
      Action = _EmpresaPrimera
      Flat = True
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001F7C1F7C00001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000001F7C000000001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000FF0300000000FF0300001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000FF03FF030000FF03FF03000000000000
        00001F7C1F7C1F7C1F7C1F7C0000FF03FF030000FF03FF03FF03FF03FF03FF03
        FF031F7C1F7C1F7C1F7C0000FF03FF030000FF03FF03FF03FF03FF03FF03FF03
        FF031F7C1F7C1F7C1F7C1F7C0000FF03FF030000FF03FF03FF03FF03FF03FF03
        FF031F7C1F7C1F7C1F7C1F7C1F7C0000FF03FF030000FF03FF03000000000000
        00001F7C1F7C1F7C1F7C1F7C1F7C1F7C0000FF0300000000FF0300001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000001F7C000000001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001F7C1F7C00001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      ParentShowHint = False
      ShowHint = True
    end
    object btnAnterior: TZetaSpeedButton
      Left = 30
      Top = 3
      Width = 25
      Height = 25
      Action = _EmpresaAnterior
      Flat = True
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000100200001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001002100200000000000000000000
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C000010021002100210021002100210020000
        1F7C1F7C1F7C1F7C1F7C1F7C0000100210021002100210021002100210020000
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C000010021002100210021002100210020000
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001002100200000000000000000000
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000100200001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      ParentShowHint = False
      ShowHint = True
    end
    object btnSiguiente: TZetaSpeedButton
      Left = 55
      Top = 3
      Width = 25
      Height = 25
      Action = _EmpresaSiguiente
      Flat = True
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000100200001F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C000000000000000000001002100200001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C0000100210021002100210021002100200001F7C
        1F7C1F7C1F7C1F7C1F7C1F7C0000100210021002100210021002100210020000
        1F7C1F7C1F7C1F7C1F7C1F7C0000100210021002100210021002100200001F7C
        1F7C1F7C1F7C1F7C1F7C1F7C000000000000000000001002100200001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000100200001F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      ParentShowHint = False
      ShowHint = True
    end
    object btnUltima: TZetaSpeedButton
      Left = 80
      Top = 3
      Width = 25
      Height = 25
      Action = _EmpresaUltima
      Flat = True
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001F7C1F7C00001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000001F7C000000001F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000FF0300000000FF0300001F7C1F7C
        1F7C1F7C1F7C1F7C00000000000000000000FF03FF030000FF03FF0300001F7C
        1F7C1F7C1F7C1F7C0000FF03FF03FF03FF03FF03FF03FF030000FF03FF030000
        1F7C1F7C1F7C1F7C0000FF03FF03FF03FF03FF03FF03FF03FF030000FF03FF03
        00001F7C1F7C1F7C0000FF03FF03FF03FF03FF03FF03FF030000FF03FF030000
        1F7C1F7C1F7C1F7C00000000000000000000FF03FF030000FF03FF0300001F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000FF0300000000FF0300001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000001F7C000000001F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001F7C1F7C00001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      ParentShowHint = False
      ShowHint = True
    end
    object btnAgregar: TZetaSpeedButton
      Left = 112
      Top = 3
      Width = 25
      Height = 25
      Action = _EmpresaAgregar
      Flat = True
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7CE07F1F7C1F7C1F7C1F7C
        1F7C1F7CE07F1F7CE07FE07F1F7CEF3DEF3DEF3DE07FE07FEF3DEF3DEF3DEF3D
        E07FE07F1F7C1F7C1F7CE07F0000000000000000000000000000000000000000
        E07F1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000
        EF3D1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000
        EF3D1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000
        EF3D1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000
        EF3D1F7C1F7CE07FE07FE07F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000
        E07FE07F1F7C1F7CE07FE07F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000
        E07FE07FE07F1F7C1F7C1F7C0000FF7FFF7FFF7FFF7F00000000000000000000
        1F7C1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7F0000FF7FFF7F00001F7C
        1F7C1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7F0000FF7F0000E07F1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7F000000001F7CE07FE07F
        1F7C1F7C1F7C1F7C1F7CE07F0000000000000000000000001F7C1F7C1F7CE07F
        E07F1F7C1F7C1F7CE07FE07F1F7C1F7C1F7C1F7CE07FE07F1F7C1F7C1F7C1F7C
        E07FE07F1F7CE07F1F7C1F7C1F7C1F7C1F7C1F7CE07F1F7C1F7C1F7C1F7C1F7C
        1F7C1F7CE07F}
      ParentShowHint = False
      ShowHint = True
    end
    object btnBorrar: TZetaSpeedButton
      Left = 137
      Top = 3
      Width = 25
      Height = 25
      Action = _EmpresaBorrar
      Flat = True
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7CEF3DEF3DEF3DEF3DEF3DEF3DEF3D1F7CEF3D1F7C1F7C007C007C
        007C1F7CEF3D1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00000042
        00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000001F7C1F7CE07FE07F
        00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000FF7FE07F0000000000000000
        00001F7C1F7C1F7C1F7C1F7C1F7C1F7C0000FF7FE07F0000E07FFF7F0000FF7F
        00001F7C1F7C1F7C1F7C1F7C1F7C0000FF7FE07FFF7FE07FFF7F0000FF7FE07F
        00001F7C1F7C1F7C1F7C1F7C1F7C0000E07FFF7FE07FFF7F0000FF7FE07F0000
        00001F7C1F7C1F7C1F7C1F7C00000000FF7FE07FFF7FE07FFF7FE07F0000E07F
        00001F7C1F7C1F7C1F7C0000FF030000E07FFF7FE07FFF7FE07F00000000E07F
        00001F7C1F7C1F7C0000FF03FF03FF030000E07FFF7FE07F0000E07F0000E07F
        00001F7C1F7C000000000000FF03FF03FF030000E07FFF7FE07FFF7F0000E07F
        00001F7C1F7C0000000000000000FF03FF03FF0300000000000000000000E07F
        00001F7C1F7C00000000000000000000FF0300001F7C1F7C1F7C1F7C0000F75E
        00001F7C1F7C1F7C000000000000000000001F7C1F7C1F7C1F7C1F7C1F7C0000
        1F7C1F7C1F7C1F7C1F7C0000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C0000
        1F7C1F7C1F7C}
      ParentShowHint = False
      ShowHint = True
    end
    object btnModificar: TZetaSpeedButton
      Left = 162
      Top = 3
      Width = 25
      Height = 25
      Action = _EmpresaModificar
      Flat = True
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C0000000000000000000000000000
        0000000000001F7C1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7F0000000000001F7C00000000000000000000FF7FFF7F0000FF7F0000
        0000FF7F0000FF0300000000E07FFF7FE07FFF7FE07F0000FF7FFF7FFF7FFF7F
        FF7FFF7F0000FF030000E07FFF7FE07FFF7F000000000000FF7FFF7FFF7FFF7F
        0000FF7F0000FF030000FF7FE07FFF7FE07FFF7FE07FFF7F0000FF7F00000000
        FF7FFF7F0000FF030000E07FFF7FE07FFF7F00000000000000000000E07F0000
        FF7FFF7F0000FF030000FF7FE07FFF7FE07FFF7FE07FFF7FE07FFF7F0000FF7F
        FF7FFF7F0000FF030000E07FFF7F0000000000000000000000000000FF7FFF7F
        FF7FFF7F0000000000000000E07FFF7FE07F00000000E07F0000FF7FFF7F0000
        0000FF7F00001F7C1F7C1F7C0000000000000000E07F0000FF7FFF7FFF7FFF7F
        FF7FFF7F00001F7C1F7C1F7C1F7C1F7C0000E07F0000FF7FFF7FFF7FFF7F0000
        0000000000001F7C1F7C1F7C1F7C0000E07F0000FF7FFF7F00000000FF7F0000
        FF7FFF7F00001F7C1F7C1F7C0000E07F00000000FF7FFF7FFF7FFF7FFF7F0000
        FF7F00001F7C1F7C1F7C0000007C00001F7C0000FF7FFF7FFF7FFF7FFF7F0000
        00001F7C1F7C1F7C1F7C1F7C00001F7C1F7C0000000000000000000000000000
        1F7C1F7C1F7C}
      ParentShowHint = False
      ShowHint = True
    end
    object btnProbar: TZetaSpeedButton
      Left = 196
      Top = 3
      Width = 25
      Height = 25
      Action = _EmpresaProbar
      Flat = True
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FE003E003FF7FFF7FFF7FFF7F
        FF7FFF7FFF7F0000186318631863186318631863E003E003E003186318631863
        1042FF7FFF7F0000186310421042104218631863E003E003E003104210421863
        10420000FF7F000018631863186318631863E003E0031863E003E00318631863
        10420000FF7F000018631042104210421042E003E00310421042E003E0031042
        FF7F0000FF7FFF7F0000186318631863186318631863186318631863E003E003
        0000FF7FFF7FFF7F00000000E07FE07FE07FE07FFF7FFF7F007C0000E003E003
        FF7FFF7FFF7FFF7F00000000E07FE07FE07FFF7F10401040FF7F00001042E003
        E003FF7FFF7FFF7F00000000E07FE07FE07FFF7F10401040FF7F000010421042
        E003E003FF7FFF7F00000000FF7FFF7FFF7FFF7F104010401040000010421042
        FF7FE003E003FF7F00000000FF7F104010401040104010401040000010421042
        FF7FFF7FE003FF7F00000000FF7F104010401040104010401040000010421042
        FF7FFF7FFF7FFF7F00000000007CFF7F10401040104010401040000010421042
        FF7FFF7FFF7FFF7F000000000000000000000000000000000000000010421042
        FF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F1042
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7F}
      ParentShowHint = False
      ShowHint = True
    end
    object btnPoblar: TZetaSpeedButton
      Left = 240
      Top = 3
      Width = 25
      Height = 25
      Action = _ProcesosPoblar
      Flat = True
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C100010001F0010001F001F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C10001F001000100010001F001000100010001F7C
        1F7C1F7C1F7C1F7C1F7C10001F00100000420042100010001000100010001000
        1F7C1F7C1F7C1F7C10001F0010001F000042004210001F0010001F0010000042
        00421F7C1F7C1F7C10001F001F001F0000420042004210001000100010000042
        00421F7C1F7C1F001F001F001F001F000042004200421F001F001F0010001F00
        100010001F7C10001F001F00004200420042004200421F001F001F0010001000
        10001F001F7C1F0010000042004200420042004200421F0000421F0010001F00
        1F001F001F7C10001F001F00004200420042004200420042004200421F001000
        1F0010001F7C1F001F001F00004200421F001F0010000042004200421F001F00
        10001F001F7C1F7C1F001F001F001F001F000042004200420042004200421000
        1F001F7C1F7C1F7C10001F000042004200420042004200420042004200421F00
        1F001F7C1F7C1F7C1F7C00420042004200421F001F0000421F00004200421F00
        1F7C1F7C1F7C1F7C1F7C1F7C004200421F001F0010001F0010001F001F001F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F0010001F001F001F001F7C1F7C1F7C
        1F7C1F7C1F7C}
      ParentShowHint = False
      ShowHint = True
    end
    object btnActualizar: TZetaSpeedButton
      Left = 265
      Top = 3
      Width = 25
      Height = 25
      Action = _ProcesosActualizar
      Flat = True
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C0000000000001F7C1F7C1F7C
        0000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001F7C1F7C1F7C1F7C
        1F7C00001F7C1F7C1F7C000000001F7C00001F7C1F7C10420000000000000000
        000010421F7C1F7C00001F7C1F7C00001F7C1F7C1F7C1F7C000010421F7C1042
        00001F7C1F7C1F7C00001F7C1F7C00001F7C1F7C1F7C1F7C104200001F7C0000
        10421F7C1F7C1F7C1F7C0000000000001F7C1F7C1F7C1F7C1F7C000010420000
        1F7C1F7C1F7C1F7C00001F7C1F7C00001F7C1F7C1F7C1F7C1F7C104200001042
        1F7C1F7C1F7C1F7C1F7C000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C00001F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C17001700170017001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C17001F7C1F7C1F7C1F7C1F7C1700170017001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C17001F7C1F7C1F7C1F7C1F7C1700170017001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C17001F7C1F7C1F7C17001F7C1F7C17001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1700170017001F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      ParentShowHint = False
      ShowHint = True
    end
    object btnExportarReportes: TZetaSpeedButton
      Left = 304
      Top = 3
      Width = 25
      Height = 25
      Action = _ExportarReportes
      Flat = True
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C00001F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C0000
        1F7C1F7C1F7C000000000000000000000000007C007C007C007C007C007C007C
        00001F7C1F7C0000FF7FFF7FFF7FFF7F0000007C007C007C007C007C007C007C
        007C00001F7C0000FF7FFF7FFF7FFF7F0000007C007C007C007C007C007C007C
        007C007C00000000FF7F00000000FF7F0000007C007C007C007C007C007C007C
        007C00001F7C0000FF7FFF7FFF7FFF7F0000007C007C007C007C007C007C007C
        00001F7C1F7C0000FF7F00000000FF7FFF7FFF7FFF7FFF7F0000007C007C0000
        1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000007C00001F7C
        1F7C1F7C1F7C0000FF7F00000000FF7F0000000000000000000000001F7C1F7C
        1F7C1F7C1F7C0000FF7FFF7FFF7FFF7F0000FF7FFF7F00001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C0000FF7F0000F75EFF7F0000FF7F00001F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C0000FF7FFF7FFF7FFF7F000000001F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C0000000000000000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      ParentShowHint = False
      ShowHint = True
    end
    object btnImportarReportes: TZetaSpeedButton
      Left = 329
      Top = 3
      Width = 25
      Height = 25
      Action = _ImportarReportes
      Flat = True
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000000000000000000000000000
        0000000000001F7C1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7F00001F7C1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7F00001F7C1F7C1F7C1F7C1F7C1F7C0000FF7F00000000FF7F00000000
        0000FF7F00001F7C1F7C1F7C00001F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7F00001F7C1F7C1F7C007C00001F7C0000FF7F0000F75E00000000FF7F
        0000FF7F00001F7C1F7C1F7C007C007C00000000FF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7F00000000007C007C007C007C007C0000FF7F00000000FF7F00000000
        0000000000000000007C007C007C007C007C007C0000FF7FFF7FFF7F0000FF7F
        FF7F00001F7C0000007C007C007C007C007C007C007C0000F75EFF7F0000FF7F
        00001F7C1F7C0000007C007C007C007C007C007C0000FF7FFF7FFF7F00000000
        1F7C1F7C1F7C0000007C007C007C007C007C0000000000000000000000001F7C
        1F7C1F7C1F7C1F7C1F7C1F7C007C007C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C007C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      ParentShowHint = False
      ShowHint = True
    end
    object ZetaSpeedButton1: TZetaSpeedButton
      Left = 368
      Top = 3
      Width = 25
      Height = 25
      Action = _ExportarTablas
      Flat = True
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C00000000000000000000000000000000000000000000
        00001F7C1F7C1F7C0000E05EE05E00001F7C1F7C0000000000000000E05EE05E
        00001F7C1F7C1F7C0000E05EE05E00001F7C1F7C0000000000000000E05EE05E
        00001F7C1F7C1F7C0000E05EE05E00001F7C1F7C0000000000000000E05EE05E
        00001F7C1F7C1F7C0000E05EE05E0000000000000000000000000000E05EE05E
        00001F7C1F7C1F7C0000E05EE05EE05EE05EE05EE05EE05EE05EE05EE05EE05E
        00001F7C1F7C1F7C0000E05EE05E0000000000000000000000000000E05EE05E
        00001F7C1F7C1F7C0000E05E0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000E05E
        00001F7C1F7C1F7C0000E05E0000FF7FFF7FFF7F1F00FF7FFF7FFF7F0000E05E
        00001F7C1F7C1F7C0000E05E0000FF7FFF7F1F001F001F00FF7FFF7F0000E05E
        00001F7C1F7C1F7C0000E05E0000FF7FFF7FFF7F1F001F001F00FF7F1F7C1F7C
        1F7C1F7C1F7C1F7C0000E05E0000FF7FFF7FFF7FFF7F1F001F001F001F7C1F00
        1F7C1F7C1F7C1F7C0000000000000000000000001F7C1F7C1F001F001F001F00
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F001F001F00
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F001F001F001F00
        1F7C1F7C1F7C}
      ParentShowHint = False
      ShowHint = True
    end
    object ZetaSpeedButton2: TZetaSpeedButton
      Left = 393
      Top = 3
      Width = 25
      Height = 25
      Action = _ImportarTablas
      Flat = True
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C00000000000000000000000000000000000000000000
        00001F7C1F7C1F7C0000E05EE05E00001F7C1F7C0000000000000000E05EE05E
        00001F7C1F7C1F7C0000E05EE05E00001F7C1F7C0000000000000000E05EE05E
        00001F7C1F7C1F7C0000E05EE05E00001F7C1F7C0000000000000000E05EE05E
        00001F7C1F7C1F7C0000E05EE05E0000000000000000000000000000E05EE05E
        00001F7C1F7C1F7C0000E05EE05EE05EE05EE05EE05EE05EE05EE05EE05EE05E
        00001F7C1F7C1F7C0000E05EE05E0000000000000000000000000000E05EE05E
        00001F7C1F7C1F7C0000E05E0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000E05E
        00001F7C1F7C1F7C0000E05E0000FF7F1F001F001F001F00FF7FFF7F0000E05E
        00001F7C1F7C1F7C0000E05E0000FF7FFF7F1F001F001F00FF7FFF7F0000E05E
        00001F7C1F7C1F7C0000E05E1F7CFF7F1F001F001F001F00FF7FFF7F00000000
        00001F7C1F7C1F7C00001F7C1F7C1F001F001F00FF7F1F00FF7FFF7F00001042
        00001F7C1F7C1F7C1F7C1F7C1F001F001F001F7C1F7C1F7C1F7C000000000000
        00001F7C1F7C1F7C1F7C1F001F001F001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      ParentShowHint = False
      ShowHint = True
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 308
    Width = 673
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object dsCompany: TDataSource
    OnStateChange = dsCompanyStateChange
    OnDataChange = dsCompanyDataChange
    Left = 144
    Top = 82
  end
  object MainMenu: TMainMenu
    Images = ImageList
    Left = 175
    Top = 82
    object Archivo: TMenuItem
      Caption = '&Archivo'
      object ArchivoAbrir: TMenuItem
        Action = _ArchivoAbrir
      end
      object ArchivoCerrar: TMenuItem
        Action = _ArchivoCerrar
      end
      object N11: TMenuItem
        Caption = '-'
      end
      object ArchivoConfigurar: TMenuItem
        Action = _ArchivoConfigurar
      end
      object ArchivoEstructuraComparte: TMenuItem
        Action = _ArchivoEstructuraComparte
      end
      object ArchivoStoredProceduresAdicionales: TMenuItem
        Action = _ArchivoStoredProcedures
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object ArchivoInvocarBDE: TMenuItem
        Action = _ArchivoBDE
      end
      object ArchivoInvocarODBC: TMenuItem
        Action = _ArchivoODBC
      end
      object N5: TMenuItem
        Caption = '-'
      end
      object ArchivoSalir: TMenuItem
        Action = _ArchivoSalir
      end
    end
    object Empresas: TMenuItem
      Caption = '&Empresas'
      object EmpresaProbarAcceso: TMenuItem
        Action = _EmpresaProbar
      end
      object EmpresaConfigurarEstructura: TMenuItem
        Action = _EmpresaEstructura
      end
      object InicializarDatos1: TMenuItem
        Action = _EmpresaInit
      end
      object N13: TMenuItem
        Caption = '-'
      end
      object EmpresasAsignarLicenciasDeEmpleados: TMenuItem
        Action = _EmpresaAsignarLicencias
      end
      object N6: TMenuItem
        Caption = '-'
      end
      object EmpresaAgregar: TMenuItem
        Action = _EmpresaAgregar
      end
      object EmpresaBorrar: TMenuItem
        Action = _EmpresaBorrar
      end
      object EmpresaModificar: TMenuItem
        Action = _EmpresaModificar
      end
      object EmpresasN1: TMenuItem
        Caption = '-'
      end
      object EmpresaPrimera: TMenuItem
        Action = _EmpresaPrimera
      end
      object EmpresaAnterior: TMenuItem
        Action = _EmpresaAnterior
      end
      object EmpresaSiguiente: TMenuItem
        Action = _EmpresaSiguiente
      end
      object EmpresaUltima: TMenuItem
        Action = _EmpresaUltima
      end
    end
    object Sistema: TMenuItem
      Caption = 'S&istema'
      object DatosDelSistema1: TMenuItem
        Action = _SistemaDatos
      end
      object ServidordeProcesos1: TMenuItem
        Action = _SistemaProcessServer
      end
      object N12: TMenuItem
        Caption = '-'
      end
      object DesbloqueoAdministradores1: TMenuItem
        Action = _SistemaDesbloquearAdministradores
      end
    end
    object Procesos: TMenuItem
      Caption = '&Procesos'
      object ProcesosActualizarEmpresa: TMenuItem
        Action = _ProcesosActualizar
      end
      object ActualizarMltiplesEmpresas1: TMenuItem
        Action = _ProcesosActualizarMultiples
      end
      object ProcesosPoblar: TMenuItem
        Action = _ProcesosPoblar
      end
      object ProcesosActualizarComparte: TMenuItem
        Action = _ProcesosActualizarComparte
      end
      object ProcesosInicializaComparte: TMenuItem
        Action = _ProcesosComparteInit
      end
      object N3: TMenuItem
        Caption = '-'
      end
      object ProcesosConsolidarEmpresas: TMenuItem
        Action = _ProcesosConsolidar
      end
      object N2: TMenuItem
        Caption = '-'
        Enabled = False
        Visible = False
      end
      object ProcesosRevisar: TMenuItem
        Action = _ProcesosRevisar
      end
      object ProcesosMigrar: TMenuItem
        Action = _ProcesosMigrar
      end
      object ProcesosMigrarComparte: TMenuItem
        Action = _ProcesosMigrarComparte
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object ProcesosCompararCatlogos: TMenuItem
        Action = _ProcesosCompararCatalogos
      end
      object ProcesosTransferirEmpleados: TMenuItem
        Action = _ProcesosTransferir
      end
      object PrepararPresupuestos1: TMenuItem
        Action = _PrepararPresupuestos
      end
    end
    object ImportExport1: TMenuItem
      Caption = 'Importar/Exportar'
      object ProcesosExportaReportes: TMenuItem
        Action = _ExportarReportes
      end
      object ProcesosImportaReportes: TMenuItem
        Action = _ImportarReportes
      end
      object N8: TMenuItem
        Caption = '-'
      end
      object ProcesosImportaConceptos: TMenuItem
        Action = _ImportarConceptos
      end
      object N10: TMenuItem
        Caption = '-'
      end
      object ProcesosExportaTablas: TMenuItem
        Action = _ExportarTablas
      end
      object ProcesosImportaTablas: TMenuItem
        Action = _ImportarTablas
      end
      object ProcesosImportarImagenes: TMenuItem
        Action = _ImportarImagenes
      end
      object N9: TMenuItem
        Caption = '-'
        Enabled = False
        Visible = False
      end
      object Importar1: TMenuItem
        Action = _ImportarDatos
      end
      object ImportarComparte1: TMenuItem
        Action = _ImportarComparte
      end
      object N7: TMenuItem
        Caption = '-'
        Enabled = False
        Visible = False
      end
      object DatosdeSQLServeraIB1: TMenuItem
        Action = _ExportarDatos
        Enabled = False
      end
      object CompartedeSQLServeraIB1: TMenuItem
        Action = _ExportarComparte
        Enabled = False
      end
    end
    object Sentinel: TMenuItem
      Caption = '&Sentinel'
      object SentinelVer: TMenuItem
        Action = _SentinelVer
        Caption = '&Ver Datos del Sentinel'
      end
      object LicenciadeSentinelVirtual1: TMenuItem
        Action = _SentinelLicencia
      end
      object SentinelActualizar: TMenuItem
        Action = _SentinelActualizar
      end
      object SentinelRenovar: TMenuItem
        Action = _SentinelRenovar
      end
      object SentinelInstalacion: TMenuItem
        Action = _SentinelInstalacion
      end
    end
    object Ayuda: TMenuItem
      Caption = '&Ayuda'
      ShortCut = 8304
      object AyudaContenido: TMenuItem
        Action = _AyudaContenido
      end
      object NuevoenTress: TMenuItem
        Action = _AyudaNuevo
      end
      object AyudaUsandoAyuda: TMenuItem
        Action = _AyudaUsandoAyuda
      end
      object AyudaN1: TMenuItem
        Caption = '-'
      end
      object AyudaLicenciadeUso: TMenuItem
        Action = _AyudaLicencia
      end
      object AyudaAcercaDe: TMenuItem
        Caption = 'AcercaD&e...'
        ShortCut = 123
        OnClick = _AyudaAcercaDeExecute
      end
    end
  end
  object ActionList: TActionList
    Images = ImageList
    Left = 144
    Top = 112
    object _ArchivoAbrir: TAction
      Category = 'Archivo'
      Caption = '&Abrir'
      Hint = 'Abrir Cat'#225'logo de Empresas'
      OnExecute = _ArchivoAbrirExecute
    end
    object _ArchivoCerrar: TAction
      Category = 'Archivo'
      Caption = 'C&errar'
      Hint = 'Cerrar Cat'#225'logo de Empresas'
      OnExecute = _ArchivoCerrarExecute
    end
    object _ArchivoConfigurar: TAction
      Category = 'Archivo'
      Caption = '&Configurar Acceso'
      Hint = 'Configurar Acceso A Base De Datos Comparte'
      ImageIndex = 12
      OnExecute = _ArchivoConfigurarExecute
    end
    object _ArchivoEstructuraComparte: TAction
      Category = 'Archivo'
      Caption = 'E&structura De Comparte'
      Hint = 'Configurar Estructura De Base De Datos Comparte'
      ImageIndex = 26
      Visible = False
      OnExecute = _ArchivoEstructuraComparteExecute
    end
    object _ArchivoStoredProcedures: TAction
      Category = 'Archivo'
      Caption = 'Store&d Procedures Adicionales'
      Hint = 'Editar Stored Procedures Adicionales'
      OnExecute = _ArchivoStoredProceduresExecute
    end
    object _ArchivoBDE: TAction
      Category = 'Archivo'
      Caption = 'Invocar &BDE'
      Hint = 'Invocar Administrador del BDE'
      ImageIndex = 9
      OnExecute = _ArchivoBDEExecute
    end
    object _ArchivoODBC: TAction
      Category = 'Archivo'
      Caption = 'Invocar &ODBC'
      Hint = 'Invocar Administrador de ODBC'
      OnExecute = _ArchivoODBCExecute
    end
    object _ArchivoSalir: TAction
      Category = 'Archivo'
      Caption = '&Salir'
      Hint = 'Salir del Programa'
      ShortCut = 32883
      OnExecute = _ArchivoSalirExecute
    end
    object _EmpresaProbar: TAction
      Category = 'Empresa'
      Caption = 'Pro&bar Acceso'
      Hint = 'Probar Acceso A Empresas'
      ImageIndex = 13
      OnExecute = _EmpresaProbarExecute
    end
    object _EmpresaEstructura: TAction
      Category = 'Empresa'
      Caption = 'Con&figurar Estructura'
      Hint = 'Configurar Estructura De La Base De Datos De Esta Empresa'
      ImageIndex = 26
      Visible = False
      OnExecute = _EmpresaEstructuraExecute
    end
    object _EmpresaInit: TAction
      Category = 'Empresa'
      Caption = '&Inicializar Datos'
      Hint = 'Inicializar Datos De Esta Empresa'
      OnExecute = _EmpresaInitExecute
    end
    object _EmpresaAsignarLicencias: TAction
      Category = 'Empresa'
      Caption = 'Asignar Licencias De Empleados'
      Hint = 'Asignar Licencias De Empleados A Empresas'
      OnExecute = _EmpresaAsignarLicenciasExecute
    end
    object _EmpresaAgregar: TAction
      Category = 'Empresa'
      Caption = '&Agregar'
      Hint = 'Agregar Empresa Al Cat'#225'logo'
      ImageIndex = 0
      ShortCut = 8237
      OnExecute = _EmpresaAgregarExecute
    end
    object _EmpresaBorrar: TAction
      Category = 'Empresa'
      Caption = '&Borrar'
      Hint = 'Borrar una Empresa del Cat'#225'logo'
      ImageIndex = 1
      ShortCut = 8238
      OnExecute = _EmpresaBorrarExecute
    end
    object _EmpresaModificar: TAction
      Category = 'Empresa'
      Caption = '&Modificar'
      Hint = 'Modificar Datos De Esta Empresa'
      ImageIndex = 2
      ShortCut = 16429
      OnExecute = _EmpresaModificarExecute
    end
    object _EmpresaPrimera: TAction
      Category = 'Empresa'
      Caption = '&Primera'
      Hint = 'Seleccionar La Primera Empresa'
      ImageIndex = 3
      OnExecute = _EmpresaPrimeraExecute
    end
    object _EmpresaAnterior: TAction
      Category = 'Empresa'
      Caption = 'A&nterior'
      Hint = 'Seleccionar Empresa Anterior'
      ImageIndex = 5
      OnExecute = _EmpresaAnteriorExecute
    end
    object _EmpresaSiguiente: TAction
      Category = 'Empresa'
      Caption = '&Siguiente'
      Hint = 'Seleccionar Siguiente Empresa'
      ImageIndex = 6
      OnExecute = _EmpresaSiguienteExecute
    end
    object _EmpresaUltima: TAction
      Category = 'Empresa'
      Caption = '&Ultima'
      Hint = 'Seleccionar La Ultima Empresa'
      ImageIndex = 4
      OnExecute = _EmpresaUltimaExecute
    end
    object _AyudaContenido: TAction
      Category = 'Ayuda'
      Caption = '&Contenido'
      Hint = 'Invocar AyudaEn L'#237'nea'
      ImageIndex = 7
      ShortCut = 8304
      OnExecute = _AyudaContenidoExecute
    end
    object _AyudaUsandoAyuda: TAction
      Category = 'Ayuda'
      Caption = '&Usando Ayuda'
      Hint = 'Como Usar La Ayuda En L'#237'nea'
      ImageIndex = 10
      OnExecute = _AyudaUsandoAyudaExecute
    end
    object _SistemaDatos: TAction
      Category = 'Sistema'
      Caption = '&Datos Del Sistema'
      Hint = 'Ver Datos Del Sistema'
      ImageIndex = 11
      OnExecute = _SistemaDatosExecute
    end
    object _SentinelVer: TAction
      Category = 'Sentinel'
      Caption = '&Ver Sentinel'
      Hint = 'Ver Contenido Del Sentinel'
      ImageIndex = 8
      OnExecute = _SentinelVerExecute
    end
    object _SentinelActualizar: TAction
      Category = 'Sentinel'
      Caption = '&Actualizar Sentinel'
      Hint = 'Capturar Claves de Actualizaci'#243'n del Sentinel'
      OnExecute = _SentinelActualizarExecute
    end
    object _SentinelRenovar: TAction
      Category = 'Sentinel'
      Caption = '&Renovar Autorizaci'#243'n'
      Hint = 'Renovar Autorizaci'#243'n Temporal'
      OnExecute = _SentinelRenovarExecute
    end
    object _SistemaProcessServer: TAction
      Category = 'Sistema'
      Caption = '&Servidor de Procesos'
      Hint = 'Ver Versi'#243'n del Servidor de Procesos'
      OnExecute = _SistemaProcessServerExecute
    end
    object _ProcesosPoblar: TAction
      Category = 'Procesos'
      Caption = '&Poblar'
      Hint = 'Poblar Empresa Con Datos Default'
      ImageIndex = 14
      OnExecute = _ProcesosPoblarExecute
    end
    object _ProcesosRevisar: TAction
      Category = 'Procesos'
      Caption = '&Revisar'
      Enabled = False
      Hint = 'Revisar Base de Datos de Tress DOS'
      ImageIndex = 15
      Visible = False
      OnExecute = _ProcesosRevisarExecute
    end
    object _ProcesosMigrar: TAction
      Category = 'Procesos'
      Caption = '&Migrar'
      Enabled = False
      Hint = 'Poblar Empresa Con Datos De Tress DOS'
      ImageIndex = 16
      Visible = False
      OnExecute = _ProcesosMigrarExecute
    end
    object _ProcesosMigrarComparte: TAction
      Category = 'Procesos'
      Caption = 'Migrar &Comparte'
      Enabled = False
      Hint = 'Migrar Empresas y Usuarios De Tress DOS a Tress Windows'
      Visible = False
      OnExecute = _ProcesosMigrarComparteExecute
    end
    object _ProcesosActualizar: TAction
      Category = 'Procesos'
      Caption = '&Actualizar Empresa'
      Hint = 'Actualizar Base de Datos de una Empresa'
      ImageIndex = 23
      OnExecute = _ProcesosActualizarExecute
    end
    object _ProcesosActualizarComparte: TAction
      Category = 'Procesos'
      Caption = 'Actuali&zar Comparte'
      Hint = 'Actualizar Base de Datos Compartida'
      OnExecute = _ProcesosActualizarComparteExecute
    end
    object _ProcesosConsolidar: TAction
      Category = 'Procesos'
      Caption = 'C&onsolidar'
      Hint = 'Consolidar Varias Empresas En Una Sola'
      ImageIndex = 20
      OnExecute = _ProcesosConsolidarExecute
    end
    object _ExportarReportes: TAction
      Category = 'Import/Export'
      Caption = '&Exportar Reportes'
      Hint = 'Exportar Reportes A Archivo Externo'
      ImageIndex = 17
      OnExecute = _ExportarReportesExecute
    end
    object _ImportarReportes: TAction
      Category = 'Import/Export'
      Caption = '&Importar Reportes'
      Hint = 'Importar Reportes Desde Archivo Externo'
      ImageIndex = 18
      OnExecute = _ImportarReportesExecute
    end
    object _ImportarConceptos: TAction
      Category = 'Import/Export'
      Caption = 'Importar Co&nceptos'
      Hint = 'Importar Conceptos De N'#243'mina'
      OnExecute = _ImportarConceptosExecute
    end
    object _ExportarTablas: TAction
      Category = 'Import/Export'
      Caption = 'E&xportar Tablas'
      Hint = 'Exportar Tablas Hacia Archivos Paradox'
      ImageIndex = 21
      OnExecute = _ExportarTablasExecute
    end
    object _ImportarTablas: TAction
      Category = 'Import/Export'
      Caption = 'Importar &Tablas'
      Hint = 'Importar Cualquier Tabla'
      ImageIndex = 22
      OnExecute = _ImportarTablasExecute
    end
    object _ImportarDatos: TAction
      Category = 'Import/Export'
      Caption = 'Datos de IB a S&QL Server'
      Hint = 'Importar Base de Datos de Interbase a SQL Server'
      Visible = False
      OnExecute = _ImportarDatosExecute
    end
    object _ImportarComparte: TAction
      Category = 'Import/Export'
      Caption = 'Comparte de I&B a SQL Server'
      Hint = 'Importar Comparte de Interbase a SQL Server'
      Visible = False
      OnExecute = _ImportarComparteExecute
    end
    object _ExportarDatos: TAction
      Category = 'Import/Export'
      Caption = 'Datos de SQL Server a IB'
      Hint = 'Exportar Base de Datos de SQL Server a Interbase'
      Visible = False
    end
    object _ExportarComparte: TAction
      Category = 'Import/Export'
      Caption = 'Comparte de SQL Server a IB'
      Hint = 'Exportar Comparte de SQL Server a Interbase'
      Visible = False
    end
    object _ProcesosCompararCatalogos: TAction
      Category = 'Procesos'
      Caption = 'Comparar Cat'#225'lo&gos'
      Hint = 'Comparar Cat'#225'logos De Dos Empresas'
      ImageIndex = 24
      OnExecute = _ProcesosCompararCatalogosExecute
    end
    object _ProcesosTransferir: TAction
      Category = 'Procesos'
      Caption = '&Transferir Empleados'
      Hint = 'Transferencia Masiva De Empleados'
      ImageIndex = 25
      OnExecute = _ProcesosTransferirExecute
    end
    object _ImportarImagenes: TAction
      Category = 'Import/Export'
      Caption = 'Importar Im'#225'&genes'
      Hint = 'Importar Archivos de Im'#225'genes'
      ImageIndex = 27
      OnExecute = _ImportarImagenesExecute
    end
    object _AyudaNuevo: TAction
      Category = 'Ayuda'
      Caption = 'Ca&mbios en Tress '
      Hint = 'Cambios en Tress '
      ImageIndex = 28
      OnExecute = _AyudaNuevoExecute
    end
    object _SistemaDesbloquearAdministradores: TAction
      Category = 'Sistema'
      Caption = 'Desblo&quear Administradores'
      Hint = 'Desbloquear Todos los Usuarios Administradores del Sistema'
      OnExecute = _SistemaDesbloquearAdministradoresExecute
    end
    object _AyudaLicencia: TAction
      Category = 'Ayuda'
      Caption = '&Licencia de Usuario'
      Hint = 'Ver Licencia de Usuario'
      OnExecute = _AyudaLicenciaExecute
    end
    object _SentinelInstalacion: TAction
      Category = 'Sentinel'
      Caption = 'Clave de Instalaci'#243'n'
      Hint = 'Capturar Clave de Instalaci'#243'n'
      OnExecute = _SentinelInstalacionExecute
    end
    object _ProcesosComparteInit: TAction
      Category = 'Procesos'
      Caption = 'Inicializar datos de Comparte'
      OnExecute = _ProcesosComparteInitExecute
    end
    object _PrepararPresupuestos: TAction
      Category = 'Procesos'
      Caption = 'Preparar Presupuestos'
      OnExecute = _PrepararPresupuestosExecute
    end
    object _ProcesosActualizarMultiples: TAction
      Category = 'Procesos'
      Caption = 'Actualizar &M'#250'ltiples Empresas'
      Hint = 'Actualizar M'#250'ltiples Empresas'
      ImageIndex = 23
      OnExecute = _ProcesosActualizarMultiplesExecute
    end
    object _SentinelLicencia: TAction
      Category = 'Sentinel'
      Caption = '&Licencia de Sentinel Virtual'
      Hint = 'Licencia de Sentinel Virtual (Archivo .LIC)'
      OnExecute = _SentinelLicenciaExecute
    end
  end
  object ImageList: TImageList
    Left = 175
    Top = 112
    Bitmap = {
      494C01011D002200040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000009000000001002000000000000090
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF00FFFFFF00FFFFFF008400840084008400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF00FFFFFF00FFFFFF00840084008400840084008400FFFFFF00FFFF
      FF0084008400FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF00FFFFFF00FFFFFF0084008400840084008400840084008400FFFFFF008400
      84008400840084008400FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084008400840084008400840084008400FFFFFF008400
      8400840084008400840084008400FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF00FFFFFF00840084008400
      84008400840084008400FFFFFF00840084008400840084008400840084008400
      8400840084008400840084008400840084000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF00FFFFFF00840084008400
      8400840084008400840084008400840084008400840084008400840084008400
      8400840084008400840084008400840084000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF00FFFFFF00840084008400
      8400840084008400840084008400840084008400840084008400840084008400
      8400840084008400840084008400840084000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF008400
      8400840084008400840084008400840084008400840084008400840084008400
      8400840084008400840084008400840084000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00840084008400840084008400840084008400840084008400840084008400
      8400840084008400840084008400840084000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF008400
      8400840084008400840084008400840084008400840084008400840084008400
      8400840084008400840084008400840084000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00840084008400840084008400840084008400840084008400840084008400
      8400840084008400840084008400840084000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF008400840084008400840084008400840084008400840084008400
      8400840084008400840084008400840084000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084008400840084008400840084008400840084008400
      8400840084008400840084008400840084000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840084008400840084008400840084008400
      8400840084008400840084008400840084000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BD00000000000000BD00000000000000BD00000000000000BD00
      000000000000BD00000000000000000000000000000000000000000000000000
      0000000000000000000000000000848484008484840084848400848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BD00000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFF0000FF000000FF000000FF000000FF000000848484008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000848484008484840084848400848484008484840084848400848484008484
      8400848484000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BD000000BD000000BD00
      0000BD000000BD000000BD000000BD0000000000000000000000000000000000
      0000BD000000BD000000BD000000000000000000000000000000000000000000
      000000000000BD00000000000000000000000000000000000000000000000000
      000000000000FFFF0000BDBD0000FFFF0000FFFF0000FF000000848484008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008484840000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BD000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00BD0000000000000000000000000000000000
      000000000000BD00000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFF00000000000084848400FFFF0000FF000000848484008484
      8400000000000000000000000000000000000000000000000000000000000000
      000000FFFF0000BDBD00000000000000000000FFFF000000000000BDBD0000FF
      FF00000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000BD00BD000000FFFFFF008484
      84008484840084848400FFFFFF00BD0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BD00000000000000000000000000000000000000000000000000
      000000000000FFFF00000000000000000000BDBD0000FF000000848484008484
      8400000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000BDBD0000FFFF0000BDBD0000BDBD0000FFFF000000
      000000000000848484000000000000000000BD000000BD000000BD000000BD00
      0000BD000000BD000000BD000000000000000000BD00BD000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00BD0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFF0000FF000000FF000000FF000000FF000000848484008484
      8400848484000000000000000000000000000000000000000000000000000000
      0000FFFF00000000000000FFFF0000FFFF000000000000FFFF0000000000FFFF
      000000000000848484000000000000000000BD000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00BD0000000000BD0000000000BD000000BD000000BD00
      0000BD000000BD000000BD000000BD0000000000000000000000000000000000
      0000FFFFFF0000FFFF00FFFFFF0000FFFF000000000000000000000000000000
      0000FFFFFF0000FFFF00FFFFFF0000FFFF000000000000000000000000000000
      000000000000FFFF0000FF000000FF000000FF000000FF000000848484008484
      8400848484008484840000000000000000000000000000000000000000000000
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      000000000000848484000000000000000000BD000000FFFFFF00848484008484
      840084848400FFFFFF00BD0000000000BD0000000000BD00000000000000BD00
      000000000000BD00000000000000BD0000000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF000000FF000000FF000000FF000000FF000000FF000000000000008484
      8400848484008484840000000000000000000000000000000000000000000000
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      000000000000848484000000000000000000BD000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00BD0000000000000000000000BD000000BD000000BD00
      0000BD000000BD000000BD000000BD0000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000000000000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF0000000000
      0000848484008484840084848400000000000000000000000000000000000000
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000FFFF0000FFFF000000
      000000000000848484000000000000000000BD000000BD000000BD000000BD00
      0000BD000000BD000000BD000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF008484
      84008484840084848400FFFFFF00000000000000000000000000FFFFFF008484
      84008484840084848400FFFFFF00000000000000000000000000FF000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF00
      0000FF0000000000000084848400000000000000000000000000000000000000
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000FFFF0000FFFF000000
      000000000000848484000000000000000000BD00000000000000BD0000000000
      0000BD00000000000000BD000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FF000000FF00
      0000FF000000FF000000848484000000000000000000FFFF0000FF000000FF00
      0000FF0000000000000084848400000000000000000000000000000000000000
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000000000FFFF
      000000000000848484000000000000000000BD000000BD000000BD000000BD00
      0000BD000000BD000000BD000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF008484
      84008484840084848400FFFFFF00000000000000000000000000FFFFFF008484
      84008484840084848400FFFFFF00000000000000000000000000FF000000FF00
      0000FF0000000000000084848400000000000000000000000000FF000000FF00
      0000FF0000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FF000000FF00
      0000FF0000000000000084848400000000000000000000000000FF000000FF00
      0000FF0000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      0000FFFF0000BDBD000000000000000000000000000000000000FFFF0000BDBD
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000000000BDBD0000BD
      BD000000000000000000000000000000000000000000000000000000000000BD
      BD0000BDBD00000000000000000000000000000000000000000000BDBD0000BD
      BD000000000000000000000000000000000000000000000000000000000000BD
      BD0000BDBD000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840000000000000000000000
      0000000000000000000084848400000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000000000BDBD0000BD
      BD000000000000000000000000000000000000000000000000000000000000BD
      BD0000BDBD00000000000000000000000000000000000000000000BDBD0000BD
      BD000000000000000000000000000000000000000000000000000000000000BD
      BD0000BDBD000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848484000000
      0000848484000000000000000000000000008484840084848400848484008484
      8400848484008484840000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000BDBD0000BD
      BD000000000000000000000000000000000000000000000000000000000000BD
      BD0000BDBD00000000000000000000000000000000000000000000BDBD0000BD
      BD000000000000000000000000000000000000000000000000000000000000BD
      BD0000BDBD000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400000000000000
      00000000000084848400000000000000000084848400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FF000000FF000000FF000000FF00
      0000FF000000FF000000FF00000000000000000000000000000000BDBD0000BD
      BD000000000000000000000000000000000000000000000000000000000000BD
      BD0000BDBD00000000000000000000000000000000000000000000BDBD0000BD
      BD000000000000000000000000000000000000000000000000000000000000BD
      BD0000BDBD000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      84000000000000000000000000000000000084848400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000BDBD0000BD
      BD0000BDBD0000BDBD0000BDBD0000BDBD0000BDBD0000BDBD0000BDBD0000BD
      BD0000BDBD00000000000000000000000000000000000000000000BDBD0000BD
      BD0000BDBD0000BDBD0000BDBD0000BDBD0000BDBD0000BDBD0000BDBD0000BD
      BD0000BDBD000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848484000000
      00008484840000000000000000000000000084848400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400000000000000
      000000000000000000000000000000000000000000000000000000BDBD0000BD
      BD000000000000000000000000000000000000000000000000000000000000BD
      BD0000BDBD00000000000000000000000000000000000000000000BDBD0000BD
      BD000000000000000000000000000000000000000000000000000000000000BD
      BD0000BDBD000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840084848400848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      840084848400848484008484840000000000000000000000000000BDBD000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      000000BDBD00000000000000000000000000000000000000000000BDBD000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      000000BDBD000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000084848400FFFFFF00848484008484
      8400848484008484840084848400848484008484840084848400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF008484840000000000000000000000000000BDBD000000
      0000FFFFFF00FFFFFF00FFFFFF00FF000000FFFFFF00FFFFFF00FFFFFF000000
      000000BDBD00000000000000000000000000000000000000000000BDBD000000
      0000FFFFFF00FF000000FF000000FF000000FF000000FFFFFF00FFFFFF000000
      000000BDBD000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000BD000000BD000000BD000000BD0000000000
      0000000000000000000000000000000000008484840084848400848484008484
      8400848484008484840084848400848484008484840084848400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF008484840000000000000000000000000000BDBD000000
      0000FFFFFF00FFFFFF00FF000000FF000000FF000000FFFFFF00FFFFFF000000
      000000BDBD00000000000000000000000000000000000000000000BDBD000000
      0000FFFFFF00FFFFFF00FF000000FF000000FF000000FFFFFF00FFFFFF000000
      000000BDBD000000000000000000000000000000000000000000BD0000000000
      000000000000000000000000000000000000BD000000BD000000BD0000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF008484840000000000000000000000000000BDBD000000
      0000FFFFFF00FFFFFF00FFFFFF00FF000000FF000000FF000000FFFFFF000000
      000000000000000000000000000000000000000000000000000000BDBD000000
      0000FFFFFF00FF000000FF000000FF000000FF000000FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000BD0000000000
      000000000000000000000000000000000000BD000000BD000000BD0000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008484840084848400848484008484840084848400848484008484
      840084848400848484008484840000000000000000000000000000BDBD000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FF000000FF000000FF0000000000
      0000FF0000000000000000000000000000000000000000000000000000000000
      0000FF000000FF000000FF000000FFFFFF00FF000000FFFFFF00FFFFFF000000
      000084848400000000000000000000000000000000000000000000000000BD00
      0000000000000000000000000000BD0000000000000000000000BD0000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000084848400FFFFFF00848484008484840084848400848484008484
      8400848484008484840084848400000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF000000FF000000FF00
      0000FF000000000000000000000000000000000000000000000000000000FF00
      0000FF000000FF00000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000BD000000BD000000BD000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008484840084848400848484008484840084848400848484008484
      8400848484008484840084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF000000FF00
      0000FF0000000000000000000000000000000000000000000000FF000000FF00
      0000FF0000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF000000FF000000FF00
      0000FF000000000000000000000000000000000000000000000000000000FF00
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF00000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      000000FFFF0000FFFF0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF000000000000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      00000000000000FFFF0000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      FF000000FF00BDBDBD00FFFFFF00FFFFFF00FFFFFF00BDBDBD000000FF000000
      FF0000000000000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF00000000000000000000FFFF0000FFFF00000000000000000000FF
      FF0000FFFF0000FFFF00000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF000000000000000000FFFFFF000000
      00000000000000000000FFFFFF000000000000000000000000000000FF000000
      FF00FFFFFF00FFFFFF00FFFFFF00FF000000FFFFFF00FFFFFF00FFFFFF000000
      FF000000FF00000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF000000000000FFFF00000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF00000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000FF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FF000000FFFF
      FF000000FF00000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF00000000000000000000000000FFFFFF00000000000000
      0000FFFFFF00000000000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      FF00000000000000000000000000FFFFFF0000000000BDBDBD00000000000000
      0000FFFFFF0000000000FFFFFF0000000000000000000000FF00BDBDBD00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00BDBDBD000000FF0000000000000000000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000000000000000000000000000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      FF000000FF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000FF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000FF0000000000000000000000000000000000FFFF0000FFFF
      000000000000FFFF0000BDBDBD0000000000000000000000000000000000FFFF
      0000FFFF0000FFFF0000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000FF000000
      FF0000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF0000000000FFFFFF000000000000000000FFFFFF000000
      000000000000000000000000000000000000000000000000FF00FFFFFF00FF00
      0000FFFFFF00FFFFFF00FFFFFF000000FF000000000000000000000000000000
      0000FFFFFF000000FF0000000000000000000000000000000000FFFF0000FFFF
      0000FFFF000000000000BDBDBD000000000000000000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000FF000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF0000000000FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF000000000000000000000000000000FF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000FF0000000000000000000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000000000000000000000000000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF0000000000BDBDBD00FFFFFF000000
      0000FFFFFF00000000000000000000000000000000000000FF00BDBDBD00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00BDBDBD000000FF0000000000000000000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF00000000000000000000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF0000000000FFFFFF00FFFFFF00FFFFFF000000
      00000000000000000000000000000000000000000000000000000000FF00FFFF
      FF00FF000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FF000000FFFF
      FF000000FF000000000000000000000000000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF00000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000000000000000000000000000FFFFFF0000000000BDBD
      BD00FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      FF00FFFFFF00FFFFFF00FFFFFF00FF000000FFFFFF00FFFFFF00FFFFFF000000
      FF000000FF000000000000000000000000000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF00000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF000000FF00BDBDBD00FFFFFF00FF000000FFFFFF00BDBDBD000000FF000000
      FF00000000000000000000000000000000000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF0084848400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000FF000000FF0000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF000000FF000000FF000000
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C60000FF000000FF000000FF0000C6C6C600C6C6
      C600C6C6C60084848400FFFFFF00FFFFFF000000000000000000000000000000
      0000000000008400000084000000FF00000084000000FF000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000848484000000FF000000FF000000
      FF00FFFFFF008400840084008400FFFFFF0084008400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C6C6C600848484008484
      840084848400C6C6C600C6C6C60000FF000000FF000000FF0000848484008484
      8400C6C6C6008484840000000000FFFFFF000000000000000000000000008400
      0000FF000000840000008400000084000000FF00000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000848484000000FF00FFFFFF00FFFF
      FF00FFFFFF008400840084008400840084008400840084848400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C60000FF000000FF0000C6C6C60000FF000000FF0000C6C6
      C600C6C6C6008484840000000000FFFFFF00000000000000000084000000FF00
      0000840000000084840000848400840000008400000084000000840000008400
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00840084008400
      8400FFFFFF008400840084008400840084008400840084008400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C6C6C600848484008484
      8400848484008484840000FF000000FF0000848484008484840000FF000000FF
      000084848400FFFFFF0000000000FFFFFF000000000084000000FF0000008400
      0000FF000000008484000084840084000000FF00000084000000FF0000008400
      0000008484000084840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00840084008400
      8400840084008400840084008400840084008400840000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF0000000000C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C60000FF
      000000FF000000000000FFFFFF00FFFFFF000000000084000000FF000000FF00
      0000FF0000000084840000848400008484008400000084000000840000008400
      0000008484000084840000000000000000000000000000000000000000007B7B
      7B000000000000000000000000007B7B7B000000000000FFFF007B7B7B000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF008400
      840084008400840084000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000000000FFFFFF00FFFFFF00000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF00FFFFFF00FFFFFF000000FF000000000000FF
      000000FF0000FFFFFF00FFFFFF00FFFFFF00FF000000FF000000FF000000FF00
      0000FF000000008484000084840000848400FF000000FF000000FF0000008400
      0000FF0000008400000084000000000000000000000000000000000000007B7B
      7B007B7B7B007B7B7B007B7B7B007B7B7B00000000000000000000FFFF000000
      000000000000000000000000000000000000FFFFFF0084848400FFFFFF008400
      84000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      000000FFFF0000FFFF0000FFFF00FFFFFF00FFFFFF00000000000000000000FF
      FF0000FFFF0000FFFF00FFFFFF008400840084008400FFFFFF00000000008484
      840000FF000000FF0000FFFFFF00FFFFFF0084000000FF000000FF0000000084
      840000848400008484000084840000848400FF000000FF000000FF0000008400
      00008400000084000000FF0000000000000000000000000000007B7B7B00FFFF
      FF00BDBDBD00FFFFFF00BDBDBD00FFFFFF007B7B7B0000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00840084000000
      000000000000C6C6C600C6C6C60000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF000000000000FFFF0000000000FFFFFF00FFFFFF00000000000000000000FF
      FF0000FFFF0000FFFF00FFFFFF008400840084008400FFFFFF00000000008484
      84008484840000FF000000FF0000FFFFFF00FF00000084000000008484000084
      840000848400008484000084840000848400FF00000000848400FF0000008400
      0000FF000000FF000000FF000000000000007B7B7B007B7B7B00FFFFFF00BDBD
      BD00FFFFFF000000FF00FFFFFF00BDBDBD00FFFFFF007B7B7B007B7B7B000000
      000000000000000000000000000000000000FFFFFF000000000000FFFF0000FF
      FF0000FFFF00000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF0000000000FFFFFF00FFFFFF00FFFFFF000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840084008400840084008400000000008484
      840084848400FFFFFF0000FF000000FF000084000000FF000000FF0000000084
      840000848400008484000084840000848400008484000084840000848400FF00
      000084000000FF0000008400000000000000000000007B7B7B00BDBDBD00FFFF
      FF00BDBDBD000000FF00BDBDBD00FFFFFF00BDBDBD007B7B7B00000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF000000
      0000C6C6C60000FFFF0000FFFF0000FFFF0000FFFF008484840000FFFF000000
      000000FFFF0000000000FFFFFF00FFFFFF00FFFFFF000000000000000000FFFF
      FF00840084008400840084008400840084008400840084008400000000008484
      840084848400FFFFFF00FFFFFF0000FF0000FF000000FF000000FF0000000084
      840000848400FF000000FF00000084000000008484000084840000848400FF00
      0000FF00000084000000FF00000000000000000000007B7B7B00FFFFFF000000
      FF000000FF000000FF000000FF000000FF00FFFFFF007B7B7B00000000000000
      0000000000000000000000000000000000000000000000000000C6C6C60000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000000000FFFF0000FF
      FF000000000084848400FFFFFF00FFFFFF00FFFFFF000000000000000000FFFF
      FF00840084008400840084008400840084008400840084008400000000008484
      840084848400FFFFFF00FFFFFF00FFFFFF0000000000FF000000FF000000FF00
      0000FF000000FF00000000848400008484000084840000848400008484000084
      840084000000FF0000000000000000000000000000007B7B7B00BDBDBD00FFFF
      FF00BDBDBD000000FF00BDBDBD00FFFFFF00BDBDBD007B7B7B00000000000000
      000000000000000000000000000000000000FFFFFF000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF008484840000FFFF000000000000FF
      FF000000000084848400FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      FF00FFFFFF008400840084008400840084008400840084008400000000008484
      840084848400FFFFFF00FFFFFF00FFFFFF000000000084000000FF0000000084
      8400008484000084840000848400008484000084840000848400008484000084
      8400FF000000FF00000000000000000000007B7B7B007B7B7B00FFFFFF00BDBD
      BD00FFFFFF000000FF00FFFFFF00BDBDBD00FFFFFF007B7B7B007B7B7B000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00C6C6C60000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000FFFF0000FFFF000000
      00008400840084848400FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      840084848400FFFFFF00FFFFFF00FFFFFF000000000000000000008484000084
      84000084840000848400FF000000FF00000000848400FF000000008484000084
      8400FF00000000000000000000000000000000000000000000007B7B7B00FFFF
      FF00BDBDBD00FFFFFF00BDBDBD00FFFFFF007B7B7B0000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF000000
      0000C6C6C600C6C6C600000000000000000000FFFF0000FFFF00000000008400
      84008400840084008400FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0084848400FFFFFF00FFFFFF00FFFFFF000000000000000000000000000084
      840000848400FF000000FF00000084000000FF00000084000000FF000000FF00
      0000000000000000000000000000000000000000000000000000000000007B7B
      7B007B7B7B007B7B7B007B7B7B007B7B7B000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000FFFF0000FFFF00000000008400840084008400840084008400
      8400840084008400840084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      000000000000FF00000084000000FF000000FF000000FF000000000000000000
      0000000000000000000000000000000000000000000000000000000000007B7B
      7B000000000000000000000000007B7B7B000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00848484000000
      FF000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFFFF0000FFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00848484000000
      FF000000FF000000FF00FFFFFF00840084008400840084008400FFFFFF008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400C6C6C600FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFF000084840000840000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000
      FF000000FF000000FF00FFFFFF00840084008400840084008400FFFFFF008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      000000000000000000000000000000000000000000000000000084848400C6C6
      C600FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFF0000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008400840084008400840084008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00C6C6C60084848400848484008484
      840084848400FFFFFF00FFFFFF00C6C6C600C6C6C600FFFFFF00C6C6C600FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000084000000
      8400FFFFFF000000840000008400000084000000840000008400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF008484
      8400FFFFFF008400840084008400840084008400840084008400840084008400
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000848484008484
      8400FFFFFF00FFFFFF00FFFFFF00C6C6C600C6C6C600FFFFFF00C6C6C600FFFF
      FF00C6C6C6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008484000084000000840000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF008484
      8400FFFFFF008400840084008400840084008400840084008400840084008400
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00C6C6
      C600C6C6C600C6C6C600C6C6C600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFF000084840000840000000000000000000000FFFFFF00000084000000
      8400FFFFFF0000008400000084000000840000008400FFFFFF00FFFFFF000000
      840000008400FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084008400840084008400840084008400840084008400
      840084848400FFFFFF00FFFFFF00FFFFFF000000FF00C6C6C6000000000000FF
      FF0000FFFF0000FFFF0000FFFF00008484000084840000000000C6C6C600FFFF
      FF00FFFFFF00FFFFFF00000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFF000084840000848400008400000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00840084008400840084008400840084000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF000000FF000000000000FFFF000000
      0000FFFFFF0000000000008484000084840000848400FFFFFF00848484000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFF0000FFFF00008484000000000000FFFFFF00000084000000
      8400FFFFFF000000840000008400FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      840000008400FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000084008400000000008400840084848400C6C6C600848484008484
      8400C6C6C60084848400FFFFFF00FFFFFF00000000000000000000FFFF000000
      00008484840000000000000000000084840000FFFF0000FFFF00000000000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848400008400
      00008400000000000000FFFF00008484000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFFFF00FFFF
      FF000000000084008400000000008400840084848400C6C6C600848484008484
      8400C6C6C60084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF0000FFFF0000FF
      FF0000000000C6C6C6000000000000FFFF0000FFFF0000FFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FF000000FF000000FF00000000FFFF00FFFF00008484
      00008400000000000000FFFF00008484000000000000FFFFFF00000084000000
      8400FFFFFF000000840000008400000084000000840000008400FFFFFF000000
      84000000840000008400FFFFFF000000000000000000FFFFFF00000000000000
      00000000000000000000000000008400840084848400C6C6C600C6C6C600C6C6
      C600C6C6C60084848400FFFFFF00FFFFFF00008400000000000000FFFF0000FF
      FF0000FFFF00000000000000000000FFFF0000FFFF0000000000C6C6C600FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FF000000FF000000FF00000000FFFF00FFFF00008484
      00008484000084000000848400008484000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000008484840000000000FFFFFF00FFFF
      FF00FFFFFF008400840084008400840084000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FF000000FF000000FF00000000FFFF0000FFFF00FFFF
      00008484000084840000848400008484000000000000FFFFFF00000084000000
      8400FFFFFF0000008400000084000000840000008400FFFFFF00FFFFFF000000
      840000008400FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0084008400840084008400840084008400C6C6C600848484008484
      8400C6C6C60084848400FFFFFF00FFFFFF000000FF000000000084848400FFFF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000000000FFFFFF00FFFFFF000000
      00000000FF000000FF000000FF000000FF000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000840000008400000084
      0000FFFF0000FFFF0000FFFF00000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF008400840084008400840084008400840000000000C6C6C600C6C6
      C6000000000084848400FFFFFF00FFFFFF000000FF000000FF0000000000FFFF
      FF008484840000FFFF000000000000000000FFFFFF0000000000000000000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000840000008400000084
      00000000000000000000000000000000000000000000FFFFFF00840000008400
      0000FFFFFF008400000084000000840000008400000084000000FFFFFF008400
      00008400000084000000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF008484840084008400840084008400840084008400840084008400
      84008400840084848400FFFFFF00FFFFFF000000FF000000FF000000FF000000
      0000FFFFFF00FFFFFF00000000000000000000000000FFFFFF000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000840000008400000084
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF008484840084008400840084008400840084008400840084008400
      84008400840084008400FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084008400000000008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000084008400840084000000000000000000C6C6C600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000840084008400840084008400000000008484840000000000C6C6C6000000
      0000C6C6C6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      8400840084008400840084008400C6C6C600000000000000000084848400C6C6
      C60000000000C6C6C60000000000000000000000000000000000000000000000
      00000000000000000000FFFF00000000000000000000FFFF0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848400000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484000000000000000000000000
      0000000000000000000000000000000000000000000000000000840084008400
      84008400840084008400C6C6C600840084008400840084008400000000000000
      0000848484000000000084848400000000000000000000000000000000000000
      00000000000000000000FFFF0000FFFF000000000000FFFF0000FFFF00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084840000848400000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484000084840000000000000000
      0000000000000000000000000000000000000000000084008400840084008400
      840084008400C6C6C60084008400840084008400840000FFFF00840084008400
      8400000000000000000084848400000000000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FFFF0000FFFF
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008484000084840000848400008484000084840000848400008484
      0000000000000000000000000000000000000000000000000000000000000000
      0000848400008484000084840000848400008484000084840000848400000000
      0000000000000000000000000000000000000000000084008400840084008400
      8400C6C6C6008400840084008400840084008400840084008400840084008400
      8400840084008400840000000000000000000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FFFF
      0000FFFF00000000000000000000000000000000000000000000000000000000
      0000848400008484000084840000848400008484000084840000848400008484
      0000000000000000000000000000000000000000000000000000000000000000
      0000848400008484000084840000848400008484000084840000848400008484
      000000000000000000000000000000000000000000008400840084008400C6C6
      C6008400840084008400840084008400840000FFFF0084008400840084008400
      8400840084008400840000000000000000000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FFFF0000FFFF
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008484000084840000848400008484000084840000848400008484
      0000000000000000000000000000000000000000000000000000000000000000
      0000848400008484000084840000848400008484000084840000848400000000
      0000000000000000000000000000000000000000000084008400C6C6C6008400
      84008400840000FFFF00840084008400840000FFFF0084008400840084008400
      8400840084000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFF0000FFFF000000000000FFFF0000FFFF00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084840000848400000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484000084840000000000000000
      00000000000000000000000000000000000000000000C6C6C600840084008400
      84008400840000FFFF00840084008400840000FFFF0084008400840084008400
      8400000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFF00000000000000000000FFFF0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848400000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      8400840084008400840000FFFF0000FFFF008400840084008400840084000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008400840084008400840084008400840084008400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000840084008400840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000000000000000000000
      000000000000000000000000000000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF000000
      00007B7B7B007B7B7B007B7B7B0000FFFF0000FFFF007B7B7B007B7B7B007B7B
      7B007B7B7B0000FFFF0000FFFF00000000007B7B7B007B7B7B007B7B7B007B7B
      7B007B7B7B007B7B7B007B7B7B00000000007B7B7B0000000000000000000000
      FF000000FF000000FF00000000007B7B7B000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000008484000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF0000000000FFFF
      FF000000000000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000007B7B7B0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF00000000000000000000000000FFFF0000000000000000000000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000007B7B7B0000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF0000FFFF0000000000000000000000
      000000000000000000000000000000000000FFFF00000000000000FFFF00FFFF
      FF0000FFFF00FFFFFF00000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000007B7B7B0000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF0000FFFF000000000000FFFF00FFFFFF000000
      0000FFFFFF00000000000000000000000000FFFF000000000000FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000000000FFFFFF000000
      000000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000000000000000000FFFF00000000000000000000FFFF0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000007B7B7B0000000000000000000000000000000000000000000000
      000000000000FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000000000FFFF
      FF0000FFFF00000000000000000000000000FFFF00000000000000FFFF00FFFF
      FF0000FFFF00FFFFFF00000000000000000000000000000000000000000000FF
      FF0000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000FFFF0000FFFF000000000000FFFF0000FFFF0000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000FFFF0000FFFF00000000000000000000000000000000000000
      00000000000000FFFF00FFFFFF0000FFFF00FFFFFF0000000000FFFFFF0000FF
      FF0000000000000000000000000000000000FFFF000000000000FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000FFFF0000FFFF000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF000000000000000000000000000000FFFF0000FFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000FFFF0000FFFF0000FFFF000000000000000000000000000000
      000000000000FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF000000
      000000FFFF00000000000000000000000000FFFF00000000000000FFFF00FFFF
      FF0000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000000000000000FFFF
      0000FFFF000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      00000000000000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00000000000000
      000000FFFF0000000000000000000000000000000000000000000000000000FF
      FF00FFFFFF0000FFFF00000000000000000000FFFF0000000000FFFFFF00FFFF
      FF000000000000000000FFFFFF00000000000000000000000000000000000000
      0000FFFF0000FFFF000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000FFFF0000FFFF
      0000FFFF00000000000000FFFF00FFFFFF0000FFFF000000000000FFFF000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000FFFF0000FFFF000000000000FFFF0000FFFF0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF000000000000FF
      FF0000000000000000000000000000000000000000000000000000000000FFFF
      0000FFFF0000FFFF00000000000000FFFF00FFFFFF0000FFFF00FFFFFF000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFF00000000000000000000FFFF0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000000000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000FFFF0000FFFF0000FFFF0000000000000000000000000000000000000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000000000FFFFFF00FFFFFF000000000000000000FFFF
      FF0000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF0000000000000000000000000000000000000000000000
      000000000000FFFF000000000000000000000000000000000000000000000000
      0000BDBDBD000000000000000000000000000000000000000000000000000000
      000000FFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF000000
      000000000000000000000000000000FFFF0000FFFF0000000000000000000000
      00000000000000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF00000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000000000000000000000
      000000000000000000000000000000FFFF000000000000000000000000000000
      000000000000000000000000000000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000900000000100010000000000800400000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFAABFE1FBFFD
      FFFFFBFFF80FD003FF80F1FBF80FE003FF80FBFFF80FE003FF00FFFBF80FE003
      0100E060F807E00300808000F803E00300AA8000F003E00301808080E001E003
      01FF8080C001E00355FF8080C181E00301FF8080C1C3E007FFFF8080C1C3DFFB
      FFFF8080E3CFBFFDFFFFFFFFFFFFFFFFFC00FFFFFFFFFE38FC00C003C003FF7D
      FC0086038603CB01FC0086038603B793000086038603B793000080038003C7C7
      000080038003B7C7003F80038003CFEF000180038003FFFF000180038003FE1F
      000180038003DF1FF801801F9003DF1FF8018017B003EEDFF8018187E3C3F1FF
      F801FFC7C7FFFFFFFFFFFF87EFFFFFFFC003FFDFFFFFFFFFC003FFCFFC00F83F
      C003FFC7FC00E00FC0030003FC00C007C0030001FC008003C0030000EC008003
      C0030001E4000001C0030003E0000001C003000700000001C003000F00010001
      C003001F00030001C003007F00078003C00300FF000F8003C00301FFE3FFC007
      C00303FFE7FFE00FC003FFFFEFFFF83F00000000FFFFFFFD00000000F83FFFF8
      00000000E00FFFF100000000C007FFE3000000008003FFC7000000008003E08F
      000000000001C01F000000000001803F000000000001001F000000000001001F
      000000000001001F000000008003001F000000008003001F00000000C007803F
      00000000E00FC07F00000000F83FE0FF0000FFF3FFFBFFFF0000FFE1FFF18001
      00008000FFFB800100000003FFFF800100000003FFF1800100000000FFF18001
      00000000FFF0800100000000DFF0800100000000754480010000000FDC048001
      00000007740080010000800FDC008001000000007401800100000000DC078001
      00000000740780010000FCFFFD5FFFFFFFFFFFFFFFFFFC7FFFFFFFFFFFFFF81F
      FFFFFFFFFFFFF0A7FB7FFF7FFEFFE051F93FFE7FFE7FC008F81FFC7FFE3F8005
      800FF807E01F00018007F007E00F00008003E007E00700018007F007E00F0003
      800FF807E01F0007F81FFC7FFE3F800FF93FFE7FFE7FE01FFB7FFF7FFEFFF83F
      FFFFFFFFFFFFFE7FFFFFFFFFFFFFFFFFFF7EFFFFFC00FFFF90010162FC00FFFF
      C003FFE32000FFFFE003FE630000FEDFE003FC030000FC9FE003F8030000F81F
      E003F0030000F0030001F0030000E0038000E0030000C003E007C0030000E003
      E00F8003E000F003E00F0003F800F81FE0270003F000FC9FC07301E3E001FEDF
      9E7983F7C403FFFF7EFEC7F7EC07FFFF00000000000000000000000000000000
      000000000000}
  end
end
