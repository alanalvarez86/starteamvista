unit FReporteImport;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls,
     FMigrar,
     ZetaWizard, jpeg;

type
  TImportarReportes = class(TMigrar)
    DirectorioGB: TGroupBox;
    PathLBL: TLabel;
    PathSeek: TSpeedButton;
    Path: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ImportarReportes: TImportarReportes;

implementation

uses ZetaCommonTools,
     ZetaTressCFGTools,
     ZetaMigrar,
     ZetaDialogo,
     FHelpContext,
     DReportes;

{$R *.DFM}

procedure TImportarReportes.FormCreate(Sender: TObject);
begin
     FParadoxPath := Path;
     dmMigracion := TdmReportes.Create( Self );
     Path.Text := ZetaTressCFGTools.SetFileNameDefaultPath( '' );
     LogFile.Text := ZetaTressCFGTools.SetFileNameDefaultPath( 'ImportarReportes.log' );
     inherited;
     HelpContext := H00004_Importar_y_Exportar_reportes;
end;

procedure TImportarReportes.FormDestroy(Sender: TObject);
begin
     inherited;
     TdmReportes( dmMigracion ).Free;
end;

procedure TImportarReportes.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     if CanMove then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             if Wizard.Adelante then
             begin
                  if Wizard.EsPaginaActual( DirectorioDOS ) then
                  begin
                       with TdmReportes( dmMigracion ) do
                       begin
                            SourceSharedPath := TablasDefaultPath;
                            try
                               CanMove := ValidaDirectorioParadox and AbreArchivosReportes;
                            except
                                  on Error: Exception do
                                  begin
                                       CanMove := False;
                                       ShowError( '� Error En Directorio De Archivos Externos !', Error );
                                  end;
                            end;
                            if not CanMove then
                               ActiveControl := Path;
                       end;
                  end;
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TImportarReportes.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
begin
     StartLog;
     with TdmReportes( dmMigracion ) do
     begin
          InitCounter( GetImportSteps );
          StartProcess;
          lOk := ImportarReportes;
          EndProcess;
     end;
     EndLog;
     inherited;
end;

end.
