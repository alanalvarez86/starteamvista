inherited ProcessServer: TProcessServer
  Left = 404
  Top = 187
  Caption = 'Servidor de Procesos de Tress Corporativo'
  ClientHeight = 582
  ClientWidth = 414
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object BinariosLBL: TLabel [0]
    Left = 7
    Top = 10
    Width = 107
    Height = 13
    Caption = '&Directorio del Servidor:'
    FocusControl = Binarios
  end
  object BinariosSeek: TSpeedButton [1]
    Left = 386
    Top = 5
    Width = 23
    Height = 22
    Hint = 'Buscar Directorio de Dll'#39's y Exe'#39's del Servidor'
    Glyph.Data = {
      42010000424D4201000000000000760000002800000011000000110000000100
      040000000000CC00000000000000000000001000000010000000000000000000
      BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
      7777700000007770000777777777700000007770FB0774444444700000007770
      BF0777777777700000007770F007777777777000000077700077777777777000
      0000777777777777777770000000700700700700700070000000777777777777
      7777700000007070000777777770700000007070BF0770000070700000007770
      FB0777777777700000007070B007777777707000000070700077777777707000
      0000777777777777777770000000700700700700700070000000777777777777
      777770000000}
    ParentShowHint = False
    ShowHint = True
    OnClick = BinariosSeekClick
  end
  object VersionLBL: TLabel [2]
    Left = 120
    Top = 30
    Width = 45
    Height = 13
    Caption = '# Versi�n'
  end
  object Bevel: TBevel [3]
    Left = 118
    Top = 45
    Width = 265
    Height = 7
    Shape = bsTopLine
    Style = bsRaised
  end
  object AsistenciaLBL: TLabel [4]
    Left = 63
    Top = 52
    Width = 51
    Height = 13
    Alignment = taRightJustify
    Caption = 'Asistencia:'
    ParentShowHint = False
    ShowHint = True
  end
  object Asistencia: TZetaTextBox [5]
    Left = 117
    Top = 50
    Width = 265
    Height = 17
    AutoSize = False
    ParentShowHint = False
    ShowAccelChar = False
    ShowHint = True
    Brush.Color = clBtnFace
    Border = True
  end
  object Cafeteria: TZetaTextBox [6]
    Left = 117
    Top = 67
    Width = 265
    Height = 17
    AutoSize = False
    ParentShowHint = False
    ShowAccelChar = False
    ShowHint = True
    Brush.Color = clBtnFace
    Border = True
  end
  object CafeteriaLBL: TLabel [7]
    Left = 67
    Top = 69
    Width = 47
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cafeter�a:'
    ParentShowHint = False
    ShowHint = True
  end
  object CafeteraLBL: TLabel [8]
    Left = 9
    Top = 86
    Width = 104
    Height = 13
    Alignment = taRightJustify
    Caption = 'Servidor de Cafeter�a:'
    ParentShowHint = False
    ShowHint = True
  end
  object Cafetera: TZetaTextBox [9]
    Left = 117
    Top = 84
    Width = 265
    Height = 17
    AutoSize = False
    ParentShowHint = False
    ShowAccelChar = False
    ShowHint = True
    Brush.Color = clBtnFace
    Border = True
  end
  object Catalogos: TZetaTextBox [10]
    Left = 117
    Top = 118
    Width = 265
    Height = 17
    AutoSize = False
    ParentShowHint = False
    ShowAccelChar = False
    ShowHint = True
    Brush.Color = clBtnFace
    Border = True
  end
  object CatalogosLBL: TLabel [11]
    Left = 64
    Top = 120
    Width = 50
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cat�logos:'
    ParentShowHint = False
    ShowHint = True
  end
  object ConsultasLBL: TLabel [12]
    Left = 65
    Top = 137
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Consultas:'
    ParentShowHint = False
    ShowHint = True
  end
  object Consultas: TZetaTextBox [13]
    Left = 117
    Top = 135
    Width = 265
    Height = 17
    AutoSize = False
    ParentShowHint = False
    ShowAccelChar = False
    ShowHint = True
    Brush.Color = clBtnFace
    Border = True
  end
  object Diccionario: TZetaTextBox [14]
    Left = 117
    Top = 152
    Width = 265
    Height = 17
    AutoSize = False
    ParentShowHint = False
    ShowAccelChar = False
    ShowHint = True
    Brush.Color = clBtnFace
    Border = True
  end
  object DiccionarioLBL: TLabel [15]
    Left = 58
    Top = 154
    Width = 56
    Height = 13
    Alignment = taRightJustify
    Caption = 'Diccionario:'
    ParentShowHint = False
    ShowHint = True
  end
  object GlobalesLBL: TLabel [16]
    Left = 70
    Top = 171
    Width = 44
    Height = 13
    Alignment = taRightJustify
    Caption = 'Globales:'
    ParentShowHint = False
    ShowHint = True
  end
  object Globales: TZetaTextBox [17]
    Left = 117
    Top = 169
    Width = 265
    Height = 17
    AutoSize = False
    ParentShowHint = False
    ShowAccelChar = False
    ShowHint = True
    Brush.Color = clBtnFace
    Border = True
  end
  object IMSS: TZetaTextBox [18]
    Left = 117
    Top = 220
    Width = 265
    Height = 17
    AutoSize = False
    ParentShowHint = False
    ShowAccelChar = False
    ShowHint = True
    Brush.Color = clBtnFace
    Border = True
  end
  object IMSSlbl: TLabel [19]
    Left = 86
    Top = 222
    Width = 29
    Height = 13
    Alignment = taRightJustify
    Caption = 'IMSS:'
    ParentShowHint = False
    ShowHint = True
  end
  object LaborLBL: TLabel [20]
    Left = 85
    Top = 239
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Labor:'
    ParentShowHint = False
    ShowHint = True
  end
  object Labor: TZetaTextBox [21]
    Left = 117
    Top = 237
    Width = 265
    Height = 17
    AutoSize = False
    ParentShowHint = False
    ShowAccelChar = False
    ShowHint = True
    Brush.Color = clBtnFace
    Border = True
  end
  object Login: TZetaTextBox [22]
    Left = 117
    Top = 254
    Width = 265
    Height = 17
    AutoSize = False
    ParentShowHint = False
    ShowAccelChar = False
    ShowHint = True
    Brush.Color = clBtnFace
    Border = True
  end
  object LoginLBL: TLabel [23]
    Left = 86
    Top = 256
    Width = 29
    Height = 13
    Alignment = taRightJustify
    Caption = 'Login:'
    ParentShowHint = False
    ShowHint = True
  end
  object NominaLBL: TLabel [24]
    Left = 76
    Top = 273
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'N�mina:'
    ParentShowHint = False
    ShowHint = True
  end
  object Nomina: TZetaTextBox [25]
    Left = 117
    Top = 271
    Width = 265
    Height = 17
    AutoSize = False
    ParentShowHint = False
    ShowAccelChar = False
    ShowHint = True
    Brush.Color = clBtnFace
    Border = True
  end
  object NominaAnuales: TZetaTextBox [26]
    Left = 117
    Top = 288
    Width = 265
    Height = 17
    AutoSize = False
    ParentShowHint = False
    ShowAccelChar = False
    ShowHint = True
    Brush.Color = clBtnFace
    Border = True
  end
  object NominaAnualesLBL: TLabel [27]
    Left = 29
    Top = 290
    Width = 86
    Height = 13
    Alignment = taRightJustify
    Caption = 'N�mina - Anuales:'
    ParentShowHint = False
    ShowHint = True
  end
  object NominaCalculoLBL: TLabel [28]
    Left = 32
    Top = 307
    Width = 83
    Height = 13
    Alignment = taRightJustify
    Caption = 'N�mina - C�lculo:'
    ParentShowHint = False
    ShowHint = True
  end
  object NominaCalculo: TZetaTextBox [29]
    Left = 117
    Top = 305
    Width = 265
    Height = 17
    AutoSize = False
    ParentShowHint = False
    ShowAccelChar = False
    ShowHint = True
    Brush.Color = clBtnFace
    Border = True
  end
  object Recursos: TZetaTextBox [30]
    Left = 117
    Top = 356
    Width = 265
    Height = 17
    AutoSize = False
    ParentShowHint = False
    ShowAccelChar = False
    ShowHint = True
    Brush.Color = clBtnFace
    Border = True
  end
  object RecursosLBL: TLabel [31]
    Left = 19
    Top = 358
    Width = 96
    Height = 13
    Alignment = taRightJustify
    Caption = 'Recursos Humanos:'
    ParentShowHint = False
    ShowHint = True
  end
  object ReportesLBL: TLabel [32]
    Left = 69
    Top = 375
    Width = 46
    Height = 13
    Alignment = taRightJustify
    Caption = 'Reportes:'
    ParentShowHint = False
    ShowHint = True
  end
  object Reportes: TZetaTextBox [33]
    Left = 117
    Top = 373
    Width = 265
    Height = 17
    AutoSize = False
    ParentShowHint = False
    ShowAccelChar = False
    ShowHint = True
    Brush.Color = clBtnFace
    Border = True
  end
  object Sistema: TZetaTextBox [34]
    Left = 117
    Top = 458
    Width = 265
    Height = 17
    AutoSize = False
    ParentShowHint = False
    ShowAccelChar = False
    ShowHint = True
    Brush.Color = clBtnFace
    Border = True
  end
  object SistemaLBL: TLabel [35]
    Left = 75
    Top = 460
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Sistema:'
    ParentShowHint = False
    ShowHint = True
  end
  object SupervisoresLBL: TLabel [36]
    Left = 51
    Top = 477
    Width = 64
    Height = 13
    Alignment = taRightJustify
    Caption = 'Supervisores:'
    ParentShowHint = False
    ShowHint = True
  end
  object Supervisores: TZetaTextBox [37]
    Left = 117
    Top = 475
    Width = 265
    Height = 17
    AutoSize = False
    ParentShowHint = False
    ShowAccelChar = False
    ShowHint = True
    Brush.Color = clBtnFace
    Border = True
  end
  object Tablas: TZetaTextBox [38]
    Left = 117
    Top = 492
    Width = 265
    Height = 17
    AutoSize = False
    ParentShowHint = False
    ShowAccelChar = False
    ShowHint = True
    Brush.Color = clBtnFace
    Border = True
  end
  object TablasLBL: TLabel [39]
    Left = 80
    Top = 494
    Width = 35
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tablas:'
    ParentShowHint = False
    ShowHint = True
  end
  object PortalTress: TZetaTextBox [40]
    Left = 117
    Top = 322
    Width = 265
    Height = 17
    AutoSize = False
    ParentShowHint = False
    ShowAccelChar = False
    ShowHint = True
    Brush.Color = clBtnFace
    Border = True
  end
  object PortalTressLBL: TLabel [41]
    Left = 56
    Top = 324
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Portal Tress:'
    ParentShowHint = False
    ShowHint = True
  end
  object GlobalSelLBL: TLabel [42]
    Left = 16
    Top = 188
    Width = 98
    Height = 13
    Alignment = taRightJustify
    Caption = 'Global de Selecci�n:'
    ParentShowHint = False
    ShowHint = True
  end
  object GlobalSeleccion: TZetaTextBox [43]
    Left = 117
    Top = 186
    Width = 265
    Height = 17
    AutoSize = False
    ParentShowHint = False
    ShowAccelChar = False
    ShowHint = True
    Brush.Color = clBtnFace
    Border = True
  end
  object Seleccion: TZetaTextBox [44]
    Left = 117
    Top = 407
    Width = 265
    Height = 17
    AutoSize = False
    ParentShowHint = False
    ShowAccelChar = False
    ShowHint = True
    Brush.Color = clBtnFace
    Border = True
  end
  object SeleccionLBL: TLabel [45]
    Left = 6
    Top = 409
    Width = 109
    Height = 13
    Alignment = taRightJustify
    Caption = 'Selecci�n de Personal:'
    ParentShowHint = False
    ShowHint = True
  end
  object SelReportesLBL: TLabel [46]
    Left = 13
    Top = 426
    Width = 102
    Height = 13
    Alignment = taRightJustify
    Caption = 'Selecci�n - Reportes:'
    ParentShowHint = False
    ShowHint = True
  end
  object SelReportes: TZetaTextBox [47]
    Left = 117
    Top = 424
    Width = 265
    Height = 17
    AutoSize = False
    ParentShowHint = False
    ShowAccelChar = False
    ShowHint = True
    Brush.Color = clBtnFace
    Border = True
  end
  object ServicioMedico: TZetaTextBox [48]
    Left = 117
    Top = 441
    Width = 265
    Height = 17
    AutoSize = False
    ParentShowHint = False
    ShowAccelChar = False
    ShowHint = True
    Brush.Color = clBtnFace
    Border = True
  end
  object ServicioMedicoLBL: TLabel [49]
    Left = 36
    Top = 443
    Width = 79
    Height = 13
    Alignment = taRightJustify
    Caption = 'Servicio M�dico:'
    ParentShowHint = False
    ShowHint = True
  end
  object CajaAhorroLBL: TLabel [50]
    Left = 41
    Top = 103
    Width = 73
    Height = 13
    Alignment = taRightJustify
    Caption = 'Caja de Ahorro:'
    ParentShowHint = False
    ShowHint = True
  end
  object CajaAhorro: TZetaTextBox [51]
    Left = 117
    Top = 101
    Width = 265
    Height = 17
    AutoSize = False
    ParentShowHint = False
    ShowAccelChar = False
    ShowHint = True
    Brush.Color = clBtnFace
    Border = True
  end
  object GlobalVisLBL: TLabel [52]
    Left = 18
    Top = 205
    Width = 96
    Height = 13
    Alignment = taRightJustify
    Caption = 'Global de Visitantes:'
    ParentShowHint = False
    ShowHint = True
  end
  object GlobalVisitantes: TZetaTextBox [53]
    Left = 117
    Top = 203
    Width = 265
    Height = 17
    AutoSize = False
    ParentShowHint = False
    ShowAccelChar = False
    ShowHint = True
    Brush.Color = clBtnFace
    Border = True
  end
  object PresupuestosLBL: TLabel [54]
    Left = 3
    Top = 341
    Width = 112
    Height = 13
    Alignment = taRightJustify
    Caption = 'Presupuestos - N�mina:'
    ParentShowHint = False
    ShowHint = True
  end
  object Presupuestos: TZetaTextBox [55]
    Left = 117
    Top = 339
    Width = 265
    Height = 17
    AutoSize = False
    ParentShowHint = False
    ShowAccelChar = False
    ShowHint = True
    Brush.Color = clBtnFace
    Border = True
  end
  object ReporteadorLBL: TLabel [56]
    Left = 10
    Top = 392
    Width = 105
    Height = 13
    Alignment = taRightJustify
    Caption = 'Reportes - Generador:'
    ParentShowHint = False
    ShowHint = True
  end
  object Reporteador: TZetaTextBox [57]
    Left = 117
    Top = 390
    Width = 265
    Height = 17
    AutoSize = False
    ParentShowHint = False
    ShowAccelChar = False
    ShowHint = True
    Brush.Color = clBtnFace
    Border = True
  end
  object VisitantesLBL: TLabel [58]
    Left = 67
    Top = 511
    Width = 48
    Height = 13
    Alignment = taRightJustify
    Caption = 'Visitantes:'
    ParentShowHint = False
    ShowHint = True
  end
  object Visitantes: TZetaTextBox [59]
    Left = 117
    Top = 509
    Width = 265
    Height = 17
    AutoSize = False
    ParentShowHint = False
    ShowAccelChar = False
    ShowHint = True
    Brush.Color = clBtnFace
    Border = True
  end
  object VisReportesLBL: TLabel [60]
    Left = 15
    Top = 528
    Width = 100
    Height = 13
    Alignment = taRightJustify
    Caption = 'Visitantes - Reportes:'
    ParentShowHint = False
    ShowHint = True
  end
  object VisReportes: TZetaTextBox [61]
    Left = 117
    Top = 526
    Width = 265
    Height = 17
    AutoSize = False
    ParentShowHint = False
    ShowAccelChar = False
    ShowHint = True
    Brush.Color = clBtnFace
    Border = True
  end
  inherited PanelBotones: TPanel
    Top = 545
    Width = 414
    Height = 37
    BevelOuter = bvNone
    inherited OK: TBitBtn
      Left = 245
      Top = 7
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 326
      Top = 7
    end
  end
  object Binarios: TEdit
    Left = 117
    Top = 6
    Width = 265
    Height = 21
    Color = clBtnFace
    ReadOnly = True
    TabOrder = 1
    OnChange = BinariosChange
    OnExit = BinariosExit
  end
end
