unit FImportarImagenes;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, FileCtrl,
     Forms, Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls, DbTables,
     FMigrar,
     ZetaWizard, jpeg;

type
  TImportarImagenes = class(TMigrar)
    DirectorioGB: TGroupBox;
    DirectorioLBL: TLabel;
    ImportFileSeek: TSpeedButton;
    Directorio: TEdit;
    OpenDialog: TOpenDialog;
    TipoImagenLBL: TLabel;
    TipoImagen: TEdit;
    Filtro: TFilterComboBox;
    Label1: TLabel;
    cbConservarImagen: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ImportFileSeekClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
  private
    { Private declarations }
    FCtr: Integer;
    function Init: Boolean;
    function GetImportDirectory: String;
    function GetImageFilter: String;
    function GetImageType: String;
  public
    { Public declarations }
  end;

var
  ImportarImagenes: TImportarImagenes;

implementation

uses ZetaMigrar,
     ZetaDialogo,
     ZetaTressCFGTools,
     ZetaCommonTools,
     FHelpContext,
     DReportes;

{$R *.DFM}

procedure TImportarImagenes.FormCreate(Sender: TObject);
begin
     LogFile.Text := ZetaTressCFGTools.SetFileNameDefaultPath( 'ImportarImagenes.log' );
     dmMigracion := TdmReportes.Create( Self );
     FCtr := 0;
     inherited;
     HelpContext := H00022_Importando_tablas;
end;

procedure TImportarImagenes.FormShow(Sender: TObject);
begin
     inherited;
     Init;
end;

procedure TImportarImagenes.FormDestroy(Sender: TObject);
begin
     TdmReportes( dmMigracion ).Free;
     inherited;
end;

function TImportarImagenes.Init: Boolean;
var
   oCursor: TCursor;
begin
     cbConservarImagen.Checked := False;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        StartProcess;
        with TdmReportes( dmMigracion ) do
        begin
             AbreBDReceptora;
             cbConservarImagen.Checked := GetGlobalConservarImagen;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                ShowError( '� Error Al Abrir Base De Datos Destino !', Error );
                Result := False;
           end;
     end;
     Screen.Cursor := oCursor;
end;

function TImportarImagenes.GetImportDirectory: String;
begin
     Result := Directorio.Text;
end;

function TImportarImagenes.GetImageType: String;
begin
     Result := TipoImagen.Text;
end;

function TImportarImagenes.GetImageFilter: String;
begin
     Result := ZetaTressCFGTools.SetFileNamePath( GetImportDirectory, Filtro.Mask );
end;

procedure TImportarImagenes.ImportFileSeekClick(Sender: TObject);
var
   sDirectory: String;
begin
     inherited;
     with Directorio do
     begin
          sDirectory := Text;
          if SelectDirectory( sDirectory, [], 0 ) then
             Text := sDirectory;
     end;
end;

procedure TImportarImagenes.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   oCursor: TCursor;

function HayArchivos( const sFileSpec: String ): Boolean;
var
   Datos: TSearchRec;
begin
     FCtr := 0;
     if ( FindFirst( sFileSpec, faAnyFile, Datos ) = 0 ) then
     begin
          Inc( FCtr );
          while ( FindNext( Datos ) = 0 ) do
          begin
               Inc( FCtr );
          end;
          FindClose( Datos );
     end;
     Result := ( FCtr > 0 );
end;

begin
     inherited;
     if CanMove then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             if Wizard.Adelante then
             begin
                  if Wizard.EsPaginaActual( DirectorioDOS ) then
                  begin
                       if not DirectoryExists( GetImportDirectory ) then
                       begin
                            ZetaDialogo.zWarning( Caption, 'Directorio de Im�genes No Existe', 0, mbOK );
                            ActiveControl := Directorio;
                            CanMove := False;
                       end
                       else
                       begin
                            if not HayArchivos( GetImageFilter ) then
                            begin
                                 ZetaDialogo.zWarning( Caption, 'El Directorio de Im�genes No Contiene Archivos Del Tipo Especificado', 0, mbOK );
                                 ActiveControl := Directorio;
                                 CanMove := False;
                            end;
                       end;
                  end;
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TImportarImagenes.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
begin
     StartLog;
     with TdmReportes( dmMigracion ) do
     begin
          InitCounter( FCtr );
          StartProcess;
          EnableStopOnError;
          lOk := ImportarImagenes( GetImageType, GetImageFilter, Self.cbConservarImagen.Checked );
          EndProcess;
     end;
     EndLog;
     inherited;
end;

end.
