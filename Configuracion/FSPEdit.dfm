inherited SPEdit: TSPEdit
  Left = 286
  Top = 131
  BorderIcons = [biSystemMenu, biMinimize, biMaximize]
  Caption = 'Editar Stored Procedure Adicional'
  ClientHeight = 323
  ClientWidth = 532
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 287
    Width = 532
    inherited OK: TBitBtn
      Left = 364
      ModalResult = 0
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 449
      OnClick = CancelarClick
    end
  end
  object PanelControles: TPanel
    Left = 0
    Top = 0
    Width = 532
    Height = 117
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object SE_NOMBRElbl: TLabel
      Left = 25
      Top = 9
      Width = 40
      Height = 13
      Alignment = taRightJustify
      BiDiMode = bdLeftToRight
      Caption = '&Nombre:'
      FocusControl = SE_NOMBRE
      ParentBiDiMode = False
    end
    object SE_TIPOlbl: TLabel
      Left = 41
      Top = 51
      Width = 24
      Height = 13
      Alignment = taRightJustify
      Caption = '&Tipo:'
    end
    object SE_DESCRIPlbl: TLabel
      Left = 6
      Top = 30
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = '&Descripci'#243'n:'
      FocusControl = SE_DESCRIP
    end
    object SE_SEQ_NUMlbl: TLabel
      Left = 11
      Top = 72
      Width = 54
      Height = 13
      Alignment = taRightJustify
      Caption = '&Secuencia:'
      FocusControl = SE_SEQ_NUM
    end
    object lblAplicarEn: TLabel
      Left = 15
      Top = 93
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = 'Aplicar en:'
    end
    object SE_NOMBRE: TDBEdit
      Left = 66
      Top = 5
      Width = 148
      Height = 21
      DataField = 'SE_NOMBRE'
      DataSource = DataSource
      TabOrder = 0
    end
    object SE_TIPO: TZetaDBKeyCombo
      Left = 66
      Top = 47
      Width = 148
      Height = 21
      AutoComplete = False
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 2
      OnChange = SE_TIPOChange
      ListaFija = lfNinguna
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
      DataField = 'SE_TIPO'
      DataSource = DataSource
      LlaveNumerica = True
    end
    object SE_DESCRIP: TDBEdit
      Left = 66
      Top = 26
      Width = 303
      Height = 21
      DataField = 'SE_DESCRIP'
      DataSource = DataSource
      TabOrder = 1
    end
    object SE_SEQ_NUM: TZetaDBNumero
      Left = 66
      Top = 68
      Width = 43
      Height = 21
      Mascara = mnDias
      TabOrder = 3
      Text = '0'
      DataField = 'SE_SEQ_NUM'
      DataSource = DataSource
    end
  end
  object SE_APLICA: TZetaDBKeyCombo
    Left = 66
    Top = 89
    Width = 303
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
    ListaFija = lfNinguna
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'SE_APLICA'
    DataSource = DataSource
    LlaveNumerica = False
  end
  object PageControlScripts: TPageControl
    Left = 0
    Top = 117
    Width = 532
    Height = 170
    ActivePage = tabProcedure
    Align = alClient
    TabOrder = 3
    object tabProcedure: TTabSheet
      Caption = 'C'#243'digo de Stored Procedure'
      object PanelIB: TPanel
        Left = 488
        Top = 0
        Width = 36
        Height = 142
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object IBLoadFromFile: TSpeedButton
          Left = 6
          Top = 8
          Width = 26
          Height = 27
          Hint = 'Leer Archivo'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            33333333333FFFFFFFFF333333000000000033333377777777773333330FFFFF
            FFF03333337F333333373333330FFFFFFFF03333337F3FF3FFF73333330F00F0
            00F03333F37F773777373330330FFFFFFFF03337FF7F3F3FF3F73339030F0800
            F0F033377F7F737737373339900FFFFFFFF03FF7777F3FF3FFF70999990F00F0
            00007777777F7737777709999990FFF0FF0377777777FF37F3730999999908F0
            F033777777777337F73309999990FFF0033377777777FFF77333099999000000
            3333777777777777333333399033333333333337773333333333333903333333
            3333333773333333333333303333333333333337333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = IBLoadFromFileClick
        end
      end
      object SE_DATA: TDBMemo
        Left = 0
        Top = 0
        Width = 488
        Height = 142
        Align = alClient
        DataField = 'SE_DATA'
        DataSource = DataSource
        ScrollBars = ssBoth
        TabOrder = 1
      end
    end
    object tabEspecial: TTabSheet
      Caption = 'Ruta de Script Especial'
      ImageIndex = 1
      object Label1: TLabel
        Left = 6
        Top = 30
        Width = 71
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ruta de Script:'
        FocusControl = SE_ARCHIVO
      end
      object SQLSelectFile: TSpeedButton
        Left = 492
        Top = 22
        Width = 26
        Height = 27
        Hint = 'Seleccionar Archivo'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          33333333333FFFFFFFFF333333000000000033333377777777773333330FFFFF
          FFF03333337F333333373333330FFFFFFFF03333337F3FF3FFF73333330F00F0
          00F03333F37F773777373330330FFFFFFFF03337FF7F3F3FF3F73339030F0800
          F0F033377F7F737737373339900FFFFFFFF03FF7777F3FF3FFF70999990F00F0
          00007777777F7737777709999990FFF0FF0377777777FF37F3730999999908F0
          F033777777777337F73309999990FFF0033377777777FFF77333099999000000
          3333777777777777333333399033333333333337773333333333333903333333
          3333333773333333333333303333333333333337333333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = SQLSelectFileClick
      end
      object SE_ARCHIVO: TDBEdit
        Left = 80
        Top = 26
        Width = 409
        Height = 21
        DataField = 'SE_ARCHIVO'
        DataSource = DataSource
        TabOrder = 0
      end
    end
  end
  object DataSource: TDataSource
    OnDataChange = DataSourceDataChange
    Left = 240
    Top = 216
  end
  object OpenDialog: TOpenDialog
    DefaultExt = 'sql'
    Filter = 
      'Archivos Texto ( *.txt )|*.txt|Scripts de SQL ( *.sql )|*.sql|To' +
      'dos los Archivos |*.*'
    FilterIndex = 2
    Title = 'Escoja El Archivo Deseado'
    Left = 392
    Top = 16
  end
  object OpenDialogScriptPath: TOpenDialog
    DefaultExt = 'sql'
    Filter = 
      'Archivos Texto ( *.txt )|*.txt|Scripts de SQL ( *.sql )|*.sql|To' +
      'dos los Archivos |*.*'
    FilterIndex = 2
    Title = 'Escoja El Archivo Deseado'
    Left = 400
    Top = 72
  end
end
