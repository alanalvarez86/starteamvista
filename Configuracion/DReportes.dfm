inherited dmReportes: TdmReportes
  Left = 283
  Top = 245
  Height = 479
  Width = 741
  inherited dbSourceShared: TDatabase
    Left = 29
    Top = 9
  end
  inherited dbSourceDatos: TDatabase
    Left = 110
  end
  inherited dbTarget: TDatabase
    Left = 185
    Top = 9
  end
  inherited tqSource: TQuery
    Left = 29
    Top = 57
  end
  inherited tqTarget: TQuery
    UniDirectional = False
    Left = 110
    Top = 57
  end
  inherited tqDelete: TQuery
    Left = 264
    Top = 58
  end
  inherited ttTarget: TTable
    Left = 184
    Top = 56
  end
  inherited BatchMove: TBatchMove
    Left = 184
    Top = 104
  end
  inherited ttSource: TTable
    Left = 263
  end
  object tqRead: TQuery
    Left = 114
    Top = 105
  end
  object dbFuente: TDatabase
    DatabaseName = 'dbFuente'
    LoginPrompt = False
    Params.Strings = (
      'USER NAME=SYSDBA'
      'PASSWORD=masterkey')
    SessionName = 'Default'
    Left = 265
    Top = 105
  end
  object tqReporte: TQuery
    Left = 34
    Top = 209
  end
  object tqCamposRep: TQuery
    Left = 34
    Top = 273
  end
end
