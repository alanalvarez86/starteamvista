unit DCompara;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, DBTables, Db, ODSI, OCL, OCI,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaASCIIFile,
     DMigrar;

{$define QUINCENALES}
{.$undefine QUINCENALES}

type
  TShowProgress = function( const sMensaje: String ): Boolean of object;
  TAvisaProgreso = function( const sMensaje: String; const Concepto, Empleado: Integer ): Boolean of object;
  TTransferLog = class( TASCIILog )
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Start( const sFileName: String );
    procedure HandleException( const sMensaje: String; Error: Exception );
  end;
  TdmCompara = class;
  {$ifdef INTERBASE}
  TMyQuery = TQuery;
  TMyStatement = TQuery;
  TMyDatabase = TDatabase;
  {$endif}
  {$ifdef MSSQL}
  TMyQuery = TOEQuery;
  TMyStatement = TOEQuery;
  TMyDatabase = THdbc;
  {$endif}
  TTablaData = class(TObject)
  private
    { Private declarations }
    FCampos: TStrings;
    FClase: Integer;
    FComparables: TStrings;
    FDescripcion: String;
    FLlaves: TStrings;
    FDifferences: TStrings;
    FDelta: TStrings;
    FNombre: String;
    FOwner: TList;
    FTransfer: Boolean;
    FLog: TTransferLog;
    FQFindTable: TMyQuery;
    FQFindField: TMyQuery;
    FQInsert: TMyStatement;
    FQRead: TMyQuery;
    FQSeek: TMyQuery;
    FDBBDE: TDatabase;
    FDBMaster: TMyDatabase;
    FDBSource: TMyDatabase;
    FdmCompara: TdmCompara;
    FIgnorar: String;
    {$ifdef MSSQL}
    FDbSourceName: String;
    {$endif}
    FUsaDBSYS : Boolean;
    function BuildPrimaryKeyFilter: String;
    function CompareFieldValues: Boolean;
    function CreateQuery( DB: TMyDatabase; const sScript: String ): TMyQuery;
    function GetFieldDescription( const sFieldName: String ): String;
    function GetNamesList( Lista: TStrings ): String;
    function GetNombreReal: String;

    procedure Connect(DataModule: TdmCompara);

    procedure GetFieldsDescriptions( Lista: TStrings );
    procedure LoadPrimaryKey( Source: TDataSet; Target: TMyQuery );
    procedure HandleException( const sMensaje: String; Error: Exception );

  protected
    { Protected declarations }
    property Ignorar: String read FIgnorar write FIgnorar;
    function CreateQueryMaster( const sScript: String ): TMyQuery;
    {$ifdef MSSQL}
    function CreateStatement( DB: TMyDatabase; const sScript: String ): TMyStatement;
    function CreateStatementMaster( const sScript: String ): TMyStatement;
    {$endif}
    function CreateQuerySource( const sScript: String ): TMyQuery;
    function GetPrimaryKeyValues( Source: TDataSet ): String; dynamic;
    function GetQueryInsert: String; dynamic;
    function GetQueryRead: String; dynamic;
    function GetQueryTransfer( const iOldEmp, iNewEmp: TNumEmp ): String; dynamic;
    procedure BuildFieldList;
    procedure BuildPrimaryKeyList; dynamic;
    procedure InsertEmployee( const iNewEmp: TNumEmp );
    procedure NotifyDelta;
    procedure PrepareQuery( Query: TMyQuery; const sScript: String );
    procedure PrepareQueryInsert;
    procedure PrepareQueryRead( const sScript: String );
    procedure TransferFieldValues; dynamic;
    procedure FieldsToParams;dynamic;
    procedure PrepareQuerySeek;virtual;
    function RowExists: Boolean;virtual;
  public
    { Public declarations }
    constructor Create( Owner: TList; const sNombre: String );
    destructor Destroy; override;
    property Campos: TStrings read FCampos;
    property CamposComparables: TStrings read FComparables;
    property Clase: Integer read FClase;
    property DBBDE: TDatabase read FDBBDE write FDBBDE;
    property DBMaster: TMyDatabase read FDBMaster write FDBMaster;
    property DBSource: TMyDatabase read FDBSource write FDBSource;
    property Descripcion: String read FDescripcion;
    property Llaves: TStrings read FLlaves;
    property Log: TTransferLog read FLog write FLog;
    property Nombre: String read FNombre;
    property NombreReal: String read GetNombreReal;
    property Transfer: Boolean read FTransfer write FTransfer;
    property QueryTable: TMyQuery read FQFindTable write FQFindTable;
    property QueryField: TMyQuery read FQFindField write FQFindField;
    property QueryRead: TMyQuery read FQRead;
    property UsaDBSYS: Boolean read FUsaDBSYS write FUsaDBSYS;
    function Comparar: Boolean; dynamic;
    procedure GetTableInfo;
    procedure BuildCamposComparables( const sComparables: String );
    procedure TransferPrepare; dynamic;
    procedure TransferEmployee( const iOldEmp, iNewEmp: TNumEmp ); dynamic;
  end;
  TNomParam = class( TTablaData )
  private
    { Private declarations }
    FQFolio: TMyQuery;
  protected
    { Protected declarations }
    procedure TransferFieldValues; override;
  public
    { Public declarations }
    function Comparar: Boolean; override;
    procedure BuildPrimaryKeyList; override;
  end;
  TGlobal = class( TTablaData )
  private
    { Private declarations }
  protected
    { Protected declarations }
    function GetPrimaryKeyValues( Source: TDataSet ): String; override;
  public
    { Public declarations }
  end;
  TCatalogo = class( TTablaData )
  private
    { Private declarations }
    FKardexKeyField: String;
    FFiltro: String;
  protected
    { Protected declarations }
    function GetQueryRead: String; override;
    function GetPrimaryKeyValues( Source: TDataSet ): String; override;
  public
    { Public declarations }
    property KardexKeyField: String read FKardexKeyField write FKardexKeyField;
    property Filtro: String read FFiltro write FFiltro;
  end;



  TColabora = class( TTablaData )
  private
    { Private declarations }
    FNivel0: String;
    FFiltro: String;
    function CambiaNivel0( const sScript: String ): String;
  protected
    { Protected declarations }
    function GetQueryTransfer( const iOldEmp, iNewEmp: TNumEmp ): String; override;
  public
    { Public declarations }
    property Nivel0: String read FNivel0 write FNivel0;
    property Filtro: String read FFiltro write FFiltro;
    procedure TransferPrepare; override;
    {$ifdef INTERBASE}
    procedure TransferEmployee( const iOldEmp, iNewEmp: TNumEmp ); override;
    {$endif}
  end;
  TKardex = class( TTablaData )
  private
    { Private declarations }
    //FFiltro: String;
  protected
    { Protected declarations }
  public
    { Public declarations }
  end;
  TdmCompara = class(TdmMigrar)
    tqCuenta: TQuery;
    HdbcSource: THdbc;
    HdbcTarget: THdbc;
    qryTable: TOEQuery;
    qryField: TOEQuery;
    qryCuenta: TOEQuery;
    qrySource: TOEQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FEmpresaActiva: Integer;
    FEmpresaFuente: Integer;
    FEmpresas: TCompanyList;
    FTablas: TList;
    FNivel0: TStrings;
    FColabora: TColabora;
 //   FAcumula: TCatalogo;
    {$ifdef MSSQL}
    FDbSourceName: String;
    {$endif}
    function GetTablas(Index: Integer): TTablaData;
    function GetTablasCount: Integer;
    function GetEmpresaActiva: TCompanyData;
    function GetEmpresaFuente: TCompanyData;
    function GetEmpresasCount: Integer;
    function GetEmpresas(Index: Integer): TCompanyData;
    procedure InitTableList(const lUsaDBSYS: Boolean = FALSE ); //CV: El parametro lUsaDBSYS indica si se utiliza SYSCOLUMNS Y SYSOBJECTS PARA determinar los campos de una tabla
    procedure ClearTablas;
    {$ifdef MSSQL}
    procedure ConectarODBC(Hdbc: THdbc; const sAlias, sUser, sPassword: String);
    procedure DesconectarODBC(Hdbc: THdbc);
    procedure HdbcBeforeConnect(Sender: TObject);
    procedure SetODBCQuery( oHdbc: THdbc; Query: TMyQuery; const sSQL: String);
    {$endif}
    procedure InitMovimientos( const lNominas, lIMSS: Boolean; const sFiltro, sNivel0: String );
    // (JB) Se agregan los parametros.
    {$ifdef MSSQL}
    procedure InitMovimientosPresupuestos( const sFiltro: String; iAnoPresupuesto : Integer; dFechaAlta, dFechaBaja : TDateTime );
    procedure InitTablasPresupuestos( const lConservarConfigNomina: Boolean );
    {$endif}
    procedure SetTargetQuery( Query: TMyQuery; const sSQL: String );
    procedure TxCommit;
    procedure TxRollback;
    procedure TxStart;
  public
    { Public declarations }
    {$ifdef MSSQL}
    property DBSourceName: String read FDBSourceName;
    {$endif}
    property Empresas[ Index: Integer ]: TCompanyData read GetEmpresas;
    property EmpresaActiva: TCompanyData read GetEmpresaActiva;
    property EmpresaFuente: TCompanyData read GetEmpresaFuente;
    property EmpresasCount: Integer read GetEmpresasCount;
    property Nivel0: TStrings read FNivel0;
    property Tablas[ Index: Integer ]: TTablaData read GetTablas;
    property TablasCount: Integer read GetTablasCount;
    function CompararTablas( CallBack: TShowProgress; Bitacora: TTransferLog ): Boolean;
    function ConectarDestino: Boolean;
    function ConectarFuente(const iEmpresa: Integer): Boolean;
    function GetEmpleadoCount(const sFiltro: String): Integer;
    function GetEmpleadoMaximo: Integer;
    function InitEmpresas( Lista: TStrings; lPresupuesto : Boolean = False ): Boolean;
    function TransferirEmpleados( CallBack: TShowProgress; Bitacora: TTransferLog; const iEmpBase: TNumEmp; const lNominas, lIMSS: Boolean; const sFiltro, sNivel0: String ): Boolean;
{$ifdef MSSQL}
    function PreparacionPresupuestos(CallBack: TShowProgress; Bitacora: TTransferLog; iYear : Integer; dFechaEmpAlta : TDateTime ; dFechaEmpBaja : TDateTime; lConservarConfNomina,lNominaMensual: Boolean ): Boolean;
{$endif}
    procedure InitTablas;
    procedure DesconectarFuente;
  end;

  TTablaPresup =class(TCatalogo)
  private

  protected
   FConservar: Boolean;
   FCamposNulos: TStrings;
   FQReadDestino: TMyQuery;
   FQReadNulls: TMyQuery;
   function GetQueryTransfer( const iOldEmp, iNewEmp: TNumEmp ): String; override;
   function GetQueryInsertSinNulls: String;
   procedure FieldsToParamsNulls;
   procedure FieldsToParams;override;
   procedure TransferFieldValues; override;
   procedure TransferFieldValuesNulls;
   procedure PrepareQuerySeek;override;
   function RowExists:Boolean;override;
  public
   property Conservar: Boolean read FConservar write FConservar;
   procedure TransferPrepare; override;
  end;

  TTablaPresupCat =class(TTablaPresup)
  private
   //FCamposNulos: TStrings;
   //FQReadDestino: TMyQuery;
   //FQReadNulls: TMyQuery;
  protected
//   function GetQueryInsertSinNulls: String;
//   procedure FieldsToParamsNulls;
//   procedure FieldsToParams;override;
   procedure TransferFieldValues; override;
//   procedure TransferFieldValuesNulls;
  public
   procedure TransferPrepare; override;
  end;

  TTablaPresupColabora =class(TColabora)
  private
   FCamposNulos: TStrings;
   FQReadDestino: TMyQuery;
   FQReadNulls: TMyQuery;
  protected
     function GetQueryInsertSinNulls: String;
     procedure FieldsToParams;override;
     function GetQueryTransfer( const iOldEmp, iNewEmp: TNumEmp ): String; override;
     function GetQueryTransferNull( const iOldEmp, iNewEmp: TNumEmp ): String;


  public
     procedure TransferPrepare; override;
     procedure TransferEmployeeNull( const iOldEmp, iNewEmp: TNumEmp );

     property QReadNulls: TMyQuery read FQReadNulls;
  end;
var
    dmCompara: TdmCompara;

implementation

uses BDEFunctions,
     DDBConfig,
     {$ifdef USE_ODBC_DATASOURCE}
     {$else}
     ZetaODBCTools,
     ZetaServerTools,
     {$endif}
     ZetaMigrar,
     ZetaTressCFGTools,
     ZetaCommonTools, Math;

const
     K_ANCHO_LINEA = 79;
     K_CONDICION = '@WHERE';
     K_TODOS = 'TODOS';
     K_DELTA_VALUES = '{ %s } <-> { %s }';
     K_ANCHO_REFERENCIA = 8;
     K_VALOR_NULO = 'NULLNULL';
     K_CONCEPTOS_PK = 'CO_NUMERO;CB_CODIGO;MO_REFEREN';
     K_EMPLEADOS_PK = 'CB_CODIGO';

{$R *.DFM}

function GetQueryFiltro( const sQuery, sFiltro: String ): String;
begin
     if ZetaCommonTools.StrLleno( sFiltro ) then
        Result := ZetaCommonTools.StrTransAll( sQuery, K_CONDICION, Format( 'where ( %s )', [ sFiltro ] ) )
     else
         Result := ZetaCommonTools.StrTransAll( sQuery, K_CONDICION, VACIO );
end;

{ ******** TTransferLog ********* }

procedure TTransferLog.Start( const sFileName: String );
begin
     if FileExists( sFileName ) then
     begin
          SysUtils.DeleteFile( sFileName );
     end;
     Init( sFileName );
end;

procedure TTransferLog.HandleException( const sMensaje: String; Error: Exception );
begin
     WriteTexto( sMensaje );
     WriteTexto( Error.ClassName + ' = ' + Error.Message );
end;

{ ******** TTablaData ********* }

constructor TTablaData.Create( Owner: TList; const sNombre: String );
begin
     FOwner := Owner;
     FNombre := sNombre;
     FDescripcion := sNombre;
     FTransfer := False;
     FClase := -1;
     FCampos := TStringList.Create;
     FLlaves := TStringList.Create;
     FDifferences := TStringList.Create;
     FDelta := TStringList.Create;
     FComparables := TStringList.Create;
     FIgnorar := VACIO;
     FOwner.Add( Self );
end;

destructor TTablaData.Destroy;
begin
     FComparables.Free;
     FDelta.Free;
     FDifferences.Free;
     FLlaves.Free;
     FCampos.Free;
     FOwner.Remove( Self );
     FQInsert.Free;
     FQRead.Free;
     FQSeek.Free;
     inherited Destroy;
end;

function TTablaData.GetNombreReal: String;
begin
     {$ifdef INTERBASE}
     Result := Nombre;
     {$endif}
     {$ifdef MSSQL}
     Result := 'DBO.' + Nombre;
     {$endif}
end;

procedure TTablaData.HandleException( const sMensaje: String; Error: Exception );
begin
     Log.HandleException( Format( '%s ( %s ): %s', [ Descripcion, Nombre, sMensaje ] ), Error );
end;

procedure TTablaData.Connect( DataModule: TdmCompara );
begin
     FdmCompara := Datamodule;
     Self.DBBDE := DataModule.dbTarget;
     {$ifdef INTERBASE}
     Self.QueryTable := DataModule.tqTarget;
     Self.QueryField := DataModule.tqDelete;
     Self.DBMaster := DataModule.dbTarget;
     Self.DBSource := DataModule.dbSourceDatos;
     {$endif}
     {$ifdef MSSQL}
     Self.QueryTable := DataModule.qryTable;
     Self.QueryField := DataModule.qryField;
     Self.DBMaster := DataModule.HdbcTarget;
     Self.DBSource := DataModule.HdbcSource;
     FDBSourceName := Datamodule.DBSourceName;
     {$endif}
end;

procedure TTablaData.PrepareQuery( Query: TMyQuery; const sScript: String );
begin
     with Query do
     begin
          Active := False;
          with SQL do
          begin
               Clear;
               Text := sScript;
          end;
          Prepare;
     end;
end;

{$ifdef MSSQL}
function TTablaData.CreateStatement( DB: TMyDatabase; const sScript: String ): TMyStatement;
begin
     Result := CreateQuery( DB, sScript );
end;

function TTablaData.CreateStatementMaster( const sScript: String ): TMyStatement;
begin
     Result := CreateStatement( DBMaster, sScript );
end;
{$endif}

function TTablaData.CreateQuery( DB: TMyDatabase; const sScript: String ): TMyQuery;
begin
     Result := TMyQuery.Create( nil );
     with Result do
     begin
          {$ifdef INTERBASE}
          Unidirectional := True;
          RequestLive := False;
          DatabaseName := DB.DatabaseName;
          {$endif}
          {$ifdef MSSQL}
          ParamBindMode := pbByName;
          Unidirectional := False;
          Hdbc := DB;
          {$endif}
     end;
     PrepareQuery( Result, sScript );
end;

function TTablaData.CreateQueryMaster( const sScript: String ): TMyQuery;
begin
     Result := CreateQuery( DBMaster, sScript );
end;

function TTablaData.CreateQuerySource( const sScript: String ): TMyQuery;
begin
     Result := CreateQuery( DBSource, sScript );
end;

procedure TTablaData.GetTableInfo;
begin
     if not UsaDBSys then
     begin
          with FQFindTable do
          begin
               Active := False;
               ParamByName( 'Tabla' ).AsString := Nombre;
               Active := True;
               if not IsEmpty then
               begin
                    FDescripcion := FieldByName( 'DI_TITULO' ).AsString;
                    FClase := FieldByName( 'DI_CALC' ).AsInteger;
               end;
               Active := False;
          end;
     end;
     BuildFieldList;
     BuildPrimaryKeyList;
end;

function TTablaData.GetFieldDescription( const sFieldName: String ): String;
begin
     with FQFindField do
     begin
          Active := False;
          ParamByName( 'Campo' ).AsString := sFieldName;
          ParamByName( 'Clase' ).AsInteger := Clase;
          Active := True;
          if IsEmpty then
             Result := sFieldName
          else
              Result := FieldByName( 'DI_TITULO' ).AsString;
          Active := False;
     end;
end;

procedure TTablaData.GetFieldsDescriptions( Lista: TStrings );
var
   i: Integer;
   sFieldName: String;
begin
     with Lista do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               sFieldName := Strings[ i ];
               Strings[ i ] := Format( '%s=%s', [ sFieldName, GetFieldDescription( sFieldName ) ] );
          end;
     end;
end;

procedure TTablaData.BuildFieldList;
begin
     BDEFunctions.BDEOpenFieldList( DBBDE.Handle, NombreReal, Campos );
end;

procedure TTablaData.BuildPrimaryKeyList;
begin
     BDEFunctions.BDEBuildPrimaryKeyFieldList( DBBDE.Handle, NombreReal, Campos, Llaves );
     If not UsaDBSYS then
        GetFieldsDescriptions( Llaves );
end;

function TTablaData.BuildPrimaryKeyFilter: String;
var
   i: Integer;
   sCampo: String;
begin
     Result := VACIO;
     with FLlaves do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               sCampo := Names[ i ];
               Result := ZetaCommonTools.ConcatFiltros( Result, Format( '( %s= :%s )', [ sCampo, sCampo ] ) );
          end;
     end;
end;

procedure TTablaData.BuildCamposComparables( const sComparables: String );
begin
     if ( sComparables <> VACIO ) then
     begin
          with CamposComparables do
          begin
               CommaText := sComparables;
          end;
          GetFieldsDescriptions( CamposComparables );
     end;
end;

procedure TTablaData.LoadPrimaryKey( Source: TDataSet; Target: TMyQuery );
var
   i: Integer;
   sCampo: String;
begin
     with FLlaves do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               sCampo := Names[ i ];
               Target.ParamByName( sCampo ).AssignField( Source.FieldByName( sCampo ) );
          end;
     end;
end;

function TTablaData.GetPrimaryKeyValues( Source: TDataSet ): String;
var
   i: Integer;
   sFieldName, sValue: String;
begin
     Result := VACIO;
     with FLlaves do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               sFieldName := Names[ i ];
               with Source.FieldByName( sFieldName )do
               begin
                    case DataType of
                         ftDate: sValue := FormatDateTime( 'dd/mmm/yyyy', AsDateTime );
                         ftDateTime: sValue := FormatDateTime( 'dd/mmm/yyyy', AsDateTime );
                         ftTime: sValue := FormatDateTime( 'dd/mmm/yyyy', AsDateTime );
                    else
                        sValue := AsString;
                    end;
               end;
               Result := Result + Format( '%s=%s, ', [ sFieldName, sValue ] );
          end;
     end;
     Result := ZetaCommonTools.CortaUltimo( Trim( Result ) );
end;

function TTablaData.RowExists: Boolean;
begin
     with FQSeek do
     begin
          Active := False;
          LoadPrimaryKey( FQRead, FQSeek );
          Active := True;
          Result := not IsEmpty;
     end;
end;

function TTablaData.GetNamesList( Lista: TStrings ): String;
var
   i: Integer;
begin
     Result := VACIO;
     with Lista do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               if UsaDBSYS then
                  Result := Result + Strings[ i ] + ','
               else
                   Result := Result + Names[ i ] + ',';
          end;
     end;
     Result := ZetaCommonTools.CortaUltimo( Trim( Result ) );
end;

procedure TTablaData.PrepareQuerySeek;
var
   sCampos: String;
begin
     if ( FComparables.Count > 0 ) then
        sCampos := GetNamesList( FComparables )
     else
         sCampos := GetNamesList( FLlaves );
     FQSeek := CreateQueryMaster( Format( 'select %s from %s where %s', [ sCampos, NombreReal, BuildPrimaryKeyFilter ] ) );
end;

function TTablaData.GetQueryInsert: String;
var
   i: Integer;
   sCampos, sValues: String;
begin
     sCampos := VACIO;
     sValues := VACIO;
     with FCampos do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               if ( Pos( Strings[ i ], FIgnorar ) = 0 ) then
               begin
                    sCampos := sCampos + Strings[ i ] + ',';
                    sValues := sValues + ':' + Strings[ i ] + ',';
               end;
          end;
          sCampos := ZetaCommonTools.CortaUltimo( sCampos );
          sValues := ZetaCommonTools.CortaUltimo( sValues );
     end;
     Result := Format( 'insert into %s ( %s ) values ( %s )', [ NombreReal, sCampos, sValues ] );
end;

function TTablaData.GetQueryRead: String;
begin
     {$ifdef INTERBASE}
     Result := Format( 'select %s from %s order by %s', [ FCampos.CommaText, NombreReal, GetNamesList( FLlaves ) ] );
     {$endif}
     {$ifdef MSSQL}
     Result := Format( 'select %s from %s.%s order by %s', [ FCampos.CommaText, FDbSourceName, NombreReal, GetNamesList( FLlaves ) ] );
     {$endif}
end;

function TTablaData.GetQueryTransfer( const iOldEmp, iNewEmp: TNumEmp ): String;
{$ifdef MSSQL}
const
     K_NUM_EMP = 'CB_CODIGO';
var
   i: Integer;
   sCampos, sValues: String;
{$endif}
begin
     {$ifdef INTERBASE}
     Result := VACIO;
     {$endif}
     {$ifdef MSSQL}
     sValues := VACIO;
     with FCampos do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               if ( Pos( Strings[ i ], FIgnorar ) = 0 ) then
               begin
                    sValues := sValues + Strings[ i ] + ',';
               end;
          end;
     end;
     sValues := ZetaCommonTools.CortaUltimo( sValues );
     sCampos := sValues;
     if ( iNewEmp <> iOldEmp ) then
        sCampos := StrTransform( sCampos, K_NUM_EMP, Format( '%d %s', [ iNewEmp, K_NUM_EMP ] ) );
     Result := Format( 'insert into %0:s ( %1:s ) select %2:s from %3:s.%0:s where ( CB_CODIGO = %4:d )', [ NombreReal, sValues, sCampos, FdbSourceName, iOldEmp ] );
     {$endif}
end;


procedure TTablaData.PrepareQueryInsert;
begin
     {$ifdef INTERBASE}
     FQInsert := CreateQueryMaster( GetQueryInsert );
     {$endif}
     {$ifdef MSSQL}
     FQInsert := CreateQueryMaster( GetQueryInsert );
     {$endif}
end;

procedure TTablaData.PrepareQueryRead( const sScript: String );
begin
     FQRead := CreateQuerySource( sScript );
end;

function TTablaData.CompareFieldValues: Boolean;
var
   i, iPos: Integer;
   sFieldName, sDescription: String;
   Master, Source: TField;
   lNotEqual: Boolean;

procedure AddSingleRow( sMaster, sSource: String );
begin
     sMaster := Trim( sMaster );
     sSource := Trim( sSource );
     with FDifferences do
     begin
          if ( Length( sMaster + sSource ) > 40 ) then
          begin
               Add( Format( '%s ( %s ):', [ sDescription, sFieldName ] ) );
               Add( Format( 'Maestra: %s', [ sMaster ] ) );
               Add( Format( 'Fuente:  %s', [ sSource ] ) );
          end
          else
              Add( Format( '%s ( %s ): ', [ sDescription, sFieldName ] ) + Format( K_DELTA_VALUES, [ sMaster, sSource ] ) );
     end;
end;

begin
     Result := False;
     iPos := FDifferences.Count;
     with CamposComparables do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               sFieldName := Names[ i ];
               sDescription := Values[ sFieldName ];
               Master := FQSeek.FieldByName( sFieldName );
               Source := FQRead.FieldByName( sFieldName );
               if Assigned( Master ) and Assigned( Source ) then
               begin
                    lNotEqual := False;
                    case Master.DataType of
                         ftString:
                         begin
                              if ( Source.AsString <> Master.AsString ) then
                              begin
                                   lNotEqual := True;
                                   AddSingleRow( Master.AsString, Source.AsString );
                              end;
                         end;
                         ftSmallInt, ftInteger, ftWord:
                         begin
                              if ( Source.AsInteger <> Master.AsInteger ) then
                              begin
                                   lNotEqual := True;
                                   AddSingleRow( Master.AsString, Source.AsString );
                              end;
                         end;
                         ftBoolean:
                         begin
                              if ( Source.AsBoolean <> Master.AsBoolean ) then
                              begin
                                   lNotEqual := True;
                                   AddSingleRow( ZetaCommonTools.zBoolToStr( Master.AsBoolean ),
                                                 ZetaCommonTools.zBoolToStr( Source.AsBoolean ) );
                              end;
                         end;
                         ftFloat:
                         begin
                              if ( Source.AsFloat <> Master.AsFloat ) then
                              begin
                                   lNotEqual := True;
                                   AddSingleRow( Master.AsString, Source.AsString );
                              end;
                         end;
                         ftCurrency:
                         begin
                              if ( Source.AsCurrency <> Master.AsCurrency ) then
                              begin
                                   lNotEqual := True;
                                   AddSingleRow( Master.AsString, Source.AsString );
                              end;
                         end;
                         ftDate, ftTime, ftDateTime:
                         begin
                              if ( Source.AsDateTime <> Master.AsDateTime ) then
                              begin
                                   lNotEqual := True;
                                   AddSingleRow( FormatDateTime( 'dd/mmm/yyyy', Master.AsDateTime ),
                                                 FormatDateTime( 'dd/mmm/yyyy', Source.AsDateTime ) );
                              end;
                         end;
                         ftMemo:
                         begin
                              if ( Source.AsString <> Master.AsString ) then
                              begin
                                   lNotEqual := True;
                                   AddSingleRow( Master.AsString, Source.AsString );
                              end;
                         end;
                    end;
                    if lNotEqual then
                    begin
                         Result := True;
                    end;
               end;
          end;
     end;
     if Result then
     begin
          with FDifferences do
          begin
               Insert( iPos, GetPrimaryKeyValues( FQRead ) );
               Insert( iPos, ' ' );
          end;
     end;
end;

procedure TTablaData.FieldsToParams;
var
   i: Integer;
   sFieldName: String;
begin
     {$ifdef INTERBASE}
     with FQRead do
     begin
          for i := 0 to ( FieldCount - 1 ) do
          begin
               with Fields[ i ] do
               begin
                    sFieldName := FieldName;
                    if ( Pos( sFieldName, FIgnorar ) = 0 ) then
                    begin
                         if ( FieldKind = fkData ) then
                         begin
                              if ( Fields[ i ] is TBlobField ) and ( TBlobField( Fields[ i ] ).BlobSize = 0 ) then
                                 FQInsert.ParamByName( FieldName ).AsBlob := TBlobData( VACIO )
                              else
                                  FQInsert.ParamByName( FieldName ).AssignField( Fields[ i ] );
                         end;
                    end;
               end;
          end;
     end;
     with FQInsert do
     begin
          for i := 0 to ( Params.Count - 1 ) do
          begin
               with Params[ i ] do
               begin
                    if IsNull then
                    begin
                         case DataType of
                              ftString: AsString := VACIO;
                              ftDate, ftTime, ftDateTime: AsDateTime := NullDateTime;
                              ftInteger, ftSmallInt, ftWord: AsInteger := 0;
                              ftFloat, ftCurrency: AsFloat := 0;
                              ftBlob: AsBlob := TBlobData( VACIO );
                              ftMemo: AsString := VACIO;
                         end;
                    end;
               end;
          end;
     end;
     {$endif}
     {$ifdef MSSQL}
     with FQRead do
     begin
          for i := 0 to ( FieldCount - 1 ) do
          //for i := 0 to ( FieldCount - 1 ) downto 0 do
          begin
               sFieldName := Fields[ i ].FieldName;
               if ( Pos( sFieldName, FIgnorar ) = 0 ) then
               begin
                    FQInsert.ParamByName( sFieldName ).Assign( Fields[ i ] );
               end;
          end;
     end;
     {$endif}
end;

procedure TTablaData.TransferFieldValues;
begin
     try
        FieldsToParams;
        with FQInsert do
        begin
             ExecSQL;
        end;
        NotifyDelta;
     except
           on Error: Exception do
           begin
                HandleException( 'Error Al Transferir', Error );
           end;
     end;
end;

procedure TTablaData.TransferPrepare;
begin
     {$ifdef INTERBASE}
     PrepareQueryInsert;
     PrepareQueryRead( Format( 'select %s from %s where ( CB_CODIGO = :Empleado ) order by %s', [ FCampos.CommaText, NombreReal, GetNamesList( FLlaves ) ] ) );
     {$endif}
end;

function TTablaPresup.GetQueryTransfer( const iOldEmp, iNewEmp: TNumEmp ): String;
{$ifdef MSSQL}
const
     K_NUM_EMP = 'CB_CODIGO';
var
   i: Integer;
   sCampos, sValues,sFiltro: String;
{$endif}
begin

     {$ifdef MSSQL}
     sValues := VACIO;
     with FCampos do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               if ( Pos( Strings[ i ], FIgnorar ) = 0 ) then
               begin
                    sValues := sValues + Strings[ i ] + ',';
               end;
          end;
     end;
     sValues := ZetaCommonTools.CortaUltimo( sValues );
     sCampos := sValues;
     if ( iNewEmp <> iOldEmp ) then
        sCampos := StrTransform( sCampos, K_NUM_EMP, Format( '%d %s', [ iNewEmp, K_NUM_EMP ] ) );

     sFiltro := Format( ConcatFiltros( '( CB_CODIGO = %d )', Parentesis( FFiltro )) , [iOldEmp] );
     Result := Format( 'insert into %0:s ( %1:s ) select %2:s from %3:s.%0:s where ( %4:s )', [ NombreReal, sValues, sCampos, FdbSourceName, sFiltro ] );
     {$endif}
end;
                         {
procedure TTablaPresupCat.TransferPrepare;
 var sScript,sCondicionNull, sCondicionNotNull: string;
     i: integer;
begin
     for i:=0 to FCampos.Count - 1do
     begin
          sCondicionNull := ConcatString(  sCondicionNull , Parentesis( FCampos[i] + ' is NULL' ),' OR ' );
          sCondicionNotNull := ConcatFiltros ( sCondicionNotNull , Parentesis( FCampos[i] + ' is NOT NULL' ) );
     end;

     sCondicionNull :=  'WHERE ' + sCondicionNull ;
     sCondicionNotNull := 'WHERE ' + sCondicionNotNull ;

     sScript:=  GetQueryFiltro( Format( 'select %s from %s %s order by %s', [ FCampos.CommaText, NombreReal, '%s', GetNamesList( FLlaves ) ] ), vacio );

     PrepareQueryRead( Format( sScript, [sCondicionNotNull] ) );
     FQReadNulls := CreateQuerySource( Format( sScript, [sCondicionNull] ) );

     FQReadDestino := CreateQueryMaster( Format( StrTransform( sScript, 'select', 'select TOP 1 ' ), [VACIO] ) );

     //Se prepara el query de insercion con todas las columnas Nulas;
     FQInsert := CreateQueryMaster( GetQueryInsert )
end;              }

procedure TTablaPresup.TransferPrepare;
 var sScript: string;
begin
     sScript:=  GetQueryFiltro( Format( 'select %s from %s %s order by %s', [ FCampos.CommaText, NombreReal, K_CONDICION, GetNamesList( FLlaves ) ] ),
                                                                              ConcatFiltros( FFiltro, '( CB_CODIGO = :Empleado )' ) );
     PrepareQueryInsert;
     PrepareQueryRead( sScript );
end;


{function TTablaPresupCat.GetQueryInsertSinNulls: String;
var
   i: Integer;
   sCampos, sValues: String;
begin
     sCampos := VACIO;
     sValues := VACIO;
     with FCampos do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               if ( Pos( Strings[ i ], FIgnorar ) = 0 ) and
                  ( FCamposNulos.IndexOf( Strings[i] )< 0 ) then
               begin
                    sCampos := sCampos + Strings[ i ] + ',';
                    sValues := sValues + ':' + Strings[ i ] + ',';
               end;
          end;
          sCampos := ZetaCommonTools.CortaUltimo( sCampos );
          sValues := ZetaCommonTools.CortaUltimo( sValues );
     end;
     Result := Format( 'insert into %s ( %s ) values ( %s )', [ NombreReal, sCampos, sValues ] );
end;
}

function TTablaPresup.RowExists: Boolean;
begin
     with FQSeek do
     begin
          Active := False;
          Active := True;
          Result := not IsEmpty;
     end;
end;

procedure TTablaPresupCat.TransferFieldValues;
begin
     try
        FieldsToParams;
        with FQInsert do
        begin
             ExecSQL;
        end;
     except
           on Error: Exception do
           begin
                if NOT( Conservar and DMigrar.PK_Violation(Error)) then
                begin
                     HandleException( 'Error Al Transferir', Error );
                end;
           end;
     end;
end;
{
procedure TTablaPresupCat.TransferFieldValuesNulls;
begin
     try
        FieldsToParamsNulls;
        with FQInsert do
        begin
             ExecSQL;
        end;
     except
           on Error: Exception do
           begin
                HandleException( 'Error Al Transferir', Error );
           end;
     end;
end;
}
{$ifdef FALSE}
procedure TTablaPresupCat.FieldsToParams;
var
   i: Integer;
   sFieldName: String;
begin
     {$ifdef MSSQL}
     with FQRead do
     begin
          if NOT FQReadDestino.Active then
             FQReadDestino.Active := TRUE;

          for i := 0 to ( FieldCount - 1 ) do
          begin
               sFieldName := Fields[ i ].FieldName;
               if ( Pos( sFieldName, FIgnorar ) = 0 ) then
               begin
                    if ( Fields[ i ].DataType = ftString ) then
                       FQInsert.ParamByName( sFieldName ).AsString := Copy( Fields[ i ].AsString, 1, FQReadDestino.FieldByName( sFieldName ).Size )
                    else
                        FQInsert.ParamByName( sFieldName ).Assign( Fields[ i ] );
               end;
          end;
     end;
     {$endif}
end;
{$ENDIF}

{$ifdef FALSE}
procedure TTablaPresupCat.FieldsToParamsNulls;
var
   i: Integer;
   sFieldName: String;
begin
     {$ifdef MSSQL}
     if ( FCamposNulos = NIL ) then
          FCamposNulos:= TStringList.Create
     else
         FCamposNulos.Clear;

     with FQReadNulls do
     begin
          //Reviso primero si existe algun dato NULO
          for i := 0 to ( FieldCount - 1 ) do
          begin
               if Fields[ i ].IsNull then
               begin
                    FCamposNulos.Add( Fields[ i ].FieldName );
               end;
          end;
          //Se prepara el query de insercion SIN las columnas Nulas;
          if FQInsert = NIL then
             FQInsert := CreateQueryMaster( GetQueryInsertSinNulls )
          else
              PrepareQuery( FQInsert, GetQueryInsertSinNulls );

          if NOT FQReadDestino.Active then
             FQReadDestino.Active := TRUE;

          for i := 0 to ( FieldCount - 1 ) do
          begin
               if NOT Fields[ i ].IsNull then
               begin
                    sFieldName := Fields[ i ].FieldName;
                    if ( Pos( sFieldName, FIgnorar ) = 0 ) then
                    begin
                         if ( Fields[ i ].DataType = ftString ) then
                            FQInsert.ParamByName( sFieldName ).AsString := Copy( Fields[ i ].AsString, 1, FQReadDestino.FieldByName( sFieldName ).Size )
                         else
                             FQInsert.ParamByName( sFieldName ).Assign( Fields[ i ] );
                    end;
               end;
          end;
     end;
     {$endif}
end;
{$ENDIF}

function TTablaPresup.GetQueryInsertSinNulls: String;
var
   i: Integer;
   sCampos, sValues: String;
begin
     sCampos := VACIO;
     sValues := VACIO;
     with FCampos do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               if ( Pos( Strings[ i ], FIgnorar ) = 0 ) and
                  ( FCamposNulos.IndexOf( Strings[i] )< 0 ) then
               begin
                    sCampos := sCampos + Strings[ i ] + ',';
                    sValues := sValues + ':' + Strings[ i ] + ',';
               end;
          end;
          sCampos := ZetaCommonTools.CortaUltimo( sCampos );
          sValues := ZetaCommonTools.CortaUltimo( sValues );
     end;
     Result := Format( 'insert into %s ( %s ) values ( %s )', [ NombreReal, sCampos, sValues ] );
end;

procedure TTablaPresupCat.TransferPrepare;
 var sScript,sCondicionNull, sCondicionNotNull: string;
     i: integer;
begin
     for i:=0 to FCampos.Count - 1do
     begin
          sCondicionNull := ConcatString(  sCondicionNull , Parentesis( FCampos[i] + ' is NULL' ),' OR ' );
          sCondicionNotNull := ConcatFiltros ( sCondicionNotNull , Parentesis( FCampos[i] + ' is NOT NULL' ) );
     end;

     sCondicionNull :=  'WHERE ' + sCondicionNull ;
     sCondicionNotNull := 'WHERE ' + sCondicionNotNull ;

     sScript:=  GetQueryFiltro( Format( 'select %s from %s %s order by %s', [ FCampos.CommaText, NombreReal, '%s', GetNamesList( FLlaves ) ] ), vacio );

     PrepareQueryRead( Format( sScript, [sCondicionNotNull] ) );
     FQReadNulls := CreateQuerySource( Format( sScript, [sCondicionNull] ) );

     FQReadDestino := CreateQueryMaster( Format( StrTransform( sScript, 'select', 'select TOP 1 ' ), [VACIO] ) );

     //Se prepara el query de insercion con todas las columnas Nulas;
     FQInsert := CreateQueryMaster( GetQueryInsert );

     if ( FNombre = 'PLAZA' ) then
        FQInsert.SQL.Text := 'set identity_insert PLAZA ON;' +CR_LF + FQInsert.SQL.Text;

end;

procedure TTablaPresup.TransferFieldValues;
begin
     try
        FieldsToParams;
        with FQInsert do
        begin
             ExecSQL;
        end;
     except
           on Error: Exception do
           begin
                if NOT( Conservar and DMigrar.PK_Violation(Error)) then
                begin
                     HandleException( 'Error Al Transferir', Error );
                end;
           end;
     end;
end;

procedure TTablaPresup.TransferFieldValuesNulls;
begin
     try
        FieldsToParamsNulls;
        with FQInsert do
        begin
             ExecSQL;
        end;
     except
           on Error: Exception do
           begin
                if NOT( Conservar and DMigrar.PK_Violation(Error)) then
                begin
                     HandleException( 'Error Al Transferir', Error );
                end;
           end;
     end;
end;

procedure TTablaPresup.FieldsToParams;
{$ifdef MSSQL}
var
   i: Integer;
   sFieldName: String;
{$endif}
begin
     {$ifdef MSSQL}
     with FQRead do
     begin
          if NOT FQReadDestino.Active then
             FQReadDestino.Active := TRUE;

          for i := 0 to ( FieldCount - 1 ) do
          begin
               sFieldName := Fields[ i ].FieldName;
               if ( Pos( sFieldName, FIgnorar ) = 0 ) then
               begin
                    if ( Fields[ i ].DataType = ftString ) then
                       FQInsert.ParamByName( sFieldName ).AsString := Copy( Fields[ i ].AsString, 1, FQReadDestino.FieldByName( sFieldName ).Size )
                    else
                        FQInsert.ParamByName( sFieldName ).Assign( Fields[ i ] );
               end;
          end;
     end;
     {$endif}
end;

procedure TTablaPresup.FieldsToParamsNulls;
{$ifdef MSSQL}
var
   i: Integer;
   sFieldName: String;
{$endif}
begin
     {$ifdef MSSQL}
     if ( FCamposNulos = NIL ) then
          FCamposNulos:= TStringList.Create
     else
         FCamposNulos.Clear;

     with FQReadNulls do
     begin
          //Reviso primero si existe algun dato NULO
          for i := 0 to ( FieldCount - 1 ) do
          begin
               if Fields[ i ].IsNull then
               begin
                    FCamposNulos.Add( Fields[ i ].FieldName );
               end;
          end;
          //Se prepara el query de insercion SIN las columnas Nulas;
          if FQInsert = NIL then
             FQInsert := CreateQueryMaster( GetQueryInsertSinNulls )
          else
              PrepareQuery( FQInsert, GetQueryInsertSinNulls );

          if NOT FQReadDestino.Active then
             FQReadDestino.Active := TRUE;

          for i := 0 to ( FieldCount - 1 ) do
          begin
               if NOT Fields[ i ].IsNull then
               begin
                    sFieldName := Fields[ i ].FieldName;
                    if ( Pos( sFieldName, FIgnorar ) = 0 ) then
                    begin
                         if ( Fields[ i ].DataType = ftString ) then
                            FQInsert.ParamByName( sFieldName ).AsString := Copy( Fields[ i ].AsString, 1, FQReadDestino.FieldByName( sFieldName ).Size )
                         else
                             FQInsert.ParamByName( sFieldName ).Assign( Fields[ i ] );
                    end;
               end;
          end;
     end;
     {$endif}
end;

procedure TTablaPresup.PrepareQuerySeek;
var
   sCampos: String;
begin
     if ( FComparables.Count > 0 ) then
        sCampos := GetNamesList( FComparables )
     else
         sCampos := GetNamesList( FLlaves );
     FQSeek := CreateQueryMaster( Format( 'select %s from %s ', [ sCampos, NombreReal  ] ) );
end;

procedure TTablaPresupColabora.TransferEmployeeNull( const iOldEmp, iNewEmp: TNumEmp );
begin
     {$ifdef MSSQL}
     PrepareQuery( FQFindTable, GetQueryTransfer( iOldEmp, iNewEmp ) );
     try
        with FQFindTable do
        begin
             ExecSQL;
        end;
     except
           on Error: EDBEngineError do
           begin
                { Si el registro ya existe, hay que seguir transfiriendo }
                with Error do
                begin
                     if ( ErrorCount > 0 ) and ( Errors[ 0 ].ErrorCode = DBIERR_KEYVIOL ) then
                     begin
                          Log.WriteTexto( Format( 'Registro Ya Existe en %s: %s', [ Nombre ] ) );
                     end
                     else
                     begin
                          { En caso contrari, se interrumpe el proceso }
                          raise;
                     end;
                end;
           end;
           on Error: Exception do
           begin
                raise;
           end;
     end;
     {$endif}
end;

function TTablaPresupColabora.GetQueryTransfer( const iOldEmp, iNewEmp: TNumEmp ): String;
{$ifdef MSSQL}
const
     K_NUM_EMP = 'CB_CODIGO';
var
   i: Integer;
   sCampos, sValues: String;
{$endif}
begin
     {$ifdef MSSQL}

     sValues := VACIO;
     if NOT fQReadDestino.Active then
        fQReadDestino.Active := TRUE;

     {if ( FCamposNulos = NIL ) then
          FCamposNulos:= TStringList.Create
     else
         FCamposNulos.Clear;

     with fQRead do
     begin
          //Reviso primero si existe algun dato NULO
          for i := 0 to ( FieldCount - 1 ) do
          begin
               if Fields[ i ].IsNull then
               begin
                    FCamposNulos.Add( Fields[ i ].FieldName );
               end;
          end;
     end; }

     with FCampos do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               if ( Pos( Strings[ i ], FIgnorar ) = 0 ) and
                  ( NOT fQRead.FieldByName(Strings[ i ] ).IsNull ) then
               begin
                    sValues := sValues + Strings[ i ] + ',';

                    with fQReadDestino.FieldByName(Strings[ i ] ) do
                    begin
                         if DataType = ftString then
                            sCampos := sCampos + Format( 'substring( %s, 1, %d )',[ Strings[ i ], Size ]) + ','
                         else
                             sCampos := sCampos + Strings[ i ] + ',';
                    end;
               end;
          end;
     end;
     sValues := ZetaCommonTools.CortaUltimo( sValues );
     sCampos := ZetaCommonTools.CortaUltimo( sCampos );
     //sCampos := sValues;
     if ( iNewEmp <> iOldEmp ) then
        sCampos := StrTransform( sCampos, K_NUM_EMP, Format( '%d %s', [ iNewEmp, K_NUM_EMP ] ) );
     Result := Format( 'insert into %0:s ( %1:s ) select %2:s from %3:s.%0:s where ( CB_CODIGO = %4:d )', [ NombreReal, sValues, sCampos, FdbSourceName, iOldEmp ] );
     {$endif}
end;

function TTablaPresupColabora.GetQueryTransferNull( const iOldEmp, iNewEmp: TNumEmp ): String;
{$ifdef MSSQL}
const
     K_NUM_EMP = 'CB_CODIGO';
var
   i: Integer;
   sCampos, sValues: String;
{$endif}
begin
     {$ifdef MSSQL}

     sValues := VACIO;
     if NOT fQReadDestino.Active then
        fQReadDestino.Active := TRUE;

     with FCampos do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               if ( Pos( Strings[ i ], FIgnorar ) = 0 ) and
                  ( NOT fQReadNulls.FieldByName(Strings[ i ] ).IsNull ) then
               begin
                    sValues := sValues + Strings[ i ] + ',';

                    with fQReadDestino.FieldByName(Strings[ i ] ) do
                    begin
                         if DataType = ftString then
                            sCampos := sCampos + Format( 'substring( %s, 1, %d )',[ Strings[ i ], Size ]) + ','
                         else
                             sCampos := sCampos + Strings[ i ] + ',';
                    end;
               end;
          end;
     end;
     sValues := ZetaCommonTools.CortaUltimo( sValues );
     sCampos := ZetaCommonTools.CortaUltimo( sCampos );
     //sCampos := sValues;
     if ( iNewEmp <> iOldEmp ) then
        sCampos := StrTransform( sCampos, K_NUM_EMP, Format( '%d %s', [ iNewEmp, K_NUM_EMP ] ) );
     Result := Format( 'insert into %0:s ( %1:s ) select %2:s from %3:s.%0:s where ( CB_CODIGO = %4:d )', [ NombreReal, sValues, sCampos, FdbSourceName, iOldEmp ] );
     {$endif}
end;

procedure TTablaPresupColabora.TransferPrepare;
 var sScript,sCondicionNull, sCondicionNotNull: string;
     i: integer;

begin
     for i:=0 to FCampos.Count - 1do
     begin
          sCondicionNull := ConcatString(  sCondicionNull , Parentesis( FCampos[i] + ' is NULL' ),' OR ' );
          sCondicionNotNull := ConcatFiltros ( sCondicionNotNull , Parentesis( FCampos[i] + ' is NOT NULL' ) );
     end;

     //sCondicionNull :=  'WHERE ' + Parentesis( FFiltro ) + ' AND '+ Parentesis( sCondicionNull );
     sCondicionNull :=  'WHERE ' + Parentesis( FFiltro ) ;
     //sCondicionNotNull := 'WHERE ' + Parentesis( FFiltro ) + ' AND '+ Parentesis( sCondicionNotNull );
     sCondicionNotNull := 'WHERE ' + Parentesis( FFiltro ); 

     sScript := CambiaNivel0( FCampos.CommaText );
     sScript := Format( 'select %s from %s %s order by %s', [ sScript, NombreReal, '%s', GetNamesList( FLlaves ) ] );
     PrepareQueryRead( Format( sScript, [sCondicionNotNull] ) );
     FQReadNulls := CreateQuerySource( Format( sScript, [sCondicionNull] ) );
     FQReadDestino := CreateQueryMaster( Format( StrTransform( sScript, 'select', 'select TOP 1 ' ), [VACIO] ) );

     //Se prepara el query de insercion con todas las columnas Nulas;
     FQInsert := CreateQueryMaster( GetQueryInsert )
end;



function TTablaPresupColabora.GetQueryInsertSinNulls: String;
var
   i: Integer;
   sCampos, sValues: String;
begin
     sCampos := VACIO;
     sValues := VACIO;
     with FCampos do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               if ( Pos( Strings[ i ], FIgnorar ) = 0 ) and
                  ( FCamposNulos.IndexOf( Strings[i] )< 0 ) then
               begin
                    sCampos := sCampos + Strings[ i ] + ',';
                    sValues := sValues + ':' + Strings[ i ] + ',';
               end;
          end;
          sCampos := ZetaCommonTools.CortaUltimo( sCampos );
          sValues := ZetaCommonTools.CortaUltimo( sValues );
     end;
     Result := Format( 'insert into %s ( %s ) values ( %s )', [ NombreReal, sCampos, sValues ] );
end;

procedure TTablaPresupColabora.FieldsToParams;
{$ifdef MSSQL}

var
   i: Integer;
   sFieldName: String;
{$endif}
begin
     {$ifdef MSSQL}
     if ( FCamposNulos = NIL ) then
          FCamposNulos:= TStringList.Create
     else
         FCamposNulos.Clear;

     with FQReadNulls do
     begin
          //Reviso primero si existe algun dato NULO
          for i := 0 to ( FieldCount - 1 ) do
          begin
               if Fields[ i ].IsNull then
               begin
                    FCamposNulos.Add( Fields[ i ].FieldName );
               end;
          end;
          //Se prepara el query de insercion SIN las columnas Nulas;
          if FQInsert = NIL then
             FQInsert := CreateQueryMaster( GetQueryInsertSinNulls )
          else
              PrepareQuery( FQInsert, GetQueryInsertSinNulls );

          if NOT FQReadDestino.Active then
             FQReadDestino.Active := TRUE;

          for i := 0 to ( FieldCount - 1 ) do
          begin
               if NOT Fields[ i ].IsNull then
               begin
                    sFieldName := Fields[ i ].FieldName;
                    if ( Pos( sFieldName, FIgnorar ) = 0 ) then
                    begin
                         if ( Fields[ i ].DataType = ftString ) then
                            FQInsert.ParamByName( sFieldName ).AsString := Copy( Fields[ i ].AsString, 1, FQReadDestino.FieldByName( sFieldName ).Size )
                         else
                             FQInsert.ParamByName( sFieldName ).Assign( Fields[ i ] );
                    end;
               end;
          end;
     end;
     {$endif}
end;



procedure TTablaData.InsertEmployee( const iNewEmp: TNumEmp );
begin
     FieldsToParams;
     with FQInsert do
     begin
          with ParamByName( 'CB_CODIGO' ) do
          begin
               if ( AsInteger <> iNewEmp ) then
                  AsInteger := iNewEmp;
          end;
          ExecSQL;
     end;
end;

procedure TTablaData.TransferEmployee( const iOldEmp, iNewEmp: TNumEmp );
{$ifdef INTERBASE}
var
   sLlaves: String;
{$endif}
begin
     {$ifdef INTERBASE}
     with FQRead do
     begin
          Active := False;
          ParamByName( 'Empleado' ).AsInteger := iOldEmp;
          Active := True;
          while not Eof do
          begin
               try
                  InsertEmployee( iNewEmp );
               except
                     on Error: EDBEngineError do
                     begin
                          { Si el registro ya existe, hay que seguir transfiriendo }
                          with Error do
                          begin
                               if ( ErrorCount > 0 ) and ( Errors[ 0 ].ErrorCode = DBIERR_KEYVIOL ) then
                               begin
                                    Log.WriteTexto( Format( 'Registro Ya Existe en %s: %s', [ Nombre, GetPrimaryKeyValues( FQRead ) ] ) );
                               end
                               else
                               begin
                                    { En caso contrari, se interrumpe el proceso }
                                    sLlaves := GetPrimaryKeyValues( FQRead );
                                    if ( sLlaves <> VACIO ) then
                                       Error.Message := Error.Message + CR_LF + sLlaves;
                                    raise;
                               end;
                          end;
                     end;
                     on Error: Exception do
                     begin
                          sLlaves := GetPrimaryKeyValues( FQRead );
                          if ( sLlaves <> VACIO ) then
                             Error.Message := Error.Message + CR_LF + sLlaves;
                          raise;
                     end;
               end;
               Next;
          end;
          Active := False;
          Unprepare;
     end;
     with FQInsert do
     begin
          Unprepare;
     end;
     {$endif}
     {$ifdef MSSQL}
     PrepareQuery( FQFindTable, GetQueryTransfer( iOldEmp, iNewEmp ) );
     try
        with FQFindTable do
        begin
             ExecSQL;
        end;
     except
           on Error: EDBEngineError do
           begin
                { Si el registro ya existe, hay que seguir transfiriendo }
                with Error do
                begin
                     if ( ErrorCount > 0 ) and ( Errors[ 0 ].ErrorCode = DBIERR_KEYVIOL ) then
                     begin
                          Log.WriteTexto( Format( 'Registro Ya Existe en %s: %s', [ Nombre ] ) );
                     end
                     else
                     begin
                          { En caso contrari, se interrumpe el proceso }
                          raise;
                     end;
                end;
           end;
           on Error: Exception do
           begin
                raise;
           end;
     end;
     {$endif}
end;

procedure TTablaData.NotifyDelta;
begin
     FDelta.Add( GetPrimaryKeyValues( FQRead ) );
end;

function TTablaData.Comparar: Boolean;
var
   i: Integer;
   sValue: String;
begin
     Result := False;
     FDifferences.Clear;
     FDelta.Clear;
     try
        PrepareQueryRead( GetQueryRead );
        PrepareQuerySeek;
        if Transfer then
        begin
             FdmCompara.TxStart;
             PrepareQueryInsert;
        end;

        try
           with FQRead do
           begin
                Active := True;
                while not Eof do
                begin
                     if RowExists then
                        CompareFieldValues
                     else
                     begin
                          Result := True;
                          if Transfer then
                             TransferFieldValues
                          else
                              NotifyDelta;
                     end;
                     Next;
                end;
                Active := False;
           end;
           if Transfer then
              FdmCompara.TxCommit;
        except
              On Error : Exception do
              begin
                   if Transfer then
                      FdmCompara.TxRollBack;
                   raise;
              end;
        end;
     except
           on Error: Exception do
           begin
                HandleException( 'Error Al Comparar', Error );
           end;
     end;
     if ( FDifferences.Count > 0 ) or ( FDelta.Count > 0 ) then
     begin
          with Log do
          begin
               WriteTexto( StringOfChar( '=', K_ANCHO_LINEA ) );
               WriteTexto( Format( 'TABLA %s ( %s )', [ Nombre, Descripcion ] ) );
               with Llaves do
               begin
                    sValue := 'Llave Primaria: ';
                    for i := 0 to ( Count - 1 ) do
                    begin
                         WriteTexto( sValue + Strings[ i ] );
                         sValue := StringOfChar( ' ', Length( sValue ) );
                    end;
               end;
               WriteTexto( StringOfChar( '=', K_ANCHO_LINEA ) );
               with FDifferences do
               begin
                    if ( Count > 0 ) then
                    begin
                         WriteTexto( 'Registros Diferentes En Empresa Maestra ' + Format( K_DELTA_VALUES, [ '<Valor Maestro>', '<Valor Fuente>' ] ) );
                         WriteTexto( StringOfChar( '-', K_ANCHO_LINEA ) );
                         for i := 0 to ( Count - 1 ) do
                         begin
                              WriteTexto( Strings[ i ] );
                         end;
                         WriteTexto( StringOfChar( '-', K_ANCHO_LINEA ) );
                    end;
               end;
               with FDelta do
               begin
                    if ( Count > 0 ) then
                    begin
                         if Transfer then
                            WriteTexto( 'Registros Agregados A Empresa Maestra' )
                         else
                             WriteTexto( 'Registros No Existentes En Empresa Maestra' );
                         WriteTexto( StringOfChar( '-', K_ANCHO_LINEA ) );
                         for i := 0 to ( Count - 1 ) do
                         begin
                              WriteTexto( Strings[ i ] );
                         end;
                         WriteTexto( StringOfChar( '-', K_ANCHO_LINEA ) );
                    end;
               end;
               WriteTexto( VACIO );
               WriteTexto( VACIO );
          end;
     end;
     Transfer := Result;
     FDifferences.Clear;
     FDelta.Clear;
end;

{ ********* TNomParam ********** }

procedure TNomParam.BuildPrimaryKeyList;
begin
     with Llaves do
     begin
          Clear;
          Add( 'NP_NOMBRE' );
     end;
     GetFieldsDescriptions( Llaves );
end;

function TNomParam.Comparar: Boolean;
begin
     FQFolio := CreateQueryMaster( 'select MAX( NP_FOLIO ) MAXIMO from NOMPARAM' );
     try
        Result := inherited Comparar;
     finally
            FreeAndNil( FQFolio );
     end;
end;

procedure TNomParam.TransferFieldValues;
var
   iFolio: Integer;
begin
     with FQFolio do
     begin
          Active := True;
          iFolio := Fields[ 0 ].AsInteger;
          Active := False;
     end;
     try
        FieldsToParams;
        with FQInsert do
        begin
             ParamByName( 'NP_FOLIO' ).AsInteger := iFolio + 1;
             ExecSQL;
        end;
        NotifyDelta;
     except
           on Error: Exception do
           begin
                HandleException( 'Error Al Transferir', Error );
           end;
     end;
end;

{ ********* TGlobal ************* }

function TGlobal.GetPrimaryKeyValues( Source: TDataSet ): String;
begin
     with Source do
     begin
          Result := Format( '%d .- %s', [ FieldByName( 'GL_CODIGO' ).AsInteger, FieldByName( 'GL_DESCRIP' ).AsString ] );
     end;
end;

{ ********* TCatalogo ************* }

function TCatalogo.GetQueryRead: String;
const
     K_CONDITION = 'KARDEX.%s = %s.%s';
     K_COUNTERS = '( select COUNT(*) from KARDEX where ( %s ) and ' +
                  '( ( select CB_ACTIVO from COLABORA where COLABORA.CB_CODIGO = KARDEX.CB_CODIGO ) = ''S'' ) ) ACTIVOS, '+
                  '( select COUNT(*) from KARDEX where ( %s ) and '+
                  '( ( select CB_ACTIVO from COLABORA where COLABORA.CB_CODIGO = KARDEX.CB_CODIGO ) <> ''S'' ) ) INACTIVOS';
var
   sCondition, sCounters: String;
begin
     Ignorar := 'ACTIVOS;INACTIVOS';
     sCondition := Format( K_CONDITION, [ FKardexKeyField, Nombre, FLlaves.Names[ 0 ] ] );
     sCounters := Format( K_COUNTERS, [ sCondition, sCondition ] );
     Result := GetQueryFiltro( Format( 'select %s,%s from %s %s order by %s', [ FCampos.CommaText, sCounters, NombreReal, K_CONDICION, GetNamesList( FLlaves ) ] ), FFiltro );
end;

function TCatalogo.GetPrimaryKeyValues( Source: TDataSet ): String;
const
     K_MENSAJE = '%s ( Empleados Activos: %.0n, Inactivos: %.0n )';
begin
     with Source do
     begin
          Result := Format( K_MENSAJE, [ inherited GetPrimaryKeyValues( Source ),
                                         FieldByName( 'ACTIVOS' ).AsFloat,
                                         FieldByName( 'INACTIVOS' ).AsFloat ] );
     end;
end;

{ ********* TColabora ************* }

function TColabora.CambiaNivel0( const sScript: String ): String;
const
     K_NIVEL_0 = 'CB_NIVEL0';
begin
     Result := sScript;
     if ZetaCommonTools.StrLleno( FNivel0 ) then
        Result := StrTransform( Result, K_NIVEL_0, Format( '%s %s', [ QuotedStr( FNivel0 ), K_NIVEL_0 ] ) );
end;

function TColabora.GetQueryTransfer( const iOldEmp, iNewEmp: TNumEmp ): String;
begin
     Result := CambiaNivel0( inherited GetQueryTransfer( iOldEmp, iNewEmp ) );
end;

procedure TColabora.TransferPrepare;
const
     K_NIVEL_0 = 'CB_NIVEL0';
var
   sScript: String;
begin
     sScript := CambiaNivel0( FCampos.CommaText );
     sScript := GetQueryFiltro( Format( 'select %s from %s %s order by %s', [ sScript, NombreReal, K_CONDICION, GetNamesList( FLlaves ) ] ), FFiltro );
     PrepareQueryInsert;
     PrepareQueryRead( sScript );
end;

{$ifdef INTERBASE}
procedure TColabora.TransferEmployee( const iOldEmp, iNewEmp: TNumEmp );
var
   sLlaves: String;
begin
     try
        InsertEmployee( iNewEmp );
     except
           on Error: Exception do
           begin
                sLlaves := GetPrimaryKeyValues( FQRead );
                if ( sLlaves <> VACIO ) then
                   Error.Message := Error.Message + CR_LF + sLlaves;
                raise;
           end;
     end;
end;
{$endif}

{ ********* TKardex ******** }

{ ********* TdmCompara ********* }

procedure TdmCompara.DataModuleCreate(Sender: TObject);
begin
     inherited;
     FTablas := TList.Create;
     FEmpresas := TCompanyList.Create;
     FNivel0 := TStringList.Create;
end;

procedure TdmCompara.DataModuleDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( FNivel0 );
     FreeAndNil( FEmpresas );
     FreeAndNil( FTablas );
end;

function TdmCompara.GetEmpresas(Index: Integer): TCompanyData;
begin
     Result := FEmpresas.Empresa[ Index ];
end;

function TdmCompara.GetEmpresaActiva: TCompanyData;
begin
     Result := Empresas[ FEmpresaActiva ];
end;

function TdmCompara.GetEmpresaFuente: TCompanyData;
begin
     Result := Empresas[ FEmpresaFuente ];
end;

function TdmCompara.GetEmpresasCount: Integer;
begin
     Result := FEmpresas.Count;
end;

function TdmCompara.InitEmpresas( Lista: TStrings; lPresupuesto : Boolean = False ): Boolean;
var
   i: Integer;
begin
     with dmDBConfig do
     begin
          FEmpresaActiva := LlenaListaCompaniasFuente( FEmpresas , lPresupuesto );
          LeeListaNivel0( FNivel0 );
     end;
     if ( lPresupuesto ) or ( FEmpresaActiva >= 0 ) then
     begin
          with Lista do
          begin
               BeginUpdate;
               try
                  Clear;
                  for i := 0 to ( EmpresasCount - 1 ) do
                  begin
                       if lPresupuesto then
                       begin
                            if ( EmpresaActiva <> NIL) then
                            begin
                                 if ( Empresas[ i ].Nombre <> EmpresaActiva.Nombre ) then
                                 begin
                                      AddObject( Empresas[ i ].Nombre, TObject( i ) );
                                 end;
                            end;
                       end
                       else
                       begin
                            if ( Empresas[ i ].Nombre <> EmpresaActiva.Nombre ) then
                            begin
                                 AddObject( Empresas[ i ].Nombre, TObject( i ) );
                            end;
                       end;
                  end;
               finally
                      EndUpdate;
               end;
               Result := ( Count > 0 );
          end;
     end
     else
         Result := False;
end;

function TdmCompara.GetTablas( Index: Integer ): TTablaData;
begin
     Result := TTablaData( FTablas.Items[ Index ] );
end;

function TdmCompara.GetTablasCount: Integer;
begin
     Result := FTablas.Count;
end;

procedure TdmCompara.ClearTablas;
var
   i: Integer;
begin
     for i := ( TablasCount - 1 ) downto 0 do
     begin
          Tablas[ i ].Free;
     end;
end;

procedure TdmCompara.TxStart;
begin
     {$ifdef INTERBASE}
     with dbTarget do
     begin
          StartTransaction;
     end;
     {$endif}
     {$ifdef MSSQL}
     with hdbcTarget do
     begin
          StartTransact;
     end;
     {$endif}
end;

procedure TdmCompara.TxCommit;
begin
     {$ifdef INTERBASE}
     with dbTarget do
     begin
          Commit;
     end;
     {$endif}
     {$ifdef MSSQL}
     with hdbcTarget do
     begin
          Commit;
          EndTransact;
     end;
     {$endif}
end;

procedure TdmCompara.TxRollback;
begin
     {$ifdef INTERBASE}
     with dbTarget do
     begin
          Rollback;
     end;
     {$endif}
     {$ifdef MSSQL}
     with hdbcTarget do
     begin
          RollBack;
          EndTransact;
     end;
     {$endif}
end;

{$ifdef MSSQL}
procedure TdmCompara.ConectarODBC( Hdbc: THdbc; const sAlias, sUser, sPassword: String );
{$ifdef USE_ODBC_DATASOURCE}
{$else}
var
   sServer, sDatabase: String;
{$endif}
begin
     with Hdbc do
     begin
          Connected := False;
          {$ifdef USE_ODBC_DATASOURCE}
          Datasource := sAlias;
          {$else}
          ZetaServerTools.GetServerDatabase( sAlias, sServer, sDatabase );
          Driver := MSSQL_DRIVER;
          with Attributes do
          begin
               Clear;
               Add( Format( 'Server=%s', [ sServer ] ) );
               Add( Format( 'Database=%s', [ sDatabase ] ) );
          end;
          {$endif}
          UserName   := sUser;
          Password   := sPassword;
          BeforeConnect := HdbcBeforeConnect;
          Connected := True;
     end;
end;

procedure TdmCompara.DesconectarODBC( Hdbc: THdbc );
begin
     with Hdbc do
     begin
          Connected := False;
     end;
end;

procedure TdmCompara.SetODBCQuery( oHdbc: THdbc; Query: TMyQuery; const sSQL: String );
begin
     with Query do
     begin
          Active := False;
          Hdbc := oHdbc;
          with SQL do
          begin
               Clear;
               if StrLleno( sSQL ) then
               begin
                    Add( sSQL );
               end;
          end;
          Prepare;
     end;
end;
{$endif}

function TdmCompara.ConectarDestino: Boolean;
begin
     try
        with EmpresaActiva do
        begin
             TargetAlias := Alias;
             TargetUserName := UserName;
             TargetPassword := Password;
             {$ifdef INTERBASE}
             OpenDBTarget;
             {$endif}
             {$ifdef MSSQL}
             {$ifdef USE_ODBC_DATASOURCE}
             ConectarODBC( HdbcTarget, Alias, UserName, Password );
             {$else}
             ConectarODBC( HdbcTarget, Datos, UserName, Password );
             {$endif}
             {$endif}
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                Result := False;
           end;
     end;
end;

function TdmCompara.ConectarFuente( const iEmpresa: Integer ): Boolean;
const
     { Tomado de C:\Program Files\Microsoft Data Access SDK\inc\sqlext.h }
     SQL_DATABASE_NAME = 16;    {/* Use SQLGetConnectOption/SQL_CURRENT_QUALIFIER */ }
begin
     try
        FEmpresaFuente := iEmpresa;
        with EmpresaFuente do
        begin
             {$ifdef INTERBASE}
             OpenAlias( dbSourceDatos, Alias, UserName, Password );
             {$endif}
             {$ifdef MSSQL}
             {$ifdef USE_ODBC_DATASOURCE}
             ConectarODBC( HdbcSource, Alias, UserName, Password );
             {$else}
             ConectarODBC( HdbcSource, Datos, UserName, Password );
             {$endif}
             FDbSourceName := HdbcSource.GetInfoString( SQL_DATABASE_NAME );
             {$endif}
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                Result := False;
           end;
     end;
end;

procedure TdmCompara.DesconectarFuente;
begin
     {$ifdef INTERBASE}
     CloseDBTarget;
     CloseDBSourceDatos;
     {$endif}
     {$ifdef MSSQL}
     DesconectarODBC( HdbcTarget );
     DesconectarODBC( HdbcSource );
     {$endif}
end;

procedure TdmCompara.SetTargetQuery( Query: TMyQuery; const sSQL: String );
begin
     {$ifdef INTERBASE}
     AttachTargetQuery( Query, sSQL );
     {$endif}
     {$ifdef MSSQL}
     SetODBCQuery( HdbcTarget, Query, sSQL );
     {$endif}
end;

procedure TdmCompara.InitTableList(const lUsaDBSYS: Boolean = FALSE ); //CV: El parametro lUsaDBSYS indica si se utiliza SYSCOLUMNS Y SYSOBJECTS PARA determinar los campos de una tabla
const
     {$ifdef INTERBASE}
     Q_DICCION_GET_TABLA = 'select DI_TITULO, DI_CALC from DICCION where ( DI_CLASIFI = -1 ) and ( DI_NOMBRE = :Tabla )';
     Q_DICCION_GET_CAMPO = 'select DI_TITULO from DICCION where ( DI_NOMBRE = :Campo ) and ( DI_CLASIFI = :Clase )';
     {$endif}
     {$ifdef MSSQL}
     Q_DICCION_GET_TABLA = 'select EN_TITULO AS DI_TITULO,EN_CODIGO AS DI_CALC from R_ENTIDAD where ( EN_TABLA = :Tabla )';
     Q_DICCION_GET_CAMPO = 'select AT_TITULO AS DI_TITULO from R_ATRIBUTO where ( AT_CAMPO = :Campo ) and ( EN_CODIGO = :Clase )';
     {$endif}

     //Q_SYS_GET_TABLA = 'select DI_TITULO, DI_CALC from DICCION where ( DI_CLASIFI = -1 ) and ( DI_NOMBRE = :Tabla )';
     Q_SYS_GET_CAMPO = 'select C.NAME from syscolumns C left outer join sysobjects T ON T.id = C.id where T.NAME =:Tabla';
begin
     ClearTablas;
     {$ifdef INTERBASE}
     SetTargetQuery( tqTarget, Q_DICCION_GET_TABLA );
     SetTargetQuery( tqDelete, Q_DICCION_GET_CAMPO );
     {$endif}
     {$ifdef MSSQL}
     if lUsaDBSYS then
     begin
          //SetTargetQuery( qryTable, Q_SYS_GET_TABLA );
          SetTargetQuery( qryField, Q_SYS_GET_CAMPO );
     end
     else
     begin
          SetTargetQuery( qryTable, Q_DICCION_GET_TABLA );
          SetTargetQuery( qryField, Q_DICCION_GET_CAMPO );
     end;
     {$endif}
end;

procedure TdmCompara.InitTablas;

procedure Add( const sNombre, sComparables: String );
begin
     with TTablaData.Create( FTablas, sNombre ) do
     begin
          Connect( Self );
          GetTableInfo;
          if ( sComparables = K_TODOS ) then
             BuildCamposComparables( Campos.CommaText )
          else
              BuildCamposComparables( sComparables );
     end;
end;

procedure Cat( const sNombre, sKardexField, sFiltro, sComparables: String );
begin
     with TCatalogo.Create( FTablas, sNombre ) do
     begin
          Connect( Self );
          KardexKeyField := sKardexField;
          Filtro := sFiltro;
          GetTableInfo;
          if ( sComparables = K_TODOS ) then
             BuildCamposComparables( Campos.CommaText )
          else
              BuildCamposComparables( sComparables );
     end;
end;

procedure Nivel( const iNivel: Integer );
const
     K_NIVEL_FIELD_LIST = 'TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO';
begin
     Cat( Format( 'NIVEL%d', [ iNivel ] ), Format( 'CB_NIVEL%d', [ iNivel ] ), VACIO, K_NIVEL_FIELD_LIST );
end;

begin
     {
     Con este query se averiguan las tablas que no tienen CB_CODIGO:

     select DISTINCT( R1.RDB$RELATION_NAME) from RDB$RELATION_FIELDS R1 where
     ( ( select COUNT(*) from RDB$RELATION_FIELDS R2 where
     ( R2.RDB$RELATION_NAME = R1.RDB$RELATION_NAME) and
     ( R2.RDB$FIELD_NAME = 'CB_CODIGO' ) ) = 0 ) and
     not ( R1.RDB$RELATION_NAME like 'RDB$%' )
     group by RDB$RELATION_NAME

     }
     {$ifdef MSSQL}
     OpenDBTarget;
     try
     {$endif}
     InitTableList;
     { Esta lista est� en orden descendente de dependencias, es decir }
     { las tablas hijas van primero que las padres }
     { Para copiar, es necesario procesarla de abajo para arriba }
     Add( 'CONCEPTO', 'CO_A_PTU,CO_ACTIVO,CO_CALCULA,CO_FORMULA,CO_G_IMSS,CO_G_ISPT,CO_IMP_CAL,CO_IMPRIME,CO_LISTADO,CO_MENSUAL,CO_QUERY,CO_RECIBO,CO_TIPO,CO_X_ISPT,CO_ISN' );
     Add( 'ART_80', VACIO );
     Add( 'T_ART_80', VACIO );
     Add( 'NUMERICA', VACIO );
     Add( 'CAFREGLA', VACIO );
     Add( 'INVITA', VACIO );
     Add( 'ENTNIVEL', VACIO );
     Add( 'ENTRENA', VACIO );
     Add( 'CALCURSO', VACIO );
     Add( 'CUR_REV',VACIO);
     Add( 'CCURSO',VACIO);
     Add( 'CURSOPRE',VACIO);
     Add( 'CURSO', VACIO );
     Add( 'MAESTRO', VACIO );
     Cat( 'CLASIFI', 'CB_CLASIFI', VACIO, 'TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_SALARIO,TB_OP1,TB_OP2,TB_OP3,TB_OP4,TB_OP5' );
     Add( 'CONTRATO', VACIO );
     Add( 'EDOCIVIL', VACIO );
     Add( 'ENTIDAD', VACIO );
     Add( 'PTOFIJAS',VACIO);
     Add( 'PTOTOOLS',VACIO);
     Cat( 'PUESTO', 'CB_PUESTO', VACIO, VACIO );
     Add( 'ESTUDIOS', VACIO );
     Add( 'EVENTO', VACIO );
     Add( 'EXTRA1', VACIO );
     Add( 'EXTRA2', VACIO );
     Add( 'EXTRA3', VACIO );
     Add( 'EXTRA4', VACIO );
     Add( 'LIQ_IMSS', VACIO );
     Add( 'PERIODO', 'PE_USO,PE_INC_BAJ,PE_SOLO_EX,PE_FEC_INI,PE_FEC_FIN,PE_FEC_PAG,'+
                     {$ifdef QUINCENALES}
                     'PE_ASI_INI,PE_ASI_FIN,'+
                     {$endif}
                     'PE_MES,PE_DIAS,PE_DIAS_AC,PE_DIA_MES,PE_POS_MES,PE_PER_MES,PE_PER_TOT,PE_AHORRO,PE_PRESTAM' );
     Add( 'FESTIVO', VACIO );
     {
     Cat( 'TURNO', 'CB_TURNO', Format( '( TU_CODIGO <> "%s" )', [ TURNO_VACIO ] ), 'TU_DESCRIP,TU_DIAS,TU_DOBLES,TU_DOMINGO,TU_FESTIVO,TU_HOR_1,TU_HOR_2,TU_HOR_3,TU_HOR_4,TU_HOR_5,TU_HOR_6,TU_HOR_7,TU_HORARIO,TU_JORNADA,TU_NOMINA,TU_RIT_INI,TU_RIT_PAT,TU_TIP_1,TU_TIP_2,' + 'TU_TIP_3,TU_TIP_4,TU_TIP_5,TU_TIP_6,TU_TIP_7,TU_TIP_JOR,TU_INGLES,TU_TEXTO,TU_NUMERO' );
     }
     Cat( 'TURNO', 'CB_TURNO', VACIO, 'TU_DESCRIP,TU_DIAS,TU_DOBLES,TU_DOMINGO,TU_FESTIVO,TU_HOR_1,TU_HOR_2,TU_HOR_3,TU_HOR_4,TU_HOR_5,TU_HOR_6,TU_HOR_7,TU_HORARIO,TU_JORNADA,TU_NOMINA,TU_RIT_INI,TU_RIT_PAT,TU_TIP_1,TU_TIP_2,' + 'TU_TIP_3,TU_TIP_4,TU_TIP_5,TU_TIP_6,TU_TIP_7,TU_TIP_JOR,TU_INGLES,TU_TEXTO,TU_NUMERO' );
     Add( 'HORARIO', 'HO_DESCRIP,HO_ADD_EAT,HO_CHK_EAT,HO_COMER,HO_DOBLES,HO_EXT_FIJ,HO_IGN_EAT,HO_IN_EAT,HO_IN_TARD,HO_IN_TEMP,HO_INTIME,HO_LASTOUT,HO_MIN_EAT,HO_JORNADA,HO_OU_TARD,HO_OU_TEMP,HO_OUT_EAT,HO_OUTTIME,HO_TIPO,HO_EXT_COM,HO_EXT_MIN,HO_OUT_BRK,HO_IN_BRK' );
     Add( 'ORDFOLIO', VACIO );
     Add( 'FOLIO', VACIO );
     with TGlobal.Create( FTablas, 'GLOBAL' ) do
     begin
          Connect( Self );
          GetTableInfo;
          BuildCamposComparables( 'GL_DESCRIP,GL_FORMULA,GL_TIPO' );
     end;
     Add( 'INCIDEN', VACIO );
     Add( 'LEY_IMSS', VACIO );
     Add( 'MONEDA', VACIO );
     Add( 'MOT_AUTO', VACIO );
     Add( 'MOT_BAJA', VACIO );
     Nivel( 1 );
     Nivel( 2 );
     Nivel( 3 );
     Nivel( 4 );
     Nivel( 5 );
     Nivel( 6 );
     Nivel( 7 );
     Nivel( 8 );
     Nivel( 9 );
     {$ifdef ACS}
     Nivel( 10 );
     Nivel( 11 );
     Nivel( 12 );
     {$endif}
     with TNomParam.Create( FTablas, 'NOMPARAM' ) do
     begin
          Connect( Self );
          GetTableInfo;
          BuildCamposComparables( 'NP_FORMULA,NP_TIPO,NP_ACTIVO' );
     end;
     Add( 'OTRASPER', VACIO );
     Add( 'PRESTACI', VACIO );
     Add( 'SSOCIAL', VACIO );
     Add( 'PRIESGO', VACIO );
     Add( 'RPATRON', VACIO );
     Add( 'QUERYS', VACIO );
     Add( 'TAHORRO', VACIO );
     Add( 'RIESGO', VACIO );
     Add( 'SAL_MIN', VACIO );
     Add( 'SUPER', VACIO );
     Add( 'TCAMBIO', VACIO );
     Add( 'TCURSO', VACIO );
     //Add( 'TFIJAS', VACIO ); Se removi� EZ 
     Add( 'TKARDEX', VACIO );
     Add( 'TPERIODO', VACIO );
     Add( 'TPRESTA', VACIO );
     Add( 'TRANSPOR', VACIO );
     Add( 'VIVE_CON', VACIO );
     Add( 'VIVE_EN', VACIO );
     Add( 'TALLA', VACIO );
     Add( 'MOT_TOOL', VACIO );
     Add( 'TOOL', VACIO );

     Add( 'BRK_HORA', VACIO );
     Add( 'BREAKS', VACIO );
     Add( 'SUP_AREA', VACIO );
     Add( 'AREA', VACIO );
     Add( 'MODULA1', VACIO );
     Add( 'MODULA2', VACIO );
     Add( 'MODULA3', VACIO );
     Add( 'TMUERTO', VACIO );
     Add( 'TOPERA', VACIO );
     Add( 'OPERA', VACIO );
     Add( 'TPARTE', VACIO );
     Add( 'DEFSTEPS', VACIO );
     Add( 'PARTES', VACIO );

     Add( 'ACCREGLA',VACIO);
     Add( 'COLONIA',VACIO);
     Add( 'EXTRA5',VACIO);
     Add( 'EXTRA6',VACIO);
     Add( 'EXTRA7',VACIO);
     Add( 'EXTRA8',VACIO);
     Add( 'EXTRA9',VACIO);
     Add( 'EXTRA10',VACIO);
     Add( 'EXTRA11',VACIO);
     Add( 'EXTRA12',VACIO);
     Add( 'EXTRA13',VACIO);
     Add( 'EXTRA14',VACIO);
     Add( 'REGLAPREST',VACIO);
     Add( 'PRESTAXREG',VACIO);
     Add( 'AULA',VACIO);
     Add( 'POL_TIPO',VACIO);
     Add( 'CERNIVEL',VACIO);
     Add( 'PTO_CERT',VACIO);
     Add( 'CERTIFIC',VACIO);
     Add( 'TCTAMOVS',VACIO);
     Add( 'CTABANCO',VACIO);
     Add( 'PROV_CAP',VACIO);
     Add( 'RSOCIAL',VACIO);
     Add( 'OCUPA_NAC',VACIO);
     Add( 'MOT_CHECA',VACIO);
     Add( 'AR_TEM_CUR',VACIO);
     Add( 'ESTABLEC',VACIO);
     Add( 'MUNICIPIO',VACIO);
     {$ifdef MSSQL}
     finally
            CloseDBTarget;
     end;
     {$endif}
end;

{$ifdef MSSQL}
procedure TdmCompara.InitTablasPresupuestos( const lConservarConfigNomina: Boolean );
var
     lUsaDBSys : Boolean;

     procedure Add( const sNombre: String; const lConservar: Boolean= FALSE );overload;
     begin
          with TTablaPresupCat.Create( FTablas, sNombre ) do
          begin
               UsaDBSys := lUsaDBSys;
               Connect( Self );
               GetTableInfo;
               Conservar := lConservar;
          end;
     end;
begin

     lUsaDBSys := TRUE;

     InitTableList(TRUE);
     // ********************************************************************* //
     // ********* Catalogos y Tablas para Migrar en Presupuestos ************ //
     //Add( 'PUESTO' );


     Add( 'RSOCIAL' );
     Add( 'RPATRON' );
     Add( 'NUMERICA' );
     Add( 'T_ART_80' );
     Add( 'ART_80' );
     Add( 'GLOBAL' );
     Add( 'CONCEPTO', lConservarConfigNomina );
     Add( 'TURNO' );
     Add( 'CLASIFI' );
     Add( 'CONTRATO' );
     Add( 'ENTIDAD' );
     Add( 'FESTIVO' );
     Add( 'HORARIO' );
     Add( 'LEY_IMSS' );
     Add( 'MOT_BAJA' );
     Add( 'NIVEL1' );
     Add( 'NIVEL2' );
     Add( 'NIVEL3' );
     Add( 'NIVEL4' );
     Add( 'NIVEL5' );
     Add( 'NIVEL6' );
     Add( 'NIVEL7' );
     Add( 'NIVEL8' );
     Add( 'NIVEL9' );
     Add( 'NOMPARAM', lConservarConfigNomina );
     Add( 'OTRASPER' );
     Add( 'SSOCIAL' );
     Add( 'PRESTACI' );
     Add( 'TPRESTA' );
     Add( 'REGLAPREST' );
     Add( 'PRESTAXREG' );
     Add( 'PRIESGO' );

     Add( 'PUESTO' );

     Add( 'QUERYS' );
     Add( 'RIESGO' );
     Add( 'SAL_MIN' );
     Add( 'TCAMBIO' );
     Add( 'TKARDEX' );
     Add( 'TPERIODO' );
     Add( 'TALLA' );
     Add( 'TOOL' );
     Add( 'MOT_TOOL' );
     Add( 'PTOTOOLS' );
     Add( 'PTOFIJAS' );

     Add( 'PLAZA' );

     Add( 'R_CLASIFI' );
     Add( 'R_MODULO' );
     Add( 'R_ENTIDAD' );
     Add( 'R_ATRIBUTO' );
     Add( 'R_RELACION' );
     Add( 'R_CLAS_ENT' );
     Add( 'R_FILTRO' );
     Add( 'R_DEFAULT' );
     Add( 'R_ORDEN' );
     Add( 'R_MOD_ENT' );
     Add( 'R_LISTAVAL' );
     Add( 'R_VALOR' );
     Add( 'REPORTE' );
     Add( 'CAMPOREP' );

     Add( 'TAHORRO' );

end;
{$endif}

{$ifdef MSSQL}
procedure TdmCompara.InitMovimientosPresupuestos( const sFiltro: String; iAnoPresupuesto : Integer; dFechaAlta, dFechaBaja : TDateTime );
var
     lUsaDBSys : Boolean;
const
     //Q_FILTRO_COLABORA = '(CB_FEC_ING <= ''%s'') AND ((CB_FEC_BAJ >= ''%s'') or (CB_FEC_BAJ = ''12/30/1899'') )';
     Q_FILTRO_COLABORA = '(CB_ACTIVO=''S'' and CB_FEC_ING<''%s'') or (CB_ACTIVO=''N'' and CB_FEC_BAJ>= ''%s'')';

     procedure Add( const sNombre: String);overload;
     begin
          with TTablaData.Create( FTablas, sNombre ) do
          begin
               UsaDBSys := lUsaDBSys;
               Connect( Self );
               GetTableInfo;
          end;
     end;

     procedure Add( const sNombre: String; sFiltroAdicional : String); overload;
     begin
          with TTablaPresup.Create( FTablas, sNombre ) do
          begin
               UsaDBSys := lUsaDBSys;
               Connect( Self );
               Filtro := sFiltroAdicional;
               GetTableInfo;
          end;
     end;


begin
     lUsaDBSys := TRUE;

     InitTableList(TRUE);

     // ********************************************************************* //
     // ***** Historiales para la migracion de Datos hacia Presupuestos ***** //
     // ***** No es necesario migrar todas las tablas hijas de COLABORA ***** //
     
     FColabora := TTablaPresupColabora.Create( FTablas, 'COLABORA' );
     with FColabora do
     begin
          Connect( Self );
          Filtro := Format( Q_FILTRO_COLABORA, [ DateToStrSQL( dFechaAlta ), DateToStrSQL( dFechaBaja ) ] ) ;
          UsaDBSys := lUsaDBSys;
          Ignorar := 'PRETTYNAME';
          GetTableInfo;
     end;

     Add( 'ACUMULA', Format( 'AC_YEAR = %d' , [ iAnoPresupuesto -1] ) );

     Add( 'AHORRO', 'AH_STATUS = 0' );
     Add( 'ACAR_ABO', Format( 'CB_CODIGO in (SELECT A.CB_CODIGO FROM %0:s.dbo.AHORRO A '+
                                            'WHERE A.CB_CODIGO = %0:s.dbo.ACAR_ABO.CB_CODIGO AND ' +
                                            '      A.AH_TIPO = %0:s.dbo.ACAR_ABO.AH_TIPO AND '+
                                            '      A.AH_STATUS = 0 )', [FdbSourceName] ) );

     Add( 'PRESTAMO', 'PR_STATUS = 0' );
     Add( 'PCAR_ABO', Format( 'CB_CODIGO in (SELECT A.CB_CODIGO FROM %0:s.dbo.PRESTAMO A '+
                                             'WHERE A.CB_CODIGO = %0:s.dbo.PCAR_ABO.CB_CODIGO AND '+
                                             'A.PR_TIPO = %0:s.dbo.PCAR_ABO.PR_TIPO AND '+
                                             'A.PR_REFEREN = %0:s.dbo.PCAR_ABO.PR_REFEREN AND '+
                                             'A.PR_STATUS = 0 )' , [FdbSourceName] ) );

     Add( 'VACACION' );
     Add( 'KAR_TOOL' );

end;
{$endif}


procedure TdmCompara.InitMovimientos( const lNominas, lIMSS: Boolean; const sFiltro, sNivel0: String );
procedure Add( const sNombre: String );
begin
     with TTablaData.Create( FTablas, sNombre ) do
     begin
          Connect( Self );
          GetTableInfo;
     end;
end;

begin
     {
     Con este query se averiguan las tablas que tienen CB_CODIGO:

     select DISTINCT( RDB$RELATION_NAME) from RDB$RELATION_FIELDS where
     RDB$FIELD_NAME = 'CB_CODIGO' group by RDB$RELATION_NAME

     Es necesario incluirlas en el arreglo A_TABLAS_TRANSFERENCIA
     En el proceso de Transferencia de Empleados de
     D:\3Win_20\MTS\DServerCalcNomina.pas

     OJO con las dependencias: Debe ir ordenado
     primero con las tablas padre y luego las hijas
     }
     {$ifdef MSSQL}
     OpenDBTarget;
     try
     {$endif}
     InitTableList;
     FColabora := TColabora.Create( FTablas, 'COLABORA' );
     with FColabora do
     begin
          Connect( Self );
          Filtro := sFiltro;
          Nivel0 := sNivel0;
          {$ifdef MSSQL}
          Ignorar := 'PRETTYNAME';
          {$endif}
          GetTableInfo;
     end;
     Add( 'PARIENTE' );
     Add( 'IMAGEN' );
     Add( 'DOCUMENTO' );
     Add( 'CAF_COME' );
     Add( 'ACUMULA' );
     Add( 'ASIGNA' );
     Add( 'AUSENCIA' );
     Add( 'CHECADAS' );
     Add( 'AHORRO' );
     Add( 'ACAR_ABO' );
     Add( 'PRESTAMO' );
     Add( 'PCAR_ABO' );
     Add( 'ANTESPTO' );
     Add( 'ANTESCUR' );
     Add( 'INCAPACI' );
     Add( 'PERMISO' );
     Add( 'VACACION' );
     with TKardex.Create( FTablas, 'KARDEX' ) do
     begin
          Connect( Self );
          {$ifdef MSSQL}
          Ignorar := 'CB_TIP_REV;CB_FEC_SAL';
          {$endif}
          GetTableInfo;
     end;
     Add( 'KAR_FIJA' );
     Add( 'KARCURSO' );
     Add( 'AGUINAL' );
     Add( 'REP_AHO' );
     Add( 'COMPARA' );
     Add( 'DECLARA' );
     Add( 'REP_PTU' );
     Add( 'KAR_TOOL' );
     Add( 'CED_EMP' );
     Add( 'KAR_AREA' );
     Add( 'WOFIJA' );
     Add( 'WORKS' );
     if lIMSS then
     begin
          Add( 'LIQ_EMP' );
          Add( 'LIQ_MOV' );
     end;
     if lNominas then
     begin
          Add( 'NOMINA' );
          Add( 'FALTAS' );
          Add( 'MOVIMIEN' );
     end;
     Add( 'KARINF');
     Add('KAR_CERT');
     {$ifdef MSSQL}
     finally
            CloseDBTarget;
     end;
     {$endif}
end;

function TdmCompara.CompararTablas( CallBack: TShowProgress; Bitacora: TTransferLog ): Boolean;
var
   i: Integer;
   lContinue: Boolean;
begin
     Result := False;
     lContinue := True;
     { La Lista Tablas est� en orden descendente de dependencias, es decir }
     { las tablas hijas van primero que las padres }
     { Es necesario procesarla de abajo para arriba }
     with Bitacora do
     begin
          WriteTexto( StringOfChar( '*', K_ANCHO_LINEA ) );
          WriteTexto( 'Comparaci�n de Tablas' );
          with EmpresaActiva do
          begin
               WriteTexto( 'Empresa Maestra: ' + Nombre );
               WriteTexto( '          Alias: ' + Alias );
          end;
          with EmpresaFuente do
          begin
               WriteTexto( ' Empresa Fuente: ' + Nombre );
               WriteTexto( '          Alias: ' + Alias );
          end;
          WriteTexto( FormatDateTime( 'dd/mmm/yyyy hh:nn AM/PM', Now ) + ': Empezando Comparaci�n' );
          WriteTexto( StringOfChar( '*', K_ANCHO_LINEA ) );
     end;
     for i := ( TablasCount - 1 ) downto 0 do
     begin
          with Tablas[ i ] do
          begin
               Log := Bitacora;
               lContinue := CallBack( Format( 'Comparando %s', [ Descripcion ] ) );
               if lContinue then
               begin
                    if Comparar then
                       Result := True;
               end
               else
                   Break;
          end;
     end;
     with Bitacora do
     begin
          if lContinue then
          begin
               WriteTexto( StringOfChar( '*', K_ANCHO_LINEA ) );
               WriteTexto( FormatDateTime( 'dd/mmm/yyyy hh:nn AM/PM', Now ) + ': Comparaci�n Terminada' );
               WriteTexto( StringOfChar( '*', K_ANCHO_LINEA ) );
          end
          else
          begin
               WriteTexto( StringOfChar( '!', K_ANCHO_LINEA ) );
               WriteTexto( FormatDateTime( 'dd/mmm/yyyy hh:nn AM/PM', Now ) + ': Comparaci�n Interrumpida' );
               WriteTexto( StringOfChar( '!', K_ANCHO_LINEA ) );
          end;
     end;
end;

function TdmCompara.GetEmpleadoMaximo: Integer;
const
     Q_GET_MAX_NUM_EMP = 'SELECT MAX( CB_CODIGO ) MAXIMO FROM COLABORA';
begin
     try
        {$ifdef INTERBASE}
        SetTargetQuery( tqTarget, Q_GET_MAX_NUM_EMP );
        with tqTarget do
        {$endif}
        {$ifdef MSSQL}
        SetTargetQuery( qryTable, Q_GET_MAX_NUM_EMP );
        with qryTable do
        {$endif}
        begin
             Active := True;
             Result := Fields[ 0 ].AsInteger;
             Active := False;
        end;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                Result := 0;
           end;
     end;
end;

function TdmCompara.GetEmpleadoCount( const sFiltro: String ): Integer;
const
     Q_CUENTA_EMPLEADOS = 'SELECT COUNT( CB_CODIGO ) CUANTOS FROM COLABORA';
var
   sQuery: String;
begin
     sQuery := Q_CUENTA_EMPLEADOS;
     if ZetaCommonTools.StrLleno( sFiltro ) then
        sQuery := Format( '%s WHERE ( %s )', [ sQuery, sFiltro ] );
     try
        {$ifdef INTERBASE}
        ConnectQuery( dbSourceDatos, tqSource, sQuery );
        with tqSource do
        {$endif}
        {$ifdef MSSQL}
        SetODBCQuery( HdbcSource, qrySource, sQuery );
        with qrySource do
        {$endif}
        begin
             Active := True;
             Result := Fields[ 0 ].AsInteger;
             Active := False;
        end;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                Result := 0;
           end;
     end;
end;

function TdmCompara.TransferirEmpleados(CallBack: TShowProgress; Bitacora: TTransferLog; const iEmpBase: TNumEmp; const lNominas, lIMSS: Boolean; const sFiltro, sNivel0: String): Boolean;
const
     aSiNo: array[ False..True ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'NO', 'SI' );
     K_CUENTA_EMPLEADOS = 'SELECT COUNT( CB_CODIGO ) CUANTOS FROM COLABORA WHERE ( CB_CODIGO = :Empleado )';
     K_UPDATE_PERIODO = 'UPDATE PERIODO set '+
                        'PERIODO.PE_NUM_EMP = ( select COUNT(*) from NOMINA N where ( N.PE_YEAR = PERIODO.PE_YEAR ) and ( N.PE_TIPO = PERIODO.PE_TIPO ) and ( N.PE_NUMERO = PERIODO.PE_NUMERO ) ), '+
                        'PERIODO.PE_TOT_PER = ( select SUM( N.NO_PERCEPC ) from NOMINA N where ( N.PE_YEAR = PERIODO.PE_YEAR ) and ( N.PE_TIPO = PERIODO.PE_TIPO ) and ( N.PE_NUMERO = PERIODO.PE_NUMERO ) ), '+
                        'PERIODO.PE_TOT_DED = ( select SUM( N.NO_DEDUCCI ) from NOMINA N where ( N.PE_YEAR = PERIODO.PE_YEAR ) and ( N.PE_TIPO = PERIODO.PE_TIPO ) and ( N.PE_NUMERO = PERIODO.PE_NUMERO ) ), '+
                        'PERIODO.PE_TOT_NET = ( select SUM( N.NO_NETO ) from NOMINA N where ( N.PE_YEAR = PERIODO.PE_YEAR ) and ( N.PE_TIPO = PERIODO.PE_TIPO ) and ( N.PE_NUMERO = PERIODO.PE_NUMERO ) ) '+
                        'where ( PERIODO.PE_STATUS >= %d ) and ( ( select COUNT(*) from NOMINA N where ( N.PE_YEAR = PERIODO.PE_YEAR ) and ( N.PE_TIPO = PERIODO.PE_TIPO ) and ( N.PE_NUMERO = PERIODO.PE_NUMERO ) ) > 0 ) ';
     K_IMSS_MENSUAL = 'update LIQ_IMSS set '+
                      'LIQ_IMSS.LS_NUM_TRA = ( select COUNT(*) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+
                      'LIQ_IMSS.LS_DIAS_CO = ( select SUM( E.LE_DIAS_CO ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+
                      'LIQ_IMSS.LS_EYM_DIN = ( select SUM( E.LE_EYM_DIN ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+
                      'LIQ_IMSS.LS_EYM_ESP = ( select SUM( E.LE_EYM_ESP ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+
                      'LIQ_IMSS.LS_EYM_EXC = ( select SUM( E.LE_EYM_EXC ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+
                      'LIQ_IMSS.LS_EYM_FIJ = ( select SUM( E.LE_EYM_FIJ ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+
                      'LIQ_IMSS.LS_RIESGOS = ( select SUM( E.LE_RIESGOS ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+
                      'LIQ_IMSS.LS_INV_VID = ( select SUM( E.LE_INV_VID ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+
                      'LIQ_IMSS.LS_GUARDER = ( select SUM( E.LE_GUARDER ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+
                      'LIQ_IMSS.LS_IMSS_OB = ( select SUM( E.LE_IMSS_OB ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+
                      'LIQ_IMSS.LS_IMSS_PA = ( select SUM( E.LE_IMSS_PA ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+

                      // SOP 4527
                      // Aportaciones voluntarias se calcula mensualmente
                      'LIQ_IMSS.LS_APO_VOL = ( select SUM( LE_APO_VOL ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ) '+
                      // ----- -----

                      'where ( ( select COUNT(*) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ) > 0 )';
     K_IMSS_BIMESTRAL = 'update LIQ_IMSS set '+
                        'LIQ_IMSS.LS_NUM_BIM = ( select COUNT(*) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+
                        'LIQ_IMSS.LS_INF_NUM = ( select COUNT(*) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) and ( E.LE_INF_AMO > 0 ) ), '+
                        'LIQ_IMSS.LS_DIAS_BM = ( select SUM( LE_DIAS_BM ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+
                        'LIQ_IMSS.LS_RETIRO =  ( select SUM( LE_RETIRO  ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+
                        'LIQ_IMSS.LS_CES_VEJ = ( select SUM( LE_CES_VEJ ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+

                        // SOP 4527
                        // Aportaciones voluntarias es calculado mensualmente
                        // 'LIQ_IMSS.LS_APO_VOL = ( select SUM( LE_APO_VOL ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+
                        // ----- -----

                        'LIQ_IMSS.LS_INF_AMO = ( select SUM( LE_INF_AMO ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+
                        'LIQ_IMSS.LS_INF_ACR = ( select SUM( LE_INF_PAT ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) and ( E.LE_INF_AMO > 0 ) ), '+
                        'LIQ_IMSS.LS_INF_NAC = ( select SUM( LE_INF_PAT ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) and ( E.LE_INF_AMO <= 0 ) ) '+
	                'where ( LIQ_IMSS.LS_MONTH in ( 2,4,6,8,10,12 ) ) and ( ( select COUNT(*) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ) > 0 )';
     K_IMSS_TOTALES = 'update LIQ_IMSS set '+
                      'LS_SUB_IMS = LS_EYM_FIJ + LS_EYM_EXC + LS_EYM_DIN + LS_EYM_ESP + LS_INV_VID + LS_RIESGOS + LS_GUARDER, '+
                      'LS_SUB_RET = LS_RETIRO + LS_CES_VEJ, '+
                      'LS_SUB_INF = LS_INF_NAC + LS_INF_ACR + LS_INF_AMO, '+
                      'LS_TOT_IMS = LS_SUB_IMS + LS_ACT_IMS + LS_REC_IMS, '+
                      'LS_TOT_RET = LS_SUB_RET + LS_ACT_RET + LS_REC_RET + LS_APO_VOL, '+
                      'LS_TOT_INF = LS_SUB_INF + LS_ACT_INF + LS_REC_INF, '+
                      'LS_TOT_MES = LS_TOT_IMS + LS_TOT_RET + LS_TOT_INF';
var
     i: Integer;
     iEmpleado, iNewEmp, iFolio: TNumEmp;
     sValor: String;
     lOk, lContinue: Boolean;

     procedure Recalcular( const sScript, sItem: String );
     begin
          try
             {$ifdef INTERBASE}
             SetTargetQuery( tqCuenta, sScript );
             tqCuenta.ExecSQL;
             {$endif}
             {$ifdef MSSQL}
             SetTargetQuery( qryCuenta, sScript );
             qryCuenta.ExecSQL;
             {$endif}
             Bitacora.WriteTexto( Format( '%s Fueron Recalculados', [ sItem ] ) );
          except
                on Error: Exception do
                begin
                     Bitacora.HandleException( Format( 'Error Al Recalcular %s', [ sItem ] ), Error );
                end;
          end;
     end;

begin
     Result := True;
     lContinue := True;
     with Bitacora do
     begin
          WriteTexto( StringOfChar( '*', K_ANCHO_LINEA ) );
          WriteTexto( 'Transferencia De Empleados' );
          with EmpresaActiva do
          begin
               WriteTexto( '              Empresa Maestra: ' + Nombre );
               WriteTexto( '                        Alias: ' + Alias );
          end;
          with EmpresaFuente do
          begin
               WriteTexto( '               Empresa Fuente: ' + Nombre );
               WriteTexto( '                        Alias: ' + Alias );
          end;
          WriteTexto( '              # Empleado Base: ' + IntToStr( iEmpBase ) );
          WriteTexto( '           Transferir N�minas: ' + aSiNo[ lNominas ] );
          WriteTexto( 'Transferir Liquidaciones IMSS: ' + aSiNo[ lIMSS ] );
          WriteTexto( FormatDateTime( 'dd/mmm/yyyy hh:nn AM/PM', Now ) + ': Empezando Transferencia' );
          WriteTexto( StringOfChar( '*', K_ANCHO_LINEA ) );
     end;
     try
        InitMovimientos( lNominas, lIMSS, sFiltro, sNivel0 );
        lOk := True;
     except
           on Error: Exception do
           begin
                Bitacora.HandleException( Format( 'Error Al Configurar Tablas Por Transferir', [ sValor ] ), Error );
                lOk := False;
           end;
     end;
     if lOk then
     begin
          sValor := VACIO;
          try
             for i := 0 to ( TablasCount - 1 ) do
             begin
                  with Tablas[ i ] do
                  begin
                       Log := Bitacora;
                       sValor := Nombre;
                       TransferPrepare;
                  end;
             end;
          except
                on Error: Exception do
                begin
                     Bitacora.HandleException( Format( 'Error Al Preparar Transferencia De La Tabla %s', [ sValor ] ), Error );
                     lOk := False;
                end;
          end;
     end;
     if lOk then
     begin
          {$ifdef INTERBASE}
          SetTargetQuery( tqCuenta, K_CUENTA_EMPLEADOS );
          {$endif}
          {$ifdef MSSQL}
          SetTargetQuery( qryCuenta, K_CUENTA_EMPLEADOS );
          {$endif}
          iFolio := iEmpBase;
          with FColabora.QueryRead do
          begin
               Active := True;
               while not Eof and lContinue do
               begin
                    iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                    {$ifdef INTERBASE}
                    with tqCuenta do
                    {$endif}
                    {$ifdef MSSQL}
                    with qryCuenta do
                    {$endif}
                    begin
                         Active := False;
                         ParamByName( 'Empleado' ).AsInteger := iEmpleado;
                         Active := True;
                         if ( Fields[ 0 ].AsInteger > 0 ) then
                         begin
                              iNewEmp := iFolio;
                              Inc( iFolio );
                         end
                         else
                             iNewEmp := iEmpleado;
                         Active := False;
                    end;
                    lContinue := CallBack( Format( 'Transfiriendo %.0n Como %.0n', [ iEmpleado / 1, iNewEmp / 1 ] ) );
                    if lContinue then
                    begin
                         sValor := VACIO;
                         TxStart;
                         try
                            for i := 0 to ( TablasCount - 1 ) do
                            begin
                                 with Tablas[ i ] do
                                 begin
                                      sValor := Nombre;
                                      TransferEmployee( iEmpleado, iNewEmp );
                                 end;
                            end;
                            TxCommit;
                            with Bitacora do
                            begin
                                 WriteTexto( Format( 'Empleado %.0n Transferido Como %.0n', [ iEmpleado / 1, iNewEmp / 1 ] ) );
                            end;
                         except
                               on Error: Exception do
                               begin
                                    TxRollBack;
                                    Bitacora.HandleException( Format( 'Error Al Transferir %s Del Empleado %.0n', [ sValor, iEmpleado / 1] ), Error );
                               end;
                         end;
                         Next;
                    end;
               end;
               Active := False;
          end;
     end;
     if lContinue then
     begin
          Recalcular( Format( K_UPDATE_PERIODO, [ Ord( spCalculadaTotal ) ] ), 'Totales De Per�odos De N�mina' );
          Recalcular( K_IMSS_MENSUAL, 'Totales Mensuales Del IMSS' );
          Recalcular( K_IMSS_BIMESTRAL, 'Totales Bimestrales Del IMSS' );
          Recalcular( K_IMSS_TOTALES, 'Totales Del IMSS' );
     end;
     with Bitacora do
     begin
          if lContinue then
          begin
               WriteTexto( StringOfChar( '*', K_ANCHO_LINEA ) );
               WriteTexto( FormatDateTime( 'dd/mmm/yyyy hh:nn AM/PM', Now ) + ': Transferencia Terminada' );
               WriteTexto( StringOfChar( '*', K_ANCHO_LINEA ) );
          end
          else
          begin
               WriteTexto( StringOfChar( '!', K_ANCHO_LINEA ) );
               WriteTexto( FormatDateTime( 'dd/mmm/yyyy hh:nn AM/PM', Now ) + ': Transferencia Interrumpida' );
               WriteTexto( StringOfChar( '!', K_ANCHO_LINEA ) );
          end;
     end;
end;

{$ifdef MSSQL}
procedure TdmCompara.HdbcBeforeConnect(Sender: TObject);
const
     SQL_PRESERVE_CURSORS = 1204;
     SQL_PC_ON = 1;
begin
     SQLSetConnectAttr(THdbc( Sender ).Handle, SQL_PRESERVE_CURSORS, Pointer(SQL_PC_ON), 0);
end;


// (JB) PreparacionPresupuestos
function TdmCompara.PreparacionPresupuestos(CallBack: TShowProgress; Bitacora: TTransferLog; iYear : Integer; dFechaEmpAlta : TDateTime ; dFechaEmpBaja : TDateTime; lConservarConfNomina,lNominaMensual: Boolean ): Boolean;
const
     Q_PRESUP_MIGRA_CATALOGOS_DELETE = 'EXECUTE SPP_MIGRACIONPRESUPUESTO_DELETE ''%s''';
     Q_PRESUP_MIGRA_KARFIJA = 'EXECUTE SP_MIGRACIONPRESUPUESTOTABLA ''%s'', ''%s'', ''%s''  ';
     Q_PRESUP_MIGRA_OPTIMIZA = 'EXECUTE SPP_MigracionOptimiza ''%s'' ';
     Q_PRESUP_BITACORA = 'SELECT BM_FEC_EVE, BM_DESCRIP, BM_DETALLE FROM BIT_MIG ORDER BY BM_FEC_EVE';
     Q_PLAZA_IDENTITY = 'set identity_insert PLAZA  %s';
var
     i, iCount: Integer;
     iEmpleado : TNumEmp;
     sServer, sDatabase, sValor, sDatabaseDestino: String;
     lOk, lContinue,lEstaVacia: Boolean;

begin
     //Inicializacion de Variables
     Result := True;
     //Mensaje Inicial de Bitacora
     with dmDBConfig do
     begin

          ConectarODBC( HdbcTarget, tqCompanyCM_DATOS.AsString, tqCompanyCM_USRNAME.AsString,  Decrypt( tqCompanyCM_PASSWRD.AsString ) );

          with Bitacora do
          begin
               WriteTexto( StringOfChar( '*', K_ANCHO_LINEA ) );
               WriteTexto( '         Preparaci�n de Empresa de Prespuestos : ' + tqCompanyCM_CODIGO.AsString + ' ' + tqCompanyCM_NOMBRE.AsString );
               WriteTexto( '                          Base de Datos Origen : ' + EmpresaFuente.Codigo  + ' ' + EmpresaFuente.Nombre );
               WriteTexto( '                        Directorio de Bitacora : ' + Bitacora.FileName );
               WriteTexto( VACIO );
               WriteTexto( '                            A�o a Presupuestar : ' + IntToStr( iYear ) );
               WriteTexto( '                       Empleados Activos desde : ' + FechaAsStr( dFechaEmpAlta ) );
               WriteTexto( '                          Empleados Baja desde : ' + FechaAsStr( dFechaEmpBaja ) );
               WriteTexto( VACIO );
               WriteTexto( FormatDateTime( 'dd/mmm/yyyy hh:nn AM/PM', Now ) + ': Preparaci�n de Empresa de Prespuestos Iniciada' );
               WriteTexto( StringOfChar( '*', K_ANCHO_LINEA ) );
          end;

          with dmDBConfig do
               OpenAlias( dbTarget, tqCompanyCM_ALIAS.AsString, tqCompanyCM_USRNAME.AsString,  Decrypt( tqCompanyCM_PASSWRD.AsString ) );

          with dbTarget do
          begin
               tqTarget.DatabaseName := dbTarget.Name;
               Bitacora.WriteTexto( 'Iniciando Depuraci�n de Cat�logos' );

               tqTarget.Sql.Clear;
               tqTarget.Sql.Add( Format( Q_PRESUP_MIGRA_CATALOGOS_DELETE, [zBoolToStr( lConservarConfNomina )] ) );
               StartTransaction;
               try
                  tqTarget.ExecSQL;
                  Commit;

                  Bitacora.WriteTexto( 'Finaliza Depuraci�n de Cat�logos' );
                  lOk := True;
                  lContinue := True;

               except
                     on Error: Exception do
                     begin
                          Rollback;
                          lOk := False;
                          lContinue := False;
                          Bitacora.HandleException( 'Error Al Depurar Cat�logos', Error );
                     End;

               end;
          end;

          if lContinue then
          begin
               with dbTarget do
               begin
                    lContinue := CallBack( 'Transfiriendo Cat�logo KAR_FIJA' );
                    tqTarget.DatabaseName := dbTarget.Name;
                    tqTarget.Sql.Clear;
                    ZetaServerTools.GetServerDatabase( EmpresaFuente.Datos, sServer, sDatabase );
                    ZetaServerTools.GetServerDatabase( tqCompanyCM_DATOS.AsString, sServer, sDatabaseDestino );
                    tqTarget.Sql.Add( Format ( Q_PRESUP_MIGRA_KARFIJA, [ 'KAR_FIJA',sDatabase,sDatabaseDestino ] ) );
                    tqTarget.ExecSQL;
                    tqTarget.Sql.Clear;
               end;
          end;

          if lContinue then
          begin
               with dbTarget do
               begin
                    tqTarget.Sql.Clear;
                    tqTarget.Sql.Add( Format ( Q_PLAZA_IDENTITY, [ 'ON' ] ) );
                    tqTarget.ExecSQL;
                    tqTarget.Sql.Clear;
               end;
          end;


          if lContinue and CallBack( 'Transfiriendo Cat�logos' ) then
          begin
               InitTablasPresupuestos(lConservarConfNomina);
               for i := 0 to ( TablasCount - 1 ) do
               begin

                    TxStart;
                    try

                       with TTablaPresupCat( Tablas[ i ] ) do
                       begin
                            iCount := 0;
                            Log := Bitacora;
                            sValor := Nombre;

                            CallBack( Format( 'Transfiriendo Cat�logo %s', [sValor] ) );
                            TransferPrepare;


                            with FQRead do
                            begin
                                 Active := TRUE;
                                 while NOT EOF do
                                 begin
                                      if not Conservar then
                                      begin
                                           TransferFieldValues;
                                      end;
                                      Next;
                                      inc(iCount);
                                      if ((iCount mod 100 ) = 0 )then
                                         CallBack( Format( 'Transfiriendo Cat�logo %s - %d registros', [sValor, iCount] ) );
                                 end;
                                 Active := FALSE;
                            end;

                            with FQReadNulls do
                            begin
                                 Active := TRUE;
                                 while NOT EOF do
                                 begin
                                      if not Conservar then
                                      begin
                                           TransferFieldValuesNulls;
                                      end;
                                      Next;
                                      inc(iCount);
                                      if ((iCount mod 100 ) = 0 )then
                                         CallBack( Format( 'Transfiriendo Cat�logo %s - %d registros', [sValor, iCount] ) );
                                 end;
                                 Active := FALSE;
                            end;

                       end;
                       TxCommit;
                       with Bitacora do
                       begin
                            WriteTexto( Format( 'Cat�logo %s Transferido', [ sValor ] ) );
                       end;
                    except
                          on Error: Exception do
                          begin
                               TxRollBack;
                               Bitacora.HandleException( Format( 'Error Al Transferir el Cat�logo %s', [ sValor ] ), Error );
                          end;
                    end;
               end;
          end;
     end;

     if lContinue then
     begin
          //Agregar la lectura de BIT MIG
          with dmDBConfig do
                OpenAlias( dbTarget, tqCompanyCM_ALIAS.AsString, tqCompanyCM_USRNAME.AsString,  Decrypt( tqCompanyCM_PASSWRD.AsString ) );
           with dbTarget do
           begin
                try
                   tqTarget.DatabaseName := dbTarget.Name;
                   tqTarget.Sql.Clear;
                   tqTarget.Sql.Add( Q_PRESUP_BITACORA );
                   tqTarget.Open;
                   with tqTarget do
                   begin
                        First;
                        while not EOF do
                        begin
                             Bitacora.WriteTexto( FieldByName('BM_DETALLE').AsString );
                             Next;
                        end;
                   end;
                   lOk := true;
                except
                      on Error: Exception do
                      begin
                           lOk := False;
                           lContinue := False;
                           Bitacora.HandleException( 'Error Al Leer Bit�cora', Error );
                      End;
                end;
           end;
      end;


     if lOk and lContinue then
     begin
          Bitacora.WriteTexto( 'Inicia la Transferencia de Empleados.' );
          try
             InitMovimientosPresupuestos( Vacio, iYear, dFechaEmpAlta, dFechaEmpBaja );
             CloseDBTarget;
             lOk := True;
          except
                on Error: Exception do
                begin
                     Bitacora.HandleException( Format( 'Error Al Configurar Tablas Por Transferir a Presupuestos', [ sValor ] ), Error );
                     lOk := False;
                     lContinue := False;
                end;
          end;
     end;

     if lOk then
     begin
          sValor := VACIO;
          try
             for i := 0 to ( TablasCount - 1 ) do
             begin
                  with Tablas[ i ] do
                  begin
                       Log := Bitacora;
                       sValor := Nombre;
                       TransferPrepare;
                  end;
             end;
          except
                on Error: Exception do
                begin
                     Bitacora.HandleException( Format( 'Error Al Preparar Transferencia De La Tabla %s', [ sValor ] ), Error );
                     lOk := False;
                     lContinue := False;
                end;
          end;
     end;
     if lOk then
     begin
         { with TTablaPresupColabora( FColabora).QReadNulls do
          begin
               //FColabora.QueryRead.SQL.LoadFromfile('D:\My Documents\algo.txt');
               Active := True;
               while not Eof and lContinue do
               begin
                    iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;

                    lContinue := CallBack( Format( 'Transfiriendo Empleado %d', [ iEmpleado ] ) );
                    if lContinue then
                    begin
                         sValor := VACIO;
                         TxStart;
                         try
                            for i := 0 to ( TablasCount - 1 ) do
                            begin
                                 with Tablas[ i ] do
                                 begin
                                      sValor := Nombre;
                                      TransferEmployee( iEmpleado, iEmpleado );
                                 end;
                            end;
                            TxCommit;
                            with Bitacora do
                            begin
                                 WriteTexto( Format( 'Empleado %d Transferido', [ iEmpleado ] ) );
                            end;
                         except
                               on Error: Exception do
                               begin
                                    TxRollBack;
                                    Bitacora.HandleException( Format( 'Error Al Transferir %s Del Empleado %.0n', [ sValor, iEmpleado / 1] ), Error );
                               end;
                         end;
                    end;
                    Next;
               end;
               Active := False;
          end;  }


          with FColabora.QueryRead do
          begin
               //FColabora.QueryRead.SQL.LoadFromfile('D:\My Documents\algo.txt');
               Active := True;
               while not Eof and lContinue do
               begin
                    iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                    
                    lContinue := CallBack( Format( 'Transfiriendo Empleado %d', [ iEmpleado ] ) );
                    if lContinue then
                    begin
                         sValor := VACIO;
                         TxStart;
                         try
                            for i := 0 to ( TablasCount - 1 ) do
                            begin
                                 with Tablas[ i ] do
                                 begin
                                      sValor := Nombre;
                                      TransferEmployee( iEmpleado, iEmpleado );
                                 end;
                            end;
                            TxCommit;
                            with Bitacora do
                            begin
                                 WriteTexto( Format( 'Empleado %d Transferido', [ iEmpleado ] ) );
                            end;
                         except
                               on Error: Exception do
                               begin
                                    TxRollBack;
                                    Bitacora.HandleException( Format( 'Error Al Transferir %s Del Empleado %.0n', [ sValor, iEmpleado / 1] ), Error );
                               end;
                         end;
                    end;
                    Next;
               end;
               Active := False;
          end;
     end;

     if lContinue then
     begin
          with dmDBConfig do
               OpenAlias( dbTarget, tqCompanyCM_ALIAS.AsString, tqCompanyCM_USRNAME.AsString,  Decrypt( tqCompanyCM_PASSWRD.AsString ) );
          with dbTarget do
          begin
               StartTransaction;
               try
                  tqTarget.DatabaseName := dbTarget.Name;
                  tqTarget.Sql.Clear;
                  Bitacora.WriteTexto( 'Inicia Optimizaci�n en Empresa de Presupuestos.' );
                  tqTarget.Sql.Add( Format( Q_PRESUP_MIGRA_OPTIMIZA ,[zBoolToStr(lNominaMensual)]));
                  tqTarget.ExecSQL;
                  Commit;
                  Bitacora.WriteTexto( 'Finaliza Optimizaci�n en Empresa de Presupuestos.' );
               except
                     on Error: Exception do
                     begin
                          Rollback;
                          Bitacora.HandleException( 'Error en Optimizaci�n en Empresa de Presupuestos' , Error );
                          lContinue := False;
                     end;
               end;
          end;
     end;
     if lContinue then
     begin
          with dbTarget do
          begin
               tqTarget.Sql.Clear;
               tqTarget.Sql.Add( Format ( Q_PLAZA_IDENTITY, [ 'OFF' ] ) );
               tqTarget.ExecSQL;
               tqTarget.Sql.Clear;
          end;
     end;
     with Bitacora do
     begin
          if lContinue then
          begin
               WriteTexto( StringOfChar( '*', K_ANCHO_LINEA ) );
               WriteTexto( FormatDateTime( 'dd/mmm/yyyy hh:nn AM/PM', Now ) + ': Preparaci�n de Empresa de Prespuestos Terminada' );
               WriteTexto( StringOfChar( '*', K_ANCHO_LINEA ) );
          end
          else
          begin
               WriteTexto( StringOfChar( '!', K_ANCHO_LINEA ) );
               WriteTexto( FormatDateTime( 'dd/mmm/yyyy hh:nn AM/PM', Now ) + ': Preparaci�n de Empresa de Prespuestos Interrumpida' );
               WriteTexto( StringOfChar( '!', K_ANCHO_LINEA ) );
          end;
     end;
end;
{$endif}
end.
