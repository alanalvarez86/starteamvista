unit FEditarDiccionario;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, Db, DBTables, ComCtrls, ExtCtrls,
     ZetaTipoEntidad,
     FInfoDiccionario;

type
  TEditarDiccionario = class(TForm)
    BaseDeDatos: TGroupBox;
    AliasLBL: TLabel;
    UserNameLBL: TLabel;
    PasswordLBL: TLabel;
    Alias: TComboBox;
    Usuario: TEdit;
    Password: TEdit;
    Conectar: TBitBtn;
    Desconectar: TBitBtn;
    PagePrincipal: TPageControl;
    TabEntidades: TTabSheet;
    TabClasificaciones: TTabSheet;
    ListaEntidades: TListBox;
    ListaClasificacion: TListBox;
    PanelClasificaciones: TPanel;
    Editar: TSpeedButton;
    PanelEntidades: TPanel;
    sbBorra: TSpeedButton;
    EditarEntidad: TSpeedButton;
    cbEntidades: TCheckBox;
    SpeedButton1: TSpeedButton;
    Memo1: TMemo;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ConectarClick(Sender: TObject);
    procedure DesconectarClick(Sender: TObject);
    procedure ListaClasificacionDblClick(Sender: TObject);
    procedure ListaEntidadesDblClick(Sender: TObject);
    procedure sbBorraClick(Sender: TObject);
    procedure ListaEntidadesClick(Sender: TObject);
    procedure ListaClasificacionClick(Sender: TObject);
    procedure cbEntidadesClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
     { Private declarations }
    function GetEntidad: TipoEntidad;
    function GetEntidadNombre(const eEntidad: TipoEntidad): String;
    procedure MuestraError(Sender: TObject; Error: Exception);
    procedure LlenaClasificaciones;
    procedure LlenaEntidades( Lista: TStrings );
    procedure GenerarListasFijas(Lista: TStrings);
  public
    { Public declarations }
  end;

var
  EditarDiccionario: TEditarDiccionario;

implementation

uses ZetaDialogo,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaTipoEntidadTools,
     FEditaTabla,
     FEditarEntidades,
     DEditarDiccionario;

{$R *.DFM}

procedure TEditarDiccionario.FormCreate(Sender: TObject);
begin
     Application.OnException := MuestraError;
     {$ifdef TRESS}
     Caption := 'Editar Diccionario de TRESS';
     {$endif}
     {$ifdef SELECCION}
     Caption := 'Editar Diccionario de SELECCION DE PERSONAL';
     {$endif}
     dmDiccionario := TdmDiccionario.Create( Self );
end;

procedure TEditarDiccionario.FormShow(Sender: TObject);
const
     K_INDEX = 1;
begin
     with Alias do
     begin
          dmDiccionario.LeerAlias( Items );
          if ( Items.Count > 0 ) then
             ItemIndex := 0;
     end;
     with PagePrincipal do
     begin
          Visible := True;
          ActivePage := TabEntidades;
          {$ifdef SELECCION}
          Pages[ K_INDEX ].TabVisible := False
          {$endif}
     end;
end;

procedure TEditarDiccionario.FormDestroy(Sender: TObject);
begin
     FreeAndNil( dmDiccionario );
end;

procedure TEditarDiccionario.MuestraError( Sender: TObject; Error: Exception );
begin
     ZetaDialogo.zExcepcion( '� Error En ' + Caption + ' !', '� Se Encontr� Un Error !', Error, 0 );
end;

function TEditarDiccionario.GetEntidad: TipoEntidad;
begin
     //Result := TipoEntidad( ListaEntidades.ItemIndex );
     with ListaEntidades do
          Result :=TipoEntidad( Items.Objects[ItemIndex] );
end;

function TEditarDiccionario.GetEntidadNombre( const eEntidad: TipoEntidad ): String;
begin
     Result := ZetaTipoEntidadTools.ObtieneEntidad( eEntidad );
end;

procedure TEditarDiccionario.LlenaEntidades( Lista: TStrings );
var
   eEntidad: TipoEntidad;
   sEntidad: String;
   oLista: TstringList;
begin
     with Lista do
     begin
          BeginUpdate;
          try
             Clear;

             oLista:= TstringList.Create;
             try

                for eEntidad := Low( TipoEntidad )  to High( TipoEntidad )  do
                begin
                     sEntidad := ZetaTipoEntidadTools.ObtieneEntidad( eEntidad );
                     if ZetaCommonTools.StrLleno( sEntidad  ) then
                     begin
                          //Add( PadL( IntToStr( Ord( eEntidad ) ), 3 )+ ' = ' + sEntidad );
                          oLista.AddObject( PadR( sEntidad, 40 )+'='+ PadL( IntToStr( Ord( eEntidad ) ), 3 ), TObject(Ord(eEntidad)) );
                          //Add( sEntidad );
                     end;
                end;

                oLista.Sorted := cbEntidades.Checked;
                Lista.AddStrings( oLista );
             finally
                    FreeAndNil( oLista );
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TEditarDiccionario.LlenaClasificaciones;
var
   eClase: eClasifiReporte;
begin
     with ListaClasificacion.Items do
     begin
          BeginUpdate;
          try
             Clear;
             for eClase := Low( eClasifiReporte ) to High( eClasifiReporte ) do
             begin
                  Add( ZetaCommonLists.ObtieneElemento( lfClasifiReporte, Ord( eClase ) ) );
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TEditarDiccionario.ConectarClick(Sender: TObject);
begin
     if dmDiccionario.Conectar( Alias.Items.Strings[ Alias.ItemIndex ], Usuario.Text, Password.Text ) then
     begin
          Conectar.Enabled := False;
          Alias.Enabled := False;
          Usuario.Enabled := False;
          Password.Enabled := False;
          Desconectar.Enabled := True;
          PagePrincipal.Visible := True;          
          LlenaEntidades( ListaEntidades.Items );
          LlenaClasificaciones;
     end;
end;

procedure TEditarDiccionario.DesconectarClick(Sender: TObject);
begin
     dmDiccionario.Desconectar;
     Conectar.Enabled := True;
     Desconectar.Enabled := False;
     Alias.Enabled := True;
     Usuario.Enabled := True;
     Password.Enabled := True;
     PagePrincipal.Visible := False;
     sbBorra.Enabled := False;
     EditarEntidad.Enabled := False;
     Editar.Enabled := False;
end;

procedure TEditarDiccionario.ListaEntidadesClick(Sender: TObject);
begin
     if not sbBorra.Enabled or not EditarEntidad.Enabled or ( ListaEntidades.ItemIndex < 0 ) then
     begin
          sbBorra.Enabled := True;
          EditarEntidad.Enabled := True;
     end;
end;

procedure TEditarDiccionario.ListaEntidadesDblClick(Sender: TObject);
begin
     //MA: Para poder obtener la entidad a la que se apunta en cualquier parte del programa
     dmDiccionario.EditaEntidad( GetEntidad );
     if not sbBorra.Enabled then
        sbBorra.Enabled := True;
end;

procedure TEditarDiccionario.sbBorraClick(Sender: TObject);
var
   eEntidad: TipoEntidad;
begin
     eEntidad := GetEntidad;
     if ZetaDialogo.ZConfirm( 'Borrar Entidad', Format( '� Est�s Seguro De Borrar La Entidad "%s" y Todos Sus Campos Del Diccionario ?', [ GetEntidadNombre( eEntidad ) ] ), 0, mbOK ) then
     begin
          dmDiccionario.BorrarEntidad( eEntidad );
          sbBorra.Enabled := False;
     end;
end;

procedure TEditarDiccionario.ListaClasificacionDblClick(Sender: TObject);
var
   eClase: eClasifiReporte;
begin
     eClase := eClasifiReporte( ListaClasificacion.ItemIndex );
     EditarEntidades := TEditarEntidades.Create( Self );
     try
        Editar.Enabled := True;
        with EditarEntidades do
        begin
             Clasificacion := eClase;
             ShowModal;
        end;
     finally
            FreeAndNil( EditarEntidades );
     end;
end;

procedure TEditarDiccionario.ListaClasificacionClick(Sender: TObject);
begin
     if not Editar.Enabled and ( ListaClasificacion.ItemIndex >= 0 ) then
     begin
          Editar.Enabled := True;
     end;
end;

procedure TEditarDiccionario.cbEntidadesClick(Sender: TObject);
begin
     LlenaEntidades( ListaEntidades.Items );
end;

procedure TEditarDiccionario.SpeedButton1Click(Sender: TObject);
 var
    Lista : TStrings;
begin
     Lista := TStringList.Create;
     try
        GenerarListasFijas(Lista);
        Lista.SaveToFile( VerificaDir( ExtractFilePath( Application.ExeName ) ) + 'TFijas.SQL' );
     finally
            FreeAndNil(Lista);
     end;
end;

procedure TEditarDiccionario.GenerarListasFijas(Lista: TStrings);
const
     K_INSERT = 'insert into TFIJAS( TF_TABLA, TF_CODIGO, TF_DESCRIP ) values ( %d, %d, ''%s'' );';
var
   eLista: ListasFijas;
   FElementos: TStrings;
   i: Integer;
begin

     FElementos := TStringList.Create;

     try
        with Lista do
        begin
             Clear;
             BeginUpdate;
             try
                Add( 'delete from TFIJAS' );
                for eLista := Low( ListasFijas ) to High( ListasFijas ) do
                begin
                     ZetaCommonLists.LlenaLista( eLista, FElementos );
                     for i := 0 to ( FElementos.Count - 1 ) do
                     begin
                          Add( Format( K_INSERT, [ Ord( eLista ), i + ZetaCommonLists.GetOffSet( eLista ), FElementos[ i ] ] ) );
                     end;
                end;
             finally
                    EndUpdate;
             end;
        end;
     finally
            FreeAndNil( FElementos );
     end;
end;


procedure TEditarDiccionario.Button1Click(Sender: TObject);
begin
      memo1.Lines.Text := dmDiccionario.SQLInfoTabla(GetEntidad);
end;

end.
