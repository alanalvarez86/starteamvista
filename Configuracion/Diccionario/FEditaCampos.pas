unit FEditaCampos;

interface

uses
  Windows, Messages,  TypInfo, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, ComCtrls, ToolWin, ZetaKeyCombo,
  ZetaDBTextBox, Mask, ZetaNumero,
  FInfoDiccionario,
  ZetaCommonLists;

type

  TEditaCampos = class(TForm)
    Panel2: TPanel;
    OK: TBitBtn;
    Cancelar: TBitBtn;
    cbConfidencial: TCheckBox;
    eAncho: TZetaNumero;
    cbEntidades: TComboBox;
    cbListaFija: TZetaKeyCombo;
    eMascara: TEdit;
    cbTipoCampo: TZetaKeyCombo;
    eTCorto: TEdit;
    eClaves: TEdit;
    lbCampo: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    lbTexto: TLabel;
    Label5: TLabel;
    Label10: TLabel;
    eTitulo: TComboBox;
    eNombre: TEdit;
    cbTipoFiltro: TComboBox;
    Button1: TButton;
    Button2: TButton;
    Memo1: TMemo;
    Button3: TButton;
    procedure OKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cbTipoCampoChange(Sender: TObject);
    procedure eTituloChange(Sender: TObject);
    procedure eNombreChange(Sender: TObject);
    procedure cbTipoFiltroChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
    FInfoCampo: TInfoCampo;
    FNuevo: Boolean;
    procedure LlenaComboEntidades( Lista: TStrings );
    procedure LlenaComboListaFija( Lista: TStrings );
    procedure Controles;
    procedure ControlesTipoFiltro;
    procedure CamposPreDefinidos;
    function GetTipoFiltro : eTipoFiltro;
    function GetTipoGlobal: eTipoGlobal;
    function GetRango : eTipoRango;
    function GetLookup : integer;
    function GetRangoActivo : eTipoRangoActivo;
  public
    { Public declarations }
    property InfoCampo : TInfoCampo Read FInfoCampo Write FInfoCampo;
    property Nuevo: Boolean read FNuevo write FNuevo;
  end;

var
  EditaCampos: TEditaCampos;

implementation

uses
    ZetaCommonTools,
    ZetaTipoEntidad,
    ZetaTipoEntidadTools,
    ZetaCommonClasses;

{$R *.DFM}

var aListasFijas : array[ListasFijas] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = (
                    'lfNinguna',
                    'lfNivelUsuario',
                    'lfTipoBitacora',
                    'lfSexoDesc',
                    'lfCandado',
                    'lfMotivoBaja',
                    'lfTipoOtrasPer',
                    'lfIMSSOtrasPer',
                    'lfISPTOtrasPer',
                    'lfIncidencias',
                    'lfIncidenciaIncapacidad',
                    'lfRPatronModulo',
                    'lfTipoTurno',
                    'lfTipoJornada',
                    'lfTipoPeriodo',
                    'lfUsoPeriodo',
                    'lfStatusPeriodo',
                    'lfTipoNomina',
                    'lfTipoFestivo',
                    'lfTipoHorario',
                    'lfTipoReporte',
                    'lfTipoRangoActivo',
                    'lfOperacionConflicto',
                    'lfStatusLectura',
                    'lfClasifiReporte',
                    'lfTipoCampo',
                    'lfTipoOpera',
                    'lfTipoOperacionCampo',
                    'lfWorkStatus',
                    'lfTipoLectura ',
                    'lfTipoCedula',
                    'lfRangoFechas',
                    'lfJustificacion',
                    'lfIncluyeEvento',
                    'lfTipoGlobal',
                    'lfTipoConcepto',
                    'lfMotivoFaltaDias',
                    'lfMotivoFaltaHoras',
                    'lfStatusAhorro',
                    'lfStatusPrestamo',
                    'lfStatusAusencia',
                    'lfTipoDiaAusencia',
                    'lfTipoChecadas',
                    'lfDescripcionChecadas',
                    'lfTipoPariente',
                    'lfTipoInfonavit',
                    'lfTipoVacacion',
                    'lfStatusKardex',
                    'lfMotivoIncapacidad',
                    'lfFinIncapacidad',
                    'lfTipoPermiso',
                    'lfTipoLiqIMSS',
                    'lfCampoCalendario',
                    'lfTipoLiqMov',
                    'lfCafeCalendario',
                    'lfTipoAhorro',
                    'lfHorasExtras',
                    'lfMotTools',
                    'lfBancaElectronica',
                    'lfYears',
                    'lfDescuentoTools',
                    'lfMeses',
                    'lfZonaGeografica',
                    'lfSexo',
                    'lfCodigoLiqMov',
                    'lfDiasSemana',
                    'lfMes13',
                    'lfTipoRango',
                    'lfLiqNomina',
                    'lfAutorizaChecadas',
                    'lfAltaAhorroPrestamo',
                    'lfStatusEmpleado',
                    'lfTipoFormato',
                    'lfExtFormato',
                    'lfFormatoFormas',
                    'lfFormatoASCII',
                    'lfEmailfrecuencia',
                    'lfEmailformato',
                    'lfEmailNotificacion',
                    'lfTipoCompanyName',
                    'lfTipoCalculo',
                    'lfCamposConteo',
                    'lfHorasDias',
                    'lfImportacion',
                    'lfOperacionMontos ',
                    'lfTipoBanda',
                    'lfPuertoSerial',
                    'lfTipoGlobalAuto',
                    'lfClaseBitacora',
                    'lfProcesos',
                    'lfRegla3x3',
                    'lfTipoCompany',
                    'lfPrioridad',
                    'lfReqStatus',
                    'lfMotivoVacante',
                    'lfEstudios',
                    'lfSolStatus',
                    'lfEdoCivil',
                    'lfEdoCivilDesc',
                    'lfCanStatus',
                    'lfEntStatus',
                    'lfTipoExamen',
                    'lfTipoConsulta',
                    'lfTipoEstudio',
                    'lfTipoAccidente',
                    'lfMotivoAcc',
                    'lfTipoMedicina',
                    'lfTipoDiagnost',
                    'lfTipoLesion',
                    'lfAxAtendio',
                    'lfAxTipo',
                    'lfExCampoTipo',
                    'lfExMostrar',
                    'lfExTipo',
                    'lfEmTermino',
                    'lfTurnoLabor',
                    'lfTipoArea',
                    'lfTipoKarFecha',
                    'lfCancelaMod',
                    'lfInicioAmortizacion',
                    'lfClaseAcciones',
                    'lfTipoCompete',
                    'lfStatusAcciones',
                    'lfStatusCursos',
                    'lfTipoCedInspeccion',
                    'lfResultadoCedInspeccion',
                    'lfValorDiasVacaciones',
                    'lfFiltroEmpOrganigrama',
                    'lfTipoPuesto',
                    'lfEmailType',
                    'lfUpdateKind',
                    'lfOperacionIncremento',
                    'lfFormatoImpFecha',
                    'lfOut2Eat',
                    'lfEmpleadoRepetido',
                    'lfTipoCorregirFechas',
                    'lfTipoNavegador',
                    'lfOrientacionNavegador',
                    'lfPosicionNavegador',
                    'lfCompressionLevel',
                    'lfEncryptionLevel',
                    'lfFontEncoding',
                    'lfPageLayout',
                    'lfPageMode',
                    'lfTransitionEffect',
                    'lfAnfitrionStatus',
                    'lfVisitanteStatus',
                    'lfCasetaStatus',
                    'lfCitaStatus',
                    'lfCatCompanys',
                    'lfLibroStatus',
                    'lfTipoGafete',
                    'lfCorteStatus',
                    'lfAccesoRegla',
                    'lfTipoKind',
                    'lfBusquedaLibro',
                    'lfImpresionGafete',
                    'lfTipoProximidad',
                    {$ifdef COMPARTE_MGR}
                    'lfWFStatusModelo',
                    'lfWFNotificacion',
                    'lfWFProcesoStatus',
                    'lfWFPasoProcesoStatus',
                    'lfWFTareaStatus',
                    'lfWFCorreoStatus',
                    {$endif}
                    {$ifdef ADUANAS}
                    'lfTipoCliente',
                    'lfTipoMaterial',
                    'lfTipoMedida',
                    'lfStatusFactura',
                    'lfFacTipoProceso',
                    'lfTipoActivo',
                    'lfStatusPedimento',
                    'lfProgramasTipoProceso',
                    'lfTipoFraccionAutorizada',
                    'lfDescargaAlmacen',
                    'lfDescargaTipo',
                    {$endif}
                    'lfStatusEncuesta',
                    'lfComentariosMGR',
                    'lfEstiloRespuesta',
                    'lfTipoEvaluaEnc',
                    'lfStatusEvaluaciones',
                    'lfStatusSujeto',
                    'lfStatusInscrito',
                    'lfStatusSesiones',
                    'lfTipoReserva',
                    'lfStatusEvaluacion',
                    'lfEVCorreoStatus',
                    'lfTipoSupuestoRH',
                    'lfTipoPeriodoMes',
                    {Lista para Kiosco}
                    'lfKJustificacion',
                    'lfAccionBoton',
                    'lfLugarBoton',
                    'lfPosicionIcono',
                    {Listas para Proyecto Especial Schneider}
                    'lfDPEscolar',
                    'lfDPNivel',
                    'lfDPExpPuesto',
                    'lfJITipoPlaza',
                    'lfJIPlazaReporta',

                    'lfTipoDiasBase',
                    'lfComentariosPedir',
                    'lfComoCalificar',
                    'lfStatusAcuerdo',
                    'lfBorrarMovimSUA',
                    'lfStatusPlanVacacion',
                    'lfStatusPoliza',
                    'lfTiposdePrestamo', // Listas del proyecto especial Caja de Ahorro
                    'lfControles',
                    'lfStatusFestivos',
                    'lfPagoPlanVacacion',
                    'lfTratamientoExtras',
                    'lfDiasAplicaExento',
                    { Listas Fijas para Guardias de COTEMAR }
                    'lfRolGuardias',
                    'lfStatusPlaza',
                    'lfStatusGuardia',
                    'lfClaseKarPlaza',
                    'lfTipoKarPlaza',
                    'lfEstadosGuardia',
                    'lfRitmosGuardia',
                    {$ifdef COMPARTE_MGR}
                    'lfWFNotificacionCuando',
                    'lfWFNotificacionQue',
                    {$endif}
                    'lfTipoABordoGuardia',         {AV: 9/Ago/06}
                    'lfTipoDiaGuardia',            {AV: 9/Ago/06}
                    'lfTipoPagoGuardia',           {AV: 9/Ago/06}
                    'lfTipoEventoGuardia',         {AV: 1/Sept/06}
                    'lfClaseEventoGuardia',        {AV: 12/Sept/06}
                    'lfStatusAusenciaGuardia',     {AV: 27/Sept/06}
                    'lfTipoServicioLogistica',     {AV: 20/Oct/06}
                    'lfViaLogistica',              {AV: 20/Oct/06}
                    'lfSentidoLogistica',          {AV: 20/Oct/06}
                    'lfTipoPuertoLogistica',       {AV: 20/Oct/06}
                    'lfStatusViaje',               {EA: 23/Oct/2006}
                    'lfStatusTarjeta',
                    'lfTipoTarjeta',
                    'lfPuEmbarque',                {EA: 01/Nov/2006}
                    'lfPuDomicilio',               {EA: 01/Nov/2006}
                    'lfPuHabitacion',              {EA: 01/Nov/2006}
                    'lfStatusTarea',               {MV: 05/Nov/2006}
                    'lfStatusHosp',                {EA: 06/Nov/2006}
                    'lfSentidoHosp',               {EA: 06/Nov/2006}
                    'lfStatusTrasporte',           {EA: 08/Nov/2006}
                    'lfTipoCalEvent',              {EA: 13/Nov/2006}
                    'lfTipoRuta',                  {EA: 27/Nov/2006}
                    'lfStatusDePlaza',             {EA: 27/Nov/2006}
                    'lfPrioridadVac',              {EA: 12/Dic/2006}
                    'lfStatusVac',                 {EA: 12/Dic/2006}
                    'lfPasoVac',                   {EA: 12/Dic/2006}
                    'lfTrabaja',                   {EA: 12/Dic/2006}
                    'lfPlantillaTipo',             {EA: 13/Dic/2006}
                    'lfPrioridadSolicitud',        {AV: 13/Dic/2006}
                    'lfCicloSolicitud',            {AV: 13/Dic/2006}
                    'lfReservacion',               {EA: 14/Dic/2006}
                    'lfStatusAutoriza',            {EA: 18/Dic/2006}
                    {Comienza listas de ICUMedical  12/Ene/07}
                    'lfSentidoCamion',
                    'lfStatusCamion',
                    'lfStatusPasajero',
                    'lfLogEventos',
                    { Lista fija Solvay }
                    'lfTipoPermuta',                {MV: 21/Feb/2007}
                    'lfPiramidacionPrestamos',      {AP: 28/Feb/2007}
                    'lfClaseSistBitacora'           {AP: 29/Mar/2007}
                    {$ifdef COMPARTE_MGR}
                    ,'lfWFAutenticacionWebService'
                    ,'lfWFCodificacionWebService'
                    {$endif}
                    ,'lfAltaMedica',  {EA: 14/03/07 }
                    'lfRDDDefaultsTexto',
                    'lfRDDDefaultsFloat',
                    'lfRDDDefaultsLogico',
                    'lfRDDValorActivo',
                    'lfStatusPlantilla',
                    'lfStatusValuacion',
                    'lfStatusLiqIMSS',
                    'lfReclasificaBool',
                    'lfReclasificaRelacion',
                    'lfReclasificaTipoVigencia',
                    'lfReclasificaZonaGeo',


                    'lfStatusTEvento',
                    'lfTurnoAbordo',
                    'lfBitacoraStatus',
                    'lfGrupoRecibo',
                    'lfSubStatus',
                    'lfTipoPuestoContrato',
                    'lfTipoPlaza',
                    'lfTipoCobertura',
                    'lfClaseBitacoraCOTEMAR',
                    'lfClaseBitacoraCOTEMARLogistica',

                    'lfModulosSolTran'


                    ,'lfViaticoNivelRestriccion',
                    'lfViaticoTopeConceptoGasto',

                    'lfAccionBitacora',

                    'lfTipoLogin',
                    'lfFiltroStatusEmpleado',
                    'lfAccionRegistros',
                    'lfTipoInfoExp',
                    'lfConflictoImp',
                    'lfTipoRangoActivo2',
                    'lfDiasCafeteria',
                    'lfRequisitoTipo',
                    'lfTipoMovInfonavit',
                    'lfBimestres',
                    'lfTipoDescuentoInfo',
                    'lfStatusReglaPrestamo',
                    'lfTipoJornadaTrabajo',
		    'lfFasesContingencia',              {279}     {AV : 16/07/08 }
                    'lfStatusPersonaExterna',          {280}      {AV : 16/07/08 }
                    'lfStatusHospedaje',               {281}      {AV : 16/07/08 }
                    'lfClaseNominaCOTEMAR',            {282}      {AV : 16/07/08 }
                    'lfLstDispositivos',
                    'lfStatusSimFiniquitos',
                    'lfTipoSaldoVacaciones'           {283}
                    ,'lfClaseRiesgo'                   {285}      {Indica la clase de prima de riesgo}
                    ,'lfObjetivoCursoSTPS'             {290}      {EZ v2010}
                    ,'lfModalidadCursoSTPS'            {291}      {EZ v2010}
                    ,'lfDiscapacidadSTPS'              {292}      {EZ v2010}
                    ,'lfTipoInstitucionSTPS'           {293}      {EZ v2010}
                    ,'lfTipoDocProbatorioSTPS'         {294}      {EZ v2010}
                    ,'lfStatusDeclaracionAnual'        {295}
                    ,'lfTipoAgenteSTPS'                {296}      {EZ V2010}
                    ,'lfTipoHoraTransferenciaCosteo'     {293}      {CV V2011}
                    ,'lfTipoDescConciliaInfonavit'       {294}      {AV V2011B4}
                    ,'lfTiposDerechosAcceso'             {295}      {CV V2011B4}
                    ,'lfTipoPantallaDerechoAcceso '      {296}      {CV V2011B4}
                    ,'lfFuenteDerechoAcceso'             {297}      {CV V2011B4}
                    ,'lfImpactoDerechoAcceso'            {298}      {CV V2011B4}
                    ,'lfTipoSGM'                         {299}      {EZ V2012}
                    ,'lfTipoEmpSGM'                      {300}      {EZ V2012}
                    ,'lfStatusSGM'                       {301}      {EZ V2012}
                    ,'lfStatusTransCosteo'               {302}      {CV Costeo 2011}
                    ,'lfStatusTransCosteoConsulta'       {303}      {CV Costeo 2011}
                    ,'lfStatusEncuestaKiosco'            {304}      {EZ v2013}
                    ,'lfTipoAsegurado'                   {305}      {EZ SGM 2DA FASE v2013}
                    ,'lfStatusSincronizacionDiurna'      {306}      {AV Cotemar cambio v2013}
	            ,'lfCotemarPorcentajeCantidad'       {307}      {AV Cotemar cambio v2013}
	            ,'lfCotemarPeriodoTipoBono'          {308}      {JB Cotemar cambio v2013}
                    ,'lfTipoPeriodoConfidencial'         {309}      {JA V2013}
                    ,'lfAuthTressEmail'                  {AV Cummins Julio 7 2012}
	            ,'lfCotemarPorcentajeCantidad'       {310}      {AV Cotemar cambio v2013}
                    ,'lfCotemarPeriodoTipoBono'          {311}      {JB Cotemar cambio v2013}
                    ,'lfStatusTimbrado'                      {312}
                    ,'lfStatusTimbradoPeticion'              {313}
                    ,'lfChecadasSimultaneas'                 {320}
                    );


function QuitaAcentos( sValue : String ) : String;
const
     LETRA_A_MINUSCULA = Ord( '�' );
     LETRA_E_MINUSCULA = Ord( '�' );
     LETRA_I_MINUSCULA = Ord( '�' );
     LETRA_O_MINUSCULA = Ord( '�' );
     LETRA_U_MINUSCULA = Ord( '�' );
     LETRA_ENE_MINUSCULA = Ord( '�' );
     LETRA_ENE_MAYUSCULA = Ord( '�' );
     LETRA_U_DIERESIS = Ord( '�' );
     LETRA_ADMIRACION = Ord( '�' );
     LETRA_PREGUNTA = Ord( '�' );
var
   i, iLen: Integer;
   cValue: Char;
begin
     iLen  := Length( sValue );
     Result := '';
     for i := 1 to iLen do
     begin
        cValue := sValue[ i ];
        case Ord( cValue ) of
               LETRA_A_MINUSCULA: cValue := 'a';
               LETRA_E_MINUSCULA: cValue := 'e';
               LETRA_I_MINUSCULA: cValue := 'i';
               LETRA_O_MINUSCULA: cValue := 'o';
               LETRA_U_MINUSCULA: cValue := 'u';
               LETRA_ENE_MINUSCULA: cValue := 'n';
               LETRA_ENE_MAYUSCULA: cValue := 'N';
               LETRA_U_DIERESIS: cValue := 'u';
               LETRA_ADMIRACION: cValue := '!';
               LETRA_PREGUNTA: cValue := '?';
        end;
        Result := Result + cValue;
     end;
end;

function QuitaPalabras( sValue : String ) : String;
begin
     Result := sValue;
     Result := UpperCase(QuitaAcentos(Result));
     Result := StrTransAll(Result, 'DE ', '' );
     Result := StrTransAll(Result, 'CON ', '' );
     Result := StrTransAll(Result, 'LAS ', '' );
     Result := StrTransAll(Result, 'QUE ', '' );
     Result := StrTransAll(Result, 'PARA ', '' );
     Result := StrTransAll(Result, 'DEL ', '' );
     Result := StrTransAll(Result, 'EL ', '' );
     Result := StrTransAll(Result, 'LOS ', '' );
     Result := StrTransAll(Result, 'LA ', '' );
     Result := StrTransAll(Result, 'EN ', '' );
     Result := StrTransAll(Result, '?', '' );
     Result := StrTransAll(Result, '/', '' );
     Result := StrTransAll(Result, '#', '' );
     Result := StrTransAll(Result, ':', '' );
end;

procedure TEditaCampos.FormShow(Sender: TObject);
 function GetItem( oLista:TStrings; iLookup: integer ): integer;
  var i: integer;
 begin
      Result := -1;
      for i:= 0 to oLista.Count - 1 do
      begin
           if Integer( oLista.Objects[i] ) = iLookup then
           begin
                Result := i;
                Break;
           end;
      end;
 end;
begin
     with InfoCampo do
     begin
          eNombre.Text := Campo;
          eTitulo.Text := Descripcion;
          eClaves.Text := PalabrasClave;
          eTCorto.Text := NombreCorto;
          cbTipoCampo.ItemIndex := Ord( Tipo );
          eMascara.Text := Mascara;
          eAncho.Valor := Ancho;
          cbConfidencial.Checked := Confidencial;
          LlenaComboEntidades( cbEntidades.Items );
          LlenaComboListaFija( cbListaFija.Items );

          case Rango of
               rRangoEntidad:
               begin
                    cbTipoFiltro.ItemIndex := Ord(tfLookup);
                    cbEntidades.ItemIndex := GetItem( CBEntidades.Items, Lookup );
               end;
               rRangoListas:
               begin
                    cbTipoFiltro.ItemIndex := Ord(tfListaFija);
                    cbListaFija.ItemIndex := GetItem( cbListaFija.Items, Lookup );;
               end;
               else
               begin
                    cbTipoFiltro.ItemIndex := Ord(tfNinguna);
               end;
          end;
     end;
     if Nuevo then
        Caption := 'Agregar Un Campo'
     else
         Caption := 'Editar Un Campo';
     Controles;
     ControlesTipoFiltro;
     if StrVacio( ENombre.Text ) then
        ActiveControl := eNombre
     else
         ActiveControl := eTitulo;
end;
{$ifdef ANTES_VERSION_2006}
procedure TEditaCampos.LlenaComboListaFija( Lista: TStrings );
 var i: Integer;
     oLista: TstringList;
begin
     with Lista do
     begin
          try
             BeginUpdate;
             Clear;

             oLista:= TstringList.Create;
             try

             for i := 0 to Ord( High( ListasFijas ) ) do
                 oLista.AddObject( aListasFijas[ ListasFijas(i) ], TObject(i) );

             oLista.Sorted := True;
             Lista.AddStrings( oLista );
           finally
                  FreeAndNil( oLista );
           end;

          finally
                 EndUpdate;
          end;
     end;
end;
{$ELSE}
procedure TEditaCampos.LlenaComboListaFija( Lista: TStrings );
var
   iLista: Integer;
   eLista: ListasFijas;
   oLista: TStringList;
begin

     oLista:= TstringList.Create;
     try
        for eLista := Low( ListasFijas ) to High( ListasFijas ) do
        begin
             //oLista.AddObject( aListasFijas[ ListasFijas(i) ], TObject( Ord( eLista ) ) );
             iLista := Ord( eLista );
             oLista.AddObject( TypInfo.GetEnumName( TypeInfo( ListasFijas ), iLista ), TObject( iLista ) );
        end;

        oLista.Sorted := True;
        with Lista do
        begin
             try
                BeginUpdate;
                Clear;
                Lista.AddStrings( oLista );
             finally
                    EndUpdate;
             end;
        end;
     finally
            FreeAndNil( oLista );
     end;
     Lista.SaveToFile('d:\temp\ListaNueva.txt');
end;

{$ENDIF}

procedure TEditaCampos.LlenaComboEntidades( Lista: TStrings );
var
   i : TipoEntidad;
   sEntidad : String;
   oLista: TstringList;
begin

     {for i := Low( TipoEntidad )  to High( TipoEntidad )  do
     begin
          sEntidad := ZetaTipoEntidadTools.ObtieneEntidad( i );
          if ( sEntidad <> VACIO ) then
             Lista.Add( sEntidad );
     end;}

     oLista:= TstringList.Create;
     try

        for i := Low( TipoEntidad )  to High( TipoEntidad )  do
        begin
             sEntidad := ZetaTipoEntidadTools.ObtieneEntidad( i );
             if ZetaCommonTools.StrLleno( sEntidad  ) then
             begin
                  oLista.AddObject( sEntidad+'='+ IntToStr( Ord( i ) ) , TObject(Ord(i)) );
             end;
        end;

        oLista.Sorted := True;
        Lista.AddStrings( oLista );
     finally
            FreeAndNil( oLista );
     end;
end;

procedure TEditaCampos.Controles;
var
   eTipo: eTipoGlobal;
begin
     eTipo := eTipoGlobal( cbTipoCampo.ItemIndex );
     case eTipo of
          tgBooleano: cbTipoFiltro.Enabled := FALSE;
          tgFloat: cbTipoFiltro.Enabled := FALSE;
          tgFecha: cbTipoFiltro.Enabled := FALSE;
     else
         cbTipoFiltro.Enabled := TRUE;
     end;
end;

procedure TEditaCampos.cbTipoCampoChange(Sender: TObject);
var
   eTipo: eTipoGlobal;
begin
     eTipo := eTipoGlobal( cbTipoCampo.ItemIndex );
     eMascara.text := FInfoDiccionario.GetDefMascara( eTipo );
     eAncho.Valor := FInfoDiccionario.GetDefAncho( eTipo );

     //Rango_cb.ItemIndex := Ord( FInfoDiccionario.GetDefRango( eTipo ) );
     //RangoActivo_cb.ItemIndex := Ord( FInfoDiccionario.GetDefRangoActivo( eTipo ) );
     cbEntidades.ItemIndex := Ord( enNinguno );
     Controles;
end;

procedure TEditaCampos.eTituloChange(Sender: TObject);
 var sTitulo : string;
begin
     sTitulo := eTitulo.Text;
     if StrVacio( eTCorto.Text ) then
     begin
          if sTitulo = 'C�digo de Tabla' then
             eTCorto.Text := 'C�digo'
          else if sTitulo = 'Descripci�n en Ingl�s' then
               eTCorto.Text := 'Ingl�s'
          else if sTitulo = 'N�mero de Empleado' then
               eTCorto.Text := 'N�mero'
          else if sTitulo = 'N�mero de Expediente' then
               eTCorto.Text := 'N�mero'
          else if sTitulo = 'N�mero de Uso General' then
               eTCorto.Text := 'N�mero'
          else if sTitulo = 'Observaciones' then
               eTCorto.Text := 'Observaciones'
          else if sTitulo = 'Texto de Uso General' then
               eTCorto.Text := 'Texto'
          else if sTitulo = 'Usuario que Modific�' then
               eTCorto.Text := 'Usuario'
          else eTCorto.Text := sTitulo;
     end;
     if StrVacio( eClaves.Text ) then
        eClaves.Text := QuitaPalabras( sTitulo );
end;

procedure TEditaCampos.eNombreChange(Sender: TObject);
begin
     CamposPreDefinidos;
end;

procedure TEditaCampos.CamposPreDefinidos;
begin
 {var sNombre, sTitulo,  sClaves ,  sCorto : string;
     iAncho, iTField, iNumero  : integer;

 procedure Niveles(const iNivel : integer);
 begin
      sTitulo := Format('Nivel #%d',[iNivel]);
      iAncho := 6;
      iTField := 4;
      sClaves := Format('NIVEL %d',[iNivel]);
      sCorto := Format('Nivel #%d', [iNivel]);
      //DI_TRANGO.ItemIndex := 40+iNivel;
      //TipoFiltro.ItemIndex := 0;
      //TipoFiltroClick(TipoFiltro);
      iNumero := 40+iNivel;
 end;
begin
     lFixedField := TRUE;
     try
        iAncho := 0;
        iTField := 0;
        iNumero := 0;
        sTitulo := '';
        sNombre := eNombre.Text;

        if sNombre = 'CB_CODIGO' then
        begin
             sTitulo := 'N�mero de Empleado';
             iAncho := 7;
             iTField := 2;
             sClaves := 'NUMERO EMPLEADO';
             sCorto := 'N�mero';
             DI_TRANGO.ItemIndex := 10;
             TipoFiltro.ItemIndex := 0;
             TipoFiltroClick(TipoFiltro);
             iNumero := 10;
             FieldByName('DI_VALORAC').AsString := '@EMPLEADO';
        end
        else if sNombre = 'US_CODIGO' then
        begin
             sTitulo := 'Usuario que Modific�';
             iAncho := 7;
             iTField := 2;
             sClaves := 'USUARIO MODIFICO';
             sCorto := 'Usuario';
             DI_TRANGO.ItemIndex := 85;
             TipoFiltro.ItemIndex := 0;
             TipoFiltroClick(TipoFiltro);
             iNumero := 85;
        end
        else if sNombre = 'CB_NIVEL1' then Niveles(1)
        else if sNombre = 'CB_NIVEL2' then Niveles(2)
        else if sNombre = 'CB_NIVEL3' then Niveles(3)
        else if sNombre = 'CB_NIVEL4' then Niveles(4)
        else if sNombre = 'CB_NIVEL5' then Niveles(5)
        else if sNombre = 'CB_NIVEL6' then Niveles(6)
        else if sNombre = 'CB_NIVEL7' then Niveles(7)
        else if sNombre = 'CB_NIVEL8' then Niveles(8)
        else if sNombre = 'CB_NIVEL9' then Niveles(9)
        else if sNombre = 'CB_PUESTO' then
        begin
             sTitulo := 'C�digo de Puesto';
             iAncho := 6;
             iTField := 4;
             sClaves := 'CODIGO PUESTO';
             sCorto := 'Puesto';
             DI_TRANGO.ItemIndex := iNumero;
             TipoFiltro.ItemIndex := 0;
             TipoFiltroClick(TipoFiltro);
             iNumero := 59;
        end
        else if sNombre = 'CB_CLASIFI' then
        begin
             sTitulo := 'C�digo de Clasificaci�n';
             iAncho := 6;
             iTField := 4;
             sClaves := 'CODIGO CLASIFICACION';
             sCorto := 'Clasificaci�n';
             DI_TRANGO.ItemIndex := iNumero;
             TipoFiltro.ItemIndex := 0;
             TipoFiltroClick(TipoFiltro);
             iNUmero := 9;
        end
        else if sNombre = 'CB_TURNO' then
        begin
             sTitulo := 'C�digo de Turno';
             iAncho := 6;
             iTField := 4;
             sClaves := 'CODIGO TURNO';
             sCorto := 'Turno';
             DI_TRANGO.ItemIndex := 59;
             TipoFiltro.ItemIndex := 0;
             TipoFiltroClick(TipoFiltro);
             iNUmero := 69;
        end
        else if sNombre = 'TB_CODIGO' then
        begin
             sTitulo := 'C�digo de Tabla';
             iAncho := 6;
             iTField := 4;
             sClaves := 'CODIGO TABLA';
             sCorto := 'C�digo';
             DI_TRANGO.ItemIndex := iNumero;
             TipoFiltro.ItemIndex := 0;
             TipoFiltroClick(TipoFiltro);
             iNumero := FieldByName('DI_CLASIFI').AsInteger;
        end
        else if sNombre = 'TB_TEXTO' then
        begin
             sTitulo := 'Texto de Uso General';
             iAncho := 30;
             iTField := 4;
             sClaves := 'TEXTO USO GENERAL';
             sCorto := 'Texto';
        end
        else if sNombre = 'TB_INGLES' then
        begin
             sTitulo := 'Descripci�n en Ingl�s';
             iAncho := 30;
             iTField := 4;
             sClaves := 'DESCRIPCION INGLES';
             sCorto := 'Ingl�s';
        end
        else if sNombre = 'TB_ELEMENT' then
        begin
             sTitulo := 'Descripci�n';
             iAncho := 30;
             iTField := 4;
             sClaves := 'DESCRIPCION';
             sCorto := 'Descripci�n';
        end

        else if sNombre = 'TB_NUMERO' then
        begin
             sTitulo := 'N�mero de Uso General';
             iAncho := 7;
             iTField := 1;
             sClaves := 'NUMERO USO GENERAL';
             sCorto := 'N�mero';
        end;
        if sTitulo <> '' then
        begin
             FieldByName('DI_TITULO').AsString := sTitulo;
             FieldByName('DI_ANCHO').AsInteger := iAncho;
             FieldByName('DI_NUMERO').AsInteger := iNumero;
             FieldByName('DI_CLAVES').AsString :=  sClaves;
             FieldByName('DI_TCORTO').AsString := sCorto;
        end;
        end;
     finally
            lFixedField := FALSE;
     end;  }
end;

procedure TEditaCampos.cbTipoFiltroChange(Sender: TObject);
begin
     ControlesTipoFiltro;
end;

procedure TEditaCampos.ControlesTipoFiltro;
begin
     cbEntidades.Visible := FALSE;
     cbListaFija.Visible := FALSE;
          lbTexto.Caption := '';

     case GetTipoFiltro of
          tfLookup:
          begin
               cbEntidades.Visible := TRUE;
               lbTexto.Caption := 'Lookup a Tabla:'
          end;
          tfListaFija:
          begin
               cbListaFija.Visible := TRUE;
               lbTexto.Caption := 'Lista Fija:'
          end;
     end;
end;

procedure TEditaCampos.OKClick(Sender: TObject);
begin
     with InfoCampo do
     begin
          Campo := eNombre.Text;
          Descripcion := eTitulo.Text;
          PalabrasClave := eClaves.Text;
          Tipo := GetTipoGlobal;
          Mascara := eMascara.Text;
          Ancho := eAncho.ValorEntero;
          Rango := GetRango;
          Lookup := GetLookup;
          RangoActivo := GetRangoActivo;
          Confidencial := cbConfidencial.Checked;
          NombreCorto := eTCorto.Text;
     end;
end;


function TEditaCampos.GetLookup : integer;
begin
     case GetRango of
          rNinguno,rFechas: Result := 0;
          rRangoEntidad: Result := INteger( cbEntidades.Items.Objects[ cbEntidades.ItemIndex ] );
          rRangoListas: Result := Integer( cbListaFija.Items.Objects[ cbListaFija.ItemIndex ] );
          rBool: Result := 60
          else Result := 0;
     end;
end;

function TEditaCampos.GetTipoFiltro: eTipoFiltro;
begin
     Result := eTipoFiltro( cbTipoFiltro.ItemIndex );
end;

function TEditaCampos.GetRangoActivo : eTipoRangoActivo;
//(raTodos,raActivo,raRango,raLista)
begin
     case GetRango of
          rNinguno, rFechas, rBool: Result := raRango
          else Result := raTodos;
     end;
end;

function TEditaCampos.GetRango : eTipoRango;
 // eTipoRango = (rNinguno,rRangoEntidad,rRangoListas, rFechas, rBool );
begin
     case GetTipoGlobal of
          tgBooleano : Result := rBool;
          tgFecha: Result := rFechas;
          tgNumero, tgTexto:
          begin
               case GetTipoFiltro of
                    tfLookup: Result := rRangoEntidad;
                    tfListaFija: Result := rRangoListas;
               else Result := rNinguno;
               end;
          end
          else Result := rNinguno
     end;
end;

function TEditaCampos.GetTipoGlobal: eTipoGlobal;
begin
     Result := ZetaCommonLists.eTipoGlobal( cbTipoCampo.Itemindex );
end;


procedure TEditaCampos.Button1Click(Sender: TObject);
begin
     eTCorto.Text := StrTransAll(eTCorto.Text, 'C�digo de ', '');
     eTCorto.Text := StrTransAll(eTCorto.Text, 'Codigo de ', '');
end;

procedure TEditaCampos.Button2Click(Sender: TObject);
begin
     eTCorto.Text := '';
end;

procedure TEditaCampos.Button3Click(Sender: TObject);
begin
     memo1.Lines.Text := Format( 'INSERT INTO DICCION( DI_CLASIFI, DI_NOMBRE, DI_TITULO, '+
                                             ' DI_ANCHO, DI_MASCARA, '+
                                             ' DI_TFIELD, DI_ORDEN,  '+
                                             ' DI_TRANGO, DI_NUMERO, '+
                                             ' DI_RANGOAC, DI_CLAVES, ' +
                                             ' DI_TCORTO, DI_CONFI ) '+
                                      'VALUES( %d, %s, %s, '+
                                             ' %d, %s, '+
                                             ' %d, %s, '+
                                             ' %d, %d, '+
                                             ' %d, %s, '+
                                             ' %s, %s ) ',
                                  [ Ord( FInfoCampo.Entidad ), EntreComillas( eNombre.Text ), EntreComillas( eTitulo.Text ),
                                    eAncho.ValorEntero, EntreComillas( eMascara.Text ),
                                    Ord( GetTipoGlobal ), EntreComillas( 'S' ),
                                    Ord( GetRango ), GetLookup,
                                    Ord( GetRangoActivo ), EntreComillas( eClaves.Text ),
                                    EntreComillas( eTCorto.Text ), EntreComillas( ZetaCommonTools.zBoolToStr( cbConfidencial.Checked ) ) ] );

end;

end.
