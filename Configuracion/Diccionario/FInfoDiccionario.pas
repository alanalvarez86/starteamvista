unit FInfoDiccionario;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, DB, DBCtrls, ComCtrls, DBTables,
     DEditarDiccionario,
     ZetaTipoEntidadTools,
     ZetaTipoEntidad,
     ZetaCommonLists;

type
  eTipoFiltro = ( tfLookup, tfListaFija, tfNinguna );
  TInfoCampo = class( TObject )
  private
    { Private declarations }
    FEntidad: TipoEntidad;
    FCampo: String;
    FDescripcion: String;
    FNombreCorto: String;
    FPalabrasClave: String;
    FTipo: eTipoGlobal;
    FAncho: Integer;
    FMascara: String;
    FConfidencial: Boolean;
    FRango: eTipoRango;
    {es un entero, porque puede tener el valor de una entidad, o valor de una lista fija}
    FLookup: integer;
    FRangoActivo: eTipoRangoActivo;
    FTipoFiltro: eTipoFiltro;
    procedure SetTipo(const Value: eTipoGlobal);
    procedure SetRango(const Value: eTipoRango);
    function GetValorAC: string;
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Campo: String read FCampo write FCampo;
    property Entidad: TipoEntidad read FEntidad write FEntidad;
    property Descripcion: String read FDescripcion write FDescripcion;
    property NombreCorto: String read FNombreCorto write FNombreCorto;
    property PalabrasClave: String read FPalabrasClave write FPalabrasClave;
    property Tipo: eTipoGlobal read FTipo write SetTipo;
    property Ancho: Integer read FAncho write FAncho;
    property Mascara: String read FMascara write FMascara;
    property Confidencial: Boolean read FConfidencial write FConfidencial;
    property Rango: eTipoRango read FRango write SetRango;
    property Lookup: integer read FLookup write FLookup;
    property RangoActivo: eTipoRangoActivo read FRangoActivo write FRangoActivo;
    property TipoFiltro: eTipoFiltro read FTipoFiltro write FTipoFiltro;
    property ValorAC: string read GetValorAC;
    procedure Assign( value : TinfoCampo );
  end;
  TInfoTabla = class( TObject )
  private
    { Private declarations }
    FProvider: TdmDiccionario;
    FCampos: TList;
    FEntidad: TipoEntidad;
    FTabla: String;
    FDescripcion: String;
    FNombreCorto: String;
    FPalabrasClave: String;
    FClasificacion: eClasifiReporte;
    FOrden: Integer;
    FRequeridos: TStrings;
    FNueva: boolean;
    function GetCampo(Index: Integer): TInfoCampo;
    procedure Delete(const Index: Integer);
    procedure AgregaDatosVacios;
  public
    { Public declarations }
    constructor Create(oProvider: TdmDiccionario);
    destructor Destroy; override;
    property Campo[ Index: Integer ]: TInfoCampo read GetCampo;
    property Entidad: TipoEntidad read FEntidad write FEntidad;
    property Tabla: String read FTabla write FTabla;
    property Descripcion: String read FDescripcion write FDescripcion;
    property NombreCorto: String read FNombreCorto write FNombreCorto;
    property PalabrasClave: String read FPalabrasClave write FPalabrasClave;
    property Clasificacion: eClasifiReporte read FClasificacion write FClasificacion;
    property Orden: Integer read FOrden write FOrden;
    property Requeridos: TStrings read FRequeridos;
    property Nueva: boolean read FNueva write FNueva;
    function AddCampo: TInfoCampo;
    function Count: Integer;
    function DescargaRequeridos: String;
    function ExisteCampo( const sCampo: String ): Boolean;
    procedure Borrar(const sCampo: String);
    procedure Clear;
    procedure Cargar( const zEntidad: TipoEntidad );
    procedure Descargar;
    procedure CargaRequeridos(const sRequeridos: String );
  end;

    function GetDefAncho(const Value: eTipoGlobal): Integer;
    function GetDefMascara(const Value: eTipoGlobal): String;
    function GetDefRangoActivo(const Value: eTipoGlobal): eTipoRangoActivo;
    function GetDefRango(const Value: eTipoGlobal): eTipoRango;

implementation

uses FEditaTabla,
     ZetaCommonTools,
     ZetaCommonClasses;

function GetDefMascara(const Value: eTipoGlobal): String;
begin
     case Value of
          tgFloat: Result := '#,0.00;-#,0.00';
          tgNumero: Result := '#0;-#0';
          tgFecha: Result := 'dd/mmm/yy';
     else
         Result := VACIO;
     end;
end;

function GetDefRangoActivo(const Value: eTipoGlobal): eTipoRangoActivo;
begin
     case Value of
          tgBooleano: Result := raRango;
          tgFloat: Result := raRango;
          tgNumero: Result := raRango;
          tgFecha: Result := raRango;
     else
         Result := raRango;
     end;
end;

function GetDefRango(const Value: eTipoGlobal): eTipoRango;
begin
     case Value of
          tgBooleano: Result := rBool;
          tgFloat: Result := rRangoListas;
          tgNumero: Result := rRangoListas;
          tgFecha: Result := rFechas;
{          tgBooleano: Result := rFechas;
          tgFecha: Result := rRangoListas;}
     else
         Result := rNinguno;
     end;
end;


function GetDefAncho(const Value: eTipoGlobal): Integer;
begin
     case Value of
          tgBooleano: Result := 1;
          tgFloat: Result := 15;
          tgNumero: Result := 7;
          tgFecha: Result := 9;
     else
         Result := 30;
     end;
end;

{ ******** TInfoCampo ********** }

constructor TInfoCampo.Create;
begin
end;

destructor TInfoCampo.Destroy;
begin
     inherited Destroy;
end;

procedure TInfoCampo.Assign(value: TinfoCampo);
begin
     Self.Campo := Value.Campo;
     Self.Entidad := Value.Entidad;
     self.Descripcion := Value.Descripcion;
     self.NombreCorto := Value.NombreCorto;
     self.PalabrasClave := Value.PalabrasClave;
     self.Tipo := Value.Tipo;
     self.Ancho := Value.Ancho;
     self.Mascara := Value.Mascara;
     self.Confidencial := Value.Confidencial;
     self.Rango := Value.Rango;
     self.Lookup := Value.Lookup;
     self.RangoActivo := Value.RangoActivo;
end;


function TInfoCampo.GetValorAC: string;
begin
     case FTipo of
          tgBooleano: Result := ZetaCommonClasses.K_GLOBAL_SI;
     else
     begin
          if Campo = 'CB_CODIGO' then Result := '@EMPLEADO'
          else if Campo = 'CB_PATRON' then Result := '@PATRON'
          else if Campo = 'LS_YEAR' then Result := '@IMSS_YEAR'
          else if Campo = 'LS_MONTH' then Result := '@IMSS_MES'
          else if Campo = 'LS_TIPO' then Result := '@IMSS_TIPO'
          else if Campo = 'PE_YEAR' then Result := '@YEAR'
          else if Campo = 'PE_TIPO' then Result := '@TIPO'
          else if Campo = 'PE_NUMERO' then Result := '@NUMERO'
          else if Campo = 'NO_NETO' then Result := '>0'
          else Result := '';
     end;
     end;
end;

procedure TInfoCampo.SetRango(const Value: eTipoRango);
begin
     FRango := Value;
     case FRango of
          rRangoEntidad: FTipoFiltro := tfLookup;
          rRangoListas: FTipoFiltro := tfListaFija;
     else
         FTipoFiltro := tfNinguna;
     end;
end;

procedure TInfoCampo.SetTipo(const Value: eTipoGlobal);
begin
     FTipo := Value;
     Ancho := GetDefAncho( FTipo );
     Rango := GetDefRango( FTipo );
     RangoACtivo := GetDefRangoActivo( FTipo );
     Mascara := GetDefMascara( FTipo );
end;

{ ********* TInfoTabla *********** }

constructor TInfoTabla.Create(oProvider: TdmDiccionario);
begin
    FCampos := TList.Create;
    FProvider := oProvider;
    FRequeridos :=TStringList.Create;
end;

destructor TInfoTabla.Destroy;
begin
     Clear;
     FreeAndNil( FCampos );
     FreeAndNil( FRequeridos );
     inherited Destroy;
end;

procedure TInfoTabla.Clear;
var
   i: Integer;
begin
     for i := ( Count - 1 ) downto 0 do
     begin
          Self.Delete( i );
     end;
     FCampos.Clear;
end;

function TInfoTabla.Count: Integer;
begin
     Result := FCampos.Count;
end;

procedure TInfoTabla.Delete(const Index: Integer);
begin
     Campo[ Index ].Free;
     FCampos.Delete( Index );
end;

function TInfoTabla.GetCampo(Index: Integer): TInfoCampo;
begin
     with FCampos do
     begin
          if ( Index >= 0 ) and ( Index < Count ) then
             Result := TInfoCampo( Items[ Index ] )
          else
              Result := nil;
     end;
end;

function TInfoTabla.AddCampo: TInfoCampo;
begin
     Result := TInfoCampo.Create;
     try
        FCampos.Add( Result );
        with Result do
        begin
             Entidad := Self.Entidad;
        end;
     except
           on Error: Exception do
           begin
                FreeAndNil( Result );
                raise;
           end;
     end;
end;

procedure TInfoTabla.Borrar( const sCampo: String );
var
   i: Integer;
begin
     for i := 0 to ( Count - 1 ) do
     begin
          if ( Campo[ i ].Campo = sCampo ) then
          begin
               Delete( i );
               Break;
          end;
     end;
end;

function TInfoTabla.ExisteCampo(const sCampo: String): Boolean;
var
   i: Integer;
begin
     Result := False;
     for i := 0 to ( Count - 1 ) do
     begin
          if ( Campo[ i ].Campo = sCampo ) then
          begin
               Result := True;
               Break;
          end;
     end;
end;

procedure TInfoTabla.Cargar( const zEntidad: TipoEntidad );
begin
     Entidad := zEntidad;
     AgregaDatosVacios;
     dmDiccionario.CargarInfoTabla;
end;

procedure TInfoTabla.Descargar;
begin
     dmDiccionario.DescargarInfoTabla;
end;

procedure TInfoTabla.AgregaDatosVacios;
Begin
     Nueva := True;
     Tabla := VACIO;
     Descripcion := ZetaTipoEntidadTools.ObtieneEntidad( Entidad );
     NombreCorto := VACIO;
     PalabrasClave := VACIO;
     {$ifdef Tress}
     Clasificacion := crEmpleados;
     {$endif}
     {$ifdef Seleccion}
     Clasificacion := crSeleccion;
     {$endif}
     Orden := 0;
     Requeridos.Clear;
     Clear;
end;

procedure TInfoTabla.CargaRequeridos(const sRequeridos: String );
var
   iLongitud, i: Integer;
   sCampoReq: String;
begin
     sCampoReq := VACIO;
     Requeridos.Clear;
     i := 1;
     iLongitud := Length( sRequeridos );
     while i <= iLongitud do
     begin
          if ( sRequeridos[ i ] <> ';' ) then
              sCampoReq := sCampoReq + sRequeridos[ i ]
          else
          begin
               Requeridos.Add( sCampoReq );
               sCampoReq := '';
          end;
          i := i + 1;
     end;
     if i > 1 then
     begin
          Requeridos.Add( sCampoReq );
          sCampoReq := '';
     end;
end;

function TInfoTabla.DescargaRequeridos: String;
var
   i: integer;
begin
     with Requeridos do
     begin
          BeginUpdate;
          try
             for i := 0 to ( Count - 1 ) do
             begin
                  Result := ConcatString( Result, Strings[ i ], ';' );
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

end.
