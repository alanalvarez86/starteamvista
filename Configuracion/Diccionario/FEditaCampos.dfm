object EditaCampos: TEditaCampos
  Left = 292
  Top = 214
  Width = 373
  Height = 474
  Anchors = [akTop, akRight]
  Caption = 'Editar Campos'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lbCampo: TLabel
    Left = 62
    Top = 33
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Campo:'
  end
  object Label1: TLabel
    Left = 39
    Top = 56
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descripci'#243'n:'
  end
  object Label3: TLabel
    Left = 24
    Top = 102
    Width = 74
    Height = 13
    Alignment = taRightJustify
    Caption = 'Palabras Clave:'
  end
  object Label2: TLabel
    Left = 30
    Top = 79
    Width = 68
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nombre Corto:'
  end
  object Label4: TLabel
    Left = 23
    Top = 10
    Width = 75
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo de Campo:'
  end
  object Label6: TLabel
    Left = 54
    Top = 124
    Width = 44
    Height = 13
    Alignment = taRightJustify
    Caption = 'Mascara:'
  end
  object lbTexto: TLabel
    Left = 63
    Top = 193
    Width = 35
    Height = 13
    Alignment = taRightJustify
    Caption = 'lbTexto'
  end
  object Label5: TLabel
    Left = 64
    Top = 147
    Width = 34
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ancho:'
  end
  object Label10: TLabel
    Left = 34
    Top = 170
    Width = 64
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo de Filtro:'
  end
  object Panel2: TPanel
    Left = 0
    Top = 400
    Width = 357
    Height = 36
    Align = alBottom
    TabOrder = 11
    DesignSize = (
      357
      36)
    object OK: TBitBtn
      Left = 194
      Top = 6
      Width = 76
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&OK'
      Default = True
      ModalResult = 1
      TabOrder = 0
      OnClick = OKClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object Cancelar: TBitBtn
      Left = 274
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Salir'
      ModalResult = 2
      TabOrder = 1
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
        F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
        000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
        338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
        45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
        3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
        F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
        000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
        338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
        4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
        8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
        333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
        0000}
      NumGlyphs = 2
    end
  end
  object cbConfidencial: TCheckBox
    Left = 34
    Top = 216
    Width = 79
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Confidencial:'
    TabOrder = 10
  end
  object eAncho: TZetaNumero
    Left = 100
    Top = 143
    Width = 36
    Height = 21
    Mascara = mnDias
    TabOrder = 6
    Text = '0'
  end
  object cbEntidades: TComboBox
    Left = 100
    Top = 188
    Width = 245
    Height = 21
    ItemHeight = 13
    TabOrder = 9
  end
  object cbListaFija: TZetaKeyCombo
    Left = 100
    Top = 189
    Width = 145
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 8
    ListaFija = lfTipoRango
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
  end
  object eMascara: TEdit
    Left = 100
    Top = 120
    Width = 145
    Height = 21
    TabOrder = 5
  end
  object cbTipoCampo: TZetaKeyCombo
    Left = 100
    Top = 6
    Width = 145
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
    OnChange = cbTipoCampoChange
    OnExit = cbTipoCampoChange
    ListaFija = lfTipoGlobal
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
  end
  object eTCorto: TEdit
    Left = 100
    Top = 75
    Width = 225
    Height = 21
    MaxLength = 30
    TabOrder = 3
  end
  object eClaves: TEdit
    Left = 100
    Top = 98
    Width = 225
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 30
    TabOrder = 4
  end
  object eTitulo: TComboBox
    Left = 100
    Top = 53
    Width = 225
    Height = 21
    DropDownCount = 50
    ItemHeight = 13
    MaxLength = 30
    Sorted = True
    TabOrder = 2
    OnExit = eTituloChange
    Items.Strings = (
      'C'#243'digo de Tabla'
      'Descripci'#243'n'
      'Descripcion #'
      'Descripci'#243'n en Ingl'#233's'
      'N'#250'mero #'
      'N'#250'mero de Empleado'
      'N'#250'mero de Expediente'
      'N'#250'mero de Uso General'
      'Observaciones'
      'Si/No #'
      'Si/No/Texto #'
      'Texto #'
      'Texto de Uso General'
      'Usuario que Modific'#243)
  end
  object eNombre: TEdit
    Left = 100
    Top = 29
    Width = 145
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 10
    TabOrder = 1
    OnChange = eNombreChange
  end
  object cbTipoFiltro: TComboBox
    Left = 100
    Top = 166
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 7
    OnChange = cbTipoFiltroChange
    Items.Strings = (
      'Lookup a una Tabla'
      'Lista Fija'
      'Ninguna de las dos')
  end
  object Button1: TButton
    Left = 256
    Top = 240
    Width = 75
    Height = 25
    Caption = 'Nombre Corto'
    TabOrder = 12
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 328
    Top = 79
    Width = 19
    Height = 17
    TabOrder = 13
    OnClick = Button2Click
  end
  object Memo1: TMemo
    Left = 8
    Top = 304
    Width = 345
    Height = 89
    Lines.Strings = (
      'Memo1')
    ScrollBars = ssVertical
    TabOrder = 14
  end
  object Button3: TButton
    Left = 256
    Top = 272
    Width = 75
    Height = 25
    Caption = 'SQL (insert)'
    TabOrder = 15
    OnClick = Button3Click
  end
end
