unit FCamposRequeridos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Grids, Buttons, ExtCtrls,
     FInfoDiccionario,
     ZetaSmartLists;

type
  TCamposRequeridos = class(TForm)
    PanelBotones: TPanel;
    OK: TBitBtn;
    Cancelar: TBitBtn;
    ListaDisponible: TListBox;
    procedure ListaDisponibleClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
    FInfoTabla: TInfoTabla;
    FRequeridos: TStrings;
    function GetFullName(const sFieldName: String): String;
    procedure Controles;
    procedure SetInfoTabla(const Value: TInfoTabla);
  public
    { Public declarations }
    property Requeridos: TStrings read FRequeridos write FRequeridos;
    property InfoTabla: TInfoTabla read FInfoTabla write SetInfoTabla;
    function HayCampos: Boolean;
  end;

var
  CamposRequeridos: TCamposRequeridos;

implementation

{$R *.DFM}

uses DEditarDiccionario,
     ZReportConst;

function TCamposRequeridos.GetFullName( const sFieldName: String ): String;
const
     K_PUNTO = '.';
begin
     Result := FInfoTabla.Tabla + K_PUNTO + sFieldName;
end;

function TCamposRequeridos.HayCampos: Boolean;
begin
     Result := ( ListaDisponible.Items.Count > 0 );
end;

procedure TCamposRequeridos.SetInfoTabla(const Value: TInfoTabla);
var
   i: Integer;
   sCampo: String;
begin
     FInfoTabla := Value;
     with ListaDisponible do
     begin
          with Items do
          begin
               BeginUpdate;
               try
                  Clear;
                  for i := 0 to ( InfoTabla.Count - 1 ) do
                  begin
                       sCampo := InfoTabla.Campo[ i ].Campo;
                       if ( Self.Requeridos.IndexOf( GetFullName( sCampo ) ) < 0 ) then
                          Add( sCampo );
                  end;
               finally
                      EndUpdate;
               end;
          end;
     end;
end;

procedure TCamposRequeridos.Controles;
begin
     with OK do
     begin
          if Not ( Enabled ) then
          begin
               Enabled := True;
               with Cancelar do
               begin
                    Kind := bkCancel;
                    Caption := '&Cancelar';
               end;
          end;
     end;
end;

procedure TCamposRequeridos.ListaDisponibleClick(Sender: TObject);
begin
     Controles;
end;

procedure TCamposRequeridos.OKClick(Sender: TObject);
var
   sCampo: String;
begin
     with ListaDisponible do
     begin
          sCampo := Items.Strings[ ItemIndex ];
     end;
     with Requeridos do
     begin
          Add( GetFullName( sCampo ) );
          if ( sCampo = K_COLABORA_CODIGO ) then
             Add( K_COLABORA_NOMBRE )
          else if ( sCampo = K_EXPEDIENTE_CODIGO )then
             Add( Q_PRETTY_EXP );
     end;
end;


end.
