object dmDiccionario: TdmDiccionario
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  object dbDiccionario: TDatabase
    DatabaseName = 'dbDiccionario'
    LoginPrompt = False
    Params.Strings = (
      'USER NAME=SYSDBA'
      'PASSWORD=masterkey')
    SessionName = 'Default'
    Left = 25
    Top = 17
  end
  object Entidades: TQuery
    DatabaseName = 'dbDiccionario'
    SQL.Strings = (
      'select DI_NOMBRE from Diccion where DI_CLASIFI = -1')
    Left = 88
    Top = 17
  end
  object Q_General: TQuery
    DatabaseName = 'dbDiccionario'
    Left = 144
    Top = 16
  end
  object Q_Orden: TQuery
    DatabaseName = 'dbDiccionario'
    Left = 200
    Top = 16
  end
end
