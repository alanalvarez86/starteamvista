unit FEditarEntidades;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, DB, DBTables,
     FEditarDiccionario,
     ZetaSmartLists,
     ZetaCommonLists;

type
  TEditarEntidades = class(TForm)
    gbEntidades: TGroupBox;
    Subir: TZetaSmartListsButton;
    Bajar: TZetaSmartListsButton;
    ListaEntidades: TZetaSmartListBox;
    OK: TBitBtn;
    Cancelar: TBitBtn;
    ZetaSmartLists1: TZetaSmartLists;
    procedure OKClick(Sender: TObject);
    procedure ZetaSmartLists1AlBajar(Sender: TObject; var Objeto: TObject; Texto: String);
    procedure ZetaSmartLists1AlSubir(Sender: TObject; var Objeto: TObject; Texto: String);
    procedure FormShow(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
  private
    { Private declarations }
    FClasificacion: eClasifiReporte;
  public
    { Public declarations }
    property Clasificacion: eClasifiReporte Read FClasificacion Write FClasificacion;
  end;

var
  EditarEntidades: TEditarEntidades;

implementation

uses ZetaCommonTools,
     DEditarDiccionario;

{$R *.DFM}

procedure TEditarEntidades.FormShow(Sender: TObject);
begin
     dmDiccionario.CargarEntidades( ListaEntidades.Items, Clasificacion );
     Caption := 'Entidades de ' + ZetaCommonLists.ObtieneElemento( lfClasifiReporte, Ord( Clasificacion ) );
     OK.Enabled := TRUE;
     with Cancelar do
     begin
          Kind := bkCancel;
          Caption := '&Cancelar';
     end;
end;

procedure TEditarEntidades.ZetaSmartLists1AlBajar(Sender: TObject; var Objeto: TObject; Texto: String);
begin
     Ok.Enabled := True;
end;

procedure TEditarEntidades.ZetaSmartLists1AlSubir(Sender: TObject; var Objeto: TObject; Texto: String);
begin
     Ok.Enabled := True;
end;

procedure TEditarEntidades.CancelarClick(Sender: TObject);
begin
     with Cancelar do
     begin
          Kind := bkClose;
          Caption := '&Salir';
     end;
end;

procedure TEditarEntidades.OKClick(Sender: TObject);
begin
     dmDiccionario.DescargarEntidades( ListaEntidades.Items, Clasificacion );
     OK.Enabled := False;
     with Cancelar do
     begin
          Kind := bkClose;
          Caption := '&Salir';
     end;
end;

end.
