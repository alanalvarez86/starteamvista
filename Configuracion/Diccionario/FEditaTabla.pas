unit FEditaTabla;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ZetaKeyCombo, ComCtrls, ToolWin, Mask, ExtCtrls, Grids,
     FInfoDiccionario,
     FCamposRequeridos,
     ZetaDBTextBox,
     ZetaNumero,
     ZetaSmartLists;

type
  TEditaTabla = class(TForm)
    GroupBox2: TGroupBox;
    ListaCampos: TStringGrid;
    Panel2: TPanel;
    OK: TBitBtn;
    Cancelar: TBitBtn;
    PanelBotones: TPanel;
    Nuevo: TSpeedButton;
    BotonBorrar: TSpeedButton;
    Edita: TSpeedButton;
    PanelEntidad: TPanel;
    GroupBox1: TGroupBox;
    PanelRequeridos: TPanel;
    Alta: TSpeedButton;
    Baja: TSpeedButton;
    Subir: TZetaSmartListsButton;
    Bajar: TZetaSmartListsButton;
    lbTabla: TLabel;
    Tabla_Ed: TEdit;
    Label1: TLabel;
    Label3: TLabel;
    Palabra_Ed: TEdit;
    Label2: TLabel;
    NomCorto_Ed: TEdit;
    Label4: TLabel;
    Clasificacion_Cb: TZetaKeyCombo;
    Label6: TLabel;
    Orden: TZetaNumero;
    ListaRequeridos: TListBox;
    CargarTodos: TSpeedButton;
    Descripcion_ed: TEdit;
    procedure ListaCamposDblClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure Tabla_EdKeyPress(Sender: TObject; var Key: Char);
    procedure Clasificacion_CbChange(Sender: TObject);
    procedure NuevoClick(Sender: TObject);
    procedure BotonBorrarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AltaClick(Sender: TObject);
    procedure BajaClick(Sender: TObject);
    procedure ListaRequeridosClick(Sender: TObject);
    procedure SubirClick(Sender: TObject);
    procedure BajarClick(Sender: TObject);
    procedure CargarTodosClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Descripcion_edExit(Sender: TObject);
  private
    { Private declarations }
    FInfoTabla: TInfoTabla;
    function EditaUnCampo( oInfoCampo: TInfoCampo; const lNuevo: Boolean ): Boolean;
    function GetCampo: TInfoCampo;
    procedure Controles;
    procedure ControlesRequeridos;
    procedure DibujaLista;
  public
    { Public declarations }
    property InfoTabla : TInfoTabla Read FInfoTabla Write FInfoTabla;
  end;

var
  EditarTabla: TEditaTabla;
  CamposReq: TCamposRequeridos;

implementation

uses ZetaDialogo,
     ZetaTipoEntidad,
     ZetaTipoEntidadTools,
     ZetaCommonLists,
     ZReportConst,
     FEditaCampos,
     DEditarDiccionario;

{$R *.DFM}

procedure TEditaTabla.FormShow(Sender: TObject);
begin
     with InfoTabla do
     begin
          Self.Caption := Format( 'Editar Entidad %s ( %d )', [ ZetaTipoEntidadTools.ObtieneEntidad( Entidad ), Ord( Entidad ) ] );
          Tabla_ed.Text := Tabla;
          Descripcion_Ed.Text := Descripcion;
          NomCorto_Ed.Text := NombreCorto;
          Palabra_Ed.Text := PalabrasClave;
          Clasificacion_Cb.ItemIndex := Ord( Clasificacion );
          Self.Orden.Valor := Orden;
          ListaRequeridos.Items := Requeridos;
          DibujaLista;
          Nuevo.Enabled := True;
     end;
     ControlesRequeridos;
     Tabla_ed.SetFocus;
end;

procedure TEditaTabla.ListaCamposDblClick(Sender: TObject);
begin
     if EditaUnCampo( GetCampo, False ) then
     begin
          DibujaLista;
     end;
end;

procedure TEditaTabla.Controles;
begin
     with OK do
     begin
          if Not ( Enabled ) then
          begin
               Enabled := True;
               with Cancelar do
               begin
                    Kind := bkCancel;
                    Caption := '&Cancelar';
               end;
          end;
     end;
end;

procedure TEditaTabla.ControlesRequeridos;
var
   lHay: Boolean;
begin
     with ListaRequeridos do
     begin
          lHay := ( Items.Count > 0 );
          Baja.Enabled := lHay;
          Subir.Enabled := lHay and ( ItemIndex > 0 );
          Bajar.Enabled := lHay and ( ItemIndex < ( Items.Count - 1 ) );
     end;
end;

procedure TEditaTabla.CancelarClick(Sender: TObject);
begin
     with Cancelar do
     begin
          Kind := bkClose;
          Caption := '&Salir';
     end;
end;

procedure TEditaTabla.Tabla_EdKeyPress(Sender: TObject; var Key: Char);
begin
     Controles;
end;

procedure TEditaTabla.Clasificacion_CbChange(Sender: TObject);
begin
     Controles;
end;

procedure TEditaTabla.ListaRequeridosClick(Sender: TObject);
begin
     ControlesRequeridos;
end;

function TEditaTabla.EditaUnCampo( oInfoCampo: TInfoCampo; const lNuevo: Boolean ): Boolean;
begin
     with TEditaCampos.Create( Self ) do
     begin
          try
             Nuevo := lNuevo;
             InfoCampo := oInfoCampo;
             ShowModal;
             Result := ( ModalResult = mrOK );
             if Result then
                Controles;
          finally
                 Free;
          end;
     end;
end;

procedure TEditaTabla.NuevoClick(Sender: TObject);
var
   oInfoCampo: TInfoCampo;
begin
     oInfoCampo := TInfoCampo.Create;
     try
        oInfoCampo.Entidad := InfoTabla.Entidad;
        if EditaUnCampo( oInfoCampo, True ) then
        begin
             InfoTabla.AddCampo.Assign( oInfoCampo );
             DibujaLista;
        end;
     finally
            FreeAndNil( oInfoCampo );
     end;
end;

procedure TEditaTabla.BotonBorrarClick(Sender: TObject);
begin
     InfoTabla.Borrar( GetCampo.Campo );
     DibujaLista;
     Controles;
end;

function TEditaTabla.GetCampo: TinfoCampo;
begin
     Result := TInfoCampo( ListaCampos.Objects[ 0, ListaCampos.Row ] );
end;

procedure TEditaTabla.DibujaLista;
var
   i: integer;
begin
     with ListaCampos do
     begin
          with InfoTabla do
          begin
               RowCount := Count + 1;
               Cells[ 0, 0 ] := 'Nombre Del Campo';          //ya que el count comienza en 1 y no en 0.
               Cells[ 1, 0 ] := 'Descripci�n Del Campo';
               if RowCount > 1 then
               begin
                    FixedRows := 1;
                    for i := 0 to ( Count - 1 ) do
                    begin
                         Cells[ 0, i + 1 ] := Campo[ i ].Campo;
                         Cells[ 1, i + 1 ] := Campo[ i ].Descripcion;
                         Objects[ 0, i + 1 ] := Campo[ i ];
                         Objects[ 1, i + 1 ] := Campo[ i ];
                    end;
                    Enabled := True;
               end
               else
                   Enabled := False;
          end;
     end;
     BotonBorrar.Enabled := ( InfoTabla.Count > 0 );
     Edita.Enabled := BotonBorrar.Enabled;
end;

procedure TEditaTabla.OKClick(Sender: TObject);
begin
     with InfoTabla do
     begin
          Tabla := Tabla_ed.Text;
          Descripcion := Descripcion_Ed.Text;
          NombreCorto := NomCorto_Ed.Text;
          PalabrasClave := Palabra_Ed.Text;
          Clasificacion := ZetacommonLists.eClasifiReporte( Clasificacion_Cb.ItemIndex );
          Requeridos.Assign( ListaRequeridos.Items );
          Descargar;
          OK.Enabled := FALSE;
          with Cancelar do
          begin
               Kind := bkClose;
               Caption := '&Salir';
          end;
     end;
end;

procedure TEditaTabla.AltaClick(Sender: TObject);
begin
     CamposReq := TCamposRequeridos.Create( Self );
     try
        with CamposReq do
        begin
             Requeridos := ListaRequeridos.Items;
             InfoTabla := Self.InfoTabla;
             if HayCampos then
             begin
                  ShowModal;
                  if ( ModalResult = mrOk ) then
                     Controles;
             end
             else
                 ZetaDialogo.zError( '� Atenci�n !', 'Ya No Hay Campos Por Designar Como Requeridos', 0 );;
        end;
     finally
            FreeAndNil( CamposReq );
     end;
     ControlesRequeridos;
end;

procedure TEditaTabla.BajaClick(Sender: TObject);
var
   i: integer;
   sCampo : string;
begin
     with ListaRequeridos do
     begin
          sCampo := Items.Strings[ ItemIndex ];
          if ( sCampo = ( InfoTabla.Tabla + '.' + K_COLABORA_CODIGO ) ) OR
             ( sCampo = ( InfoTabla.Tabla + '.' + K_EXPEDIENTE_CODIGO ) ) then
          begin
               for i := 0 to ( Items.Count - 1 ) do
               begin
                    sCampo := Items.Strings[ i ];
                    if ( sCampo = K_COLABORA_NOMBRE ) OR
                       ( sCampo = Q_PRETTY_EXP ) then
                    begin
                         Items.Delete( i );
                         Break;
                    end;
               end;
          end;
          Items.Delete( ItemIndex );
     end;
     Baja.Enabled := False;
     ControlesRequeridos;
     Controles;
end;

procedure TEditaTabla.SubirClick(Sender: TObject);
var
   i: Integer;
begin
     with ListaRequeridos do
     begin
          i := ItemIndex;
          if ( i > 0 ) then
          begin
               Items.Exchange( i, ( i - 1 ) );
          end;
     end;
     ControlesRequeridos;
end;

procedure TEditaTabla.BajarClick(Sender: TObject);
var
   i: Integer;
begin
     with ListaRequeridos do
     begin
          i := ItemIndex;
          with Items do
          begin
               if ( i < ( Count - 1 ) ) then
               begin
                    Items.Exchange( i, ( i + 1 ) );
               end;
          end;
     end;
     ControlesRequeridos;
end;

procedure TEditaTabla.CargarTodosClick(Sender: TObject);
begin
     with InfoTabla do
     begin
          Tabla := Tabla_ed.Text;
     end;
     if dmDiccionario.CargarTodosLosCampos then
     begin
          DibujaLista;
          Controles;
     end;
end;

procedure TEditaTabla.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     case Key of
          VK_RETURN:
          begin
               if ( not ( ActiveControl is TButton ) ) then
               begin
                    Key := 0;
                    ListaCamposDblClick( Sender );
               end;
          end;
     end;

end;

procedure TEditaTabla.Descripcion_edExit(Sender: TObject);
begin
     Palabra_ed.Text := Descripcion_ed.Text;
     NomCorto_Ed.Text := Descripcion_ed.Text;
end;

end.
