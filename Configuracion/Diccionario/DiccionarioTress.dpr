program DiccionarioTress;

uses
  Forms,
  FEditarDiccionario in 'FEditarDiccionario.pas' {EditarDiccionario},
  DEditarDiccionario in 'DEditarDiccionario.pas' {dmDiccionario: TDataModule},
  FEditarEntidades in 'FEditarEntidades.pas' {EditarEntidades},
  FEditaTabla in 'FEditaTabla.pas' {EditaTabla},
  FEditaCampos in 'FEditaCampos.pas' {EditaCampos},
  FCamposRequeridos in 'FCamposRequeridos.pas' {CamposRequeridos};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Edici�n Diccionario De Datos';
  Application.CreateForm(TEditarDiccionario, EditarDiccionario);
  Application.Run;
end.
