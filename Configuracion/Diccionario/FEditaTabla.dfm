object EditaTabla: TEditaTabla
  Left = 297
  Top = 173
  Width = 551
  Height = 519
  Caption = 'Editar Entidad '
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox2: TGroupBox
    Left = 0
    Top = 150
    Width = 543
    Height = 289
    Align = alClient
    Caption = ' Campos Pertenecientes a la Entidad: '
    TabOrder = 0
    object ListaCampos: TStringGrid
      Left = 2
      Top = 15
      Width = 509
      Height = 272
      Align = alClient
      ColCount = 2
      DefaultColWidth = 259
      DefaultRowHeight = 20
      FixedCols = 0
      RowCount = 6
      GridLineWidth = 0
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRowSelect]
      ScrollBars = ssVertical
      TabOrder = 0
      OnDblClick = ListaCamposDblClick
      ColWidths = (
        250
        309)
    end
    object PanelBotones: TPanel
      Left = 511
      Top = 15
      Width = 30
      Height = 272
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object Nuevo: TSpeedButton
        Left = 3
        Top = 41
        Width = 25
        Height = 25
        Hint = 'Agregar Nuevo Campo'
        Enabled = False
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
          333B3BB3888BB8888BB333B0000000000B333330FFFFFFFF08333330FFFFFFFF
          08333330FFFFFFFF08333330FFFFFFFF0833BBB0FFFFFFFF0BB33BB0FFFFFFFF
          0BBB3330FFFF000003333330FFFF0FF033333330FFFF0F0B33333330FFFF003B
          B33333B000000333BB333BB3333BB3333BB3B333333B3333333B}
        ParentShowHint = False
        ShowHint = True
        OnClick = NuevoClick
      end
      object BotonBorrar: TSpeedButton
        Left = 3
        Top = 68
        Width = 25
        Height = 25
        Hint = 'Borrar Campo'
        Enabled = False
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
          55558888888585599958555555555550305555555550055BB0555555550FB000
          0055555550FB0BF0F05555550FBFBF0FB05555550BFBF0FB005555500FBFBFB0
          B055550E0BFBFB00B05550EEE0BFB0B0B055000EEE0BFBF0B0550000EEE00000
          B05500000E055550705550000055555505555500055555550555}
        ParentShowHint = False
        ShowHint = True
        OnClick = BotonBorrarClick
      end
      object Edita: TSpeedButton
        Left = 3
        Top = 95
        Width = 25
        Height = 25
        Hint = 'Editar Campo'
        Enabled = False
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
          00003333330FFFFFFFF000300000FF0F00F0E00BFBFB0FFFFFF0E0BFBF000FFF
          F0F0E0FBFBFBF0F00FF0E0BFBF00000B0FF0E0FBFBFBFBF0FFF0E0BF0000000F
          FFF0000BFB00B0FF00F03330000B0FFFFFF0333330B0FFFF000033330B0FF00F
          0FF03330B00FFFFF0F033309030FFFFF00333330330000000333}
        ParentShowHint = False
        ShowHint = True
        OnClick = ListaCamposDblClick
      end
      object CargarTodos: TSpeedButton
        Left = 3
        Top = 1
        Width = 25
        Height = 25
        Hint = 'Agregar Todos Los Campos Pendientes De La Tabla'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
          5555555555FFFFFFFFFF5555500000000005555557777777777F55550BFBFBFB
          FB0555557F555555557F55500FBFBFBFBF0555577F555555557F550B0BFBFBFB
          FB05557F7F555555557F500F0FBFBFBFBF05577F7F555555557F0B0B0BFBFBFB
          FB057F7F7F555555557F0F0F0FBFBFBFBF057F7F7FFFFFFFFF750B0B00000000
          00557F7F7777777777550F0FB0FBFB0F05557F7FF75FFF7575550B0007000070
          55557F777577775755550FB0FBFB0F0555557FF75FFF75755555000700007055
          5555777577775755555550FBFB0555555555575FFF7555555555570000755555
          5555557777555555555555555555555555555555555555555555}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = CargarTodosClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 439
    Width = 543
    Height = 36
    Align = alBottom
    TabOrder = 1
    object OK: TBitBtn
      Left = 377
      Top = 6
      Width = 76
      Height = 25
      Caption = '&OK'
      Enabled = False
      ModalResult = 1
      TabOrder = 0
      OnClick = OKClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object Cancelar: TBitBtn
      Left = 462
      Top = 6
      Width = 75
      Height = 25
      Caption = '&Salir'
      ModalResult = 2
      TabOrder = 1
      OnClick = CancelarClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
        F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
        000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
        338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
        45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
        3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
        F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
        000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
        338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
        4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
        8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
        333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
        0000}
      NumGlyphs = 2
    end
  end
  object PanelEntidad: TPanel
    Left = 0
    Top = 0
    Width = 543
    Height = 150
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object lbTabla: TLabel
      Left = 56
      Top = 12
      Width = 30
      Height = 13
      Caption = 'Tabla:'
    end
    object Label1: TLabel
      Left = 27
      Top = 34
      Width = 59
      Height = 13
      Caption = 'Descripción:'
    end
    object Label3: TLabel
      Left = 12
      Top = 56
      Width = 74
      Height = 13
      Caption = 'Palabras Clave:'
    end
    object Label2: TLabel
      Left = 18
      Top = 78
      Width = 68
      Height = 13
      Caption = 'Nombre Corto:'
    end
    object Label4: TLabel
      Left = 24
      Top = 100
      Width = 62
      Height = 13
      Caption = 'Clasificación:'
    end
    object Label6: TLabel
      Left = 54
      Top = 122
      Width = 32
      Height = 13
      Alignment = taRightJustify
      Caption = 'Orden:'
    end
    object GroupBox1: TGroupBox
      Left = 325
      Top = 0
      Width = 218
      Height = 150
      Align = alRight
      Caption = ' Campos Requeridos: '
      TabOrder = 6
      object PanelRequeridos: TPanel
        Left = 185
        Top = 15
        Width = 31
        Height = 133
        Align = alRight
        TabOrder = 0
        object Alta: TSpeedButton
          Left = 3
          Top = 3
          Width = 25
          Height = 25
          Hint = 'Alta de Campo Requerido'
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
            333B3BB3888BB8888BB333B0000000000B333330FFFFFFFF08333330FFFFFFFF
            08333330FFFFFFFF08333330FFFFFFFF0833BBB0FFFFFFFF0BB33BB0FFFFFFFF
            0BBB3330FFFF000003333330FFFF0FF033333330FFFF0F0B33333330FFFF003B
            B33333B000000333BB333BB3333BB3333BB3B333333B3333333B}
          ParentShowHint = False
          ShowHint = True
          OnClick = AltaClick
        end
        object Baja: TSpeedButton
          Left = 3
          Top = 29
          Width = 25
          Height = 25
          Hint = 'Borrar Campo Requerido'
          Enabled = False
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            55558888888585599958555555555550305555555550055BB0555555550FB000
            0055555550FB0BF0F05555550FBFBF0FB05555550BFBF0FB005555500FBFBFB0
            B055550E0BFBFB00B05550EEE0BFB0B0B055000EEE0BFBF0B0550000EEE00000
            B05500000E055550705550000055555505555500055555550555}
          ParentShowHint = False
          ShowHint = True
          OnClick = BajaClick
        end
        object Subir: TZetaSmartListsButton
          Left = 3
          Top = 77
          Width = 25
          Height = 25
          Anchors = [akLeft, akBottom]
          Enabled = False
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000333
            3333333333090333333333333309033333333333330903333333333333090333
            3333333333090333333333300009000033333330999999903333333309999903
            3333333309999903333333333099903333333333309990333333333333090333
            3333333333090333333333333330333333333333333033333333}
          OnClick = SubirClick
          Tipo = bsSubir
        end
        object Bajar: TZetaSmartListsButton
          Left = 3
          Top = 104
          Width = 25
          Height = 25
          Anchors = [akLeft, akBottom]
          Enabled = False
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333303333
            3333333333303333333333333309033333333333330903333333333330999033
            3333333330999033333333330999990333333333099999033333333099999990
            3333333000090000333333333309033333333333330903333333333333090333
            3333333333090333333333333309033333333333330003333333}
          OnClick = BajarClick
          Tipo = bsBajar
        end
      end
      object ListaRequeridos: TListBox
        Left = 2
        Top = 15
        Width = 183
        Height = 133
        Align = alClient
        ItemHeight = 13
        TabOrder = 1
        OnClick = ListaRequeridosClick
      end
    end
    object Tabla_Ed: TEdit
      Left = 88
      Top = 8
      Width = 143
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 10
      TabOrder = 0
      OnKeyPress = Tabla_EdKeyPress
    end
    object Palabra_Ed: TEdit
      Left = 88
      Top = 52
      Width = 225
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 30
      TabOrder = 2
      OnKeyPress = Tabla_EdKeyPress
    end
    object NomCorto_Ed: TEdit
      Left = 88
      Top = 74
      Width = 225
      Height = 21
      MaxLength = 30
      TabOrder = 3
      OnKeyPress = Tabla_EdKeyPress
    end
    object Clasificacion_Cb: TZetaKeyCombo
      Left = 88
      Top = 96
      Width = 145
      Height = 21
      Style = csDropDownList
      DropDownCount = 50
      ItemHeight = 13
      TabOrder = 4
      OnChange = Clasificacion_CbChange
      ListaFija = lfClasifiReporte
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
    end
    object Orden: TZetaNumero
      Left = 88
      Top = 118
      Width = 33
      Height = 21
      Color = clMenu
      Enabled = False
      Mascara = mnDias
      ReadOnly = True
      TabOrder = 5
      Text = '0'
    end
    object Descripcion_ed: TEdit
      Left = 88
      Top = 30
      Width = 225
      Height = 21
      MaxLength = 30
      TabOrder = 1
      OnExit = Descripcion_edExit
      OnKeyPress = Tabla_EdKeyPress
    end
  end
end
