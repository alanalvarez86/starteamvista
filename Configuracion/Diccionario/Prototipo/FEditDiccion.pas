unit FEditDiccion;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, DBCtrls, Db, StdCtrls, Mask, DBTables, Buttons, Grids, DBGrids,
  CheckLst, ComCtrls, ZetaKeyCombo,
  ZetaTipoEntidad;

type
  TEditDiccion = class(TForm)
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    Database1: TDatabase;
    tblDiccion: TTable;
    tblDiccionDI_CLASIFI: TSmallintField;
    tblDiccionDI_NOMBRE: TStringField;
    tblDiccionDI_TITULO: TStringField;
    tblDiccionDI_CALC: TSmallintField;
    tblDiccionDI_ANCHO: TSmallintField;
    tblDiccionDI_MASCARA: TStringField;
    tblDiccionDI_TFIELD: TSmallintField;
    tblDiccionDI_ORDEN: TStringField;
    tblDiccionDI_REQUIER: TStringField;
    tblDiccionDI_TRANGO: TSmallintField;
    tblDiccionDI_NUMERO: TSmallintField;
    tblDiccionDI_VALORAC: TStringField;
    tblDiccionDI_RANGOAC: TSmallintField;
    tblDiccionDI_CLAVES: TStringField;
    tblDiccionDI_TCORTO: TStringField;
    tblDiccionDI_CONFI: TStringField;
    dsDiccion: TDataSource;
    Button1: TButton;
    Edit1: TEdit;
    Button2: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Splitter1: TSplitter;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label5: TLabel;
    DI_NOMBRE: TDBEdit;
    DI_TITULO: TDBEdit;
    DI_ANCHO: TDBEdit;
    DI_MASCARA: TDBEdit;
    DI_CLAVES: TDBEdit;
    DI_TCORTO: TDBEdit;
    DI_CLASIFI2: TDBEdit;
    DBNavigator1: TDBNavigator;
    DI_TIFIELD: TDBRadioGroup;
    DI_NUMERO: TDBEdit;
    DI_VALORAC: TDBEdit;
    DI_TRANGO: TComboBox;
    TipoFiltro: TRadioGroup;
    DI_RANGOAC: TComboBox;
    DI_TRANGO2: TDBEdit;
    DI_RANGOAC2: TDBEdit;
    DI_CLASIFI: TComboBox;
    DBNavigator2: TDBNavigator;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    LbTablas: TListBox;
    DBEdit2: TDBEdit;
    Label10: TLabel;
    Label11: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Label12: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    DBEdit7: TDBEdit;
    Label16: TLabel;
    BitBtn5: TBitBtn;
    DBNavigator3: TDBNavigator;
    BitBtn6: TBitBtn;
    cbClasifiDef: TComboBox;
    RadioGroup1: TRadioGroup;
    ComboBox1: TComboBox;
    procedure tblDiccionNewRecord(DataSet: TDataSet);
    procedure tblDiccionAfterPost(DataSet: TDataSet);
    procedure tblDiccionDI_TITULOChange(Sender: TField);
    procedure tblDiccionDI_TFIELDChange(Sender: TField);
    procedure TipoFiltroClick(Sender: TObject);
    procedure DI_CLASIFIClick(Sender: TObject);
    procedure tblDiccionDI_NOMBREChange(Sender: TField);
    procedure BitBtn1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure dsDiccionDataChange(Sender: TObject; Field: TField);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure DI_RANGOACClick(Sender: TObject);
    procedure DI_TRANGOClick(Sender: TObject);
  private
    { Private declarations }
    FOldClasifi : Integer;
    procedure AgregaTabla(eTipo: TipoEntidad);
  public
    { Public declarations }
  end;

var
  EditDiccion: TEditDiccion;

implementation
{$R *.DFM}

function StrTransAll( sOrigen, sSearch, sReplace: String ): String;
var
   iPos, iLast, iSearch: Integer;
   sCopia : String;
begin
     Result  := '';
     sSearch := AnsiUpperCase( sSearch );
     iSearch := Length( sSearch );
     sCopia  := AnsiUpperCase( sOrigen );
     iPos    := AnsiPos( sSearch, sCopia );
     while ( iPos > 0 ) do
     begin
          Result := Result + Copy( sOrigen, 1, iPos-1 ) + sReplace;
          iLast := iPos + iSearch;
          sCopia := Copy( sCopia, iLast, MAXINT );
          sOrigen := Copy( sOrigen, iLast, MAXINT );
          iPos := AnsiPos( sSearch, sCopia );
     end;
     Result := Result + sOrigen;
end;

function QuitaAcentos( sValue : String ) : String;
const
     LETRA_A_MINUSCULA = Ord( '�' );
     LETRA_E_MINUSCULA = Ord( '�' );
     LETRA_I_MINUSCULA = Ord( '�' );
     LETRA_O_MINUSCULA = Ord( '�' );
     LETRA_U_MINUSCULA = Ord( '�' );
     LETRA_ENE_MINUSCULA = Ord( '�' );
     LETRA_ENE_MAYUSCULA = Ord( '�' );
     LETRA_U_DIERESIS = Ord( '�' );
     LETRA_ADMIRACION = Ord( '�' );
     LETRA_PREGUNTA = Ord( '�' );
var
   i, iLen: Integer;
   cValue: Char;
begin
     iLen  := Length( sValue );
     Result := '';
     for i := 1 to iLen do
     begin
        cValue := sValue[ i ];
        case Ord( cValue ) of
               LETRA_A_MINUSCULA: cValue := 'a';
               LETRA_E_MINUSCULA: cValue := 'e';
               LETRA_I_MINUSCULA: cValue := 'i';
               LETRA_O_MINUSCULA: cValue := 'o';
               LETRA_U_MINUSCULA: cValue := 'u';
               LETRA_ENE_MINUSCULA: cValue := 'n';
               LETRA_ENE_MAYUSCULA: cValue := 'N';
               LETRA_U_DIERESIS: cValue := 'u';
               LETRA_ADMIRACION: cValue := '!';
               LETRA_PREGUNTA: cValue := '?';
        end;
        Result := Result + cValue;
     end;
end;

function QuitaPalabras( sValue : String ) : String;
begin
     Result := sValue;
     Result := UpperCase(QuitaAcentos(Result));
     Result := StrTransAll(Result, 'DE ', '' );
     Result := StrTransAll(Result, 'CON ', '' );
     Result := StrTransAll(Result, 'LAS ', '' );
     Result := StrTransAll(Result, 'QUE ', '' );
     Result := StrTransAll(Result, 'PARA ', '' );
     Result := StrTransAll(Result, 'DEL ', '' );
     Result := StrTransAll(Result, 'EL ', '' );
     Result := StrTransAll(Result, 'LOS ', '' );
     Result := StrTransAll(Result, 'LA ', '' );
     Result := StrTransAll(Result, 'EN ', '' );
     Result := StrTransAll(Result, '?', '' );
     Result := StrTransAll(Result, '/', '' );
     Result := StrTransAll(Result, '#', '' );
     Result := StrTransAll(Result, ':', '' );
end;


procedure TEditDiccion.tblDiccionNewRecord(DataSet: TDataSet);
begin
    with tblDiccion do
    begin
        FieldByName( 'DI_ORDEN' ).AsString := 'S';
        FieldByName( 'DI_CLASIFI' ).AsInteger := FOldClasifi;
        FieldByName( 'DI_ANCHO' ).AsInteger := 10;
        FieldByName( 'DI_TFIELD' ).AsInteger := 2;
        FieldByName( 'DI_CONFI' ).AsString := 'N';
        FieldByName( 'DI_NOMBRE' ).FocusControl;
    end;
end;

procedure TEditDiccion.tblDiccionAfterPost(DataSet: TDataSet);
begin
    with tblDiccion do
    begin
        FOldClasifi := FieldByName( 'DI_CLASIFI' ).AsInteger;
    end;
end;

procedure TEditDiccion.tblDiccionDI_TITULOChange(Sender: TField);
begin
    with tblDiccion do
    begin
        FieldByName( 'DI_TCORTO' ).AsString := FieldByName( 'DI_TITULO' ).AsString;
        FieldByName( 'DI_CLAVES' ).AsString := QuitaPalabras(FieldByName( 'DI_TITULO' ).AsString );
    end;
end;

procedure TEditDiccion.tblDiccionDI_TFIELDChange(Sender: TField);
var
    nAncho,nTRango,nNumero,nRangoActivo : integer;
    sMascara,sValorActivo : String;
begin
     sMascara := '';
     sValorActivo := '';
     nTRango := 0;
     nNumero := 0;
     //nRangoActivo := 0;
     TipoFiltro.Enabled := FALSE;

     DI_TRANGO.Enabled := FALSE;
     DI_NUMERO.Enabled := FALSE;
     DI_VALORAC.Enabled := FALSE;
     DI_RANGOAC.Enabled := FALSE;

    with tblDiccion do
    begin
        case FieldByName( 'DI_TFIELD' ).AsInteger of
          0 : begin
                   nAncho := 1;
                   nTRango := 4;
                   nNumero := 60;
                   sValorActivo := 'S';
                   nRangoActivo := 2;
              end;
          1 : begin
                   nAncho := 15;
                   sMascara := '#,0.00;-#,0.00';
                   nRangoActivo := 2;
              end;
          2 : begin
                   TipoFiltro.Enabled := TRUE;
                   nAncho := 7;
                   sMascara := '#0;-#0';
                   nRangoActivo := 2;
              end;
          3 : begin
                   nAncho := 9;
                   sMascara := 'dd/mmm/yy';
                   nTRango := 3;
                   nRangoActivo := 2;
              end;
          else
              begin
                   TipoFiltro.Enabled := TRUE;
                   nAncho := 30;
                   nRangoActivo := 2;
              end;
        end;
        FieldByName( 'DI_ANCHO' ).AsInteger := nAncho;
        FieldByName( 'DI_MASCARA' ).AsString := sMascara;
        FieldByName( 'DI_TRANGO' ).AsInteger := nTRango;
        FieldByName( 'DI_NUMERO' ).AsInteger := nNumero;
        FieldByName( 'DI_VALORAC' ).AsString := sValorActivo;
        FieldByName( 'DI_RANGOAC' ).AsInteger := nRangoActivo;
    end;

end;

procedure TEditDiccion.TipoFiltroClick(Sender: TObject);
 var nTRango,nNumero,nRangoActivo, iAncho : integer;
begin
     nTRango := 0;
     nNumero := 0;
     nRangoActivo := 0;
     iAncho := tblDiccion.FieldByName('DI_ANCHO').AsInteger;
     DI_TRANGO.Enabled := TRUE;
     DI_NUMERO.Enabled := TRUE;
     DI_VALORAC.Enabled := TRUE;
     DI_RANGOAC.Enabled := TRUE;
     case TipoFiltro.ItemIndex of
          0:
          begin
               DI_TRANGO.Style := csDropDownList;
               DI_RANGOAC.Style := csSimple;
               nTRango := 1;
               nNumero := DI_TRANGO.ItemIndex;
               nRangoActivo :=0;
               iAncho := 6;
               DI_NUMERO.Enabled := FALSE;
               DI_VALORAC.Enabled := FALSE;
               DI_RANGOAC.Enabled := FALSE;
          end;
          1:
          begin
               DI_TRANGO.Style := csSimple;
               DI_RANGOAC.Style := csDropDownList;
               nTRango := 2;
               iAncho := 2;
               nNumero := DI_RANGOAC.ItemIndex;
               nRangoActivo :=0;

               DI_TRANGO.Enabled := FALSE;
               DI_NUMERO.Enabled := FALSE;
               DI_VALORAC.Enabled := FALSE;
          end;
          2:
          begin
               DI_TRANGO.Style := csSimple;
               DI_RANGOAC.Style := csSimple;
               nTRango := 0;
               nNumero := 0;
               nRangoActivo := 2;
               DI_TRANGO.Enabled := FALSE;
               DI_NUMERO.Enabled := FALSE;
               DI_VALORAC.Enabled := FALSE;
               DI_RANGOAC.Enabled := FALSE;
          end;
     end;
     with tblDiccion do
     begin
          if State = dsBrowse then Edit;
          FieldByName( 'DI_TRANGO' ).AsInteger := nTRango;
          FieldByName( 'DI_NUMERO' ).AsInteger := nNumero;
          FieldByName( 'DI_RANGOAC' ).AsInteger := nRangoActivo;
     end;

end;

procedure TEditDiccion.DI_CLASIFIClick(Sender: TObject);
begin
     with tblDiccion do
     begin
          if State= dsBrowse then Edit;
          FieldByName( 'DI_CLASIFI' ).AsInteger := DI_CLASIFI.ItemIndex;
     end;
end;

procedure TEditDiccion.tblDiccionDI_NOMBREChange(Sender: TField);
 var sNombre, sTitulo,  sClaves ,  sCorto : string;
     iAncho, iTField, iNumero  : integer;

 procedure Niveles(const iNivel : integer);
 begin
      sTitulo := Format('Nivel #%s',[iNivel]);
      iAncho := 6;
      iTField := 4;
      sClaves := Format('NIVEL %s',[iNivel]);
      sCorto := Format('Nivel #%s', [iNivel]);
      iNumero := 40+iNivel;
      DI_TRANGO.ItemIndex := 40+iNivel;
 end;
begin
     iAncho := 0;
     iTField := 0;
     iNumero := 0;
     sTitulo := '';
     with tblDiccion do
     begin
          sNombre := FieldByName( 'DI_NOMBRE' ).AsString;
          if sNombre = 'CB_CODIGO' then
          begin
               sTitulo := 'N�mero de Empleado';
               iAncho := 7;
               iTField := 2;
               sClaves := 'NUMERO EMPLEADO';
               sCorto := 'N�mero';
               iNumero := 10;
               DI_TRANGO.ItemIndex := 10;
          end
          else if sNombre = 'US_CODIGO' then
          begin
               sTitulo := 'Usuario que Modific�';
               iAncho := 7;
               iTField := 2;
               sClaves := 'USUARIO MODIFICO';
               sCorto := 'Usuario';
               iNumero := 85;
               DI_TRANGO.ItemIndex := 85;
          end
          else if sNombre = 'CB_NIVEL1' then Niveles(1)
          else if sNombre = 'CB_NIVEL2' then Niveles(2)
          else if sNombre = 'CB_NIVEL3' then Niveles(3)
          else if sNombre = 'CB_NIVEL4' then Niveles(4)
          else if sNombre = 'CB_NIVEL5' then Niveles(5)
          else if sNombre = 'CB_NIVEL6' then Niveles(6)
          else if sNombre = 'CB_NIVEL7' then Niveles(7)
          else if sNombre = 'CB_NIVEL8' then Niveles(8)
          else if sNombre = 'CB_NIVEL9' then Niveles(9)
          {else if sNombre = 'CB_PUESTO' then
          begin
               sTitulo := 'C�digo de Tabla';
               iAncho := 6;
               iTField := 4;
               sClaves := 'CODIGO TABLA';
               sCorto := 'C�digo';
               iNUmero := FieldByName('DI_CLASIFI').AsInteger;
               DI_TRANGO.ItemIndex := iNumero;
          end
          else if sNombre = 'CB_PUESTO' then
          begin
               sTitulo := 'C�digo de Tabla';
               iAncho := 6;
               iTField := 4;
               sClaves := 'CODIGO TABLA';
               sCorto := 'C�digo';
               iNUmero := FieldByName('DI_CLASIFI').AsInteger;
               DI_TRANGO.ItemIndex := iNumero;
          end
          else if sNombre = 'CB_TURNO' then
          begin
               sTitulo := 'C�digo de Tabla';
               iAncho := 6;
               iTField := 4;
               sClaves := 'CODIGO TABLA';
               sCorto := 'C�digo';
               iNUmero := FieldByName('DI_CLASIFI').AsInteger;
               DI_TRANGO.ItemIndex := iNumero;
          end}
          else if sNombre = ['TB_CODIGO','FP_CODIGO'] then
          begin
               sTitulo := 'C�digo de Tabla';
               iAncho := 6;
               iTField := 4;
               sClaves := 'CODIGO TABLA';
               sCorto := 'C�digo';
               iNUmero := FieldByName('DI_CLASIFI').AsInteger;
               DI_TRANGO.ItemIndex := iNumero;
          end
          else if sNombre in ['TB_TEXTO'] then
          begin
               sTitulo := 'Texto de Uso General';
               iAncho := 30;
               iTField := 4;
               sClaves := 'TEXTO USO GENERAL';
               sCorto := 'Texto';
          end
          else if sNombre = 'TB_INGLES' then
          begin
               sTitulo := 'Descripci�n en Ingl�s';
               iAncho := 30;
               iTField := 4;
               sClaves := 'DESCRIPCION INGLES';
               sCorto := 'Ingl�s';
          end
          else if sNombre = 'TB_ELEMENT' then
          begin
               sTitulo := 'Descripci�n';
               iAncho := 30;
               iTField := 4;
               sClaves := 'DESCRIPCION';
               sCorto := 'Descripci�n';
          end

          else if sNombre = 'TB_NUMERO' then
          begin
               sTitulo := 'N�mero de Uso General';
               iAncho := 7;
               iTField := 1;
               sClaves := 'NUMERO USO GENERAL';
               sCorto := 'N�mero';
          end;
          if sTitulo <> '' then
          begin
               FieldByName('DI_TITULO').AsString := sTitulo;
               FieldByName('DI_ANCHO').AsInteger := iAncho;
               FieldByName('DI_TFIELD').AsInteger := iTField;
               FieldByName('DI_NUMERO').AsInteger := iNumero;
               FieldByName('DI_CLAVES').AsString :=  sClaves;
               FieldByName('DI_TCORTO').AsString := sCorto;
          end;
     end;
end;


procedure TEditDiccion.BitBtn1Click(Sender: TObject);
begin
     tblDiccion.Append;
end;

procedure TEditDiccion.Button1Click(Sender: TObject);
begin
     pageControl1.VISIBLE := NOT PageControl1.VISIBLE;
end;

procedure TEditDiccion.BitBtn2Click(Sender: TObject);
begin
     tblDiccion.Prior;
end;

procedure TEditDiccion.BitBtn3Click(Sender: TObject);
begin
     tblDiccion.Next;
end;

procedure TEditDiccion.dsDiccionDataChange(Sender: TObject; Field: TField);
begin
     with tblDiccion do
          if State = dsBrowse then
          begin
               DI_TRANGO.ItemIndex := 0;
               DI_RANGOAC.ItemIndex := 0;
               TipoFiltro.OnClick := NIL;
               TipoFiltro.ItemIndex := 2;
               case fieldByName('DI_TRANGO').AsInteger of
                    0: Label5.Caption := 'RANGO ABIERTO';
                    1:
                    begin
                         DI_TRANGO.ItemIndex := fieldByName('DI_NUMERO').AsInteger;
                         Label5.Caption := 'LOOKUP';
                         TipoFiltro.ItemIndex := 0;
                    end;
                    2:
                    begin
                         DI_RANGOAC.ItemIndex := FieldByName('DI_NUMERO').AsInteger;
                         Label5.Caption := 'LISTA FIJA';
                         TipoFiltro.ItemIndex := 1;
                    end;
               end;
               TipoFiltro.OnClick := TipoFiltroClick;
          end;
end;

procedure TEditDiccion.Button2Click(Sender: TObject);
begin
     Database1.Connected := FALSE;
     Database1.AliasName := Edit1.Text;
     Database1.Connected := TRUE;
     tblDiccion.Active := TRUE;
end;

procedure LlenaLista(Lista:TStrings);
 var i: Integer;
begin
     with Lista do
     begin
          try
             BeginUpdate;
             Clear;
             for i := 0 to Ord( High( TipoEntidad ) ) do
                 Add( aTipoEntidad[ TipoEntidad(i) ] );
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TEditDiccion.FormCreate(Sender: TObject);
begin
     BitBtn6.Caption := '3.- Mostrar Tablas por '+CHR(13)+CHR(10)+
                        'clasificaci�n y ordenadas';
     LlenaLista(LbTablas.Items);
     LlenaLista(DI_CLASIFI.Items);
     LlenaLista(DI_TRANGO.Items);
end;

procedure TEditDiccion.BitBtn4Click(Sender: TObject);
begin
     with LbTablas do
          AgregaTabla(TipoEntidad(ItemIndex));
end;
procedure TEditDiccion.AgregaTabla(eTipo: TipoEntidad);
begin
     with tblDiccion do
     begin
          Insert;
          FieldByName('DI_CLASIFI').AsInteger := -1;
          FieldByName('DI_NOMBRE').FocusControl;
          FieldByName('DI_TITULO').AsString := aTipoEntidad[eTipo];
          FieldByName('DI_CLAVES').AsString := UpperCase(aTipoEntidad[eTipo]);
          FieldByName('DI_TCORTO').AsString := aTipoEntidad[eTipo];
          FieldByName('DI_CALC').AsInteger := ord(eTipo); //Entidad
          FieldByName('DI_ANCHO').AsInteger := cbClasifiDef.ItemIndex; //Clasificacion
          Combobox1.ItemIndex := cbClasifiDef.ItemIndex;
          FieldByName('DI_TFIELD').AsInteger := 0; //Posicion
     end
end;

procedure TEditDiccion.BitBtn5Click(Sender: TObject);
begin
     tblDiccion.FieldByName('DI_ANCHO').AsInteger := ComboBox1.ItemIndex; //Clasificacion
     tblDiccion.Post;
end;

procedure TEditDiccion.BitBtn6Click(Sender: TObject);
begin
     with tblDiccion do
     begin
          //Active := FALSE;
          IndexFieldNames := 'DI_CLASIFI;DI_ANCHO;DI_TFIELD';
          First;
          //Active := TRUE;
     end;
end;

procedure TEditDiccion.RadioGroup1Click(Sender: TObject);
begin
     with tblDiccion do
          case RadioGroup1.ItemIndex of
               0: Filtered :=FALSE;
               1:
               begin
                    Filter := 'DI_CLASIFI = -1';
                    Filtered :=TRUE;
                    IndexFieldNames := 'DI_CLASIFI;DI_ANCHO;DI_TFIELD';
                    First;
               end;
               2:
               begin
                    Filter := 'DI_CLASIFI <> -1';
                    Filtered :=TRUE;
                    IndexFieldNames := 'DI_CLASIFI;DI_NOMBRE';
                    First;
               end;
     end;
end;

procedure TEditDiccion.ComboBox1Change(Sender: TObject);
begin
     with tblDiccion do
     begin
          if not( State in [dsInsert,dsEdit] ) then
             Edit;
          FieldByName('DI_ANCHO').AsInteger := ComboBox1.ItemIndex; //Clasificacion
     end;
end;

procedure TEditDiccion.DI_RANGOACClick(Sender: TObject);
begin
     with tblDiccion do
     begin
          if not( State in [dsInsert,dsEdit] ) then
             Edit;
          FieldByName('DI_NUMERO').AsInteger := DI_RANGOAC.ItemIndex;
     end;

end;

procedure TEditDiccion.DI_TRANGOClick(Sender: TObject);
begin
     with tblDiccion do
     begin
          if not( State in [dsInsert,dsEdit] ) then
             Edit;
          FieldByName('DI_NUMERO').AsInteger := DI_TRANGO.ItemIndex;
     end;

end;

END.


