object TransferirEmpleados: TTransferirEmpleados
  Left = 201
  Top = 161
  AutoScroll = False
  Caption = 'Transferir Empleados De Una Empresa Hacia Otra'
  ClientHeight = 280
  ClientWidth = 447
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Wizard: TZetaWizard
    Left = 0
    Top = 227
    Width = 447
    Height = 34
    Align = alBottom
    TabOrder = 0
    AlEjecutar = WizardAlEjecutar
    AlCancelar = WizardAlCancelar
    AfterMove = WizardAfterMove
    BeforeMove = WizardBeforeMove
    PageControl = PageControl
    Reejecutar = True
    object Anterior: TZetaWizardButton
      Left = 5
      Top = 5
      Width = 86
      Height = 25
      Hint = 'Regresar al Paso Anterior'
      Caption = '&Anterior'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333333333333333333333333333333333333333333333
        3333333333333FF3333333333333003333333333333F77F33333333333009033
        333333333F7737F333333333009990333333333F773337FFFFFF330099999000
        00003F773333377777770099999999999990773FF33333FFFFF7330099999000
        000033773FF33777777733330099903333333333773FF7F33333333333009033
        33333333337737F3333333333333003333333333333377333333333333333333
        3333333333333333333333333333333333333333333333333333333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 2
      Tipo = bwAnterior
      Wizard = Wizard
    end
    object Siguiente: TZetaWizardButton
      Left = 95
      Top = 5
      Width = 86
      Height = 25
      Hint = 'Avanzar Hacia El Siguiente Paso'
      Caption = '&Siguiente'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333333333333333333333333333333333333333333333
        3333333333333333333333333333333333333333333FF3333333333333003333
        3333333333773FF3333333333309003333333333337F773FF333333333099900
        33333FFFFF7F33773FF30000000999990033777777733333773F099999999999
        99007FFFFFFF33333F7700000009999900337777777F333F7733333333099900
        33333333337F3F77333333333309003333333333337F77333333333333003333
        3333333333773333333333333333333333333333333333333333333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 2
      Tipo = bwSiguiente
      Wizard = Wizard
    end
    object Transferir: TZetaWizardButton
      Left = 254
      Top = 5
      Width = 86
      Height = 25
      Hint = 'Efectuar Comparaci�n'
      Caption = '&Transferir'
      Enabled = False
      ModalResult = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
      Tipo = bwEjecutar
      Wizard = Wizard
    end
    object Cancelar: TZetaWizardButton
      Left = 344
      Top = 5
      Width = 86
      Height = 25
      Hint = 'Cancelar y Salir'
      Caption = '&Cancelar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Kind = bkCancel
      Tipo = bwCancelar
      Wizard = Wizard
    end
  end
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 447
    Height = 227
    ActivePage = Parametros
    Align = alClient
    TabOrder = 1
    object Intro: TTabSheet
      Caption = 'Intro'
      TabVisible = False
      object Mensaje: TMemo
        Left = 0
        Top = 0
        Width = 439
        Height = 217
        Align = alClient
        Alignment = taCenter
        BorderStyle = bsNone
        Color = clBtnFace
        Lines.Strings = (
          '')
        TabOrder = 0
      end
    end
    object Empresas: TTabSheet
      Caption = 'Empresas'
      TabVisible = False
      object EmpresaFuenteGB: TGroupBox
        Left = 0
        Top = 0
        Width = 437
        Height = 224
        Align = alClient
        Caption = ' Escoja La &Empresa Fuente: '
        TabOrder = 0
        object EmpresaFuente: TListBox
          Left = 2
          Top = 15
          Width = 433
          Height = 207
          Align = alClient
          BorderStyle = bsNone
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ItemHeight = 20
          ParentFont = False
          TabOrder = 0
        end
      end
    end
    object Parametros: TTabSheet
      Caption = 'Parametros'
      TabVisible = False
      object PanelSuperior: TPanel
        Left = 0
        Top = 0
        Width = 439
        Height = 112
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object LogFileLBL: TLabel
          Left = 46
          Top = 7
          Width = 101
          Height = 13
          Alignment = taRightJustify
          Caption = '&Bit�cora del Proceso:'
          FocusControl = LogFile
        end
        object LogFileSeek: TSpeedButton
          Left = 411
          Top = 1
          Width = 25
          Height = 25
          Hint = 'Buscar Directorio'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
            300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
            330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
            333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
            339977FF777777773377000BFB03333333337773FF733333333F333000333333
            3300333777333333337733333333333333003333333333333377333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = LogFileSeekClick
        end
        object NumeroBaseLBL: TLabel
          Left = 43
          Top = 29
          Width = 104
          Height = 13
          Hint = 
            'Los Empleados Transferidos Tendr�n Un N�mero Igual O Superior A ' +
            'Esta Base'
          Caption = '# De &Empleado Base:'
          FocusControl = NumeroBase
          ParentShowHint = False
          ShowHint = True
        end
        object ConfidencialidadLBL: TLabel
          Left = 66
          Top = 51
          Width = 81
          Height = 13
          Hint = 'Nivel de Confidencialidad Asignado A Los Empleados Transferidos'
          Alignment = taRightJustify
          Caption = 'Conf&idencialidad:'
          FocusControl = Confidencialidad
          ParentShowHint = False
          ShowHint = True
        end
        object LogFile: TEdit
          Left = 149
          Top = 3
          Width = 261
          Height = 21
          TabOrder = 0
          Text = 'Bitacora.log'
        end
        object NumeroBase: TZetaNumero
          Left = 149
          Top = 25
          Width = 90
          Height = 21
          Hint = 
            'Los Empleados Transferidos Tendr�n Un N�mero Igual O Superior A ' +
            'Esta Base'
          Mascara = mnEmpleado
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
        end
        object Confidencialidad: TComboBox
          Left = 149
          Top = 47
          Width = 261
          Height = 21
          Hint = 'Nivel de Confidencialidad Asignado A Los Empleados Transferidos'
          Style = csDropDownList
          ItemHeight = 13
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object TransferirNominas: TCheckBox
          Left = 54
          Top = 70
          Width = 108
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Transferir &N�minas:'
          Checked = True
          State = cbChecked
          TabOrder = 3
        end
        object TransferirLiquidaciones: TCheckBox
          Left = 1
          Top = 90
          Width = 161
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Transferir &Liquidaciones IMSS:'
          Checked = True
          State = cbChecked
          TabOrder = 4
        end
      end
      object FiltroGB: TGroupBox
        Left = 0
        Top = 112
        Width = 439
        Height = 105
        Align = alClient
        Caption = ' Filtro De SQL Para Empleados En Empresa Fuente '
        TabOrder = 1
        object Filtro: TMemo
          Left = 2
          Top = 15
          Width = 435
          Height = 88
          Align = alClient
          ScrollBars = ssVertical
          TabOrder = 0
        end
      end
    end
    object Comparacion: TTabSheet
      Caption = 'Comparacion'
      TabVisible = False
      object EmpleadosTransferibles: TLabel
        Left = 162
        Top = 120
        Width = 112
        Height = 13
        Alignment = taCenter
        Caption = 'EmpleadosTransferibles'
      end
      object AvanceGB: TGroupBox
        Left = 0
        Top = 0
        Width = 437
        Height = 42
        Align = alTop
        Caption = ' Avance '
        TabOrder = 0
        object ProgressBar: TProgressBar
          Left = 2
          Top = 15
          Width = 433
          Height = 25
          Align = alClient
          Min = 0
          Max = 100
          TabOrder = 0
        end
      end
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 261
    Width = 447
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object LogFileDialog: TSaveDialog
    DefaultExt = 'log'
    Filter = 'Log Files|*.log|Text Files|*.txt|All Files|*.*'
    Left = 192
    Top = 163
  end
end
