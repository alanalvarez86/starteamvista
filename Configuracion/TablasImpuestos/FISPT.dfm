object FormaISPT: TFormaISPT
  Left = 298
  Top = 153
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Tablas de ISPT y Num�ricas'
  ClientHeight = 472
  ClientWidth = 301
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 301
    Height = 77
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clHighlight
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    object DBText1: TDBText
      Left = 11
      Top = 44
      Width = 17
      Height = 17
      Alignment = taRightJustify
      DataField = 'NU_CODIGO'
      DataSource = dmSugeridas.dsNumerica
    end
    object DBText2: TDBText
      Left = 46
      Top = 44
      Width = 249
      Height = 17
      DataField = 'NU_DESCRIP'
      DataSource = dmSugeridas.dsNumerica
    end
    object DBNavigator1: TDBNavigator
      Left = 8
      Top = 8
      Width = 136
      Height = 25
      DataSource = dmSugeridas.dsNumerica
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
      Flat = True
      TabOrder = 0
    end
    object btnGrabar: TBitBtn
      Left = 152
      Top = 8
      Width = 69
      Height = 25
      Caption = 'Grabar'
      Enabled = False
      TabOrder = 1
      OnClick = btnGrabarClick
    end
    object btnCancelar: TBitBtn
      Left = 224
      Top = 8
      Width = 69
      Height = 25
      Caption = 'Cancelar'
      Enabled = False
      TabOrder = 2
      OnClick = btnCancelarClick
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 113
    Width = 301
    Height = 359
    Align = alClient
    DataSource = dsArt_80
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'A80_LI'
        Title.Alignment = taRightJustify
        Width = 99
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'A80_CUOTA'
        Title.Alignment = taRightJustify
        Width = 92
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'A80_TASA'
        Title.Alignment = taRightJustify
        Visible = True
      end>
  end
  object Panel2: TPanel
    Left = 0
    Top = 77
    Width = 301
    Height = 36
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clInfoBk
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object btnBorrar: TBitBtn
      Left = 12
      Top = 4
      Width = 97
      Height = 25
      Caption = 'Borrar Rengl�n'
      TabOrder = 0
      OnClick = btnBorrarClick
    end
  end
  object dsArt_80: TDataSource
    DataSet = dmSugeridas.qryArt_80
    OnStateChange = dsArt_80StateChange
    Left = 236
    Top = 40
  end
end
