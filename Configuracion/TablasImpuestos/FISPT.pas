unit FISPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, StdCtrls, DBCtrls, ExtCtrls, ComCtrls, Buttons, Db;

type
  TFormaISPT = class(TForm)
    Panel1: TPanel;
    DBNavigator1: TDBNavigator;
    DBText1: TDBText;
    DBText2: TDBText;
    DBGrid1: TDBGrid;
    btnGrabar: TBitBtn;
    btnCancelar: TBitBtn;
    dsArt_80: TDataSource;
    Panel2: TPanel;
    btnBorrar: TBitBtn;
    procedure btnGrabarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure dsArt_80StateChange(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btnBorrarClick(Sender: TObject);
  private
    { Private declarations }
    procedure PrendeBotones;
    procedure ApagaBotones;
  public
    { Public declarations }
  end;

var
  FormaISPT: TFormaISPT;

implementation

uses DSugeridas;

{$R *.DFM}

procedure TFormaISPT.btnGrabarClick(Sender: TObject);
begin
    dmSugeridas.Grabar;
    ApagaBotones;
end;

procedure TFormaISPT.btnCancelarClick(Sender: TObject);
begin
    dmSugeridas.Cancelar;
    ApagaBotones;
end;

procedure TFormaISPT.dsArt_80StateChange(Sender: TObject);
begin
    if ( dsArt_80.State in [ dsEdit, dsInsert ] ) and ( not btnGrabar.Enabled ) then
    begin
        PrendeBotones;
    end;
end;

procedure TFormaISPT.ApagaBotones;
begin
        btnGrabar.Enabled := FALSE;
        btnCancelar.Enabled := FALSE;
        DBNavigator1.Enabled := TRUE;
end;

procedure TFormaISPT.PrendeBotones;
begin
        btnGrabar.Enabled := TRUE;
        btnCancelar.Enabled := TRUE;
        DBNavigator1.Enabled := FALSE;
end;

procedure TFormaISPT.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var
    nRespuesta : Integer;
begin
    CanClose := TRUE;
    if ( btnGrabar.Enabled ) then
    begin
      nRespuesta := MessageDlg( '� Desea Grabar los Cambios ?', mtConfirmation, mbYesNoCancel, 0 );
      case nRespuesta of
          mrYes : btnGrabarClick( self );
          mrNo  : btnCancelarClick( self );
          else    CanClose := FALSE;
      end;
    end;
end;

procedure TFormaISPT.btnBorrarClick(Sender: TObject);
begin
    dsArt_80.DataSet.Delete;
    PrendeBotones;
end;

end.
