unit Unit1;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, FileCtrl;

type
  TFormaPrincipal = class(TForm)
    btnISPT: TButton;
    DirectorioLBL: TLabel;
    Directorio: TEdit;
    DirectorioFind: TSpeedButton;
    procedure btnISPTClick(Sender: TObject);
    procedure DirectorioFindClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormaPrincipal: TFormaPrincipal;

implementation

uses DSugeridas,
     FISPT;

{$R *.DFM}

procedure TFormaPrincipal.btnISPTClick(Sender: TObject);
begin
     if dmSugeridas.Init( Directorio.Text ) then
     begin
          with FormaISPT do
          begin
              ShowModal;
          end;
     end;
end;

procedure TFormaPrincipal.DirectorioFindClick(Sender: TObject);
var
   sDir: String;
begin
     sDir := Directorio.Text;
     if SelectDirectory( sDir, [], 0 ) then
        Directorio.Text := sDir;
end;

end.
