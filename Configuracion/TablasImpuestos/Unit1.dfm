object FormaPrincipal: TFormaPrincipal
  Left = 192
  Top = 133
  Width = 339
  Height = 174
  Caption = 'Tablas Sugeridas'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object DirectorioLBL: TLabel
    Left = 8
    Top = 100
    Width = 48
    Height = 13
    Caption = 'Directorio:'
  end
  object DirectorioFind: TSpeedButton
    Left = 303
    Top = 96
    Width = 23
    Height = 22
    Hint = 'Buscar Directorio de Archivos *.DB'
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000130B0000130B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
      300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
      330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
      333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
      339977FF777777773377000BFB03333333337773FF733333333F333000333333
      3300333777333333337733333333333333003333333333333377333333333333
      333333333333333333FF33333333333330003333333333333777333333333333
      3000333333333333377733333333333333333333333333333333}
    NumGlyphs = 2
    ParentShowHint = False
    ShowHint = True
    OnClick = DirectorioFindClick
  end
  object btnISPT: TButton
    Left = 12
    Top = 12
    Width = 309
    Height = 45
    Caption = 'I. S. P. T. y Numéricas'
    TabOrder = 0
    OnClick = btnISPTClick
  end
  object Directorio: TEdit
    Left = 59
    Top = 96
    Width = 241
    Height = 21
    TabOrder = 1
    Text = 'D:\3Win_13\Datos\Fuentes\Sugerido'
  end
end
