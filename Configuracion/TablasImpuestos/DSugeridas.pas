unit DSugeridas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBTables, Db;

type
  TdmSugeridas = class(TDataModule)
    dbSugerido: TDatabase;
    tblNumerica: TTable;
    dsNumerica: TDataSource;
    qryArt_80: TQuery;
    tblNumericaNU_CODIGO: TSmallintField;
    tblNumericaNU_DESCRIP: TStringField;
    tblNumericaNU_INGLES: TStringField;
    tblNumericaNU_NUMERO: TFloatField;
    tblNumericaNU_TEXTO: TStringField;
    qryArt_80NU_CODIGO: TSmallintField;
    qryArt_80A80_LI: TFloatField;
    qryArt_80A80_CUOTA: TFloatField;
    qryArt_80A80_TASA: TFloatField;
    updArt_80: TUpdateSQL;
    procedure qryArt_80NewRecord(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    function Init(const sPath: String): Boolean;
    procedure Grabar;
    procedure Cancelar;
  end;

var
  dmSugeridas: TdmSugeridas;

implementation

{$R *.DFM}

{ TdmSugeridas }

function TdmSugeridas.Init( const sPath: String ): Boolean;
begin
     try
        with qryArt_80 do
        begin
             Active := False;
        end;
        with tblNumerica do
        begin
             Active := False;
        end;
        with dbSugerido do
        begin
             Connected := False;
             Params.Values[ 'PATH' ] := sPath;
             Connected := True;
        end;
        with tblNumerica do
        begin
             Active := True;
        end;
        with qryArt_80 do
        begin
             Active := True;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                Result := False;
           end;
     end;
end;

procedure TdmSugeridas.Cancelar;
begin
    with qryArt_80 do
    begin
        CancelUpdates;
    end;
end;

procedure TdmSugeridas.Grabar;
begin
    with qryArt_80 do
    begin
        ApplyUpdates;
        CommitUpdates;
    end;
end;

procedure TdmSugeridas.qryArt_80NewRecord(DataSet: TDataSet);
begin
    qryArt_80.FieldByName( 'NU_CODIGO' ).AsInteger :=
      tblNumerica.FieldByName( 'NU_CODIGO' ).AsInteger;
end;

end.
