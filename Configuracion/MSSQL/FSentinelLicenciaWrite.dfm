object SentinelLicenciaWrite: TSentinelLicenciaWrite
  Left = 925
  Top = 356
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Licencia de Sentinel Virtual'
  ClientHeight = 135
  ClientWidth = 357
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 13
    Top = 15
    Width = 134
    Height = 13
    Caption = 'Licencia de Sentinel Virtual: '
  end
  object btnBuscaArchivo: TSpeedButton
    Left = 315
    Top = 28
    Width = 23
    Height = 22
    Hint = 'Busca Archivo de Licencia'
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
      777777777777777770000070000077777000E00BFBFB07777777E0BFBF000777
      7700E0FBFBFBF0777700E0BFBF0000007777E0FBFBFBFBFB0799E0BF00000000
      7799000BFB077777777777700077777777007777777777777700777777777777
      7777777777777777700077777777777770007777777777777777}
    ParentShowHint = False
    ShowHint = True
    OnClick = btnBuscaArchivoClick
  end
  object panelBotones: TPanel
    Left = 0
    Top = 95
    Width = 357
    Height = 40
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      357
      40)
    object ReadOnlyLBL: TLabel
      Left = 8
      Top = 8
      Width = 123
      Height = 26
      Alignment = taCenter
      AutoSize = False
      Caption = 'No tiene derechos de administrador'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
      WordWrap = True
    end
    object Salir: TBitBtn
      Left = 273
      Top = 8
      Width = 75
      Height = 25
      Hint = 'Salir'
      Anchors = [akRight, akBottom]
      Caption = '&Salir'
      TabOrder = 0
      Kind = bkClose
    end
  end
  object btnEscribirArchivoLicencia: TBitBtn
    Left = 133
    Top = 54
    Width = 86
    Height = 25
    Hint = 'Escribe el Archivo de Licencia especificado'
    Caption = '&Escribir'
    Default = True
    Enabled = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    OnClick = btnEscribirArchivoLicenciaClick
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000010000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
  object ArchivoLicencia: TZetaEdit
    Left = 13
    Top = 30
    Width = 300
    Height = 21
    Hint = 'Archivo con Licencia de Sentinel Virtual'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    OnChange = ArchivoLicenciaChange
  end
  object OpenDialog: TOpenDialog
    DefaultExt = '.lic'
    Filter = 'Archivo de Licencia (*.lic)|*.lic'
    Left = 508
    Top = 24
  end
end
