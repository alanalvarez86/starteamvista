unit DMigrarDatosIB;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     DBTables, Db, OVCL, OCL, ODSI, OCI,
     ZetaCommonLists,
     FMigrar,
     DMigrar,
     {$ifdef VER130}IBDataset,{$else}IBODataset,{$endif}
     IB_Components;

type
  TdmMigrarDatosIB = class(TdmMigrar)
    HdbcMSSQL: THdbc;
    oeTarget: TOEQuery;
    oeCheckpoint: TOEQuery;
    IBOTarget: TIBOQuery;
    IBODelete: TIBOQuery;
    IBODatabase: TIBODatabase;
    procedure HdbcMSSQLBeforeConnect(Sender: TObject);
  private
    { Private declarations }
    FIBDatabase: String;
    FIBPassword: String;
    FIBUser: String;
    FMSSQLDatabase: String;
    FMSSQLPassword: String;
    FMSSQLUser: String;
    FErrores: integer;
    function DoConectaInterbase: Boolean;
    function DoConectaMSSQL: Boolean;
    function ImportaTabla( const sTabla: String ): Boolean;
    function QueryTarget: {$ifdef USE_BDE}TQuery{$else}TIBOQuery{$endif};
    function QueryDelete: {$ifdef USE_BDE}TQuery{$else}TIBOQuery{$endif};
    function TransfiereAsistencia( const sTabla: String; const iTope, iCuantos: Integer; var iRegistros, iCount: Integer ): Boolean;
    function TransfiereNomina( const sTabla: String; const iTope, iCuantos: Integer; var iRegistros, iCount: Integer ): Boolean;
    function TransfiereAcumula( const sTabla: String; const iTope, iCuantos: Integer; var iRegistros, iCount: Integer ): Boolean;
    function TransfierePorFecha( const sTabla, sFieldFecha: String; const iTope, iCuantos: Integer; var iRegistros, iCount: Integer ): Boolean;
    //function TransfiereKardex( const iTope, iCuantos: Integer; var iRegistros, iCount: Integer ): Boolean;
    function TransfiereTabla( const sTabla, sFiltro: String; const iTope, iCuantos: Integer; var iRegistros, iCount: Integer ): Boolean;
    function EsTablaExclusivaMSSQL( const sTabla: String ): Boolean;
    function EsTablaIdentityMSSQL( const sTabla: String ): Boolean;
    function GetCamposExcluidos( const sTabla: String ): String;
    procedure AttachQuery( const Script: String );
    procedure BorrarTablas;
    procedure DoDesconectaMSSQL;
    procedure DoDesconectaInterbase;
    procedure ListarTablas;
    procedure MigrarTablas;
    procedure Checkpoint;
    procedure ActualizarCodigo(Const sCodigoAnt,sCodigoActual:string;const Actualiza:Boolean);
  protected
    { Protected declarations }
    {$ifndef USE_BDE}
    procedure AttachTargetQuery( Query: TIBOQuery; const Script: String );
    {$endif}
  public
    { Public declarations }
    property IBDataBase: String read FIBDatabase write FIBDatabase;
    property IBUser: String read FIBUser write FIBUser;
    property IBPassword: String read FIBPassword write FIBPassword;
    procedure Importar( Forma: TMigrar; const lMigrarComparte, lBorrarTablas: Boolean );
    procedure ActualizaCodigoEmpresa(Const CodigoAnt,CodigoActual:string;const Actualiza:Boolean);
    procedure ConectaEmpresa;
  end;

var
  dmMigrarDatosIB: TdmMigrarDatosIB;

implementation

uses DDBConfig,
     ZetaServerTools,
     ZetaCommonClasses,
     ZetaCommonTools;

{$R *.DFM}

{ TdmMigrarDatosIB }

const
     K_QTY_EXCLUSIVAS = 21;
     K_QTY_IDENTITY = 3;
     aTablasExclusivasMSSQL : array [ 1..K_QTY_EXCLUSIVAS ] of PChar = ( 'R_LISTAVAL',
                                                                         'R_VALOR',
                                                                         'R_ENTIDAD',
                                                                         'R_ATRIBUTO',
                                                                         'R_RELACION',
                                                                         'R_CLASIFI',
                                                                         'R_CLAS_ACC',
                                                                         'R_ENT_ACC',
                                                                         'R_CLAS_ENT',
                                                                         'R_MODULO',
                                                                         'R_MOD_ENT',
                                                                         'R_ORDEN',
                                                                         'R_FILTRO',
                                                                         'R_DEFAULT',
                                                                         'VALPLANT',
                                                                         'VFACTOR',
                                                                         'VSUBFACT',
                                                                         'VALNIVEL',
                                                                         'VAL_PTO',
                                                                         'VPUNTOS',
                                                                         'TFIJAS' );

     aTablasIdentityMSSQL : array [ 1..K_QTY_IDENTITY ] of PChar = ( 'GR_AD_ACC',
                                                                     'PLAZA',
                                                                     'CTA_MOVS'
                                                                    );

procedure TdmMigrarDatosIB.HdbcMSSQLBeforeConnect(Sender: TObject);
const
     SQL_PRESERVE_CURSORS = 1204;
     SQL_PC_ON = 1;
begin
     SQLSetConnectAttr( THdbc( Sender ).Handle, SQL_PRESERVE_CURSORS, Pointer( SQL_PC_ON ), 0);
end;

function TdmMigrarDatosIB.DoConectaInterbase: Boolean;
begin
     try
        {$ifdef USE_BDE}
        OpenDatabase( dbTarget, FIBDatabase, FIBUser, FIBPassword );
        {$else}
        with IBODatabase do
        begin
             Connected := False;
             DatabaseName := FIBDatabase;
             UserName := FIBUser;
             Password := FIBPassword;
             Connected := True;
        end;
        {$endif}
        Result := True;
     except
           on Error: Exception do
           begin
                Bitacora.AgregaError( 'Error al Conectar Base de Datos de Interbase:' + CR_LF +
                                      FIBDataBase + CR_LF +
                                      Error.Message );
                Result := False;
           end;
     end;
end;

procedure TdmMigrarDatosIB.DoDesconectaInterbase;
begin
     CloseDBTarget;
end;

{$ifndef USE_BDE}
procedure TdmMigrarDatosIB.AttachTargetQuery( Query: TIBOQuery; const Script: String );
begin
     with Query do
     begin
          Active := False;
          with SQL do
          begin
               Clear;
               if StrLleno( Script ) then
               begin
                    Add( Script );
                    Prepare;
               end;
          end;
     end;
end;
{$endif}

function TdmMigrarDatosIB.QueryTarget: {$ifdef USE_BDE}TQuery{$else}TIBOQuery{$endif};
begin
     {$ifdef USE_BDE}
     Result := tqTarget;
     {$else}
     Result := IBOTarget;
     {$endif}
end;

function TdmMigrarDatosIB.QueryDelete: {$ifdef USE_BDE}TQuery{$else}TIBOQuery{$endif};
begin
     {$ifdef USE_BDE}
     Result := tqDelete;
     {$else}
     Result := IBODelete;
     {$endif}
end;

function TdmMigrarDatosIB.DoConectaMSSQL: Boolean;
var
   sServer, sDB: String;
begin
     Result := False;
     ZetaServerTools.GetServerDatabase( FMSSQLDatabase, sServer, sDB );
     with HdbcMSSQL do
     begin
          Connected := False;
          UserName := FMSSQLUser;
          Password := FMSSQLPassword;
          Driver := 'SQL Server';
          with Attributes do
          begin
               Clear;
               Add( Format( 'Server=%s', [ sServer ] ) );
               Add( Format( 'Database=%s', [ sDB ] ) );
          end;
          try
             Connected := True;
             Result := True;
          except
                on Error: Exception do
                begin
                     Bitacora.AgregaError( 'Error al Conectar Base de Datos de SQL Server:' + CR_LF +
                                           dmDBConfig.GetCompanyDatabase + CR_LF +
                                           Error.Message );
                end;
          end;
     end;
end;

procedure TdmMigrarDatosIB.DoDesconectaMSSQL;
begin
     hdbcMSSQL.Connected := False;
end;

procedure TdmMigrarDatosIB.AttachQuery( const Script: String );
begin
     with oeTarget do
     begin
          Active := False;
          with SQL do
          begin
               Clear;
               if StrLleno( Script ) then
               begin
                    Add( Script );
                    Prepare;
               end;
          end;
     end;
end;

procedure TdmMigrarDatosIB.ListarTablas;
const
     Q_LIST_TABLES = 'select T.RDB$RELATION_NAME TABLE_NAME from RDB$RELATIONS T where ' +
                     '( ( T.RDB$SYSTEM_FLAG = 0 ) or ( T.RDB$SYSTEM_FLAG is NULL ) ) and ' +
                     '( T.RDB$DBKEY_LENGTH = 8 ) and ' +
                     '( ( select COUNT(*) from RDB$VIEW_RELATIONS V where ( V.RDB$VIEW_NAME = T.RDB$RELATION_NAME ) ) = 0 ) ' +
                     'order by T.RDB$RELATION_NAME';
     Q_GET_FOREIGN_KEYS = 'select RDB$FOREIGN_KEY KEY_NAME from RDB$INDICES where ( RDB$RELATION_NAME = :Tabla ) and not ( ( RDB$FOREIGN_KEY is NULL ) or ( RDB$FOREIGN_KEY = "" ) )';
     Q_GET_FOREIGN_TABLE = 'select RDB$RELATION_NAME TABLE_NAME from RDB$INDICES where ( RDB$INDEX_NAME = :Indice )';
     Q_GET_MSSQL_F_KEYS = 'EXEC dbo.sp_fkeys @FKTABLE_NAME = :Tabla';
var
   i: Integer;
   FTablas: TStrings;

procedure DeterminaDependencias( const sValor: String );
var
   FDependencias: TStrings;
   i: Integer;
   sAnterior, sLlaveForanea: String;
begin
     FDependencias := TStringList.Create;
     try
        try
           with QueryTarget do
           begin
                Active := False;
                ParamByName( 'Tabla' ).AsString := sValor;
                Active := True;
                while not Eof do
                begin
                     sLlaveForanea := FieldByName( 'KEY_NAME' ).AsString;
                     if ZetaCommonTools.StrLleno( sLlaveForanea ) then
                     begin
                          with QueryDelete do
                          begin
                               Active := False;
                               ParamByName( 'Indice' ).AsString := sLlaveForanea;
                               Active := True;
                               FDependencias.Add( FieldByName( 'TABLE_NAME' ).AsString );
                               Active := False;
                          end;
                     end;
                     Next;
                end;
                Active := False;
           end;
        except
              on Error: Exception do
              begin
                   FDependencias.Clear;
                   Bitacora.AgregaError( 'Error al Determinar Dependencias x IB: ' + CR_LF + Error.Message );
              end;
        end;
        try
           with oeTarget do
           begin
                Active := False;
                ParamByName( 'Tabla' ).AsString := sValor;
                Active := True;
                sAnterior := '';
                while not Eof do
                begin
                     sLlaveForanea := FieldByName( 'PKTABLE_NAME' ).AsString;
                     if ZetaCommonTools.StrLleno( sLlaveForanea ) and ( sLlaveForanea <> sAnterior ) and ( FDependencias.IndexOf( sLlaveForanea ) < 0 ) then
                     begin
                          FDependencias.Add( sLlaveForanea );
                          sAnterior := sLlaveForanea;
                     end;
                     Next;
                end;
                Active := False;
           end;
        except
              on Error: Exception do
              begin
                   FDependencias.Clear;
                   Bitacora.AgregaError( 'Error al Determinar Dependencias x IB: ' + CR_LF + Error.Message );
              end;
        end;
        with FDependencias do
        begin
             for i := 0 to ( Count - 1 ) do
             begin
                  DeterminaDependencias( Strings[ i ] );
             end;
        end;
        with FLista do
        begin
             if ( IndexOf( sValor ) < 0 ) and ( not EsTablaExclusivaMSSQL( sValor ) ) then  { No Existe en la Lista }
             begin
                  Add( sValor );                  { Agrega al Final }
             end;
        end;
     finally
            FDependencias.Free;
     end;
end;

function AgregaTabla( const sTabla: String ): Integer;
begin
     with FLista do
     begin
          Result := IndexOf( sTabla );
          if ( Result < 0 ) then  { No Existe en la Lista }
          begin
               Add( sTabla );                  { Agrega al Final }
               Result := IndexOf( sTabla );
          end;
     end;
end;

begin
     with FLista do
     begin
          BeginUpdate;
          Clear;
     end;
     try
        FTablas := TStringList.Create;
        try
           AttachTargetQuery( QueryTarget, Q_LIST_TABLES );
           with QueryTarget do
           begin
                Active := True;
                while not Eof do
                begin
                     FTablas.Add( FieldByName( 'TABLE_NAME' ).AsString );
                     Next;
                end;
                Active := False;
           end;
           AttachTargetQuery( QueryTarget, Q_GET_FOREIGN_KEYS );
           AttachTargetQuery( QueryDelete, Q_GET_FOREIGN_TABLE );
           AttachQuery( Q_GET_MSSQL_F_KEYS );
           with FTablas do
           begin
                for i := 0 to ( Count - 1 ) do
                begin
                     DeterminaDependencias( Strings[ i ] );
                end;
           end;
        finally
               FreeAndNil( FTablas );
        end;
     finally
            with FLista do
            begin
                 EndUpdate;
            end;
     end;
end;

function TdmMigrarDatosIB.EsTablaExclusivaMSSQL( const sTabla: String ): Boolean;
var
   i: Byte;
begin
     Result:= FALSE;
     for i:= 1 to K_QTY_EXCLUSIVAS do
     begin
          if ( aTablasExclusivasMSSQL[i] = sTabla ) then
          begin
               Result:= TRUE;
               Break;
          end;
     end;
end;

procedure TdmMigrarDatosIB.Checkpoint;
begin
     with oeCheckpoint do
     begin
          ExecSQL;
     end;
end;

function TdmMigrarDatosIB.EsTablaIdentityMSSQL( const sTabla: String): Boolean;
var
   i: Byte;
begin
     Result:= FALSE;
     for i:= 1 to K_QTY_IDENTITY do
     begin
          if ( aTablasIdentityMSSQL[i] = sTabla ) then
          begin
               Result:= TRUE;
               Break;
          end;
     end;
end;

function TdmMigrarDatosIB.GetCamposExcluidos( const sTabla: String): String;
begin
     Result := VACIO;
     if ( sTabla = 'CTA_MOVS' ) then
        Result := 'CM_DEPOSIT,CM_RETIRO';
end;

function TdmMigrarDatosIB.TransfiereTabla( const sTabla, sFiltro: String; const iTope, iCuantos: Integer; var iRegistros, iCount: Integer ): Boolean;
const
     Q_SELECT_CAMPOS = 'select * from %s %s';
     Q_INSERT_CAMPOS = 'insert into %s ( %s ) values ( %s );';
     K_MAX_ERRORES = 100;
     Q_IDENT_INSERT = 'SET IDENTITY_INSERT %s %s;';
     aSetIdentity: array[ FALSE..TRUE ] of PChar = ( 'OFF', 'ON' );
var
   sCampos, sParams: String;
   sExcluidos: String;
   i: Integer;

   procedure SetCampos;
   var
      j: Integer;
      sCampo: String;
   begin
        with QueryTarget do
        begin
             for j := 0 to ( FieldCount - 1 ) do
             begin
                  sCampo := Fields[ j ].FieldName;
                  if ( Pos( UpperCase( sCampo ), sExcluidos ) = 0 ) then
                  begin
                       sCampos := ConcatString( sCampos, sCampo, ',' );
                       sParams := ConcatString( sParams, ':' + sCampo, ',' );
                  end;
             end;
        end;
   end;

begin
     Result := True;
     sExcluidos := GetCamposExcluidos( sTabla );
     AttachTargetQuery( QueryTarget, Trim( Format( Q_SELECT_CAMPOS, [ sTabla, sFiltro ] ) ) );
     try
        with QueryTarget do
        begin
             Active := True;
             SetCampos;
             if EsTablaIdentityMSSQL( sTabla ) then
                AttachQuery( Format( Q_IDENT_INSERT, [ sTabla, aSetIdentity[TRUE] ] ) +
                             Format( Q_INSERT_CAMPOS, [ sTabla, sCampos, sParams ] ) +
                             Format( Q_IDENT_INSERT, [ sTabla, aSetIdentity[FALSE] ] ) )
             else
                 AttachQuery( Format( Q_INSERT_CAMPOS, [ sTabla, sCampos, sParams ] ) );
             while not Eof and Result do
             begin
                  Inc( iCount );
                  try
                     for i := 0 to ( FieldCount - 1 ) do
                     begin
                          with Fields[ i ] do
                          begin
                               if ( Pos( Uppercase( FieldName ), sExcluidos ) = 0 ) then
                               begin
                                    case DataType of
                                         ftString: oeTarget.ParamByName( FieldName ).AsString := AsString;
                                         ftSmallint: oeTarget.ParamByName( FieldName ).AsInteger := AsInteger;
                                         ftInteger: oeTarget.ParamByName( FieldName ).AsInteger := AsInteger;
                                         ftWord: oeTarget.ParamByName( FieldName ).AsInteger := AsInteger;
                                         ftBoolean: oeTarget.ParamByName( FieldName ).AsBoolean := AsBoolean;
                                         ftFloat: oeTarget.ParamByName( FieldName ).AsFloat := AsFloat;
                                         ftCurrency: oeTarget.ParamByName( FieldName ).AsFloat := AsFloat;
                                         ftBCD: oeTarget.ParamByName( FieldName ).AsFloat := AsFloat;            //	Binary-Coded Decimal field
                                         ftDate: oeTarget.ParamByName( FieldName ).AsDate := ZetaCommonTools.rMax( NullDateTime, AsDateTime );
                                         ftTime: oeTarget.ParamByName( FieldName ).AsTime := ZetaCommonTools.rMax( NullDateTime, AsDateTime );
                                         ftDateTime: oeTarget.ParamByName( FieldName ).AsDateTime := ZetaCommonTools.rMax( NullDateTime, AsDateTime );
                                         ftAutoInc: oeTarget.ParamByName( FieldName ).AsInteger := AsInteger;    // Auto-incrementing 32-bit integer counter field
                                         ftBlob: oeTarget.ParamByName( FieldName ).AsBlob := AsString;	        // Binary Large OBject field
                                         ftMemo: oeTarget.ParamByName( FieldName ).AsMemo := AsString;	        // Text memo field
                                         ftFixedChar: oeTarget.ParamByName( FieldName ).AsString := AsString;	// Fixed character field
                                         ftWideString: oeTarget.ParamByName( FieldName ).AsString := AsString;	// Wide string field
                                         ftLargeInt: oeTarget.ParamByName( FieldName ).AsInteger := AsInteger;	// Large integer field
                                    else
                                        oeTarget.Params[ i ].Value := Value;
                                    end;
                               end;
                          end;
                     end;
                     try
                        with hdbcMSSQL do
                        begin
                             StartTransact;
                             try
                                oeTarget.ExecSQL;
                                Commit;
                                Inc( iRegistros );
                             except
                                   on Error: Exception do
                                   begin
                                        RollBack;
                                        if ( FErrores <= K_MAX_ERRORES ) then
                                        begin
                                             Bitacora.AgregaError( Format( 'Error al Importar Registro %d de Tabla %s', [ iCount, sTabla ] ) + CR_LF + Error.Message );
                                             Inc( FErrores );
                                        end;
                                   end;
                             end;
                             EndTransact;
                        end;
                     except
                           on Error: Exception do
                           begin
                                Result := False;
                                if ( FErrores <= K_MAX_ERRORES ) then
                                begin
                                     Bitacora.AgregaError( Format( 'Error al Importar Registro %d de Tabla %s', [ iCount, sTabla ] ) + CR_LF + Error.Message );
                                     Inc( FErrores );
                                end;
                           end;
                     end;
                  except
                        on Error: Exception do
                        begin
                             Result := False;
                             if ( FErrores <= K_MAX_ERRORES ) then
                             begin
                                  Bitacora.AgregaError( Format( 'Error al Importar Registro %d de Tabla %s', [ iCount, sTabla ] ) + CR_LF + Error.Message );
                                  Inc( FErrores );
                             end;
                        end;
                  end;
                  if ( ( iCount mod iTope ) = 0 ) then
                  begin
                       ItemName := Format( '%s %3.0n %s (%d/%d)', [ sTabla, ( iCount / iCuantos * 100 ), '%', iCount, iCuantos ] );
                       DoCallBack( Self, Result );
                  end;
                  Next;
             end;
             Active := False;
        end;
     except
           on Error: Exception do
           begin
                Bitacora.AgregaError( Format( 'Error al Importar Tabla %s', [ sTabla ] ) + CR_LF + Error.Message );
                Inc( FErrores );
           end;
     end;
end;

function TdmMigrarDatosIB.TransfiereAsistencia( const sTabla: String; const iTope, iCuantos: Integer; var iRegistros, iCount: Integer ): Boolean;
const
     Q_FECHAS = 'select MIN( AU_FECHA ) INICIO, MAX( AU_FECHA ) FINAL from %s';
     Q_FILTRO = 'where ( %s.AU_FECHA = %s )';
var
   dInicio, dFinal: TDate;
begin
     Result := TRUE;
     AttachTargetQuery( QueryDelete, Format( Q_FECHAS, [ sTabla ] ) );
     with QueryDelete do
     begin
          Active := TRUE;
          dInicio := FieldByName( 'INICIO' ).AsDateTime;
          dFinal := FieldByName( 'FINAL' ).AsDateTime;
          Active := FALSE;
     end;
     if ( dInicio > NullDateTime ) and ( dFinal > NullDateTime ) then
     begin
          while ( dInicio <= dFinal ) do
          begin
               Result := Result and TransfiereTabla( sTabla, Format( Q_FILTRO, [ sTabla, ZetaCommonTools.DateToStrSQLC( dInicio ) ] ), iTope, iCuantos, iRegistros, iCount );
               dInicio := dInicio + 1;
          end;
     end;
end;

function TdmMigrarDatosIB.TransfiereNomina( const sTabla: String; const iTope, iCuantos: Integer; var iRegistros, iCount: Integer ): Boolean;
const
     Q_PERIODOS = 'select PE_YEAR, PE_TIPO, PE_NUMERO from %s group by PE_YEAR, PE_TIPO, PE_NUMERO';
     Q_FILTRO = 'where ( %0:s.PE_YEAR = %1:d ) and ( %0:s.PE_TIPO = %2:d ) and ( %0:s.PE_NUMERO = %3:d )';
begin
     Result := TRUE;
     AttachTargetQuery( QueryDelete, Format( Q_PERIODOS, [ sTabla ] ) );
     with QueryDelete do
     begin
          Active := TRUE;
          while ( not EOF ) do
          begin
               Result := Result and TransfiereTabla( sTabla, Format( Q_FILTRO, [ sTabla,
                                                                                 FieldByName( 'PE_YEAR' ).AsInteger,
                                                                                 FieldByName( 'PE_TIPO' ).AsInteger,
                                                                                 FieldByName( 'PE_NUMERO' ).AsInteger ] ),
                                                                                 iTope, iCuantos, iRegistros, iCount );
               Next;
          end;
          Active := FALSE;
     end;
end;

function TdmMigrarDatosIB.TransfiereAcumula( const sTabla: String; const iTope, iCuantos: Integer; var iRegistros, iCount: Integer ): Boolean;
const
     Q_ANNOS = 'select MIN( AC_YEAR ) INICIO, MAX( AC_YEAR ) FINAL from %s';
     Q_FILTRO = 'where ( %s.AC_YEAR = %d )';
var
   iInicio, iFinal: Integer;
begin
     Result := TRUE;
     AttachTargetQuery( QueryDelete, Format( Q_ANNOS, [ sTabla ] ) );
     with QueryDelete do
     begin
          Active := TRUE;
          iInicio := FieldByName( 'INICIO' ).AsInteger;
          iFinal := FieldByName( 'FINAL' ).AsInteger;
          Active := FALSE;
     end;
     while ( iInicio <= iFinal ) do
     begin
          Result := Result and TransfiereTabla( sTabla, Format( Q_FILTRO, [ sTabla, iInicio ] ), iTope, iCuantos, iRegistros, iCount );
          Inc( iInicio );
     end;
end;

function TdmMigrarDatosIB.TransfierePorFecha( const sTabla, sFieldFecha: String; const iTope, iCuantos: Integer; var iRegistros, iCount: Integer ): Boolean;
const
     Q_FECHAS = 'select DISTINCT(%0:s) FECHA from %1:s order by %0:s';
     Q_FILTRO = 'where ( %s.%s = %s )';
begin
     Result := True;
     AttachTargetQuery( QueryDelete, Format( Q_FECHAS, [ sFieldFecha, sTabla ] ) );
     with QueryDelete do
     begin
          Active := True;
          while ( not EOF ) do
          begin
               Result := Result and TransfiereTabla( sTabla, Format( Q_FILTRO, [ sTabla, sFieldFecha, ZetaCommonTools.DateToStrSQLC( FieldByName( 'FECHA' ).AsDateTime ) ] ), iTope, iCuantos, iRegistros, iCount );
               Next;
          end;
          Active := False;
     end;
end;

{
function TdmMigrarDatosIB.TransfiereKardex( const iTope, iCuantos: Integer; var iRegistros, iCount: Integer ): Boolean;
const
     Q_FECHAS = 'select DISTINCT(CB_FECHA) FECHA from KARDEX order by CB_FECHA';
     Q_FILTRO = 'where ( KARDEX.CB_FECHA = %s )';
begin
     Result := True;
     AttachTargetQuery( QueryDelete, Q_FECHAS );
     with QueryDelete do
     begin
          Active := True;
          while ( not EOF ) do
          begin
               Result := Result and TransfiereTabla( 'KARDEX', Format( Q_FILTRO, [ ZetaCommonTools.DateToStrSQLC( FieldByName( 'FECHA' ).AsDateTime ) ] ), iTope, iCuantos, iRegistros, iCount );
               Next;
          end;
          Active := False;
     end;
end;
}

function TdmMigrarDatosIB.ImportaTabla( const sTabla: String ): Boolean;
const
     Q_SELECT_COUNT  = 'select COUNT(*) CUANTOS from %s';
var
   sMsg: String;
   iCuantos, iRegistros, iCount, iTope: Integer;
   lHayError, lOk: Boolean;
begin
     lHayError := False;
     Result := True;
     if ZetaCommonTools.StrLleno( sTabla ) then
     begin
          DoStartCallBack( sTabla );
          { Preguntar Si La tabla Existe En La Base De Datos Destino - SQL Server }
          try
             AttachQuery( Format( Q_SELECT_COUNT, [ sTabla ] ) );
             with oeTarget do
             begin
                  Active := True;
                  Active := False;
             end;
          except
                on Error: Exception do
                begin
                     lHayError := True;
                     Bitacora.AgregaError( Format( 'Tabla %s No Existe En Base De Datos Destino', [ sTabla ] ) + CR_LF + Error.Message );
                end;
          end;
          if not lHayError then
          begin
               { Preguntar Cuantos Registros Hay En La Tabla En La Base De Datos Fuente - Interbase }
               AttachTargetQuery( QueryTarget, Format( Q_SELECT_COUNT, [ sTabla ] ) );
               with QueryTarget do
               begin
                    Active := True;
                    iCuantos := Fields[ 0 ].AsInteger;
                    Active := False;
               end;
               if ( iCuantos < 1000 ) then
                  iTope := 10
               else
                   iTope := 100;
               if ( iCuantos <= 0 ) then
               begin
                    Bitacora.AgregaResumen( Format( 'Tabla %s NO Contiene Registros', [ sTabla ] ) );
               end
               else
               begin
                    iRegistros := 0;
                    iCount := 0;
                    if ( sTabla = 'AUSENCIA' ) or ( sTabla = 'CHECADAS' ) then
                    begin
                         lOk := TransfiereAsistencia( sTabla, iTope, iCuantos, iRegistros, iCount );
                    end
                    else if ( sTabla = 'KARDEX' ) then
                    begin
                         lOk := TransfierePorFecha( 'KARDEX', 'CB_FECHA', iTope, iCuantos, iRegistros, iCount );
                    end
                    else if ( sTabla = 'NOMINA' ) or ( sTabla = 'MOVIMIEN' ) or ( sTabla = 'POLIZA' ) then
                    begin
                         lOk := TransfiereNomina( sTabla, iTope, iCuantos, iRegistros, iCount );
                    end
                    else if ( sTabla = 'ACUMULA' ) then
                    begin
                         lOk := TransfiereAcumula( sTabla, iTope, iCuantos, iRegistros, iCount );
                    end
                    else if ( sTabla = 'PROCESO' ) then
                    begin
                         lOk := TransfierePorFecha( sTabla, 'PC_FEC_INI', iTope, iCuantos, iRegistros, iCount );
                    end
                    else if ( sTabla = 'BITACORA' ) then
                    begin
                         lOk := TransfierePorFecha( sTabla, 'BI_FECHA', iTope, iCuantos, iRegistros, iCount );
                    end
                    else if ( sTabla = 'KARCURSO' ) then
                    begin
                         lOk := TransfierePorFecha( sTabla, 'KC_FEC_TOM', iTope, iCuantos, iRegistros, iCount );
                    end
                    else if ( sTabla = 'ACCESLOG' ) then
                    begin
                         lOk := TransfierePorFecha( sTabla, 'AL_FECHA', iTope, iCuantos, iRegistros, iCount );
                    end
                    else if ( sTabla = 'CEDULA' ) then
                    begin
                         lOk := TransfierePorFecha( sTabla, 'CE_FECHA', iTope, iCuantos, iRegistros, iCount );
                    end
                    else if ( sTabla = 'WORKS' ) then
                    begin
                         lOk := TransfierePorFecha( sTabla, 'AU_FECHA', iTope, iCuantos, iRegistros, iCount );
                    end
                    else
                        lOk := TransfiereTabla( sTabla, ZetaCommonClasses.VACIO, iTope, iCuantos, iRegistros, iCount );
                    if lOk then
                       sMsg := ''
                    else
                        sMsg := 'Con ERRORES';
                    Bitacora.AgregaResumen( Format( 'Tabla %s Fu� Migrada al %3.0n %s ( %d / %d ) %s', [ sTabla, ( iRegistros / iCount * 100 ), '%', iRegistros, iCount, sMsg ] ) );
                    CheckPoint;
               end;
          end;
     end;
end;

procedure TdmMigrarDatosIB.BorrarTablas;
const
     Q_DELETE_CAMPOS = 'delete from %s';
var
   i: Integer;
   sTabla: String;
begin
     with FLista do
     begin
          for i := ( Count - 1 ) downto 0 do
          begin
               sTabla := Strings[ i ];
               Bitacora.AgregaResumen( 'Borrando Tabla ' + sTabla );
               try
                  AttachQuery( Format( Q_DELETE_CAMPOS, [ sTabla ] ) );
                  with hdbcMSSQL do
                  begin
                       StartTransact;
                       try
                          oeTarget.ExecSQL;
                          Commit;
                          EndTransact;
                       except
                             on Error: Exception do
                             begin
                                  RollBack;
                                  Bitacora.AgregaError( 'Error al Borrar ' + sTabla + CR_LF + Error.Message );
                             end;
                       end;
                       EndTransact;
                  end;
                  Checkpoint;
               except
                     on Error: Exception do
                     begin
                          Bitacora.AgregaError( 'Error al Borrar Tabla : ' + sTabla + CR_LF + Error.Message );
                     end;
               end;
               if not DoEndCallBack then
                  Exit;
          end;
     end;
end;

procedure TdmMigrarDatosIB.MigrarTablas;
var
   i: Integer;
begin
     with FLista do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               FErrores := 1;
               if not ImportaTabla( Strings[ i ] ) or not DoEndCallBack then
                  Exit;
          end;
     end;
end;

procedure TdmMigrarDatosIB.Importar( Forma: TMigrar; const lMigrarComparte, lBorrarTablas: Boolean );
var
   dStart: TDateTime;
   iHoras, iMinutos, iSegundos, iMilisegundos: Word;
   sTiempo: String;
begin
     ClearErrorFound;
     with Bitacora do
     begin
          AgregaResumen( 'Migraci�n de Interbase a Microsoft SQL Server' );
          AgregaResumen( GetTimeStamp + ' Conectando Bases de Datos' );
     end;
     dStart := Now;
     if DoConectaInterbase then
     begin
          try
             with dmDBConfig do
             begin
                  if lMigrarComparte then
                  begin
                       with Registry do
                       begin
                            FMSSQLDatabase := Database;
                            FMSSQLUser := UserName;
                            FMSSQLPassword := Password;
                       end;
                  end
                  else
                  begin
                       FMSSQLDatabase := GetCompanyDatabase;
                       FMSSQLUser := GetCompanyUserName;
                       FMSSQLPassword := GetCompanyPassword;
                  end;
             end;
             if DoConectaMSSQL then
             begin
                  try
                     try
                        ListarTablas;
                        if lBorrarTablas then
                        begin
                             Forma.InitCounter( 2 * FLista.Count );
                             BorrarTablas;
                        end
                        else
                            Forma.InitCounter( FLista.Count );
                        MigrarTablas;
                        Forma.StopCounter;
                     except
                           on Error: Exception do
                           begin
                                Bitacora.AgregaError( 'Error Al Migrar Base De Datos: ' + CR_LF + Error.Message );
                           end;
                     end;
                  finally
                         DoDesconectaMSSQL;
                  end;
             end;
          finally
                 DoDesconectaInterbase;
          end;
     end;
     dStart := Now - dStart;
     sTiempo := '';
     if ( dStart >= 2 ) then
        sTiempo := sTiempo + Format( '%d D�as', [ Trunc( dStart ) ] )
     else
         if ( dStart >= 1 ) then
            sTiempo := sTiempo + '1 D�a';
     DecodeTime( dStart, iHoras, iMinutos, iSegundos, iMilisegundos );
     if ( iHoras > 0 ) then
        sTiempo := sTiempo + Format( ' %d Horas %d Minutos', [ iHoras, iMinutos ] )
     else
         if ( iMinutos > 0 ) then
            sTiempo := sTiempo + Format( ' %d Minutos %d Segundos', [ iMinutos, iSegundos ] )
         else
             sTiempo := sTiempo + Format( ' %d Segundos', [ iSegundos ] );
     Bitacora.AgregaResumen( GetTimeStamp + ': Migraci�n De Base De Datos Terminada' );
     Bitacora.AgregaResumen( 'Tiempo Total: ' + Trim( sTiempo ) );
end;

procedure TdmMigrarDatosIB.ConectaEmpresa;
begin
     with dmDBConfig do
     begin
          FMSSQLDatabase := GetCompanyDatabase;
          FMSSQLUser := GetCompanyUserName;
          FMSSQLPassword := GetCompanyPassword;
     end;
end;

procedure TdmMigrarDatosIB.ActualizaCodigoEmpresa(const CodigoAnt,CodigoActual:string;const Actualiza:Boolean);
begin
     ConectaEmpresa;
     if DoConectaMSSQL then
     begin
          try
             try
                ActualizarCodigo(CodigoAnt,CodigoActual,Actualiza)
             except
                   on Error: Exception do
                   begin
                        Bitacora.AgregaError( 'Error Al Actualizar Base De Datos: ' + CR_LF + Error.Message );
                   end;
             end;
          finally
                 DoDesconectaMSSQL;
          end;
     end;
end;

procedure TdmMigrarDatosIB.ActualizarCodigo(Const sCodigoAnt,sCodigoActual:string;const Actualiza:Boolean);
var
   sTabla:string;
const
     Q_UP_CM_CODIGO = 'update %s set CM_CODIGO = ''%s'' where CM_CODIGO = ''%s''';
     Q_DEL_CM_CODIGO = 'delete %s where CM_CODIGO = ''%s''';

     procedure ActualizaTabla;
     begin
           if Actualiza then
              AttachQuery( Format( Q_UP_CM_CODIGO, [ sTabla,sCodigoActual,sCodigoAnt ] ) )
           else
               AttachQuery( Format( Q_DEL_CM_CODIGO, [ sTabla,sCodigoActual ] ) );
           with hdbcMSSQL do
           begin
                StartTransact;
                try
                   oeTarget.ExecSQL;
                   Commit;
                   EndTransact;
                except
                   on Error: Exception do
                   begin
                        RollBack;
                        //Bitacora.AgregaError( 'Error al Actualizar ' + sTabla + CR_LF + Error.Message );
                   end;
                end;
                EndTransact;
           end;
     end;
begin
     sTabla := 'R_CLAS_ACC';
     ActualizaTabla;

     sTabla := 'R_ENT_ACC';
     ActualizaTabla;

     sTabla := 'GR_AD_ACC';
     ActualizaTabla;
end;

end.
