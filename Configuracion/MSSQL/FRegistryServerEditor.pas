unit FRegistryServerEditor;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ExtCtrls, FileCtrl, DB, DBTables, OVCL,
     ZetaRegistryServer,
     ZBaseDlgModal,
     ZetaDBTextBox, ZetaKeyCombo;

type
  TServerRegistryEditor = class(TZetaDlgModal)
    UserName: TEdit;
    UserNameLBL: TLabel;
    PasswordLBL: TLabel;
    Password: TEdit;
    PasswordCheck: TEdit;
    PasswordCheckLBL: TLabel;
    DatabaseLBL: TLabel;
    BuscarDatabase: TSpeedButton;
    Database: TZetaTextBox;
    TipoLogin: TZetaKeyCombo;
    TipoLoginLBL: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BuscarDatabaseClick(Sender: TObject);
    procedure PasswordChange(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
  private
    { Private declarations }
    FRegistry: TZetaRegistryServer;
  protected
    { Protected declarations }
    property Registry: TZetaRegistryServer read FRegistry;
    procedure LoadControls; virtual;
    procedure UnloadControls; virtual;
  public
    { Public declarations }
    function IsValid: Boolean;
  end;

var
  ServerRegistryEditor: TServerRegistryEditor;

function EspecificaComparte: Boolean;

implementation

uses FSQLDBLookup,
     ZetaServerTools,
     ZetaDialogo,
     ZetaAliasCrear,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaWinAPITools;

{$R *.DFM}

const
     {$ifdef PORTAL}
     K_ALIAS_COMPARTE = 'PortalODBC';
     {$else}
     K_ALIAS_COMPARTE = 'ComparteODBC';
     {$endif}

function EspecificaComparte: Boolean;
begin
     with TServerRegistryEditor.Create( Application ) do
     begin
          try
             ShowModal;
             Result := ( ModalResult = mrOK );
          finally
                 Free;
          end;
     end;
end;

{ ************* TServerRegistryEditor ******** }

procedure TServerRegistryEditor.FormCreate(Sender: TObject);
begin
     inherited;
     FRegistry := TZetaRegistryServer.Create;
     {$ifndef CNXCFG}
     TipoLogin.ListaFija := lfTipoLogin;
     {$else}
     TipoLogin.Visible := False;
     TipoLoginLBL.Visible := False;
     Self.Height := Self.Height - TipoLogin.Height; 
     {$endif}
end;


procedure TServerRegistryEditor.FormShow(Sender: TObject);
begin
     inherited;
     LoadControls;
     HelpContext := H50011_Indicando_la_base_de_datos;
end;

procedure TServerRegistryEditor.FormDestroy(Sender: TObject);
begin
     FRegistry.Free;
     inherited;
end;

function TServerRegistryEditor.IsValid: Boolean;
begin
     Result := FRegistry.IsValid;
end;

procedure TServerRegistryEditor.LoadControls;
begin
     with Registry do
     begin
          Self.Database.Caption := Database;
          Self.UserName.Text := UserName;
          Self.Password.Text := Password;
          Self.PasswordCheck.Text := Password;
          {$ifndef CNXCFG}
          Self.TipoLogin.Valor := TipoLogin;
          {$endif}
     end;
end;

procedure TServerRegistryEditor.UnloadControls;
begin
     with Registry do
     begin
          AliasComparte := K_ALIAS_COMPARTE;
          Database := Self.Database.Caption;
          UserName := Self.UserName.Text;
          Password := Self.Password.Text;
          {$ifndef CNXCFG}
          TipoLogin:= Self.TipoLogin.Valor;
          {$endif}
     end;
end;

procedure TServerRegistryEditor.PasswordChange(Sender: TObject);
begin
     inherited;
     PasswordCheck.Text := '';
end;

procedure TServerRegistryEditor.BuscarDatabaseClick(Sender: TObject);
var
   sDatabase: String;
begin
     inherited;
     sDatabase := Database.Caption;
     if FSQLDBLookup.LookupSQLDatabases( K_ALIAS_COMPARTE, UserName.Text, Password.Text, sDatabase ) then
        Database.Caption := sDatabase;
end;

procedure TServerRegistryEditor.OKClick(Sender: TObject);
begin
     inherited;
     if ( Password.Text = PasswordCheck.Text ) then
     begin
          UnloadControls;
		  ModalResult := mrOk;
          {$ifdef CNXCFG}
          ZInformation(Self.Caption,'Conexi�n Grabada Exitosamente',0);
          Close;
          {$endif}
     end
     else
     begin
          ZetaDialogo.zError( Caption, 'Clave de Acceso No Fu� Capturada Correctamente', 0 );
          ActiveControl := Password;
     end;
end;

procedure TServerRegistryEditor.CancelarClick(Sender: TObject);
begin
     inherited;
     Close;
end;

end.
