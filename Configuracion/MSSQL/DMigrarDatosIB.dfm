inherited dmMigrarDatosIB: TdmMigrarDatosIB
  Left = 383
  Top = 161
  Height = 479
  Width = 741
  object HdbcMSSQL: THdbc
    UserName = 'sa'
    ConnectionPooling = cpDefault
    Version = '5.07'
    BeforeConnect = HdbcMSSQLBeforeConnect
    Left = 32
    Top = 192
  end
  object oeTarget: TOEQuery
    hDbc = HdbcMSSQL
    hStmt.CursorType = 0
    hStmt.Target.Target = (
      'OE5.07'
      ''
      ''
      ''
      '')
    SQL.Strings = (
      'EXEC sp_fkeys @pktable_name = '#39'TAHORRO'#39)
    UniDirectional = True
    Params = <>
    Left = 112
    Top = 192
  end
  object oeCheckpoint: TOEQuery
    hDbc = HdbcMSSQL
    hStmt.CursorType = 0
    hStmt.Target.Target = (
      'OE5.07'
      ''
      ''
      ''
      '')
    SQL.Strings = (
      'checkpoint')
    UniDirectional = True
    Params = <>
    Left = 176
    Top = 192
  end
  object IBOTarget: TIBOQuery
    Params = <>
    IB_Connection = IBODatabase
    RecordCountAccurate = True
    FieldOptions = []
    Left = 120
    Top = 256
  end
  object IBODelete: TIBOQuery
    Params = <>
    IB_Connection = IBODatabase
    RecordCountAccurate = True
    FieldOptions = []
    Left = 184
    Top = 256
  end
  object IBODatabase: TIBODatabase
    Left = 32
    Top = 256
  end
end
