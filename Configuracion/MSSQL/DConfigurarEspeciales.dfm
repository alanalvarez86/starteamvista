object dmConfigurarEspeciales: TdmConfigurarEspeciales
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 749
  Top = 338
  Height = 376
  Width = 521
  object OESchema: TOESchema
    hDbc = Hdbc
    Tables.Tables = (
      'OE5.07'
      ())
    Views.Views = (
      'OE5.07'
      ())
    ExecMarker = 'go'
    NameConstraints = False
    OnProgress = OESchemaProgress
    Left = 20
    Top = 8
  end
  object Hdbc: THdbc
    ConnectionPooling = cpDefault
    Version = '5.07'
    Left = 24
    Top = 72
  end
  object ADOConnection: TADOConnection
    CommandTimeout = 0
    LoginPrompt = False
    Provider = 'Windows Search Data Source'
    Left = 104
    Top = 72
  end
  object ADOCommand: TADOCommand
    CommandTimeout = 0
    Connection = ADOConnection
    Parameters = <>
    Left = 104
    Top = 16
  end
  object HstmtScript: THstmt
    hDbc = Hdbc
    SQLParsing = False
    Left = 24
    Top = 144
  end
end
