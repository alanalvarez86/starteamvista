inherited EmpresaEdit: TEmpresaEdit
  Left = 292
  Top = 196
  Caption = 'EmpresaEdit'
  ClientHeight = 505
  ClientWidth = 455
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 81
    Top = 12
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = '&C'#243'digo:'
    FocusControl = CM_CODIGO
  end
  object Label2: TLabel [1]
    Left = 77
    Top = 34
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = '&Nombre:'
    FocusControl = CM_NOMBRE
  end
  object lblAcumula: TLabel [2]
    Left = 64
    Top = 79
    Width = 53
    Height = 13
    Alignment = taRightJustify
    Caption = 'Acu&mula a:'
    FocusControl = CM_ACUMULA
  end
  object lblSumar: TLabel [3]
    Left = 26
    Top = 101
    Width = 91
    Height = 13
    Alignment = taRightJustify
    Caption = '&Sumar al Acumular:'
    FocusControl = CM_EMPATE
  end
  object CM_TIPO_Elbl: TLabel [4]
    Left = 93
    Top = 56
    Width = 24
    Height = 13
    Alignment = taRightJustify
    Caption = '&Tipo:'
    FocusControl = CM_TIPO_E
  end
  object lblDigitoGafetes: TLabel [5]
    Left = 30
    Top = 156
    Width = 87
    Height = 13
    Alignment = taRightJustify
    Caption = 'D'#237'gito en &Gafetes:'
    FocusControl = CM_DIGITO
  end
  object lblReportesKiosco: TLabel [6]
    Left = 21
    Top = 310
    Width = 96
    Height = 13
    Alignment = taRightJustify
    Caption = '&Reportes en Kiosco:'
    FocusControl = CM_KCLASIFI
  end
  object gbConfidencialidad: TGroupBox [7]
    Left = 24
    Top = 177
    Width = 412
    Height = 127
    Caption = ' Conf&idencialidad: '
    TabOrder = 12
    object btSeleccionarConfiden: TSpeedButton
      Left = 380
      Top = 48
      Width = 23
      Height = 23
      Hint = 'Seleccionar Confidencialidad'
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C00001F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C00001F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C00001F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C007C007C00001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C007C007C007C007C007C007C00001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1042007C007C00001F7C007C007C007C00001F7C1F7C1F7C
        1F7C1F7C1F7C1042007C00001F7C1F7C1F7C1F7C007C007C00001F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C00001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C00001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C00001F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1042007C0000
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1042007C
        00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        007C007C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      ParentShowHint = False
      ShowHint = True
      OnClick = btSeleccionarConfidenClick
    end
    object listaConfidencialidad: TListBox
      Left = 94
      Top = 48
      Width = 285
      Height = 69
      TabStop = False
      ExtendedSelect = False
      ItemHeight = 13
      TabOrder = 0
    end
    object rbConfidenNinguna: TRadioButton
      Left = 94
      Top = 11
      Width = 131
      Height = 17
      Caption = 'Sin Confidencialidad'
      TabOrder = 1
      OnClick = rbConfidenNingunaClick
    end
    object rbConfidenAlgunas: TRadioButton
      Left = 94
      Top = 28
      Width = 156
      Height = 17
      Caption = 'Aplica algunas'
      TabOrder = 2
      OnClick = rbConfidenAlgunasClick
    end
  end
  inherited PanelBotones: TPanel
    Top = 469
    Width = 455
    TabOrder = 11
    inherited OK: TBitBtn
      Left = 287
      ModalResult = 0
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 372
      ModalResult = 0
      OnClick = CancelarClick
      Kind = bkCustom
    end
    object DBNavigator: TDBNavigator
      Left = 5
      Top = 5
      Width = 108
      Height = 25
      DataSource = DataSource
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
      TabOrder = 2
    end
  end
  object DBGroupBox: TGroupBox
    Left = 0
    Top = 335
    Width = 455
    Height = 134
    Align = alBottom
    Caption = ' Base de Datos  '
    TabOrder = 10
    object Label8: TLabel
      Left = 50
      Top = 114
      Width = 65
      Height = 13
      Alignment = taRightJustify
      Caption = '&Alias de BDE:'
    end
    object Label9: TLabel
      Left = 21
      Top = 22
      Width = 94
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nom&bre de Usuario:'
      FocusControl = CM_USRNAME
    end
    object Label5: TLabel
      Left = 31
      Top = 45
      Width = 84
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cla&ve de Acceso:'
    end
    object PasswordCheckLBL: TLabel
      Left = 51
      Top = 68
      Width = 64
      Height = 13
      Alignment = taRightJustify
      Caption = 'Con&firmaci'#243'n:'
      FocusControl = PasswordCheck
    end
    object BuscarServidor: TSpeedButton
      Left = 397
      Top = 86
      Width = 23
      Height = 23
      Hint = 'Buscar Base de Datos de Microsoft SQL Server'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333333FF33333333FF333993333333300033377F3333333777333993333333
        300033F77FFF3333377739999993333333333777777F3333333F399999933333
        33003777777333333377333993333333330033377F3333333377333993333333
        3333333773333333333F333333333333330033333333F33333773333333C3333
        330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
        993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
        333333333337733333FF3333333C333330003333333733333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      OnClick = BuscarServidorClick
    end
    object Label6: TLabel
      Left = 40
      Top = 90
      Width = 75
      Height = 13
      Alignment = taRightJustify
      Caption = '&Base De Datos:'
    end
    object CM_DATOS: TZetaDBTextBox
      Left = 118
      Top = 87
      Width = 276
      Height = 20
      AutoSize = False
      Caption = 'CM_DATOS'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CM_DATOS'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CM_ALIAS: TZetaDBTextBox
      Left = 118
      Top = 110
      Width = 276
      Height = 20
      AutoSize = False
      Caption = 'CM_ALIAS'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CM_ALIAS'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CM_USRNAME: TDBEdit
      Left = 118
      Top = 18
      Width = 145
      Height = 21
      DataField = 'CM_USRNAME'
      DataSource = DataSource
      TabOrder = 0
    end
    object PasswordCheck: TEdit
      Left = 118
      Top = 64
      Width = 145
      Height = 21
      PasswordChar = '*'
      TabOrder = 2
    end
    object CM_PASSWRD: TDBEdit
      Left = 118
      Top = 41
      Width = 145
      Height = 21
      DataField = 'CM_PASSWRD'
      DataSource = DataSource
      PasswordChar = '*'
      TabOrder = 1
      OnChange = CM_PASSWRDChange
    end
  end
  object CM_NOMBRE: TDBEdit
    Left = 119
    Top = 30
    Width = 289
    Height = 21
    DataField = 'CM_NOMBRE'
    DataSource = DataSource
    TabOrder = 1
  end
  object CM_EMPATE: TZetaDBNumero
    Left = 119
    Top = 97
    Width = 50
    Height = 21
    Mascara = mnDias
    TabOrder = 4
    Text = '0'
    DataField = 'CM_EMPATE'
    DataSource = DataSource
  end
  object CM_ACUMULA: TZetaDBKeyCombo
    Left = 119
    Top = 75
    Width = 288
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 3
    ListaFija = lfNinguna
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'CM_ACUMULA'
    DataSource = DataSource
    LlaveNumerica = False
  end
  object CM_USACAFE: TDBCheckBox
    Left = 45
    Top = 118
    Width = 87
    Height = 17
    Alignment = taLeftJustify
    Caption = '&Usa Cafeter'#237'a:'
    DataField = 'CM_USACAFE'
    DataSource = DataSource
    TabOrder = 5
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  object CM_TIPO_E: TComboBox
    Left = 119
    Top = 52
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 2
    OnChange = CM_TIPO_EChange
  end
  object CM_DIGITO: TDBEdit
    Left = 118
    Top = 152
    Width = 23
    Height = 21
    DataField = 'CM_DIGITO'
    DataSource = DataSource
    MaxLength = 1
    TabOrder = 8
    OnKeyPress = CM_DIGITOKeyPress
  end
  object CM_KCLASIFI: TZetaKeyCombo
    Left = 118
    Top = 306
    Width = 288
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 9
    OnChange = CM_KCLASIFIChange
    ListaFija = lfNinguna
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
  end
  object CM_CODIGO: TZetaDBEdit
    Left = 119
    Top = 8
    Width = 142
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 0
    Text = 'CM_CODIGO'
    ConfirmEdit = True
    DataField = 'CM_CODIGO'
    DataSource = DataSource
  end
  object CM_NIVEL0: TCheckListBox
    Left = 486
    Top = 247
    Width = 289
    Height = 74
    Columns = 1
    ItemHeight = 13
    TabOrder = 7
    Visible = False
  end
  object CM_USACASE: TDBCheckBox
    Left = 56
    Top = 134
    Width = 76
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Usa Ca&seta:'
    DataField = 'CM_USACASE'
    DataSource = DataSource
    TabOrder = 6
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  object DataSource: TDataSource
    OnStateChange = DataSourceStateChange
    OnDataChange = DataSourceDataChange
    Left = 184
    Top = 105
  end
end
