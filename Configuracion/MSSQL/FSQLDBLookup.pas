unit FSQLDBLookup;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls,
     ZBaseDlgModal, OCL, Db, ODSI, OVCL;

type
  TSQLServerDBLookup = class(TZetaDlgModal)
    Hdbc: THdbc;
    OEQuery: TOEQuery;
    ServerLBL: TLabel;
    BuscarServidor: TSpeedButton;
    Server: TEdit;
    DBList: TComboBox;
    Label1: TLabel;
    OEAdministrator: TOEAdministrator;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BuscarServidorClick(Sender: TObject);
    procedure ServerExit(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
    FUser: String;
    FPassword: String;
    FServer: String;
    FDatasource: String;
    function Conectar: Boolean;
    function GetDatabase: String;
    function GetSQLServer: String;
    function CrearODBCDataSource( const sDatasource, sServer, sDatabase: String ): Boolean;
    procedure SetDatabase(const Value: String);
  public
    { Public declarations }
    property Database: String read GetDatabase write SetDatabase;
    property Datasource: String read FDatasource write FDatasource;
    property Password: String read FPassword write FPassword;
    property SQLServer: String read GetSQLServer;
    property User: String read FUser write FUser;
  end;

var
  SQLServerDBLookup: TSQLServerDBLookup;

function LookupSQLDatabases( const sDatasource, sUser, sPassword: String; var sDatabase: String ): Boolean;

implementation

uses ZetaDialogo,
     ZetaNetworkBrowser,
     ZetaServerTools,
     ZetaWinAPITools,
     ZetaODBCTools,
     ZetaCommonTools;

function LookupSQLDatabases( const sDatasource, sUser, sPassword: String; var sDatabase: String ): Boolean;
begin
     with TSQLServerDBLookup.Create( Application ) do
     begin
          try
             User := sUser;
             Password := sPassword;
             Database := sDatabase;
             Datasource := sDatasource;
             ShowModal;
             Result := ( ModalResult = mrOk );
             if Result then
             begin
                  if ( SQLServer <> '' ) and ( Database <> '' ) then
                     sDatabase := Format( '%s.%s', [ SQLServer, Database ] )
                  else
                      if ( SQLServer <> '' ) then
                         sDatabase := SQLServer
                      else
                          if ( Database <> '' ) then
                             sDatabase := Database;
             end;
          finally
                 Free;
          end;
     end;
end;

{$R *.DFM}

{ ******** TSQLServerDBLookup ******** }

procedure TSQLServerDBLookup.FormCreate(Sender: TObject);
begin
     inherited;
     FServer := '';
end;

procedure TSQLServerDBLookup.FormShow(Sender: TObject);
begin
     inherited;
     with Server do
     begin
          if ( Text = '' ) then
          begin
               Text := ZetaWinAPITools.GetComputerName;
          end;
     end;
end;

function TSQLServerDBLookup.Conectar: Boolean;
const
     K_LISTA_TABLAS = 'select DB_NAME(dbid) as DB_NAME from MASTER.DBO.SYSDATABASES where ( DB_NAME( DBID ) <> ''master'' ) order by DB_NAME';
     K_IS_SYSADMIN = 'select IS_SRVROLEMEMBER (''sysadmin'', ''%s'' ) as IS_MEMBER';
var
   oCursor: TCursor;
   sServer: String;
   lSysAdmin: Boolean;
begin
     sServer := GetSQLServer;
     if ( sServer = FServer ) then
        Result := True
     else
         if ( sServer = '' ) then
         begin
              Result := True;
              DBList.Items.Clear;
         end
         else
         begin
              Result := False;
              oCursor := Screen.Cursor;
              Screen.Cursor := crHourglass;
              try
                 try
                    with HDBC do
                    begin
                         Driver := 'SQL Server';
                         UserName := FUser;
                         Password := FPassword;
                         with Attributes do
                         begin
                              Clear;
                              Add( Format( 'Server=%s', [ sServer ] ) );
                         end;
                         Connected := True;
                    end;
                    with OEQuery do
                    begin
                         Active := False;
                         with SQL do
                         begin
                              Clear;
                              Add( K_LISTA_TABLAS );
                         end;
                         Active := True;
                         with DBList do
                         begin
                              with Items do
                              begin
                                   BeginUpdate;
                                   Clear;
                                   while not Eof do
                                   begin
                                        Add( FieldByName( 'DB_NAME' ).AsString );
                                        Next;
                                   end;
                                   EndUpdate;
                                   if ( Count > 0 ) then
                                      ItemIndex := 0;
                              end;
                         end;
                         Active := False;
                         with SQL do
                         begin
                              Clear;
                              Add( Format( K_IS_SYSADMIN, [ FUser ] ) );
                         end;
                         Active := True;
                         lSysAdmin := ( Fields[ 0 ].AsInteger = 1 );
                         Active := False;
                    end;
                    if not lSysAdmin then
                    begin
                         ZetaDialogo.zError( '� Usuario Inv�lido ', Format( 'El Usuario "%s" No Es Miembro del SYSADMIN (Rol Administrativo de SQL Server)', [ FUser ] ), 0 );
                    end;
                    Result := True;
                 except
                       on Error: EODBC do
                       begin
                            EODBC( Error ).Detail( 'Error Al Conectar A Servidor De MS SQL' );
                       end;
                       on Error: Exception do
                       begin
                            Application.HandleException( Error );
                       end;
                 end;
                 FServer := sServer;
                 with HDBC do
                 begin
                      Connected := False;
                 end;
              finally
                     Screen.Cursor := oCursor;
              end;
         end;
end;

function TSQLServerDBLookup.CrearODBCDataSource( const sDatasource, sServer, sDatabase: String ): Boolean;
begin
     {
     Result := ZetaODBCTools.SetSQLServerDataSource( K_ALIAS_COMPARTE, 'Tress Comparte', Server.Text, Database.Text, sError );
     }
     try
        with OEAdministrator do
        begin
             DataSource := sDatasource;
             Driver := MSSQL_DRIVER;
             ZetaODBCTools.SetSQLServerAttributes( Attributes, sServer, sDatabase );
             if not Modify then
                Add;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                Result := False;
           end;
     end;
end;

function TSQLServerDBLookup.GetSQLServer: String;
begin
     Result := Server.Text;
end;

function TSQLServerDBLookup.GetDatabase: String;
begin
     with DBList do
     begin
          Result := Items.Strings[ ItemIndex ];
     end;
end;

procedure TSQLServerDBLookup.SetDatabase(const Value: String);
var
   sServer, sDatabase: String;
   iPtr: Integer;
begin
     ZetaServerTools.GetServerDatabase( Value, sServer, sDatabase );
     Server.Text := sServer;
     if Conectar then
     begin
          with DBList do
          begin
               iPtr := Items.IndexOf( sDatabase );
               ItemIndex := iPtr;
          end;
     end;
end;

procedure TSQLServerDBLookup.BuscarServidorClick(Sender: TObject);
begin
     inherited;
     with Server do
     begin
          Text := ZetaNetworkBrowser.GetServerName( Text );
     end;
end;

procedure TSQLServerDBLookup.ServerExit(Sender: TObject);
begin
     inherited;
     if not Conectar then
        ActiveControl := Server;
end;

procedure TSQLServerDBLookup.OKClick(Sender: TObject);
var
   sServer, sDatabase: String;
   lOk: Boolean;
begin
     inherited;
     sServer := GetSQLServer;
     sDatabase := GetDatabase;
     lOk := False;
     if ZetaCommonTools.StrVacio( sServer ) then
     begin
          ZetaDialogo.zError( Caption, 'No Se Especific� El Servidor', 0 );
          ActiveControl := Server;
     end
     else
         if ZetaCommonTools.StrVacio( sDatabase ) then
         begin
              ZetaDialogo.zError( Caption, 'No Se Especific� La Base De Datos', 0 );
              ActiveControl := DBList;
         end
         else
             if ( AnsiLowerCase( sDatabase ) = 'master' ) then
             begin
                  ZetaDialogo.zError( Caption, 'No Se Puede Usar La Base De Datos Maestra ( master )', 0 );
                  ActiveControl := DBList;
             end
             else
                 lOk := True;
     if lOk then
     begin
          if CrearODBCDataSource( FDatasource, sServer, sDatabase ) then
          begin
               ModalResult := mrOk;
          end
          else
          begin
               ZetaDialogo.zError( Caption, 'Error Al Configurar ODBC Datasource ' + FDatasource, 0 );
               ActiveControl := Server;
          end;
     end;
end;

end.
