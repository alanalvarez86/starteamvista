program TressCfg;

uses
  Forms,
  FEmpresas in '..\FEmpresas.pas' {Empresas},
  FMigrar in '..\FMigrar.pas' {Migrar},
  DMigrar in '..\DMigrar.pas' {dmMigrar: TDataModule},
  FAcercaDe in '..\..\Linx.5\LinxBase\FAcercaDe.pas' {AcercaDe},
  ZBaseDlgModal in '..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  ZWizardBasico in '..\..\Tools\ZWizardBasico.pas' {WizardBasico},
  FImportaReportesXML in '..\FImportaReportesXML.pas',
  FAutoServerLoader in '..\FAutoServerLoader.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Configurador de Tress';
  Application.HelpFile := 'Tresscfg.chm';
  Application.CreateForm(TEmpresas, Empresas);
  Application.Run;
end.
