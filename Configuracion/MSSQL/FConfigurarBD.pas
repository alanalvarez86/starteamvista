unit FConfigurarBD;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, OCL, OCI, OVCL, ComCtrls, StdCtrls, Buttons, ExtCtrls,
     FMigrar,
     ZetaDBTextBox,
     ZetaWizard, jpeg;

type
  TConfigurarBD = class(TMigrar)
    OESchema: TOESchema;
    Hdbc: THdbc;
    OpenDialog: TOpenDialog;
    ArchivoLBL: TLabel;
    Archivo: TEdit;
    ArchivoBuscar: TSpeedButton;
    DBDataGB: TGroupBox;
    EmpresaLBL: TLabel;
    Empresa: TZetaTextBox;
    BaseDeDatos: TZetaTextBox;
    BaseDeDatosLBL: TLabel;
    UsuarioLBL: TLabel;
    Usuario: TZetaTextBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ArchivoBuscarClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
    procedure OESchemaProgress(Sender: TObject; Info: String);
  private
    { Private declarations }
    FTipo: Integer;
    FUserName: String;
    FPassword: String;
    FCompany: String;
    FDatabase: String;
    FAlias: String;
    FOK: Boolean;
    function ConectaMSSQL: Boolean;
    function GetScriptFile: String;
    function ProcesaScript: Boolean;
    procedure CounterClear(Sender: TObject; var Continue: Boolean);
    procedure DesconectaMSSQL;
    procedure CounterMove(Sender: TObject; var lOk: Boolean);
    procedure SetScriptFile(const Value: String);
    procedure CounterStart(Sender: TObject; const sMsg: String);
  protected
    { Protected declarations }
    property ScriptFile: String read GetScriptFile write SetScriptFile;
  public
    { Public declarations }
    property Tipo: Integer read FTipo write FTipo;
    property OK: Boolean read FOK;
    property Alias: String read FAlias write FAlias;
    property Database: String read FDatabase write FDatabase;
    property Company: String read FCompany write FCompany;
    property Password: String read FPassword write FPassword;
    property UserName: String read FUserName write FUserName;
  end;

var
  ConfigurarBD: TConfigurarBD;

implementation

uses ZetaDialogo,
     ZetaTressCFGTools,
     ZetaServerTools,
     ZetaCommonTools,
     ZetaCommonClasses,
     FHelpContext,
     DReportes;

{$R *.DFM}

procedure TConfigurarBD.FormCreate(Sender: TObject);
begin
     inherited;
     dmMigracion := TdmReportes.Create( Self );
     FOk := False;
end;

procedure TConfigurarBD.FormShow(Sender: TObject);
begin
     inherited;
     LogFileName := ZetaTressCFGTools.SetFileNameDefaultPath( 'ConfigurarBD.log' );
     ScriptFile := ZetaTressCFGTools.SetFileNameDefaultPath( 'Fuentes\Patch\DB_MSSQL.DB' );
     if ( FTipo < 0 ) then
     begin
          HelpContext := H50013_Configurando_la_estructura_de_Comparte;
          Caption := 'Configurar Estructura De Comparte';
          with MensajeInicial.Lines do
          begin
               BeginUpdate;
               try
                  Clear;
                  Add( '' );
                  Add( 'Este Proceso Configura La Estructura De' );
                  Add( 'La Base De Datos COMPARTE' );
                  Add( '' );
                  Add( 'Todas Las Tablas e Indices Ser�n Creados' );
                  Add( '' );
                  Add( 'Se Requiere Que Esta Base De Datos No' );
                  Add( 'Contenga Ninguna Estructura Previa' );
               finally
                      EndUpdate;
               end;
          end;
     end
     else
     begin
          HelpContext := H50014_Configurando_la_estructura_de_una_base_de_datos;
          Caption := 'Configurar Estructura De Una Empresa';
          with MensajeInicial.Lines do
          begin
               BeginUpdate;
               try
                  Clear;
                  Add( '' );
                  Add( 'Este Proceso Configura La Estructura De' );
                  Add( 'La Base De Datos de la Empresa' );
                  Add( '' );
                  Add( Company );
                  Add( '' );
                  Add( 'Todas Las Tablas e Indices Ser�n Creados' );
                  Add( '' );
                  Add( 'Se Requiere Que Esta Base De Datos No' );
                  Add( 'Contenga Ninguna Estructura Previa' );
               finally
                      EndUpdate;
               end;
          end;
     end;
     Empresa.Caption := Company;
     BaseDeDatos.Caption := Database;
     Usuario.Caption := UserName;
end;

procedure TConfigurarBD.FormDestroy(Sender: TObject);
begin
     FreeAndNil( dmMigracion );
     inherited;
end;

function TConfigurarBD.GetScriptFile: String;
begin
     Result := Archivo.Text;
end;

procedure TConfigurarBD.SetScriptFile(const Value: String);
begin
     Archivo.Text := Value;
end;

function TConfigurarBD.ConectaMSSQL: Boolean;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := oCursor;
     try
        try
           with Hdbc do
           begin
                Connected := False;
                Datasource := FAlias;
                UserName := FUserName;
                Password := FPassword;
                BeforeConnect := dmMigracion.HdbcBeforeConnect;
                Connected := True;
           end;
           Result := True;
        except
              on Error: Exception do
              begin
                   Application.HandleException( Error );
                   Result := False;
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TConfigurarBD.DesconectaMSSQL;
begin
     hdbc.Connected := False;
end;

function TConfigurarBD.ProcesaScript: Boolean;
var
   dStart, dWait: TDateTime;
begin
     with TdmReportes( dmMigracion ) do
     begin
          ConfiguraBaseDeDatosMsg( Format( 'Corriendo Script %s', [ VerActual ] ) );
          try
             OESchema.RunScript( ItemName );
             Result := True;
          except
                on Error: Exception do
                begin
                     Error.Message := 'Script: ' + VerActual + CR_LF + Error.Message;
                     ConfiguraBaseDeDatosError( '� Error Al Procesar Script !', Error );
                     Result := False;
                end;
          end;
          ConfiguraBaseDeDatosMsg( Format( 'Script %s Fu� Ejecutado', [ VerActual ] ) );
     end;
     { GA: 01/Ago/2002: Parece que es necesario dejar pasar unos segundos entre }
     { la ejecuci�n de cada script para permitirle al servidor actualizar su schema }
     dWait := EncodeTime( 0, 0, 3, 0 ); { 3 Segundos }
     dStart := Now;
     while True do
     begin
          if ( ( Now - dStart ) > dWait ) then
             Break;
     end;
end;

procedure TConfigurarBD.OESchemaProgress(Sender: TObject; Info: String);
begin
     inherited;
     TdmReportes( dmMigracion ).ConfiguraBaseDeDatosMsg( Info );
end;

procedure TConfigurarBD.CounterStart(Sender: TObject; const sMsg: String);
begin
     with AvanceProceso do
     begin
          Max := TdmReportes( dmMigracion ).GetConfigureBDSteps;
          Step := 1;
          Position := 0;
     end;
end;

procedure TConfigurarBD.CounterMove(Sender: TObject; var lOk: Boolean);
begin
     StatusBar.Panels[ 0 ].Text := TdmReportes( dmMigracion ).VerActual;
     Application.ProcessMessages;
     ProcesaScript;
     with AvanceProceso do
     begin
          StepIt;
     end;
     UpdateLogDisplay;
     Application.ProcessMessages;
     lOk := PuedeContinuar;
end;

procedure TConfigurarBD.CounterClear(Sender: TObject; var Continue: Boolean);
begin
     UpdateLogDisplay;
     with AvanceProceso do
     begin
          Position := Max;
     end;
end;

procedure TConfigurarBD.ArchivoBuscarClick(Sender: TObject);
var
   sFileName: String;
begin
     sFileName := ScriptFile;
     with OpenDialog do
     begin
          FileName := sFileName;
          InitialDir := ExtractFilePath( sFileName );
          if Execute then
             ScriptFile := FileName;
     end;
end;

procedure TConfigurarBD.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     with Wizard do
     begin
          if Adelante then
          begin
               if EsPaginaActual( DirectorioDOS ) then
               begin
                    if not FileExists( ScriptFile ) then
                    begin
                         ZetaDialogo.ZError( Self.Caption, 'Archivo De Scripts No Existe', 0 );
                         CanMove := False;
                         ActiveControl := Archivo;
                    end;
               end;
          end;
     end;
end;

procedure TConfigurarBD.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
var
   oCursor: TCursor;
begin
     FOk := False;
     StartLog;
     if ConectaMSSQL then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             try
                with TdmReportes( dmMigracion ) do
                begin
                     StartCallBack := Self.CounterStart;
                     CallBack := Self.CounterMove;
                     EndCallBack := Self.CounterClear;
                     lOk := ConfiguraBaseDeDatos( ScriptFile, FDatabase, Tipo + 1 );
                end;
             except
                   on Error: Exception do
                   begin
                        TdmReportes( dmMigracion ).ConfiguraBaseDeDatosError( '� Error Al Configurar !', Error );
                        lOk := False;
                   end;
             end;
          finally
                 DesconectaMSSQL;
                 Screen.Cursor := oCursor;
          end;
     end;
     EndLog;
     EndProcess;
     inherited;
     FOk := lOk;
end;

end.
