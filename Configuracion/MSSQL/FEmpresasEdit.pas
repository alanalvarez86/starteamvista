unit FEmpresasEdit;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Mask, DBCtrls, ExtCtrls, Buttons, Db,
     ZBaseDlgModal,
     ZetaKeyCombo,
     ZetaNumero,
     ZetaDBTextBox,
     DDBConfig,
     ZetaCommonLists,
     ZetaMigrar,
     DReportes, ZetaEdit, CheckLst, FSeleccionarConfidencialidad;

type
  TEmpresaEdit = class(TZetaDlgModal)
    DBGroupBox: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    Label5: TLabel;
    PasswordCheckLBL: TLabel;
    BuscarServidor: TSpeedButton;
    Label6: TLabel;
    CM_USRNAME: TDBEdit;
    PasswordCheck: TEdit;
    CM_PASSWRD: TDBEdit;
    Label1: TLabel;
    Label2: TLabel;
    lblAcumula: TLabel;
    lblSumar: TLabel;
    CM_NOMBRE: TDBEdit;
    CM_EMPATE: TZetaDBNumero;
    CM_ACUMULA: TZetaDBKeyCombo;
    CM_USACAFE: TDBCheckBox;
    DBNavigator: TDBNavigator;
    DataSource: TDataSource;
    CM_DATOS: TZetaDBTextBox;
    CM_ALIAS: TZetaDBTextBox;
    CM_TIPO_Elbl: TLabel;
    CM_TIPO_E: TComboBox;
    lblDigitoGafetes: TLabel;
    CM_DIGITO: TDBEdit;
    lblReportesKiosco: TLabel;
    CM_KCLASIFI: TZetaKeyCombo;
    CM_CODIGO: TZetaDBEdit;
    CM_NIVEL0: TCheckListBox;
    btSeleccionarConfiden: TSpeedButton;
    listaConfidencialidad: TListBox;
    gbConfidencialidad: TGroupBox;
    rbConfidenNinguna: TRadioButton;
    rbConfidenAlgunas: TRadioButton;
    CM_USACASE: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure DataSourceStateChange(Sender: TObject);
    procedure BuscarServidorClick(Sender: TObject);
    procedure CM_PASSWRDChange(Sender: TObject);
    procedure CM_TIPO_EChange(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure CM_DIGITOKeyPress(Sender: TObject; var Key: Char);
    procedure SetControlesDigito;
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure CM_KCLASIFIChange(Sender: TObject);
    procedure btSeleccionarConfidenClick(Sender: TObject);
    procedure rbConfidenNingunaClick(Sender: TObject);
    procedure rbConfidenAlgunasClick(Sender: TObject);
  private
    { Private declarations }
    dmDBConfig: TdmDBConfig;
    dmReportes: TdmReportes;
    FModo: TDataSetState;
    lValoresConfidencialidad : TStringList;

    procedure SetControls( const lEnabled: Boolean );
    procedure SetModo( Value: TDataSetState );
    procedure SetDBConfig( Value: TdmDBConfig );
    procedure ActualizaComboClasifi( eClasifi: eClasifiReporte );
    procedure SetClasifiKiosco;
    procedure ConectaClasificaciones;

    procedure SetListConfidencialidad( sValores : string);
    procedure GetConfidencialidad;

  public
    { Public declarations }
    property DBConfig: TdmDBConfig write SetDBConfig;
    property Modo: TDataSetState read FModo write SetModo;
  end;

var
  EmpresaEdit: TEmpresaEdit;

implementation

uses FSQLDBLookup,
     FHelpContext,
     ZetaCommonTools,
     ZetaDialogo,
     ZetaCommonClasses;

const
     {Funciona de forma similar a crFavoritos}
     crSinRestriccion = MaxInt;
     K_SIN_RESTRICCION = -1;

{$R *.DFM}

procedure TEmpresaEdit.FormCreate(Sender: TObject);
begin
     inherited;
     ZetaCommonLists.LlenaLista( lfTipoCompanyName, CM_TIPO_E.Items );
     HelpContext := H00020_Agregar_y_borrar_empresas;
     dmReportes := TdmReportes.Create( Self );
     lValoresConfidencialidad := TStringList.Create;
end;

procedure TEmpresaEdit.FormDestroy(Sender: TObject);
begin
     inherited;
     TdmReportes( dmReportes ).Free;
     FreeAndNil( lValoresConfidencialidad ) ;
end;

procedure TEmpresaEdit.FormShow(Sender: TObject);
begin
     inherited;
     ConectaClasificaciones;
end;

procedure TEmpresaEdit.SetDBConfig( Value: TdmDBConfig );
begin
     if ( dmDBConfig <> Value ) then
     begin
          dmDBConfig := Value;
          if ( dmDBConfig <> nil ) then
          begin
               with dmDBConfig do
               begin
                    LlenaListaCompanias( CM_ACUMULA.Lista );
//                    LlenaListaNivel0( CM_NIVEL0.Lista );
                    LeeListaNivel0( lValoresConfidencialidad );

                    ConectarCompany( DataSource );
               end;
          end;
     end;
end;

procedure TEmpresaEdit.SetModo( Value: TDataSetState );
begin
     if ( FModo <> Value ) then
     begin
          FModo := Value;
          with dmDBConfig do
          begin
               case Modo of
                    dsEdit: ModificaCompany;
                    dsInsert: AgregaCompany;
               end;
          end;
     end;
end;

procedure TEmpresaEdit.SetControls( const lEnabled: Boolean );
begin
     with PasswordCheck do
     begin
          if lEnabled then
             Text := VACIO;
          Enabled := lEnabled;
          PasswordCheckLBL.Enabled := Enabled;
     end;
end;

procedure TEmpresaEdit.DataSourceDataChange(Sender: TObject; Field: TField);
var
   eTipo : eTipoCompany;
begin
     inherited;
     if ( Field = nil ) then
     begin
          PasswordCheck.Text := CM_PASSWRD.Text;
          CM_TIPO_E.Text := dmDBConfig.GetTipoEmpresaEdit;
          SetControls( False );
          SetControlesDigito;

          with DataSource.DataSet do
          begin
               if ( State in [dsBrowse] ) then
               begin
                  ConectaClasificaciones;
                  GetConfidencialidad;
               end
               else if (State in [dsInsert]) then
                    rbConfidenNinguna.Checked := TRUE;

               CM_TIPO_E.Enabled := True;
               if ( State in [dsBrowse, dsEdit] ) then
               begin
                   eTipo := dmDBConfig.GetTipoEmpresaReal;
                   CM_TIPO_E.Enabled := ( eTipo <> tc3Prueba );
               end;
          end;
     end;
end;

procedure TEmpresaEdit.DataSourceStateChange(Sender: TObject);
var
   ModalResultBuffer: TModalResult;
   lEdit: Boolean;
begin
     inherited;
     with DataSource do
     begin
          case State of
               dsEdit: Caption := 'Modificar ' + dmDBConfig.GetCompanyName;
               dsInsert: Caption := 'Agregar Compa��a';
          else
              Caption := 'Consultar Compa��a';
          end;
          lEdit := ( State in [ dsEdit, dsInsert ] );
     end;
     OK.Enabled := lEdit;
     DBNavigator.Enabled := not lEdit;
     with Cancelar do
     begin
          ModalResultBuffer := ModalResult;
          if lEdit then
          begin
               Kind := bkCancel;
               Cancel := False;
               Caption := '&Cancelar';
          end
          else
          begin
               Kind := bkClose;
               Cancel := True;
               Caption := '&Salir';
          end;
          ModalResult := ModalResultBuffer;
     end;
end;

procedure TEmpresaEdit.CM_PASSWRDChange(Sender: TObject);
begin
     inherited;
     SetControls( True );
end;

procedure TEmpresaEdit.CM_TIPO_EChange(Sender: TObject);
begin
     inherited;
     dmDBConfig.CambiaTipo( CM_TIPO_E.Text );
     SetControlesDigito;
end;



procedure TEmpresaEdit.SetClasifiKiosco;
var
   iClasifi: Integer;
begin
     with CM_KCLASIFI, DataSource.DataSet do
     begin
          { Cuando es sin restricci�n, se graba como -1 }
          iClasifi:=  Integer(Items.Objects[ItemIndex]);
          if  iClasifi = crSinRestriccion then
            FieldByName('CM_KCLASIFI').AsInteger:= K_SIN_RESTRICCION
          else
              FieldByName('CM_KCLASIFI').AsInteger := iClasifi;
     end;
end;

procedure TEmpresaEdit.OKClick(Sender: TObject);
begin
     inherited;
     if ( CM_PASSWRD.Text <> PasswordCheck.Text ) then
        ZetaDialogo.zError( Caption, 'Clave De Acceso No Fu� Capturada Correctamente', 0 )
     else
     begin
          try
             with dmDBConfig do
             begin
                  SetClasifiKiosco;
                  SetCompanyPassword( CM_PASSWRD.Text );
                  GrabaCompany;
             end;
          except
                on Error: Exception do
                begin
                     ZetaDialogo.zExcepcion( Caption, 'Error Al Agregar Empresa', Error, 0 );
                end;
          end;
     end;
end;

procedure TEmpresaEdit.CancelarClick(Sender: TObject);
begin
     inherited;
     case DataSource.State of
          dsEdit, dsInsert:
          begin
               dmDBConfig.CancelaCambiosCompany;
               ConectaClasificaciones;
               GetConfidencialidad;
          end;
     else
         Close;
     end;
end;

procedure TEmpresaEdit.BuscarServidorClick(Sender: TObject);
var
   sAlias, sDatabase: String;
begin
     inherited;
     with Datasource.Dataset do
     begin
          sAlias := FieldByName( 'CM_ALIAS' ).AsString;
          if ( sAlias = VACIO ) then
             sAlias := Format( 'Tress %s ODBC', [ CM_CODIGO.TEXT ] );
          sDatabase := FieldByName( 'CM_DATOS' ).AsString;
          if FSQLDBLookup.LookupSQLDatabases( sAlias, CM_USRNAME.Text, CM_PASSWRD.Text, sDatabase ) then
          begin
               dmDBConfig.SetEditMode;
               FieldByName( 'CM_DATOS' ).AsString := sDatabase;
               FieldByName( 'CM_ALIAS' ).AsString := sAlias;
          end;
     end;
end;

procedure TEmpresaEdit.CM_DIGITOKeyPress(Sender: TObject; var Key: Char);
begin
     inherited;
     if ZetaCommonTools.EsMinuscula( Key ) then     // Solo acepta may�sculas
        Key := ZetaCommonTools.aMayuscula( Key );
     if ( Key >= #32 ) and
        ( ( not ( ZetaCommonTools.EsDigito( Key ) ) ) and ( not ( ZetaCommonTools.EsMayuscula( Key ) ) ) ) then
        Key := #0;       // Si no es caracter de control y no es D�gito o Letra May�scula se ignora
end;

procedure TEmpresaEdit.SetControlesDigito;
var
   l3Datos, lPresup : Boolean;
begin
     with dmDBConfig do
     begin
          l3Datos := GetTipoCompanyName( tqCompany.FieldByName( 'CM_CONTROL' ).AsString ) = tc3Datos;
          lPresup := GetTipoCompanyName( tqCompany.FieldByName( 'CM_CONTROL' ).AsString ) = tcPresupuesto;
     end;


     lblAcumula.Enabled := l3Datos or not lPresup;
     lblSumar.Enabled := l3Datos or not lPresup;
     lblDigitoGafetes.Enabled := l3Datos;
     gbConfidencialidad.Enabled := l3Datos or not lPresup;
     lblReportesKiosco.Enabled := l3Datos;

     CM_ACUMULA.Enabled := l3Datos or not lPresup;
     CM_EMPATE.Enabled := l3Datos or not lPresup;
     CM_USACAFE.Enabled := l3Datos or not lPresup;
     CM_USACASE.Enabled := l3Datos or not lPresup;
     CM_DIGITO.Enabled := l3Datos;
     rbConfidenNinguna.Enabled := l3Datos or not lPresup;
     rbConfidenAlgunas.Enabled := l3Datos or not lPresup;
     listaConfidencialidad.Enabled := l3Datos or not lPresup;
     btSeleccionarConfiden.Enabled := l3Datos or not lPresup;

     CM_KCLASIFI.Enabled := l3Datos;

     if ( lPresup ) then
     begin
          //Valores default de Prespuestos
          CM_ACUMULA.ItemIndex := CM_ACUMULA.Items.Count - 1;
          //CM_NIVEL0.ItemIndex := CM_NIVEL0.Items.Count - 1;
          CM_KCLASIFI.ItemIndex := 0;
          CM_USACAFE.Checked := False;
          CM_USACASE.Checked := False;
          CM_EMPATE.Text := '0';
          CM_DIGITO.Text := VACIO;
     end;
end;

procedure TEmpresaEdit.ActualizaComboClasifi(eClasifi: eClasifiReporte);
begin
     {Para la base de datos el valor cuando es sin restricci�n es negativo, pero para Tress es el MaxInt}
     if ( Ord( eClasifi ) = K_SIN_RESTRICCION ) then
        eClasifi:= eClasifiReporte( crSinRestriccion );
     CM_KCLASIFI.ItemIndex := dmReportes.GetPosClasifiEmp( CM_KCLASIFI.Items, eClasifi);
end;

procedure TEmpresaEdit.CM_KCLASIFIChange(Sender: TObject);
begin
     with DataSource.DataSet do
     begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;
     end;
end;

procedure TEmpresaEdit.ConectaClasificaciones;
begin
     if not dmReportes.GetListaClasifiEmpresa( CM_KCLASIFI.Items ) then
        ActualizaComboClasifi( eClasifiReporte( K_SIN_RESTRICCION ) )
     else
         ActualizaComboClasifi( eClasifiReporte( Datasource.Dataset.FieldByName( 'CM_KCLASIFI' ).AsInteger ) );
end;

procedure TEmpresaEdit.SetListConfidencialidad( sValores : string );
var
   Lista : TStringList;
   i, j : integer;
begin
    Lista := TStringList.Create;

    sValores := Trim( sValores );
    if StrLleno( sValores ) then
        Lista.CommaText  := sValores;

    listaConfidencialidad.Items.Clear;

    rbConfidenNinguna.OnClick := nil;
    rbConfidenAlgunas.OnClick := nil;
    rbConfidenNinguna.Checked := False;
    rbConfidenAlgunas.Checked := False;

    for i:= 0 to lValoresConfidencialidad.Count - 1 do
    begin
        if ( Lista.Count > 0 ) then
        begin
                j := Lista.IndexOf( Trim( lValoresConfidencialidad.Names[i] )  );

                if ( j >= 0 ) then
                   listaConfidencialidad.Items.Add( lValoresConfidencialidad[i] )

                //CM_NIVEL0.Checked[i] :=  ( j >= 0 );
        end
    end;

    rbConfidenNinguna.Checked :=   listaConfidencialidad.Items.Count = 0;
    rbConfidenAlgunas.Checked := not rbConfidenNinguna.Checked;

    if ( gbConfidencialidad.Enabled ) then
    begin
        listaConfidencialidad.Enabled := rbConfidenAlgunas.Checked;
        btSeleccionarConfiden.Enabled := rbConfidenAlgunas.Checked;
    end;

    rbConfidenNinguna.OnClick := rbConfidenNingunaClick;
    rbConfidenAlgunas.OnClick := rbConfidenAlgunasClick;

    FreeAndNil( Lista );

end;

procedure TEmpresaEdit.GetConfidencialidad;
begin
   if (  DataSource.DataSet  <> nil ) then
   with DataSource.DataSet do
   begin
        SetListConfidencialidad(  FieldByName('CM_NIVEL0').AsString );
   end;
end;

procedure TEmpresaEdit.btSeleccionarConfidenClick(Sender: TObject);
var
   selConfi : TSeleccionarConfidencialidad;
   sCM_NIVEL0Nuevo : string;
begin
  inherited;

  selConfi := TSeleccionarConfidencialidad.Create( Self );
  sCM_NIVEL0Nuevo := selConfi.GetConfidencialidad( DataSource.DataSet.FieldByName('CM_NIVEL0').AsString, lValoresConfidencialidad );

  if (  sCM_NIVEL0Nuevo <>  DataSource.DataSet.FieldByName('CM_NIVEL0').AsString ) then
  begin
     with DataSource.DataSet do
     begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

           FieldByName('CM_NIVEL0').AsString  := sCM_NIVEL0Nuevo;
           GetConfidencialidad;
     end;
  end;

  FreeAndNil ( selConfi ) ;

end;

procedure TEmpresaEdit.rbConfidenNingunaClick(Sender: TObject);
begin
  inherited;
   with DataSource.DataSet do
   begin

        if not( State in [dsEdit,dsInsert] ) then
             Edit;


        FieldByName('CM_NIVEL0').AsString  :=  VACIO;
   end;
   GetConfidencialidad;
end;

procedure TEmpresaEdit.rbConfidenAlgunasClick(Sender: TObject);
begin
  inherited;
     if ( rbConfidenAlgunas.Checked ) then
     begin
        btSeleccionarConfidenClick( Sender );
        GetConfidencialidad;
     end;
end;

end.
