unit FSentinelLicenciaWrite;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Checklst, Mask,
     ZetaNumero,
     ZetaDBTextBox, ComCtrls, ZetaWinAPITools, ZetaEdit;

type
  TSentinelLicenciaWrite = class(TForm)
    Salir: TBitBtn;
    panelBotones: TPanel;
    Label2: TLabel;
    ArchivoLicencia: TZetaEdit;
    btnBuscaArchivo: TSpeedButton;
    btnEscribirArchivoLicencia: TBitBtn;
    OpenDialog: TOpenDialog;
    ReadOnlyLBL: TLabel;
    procedure FormShow(Sender: TObject);
    procedure btnEscribirArchivoLicenciaClick(Sender: TObject);
    procedure btnBuscaArchivoClick(Sender: TObject);
    procedure ArchivoLicenciaChange(Sender: TObject);
  private
    procedure CargaDatosSentinel;
    {$ifdef MSSQL}
    procedure CargaDatosSentinelVirtual;
    {$endif}
  public

  end;

var
  SentinelLicenciaWrite: TSentinelLicenciaWrite;

implementation

uses FAutoClasses,
     FHelpContext,
{$ifdef MSSQL}
     FSentinelRegistry,
     ZetaClientTools,
     ZetaCommonTools,
{$endif}
     ZetaCommonClasses,
     ZetaDialogo;

{$R *.DFM}

procedure TSentinelLicenciaWrite.FormShow(Sender: TObject);
begin
     CargaDatosSentinel;
     //HelpContext := H00014_Actualizar_sentinel;
end;

procedure TSentinelLicenciaWrite.btnEscribirArchivoLicenciaClick(Sender: TObject);
var
   oCursor: TCursor;
   FRegistry: TSentinelRegistryServer;
begin
     // Si existe el archivo especificado.
     if FileExists( Self.ArchivoLicencia.Text )then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourGlass;

          try
             FRegistry := TSentinelRegistryServer.Create( True );
             try
                try
                   FRegistry.ArchivoLicencia := Self.ArchivoLicencia.Text;
                   Self.btnEscribirArchivoLicencia.Enabled := False;
                   ZetaDialogo.ZInformation( '� Exito !', 'Los datos del Archivo Licencia fueron escritos', 0 );
                   CargaDatosSentinelVirtual;
                except
                      on Error: Exception do
                      begin
                           ZetaDialogo.ZExcepcion( '� Error !', 'Error al escribir datos del Archivo Licencia', Error, 0 );
                      end;
                end;
             finally
                    FreeAndNil( FRegistry );
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end
     else
         ZetaDialogo.ZError( '� Error !','El archivo especificado no existe', 0 );
end;

procedure TSentinelLicenciaWrite.btnBuscaArchivoClick(Sender: TObject);
begin
{$ifdef MSSQL}
     Self.ArchivoLicencia.Text := ZetaClientTools.AbreDialogo( OpenDialog, Self.ArchivoLicencia.Text, 'Lic' );
{$endif}
end;

procedure TSentinelLicenciaWrite.ArchivoLicenciaChange(Sender: TObject);
begin
{$ifdef MSSQL}
     Self.btnEscribirArchivoLicencia.Enabled := StrLleno( Self.ArchivoLicencia.Text );
{$endif}
end;

{$ifdef MSSQL}
procedure TSentinelLicenciaWrite.CargaDatosSentinelVirtual;
var
   FRegistry: TSentinelRegistryServer;
   lCanWrite: Boolean;
begin
     // Inicializa valores.
     lCanWrite := false;

     FRegistry := TSentinelRegistryServer.Create( True );
     try
        if FSentinelRegistry.CheckComputerName then
        begin
             lCanWrite := FRegistry.CanWrite;
        end
        else
        begin
             with Self.ReadOnlyLBL do
             begin
                  Caption := 'Servidor No Autorizado';
                  Visible := True;
             end;
        end;


        Self.ReadOnlyLBL.Visible := not lCanWrite;

        //lblNombreServer.Caption := ZetaWinAPITools.GetComputerName;
        Self.ArchivoLicencia.Text := FRegistry.ArchivoLicencia;
        Self.btnEscribirArchivoLicencia.Enabled := False;

     finally
            FreeAndNil( FRegistry );
     end;
end;
{$endif}


procedure TSentinelLicenciaWrite.CargaDatosSentinel;
begin
{$ifdef MSSQL}
   CargaDatosSentinelVirtual;
{$endif}
end;

end.
