inherited DBChecker: TDBChecker
  Left = 309
  Top = 212
  Width = 507
  Height = 289
  BorderIcons = [biSystemMenu, biMaximize]
  BorderStyle = bsSizeable
  Caption = 'Probar Acceso A Bases De Datos'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 219
    Width = 499
    inherited OK: TBitBtn
      Left = 340
      Top = 5
      Height = 26
      Hint = 'Empezar La Prueba'
      Caption = '&Probar'
      ModalResult = 0
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 420
      Top = 5
      Height = 26
      Hint = 'Abandonar Esta Pantalla'
      Caption = '&Salir'
      Kind = bkClose
    end
    object ckDigito: TCheckBox
      Left = 17
      Top = 9
      Width = 159
      Height = 17
      Caption = 'Actualizar &D�gito de Empresa'
      TabOrder = 2
    end
  end
  object ResultadoGB: TGroupBox
    Left = 0
    Top = 0
    Width = 499
    Height = 219
    Align = alClient
    Caption = ' Resultados '
    TabOrder = 1
    object Resultados: TMemo
      Left = 2
      Top = 15
      Width = 495
      Height = 202
      Align = alClient
      BorderStyle = bsNone
      Color = clBtnFace
      ReadOnly = True
      ScrollBars = ssBoth
      TabOrder = 0
    end
  end
  object HdbcComparte: THdbc
    ConnectionPooling = cpDefault
    Version = '5.06'
    BeforeConnect = HdbcBeforeConnect
    Left = 33
    Top = 94
  end
  object HdbcEmpresa: THdbc
    ConnectionPooling = cpDefault
    Version = '5.06'
    BeforeConnect = HdbcBeforeConnect
    Left = 95
    Top = 94
  end
  object oeCompany: TOEQuery
    hDbc = HdbcComparte
    hStmt.Target.Target = (
      'OE5.06'
      ''
      ''
      ''
      '')
    Params = <>
    Left = 64
    Top = 94
  end
  object oeVersion: TOEQuery
    hDbc = HdbcComparte
    hStmt.CursorType = 0
    hStmt.Target.Target = (
      'OE5.06'
      ''
      ''
      ''
      '')
    UniDirectional = True
    Params = <>
    Left = 32
    Top = 126
  end
  object oeDigito: TOEQuery
    hDbc = HdbcEmpresa
    hStmt.CursorType = 0
    hStmt.Target.Target = (
      'OE5.06'
      ''
      ''
      ''
      '')
    UniDirectional = True
    Params = <>
    Left = 96
    Top = 126
  end
  object oeUpdDigito: TOEQuery
    hDbc = HdbcComparte
    hStmt.CursorType = 0
    hStmt.Target.Target = (
      'OE5.06'
      ''
      ''
      ''
      '')
    SQL.Strings = (
      'EXEC sp_fkeys @pktable_name = '#39'TAHORRO'#39)
    UniDirectional = True
    Params = <>
    Left = 64
    Top = 126
  end
  object oeSysAdmin: TOEQuery
    hDbc = HdbcEmpresa
    hStmt.CursorType = 0
    hStmt.Target.Target = (
      'OE5.06'
      ''
      ''
      ''
      '')
    UniDirectional = True
    Params = <>
    Left = 64
    Top = 158
  end
end
