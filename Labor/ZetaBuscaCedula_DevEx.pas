unit ZetaBuscaCedula_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, Grids, DBGrids,
     ExtCtrls, Db, Mask,
     ZetaMessages,
     ZetaDBGrid,
     ZetaCommonClasses,
     ZetaClientDataset,
     ZetaEdit,
     ZetaKeyLookup,
     ZetaFecha, ZetaKeyLookup_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, cxButtons, cxControls,
  cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  ZetaCXGrid;

type
  TBuscaCedulas_DevEx = class(TForm)
    PanelControles: TPanel;
    BuscarBtn: TcxButton;
    AceptarBtn: TcxButton;
    CancelarBtn: TcxButton;
    DataSource: TDataSource;
    Status: TRadioGroup;
    Tipo: TRadioGroup;
    Filtros: TGroupBox;
    lbInicio: TLabel;
    lbFin: TLabel;
    FechaFinal: TZetaFecha;
    FechaInicial: TZetaFecha;
    CBFecha: TCheckBox;
    WO_NUMBER: TZetaKeyLookup_DevEx;
    AR_CODIGO: TZetaKeyLookup_DevEx;
    OP_NUMBER: TZetaKeyLookup_DevEx;
    CE_AREA: TZetaKeyLookup_DevEx;
    US_CODIGO: TZetaKeyLookup_DevEx;
    lbUsuario: TLabel;
    lbArea: TLabel;
    lbOpera: TLabel;
    lbParte: TLabel;
    lbWorder: TLabel;
    GridDBTableView1: TcxGridDBTableView;
    GridLevel1: TcxGridLevel;
    Grid: TZetaCXGrid;
    CE_FOLIO: TcxGridDBColumn;
    CE_FECHA: TcxGridDBColumn;
    CE_HORA: TcxGridDBColumn;
    CE_TIEMPO: TcxGridDBColumn;
    CE_TIPO: TcxGridDBColumn;
    WO_NUMBERT: TcxGridDBColumn;
    AR_CODIGOT: TcxGridDBColumn;
    AR_NOMBRE: TcxGridDBColumn;
    CE_PIEZAS: TcxGridDBColumn;
    OP_NUMBERT: TcxGridDBColumn;
    CE_AREAT: TcxGridDBColumn;
    US_CODIGOT: TcxGridDBColumn;
    CE_TMUERTO: TcxGridDBColumn;
    CE_STATUS: TcxGridDBColumn;
    CE_MOD1: TcxGridDBColumn;
    CE_MOD2: TcxGridDBColumn;
    CE_MOD3: TcxGridDBColumn;
    CE_COMENTA: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BuscarBtnClick(Sender: TObject);
    procedure AceptarBtnClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure CBFechaClick(Sender: TObject);
    procedure GridDBTableView1DataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure GridDBTableView1DblClick(Sender: TObject);
  private
    { Private declarations }
    FFiltro: String;
    FCodigoCedulas: String;
    FDescripcion: String;
    FCapturando: Boolean;
    FParametros : TZetaParams;
    function LookupDataset: TZetaClientDataset;
    procedure Buscar;
    procedure SetNumero;
    procedure SetBotones( const Vacio : Boolean );
    procedure ShowGrid( const lVisible: Boolean );
    procedure Connect;
    procedure Disconnect;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    procedure AsignaColumnas;
     procedure ApplyMinWidth;
  protected
    { Protected declarations }
  public
    { Public declarations }
    property CodigoCedulas: String read FCodigoCedulas;
    property Descripcion: String read FDescripcion;
    property Filtro: String read FFiltro write FFiltro;
  end;

procedure SetOnlyActivos( const lActivos: Boolean );

var
  BuscaCedulas_DevEx: TBuscaCedulas_DevEx;

function BuscaCedulasDialogo( const sFilter: String; var sKey, sDescription: String ): Boolean;

implementation

uses ZetaDialogo,
     ZetaCommonTools,
     DGlobal,
     ZGlobalTress,
     ZetaLaborTools,
     ZetaClientTools,
     DCliente,
     DLabor, DSistema,
     ZGridModeTools;

var
   FShowOnlyActivos: Boolean;

{$R *.DFM}

procedure SetOnlyActivos( const lActivos: Boolean );
begin
     FShowOnlyActivos := lActivos;
end;

function BuscaCedulasDialogo( const sFilter: String; var sKey, sDescription: String ): Boolean;
begin
     Result := False;
     if ( BuscaCedulas_DevEx = nil ) then
        BuscaCedulas_DevEx := TBuscaCedulas_DevEx.Create( Application.MainForm );
     if ( BuscaCedulas_DevEx <> nil ) then
     begin
          with BuscaCedulas_DevEx do
          begin
               ShowModal;
               if ( ModalResult = mrOk ) and ( CodigoCedulas <> '' ) then
               begin
                    sKey := CodigoCedulas;
                    sDescription := Descripcion;
                    Result := True;
               end;
          end;
     end;
end;

{************** TBuscaEmpleado ************** }

procedure TBuscaCedulas_DevEx.FormCreate(Sender: TObject);
begin
     //HelpContext := H00012_Busqueda_empleados;
     FParametros := TZetaParams.Create;
     FechaInicial.Valor := Date;
     FechaFinal.Valor := Date;
end;

procedure TBuscaCedulas_DevEx.FormShow(Sender: TObject);
begin
     WindowState := wsNormal;
     ActiveControl := Status;
     Connect;
     AsignaColumnas;

      //Desactiva la posibilidad de agrupar
      GridDBTableView1.OptionsCustomize.ColumnGrouping := False;
      //ZetaDBGridDBTableView.OptionsCustomize.ColumnFiltering := False;
      //Esconde la caja de agrupamiento
      GridDBTableView1.OptionsView.GroupByBox := False;
      //Para que nunca muestre el filterbox inferior
      GridDBTableView1.FilterBox.Visible := fvNever;
      //Aplica el minwidth a las columnas despues de asignar todas las columnas y captions de las mismas
      ApplyMinWidth;
      //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
      GridDBTableView1.ApplyBestFit();
end;

procedure TBuscaCedulas_DevEx.GridDBTableView1DataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
begin
 inherited;
     ZGridModeTools.BorrarItemGenericoAll( AValueList );
     if GridDBTableView1.DataController.IsGridMode then
        ZGridModeTools.FiltroSetValueLista( GridDBTableView1, AItemIndex, AValueList );
end;

procedure TBuscaCedulas_DevEx.GridDBTableView1DblClick(Sender: TObject);
begin

     with AceptarBtn do
     begin
          if enabled then
             AceptarBtn.Click
          else
              ModalResult := mrCancel;
     end;
end;

procedure TBuscaCedulas_DevEx.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions =40;
begin
     with  GridDBTableView1 do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;

end;

procedure TBuscaCedulas_DevEx.AsignaColumnas;
begin
     with Grid do
     begin
          AsignaGlobal( K_GLOBAL_LABOR_ORDEN, lbWorder, WO_NUMBER );
          AsignaGlobal( K_GLOBAL_LABOR_PARTE, lbparte, AR_CODIGO );
          AsignaGlobal( K_GLOBAL_LABOR_AREA, lbArea, CE_AREA );
          AsignaGlobal( K_GLOBAL_LABOR_OPERACION, lbOpera, OP_NUMBER );

          AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_ORDEN, GridDBTableView1.GetColumnByFieldName( 'WO_NUMBER' )) ;// Columns[ GetFieldColumnIndex( 'WO_NUMBER' ) ] );
          AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_PARTE, GridDBTableView1.GetColumnByFieldName( 'AR_CODIGO' )); //Columns[ GetFieldColumnIndex( 'AR_CODIGO' ) ] );
          AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_PIEZAS, GridDBTableView1.GetColumnByFieldName( 'CE_PIEZAS' )) ;//Columns[ GetFieldColumnIndex( 'CE_PIEZAS' ) ] );
          AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_OPERACION, GridDBTableView1.GetColumnByFieldName( 'OP_NUMBER' )) ;//Columns[ GetFieldColumnIndex( 'OP_NUMBER' ) ] );
          AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_AREA, GridDBTableView1.GetColumnByFieldName( 'CE_AREA' ));//Columns[ GetFieldColumnIndex( 'CE_AREA' ) ] );
          AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_TMUERTO, GridDBTableView1.GetColumnByFieldName( 'CE_TMUERTO' )) ;//Columns[ GetFieldColumnIndex( 'CE_TMUERTO' ) ] );
          AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_MODULA1, GridDBTableView1.GetColumnByFieldName( 'CE_MOD1' )) ;//Columns[ GetFieldColumnIndex( 'CE_MOD1' ) ] );
          AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_MODULA2, GridDBTableView1.GetColumnByFieldName( 'CE_MOD2' )) ;//Columns[ GetFieldColumnIndex( 'CE_MOD2' ) ] );
          AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_MODULA3, GridDBTableView1.GetColumnByFieldName( 'CE_MOD3')) ;//Columns[ GetFieldColumnIndex( 'CE_MOD3' ) ] );
     end;
end;


procedure TBuscaCedulas_DevEx.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Disconnect;
     Action := caHide;
end;

function TBuscaCedulas_DevEx.LookupDataset: TZetaClientDataset;
begin
     Result := dmLabor.cdsCedulaLookup;
end;


procedure TBuscaCedulas_DevEx.Connect;
begin
     ShowGrid( False );
     SetBotones( True );
     ActiveControl := Status;
     Wo_number.Llave := Vacio;
     Ar_codigo.Llave := Vacio;
     Op_number.Llave := Vacio;
     Ce_area.Llave := Vacio;
     Us_codigo.Llave := Vacio;

     Status.ItemIndex := 0;
     Tipo.ItemIndex := 0;

     dmSistema.cdsUsuarios.Conectar;
     with dmLabor do
     begin
          cdsPartes.Conectar;
          cdsOpera.Conectar;
          cdsArea.Conectar;
          cdsTiempoMuerto.Conectar;
          DataSource.DataSet := LookupDataset;
     end;
end;

procedure TBuscaCedulas_DevEx.Disconnect;
begin
     GridDBTableView1.DataController.Filter.Root.Clear;
     Datasource.Dataset := nil;
end;

procedure TBuscaCedulas_DevEx.Buscar;
var
   oCursor: TCursor;
begin
     AceptarBtn.Enabled := False;
     Application.ProcessMessages;
     oCursor := Screen.Cursor;
     try
        Screen.Cursor := crHourglass;
        with LookupDataset do
        begin
             try
                DisableControls;
                Active := False;
                with FParametros do
                begin
                     AddInteger( 'Status', Status.ItemIndex - 1 );
                     AddInteger( 'Tipo', Tipo.ItemIndex - 1 );

                     AddString( 'Parte', AR_CODIGO.Llave );
                     AddString( 'Operacion', OP_NUMBER.Llave );
                     AddString( 'Orden', WO_NUMBER.Llave );
                     AddString( 'Area', CE_AREA.Llave );
                     AddString( 'Cedula', Vacio );
                     AddInteger( 'Usuario', US_CODIGO.Valor );


                     if CBFecha.Checked then
                     begin
                          AddDate( 'FechaInicial', FechaInicial.Valor );
                          AddDate( 'FechaFinal', FechaFinal.Valor );
                     end
                     else
                     begin
                          AddDate( 'FechaInicial', NullDateTime );
                          AddDate( 'FechaFinal', NullDateTime );
                     end;
                end;
                dmLabor.RefrescarLookupCedula( FParametros );
             finally
                    EnableControls;
             end;
             if IsEmpty then
             begin
                  zWarning( 'B�squeda de C�dulas', '� No Hay C�dulas Con Estos Datos !', 0, mbOK );
                  SetBotones( True );
                  ActiveControl := Status;
             end
             else
             begin
                  ShowGrid( True );
                  SetBotones( False );
                  ActiveControl := Grid;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TBuscaCedulas_DevEx.SetNumero;
begin
     with LookupDataset do
     begin
          FCodigoCedulas := FieldByName( 'CE_FOLIO' ).AsString;
          FDescripcion := FieldByName( 'CE_COMENTA' ).AsString;
     end;
     ModalResult := mrOK;
end;

procedure TBuscaCedulas_DevEx.SetBotones( const Vacio: Boolean );
begin
     FCapturando := Vacio;
     BuscarBtn.Default := Vacio;
     with AceptarBtn do
     begin
          Default := not Vacio;
          Enabled := not Vacio;
     end;
end;

procedure TBuscaCedulas_DevEx.ShowGrid( const lVisible: Boolean );
const
     GRID_HEIGHT = 150;
     FORM_HEIGHT = 228;
     //HEIGHT_OFFSET = 25;
begin
     with Grid do
     begin
          if lVisible then
          begin
               Visible:= True;
               Height := ZetaClientTools.GetScaledHeight( GRID_HEIGHT );
               Self.Height := ZetaClientTools.GetScaledHeight( FORM_HEIGHT + GRID_HEIGHT );
          end
          else
          begin
               Visible:= False;
               Height := 0;
               Self.Height := ZetaClientTools.GetScaledHeight( FORM_HEIGHT );
          end;
     end;
     Repaint;
end;

procedure TBuscaCedulas_DevEx.WMExaminar(var Message: TMessage);
begin
     AceptarBtn.Click;
end;

procedure TBuscaCedulas_DevEx.BuscarBtnClick(Sender: TObject);
begin
     Buscar;
     GridDBTableView1.ApplyBestFit();
end;

procedure TBuscaCedulas_DevEx.AceptarBtnClick(Sender: TObject);
begin
     if not LookupDataset.IsEmpty then
        SetNumero;
end;

procedure TBuscaCedulas_DevEx.FormDestroy(Sender: TObject);
begin
     FParametros.Free;
end;

procedure TBuscaCedulas_DevEx.CBFechaClick(Sender: TObject);
 var lEnabled : Boolean;
begin
     lEnabled := CbFecha.Checked;
     lbInicio.Enabled := lEnabled;
     lbFin.Enabled := lEnabled;
     FechaInicial.Enabled := lEnabled;
     FechaFinal.Enabled := lEnabled;
end;

end.
