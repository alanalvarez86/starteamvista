program TressLabor;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}


uses
  Forms,
  ZetaClientTools,
  MidasLib,
  ZBaseShell in '..\Tools\ZBaseShell.pas' {BaseShell},
  DBaseCliente in '..\DataModules\DBaseCliente.pas' {BaseCliente: TDataModule},
  ZBaseConsulta in '..\Tools\ZBaseConsulta.pas' {BaseConsulta},
  ZBaseDlgModal in '..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  ZReportTools in '..\Reportes\ZReportTools.pas',
  DGlobal in '..\DataModules\DGlobal.pas',
  DBaseDiccionario in '..\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  ZetaSQL in '..\Servidor\ZetaSQL.pas',
  DDiccionario in 'DDiccionario.pas' {dmDiccionario: TDataModule},
  DReportes in '..\DataModules\DReportes.pas' {dmReportes: TDataModule},
  DLabor in 'DLabor.pas' {dmLabor: TDataModule},
  DTablas in 'DTablas.pas' {dmTablas: TDataModule},
  ZetaDespConsulta in 'ZetaDespConsulta.pas',
  ZArbolTress in 'ZArbolTress.pas',
  ZetaBuscaWorder in 'ZetaBuscaWorder.pas' {BuscaWorder},
  ZBaseThreads in '..\Tools\ZBaseThreads.pas',
  DConsultas in '..\DataModules\DConsultas.pas' {dmConsultas: TDataModule},
  ZetaLaborTools in 'ZetaLaborTools.pas',
  DCatalogos in 'DCatalogos.pas' {dmCatalogos: TDataModule},
  DCliente in 'DCliente.pas' {dmCliente: TDataModule},
  DSistema in 'DSistema.pas' {dmSistema: TDataModule},
  DBaseReportes in '..\DataModules\DBaseReportes.pas' {dmBaseReportes: TDataModule},
  DBasicoCliente in '..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  dBaseGlobal in '..\DataModules\dBaseGlobal.pas',
  dmailmerge in '..\DataModules\dmailmerge.pas' {dmMailMerge: TDataModule},
  DConfigPoliza in '..\DataModules\DConfigPoliza.pas' {dmConfigPoliza: TDataModule},
  DExportEngine in '..\DataModules\DExportEngine.pas' {dmExportEngine: TDataModule},
  dBaseTressDiccionario in '..\DataModules\dBaseTressDiccionario.pas' {dmBaseTressDiccionario: TDataModule},
  DBaseSistema in '..\DataModules\DBaseSistema.pas' {dmBaseSistema: TDataModule},
  FTressShell in 'FTressShell.pas';

{$R *.RES}
//{$R WindowsXP.res}

{$R ..\..\Traducciones\Spanish.RES}

procedure CierraSplash;
begin

end;

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;

  Application.Title := 'Tress Labor';
  Application.HelpFile := 'LABOR.chm';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
  begin
       if Login( False ) then
       begin
            Show;
            Update;
            BeforeRun;
            Application.Run;
       end
       else
       begin
            Free;
       end;
  end;

end.
