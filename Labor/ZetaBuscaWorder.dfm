object BuscaWorder: TBuscaWorder
  Left = 248
  Top = 161
  VertScrollBar.Visible = False
  AutoScroll = False
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'B�squeda de Empleados'
  ClientHeight = 309
  ClientWidth = 516
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanelControles: TPanel
    Left = 0
    Top = 0
    Width = 516
    Height = 153
    Align = alTop
    TabOrder = 0
    object lbInicio: TLabel
      Left = 76
      Top = 70
      Width = 45
      Height = 13
      Alignment = taRightJustify
      Caption = '&Inicio del:'
      Enabled = False
      FocusControl = FechaInicial
    end
    object Label1: TLabel
      Left = 64
      Top = 94
      Width = 57
      Height = 13
      Caption = 'N�mero del:'
    end
    object Label5: TLabel
      Left = 248
      Top = 94
      Width = 12
      Height = 13
      Caption = 'Al:'
    end
    object lbFin: TLabel
      Left = 248
      Top = 70
      Width = 12
      Height = 13
      Alignment = taRightJustify
      Caption = 'Al:'
      Enabled = False
      FocusControl = FechaInicial
    end
    object lbParte: TLabel
      Left = 93
      Top = 118
      Width = 28
      Height = 13
      Alignment = taRightJustify
      Caption = 'Parte:'
      FocusControl = Parte
    end
    object BuscarBtn: TBitBtn
      Left = 426
      Top = 5
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Buscar'
      Default = True
      TabOrder = 7
      OnClick = BuscarBtnClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33033333333333333F7F3333333333333000333333333333F777333333333333
        000333333333333F777333333333333000333333333333F77733333333333300
        033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
        33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
        3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
        33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
        333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
        333333773FF77333333333370007333333333333777333333333}
      NumGlyphs = 2
    end
    object AceptarBtn: TBitBtn
      Left = 426
      Top = 35
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Aceptar'
      Enabled = False
      ModalResult = 1
      TabOrder = 8
      OnClick = AceptarBtnClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object CancelarBtn: TBitBtn
      Left = 425
      Top = 65
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = '&Salir'
      Default = True
      ModalResult = 2
      TabOrder = 9
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333333333000033338833333333333333333F333333333333
        0000333911833333983333333388F333333F3333000033391118333911833333
        38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
        911118111118333338F3338F833338F3000033333911111111833333338F3338
        3333F8330000333333911111183333333338F333333F83330000333333311111
        8333333333338F3333383333000033333339111183333333333338F333833333
        00003333339111118333333333333833338F3333000033333911181118333333
        33338333338F333300003333911183911183333333383338F338F33300003333
        9118333911183333338F33838F338F33000033333913333391113333338FF833
        38F338F300003333333333333919333333388333338FFF830000333333333333
        3333333333333333333888330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object Status: TRadioGroup
      Left = 124
      Top = 0
      Width = 258
      Height = 41
      Caption = 'Status'
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'Todos'
        'Completo'
        'Incompleto')
      TabOrder = 0
    end
    object FechaInicial: TZetaFecha
      Left = 124
      Top = 65
      Width = 115
      Height = 22
      Cursor = crArrow
      Enabled = False
      TabOrder = 2
      Text = '02/Jan/98'
      Valor = 35797
    end
    object FechaFinal: TZetaFecha
      Left = 266
      Top = 65
      Width = 115
      Height = 22
      Cursor = crArrow
      Enabled = False
      TabOrder = 3
      Text = '02/Jan/98'
      Valor = 35797
    end
    object WO_NUMBER_INI: TZetaEdit
      Left = 124
      Top = 90
      Width = 115
      Height = 21
      TabOrder = 4
    end
    object WO_NUMBER_FIN: TZetaEdit
      Left = 266
      Top = 90
      Width = 115
      Height = 21
      TabOrder = 5
    end
    object Parte: TZetaKeyLookup
      Left = 124
      Top = 114
      Width = 378
      Height = 21
      LookupDataset = dmLabor.cdsPartes
      TabOrder = 6
      TabStop = True
      WidthLlave = 100
    end
    object CBFecha: TCheckBox
      Left = 124
      Top = 44
      Width = 108
      Height = 17
      Caption = 'Filtrar por Fechas'
      TabOrder = 1
      OnClick = CBFechaClick
    end
  end
  object Grid: TZetaDBGrid
    Left = 0
    Top = 153
    Width = 516
    Height = 156
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'WO_NUMBER'
        Title.Caption = 'N�mero'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WO_DESCRIP'
        Title.Caption = 'Descripci�n'
        Width = 156
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WO_FEC_INI'
        Title.Caption = 'Inicio'
        Width = 83
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WO_STATUS'
        Title.Caption = 'Status'
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AR_NOMBRE'
        Title.Caption = 'Parte'
        Width = 75
        Visible = True
      end>
  end
  object DataSource: TDataSource
    Left = 149
    Top = 143
  end
end
