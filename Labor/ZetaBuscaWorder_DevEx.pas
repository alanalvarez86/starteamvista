unit ZetaBuscaWorder_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, Grids, DBGrids,
     ExtCtrls, Db, Mask,
     ZetaMessages,
     ZetaDBGrid,
     ZetaCommonClasses,
     ZetaClientDataset,
     ZetaEdit,
     ZetaKeyLookup,
     ZetaFecha, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore, TressMorado2013, cxButtons,
  ZetaKeyLookup_DevEx, cxControls, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ZetaCXGrid;

type
  TBuscaWorder_DevEx = class(TForm)
    PanelControles: TPanel;
    DataSource: TDataSource;
    Status: TRadioGroup;
    GroupBox1: TGroupBox;
    CBFecha: TCheckBox;
    lbInicio: TLabel;
    FechaInicial: TZetaFecha;
    lbFin: TLabel;
    FechaFinal: TZetaFecha;
    Label1: TLabel;
    WO_NUMBER_INI: TZetaEdit;
    Label5: TLabel;
    WO_NUMBER_FIN: TZetaEdit;
    lbParte: TLabel;
    Parte: TZetaKeyLookup_DevEx;
    BuscarBtn_DevEx: TcxButton;
    AceptarBtn_DevEx: TcxButton;
    ZetaDBGrid: TZetaCXGrid;
    ZetaDBGridDBTableView: TcxGridDBTableView;
    ZetaDBGridLevel: TcxGridLevel;
    WO_NUMBER: TcxGridDBColumn;
    WO_DESCRIP: TcxGridDBColumn;
    WO_FEC_INI: TcxGridDBColumn;
    WO_STATUS: TcxGridDBColumn;
    AR_NOMBRE: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BuscarBtnClick(Sender: TObject);
    procedure AceptarBtnClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure CBFechaClick(Sender: TObject);
    procedure BuscarBtn_DevExClick(Sender: TObject);
    procedure AceptarBtn_DevExClick(Sender: TObject);
    procedure ZetaDBGridDBTableViewDblClick(Sender: TObject);
    procedure ZetaDBGridDBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure ZetaDBGridDBTableViewDataControllerSortingChanged(
      Sender: TObject);
    procedure ZetaDBGridDBTableViewColumnHeaderClick(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
  private
    { Private declarations }
    FFiltro: String;
    FCodigoWorder: String;
    FDescripcion: String;
    FCapturando: Boolean;
    FParametros : TZetaParams;
    function LookupDataset: TZetaClientDataset;
    procedure Buscar;
    procedure SetNumero;
    procedure SetBotones( const Vacio : Boolean );
    procedure ShowGrid( const lVisible: Boolean );
    procedure Connect;
    procedure Disconnect;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    procedure ApplyMinWidth;
  protected
    { Protected declarations }
  protected
    { Protected declarations }
    AColumn: TcxGridDBColumn;
  public
    { Public declarations }
    property CodigoWorder: String read FCodigoWorder;
    property Descripcion: String read FDescripcion;
    property Filtro: String read FFiltro write FFiltro;
  end;

procedure SetOnlyActivos( const lActivos: Boolean );

var
  BuscaWorder_DevEx: TBuscaWorder_DevEx;

function BuscaWorderDialogo( const sFilter: String; var sKey, sDescription: String ): Boolean;

implementation

uses ZetaDialogo,
     ZetaCommonTools,
     DGlobal,
     ZGlobalTress,
     ZetaLaborTools,
     ZetaClientTools,
     DCliente,
     DLabor,
     ZGridModeTools;

var
   FShowOnlyActivos: Boolean;

{$R *.DFM}

procedure SetOnlyActivos( const lActivos: Boolean );
begin
     FShowOnlyActivos := lActivos;
end;

function BuscaWorderDialogo( const sFilter: String; var sKey, sDescription: String ): Boolean;
begin
     Result := False;
     if (  BuscaWorder_DevEx = nil ) then
         BuscaWorder_DevEx := TBuscaWorder_DevEx.Create( Application.MainForm );
     if (  BuscaWorder_DevEx <> nil ) then
     begin
          with  BuscaWorder_DevEx do
          begin
               ShowModal;
               if ( ModalResult = mrOk ) and ( CodigoWorder <> '' ) then
               begin
                    sKey := CodigoWorder;
                    sDescription := Descripcion;
                    Result := True;
               end;
          end;
     end;
end;

{************** TBuscaEmpleado ************** }

procedure TBuscaWorder_DevEx.FormCreate(Sender: TObject);
begin
     //HelpContext := H00012_Busqueda_empleados;
     FParametros := TZetaParams.Create;
     FechaInicial.Valor := Date;
     FechaFinal.Valor := Date;
     PanelControles.Height := 177;
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= True;
     ZetaDBGridDBTableView.DataController.Filter.AutoDataSetFilter := TRUE;

end;

procedure TBuscaWorder_DevEx.FormShow(Sender: TObject);
begin
     WindowState := wsNormal;
     ActiveControl := Status;
     Connect;
     Caption := 'B�squedas de ' + Global.GetGlobalString( K_GLOBAL_LABOR_ORDEN );
     AsignaGlobal( K_GLOBAL_LABOR_PARTE, lbParte, Parte );
     AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_PARTE, ZetaDBGridDBTableView.GetColumnByFieldName('AR_NOMBRE'));
     {$ifdef INTERRUPTORES}
      Parte.WidthLlave := K_WIDTHLLAVE;
     {$endif}
      //Desactiva la posibilidad de agrupar
      ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := False;
      //ZetaDBGridDBTableView.OptionsCustomize.ColumnFiltering := False;
      //Esconde la caja de agrupamiento
      ZetaDBGridDBTableView.OptionsView.GroupByBox := False;
      //Para que nunca muestre el filterbox inferior
      ZetaDBGridDBTableView.FilterBox.Visible := fvNever;
      //Aplica el minwidth a las columnas despues de asignar todas las columnas y captions de las mismas
      ApplyMinWidth;
      //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
      ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TBuscaWorder_DevEx.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Disconnect;
     Action := caHide;
end;

function TBuscaWorder_DevEx.LookupDataset: TZetaClientDataset;
begin
     Result := dmLabor.cdsWOrderLookup;
end;


procedure TBuscaWorder_DevEx.Connect;
begin
     ShowGrid( False );
     SetBotones( True );
     ActiveControl := Status;
     WO_NUMBER_INI.Clear;
     WO_NUMBER_FIN.Clear;
     Parte.Llave :='';
     Status.ItemIndex := 0;

     dmLabor.cdsPartes.Conectar;
     Datasource.Dataset := LookupDataset;
end;

procedure TBuscaWorder_DevEx.Disconnect;
begin
     ZetaDBGridDBTableView.DataController.Filter.Root.Clear;
     Datasource.Dataset := nil;
end;

procedure TBuscaWorder_DevEx.Buscar;
var
   oCursor: TCursor;
begin
     AceptarBtn_DevEx.Enabled := False;
     Application.ProcessMessages;
     oCursor := Screen.Cursor;
     try
        Screen.Cursor := crHourglass;
        with LookupDataset do
        begin
             try
                DisableControls;
                Active := False;
                with FParametros do
                begin
                     AddInteger( 'Tipo', Status.ItemIndex-1 );
                     AddString( 'Parte', Parte.Llave );
                     if CBFecha.Checked then
                     begin
                          AddDate( 'FechaInicial', FechaInicial.Valor );
                          AddDate( 'FechaFinal', FechaFinal.Valor );
                     end
                     else
                     begin
                          AddDate( 'FechaInicial', NullDateTime );
                          AddDate( 'FechaFinal', NullDateTime );
                     end;
                     AddString( 'OrderInicial', WO_NUMBER_INI.Valor );
                     AddString( 'OrderFinal', WO_NUMBER_FIN.Valor );
                end;
                dmLabor.RefrescarLookupWOrder( FParametros );
             finally
                    EnableControls;
             end;
             if IsEmpty then
             begin
                  zWarning( 'B�squeda de Orden de Trabajo', '� No Hay Ordenes Con Estos Datos !', 0, mbOK );
                  SetBotones( True );
                  ActiveControl := Status;
             end
             else
             begin
                  ShowGrid( True );
                  SetBotones( False );
                  ActiveControl := ZetaDBGrid;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TBuscaWorder_DevEx.SetNumero;
begin
     with LookupDataset do
     begin
          FCodigoWorder := FieldByName( 'WO_NUMBER' ).AsString;
          FDescripcion := FieldByName( 'WO_DESCRIP' ).AsString;
     end;
     ModalResult := mrOK;
end;

procedure TBuscaWorder_DevEx.SetBotones( const Vacio: Boolean );
begin
     FCapturando := Vacio;
     BuscarBtn_DevEx.Default := Vacio;
     with AceptarBtn_DevEx do
     begin
          Default := not Vacio;
          Enabled := not Vacio;
     end;
end;

procedure TBuscaWorder_DevEx.ShowGrid( const lVisible: Boolean );
const
     GRID_HEIGHT = 200;
     FORM_HEIGHT = 216;
     //HEIGHT_OFFSET = 25;
begin
     with ZetaDBGrid do
     begin
          if lVisible then
          begin
               Visible:= True;
               Height := ZetaClientTools.GetScaledHeight( GRID_HEIGHT );
               Self.Height := ZetaClientTools.GetScaledHeight( FORM_HEIGHT + GRID_HEIGHT );
          end
          else
          begin
               Visible:= False;
               Height := 0;
               Self.Height := ZetaClientTools.GetScaledHeight( FORM_HEIGHT );
          end;
     end;
     Repaint;
end;

procedure TBuscaWorder_DevEx.WMExaminar(var Message: TMessage);
begin
     //AceptarBtn.Click;
     AceptarBtn_DevEx.Click;
end;

procedure TBuscaWorder_DevEx.BuscarBtnClick(Sender: TObject);
begin
     Buscar;
end;

procedure TBuscaWorder_DevEx.AceptarBtnClick(Sender: TObject);
begin
     if not LookupDataset.IsEmpty then
        SetNumero;
end;

procedure TBuscaWorder_DevEx.FormDestroy(Sender: TObject);
begin
     FParametros.Free;
end;

procedure TBuscaWorder_DevEx.CBFechaClick(Sender: TObject);
 var lEnabled : Boolean;
begin
     lEnabled := CbFecha.Checked;
     lbInicio.Enabled := lEnabled;
     lbFin.Enabled := lEnabled;
     FechaInicial.Enabled := lEnabled;
     FechaFinal.Enabled := lEnabled;
end;

procedure TBuscaWorder_DevEx.BuscarBtn_DevExClick(Sender: TObject);
begin
     Buscar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TBuscaWorder_DevEx.AceptarBtn_DevExClick(Sender: TObject);
begin
     if not LookupDataset.IsEmpty then
        SetNumero
     else
         ModalResult := mrCancel;
end;

procedure TBuscaWorder_DevEx.ZetaDBGridDBTableViewDblClick(
  Sender: TObject);
begin
     with AceptarBtn_DevEx do
     begin
          if enabled then
             AceptarBtn_DevEx.Click
          else
              ModalResult := mrCancel;
     end;
end;

procedure TBuscaWorder_DevEx.ZetaDBGridDBTableViewColumnHeaderClick(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
     self.AColumn := TcxGridDBColumn(AColumn);
end;

procedure TBuscaWorder_DevEx.ZetaDBGridDBTableViewDataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
var
  AIndex: Integer;
begin
  inherited;
  AIndex := AValueList.FindItemByKind(fviCustom);
    if AIndex <> -1 then
      AValueList.Delete(AIndex);

     ZGridModeTools.BorrarItemGenericoAll( AValueList );
     if ZetaDBGridDBTableView.DataController.IsGridMode then
        ZGridModeTools.FiltroSetValueLista( ZetaDBGridDBTableView, AItemIndex, AValueList );
end;

procedure TBuscaWorder_DevEx.ZetaDBGridDBTableViewDataControllerSortingChanged(
  Sender: TObject);
begin
     ZetaDBGrid.OrdenarPor( AColumn , TZetaClientDataset(DataSource.DataSet) );
end;

procedure TBuscaWorder_DevEx.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions =40;
begin
     with  ZetaDBGridDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;

end;

end.
