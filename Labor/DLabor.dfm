object dmLabor: TdmLabor
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 511
  Width = 741
  object cdsModula1: TZetaLookupDataSet
    Tag = 1
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    BeforePost = cdsTablaBeforePost
    AfterPost = AfterPost
    AfterDelete = AfterPost
    OnReconcileError = ReconcileError
    AlAdquirirDatos = CatalogosAlAdquirirDatos
    AlModificar = CatalogosAlModificar
    UsaCache = True
    LookupName = 'Modulador #1'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    Left = 337
    Top = 8
  end
  object cdsModula2: TZetaLookupDataSet
    Tag = 2
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    BeforePost = cdsTablaBeforePost
    AfterPost = AfterPost
    AfterDelete = AfterPost
    OnReconcileError = ReconcileError
    AlAdquirirDatos = CatalogosAlAdquirirDatos
    AlModificar = CatalogosAlModificar
    UsaCache = True
    LookupName = 'Modulador #2'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    Left = 337
    Top = 52
  end
  object cdsModula3: TZetaLookupDataSet
    Tag = 3
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    BeforePost = cdsTablaBeforePost
    AfterPost = AfterPost
    AfterDelete = AfterPost
    OnReconcileError = ReconcileError
    AlAdquirirDatos = CatalogosAlAdquirirDatos
    AlModificar = CatalogosAlModificar
    UsaCache = True
    LookupName = 'Modulador #3'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    Left = 337
    Top = 101
  end
  object cdsTiempoMuerto: TZetaLookupDataSet
    Tag = 4
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    BeforePost = cdsTablaBeforePost
    AfterPost = AfterPost
    AfterDelete = AfterPost
    OnReconcileError = ReconcileError
    AlAdquirirDatos = CatalogosAlAdquirirDatos
    AlModificar = CatalogosAlModificar
    UsaCache = True
    LookupName = 'Tiempo Muerto'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    Left = 337
    Top = 156
  end
  object cdsOpera: TZetaLookupDataSet
    Tag = 8
    Aggregates = <>
    IndexFieldNames = 'OP_NUMBER'
    Params = <>
    BeforePost = cdsTablaBeforePost
    AfterPost = AfterPost
    AfterDelete = AfterPost
    OnReconcileError = ReconcileError
    AlAdquirirDatos = CatalogosAlAdquirirDatos
    AlModificar = CatalogosAlModificar
    LookupName = 'Cat'#225'logo de Operaciones'
    LookupDescriptionField = 'OP_NOMBRE'
    LookupKeyField = 'OP_NUMBER'
    Left = 264
    Top = 156
  end
  object cdsTParte: TZetaLookupDataSet
    Tag = 5
    Aggregates = <>
    IndexFieldNames = 'TT_CODIGO'
    Params = <>
    BeforePost = cdsTablaBeforePost
    AfterPost = AfterPost
    AfterDelete = AfterPost
    OnReconcileError = ReconcileError
    AlAdquirirDatos = CatalogosAlAdquirirDatos
    AlModificar = CatalogosAlModificar
    UsaCache = True
    LookupName = 'Cat'#225'logo de Tipo de Parte'
    LookupDescriptionField = 'TT_DESCRIP'
    LookupKeyField = 'TT_CODIGO'
    Left = 264
    Top = 8
  end
  object cdsPartes: TZetaLookupDataSet
    Tag = 7
    Aggregates = <>
    IndexFieldNames = 'AR_CODIGO'
    Params = <>
    BeforePost = cdsTablaBeforePost
    AfterCancel = cdsPartesAfterCancel
    BeforeDelete = cdsPartesBeforeDelete
    OnNewRecord = cdsPartesNewRecord
    OnReconcileError = ReconcileError
    AlAdquirirDatos = CatalogosAlAdquirirDatos
    AlEnviarDatos = cdsPartesAlEnviarDatos
    AlCrearCampos = cdsPartesAlCrearCampos
    AlModificar = CatalogosAlModificar
    LookupName = 'Cat'#225'logo de  Partes'
    LookupDescriptionField = 'AR_NOMBRE'
    LookupKeyField = 'AR_CODIGO'
    Left = 32
    Top = 156
  end
  object cdsEditParte: TZetaClientDataSet
    Tag = 3
    Aggregates = <>
    IndexFieldNames = 'AR_CODIGO'
    Params = <>
    OnReconcileError = ReconcileError
    Left = 32
    Top = 101
  end
  object cdsDatosEmpleado: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = ReconcileError
    AlAdquirirDatos = cdsDatosEmpleadoAlAdquirirDatos
    AlCrearCampos = cdsDatosEmpleadoAlCrearCampos
    Left = 32
    Top = 8
  end
  object cdsWoFija: TZetaClientDataSet
    Tag = 9
    Aggregates = <>
    IndexFieldNames = 'CB_CODIGO;WF_FEC_INI'
    Params = <>
    AfterPost = AfterPost
    AfterDelete = AfterPost
    OnNewRecord = cdsWoFijaNewRecord
    OnReconcileError = ReconcileError
    AlAdquirirDatos = cdsWoFijaAlAdquirirDatos
    AlCrearCampos = cdsWoFijaAlCrearCampos
    AlModificar = CatalogosAlModificar
    Left = 184
    Top = 8
  end
  object cdsLecturas: TZetaClientDataSet
    Tag = 11
    Aggregates = <>
    IndexFieldNames = 'LX_HORA;LX_WORDER;LX_OPERA'
    Params = <>
    AfterPost = AfterPost
    AfterDelete = AfterPost
    OnNewRecord = cdsLecturasNewRecord
    OnReconcileError = ReconcileError
    AlAdquirirDatos = cdsLecturasAlAdquirirDatos
    AlCrearCampos = cdsLecturasAlCrearCampos
    AlModificar = CatalogosAlModificar
    Left = 114
    Top = 8
  end
  object cdsMultiLote: TZetaClientDataSet
    Tag = 10
    Aggregates = <>
    IndexFieldNames = 'WO_NUMBER'
    Params = <>
    AfterOpen = cdsMultiLoteAfterOpen
    Left = 184
    Top = 52
    object cdsMultiLoteWO_NUMBER: TStringField
      FieldName = 'WO_NUMBER'
    end
    object cdsMultiLoteWO_DESCRIP: TStringField
      FieldName = 'WO_DESCRIP'
      Size = 50
    end
  end
  object cdsTOpera: TZetaLookupDataSet
    Tag = 6
    Aggregates = <>
    IndexFieldNames = 'TO_CODIGO'
    Params = <>
    BeforePost = cdsTablaBeforePost
    AfterPost = AfterPost
    AfterDelete = AfterPost
    OnReconcileError = ReconcileError
    AlAdquirirDatos = CatalogosAlAdquirirDatos
    AlModificar = CatalogosAlModificar
    UsaCache = True
    LookupName = 'Cat'#225'logo de Clasificaci'#243'n de Operaciones'
    LookupDescriptionField = 'TO_DESCRIP'
    LookupKeyField = 'TO_CODIGO'
    Left = 264
    Top = 52
  end
  object cdsDefSteps: TZetaLookupDataSet
    Tag = 7
    Aggregates = <>
    IndexFieldNames = 'DF_SEQUENC'
    Params = <>
    BeforeInsert = cdsDefStepsBeforeInsert
    AfterInsert = cdsDefStepsAfterDelete
    AfterEdit = cdsDefStepsAfterDelete
    AfterDelete = cdsDefStepsAfterDelete
    OnNewRecord = cdsDefStepsNewRecord
    AlCrearCampos = cdsDefStepsAlCrearCampos
    LookupName = 'Pasos Est'#225'ndar'
    LookupKeyField = 'AR_CODIGO'
    Left = 114
    Top = 156
  end
  object cdsArea: TZetaLookupDataSet
    Tag = 15
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    AfterOpen = cdsAreaAfterOpen
    BeforePost = cdsTablaBeforePost
    AfterPost = cdsAreaAfterPost
    AfterDelete = cdsAreaAfterDelete
    OnNewRecord = cdsAreaNewRecord
    OnReconcileError = ReconcileError
    AlAdquirirDatos = CatalogosAlAdquirirDatos
    AlEnviarDatos = cdsAreaAlEnviarDatos
    AlModificar = CatalogosAlModificar
    UsaCache = True
    LookupName = 'Cat'#225'logo de Areas'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    Left = 264
    Top = 101
  end
  object cdsWOrderLookup: TZetaLookupDataSet
    Tag = 14
    Aggregates = <>
    IndexFieldNames = 'WO_NUMBER'
    Params = <>
    OnReconcileError = ReconcileError
    AlCrearCampos = cdsWOrderLookupAlCrearCampos
    LookupName = 'Ordenes de Trabajo'
    LookupDescriptionField = 'WO_DESCRIP'
    LookupKeyField = 'WO_NUMBER'
    OnLookupKey = cdsWOrderLookupLookupKey
    OnLookupSearch = cdsWOrderLookupLookupSearch
    Left = 264
    Top = 214
  end
  object cdsWOrderSteps: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'WO_NUMBER'
    Params = <>
    AfterCancel = cdsWOrderStepsAfterCancel
    BeforeDelete = cdsWOrderStepsBeforeDelete
    AfterDelete = cdsWOrderStepsAfterDelete
    OnNewRecord = cdsWOrderStepsNewRecord
    OnReconcileError = ReconcileError
    AlEnviarDatos = cdsWOrderStepsAlEnviarDatos
    AlCrearCampos = cdsWOrderStepsAlCrearCampos
    AlModificar = cdsWOrderStepsAlModificar
    Left = 32
    Top = 214
  end
  object cdsSteps: TZetaLookupDataSet
    Tag = 7
    Aggregates = <>
    IndexFieldNames = 'ST_SEQUENC'
    Params = <>
    BeforeInsert = cdsStepsBeforeInsert
    AfterInsert = cdsStepsAfterDelete
    AfterEdit = cdsStepsAfterDelete
    AfterDelete = cdsStepsAfterDelete
    OnNewRecord = cdsStepsNewRecord
    OnReconcileError = ReconcileError
    AlCrearCampos = cdsStepsAlCrearCampos
    LookupName = 'Pasos Est'#225'ndar'
    LookupKeyField = 'WO_NUMBER'
    Left = 114
    Top = 214
  end
  object cdsHistorialWOrder: TZetaClientDataSet
    Tag = 14
    Aggregates = <>
    IndexFieldNames = 'WO_NUMBER'
    Params = <>
    AfterPost = AfterPost
    AfterDelete = cdsHistorialWOrderAfterDelete
    OnReconcileError = ReconcileError
    AlCrearCampos = cdsHistorialWOrderAlCrearCampos
    AlModificar = CatalogosAlModificar
    Left = 122
    Top = 101
  end
  object cdsKardexWOrder: TZetaClientDataSet
    Tag = 14
    Aggregates = <>
    IndexFieldNames = 'WO_NUMBER'
    Params = <>
    BeforeDelete = cdsKardexWOrderBeforeDelete
    OnReconcileError = ReconcileError
    AlCrearCampos = cdsKardexWOrderAlCrearCampos
    Left = 32
    Top = 264
  end
  object cdsWorksHijo: TZetaClientDataSet
    Tag = 10
    Aggregates = <>
    IndexFieldNames = 'AU_FECHA'
    Params = <>
    OnReconcileError = ReconcileError
    AlCrearCampos = cdsWorksHijoAlCrearCampos
    Left = 114
    Top = 264
  end
  object cdsWorks: TZetaClientDataSet
    Tag = 10
    Aggregates = <>
    IndexFieldNames = 'WK_HORA_A'
    Params = <>
    BeforePost = cdsWorksBeforePost
    AfterDelete = cdsWorksAfterDelete
    OnCalcFields = cdsWorksCalcFields
    OnNewRecord = cdsWorksNewRecord
    OnReconcileError = ReconcileError
    AlAdquirirDatos = cdsWorksAlAdquirirDatos
    AlEnviarDatos = cdsWorksAlEnviarDatos
    AlCrearCampos = cdsWorksAlCrearCampos
    AlBorrar = cdsWorksAlBorrar
    AlModificar = cdsWorksAlModificar
    Left = 114
    Top = 52
  end
  object cdsAusencia: TZetaClientDataSet
    Tag = 10
    Aggregates = <>
    IndexFieldNames = 'AU_FECHA'
    Params = <>
    OnCalcFields = cdsAusenciaCalcFields
    AlCrearCampos = cdsAusenciaAlCrearCampos
    Left = 32
    Top = 52
  end
  object cdsDataSet: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 184
    Top = 101
  end
  object cdsCedulas: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    BeforePost = cdsCedulasBeforePost
    BeforeCancel = cdsCedulasBeforeCancel
    AfterCancel = cdsCedulasAfterCancel
    AfterDelete = cdsCedulasAfterDelete
    OnNewRecord = cdsCedulasNewRecord
    OnReconcileError = ReconcileError
    AlEnviarDatos = cdsCedulasAlEnviarDatos
    AlCrearCampos = cdsCedulasAlCrearCampos
    AlModificar = cdsCedulasAlModificar
    Left = 184
    Top = 156
  end
  object cdsBreaks: TZetaLookupDataSet
    Tag = 14
    Aggregates = <>
    IndexFieldNames = 'BR_CODIGO'
    Params = <>
    AfterOpen = cdsBreaksAfterOpen
    BeforePost = cdsBreaksBeforePost
    AfterCancel = cdsBreaksAfterCancel
    BeforeDelete = cdsBreaksBeforeDelete
    AfterDelete = cdsBreaksAfterDelete
    OnReconcileError = ReconcileError
    AlAdquirirDatos = cdsBreaksAlAdquirirDatos
    AlEnviarDatos = cdsBreaksAlEnviarDatos
    AlModificar = cdsBreaksAlModificar
    LookupName = 'Breaks'
    LookupDescriptionField = 'BR_NOMBRE'
    LookupKeyField = 'BR_CODIGO'
    Left = 337
    Top = 214
  end
  object cdsBrkHora: TZetaLookupDataSet
    Tag = 10
    Aggregates = <>
    IndexFieldNames = 'BR_CODIGO'
    Params = <>
    AfterOpen = cdsBrkHoraAfterOpen
    AfterDelete = cdsBrkHoraAfterDelete
    OnNewRecord = cdsBrkHoraNewRecord
    OnReconcileError = ReconcileError
    Left = 184
    Top = 264
  end
  object cdsASCII: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 184
    Top = 214
    object cdsASCIIRenglon: TStringField
      FieldName = 'Renglon'
      Size = 1024
    end
  end
  object cdsCedEmp: TZetaClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'CB_CODIGO'
        Fields = 'CB_CODIGO'
        Options = [ixPrimary, ixUnique]
      end>
    IndexName = 'CB_CODIGO'
    Params = <>
    StoreDefs = True
    AfterOpen = cdsCedEmpAfterOpen
    AfterInsert = cdsCedulasHuboCambios
    AfterEdit = cdsCedulasHuboCambios
    AfterDelete = cdsCedulasHuboCambios
    AlAdquirirDatos = cdsCedEmpAlAdquirirDatos
    Left = 416
    Top = 320
    object cdsCedEmpCE_FOLIO: TIntegerField
      FieldName = 'CE_FOLIO'
    end
    object cdsCedEmpCB_CODIGO: TIntegerField
      FieldName = 'CB_CODIGO'
    end
    object cdsCedEmpCE_POSICIO: TSmallintField
      FieldName = 'CE_POSICIO'
    end
    object cdsCedEmpCB_PUESTO: TStringField
      FieldName = 'CB_PUESTO'
      FixedChar = True
      Size = 6
    end
    object cdsCedEmpPRETTYNAME: TStringField
      FieldName = 'PRETTYNAME'
      Origin = 'TRESS DATOS.COLABORA.CB_APE_PAT'
      Size = 93
    end
  end
  object cdsCedMulti: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsCedMultiAfterOpen
    AfterInsert = cdsCedulasHuboCambios
    AfterEdit = cdsCedulasHuboCambios
    AfterDelete = cdsCedulasHuboCambios
    OnReconcileError = cdsCedMultiReconcileError
    AlAdquirirDatos = cdsCedMultiAlAdquirirDatos
    AlEnviarDatos = cdsCedMultiAlEnviarDatos
    Left = 416
    Top = 264
    object cdsCedMultiCE_FOLIO: TIntegerField
      FieldName = 'CE_FOLIO'
      Origin = 'TRESS DATOS.CED_WORD.CE_FOLIO'
    end
    object cdsCedMultiWO_NUMBER: TStringField
      FieldName = 'WO_NUMBER'
      Origin = 'TRESS DATOS.CED_WORD.WO_NUMBER'
    end
    object cdsCedMultiCW_POSICIO: TSmallintField
      FieldName = 'CW_POSICIO'
      Origin = 'TRESS DATOS.CED_WORD.CW_POSICIO'
    end
    object cdsCedMultiCW_PIEZAS: TFloatField
      FieldName = 'CW_PIEZAS'
    end
    object cdsCedMultiWO_DESCRIP: TStringField
      FieldName = 'WO_DESCRIP'
      Size = 50
    end
    object cdsCedMultiAR_CODIGO: TStringField
      FieldName = 'AR_CODIGO'
      Size = 15
    end
  end
  object cdsSupArea: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 264
    Top = 264
  end
  object cdsKarArea: TZetaClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'Descendente'
        Fields = 'KA_FECHA;KA_HORA'
        Options = [ixDescending]
      end>
    IndexName = 'Descendente'
    Params = <>
    StoreDefs = True
    BeforePost = cdsKarAreaBeforePost
    AfterDelete = cdsKarAreaAfterDelete
    OnNewRecord = cdsKarAreaNewRecord
    OnReconcileError = ReconcileError
    AlAdquirirDatos = cdsKarAreaAlAdquirirDatos
    AlEnviarDatos = cdsKarAreaAlEnviarDatos
    AlCrearCampos = cdsKarAreaAlCrearCampos
    AlModificar = cdsKarAreaAlModificar
    Left = 337
    Top = 264
  end
  object cdsTDefecto: TZetaLookupDataSet
    Tag = 25
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    BeforePost = cdsTablaBeforePost
    AfterPost = AfterPost
    AfterDelete = AfterPost
    OnReconcileError = ReconcileError
    AlAdquirirDatos = CatalogosAlAdquirirDatos
    AlModificar = CatalogosAlModificar
    UsaCache = True
    LookupName = 'Clase de Defectos'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    Left = 416
    Top = 8
  end
  object cdsHistorialCedInsp: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'CI_FOLIO'
    Params = <>
    BeforeDelete = cdsHistorialCedInspBeforeDelete
    AlCrearCampos = cdsHistorialCedInspAlCrearCampos
    Left = 416
    Top = 52
  end
  object cdsCedulaLookup: TZetaLookupDataSet
    Tag = 14
    Aggregates = <>
    IndexFieldNames = 'CE_FOLIO'
    Params = <>
    OnReconcileError = ReconcileError
    AlCrearCampos = cdsCedulaAlCrearCampos
    LookupName = 'C'#233'dula'
    LookupDescriptionField = 'CE_COMENTA'
    LookupKeyField = 'CE_FOLIO'
    OnLookupKey = cdsCedulaLookupKey
    OnLookupSearch = cdsCedulaLookupSearch
    Left = 416
    Top = 214
  end
  object cdsCedInsp: TZetaClientDataSet
    Tag = 26
    Aggregates = <>
    IndexFieldNames = 'CI_FOLIO'
    Params = <>
    BeforePost = cdsCedInspBeforePost
    AfterCancel = cdsCedInspAfterCancel
    OnNewRecord = cdsCedInspNewRecord
    OnReconcileError = cdsCedInspReconcileError
    AlEnviarDatos = cdsCedInspAlEnviarDatos
    AlCrearCampos = cdsCedInspAlCrearCampos
    AlModificar = cdsCedInspAlModificar
    Left = 416
    Top = 101
  end
  object cdsDefectos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsDefectosAfterOpen
    AfterInsert = cdsCedInspHuboCambios
    AfterEdit = cdsCedInspHuboCambios
    BeforePost = cdsDefectosBeforePost
    AfterDelete = cdsCedInspHuboCambios
    OnNewRecord = cdsDefectosNewRecord
    AlCrearCampos = cdsDefectosAlCrearCampos
    Left = 416
    Top = 156
  end
  object cdsMotivoScrap: TZetaLookupDataSet
    Tag = 28
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    BeforePost = cdsTablaBeforePost
    AfterPost = AfterPost
    AfterDelete = AfterPost
    OnReconcileError = ReconcileError
    AlAdquirirDatos = CatalogosAlAdquirirDatos
    AlModificar = CatalogosAlModificar
    UsaCache = True
    LookupName = 'Motivos de Scrap'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    Left = 32
    Top = 320
  end
  object cdsComponentes: TZetaLookupDataSet
    Tag = 29
    Aggregates = <>
    IndexFieldNames = 'CN_CODIGO'
    Params = <>
    BeforePost = cdsTablaBeforePost
    AfterPost = AfterPost
    AfterDelete = AfterPost
    OnReconcileError = ReconcileError
    AlAdquirirDatos = cdsComponentesAlAdquirirDatos
    AlCrearCampos = cdsComponentesAlCrearCampos
    AlAgregar = cdsComponentesAlAgregar
    AlModificar = cdsComponentesAlModificar
    UsaCache = True
    LookupName = 'Cat'#225'logo de Componentes'
    LookupDescriptionField = 'CN_NOMBRE'
    LookupKeyField = 'CN_CODIGO'
    Left = 104
    Top = 320
  end
  object cdsCedScrap: TZetaClientDataSet
    Tag = 30
    Aggregates = <>
    IndexFieldNames = 'CS_FOLIO'
    Params = <>
    BeforePost = cdsCedScrapBeforePost
    AfterCancel = cdsCedScrapAfterCancel
    OnNewRecord = cdsCedScrapNewRecord
    OnReconcileError = ReconcileError
    AlEnviarDatos = cdsCedScrapAlEnviarDatos
    AlCrearCampos = cdsCedScrapAlCrearCampos
    AlModificar = cdsCedScrapAlModificar
    Left = 272
    Top = 320
  end
  object cdsScrap: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsScrapAfterOpen
    AfterInsert = cdsCedScrapHuboCambios
    AfterEdit = cdsCedScrapHuboCambios
    BeforePost = cdsScrapBeforePost
    AfterDelete = cdsCedScrapHuboCambios
    OnNewRecord = cdsScrapNewRecord
    OnReconcileError = cdsScrapReconcileError
    AlCrearCampos = cdsScrapAlCrearCampos
    Left = 337
    Top = 320
  end
  object cdsHistorialCedScrap: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'CS_FOLIO'
    Params = <>
    BeforeDelete = cdsHistorialCedScrapBeforeDelete
    AlCrearCampos = cdsHistorialCedScrapAlCrearCampos
    Left = 184
    Top = 320
  end
  object cdsEditComponentes: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'CN_CODIGO'
    Params = <>
    BeforePost = cdsEditComponentesBeforePost
    AfterDelete = cdsEditComponentesAfterDelete
    OnReconcileError = ReconcileError
    AlAdquirirDatos = cdsEditComponentesAlAdquirirDatos
    AlEnviarDatos = cdsEditComponentesAlEnviarDatos
    Left = 104
    Top = 380
  end
  object cdsTMaquinas: TZetaLookupDataSet
    Tag = 32
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    AfterEdit = AfterPost
    BeforePost = cdsTMaquinasBeforePost
    AfterPost = AfterPost
    AfterDelete = AfterPost
    OnReconcileError = ReconcileError
    AlAdquirirDatos = CatalogosAlAdquirirDatos
    AlModificar = CatalogosAlModificar
    UsaCache = True
    LookupName = 'Cat'#225'logo de Tipos de M'#225'quinas'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    Left = 520
    Top = 12
  end
  object cdsMaquinas: TZetaLookupDataSet
    Tag = 33
    Aggregates = <>
    IndexFieldNames = 'MQ_CODIGO'
    Params = <>
    AfterEdit = AfterPost
    BeforePost = cdsMaquinasBeforePost
    AfterPost = AfterPost
    AfterDelete = AfterPost
    OnNewRecord = cdsMaquinasNewRecord
    OnReconcileError = ReconcileError
    AlAdquirirDatos = CatalogosAlAdquirirDatos
    AlCrearCampos = cdsMaquinasAlCrearCampos
    AlModificar = CatalogosAlModificar
    LookupName = 'Cat'#225'logo de M'#225'quinas'
    LookupDescriptionField = 'MQ_NOMBRE'
    LookupKeyField = 'MQ_CODIGO'
    Left = 520
    Top = 60
  end
  object cdsMaqCertific: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    BeforePost = cdsMaqCertificBeforePost
    OnNewRecord = cdsMaqCertificNewRecord
    AlAdquirirDatos = cdsMaqCertificAlAdquirirDatos
    AlEnviarDatos = cdsMaqCertificAlEnviarDatos
    AlCrearCampos = cdsMaqCertificAlCrearCampos
    AlAgregar = cdsMaqCertificAlAgregar
    AlBorrar = cdsMaqCertificAlAgregar
    AlModificar = cdsMaqCertificAlAgregar
    Left = 520
    Top = 120
  end
  object cdsLayouts: TZetaLookupDataSet
    Tag = 36
    Aggregates = <>
    IndexFieldNames = 'LY_CODIGO'
    Params = <>
    AfterEdit = AfterPost
    BeforePost = cdsLayoutsBeforePost
    AfterPost = AfterPost
    AfterDelete = AfterPost
    OnReconcileError = ReconcileError
    AlAdquirirDatos = CatalogosAlAdquirirDatos
    AlCrearCampos = cdsLayoutsAlCrearCampos
    AlModificar = CatalogosAlModificar
    LookupName = 'Cat'#225'logo Layouts'
    LookupDescriptionField = 'LY_NOMBRE'
    LookupKeyField = 'LY_CODIGO'
    Left = 520
    Top = 228
  end
  object cdsLayMaquinas: TZetaLookupDataSet
    Tag = 38
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsLayMaquinasAlAdquirirDatos
    AlEnviarDatos = cdsLayMaquinasAlEnviarDatos
    LookupName = 'Cat'#225'logo M'#225'quinas por Layout'
    LookupDescriptionField = 'MQ_CODIGO'
    LookupKeyField = 'MQ_CODIGO'
    Left = 520
    Top = 276
  end
  object cdsSillasMaquina: TZetaClientDataSet
    Tag = 40
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsLayMaquinasAlAdquirirDatos
    AlEnviarDatos = cdsSillasMaquinaAlEnviarDatos
    Left = 520
    Top = 332
  end
  object cdsSillasLayout: TZetaClientDataSet
    Tag = 39
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsLayMaquinasAlAdquirirDatos
    AlEnviarDatos = cdsSillasLayoutAlEnviarDatos
    Left = 520
    Top = 388
  end
  object cdsKarEmpMaquinas: TZetaClientDataSet
    Tag = 37
    Aggregates = <>
    Params = <>
    AfterEdit = AfterPost
    AfterPost = AfterPost
    AfterDelete = AfterPost
    OnNewRecord = cdsKarEmpMaquinasNewRecord
    OnReconcileError = ReconcileError
    AlAdquirirDatos = cdsKarEmpMaquinasAlAdquirirDatos
    AlCrearCampos = cdsKarEmpMaquinasAlCrearCampos
    AlModificar = CatalogosAlModificar
    Left = 328
    Top = 396
  end
end
