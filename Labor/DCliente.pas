unit DCliente;

interface

{$DEFINE REPORTING}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, DBClient,
     DBaseCliente,
{$ifdef DOS_CAPAS}
     DServerCatalogos,
     DServerConsultas,
     DServerGlobal,
     DServerSistema,
     DServerTablas,
     DServerAsistencia,
     DServerReportes,
     {$IFDEF REPORTING}
     DServerReporting,
     {$endif}
     DServerCalcNomina,
     DServerRecursos,
     DServerLabor,
     DServerAnualNomina,
{$endif}
     ZetaClientDataSet,
     ZetaCommonClasses,
     ZetaCommonLists;

type
  TdmCliente = class(TBaseCliente)
    cdsEmpleado: TZetaClientDataSet;
    cdsPeriodo: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsEmpleadoAlAdquirirDatos(Sender: TObject);
  private
    { Private declarations }
    FEmpleado: TNumEmp;
    FEmpleadoBOF: Boolean;
    FEmpleadoEOF: Boolean;
    FFecha: TDate;
{$ifdef DOS_CAPAS}
    FServerCatalogos: TdmServerCatalogos;
    FServerConsultas: TdmServerConsultas;
    FServerGlobal: TdmServerGlobal;
    FServerSistema: TdmServerSistema;
    FServerTablas: TdmServerTablas;
    FServerAsistencia: TdmServerAsistencia;
    FServerReportes : TdmServerReportes;
    {$ifdef REPORTING}
    FServerReporteador: TdmServerReporting;
    {$endif}
    FServerCalcNomina: TdmServerCalcNomina;
    FServerRecursos: TdmServerRecursos;
    FServerLabor: TdmServerLabor;
    FServerAnualNomina: TdmServerAnualNomina;
{$endif}
    function FetchEmployee(const iEmpleado: TNumEmp): Boolean;
    function FetchNextEmployee(const iEmpleado: TNumEmp): Boolean;
    function FetchPreviousEmployee(const iEmpleado: TNumEmp): Boolean;
    function GetAsistenciaDescripcion: String;
    function GetEmpleadoDescripcion: String;
    function EsEmpleado: boolean;
  protected
    procedure InitCacheModules; override;
  public
    { Public declarations }
    property Empleado: TNumEmp read FEmpleado;
    property Fecha: TDate read FFecha write FFecha;
    procedure GetStatusEmpleado(EmpleadoActivo: TInfoEmpleado; var Caption, Hint : string; var Color : TColor );
{$ifdef DOS_CAPAS}
    property ServerCatalogos: TdmServerCatalogos read FServerCatalogos;
    property ServerConsultas: TdmServerConsultas read FServerConsultas;
    property ServerGlobal: TdmServerGlobal read FServerGlobal;
    property ServerSistema: TdmServerSistema read FServerSistema;
    property ServerTablas: TdmServerTablas read FServerTablas;
    property ServerAsistencia: TdmServerAsistencia read FServerAsistencia;
    property ServerReportes: TdmServerReportes read FServerReportes;
    {$ifdef REPORTING}
    property ServerReporteador: TdmServerReporting read FServerReporteador;
    {$endif}
    property ServerCalcNomina: TdmServerCalcNomina read FServerCalcNomina;
    property ServerRecursos: TdmServerRecursos read FServerRecursos;
    property ServerLabor: TdmServerLabor read FServerLabor;
    property ServerAnualNomina: TdmServerAnualNomina read FServerAnualNomina;
{$endif}
    function GetValorActivoStr(const eTipo: TipoEstado): String; override;
    function GetEmpleadoAnterior: Boolean;
    function GetEmpleadoInicial: Boolean;
    function GetEmpleadoPrimero: Boolean;
    function GetEmpleadoSiguiente: Boolean;
    function GetEmpleadoUltimo: Boolean;
    function GetDatosEmpleadoActivo: TInfoEmpleado;
    function SetEmpleadoNumero(const Value: TNumEmp): Boolean;
    function GetDatosPeriodoActivo: TDatosPeriodo;
    function GetPeriodoInicial( const iYear: integer; const eTipo: eTipoPeriodo ): Boolean;
    procedure CargaActivosTodos(Parametros: TZetaParams); override;
    procedure CargaActivosIMSS( Parametros: TZetaParams );
    procedure CargaActivosPeriodo(Parametros: TZetaParams);
    procedure CargaActivosSistema(Parametros: TZetaParams);
    procedure InitActivosEmpleado;
    function EmpleadoUpEnabled: Boolean;
    function EmpleadoDownEnabled: Boolean;
    function GetEmpresas : OleVariant;
  end;

var
  dmCliente: TdmCliente;

implementation

{$R *.DFM}

uses DLabor,
     DTablas,
     DCatalogos,
     DDiccionario,
     ZGlobalTress,
     DGlobal,
     ZetaCommonTools;

{ ************** TdmCliente *************** }

procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
     inherited;
     ModoLabor := TRUE;
{$ifdef DOS_CAPAS}
     FServerCatalogos := TdmServerCatalogos.Create( Self );
     FServerConsultas := TdmServerConsultas.Create( Self );
     FServerGlobal := TdmServerGlobal.Create( Self );
     FServerSistema := TdmServerSistema.Create( Self );
     FServerTablas := TdmServerTablas.Create( Self );
     FServerAsistencia := TdmServerAsistencia.Create( Self );
     FServerReportes := TdmServerReportes.Create( Self );
     {$ifdef REPORTING}
     FServerReporteador := TdmServerReporting.Create( self );
     {$endif}
     FServerCalcNomina := TdmServerCalcNomina.Create( Self );
     FServerRecursos := TdmServerRecursos.Create( Self );
     FServerLabor := TdmServerLabor.Create( Self );
     FServerAnualNomina := TdmServerAnualNomina.Create( Self );
{$endif}
end;

procedure TdmCliente.DataModuleDestroy(Sender: TObject);
begin
{$ifdef DOS_CAPAS}
     FreeAndNil( FServerAnualNomina );
     FreeAndNil( FServerLabor );
     FreeAndNil( FServerRecursos );
     FreeAndNil( FServerCalcNomina );
     {$ifdef REPORTING}
     FreeAndNil( FServerReporteador );
     {$endif}
     FreeAndNil( FServerReportes );
     FreeAndNil( FServerAsistencia );
     FreeAndNil( FServerTablas );
     FreeAndNil( FServerSistema );
     FreeAndNil( FServerGlobal );
     FreeAndNil( FServerConsultas );
     FreeAndNil( FServerCatalogos );
{$endif}
     inherited;
end;

function TdmCliente.EmpleadoDownEnabled: Boolean;
begin
    with cdsEmpleado do
     begin
          Result := Active and not IsEmpty and not FEmpleadoBOF;
     end;
end;

function TdmCliente.EmpleadoUpEnabled: Boolean;
begin
   with cdsEmpleado do
     begin
          Result := Active and not IsEmpty and not FEmpleadoEOF;
     end;
end;

function TdmCliente.EsEmpleado: boolean;
begin
   //Result := ( eExTipo( dmCliente.cdsEmpleado.FieldByName( 'CB_TIPREV' ).AsInteger ) = exEmpleado );
   result := true;
end;

procedure TdmCliente.InitCacheModules;
begin
     inherited;
     DataCache.AddObject( 'Tabla', dmTablas );
     DataCache.AddObject( 'Cat�logo', dmCatalogos );
     DataCache.AddObject( 'Diccionario', dmDiccionario );
     DataCache.AddObject( 'Labor', dmLabor );
end;

procedure TdmCliente.CargaActivosIMSS( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddString( 'RegistroPatronal', '' );
          AddInteger( 'IMSSYear', 0 );
          AddInteger( 'IMSSMes', 0 );
          AddInteger( 'IMSSTipo', 0 );
     end;
end;

procedure TdmCliente.CargaActivosPeriodo( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddInteger( 'Year', TheYear( Fecha ) );
          AddInteger( 'Tipo', 0 );
          AddInteger( 'Numero', 0 );
     end;
end;

procedure TdmCliente.CargaActivosSistema( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddDate( 'FechaAsistencia', Date );
          AddDate( 'FechaDefault', Fecha );
          AddInteger( 'YearDefault', TheYear( Date ) );
          AddInteger( 'EmpleadoActivo', Empleado );
          AddString( 'NombreUsuario', GetDatosUsuarioActivo.Nombre );
          AddString( 'CodigoEmpresa', GetDatosEmpresaActiva.Codigo );
     end;
end;

procedure TdmCliente.InitActivosEmpleado;
begin
     FEmpleadoBOF := False;
     FEmpleadoEOF := False;
     FEmpleado := 0;
     FFecha := Date;
     GetEmpleadoInicial;
end;

procedure TdmCliente.CargaActivosTodos(Parametros: TZetaParams);
begin
     CargaActivosIMSS( Parametros );
     CargaActivosPeriodo( Parametros );
     CargaActivosSistema( Parametros );
end;


function TdmCliente.GetEmpleadoDescripcion: String;
begin
     with cdsEmpleado do
     begin
          Result := IntToStr( FEmpleado ) + ': '+ FieldByName( 'PRETTYNAME' ).AsString;
     end;
end;

function TdmCliente.GetAsistenciaDescripcion: String;
begin
     Result := FechaCorta( Fecha );
end;

function TdmCliente.GetValorActivoStr(const eTipo: TipoEstado): String;
begin
     case eTipo of
          stEmpleado: Result := GetEmpleadoDescripcion;
          //stPeriodo: Result := GetPeriodoDescripcion;
          //stIMSS: Result := GetIMSSDescripcion;
          stFecha: Result := GetAsistenciaDescripcion;
          //stSistema: Result := GetSistemaDescripcion;
     end;
end;

function TdmCliente.GetEmpleadoAnterior: Boolean;
begin
     Result := FetchPreviousEmployee( FEmpleado );
     if Result then
     begin
          with cdsEmpleado do
          begin
               FEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
          end;
          FEmpleadoEOF := False;
     end
     else
         FEmpleadoBOF := True;
end;

function TdmCliente.GetEmpleadoSiguiente: Boolean;
begin
     Result := FetchNextEmployee( FEmpleado );
     if Result then
     begin
          with cdsEmpleado do
          begin
               FEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
          end;
          FEmpleadoBOF := False;
     end
     else
         FEmpleadoEOF := True;
end;

function TdmCliente.GetEmpleadoUltimo: Boolean;
var
   iNewEmp: TNumEmp;
begin
     Result := FetchPreviousEmployee( MAXINT );
     if Result then
     begin
          with cdsEmpleado do
          begin
               iNewEmp := FieldByName( 'CB_CODIGO' ).AsInteger;
               FEmpleadoBOF := False;
               FEmpleado := iNewEmp;
               FEmpleadoEOF := ( iNewEmp = FEmpleado );
          end;
     end;
end;


function TdmCliente.GetEmpresas: OleVariant;
begin
     Result := Servidor.GetCompanys( 0, Ord( tc3Datos ) );
end;

function TdmCliente.FetchEmployee( const iEmpleado: TNumEmp ): Boolean;
var
   Datos: OleVariant;
begin
     Result := Servidor.GetEmpleado( Empresa, iEmpleado, Datos );
     if Result then
     begin
          with cdsEmpleado do
          begin
               Data := Datos;
          end;
     end;
end;

function TdmCliente.FetchPreviousEmployee( const iEmpleado: TNumEmp ): Boolean;
var
   Datos: OleVariant;
begin
     Result := Servidor.GetEmpleadoAnterior( Empresa, iEmpleado,VACIO, Datos );
     if Result then
     begin
          with cdsEmpleado do
          begin
               Data := Datos;
          end;
     end;
end;

function TdmCliente.FetchNextEmployee( const iEmpleado: TNumEmp ): Boolean;
var
   Datos: OleVariant;
begin
     Result := Servidor.GetEmpleadoSiguiente( Empresa, iEmpleado,VACIO, Datos );
     if Result then
     begin
          with cdsEmpleado do
          begin
               Data := Datos;
          end;
     end;
end;

function TdmCliente.GetEmpleadoPrimero: Boolean;
var
   iNewEmp: TNumEmp;
begin
     Result := FetchNextEmployee( 0 );
     if Result then
     begin
          with cdsEmpleado do
          begin
               iNewEmp := FieldByName( 'CB_CODIGO' ).AsInteger;
               FEmpleado := iNewEmp;
               FEmpleadoBOF := ( iNewEmp = FEmpleado );
               FEmpleadoEOF := False;
          end;
     end;
end;

function TdmCliente.GetEmpleadoInicial: Boolean;
var
   Datos: OleVariant;
begin
     Result := Servidor.GetEmpleadoAnterior( Empresa, MAXINT,VACIO, Datos );
     with cdsEmpleado do
     begin
          Data := Datos;
          FEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
     end;
     FEmpleadoBOF := False;
end;

function TdmCliente.GetDatosEmpleadoActivo: TInfoEmpleado;
begin
     with Result do
     begin
          Numero := Self.Empleado;
          with cdsEmpleado do
          begin
               Nombre := FieldByName( 'PRETTYNAME' ).AsString;
               Activo := zStrToBool( FieldByName( 'CB_ACTIVO' ).AsString );
               Baja := FieldByName( 'CB_FEC_BAJ' ).AsDateTime;
               Ingreso := FieldByName( 'CB_FEC_ING' ).AsDateTime;
               Recontratable := TRUE; //no mostrar hint de no recontratable
          end;
     end;
end;

procedure TdmCliente.cdsEmpleadoAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     FetchEmployee( FEmpleado );
end;

function TdmCliente.SetEmpleadoNumero( const Value: TNumEmp ): Boolean;
begin
     if ( FEmpleado <> Value ) then
     begin
          Result := FetchEmployee( Value );
          if Result then
          begin
               with cdsEmpleado do
               begin
                    FEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
               end;
               FEmpleadoBOF := False;
               FEmpleadoEOF := False;
          end;
     end
     else
         Result := True;
end;


//MV: 04/junio/2003 Se Esta usando solo en ZReportModulo y por eso esta declarado solo el metodo
function TdmCliente.GetDatosPeriodoActivo: TDatosPeriodo;
begin
end;


function TdmCliente.GetPeriodoInicial( const iYear: integer; const eTipo: eTipoPeriodo ): Boolean;
begin
     with cdsPeriodo do
     begin
          Data := Servidor.GetPeriodoInicial( Empresa, iYear, Ord( eTipo ) );
          Result := not IsEmpty;
     end;
end;

procedure TdmCliente.GetStatusEmpleado(EmpleadoActivo: TInfoEmpleado;
  var Caption, Hint: string; var Color: TColor);
const
   aRecontratable: array[ FALSE..TRUE ] of PChar = ( ZetaCommonClasses.CR_LF + 'NO RECONTRATAR', VACIO );
begin
      with EmpleadoActivo do
      begin
            if (not Global.GetGlobalBooleano(K_GLOBAL_CB_ACTIVO_AL_DIA) ) then
            begin
                 if Activo then
                 begin
                      if ( Baja = NullDateTime ) then
                      begin
                           Caption := 'Activo';
                           Hint := 'Ingres� ' + FechaCorta( Ingreso );
                           Color := clBlack;
                      end
                      else
                      begin
                           Caption := 'Reingreso';
                           Hint := 'Baja: ' + FechaCorta( Baja ) + ' Reingreso: ' + FechaCorta( Ingreso );
                           Color := clGreen;
                      end;
                 end
                 else
                 begin
                      Caption := 'Baja';
                      Hint := 'Baja desde ' + FechaCorta( Baja ) + aRecontratable[ Recontratable ];
                      Color := clRed;
                 end;
            end
            else
            begin
                 if Activo then
                 begin
                      if ( Baja = NullDateTime ) then
                      begin
                           if ( Ingreso > FechaDefault ) then
                           begin
                                Caption := 'Antes ingreso';
                                Hint := 'Ingresa ' + FechaCorta( Ingreso );
                                Color := clRed;
                           end
                           else
                           begin
                                Caption := 'Activo';
                                Hint := 'Ingres� ' + FechaCorta( Ingreso );
                                Color := clBlue;
                           end;
                      end
                      else if (Ingreso <= FechaDefault) then
                      begin
                           Caption := 'Reingreso';
                           Hint := 'Baja: ' + FechaCorta( Baja ) + ' Reingreso: ' + FechaCorta( Ingreso );
                           Color := clGreen;
                      end
                      else
                      begin
                           Caption := 'Baja';
                           Hint := 'Baja desde ' + FechaCorta( Baja )+ aRecontratable [ Recontratable ];
                           Color := clRed;
                      end;
                 end
                 else
                 begin
                      if not EsEmpleado then
                      begin
                           Caption := 'Antes ingreso';
                           //Hint := 'Baja desde ' + FechaCorta( Baja )+ aRecontratable[ Recontratable ];
                           Color := clBlue;
                      end
                      else if Baja < FechaDefault then
                      begin
                           Caption := 'Baja';
                           Hint := 'Baja desde ' + FechaCorta( Baja )+ aRecontratable[ Recontratable ];
                           Color := clRed;
                      end
                      else
                      begin
                           Caption := 'Activo';
                           Hint := 'Ingres� ' + FechaCorta( Ingreso );
                           Color := clBlack;
                      end;
                 end;
            end;
      end;


end;

end.

