object BuscaCedulas: TBuscaCedulas
  Left = 139
  Top = 53
  VertScrollBar.Visible = False
  AutoScroll = False
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'B�squeda de C�dulas'
  ClientHeight = 194
  ClientWidth = 630
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanelControles: TPanel
    Left = 0
    Top = 0
    Width = 630
    Height = 197
    Align = alTop
    TabOrder = 0
    object lbUsuario: TLabel
      Left = 93
      Top = 166
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = '&Usuario:'
      FocusControl = US_CODIGO
    end
    object lbArea: TLabel
      Left = 107
      Top = 144
      Width = 25
      Height = 13
      Alignment = taRightJustify
      Caption = 'Area:'
      FocusControl = CE_AREA
    end
    object lbOpera: TLabel
      Left = 100
      Top = 122
      Width = 32
      Height = 13
      Alignment = taRightJustify
      Caption = 'Opera:'
      FocusControl = OP_NUMBER
    end
    object lbParte: TLabel
      Left = 104
      Top = 100
      Width = 28
      Height = 13
      Alignment = taRightJustify
      Caption = 'Parte:'
      FocusControl = AR_CODIGO
    end
    object lbWorder: TLabel
      Left = 100
      Top = 78
      Width = 32
      Height = 13
      Alignment = taRightJustify
      Caption = 'Orden:'
      FocusControl = WO_NUMBER
    end
    object BuscarBtn: TBitBtn
      Left = 537
      Top = 13
      Width = 75
      Height = 25
      Anchors = [akRight]
      Caption = '&Buscar'
      Default = True
      TabOrder = 0
      OnClick = BuscarBtnClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33033333333333333F7F3333333333333000333333333333F777333333333333
        000333333333333F777333333333333000333333333333F77733333333333300
        033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
        33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
        3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
        33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
        333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
        333333773FF77333333333370007333333333333777333333333}
      NumGlyphs = 2
    end
    object AceptarBtn: TBitBtn
      Left = 537
      Top = 43
      Width = 75
      Height = 25
      Anchors = [akRight]
      Caption = '&Aceptar'
      Enabled = False
      ModalResult = 1
      TabOrder = 1
      OnClick = AceptarBtnClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object CancelarBtn: TBitBtn
      Left = 536
      Top = 73
      Width = 75
      Height = 25
      Anchors = [akRight]
      Cancel = True
      Caption = '&Salir'
      Default = True
      ModalResult = 2
      TabOrder = 2
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333333333000033338833333333333333333F333333333333
        0000333911833333983333333388F333333F3333000033391118333911833333
        38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
        911118111118333338F3338F833338F3000033333911111111833333338F3338
        3333F8330000333333911111183333333338F333333F83330000333333311111
        8333333333338F3333383333000033333339111183333333333338F333833333
        00003333339111118333333333333833338F3333000033333911181118333333
        33338333338F333300003333911183911183333333383338F338F33300003333
        9118333911183333338F33838F338F33000033333913333391113333338FF833
        38F338F300003333333333333919333333388333338FFF830000333333333333
        3333333333333333333888330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object Status: TRadioGroup
      Left = 7
      Top = 3
      Width = 93
      Height = 65
      Caption = ' Status '
      ItemIndex = 0
      Items.Strings = (
        'Todos'
        'Completo'
        'Incompleto')
      TabOrder = 3
    end
    object Tipo: TRadioGroup
      Left = 103
      Top = 3
      Width = 233
      Height = 65
      Caption = ' Tipo '
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Todos'
        'Operaciones'
        'Horas Especiales'
        'Tiempo Muerto')
      TabOrder = 4
    end
    object Filtros: TGroupBox
      Left = 339
      Top = 3
      Width = 173
      Height = 65
      Caption = '                     '
      TabOrder = 5
      object lbInicio: TLabel
        Left = 6
        Top = 18
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = '&Inicio del:'
        Enabled = False
        FocusControl = FechaInicial
      end
      object lbFin: TLabel
        Left = 39
        Top = 41
        Width = 12
        Height = 13
        Alignment = taRightJustify
        Caption = 'Al:'
        Enabled = False
        FocusControl = FechaInicial
      end
      object FechaFinal: TZetaFecha
        Left = 53
        Top = 38
        Width = 115
        Height = 22
        Cursor = crArrow
        Enabled = False
        TabOrder = 1
        Text = '02/Jan/98'
        Valor = 35797
      end
      object FechaInicial: TZetaFecha
        Left = 53
        Top = 15
        Width = 115
        Height = 22
        Cursor = crArrow
        Enabled = False
        TabOrder = 0
        Text = '02/Jan/98'
        Valor = 35797
      end
    end
    object CBFecha: TCheckBox
      Left = 353
      Top = 1
      Width = 108
      Height = 17
      Caption = 'Filtrar por Fechas'
      TabOrder = 6
      OnClick = CBFechaClick
    end
    object WO_NUMBER: TZetaKeyLookup
      Left = 134
      Top = 74
      Width = 378
      Height = 21
      LookupDataset = dmLabor.cdsWOrderLookup
      TabOrder = 7
      TabStop = True
      WidthLlave = 100
    end
    object AR_CODIGO: TZetaKeyLookup
      Left = 134
      Top = 96
      Width = 378
      Height = 21
      LookupDataset = dmLabor.cdsPartes
      TabOrder = 8
      TabStop = True
      WidthLlave = 100
    end
    object OP_NUMBER: TZetaKeyLookup
      Left = 134
      Top = 118
      Width = 378
      Height = 21
      LookupDataset = dmLabor.cdsOpera
      TabOrder = 9
      TabStop = True
      WidthLlave = 100
    end
    object CE_AREA: TZetaKeyLookup
      Left = 134
      Top = 140
      Width = 378
      Height = 21
      LookupDataset = dmLabor.cdsArea
      TabOrder = 10
      TabStop = True
      WidthLlave = 100
    end
    object US_CODIGO: TZetaKeyLookup
      Left = 134
      Top = 162
      Width = 323
      Height = 21
      LookupDataset = dmSistema.cdsUsuarios
      TabOrder = 11
      TabStop = True
      WidthLlave = 50
    end
  end
  object Grid: TZetaDBGrid
    Left = 0
    Top = 197
    Width = 630
    Height = 0
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'CE_FOLIO'
        Title.Caption = 'Folio'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CE_FECHA'
        Title.Caption = 'Fecha'
        Width = 75
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'CE_HORA'
        Title.Caption = 'Hora'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CE_TIEMPO'
        Title.Caption = 'Duraci�n'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CE_TIPO'
        Title.Caption = 'Tipo'
        Width = 95
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WO_NUMBER'
        Title.Caption = 'Orden'
        Width = 107
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AR_CODIGO'
        Title.Caption = 'Parte'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AR_NOMBRE'
        Title.Caption = 'Descripci�n'
        Width = 125
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'CE_PIEZAS'
        Title.Caption = 'Cantidad'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'OP_NUMBER'
        Title.Caption = 'Operaci�n'
        Width = 144
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CE_AREA'
        Title.Caption = 'Area'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_CODIGO'
        Title.Caption = 'Usuario'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CE_TMUERTO'
        Title.Caption = 'T. Muerto'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CE_STATUS'
        Title.Caption = 'Status'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CE_MOD1'
        Title.Caption = 'Mod. 1'
        Width = 38
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CE_MOD2'
        Title.Caption = 'Mod. 2'
        Width = 38
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CE_MOD3'
        Title.Caption = 'Mod. 3'
        Width = 38
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CE_COMENTA'
        Title.Caption = 'Comentarios'
        Width = 100
        Visible = True
      end>
  end
  object DataSource: TDataSource
    Left = 13
    Top = 95
  end
end
