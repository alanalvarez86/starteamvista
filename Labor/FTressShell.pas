unit FTressShell;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      FTressShell.pas                            ::
  :: Descripci�n: Programa principal de TressLabor.exe       ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

{$DEFINE MULTIPLES_ENTIDADES}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComCtrls, Menus, ImgList, ActnList, StdCtrls, Mask, Buttons, ExtCtrls, DB,
     ZetaCommonClasses,
//:Todo     ZBaseArbolShell,
     ZetaFecha,
     ZetaStateComboBox,
     ZetaSmartLists,
     ZBaseConsulta,
     ZetaTipoEntidad,
     ZetaCommonLists,
     ZetaClientDataSet,
     ZetaDespConsulta,
     ZetaMessages,
     ZBaseNavBarShell, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, TressMorado2013, dxRibbonSkins,
  dxSkinsdxRibbonPainter, System.Actions, cxStyles, cxClasses, dxSkinsForm,
  dxRibbon, dxStatusBar, dxRibbonStatusBar, dxSkinscxPCPainter,dxGDIPlusClasses,
  cxPCdxBarPopupMenu, dxSkinsdxNavBarPainter, dxSkinsdxBarPainter,
  cxLocalization, dxBar, dxNavBar, cxButtons, cxPC, cxContainer, cxEdit, cxImage,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, ZetaCXStateComboBox, dxSkinsDefaultPainters,
  dxBarBuiltInMenu, dxRibbonCustomizationForm;

type
  TInfoEmpleado = record
    Numero: TNumEmp;
    Nombre: String;
    Activo: Boolean;
    Baja: TDate;
    Ingreso: TDate;
    Recontratable: Boolean;
  end;

type
  TTressShell = class(TBaseNavBarShell)
    _Emp_Primero: TAction;
    _Emp_Anterior: TAction;
    _Emp_Siguiente: TAction;
    _Emp_Ultimo: TAction;
    _E_BuscarEmpleado: TAction;
    _E_CambiarEmpleado: TAction;
    _P_CalculoTiempos: TAction;
    _P_ImportarOrdenes: TAction;
    _P_ImportarPartes: TAction;
    _P_Depuracion: TAction;
    _R_MultiLote: TAction;
    _V_Empleados: TAction;
    _V_Fecha: TAction;
    _R_CedulaInspeccion: TAction;
    _P_AfectarLabor: TAction;
    _P_ImportarComponentes: TAction;
    _R_CedulaScrap: TAction;
    _P_ImportarCedulas: TAction;
    TabLabor: TdxRibbonTab;
    DevEx_BarManagerBar1: TdxBar;
    DevEx_BarManagerBar2: TdxBar;
    DevEx_BarManagerBar3: TdxBar;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton9: TdxBarLargeButton;
    dxBarLargeButton10: TdxBarLargeButton;
    EmpleadoLBL: TLabel;
    EmpleadoNumeroCB: TcxStateComboBox;
    EmpleadoPrettyName: TLabel;
    EmpleadoPrimero: TcxButton;
    EmpleadoAnterior: TcxButton;
    EmpleadoSiguiente: TcxButton;
    EmpleadoUltimo: TcxButton;
    EmpleadoBuscaBtn_DevEx: TcxButton;
    ImagenStatus_DevEx: TcxImage;
    Panel2: TPanel;
    AsistenciaFechaLBL: TLabel;
    FechaZF: TZetaFecha;
    FechaZFDia: TLabel;
    Image24_StatusEmpleado: TcxImageList;
    dxBarLargeButton11: TdxBarLargeButton;
    dxBarLargeButton12: TdxBarLargeButton;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure _V_EmpleadosExecute(Sender: TObject);
    procedure _V_FechaExecute(Sender: TObject);
    procedure ClientAreaPG_DevExTcxPageControlPropertiesTcxPCCustomButtonsButtons0Click(
      Sender: TObject);
    procedure _E_CambiarEmpleadoExecute(Sender: TObject);
    procedure PGSuperiorChange(Sender: TObject);
    procedure _Emp_PrimeroExecute(Sender: TObject);
    procedure _Emp_AnteriorExecute(Sender: TObject);
    procedure _Emp_SiguienteExecute(Sender: TObject);
    procedure _Emp_UltimoExecute(Sender: TObject);
    procedure FechaZFValidDate(Sender: TObject);
    procedure EmpleadoBuscaBtnClick(Sender: TObject);
    procedure EmpleadoNumeroCBLookUp(Sender: TObject; var lOk: Boolean);
    procedure _E_BuscarEmpleadoExecute(Sender: TObject);
    procedure _R_MultiLoteExecute(Sender: TObject);
    procedure _P_CalculoTiemposExecute(Sender: TObject);
    procedure _P_DepuracionExecute(Sender: TObject);
    procedure _P_ImportarOrdenesEecute(Sender: TObject);
    procedure _P_ImportarPartesExecute(Sender: TObject);
    procedure _R_CedulaInspeccionExecute(Sender: TObject);
    procedure _P_AfectarLaborExecute(Sender: TObject);
    procedure _P_ImportarComponentesExecute(Sender: TObject);
    procedure _R_CedulaScrapExecute(Sender: TObject);
    procedure _P_ImportarCedulasExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure _Emp_UltimoUpdate(Sender: TObject);
    procedure _Emp_PrimeroUpdate(Sender: TObject);
    procedure dxBarLargeButton4Click(Sender: TObject);
  private
    { Private declarations }
    function ChecaGlobalDefecto: boolean;
    procedure RevisaDerechos;
    procedure CargaEmpleadoActivos;
    procedure RefrescaEmpleadoActivos;
    procedure CambioEmpleadoActivo;
    procedure CargaFechaActivos;
    procedure RefrescaFechaActivos;
    procedure WMWizardEnd(var Message: TMessage); message WM_WIZARD_END;
    procedure LLenaOpcionesGrupos;
  protected
    { Protected declarations }
    procedure CargaTraducciones; override;
    function ProcesaDialogo(oLookUp: TZetaLookupDataset; const eEntidad: TipoEntidad; const sFilter: String;
             var sKey, sDescription: String; var lEncontrado: Boolean ): Boolean; override;

    procedure DoCloseAll; override;
    procedure HabilitaControles; override;
    {$ifdef MULTIPLES_ENTIDADES}
    procedure NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado); override;
    {$else}
    procedure NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado); override;
    {$endif}
  public
    { Public declarations }
    procedure DoOpenAll; override;
    function GetLookUpDataSet( const eEntidad: TipoEntidad ): TZetaLookUpDataSet; override;
    procedure SetCedulaInspeccionEnabled;
    procedure SetAfectarLaborEnabled;
  end;

var
  TressShell: TTressShell;
  StatusEmpleado: array [0..3] of string = ('Activo', 'Reingreso','Baja','Antes ingreso');

implementation

{$R *.DFM}

uses DCliente,
     DConsultas,
     DSistema,
     DCatalogos,
     DReportes,
     DDiccionario,
     DGlobal,
     DLabor,
     DTablas,
{$ifdef DOS_CAPAS}
     DZetaServerProvider,
     ZetaSQLBroker,
     ZetaRegistryServerEditor,
{$endif}
     ZetaBuscaEmpleado_DevEx,
     ZetaBuscaWOrder_DevEx,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaDialogo,
     ZGlobalTress,
     ZAccesosTress,
     ZArbolTress,
     ZAccesosMgr,
     ZetaBuscaCedula_DevEx,
     DBasicoCliente;

const
     K_PANEL_EMPLEADO = 1;
     K_PANEL_FECHA = 5;

function TTressShell.GetLookUpDataSet( const eEntidad: TipoEntidad ): TZetaLookUpDataSet;
begin
     case eEntidad of
          {Entidades de Tress}
          enEmpleado: Result := dmCliente.cdsEmpleadoLookUp;
          enNivel1: Result := dmTablas.cdsNivel1;
          enNivel2: Result := dmTablas.cdsNivel2;
          enNivel3: Result := dmTablas.cdsNivel3;
          enNivel4: Result := dmTablas.cdsNivel4;
          enNivel5: Result := dmTablas.cdsNivel5;
          enNivel6: Result := dmTablas.cdsNivel6;
          enNivel7: Result := dmTablas.cdsNivel7;
          enNivel8: Result := dmTablas.cdsNivel8;
          enNivel9: Result := dmTablas.cdsNivel9;
          enHorario: Result := dmCatalogos.cdsHorarios;
          enTurno: Result := dmCatalogos.cdsTurnos;
          enPuesto: Result := dmCatalogos.cdsPuestos;
          enClasifi: Result := dmCatalogos.cdsClasifi;          
          {Entidades de Labor}
          enOpera: Result := dmLabor.cdsOpera;
          enPartes: Result := dmLabor.cdsPartes;
          enWorder: Result := dmLabor.cdsWorderLookup;
          enModula1: Result := dmLabor.cdsModula1;
          enModula2: Result := dmLabor.cdsModula2;
          enModula3: Result := dmLabor.cdsModula3;
          enTMuerto: Result := dmLabor.cdsTiempoMuerto;
          enTParte: Result := dmLabor.cdsTParte;
          enTOpera: Result := dmLabor.cdsTOpera;
          enArea: Result := dmLabor.cdsArea;
          enTDefecto: Result := dmLabor.cdsTDefecto;
          enCedula: Result := dmLabor.cdsCedulaLookup;
          enUsuarios: Result := dmSistema.cdsUsuarios;
          enComponentes: Result := dmLabor.cdsComponentes;
          enMotivoScrap: Result := dmLabor.cdsMotivoScrap;       
     else

         Result := nil;
     end;

end;

{ **************** TTressShell **************** }

procedure TTressShell.FormCreate(Sender: TObject);
begin
{$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
{$endif}
     Global := TdmGlobal.Create;
     dmCliente := TdmCliente.Create( Self );
     dmConsultas := TdmConsultas.Create( Self );
     dmSistema := TdmSistema.Create( Self );
     dmCatalogos := TdmCatalogos.Create( Self );
     dmTablas := TdmTablas.Create( Self );
     dmReportes := TdmReportes.Create( Self );
     dmDiccionario := TdmDiccionario.Create( Self );
     dmLabor := TdmLabor.Create( Self );
     inherited;
     HelpContext := H00001_Labor_Introduccion;
     FHelpGlosario := 'Labor.chm';
end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
     //FFormManager.Free;
     inherited;
     dmLabor.Free;
     dmDiccionario.Free;
     dmReportes.Free;
     dmTablas.Free;
     dmCatalogos.Free;
     dmSistema.Free;
     dmConsultas.Free;
     //inherited;
     dmCliente.Free;
     Global.Free;
{$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll;
     DZetaServerProvider.FreeAll;
{$endif}
end;

procedure TTressShell.FormShow(Sender: TObject);
begin
  inherited;
  CargaTraducciones;
  StatusBar_DevEx.Panels[2].Visible:=false;
  StatusBar_DevEx.Panels[3].Visible:=false;
  StatusBar_DevEx.Panels[6].Visible:=false;

end;

procedure TTressShell.DoOpenAll;
var
   sNombre: String;
begin
     try
        inherited DoCloseAll;
        inherited DoOpenAll;
        Global.Conectar;

        dxBarLargeButton6.Caption := Format( 'Importar %s', [ Global.GetGlobalString( K_GLOBAL_LABOR_ORDENES ) ] );
        dxBarLargeButton6.Hint := Format( 'Importar %s', [ Global.GetGlobalString( K_GLOBAL_LABOR_ORDENES ) ] );
        dxBarLargeButton7.Caption := Format( 'Importar %s', [ Global.GetGlobalString( K_GLOBAL_LABOR_PARTES ) ] );
        dxBarLargeButton7.Hint := Format( 'Importar %s', [ Global.GetGlobalString( K_GLOBAL_LABOR_PARTES ) ] );

        {AV: 2010-11-16 Asigna El Global Usar Validacion Status Activo de Tablas y Cat�logos al ZetaClienteDataSet }
        ZetaClientDataSet.GlobalSoloKeysActivos := Global.GetGlobalBooleano( K_GLOBAL_USAR_VALIDACION_STATUS_TABLAS );
        ZetaClientDataSet.GlobalListaConfidencialidad := dmCliente.Confidencialidad;

        with dmCliente do
        begin
             InitActivosSistema;
             InitActivosEmpleado;
             InitArrayTPeriodo;
        end;
//        dmLabor.InitcdsSuper;
        CargaEmpleadoActivos;
        CargaFechaActivos;
        //CreaArbol( Revisa( D_LABOR ) );
        RevisaDerechos;
        _V_Empleados.Execute;
        sNombre := Global.GetGlobalString( K_GLOBAL_LABOR_ORDENES );
        with _P_ImportarOrdenes do
        begin
             Caption := Format( 'Importar %s', [ lowercase(sNombre) ] );
             Hint := Format( 'Importar %s', [ lowercase(sNombre) ] );
        end;
        sNombre := Global.GetGlobalString( K_GLOBAL_LABOR_PARTES );
        with _P_ImportarPartes do
        begin
             Caption := Format( 'Importar %s', [ lowercase(sNombre) ] );
             Hint := Format( 'Importar %s', [ lowercase(sNombre) ] );
        end;
        with _R_CedulaInspeccion do
        begin
             Caption := 'C�dula de inspecci�n';
             Hint :=  'C�dula de inspecci�n';
        end;
        with _R_CedulaScrap do
        begin
             Caption := 'C�dula de scrap';
             Hint :=  'C�dula de scrap';
        end;
        with _R_MultiLote do
        begin
             Caption := 'Multilote';
             Hint :=  'Multilote';
        end;
        with _P_AfectarLabor do
        begin
             Caption := 'Afectar labor';
             Hint :=  'Afectar labor';
        end;
        SetArbolitosLength(12);
        CreaNavBar;
        LLenaOpcionesGrupos;
     except
           on Error : Exception do
           begin
                ZetaDialogo.zError( 'Error al conectar', 'Empresa: ' + dmCliente.Compania + CR_LF + Error.Message, 0 );
                DoCloseAll;
           end;
     end;
end;

procedure TTressShell.dxBarLargeButton4Click(Sender: TObject);
begin
  inherited;
  DLabor.ShowWizard( prLabCalcularTiempos );
end;

procedure TTressShell.DoCloseAll;
begin
     CierraFormaTodas;
     if EmpresaAbierta then
     begin
          dmCliente.CierraEmpresa;
          CierraDatasets( dmDiccionario );
          CierraDatasets( dmConsultas );
          CierraDatasets( dmReportes );
          CierraDatasets( dmTablas );
          CierraDatasets( dmCatalogos );
          CierraDatasets( dmSistema );
          CierraDatasets( dmLabor );
     end;
     inherited DoCloseAll;
{$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll;
{$endif}
end;

procedure TTressShell.HabilitaControles;
begin
     inherited HabilitaControles;
    // ZetaClientTools.SetEnabled( EmpleadoPanel, EmpresaAbierta );
    // ZetaClientTools.SetEnabled( AsistenciaPanel, EmpresaAbierta );
     if not EmpresaAbierta then
     begin
          StatusBarMsg( '', K_PANEL_EMPLEADO );
          StatusBarMsg( '', K_PANEL_FECHA );

     end;
end;

procedure TTressShell.LLenaOpcionesGrupos;
var
   iIndice : Integer;

begin
     if ( dmCliente.EmpresaAbierta ) then
     begin
          // @DChavez : Cuando no tiene derecho se asigna los iconos que corresponden al navbar por el index fijo en la lista
          if( ( DevEx_NavBar.Groups.Count = 1 ) and ( DevEx_NavBar.Groups[0].Caption = 'No tiene derechos'  ) )   then
          begin
                 DevEx_NavBar.Groups[0].LargeImageIndex := 4;
                 DevEx_NavBar.Groups[0].SmallImageIndex := 4;
                 DevEx_NavBar.Groups[0].UseSmallImages := False;
          end
          else
              //recorre los grupos del navbar, le asigna su imagen por su nombre
              for iIndice := 0 to (DevEx_NavBar.Groups.Count - 1)  do
              begin
                   DevEx_NavBar.Groups[iIndice].LargeImageIndex := iIndice;
                   DevEx_NavBar.Groups[iIndice].SmallImageIndex := iIndice;
                   //para que use las imagenes grandes
                   DevEx_NavBar.Groups[iIndice].UseSmallImages := False;
              end;
     end;

end;

procedure TTressShell.RevisaDerechos;
begin
  //:Todo   Registro.Enabled := Revisa( D_LAB_REGISTRO );
     _R_MultiLote.Enabled := Revisa( D_LAB_REG_MULTILOTE );
 //:Todo    Procesos.Enabled := Revisa( D_LAB_PROCESO );
     _P_CalculoTiempos.Enabled := Revisa( D_LAB_PROC_CALC_TIEMPOS );
     _P_ImportarCedulas.Enabled := Revisa(D_LAB_PROC_IMP_CEDULAS);
     _P_ImportarOrdenes.Enabled := Revisa( D_LAB_PROC_IMP_ORDENES );
     _P_ImportarPartes.Enabled := Revisa( D_LAB_PROC_IMP_PARTES );
     _P_Depuracion.Enabled := Revisa( D_LAB_PROC_DEPURACION );
     _P_AfectarLabor.Enabled := Revisa( D_LAB_PROC_AFECTARLABOR );
     SetCedulaInspeccionEnabled;
     SetAfectarLaborEnabled;
end;

procedure TTressShell.WMWizardEnd(var Message: TMessage);
begin
     with Message do
     begin
          dmLabor.HandleProcessEnd( WParam, LParam );
     end;
end;

procedure TTressShell._E_CambiarEmpleadoExecute(Sender: TObject);
begin
     inherited;
     //PGSuperior.ActivePage := EmpleadoTab;
     //EmpleadoNumeroCB.SetFocus;
  //:Todo   VentanaEmpleados.Checked := True;
end;

procedure TTressShell._V_EmpleadosExecute(Sender: TObject);
begin
     _E_CambiarEmpleadoExecute( Sender );
end;

procedure TTressShell._V_FechaExecute(Sender: TObject);
begin
     inherited;
     //PGSuperior.ActivePage := AsistenciaTab;
     FechaZF.SetFocus;
//:Todo     VentanaAsistencia.Checked := True;
end;

procedure TTressShell.PGSuperiorChange(Sender: TObject);
begin
     inherited;
     {case PGSuperior.ActivePage.PageIndex of
          0: _V_Empleados.Execute;
          1: _V_Fecha.Execute;
     end;}
end;

procedure TTressShell._Emp_PrimeroExecute(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          if GetEmpleadoPrimero then
          begin
               EmpleadoNumeroCB.ValorEntero := Empleado;
               CambioEmpleadoActivo;
          end;
     end;
end;

procedure TTressShell._Emp_PrimeroUpdate(Sender: TObject);
begin
  inherited;
  TAction( Sender ).Enabled := EmpresaAbierta and dmCliente.EmpleadoDownEnabled;
end;

procedure TTressShell._Emp_AnteriorExecute(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          if GetEmpleadoAnterior then
          begin
               EmpleadoNumeroCB.ValorEntero := Empleado;
               CambioEmpleadoActivo;
          end;
     end;
end;

procedure TTressShell._Emp_SiguienteExecute(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          if GetEmpleadoSiguiente then
          begin
               EmpleadoNumeroCB.ValorEntero := Empleado;
               CambioEmpleadoActivo;
          end;
     end;
end;

procedure TTressShell._Emp_UltimoExecute(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          if GetEmpleadoUltimo then
          begin
               EmpleadoNumeroCB.ValorEntero := Empleado;
               CambioEmpleadoActivo;
          end;
     end;
end;

procedure TTressShell._Emp_UltimoUpdate(Sender: TObject);
begin
  inherited;
  TAction( Sender ).Enabled := EmpresaAbierta and dmCliente.EmpleadoUpEnabled;
end;

procedure TTressShell.CargaEmpleadoActivos;
begin
     with dmCliente do
     begin
          EmpleadoNumeroCB.ValorEntero := Empleado;
     end;
     RefrescaEmpleadoActivos;
end;

procedure TTressShell.RefrescaEmpleadoActivos;
var
   oColor : TColor;
   sCaption, sHint : String;
   iTipo: Integer;
   i: Integer;
   oInfoEmpleado: TInfoEmpleado;

   APngImage: TdxPNGImage;
   ABitmap: TcxAlphaBitmap;
begin
     with dmCliente do
     begin
           StatusBarMsg( 'Empleado: '+ IntToStr( Empleado ), K_PANEL_EMPLEADO );
           with GetDatosEmpleadoActivo do
           begin
                EmpleadoPrettyName.Caption := Nombre;
                GetStatusEmpleado(GetDatosEmpleadoActivo, sCaption, sHint, oColor);
                {with EmpleadoActivoPanel do
                begin
                     Font.Color := oColor;
                     Caption := sCaption;
                     Hint := sHint;
                end;}
           end;
           // en construccion
           StatusBarMsg( 'Empleado: '+ IntToStr( Empleado ), K_PANEL_EMPLEADO );
          //MuestraDatosUsuarioActivo; //DevEx(by am): Se refresca el usuario y el grupo en el Panel de valores activos
          with GetDatosEmpleadoActivo do
          begin
               //DevEx (by am): Se despliega el valor activo del empleado de distinta forma en cada vista

                EmpleadoPrettyName.Caption := Nombre;

                GetStatusEmpleado(GetDatosEmpleadoActivo, sCaption, sHint, oColor);
                    //DevEx (by am): Inicializando Bitmat y PNG
                ABitmap := TcxAlphaBitmap.CreateSize(24, 24);
                APngImage := nil;
                //sCaption := ;
                for i := Low(StatusEmpleado) to High(StatusEmpleado) do
                begin

                     if (sCaption = StatusEmpleado[i]) then
                     begin
                        try
                           //DevEx (by am): Creando el Bitmap
                           ABitmap.Clear;
                           Image24_StatusEmpleado.GetBitmap(i, ABitmap);
                           //DevEx (by am): Creando el PNG
                           APngImage := TdxPNGImage.CreateFromBitmap(ABitmap);
                           ImagenStatus_DevEx.Picture.Graphic := APngImage;
                           ImagenStatus_DevEx.Hint := sHint;
                        finally
                               APngImage.Free;
                               ABitmap.Free;
                        end;
                        Break;
                     end;
                 end;
           // fin construccion
           end;
     end;
end;

procedure TTressShell.CambioEmpleadoActivo;
begin
     RefrescaEmpleadoActivos;
     CambioValoresActivos( stEmpleado );
end;

{$ifdef MULTIPLES_ENTIDADES}
procedure TTressShell.NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
{$else}
procedure TTressShell.NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
{$endif}
begin
     dmLabor.NotifyDataChange( Entidades, Estado );
     inherited NotifyDataChange( Entidades, Estado );
end;

procedure TTressShell.FechaZFValidDate(Sender: TObject);
begin
     inherited;
     dmCliente.Fecha := FechaZF.Valor;
     RefrescaFechaActivos;
     CambioValoresActivos( stFecha );
end;

procedure TTressShell.RefrescaFechaActivos;
begin
     FechaZFDia.Caption := ZetaCommonTools.DiaSemana( FechaZF.Valor );
     StatusBarMsg( 'Fecha sistema:    '+ DateToStr( Now ) +' '+ FechaZFDia.Caption, K_PANEL_FECHA );
end;

procedure TTressShell.CargaFechaActivos;
begin
     with dmCliente do
     begin
          FechaZF.Valor := Fecha;
     end;
     RefrescaFechaActivos;
end;

procedure TTressShell.CargaTraducciones;
begin
  inherited;
  DevEx_cxLocalizer.Active := True;
  DevEx_cxLocalizer.Locale := 2058;  // Carinal para Espanol (Mexico)

end;

procedure TTressShell.SetCedulaInspeccionEnabled;
var
   lGlobalDefecto: boolean;
begin
     lGlobalDefecto := ChecaGlobalDefecto;
     _P_ImportarComponentes.Enabled := Revisa( D_LAB_PROC_IMP_COMPONENTES ) and lGlobalDefecto;
     _R_CedulaScrap.Enabled := ZAccesosMgr.CheckDerecho( D_LAB_CEDULAS_SCRAP, K_DERECHO_ALTA ) and lGlobalDefecto;
     _R_CedulaInspeccion.Enabled := ZAccesosMgr.CheckDerecho( D_LAB_CEDULAS_INSP, K_DERECHO_ALTA ) and lGlobalDefecto;
end;

procedure TTressShell.SetAfectarLaborEnabled;
begin
     _P_AfectarLabor.Enabled := Global.GetGlobalBooleano( K_GLOBAL_LABOR_VALIDA_FECHACORTE ) and Revisa( D_LAB_PROC_AFECTARLABOR );
end;

function TTressShell.ChecaGlobalDefecto: boolean;
begin
     Result := ( strLleno( Global.GetGlobalString( K_GLOBAL_LABOR_TDEFECTO ) ) );
end;

procedure TTressShell.ClientAreaPG_DevExTcxPageControlPropertiesTcxPCCustomButtonsButtons0Click(
  Sender: TObject);
begin
   _V_Cerrar.Execute;
end;

procedure TTressShell.EmpleadoBuscaBtnClick(Sender: TObject);
var
   sKey, sDescription: String;
begin
     inherited;
     if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
        EmpleadoNumeroCB.AsignaValorEntero( StrToIntDef( sKey, 0 ) );
end;

procedure TTressShell.EmpleadoNumeroCBLookUp(Sender: TObject; var lOk: Boolean);
begin
     inherited;
     lOk := dmCliente.SetEmpleadoNumero( EmpleadoNumeroCB.ValorEntero );
     if lOk then
        CambioEmpleadoActivo
     else
         ZetaDialogo.zInformation( 'Error', '! Empleado no encontrado !', 0 );
end;

function TTressShell.ProcesaDialogo(oLookUp: TZetaLookupDataset; const eEntidad: TipoEntidad;
         const sFilter: String; var sKey, sDescription: String; var lEncontrado: Boolean ): Boolean;
begin
     Result := ( eEntidad in [enEmpleado,enWorder, enCedula] );

     if Result then
     begin
          case eEntidad of
               enEmpleado: lEncontrado := ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sKey, sDescription );
               enWOrder: lEncontrado := ZetaBuscaWOrder_DevEx.BuscaWorderDialogo( '', sKey, sDescription );
               enCedula: lEncontrado := ZetaBuscaCedula_DevEx.BuscaCedulasDialogo( '', sKey, sDescription );
          end;
     end;
end;

procedure TTressShell._E_BuscarEmpleadoExecute(Sender: TObject);
var
   sKey, sDescription: String;
begin
     inherited;
     if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
        EmpleadoNumeroCB.AsignaValorEntero( StrToIntDef( sKey, 0 ) );
end;

procedure TTressShell._R_MultiLoteExecute(Sender: TObject);
begin
     inherited;
     dmLabor.EditarMultiLote;
end;

procedure TTressShell._P_CalculoTiemposExecute(Sender: TObject);
begin
     inherited;
     DLabor.ShowWizard( prLabCalcularTiempos );
end;

procedure TTressShell._P_ImportarOrdenesEecute(Sender: TObject);
begin
     inherited;
     DLabor.ShowWizard( prLabImportarOrdenes );
end;

procedure TTressShell._P_ImportarPartesExecute(Sender: TObject);
begin
     inherited;
     DLabor.ShowWizard( prLabImportarPartes );
end;

procedure TTressShell._P_ImportarCedulasExecute(Sender: TObject);
begin
     inherited;
     DLabor.ShowWizard( prLabImportarCedulas );
end;

procedure TTressShell._P_DepuracionExecute(Sender: TObject);
begin
     inherited;
     DLabor.ShowWizard( prLabDepuracion );
end;

procedure TTressShell._R_CedulaInspeccionExecute(Sender: TObject);
begin
     inherited;
     dmLabor.AgregarCedulaInspeccion;
end;

procedure TTressShell._P_AfectarLaborExecute(Sender: TObject);
begin
     inherited;
     DLabor.ShowWizard( prLabAfectarLabor );
end;

procedure TTressShell._P_ImportarComponentesExecute(Sender: TObject);
begin
     inherited;
     DLabor.ShowWizard( prLabImportarComponentes );
end;

procedure TTressShell._R_CedulaScrapExecute(Sender: TObject);
begin
     inherited;
     dmLabor.AgregarCedulaScrap;
end;

end.
