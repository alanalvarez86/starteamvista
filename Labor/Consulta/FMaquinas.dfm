inherited Maquinas: TMaquinas
  Left = 187
  Top = 278
  Caption = 'M'#225'quinas'
  ClientHeight = 256
  ClientWidth = 577
  ExplicitWidth = 577
  ExplicitHeight = 256
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 577
    ExplicitWidth = 577
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 318
      ExplicitWidth = 318
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 312
        ExplicitLeft = 232
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 577
    Height = 237
    ExplicitWidth = 577
    ExplicitHeight = 237
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object MQ_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'MQ_CODIGO'
        Options.Grouping = False
      end
      object MQ_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'MQ_NOMBRE'
        Options.Grouping = False
      end
      object MQ_MULTIP: TcxGridDBColumn
        Caption = 'M'#250'ltiple'
        DataBinding.FieldName = 'MQ_MULTIP'
        Options.Grouping = False
      end
      object MQ_TMAQUIN_TXT: TcxGridDBColumn
        Caption = 'Clase'
        DataBinding.FieldName = 'MQ_TMAQUIN_TXT'
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 496
    Top = 64
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
