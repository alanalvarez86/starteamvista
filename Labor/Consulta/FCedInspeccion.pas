unit FCedInspeccion;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, StdCtrls, ExtCtrls, Db, Mask, ZetaFecha, ZetaHora,
  ZetaKeyLookup, Grids, DBGrids, ZetaDBGrid, Buttons, ZetaCommonClasses,
  ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, TressMorado2013, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, Vcl.Menus,
  System.Actions, Vcl.ActnList, Vcl.ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons, ZetaKeyLookup_DevEx,
  cxContainer, cxGroupBox, cxRadioGroup;

type
  TFCedulaInspeccion = class(TBaseGridLectura_DevEx)
    PanelControles: TPanel;
    gbTipo: TcxRadioGroup;
    gbResultado: TcxRadioGroup;
    FiltrosFechas: TGroupBox;
    lbInicio: TLabel;
    lbFin: TLabel;
    FechaFinal: TZetaFecha;
    FechaInicial: TZetaFecha;
    lbWorder: TLabel;
    WO_NUMBER: TZetaKeyLookup_DevEx;
    AR_CODIGO: TZetaKeyLookup_DevEx;
    lbParte: TLabel;
    CI_AREA: TZetaKeyLookup_DevEx;
    lbArea: TLabel;
    lbUsuario: TLabel;
    US_CODIGO: TZetaKeyLookup_DevEx;
    Refrescar: TcxButton;
    CI_FOLIO: TcxGridDBColumn;
    CI_FECHA: TcxGridDBColumn;
    CI_HORA: TcxGridDBColumn;
    WO_NUMBERG: TcxGridDBColumn;
    AR_CODIGOG: TcxGridDBColumn;
    CI_AREAG: TcxGridDBColumn;
    NumDefectos: TcxGridDBColumn;
    CI_TAMLOTE: TcxGridDBColumn;
    CI_RESULT: TcxGridDBColumn;
    CI_TIPO: TcxGridDBColumn;
    CI_COMENTA: TcxGridDBColumn;
    US_CODIGOG: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure RefrescarClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FParametros: TZetaParams;
    procedure AsignaColumnas;
  protected
    procedure Connect;override;
    procedure Refresh;override;
    procedure Modificar;override;
    procedure Agregar;override;
    procedure Borrar;override;
    function PuedeBorrar(var sMensaje: String): Boolean;override;
  public
    { Public declarations }
  end;

var
  FCedulaInspeccion: TFCedulaInspeccion;

implementation

uses DSistema, DLabor, ZGlobalTress, ZetaLaborTools, DCliente;

{$R *.DFM}

procedure TFCedulaInspeccion.FormCreate(Sender: TObject);
begin
     inherited;
     FechaInicial.Valor := Date;
     FechaFinal.Valor := Date;
     FParametros := TZetaParams.Create;
     HelpContext := H00052_Cedulas_de_inspeccion;
     with dmLabor do
     begin
          Ar_codigo.LookupDataset := cdsPartes;
          Ci_area.LookupDataset := cdsArea;
          Wo_number.LookupDataset := cdsWOrderLookup;
     end;
     Us_codigo.LookupDataset := dmSistema.cdsUsuarios;
     dmSistema.cdsUsuarios.Conectar;
     US_CODIGO.Valor := dmCliente.Usuario;
end;

procedure TFCedulaInspeccion.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmLabor do
     begin
          cdsPartes.Conectar;
          cdsArea.Conectar;
          cdsTiempoMuerto.Conectar;
          //cdsHistorialCedInsp.Conectar;
          if cdsHistorialCedInsp.HayQueRefrescar then
          begin
               Refresh;
          end;
          DataSource.DataSet := cdsHistorialCedInsp;
     end;
end;

procedure TFCedulaInspeccion.RefrescarClick(Sender: TObject);
begin
     inherited;
     DoRefresh;
     ZetaDBGrid.SetFocus;
     DoBestFit;
end;

procedure TFCedulaInspeccion.Modificar;
begin
     with dmLabor do
     begin
          RefrescaCedulasInspeccion( cdsHistorialCedInsp.FieldByName('CI_FOLIO').AsInteger );
          cdsCedInsp.Modificar;
     end;
end;

procedure TFCedulaInspeccion.Agregar;
begin
     dmLabor.AgregarCedulaInspeccion;
     {with dmLabor do
     begin
          RefrescaCedulasInspeccion(-1);
          cdsCedInsp.Agregar;
     end;}
end;

procedure TFCedulaInspeccion.Borrar;
begin
     with dmLabor.cdsHistorialCedInsp do
     begin
          Borrar;
          Edit;
          Enviar;               
     end;
end;

procedure TFCedulaInspeccion.Refresh;
var
   dInicio, dFin: TDate;
begin

     dInicio := FechaInicial.Valor;
     dFin := FechaFinal.Valor;
     {if CIHora.Checked then
     begin
          sInicio := HoraInicial.Valor;
          sFin := HoraFinal.Valor;
     end
     else
     begin
          sInicio := VACIO;
          sFin := VACIO;
     end;}

     with FParametros do
     begin
          AddInteger( 'Tipo', gbTipo.ItemIndex - 1 );
          AddInteger( 'Resultado', gbResultado.ItemIndex - 1 );
          //AddString( 'Cedula', CE_FOLIO.Llave );
          AddString( 'Parte', AR_CODIGO.Llave );
          AddString( 'Orden', WO_NUMBER.Llave );
          AddString( 'Area', CI_AREA.Llave );
          AddDate( 'FechaInicial', dInicio );
          AddDate( 'FechaFinal', dFin );
          {AddString( 'HoraInicial', sInicio );
          AddString( 'HoraFinal', sFin );}
          AddInteger( 'Usuario', US_CODIGO.Valor );
     end;
     dmLabor.RefrescarCedulasInspeccion( FParametros );
     DoBestFit;
end;


procedure TFCedulaInspeccion.FormDestroy(Sender: TObject);
begin
     inherited;
     FParametros.Free;
end;

procedure TFCedulaInspeccion.FormShow(Sender: TObject);
begin
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
     ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
     AsignaColumnas;
     {$ifdef INTERRUPTORES}
      WO_NUMBER.WidthLlave := K_WIDTHLLAVE;
      AR_CODIGO.WidthLlave := K_WIDTHLLAVE;
      CI_AREA.WidthLlave := K_WIDTHLLAVE;
      US_CODIGO.WidthLlave := K_WIDTHLLAVE;

      WO_NUMBER.Width := K_WIDTH_LOOKUP;
      AR_CODIGO.Width := K_WIDTH_LOOKUP;
      CI_AREA.Width := K_WIDTH_LOOKUP;
      US_CODIGO.Width := K_WIDTH_LOOKUP;

      Refrescar.Left := K_LEFT_630;
     {$endif}
     DoBestFit;
end;

procedure TFCedulaInspeccion.AsignaColumnas;
begin
     with ZetaDBGridDBTableView do
     begin
          AsignaGlobal( K_GLOBAL_LABOR_ORDEN, lbWorder, WO_NUMBER );
          AsignaGlobal( K_GLOBAL_LABOR_PARTE, lbparte, AR_CODIGO );
          AsignaGlobal( K_GLOBAL_LABOR_AREA, lbArea, CI_AREA );

          AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_ORDEN, ZetaDBGridDBTableView.Columns[3]);
          AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_PARTE, ZetaDBGridDBTableView.Columns[4] );
          AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_AREA, ZetaDBGridDBTableView.Columns[5] );
     end;
end;

function TFCedulaInspeccion.PuedeBorrar( var sMensaje: String ): Boolean;
begin
     Result := dmLabor.PuedeModificarCedInspeccion;
     sMensaje := Format( K_MSG_CED_INSPECCION, ['Borrar'] );

     if Result then
     begin
          Result := inherited PuedeBorrar( sMensaje );
     end;
end;
end.
