unit FWoFija;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid,
  ZBaseConsulta, Vcl.StdCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, TressMorado2013,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, cxMaskEdit;

type
  TWoFija = class(TBaseGridLectura_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    WF_FEC_INI: TcxGridDBColumn;
    WO_NUMBER: TcxGridDBColumn;
    OP_NOMBRE: TcxGridDBColumn;
    AR_NOMBRE: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Connect;override;
    procedure Refresh;override;
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;

  end;

var
  WoFija_DevEx: TWoFija;

implementation

{$R *.DFM}

uses
     DGlobal,
     ZGlobalTress,
     ZetaLaborTools,
     ZetaCommonClasses,
     DLabor;

procedure TWoFija.FormCreate(Sender: TObject);
begin
     inherited;
     Caption := Global.GetGlobalString(K_GLOBAL_LABOR_ORDENES)+ ' Fijas';
     AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_ORDEN,ZetaDBGridDBTableView.Columns[3]);
     AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_OPERACION,ZetaDBGridDBTableView.Columns[4]);
     AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_PARTE,ZetaDBGridDBTableView.Columns[5]);
     HelpContext := H9504_Ordenes_fijas;
end;

procedure TWoFija.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
  inherited;
  CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('CB_CODIGO'), K_SIN_TIPO , '', skCount );
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
  ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
end;

procedure TWoFija.Connect;
begin
     with dmLabor do
     begin
          cdsWoFija.Conectar;
          DataSource.DataSet := cdsWoFija;
     end;
end;
procedure TWoFija.Refresh;
begin
     dmLabor.cdsWoFija.Refrescar;
end;

procedure TWoFija.Agregar;
begin
     dmLabor.cdsWoFija.Agregar;
end;

procedure TWoFija.Borrar;
begin
     dmLabor.cdsWoFija.Borrar;
end;

procedure TWoFija.Modificar;
begin
     dmLabor.cdsWoFija.Modificar;
end;

end.
