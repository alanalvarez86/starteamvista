unit FConsultaEmpleados;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, StdCtrls, Buttons,
  //:TodoTDMULTIP,
  imageenview, ieview,
  ZetaDBTextBox, Mask, DBCtrls,
  ComCtrls, Db, ExtCtrls, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, dxSkinsCore, TressMorado2013, cxButtons, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, cxControls, cxPC;

type
  TConsultaEmpleados = class(TBaseConsulta)
    PageControl1: TcxPageControl;
    TabGenerales: TcxTabSheet;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    CB_APE_PAT: TDBEdit;
    CB_APE_MAT: TDBEdit;
    CB_NOMBRES: TDBEdit;
    TU_DESCRIP: TDBEdit;
    PU_DESCRIP: TDBEdit;
    TB_ELEMENT: TDBEdit;
    CONTRATO: TDBEdit;
    CB_FEC_ING: TDBEdit;
    TabNiveles: TcxTabSheet;
    CB_NIVEL1lbl: TLabel;
    CB_NIVEL2lbl: TLabel;
    CB_NIVEL3lbl: TLabel;
    CB_NIVEL4lbl: TLabel;
    CB_NIVEL5lbl: TLabel;
    CB_NIVEL5: TZetaDBTextBox;
    CB_NIVEL4: TZetaDBTextBox;
    CB_NIVEL3: TZetaDBTextBox;
    CB_NIVEL2: TZetaDBTextBox;
    CB_NIVEL1: TZetaDBTextBox;
    CB_NIVEL6lbl: TLabel;
    CB_NIVEL6: TZetaDBTextBox;
    CB_NIVEL7lbl: TLabel;
    CB_NIVEL7: TZetaDBTextBox;
    CB_NIVEL8lbl: TLabel;
    CB_NIVEL8: TZetaDBTextBox;
    CB_NIVEL9lbl: TLabel;
    CB_NIVEL9: TZetaDBTextBox;
    ZNIVEL1: TZetaTextBox;
    ZNIVEL2: TZetaTextBox;
    ZNIVEL3: TZetaTextBox;
    ZNIVEL4: TZetaTextBox;
    ZNIVEL5: TZetaTextBox;
    ZNIVEL6: TZetaTextBox;
    ZNIVEL7: TZetaTextBox;
    ZNIVEL8: TZetaTextBox;
    ZNIVEL9: TZetaTextBox;
  //:Todo  FOTOGRAFIA: TPDBMultiImage;
    LabelArea: TLabel;
    AR_DESCRIP: TDBEdit;
    CB_NIVEL10lbl: TLabel;
    CB_NIVEL10: TZetaDBTextBox;
    ZNIVEL10: TZetaTextBox;
    CB_NIVEL11lbl: TLabel;
    CB_NIVEL11: TZetaDBTextBox;
    ZNIVEL11: TZetaTextBox;
    CB_NIVEL12lbl: TLabel;
    CB_NIVEL12: TZetaDBTextBox;
    ZNIVEL12: TZetaTextBox;
    FOTO: TImageEnView;
    FotoSwitch: TcxButton;
    procedure FotoSwitchClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
  private
    procedure SetCamposNivel;
    procedure ShowFoto(const lEnabled: Boolean);
    {$IF Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
    procedure CambiosVisuales;
    {$endif}
    { Private declarations }
  public
    { Public declarations }
    procedure Connect;override;
    procedure Refresh; override;
    function PuedeAgregar(var sMensaje: String): Boolean;override;
    function PuedeBorrar(var sMensaje: String): Boolean;override;
    function PuedeModificar(var sMensaje: String): Boolean;override;
  end;

var
  ConsultaEmpleados: TConsultaEmpleados;

implementation

uses DLabor,
     DGlobal,
     DTablas,
     DCatalogos,
     ZetaClientTools,
     ZetaDialogo,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaLaborTools,
     ZGlobalTress,
     FToolsImageEn;

{$R *.DFM}

procedure TConsultaEmpleados.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     CB_NIVEL10.DataSource := DataSource;
     CB_NIVEL11.DataSource := DataSource;
     CB_NIVEL12.DataSource := DataSource;
     CB_NIVEL10.Visible := True;
     CB_NIVEL11.Visible := True;
     CB_NIVEL12.Visible := True;
     CB_NIVEL10lbl.Visible := True;
     CB_NIVEL11lbl.Visible := True;
     CB_NIVEL12lbl.Visible := True;
     ZNIVEL10.Visible := True;
     ZNIVEL11.Visible := True;
     ZNIVEL12.Visible := True;
     {$endif}
     TipoValorActivo1 := stEmpleado;
     HelpContext := H95001_Datos_del_empleado;
     LabelArea.Caption := GetLaborLabel( K_GLOBAL_LABOR_AREA );
     SetCamposNivel;
end;

procedure TConsultaEmpleados.FormShow(Sender: TObject);
begin
  inherited;
  foto.Visible:= false;
end;

procedure TConsultaEmpleados.Connect;
begin
     with dmTablas do
     begin
          if ZNIVEL1.Visible then cdsNivel1.Conectar;
          if ZNIVEL2.Visible then cdsNivel2.Conectar;
          if ZNIVEL3.Visible then cdsNivel3.Conectar;
          if ZNIVEL4.Visible then cdsNivel4.Conectar;
          if ZNIVEL5.Visible then cdsNivel5.Conectar;
          if ZNIVEL6.Visible then cdsNivel6.Conectar;
          if ZNIVEL7.Visible then cdsNivel7.Conectar;
          if ZNIVEL8.Visible then cdsNivel8.Conectar;
          if ZNIVEL9.Visible then cdsNivel9.Conectar;
	  {$ifdef ACS}
          if ZNIVEL10.Visible then cdsNivel10.Conectar;
          if ZNIVEL11.Visible then cdsNivel11.Conectar;
          if ZNIVEL12.Visible then cdsNivel12.Conectar;
	  {$endif}
     end;
     with dmLabor do
     begin
          //ObtenerFoto := FOTOGRAFIA.Visible;
          cdsDatosEmpleado.Conectar;
          DataSource.DataSet:= cdsDatosEmpleado;
     FotoSwitch.Down := false;
     FToolsImageEn.AsignaBlobAImagen( FOTO, cdsDatosEmpleado, 'FOTOGRAFIA' );
     end;
   {$IF Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
    CambiosVisuales;
   {$endif}
end;

procedure TConsultaEmpleados.Refresh;
begin
     dmLabor.cdsDatosEmpleado.Refrescar;
end;

function TConsultaEmpleados.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No se pueden agregar empleados en m�dulo Labor';
end;

function TConsultaEmpleados.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No se pueden borrar empleados en m�dulo Labor';
end;

function TConsultaEmpleados.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No se pueden modificar empleados en m�dulo Labor';
end;

procedure TConsultaEmpleados.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     with dmLabor.cdsDatosEmpleado do
     begin
          if ZNIVEL1.Visible then ZNIVEL1.Caption:= dmTablas.cdsNivel1.GetDescripcion( FieldByName( 'CB_NIVEL1' ).AsString );
          if ZNIVEL2.Visible then ZNIVEL2.Caption:= dmTablas.cdsNivel2.GetDescripcion( FieldByName( 'CB_NIVEL2' ).AsString );
          if ZNIVEL3.Visible then ZNIVEL3.Caption:= dmTablas.cdsNivel3.GetDescripcion( FieldByName( 'CB_NIVEL3' ).AsString );
          if ZNIVEL4.Visible then ZNIVEL4.Caption:= dmTablas.cdsNivel4.GetDescripcion( FieldByName( 'CB_NIVEL4' ).AsString );
          if ZNIVEL5.Visible then ZNIVEL5.Caption:= dmTablas.cdsNivel5.GetDescripcion( FieldByName( 'CB_NIVEL5' ).AsString );
          if ZNIVEL6.Visible then ZNIVEL6.Caption:= dmTablas.cdsNivel6.GetDescripcion( FieldByName( 'CB_NIVEL6' ).AsString );
          if ZNIVEL7.Visible then ZNIVEL7.Caption:= dmTablas.cdsNivel7.GetDescripcion( FieldByName( 'CB_NIVEL7' ).AsString );
          if ZNIVEL8.Visible then ZNIVEL8.Caption:= dmTablas.cdsNivel8.GetDescripcion( FieldByName( 'CB_NIVEL8' ).AsString );
          if ZNIVEL9.Visible then ZNIVEL9.Caption:= dmTablas.cdsNivel9.GetDescripcion( FieldByName( 'CB_NIVEL9' ).AsString );
          {$ifdef ACS}
          if ZNIVEL10.Visible then ZNIVEL10.Caption:= dmTablas.cdsNivel10.GetDescripcion( FieldByName( 'CB_NIVEL10' ).AsString );
          if ZNIVEL11.Visible then ZNIVEL11.Caption:= dmTablas.cdsNivel11.GetDescripcion( FieldByName( 'CB_NIVEL11' ).AsString );
          if ZNIVEL12.Visible then ZNIVEL12.Caption:= dmTablas.cdsNivel12.GetDescripcion( FieldByName( 'CB_NIVEL12' ).AsString );
         {$endif}
     end;
end;

procedure TConsultaEmpleados.SetCamposNivel;
var
   iNiveles : Integer;
begin
     iNiveles := Global.NumNiveles;
     SetCampoNivel( 1, iNiveles, CB_NIVEL1lbl, CB_NIVEL1, ZNIVEL1 );
     SetCampoNivel( 2, iNiveles, CB_NIVEL2lbl, CB_NIVEL2, ZNIVEL2 );
     SetCampoNivel( 3, iNiveles, CB_NIVEL3lbl, CB_NIVEL3, ZNIVEL3 );
     SetCampoNivel( 4, iNiveles, CB_NIVEL4lbl, CB_NIVEL4, ZNIVEL4 );
     SetCampoNivel( 5, iNiveles, CB_NIVEL5lbl, CB_NIVEL5, ZNIVEL5 );
     SetCampoNivel( 6, iNiveles, CB_NIVEL6lbl, CB_NIVEL6, ZNIVEL6 );
     SetCampoNivel( 7, iNiveles, CB_NIVEL7lbl, CB_NIVEL7, ZNIVEL7 );
     SetCampoNivel( 8, iNiveles, CB_NIVEL8lbl, CB_NIVEL8, ZNIVEL8 );
     SetCampoNivel( 9, iNiveles, CB_NIVEL9lbl, CB_NIVEL9, ZNIVEL9 );
     {$ifdef ACS}
     SetCampoNivel( 10, iNiveles, CB_NIVEL10lbl, CB_NIVEL10, ZNIVEL10 );
     SetCampoNivel( 11, iNiveles, CB_NIVEL11lbl, CB_NIVEL11, ZNIVEL11 );
     SetCampoNivel( 12, iNiveles, CB_NIVEL12lbl, CB_NIVEL12, ZNIVEL12 );
     {$endif}
end;

procedure TConsultaEmpleados.FotoSwitchClick(Sender: TObject);
begin
     inherited;
{     with FOTOGRAFIA do
     begin                                    //:Todo
          Visible := FotoSwitch.Down;
          ShowFoto( Visible );
     end;}
     FOTO.Visible := FotoSwitch.Down;
end;

procedure TConsultaEmpleados.ShowFoto( const lEnabled: Boolean );
{var
   lFotoActivada: Boolean;}
begin

end;

{$IF Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
procedure TConsultaEmpleados.CambiosVisuales;
var
  I : Integer;
begin
     //KardexVarios_DevEx.Width:= K_WIDTH_SMALLFORM;
     for I := 0 to TabNiveles.ControlCount - 1 do
     begin
          if TabNiveles.Controls[I].ClassNameIs( 'TLabel' ) then
          begin
               TabNiveles.Controls[I].Left := K_WIDTH_SMALL_LEFT - (TabNiveles.Controls[I].Width+2);
          end;
          if TabNiveles.Controls[I].ClassNameIs( 'TZetaDbTextBox' ) then
          begin
               if ( TabNiveles.Controls[I].Visible ) then
               begin
                    TabNiveles.Controls[I].Width := K_WIDTHLLAVE;
                    TabNiveles.Controls[I].Left := K_WIDTH_SMALL_LEFT;
               end;
          end;
          if TabNiveles.Controls[I].ClassNameIs( 'TZetaTextBox' ) then
          begin
               if ( TabNiveles.Controls[I].Visible ) then
               begin
                    TabNiveles.Controls[I].Width :=  110;
                    TabNiveles.Controls[I].Left := K_WIDTH_SMALL_LEFT + K_WIDTHLLAVE + 2;
               end;
          end;
     end;
end;
{$endif}

end.

