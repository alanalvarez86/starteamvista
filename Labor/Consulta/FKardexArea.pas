unit FKardexArea;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, Vcl.StdCtrls,
  ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, TressMorado2013, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, Vcl.Menus,
  System.Actions, Vcl.ActnList, Vcl.ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TKardexArea = class(TBaseGridLectura_DevEx)
    KA_FECHA: TcxGridDBColumn;
    KA_HORA: TcxGridDBColumn;
    CB_AREA: TcxGridDBColumn;
    TB_ELEMENT: TcxGridDBColumn;
    KA_FEC_MOV: TcxGridDBColumn;
    KA_HOR_MOV: TcxGridDBColumn;
    US_CODIGO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect;override;
    procedure Agregar;override;
    procedure Borrar;override;
    procedure Modificar;override;
    procedure Refresh;override;
  public
    { Public declarations }
  end;

const
     K_FORM_CAPTION = 'Kardex de %s';
var
  KardexArea: TKardexArea;

implementation

uses dLabor, ZetaLaborTools, ZGlobalTress, ZetaCommonLists, ZetaCommonClasses;

{$R *.DFM}

procedure TKardexArea.FormCreate(Sender: TObject);
begin
     inherited;
     AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_AREA,ZetaDBGridDBTableView.Columns[2]);
     Self.Caption := Format( K_FORM_CAPTION, [ GetLaborLabel( K_GLOBAL_LABOR_AREAS ) ] );
     TipoValorActivo1 := stEmpleado;
     HelpContext := H00051_Kardex_de_areas;

     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= TRUE;
     ZetaDBGridDBTableView.DataController.Filter.AutoDataSetFilter := TRUE;
end;

procedure TKardexArea.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
  inherited;
  CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('KA_FECHA'), K_SIN_TIPO , '', skCount );
  //KA_FECHA.Width := 84;
end;

procedure TKardexArea.Connect;
begin
     with dmLabor do
     begin
          cdsKarArea.Conectar;
          DataSource.DataSet := cdsKarArea;
     end;
end;

procedure TKardexArea.Refresh;
begin
     dmLabor.cdsKarArea.Refrescar;
end;

procedure TKardexArea.Agregar;
begin
     dmLabor.cdsKarArea.Agregar;
end;

procedure TKardexArea.Borrar;
begin
     dmLabor.cdsKarArea.Borrar;
end;

procedure TKardexArea.Modificar;
begin
     dmLabor.cdsKarArea.Modificar;
end;

end.
