inherited KardexWOrder: TKardexWOrder
  Left = 219
  Top = 252
  ActiveControl = WO_NUMBER.FLlave
  Caption = 'Kardex de Orden de Trabajo'
  ClientHeight = 421
  ClientWidth = 521
  OnDestroy = FormDestroy
  ExplicitWidth = 521
  ExplicitHeight = 421
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 521
    ExplicitWidth = 521
    inherited Slider: TSplitter
      Left = 339
      ExplicitLeft = 339
    end
    inherited ValorActivo1: TPanel
      Width = 323
      ExplicitWidth = 323
      inherited textoValorActivo1: TLabel
        Width = 317
      end
    end
    inherited ValorActivo2: TPanel
      Left = 342
      Width = 179
      ExplicitLeft = 342
      ExplicitWidth = 179
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 173
        ExplicitLeft = 93
      end
    end
  end
  object PanelFiltros: TPanel [1]
    Left = 0
    Top = 19
    Width = 521
    Height = 94
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object Label1: TLabel
      Left = 93
      Top = 52
      Width = 33
      Height = 13
      Caption = 'Status:'
    end
    object lbParte: TLabel
      Left = 98
      Top = 32
      Width = 28
      Height = 13
      Alignment = taRightJustify
      Caption = 'Parte:'
    end
    object Label2: TLabel
      Left = 81
      Top = 73
      Width = 45
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cantidad:'
    end
    object WO_QTY: TZetaDBTextBox
      Left = 132
      Top = 71
      Width = 65
      Height = 17
      AutoSize = False
      Caption = 'WO_QTY'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'WO_QTY'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object WO_STATUS: TZetaDBTextBox
      Left = 132
      Top = 50
      Width = 65
      Height = 17
      AutoSize = False
      Caption = 'WO_STATUS'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'WO_STATUS'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object AR_CODIGO: TZetaDBTextBox
      Left = 132
      Top = 30
      Width = 100
      Height = 17
      AutoSize = False
      Caption = 'AR_CODIGO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'AR_CODIGO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object AR_NOMBRE: TZetaDBTextBox
      Left = 235
      Top = 30
      Width = 247
      Height = 17
      AutoSize = False
      Caption = 'AR_NOMBRE'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'AR_NOMBRE'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object lbWOrder: TLabel
      Left = 40
      Top = 10
      Width = 86
      Height = 13
      Alignment = taRightJustify
      Caption = 'Orden de Trabajo:'
    end
    object WO_NUMBER: TZetaDBKeyLookup_DevEx
      Left = 132
      Top = 6
      Width = 350
      Height = 21
      LookupDataset = dmLabor.cdsWOrderLookup
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 100
      OnValidKey = WO_NUMBERValidKey
      DataField = 'WO_NUMBER'
      DataSource = DataSource
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 113
    Width = 521
    Height = 308
    ExplicitTop = 113
    ExplicitWidth = 521
    ExplicitHeight = 308
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = dsRenglon
      object AU_FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'AU_FECHA'
        Options.Grouping = False
      end
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'Empleado'
        DataBinding.FieldName = 'CB_CODIGO'
        Options.Grouping = False
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
        Options.Grouping = False
      end
      object OP_NUMBER: TcxGridDBColumn
        Caption = 'Operaci'#243'n'
        DataBinding.FieldName = 'OP_NUMBER'
      end
      object WK_TIEMPO: TcxGridDBColumn
        Caption = 'Duraci'#243'n'
        DataBinding.FieldName = 'WK_TIEMPO'
        Options.Grouping = False
      end
      object WK_STATUS: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'WK_STATUS'
        Options.Grouping = False
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 440
    Top = 64
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
  object dsRenglon: TDataSource
    Left = 368
    Top = 64
  end
end
