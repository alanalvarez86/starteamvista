unit FKardexEmpMaq;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, DB, ExtCtrls,DLabor;

type
  TKardexEmpMaquina = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  private

    { Private declarations }
  public
    { Public declarations }
  protected
    procedure Connect;override;
    procedure Modificar; override;
    procedure Refresh; override;
    procedure Agregar;override;
    procedure Borrar;override;
  end;

var
  KardexEmpMaquina: TKardexEmpMaquina;

implementation

USES
    DGlobal,ZetaCommonClasses,ZetaCommonLists,ZGlobalTress;

{$R *.dfm}


procedure TKardexEmpMaquina.Connect;
begin
     inherited;
     with dmLabor do
     begin
          cdsKarEmpMaquinas.Conectar; 
          DataSource.DataSet := cdsKarEmpMaquinas;
     end;
end;

procedure TKardexEmpMaquina.FormCreate(Sender: TObject);
begin
     inherited;
     Caption := 'Kardex Empleado - '+Global.GetGlobalString(K_GLOBAL_LABOR_MAQUINA );
     CanLookUp := TRUE;
     HelpContext := H_LAB_KAR_EMP_MAQ;
end;

procedure TKardexEmpMaquina.Modificar;
begin
     inherited;
     dmLabor.cdsKarEmpMaquinas.Modificar;
end;

procedure TKardexEmpMaquina.Refresh;
begin
     inherited;
     dmLabor.cdsKarEmpMaquinas.Refrescar;
end;

procedure TKardexEmpMaquina.Agregar;
begin
     dmLabor.cdsKarEmpMaquinas.Agregar;
end;

procedure TKardexEmpMaquina.Borrar;
begin
     dmLabor.cdsKarEmpMaquinas.Borrar;
end;

end.
