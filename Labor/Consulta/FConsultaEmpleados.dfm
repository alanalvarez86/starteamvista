inherited ConsultaEmpleados: TConsultaEmpleados
  Left = 197
  Top = 178
  Caption = 'Datos empleado'
  ClientHeight = 282
  ClientWidth = 541
  OnShow = FormShow
  ExplicitWidth = 541
  ExplicitHeight = 282
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 541
    ExplicitWidth = 541
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 282
      ExplicitWidth = 282
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 276
        ExplicitLeft = 3
      end
    end
  end
  object PageControl1: TcxPageControl [1]
    Left = 0
    Top = 19
    Width = 337
    Height = 263
    Hint = 'Datos del Empleado'
    Align = alLeft
    TabOrder = 1
    Properties.ActivePage = TabGenerales
    Properties.CustomButtons.Buttons = <>
    Properties.TabPosition = tpBottom
    ClientRectBottom = 236
    ClientRectLeft = 2
    ClientRectRight = 335
    ClientRectTop = 2
    object TabGenerales: TcxTabSheet
      Hint = 'Datos generales'
      Caption = 'Datos generales'
      object Label2: TLabel
        Left = 21
        Top = 5
        Width = 86
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Apellido paterno:'
        FocusControl = CB_APE_PAT
      end
      object Label3: TLabel
        Left = 21
        Top = 29
        Width = 86
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Apellido materno:'
        FocusControl = CB_APE_MAT
      end
      object Label4: TLabel
        Left = 21
        Top = 53
        Width = 86
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Nombres:'
        FocusControl = CB_NOMBRES
      end
      object Label6: TLabel
        Left = 21
        Top = 77
        Width = 86
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Turno:'
      end
      object Label7: TLabel
        Left = 21
        Top = 101
        Width = 86
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Puesto:'
      end
      object Label8: TLabel
        Left = 21
        Top = 125
        Width = 86
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Clasificaci'#243'n:'
      end
      object Label9: TLabel
        Left = 21
        Top = 149
        Width = 86
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Contrato:'
      end
      object Label10: TLabel
        Left = 21
        Top = 173
        Width = 86
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Fecha de ingreso:'
      end
      object LabelArea: TLabel
        Left = 21
        Top = 197
        Width = 86
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Area:'
      end
      object CB_APE_PAT: TDBEdit
        Left = 113
        Top = 2
        Width = 175
        Height = 21
        Hint = 'Apellido Paterno del Empleado'
        DataField = 'CB_APE_PAT'
        DataSource = DataSource
        ParentShowHint = False
        ReadOnly = True
        ShowHint = False
        TabOrder = 0
      end
      object CB_APE_MAT: TDBEdit
        Left = 113
        Top = 26
        Width = 175
        Height = 21
        Hint = 'Apellido Materno del Empleado'
        DataField = 'CB_APE_MAT'
        DataSource = DataSource
        ParentShowHint = False
        ReadOnly = True
        ShowHint = False
        TabOrder = 1
      end
      object CB_NOMBRES: TDBEdit
        Left = 113
        Top = 50
        Width = 175
        Height = 21
        Hint = 'Nombre(s) del Empleado'
        DataField = 'CB_NOMBRES'
        DataSource = DataSource
        ParentShowHint = False
        ReadOnly = True
        ShowHint = False
        TabOrder = 2
      end
      object TU_DESCRIP: TDBEdit
        Left = 113
        Top = 74
        Width = 175
        Height = 21
        Hint = 'Turno del Empleado'
        DataField = 'TU_DESCRIP'
        DataSource = DataSource
        ParentShowHint = False
        ReadOnly = True
        ShowHint = False
        TabOrder = 3
      end
      object PU_DESCRIP: TDBEdit
        Left = 113
        Top = 98
        Width = 175
        Height = 21
        Hint = 'Puesto del Empleado'
        DataField = 'PU_DESCRIP'
        DataSource = DataSource
        ParentShowHint = False
        ReadOnly = True
        ShowHint = False
        TabOrder = 4
      end
      object TB_ELEMENT: TDBEdit
        Left = 113
        Top = 122
        Width = 175
        Height = 21
        Hint = 'Clasificaci'#243'n del Empleado'
        DataField = 'TB_ELEMENT'
        DataSource = DataSource
        ParentShowHint = False
        ReadOnly = True
        ShowHint = False
        TabOrder = 5
      end
      object CONTRATO: TDBEdit
        Left = 113
        Top = 146
        Width = 175
        Height = 21
        Hint = 'Tipo de Contrato'
        DataField = 'CONTRATO'
        DataSource = DataSource
        ParentShowHint = False
        ReadOnly = True
        ShowHint = False
        TabOrder = 6
      end
      object CB_FEC_ING: TDBEdit
        Left = 113
        Top = 170
        Width = 175
        Height = 21
        Hint = 'Fecha de Ingreso'
        DataField = 'CB_FEC_ING'
        DataSource = DataSource
        ParentShowHint = False
        ReadOnly = True
        ShowHint = False
        TabOrder = 7
      end
      object AR_DESCRIP: TDBEdit
        Left = 113
        Top = 194
        Width = 175
        Height = 21
        Hint = 'Area Asignada'
        DataField = 'AR_DESCRIP'
        DataSource = DataSource
        ParentShowHint = False
        ReadOnly = True
        ShowHint = False
        TabOrder = 8
      end
    end
    object TabNiveles: TcxTabSheet
      Hint = 'Niveles de Organigrama'
      Caption = 'Niveles'
      object CB_NIVEL1lbl: TLabel
        Left = 80
        Top = 7
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel1:'
      end
      object CB_NIVEL2lbl: TLabel
        Left = 80
        Top = 26
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel2:'
      end
      object CB_NIVEL3lbl: TLabel
        Left = 80
        Top = 45
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel3:'
      end
      object CB_NIVEL4lbl: TLabel
        Left = 80
        Top = 64
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel4:'
      end
      object CB_NIVEL5lbl: TLabel
        Left = 80
        Top = 84
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel5:'
      end
      object CB_NIVEL5: TZetaDBTextBox
        Left = 115
        Top = 82
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL5'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL5'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL4: TZetaDBTextBox
        Left = 115
        Top = 62
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL4'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL4'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL3: TZetaDBTextBox
        Left = 115
        Top = 43
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL3'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL3'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL2: TZetaDBTextBox
        Left = 115
        Top = 24
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL2'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL2'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL1: TZetaDBTextBox
        Left = 115
        Top = 5
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL1'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL1'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL6lbl: TLabel
        Left = 80
        Top = 103
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel6:'
      end
      object CB_NIVEL6: TZetaDBTextBox
        Left = 115
        Top = 101
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL6'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL6'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL7lbl: TLabel
        Left = 80
        Top = 122
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel7:'
      end
      object CB_NIVEL7: TZetaDBTextBox
        Left = 115
        Top = 120
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL7'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL7'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL8lbl: TLabel
        Left = 80
        Top = 142
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel8:'
      end
      object CB_NIVEL8: TZetaDBTextBox
        Left = 115
        Top = 140
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL8'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL8'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL9lbl: TLabel
        Left = 80
        Top = 161
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel9:'
      end
      object CB_NIVEL9: TZetaDBTextBox
        Left = 115
        Top = 159
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL9'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL9'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object ZNIVEL1: TZetaTextBox
        Left = 166
        Top = 5
        Width = 160
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL1'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL2: TZetaTextBox
        Left = 166
        Top = 24
        Width = 160
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL2'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL3: TZetaTextBox
        Left = 166
        Top = 44
        Width = 160
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL3'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL4: TZetaTextBox
        Left = 166
        Top = 63
        Width = 160
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL4'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL5: TZetaTextBox
        Left = 166
        Top = 83
        Width = 160
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL5'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL6: TZetaTextBox
        Left = 166
        Top = 102
        Width = 160
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL6'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL7: TZetaTextBox
        Left = 166
        Top = 121
        Width = 160
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL7'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL8: TZetaTextBox
        Left = 166
        Top = 141
        Width = 160
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL8'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL9: TZetaTextBox
        Left = 166
        Top = 160
        Width = 160
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL9'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object CB_NIVEL10lbl: TLabel
        Left = 74
        Top = 180
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel10:'
        Visible = False
      end
      object CB_NIVEL10: TZetaDBTextBox
        Left = 115
        Top = 178
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL10'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL10'
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object ZNIVEL10: TZetaTextBox
        Left = 166
        Top = 179
        Width = 160
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL10'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clSilver
        Border = False
      end
      object CB_NIVEL11lbl: TLabel
        Left = 74
        Top = 199
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel11:'
        Visible = False
      end
      object CB_NIVEL11: TZetaDBTextBox
        Left = 115
        Top = 197
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL11'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL10'
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object ZNIVEL11: TZetaTextBox
        Left = 166
        Top = 198
        Width = 160
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL11'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clSilver
        Border = False
      end
      object CB_NIVEL12lbl: TLabel
        Left = 74
        Top = 218
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel12:'
        Visible = False
      end
      object CB_NIVEL12: TZetaDBTextBox
        Left = 115
        Top = 216
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL12'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL12'
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object ZNIVEL12: TZetaTextBox
        Left = 166
        Top = 217
        Width = 160
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL12'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clSilver
        Border = False
      end
    end
  end
  object FOTO: TImageEnView [2]
    Left = 343
    Top = 95
    Width = 190
    Height = 143
    Cursor = crDefault
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    ParentCustomHint = False
    Background = clBtnFace
    ParentCtl3D = False
    LegacyBitmap = False
    AutoFit = True
    AutoStretch = True
    EnableInteractionHints = True
    PlayLoop = False
    ParentShowHint = False
    ShowHint = False
    TabOrder = 2
  end
  object FotoSwitch: TcxButton [3]
    Left = 343
    Top = 63
    Width = 100
    Height = 26
    Hint = 'Mostrar foto'
    Caption = 'Mostrar foto'
    OptionsImage.Glyph.Data = {
      36090000424D3609000000000000360000002800000018000000180000000100
      20000000000000090000000000000000000000000000000000005A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFFA2C2FFFFC1D6FFFFC1D6FFFFC1D6FFFFC1D6FFFFC1D6FFFFBED4
      FFFF6A9CFFFF679BFFFFA5C4FFFFC1D6FFFFAFCAFFFF76A5FFFF6297FFFFB9D1
      FFFFC1D6FFFFC1D6FFFFADC9FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFFFCFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA8C5
      FFFF7EAAFFFFEFF5FFFFB9D1FFFF98BBFFFFAFCAFFFFEFF5FFFF98BBFFFF8EB4
      FFFFFCFDFFFFFFFFFFFFFFFFFFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFF5FFFF6297
      FFFFEDF3FFFF83ADFFFF5A92FFFF5A92FFFF5A92FFFF6FA0FFFFF2F6FFFF74A3
      FFFFD0E0FFFFFFFFFFFFFFFFFFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC6D9FFFF8BB2
      FFFFD3E2FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFFB9D1FFFFA5C4
      FFFFA8C5FFFFFFFFFFFFFFFFFFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFADC9FFFF9BBD
      FFFFC1D6FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFFA0C0FFFFB9D1
      FFFF8BB2FFFFFFFFFFFFFFFFFFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC6D9FFFF7CA8
      FFFFE5EEFFFF5D94FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFFC9DBFFFF9BBD
      FFFFA5C4FFFFFFFFFFFFFFFFFFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEAF1FFFF5F95
      FFFFCEDFFFFFB1CCFFFF5D94FFFF5A92FFFF5A92FFFF98BBFFFFE5EEFFFF5F95
      FFFFD6E4FFFFFFFFFFFFFFFFFFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB4CD
      FFFF6A9CFFFFCEDFFFFFEDF3FFFFCBDDFFFFE3ECFFFFE0EAFFFF74A3FFFF98BB
      FFFFFFFFFFFFFFFFFFFFFFFFFFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFD
      FFFFB9D1FFFF6297FFFF76A5FFFF90B6FFFF79A7FFFF5F95FFFFA8C5FFFFFAFC
      FFFFFFFFFFFFFFFFFFFFFFFFFFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFF2F6FFFFC9DBFFFFADC9FFFFC3D8FFFFEDF3FFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFF98BBFFFFFFFFFFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFFD3E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFE3ECFFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF90B6FFFF98BBFFFF98BBFFFF6A9CFFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF96B9FFFFC1D6FFFFB1CCFFFF5D94FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92
      FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF5A92FFFF}
    OptionsImage.Margin = 1
    ParentShowHint = False
    ShowHint = True
    SpeedButtonOptions.GroupIndex = 1
    SpeedButtonOptions.AllowAllUp = True
    TabOrder = 3
    OnClick = FotoSwitchClick
  end
  inherited DataSource: TDataSource
    Left = 440
    Top = 40
  end
end
