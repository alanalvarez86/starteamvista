inherited WoFija: TWoFija
  Left = 257
  Top = 223
  Caption = 'Ordenes Fijas'
  ClientHeight = 282
  ClientWidth = 529
  ExplicitWidth = 529
  ExplicitHeight = 282
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 529
    ExplicitWidth = 529
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 270
      ExplicitWidth = 270
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 264
        ExplicitLeft = 460
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 529
    Height = 263
    ExplicitWidth = 529
    ExplicitHeight = 263
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'Empleado'
        DataBinding.FieldName = 'CB_CODIGO'
        Options.Grouping = False
        Width = 76
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
        Options.Grouping = False
      end
      object WF_FEC_INI: TcxGridDBColumn
        Caption = 'Inicio'
        DataBinding.FieldName = 'WF_FEC_INI'
        PropertiesClassName = 'TcxMaskEditProperties'
        Options.Grouping = False
      end
      object WO_NUMBER: TcxGridDBColumn
        Caption = 'Orden trabajo'
        DataBinding.FieldName = 'WO_NUMBER'
      end
      object OP_NOMBRE: TcxGridDBColumn
        Caption = 'Operaci'#243'n'
        DataBinding.FieldName = 'OP_NOMBRE'
      end
      object AR_NOMBRE: TcxGridDBColumn
        Caption = 'Producto'
        DataBinding.FieldName = 'AR_NOMBRE'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
