unit FWOrder;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Grids, DBGrids, Db, ExtCtrls, StdCtrls, Mask, DBCtrls, ComCtrls, Buttons,
     ZBaseGridLectura_DevEx,
     ZetaCommonClasses,
     ZetaDBGrid,
     ZetaDBTextBox,
     ZetaKeyCombo,
     ZetaFecha,
     ZetaKeyLookup,
     ZetaEdit,
     ZetaSmartLists, ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, TressMorado2013,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid,
  ZetaKeyLookup_DevEx;

  type
  TWOrder = class(TBaseGridLectura_DevEx)
    dsRenglon: TDataSource;
    Panel: TPanel;
    Label1: TLabel;
    lbParte: TLabel;
    Label2: TLabel;
    WO_QTY: TZetaDBTextBox;
    WO_STATUS: TZetaDBTextBox;
    AR_CODIGO: TZetaDBTextBox;
    AR_NOMBRE: TZetaDBTextBox;
    lbWOrder: TLabel;
    WO_NUMBER: TZetaDBKeyLookup_DevEx;
    ST_SEQUENC: TcxGridDBColumn;
    OP_NUMBER: TcxGridDBColumn;
    OP_NOMBRE: TcxGridDBColumn;
    ST_STD_HR: TcxGridDBColumn;
    ST_REAL_HR: TcxGridDBColumn;
    ST_QTY: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure WO_NUMBERValidKey(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FParametros: TZetaParams;
    FLlave : string;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Agregar;override;
    procedure Borrar;override;
    procedure Modificar;override;
    procedure Refresh;override;

  public
    { Public declarations }
  end;

var
  WOrder: TWOrder;

implementation

{$R *.DFM}

uses DLabor, DGlobal, ZGlobalTress, ZetaLaborTools, ZetaDialogo, ZetaBuscaWOrder, ZAccesosTress,
     ZetaCommonLists, ZetaCommonTools;

procedure TWOrder.FormCreate(Sender: TObject);
begin
     inherited;
     Caption := Global.GetGlobalString(K_GLOBAL_LABOR_ORDEN);
     lbParte.Caption := Global.GetGlobalString(K_GLOBAL_LABOR_PARTE);
     AsignaGlobal( K_GLOBAL_LABOR_ORDEN, lbWorder, WO_NUMBER );

     AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_OPERACION, ZetaDBGridDBTableView.Columns[2] );

     HelpContext := H9505_Orden_de_trabajo;
     FParametros := TZetaParams.Create;
     AR_CODIGO.Caption := '';
     AR_NOMBRE.Caption := '';
     WO_STATUS.Caption := '';
     WO_QTY.Caption := '';
     FLlave := '';

     WO_NUMBER.LookupDataset := dmLabor.cdsWOrderLookup;
end;

procedure TWOrder.FormDestroy(Sender: TObject);
begin
     FParametros.Free;
     inherited;
end;

procedure TWOrder.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
  inherited;
  CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
  ZetaDBGridDBTableView.Columns[0].width := 84;
end;

procedure TWOrder.Connect;
begin
     with dmLabor do
     begin
          DataSource.DataSet:= cdsWOrderSteps;
          cdsWOrderSteps.Tag := D_LAB_WORK_ORDER;
          dsRenglon.DataSet := cdsSteps;
     end;
     DoRefresh;
end;

procedure TWOrder.Refresh;
begin
     if FLlave <> WO_NUMBER.Llave then
     begin
          FLlave := WO_NUMBER.Llave;
          if FLlave > '' then
          begin
               with FParametros do
                    AddString( 'WO_NUMBER', FLlave );
               dmLabor.RefrescarWOrderSteps( FParametros );
               //ZetaDbGrid.SetFocus;
          end
     end;
end;

procedure TWOrder.Modificar;
begin
     dmLabor.cdsWOrderSteps.Modificar;
end;

procedure TWOrder.Agregar;
begin
     with FParametros do
          AddString( 'WO_NUMBER', '' );
     with dmLabor do
     begin
          RefrescarWOrderSteps( FParametros );
          cdsWOrderSteps.Agregar;
     end;
end;

procedure TWOrder.Borrar;
begin
     with dmLabor.cdsWOrderSteps do
     begin
          Borrar;
          Enviar;
     end;
{     with dmLabor do
          if cdsSteps.IsEmpty then
             zInformation( Caption, 'No Hay ' + Global.GetGlobalString( K_GLOBAL_LABOR_OPERACIONES ) +
                           ' Para Borrar', 0 )
          else
          begin
               cdsSteps.Borrar;
               cdsWOrderSteps.Enviar;
          end }
end;

procedure TWOrder.WO_NUMBERValidKey(Sender: TObject);
begin
     inherited;
     DoRefresh;
end;

end.


