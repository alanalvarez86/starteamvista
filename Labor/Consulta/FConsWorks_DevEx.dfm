inherited ConsWorks_DevEx: TConsWorks_DevEx
  Left = 612
  Top = 231
  Caption = 'Operaciones Registradas'
  ClientHeight = 334
  ClientWidth = 484
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 298
    Width = 484
    inherited OK_DevEx: TcxButton
      Left = 314
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 394
    end
  end
  object ValorActivo1: TPanel [1]
    Left = 0
    Top = 0
    Width = 323
    Height = 18
    Align = alLeft
    Alignment = taLeftJustify
    BorderWidth = 2
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object textoValorActivo1: TLabel
      Left = 3
      Top = 3
      Width = 317
      Height = 13
      Align = alTop
      Caption = 'textoValorActivo1'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
  end
  object ValorActivo2: TPanel [2]
    Left = 323
    Top = 0
    Width = 161
    Height = 18
    Align = alClient
    Alignment = taRightJustify
    BorderWidth = 2
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    object textoValorActivo2: TLabel
      Left = 3
      Top = 3
      Width = 155
      Height = 13
      Align = alTop
      Alignment = taRightJustify
      Caption = 'textoValorActivo2'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
  end
  object cxPageControl: TcxPageControl [3]
    Left = 0
    Top = 18
    Width = 484
    Height = 280
    Align = alBottom
    TabOrder = 1
    Properties.ActivePage = Generales_DevEx
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 278
    ClientRectLeft = 2
    ClientRectRight = 482
    ClientRectTop = 28
    object Generales_DevEx: TcxTabSheet
      Caption = 'Generales'
      ImageIndex = 0
      object WK_PIEZAS: TZetaDBTextBox
        Left = 120
        Top = 218
        Width = 49
        Height = 17
        AutoSize = False
        Caption = 'WK_PIEZAS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'WK_PIEZAS'
        DataSource = DataSource
        FormatFloat = '%2.2n'
        FormatCurrency = '%m'
      end
      object WK_PRE_CAL: TZetaDBTextBox
        Left = 120
        Top = 48
        Width = 49
        Height = 17
        AutoSize = False
        Caption = 'WK_PRE_CAL'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'WK_PRE_CAL'
        DataSource = DataSource
        FormatFloat = '%2.2n'
        FormatCurrency = '%m'
      end
      object WK_HORA_R: TZetaDBTextBox
        Left = 120
        Top = 27
        Width = 49
        Height = 17
        AutoSize = False
        Caption = 'WK_HORA_R'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'WK_HORA_R'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object lblMinutos: TLabel
        Left = 172
        Top = 52
        Width = 37
        Height = 13
        Caption = 'Minutos'
      end
      object LblTiempo: TLabel
        Left = 12
        Top = 50
        Width = 104
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tiempo PreCalculado:'
      end
      object WK_CEDULA: TZetaDBTextBox
        Left = 265
        Top = 7
        Width = 99
        Height = 17
        AutoSize = False
        Caption = 'WK_CEDULA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'WK_CEDULA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label2: TLabel
        Left = 225
        Top = 9
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#233'dula:'
      end
      object Label1: TLabel
        Left = 82
        Top = 222
        Width = 34
        Height = 13
        Alignment = taRightJustify
        Caption = 'Piezas:'
      end
      object lbTiempoMuerto: TLabel
        Left = 42
        Top = 200
        Width = 74
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tiempo Muerto:'
      end
      object WK_TIPO: TZetaDBTextBox
        Left = 120
        Top = 7
        Width = 99
        Height = 17
        AutoSize = False
        Caption = 'WK_TIPO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'WK_TIPO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label23: TLabel
        Left = 80
        Top = 178
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Puesto:'
      end
      object lbOperacion: TLabel
        Left = 64
        Top = 156
        Width = 52
        Height = 13
        Alignment = taRightJustify
        Caption = 'Operaci'#243'n:'
      end
      object Label21: TLabel
        Left = 93
        Top = 9
        Width = 24
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo:'
      end
      object Label20: TLabel
        Left = 68
        Top = 70
        Width = 49
        Height = 13
        Alignment = taRightJustify
        Caption = 'Multi-Lote:'
      end
      object lbArea: TLabel
        Left = 91
        Top = 134
        Width = 25
        Height = 13
        Alignment = taRightJustify
        Caption = 'Area:'
      end
      object lbl_hora_R: TLabel
        Left = 65
        Top = 30
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Hora Real:'
      end
      object lbWOrder: TLabel
        Left = 30
        Top = 90
        Width = 86
        Height = 13
        Alignment = taRightJustify
        Caption = 'Orden de Trabajo:'
      end
      object lbParte: TLabel
        Left = 88
        Top = 112
        Width = 28
        Height = 13
        Alignment = taRightJustify
        Caption = 'Parte:'
      end
      object WK_TMUERTO: TZetaDBKeyLookup_DevEx
        Left = 120
        Top = 196
        Width = 350
        Height = 21
        LookupDataset = dmLabor.cdsTiempoMuerto
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 100
        DataField = 'WK_TMUERTO'
        DataSource = DataSource
      end
      object CBMultiLote: TCheckBox
        Left = 120
        Top = 69
        Width = 17
        Height = 17
        Enabled = False
        TabOrder = 1
      end
      object CB_PUESTO: TZetaDBKeyLookup_DevEx
        Left = 120
        Top = 174
        Width = 350
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 100
        DataField = 'CB_PUESTO'
        DataSource = DataSource
      end
      object OP_NUMBER: TZetaDBKeyLookup_DevEx
        Left = 120
        Top = 152
        Width = 350
        Height = 21
        LookupDataset = dmLabor.cdsOpera
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 100
        DataField = 'OP_NUMBER'
        DataSource = DataSource
      end
      object CB_AREA: TZetaDBKeyLookup_DevEx
        Left = 120
        Top = 130
        Width = 350
        Height = 21
        LookupDataset = dmLabor.cdsArea
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 100
        DataField = 'CB_AREA'
        DataSource = DataSource
      end
      object AR_CODIGO: TZetaDBKeyLookup_DevEx
        Left = 120
        Top = 108
        Width = 350
        Height = 21
        LookupDataset = dmLabor.cdsPartes
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 100
        DataField = 'AR_CODIGO'
        DataSource = DataSource
      end
      object WO_NUMBER: TZetaDBKeyLookup_DevEx
        Left = 120
        Top = 86
        Width = 350
        Height = 21
        LookupDataset = dmLabor.cdsWOrderLookup
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 100
        DataField = 'WO_NUMBER'
        DataSource = DataSource
      end
    end
    object Moduladores_DevEx: TcxTabSheet
      Caption = 'Moduladores'
      ImageIndex = 1
      object Label4: TLabel
        Left = 92
        Top = 52
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Status:'
      end
      object lbModula3: TLabel
        Left = 57
        Top = 122
        Width = 69
        Height = 13
        Alignment = taRightJustify
        Caption = 'Modulador #3:'
      end
      object lbModula2: TLabel
        Left = 57
        Top = 99
        Width = 69
        Height = 13
        Alignment = taRightJustify
        Caption = 'Modulador #2:'
      end
      object lbModula1: TLabel
        Left = 57
        Top = 75
        Width = 69
        Height = 13
        Alignment = taRightJustify
        Caption = 'Modulador #1:'
      end
      object WK_STATUS: TZetaDBKeyCombo
        Left = 129
        Top = 48
        Width = 145
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 0
        ListaFija = lfStatusLectura
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'WK_STATUS'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object WK_MOD_3: TZetaDBKeyLookup_DevEx
        Left = 129
        Top = 118
        Width = 300
        Height = 21
        LookupDataset = dmLabor.cdsModula3
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        DataField = 'WK_MOD_3'
        DataSource = DataSource
      end
      object WK_MOD_2: TZetaDBKeyLookup_DevEx
        Left = 129
        Top = 95
        Width = 300
        Height = 21
        LookupDataset = dmLabor.cdsModula2
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        DataField = 'WK_MOD_2'
        DataSource = DataSource
      end
      object WK_MOD_1: TZetaDBKeyLookup_DevEx
        Left = 129
        Top = 71
        Width = 300
        Height = 21
        LookupDataset = dmLabor.cdsModula1
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        DataField = 'WK_MOD_1'
        DataSource = DataSource
      end
    end
    object Tiempos_DevEx: TcxTabSheet
      Caption = 'Tiempos y Lecturas'
      ImageIndex = 2
      object GroupBox3: TGroupBox
        Left = 68
        Top = 18
        Width = 353
        Height = 103
        Caption = ' Tiempos '
        TabOrder = 0
        object Label15: TLabel
          Left = 12
          Top = 58
          Width = 68
          Height = 13
          Alignment = taRightJustify
          Caption = 'Extras Dobles:'
        end
        object WK_TIEMPOlbl: TLabel
          Left = 35
          Top = 18
          Width = 46
          Height = 13
          Alignment = taRightJustify
          Caption = 'Duraci'#243'n:'
        end
        object Label17: TLabel
          Left = 31
          Top = 38
          Width = 50
          Height = 13
          Alignment = taRightJustify
          Caption = 'Ordinarias:'
        end
        object WK_HRS_ORD: TZetaDBTextBox
          Left = 83
          Top = 37
          Width = 113
          Height = 17
          AutoSize = False
          Caption = 'WK_HRS_ORD'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'WK_HRS_ORD'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object WK_HRS_2EX: TZetaDBTextBox
          Left = 83
          Top = 57
          Width = 113
          Height = 17
          AutoSize = False
          Caption = 'WK_HRS_2EX'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'WK_HRS_2EX'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label3: TLabel
          Left = 14
          Top = 78
          Width = 66
          Height = 13
          Alignment = taRightJustify
          Caption = 'Extras Triples:'
        end
        object WK_HRS_3EX: TZetaDBTextBox
          Left = 83
          Top = 77
          Width = 113
          Height = 17
          AutoSize = False
          Caption = 'WK_HRS_3EX'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'WK_HRS_3EX'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object WK_TIEMPO: TZetaDBTextBox
          Left = 83
          Top = 16
          Width = 113
          Height = 17
          AutoSize = False
          Caption = 'WK_TIEMPO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'WK_TIEMPO'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label5: TLabel
          Left = 198
          Top = 18
          Width = 37
          Height = 13
          Caption = 'Minutos'
        end
        object Label11: TLabel
          Left = 198
          Top = 39
          Width = 37
          Height = 13
          Caption = 'Minutos'
        end
        object Label12: TLabel
          Left = 198
          Top = 59
          Width = 37
          Height = 13
          Caption = 'Minutos'
        end
        object Label13: TLabel
          Left = 198
          Top = 79
          Width = 37
          Height = 13
          Caption = 'Minutos'
        end
      end
      object GroupBox1: TGroupBox
        Left = 68
        Top = 124
        Width = 353
        Height = 104
        Caption = ' Lecturas '
        TabOrder = 1
        object Label7: TLabel
          Left = 10
          Top = 59
          Width = 70
          Height = 13
          Alignment = taRightJustify
          Caption = 'Hora Ajustada:'
        end
        object Label8: TLabel
          Left = 48
          Top = 17
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fecha:'
        end
        object Label9: TLabel
          Left = 38
          Top = 37
          Width = 43
          Height = 13
          Alignment = taRightJustify
          Caption = 'Terminal:'
        end
        object Label10: TLabel
          Left = 42
          Top = 80
          Width = 38
          Height = 13
          Alignment = taRightJustify
          Caption = 'Manual:'
        end
        object WK_FECHA_R: TZetaDBTextBox
          Left = 83
          Top = 16
          Width = 112
          Height = 17
          AutoSize = False
          Caption = 'WK_FECHA_R'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'WK_FECHA_R'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object WK_LINX_ID: TZetaDBTextBox
          Left = 83
          Top = 36
          Width = 112
          Height = 17
          AutoSize = False
          Caption = 'WK_LINX_ID'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'WK_LINX_ID'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object WK_HORA_A: TZetaDBTextBox
          Left = 83
          Top = 57
          Width = 112
          Height = 17
          AutoSize = False
          Caption = 'WK_HORA_A'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'WK_HORA_A'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object WK_MANUAL: TZetaDBTextBox
          Left = 83
          Top = 78
          Width = 112
          Height = 17
          AutoSize = False
          Caption = 'WK_MANUAL'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'WK_MANUAL'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  object DataSource: TDataSource
    AutoEdit = False
    Left = 8
    Top = 40
  end
end
