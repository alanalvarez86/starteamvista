inherited Layouts: TLayouts
  Left = 242
  Top = 288
  Caption = 'Layouts '
  ClientHeight = 270
  ClientWidth = 634
  ExplicitWidth = 634
  ExplicitHeight = 270
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 634
    ExplicitWidth = 634
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 375
      ExplicitWidth = 375
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 369
        ExplicitLeft = 289
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 634
    Height = 251
    ParentFont = False
    ExplicitWidth = 634
    ExplicitHeight = 251
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object LY_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'LY_CODIGO'
        Options.Grouping = False
      end
      object LY_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'LY_NOMBRE'
        Options.Grouping = False
      end
      object AR_CODIGO_TXT: TcxGridDBColumn
        Caption = 'L'#237'nea'
        DataBinding.FieldName = 'AR_CODIGO_TXT'
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 496
    Top = 64
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
