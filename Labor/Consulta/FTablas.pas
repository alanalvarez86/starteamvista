unit FTablas;

interface

uses ZBaseTablasConsulta_DevEx, cxCustomData,
    Controls, ExtCtrls, Buttons, Dialogs,
     ZetaDBGrid,
     cxButtons,
     DCliente,
     ZetaCommonLists,Graphics;

type
  TTOModula1 = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOModula2 = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOModula3 = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOTiempoMuerto = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TArea = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOTDefecto = class(TTablaLookup)
   protected
     {Protected declarations}
     procedure AfterCreate; override;
   end;
  TTOMotivoScrap = class(TTablaLookup)
  protected
    procedure AfterCreate; override;
  end;
   TTMaquinas = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;

implementation

uses DLabor,
     DGlobal,
     ZGlobalTress,
     ZetaCommonClasses,


     ZBaseDlgModal_DevEx, cxGridDBTableView,
     ZBaseGridLectura_DevEx,
     Forms;

{ ****** TTOModula1 ****** }

procedure TTOModula1.AfterCreate;
var
  i:integer;
begin
     ApplyMinWidth;
     inherited AfterCreate;
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
     ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= TRUE;
     ZetaDBGridDBTableView.DataController.Filter.AutoDataSetFilter := TRUE;
     LookupDataset := dmLabor.cdsModula1;
     HelpContext := H9515_Moduladores;

     i:=0;
     while i <=  4 do begin
         //ZetaDBGridDBTableView.Columns[i].Options.Filtering:=false;
         inc(i);
     end;
end;

{ TTOModula2 }

procedure TTOModula2.AfterCreate;
var
  i:integer;
begin
     ApplyMinWidth;
     inherited AfterCreate;
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= TRUE;
     ZetaDBGridDBTableView.DataController.Filter.AutoDataSetFilter := TRUE;
     ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
     LookupDataset := dmLabor.cdsModula2;
     HelpContext := H9515_Moduladores;
     i:=0;
     while i <=  4 do begin
         //ZetaDBGridDBTableView.Columns[i].Options.Filtering:=false;
         inc(i);
     end;
end;

{ TTOModula3 }

procedure TTOModula3.AfterCreate;
var
  i:integer;
begin
     ApplyMinWidth;
     inherited AfterCreate;
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
     ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= TRUE;
     ZetaDBGridDBTableView.DataController.Filter.AutoDataSetFilter := TRUE;
     LookupDataset := dmLabor.cdsModula3;
     HelpContext := H9515_Moduladores;
     i:=0;
     while i <=  4 do begin
         //ZetaDBGridDBTableView.Columns[i].Options.Filtering:=false;
         inc(i);
     end;
end;

{ TTOTiempoMuerto }

procedure TTOTiempoMuerto.AfterCreate;
var
  i:integer;
begin
     ApplyMinWidth;
     inherited AfterCreate;
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= TRUE;
     ZetaDBGridDBTableView.DataController.Filter.AutoDataSetFilter := TRUE;
     ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
     LookupDataset := dmLabor.cdsTiempoMuerto;
     HelpContext := H9519_TiempoMuerto;
     i:=0;
     while i <=  4 do begin
         //ZetaDBGridDBTableView.Columns[i].Options.Filtering:=false;
         inc(i);
     end;
end;

{ TArea }

procedure TArea.AfterCreate;
var
  i:integer;
begin
     inherited AfterCreate;
     ApplyMinWidth;
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= TRUE;
     ZetaDBGridDBTableView.DataController.Filter.AutoDataSetFilter := TRUE;
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
     ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
     LookupDataset := dmLabor.cdsArea;
     HelpContext := H9514_Catalogo_de_Areas;
     i:=0;
     while i <=  4 do begin
         //ZetaDBGridDBTableView.Columns[i].Options.Filtering:=false;
         inc(i);
     end;
end;

procedure TTOTDefecto.AfterCreate;
var
  i:integer;
begin
     ApplyMinWidth;
     inherited AfterCreate;
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= TRUE;
     ZetaDBGridDBTableView.DataController.Filter.AutoDataSetFilter := TRUE;
     inherited AfterCreate;
     LookupDataset := dmLabor.cdsTDefecto;
     HelpContext := H00053_Clasificacion_de_Defectos;
     i:=0;
     while i <=  4 do begin
         //ZetaDBGridDBTableView.Columns[i].Options.Filtering:=false;
         inc(i);
     end;
end;
{
procedure TSuper.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmLabor.cdsSuper;
//   PENDIENTE:  HelpContext := H9514_Catalogo_de_Supervisores;
end;

function TSuper.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := FALSE;
     sMensaje := 'No Se Puede Agregar A ' + LookUpDataSet.LookUpName;
end;

function TSuper.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := FALSE;
     sMensaje := 'No Se Puede Borrar En ' + LookUpDataSet.LookUpName;
end;
}

{ TTOMotivoScrap }

procedure TTOMotivoScrap.AfterCreate;
var
  i:integer;
begin
     ApplyMinWidth;
     inherited AfterCreate;
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= TRUE;
     ZetaDBGridDBTableView.DataController.Filter.AutoDataSetFilter := TRUE;
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
     ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
     LookupDataset := dmLabor.cdsMotivoScrap;
     HelpContext := H00057_Motivos_de_scrap;
     i:=0;
     while i <=  4 do begin
         //ZetaDBGridDBTableView.Columns[i].Options.Filtering:=false;
         inc(i);
     end;
end;

{ TTOTMaquinas }

procedure TTMaquinas.AfterCreate;
var
  i:integer;
begin
     ApplyMinWidth;
     inherited AfterCreate;
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= TRUE;
     ZetaDBGridDBTableView.DataController.Filter.AutoDataSetFilter := TRUE;
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
     ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
     LookupDataset := dmLabor.cdsTMaquinas;
     HelpContext := 9522;
     i:=0;
     while i <=  4 do begin
         //ZetaDBGridDBTableView.Columns[i].Options.Filtering:=false;
         inc(i);
     end;
end;

end.
