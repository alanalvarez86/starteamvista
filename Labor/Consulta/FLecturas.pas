unit FLecturas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid;

type
  TLecturas = class(TBaseConsulta)
    Grid: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Agregar;override;
    procedure Borrar;override;
    procedure Connect;override;
    procedure Modificar;override;
    procedure Refresh;override;
  end;

var
  Lecturas: TLecturas;

implementation

uses DLabor,
     DGlobal,
     ZGlobalTress,
     ZetaLaborTools,
     ZetaCommonClasses,
     ZetaCommonLists;

{$R *.DFM}

procedure TLecturas.FormCreate(Sender: TObject);
begin
     inherited;
     AsignaGlobalColumn( K_GLOBAL_LABOR_ORDEN,Grid.Columns[1]);
     AsignaGlobalColumn( K_GLOBAL_LABOR_OPERACION,Grid.Columns[2]);
     HelpContext := H9503_Lecturas_del_empleado;
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stFecha;
end;

procedure TLecturas.Connect;
begin
     with dmLabor do
     begin
          cdsLecturas.Conectar;
          DataSource.DataSet := cdsLecturas;
     end;
end;

procedure TLecturas.Refresh;
begin
     dmLabor.cdsLecturas.Refrescar;
end;

procedure TLecturas.Agregar;
begin
     dmLabor.cdsLecturas.Agregar;
end;

procedure TLecturas.Borrar;
begin
     dmLabor.cdsLecturas.Borrar;
end;

procedure TLecturas.Modificar;
begin
     dmLabor.cdsLecturas.Modificar;
end;

end.
