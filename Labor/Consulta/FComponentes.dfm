inherited Componentes: TComponentes
  Left = 175
  Top = 205
  Caption = 'Componentes'
  ClientHeight = 266
  ClientWidth = 569
  ExplicitWidth = 569
  ExplicitHeight = 266
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 569
    ExplicitWidth = 569
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 310
      ExplicitWidth = 310
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 304
        ExplicitLeft = 224
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 569
    Height = 247
    ExplicitWidth = 569
    ExplicitHeight = 247
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CN_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CN_CODIGO'
      end
      object CN_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'CN_NOMBRE'
      end
      object CN_COSTO: TcxGridDBColumn
        Caption = 'Costo'
        DataBinding.FieldName = 'CN_COSTO'
      end
      object CN_UNIDAD: TcxGridDBColumn
        Caption = 'Unidad'
        DataBinding.FieldName = 'CN_UNIDAD'
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 32
    Top = 120
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
