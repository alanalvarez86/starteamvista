inherited Works_DevEx: TWorks_DevEx
  Left = 266
  Top = 394
  Caption = 'Operaciones Registradas'
  ClientHeight = 289
  ClientWidth = 731
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 731
    inherited ValorActivo2: TPanel
      Width = 472
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 731
    Height = 82
    Align = alTop
    TabOrder = 2
    object Label1: TLabel
      Left = 27
      Top = 7
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha:'
    end
    object AU_FECHA: TZetaDBTextBox
      Left = 64
      Top = 5
      Width = 75
      Height = 17
      AutoSize = False
      Caption = 'AU_FECHA'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'AU_FECHA'
      DataSource = dsAusencia
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object AU_DIA_SEM: TZetaDBTextBox
      Left = 141
      Top = 5
      Width = 88
      Height = 17
      AutoSize = False
      Caption = 'AU_DIA_SEM'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'AU_DIA_SEM'
      DataSource = dsAusencia
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object LblDia: TLabel
      Left = 39
      Top = 25
      Width = 21
      Height = 13
      Alignment = taRightJustify
      Caption = 'D'#237'a:'
    end
    object AU_STATUS: TZetaDBTextBox
      Left = 64
      Top = 23
      Width = 165
      Height = 17
      AutoSize = False
      Caption = 'AU_STATUS'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'AU_STATUS'
      DataSource = dsAusencia
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object AU_TIPODIA: TZetaDBTextBox
      Left = 64
      Top = 41
      Width = 165
      Height = 17
      AutoSize = False
      Caption = 'AU_TIPODIA'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'AU_TIPODIA'
      DataSource = dsAusencia
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label4: TLabel
      Left = 36
      Top = 43
      Width = 24
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo:'
    end
    object HO_DESCRIP: TZetaDBTextBox
      Left = 132
      Top = 59
      Width = 180
      Height = 17
      AutoSize = False
      Caption = 'HO_DESCRIP'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'HO_DESCRIP'
      DataSource = dsAusencia
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object HO_CODIGO: TZetaDBTextBox
      Left = 64
      Top = 59
      Width = 60
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'HO_CODIGO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'HO_CODIGO'
      DataSource = dsAusencia
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label3: TLabel
      Left = 23
      Top = 61
      Width = 37
      Height = 13
      Caption = 'Horario:'
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 101
    Width = 731
    Height = 188
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object WK_HORA_R: TcxGridDBColumn
        Caption = 'H. Real'
        DataBinding.FieldName = 'WK_HORA_R'
        MinWidth = 80
      end
      object WK_HORA_A: TcxGridDBColumn
        Caption = 'H. Ajus.'
        DataBinding.FieldName = 'WK_HORA_A'
        MinWidth = 80
      end
      object WK_TIEMPO: TcxGridDBColumn
        Caption = 'Duraci'#243'n'
        DataBinding.FieldName = 'WK_TIEMPO'
        MinWidth = 80
      end
      object WK_TIPO: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'WK_TIPO'
        MinWidth = 70
      end
      object WK_CEDULA: TcxGridDBColumn
        Caption = 'C'#233'dula'
        DataBinding.FieldName = 'WK_CEDULA'
        MinWidth = 70
      end
      object WO_NUMBER: TcxGridDBColumn
        Caption = 'Orden'
        DataBinding.FieldName = 'WO_NUMBER'
        MinWidth = 70
      end
      object OP_NOMBRE: TcxGridDBColumn
        Caption = 'Operaci'#243'n'
        DataBinding.FieldName = 'OP_NOMBRE'
        MinWidth = 85
      end
      object AR_NOMBRE: TcxGridDBColumn
        Caption = 'Parte'
        DataBinding.FieldName = 'AR_NOMBRE'
        MinWidth = 80
      end
      object CB_AREA: TcxGridDBColumn
        Caption = 'Area'
        DataBinding.FieldName = 'CB_AREA'
        MinWidth = 70
      end
      object WK_TMUERTO: TcxGridDBColumn
        Caption = 'T. Muerto'
        DataBinding.FieldName = 'WK_TMUERTO'
        MinWidth = 80
      end
      object WK_MANUAL: TcxGridDBColumn
        Caption = 'Manual'
        DataBinding.FieldName = 'WK_MANUAL'
        MinWidth = 75
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 16
    Top = 144
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
  object dsAusencia: TDataSource
    OnDataChange = DataSourceDataChange
    Left = 48
    Top = 144
  end
end
