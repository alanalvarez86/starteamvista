unit FBreaks;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient, ExtCtrls, Grids, DBGrids,
     ZBaseTablasConsulta_DevEx, ZetaDBGrid, Vcl.StdCtrls, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  TressMorado2013, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList,
  Vcl.ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TBreaks = class(TTablaOpcion_DevEx)
    BR_CODIGO: TcxGridDBColumn;
    BR_NOMBRE: TcxGridDBColumn;
    BR_INGLES: TcxGridDBColumn;
    BR_NUMERO: TcxGridDBColumn;
    BR_TEXTO: TcxGridDBColumn;
    procedure AfterCreate; override;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
    procedure DoLookup;override;
  end;

var
  Breaks: TBreaks;

implementation

uses dLabor, ZetaCommonClasses, ZetaBuscador_DevEx;

{$R *.DFM}

procedure TBreaks.AfterCreate;
begin
     ApplyMinWidth;
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
     ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
     //ZetaDBGridDBTableView.Columns[0].Width:=84;
     inherited AfterCreate;
     ZetaDataset := dmLabor.cdsBreaks;
     HelpContext := H9518_Catalogo_Breaks;
end;

procedure TBreaks.DoLookup;
begin
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Caption, 'BR_CODIGO', ZetaDataset );
end;

procedure TBreaks.FormShow(Sender: TObject);
begin
  inherited;
  ApplyMinWidth;
  CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[5],0,'', skCount );
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
  ZetaDBGridDBTableView.Columns[5].Width:=84;

end;

end.
