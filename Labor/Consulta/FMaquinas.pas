unit FMaquinas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseGridLectura_DevEx, Grids, DBGrids, ZetaDBGrid, DB, ExtCtrls,DLabor,
  ZBaseConsulta, Vcl.StdCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, TressMorado2013,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TMaquinas = class(TBaseGridLectura_DevEx)
    MQ_CODIGO: TcxGridDBColumn;
    MQ_NOMBRE: TcxGridDBColumn;
    MQ_MULTIP: TcxGridDBColumn;
    MQ_TMAQUIN_TXT: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private

    { Private declarations }
  public
    { Public declarations }
  protected
    procedure Connect;override;
    procedure Modificar; override;
    procedure Refresh; override;
    procedure Agregar;override;
    procedure Borrar;override;
    procedure DoLookup;override;
  end;

var
  Maquinas: TMaquinas;

implementation

USES
    DGlobal,
    ZetaCommonClasses,
    ZetaCommonLists,
    ZGlobalTress,
    ZetaBuscador_DevEx;

{$R *.dfm}


procedure TMaquinas.Connect;
begin
     inherited;
     with dmLabor do
     begin
          cdsMaquinas.Conectar;
          DataSource.DataSet := cdsMaquinas;
     end;
end;

procedure TMaquinas.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', 'Maquinas', 'MQ_CODIGO', dmLabor.cdsMaquinas );
end;

procedure TMaquinas.FormCreate(Sender: TObject);
begin
     inherited;
     Caption := Global.GetGlobalString(K_GLOBAL_LABOR_MAQUINAS );
     CanLookUp := TRUE;
     HelpContext := H_LAB_CAT_MAQUINAS;
end;

procedure TMaquinas.FormShow(Sender: TObject);
begin
   ApplyMinWidth;
   inherited;
   CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
   ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
   ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
  // ZetaDBGridDBTableView.Columns[0].Width:=84;
end;

procedure TMaquinas.Modificar;
begin
     inherited;
     dmLabor.cdsMaquinas.Modificar;
end;

procedure TMaquinas.Refresh;
begin
     inherited;
     dmLabor.cdsMaquinas.Refrescar;
end;

procedure TMaquinas.Agregar;
begin
     dmLabor.cdsMaquinas.Agregar;
end;

procedure TMaquinas.Borrar;
begin
     dmLabor.cdsMaquinas.Borrar;
end;

end.
