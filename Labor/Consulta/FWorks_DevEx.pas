unit FWorks_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  {ZBaseConsulta,} ZBaseGridLectura_DevEx, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, ZetaDBTextBox,
  {$ifndef VER130}
  Variants,
  {$endif}  
  StdCtrls, ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  TressMorado2013, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Menus, ActnList, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TWorks_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    AU_FECHA: TZetaDBTextBox;
    AU_DIA_SEM: TZetaDBTextBox;
    LblDia: TLabel;
    AU_STATUS: TZetaDBTextBox;
    AU_TIPODIA: TZetaDBTextBox;
    Label4: TLabel;
    HO_DESCRIP: TZetaDBTextBox;
    HO_CODIGO: TZetaDBTextBox;
    Label3: TLabel;
    dsAusencia: TDataSource;
    WK_HORA_R: TcxGridDBColumn;
    WK_HORA_A: TcxGridDBColumn;
    WK_TIEMPO: TcxGridDBColumn;
    WK_TIPO: TcxGridDBColumn;
    WK_CEDULA: TcxGridDBColumn;
    WO_NUMBER: TcxGridDBColumn;
    OP_NOMBRE: TcxGridDBColumn;
    AR_NOMBRE: TcxGridDBColumn;
    CB_AREA: TcxGridDBColumn;
    WK_TMUERTO: TcxGridDBColumn;
    WK_MANUAL: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure AsignaColumnas;
  public
    { Public declarations }
    procedure Connect;override;
    procedure Agregar;override;
    procedure Borrar;override;
    procedure Modificar;override;
    procedure Refresh;override;
    function ValoresGrid: Variant;override;
  end;

var
  Works_DevEx: TWorks_DevEx;

implementation

uses DLabor,
     DGlobal,
     ZGlobalTress,
     ZetaLaborTools,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaCommonLists, DCliente;

{$R *.DFM}

procedure TWorks_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H9502_Operaciones_del_empleado;
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stFecha;
end;

procedure TWorks_DevEx.FormShow(Sender: TObject);
begin
     AsignaColumnas;
     ApplyMinWidth;
     inherited;
end;

procedure TWorks_DevEx.Connect;
begin
     with dmLabor do
     begin
          cdsWorks.Conectar;
          DataSource.DataSet := cdsWorks;
          dsAusencia.DataSet := cdsAusencia;
     end;
end;

procedure TWorks_DevEx.Refresh;
begin
     dmLabor.cdsWorks.Refrescar;
     DoBestFit;
end;

procedure TWorks_DevEx.Agregar;
begin
     dmLabor.cdsWorks.Agregar;
end;

procedure TWorks_DevEx.Borrar;
begin
     dmLabor.cdsWorks.Borrar;
     DoBestFit;
end;

procedure TWorks_DevEx.Modificar;
begin
     dmLabor.cdsWorks.Modificar;
end;

procedure TWorks_DevEx.AsignaColumnas;
begin
     {with Grid do
     begin
          AsignaGlobalColumn( K_GLOBAL_LABOR_ORDEN, Columns[ GetFieldColumnIndex( 'WO_NUMBER' ) ] );
          AsignaGlobalColumn( K_GLOBAL_LABOR_OPERACION, Columns[ GetFieldColumnIndex( 'OP_NOMBRE' ) ] );
          AsignaGlobalColumn( K_GLOBAL_LABOR_PARTE, Columns[ GetFieldColumnIndex( 'AR_NOMBRE' ) ] );
          AsignaGlobalColumn( K_GLOBAL_LABOR_AREA, Columns[ GetFieldColumnIndex( 'CB_AREA' ) ] );
     end;
     }
     with ZetaDBGridDBTableView do
     begin
           AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_ORDEN, GetColumnByFieldName('WO_NUMBER'));
           AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_OPERACION, GetColumnByFieldName('OP_NOMBRE'));
           AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_PARTE, GetColumnByFieldName('AR_NOMBRE'));
           AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_AREA, GetColumnByFieldName('CB_AREA'));
     end;
end;

function TWorks_DevEx.ValoresGrid : Variant;
begin
     Result := VarArrayOf( [ dmCliente.GetValorActivoStr( stEmpleado ),
                             dmCliente.GetValorActivoStr( stFecha ),
                             stEmpleado,stFecha] );
end;

end.
