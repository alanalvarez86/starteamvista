unit FPartes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid,
  ZBaseConsulta, Vcl.StdCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, TressMorado2013,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TPartes = class(TBaseGridLectura_DevEx)
    AR_CODIGO: TcxGridDBColumn;
    AR_NOMBRE: TcxGridDBColumn;
    TT_DESCRIP: TcxGridDBColumn;
    AR_STD_HR: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Connect;override;
    procedure Agregar;override;
    procedure Borrar;override;
    procedure Modificar;override;
    procedure Refresh;override;
    procedure DoLookup;override;
  end;

var
  Partes: TPartes;

implementation
uses DLabor,
     DGlobal,
     ZGlobalTress,
     ZetaCommonClasses,
     ZetaBuscador_DevEx;
{$R *.DFM}

procedure TPartes.FormCreate(Sender: TObject);
begin
     inherited;
     Caption := Global.GetGlobalString(K_GLOBAL_LABOR_PARTES);
     CanLookUp := TRUE;
     HelpContext := H9510_Partes;
end;

procedure TPartes.FormShow(Sender: TObject);
begin
  inherited;
  ApplyMinWidth;
  CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
  ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
end;

procedure TPartes.Connect;
begin
     with dmLabor do
     begin
          cdsTParte.Conectar;
          cdsPartes.Conectar;
          DataSource.DataSet := cdsPartes;
     end;
end;

procedure TPartes.Refresh;
begin
     dmLabor.cdsPartes.Refrescar;
end;

procedure TPartes.Agregar;
begin
     dmLabor.cdsPartes.Agregar;
end;

procedure TPartes.Borrar;
begin
     dmLabor.cdsPartes.Borrar;
end;

procedure TPartes.Modificar;
begin
     dmLabor.cdsPartes.Modificar;
end;

procedure TPartes.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', 'Partes', 'AR_CODIGO', dmLabor.cdsPartes );
end;
end.
