inherited Operaciones: TOperaciones
  Caption = 'Operaciones'
  ClientHeight = 282
  ClientWidth = 595
  ExplicitWidth = 595
  ExplicitHeight = 282
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 595
    ExplicitWidth = 595
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 336
      ExplicitWidth = 336
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 330
        ExplicitLeft = 250
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 595
    Height = 263
    ExplicitWidth = 595
    ExplicitHeight = 263
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object OP_NUMBER: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'OP_NUMBER'
        Options.Grouping = False
      end
      object OP_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'OP_NOMBRE'
        Options.Grouping = False
      end
      object TO_CODIGO: TcxGridDBColumn
        Caption = 'Clasificaci'#243'n'
        DataBinding.FieldName = 'TO_CODIGO'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
