inherited Lecturas: TLecturas
  Left = 132
  Top = 109
  Caption = 'Lecturas'
  PixelsPerInch = 96
  TextHeight = 13
  object Grid: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 429
    Height = 254
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'LX_HORA'
        Title.Caption = 'Hora'
        Width = 57
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LX_WORDER'
        Title.Caption = 'Orden'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'OP_NOMBRE'
        Title.Caption = 'Operación'
        Width = 151
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'LX_PIEZAS'
        Title.Caption = 'Piezas'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LX_LINX_ID'
        Title.Caption = 'Terminal'
        Width = 52
        Visible = True
      end>
  end
  inherited DataSource: TDataSource
    Left = 344
    Top = 72
  end
end
