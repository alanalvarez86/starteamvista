unit FOperaciones;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  TressMorado2013, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList,
  Vcl.ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid,
  Vcl.StdCtrls;

type
  TOperaciones = class(TBaseGridLectura_DevEx)
    OP_NUMBER: TcxGridDBColumn;
    OP_NOMBRE: TcxGridDBColumn;
    TO_CODIGO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Connect;override;
    procedure Agregar;override;
    procedure Borrar;override;
    procedure Modificar;override;
    procedure Refresh;override;
    procedure DoLookup;override;
  end;

var
  Operaciones: TOperaciones;

implementation
uses DLabor,
     DGlobal,
     ZGlobalTress,
     ZetaCommonClasses,
     ZetaBuscador_DevEx;
{$R *.DFM}

procedure TOperaciones.FormCreate(Sender: TObject);
begin
     inherited;
     Caption := Global.GetGlobalString(K_GLOBAL_LABOR_OPERACIONES);
     CanLookUp := TRUE;
     HelpContext := H9511_Operaciones;

end;

procedure TOperaciones.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
  inherited;
  CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('OP_NUMBER'), K_SIN_TIPO , '', skCount );
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
  ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
  //OP_NUMBER.Width := 84;
end;

procedure TOperaciones.Connect;
begin
     with dmLabor do
     begin
          cdsOpera.Conectar;
          DataSource.DataSet := cdsOpera;
     end;
end;

procedure TOperaciones.Refresh;
begin
     dmLabor.cdsOpera.Refrescar;
end;

procedure TOperaciones.Agregar;
begin
     dmLabor.cdsOpera.Agregar;
end;

procedure TOperaciones.Borrar;
begin
     dmLabor.cdsOpera.Borrar;
end;

procedure TOperaciones.Modificar;
begin
     dmLabor.cdsOpera.Modificar;
end;

procedure TOperaciones.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', 'Operaciones', 'OP_NUMBER', dmLabor.cdsOpera );
end;

end.
