unit FCedScrap;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, StdCtrls, ExtCtrls, Db, Mask, ZetaFecha, ZetaHora,
  ZetaKeyLookup, Grids, DBGrids, ZetaDBGrid, Buttons, ZetaCommonClasses,
  ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, TressMorado2013, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, Vcl.Menus,
  System.Actions, Vcl.ActnList, Vcl.ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, ZetaCXGrid, cxButtons, ZetaKeyLookup_DevEx;

type
  TCedulaScrap = class(TBaseGridLectura_DevEx)
    PanelControles: TPanel;
    FiltrosFechas: TGroupBox;
    lbInicio: TLabel;
    lbFin: TLabel;
    FechaFinal: TZetaFecha;
    FechaInicial: TZetaFecha;
    lbWorder: TLabel;
    WO_NUMBER: TZetaKeyLookup_DevEx;
    AR_CODIGO: TZetaKeyLookup_DevEx;
    lbParte: TLabel;
    CS_AREA: TZetaKeyLookup_DevEx;
    lbArea: TLabel;
    lbUsuario: TLabel;
    US_CODIGO: TZetaKeyLookup_DevEx;
    Refrescar: TcxButton;
    OP_NUMBER: TZetaKeyLookup_DevEx;
    lbOpera: TLabel;
    CS_FOLIO: TcxGridDBColumn;
    CS_FECHA: TcxGridDBColumn;
    CS_FEC_FAB: TcxGridDBColumn;
    CS_HORA: TcxGridDBColumn;
    WO_NUMBERG: TcxGridDBColumn;
    CS_AREAG: TcxGridDBColumn;
    NumScrap: TcxGridDBColumn;
    CS_TAMLOTE: TcxGridDBColumn;
    AR_CODIGOG: TcxGridDBColumn;
    CS_COMENTA: TcxGridDBColumn;
    US_CODIGOG: TcxGridDBColumn;
    OP_NUMBERG: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure RefrescarClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FParametros: TZetaParams;
    procedure AsignaColumnas;
  protected
    procedure Connect;override;
    procedure Refresh;override;
    procedure Modificar;override;
    procedure Agregar;override;
    procedure Borrar;override;
    function PuedeBorrar(var sMensaje: String): Boolean;override;
  public
    { Public declarations }
  end;

var
  CedulaScrap: TCedulaScrap;

implementation

uses DSistema, DLabor, ZGlobalTress, ZetaLaborTools, DCliente;

{$R *.DFM}

procedure TCedulaScrap.FormCreate(Sender: TObject);
begin
     inherited;
     FechaInicial.Valor := Date;
     FechaFinal.Valor := Date;
     FParametros := TZetaParams.Create;

     with dmLabor do
     begin
          Ar_codigo.LookupDataset := cdsPartes;
          Cs_area.LookupDataset := cdsArea;
          Wo_number.LookupDataset := cdsWOrderLookup;
          Op_number.LookupDataset := cdsOpera;
     end;
     Us_codigo.LookupDataset := dmSistema.cdsUsuarios;
end;

procedure TCedulaScrap.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmLabor do
     begin
          cdsPartes.Conectar;
          cdsArea.Conectar;
          cdsOpera.Conectar;
          if cdsHistorialCedScrap.HayQueRefrescar then
          begin
               Refresh;
          end;
          DataSource.DataSet := cdsHistorialCedScrap;
     end;
end;

procedure TCedulaScrap.RefrescarClick(Sender: TObject);
begin
     inherited;
     DoRefresh;
     ZetaDBGrid.SetFocus;
     DoBestFit;
end;

procedure TCedulaScrap.Modificar;
begin
     with dmLabor do
     begin
          RefrescaCedulasScrap( cdsHistorialCedScrap.FieldByName('CS_FOLIO').AsInteger );
          cdsCedScrap.Modificar;
     end;
end;

procedure TCedulaScrap.Agregar;
begin
     dmLabor.AgregarCedulaScrap;
end;

procedure TCedulaScrap.Borrar;
begin
     with dmLabor.cdsHistorialCedScrap do
     begin
          Borrar;
          Edit;
          Enviar;
     end;
end;

procedure TCedulaScrap.Refresh;
var
   dInicio, dFin: TDate;
begin

     dInicio := FechaInicial.Valor;
     dFin := FechaFinal.Valor;
     with FParametros do
     begin
          AddString( 'Parte', AR_CODIGO.Llave );
          AddString( 'Operacion', OP_NUMBER.Llave );
          AddString( 'Orden', WO_NUMBER.Llave );
          AddString( 'Area', CS_AREA.Llave );
          AddDate( 'FechaInicial', dInicio );
          AddDate( 'FechaFinal', dFin );
          AddInteger( 'Usuario', US_CODIGO.Valor );
     end;
     dmLabor.RefrescarCedulasScrap( FParametros );
     DoBestFit;
end;


procedure TCedulaScrap.FormDestroy(Sender: TObject);
begin
     inherited;
     FParametros.Free;
end;

procedure TCedulaScrap.FormShow(Sender: TObject);
begin
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
     ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
     AsignaColumnas;
     {$ifdef INTERRUPTORES}
      WO_NUMBER.WidthLlave:=K_WIDTHLLAVE;
      AR_CODIGO.WidthLlave:=K_WIDTHLLAVE;
      OP_NUMBER.WidthLlave:=K_WIDTHLLAVE;
      CS_AREA.WidthLlave:=K_WIDTHLLAVE;
      US_CODIGO.WidthLlave:=K_WIDTHLLAVE;

      WO_NUMBER.Width := K_WIDTH_LOOKUP;
      AR_CODIGO.Width := K_WIDTH_LOOKUP;
      OP_NUMBER.Width := K_WIDTH_LOOKUP;
      CS_AREA.Width := K_WIDTH_LOOKUP;
      US_CODIGO.Width := K_WIDTH_LOOKUP;

      Refrescar.Left := K_LEFT_630;
     {$endif}


     DoBestFit;
     HelpContext := H00055_Cedulas_de_scrap;
end;

procedure TCedulaScrap.AsignaColumnas;
begin
     with ZetaDBGridDBTableView do
     begin
          AsignaGlobal( K_GLOBAL_LABOR_ORDEN, lbWorder, WO_NUMBER );
          AsignaGlobal( K_GLOBAL_LABOR_PARTE, lbparte, AR_CODIGO );
          AsignaGlobal( K_GLOBAL_LABOR_AREA, lbArea, CS_AREA );
          AsignaGlobal( K_GLOBAL_LABOR_OPERACION, lbOpera, OP_NUMBER );

          AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_ORDEN, ZetaDBGridDBTableView.Columns[4] );
          AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_PARTE, ZetaDBGridDBTableView.Columns[5] );
          AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_AREA, ZetaDBGridDBTableView.Columns[7]);
          AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_OPERACION, ZetaDBGridDBTableView.Columns[6]);
     end;
end;

function TCedulaScrap.PuedeBorrar( var sMensaje: String ): Boolean;
begin
     Result := dmLabor.PuedeModificarCedScrap;
     sMensaje := Format( K_MSG_CED_INSPECCION, ['Borrar'] );

     if Result then
     begin
          Result := inherited PuedeBorrar( sMensaje );
     end;
end;
end.
