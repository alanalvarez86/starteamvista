inherited Partes: TPartes
  Left = 267
  Top = 209
  Caption = 'Partes'
  ClientHeight = 212
  ClientWidth = 585
  ExplicitWidth = 585
  ExplicitHeight = 212
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 585
    ExplicitWidth = 585
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 326
      ExplicitWidth = 326
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 320
        ExplicitLeft = 516
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 585
    Height = 193
    ExplicitWidth = 585
    ExplicitHeight = 193
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object AR_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'AR_CODIGO'
        Width = 84
      end
      object AR_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'AR_NOMBRE'
      end
      object TT_DESCRIP: TcxGridDBColumn
        Caption = 'Clasificaci'#243'n'
        DataBinding.FieldName = 'TT_DESCRIP'
      end
      object AR_STD_HR: TcxGridDBColumn
        Caption = 'Tiempo std.'
        DataBinding.FieldName = 'AR_STD_HR'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
