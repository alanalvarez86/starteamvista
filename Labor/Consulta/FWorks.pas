unit FWorks;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, ZetaDBTextBox,
  {$ifndef VER130}
  Variants,
  {$endif}
  StdCtrls, ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, TressMorado2013,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TWorks = class(TBaseGridLectura_DevEx)
    Grid: TZetaDBGrid;
    Panel1: TPanel;
    Label1: TLabel;
    AU_FECHA: TZetaDBTextBox;
    AU_DIA_SEM: TZetaDBTextBox;
    LblDia: TLabel;
    AU_STATUS: TZetaDBTextBox;
    AU_TIPODIA: TZetaDBTextBox;
    Label4: TLabel;
    HO_DESCRIP: TZetaDBTextBox;
    HO_CODIGO: TZetaDBTextBox;
    Label3: TLabel;
    dsAusencia: TDataSource;
    WK_HORA_R: TcxGridDBColumn;
    WK_HORA_A: TcxGridDBColumn;
    WK_TIEMPO: TcxGridDBColumn;
    WK_TIPO: TcxGridDBColumn;
    WK_CEDULA: TcxGridDBColumn;
    WO_NUMBER: TcxGridDBColumn;
    OP_NOMBRE: TcxGridDBColumn;
    AR_NOMBRE: TcxGridDBColumn;
    CB_AREA: TcxGridDBColumn;
    WK_TMUERTO: TcxGridDBColumn;
    WK_MANUAL: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure AsignaColumnas;
  public
    { Public declarations }
    procedure Connect;override;
    procedure Agregar;override;
    procedure Borrar;override;
    procedure Modificar;override;
    procedure Refresh;override;
    function ValoresGrid: Variant;override;
  end;

var
  Works: TWorks;

implementation

uses DLabor,
     DGlobal,
     ZGlobalTress,
     ZetaLaborTools,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaCommonLists, DCliente;

{$R *.DFM}

procedure TWorks.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H9502_Operaciones_del_empleado;
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stFecha;
end;

procedure TWorks.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     AsignaColumnas;
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
end;

procedure TWorks.Connect;
begin
     with dmLabor do
     begin
          cdsWorks.Conectar;
          DataSource.DataSet := cdsWorks;
          dsAusencia.DataSet := cdsAusencia;
     end;
end;

procedure TWorks.Refresh;
begin
     dmLabor.cdsWorks.Refrescar;
     ApplyMinWidth;
end;

procedure TWorks.Agregar;
begin
     dmLabor.cdsWorks.Agregar;
end;

procedure TWorks.Borrar;
begin
     dmLabor.cdsWorks.Borrar;
end;

procedure TWorks.Modificar;
begin
     dmLabor.cdsWorks.Modificar;
end;

procedure TWorks.AsignaColumnas;
begin
     with Grid do
     begin
          AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_ORDEN, ZetaDBGridDBTableView.Columns[5] );
          AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_OPERACION, ZetaDBGridDBTableView.Columns[6] );
          AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_PARTE, ZetaDBGridDBTableView.Columns[7] );
          AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_AREA, ZetaDBGridDBTableView.Columns[8] );
     end;
end;

function TWorks.ValoresGrid : Variant;
begin
     Result := VarArrayOf( [ dmCliente.GetValorActivoStr( stEmpleado ),
                             dmCliente.GetValorActivoStr( stFecha ),
                             stEmpleado,stFecha] );
end;

end.
