unit FTOpera;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseTablasConsulta_DevEx, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid,
  Vcl.StdCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, TressMorado2013,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, cxDBData, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TTOpera = class(TTablaOpcion_DevEx)
    TO_CODIGO: TcxGridDBColumn;
    TO_DESCRIP: TcxGridDBColumn;
    TO_INGLES: TcxGridDBColumn;
    TO_NUMERO: TcxGridDBColumn;
    TO_TEXTO: TcxGridDBColumn;
  private
    { Private declarations }
  protected
    procedure AfterCreate; override;
  public
    { Public declarations }
    procedure DoLookup;override;
  end;

var
  TOpera: TTOpera;

implementation

uses DLabor,
     DGlobal,
     ZetaBuscador_DevEx,
     ZGlobalTress,
     ZetaCommonClasses;

{$R *.DFM}

procedure TTOpera.AfterCreate;
begin
     ApplyMinWidth;
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[5],0,'', skCount );
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
     ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
    // ZetaDBGridDBTableView.Columns[5].Width:=84;
     inherited AfterCreate;
     Caption := dmLabor.cdsTOpera.LookupName;
     ZetaDataset := dmLabor.cdsTOpera;
     HelpContext := H9513_Clasificacion_de_operaciones;

end;

procedure TTOpera.DoLookup;
begin
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Caption, 'TO_CODIGO', ZetaDataset );
end;

end.
