inherited KardexArea: TKardexArea
  Left = 346
  Top = 379
  Caption = 'Kardex de Areas'
  ClientHeight = 270
  ClientWidth = 509
  ExplicitWidth = 509
  ExplicitHeight = 270
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 509
    ExplicitWidth = 509
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 250
      ExplicitWidth = 250
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 244
        ExplicitLeft = 3
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 509
    Height = 251
    ExplicitWidth = 509
    ExplicitHeight = 251
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object KA_FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'KA_FECHA'
        Width = 84
      end
      object KA_HORA: TcxGridDBColumn
        Caption = 'Hora'
        DataBinding.FieldName = 'KA_HORA'
      end
      object CB_AREA: TcxGridDBColumn
        Caption = #193'rea'
        DataBinding.FieldName = 'CB_AREA'
      end
      object TB_ELEMENT: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'TB_ELEMENT'
      end
      object KA_FEC_MOV: TcxGridDBColumn
        Caption = 'Fecha cambio'
        DataBinding.FieldName = 'KA_FEC_MOV'
      end
      object KA_HOR_MOV: TcxGridDBColumn
        Caption = 'Hora cambio'
        DataBinding.FieldName = 'KA_HOR_MOV'
      end
      object US_CODIGO: TcxGridDBColumn
        Caption = 'Usuario'
        DataBinding.FieldName = 'US_CODIGO'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
