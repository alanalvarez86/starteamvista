inherited WizLaborAfectarLabor: TWizLaborAfectarLabor
  Left = 331
  Top = 247
  Caption = 'Afectar Labor'
  ClientHeight = 375
  ClientWidth = 415
  ExplicitWidth = 421
  ExplicitHeight = 404
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 415
    Height = 375
    Header.AssignedValues = [wchvGlyph, wchvTitleFont]
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
      6000003A980000176F925FC546000000097048597300000EC400000EC401952B
      0E1B0000032D494441545847DD57316B1441149E4B8C448310340141455B1BB1
      4861A12088D8042B7F8085A08501C5201681F112D1DCEDCA156252A4086823D1
      18EE622A851401C5A4086A2141249128B98B452A4911F5FCDEFA66B2376F2E17
      EFB6F2838FDD7D33FBBE6F66DFCCEEAA72B95CC15AC8E95C7B5667AF85E9E071
      98CE7E08D2D98F383E45AC77500F1EE46E5521F444600B843AD30DE165B0EC23
      CCAC853A7B91BB7B21F444A00A90F8564C6C039C0FD2C148D89F7D80F339709D
      DBCA617F90E3DB04849E087880E93D8EC4244A02F374CD4D16397DEF081EC5B4
      3111E8E034375540E8898007246AC4B5D63B38EC05666592FB2EA26F2B872D84
      9E08380875D8C109377C2377418518D502EEC9E84C17872D849E0838E0C2A302
      9BE1504DA03F5648F418AE72C842E88980035B7C5B14960B5EA6B8271CE69085
      D013010778A6B7A3643A73894335414B91EEB99F0E46396421F444C0C1A681AD
      D7771CFFA7814067FA3854138919A06507036F22037F97D6E27688BEDF71C4D2
      C5BBC2D990849E0830ECD42740E41A311B98D01301C04C6192A4016DDF00A6CE
      97A411D223A459107A6E80DEF7BE0449906ACAD5ABB82062EF3FE9BB39095241
      BA7A151744EAE4BB3909266DE0B773ACC6CF20E5EC40EEC3AE5EC505B10E03D5
      B80A5E06DB979FB71C2A16D4AB5241CD9426D599A8D20D1A3010A76BE613B80F
      6C7E3974B669A590BA03F172C4BC9AFA56504D91F674E3067E81246E3ED788B4
      631EC588074A85D45C31AFDE597126DA96701C01338D1A302423E67C6A61ACF3
      405C10A35E5B2DA81B38EF817831DED6A8015F215E5F7AD676CA8A40FCCB78DB
      1E9A7202A67F27CF4022067E82F1D1132F80BB57F2A92112A091E33A45E2C81D
      1DF158BAD9C06CA3060CE33340DF902D1079C82257701D159DC1EAA43AC76D33
      C2007DD5C692D5C39EAF13AD2758202AB8D7A35DF6F3BCF44235213E6BDA8501
      C2E6FBBC2EBE7FFBE8D87E231031AF1668DA79E4569CE83580AFD9014FE27FE1
      F962BEF96624EC543D1166D6392E1F01815E9BF1DFAC3AF863F86EDF2E1CDB97
      C75533C49E5871DA11F3AAA534A652D4E635402013787DF62209FD96C5379AED
      720283E80CFA83BD103C6D0CD0394B004AFD016F27F80E1D9439200000000049
      454E44AE426082}
    ParentFont = False
    ExplicitWidth = 415
    ExplicitHeight = 375
    inherited Parametros: TdxWizardControlPage
      Header.AssignedValues = [wchvTitleFont]
      Header.TitleFont.Style = [fsBold]
      Header.Description = 
        'Proceso que protege los c'#225'lculos de tiempos anteriores a una fec' +
        'ha de corte.'
      Header.Title = 'Afectar labor'
      ParentFont = False
      ExplicitWidth = 393
      ExplicitHeight = 233
      object FechaCorteLBL: TLabel
        Left = 85
        Top = 17
        Width = 75
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha de corte:'
      end
      object gbDatosPeriodo: TGroupBox
        Left = 73
        Top = 41
        Width = 239
        Height = 99
        Caption = ' Datos de la n'#243'mina '
        TabOrder = 0
        object PeriodoLBL: TLabel
          Left = 29
          Top = 71
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
        end
        object TipoLBL: TLabel
          Left = 45
          Top = 47
          Width = 24
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo:'
        end
        object AnioLBL: TLabel
          Left = 47
          Top = 23
          Width = 22
          Height = 13
          Alignment = taRightJustify
          Caption = 'A'#241'o:'
        end
        object Numero: TZetaNumero
          Left = 74
          Top = 67
          Width = 50
          Height = 21
          Mascara = mnDias
          TabOrder = 2
          Text = '0'
        end
        object Tipo: TZetaKeyCombo
          Left = 74
          Top = 43
          Width = 145
          Height = 21
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 1
          ListaFija = lfTipoPeriodo
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object Year: TZetaNumero
          Left = 74
          Top = 19
          Width = 50
          Height = 21
          Mascara = mnDias
          TabOrder = 0
          Text = '0'
        end
      end
      object FechaCorte: TZetaFecha
        Left = 162
        Top = 12
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 1
        Text = '30/May/03'
        Valor = 37771.000000000000000000
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      Header.AssignedValues = [wchvTitleFont]
      Header.TitleFont.Style = [fsBold]
      ParentFont = False
      ExplicitWidth = 393
      ExplicitHeight = 233
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 393
        ExplicitHeight = 138
        Height = 138
        Width = 393
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 393
        Width = 393
        inherited Advertencia: TcxLabel
          Caption = 
            'Al aplicar este proceso se proteger'#225'n los c'#225'lculos de tiempos an' +
            'teriores a la fecha de corte.'
          Style.IsFontAssigned = True
          ExplicitLeft = 70
          ExplicitWidth = 321
          Width = 321
          AnchorY = 49
        end
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
  end
end
