unit FWizLaborRecalcularTiempos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Buttons, ComCtrls, ExtCtrls, Mask, StdCtrls,
     ZCXBaseWizardFiltro,
     ZetaFecha,
     ZetaKeyLookup,
     ZetaEdit,
     ZetaWizard, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, TressMorado2013, cxContainer, cxEdit, Vcl.Menus, ZetaCXWizard,
  cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx, cxButtons,
  dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
  dxWizardControl, cxCheckBox;

type
  TWizLaborReCalcularTiempos = class(TBaseCXWizardFiltro)
    Label1: TLabel;
    Label2: TLabel;
    dInicial: TZetaFecha;
    dFinal: TZetaFecha;
    ckRastrear: TcxCheckBox;
    CBLecturas: TcxCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure InitControlesAuto(const lEnabled: Boolean);
  protected
    { Protected declarations }
    function Verificar: Boolean; override;
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
    procedure CargaListaVerificacion; override;
  public
    { Public declarations }
  end;

var
  WizLaborReCalcularTiempos: TWizLaborReCalcularTiempos;

implementation

uses DCliente,
     DLabor,
     FAutoClasses,
     FEmpleadoGridSelect_DevEx,
     ZBaseSelectGrid_DevEx,
     ZetaCommonClasses;

{$R *.DFM}

procedure TWizLaborReCalcularTiempos.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := dInicial;
     HelpContext := H00004_Calcular_tiempos;
     with dmCliente do
     begin
          dInicial.Valor := Fecha;
          dFinal.Valor := Fecha;
     end;
     ckRastrear.Checked := FALSE;
     with CBLecturas do
     begin
          Checked := False; //TRUE;
          Visible := False;
     end;
end;

procedure TWizLaborReCalcularTiempos.FormShow(Sender: TObject);
begin
     inherited;
     InitControlesAuto( ( not Autorizacion.EsDemo ) and Autorizacion.OkModulo( okLabor ) );
     Seleccionar.Visible := true;
     Advertencia.Caption := 'Este proceso convierte las lecturas y �rdenes en una operaci�n que '+
     'se puede consultar en la pantalla operaciones registradas del empleado.';
     ckRastrear.SetFocus;
end;

procedure TWizLaborReCalcularTiempos.CargaListaVerificacion;
begin
     dmLabor.CalcularTiemposGetLista( ParameterList );
end;

procedure TWizLaborReCalcularTiempos.CargaParametros;
begin
     inherited;
     with ParameterList do
     begin
          AddDate( 'FechaInicial', dInicial.Valor );
          AddDate( 'FechaFinal', dFinal.Valor );
          AddBoolean( 'RegistrarBitacora', ckRastrear.Checked );
          AddBoolean( 'CalcularLecturas', CBLecturas.Checked );
     end;
     with Descripciones do
     begin
          AddDate( 'Fecha inicial', dInicial.Valor );
          AddDate( 'Fecha final', dFinal.Valor );
          AddBoolean( 'Rastrear c�lculos', ckRastrear.Checked );
     end;
end;

function TWizLaborReCalcularTiempos.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmLabor.cdsDataset, TEmpleadoGridSelect_DevEx);
end;

function TWizLaborReCalcularTiempos.EjecutarWizard: Boolean;
begin
     Result := dmLabor.CalcularTiempos( ParameterList, Verificacion );
end;

procedure TWizLaborReCalcularTiempos.InitControlesAuto( const lEnabled: Boolean );
begin
     dInicial.Enabled := lEnabled;
     dFinal.Enabled := lEnabled;
     gbRango.Enabled := lEnabled;
     RBLista.Checked := ( not lEnabled );
end;

end.
