unit ZBaseWizardImportar;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Mask, ComCtrls, Buttons, StdCtrls, ExtCtrls,
     ZcxBaseWizard,
     ZetaFecha,
     ZetaWizard, ZetaKeyCombo, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, TressMorado2013, cxContainer, cxEdit,
  ZetaCXWizard, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl, Vcl.Menus, cxButtons;

type
  TTBaseWizardImportar = class(TcxBaseWizard)
    ProgramaLBL: TLabel;
    Programa: TEdit;
    ProgramaSeek: TcxButton;
    ComandosLBL: TLabel;
    Comandos: TEdit;
    ArchivoLBL: TLabel;
    Archivo: TEdit;
    Label8: TLabel;
    Formato: TZetaKeyCombo;
    ArchivoSeek: TcxButton;
    OpenDialog: TOpenDialog;
    procedure FormCreate(Sender: TObject);
    procedure ProgramaSeekClick(Sender: TObject);
    procedure ArchivoSeekClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure FormShow(Sender: TObject);
  private
    FNombreCatalogo : String;
    { Private declarations }
  protected
    { Protected declarations }
    property NombreCatalogo : String read FNombreCatalogo write FNombreCatalogo;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  TBaseWizardImportar: TTBaseWizardImportar;

implementation

uses ZetaCommonTools,
     ZetaClientTools,
     ZetaCommonLists,
     ZetaCommonClasses;

{$R *.DFM}

procedure TTBaseWizardImportar.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := Archivo;
     Formato.ItemIndex := Ord( faASCIIDel );

     self.Caption := 'Importar ' + FNombreCatalogo;    // Debe Tomar Valor en el inherited de los hijos
    { with Advertencia.Lines do
     begin
          Clear;
          Add( VACIO );
          Add( 'Este Proceso Importa El Cat�logo De ' + FNombreCatalogo );
          Add( VACIO );
          Add( 'Presione El Bot�n ''Ejecutar'' Para Iniciar El Proceso' );
     end;}
end;

procedure TTBaseWizardImportar.FormShow(Sender: TObject);
begin
  inherited;
  programa.SetFocus;
end;

procedure TTBaseWizardImportar.CargaParametros;
begin
     inherited;
     with ParameterList do
     begin
          {AddString( 'Programa', Programa.Text );
          AddString( 'Parametros', Comandos.Text );}
          AddString( 'Archivo', Archivo.Text );
          AddInteger( 'Formato', Formato.Valor );
     end;
     with Descripciones do
     begin
          AddString( 'Programa', Programa.Text );
          AddString( 'Par�metros', Comandos.Text );
          AddString( 'Archivo', Archivo.Text );
          AddString( 'Formato', Formato.Text );
     end;
end;

procedure TTBaseWizardImportar.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   sArchivo, sMensaje : String;
begin
     inherited;
     if CanMove and Wizard.Adelante and Wizard.EsPaginaActual( Parametros ) then
     begin
          sArchivo := Archivo.Text;
          if ZetaCommonTools.StrVacio( sArchivo ) then
             CanMove := Error( 'No se especific� el archivo a importar', self.Archivo )
          else if (not EjecutaProgramaArchivo( Programa.Text, Comandos.Text, sArchivo, sMensaje ) ) then
             CanMove := Error( sMensaje, self.Programa )
          else if ZetaCommonTools.StrVacio( Programa.Text )  then
             CanMove := Error( 'El programa no fue encontrado', self.Programa );
     end;
end;

procedure TTBaseWizardImportar.ProgramaSeekClick(Sender: TObject);
begin
     with OpenDialog do
     begin
          Filter := 'Batch ( *.bat )|*.bat|Ejecutables ( *.exe )|*.exe|Todos ( *.* )|*.*';
          FilterIndex := 0;
          Title := 'Seleccione el programa deseado';
     end;
     Programa.Text := ZetaClientTools.AbreDialogo( OpenDialog, Programa.Text, 'BAT' );
end;

procedure TTBaseWizardImportar.ArchivoSeekClick(Sender: TObject);
begin
     inherited;
     with OpenDialog do
     begin
          Filter := 'Archivos DAT ( *.dat )|*.dat|Archivos TXT ( *.txt )|*.txt|Todos ( *.* )|*.*';
          FilterIndex := 0;
          Title := 'Seleccione el archivo deseado';
     end;
     Archivo.Text := ZetaClientTools.AbreDialogo( OpenDialog, Archivo.Text, 'DAT' );
end;

end.
