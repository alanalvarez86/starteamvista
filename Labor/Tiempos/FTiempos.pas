unit FTiempos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, Mask, Db, ExtCtrls, Grids, DBGrids,
     ZetaFecha,
     ZetaNumero,
     ZetaDBGrid,
     ZetaSmartLists;

type
  TTimeCount = class(TForm)
    PanelSuperior: TPanel;
    EmpleadoLBL: TLabel;
    Empleado: TZetaNumero;
    FechaLBL: TLabel;
    Fecha: TZetaFecha;
    PanelInferior: TPanel;
    Agregar: TBitBtn;
    Cancelar: TBitBtn;
    DataSource: TDataSource;
    ZetaDBGrid: TZetaDBGrid;
    Borrar: TBitBtn;
    Modificar: TBitBtn;
    Nombre: TLabel;
    EmpleadoBuscaBtn: TZetaSpeedButton;
    Leer: TSpeedButton;
    OK: TBitBtn;
    DiaSemana: TLabel;
    RefrescarCatalogos: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure LeerClick(Sender: TObject);
    procedure EmpleadoExit(Sender: TObject);
    procedure EmpleadoBuscaBtnClick(Sender: TObject);
    procedure FechaValidDate(Sender: TObject);
    procedure BorrarClick(Sender: TObject);
    procedure ZetaDBGridDblClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure OKClick(Sender: TObject);
    procedure AgregarClick(Sender: TObject);
    procedure ModificarClick(Sender: TObject);
    procedure RefrescarCatalogosClick(Sender: TObject);
    procedure FechaExit(Sender: TObject);
  private
    { Private declarations }
    FUltima: TDate;
    procedure ManejaError(Sender: TObject; E: Exception);
    function HayDatos: Boolean;
  public
    { Public declarations }
    procedure Conectar;
    procedure Desconectar;
    procedure SetControls;
  end;

var
  TimeCount: TTimeCount;

implementation

uses DCliente,
     ZetaBuscaEmpleado,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo;

{$R *.DFM}

{ ********** TTimeCount ********** }

procedure TTimeCount.FormCreate(Sender: TObject);
begin
     ZetaCommonTools.Environment;
     DCliente.dmCliente := TdmCliente.Create( Self );
     with Application do
     begin
          OnException := ManejaError;
          UpdateFormatSettings := FALSE;
     end;
     FUltima := 0;
end;

procedure TTimeCount.FormShow(Sender: TObject);
const
     PTR_ORDEN = 0;
     PTR_PARTE = 1;
     PTR_OPERACION = 2;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with DCliente.dmCliente do
        begin
             if Conectar then
             begin
                  with ZetaDBGrid do
                  begin
                       Columns[ PTR_ORDEN ].Title.Caption := LabelOrden;
                       Columns[ PTR_PARTE ].Title.Caption := LabelParte;
                       Columns[ PTR_OPERACION ].Title.Caption := LabelOperacion;
                  end;
             end;
             Self.Empleado.Valor := Empleado;
             Self.Nombre.Caption := EmpleadoNombre;
        end;
        with Fecha do
        begin
             Valor := Now; { Asigna default y lee lo que exista }
             FUltima := Valor;
        end;
        SetControls;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTimeCount.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
     if ( dmCliente.cdsTiempos.ChangeCount > 0 ) then
        CanClose := ZetaDialogo.zConfirm( Caption, 'Hay Datos Por Grabar' + CR_LF + '� Desea Salir ?', 0, mbNo )
     else
         CanClose := True;
end;

procedure TTimeCount.FormDestroy(Sender: TObject);
begin
     FreeAndNil( DCliente.dmCliente );
end;

procedure TTimeCount.ManejaError(Sender: TObject; E: Exception);
begin
     ZetaDialogo.zExcepcion( Caption, '� Se Encontr� Un Error !', E, 0 );
end;

procedure TTimeCount.Conectar;
begin
     Datasource.Dataset := dmCliente.cdsTiempos;
end;

procedure TTimeCount.Desconectar;
begin
     with Datasource do
     begin
          Dataset := nil;
     end;
end;

function TTimeCount.HayDatos: Boolean;
begin
     with dmCliente.cdsTiempos do
     begin
          Result := Active and ( RecordCount > 0 );
     end;
end;

procedure TTimeCount.SetControls;
var
   lEnabled, lHayDatos: Boolean;
begin
     with dmCliente.cdsTiempos do
     begin
          lEnabled := not Active or ( ChangeCount = 0 );
     end;
     lHayDatos := HayDatos;
     EmpleadoLBL.Enabled := lEnabled;
     Empleado.Enabled := lEnabled;
     FechaLBL.Enabled := lEnabled;
     Fecha.Enabled := lEnabled;
     Leer.Enabled := lEnabled;
     ZetaDBGrid.Enabled := lHayDatos;
     Agregar.Enabled := True;
     Borrar.Enabled := lHayDatos;
     Modificar.Enabled := lHayDatos;
     OK.Visible := not lEnabled;
     with Cancelar do
     begin
          if lEnabled then
          begin
               Kind := bkClose;
               Caption := '&Salir';
               ModalResult := mrCancel;
          end
          else
          begin
               Kind := bkCancel;
               Caption := '&Cancelar';
               ModalResult := mrNone;
          end;
     end;
end;

procedure TTimeCount.EmpleadoExit(Sender: TObject);
var
   oCursor: TCursor;
   iEmpleado: TNumEmp;
begin
     iEmpleado := Empleado.ValorEntero;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmCliente do
        begin
             Empleado := iEmpleado;
             if ( Empleado = iEmpleado ) then
             begin
                  Nombre.Caption := EmpleadoNombre;
                  Leer.Click;
             end
             else
             begin
                  ZetaDialogo.zError( Caption, '� Empleado No Existe !', 0 );
                  ActiveControl := Self.Empleado;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTimeCount.EmpleadoBuscaBtnClick(Sender: TObject);
var
   sKey, sDescription: String;
begin
     inherited;
     if ZetaBuscaEmpleado.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
     begin
          Empleado.Valor := StrToIntDef( sKey, 0 );
          EmpleadoExit( Self );
     end;
end;

procedure TTimeCount.FechaValidDate(Sender: TObject);
begin
     DiaSemana.Caption := FormatDateTime( 'dddd	', Fecha.Valor );
     Leer.Click;
end;

procedure TTimeCount.FechaExit(Sender: TObject);
begin
     if ( Fecha.Valor <> FUltima ) then
     begin
          if HayDatos then
             ActiveControl := ZetaDBGrid
          else
              ActiveControl := Self.Agregar;
          FUltima := Fecha.Valor;
     end;
end;

procedure TTimeCount.LeerClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        Desconectar;
        dmCliente.LeerTiempos( Self.Fecha.Valor );
        Conectar;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTimeCount.RefrescarCatalogosClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        Desconectar;
        dmCliente.RefrescarCatalogos;
        Conectar;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTimeCount.ZetaDBGridDblClick(Sender: TObject);
begin
     Modificar.Click;
end;

procedure TTimeCount.AgregarClick(Sender: TObject);
begin
     DCliente.dmCliente.cdsTiempos.Agregar;
end;

procedure TTimeCount.BorrarClick(Sender: TObject);
begin
     if ZetaDialogo.zConfirm( Caption, '� Desea Borrar Esta Actividad ?', 0, mbNo ) then
     begin
          DCliente.dmCliente.Borrar;
     end;
     SetControls;
end;

procedure TTimeCount.ModificarClick(Sender: TObject);
begin
     with DCliente.dmCliente.cdsTiempos do
     begin
          Edit;
          Modificar;
     end;
end;

procedure TTimeCount.OKClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmCliente do
        begin
             Enviar;
        end;
        ActiveControl := Fecha;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTimeCount.CancelarClick(Sender: TObject);
begin
     if ( Cancelar.Modalresult = mrNone ) then
     begin
          DCliente.dmCliente.Cancelar;
     end;
end;

end.
