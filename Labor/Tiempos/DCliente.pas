unit DCliente;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, DB, DBClient,
     DBaseCliente,
     ZetaCommonLists,
     ZetaCommonClasses,
{$ifdef DOS_CAPAS}
     DServerLabor,
     DServerGlobal,
{$else}
     Labor_TLB,
{$endif}
     ZetaRegistryCliente,
     ZetaClientDataset;

type
  TTiemposRegistry = class( TZetaClientRegistry )
  private
    { Private declarations }
    function GetUltimoEmpleado: Integer;
    procedure SetUltimoEmpleado(const Value: Integer);
  protected
    { Protected declarations }
  public
    { Public declarations }
    property UltimoEmpleado: Integer read GetUltimoEmpleado write SetUltimoEmpleado;
  end;
  TdmCliente = class(TBaseCliente)
    cdsOpera: TZetaLookupDataSet;
    cdsWOrderLookup: TZetaLookupDataSet;
    cdsPartes: TZetaLookupDataSet;
    cdsTiempos: TZetaClientDataSet;
    cdsEmpleado: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsOperaAlAdquirirDatos(Sender: TObject);
    procedure cdsPartesAlAdquirirDatos(Sender: TObject);
    procedure cdsTiemposAlCrearCampos(Sender: TObject);
    procedure cdsTiemposCalcFields(DataSet: TDataSet);
    procedure cdsTiemposAfterOpen(DataSet: TDataSet);
    procedure cdsTiemposAfterPost(DataSet: TDataSet);
    procedure cdsTiemposAfterDelete(DataSet: TDataSet);
    procedure cdsTiemposAlModificar(Sender: TObject);
    procedure cdsTiemposAfterCancel(DataSet: TDataSet);
    procedure cdsWOrderLookupLookupKey(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter, sKey: String; var sDescription: String);
  private
    { Private declarations }
    FParametros: TZetaParams;
    FEmpleado: TNumEmp;
    FFecha: TDate;
    FSistema: String;
    FRegistry: TTiemposRegistry;
    FLabelOrden: String;
    FLabelParte: String;
    FLabelOperacion: String;
{$ifdef DOS_CAPAS}
    FServerLabor: TdmServerLabor;
    FServerGlobal: TdmServerGlobal;
{$else}
    function CreateServer(const ClassID: TGUID): IDispatch;
    function GetServerLabor: IdmServerLaborDisp;
    function GetServerGlobal: IdmServerGlobalDisp;
{$endif}
    function FetchEmployee(const iEmpleado: TNumEmp): Boolean;
    function GetEmpleadoNombre: String;
    procedure ConectaWOrderLookUp(const sOrden: String);
    procedure RefrescarLookupWOrder(Parametros: TZetaParams);
    procedure SetEmpleado(const Value: TNumEmp);
  protected
    { Protected declarations }
{$ifdef DOS_CAPAS}
    property ServerLabor: TdmServerLabor read FServerLabor;
    property ServerGlobal: TdmServerGlobal read FServerGlobal;
{$else}
    property ServerGlobal: IdmServerGlobalDisp read GetServerGlobal;
    property ServerLabor: IdmServerLaborDisp read GetServerLabor;
{$endif}
  public
    { Public declarations }
    property Empleado: TNumEmp read FEmpleado write SetEmpleado;
    property EmpleadoNombre: String read GetEmpleadoNombre;
    property LabelOrden: String read FLabelOrden;
    property LabelParte: String read FLabelParte;
    property LabelOperacion: String read FLabelOperacion;
    property Sistema: String read FSistema;
    function Conectar: Boolean;
    function GetValorActivo( const sValor: String ): String; override;
    function GetValorActivoStr( const eTipo: TipoEstado ): String; override;
    procedure Borrar;
    procedure Cancelar;
    procedure CargaActivosTodos(Parametros: TZetaParams); override;
    procedure Enviar;
    procedure LeerTiempos(const dValue: TDate);
    procedure RefrescarCatalogos;
  end;

var
  dmCliente: TdmCliente;

implementation

uses FTiempos,
     FTiemposEditar,
     ZGlobalTress,
{$ifdef DOS_CAPAS}
     DZetaServerProvider,
     ZetaSQLBroker,
     ZetaRegistryServerEditor,
{$else}
     ZetaRegistryCliente,
{$endif}
     ZetaCommonTools;

{$R *.DFM}

const
     K_TIEMPOS = 'Tiempos';
     K_TIEMPOS_ULTIMO_EMPLEADO = 'UltimoEmpleado';

{ ******** TTiemposRegistry ********* }

function TTiemposRegistry.GetUltimoEmpleado: Integer;
begin
     Result := Registry.ReadInteger( K_TIEMPOS, K_TIEMPOS_ULTIMO_EMPLEADO, 0 );
end;

procedure TTiemposRegistry.SetUltimoEmpleado(const Value: Integer);
begin
     Registry.WriteInteger( K_TIEMPOS, K_TIEMPOS_ULTIMO_EMPLEADO, Value );
end;

{ ********* TdmCliente ********* }

procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
     FRegistry := TTiemposRegistry.Create;
     FEmpleado := 0;
     FFecha := NullDateTime;
{$ifdef DOS_CAPAS}
     ZetaRegistryServerEditor.InitComparte;
     DZetaServerProvider.InitAll;
{$else}
     ZetaRegistryCliente.InitClientRegistry;
     ClientRegistry.InitComputerName;
{$endif}
     inherited;
{$ifdef DOS_CAPAS}
     FServerGlobal := TdmServerGlobal.Create( Self );
     FServerLabor := TdmServerLabor.Create( Self );
{$endif}
end;

procedure TdmCliente.DataModuleDestroy(Sender: TObject);
begin
{$ifdef DOS_CAPAS}
     FreeAndNil( FServerLabor );
     FreeAndNil( FServerGlobal );
{$endif}
     inherited;
{$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll(TRUE);
     DZetaServerProvider.FreeAll;
{$else}
     ZetaRegistryCliente.ClearClientRegistry;
{$endif}
     FRegistry.Free;
end;

{$ifndef DOS_CAPAS}
function TdmCliente.CreateServer( const ClassID: TGUID ): IDispatch;
begin
     with ClientRegistry do
     begin
          if ( ComputerName = '' ) then
             Result := ComObj.CreateComObject( ClassID ) as IDispatch
          else
              Result := ComObj.CreateRemoteComObject( ComputerName, ClassID ) as IDispatch;
     end;
end;

function TdmCliente.GetServerGlobal: IdmServerGlobalDisp;
begin
     Result := IdmServerGlobalDisp( CreateServer( CLASS_dmServerGlobal ) );
end;

function TdmCliente.GetServerLabor: IdmServerLaborDisp;
begin
     Result := IdmServerLaborDisp( CreateServer( CLASS_dmServerLabor ) );
end;
{$endif}

procedure TdmCliente.ConectaWOrderLookUp( const sOrden: String );
begin
     if ( FParametros = nil ) then
        FParametros := TZetaParams.Create;
     with FParametros do
     begin
          AddInteger( 'Tipo', -1 );
          AddString( 'Parte', VACIO );
          AddDate( 'FechaInicial', NullDateTime );
          AddDate( 'FechaFinal', NullDateTime );
          AddString( 'OrderInicial', sOrden );
          AddString( 'OrderFinal', sOrden );
     end;
     RefrescarLookupWOrder( FParametros )
end;

procedure TdmCliente.RefrescarLookupWOrder(Parametros: TZetaParams);
begin
     cdsWOrderLookup.Data := ServerLabor.GetWorder( Empresa, Parametros.VarValues, Ord( ewUnaOrden ) );
end;

function TdmCliente.Conectar: Boolean;
var
   aGlobalServer: Variant;
begin
     Result := False;
     try
        SetUsuario( 0 );
        if ( InitCompany > 0 ) and FindCompany( 'DESARROLLO' ) then
        begin
             SetCompany;
             ConectaWOrderLookUp( '' );
             cdsOpera.Conectar;
             cdsPartes.Conectar;
             aGlobalServer := ServerGlobal.GetGlobales( Empresa );
             FLabelOrden := aGlobalServer[ K_GLOBAL_LABOR_ORDEN ];
             FLabelParte := aGlobalServer[ K_GLOBAL_LABOR_PARTE ];
             FLabelOperacion := aGlobalServer[ K_GLOBAL_LABOR_OPERACION ];
             SetEmpleado( FRegistry.UltimoEmpleado );
             Result := True;
        end;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
end;

procedure TdmCliente.RefrescarCatalogos;
begin
     ConectaWOrderLookUp( '' );
     cdsOpera.Refrescar;
     cdsPartes.Refrescar;
end;

procedure TdmCliente.Borrar;
begin
     with cdsTiempos do
     begin
          Delete;
     end;
end;

procedure TdmCliente.Enviar;
begin
     try
        with cdsTiempos do
        begin
             MergeChangeLog;
             ServerLabor.GrabaTiempos( Empresa, FEmpleado, FFecha, Data );
        end;
        FRegistry.UltimoEmpleado := FEmpleado;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                LeerTiempos( FFecha );
           end;
     end;
     TimeCount.SetControls;
end;

procedure TdmCliente.Cancelar;
begin
     with cdsTiempos do
     begin
          CancelUpdates;
     end;
     TimeCount.SetControls;
end;

function TdmCliente.FetchEmployee( const iEmpleado: TNumEmp ): Boolean;
var
   Datos: OleVariant;
begin
     Result := Servidor.GetEmpleado( Empresa, iEmpleado, Datos );
     if Result then
     begin
          with cdsEmpleado do
          begin
               Data := Datos;
          end;
     end;
end;

function TdmCliente.GetValorActivo(const sValor: String): String;
begin
end;

procedure TdmCliente.CargaActivosTodos(Parametros: TZetaParams);
begin
end;

function TdmCliente.GetValorActivoStr(const eTipo: TipoEstado): String;
begin
end;

procedure TdmCliente.SetEmpleado( const Value: TNumEmp );
begin
     if ( FEmpleado <> Value ) then
     begin
          if ( Value = 0 ) then
             FEmpleado := 0
          else
          begin
               if FetchEmployee( Value ) then
               begin
                    with cdsEmpleado do
                    begin
                         FEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                    end;
               end;
          end;
          TimeCount.Desconectar;
     end;
end;

function TdmCliente.GetEmpleadoNombre: String;
begin
     with cdsEmpleado do
     begin
          if Active then
             Result := FieldByName( 'PRETTYNAME' ).AsString
          else
              Result := '';
     end;
end;

procedure TdmCliente.LeerTiempos( const dValue: TDate );
begin
     if ( FEmpleado > 0 ) then
     begin
          with cdsTiempos do
          begin
               Data := ServerLabor.GetTiempos( Empresa, FEmpleado, dValue );
               FFecha := dValue;
               LogChanges := True;
               TimeCount.SetControls;
          end;
     end;
end;

procedure TdmCliente.cdsOperaAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     with cdsOpera do
     begin
          Data := ServerLabor.GetCatalogo( Empresa, Tag );
     end;                       
end;

procedure TdmCliente.cdsPartesAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     with cdsPartes do
     begin
          Data := ServerLabor.GetCatalogo( Empresa, Tag );
     end;
end;

procedure TdmCliente.cdsTiemposAlCrearCampos(Sender: TObject);
begin
     inherited;
     with cdsTiempos do
     begin
          CreateSimpleLookUp( cdsOpera, 'OP_NOMBRE', 'OP_NUMBER' );
          CreateSimpleLookUp( cdsPartes, 'AR_NOMBRE', 'AR_CODIGO' );
          CreateCalculated( 'WK_HORAS', ftFloat, 0 );
     end;
end;

procedure TdmCliente.cdsTiemposAfterOpen(DataSet: TDataSet);
begin
     inherited;
     with cdsTiempos do
     begin
          MaskNumerico( 'WK_HORAS','#,0.00;-#,0.00' );
     end;
end;

procedure TdmCliente.cdsTiemposCalcFields(DataSet: TDataSet);
begin
     inherited;
     with cdsTiempos do
     begin
          FieldByName( 'WK_HORAS' ).AsFloat := FieldByName( 'WK_TIEMPO' ).AsFloat / 60;
     end;
end;

procedure TdmCliente.cdsTiemposAfterPost(DataSet: TDataSet);
begin
     inherited;
     TimeCount.SetControls;
end;

procedure TdmCliente.cdsTiemposAfterCancel(DataSet: TDataSet);
begin
     inherited;
     TimeCount.SetControls;
end;

procedure TdmCliente.cdsTiemposAfterDelete(DataSet: TDataSet);
begin
     inherited;
     TimeCount.SetControls;
end;

procedure TdmCliente.cdsTiemposAlModificar(Sender: TObject);
begin
     inherited;
     FTiemposEditar.ShowFormaEdicion;
end;

procedure TdmCliente.cdsWOrderLookupLookupKey(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter, sKey: String; var sDescription: String);
begin
     inherited;
     with Sender do
     begin
          DisableControls;
          try
             lOk := Locate( 'WO_NUMBER', sKey, [] );
             if lOk then
             begin
                  sDescription := GetDescription;
                  FSistema := FieldByName( 'AR_CODIGO' ).AsString;
             end;
          finally
                 EnableControls;
          end;
     end;
end;

end.
