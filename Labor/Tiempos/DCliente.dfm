inherited dmCliente: TdmCliente
  OldCreateOrder = True
  object cdsOpera: TZetaLookupDataSet
    Tag = 8
    Aggregates = <>
    IndexFieldNames = 'OP_NUMBER'
    Params = <>
    AlAdquirirDatos = cdsOperaAlAdquirirDatos
    LookupName = 'Cat�logo de Operaciones'
    LookupDescriptionField = 'OP_NOMBRE'
    LookupKeyField = 'OP_NUMBER'
    Left = 160
    Top = 12
  end
  object cdsWOrderLookup: TZetaLookupDataSet
    Tag = 14
    Aggregates = <>
    IndexFieldNames = 'WO_NUMBER'
    Params = <>
    LookupName = 'Ordenes de Trabajo'
    LookupDescriptionField = 'WO_DESCRIP'
    LookupKeyField = 'WO_NUMBER'
    OnLookupKey = cdsWOrderLookupLookupKey
    Left = 160
    Top = 62
  end
  object cdsPartes: TZetaLookupDataSet
    Tag = 7
    Aggregates = <>
    IndexFieldNames = 'AR_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsPartesAlAdquirirDatos
    LookupName = 'Cat�logo de  Partes'
    LookupDescriptionField = 'AR_NOMBRE'
    LookupKeyField = 'AR_CODIGO'
    Left = 160
    Top = 116
  end
  object cdsTiempos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsTiemposAfterOpen
    AfterPost = cdsTiemposAfterPost
    AfterCancel = cdsTiemposAfterCancel
    AfterDelete = cdsTiemposAfterDelete
    OnCalcFields = cdsTiemposCalcFields
    AlCrearCampos = cdsTiemposAlCrearCampos
    AlModificar = cdsTiemposAlModificar
    Left = 256
    Top = 32
  end
  object cdsEmpleado: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 254
    Top = 96
  end
end
