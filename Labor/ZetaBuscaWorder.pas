unit ZetaBuscaWorder;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, Grids, DBGrids,
     ExtCtrls, Db, Mask,
     ZetaMessages,
     ZetaDBGrid,
     ZetaCommonClasses,
     ZetaClientDataset,
     ZetaEdit,
     ZetaKeyLookup,
     ZetaFecha;

type
  TBuscaWorder = class(TForm)
    PanelControles: TPanel;
    BuscarBtn: TBitBtn;
    AceptarBtn: TBitBtn;
    CancelarBtn: TBitBtn;
    DataSource: TDataSource;
    Grid: TZetaDBGrid;
    Status: TRadioGroup;
    lbInicio: TLabel;
    FechaInicial: TZetaFecha;
    FechaFinal: TZetaFecha;
    Label1: TLabel;
    WO_NUMBER_INI: TZetaEdit;
    Label5: TLabel;
    lbFin: TLabel;
    WO_NUMBER_FIN: TZetaEdit;
    Parte: TZetaKeyLookup;
    lbParte: TLabel;
    CBFecha: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BuscarBtnClick(Sender: TObject);
    procedure AceptarBtnClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure CBFechaClick(Sender: TObject);
  private
    { Private declarations }
    FFiltro: String;
    FCodigoWorder: String;
    FDescripcion: String;
    FCapturando: Boolean;
    FParametros : TZetaParams;
    function LookupDataset: TZetaClientDataset;
    procedure Buscar;
    procedure SetNumero;
    procedure SetBotones( const Vacio : Boolean );
    procedure ShowGrid( const lVisible: Boolean );
    procedure Connect;
    procedure Disconnect;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
  protected
    { Protected declarations }
  public
    { Public declarations }
    property CodigoWorder: String read FCodigoWorder;
    property Descripcion: String read FDescripcion;
    property Filtro: String read FFiltro write FFiltro;
  end;

procedure SetOnlyActivos( const lActivos: Boolean );

var
  BuscaWorder: TBuscaWorder;

function BuscaWorderDialogo( const sFilter: String; var sKey, sDescription: String ): Boolean;

implementation

uses ZetaDialogo,
     ZetaCommonTools,
     DGlobal,
     ZGlobalTress,
     ZetaLaborTools,
     ZetaClientTools,
     DCliente,
     DLabor;

var
   FShowOnlyActivos: Boolean;

{$R *.DFM}

procedure SetOnlyActivos( const lActivos: Boolean );
begin
     FShowOnlyActivos := lActivos;
end;

function BuscaWorderDialogo( const sFilter: String; var sKey, sDescription: String ): Boolean;
begin
     Result := False;
     if ( BuscaWorder = nil ) then
        BuscaWorder := TBuscaWorder.Create( Application.MainForm );
     if ( BuscaWorder <> nil ) then
     begin
          with BuscaWorder do
          begin
               ShowModal;
               if ( ModalResult = mrOk ) and ( CodigoWorder <> '' ) then
               begin
                    sKey := CodigoWorder;
                    sDescription := Descripcion;
                    Result := True;
               end;
          end;
     end;
end;

{************** TBuscaEmpleado ************** }

procedure TBuscaWorder.FormCreate(Sender: TObject);
begin
     //HelpContext := H00012_Busqueda_empleados;
     FParametros := TZetaParams.Create;
     FechaInicial.Valor := Date;
     FechaFinal.Valor := Date;
end;

procedure TBuscaWorder.FormShow(Sender: TObject);
begin
     WindowState := wsNormal;
     ActiveControl := Status;
     Connect;
     Caption := 'B�squedas de ' + Global.GetGlobalString( K_GLOBAL_LABOR_ORDEN );
     AsignaGlobal( K_GLOBAL_LABOR_PARTE, lbParte, Parte );
     AsignaGlobalColumn( K_GLOBAL_LABOR_PARTE, Grid.Columns[ Grid.GetFieldColumnIndex( 'AR_NOMBRE' ) ]);
end;

procedure TBuscaWorder.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Disconnect;
     Action := caHide;
end;

function TBuscaWorder.LookupDataset: TZetaClientDataset;
begin
     Result := dmLabor.cdsWOrderLookup;
end;


procedure TBuscaWorder.Connect;
begin
     ShowGrid( False );
     SetBotones( True );
     ActiveControl := Status;
     WO_NUMBER_INI.Clear;
     WO_NUMBER_FIN.Clear;
     Parte.Llave :='';
     Status.ItemIndex := 0;

     dmLabor.cdsPartes.Conectar;
     Datasource.Dataset := LookupDataset;
end;

procedure TBuscaWorder.Disconnect;
begin
     Datasource.Dataset := nil;
end;

procedure TBuscaWorder.Buscar;
var
   oCursor: TCursor;
begin
     AceptarBtn.Enabled := False;
     Application.ProcessMessages;
     oCursor := Screen.Cursor;
     try
        Screen.Cursor := crHourglass;
        with LookupDataset do
        begin
             try
                DisableControls;
                Active := False;
                with FParametros do
                begin
                     AddInteger( 'Tipo', Status.ItemIndex-1 );
                     AddString( 'Parte', Parte.Llave );
                     if CBFecha.Checked then
                     begin
                          AddDate( 'FechaInicial', FechaInicial.Valor );
                          AddDate( 'FechaFinal', FechaFinal.Valor );
                     end
                     else
                     begin
                          AddDate( 'FechaInicial', NullDateTime );
                          AddDate( 'FechaFinal', NullDateTime );
                     end;
                     AddString( 'OrderInicial', WO_NUMBER_INI.Valor );
                     AddString( 'OrderFinal', WO_NUMBER_FIN.Valor );
                end;
                dmLabor.RefrescarLookupWOrder( FParametros );
             finally
                    EnableControls;
             end;
             if IsEmpty then
             begin
                  zWarning( 'B�squeda de Orden de Trabajo', '� No Hay Ordenes Con Estos Datos !', 0, mbOK );
                  SetBotones( True );
                  ActiveControl := Status;
             end
             else
             begin
                  ShowGrid( True );
                  SetBotones( False );
                  ActiveControl := Grid;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TBuscaWorder.SetNumero;
begin
     with LookupDataset do
     begin
          FCodigoWorder := FieldByName( 'WO_NUMBER' ).AsString;
          FDescripcion := FieldByName( 'WO_DESCRIP' ).AsString;
     end;
     ModalResult := mrOK;
end;

procedure TBuscaWorder.SetBotones( const Vacio: Boolean );
begin
     FCapturando := Vacio;
     BuscarBtn.Default := Vacio;
     with AceptarBtn do
     begin
          Default := not Vacio;
          Enabled := not Vacio;
     end;
end;

procedure TBuscaWorder.ShowGrid( const lVisible: Boolean );
const
     GRID_HEIGHT = 200;
     FORM_HEIGHT = 180;
     //HEIGHT_OFFSET = 25;
begin
     with Grid do
     begin
          if lVisible then
          begin
               Visible:= True;
               Height := ZetaClientTools.GetScaledHeight( GRID_HEIGHT );
               Self.Height := ZetaClientTools.GetScaledHeight( FORM_HEIGHT + GRID_HEIGHT );
          end
          else
          begin
               Visible:= False;
               Height := 0;
               Self.Height := ZetaClientTools.GetScaledHeight( FORM_HEIGHT );
          end;
     end;
     Repaint;
end;

procedure TBuscaWorder.WMExaminar(var Message: TMessage);
begin
     AceptarBtn.Click;
end;

procedure TBuscaWorder.BuscarBtnClick(Sender: TObject);
begin
     Buscar;
end;

procedure TBuscaWorder.AceptarBtnClick(Sender: TObject);
begin
     if not LookupDataset.IsEmpty then
        SetNumero;
end;

procedure TBuscaWorder.FormDestroy(Sender: TObject);
begin
     FParametros.Free;
end;

procedure TBuscaWorder.CBFechaClick(Sender: TObject);
 var lEnabled : Boolean;
begin
     lEnabled := CbFecha.Checked;
     lbInicio.Enabled := lEnabled;
     lbFin.Enabled := lEnabled;
     FechaInicial.Enabled := lEnabled;
     FechaFinal.Enabled := lEnabled;
end;

end.
