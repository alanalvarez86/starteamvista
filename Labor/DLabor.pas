unit DLabor;

interface

{$DEFINE MULTIPLES_ENTIDADES}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient, Mask,
     {$ifndef VER130}Variants,{$endif}       
{$ifdef DOS_CAPAS}
     DServerLabor,
     DServerAnualNomina,
{$else}
     Labor_TLB,
     DAnualNomina_TLB,
{$endif}
     ZetaClientDataSet,
     ZBaseThreads,
     ZBaseEdicion_DevEx,
     ZetaTipoEntidad,
     ZetaCommonLists,
     ZetaCommonClasses;

type
  TLaborThread = class( TBaseThread )
  private
    { Private declarations }
    {$ifdef DOS_CAPAS}
    function GetServidor: TdmServerLabor;
    property Servidor: TdmServerLabor read GetServidor;
    {$else}
    function GetServidor: IdmServerLaborDisp;
    property Servidor: IdmServerLaborDisp read GetServidor;
    {$endif}
  protected
    { Protected declarations }
    function Procesar: OleVariant; override;
  end;
  TAnualThread = class( TBaseThread )
  private
    { Private declarations }
    {$ifdef DOS_CAPAS}
    function GetServidor: TdmServerAnualNomina;
    property Servidor: TdmServerAnualNomina read GetServidor;
    {$else}
    function GetServidor: IdmServerAnualNominaDisp;
    property Servidor: IdmServerAnualNominaDisp read GetServidor;

    {$endif}
  protected
    { Protected declarations }
    function Procesar: OleVariant; override;
  end;
  TdmLabor = class(TDataModule)
    cdsModula1: TZetaLookupDataSet;
    cdsModula2: TZetaLookupDataSet;
    cdsModula3: TZetaLookupDataSet;
    cdsTiempoMuerto: TZetaLookupDataSet;
    cdsOpera: TZetaLookupDataSet;
    cdsTParte: TZetaLookupDataSet;
    cdsPartes: TZetaLookupDataSet;
    cdsEditParte: TZetaClientDataSet;
    cdsDatosEmpleado: TZetaClientDataSet;
    cdsWoFija: TZetaClientDataSet;
    cdsLecturas: TZetaClientDataSet;
    cdsMultiLote: TZetaClientDataSet;
    cdsTOpera: TZetaLookupDataSet;
    cdsDefSteps: TZetaLookupDataSet;
    cdsArea: TZetaLookupDataSet;
    cdsWOrderLookup: TZetaLookupDataSet;
    cdsWOrderSteps: TZetaClientDataSet;
    cdsSteps: TZetaLookupDataSet;
    cdsHistorialWOrder: TZetaClientDataSet;
    cdsKardexWOrder: TZetaClientDataSet;
    cdsWorksHijo: TZetaClientDataSet;
    cdsWorks: TZetaClientDataSet;
    cdsMultiLoteWO_NUMBER: TStringField;
    cdsMultiLoteWO_DESCRIP: TStringField;
    cdsAusencia: TZetaClientDataSet;
    cdsDataSet: TZetaClientDataSet;
    cdsCedulas: TZetaClientDataSet;
    cdsBreaks: TZetaLookupDataSet;
    cdsBrkHora: TZetaLookupDataSet;
    cdsASCII: TZetaClientDataSet;
    cdsASCIIRenglon: TStringField;
    cdsCedEmp: TZetaClientDataSet;
    cdsCedEmpCE_FOLIO: TIntegerField;
    cdsCedEmpCB_CODIGO: TIntegerField;
    cdsCedEmpCE_POSICIO: TSmallintField;
    cdsCedEmpCB_PUESTO: TStringField;
    cdsCedEmpPRETTYNAME: TStringField;
    cdsCedMulti: TZetaClientDataSet;
    cdsCedMultiCE_FOLIO: TIntegerField;
    cdsCedMultiWO_NUMBER: TStringField;
    cdsCedMultiCW_POSICIO: TSmallintField;
    cdsCedMultiCW_PIEZAS: TFloatField;
    cdsCedMultiWO_DESCRIP: TStringField;
    cdsCedMultiAR_CODIGO: TStringField;
    cdsSupArea: TZetaClientDataSet;
    cdsKarArea: TZetaClientDataSet;
    cdsTDefecto: TZetaLookupDataSet;
    cdsHistorialCedInsp: TZetaClientDataSet;
    cdsCedulaLookup: TZetaLookupDataSet;
    cdsCedInsp: TZetaClientDataSet;
    cdsDefectos: TZetaClientDataSet;
    cdsMotivoScrap: TZetaLookupDataSet;
    cdsComponentes: TZetaLookupDataSet;
    cdsCedScrap: TZetaClientDataSet;
    cdsScrap: TZetaClientDataSet;
    cdsHistorialCedScrap: TZetaClientDataSet;
    cdsEditComponentes: TZetaClientDataSet;
    cdsTMaquinas: TZetaLookupDataSet;
    cdsMaquinas: TZetaLookupDataSet;
    cdsMaqCertific: TZetaClientDataSet;
    cdsLayouts: TZetaLookupDataSet;
    cdsLayMaquinas: TZetaLookupDataSet;
    cdsSillasMaquina: TZetaClientDataSet;
    cdsSillasLayout: TZetaClientDataSet;
    cdsKarEmpMaquinas: TZetaClientDataSet;
    procedure CatalogosAlAdquirirDatos(Sender: TObject);
    procedure CatalogosAlModificar(Sender: TObject);
    {$ifdef VER130}
    procedure ReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure ReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    procedure AfterPost(DataSet: TDataSet);
    procedure cdsPartesAlCrearCampos(Sender: TObject);
    procedure cdsPartesAfterCancel(DataSet: TDataSet);
    procedure cdsDefStepsAfterDelete(DataSet: TDataSet);
    procedure cdsPartesAlEnviarDatos(Sender: TObject);
    procedure cdsDatosEmpleadoAlAdquirirDatos(Sender: TObject);
    procedure cdsAlCrearCampos(Sender: TObject);
    procedure cdsLecturasAlAdquirirDatos(Sender: TObject);
    procedure cdsDatosEmpleadoAlCrearCampos(Sender: TObject);
    procedure cdsLecturasAlCrearCampos(Sender: TObject);
    procedure cdsWorksAlAdquirirDatos(Sender: TObject);
    procedure cdsWoFijaAlAdquirirDatos(Sender: TObject);
    procedure cdsWOrderLookupLookupSearch(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String; var sKey, sDescription: String);
    procedure cdsWorksAlCrearCampos(Sender: TObject);
    procedure cdsKardexWOrderAlCrearCampos(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure cdsDefStepsNewRecord(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsLecturasNewRecord(DataSet: TDataSet);
    procedure cdsWoFijaNewRecord(DataSet: TDataSet);
    procedure cdsWOrderLookupLookupKey(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter, sKey: String; var sDescription: String);
    procedure cdsDefStepsAlCrearCampos(Sender: TObject);
    procedure cdsKardexWOrderBeforeDelete(DataSet: TDataSet);
    procedure cdsWorksHijoAlCrearCampos(Sender: TObject);
    procedure cdsWoFijaAlCrearCampos(Sender: TObject);
    procedure cdsWOrderLookupAlCrearCampos(Sender: TObject);
    procedure cdsStepsAfterDelete(DataSet: TDataSet);
    procedure cdsStepsAlCrearCampos(Sender: TObject);
    procedure cdsStepsBeforeInsert(DataSet: TDataSet);
    procedure cdsStepsNewRecord(DataSet: TDataSet);
    procedure cdsWOrderStepsAfterCancel(DataSet: TDataSet);
    procedure cdsWOrderStepsAlEnviarDatos(Sender: TObject);
    procedure cdsWOrderStepsAlCrearCampos(Sender: TObject);
    procedure cdsWOrderStepsNewRecord(DataSet: TDataSet);
    procedure cdsDefStepsBeforeInsert(DataSet: TDataSet);
    procedure cdsHistorialWOrderAlCrearCampos(Sender: TObject);
    procedure cdsWorksAlEnviarDatos(Sender: TObject);
    procedure cdsAusenciaAlCrearCampos(Sender: TObject);
    procedure cdsAusenciaCalcFields(DataSet: TDataSet);
    procedure cdsPartesBeforeDelete(DataSet: TDataSet);
    procedure cdsWorksAfterDelete(DataSet: TDataSet);
    procedure cdsWorksAlModificar(Sender: TObject);
    procedure cdsWorksNewRecord(DataSet: TDataSet);
    procedure cdsCedulasAlCrearCampos(Sender: TObject);
    procedure cdsPartesNewRecord(DataSet: TDataSet);
    procedure cdsBreaksAlAdquirirDatos(Sender: TObject);
    procedure cdsBreaksAfterOpen(DataSet: TDataSet);
    procedure cdsBreaksAfterCancel(DataSet: TDataSet);
    procedure cdsBrkHoraAfterDelete(DataSet: TDataSet);
    procedure cdsBrkHoraNewRecord(DataSet: TDataSet);
    procedure cdsBrkHoraAfterOpen(DataSet: TDataSet);
    procedure cdsBreaksAlEnviarDatos(Sender: TObject);
    procedure cdsBreaksAlModificar(Sender: TObject);
    procedure cdsBreaksAfterDelete(DataSet: TDataSet);
    procedure cdsMultiLoteAfterOpen(DataSet: TDataSet);
    procedure cdsHistorialWOrderAfterDelete(DataSet: TDataSet);
    procedure cdsWOrderStepsBeforeDelete(DataSet: TDataSet);
    procedure cdsWOrderStepsAfterDelete(DataSet: TDataSet);
    procedure cdsWoFijaCB_CODIGOChange(Sender: TField);
    procedure cdsWoFijaCB_CODIGOValidate(Sender: TField);
    procedure HoraSetText(Sender: TField; const Text: String);
    procedure HoraGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure AU_FECHAChange(Sender: TField);
    procedure CB_AREAChange(Sender: TField);
    procedure CB_AREAValidate(Sender: TField);
    procedure OP_NUMBERGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure WK_HORA_RChange(Sender: TField);
    procedure US_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    { Edici�n de C�dulas }
    procedure cdsCedMultiAlAdquirirDatos(Sender: TObject);
    procedure cdsCedMultiAlEnviarDatos(Sender: TObject);
    procedure cdsCedMultiAfterOpen(DataSet: TDataSet);
    procedure cdsCedMultiWO_NUMBERChange(Sender: TField);
    procedure cdsCedMultiWO_NUMBERValidate(Sender: TField);
    procedure cdsCedEmpAfterOpen(DataSet: TDataSet);
    procedure cdsCedEmpCB_CODIGOValidate(Sender: TField);
    procedure cdsCedEmpCB_CODIGOChange(Sender: TField);
    procedure cdsCedulasHuboCambios(DataSet: TDataSet);
    procedure cdsCedEmpAlAdquirirDatos(Sender: TObject);
    procedure cdsCedulasAfterCancel(DataSet: TDataSet);
    procedure cdsCedulasAlEnviarDatos(Sender: TObject);
    procedure cdsCedulasAlModificar(Sender: TObject);
    procedure cdsMultiLoteWO_NUMBERChange(Sender: TField);
    procedure cdsCedulasBeforeCancel(DataSet: TDataSet);
    procedure cdsCedulasBeforePost(DataSet: TDataSet);
    procedure cdsCedulasNewRecord(DataSet: TDataSet);
    procedure cdsCedulasAfterDelete(DataSet: TDataSet);
    procedure cdsWorksBeforePost(DataSet: TDataSet);
    procedure cdsTablaBeforePost(DataSet: TDataSet);
    procedure cdsWorksCalcFields(DataSet: TDataSet);
    procedure cdsAreaAfterPost(DataSet: TDataSet);
    procedure cdsAreaNewRecord(DataSet: TDataSet);
    procedure cdsAreaAfterDelete(DataSet: TDataSet);
    procedure cdsAreaAlEnviarDatos(Sender: TObject);
    procedure cdsAreaAfterOpen(DataSet: TDataSet);
    procedure cdsKarAreaAlAdquirirDatos(Sender: TObject);
    procedure cdsKarAreaAlCrearCampos(Sender: TObject);
    procedure cdsKarAreaAlEnviarDatos(Sender: TObject);
    procedure cdsKarAreaBeforePost(DataSet: TDataSet);
    procedure cdsKarAreaNewRecord(DataSet: TDataSet);
    procedure cdsKarAreaAlModificar(Sender: TObject);
    procedure cdsKarAreaAfterDelete(DataSet: TDataSet);
    procedure cdsWorksAlBorrar(Sender: TObject);
    procedure cdsWOrderStepsAlModificar(Sender: TObject);
    procedure cdsCedulaAlCrearCampos(Sender: TObject);
    procedure cdsCedulaLookupKey(Sender: TZetaLookupDataSet;
      var lOk: Boolean; const sFilter, sKey: String;
      var sDescription: String);
    procedure cdsCedulaLookupSearch(Sender: TZetaLookupDataSet;
      var lOk: Boolean; const sFilter: String; var sKey,
      sDescription: String);
    procedure cdsCedInspAlEnviarDatos(Sender: TObject);
    procedure cdsDefectosAlCrearCampos(Sender: TObject);
    procedure cdsCedInspBeforePost(DataSet: TDataSet);
    procedure cdsCedInspAlModificar(Sender: TObject);
    procedure cdsCedInspNewRecord(DataSet: TDataSet);
    procedure cdsHistorialCedInspBeforeDelete(DataSet: TDataSet);
    procedure cdsCedInspAlCrearCampos(Sender: TObject);
    procedure cdsHistorialCedInspAlCrearCampos(Sender: TObject);
    procedure cdsCedInspHuboCambios(DataSet: TDataSet);
    procedure cdsDefectosDE_CODIGOValidate(Sender: TField);
    procedure cdsDefectosAfterOpen(DataSet: TDataSet);
    procedure cdsDefectosBeforePost(DataSet: TDataSet);
    procedure cdsCedInspAfterCancel(DataSet: TDataSet);
    procedure cdsDefectosNewRecord(DataSet: TDataSet);

    {.$ifdef LABORSCRAP}
    procedure cdsComponentesbakAfterDelete(DataSet: TDataSet);
    procedure cdsComponentesAlAdquirirDatos(Sender: TObject);
    procedure cdsComponentesAlCrearCampos(Sender: TObject);
    procedure cdsCedScrapAfterCancel(DataSet: TDataSet);
    procedure cdsCedScrapAlCrearCampos(Sender: TObject);
    procedure cdsCedScrapAlEnviarDatos(Sender: TObject);
    procedure cdsCedScrapAlModificar(Sender: TObject);
    procedure cdsCedScrapBeforePost(DataSet: TDataSet);
    procedure cdsCedScrapNewRecord(DataSet: TDataSet);
    procedure cdsScrapAfterOpen(DataSet: TDataSet);
    procedure cdsScrapAlCrearCampos(Sender: TObject);
    procedure cdsScrapBeforePost(DataSet: TDataSet);
    procedure cdsScrapNewRecord(DataSet: TDataSet);
    procedure cdsHistorialCedScrapAlCrearCampos(Sender: TObject);
    procedure cdsHistorialCedScrapBeforeDelete(DataSet: TDataSet);
    procedure cdsEditComponentesAlEnviarDatos(Sender: TObject);
    procedure cdsEditComponentesAlAdquirirDatos(Sender: TObject);
    procedure cdsComponentesAlAgregar(Sender: TObject);
    procedure cdsComponentesAlModificar(Sender: TObject);
    procedure cdsCedScrapHuboCambios(DataSet: TDataSet);
    procedure cdsEditComponentesAfterDelete(DataSet: TDataSet);
    procedure cdsMaquinasAlCrearCampos(Sender: TObject);
    procedure cdsMaqCertificAlAdquirirDatos(Sender: TObject);
    procedure cdsMaqCertificAlEnviarDatos(Sender: TObject);
    procedure cdsMaqCertificAlCrearCampos(Sender: TObject);
    procedure cdsMaqCertificAlAgregar(Sender: TObject);
    procedure cdsMaqCertificBeforePost(DataSet: TDataSet);
    {.$endif}
    procedure cdsMaqCertificCI_CODIGOValidate(Sender: TField);
    procedure cdsLayoutsBeforePost(DataSet: TDataSet);
    procedure cdsLayoutsAlCrearCampos(Sender: TObject);
    procedure cdsLayMaquinasAlAdquirirDatos(Sender: TObject);
    procedure cdsLayMaquinasAlEnviarDatos(Sender: TObject);
    procedure cdsSillasLayoutAlEnviarDatos(Sender: TObject);
    procedure cdsSillasMaquinaAlEnviarDatos(Sender: TObject);
    procedure cdsKarEmpMaquinasAlAdquirirDatos(Sender: TObject);
    procedure cdsKarEmpMaquinasAlCrearCampos(Sender: TObject);
    procedure cdsKarEmpMaquinasNewRecord(DataSet: TDataSet);
    procedure cdsTMaquinasBeforePost(DataSet: TDataSet);
    procedure cdsMaquinasBeforePost(DataSet: TDataSet);
    procedure cdsMaquinasNewRecord(DataSet: TDataSet);
    procedure cdsMaqCertificNewRecord(DataSet: TDataSet);
    procedure cdsBreaksBeforeDelete(DataSet: TDataSet);
    procedure cdsEditComponentesBeforePost(DataSet: TDataSet);
    procedure cdsBreaksBeforePost(DataSet: TDataSet);
    procedure cdsCedMultiReconcileError(DataSet: TCustomClientDataSet;
      E: EReconcileError; UpdateKind: TUpdateKind;
      var Action: TReconcileAction);
    procedure cdsScrapReconcileError(DataSet: TCustomClientDataSet;
      E: EReconcileError; UpdateKind: TUpdateKind;
      var Action: TReconcileAction);
    procedure cdsCedInspReconcileError(DataSet: TCustomClientDataSet;
      E: EReconcileError; UpdateKind: TUpdateKind;
      var Action: TReconcileAction);


  private
    { Private declarations }
    FLastStep: Integer;
    FParametros: TZetaParams;
    FListaProcesos: TProcessList;
    FCambiosWorks: Boolean;
    FValidaLookups: Boolean;
    FHuboCambiosMultiLote: Boolean;
    FOperacion : eOperacionConflicto;
{$ifdef DOS_CAPAS}
    function GetServerLabor: TdmServerLabor;
    function GetServerAnual: TdmServerAnualNomina;
{$else}
    FServerLabor: IdmServerLaborDisp;
    FServerAnual: IdmServerAnualNominaDisp;
    function GetServerLabor: IdmServerLaborDisp;
    function GetServerAnual: IdmServerAnualNominaDisp;
{$endif}
    function CorreThreadAnual( const eProceso: Procesos; const oLista: OleVariant; oParametros: TZetaParams ): Boolean;
    function CorreThreadLabor(const eProceso: Procesos; const oLista: OleVariant; oParametros: TZetaParams): Boolean;
    function GetLista(ZetaDataset: TZetaClientDataset): OLEVariant;
    function GetDefectos: OleVariant;
    procedure TB_BREAK_2Change(Sender: TField);
    procedure ValidaHora(Sender: TField);
    procedure ShowEdicion(const iTabla: Integer);
    procedure ShowEdicionCedulas;
    procedure EditarGridCedulas(var Forma; EdicionClass: TBaseEdicionClass_DevEx);
    function VerificaCambiosMultiLote:Boolean;

    {.$ifdef LABORSCRAP}
    procedure EditarCedulaScrap;
    function GetScrap: OleVariant;
    procedure cdsScrapCN_CODIGOValidate(Sender: TField);
    procedure cdsScrapSC_MOTIVOValidate(Sender: TField);
    {.$endif}


  protected
    { Protected declarations }
{$ifdef DOS_CAPAS}
    property ServerLabor: TdmServerLabor read GetServerLabor;
    property ServerAnual: TdmServerAnualNomina read GetServerAnual;
{$else}
    property ServerLabor: IdmServerLaborDisp read GetServerLabor;
    property ServerAnual: IdmServerAnualNominaDisp read GetServerAnual;
{$endif}
  public
    { Public declarations }
    function CalcularTiempos(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    function Depuracion(Parametros: TZetaParams): Boolean;
    function EsPrecalculada: Boolean;
    function ImportarASCII( const eProcess: Procesos; Parametros: TZetaParams ): Boolean;
    function ImportarOrdenes(Parametros: TZetaParams): Boolean;
    function ImportarPartes(Parametros: TZetaParams): Boolean;
    function ImportarComponentes(Parametros: TZetaParams): Boolean;
    function ImportarCedulas(Parametros: TZetaParams): Boolean;
    function GetAreaEmpleado( const iEmpleado: integer ): string;
    function AfectarLabor(Parametros: TZetaParams): Boolean;
    function GetMaquinaGlobal:string;
    function GetLayoutGlobal:string;
    procedure DepurarListaEmpleados;
    procedure EditarMultiLote;
    function ShowMultiLote:Boolean;
    procedure ConectaWOrderLookUp( const sOrden: String );overload;
    procedure RefrescaPartes;
    procedure RefrescarLookupWOrder(Parametros: TZetaParams );
    procedure RefrescarHistorialWOrder(Parametros: TZetaParams );
    procedure RefrescarKardexWOrder(Parametros: TZetaParams);
    procedure RefrescarWOrderSteps(Parametros: TZetaParams);
    procedure RefrescarHistorialCedulas(Parametros: TZetaParams);
    procedure RefrescarCedulasInspeccion( Parametros : TZetaParams );
    {$ifdef MULTIPLES_ENTIDADES}
    procedure NotifyDataChange(const Entidades: Array of TipoEntidad; const Estado: TipoEstado);
    {$else}
    procedure NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
    {$endif}
    procedure CalcularTiemposGetLista(Parametros: TZetaParams);
    procedure HandleProcessEnd(const iIndice, iValor: Integer);
    procedure DepuraCedDataSets;
    procedure SetLookupNames;
    procedure ConectaCedulaLookUp( const iCedula: integer );
    procedure RefrescarLookupCedula(Parametros: TZetaParams );
    procedure RefrescaCedulasInspeccion( const iFolio: Integer );
    procedure EditarCedulaInspeccion;
    function MismoUsuario(DataSet: TDataSet): Boolean;
    function PuedeModificarCedInspeccion: Boolean;
    procedure DialogoErrorCedInspeccion;
    procedure AgregarCedulaInspeccion;

    function PuedeModificarCedScrap: Boolean;
    procedure RefrescarCedulasSCrap(Parametros: TZetaParams);
    procedure RefrescaCedulasScrap(const iFolio: Integer);
    procedure AgregarCedulaScrap;
    procedure DialogoErrorCedulaScrap;

  end;

procedure ShowWizard( const eProceso: Procesos );

const
     USE_CDSMULTILOTE = 0;
     USE_CDSCEDMULTI = 1;
     K_MASK_FOLIO_CEDULA = '###,###,###';
     K_MSG_CED_INSPECCION = 'Usuario no tiene derecho para %s esta c�dula de inspecci�n';
     K_MSG_CED_SCRAP = 'Usuario no tiene derecho para %s esta c�dula de scrap';

var
  dmLabor: TdmLabor;

implementation

uses DCliente, DCatalogos, DTablas, DGlobal, DConsultas, DSistema,
     FEditBreaks, FEditTablas, FEditTOpera, FEditOpera, FEditTPartes, FEditPartes,
     FEditWoFija, FEditArea, FEditWorks, FEditLecturas, FEditWorder, FEditMultiLotes,
     FTressShell, ZAsciiTools, ZReconcile, ZetaBuscaWorder_DevEx,ZetaBuscaCedula,
     ZetaCommonTools,ZAccesosMgr, ZAccesosTress, ZBaseGridEdicion_DevEx,
     ZetaWizardFeedBack_DevEx,FWizLaborDepurar,FWizLaborImportarPartes,
     FWizLaborImportarComponentes, FWizLaborImportarCedulas,FWizLaborAfectarLabor,
     FWizLaborRecalcularTiempos,
     FWizLaborImportarOrdenes,
     ZcxWizardBasico,
     ZCXBaseWizardFiltro,
     ZetaDialogo, ZGlobalTress,
     ZetaLaborTools, ZBaseDlgModal,ZetaMsgDlg,

     FCedulasOperaciones_DevEx,
     FCedulasEspeciales, FCedulasTMuerto, FGridMultiLote,
     FEditKardexArea, FConsWorks_DevEx,
     FEditCedInsp,
     FEditCedScrap, FEditComponentes,
     FEditTMaquinas,FEditMaquinas,FEditLayout, ZetaBuscaWorder;

{$R *.DFM}

procedure ShowWizard( const eProceso: Procesos );
var                                //:Todo
   WizardClass: TCXWizardBasicoClass;
begin
     WizardClass := nil;
     case eProceso of
          prLabCalcularTiempos:     WizardClass := TWizLaborReCalcularTiempos;
          prLabDepuracion:          WizardClass := TWizLaborDepurar;
          prLabImportarOrdenes:     WizardClass := TWizLaborImportarOrdenes;
          prLabImportarPartes:      WizardClass := TWizLaborImportarPartes;
          prLabImportarComponentes: WizardClass := TWizLaborImportarComponentes;
          prLabAfectarLabor:        WizardClass := TWizLaborAfectarLabor;
          prLabImportarCedulas:     WizardClass := TWizLaborImportarCedulas;
     end;
     if ( WizardClass = nil ) then { Este IF debe ser Temporal }
        ZetaDialogo.zInformation( 'Estamos trabajando...', 'esta opci�n no ha sido implementada', 0 )
     else
         ZcxWizardBasico.ShowWizard( WizardClass );
     TressShell.ReconectaMenu;
     Application.ProcessMessages;
end;

procedure SetDataChange( const Proceso: Procesos );
begin
     case Proceso of
          prLabCalcularTiempos: TressShell.SetDataChange( [ enWorks, enLecturas ] );
          prLabDepuracion: TressShell.SetDataChange( [ enWorks, enLecturas ] );
          prLabImportarOrdenes: TressShell.SetDataChange( [ enWorder, enSteps ] );
          prLabImportarPartes: TressShell.SetDataChange( [ enPartes ] );
          prLabImportarComponentes: TressShell.SetDataChange( [ enComponentes ] );
          prLabAfectarLabor: TressShell.SetDataChange( [ ] ); // No Esta Afectando nada en la Base de Datos, Solo Cambia un Global
          prLabImportarCedulas: TressShell.SetDataChange( [ enCedula ] );
     end;
end;

{ ********** TLaborThread ********** }

{$ifdef DOS_CAPAS}
function TLaborThread.GetServidor: TdmServerLabor;
begin
     Result := DCliente.dmCliente.ServerLabor;
end;

{$else}
function TLaborThread.GetServidor: IdmServerLaborDisp;
begin
     Result := IdmServerLaborDisp( CreateServer( CLASS_dmServerLabor ) );
end;
{$endif}

function TLaborThread.Procesar: OleVariant;
begin
     case Proceso of
          prLabDepuracion: Result := Servidor.Depuracion( Empresa, Parametros.VarValues );
          prLabImportarOrdenes: Result := Servidor.ImportarOrdenes( Empresa, Parametros.VarValues, Employees );
          prLabImportarPartes: Result := Servidor.ImportarPartes( Empresa, Parametros.VarValues, Employees );
          prLabImportarComponentes: Result := Servidor.ImportarComponentes( Empresa, Parametros.VarValues, Employees );
          prLabImportarCedulas: Result := Servidor.ImportarCedulas( Empresa, Parametros.VarValues, Employees );
     end;
end;

{ ********** TAnualThread *********** }

{$ifdef DOS_CAPAS}
function TAnualThread.GetServidor: TdmServerAnualNomina;
begin
     Result := DCliente.dmCliente.ServerAnualNomina;
end;
{$else}
function TAnualThread.GetServidor: IdmServerAnualNominaDisp;
begin
     Result := IdmServerAnualNominaDisp( CreateServer( CLASS_dmServerAnualNomina ) );
end;
{$endif}

function TAnualThread.Procesar: OleVariant;
begin
     case Proceso of
          prLabCalcularTiempos: Result := Servidor.CalcularTiempos( Empresa, Parametros.VarValues );
          prLabAfectarLabor     : Result := Servidor.AfectarLabor( Empresa, Parametros.VarValues );
     end;
end;

{ *********** TdmLabor ************ }

procedure TdmLabor.DataModuleCreate(Sender: TObject);
begin
     FLastStep := 0;
     FListaProcesos := TProcessList.Create;
     {$ifdef INTERRUPTORES}
     cdsCedMulti.Fields.FindField( 'AR_CODIGO' ).Size := 20;
     cdsCedMulti.Fields.FindField( 'AR_CODIGO' ).DisplayWidth := 20;
     {$endif}

end;

procedure TdmLabor.DataModuleDestroy(Sender: TObject);
begin
     FParametros.Free;
     FListaProcesos.Free;
end;

{$ifdef DOS_CAPAS}
function TdmLabor.GetServerLabor: TdmServerLabor;
begin
     Result := DCliente.dmCliente.ServerLabor;
end;

function TdmLabor.GetServerAnual: TdmServerAnualNomina;
begin
     Result := DCliente.dmCliente.ServerAnualNomina;
end;
{$else}
function TdmLabor.GetServerLabor: IdmServerLaborDisp;
begin
     Result := IdmServerLaborDisp( dmCliente.CreaServidor( CLASS_dmServerLabor, FServerLabor ) );

end;

function TdmLabor.GetServerAnual: IdmServerAnualNominaDisp;
begin
     Result := IdmServerAnualNominaDisp( dmCliente.CreaServidor( CLASS_dmServerAnualNomina, FServerAnual ) );
end;
{$endif}

procedure TdmLabor.CatalogosAlAdquirirDatos(Sender: TObject);
begin
     with TZetaClientDataSet( Sender ) do
     begin
          Data := ServerLabor.GetCatalogo( dmCliente.Empresa, Tag );
     end;
  with cdsTOpera do
     MaskPesos('TO_NUMERO');
  with cdsArea do
     MaskPesos('TB_NUMERO');
  with cdsTMaquinas do
     MaskPesos('TB_NUMERO');
end;

procedure TdmLabor.AfterPost(DataSet: TDataSet);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with TClientDataset( Dataset ) do
     begin
          if ( ChangeCount > 0 ) then
          begin
               if not Reconcile( ServerLabor.GrabaCatalogo( dmCliente.Empresa, Tag, Delta, ErrorCount ) ) then
                  if Tag = 33 then
                     cdsMaquinas.Refrescar;
          end;

     end;
end;

procedure TdmLabor.ShowEdicion( const iTabla: Integer );
begin
     case iTabla of
          1:  ZBaseEdicion_DevEx.ShowFormaEdicion( EditModula1, TTOEditModula1 );
          2:  ZBaseEdicion_DevEx.ShowFormaEdicion( EditModula2, TTOEditModula2 );
          3:  ZBaseEdicion_DevEx.ShowFormaEdicion( EditModula3, TTOEditModula3 );
          4:  ZBaseEdicion_DevEx.ShowFormaEdicion( EditTiempoMuerto, TTOEditTiempoMuerto );
          5:  ZBaseEdicion_DevEx.ShowFormaEdicion( EditTPartes, TEditTPartes);
          6:  ZBaseEdicion_DevEx.ShowFormaEdicion( EditTOpera, TEditTOpera);
          7:  ZBaseEdicion_DevEx.ShowFormaEdicion( EditPartes, TEditPartes );
          8:  ZBaseEdicion_DevEx.ShowFormaEdicion( EditOpera, TEditOpera );
          9:  ZBaseEdicion_DevEx.ShowFormaEdicion( EditWoFija, TEditWoFija );
         10: ZBaseEdicion_DevEx.ShowFormaEdicion( EditWorks, TEditWorks );
         { 11: ZBaseEdicion.ShowFormaEdicion( EditLecturas, TEditLecturas );}

          15: ZBaseEdicion_DevEx.ShowFormaEdicion( EditArea, TEditArea );
          25: ZBaseEdicion_DevEx.ShowFormaEdicion( EditTDefecto, TTOEditTDefecto );
          26: ZBaseEdicion_DevEx.ShowFormaEdicion( EditCedInsp, TEditCedInsp );
          28: ZBaseEdicion_DevEx.ShowFormaEdicion( EditMotivoScrap, TEditMotivoScrap );
          29: ZBaseEdicion_DevEx.ShowFormaEdicion( EditComponentes, TEditComponentes );
          30: ZBaseEdicion_DevEx.ShowFormaEdicion( EditCedScrap, TEditCedScrap );
          32: ZBaseEdicion_DevEx.ShowFormaEdicion( TOTMaquinas, TTOTMaquinas );
          33: ZBaseEdicion_DevEx.ShowFormaEdicion( EditMaquinas, TEditMaquinas );
         36: ZBaseEdicion_DevEx.ShowFormaEdicion( EditPlantillas, TEditPlantillas );
     end;
end;

procedure TdmLabor.CatalogosAlModificar(Sender: TObject);
begin
     ShowEdicion( TClientDataSet( Sender ).Tag );
end;

{$ifdef VER130}
procedure TdmLabor.ReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    function ConflictoBreaks: Boolean;
    begin
         Result := ( Pos( 'Esta c�dula abarca un break', E.Message ) > 0 );
    end;
begin
     Action := raAbort;
     if ConflictoBreaks then
     begin
          if ZetaDialogo.ZConfirm( '�Conflicto con breaks!', E.Message + CR_LF + CR_LF +
                                          '� Desea registrarla ?', 0, mbNo ) then
          begin
               {  Con un solo registro que se elija continuar se enviar� ocIgnorar para volver a reconciliar,
                  los registros que se decide no continuar se cancelan }
               FOperacion := ocIgnorar;
               Action := raCorrect;
          end;
     end
     else
          ZetaDialogo.ZError( 'Error', GetErrorDescription( E ), 0 );
end;
{$else}
procedure TdmLabor.ReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    function ConflictoBreaks: Boolean;
    begin
         Result := ( Pos( 'Esta c�dula abarca un break', E.Message ) > 0 );
    end;
begin
     Action := raAbort;
     if ConflictoBreaks then
     begin
          if ZetaDialogo.ZConfirm( '�Conflicto con breaks!', E.Message + CR_LF + CR_LF +
                                          '� Desea registrarla ?', 0, mbNo ) then
          begin
               {  Con un solo registro que se elija continuar se enviar� ocIgnorar para volver a reconciliar,
                  los registros que se decide no continuar se cancelan }
               FOperacion := ocIgnorar;
               Action := raCorrect;
          end;
     end
     else
          ZetaDialogo.ZError( 'Error', GetErrorDescription( E ), 0 );
end;
{$endif}

{ ******* cdsArea ********* }

procedure TdmLabor.TB_BREAK_2Change(Sender: TField);

  procedure InitBreak( const sBreak: String );
  begin
       with Sender.DataSet.FieldByName( sBreak ) do
       begin
            if StrVacio( AsString ) then
               AsString := Sender.AsString;
       end;
  end;

begin
     InitBreak( 'TB_BREAK_1' );
     InitBreak( 'TB_BREAK_3' );
     InitBreak( 'TB_BREAK_4' );
     InitBreak( 'TB_BREAK_5' );
     InitBreak( 'TB_BREAK_6' );
     InitBreak( 'TB_BREAK_7' );
end;

procedure TdmLabor.cdsAreaAfterOpen(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName( 'TB_BREAK_2' ).OnChange :=  TB_BREAK_2Change;
     end;
end;

procedure TdmLabor.cdsAreaAfterPost(DataSet: TDataSet);
begin
{     with cdsArea do
     begin
          with oLaborBreaks do
          begin
               Break1 := FieldByName( 'TB_BREAK_1' ).AsString;
               Break2 := FieldByName( 'TB_BREAK_2' ).AsString;
               Break3 := FieldByName( 'TB_BREAK_3' ).AsString;
               Break4 := FieldByName( 'TB_BREAK_4' ).AsString;
               Break5 := FieldByName( 'TB_BREAK_5' ).AsString;
               Break6 := FieldByName( 'TB_BREAK_6' ).AsString;
               Break7 := FieldByName( 'TB_BREAK_7' ).AsString;
          end;
     end;}
end;

procedure TdmLabor.cdsAreaAfterDelete(DataSet: TDataSet);
begin
     cdsArea.Enviar;
//     cdsAreaAlEnviarDatos( Dataset );
end;

procedure TdmLabor.cdsAreaAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     with cdsArea do
     begin
          if State in [ dsInsert, dsEdit ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerLabor.GrabaCatalogo( dmCliente.Empresa, Tag, Delta, ErrorCount ) );
          end;
     end;
end;

procedure TdmLabor.cdsAreaNewRecord(DataSet: TDataSet);
const
     K_HORA = '0000';
begin
     with cdsArea do
     begin
{          with oLaborBreaks do
          begin}
               FieldByName( 'TB_OP_UNI' ).AsString := K_GLOBAL_NO;
               FieldByName( 'AR_PRI_HOR' ).AsString := K_HORA;
{               FieldByName( 'TB_BREAK_1' ).AsString := Break1;
               FieldByName( 'TB_BREAK_2' ).AsString := Break2;
               FieldByName( 'TB_BREAK_3' ).AsString := Break3;
               FieldByName( 'TB_BREAK_4' ).AsString := Break4;
               FieldByName( 'TB_BREAK_5' ).AsString := Break5;
               FieldByName( 'TB_BREAK_6' ).AsString := Break6;
               FieldByName( 'TB_BREAK_7' ).AsString := Break7;
          end;
}
     end;
end;

procedure TdmLabor.cdsDatosEmpleadoAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsDatosEmpleado.Data := ServerLabor.GetDatosEmpleado(Empresa,Empleado);
     end;
end;

procedure TdmLabor.cdsAlCrearCampos(Sender: TObject);
begin
     cdsOpera.Conectar;
     with TZetaClientDataSet( Sender ) do
     begin
          CreateSimpleLookUp( cdsOpera, 'OP_NOMBRE', 'OP_NUMBER' );
     end;
end;

procedure TdmLabor.cdsLecturasAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsLecturas.Data := ServerLabor.GetLecturas(Empresa,Empleado,Fecha);
     end;
end;

procedure TdmLabor.cdsDatosEmpleadoAlCrearCampos(Sender: TObject);
begin
     cdsArea.Conectar;
     with TZetaClientDataSet( Sender ) do
     begin
          with dmCatalogos do
          begin
               cdsTurnos.Conectar;
               cdsPuestos.Conectar;
               cdsClasifi.Conectar;
               CreateSimpleLookUp( cdsTurnos, 'TU_DESCRIP', 'CB_TURNO' );
               CreateSimpleLookUp( cdsPuestos, 'PU_DESCRIP', 'CB_PUESTO' );
               CreateSimpleLookUp( cdsClasifi, 'TB_ELEMENT', 'CB_CLASIFI' );
          end;
          CreateSimpleLookUp( cdsArea, 'AR_DESCRIP', 'CB_AREA' );
     end;
end;

procedure TdmLabor.cdsLecturasAlCrearCampos(Sender: TObject);
begin
     cdsOpera.Conectar;
     with TZetaClientDataSet( Sender ) do
     begin
          MaskHoras( 'LX_HORA' );
          with FieldByName( 'LX_HORA' ) do
          begin
               OnSetText := HoraSetText;
               OnGetText := HoraGetText;
          end;
          CreateSimpleLookUp( cdsOpera, 'OP_NOMBRE', 'LX_OPERA' );
     end;
end;

{$ifdef MULTIPLES_ENTIDADES}
procedure TdmLabor.NotifyDataChange(const Entidades: Array of TipoEntidad; const Estado: TipoEstado);
{$else}
procedure TdmLabor.NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
{$endif}
begin
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enEmpleado , Entidades )
     {$else}
     if ( enEmpleado in Entidades )
     {$endif}
     or ( Estado = stEmpleado ) then
     begin
          cdsDatosEmpleado.SetDataChange;
          cdsWorks.SetDataChange;
          cdsLecturas.SetDataChange;
          cdsKarArea.SetDataChange;
          cdsKarEmpMaquinas.SetDataChange; 
     end;

     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enWorks , Entidades ) then
     {$else}
     if ( enWorks in Entidades ) then
     {$endif}
     begin
          cdsWorks.SetDataChange;
          cdsWoFija.SetDataChange;
     end;

     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enCedula , Entidades ) then
     {$else}
     if ( enCedula in Entidades ) then
     {$endif}
     begin
          cdsCedulas.SetDataChange;
     end;

     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enWorder , Entidades ) then
     {$else}
     if ( enWorder in Entidades ) then
     {$endif}
     begin
          cdsWorderSteps.SetDataChange;
          cdsKardexWorder.SetDataChange;
          cdsHistorialWorder.SetDataChange;
     end;

     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enLecturas , Entidades ) then
     {$else}
     if ( enLecturas in Entidades ) then
     {$endif}
     begin
          cdsLecturas.SetDataChange;
     end;

     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enKardexArea , Entidades ) then
     {$else}
     if ( enKardexArea in Entidades ) then
     {$endif}
     begin
          cdsDatosEmpleado.SetDataChange;
          cdsWorks.SetDataChange;
          cdsWoFija.SetDataChange;        // Si se Requiere ??
     end;

     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enCedulaInspeccion , Entidades ) then
     {$else}
     if ( enCedulaInspeccion in Entidades ) then
     {$endif}
     begin
          cdsHistorialCedInsp.SetDataChange;
     end;

     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enCedulaScrap , Entidades ) then
     {$else}
     if ( enCedulaScrap in Entidades ) then
     {$endif}
     begin
          cdsHistorialCedScrap.SetDataChange;
     end;

     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro(enComponentes , Entidades) then
     {$else}
     if (enComponentes in Entidades) then
     {$endif}
     begin
          cdsComponentes.SetDataChange;
     end;

     if ( Estado = stFecha ) then
     begin
          cdsWorks.SetDataChange;
          cdsLecturas.SetDataChange;
     end;
end;


procedure TdmLabor.RefrescarKardexWOrder( Parametros: TZetaParams );
var
   oWorksHijo: OleVariant;
begin
     cdsKardexWOrder.Data := ServerLabor.GetKardexWorder( dmCliente.Empresa, oWorksHijo, Parametros.VarValues );
     cdsWorksHijo.Data := oWorksHijo;
end;

procedure TdmLabor.ConectaWOrderLookUp( const sOrden: String );
begin
     if ( Trim( sOrden ) > '' ) then
     begin
          if ( FParametros = nil ) then
             FParametros := TZetaParams.Create
          else
              FParametros.Clear;

          with FParametros do
          begin
               AddInteger( 'Tipo', -1 );
               AddString( 'Parte', VACIO );
               AddDate( 'FechaInicial', NullDateTime );
               AddDate( 'FechaFinal', NullDateTime );
               AddString( 'OrderInicial', sOrden );
               AddString( 'OrderFinal', sOrden );
          end;
          RefrescarLookupWOrder(FParametros)
     end;
end;

procedure TdmLabor.cdsWoFijaAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsWoFija.Data := ServerLabor.GetOrdenesFijas( Empresa, Empleado );
     end;
end;

procedure TdmLabor.cdsWOrderLookupLookupSearch(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String; var sKey, sDescription: String);
begin
     lOk := ZetaBuscaWorder_DevEx.BuscaWorderDialogo( sFilter, sKey, sDescription );

end;

procedure TdmLabor.cdsKardexWOrderAlCrearCampos(Sender: TObject);
begin
     cdsPartes.Conectar;
     with cdsKardexWOrder do
     begin
          CreateSimpleLookUp( cdsPartes, 'AR_NOMBRE', 'AR_CODIGO' );
          MaskFecha( 'WO_FEC_INI' );
          ListaFija( 'WO_STATUS', lfWorkStatus );
     end;
end;

procedure TdmLabor.cdsLecturasNewRecord(DataSet: TDataSet);
begin
     with cdsLecturas do
     begin
          with dmCliente do
          begin
               FieldByName( 'CB_CODIGO' ).AsInteger := Empleado;
               FieldByName( 'LX_FECHA' ).AsDateTime := Fecha;
          end;
          FieldByName( 'LX_STATUS' ).AsInteger := 0;
          FieldByName( 'LX_HORA' ).AsString := '0000';
          FieldByName( 'LX_WORDER' ).AsString := '';
          FieldByName( 'LX_OPERA' ).AsString := '';
          FieldByName( 'LX_MODULA1' ).AsString := '';
          FieldByName( 'LX_MODULA2' ).AsString := '';
          FieldByName( 'LX_MODULA3' ).AsString := '';
          FieldByName( 'LX_TMUERTO' ).AsString := '';
     end;
end;

procedure TdmLabor.cdsWoFijaNewRecord(DataSet: TDataSet);
begin
     with cdsWoFija do
     begin
          with dmCliente do
          begin
               FieldByName( 'CB_CODIGO' ).AsInteger := Empleado;
               FieldByName( 'WF_FEC_INI' ).AsDateTime := Fecha;
          end;
     end;
end;

procedure TdmLabor.cdsWOrderLookupLookupKey(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter, sKey: String; var sDescription: String);
begin
     ConectaWOrderLookUp( sKey );
     lOk := not cdsWOrderLookup.IsEmpty;
     if lOk then
        sDescription := cdsWOrderLookup.FieldByName( 'WO_DESCRIP' ).AsString;
end;

procedure TdmLabor.cdsKardexWOrderBeforeDelete(DataSet: TDataSet);
begin
     cdsWorksHijo.EmptyDataSet;;
end;

procedure TdmLabor.cdsWorksHijoAlCrearCampos(Sender: TObject);
begin
     with cdsWorksHijo do
     begin
          MaskNumerico( 'WK_TIEMPO', '#,0;-#,0' );
          ListaFija( 'WK_STATUS', lfStatusLectura );
          MaskFecha('AU_FECHA');
     end;
end;

procedure TdmLabor.cdsWoFijaAlCrearCampos(Sender: TObject);
begin
     cdsOpera.Conectar;
     cdsPartes.Conectar;
     with cdsWoFija do
     begin

          FieldByName( 'CB_CODIGO' ).OnValidate := cdsWoFijaCB_CODIGOValidate;
          FieldByName( 'CB_CODIGO' ).OnChange := cdsWoFijaCB_CODIGOChange;
          CreateSimpleLookUp( cdsOpera, 'OP_NOMBRE', 'OP_NUMBER' );
          CreateSimpleLookUp( cdsPartes, 'AR_NOMBRE', 'AR_CODIGO' );

          MaskFecha( 'WF_FEC_INI' );
     end;
end;

procedure TdmLabor.cdsWoFijaCB_CODIGOValidate(Sender: TField);
begin
     with Sender do
     begin
          if ( AsInteger > 0 ) then
          begin
               if ( dmCliente.cdsEmpleadoLookup.GetDescripcion( IntToStr( AsInteger ) ) = VACIO ) then    // Posiciona el empleado en cdsEmpleadoLookup
                  DataBaseError( 'No existe el empleado #' + IntToStr( AsInteger ) );
          end
          else
              DataBaseError( 'N�mero de empleado debe ser mayor a cero' );
     end;
end;

procedure TdmLabor.cdsWoFijaCB_CODIGOChange(Sender: TField);
var
   oCursor: TCursor;
begin
     with cdsWoFija do
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             FieldByName( 'PRETTYNAME' ).AsString := dmCliente.cdsEmpleadoLookup.GetDescription;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TdmLabor.cdsWOrderLookupAlCrearCampos(Sender: TObject);
begin
     cdsPartes.Conectar;
     with cdsWOrderLookup do
     begin
          CreateSimpleLookUp( cdsPartes, 'AR_NOMBRE', 'AR_CODIGO' );
          ListaFija( 'WO_STATUS', lfWorkStatus );
     end;

end;

{****************** MASTER DETAIL cdsWorder-cdsSteps ************** }

procedure TdmLabor.RefrescarWOrderSteps( Parametros: TZetaParams);
var
   oSteps: OleVariant;
begin
     cdsWOrderSteps.Data := ServerLabor.GetWOrderSteps( dmCliente.Empresa, oSteps, Parametros.VarValues );
     cdsSteps.Data := oSteps;
end;

procedure TdmLabor.cdsWOrderStepsAfterCancel(DataSet: TDataSet);
begin
     cdsSteps.CancelUpdates;
end;

procedure TdmLabor.cdsWOrderStepsAfterDelete(DataSet: TDataSet);
begin
     cdsSteps.EmptyDataSet;
end;

procedure TdmLabor.cdsWOrderStepsAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditWorder, TEditWorder );
end;

procedure TdmLabor.cdsWOrderStepsAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   {oDelta: OleVariant;}

   function GetSteps: OleVariant;
   var
      oStep: OleVariant;
      i, iCount: Integer;
   begin
        with cdsSteps do
        begin
             DisableControls;
             try
                iCount := RecordCount;
                if ( iCount > 0 ) and ( ChangeCount > 0 ) then
                begin
                     i := 1;
                     Result := VarArrayCreate( [ 1, iCount ], varVariant );
                     First;
                     while not EOF do
                     begin
                          oStep := VarArrayCreate( [ 1, 4 ], varVariant );
                          oStep[ 1 ]:= FieldByName( 'OP_NUMBER' ).AsString;
                          oStep[ 2 ]:= FieldByName( 'ST_STD_HR' ).AsFloat;
                          oStep[ 3 ]:= FieldByName( 'ST_REAL_HR' ).AsFloat;
                          oStep[ 4 ]:= FieldByName( 'ST_QTY' ).AsInteger;
                          Result[ i ] := oStep;
                          Inc( i );
                          Next;
                     end;
                end
                else
                    ZetaCommonTools.SetOLEVariantToNull( Result );
             finally
                    EnableControls;
             end;
        end;
   end;

begin
     ErrorCount := 0;
     with dmLabor.cdsSteps do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
     end;
     with cdsWorderSteps do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) or ( cdsSteps.ChangeCount > 0 ) then
          begin
               {if ( ChangeCount > 0 ) then
                  oDelta := Delta
               else
                   ZetaCommonTools.SetOLEVariantToNull( oDelta );
               }
               Reconcile( ServerLabor.GrabaWorderSteps( dmCliente.Empresa,
                                                        DeltaNull, {oDelta}
                                                        GetSteps,
                                                        FieldByName( 'WO_NUMBER' ).AsString,
                                                        ErrorCount ) );
               if ( ErrorCount = 0 ) then
               begin
                    TressShell.SetDataChange( [ enWorks, enWorder ] );
                    cdsSteps.MergeChangeLog;    // Se aplicaron los cambios -> Limpiar el Delta
               end;
          end;
     end;
     with dmLabor.cdsSteps do
     begin
          First;
          EnableControls;
     end;
end;

procedure TdmLabor.cdsWOrderStepsAlCrearCampos(Sender: TObject);
begin
     cdsPartes.Conectar;
     with cdsWOrderSteps do
     begin
          MaskFecha( 'WO_FEC_INI' );
          MaskFecha( 'WO_FEC_FIN' );
          MaskFecha( 'WO_FEC_CIE' );
          ListaFija( 'WO_STATUS', lfWorkStatus );
          CreateSimpleLookUp( cdsPartes, 'AR_NOMBRE', 'AR_CODIGO' );
     end;
end;

procedure TdmLabor.cdsWOrderStepsNewRecord(DataSet: TDataSet);
begin
     with cdsWOrderSteps do
     begin
          FieldByName( 'WO_STATUS' ).AsInteger := Ord( wsEnProceso );
          FieldByName( 'WO_FEC_INI' ).AsDateTime := Date;
          FieldByName( 'WO_FEC_FIN' ).AsDateTime := Date;
          FieldByName( 'WO_FEC_CIE' ).AsDateTime := Date;
          FieldByName( 'WO_TEXTO' ).AsString := VACIO;
          FieldByName( 'WO_NUM_GEN' ).AsFloat := 0;
     end;
end;

procedure TdmLabor.cdsWOrderStepsBeforeDelete(DataSet: TDataSet);
var
   ErrorCount: Integer;
begin
     with cdsWOrderSteps do
     begin
          Reconcile( ServerLabor.BorraWOrder( dmCliente.Empresa,
                                              FieldByName( 'WO_NUMBER' ).AsString,
                                              ErrorCount ) );
     end;
end;

procedure TdmLabor.cdsStepsAlCrearCampos(Sender: TObject);
begin
     cdsOpera.Conectar;
     with cdsSteps do
     begin
          CreateSimpleLookUp( cdsOpera, 'OP_NOMBRE', 'OP_NUMBER' );
     end;
end;

procedure TdmLabor.cdsStepsBeforeInsert(DataSet: TDataSet);
begin
     with cdsSteps do
     begin
          Last;
          if not IsEmpty then
             FLastStep := FieldByName( 'ST_SEQUENC' ).AsInteger
          else
              FLastStep := 0;
     end;
end;

procedure TdmLabor.cdsStepsNewRecord(DataSet: TDataSet);
begin
     Inc( FLastStep );
     with cdsSteps do
     begin
          FieldByName( 'ST_SEQUENC' ).AsInteger := FLastStep;
          FieldByName( 'ST_QTY' ).AsInteger := 1;
     end;
end;

procedure TdmLabor.cdsStepsAfterDelete(DataSet: TDataSet);
begin
     with cdsWorderSteps do
     begin
          if ( State = dsBrowse ) then
             Edit;
     end;
end;

{****************** MASTER DETAIL cdsPartes-cdsDefSteps ************** }

procedure TdmLabor.cdsPartesAfterCancel(DataSet: TDataSet);
begin
     cdsDefSteps.CancelUpdates;
end;

procedure TdmLabor.cdsPartesAlCrearCampos(Sender: TObject);
begin
     cdsTParte.Conectar;
     cdsDefSteps.Conectar;
     with cdsPartes do
     begin
          CreateSimpleLookUp( cdsTParte, 'TT_DESCRIP', 'TT_CODIGO' );
          MaskPesos('AR_STD_HR');
          maskFecha('WO_FEC_INI');
     end;
end;

procedure TdmLabor.cdsPartesAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   //oDelta: OleVariant;

  function GetSteps: OleVariant;
  var
     oStep: OleVariant;
     i, iCount: Integer;
  begin
       with cdsDefSteps do
       begin
            DisableControls;
            iCount := RecordCount;
            if ( iCount > 0 ) and ( ChangeCount > 0 ) then
            begin
                 i := 1;
                 Result := VarArrayCreate( [ 1, iCount ], varVariant );
                 First;
                 while not Eof do
                 begin
                      oStep := VarArrayCreate( [ 1, 2 ], varVariant );
                      oStep[ 1 ]:= FieldByName( 'OP_NUMBER' ).AsString;
                      oStep[ 2 ]:= FieldByName( 'DF_STD_HR' ).AsFloat;
                      Result[ i ] := oStep;
                      Inc( i );
                      Next;
                 end;
            end
            else
                ZetaCommonTools.SetOLEVariantToNull( Result );
            EnableControls;
       end;
  end;

begin
     ErrorCount := 0;
     with dmLabor.cdsDefSteps do
     begin
          DisableControls;
          if State in [ dsEdit, dsInsert ] then
             Post;
     end;
     with cdsPartes do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) or ( cdsDefSteps.ChangeCount > 0 )then
          begin
               {if ( ChangeCount > 0 ) then
                  oDelta := Delta
               else
                   ZetaCommonTools.SetOLEVariantToNull( oDelta );}
               Reconcile( ServerLabor.GrabaPartes( dmCliente.Empresa,
                                                   DeltaNull, {oDelta,}
                                                   GetSteps,
                                                   FieldByName( 'AR_CODIGO' ).AsString,
                                                   ErrorCount ) );
          end;
     end;
     with dmLabor.cdsDefSteps do
     begin
          First;
          EnableControls;
     end;
end;

procedure TdmLabor.cdsPartesBeforeDelete(DataSet: TDataSet);
var
   ErrorCount: Integer;
begin
     with cdsPartes do
     begin
          Reconcile( ServerLabor.BorraParte( dmCliente.Empresa,
                                             FieldByName( 'AR_CODIGO' ).AsString,
                                             ErrorCount ) );
     end;
end;

procedure TdmLabor.cdsPartesNewRecord(DataSet: TDataSet);
begin
     with cdsDefSteps do
     begin
          if Active then
             EmptyDataSet;
     end;
end;

procedure TdmLabor.cdsDefStepsAfterDelete(DataSet: TDataSet);
begin
     if ( cdsPartes.State = dsBrowse ) then
        cdsPartes.Edit;
end;

procedure TdmLabor.cdsDefStepsAlCrearCampos(Sender: TObject);
begin
     cdsOpera.Conectar;
     with TZetaClientDataSet( Sender ) do
     begin
          CreateSimpleLookUp( cdsOpera, 'OP_NOMBRE', 'OP_NUMBER' );
     end;
end;

procedure TdmLabor.cdsDefStepsBeforeInsert(DataSet: TDataSet);
begin
     with cdsDefSteps do
     begin
          Last;
          if not IsEmpty then
             FLastStep := FieldByName( 'DF_SEQUENC' ).AsInteger
          else
              FLastStep := 0;
     end;

end;

procedure TdmLabor.cdsDefStepsNewRecord(DataSet: TDataSet);
begin
     Inc(FLastStep);
     with cdsDefSteps do
     begin
          FieldByName( 'DF_SEQUENC' ).AsInteger := FLastStep;
     end;
end;

procedure TdmLabor.RefrescaPartes;
var
   oDefSteps: OleVariant;
begin
     with cdsPartes do
     begin
          ServerLabor.GetPartesSteps( dmCliente.Empresa, oDefSteps, FieldByName( 'AR_CODIGO' ).AsString );
     end;
     cdsDefSteps.Data := oDefSteps;
end;

procedure TdmLabor.RefrescarHistorialWOrder(Parametros: TZetaParams);
begin
     cdsHistorialWOrder.Data := ServerLabor.GetWorder( dmCliente.Empresa, Parametros.VarValues, Ord(ewHistorial) );
end;

procedure TdmLabor.RefrescarLookupWOrder(Parametros: TZetaParams);
begin
     cdsWOrderLookup.Data := ServerLabor.GetWorder( dmCliente.Empresa, Parametros.VarValues, Ord(ewLookup) );
end;

procedure TdmLabor.cdsHistorialWOrderAlCrearCampos(Sender: TObject);
begin
     cdsPartes.Conectar;
     with cdsHistorialWOrder do
     begin
          MaskFecha( 'WO_FEC_INI' );
          ListaFija( 'WO_STATUS', lfWorkStatus );
          CreateSimpleLookUp( cdsPartes, 'AR_NOMBRE', 'AR_CODIGO' );
     end;
end;

procedure TdmLabor.cdsHistorialWOrderAfterDelete(DataSet: TDataSet);
begin
     cdsHistorialWOrder.MergeChangeLog;
end;

procedure TdmLabor.EditarMultiLote;
begin
     with cdsWorks do
     begin
          Conectar;
          Append;
          ZBaseEdicion_DevEx.ShowFormaEdicion( EditMultiLotes, TEditMultiLotes );
     end;
end;

{ ******* cdsAusencia ********* }

procedure TdmLabor.cdsAusenciaAlCrearCampos(Sender: TObject);
begin
     dmCatalogos.cdsHorarios.Conectar;
     with cdsAusencia do
     begin
          ListaFija( 'AU_STATUS', lfStatusAusencia );
          ListaFija( 'AU_TIPODIA', lfTipoDiaAusencia );
          CreateCalculated( 'AU_DIA_SEM', ftString, 10 );
          CreateSimpleLookUp( dmCatalogos.cdsHorarios, 'HO_DESCRIP', 'HO_CODIGO' );
     end;
end;

procedure TdmLabor.cdsAusenciaCalcFields(DataSet: TDataSet);
begin
     with cdsAusencia do
     begin
          FieldByName( 'AU_DIA_SEM' ).AsString := ZetaCommonTools.DiaSemana( FieldByName( 'AU_FECHA' ).AsDateTime );
     end;
end;

procedure TdmLabor.HoraSetText(Sender: TField; const Text: String);
begin
     Sender.AsString := ConvierteHora( Text );
end;

procedure TdmLabor.HoraGetText(Sender: TField; var Text: String; DisplayText: Boolean);
const
     K_HORA_NULA = '----';
var
   iHoras: Integer;
begin
     Text := Sender.AsString;
     if ( Length( Text ) > 0 ) and ( Text <> K_HORA_NULA ) then
     begin
          iHoras := StrToInt( Text );
          //Se Comento estas Lineas de C�digo por que ya no se debe validar si las horas son mayores a 24 horas
          {if ( iHoras >= 2400 ) then
             iHoras := iHoras - 2400;}
          Text := Format( '%4.4d', [ iHoras ] );
          Text := Copy( Text, 1, 2 ) + ':' + Copy( Text, 3, 2 );
     end;
end;

procedure TdmLabor.HandleProcessEnd( const iIndice, iValor: Integer );
begin
     with TWizardFeedback_DevEx.Create( Application ) do
     begin
          try
             with FListaProcesos do
             begin
                  try
                     with LockList do               //:Todo
                     begin
                          if ( iIndice >= 0 ) and ( iIndice < Count ) then
                             ProcessData.Assign( TProcessInfo( Items[ iIndice ] ) )
                          else
                              raise Exception.Create( 'Indice de lista de procesos fuera de rango ( ' + IntToStr( iIndice ) + ' )' );
                     end;
                  finally
                         UnlockList;
                  end;
             end;
             if ProcessOK then
                SetMensajes( '' );
             dmConsultas.LeerUnProceso( ShowProcessInfo( True ) );
             if ProcessOK then
                SetDataChange( ProcessData.Proceso );
          finally
                 Free;
          end;
     end;
     TressShell.ReconectaMenu;
     Application.ProcessMessages;
end;

function TdmLabor.GetLista( ZetaDataset: TZetaClientDataset ): OLEVariant;
begin
     with ZetaDataset do
     begin
          MergeChangeLog;  // Elimina Registros Borrados //
          Result := Data;
     end;
end;

procedure TdmLabor.ValidaHora(Sender: TField);
var
   sHora: String;
begin
     sHora := Sender.AsString;
     if not ZetaCommonTools.ValidaHora( sHora, '48' ) then
     begin
          raise ERangeError.Create( 'Hora "' + Copy( sHora, 1, 2 ) + ':' +
                                    Copy( sHora, 3, 2 ) + '" inv�lida' );
     end;
end;
{*********************************************************}
{********************** PROCESOS DE LABOR ***************** }
{*********************************************************}

procedure TdmLabor.CalcularTiemposGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerAnual.CalcularTiemposGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmLabor.CalcularTiempos(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
var
   vLista: OleVariant;
begin
     if lVerificacion then
        vLista := GetLista( cdsDataset )
     else
         vLista := NULL;
     Result := CorreThreadAnual( prLabCalcularTiempos, vLista, Parametros );
end;

procedure TdmLabor.DepurarListaEmpleados;
begin
     with cdsCedEmp do
     begin
          try
             DisableControls;
             First;
             while not EOF do
                   Delete;
          finally
             EnableControls;
          end;
     end;
end;

function TdmLabor.Depuracion(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadLabor( prLabDepuracion, NULL, Parametros );
end;

function TdmLabor.AfectarLabor(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadAnual( prLabAfectarLabor, NULL, Parametros );
end;

function TdmLabor.ImportarASCII( const eProcess: Procesos; Parametros: TZetaParams ): Boolean;
begin
     ZAsciiTools.FillcdsASCII( cdsASCII, Parametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Active := True;
          try
             Result := CorreThreadLabor( eProcess, GetLista( cdsASCII ), Parametros );
          finally
                 Active := False;
          end;
     end;
end;

function TdmLabor.ImportarOrdenes(Parametros: TZetaParams): Boolean;
begin
     Result := ImportarASCII( prLabImportarOrdenes, Parametros );

    { ZAsciiTools.FillcdsASCII( cdsASCII, Parametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Active := True;
          try
             Result := CorreThreadLabor( prLabImportarOrdenes, GetLista( cdsASCII ), Parametros );
          finally
                 Active := False;
          end;
     end;}

end;

function TdmLabor.ImportarPartes(Parametros: TZetaParams): Boolean;
begin
     Result := ImportarASCII( prLabImportarPartes, Parametros );
{
     ZAsciiTools.FillcdsASCII( cdsASCII, Parametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Active := True;
          try
             Result := CorreThreadLabor( prLabImportarPartes, GetLista( cdsASCII ), Parametros );
          finally
                 Active := False;
          end;
     end;
}
end;

function TdmLabor.ImportarComponentes(Parametros: TZetaParams): Boolean;
begin
     Result := ImportarASCII( prLabImportarComponentes, Parametros );
{
     ZAsciiTools.FillcdsASCII( cdsASCII, Parametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Active := True;
          try
             Result := CorreThreadLabor( prLabImportarComponentes, GetLista( cdsASCII ), Parametros );
          finally
                 Active := False;
          end;
     end;
}
end;

function TdmLabor.ImportarCedulas(Parametros: TZetaParams): Boolean;
begin
     Result := ImportarASCII( prLabImportarCedulas, Parametros );
end;

function TdmLabor.CorreThreadLabor( const eProceso: Procesos; const oLista: OleVariant; oParametros: TZetaParams ): Boolean;
begin
     with TLaborThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          Employees := oLista;
          CargaParametros( oParametros );
          Resume;
     end;
     Result := True;
end;

function TdmLabor.CorreThreadAnual( const eProceso: Procesos; const oLista: OleVariant; oParametros: TZetaParams ): Boolean;
begin
     with TAnualThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          Employees := oLista;
          CargaParametros( oParametros );
          Resume;
     end;
     Result := True;
end;

{ ******* cdsWorks ********** }

procedure TdmLabor.cdsWorksAlAdquirirDatos(Sender: TObject);
var
   oAusencia: OleVariant;
begin
     with dmCliente do
     begin
          cdsWorks.Data := ServerLabor.GetWorksEmpleado(Empresa, oAusencia, Empleado, Fecha );
     end;
     cdsAusencia.Data := oAusencia;
end;

procedure TdmLabor.cdsWorksAlCrearCampos(Sender: TObject);
begin
     cdsOpera.Conectar;
     cdsPartes.Conectar;
     with cdsWorks do
     begin
          MaskHoras( 'WK_HORA_A' );
          MaskHoras( 'WK_HORA_R' );
          MaskNumerico( 'WK_TIEMPO','#,0;-#,0' );
          MaskNumerico( 'WK_CEDULA', K_MASK_FOLIO_CEDULA );
          MaskBool( 'WK_MANUAL' );
          with FieldByName( 'WK_HORA_A' ) do
          begin
               OnSetText := HoraSetText;
               OnGetText := HoraGetText;
          end;
          with FieldByName( 'WK_HORA_R' ) do
          begin
               OnSetText := HoraSetText;
               OnGetText := HoraGetText;
               OnChange := WK_HORA_RChange;
          end;
          with FieldByName( 'AU_FECHA' ) do
          begin
               OnChange := AU_FECHAChange;
          end;
          with FieldByName( 'CB_AREA' ) do
          begin
               OnChange  := CB_AREAChange;
               OnValidate := CB_AREAValidate;
          end;
          CreateSimpleLookUp( cdsOpera, 'OP_NOMBRE', 'OP_NUMBER' );
          CreateSimpleLookUp( cdsPartes, 'AR_NOMBRE', 'AR_CODIGO' );
          ListaFija( 'WK_STATUS', lfStatusLectura );
          ListaFija( 'WK_TIPO', lfTipoLectura );
        //CreateCalculated( 'WK_HORA_INI', ftString, 4 );
        //MaskTime( 'WK_HORA_INI' );
     end;
end;

procedure TdmLabor.cdsWorksCalcFields(DataSet: TDataSet);
{var
   sTotal : string;
 }
begin
  {   with cdsWorks do
     begin
          sTotal :=  aHoraString( AMinutos( FieldByName( 'WK_HORA_R' ).AsString ) - FieldByName( 'WK_TIEMPO' ).AsInteger );
          FieldByName( 'WK_HORA_INI' ).AsString := sTotal;
     end;
     }
end;

procedure TdmLabor.CB_AREAChange(Sender: TField);
begin
     if ( Sender.AsString <> VACIO ) then
        Sender.DataSet.FieldByName( 'OP_NUMBER' ).AsString := cdsArea.FieldByName('TB_OPERA').AsString;
end;

procedure TdmLabor.CB_AREAValidate(Sender: TField);
const
     sMensaje = 'No existe el c�digo: %s en cat�logo de areas';
var
   sValor : string;
begin
     sValor := Sender.AsString;
     if ( sValor <> VACIO ) then
        with cdsArea do
             if ( GetDescripcion( sValor ) = VACIO ) then
                DataBaseError( Format( sMensaje, [ sValor ] ) );
end;

procedure TdmLabor.AU_FECHAChange(Sender: TField);
begin
     with cdsWorks do
     begin
          FieldByName( 'WK_FECHA_R' ).AsDateTime:= Sender.AsDateTime;
     end;
end;

procedure TdmLabor.WK_HORA_RChange(Sender: TField);
begin
     with cdsWorks do
     begin
          FieldByName( 'WK_HORA_A' ).AsString := FieldByName( 'WK_HORA_R' ).AsString;
//          FieldByName( 'CB_AREA' ).AsString := GetAreaEmpleado( FieldByName( 'CB_CODIGO' ).AsInteger );
     end;
end;

function TdmLabor.EsPrecalculada: Boolean;
begin
     Result := ( eTipoLectura( cdsWorks.FieldByName( 'WK_TIPO' ).AsInteger ) in [ wtLibre, wtPrecalculada ] );
end;

procedure TdmLabor.cdsWorksNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          with dmCliente do
          begin
               FieldByName( 'CB_CODIGO' ).AsInteger := Empleado;
               FieldByName( 'AU_FECHA' ).AsDateTime := Fecha;
          end;
          FieldByName( 'WK_HORA_R' ).AsString := FormatDateTime( 'hhmm', Now );   //'0000';
          FieldByName( 'WK_MANUAL' ).AsString := K_GLOBAL_SI;
          FieldByName( 'WK_TIPO' ).AsInteger := Ord( wtChecada );
          FieldByName( 'WK_PIEZAS' ).AsInteger := 1;
          FieldByName( 'WO_NUMBER' ).AsString := '';
          FieldByName( 'AR_CODIGO' ).AsString := '';
          FieldByName( 'WK_MOD_1' ).AsString := '';
          FieldByName( 'WK_MOD_2' ).AsString := '';
          FieldByName( 'WK_MOD_3' ).AsString := '';
          FieldByName( 'CB_PUESTO' ).AsString := '';
          FieldByName( 'WK_TMUERTO' ).AsString := '';
        //FieldByName( 'OP_NUMBER' ).AsString := '';
        //FieldByName( 'CB_AREA' ).AsString := '';
        //FieldByName( 'WK_HORA_A' ).AsString := '0000';
     end;
end;

procedure TdmLabor.cdsWorksAlBorrar(Sender: TObject);
begin
     if not ( zStrToBool( cdsWorks.FieldByName( 'WK_MANUAL' ).AsString ) ) then
               ZetaDialogo.ZError( 'Operaciones registradas',
                                   'No se pueden borrar checadas de sistema', 0 )
     else
     begin
          if ZetaMsgDlg.ConfirmaCambio( '� Desea borrar este registro ?' ) then
          begin
               cdsWorks.Delete;
          end;
     end;
end;

procedure TdmLabor.cdsWorksAlModificar(Sender: TObject);
var
   ConsWorks: TConsWorks_DevEx;
begin
    with cdsWorks do
     begin
          {
          if not ( eTipoLectura( cdsWorks.FieldByName( 'WK_TIPO' ).AsInteger) in [ wtChecada, wtLibre ] ) then
          begin
               ZetaDialogo.ZError( 'Operaciones Registradas',
                                   'No Se Pueden Editar Checadas de Tipo: '+
                                   ObtieneElemento( lfTipoLectura, FieldByName( 'WK_TIPO' ).AsInteger ), 0 );
          end
          else if not ( zStrToBool( FieldByName( 'WK_MANUAL' ).AsString ) ) then
               ZetaDialogo.ZError( 'Operaciones Registradas',
                                   'No se Pueden Editar Checadas de Sistema', 0 )
          else
          }
          if ( zStrToBool( FieldByName( 'WK_MANUAL' ).AsString ) ) then
             ShowEdicion( cdsWorks.Tag )
          else
          begin
               ConsWorks := TConsWorks_DevEx.Create( Self );
               try
                  with ConsWorks do
                  begin
                       Connect;
                       ShowModal;
                  end;
               finally
                      FreeAndNil( ConsWorks );
               end;
          end;
     end;
end;

procedure TdmLabor.cdsWorksBeforePost(DataSet: TDataSet);
begin
     with cdsWorks do
     begin
          if StrVacio(FieldByName( 'WK_HORA_R' ).AsString) then
             DatabaseError( 'Hora real no puede quedar vac�a' );
          if FieldByName( 'WK_HORA_R' ).AsString = '0000' then
             DatabaseError( 'Hora real no puede ser 00:00' );
          {
          if EsPrecalculada then
          begin
          }
               { GA: Fix temporal para almacenar en WK_HRS_3EX la }
               { duraci�n de una checada precalculada. Se debe poner }
               { tambi�n en D:\3Win_20\Super\DLabor.pas }
               { MV 16/Ago/2002: Se Creo un nuevo campo para cuando la checada es PreCalculada }
          {
               FieldByName( 'WK_PRE_CAL' ).AsFloat := FieldByName( 'WK_TIEMPO' ).AsFloat;
          end;
          }
     end;
end;

procedure TdmLabor.cdsWorksAfterDelete(DataSet: TDataSet);
begin
     cdsWorks.Enviar;
end;

procedure TdmLabor.cdsWorksAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsWorks do
     begin
          if State in [ dsInsert, dsEdit ] then
             Post;
          if ( ChangeCount > 0 ) then
             Reconcile( ServerLabor.GrabaWorks( dmCliente.Empresa, Delta, ErrorCount ) );
          TressShell.SetDataChange( [ enWorks ] );
//          cdsWorks.Refrescar;
     end;
end;

{ ****** cdsCedulas ****** }

procedure TdmLabor.cdsCedulasHuboCambios(DataSet: TDataSet);
begin
     with cdsCedulas do
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;

end;


procedure TdmLabor.cdsCedulasAlCrearCampos(Sender: TObject);
begin
     with cdsCedulas do
     begin
          MaskNumerico( 'CE_FOLIO', K_MASK_FOLIO_CEDULA );
          MaskPesos( 'CE_TIEMPO' );
          MaskPesos( 'CE_PIEZAS' );
          MaskTime( 'CE_HORA' );
          ListaFija( 'CE_STATUS', lfTipoLectura );
          ListaFija( 'CE_TIPO', lfTipoCedula );
          FieldByName( 'CE_HORA' ).OnSetText := HoraSetText;
          FieldByName( 'OP_NUMBER' ).OnGetText := OP_NUMBERGetText;
          MaskFecha('CE_FECHA');
          with FieldByName( 'US_CODIGO' ) do
          begin
               OnGetText := US_CODIGOGetText;
               Alignment := taLeftJustify;
          end;
          cdsModula1.Conectar;
          cdsModula2.Conectar;
          cdsModula3.Conectar;
          CreateSimpleLookUp( cdsPartes, 'AR_NOMBRE', 'AR_CODIGO' );
          CreateLookUp(cdsModula1, 'Modulo1', 'CE_MOD_1', 'TB_CODIGO', 'TB_ELEMENT');
          CreateLookUp(cdsModula2, 'Modulo2', 'CE_MOD_2', 'TB_CODIGO', 'TB_ELEMENT');
          CreateLookUp(cdsModula3, 'Modulo3', 'CE_MOD_3', 'TB_CODIGO', 'TB_ELEMENT');
     end;
end;

procedure TdmLabor.OP_NUMBERGetText(Sender: TField; var Text: String; DisplayText: Boolean);
var
   iTipo: Integer;
begin
     if DisplayText then
     begin
          iTipo := Sender.DataSet.FieldByName( 'CE_TIPO' ).AsInteger;
          if ( eTipoCedula( iTipo ) <> tcTMuerto ) then
             Text := cdsOpera.GetDescripcion( Sender.AsString )
          else
             Text := cdsTiempoMuerto.GetDescripcion( Sender.DataSet.FieldByName( 'CE_TMUERTO' ).AsString );
     end;
end;

procedure TdmLabor.US_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if ( Sender.AsInteger = 0 ) then
        Text := ''
     else
        Text := Trim( Sender.AsString + '=' + dmSistema.cdsUsuariosLookUp.GetDescripcion( Sender.AsString ) );
end;

procedure TdmLabor.RefrescarHistorialCedulas( Parametros : TZetaParams );
begin
     cdsCedulas.Data := ServerLabor.GetHistorialCedulas( dmCliente.Empresa, Parametros.VarValues );
end;

{********** cdsCedInsp *******}
procedure TdmLabor.RefrescarCedulasInspeccion( Parametros : TZetaParams );
begin
     cdsHistorialCedInsp.Data := ServerLabor.GetHistorialCedInsp( dmCliente.Empresa, Parametros.VarValues );
end;


{ ******* cdsBreaks ****** }

procedure TdmLabor.cdsBreaksAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsBreaks.Data := ServerLabor.GetBreaks( Empresa );
     end;
end;

procedure TdmLabor.cdsBreaksAfterOpen(DataSet: TDataSet);
begin
     cdsBrkHora.DataSetField := TDataSetField( cdsBreaks.FieldByName( 'qryDetail' ) );
end;

procedure TdmLabor.cdsBreaksAlModificar(Sender: TObject);
begin
   ZBaseEdicion_DevEx.ShowFormaEdicion( EditBreaks, TEditBreaks );
end;

procedure TdmLabor.cdsBreaksAfterCancel(DataSet: TDataSet);
begin
     cdsBrkHora.CancelUpdates;
end;

procedure TdmLabor.cdsBreaksAfterDelete(DataSet: TDataSet);
begin
     cdsBreaksAlEnviarDatos( Dataset );
end;

procedure TdmLabor.cdsBreaksAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     with cdsBreaks do
     begin
          if State in [ dsInsert, dsEdit ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerLabor.GrabaBreaks( dmCliente.Empresa, Delta, ErrorCount ) );
          end;
     end;
end;

{ ********* cdsBrkHora ********** }

procedure TdmLabor.cdsBrkHoraAfterOpen(DataSet: TDataSet);
begin
     with cdsBrkHora do
     begin
          MaskTime( 'BH_INICIO' );
          with FieldByName( 'BH_INICIO' ) do
          begin
               OnSetText := HoraSetText;
               OnValidate := ValidaHora;
          end;
     end;
end;

procedure TdmLabor.cdsBrkHoraAfterDelete(DataSet: TDataSet);
begin
     with cdsBreaks do
     begin
          if ( State = dsBrowse ) then
             Edit;
     end;
end;

procedure TdmLabor.cdsBrkHoraNewRecord(DataSet: TDataSet);
begin
     with cdsBrkHora do
     begin
          FieldByName( 'BR_CODIGO' ).AsString := cdsBreaks.FieldByName( 'BR_CODIGO' ).AsString;
     end;
end;

procedure TdmLabor.cdsMultiLoteAfterOpen(DataSet: TDataSet);
begin
     with cdsMultiLote.FieldByName( 'WO_NUMBER' ) do
     begin
          OnChange := cdsMultiLoteWO_NUMBERChange;
     end;
end;

procedure TdmLabor.cdsMultiLoteWO_NUMBERChange(Sender: TField);
var
   sDescription : string;
begin
     with cdsMultiLote do
     begin
          if cdsWOrderLookup.LookupKey( FieldByName( 'WO_NUMBER' ).AsString, '', sDescription ) then
             FieldByName( 'WO_DESCRIP' ).AsString := sDescription;
     end;
end;

{ cdsKardexArea }

procedure TdmLabor.cdsKarAreaAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
          cdsKarArea.Data := ServerLabor.GetKardexArea( Empresa, Empleado );
end;

procedure TdmLabor.cdsKarAreaAlCrearCampos(Sender: TObject);
begin
     cdsArea.Conectar;
     with TZetaClientDataSet( Sender ) do
     begin
          MaskFecha( 'KA_FECHA' );
          MaskFecha( 'KA_FEC_MOV' );
          MaskTime( 'KA_HORA' );
          MaskTime( 'KA_HOR_MOV' );
          FieldByName( 'KA_HORA' ).OnSetText := HoraSetText;
          CreateSimpleLookUp( cdsArea, 'TB_ELEMENT', 'CB_AREA' );
     end;
end;

procedure TdmLabor.cdsKarAreaNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          with dmCliente do
          begin
               FieldByName( 'CB_CODIGO' ).AsInteger := Empleado;
               FieldByName( 'KA_FECHA' ).AsDateTime := FechaDefault;
          end;
          FieldByName( 'KA_HORA' ).AsString := FormatDateTime( 'hhmm', Now );
     end;
end;

procedure TdmLabor.cdsKarAreaAlModificar(Sender: TObject);
begin
   ZBaseEdicion_DevEx.ShowFormaEdicion( EditKardexArea, TEditKardexArea );
end;

procedure TdmLabor.cdsKarAreaBeforePost(DataSet: TDataSet);
begin
     with TClientDataSet( DataSet ) do
     begin
          if ( StrVacio(FieldByName( 'KA_HORA' ).AsString) ) or ( FieldByName( 'KA_HORA' ).AsString = '0000' ) then
             DataBaseError( 'Hora de transferencia no puede quedar vac�a' );
          FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
          FieldByName( 'KA_FEC_MOV' ).AsDateTime := dmCliente.FechaDefault;
          FieldByName( 'KA_HOR_MOV' ).AsString := FormatDateTime( 'hhmm', Now );
     end;
end;

procedure TdmLabor.cdsKarAreaAfterDelete(DataSet: TDataSet);
begin
     cdsKarArea.Enviar;
end;

procedure TdmLabor.cdsKarAreaAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsKarArea do
     begin
          if State in [ dsInsert, dsEdit ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerLabor.GrabaKardexArea( dmCliente.Empresa, Delta, ErrorCount ) );
               TressShell.SetDataChange( [ enKardexArea ] );
          end;
     end;
end;

{ cdsCedMulti }

procedure TdmLabor.cdsCedMultiAlAdquirirDatos(Sender: TObject);
begin
     cdsCedMulti.Data := ServerLabor.GetCedMultiLote(dmCliente.Empresa,
                       cdsCedulas.FieldByName( 'CE_FOLIO' ).AsInteger );
     FHuboCambiosMultiLote:= FALSE;
end;

procedure TdmLabor.cdsCedMultiAfterOpen(DataSet: TDataSet);
begin
     with cdsCedMulti.FieldByName( 'WO_NUMBER' ) do
     begin
          OnValidate := cdsCedMultiWO_NUMBERValidate;
          OnChange := cdsCedMultiWO_NUMBERChange;
     end;
end;

procedure TdmLabor.cdsCedMultiWO_NUMBERValidate(Sender: TField);
const
     K_MENSAJE = 'No existe el c�digo: %s en %s';
var
   sValor : String;
begin
     if FValidaLookups then
     begin
          with Sender do
          begin
               sValor := AsString;
          end;
          if ( sValor <> VACIO ) then
          begin
             with cdsWOrderLookup do
             begin
                  if ( GetDescripcion( sValor ) = VACIO ) then
                     DataBaseError( Format( K_MENSAJE, [ sValor, LookupName ] ) );
             end;
          end;
     end;
end;

procedure TdmLabor.cdsCedMultiWO_NUMBERChange(Sender: TField);
begin
     cdsCedMulti.FieldByName( 'WO_DESCRIP' ).AsString := cdsWOrderLookup.GetDescription;
     cdsCedMulti.FieldByName( 'AR_CODIGO' ).AsString := cdsWOrderLookup.FieldByName('AR_CODIGO').AsString;
end;

procedure TdmLabor.cdsCedMultiAlEnviarDatos(Sender: TObject);
begin
     with cdsCedMulti do
     begin
          if State in [dsEdit, dsInsert] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               MergeChangeLog;
               FHuboCambiosMultiLote:= TRUE;
          end;
     end;
end;

procedure TdmLabor.cdsCedMultiReconcileError(DataSet: TCustomClientDataSet;
  E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
end;

{ cdsCedEmp }

procedure TdmLabor.cdsCedEmpAfterOpen(DataSet: TDataSet);
begin
     with cdsCedEmp.FieldByName( 'CB_CODIGO' ) do
     begin
          OnValidate := cdsCedEmpCB_CODIGOValidate;
          OnChange := cdsCedEmpCB_CODIGOChange;
     end;
end;

procedure TdmLabor.cdsCedEmpCB_CODIGOValidate(Sender: TField);
var
   iEmpleado: Integer;
   sNombre : String;
begin
     iEmpleado := Sender.AsInteger;
     if ( iEmpleado <= 0 ) then
        DataBaseError( 'N�mero de empleado debe ser mayor a cero' )
     else
     begin
          sNombre:= dmCliente.cdsEmpleadoLookup.GetDescripcion( IntToStr( iEmpleado ) );
          if ( sNombre = VACIO ) then    // Posiciona el empleado en cdsEmpleadoLookup
             DataBaseError( 'No existe el empleado #' + IntToStr( iEmpleado ) )
          else if ( not zStrToBool( dmCliente.cdsEmpleadoLookup.FieldByName( 'CB_ACTIVO' ).AsString ) ) then
             DataBaseError( 'El empleado # ' + IntToStr( iEmpleado ) + ' est� dado de baja' )
     end;
end;

procedure TdmLabor.cdsCedEmpCB_CODIGOChange(Sender: TField);
begin
     with Sender.DataSet, dmCliente do
     begin
          FieldByName( 'PRETTYNAME' ).AsString := cdsEmpleadoLookup.GetDescription;
          FieldByName( 'CB_PUESTO' ).AsString := cdsEmpleadoLookup.FieldByName( 'CB_PUESTO' ).AsString;
     end;
end;

procedure TdmLabor.cdsCedEmpAlAdquirirDatos(Sender: TObject);
begin
     cdsCedEmp.Data := ServerLabor.GetCedEmpleados(dmCliente.Empresa,
                       cdsCedulas.FieldByName( 'CE_FOLIO' ).AsInteger );
end;



procedure TdmLabor.cdsCedulasAfterCancel(DataSet: TDataSet);
begin
     if zStrToBool( cdsCedulas.FieldByName( 'CE_MULTI' ).AsString ) then
        cdsCedMulti.Refrescar
     else
     begin
          InitcdsColectivo( cdsCedMulti );
          FHuboCambiosMultiLote:= FALSE;
     end;
end;

procedure TdmLabor.cdsCedulasAlEnviarDatos(Sender: TObject);
var
   ErrorCount : Integer;
   ErrorVar: OleVariant;
   Parametros: TZetaParams;
begin
     ErrorCount := 0;
     with cdsCedEmp do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
     end;
     with cdsCedulas do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount = 0 ) and ( ( cdsCedEmp.ChangeCount > 0 ) or ( FHuboCambiosMultiLote ) ) then   //Solo Cambiaron las Empleados o MultiLotes
          begin
               Edit;
               FieldByName( 'SETCAMBIOS' ).AsString := K_GLOBAL_SI;   // Forzar que lleve un Delta
               Post;
          end;
          if ( ChangeCount > 0 ) then
          begin
               Parametros := TZetaParams.Create;
               try
                  FOperacion := ocReportar;
                  Repeat
                        Parametros.AddInteger( 'Operacion', Ord( FOperacion ) );
                        ErrorVar:= ServerLabor.GrabaCedula( dmCliente.Empresa, Delta, cdsCedEmp.Data, cdsCedMulti.Data, Parametros.VarValues, ErrorCount );
                  Until ( Reconciliar( ErrorVar ) or ( FOperacion = ocReportar ) );   // Cuando es individual se abortar� y pondr� ocReportar en True
               finally
                      FreeAndNil( Parametros );
               end;
               if ErrorCount = 0 then
                  FCambiosWorks := TRUE;
          end;
     end;
end;


procedure TdmLabor.cdsCedulasAfterDelete(DataSet: TDataSet);
var
   ErrorCount : Integer;
begin
     ErrorCount := 0;
     with cdsCedulas do
          if ( ChangeCount > 0 ) then
          begin
               Reconcile ( ServerLabor.BorraCedula( dmCliente.Empresa, Delta, ErrorCount ) );
               if ErrorCount = 0 then
                  NotifyDataChange( [ enWorks ], stNinguno )
               else
                  cdsCedulas.CancelUpdates;
          end;
end;

procedure TdmLabor.cdsCedulasAlModificar(Sender: TObject);
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             cdsCedEmp.Refrescar;
             if zStrToBool( cdsCedulas.FieldByName( 'CE_MULTI' ).AsString ) then
                cdsCedMulti.Refrescar
             else
             begin
                  InitcdsColectivo( cdsCedMulti );
                  FHuboCambiosMultiLote:= FALSE;
             end;
          finally
             Cursor := oCursor;
          end;
     end;
     ShowEdicionCedulas;
end;

procedure TdmLabor.ShowEdicionCedulas;
begin
     case eTipoCedula( cdsCedulas.FieldByName( 'CE_TIPO' ).AsInteger ) of
          tcOperacion : EditarGridCedulas( CedulasOperaciones_DevEx, TBaseEdicionClass_DevEx( TCedulasOperaciones_DevEx) );
          tcEspeciales: EditarGridCedulas( CedulasEspeciales, TBaseEdicionClass_DevEx( TCedulasEspeciales) );
          tcTMuerto: EditarGridCedulas( CedulasTMuerto, TBaseEdicionClass_DevEx( TCedulasTMuerto) );
     end;
end;

procedure TdmLabor.EditarGridCedulas(var Forma; EdicionClass: TBaseEdicionClass_DevEx);
begin
     try
        dmCliente.SetLookupEmpleado( eLookEmpCedulas );
        FCambiosWorks:= FALSE;
        ZBaseEdicion_DevEx.ShowFormaEdicion( Forma, EdicionClass );
        Application.ProcessMessages;
        if FCambiosWorks then                                          //:Todo
        begin
             NotifyDataChange( [ enWorks, enAusencia ], stNinguno );
             cdsCedulas.ResetDataChange;
        end;
     finally
        dmCliente.ResetLookupEmpleado;
     end;
end;

procedure TdmLabor.cdsCedulasBeforeCancel(DataSet: TDataSet);
begin
     if ( cdsCedulas.State = dsEdit ) then     // Es Modificacion
     begin
          if ( cdsCedEmp.State in [ dsEdit, dsInsert ] ) then
             cdsCedEmp.Cancel;
          cdsCedEmp.CancelUpdates;
     end;
end;

procedure TdmLabor.cdsCedulasBeforePost(DataSet: TDataSet);
begin
     cdsCedulas.FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
end;

procedure TdmLabor.cdsCedulasNewRecord(DataSet: TDataSet);
begin
     with cdsCedulas do
     begin
          FieldByName( 'CE_FECHA' ).AsDateTime := dmCliente.FechaDefault;
          FieldByName( 'CE_HORA' ).AsString := FormatDateTime( 'hhmm', Now );
          FieldByName( 'CE_PIEZAS' ).AsFloat := 1;
     end;
end;

function TdmLabor.ShowMultiLote:Boolean;
var
   sOrden: String;
begin
     sOrden := cdsCedulas.FieldByName( 'WO_NUMBER' ).AsString;
     with cdsCedMulti do
     begin
          if ( IsEmpty or ( RecordCount < 2 ) ) and StrLleno( sOrden ) then
          begin
               if IsEmpty then
                  Append
               else
                  Edit;
               FieldByName( 'WO_NUMBER' ).AsString := sOrden;
               FieldByName( 'CW_PIEZAS' ).AsFloat := cdsCedulas.FieldByName( 'CE_PIEZAS' ).AsFloat;
               Post;
               MergeChangeLog;
          end;
          FValidaLookups:= TRUE;
          try
              ZBaseGridEdicion_DevEx.ShowGridEdicion( GridMultiLote, TGridMultiLote, USE_CDSCEDMULTI );
          finally
             FValidaLookups := FALSE;
          end;
          Result := VerificaCambiosMultiLote;
     end;
end;

function TdmLabor.VerificaCambiosMultiLote:Boolean;
var
   lMulti : Boolean;
begin
     with cdsCedMulti do
     begin
          Result := ( RecordCount > 1 );
          if FHuboCambiosMultiLote then
          begin
               lMulti := ( RecordCount > 1 );
               // Graba Cambios en cdsCedulas
               if not ( cdsCedulas.State in [ dsEdit, dsInsert ] ) then
                  cdsCedulas.Edit;
               cdsCedulas.FieldByName( 'CE_MULTI' ).AsString := zBoolToStr( lMulti );
               if IsEmpty then
               begin
                    cdsCedulas.FieldByName( 'WO_NUMBER' ).AsString := VACIO;
                    cdsCedulas.FieldByName( 'AR_CODIGO' ).AsString := VACIO;
                    cdsCedulas.FieldByName( 'CE_PIEZAS' ).AsFloat := 0;
               end
               else
               begin
                    First;
                    cdsCedulas.FieldByName( 'WO_NUMBER' ).AsString := FieldByName( 'WO_NUMBER' ).AsString;
                    cdsCedulas.FieldByName( 'AR_CODIGO' ).AsString := FieldByName( 'AR_CODIGO' ).AsString;
                    cdsCedulas.FieldByName( 'CE_PIEZAS' ).AsFloat := FieldByName( 'CW_PIEZAS' ).AsFloat;
                    if ( not lMulti ) then     // Si no es MultiLote Depurar
                       EmptyDataSet;
               end;
          end;

     end;
end;

procedure TdmLabor.DepuraCedDataSets;
var
   Lista: TStrings;
begin
     Lista:= TStringList.Create;
     try
        RevisaDataSet( 'CB_CODIGO', cdsCedEmp, Lista );
        if zStrToBool( cdsCedulas.FieldByName( 'CE_MULTI' ).AsString ) then
        begin
             RevisaDataSet( 'WO_NUMBER', cdsCedMulti, Lista );
             with cdsCedMulti do
             begin
                  if ( ChangeCount > 0 ) then
                  begin
                       Enviar;
                       VerificaCambiosMultiLote;
                  end;
             end;
        end;
     finally
        FreeAndNil( Lista );
     end;
end;

procedure TdmLabor.SetLookupNames;
begin
     cdsModula1.LookupName := Global.GetGlobalString( K_GLOBAL_LABOR_MODULA1 );
     cdsModula2.LookupName := Global.GetGlobalString( K_GLOBAL_LABOR_MODULA2 );
     cdsModula3.LookupName := Global.GetGlobalString( K_GLOBAL_LABOR_MODULA3 );
     cdsTiempoMuerto.LookupName := Global.GetGlobalString( K_GLOBAL_LABOR_TMUERTO );
     cdsArea.LookupName := Global.GetGlobalString( K_GLOBAL_LABOR_AREA );
     cdsTParte.LookupName := 'Clase de ' + Global.GetGlobalString(K_GLOBAL_LABOR_PARTE);
     cdsTOpera.LookupName := 'Clase de ' + Global.GetGlobalString(K_GLOBAL_LABOR_OPERACION);
     cdsTDefecto.LookupName := 'Clase de ' + Global.GetGlobalString(K_GLOBAL_LABOR_TDEFECTOS);
     cdsPartes.LookupName := Global.GetGlobalString(K_GLOBAL_LABOR_PARTE);
     cdsOpera.LookupName := Global.GetGlobalString(K_GLOBAL_LABOR_OPERACION);
     cdsWOrderLookup.LookupName := Global.GetGlobalString(K_GLOBAL_LABOR_ORDENES);
end;

procedure TdmLabor.cdsTablaBeforePost(DataSet: TDataSet);
begin
     if DataSet.Fields[0].IsNull then
        DB.DatabaseError( 'C�digo no puede quedar vac�o' );
end;

function TdmLabor.GetAreaEmpleado( const iEmpleado: integer ): string;
begin
     with dmCliente do
     begin
          Result := ServerLabor.GetAreaKardex( Empresa, iEmpleado, Fecha, cdsWorks.FieldByName( 'WK_HORA_R' ).AsString );
     end;
end;


procedure TdmLabor.ConectaCedulaLookUp( const iCedula: integer );
begin
     if ( iCedula >= 0 ) then
     begin
          if ( FParametros = nil ) then
             FParametros := TZetaParams.Create
          else
              FParametros.Clear;

          with FParametros do
          begin
               AddInteger( 'Status', - 1 );
               AddInteger( 'Tipo', - 1 );
               AddString( 'Parte', Vacio );
               AddString( 'Operacion', Vacio );
               AddString( 'Orden', Vacio );
               AddString( 'Area', Vacio );
               //AddString( 'Cedula', IntToStr( iCedula ) );
               AddDate( 'FechaInicial', NullDateTime );
               AddDate( 'FechaFinal', NullDateTime );
               AddInteger( 'Usuario', 0 );
          end;

          RefrescarLookupCedula( FParametros )
     end;
end;

procedure TdmLabor.RefrescarLookupCedula( Parametros: TZetaParams );
begin
     cdsCedulaLookup.Data := ServerLabor.GetHistorialCedulas( dmCliente.Empresa, Parametros.VarValues );
end;

procedure TdmLabor.cdsCedulaAlCrearCampos(Sender: TObject);
begin
     dmSistema.cdsUsuarios.Conectar;
     cdsPartes.Conectar;
     cdsOpera.Conectar;
     cdsArea.Conectar;
     cdsTiempoMuerto.Conectar;

     with cdsCedulaLookup do
     begin;
          MaskTime( 'CE_HORA' );
     end;

end;

procedure TdmLabor.cdsCedulaLookupKey( Sender: TZetaLookupDataSet;
                                       var lOk: Boolean;
                                       const sFilter, sKey: String;
                                       var sDescription: String );
var
    iKey : integer;
begin
     iKey := StrToIntDef( sKey, 0 );
     if ( iKey = 0 ) then
     begin
          lOk := TRUE;
          sDescription := VACIO;
     end
     else
     begin
          ConectaCedulaLookUp( iKey );
          lOk := not cdsCedulaLookup.IsEmpty;
          if lOk then
             sDescription := cdsCedulaLookup.FieldByName( 'CE_COMENTA' ).AsString;
     end;
end;


procedure TdmLabor.cdsCedulaLookupSearch( Sender: TZetaLookupDataSet;
                                          var lOk: Boolean;
                                          const sFilter: String;
                                          var sKey, sDescription: String);
begin
     lOk := ZetaBuscaCedula.BuscaCedulasDialogo( sFilter, sKey, sDescription );
end;

{ ************ cdsCedInsp ************** }
procedure TdmLabor.RefrescaCedulasInspeccion( const iFolio: Integer );
var
   oDefectos: OleVariant;
begin
     cdsCedInsp.Data := ServerLabor.GetCedInspeccion( dmCliente.Empresa, oDefectos, iFolio );
     cdsDefectos.Data := oDefectos;
end;

function TdmLabor.GetDefectos: OleVariant;
var
   oDefecto: OleVariant;
   i, iCount: Integer;
begin
     with cdsDefectos do
     begin
          DisableControls;
          try
             iCount := RecordCount;
             if ( iCount > 0 ) and ( ChangeCount > 0 ) then
             begin
                  i := 1;
                  Result := VarArrayCreate( [ 1, iCount ], varVariant );
                  First;
                  while not EOF do
                  begin
                       oDefecto := VarArrayCreate( [ 1, 5 ], varVariant );
                       oDefecto[ 1 ]:= FieldByName( 'CI_FOLIO' ).AsInteger;
                       oDefecto[ 2 ]:= FieldByName( 'DE_FOLIO' ).AsInteger;
                       oDefecto[ 3 ]:= FieldByName( 'DE_CODIGO' ).AsString;
                       oDefecto[ 4 ]:= FieldByName( 'DE_PIEZAS' ).AsFloat;
                       oDefecto[ 5 ]:= FieldByName( 'DE_COMENTA' ).AsString;
                       Result[ i ] := oDefecto;
                       Inc( i );
                       Next;
                  end;
             end
             else
                 ZetaCommonTools.SetOLEVariantToNull( Result );
          finally
                 EnableControls;
          end;
     end;
end;

procedure TdmLabor.cdsCedInspAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   //oDeltaCedula : OleVariant;
begin
     ErrorCount := 0;
     with cdsDefectos do
     begin
          DisableControls;
          if State in [ dsEdit, dsInsert ] then
             Post;
     end;
     try
        with cdsCedInsp do
        begin
             if State in [ dsEdit, dsInsert ] then
                Post;

             {if ChangeCount > 0 then
                oDeltaCedula := Delta
             else
                 SetOLEVariantToNull( oDeltaCedula );}

             if ( ChangeCount > 0 ) or ( cdsDefectos.ChangeCount > 0 ) then
             begin

                  Reconcile( ServerLabor.GrabaCedInspeccion( dmCliente.Empresa,
                                                             DeltaNull, {oDeltaCedula,}
                                                             GetDefectos,
                                                             FieldByName( 'CI_FOLIO' ).AsInteger,
                                                             ErrorCount ) );
                  if ( ErrorCount = 0 ) then
                  begin
                       TressShell.SetDataChange( [ enCedulaInspeccion ] );
                       cdsDefectos.MergeChangeLog;    // Se aplicaron los cambios -> Limpiar el Delta
                  end;
             end;
        end;
     finally
            with cdsDefectos do
            begin
                 First;
                 EnableControls;
            end;
     end;
end;

procedure TdmLabor.cdsDefectosAlCrearCampos(Sender: TObject);
begin
     cdsTDefecto.Conectar;
     with cdsDefectos do
           CreateSimpleLookUp( cdsTDefecto, 'TB_ELEMENT', 'DE_CODIGO' );
end;

procedure TdmLabor.cdsCedInspBeforePost(DataSet: TDataSet);

 procedure ValidaCodigo( const sField: string; const iGlobal : integer );
  var sGlobal : string;
 begin
      sGlobal := Global.GetGlobalString( iGlobal );
      if StrLleno( sGLobal ) then
      begin
           with cdsCedInsp do
                if StrVacio( FieldByName( sField ).AsString ) then
                   DataBaseError( Format( 'El c�digo de %s no puede quedar vacio', [ sGlobal ] ) );
      end;
 end;
begin
     with cdsCedInsp do
     begin
          ValidaCodigo( 'WO_NUMBER', K_GLOBAL_LABOR_ORDEN );
          ValidaCodigo( 'AR_CODIGO', K_GLOBAL_LABOR_PARTE );
          ValidaCodigo( 'CI_AREA', K_GLOBAL_LABOR_AREA );

          FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
     end;

end;

procedure TdmLabor.cdsCedInspAlModificar(Sender: TObject);
begin
     EditarCedulaInspeccion;
end;

procedure TdmLabor.AgregarCedulaInspeccion;
begin
     cdsArea.Conectar;
     RefrescaCedulasInspeccion(-1);
     with cdsCedInsp do
     begin
          Agregar;
     end;
end;

procedure TdmLabor.EditarCedulaInspeccion;
begin
     ShowEdicion(cdsCedInsp.Tag)
end;

procedure TdmLabor.cdsCedInspNewRecord(DataSet: TDataSet);
begin
     with cdsCedInsp do
     begin
          FieldByName('CI_FECHA').AsDateTime := Date;
          FieldByName('CI_HORA').AsString := FormatDateTime( 'hhmm', Time );
          FieldByName('CI_TIPO').AsInteger := Ord( citFinal );
          FieldByName('CI_RESULT').AsInteger := Ord( cirAceptado );
     end;
end;

procedure TdmLabor.cdsCedInspReconcileError(DataSet: TCustomClientDataSet;
  E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
    showMessage('asd');
end;

procedure TdmLabor.cdsHistorialCedInspBeforeDelete(DataSet: TDataSet);
var
   ErrorCount: Integer;
begin
     with cdsHistorialCedInsp do
     begin
          Reconcile( ServerLabor.BorraCedulasInsp( dmCliente.Empresa,
                                                  FieldByName( 'CI_FOLIO' ).AsInteger,
                                                  ErrorCount ) );
     end;
end;

procedure TdmLabor.cdsCedInspAlCrearCampos(Sender: TObject);
begin
     //dmSistema.cdsUsuarios.Conectar;
     with cdsCedInsp do
     begin
          with FieldByName( 'US_CODIGO' ) do
          begin
               OnGetText := US_CODIGOGetText;
               Alignment := taLeftJustify;
          end;
          //CreateSimpleLookup(dmSistema.cdsUsuarios,'US_NOMBRE','US_CODIGO');
     end;
end;

procedure TdmLabor.cdsHistorialCedInspAlCrearCampos(Sender: TObject);
begin
     //cdsPartes.Conectar;
     //cdsArea.Conectar;
     //cdsCedulaLookup.Conectar;
     //dmSistema.cdsUsuarios.Conectar;
     with cdsHistorialCedInsp do
     begin
          MaskTime( 'CI_HORA' );
          ListaFija( 'CI_TIPO', lfTipoCedInspeccion );
          ListaFija( 'CI_RESULT', lfResultadoCedInspeccion );
          MaskNumerico( 'NumDefectos', '#,0;-#,0;#' );
          MaskFecha( 'CI_FECHA' );
          //CreateSimpleLookUp( cdsPartes, 'AR_NOMBRE', 'AR_CODIGO' );
          //CreateSimpleLookUp( cdsArea, 'AR_DESCRIP', 'CI_AREA' );
          //CreateSimpleLookUp( cdsCedulaLookup, 'CE_COMENTA', 'CE_FOLIO' );
          with FieldByName( 'US_CODIGO' ) do
          begin
               OnGetText := US_CODIGOGetText;
               Alignment := taLeftJustify;
          end;
     end;
end;


procedure TdmLabor.cdsDefectosDE_CODIGOValidate(Sender: TField);
const
    sMensaje = 'No existe el c�digo: %s en cat�logo de defectos';
var
   sValor, sDescripcion : string;
begin
     sValor := Sender.AsString;
     if ( sValor <> VACIO ) then
     begin
          with cdsTDefecto do
               if NOT LookUpKey( sValor, '', sDescripcion )  then
                  DataBaseError( Format( sMensaje, [ sValor ] ) );
     end;
end;

procedure TdmLabor.cdsCedInspHuboCambios(DataSet: TDataSet);
begin
     with cdsCedInsp do
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
end;

procedure TdmLabor.cdsDefectosAfterOpen(DataSet: TDataSet);
begin
     cdsDefectos.FieldByName( 'DE_CODIGO' ).OnValidate := cdsDefectosDE_CODIGOValidate;
end;

procedure TdmLabor.cdsDefectosNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName( 'CI_FOLIO' ).AsInteger := cdsCedInsp.FieldByName( 'CI_FOLIO' ).AsInteger;
          FieldByName( 'DE_FOLIO' ).AsInteger := 0;
          FieldByName( 'DE_CODIGO' ).AsString := VACIO;

     end;
end;

procedure TdmLabor.cdsDefectosBeforePost(DataSet: TDataSet);
begin
     if cdsDefectos.FieldByName('DE_CODIGO').IsNull then
        DB.DatabaseError( 'C�digo no puede quedar vac�o' );
end;

procedure TdmLabor.cdsCedInspAfterCancel(DataSet: TDataSet);
begin
     cdsDefectos.CancelUpdates;
end;

function TdmLabor.MismoUsuario( Dataset : TDataset ): Boolean;
begin
     Result := dmCliente.Usuario = DataSet.FieldByName('US_CODIGO').AsInteger;
end;


function TdmLabor.PuedeModificarCedInspeccion : Boolean;
begin
     Result := ( cdsCedInsp.State = dsInsert ) OR
               MismoUsuario(cdsHistorialCedInsp) OR
               ZAccesosMgr.CheckDerecho( D_LAB_CEDULAS_INSP, K_DERECHO_SIST_KARDEX );
end;


procedure TdmLabor.DialogoErrorCedInspeccion;
begin
     ZetaDialogo.ZInformation( 'C�dula de inspecci�n', Format( K_MSG_CED_INSPECCION, ['Modificar'] ), 0 );
end;


{.$ifdef LABORSCRAP}
procedure TdmLabor.cdsComponentesbakAfterDelete(DataSet: TDataSet);
begin
     cdsComponentes.Enviar;
end;


procedure TdmLabor.cdsComponentesAlAdquirirDatos(Sender: TObject);
begin
     with TZetaClientDataSet( Sender ) do
     begin
          Data := ServerLabor.GetComponentes( dmCliente.Empresa );
     end;
end;

procedure TdmLabor.cdsComponentesAlCrearCampos(Sender: TObject);
begin
     with cdsComponentes do
     begin
          MaskPesos( 'CN_COSTO' );
          MaskPesos( 'CN_NUMERO1' );
          MaskPesos( 'CN_NUMERO2' );
     end;
end;


procedure TdmLabor.cdsCedScrapAfterCancel(DataSet: TDataSet);
begin
     cdsScrap.CancelUpdates;
end;

procedure TdmLabor.cdsCedScrapAlCrearCampos(Sender: TObject);
begin
     with cdsCedScrap do
     begin
          with FieldByName( 'US_CODIGO' ) do
          begin
               OnGetText := US_CODIGOGetText;
               Alignment := taLeftJustify;
          end;
     end;
end;

function TdmLabor.GetScrap: OleVariant;
var
   oScrap: OleVariant;
   i, iCount: Integer;
begin
     with cdsScrap do
     begin
          DisableControls;
          try
             iCount := RecordCount;
             if ( iCount > 0 ) and ( ChangeCount > 0 ) then
             begin
                  i := 1;
                  Result := VarArrayCreate( [ 1, iCount ], varVariant );
                  First;
                  while not EOF do
                  begin
                       oScrap := VarArrayCreate( [ 1, 6 ], varVariant );
                       oScrap[ 1 ]:= FieldByName( 'CS_FOLIO' ).AsInteger;
                       oScrap[ 2 ]:= FieldByName( 'SC_FOLIO' ).AsInteger;
                       oScrap[ 3 ]:= FieldByName( 'CN_CODIGO' ).AsString;
                       oScrap[ 4 ]:= FieldByName( 'SC_MOTIVO' ).AsString;
                       oScrap[ 5 ]:= FieldByName( 'SC_PIEZAS' ).AsFloat;
                       oScrap[ 6 ]:= FieldByName( 'SC_COMENTA' ).AsString;
                       Result[ i ] := oScrap;
                       Inc( i );
                       Next;
                  end;
             end
             else
                 ZetaCommonTools.SetOLEVariantToNull( Result );
          finally
                 EnableControls;
          end;
     end;
end;

procedure TdmLabor.cdsCedScrapAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   //oDeltaCedula : OleVariant;
begin
     ErrorCount := 0;
     try
        with cdsScrap do
        begin
             DisableControls;
             if State in [ dsEdit, dsInsert ] then
                Post;
        end;
        with cdsCedScrap do
        begin
             if State in [ dsEdit, dsInsert ] then
                Post;

             {if ChangeCount > 0 then
                oDeltaCedula := Delta
             else
                 SetOLEVariantToNull( oDeltaCedula );}

             if ( ChangeCount > 0 ) or ( cdsScrap.ChangeCount > 0 ) then
             begin

                  Reconcile( ServerLabor.GrabaCedulaScrap( dmCliente.Empresa,
                                                           DeltaNull, {oDeltaCedula,}
                                                           GetScrap,
                                                           FieldByName( 'CS_FOLIO' ).AsInteger,
                                                           ErrorCount ) );
                  if ( ErrorCount = 0 ) then
                  begin
                       TressShell.SetDataChange( [ enCedulaScrap ] );
                       cdsScrap.MergeChangeLog;    // Se aplicaron los cambios -> Limpiar el Delta
                       cdsScrap.First;
                  end;
             end;
        end;
     finally
            cdsScrap.EnableControls;
     end;
end;

procedure TdmLabor.cdsCedScrapAlModificar(Sender: TObject);
begin
     EditarCedulaScrap;
end;

procedure TdmLabor.cdsCedScrapBeforePost(DataSet: TDataSet);
 procedure ValidaCodigo( const sField: string; const iGlobal : integer );
  var sGlobal : string;
 begin
      sGlobal := Global.GetGlobalString( iGlobal );
      if StrLleno( sGLobal ) then
      begin
           with cdsCedScrap do
                if StrVacio( FieldByName( sField ).AsString ) then
                   DataBaseError( Format( 'El c�digo de %s no puede quedar vacio', [ sGlobal ] ) );
      end;
 end;
begin
     with cdsCedScrap do
     begin
          ValidaCodigo( 'CS_AREA', K_GLOBAL_LABOR_AREA );
          FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
     end;
end;

procedure TdmLabor.cdsCedScrapNewRecord(DataSet: TDataSet);
begin
     with cdsCedScrap do
     begin
          FieldByName('CS_FECHA').AsDateTime := Date;
          FieldByName('CS_FEC_FAB').AsDateTime := Date;
          FieldByName('CS_HORA').AsString := FormatDateTime( 'hhmm', Time );
     end;
end;

procedure TdmLabor.cdsScrapAfterOpen(DataSet: TDataSet);
begin
     cdsScrap.FieldByName( 'CN_CODIGO' ).OnValidate := cdsScrapCN_CODIGOValidate;
     cdsScrap.FieldByName( 'SC_MOTIVO' ).OnValidate := cdsScrapSC_MOTIVOValidate;
end;

procedure TdmLabor.cdsScrapSC_MOTIVOValidate(Sender: TField);
const
    sMensaje = 'No existe el c�digo: %s en cat�logo de motivos de scrap';
var
   sValor, sDescripcion : string;
begin
     sValor := Sender.AsString;
     if ( sValor <> VACIO ) then
     begin
          with cdsMotivoScrap do
               if NOT LookUpKey( sValor, Vacio, sDescripcion )  then
                  DataBaseError( Format( sMensaje, [ sValor ] ) );
     end;
end;

procedure TdmLabor.cdsScrapCN_CODIGOValidate(Sender: TField);
const
    sMensaje = 'No existe el c�digo: %s en cat�logo de componentes';
var
   sValor, sDescripcion : string;
begin
     sValor := Sender.AsString;
     if ( sValor <> VACIO ) then
     begin
          with cdsComponentes do
               if NOT LookUpKey( sValor, Vacio, sDescripcion )  then
                  DataBaseError( Format( sMensaje, [ sValor ] ) );
     end;
end;

procedure TdmLabor.cdsScrapAlCrearCampos(Sender: TObject);
begin
     cdsComponentes.Conectar;
     with cdsScrap do
     begin
          CreateSimpleLookUp( cdsComponentes, 'CN_NOMBRE', 'CN_CODIGO' );
          MaskNumerico( 'SC_PIEZAS', '#,0;-#,0;#' );
     end;
end;

procedure TdmLabor.cdsScrapBeforePost(DataSet: TDataSet);
begin
     with cdsScrap.FieldByName('CN_CODIGO') do
     begin
          if IsNull and StrVacio( AsString ) then
             DB.DatabaseError( 'C�digo no puede quedar vac�o' );
     end;
     with cdsScrap.FieldByName('SC_MOTIVO') do
     begin
          if IsNull and StrVacio( AsString ) then
             DB.DatabaseError( 'El motivo no puede quedar vac�o' );
     end;
end;

procedure TdmLabor.cdsScrapNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName( 'CS_FOLIO' ).AsInteger := cdsCedScrap.FieldByName( 'CS_FOLIO' ).AsInteger;
          FieldByName( 'SC_FOLIO' ).AsInteger := 0;
     end;
end;

procedure TdmLabor.cdsScrapReconcileError(DataSet: TCustomClientDataSet;
  E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
    showMessage('asd');
end;

procedure TdmLabor.cdsHistorialCedScrapAlCrearCampos(Sender: TObject);
begin
     with cdsHistorialCedScrap do
     begin
          MaskTime( 'CS_HORA' );
          MaskNumerico( 'NumScrap', '#,0;-#,0;#' );
          with FieldByName( 'US_CODIGO' ) do
          begin
               OnGetText := US_CODIGOGetText;
               Alignment := taLeftJustify;
          end;
          MaskFecha( 'CS_FECHA' );
          MaskFecha( 'CS_FEC_FAB' );
     end;
end;

procedure TdmLabor.cdsHistorialCedScrapBeforeDelete(DataSet: TDataSet);
var
   ErrorCount: Integer;
begin
     with cdsHistorialCedScrap do
     begin
          Reconcile( ServerLabor.BorraCedulaScrap( dmCliente.Empresa,
                                                   FieldByName( 'CS_FOLIO' ).AsInteger,
                                                   ErrorCount ) );
     end;
end;

procedure TdmLabor.EditarCedulaScrap;
begin
     ShowEdicion( cdsCedScrap.Tag );
end;

procedure TdmLabor.AgregarCedulaScrap;
begin
     cdsArea.Conectar;
     RefrescaCedulasScrap(-1);
     with cdsCedScrap do
     begin
          Agregar;
     end;
end;

procedure TdmLabor.RefrescarCedulasSCrap( Parametros : TZetaParams );
begin
     cdsHistorialCedScrap.Data := ServerLabor.GetHistorialCedScrap( dmCliente.Empresa, Parametros.VarValues );
end;

procedure TdmLabor.RefrescaCedulasScrap( const iFolio: Integer );
var
   oScrap: OleVariant;
begin
     cdsCedScrap.Data := ServerLabor.GetCedulaScrap( dmCliente.Empresa, oScrap, iFolio );
     cdsScrap.Data := oScrap;
end;

function TdmLabor.PuedeModificarCedScrap : Boolean;
begin
     Result := ( cdsCedScrap.State = dsInsert ) OR
               MismoUsuario(cdsHistorialCedScrap) OR
               ZAccesosMgr.CheckDerecho( D_LAB_CEDULAS_SCRAP, K_DERECHO_SIST_KARDEX );
end;

procedure TdmLabor.DialogoErrorCedulaScrap;
begin
     ZetaDialogo.ZInformation( 'C�dula de scrap', Format( K_MSG_CED_SCRAP, ['Modificar'] ), 0 );
end;


procedure TdmLabor.cdsEditComponentesAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsEditComponentes do
     begin
          if State in [ dsInsert, dsEdit ] then
             Post;

          if ( ChangeCount > 0 ) then
             Reconcile( ServerLabor.GrabaComponente( dmCliente.Empresa,
                                                     Delta,
                                                     ErrorCount ) );
          //TressShell.SetDataChange( [ enComponentes ] );
     end;
     if ( ErrorCount = 0 ) then
        cdsComponentes.Refrescar;
end;

procedure TdmLabor.cdsEditComponentesBeforePost(DataSet: TDataSet);
begin
    if DataSet.Fields[0].IsNull then
        DB.DatabaseError( 'C�digo no puede quedar vac�o' );
end;

procedure TdmLabor.cdsEditComponentesAlAdquirirDatos(Sender: TObject);
begin
     with cdsComponentes do
     begin
          cdsEditComponentes.Data := ServerLabor.GetComponente( dmCliente.Empresa, FieldByName('CN_CODIGO').AsString );
     end;
end;

procedure TdmLabor.cdsEditComponentesAfterDelete(DataSet: TDataSet);
begin
     cdsEditcomponentes.Enviar;
end;

procedure TdmLabor.cdsComponentesAlAgregar(Sender: TObject);
begin
     with cdsEditComponentes do
     begin
          Conectar;
          Agregar;
     end;
     ShowEdicion( cdsComponentes.Tag )
end;

procedure TdmLabor.cdsComponentesAlModificar(Sender: TObject);
begin
     cdsEditComponentes.Refrescar;
     ShowEdicion( cdsComponentes.Tag );
end;


procedure TdmLabor.cdsCedScrapHuboCambios(DataSet: TDataSet);
begin
     with cdsCedScrap do
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
end;


procedure TdmLabor.cdsMaquinasAlCrearCampos(Sender: TObject);
begin
     cdsTMaquinas.Conectar;

     with TZetaClientDataSet( Sender ) do
     begin
          CreateSimpleLookUp( cdsTMaquinas, 'MQ_TMAQUIN_TXT', 'MQ_TMAQUIN' );
     end;
end;

procedure TdmLabor.cdsMaqCertificAlAdquirirDatos(Sender: TObject);
begin
     cdsMaqCertific.Data := ServerLabor.GetCertificaciones(dmCliente.Empresa,cdsMaquinas.FieldByName('MQ_CODIGO').AsString );
end;

procedure TdmLabor.cdsMaqCertificAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsMaqCertific do
     begin
          if State in [ dsInsert, dsEdit ] then
             Post;

          if ( ChangeCount > 0 ) then
             Reconcile( ServerLabor.GrabaCertificaciones( dmCliente.Empresa,
                                                     Delta,'',
                                                     ErrorCount ) );
          Refrescar;
     end;

end;

procedure TdmLabor.cdsMaqCertificAlCrearCampos(Sender: TObject);
begin
     dmCatalogos.cdsCertificaciones.Conectar;
     with TZetaClientDataSet( Sender ) do
     begin
          CreateSimpleLookUp( dmCatalogos.cdsCertificaciones, 'CI_DESCRIP', 'CI_CODIGO' );
     end;
     cdsMaqCertific.FieldByName( 'CI_CODIGO' ).OnValidate := cdsMaqCertificCI_CODIGOValidate;
end;

procedure TdmLabor.cdsMaqCertificAlAgregar(Sender: TObject);
begin
      with cdsMaquinas do
           if ( not ( State in [ dsEdit, dsInsert ] ) ) then
               Edit;
end;

procedure TdmLabor.cdsMaqCertificBeforePost(DataSet: TDataSet);
begin
     if cdsMaqCertific.FieldByName('CI_CODIGO').AsString = VACIO then
        DataBaseError('Certificaci�n no puede quedar vac�o');
end;

procedure TdmLabor.cdsMaqCertificCI_CODIGOValidate(Sender: TField);
const
    sMensaje = 'No existe el c�digo: %s en cat�logo de certificaciones';
var
   sValor, sDescripcion : string;
begin
     sValor := Sender.AsString;
     if ( sValor <> VACIO ) then
     begin
          with dmCatalogos.cdsCertificaciones do
               if NOT LookUpKey( sValor, Vacio, sDescripcion )  then
                  DataBaseError( Format( sMensaje, [ sValor ] ) );
     end;
end;

procedure TdmLabor.cdsLayoutsBeforePost(DataSet: TDataSet);
begin
     if DataSet.FieldByName('LY_CODIGO').AsString = VACIO then
      DataBaseError('C�digo no puede quedar vac�o');
     if DataSet.FieldByName('LY_NOMBRE').AsString = VACIO then
      DataBaseError('Nombre no puede quedar vac�o');
end;

procedure TdmLabor.cdsLayoutsAlCrearCampos(Sender: TObject);
begin
     cdsArea.Conectar;
     with TZetaClientDataSet( Sender ) do
     begin
          CreateSimpleLookUp( dmLabor.cdsArea, 'AR_CODIGO_TXT', 'LY_AREA' );
     end;
end;

procedure TdmLabor.cdsLayMaquinasAlAdquirirDatos(Sender: TObject);
var
   oSillas,oSillasMaq,oCerMaq:Olevariant;
begin
     cdsLayMaquinas.Data := ServerLabor.GetMapa(dmCliente.Empresa,cdsLayouts.FieldByName('LY_CODIGO').AsString,oSillas,oSillasMaq,oCerMaq );
     cdsSillasMaquina.Data := oSillasMaq;
     cdsSillasLayout.Data := oSillas;
end;

procedure TdmLabor.cdsLayMaquinasAlEnviarDatos(Sender: TObject);
var
   ErrorCount:Integer;
begin
     ErrorCount := 0;
     with cdsLayMaquinas do
     begin
          if State in [ dsInsert, dsEdit ] then
             Post;

          if ( ChangeCount > 0 ) then
             Reconcile( ServerLabor.GrabaCatalogo(dmCliente.Empresa,Tag,Delta,ErrorCount));
     end;
end;

procedure TdmLabor.cdsSillasLayoutAlEnviarDatos(Sender: TObject);
var
   ErrorCount:Integer;
begin
     ErrorCount := 0;
     with cdsSillasLayout do
     begin
          if State in [ dsInsert, dsEdit ] then
             Post;
          if ( ChangeCount > 0 ) then
             Reconcile( ServerLabor.GrabaCatalogo(dmCliente.Empresa,Tag,Delta,ErrorCount));
     end;
end;

procedure TdmLabor.cdsSillasMaquinaAlEnviarDatos(Sender: TObject);
var
   ErrorCount:Integer;
begin
     ErrorCount := 0;
     with cdsSillasMaquina do
     begin
          if State in [ dsInsert, dsEdit ] then
             Post;
          if ( ChangeCount > 0 ) then
             Reconcile( ServerLabor.GrabaCatalogo(dmCliente.Empresa,Tag,Delta,ErrorCount));
     end;
end;

procedure TdmLabor.cdsKarEmpMaquinasAlAdquirirDatos(Sender: TObject);
begin
     cdsKarEmpMaquinas.Data := ServerLabor.GetKardexEmpleadoMaquina(dmCliente.Empresa,dmCliente.Empleado );
end;

procedure TdmLabor.cdsKarEmpMaquinasAlCrearCampos(Sender: TObject);
begin
     cdsMaquinas.Conectar;
     cdsLayouts.Conectar;
     with TZetaClientDataSet( Sender ) do
     begin
          MaskHoras( 'EM_HORA' );
          MaskHoras( 'EM_HOR_MOV' );
          with FieldByName( 'EM_HORA' ) do
          begin
               OnSetText := HoraSetText;
               OnGetText := HoraGetText;
          end;
          with FieldByName( 'EM_HOR_MOV' ) do
          begin
               OnSetText := HoraSetText;
               OnGetText := HoraGetText;
          end;
          CreateSimpleLookUp( cdsMaquinas, 'MQ_NOMBRE', 'MQ_CODIGO' );
          CreateSimpleLookUp( cdsLayouts, 'LY_DESCRIP', 'LY_CODIGO' );
     end;     
end;

procedure TdmLabor.cdsKarEmpMaquinasNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          with dmCliente do
          begin
               FieldByName( 'CB_CODIGO' ).AsInteger := Empleado;
               FieldByName( 'EM_FECHA' ).AsDateTime := Fecha;
               FieldByName( 'EM_HORA' ).AsString := FormatDateTime( 'hhmm', Now );
          end;
     end
end;

function TdmLabor.GetMaquinaGlobal:string;
begin
     Result := Global.GetGlobalString( K_GLOBAL_LABOR_MAQUINA );
end;

function TdmLabor.GetLayoutGlobal:string;
begin
     Result := Global.GetGlobalString( K_GLOBAL_LABOR_PLANTILLA );
end;


procedure TdmLabor.cdsTMaquinasBeforePost(DataSet: TDataSet);
begin
     if DataSet.FieldByName('TB_CODIGO').AsString = VACIO then
        DataBaseError('C�digo no puede quedar vac�o');

     if DataSet.FieldByName('TB_ELEMENT').AsString = VACIO then
        DataBaseError('Descripci�n no puede quedar vac�a');

end;

procedure TdmLabor.cdsMaquinasBeforePost(DataSet: TDataSet);
begin
      if DataSet.FieldByName('MQ_CODIGO').AsString = VACIO then
        DataBaseError('C�digo no puede quedar vac�o');

     if DataSet.FieldByName('MQ_NOMBRE').AsString = VACIO then
        DataBaseError('Nombre no puede quedar vac�o');

end;

procedure TdmLabor.cdsMaquinasNewRecord(DataSet: TDataSet);
begin
     cdsMaquinas.FieldByName('MQ_MULTIP').AsString := K_GLOBAL_NO;
     cdsMaqCertific.Refrescar;
end;

procedure TdmLabor.cdsMaqCertificNewRecord(DataSet: TDataSet);
begin
     cdsMaqCertific.FieldByName('MQ_CODIGO').AsString :=  cdsMaquinas.FieldByName('MQ_CODIGO').AsString;
     cdsMaqCertific.FieldByName('MC_FECHA').AsDateTime := Now;
     cdsMaqCertific.FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;
     
end;

procedure TdmLabor.cdsBreaksBeforeDelete(DataSet: TDataSet);
begin
     with cdsBrkHora do
     begin
          first;
          while not eof do
          begin
               delete;
          end;
     end;
end;

procedure TdmLabor.cdsBreaksBeforePost(DataSet: TDataSet);
begin
  if StrVacio( cdsBreaks.FieldByName('BR_CODIGO').AsString ) then
      DataBaseError('C�digo no puede quedar vac�o');
end;

end.
