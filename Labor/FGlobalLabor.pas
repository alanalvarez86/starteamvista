unit FGlobalLabor;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, ComCtrls, Mask,
     ZBaseGlobal_DevEx,
     ZetaHora,
     ZetaKeyLookup,
     ZetaFecha,
     //:Todo ZBaseGlobal,
     cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, dxSkinsCore, TressMorado2013, Vcl.ImgList, cxButtons,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxControls, cxPC, ZetaKeyLookup_DevEx,
  cxContainer, cxEdit, cxCheckBox;

type
  TGlobalLabor = class(TBaseGlobal_DevEx)
    PageControl: TcxPageControl;
    Catalogo: TcxTabSheet;
    Generales: TcxTabSheet;
    gbTextos: TGroupBox;
    TiempoLbl: TLabel;
    PiezasLbl: TLabel;
    Tiempo: TEdit;
    Piezas: TEdit;
    gbOperaciones: TGroupBox;
    DefSteps: TCheckBox;
    gbOrdenTrabajo: TGroupBox;
    ValidaWOrder: TCheckBox;
    GroupBox4: TGroupBox;
    Modula1Lbl: TLabel;
    Modula2Lbl: TLabel;
    Modula3Lbl: TLabel;
    Modula1: TEdit;
    Modula2: TEdit;
    Modula3: TEdit;
    Textos: TGroupBox;
    WOrderLbl: TLabel;
    ParteLbl: TLabel;
    OperacionLbl: TLabel;
    AreaLbl: TLabel;
    TMuertoLbl: TLabel;
    WOrder: TEdit;
    WOrders: TEdit;
    Parte: TEdit;
    Partes: TEdit;
    Operacion: TEdit;
    Operaciones: TEdit;
    Area: TEdit;
    Areas: TEdit;
    TMuerto: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    GBNoRegistrado: TGroupBox;
    TMuertoNR: TZetaKeyLookup_DevEx;
    gbMultilotes: TGroupBox;
    Multilotes: TCheckBox;
    TDefectoLbl: TLabel;
    TDefecto: TEdit;
    PanelFechaCorte: TPanel;
    gbFechaCorte: TGroupBox;
    FechaCorteLBL: TLabel;
    FechaCorte: TZetaFecha;
    ckFechaCorte: TCheckBox;
    Label1: TLabel;
    TMaquina: TEdit;
    TMaquinas: TEdit;
    TDefectos: TEdit;
    TPlantilla: TEdit;
    Label2: TLabel;
    TPlantillas: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure ControlEditChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TMuertoExit(Sender: TObject);
    procedure ckFechaCorteClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    //procedure OrdenChange(Sender: TObject);
    //procedure ParteChange(Sender: TObject);
    //procedure OperacionChange(Sender: TObject);
    //procedure AreaChange(Sender: TObject);
  private
    { Private declarations }
    procedure InitControles;
    procedure SetControlEnabled( const lEnabled: Boolean; const iTag: Integer );
  public
    { Public declarations }
    procedure Descargar; override;
  end;

const
     LABEL_TMUERTO_NO_REGISTRADO = '%s - &No registrado';
var
  GlobalLabor: TGlobalLabor;

implementation

uses DLabor,
     ZAccesosTress,
     ZGlobalTress,
     ZetaDialogo,
     ZetaCommonClasses,
     ZetaCommonTools,
     FTressShell;

{$R *.DFM}

procedure TGlobalLabor.FormCreate(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage := Generales;
     IndexDerechos    := ZAccesosTress.D_LAB_CAT_GLOBAL;
     HelpContext      := H9516_Catalogo_Globales;
     ActualizaDiccion := TRUE;

     WOrder.Tag       := K_GLOBAL_LABOR_ORDEN;
     WOrders.Tag      := K_GLOBAL_LABOR_ORDENES;
     Parte.Tag        := K_GLOBAL_LABOR_PARTE;
     Partes.Tag       := K_GLOBAL_LABOR_PARTES;
     Operacion.Tag    := K_GLOBAL_LABOR_OPERACION;
     Operaciones.Tag  := K_GLOBAL_LABOR_OPERACIONES;
     Area.Tag         := K_GLOBAL_LABOR_AREA;
     Areas.Tag        := K_GLOBAL_LABOR_AREAS;
     TDefecto.Tag     := K_GLOBAL_LABOR_TDEFECTO;
     TDefectos.Tag    := K_GLOBAL_LABOR_TDEFECTOS;

     TMuerto.Tag      := K_GLOBAL_LABOR_TMUERTO;

     Modula1.Tag      := K_GLOBAL_LABOR_MODULA1;
     Modula2.Tag      := K_GLOBAL_LABOR_MODULA2;
     Modula3.Tag      := K_GLOBAL_LABOR_MODULA3;

     DefSteps.Tag     := K_GLOBAL_LABOR_USA_DEFSTEPS;
     ValidaWOrder.Tag := K_GLOBAL_LABOR_VALIDASTEPS;
     Multilotes.Tag   := K_GLOBAL_LABOR_MULTILOTES;

     Tiempo.Tag       := K_GLOBAL_LABOR_TIEMPO;
     Piezas.Tag       := K_GLOBAL_LABOR_PIEZAS;

     ckFechaCorte.Tag := K_GLOBAL_LABOR_VALIDA_FECHACORTE;
     FechaCorte.Tag   := K_GLOBAL_LABOR_FECHACORTE;

     TMaquina.Tag     := K_GLOBAL_LABOR_MAQUINA;
     TMaquinas.Tag    := K_GLOBAL_LABOR_MAQUINAS;

     TPlantilla.Tag     := K_GLOBAL_LABOR_PLANTILLA;
     TPlantillas.Tag    := K_GLOBAL_LABOR_PLANTILLAS;

     with TMuertoNR do
     begin
          Tag := K_GLOBAL_LABOR_TIPO_TMUERTO;
          LookupDataset := dmLabor.cdsTiempoMuerto;
     end;
     FechaCorte.Valor := Now;
end;

procedure TGlobalLabor.FormShow(Sender: TObject);
begin
     dmLabor.cdsTiempoMuerto.Conectar;
     inherited;
     InitControles;
{
     WOrders.Enabled := StrLleno( WOrder.Text );
     Partes.Enabled := StrLleno( Parte.Text );
     Operaciones.Enabled := StrLleno( Operacion.Text );
     Areas.Enabled := StrLleno( Area.Text );
}
end;

procedure TGlobalLabor.DesCargar;
const
     K_MESS_ERROR = '%s No Debe Quedar Vac�o';

   procedure ReportaError( const sMensaje: String; oControl: TWinControl );
   begin
        ZetaDialogo.ZError( Caption, sMensaje, 0 );
        PageControl.ActivePage := Catalogo;
        oControl.SetFocus;
   end;

begin
     if StrVacio( Area.Text ) then
        ReportaError( Format( K_MESS_ERROR, [ 'Area' ] ), Area )
     else
         if StrVacio( TMuerto.Text ) then
            ReportaError( Format( K_MESS_ERROR, [ 'Tiempo Muerto' ] ), TMuerto )
         else
             inherited Descargar;        // Procede a Grabar y poner el LastAction en Modificaci�n
end;

procedure TGlobalLabor.InitControles;
begin
     SetControlEnabled( StrLleno( WOrder.Text ), WOrder.Tag );
     SetControlEnabled( StrLleno( Parte.Text ), Parte.Tag );
     SetControlEnabled( StrLleno( Operacion.Text ), Operacion.Tag );
     SetControlEnabled( StrLleno( Area.Text ), Area.Tag );
     SetControlEnabled( StrLleno( TDefecto.Text ), TDefecto.Tag );
     SetControlEnabled( StrLleno( TMaquina.Text ), TMaquina.Tag );
     SetControlEnabled( StrLleno( TPlantilla.Text ), TPlantilla.Tag );

     TMuertoExit( self );
end;

procedure TGlobalLabor.OKClick(Sender: TObject);
begin
  inherited;
  //TressShell.DevEx_NavBar.Refresh;
end;

procedure TGlobalLabor.OK_DevExClick(Sender: TObject);
begin
  inherited;
  TressShell.DoOpenAll;
end;

procedure TGlobalLabor.SetControlEnabled( const lEnabled: Boolean; const iTag: Integer );
begin
     case iTag of
          K_GLOBAL_LABOR_ORDEN : WOrders.Enabled := lEnabled;
          K_GLOBAL_LABOR_PARTE : Partes.Enabled := lEnabled;
          K_GLOBAL_LABOR_OPERACION : Operaciones.Enabled := lEnabled;
          K_GLOBAL_LABOR_AREA : Areas.Enabled := lEnabled;
          K_GLOBAL_LABOR_TDEFECTO : TDefectos.Enabled := lEnabled;
          K_GLOBAL_LABOR_MAQUINA : TMaquinas.Enabled := lEnabled;
          K_GLOBAL_LABOR_PLANTILLA : TPlantillas.Enabled := lEnabled;

     end;
end;

procedure TGlobalLabor.ControlEditChange(Sender: TObject);
begin
     inherited;
     with Sender as TEdit do
     begin
          SetControlEnabled( StrLleno( Text ), Tag );
     end;
end;

procedure TGlobalLabor.TMuertoExit(Sender: TObject);
begin
     inherited;
     GBNoRegistrado.Caption := Format( LABEL_TMUERTO_NO_REGISTRADO, [ TMuerto.Text ] );
end;

{
procedure TGlobalLabor.OrdenChange(Sender: TObject);
begin
     inherited;
     WOrders.Enabled := StrLleno( WOrder.Text );
end;

procedure TGlobalLabor.ParteChange(Sender: TObject);
begin
     inherited;
     Partes.Enabled := StrLleno( Parte.Text );
end;

procedure TGlobalLabor.OperacionChange(Sender: TObject);
begin
     inherited;
     Operaciones.Enabled := StrLleno( Operacion.Text );
end;

procedure TGlobalLabor.AreaChange(Sender: TObject);
begin
     inherited;
     Areas.Enabled := StrLleno( Area.Text );
end;
}

procedure TGlobalLabor.ckFechaCorteClick(Sender: TObject);
begin
     inherited;
     FechaCorteLBL.Enabled := ckFechaCorte.Checked;
     FechaCorte.Enabled := FechaCorteLBL.Enabled;
end;

end.

