unit ZetaBuscaCedula;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, Grids, DBGrids,
     ExtCtrls, Db, Mask,
     ZetaMessages,
     ZetaDBGrid,
     ZetaCommonClasses,
     ZetaClientDataset,
     ZetaEdit,
     ZetaKeyLookup,
     ZetaFecha;

type
  TBuscaCedulas = class(TForm)
    PanelControles: TPanel;
    BuscarBtn: TBitBtn;
    AceptarBtn: TBitBtn;
    CancelarBtn: TBitBtn;
    DataSource: TDataSource;
    Status: TRadioGroup;
    Tipo: TRadioGroup;
    Filtros: TGroupBox;
    lbInicio: TLabel;
    lbFin: TLabel;
    FechaFinal: TZetaFecha;
    FechaInicial: TZetaFecha;
    CBFecha: TCheckBox;
    WO_NUMBER: TZetaKeyLookup;
    AR_CODIGO: TZetaKeyLookup;
    OP_NUMBER: TZetaKeyLookup;
    CE_AREA: TZetaKeyLookup;
    US_CODIGO: TZetaKeyLookup;
    lbUsuario: TLabel;
    lbArea: TLabel;
    lbOpera: TLabel;
    lbParte: TLabel;
    lbWorder: TLabel;
    Grid: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BuscarBtnClick(Sender: TObject);
    procedure AceptarBtnClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure CBFechaClick(Sender: TObject);
  private
    { Private declarations }
    FFiltro: String;
    FCodigoCedulas: String;
    FDescripcion: String;
    FCapturando: Boolean;
    FParametros : TZetaParams;
    function LookupDataset: TZetaClientDataset;
    procedure Buscar;
    procedure SetNumero;
    procedure SetBotones( const Vacio : Boolean );
    procedure ShowGrid( const lVisible: Boolean );
    procedure Connect;
    procedure Disconnect;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    procedure AsignaColumnas;
  protected
    { Protected declarations }
  public
    { Public declarations }
    property CodigoCedulas: String read FCodigoCedulas;
    property Descripcion: String read FDescripcion;
    property Filtro: String read FFiltro write FFiltro;
  end;

procedure SetOnlyActivos( const lActivos: Boolean );

var
  BuscaCedulas: TBuscaCedulas;

function BuscaCedulasDialogo( const sFilter: String; var sKey, sDescription: String ): Boolean;

implementation

uses ZetaDialogo,
     ZetaCommonTools,
     DGlobal,
     ZGlobalTress,
     ZetaLaborTools,
     ZetaClientTools,
     DCliente,
     DLabor, DSistema;

var
   FShowOnlyActivos: Boolean;

{$R *.DFM}

procedure SetOnlyActivos( const lActivos: Boolean );
begin
     FShowOnlyActivos := lActivos;
end;

function BuscaCedulasDialogo( const sFilter: String; var sKey, sDescription: String ): Boolean;
begin
     Result := False;
     if ( BuscaCedulas = nil ) then
        BuscaCedulas := TBuscaCedulas.Create( Application.MainForm );
     if ( BuscaCedulas <> nil ) then
     begin
          with BuscaCedulas do
          begin
               ShowModal;
               if ( ModalResult = mrOk ) and ( CodigoCedulas <> '' ) then
               begin
                    sKey := CodigoCedulas;
                    sDescription := Descripcion;
                    Result := True;
               end;
          end;
     end;
end;

{************** TBuscaEmpleado ************** }

procedure TBuscaCedulas.FormCreate(Sender: TObject);
begin
     //HelpContext := H00012_Busqueda_empleados;
     FParametros := TZetaParams.Create;
     FechaInicial.Valor := Date;
     FechaFinal.Valor := Date;
end;

procedure TBuscaCedulas.FormShow(Sender: TObject);
begin
     WindowState := wsNormal;
     ActiveControl := Status;
     Connect;
     AsignaColumnas;
end;

procedure TBuscaCedulas.AsignaColumnas;
begin
     with Grid do
     begin
          AsignaGlobal( K_GLOBAL_LABOR_ORDEN, lbWorder, WO_NUMBER );
          AsignaGlobal( K_GLOBAL_LABOR_PARTE, lbparte, AR_CODIGO );
          AsignaGlobal( K_GLOBAL_LABOR_AREA, lbArea, CE_AREA );
          AsignaGlobal( K_GLOBAL_LABOR_OPERACION, lbOpera, OP_NUMBER );

          AsignaGlobalColumn( K_GLOBAL_LABOR_ORDEN, Columns[ GetFieldColumnIndex( 'WO_NUMBER' ) ] );
          AsignaGlobalColumn( K_GLOBAL_LABOR_PARTE, Columns[ GetFieldColumnIndex( 'AR_CODIGO' ) ] );
          AsignaGlobalColumn( K_GLOBAL_LABOR_PIEZAS, Columns[ GetFieldColumnIndex( 'CE_PIEZAS' ) ] );
          AsignaGlobalColumn( K_GLOBAL_LABOR_OPERACION, Columns[ GetFieldColumnIndex( 'OP_NUMBER' ) ] );
          AsignaGlobalColumn( K_GLOBAL_LABOR_AREA, Columns[ GetFieldColumnIndex( 'CE_AREA' ) ] );
          AsignaGlobalColumn( K_GLOBAL_LABOR_TMUERTO, Columns[ GetFieldColumnIndex( 'CE_TMUERTO' ) ] );
          AsignaGlobalColumn( K_GLOBAL_LABOR_MODULA1, Columns[ GetFieldColumnIndex( 'CE_MOD1' ) ] );
          AsignaGlobalColumn( K_GLOBAL_LABOR_MODULA2, Columns[ GetFieldColumnIndex( 'CE_MOD2' ) ] );
          AsignaGlobalColumn( K_GLOBAL_LABOR_MODULA3, Columns[ GetFieldColumnIndex( 'CE_MOD3' ) ] );
     end;
end;


procedure TBuscaCedulas.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Disconnect;
     Action := caHide;
end;

function TBuscaCedulas.LookupDataset: TZetaClientDataset;
begin
     Result := dmLabor.cdsCedulaLookup;
end;


procedure TBuscaCedulas.Connect;
begin
     ShowGrid( False );
     SetBotones( True );
     ActiveControl := Status;
     Wo_number.Llave := Vacio;
     Ar_codigo.Llave := Vacio;
     Op_number.Llave := Vacio;
     Ce_area.Llave := Vacio;
     Us_codigo.Llave := Vacio;

     Status.ItemIndex := 0;
     Tipo.ItemIndex := 0;

     dmSistema.cdsUsuarios.Conectar;
     with dmLabor do
     begin
          cdsPartes.Conectar;
          cdsOpera.Conectar;
          cdsArea.Conectar;
          cdsTiempoMuerto.Conectar;
          DataSource.DataSet := LookupDataset;
     end;
end;

procedure TBuscaCedulas.Disconnect;
begin
     Datasource.Dataset := nil;
end;

procedure TBuscaCedulas.Buscar;
var
   oCursor: TCursor;
begin
     AceptarBtn.Enabled := False;
     Application.ProcessMessages;
     oCursor := Screen.Cursor;
     try
        Screen.Cursor := crHourglass;
        with LookupDataset do
        begin
             try
                DisableControls;
                Active := False;
                with FParametros do
                begin
                     AddInteger( 'Status', Status.ItemIndex - 1 );
                     AddInteger( 'Tipo', Tipo.ItemIndex - 1 );

                     AddString( 'Parte', AR_CODIGO.Llave );
                     AddString( 'Operacion', OP_NUMBER.Llave );
                     AddString( 'Orden', WO_NUMBER.Llave );
                     AddString( 'Area', CE_AREA.Llave );
                     AddString( 'Cedula', Vacio );
                     AddInteger( 'Usuario', US_CODIGO.Valor );


                     if CBFecha.Checked then
                     begin
                          AddDate( 'FechaInicial', FechaInicial.Valor );
                          AddDate( 'FechaFinal', FechaFinal.Valor );
                     end
                     else
                     begin
                          AddDate( 'FechaInicial', NullDateTime );
                          AddDate( 'FechaFinal', NullDateTime );
                     end;
                end;
                dmLabor.RefrescarLookupCedula( FParametros );
             finally
                    EnableControls;
             end;
             if IsEmpty then
             begin
                  zWarning( 'B�squeda de C�dulas', '� No Hay C�dulas Con Estos Datos !', 0, mbOK );
                  SetBotones( True );
                  ActiveControl := Status;
             end
             else
             begin
                  ShowGrid( True );
                  SetBotones( False );
                  ActiveControl := Grid;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TBuscaCedulas.SetNumero;
begin
     with LookupDataset do
     begin
          FCodigoCedulas := FieldByName( 'CE_FOLIO' ).AsString;
          FDescripcion := FieldByName( 'CE_COMENTA' ).AsString;
     end;
     ModalResult := mrOK;
end;

procedure TBuscaCedulas.SetBotones( const Vacio: Boolean );
begin
     FCapturando := Vacio;
     BuscarBtn.Default := Vacio;
     with AceptarBtn do
     begin
          Default := not Vacio;
          Enabled := not Vacio;
     end;
end;

procedure TBuscaCedulas.ShowGrid( const lVisible: Boolean );
const
     GRID_HEIGHT = 150;
     FORM_HEIGHT = 228;
     //HEIGHT_OFFSET = 25;
begin
     with Grid do
     begin
          if lVisible then
          begin
               Visible:= True;
               Height := ZetaClientTools.GetScaledHeight( GRID_HEIGHT );
               Self.Height := ZetaClientTools.GetScaledHeight( FORM_HEIGHT + GRID_HEIGHT );
          end
          else
          begin
               Visible:= False;
               Height := 0;
               Self.Height := ZetaClientTools.GetScaledHeight( FORM_HEIGHT );
          end;
     end;
     Repaint;
end;

procedure TBuscaCedulas.WMExaminar(var Message: TMessage);
begin
     AceptarBtn.Click;
end;

procedure TBuscaCedulas.BuscarBtnClick(Sender: TObject);
begin
     Buscar;
end;

procedure TBuscaCedulas.AceptarBtnClick(Sender: TObject);
begin
     if not LookupDataset.IsEmpty then
        SetNumero;
end;

procedure TBuscaCedulas.FormDestroy(Sender: TObject);
begin
     FParametros.Free;
end;

procedure TBuscaCedulas.CBFechaClick(Sender: TObject);
 var lEnabled : Boolean;
begin
     lEnabled := CbFecha.Checked;
     lbInicio.Enabled := lEnabled;
     lbFin.Enabled := lEnabled;
     FechaInicial.Enabled := lEnabled;
     FechaFinal.Enabled := lEnabled;
end;

end.
