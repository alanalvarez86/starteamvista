unit FEditSuperArea;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx, StdCtrls, Buttons, ExtCtrls, ZetaSmartLists,
  ZetaDBTextBox, Db, ZBaseDlgModal, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, TressMorado2013, Vcl.ImgList,
  cxButtons;

type
  TEditSupArea = class(TZetaDlgModal)
    Panel1: TPanel;
    DataSource: TDataSource;
    DBCodigoLBL: TLabel;
    DBDescripcionLBL: TLabel;
    TB_CODIGO: TZetaDBTextBox;
    TB_ELEMENT: TZetaDBTextBox;
    LblDisponibles: TLabel;
    LblSeleccionadas: TLabel;
    LBDisponibles: TZetaSmartListBox;
    LBSeleccion: TZetaSmartListBox;
    ZetaSmartListsButton1: TZetaSmartListsButton;
    ZetaSmartListsButton2: TZetaSmartListsButton;
    ZetaSmartListsButton3: TZetaSmartListsButton;
    ZetaSmartListsButton4: TZetaSmartListsButton;
    SmartListAreas: TZetaSmartLists;
    procedure FormShow(Sender: TObject);
    procedure SmartListAreasAlCambiar(Sender: TObject; var Objeto: TObject; Texto: String);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

const
     K_FORMA_CAPTION = 'Asignación de %s a %s';
     K_LISTA_DISP_CAPTION = '%s Disponibles:';
     K_LISTA_SELE_CAPTION = '%s Seleccionadas:';
var
  EditSupArea: TEditSupArea;

implementation

uses dLabor, dGlobal, ZetaLaborTools, ZGlobalTress;

{$R *.DFM}

procedure TEditSupArea.FormShow(Sender: TObject);
var
   sCaptionAreas: String;
begin
     inherited;
     sCaptionAreas := ZetaLaborTools.GetLaborLabel( K_GLOBAL_LABOR_AREAS );
     with Global do
          self.Caption := Format( K_FORMA_CAPTION, [ sCaptionAreas, NombreNivel( GetGlobalInteger(K_GLOBAL_NIVEL_SUPERVISOR) ) ] );
     LblDisponibles.Caption := Format( K_LISTA_DISP_CAPTION, [ sCaptionAreas ] );
     LblSeleccionadas.Caption := Format( K_LISTA_SELE_CAPTION, [ sCaptionAreas ] );
     with dmLabor do
     begin
          DataSource.DataSet := cdsSuper;
          CargarAreas( LBDisponibles.Items, LBSeleccion.Items );
     end;
     OK.Enabled := FALSE;
     ActiveControl := LBDisponibles;
end;

procedure TEditSupArea.SmartListAreasAlCambiar(Sender: TObject; var Objeto: TObject; Texto: String);
begin
     inherited;
     with OK do
     begin
          if not Enabled then
             Enabled := TRUE;
     end;
end;

procedure TEditSupArea.OKClick(Sender: TObject);
begin
     inherited;
     dmLabor.DescargarAreas( LBSeleccion.Items );
end;

end.
