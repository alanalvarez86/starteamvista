unit FEditComponentes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaEdit, Grids,
  DBGrids, ZetaDBGrid, Mask, ZetaNumero, ZetaDBTextBox, ComCtrls,
  ZetaKeyCombo, ZetaKeyLookup, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, TressMorado2013, cxControls,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, Vcl.ImgList,
  cxNavigator, cxDBNavigator, cxButtons;

type
  TEditComponentes = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    AH_TIPOLbl: TLabel;
    CN_CODIGO: TZetaDBEdit;
    LBLOP_NOMBRE: TLabel;
    CN_NOMBRE: TDBEdit;
    CN_DETALLE: TDBMemo;
    Label1: TLabel;
    CN_COSTO: TZetaDBNumero;
    Label5: TLabel;
    Label2: TLabel;
    CN_SHORT: TDBEdit;
    Label3: TLabel;
    Label7: TLabel;
    CN_INGLES: TDBEdit;
    Label8: TLabel;
    CN_BARCODE: TDBEdit;
    CN_UNIDAD: TDBEdit;
    Label4: TLabel;
    CN_NUMERO1: TZetaDBNumero;
    Label6: TLabel;
    CN_NUMERO2: TZetaDBNumero;
    Label9: TLabel;
    CN_TEXTO1: TDBEdit;
    Label10: TLabel;
    CN_TEXTO2: TDBEdit;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    procedure Connect;override;
    procedure DoLookup;override;
  end;

var
  EditComponentes: TEditComponentes;

implementation

uses DLabor,
     DTablas,
     DGlobal,
     ZGlobalTress,
     ZetaClientTools,
     ZetaCommonClasses,
     ZAccesosTress,
     ZetaBuscador_DevEx;

{$R *.DFM}

procedure TEditComponentes.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_LAB_CAT_COMPONENTES;
     FirstControl := CN_CODIGO;
     HelpContext := H00056_Componentes;
end;

procedure TEditComponentes.Connect;
begin
     with dmLabor do
     begin
          cdsEditComponentes.Conectar;
          DataSource.DataSet := cdsEditComponentes;
     end;
end;

procedure TEditComponentes.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Caption, 'CN_CODIGO', dmLabor.cdsComponentes );
end;

end.
