unit FEditLecturas;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask, ComCtrls,
     ZBaseEdicion_DevEx,
     ZetaEdit,
     ZetaKeyCombo,
     ZetaFecha,
     ZetaKeyLookup,
     ZetaNumero,
     ZetaHora, ZetaSmartLists, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, TressMorado2013, cxControls,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, Vcl.ImgList,
  cxNavigator, cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx,
  dxSkinscxPCPainter, dxBarBuiltInMenu, cxPC;

type
  TEditLecturas = class(TBaseEdicion_DevEx)
    PageControl: TcxPageControl;
    Generales: TcxTabSheet;
    Moduladores: TcxTabSheet;
    LX_WORDER: TZetaDBKeyLookup_DevEx;
    LX_OPERA: TZetaDBKeyLookup_DevEx;
    Label4: TLabel;
    lbOpera: TLabel;
    lbWOrder: TLabel;
    Label6: TLabel;
    lbModula1: TLabel;
    LX_MODULA1: TZetaDBKeyLookup_DevEx;
    LX_MODULA2: TZetaDBKeyLookup_DevEx;
    LX_TMUERTO: TZetaDBKeyLookup_DevEx;
    lbModula2: TLabel;
    lbModula3: TLabel;
    LX_MODULA3: TZetaDBKeyLookup_DevEx;
    LX_HORA: TZetaDBHora;
    LX_PIEZAS: TZetaDBNumero;
    Label19: TLabel;
    LX_STATUS: TZetaDBKeyCombo;
    LX_NUMERO: TZetaDBEdit;
    Label20: TLabel;
    Label21: TLabel;
    LX_LINX_ID: TZetaDBEdit;
    Label7: TLabel;
    LX_FOLIO: TZetaDBNumero;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Connect;override;
 end;

var
  EditLecturas: TEditLecturas;

implementation

uses DLabor,
     DGlobal,
     ZGlobalTress,
     ZetaLaborTools,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaCommonLists;

{$R *.DFM}

{ TEditLecturas }

procedure TEditLecturas.FormCreate(Sender: TObject);
begin
     inherited;

     AsignaGlobal( K_GLOBAL_LABOR_ORDEN, lbWOrder, LX_WORDER );
     AsignaGlobal( K_GLOBAL_LABOR_OPERACION, lbOpera, LX_OPERA );
     AsignaGlobal( K_GLOBAL_LABOR_MODULA1, lbModula1, LX_MODULA1 );
     AsignaGlobal( K_GLOBAL_LABOR_MODULA2, lbModula2, LX_MODULA2 );
     AsignaGlobal( K_GLOBAL_LABOR_MODULA3, lbModula3, LX_MODULA3 );

     HelpContext := H9503_Lecturas_del_empleado;
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stFecha;
     FirstControl := LX_HORA;

     Moduladores.TabVisible := StrLleno( lbModula1.Caption );

     LX_WORDER.LookupDataset := dmLabor.cdsWOrderLookup;
     LX_OPERA.LookupDataset := dmLabor.cdsOpera;
     LX_MODULA1.LookupDataset := dmLabor.cdsModula1;
     LX_MODULA2.LookupDataset := dmLabor.cdsModula2;
     LX_MODULA3.LookupDataset := dmLabor.cdsModula3;
     LX_TMUERTO.LookupDataset := dmLabor.cdsTiempoMuerto;
end;

procedure TEditLecturas.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage := Generales;
end;

procedure TEditLecturas.Connect;
begin
     with dmLabor do
     begin
          cdsOpera.Conectar;
          cdsModula1.Conectar;
          cdsModula2.Conectar;
          cdsModula3.Conectar;
          cdsTiempoMuerto.Conectar;
          cdsLecturas.Conectar;
          DataSource.DataSet := cdsLecturas;
     end;
end;

end.

