inherited CedulasOperaciones_DevEx: TCedulasOperaciones_DevEx
  Left = 261
  Top = 181
  Caption = 'Producci'#243'n por %s '
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    TabOrder = 2
    inherited OK_DevEx: TcxButton
      TabOrder = 1
    end
    inherited Cancelar_DevEx: TcxButton
      TabOrder = 2
      TabStop = True
    end
    inherited BtnTemp: TButton
      Enabled = False
      TabOrder = 3
      TabStop = False
    end
    inherited ListaEmpleados_DevEx: TcxButton
      Caption = ' &Lista de empleados'
      TabOrder = 0
    end
  end
  inherited PanelIdentifica: TPanel
    inherited ValorActivo2: TPanel
      inherited textoValorActivo2: TLabel
        ExplicitLeft = 82
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 4
  end
  inherited Panel1: TPanel
    Height = 205
    TabOrder = 3
    ExplicitHeight = 205
    inherited PageControl1: TcxPageControl
      Height = 203
      ExplicitHeight = 203
      ClientRectBottom = 201
      inherited Generales: TcxTabSheet
        ExplicitHeight = 174
        inherited Panel3: TPanel
          Height = 174
          ExplicitHeight = 174
          inherited PanelHora: TPanel
            Height = 72
            ExplicitHeight = 72
            inherited LblHora: TLabel
              Left = 99
              Top = 31
              ExplicitLeft = 99
              ExplicitTop = 31
            end
            inherited LblCantidad: TLabel
              Left = 80
              Top = 55
              ExplicitLeft = 80
              ExplicitTop = 55
            end
            inherited LblFecha: TLabel
              Top = 7
              ExplicitTop = 7
            end
            inherited CE_HORA: TZetaDBHora
              Left = 128
              Top = 27
              ExplicitLeft = 128
              ExplicitTop = 27
            end
            inherited CE_PIEZAS: TZetaDBNumero
              Left = 128
              Top = 51
              ExplicitLeft = 128
              ExplicitTop = 51
            end
            inherited CE_FECHA: TZetaDBFecha
              Top = 2
              ExplicitTop = 2
            end
          end
          inherited PanelOrden: TPanel
            Top = 72
            Height = 25
            ExplicitTop = 72
            ExplicitHeight = 25
            inherited LblOrden: TLabel
              Left = 43
              Top = 8
              Width = 82
              Caption = 'Orden de trabajo:'
              ExplicitLeft = 43
              ExplicitTop = 8
              ExplicitWidth = 82
            end
            inherited WO_NUMBER: TZetaDBKeyLookup_DevEx
              Top = 4
              ExplicitTop = 4
            end
            inherited MultiLote_DevEx: TcxButton
              Top = 4
              ExplicitTop = 4
            end
          end
          inherited PanelParte: TPanel
            Top = 97
            Height = 25
            ExplicitTop = 97
            ExplicitHeight = 25
            inherited LblParte: TLabel
              Top = 8
              ExplicitTop = 8
            end
            inherited AR_CODIGO: TZetaDBKeyLookup_DevEx
              Top = 4
              ExplicitTop = 4
            end
          end
          inherited PanelArea: TPanel
            Top = 122
            Height = 25
            ExplicitTop = 122
            ExplicitHeight = 25
            inherited LblArea: TLabel
              Top = 8
              ExplicitTop = 8
            end
            inherited CE_AREA: TZetaDBKeyLookup_DevEx
              Top = 4
              ExplicitTop = 4
            end
          end
          inherited PanelOpera: TPanel
            Top = 147
            Height = 25
            ExplicitTop = 147
            ExplicitHeight = 25
            inherited LblOpera: TLabel
              Top = 8
              ExplicitTop = 8
            end
            inherited OP_NUMBER: TZetaDBKeyLookup_DevEx
              Top = 4
              ExplicitTop = 4
            end
          end
        end
      end
      inherited Moduladores: TcxTabSheet
        ExplicitHeight = 174
        inherited Panel4: TPanel
          Top = 50
          Height = 25
          ExplicitTop = 50
          ExplicitHeight = 25
          inherited lbUsuario: TLabel
            Top = 8
            ExplicitTop = 8
          end
          inherited US_CODIGO: TZetaDBTextBox
            Top = 6
            ExplicitTop = 6
          end
        end
        inherited PanelStatus: TPanel
          Height = 25
          ExplicitHeight = 25
          inherited LblStatus: TLabel
            Top = 8
            ExplicitTop = 8
          end
          inherited CE_STATUS: TZetaDBKeyCombo
            Top = 4
            ExplicitTop = 4
          end
        end
        inherited PanelModula1: TPanel
          Top = 75
          Height = 25
          ExplicitTop = 75
          ExplicitHeight = 25
          inherited LblModula1: TLabel
            Top = 6
            ExplicitTop = 6
          end
          inherited CE_MOD_1: TZetaDBKeyLookup_DevEx
            Top = 2
            ExplicitTop = 2
          end
        end
        inherited PanelModula2: TPanel
          Top = 100
          Height = 25
          ExplicitTop = 100
          ExplicitHeight = 25
          inherited LblModula2: TLabel
            Top = 6
            ExplicitTop = 6
          end
          inherited CE_MOD_2: TZetaDBKeyLookup_DevEx
            Top = 2
            ExplicitTop = 2
          end
        end
        inherited PanelModula3: TPanel
          Top = 125
          Height = 25
          ExplicitTop = 125
          ExplicitHeight = 25
          inherited LblModula3: TLabel
            Top = 6
            ExplicitTop = 6
          end
          inherited CE_MOD_3: TZetaDBKeyLookup_DevEx
            Top = 2
            ExplicitTop = 2
          end
        end
        inherited PanelComenta: TPanel
          Top = 25
          Height = 25
          ExplicitTop = 25
          ExplicitHeight = 25
          inherited LblComenta: TLabel
            Top = 8
            ExplicitTop = 8
          end
          inherited CE_COMENTA: TZetaDBEdit
            Top = 4
            ExplicitTop = 4
          end
        end
      end
    end
  end
  inherited PageControl: TcxPageControl
    Top = 255
    Height = 162
    TabOrder = 1
    ExplicitTop = 255
    ExplicitHeight = 162
    ClientRectBottom = 160
    inherited Tabla: TcxTabSheet
      ExplicitHeight = 133
      inherited GridRenglones: TZetaDBGrid
        Height = 100
        OnEnter = GridRenglonesEnter
        OnExit = GridRenglonesExit
        Columns = <
          item
            ButtonStyle = cbsNone
            Expanded = False
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = '0'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -11
            Title.Font.Name = 'Wingdings'
            Title.Font.Style = []
            Width = 18
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CHECADAS'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #183
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -11
            Title.Font.Name = 'Wingdings'
            Title.Font.Style = []
            Width = 18
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CB_CODIGO'
            Title.Caption = 'N'#250'mero'
            Width = 65
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'PRETTYNAME'
            ReadOnly = True
            Title.Caption = 'Nombre completo'
            Width = 370
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RETARDOS'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'STATUS'
            Visible = False
          end
          item
            ButtonStyle = cbsNone
            Expanded = False
            FieldName = 'CB_TIPO'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = '0'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -11
            Title.Font.Name = 'Wingdings'
            Title.Font.Style = []
            Visible = False
          end>
      end
      inherited Panel2: TPanel
        inherited DepurarLista_DevEx: TcxButton
          Caption = ' &Borrar lista'
          OptionsImage.Glyph.Data = {
            36090000424D3609000000000000360000002800000018000000180000000100
            20000000000000090000000000000000000000000000000000004858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4959CDFF4858CCFF4A5ACDFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4A5ACCFFF8F9FDFF4A59CDFF4C5CCEFFF8F9FDFF4A5ACCFF4858
            CCFF4858CCFF4B5ACDFF4C5CCDFF4A5ACCFF4A5ACCFF4A5ACCFF4858CCFF4A5A
            CCFF4858CCFF4A59CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4959CCFF4858CCFF4A5ACDFEF7F8FCFFF7F8FCFF4959CCFF4858CCFF4858
            CCFFF7F8FCFFF8F9FDFFF8F9FDFFF8F9FDFFF8F9FDFFF8F9FDFFF8F9FDFFF8F9
            FDFFF7F8FCFFF7F8FCFFF8F9FDFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4959CDFF4858CCFF4959CDFFF7F8FCFFF2F4F9FF4858CCFF4858CCFF4858
            CCFFF8F9FDFFF8F9FDFFF8F9FDFFF8F9FDFFF8F9FDFFF8F9FDFFF8F9FDFFF7F8
            FDFFF8F9FDFFF7F8FCFFF7F8FCFF4858CCFF4858CCFF4858CCFF4858CCFF4B5B
            CDFF4858CCFF4959CCFFF8F9FDFF4858CCFF4959CDFFF8F9FDFF4959CCFF4A5A
            CCFF4959CDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4C5CCCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4A5ACDFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4959CCFF4A5ACDFF4959CCFF4959
            CCFF4959CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4959CCFF4858CCFF4958CCFF4958CCFF4858
            CCFF4858CCFF4958CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4959CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4A5ACDFFF8F9FDFF4858CCFF4959CDFFF8F9FDFF4858CCFF4A5A
            CCFF4A5ACCFF4A5ACCFF4858CCFF4A5ACCFF4A5ACCFF4858CCFF4D5DCDFF4A5A
            CCFF4858CCFF4959CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4959
            CCFF4A59CDFF4858CCFF4B5ACDFFF6F7FBFFF6F7FBFF4858CCFF4858CCFF4858
            CCFFF7F8FCFFF7F8FDFFF8F9FDFFF8F9FDFFF8F9FDFFF8F9FDFFF8F9FDFFF8F9
            FDFFF7F8FCFFF7F8FCFFF7F8FDFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4A5ACDFF4858CCFF4858CBFEF6F7FBFFF6F7FBFF4B5BCFFF4858CCFF4858
            CCFFF7F8FCFFF7F8FDFFF7F8FDFFF8F9FDFFF7F8FDFFF6F7FCFFF5F7FCFFF7F8
            FDFFF7F8FCFFF7F8FCFFF7F8FCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4B5BCDFFF8F9FDFF4858CCFF4A5ACDFFF8F9FDFF5260CFFF4C5A
            CCFF4959CCFF4B5ACDFF4B5BCDFF4A5ACDFF4959CCFF4959CCFF4858CCFF4858
            CCFF4959CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4C5BCDFF4858CCFF4B5ACDFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4959CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4959CDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4A5A
            CCFF4858CCFF4858CCFF4858CCFF4959CCFF4858CCFF4858CCFF4959CCFF4858
            CCFF4858CCFF4858CCFF4959CCFF4959CCFF4959CCFF4959CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4A5ACDFFF8F9FDFF4858CCFF4A5ACDFFF8F9FDFF4B5BCDFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4A5ACCFF4858CCFF4B5BCDFF4B5B
            CCFF4858CCFF4959CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4A5ACDFF4858CCFF4C5BCDFFF3F5FBFFF7F8FCFF4F5ECEFF4858CBFE4858
            CBFEF8F9FDFFF8F9FDFFF8F9FDFFF8F9FDFFF8F9FDFFF8F9FDFFF8F9FDFFF8F9
            FDFFF8F9FDFFF8F9FDFFF6F7FCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4959CDFF4858CCFF4858CCFFF5F6FCFFF6F7FCFF4959CCFF4858CCFF4858
            CCFFF6F7FDFFF7F8FCFFF7F8FCFFF7F8FDFFF8F9FDFFF8F9FDFFF7F8FDFFF8F9
            FDFFF7F8FDFFF7F8FDFFF7F8FDFF4858CCFF4858CCFF4858CCFF4858CCFF4B5B
            CDFF4858CCFF4A5ACCFFF8F9FDFF4858CCFF4C5CCEFFF8F9FDFF4858CCFF4959
            CCFF4B5BCDFF5160CDFF4D5DCCFF4858CCFF4858CCFF4858CCFF4858CCFF4A5A
            CCFF4B5BCDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4C5BCDFF4858CCFF4E5DCEFF4858CCFF4858CCFF4E5DCEFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF}
        end
      end
    end
    inherited Datos: TcxTabSheet
      ExplicitHeight = 133
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_BuscarBtn: TdxBarButton
      Visible = ivAlways
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  inherited ImageEmp_DevEx: TcxImageList
    FormatVersion = 1
  end
end
