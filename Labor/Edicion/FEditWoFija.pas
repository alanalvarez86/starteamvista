unit FEditWoFija;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask, ZetaFecha,
  ZetaKeyLookup, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus,
  dxSkinsCore, TressMorado2013, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, Vcl.ImgList, cxNavigator, cxDBNavigator, cxButtons,
  ZetaKeyLookup_DevEx;

type
  TEditWoFija = class(TBaseEdicion_DevEx)
    CB_CODIGO: TZetaDBKeyLookup_DevEx;
    WO_NUMBER: TZetaDBKeyLookup_DevEx;
    WF_FEC_INI: TZetaDBFecha;
    AR_CODIGO: TZetaDBKeyLookup_DevEx;
    OP_NUMBER: TZetaDBKeyLookup_DevEx;
    Label1: TLabel;
    Label2: TLabel;
    lbOrden: TLabel;
    lbParte: TLabel;
    lbOperacion: TLabel;
    procedure WO_NUMBERValidKey(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    procedure Connect;override;
  end;

var
  EditWoFija: TEditWoFija;

implementation

uses DCliente,
     DLabor,
     DGlobal,
     ZGlobalTress,
     ZetaLaborTools,
     ZAccesosTress,
     ZetaCommonClasses;

{$R *.DFM}

{ TEditWoFija }

procedure TEditWoFija.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if ((WO_NUMBER.Llave <> '') and (WO_NUMBER.Descripcion <> '???')) then
    OK_DevEx.Enabled:= true
  else
    OK_DevEx.Enabled:=false;
end;

procedure TEditWoFija.FormCreate(Sender: TObject);
begin
     inherited;
     Caption := Global.GetGlobalString(K_GLOBAL_LABOR_ORDENES)+ ' Fija';
     AsignaGlobal( K_GLOBAL_LABOR_ORDEN, lbOrden, WO_NUMBER );
     AsignaGlobal( K_GLOBAL_LABOR_OPERACION,lbOperacion,OP_NUMBER);
     AsignaGlobal( K_GLOBAL_LABOR_PARTE,lbParte,AR_CODIGO);
     HelpContext := H9504_Ordenes_fijas;
     IndexDerechos := ZAccesosTress.D_LAB_EMP_ORDENES_FIJAS;
     CB_CODIGO.LookupDataset := dmCliente.cdsEmpleadoLookUp;
     WO_NUMBER.LookupDataset := dmLabor.cdsWOrderLookup;
     AR_CODIGO.LookupDataset := dmLabor.cdsPartes;
     OP_NUMBER.LookupDataset := dmLabor.cdsOpera;
end;

procedure TEditWoFija.FormShow(Sender: TObject);
begin
     inherited;
     OK_DevEx.Enabled:=false;
     {$ifdef INTERRUPTORES}
      EditWoFija.width:=K_LEFT_630;
      WO_NUMBER.WidthLlave := K_WIDTHLLAVE;
      AR_CODIGO.WidthLlave := K_WIDTHLLAVE;
      OP_NUMBER.WidthLlave := K_WIDTHLLAVE;

      WO_NUMBER.Width := K_WIDTH_LOOKUP;
      AR_CODIGO.Width := K_WIDTH_LOOKUP;
      OP_NUMBER.Width := K_WIDTH_LOOKUP;
     {$endif}

end;

procedure TEditWoFija.OK_DevExClick(Sender: TObject);
begin
  inherited;
  HabilitaControles;
end;

procedure TEditWoFija.Cancelar_DevExClick(Sender: TObject);
begin
  inherited;
  OK_DevEx.Enabled:=false;
end;

procedure TEditWoFija.Connect;
begin
     with dmLabor do
     begin
          cdsPartes.Conectar;
          cdsOpera.Conectar;
          cdsWorderLookup.Conectar;
          cdsWoFija.Conectar;
          DataSource.DataSet := cdsWoFija;
          CB_CODIGO.Enabled := cdsWoFija.State = dsInsert;
          if CB_CODIGO.Enabled then
             FirstControl := CB_CODIGO
          else FirstControl := WF_FEC_INI;
     end;
     {if ((WO_NUMBER.Llave <> '') and (dmLabor.cdsWOrderLookup.State = dsEdit)) then
      OK_DevEx.Enabled:= true
    else
      OK_DevEx.Enabled:=false;}
end;

procedure TEditWoFija.WO_NUMBERValidKey(Sender: TObject);
begin
     inherited;
     if (WO_NUMBER.Llave <> '') AND (AR_CODIGO.Llave = '') then
     begin
          AR_CODIGO.Llave := WO_NUMBER.LookUpDataSet.FieldByName('AR_CODIGO').AsString;
     end;
end;

end.

