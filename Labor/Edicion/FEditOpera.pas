unit FEditOpera;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaEdit, Grids,
  DBGrids, ZetaDBGrid, Mask, ZetaNumero, ZetaDBTextBox, ComCtrls,
  ZetaKeyCombo, ZetaKeyLookup, ZetaSmartLists, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, TressMorado2013, cxControls,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, Vcl.ImgList,
  cxNavigator, cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx, cxContainer,
  cxEdit, cxTextEdit, cxMemo, cxDBEdit, dxSkinscxPCPainter, cxPCdxBarPopupMenu,
  cxPC;

type
  TEditOpera = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    AH_TIPOLbl: TLabel;
    PageControl: TcxPageControl;
    Datos: TcxTabSheet;
    Tabla: TcxTabSheet;
    OP_NUMBER: TZetaDBEdit;
    LBLOP_NOMBRE: TLabel;
    OP_NOMBRE: TDBEdit;
    Label1: TLabel;
    OP_DESCRIP: TcxDBMemo;
    Label4: TLabel;
    Label5: TLabel;
    OP_STD_HR: TZetaDBNumero;
    OP_FACTOR: TZetaDBNumero;
    Label6: TLabel;
    TO_CODIGO: TZetaDBKeyLookup_DevEx;
    Label2: TLabel;
    OP_STD_CST: TZetaDBNumero;
    CB_NIVEL1lbl: TLabel;
    CB_NIVEL1: TZetaDBKeyLookup_DevEx;
    CB_NIVEL2lbl: TLabel;
    CB_NIVEL2: TZetaDBKeyLookup_DevEx;
    CB_NIVEL3lbl: TLabel;
    CB_NIVEL4lbl: TLabel;
    CB_NIVEL5lbl: TLabel;
    CB_NIVEL6lbl: TLabel;
    CB_NIVEL7lbl: TLabel;
    CB_NIVEL8lbl: TLabel;
    CB_NIVEL9lbl: TLabel;
    CB_NIVEL10lbl: TLabel;
    CB_NIVEL11lbl: TLabel;
    CB_NIVEL12lbl: TLabel;
    CB_NIVEL12: TZetaDBKeyLookup_DevEx;
    CB_NIVEL11: TZetaDBKeyLookup_DevEx;
    CB_NIVEL10: TZetaDBKeyLookup_DevEx;
    CB_NIVEL9: TZetaDBKeyLookup_DevEx;
    CB_NIVEL8: TZetaDBKeyLookup_DevEx;
    CB_NIVEL7: TZetaDBKeyLookup_DevEx;
    CB_NIVEL6: TZetaDBKeyLookup_DevEx;
    CB_NIVEL5: TZetaDBKeyLookup_DevEx;
    CB_NIVEL4: TZetaDBKeyLookup_DevEx;
    CB_NIVEL3: TZetaDBKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CB_NIVEL12ValidLookup(Sender: TObject);
    procedure CB_NIVEL11ValidLookup(Sender: TObject);
    procedure CB_NIVEL10ValidLookup(Sender: TObject);
    procedure CB_NIVEL9ValidLookup(Sender: TObject);
    procedure CB_NIVEL8ValidLookup(Sender: TObject);
    procedure CB_NIVEL7ValidLookup(Sender: TObject);
    procedure CB_NIVEL6ValidLookup(Sender: TObject);
    procedure CB_NIVEL5ValidLookup(Sender: TObject);
    procedure CB_NIVEL4ValidLookup(Sender: TObject);
    procedure CB_NIVEL3ValidLookup(Sender: TObject);
    procedure CB_NIVEL2ValidLookup(Sender: TObject);
    procedure SetEditarSoloActivos;
  private
    procedure SetCamposNivel;
    {$ifdef ACS}procedure SetPosicionNiveles;{ACS}{$endif}
    {$ifdef ACS}
    procedure LimpiaLookUpOlfKey;{ACS}
    {$endif}
    { Private declarations }
  public
    procedure Connect;override;
    procedure DoLookup;override;
  end;

var
  EditOpera: TEditOpera;

{$ifdef ACS}
const
     K_ALT_DEF = 24;
     K_FORMA_ACS = 508;
{$endif}

implementation

uses DLabor,
     DTablas,
     DGlobal,
     ZGlobalTress,
     ZetaClientTools,
     ZetaCommonClasses,
     ZAccesosTress,
     ZetaBuscador_DevEx,
     ZetaCommonTools;

{$R *.DFM}

procedure TEditOpera.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     CB_NIVEL10.DataSource := DataSource;
     CB_NIVEL11.DataSource := DataSource;
     CB_NIVEL12.DataSource := DataSource;

     CB_NIVEL12lbl.Top := CB_NIVEL1lbl.Top;
     CB_NIVEL11lbl.Top := CB_NIVEL12lbl.Top + K_ALT_DEF;
     CB_NIVEL10lbl.Top := CB_NIVEL11lbl.Top + K_ALT_DEF;
     CB_NIVEL9lbl.Top   := CB_NIVEL10lbl.Top + K_ALT_DEF;
     CB_NIVEL8lbl.Top  := CB_NIVEL9lbl.Top + K_ALT_DEF;
     CB_NIVEL7lbl.Top  := CB_NIVEL8lbl.Top + K_ALT_DEF;
     CB_NIVEL6lbl.Top  := CB_NIVEL7lbl.Top + K_ALT_DEF;
     CB_NIVEL5lbl.Top  := CB_NIVEL6lbl.Top + K_ALT_DEF;
     CB_NIVEL4lbl.Top  := CB_NIVEL5lbl.Top + K_ALT_DEF;
     CB_NIVEL3lbl.Top  := CB_NIVEL4lbl.Top + K_ALT_DEF;
     CB_NIVEL2lbl.Top  := CB_NIVEL3lbl.Top + K_ALT_DEF;
     CB_NIVEL1lbl.Top  := CB_NIVEL2lbl.Top + K_ALT_DEF;

     CB_NIVEL12.Top := CB_NIVEL1.Top;
     CB_NIVEL11.Top := CB_NIVEL12.Top + K_ALT_DEF;
     CB_NIVEL10.Top := CB_NIVEL11.Top + K_ALT_DEF;
     CB_NIVEL9.Top   := CB_NIVEL10.Top + K_ALT_DEF;
     CB_NIVEL8.Top  := CB_NIVEL9.Top + K_ALT_DEF;
     CB_NIVEL7.Top  := CB_NIVEL8.Top + K_ALT_DEF;
     CB_NIVEL6.Top  := CB_NIVEL7.Top + K_ALT_DEF;
     CB_NIVEL5.Top  := CB_NIVEL6.Top + K_ALT_DEF;
     CB_NIVEL4.Top  := CB_NIVEL5.Top + K_ALT_DEF;
     CB_NIVEL3.Top  := CB_NIVEL4.Top + K_ALT_DEF;
     CB_NIVEL2.Top  := CB_NIVEL3.Top + K_ALT_DEF;
     CB_NIVEL1.Top  := CB_NIVEL2.Top + K_ALT_DEF;

     CB_NIVEL12.TabOrder := 1;
     CB_NIVEL11.TabOrder := 2;
     CB_NIVEL10.TabOrder := 3;
     CB_NIVEL9.TabOrder := 4;
     CB_NIVEL8.TabOrder := 5;
     CB_NIVEL7.TabOrder := 6;
     CB_NIVEL6.TabOrder := 7;
     CB_NIVEL5.TabOrder := 8;
     CB_NIVEL4.TabOrder := 9;
     CB_NIVEL3.TabOrder := 10;
     CB_NIVEL2.TabOrder := 11;
     CB_NIVEL1.TabOrder := 12;

     Self.Height := K_FORMA_ACS;

     {
     CB_NIVEL10.Visible := True;
     CB_NIVEL11.Visible := True;
     CB_NIVEL12.Visible := True;
     CB_NIVEL10lbl.Visible := True;
     CB_NIVEL11lbl.Visible := True;
     CB_NIVEL12lbl.Visible := True;
     }
     {$endif}
     IndexDerechos := D_LAB_CAT_OPERACIONES;
     FirstControl := OP_NUMBER;
     HelpContext := H9511_Operaciones;
     SetCamposNivel;
     TO_CODIGO.LookupDataset := dmLabor.cdsTOpera;
     {$ifdef ACS}
     CB_NIVEL10.LookupDataset := dmTablas.cdsNivel10;
     CB_NIVEL11.LookupDataset := dmTablas.cdsNivel11;
     CB_NIVEL12.LookupDataset := dmTablas.cdsNivel12;
     {$endif}
     CB_NIVEL9.LookupDataset := dmTablas.cdsNivel9;
     CB_NIVEL8.LookupDataset := dmTablas.cdsNivel8;
     CB_NIVEL7.LookupDataset := dmTablas.cdsNivel7;
     CB_NIVEL6.LookupDataset := dmTablas.cdsNivel6;
     CB_NIVEL5.LookupDataset := dmTablas.cdsNivel5;
     CB_NIVEL4.LookupDataset := dmTablas.cdsNivel4;
     CB_NIVEL3.LookupDataset := dmTablas.cdsNivel3;
     CB_NIVEL2.LookupDataset := dmTablas.cdsNivel2;
     CB_NIVEL1.LookupDataset := dmTablas.cdsNivel1;
     //@DACP Se cambio el valor por codigo, ya que no respetaba lo que se le asigne en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TEditOpera.FormShow(Sender: TObject);
begin
     inherited;
     Caption := Global.GetGlobalString(K_GLOBAL_LABOR_OPERACIONES);
     PageControl.ActivePage := Datos;
     {$ifdef ACS}SetPosicionNiveles;{ACS}{$endif}
end;

procedure TEditOpera.Connect;
begin
     with dmTablas do
     begin
          {$ifdef ACS}
          LimpiaLookUpOlfKey;
          {$endif}
          if CB_NIVEL1.Visible then cdsNivel1.Conectar;
          if CB_NIVEL2.Visible then cdsNivel2.Conectar;
          if CB_NIVEL3.Visible then cdsNivel3.Conectar;
          if CB_NIVEL4.Visible then cdsNivel4.Conectar;
          if CB_NIVEL5.Visible then cdsNivel5.Conectar;
          if CB_NIVEL6.Visible then cdsNivel6.Conectar;
          if CB_NIVEL7.Visible then cdsNivel7.Conectar;
          if CB_NIVEL8.Visible then cdsNivel8.Conectar;
          if CB_NIVEL9.Visible then cdsNivel9.Conectar;
          {$ifdef ACS}
          if CB_NIVEL10.Visible then cdsNivel10.Conectar;
          if CB_NIVEL11.Visible then cdsNivel11.Conectar;
          if CB_NIVEL12.Visible then cdsNivel12.Conectar;
          {$endif}
     end;

     with dmLabor do
     begin
          cdsTOpera.Conectar;
          cdsOpera.Conectar;
          DataSource.DataSet := cdsOpera;
     end;
end;

procedure TEditOpera.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Caption, 'OP_NUMBER', dmLabor.cdsOpera );
end;

procedure TEditOpera.SetCamposNivel;
var
   iNiveles : Integer;
begin
     iNiveles := Global.NumNiveles;
     SetCampoNivel( 1, iNiveles, CB_NIVEL1lbl, CB_NIVEL1 );
     SetCampoNivel( 2, iNiveles, CB_NIVEL2lbl, CB_NIVEL2 );
     SetCampoNivel( 3, iNiveles, CB_NIVEL3lbl, CB_NIVEL3 );
     SetCampoNivel( 4, iNiveles, CB_NIVEL4lbl, CB_NIVEL4 );
     SetCampoNivel( 5, iNiveles, CB_NIVEL5lbl, CB_NIVEL5 );
     SetCampoNivel( 6, iNiveles, CB_NIVEL6lbl, CB_NIVEL6 );
     SetCampoNivel( 7, iNiveles, CB_NIVEL7lbl, CB_NIVEL7 );
     SetCampoNivel( 8, iNiveles, CB_NIVEL8lbl, CB_NIVEL8 );
     SetCampoNivel( 9, iNiveles, CB_NIVEL9lbl, CB_NIVEL9 );
     {$ifdef ACS}
     SetCampoNivel( 10, iNiveles, CB_NIVEL10lbl, CB_NIVEL10 );
     SetCampoNivel( 11, iNiveles, CB_NIVEL11lbl, CB_NIVEL11 );
     SetCampoNivel( 12, iNiveles, CB_NIVEL12lbl, CB_NIVEL12 );
     {$endif}
end;

{ACS: Se establece la relaci�n del nivel seleccionado con el nivel que tiene relacionado
      (nivel 12 al 2, el nivel 1 no tiene relaci�n por ser el nivel inferior).}
procedure TEditOpera.CB_NIVEL12ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel12.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL11.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL11' ).AsString := dmTablas.cdsNivel12.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditOpera.CB_NIVEL11ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel11.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL10.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL10' ).AsString := dmTablas.cdsNivel11.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditOpera.CB_NIVEL10ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel10.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL9.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL9' ).AsString := dmTablas.cdsNivel10.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditOpera.CB_NIVEL9ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel9.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL8.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL8' ).AsString := dmTablas.cdsNivel9.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditOpera.CB_NIVEL8ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel8.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL7.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL7' ).AsString := dmTablas.cdsNivel8.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditOpera.CB_NIVEL7ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel7.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL6.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL6' ).AsString := dmTablas.cdsNivel7.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditOpera.CB_NIVEL6ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel6.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL5.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL5' ).AsString := dmTablas.cdsNivel6.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditOpera.CB_NIVEL5ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel5.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL4.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL4' ).AsString := dmTablas.cdsNivel5.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditOpera.CB_NIVEL4ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel4.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL3.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL3' ).AsString := dmTablas.cdsNivel4.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditOpera.CB_NIVEL3ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel3.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL2.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL2' ).AsString := dmTablas.cdsNivel3.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditOpera.CB_NIVEL2ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel2.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL1.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL1' ).AsString := dmTablas.cdsNivel2.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

{$ifdef ACS}
{ACS: Este procedimiento re-ajustar� la posici�n de los controles de los niveles.}
procedure TEditOpera.SetPosicionNiveles;
const
     K_ALTURA_DEF = 24;
     K_TOP_N1 = 266;
     K_TOP_N1_LBL = 270;
     K_TOP_N2 = 242;
     K_TOP_N2_LBL = 246;
     K_TOP_N3 = 218;
     K_TOP_N3_LBL = 222;
     K_TOP_N4 = 194;
     K_TOP_N4_LBL = 198;
     K_TOP_N5 = 170;
     K_TOP_N5_LBL = 174;
     K_TOP_N6 = 146;
     K_TOP_N6_LBL = 150;
     K_TOP_N7 = 122;
     K_TOP_N7_LBL = 126;
     K_TOP_N8 = 98;
     K_TOP_N8_LBL = 102;
     K_TOP_N9 = 74;
     K_TOP_N9_LBL = 78;
     K_TOP_N10 = 50;
     K_TOP_N10_LBL = 54;
     K_TOP_N11 = 26;
     K_TOP_N11_LBL = 30;
     K_TOP_N12 = 2;
     K_TOP_N12_LBL = 6;
var
     iNivelesNoVisibles, iTotalHorizontal: Integer;
begin
     {Se obtiene la cantidad total de niveles no visibles}
     iNivelesNoVisibles := 0;
     iTotalHorizontal := 0;
     if Not CB_NIVEL12.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL11.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL10.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL9.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL8.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL7.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL6.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL5.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL4.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL3.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL2.Visible then
        inc( iNivelesNoVisibles );

     iTotalHorizontal := ( iNivelesNoVisibles * K_ALTURA_DEF );
     {Se re-ajustan todos los controles en la posici�n horizontal}
     CB_NIVEL12.Top := K_TOP_N12 - iTotalHorizontal;
     CB_NIVEL12lbl.Top := K_TOP_N12_LBL - iTotalHorizontal;
     CB_NIVEL11.Top := K_TOP_N11 - iTotalHorizontal;
     CB_NIVEL11lbl.Top := K_TOP_N11_LBL - iTotalHorizontal;
     CB_NIVEL10.Top := K_TOP_N10 - iTotalHorizontal;
     CB_NIVEL10lbl.Top := K_TOP_N10_LBL - iTotalHorizontal;
     CB_NIVEL9.Top := K_TOP_N9 - iTotalHorizontal;
     CB_NIVEL9lbl.Top := K_TOP_N9_LBL - iTotalHorizontal;
     CB_NIVEL8.Top := K_TOP_N8 - iTotalHorizontal;
     CB_NIVEL8lbl.Top := K_TOP_N8_LBL - iTotalHorizontal;
     CB_NIVEL7.Top := K_TOP_N7 - iTotalHorizontal;
     CB_NIVEL7lbl.Top := K_TOP_N7_LBL - iTotalHorizontal;
     CB_NIVEL6.Top := K_TOP_N6 - iTotalHorizontal;
     CB_NIVEL6lbl.Top := K_TOP_N6_LBL - iTotalHorizontal;
     CB_NIVEL5.Top := K_TOP_N5 - iTotalHorizontal;
     CB_NIVEL5lbl.Top := K_TOP_N5_LBL - iTotalHorizontal;
     CB_NIVEL4.Top := K_TOP_N4 - iTotalHorizontal;
     CB_NIVEL4lbl.Top := K_TOP_N4_LBL - iTotalHorizontal;
     CB_NIVEL3.Top := K_TOP_N3 - iTotalHorizontal;
     CB_NIVEL3lbl.Top := K_TOP_N3_LBL - iTotalHorizontal;
     CB_NIVEL2.Top := K_TOP_N2 - iTotalHorizontal;
     CB_NIVEL2lbl.Top := K_TOP_N2_LBL - iTotalHorizontal;
     CB_NIVEL1.Top := K_TOP_N1 - iTotalHorizontal;
     CB_NIVEL1lbl.Top := K_TOP_N1_LBL - iTotalHorizontal;
end;
{$endif}
{$ifdef ACS}
{Este procedimiento sirve para eliminar cualquier OLDKEY de los lookups de niveles}
procedure TEditOpera.LimpiaLookUpOlfKey;
begin
          CB_NIVEL1.ResetMemory;
          CB_NIVEL2.ResetMemory;
          CB_NIVEL3.ResetMemory;
          CB_NIVEL4.ResetMemory;
          CB_NIVEL5.ResetMemory;
          CB_NIVEL6.ResetMemory;
          CB_NIVEL7.ResetMemory;
          CB_NIVEL8.ResetMemory;
          CB_NIVEL9.ResetMemory;
          CB_NIVEL10.ResetMemory;
          CB_NIVEL11.ResetMemory;
          CB_NIVEL12.ResetMemory;
end;
{$endif}


procedure TEditOpera.SetEditarSoloActivos;
begin
     CB_NIVEL1.EditarSoloActivos := TRUE;
     CB_NIVEL2.EditarSoloActivos := TRUE;
     CB_NIVEL3.EditarSoloActivos := TRUE;
     CB_NIVEL4.EditarSoloActivos := TRUE;
     CB_NIVEL5.EditarSoloActivos := TRUE;
     CB_NIVEL6.EditarSoloActivos := TRUE;
     CB_NIVEL7.EditarSoloActivos := TRUE;
     CB_NIVEL8.EditarSoloActivos := TRUE;
     CB_NIVEL9.EditarSoloActivos := TRUE;
     CB_NIVEL10.EditarSoloActivos := TRUE;
     CB_NIVEL11.EditarSoloActivos := TRUE;
     CB_NIVEL12.EditarSoloActivos := TRUE;
end;

end.

