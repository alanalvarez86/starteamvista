inherited EditKarEmpMaq: TEditKarEmpMaq
  Left = 399
  Top = 365
  Caption = 'Kardex de M'#225'quina'
  ClientHeight = 234
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 66
    Top = 67
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha:'
  end
  object Label6: TLabel [1]
    Left = 48
    Top = 93
    Width = 51
    Height = 13
    Alignment = taRightJustify
    Caption = 'Hora Real:'
  end
  object lbArea: TLabel [2]
    Left = 64
    Top = 120
    Width = 35
    Height = 13
    Alignment = taRightJustify
    Caption = 'Layout:'
  end
  object Label2: TLabel [3]
    Left = 13
    Top = 175
    Width = 86
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha de Cambio:'
  end
  object Label3: TLabel [4]
    Left = 310
    Top = 175
    Width = 79
    Height = 13
    Alignment = taRightJustify
    Caption = 'Hora de Cambio:'
  end
  object ZetaDBTextBox1: TZetaDBTextBox [5]
    Left = 104
    Top = 173
    Width = 100
    Height = 17
    AutoSize = False
    Caption = 'ZetaDBTextBox1'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'EM_FEC_MOV'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object ZetaDBTextBox2: TZetaDBTextBox [6]
    Left = 395
    Top = 173
    Width = 59
    Height = 17
    AutoSize = False
    Caption = 'ZetaDBTextBox2'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'EM_HOR_MOV'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object Label4: TLabel [7]
    Left = 55
    Top = 144
    Width = 44
    Height = 13
    Alignment = taRightJustify
    Caption = 'M'#225'quina:'
  end
  inherited PanelBotones: TPanel
    Top = 198
    TabOrder = 5
  end
  inherited PanelSuperior: TPanel
    TabOrder = 0
  end
  inherited PanelIdentifica: TPanel
    TabOrder = 1
  end
  object EM_FECHA: TZetaDBFecha [11]
    Left = 104
    Top = 62
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 2
    Text = '10/Dec/97'
    Valor = 35774.000000000000000000
    DataField = 'EM_FECHA'
    DataSource = DataSource
  end
  object KA_HORA: TZetaDBHora [12]
    Left = 104
    Top = 89
    Width = 50
    Height = 21
    EditMask = '99:99;0'
    TabOrder = 3
    Text = '    '
    Tope = 24
    Valor = '    '
    DataField = 'EM_HORA'
    DataSource = DataSource
  end
  object LY_CODIGO: TZetaDBKeyLookup [13]
    Left = 104
    Top = 116
    Width = 350
    Height = 21
    TabOrder = 4
    TabStop = True
    WidthLlave = 100
    OnValidKey = LY_CODIGOValidKey
    DataField = 'LY_CODIGO'
    DataSource = DataSource
  end
  object MQ_CODIGO: TZetaDBKeyLookup [14]
    Left = 104
    Top = 140
    Width = 350
    Height = 21
    TabOrder = 6
    TabStop = True
    WidthLlave = 100
    DataField = 'MQ_CODIGO'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 388
    Top = 9
  end
end
