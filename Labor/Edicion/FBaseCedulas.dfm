inherited BaseCedulas: TBaseCedulas
  Left = 325
  Top = 300
  Caption = 'Producci'#243'n por Grupo de Empleados'
  ClientHeight = 453
  ClientWidth = 493
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 417
    Width = 493
    DesignSize = (
      493
      36)
    object ListaEmpleados: TSpeedButton [0]
      Left = 8
      Top = 6
      Width = 130
      Height = 25
      Hint = 'Carga Manual Lista de Empleados'
      AllowAllUp = True
      GroupIndex = 2
      Caption = '&Lista de Empleados'
      Glyph.Data = {
        36060000424D3606000000000000360400002800000020000000100000000100
        08000000000000020000C40E0000C40E00000001000000010000000000008080
        8000000080000080800000800000808000008000000080008000408080004040
        0000FF80000080400000FF00400000408000FFFFFF00C0C0C0000000FF0000FF
        FF0000FF0000FFFF0000FF000000FF00FF0080FFFF0080FF0000FFFF8000FF80
        80008000FF004080FF0000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000000000000000000000000000000000000E0E0E0E0E0E
        0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E131313
        00001313000013130E0E0E0E0E000E0E0E000E000E000E000E0E0E0E00131300
        13130000130013000E0E0E0E000E0E000E0E0E0E0E000E000E0E0E0E0E000013
        13000F0E0000000E0E0E0E0E0E00000E0E0E0E0E0000000E0E0E0E0E0E0E0000
        000F0E0F000E0E0E0E0E0E0E0E0E0000000E0E0E000E0E0E0E0E0E0E0E0E0E00
        0F0E000000000E0E0E0E0E0E0E0E0E000E0E000000000E0E0E0E0E0E0E0E0000
        000F0E0F0E0F000E0E0E0E0E0E0E0001010E0E0E0E0E000E0E0E0E0E0E0E0000
        000E0F0E0F00000E0E0E0E0E0E0E0001010E0E0E0E01000E0E0E0E0E0E000000
        000F0E0F0E0F000E0E0E0E0E0E000101010E0E0E0E0E000E0E0E0E0E00000000
        0F0E0F0E0F0E0F000E0E0E0E000101010E0E0E0E0E0E0E000E0E0E0E00000000
        00000E0F140F000E0E0E0E0E0001010101010E0E010E000E0E0E0E0E00000000
        000E0F0E00000E0E0E0E0E0E00010101010E0E0E01000E0E0E0E0E0E00000000
        010F0E0F0E000E0E0E0E0E0E000101010E0E0E0E0E000E0E0E0E0E0E00000000
        0000010E000E0E0E0E0E0E0E0001010101010E0E000E0E0E0E0E0E0E0E000000
        0000000000000E0E0E0E0E0E0E0001010101010101000E0E0E0E0E0E0E0E0000
        00000000000E0E0E0E0E0E0E0E0E000000000000000E0E0E0E0E}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = ListaEmpleadosClick
    end
    inherited OK: TBitBtn
      Left = 335
      TabOrder = 1
    end
    inherited Cancelar: TBitBtn
      Left = 414
      TabOrder = 2
    end
    object BtnTemp: TButton
      Left = 248
      Top = 8
      Width = 0
      Height = 25
      TabOrder = 0
      OnEnter = BtnTempEnter
    end
  end
  inherited PanelSuperior: TPanel
    Width = 493
    inherited AgregarBtn: TSpeedButton
      Visible = False
    end
    inherited BorrarBtn: TSpeedButton
      Visible = False
    end
    inherited ModificarBtn: TSpeedButton
      Visible = False
    end
    inherited BuscarBtn: TSpeedButton
      Visible = True
    end
    inherited DBNavigator: TDBNavigator
      Visible = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 493
    inherited ValorActivo2: TPanel
      Width = 167
    end
  end
  inherited Panel1: TPanel
    Width = 493
    Height = 174
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 491
      Height = 172
      ActivePage = Generales
      Align = alClient
      TabOrder = 0
      object Generales: TTabSheet
        Caption = '&Generales'
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 483
          Height = 144
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object PanelHora: TPanel
            Left = 0
            Top = 0
            Width = 483
            Height = 23
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object LblHora: TLabel
              Left = 257
              Top = 4
              Width = 26
              Height = 13
              Alignment = taRightJustify
              Caption = 'Hora:'
            end
            object LblCantidad: TLabel
              Left = 340
              Top = 4
              Width = 45
              Height = 13
              Alignment = taRightJustify
              Caption = 'Cantidad:'
            end
            object LblFecha: TLabel
              Left = 92
              Top = 4
              Width = 33
              Height = 13
              Alignment = taRightJustify
              Caption = 'Fecha:'
            end
            object CE_HORA: TZetaDBHora
              Left = 286
              Top = 0
              Width = 42
              Height = 21
              EditMask = '99:99;0'
              TabOrder = 1
              Text = '    '
              Tope = 48
              Valor = '    '
              DataField = 'CE_HORA'
              DataSource = DataSource
            end
            object CE_PIEZAS: TZetaDBNumero
              Left = 388
              Top = 0
              Width = 65
              Height = 21
              Mascara = mnPesos
              TabOrder = 2
              Text = '0.00'
              DataField = 'CE_PIEZAS'
              DataSource = DataSource
            end
            object CE_FECHA: TZetaDBFecha
              Left = 128
              Top = 0
              Width = 115
              Height = 22
              Cursor = crArrow
              TabOrder = 0
              Text = '26/jul/00'
              Valor = 36733.000000000000000000
              DataField = 'CE_FECHA'
              DataSource = DataSource
            end
          end
          object PanelOrden: TPanel
            Left = 0
            Top = 23
            Width = 483
            Height = 23
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object LblOrden: TLabel
              Left = 39
              Top = 4
              Width = 86
              Height = 13
              Alignment = taRightJustify
              Caption = 'Orden de Trabajo:'
            end
            object MultiLote: TBitBtn
              Left = 458
              Top = 0
              Width = 23
              Height = 21
              Hint = 'MultiLotes'
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              OnClick = MultiLoteClick
              Glyph.Data = {
                42010000424D4201000000000000760000002800000011000000110000000100
                040000000000CC00000000000000000000001000000010000000000000000000
                BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
                7777700000007777770000000000700000007777770FFFFFFFF0700000007777
                770FFFFFFFF0700000007777770FFFFFFFF07000000088888800000000007000
                00008FFFFF0FCCCCCCC0700000008FFFFF0000000000700000008FFFFFFFF877
                7777700000008888888888888887700000008F88888888FFFF87700000008888
                888888FFFF8770000000777778FFFFFFFF877000000077777888888888877000
                0000777778F88888888770000000777778888888888770000000777777777777
                777770000000}
            end
            object WO_NUMBER: TZetaDBKeyLookup
              Left = 128
              Top = 0
              Width = 327
              Height = 21
              LookupDataset = dmLabor.cdsWOrderLookup
              EditarSoloActivos = False
              IgnorarConfidencialidad = False
              TabOrder = 0
              TabStop = True
              WidthLlave = 100
              OnValidKey = WO_NUMBERValidKey
              DataField = 'WO_NUMBER'
              DataSource = DataSource
            end
          end
          object PanelParte: TPanel
            Left = 0
            Top = 46
            Width = 483
            Height = 23
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 2
            object LblParte: TLabel
              Left = 97
              Top = 4
              Width = 28
              Height = 13
              Alignment = taRightJustify
              Caption = 'Parte:'
            end
            object AR_CODIGO: TZetaDBKeyLookup
              Left = 128
              Top = 0
              Width = 327
              Height = 21
              LookupDataset = dmLabor.cdsPartes
              EditarSoloActivos = False
              IgnorarConfidencialidad = False
              TabOrder = 0
              TabStop = True
              WidthLlave = 100
              DataField = 'AR_CODIGO'
              DataSource = DataSource
            end
          end
          object PanelArea: TPanel
            Left = 0
            Top = 69
            Width = 483
            Height = 23
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 3
            object LblArea: TLabel
              Left = 100
              Top = 4
              Width = 25
              Height = 13
              Alignment = taRightJustify
              Caption = 'Area:'
            end
            object CE_AREA: TZetaDBKeyLookup
              Left = 128
              Top = 0
              Width = 327
              Height = 21
              LookupDataset = dmLabor.cdsArea
              EditarSoloActivos = False
              IgnorarConfidencialidad = False
              TabOrder = 0
              TabStop = True
              WidthLlave = 75
              OnValidKey = CE_AREAValidKey
              DataField = 'CE_AREA'
              DataSource = DataSource
            end
          end
          object PanelOpera: TPanel
            Left = 0
            Top = 92
            Width = 483
            Height = 23
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 4
            object LblOpera: TLabel
              Left = 73
              Top = 4
              Width = 52
              Height = 13
              Alignment = taRightJustify
              Caption = 'Operaci'#243'n:'
            end
            object OP_NUMBER: TZetaDBKeyLookup
              Left = 128
              Top = 0
              Width = 327
              Height = 21
              LookupDataset = dmLabor.cdsOpera
              EditarSoloActivos = False
              IgnorarConfidencialidad = False
              TabOrder = 0
              TabStop = True
              WidthLlave = 75
              DataField = 'OP_NUMBER'
              DataSource = DataSource
            end
          end
        end
      end
      object Moduladores: TTabSheet
        Caption = '&Moduladores'
        ImageIndex = 1
        object PanelComenta: TPanel
          Left = 0
          Top = 23
          Width = 483
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object LblComenta: TLabel
            Left = 87
            Top = 4
            Width = 61
            Height = 13
            Alignment = taRightJustify
            Caption = 'Comentarios:'
          end
          object CE_COMENTA: TZetaDBEdit
            Left = 151
            Top = 0
            Width = 300
            Height = 21
            MaxLength = 40
            TabOrder = 0
            Text = 'CE_COMENTA'
            DataField = 'CE_COMENTA'
            DataSource = DataSource
          end
        end
        object PanelModula3: TPanel
          Left = 0
          Top = 92
          Width = 483
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 4
          object LblModula3: TLabel
            Left = 79
            Top = 4
            Width = 69
            Height = 13
            Alignment = taRightJustify
            Caption = 'Modulador #3:'
          end
          object CE_MOD_3: TZetaDBKeyLookup
            Left = 151
            Top = 0
            Width = 300
            Height = 21
            LookupDataset = dmLabor.cdsModula3
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 0
            TabStop = True
            WidthLlave = 45
            DataField = 'CE_MOD_3'
            DataSource = DataSource
          end
        end
        object PanelModula2: TPanel
          Left = 0
          Top = 69
          Width = 483
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 3
          object LblModula2: TLabel
            Left = 79
            Top = 4
            Width = 69
            Height = 13
            Alignment = taRightJustify
            Caption = 'Modulador #2:'
          end
          object CE_MOD_2: TZetaDBKeyLookup
            Left = 151
            Top = 0
            Width = 300
            Height = 21
            LookupDataset = dmLabor.cdsModula2
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 0
            TabStop = True
            WidthLlave = 45
            DataField = 'CE_MOD_2'
            DataSource = DataSource
          end
        end
        object PanelModula1: TPanel
          Left = 0
          Top = 46
          Width = 483
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 2
          object LblModula1: TLabel
            Left = 79
            Top = 4
            Width = 69
            Height = 13
            Alignment = taRightJustify
            Caption = 'Modulador #1:'
          end
          object CE_MOD_1: TZetaDBKeyLookup
            Left = 151
            Top = 0
            Width = 300
            Height = 21
            LookupDataset = dmLabor.cdsModula1
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 0
            TabStop = True
            WidthLlave = 45
            DataField = 'CE_MOD_1'
            DataSource = DataSource
          end
        end
        object PanelStatus: TPanel
          Left = 0
          Top = 0
          Width = 483
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object LblStatus: TLabel
            Left = 115
            Top = 4
            Width = 33
            Height = 13
            Alignment = taRightJustify
            Caption = 'Status:'
          end
          object CE_STATUS: TZetaDBKeyCombo
            Left = 151
            Top = 0
            Width = 170
            Height = 21
            AutoComplete = False
            BevelKind = bkFlat
            Style = csDropDownList
            Ctl3D = False
            ItemHeight = 13
            ParentCtl3D = False
            TabOrder = 0
            ListaFija = lfStatusLectura
            ListaVariable = lvPuesto
            Offset = 0
            Opcional = False
            EsconderVacios = False
            DataField = 'CE_STATUS'
            DataSource = DataSource
            LlaveNumerica = True
          end
        end
        object Panel4: TPanel
          Left = 0
          Top = 115
          Width = 483
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 5
          object lbUsuario: TLabel
            Left = 109
            Top = 2
            Width = 39
            Height = 13
            Alignment = taRightJustify
            Caption = 'Usuario:'
          end
          object US_CODIGO: TZetaDBTextBox
            Left = 152
            Top = 0
            Width = 297
            Height = 17
            AutoSize = False
            Caption = 'US_CODIGO'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'US_CODIGO'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
        end
      end
    end
  end
  inherited PageControl: TPageControl
    Top = 225
    Width = 493
    Height = 192
    ActivePage = Tabla
    Visible = False
    inherited Tabla: TTabSheet [0]
      Caption = '&Empleados'
      inherited GridRenglones: TZetaDBGrid
        Width = 485
        Height = 135
        TabOrder = 0
        OnDrawColumnCell = GridRenglonesDrawColumnCell
        Columns = <
          item
            ButtonStyle = cbsNone
            Expanded = False
            FieldName = 'CB_TIPO'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = '0'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -11
            Title.Font.Name = 'Wingdings'
            Title.Font.Style = []
            Width = 13
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CHECADAS'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #183
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -11
            Title.Font.Name = 'Wingdings'
            Title.Font.Style = []
            Width = 13
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CB_CODIGO'
            Title.Caption = 'N'#250'mero'
            Width = 65
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'PRETTYNAME'
            ReadOnly = True
            Title.Caption = 'Nombre Completo'
            Width = 375
            Visible = True
          end>
      end
      inherited Panel2: TPanel
        Width = 485
        TabOrder = 1
        inherited BBAgregar: TBitBtn
          Left = 5
          Width = 80
          Caption = '&Agregar'
        end
        inherited BBBorrar: TBitBtn
          Left = 90
          Width = 80
          Caption = '&Borrar'
        end
        inherited BBModificar: TBitBtn
          Left = 175
          Width = 80
          Caption = 'M&odificar'
        end
        object DepurarLista: TBitBtn
          Left = 385
          Top = 1
          Width = 95
          Height = 25
          Hint = 'Depurar Lista de Empleados'
          Caption = '&Borrar Lista'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = DepurarListaClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333000000000
            3333333777777777F3333330F777777033333337F3F3F3F7F3333330F0808070
            33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
            33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
            333333F7F7F7F7F7F3F33030F080707030333737F7F7F7F7F7333300F0808070
            03333377F7F7F7F773333330F080707033333337F7F7F7F7F333333070707070
            33333337F7F7F7F7FF3333000000000003333377777777777F33330F88877777
            0333337FFFFFFFFF7F3333000000000003333377777777777333333330777033
            3333333337FFF7F3333333333000003333333333377777333333}
          NumGlyphs = 2
        end
      end
    end
    inherited Datos: TTabSheet [1]
      Caption = '&Moduladores'
      TabVisible = False
    end
  end
  object ImageEmp: TImageList
    BkColor = clWhite
    Width = 13
    Left = 428
    Top = 1
    Bitmap = {
      494C01010300040004000D001000FFFFFF00FF10FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000034000000100000000100200000000000000D
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFF0000FFFF0000FFFF00000000000000000000FFFF
      0000FFFF00000000000000000000FFFF0000FFFF0000FFFFFF00FFFFFF000000
      FF0000000000FFFF00000000000000000000FFFF0000FFFF0000000000000000
      0000FFFF0000FFFF0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFF0000FFFF
      000000000000FFFF0000FFFF00000000000000000000FFFF000000000000FFFF
      000000000000FFFFFF00000000000000FF000000FF0000000000FFFF0000FFFF
      00000000000000000000FFFF000000000000FFFF000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF000000000000000000FFFF0000FFFF000000000000C0C0
      C000FFFFFF00000000000000000000000000FFFFFF00FFFFFF00FFFFFF000000
      FF000000FF000000FF000000000000000000C0C0C000FFFFFF00000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF000000
      00000000000000000000C0C0C000FFFFFF00C0C0C00000000000FFFFFF00FFFF
      FF00FFFFFF000000FF000000FF000000FF000000FF000000FF000000FF000000
      0000FFFFFF00C0C0C00000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C0C0C000FFFFFF000000
      0000000000000000000000000000FFFFFF00FFFFFF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF000000000000000000FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF000000
      00000000000000000000C0C0C000FFFFFF00C0C0C000FFFFFF00C0C0C0000000
      0000FFFFFF000000FF000000FF000000FF000000FF000000FF000000FF000000
      0000FFFFFF00C0C0C000FFFFFF00C0C0C00000000000FFFFFF00FFFFFF000000
      000000FFFF0000FFFF00FFFFFF00FFFFFF00FFFFFF0000FFFF0000FFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFFFF00C0C0
      C000FFFFFF00C0C0C0000000000000000000FFFFFF00FFFFFF00FFFFFF000000
      FF000000FF000000FF0000000000FFFFFF00C0C0C000FFFFFF00C0C0C0000000
      000000000000FFFFFF00000000000000000000FFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000FFFF000000000000000000FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00000000000000
      00000000000000000000C0C0C000FFFFFF00C0C0C000FFFFFF00C0C0C0000000
      0000FFFFFF00FFFFFF00FFFFFF000000FF000000FF000000000000000000C0C0
      C000FFFFFF00C0C0C000FFFFFF00C0C0C00000000000FFFFFF00000000000000
      000000FFFF00FFFFFF00FFFFFF00000000007F7F7F007F7F7F0000FFFF000000
      000000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000000000000000000000000000C0C0C000FFFFFF00C0C0
      C000FFFFFF00C0C0C000FFFFFF00C0C0C00000000000FFFFFF00000000000000
      FF000000000000000000C0C0C000FFFFFF00C0C0C000FFFFFF00C0C0C000FFFF
      FF00C0C0C00000000000000000000000000000FFFF00FFFFFF00FFFFFF007F7F
      7F00FFFFFF00FFFFFF0000FFFF000000000000000000FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      0000000000000000000000000000FFFFFF00C0C0C000FF000000C0C0C0000000
      0000FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000FFFFFF00C0C0C000FF000000C0C0C00000000000FFFFFF00FFFFFF000000
      000000FFFF0000FFFF00FFFFFF007F7F7F00FFFFFF0000FFFF0000FFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000FFFFFF00C0C0
      C000FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000FFFFFF00C0C0C000FFFFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF00000000000000000000FFFF0000FFFF007F7F
      7F0000FFFF0000FFFF000000000000000000FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      00000000000080808000C0C0C000FFFFFF00C0C0C000FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF000000000000000000000000000000000080808000C0C0
      C000FFFFFF00C0C0C000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000008080
      8000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      00000000000000000000000000000000000080808000FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000034000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFE000000C006003FFE000000
      8004003FFE000000C08E047FFE000000E13809F01E000000F21800E00E000000
      E1480A600E000000E28E144006000000C14E0A400600000082A4150006000000
      810C08600E000000829C14E00E000000815C0AF01E00000080BC05FFFE000000
      C01E00FFFE000000E03F01FFFE00000000000000000000000000000000000000
      000000000000}
  end
end
