unit FGridMultiLote;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridEdicion_DevEx, Db, Grids, DBGrids, ZetaDBGrid, DBCtrls, Buttons,
  {$ifndef VER130}Variants,{$endif}
  ExtCtrls, StdCtrls, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, dxSkinsCore, TressMorado2013, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, Vcl.ImgList, cxNavigator, cxDBNavigator,
  cxButtons;

type
  TGridMultiLote = class(TBaseGridEdicion_DevEx)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure BuscaOrden;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Buscar; override;
    function ValoresGrid: Variant;override;
  public
    { Public declarations }
  end;

var
  GridMultiLote: TGridMultiLote;

implementation

uses DLabor, DGlobal, ZGlobalTress,ZetaCommonLists,
     ZetaLaborTools, DCliente;

{$R *.DFM}

{ TGridMultiLote }

procedure TGridMultiLote.FormCreate(Sender: TObject);
begin
     AsignaGlobalColumn( K_GLOBAL_LABOR_PARTE, ZetaDBGrid.Columns[2] );
     inherited;
end;

procedure TGridMultiLote.FormShow(Sender: TObject);
begin
     AsignaGlobalColumn( K_GLOBAL_LABOR_PIEZAS, ZetaDBGrid.Columns[3] );
     inherited;
end;

procedure TGridMultiLote.Connect;
begin
     with dmLabor do
     begin
          cdsPartes.Conectar;
          if ( General = USE_CDSCEDMULTI ) then
             DataSource.DataSet := cdsCedMulti
          else
             DataSource.DataSet := cdsMultiLote;
     end;
end;

procedure TGridMultiLote.Buscar;
begin
     inherited;
     with ZetaDBGrid do
     begin
          if ( SelectedField.FieldName = 'WO_NUMBER' ) then
             BuscaOrden;
     end;
end;

procedure TGridMultiLote.BuscaOrden;
var
   sOrden, sOrdenDesc: String;
begin
     with DataSource.DataSet do
     begin
          sOrden := FieldByName( 'WO_NUMBER' ).AsString;
          if ( dmLabor.cdsWOrderLookup.Search( '', sOrden, sOrdenDesc ) ) then
          begin
               if ( sOrden <> FieldByName( 'WO_NUMBER' ).AsString ) then
               begin
                    if not ( State in [ dsEdit, dsInsert ] ) then
                       Edit;
                    FieldByName('WO_NUMBER').AsString := sOrden;
               end;
          end;
     end;
end;

function TGridMultiLote.ValoresGrid : Variant;
begin
     Result := VarArrayOf( [ dmCliente.GetValorActivoStr( stEmpleado ),
                             dmCliente.GetValorActivoStr( stFecha ),
                             stEmpleado,stFecha] );
end;

end.
