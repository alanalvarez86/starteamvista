unit FEditWorks;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask, ComCtrls,
     ZBaseEdicion_DevEx,
     ZetaEdit,
     ZetaKeyCombo,
     ZetaFecha,
     ZetaKeyLookup,
     ZetaNumero,
     ZetaHora,
     ZetaDBTextBox, ZetaSmartLists, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, TressMorado2013, cxControls,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, Vcl.ImgList,
  cxNavigator, cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC, cxContainer, cxEdit, cxCheckBox,
  dxBarBuiltInMenu;

type
  TEditWorks = class(TBaseEdicion_Devex)
    PageControl: TcxPageControl;
    Generales: TcxTabSheet;
    Tiempos: TcxTabSheet;
    Moduladores: TcxTabSheet;
    GroupBox1: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    WO_NUMBER: TZetaDBKeyLookup_DevEx;
    AR_CODIGO: TZetaDBKeyLookup_DevEx;
    lbParte: TLabel;
    lbWOrder: TLabel;
    lbl_hora_R: TLabel;
    GroupBox3: TGroupBox;
    Label15: TLabel;
    WK_TIEMPOlbl: TLabel;
    Label17: TLabel;
    lbModula1: TLabel;
    WK_MOD_1: TZetaDBKeyLookup_DevEx;
    WK_MOD_2: TZetaDBKeyLookup_DevEx;
    lbModula2: TLabel;
    lbModula3: TLabel;
    WK_MOD_3: TZetaDBKeyLookup_DevEx;
    WK_HORA_R: TZetaDBHora;
    lbArea: TLabel;
    CB_AREA: TZetaDBKeyLookup_DevEx;
    Label20: TLabel;
    Label21: TLabel;
    lbOperacion: TLabel;
    OP_NUMBER: TZetaDBKeyLookup_DevEx;
    Label23: TLabel;
    CB_PUESTO: TZetaDBKeyLookup_DevEx;
    CBMultiLote: TcxCheckBox;
    Label4: TLabel;
    WK_STATUS: TZetaDBKeyCombo;
    WK_TIPO: TZetaDBTextBox;
    lbTiempoMuerto: TLabel;
    WK_TMUERTO: TZetaDBKeyLookup_DevEx;
    Label1: TLabel;
    WK_PIEZAS: TZetaDBNumero;
    Label2: TLabel;
    WK_CEDULA: TZetaDBTextBox;
    WK_HRS_ORD: TZetaDBTextBox;
    WK_HRS_2EX: TZetaDBTextBox;
    Label3: TLabel;
    WK_HRS_3EX: TZetaDBTextBox;
    WK_FECHA_R: TZetaDBTextBox;
    WK_LINX_ID: TZetaDBTextBox;
    WK_HORA_A: TZetaDBTextBox;
    WK_MANUAL: TZetaDBTextBox;
    WK_TIEMPO: TZetaDBTextBox;
    Label5: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    LblTiempo: TLabel;
    WK_PRE_CAL: TZetaDBNumero;
    lblMinutos: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WO_NUMBERValidKey(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure OKClick(Sender: TObject);
//    procedure CB_AREAValidKey(Sender: TObject);
  private
    { Private declarations }
    procedure SetControlesVisibles;
    procedure SetControlesOperacion;
  protected
    { Protected declarations }
    Control : TWinControl;
  public
    { Public declarations }
    procedure Connect;override;
 end;

var
  EditWorks: TEditWorks;

implementation

uses DCliente,
     DLabor,
     DCatalogos,
     DGlobal,
     ZGlobalTress,
     ZetaLaborTools,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaDialogo,
     ZAccesosTress,
     ZetaCommonTools;

{$R *.DFM}

{ TEditWorks }

procedure TEditWorks.FormCreate(Sender: TObject);
begin
     inherited;
     Helpcontext := H9502_Operaciones_del_empleado;
     FirstControl := WK_HORA_R;
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stFecha;
     CB_Puesto.LookUpDataSet := dmCatalogos.cdsPuestos;

     if dmCliente.ModoSuper then
     begin
          CB_AREA.Filtro := MIS_AREAS;
          IndexDerechos := D_SUPER_LABOR;
     end
     else IndexDerechos := D_LAB_EMP_OPERACIONES;

     AsignaGlobal( K_GLOBAL_LABOR_ORDEN, lbWOrder, WO_NUMBER );
     AsignaGlobal( K_GLOBAL_LABOR_OPERACION, lbOperacion, OP_NUMBER );
     AsignaGlobal( K_GLOBAL_LABOR_AREA, lbArea, CB_AREA );
     AsignaGlobal( K_GLOBAL_LABOR_TMUERTO, lbTiempoMuerto, WK_TMUERTO );
     AsignaGlobal( K_GLOBAL_LABOR_PARTE, lbParte, AR_CODIGO );
     AsignaGlobal( K_GLOBAL_LABOR_MODULA1,lbModula1, WK_MOD_1 );
     AsignaGlobal( K_GLOBAL_LABOR_MODULA2, lbModula2, WK_MOD_2 );
     AsignaGlobal( K_GLOBAL_LABOR_MODULA3, lbModula3, WK_MOD_3 );
     SetControlesVisibles;
     Control := CB_PUESTO;
end;

procedure TEditWorks.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage := Generales;
     {$ifdef INTERRUPTORES}
      EditWorks.width:=K_WIDTH_BASECEDULAS;
      WO_NUMBER.WidthLlave:=K_WIDTHLLAVE;
      AR_CODIGO.WidthLlave:=K_WIDTHLLAVE;
      CB_AREA.WidthLlave:=K_WIDTHLLAVE;
      OP_NUMBER.WidthLlave:=K_WIDTHLLAVE;
      CB_PUESTO.WidthLlave:=K_WIDTHLLAVE;
      WK_TMUERTO.WidthLlave:=K_WIDTHLLAVE;
      WO_NUMBER.Width := K_WIDTH_LOOKUP;
      AR_CODIGO.Width := K_WIDTH_LOOKUP;
      CB_AREA.Width := K_WIDTH_LOOKUP;
      OP_NUMBER.Width := K_WIDTH_LOOKUP;
      CB_PUESTO.Width := K_WIDTH_LOOKUP;
      WK_TMUERTO.Width:= K_WIDTH_LOOKUP;
     {$endif}
     cbMultilote.Visible := Global.GetGlobalBooleano( K_GLOBAL_LABOR_MULTILOTES );
     label20.Visible := cbMultilote.Visible;
     WK_HORA_R.SetFocus;
     {if dmCliente.ModoLabor then
        DataSource.AutoEdit := TRUE;}       // PENDIENTE : Mientras se Definen Derechos de Acceso para Labor
end;

procedure TEditWorks.Connect;
var
   eTipo: eTipoLectura;
begin
     dmCatalogos.cdsPuestos.Conectar;
     with dmLabor do
     begin
          cdsArea.Conectar;
          cdsOpera.Conectar;
          cdsModula1.Conectar;
          cdsModula2.Conectar;
          cdsModula3.Conectar;
          cdsTiempoMuerto.Conectar;
          cdsWorks.Conectar;
          DataSource.DataSet := cdsWorks;
          with cdsWorks do
          begin
               CBMultiLote.Checked := ( FieldByName( 'WK_FOLIO' ).AsInteger > 0 );
               eTipo := eTipoLectura( cdsWorks.FieldByName( 'WK_TIPO' ).AsInteger );
          end;
          lbTiempoMuerto.Enabled := ( eTipo = wtLibre );
          WK_TMUERTO.Enabled := lbTiempoMuerto.Enabled;
          {
          WK_TIEMPO.Enabled := EsPrecalculada;
          WK_TIEMPOlbl.Enabled := WK_TIEMPO.Enabled;
          }
     end;
end;

procedure TEditWorks.WO_NUMBERValidKey(Sender: TObject);
begin
     inherited;
     if (WO_NUMBER.Llave <> '') AND (AR_CODIGO.Llave = '') then
     begin
          AR_CODIGO.Llave := WO_NUMBER.LookUpDataSet.FieldByName('AR_CODIGO').AsString;
     end;
end;

procedure TEditWorks.SetControlesVisibles;

   procedure SetControlLookup( oControl: TZetaDBKeyLookup_DevEx; oLabel : TLabel );
   begin
        if not oControl.Enabled then
        begin
             oControl.Visible := FALSE;
             oLabel.Visible := FALSE;
        end;
   end;

begin
     // De momento solo funciona en moduladores
     SetControlLookup( WK_MOD_1, lbModula1 );
     SetControlLookup( WK_MOD_2, lbModula2 );
     SetControlLookup( WK_MOD_3, lbModula3 );
     // Si se tiene Orden no se puede capturar parte
     if WO_NUMBER.Enabled and AR_CODIGO.Enabled then
     begin
          lbParte.Enabled := FALSE;
          AR_CODIGO.Enabled := FALSE;
     end;
end;

procedure TEditWorks.SetControlesOperacion;
var
   lEnabled, lActivado : boolean;
begin
     lEnabled := ( ( CB_AREA.Llave <> VACIO ) and ( not( zStrToBool( dmLabor.cdsArea.FieldByName( 'TB_OP_UNI' ).AsString ) ) ) ) or ( CB_AREA.Llave = VACIO );
     lActivado := OP_NUMBER.Enabled;
     if ( ACtiveControl <> nil ) and ( ActiveControl.ClassName = 'TZetaLlave' ) and ( ActiveControl.Parent = OP_NUMBER ) and  ( Not lEnabled ) then
        Control.SetFocus;
     OP_NUMBER.Enabled := lEnabled;
     lbOperacion.Enabled := lEnabled;
     if ( Control.Classname = 'TZetaDBKeyLookup' ) and ( TWinControl( Control.Controls[ 2 ] ).Focused ) and lEnabled and ( not lActivado ) then
        OP_NUMBER.SetFocus;
end;

procedure TEditWorks.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( ( Field = nil ) or ( Field.FieldName = 'CB_AREA' ) ) then
     begin
          SetControlesOperacion;
     end;

     { ****** Control del Tiempo PreCalculado ******* }
     with lblTiempo do
     begin
          Visible := dmLabor.EsPrecalculada; //or ( ( zStrToBool( cdsWorks.FieldByName( 'WK_MANUAL' ).AsString ) ) and ( cdsWorks.FieldByName( 'WK_TIPO' ).AsInteger = ord( wtBreak ) ) );
          lblMinutos.Visible := Visible;
          WK_PRE_CAL.enabled := Visible;
          WK_PRE_CAL.Visible := Visible;

          if Visible or ( ( zStrToBool( dmLabor.cdsWorks.FieldByName( 'WK_MANUAL' ).AsString ) ) and ( dmLabor.cdsWorks.FieldByName( 'WK_TIPO' ).AsInteger = ord( wtBreak ) ) ) then
             { ****** Controles de Hora Inicial ******* }
             lbl_hora_r.Caption := 'Hora Final:'
          else
              { ****** Controles de Hora Real ******* }
              lbl_hora_r.Caption := 'Hora Real:';
     end;
end;

procedure TEditWorks.OKClick(Sender: TObject);
begin
     inherited;
     if ( dmLabor.cdsWorks.ChangeCount = 0 ) then
        Close;
end;

end.

