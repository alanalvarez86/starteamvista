unit FEditBreaks;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicionRenglon_DevEx, ZetaEdit, StdCtrls, Mask, DBCtrls, Db, Grids,
  DBGrids, ZetaDBGrid, ComCtrls, ExtCtrls, Buttons, ZetaNumero, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore,
  TressMorado2013, cxControls, dxSkinscxPCPainter, cxPCdxBarPopupMenu,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, Vcl.ImgList, cxPC,
  cxNavigator, cxDBNavigator, cxButtons;

type
  TEditBreaks = class(TBaseEdicionRenglon_DevEx)
    DBCodigoLBL: TLabel;
    DBDescripcionLBL: TLabel;
    BR_NOMBRE: TDBEdit;
    BR_CODIGO: TZetaDBEdit;
    DBInglesLBL: TLabel;
    BR_INGLES: TDBEdit;
    Label2: TLabel;
    Label1: TLabel;
    BR_TEXTO: TDBEdit;
    BR_NUMERO: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure BuscaMotivo;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure DoLookup; override;
    procedure HabilitaControles; override;
  public
    { Public declarations }
  end;

var
  EditBreaks: TEditBreaks;

implementation

uses DLabor,
     ZetaBuscador_DevEx,
     ZetaCommonClasses,
     ZAccesosTress;

{$R *.DFM}

procedure TEditBreaks.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_LAB_CAT_BREAKS;
     HelpContext := H9518_Catalogo_Breaks;
     //BuscarBtn.Visible := True;
     FirstControl := BR_CODIGO;
end;

procedure TEditBreaks.HabilitaControles;
begin
     inherited;
    // BuscarBtn.Visible := True;
end;

procedure TEditBreaks.FormShow(Sender: TObject);
begin
     inherited;
     //PageControl.ActivePage := ;
end;

procedure TEditBreaks.Connect;
begin
     with dmLabor do
     begin
          cdsTiempoMuerto.Conectar;
          cdsBreaks.Conectar;
          DataSource.DataSet := cdsBreaks;
          dsRenglon.Dataset := cdsBrkHora;
     end;
end;



procedure TEditBreaks.DoLookup;
begin
     inherited;
     with GridRenglones do
     begin
          if Focused and ( SelectedField.FieldName = 'BH_MOTIVO' ) then
             BuscaMotivo
          else
              ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Caption, 'BR_CODIGO', dmLabor.cdsBreaks );
     end;
end;

procedure TEditBreaks.BuscaMotivo;
var
   sMotivo, sMotivoDesc: String;
begin
     with dmLabor.cdsBrkHora do
     begin
          sMotivo := FieldByName( 'BH_MOTIVO' ).AsString;
          if dmLabor.cdsTiempoMuerto.Search( '', sMotivo, sMotivoDesc ) then
          begin
               if ( sMotivo <> FieldByName( 'BH_MOTIVO' ).AsString ) then
               begin
                    if not ( State in [ dsEdit, dsInsert ] ) then
                       Edit;
                    FieldByName( 'BH_MOTIVO' ).AsString := sMotivo;
               end;
          end;
     end;
end;

end.
