unit FEditTMaquinas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaNumero, Mask,
  ZetaEdit, ZetaSmartLists, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, dxSkinsCore, TressMorado2013, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, Vcl.ImgList, cxNavigator, cxDBNavigator,
  cxButtons;

type
  TTOTMaquinas = class(TBaseEdicion_DevEx)
    DBCodigoLBL: TLabel;
    TB_CODIGO: TZetaDBEdit;
    DBDescripcionLBL: TLabel;
    TB_ELEMENT: TDBEdit;
    DBInglesLBL: TLabel;
    TB_INGLES: TDBEdit;
    Label2: TLabel;
    TB_NUMERO: TZetaDBNumero;
    Label1: TLabel;
    TB_TEXTO: TDBEdit;
    cxButton1: TcxButton;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect;override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  TOTMaquinas: TTOTMaquinas;

implementation

uses dTablas,
     ZetaBuscador_DevEx,
     ZetaCommonClasses,
     DGlobal,
     ZGlobalTress,
     ZAccesosTress, DLabor;


{$R *.DFM}

procedure TTOTMaquinas.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := TB_CODIGO;
     IndexDerechos := ZAccesosTress.D_LAB_CAT_TMAQUINAS;
     //HelpContext:= HTiposMaquinas;
end;

procedure TTOTMaquinas.Connect;
begin
     with dmLabor do
     begin
          DataSource.DataSet := cdsTMaquinas;
          Self.Caption := Format('Tipos de %s',[Global.GetGlobalString(K_GLOBAL_LABOR_MAQUINAS)]);
     end;
end;

procedure TTOTMaquinas.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Self.Caption, 'TB_CODIGO', dmLabor.cdsTMaquinas);
end;


end.
