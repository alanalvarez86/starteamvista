unit FEditKardexEmpMaq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, StdCtrls, Mask, ZetaFecha, Db, ExtCtrls, DBCtrls, Buttons,
  ZetaKeyLookup, ZetaHora, ZetaDBTextBox, ZetaSmartLists;

type
  TEditKarEmpMaq = class(TBaseEdicion)
    EM_FECHA: TZetaDBFecha;
    Label1: TLabel;
    KA_HORA: TZetaDBHora;
    Label6: TLabel;
    lbArea: TLabel;
    LY_CODIGO: TZetaDBKeyLookup;
    Label2: TLabel;
    Label3: TLabel;
    ZetaDBTextBox1: TZetaDBTextBox;
    ZetaDBTextBox2: TZetaDBTextBox;
    Label4: TLabel;
    MQ_CODIGO: TZetaDBKeyLookup;
    procedure FormCreate(Sender: TObject);
    procedure LY_CODIGOValidKey(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
  public
    { Public declarations }
  end;


var
  EditKarEmpMaq: TEditKarEmpMaq;

implementation

uses DLabor, ZetaLaborTools, ZetaCommonLists, ZetaCommonClasses,DGlobal, ZGlobalTress, ZAccesosTress;

{$R *.DFM}

procedure TEditKarEmpMaq.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := ZAccesosTress.D_LAB_KARDEX_MAQUINA_EMP ;

     Self.Caption := Format( 'Kardex de %s',[Global.GetGlobalString( K_GLOBAL_LABOR_MAQUINAS ) ] );
     TipoValorActivo1 := ZetaCommonLists.stEmpleado;
     FirstControl := EM_FECHA;
     LY_CODIGO.LookupDataset := dmLabor.cdsLayouts;
     MQ_CODIGO.LookupDataset := dmLabor.cdsMaquinas;
end;

procedure TEditKarEmpMaq.Connect;
begin
     with dmLabor do
     begin
          cdsLayouts.Conectar;
          cdsMaquinas.Conectar;

          DataSource.DataSet := cdsKarEmpMaquinas;

     end;
end;

procedure TEditKarEmpMaq.LY_CODIGOValidKey(Sender: TObject);
var
   sFiltro:string;
begin
     inherited;
     sFiltro := 'MQ_CODIGO in (';
     with dmLabor do
     begin
          cdsLayMaquinas.Refrescar;
          with cdsLayMaquinas do
          begin
               First;
               sFiltro := sFiltro + Format('''%s''',[ FieldByName('MQ_CODIGO').AsString ]);
               Next;
               while not eof do
               begin
                    sFiltro := sFiltro + Format(',''%s''',[ FieldByName('MQ_CODIGO').AsString ]);
                    Next;
               end;
               sFiltro := sFiltro + ')';
          end;
          cdsMaquinas.Filtered := False;
          cdsMaquinas.Filter := sFiltro;
          cdsMaquinas.Filtered := True;
     end;

end;

procedure TEditKarEmpMaq.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     dmLabor.cdsMaquinas.Filtered := False;
     inherited;

end;

end.

