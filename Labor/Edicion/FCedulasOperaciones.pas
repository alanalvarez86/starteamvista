unit FCedulasOperaciones;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FBaseCedulas,
  Db, StdCtrls, ZetaKeyCombo, Grids, DBGrids, ZetaDBGrid,
  ComCtrls, ZetaFecha, ZetaNumero, Mask, ZetaHora, ZetaKeyLookup, ExtCtrls,
  DBCtrls, Buttons, ZetaEdit, ImgList, ZetaDBTextBox, ZetaSmartLists;

type
  TCedulasOperaciones = class(TBaseCedulas)
    procedure FormCreate(Sender: TObject);
    procedure MultiLoteClick(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure ValidaDatosLabor; override;
  public
    { Public declarations }
  end;

var
  CedulasOperaciones: TCedulasOperaciones;

implementation

uses DGlobal, ZGlobalTress, ZetaCommonTools, DLabor,ZetaLaborTools,Variants,ZetaDialogo, ZetaCommonClasses;

{$R *.DFM}

procedure TCedulasOperaciones.FormCreate(Sender: TObject);
begin
     //self.Caption:= Format( self.Caption, [ CE_AREA.Descripcion ] );//Global.GetGlobalString( K_GLOBAL_LABOR_OPERACION ) ] );
     CedulaCaption := 'Producción';
     inherited;
end;

procedure TCedulasOperaciones.ValidaDatosLabor;
{$ifdef SUPERVISORES}
{$ifdef Hyundai}
var
   dPiezas:TDiasHoras;
   sAreaAnt:string;
   sOrdenAnt:string;
{$endif}
{$endif}
begin
     inherited;
     if StrVacio( CE_HORA.Valor ) then
     begin
          ActiveControl := CE_HORA;
          DataBaseError( 'No Se Ha Especificado La Hora' );
     end;

     {$ifdef SUPERVISORES}
     {$ifdef Hyundai}
      with dmLabor do
      begin
           with cdsCedulas do
           begin
                if not zStrToBool( cdsCedulas.FieldByName( 'CE_MULTI' ).AsString )then
                begin
                     dPiezas := 0;
                     sAreaAnt := VACIO;
                     sOrdenAnt := VACIO;
                     if State = dsEdit then
                     begin
                         if not VarIsNull( FieldByName('CE_PIEZAS').OldValue ) then
                         begin
                              dPiezas := FieldByName('CE_PIEZAS').OldValue;
                         end;
                         if not VarIsNull( FieldByName('CE_AREA').OldValue ) then
                         begin
                              sAreaAnt := FieldByName('CE_AREA').OldValue;
                         end;
                         if not VarIsNull( FieldByName('WO_NUMBER').OldValue ) then
                         begin
                              sOrdenAnt := FieldByName('WO_NUMBER').OldValue;
                         end;
                     end;
                     ValidaPiezasOrden(FieldByName( 'WO_NUMBER' ).AsString,FieldByName( 'CE_PIEZAS' ).AsFloat,dPiezas,sAreaAnt,False,sOrdenAnt);
                end;
           end;
      end;
     {$endif}
     {$endif}
end;

procedure TCedulasOperaciones.MultiLoteClick(Sender: TObject);
begin
     {$ifdef HYUNDAI}
     {$ifdef SUPERVISORES}
     dmLabor.VerificaPiezas := True;
     if StrLleno(CE_AREA.Llave )then
     begin
          CE_PIEZAS.Enabled := NOT dmLabor.ShowMultiLote;
          LblCantidad.Enabled := CE_PIEZAS.Enabled;
     end
     else
     begin
          ZWarning(Self.Caption,Format('Debe Especificar %s Para Validación',[GetLaborLabel( K_GLOBAL_LABOR_AREA )]),0,mbOk);
          CE_AREA.SetFocus;
     end;
     {$else}
     inherited;
     {$endif}
     {$else}
     inherited;
     {$endif}

end;

end.
