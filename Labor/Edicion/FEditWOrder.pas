unit FEditWOrder;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, Grids, DBGrids, ComCtrls, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask,
     ZBaseEdicionRenglon_DevEx,
     ZetaDBGrid,
     ZetaFecha,
     ZetaEdit,
     ZetaKeyCombo,
     ZetaNumero,
     ZetaKeyLookup,
     ZetaSmartLists, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, dxSkinsCore, TressMorado2013, cxControls, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  Vcl.ImgList, cxPC, cxNavigator, cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx,
  ZetaSmartLists_DevEx, dxBarBuiltInMenu;

type
  TEditWOrder = class(TBaseEdicionRenglon_DevEx)
    WO_NUMBER: TZetaDBEdit;
    lbWOrder: TLabel;
    WO_DESCRIP: TDBMemo;
    Label2: TLabel;
    Label3: TLabel;
    WO_FEC_INI: TZetaDBFecha;
    Label4: TLabel;
    WO_FEC_FIN: TZetaDBFecha;
    LBParte: TLabel;
    AR_CODIGO: TZetaDBKeyLookup_DevEx;
    WO_QTY: TZetaDBNumero;
    Label8: TLabel;
    Label9: TLabel;
    WO_STATUS: TZetaDBKeyCombo;
    Panel3: TPanel;
    BArribaOrden: TZetaSmartListsButton_DevEx;
    BAbajoOrden: TZetaSmartListsButton_DevEx;
    Label1: TLabel;
    WO_TEXTO: TZetaDBEdit;
    Label5: TLabel;
    WO_NUM_GEN: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PageControlChange(Sender: TObject);
    procedure BArribaOrdenClick(Sender: TObject);
    procedure BAbajoOrdenClick(Sender: TObject);
    procedure AR_CODIGOValidKey(Sender: TObject);
    procedure BuscarBtnClick(Sender: TObject);
    procedure dxBarButton_BuscarBtnClick(Sender: TObject);
    procedure GridRenglonesEnter(Sender: TObject);
    procedure GridRenglonesExit(Sender: TObject);
  private
    { Private declarations }
    FConectando: Boolean;
    FParte: String;
    function GetPosicion: Integer;
    procedure Intercambia(const lUp: Boolean);
    procedure SetPosicion(const iPos: Integer);
    procedure SetBuscaBtn;
    procedure AgregaDefSteps;
    procedure HabilitaCtrlBusquedaPasos;
  protected
    { Protected declarations }
    procedure KeyDown( var Key: Word; Shift: TShiftState );override;
  public
    { Public declarations }
    procedure Connect;override;
    procedure Buscar;
    procedure HabilitaControles;override;
  end;

var
  EditWOrder: TEditWOrder;

implementation

uses FTressShell,
     ZetaTipoEntidad,
     ZetaCommonClasses,
     ZetaCommonTools,
     DGlobal,
     ZGlobalTress,
     ZetaDialogo,
     ZetaLaborTools,
     ZAccesosTress,
     DLabor;

{$R *.DFM}

procedure TEditWOrder.FormCreate(Sender: TObject);
var
   sOperaciones: String;
begin
     inherited;
     HelpContext := H9505_Orden_de_trabajo;
     FConectando := FALSE;
     FirstControl := WO_NUMBER;
     AsignaGlobal( K_GLOBAL_LABOR_PARTE, lbParte, AR_CODIGO );
     AsignaGlobal( K_GLOBAL_LABOR_ORDEN, lbWOrder, WO_NUMBER );
     with Global do
     begin
          Caption := GetGlobalString( K_GLOBAL_LABOR_ORDEN );
          sOperaciones  := GetGlobalString( K_GLOBAL_LABOR_OPERACIONES );
          with Tabla do
          begin
               TabVisible := GetGlobalBooleano( K_GLOBAL_LABOR_USA_DEFSTEPS ) and StrLleno( sOperaciones );
               Caption := sOperaciones;
          end;
     end;
     AR_CODIGO.LookupDataset := dmLabor.cdsPartes;
end;

procedure TEditWOrder.FormShow(Sender: TObject);
begin
     IndexDerechos := dmLabor.cdsWOrderSteps.Tag;         // Asigna el Derecho de Acceso Obtenido en el Connect de las formas de consulta
     inherited;
     PageControl.ActivePage := Datos;
     dxBarButton_BuscarBtn.Enabled := false;
     HabilitaCtrlBusquedaPasos;
     {$ifdef INTERRUPTORES}
      AR_CODIGO.WidthLlave := K_WIDTHLLAVE;
      AR_CODIGO.Width := K_WIDTH_LOOKUP;
      EditWOrder.Width := K_WIDTH_BASECEDULAS;
     {$endif}
end;

procedure TEditWOrder.Buscar;
var
   sLlave, sDescripcion: String;
begin
     with GridRenglones.SelectedField do
     begin
          if ( FieldName = 'OP_NUMBER' ) or ( FieldName = 'OP_NOMBRE' ) then
          begin
               if TressShell.BuscaDialogo( enOpera, '', sLlave, sDescripcion ) then
               begin
                    with dmLabor.cdsSteps do
                    begin
                         if ( State = dsBrowse ) then
                            Edit;
                         FieldByName( 'OP_NUMBER' ).AsString := sLlave;
                    end;
               end;
          end;
     end;
end;

procedure TEditWOrder.Connect;
begin
     FConectando := TRUE;
     with dmLabor do
     begin
          cdsPartes.Conectar;
          cdsWOrderSteps.Conectar;
          cdsSteps.Conectar;
          //Se pone esto para que el Grid de Pasos
          //Siempre salga ordenado por Numero de Paso
          cdsSteps.IndexFieldNames := 'ST_SEQUENC';
          DataSource.DataSet := cdsWOrderSteps;
          dsRenglon.DataSet := cdsSteps;
          with cdsSteps do
          begin
               DisableControls;
               First;
               EnableControls;
          end;
          FParte := cdsWOrderSteps.FieldByName( 'AR_CODIGO' ).AsString;
     end;
     FConectando := FALSE;
end;

procedure TEditWOrder.dxBarButton_BuscarBtnClick(Sender: TObject);
begin
  Buscar;
end;

procedure TEditWOrder.PageControlChange(Sender: TObject);
begin
     inherited;
     SetBuscaBtn;
     HabilitaCtrlBusquedaPasos;
end;

procedure TEditWorder.KeyDown( var Key: Word; Shift: TShiftState );
begin
     if ( Key <> 0 ) and
        ( ssCtrl in Shift ) and { CTRL }
        ( Key = 66 )  then      { Letra B = Buscar }
     begin
          Key := 0;
          Buscar;
     end;
     inherited KeyDown( Key, Shift );
end;

procedure TEditWorder.Intercambia(const lUp: Boolean);
var
   Actual, Actual2: TBookmark;
   iActual, iOtro: Integer;
   lExiste: Boolean;
begin
    with dmLabor.cdsSteps do
    begin
         if State in [ dsEdit, dsInsert ] then
            Post;
         iActual := GetPosicion;
         Actual := GetBookmark;
         try
            if lUp then
               lExiste := FindPrior
            else
                lExiste := FindNext;
            if lExiste then
            begin
                 iOtro := GetPosicion;
                 SetPosicion( -1 );
                 Actual2 := GetBookmark;
                 try
                    GotoBookmark( Actual );
                    SetPosicion( iOtro );
                    GotoBookmark( Actual2 );
                    SetPosicion( iActual );
                 finally
                        FreeBookmark( Actual2 );
                 end;
                 if lUp then
                    Prior
                 else
                     Next;
            end;
         finally
                FreeBookmark( Actual );
         end;
    end;
end;

function TEditWorder.GetPosicion: Integer;
begin
     Result := dmLabor.cdsSteps.FieldByName( 'ST_SEQUENC' ).AsInteger;
end;

procedure TEditWOrder.GridRenglonesEnter(Sender: TObject);
begin
  //inherited;
  dxBarButton_BuscarBtn.Enabled:= true;
end;

procedure TEditWOrder.GridRenglonesExit(Sender: TObject);
begin
  ///inherited;
  dxBarButton_BuscarBtn.Enabled:=false;
end;

procedure TEditWorder.SetPosicion( const iPos: Integer);
begin
     with dmLabor.cdsSteps do
     begin
          Edit;
          FieldByName( 'ST_SEQUENC' ).AsInteger := iPos;
          Post;
     end;
end;

procedure TEditWOrder.BArribaOrdenClick(Sender: TObject);
begin
     inherited;
     Intercambia( TRUE );
end;

procedure TEditWOrder.BAbajoOrdenClick(Sender: TObject);
begin
     inherited;
     Intercambia( FALSE );
end;

procedure TEditWOrder.HabilitaControles;
var
  bStatusBusqueda:boolean;
begin
     bStatusBusqueda:=dxBarButton_BuscarBtn.Enabled;
     inherited HabilitaControles;
     SetBuscaBtn;
     dxBarButton_BuscarBtn.Enabled:=bStatusBusqueda;
end;

procedure TEditWOrder.HabilitaCtrlBusquedaPasos;
begin

end;

procedure TEditWOrder.SetBuscaBtn;
begin
     //BuscarBtn.Enabled := PageControl.ActivePage = Tabla;
end;

procedure TEditWOrder.AgregaDefSteps;
var
   i: Integer;
begin
     i := 1;
     with dmLabor do
     begin
          if not cdsSteps.IsEmpty and ZetaDialogo.ZConfirm( self.Caption, 'La Lista de ' + Tabla.Caption + ' tiene Información !' + CR_LF + '¿ Desea Reemplazar ?', 0, mbYes ) then
          begin
               with cdsSteps do
               begin
                    DisableControls;
                    try
                       First;
                       while not EOF do
                       begin
                            Delete; { El Delete hace el Next }
                       end;
                    finally
                           EnableControls;
                    end;
               end;
          end;
          if cdsSteps.IsEmpty then
          begin
               with cdsDefSteps do
               begin
                    RefrescaPartes;
                    First;
                    cdsSteps.DisableControls;
                    try
                       while not Eof do
                       begin
                            cdsSteps.Append;
                            cdsSteps.FieldByName( 'ST_SEQUENC' ).AsInteger := i;
                            cdsSteps.FieldByName( 'OP_NUMBER' ).AsString := FieldByName( 'OP_NUMBER' ).AsString;
                            cdsSteps.FieldByName( 'ST_STD_HR' ).AsFloat := FieldByName( 'DF_STD_HR' ).AsFloat;
                            cdsSteps.Post;
                            Next;
                            Inc( i );
                       end;
                       cdsSteps.First;
                    finally
                           cdsSteps.EnableControls;
                    end;
               end;
          end;
     end;
end;

procedure TEditWOrder.AR_CODIGOValidKey(Sender: TObject);
begin
     inherited;
     with AR_CODIGO do
     begin
          if ( LLave <> '' ) and ( FParte <> Llave ) then
          begin
               FParte := Llave;
               AgregaDefSteps;
          end;
     end;
end;

procedure TEditWOrder.BuscarBtnClick(Sender: TObject);
begin
     Buscar;
end;

end.

