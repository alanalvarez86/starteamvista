unit FEditPartes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZetaKeyLookup, Mask, ZetaNumero, StdCtrls, ZetaEdit,
  Db, Grids, DBGrids, ZetaDBGrid, ComCtrls, ExtCtrls, DBCtrls, Buttons,
  ZetaSmartLists, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus,
  dxSkinsCore, TressMorado2013, dxSkinsDefaultPainters,  
    cxControls, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  Vcl.ImgList, cxPC, cxNavigator, cxDBNavigator, cxButtons,
  ZBaseEdicionRenglon_DevEx, ZetaKeyLookup_DevEx, ZetaSmartLists_DevEx,
  dxBarBuiltInMenu;

type
  TEditPartes = class(TBaseEdicionRenglon_DevEx)
    AH_TIPOLbl: TLabel;
    AR_CODIGO: TZetaDBEdit;
    AR_NOMBRE: TZetaDBEdit;
    LBLOP_NOMBRE: TLabel;
    Label2: TLabel;
    AR_SHORT: TZetaDBEdit;
    AR_BARCODE: TZetaDBEdit;
    AR_STD_HR: TZetaDBNumero;
    AR_FACTOR: TZetaDBNumero;
    TT_CODIGO: TZetaDBKeyLookup_DevEx;
    Label6: TLabel;
    Label5: TLabel;
    Label4: TLabel;
    Label1: TLabel;
    Panel3: TPanel;
    Label3: TLabel;
    AR_STD_CST: TZetaDBNumero;
    btnSubir: TZetaSmartListsButton_DevEx;
    btnBajar: TZetaSmartListsButton_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure PageControlChange(Sender: TObject);
    procedure BArribaOrdenClick(Sender: TObject);
    procedure BAbajoOrdenClick(Sender: TObject);
    procedure BuscarBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GridRenglonesEnter(Sender: TObject);
    procedure GridRenglonesExit(Sender: TObject);
    procedure dxBarButton_BuscarBtnClick(Sender: TObject);
  private
    function GetPosicion: Integer;
    procedure Intercambia(const lUp: Boolean);
    procedure SetBuscaBtn;
    procedure SetPosicion(const nPos: Integer);
    { Private declarations }
  protected
    procedure KeyDown( var Key: Word; Shift: TShiftState );override;
    procedure HabilitaControles;override;
  public
    { Public declarations }
    procedure Connect;override;
    procedure Buscar;
  end;

var
  EditPartes: TEditPartes;

implementation

uses DLabor,
     DGlobal,
     ZGlobalTress,
     ZetaBuscador_DevEx,
     ZetaTipoEntidad,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZAccesosTress,
     FTressShell;

{$R *.DFM}

procedure TEditPartes.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_LAB_CAT_PARTES;
     HelpContext := H9510_Partes;
     FirstControl := AR_CODIGO;
     TT_CODIGO.LookupDataset := dmLabor.cdsTParte;

     {.$ifdef LABORSCRAP}
     AR_FACTOR.Mascara := mnNumeroGlobal;
     {.$ENDIF}
end;

procedure TEditPartes.FormShow(Sender: TObject);
var
   sOperaciones : string;
begin
     inherited;
     with Global do
     begin
          Caption := GetGlobalString( K_GLOBAL_LABOR_PARTES );
          sOperaciones  := GetGlobalString( K_GLOBAL_LABOR_OPERACIONES );
          Tabla.TabVisible := GetGlobalBooleano( K_GLOBAL_LABOR_USA_DEFSTEPS ) AND StrLleno( sOperaciones );
          Tabla.Caption := sOperaciones;
          if pagecontrol.Pages[1].TabVisible = true then
            dxBarButton_ExportarBtn.Visible := ivAlways
          else
            dxBarButton_ExportarBtn.Visible := ivNever;
     end;
     PageControl.ActivePage := Datos;
     dxBarButton_BuscarBtn.Enabled:=false;
     {$ifdef INTERRUPTORES}
      AR_CODIGO.Width:= K_WIDTHLLAVE;
     {$endif}
end;

procedure TEditPartes.Connect;
begin
     with dmLabor do
     begin
          //cdsPartes.Conectar;
          //Se pone esto para que el Grid de Pasos
          //Siempre salga ordenado por Numero de Paso
          cdsDefSteps.IndexFieldNames := 'DF_SEQUENC';

          DataSource.DataSet := cdsPartes;
          dsRenglon.DataSet := cdsDefSteps;
     end;
     dmLabor.RefrescaPartes;
end;

procedure TEditPartes.dxBarButton_BuscarBtnClick(Sender: TObject);
begin
  inherited;
  Buscar;
end;

procedure TEditPartes.Buscar;
 var sLlave, sDescripcion : string;
begin
     with GridRenglones.SelectedField do
     begin
          if ( FieldName = 'OP_NUMBER' ) OR
             ( FieldName = 'OP_NOMBRE' ) then
             if TressShell.BuscaDialogo( enOpera, '', sLlave, sDescripcion ) then
             begin
                  with dmLabor.cdsDefSteps do
                  begin
                       if State = dsBrowse then
                          Edit;
                       FieldByName('OP_NUMBER').AsString := sLlave;
                  end;
             end;
     end;
end;

procedure TEditPartes.PageControlChange(Sender: TObject);
begin
     inherited;
     SetBuscaBtn;
end;

procedure TEditPartes.KeyDown( var Key: Word; Shift: TShiftState );
begin
     if ( Key <> 0 ) AND
        ( ssCtrl in Shift )  AND { CTRL }
        ( Key = 66 )  then { Letra B = Buscar }
     begin
          Key := 0;
          Buscar;
     end;
     inherited KeyDown( Key, Shift );
end;

procedure TEditPartes.HabilitaControles;
begin
     inherited HabilitaControles;
     SetBuscaBtn;
end;

procedure TEditPartes.SetBuscaBtn;
begin
     //BuscarBtn.Enabled := PageControl.ActivePage = Tabla;
end;

procedure TEditPartes.BArribaOrdenClick(Sender: TObject);
begin
     inherited;
     Intercambia( TRUE );
end;

procedure TEditPartes.BAbajoOrdenClick(Sender: TObject);
begin
     inherited;
     Intercambia( FALSE );
end;

procedure TEditPartes.Intercambia(const lUp: Boolean);
var
    Actual : TBookmark;
    Actual2 : TBookmark;
    nActual : Integer;
    nOtro : Integer;
    lExiste : Boolean;
begin
    with dmLabor.cdsDefSteps do
    begin
        nActual := GetPosicion;
        Actual  := GetBookmark;
        if ( lUP ) then
            lExiste := FindPrior
        else
            lExiste := FindNext;
        if lExiste then
        begin
            nOtro := GetPosicion;
            SetPosicion( -1 );
            Actual2 := GetBookmark;
            GotoBookmark( Actual );
            SetPosicion( nOtro );
            GotoBookmark( Actual2 );
            SetPosicion( nActual );
            FreeBookmark( Actual2 );
            if ( lUP ) then Prior
            else Next;
        end;
        FreeBookmark( Actual );
    end;
end;

function TEditPartes.GetPosicion: Integer;
begin
    Result := dmLabor.cdsDefSteps.FieldByName( 'DF_SEQUENC' ).AsInteger;
end;

procedure TEditPartes.GridRenglonesEnter(Sender: TObject);
begin
  inherited;
  dxBarButton_BuscarBtn.Enabled := true;
end;

procedure TEditPartes.GridRenglonesExit(Sender: TObject);
begin
  inherited;
  dxBarButton_BuscarBtn.Enabled := false;
end;

procedure TEditPartes.SetPosicion(const nPos: Integer);
begin
    with dmLabor.cdsDefSteps do
    begin
        Edit;
        FieldByName( 'DF_SEQUENC' ).AsInteger := nPos;
        Post;
    end;
end;

procedure TEditPartes.BuscarBtnClick(Sender: TObject);
begin
     Buscar;
end;

end.

