unit FBaseCedulas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicionRenglon_DevEx, Db, Grids, DBGrids, ZetaDBGrid, ComCtrls, ExtCtrls,
  DBCtrls, Buttons, StdCtrls, ZetaFecha, ZetaNumero, Mask, ZetaHora,
  ZetaKeyLookup, ZetaKeyCombo, ZetaMessages, ZetaEdit, ImgList,
  {$ifndef VER130}Variants,{$endif}  
  ZetaDBTextBox, ZetaSmartLists;

type
  TBaseCedulas = class(TBaseEdicionRenglon_DevEx)
    DepurarLista: TBitBtn;
    ImageEmp: TImageList;
    PageControl1: TPageControl;
    Generales: TTabSheet;
    Moduladores: TTabSheet;
    PanelComenta: TPanel;
    LblComenta: TLabel;
    CE_COMENTA: TZetaDBEdit;
    PanelModula3: TPanel;
    LblModula3: TLabel;
    CE_MOD_3: TZetaDBKeyLookup;
    PanelModula2: TPanel;
    LblModula2: TLabel;
    CE_MOD_2: TZetaDBKeyLookup;
    PanelModula1: TPanel;
    LblModula1: TLabel;
    CE_MOD_1: TZetaDBKeyLookup;
    PanelStatus: TPanel;
    LblStatus: TLabel;
    CE_STATUS: TZetaDBKeyCombo;
    Panel3: TPanel;
    PanelHora: TPanel;
    LblHora: TLabel;
    LblCantidad: TLabel;
    LblFecha: TLabel;
    CE_HORA: TZetaDBHora;
    CE_PIEZAS: TZetaDBNumero;
    CE_FECHA: TZetaDBFecha;
    PanelOrden: TPanel;
    LblOrden: TLabel;
    MultiLote: TBitBtn;
    WO_NUMBER: TZetaDBKeyLookup;
    PanelParte: TPanel;
    LblParte: TLabel;
    AR_CODIGO: TZetaDBKeyLookup;
    PanelArea: TPanel;
    LblArea: TLabel;
    CE_AREA: TZetaDBKeyLookup;
    PanelOpera: TPanel;
    LblOpera: TLabel;
    OP_NUMBER: TZetaDBKeyLookup;
    ListaEmpleados: TSpeedButton;
    BtnTemp: TButton;
    Panel4: TPanel;
    lbUsuario: TLabel;
    US_CODIGO: TZetaDBTextBox;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure WO_NUMBERValidKey(Sender: TObject);
    procedure DepurarListaClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure MultiLoteClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure CE_AREAValidKey(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure BBBorrarClick(Sender: TObject);
    procedure DataSourceStateChange(Sender: TObject);
    procedure GridRenglonesDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure ListaEmpleadosClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure BtnTempEnter(Sender: TObject);
  private
    { Private declarations }
    FArea: String;
    FCedulaCaption: String;
    function TieneDerechoModuladores: Boolean;
    procedure BuscaEmpleado;
    procedure SeleccionaSiguienteRenglon;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    procedure SetEmpTabCaption;
    procedure SetFormaPosicion;
    procedure InitOperacion;
    procedure SetBotonEmpleados;
    procedure SetControlesEmpleados;
    procedure InitControlesEmpleados( const lVisible: Boolean );
  protected
    FValidaHora: Boolean;
    function ValoresGrid: Variant;override;
    function TieneDerechoGenerales: Boolean;
    //function DerechoControles: boolean;
    procedure InitLookupsDataSet; virtual;
    procedure Connect; override;
    procedure DoLookup; override;
    procedure ValidaDatosLabor; virtual;
    procedure SetControlesVisibles; virtual;
    procedure SetControlesHabilitados; virtual;
    procedure EscribirCambios; override;
    procedure HabilitaControles; override;
    procedure KeyDown( var Key: Word; Shift: TShiftState ); override;
    procedure KeyPress( var Key: Char ); override; { TWinControl }
    function GetExtrasControles: integer;virtual;

  public
    { Public declarations }
    property CedulaCaption: String read FCedulaCaption write FCedulaCaption;
  end;

const
     K_ALTURA_MODULADORES = 150;
     MARGEN_ALTURA = 33;
     EMP_CAPTION = '&Empleados%s';
     COLUMNA_IMAGEN1 = 0;
     COLUMNA_IMAGEN2 = 1;
     COLUMNA_CODIGO = 2;
var
  BaseCedulas: TBaseCedulas;

implementation

uses dLabor, dGlobal, DCliente, ZGlobalTress, ZetaLaborTools, ZetaCommonTools,
     ZetaCommonLists,ZetaBuscaEmpleado_DevEx, ZAccesosTress,
     ZAccesosMgr, ZetaCommonClasses, ZetaDialogo,
  DSistema;

{$R *.DFM}

procedure TBaseCedulas.FormCreate(Sender: TObject);
begin
     inherited;
     InitLookupsDataSet;
     FValidaHora:= TRUE;
     //FirstControl := CE_FECHA;            - Se est� reasignando en el FormShow.SetControlesHabilitados
{$ifdef SUPERVISORES}
     CE_AREA.Filtro := MIS_AREAS;
     IndexDerechos := D_SUPER_CEDULAS;
     HelpContext := 9504;                    // Falta Constante para HelpContext en Supervisores
{$else}
     IndexDerechos := D_LAB_CEDULAS;
     HelpContext := H00008_Cedulas_de_captura;
{$endif}
end;

procedure TBaseCedulas.FormShow(Sender: TObject);
begin
     SetControlesVisibles;
     Panel1.Height := iMax( GetAltura( Panel3 ) + MARGEN_ALTURA + GetExtrasControles, K_ALTURA_MODULADORES );
     SetControlesHabilitados;
     WO_NUMBER.SetLlaveDescripcion( VACIO, VACIO );    // Requerido cuando se Cambia de Empresa, por que la Forma no se destruye y el control se queda con la �ltima orden capturada
     inherited;
     InitOperacion;
     SetBotonEmpleados;
     SetControlesEmpleados;
end;

{
function TBaseCedulas.DerechoControles: boolean;
begin
{$ifdef SUPERVISORES
     Result := ZAccesosMgr.CheckDerecho( D_SUPER_CEDULAS, K_DERECHO_SIST_KARDEX );
{$else
     Result := ZAccesosMgr.CheckDerecho( D_LAB_CEDULAS, K_DERECHO_SIST_KARDEX );
{$endif
end;
}

procedure TBaseCedulas.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmLabor do
     begin
          cdsArea.Conectar;
          if ( PanelOrden.Visible ) or ( PanelParte.Visible ) then
             cdsPartes.Conectar;
          if PanelOpera.Visible then
             cdsOpera.Conectar;
          if PanelModula1.Visible then
             cdsModula1.Conectar;
          if PanelModula2.Visible then
             cdsModula2.Conectar;
          if PanelModula3.Visible then
             cdsModula3.Conectar;

          cdsCedulas.Conectar;
          DataSource.DataSet:= cdsCedulas;
          with cdsCedEmp do
          begin
               Conectar;
               First;
          end;
          dsRenglon.DataSet := cdsCedEmp;
          cdsCedMulti.Conectar;

          FArea := CE_AREA.Llave;
          SetEmpTabCaption;
     end;
end;

procedure TBaseCedulas.DoLookup;
begin
     if GridEnfocado then
        if ( GridRenglones.SelectedField.FieldName = 'CB_CODIGO' ) then
           BuscaEmpleado;
end;

procedure TBaseCedulas.BuscaEmpleado;
var
   sKey, sDescription: String;
begin
     {***DevEx (by am): Se agrego el if de la vista por si la pantalla par agregar cedulas es utilizada en Labor
                u otro ejecutable que no sea parte del proyecto NIS***}
     {if dmCliente.GetDatosUsuarioActivo.Vista = tvClasica then
     begin
          if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
          begin
               with dmLabor.cdsCedEmp do
                begin                                                            //:todo
                     if not ( State in [ dsEdit, dsInsert ] ) then
                        Edit;
                     FieldByName('CB_CODIGO').AsString:= sKey;
                end;
          end;
     end
     else
     begin }
           if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
          begin
               with dmLabor.cdsCedEmp do
                begin
                     if not ( State in [ dsEdit, dsInsert ] ) then
                        Edit;
                     FieldByName('CB_CODIGO').AsString:= sKey;
                end;
          end;
     //end;
     {***}
end;

procedure TBaseCedulas.ValidaDatosLabor;
begin
     if StrVacio( CE_HORA.Valor ) then
        ReportaControlError( CE_HORA, 'Debe Especificarse la Hora' );
     if ( CE_PIEZAS.Visible ) and ( CE_PIEZAS.Valor < 0 ) then
        ReportaControlError( CE_PIEZAS, LblCantidad.Caption + ' Debe Ser Mayor o Igual Que Cero' );
     ValidaControlLookup( WO_NUMBER, LblOrden );
     ValidaControlLookup( AR_CODIGO, LblParte );
     ValidaControlLookup( CE_AREA, LblArea );
     ValidaControlLookup( OP_NUMBER, LblOpera );
end;

procedure TBaseCedulas.EscribirCambios;
begin
     ValidaDatosLabor;
     inherited;
end;

procedure TBaseCedulas.DataSourceDataChange(Sender: TObject; Field: TField);
var
   lMulti : Boolean;
begin
     if ( Field = Nil ) or ( Field.FieldName = 'CE_MULTI' ) then
     begin
          lMulti := zStrToBool( dmLabor.cdsCedulas.FieldByName( 'CE_MULTI' ).AsString );
          self.WO_NUMBER.Enabled := TieneDerechoGenerales and ( not lMulti );
     end;
     if ( Field = Nil ) or ( Field.FieldName = 'CE_AREA' ) then
     begin
          with CE_AREA do
          begin
               if not StrVacio( Descripcion ) then
                   Caption := CedulaCaption + ' por ' + Descripcion
               else
                   Caption := CedulaCaption;
          end;
     end;
     inherited;
end;

procedure TBaseCedulas.DataSourceStateChange(Sender: TObject);
begin
     inherited;
     SetEmpTabCaption;
end;

procedure TBaseCedulas.InitLookupsDataSet;
begin
     with dmLabor do
     begin
          WO_NUMBER.LookupDataSet := cdsWOrderLookup;
          AR_CODIGO.LookupDataSet := cdsPartes;
          CE_AREA.LookupDataSet := cdsArea;
          OP_NUMBER.LookupDataSet := cdsOpera;
          CE_MOD_1.LookupDataSet := cdsModula1;
          CE_MOD_2.LookupDataSet := cdsModula2;
          CE_MOD_3.LookupDataSet := cdsModula3;
     end;
end;

procedure TBaseCedulas.SetControlesVisibles;
const
     DOSPUNTOS = ':';
begin
     LblArea.Caption := GetLaborLabel( K_GLOBAL_LABOR_AREA ) + DOSPUNTOS;
     LblCantidad.Caption := Global.GetGlobalString( K_GLOBAL_LABOR_PIEZAS );
     LblCantidad.Visible := strLleno( LblCantidad.Caption );
     CE_PIEZAS.Visible := LblCantidad.Visible;
     LblCantidad.Caption := LblCantidad.Caption + DOSPUNTOS;
     SetPanelLabel( PanelOrden, LblOrden, K_GLOBAL_LABOR_ORDEN );
     SetPanelLabel( PanelParte, LblParte, K_GLOBAL_LABOR_PARTE );
     SetPanelLabel( PanelOpera, LblOpera, K_GLOBAL_LABOR_OPERACION );
     SetPanelLabel( PanelModula1, LblModula1, K_GLOBAL_LABOR_MODULA1 );
     SetPanelLabel( PanelModula2, LblModula2, K_GLOBAL_LABOR_MODULA2 );
     SetPanelLabel( PanelModula3, LblModula3, K_GLOBAL_LABOR_MODULA3 );

     Multilote.Visible := Global.GetGlobalBooleano( K_GLOBAL_LABOR_MULTILOTES );

     InitControlesEmpleados( CheckDerechos( K_DERECHO_SIST_KARDEX ) );   // Bit #5 de IndexDerechos 'Editar Lista de Empleados' ( D_SUPER_CEDULAS � D_LAB_CEDULAS )
end;

procedure TBaseCedulas.InitControlesEmpleados( const lVisible: Boolean );
Begin
     //Panel2.Visible := DerechoControles;
     Panel2.Visible := lVisible;
     dsRenglon.AutoEdit := lVisible;
     ListaEmpleados.Visible := lVisible;
     with GridRenglones do
     begin
          Columns[ COLUMNA_IMAGEN1 ].Visible := lVisible and dmCliente.ModoSuper;
          Columns[ COLUMNA_IMAGEN2 ].Visible := lVisible and dmCliente.ModoSuper;
          SelectedIndex := COLUMNA_CODIGO;
          ReadOnly := Not ( lVisible );
          if ReadOnly then
             Options := [ dgRowSelect, dgTitles, dgColumnResize, dgColLines, dgRowLines ]
          else
             Options := [ dgEditing, dgTitles, dgIndicator, dgColumnResize, dgTabs, dgColLines, dgRowLines ];
     end;
end;

function TBaseCedulas.TieneDerechoGenerales: Boolean;
begin
     Result := CheckDerechos( K_DERECHO_ADICIONAL9 );                // Bit #9 de IndexDerechos 'Capturar Datos Generales de la C�dula' ( D_SUPER_CEDULAS � D_LAB_CEDULAS )
end;

function TBaseCedulas.TieneDerechoModuladores: Boolean;
begin
     Result := CheckDerechos( K_DERECHO_ADICIONAL10 );               // Bit #10 de IndexDerechos 'Capturar Moduladores de la C�dula' ( D_SUPER_CEDULAS � D_LAB_CEDULAS )
end;

procedure TBaseCedulas.SetControlesHabilitados;
begin
     // Generales
     HabilitaPanel( PanelHora, TieneDerechoGenerales );
     HabilitaPanel( PanelOrden, TieneDerechoGenerales );
     HabilitaPanel( PanelParte, TieneDerechoGenerales and ( not PanelOrden.Visible ) );
     HabilitaPanel( PanelArea, TieneDerechoGenerales );
     HabilitaPanel( PanelOpera, TieneDerechoGenerales );
     // Moduladores
     HabilitaPanel( PanelStatus, TieneDerechoModuladores );
     HabilitaPanel( PanelComenta, TieneDerechoModuladores );
     HabilitaPanel( PanelModula1, TieneDerechoModuladores );
     HabilitaPanel( PanelModula2, TieneDerechoModuladores );
     HabilitaPanel( PanelModula3, TieneDerechoModuladores );

     if TieneDerechoModuladores and ( not TieneDerechoGenerales ) then     // Si no tiene Permiso para Generales y si Moduladores se Activa la Pesta�a de Moduladores
     begin
          PageControl1.ActivePage := Moduladores;
          FirstControl := CE_STATUS;
     end
     else
     begin
          PageControl1.ActivePage := Generales;
          FirstControl := CE_FECHA;
     end;
    CE_PIEZAS.Enabled := not zStrToBool( dmLabor.cdsCedulas.FieldByName( 'CE_MULTI' ).AsString );
    LblCantidad.Enabled := CE_PIEZAS.Enabled;
end;

procedure TBaseCedulas.WO_NUMBERValidKey(Sender: TObject);
begin
     inherited;
     if ( WO_NUMBER.Llave <> VACIO ) and ( AR_CODIGO.Visible ) then
     begin
          AR_CODIGO.Llave := WO_NUMBER.LookUpDataSet.FieldByName( 'AR_CODIGO' ).AsString;
     end;
end;

procedure TBaseCedulas.BBAgregarClick(Sender: TObject);
begin
     inherited;
     SetEmpTabCaption;
end;

procedure TBaseCedulas.BBBorrarClick(Sender: TObject);
begin
     inherited;
     SetEmpTabCaption;
end;

procedure TBaseCedulas.DepurarListaClick(Sender: TObject);
begin
{ifdef SUPERVISORES}
     if CheckDerechosPadre then
        dmLabor.DepurarListaEmpleados;
     SetEmpTabCaption;
{endif}
end;

procedure TBaseCedulas.MultiLoteClick(Sender: TObject);
begin
     CE_PIEZAS.Enabled := NOT dmLabor.ShowMultiLote;
     LblCantidad.Enabled := CE_PIEZAS.Enabled;
     //self.MultiLote.down := zStrToBool( dmLabor.cdsCedulas.FieldByName( 'CE_MULTI' ).AsString );
end;

procedure TBaseCedulas.CE_AREAValidKey(Sender: TObject);
var
   lEnabled : boolean;
begin
     inherited;
     if StrLleno( CE_AREA.Llave ) and ( FArea <> CE_AREA.Llave ) then
     begin
          if ( PanelOpera.Visible ) and TieneDerechoGenerales then
          begin
               lEnabled := not( zStrToBool( dmLabor.cdsArea.FieldByName( 'TB_OP_UNI' ).AsString ) );
               if ( ActiveControl.ClassName = 'TZetaLlave' ) and ( ActiveControl.Parent = OP_NUMBER ) and  ( Not lEnabled )  then
                  SetOk;
               OP_NUMBER.Enabled := lEnabled;
               lblOpera.Enabled := lEnabled;
               if ( ActiveControl = BtnTemp ) and lEnabled then
                  OP_NUMBER.SetFocus;
               OP_NUMBER.Llave := CE_AREA.LookUpDataSet.FieldByName('TB_OPERA').AsString;
          end;
          FArea := CE_AREA.LLave;
     end;
end;

procedure TBaseCedulas.HabilitaControles;
begin
     inherited;
     //BuscarBtn.Enabled := TRUE;
     //ImprimirBtn.Enabled := TRUE;
end;

procedure TBaseCedulas.KeyDown(var Key: Word; Shift: TShiftState);
begin
     if ( GridEnfocado) then
     begin
          if ( ssCtrl in Shift ) then { CTRL }
             case Key of
                   66:  { Letra F = Buscar }
                   begin
                        Key := 0;
                        DoLookup;
                   end;
              end;
          if ( Key = VK_RETURN ) then
          begin
               Key := 0;
          end;
     end;
     inherited KeyDown( Key, Shift );
end;

procedure TBaseCedulas.KeyPress(var Key: Char);
begin
     if ( GridEnfocado ) then
     begin
          if ( Key = Chr( VK_RETURN ) ) then
          begin
               Key := #0;
               SeleccionaSiguienteRenglon;
          end
          else
              if ( Key = Chr( VK_ESCAPE ) ) then
              begin
                   GridRenglones.selectedIndex := COLUMNA_CODIGO;
              end;
     end
     else if ActiveControl.TabStop then
          inherited KeyPress( Key );
end;

procedure TBaseCedulas.SeleccionaSiguienteRenglon;
begin
     with dmLabor.cdsCedEmp do
     begin
          Next;
          if Eof then
             Self.Agregar;
     end;
end;

procedure TBaseCedulas.WMExaminar(var Message: TMessage);
begin
     if ( TExaminador(  Message.LParam ) = exEnter ) then
        if GridEnfocado then
           SeleccionaSiguienteRenglon;
end;

procedure TBaseCedulas.OKClick(Sender: TObject);
begin
     dmLabor.DepuraCedDataSets;
     inherited;
     if dmLabor.cdsCedulas.ChangeCount = 0 then    //Si se aplicaron los cambios
        Close;
end;

function TBaseCedulas.ValoresGrid : Variant;
begin
     Result := VarArrayOf( [ dmCliente.GetValorActivoStr( stFecha ),
                             '',stFecha,stNinguno] );
end;

procedure TBaseCedulas.SetEmpTabCaption;
var
   iCuantos: Integer;
begin
     iCuantos := dmLabor.cdsCedEmp.RecordCount;
     if iCuantos > 0 then
        Tabla.Caption := Format( EMP_CAPTION, [ ' (' + IntToStr( iCuantos ) + ')' ] )
     else
        Tabla.Caption := Format( EMP_CAPTION, [ VACIO ] );
end;

procedure TBaseCedulas.GridRenglonesDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer;
          Column: TColumn; State: TGridDrawState);
{$ifdef SUPERVISORES}
var
   iImagen: Integer;
{$endif}
begin
     inherited;
{$ifdef SUPERVISORES}
     if ( Column.FieldName = 'CB_TIPO' ) then
     begin
          if ( Column.Field.DataSet.FieldByName( 'CB_CODIGO' ).AsInteger = 0 ) then
             iImagen := 0
          else
             iImagen := Abs( Column.Field.AsInteger - 1 );
          ImageEmp.Draw( TDBGrid( Sender ).Canvas, Rect.left, Rect.top + 1, iImagen );
     end
     else if ( Column.FieldName = 'CHECADAS' ) and ( Column.Field.AsInteger > 0 ) then
        ImageEmp.Draw( TDBGrid( Sender ).Canvas, Rect.left, Rect.top + 1, 2 );
{$endif}
end;

procedure TBaseCedulas.SetFormaPosicion;
begin
     if PageControl.Visible then
        self.Height := Trunc( self.Monitor.Height * 0.90 )
     else
         Self.ClientHeight := Panel1.Height + {PanelSuperior.Height//:todo }  PanelBotones.Height; // + PanelIdentifica.Height;   - No se muestra el PanelIdentifica
     self.Top :=  Trunc( ( self.monitor.Height - self.Height ) / 2 );
end;

procedure TBaseCedulas.ListaEmpleadosClick(Sender: TObject);
begin
     if not Self.ListaEmpleados.Down then
     begin
{ifdef SUPERVISORES}
          if CheckDerechosPadre then
             dmLabor.DepurarListaEmpleados;
          SetEmpTabCaption;
{endif}
     end;
     with ClientDataSet do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
     end;
     SetControlesEmpleados;
     if Self.ListaEmpleados.Down then
        GridRenglones.SetFocus;
end;

procedure TBaseCedulas.SetControlesEmpleados;
begin
     with PageControl do
     begin
          Visible := Self.ListaEmpleados.Down;
          ActivePage := Tabla;
     end;
     GridRenglones.SelectedIndex := COLUMNA_CODIGO;
     SetFormaPosicion;
     Application.ProcessMessages;
end;

procedure TBaseCedulas.SetBotonEmpleados;
begin
     Self.ListaEmpleados.Down := ( dmLabor.cdsCedEmp.RecordCount > 0 );
end;

procedure TBaseCedulas.CancelarClick(Sender: TObject);
begin
     inherited;
     SetBotonEmpleados;
     SetControlesEmpleados;
     SetControlesHabilitados;
end;

procedure TBaseCedulas.InitOperacion;
var
   sTexto: string;
begin
     if ( CE_AREA.Llave <> VACIO ) and ( PanelOpera.Visible ) and TieneDerechoGenerales and ( dmlabor.cdsArea.LookupKey( CE_AREA.Llave, VACIO, sTexto ) ) then
     begin
          with OP_NUMBER do
          begin
               Enabled := not( zStrToBool( dmLabor.cdsArea.FieldByName( 'TB_OP_UNI' ).AsString ) );
               lblOpera.Enabled := Enabled;
          end;
     end;
end;

procedure TBaseCedulas.BtnTempEnter(Sender: TObject);
begin
     inherited;
     SetOk;
end;

//procedure TBaseCedulas.AgregarTodosClick(Sender: TObject);
{$ifdef SUPERVISORES}
{var
   lTodos : Boolean;}
{$endif}
//begin
{$ifdef SUPERVISORES}
  {   if CheckDerechosPadre then
     begin
          lTodos := StrVacio( CE_AREA.Llave );
          if lTodos then
             dmLabor.DepurarListaEmpleados;
          dmLabor.LlenaListaCedEmp( VACIO, lTodos );     // Agregar todos los que Tengan area vacia
     end;
     SetEmpTabCaption;}
{$endif}
//end;

function TBaseCedulas.GetExtrasControles: integer;
begin
     Result := 0;
end;

end.
