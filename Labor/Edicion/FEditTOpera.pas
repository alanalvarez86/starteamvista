unit FEditTOpera;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, Mask, DBCtrls, StdCtrls, ExtCtrls, Buttons,
     ZetaEdit, ZBaseTablas_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, TressMorado2013, cxControls,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, Vcl.ImgList, ZetaNumero,
  cxNavigator, cxDBNavigator, cxButtons;

type
  TEditTOpera = class(TEditTablas_DevEx)
  private
    { Private declarations }
  protected
    procedure AfterCreate; override;
  public
    { Public declarations }
    procedure DoLooKup;override;
  end;

var
  EditTOpera: TEditTOpera;

implementation

uses DLabor,
     DGlobal,
     ZGlobalTress,
     ZetaCommonClasses,
     ZAccesosTress,
     ZetaBuscador_DevEx;

{$R *.DFM}

procedure TEditTOpera.AfterCreate;
begin
     inherited AfterCreate;
     Caption := dmLabor.cdsTOpera.LookupName;
     ZetaDataset := dmLabor.cdsTOpera;
     HelpContext := H9513_Clasificacion_de_operaciones;
     IndexDerechos := ZAccesosTress.D_LAB_CAT_TIPO_OPERACION;
end;

procedure TEditTOpera.DoLookup;
begin
     inherited;
     //ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Caption, 'TO_CODIGO', dmLabor.cdsTOpera );
end;

end.
