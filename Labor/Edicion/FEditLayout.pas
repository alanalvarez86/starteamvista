
unit FEditLayout;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicion_DevEx, Mask, ZetaNumero, ZetaKeyLookup, StdCtrls,
  ZetaEdit, DB, ExtCtrls, DBCtrls, ZetaSmartLists, Buttons, Grids,
//:Todo  GIFImage, fcpanel, fctrackbar,
  ImgList,FMapasProduccion,ZetaDialogo, gtClasses3,
  gtCstDocEng, gtCstPlnEng, gtCstPDFEng, gtExPDFEng, gtPDFEng,Printers,
  Vcl.Imaging.GIFImg, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, TressMorado2013, cxControls,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, cxNavigator,
  cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx, cxContainer, cxEdit, cxTrackBar,
  dxZoomTrackBar, cxLabel, cxSplitter, dxGDIPlusClasses;



type
  TEditPlantillas = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    LY_CODIGO: TZetaDBEdit;
    Label2: TLabel;
    LY_NOMBRE: TZetaDBEdit;
    Label3: TLabel;
    LY_AREA: TZetaDBKeyLookup_DevEx;
    Label4: TLabel;
    LY_INGLES: TZetaDBEdit;
    Label5: TLabel;
    Label6: TLabel;
    LY_TEXTO: TZetaDBEdit;
    LY_NUMERO: TZetaDBNumero;
    dsMaquinas: TDataSource;
    ListaImagenes: TImageList;
    btnEmpleado: TcxButton;
    imgEmpleado: TImage;
    GroupBox1: TGroupBox;
    gtPDFGenerador: TgtPDFEngine;
    imgMapaAux: TImage;
    ZoomIn: TcxButton;
    LblPorcentajeZoom: TcxLabel;
    ZoomOut: TcxButton;
    trkZoom: TdxZoomTrackBar;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    GridMaquina: TDrawGrid;
    GridLayout: TDrawGrid;
    cxSplitter1: TcxSplitter;
    procedure FormCreate(Sender: TObject);
    procedure btnClearLayoutClick(Sender: TObject);
    procedure Image4Click(Sender: TObject);
    procedure GridLayoutSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure GridLayoutDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure GridMaquinaDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure GridMaquinaSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure trkZoomChange(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure ExportarBtnClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure GridLayoutMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure GridLayoutMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure FormMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure ZoomOutClick(Sender: TObject);
    procedure ZoomInClick(Sender: TObject);
    procedure trkZoomPropertiesChange(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
  private
    FEstadoActual:TEstados;
    FImagenSelecc:TPicture;
    FIndiceMaq:Integer;
    FCodigoClip:string;
    MovingMaquina : Boolean;
    FMultipleSelecc:Boolean;
    procedure GrabarMatriz;
    procedure AgregarMaquina(Elemento: TElemento;i,j:Integer);
    procedure AgregarSilla(Elemento: TElemento;i,j:Integer);

    procedure LimpiarMatriz;
    procedure EditMode;
    function SillasDesocupadas(var i,j:integer): Boolean;
    { Private declarations }
  public
    { Public declarations }
  protected
     procedure Connect; override;
     procedure EscribirCambios;override;
     procedure WMMouseWheel(var Message : TWMMouseWheel); message WM_MOUSEWHEEL;
  end;

var
  EditPlantillas: TEditPlantillas;

  Maquinas : TList;


implementation

uses
    ZAccesosTress,ZetaCommonLists,ZetaCommonClasses,DLabor,DCatalogos,DGlobal,ZGlobalTress,ZetaCommonTools,
  Types;

{$R *.dfm}

procedure TEditPlantillas.FormCreate(Sender: TObject);
begin
     inherited;
     //DataSource.DataSet := dmLabor.cdsLayouts;
     IndexDerechos := D_LAB_PLANTILLAS;
end;

procedure TEditPlantillas.WMMouseWheel(var Message : TWMMouseWheel);
begin
end;

procedure TEditPlantillas.ZoomInClick(Sender: TObject);
begin
  inherited;
  trkZoom.Position := trkZoom.Position - trkZoom.Properties.TrackSize;
end;

procedure TEditPlantillas.ZoomOutClick(Sender: TObject);
begin
  inherited;
  trkZoom.Position := trkZoom.Position + trkZoom.Properties.TrackSize;
end;

procedure TEditPlantillas.Connect;
var
   Maq:TMaquina;
   i:integer;
   MS: TMemoryStream;
begin
     Caption := Global.GetGlobalString( K_GLOBAL_LABOR_PLANTILLA );
     MS := TMemoryStream.Create;
     Maquinas := TList.Create;
     Maquinas.Clear;
     InicializarLayout;
     MovingMaquina:= False;
     try
        with dmLabor do
        begin
             cdsLayouts.Conectar;
             DataSource.DataSet := dmLabor.cdsLayouts;
             cdsMaquinas.Conectar;
             //cdsMaqCertific.Conectar;
             cdsLayMaquinas.Refrescar;
             cdsArea.Conectar;
             LY_AREA.LookupDataset := cdsArea;
             dsMaquinas.DataSet := cdsMaquinas;
        end;

        with dmLabor.cdsMaquinas do
        begin
             First;
             i:= 0;
             while not eof do
             begin
                  Maq := TMaquina.Create;
                  MS.Clear;
                  Maq.Imagen := TPicture.Create;
                  Maq.Codigo := FieldByName('MQ_CODIGO').AsString;
                  Maq.Nombre := FieldByName('MQ_Nombre').AsString;
                  Maq.Multiple := zStrToBool( FieldByName('MQ_MULTIP').AsString);
                  TBlobField(FieldByName('MQ_IMAGEN')).SaveToStream(MS);
                  GeneraImagen(MS,Maq);
                  If Maq.Imagen.Width > 0 then
                  begin
                       Maquinas.Add(Maq);
                       i:= i+1;
                  end;
                  Next;
             end;
        end;
     finally
            MS.free;
     end;
     GridMaquina.RowCount := i;
     FEstadoActual := Libre;
     FCodigoClip := VACIO;
end;


procedure TEditPlantillas.FormShow(Sender: TObject);
begin
     inherited;
     with dmLabor do
     begin
          FirstControl := LY_CODIGO;
          InicializarLayout;
          CargarMaquinas(cdsLayMaquinas,cdsMaquinas);
          CargarSillas(cdsSillasLayout,cdsSillasMaquina,imgEmpleado.Picture);
          LY_CODIGO.SetFocus;
     end;
end;


procedure TEditPlantillas.btnClearLayoutClick(Sender: TObject);
begin
     inherited;
     if ZConfirm(Caption,Format('� Est�s seguro de limpiar %s ? Se borrar�n todas las %s y operadores creados',[dmLabor.GetLayoutGlobal,dmLabor.GetMaquinaGlobal]),0,mbOk)then
     begin
          LimpiarLayout;
          //InicializarLayout;
          GridLayout.Repaint;
          GridMaquina.Repaint;
          EditMode;
     end;
end;

procedure TEditPlantillas.EditMode;
begin
      with DmLabor.cdsLayouts do
      begin
           if State in [ dsBrowse ] then
              Edit;
      end;
end;

procedure TEditPlantillas.Image4Click(Sender: TObject);
begin
     inherited;
     FImagenSelecc := imgEmpleado.Picture;
     FMultipleSelecc := False;
     FEstadoActual := Silla;
     btnEmpleado.Down := True;
end;


procedure TEditPlantillas.GridLayoutSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
var
   lAsigno:Boolean;
   oSilla:TSilla;
   oMaquina:TMaquina;

procedure DibujarEnMapa;
begin
     EditMode;
     with GridEstados[ACol][ARow] do
     begin
          Estado := FEstadoActual;
          Imagen := FImagenSelecc;
     end;
end;

begin
     if GridEstados[ACol][ARow] = nil then
        GridEstados[ACol][ARow] := TElemento.Crear;

     if( ( GridEstados[ACol][ARow].Estado  = Maquina )or( GridEstados[ACol][ARow].Estado = Silla ) ) then
     begin
          IF  GridEstados[ACol][ARow].Estado = Maquina then
          begin
               MovingMaquina := True;
               FEstadoActual := Maquina;
               FCodigoClip := GridEstados[ACol][ARow].Codigo;
              // RemoverAsignacionMaquina(GridEstados[ACol][ARow].Codigo);
               RemoverSillasAsignadas(ACol,ARow,TMaquina(GridEstados[ACol][ARow]));
          end
          else
              FEstadoActual := Silla;

          FImagenSelecc := GridEstados[ACol][ARow].Imagen;

          GridEstados[ACol][ARow] := nil;
          GridEstados[ACol][ARow] := TElemento.Create;
          EditMode;
     end
     else
     begin
          if ( ( FImagenSelecc <> nil ) or ( GridEstados[ACol][ARow].Estado <> Libre ) )then
          begin
               if GridEstados[ACol][ARow].Estado = Libre then
               begin
                    if ( ( ExisteMaquina ( FCodigoClip ) ) and ( FEstadoActual = Maquina ) and (Not MovingMaquina) and ( not FMultipleSelecc ) )then
                    begin
                         ZWarning(Caption,Format('Esta %s ya existe en %s',[dmLabor.GetMaquinaGlobal,dmLabor.GetLayoutGlobal]),0,mbOk);
                    end
                    else
                    begin
                         if FEstadoActual = Silla then
                         begin
                              oSilla := TSilla.Create;
                              lAsigno := BuscarMaquinaAsignadas (ACol,ARow,TSilla(oSilla));

                              if not lAsigno then
                              begin
                                   ZWarning(Caption,Format('Posici�n inv�lida debe estar al lado de una %s',[dmLabor.GetMaquinaGlobal]),0,mbOk);
                              end
                              else
                              begin
                                   GridEstados[ACol][ARow] := oSilla;
                                   DibujarEnMapa
                              end;
                         end;
                         if FEstadoActual = Maquina then
                         begin
                              oMaquina := TMaquina.Create;
                              with oMaquina do
                              begin
                                   Estado := Maquina;
                                   Codigo := FCodigoClip;
                                   Multiple := FMultipleSelecc;
                              end;
                              if FMultipleSelecc then
                              begin
                                   //if not BuscarMaqMultiple(ACol,ARow,oMaquina) and  ExisteMaquina ( FCodigoClip ) then
                                   //begin
                                   //      ZWarning(Caption,Format('Posici�n Inv�lida debe estar al lado de %s m�ltiple',[dmLabor.GetMaquinaGlobal]),0,mbOk);
                                  // end
                                   //else
                                   //begin
                                        GridEstados[ACol][ARow] := oMaquina;
                                        DibujarEnMapa;
                                        BuscarSillasAsignadas(ACol,ARow,oMaquina);
                                   //end;
                              end
                              else
                              begin
                                   GridEstados[ACol][ARow] := oMaquina;
                                   DibujarEnMapa;
                                   BuscarSillasAsignadas(ACol,ARow,oMaquina);
                              end;
                              if not FMultipleSelecc then
                                 FImagenSelecc:= NIL;
                         end;

                         If ( (FEstadoActual = Maquina) and (not MovingMaquina ) and ( not TMaquina( GridEstados[ACol][ARow] ).Multiple ) )then
                         begin
                              //DibujarEnMapa;
                              //Maquinas[FIndiceMaq] := nil;
                              FImagenSelecc:= NIL;
                              FMultipleSelecc := False;
                         end;
                         GridLayoutDrawCell(Sender,ACol,ARow,GridLayout.CellRect(ACol,ARow),[]);
                    end;
               end
          end
          else
              ZWarning(Caption,Format('Seleccionar %s un operador para dise�ar',[dmLabor.GetMaquinaGlobal]),0,mbOk);
     end;
     
end;

procedure TEditPlantillas.GridLayoutDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
     if GridEstados[ACol][ARow] <> nil then
     begin
           if GridEstados[ACol][ARow].Estado = Libre then
           begin
                GridLayout.Canvas.Brush.Color := clWhite;
                GridLayout.Canvas.FillRect(Rect)
           end
           else if ((GridEstados[ACol][ARow].Estado = Maquina ) or ( GridEstados[ACol][ARow].Estado = Silla ) ) then
           begin
                if ( GridEstados[ACol][ARow].Imagen <> nil ) and (GridEstados[ACol][ARow].Imagen.Width > 0 ) then
                   GridLayout.Canvas.StretchDraw(Rect,GridEstados[ACol][ARow].Imagen.Graphic );
           end;
           btnEmpleado.Down := FEstadoActual = Silla;
     end;
end;



procedure TEditPlantillas.GridMaquinaDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
   ImagenR :TRect;
begin
      with GridMaquina do
      begin
          Canvas.Font.Style := [fsBold];

          if ( Maquinas.Count > 0 )and (Maquinas[ARow] <> nil) then
          begin
               if ExisteMaquina(TMaquina(Maquinas[ARow]).Codigo)then
                   Canvas.Font.Color := clRed
               else
                   Canvas.Font.Color := clGreen;
               with ImagenR do
               begin
                    Left := 0;
                    Top := Rect.Top;
                    Right := 35;
                    Bottom := Rect.Bottom;
               end;
               if TMaquina(Maquinas[ARow]).Imagen.Width > 0 then
                  Canvas.StretchDraw(ImagenR,TMaquina(Maquinas[ARow]).Imagen.Graphic );

               Canvas.TextOut(36,Rect.Top+10,TMaquina(Maquinas[ARow]).Codigo);
               If TMaquina(Maquinas[ARow]).Multiple then
               begin
                    Canvas.Font.Style := [fsBold];
                    Canvas.Font.Color := clBlue;
                    Canvas.TextOut(36,Rect.Top+25,'M�ltiple');
               end;
          end
          else
          begin
               Canvas.Brush.Color := clWhite;
               Canvas.FillRect(Rect)
          end;
      end;
end;

procedure TEditPlantillas.GridMaquinaSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
     inherited;
     if ( Maquinas.Count > 0 )then
     begin
          FImagenSelecc := TMaquina(Maquinas[ARow]).Imagen;
          FMultipleSelecc := TMaquina(Maquinas[ARow]).Multiple;
          MovingMaquina := False;
          FIndiceMaq := ARow;
          FEstadoActual := Maquina;
          FCodigoClip := TMaquina(Maquinas[ARow]).Codigo;
     end;
end;

procedure TEditPlantillas.GrabarMatriz;
var i,j:Integer;
begin
     for i := 0 to K_MAX_COL do
     begin
          for j := 0 to K_MAX_ROW do
          begin
               if ( ( GridEstados[i][j] <> nil ) and ( GridEstados[i][j].Estado <> Libre ) )then
               begin
                    if ( ( GridEstados[i][j].Estado = Maquina ) ) then
                    begin
                         AgregarMaquina( GridEstados[i][j],i,j );
                    end;
                    if ( ( GridEstados[i][j].Estado = Silla ) ) then
                    begin
                         AgregarSilla( GridEstados[i][j],i,j );
                    end;
               end;
          end;
     end;
end;

procedure TEditPlantillas.LimpiarMatriz;
begin
     with dmLabor.cdsLayMaquinas do
     begin
          First;
          while not eof do
          begin
               Delete;
          end;
     end;
     with dmLabor.cdsSillasLayout do
     begin
          First;
          while not eof do
          begin
               Delete;
          end;
     end;

     with dmLabor.cdsSillasMaquina do
     begin
          First;
          while not eof do
          begin
               Delete;
          end;
     end;
end;

procedure TEditPlantillas.AgregarMaquina(Elemento:TElemento;i,j:Integer);
begin
     with dmLabor.cdsLayMaquinas do
     begin
          Append;
          FieldByName('LY_CODIGO').AsString := LY_CODIGO.Valor;
          FieldByName('MQ_CODIGO').AsString := Elemento.Codigo;
          FieldByName('LM_POS_X').AsInteger := i;
          FieldByName('LM_POS_Y').AsInteger := j;
          Post;
     end;
end;

procedure TEditPlantillas.EscribirCambios;
var
   i,j:integer;
   oCursor: TCursor;
begin
     if not SillasDesocupadas(i,j) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             try
                 LimpiarMatriz;
                 GrabarMatriz;
                   inherited;
                 with dmLabor do
                 begin
                      cdsLayMaquinas.Enviar;
                      cdsSillasMaquina.Enviar;
                      cdsSillasLayout.Enviar;
                      LimpiarLayout;
                      InicializarLayout;
                      CargarMaquinas(cdsLayMaquinas,cdsMaquinas);
                      CargarSillas(cdsSillasLayout,cdsSillasMaquina,imgEmpleado.Picture);
                 end;
                 FImagenSelecc := nil;
                 FEstadoActual := Libre;
             except
                  on Error: Exception do
                  begin
                       ZExcepcion('Tress Labor','Excepcion al grabar layout', Error,0 );
                  end;
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end
     else
     begin
          ZWarning(Caption,Format('Se quedo un operador sin %s en %d ,%d ',[dmLabor.GetMaquinaGlobal,i,j]),0,mbOk);
     end;
end;

function TEditPlantillas.SillasDesocupadas(var i,j:integer):Boolean;
begin
     Result := not BuscarSillaAsignadas(i,j);
end;

procedure TEditPlantillas.AgregarSilla(Elemento: TElemento; i,j: Integer);
var
   x:Integer;
begin
     with dmLabor.cdsSillasLayout do
     begin
          Append;
          FieldByName('LY_CODIGO').AsString := LY_CODIGO.Valor;
          FieldByName('SL_CODIGO').AsString := IntToStr(i)+','+IntToStr(j);
          FieldByName('SL_POS_X').AsInteger := i;
          FieldByName('SL_POS_Y').AsInteger := j;
          Post;
     end;

     for x := 0 to TSilla(Elemento).Maquinas.Count -1 do
     begin
          if TElemento(TSilla(Elemento).Maquinas[x]) <> nil then
          begin
               with dmLabor.cdsSillasMaquina do
               begin
                    Append;
                    FieldByName('LY_CODIGO').AsString := LY_CODIGO.Valor;
                    FieldByName('SL_CODIGO').AsString := IntToStr(i)+','+IntToStr(j);
                    FieldByName('MQ_CODIGO').AsString := TElemento(TSilla(Elemento).Maquinas[x]).Codigo;
                    Post;
               end;
          end;
     end;
end;


procedure TEditPlantillas.CancelarClick(Sender: TObject);
begin
     inherited;
     with dmLabor do
     begin
          LimpiarLayout;
          InicializarLayout;
          CargarMaquinas(cdsLayMaquinas,cdsMaquinas);
          CargarSillas(cdsSillasLayout,cdsSillasMaquina,imgEmpleado.Picture);
          GridLayout.Repaint;
     end;
end;

procedure TEditPlantillas.Cancelar_DevExClick(Sender: TObject);
begin
  inherited;
   with dmLabor do
   begin
        LimpiarLayout;
        InicializarLayout;
        CargarMaquinas(cdsLayMaquinas,cdsMaquinas);
        CargarSillas(cdsSillasLayout,cdsSillasMaquina,imgEmpleado.Picture);
        GridLayout.Repaint;
   end;
end;

procedure TEditPlantillas.trkZoomChange(Sender: TObject);
var
   iValor:Integer;
begin
     inherited;
  //:Todo   iValor := Trunc(trkZoom.Position * 30);
     GridLayout.DefaultColWidth := Trunc(iValor div 100);
     GridLayout.DefaultRowHeight:= Trunc(iValor div 100);
     with dmLabor do
     begin
          GridLayout.Repaint;
     end;
end;

procedure TEditPlantillas.trkZoomPropertiesChange(Sender: TObject);
  var
   iValor:Integer;
begin
     inherited;
     iValor := Trunc(trkZoom.Position * 30);
     GridLayout.DefaultColWidth := Trunc(iValor div 100);
     GridLayout.DefaultRowHeight:= Trunc(iValor div 100);
     with dmLabor do
     begin
          GridLayout.Repaint;
     end;
     LblPorcentajeZoom.Caption := IntToStr( trkZoom.Position ) + ' %';
end;

procedure TEditPlantillas.SpeedButton3Click(Sender: TObject);
begin
     inherited;
  //:Todo   trkZoom.Position := trkZoom.Position + trkZoom.Increment;
end;

procedure TEditPlantillas.SpeedButton4Click(Sender: TObject);
begin
     inherited;
    // trkZoom.Position := trkZoom.Position - trkZoom.Increment;
end;

procedure TEditPlantillas.ExportarBtnClick(Sender: TObject);
begin
      try
          with gtPDFGenerador do
          begin
               Page.Orientation := poLandscape;
               FileName := Application.GetNamePath+Format('\%s_%s.pdf',[dmLabor.GetLayoutGlobal,LY_NOMBRE.Text]);

               imgMapaAux.Width := GridLayout.Width;
               imgMapaAux.Height := GridLayout.Height;

               imgMapaAux.Canvas.CopyRect(Rect(0,0,GridLayout.Width ,GridLayout.Height),GridLayout.Canvas,Rect(0,0,GridLayout.Width ,GridLayout.Height ) );

               Page.PaperSize := Letter;
               Preferences.ShowSetupDialog := false;
               BeginDoc;

               TextOut(0.2,0.1,dmLabor.GetLayoutGlobal+':'+ LY_CODIGO.Text +'  ' +LY_NOMBRE.Text );
               TextOut(0.2,0.3,'L�nea : '+ LY_AREA.Descripcion );
               Line(0,0.5,500,0.5);

               DrawImage(0.1,0.6,imgMapaAux.Picture.Graphic);

               EndDoc;

               imgMapaAux.Canvas.FillRect(Rect(0,0,GridLayout.Width,GridLayout.Height));
          end;

      except on Error: Exception do
             begin
                  ZError (sELF.Caption , 'No se pudo exportar el mapa a archivo PDF - ' + Error.Message,0 );
             end;
      end;
end;

procedure TEditPlantillas.FormClose(Sender: TObject;
  var Action: TCloseAction);
var i:Integer;
begin
     inherited;
     LimpiarLayout;
     for i := 0 to Maquinas.Count - 1  do
     begin
          TMaquina(Maquinas[i]).Free;
     end;
     FreeAndNil(Maquinas);
end;

procedure TEditPlantillas.GridLayoutMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
     //inherited;
     if ( ssCtrl in Shift ) then { CTRL }
     begin
   //:Todo       trkZoom.Position := trkZoom.Position - trkZoom.Increment;
     end;
     Handled := True;
end;

procedure TEditPlantillas.GridLayoutMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
     //inherited; //Deshabilitado
     if ( ssCtrl in Shift ) then { CTRL }
     begin
     //:Todo     trkZoom.Position := trkZoom.Position + trkZoom.Increment;
     end;
     Handled := True;
end;

procedure TEditPlantillas.FormMouseWheel(Sender: TObject;
  Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint;
  var Handled: Boolean);
begin
    //inherited;
      Handled := True;
end;

end.
