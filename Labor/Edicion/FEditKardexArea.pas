unit FEditKardexArea;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, ZetaFecha, Db, ExtCtrls, DBCtrls, Buttons,
  ZetaKeyLookup, ZetaHora, ZetaDBTextBox, ZetaSmartLists,
  ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  TressMorado2013, cxControls,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, ImgList,
  cxNavigator, cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx;

type
  TEditKardexArea = class(TBaseEdicion_DevEx)
    KA_FECHA: TZetaDBFecha;
    Label1: TLabel;
    KA_HORA: TZetaDBHora;
    Label6: TLabel;
    lbArea: TLabel;
    CB_AREA: TZetaDBKeyLookup_DevEx;
    Label2: TLabel;
    Label3: TLabel;
    ZetaDBTextBox1: TZetaDBTextBox;
    ZetaDBTextBox2: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
  public
    { Public declarations }
  end;

const
     K_FORM_CAPTION = 'Kardex de %s';

var
  EditKardexArea: TEditKardexArea;

implementation

uses DLabor, ZetaLaborTools, ZetaCommonLists, ZetaCommonClasses, ZGlobalTress, ZAccesosTress;

{$R *.DFM}

procedure TEditKardexArea.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef SUPERVISORES}
     HelpContext := 85000;
     IndexDerechos := ZAccesosTress.D_SUPER_ASIGNAR_AREAS;
     {$else}
     HelpContext := H00051_Kardex_de_areas;
     IndexDerechos := ZAccesosTress.D_LAB_EMP_KARDEX_AREAS;
     {$endif}
     Self.Caption := Format( K_FORM_CAPTION, [ GetLaborLabel( K_GLOBAL_LABOR_AREAS ) ] );
     TipoValorActivo1 := ZetaCommonLists.stEmpleado;
     FirstControl := KA_FECHA;
     CB_AREA.LookupDataset := dmLabor.cdsArea;
     {$ifdef SUPERVISORES}
     dxBarControlContainerItem_DBNavigator.Visible := ivNever;
     {$endif}
end;

procedure TEditKardexArea.Connect;
begin
     with dmLabor do
     begin
          cdsArea.Conectar;
          {$ifdef SUPERVISORES}
          DataSource.DataSet := cdsKardexDiario;
          {$else}
          cdsKarArea.Conectar;
          DataSource.DataSet := cdsKarArea;
          {$endif}
     end;
end;

procedure TEditKardexArea.OKClick(Sender: TObject);
begin
     inherited;
     {$ifdef SUPERVISORES}
     Close;
     {$endif}
end;

end.

