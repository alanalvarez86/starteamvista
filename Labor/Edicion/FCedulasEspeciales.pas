unit FCedulasEspeciales;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FBaseCedulas_DevEx, Db, StdCtrls, ZetaKeyCombo, Grids, DBGrids, ZetaDBGrid,
  ComCtrls, ZetaFecha, ZetaNumero, Mask, ZetaHora, ZetaKeyLookup, ExtCtrls,
  DBCtrls, Buttons, ZetaEdit, ImgList, ZetaDBTextBox, ZetaSmartLists,
  FBaseCedulas, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore, TressMorado2013, cxControls,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ZetaKeyLookup_DevEx, cxPC, cxNavigator,
  cxDBNavigator, cxButtons;

type
  TCedulasEspeciales = class(TBaseCedulas_DevEx)
    Label1: TLabel;
    CE_HORA_I: TZetaHora;
    LblDuracion: TLabel;
    CE_TIEMPO: TZetaDBNumero;
    Label2: TLabel;
    procedure CE_HORA_IExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CE_HORA_IKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure GridRenglonesEnter(Sender: TObject);
    procedure GridRenglonesExit(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure ValidaDatosLabor; override;
  public
    { Public declarations }
  end;

var
  CedulasEspeciales: TCedulasEspeciales;

implementation

uses ZetaCommonTools;

{$R *.DFM}

{ TCedulasEspeciales }

procedure TCedulasEspeciales.FormShow(Sender: TObject);
begin
     inherited;
     FValidaHora:= FALSE;
     CE_HORA_I.Valor := aHoraString( aMinutos( CE_HORA.Valor ) - CE_TIEMPO.ValorEntero );
     FValidaHora:= TRUE;
     dxBarButton_BuscarBtn.Enabled := false;
end;

procedure TCedulasEspeciales.GridRenglonesEnter(Sender: TObject);
begin
  inherited;
    dxBarButton_BuscarBtn.Enabled := true;
end;

procedure TCedulasEspeciales.GridRenglonesExit(Sender: TObject);
begin
  inherited;
  dxBarButton_BuscarBtn.Enabled := false;
end;

procedure TCedulasEspeciales.ValidaDatosLabor;
begin
     inherited;
     if ( CE_TIEMPO.Valor <= 0 ) then
     begin
          ActiveControl := CE_HORA;
          DataBaseError( 'Duraci�n debe ser mayor que Cero' );
     end;
end;

procedure TCedulasEspeciales.CE_HORA_IExit(Sender: TObject);
var
   sInicio, sFinal : String;
   iInicio, iFinal : Integer;
begin
     inherited;
     sInicio := CE_HORA_I.Valor;
     sFinal  := CE_HORA.Valor;
     if StrLleno( sInicio ) and StrLleno( sFinal ) then
     begin
          iInicio := aMinutos( sInicio );
          iFinal := aMinutos( sFinal );
{          if ( iInicio >= iFinal ) then
          begin
               self.ActiveControl := TWinControl( Sender );
               DataBaseError( 'La Hora Inicial debe ser menor que la Hora Final' );
          end
          else }
             CE_TIEMPO.Valor := iFinal - iInicio;
     end;
end;


procedure TCedulasEspeciales.CE_HORA_IKeyPress(Sender: TObject; var Key: Char);
begin
     with ClientDataSet do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
     end;
end;

procedure TCedulasEspeciales.FormCreate(Sender: TObject);
{$ifdef LA_ADA}
var
   CE_CONTEO: TZetaDBNumero;
   lblConteo: Tlabel;
{$endif}
begin
     //self.Caption:= Format( self.Caption, [ CE_AREA.Descripcion ] );
     CedulaCaption := 'Horas Especiales';
     {$ifdef LA_ADA}
     CE_CONTEO := TZetaDBNumero.Create(Self);
     CE_CONTEO.Parent := PanelHora;
     CE_CONTEO.Top := 47;
     CE_CONTEO.left := 328;
     CE_CONTEO.Width := 65;
     CE_CONTEO.Height := 21;
     CE_CONTEO.Visible := TRUE;
     CE_CONTEO.Datafield := 'CE_CONTEO';
     CE_CONTEO.Datasource := DataSource;
     CE_CONTEO.TabOrder := 5;
     CE_CONTEO.Mascara := mnPesos;
     lblConteo := Tlabel.Create(Self);
     lblConteo.Parent := PanelHora;
     lblConteo.Caption := 'Conteo:';
     lblConteo.Left := 288;
     lblConteo.Top := 49;
     lblConteo.Alignment := taRightJustify;
     lblConteo.Visible := TRUE;
     {$endif}

     inherited;
end;

end.
