unit FEditCedInsp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicionRenglon_DevEx, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaHora, Mask,
  ZetaFecha, ZetaKeyLookup, ComCtrls, Grids, DBGrids, ZetaDBGrid,
  ZetaKeyCombo, ZetaDBTextBox, ZetaNumero, ZetaMessages,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters,  
    cxControls, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  Vcl.ImgList, cxPC, cxNavigator, cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx,
  dxBarBuiltInMenu;

type
  TEditCedInsp = class(TBaseEdicionRenglon_DevEx)
    PanelControles: TPanel;
    lbFecha: TLabel;
    CI_FECHA: TZetaDBFecha;
    Label2: TLabel;
    CI_FOLIO: TZetaDBTextBox;
    dsDefectos: TDataSource;
    lbHora: TLabel;
    CI_HORA: TZetaDBHora;
    lbStatus: TLabel;
    CI_TIPO: TZetaDBKeyCombo;
    CI_RESULT: TZetaDBKeyCombo;
    Label1: TLabel;
    WO_NUMBER: TZetaDBKeyLookup_DevEx;
    lbWorder: TLabel;
    AR_CODIGO: TZetaDBKeyLookup_DevEx;
    lbParte: TLabel;
    lbArea: TLabel;
    CI_AREA: TZetaDBKeyLookup_DevEx;
    CI_COMENTA: TDBEdit;
    lbComenta: TLabel;
    CI_TAMLOTE: TZetaDBNumero;
    lbTamLote: TLabel;
    lbMuestra: TLabel;
    CI_MUESTRA: TZetaDBNumero;
    CI_NUMERO1: TZetaDBNumero;
    CI_NUMERO2: TZetaDBNumero;
    CI_OBSERVA: TDBMemo;
    Label3: TLabel;
    lbNum2: TLabel;
    lbnum1: TLabel;
    lbTiempo: TLabel;
    CI_TIEMPO: TZetaDBNumero;
    lbminutos: TLabel;
    Label4: TLabel;
    US_CODIGO: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PageControlChange(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure BBBorrarClick(Sender: TObject);
    procedure GridRenglonesEnter(Sender: TObject);
    procedure WO_NUMBERValidKey(Sender: TObject);
    procedure GridRenglonesExit(Sender: TObject);
    //procedure CE_FOLIOValidKey(Sender: TObject);
  private
    { Private declarations }
    FPuedeModificar: Boolean;
    procedure AsignaColumnas;
    procedure SetBuscaBtn;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    procedure SeleccionaSiguienteColumna;
    procedure HabilitaBusqueda(opc:boolean);
  protected
    procedure Connect; override;
    procedure DoLookup; override;
    function PuedeAgregar( var sMensaje: String ): Boolean;override;
    function PuedeBorrar( var sMensaje: String ): Boolean;override;
    function PuedeModificar( var sMensaje: String ): Boolean;override;
    function CheckDerechosPadre: Boolean;
    procedure KeyDown( var Key: Word; Shift: TShiftState );override;
    procedure KeyPress(var Key: Char);override;
    procedure EscribirCambios;override;
  public
    { Public declarations }
    procedure HabilitaControles;override;
  end;

var
  EditCedInsp: TEditCedInsp;

implementation

uses DLabor,
     DGlobal,
     ZAccesosTress,
     ZGlobalTress,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaLaborTools,
     ZetaTipoEntidad,
     //ZAccesosMgr,
     ZetaDialogo,
     FTressShell ;

{$R *.DFM}

procedure TEditCedInsp.FormCreate(Sender: TObject);
begin
     inherited;
     GridRenglones.Options := [ dgEditing,
                                dgTitles,
                                dgIndicator,
                                dgColumnResize,
                                dgColLines,
                                dgRowLines,
                                dgTabs ];

     FirstControl := CI_FECHA;
     IndexDerechos := D_LAB_CEDULAS_INSP;
     HelpContext := H00052_Cedulas_de_inspeccion;
     with dmLabor do
     begin
          Ar_codigo.LookupDataset := cdsPartes;
          Ci_area.LookupDataset := cdsArea;
          Wo_number.LookupDataset := cdsWOrderLookup;
     end;
     Tabla.Caption := 'Lista de ' + Global.GetGlobalString( K_GLOBAL_LABOR_TDEFECTOS );
end;


procedure TEditCedInsp.Connect;
begin
     with dmLabor do
     begin
          cdsCedInsp.Refrescar;
          DataSource.DataSet := cdsCedInsp;
          dsRenglon.DataSet := cdsDefectos;

          FPuedeModificar := PuedeModificarCedInspeccion;
     end;
end;

procedure TEditCedInsp.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage := Tabla;
     AsignaColumnas;
     dxBarButton_BuscarBtn.Visible:=ivAlways;
     HabilitaBusqueda(false);
     {$ifdef INTERRUPTORES}
      WO_NUMBER.WidthLlave := K_WIDTHLLAVE;
      AR_CODIGO.WidthLlave := K_WIDTHLLAVE;
      CI_AREA.WidthLlave := K_WIDTHLLAVE;

      WO_NUMBER.Width := K_WIDTH_LOOKUP;
      AR_CODIGO.Width := K_WIDTH_LOOKUP;
      CI_COMENTA.Width := 475;
      CI_AREA.Width := K_WIDTH_LOOKUP;
      EditCedInsp.Width := K_WIDTH_CEDULAS;
      {$endif}
end;

procedure TEditCedInsp.AsignaColumnas;
 var lWorder : Boolean;
begin
      AsignaGlobal( K_GLOBAL_LABOR_ORDEN, lbWorder, WO_NUMBER );
      AsignaGlobal( K_GLOBAL_LABOR_PARTE, lbparte, AR_CODIGO );
      AsignaGlobal( K_GLOBAL_LABOR_AREA, lbArea, CI_AREA );

      lWorder := WO_NUMBER.Enabled;
      lbParte.Enabled := lbParte.Enabled AND NOT lWorder;
      AR_CODIGO.Enabled := AR_CODIGO.Enabled AND NOT lWorder;
end;

procedure TEditCedInsp.DoLookup;
var
   sLlave, sDescripcion: String;
begin
     if ( ActiveControl = GridRenglones ) then
     begin
          with GridRenglones.SelectedField do
          begin
               if ( FieldName = 'DE_CODIGO' ) or ( FieldName = 'TB_ELEMENT' ) then
               begin
                    if TressShell.BuscaDialogo( enTDefecto, '', sLlave, sDescripcion ) then
                    begin
                         with dmLabor.cdsDefectos do
                         begin
                              if ( State = dsBrowse ) then
                                 Edit;
                              FieldByName( 'DE_CODIGO' ).AsString := sLlave;
                         end;
                    end;
               end;
          end;
     end;
end;

procedure TEditCedInsp.SetBuscaBtn;
begin
     //:Todo BuscarBtn.Enabled := (PageControl.ActivePage = Tabla) and ( ActiveControl = GridRenglones );
end;

procedure TEditCedInsp.PageControlChange(Sender: TObject);
begin
     inherited;
     SetBuscaBtn;
end;

procedure TEditCedInsp.HabilitaBusqueda(opc:boolean);
begin
  if opc = true then
     dxBarButton_BuscarBtn.Enabled := true
   else
     dxBarButton_BuscarBtn.Enabled := false;
end;

procedure TEditCedInsp.HabilitaControles;
begin
     inherited HabilitaControles;
   {  DBNavigator.Visible := False;
     AgregarBtn.Visible := False;   //:Todo
     BorrarBtn.Visible := False;
     ModificarBtn.Visible := False;}
     SetBuscaBtn;
end;

procedure TEditCedInsp.KeyDown( var Key: Word; Shift: TShiftState );
begin
     if ( Key <> 0 ) and
        ( ssCtrl in Shift ) and { CTRL }
        ( Key = 66 )  then      { Letra B = Buscar }
     begin
          Key := 0;
          DoLookup;
     end;
     inherited KeyDown( Key, Shift );
end;

function TEditCedInsp.PuedeAgregar( var sMensaje: String ): Boolean;
begin
     Result := False;
     if not Result then
     begin
          sMensaje := 'No se puede agregar el registro';
          Result := inherited PuedeAgregar( sMensaje );
     end;
end;

function TEditCedInsp.PuedeBorrar( var sMensaje: String ): Boolean;
begin
     Result := False;
     if not Result then
     begin
          sMensaje := 'No se puede borrar el registro';
          Result := inherited PuedeBorrar( sMensaje );
     end;
end;

function TEditCedInsp.PuedeModificar( var sMensaje: String ): Boolean;
begin
     Result := False;
     if not Result then
     begin
          sMensaje := 'No se puede modificar el registro';
          Result := inherited PuedeModificar( sMensaje );
     end;
end;

procedure TEditCedInsp.BBAgregarClick(Sender: TObject);
begin
     with ClientDatasetHijo do
     begin
          Append;
     end;
     with GridRenglones do
     begin
          SelectedIndex := PrimerColumna;
          SetFocus;
     end;

end;

procedure TEditCedInsp.BBBorrarClick(Sender: TObject);
begin
     with ClientDatasetHijo do
     begin
          if not IsEmpty then
             Delete;
     end;
end;

function TEditCedInsp.CheckDerechosPadre: Boolean;
begin
     Result := TRUE;
end;


procedure TEditCedInsp.GridRenglonesEnter(Sender: TObject);
begin
     inherited;
     SetBuscaBtn;
     HabilitaBusqueda(true);
end;

procedure TEditCedInsp.GridRenglonesExit(Sender: TObject);
begin
  inherited;
  HabilitaBusqueda(false);
end;

procedure TEditCedInsp.WMExaminar(var Message: TMessage);
begin
     if ( TExaminador(  Message.LParam ) = exEnter ) then
     begin
          if GridEnfocado then
          begin
               with ClientDatasetHijo do
               begin
                    if StrVacio( FieldByName('DE_CODIGO').AsString ) then
                       SetOk
                    else
                    begin
                         SeleccionaSiguienteColumna;
                    end;
               end;
          end;
     end;
end;

procedure TEditCedInsp.KeyPress(var Key: Char);
begin
     if ( GridEnfocado ) then
     begin
          if ( Key = Chr( VK_RETURN ) ) then
          begin
               Key := #0;
               SeleccionaSiguienteColumna;
          end
          else
              if ( Key = Chr( VK_ESCAPE ) ) then
              begin
                   GridRenglones.selectedIndex := 0;
              end;
     end
     else
          inherited KeyPress( Key );
end;

procedure TEditCedInsp.SeleccionaSiguienteColumna;
begin
     with GridRenglones do
     begin
          SelectedIndex := PosicionaSiguienteColumna;
          if ( SelectedIndex = PrimerColumna ) then
          begin
               with ClientDatasetHijo do
               begin
                    Next;
                    if EOF then
                       Self.Agregar;
               end;
          end;
     end;
end;

procedure TEditCedInsp.EscribirCambios;
begin
     if FPuedeModificar then
        inherited EscribirCambios
     else
     begin
          dmLabor.DialogoErrorCedInspeccion
     end;
end;

procedure TEditCedInsp.WO_NUMBERValidKey(Sender: TObject);
begin
     inherited;
     if ( WO_NUMBER.Llave <> '' ) and ( AR_CODIGO.Visible ) then
     begin
          AR_CODIGO.Llave := WO_NUMBER.LookUpDataSet.FieldByName('AR_CODIGO').AsString;
     end;
end;

end.



