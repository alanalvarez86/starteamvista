inherited CedulasOperaciones: TCedulasOperaciones
  Left = 261
  Top = 181
  Caption = 'Producción por %s '
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Height = 193
    inherited PageControl1: TPageControl
      Height = 191
      inherited Generales: TTabSheet
        inherited Panel3: TPanel
          Height = 163
          inherited PanelHora: TPanel
            Height = 69
            inherited LblHora: TLabel
              Left = 99
              Top = 28
            end
            inherited LblCantidad: TLabel
              Left = 80
              Top = 50
            end
            inherited LblFecha: TLabel
              Top = 5
            end
            inherited CE_HORA: TZetaDBHora
              Left = 128
              Top = 24
            end
            inherited CE_PIEZAS: TZetaDBNumero
              Left = 128
              Top = 46
            end
          end
          inherited PanelOrden: TPanel
            Top = 69
          end
          inherited PanelParte: TPanel
            Top = 92
          end
          inherited PanelArea: TPanel
            Top = 115
          end
          inherited PanelOpera: TPanel
            Top = 138
          end
        end
      end
    end
  end
  inherited PageControl: TPageControl
    Top = 244
    Height = 173
    inherited Tabla: TTabSheet
      inherited GridRenglones: TZetaDBGrid
        Height = 116
      end
    end
  end
end
