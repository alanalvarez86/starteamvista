inherited CedulasEspeciales: TCedulasEspeciales
  Left = 289
  Top = 147
  Caption = 'Horas Especiales por Grupo de Empleados'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    inherited OK_DevEx: TcxButton
      TabOrder = 1
    end
    inherited Cancelar_DevEx: TcxButton
      TabOrder = 2
      TabStop = True
    end
    inherited BtnTemp: TButton
      Enabled = False
      TabOrder = 3
    end
    inherited ListaEmpleados_DevEx: TcxButton
      Caption = ' &Lista de empleados'
      TabOrder = 0
    end
  end
  inherited PanelIdentifica: TPanel
    inherited ValorActivo2: TPanel
      inherited textoValorActivo2: TLabel
        ExplicitLeft = 82
      end
    end
  end
  inherited Panel1: TPanel
    Height = 199
    ExplicitHeight = 199
    inherited PageControl1: TcxPageControl
      Height = 197
      ExplicitHeight = 197
      ClientRectBottom = 195
      inherited Generales: TcxTabSheet
        ExplicitHeight = 168
        inherited Panel3: TPanel
          Height = 168
          ExplicitHeight = 168
          inherited PanelHora: TPanel
            Height = 70
            ExplicitHeight = 70
            inherited LblHora: TLabel
              Left = 77
              Top = 51
              Width = 48
              Caption = 'Hora final:'
              ExplicitLeft = 77
              ExplicitTop = 51
              ExplicitWidth = 48
            end
            inherited LblCantidad: TLabel
              Left = 279
              Top = 5
              ExplicitLeft = 279
              ExplicitTop = 5
            end
            object Label1: TLabel [3]
              Left = 70
              Top = 27
              Width = 55
              Height = 13
              Alignment = taRightJustify
              Caption = 'Hora inicial:'
            end
            object LblDuracion: TLabel [4]
              Left = 279
              Top = 27
              Width = 46
              Height = 13
              Alignment = taRightJustify
              Caption = 'Duraci'#243'n:'
            end
            object Label2: TLabel [5]
              Left = 384
              Top = 27
              Width = 36
              Height = 13
              Alignment = taRightJustify
              Caption = 'minutos'
            end
            inherited CE_HORA: TZetaDBHora
              Left = 128
              Top = 47
              TabOrder = 3
              OnExit = CE_HORA_IExit
              ExplicitLeft = 128
              ExplicitTop = 47
            end
            inherited CE_PIEZAS: TZetaDBNumero
              Left = 328
              Top = 1
              TabOrder = 1
              ExplicitLeft = 328
              ExplicitTop = 1
            end
            object CE_HORA_I: TZetaHora
              Left = 128
              Top = 23
              Width = 42
              Height = 21
              EditMask = '99:99;0'
              TabOrder = 2
              Text = ''
              Tope = 48
              OnExit = CE_HORA_IExit
              OnKeyPress = CE_HORA_IKeyPress
            end
            object CE_TIEMPO: TZetaDBNumero
              Left = 328
              Top = 24
              Width = 49
              Height = 21
              TabStop = False
              Color = clBtnFace
              Enabled = False
              Mascara = mnDias
              TabOrder = 4
              Text = '0'
              DataField = 'CE_TIEMPO'
              DataSource = DataSource
            end
          end
          inherited PanelOrden: TPanel
            Top = 70
            ExplicitTop = 70
            inherited LblOrden: TLabel
              Left = 43
              Width = 82
              Caption = 'Orden de trabajo:'
              ExplicitLeft = 43
              ExplicitWidth = 82
            end
          end
          inherited PanelParte: TPanel
            Top = 93
            ExplicitTop = 93
          end
          inherited PanelArea: TPanel
            Top = 116
            ExplicitTop = 116
          end
          inherited PanelOpera: TPanel
            Top = 139
            ExplicitTop = 139
          end
        end
      end
      inherited Moduladores: TcxTabSheet
        ExplicitHeight = 168
      end
    end
  end
  inherited PageControl: TcxPageControl
    Top = 249
    Height = 168
    Properties.ActivePage = Tabla
    ExplicitTop = 249
    ExplicitHeight = 168
    ClientRectBottom = 166
    inherited Tabla: TcxTabSheet
      ExplicitHeight = 139
      inherited GridRenglones: TZetaDBGrid
        Height = 106
        OnEnter = GridRenglonesEnter
        OnExit = GridRenglonesExit
        Columns = <
          item
            ButtonStyle = cbsNone
            Expanded = False
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = '0'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -11
            Title.Font.Name = 'Wingdings'
            Title.Font.Style = []
            Width = 18
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CHECADAS'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #183
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -11
            Title.Font.Name = 'Wingdings'
            Title.Font.Style = []
            Width = 18
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CB_CODIGO'
            Title.Caption = 'N'#250'mero'
            Width = 65
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'PRETTYNAME'
            ReadOnly = True
            Title.Caption = 'Nombre completo'
            Width = 370
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RETARDOS'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'STATUS'
            Visible = False
          end
          item
            ButtonStyle = cbsNone
            Expanded = False
            FieldName = 'CB_TIPO'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = '0'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -11
            Title.Font.Name = 'Wingdings'
            Title.Font.Style = []
            Visible = False
          end>
      end
      inherited Panel2: TPanel
        inherited DepurarLista_DevEx: TcxButton
          Caption = ' &Borrar lista'
          OptionsImage.Glyph.Data = {
            36090000424D3609000000000000360000002800000018000000180000000100
            20000000000000090000000000000000000000000000000000004858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4959CDFF4858CCFF4A5ACDFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4A5ACCFFF8F9FDFF4A59CDFF4C5CCEFFF8F9FDFF4A5ACCFF4858
            CCFF4858CCFF4B5ACDFF4C5CCDFF4A5ACCFF4A5ACCFF4A5ACCFF4858CCFF4A5A
            CCFF4858CCFF4A59CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4959CCFF4858CCFF4A5ACDFEF7F8FCFFF7F8FCFF4959CCFF4858CCFF4858
            CCFFF7F8FCFFF8F9FDFFF8F9FDFFF8F9FDFFF8F9FDFFF8F9FDFFF8F9FDFFF8F9
            FDFFF7F8FCFFF7F8FCFFF8F9FDFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4959CDFF4858CCFF4959CDFFF7F8FCFFF2F4F9FF4858CCFF4858CCFF4858
            CCFFF8F9FDFFF8F9FDFFF8F9FDFFF8F9FDFFF8F9FDFFF8F9FDFFF8F9FDFFF7F8
            FDFFF8F9FDFFF7F8FCFFF7F8FCFF4858CCFF4858CCFF4858CCFF4858CCFF4B5B
            CDFF4858CCFF4959CCFFF8F9FDFF4858CCFF4959CDFFF8F9FDFF4959CCFF4A5A
            CCFF4959CDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4C5CCCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4A5ACDFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4959CCFF4A5ACDFF4959CCFF4959
            CCFF4959CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4959CCFF4858CCFF4958CCFF4958CCFF4858
            CCFF4858CCFF4958CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4959CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4A5ACDFFF8F9FDFF4858CCFF4959CDFFF8F9FDFF4858CCFF4A5A
            CCFF4A5ACCFF4A5ACCFF4858CCFF4A5ACCFF4A5ACCFF4858CCFF4D5DCDFF4A5A
            CCFF4858CCFF4959CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4959
            CCFF4A59CDFF4858CCFF4B5ACDFFF6F7FBFFF6F7FBFF4858CCFF4858CCFF4858
            CCFFF7F8FCFFF7F8FDFFF8F9FDFFF8F9FDFFF8F9FDFFF8F9FDFFF8F9FDFFF8F9
            FDFFF7F8FCFFF7F8FCFFF7F8FDFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4A5ACDFF4858CCFF4858CBFEF6F7FBFFF6F7FBFF4B5BCFFF4858CCFF4858
            CCFFF7F8FCFFF7F8FDFFF7F8FDFFF8F9FDFFF7F8FDFFF6F7FCFFF5F7FCFFF7F8
            FDFFF7F8FCFFF7F8FCFFF7F8FCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4B5BCDFFF8F9FDFF4858CCFF4A5ACDFFF8F9FDFF5260CFFF4C5A
            CCFF4959CCFF4B5ACDFF4B5BCDFF4A5ACDFF4959CCFF4959CCFF4858CCFF4858
            CCFF4959CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4C5BCDFF4858CCFF4B5ACDFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4959CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4959CDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4A5A
            CCFF4858CCFF4858CCFF4858CCFF4959CCFF4858CCFF4858CCFF4959CCFF4858
            CCFF4858CCFF4858CCFF4959CCFF4959CCFF4959CCFF4959CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4A5ACDFFF8F9FDFF4858CCFF4A5ACDFFF8F9FDFF4B5BCDFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4A5ACCFF4858CCFF4B5BCDFF4B5B
            CCFF4858CCFF4959CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4A5ACDFF4858CCFF4C5BCDFFF3F5FBFFF7F8FCFF4F5ECEFF4858CBFE4858
            CBFEF8F9FDFFF8F9FDFFF8F9FDFFF8F9FDFFF8F9FDFFF8F9FDFFF8F9FDFFF8F9
            FDFFF8F9FDFFF8F9FDFFF6F7FCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4959CDFF4858CCFF4858CCFFF5F6FCFFF6F7FCFF4959CCFF4858CCFF4858
            CCFFF6F7FDFFF7F8FCFFF7F8FCFFF7F8FDFFF8F9FDFFF8F9FDFFF7F8FDFFF8F9
            FDFFF7F8FDFFF7F8FDFFF7F8FDFF4858CCFF4858CCFF4858CCFF4858CCFF4B5B
            CDFF4858CCFF4A5ACCFFF8F9FDFF4858CCFF4C5CCEFFF8F9FDFF4858CCFF4959
            CCFF4B5BCDFF5160CDFF4D5DCCFF4858CCFF4858CCFF4858CCFF4858CCFF4A5A
            CCFF4B5BCDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4C5BCDFF4858CCFF4E5DCEFF4858CCFF4858CCFF4E5DCEFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF}
        end
      end
    end
    inherited Datos: TcxTabSheet
      ExplicitHeight = 139
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_BuscarBtn: TdxBarButton
      Visible = ivAlways
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  inherited ImageEmp_DevEx: TcxImageList
    FormatVersion = 1
  end
end
