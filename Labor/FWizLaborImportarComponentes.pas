unit FWizLaborImportarComponentes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseWizardImportar, ComCtrls, StdCtrls, ZetaKeyCombo, Buttons,
  ZetaWizard, ExtCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, TressMorado2013, cxContainer, cxEdit,
  ZetaCXWizard, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl, Vcl.Menus, cxButtons;

type
  TWizLaborImportarComponentes = class(TTBaseWizardImportar)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
  public
    { Public declarations }
  end;

var
  WizLaborImportarComponentes: TWizLaborImportarComponentes;

implementation

uses DLabor,
     ZGlobalTress,
     ZetaCommonClasses;

{$R *.DFM}

procedure TWizLaborImportarComponentes.FormCreate(Sender: TObject);
begin
     Programa.Tag := K_GLOBAL_DEF_LAB_IMP_COM_PROGRAMA;
     Comandos.Tag := K_GLOBAL_DEF_LAB_IMP_COM_PARAMETROS;
     Archivo.Tag := K_GLOBAL_DEF_LAB_IMP_COM_ARCHIVO;
     NombreCatalogo := 'componentes';
     HelpContext := 9523;
     inherited;
end;

function TWizLaborImportarComponentes.EjecutarWizard: Boolean;
begin
     Result := dmLabor.ImportarComponentes( ParameterList );
end;

end.
