inherited WizLaborDepurar: TWizLaborDepurar
  Left = 294
  Top = 177
  Caption = 'Depuraci'#243'n'
  ClientHeight = 402
  ClientWidth = 509
  ExplicitWidth = 515
  ExplicitHeight = 436
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 509
    Height = 402
    Header.AssignedValues = [wchvGlyph]
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
      6000003A980000176F925FC546000000097048597300000EC400000EC401952B
      0E1B00000380494441545847C5563D681451109E9C178918A3428A20410CA408
      29C414628A14012D4452886827A92D52085A58A8EBC5C2BBDB3BD324959D9588
      723F315D20458A4052898888A08298BBD32285450AC5F3FBE666377BC7DEE57E
      92F8C1B0BBF366DFF7BD99796F57FE07E69DF9DEB4931EB6C78345D2490EB831
      77CD8D257FE1FE9CB90F0673CE933320FE908AB9E58A25DFD9D0FE23E5A44640
      5AD821777FBB8E3B63C3FB0B904F60E55B35E4D775B05C2EB764AD22E524A658
      6F8FDC6A7FC9863B13E0384E1493DD749DC4FDB4131F37B70F8E71B501F29FCC
      860D571046D2C83C541ACA5DF326579B75E76C98E4B7ABC6587FF4810DEF208C
      A4911173CEDC719DB09A408DC4E959D70DFAD8F971273EA82F7782CDAC1CDDCC
      4427798FDA5EC6E47E7A1BD806440DE804EDE2CB8A4869512E1673B25DCA4BB9
      988D681D31F11510D4158195AFF2D4D349DA0189413A067BABC47959F6EE0B39
      19650C445C03D99F5A7218B79A66AB6584107FC5F57C19FEEF79394C3F6C1B25
      39C57834DF0C08FFD608A085375E3D6C65A48B4455C43999A4DF4214EC058C15
      31B685B863F4A563EE831001B40D7DA9110A8BD2F3232F376CA5758983405C1F
      055008DE3F8C33A10B67FB428880C6A7179B8BA44A5C49ED5823E22090894196
      42339693281AAE1B8DB71C24E716B4F07080785DC9B332CADA7B28BD942EACEC
      843DD6059A7158179093A5D21B8940440F48DFFB229CE43D0B0D075EBE6AAB1F
      329714B2721ABE554CDA6FAE8660D654445E9E4140043BA31F223EA1242B3CAE
      2D2C1C546DB55C461626707D6182C62CA429207ECA44C4411A79FA2831D2F4E9
      8797340B9EB54AEE010B99E6FB9F5F9DBC6BAEE6C02C783B00353D6B6E1FA58C
      1CC1D82D7BAC0B085FE01C9BD9EE0BE66A1EEC685BFD020F1C8257F4C3384BB4
      5B3FF8CD989784B9EA7ECCEAC24B21EBC909D9177CE6BD8584E2DB6B39A419CC
      C947DE9BBB0D01D87A207C6E2B5163066CD8078EE388972502710F19CB2C9A4B
      D1B200C276C5920AC849264844203B439A19C4F1D9B66C1927E91D0D08A02D01
      44CD07475745212C8D66C50E28FB76AC33F5CC0A7D41B42D80088A00C1248812
      9695690BF1B72FB360AE2A7424805011D68834369AB7526E4D159717FF3FB016
      1D0B20D898ACAF89E077A34FFD20560110A28121D813011E78402103450AC1F5
      B10A0A94230C7B2A80286424CA959B08FE9074DB5028F65C8007A4BE97FD618F
      75B1BB00917F55B010126517B57A0000000049454E44AE426082}
    ExplicitWidth = 509
    ExplicitHeight = 402
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Este proceso elimina informaci'#243'n de labor a una fecha de referen' +
        'cia de acuerdo al tipo de informaci'#243'n que se especifque.'
      Header.Title = 'Depuraci'#243'n de informaci'#243'n'
      ExplicitWidth = 487
      ExplicitHeight = 260
      object BorrarDatosGB: TGroupBox
        Left = 100
        Top = 3
        Width = 297
        Height = 124
        Caption = ' Borrar datos '
        TabOrder = 0
        object FechaLBL: TLabel
          Left = 103
          Top = 26
          Width = 59
          Height = 13
          Caption = '&Anteriores a:'
          FocusControl = Fecha
        end
        object Label4: TLabel
          Left = 77
          Top = 101
          Width = 85
          Height = 13
          Caption = 'C'#233'&dulas de scrap:'
          FocusControl = Fecha
        end
        object Label3: TLabel
          Left = 52
          Top = 83
          Width = 110
          Height = 13
          Caption = 'C'#233'dulas de &inspecci'#243'n:'
          FocusControl = Fecha
        end
        object Label2: TLabel
          Left = 50
          Top = 66
          Width = 112
          Height = 13
          Caption = 'C'#233'dulas de &producci'#243'n:'
          FocusControl = Fecha
        end
        object Label1: TLabel
          Left = 50
          Top = 48
          Width = 112
          Height = 13
          Caption = '&Kardex de operaciones:'
          FocusControl = Fecha
        end
        object CBKardex: TCheckBox
          Left = 52
          Top = 47
          Width = 131
          Height = 17
          Alignment = taLeftJustify
          Checked = True
          State = cbChecked
          TabOrder = 1
        end
        object Fecha: TZetaFecha
          Left = 169
          Top = 22
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 0
          Text = '30/Jun/00'
          Valor = 36707.000000000000000000
        end
        object CBCedulas: TCheckBox
          Left = 52
          Top = 65
          Width = 131
          Height = 17
          Alignment = taLeftJustify
          Checked = True
          State = cbChecked
          TabOrder = 2
        end
        object CBCedInspeccion: TCheckBox
          Left = 52
          Top = 83
          Width = 131
          Height = 17
          Alignment = taLeftJustify
          Checked = True
          State = cbChecked
          TabOrder = 3
        end
        object cbCedScrap: TCheckBox
          Left = 77
          Top = 100
          Width = 106
          Height = 17
          Alignment = taLeftJustify
          Checked = True
          State = cbChecked
          TabOrder = 4
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      ExplicitWidth = 487
      ExplicitHeight = 260
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 487
        ExplicitHeight = 165
        Height = 165
        Width = 487
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 487
        Width = 487
        inherited Advertencia: TcxLabel
          Caption = 
            'Al aplicar este proceso se eliminar'#225'n los registros de acuerdo a' +
            ' los par'#225'metros indicados.'
          Style.IsFontAssigned = True
          ExplicitLeft = 70
          ExplicitWidth = 415
          Width = 415
          AnchorY = 49
        end
      end
    end
  end
end
