inherited EditCatValFactores_DevEx: TEditCatValFactores_DevEx
  Left = 471
  Top = 385
  Caption = 'Factor de Valuaci'#243'n'
  ClientHeight = 467
  ClientWidth = 443
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 24
    Top = 60
    Width = 39
    Height = 13
    Caption = 'Plantilla:'
  end
  object Label2: TLabel [1]
    Left = 31
    Top = 84
    Width = 32
    Height = 13
    Caption = 'Orden:'
  end
  object Label3: TLabel [2]
    Left = 27
    Top = 108
    Width = 36
    Height = 13
    Caption = 'C'#243'digo:'
  end
  object Label4: TLabel [3]
    Left = 23
    Top = 132
    Width = 40
    Height = 13
    Caption = 'Nombre:'
  end
  object Label5: TLabel [4]
    Left = 4
    Top = 156
    Width = 59
    Height = 13
    Caption = 'Descripci'#243'n:'
  end
  object Label6: TLabel [5]
    Left = 32
    Top = 252
    Width = 31
    Height = 13
    Caption = 'Ingl'#233's:'
  end
  object Label7: TLabel [6]
    Left = 4
    Top = 276
    Width = 59
    Height = 13
    Caption = 'Descripci'#243'n:'
  end
  object Label8: TLabel [7]
    Left = 23
    Top = 372
    Width = 40
    Height = 13
    Caption = 'N'#250'mero:'
  end
  object Label9: TLabel [8]
    Left = 33
    Top = 396
    Width = 30
    Height = 13
    Caption = 'Texto:'
  end
  inherited PanelBotones: TPanel
    Top = 431
    Width = 443
    TabOrder = 12
    inherited OK_DevEx: TcxButton
      Left = 279
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 358
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 443
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 117
      inherited textoValorActivo2: TLabel
        Width = 111
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 16
  end
  object VL_CODIGO: TZetaDBNumero [12]
    Left = 72
    Top = 56
    Width = 65
    Height = 21
    Color = clBtnFace
    Enabled = False
    Mascara = mnEmpleado
    TabOrder = 1
    DataField = 'VL_CODIGO'
    DataSource = DataSource
  end
  object VF_ORDEN: TZetaDBNumero [13]
    Left = 72
    Top = 80
    Width = 65
    Height = 21
    Color = clBtnFace
    Enabled = False
    Mascara = mnEmpleado
    TabOrder = 2
    DataField = 'VF_ORDEN'
    DataSource = DataSource
  end
  object VF_CODIGO: TDBEdit [14]
    Left = 72
    Top = 104
    Width = 105
    Height = 21
    CharCase = ecUpperCase
    DataField = 'VF_CODIGO'
    DataSource = DataSource
    TabOrder = 3
  end
  object VF_NOMBRE: TDBEdit [15]
    Left = 72
    Top = 128
    Width = 329
    Height = 21
    DataField = 'VF_NOMBRE'
    DataSource = DataSource
    TabOrder = 4
  end
  object VF_DESCRIP: TcxDBMemo [16]
    Left = 72
    Top = 152
    DataBinding.DataField = 'VF_DESCRIP'
    DataBinding.DataSource = DataSource
    Properties.ScrollBars = ssVertical
    TabOrder = 5
    Height = 89
    Width = 329
  end
  object VF_INGLES: TDBEdit [17]
    Left = 72
    Top = 248
    Width = 329
    Height = 21
    DataField = 'VF_INGLES'
    DataSource = DataSource
    TabOrder = 6
  end
  object VF_DES_ING: TcxDBMemo [18]
    Left = 72
    Top = 272
    DataBinding.DataField = 'VF_DES_ING'
    DataBinding.DataSource = DataSource
    Properties.ScrollBars = ssVertical
    TabOrder = 7
    Height = 89
    Width = 329
  end
  object VF_TEXTO: TDBEdit [19]
    Left = 72
    Top = 392
    Width = 329
    Height = 21
    DataField = 'VF_TEXTO'
    DataSource = DataSource
    TabOrder = 9
  end
  object VF_NUMERO: TZetaDBNumero [20]
    Left = 72
    Top = 368
    Width = 121
    Height = 21
    Mascara = mnNumeroGlobal
    TabOrder = 8
    Text = '0.00'
    DataField = 'VF_NUMERO'
    DataSource = DataSource
  end
  object VL_NOMBRE: TEdit [21]
    Left = 144
    Top = 56
    Width = 257
    Height = 21
    Color = clBtnFace
    Enabled = False
    TabOrder = 10
    Text = 'VL_NOMBRE'
  end
  inherited DataSource: TDataSource
    Left = 388
    Top = 1
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_BuscarBtn: TdxBarButton
      Visible = ivAlways
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
