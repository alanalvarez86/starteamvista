unit FEditCatSegurosGastosMedicos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZetaNumero, ZetaDBTextBox, StdCtrls,
  Mask, ZetaFecha, DBCtrls, Db,
  {$ifdef CAJAAHORRO}
  dCajaAhorro,
  {$else}
  dRecursos,
  {$endif}
  Grids, DBGrids, ZetaDBGrid, ComCtrls, ExtCtrls, Buttons, ZetaKeyCombo,
  ZetaMessages, ZBaseEdicion_DevEx, ZetaEdit, 
  ZBaseEdicionRenglon_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxPC, cxNavigator, cxDBNavigator, cxButtons, cxContainer,
  cxEdit, cxGroupBox, cxTextEdit, cxMemo, cxDBEdit;

type                                            
  TEditSeguroGastosMedicos_DevEx = class(TBaseEdicionRenglon_DevEx)
    Label2: TLabel;
    PM_OBSERVA: TcxDBMemo;
    DedLbl: TLabel;
    Label8: TLabel;
    PM_CODIGO: TDBEdit;
    GroupBox2: TcxGroupBox;
    PM_NOM_CT1: TDBEdit;
    PM_COR_CT1: TDBEdit;
    Label19: TLabel;
    Label10: TLabel;
    Label12: TLabel;
    PM_TEL1CT1: TDBEdit;
    Label13: TLabel;
    PM_TEL2CT1: TDBEdit;
    btnBuscarEmp: TSpeedButton;
    Label1: TLabel;
    PM_NUMERO: TDBEdit;
    GroupBox1: TcxGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    PM_NOM_CT2: TDBEdit;
    PM_COR_CT2: TDBEdit;
    PM_TEL1CT2: TDBEdit;
    PM_TEL2CT2: TDBEdit;
    PM_DESCRIP: TDBEdit;
    Label7: TLabel;
    PM_ASEGURA: TDBEdit;
    Label9: TLabel;
    Label11: TLabel;
    PM_BROKER: TDBEdit;
    PM_TEL_BRK: TDBEdit;
    Label14: TLabel;
    Label15: TLabel;
    PM_TIPO: TZetaDBKeyCombo;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure BBModificarClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure BBBorrarClick(Sender: TObject);
    procedure GridRenglonesDblClick(Sender: TObject);
  private
    procedure AplicaFiltro;
    procedure HabilitaControls(const lHabilita: Boolean);
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  EditSeguroGastosMedicos_DevEx: TEditSeguroGastosMedicos_DevEx;

implementation

uses ZetaCommonClasses,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaDialogo,
     ZetaTipoEntidad,
     ZAccesosTress,
     dSistema,
     ZAccesosMgr,
     dCatalogos,
     FEditCatVigenciasSGM_DevEx;
{$R *.DFM}


procedure TEditSeguroGastosMedicos_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsSegGastosMed.Conectar;
          Datasource.Dataset := cdsSegGastosMed;
          dsRenglon.DataSet := cdsVigenciasSGM;
     end;
end;

procedure TEditSeguroGastosMedicos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= ZetaCommonClasses.H_Cat_SGM_EDIT;
     IndexDerechos := ZAccesosTress.D_CAT_GRAL_SEG_GASTOS_MEDICOS;

     FirstControl := PM_CODIGO;
     PM_TIPO.ListaFija := lfTipoSGM;
end;


procedure TEditSeguroGastosMedicos_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage := Tabla;
end;

procedure TEditSeguroGastosMedicos_DevEx.BBAgregarClick(Sender: TObject);
begin
     //inherited;
     IF ( ZAccesosMgr.CheckDerecho( D_CAT_GRAL_SEG_GASTOS_MEDICOS , K_DERECHO_CAMBIO ) ) and ( dsRenglon.DataSet.State = dsBrowse ) then
     begin
          dsRenglon.DataSet.Append;
          ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatVigenciasSGM_DevEx, TEditCatVigenciasSGM_DevEx );
     end
     else
         ZInformation('Seguro de Gastos M�dicos','No Tiene Permiso Para Modificar Registros',0);
end;

procedure TEditSeguroGastosMedicos_DevEx.BBModificarClick(Sender: TObject);
begin
     //inherited;
     IF ( ZAccesosMgr.CheckDerecho( D_CAT_GRAL_SEG_GASTOS_MEDICOS , K_DERECHO_CAMBIO ) ) and ( dsRenglon.DataSet.State = dsBrowse ) THEN
     begin
          dsRenglon.DataSet.Edit;
          ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatVigenciasSGM_DevEx, TEditCatVigenciasSGM_DevEx );
     end
     else
         ZInformation('Seguro de Gastos M�dicos','No Tiene Permiso Para Modificar Registros',0);
end;

procedure TEditSeguroGastosMedicos_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
     Procedure HabilitaVigencias( bHabilita : Boolean );
     begin
          BBAgregar_DevEx.Enabled := bHabilita;
          BBBorrar_DevEx.Enabled := bHabilita;
          BBModificar_DevEx.Enabled := bHabilita;
     end;
begin
     inherited;
     if Field = nil then
     begin
          AplicaFiltro;
          with dmCatalogos.cdsSegGastosMed do
          begin
               if State = dsInsert then
               begin
                    HabilitaControls( ZAccesosMgr.CheckDerecho( D_CAT_GRAL_SEG_GASTOS_MEDICOS , K_DERECHO_ALTA )  );
                    HabilitaVigencias( False );
               end;
               if State = dsBrowse then
               begin
                    HabilitaControls( ZAccesosMgr.CheckDerecho( D_CAT_GRAL_SEG_GASTOS_MEDICOS , K_DERECHO_CAMBIO )  );
                    HabilitaVigencias( True );
               end;
          end;
     end;
end;

procedure TEditSeguroGastosMedicos_DevEx.AplicaFiltro;
begin
     with dmCatalogos.cdsVigenciasSGM do
     begin
          Filtered := False;
          Filter := Format('PM_CODIGO = ''%s''',[dmCatalogos.cdsSegGastosMed.FieldByName('PM_CODIGO').AsString]);
          Filtered := True;
     end;
end;


procedure TEditSeguroGastosMedicos_DevEx.HabilitaControls(const lHabilita:Boolean);
begin
     PM_CODIGO.Enabled := lHabilita;
     PM_NUMERO.Enabled := lHabilita;
     PM_DESCRIP.Enabled := lHabilita;
     PM_ASEGURA.Enabled := lHabilita;
     PM_BROKER.Enabled := lHabilita;
     PM_TEL_BRK.Enabled := lHabilita;
     PM_NOM_CT1.Enabled := lHabilita;
     PM_COR_CT1.Enabled := lHabilita;
     PM_TEL1CT1.Enabled := lHabilita;
     PM_TEL2CT1.Enabled := lHabilita;
     PM_NOM_CT2.Enabled := lHabilita;
     PM_COR_CT2.Enabled := lHabilita;
     PM_TEL1CT2.Enabled := lHabilita;
     PM_TEL2CT2.Enabled := lHabilita;
     PM_OBSERVA.Enabled := lHabilita;
     PM_TIPO.Enabled := lHabilita;

end;

procedure TEditSeguroGastosMedicos_DevEx.BBBorrarClick(Sender: TObject);
begin
     IF ( ZAccesosMgr.CheckDerecho( D_CAT_GRAL_SEG_GASTOS_MEDICOS , K_DERECHO_CAMBIO ) ) then
     begin
          inherited
     end
     else
         ZInformation('Seguro de Gastos M�dicos','No Tiene Permiso Para Modificar Registros',0);

end;

procedure TEditSeguroGastosMedicos_DevEx.GridRenglonesDblClick(Sender: TObject);
begin
     //inherited;
     if dmCatalogos.cdsVigenciasSGM.RecordCount > 0 then
        ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatVigenciasSGM_DevEx, TEditCatVigenciasSGM_DevEx );
end;

end.
