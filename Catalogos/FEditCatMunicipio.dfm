inherited TOEditEntidad: TTOEditEntidad
  Left = 319
  Top = 226
  Caption = 'Municipio'
  ClientHeight = 249
  ClientWidth = 381
  PixelsPerInch = 96
  TextHeight = 13
  object DBCodigoLBL: TLabel [0]
    Left = 41
    Top = 40
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo:'
  end
  object DBDescripcionLBL: TLabel [1]
    Left = 18
    Top = 62
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descripci'#243'n:'
  end
  object DBInglesLBL: TLabel [2]
    Left = 46
    Top = 84
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ingl'#233's:'
  end
  object Label2: TLabel [3]
    Left = 37
    Top = 106
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero:'
  end
  object Label1: TLabel [4]
    Left = 47
    Top = 128
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Texto:'
  end
  object lblClave: TLabel [5]
    Left = 16
    Top = 172
    Width = 61
    Height = 13
    Alignment = taRightJustify
    Caption = 'Clave STPS:'
  end
  object Label13: TLabel [6]
    Left = 41
    Top = 149
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Estado:'
  end
  inherited PanelBotones: TPanel
    Top = 213
    Width = 381
    TabOrder = 8
    inherited OK: TBitBtn
      Left = 213
    end
    inherited Cancelar: TBitBtn
      Left = 298
    end
  end
  inherited PanelSuperior: TPanel
    Width = 381
    TabOrder = 0
  end
  inherited PanelIdentifica: TPanel
    Width = 381
    TabOrder = 1
    inherited ValorActivo2: TPanel
      Width = 55
    end
  end
  object TB_CODIGO: TZetaDBEdit [10]
    Left = 81
    Top = 36
    Width = 65
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 2
    ConfirmEdit = True
    DataField = 'TB_CODIGO'
    DataSource = DataSource
  end
  object TB_ELEMENT: TDBEdit [11]
    Left = 81
    Top = 58
    Width = 280
    Height = 21
    DataField = 'TB_ELEMENT'
    DataSource = DataSource
    TabOrder = 3
  end
  object TB_INGLES: TDBEdit [12]
    Left = 81
    Top = 80
    Width = 280
    Height = 21
    DataField = 'TB_INGLES'
    DataSource = DataSource
    TabOrder = 4
  end
  object TB_NUMERO: TZetaDBNumero [13]
    Left = 81
    Top = 102
    Width = 100
    Height = 21
    Mascara = mnNumeroGlobal
    TabOrder = 5
    Text = '0.00'
    DataField = 'TB_NUMERO'
    DataSource = DataSource
  end
  object TB_TEXTO: TDBEdit [14]
    Left = 81
    Top = 124
    Width = 280
    Height = 21
    DataField = 'TB_TEXTO'
    DataSource = DataSource
    TabOrder = 6
  end
  object TB_CURP: TDBEdit [15]
    Left = 81
    Top = 168
    Width = 65
    Height = 21
    CharCase = ecUpperCase
    DataField = 'TB_STPS'
    DataSource = DataSource
    TabOrder = 7
  end
  object TB_ENTIDAD: TZetaDBKeyLookup [16]
    Left = 81
    Top = 146
    Width = 300
    Height = 21
    LookupDataset = dmTablas.cdsEstado
    TabOrder = 9
    TabStop = True
    WidthLlave = 60
    DataField = 'TB_ENTIDAD'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 268
    Top = 1
  end
end
