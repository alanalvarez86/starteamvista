unit FCatTools_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Grids, DBGrids, Db, ExtCtrls,
     ZBaseConsulta,
     ZetaDBGrid, StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
     cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
     
     TressMorado2013,
     cxCustomData, cxFilter, cxData,
     cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
     cxGridTableView, cxGridDBTableView, Menus, ActnList, ImgList,
     cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, dxSkinsCore,
     dxSkinsDefaultPainters, dxSkinscxPCPainter;

type
  TCatTools_DevEx = class(TBaseGridLectura_DevEx)
    TO_CODIGO: TcxGridDBColumn;
    TO_DESCRIP: TcxGridDBColumn;
    TO_INGLES: TcxGridDBColumn;
    TO_NUMERO: TcxGridDBColumn;
    TO_TEXTO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar;override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  CatTools_DevEx: TCatTools_DevEx;

implementation

uses DCatalogos,
     ZetaBuscador_DevEx,
     ZetaCommonClasses;

{$R *.DFM}

{ TCatCursos }

procedure TCatTools_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H60663_Herramientas;
end;

procedure TCatTools_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsTools.Conectar;
          DataSource.DataSet:= cdsTools;
     end;
end;

procedure TCatTools_DevEx.Refresh;
begin
     dmCatalogos.cdsTools.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatTools_DevEx.Agregar;
begin
     dmCatalogos.cdsTools.Agregar;
end;

procedure TCatTools_DevEx.Borrar;
begin
     dmCatalogos.cdsTools.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatTools_DevEx.Modificar;
begin
     dmCatalogos.cdsTools.Modificar;
end;

procedure TCatTools_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Herramienta', 'Herramienta', 'TO_CODIGO', dmCatalogos.cdsTools );
end;

procedure TCatTools_DevEx.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
  inherited;

  //columna sumatoria: cuantos
  CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TO_CODIGO'), K_SIN_TIPO , '', skCount);
  ZetaDBGridDBTableView.ApplyBestFit();
end;

end.
