unit FEditFestTurno_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, StdCtrls, Mask,  Db, Buttons, DBCtrls,ExtCtrls,
  ZetaKeyCombo, ZetaFecha, ZetaNumero, ZetaDBTextBox,
  cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, cxDBEdit, cxContainer, cxEdit, cxGroupBox, cxRadioGroup;

type
  TEditFestTurno_DevEx = class(TBaseEdicion_DevEx)
    Label3: TLabel;
    Label5: TLabel;
    TurnoLbl: TLabel;
    FE_DESCRIP: TDBEdit;
    zTurno: TZetaTextBox;
    Label4: TLabel;
    RGVigencia: TRadioGroup;
    GBTodos: TcxGroupBox;
    FE_MES: TZetaDBKeyCombo;
    FE_DIA: TZetaDBNumero;
    FE_DIALbl: TLabel;
    FE_MESLbl: TLabel;
    GBAplicaPara: TcxGroupBox;
    FE_CAMBIOLbl: TLabel;
    FE_CAMBIO: TZetaDBFecha;
    FechaLbl: TLabel;
    zFecha: TZetaFecha;
    FE_TIPO: TDBRadioGroup;
    procedure FE_TIPOChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure RGVigenciaClick(Sender: TObject);
    procedure zFechaChange(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
    FConectandoDataSet: Boolean;
    procedure SetTipoTurno(const lEnabled: Boolean);
    procedure SetValorVigencia;
    procedure SetValorzFecha;    
    procedure SetValorFE_YEAR;
    procedure HabilitaControlesFest( const lHabilita: Boolean);
    function AplicaparaInvalido: Boolean;
  protected
    procedure Connect; override;
  public
  end;

const
     K_FESTIVO = 0;
     K_INTERCAMBIO = 1;
     K_TODOS = 0;
     K_UN_YEAR = 1;

var
  EditFestTurno_DevEx: TEditFestTurno_DevEx;

implementation

uses dCatalogos, dCliente, ZetaCommonClasses, ZAccesosTress, ZetaCommonTools, ZetaDialogo;

{$R *.DFM}

procedure TEditFestTurno_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := FE_MES;
end;

procedure TEditFestTurno_DevEx.FormShow(Sender: TObject);
begin
     FConectandoDataSet := TRUE;
     inherited;
     SetTipoTurno( dmCatalogos.TurnoFestivo <> TURNO_VACIO );
     SetValorVigencia;
     FConectandoDataSet := FALSE;
end;

procedure TEditFestTurno_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsTurnos.Conectar;
          DataSource.DataSet:= cdsFestTurno;
          if ( TurnoFestivo = TURNO_VACIO ) then
          begin
               IndexDerechos:= D_CAT_GRALES_FEST_GRAL;
               HelpContext:= H3900000_Edit_Festivos_generales;
          end
          else
          begin
               IndexDerechos:= D_CAT_GRALES_FEST_TURNO;
               HelpContext:= H60633_Edit_Festivos_Turno;
          end;
     end;
end;

procedure TEditFestTurno_DevEx.SetTipoTurno( const lEnabled: Boolean );
const
     K_CAPTION_PORTURNO = 'Dias Festivos por Turno';
     K_CAPTION_GENERALES = 'Dias Festivos Generales';
begin
     TurnoLbl.Visible:= lEnabled;
     zTurno.Visible := lEnabled;
     if lEnabled then
     begin
          IndexDerechos:= D_CAT_GRALES_FEST_TURNO;
          HelpContext:= H60633_Edit_Festivos_Turno;
          with dmCatalogos do
               zTurno.Caption := cdsFestTurno.FieldByName( 'TU_CODIGO' ).AsString + ' = ' + cdsTurnos.GetDescripcion( cdsFestTurno.FieldByName( 'TU_CODIGO' ).AsString );
          Self.Caption := K_CAPTION_PORTURNO;
     end
     else
     begin
          IndexDerechos:= D_CAT_GRALES_FEST_GRAL;
          HelpContext:= H3900000_Edit_Festivos_generales;
          Self.Caption := K_CAPTION_GENERALES;
     end;
end;

function TEditFestTurno_DevEx.AplicaparaInvalido: Boolean;
begin
     with dmCatalogos.cdsFestTurno do
        Result := ( RGVigencia.ItemIndex = K_TODOS ) and ( FieldByName( 'FE_YEAR' ).AsInteger <> K_TODOS ) OR
             ( RGVigencia.ItemIndex = K_UN_YEAR ) and ( FieldByName( 'FE_YEAR' ).AsInteger = 0 )
end;

procedure TEditFestTurno_DevEx.FE_TIPOChange(Sender: TObject);
begin
     inherited;
     if FE_TIPO.ItemIndex = K_INTERCAMBIO then
        RGVigencia.ItemIndex := K_UN_YEAR;
     HabilitaControlesFest( ( FE_TIPO.ItemIndex = K_FESTIVO ) and ( RGVigencia.ItemIndex = K_TODOS ) );

     // Nota Carlos: La siguiente validaci�n se hizo aqu� porque el RadioButton no activa el
     // OnChange de FE_TIPO sino hasta que le das Tab o OK (!)
     if DataSource.Dataset <> nil then
     begin
         with DataSource.Dataset do
         begin
              if (state in [ dsInsert, dsEdit ]) and (FE_TIPO.ItemIndex = K_INTERCAMBIO ) then
                   FieldByName( 'FE_CAMBIO' ).AsDateTime := dmCliente.FechaDefault;
         end;
     end;
end;

procedure TEditFestTurno_DevEx.RGVigenciaClick(Sender: TObject);
begin
     inherited;
     HabilitaControlesFest( RGVigencia.ItemIndex = K_TODOS );

     with dmCatalogos.cdsFestTurno do
     begin
          if AplicaparaInvalido then
          begin
               if not ( state in [ dsInsert, dsEdit ]) then
                  Edit;
               SetValorzFecha;
               SetValorFE_YEAR;
          end;
     end;
end;

procedure TEditFestTurno_DevEx.zFechaChange(Sender: TObject);
begin
     if NOT FConectandoDataSet then
     begin
          with dmCatalogos.cdsFestTurno do
          begin
               if NOT ( state in [ dsInsert, dsEdit ])  then
                  Edit;
               SetValorFE_YEAR;
          end;
     end;
     inherited;
end;

procedure TEditFestTurno_DevEx.HabilitaControlesFest(const lHabilita: Boolean);
begin
     FE_MES.Enabled := lHabilita;
     FE_MESLbl.Enabled := FE_MES.Enabled;
     FE_DIA.Enabled := lHabilita;
     FE_DIALbl.Enabled := FE_DIA.Enabled;
     FE_CAMBIO.Enabled := FE_TIPO.ItemIndex = K_INTERCAMBIO;
     FE_CAMBIOLbl.Enabled := FE_CAMBIO.Enabled;
     zFecha.Enabled := NOT( ( FE_TIPO.ItemIndex = K_FESTIVO ) and ( RGVigencia.ItemIndex = K_TODOS ) );
     FechaLbl.Enabled := zFecha.Enabled;
     RGVigencia.Enabled :=  NOT ( FE_TIPO.ItemIndex = K_INTERCAMBIO );
end;

procedure TEditFestTurno_DevEx.SetValorVigencia;
begin
     with dmCatalogos.cdsFestTurno do
     begin
          RGVigencia.ItemIndex:= BoolToInt( FieldByName('FE_YEAR').AsInteger <> 0 );
     end;
end;

procedure TEditFestTurno_DevEx.SetValorzFecha;
begin
     with dmCatalogos.cdsFestTurno do
     begin
          if RGVigencia.ItemIndex = K_UN_YEAR then
             zFecha.Valor := EncodeDate( dmCliente.YearDefault, FieldByName('FE_MES').AsInteger, FieldByName('FE_DIA').AsInteger );
     end;
end;


procedure TEditFestTurno_DevEx.SetValorFE_YEAR;
var
   wYear, wMes, wDia : Word;
begin
     with dmCatalogos.cdsFestTurno do
     begin
          if RGVigencia.ItemIndex = K_UN_YEAR then
          begin
               decodeDate( zFecha.Valor, wYear, wMes, wDia );
               FieldByName('FE_YEAR').AsInteger := wYear;
               FieldByName('FE_MES').AsInteger := wMes;
               FieldByName('FE_DIA').AsInteger := wDia;
          end
          else
              FieldByName('FE_YEAR').AsInteger := 0;
     end;
end;

procedure TEditFestTurno_DevEx.OK_DevExClick(Sender: TObject);
var
   iYear : Integer;
begin
     iYear := dmCatalogos.cdsFestTurno.FieldByName('FE_YEAR').AsInteger;
     inherited;
     if iYear <> dmCatalogos.cdsFestTurno.FieldByName('FE_YEAR').AsInteger then
        Close;
end;

procedure TEditFestTurno_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( ( Field = nil ) and ( not( dmCatalogos.cdsFestTurno.State in [dsEdit,dsInsert]) ) )then
     begin
          zFecha.Onchange := NIL;
          try
             SetValorVigencia;
             SetValorzFecha;
          finally
                 zFecha.Onchange := zFechaChange;
          end;
     end;

end;


end.
