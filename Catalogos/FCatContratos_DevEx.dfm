inherited CatContratos_DevEx: TCatContratos_DevEx
  Left = 254
  Top = 147
  Caption = 'Tipo de Contrato'
  ClientHeight = 254
  ClientWidth = 720
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 720
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 394
      inherited textoValorActivo2: TLabel
        Width = 388
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 720
    Height = 235
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object TB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'TB_CODIGO'
      end
      object TB_ELEMENT: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'TB_ELEMENT'
      end
      object TB_DIAS: TcxGridDBColumn
        Caption = 'Duraci'#243'n'
        DataBinding.FieldName = 'TB_DIAS'
        Width = 48
      end
      object TB_ACTIVO: TcxGridDBColumn
        Caption = 'Activo'
        DataBinding.FieldName = 'TB_ACTIVO'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
