unit FDiccion_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls,
  Grids, DBGrids, ZetaDBGrid, StdCtrls, ZetaKeyCombo,ZBaseGridLectura_DevEx,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxClasses, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList, cxGridLevel,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TDiccion_DevEx = class(TBaseGridLectura_DevEx)
    DI_NOMBRE: TcxGridDBColumn;
    DI_TITULO: TcxGridDBColumn;
    DI_ANCHO: TcxGridDBColumn;
    DI_MASCARA: TcxGridDBColumn;
    DI_CONFI: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    Panel1: TPanel;
    Label1: TLabel;
    CBEntidades: TComboBox;
    procedure FormShow(Sender: TObject);
    procedure CBEntidadesChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
  protected
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
    procedure Connect; override;
    procedure Refresh; override;

  public
    function PuedeAgregar( var sMensaje: String ): Boolean;override;
    function PuedeBorrar( var sMensaje: String ): Boolean;override;
  end;

var
  Diccion_DevEx: TDiccion_DevEx;

implementation
uses DBaseDiccionario,
     DDiccionario,
     ZetaCommonClasses;

{$R *.DFM}
{ TSistDiccion }

procedure TDiccion_DevEx.Connect;
begin
     with dmDiccionario do
     begin
          ConectaDiccion;
          DataSource.DataSet:= cdsDiccion;
     end;
end;

procedure TDiccion_DevEx.Refresh;
begin
     {with CbEntidades do
          dmDiccionario.Entidad := TObjetoEntidad( Items.Objects[ ItemIndex ] ).Entidad;
     dmDiccionario.cdsEditDiccion.Refrescar;}
     with CBEntidades do
          dmDiccionario.RefrescaFormaConsulta(TObjetoEntidad( Items.Objects[ ItemIndex ] ).Entidad);
end;

procedure TDiccion_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     dmDiccionario.GetTablas( CBEntidades.Items );
     CBEntidades.ItemIndex := 0;
     CBEntidadesChange(Sender);
     ApplyMinWidth;
end;

procedure TDiccion_DevEx.CBEntidadesChange(Sender: TObject);
begin
     inherited;
     Refresh;
end;

procedure TDiccion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef VISITANTES}
     HelpContext := H_VISMGR_DICCIONARIO;
     {$ELSE}
            {$ifdef EVALUACION}
            HelpContext := 107000;
            {$else}
            HelpContext := H60653_Diccionario_datos;
            {$endif}
     {$endif}     
end;

procedure TDiccion_DevEx.Agregar;
begin
     dmDiccionario.cdsDiccion.Agregar;
end;

procedure TDiccion_DevEx.Borrar;
begin
     dmDiccionario.cdsDiccion.Borrar;
end;

procedure TDiccion_DevEx.Modificar;
begin
     dmDiccionario.cdsDiccion.Modificar;
     DoBestFit;
end;

function TDiccion_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
        Result:= False;
        sMensaje := 'No Se Puede Agregar Al Diccionario De Datos';
end;

function TDiccion_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
        Result:= False;
        sMensaje := 'No Se Puede Borrar En El Diccionario De Datos';
end;

end.
