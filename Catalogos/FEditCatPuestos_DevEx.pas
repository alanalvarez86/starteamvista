unit FEditCatPuestos_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Db, ExtCtrls, Buttons,
     DBCtrls, StdCtrls, Mask, dbClient,
     ZBaseEdicion_DevEx,
     ZetaNumero,
     ZetaEdit, ComCtrls, ZetaKeyCombo, ZetaSmartLists,ZetaClientDataSet,
  Grids, DBGrids, ZetaDBGrid, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC,
  ZetaKeyLookup_DevEx, dxBarBuiltInMenu;

type
  TEditCatPuestos_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    PU_CODIGO: TZetaDBEdit;
    PU_DESCRIP: TDBEdit;
    Label2: TLabel;
    PU_ACTIVO: TDBCheckBox;
    dsGpoCompetencias: TDataSource;
    btnFijas_DevEx: TcxButton;
    btnTools_DevEx: TcxButton;
    dxBarButton_bImpSubCuentas: TdxBarButton;
    PageControl_DevEx: TcxPageControl;
    tsGenerales_DevEx: TcxTabSheet;
    tsContratacion_DevEx: TcxTabSheet;
    tsAreas_DevEx: TcxTabSheet;
    tsSalario_DevEx: TcxTabSheet;
    tsOrganigrama_DevEx: TcxTabSheet;
    STPS_DevEx: TcxTabSheet;
    tsConfidencialidad_DevEx: TcxTabSheet;
    tbCompetencias_DevEx: TcxTabSheet;
    PU_SUB_CTA: TDBEdit;
    PU_TIPO: TZetaDBKeyCombo;
    PU_DETALLE: TDBMemo;
    PU_TEXTO: TDBEdit;
    PU_NUMERO: TZetaDBNumero;
    PU_INGLES: TDBEdit;
    Label12: TLabel;
    Label9: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label6: TLabel;
    Label5: TLabel;
    Label3: TLabel;
    PU_CHECA: TZetaDBKeyCombo;
    PU_CLASIFI: TZetaDBKeyLookup_DevEx;
    PU_AREA: TZetaDBKeyLookup_DevEx;
    PU_PATRON: TZetaDBKeyLookup_DevEx;
    PU_CONTRAT: TZetaDBKeyLookup_DevEx;
    PU_TURNO: TZetaDBKeyLookup_DevEx;
    Label21: TLabel;
    Label4: TLabel;
    Arealbl: TLabel;
    Label29: TLabel;
    Label27: TLabel;
    Label26: TLabel;
    PU_NIVEL12: TZetaDBKeyLookup_DevEx;
    PU_NIVEL11: TZetaDBKeyLookup_DevEx;
    PU_NIVEL10: TZetaDBKeyLookup_DevEx;
    PU_NIVEL9: TZetaDBKeyLookup_DevEx;
    PU_NIVEL8: TZetaDBKeyLookup_DevEx;
    PU_NIVEL7: TZetaDBKeyLookup_DevEx;
    PU_NIVEL1: TZetaDBKeyLookup_DevEx;
    PU_NIVEL6: TZetaDBKeyLookup_DevEx;
    PU_NIVEL5: TZetaDBKeyLookup_DevEx;
    PU_NIVEL4: TZetaDBKeyLookup_DevEx;
    PU_NIVEL3: TZetaDBKeyLookup_DevEx;
    PU_NIVEL2: TZetaDBKeyLookup_DevEx;
    PU_NIVEL12lbl: TLabel;
    PU_NIVEL11lbl: TLabel;
    PU_NIVEL10lbl: TLabel;
    PU_NIVEL9lbl: TLabel;
    PU_NIVEL8lbl: TLabel;
    PU_NIVEL7lbl: TLabel;
    PU_NIVEL1lbl: TLabel;
    PU_NIVEL6lbl: TLabel;
    PU_NIVEL5lbl: TLabel;
    PU_NIVEL4lbl: TLabel;
    PU_NIVEL3lbl: TLabel;
    PU_NIVEL2lbl: TLabel;
    PU_ZONA_GE: TDBComboBox;
    GBRangoSal: TGroupBox;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    PU_SAL_MIN: TZetaDBNumero;
    PU_SAL_MAX: TZetaDBNumero;
    PU_SAL_MED: TZetaDBNumero;
    PU_SAL_EN1: TZetaDBNumero;
    PU_SAL_EN2: TZetaDBNumero;
    PU_AUTOSAL: TZetaDBKeyCombo;
    PU_TABLASS: TZetaDBKeyLookup_DevEx;
    PU_PER_VAR: TZetaDBNumero;
    PU_SALARIO: TZetaDBNumero;
    LblTabulador: TLabel;
    Label28: TLabel;
    Label33: TLabel;
    LblSalario: TLabel;
    Label31: TLabel;
    GBPresupuesto: TGroupBox;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    PU_COSTO1: TZetaDBNumero;
    PU_COSTO2: TZetaDBNumero;
    PU_COSTO3: TZetaDBNumero;
    GroupBox1: TGroupBox;
    Label10: TLabel;
    ON_CODIGO: TZetaDBKeyLookup_DevEx;
    gridRenglones: TZetaDBGrid;
    Panel4: TPanel;
    BBAgregar_DevEx: TcxButton;
    BBBorrar_DevEx: TcxButton;
    BBModificar_DevEx: TcxButton;
    btnBuscarPerfil_DevEx: TcxButton;
    gbConfidencialidad: TGroupBox;
    rbConfidenTodas: TRadioButton;
    rbConfidenAlgunas: TRadioButton;
    listaConfidencialidad: TListBox;
    btSeleccionarConfiden_DevEx: TcxButton;
    cxTabSheet1: TcxTabSheet;
    GroupBox2: TGroupBox;
    Label11: TLabel;
    RIESGO_PUESTO_SAT: TZetaDBKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    //procedure btnFijasClick(Sender: TObject);
    //procedure btnToolsClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DataSourceStateChange(Sender: TObject);
    //procedure bImpSubCuentasClick(Sender: TObject);
    procedure PU_NIVEL12ValidLookup(Sender: TObject);
    procedure PU_NIVEL11ValidLookup(Sender: TObject);
    procedure PU_NIVEL10ValidLookup(Sender: TObject);
    procedure PU_NIVEL9ValidLookup(Sender: TObject);
    procedure PU_NIVEL8ValidLookup(Sender: TObject);
    procedure PU_NIVEL7ValidLookup(Sender: TObject);
    procedure PU_NIVEL6ValidLookup(Sender: TObject);
    procedure PU_NIVEL5ValidLookup(Sender: TObject);
    procedure PU_NIVEL4ValidLookup(Sender: TObject);
    procedure PU_NIVEL2ValidLookup(Sender: TObject);
    procedure PU_NIVEL3ValidLookup(Sender: TObject);
    //procedure btSeleccionarConfidenClick(Sender: TObject);
    procedure rbConfidenTodasClick(Sender: TObject);
    procedure rbConfidenAlgunasClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    //procedure bAgregaClick(Sender: TObject);
    //procedure bModificaClick(Sender: TObject);
    //procedure bBorraClick(Sender: TObject);
    procedure gridRenglonesColExit(Sender: TObject);
    procedure gridRenglonesDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    //procedure btnBuscarPerfilClick(Sender: TObject);
    procedure btnFijas_DevExClick(Sender: TObject);
    procedure btnTools_DevExClick(Sender: TObject);
    procedure dxBarButton_bImpSubCuentasClick(Sender: TObject);
    procedure btSeleccionarConfiden_DevExClick(Sender: TObject);
    procedure BBAgregar_DevExClick(Sender: TObject);
    procedure BBModificar_DevExClick(Sender: TObject);
    procedure BBBorrar_DevExClick(Sender: TObject);
    procedure MyMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure btnBuscarPerfil_DevExClick(Sender: TObject);
    procedure SetEditarSoloActivos;
  private
    { Private declarations }
    lTieneDerecho: Boolean;
    FPrimerColumna: Integer;
    lValoresConfidencialidad : TStringList;
    procedure SetCamposNivel;
    procedure LlenaListaBoolean( oLista: TStrings );
    {$ifdef ACS}procedure SetPosicionNiveles;{ACS}{$endif}
    {$ifdef ACS}
    procedure LimpiaLookUpOlfKey;{ACS}
    {$endif}
    procedure AplicaFiltro;
    procedure SeleccionaPrimerColumna;
    procedure FillListaConfidencialidad;
    procedure SetListConfidencialidad( sValores : string );
    procedure GetConfidencialidad;
  protected
    { Protected declarations }
    property PrimerColumna: Integer read FPrimerColumna write FPrimerColumna;
    procedure Connect; override;
    procedure DoLookup; override;
    procedure HabilitaControles;override;
    procedure KeyDown(var Key: Word; Shift: TShiftState);override;
    function ClientDatasetHijo: TZetaClientDataset;
    function GridEnfocado: Boolean;
    function CheckDerechosPadre: Boolean;
    function PosicionaSiguienteColumna: Integer; Virtual;
  public
    { Public declarations }
  end;
  TDBGridHack = class(TDBGrid);

var
  EditCatPuestos_DevEx: TEditCatPuestos_DevEx;

{$ifdef ACS}
const
     K_FORMA_ACS = 492;
     K_ALT_DEF = 24;
{$endif}

implementation

{$R *.DFM}

uses DCatalogos,
     DGlobal,
     ZetaCommonClasses,
     ZGlobalTress,
     ZetaBuscador_DevEx,
     ZAccesosMgr,
     ZAccesosTress,
     ZetaClientTools,
     ZImportaSubCuentas,
     dSistema,
     dTablas,
     dCliente,
     ZetaDialogo,
     ZetaCommonTools,
     FSeleccionarConfidencialidad_DevEx;

procedure TEditCatPuestos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     PU_NIVEL10.DataSource := DataSource;
     PU_NIVEL11.DataSource := DataSource;
     PU_NIVEL12.DataSource := DataSource;

     PU_NIVEL12lbl.Top := PU_NIVEL1lbl.Top;
     PU_NIVEL11lbl.Top := PU_NIVEL12lbl.Top + K_ALT_DEF;
     PU_NIVEL10lbl.Top := PU_NIVEL11lbl.Top + K_ALT_DEF;
     PU_NIVEL9lbl.Top := PU_NIVEL10lbl.Top + K_ALT_DEF;
     PU_NIVEL8lbl.Top := PU_NIVEL9lbl.Top + K_ALT_DEF;
     PU_NIVEL7lbl.Top := PU_NIVEL8lbl.Top + K_ALT_DEF;
     PU_NIVEL6lbl.Top := PU_NIVEL7lbl.Top + K_ALT_DEF;
     PU_NIVEL5lbl.Top := PU_NIVEL6lbl.Top + K_ALT_DEF;
     PU_NIVEL4lbl.Top := PU_NIVEL5lbl.Top + K_ALT_DEF;
     PU_NIVEL3lbl.Top := PU_NIVEL4lbl.Top + K_ALT_DEF;
     PU_NIVEL2lbl.Top := PU_NIVEL3lbl.Top + K_ALT_DEF;
     PU_NIVEL1lbl.Top := PU_NIVEL2lbl.Top + K_ALT_DEF;

     PU_NIVEL12.Top := PU_NIVEL1.Top;
     PU_NIVEL11.Top := PU_NIVEL12.Top + K_ALT_DEF;
     PU_NIVEL10.Top := PU_NIVEL11.Top + K_ALT_DEF;
     PU_NIVEL9.Top := PU_NIVEL10.Top + K_ALT_DEF;
     PU_NIVEL8.Top := PU_NIVEL9.Top + K_ALT_DEF;
     PU_NIVEL7.Top := PU_NIVEL8.Top + K_ALT_DEF;
     PU_NIVEL6.Top := PU_NIVEL7.Top + K_ALT_DEF;
     PU_NIVEL5.Top := PU_NIVEL6.Top + K_ALT_DEF;
     PU_NIVEL4.Top := PU_NIVEL5.Top + K_ALT_DEF;
     PU_NIVEL3.Top := PU_NIVEL4.Top + K_ALT_DEF;
     PU_NIVEL2.Top := PU_NIVEL3.Top + K_ALT_DEF;
     PU_NIVEL1.Top := PU_NIVEL2.Top + K_ALT_DEF;

     PU_NIVEL12.TabOrder := 1;
     PU_NIVEL11.TabOrder := 2;
     PU_NIVEL10.TabOrder := 3;
     PU_NIVEL9.TabOrder := 4;
     PU_NIVEL8.TabOrder := 5;
     PU_NIVEL7.TabOrder := 6;
     PU_NIVEL6.TabOrder := 7;
     PU_NIVEL5.TabOrder := 8;
     PU_NIVEL4.TabOrder := 9;
     PU_NIVEL3.TabOrder := 10;
     PU_NIVEL2.TabOrder := 11;
     PU_NIVEL1.TabOrder := 12;

     Self.Height := K_FORMA_ACS;
     {
     PU_NIVEL10.Visible := True;
     PU_NIVEL11.Visible := True;
     PU_NIVEL12.Visible := True;
     PU_NIVEL10lbl.Visible := True;
     PU_NIVEL11lbl.Visible := True;
     PU_NIVEL12lbl.Visible := True;
     }
     {$endif}
     IndexDerechos :=  D_CAT_CONT_PUESTOS;
     FirstControl := PU_CODIGO;
     with dmCatalogos do
     begin
          PU_CLASIFI.LookupDataset := cdsClasifi;
          PU_TURNO.LookupDataset := cdsTurnos;
          PU_CONTRAT.LookupDataset := cdsContratos;
          PU_TABLASS.LookupDataset := cdsSSocial;
          PU_PATRON.LookupDataset := cdsRPatron;
          PU_AREA.LookupDataset := cdsAreas;
     end;
     with dmTablas do
     begin
          PU_NIVEL1.LookupDataset := cdsNivel1;
          PU_NIVEL2.LookupDataset := cdsNivel2;
          PU_NIVEL3.LookupDataset := cdsNivel3;
          PU_NIVEL4.LookupDataset := cdsNivel4;
          PU_NIVEL5.LookupDataset := cdsNivel5;
          PU_NIVEL6.LookupDataset := cdsNivel6;
          PU_NIVEL7.LookupDataset := cdsNivel7;
          PU_NIVEL8.LookupDataset := cdsNivel8;
          PU_NIVEL9.LookupDataset := cdsNivel9;
          {$ifdef ACS}
          PU_NIVEL10.LookupDataset := cdsNivel10;
          PU_NIVEL11.LookupDataset := cdsNivel11;
          PU_NIVEL12.LookupDataset := cdsNivel12;
          {$endif}
     end;
//     PU_NIVEL0.LookupDataset := dmSistema.cdsNivel0;
     ON_CODIGO.LookupDataset := dmTablas.cdsOcupaNac;

     lValoresConfidencialidad := TStringList.Create;

     SetCamposNivel;
     LlenaListaBoolean( PU_AUTOSAL.Lista );
     LlenaListaBoolean( PU_CHECA.Lista );
     lTieneDerecho := ZAccesosMgr.CheckDerecho( D_CAT_CONT_PUESTOS, K_DERECHO_CAMBIO );
     HelpContext:= H60611_Puestos;
     {$ifdef DOS_CAPAS}
     tbCompetencias.Visible := False;
     tbCompetencias.TabVisible := False;
     {$endif}

      //DevEx (by am):Colores Base para el grid de Edicion
      GridRenglones.FixedColor := RGB(235,235,235);//Gris
      GridRenglones.TitleFont.Color := RGB(77,59,75);//Gris-Morado tono fuerte
      GridRenglones.Font.Color := RGB(156,129,139);//Gris-Morado

      //DevEx (by am): Asigna el evento OnMouseWheel al grid de edicion
      TDBGridHack( GridRenglones ).OnMouseWheel:= MyMouseWheel;

      //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
      SetEditarSoloActivos;
end;

procedure TEditCatPuestos_DevEx.MyMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin
     Handled := True;
     with GridRenglones.DataSource.DataSet do
     begin
          if WheelDelta > 0 then
             Prior
          else
              Next;
     end;
end;

procedure TEditCatPuestos_DevEx.Connect;
begin
     dmSistema.cdsNivel0.Conectar;
     with dmTablas do
     begin
          {$ifdef ACS}
          LimpiaLookUpOlfKey;
          {$endif}
          if PU_NIVEL1.Visible then cdsNivel1.Conectar;
          if PU_NIVEL2.Visible then cdsNivel2.Conectar;
          if PU_NIVEL3.Visible then cdsNivel3.Conectar;
          if PU_NIVEL4.Visible then cdsNivel4.Conectar;
          if PU_NIVEL5.Visible then cdsNivel5.Conectar;
          if PU_NIVEL6.Visible then cdsNivel6.Conectar;
          if PU_NIVEL7.Visible then cdsNivel7.Conectar;
          if PU_NIVEL8.Visible then cdsNivel8.Conectar;
          if PU_NIVEL9.Visible then cdsNivel9.Conectar;
          {$ifdef ACS}
          if PU_NIVEL10.Visible then cdsNivel10.Conectar;
          if PU_NIVEL11.Visible then cdsNivel11.Conectar;
          if PU_NIVEL12.Visible then cdsNivel12.Conectar;
          {$endif}
          cdsOcupaNac.Conectar;
     end;
     with dmCatalogos do
     begin
          cdsTurnos.Conectar;
          cdsContratos.Conectar;
          cdsSSocial.Conectar;
          cdsRPatron.Conectar;
          cdsAreas.Conectar;
          cdsClasifi.Conectar;
          cdsPuestosLookUp.Conectar;
          {$ifndef DOS_CAPAS}
          cdsCatPerfiles.Conectar;
          dsGpoCompetencias.DataSet := cdsGpoCompPuesto;
          {$endif}
          DataSource.DataSet := cdsPuestos;
     end;

     with dmTablas do
     begin
          cdsRiesgosPuestoSat.Conectar;
          RIESGO_PUESTO_SAT.LookUpDataSet := dmTablas.cdsRiesgosPuestoSat;
     end;
end;

procedure TEditCatPuestos_DevEx.FormShow(Sender: TObject);
begin
     inherited;
{
     btnFijas.Enabled := lTieneDerecho;       - Se define en DataSourceStateChange
     btnTools.Enabled := btnFijas.Enabled;
}
     Arealbl.Caption := Global.GetGlobalString( K_GLOBAL_LABOR_AREAS ) + ' en Labor:';
     PageControl_DevEx.ActivePage := tsGenerales_DevEx;
     {$ifdef ACS}SetPosicionNiveles;{ACS}{$endif}

     FillListaConfidencialidad;
end;

procedure TEditCatPuestos_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', 'Puestos', 'PU_CODIGO', dmCatalogos.cdsPuestos );
end;

procedure TEditCatPuestos_DevEx.SetCamposNivel;
var
   iNiveles : Integer;
begin
     iNiveles := Global.NumNiveles;
     SetCampoNivel( 1, iNiveles, PU_NIVEL1lbl, PU_NIVEL1 );
     SetCampoNivel( 2, iNiveles, PU_NIVEL2lbl, PU_NIVEL2 );
     SetCampoNivel( 3, iNiveles, PU_NIVEL3lbl, PU_NIVEL3 );
     SetCampoNivel( 4, iNiveles, PU_NIVEL4lbl, PU_NIVEL4 );
     SetCampoNivel( 5, iNiveles, PU_NIVEL5lbl, PU_NIVEL5 );
     SetCampoNivel( 6, iNiveles, PU_NIVEL6lbl, PU_NIVEL6 );
     SetCampoNivel( 7, iNiveles, PU_NIVEL7lbl, PU_NIVEL7 );
     SetCampoNivel( 8, iNiveles, PU_NIVEL8lbl, PU_NIVEL8 );
     SetCampoNivel( 9, iNiveles, PU_NIVEL9lbl, PU_NIVEL9 );
     {$ifdef ACS}
     SetCampoNivel( 10, iNiveles, PU_NIVEL10lbl, PU_NIVEL10 );
     SetCampoNivel( 11, iNiveles, PU_NIVEL11lbl, PU_NIVEL11 );
     SetCampoNivel( 12, iNiveles, PU_NIVEL12lbl, PU_NIVEL12 );
     {$endif}
end;

procedure TEditCatPuestos_DevEx.LlenaListaBoolean( oLista: TStrings );
const
     K_DEF_GLOBALES = 'Usar Global de Empresa';
begin
     with oLista do
     begin
          BeginUpdate;
          try
              Clear;
              Add( K_GLOBAL_SI + '=' + K_PROPPERCASE_SI );
              Add( K_GLOBAL_NO + '=' + K_PROPPERCASE_NO );
              Add( K_USAR_GLOBAL + '=' + K_DEF_GLOBALES );
          finally
                 EndUpdate;
          end;
     end;
end;

{procedure TEditCatPuestos_DevEx.btnFijasClick(Sender: TObject);
begin
     inherited;
     dmCatalogos.SetPuestoPercepcionesFijas;
end;

procedure TEditCatPuestos_DevEx.btnToolsClick(Sender: TObject);
begin
     inherited;
     dmCatalogos.SetPuestoHerramientas;
end;}

procedure TEditCatPuestos_DevEx.DataSourceStateChange(Sender: TObject);
begin
     inherited;
     btnFijas_DevEx.Enabled := lTieneDerecho and ( dmCatalogos.cdsPuestos.State = dsBrowse );
     btnTools_DevEx.Enabled := btnFijas_DevEx.Enabled;
     BBAgregar_DevEx.Enabled := dmCatalogos.cdsPuestos.State <> dsInsert ;
     BBModificar_DevEx.Enabled := BBAgregar_DevEx.Enabled;
     BBBorrar_DevEx.Enabled := BBAgregar_DevEx.Enabled;
     GridRenglones.Enabled := BBAgregar_DevEx.Enabled;
     if(dmCatalogos.cdsPuestos.State = dsInsert )then
       AplicaFiltro;
end;

{procedure TEditCatPuestos_DevEx.bImpSubCuentasClick(Sender: TObject);
begin
     inherited;
     ZImportaSubCuentas.ImportaArchivo( TZetaClientDataset( DataSource.DataSet ), 'PU_CODIGO', 'PU_SUB_CTA' );
end;}

procedure TEditCatPuestos_DevEx.HabilitaControles;
begin
     inherited;
     dxBarButton_bImpSubCuentas.Enabled := not Editing;
end;

{ACS: Se establece la relaci�n del nivel seleccionado con el nivel que tiene relacionado
      (nivel 12 al 2, el nivel 1 no tiene relaci�n por ser el nivel inferior).}
procedure TEditCatPuestos_DevEx.PU_NIVEL12ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel12.FieldByName( 'TB_RELACIO' ).AsString ) and PU_NIVEL11.Visible then
           DataSource.DataSet.FieldByName( 'PU_NIVEL11' ).AsString := dmTablas.cdsNivel12.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditCatPuestos_DevEx.PU_NIVEL11ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel11.FieldByName( 'TB_RELACIO' ).AsString ) and PU_NIVEL10.Visible then
           DataSource.DataSet.FieldByName( 'PU_NIVEL10' ).AsString := dmTablas.cdsNivel11.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditCatPuestos_DevEx.PU_NIVEL10ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel10.FieldByName( 'TB_RELACIO' ).AsString ) and PU_NIVEL9.Visible then
           DataSource.DataSet.FieldByName( 'PU_NIVEL9' ).AsString := dmTablas.cdsNivel10.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditCatPuestos_DevEx.PU_NIVEL9ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel9.FieldByName( 'TB_RELACIO' ).AsString ) and PU_NIVEL8.Visible then
           DataSource.DataSet.FieldByName( 'PU_NIVEL8' ).AsString := dmTablas.cdsNivel9.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditCatPuestos_DevEx.PU_NIVEL8ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel8.FieldByName( 'TB_RELACIO' ).AsString ) and PU_NIVEL7.Visible then
           DataSource.DataSet.FieldByName( 'PU_NIVEL7' ).AsString := dmTablas.cdsNivel8.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditCatPuestos_DevEx.PU_NIVEL7ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel7.FieldByName( 'TB_RELACIO' ).AsString ) and PU_NIVEL6.Visible then
           DataSource.DataSet.FieldByName( 'PU_NIVEL6' ).AsString := dmTablas.cdsNivel7.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditCatPuestos_DevEx.PU_NIVEL6ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel6.FieldByName( 'TB_RELACIO' ).AsString ) and PU_NIVEL5.Visible then
           DataSource.DataSet.FieldByName( 'PU_NIVEL5' ).AsString := dmTablas.cdsNivel6.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditCatPuestos_DevEx.PU_NIVEL5ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel5.FieldByName( 'TB_RELACIO' ).AsString ) and PU_NIVEL4.Visible then
           DataSource.DataSet.FieldByName( 'PU_NIVEL4' ).AsString := dmTablas.cdsNivel5.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditCatPuestos_DevEx.PU_NIVEL4ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel4.FieldByName( 'TB_RELACIO' ).AsString ) and PU_NIVEL3.Visible then
           DataSource.DataSet.FieldByName( 'PU_NIVEL3' ).AsString := dmTablas.cdsNivel4.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditCatPuestos_DevEx.PU_NIVEL3ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel3.FieldByName( 'TB_RELACIO' ).AsString ) and PU_NIVEL2.Visible then
           DataSource.DataSet.FieldByName( 'PU_NIVEL2' ).AsString := dmTablas.cdsNivel3.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditCatPuestos_DevEx.PU_NIVEL2ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel2.FieldByName( 'TB_RELACIO' ).AsString ) and PU_NIVEL1.Visible then
           DataSource.DataSet.FieldByName( 'PU_NIVEL1' ).AsString := dmTablas.cdsNivel2.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

{$ifdef ACS}
{ACS: Este procedimiento re-ajustar� la posici�n de los controles de los niveles.}
procedure TEditCatPuestos_DevEx.SetPosicionNiveles;
const
     K_ALTURA_DEF = 24;
     K_TOP_N1 = 266;
     K_TOP_N1_LBL = 270;
     K_TOP_N2 = 242;
     K_TOP_N2_LBL = 246;
     K_TOP_N3 = 218;
     K_TOP_N3_LBL = 222;
     K_TOP_N4 = 194;
     K_TOP_N4_LBL = 198;
     K_TOP_N5 = 170;
     K_TOP_N5_LBL = 174;
     K_TOP_N6 = 146;
     K_TOP_N6_LBL = 150;
     K_TOP_N7 = 122;
     K_TOP_N7_LBL = 126;
     K_TOP_N8 = 98;
     K_TOP_N8_LBL = 102;
     K_TOP_N9 = 74;
     K_TOP_N9_LBL = 78;
     K_TOP_N10 = 50;
     K_TOP_N10_LBL = 54;
     K_TOP_N11 = 26;
     K_TOP_N11_LBL = 30;
     K_TOP_N12 = 2;
     K_TOP_N12_LBL = 6;
var
     iNivelesNoVisibles, iTotalHorizontal: Integer;
begin
     iNivelesNoVisibles:= 0;
     {Se obtiene la cantidad total de niveles no visibles}
     if Not PU_NIVEL12.Visible then
        inc( iNivelesNoVisibles );
     if Not PU_NIVEL11.Visible then
        inc( iNivelesNoVisibles );
     if Not PU_NIVEL10.Visible then
        inc( iNivelesNoVisibles );
     if Not PU_NIVEL9.Visible then
        inc( iNivelesNoVisibles );
     if Not PU_NIVEL8.Visible then
        inc( iNivelesNoVisibles );
     if Not PU_NIVEL7.Visible then
        inc( iNivelesNoVisibles );
     if Not PU_NIVEL6.Visible then
        inc( iNivelesNoVisibles );
     if Not PU_NIVEL5.Visible then
        inc( iNivelesNoVisibles );
     if Not PU_NIVEL4.Visible then
        inc( iNivelesNoVisibles );
     if Not PU_NIVEL3.Visible then
        inc( iNivelesNoVisibles );
     if Not PU_NIVEL2.Visible then
        inc( iNivelesNoVisibles );

     iTotalHorizontal := ( iNivelesNoVisibles * K_ALTURA_DEF );
     {Se re-ajustan todos los controles en la posici�n horizontal}
     PU_NIVEL12.Top := K_TOP_N12 - iTotalHorizontal;
     PU_NIVEL12lbl.Top := K_TOP_N12_LBL - iTotalHorizontal;
     PU_NIVEL11.Top := K_TOP_N11 - iTotalHorizontal;
     PU_NIVEL11lbl.Top := K_TOP_N11_LBL - iTotalHorizontal;
     PU_NIVEL10.Top := K_TOP_N10 - iTotalHorizontal;
     PU_NIVEL10lbl.Top := K_TOP_N10_LBL - iTotalHorizontal;
     PU_NIVEL9.Top := K_TOP_N9 - iTotalHorizontal;
     PU_NIVEL9lbl.Top := K_TOP_N9_LBL - iTotalHorizontal;
     PU_NIVEL8.Top := K_TOP_N8 - iTotalHorizontal;
     PU_NIVEL8lbl.Top := K_TOP_N8_LBL - iTotalHorizontal;
     PU_NIVEL7.Top := K_TOP_N7 - iTotalHorizontal;
     PU_NIVEL7lbl.Top := K_TOP_N7_LBL - iTotalHorizontal;
     PU_NIVEL6.Top := K_TOP_N6 - iTotalHorizontal;
     PU_NIVEL6lbl.Top := K_TOP_N6_LBL - iTotalHorizontal;
     PU_NIVEL5.Top := K_TOP_N5 - iTotalHorizontal;
     PU_NIVEL5lbl.Top := K_TOP_N5_LBL - iTotalHorizontal;
     PU_NIVEL4.Top := K_TOP_N4 - iTotalHorizontal;
     PU_NIVEL4lbl.Top := K_TOP_N4_LBL - iTotalHorizontal;
     PU_NIVEL3.Top := K_TOP_N3 - iTotalHorizontal;
     PU_NIVEL3lbl.Top := K_TOP_N3_LBL - iTotalHorizontal;
     PU_NIVEL2.Top := K_TOP_N2 - iTotalHorizontal;
     PU_NIVEL2lbl.Top := K_TOP_N2_LBL - iTotalHorizontal;
     PU_NIVEL1.Top := K_TOP_N1 - iTotalHorizontal;
     PU_NIVEL1lbl.Top := K_TOP_N1_LBL - iTotalHorizontal;
end;
{$endif}

{$ifdef ACS}
{Este procedimiento sirve para eliminar cualquier OLDKEY de los lookups de niveles}
procedure TEditCatPuestos_DevEx.LimpiaLookUpOlfKey;
begin
     PU_NIVEL1.ResetMemory;
     PU_NIVEL2.ResetMemory;
     PU_NIVEL3.ResetMemory;
     PU_NIVEL4.ResetMemory;
     PU_NIVEL5.ResetMemory;
     PU_NIVEL6.ResetMemory;
     PU_NIVEL7.ResetMemory;
     PU_NIVEL8.ResetMemory;
     PU_NIVEL9.ResetMemory;
     PU_NIVEL10.ResetMemory;
     PU_NIVEL11.ResetMemory;
     PU_NIVEL12.ResetMemory;
end;
{$endif}

{procedure TEditCatPuestos_DevEx.btSeleccionarConfidenClick(Sender: TObject);
var
   selConfi : TSeleccionarConfidencialidad;
   sCM_NIVEL0Nuevo : string;
begin
     inherited;

     selConfi := TSeleccionarConfidencialidad.Create( Self );
     sCM_NIVEL0Nuevo := selConfi.GetConfidencialidad( DataSource.DataSet.FieldByName('PU_NIVEL0').AsString, lValoresConfidencialidad, True, dmCliente.Confidencialidad );

     if (  sCM_NIVEL0Nuevo <>  DataSource.DataSet.FieldByName('PU_NIVEL0').AsString ) then
     begin
        with DataSource.DataSet do
        begin
             if not( State in [dsEdit,dsInsert] ) then
                Edit;

              FieldByName('PU_NIVEL0').AsString  := sCM_NIVEL0Nuevo;
              GetConfidencialidad;
        end;
     end;

     FreeAndNil ( selConfi ) ;
end;}

procedure TEditCatPuestos_DevEx.FillListaConfidencialidad;
begin
     with dmSistema.cdsNivel0 do
     begin
          First;
          lValoresConfidencialidad.Clear;
          lValoresConfidencialidad.NameValueSeparator := '=' ;
          While( not Eof )do
          begin
               lValoresConfidencialidad.Add(FieldByName('TB_CODIGO').AsString +'='+ FieldByName('TB_ELEMENT').AsString);
               Next;
          end;
     end;
end;

procedure TEditCatPuestos_DevEx.GetConfidencialidad;
begin
     if (  DataSource.DataSet  <> nil ) then
     with DataSource.DataSet do
     begin
          SetListConfidencialidad(  FieldByName('PU_NIVEL0').AsString );
     end;
end;

procedure TEditCatPuestos_DevEx.SetListConfidencialidad(sValores: string);
var
   Lista : TStringList;
   i, j : integer;
begin
    Lista := TStringList.Create;

    sValores := Trim( sValores );
    if StrLleno( sValores ) then
        Lista.CommaText  := sValores;

    listaConfidencialidad.Items.Clear;

    rbConfidenTodas.OnClick := nil;
    rbConfidenAlgunas.OnClick := nil;
    rbConfidenTodas.Checked := False;
    rbConfidenAlgunas.Checked := False;

    if lValoresConfidencialidad.Count = 0 then
       FillListaConfidencialidad;

    for i:= 0 to lValoresConfidencialidad.Count - 1 do
    begin
        if ( Lista.Count > 0 ) then
        begin
                j := Lista.IndexOf( Trim( lValoresConfidencialidad.Names[i] )  );

                if ( j >= 0 ) then
                   listaConfidencialidad.Items.Add( lValoresConfidencialidad[i] )

                //CM_NIVEL0.Checked[i] :=  ( j >= 0 );
        end
    end;

    rbConfidenTodas.Checked :=   listaConfidencialidad.Items.Count = 0;
    rbConfidenAlgunas.Checked := not rbConfidenTodas.Checked;

    if ( tsConfidencialidad_DevEx.TabVisible ) then
    begin
        listaConfidencialidad.Enabled := rbConfidenAlgunas.Checked;
        btSeleccionarConfiden_DevEx.Enabled := rbConfidenAlgunas.Checked;
    end;

    rbConfidenTodas.OnClick := rbConfidenTodasClick;
    rbConfidenAlgunas.OnClick := rbConfidenAlgunasClick;

    FreeAndNil( Lista );

end;

procedure TEditCatPuestos_DevEx.rbConfidenTodasClick(Sender: TObject);
begin
  inherited;

   with DataSource.DataSet do
   begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

        FieldByName('PU_NIVEL0').AsString  :=  '';
   end;
   GetConfidencialidad;
end;

procedure TEditCatPuestos_DevEx.rbConfidenAlgunasClick(Sender: TObject);
begin
     inherited;
     if ( rbConfidenAlgunas.Checked ) then
     begin
        btSeleccionarConfiden_DevExClick( Sender );
        GetConfidencialidad;
     end;
end;

procedure TEditCatPuestos_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( Field = nil ) then
     begin
          with DataSource.DataSet do
          begin
               if ( State in [dsBrowse] ) then
               begin
                    GetConfidencialidad;
                    AplicaFiltro;
               end
               else if (State in [dsInsert]) then
               begin
                    rbConfidenTodas.Checked := TRUE;
               end;
          end;

     end;
end;

procedure TEditCatPuestos_DevEx.AplicaFiltro;
begin
     {$ifndef DOS_CAPAS}
     with dmCatalogos.cdsGpoCompPuesto do
     begin
          Filtered := False;
          Filter := Format('PU_CODIGO = ''%s''',[dmCatalogos.cdsPuestos.FieldByName('PU_CODIGO').AsString]);
          Filtered := True;
     end;
     {$endif}
end;

{procedure TEditCatPuestos_DevEx.bAgregaClick(Sender: TObject);
begin
     inherited;
     dmCatalogos.cdsGpoCompPuesto.Append;
     gridRenglones.SetFocus;
     SeleccionaPrimerColumna;
end;

procedure TEditCatPuestos_DevEx.bModificaClick(Sender: TObject);
begin
     inherited;
     dmCatalogos.cdsGpoCompPuesto.Edit;
     gridRenglones.SetFocus;
     SeleccionaPrimerColumna;
end;

procedure TEditCatPuestos_DevEx.bBorraClick(Sender: TObject);
begin
     inherited;
     dmCatalogos.cdsGpoCompPuesto.Borrar;
end; }

procedure TEditCatPuestos_DevEx.gridRenglonesColExit(Sender: TObject);
begin
     inherited;
     with GridRenglones do
     begin
          if ( SelectedField <> nil ) then
          begin
               if ( SelectedField.FieldName = 'CP_CODIGO' ) and
                  ( btnBuscarPerfil_DevEx.Visible ) then
                  btnBuscarPerfil_DevEx.Visible:= False;
          end;
     end;
end;

procedure TEditCatPuestos_DevEx.gridRenglonesDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
          State: TGridDrawState);
var
   iCurrentRow, iRecNo: Integer;
begin
     {
     with ZetaDBGrid, Canvas, Brush do
    begin
         if ( State = [] ) then
         begin
              iCurrentRow := Rect.Top div (1+Rect.Bottom-Rect.Top);
              iRecNo:= TDBGridHack( ZetaDBGrid ).Row;
              if iCurrentRow = iRecNo then
              begin
                   //Color:= clBtnShadow;
                   Color:=  RGB(237,230,249);//RGB(156,129,139); //morado row seleccionado
                   Font.Color := clWindowText;
              end;
         end
         else if gdSelected in State then
         begin
              //Color:= clHighlight;
              Color:= RGB(212,188,251);//RGB(136,174,213); //Azul de celda seleccionado
              Font.Color := clHighlightText; //Texto en blanco para celda seleccionada
         end;
         DefaultDrawColumnCell(Rect, DataCol, Column, State);
    end;
     }
     with GridRenglones, Canvas, Brush do
    begin
         if ( State = [] ) then
         begin
              iCurrentRow := Rect.Top div (1+Rect.Bottom-Rect.Top);
              iRecNo:= TDBGridHack( GridRenglones ).Row;
              if iCurrentRow = iRecNo then
              begin
                   //Color:= clBtnShadow;
                   Color:=  RGB(237,230,249);//RGB(156,129,139); //morado row seleccionado
                   Font.Color := clWindowText;
              end;
         end
         else if gdSelected in State then
         begin
              //Color:= clHighlight;
              Color:= RGB(212,188,251);//RGB(136,174,213); //Azul de celda seleccionado
              Font.Color := clHighlightText; //Texto en blanco para celda seleccionada
         end;
         DefaultDrawColumnCell(Rect, DataCol, Column, State);
    end;
     //Se muestra el boton para buscar codigo
     if (( gdFocused in State ) and ( ClientDatasetHijo.State in [dsEdit,dsInsert] ) )then
     begin
          if ( Column.FieldName = 'CP_CODIGO' ) then
          begin
               with btnBuscarPerfil_DevEx do
               begin
                    Left := Rect.Left + GridRenglones.Left + Column.Width - Width;
                    Top := Rect.Top;
                    Visible := True;
               end;
          end;
     end
     else if  (ClientDatasetHijo.State in [dsBrowse ] )then
     begin
          btnBuscarPerfil_DevEx.Visible := False;
     end;
end;

{procedure TEditCatPuestos_DevEx.btnBuscarPerfilClick(Sender: TObject);
begin
     inherited;
     with dmCatalogos do
     begin
          ZetaBuscador_DevEx.BuscarCodigo( 'Perfiles', 'Perfiles', 'CP_CODIGO', cdsCatPerfiles );
          cdsGpoCompPuesto.FieldByName('CP_CODIGO').AsString := cdsCatPerfiles.FieldByName('CP_CODIGO').AsString;
     end;
end; }

procedure TEditCatPuestos_DevEx.KeyDown( var Key: Word; Shift: TShiftState );
begin
     {$ifndef DOS_CAPAS}
     if GridEnfocado and not ( ClientDatasetHijo.State in [ dsInsert, dsEdit ] ) then
     begin
          if ( ssShift in Shift ) then { SHIFT }
          begin
               case Key of
                    VK_INSERT:    { INS = Agregar }
                    begin
                         Key := 0;
                         DoInsert;
                    end;
                    VK_DELETE:    { DEL = Borrar }
                    begin
                         Key := 0;
                         DoDelete;
                    end;
               end;
          end
          else
              if ( ssCtrl in Shift ) then { CTRL }
              begin
                   case Key of
                        VK_INSERT:   { INS = Modificar }
                        begin
                             Key := 0;
                             DoEdit;
                        end;
                   end;
              end;
              if ( ssCtrl in Shift ) then { CTRL }
              begin
                   case Key of
                        66:   { F }
                        begin
                             Key := 0;
                             btnBuscarPerfil_DevExClick(Self);
                        end;
                   end;
              end;
     end;
     {$endif}
     inherited KeyDown( Key, Shift );
end;

function TEditCatPuestos_DevEx.GridEnfocado: Boolean;
begin
     Result := ( ActiveControl = GridRenglones );
end;

function TEditCatPuestos_DevEx.PosicionaSiguienteColumna: Integer;
var
   i: Integer;
begin
     with GridRenglones do
     begin
          i := SelectedIndex + 1;
          while ( i < Columns.Count ) and ( Columns[ i ].ReadOnly or not Columns[ i ].Visible ) do
          begin
               i := i + 1;
          end;
          if ( i = Columns.Count ) then
             Result := FPrimerColumna
          else
              Result := i;
     end;

end;

procedure TEditCatPuestos_DevEx.SeleccionaPrimerColumna;
begin
     Self.ActiveControl := GridRenglones;
     GridRenglones.SelectedField := GridRenglones.Columns[ FPrimerColumna ].Field;
end;

function TEditCatPuestos_DevEx.CheckDerechosPadre: Boolean;
var
   sMensaje: String;
begin
     if Editing then
        Result := True
     else
         if PuedeModificar( sMensaje ) then
            Result := True
         else
         begin
              ZetaDialogo.zInformation( Caption, sMensaje, 0 );
              Result := False;
         end;
end;

function TEditCatPuestos_DevEx.ClientDatasetHijo: TZetaClientDataset;
begin
     Result := TZetaClientDataset( dsGpoCompetencias.Dataset );
end;

procedure TEditCatPuestos_DevEx.btnFijas_DevExClick(Sender: TObject);
begin
  inherited;
     dmCatalogos.SetPuestoPercepcionesFijas;
end;

procedure TEditCatPuestos_DevEx.btnTools_DevExClick(Sender: TObject);
begin
  inherited;
     dmCatalogos.SetPuestoHerramientas;

end;

procedure TEditCatPuestos_DevEx.dxBarButton_bImpSubCuentasClick(
  Sender: TObject);
begin
  inherited;
     ZImportaSubCuentas.ImportaArchivo( TZetaClientDataset( DataSource.DataSet ), 'PU_CODIGO', 'PU_SUB_CTA' );

end;

procedure TEditCatPuestos_DevEx.btSeleccionarConfiden_DevExClick(
  Sender: TObject);
var
   selConfi : TSeleccionarConfidencialidad_DevEx;
   sCM_NIVEL0Nuevo : string;
begin
     inherited;

     selConfi := TSeleccionarConfidencialidad_DevEx.Create( Self );
     sCM_NIVEL0Nuevo := selConfi.GetConfidencialidad( DataSource.DataSet.FieldByName('PU_NIVEL0').AsString, lValoresConfidencialidad, True, dmCliente.Confidencialidad );

     if (  sCM_NIVEL0Nuevo <>  DataSource.DataSet.FieldByName('PU_NIVEL0').AsString ) then
     begin
        with DataSource.DataSet do
        begin
             if not( State in [dsEdit,dsInsert] ) then
                Edit;

              FieldByName('PU_NIVEL0').AsString  := sCM_NIVEL0Nuevo;
              GetConfidencialidad;
        end;
     end;

     FreeAndNil ( selConfi ) ;
end;

procedure TEditCatPuestos_DevEx.BBAgregar_DevExClick(Sender: TObject);
begin
     inherited;
     dmCatalogos.cdsGpoCompPuesto.Append;
     gridRenglones.SetFocus;
     SeleccionaPrimerColumna;
end;

procedure TEditCatPuestos_DevEx.BBModificar_DevExClick(Sender: TObject);
begin
     inherited;
     dmCatalogos.cdsGpoCompPuesto.Edit;
     gridRenglones.SetFocus;
     SeleccionaPrimerColumna;
end;

procedure TEditCatPuestos_DevEx.BBBorrar_DevExClick(Sender: TObject);
begin
     inherited;
     dmCatalogos.cdsGpoCompPuesto.Borrar;
end;

procedure TEditCatPuestos_DevEx.btnBuscarPerfil_DevExClick(
  Sender: TObject);
begin
     inherited;
     with dmCatalogos do
     begin
          ZetaBuscador_DevEx.BuscarCodigo( 'Perfiles', 'Perfiles', 'CP_CODIGO', cdsCatPerfiles );
          cdsGpoCompPuesto.FieldByName('CP_CODIGO').AsString := cdsCatPerfiles.FieldByName('CP_CODIGO').AsString;
     end;
end;

procedure TEditCatPuestos_DevEx.SetEditarSoloActivos;
begin
     PU_CONTRAT.EditarSoloActivos := TRUE;
     PU_CLASIFI.EditarSoloActivos := TRUE;
     PU_TURNO.EditarSoloActivos := TRUE;
     PU_PATRON.EditarSoloActivos := TRUE;
     PU_NIVEL1.EditarSoloActivos := TRUE;
     PU_NIVEL2.EditarSoloActivos := TRUE;
     PU_NIVEL3.EditarSoloActivos := TRUE;
     PU_NIVEL4.EditarSoloActivos := TRUE;
     PU_NIVEL5.EditarSoloActivos := TRUE;
     PU_NIVEL6.EditarSoloActivos := TRUE;
     PU_NIVEL7.EditarSoloActivos := TRUE;
     PU_NIVEL8.EditarSoloActivos := TRUE;
     PU_NIVEL9.EditarSoloActivos := TRUE;
     PU_NIVEL10.EditarSoloActivos := TRUE;
     PU_NIVEL11.EditarSoloActivos := TRUE;
     PU_NIVEL12.EditarSoloActivos := TRUE;
     PU_TABLASS.EditarSoloActivos := TRUE;
end;

end.
