unit FEditCatTiposPension_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, ZetaNumero, Mask, DBCtrls,
  StdCtrls, ZetaEdit, DB, ExtCtrls, ZetaBusqueda_DevEx, Buttons,ZetaDialogo,
  ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter, cxContainer,
  cxEdit, cxListBox, dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator,
  cxDBNavigator, cxButtons;

type
  TEditCatTiposPension_DevEx = class(TBaseEdicion_DevEx)
    DBCodigoLBL: TLabel;
    TP_CODIGO: TZetaDBEdit;
    TP_NOMBRE: TDBEdit;
    TP_INGLES: TDBEdit;
    PT_NUMERO: TZetaDBNumero;
    Label2: TLabel;
    DBInglesLBL: TLabel;
    lblNombre: TLabel;
    GroupBox1: TGroupBox;
    TP_PERCEP: TDBCheckBox;
    TP_PRESTA: TDBCheckBox;
    GroupBox2: TGroupBox;
    TP_APL_NOR: TDBCheckBox;
    TP_APL_LIQ: TDBCheckBox;
    TP_APL_IND: TDBCheckBox;
    Label1: TLabel;
    TP_TEXTO: TDBEdit;
    Label3: TLabel;
    btnSelConceptosDeduc: TcxButton;
    btnSelConceptosPago: TcxButton;
    btnRemoverPensiones: TcxButton;
    btnRemoverDescuento: TcxButton;
    TP_CO_PENS: TcxListBox;
    TP_CO_DESC: TcxListBox;
    TP_DESC_N: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure btnSelConceptosPagoClick(Sender: TObject);
    procedure btnSelConceptosDeducClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure OKClick(Sender: TObject);
    procedure btnRemoverDescuentoClick(Sender: TObject);
    procedure btnRemoverPensionesClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);

  private
    { Private declarations }
    
    procedure SeleccionarConceptos(Lista:TcxListBox);
    function NoExisteConcepto(sKey:string;Lista:TcxListBox): Boolean;
    procedure LLenarLista(TP_CO_PENS, TP_CO_DESC: TcxListBox);
    procedure GrabarListas(TP_CO_PENS, TP_CO_DESC: TcxListBox);
    procedure RemoverConcepto(Lista: TcxListBox);
  public
    { Public declarations }
      procedure DoLookup; override;
  protected
     procedure Connect; override;
  end;

var
  EditCatTiposPension_DevEx: TEditCatTiposPension_DevEx;

implementation

uses
    DReportes,
    zReportTools,
    dCatalogos,
    ZetaCommonClasses,
    ZetaCommonTools,
    ZAccesosTress,
    ZetaBuscador_DevEx;

{$R *.dfm}

{ TFEditCatTipoPoliza }

procedure TEditCatTiposPension_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl  := TP_CODIGO;
     IndexDerechos := D_CAT_NOMINA_TPENSION;
     HelpContext   := H_Cat_TiposPension_Edit;
end;

procedure TEditCatTiposPension_DevEx.Connect;
begin

     with dmCatalogos do
     begin
          cdsConceptos.Conectar;
          cdsTiposPension.Conectar;
          DataSource.DataSet := cdsTiposPension;
     end;

end;

procedure TEditCatTiposPension_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Self.Caption, 'TP_CODIGO', dmCatalogos.cdsTiposPension );
end;

procedure TEditCatTiposPension_DevEx.SeleccionarConceptos(Lista:TcxListBox);
var
   sKey:string;
   sDescrip:string;
begin
     with dmCatalogos do
     begin

          if ZetaBusqueda_DevEx.ShowSearchForm( cdsConceptos, VACIO, sKey, sDescrip, FALSE, FALSE ) then
          begin
               if NoExisteConcepto(sKey +':'+sDescrip,Lista) then
               begin
                    if cdsTiposPension.State = dsBrowse then
                       cdsTiposPension.Edit;
                    if ( StrLLeno ( sKey ) ) then
                    begin
                         Lista.Items.Add(sKey +':'+sDescrip);
                    end
               end
               else
                   ZWarning('Tipos de Pensi�n',Format('Ya existe el Concepto: %s en la lista',[sKey +':'+sDescrip]),0,mbOk);
          end;
     end;
end;

function TEditCatTiposPension_DevEx.NoExisteConcepto(sKey:string;Lista:TcxListBox):Boolean;
var
    i:Integer;
begin
     Result := True;
     for i := 0 to  Lista.Items.Count - 1 do
     begin
          Result := not (  Lista.Items[ i ] = sKey );
          if Not Result then
             Exit;
     end;

end;

procedure TEditCatTiposPension_DevEx.btnSelConceptosPagoClick(Sender: TObject);
begin
     inherited;
     SeleccionarConceptos(TP_CO_PENS);
end;

procedure TEditCatTiposPension_DevEx.btnSelConceptosDeducClick(Sender: TObject);
begin
     inherited;
     SeleccionarConceptos(TP_CO_DESC);
end;

procedure TEditCatTiposPension_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
     inherited;
     if Field = nil then
     begin
          LLenarLista(TP_CO_PENS,TP_CO_DESC);
     end;
end;

Procedure  TEditCatTiposPension_DevEx.LLenarLista(TP_CO_PENS,TP_CO_DESC:TcxListBox);
var
   oLista:TStringList;
   sDescrip:string;

   procedure LlenaLista(Lista:TcxListBox;sFieldName:string);
   var
      i:Integer;
   begin
        oLista := TStringList.Create;
        try
           oLista.CommaText := dmCatalogos.cdsTiposPension.FieldByName(sFieldName).AsString;
           Lista.Items.Clear;
           for i := 0 to oLista.Count - 1 do
           begin
                sDescrip := dmCatalogos.cdsConceptos.GetDescripcion(oLista.Strings[i]);
                Lista.Items.Add(oLista.Strings[i]+':'+sDescrip)
           end;
        finally
               FreeAndNil(oLista);
        end;
   end;
begin

     with dmCatalogos.cdsTiposPension do
     begin
          LlenaLista(TP_CO_PENS,'TP_CO_PENS');
          LlenaLista(TP_CO_DESC,'TP_CO_DESC');

     end;
end;

procedure TEditCatTiposPension_DevEx.GrabarListas(TP_CO_PENS,TP_CO_DESC:TcxListBox);
var
   sValor,sKey:string;
   procedure GrabaLista(Lista:TcxListBox;sFieldName:string);
   var
      i:Integer;
   begin
        sValor := VACIO;
        for i := 0 to Lista.Items.Count - 1 do
        begin
             sKey := StrLeft( Lista.Items[i],Pos(':',Lista.Items[i] )-1 );
             sValor := ConcatString(sValor,sKey,',');
        end;
        dmCatalogos.cdsTiposPension.FieldByName(sFieldName).AsString := sValor;
   end;
begin
     GrabaLista(TP_CO_PENS,'TP_CO_PENS');
     GrabaLista(TP_CO_DESC,'TP_CO_DESC');
end;

procedure TEditCatTiposPension_DevEx.OKClick(Sender: TObject);
begin
     GrabarListas(TP_CO_PENS,TP_CO_DESC);
     inherited;
end;

procedure TEditCatTiposPension_DevEx.RemoverConcepto(Lista: TcxListBox);
var
   iItemSel :Integer;
begin
     if Lista.ItemIndex > -1 then
     begin
          iItemSel := Lista.ItemIndex;
          with dmCatalogos.cdsTiposPension do
          begin
               if State = dsBrowse then
                  Edit;
          end;
          Lista.Items.Delete(iItemSel);
     end
     else
         ZWarning('Tipos de Pensi�n','Seleccionar Concepto a Remover',0,mbOk);

end;

procedure TEditCatTiposPension_DevEx.btnRemoverDescuentoClick(Sender: TObject);
begin
     RemoverConcepto(TP_CO_DESC);
end;

procedure TEditCatTiposPension_DevEx.btnRemoverPensionesClick(Sender: TObject);
begin
     RemoverConcepto(TP_CO_PENS);
end;

procedure TEditCatTiposPension_DevEx.OK_DevExClick(Sender: TObject);
begin
     GrabarListas(TP_CO_PENS,TP_CO_DESC);
     inherited;
end;

end.
