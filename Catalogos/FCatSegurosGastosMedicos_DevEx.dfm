inherited CatSegurosGastosMedicos_DevEx: TCatSegurosGastosMedicos_DevEx
  Left = 261
  Top = 203
  Caption = 'Seguros de Gastos M'#233'dicos'
  ClientWidth = 1037
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 1037
    inherited ValorActivo2: TPanel
      Width = 778
      inherited textoValorActivo2: TLabel
        Width = 772
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 1037
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object PM_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'PM_CODIGO'
        Options.Grouping = False
      end
      object PM_NUMERO: TcxGridDBColumn
        Caption = 'No. P'#243'liza'
        DataBinding.FieldName = 'PM_NUMERO'
        Options.Grouping = False
      end
      object PM_DESCRIP: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'PM_DESCRIP'
        Options.Grouping = False
      end
      object PM_TIPO: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'PM_TIPO'
      end
      object PM_ASEGURA: TcxGridDBColumn
        Caption = 'Aseguradora'
        DataBinding.FieldName = 'PM_ASEGURA'
        Options.Grouping = False
      end
      object PM_BROKER: TcxGridDBColumn
        Caption = 'Broker'
        DataBinding.FieldName = 'PM_BROKER'
        Options.Grouping = False
      end
      object PM_NOM_CT1: TcxGridDBColumn
        Caption = 'Contacto #1'
        DataBinding.FieldName = 'PM_NOM_CT1'
        Options.Grouping = False
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 576
    Top = 224
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
