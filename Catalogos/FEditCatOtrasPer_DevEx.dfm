inherited EditCatOtrasPer_DevEx: TEditCatOtrasPer_DevEx
  Left = 696
  Top = 205
  Caption = 'Percepciones Fijas'
  ClientHeight = 378
  ClientWidth = 359
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 340
    Width = 359
    Height = 38
    TabOrder = 3
    inherited OK_DevEx: TcxButton
      Left = 191
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 270
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 359
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 33
      inherited textoValorActivo2: TLabel
        Width = 27
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    Left = 16
    Top = 136
    TabOrder = 7
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 50
    Width = 359
    Height = 290
    Align = alClient
    BevelInner = bvRaised
    BevelOuter = bvNone
    TabOrder = 1
    object Label1: TLabel
      Left = 77
      Top = 11
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label2: TLabel
      Left = 54
      Top = 33
      Width = 59
      Height = 13
      Caption = 'Descripci'#243'n:'
    end
    object Label3: TLabel
      Left = 82
      Top = 55
      Width = 31
      Height = 13
      Caption = 'Ingl'#233's:'
    end
    object Label5: TLabel
      Left = 73
      Top = 76
      Width = 40
      Height = 13
      Caption = 'N'#250'mero:'
    end
    object Label6: TLabel
      Left = 83
      Top = 98
      Width = 30
      Height = 13
      Caption = 'Texto:'
    end
    object Label7: TLabel
      Left = 89
      Top = 136
      Width = 24
      Height = 13
      Caption = 'Tipo:'
    end
    object MontoLbl: TLabel
      Left = 38
      Top = 178
      Width = 75
      Height = 13
      Caption = 'Cantidad Diaria:'
    end
    object TasaLbl: TLabel
      Left = 9
      Top = 201
      Width = 104
      Height = 13
      Caption = 'Porcentaje de Salario:'
    end
    object TopeLbl: TLabel
      Left = 58
      Top = 223
      Width = 55
      Height = 13
      Caption = 'Tope Diario'
    end
    object Label10: TLabel
      Left = 84
      Top = 245
      Width = 29
      Height = 13
      Caption = 'IMSS:'
    end
    object Label11: TLabel
      Left = 86
      Top = 269
      Width = 27
      Height = 13
      Caption = 'ISPT:'
    end
    object TB_CODIGO: TZetaDBEdit
      Left = 119
      Top = 7
      Width = 50
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
      ConfirmEdit = True
      DataField = 'TB_CODIGO'
      DataSource = DataSource
    end
    object TB_ELEMENT: TDBEdit
      Left = 119
      Top = 29
      Width = 230
      Height = 21
      DataField = 'TB_ELEMENT'
      DataSource = DataSource
      TabOrder = 1
    end
    object TB_INGLES: TDBEdit
      Left = 119
      Top = 51
      Width = 230
      Height = 21
      DataField = 'TB_INGLES'
      DataSource = DataSource
      TabOrder = 2
    end
    object TB_NUMERO: TZetaDBNumero
      Left = 119
      Top = 72
      Width = 121
      Height = 21
      Mascara = mnNumeroGlobal
      TabOrder = 3
      Text = '0.00'
      UseEnterKey = True
      DataField = 'TB_NUMERO'
      DataSource = DataSource
    end
    object TB_TEXTO: TDBEdit
      Left = 119
      Top = 94
      Width = 230
      Height = 21
      DataField = 'TB_TEXTO'
      DataSource = DataSource
      TabOrder = 4
    end
    object TB_TIPO: TDBRadioGroup
      Left = 119
      Top = 115
      Width = 148
      Height = 54
      DataField = 'TB_TIPO'
      DataSource = DataSource
      Items.Strings = (
        'Cantidad Diaria'
        'Porcentaje de Salario')
      TabOrder = 5
      Values.Strings = (
        '0'
        '1')
      OnChange = TB_TIPOChange
    end
    object TB_MONTO: TZetaDBNumero
      Left = 119
      Top = 174
      Width = 121
      Height = 21
      Mascara = mnPesos
      TabOrder = 6
      Text = '0.00'
      UseEnterKey = True
      DataField = 'TB_MONTO'
      DataSource = DataSource
    end
    object TB_TASA: TZetaDBNumero
      Left = 119
      Top = 197
      Width = 121
      Height = 21
      Mascara = mnTasa
      TabOrder = 7
      Text = '0.0 %'
      UseEnterKey = True
      DataField = 'TB_TASA'
      DataSource = DataSource
    end
    object TB_TOPE: TZetaDBNumero
      Left = 119
      Top = 219
      Width = 121
      Height = 21
      Mascara = mnPesos
      TabOrder = 8
      Text = '0.00'
      UseEnterKey = True
      DataField = 'TB_TOPE'
      DataSource = DataSource
    end
    object TB_IMSS: TZetaDBKeyCombo
      Left = 119
      Top = 242
      Width = 150
      Height = 21
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 9
      ListaFija = lfIMSSOtrasPer
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
      DataField = 'TB_IMSS'
      DataSource = DataSource
      LlaveNumerica = True
    end
    object TB_ISPT: TZetaDBKeyCombo
      Left = 119
      Top = 265
      Width = 150
      Height = 21
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 10
      ListaFija = lfISPTOtrasPer
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
      DataField = 'TB_ISPT'
      DataSource = DataSource
      LlaveNumerica = True
    end
  end
  inherited DataSource: TDataSource
    Left = 300
    Top = 177
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 14156080
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    Left = 312
    Top = 256
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_BuscarBtn: TdxBarButton
      Visible = ivAlways
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    DesignInfo = 19398976
  end
end
