unit FEditCatValNivel_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ExtCtrls, DBCtrls, StdCtrls, Mask, ZetaNumero, ZetaEdit,
  ZBaseEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons, cxContainer, cxEdit,
  cxTextEdit, cxMemo, cxDBEdit;

type
  TEditCatValNivel_DevEx = class(TBaseEdicion_DevEx)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    VL_CODIGO: TZetaDBNumero;
    VN_ORDEN: TZetaDBNumero;
    VN_CODIGO: TDBEdit;
    VN_NOMBRE: TDBEdit;
    VN_DESCRIP: TcxDBMemo;
    VN_INGLES: TDBEdit;
    VN_DES_ING: TcxDBMemo;
    VN_TEXTO: TDBEdit;
    VN_NUMERO: TZetaDBNumero;
    VL_NOMBRE: TEdit;
    VN_COLOR: TColorBox;
    Label10: TLabel;
    Label11: TLabel;
    VS_ORDEN: TZetaDBNumero;
    VS_NOMBRE: TEdit;
    Label12: TLabel;
    VN_PUNTOS: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
    procedure VN_COLORChange(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
  protected
    procedure Connect; override;
  public
    { Public declarations }
    procedure DoLookup; override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
  end;

var
  EditCatValNivel_DevEx: TEditCatValNivel_DevEx;

implementation

uses dCatalogos,
     ZetaBuscador_DevEx,
     ZetaCommonClasses,
     ZAccesosTress;

{$R *.dfm}

{ TEditCatValNivel }

procedure TEditCatValNivel_DevEx.Connect;
begin
     inherited;
     DataSource.DataSet := dmCatalogos.cdsValNiveles;
     with dmCatalogos do
     begin
          VL_NOMBRE.Text:= cdsValPlantilla.GetDescripcion( VL_CODIGO.Text );
          VS_NOMBRE.Text:= cdsSubfactores.FieldByName('VS_NOMBRE').AsString;
          //VN_COLOR.Selected:= cdsValNiveles.FieldByName('VN_COLOR').AsInteger;
     end;
end;

procedure TEditCatValNivel_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Escala de Subfactor', Self.Caption, 'VN_ORDEN', dmCatalogos.cdsValNiveles );
end;


procedure TEditCatValNivel_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_VAL_SUBFACTORES;
     FirstControl := VN_CODIGO;
     HelpContext:= H_VALUACION_NIVELES;
end;

function TEditCatValNivel_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeAgregar( sMensaje );
     if Result then
          Result:= dmCatalogos.PlantillaEnDiseno( dmCatalogos.PlantillaActiva, sMensaje, 'Agregar' );
end;

function TEditCatValNivel_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeBorrar( sMensaje );
     if Result then
        Result:= dmCatalogos.PlantillaEnDiseno( dmCatalogos.PlantillaActiva, sMensaje, 'Borrar');
end;

procedure TEditCatValNivel_DevEx.VN_COLORChange(Sender: TObject);
begin
     with dmCatalogos.cdsValNiveles do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
          FieldByName('VN_COLOR').AsInteger:= VN_COLOR.Selected;
     end;
end;

procedure TEditCatValNivel_DevEx.DataSourceDataChange(Sender: TObject;Field: TField);
begin
     inherited;
     with dmCatalogos.cdsValNiveles do
     begin
          if not ( State in [ dsInactive, dsEdit ] ) then
             VN_COLOR.Selected:= FieldByName('VN_COLOR').AsInteger;
     end;
end;

end.
