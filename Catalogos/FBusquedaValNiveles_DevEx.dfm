inherited BusquedaValNiveles_DevEx: TBusquedaValNiveles_DevEx
  Left = 527
  Top = 200
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderIcons = [biSystemMenu, biMaximize]
  BorderStyle = bsSizeable
  Caption = 'Seleccionar Nivel de Subfactor'
  ClientHeight = 556
  ClientWidth = 385
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  ExplicitWidth = 401
  ExplicitHeight = 594
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 0
    Top = 379
    Width = 385
    Height = 3
    Cursor = crVSplit
    Align = alBottom
  end
  inherited PanelBotones: TPanel
    Top = 520
    Width = 385
    ExplicitTop = 520
    ExplicitWidth = 385
    inherited OK_DevEx: TcxButton
      Left = 221
      OnClick = OK_DevExClick
      ExplicitLeft = 221
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 300
      Cancel = True
      ExplicitLeft = 300
    end
  end
  object PageControl2: TcxPageControl [2]
    Left = 0
    Top = 382
    Width = 385
    Height = 138
    Hint = ''
    Align = alBottom
    TabOrder = 1
    Properties.ActivePage = TabSheet4
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 136
    ClientRectLeft = 2
    ClientRectRight = 383
    ClientRectTop = 27
    object TabSheet3: TcxTabSheet
      Caption = 'Descripci'#243'n'
      object VN_DESCRIP: TcxDBMemo
        Left = 0
        Top = 0
        Hint = ''
        Align = alClient
        DataBinding.DataField = 'VN_DESCRIP'
        DataBinding.DataSource = DataSource
        TabOrder = 0
        Height = 109
        Width = 381
      end
    end
    object TabSheet4: TcxTabSheet
      Caption = 'Ingl'#233's'
      ImageIndex = 1
      object VN_DES_ING: TcxDBMemo
        Left = 0
        Top = 0
        Hint = ''
        Align = alClient
        DataBinding.DataField = 'VN_DES_ING'
        DataBinding.DataSource = DataSource
        TabOrder = 0
        Height = 109
        Width = 381
      end
    end
  end
  object DBGrid: TcxGrid [3]
    Left = 0
    Top = 0
    Width = 385
    Height = 379
    Hint = ''
    Align = alClient
    TabOrder = 2
    object ZetaDBGridDBTableView: TcxGridDBTableView
      OnDblClick = ZetaDBGridDBTableViewDblClick
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = DataSource
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      object VN_ORDEN: TcxGridDBColumn
        Caption = '#'
        DataBinding.FieldName = 'VN_ORDEN'
        Width = 64
      end
      object VN_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'VN_CODIGO'
        Width = 64
      end
      object VN_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'VN_NOMBRE'
        Width = 64
      end
      object VN_PUNTOS: TcxGridDBColumn
        Caption = 'Puntos'
        DataBinding.FieldName = 'VN_PUNTOS'
      end
    end
    object ZetaDBGridLevel: TcxGridLevel
      GridView = ZetaDBGridDBTableView
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  object DataSource: TDataSource
    Left = 256
    Top = 48
  end
end
