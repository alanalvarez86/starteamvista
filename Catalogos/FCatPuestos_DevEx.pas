unit FCatPuestos_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, Grids, DBGrids,
     ZBaseGridLectura_DevEx,
     ZetaDBGrid, StdCtrls, Buttons, ZBaseConsulta, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons;

type
  TCatPuestos_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    PU_CODIGO: TcxGridDBColumn;
    PU_DESCRIP: TcxGridDBColumn;
    PU_CLASIFI: TcxGridDBColumn;
    PU_SUB_CTA: TcxGridDBColumn;
    PU_INGLES: TcxGridDBColumn;
    btnFijas_DevEx: TcxButton;
    btnTools_DevEx: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnFijas_DevExClick(Sender: TObject);
    procedure btnTools_DevExClick(Sender: TObject);
  private
    { Private declarations }
     procedure ConfigAgrupamiento;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar;override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations}
    procedure DoLookup; override;
  end;

var
  CatPuestos_DevEx: TCatPuestos_DevEx;

implementation

uses DCatalogos,
     ZetaBuscador_DevEx,
     ZAccesosMgr,
     ZAccesosTress,
     ZetaCommonClasses;

{$R *.DFM}

procedure TCatPuestos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     //Desactivar gridmode
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= FALSE;
     CanLookup := True;
     HelpContext:= H60611_Puestos;
end;

procedure TCatPuestos_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsPuestos.Conectar;
          DataSource.DataSet:= cdsPuestos;
     end;
end;

procedure TCatPuestos_DevEx.ConfigAgrupamiento;
begin
     //Cambio de configuracion
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //Desactiva la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Esconde la caja de agrupamiento
     //Determinar las columnas por las que se podra agrupar
     PU_CODIGO.Options.Grouping:= FALSE;
     PU_DESCRIP.Options.Grouping:= FALSE;
     PU_SUB_CTA.Options.Grouping:= FALSE;
     PU_INGLES.Options.Grouping:= FALSE;
end;

procedure TCatPuestos_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
      {***Banda Sumatoria***}
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
     inherited;
     btnFijas_DevEx.Enabled := ZAccesosMgr.CheckDerecho( D_CAT_CONT_PUESTOS, K_DERECHO_CAMBIO );
     btnTools_DevEx.Enabled := btnFijas_DevEx.Enabled;
     ConfigAgrupamiento;
end;

procedure TCatPuestos_DevEx.Refresh;
begin
     dmCatalogos.cdsPuestos.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatPuestos_DevEx.Agregar;
begin
     inherited;
     dmCatalogos.cdsPuestos.Agregar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatPuestos_DevEx.Borrar;
begin
     inherited;
     dmCatalogos.cdsPuestos.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatPuestos_DevEx.Modificar;
begin
     inherited;
     dmCatalogos.cdsPuestos.Modificar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatPuestos_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', 'Puestos', 'PU_CODIGO', dmCatalogos.cdsPuestos );
end;

procedure TCatPuestos_DevEx.btnFijas_DevExClick(Sender: TObject);
begin
  dmCatalogos.SetPuestoPercepcionesFijas;
end;

procedure TCatPuestos_DevEx.btnTools_DevExClick(Sender: TObject);
begin
  dmCatalogos.SetPuestoHerramientas;
end;

end.
