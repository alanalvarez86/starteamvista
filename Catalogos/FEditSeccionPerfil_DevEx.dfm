inherited EditSeccionPerfil_DevEx: TEditSeccionPerfil_DevEx
  Left = 406
  Top = 372
  Caption = 'Secci'#243'n de Perfil'
  ClientWidth = 407
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 31
    Top = 59
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo:'
  end
  object Label2: TLabel [1]
    Left = 27
    Top = 81
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nombre:'
  end
  object Label3: TLabel [2]
    Left = 36
    Top = 104
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ingl'#233's:'
  end
  object Label5: TLabel [3]
    Left = 27
    Top = 125
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero:'
  end
  object Label6: TLabel [4]
    Left = 37
    Top = 147
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Texto:'
    FocusControl = DT_TEXTO
  end
  object Label4: TLabel [5]
    Left = 35
    Top = 166
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Orden:'
  end
  object DT_ORDEN: TZetaDBTextBox [6]
    Left = 72
    Top = 165
    Width = 41
    Height = 17
    AutoSize = False
    Caption = 'DT_ORDEN'
    Color = clScrollBar
    ParentColor = False
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'DT_ORDEN'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  inherited PanelBotones: TPanel
    Width = 407
    TabOrder = 5
    inherited OK_DevEx: TcxButton
      Left = 243
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 322
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 407
    TabOrder = 7
    inherited ValorActivo2: TPanel
      Width = 81
      inherited textoValorActivo2: TLabel
        Width = 75
      end
    end
  end
  object DT_TEXTO: TDBEdit [10]
    Left = 72
    Top = 143
    Width = 303
    Height = 21
    DataField = 'DT_TEXTO'
    DataSource = DataSource
    TabOrder = 4
  end
  object DT_NUMERO: TZetaDBNumero [11]
    Left = 72
    Top = 121
    Width = 121
    Height = 21
    Mascara = mnNumeroGlobal
    TabOrder = 3
    Text = '0.00'
    UseEnterKey = True
    DataField = 'DT_NUMERO'
    DataSource = DataSource
  end
  object DT_NOMBRE: TDBEdit [12]
    Left = 72
    Top = 77
    Width = 303
    Height = 21
    DataField = 'DT_NOMBRE'
    DataSource = DataSource
    MaxLength = 40
    TabOrder = 1
  end
  object DT_INGLES: TDBEdit [13]
    Left = 72
    Top = 99
    Width = 303
    Height = 21
    DataField = 'DT_INGLES'
    DataSource = DataSource
    TabOrder = 2
  end
  object DT_CODIGO: TZetaDBEdit [14]
    Left = 72
    Top = 55
    Width = 57
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 6
    TabOrder = 0
    Text = 'DT_CODIGO'
    ConfirmEdit = True
    DataField = 'DT_CODIGO'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 372
    Top = 57
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_BuscarBtn: TdxBarButton
      Visible = ivAlways
    end
    inherited dxBarButton_ExportarBtn: TdxBarButton
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
