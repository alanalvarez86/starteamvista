inherited EditDescPerfil_DevEx: TEditDescPerfil_DevEx
  Left = 330
  Top = 263
  Caption = 'Descripci'#243'n de perfil del puesto'
  ClientHeight = 316
  ClientWidth = 558
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 280
    Width = 558
    inherited OK_DevEx: TcxButton
      Left = 394
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 473
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 558
    inherited ValorActivo2: TPanel
      Width = 232
      inherited textoValorActivo2: TLabel
        Width = 226
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 5
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 50
    Width = 558
    Height = 29
    Align = alTop
    TabOrder = 3
    object Label8: TLabel
      Left = 7
      Top = 6
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'Puesto:'
    end
    object PU_CODIGO: TZetaDBTextBox
      Left = 45
      Top = 4
      Width = 60
      Height = 17
      AutoSize = False
      Caption = 'PU_CODIGO'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'PU_CODIGO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object PU_DESCRIP: TZetaDBTextBox
      Left = 105
      Top = 4
      Width = 350
      Height = 17
      AutoSize = False
      Caption = 'PU_DESCRIP'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'PU_DESCRIP'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
  end
  object PageControl: TcxPageControl [4]
    Left = 0
    Top = 79
    Width = 558
    Height = 201
    Align = alClient
    TabOrder = 8
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 199
    ClientRectLeft = 2
    ClientRectRight = 556
    ClientRectTop = 2
  end
  inherited DataSource: TDataSource
    Left = 516
    Top = 89
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_ExportarBtn: TdxBarButton
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
