inherited CatFestTurno_DevEx: TCatFestTurno_DevEx
  Left = 402
  Top = 295
  Caption = 'D'#237'as Festivos Por Turno'
  ClientHeight = 232
  ClientWidth = 518
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 518
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 192
      inherited textoValorActivo2: TLabel
        Width = 186
      end
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 518
    Height = 40
    Align = alTop
    TabOrder = 2
    object Label1: TLabel
      Left = 24
      Top = 15
      Width = 31
      Height = 13
      Caption = 'Turno:'
    end
    object LookupTurnos: TZetaKeyLookup_DevEx
      Left = 61
      Top = 11
      Width = 300
      Height = 21
      Filtro = 'TU_CODIGO<>'#39'!!!'#39
      LookupDataset = dmCatalogos.cdsTurnos
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      OnValidKey = LookupTurnosValidKey
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 59
    Width = 518
    Height = 173
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object FE_MES: TcxGridDBColumn
        Caption = 'Mes'
        DataBinding.FieldName = 'FE_MES'
      end
      object FE_DIA: TcxGridDBColumn
        Caption = 'D'#237'a'
        DataBinding.FieldName = 'FE_DIA'
        Options.Grouping = False
      end
      object FE_YEAR: TcxGridDBColumn
        Caption = 'A'#241'o'
        DataBinding.FieldName = 'FE_YEAR'
        Options.Grouping = False
      end
      object FE_DESCRIP: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'FE_DESCRIP'
        Options.Grouping = False
      end
      object FE_TIPO: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'FE_TIPO'
        Options.Grouping = False
      end
      object FE_CAMBIO: TcxGridDBColumn
        Caption = 'Cambiar Por'
        DataBinding.FieldName = 'FE_CAMBIO'
        Options.Grouping = False
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
  object dsTurnos: TDataSource
    Left = 398
    Top = 16
  end
end
