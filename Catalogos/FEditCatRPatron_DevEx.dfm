inherited EditCatRPatron_DevEx: TEditCatRPatron_DevEx
  Left = 408
  Top = 187
  Caption = 'Registros Patronales'
  ClientHeight = 548
  ClientWidth = 463
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 512
    Width = 463
    inherited OK_DevEx: TcxButton
      Left = 299
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 378
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 463
    inherited ValorActivo2: TPanel
      Width = 137
      inherited textoValorActivo2: TLabel
        Width = 131
      end
    end
  end
  inherited Panel1: TPanel
    Width = 463
    Height = 52
    object Label1: TLabel
      Left = 90
      Top = 9
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label2: TLabel
      Left = 67
      Top = 31
      Width = 59
      Height = 13
      Caption = 'Descripci'#243'n:'
    end
    object TB_CODIGO: TZetaDBEdit
      Left = 130
      Top = 5
      Width = 52
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
      ConfirmEdit = True
      DataField = 'TB_CODIGO'
      DataSource = DataSource
    end
    object TB_ELEMENT: TDBEdit
      Left = 130
      Top = 27
      Width = 232
      Height = 21
      DataField = 'TB_ELEMENT'
      DataSource = DataSource
      TabOrder = 1
    end
    object TB_ACTIVO: TDBCheckBox
      Left = 311
      Top = 5
      Width = 51
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Activo:'
      DataField = 'TB_ACTIVO'
      DataSource = DataSource
      TabOrder = 2
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
  end
  inherited PageControl: TcxPageControl
    Top = 102
    Width = 463
    Height = 410
    ClientRectBottom = 408
    ClientRectRight = 461
    inherited Datos: TcxTabSheet
      object Label3: TLabel
        Left = 91
        Top = 14
        Width = 31
        Height = 13
        Caption = 'Ingl'#233's:'
      end
      object Label5: TLabel
        Left = 82
        Top = 36
        Width = 40
        Height = 13
        Caption = 'N'#250'mero:'
      end
      object Label6: TLabel
        Left = 92
        Top = 58
        Width = 30
        Height = 13
        Caption = 'Texto:'
      end
      object Label4: TLabel
        Left = 38
        Top = 80
        Width = 84
        Height = 13
        Alignment = taRightJustify
        Caption = 'Registro Patronal:'
      end
      object Label7: TLabel
        Left = 84
        Top = 102
        Width = 38
        Height = 13
        Alignment = taRightJustify
        Caption = 'M'#243'dulo:'
      end
      object lbRazonsocial: TLabel
        Left = 56
        Top = 125
        Width = 66
        Height = 13
        Caption = 'Raz'#243'n Social:'
      end
      object Label8: TLabel
        Left = 96
        Top = 148
        Width = 26
        Height = 13
        Alignment = taRightJustify
        Caption = 'Calle:'
      end
      object Label9: TLabel
        Left = 74
        Top = 171
        Width = 48
        Height = 13
        Alignment = taRightJustify
        Caption = '# Exterior:'
      end
      object Label10: TLabel
        Left = 204
        Top = 171
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = '# Interior:'
      end
      object Label11: TLabel
        Left = 84
        Top = 194
        Width = 38
        Height = 13
        Alignment = taRightJustify
        Caption = 'Colonia:'
      end
      object Label12: TLabel
        Left = 86
        Top = 217
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ciudad:'
      end
      object Label13: TLabel
        Left = 86
        Top = 240
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Estado:'
      end
      object Label14: TLabel
        Left = 54
        Top = 264
        Width = 68
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#243'digo Postal:'
      end
      object Label15: TLabel
        Left = 77
        Top = 286
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tel'#233'fono:'
      end
      object Label16: TLabel
        Left = 99
        Top = 309
        Width = 23
        Height = 13
        Alignment = taRightJustify
        Caption = 'FAX:'
      end
      object Label17: TLabel
        Left = 33
        Top = 332
        Width = 89
        Height = 13
        Alignment = taRightJustify
        Caption = 'Correo electr'#243'nico:'
      end
      object Label18: TLabel
        Left = 33
        Top = 355
        Width = 89
        Height = 13
        Alignment = taRightJustify
        Caption = 'P'#225'gina de internet:'
      end
      object TB_INGLES: TDBEdit
        Left = 126
        Top = 10
        Width = 232
        Height = 21
        DataField = 'TB_INGLES'
        DataSource = DataSource
        TabOrder = 0
      end
      object TB_TEXTO: TDBEdit
        Left = 126
        Top = 54
        Width = 232
        Height = 21
        DataField = 'TB_TEXTO'
        DataSource = DataSource
        TabOrder = 2
      end
      object TB_NUMREG: TDBEdit
        Left = 126
        Top = 76
        Width = 100
        Height = 21
        DataField = 'TB_NUMREG'
        DataSource = DataSource
        TabOrder = 3
      end
      object TB_MODULO: TZetaDBKeyCombo
        Left = 126
        Top = 98
        Width = 230
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 4
        ListaFija = lfRPatronModulo
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'TB_MODULO'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object TB_NUMERO: TZetaDBNumero
        Left = 126
        Top = 32
        Width = 100
        Height = 21
        Mascara = mnNumeroGlobal
        TabOrder = 1
        Text = '0.00'
        DataField = 'TB_NUMERO'
        DataSource = DataSource
      end
      object RS_CODIGO: TZetaDBKeyLookup_DevEx
        Left = 126
        Top = 121
        Width = 254
        Height = 21
        LookupDataset = dmCatalogos.cdsRSocial
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 60
        DataField = 'RS_CODIGO'
        DataSource = DataSource
      end
      object TB_CALLE: TDBEdit
        Left = 126
        Top = 144
        Width = 274
        Height = 21
        DataField = 'TB_CALLE'
        DataSource = DataSource
        TabOrder = 6
      end
      object TB_NUMEXT: TDBEdit
        Left = 126
        Top = 167
        Width = 70
        Height = 21
        DataField = 'TB_NUMEXT'
        DataSource = DataSource
        TabOrder = 7
      end
      object TB_NUMINT: TDBEdit
        Left = 253
        Top = 167
        Width = 70
        Height = 21
        DataField = 'TB_NUMINT'
        DataSource = DataSource
        TabOrder = 8
      end
      object TB_COLONIA: TDBEdit
        Left = 126
        Top = 190
        Width = 274
        Height = 21
        DataField = 'TB_COLONIA'
        DataSource = DataSource
        TabOrder = 9
      end
      object TB_CIUDAD: TDBEdit
        Left = 126
        Top = 213
        Width = 274
        Height = 21
        DataField = 'TB_CIUDAD'
        DataSource = DataSource
        TabOrder = 10
      end
      object TB_ENTIDAD: TZetaDBKeyLookup_DevEx
        Left = 126
        Top = 236
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsEstado
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 11
        TabStop = True
        WidthLlave = 60
        DataField = 'TB_ENTIDAD'
        DataSource = DataSource
      end
      object TB_CODPOST: TDBEdit
        Left = 126
        Top = 259
        Width = 188
        Height = 21
        DataField = 'TB_CODPOST'
        DataSource = DataSource
        TabOrder = 12
      end
      object TB_TEL: TDBEdit
        Left = 126
        Top = 282
        Width = 188
        Height = 21
        DataField = 'TB_TEL'
        DataSource = DataSource
        TabOrder = 13
      end
      object TB_EMAIL: TDBEdit
        Left = 126
        Top = 328
        Width = 274
        Height = 21
        DataField = 'TB_EMAIL'
        DataSource = DataSource
        TabOrder = 15
      end
      object TB_FAX: TDBEdit
        Left = 126
        Top = 305
        Width = 188
        Height = 21
        DataField = 'TB_FAX'
        DataSource = DataSource
        TabOrder = 14
      end
      object TB_WEB: TDBEdit
        Left = 126
        Top = 351
        Width = 274
        Height = 21
        DataField = 'TB_WEB'
        DataSource = DataSource
        TabOrder = 16
      end
    end
    inherited Tabla: TcxTabSheet
      Caption = 'Prima de Riesgo'
      inherited GridRenglones: TZetaDBGrid
        Top = 70
        Width = 459
        Height = 310
        Columns = <
          item
            Expanded = False
            FieldName = 'RT_FECHA'
            Title.Caption = 'Fecha'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RT_PRIMA'
            Title.Caption = 'Prima'
            Width = 110
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RT_S'
            Title.Caption = 'S'
            Width = 52
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RT_I'
            Title.Caption = 'I'
            Width = 52
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RT_D'
            Title.Caption = 'D'
            Width = 52
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RT_N'
            Title.Caption = 'N'
            Width = 52
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RT_M'
            Title.Caption = 'M'
            Width = 52
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RT_F'
            Title.Caption = 'F'
            Width = 52
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RT_PTANT'
            Title.Caption = 'Prima Ant.'
            Visible = True
          end>
      end
      inherited Panel2: TPanel
        Width = 459
        Height = 70
        object lblClase: TLabel [0]
          Left = 64
          Top = 14
          Width = 29
          Height = 13
          Caption = 'Clase:'
        end
        object lblFraccion: TLabel [1]
          Left = 232
          Top = 14
          Width = 44
          Height = 13
          Caption = 'Fracci'#243'n:'
        end
        inherited BBAgregar_DevEx: TcxButton
          Left = 27
          Top = 38
          Hint = 'Agregar Prima'
          Caption = ' Agregar Prima'
          TabOrder = 2
        end
        inherited BBBorrar_DevEx: TcxButton
          Left = 167
          Top = 38
          Hint = 'Borrar Prima'
          Caption = '  Borrar Prima'
          TabOrder = 3
        end
        inherited BBModificar_DevEx: TcxButton
          Left = 307
          Top = 38
          Hint = 'Modificar Prima'
          Caption = ' Modificar Prima'
          TabOrder = 4
        end
        object TB_CLASE: TZetaDBKeyCombo
          Left = 96
          Top = 10
          Width = 121
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 0
          ListaFija = lfClaseRiesgo
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
          DataField = 'TB_CLASE'
          DataSource = DataSource
          LlaveNumerica = True
        end
        object TB_FRACC: TZetaDBNumero
          Left = 278
          Top = 10
          Width = 89
          Height = 21
          Mascara = mnDias
          TabOrder = 1
          Text = '0'
          DataField = 'TB_FRACC'
          DataSource = DataSource
        end
      end
    end
    object STPS: TcxTabSheet
      Caption = 'STPS'
      ImageIndex = 2
      object GBPrimaRiesgo: TcxGroupBox
        Left = 8
        Top = 8
        Caption = 'C'#225'lculo de Prima de Riesgo:'
        TabOrder = 0
        Height = 57
        Width = 401
        object TB_STYPS: TDBCheckBox
          Left = 45
          Top = 24
          Width = 125
          Height = 17
          Alignment = taLeftJustify
          Caption = #191'Acreditaci'#243'n STPS?:'
          DataField = 'TB_STYPS'
          DataSource = DataSource
          TabOrder = 0
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
      end
      object GroupBox1: TcxGroupBox
        Left = 8
        Top = 80
        Caption = ' Forma DC4 '
        TabOrder = 1
        Height = 113
        Width = 401
        object Label19: TLabel
          Left = 125
          Top = 21
          Width = 29
          Height = 13
          Caption = 'SIEM:'
        end
        object lblFolioPlan: TLabel
          Left = 30
          Top = 80
          Width = 125
          Height = 13
          Caption = 'Folio de Plan y Programas:'
        end
        object TB_SIEM: TDBEdit
          Left = 158
          Top = 18
          Width = 232
          Height = 21
          DataField = 'TB_SIEM'
          DataSource = DataSource
          TabOrder = 0
        end
        object TB_PLANPRE: TDBCheckBox
          Left = 24
          Top = 40
          Width = 147
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Present'#243' Plan y Programas:'
          DataField = 'TB_PLANPRE'
          DataSource = DataSource
          TabOrder = 1
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          OnClick = TB_PLANPERClick
        end
        object TB_PLANPER: TDBCheckBox
          Left = 6
          Top = 59
          Width = 165
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Pertenece al Plan y Programas:'
          DataField = 'TB_PLANPER'
          DataSource = DataSource
          TabOrder = 2
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          OnClick = TB_PLANPERClick
        end
        object TB_PLANFOL: TDBEdit
          Left = 158
          Top = 78
          Width = 232
          Height = 21
          DataField = 'TB_PLANFOL'
          DataSource = DataSource
          TabOrder = 3
        end
      end
    end
    object tsConfidencialidad: TcxTabSheet
      Caption = 'Confidencialidad'
      ImageIndex = 3
      object gbConfidencialidad: TcxGroupBox
        Left = 8
        Top = 8
        TabOrder = 0
        Height = 233
        Width = 417
        object btSeleccionarConfiden: TcxButton
          Left = 380
          Top = 48
          Width = 25
          Height = 25
          Hint = 'Seleccionar Confidencialidad'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = btSeleccionarConfidenClick
          OptionsImage.Glyph.Data = {
            7A080000424D7A08000000000000360000002800000017000000170000000100
            20000000000044080000C40E0000C40E0000000000000000000000B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF97E0F6FF97E0F6FF2CBFEDFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF9FE2
            F7FFFFFFFFFFFFFFFFFFB7E9F9FF44C7EFFF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF9FE2F7FFFFFF
            FFFFFBFEFFFF9FE2F7FF18B9EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF9FE2F7FFAFE7F8FF24BD
            ECFF00B2E9FF0CB6EAFF68D1F2FF04B3E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF10B7EAFF00B2E9FF14B8EBFF4CC9
            F0FFFFFFFFFFFFFFFFFF64D0F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF40C5EFFFDFF5FCFFFFFFFFFFFFFF
            FFFFFFFFFFFFEBF9FDFF10B7EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FFA3E3F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFF8BDCF5FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF1CBAEBFFF7FDFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFBFEFFFF28BEECFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF80D9F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFB7E9F9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF08B4EAFFE3F7FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7E9
            F9FF08B4EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF5CCEF1FFFFFFFFFFFFFFFFFFD7F3FCFF48C8EFFF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FFB7E9F9FF6CD3F2FF04B3E9FF00B2E9FF1CBAEBFFA3E3F7FF8BDC
            F5FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF08B4EAFF80D9F4FFF7FDFEFFFFFFFFFFA3E3F7FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF40C5EFFFE3F7FDFFFFFFFFFFF7FDFEFF7CD7F4FF08B4EAFF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF18B9
            EBFFBFECF9FFA7E4F7FF1CBAEBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
        end
        object listaConfidencialidad: TcxListBox
          Left = 94
          Top = 48
          Width = 285
          Height = 169
          TabStop = False
          ExtendedSelect = False
          ItemHeight = 13
          TabOrder = 0
        end
        object rbConfidenTodas: TcxRadioButton
          Left = 94
          Top = 10
          Width = 113
          Height = 17
          Caption = 'Todas '
          TabOrder = 1
          OnClick = rbConfidenTodasClick
          Transparent = True
        end
        object rbConfidenAlgunas: TcxRadioButton
          Left = 94
          Top = 28
          Width = 156
          Height = 17
          Caption = 'Aplica algunas'
          TabOrder = 2
          OnClick = rbConfidenAlgunasClick
          Transparent = True
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 420
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 2621710
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    Left = 240
    Top = 40
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    DesignInfo = 2621648
  end
  inherited dsRenglon: TDataSource
    Left = 423
  end
end
