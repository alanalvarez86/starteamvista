inherited CatRSocial_DevEx: TCatRSocial_DevEx
  Left = 226
  Top = 239
  Caption = 'Razones Sociales'
  ClientHeight = 258
  ClientWidth = 520
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 520
    inherited ValorActivo2: TPanel
      Width = 261
      inherited textoValorActivo2: TLabel
        Width = 255
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 520
    Height = 239
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object RS_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'RS_CODIGO'
      end
      object RS_NOMBRE: TcxGridDBColumn
        Caption = 'Raz'#243'n Social'
        DataBinding.FieldName = 'RS_NOMBRE'
      end
      object RS_RFC: TcxGridDBColumn
        Caption = 'R.F.C'
        DataBinding.FieldName = 'RS_RFC'
      end
      object RS_RLEGAL: TcxGridDBColumn
        Caption = 'Representante legal'
        DataBinding.FieldName = 'RS_RLEGAL'
      end
      object RS_TEL: TcxGridDBColumn
        Caption = 'Tel'#233'fono'
        DataBinding.FieldName = 'RS_TEL'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
