unit FEditCosteoGrupos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ExtCtrls, DBCtrls, Buttons,
  StdCtrls, ZetaEdit, Mask, ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons;

type
  TEditCosteoGrupos_DevEx = class(TBaseEdicion_DevEx)
    GpoCostoNombre: TDBEdit;
    Label2: TLabel;
    Label1: TLabel;
    GpoCostoCodigo: TZetaDBEdit;
    TB_ACTIVO: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect; override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  EditCosteoGrupos_DevEx: TEditCosteoGrupos_DevEx;

implementation

{$R *.dfm}

uses
     DCatalogos,
     ZetaCommonClasses,
     ZetaBuscador_DevEx,
     ZAccesosTress;

procedure TEditCosteoGrupos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := GpoCostoCodigo;
     HelpContext:= H00002_Edit_Costeo_Grupos;
     IndexDerechos := D_COSTEO_GRUPOS;
end;

procedure TEditCosteoGrupos_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsCosteoGrupos.Conectar;
          DataSource.DataSet:= cdsCosteoGrupos;
     end;
end;

procedure TEditCosteoGrupos_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', 'Grupo de Costeo', 'GpoCostoCodigo', dmCatalogos.cdsCosteoGrupos );
end;

end.
