inherited CatRPatron_DevEx: TCatRPatron_DevEx
  Left = 200
  Top = 117
  Caption = 'Registros Patronales'
  ClientWidth = 484
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 484
    inherited ValorActivo2: TPanel
      Width = 225
      inherited textoValorActivo2: TLabel
        Width = 219
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 484
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object TB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'TB_CODIGO'
      end
      object TB_ELEMENT: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'TB_ELEMENT'
      end
      object TB_NUMREG: TcxGridDBColumn
        Caption = 'N'#250'mero de Registro'
        DataBinding.FieldName = 'TB_NUMREG'
      end
      object TB_MODULO: TcxGridDBColumn
        Caption = 'M'#243'dulo'
        DataBinding.FieldName = 'TB_MODULO'
      end
      object RS_NOMBRE: TcxGridDBColumn
        Caption = 'Raz'#243'n Social'
        DataBinding.FieldName = 'RS_NOMBRE'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
