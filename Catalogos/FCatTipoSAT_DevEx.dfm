inherited CatTipoSAT_DevEx: TCatTipoSAT_DevEx
  Left = 226
  Top = 239
  Caption = 'Tipos de Conceptos'
  ClientHeight = 258
  ClientWidth = 520
  ExplicitWidth = 520
  ExplicitHeight = 258
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 520
    ExplicitWidth = 520
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 261
      ExplicitWidth = 261
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 255
        ExplicitLeft = 175
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 520
    Height = 239
    ExplicitWidth = 520
    ExplicitHeight = 239
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OnCellClick = ZetaDBGridDBTableViewCellClick
      object TB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'TB_CODIGO'
      end
      object RS_NOMBRE: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'TB_ELEMENT'
        Width = 193
      end
      object TB_SAT_CLA: TcxGridDBColumn
        Caption = 'Clase'
        DataBinding.FieldName = 'TB_SAT_CLA'
        Width = 98
      end
      object TB_SAT_NUM: TcxGridDBColumn
        Caption = 'N'#250'mero en SAT'
        DataBinding.FieldName = 'TB_SAT_NUM'
        Width = 109
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 112
    Top = 184
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
    DesignInfo = 12058664
  end
  inherited ActionList: TActionList
    Left = 34
    Top = 136
  end
  inherited PopupMenu1: TPopupMenu
    Left = 32
    Top = 88
  end
end
