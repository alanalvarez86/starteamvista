unit FEditMatrizPuestoCertifica_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, Db, ExtCtrls,
  Buttons, DBCtrls, StdCtrls, Mask, ZetaNumero, 
  ZetaDBTextBox, FCatCerNivel_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx;

type
  TEditMatrizPuestoCertificacion_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    ZCertificacion: TZetaTextBox;
    Label2: TLabel;
    PU_CODIGO: TZetaDBKeyLookup_DevEx;
    EN_DIAS: TZetaDBNumero;
    Label4: TLabel;
    Label3: TLabel;
    PC_OPCIONA: TDBCheckBox;
    gbProgramacion: TGroupBox;
    gbProgramarA: TDBRadioGroup;
    bModifica_DevEx: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure gbProgramarAChange(Sender: TObject);
    procedure bModifica_DevExClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure SetEditarSoloActivos;
  private
  protected
    procedure Connect; override;
  public
  end;

var
  EditMatrizPuestoCertificacion_DevEx: TEditMatrizPuestoCertificacion_DevEx;

implementation

uses FTressShell, ZetaCommonClasses, dCatalogos, dGlobal, ZGlobalTress,
     ZBaseDlgModal_DevEx, ZAccesosTress, ZetaClientTools;

{$R *.DFM}

procedure TEditMatrizPuestoCertificacion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_CAPA_MATRIZ_PUESTO_CERT;
     HelpContext:= H_Matriz_Edt_PuestoCertifica;
     FirstControl := PU_CODIGO;
     PU_CODIGO.LookupDataset := dmCatalogos.cdsPuestos;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TEditMatrizPuestoCertificacion_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     ZCertificacion.Caption := Datasource.DataSet.FieldByName( 'CI_CODIGO' ).AsString + ' = ' + dmCatalogos.cdsCertificaciones.FieldByName( 'CI_NOMBRE' ).AsString;
     gbProgramacion.Visible := Global.GetGlobalInteger( K_NIVEL_PROGRAMACION_CERTIFIC ) > 0;
     if gbProgramacion.Visible then
     begin
          gbProgramacion.Caption := 'Programación por ' + Global.GetDescNivelProg(K_NIVEL_PROGRAMACION_CERTIFIC);
          Height := GetScaledHeight( 325 ); //+50 Se aumentó la altura debido a los Large Fonts;
     end
     else
         Height := GetScaledHeight( 218 ); //+50 Se aumentó la altura debido a los Large Fonts0; //DevEx (by am): Se aumenta width por nueva imagen
end;

procedure TEditMatrizPuestoCertificacion_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsPuestos.Conectar;
          cdsMatrizCertificPuesto.Conectar;
          Datasource.Dataset := cdsMatrizCertificPuesto;
     end;
end;

procedure TEditMatrizPuestoCertificacion_DevEx.gbProgramarAChange(Sender: TObject);
begin
     inherited;
     bModifica_DevEx.Enabled := ( gbProgramarA.Value = 'S' );
end;

procedure TEditMatrizPuestoCertificacion_DevEx.bModifica_DevExClick(
  Sender: TObject);
begin
  inherited;
     inherited;
     ShowDlgModal( CatCerNivel_DevEx, TCatCerNivel_DevEx );

end;

procedure TEditMatrizPuestoCertificacion_DevEx.OK_DevExClick(
  Sender: TObject);
begin
  if ( gbProgramarA.Value = 'N' ) then
        with dmCatalogos.cdsCerNivel do
        begin
             First;
             while not EOF do
                   Delete;
        end;
     inherited;

end;

procedure TEditMatrizPuestoCertificacion_DevEx.SetEditarSoloActivos;
begin
     PU_CODIGO.EditarSoloActivos := TRUE;
end;


end.



