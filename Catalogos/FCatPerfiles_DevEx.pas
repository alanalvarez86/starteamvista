unit FCatPerfiles_Devex;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Grids, DBGrids, Db, ExtCtrls,
     ZBaseConsulta,ZBaseGridLectura_DevEx, ZetaDBGrid,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, ZetaKeyLookup_DevEx, Menus, ActnList,
  ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid,
  StdCtrls, System.Actions;

type
  TCatPerfiles_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    lookTPerfiles: TZetaKeyLookup_DevEx;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure lookTPerfilesValidKey(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure AplicaFiltro;
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
  public
    { Public declarations}
    procedure DoLookup; override;
  end;

var
  CatPerfiles_DevEx: TCatPerfiles_DevEx;

implementation

uses DCatalogos,
     ZetaBuscador_DevEx,ZAccesosTress,ZAccesosMgr,
     ZetaCommonClasses,ZetaCommonTools, dTablas;

{$R *.DFM}

{ TCatCompetencias }

procedure TCatPerfiles_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H_Cat_GpoCompetencias ;
     IndexDerechos := D_CAT_CPERFILES;
end;

procedure TCatPerfiles_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsCatPerfiles.Conectar;
          DataSource.DataSet:= cdsCatPerfiles;
     end;
     with dmTablas do
     begin
          cdsTPerfiles.Conectar;
          lookTPerfiles.LookupDataset := cdsTPerfiles;
     end;
     AplicaFiltro
end;

procedure TCatPerfiles_DevEx.Refresh;
begin
     dmCatalogos.cdsCatPerfiles.Refrescar;
end;

procedure TCatPerfiles_DevEx.Agregar;
begin
     dmCatalogos.cdsCatPerfiles.Agregar;
end;

procedure TCatPerfiles_DevEx.Borrar;
begin
     dmCatalogos.cdsCatPerfiles.Borrar;
end;

procedure TCatPerfiles_DevEx.Modificar;
begin
     dmCatalogos.cdsCatPerfiles.Modificar;
end;

procedure TCatPerfiles_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Grupos de Competencias', 'Grupos de Competencias', 'CP_CODIGO', dmCatalogos.cdsCatPerfiles );
end;

procedure TCatPerfiles_DevEx.lookTPerfilesValidKey(Sender: TObject);
begin
     inherited;
     AplicaFiltro;
end;


Procedure TCatPerfiles_DevEx.AplicaFiltro;
begin
     if StrLleno(lookTPerfiles.Llave) then
     begin
          with dmCatalogos.cdsCatPerfiles do
          begin
               Filtered := False;
               Filter := Format(' TB_CODIGO = ''%s'' ',[dmTablas.cdsTPerfiles.FieldByName('TB_CODIGO').AsString ]);
               Filtered := True;
          end;
     end
     else
         dmCatalogos.cdsCatPerfiles.Filtered := False;
end;


function TCatPerfiles_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := ZAccesosMgr.CheckDerecho( D_CAT_CPERFILES , K_DERECHO_CONSULTA );
end;

procedure TCatPerfiles_DevEx.FormShow(Sender: TObject);
begin
  CreaColumaSumatoria(zetadbGriddbtableview.Columns[0],0,'',SkCount);
  inherited;
 ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
  //Esconde la caja de agrupamiento
  ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
end;

end.
