inherited CatInvitadores_DevEx: TCatInvitadores_DevEx
  Left = 915
  Top = 762
  Caption = 'Invitadores'
  ClientWidth = 671
  ExplicitWidth = 671
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 671
    ExplicitWidth = 671
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 412
      ExplicitWidth = 412
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 406
        ExplicitLeft = 3
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 671
    ExplicitWidth = 671
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object IV_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'IV_CODIGO'
      end
      object IV_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'IV_NOMBRE'
      end
      object CB_CODIGO: TcxGridDBColumn
        Caption = '# Empleado'
        DataBinding.FieldName = 'CB_CODIGO'
        Width = 84
      end
      object IV_ACTIVO: TcxGridDBColumn
        Caption = 'Activo'
        DataBinding.FieldName = 'IV_ACTIVO'
        Width = 67
      end
      object IV_FEC_INI: TcxGridDBColumn
        Caption = 'Inicio'
        DataBinding.FieldName = 'IV_FEC_INI'
      end
      object IV_FEC_FIN: TcxGridDBColumn
        Caption = 'Final'
        DataBinding.FieldName = 'IV_FEC_FIN'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
