unit FCatRPatron_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls, StdCtrls,
  ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid;

type
  TCatRPatron_DevEx = class(TBaseGridLectura_DevEx)
    TB_CODIGO: TcxGridDBColumn;
    TB_ELEMENT: TcxGridDBColumn;
    TB_NUMREG: TcxGridDBColumn;
    TB_MODULO: TcxGridDBColumn;
    RS_NOMBRE: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
  protected
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
  end;

var
  CatRPatron_DevEx: TCatRPatron_DevEx;

implementation

uses dCatalogos,ZetaCommonClasses;

{$R *.DFM}

{ TCatRPatron }
{ NOTA : DADO EL TAMA�O REDUCIDO DE ESTE CATALOGO NO SE IMPLEMENTARA BUSQUEDA DE CODIGOS }

procedure TCatRPatron_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H60631_Registros_patronales;
end;

procedure TCatRPatron_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsRPatron.Conectar;
          DataSource.DataSet:= cdsRPatron;
     end;
end;

procedure TCatRPatron_DevEx.Refresh;
begin
     dmCatalogos.cdsRPatron.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatRPatron_DevEx.Agregar;
begin
     dmCatalogos.cdsRPatron.Agregar;
end;

procedure TCatRPatron_DevEx.Borrar;
begin
     dmCatalogos.cdsRPatron.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatRPatron_DevEx.Modificar;
begin
     dmCatalogos.cdsRPatron.Modificar;
end;

procedure TCatRPatron_DevEx.FormShow(Sender: TObject);
begin
 ApplyMinWidth;
  inherited;

   //columna sumatoria: cuantos
   CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
   ZetaDBGridDBTableView.ApplyBestFit();

end;

end.
