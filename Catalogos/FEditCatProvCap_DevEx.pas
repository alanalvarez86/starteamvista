unit FEditCatProvCap_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicion_DevEx, DB, ExtCtrls, DBCtrls, ZetaSmartLists, Buttons,
  StdCtrls, Mask, ZetaEdit, ZetaNumero,  
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx;

type
  TEditCatProvCap_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    lbCodigo: TLabel;
    PC_CODIGO: TZetaDBEdit;
    lbNombre: TLabel;
    PC_NOMBRE: TDBEdit;
    Label3: TLabel;
    PC_DIRECCI: TDBEdit;
    lbCiudad: TLabel;
    PC_CIUDAD: TDBEdit;
    Label1: TLabel;
    PC_ESTADO: TZetaDBKeyLookup_DevEx;
    Label2: TLabel;
    PC_PAIS: TDBEdit;
    Label4: TLabel;
    PC_CODPOST: TDBEdit;
    Label5: TLabel;
    PC_RFC: TDBEdit;
    Label6: TLabel;
    PC_CONTACT: TDBEdit;
    Label7: TLabel;
    PC_TEL: TDBEdit;
    Label8: TLabel;
    PC_FAX: TDBEdit;
    Label9: TLabel;
    PC_EMAIL: TDBEdit;
    Label10: TLabel;
    PC_WEB: TDBEdit;
    Label11: TLabel;
    PC_NUMERO: TZetaDBNumero;
    Label12: TLabel;
    PC_TEXTO: TDBEdit;
    PC_ACTIVO: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
     procedure Connect; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  EditCatProvCap_DevEx: TEditCatProvCap_DevEx;

implementation

uses ZAccesosTress,
     ZetaCommonClasses,
     ZetaBuscador_DevEx,
     dCatalogos;

{$R *.dfm}

procedure TEditCatProvCap_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := PC_CODIGO;
     IndexDerechos := D_CAT_CAPA_PROVEEDORES;
     HelpContext:= H_PROV_CAPACITACION;
end;

procedure TEditCatProvCap_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          DataSource.DataSet := cdsProvCap;
          Self.Caption := cdsProvCap.LookupName;
     end;
end;

procedure TEditCatProvCap_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Proveedor:', Self.Caption, 'PC_CODIGO', dmCatalogos.cdsProvCap );
end;

end.
