inherited CatValuaciones_DevEx: TCatValuaciones_DevEx
  Left = 547
  Top = 130
  Caption = 'Valuaciones de Puesto'
  ClientHeight = 488
  ClientWidth = 613
  OnDestroy = FormDestroy
  ExplicitWidth = 613
  ExplicitHeight = 488
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 613
    ExplicitWidth = 613
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 354
      ExplicitWidth = 354
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 348
        ExplicitLeft = 3
      end
    end
  end
  object PanelFiltros: TPanel [1]
    Left = 0
    Top = 19
    Width = 613
    Height = 134
    Align = alTop
    TabOrder = 2
    object Label1: TLabel
      Left = 51
      Top = 12
      Width = 39
      Height = 13
      Caption = 'Plantilla:'
    end
    object Plantilla: TZetaKeyLookup_DevEx
      Left = 96
      Top = 8
      Width = 417
      Height = 21
      LookupDataset = dmCatalogos.cdsValPlantilla
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      OnValidKey = PlantillaValidKey
    end
    object GroupBox1: TcxGroupBox
      Left = 16
      Top = 29
      Caption = 'Filtros:'
      TabOrder = 1
      Height = 92
      Width = 545
      object Label4: TLabel
        Left = 33
        Top = 54
        Width = 40
        Height = 13
        Caption = 'Historial:'
      end
      object Label3: TLabel
        Left = 232
        Top = 76
        Width = 33
        Height = 13
        Caption = 'Status:'
        Enabled = False
        Visible = False
      end
      object Label5: TLabel
        Left = 233
        Top = 102
        Width = 32
        Height = 13
        Caption = 'Grado:'
        Enabled = False
        Visible = False
      end
      object Label2: TLabel
        Left = 29
        Top = 90
        Width = 36
        Height = 13
        Caption = 'Puesto:'
        Enabled = False
        Visible = False
      end
      object lbInicial: TLabel
        Left = 40
        Top = 24
        Width = 30
        Height = 13
        Caption = 'Inicial:'
      end
      object lbFinal: TLabel
        Left = 215
        Top = 24
        Width = 25
        Height = 13
        Caption = 'Final:'
      end
      object FiltrarFecha: TCheckBox
        Left = 240
        Top = 54
        Width = 161
        Height = 17
        Caption = 'Filtrar por fecha de valuaci'#243'n'
        Checked = True
        Enabled = False
        State = cbChecked
        TabOrder = 1
        Visible = False
        OnClick = FiltrarFechaClick
      end
      object VP_GRADO: TZetaNumero
        Left = 272
        Top = 98
        Width = 129
        Height = 21
        Enabled = False
        Mascara = mnDias
        TabOrder = 4
        Text = '0'
        Visible = False
      end
      object cbHistorial: TComboBox
        Left = 80
        Top = 51
        Width = 129
        Height = 21
        TabOrder = 3
        OnChange = BtnFiltrarClick
      end
      object VP_STATUS: TZetaKeyCombo
        Left = 272
        Top = 72
        Width = 129
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        Enabled = False
        ParentCtl3D = False
        TabOrder = 2
        Visible = False
        ListaFija = lfStatusValuacion
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
      object BtnCopiar: TcxButton
        Left = 385
        Top = 19
        Width = 120
        Height = 25
        Hint = 'Copiar Valuaci'#243'n'
        Caption = 'Copiar Valuaci'#243'n'
        OptionsImage.Glyph.Data = {
          7A080000424D7A08000000000000360000002800000017000000170000000100
          2000000000004408000000000000000000000000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFEFDCCBFFFBF6F1FFFAF4EFFFFAF4EFFFFAF4
          EFFFFAF4EFFFFAF4EFFFFBF7F3FFE2BF9FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFFAF4EFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFE7CAAFFFECD5BFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AB81FFEDD8C5FFF5EA
          DFFFF5EADFFFFEFCFBFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFE7CAAFFFECD5BFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE4C5A7FFF7EEE5FFE7CAAFFFE7CA
          AFFFFCF8F5FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE7CA
          AFFFECD5BFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFE7CAAFFFF0DFCFFFD8AA7FFFD8AA7FFFFAF4
          EFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE7CAAFFFECD5
          BFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFE7CAAFFFF0DFCFFFD8AA7FFFD8AA7FFFFAF4EFFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE7CAAFFFECD5BFFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFE7CAAFFFF0DFCFFFD8AA7FFFD8AA7FFFF8F0E9FFECD5BFFFECD5
          BFFFE5C6A9FFD8AA7FFFD8AA7FFFD8AA7FFFE7CAAFFFECD5BFFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFE7CAAFFFF0DFCFFFD8AA7FFFD8AA7FFFDFB995FFFDFBF9FFFFFFFFFFFEFE
          FDFFD8AA7FFFD8AA7FFFD8AA7FFFE7CAAFFFECD5BFFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE7CA
          AFFFF0DFCFFFD8AA7FFFD8AA7FFFD8AA7FFFE1BE9DFFFDFBF9FFFFFFFFFFD8AA
          7FFFD8AA7FFFD8AA7FFFE7CAAFFFECD5BFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE7CAAFFFF0DF
          CFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE1BE9DFFFDFBF9FFECD5BFFFECD5
          BFFFECD5BFFFF3E6D9FFE8CDB3FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE1BE9DFFFDFAF7FFFAF4
          EFFFFAF4EFFFF4E7DBFFDCB28BFFD8AA7FFFDFBA97FFE7CAAFFFEDD7C3FFF9F2
          EBFFE4C3A5FFD9AB81FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE5C6A9FFFEFEFDFFFFFF
          FFFFFFFFFFFFE7CAAFFFD8AA7FFFD8AA7FFFD8AA7FFFE2BF9FFFF5EADFFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE5C6A9FFFEFEFDFFFFFF
          FFFFE7CAAFFFD8AA7FFFD8AA7FFFD8AA7FFFE2BF9FFFF5EADFFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE9CEB5FFFFFFFFFFEAD1
          B9FFDDB58FFFDDB58FFFDDB58FFFE7CBB1FFF5E8DDFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFEAD1B9FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFBF7F3FFE5C7ABFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
        OptionsImage.Margin = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = BtnCopiarClick
      end
      object Puesto: TZetaKeyLookup_DevEx
        Left = 72
        Top = 80
        Width = 417
        Height = 21
        Enabled = False
        LookupDataset = dmCatalogos.cdsPuestos
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        Visible = False
        WidthLlave = 60
      end
      object FechaInicial: TZetaFecha
        Left = 80
        Top = 19
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 6
        Text = '15/May/07'
        Valor = 39217.000000000000000000
        OnChange = BtnFiltrarClick
      end
      object FechaFinal: TZetaFecha
        Left = 250
        Top = 19
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 7
        Text = '15/May/07'
        Valor = 39217.000000000000000000
        OnChange = BtnFiltrarClick
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 153
    Width = 613
    Height = 335
    ExplicitTop = 153
    ExplicitWidth = 613
    ExplicitHeight = 335
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      object VP_FOLIO: TcxGridDBColumn
        Caption = 'Folio'
        DataBinding.FieldName = 'VP_FOLIO'
      end
      object PU_CODIGO: TcxGridDBColumn
        Caption = 'Puesto'
        DataBinding.FieldName = 'PU_CODIGO'
        Width = 64
      end
      object PU_DESCRIP: TcxGridDBColumn
        Caption = 'Descripci'#243'n de Puesto'
        DataBinding.FieldName = 'PU_DESCRIP'
        Width = 64
      end
      object VP_FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'VP_FECHA'
        Width = 64
      end
      object VP_GRADO_GRID: TcxGridDBColumn
        Caption = 'Grado'
        DataBinding.FieldName = 'VP_GRADO'
        Width = 64
      end
      object VP_PUNTOS: TcxGridDBColumn
        Caption = 'Puntos'
        DataBinding.FieldName = 'VP_PUNTOS'
      end
      object VP_NUM_FIN: TcxGridDBColumn
        Caption = 'Respuestas'
        DataBinding.FieldName = 'VP_NUM_FIN'
      end
      object VP_STATUS_GRID: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'VP_STATUS'
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 552
    Top = 24
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
    DesignInfo = 7864936
  end
  inherited ActionList: TActionList
    Left = 578
    Top = 120
  end
  inherited PopupMenu1: TPopupMenu
    Left = 544
    Top = 120
  end
end
