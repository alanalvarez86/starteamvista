unit FEditCatPeriodos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls, ZetaKeyCombo, ZetaNumero,
  ZetaFecha, ZetaDBTextBox, Db, ExtCtrls, Buttons, ZetaSmartLists,
  ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons;
type
  TEditCatPeriodos_DevEx = class(TBaseEdicion_DevEx)
    Panel2: TPanel;
    gbTotales: TGroupBox;
    lblEmpleados: TLabel;
    PE_NUM_EMP: TZetaDBTextBox;
    lblPercepciones: TLabel;
    PE_TOT_PER: TZetaDBTextBox;
    lblDeducciones: TLabel;
    PE_TOT_DED: TZetaDBTextBox;
    lblNetoAPagar: TLabel;
    PE_TOT_NET: TZetaDBTextBox;
    Panel1: TPanel;
    lblDescripcion: TLabel;
    Label10: TLabel;
    PE_FEC_MOD: TZetaDBTextBox;
    Label9: TLabel;
    Label4: TLabel;
    lblNumero: TLabel;
    lblAnio: TLabel;
    PE_YEAR: TZetaDBTextBox;
    lblTipo: TLabel;
    PE_DESCRIP: TDBEdit;
    gbAcumulados: TGroupBox;
    lblPeriodoMes: TLabel;
    lblPeriodoMesDe: TLabel;
    lblPeriodoAnio: TLabel;
    lblAcumula: TLabel;
    gbDias: TGroupBox;
    lblEstaNomina: TLabel;
    lblAcumuladoMes: TLabel;
    lblAcumuladoMesDe: TLabel;
    gbOpciones: TGroupBox;
    PE_AHORRO: TDBCheckBox;
    PE_PRESTAM: TDBCheckBox;
    PE_NUMERO: TZetaDBNumero;
    PE_DIAS: TZetaDBNumero;
    PE_DIAS_AC: TZetaDBNumero;
    PE_DIA_MES: TZetaDBNumero;
    PE_POS_MES: TZetaDBNumero;
    PE_PER_TOT: TZetaDBNumero;
    PE_PER_MES: TZetaDBNumero;
    ZNumero: TZetaDBTextBox;
    lblPeriodoAnioDe: TLabel;
    PE_MES: TZetaDBKeyCombo;
    PE_USO: TZetaDBKeyCombo;
    lblUsoNomina: TLabel;
    PE_SOLO_EX: TDBCheckBox;
    PE_INC_BAJ: TDBCheckBox;
    PE_STATUS: TZetaDBTextBox;
    US_CODIGO: TZetaDBTextBox;
    PE_TIPO: TZetaDBTextBox;
    gbAsistencia: TGroupBox;
    Label24: TLabel;
    PE_ASI_INI: TZetaDBFecha;
    Label25: TLabel;
    PE_ASI_FIN: TZetaDBFecha;
    gbPeriodo: TGroupBox;
    Label8: TLabel;
    PE_FEC_INI: TZetaDBFecha;
    Label7: TLabel;
    PE_FEC_FIN: TZetaDBFecha;
    Label18: TLabel;
    PE_FEC_PAG: TZetaDBFecha;
    PE_CAL: TDBCheckBox;
    gbEstatus: TGroupBox;
    gbFonacot: TGroupBox;
    lblCedulaPago: TLabel;
    PE_MES_FON: TZetaDBKeyCombo;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FECHASChange(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure PE_MESChange(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
  private
  protected
    procedure Connect; override;
    procedure Borrar; override;
    procedure NoAplica;
  public
    { Public declarations }
  end;

var
  EditCatPeriodos_DevEx: TEditCatPeriodos_DevEx;

implementation

uses dCliente, dCatalogos, dNomina, ZetaCommonClasses,
     DGlobal,ZGlobalTress,
     ZetaCommonTools, ZAccesosTress, ZetaCommonLists;

{$R *.DFM}

procedure TEditCatPeriodos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_NOMINA_PERIODOS;
     HelpContext:= H60623_Periodos_nomina;
     FirstControl := PE_NUMERO
end;

procedure TEditCatPeriodos_DevEx.NoAplica;
var lEnabled : Boolean;
begin
     with dmCatalogos.cdsPeriodo do
     begin
          lEnabled := True;

          if PE_MES.ItemIndex = 12 then
          begin
               lEnabled := False;
               PE_MES_FON.ItemIndex := ord(emfNoAplica);
          end;


          PE_MES_FON.Enabled := lEnabled;
          lblCedulaPago.Enabled := lEnabled;
     end;
end;

procedure TEditCatPeriodos_DevEx.FormShow(Sender: TObject);
begin
     inherited;

     if dmCatalogos.cdsPeriodo.FieldByName( 'PE_MES' ).AsString = '' then
        PE_MES.ItemIndex := 0;
     //OK_DevEx.Enabled := dmCliente.GetDatosPeriodoActivo.Tipo > K_PERIODO_VACIO;
          
     lblCedulaPago.Enabled := TRUE;
     PE_MES_FON.Enabled := TRUE;
     gbFonacot.Enabled := TRUE;
     
     NoAplica;
end;

procedure TEditCatPeriodos_DevEx.PE_MESChange(Sender: TObject);
begin
     inherited;
     dmCatalogos.Asignar_PE_MES_FON (PE_MES.ItemIndex + 1);
     NoAplica;
end;

procedure TEditCatPeriodos_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
     inherited;
     NoAplica;
end;

procedure TEditCatPeriodos_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          //cdsPeriodo.Conectar;
          DataSource.DataSet := cdsPeriodo;
     end;
end;

procedure TEditCatPeriodos_DevEx.Borrar;
begin
     dmCatalogos.cdsPeriodo.Borrar;
    // dmNomina.BorrarNominaActiva( False );
end;

procedure TEditCatPeriodos_DevEx.FECHASChange(Sender: TObject);
begin
     inherited;
     with dmCatalogos.cdsPeriodo do
     begin
          if state in [ dsEdit, dsInsert ] then
             FieldByName( 'PE_DIAS' ).AsInteger := iMin( iMax( Round( Fieldbyname( 'PE_FEC_FIN' ).AsDateTime - Fieldbyname( 'PE_FEC_INI' ).AsDateTime + 1 ), 1 ), 999 );
     end;
end;

procedure TEditCatPeriodos_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
     inherited;
     if ( ( Field = nil ) or ( Field.FieldName = 'PE_NUMERO' ) ) then
     begin
          with dmCatalogos.cdsPeriodo do
               //PE_CAL.Enabled :=  ( FieldByName( 'PE_NUMERO' ).AsInteger < K_LIMITE_NOM_NORMAL );
               PE_CAL.Enabled :=  ( FieldByName( 'PE_NUMERO' ).AsInteger < Global.GetGlobalInteger( K_GLOBAL_LIMITE_NOMINAS_ORDINARIAS ) );
     end;

     NoAplica;
end;

end.
