unit FCatMaestros_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Grids, DBGrids, Db, ExtCtrls,
     ZBaseGridLectura_DevEx,
     ZetaDBGrid, StdCtrls, ZBaseConsulta, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TCatMaestros_DevEx = class(TBaseGridLectura_DevEx)
    MA_CODIGO: TcxGridDBColumn;
    MA_NOMBRE: TcxGridDBColumn;
    MA_CEDULA: TcxGridDBColumn;
    MA_RFC: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations}
    procedure DoLookup; override;
  end;

var
  CatMaestros_DevEx: TCatMaestros_DevEx;

implementation

uses DCatalogos,
     ZetaBuscador_DevEx,
     ZetaCommonClasses;

{$R *.DFM}

{ TCatMaestros }

procedure TCatMaestros_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H60642_Maestros;
end;

procedure TCatMaestros_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsMaestros.Conectar;
          DataSource.DataSet:= cdsMaestros;
     end;
end;

procedure TCatMaestros_DevEx.Refresh;
begin
     dmCatalogos.cdsMaestros.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatMaestros_DevEx.Agregar;
begin
     dmCatalogos.cdsMaestros.Agregar;
end;

procedure TCatMaestros_DevEx.Borrar;
begin
     dmCatalogos.cdsMaestros.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatMaestros_DevEx.Modificar;
begin
     dmCatalogos.cdsMaestros.Modificar;
end;

procedure TCatMaestros_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Maestro', 'Maestro', 'MA_CODIGO', dmCatalogos.cdsMaestros );
end;

procedure TCatMaestros_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     {***Banda Sumatoria***}
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
  inherited;

end;

end.
