unit FCatEntNivel_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx, StdCtrls, Buttons, ExtCtrls, Db, DBClient,
  ZetaClientDataSet, CheckLst, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, ImgList, cxButtons, cxControls,
  cxContainer, cxEdit, cxCheckListBox;

type
  TCatEntNivel_DevEx = class(TZetaDlgModal_DevEx)
    Lista_DevEx: TcxCheckListBox;
    procedure FormShow(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    function  GetCriterioAdicional: TZetaLookupDataSet;
    function  YaExistia( const sCodigo : String ) : Boolean;
    procedure Inicializa;
    procedure GrabaSeleccionados;
  public
    { Public declarations }
  end;

var
  CatEntNivel_DevEx: TCatEntNivel_DevEx;
  sCodigo : String;

implementation

uses dCatalogos, dTablas, dGlobal, ZGlobalTress;

{$R *.DFM}

procedure TCatEntNivel_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     Caption := 'Seleccione ' + Global.GetDescNivelProg(K_NIVEL_PROGRAMACION_CURSOS);
     Inicializa;
end;

procedure TCatEntNivel_DevEx.Inicializa;
var
   oDataSet : TZetaLookupDataset;
   iPosicion : integer;
begin
     iPosicion := 0;
     oDataSet := GetCriterioAdicional;
     if Assigned( oDataSet ) then
        with oDataSet do
        begin
             Conectar;
             First;
             Lista_DevEx.Clear;
             while not Eof do
             begin
                  Lista_DevEx.Items.Add.Text := FieldByName( LookupKeyField ).AsString + '=' +
                                   FieldByName( LookupDescriptionField ).AsString ;
                  sCodigo := FieldByName(LookupKeyField).AsString;
                  if YaExistia( sCodigo ) then
                     Lista_DevEx.Items[iPosicion].Checked := TRUE;
                  iPosicion := iPosicion + 1;
                  Next;
             end;
        end;
end;


function TCatEntNivel_DevEx.GetCriterioAdicional: TZetaLookupDataSet;
begin
     Result := NIL;
     case Global.GetGlobalInteger( K_NIVEL_PROGRAMACION_CURSOS ) of
       1 : Result := dmCatalogos.cdsClasifi;
       2 : Result := dmCatalogos.cdsTurnos;
       3 : Result := dmTablas.cdsNivel1;
       4 : Result := dmTablas.cdsNivel2;
       5 : Result := dmTablas.cdsNivel3;
       6 : Result := dmTablas.cdsNivel4;
       7 : Result := dmTablas.cdsNivel5;
       8 : Result := dmTablas.cdsNivel6;
       9 : Result := dmTablas.cdsNivel7;
       10: Result := dmTablas.cdsNivel8;
       11: Result := dmTablas.cdsNivel9;
     end;
end;

procedure TCatEntNivel_DevEx.GrabaSeleccionados;
var
   i : Integer;
   sCodigo : String;
   tmpLista : TStringList;
procedure Split(Delimiter: Char; Str: string; ListOfStrings: TStrings) ;
begin
   ListOfStrings.Clear;
   ListOfStrings.Delimiter     := Delimiter;
   ListOfStrings.DelimitedText := Str;
end;
begin
     tmpLista := Tstringlist.create();
     with dmCatalogos.cdsEntNivel, Lista_DevEx do
     begin
        for i := 0 to Items.Count-1 do
        begin
             //DevEx (by am): Separamos la cadena para obtener el codigo
             Split('=',Lista_DevEx.Items[i].Text,tmpLista);
             sCodigo := trim(tmpLista[0]);
             if Items[ i ].Checked then                     // Si est� seleccionado
             begin
                  if not YaExistia( sCodigo ) then    // Y no existia, lo tengo que agregar
                  begin
                       Append;
                       FieldByName( 'ET_CODIGO' ).AsString := sCodigo;
                       Post;
                  end;
             end
             else                                     // No est� seleccionado, Si ya exist�a, lo tengo que borrar
                  if YaExistia( sCodigo ) then
                     Delete;
        end;
     end;
end;

function TCatEntNivel_DevEx.YaExistia(const sCodigo: String): Boolean;
begin
     Result := dmCatalogos.cdsEntNivel.Locate( 'ET_CODIGO', sCodigo, [ ] );
end;

procedure TCatEntNivel_DevEx.OK_DevExClick(Sender: TObject);
begin
  inherited;
     GrabaSeleccionados;
end;

end.
