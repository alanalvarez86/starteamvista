unit FEditCatPerfiles_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicionRenglon_DevEx, ZetaNumero, ZetaDBTextBox, StdCtrls, 
  Mask, ZetaFecha, DBCtrls, Db,
  {$ifdef CAJAAHORRO}
  dCajaAhorro,
  {$else}
  dRecursos,
  {$endif}
  Grids, DBGrids, ZetaDBGrid, ComCtrls, ExtCtrls, Buttons, ZetaKeyCombo,
  ZetaMessages, ZetaEdit, ZetaClientDataSet,  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,cxControls, dxSkinscxPCPainter, cxPCdxBarPopupMenu,
  dxSkinsdxBarPainter, ZetaKeyLookup_DevEx, dxBarExtItems, dxBar,
  cxClasses, ImgList, cxPC, cxNavigator, cxDBNavigator, cxButtons,
  cxContainer, cxEdit, cxTextEdit, cxMemo, cxDBEdit;

type
  TEditCPerfiles_DevEx = class(TBaseEdicionRenglon_DevEx)
    DedLbl: TLabel;
    Label8: TLabel;
    btnBuscarEmp: TSpeedButton;
    CP_ELEMENT: TDBEdit;
    Label4: TLabel;
    General: TcxTabSheet;
    CP_DETALLE: TDBMemo;
    Label2: TLabel;
    CP_INGLES: TDBEdit;
    Label3: TLabel;
    Label1: TLabel;
    CP_TEXTO: TDBEdit;
    Label7: TLabel;
    Label5: TLabel;
    dsCursos: TDataSource;
    dsRevisiones: TDataSource;
    CP_ACTIVO: TDBCheckBox;
    lookTPerfiles: TZetaDBKeyLookup_DevEx;
    zFecha: TZetaDBFecha;
    CP_NUMERO: TZetaDBNumero;
    CP_CODIGO: TZetaDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure BBModificarClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure BBBorrarClick(Sender: TObject);
    procedure GridRenglonesDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure GridRenglonesColExit(Sender: TObject);
  private
    procedure AplicaFiltro;
    procedure HabilitaControls(const lHabilita: Boolean);
    procedure AgregarDatos(Datos:TDataSet;iForma:Integer);

    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure KeyPress( var Key: Char ); override; { TWinControl }
  public
    { Public declarations }
  end;

var
  EditCPerfiles_DevEx: TEditCPerfiles_DevEx;

implementation

uses ZetaCommonClasses,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaDialogo,
     ZetaTipoEntidad,
     ZAccesosTress,
     dSistema,
     ZAccesosMgr,
     dCatalogos,
     dTablas;
{$R *.DFM}


procedure TEditCPerfiles_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsCatPerfiles.Conectar;
          Datasource.Dataset := cdsCatPerfiles;
          dsRenglon.DataSet := cdsRevPerfiles;
     end;
     with dmTablas do
     begin
          cdsTPerfiles.Conectar;
          lookTPerfiles.LookupDataset := cdsTPerfiles;
     end;
end;

procedure TEditCPerfiles_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= ZetaCommonClasses.H_Cat_GpoCompetencias ;
     IndexDerechos := ZAccesosTress.D_CAT_CPERFILES;
     FirstControl := CP_CODIGO;
end;

procedure TEditCPerfiles_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage := General;
end;


procedure TEditCPerfiles_DevEx.AgregarDatos(Datos:TDataSet;iForma:Integer);
begin
     IF ( ZAccesosMgr.CheckDerecho( D_CAT_CPERFILES  , K_DERECHO_CAMBIO ) )  then
     begin
          Datos.Append;
          GridRenglones.SetFocus;
     end
     else
         ZInformation('Perfiles','No Tiene Permiso Para Modificar Registro',0);
end;


procedure TEditCPerfiles_DevEx.BBAgregarClick(Sender: TObject);
begin
     AgregarDatos(dsRenglon.DataSet,1);
end;

procedure TEditCPerfiles_DevEx.BBModificarClick(Sender: TObject);
begin
     IF ( ZAccesosMgr.CheckDerecho( D_CAT_CPERFILES , K_DERECHO_CAMBIO ) ) THEN
     begin
          dsRenglon.DataSet.Edit;
          GridRenglones.SetFocus;  
     end
     else
         ZInformation('Perfiles','No Tiene Permiso Para Modificar Registros',0);
end;

procedure TEditCPerfiles_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
     Procedure HabilitaDetalles( bHabilita : Boolean );
     begin
          BBAgregar_DevEx.Enabled := bHabilita;
          BBBorrar_DevEx.Enabled := bHabilita;
          BBModificar_DevEx.Enabled := bHabilita;
          GridRenglones.Enabled := bHabilita; 
     end;
begin
     inherited;
     if Field = nil then
     begin
          with dmCatalogos.cdsCatPerfiles do
          begin
               if State = dsInsert then
               begin
                    AplicaFiltro;
                    HabilitaControls( ZAccesosMgr.CheckDerecho( D_CAT_CPERFILES , K_DERECHO_ALTA )  );
                    HabilitaDetalles( False );
               end;
               if State = dsBrowse then
               begin
                    AplicaFiltro;
                    HabilitaControls( ZAccesosMgr.CheckDerecho( D_CAT_CPERFILES , K_DERECHO_CAMBIO )  );
                    HabilitaDetalles( True );
               end;
          end;
     end;
end;

procedure TEditCPerfiles_DevEx.AplicaFiltro;
     procedure AplicandoFiltro(Datos:TZetaClientDataSet);
     begin
          with Datos do
          begin
               Filtered := False;
               Filter := Format('CP_CODIGO = ''%s''',[dmCatalogos.cdsCatPerfiles.FieldByName('CP_CODIGO').AsString]);
               Filtered := True;
          end;
     end;
begin
     with dmCatalogos do
     begin
          AplicandoFiltro(cdsRevPerfiles);
     end;
end;


procedure TEditCPerfiles_DevEx.HabilitaControls(const lHabilita:Boolean);
begin
     CP_CODIGO.Enabled := lHabilita;
     CP_NUMERO.Enabled := lHabilita;
     CP_ELEMENT.Enabled := lHabilita;
     CP_INGLES.Enabled := lHabilita;
     CP_TEXTO.Enabled := lHabilita;
     CP_DETALLE.Enabled := lHabilita;
     CP_ACTIVO.Enabled := lHabilita;
     lookTPerfiles.Enabled := lHabilita;
     GridRenglones.Enabled := lHabilita; 

end;

procedure TEditCPerfiles_DevEx.BBBorrarClick(Sender: TObject);
begin
     IF ( ZAccesosMgr.CheckDerecho( D_CAT_CPERFILES , K_DERECHO_CAMBIO ) ) then
     begin
          dmCatalogos.cdsRevPerfiles.Borrar;  
     end
     else
         ZInformation('Perfiles','No Tiene Permiso Para Modificar Registros',0);
end;

procedure TEditCPerfiles_DevEx.GridRenglonesDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
 procedure AjustaControlEnCelda( oControl: TControl );
   begin
        with oControl do
        begin
             Left := Rect.Left + GridRenglones.Left;
             Top := Rect.Top + GridRenglones.Top;
             Width := Column.Width + 2;
             Visible := True;
        end;
   end;
begin
     inherited;
     if ( gdFocused in State ) then
     begin
          if ( Column.FieldName = 'RP_FEC_INI' ) then
             AjustaControlEnCelda( zFecha )
     end;
end;

procedure TEditCPerfiles_DevEx.KeyPress(var Key: Char);
function EsControlGrid: Boolean;
   begin
        Result := ( self.ActiveControl = zFecha );
   end;

begin
     inherited;
     if GridEnfocado then
     begin
          if ( ( Key <> Chr( 9 ) ) and ( Key <> #0 ) ) then
          begin
               if ( GridRenglones.SelectedField.FieldName = 'RP_FEC_INI' ) then
               begin
                    zFecha.SetFocus;
                    SendMessage( zFecha.Handle, WM_CHAR, Word( Key ), 0 );
                    Key := #0;
               end;
          end;
     end
     else if EsControlGrid and ( ( Key = Chr( VK_RETURN ) ) or ( Key = Chr( 9 ) ) ) then
     begin
          Key := #0;
          with GridRenglones do
          begin
               SetFocus;
               GridRenglones.SelectedIndex := PosicionaSiguienteColumna;
          end;
     end;

end;

procedure TEditCPerfiles_DevEx.GridRenglonesColExit(Sender: TObject);
begin
   inherited;
   with GridRenglones do
     begin
          if ( SelectedField <> nil ) then
          begin
               if ( SelectedField.FieldName = 'RP_FEC_INI' ) and ( zFecha.Visible ) then
                  zFecha.Visible:= FALSE;
          end;
     end;
end;

end.
