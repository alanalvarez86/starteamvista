inherited EditCosteoCriteriosPorConcepto_DevEx: TEditCosteoCriteriosPorConcepto_DevEx
  Left = 0
  Top = 0
  Caption = 'Edici'#243'n de Conceptos de Costeo'
  ClientHeight = 178
  ClientWidth = 447
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 17
    Top = 44
    Width = 83
    Height = 13
    BiDiMode = bdLeftToRight
    Caption = 'Grupo de Costeo:'
    ParentBiDiMode = False
  end
  object Label2: TLabel [1]
    Left = 14
    Top = 68
    Width = 86
    Height = 13
    BiDiMode = bdLeftToRight
    Caption = 'Criterio de Costeo:'
    ParentBiDiMode = False
  end
  object Label3: TLabel [2]
    Left = 52
    Top = 92
    Width = 49
    Height = 13
    BiDiMode = bdLeftToRight
    Caption = '&Concepto:'
    FocusControl = CO_NUMERO
    ParentBiDiMode = False
  end
  object GrupoId: TZetaDBTextBox [3]
    Left = 104
    Top = 40
    Width = 60
    Height = 17
    AutoSize = False
    Caption = 'GrupoId'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'GpoCostoCodigo'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object GrupoDescripcion: TZetaDBTextBox [4]
    Left = 166
    Top = 40
    Width = 211
    Height = 17
    AutoSize = False
    Caption = 'GrupoDescripcion'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'GpoCostoNombre'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CriterioId: TZetaDBTextBox [5]
    Left = 104
    Top = 64
    Width = 60
    Height = 17
    AutoSize = False
    Caption = 'CriterioId'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'CritCostoID'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CriterioDescripcion: TZetaDBTextBox [6]
    Left = 166
    Top = 64
    Width = 211
    Height = 17
    AutoSize = False
    Caption = 'CriterioDescripcion'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'DescripcionCriterios'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  inherited PanelBotones: TPanel
    Top = 142
    Width = 447
    TabOrder = 1
    inherited OK_DevEx: TcxButton
      Left = 283
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 362
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 447
    TabOrder = 3
    inherited ValorActivo2: TPanel
      Width = 121
      inherited textoValorActivo2: TLabel
        Width = 115
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    Left = 0
    Top = 0
  end
  object CO_NUMERO: TZetaDBKeyLookup_DevEx [10]
    Left = 104
    Top = 88
    Width = 300
    Height = 21
    Hint = 'Concepto de Costeo'
    Opcional = False
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    ShowHint = True
    TabOrder = 0
    TabStop = True
    WidthLlave = 60
    DataField = 'CO_NUMERO'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 396
    Top = 1
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_AgregarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_ImprimirBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_ImprimirFormaBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_ExportarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_CortarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_CopiarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_PegarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_UndoBtn: TdxBarButton
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
