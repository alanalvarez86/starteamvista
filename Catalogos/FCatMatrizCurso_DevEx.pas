{$HINTS OFF}
unit FCatMatrizCurso_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls, StdCtrls,
  Buttons, ZBaseConsulta, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons, ZetaKeyLookup_DevEx,
  System.Actions;

type
  TCatMatrizCurso_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    LookUpCursos: TZetaKeyLookup_DevEx;
    PU_CODIGO: TcxGridDBColumn;
    PU_NOMBRE: TcxGridDBColumn;
    EN_DIAS: TcxGridDBColumn;
    EN_OPCIONA: TcxGridDBColumn;
    EN_LISTA: TcxGridDBColumn;
    BAgregaPuestos_DevEx: TcxButton;
    procedure LookUpCursosValidKey(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BAgregaPuestosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BAgregaPuestos_DevExClick(Sender: TObject);
    procedure SetEditarSoloActivos;
  private
    procedure ConfigAgrupamiento;
  protected
    procedure Connect; override;
    procedure DisConnect;override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
  public
  end;

var
  CatMatrizCurso_DevEx: TCatMatrizCurso_DevEx;

implementation

uses dCatalogos, dCliente, ZetaCommonClasses, FAutoClasses, FListaPuestos_DevEx;

{$R *.DFM}

{ TCatMatrizCurso }

procedure TCatMatrizCurso_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H60643_Matriz_curso;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TCatMatrizCurso_DevEx.DisConnect;
begin
     LookUpCursos.LookUpDataSet := NIL;
end;

procedure TCatMatrizCurso_DevEx.Connect;
begin
     LookUpCursos.LookUpDataSet := dmCatalogos.cdsCursos;
     with dmCatalogos do
     begin
          with cdsCursos do
          begin
               Conectar;
               LookUpCursos.Llave := FieldByName('CU_CODIGO').AsString;
          end;
          SetMatriz( TRUE, LookUpCursos.Llave );
          cdsPuestos.Conectar;
          cdsMatrizCurso.Conectar;
          DataSource.DataSet:= cdsMatrizCurso;
          DoRefresh;
     end;
end;

procedure TCatMatrizCurso_DevEx.Refresh;
begin
     dmCatalogos.cdsMatrizCurso.Refrescar;
     // No s� por qu�, pero se pierde al activar la otra Matriz de Cursos
     ZetaDBGridDBTableView.Columns[ 0 ].DataBinding.FieldName := 'PU_CODIGO';
     ZetaDBGridDBTableView.Columns[ 2 ].DataBinding.FieldName := 'EN_DIAS';
     ZetaDBGridDBTableView.Columns[ 3 ].DataBinding.FieldName := 'EN_OPCIONA';
     ZetaDBGridDBTableView.Columns[ 4 ].DataBinding.FieldName := 'EN_LISTA';
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatMatrizCurso_DevEx.LookUpCursosValidKey(Sender: TObject);
begin
     inherited;
     DoRefresh;
end;

procedure TCatMatrizCurso_DevEx.Agregar;
begin
     dmCatalogos.cdsMatrizCurso.Agregar;
end;

procedure TCatMatrizCurso_DevEx.Borrar;
begin
     dmCatalogos.cdsMatrizCurso.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatMatrizCurso_DevEx.Modificar;
begin
     dmCatalogos.cdsMatrizCurso.Modificar;
end;

function TCatMatrizCurso_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeAgregar( sMensaje );
end;

function TCatMatrizCurso_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeModificar( sMensaje );
end;

function TCatMatrizCurso_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeBorrar( sMensaje );
end;

procedure TCatMatrizCurso_DevEx.BAgregaPuestosClick(Sender: TObject);
begin
     inherited;
     if SetListaPuestos( LookUpCursos.Llave,True ) then
        DoRefresh;
end;

procedure TCatMatrizCurso_DevEx.ConfigAgrupamiento;
begin
     //Cambio de configuracion
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //Desactiva la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Esconde la caja de agrupamiento
     //Determinar las columnas por las que se podra agrupar
     PU_CODIGO.Options.Grouping:= FALSE;
     PU_NOMBRE.Options.Grouping:= FALSE;
     EN_DIAS.Options.Grouping:= FALSE;
     EN_LISTA.Options.Grouping:= FALSE;
end;

procedure TCatMatrizCurso_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     {***Banda Sumatoria***}
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
     inherited;
     ConfigAgrupamiento;
end;

procedure TCatMatrizCurso_DevEx.BAgregaPuestos_DevExClick(Sender: TObject);
var
  lFiltrado : Boolean;
begin
  inherited;
  with dmCatalogos.cdsMatrizCurso do
  begin
    lFiltrado := Filtered;
    Filtered := False;

     if SetListaPuestos( LookUpCursos.Llave,True ) then
        DoRefresh;

    Filtered := lFiltrado;
  end;
end;

procedure TCatMatrizCurso_DevEx.SetEditarSoloActivos;
begin
     LookUpCursos.EditarSoloActivos := TRUE;
end;


end.
