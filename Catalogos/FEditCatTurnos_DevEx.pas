unit FEditCatTurnos_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, ComCtrls,
     Forms, Dialogs, Db, Buttons, DBCtrls, StdCtrls, ExtCtrls, Mask,
     dbClient, ZBaseEdicion_DevEx,
     ZetaKeyCombo,
     ZetaFecha,
     ZetaNumero, ZetaEdit, ZetaClientDataSet,
     cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC,
  ZetaKeyLookup_DevEx, cxContainer, cxEdit, cxTextEdit, cxMemo, cxDBEdit,
  dxBarBuiltInMenu;

type
  TEditCatTurnos_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    TU_CODIGOlbl: TLabel;
    TU_CODIGO: TZetaDBEdit;
    TU_DESCRIPlbl: TLabel;
    TU_DESCRIP: TDBEdit;
    TU_ACTIVO: TDBCheckBox;
    dxBarButton_bImpSubCuentas: TdxBarButton;
    bbMostrarCalendario_DevEx: TcxButton;
    PageControl_DevEx: TcxPageControl;
    TabDatos_DevEx: TcxTabSheet;
    TabHorarios_DevEx: TcxTabSheet;
    TabRitmicos_DevEx: TcxTabSheet;
    tsConfidencialidad_DevEx: TcxTabSheet;
    TU_TIP_JT: TZetaDBKeyCombo;
    TU_SUB_CTA: TDBEdit;
    gbJornada: TGroupBox;
    TU_JORNADAlbl: TLabel;
    Label2: TLabel;
    TU_DOBLESlbl: TLabel;
    TU_DOMINGOlbl: TLabel;
    Label4: TLabel;
    TU_DIAS: TZetaDBNumero;
    TU_JORNADA: TZetaDBNumero;
    TU_DOBLES: TZetaDBNumero;
    TU_DOMINGO: TZetaDBNumero;
    TU_DIAS_BA: TZetaDBNumero;
    gbVacaciones: TGroupBox;
    lblHabil: TLabel;
    lblSabado: TLabel;
    lblFestivo: TLabel;
    TU_VACA_HA: TZetaDBNumero;
    TU_VACA_SA: TZetaDBNumero;
    TU_VACA_DE: TZetaDBNumero;
    TB_TEXTO: TDBEdit;
    TB_NUMERO: TZetaDBNumero;
    TB_INGLES: TDBEdit;
    TU_TIP_JOR: TZetaDBKeyCombo;
    TU_HORARIO: TZetaDBKeyCombo;
    lblTipoJornada: TLabel;
    lblSubCuenta: TLabel;
    Label1: TLabel;
    Label6: TLabel;
    Label5: TLabel;
    Label3: TLabel;
    TU_HORARIOlbl: TLabel;
    gbHorSemana: TGroupBox;
    Dia1Lbl: TLabel;
    Dia2Lbl: TLabel;
    Dia3Lbl: TLabel;
    Dia4Lbl: TLabel;
    Dia5Lbl: TLabel;
    Dia6Lbl: TLabel;
    Dia7Lbl: TLabel;
    TU_TIP_1: TZetaDBKeyCombo;
    TU_TIP_2: TZetaDBKeyCombo;
    TU_TIP_3: TZetaDBKeyCombo;
    TU_TIP_4: TZetaDBKeyCombo;
    TU_TIP_5: TZetaDBKeyCombo;
    TU_TIP_6: TZetaDBKeyCombo;
    TU_TIP_7: TZetaDBKeyCombo;
    TU_HOR_1: TZetaDBKeyLookup_DevEx;
    TU_HOR_2: TZetaDBKeyLookup_DevEx;
    TU_HOR_3: TZetaDBKeyLookup_DevEx;
    TU_HOR_4: TZetaDBKeyLookup_DevEx;
    TU_HOR_5: TZetaDBKeyLookup_DevEx;
    TU_HOR_6: TZetaDBKeyLookup_DevEx;
    TU_HOR_7: TZetaDBKeyLookup_DevEx;
    TU_HOR_FES: TZetaDBKeyLookup_DevEx;
    DiaFestLbl: TLabel;
    TU_RIT_INI: TZetaDBFecha;
    TU_RIT_PAT: TcxDBMemo;
    TU_RIT_PATlbl: TLabel;
    TU_RIT_INIlbl: TLabel;
    gbConfidencialidad: TGroupBox;
    listaConfidencialidad: TListBox;
    rbConfidenTodas: TRadioButton;
    rbConfidenAlgunas: TRadioButton;
    btSeleccionarConfiden_DevEx: TcxButton;
    TIPO_JORNADA_SAT: TZetaDBKeyLookup_DevEx;
    Label7: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure TU_RIT_PATChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TU_RIT_PATKeyPress(Sender: TObject; var Key: Char);
    //procedure bbMostrarCalendarioClick(Sender: TObject);
    //procedure bImpSubCuentasClick(Sender: TObject);
    //procedure btSeleccionarConfidenClick(Sender: TObject);
    procedure rbConfidenTodasClick(Sender: TObject);
    procedure rbConfidenAlgunasClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure dxBarButton_bImpSubCuentasClick(Sender: TObject);
    procedure bbMostrarCalendario_DevExClick(Sender: TObject);
    procedure btSeleccionarConfiden_DevExClick(Sender: TObject);
    procedure SetEditarSoloActivos;
  private
    lValoresConfidencialidad : TStringList;
    function DiaSemanaNomina( const iPrimero, iDia: Integer ): String;
    procedure SetControls;
    procedure SetVacacionesControls( const lNoUsar: Boolean);
    procedure SetDiasLabels;
    procedure FillListaConfidencialidad;
    procedure SetListConfidencialidad( sValores : string );
    procedure GetConfidencialidad;
  protected
    procedure Connect; override;
    procedure DoLookup; override;
    procedure HabilitaControles;override;
  public
  end;

var
  EditCatTurnos_DevEx: TEditCatTurnos_DevEx;

implementation

uses dCatalogos,
     dGlobal,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZGlobalTress,
     ZetaBuscador_DevEx,
     ZAccesosTress,
     ZetaCommonLists,
     ZImportaSubCuentas,
     FSeleccionarConfidencialidad_DevEx, DCliente, dSistema,DTablas;

{$R *.DFM}

// directiva temporal se va a mostrar el dato pero no se va a hacer nada con el
{$define TJORNADA_FESTIVOS}

{ TEditCatTurnos }

procedure TEditCatTurnos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_CONT_TURNOS;
     HelpContext:= H60614_Turnos;
     FirstControl := TU_CODIGO;
     with dmCatalogos do
     begin
          TU_HOR_1.LookupDataSet := cdsHorarios;
          TU_HOR_2.LookupDataSet := cdsHorarios;
          TU_HOR_3.LookupDataSet := cdsHorarios;
          TU_HOR_4.LookupDataSet := cdsHorarios;
          TU_HOR_5.LookupDataSet := cdsHorarios;
          TU_HOR_6.LookupDataSet := cdsHorarios;
          TU_HOR_7.LookupDataSet := cdsHorarios;
          TU_HOR_FES.LookupDataSet := cdsHorarios;

          {$ifdef TJORNADA_FESTIVOS}
          with TU_TIP_JT do
          begin
               Visible := TRUE;
               DataSource:= Self.DataSource;
               DataField:= 'TU_TIP_JT';
          end;
          lblTipoJornada.Visible:= TRUE;
          {$else}
          TU_TIP_JT.Visible := FALSE;
          lblTipoJornada.Visible:= FALSE;
          TU_SUB_CTA.Top:= TU_TIP_JT.Top;
          lblSubCuenta.Top:= lblTipoJornada.Top;
          {$endif}
     end;
     lValoresConfidencialidad := TStringList.Create;

     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;

end;

procedure TEditCatTurnos_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     SetDiasLabels;
     //OBJETIVO : Siempre Este Activa la Primera Pesta�a, si no mientras la forma este creada,
     //           inicia con la ultima activa
     PageControl_DevEx.ActivePage := TabDatos_DevEx;
     SetVacacionesControls( eValorDiasVacaciones( Global.GetGlobalInteger( K_GLOBAL_DIAS_VACACIONES ) ) <> dvNoUsar );
     SetControls;
     FillListaConfidencialidad;
end;

procedure TEditCatTurnos_DevEx.Connect;
begin
     dmSistema.cdsNivel0.Conectar;
     dmCatalogos.cdsHorarios.Conectar;
     Datasource.DataSet := dmCatalogos.cdsTurnos;

     with dmTablas do
     begin
          cdsTipoJornadaSat.Conectar;
          TIPO_JORNADA_SAT.LookUpDataSet := dmTablas.cdsTipoJornadaSat;
     end;

end;

procedure TEditCatTurnos_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', 'Turnos', 'TU_CODIGO', dmCatalogos.cdsTurnos );
end;

procedure TEditCatTurnos_DevEx.SetVacacionesControls( const lNoUsar: Boolean);
begin
     lblHabil.Enabled := lNoUsar;
     TU_VACA_HA.Enabled := lNoUsar;
     lblSabado.Enabled := lNoUsar;
     TU_VACA_SA.Enabled := lNoUsar;
     lblFestivo.Enabled := lNoUsar;
     TU_VACA_DE.Enabled := lNoUsar;
end;

procedure TEditCatTurnos_DevEx.SetControls;
begin
     with TU_RIT_INI do
     begin
          Enabled := ( TU_RIT_PAT.Lines.Count > 0 );
          TU_RIT_INIlbl.Enabled := Enabled;
     end;
end;

procedure TEditCatTurnos_DevEx.TU_RIT_PATChange(Sender: TObject);
begin
     inherited;
     SetControls;
end;

procedure TEditCatTurnos_DevEx.SetDiasLabels;
var
   iPrimerDia: Integer;
begin
     iPrimerDia := Global.GetGlobalInteger( K_GLOBAL_PRIMER_DIA );
     Dia1Lbl.Caption := DiaSemanaNomina( iPrimerDia, 1 ) + ':';
     Dia2Lbl.Caption := DiaSemanaNomina( iPrimerDia, 2 ) + ':';
     Dia3Lbl.Caption := DiaSemanaNomina( iPrimerDia, 3 ) + ':';
     Dia4Lbl.Caption := DiaSemanaNomina( iPrimerDia, 4 ) + ':';
     Dia5Lbl.Caption := DiaSemanaNomina( iPrimerDia, 5 ) + ':';
     Dia6Lbl.Caption := DiaSemanaNomina( iPrimerDia, 6 ) + ':';
     Dia7Lbl.Caption := DiaSemanaNomina( iPrimerDia, 7 ) + ':';
end;

function TEditCatTurnos_DevEx.DiaSemanaNomina(const iPrimero, iDia: Integer): String;
var
   iPos : Integer;
begin { La Descripci�n del d�a de la semana de nomina de acuerdo al primer dia de la semana }
     iPos := ( ( iDia + iPrimero - 1 ) mod K_DIAS_SEMANA ) ;
     if iPos = 0 then
        Result := {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}LongDayNames[ K_DIAS_SEMANA ]
     else
        Result := {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}LongDayNames[ iPos ];
end;

procedure TEditCatTurnos_DevEx.TU_RIT_PATKeyPress(Sender: TObject; var Key: Char);

   function LongitudMemo: Integer;
   var
      i: Integer;
   begin
        Result := 0;
        with TU_RIT_PAT.Lines do
        begin
             for i := 0 to Count - 1 do
                 Result := Result + Length( Strings[i] );
        end;
   end;

begin
   {  if ( Ord( Key ) >= 32 ) and ( Ord( Key ) <= 127 ) and ( LongitudMemo >= K_ANCHO_FORMULA ) then
      17/10/2007: La longitud queda condicionada en la base de datos, �sta puede crecer a mas de 255 }
     if ( Ord( Key ) >= 32 ) and ( Ord( Key ) <= 127 ) and ( LongitudMemo >= TU_RIT_PAT.DataBinding.Field.Size ) then
        Key := #0;
     inherited;
end;

{procedure TEditCatTurnos_DevEx.bbMostrarCalendarioClick(Sender: TObject);
begin
     inherited;
     dmCatalogos.ShowCalendarioRitmo( TU_CODIGO.Text );
end; }

{procedure TEditCatTurnos_DevEx.bImpSubCuentasClick(Sender: TObject);
begin
     inherited;
     ZImportaSubCuentas.ImportaArchivo( TZetaClientDataset( DataSource.DataSet ), 'TU_CODIGO', 'TU_SUB_CTA' );
end;}


procedure TEditCatTurnos_DevEx.HabilitaControles;
begin
     inherited;
     dxBarButton_bImpSubCuentas.Enabled := not Editing;
end;

{procedure TEditCatTurnos_DevEx.btSeleccionarConfidenClick(Sender: TObject);
var
   selConfi : TSeleccionarConfidencialidad;
   sCM_NIVEL0Nuevo : string;
begin
  inherited;

  selConfi := TSeleccionarConfidencialidad.Create( Self );
  sCM_NIVEL0Nuevo := selConfi.GetConfidencialidad( DataSource.DataSet.FieldByName('TU_NIVEL0').AsString, lValoresConfidencialidad, True, dmCliente.Confidencialidad );

  if (  sCM_NIVEL0Nuevo <>  DataSource.DataSet.FieldByName('TU_NIVEL0').AsString ) then
  begin
     with DataSource.DataSet do
     begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

           FieldByName('TU_NIVEL0').AsString  := sCM_NIVEL0Nuevo;
           GetConfidencialidad;
     end;
  end;

  FreeAndNil ( selConfi ) ;
end;}

procedure TEditCatTurnos_DevEx.FillListaConfidencialidad;
begin
     with dmSistema.cdsNivel0 do
     begin
          First;
          lValoresConfidencialidad.Clear;
          lValoresConfidencialidad.NameValueSeparator := '=' ;
          While( not Eof )do
          begin
               lValoresConfidencialidad.Add(FieldByName('TB_CODIGO').AsString +'='+ FieldByName('TB_ELEMENT').AsString);
               Next;
          end;
     end;
end;

procedure TEditCatTurnos_DevEx.GetConfidencialidad;
begin
   if (  DataSource.DataSet  <> nil ) then
   with DataSource.DataSet do
   begin
        SetListConfidencialidad(  FieldByName('TU_NIVEL0').AsString );
   end;
end;

procedure TEditCatTurnos_DevEx.SetListConfidencialidad(sValores: string);
var
   Lista : TStringList;
   i, j : integer;
begin
    Lista := TStringList.Create;

    sValores := Trim( sValores );
    if StrLleno( sValores ) then
        Lista.CommaText  := sValores;

    listaConfidencialidad.Items.Clear;

    rbConfidenTodas.OnClick := nil;
    rbConfidenAlgunas.OnClick := nil;
    rbConfidenTodas.Checked := False;
    rbConfidenAlgunas.Checked := False;
                                                 
    if lValoresConfidencialidad.Count = 0 then
       FillListaConfidencialidad;
       
    for i:= 0 to lValoresConfidencialidad.Count - 1 do
    begin
        if ( Lista.Count > 0 ) then
        begin
                j := Lista.IndexOf( Trim( lValoresConfidencialidad.Names[i] )  );
                if ( j >= 0 ) then
                   listaConfidencialidad.Items.Add( lValoresConfidencialidad[i] )
        end
    end;

    rbConfidenTodas.Checked :=   listaConfidencialidad.Items.Count = 0;
    rbConfidenAlgunas.Checked := not rbConfidenTodas.Checked;

    if ( gbConfidencialidad.Enabled ) then
    begin
        listaConfidencialidad.Enabled := rbConfidenAlgunas.Checked;
        btSeleccionarConfiden_DevEx.Enabled := rbConfidenAlgunas.Checked;
    end;

    rbConfidenTodas.OnClick := rbConfidenTodasClick;
    rbConfidenAlgunas.OnClick := rbConfidenAlgunasClick;

    FreeAndNil( Lista );

end;

procedure TEditCatTurnos_DevEx.rbConfidenTodasClick(Sender: TObject);
begin
  inherited;

   with DataSource.DataSet do
   begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

        FieldByName('TU_NIVEL0').AsString  :=  '';
   end;
   GetConfidencialidad;
end;

procedure TEditCatTurnos_DevEx.rbConfidenAlgunasClick(Sender: TObject);
begin
  inherited;
     if ( rbConfidenAlgunas.Checked ) then
     begin
        btSeleccionarConfiden_DevExClick( Sender );
        GetConfidencialidad;
     end;

end;

procedure TEditCatTurnos_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
     if ( Field = nil ) then
     begin
          with DataSource.DataSet do
          begin
               if ( State in [dsBrowse] ) then
                  GetConfidencialidad
               else if (State in [dsInsert]) then
                    rbConfidenTodas.Checked := TRUE;
          end;

     end;
end;

procedure TEditCatTurnos_DevEx.dxBarButton_bImpSubCuentasClick(
  Sender: TObject);
begin
  inherited;
     ZImportaSubCuentas.ImportaArchivo( TZetaClientDataset( DataSource.DataSet ), 'TU_CODIGO', 'TU_SUB_CTA' );

end;

procedure TEditCatTurnos_DevEx.bbMostrarCalendario_DevExClick(
  Sender: TObject);
begin
  inherited;
     dmCatalogos.ShowCalendarioRitmo( TU_CODIGO.Text );

end;

procedure TEditCatTurnos_DevEx.btSeleccionarConfiden_DevExClick(
  Sender: TObject);
var
   selConfi : TSeleccionarConfidencialidad_DevEx;
   sCM_NIVEL0Nuevo : string;
begin
  inherited;

  selConfi := TSeleccionarConfidencialidad_DevEx.Create( Self );
  sCM_NIVEL0Nuevo := selConfi.GetConfidencialidad( DataSource.DataSet.FieldByName('TU_NIVEL0').AsString, lValoresConfidencialidad, True, dmCliente.Confidencialidad );

  if (  sCM_NIVEL0Nuevo <>  DataSource.DataSet.FieldByName('TU_NIVEL0').AsString ) then
  begin
     with DataSource.DataSet do
     begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

           FieldByName('TU_NIVEL0').AsString  := sCM_NIVEL0Nuevo;
           GetConfidencialidad;
     end;
  end;

  FreeAndNil ( selConfi ) ;
end;

procedure TEditCatTurnos_DevEx.SetEditarSoloActivos;
begin
     TU_HOR_1.EditarSoloActivos := TRUE;
     TU_HOR_2.EditarSoloActivos := TRUE;
     TU_HOR_3.EditarSoloActivos := TRUE;
     TU_HOR_4.EditarSoloActivos := TRUE;
     TU_HOR_5.EditarSoloActivos := TRUE;
     TU_HOR_6.EditarSoloActivos := TRUE;
     TU_HOR_7.EditarSoloActivos := TRUE;
     TU_HOR_FES.EditarSoloActivos := TRUE;
end;

end.
