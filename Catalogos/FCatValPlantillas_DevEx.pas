unit FCatValPlantillas_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseGridLectura_DevEx, DB, ExtCtrls,
  StdCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid;

type
  TCatValPlantillas_DevEx = class(TBaseGridLectura_DevEx)
    VL_CODIGO: TcxGridDBColumn;
    VL_NOMBRE: TcxGridDBColumn;
    VL_FECHA: TcxGridDBColumn;
    VL_MAX_PTS: TcxGridDBColumn;
    VL_NUM_FAC: TcxGridDBColumn;
    VL_NUM_SUB: TcxGridDBColumn;
    VL_NUM_PTO: TcxGridDBColumn;
    VL_STATUS: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
   { Public declarations }
   procedure DoLookup; override;
  end;

var
  CatValPlantillas_DevEx: TCatValPlantillas_DevEx;

implementation

uses DCatalogos,
     ZetaCommonClasses,
     ZetaBuscador_DevEx;

{$R *.dfm}

procedure TCatValPlantillas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H_VALUACION_PLANTILLA;
end;

procedure TCatValPlantillas_DevEx.Agregar;
begin
     dmCatalogos.cdsValPlantilla.Agregar;
end;

procedure TCatValPlantillas_DevEx.Borrar;
begin
     dmCatalogos.cdsValPlantilla.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatValPlantillas_DevEx.Modificar;
begin
     dmCatalogos.cdsValPlantilla.Modificar;
end;


procedure TCatValPlantillas_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsValPlantilla.Conectar;
          DataSource.DataSet:= cdsValPlantilla;
     end;
end;

procedure TCatValPlantillas_DevEx.Refresh;
begin
     dmCatalogos.cdsValPlantilla.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatValPlantillas_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Plantilla', 'Plantilla', 'VL_CODIGO', dmCatalogos.cdsValPlantilla );
end;

procedure TCatValPlantillas_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;

     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
     ZetaDBGridDBTableView.OptionsView.GroupByBox := True;

     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('VL_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

end.
