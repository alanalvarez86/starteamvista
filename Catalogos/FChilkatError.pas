unit FChilkatError;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ZBaseDlgModal_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore,
  TressMorado2013, cxControls, cxContainer, cxEdit, cxLabel, cxTextEdit, cxMemo,
  Vcl.ImgList, Vcl.StdCtrls, cxButtons, Vcl.ExtCtrls, cxPC, dxDockControl,
  dxDockPanel, cxImage, dxGDIPlusClasses;

type
  TChilkatError = class(TZetaDlgModal_DevEx)
    pnlErrorDetalle: TPanel;
    memChilkatError: TcxMemo;
    btnVerDetalle: TcxButton;
    lblError: TLabel;
    imgError: TcxImage;
    lblMensaje: TLabel;
    procedure FormShow(Sender: TObject);
    procedure btnVerDetalleClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FError: String;
    FErrorDetalle: String;
  public
    { Public declarations }
    property Error: String read FError write FError;
    property ErrorDetalle: String read FErrorDetalle write FErrorDetalle;
  end;

var
  ChilkatError: TChilkatError;

implementation

{$R *.dfm}

uses ZetaCommonClasses;

const
     K_MENSAJE = '. Las posibles causas que no le permiten continuar pueden ser:' + CR_LF +
                 '  1. La contraseņa de la llave privada es incorrecta.' + CR_LF +
                 '  2. No tiene registrados debidamente los componentes (dlls) del cliente o servidor. ' + CR_LF +
                 '  3. Los archivos de la llave privada y certificado no coinciden.' + CR_LF +
                 '  4. Los archivos de la llave privada o certificado no fueron expedidos por el SAT.';

procedure TChilkatError.btnVerDetalleClick(Sender: TObject);
begin
  inherited;
  pnlErrorDetalle.Visible := TRUE;
  Height := 510;
  pnlErrorDetalle.Top := 144;
  pnlErrorDetalle.Height := 283;
  btnVerDetalle.Enabled := FALSE;
  OK_DevEx.SetFocus;
end;

procedure TChilkatError.FormCreate(Sender: TObject);
begin
  inherited;
  Height := 218;
  pnlErrorDetalle.Height := 1;
end;

procedure TChilkatError.FormShow(Sender: TObject);
begin
  inherited;
    Caption := Error;
    lblError.Caption := Error + K_MENSAJE;
    memChilkatError.Lines.Text := ErrorDetalle;
    btnVerDetalle.SetFocus;
end;

end.


