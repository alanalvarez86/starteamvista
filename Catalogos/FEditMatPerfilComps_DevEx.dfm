inherited EditMatPerComps_DevEx: TEditMatPerComps_DevEx
  Left = 244
  Top = 337
  Caption = 'Matriz de Funciones'
  ClientHeight = 185
  ClientWidth = 491
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 7
    Top = 51
    Width = 112
    Height = 13
    Caption = 'Grupo de Competencia:'
  end
  object Label2: TLabel [1]
    Left = 54
    Top = 73
    Width = 65
    Height = 13
    Caption = 'Competencia:'
  end
  object ZPerfil: TZetaTextBox [2]
    Left = 121
    Top = 49
    Width = 273
    Height = 17
    AutoSize = False
    Caption = 'ZPerfil'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object Label3: TLabel [3]
    Left = 92
    Top = 97
    Width = 27
    Height = 13
    Caption = 'Nivel:'
  end
  object Label4: TLabel [4]
    Left = 92
    Top = 121
    Width = 27
    Height = 13
    Caption = 'Peso:'
  end
  inherited PanelBotones: TPanel
    Top = 149
    Width = 491
    TabOrder = 3
    inherited OK_DevEx: TcxButton
      Left = 326
      Top = 4
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 405
      Top = 4
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 491
    TabOrder = 5
    inherited ValorActivo2: TPanel
      Width = 165
      inherited textoValorActivo2: TLabel
        Width = 159
      end
    end
  end
  object CC_CODIGO: TZetaDBKeyLookup_DevEx [8]
    Left = 121
    Top = 69
    Width = 300
    Height = 21
    Opcional = False
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 0
    TabStop = True
    WidthLlave = 60
    OnValidKey = CC_CODIGOValidKey
    DataField = 'CC_CODIGO'
    DataSource = DataSource
  end
  object PC_PESO: TZetaDBNumero [9]
    Left = 121
    Top = 117
    Width = 60
    Height = 21
    Mascara = mnTasa
    TabOrder = 2
    Text = '0.0 %'
    DataField = 'PC_PESO'
    DataSource = DataSource
  end
  object NC_NIVEL: TZetaDBKeyLookup_DevEx [10]
    Left = 121
    Top = 93
    Width = 300
    Height = 21
    Opcional = False
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 1
    TabStop = True
    WidthLlave = 60
    DataField = 'NC_NIVEL'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 164
    Top = 65534
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
