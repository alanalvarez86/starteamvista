unit FEditCatInvitadores_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls, Buttons,
  DBCtrls, StdCtrls, ZetaFecha, Mask, ZetaNumero,
  ZetaDBTextBox, 
  //(@am): NO NECESARIO PARA VALIDACION DE COMPONENTES
  {$ifndef DOS_CAPAS} {$ifndef INTERFAZ_SY}FEnrolamiento_DevEx,{$endif} {$endif}ZAccesosMgr,
  ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  TressMorado2013,
  cxControls, dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons,
  ZetaKeyLookup_DevEx, dxSkinsDefaultPainters, dxSkinsdxBarPainter,
  cxContainer, cxEdit, cxGroupBox, cxCheckBox, cxDBEdit ;

type
  TEditCatInvitadores_DevEx = class(TBaseEdicion_DevEx)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    CB_CODIGO: TZetaDBKeyLookup_DevEx;
    IV_NOMBRE: TDBEdit;
    IV_ACTIVO: TDBCheckBox;
    Topes: TGroupBox;
    Fechas: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    IV_FEC_INI: TZetaDBFecha;
    IV_FEC_FIN: TZetaDBFecha;
    Label4: TLabel;
    IV_TOPE_1: TZetaDBNumero;
    Label7: TLabel;
    IV_TOPE_2: TZetaDBNumero;
    Label8: TLabel;
    IV_TOPE_3: TZetaDBNumero;
    Label9: TLabel;
    IV_TOPE_4: TZetaDBNumero;
    IV_TOPE_5: TZetaDBNumero;
    Label10: TLabel;
    Label11: TLabel;
    IV_TOPE_6: TZetaDBNumero;
    Label12: TLabel;
    IV_TOPE_7: TZetaDBNumero;
    IV_TOPE_8: TZetaDBNumero;
    Label13: TLabel;
    Label14: TLabel;
    IV_TOPE_9: TZetaDBNumero;
    IV_CODIGO: TZetaDBNumero;
    Label15: TLabel;
    IV_ID_NUM: TDBEdit;
    GBInvitadores: TGroupBox;
    gbBiometrico: TGroupBox; // BIOMETRICO
    lblNumeroBiometrico: TLabel; // BIOMETRICO
    lblCB_GP_COD: TLabel; // BIOMETRICO
    IV_ID_BIO: TZetaDBTextBox;
    IV_ID_GPO: TZetaDBKeyLookup_DevEx;
    btnAsignarBio: TcxButton;
    btnBorrarBio: TcxButton;
    btnEnrolar: TcxButton;
    btnBorrarHuellas: TcxButton; // BIOMETRICO
    lblCantidadHuellas: TLabel;
    CANTIDAD_HUELLAS: TZetaDBTextBox;
    GroupBox1: TGroupBox;
    procedure FormCreate(Sender: TObject);
    procedure IV_FEC_FINExit(Sender: TObject);
    procedure IV_ID_NUMExit(Sender: TObject);
    procedure btnAsignarBioClick(Sender: TObject); // BIOMETRICO
    procedure btnEnrolarClick(Sender: TObject); // BIOMETRICO
    procedure btnBorrarHuellasClick(Sender: TObject); // BIOMETRICO
    procedure btnBorrarBioClick(Sender: TObject); // BIOMETRICO
    procedure DataSourceDataChange(Sender: TObject; Field: TField); //BIOMETRICO
    procedure FormShow(Sender: TObject); // BIOMETRICO
  private
  protected
    procedure Connect; override;
    procedure DoLookup; override;
{$ifndef DOS_CAPAS}
  public
    procedure RegistroBiometrico(lTieneRegistro : Boolean); // BIOMETRICO
{$endif}
  end;

  const
    K_FILTRAR_HUELLA_INVITADOR = 1;
var
  EditCatInvitadores_DevEx: TEditCatInvitadores_DevEx;

implementation

uses dCatalogos, dCliente, ZetaCommonClasses, ZetaCommonTools, ZAccesosTress,
     ZetaDialogo, dSistema, ZetaBuscaEntero_DevEx;

{$R *.DFM}

procedure TEditCatInvitadores_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H60662_invitadores;
     IndexDerechos := D_CAT_CAFE_INVITADORES;
     FirstControl := IV_CODIGO;
     CB_CODIGO.LookupDataset := dmCliente.cdsEmpleadoLookUp;

     // BIOMETRICO
     {$ifndef DOS_CAPAS}
     IV_ID_GPO.DataField := 'IV_ID_GPO'; 
     IV_ID_BIO.DataField := 'IV_ID_BIO'; 

     with dmSistema do
     begin
          cdsListaGrupos.Conectar; 
          IV_ID_GPO.LookUpDataSet := dmSistema.cdsListaGrupos;
     end;

     gbBiometrico.Visible := True;
     {$endif}
end;

procedure TEditCatInvitadores_DevEx.FormShow(Sender: TObject);
begin
  inherited;
     {$ifndef DOS_CAPAS}
     lblNumeroBiometrico.Enabled := ZAccesosMgr.CheckDerecho( D_CAT_CAFE_INVITADORES, K_DERECHO_ADICIONAL11 );
     lblNumeroBiometrico.Visible := True;
     IV_ID_BIO.Enabled := lblNumeroBiometrico.Enabled;
     IV_ID_GPO.Enabled := lblNumeroBiometrico.Enabled;
     lblCB_GP_COD.Enabled := lblNumeroBiometrico.Enabled;
     btnAsignarBio.Enabled := lblNumeroBiometrico.Enabled;
     btnBorrarBio.Enabled := lblNumeroBiometrico.Enabled;

     //Tiene huella?
     //chkTieneHuella.Visible := True;
     //chkTieneHuella.Checked := dmSistema.TieneHuellaInvitador( dmCatalogos.cdsInvitadores.FieldByName( 'IV_CODIGO' ).AsInteger );

     // Cantidad de huellas.
     lblCantidadHuellas.Enabled := ZAccesosMgr.CheckDerecho( D_CAT_CAFE_INVITADORES, K_DERECHO_ADICIONAL11 ); // SYNERGY
     lblCantidadHuellas.Visible := TRUE;
     CANTIDAD_HUELLAS.Visible := TRUE; // SYNERGY
     CANTIDAD_HUELLAS.Enabled := lblCantidadHuellas.Enabled; // SYNERGY
     CANTIDAD_HUELLAS.Caption := IntToStr (
        dmSistema.CantidadHuellaEmpleado( DataSource.DataSet.FieldByName( 'CB_CODIGO' ).AsInteger ,
        dmCatalogos.cdsInvitadores.FieldByName( 'IV_ID_BIO' ).AsInteger,
        dmCatalogos.cdsInvitadores.FieldByName( 'IV_CODIGO' ).AsInteger));

     {$endif}
end;

procedure TEditCatInvitadores_DevEx.Connect;
begin
     dmCliente.cdsEmpleado.Conectar;
     Datasource.Dataset := dmCatalogos.cdsInvitadores;
end;

procedure TEditCatInvitadores_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscaEntero_DevEx.BuscarCodigo( 'N�mero', 'Invitador', 'IV_CODIGO', dmCatalogos.cdsInvitadores );
end;

procedure TEditCatInvitadores_DevEx.IV_FEC_FINExit(Sender: TObject);
begin
     {if ( IV_FEC_FIN.Valor < IV_FEC_INI.Valor ) then
     begin
          ZetaDialogo.ZError( 'Operaci�n No V�lida', 'La Fecha Final No Puede Ser Menor Que la Fecha Inicial ', 0 );
     end;}
end;

procedure TEditCatInvitadores_DevEx.IV_ID_NUMExit(Sender: TObject);
var
   sTarjeta: string;
begin
     sTarjeta := ZetaCommonTools.PreparaGafeteProximidad( IV_ID_NUM.Text );
     with dmCatalogos.cdsInvitadores do
     begin
          if ( sTarjeta <> FieldByName( 'IV_ID_NUM' ).AsString ) then
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'IV_ID_NUM' ).AsString := sTarjeta;
          end;
     end;
end;

// BIOMETRICO
procedure TEditCatInvitadores_DevEx.btnAsignarBioClick(Sender: TObject);
begin
  inherited;
     {$ifndef DOS_CAPAS}
     with dmCatalogos.cdsInvitadores do
     begin
          if not( State in [ dsEdit, dsInsert ] )then
             Edit;
          FieldByName( 'IV_ID_BIO' ).AsInteger := dmSistema.ObtenMaxIdBio;
          IV_ID_GPO.Enabled := ( FieldByName( 'IV_ID_BIO' ).AsInteger <> 0 );
     end;
     {$endif}
end;

// BIOMETRICO
procedure TEditCatInvitadores_DevEx.btnEnrolarClick(Sender: TObject);
begin
     inherited;
     //Levanta Ventana de Enrola Huellas
{$ifndef DOS_CAPAS}
     {$ifndef INTERFAZ_SY}
     with dmCatalogos.cdsInvitadores do
     begin
          if not( State in [ dsEdit, dsInsert ] )then
          begin
               with TEnrolamiento_DevEx.Create( Self ) do
               begin
                    try

                       dmSistema.IdBiometrico := FieldByName( 'IV_ID_BIO' ).AsInteger;
                                           
                       Empleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                       Invitador := FieldByName( 'IV_CODIGO' ).AsInteger;

                       ShowModal;
                       //chkTieneHuella.Checked := dmSistema.TieneHuellaInvitador( FieldByName( 'IV_CODIGO' ).AsInteger );
                       CANTIDAD_HUELLAS.Caption := IntToStr (
                            dmSistema.CantidadHuellaEmpleado( Empleado, dmSistema.IdBiometrico, Invitador));
                    finally
                           Free;
                    end;
               end;
          end;
     end;
     {$endif}
{$endif}
end;

// BIOMETRICO
procedure TEditCatInvitadores_DevEx.btnBorrarHuellasClick(Sender: TObject);
{$ifndef DOS_CAPAS}
var
   sHeader : String;
{$endif}
begin
  inherited;
{$ifndef DOS_CAPAS}
      sHeader := 'Eliminaci�n de Huellas';
      with dmSistema do
      begin
           if ( ZConfirm( sHeader, 'Se eliminar�n todas las huellas del invitador.' + CR_LF + '�Desea continuar?',0,mbNo ) ) then
           begin
                EliminaHuella( 0, dmCatalogos.cdsInvitadores.FieldByName( 'IV_CODIGO' ).AsInteger, K_TERMINAL_HUELLA );
                DeleteFile( LocalBioDbPath );
                //chkTieneHuella.Checked := TieneHuellaInvitador( dmCatalogos.cdsInvitadores.FieldByName( 'IV_CODIGO' ).AsInteger );
                CANTIDAD_HUELLAS.Caption := IntToStr (
                    dmSistema.CantidadHuellaEmpleado( DataSource.DataSet.FieldByName( 'CB_CODIGO' ).AsInteger ,
                                                      dmCatalogos.cdsInvitadores.FieldByName( 'IV_ID_BIO' ).AsInteger,
                                                      dmCatalogos.cdsInvitadores.FieldByName( 'IV_CODIGO' ).AsInteger));
           end;
      end;
{$endif}
end;

// BIOMETRICO
procedure TEditCatInvitadores_DevEx.btnBorrarBioClick(Sender: TObject);
var bContinuar: Boolean;
begin
     inherited;
     {$ifndef DOS_CAPAS}
     bContinuar := TRUE;

     {if chkTieneHuella.Checked then
     begin
          if not ( ZConfirm( 'Borrar ID biom�trico', 'El invitador tiene huellas asignadas.' + CR_LF +
                                          '�Desea continuar?',0, mbNo ) )then
          begin
               bContinuar := FALSE;
          end;
     end;}

     if bContinuar then
     begin
          with dmCatalogos.cdsInvitadores do
          begin
               if not( State in [ dsEdit, dsInsert ] )then
                  Edit;
               FieldByName( 'IV_ID_BIO' ).AsInteger := 0;
               FieldByName( 'IV_ID_GPO' ).AsString := VACIO;
          end;
          IV_ID_GPO.Enabled := False;
     end;

     {$endif}
end;


{$ifndef DOS_CAPAS}
// BIOMETRICO
procedure TEditCatInvitadores_DevEx.RegistroBiometrico(lTieneRegistro: Boolean);
begin
     //Derechos de Acceso
     BtnEnrolar.Enabled := (lTieneRegistro and ZAccesosMgr.CheckDerecho( D_CAT_CAFE_INVITADORES, K_DERECHO_ADICIONAL12 ) );
     btnBorrarHuellas.Enabled := ( lTieneRegistro and ZAccesosMgr.CheckDerecho( D_CAT_CAFE_INVITADORES, K_DERECHO_ADICIONAL13 ) );
end;
{$endif}

// BIOMETRICO
procedure TEditCatInvitadores_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
{$ifndef DOS_CAPAS}
var
   lBiometrico : Boolean;
{$endif}
begin
     inherited;
     {$ifndef DOS_CAPAS}
     with dmCatalogos.cdsInvitadores do
     begin
          lBiometrico := ( ( FieldByName( 'IV_ID_BIO' ).AsInteger <> 0 ) and ( FieldByName( 'IV_ID_GPO' ).AsString <> VACIO ) and ( State = dsBrowse ) );
          //chkTieneHuella.Checked := dmSistema.TieneHuellaInvitador( FieldByName( 'IV_CODIGO' ).AsInteger );
          RegistroBiometrico( lBiometrico );

          // if State = dsBrowse then
          if ( ( Field = nil ) or ( Field.FieldName = 'IV_CODIGO' ) ) then
          // if FieldByName( 'IV_ID_BIO' ).AsInteger <> 0 then
                CANTIDAD_HUELLAS.Caption := IntToStr (
                    dmSistema.CantidadHuellaEmpleado(
                        FieldByName( 'CB_CODIGO' ).AsInteger ,
                        FieldByName( 'IV_ID_BIO' ).AsInteger,
                        FieldByName( 'IV_CODIGO' ).AsInteger));
     end;
     {$endif}
end;

end.
