unit FEditCatCompetencias_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicionRenglon_DevEx, ZetaNumero, ZetaDBTextBox, StdCtrls, 
  Mask, ZetaFecha, DBCtrls, Db,
  {$ifdef CAJAAHORRO}
  dCajaAhorro,
  {$else}
  dRecursos,
  {$endif}
  Grids, DBGrids, ZetaDBGrid, ComCtrls, ExtCtrls, Buttons, ZetaKeyCombo,
  ZetaMessages, ZetaEdit, ZetaClientDataSet,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore, cxControls, dxSkinscxPCPainter, cxPCdxBarPopupMenu,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, ImgList, cxPC,
  cxNavigator, cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx, cxContainer,
  cxEdit, cxTextEdit, cxMemo, cxDBEdit, TressMorado2013;

type
  TEditCompetencias_DevEx = class(TBaseEdicionRenglon_DevEx)
    DedLbl: TLabel;
    Label8: TLabel;
    btnBuscarEmp_old: TSpeedButton;
    CC_ELEMENT: TDBEdit;
    Label4: TLabel;
    General: TcxTabSheet;
    Label5: TLabel;
    Cursos: TcxTabSheet;
    Revisiones: TcxTabSheet;
    gridCursos: TZetaDBGrid;
    SpeedButton1_old: TSpeedButton;
    Panel3: TPanel;
    gridRevisiones: TZetaDBGrid;
    SpeedButton2_old: TSpeedButton;
    Panel4: TPanel;
    dsCursos: TDataSource;
    dsRevisiones: TDataSource;
    CC_ACTIVO: TDBCheckBox;
    lookTCompetencias: TZetaDBKeyLookup_DevEx;
    zFecha: TZetaDBFecha;
    CC_CODIGO: TZetaDBEdit;
    Panel6: TPanel;
    Label7: TLabel;
    CC_TEXTO: TDBEdit;
    CC_NUMERO: TZetaDBNumero;
    Label1: TLabel;
    CC_INGLES: TDBEdit;
    Label3: TLabel;
    TN_CODIGO: TZetaDBKeyLookup_DevEx;
    Label2: TLabel;
    GroupBox1: TGroupBox;
    CC_DETALLE: TDBMemo;
    bAgregaCurso: TcxButton;
    bBorraCurso: TcxButton;
    bModifCurso: TcxButton;
    bAgregaRevision: TcxButton;
    bBorraRev: TcxButton;
    bModifRev: TcxButton;
    btnBuscarEmp: TcxButton;
    SpeedButton1: TcxButton;
    SpeedButton2: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure bAgregaCursoClick(Sender: TObject);
    procedure bAgregaRevisionClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure BBModificarClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure BBBorrarClick(Sender: TObject);
    procedure gridRevisionesColExit(Sender: TObject);
    procedure gridRevisionesDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure bModifRevClick(Sender: TObject);
    procedure bBorraRevClick(Sender: TObject);
    procedure bModifCursoClick(Sender: TObject);
    procedure bBorraCursoClick(Sender: TObject);
    procedure gridCursosDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BuscarBtnClick(Sender: TObject);
    procedure gridCursosColExit(Sender: TObject);
    procedure dxBarButton_BuscarBtnClick(Sender: TObject);
    procedure PageControlChange(Sender: TObject);
  private
    procedure AplicaFiltro;
    procedure HabilitaControls(const lHabilita: Boolean);
    procedure AgregarDatos(Datos:TDataSet;iForma:Integer);
    procedure BorrarDatos(Datos: TZetaClientDataSet);


    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure KeyPress( var Key: Char ); override; { TWinControl }
    procedure KeyDown(var Key: Word; Shift: TShiftState);override;
  public
    { Public declarations }

  end;

var
  EditCompetencias_DevEx: TEditCompetencias_DevEx;

implementation

uses ZetaCommonClasses,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaDialogo,
     ZetaTipoEntidad,
     ZAccesosTress,
     dSistema,
     ZAccesosMgr,
     dCatalogos,
     ZetaBusqueda_DevEx,
     dTablas, DBClient;
{$R *.DFM}


procedure TEditCompetencias_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsCompetencias.Conectar;
          Datasource.Dataset := cdsCompetencias;
          dsRenglon.DataSet := cdsCompNiveles;
          dsCursos.DataSet := cdsCompCursos;
          dsRevisiones.DataSet  := cdsCompRevisiones;

     end;
     with dmTablas do
     begin
          cdsTNacComp.Conectar;
          lookTCompetencias.LookupDataset := cdsTCompetencias;
          TN_CODIGO.LookupDataset := cdsTNacComp;
     end;
end;

procedure TEditCompetencias_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= ZetaCommonClasses.H_Cat_Competencias;
     IndexDerechos := ZAccesosTress.D_CAT_COMPETENCIAS;
     FirstControl := CC_CODIGO;
end;

procedure TEditCompetencias_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage := General;
     dxBarButton_BuscarBtn.Enabled := false;
end;


procedure TEditCompetencias_DevEx.AgregarDatos(Datos:TDataSet;iForma:Integer);
begin
     IF ( ZAccesosMgr.CheckDerecho( D_CAT_COMPETENCIAS , K_DERECHO_CAMBIO ) ) then
     begin
          Datos.Append;
     end
     else
         ZInformation('Competencia','No Tiene Permiso Para Modificar Registro',0);
end;


procedure TEditCompetencias_DevEx.BBAgregarClick(Sender: TObject);
begin
     AgregarDatos(dsRenglon.DataSet,1);
     GridRenglones.SetFocus;
end;

procedure TEditCompetencias_DevEx.BBModificarClick(Sender: TObject);
begin
     //inherited;
     IF ( ZAccesosMgr.CheckDerecho( D_CAT_COMPETENCIAS , K_DERECHO_CAMBIO ) ) THEN
     begin
          dsRenglon.DataSet.Edit;
          GridRenglones.SetFocus;
     end
     else
         ZInformation('Competencias','No Tiene Permiso Para Modificar Registros',0);
end;

procedure TEditCompetencias_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
     Procedure HabilitaDetalles( bHabilita : Boolean );
     begin
          BBAgregar_DevEx.Enabled := bHabilita;
          BBBorrar_DevEx.Enabled := bHabilita;
          BBModificar_DevEx.Enabled := bHabilita;
          bAgregaCurso.Enabled := bHabilita;
          bModifCurso.Enabled := bHabilita;
          bModifRev.Enabled := bHabilita;
          bBorraCurso.Enabled := bHabilita;
          bBorraRev.Enabled := bHabilita;
          bAgregaRevision.Enabled := bHabilita;
          gridCursos.Enabled := bHabilita;
          gridRevisiones.Enabled := bHabilita;
          GridRenglones.Enabled := bHabilita;
     end;
begin
     inherited;
     if Field = nil then
     begin
          with dmCatalogos.cdsCompetencias do
          begin
               if State = dsBrowse then
               begin
                    AplicaFiltro; 
                    HabilitaControls( ZAccesosMgr.CheckDerecho( D_CAT_COMPETENCIAS , K_DERECHO_CAMBIO )  );
                    HabilitaDetalles( True );
               end;

               if State = dsInsert then
               begin
                    AplicaFiltro; 
                    HabilitaControls( ZAccesosMgr.CheckDerecho( D_CAT_COMPETENCIAS , K_DERECHO_ALTA )  );
                    HabilitaDetalles( False );
               end;
          end;
     end;
end;

procedure TEditCompetencias_DevEx.AplicaFiltro;
     procedure AplicandoFiltro(Datos:TZetaClientDataSet);
     begin
          with Datos do
          begin
               Filtered := False;
               Filter := Format('CC_CODIGO = ''%s''',[dmCatalogos.cdsCompetencias.FieldByName('CC_CODIGO').AsString]);
               Filtered := True;
          end;
     end;
begin
     with dmCatalogos do
     begin
          AplicandoFiltro(cdsCompNiveles);
          AplicandoFiltro(cdsCompCursos);
          AplicandoFiltro(cdsCompRevisiones);
     end;
end;


procedure TEditCompetencias_DevEx.HabilitaControls(const lHabilita:Boolean);
begin
     CC_CODIGO.Enabled := lHabilita;
     CC_NUMERO.Enabled := lHabilita;
     CC_ELEMENT.Enabled := lHabilita;
     CC_INGLES.Enabled := lHabilita;
     CC_TEXTO.Enabled := lHabilita;
     CC_DETALLE.Enabled := lHabilita;
     CC_ACTIVO.Enabled := lHabilita;
     gridRevisiones.Enabled := lHabilita;
     gridCursos.Enabled := lHabilita;
     GridRenglones.Enabled := lHabilita;
     lookTCompetencias.Enabled := lHabilita;
end;

procedure TEditCompetencias_DevEx.BBBorrarClick(Sender: TObject);
begin
     BorrarDatos(dmCatalogos.cdsCompNiveles );
end;

procedure TEditCompetencias_DevEx.bAgregaCursoClick(Sender: TObject);
begin
     //inherited;
     AgregarDatos(dsCursos.DataSet,2);
     GridCursos.SetFocus; 
end;

procedure TEditCompetencias_DevEx.bAgregaRevisionClick(Sender: TObject);
begin
     AgregarDatos(dsRevisiones.DataSet,3);
     GridRevisiones.SetFocus;
end;

procedure TEditCompetencias_DevEx.gridRevisionesColExit(Sender: TObject);
begin
     inherited;
     with gridRevisiones do
     begin
          if ( SelectedField <> nil ) then
          begin
               if ( SelectedField.FieldName = 'RC_FEC_INI' ) and ( zFecha.Visible ) then
                  zFecha.Visible:= FALSE;
          end;
     end;

end;

procedure TEditCompetencias_DevEx.gridRevisionesDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
 procedure AjustaControlEnCelda( oControl: TControl );
   begin
        with oControl do
        begin
             Left := Rect.Left + gridRevisiones.Left;
             Top := Rect.Top + gridRevisiones.Top;
             Width := Column.Width + 2;
             Visible := True;
        end;
   end;
begin
     inherited;
     if ( gdFocused in State ) then
     begin
          if ( Column.FieldName = 'RC_FEC_INI' ) then
             AjustaControlEnCelda( zFecha )
     end;

end;

procedure TEditCompetencias_DevEx.KeyPress(var Key: Char);

   function EsControlGrid: Boolean;
   begin
        Result := ( self.ActiveControl = zFecha );
   end;

begin
     inherited;
     if GridEnfocado then
     begin
          if ( ( Key <> Chr( 9 ) ) and ( Key <> #0 ) ) then
          begin
               if ( gridRevisiones.SelectedField.FieldName = 'RC_FEC_INI' ) then
               begin
                    zFecha.SetFocus;
                    SendMessage( zFecha.Handle, WM_CHAR, Word( Key ), 0 );
                    Key := #0;
               end;
          end;
     end
     else if EsControlGrid and ( ( Key = Chr( VK_RETURN ) ) or ( Key = Chr( 9 ) ) ) then
     begin
          Key := #0;
          with gridRevisiones do
          begin
               SetFocus;
               gridRevisiones.SelectedIndex := PosicionaSiguienteColumna;
          end;
     end;
end;

procedure TEditCompetencias_DevEx.bModifRevClick(Sender: TObject);
begin
     inherited;
     IF ( ZAccesosMgr.CheckDerecho( D_CAT_COMPETENCIAS , K_DERECHO_CAMBIO ) ) THEN
     begin
          dsRevisiones.DataSet.Edit;
          GridRevisiones.SetFocus;
     end
     else
         ZInformation('Competencias','No Tiene Permiso Para Modificar Registros',0);

end;

procedure TEditCompetencias_DevEx.bBorraRevClick(Sender: TObject);
begin
     inherited;
     BorrarDatos(dmCatalogos.cdsCompRevisiones );
end;

procedure TEditCompetencias_DevEx.bModifCursoClick(Sender: TObject);
begin
     inherited;
     IF ( ZAccesosMgr.CheckDerecho( D_CAT_COMPETENCIAS , K_DERECHO_CAMBIO ) ) THEN
     begin
          dsCursos.DataSet.Edit;
          GridCursos.SetFocus;
     end
     else
         ZInformation('Competencias','No Tiene Permiso Para Modificar Registros',0);
end;

Procedure TEditCompetencias_DevEx.BorrarDatos(Datos:TZetaClientDataSet);
begin
     IF ( ZAccesosMgr.CheckDerecho( D_CAT_COMPETENCIAS , K_DERECHO_CAMBIO ) )  THEN
     begin
          if not Datos.IsEmpty then
             Datos.Borrar;
     end
     else
         ZInformation('Competencias','No Tiene Permiso Para Modificar Registros',0);
end;

procedure TEditCompetencias_DevEx.bBorraCursoClick(Sender: TObject);
begin
     inherited;
     BorrarDatos(dmCatalogos.cdsCompCursos );
end;

procedure TEditCompetencias_DevEx.gridCursosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
     inherited;
     if ( gdFocused in State ) then
     begin
          if ( Column.FieldName = 'CU_CODIGO' ) then
          begin
               dxBarButton_BuscarBtn.Enabled:= True;
          end;
     end;
end;

procedure TEditCompetencias_DevEx.BuscarBtnClick(Sender: TObject);
var
   sKey,sDescription:string;
begin
     with dmCatalogos.cdsCompCursos do
     begin
          if ZetaBusqueda_DevEx.ShowSearchForm( dmCatalogos.cdsCursos, VACIO, sKey, sDescription, FALSE, FALSE )then
          begin
               if ( not ( State in [dsEdit] ) ) then
                  Edit;
               FieldByName('CU_CODIGO').AsString := sKey;
          end;
     end;

end;

procedure TEditCompetencias_DevEx.gridCursosColExit(Sender: TObject);
begin
     with gridCursos do
     begin
          if ( SelectedField <> nil ) then
          begin
               if ( dxBarButton_BuscarBtn.enabled =true ) then
                  dxBarButton_BuscarBtn.enabled := false;
          end;
     end;

end;

procedure TEditCompetencias_DevEx.KeyDown(var Key: Word; Shift: TShiftState);
begin
     if ( gridCursos.Focused ) then
     begin
          if ( ssCtrl in Shift ) then { CTRL }
          begin
               case Key of
                    70: { Letra F = Buscar }
                    begin
                         Key := 0;
                         BuscarBtnClick(Self);
                    end;
               end;
          end;

          if ( Key = VK_RETURN ) then
          begin
               Key := 0;
          end;
     end
     else
         inherited KeyDown( Key, Shift );
end;

procedure TEditCompetencias_DevEx.dxBarButton_BuscarBtnClick(
  Sender: TObject);
var
sKey,sDescription:string;
begin
  with dmCatalogos.cdsCompCursos do
     begin
          if ZetaBusqueda_DevEx.ShowSearchForm( dmCatalogos.cdsCursos, VACIO, sKey, sDescription, FALSE, FALSE )then
          begin
               if ( not ( State in [dsEdit] ) ) then
                  Edit;
               FieldByName('CU_CODIGO').AsString := sKey;
          end;
     end;
end;

procedure TEditCompetencias_DevEx.PageControlChange(Sender: TObject);
begin
  inherited;
       dxBarButton_BuscarBtn.enabled := false;
end;

end.
