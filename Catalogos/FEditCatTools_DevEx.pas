unit FEditCatTools_DevEx;

interface

uses Windows, Messages,
 SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Db, ExtCtrls, Buttons,
     DBCtrls, StdCtrls, Mask,
     ZetaNumero,
     ZetaEdit, ZetaKeyCombo, ZBaseEdicion_DevEx,
     cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
     
     cxControls, dxSkinsdxBarPainter, dxBarExtItems,
     dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons,
     dxSkinsCore, TressMorado2013, dxSkinsDefaultPainters;

type
  TEditCatTools_DevEx = class(TBaseEdicion_DevEx)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    TO_CODIGO: TZetaDBEdit;
    TO_DESCRIP: TDBEdit;
    TO_INGLES: TDBEdit;
    TO_NUMERO: TZetaDBNumero;
    TO_TEXTO: TDBEdit;
    MontoLbl: TLabel;
    TO_COSTO: TZetaDBNumero;
    Label4: TLabel;
    TO_VAL_REP: TZetaDBNumero;
    Label7: TLabel;
    Label8: TLabel;
    TO_VAL_BAJ: TZetaDBNumero;
    TO_VIDA: TZetaDBNumero;
    Label9: TLabel;
    Label10: TLabel;
    TO_DESCTO: TZetaDBKeyCombo;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  EditCatTools_DevEx: TEditCatTools_DevEx;

implementation

{$R *.DFM}

uses DCatalogos,
     ZetaCommonClasses,
     ZetaBuscador_DevEx,
     ZAccesosTress;

procedure TEditCatTools_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos :=  D_CAT_HERR_TOOLS;
     HelpContext:= H60663_Herramientas;
     FirstControl := TO_CODIGO;
end;

procedure TEditCatTools_DevEx.Connect;
begin
     DataSource.DataSet := dmCatalogos.cdsTools;
end;

procedure TEditCatTools_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Herramienta', 'Herramienta', 'TO_CODIGO', dmCatalogos.cdsTools );
end;

end.
