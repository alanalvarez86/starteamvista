unit FCatCursos_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Grids, DBGrids, Db, ExtCtrls,
     ZBaseGridLectura_DevEx,
     ZetaDBGrid, StdCtrls, ZBaseConsulta, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, System.Actions;

type
  TCatCursos_DevEx = class(TBaseGridLectura_DevEx)
    CU_CODIGO: TcxGridDBColumn;
    CU_DESCRIP: TcxGridDBColumn;
    CU_REVISIO: TcxGridDBColumn;
    CU_ACTIVO: TcxGridDBColumn;
    CU_STPS: TcxGridDBColumn;
    CU_CLASIFI: TcxGridDBColumn;
    CU_CLASE: TcxGridDBColumn;
    CU_HORAS: TcxGridDBColumn;
    MA_CODIGO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ConfigAgrupamiento;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar;override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  CatCursos_DevEx: TCatCursos_DevEx;

implementation

uses DCatalogos,
     ZetaBuscador_DevEx,
     ZetaCommonClasses;

{$R *.DFM}

{ TCatCursos }

procedure TCatCursos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H60641_Cursos;
end;

procedure TCatCursos_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsCursos.Conectar;
          DataSource.DataSet:= cdsCursos;
     end;
end;

procedure TCatCursos_DevEx.Refresh;
begin
     dmCatalogos.cdsCursos.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatCursos_DevEx.Agregar;
begin
     dmCatalogos.cdsCursos.Agregar;
end;

procedure TCatCursos_DevEx.Borrar;
begin
     dmCatalogos.cdsCursos.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatCursos_DevEx.Modificar;
begin
     dmCatalogos.cdsCursos.Modificar;
end;

procedure TCatCursos_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Curso', 'Curso', 'CU_CODIGO', dmCatalogos.cdsCursos );
end;

procedure TCatCursos_DevEx.ConfigAgrupamiento;
begin
     //Cambio de configuracion
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //Desactiva la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Esconde la caja de agrupamiento
     //Determinar las columnas por las que se podra agrupar
     CU_CODIGO.Options.Grouping:= FALSE;
     CU_DESCRIP.Options.Grouping:= FALSE;
     CU_REVISIO.Options.Grouping:= FALSE;
     CU_ACTIVO.Options.Grouping:= FALSE;
     CU_STPS.Options.Grouping:= FALSE;
     CU_HORAS.Options.Grouping:= FALSE;
end;

procedure TCatCursos_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     {***Banda Sumatoria***}
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
  inherited;
     //DevEx (by am):Configuracion Especial para agrupamiento
     ConfigAgrupamiento;
end;

end.
