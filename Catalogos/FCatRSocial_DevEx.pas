unit FCatRSocial_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, DB, ExtCtrls,
  StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid;

type
  TCatRSocial_DevEx = class(TBaseGridLectura_DevEx)
    RS_CODIGO: TcxGridDBColumn;
    RS_NOMBRE: TcxGridDBColumn;
    RS_RFC: TcxGridDBColumn;
    RS_RLEGAL: TcxGridDBColumn;
    RS_TEL: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  CatRSocial_DevEx: TCatRSocial_DevEx;

implementation

uses dCatalogos,
     ZetaCommonClasses,
     ZetaBuscador_DevEx;

{$R *.dfm}

procedure TCatRSocial_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext:= H_CAT_RAZONES_SOCIALES;
end;

procedure TCatRSocial_DevEx.Connect;
begin
     inherited;
     with dmCatalogos do
     begin
          cdsRSocial.Conectar;
          DataSource.DataSet:= cdsRSocial;
     end;
end;

procedure TCatRSocial_DevEx.Agregar;
begin
     inherited;
     dmCatalogos.cdsRSocial.Agregar;
end;

procedure TCatRSocial_DevEx.Borrar;
begin
     inherited;
     dmCatalogos.cdsRSocial.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatRSocial_DevEx.Modificar;
begin
     inherited;
     dmCatalogos.cdsRSocial.Modificar;
end;

procedure TCatRSocial_DevEx.Refresh;
begin
     inherited;
     dmCatalogos.cdsRSocial.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatRSocial_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', 'Raz�n Social', 'RS_CODIGO', dmCatalogos.cdsRSocial );
end;

procedure TCatRSocial_DevEx.FormShow(Sender: TObject);
begin
 ApplyMinWidth;
  inherited;

end;

end.
