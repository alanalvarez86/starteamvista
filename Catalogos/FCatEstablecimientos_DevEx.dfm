inherited Establecimientos_DevEx: TEstablecimientos_DevEx
  Left = 232
  Top = 262
  Caption = 'Establecimientos'
  ClientWidth = 752
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 752
    inherited ValorActivo2: TPanel
      Width = 493
      inherited textoValorActivo2: TLabel
        Width = 487
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 752
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object ES_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'ES_CODIGO'
        MinWidth = 70
        Width = 70
      end
      object ES_ELEMENT: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'ES_ELEMENT'
        MinWidth = 100
        Width = 224
      end
      object ES_INGLES: TcxGridDBColumn
        Caption = 'Ingl'#233's'
        DataBinding.FieldName = 'ES_INGLES'
        MinWidth = 100
        Width = 119
      end
      object ES_NUMERO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'ES_NUMERO'
        MinWidth = 80
        Width = 80
      end
      object ES_TEXTO: TcxGridDBColumn
        Caption = 'Texto'
        DataBinding.FieldName = 'ES_TEXTO'
        MinWidth = 80
        Width = 251
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
