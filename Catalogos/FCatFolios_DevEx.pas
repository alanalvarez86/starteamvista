unit FCatFolios_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Grids, DBGrids, Db, ExtCtrls,
     ZBaseConsulta,
     ZetaDBGrid, StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TCatFolios_devEx = class(TBaseGridLectura_DevEx)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  CatFolios_devEx: TCatFolios_devEx;

implementation

uses DCatalogos,
     ZetaCommonClasses,
     ZetaCommonLists;

{$R *.DFM}

{ TCatFolios }
{ NOTA : DADO EL TAMA�O REDUCIDO DE ESTE CATALOGO NO SE IMPLEMENTARA BUSQUEDA DE CODIGOS }

procedure TCatFolios_devEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H60624_Folios_recibos;
end;

procedure TCatFolios_devEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsFolios.Conectar;
          DataSource.DataSet := cdsFolios;
     end;
end;

procedure TCatFolios_devEx.Refresh;
begin
     dmCatalogos.cdsFolios.Refrescar;
end;

procedure TCatFolios_devEx.Agregar;
begin
     dmCatalogos.cdsFolios.Agregar;
end;

procedure TCatFolios_devEx.Borrar;
begin
     dmCatalogos.cdsFolios.Borrar;
end;

procedure TCatFolios_devEx.Modificar;
begin
     dmCatalogos.cdsFolios.Modificar;
end;

procedure TCatFolios_devEx.FormShow(Sender: TObject);
begin
 CreaColumaSumatoria(ZetaDbGridDBtableView.Columns[0],0 , '' , SkCount );
  inherited;

end;

end.


