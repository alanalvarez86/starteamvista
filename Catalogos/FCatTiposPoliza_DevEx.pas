unit FCatTiposPoliza_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, DB, ExtCtrls,
  StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid;

type
  TCatTiposPoliza_DevEx = class(TBaseGridLectura_DevEx)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure DoLookup; override;
  protected
    { Public declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;

  end;

var
  CatTiposPoliza_DevEx: TCatTiposPoliza_DevEx;

implementation

uses ZetaDialogo,
     ZImprimeForma,
     ZetaTipoEntidad,
     ZetaCommonClasses,
     ZetaBuscador_DevEx,
     dCatalogos;

{$R *.dfm}

procedure TCatTiposPoliza_DevEx.FormCreate(Sender: TObject);
begin
  inherited;
  CanLookup := True;
  HelpContext:= H_Cat_TiposPoliza;
end;

procedure TCatTiposPoliza_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsTiposPoliza.Conectar;
          DataSource.DataSet := cdsTiposPoliza;
     end;
end;

procedure TCatTiposPoliza_DevEx.Refresh;
begin
     dmCatalogos.cdsTiposPoliza.Refrescar;
end;

procedure TCatTiposPoliza_DevEx.Agregar;
begin
     dmCatalogos.cdsTiposPoliza.Agregar;
end;

procedure TCatTiposPoliza_DevEx.Borrar;
begin
     dmCatalogos.cdsTiposPoliza.Borrar;
end;

procedure TCatTiposPoliza_DevEx.Modificar;
begin
     dmCatalogos.cdsTiposPoliza.Modificar;
end;


procedure TCatTiposPoliza_DevEx.DoLookup;
begin
     ZetaBuscador_DevEx.BuscarCodigo( 'Tipos de P�liza', 'Tipos de P�liza', 'PT_CODIGO', dmCatalogos.cdsTiposPoliza );
end;

procedure TCatTiposPoliza_DevEx.FormShow(Sender: TObject);
begin
  CreaColumaSumatoria(ZetaDbGridDBTableView.Columns[0],0,'',SkCount );
  inherited;

end;

end.
