unit FCatEstablecimientos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, DB, ExtCtrls,
  StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid;

type
  TEstablecimientos_DevEx = class(TBaseGridLectura_DevEx)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  end;

var
  Establecimientos_DevEx: TEstablecimientos_DevEx;

implementation

uses
     ZImprimeForma,
     ZetaTipoEntidad,
     ZetaCommonClasses, dCatalogos;

{$R *.dfm}

{ TEstablecimientos }

procedure TEstablecimientos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_CAT_ESTABLECIMIENTOS;
end;

procedure TEstablecimientos_DevEx.Agregar;
begin
     inherited;
     dmCatalogos.cdsEstablecimientos.Agregar;
end;

procedure TEstablecimientos_DevEx.Borrar;
begin
     inherited;
     dmCatalogos.cdsEstablecimientos.Borrar;
end;

procedure TEstablecimientos_DevEx.Connect;
begin
     inherited;
     with dmCatalogos do
     begin
          cdsEstablecimientos.Conectar;
          DataSource.DataSet := cdsEstablecimientos;
     end;
end;

procedure TEstablecimientos_DevEx.Modificar;
begin
     inherited;
     dmCatalogos.cdsEstablecimientos.Modificar;
end;

procedure TEstablecimientos_DevEx.Refresh;
begin
     inherited;
     dmCatalogos.cdsEstablecimientos.Conectar;
end;

procedure TEstablecimientos_DevEx.FormShow(Sender: TObject);
begin
CreaColumaSumatoria(ZetaDBGridDBTAbleView.Columns[0],0 ,'' ,SkCount ); 
  inherited;

end;

end.
