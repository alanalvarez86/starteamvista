inherited NomParams_DevEx: TNomParams_DevEx
  Left = 196
  Top = 106
  Caption = 'Par'#225'metros de N'#243'mina'
  ClientHeight = 282
  ExplicitHeight = 282
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    ExplicitWidth = 0
    inherited Slider: TSplitter
      Left = 137
      ExplicitLeft = 137
    end
    inherited ValorActivo1: TPanel
      Width = 121
      ExplicitWidth = 121
      inherited textoValorActivo1: TLabel
        Width = 115
      end
    end
    inherited ValorActivo2: TPanel
      Left = 140
      Width = 484
      ExplicitLeft = 140
      ExplicitWidth = 484
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 478
        ExplicitLeft = 398
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Height = 263
    ExplicitWidth = 0
    ExplicitHeight = 262
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object NP_FOLIO: TcxGridDBColumn
        Caption = 'Folio'
        DataBinding.FieldName = 'NP_FOLIO'
        MinWidth = 60
        Options.Grouping = False
        Width = 60
      end
      object NP_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'NP_NOMBRE'
        MinWidth = 80
        Options.Grouping = False
        Width = 80
      end
      object NP_ACTIVO: TcxGridDBColumn
        Caption = 'Activo'
        DataBinding.FieldName = 'NP_ACTIVO'
        MinWidth = 70
        Width = 70
      end
      object NP_FORMULA: TcxGridDBColumn
        Caption = 'F'#243'rmula'
        DataBinding.FieldName = 'NP_FORMULA'
        MinWidth = 100
        Options.Filtering = False
        Options.Grouping = False
        Options.Sorting = False
        Width = 100
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
