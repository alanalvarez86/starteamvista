inherited CatMatrizPerfilComps_DevEx: TCatMatrizPerfilComps_DevEx
  Left = 248
  Top = 228
  Caption = 'Matriz de Funciones'
  ClientHeight = 326
  ClientWidth = 926
  ExplicitWidth = 320
  ExplicitHeight = 240
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 926
    TabOrder = 2
    ExplicitWidth = 926
    inherited Slider: TSplitter
      Left = 378
      ExplicitLeft = 378
    end
    inherited ValorActivo1: TPanel
      Width = 362
      ExplicitWidth = 362
    end
    inherited ValorActivo2: TPanel
      Left = 381
      Width = 545
      ExplicitLeft = 381
      ExplicitWidth = 545
      inherited textoValorActivo2: TLabel
        Left = 735
        ExplicitLeft = 735
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 57
    Width = 926
    Height = 269
    ExplicitTop = 57
    ExplicitWidth = 926
    ExplicitHeight = 269
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CC_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CC_CODIGO'
        MinWidth = 70
        Width = 70
      end
      object CC_ELEMENT: TcxGridDBColumn
        Caption = 'Competencia'
        DataBinding.FieldName = 'CC_ELEMENT'
        MinWidth = 260
        Width = 263
      end
      object NC_DESCRIP: TcxGridDBColumn
        Caption = 'Nivel'
        DataBinding.FieldName = 'NC_DESCRIP'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        MinWidth = 70
      end
      object RP_REVISIO: TcxGridDBColumn
        Caption = 'Revisi'#243'n'
        DataBinding.FieldName = 'RP_REVISIO'
        MinWidth = 70
      end
      object PC_PESO: TcxGridDBColumn
        Caption = 'Peso'
        DataBinding.FieldName = 'PC_PESO'
        MinWidth = 70
        Width = 70
      end
    end
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 19
    Width = 926
    Height = 38
    Align = alTop
    TabOrder = 0
    object lblCatalogo: TLabel
      Left = 8
      Top = 13
      Width = 117
      Height = 13
      Alignment = taRightJustify
      Caption = 'Grupo de Competencias:'
    end
    object lookUpCatalogo: TZetaKeyLookup_DevEx
      Left = 129
      Top = 9
      Width = 300
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      OnValidKey = lookUpCatalogoValidKey
    end
  end
  inherited DataSource: TDataSource
    Left = 768
    Top = 24
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
