inherited CatOtrasPer_DevEx: TCatOtrasPer_DevEx
  Left = 126
  Top = 157
  Caption = 'Percepciones Fijas'
  PixelsPerInch = 96
  TextHeight = 13
  inherited ZetaDBGrid: TZetaCXGrid
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object TB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'TB_CODIGO'
      end
      object TB_ELEMENT: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'TB_ELEMENT'
      end
      object TB_TIPO: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'TB_TIPO'
      end
      object TB_MONTO: TcxGridDBColumn
        Caption = 'Monto Diario'
        DataBinding.FieldName = 'TB_MONTO'
      end
      object TB_TASA: TcxGridDBColumn
        Caption = 'Porcentaje'
        DataBinding.FieldName = 'TB_TASA'
      end
      object TB_IMSS: TcxGridDBColumn
        Caption = 'IMSS'
        DataBinding.FieldName = 'TB_IMSS'
      end
      object TB_ISPT: TcxGridDBColumn
        Caption = 'ISPT'
        DataBinding.FieldName = 'TB_ISPT'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
