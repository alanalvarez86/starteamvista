inherited EditCatTools_DevEx: TEditCatTools_DevEx
  Left = 279
  Top = 174
  Caption = 'Herramienta'
  ClientHeight = 304
  ClientWidth = 417
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 69
    Top = 41
    Width = 36
    Height = 13
    Caption = 'C'#243'digo:'
  end
  object Label2: TLabel [1]
    Left = 46
    Top = 63
    Width = 59
    Height = 13
    Caption = 'Descripci'#243'n:'
  end
  object Label3: TLabel [2]
    Left = 74
    Top = 86
    Width = 31
    Height = 13
    Caption = 'Ingl'#233's:'
  end
  object Label5: TLabel [3]
    Left = 65
    Top = 108
    Width = 40
    Height = 13
    Caption = 'N'#250'mero:'
  end
  object Label6: TLabel [4]
    Left = 75
    Top = 130
    Width = 30
    Height = 13
    Caption = 'Texto:'
  end
  object MontoLbl: TLabel [5]
    Left = 36
    Top = 152
    Width = 69
    Height = 13
    Alignment = taRightJustify
    Caption = 'Costo Unitario:'
  end
  object Label4: TLabel [6]
    Left = 22
    Top = 174
    Width = 83
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Reposici'#243'n:'
  end
  object Label7: TLabel [7]
    Left = 63
    Top = 218
    Width = 42
    Height = 13
    Alignment = taRightJustify
    Caption = 'Vida Util:'
  end
  object Label8: TLabel [8]
    Left = 39
    Top = 196
    Width = 66
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor en Baja:'
  end
  object Label9: TLabel [9]
    Left = 194
    Top = 218
    Width = 23
    Height = 13
    Caption = 'D'#237'as'
  end
  object Label10: TLabel [10]
    Left = 11
    Top = 239
    Width = 94
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo de Descuento:'
  end
  inherited PanelBotones: TPanel
    Top = 268
    Width = 417
    TabOrder = 10
    inherited OK_DevEx: TcxButton
      Left = 253
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 332
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 417
    TabOrder = 12
    inherited Splitter: TSplitter
      Left = 209
    end
    inherited ValorActivo1: TPanel
      Width = 209
      inherited textoValorActivo1: TLabel
        Width = 203
      end
    end
    inherited ValorActivo2: TPanel
      Left = 212
      Width = 205
      inherited textoValorActivo2: TLabel
        Width = 199
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 16
  end
  object TO_CODIGO: TZetaDBEdit [14]
    Left = 113
    Top = 37
    Width = 75
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 0
    ConfirmEdit = True
    DataField = 'TO_CODIGO'
    DataSource = DataSource
  end
  object TO_DESCRIP: TDBEdit [15]
    Left = 113
    Top = 59
    Width = 274
    Height = 21
    DataField = 'TO_DESCRIP'
    DataSource = DataSource
    TabOrder = 1
  end
  object TO_INGLES: TDBEdit [16]
    Left = 113
    Top = 82
    Width = 274
    Height = 21
    DataField = 'TO_INGLES'
    DataSource = DataSource
    TabOrder = 2
  end
  object TO_NUMERO: TZetaDBNumero [17]
    Left = 113
    Top = 104
    Width = 121
    Height = 21
    Mascara = mnNumeroGlobal
    TabOrder = 3
    Text = '0.00'
    UseEnterKey = True
    DataField = 'TO_NUMERO'
    DataSource = DataSource
  end
  object TO_TEXTO: TDBEdit [18]
    Left = 113
    Top = 126
    Width = 274
    Height = 21
    DataField = 'TO_TEXTO'
    DataSource = DataSource
    TabOrder = 4
  end
  object TO_COSTO: TZetaDBNumero [19]
    Left = 113
    Top = 148
    Width = 121
    Height = 21
    Mascara = mnPesos
    TabOrder = 5
    Text = '0.00'
    UseEnterKey = True
    DataField = 'TO_COSTO'
    DataSource = DataSource
  end
  object TO_VAL_REP: TZetaDBNumero [20]
    Left = 113
    Top = 170
    Width = 121
    Height = 21
    Mascara = mnPesos
    TabOrder = 6
    Text = '0.00'
    UseEnterKey = True
    DataField = 'TO_VAL_REP'
    DataSource = DataSource
  end
  object TO_VAL_BAJ: TZetaDBNumero [21]
    Left = 113
    Top = 192
    Width = 121
    Height = 21
    Mascara = mnPesos
    TabOrder = 7
    Text = '0.00'
    UseEnterKey = True
    DataField = 'TO_VAL_BAJ'
    DataSource = DataSource
  end
  object TO_VIDA: TZetaDBNumero [22]
    Left = 113
    Top = 214
    Width = 72
    Height = 21
    Mascara = mnDias
    TabOrder = 8
    Text = '0'
    UseEnterKey = True
    DataField = 'TO_VIDA'
    DataSource = DataSource
  end
  object TO_DESCTO: TZetaDBKeyCombo [23]
    Left = 113
    Top = 236
    Width = 274
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 9
    ListaFija = lfDescuentoTools
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'TO_DESCTO'
    DataSource = DataSource
    LlaveNumerica = True
  end
  inherited DataSource: TDataSource
    Left = 352
    Top = 172
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_BuscarBtn: TdxBarButton
      Visible = ivAlways
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
