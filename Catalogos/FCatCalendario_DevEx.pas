unit FCatCalendario_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Grids, DBGrids, Db, ExtCtrls,
     ZBaseGridLectura_DevEx,
     ZetaDBGrid, StdCtrls, ZBaseConsulta, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TCatCalendario_DevEx = class(TBaseGridLectura_DevEx)
    CU_CODIGO: TcxGridDBColumn;
    CU_NOMBRE: TcxGridDBColumn;
    CU_REVISIO: TcxGridDBColumn;
    CC_FECHA: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  CatCalendario_DevEx: TCatCalendario_DevEx;

implementation

uses DCatalogos,
     ZetaBuscador_DevEx,
     ZetaCommonClasses;

{$R *.DFM}

{ TCatCalendario }

procedure TCatCalendario_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H60645_Calendario;
end;

procedure TCatCalendario_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsCursos.Conectar;
          cdsCalendario.Conectar;
          DataSource.DataSet:= cdsCalendario;
     end;
end;

procedure TCatCalendario_DevEx.Refresh;
begin
     dmCatalogos.cdsCalendario.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatCalendario_DevEx.Agregar;
begin
     dmCatalogos.cdsCalendario.Agregar;
end;

procedure TCatCalendario_DevEx.Borrar;
begin
     dmCatalogos.cdsCalendario.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatCalendario_DevEx.Modificar;
begin
     dmCatalogos.cdsCalendario.Modificar;
end;

procedure TCatCalendario_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Curso', 'Calendario de Cursos', 'CU_CODIGO', dmCatalogos.cdsCalendario );
end;

procedure TCatCalendario_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     {***Banda Sumatoria***}
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
     inherited;
end;

end.
