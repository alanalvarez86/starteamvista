unit FEditCatOtrasPer_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, Db, ExtCtrls,
  Buttons, DBCtrls, StdCtrls, Mask,  ZetaKeyCombo, ZetaNumero, ZetaEdit,
  cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons;

type
  TEditCatOtrasPer_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    TB_CODIGO: TZetaDBEdit;
    Label2: TLabel;
    TB_ELEMENT: TDBEdit;
    Label3: TLabel;
    TB_INGLES: TDBEdit;
    Label5: TLabel;
    TB_NUMERO: TZetaDBNumero;
    Label6: TLabel;
    TB_TEXTO: TDBEdit;
    Label7: TLabel;
    TB_TIPO: TDBRadioGroup;
    MontoLbl: TLabel;
    TB_MONTO: TZetaDBNumero;
    TasaLbl: TLabel;
    TB_TASA: TZetaDBNumero;
    TB_TOPE: TZetaDBNumero;
    TopeLbl: TLabel;
    Label10: TLabel;
    TB_IMSS: TZetaDBKeyCombo;
    TB_ISPT: TZetaDBKeyCombo;
    Label11: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure TB_TIPOClick(Sender: TObject);
    procedure TB_TIPOChange(Sender: TObject);
  private
  protected
    procedure Connect; override;
    procedure DoLookup; override;
  public
  end;

var
  EditCatOtrasPer_DevEx: TEditCatOtrasPer_DevEx;

implementation

uses ZetaCommonClasses, dCatalogos, ZAccesosTress, ZetaBuscador_DevEx;

{$R *.DFM}

procedure TEditCatOtrasPer_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_CONT_PER_FIJAS;
     HelpContext:= H60613_Percepciones_fijas;
     FirstControl := TB_CODIGO;
end;

procedure TEditCatOtrasPer_DevEx.Connect;
begin
     DataSource.DataSet := dmCatalogos.cdsOtrasPer;
end;

procedure TEditCatOtrasPer_DevEx.TB_TIPOClick(Sender: TObject);
begin
     inherited;
     // OBJETIVO : TIPO = CANTIDAD ;Habilita  MONTO ; Deshabilita TASA
     //             TIPO = TASA ; Habilita TASA; Deshabilita MONTO
     //                  tambi�n habilita Tope diario

     TB_MONTO.Enabled := ( TB_TIPO.Value = '0' );
     MontoLbl.Enabled := TB_MONTO.Enabled;

     TB_TASA.Enabled := NOT TB_MONTO.Enabled;
     TasaLbl.Enabled  := TB_TASA.Enabled;

     TB_TOPE.Enabled := NOT TB_MONTO.Enabled;
     Topelbl.Enabled := TB_TOPE.Enabled;
end;

procedure TEditCatOtrasPer_DevEx.TB_TIPOChange(Sender: TObject);
begin
     inherited;
     // OBJETIVO : TIPO = CANTIDAD ;Habilita  MONTO ; Deshabilita TASA
     //             TIPO = TASA ; Habilita TASA; Deshabilita MONTO
     //                  tambi�n habilita Tope diario

     TB_MONTO.Enabled := ( TB_TIPO.Value = '0' );
     MontoLbl.Enabled := TB_MONTO.Enabled;

     TB_TASA.Enabled := NOT TB_MONTO.Enabled;
     TasaLbl.Enabled  := TB_TASA.Enabled;

     TB_TOPE.Enabled := NOT TB_MONTO.Enabled;
     Topelbl.Enabled := TB_TOPE.Enabled;
end;

procedure TEditCatOtrasPer_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', 'Otras Percepciones', 'TB_CODIGO', dmCatalogos.cdsOtrasPer );
end;

end.
