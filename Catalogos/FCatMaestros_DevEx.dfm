inherited CatMaestros_DevEx: TCatMaestros_DevEx
  Left = 990
  Top = 262
  Caption = 'Maestros'
  ClientWidth = 488
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 488
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 162
      inherited textoValorActivo2: TLabel
        Width = 156
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 488
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object MA_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'MA_CODIGO'
      end
      object MA_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'MA_NOMBRE'
      end
      object MA_CEDULA: TcxGridDBColumn
        Caption = 'C'#233'dula'
        DataBinding.FieldName = 'MA_CEDULA'
      end
      object MA_RFC: TcxGridDBColumn
        Caption = 'R.F.C.'
        DataBinding.FieldName = 'MA_RFC'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
