unit FCatEventos_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Grids, DBGrids, Db, ExtCtrls,
     ZBaseConsulta,
     ZetaDBGrid, StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TCatEventos_DevEx = class(TBaseGridLectura_DevEx)
    EV_CODIGO: TcxGridDBColumn;
    EV_DESCRIP: TcxGridDBColumn;
    EV_ACTIVO: TcxGridDBColumn;
    EV_PRIORID: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar;override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  CatEventos_DevEx: TCatEventos_DevEx;

implementation

uses DCatalogos,
     ZetaBuscador_DevEx,
     ZetaCommonClasses;

{$R *.DFM}

{ TCatEventos }

procedure TCatEventos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H60635_Eventos;
     {$ifdef PRESUPUESTOS2}
     Caption := 'Cambios y Recortes';
     {$endif}
end;

procedure TCatEventos_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsEventos.Conectar;
          DataSource.DataSet:= cdsEventos;
     end;
end;

procedure TCatEventos_DevEx.Refresh;
begin
     dmCatalogos.cdsEventos.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatEventos_DevEx.Agregar;
begin
     dmCatalogos.cdsEventos.Agregar;
end;

procedure TCatEventos_DevEx.Borrar;
begin
     dmCatalogos.cdsEventos.Borrar;
      ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatEventos_DevEx.Modificar;
begin
     dmCatalogos.cdsEventos.Modificar;
end;

procedure TCatEventos_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Evento', 'Evento', 'EV_CODIGO', dmCatalogos.cdsEventos );
end;

procedure TCatEventos_DevEx.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
  inherited;

    //columna sumatoria: cuantos
   CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('EV_CODIGO'), K_SIN_TIPO , '', skCount);
   ZetaDBGridDBTableView.ApplyBestFit();


end;

end.

