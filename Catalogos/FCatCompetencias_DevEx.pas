unit FCatCompetencias_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Grids, DBGrids, Db, ExtCtrls,
     ZBaseConsulta,
     ZetaDBGrid, StdCtrls, ZbaseGridLectura_DevEx,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore,dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, ZetaKeyLookup_DevEx,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, Menus,
  ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  ZetaCXGrid, TressMorado2013, System.Actions;

type
  TCatCompetencias_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    lookTCompetencias: TZetaKeyLookup_DevEx;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure lookTCompetenciasValidKey(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure AplicaFiltro;
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
  public
    { Public declarations}
    procedure DoLookup; override;
  end;

var
  CatCompetencias_DevEx: TCatCompetencias_DevEx;

implementation

uses DCatalogos,
     ZetaBuscador_DevEx,
     ZAccesosTress,ZAccesosMgr,
     ZetaCommonClasses,ZetaCommonTools, dTablas;

{$R *.DFM}

{ TCatCompetencias }

procedure TCatCompetencias_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H_Cat_Competencias;
end;

procedure TCatCompetencias_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsCompetencias.Conectar;
          DataSource.DataSet:= cdsCompetencias;
     end;
     with dmTablas do
     begin
          cdsTCompetencias.Conectar;
          lookTCompetencias.LookupDataset := cdsTCompetencias;
     end;
     AplicaFiltro
end;

procedure TCatCompetencias_DevEx.Refresh;
begin
     dmCatalogos.cdsCompetencias.Refrescar;
end;

procedure TCatCompetencias_DevEx.Agregar;
begin
     dmCatalogos.cdsCompetencias.Agregar;
     DoRefresh;
end;

procedure TCatCompetencias_DevEx.Borrar;
begin
     dmCatalogos.cdsCompetencias.Borrar;
end;

procedure TCatCompetencias_DevEx.Modificar;
begin
     dmCatalogos.cdsCompetencias.Modificar;
end;

procedure TCatCompetencias_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Competencia', 'Competencia', 'CC_CODIGO', dmCatalogos.cdsCompetencias );
end;

Procedure TCatCompetencias_DevEx.AplicaFiltro;
begin
     if StrLleno(lookTCompetencias.Llave) then
     begin
          with dmCatalogos.cdsCompetencias do
          begin
               Filtered := False;
               Filter := Format(' TB_CODIGO = ''%s'' ',[dmTablas.cdsTCompetencias.FieldByName('TB_CODIGO').AsString ]);
               Filtered := True;
          end;
     end
     else
         dmCatalogos.cdsCompetencias.Filtered := False;
end;

procedure TCatCompetencias_DevEx.lookTCompetenciasValidKey(Sender: TObject);
begin
     inherited;
     AplicaFiltro;
     DoRefresh;
end;

function TCatCompetencias_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := ZAccesosMgr.CheckDerecho( D_CAT_COMPETENCIAS , K_DERECHO_CONSULTA );
end;

procedure TCatCompetencias_DevEx.FormShow(Sender: TObject);
begin
  //CreaColumaSumatoria('CC_Codigo',skCount);
  CreaColumaSumatoria(zetadbGriddbtableview.Columns[0],0,'',SkCount);
  inherited;
 ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
  //Esconde la caja de agrupamiento
  ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
end;

end.
