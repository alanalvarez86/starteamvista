unit FEditCatVigenciasSGM_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs,  ZBaseEdicion_DevEx,
  ZetaNumero, Mask, DBCtrls,
  StdCtrls, ZetaEdit, DB, ExtCtrls, Buttons, ZetaFecha,
  ZetaDBTextBox, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, cxContainer, cxEdit, cxGroupBox, cxTextEdit, cxMemo, cxDBEdit;

type
  TEditCatVigenciasSGM_DevEx = class(TBaseEdicion_DevEx)
    PV_TEXTO: TDBEdit;
    Label1: TLabel;
    Panel1: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Panel2: TPanel;
    PV_REFEREN: TZetaDBEdit;
    DBCodigoLBL: TLabel;
    GroupBox1: TcxGroupBox;
    PV_FEC_INI: TZetaDBFecha;
    PV_FEC_FIN: TZetaDBFecha;
    Label6: TLabel;
    Label7: TLabel;
    Label2: TLabel;
    GroupBox2: TcxGroupBox;
    PV_OBSERVA: TcxDBMemo;
    PV_NUMERO: TZetaDBNumero;
    zTxtNoPoliza: TZetaTextBox;
    zTxtDescripcion: TZetaTextBox;
    zTxtTipoDescrip: TZetaTextBox;
    PV_CFIJO: TZetaDBNumero;
    Label8: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);


  private
    procedure HabilitaControls(const lHabilita: Boolean);

    { Private declarations }
  public
    { Public declarations }
      procedure DoLookup; override;
  protected
     procedure Connect; override;
     function PuedeModificar(var sMensaje: String): Boolean;override;
     function PuedeAgregar(var sMensaje: String): Boolean;override;
     function PuedeBorrar(var sMensaje: String): Boolean;override;
     procedure EscribirCambios;override;
  end;

var
  EditCatVigenciasSGM_DevEx: TEditCatVigenciasSGM_DevEx;

implementation

uses
    DReportes,
    zReportTools,
    dCatalogos,
    ZetaCommonClasses,
    ZetaCommonTools,
    ZetaCommonLists,
    ZAccesosTress,
    ZAccesosMgr,
    ZetaBuscador_DevEx;

{$R *.dfm}

{ TFEditCatTipoPoliza }

procedure TEditCatVigenciasSGM_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl  := PV_REFEREN;
     IndexDerechos := D_CAT_GRAL_SEG_GASTOS_MEDICOS;
     HelpContext   := H_Cat_SGM;
end;

procedure TEditCatVigenciasSGM_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsVigenciasSGM.Conectar;
          DataSource.DataSet := cdsVigenciasSGM;
     end;
end;

procedure TEditCatVigenciasSGM_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Self.Caption, 'PV_REFEREN', dmCatalogos.cdsSegGastosMed );
end;

procedure TEditCatVigenciasSGM_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with dmCatalogos.cdsSegGastosMed do
     begin
          zTxtNoPoliza.Caption := FieldByName('PM_NUMERO').AsString;
          zTxtDescripcion.Caption := FieldByName('PM_DESCRIP').AsString;
          zTxtTipoDescrip.Caption := ObtieneElemento( lfTipoSGM,FieldByName('PM_TIPO').AsInteger );
     end;
end;

procedure TEditCatVigenciasSGM_DevEx.HabilitaControls(const lHabilita:Boolean);
begin
     PV_REFEREN.Enabled := lHabilita;
     PV_FEC_INI.Enabled := lHabilita;
     PV_FEC_FIN.Enabled := lHabilita;
     PV_OBSERVA.Enabled := lHabilita;
     PV_TEXTO.Enabled := lHabilita;
     PV_NUMERO.Enabled := lHabilita;
end;

procedure TEditCatVigenciasSGM_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
     inherited;
     if Field = nil then
     begin
          if dmCatalogos.cdsVigenciasSGM.state = dsBrowse then
          begin
               HabilitaControls(   ZAccesosMgr.CheckDerecho( D_CAT_GRAL_SEG_GASTOS_MEDICOS , K_DERECHO_CAMBIO )  );
          end
          else
          if dmCatalogos.cdsVigenciasSGM.state = dsInsert then
          begin
               HabilitaControls( True );
          end;
     end;
end;

function TEditCatVigenciasSGM_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No Tiene Permiso Para Agregar Registros';
     Result := ZAccesosMgr.CheckDerecho( D_CAT_GRAL_SEG_GASTOS_MEDICOS , K_DERECHO_CAMBIO );
end;

function TEditCatVigenciasSGM_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No Tiene Permiso Para Borrar Registros';
     Result := ZAccesosMgr.CheckDerecho( D_CAT_GRAL_SEG_GASTOS_MEDICOS , K_DERECHO_CAMBIO );
end;

function TEditCatVigenciasSGM_DevEx.PuedeModificar(
  var sMensaje: String): Boolean;
begin
     sMensaje := 'No Tiene Permiso Para Modificar Registros';
     Result := ZAccesosMgr.CheckDerecho( D_CAT_GRAL_SEG_GASTOS_MEDICOS , K_DERECHO_CAMBIO );
end;

procedure TEditCatVigenciasSGM_DevEx.EscribirCambios;
var
   bPos : Boolean;
   bAnterior: TBookMark;
begin
     with dmCatalogos.cdsVigenciasSGM do
     begin
          bAnterior := GetBookMark;
          bPos := State = dsInsert;
          Post;
          if bPos then
             Last
          else
          begin
               GotoBookMark( bAnterior );
               FreeBookMark( bAnterior );
          end;

     end;
     //inherited;
end;

end.
