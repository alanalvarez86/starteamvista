unit FEditNomParams_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, Buttons, DBCtrls, StdCtrls, Mask,
     ZetaKeyCombo, ZetaNumero, ZetaTipoEntidad,
  ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, cxContainer, cxEdit, cxTextEdit, cxMemo, cxDBEdit;

type
  TEditNomParams_DevEx = class(TBaseEdicion_DevEx)
    Label1: TLabel;
    Label2: TLabel;
    NP_FOLIO: TZetaDBNumero;
    NP_NOMBRE: TDBEdit;
    NP_FORMULA: TcxDBMemo;
    Label3: TLabel;
    Label4: TLabel;
    NP_TIPO: TZetaDBKeyCombo;
    NP_ACTIVO: TDBCheckBox;
    Label5: TLabel;
    NP_DESCRIP: TDBEdit;
    SBCO_FORMULA: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure SBCO_FORMULAClick(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  EditNomParams_DevEx: TEditNomParams_DevEx;

implementation

uses DCatalogos,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZAccesosTress,
     ZConstruyeFormula,
     ZetaBuscaEntero_DevEx;

{$R *.DFM}

procedure TEditNomParams_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := ZetaCommonClasses.H60622_Parametros_nomina;
     IndexDerechos := ZAccesosTress.D_CAT_NOMINA_PARAMETROS;
     FirstControl := NP_FOLIO;
end;

procedure TEditNomParams_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          // se requiere conectar cdsLookup antes de de que cdsNomParam entre en edici�n
          // si la transf. de Data se hace despu�s, cdsNomParam regresa a dsBrowse y provoca error
          // El LookUp conecta tambi�n a cdsNomParam
          cdsNomParamLookUp.Conectar;
          DataSource.DataSet := cdsNomParam;
     end;
end;

procedure TEditNomParams_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscaEntero_DevEx.BuscarCodigo( 'Folio', 'Param�tro de N�mina', 'NP_FOLIO', dmCatalogos.cdsNomParam );
end;

procedure TEditNomParams_DevEx.SBCO_FORMULAClick(Sender: TObject);
begin
     with dmCatalogos.cdsNomParam do
     begin
          if not ( Editing or Inserting ) then
             Edit;
          FieldByName( 'NP_FORMULA' ).AsString := ZConstruyeFormula.GetFormulaUnlimited( enNomina, NP_FORMULA.Lines.Text, NP_FORMULA.SelStart, evNomina );
     end;
end;

end.
