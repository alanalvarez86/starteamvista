unit FEditCatHorarios_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, StdCtrls, DBCtrls, Mask, Db, Buttons, ExtCtrls, ComCtrls,
  ZetaKeyCombo, ZetaDBTextBox, ZetaNumero, ZetaHora, ZetaEdit,
  cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC, dxBarBuiltInMenu;

type
  TEditCatHorarios_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    Label10: TLabel;
    HO_CODIGO: TZetaDBEdit;
    Label11: TLabel;
    HO_DESCRIP: TDBEdit;
    Label12: TLabel;
    HO_TIPO: TZetaDBKeyCombo;
    HO_ACTIVO: TDBCheckBox;
    PageControl_DevEx: TcxPageControl;
    TabGenerales_DevEx: TcxTabSheet;
    TabComidas_DevEx: TcxTabSheet;
    tsConfidencialidad_DevEx: TcxTabSheet;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    HO_INTIME: TZetaDBHora;
    ZetaDBNumero1: TZetaDBNumero;
    ZetaDBNumero2: TZetaDBNumero;
    HO_PARES: TDBCheckBox;
    SalidaGB: TGroupBox;
    HO_OUTTIMElbl: TLabel;
    HO_OU_TEMPlbl: TLabel;
    HO_OU_TARDlbl: TLabel;
    LMin3: TLabel;
    LMin4: TLabel;
    Label3: TLabel;
    HO_OUTTIME: TZetaDBHora;
    HO_OU_TEMP: TZetaDBNumero;
    HO_OU_TARD: TZetaDBNumero;
    HO_LASTOUT: TZetaDBHora;
    GBHoras: TGroupBox;
    TU_JORNADAlbl: TLabel;
    HO_DOBLESlbl: TLabel;
    HO_JORNADA: TZetaDBTextBox;
    Label2: TLabel;
    HO_DOBLES: TZetaDBNumero;
    HO_EXT_FIJ: TZetaDBNumero;
    HO_COMERlbl: TLabel;
    HO_COMER: TZetaDBNumero;
    Label5: TLabel;
    HO_MIN_EATLbl: TLabel;
    HO_MIN_EAT: TZetaDBNumero;
    Label4: TLabel;
    HO_CHK_EAT: TDBCheckBox;
    HO_IGN_EAT: TDBCheckBox;
    HO_ADD_EAT: TDBCheckBox;
    HO_EXT_COMlbl: TLabel;
    ZetaDBNumero3: TZetaDBNumero;
    Label6: TLabel;
    HO_EXT_MINlbl: TLabel;
    ZetaDBNumero4: TZetaDBNumero;
    Label7: TLabel;
    HO_OUT_EATlbl: TLabel;
    HO_OUT_EAT: TZetaDBHora;
    HO_IN_EATlbl: TLabel;
    HO_IN_EAT: TZetaDBHora;
    HO_OUT_BRKlbl: TLabel;
    HO_OUT_BRK: TZetaDBHora;
    HO_IN_BRKlbl: TLabel;
    HO_IN_BRK: TZetaDBHora;
    gbConfidencialidad: TGroupBox;
    listaConfidencialidad: TListBox;
    rbConfidenTodas: TRadioButton;
    rbConfidenAlgunas: TRadioButton;
    btSeleccionarConfiden_DevEx: TcxButton;
    GroupBox2: TGroupBox;
    lblIngles: TLabel;
    HO_INGLES: TDBEdit;
    lblNumero: TLabel;
    HO_NUMERO: TZetaDBNumero;
    HO_TEXTO: TDBEdit;
    lblTexto: TLabel;
    lblTipoJornada: TLabel;
    HO_TIP_JT: TZetaKeyCombo;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure HO_CHK_EATClick(Sender: TObject);
    procedure HO_ADD_EATClick(Sender: TObject);
    procedure HO_TIPOChange(Sender: TObject);         
    //procedure btSeleccionarConfidenClick(Sender: TObject);
    procedure rbConfidenTodasClick(Sender: TObject);
    procedure rbConfidenAlgunasClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure btSeleccionarConfiden_DevExClick(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure HO_TIP_JTChange(Sender: TObject);
  private 
    lValoresConfidencialidad : TStringList;     
    procedure FillListaConfidencialidad;
    procedure SetListConfidencialidad( sValores : string );
    procedure GetConfidencialidad;
    procedure IniciaHorarioTipoJornada;
    procedure GetHorarioTipoJornada;
  protected
    procedure Connect; override;
    procedure DoLookup; override;
  public
  end;

const
     K_T_TODOS = 0;
     K_T_MATUTINO = 8;
     K_T_VESPERTINO = 7.5;
     K_T_NOCTURNO = 7;
var
  EditCatHorarios_DevEx: TEditCatHorarios_DevEx;

implementation

uses dCatalogos,
     ZetaCommonClasses,
     ZAccesosTress,
     ZetaBuscador_DevEx,
     ZetaCommonLists,
     FSeleccionarConfidencialidad_DevEx, dSistema, DCliente, ZetaCommonTools;

{$R *.DFM}

procedure TEditCatHorarios_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_CONT_HORARIOS;
     FirstControl := HO_CODIGO;
     HelpContext:= H60615_Horarios;  
     lValoresConfidencialidad := TStringList.Create;
     {*** US 12790: Agregar el dato de tipo de jornada al cat�logo de horarios ***}
     with HO_TIP_JT do
     begin
          ZetaCommonLists.LlenaLista( lfTipoJornadaTrabajo, Items );
          Items.Insert( 0, 'Todos' );
          ItemIndex := 0;
     end;
end;

procedure TEditCatHorarios_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
     inherited;
     {*** US 12790: Agregar el dato de tipo de jornada al cat�logo de horarios ***}
     IniciaHorarioTipoJornada;      
end;

procedure TEditCatHorarios_DevEx.Connect;
begin   
     dmSistema.cdsNivel0.Conectar;
     with dmCatalogos do
     begin
          Datasource.Dataset := cdsHorarios;
          {*** US 12790: Agregar el dato de tipo de jornada al cat�logo de horarios ***}
          IniciaHorarioTipoJornada;
     end;
     HO_CHK_EATClick( Self );
     HO_ADD_EATClick( Self );
     HO_TIPOChange( Self );
end;

procedure TEditCatHorarios_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     Pagecontrol_DevEx.ActivePage:= TabGenerales_DevEx;
     FillListaConfidencialidad;
end;

procedure TEditCatHorarios_DevEx.HO_CHK_EATClick(Sender: TObject);
begin
     inherited;
     HO_IGN_EAT.Enabled:= HO_CHK_EAT.Checked;  // Ignorar Salida habilitada ; si Checan Salir a Comer
end;

procedure TEditCatHorarios_DevEx.HO_ADD_EATClick(Sender: TObject);
begin
     inherited;
     HO_OUT_BRK.Enabled    := HO_ADD_EAT.Checked;  // Si Agregar Comida AUTO ; se capturan horas de salida y regreso
     HO_OUT_BRKLbl.Enabled := HO_OUT_BRK.Enabled;
     HO_IN_BRK.Enabled     := HO_OUT_BRK.Enabled;
     HO_IN_BRKLbl.Enabled  := HO_OUT_BRK.Enabled;
end;

procedure TEditCatHorarios_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Horario', 'Horario', 'HO_CODIGO', dmCatalogos.cdsHorarios );
end;

procedure TEditCatHorarios_DevEx.HO_TIPOChange(Sender: TObject);
begin
     inherited;
     HO_PARES.Enabled := ( eTipoHorario( HO_TIPO.ItemIndex ) in [ thSinHorario,thNormal,thMadrugadaEntrada ] );
end;

{procedure TEditCatHorarios_DevEx.btSeleccionarConfidenClick(Sender: TObject);
var
   selConfi : TSeleccionarConfidencialidad;
   sCM_NIVEL0Nuevo : string;
begin
  inherited;

  selConfi := TSeleccionarConfidencialidad.Create( Self );
  sCM_NIVEL0Nuevo := selConfi.GetConfidencialidad( DataSource.DataSet.FieldByName('HO_NIVEL0').AsString, lValoresConfidencialidad, True, dmCliente.Confidencialidad );

  if (  sCM_NIVEL0Nuevo <>  DataSource.DataSet.FieldByName('HO_NIVEL0').AsString ) then
  begin
     with DataSource.DataSet do
     begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

           FieldByName('HO_NIVEL0').AsString  := sCM_NIVEL0Nuevo;
           GetConfidencialidad;
     end;
  end;

  FreeAndNil ( selConfi ) ;
end;}

procedure TEditCatHorarios_DevEx.FillListaConfidencialidad;
begin
     with dmSistema.cdsNivel0 do
     begin
          First;
          lValoresConfidencialidad.Clear;
          lValoresConfidencialidad.NameValueSeparator := '=' ;
          While( not Eof )do
          begin
               lValoresConfidencialidad.Add(FieldByName('TB_CODIGO').AsString +'='+ FieldByName('TB_ELEMENT').AsString);
               Next;
          end;
     end;
end;

procedure TEditCatHorarios_DevEx.GetConfidencialidad;
begin
   if (  DataSource.DataSet  <> nil ) then
   with DataSource.DataSet do
   begin
        SetListConfidencialidad(  FieldByName('HO_NIVEL0').AsString );
   end;
end;

procedure TEditCatHorarios_DevEx.SetListConfidencialidad(sValores: string);
var
   Lista : TStringList;
   i, j : integer;
begin
    Lista := TStringList.Create;

    sValores := Trim( sValores );
    if StrLleno( sValores ) then
        Lista.CommaText  := sValores;

    listaConfidencialidad.Items.Clear;

    rbConfidenTodas.OnClick := nil;
    rbConfidenAlgunas.OnClick := nil;
    rbConfidenTodas.Checked := False;
    rbConfidenAlgunas.Checked := False;
                                                 
    if lValoresConfidencialidad.Count = 0 then
       FillListaConfidencialidad;

    for i:= 0 to lValoresConfidencialidad.Count - 1 do
    begin
        if ( Lista.Count > 0 ) then
        begin
                j := Lista.IndexOf( Trim( lValoresConfidencialidad.Names[i] )  );
                if ( j >= 0 ) then
                   listaConfidencialidad.Items.Add( lValoresConfidencialidad[i] )
        end
    end;

    rbConfidenTodas.Checked :=   listaConfidencialidad.Items.Count = 0;
    rbConfidenAlgunas.Checked := not rbConfidenTodas.Checked;

    if ( gbConfidencialidad.Enabled ) then
    begin
        listaConfidencialidad.Enabled := rbConfidenAlgunas.Checked;
        btSeleccionarConfiden_DevEx.Enabled := rbConfidenAlgunas.Checked;
    end;

    rbConfidenTodas.OnClick := rbConfidenTodasClick;
    rbConfidenAlgunas.OnClick := rbConfidenAlgunasClick;

    FreeAndNil( Lista );

end;

procedure TEditCatHorarios_DevEx.rbConfidenTodasClick(Sender: TObject);
begin
  inherited;

   with DataSource.DataSet do
   begin
        if not( State in [dsEdit,dsInsert] ) then
           Edit;
        FieldByName('HO_NIVEL0').AsString  :=  '';
   end;
   GetConfidencialidad;
end;

procedure TEditCatHorarios_DevEx.rbConfidenAlgunasClick(Sender: TObject);
begin
  inherited;
     if ( rbConfidenAlgunas.Checked ) then
     begin
        btSeleccionarConfiden_DevExClick( Sender );
        GetConfidencialidad;
     end;

end;

procedure TEditCatHorarios_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
     if ( Field = nil ) then
     begin
          with DataSource.DataSet do
          begin
               if ( State in [dsBrowse] ) then
               begin
                    GetConfidencialidad;
                    {*** US 12963: Al navegar entre Horarios no muestra el valor correcto del tipo de Jornada ***}
                    GetHorarioTipoJornada;
               end
               else if (State in [dsInsert]) then
               begin
                    rbConfidenTodas.Checked := TRUE;
                    HO_TIP_JT.ItemIndex := 0;
               end;
          end;
     end;
end;

procedure TEditCatHorarios_DevEx.btSeleccionarConfiden_DevExClick(
  Sender: TObject);
var
   selConfi : TSeleccionarConfidencialidad_DevEx;
   sCM_NIVEL0Nuevo : string;
begin
  inherited;

  selConfi := TSeleccionarConfidencialidad_DevEx.Create( Self );
  sCM_NIVEL0Nuevo := selConfi.GetConfidencialidad( DataSource.DataSet.FieldByName('HO_NIVEL0').AsString, lValoresConfidencialidad, True, dmCliente.Confidencialidad );

  if (  sCM_NIVEL0Nuevo <>  DataSource.DataSet.FieldByName('HO_NIVEL0').AsString ) then
  begin
     with DataSource.DataSet do
     begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

           FieldByName('HO_NIVEL0').AsString  := sCM_NIVEL0Nuevo;
           GetConfidencialidad;
     end;
  end;

  FreeAndNil ( selConfi ) ;
end;

{*** US 12963: Al navegar entre Horarios no muestra el valor correcto del tipo de Jornada ***}
procedure TEditCatHorarios_DevEx.GetHorarioTipoJornada;
begin
     if (  DataSource.DataSet  <> nil ) then
     begin
          with DataSource.DataSet do
          begin
               IniciaHorarioTipoJornada;
          end;
     end;
end;

{*** US 12790: Agregar el dato de tipo de jornada al cat�logo de horarios ***}
procedure TEditCatHorarios_DevEx.IniciaHorarioTipoJornada;
begin
     with DataSource.DataSet, HO_TIP_JT do
     begin
          ItemIndex := K_T_TODOS;
          if ( FieldByName( 'HO_TIP_JT' ).AsFloat = K_T_TODOS ) then
             ItemIndex := K_T_TODOS
          else if ( FieldByName( 'HO_TIP_JT' ).AsFloat = K_T_MATUTINO ) then
             ItemIndex := Ord( tjtmatutino ) + 1
          else if ( FieldByName( 'HO_TIP_JT' ).AsFloat = K_T_VESPERTINO ) then
             ItemIndex := Ord( tjtvespertino ) + 1
          else if ( FieldByName( 'HO_TIP_JT' ).AsFloat = K_T_NOCTURNO ) then
             ItemIndex := Ord( tjtnocturno ) + 1;
     end;
end;

{*** US 12790: Agregar el dato de tipo de jornada al cat�logo de horarios ***}
procedure TEditCatHorarios_DevEx.HO_TIP_JTChange(Sender: TObject);
var
   lValorTipoJornada : Currency;
   function ObternerValor( ItemIndex : Integer ): Currency;
   begin
        Result := K_T_TODOS;
        if ItemIndex in [ K_T_TODOS ] then
             Result := K_T_TODOS
          else if ( ItemIndex in [ ord( tjtmatutino ) + 1 ] ) then
             Result := K_T_MATUTINO 
          else if ( ItemIndex in [ ord( tjtvespertino ) + 1 ] ) then
             Result := K_T_VESPERTINO
          else if ( ItemIndex in [ ord( tjtnocturno ) + 1 ]  ) then
             Result := K_T_NOCTURNO;
   end;   
begin
     lValorTipoJornada := ObternerValor( HO_TIP_JT.ItemIndex );
     with DataSource.DataSet do
     begin
          if ( lValorTipoJornada <> FieldByName( 'HO_TIP_JT' ).AsFloat ) then
          begin
               if not Editing then
                  Edit;
               FieldByName( 'HO_TIP_JT' ).AsFloat:= lValorTipoJornada;
          end;
     end;
end;
{*** FIN ***}

end.
