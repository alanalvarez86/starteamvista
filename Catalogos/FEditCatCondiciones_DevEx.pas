unit FEditCatCondiciones_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DBCtrls, Mask, Db, Buttons, ExtCtrls,
  ZetaDBTextBox, ZetaKeyCombo, ZetaEdit, ZetaTipoEntidad, ZetaCommonLists,
  ZetaNumero, ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, cxContainer, cxEdit, cxImage, cxTextEdit, cxMemo, cxDBEdit,
  cxCheckBox, dxGDIPlusClasses;

type
  TEditCatCondiciones_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    QU_CODIGO: TZetaDBEdit;
    QU_DESCRIPlbl: TLabel;
    QU_DESCRIP: TDBEdit;
    QU_FILTROlbl: TLabel;
    QU_FILTRO: TcxDBMemo;
    sbtnContructor: TcxButton;
    Label2: TLabel;
    QU_NIVEL: TZetaDBKeyCombo;
    QU_NAVEGA: TDBCheckBox;
    QU_CANDADO: TDBCheckBox;
    lblQU_ORDEN: TLabel;
    QU_ORDEN: TZetaDBNumero;
    lblUs_Codigo: TLabel;
    US_CODIGO: TZetaDBTextBox;
    TestBtn: TcxButton;
    ErrorImg: TcxImage;
    OkImg: TcxImage;
    procedure FormCreate(Sender: TObject);
    procedure sbtnContructorClick(Sender: TObject);
    procedure TestBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure QU_NAVEGAClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    procedure CreaCondicion;
  protected
    lOKFormula : Boolean;
    FEntidadActiva: TipoEntidad;
    FParams : TStrings;
    FTipoEvaluador: eTipoEvaluador;
    procedure Connect; override;
    procedure DoLookup; override;
    {$ifdef TRESS}
    procedure EscribirCambios; override;
    {$endif}
  public
    //property EntidadActiva: TipoEntidad read FEntidadActiva write FEntidadActiva;
    property Params: TStrings read FParams write FParams;
    property TipoEvaluador: eTipoEvaluador read FTipoEvaluador write FTipoEvaluador;
  end;

var
  EditCatCondiciones_DevEx: TEditCatCondiciones_DevEx;

implementation

uses dCatalogos,
     ZetaBuscador_DevEx,
     FConstruyeFiltro_DevEx,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo,
     ZAccesosTress,
     DDiccionario,
     FTressShell,
	 DCliente;

{$R *.DFM}

{En seleccion y visitantes no existen los campos QU_NAVEGA,QU_ORDEN,QU_CANDADO
 tomar en cuenta para futuras programaciones sobre esta forma}

procedure TEditCatCondiciones_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_GRALES_CONDICIONES;
     FirstControl := QU_CODIGO;
     {$ifdef VISITANTES}
     HelpContext :=H_VISMGR_EDIT_TCONDICION;
     {$else}
     HelpContext := H_CAT_EDIT_CONDICIONES;
     //HelpContext := H60634_Condiciones;
     {$endif}
     {$ifdef TRESS}
      TestBtn.Visible := True;
      QU_NAVEGA.Visible := True;
      QU_ORDEN.Visible := True;
      lblQU_ORDEN.Visible := True;
      FEntidadActiva := enEmpleado;
     {$else}
      with QU_NAVEGA do
      begin
           DataField:= VACIO;
           Visible := False;
           lblUs_Codigo.Top := Top;
           US_CODIGO.Top := Top;
      end;
      with QU_ORDEN do
      begin
           DataField:= VACIO;
           Visible := False;
           lblQU_ORDEN.Visible := Visible;
      end;
      with QU_CANDADO do
      begin
           DataField:= VACIO;
           Visible:= False;
      end;
      TestBtn.Visible := False;
     {$endif}
end;

procedure TEditCatCondiciones_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     OkImg.Visible := FALSE;     
     ErrorImg.Visible := FALSE;
end;

procedure TEditCatCondiciones_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          DataSource.DataSet:= cdsCondiciones;
          with QU_CODIGO do
             MaxLength := cdsCondiciones.FieldByName( 'QU_CODIGO' ).Size;

         QU_ORDEN.Enabled := QU_NAVEGA.Checked;
         lblQU_ORDEN.Enabled := QU_ORDEN.Enabled;
     end;
end;

procedure TEditCatCondiciones_DevEx.CreaCondicion;
  var sFiltro: string;
begin
     if GetConstruyeFiltro( EntidadDefaultCondicion, sFiltro, NOT QU_NAVEGA.Checked ) then
     begin
          if ( Modo = dsBrowse ) then DataSource.DataSet.Edit;
          with DataSource.DataSet.FieldByName('QU_FILTRO') do
               AsString := ConcatFiltros( AsString, sFiltro );
     end;
end;

procedure TEditCatCondiciones_DevEx.sbtnContructorClick(Sender: TObject);
begin
     with dmCatalogos.cdsCondiciones do
     begin
          if ( NOT ( dmcliente.GetDatosUsuarioActivo.Grupo = D_GRUPO_SIN_RESTRICCION ) ) and
             ( zStrToBool( FieldByName('QU_CANDADO').AsString )) and
             ( FieldByName('US_CODIGO').AsInteger <> dmCliente.Usuario ) then
             ZetaDialogo.ZError('Error en Tress', '�Se encontr� un Error! '+ CR_LF + 'Usuario No Puede Modificar/Borrar Esta Condici�n',0)
//                   ShowMessage('Usuario No Puede Modificar/Borrar Esta Condici�n')
          else
          begin
              inherited;
              {$ifdef ANTES}
              if QU_FILTRO.Text > ''  then
              begin
                   if ZConfirm('','Esta Opci�n Sustituye la Condici�n Actual' + CR_LF + '�Desea Continuar?',0,mbYes ) then
                      CreaCondicion
              end
              else CreaCondicion;
              {$endif}
              CreaCondicion;
          end;
      end;
end;

procedure TEditCatCondiciones_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Condici�n', 'Condici�n', 'QU_CODIGO', dmCatalogos.cdsCondiciones );
end;

procedure TEditCatCondiciones_DevEx.TestBtnClick(Sender: TObject);
var
   sFormula: String;
begin
     OkImg.Visible := True;
     ErrorImg.Visible := False;
     sFormula := QU_FILTRO.Lines.Text;
     if not dmDiccionario.SQLValido( sFormula, FEntidadActiva ) then
     begin
          OkImg.Visible := False;
          ErrorImg.Visible := True;
          ZetaDialogo.ZError( Caption, sFormula, 0 );
     end;
end;

{$ifdef TRESS}
procedure TEditCatCondiciones_DevEx.EscribirCambios;
var
   sFormula: String;
begin
     sFormula := QU_FILTRO.Lines.Text;
     if ( dmCatalogos.cdsCondiciones.FieldByName('QU_NAVEGA').AsString = 'S') and ( NOT dmDiccionario.SQLValido( sFormula, FEntidadActiva ) ) then
     begin
          ZetaDialogo.ZError( Caption, sFormula, 0 );
          dmCatalogos.HayErrorCount := dmCatalogos.HayErrorCount + 1;
     end
     else inherited EscribirCambios;
end;
{$endif}

procedure TEditCatCondiciones_DevEx.QU_NAVEGAClick(Sender: TObject);
begin
     inherited;
     QU_ORDEN.Enabled := QU_NAVEGA.Checked;
     lblQU_ORDEN.Enabled := QU_ORDEN.Enabled;
end;

procedure TEditCatCondiciones_DevEx.OK_DevExClick(Sender: TObject);
begin
     inherited;
     //validar si hubo errores
     {$ifdef TRESS}
     if dmCatalogos.HayErrorCount = 0 then
     begin
          TressShell.RefrescaNavegacionInfo;
          Close;
     end;
     {$endif}
end;

end.
