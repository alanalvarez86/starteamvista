inherited CatCalendario_DevEx: TCatCalendario_DevEx
  Left = 280
  Top = 159
  Caption = 'Calendario de Cursos'
  ClientHeight = 261
  ClientWidth = 645
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 645
    inherited ValorActivo2: TPanel
      Width = 386
      inherited textoValorActivo2: TLabel
        Width = 380
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 645
    Height = 242
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object CU_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CU_CODIGO'
      end
      object CU_NOMBRE: TcxGridDBColumn
        Caption = 'Curso'
        DataBinding.FieldName = 'CU_NOMBRE'
      end
      object CU_REVISIO: TcxGridDBColumn
        Caption = 'Revisi'#243'n'
        DataBinding.FieldName = 'CU_REVISIO'
      end
      object CC_FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'CC_FECHA'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
