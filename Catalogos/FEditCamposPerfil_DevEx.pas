unit FEditCamposPerfil_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DBCtrls, ZetaNumero, ZetaKeyCombo, Mask,
  ZetaEdit, DB, ExtCtrls, Buttons, ZetaCommonLists,ZetaDBTextBox,
  ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons,
  cxContainer, cxEdit, cxTextEdit, cxMemo, cxDBEdit;

type
  TEditCamposPerfil_DevEx = class(TBaseEdicion_DevEx)
    Label1: TLabel;
    Label4: TLabel;
    DF_TITULO: TDBEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    DF_LIMITE: TZetaDBNumero;
    LblLimite: TLabel;
    DF_VALORES: TcxDBMemo;
    LblValores: TLabel;
    DF_CONTROL: TZetaDBKeyCombo;
    DF_CAMPO: TDBComboBox;
    DT_NOMBRE: TZetaDBTextBox;
    DT_CODIGO: TZetaDBTextBox;
    DF_ORDEN: TZetaDBTextBox;
    procedure DF_CONTROLChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
    procedure SetControlesTipo( oControl: eControles );
  protected
    { Protected declarations }
    procedure Connect; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  EditCamposPerfil_DevEx: TEditCamposPerfil_DevEx;

implementation
uses
    dCatalogos,
    ZetaBuscador_DevEx,
    ZetaCommonClasses,
    ZetaClientTools,
    ZAccesosTress;
{$R *.dfm}

procedure TEditCamposPerfil_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_DESC_CAMPOS_PERF;
     HelpContext   := H_CAT_CAMPOS_PERF;
     FirstControl  := DF_TITULO;
end;

procedure TEditCamposPerfil_DevEx.FormShow(Sender: TObject);
begin
     inherited;
//     DF_CONTROLChange(Sender);
end;

procedure TEditCamposPerfil_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsCamposPerfil.Conectar;
          DataSource.DataSet := cdsCamposPerfil;
     end;
end;

procedure TEditCamposPerfil_DevEx.DF_CONTROLChange(Sender: TObject);
begin
     inherited;
     SetControlesTipo( eControles( DF_CONTROL.ItemIndex ) );
end;

procedure TEditCamposPerfil_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo',Self.Caption,'DT_CODIGO', dmCatalogos.cdsCamposPerfil );
end;

procedure TEditCamposPerfil_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( Field = nil ) then
        SetControlesTipo( eControles( dmCatalogos.cdsCamposPerfil.FieldByName( 'DF_CONTROL' ).AsInteger ) );
end;

procedure TEditCamposPerfil_DevEx.SetControlesTipo( oControl: eControles );
begin
     inherited;
     ZetaClientTools.SetControlEnabled( DF_LIMITE, LblLimite, oControl in [ coTextoCorto , coTextoLargo ] );
     ZetaClientTools.SetControlEnabled( DF_VALORES, LblValores, oControl in [ coCombo , coLista ] );
end;



end.
