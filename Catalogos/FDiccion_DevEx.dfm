inherited Diccion_DevEx: TDiccion_DevEx
  Left = 115
  Top = 233
  Caption = 'Diccionario de Datos'
  ClientHeight = 271
  ClientWidth = 592
  ExplicitWidth = 592
  ExplicitHeight = 271
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 592
    ExplicitWidth = 592
    inherited Slider: TSplitter
      Left = 323
      ExplicitLeft = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      ExplicitWidth = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 266
      ExplicitLeft = 326
      ExplicitWidth = 266
      inherited textoValorActivo2: TLabel
        Width = 260
        ExplicitLeft = 180
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 49
    Width = 592
    Height = 222
    ExplicitWidth = 592
    ExplicitHeight = 252
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsData.DeletingConfirmation = True
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      Styles.Content = cxStyle1
      Styles.Header = cxStyle2
      Styles.Indicator = cxStyle2
      object DI_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'DI_NOMBRE'
        Styles.Content = cxStyle3
        Width = 83
      end
      object DI_TITULO: TcxGridDBColumn
        Caption = 'T'#237'tulo'
        DataBinding.FieldName = 'DI_TITULO'
        Styles.Content = cxStyle4
        Width = 214
      end
      object DI_ANCHO: TcxGridDBColumn
        Caption = 'Ancho'
        DataBinding.FieldName = 'DI_ANCHO'
        Styles.Content = cxStyle5
        Width = 37
      end
      object DI_MASCARA: TcxGridDBColumn
        Caption = 'M'#225'scara'
        DataBinding.FieldName = 'DI_MASCARA'
        Styles.Content = cxStyle6
        Width = 121
      end
      object DI_CONFI: TcxGridDBColumn
        Caption = 'Confidencial?'
        DataBinding.FieldName = 'DI_CONFI'
        Styles.Content = cxStyle7
        Width = 72
      end
    end
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 19
    Width = 592
    Height = 30
    Align = alTop
    TabOrder = 2
    ExplicitLeft = -18
    ExplicitWidth = 610
    object Label1: TLabel
      Left = 16
      Top = 8
      Width = 73
      Height = 13
      Caption = 'Tabla Principal:'
    end
    object CBEntidades: TComboBox
      Left = 95
      Top = 4
      Width = 258
      Height = 21
      Style = csDropDownList
      TabOrder = 0
      OnChange = CBEntidadesChange
    end
  end
  inherited DataSource: TDataSource
    Left = 16
    Top = 128
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
  object cxStyleRepository1: TcxStyleRepository
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWindow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
  end
end
