unit FEditMatriz_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, Db, ExtCtrls,
  Buttons, DBCtrls, StdCtrls, Mask, ZetaNumero, 
  ZetaDBTextBox, FCatEntNivel_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx;

type
  TEditMatriz_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    ZCurso: TZetaTextBox;
    Label2: TLabel;
    PU_CODIGO: TZetaDBKeyLookup_DevEx;
    Label3: TLabel;
    EN_DIAS: TZetaDBNumero;
    Label4: TLabel;
    EN_OPCIONA: TDBCheckBox;
    gbReprogramacion: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    EN_REPROG: TDBCheckBox;
    EN_RE_DIAS: TZetaDBNumero;
    gbProgramacion: TGroupBox;
    gbProgramarA: TDBRadioGroup;
    bModifica_DevEx: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure gbProgramarAChange(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure bModifica_DevExClick(Sender: TObject);
    procedure SetEditarSoloActivos;
  private
  protected
    procedure Connect; override;
  public
  end;

var
  EditMatriz_DevEx: TEditMatriz_DevEx;

implementation

uses FTressShell, ZetaCommonClasses, dCatalogos, dGlobal, ZGlobalTress,
     ZBaseDlgModal_DevEx, ZAccesosTress, ZetaClientTools;

{$R *.DFM}

procedure TEditMatriz_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_CAPA_MATRIZ_CURSO;
     HelpContext:= H60643_Matriz_curso;
     FirstControl := PU_CODIGO;
     PU_CODIGO.LookupDataset := dmCatalogos.cdsPuestos;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TEditMatriz_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     ZCurso.Caption := Datasource.DataSet.FieldByName( 'CU_CODIGO' ).AsString + ' = ' + dmCatalogos.cdsCursos.FieldByName( 'CU_NOMBRE' ).AsString;
     gbProgramacion.Visible := Global.GetGlobalInteger( K_NIVEL_PROGRAMACION_CURSOS ) > 0;
     if gbProgramacion.Visible then
     begin
          gbProgramacion.Caption := 'Programaci�n por ' + Global.GetDescNivelProg(K_NIVEL_PROGRAMACION_CURSOS);
          Height := GetScaledHeight( 382  ); //+50 Se aument� la altura debido a los Large Fonts;  //DevEx (by am): Se agumentaron 3 px. por la nueva imagen
          //Height := GetScaledHeight( 325 ); //+50 Se aument� la altura debido a los Large Fonts;
     end
     else
         Height := GetScaledHeight( 275 ); //+50 Se aument� la altura debido a los Large Fonts0;  //DevEx (by am): Se agumentaron 3 px. por la nueva imagen
         //Height := GetScaledHeight( 208 ); //+50 Se aument� la altura debido a los Large Fonts0;
end;

procedure TEditMatriz_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsPuestos.Conectar;
          cdsMatrizCurso.Conectar;
          Datasource.Dataset := cdsMatrizCurso;
     end;
end;

procedure TEditMatriz_DevEx.gbProgramarAChange(Sender: TObject);
begin
     inherited;
     bModifica_DevEx.Enabled := ( gbProgramarA.Value = 'S' );
end;

procedure TEditMatriz_DevEx.OK_DevExClick(Sender: TObject);
begin
  if ( gbProgramarA.Value = 'N' ) then
        with dmCatalogos.cdsEntNivel do
        begin
             First;
             while not EOF do
                   Delete;
        end;
     inherited;
end;

procedure TEditMatriz_DevEx.bModifica_DevExClick(Sender: TObject);
begin
  inherited;
     ShowDlgModal( CatEntNivel_DevEx, TCatEntNivel_DevEx );

end;

procedure TEditMatriz_DevEx.SetEditarSoloActivos;
begin
     PU_CODIGO.EditarSoloActivos := TRUE;
end;

end.



