inherited CosteoCriterios_DevEx: TCosteoCriterios_DevEx
  Left = 237
  Top = 144
  Caption = 'Criterios de Costeo'
  ClientHeight = 212
  ClientWidth = 535
  ExplicitWidth = 535
  ExplicitHeight = 212
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 535
    ExplicitWidth = 535
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 83
      end
    end
    inherited ValorActivo2: TPanel
      Width = 276
      ExplicitWidth = 276
      inherited textoValorActivo2: TLabel
        Left = 190
        Width = 83
        ExplicitLeft = 190
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 535
    Height = 193
    ExplicitWidth = 535
    ExplicitHeight = 193
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CritCostoID: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CritCostoID'
        MinWidth = 70
        Width = 70
      end
      object CritCostoNombre: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'CritCostoNombre'
        MinWidth = 100
        Width = 100
      end
      object CritCostoActivo: TcxGridDBColumn
        Caption = 'Activo'
        DataBinding.FieldName = 'CritCostoActivo'
        MinWidth = 70
        Width = 70
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 408
    Top = 24
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
