unit FEditCatTPeriodos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls, DBCtrls, Buttons, StdCtrls,
  ZetaStateComboBox, Mask, ZetaNumero, ZetaDBTextBox, ZetaKeyCombo,
  ComCtrls, ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC, dxBarExtItems, dxBar,
  cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons, cxContainer,
  cxEdit, cxTextEdit, cxMemo, cxDBEdit, cxListBox, dxBarBuiltInMenu, cxMaskEdit,
  cxDropDownEdit, ZetaCXStateComboBox;

type
  TEditCatTPeriodos_DevEx = class(TBaseEdicion_DevEx)
    lblNombre: TLabel;
    lblDescripcion: TLabel;
    Label3: TLabel;
    TP_DIAS: TZetaDBNumero;
    Label4: TLabel;
    Label6: TLabel;
    TP_HORAS: TZetaDBNumero;
    GBDiasBase: TGroupBox;
    TP_DIAS_7: TZetaDBNumero;
    Label7: TLabel;
    LblDiasFijos: TLabel;
    TP_HORASJO: TDBCheckBox;
    Label5: TLabel;
    TP_DIAS_EV: TcxDBMemo;
    SBFormulaDias: TcxButton;
    TP_DESCRIP: TDBEdit;
    TP_DIAS_BT: TZetaDBKeyCombo;
    PageControl: TcxPageControl;
    tsGenerales: TcxTabSheet;
    tsConfidencialidad: TcxTabSheet;
    Panel1: TPanel;
    gbConfidencialidad: TGroupBox;
    btSeleccionarConfiden: TcxButton;
    listaConfidencialidad: TcxListBox;
    rbConfidenTodas: TRadioButton;
    rbConfidenAlgunas: TRadioButton;
    TabSheet1: TcxTabSheet;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    spTotalAsistencia1: TcxButton;
    spTotalAsistencia2: TcxButton;
    spTotalAsistencia3: TcxButton;
    spTotalAsistencia4: TcxButton;
    spTotalAsistencia5: TcxButton;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    TP_TOT_EV1: TcxDBMemo;
    TP_TOT_EV2: TcxDBMemo;
    TP_TOT_EV3: TcxDBMemo;
    TP_TOT_EV4: TcxDBMemo;
    TP_TOT_EV5: TcxDBMemo;
    lblClasificacion: TLabel;
    TP_CLAS: TZetaDBKeyCombo;
    TP_NOMBRE: TDBEdit;
    lblCodigo: TLabel;
    TP_TIPO: TZetaDBTextBox;
    procedure SBFormulaDiasClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TP_DIAS_BTChange(Sender: TObject);
    procedure FormShow(Sender: TObject);    
    procedure btSeleccionarConfidenClick(Sender: TObject);
    procedure rbConfidenTodasClick(Sender: TObject);
    procedure rbConfidenAlgunasClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure spTotalAsistencia1Click(Sender: TObject);
    procedure spTotalAsistencia2Click(Sender: TObject);
    procedure spTotalAsistencia3Click(Sender: TObject);
    procedure spTotalAsistencia4Click(Sender: TObject);
    procedure spTotalAsistencia5Click(Sender: TObject);
    procedure TP_CLASChange(Sender: TObject);
  private
    { Private declarations }
    lValoresConfidencialidad : TStringList;
    procedure HabilitaDiasBase;
    procedure FillListaConfidencialidad;
    procedure SetListConfidencialidad( sValores : string );
    procedure GetConfidencialidad;
    procedure AsignarValores;
  protected
    { Protected declarations }
     procedure Connect; override;
  public
    { Public declarations }
  end;

var
  EditCatTPeriodos_DevEx: TEditCatTPeriodos_DevEx;

implementation

uses DCatalogos,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaTipoEntidad,
     ZAccesosTress,
     ZConstruyeFormula,
     ZetaDialogo,
     FSeleccionarConfidencialidad_DevEx, dSistema, DCliente, ZetaCommonTools;

{$R *.DFM}

procedure TEditCatTPeriodos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := TP_NOMBRE;
     IndexDerechos := D_CAT_TPERIODOS;
     HelpContext:= H_TIPOS_D_PERIODO;
     lValoresConfidencialidad := TStringList.Create;
end;

procedure TEditCatTPeriodos_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     HabilitaDiasBase;
     PageControl.ActivePage := tsGenerales;
     FillListaConfidencialidad;
     TP_CLAS.ListaFija :=  lfClasifiPeriodoConfidencial;
end;

procedure TEditCatTPeriodos_DevEx.TP_CLASChange(Sender: TObject);
begin
     inherited;
     AsignarValores;
end;

procedure TEditCatTPeriodos_DevEx.AsignarValores;
begin
       with dmCatalogos.cdsTPeriodos do
       begin
            if not ( Editing or Inserting ) then
               Edit;
            FieldByName( 'TP_CLAS' ).AsInteger := Ord(eClasifiPeriodo( StrToInt( TP_CLAS.Llave ) ));
            FieldByName( 'TP_DIAS' ).AsInteger := StrToInt( ObtieneElemento(lfTPDias, StrToInt( TP_CLAS.Llave )));
            FieldByName( 'TP_DIAS_7' ).AsFloat := StrToFloat( ObtieneElemento(lfTPDias7, StrToInt( TP_CLAS.Llave )));
            FieldByName( 'TP_HORAS' ).AsFloat := StrToInt( ObtieneElemento(lfTPHoras, StrToInt( TP_CLAS.Llave )));
            FieldByName( 'TP_DIAS_BT' ).AsInteger := StrToInt( ObtieneElemento(lfTPDiasBT, StrToInt( TP_CLAS.Llave )));
            FieldByName( 'TP_HORASJO' ).AsString := ObtieneElemento(lfTPHorasJo, StrToInt( TP_CLAS.Llave ));
       end;
end;

procedure TEditCatTPeriodos_DevEx.Connect;
begin
     dmSistema.cdsNivel0.Conectar;
     DataSource.DataSet := dmCatalogos.cdsTPeriodos;
end;

procedure TEditCatTPeriodos_DevEx.TP_DIAS_BTChange(Sender: TObject);
begin
     inherited;
     HabilitaDiasBase;
end;

procedure TEditCatTPeriodos_DevEx.HabilitaDiasBase;
var
   lEnabled : Boolean;
begin
     lEnabled := ( eTipoDiasBase( TP_DIAS_BT.Valor ) = dbFijos );
     LblDiasFijos.Enabled := lEnabled;
     TP_DIAS_7.Enabled := lEnabled;
end;

procedure TEditCatTPeriodos_DevEx.SBFormulaDiasClick(Sender: TObject);
begin
     inherited;
     with dmCatalogos.cdsTPeriodos do
     begin
          if not ( Editing or Inserting ) then
             Edit;
          FieldByName( 'TP_DIAS_EV' ).AsString := ZConstruyeFormula.GetFormulaUnlimited( enNomina, TP_DIAS_EV.Lines.Text, TP_DIAS_EV.SelStart, evNomina );
     end;
end;


procedure TEditCatTPeriodos_DevEx.btSeleccionarConfidenClick(Sender: TObject);
var
   selConfi : TSeleccionarConfidencialidad_DevEx;
   sCM_NIVEL0Nuevo : string;
begin
     inherited;

     selConfi := TSeleccionarConfidencialidad_DevEx.Create( Self );
     sCM_NIVEL0Nuevo := selConfi.GetConfidencialidad( DataSource.DataSet.FieldByName('TP_NIVEL0').AsString, lValoresConfidencialidad, True, dmCliente.Confidencialidad );

     if (  sCM_NIVEL0Nuevo <>  DataSource.DataSet.FieldByName('TP_NIVEL0').AsString ) then
     begin
          with DataSource.DataSet do
          begin
               if not( State in [dsEdit,dsInsert] ) then
                  Edit;

               FieldByName('TP_NIVEL0').AsString  := sCM_NIVEL0Nuevo;
               GetConfidencialidad;
          end;
     end;

     FreeAndNil ( selConfi ) ;
end;

procedure TEditCatTPeriodos_DevEx.FillListaConfidencialidad;
begin
     with dmSistema.cdsNivel0 do
     begin
          First;
          lValoresConfidencialidad.Clear;
          lValoresConfidencialidad.NameValueSeparator := '=' ;
          While( not Eof )do
          begin
               lValoresConfidencialidad.Add(FieldByName('TB_CODIGO').AsString +'='+ FieldByName('TB_ELEMENT').AsString);
               Next;
          end;
     end;
end;

procedure TEditCatTPeriodos_DevEx.GetConfidencialidad;
begin
     if (  DataSource.DataSet  <> nil ) then
     with DataSource.DataSet do
     begin
          SetListConfidencialidad(  FieldByName('TP_NIVEL0').AsString );
     end;
end;

procedure TEditCatTPeriodos_DevEx.SetListConfidencialidad(sValores: string);
var
   Lista : TStringList;
   i, j : integer;
begin
     Lista := TStringList.Create;

     sValores := Trim( sValores );
     if StrLleno( sValores ) then
        Lista.CommaText  := sValores;

     listaConfidencialidad.Items.Clear;

     rbConfidenTodas.OnClick := nil;
     rbConfidenAlgunas.OnClick := nil;
     rbConfidenTodas.Checked := False;
     rbConfidenAlgunas.Checked := False;

     if lValoresConfidencialidad.Count = 0 then
        FillListaConfidencialidad;

     for i:= 0 to lValoresConfidencialidad.Count - 1 do
     begin
          if ( Lista.Count > 0 ) then
          begin
               j := Lista.IndexOf( Trim( lValoresConfidencialidad.Names[i] )  );
               if ( j >= 0 ) then
                  listaConfidencialidad.Items.Add( lValoresConfidencialidad[i] )
          end
     end;

     rbConfidenTodas.Checked :=   listaConfidencialidad.Items.Count = 0;
     rbConfidenAlgunas.Checked := not rbConfidenTodas.Checked;

     if ( gbConfidencialidad.Enabled ) then
     begin
          listaConfidencialidad.Enabled := rbConfidenAlgunas.Checked;
          btSeleccionarConfiden.Enabled := rbConfidenAlgunas.Checked;
     end;

     rbConfidenTodas.OnClick := rbConfidenTodasClick;
     rbConfidenAlgunas.OnClick := rbConfidenAlgunasClick;

     FreeAndNil( Lista );
end;

procedure TEditCatTPeriodos_DevEx.rbConfidenTodasClick(Sender: TObject);
begin
     inherited;
     with DataSource.DataSet do
     begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

          FieldByName('TP_NIVEL0').AsString  :=  '';
     end;
     GetConfidencialidad;
end;

procedure TEditCatTPeriodos_DevEx.rbConfidenAlgunasClick(Sender: TObject);
begin
     inherited;
     if ( rbConfidenAlgunas.Checked ) then
     begin
          btSeleccionarConfidenClick( Sender );
          GetConfidencialidad;
     end;
end;

procedure TEditCatTPeriodos_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( Field = nil ) then
     begin
          with DataSource.DataSet do
          begin
               if ( State in [dsBrowse] ) then
               begin
                    GetConfidencialidad;
                    HabilitaDiasBase;
               end
               else if (State in [dsInsert]) then
                    rbConfidenTodas.Checked := TRUE
               else if(State in [dsEdit]) then
               begin
                    dmCatalogos.PeriodosClas := dmCatalogos.cdsTPeriodos.FieldByName('TP_CLAS').AsInteger;
               end;
          end;
     end;
end;

procedure TEditCatTPeriodos_DevEx.spTotalAsistencia1Click(Sender: TObject);
begin
     inherited;
     with dmCatalogos.cdsTPeriodos do
     begin
          if not ( Editing or Inserting ) then
             Edit;
          FieldByName( 'TP_TOT_EV1' ).AsString := ZConstruyeFormula.GetFormulaUnlimited( enNomina, TP_TOT_EV1.Lines.Text, TP_TOT_EV1.SelStart, evNomina );
     end;
end;

procedure TEditCatTPeriodos_DevEx.spTotalAsistencia2Click(Sender: TObject);
begin
     inherited;
     with dmCatalogos.cdsTPeriodos do
     begin
          if not ( Editing or Inserting ) then
             Edit;
          FieldByName( 'TP_TOT_EV2' ).AsString := ZConstruyeFormula.GetFormulaUnlimited( enNomina, TP_TOT_EV2.Lines.Text, TP_TOT_EV2.SelStart, evNomina );
     end;
end;

procedure TEditCatTPeriodos_DevEx.spTotalAsistencia3Click(Sender: TObject);
begin
     inherited;
     with dmCatalogos.cdsTPeriodos do
     begin
          if not ( Editing or Inserting ) then
             Edit;
          FieldByName( 'TP_TOT_EV3' ).AsString := ZConstruyeFormula.GetFormulaUnlimited( enNomina, TP_TOT_EV3.Lines.Text, TP_TOT_EV3.SelStart, evNomina );
     end;
end;

procedure TEditCatTPeriodos_DevEx.spTotalAsistencia4Click(Sender: TObject);
begin
     inherited;
     with dmCatalogos.cdsTPeriodos do
     begin
          if not ( Editing or Inserting ) then
             Edit;
          FieldByName( 'TP_TOT_EV4' ).AsString := ZConstruyeFormula.GetFormulaUnlimited( enNomina, TP_TOT_EV4.Lines.Text, TP_TOT_EV4.SelStart, evNomina );
     end;
end;

procedure TEditCatTPeriodos_DevEx.spTotalAsistencia5Click(Sender: TObject);
begin
     inherited;
     with dmCatalogos.cdsTPeriodos do
     begin
          if not ( Editing or Inserting ) then
             Edit;
          FieldByName( 'TP_TOT_EV5' ).AsString := ZConstruyeFormula.GetFormulaUnlimited( enNomina, TP_TOT_EV5.Lines.Text, TP_TOT_EV5.SelStart, evNomina );
     end;
end;

end.
