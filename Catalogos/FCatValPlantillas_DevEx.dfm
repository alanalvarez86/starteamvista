inherited CatValPlantillas_DevEx: TCatValPlantillas_DevEx
  Left = 278
  Top = 359
  Caption = 'Plantillas de Valuaci'#243'n'
  ClientWidth = 796
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 796
    inherited ValorActivo2: TPanel
      Width = 537
      inherited textoValorActivo2: TLabel
        Width = 531
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 796
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      object VL_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'VL_CODIGO'
        Options.Grouping = False
      end
      object VL_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'VL_NOMBRE'
        Options.Grouping = False
        Width = 64
      end
      object VL_FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'VL_FECHA'
        Options.Grouping = False
        Width = 64
      end
      object VL_MAX_PTS: TcxGridDBColumn
        Caption = 'Puntos'
        DataBinding.FieldName = 'VL_MAX_PTS'
        Options.Grouping = False
      end
      object VL_NUM_FAC: TcxGridDBColumn
        Caption = 'Factores'
        DataBinding.FieldName = 'VL_NUM_FAC'
        Options.Grouping = False
      end
      object VL_NUM_SUB: TcxGridDBColumn
        Caption = 'Subfactores'
        DataBinding.FieldName = 'VL_NUM_SUB'
        Options.Grouping = False
      end
      object VL_NUM_PTO: TcxGridDBColumn
        Caption = 'Valuaciones'
        DataBinding.FieldName = 'VL_NUM_PTO'
        Options.Grouping = False
      end
      object VL_STATUS: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'VL_STATUS'
        Width = 64
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
