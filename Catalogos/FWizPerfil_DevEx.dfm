inherited WizPerfil_DevEx: TWizPerfil_DevEx
  Left = 334
  Top = 214
  Caption = 'Perfil Nuevo'
  ClientHeight = 602
  ClientWidth = 424
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 424
    Height = 602
    inherited Parametros: TdxWizardControlPage
      Header.AssignedValues = [wchvGlyph]
      Header.Glyph.Data = {
        89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
        F4000000017352474200AECE1CE90000000467414D410000B18F0BFC61050000
        00097048597300000EBC00000EBC0195BC7249000001EA494441545847ED56A1
        4E034110452010086405028140202A9095157C0012814020101508DCF46AE8ED
        EEA582260804028940202A110804A202814020911508C40978734CDAB41972B7
        77DB6278C94B93EBCC9BB9D99DB959F2458FCE362CD9631BD94B17D9A18BCC33
        7EAF0D99169E6F8B597810D132829C20D827F8F50BD3A4632D6C57C42D0C10B8
        86377E5402AAB4917971E4B6C4BD3A10FC4E0B94C321574D24CAC3913950C40B
        1189B745A61CF82C51CE91265E906997BAEB22E78F98E21D45D48BE88C3D91F3
        07B79B26EA43EE0A91F387F4BA2AECC181C8F983B357043D696E44CE1F7C7EBA
        6871F2F012397F24946C6AA23E44024D912B079F09A8F0ADF258E6918A59F0A1
        88E731856F4364AAA14C3B566A3F0D3C928B4C45AE16272C6E61C1631577E216
        81B44F720A0E785F10F3F9417683BAA3F890DF964776F01D80C16DC87DCCC1E4
        516154F1C5A58B8F504EAC5B931273E98B8871E024B257F0E123115FF38A3B74
        9ABB1FF4A9BF2A673C0E3C4B883DB88EBB40322D70176C665DD231E718BBF7B0
        19079E25FBC2BE26E1A6912D9B9CA9E21898EF7C6724EC04796F1E984F53C7C1
        A5548CE64ABE67121E43E667B7570DE7451CF728AB428F7A6B9AC12288CAD7F9
        63D3D0FE5C0491C07E90BDAF2CB30F166E7F5BFB7311E481F59FC01F2760AFBE
        01F7CB61192659F11B0000000049454E44AE426082}
      Header.Description = 
        'Este proceso te permite crear perfiles de puestos correspondient' +
        'es a un puesto.'
      Header.Title = 'Perfil nuevo'
      object Label1: TLabel
        Left = 31
        Top = 60
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Puesto:'
        Transparent = True
      end
      object Label2: TLabel
        Left = 34
        Top = 92
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha:'
        Transparent = True
      end
      object PU_CODIGO: TZetaDBKeyLookup_DevEx
        Left = 71
        Top = 56
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        DataField = 'PU_CODIGO'
        DataSource = DataSource
      end
      object PF_FECHA: TZetaDBFecha
        Left = 71
        Top = 87
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 1
        Text = '05/abr/06'
        Valor = 38812.000000000000000000
        DataField = 'PF_FECHA'
        DataSource = DataSource
      end
      object GroupBox1: TcxGroupBox
        Left = 38
        Top = 131
        Caption = ' C'#243'digos de Control '
        TabOrder = 2
        Height = 118
        Width = 331
        object Label3: TLabel
          Left = 20
          Top = 37
          Width = 9
          Height = 13
          Alignment = taRightJustify
          Caption = '1:'
          Transparent = True
        end
        object Label4: TLabel
          Left = 20
          Top = 67
          Width = 9
          Height = 13
          Alignment = taRightJustify
          Caption = '2:'
          Transparent = True
        end
        object PF_CONTRL1: TDBEdit
          Left = 33
          Top = 33
          Width = 287
          Height = 21
          DataField = 'PF_CONTRL1'
          DataSource = DataSource
          TabOrder = 0
        end
        object PF_CONTRL2: TDBEdit
          Left = 33
          Top = 63
          Width = 287
          Height = 21
          DataField = 'PF_CONTRL2'
          DataSource = DataSource
          TabOrder = 1
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 367
        Width = 402
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 402
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 334
          AnchorY = 51
        end
      end
    end
    object Descripcion: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = 'Ingrese las descripciones generales del perfil de puesto.'
      Header.Title = 'Descripci'#243'n General '
      object Label5: TLabel
        Left = 31
        Top = 52
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Puesto:'
        Transparent = True
      end
      object PU_CODIGO_2: TZetaDBTextBox
        Left = 71
        Top = 48
        Width = 60
        Height = 21
        AutoSize = False
        Caption = 'PU_CODIGO_2'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'PU_CODIGO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object PU_DESCRIP: TZetaDBTextBox
        Left = 132
        Top = 48
        Width = 240
        Height = 21
        AutoSize = False
        Caption = 'PU_DESCRIP'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'PU_DESCRIP'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object PF_DESCRIP: TDBMemo
        Left = 31
        Top = 80
        Width = 341
        Height = 300
        Align = alCustom
        DataField = 'PF_DESCRIP'
        DataSource = DataSource
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object Objetivos: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = 'Ingrese los objetivos del perfil de puesto.'
      Header.Title = 'Objetivos del Puesto '
      object Label6: TLabel
        Left = 31
        Top = 52
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Puesto:'
        Transparent = True
      end
      object PU_CODIGO_3: TZetaDBTextBox
        Left = 71
        Top = 48
        Width = 60
        Height = 21
        AutoSize = False
        Caption = 'PU_CODIGO_3'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'PU_CODIGO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object PU_DESCRIP_3: TZetaDBTextBox
        Left = 132
        Top = 48
        Width = 240
        Height = 21
        AutoSize = False
        Caption = 'PU_DESCRIP_3'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'PU_DESCRIP'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object PF_OBJETIV: TDBMemo
        Left = 31
        Top = 80
        Width = 341
        Height = 300
        Align = alCustom
        DataField = 'PF_OBJETIV'
        DataSource = DataSource
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object Publicacion: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = 'Ingrese la publicaci'#243'n del perfil de puesto.'
      Header.Title = 'Publicaci'#243'n'
      object Label7: TLabel
        Left = 31
        Top = 52
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Puesto:'
        Transparent = True
      end
      object PU_CODIGO_4: TZetaDBTextBox
        Left = 71
        Top = 48
        Width = 60
        Height = 21
        AutoSize = False
        Caption = 'PU_CODIGO_4'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'PU_CODIGO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object PU_DESCRIP_4: TZetaDBTextBox
        Left = 132
        Top = 48
        Width = 240
        Height = 21
        AutoSize = False
        Caption = 'PU_DESCRIP_4'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'PU_DESCRIP'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object PF_POSTING: TDBMemo
        Left = 31
        Top = 80
        Width = 341
        Height = 300
        Align = alCustom
        DataField = 'PF_POSTING'
        DataSource = DataSource
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object Requisitos: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = 'Seleccione los requisitos del perfil de puesto.'
      Header.Title = 'Requisitos'
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 402
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label8: TLabel
          Left = 26
          Top = 14
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = 'Puesto:'
        end
        object PU_DESCRIP_5: TZetaDBTextBox
          Left = 127
          Top = 10
          Width = 240
          Height = 21
          AutoSize = False
          Caption = 'PU_DESCRIP_5'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'PU_DESCRIP'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object PU_CODIGO_5: TZetaDBTextBox
          Left = 66
          Top = 10
          Width = 60
          Height = 21
          AutoSize = False
          Caption = 'PU_CODIGO_5'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'PU_CODIGO'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
      object GroupBox5: TcxGroupBox
        Left = 0
        Top = 41
        Align = alClient
        Caption = ' Requisitos '
        ParentFont = False
        TabOrder = 1
        Height = 421
        Width = 402
        object Label10: TLabel
          Left = 68
          Top = 22
          Width = 58
          Height = 13
          Alignment = taRightJustify
          Caption = 'Escolaridad:'
          Transparent = True
        end
        object Label13: TLabel
          Left = 173
          Top = 84
          Width = 5
          Height = 13
          Alignment = taRightJustify
          Caption = 'y'
          Transparent = True
        end
        object Label12: TLabel
          Left = 71
          Top = 84
          Width = 55
          Height = 13
          Alignment = taRightJustify
          Caption = 'Edad entre:'
          Transparent = True
        end
        object Label11: TLabel
          Left = 27
          Top = 53
          Width = 99
          Height = 13
          Alignment = taRightJustify
          Caption = 'A'#241'os de experiencia:'
          Transparent = True
        end
        object anios: TLabel
          Left = 227
          Top = 84
          Width = 23
          Height = 13
          Caption = 'a'#241'os'
          Transparent = True
        end
        object PF_ESTUDIO: TZetaDBKeyCombo
          Left = 128
          Top = 18
          Width = 151
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 0
          ListaFija = lfEstudios
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
          DataField = 'PF_ESTUDIO'
          DataSource = DataSource
          LlaveNumerica = True
        end
        object PF_EXP_PTO: TZetaDBNumero
          Left = 128
          Top = 49
          Width = 41
          Height = 21
          Mascara = mnDias
          TabOrder = 1
          Text = '0'
          DataField = 'PF_EXP_PTO'
          DataSource = DataSource
        end
        object PF_EDADMIN: TZetaDBNumero
          Left = 128
          Top = 80
          Width = 41
          Height = 21
          Mascara = mnDias
          TabOrder = 2
          Text = '0'
          DataField = 'PF_EDADMIN'
          DataSource = DataSource
        end
        object PF_EDADMAX: TZetaDBNumero
          Left = 182
          Top = 80
          Width = 41
          Height = 21
          Mascara = mnDias
          TabOrder = 3
          Text = '0'
          DataField = 'PF_EDADMAX'
          DataSource = DataSource
        end
        object GroupBox6: TcxGroupBox
          Left = 3
          Top = 296
          Align = alBottom
          Caption = ' Numeros Adicionales '
          TabOrder = 6
          Height = 115
          Width = 396
          object Label9: TLabel
            Left = 113
            Top = 32
            Width = 9
            Height = 13
            Alignment = taRightJustify
            Caption = '1:'
            Transparent = True
          end
          object Label14: TLabel
            Left = 113
            Top = 62
            Width = 9
            Height = 13
            Alignment = taRightJustify
            Caption = '2:'
            Transparent = True
          end
          object PF_NUMERO1: TZetaDBNumero
            Left = 125
            Top = 27
            Width = 78
            Height = 21
            Mascara = mnNumeroGlobal
            TabOrder = 0
            Text = '0.00'
            DataField = 'PF_NUMERO1'
            DataSource = DataSource
          end
          object PF_NUMERO2: TZetaDBNumero
            Left = 125
            Top = 57
            Width = 78
            Height = 21
            Mascara = mnNumeroGlobal
            TabOrder = 1
            Text = '0.00'
            DataField = 'PF_NUMERO2'
            DataSource = DataSource
          end
        end
        object GroupBox7: TcxGroupBox
          Left = 3
          Top = 170
          Align = alBottom
          Caption = ' Textos Adicionales '
          TabOrder = 5
          Height = 126
          Width = 396
          object PF_TEXTO1_LBL: TLabel
            Left = 113
            Top = 41
            Width = 9
            Height = 13
            Alignment = taRightJustify
            Caption = '1:'
            Transparent = True
          end
          object PF_TEXTO2_LBL: TLabel
            Left = 113
            Top = 71
            Width = 9
            Height = 13
            Alignment = taRightJustify
            Caption = '2:'
            Transparent = True
          end
          object PF_TEXTO1: TDBEdit
            Left = 124
            Top = 36
            Width = 223
            Height = 21
            DataField = 'PF_TEXTO1'
            DataSource = DataSource
            TabOrder = 0
          end
          object PF_TEXTO2: TDBEdit
            Left = 124
            Top = 67
            Width = 223
            Height = 21
            DataField = 'PF_TEXTO2'
            DataSource = DataSource
            TabOrder = 1
          end
        end
        object PF_SEXO: TDBRadioGroup
          Left = 46
          Top = 113
          Width = 233
          Height = 41
          Caption = ' Sexo '
          Columns = 3
          DataField = 'PF_SEXO'
          DataSource = DataSource
          Items.Strings = (
            'Indistinto'
            'Masculino'
            'Femenino')
          TabOrder = 4
          Values.Strings = (
            'I'
            'M'
            'F')
        end
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 336
    Top = 88
  end
  object DataSource: TDataSource
    Left = 372
    Top = 95
  end
end
