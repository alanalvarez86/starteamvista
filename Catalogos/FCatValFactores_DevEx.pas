unit FCatValFactores_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ExtCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, StdCtrls, Mask, ZetaNumero,
  ZetaKeyLookup_DevEx, Menus,
  ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  ZetaCXGrid, cxButtons, ZetaSmartLists_DevEx;

type
  TCatValFactores_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Panel2: TPanel;
    zmlSubir: TZetaSmartListsButton_DevEx;
    zmlbajar: TZetaSmartListsButton_DevEx;
    VL_CODIGO: TZetaKeyLookup_DevEx;
    VL_MAX_PTS: TZetaNumero;
    VF_ORDEN: TcxGridDBColumn;
    VF_CODIGO: TcxGridDBColumn;
    VF_NOMBRE: TcxGridDBColumn;
    VF_MAX_PTS: TcxGridDBColumn;
    VF_NUM_SUB: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure zmlArribaAbajoClick(Sender: TObject);
    procedure VL_CODIGOValidKey(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
   { Public declarations }
   procedure DoLookup; override;
   function PuedeAgregar( var sMensaje: String ): Boolean; override;
   function PuedeBorrar( var sMensaje: String ): Boolean; override;
  end;

var
  CatValFactores_DevEx: TCatValFactores_DevEx;

implementation

uses DCatalogos,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaBuscador_DevEx,
     ZetaDialogo;

{$R *.dfm}

{ TCatValFactores }

procedure TCatValFactores_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H_VALUACION_FACTORES;
end;

procedure TCatValFactores_DevEx.Agregar;
begin
     dmCatalogos.cdsValFactores.Agregar;
end;

procedure TCatValFactores_DevEx.Borrar;
begin
     dmCatalogos.cdsValFactores.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatValFactores_DevEx.Modificar;
begin
     dmCatalogos.cdsValFactores.Modificar;
end;

procedure TCatValFactores_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsValPlantilla.Conectar;
          VL_CODIGO.Valor:= PlantillaActiva;
          if ( cdsValPlantilla.Locate( 'VL_CODIGO', PlantillaActiva, [] ) ) then
             VL_MAX_PTS.Valor:= dmCatalogos.cdsValPlantilla.FieldByName('VL_MAX_PTS').AsInteger;
          cdsValFactores.Conectar;
          DataSource.DataSet:= cdsValFactores;
     end;
end;

procedure TCatValFactores_DevEx.Refresh;
begin
     dmCatalogos.cdsValFactores.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatValFactores_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Factor', 'Factor', 'VF_ORDEN', dmCatalogos.cdsValFactores );
end;

procedure TCatValFactores_DevEx.zmlArribaAbajoClick(Sender: TObject);
var sMensaje:string;
begin
     if  PuedeModificar(sMensaje) then
     begin
          with dmCatalogos do
     	  begin
               if ( PlantillaEnDiseno ( cdsValFactores.FieldByName('VL_CODIGO').AsInteger, sMensaje, 'Cambiar Orden' ) ) then
                    SubeBajaOrden( eSubeBaja( TZetaSmartListsButton_DevEx(Sender).Tag ), cdsValFactores,cdsValFactores.FieldByName('VL_CODIGO').AsString,VACIO)
               else
                   ZInformation( Caption, sMensaje,0 );
     	  end;
     end
     else
         zInformation( Caption, 'No tiene permisos para cambiar el orden', 0 );
end;

procedure TCatValFactores_DevEx.VL_CODIGOValidKey(Sender: TObject);
begin
     inherited;
     dmCatalogos.PlantillaActiva := VL_CODIGO.Valor;
     with dmCatalogos.cdsValPlantilla do
     begin
          if ( Locate('VL_CODIGO', VL_CODIGO.Llave, [] ) ) then
             VL_MAX_PTS.Valor:= dmCatalogos.cdsValPlantilla.FieldByName('VL_MAX_PTS').AsInteger;
     end;
end;

procedure TCatValFactores_DevEx.DataSourceDataChange(Sender: TObject;Field: TField);
begin
     inherited;

     zmlSubir.Enabled := not NoHayDatos;
     zmlBajar.Enabled := not NoHayDatos;
end;

function TCatValFactores_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeAgregar( sMensaje );
     if Result then
          Result:= dmCatalogos.PlantillaEnDiseno( dmCatalogos.PlantillaActiva, sMensaje, 'Agregar' );
end;

function TCatValFactores_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeBorrar( sMensaje );
     if Result then
        Result:= dmCatalogos.PlantillaEnDiseno( dmCatalogos.PlantillaActiva, sMensaje, 'Borrar' );

end;

procedure TCatValFactores_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('VF_ORDEN'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

end.
