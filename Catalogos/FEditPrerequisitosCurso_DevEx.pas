unit FEditPrerequisitosCurso_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls, DBCtrls, Buttons, StdCtrls,
  ZetaDBTextBox, ZBaseEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, cxContainer, cxEdit, cxTextEdit, cxMemo,
  cxDBEdit, ZetaKeyLookup_DevEx, dxBarExtItems, dxBar, cxClasses, ImgList,
  cxNavigator, cxDBNavigator, cxButtons;

type
  TEditPrerequisitosCurso_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    ZCurso: TZetaTextBox;
    Label2: TLabel;
    CP_CURSO: TZetaDBKeyLookup_DevEx;
    CP_OPCIONA: TDBCheckBox;
    lblObservaciones: TLabel;
    CP_COMENTA: TcxDBMemo;
    CU_CODIGO: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SetEditarSoloActivos;
  private
    { Private declarations }
  protected
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  EditPrerequisitosCurso_DevEx: TEditPrerequisitosCurso_DevEx;

implementation

uses DCatalogos,
     ZAccesosTress,
     ZetaCommonClasses;

{$R *.DFM}

procedure TEditPrerequisitosCurso_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_PREREQUISITOS_CURSO;
     HelpContext:= H_Prerequisitos_curso;
     FirstControl := CP_CURSO;
     CP_CURSO.LookupDataset := dmCatalogos.cdsCursos;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TEditPrerequisitosCurso_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with dmCatalogos do
     begin
          ZCurso.Caption := cdsCursos.GetDescripcion( PrerequisitosCurso );
     end;
end;

procedure TEditPrerequisitosCurso_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsCursos.Conectar;
          cdsPrerequisitosCurso.Conectar;
          Datasource.Dataset := cdsPrerequisitosCurso;
     end;
end;

procedure TEditPrerequisitosCurso_DevEx.SetEditarSoloActivos;
begin
     CP_CURSO.EditarSoloActivos := TRUE;
end;

end.
