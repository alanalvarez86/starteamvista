inherited PercepcionesFijas_DevEx: TPercepcionesFijas_DevEx
  Left = 317
  Top = 192
  Caption = 'Percepciones Fijas'
  ClientHeight = 218
  ClientWidth = 474
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label22: TLabel [0]
    Left = 242
    Top = 6
    Width = 173
    Height = 13
    Caption = 'Percepciones  Seleccionadas:'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object Label20: TLabel [1]
    Left = 7
    Top = 6
    Width = 151
    Height = 13
    Caption = 'Percepciones Disponibles:'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  inherited PanelBotones: TPanel
    Top = 182
    Width = 474
    inherited OK_DevEx: TcxButton
      Left = 304
      Enabled = False
      OnClick = OK_DevExClick
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 384
      Cancel = True
    end
  end
  object ZetaSmartListsButton4: TZetaSmartListsButton_DevEx [3]
    Left = 439
    Top = 102
    Width = 25
    Height = 26
    Hint = 'Bajar Registro'
    Enabled = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 6
    OptionsImage.Glyph.Data = {
      F6060000424DF606000000000000360000002800000018000000180000000100
      180000000000C0060000C30E0000C30E000000000000000000008B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C908791968D978B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C908791E4E1E4EDEB
      ED968D978B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8D838E
      D9D5D9FFFFFFFFFFFFE4E1E49087918B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818CD3D0D4FFFFFFFFFFFFFFFFFFFFFFFFE0DDE08D838E8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818CCCC8CCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD9
      D5D98B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818CBEB8BFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFCCC8CC8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818CB5AEB5FFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3BEC48B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CAFA9B0FDFDFDFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      BCB6BD8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CA69F
      A7FAF9FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFDFDFDB1ABB28B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818CB1ABB2B7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B7B0
      B7B7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B5AEB58B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C}
    OptionsImage.Margin = 1
    Tipo = bsBajar
    SmartLists = ZetaSmartLists
  end
  object ZetaSmartListsButton3: TZetaSmartListsButton_DevEx [4]
    Left = 439
    Top = 75
    Width = 25
    Height = 26
    Hint = 'Subir Registro'
    Enabled = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 5
    OptionsImage.Glyph.Data = {
      F6060000424DF606000000000000360000002800000018000000180000000100
      180000000000C0060000C30E0000C30E000000000000000000008B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CACA5
      ACB7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B7
      B0B7B7B0B7B7B0B7B7B0B7B1ABB28B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C9D959EF4F3F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAF9FAA69FA78B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818CA69FA7FAF9FAFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDB1ABB28B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CAAA3ABFD
      FDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7B0B7
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818CB1ABB2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFBEB8BF8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818CBEB8BFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFCCC8CC8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CC7C2C7
      FFFFFFFFFFFFFFFFFFFFFFFFD5D2D68B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818CCECACEFFFFFFFFFFFFDBD7DB8D838E8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8D838ED9D5D9E4E1E49087918B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8D838E9087
      918B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C}
    OptionsImage.Margin = 1
    Tipo = bsSubir
    SmartLists = ZetaSmartLists
  end
  object ZetaSmartListsButton2: TZetaSmartListsButton_DevEx [5]
    Left = 207
    Top = 102
    Width = 26
    Height = 26
    Hint = 'Rechazar Registro'
    Enabled = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
    OptionsImage.Glyph.Data = {
      F6060000424DF606000000000000360000002800000018000000180000000100
      180000000000C0060000C30E0000C30E000000000000000000008B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818CA69FA7B1ABB28B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818CB1ABB2FAF9FAB7B0B78B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818CB7B0B7FDFDFDFFFFFFB7B0B78B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818CC0BAC0FFFFFFFFFFFFFF
      FFFFB7B0B78B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CCCC8CCFFFF
      FFFFFFFFFFFFFFFFFFFFB7B0B78B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8D838E
      D5D2D6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7B0B78B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C908791DBD7DBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7B0B78B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C928993E4E1E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFB7B0B78B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8D838ED9D5D9FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFB7B0B78B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8D838ECECACE
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7B0B78B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818CC7C2C7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7B0B78B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818CBEB8BFFFFFFFFFFFFFFFFFFFFF
      FFFFB7B0B78B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CB1AB
      B2FDFDFDFFFFFFFFFFFFB7B0B78B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818CAAA3ABFAF9FAFFFFFFB7B0B78B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818CA69FA7F4F3F4B7B0B78B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C9D
      959EACA5AC8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C}
    OptionsImage.Margin = 1
    Tipo = bsRechazar
    SmartLists = ZetaSmartLists
  end
  object ZetaSmartListsButton1: TZetaSmartListsButton_DevEx [6]
    Left = 207
    Top = 75
    Width = 26
    Height = 26
    Hint = 'Escoger Registro'
    Enabled = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    OptionsImage.Glyph.Data = {
      F6060000424DF606000000000000360000002800000018000000180000000100
      180000000000C0060000C30E0000C30E000000000000000000008B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818CB5AEB5B1ABB28B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CB7B0B7FDFDFDBCB6BD
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CB7
      B0B7FFFFFFFFFFFFC3BEC48B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818CB7B0B7FFFFFFFFFFFFFFFFFFCCC8CC8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818CB7B0B7FFFFFFFFFFFFFFFFFFFFFFFFD9D5
      D98D838E8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CB7B0B7FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFE0DDE09087918B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CB7
      B0B7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE4E1E4968D978B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818CB7B0B7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFED
      EBED968D978B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818CB7B0B7FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFE4E1E49087918B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CB7B0B7FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFD9D5D99087918B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CB7
      B0B7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD3D0D48D838E8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818CB7B0B7FFFFFFFFFFFFFFFFFFFFFFFFCAC6CB8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818CB7B0B7FFFFFFFFFFFFFFFFFFBEB8BF8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CB7B0B7FFFFFFFDFDFD
      B5AEB58B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CB7
      B0B7FAF9FAAFA9B08B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818CAFA9B0A69FA78B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
      8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
      8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
      818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C}
    OptionsImage.Margin = 1
    Tipo = bsEscoger
    SmartLists = ZetaSmartLists
  end
  object SBMas_DevEx: TcxButton [7]
    Left = 439
    Top = 21
    Width = 25
    Height = 26
    Hint = 'Consultar Detalle de Percepciones'
    Caption = 'SBMas_DevEx'
    Enabled = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 7
    OnClick = SBMas_DevExClick
    OptionsImage.Glyph.Data = {
      36090000424D3609000000000000360000002800000018000000180000000100
      20000000000000090000000000000000000000000000000000008B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFCCC8CCFFCECA
      CEFF8D838EFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFB5AEB5FFFFFFFFFFFFFF
      FFFFE4E1E4FF8F8590FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF9F97A0FFFAF9FAFFFFFFFFFFFFFF
      FFFFD2CED2FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF908791FFEFEDEFFFFFFFFFFFFFFFFFFFE9E7
      E9FF8F8590FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFFDBD7DBFFFFFFFFFFFFFFFFFFF8F7F8FF9A91
      9AFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFA49DA5FFD9D5D9FFFDFD
      FDFFFFFFFFFFF4F3F4FFE4E1E4FFFFFFFFFFFFFFFFFFFFFFFFFFACA5ACFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFFB1ABB2FFFDFDFDFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC7C2C7FF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF9B939CFFFDFDFDFFFFFFFFFFEDEBEDFFB5AE
      B5FFA8A1A9FFBEB8BFFFF4F3F4FFFFFFFFFFFFFFFFFF988F99FF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFFD3D0D4FFFFFFFFFFEBE9EBFF908791FF8B81
      8CFF8B818CFF8B818CFF988F99FFF4F3F4FFFFFFFFFFCAC6CBFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFFF0EFF1FFFFFFFFFFB3ACB4FF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFFC3BEC4FFFFFFFFFFE7E5E8FF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFFFFFFFFFFFFFFFFFFA69FA7FF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFFB7B0B7FFFFFFFFFFF0EFF1FF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFFEFEDEFFFFFFFFFFFB8B2B9FF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFFCCC8CCFFFFFFFFFFE4E1E4FF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFFCCC8CCFFFFFFFFFFF4F3F4FF968D97FF8B81
      8CFF8B818CFF8B818CFF9F97A0FFFBFBFBFFFFFFFFFFC2BCC2FF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF988F99FFF4F3F4FFFFFFFFFFF4F3F4FFC2BC
      C2FFB7B0B7FFC7C2C7FFFAF9FAFFFFFFFFFFF2F1F2FF8F8590FF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFFA49DA5FFF8F7F8FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFEBE9EBFF988F99FF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF928993FFC5C0C6FFE2DF
      E2FFF0EFF1FFE0DDE0FFBCB6BDFF908791FF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
    PaintStyle = bpsGlyph
  end
  object LBSeleccion: TZetaSmartListBox_DevEx [8]
    Left = 242
    Top = 21
    Width = 190
    Height = 150
    ItemHeight = 13
    TabOrder = 4
  end
  object LBDisponibles: TZetaSmartListBox_DevEx [9]
    Left = 7
    Top = 21
    Width = 190
    Height = 150
    ItemHeight = 13
    TabOrder = 1
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  object ZetaSmartLists: TZetaSmartLists_DevEx
    BorrarAlCopiar = True
    CopiarObjetos = True
    ListaDisponibles = LBDisponibles
    ListaEscogidos = LBSeleccion
    AlBajar = ZetaSmartListsCambios
    AlEscoger = ZetaSmartListsCambios
    AlRechazar = ZetaSmartListsCambios
    AlSeleccionar = ZetaSmartListsAlSeleccionar
    AlSubir = ZetaSmartListsCambios
    Left = 256
    Top = 168
  end
end
