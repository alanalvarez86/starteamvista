inherited BusquedaConceptos_Version_DevEx: TBusquedaConceptos_Version_DevEx
  Left = 656
  Top = 352
  Caption = 'BusquedaConceptos_Version_DevEx'
  ClientWidth = 495
  ExplicitWidth = 511
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 495
    ExplicitWidth = 664
    inherited Cancelar_DevEx: TcxButton
      Left = 416
      ExplicitLeft = 585
    end
    inherited OK_DevEx: TcxButton
      Left = 336
      ExplicitLeft = 505
    end
  end
  inherited PanelSuperior: TPanel
    Width = 495
    ExplicitWidth = 664
    inherited MostrarActivos: TCheckBox
      Left = 339
      ExplicitLeft = 508
    end
    inherited Refrescar_DevEx: TcxButton
      Top = 4
      ExplicitTop = 4
    end
  end
  inherited DBGrid_DevEx: TZetaCXGrid
    Width = 495
    ExplicitWidth = 664
    inherited DBGrid_DevExDBTableView: TcxGridDBTableView
      object DBGrid_DevExDBTableViewColumn1: TcxGridDBColumn
        Caption = 'N'#250'mero SAT'
        DataBinding.FieldName = 'TB_SAT_NUM'
        Width = 77
      end
    end
  end
  inherited cxStyleRepository1: TcxStyleRepository
    PixelsPerInch = 96
  end
end
