unit FEditCatRSocial_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ExtCtrls, DBCtrls, ZetaSmartLists, Buttons,
  StdCtrls, ComCtrls, Mask, ZetaEdit, ZetaNumero,
  ZBaseEdicion_DevEx, ZetaKeyLookup_DevEx,
  {$IFDEF TRESS_DELPHIXE5_UP}
  Chilkat_v9_5_0_TLB,
  {$ELSE}
  CHILKATCERTIFICATELib_TLB,
  {$ENDIF}
  cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons,
  cxContainer, cxEdit, cxTextEdit, cxMemo, cxDBEdit, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, cxPC, cxGroupBox, cxCheckBox, OleCtrls;

type
  TEditCatRSocial_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    PageControl: TcxPageControl;
    tsIdentificacion: TcxTabSheet;
    tsCapacitacion: TcxTabSheet;
    Label1: TLabel;
    Label2: TLabel;
    RS_CODIGO: TZetaDBEdit;
    Label13: TLabel;
    Label3: TLabel;
    Label14: TLabel;
    Label16: TLabel;
    Label15: TLabel;
    Label17: TLabel;
    Label4: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    RS_CALLE: TDBEdit;
    RS_NUMEXT: TDBEdit;
    RS_NUMINT: TDBEdit;
    RS_COLONIA: TDBEdit;
    RS_CIUDAD: TDBEdit;
    RS_CODPOST: TDBEdit;
    RS_TEL: TDBEdit;
    RS_FAX: TDBEdit;
    RS_EMAIL: TDBEdit;
    RS_WEB: TDBEdit;
    RS_RFC: TDBEdit;
    RS_INFO: TDBEdit;
    RS_GUIA: TDBEdit;
    RS_ENTIDAD: TZetaDBKeyLookup_DevEx;
    RS_NOMBRE: TDBEdit;
    GBSecTrabajo: TcxGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    Label20: TLabel;
    RS_STPS: TDBEdit;
    RS_STPS_R1: TDBEdit;
    RS_STPS_R2: TDBEdit;
    Label22: TLabel;
    RS_GIRO: TcxDBMemo;
    RS_SUBSID: TZetaDBNumero;
    TabSheet1: TcxTabSheet;
    Label23: TLabel;
    RS_CURP: TDBEdit;
    GroupBox1: TcxGroupBox;
    Label21: TLabel;
    RS_LEGAL: TDBEdit;
    Label24: TLabel;
    RS_RL_RFC: TDBEdit;
    RS_RL_CURP: TDBEdit;
    Label25: TLabel;
    gbSello: TcxGroupBox;
    lbKey: TLabel;
    lbCert: TLabel;
    BuscarCertificado: TcxButton;
    BuscarLlavePrivada: TcxButton;
    ArchivoCertificado: TEdit;
    ArchivoLlavePrivada: TEdit;
    bValidaArchivosSello: TcxButton;
    OpenDialog: TOpenDialog;
    RS_SELLO: TDBCheckBox;
    Certificado: TcxMemo;
    procedure FormCreate(Sender: TObject);
    procedure BuscarLlavePrivadaClick(Sender: TObject);
    procedure BuscarCertificadoClick(Sender: TObject);
    procedure bValidaArchivosSelloClick(Sender: TObject);
    procedure RS_SELLOClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    function ConvierteCertificado(const sCertificate: string; var sMensaje: string): Boolean;
    function ConvierteLlavePrivada(const sKeyFile: string; var sMensaje: string; var lCancelado: Boolean ): Boolean;
    procedure SetCertificadoMsg;
    { Private declarations }
   protected
     procedure Connect; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  EditCatRSocial_DevEx: TEditCatRSocial_DevEx;

implementation

{$R *.dfm}

uses DCatalogos,
     ZAccesosTress,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo,
     ZetaClientTools,
     ZetaBuscador_DevEx,
     FPreguntaPassWordSAT_DevEx,
     {$IFDEF TRESS_DELPHIXE5_UP}
     FChilkatError,
     {$ENDIF}
     dTablas;

{
  AL. 2014-1015.
  Bug 7705:  Cambi� el mensaje de error/validaci�n cuando se intenta cargar certificado con clave incorrecta.
  Los mensajes de error al procesar los archivos .key y .cer se mostrar�n en una pantalla especial.
}
{const
     K_MENSAJE = 'Las posibles causas que no le permiten continuar pueden ser:' + CR_LF +
                 '1. La contrase�a de la llave privada es incorrecta.' + CR_LF +
                 '2. No tiene registrados debidamente los Dlls del cliente o servidor. ' + CR_LF +
                 '3. Los archivos de la llave privada y certificado no coinciden.' + CR_LF +
                 '4. Los archivos de la llave privada o certificado no fueron expedidos por el SAT.' + CR_LF +
                 'Si el problema contin�a, env�e esta pantalla a su asesor para obtener asistencia:' + CR_LF ;}


{ TEditCatRSocial }

procedure TEditCatRSocial_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := RS_CODIGO;
     IndexDerechos := D_CAT_GRALES_SOCIALES;;
     HelpContext:= H_CAT_RAZONES_SOCIALES;
     PageControl.ActivePage:= tsIdentificacion;
     RS_ENTIDAD.LookupDataSet:= dmTablas.cdsEstado;
end;

procedure TEditCatRSocial_DevEx.Connect;
begin
     inherited;
     dmTablas.cdsEstado.Conectar;
     DataSource.DataSet := dmCatalogos.cdsRSocial;

     SetCertificadoMsg;
end;

procedure TEditCatRSocial_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Self.Caption, 'RS_CODIGO', dmCatalogos.cdsRSocial );
end;

procedure TEditCatRSocial_DevEx.BuscarLlavePrivadaClick(Sender: TObject);
begin
     inherited;
     with ArchivoLlavePrivada do
     begin
          OpenDialog.Filter := VACIO;
          Text := AbreDialogo( OpenDialog, Text, 'Llave Privada|*.key' )
     end;
end;

procedure TEditCatRSocial_DevEx.BuscarCertificadoClick(Sender: TObject);
begin
     inherited;
     with ArchivoCertificado do
     begin
          OpenDialog.Filter := VACIO;
          Text := AbreDialogo( OpenDialog, Text,  'Certificado|*.cer' )
     end;

end;

function TEditCatRSocial_DevEx.ConvierteLlavePrivada( const sKeyFile: string; var sMensaje: string; var lCancelado: Boolean ) : Boolean;
var
   privKey: IprivateKey;
   iSuccess: Integer;
   sPassword: string;

function GetPassWord: string;
begin
     Result := VACIO;
     if FPreguntaPassWordSAT_DevEx.GetPassword then
        Result := PreguntaPassWordSAT_DevEx.Clave
     else
     begin
          lCancelado := TRUE;
          ZWarning( Caption, 'Es necesario proporcionar la clave de la llave privada expedida por el SAT, para poder continuar' , 0, mbOK);
          Abort;
     end;
end;
begin
     // Procedimiento para cargar y convertir el .key a formato PEM//
     Result := FALSE;
     lCancelado := FALSE;
     try
        sPassword := GetPassWord;
        if not lCancelado then
        begin
             privKey := TprivateKey.Create(nil).ControlInterface;
             iSuccess := privKey.LoadPkcs8EncryptedFile( ArchivoLlavePrivada.Text, sPassword );
             Result := iSuccess = 1;
             if Result then
             begin
                  if DataSource.DataSet.State = dsBrowse then
                     DataSource.DataSet.Edit;
                  DataSource.DataSet.FieldByName('RS_KEY_PR').AsString := privKey.GetXML;
             end
             else
                 sMensaje :=  privKey.LastErrorText;
        end;
     except
           on Error: Exception do
              sMensaje := Error.Message;
     end;
end;

function TEditCatRSocial_DevEx.ConvierteCertificado( const sCertificate: string; var sMensaje: string ) : Boolean;
var
   Cert: TChilkatCert;
   publicKey: IPublicKey;
   iSuccess: Integer;
begin
     Result := FALSE;
     try
        // Procedimiento para cargar y convertir el .cer a formato PEM//
        Cert := TChilkatCert.Create( nil );
        try
           iSuccess := Cert.LoadFromFile( sCertificate );

           Result := iSuccess = 1;

           if Result then
           begin
                with Cert do
                begin
                     publicKey := ExportPublicKey() As IPublicKey;
                     if DataSource.DataSet.State = dsBrowse then
                        DataSource.DataSet.Edit;

                     DataSource.DataSet.FieldByName('RS_CERT').AsString := Cert.ExportCertPem;
                     DataSource.DataSet.FieldByName('RS_SERIAL').AsString := Cert.SerialNumber;
                     DataSource.DataSet.FieldByName('RS_ISSUETO').AsString := Cert.SubjectCN ;
                     DataSource.DataSet.FieldByName('RS_ISSUEBY').AsString := Cert.IssuerCN ;
                     DataSource.DataSet.FieldByName('RS_VAL_INI').AsDateTime := Cert.ValidFrom ;
                     DataSource.DataSet.FieldByName('RS_VAL_FIN').AsDateTime := Cert.ValidTo ;
                end;
                with publicKey do
                begin
                     DataSource.DataSet.FieldByName('RS_KEY_PU').AsString := GetXML;
                end;
           end
           else
               sMensaje := Cert.LastErrorText;


        if NOT ( ( Date >= DataSource.DataSet.FieldByName('RS_VAL_INI').AsDateTime ) and
               ( Date <= DataSource.DataSet.FieldByName('RS_VAL_FIN').AsDateTime ) ) then
        begin
             Result := FALSE;
             sMensaje := 'El certificado no est� vigente'
        end;

        except
              on Error: Exception do
                 sMensaje := Error.Message;
        end;
     finally
            FreeAndNil( Cert );
     end;
end;

procedure TEditCatRSocial_DevEx.bValidaArchivosSelloClick(Sender: TObject);
 var sMensaje: string;
     lCancelado: Boolean;
begin
     inherited;
     if StrVacio( ArchivoLlavePrivada.Text ) or StrVacio( ArchivoCertificado.Text ) then
     begin
          ZError( Caption, 'No est�n indicadas las dos rutas de los archivos.' + CR_LF +
                           'Se requieren los dos archivos para poder generar el Sello Digital en la Declaraci�n', 0 );
     end
     else
     begin

          if NOT ConvierteLlavePrivada( ArchivoLlavePrivada.Text, sMensaje, lCancelado )  then
          begin
               if NOT lCancelado then
               begin
                    {$IFDEF TRESS_DELPHIXE5_UP}
                    // Invocar nueva ventana de error Chilkat.
                    with TChilkatError.Create(nil) do
                    begin
                          Error := 'Error al cargar archivo de llave privada (*.key)';
                          ErrorDetalle := sMensaje;
                          ShowModal;
                    end;
                    {$ELSE}
                    ZError( Caption, sMensaje, 0 );
                    {$ENDIF}
               end;
          end
          else
          begin
               if ConvierteCertificado( ArchivoCertificado.Text, sMensaje ) then
               begin
                    SetCertificadoMsg;
               end
               else
               begin
                    {$IFDEF TRESS_DELPHIXE5_UP}
                    // Invocar nueva ventana de error Chilkat.
                    with TChilkatError.Create(nil) do
                    begin
                          Error := 'Error al cargar archivo del certificado (*.cer)';
                          ErrorDetalle := sMensaje;
                          ShowModal;
                    end;
                    {$ELSE}
                    ZError( Caption, sMensaje, 0 );
                    {$ENDIF}
               end;
          end;
     end;
end;

procedure TEditCatRSocial_DevEx.SetCertificadoMsg;
begin
     Certificado.Lines.Clear;

     if zStrToBool( DataSource.DataSet.FieldByName('RS_SELLO').AsString ) then
        with Certificado.Lines do
        begin
             Add( 'Serie: ' + DataSource.DataSet.FieldByName('RS_SERIAL').AsString );
             Add( 'Emitido a: ' + DataSource.DataSet.FieldByName('RS_ISSUETO').AsString );
             Add( 'Emitido por: ' + DataSource.DataSet.FieldByName('RS_ISSUEBY').AsString );
             Add( Format( 'Vigente desde %s hasta %s ' ,
                          [ FechaCorta( DataSource.DataSet.FieldByName('RS_VAL_INI').AsDateTime ),
                            FechaCorta( DataSource.DataSet.FieldByName('RS_VAL_FIN').AsDateTime ) ] ) );
        end;
end;

procedure TEditCatRSocial_DevEx.RS_SELLOClick(Sender: TObject);
 var
    lEnabled : Boolean;
begin

     inherited;
     lEnabled:= RS_SELLO.Checked;

     lbKey.Enabled := lEnabled;
     lbCert.Enabled := lEnabled;
     ArchivoCertificado.Enabled := lEnabled;
     ArchivoLlavePrivada.Enabled := lEnabled;
     BuscarCertificado.Enabled := lEnabled;
     BuscarLlavePrivada.Enabled := lEnabled;
     bValidaArchivosSello.Enabled := lEnabled;
     Certificado.Enabled := lEnabled;

end;

procedure TEditCatRSocial_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     RS_SELLOClick( nil );
     ArchivoLlavePrivada.Text := '';
     ArchivoCertificado.Text := '';
end;

end.
