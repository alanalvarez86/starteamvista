unit FCatFestGrales_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls,
  Grids, DBGrids, ZetaDBGrid, StdCtrls, ZBaseGridLectura_DevEx, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TCatFestGrales_DevEx = class(TBaseGridLectura_DevEx)
    FE_MES: TcxGridDBColumn;
    FE_DIA: TcxGridDBColumn;
    FE_YEAR: TcxGridDBColumn;
    FE_DESCRIP: TcxGridDBColumn;
    FE_TIPO: TcxGridDBColumn;
    FE_CAMBIO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
  protected
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  CatFestGrales_DevEx: TCatFestGrales_DevEx;

implementation

uses dCatalogos, ZetaCommonClasses;

{$R *.DFM}

{ TCatFestGrales }

procedure TCatFestGrales_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H60632_Festivos_General;
end;

procedure TCatFestGrales_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          TurnoFestivo := TURNO_VACIO;
          cdsFestTurno.Conectar;
          DataSource.DataSet:= cdsFestTurno;
          Refresh;
     end;
end;

procedure TCatFestGrales_DevEx.Refresh;
begin
     dmCatalogos.cdsFestTurno.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatFestGrales_DevEx.Agregar;
begin
     dmCatalogos.cdsFestTurno.Agregar;
end;

procedure TCatFestGrales_DevEx.Borrar;
begin
     dmCatalogos.cdsFestTurno.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatFestGrales_DevEx.Modificar;
begin
     dmCatalogos.cdsFestTurno.Modificar;
end;

procedure TCatFestGrales_DevEx.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
  inherited;

   //columna sumatoria: cuantos
   CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('FE_MES'), K_SIN_TIPO , '', skCount);
   ZetaDBGridDBTableView.ApplyBestFit();


     //agrupar
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
  //ver la caja de agrupamiento
  ZetaDBGridDBTableView.OptionsView.GroupByBox := True;

end;

end.

