unit FEditCatClasifi_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, Db, ExtCtrls, dbClient,
  Buttons, DBCtrls, StdCtrls, Mask, ZetaClientDataset,
  ZetaNumero, ZetaEdit,   
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC;

type
  TEditCatClasifi_DevEx = class(TBaseEdicion_DevEx)
    dxBarButton_bImpSubCuentas: TdxBarButton;
    Panel1: TPanel;
    PageControl: TcxPageControl;
    TabGenerales: TcxTabSheet;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label4: TLabel;
    Label12: TLabel;
    TB_CODIGO: TZetaDBEdit;
    TB_ELEMENT: TDBEdit;
    TB_INGLES: TDBEdit;
    TB_NUMERO: TZetaDBNumero;
    TB_TEXTO: TDBEdit;
    TB_SALARIO: TZetaDBNumero;
    TB_SUB_CTA: TDBEdit;
    TB_ACTIVO: TDBCheckBox;
    TabPercepciones: TcxTabSheet;
    GBFijas: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    TB_OP1: TZetaDBKeyLookup_DevEx;
    TB_OP2: TZetaDBKeyLookup_DevEx;
    TB_OP3: TZetaDBKeyLookup_DevEx;
    TB_OP4: TZetaDBKeyLookup_DevEx;
    TB_OP5: TZetaDBKeyLookup_DevEx;
    TabConfidencialidad: TcxTabSheet;
    gbConfidencialidad: TGroupBox;
    listaConfidencialidad: TListBox;
    rbConfidenTodas: TRadioButton;
    rbConfidenAlgunas: TRadioButton;
    btSeleccionarConfiden_DevEx: TcxButton;
    Label6: TLabel;
    procedure FormCreate(Sender: TObject);
    //procedure bImpSubCuentasClick(Sender: TObject);   
    //procedure btSeleccionarConfidenClick(Sender: TObject);
    procedure rbConfidenTodasClick(Sender: TObject);
    procedure rbConfidenAlgunasClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
    procedure dxBarButton_bImpSubCuentasClick(Sender: TObject);
    procedure btSeleccionarConfiden_DevExClick(Sender: TObject);
  private
    { Private declarations } 
    lValoresConfidencialidad : TStringList;
    procedure FillListaConfidencialidad;
    procedure SetListConfidencialidad( sValores : string );
    procedure GetConfidencialidad;
  protected
    procedure Connect; override;
    procedure DoLookup; override;
    procedure HabilitaControles;override;

  public
  end;

var
  EditCatClasifi_DevEx: TEditCatClasifi_DevEx;

implementation

uses dCatalogos,
     ZetaCommonClasses,
     ZAccesosTress,
     ZImportaSubCuentas,
     ZetaBuscador_DevEx,
     FSeleccionarConfidencialidad_DevEx, dSistema, DCliente, ZetaCommonTools;

{$R *.DFM}

procedure TEditCatClasifi_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_CONT_CLASIFI;
     FirstControl := TB_CODIGO;
     HelpContext:= H60612_Clasificaciones;

     with dmCatalogos do
     begin
          TB_OP1.LookupDataset := cdsOtrasPer;
          TB_OP2.LookupDataset := cdsOtrasPer;
          TB_OP3.LookupDataset := cdsOtrasPer;
          TB_OP4.LookupDataset := cdsOtrasPer;
          TB_OP5.LookupDataset := cdsOtrasPer;
     end;
     lValoresConfidencialidad := TStringList.Create;
end;

procedure TEditCatClasifi_DevEx.Connect;
begin
     dmSistema.cdsNivel0.Conectar;
     with dmCatalogos do
     begin
          cdsOtrasPer.Conectar;
          DataSource.DataSet:= cdsClasifi;
     end;
end;
 
procedure TEditCatClasifi_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  lValoresConfidencialidad := TStringList.Create;
  FillListaConfidencialidad;
end;

procedure TEditCatClasifi_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', 'Clasificaci�n', 'TB_CODIGO', dmCatalogos.cdsClasifi );
end;

{procedure TEditCatClasifi_DevEx.bImpSubCuentasClick(Sender: TObject);
begin
     inherited;
     ZImportaSubCuentas.ImportaArchivo( TZetaClientDataSet( DataSource.DataSet ), 'TB_CODIGO', 'TB_SUB_CTA' );
end; }

procedure TEditCatClasifi_DevEx.HabilitaControles;
begin
     inherited;
     dxBarButton_bImpSubCuentas.Enabled := not Editing;

end;

{procedure TEditCatClasifi_DevEx.btSeleccionarConfidenClick(Sender: TObject);
var
   selConfi : TSeleccionarConfidencialidad;
   sCM_NIVEL0Nuevo : string;
begin
  inherited;

  selConfi := TSeleccionarConfidencialidad.Create( Self );
  sCM_NIVEL0Nuevo := selConfi.GetConfidencialidad( DataSource.DataSet.FieldByName('TB_NIVEL0').AsString, lValoresConfidencialidad, True, dmCliente.Confidencialidad );

  if (  sCM_NIVEL0Nuevo <>  DataSource.DataSet.FieldByName('TB_NIVEL0').AsString ) then
  begin
     with DataSource.DataSet do
     begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

           FieldByName('TB_NIVEL0').AsString  := sCM_NIVEL0Nuevo;
           GetConfidencialidad;
     end;
  end;

  FreeAndNil ( selConfi ) ;
end; }

procedure TEditCatClasifi_DevEx.FillListaConfidencialidad;
begin
     with dmSistema.cdsNivel0 do
     begin
          First;
          lValoresConfidencialidad.Clear;
          lValoresConfidencialidad.NameValueSeparator := '=' ;
          While( not Eof )do
          begin
               lValoresConfidencialidad.Add(FieldByName('TB_CODIGO').AsString +'='+ FieldByName('TB_ELEMENT').AsString);
               Next;
          end;
     end;
end;

procedure TEditCatClasifi_DevEx.GetConfidencialidad;
begin
   if (  DataSource.DataSet  <> nil ) then
   with DataSource.DataSet do
   begin
        SetListConfidencialidad(  FieldByName('TB_NIVEL0').AsString );
   end;
end;

procedure TEditCatClasifi_DevEx.SetListConfidencialidad(sValores: string);
var
   Lista : TStringList;
   i, j : integer;
begin
    Lista := TStringList.Create;

    sValores := Trim( sValores );
    if StrLleno( sValores ) then
        Lista.CommaText  := sValores;

    listaConfidencialidad.Items.Clear;

    rbConfidenTodas.OnClick := nil;
    rbConfidenAlgunas.OnClick := nil;
    rbConfidenTodas.Checked := False;
    rbConfidenAlgunas.Checked := False;
                                                 
    if lValoresConfidencialidad.Count = 0 then
       FillListaConfidencialidad;

    for i:= 0 to lValoresConfidencialidad.Count - 1 do
    begin
        if ( Lista.Count > 0 ) then
        begin
                j := Lista.IndexOf( Trim( lValoresConfidencialidad.Names[i] )  );
                if ( j >= 0 ) then
                   listaConfidencialidad.Items.Add( lValoresConfidencialidad[i] )
        end
    end;

    rbConfidenTodas.Checked :=   listaConfidencialidad.Items.Count = 0;
    rbConfidenAlgunas.Checked := not rbConfidenTodas.Checked;

    if ( gbConfidencialidad.Enabled ) then
    begin
        listaConfidencialidad.Enabled := rbConfidenAlgunas.Checked;
        btSeleccionarConfiden_DevEx.Enabled := rbConfidenAlgunas.Checked;
    end;

    rbConfidenTodas.OnClick := rbConfidenTodasClick;
    rbConfidenAlgunas.OnClick := rbConfidenAlgunasClick;

    FreeAndNil( Lista );

end;

procedure TEditCatClasifi_DevEx.rbConfidenTodasClick(Sender: TObject);
begin
  inherited;

   with DataSource.DataSet do
   begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

        FieldByName('TB_NIVEL0').AsString  :=  '';
   end;
   GetConfidencialidad;
end;

procedure TEditCatClasifi_DevEx.rbConfidenAlgunasClick(Sender: TObject);
begin
  inherited;
     if ( rbConfidenAlgunas.Checked ) then
     begin
        btSeleccionarConfiden_DevExClick( Sender );
        GetConfidencialidad;
     end;

end;

procedure TEditCatClasifi_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
     if ( Field = nil ) then
     begin
          with DataSource.DataSet do
          begin
               if ( State in [dsBrowse] ) then
                  GetConfidencialidad
               else if (State in [dsInsert]) then
                    rbConfidenTodas.Checked := TRUE;
          end;

     end;
end;

procedure TEditCatClasifi_DevEx.dxBarButton_bImpSubCuentasClick(
  Sender: TObject);
begin
  inherited;
     ZImportaSubCuentas.ImportaArchivo( TZetaClientDataSet( DataSource.DataSet ), 'TB_CODIGO', 'TB_SUB_CTA' );

end;

procedure TEditCatClasifi_DevEx.btSeleccionarConfiden_DevExClick(
  Sender: TObject);
var
   selConfi : TSeleccionarConfidencialidad_DevEx;
   sCM_NIVEL0Nuevo : string;
begin
  inherited;

  selConfi := TSeleccionarConfidencialidad_DevEx.Create( Self );
  sCM_NIVEL0Nuevo := selConfi.GetConfidencialidad( DataSource.DataSet.FieldByName('TB_NIVEL0').AsString, lValoresConfidencialidad, True, dmCliente.Confidencialidad );

  if (  sCM_NIVEL0Nuevo <>  DataSource.DataSet.FieldByName('TB_NIVEL0').AsString ) then
  begin
     with DataSource.DataSet do
     begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

           FieldByName('TB_NIVEL0').AsString  := sCM_NIVEL0Nuevo;
           GetConfidencialidad;
     end;
  end;

  FreeAndNil ( selConfi ) ;
end;

end.
