unit FEditCatEstablecimientos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DBCtrls, ZetaNumero, 
  Mask, ZetaEdit, DB, ExtCtrls, Buttons,
  ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  ZetaKeyLookup_DevEx, dxBarExtItems, dxBar, cxClasses, ImgList,
  cxNavigator, cxDBNavigator, cxButtons;

type
  TEditEstablecimientos_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    ES_ACTIVO: TDBCheckBox;
    ES_TEXTO: TDBEdit;
    Label12: TLabel;
    ES_NUMERO: TZetaDBNumero;
    Label11: TLabel;
    ES_CODPOST: TDBEdit;
    Label10: TLabel;
    ES_ENTIDAD: TZetaDBKeyLookup_DevEx;
    Label9: TLabel;
    ES_CIUDAD: TDBEdit;
    Label8: TLabel;
    ES_COLONIA: TDBEdit;
    Label5: TLabel;
    ES_NUMINT: TDBEdit;
    Label7: TLabel;
    ES_NUMEXT: TDBEdit;
    Label6: TLabel;
    ES_CALLE: TDBEdit;
    Label4: TLabel;
    ES_INGLES: TDBEdit;
    Label3: TLabel;
    ES_ELEMENT: TDBEdit;
    Label2: TLabel;
    ES_CODIGO: TZetaDBEdit;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
      procedure Connect; override;
      procedure DoLookup; override;
  end;

var
  EditEstablecimientos_DevEx: TEditEstablecimientos_DevEx;

implementation

uses dCatalogos,
     dTablas,
     ZetaCommonClasses,
     ZetaBuscador_DevEx,
     ZAccesosTress;

{$R *.dfm}

{ TEditEstablecimientos }

procedure TEditEstablecimientos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H_CAT_ESTABLECIMIENTOS_EDICION;
     IndexDerechos := D_CAT_ESTABLECIMIENTOS;
     FirstControl := ES_CODIGO;
end;

procedure TEditEstablecimientos_DevEx.Connect;
begin
     inherited;
     with dmCatalogos do
     begin
          cdsEstablecimientos.Conectar;
          Datasource.Dataset := cdsEstablecimientos;
     end;

     with dmTablas do
     begin
          cdsEstado.Conectar;
          ES_ENTIDAD.LookupDataset := cdsEstado;
     end;
end;

procedure TEditEstablecimientos_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo',Self.Caption,'ES_CODIGO', dmCatalogos.cdsEstablecimientos );
end;



end.
