inherited EditCatMaestros: TEditCatMaestros
  Left = 299
  Top = 233
  Caption = 'Maestros'
  ClientHeight = 400
  ClientWidth = 455
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 364
    Width = 455
    TabOrder = 2
    inherited OK: TBitBtn
      Left = 286
    end
    inherited Cancelar: TBitBtn
      Left = 371
    end
  end
  inherited PanelSuperior: TPanel
    Width = 455
    inherited BuscarBtn: TSpeedButton
      Visible = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 455
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 129
    end
  end
  object PageControl1: TPageControl [3]
    Left = 0
    Top = 105
    Width = 455
    Height = 259
    ActivePage = tbImagen
    Align = alClient
    TabOrder = 4
    object tbGenerales: TTabSheet
      Caption = 'Generales'
      object lblCedula: TLabel
        Left = 21
        Top = 9
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#233'dula:'
        FocusControl = MA_CEDULA
      end
      object lblRFC: TLabel
        Left = 24
        Top = 31
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'R.F.C.:'
        FocusControl = MA_RFC
      end
      object lblNumero: TLabel
        Left = 17
        Top = 53
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'N'#250'mero:'
      end
      object lblTexto: TLabel
        Left = 27
        Top = 75
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Texto:'
      end
      object lblEmpresa: TLabel
        Left = 13
        Top = 97
        Width = 44
        Height = 13
        Alignment = taRightJustify
        Caption = 'Empresa:'
      end
      object lblCalle: TLabel
        Left = 9
        Top = 143
        Width = 48
        Height = 13
        Alignment = taRightJustify
        Caption = 'Direcci'#243'n:'
      end
      object lblCiudad: TLabel
        Left = 21
        Top = 165
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ciudad:'
      end
      object lblTelefono: TLabel
        Left = 12
        Top = 187
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tel'#233'fono:'
      end
      object MA_CEDULA: TDBEdit
        Left = 64
        Top = 5
        Width = 120
        Height = 21
        DataField = 'MA_CEDULA'
        DataSource = DataSource
        TabOrder = 0
      end
      object MA_RFC: TDBEdit
        Left = 64
        Top = 27
        Width = 120
        Height = 21
        DataField = 'MA_RFC'
        DataSource = DataSource
        TabOrder = 1
      end
      object MA_TEXTO: TDBEdit
        Left = 64
        Top = 71
        Width = 280
        Height = 21
        DataField = 'MA_TEXTO'
        DataSource = DataSource
        TabOrder = 3
      end
      object MA_EMPRESA: TDBEdit
        Left = 64
        Top = 117
        Width = 361
        Height = 21
        DataField = 'MA_EMPRESA'
        DataSource = DataSource
        TabOrder = 5
      end
      object MA_DIRECCI: TDBEdit
        Left = 64
        Top = 139
        Width = 280
        Height = 21
        DataField = 'MA_DIRECCI'
        DataSource = DataSource
        TabOrder = 6
      end
      object MA_CIUDAD: TDBEdit
        Left = 64
        Top = 161
        Width = 280
        Height = 21
        DataField = 'MA_CIUDAD'
        DataSource = DataSource
        TabOrder = 7
      end
      object MA_TEL: TDBEdit
        Left = 64
        Top = 183
        Width = 280
        Height = 21
        DataField = 'MA_TEL'
        DataSource = DataSource
        TabOrder = 8
      end
      object MA_NUMERO: TZetaDBNumero
        Left = 64
        Top = 49
        Width = 120
        Height = 21
        Mascara = mnNumeroGlobal
        TabOrder = 2
        Text = '0.00'
        DataField = 'MA_NUMERO'
        DataSource = DataSource
      end
      object MA_ACTIVO: TDBCheckBox
        Left = 23
        Top = 206
        Width = 55
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Activo:'
        DataField = 'MA_ACTIVO'
        DataSource = DataSource
        TabOrder = 9
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object PC_CODIGO: TZetaDBKeyLookup
        Left = 64
        Top = 93
        Width = 364
        Height = 21
        LookupDataset = dmCatalogos.cdsProvCap
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        DataField = 'PC_CODIGO'
        DataSource = DataSource
      end
    end
    object tbImagen: TTabSheet
      Caption = 'Imagen'
      ImageIndex = 1
      object Label1: TLabel
        Left = 17
        Top = 8
        Width = 38
        Height = 13
        Caption = 'Imagen:'
      end
      object LeerDisco: TSpeedButton
        Left = 331
        Top = 191
        Width = 24
        Height = 24
        Hint = 'Leer de Disco'
        Flat = True
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
          333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
          0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
          07333337F3FF3FFF7F333330F00F000F07333337F77377737F333330FFFFFFFF
          07333FF7F3FFFF3F7FFFBBB0F0000F0F0BB37777F7777373777F3BB0FFFFFFFF
          0BBB3777F3FF3FFF77773330F00F000003333337F773777773333330FFFF0FF0
          33333337F3FF7F37F3333330F08F0F0B33333337F7737F77FF333330FFFF003B
          B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
          3BB33773333773333773B333333B3333333B7333333733333337}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = LeerDiscoClick
      end
      object GrabarDisco: TSpeedButton
        Left = 363
        Top = 191
        Width = 24
        Height = 24
        Hint = 'Grabar a Disco'
        Flat = True
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333330070
          7700333333337777777733333333008088003333333377F73377333333330088
          88003333333377FFFF7733333333000000003FFFFFFF77777777000000000000
          000077777777777777770FFFFFFF0FFFFFF07F3333337F3333370FFFFFFF0FFF
          FFF07F3FF3FF7FFFFFF70F00F0080CCC9CC07F773773777777770FFFFFFFF039
          99337F3FFFF3F7F777F30F0000F0F09999937F7777373777777F0FFFFFFFF999
          99997F3FF3FFF77777770F00F000003999337F773777773777F30FFFF0FF0339
          99337F3FF7F3733777F30F08F0F0337999337F7737F73F7777330FFFF0039999
          93337FFFF7737777733300000033333333337777773333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = GrabarDiscoClick
      end
      object BorraImagen: TSpeedButton
        Left = 395
        Top = 191
        Width = 24
        Height = 24
        Hint = 'Borrar Imagen'
        Flat = True
        Glyph.Data = {
          96090000424D9609000000000000360000002800000028000000140000000100
          1800000000006009000000000000000000000000000000000000007F7F007F7F
          007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F
          7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F00
          7F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F
          007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F
          7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F00
          7F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F
          007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F
          7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F00
          7F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F
          007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F
          7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F00
          7F7FFFFFFF007F7F007F7F007F7FFFFFFF007F7F007F7F007F7F007F7F007F7F
          007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F
          7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F00
          7F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7FFFFFFF007F7F
          007F7FFFFFFFFFFFFFFFFFFF007F7F007F7FFFFFFF007F7F007F7F007F7F007F
          7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F00
          7F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F
          007F7F007F7F007F7F007F7F007F7F007F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF007F7F007F7F007F7F007F7F007F7F00
          7F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F
          007F7F007F7F007F7FFFFFFFFFFFFFFFFFFFFFFFFF007F7F007F7F007F7F007F
          7F007F7F007F7F007F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF7F7F7F00007F00007F00007F007F7F007F7F007F7F007F7F007F7F007F7F
          007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F7F7F
          7F7F7F7F7F7F7F7F7F7FFFFFFFFFFFFF007F7F007F7F007F7F007F7F007F7F00
          7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F0000FFFF00FF
          FF00FF7F007F00007F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F
          7F007F7F007F7F007F7F007F7F007F7F007F7F7F7F7FFFFFFF007F7F007F7F7F
          7F7F7F7F7FFFFFFFFFFFFF007F7F007F7F007F7F007F7F007F7FFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF00FF0000FFFF00FFFF00FF7F00
          7F00007F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F00
          7F7F007F7F007F7F007F7F7F7F7FFFFFFF007F7F007F7F007F7F7F7F7F7F7F7F
          FFFFFFFFFFFF007F7F007F7F007F7F007F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFF007F7FFF00FF0000FFFFFFFF0000FFFF00FFFF00FF7F007F00000000
          7F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F
          007F7F7F7F7FFFFFFF007F7F007F7F007F7F007F7F7F7F7F7F7F7FFFFFFFFFFF
          FF007F7F007F7F007F7FFFFFFFFFFFFF007F7FFFFFFFFFFFFFFFFFFF007F7F00
          00FFFF00FFFFFFFFFF00FF0000FFFF00FF0000007F7F00000000007F7F007F7F
          007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F7F7F7F007F
          7FFFFFFF007F7F007F7F007F7F7F7F7F7F7F7F7F7F7FFFFFFFFFFFFF007F7F00
          7F7FFFFFFF007F7F007F7F007F7FFFFFFF007F7F007F7F007F7F0000FFFF00FF
          FFFFFFFF00FF000000007F007F00007F7F00000000007F7F007F7F007F7F007F
          7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F7F7F7F007F7FFFFFFF00
          7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7FFFFFFFFFFFFF007F7FFFFFFF007F7F
          007F7F007F7F007F7F007F7F007F7F007F7F007F7F0000FFFF00FF00000000FF
          00007F00007F007F00007F7F00000000007F7F007F7F007F7F007F7F007F7F00
          7F7F007F7F007F7F007F7F007F7F007F7F7F7F7F007F7F7F7F7F007F7F7F7F7F
          7F7F7F7F7F7F7F7F7F7F7F7FFFFFFFFFFFFF007F7F007F7F007F7F007F7F007F
          7F007F7F007F7F007F7F007F7F007F7F00000000FF0000FFFF00FF00007F0000
          7F000000007F7F7F000000007F7F007F7F007F7F007F7F007F7F007F7F007F7F
          007F7F007F7F007F7F007F7F7F7F7F007F7FFFFFFF007F7F7F7F7F7F7F7F7F7F
          7F7F7F7F7F7F7FFFFFFF007F7F007F7F007F7F007F7F007F7F007F7F007F7F00
          7F7F007F7F007F7F007F7F00000000FF0000FFFF00FF000000007F00007F7F7F
          7F7F7F000000007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F
          7F007F7F007F7F7F7F7F007F7FFFFFFF007F7F7F7F7F7F7F7F7F7F7F7F7F7F7F
          7F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F
          007F7F007F7F00000000FF00000000BFBFBF00FFFF7F00007F7F7F7F7F7F007F
          7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F00
          7F7F7F7F7F007F7F7F7F7F007F7F007F7F7F7F7F7F7F7F7F7F7F007F7F007F7F
          007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F
          7F000000BFBFBFFFFFFF00FFFF00FFFF7F00007F7F7F007F7F007F7F007F7F00
          7F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F7F7F7F
          007F7FFFFFFF007F7F007F7F7F7F7F7F7F7F007F7F007F7F007F7F007F7F007F
          7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F000000FF
          FFFFFFFFFF00FFFF00FFFF7F0000007F7F007F7F007F7F007F7F007F7F007F7F
          007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F7F7F7F007F7FFFFF
          FF007F7F007F7F7F7F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F00
          7F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F000000FFFFFFFFFFFF
          00FFFF00FFFF007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F
          7F007F7F007F7F007F7F007F7F007F7F007F7F7F7F7F007F7FFFFFFF007F7F00
          7F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F
          007F7F007F7F007F7F007F7F007F7F007F7F000000FFFFFFFFFFFF00FFFF007F
          7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F007F7F00
          7F7F007F7F007F7F007F7F007F7F7F7F7F007F7F007F7F007F7F}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = BorraImagenClick
      end
      object Imagen: TPDBMultiImage
        Left = 64
        Top = 8
        Width = 355
        Height = 181
        ImageReadRes = lAutoMatic
        ImageWriteRes = sAutoMatic
        DataEngine = deBDE
        JPegSaveQuality = 75
        JPegSaveSmooth = 0
        ImageDither = True
        UpdateAsJPG = False
        UpdateAsBMP = False
        UpdateAsGIF = False
        UpdateAsPCX = False
        UpdateAsPNG = False
        UpdateAsTIF = True
        Color = clBtnFace
        DataField = 'MA_IMAGEN'
        DataSource = DataSource
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnFace
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        RichTools = False
        ReadOnly = True
        StretchRatio = True
        TabOrder = 0
        TextLeft = 0
        TextTop = 0
        TextRotate = 0
        TifSaveCompress = sNONE
        UseTwainWindow = False
      end
    end
    object tbSTPS: TTabSheet
      Caption = 'STPS'
      ImageIndex = 2
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 447
        Height = 84
        Align = alTop
        Caption = ' Agente Capacitador '
        TabOrder = 0
        object Label2: TLabel
          Left = 37
          Top = 28
          Width = 24
          Height = 13
          Caption = 'Tipo:'
        end
        object Label3: TLabel
          Left = 19
          Top = 52
          Width = 42
          Height = 13
          Caption = 'Registro:'
        end
        object MA_TAGENTE: TZetaDBKeyCombo
          Left = 65
          Top = 24
          Width = 145
          Height = 21
          AutoComplete = False
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 0
          ListaFija = lfTipoAgenteSTPS
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
          DataField = 'MA_TAGENTE'
          DataSource = DataSource
          LlaveNumerica = True
        end
        object MA_NAGENTE: TZetaDBEdit
          Left = 65
          Top = 48
          Width = 294
          Height = 21
          TabOrder = 1
          DataField = 'MA_NAGENTE'
          DataSource = DataSource
        end
      end
    end
  end
  object Panel1: TPanel [4]
    Left = 0
    Top = 51
    Width = 455
    Height = 54
    Align = alTop
    TabOrder = 3
    object lblCodigo: TLabel
      Left = 25
      Top = 8
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'digo:'
      FocusControl = MA_CODIGO
    end
    object lblNombre: TLabel
      Left = 21
      Top = 30
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nombre:'
      FocusControl = MA_NOMBRE
    end
    object MA_CODIGO: TZetaDBEdit
      Left = 68
      Top = 4
      Width = 80
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
      ConfirmEdit = True
      DataField = 'MA_CODIGO'
      DataSource = DataSource
    end
    object MA_NOMBRE: TDBEdit
      Left = 68
      Top = 26
      Width = 353
      Height = 21
      DataField = 'MA_NOMBRE'
      DataSource = DataSource
      TabOrder = 1
    end
  end
  inherited DataSource: TDataSource
    Left = 396
    Top = 1
  end
  object SavePictureDialog: TSavePictureDialog
    Filter = 
      'JPEG Image File (*.jpg)|*.jpg|JPEG Image File (*.jpeg)|*.jpeg|Bi' +
      'tmaps (*.bmp)|*.bmp|GIF Image File (*.gif)|*.gif|TIF Image File ' +
      '(*.tif)|*.tif|PCX Image File (*.pcx)|*.pcx|PNG Image File (*.png' +
      ')|*.png'
    FilterIndex = 0
    Left = 48
    Top = 336
  end
  object OpenPictureDialog: TOpenPictureDialog
    DefaultExt = 'JPG'
    Filter = 
      'JPEG Image File (*.jpg)|*.jpg|JPEG Image File (*.jpeg)|*.jpeg|Bi' +
      'tmaps (*.bmp)|*.bmp|GIF Image File (*.gif)|*.gif|TIF Image File ' +
      '(*.tif)|*.tif|PCX Image File (*.pcx)|*.pcx|PNG Image File (*.png' +
      ')|*.png'
    FilterIndex = 0
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist]
    Title = 'Leer de Disco'
    Left = 8
    Top = 335
  end
end
