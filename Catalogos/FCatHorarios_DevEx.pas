unit FCatHorarios_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, StdCtrls, Mask, DBCtrls, Grids, DBGrids, ExtCtrls,
     ZBaseGridLectura_DevEx,
     ZetaDBGrid, ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid;

type
  TCatHorarios_DevEx = class(TBaseGridLectura_DevEx)
    HO_CODIGO: TcxGridDBColumn;
    HO_DESCRIP: TcxGridDBColumn;
    HO_INTIME: TcxGridDBColumn;
    HO_OUTTIME: TcxGridDBColumn;
    HO_JORNADA: TcxGridDBColumn;
    HO_TIPO: TcxGridDBColumn;
    HO_ACTIVO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ConfigAgrupamiento;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar;override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations}
    procedure DoLookup; override;
  end;

var
  CatHorarios_DevEx: TCatHorarios_DevEx;

implementation

uses DCatalogos,
     ZetaBuscador_DevEx,
     ZetaCommonClasses;

{$R *.DFM}

{ TCatHorarios }

procedure TCatHorarios_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     //Desactivar gridmode (FormCreate)
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= FALSE;
     CanLookup := True;
     HelpContext := H60615_Horarios;
end;

procedure TCatHorarios_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsHorarios.Conectar;
          DataSource.DataSet := cdsHorarios;
     end;
end;

procedure TCatHorarios_DevEx.Refresh;
begin
     dmCatalogos.cdsHorarios.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatHorarios_DevEx.Agregar;
begin
     dmCatalogos.cdsHorarios.Agregar;
end;

procedure TCatHorarios_DevEx.Borrar;
begin
     dmCatalogos.cdsHorarios.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatHorarios_DevEx.Modificar;
begin
     dmCatalogos.cdsHorarios.Modificar;
end;

procedure TCatHorarios_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Horario', 'Horario', 'HO_CODIGO', dmCatalogos.cdsHorarios );
end;

procedure TCatHorarios_DevEx.ConfigAgrupamiento;
begin
     //Cambio de configuracion
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //Desactiva la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Esconde la caja de agrupamiento
     //Determinar las columnas por las que se podra agrupar
     HO_CODIGO.Options.Grouping:= FALSE;
     HO_DESCRIP.Options.Grouping:= FALSE;
     HO_JORNADA.Options.Grouping:= FALSE;
     HO_ACTIVO.Options.Grouping:= FALSE;
end;

procedure TCatHorarios_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     {***Banda Sumatoria***}
  CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
  inherited;
  //DevEx (by am):Configuracion Especial para agrupamiento
  ConfigAgrupamiento;

end;

end.

