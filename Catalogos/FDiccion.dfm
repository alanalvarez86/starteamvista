inherited Diccion: TDiccion
  Left = 115
  Top = 233
  Caption = 'Diccionario de Datos'
  ClientWidth = 612
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 612
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 286
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 49
    Width = 612
    Height = 224
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'DI_NOMBRE'
        Title.Caption = 'Nombre'
        Width = 83
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DI_TITULO'
        Title.Caption = 'T�tulo'
        Width = 214
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DI_ANCHO'
        Title.Caption = 'Ancho'
        Width = 37
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DI_MASCARA'
        Title.Caption = 'M�scara'
        Width = 121
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DI_CONFI'
        Title.Caption = 'Confidencial?'
        Width = 72
        Visible = True
      end>
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 19
    Width = 612
    Height = 30
    Align = alTop
    TabOrder = 2
    object Label1: TLabel
      Left = 16
      Top = 8
      Width = 73
      Height = 13
      Caption = 'Tabla Principal:'
    end
    object CBEntidades: TComboBox
      Left = 95
      Top = 4
      Width = 258
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnChange = CBEntidadesChange
    end
  end
  inherited DataSource: TDataSource
    Left = 16
    Top = 128
  end
end
