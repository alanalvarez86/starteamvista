unit FPtoTools_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx, StdCtrls, Buttons, ExtCtrls, CheckLst, 
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters, ImgList,
  cxButtons, cxControls, cxContainer, cxEdit, cxCheckListBox;

type
  TFHerramientas_DevEx = class(TZetaDlgModal_DevEx)
    Prender_DevEx: TcxButton;
    Apagar_DevEx: TcxButton;
    Niveles: TcxCheckListBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    //procedure PrenderClick(Sender: TObject);
    //procedure ApagarClick(Sender: TObject);
    //procedure OKClick(Sender: TObject);
    //procedure NivelesClickCheck(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure Prender_DevExClick(Sender: TObject);
    procedure Apagar_DevExClick(Sender: TObject);
    procedure NivelesClickCheck(Sender: TObject; AIndex: Integer;
      APrevState, ANewState: TcxCheckBoxState);
  private
    { Private declarations }
    FLista: TStrings;
    procedure Cargar;
    procedure Descargar;
    procedure PrendeApaga( const lState: Boolean );
    procedure ActivaOK;
  public
    { Public declarations }
  end;

var
  FHerramientas_DevEx: TFHerramientas_DevEx;

implementation

uses DCatalogos;

{$R *.DFM}

procedure TFHerramientas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FLista := TStringList.Create;
end;

procedure TFHerramientas_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     Cargar;
     OK_DevEx.Enabled := False;
     ActiveControl := Niveles;
     with dmCatalogos.cdsPuestos do
     begin
          self.Caption := Format( 'Asignar Herramientas al Puesto: %s = %s', [ FieldByName( 'PU_CODIGO' ).AsString,
                                  FieldByName( 'PU_DESCRIP' ).AsString ] );
     end;
end;

procedure TFHerramientas_DevEx.FormDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( FLista );
end;

//DevEx
procedure TFHerramientas_DevEx.Cargar;
var
   i: Integer;
begin
     dmCatalogos.CargaListaHerramientas( FLista );
     with Niveles do
     begin
          Items.BeginUpdate;
          try
             Clear;
             with FLista do
             begin
                  for i := 0 to ( Count - 1 ) do
                  begin
                       Items.Add.Text := FLista[i];
                       Items[i].Checked:= ( Integer( Objects[ i ] ) > 0 );
                  end;
             end;
          finally
                 Items.EndUpdate;
          end;
     end;

end;

procedure TFHerramientas_DevEx.Descargar;
var
   i: Integer;
begin
     with Niveles do
     begin
          with Items do
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    if Items[i].Checked then
                       FLista.Objects[ i ] := TObject( 1 )
                    else
                        FLista.Objects[ i ] := TObject( 0 );
               end;
          end;
     end;
     dmCatalogos.DescargaHerramientas( FLista );
end;

procedure TFHerramientas_DevEx.PrendeApaga( const lState: Boolean );
var
   i: Integer;
begin
     with Niveles do
     begin
          with Items do
          begin
               try
                  BeginUpdate;
                  for i := 0 to ( Count - 1 ) do
                      Items[ i ].Checked := lState;
               finally
                      EndUpdate;
               end;
          end;
     end;
     ActivaOK;
end;

{procedure TFHerramientas_DevEx.NivelesClickCheck(Sender: TObject);
begin
     inherited;
     with OK_DevEx do
     begin
          if not Enabled then
             Enabled := True;
     end;
end;}

procedure TFHerramientas_DevEx.OK_DevExClick(Sender: TObject);
begin
   inherited;
   Descargar;

end;

procedure TFHerramientas_DevEx.Prender_DevExClick(Sender: TObject);
begin
   inherited;
     PrendeApaga( True );

end;

procedure TFHerramientas_DevEx.Apagar_DevExClick(Sender: TObject);
begin
  inherited;
     PrendeApaga( False );

end;

procedure TFHerramientas_DevEx.ActivaOK;
begin
     with OK_DevEx do
     begin
          if not Enabled then
             Enabled := True;
     end;
end;

procedure TFHerramientas_DevEx.NivelesClickCheck(Sender: TObject;
  AIndex: Integer; APrevState, ANewState: TcxCheckBoxState);
begin
  inherited;
       ActivaOK;
end;

end.
