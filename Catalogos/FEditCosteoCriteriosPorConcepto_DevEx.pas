unit FEditCosteoCriteriosPorConcepto_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DB, ExtCtrls, DBCtrls, ZBaseEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, ZetaKeyLookup_DevEx, ZetaDBTextBox,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons;

type
  TEditCosteoCriteriosPorConcepto_DevEx = class(TBaseEdicion_DevEx)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    CO_NUMERO: TZetaDBKeyLookup_DevEx;
    GrupoId: TZetaDBTextBox;
    GrupoDescripcion: TZetaDBTextBox;
    CriterioId: TZetaDBTextBox;
    CriterioDescripcion: TZetaDBTextBox;

    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect; override;

  public
    { Public declarations }
  end;

var
  EditCosteoCriteriosPorConcepto_DevEx: TEditCosteoCriteriosPorConcepto_DevEx;

implementation

uses ZAccesosTress,
     ZGlobalTress,
     ZetaCommonClasses,
     DGlobal,
     DCliente,
     DTablas,
     DAsistencia,
     DBaseCliente,
     dCatalogos;

{$R *.dfm}

{ TEditCosteoCriteriosPorConcepto }

procedure TEditCosteoCriteriosPorConcepto_DevEx.FormCreate(Sender: TObject);
begin
     IndexDerechos := D_COSTEO_CRITERIOS_CONCEPTO;
     FirstControl := CO_NUMERO;
     HelpContext := H00003_Edit_Costeo_CriteriosPorConcepto;
end;

procedure TEditCosteoCriteriosPorConcepto_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsConceptosDisponibles.Refrescar;
          CO_NUMERO.LookupDataset := cdsConceptosDisponibles;
          DataSource.DataSet := cdsCosteoCriteriosPorConcepto;
     end;
     inherited;
end;

end.
