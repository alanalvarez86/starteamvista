inherited MatrizCertificaPuesto_DevEx: TMatrizCertificaPuesto_DevEx
  Left = 286
  Top = 204
  Caption = 'Certificaciones por Puesto'
  ClientWidth = 655
  ExplicitWidth = 655
  ExplicitHeight = 240
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 655
    TabOrder = 2
    ExplicitWidth = 655
    inherited Slider: TSplitter
      Left = 378
      ExplicitLeft = 378
    end
    inherited ValorActivo1: TPanel
      Width = 362
      ExplicitWidth = 362
    end
    inherited ValorActivo2: TPanel
      Left = 381
      Width = 274
      ExplicitLeft = 381
      ExplicitWidth = 274
      inherited textoValorActivo2: TLabel
        Left = 188
        ExplicitLeft = 188
      end
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 655
    Height = 46
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 37
      Top = 16
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'Puesto:'
    end
    object LookUpPuesto: TZetaKeyLookup_DevEx
      Left = 80
      Top = 12
      Width = 300
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      OnValidKey = LookUpPuestoValidKey
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 65
    Width = 655
    Height = 159
    ExplicitTop = 65
    ExplicitWidth = 655
    ExplicitHeight = 159
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CI_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CI_CODIGO'
      end
      object CI_NOMBRE: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'CI_NOMBRE'
      end
      object PC_DIAS: TcxGridDBColumn
        Caption = 'D'#237'as de Antig'#252'edad'
        DataBinding.FieldName = 'PC_DIAS'
      end
      object PC_OPCIONA: TcxGridDBColumn
        Caption = 'Opcional'
        DataBinding.FieldName = 'PC_OPCIONA'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
