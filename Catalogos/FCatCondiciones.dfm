inherited CatCondiciones: TCatCondiciones
  Left = 320
  Top = 177
  Caption = 'Condiciones'
  ClientHeight = 271
  ClientWidth = 538
  PixelsPerInch = 96
  TextHeight = 13
  object ZetaDBGrid1: TZetaDBGrid [0]
    Left = 0
    Top = 19
    Width = 538
    Height = 252
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'QU_CODIGO'
        Title.Caption = 'C'#243'digo'
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'QU_DESCRIP'
        Title.Caption = 'Descripci'#243'n'
        Width = 292
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'QU_NAVEGA'
        Title.Caption = #191'Navegaci'#243'n?'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'QU_ORDEN'
        Title.Caption = 'Orden'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_CODIGO'
        Title.Alignment = taRightJustify
        Title.Caption = 'Modific'#243
        Width = 49
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'QU_NIVEL'
        Title.Alignment = taRightJustify
        Title.Caption = 'Nivel'
        Width = 30
        Visible = True
      end>
  end
  inherited PanelIdentifica: TPanel
    Width = 538
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 212
    end
  end
end
