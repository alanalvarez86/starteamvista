inherited CatPerfiles_DevEx: TCatPerfiles_DevEx
  Left = 527
  Top = 324
  Caption = 'Grupos de Competencias'
  ClientWidth = 608
  ExplicitWidth = 608
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 608
    ExplicitWidth = 609
    inherited Slider: TSplitter
      Left = 323
      ExplicitLeft = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      ExplicitWidth = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 282
      ExplicitLeft = 326
      ExplicitWidth = 283
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 276
        ExplicitLeft = 473
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 60
    Width = 608
    Height = 164
    ExplicitTop = 60
    ExplicitWidth = 609
    ExplicitHeight = 164
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CP_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CP_CODIGO'
        MinWidth = 80
        Options.Grouping = False
        Width = 80
      end
      object CP_ELEMENT: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'CP_ELEMENT'
        MinWidth = 80
        Options.Grouping = False
        Width = 223
      end
      object TB_ELEMENT: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'TB_ELEMENT'
        MinWidth = 80
        Width = 178
      end
      object CP_ACTIVO: TcxGridDBColumn
        Caption = 'Activo'
        DataBinding.FieldName = 'CP_ACTIVO'
        MinWidth = 80
        Options.Grouping = False
      end
    end
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 19
    Width = 608
    Height = 41
    Align = alTop
    TabOrder = 2
    ExplicitWidth = 609
    object Label1: TLabel
      Left = 10
      Top = 12
      Width = 24
      Height = 13
      Caption = 'Tipo:'
    end
    object lookTPerfiles: TZetaKeyLookup_DevEx
      Left = 36
      Top = 8
      Width = 300
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      OnValidKey = lookTPerfilesValidKey
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
