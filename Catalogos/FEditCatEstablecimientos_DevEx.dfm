inherited EditEstablecimientos_DevEx: TEditEstablecimientos_DevEx
  Left = 265
  Top = 200
  Caption = 'Establecimiento'
  ClientHeight = 400
  ClientWidth = 487
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel [0]
    Left = 0
    Top = 50
    Width = 487
    Height = 335
    Align = alTop
    TabOrder = 7
    object Label12: TLabel
      Left = 58
      Top = 248
      Width = 30
      Height = 13
      Alignment = taRightJustify
      Caption = 'Texto:'
    end
    object Label11: TLabel
      Left = 48
      Top = 224
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'mero:'
    end
    object Label10: TLabel
      Left = 20
      Top = 200
      Width = 68
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'digo Postal:'
    end
    object Label9: TLabel
      Left = 52
      Top = 176
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'Estado:'
    end
    object Label8: TLabel
      Left = 52
      Top = 152
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'Ciudad:'
    end
    object Label5: TLabel
      Left = 50
      Top = 128
      Width = 38
      Height = 13
      Alignment = taRightJustify
      Caption = 'Colonia:'
    end
    object Label7: TLabel
      Left = 179
      Top = 103
      Width = 45
      Height = 13
      Alignment = taRightJustify
      Caption = '# Interior:'
    end
    object Label6: TLabel
      Left = 40
      Top = 104
      Width = 48
      Height = 13
      Alignment = taRightJustify
      Caption = '# Exterior:'
    end
    object Label4: TLabel
      Left = 62
      Top = 80
      Width = 26
      Height = 13
      Alignment = taRightJustify
      Caption = 'Calle:'
    end
    object Label3: TLabel
      Left = 57
      Top = 56
      Width = 31
      Height = 13
      Alignment = taRightJustify
      Caption = 'Ingl'#233's:'
    end
    object Label2: TLabel
      Left = 48
      Top = 32
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nombre:'
    end
    object Label1: TLabel
      Left = 52
      Top = 8
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'digo:'
    end
    object ES_ACTIVO: TDBCheckBox
      Left = 53
      Top = 266
      Width = 52
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Activo:'
      DataField = 'ES_ACTIVO'
      DataSource = DataSource
      TabOrder = 12
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
    object ES_TEXTO: TDBEdit
      Left = 93
      Top = 244
      Width = 358
      Height = 21
      DataField = 'ES_TEXTO'
      DataSource = DataSource
      TabOrder = 11
    end
    object ES_NUMERO: TZetaDBNumero
      Left = 93
      Top = 220
      Width = 80
      Height = 21
      Mascara = mnHoras
      TabOrder = 10
      Text = '0.00'
      DataField = 'ES_NUMERO'
      DataSource = DataSource
    end
    object ES_CODPOST: TDBEdit
      Left = 93
      Top = 196
      Width = 80
      Height = 21
      DataField = 'ES_CODPOST'
      DataSource = DataSource
      TabOrder = 9
    end
    object ES_ENTIDAD: TZetaDBKeyLookup_DevEx
      Left = 93
      Top = 172
      Width = 358
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 8
      TabStop = True
      WidthLlave = 60
      DataField = 'ES_ENTIDAD'
      DataSource = DataSource
    end
    object ES_CIUDAD: TDBEdit
      Left = 93
      Top = 148
      Width = 358
      Height = 21
      DataField = 'ES_CIUDAD'
      DataSource = DataSource
      TabOrder = 7
    end
    object ES_COLONIA: TDBEdit
      Left = 93
      Top = 124
      Width = 358
      Height = 21
      DataField = 'ES_COLONIA'
      DataSource = DataSource
      TabOrder = 6
    end
    object ES_NUMINT: TDBEdit
      Left = 228
      Top = 101
      Width = 80
      Height = 21
      DataField = 'ES_NUMINT'
      DataSource = DataSource
      TabOrder = 5
    end
    object ES_NUMEXT: TDBEdit
      Left = 93
      Top = 101
      Width = 80
      Height = 21
      DataField = 'ES_NUMEXT'
      DataSource = DataSource
      TabOrder = 4
    end
    object ES_CALLE: TDBEdit
      Left = 93
      Top = 76
      Width = 358
      Height = 21
      DataField = 'ES_CALLE'
      DataSource = DataSource
      TabOrder = 3
    end
    object ES_INGLES: TDBEdit
      Left = 93
      Top = 52
      Width = 358
      Height = 21
      DataField = 'ES_INGLES'
      DataSource = DataSource
      TabOrder = 2
    end
    object ES_ELEMENT: TDBEdit
      Left = 93
      Top = 28
      Width = 358
      Height = 21
      DataField = 'ES_ELEMENT'
      DataSource = DataSource
      TabOrder = 1
    end
    object ES_CODIGO: TZetaDBEdit
      Left = 95
      Top = 4
      Width = 80
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
      ConfirmEdit = True
      DataField = 'ES_CODIGO'
      DataSource = DataSource
    end
  end
  inherited PanelBotones: TPanel
    Top = 364
    Width = 487
    inherited OK_DevEx: TcxButton
      Left = 322
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 401
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 487
    TabOrder = 2
    inherited ValorActivo2: TPanel
      Width = 161
      inherited textoValorActivo2: TLabel
        Width = 155
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 404
    Top = 329
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
