inherited CatCompetencias_DevEx: TCatCompetencias_DevEx
  Left = 513
  Top = 287
  Caption = 'Competencias'
  ClientHeight = 227
  ClientWidth = 642
  ExplicitWidth = 642
  ExplicitHeight = 227
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 642
    ExplicitWidth = 642
    inherited Slider: TSplitter
      Left = 323
      ExplicitLeft = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      ExplicitWidth = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 316
      ExplicitLeft = 326
      ExplicitWidth = 316
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 310
        ExplicitLeft = 3
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 60
    Width = 642
    Height = 167
    ExplicitTop = 60
    ExplicitWidth = 642
    ExplicitHeight = 167
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CC_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CC_CODIGO'
        MinWidth = 60
        Options.Grouping = False
        Width = 60
      end
      object CC_ELEMENT: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'CC_ELEMENT'
        MinWidth = 220
        Options.Grouping = False
        Width = 223
      end
      object TB_ELEMENT: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'TB_ELEMENT'
        MinWidth = 220
        Width = 226
      end
      object CC_ACTIVO: TcxGridDBColumn
        Caption = 'Activo'
        DataBinding.FieldName = 'CC_ACTIVO'
        MinWidth = 65
        Options.Grouping = False
      end
    end
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 19
    Width = 642
    Height = 41
    Align = alTop
    TabOrder = 2
    object Label1: TLabel
      Left = 10
      Top = 12
      Width = 24
      Height = 13
      Caption = 'Tipo:'
    end
    object lookTCompetencias: TZetaKeyLookup_DevEx
      Left = 42
      Top = 8
      Width = 300
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      OnValidKey = lookTCompetenciasValidKey
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
