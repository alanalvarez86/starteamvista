inherited EditCatProvCap_DevEx: TEditCatProvCap_DevEx
  Left = 386
  Top = 373
  Caption = 'Proveedores de Capacitaci'#243'n'
  ClientHeight = 467
  ClientWidth = 482
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 431
    Width = 482
    TabOrder = 2
    inherited OK_DevEx: TcxButton
      Left = 318
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 397
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 482
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 156
      inherited textoValorActivo2: TLabel
        Width = 150
      end
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 50
    Width = 482
    Height = 381
    Align = alClient
    TabOrder = 7
    object lbCodigo: TLabel
      Left = 64
      Top = 12
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object lbNombre: TLabel
      Left = 60
      Top = 36
      Width = 40
      Height = 13
      Caption = 'Nombre:'
    end
    object Label3: TLabel
      Left = 52
      Top = 60
      Width = 48
      Height = 13
      Caption = 'Direcci'#243'n:'
    end
    object lbCiudad: TLabel
      Left = 64
      Top = 84
      Width = 36
      Height = 13
      Caption = 'Ciudad:'
    end
    object Label1: TLabel
      Left = 64
      Top = 108
      Width = 36
      Height = 13
      Caption = 'Estado:'
    end
    object Label2: TLabel
      Left = 75
      Top = 132
      Width = 25
      Height = 13
      Caption = 'Pa'#237's:'
    end
    object Label4: TLabel
      Left = 32
      Top = 156
      Width = 68
      Height = 13
      Caption = 'C'#243'digo Postal:'
    end
    object Label5: TLabel
      Left = 67
      Top = 180
      Width = 33
      Height = 13
      Caption = 'R.F.C.:'
    end
    object Label6: TLabel
      Left = 54
      Top = 204
      Width = 46
      Height = 13
      Caption = 'Contacto:'
    end
    object Label7: TLabel
      Left = 55
      Top = 228
      Width = 45
      Height = 13
      Caption = 'Tel'#233'fono:'
    end
    object Label8: TLabel
      Left = 77
      Top = 252
      Width = 23
      Height = 13
      Caption = 'FAX:'
    end
    object Label9: TLabel
      Left = 10
      Top = 276
      Width = 90
      Height = 13
      Caption = 'Correo Electr'#243'nico:'
    end
    object Label10: TLabel
      Left = 25
      Top = 300
      Width = 75
      Height = 13
      Caption = 'P'#225'gina Internet:'
    end
    object Label11: TLabel
      Left = 60
      Top = 324
      Width = 40
      Height = 13
      Caption = 'N'#250'mero:'
    end
    object Label12: TLabel
      Left = 70
      Top = 348
      Width = 30
      Height = 13
      Caption = 'Texto:'
    end
    object PC_CODIGO: TZetaDBEdit
      Left = 104
      Top = 8
      Width = 65
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
      Text = 'PC_CODIGO'
      ConfirmEdit = True
      DataField = 'PC_CODIGO'
      DataSource = DataSource
    end
    object PC_NOMBRE: TDBEdit
      Left = 104
      Top = 32
      Width = 364
      Height = 21
      DataField = 'PC_NOMBRE'
      DataSource = DataSource
      TabOrder = 1
    end
    object PC_DIRECCI: TDBEdit
      Left = 104
      Top = 56
      Width = 364
      Height = 21
      DataField = 'PC_DIRECCI'
      DataSource = DataSource
      TabOrder = 2
    end
    object PC_CIUDAD: TDBEdit
      Left = 104
      Top = 80
      Width = 257
      Height = 21
      DataField = 'PC_CIUDAD'
      DataSource = DataSource
      TabOrder = 3
    end
    object PC_ESTADO: TZetaDBKeyLookup_DevEx
      Left = 104
      Top = 104
      Width = 364
      Height = 21
      LookupDataset = dmTablas.cdsEstado
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 4
      TabStop = True
      WidthLlave = 60
      DataField = 'PC_ESTADO'
      DataSource = DataSource
    end
    object PC_PAIS: TDBEdit
      Left = 104
      Top = 128
      Width = 257
      Height = 21
      DataField = 'PC_PAIS'
      DataSource = DataSource
      TabOrder = 5
    end
    object PC_CODPOST: TDBEdit
      Left = 104
      Top = 152
      Width = 65
      Height = 21
      DataField = 'PC_CODPOST'
      DataSource = DataSource
      TabOrder = 6
    end
    object PC_RFC: TDBEdit
      Left = 104
      Top = 176
      Width = 257
      Height = 21
      DataField = 'PC_RFC'
      DataSource = DataSource
      TabOrder = 7
    end
    object PC_CONTACT: TDBEdit
      Left = 104
      Top = 200
      Width = 257
      Height = 21
      DataField = 'PC_CONTACT'
      DataSource = DataSource
      TabOrder = 8
    end
    object PC_TEL: TDBEdit
      Left = 104
      Top = 224
      Width = 257
      Height = 21
      DataField = 'PC_TEL'
      DataSource = DataSource
      TabOrder = 9
    end
    object PC_FAX: TDBEdit
      Left = 104
      Top = 248
      Width = 257
      Height = 21
      DataField = 'PC_FAX'
      DataSource = DataSource
      TabOrder = 10
    end
    object PC_EMAIL: TDBEdit
      Left = 104
      Top = 272
      Width = 364
      Height = 21
      DataField = 'PC_EMAIL'
      DataSource = DataSource
      TabOrder = 11
    end
    object PC_WEB: TDBEdit
      Left = 104
      Top = 296
      Width = 364
      Height = 21
      DataField = 'PC_WEB'
      DataSource = DataSource
      TabOrder = 12
    end
    object PC_NUMERO: TZetaDBNumero
      Left = 104
      Top = 320
      Width = 121
      Height = 21
      Mascara = mnHoras
      TabOrder = 13
      Text = '0.00'
      DataField = 'PC_NUMERO'
      DataSource = DataSource
    end
    object PC_TEXTO: TDBEdit
      Left = 104
      Top = 344
      Width = 257
      Height = 21
      DataField = 'PC_TEXTO'
      DataSource = DataSource
      TabOrder = 14
    end
    object PC_ACTIVO: TDBCheckBox
      Left = 65
      Top = 366
      Width = 52
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Activo:'
      DataField = 'PC_ACTIVO'
      DataSource = DataSource
      TabOrder = 15
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
  end
  inherited DataSource: TDataSource
    Left = 396
    Top = 1
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 9961472
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    Left = 16
    Top = 48
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_BuscarBtn: TdxBarButton
      Visible = ivAlways
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    DesignInfo = 5242896
  end
end
