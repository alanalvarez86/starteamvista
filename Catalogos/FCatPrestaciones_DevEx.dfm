inherited CatPrestaciones_DevEx: TCatPrestaciones_DevEx
  Left = 256
  Top = 156
  Caption = 'Prestaciones'
  ClientWidth = 498
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 498
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 172
      inherited textoValorActivo2: TLabel
        Width = 166
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 498
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object TB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'TB_CODIGO'
      end
      object TB_ELEMENT: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'TB_ELEMENT'
      end
      object TB_PRIMAVA: TcxGridDBColumn
        Caption = 'Prima Vacacional'
        DataBinding.FieldName = 'TB_PRIMAVA'
      end
      object TB_DIAS_AG: TcxGridDBColumn
        Caption = 'D'#237'as Aguinaldo'
        DataBinding.FieldName = 'TB_DIAS_AG'
      end
      object TB_ACTIVO: TcxGridDBColumn
        Caption = 'Activo'
        DataBinding.FieldName = 'TB_ACTIVO'
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 432
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
