unit FEditSeccionPerfil_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, ZetaNumero, ZetaEdit, StdCtrls, Mask, DBCtrls, DB,
  ExtCtrls, ZetaDBTextBox, ZBaseEdicion_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TEditSeccionPerfil_DevEx = class(TBaseEdicion_DevEx)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DT_TEXTO: TDBEdit;
    DT_NUMERO: TZetaDBNumero;
    DT_NOMBRE: TDBEdit;
    DT_INGLES: TDBEdit;
    Label4: TLabel;
    DT_ORDEN: TZetaDBTextBox;
    DT_CODIGO: TZetaDBEdit;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  EditSeccionPerfil_DevEx: TEditSeccionPerfil_DevEx;

implementation
uses
    dCatalogos,
    ZetaCommonClasses,
    ZetaBuscador_DevEx,
    ZAccesosTress;

{$R *.dfm}

procedure TEditSeccionPerfil_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_DESC_SECC_PERF;
     HelpContext   := H_CAT_SECC_PERF;
     FirstControl  := DT_CODIGO;
     
end;

procedure TEditSeccionPerfil_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsSeccionesPerfil.Conectar;
          DataSource.DataSet := cdsSeccionesPerfil;
     end;
end;

procedure TEditSeccionPerfil_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo',Self.Caption,'DT_CODIGO', dmCatalogos.cdsSeccionesPerfil );
end;



end.
