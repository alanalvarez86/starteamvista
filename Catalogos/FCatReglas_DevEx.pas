unit FCatReglas_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, Grids, DBGrids,
     ZBaseConsulta,
     ZetaDBGrid, StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
     cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
      TressMorado2013,
     dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
     cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
     cxGridTableView, cxGridDBTableView, Menus, ActnList, ImgList,
     cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid,
     dxSkinsDefaultPainters;

type
  TCatReglas_DevEx = class(TBaseGridLectura_DevEx)
    CL_CODIGO: TcxGridDBColumn;
    CL_LETRERO: TcxGridDBColumn;
    CL_LIMITE: TcxGridDBColumn;
    CL_TIPOS: TcxGridDBColumn;
    CL_ACTIVO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations}
    procedure DoLookup; override;
  end;

var
  CatReglas_DevEx: TCatReglas_DevEx;

implementation

uses DCatalogos,
     ZetaBuscaEntero_DevEx,
     ZetaCommonClasses;

{$R *.DFM}

{ TCatReglas_DevEx }

procedure TCatReglas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext:= H60661_Reglas;
end;

procedure TCatReglas_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsReglas.Conectar;
          DataSource.DataSet := cdsReglas;
     end;
end;

procedure TCatReglas_DevEx.Refresh;
begin
     dmCatalogos.cdsReglas.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatReglas_DevEx.Agregar;
begin
     dmCatalogos.cdsReglas.Agregar;
end;

procedure TCatReglas_DevEx.Borrar;
begin
     dmCatalogos.cdsReglas.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatReglas_DevEx.Modificar;
begin
     dmCatalogos.cdsReglas.Modificar;
end;

procedure TCatReglas_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscaEntero_DevEx.BuscarCodigo( 'N�mero', 'Regla de Cafeter�a', 'CL_CODIGO', dmCatalogos.cdsReglas );
end;

procedure TCatReglas_DevEx.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
  inherited;

  //agrupar
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
  //ver la caja de agrupamiento
  ZetaDBGridDBTableView.OptionsView.GroupByBox := True;

   //columna sumatoria: cuantos
   CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('CL_CODIGO'), K_SIN_TIPO , '', skCount);
   ZetaDBGridDBTableView.ApplyBestFit();
 
end;

end.
