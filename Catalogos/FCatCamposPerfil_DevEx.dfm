inherited CatCamposPerfil_DevEx: TCatCamposPerfil_DevEx
  Left = 395
  Top = 333
  Caption = 'Campos de Perfil'
  ClientHeight = 272
  ClientWidth = 534
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 534
    inherited ValorActivo2: TPanel
      Width = 275
      inherited textoValorActivo2: TLabel
        Width = 269
      end
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 534
    Height = 38
    Align = alTop
    TabOrder = 3
    object Label1: TLabel
      Left = 19
      Top = 13
      Width = 42
      Height = 13
      Alignment = taRightJustify
      Caption = 'Secci'#243'n:'
    end
    object SECCIONES: TZetaKeyCombo
      Left = 64
      Top = 8
      Width = 289
      Height = 21
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 0
      OnChange = SECCIONESChange
      ListaFija = lfNinguna
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
    end
  end
  object Panel8: TPanel [2]
    Left = 504
    Top = 57
    Width = 30
    Height = 215
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 2
    object zmlArriba: TZetaSmartListsButton_DevEx
      Left = 2
      Top = 2
      Width = 26
      Height = 26
      Hint = 'Subir Registro'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = zmlArribaAbajoClick
      OptionsImage.Glyph.Data = {
        F6060000424DF606000000000000360000002800000018000000180000000100
        180000000000C0060000C30E0000C30E000000000000000000008B818C8B818C
        8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
        8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
        818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
        8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
        8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
        818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
        8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
        8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
        818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
        8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
        8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
        818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
        8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
        8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
        818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
        8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CACA5
        ACB7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B7
        B0B7B7B0B7B7B0B7B7B0B7B1ABB28B818C8B818C8B818C8B818C8B818C8B818C
        8B818C8B818C9D959EF4F3F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAF9FAA69FA78B818C8B818C8B818C8B
        818C8B818C8B818C8B818C8B818C8B818CA69FA7FAF9FAFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDB1ABB28B818C8B81
        8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CAAA3ABFD
        FDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7B0B7
        8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
        8C8B818C8B818CB1ABB2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFBEB8BF8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
        8B818C8B818C8B818C8B818C8B818C8B818CBEB8BFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFCCC8CC8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
        818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CC7C2C7
        FFFFFFFFFFFFFFFFFFFFFFFFD5D2D68B818C8B818C8B818C8B818C8B818C8B81
        8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
        818C8B818C8B818CCECACEFFFFFFFFFFFFDBD7DB8D838E8B818C8B818C8B818C
        8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
        8C8B818C8B818C8B818C8B818C8B818C8D838ED9D5D9E4E1E49087918B818C8B
        818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
        8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8D838E9087
        918B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
        818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
        8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
        8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
        818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
        8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
        8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
        818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
        8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
        8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
        818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
        8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
        8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
        818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
        8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
        8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
        818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C}
      OptionsImage.Margin = 1
      Tipo = bsSubir
    end
    object zmlAbajo: TZetaSmartListsButton_DevEx
      Tag = 1
      Left = 2
      Top = 32
      Width = 26
      Height = 26
      Hint = 'Bajar Registro'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = zmlArribaAbajoClick
      OptionsImage.Glyph.Data = {
        F6060000424DF606000000000000360000002800000018000000180000000100
        180000000000C0060000C30E0000C30E000000000000000000008B818C8B818C
        8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
        8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
        818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
        8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
        8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
        818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
        8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
        8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
        818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
        8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
        8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
        818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
        8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
        8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
        818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
        8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
        8C8B818C8B818C8B818C8B818C8B818C8B818C908791968D978B818C8B818C8B
        818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
        8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C908791E4E1E4EDEB
        ED968D978B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
        818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8D838E
        D9D5D9FFFFFFFFFFFFE4E1E49087918B818C8B818C8B818C8B818C8B818C8B81
        8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
        818C8B818CD3D0D4FFFFFFFFFFFFFFFFFFFFFFFFE0DDE08D838E8B818C8B818C
        8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
        8C8B818C8B818C8B818CCCC8CCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD9
        D5D98B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
        8B818C8B818C8B818C8B818C8B818CBEB8BFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFCCC8CC8B818C8B818C8B818C8B818C8B818C8B818C8B
        818C8B818C8B818C8B818C8B818C8B818C8B818CB5AEB5FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3BEC48B818C8B818C8B81
        8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CAFA9B0FDFDFDFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        BCB6BD8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CA69F
        A7FAF9FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFDFDFDB1ABB28B818C8B818C8B818C8B818C8B818C8B818C
        8B818C8B818CB1ABB2B7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B7B0
        B7B7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B5AEB58B818C8B818C8B818C8B
        818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
        8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
        8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
        818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
        8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
        8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
        818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
        8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
        8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
        818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
        8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
        8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
        818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
        8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
        8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
        818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C}
      OptionsImage.Margin = 1
      Tipo = bsBajar
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 57
    Width = 504
    Height = 215
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      object DF_ORDEN: TcxGridDBColumn
        Caption = 'Orden'
        DataBinding.FieldName = 'DF_ORDEN'
        Width = 64
      end
      object DF_TITULO: TcxGridDBColumn
        Caption = 'T'#237'tulo'
        DataBinding.FieldName = 'DF_TITULO'
        Width = 64
      end
      object DF_CAMPO: TcxGridDBColumn
        Caption = 'Campo'
        DataBinding.FieldName = 'DF_CAMPO'
        Width = 64
      end
      object DF_CONTROL: TcxGridDBColumn
        Caption = 'Capturar como'
        DataBinding.FieldName = 'DF_CONTROL'
        Width = 64
      end
      object DF_LIMITE: TcxGridDBColumn
        Caption = 'Ancho l'#237'mite'
        DataBinding.FieldName = 'DF_LIMITE'
        Width = 64
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 456
    Top = 232
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
