unit FCatContratos_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Grids, DBGrids, Db, ExtCtrls,
     ZBaseGridLectura_DevEx,
     ZetaDBGrid, StdCtrls, ZBaseConsulta, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TCatContratos_DevEx = class(TBaseGridLectura_DevEx)
    TB_CODIGO: TcxGridDBColumn;
    TB_ELEMENT: TcxGridDBColumn;
    TB_DIAS: TcxGridDBColumn;
    TB_ACTIVO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  CatContratos_DevEx: TCatContratos_DevEx;

implementation

uses DCatalogos,
     ZetaBuscador_DevEx,
     ZetaCommonClasses;

{$R *.DFM}

{ TCatContratos }

procedure TCatContratos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     //Desactivar gridmode (FormCreate)
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= FALSE;
     CanLookup := True;
     HelpContext := H60617_Tipo_contrato;
end;

procedure TCatContratos_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsContratos.Conectar;
          DataSource.DataSet:= cdsContratos;
     end;
end;

procedure TCatContratos_DevEx.Refresh;
begin
     dmCatalogos.cdsContratos.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatContratos_DevEx.Agregar;
begin
     dmCatalogos.cdsContratos.Agregar;
end;

procedure TCatContratos_DevEx.Borrar;
begin
     dmCatalogos.cdsContratos.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatContratos_DevEx.Modificar;
begin
     dmCatalogos.cdsContratos.Modificar;
end;

procedure TCatContratos_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', 'Contrato', 'TB_CODIGO', dmCatalogos.cdsContratos );
end;

procedure TCatContratos_DevEx.FormShow(Sender: TObject);
begin
      ApplyMinWidth;
  {***Banda Sumatoria***}
  CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
  inherited;

end;

end.

