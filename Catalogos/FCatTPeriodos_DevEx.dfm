inherited CatTPeriodos_DevEx: TCatTPeriodos_DevEx
  Left = 385
  Top = 343
  Caption = 'Tipos de Periodo'
  ClientWidth = 528
  ExplicitWidth = 528
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 528
    ExplicitWidth = 525
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 269
      ExplicitWidth = 266
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 263
        ExplicitLeft = 180
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 528
    ExplicitWidth = 525
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object TP_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'TP_NOMBRE'
        MinWidth = 100
        Width = 100
      end
      object TP_DESCRIP: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'TP_DESCRIP'
        MinWidth = 100
        Width = 100
      end
      object TP_CLAS: TcxGridDBColumn
        Caption = 'Clasificaci'#243'n'
        DataBinding.FieldName = 'TP_CLAS'
        MinWidth = 100
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
