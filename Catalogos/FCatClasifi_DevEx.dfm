inherited CatClasifi_DevEx: TCatClasifi_DevEx
  Left = 483
  Top = 217
  Caption = 'Clasificaciones'
  ClientWidth = 548
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 548
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 222
      inherited textoValorActivo2: TLabel
        Width = 216
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 548
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object TB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'TB_CODIGO'
      end
      object TB_ELEMENT: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'TB_ELEMENT'
      end
      object TB_SALARIO: TcxGridDBColumn
        Caption = 'Salario'
        DataBinding.FieldName = 'TB_SALARIO'
      end
      object TB_OP1: TcxGridDBColumn
        Caption = 'Fija 1'
        DataBinding.FieldName = 'TB_OP1'
      end
      object TB_OP2: TcxGridDBColumn
        Caption = 'Fija 2'
        DataBinding.FieldName = 'TB_OP2'
      end
      object TB_OP3: TcxGridDBColumn
        Caption = 'Fija 3'
        DataBinding.FieldName = 'TB_OP3'
      end
      object TB_OP4: TcxGridDBColumn
        Caption = 'Fija 4'
        DataBinding.FieldName = 'TB_OP4'
      end
      object TB_OP5: TcxGridDBColumn
        Caption = 'Fija 5'
        DataBinding.FieldName = 'TB_OP5'
      end
      object TB_SUB_CTA: TcxGridDBColumn
        Caption = 'Subcuenta'
        DataBinding.FieldName = 'TB_SUB_CTA'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
