unit FCatAccReglas_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, Grids, DBGrids,
     ZBaseConsulta,
     ZetaDBGrid, StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TCatAccReglas_DevEx = class(TBaseGridLectura_DevEx)
    AE_CODIGO: TcxGridDBColumn;
    AE_LETRERO: TcxGridDBColumn;
    AE_ACTIVO: TcxGridDBColumn;
    US_DESCRIP: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations}
    procedure DoLookup; override;
  end;

var
  CatAccReglas_DevEx: TCatAccReglas_DevEx;

implementation

uses DCatalogos,
     DSistema,
     ZetaBuscaEntero_DevEx,
     ZetaCommonClasses;

{$R *.DFM}

{ TCatReglas }

procedure TCatAccReglas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext:= H_REGLAS_CASETA;
end;

procedure TCatAccReglas_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmCatalogos do
     begin
          cdsAccReglas.Conectar;
          DataSource.DataSet := cdsAccReglas;
     end;
end;

procedure TCatAccReglas_DevEx.Refresh;
begin
     dmCatalogos.cdsAccReglas.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatAccReglas_DevEx.Agregar;
begin
     dmCatalogos.cdsAccReglas.Agregar;
end;

procedure TCatAccReglas_DevEx.Borrar;
begin
     dmCatalogos.cdsAccReglas.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatAccReglas_DevEx.Modificar;
begin
     dmCatalogos.cdsAccReglas.Modificar;
end;

procedure TCatAccReglas_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscaEntero_DevEx.BuscarCodigo( 'N�mero', 'Regla de Caseta', 'AE_CODIGO', dmCatalogos.cdsAccReglas );
end;

procedure TCatAccReglas_DevEx.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
  inherited;

   //columna sumatoria: cuantos
   CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('AE_CODIGO'), K_SIN_TIPO , '', skCount);
   ZetaDBGridDBTableView.ApplyBestFit();

end;

end.
