inherited CatSesiones: TCatSesiones
  Left = 201
  Top = 204
  Caption = 'Sesiones'
  ClientHeight = 361
  ClientWidth = 635
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 635
    Align = alNone
    inherited Slider: TSplitter
      Left = 16
    end
    inherited ValorActivo1: TPanel
      Align = alNone
      Visible = False
    end
    inherited ValorActivo2: TPanel
      Left = 304
      Width = 435
      Align = alNone
      Visible = False
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 65
    Width = 635
    Height = 296
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'SE_FOLIO'
        Title.Caption = 'Sesi�n'
        Width = 69
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CU_CODIGO'
        Title.Caption = 'Curso'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SE_REVISIO'
        Title.Caption = 'Revisi�n'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MA_NOMBRE'
        Title.Caption = 'Maestro'
        Width = 240
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SE_FEC_INI'
        Title.Caption = 'Inicio'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SE_FEC_FIN'
        Title.Caption = 'Fin'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SE_CUPO'
        Title.Caption = 'Cupo'
        Width = 35
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SE_COSTOT'
        Title.Caption = 'Costo Total'
        Width = 90
        Visible = True
      end>
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 0
    Width = 635
    Height = 65
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Label1: TLabel
      Left = 221
      Top = 36
      Width = 30
      Height = 13
      Alignment = taRightJustify
      Caption = 'Curso:'
    end
    object lblFecIni: TLabel
      Left = 17
      Top = 13
      Width = 63
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha Inicial:'
    end
    object lblFecFin: TLabel
      Left = 22
      Top = 37
      Width = 58
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha Final:'
    end
    object lblFolio: TLabel
      Left = 216
      Top = 12
      Width = 35
      Height = 13
      Alignment = taRightJustify
      Caption = 'Sesi�n:'
    end
    object sbBuscar: TSpeedButton
      Left = 329
      Top = 7
      Width = 23
      Height = 22
      Hint = 'Buscar Sesi�n Por Folio'
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D808DDDDDDDD
        DDDD83308DDDDDDDDDDD873308DDDDDDDDDDD373308DDDDDDDDDDD3733088888
        8DDDDDD37330000088DDDDDD37338777808DDDDDD038F7F77808DDDDD08FCCCC
        C788DDDCD0F7C7F7C770DDDCC07FCCCCCF70DDDCD0F7CFFCF770DDDDC88FCCCC
        7F88DDDDDD08F7F7F80DDDDDDDD08F7F80DDDDDDDDDD80008DDD}
      ParentShowHint = False
      ShowHint = True
      OnClick = sbBuscarClick
    end
    object CU_CURSO: TZetaKeyLookup
      Left = 255
      Top = 32
      Width = 325
      Height = 21
      TabOrder = 3
      TabStop = True
      WidthLlave = 70
      OnValidKey = CU_CURSOValidKey
    end
    object zFechaIni: TZetaFecha
      Left = 83
      Top = 8
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 0
      Text = '27/Jan/04'
      Valor = 38013
      OnValidDate = zFechaIniValidDate
    end
    object zFechaFin: TZetaFecha
      Left = 83
      Top = 32
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 1
      Text = '27/Jan/04'
      Valor = 38013
      OnValidDate = zFechaFinValidDate
    end
    object znFolio: TZetaNumero
      Left = 255
      Top = 8
      Width = 70
      Height = 21
      Mascara = mnDias
      TabOrder = 2
      Text = '0'
    end
  end
  inherited DataSource: TDataSource
    Left = 584
    Top = 96
  end
end
