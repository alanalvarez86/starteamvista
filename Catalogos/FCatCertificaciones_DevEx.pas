unit FCatCertificaciones_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, DB, ExtCtrls,
  StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid;

type
  TCatCertificaciones_DevEx = class(TBaseGridLectura_DevEx)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure DoLookup; override;
  protected
    { Public declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;

  end;

var
  CatCertificaciones_DevEx: TCatCertificaciones_DevEx;

implementation

uses ZetaDialogo,
     ZImprimeForma,
     ZetaTipoEntidad,
     ZetaBuscador_DevEx,
     ZetaCommonClasses, dCatalogos;

{$R *.dfm}
procedure TCatCertificaciones_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext:= H_Cat_Certificaciones;
end;



procedure TCatCertificaciones_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsCertificaciones.Conectar;
          DataSource.DataSet := cdsCertificaciones;
     end;
end;

procedure TCatCertificaciones_DevEx.Refresh;
begin
     dmCatalogos.cdsCertificaciones.Refrescar;
end;

procedure TCatCertificaciones_DevEx.Agregar;
begin
     dmCatalogos.cdsCertificaciones.Agregar;
end;

procedure TCatCertificaciones_DevEx.Borrar;
begin
     dmCatalogos.cdsCertificaciones.Borrar;
end;

procedure TCatCertificaciones_DevEx.Modificar;
begin
     dmCatalogos.cdsCertificaciones.Modificar;
end;

procedure TCatCertificaciones_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo',Self.Caption,'CI_CODIGO', dmCatalogos.cdsCertificaciones );
end;

procedure TCatCertificaciones_DevEx.FormShow(Sender: TObject);
begin
  CreaColumaSumatoria(ZetaDBGridDBTAbleView.Columns[0],0 ,'' ,SkCount ); 
  inherited;

end;

end.
