unit FEditCatSubfactor_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicionRenglon_DevEx, DB, ComCtrls,
  ExtCtrls, DBCtrls, StdCtrls, Mask, ZetaNumero,
  cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxPC, cxNavigator, cxDBNavigator, cxButtons, cxContainer,
  cxEdit, cxTextEdit, cxMemo, cxDBEdit, ZetaKeyLookup_DevEx,
  ZetaSmartLists_DevEx, Grids, DBGrids, ZetaDBGrid;

type
  TEditCatSubfactor_DevEx = class(TBaseEdicionRenglon_DevEx)
    VL_CODIGO: TZetaDBNumero;
    VL_NOMBRE: TEdit;
    VS_ORDEN: TZetaDBNumero;
    VS_CODIGO: TDBEdit;
    VS_NOMBRE: TDBEdit;
    Label5: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label1: TLabel;
    Label6: TLabel;
    VS_DESCRIP: TcxDBMemo;
    Label7: TLabel;
    VS_INGLES: TDBEdit;
    Label8: TLabel;
    VS_DES_ING: TcxDBMemo;
    Label10: TLabel;
    VS_NUMERO: TZetaDBNumero;
    Label9: TLabel;
    VS_TEXTO: TDBEdit;
    Panel3: TPanel;
    zmlSubir: TZetaSmartListsButton_DevEx;
    zmlBajar: TZetaSmartListsButton_DevEx;
    VF_ORDEN: TZetaDBKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure BBBorrarClick(Sender: TObject);
    procedure BBModificarClick(Sender: TObject);
    procedure zmlSubirBajarClick(Sender: TObject);
    procedure dsRenglonDataChange(Sender: TObject; Field: TField);
    procedure HabilitaControles; override;

  private
    { Private declarations }
    //procedure AsignaNiveles;
  protected
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  EditCatSubfactor_DevEx: TEditCatSubfactor_DevEx;

implementation

uses
    DCatalogos,
    ZetaCommonClasses,
    ZAccesosTress,
    ZetaCommonLists,
    ZetaDialogo,
    ZetaClientTools;

{$R *.dfm}

procedure TEditCatSubfactor_DevEx.Connect;
begin
     inherited;
     with dmCatalogos do
     begin
          VF_ORDEN.SetLlaveDescripcion( VACIO , VACIO );
          cdsValFactores.Conectar;
          cdsSubfactores.Conectar;
          DataSource.DataSet:= cdsSubfactores;
          if Inserting then
          begin
               with cdsValNiveles do
               begin
                    if ( State = dsInactive ) then
                       Refrescar;
                    EmptyDataSet;
               end;
          end
          else
             cdsValNiveles.Refrescar;
          GridRenglones.DataSource:= dsRenglon;
          dsRenglon.DataSet := dmCatalogos.cdsValNiveles;
          VL_NOMBRE.Text:= cdsValPlantilla.GetDescripcion( VL_CODIGO.Text );
     end;
end;

procedure TEditCatSubfactor_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_VAL_SUBFACTORES;
     FirstControl := VS_CODIGO;
     HelpContext := H_VALUACION_SUBFACTORES;
end;

procedure TEditCatSubfactor_DevEx.BBAgregarClick(Sender: TObject);
var
   sMensaje: String;
begin
     if ( dmCatalogos.PlantillaEnDiseno( dmCatalogos.PlantillaActiva, sMensaje, 'Agregar'  ) ) then
        dmCatalogos.cdsValNiveles.Agregar
     else
        ZInformation( Caption, sMensaje,0 );
end;

procedure TEditCatSubfactor_DevEx.BBBorrarClick(Sender: TObject);
var
   sMensaje: String;
begin
     with dmCatalogos do
     begin
           if ( dsRenglon.DataSet.IsEmpty ) then
              ZInformation('Niveles de Valuaci�n','No Hay Datos Para Modificar', 0 )
           else
           begin
                if ( dmCatalogos.PlantillaEnDiseno( dmCatalogos.PlantillaActiva, sMensaje,'Borrar'  ) ) then
                   cdsValNiveles.Borrar
                else
                    ZInformation( Caption, sMensaje, 0 );
           end;
     end;
end;

procedure TEditCatSubfactor_DevEx.BBModificarClick(Sender: TObject);
begin
     if ( dsRenglon.DataSet.IsEmpty ) then
        ZInformation('Niveles de Valuaci�n','No Hay Datos Para Modificar', 0 )
     else
        dmCatalogos.cdsValNiveles.Modificar;
end;

procedure TEditCatSubfactor_DevEx.zmlSubirBajarClick(Sender: TObject);
var sMensaje:string;
begin
     if  PuedeModificar(sMensaje) then
     begin
          with dmCatalogos do
     	  begin
               if ( PlantillaEnDiseno ( cdsValNiveles.FieldByName('VL_CODIGO').AsInteger, sMensaje, 'Cambiar Orden' ) ) then
               begin
                    SubeBajaOrden( eSubeBaja( TZetaSmartListsButton_DevEX(Sender).Tag ), cdsValNiveles,cdsValNiveles.FieldByName('VL_CODIGO').AsString, cdsValNiveles.FieldByName('VS_ORDEN').AsString);
               end
               else
                   ZInformation( Caption, sMensaje,0 );
     	  end;
     end
     else
         zInformation( Caption, 'No tiene permisos para cambiar el orden', 0 );
end;

procedure TEditCatSubfactor_DevEx.dsRenglonDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     zmlSubir.Enabled := not dsRenglon.DataSet.IsEmpty;
     zmlBajar.Enabled := not dsRenglon.DataSet.IsEmpty;
end;

procedure TEditCatSubfactor_DevEx.HabilitaControles;
begin
     inherited;
     SetEnabledControl( Tabla, not Inserting );
     SetEnabledControl( Panel2, not Inserting );
end;

end.
