unit FCatSeccionesPerfil_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, DB, ExtCtrls, StdCtrls, ZBaseGridLectura_DevEx, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons, ZetaSmartLists_DevEx;

type
  TCatSeccionesPerfil_DevEx = class(TBaseGridLectura_DevEx)
    Panel8: TPanel;
    zmlArriba: TZetaSmartListsButton_DevEx;
    zmlAbajo: TZetaSmartListsButton_DevEx;
    DT_ORDEN: TcxGridDBColumn;
    DT_CODIGO: TcxGridDBColumn;
    DT_NOMBRE: TcxGridDBColumn;
    DT_INGLES: TcxGridDBColumn;
    DT_NUMERO: TcxGridDBColumn;
    DT_TEXTO: TcxGridDBColumn;
    procedure zmlArribaAbajoClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
   procedure DoLookup; override;
  end;

var
  CatSeccionesPerfil_DevEx: TCatSeccionesPerfil_DevEx;

implementation
uses
    dCatalogos,
    ZetaBuscador_DevEx,
    ZetaCommonClasses,
    ZetaDialogo,
    ZetaMsgDlg,
    ZetaCommonLists;

{$R *.dfm}

{ TCatSeccionesPerfil }
procedure TCatSeccionesPerfil_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stNinguno;
     TipoValorActivo2 := stNinguno;
     HelpContext      := H_CAT_SECC_PERF;
     CanLookup        := True;
end;

procedure TCatSeccionesPerfil_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsSeccionesPerfil.Conectar;
          DataSource.DataSet := cdsSeccionesPerfil;
     end;
end;

procedure TCatSeccionesPerfil_DevEx.Refresh;
begin
     dmCatalogos.cdsSeccionesPerfil.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatSeccionesPerfil_DevEx.Agregar;
begin
     inherited;
     with dmCatalogos.cdsSeccionesPerfil do
     begin
          Agregar;
     end;
end;

procedure TCatSeccionesPerfil_DevEx.Borrar;
begin
     with dmCatalogos do
     begin
          cdsSeccionesPerfil.Borrar;
     end;
     ZetaDBGridDBTableView.ApplyBestFit();
end;


procedure TCatSeccionesPerfil_DevEx.Modificar;
begin
     inherited;
     with dmCatalogos.cdsSeccionesPerfil do
     begin
          Modificar;
     end;
end;

procedure TCatSeccionesPerfil_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo',Self.Caption, 'DT_CODIGO', dmCatalogos.cdsSeccionesPerfil );
end;

procedure TCatSeccionesPerfil_DevEx.zmlArribaAbajoClick(Sender: TObject);
var sMensaje:string;
begin
     if  PuedeModificar(sMensaje) then
     begin
          with dmCatalogos do
     	  begin
               SubeBajaOrden( eSubeBaja( TZetaSmartListsButton_DevEX(Sender).Tag ), cdsSeccionesPerfil,VACIO,VACIO);
     	  end;
     end
     else
         zInformation( Caption, 'No tiene permisos para cambiar el orden', 0 );
end;

procedure TCatSeccionesPerfil_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     zmlArriba.Enabled := not NoHayDatos;
     zmlAbajo.Enabled := not NoHayDatos;
end;

procedure TCatSeccionesPerfil_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('DT_ORDEN'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

end.
