unit FCatPrestaciones_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Grids, DBGrids, Db, StdCtrls, ExtCtrls,
     ZBaseGridLectura_DevEx,
     ZetaDBGrid,
     ZetaDBTextBox, ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid;

type
  TCatPrestaciones_DevEx = class(TBaseGridLectura_DevEx)
    TB_CODIGO: TcxGridDBColumn;
    TB_ELEMENT: TcxGridDBColumn;
    TB_PRIMAVA: TcxGridDBColumn;
    TB_DIAS_AG: TcxGridDBColumn;
    TB_ACTIVO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations}
    procedure DoLookup; override;
  end;

var
  CatPrestaciones_DevEx: TCatPrestaciones_DevEx;

implementation


uses DCatalogos,
     ZetaBuscador_DevEx,
     ZetaCommonClasses;

{$R *.DFM}

{ TCatPrestaciones }

procedure TCatPrestaciones_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H60616_Prestaciones;
end;

procedure TCatPrestaciones_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsSSocial.Conectar;
          DataSource.DataSet:= cdsSSocial;
     end;
end;

procedure TCatPrestaciones_DevEx.Refresh;
begin
     dmCatalogos.cdsSSocial.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatPrestaciones_DevEx.Agregar;
begin
     dmCatalogos.cdsSSocial.Agregar;
end;

procedure TCatPrestaciones_DevEx.Borrar;
begin
     dmCatalogos.cdsSSocial.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatPrestaciones_DevEx.Modificar;
begin
     dmCatalogos.cdsSSocial.Modificar;
end;

procedure TCatPrestaciones_DevEx.DoLookup;
begin
inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', 'Prestaciones', 'TB_CODIGO', dmCatalogos.cdsSSocial );
end;

procedure TCatPrestaciones_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     {***Banda Sumatoria***}
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
     inherited;

end;

end.
