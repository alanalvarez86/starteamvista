inherited CatAccReglas_DevEx: TCatAccReglas_DevEx
  Left = 319
  Top = 184
  Caption = 'Reglas de Caseta'
  PixelsPerInch = 96
  TextHeight = 13
  inherited ZetaDBGrid: TZetaCXGrid
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object AE_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'AE_CODIGO'
      end
      object AE_LETRERO: TcxGridDBColumn
        Caption = 'Letrero'
        DataBinding.FieldName = 'AE_LETRERO'
      end
      object AE_ACTIVO: TcxGridDBColumn
        Caption = 'Activa'
        DataBinding.FieldName = 'AE_ACTIVO'
      end
      object US_DESCRIP: TcxGridDBColumn
        Caption = 'Usuario'
        DataBinding.FieldName = 'US_DESCRIP'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
