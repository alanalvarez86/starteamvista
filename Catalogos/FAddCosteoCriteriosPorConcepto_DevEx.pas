unit FAddCosteoCriteriosPorConcepto_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, CheckLst, ComCtrls, DB, ExtCtrls,
  DBCtrls, ZetaKeyLookup_DevEx, ZetaDBTextBox, ZBaseDlgModal_DevEx,
  cxCheckListBox, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, ImgList, cxButtons, cxControls,
  cxContainer, cxEdit,cxGroupBox, dxSkinscxPCPainter, cxPCdxBarPopupMenu,
  cxPC;

type
  eAccionistBox = ( eTodos, eNinguno );

  TAddCosteoCriteriosPorConcepto_DevEx = class(TZetaDlgModal_DevEx)
    PageControl1: TcxPageControl;
    TabSheet1: TcxTabSheet;
    TabSheet2: TcxTabSheet;
    GroupBox3: TcxGroupBox;
    Otros: TcxCheckListBox;
    GroupBox2: TcxGroupBox;
    Percepciones: TcxCheckListBox;
    GroupBox1: TcxGroupBox;
    Deducciones: TcxCheckListBox;
    Splitter1: TSplitter;
    Panel1: TPanel;
    Panel4: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    dsCriterios: TDataSource;
    ApagaBtn: TcxButton;
    PrendeBtn: TcxButton;
    Panel2: TPanel;
    BitBtn1: TcxButton;
    BitBtn2: TcxButton;
    Panel3: TPanel;
    BitBtn3: TcxButton;
    BitBtn4: TcxButton;
    dsGrupos: TDataSource;
    CosteoGrupos: TZetaKeyLookup_DevEx;
    CosteoCriterios: TZetaKeyLookup_DevEx;
    ConceptosDisponibles: TcxButton;
    procedure OK_DevExClick(Sender: TObject);
    procedure PercepcionesClickCheck( Sender: TObject; AIndex: Integer;
              APrevState, ANewState: TcxCheckBoxState);
    procedure DeduccionesClickCheck( Sender: TObject; AIndex: Integer;
              APrevState, ANewState: TcxCheckBoxState);
    procedure OtrosClickCheck( Sender: TObject; AIndex: Integer;
              APrevState, ANewState: TcxCheckBoxState);
    procedure FormShow(Sender: TObject);
    procedure PrendeBtnClick(Sender: TObject);
    procedure ApagaBtnClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure ConceptosDisponiblesClick(Sender: TObject);

  private
    bEditing: Boolean;
    procedure Selecciona(lbLista: TcxCheckListBox; eAccion: eAccionistBox);
    procedure LlenaCheckListBox(lbLista: TcxCheckListBox; Tipo: Integer);
    procedure VaciaCheckListBox(lbLista: TcxCheckListBox);
    procedure EscribirCambios;
    procedure GrabaCambios(lbLista: TcxCheckListBox);
    procedure ActivaSegundaZona(Activa : Boolean);
    procedure HabilitaControles;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AddCosteoCriteriosPorConcepto_DevEx: TAddCosteoCriteriosPorConcepto_DevEx;

implementation

{$R *.dfm}

uses
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaDialogo,
     ZAccesosTress,
     dCatalogos, ZetaClientDataSet;

const
     K_PERCEPCION = 1;
     K_DEDDUCCION = 2;
     K_OTRO = 3;
     K_MSJ_NOGRUPO_CRITERIO_SEL = 'Para agregar un Concepto es necesario indicar el Grupo y el Criterio.';
     
procedure TAddCosteoCriteriosPorConcepto_DevEx.FormShow(Sender: TObject);
begin
     HelpContext := H00003_Costeo_CriteriosPorConcepto;
     PageControl1.ActivePage := TabSheet1;
     with dmCatalogos do
     begin
          dsGrupos.DataSet := cdsCosteoGrupos;
          dsCriterios.DataSet := cdsCosteoCriterios;

          CosteoGrupos.LookupDataset := cdsCosteoGrupos;
          CosteoCriterios.LookupDataset := cdsCosteoCriterios;
     end;
     ActivaSegundaZona(False);
     CosteoGrupos.SetFocus;
     bEditing := False;
end;

procedure TAddCosteoCriteriosPorConcepto_DevEx.LlenaCheckListBox( lbLista : TcxCheckListBox; Tipo : Integer );
var
   iConcepto : Integer;
   sFiltroConceptos : String;
   cItem : TcxCheckListBoxItem;
begin
     lbLista.Items.Clear;
     case Tipo of
       K_PERCEPCION: sFiltroConceptos := Format('( CO_TIPO = %d or CO_TIPO = %d )', [ ord( coPercepcion ), ord( coPrestacion ) ]);
       K_DEDDUCCION: sFiltroConceptos := Format('( CO_TIPO = %d or CO_TIPO = %d )', [ ord( coDeduccion ), ord( coObligacion ) ]);
       K_OTRO: sFiltroConceptos := Format('( CO_TIPO = %d or CO_TIPO = %d or CO_TIPO = %d )', [ ord( coCalculo ), ord( coCaptura ), ord( coResultados ) ]);
     end;
     lbLista.Items.Clear;
     with dmCatalogos.cdsConceptosDisponibles do
     begin
          Filter := Format( '%s and CO_NUMERO <= %d' , [ sFiltroConceptos, K_MAX_CONCEPTO ]);
          Filtered := True;
          First;
          while not EOF do
          begin
               iConcepto := FieldByName('CO_NUMERO').AsInteger;
               //lbLista.Items.AddObject( IntToStr( iConcepto ) + '=' + FieldByName('CO_DESCRIP').AsString, TObject( iConcepto ) );
               cItem := lbLista.Items.Add;
               cItem.ItemObject :=  TObject( iConcepto );
               cItem.Text := IntToStr( iConcepto ) + '=' + FieldByName('CO_DESCRIP').AsString;
               //lbLista.Checked[ lbLista.Items.Count - 1 ] := ( FieldByName('Encendido').AsInteger <> 0 );
               cItem.Checked := ( FieldByName('Encendido').AsInteger <> 0 );
               Next;
          end;
          Filtered := False;
     end;
end;

//Mecanismo para seleccionar y deseleccionar (eAccion) todos los items de un grid (lbLista)
procedure TAddCosteoCriteriosPorConcepto_DevEx.Selecciona(lbLista: TcxCheckListBox; eAccion : eAccionistBox);
var
   i : Integer;
   lHuboCambios : Boolean;
begin
     lHuboCambios := False;
     for i := 0 to lbLista.Items.Count - 1 do
     begin
          //if ( lbLista.Checked[i] <> ( eAccion = eTodos ) ) then
          if ( lbLista.Items.Items[i].Checked <> ( eAccion = eTodos ) ) then   //DevEx
               lHuboCambios := true;
          //lbLista.Checked[i] := ( eAccion = eTodos );
          lbLista.Items.Items[i].Checked := ( eAccion = eTodos );   //DevEx
     end;
     if (lHuboCambios) then
          OK_DevEx.Enabled := True;
end;

procedure TAddCosteoCriteriosPorConcepto_DevEx.OK_DevExClick(Sender: TObject);
begin
     EscribirCambios;
end;

procedure TAddCosteoCriteriosPorConcepto_DevEx.EscribirCambios;
begin
     try
        with dmCatalogos.cdsCosteoCriteriosPorConcepto do
        begin
             AfterDelete := nil;
             GrabaCambios( Percepciones );
             GrabaCambios( Deducciones );
             GrabaCambios( Otros );
             Enviar;
        end;
     finally
            dmCatalogos.cdsCosteoCriteriosPorConcepto.AfterDelete := dmCatalogos.cdsCosteoCriteriosPorConceptoAfterDelete;
     end;
end;

procedure TAddCosteoCriteriosPorConcepto_DevEx.GrabaCambios( lbLista: TcxCheckListBox );
var
   i, iConcepto : Integer;
   lAgrega : Boolean;
begin
     with lbLista, dmCatalogos.cdsCosteoCriteriosPorConcepto do
     begin
          Filter := Format('GpoCostoCodigo = %s and CritCostoID = %d',[ EntreComillas(CosteoGrupos.Llave), CosteoCriterios.Valor ]);
          Filtered := True;
          for i := 0 to Items.Count - 1 do
          begin
              //lAgrega := Checked[i];
              lAgrega := Items.Items[i].Checked;    //DevEx
              iConcepto := Integer( lbLista.Items.Objects[i] );
               if ( Locate('CO_NUMERO', iConcepto, [] ) ) then
               begin
                    if ( not lAgrega ) then
                         Delete;
               end
               else
               begin
                    if ( lAgrega ) then
                    begin
                         Append;
                         FieldByName('CO_NUMERO').AsInteger := iConcepto;
                         Post;
                    end;
               end;
          end;
          Filtered := False;
     end;
end;

procedure TAddCosteoCriteriosPorConcepto_DevEx.HabilitaControles;
begin
     OK_DevEx.Enabled := bEditing;
end;


procedure TAddCosteoCriteriosPorConcepto_DevEx.PercepcionesClickCheck (
  Sender: TObject; AIndex: Integer; APrevState, ANewState: TcxCheckBoxState);
begin
   inherited;
   bEditing := True;
   HabilitaControles;

end;

procedure TAddCosteoCriteriosPorConcepto_DevEx.DeduccionesClickCheck(
  Sender: TObject; AIndex: Integer; APrevState, ANewState: TcxCheckBoxState);
begin
     inherited;
     bEditing := True;
     HabilitaControles;
end;

procedure TAddCosteoCriteriosPorConcepto_DevEx.OtrosClickCheck (
Sender: TObject; AIndex: Integer; APrevState, ANewState: TcxCheckBoxState);
begin
     inherited;
     bEditing := True;
     HabilitaControles;
end;

procedure TAddCosteoCriteriosPorConcepto_DevEx.PrendeBtnClick(Sender: TObject);
begin
     Selecciona( Percepciones, eTodos );
end;

procedure TAddCosteoCriteriosPorConcepto_DevEx.ApagaBtnClick(Sender: TObject);
begin
     Selecciona( Percepciones, eNinguno );
end;

procedure TAddCosteoCriteriosPorConcepto_DevEx.BitBtn2Click(Sender: TObject);
begin
     Selecciona( Deducciones, eTodos );
end;

procedure TAddCosteoCriteriosPorConcepto_DevEx.BitBtn1Click(Sender: TObject);
begin
     Selecciona( Deducciones, eNinguno );
end;

procedure TAddCosteoCriteriosPorConcepto_DevEx.BitBtn4Click(Sender: TObject);
begin
     Selecciona( Otros, eTodos );
end;

procedure TAddCosteoCriteriosPorConcepto_DevEx.BitBtn3Click(Sender: TObject);
begin
     Selecciona( Otros, eNinguno );
end;

procedure TAddCosteoCriteriosPorConcepto_DevEx.VaciaCheckListBox(lbLista: TcxCheckListBox);
begin
     lbLista.Items.Clear;
end;

procedure TAddCosteoCriteriosPorConcepto_DevEx.ActivaSegundaZona(Activa : Boolean);

begin
     Percepciones.Enabled := Activa;
     Deducciones.Enabled := Activa;
     Otros.Enabled := Activa;

     PrendeBtn.Enabled := Activa;
     ApagaBtn.Enabled := Activa;
     BitBtn2.Enabled := Activa;
     BitBtn1.Enabled := Activa;
     BitBtn4.Enabled := Activa;
     BitBtn3.Enabled := Activa;

     CosteoGrupos.Enabled := not Activa;
     CosteoCriterios.Enabled := not Activa;


     if not Activa then
     begin
          VaciaCheckListBox( Percepciones );
          VaciaCheckListBox( Deducciones );
          VaciaCheckListBox( Otros );

          CosteoGrupos.Llave := VACIO;
          CosteoCriterios.Llave := VACIO;
     end;
end;

procedure TAddCosteoCriteriosPorConcepto_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
     bEditing := False;
     ActivaSegundaZona(False);
     HabilitaControles;
end;

procedure TAddCosteoCriteriosPorConcepto_DevEx.ConceptosDisponiblesClick(
  Sender: TObject);
begin
     //Muestra los conceptos Disponibles
     if ( StrLleno( CosteoGrupos.Llave ) and StrLleno( CosteoCriterios.Llave ) ) then
     begin
          with dmCatalogos do
          begin
               GrupoDeCosteo := CosteoGrupos.Llave;
               CriterioDeCosteo := CosteoCriterios.Valor;
               cdsConceptosDisponibles.Refrescar;
          end;
          ActivaSegundaZona(True);
          LlenaCheckListBox( Percepciones, K_PERCEPCION );
          LlenaCheckListBox( Deducciones, K_DEDDUCCION );
          LlenaCheckListBox( Otros, K_OTRO );
     end
     else
          ZetaDialogo.zInformation( 'Edici�n de Conceptos de Costeo', K_MSJ_NOGRUPO_CRITERIO_SEL, 0 );
end;

end.

