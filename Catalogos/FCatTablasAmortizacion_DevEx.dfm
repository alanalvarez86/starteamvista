inherited CatTablasAmortizacion_DevEx: TCatTablasAmortizacion_DevEx
  Left = 243
  Top = 155
  Caption = 'Tablas de Cotizaci'#243'n SGM'
  ClientWidth = 488
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 488
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 162
      inherited textoValorActivo2: TLabel
        Width = 156
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 488
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object AT_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'AT_CODIGO'
      end
      object AT_DESCRIP: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'AT_DESCRIP'
      end
      object AT_INGLES: TcxGridDBColumn
        Caption = 'Ingl'#233's'
        DataBinding.FieldName = 'AT_INGLES'
      end
      object AT_NUMERO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'AT_NUMERO'
      end
      object AT_ACTIVO: TcxGridDBColumn
        Caption = 'Activo'
        DataBinding.FieldName = 'AT_ACTIVO'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
