unit FListaPuestos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ZetaCommonTools,
  StdCtrls, checklst, Buttons, ExtCtrls ,ZetaClientDataSet, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxButtons, ImgList, cxControls, cxContainer, cxEdit, cxCheckListBox,
  System.Actions, Vcl.ActnList;

type
  TListaPuestos_DevEx = class(TForm)
    PanelInferior: TPanel;
    cxImageList24_PanelBotones: TcxImageList;
    OK_DevEx: TcxButton;
    Cancelar_DevEx: TcxButton;
    PrendeBtn_DevEx: TcxButton;
    ApagaBtn_DevEx: TcxButton;
    Lista_DevEx: TcxCheckListBox;
    PanelBusqueda: TPanel;
    Label1: TLabel;
    EditBusca: TEdit;
    BuscaBtn: TcxButton;
    chkActivos: TCheckBox;
    alBusqueda: TActionList;
    acBuscarPuestos: TAction;
    procedure PrendeBtnClick(Sender: TObject);
    procedure ApagaBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure PrendeBtn_DevExClick(Sender: TObject);
    procedure ApagaBtn_DevExClick(Sender: TObject);
    procedure BuscaBtnClick(Sender: TObject);
    procedure chkActivosClick(Sender: TObject);
    procedure acBuscarPuestosExecute(Sender: TObject);
    procedure EditBuscaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    FCurso: String;
    FListaTemp: TStringList;
    FMatrizCursos : Boolean;


    FLista: TStrings;
    FUltimoTexto: String;
    FPosUltimo: Integer;
    function EncontroPuesto: Boolean;

    procedure PrendeApaga( lState: Boolean );
    procedure AgregaListaPuestos;
    procedure FiltraPuestosProg;
    function GetDataSet: TZetaClientDataSet;

    procedure Busqueda;

  public
    property Curso: String read FCurso write FCurso;
    property MatrizCursos :Boolean read FMatrizCursos write FMatrizCursos;
  end;

function SetListaPuestos( const sCurso: String ; const MatrizCurso:Boolean ): Boolean;

var
  ListaPuestos_DevEx: TListaPuestos_DevEx;

implementation

uses dCatalogos, ZetaDialogo;

function SetListaPuestos( const sCurso: String; const MatrizCurso:Boolean  ): Boolean;
begin
     if ( ListaPuestos_DevEx = nil ) then
        ListaPuestos_DevEx:= TListaPuestos_DevEx.Create( Application );
     try
        with ListaPuestos_DevEx do
        begin
             Curso := sCurso;
             MatrizCursos := MatrizCurso;
             ShowModal;
             Result := ( ModalResult = mrOk );
        end;
     finally
        FreeAndNil( ListaPuestos_DevEx );
     end;
end;

{$R *.DFM}

procedure TListaPuestos_DevEx.FormCreate(Sender: TObject);
begin
     FListaTemp:= TStringList.Create;
     //HelpContext := H80815_Supervision_de_usuarios;
end;

procedure TListaPuestos_DevEx.FormShow(Sender: TObject);
begin
     chkActivos.Checked := TRUE;
     dmCatalogos.CargaListaPuestos( Lista_DevEx.Items, chkActivos.Checked );
end;

procedure TListaPuestos_DevEx.FormDestroy(Sender: TObject);
begin
     FListaTemp.Free;
end;

procedure TListaPuestos_DevEx.acBuscarPuestosExecute(Sender: TObject);
begin
     Busqueda();
end;

procedure TListaPuestos_DevEx.AgregaListaPuestos;
var
   i: Integer;
   tmpLista: TStringList;
   sCodigo : String;
procedure Split(Delimiter: Char; Str: string; ListOfStrings: TStrings) ;
begin
   ListOfStrings.Clear;
   ListOfStrings.Delimiter     := Delimiter;
   ListOfStrings.DelimitedText := Str;
end;
begin
     FiltraPuestosProg;
     tmpLista := Tstringlist.create();

     for i := 0 to Lista_DevEx.Items.Count - 1 do
         if Lista_DevEx.Items[ i ] .Checked then
         begin
              //DevEx (by am): Separamos la cadena para obtener el codigo
             Split('=',Lista_DevEx.Items[i].Text,tmpLista);
             sCodigo := trim(tmpLista[0]);
             dmCatalogos.GuardaEntrenaPuestosProg( sCodigo, Curso,FMatrizCursos  );
         end;
end;

procedure TListaPuestos_DevEx.PrendeBtnClick(Sender: TObject);
begin
     PrendeApaga( True );
end;

procedure TListaPuestos_DevEx.ApagaBtnClick(Sender: TObject);
begin
     PrendeApaga( False );
end;

procedure TListaPuestos_DevEx.PrendeApaga( lState: Boolean );
var
   i: Integer;
begin
     with Lista_DevEx do
     begin
          with Items do
          begin
               try
                  BeginUpdate;
                  for i := 0 to ( Count - 1 ) do
                      Items[ i ].Checked := lState;
               finally
                      EndUpdate;
               end;
          end;
     end;
end;

function TListaPuestos_DevEx.GetDataSet:TZetaClientDataSet;
begin
     if FMatrizCursos then
     begin
          Result := dmCatalogos.cdsMatrizCurso ;
     end
     else
         Result := dmCatalogos.cdsMatrizCertificPuesto;

end;

procedure TListaPuestos_DevEx.FiltraPuestosProg;
var
   iPos: Integer;
begin
     with GetDataSet, Lista_DevEx.Items do
     begin
          BeginUpdate;
          First;
          while not EOF do
          begin
               iPos := IndexOf( FieldByName( 'PU_CODIGO' ).AsString + '=' +
                                FieldByName( 'PU_NOMBRE' ).AsString );
               if ( iPos >= 0 ) then
                  Lista_DevEx.Items[iPos].Checked := FALSE;
               Next;
          end;
          EndUpdate;
     end;
end;

procedure TListaPuestos_DevEx.OK_DevExClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     with GetDataSet  do
     begin
          DisableControls;
          try
             with Screen do
             begin
                  oCursor := Cursor;
                  Cursor := crHourglass;
                  try
                     AgregaListaPuestos;
                     Enviar;
                  finally
                     Cursor := oCursor;
                  end;
             end;
             if ( ChangeCount > 0 ) then     // Algunos no se pudieron agregar
             begin
                  CancelUpdates;
                  ZError( self.Caption, 'No se agregaron todos los puestos seleccionados', 0 );
             end;
          finally
             EnableControls;
          end;
     end;
end;

procedure TListaPuestos_DevEx.PrendeBtn_DevExClick(Sender: TObject);
begin
     PrendeApaga( True );
end;

procedure TListaPuestos_DevEx.ApagaBtn_DevExClick(Sender: TObject);
begin
      PrendeApaga( False );
end;

procedure TListaPuestos_DevEx.BuscaBtnClick(Sender: TObject);
begin
     Busqueda ();
end;

procedure TListaPuestos_DevEx.EditBuscaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     if ( Key = VK_RETURN ) then
     begin
          Key := 0;
          Busqueda ();
     end;
     inherited;
end;

function TListaPuestos_DevEx.EncontroPuesto: Boolean;
var
   i : Integer;
   sTexto: String;
begin
     sTexto:= UpperCase( EditBusca.Text );
     with Lista_DevEx do
     begin
          if ( sTexto <> FUltimoTexto ) then
          begin
               FUltimoTexto := sTexto;
               FPosUltimo:= 0;
          end;
          Result:= False;
          for i:= FPosUltimo  to ( Count - 1 ) do
          begin
               if ( Pos( sTexto, UpperCase( Items[i].text ) ) > 0 ) then
               begin
                    FUltimoTexto:= sTexto;
                    FPosUltimo:= i;
                    Selected[ FPosUltimo ]:= True;
                    FPosUltimo:= FPosUltimo + 1;
                    Result:= True;
                    Exit;
               end;
          end;
     end;
end;

procedure  TListaPuestos_DevEx.Busqueda;
var
   lEncontro: Boolean;
begin
     if strLleno( EditBusca.Text ) and ( FPosUltimo <> Lista_DevEx.Count )then
     begin
          lEncontro:= EncontroPuesto;
     end
     else
     begin
          lEncontro:= FALSE;
     end;
     if ( not lEncontro ) then
     begin
          Lista_DevEx.Selected[ FPosUltimo ]:= False;

          if( FPosUltimo > 0 ) then
              ZetaDialogo.ZInformation( 'B�squeda de puestos', 'No hay otro puesto con esos datos' ,0 )
          else
              ZetaDialogo.ZInformation( 'B�squeda de puestos', 'No hay un puesto con esos datos' ,0 );
          with EditBusca do
          begin
               SetFocus;
               SelectAll;
          end;

          FPosUltimo:=0;
     end;
end;

procedure TListaPuestos_DevEx.chkActivosClick(Sender: TObject);
begin
     dmCatalogos.CargaListaPuestos( Lista_DevEx.Items, chkActivos.Checked );
end;

end.
