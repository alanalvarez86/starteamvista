{$HINTS OFF}
unit FMatrizCertificaPuesto_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls, StdCtrls,
  ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid, ZetaKeyLookup_DevEx, System.Actions;

type
  TMatrizCertificaPuesto_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    LookUpPuesto: TZetaKeyLookup_DevEx;
    CI_CODIGO: TcxGridDBColumn;
    CI_NOMBRE: TcxGridDBColumn;
    PC_DIAS: TcxGridDBColumn;
    PC_OPCIONA: TcxGridDBColumn;
    procedure LookUpPuestoValidKey(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SetEditarSoloActivos;
  private
    procedure ConfigAgrupamiento;
  protected
    procedure Connect; override;
    procedure DisConnect;override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
  public
  end;

var
  MatrizCertificaPuesto_DevEx: TMatrizCertificaPuesto_DevEx;

implementation

uses dCatalogos, dCliente, ZetaCommonClasses, FAutoClasses;

{$R *.DFM}

{ TCatMatrizPuesto }

procedure TMatrizCertificaPuesto_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H_MatrizCertificacionesPorPuesto  ;{OP: 25/06/08}
     //HelpContext:= HMatrizCertificacionesPorPuesto  ;

     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TMatrizCertificaPuesto_DevEx.DisConnect;
begin
     inherited;
     LookUpPuesto.LookUpDataset := NIL;
end;

procedure TMatrizCertificaPuesto_DevEx.Connect;
begin
     LookUpPuesto.LookUpDataset :=  dmCatalogos.cdsPuestos;
     with dmCatalogos do
     begin
          with cdsPuestos do
          begin
               Conectar;
               //First;
               LookUpPuesto.Llave := FieldByName('PU_CODIGO').AsString;
          end;
          MatrizPorCertific := False;
          cdsMatrizCertificPuesto.Conectar;
          DataSource.DataSet:= cdsMatrizCertificPuesto;
          DoRefresh;
     end;
end;

procedure TMatrizCertificaPuesto_DevEx.Refresh;
begin
     dmCatalogos.cdsMatrizCertificPuesto.Refrescar;
     ZetaDBGridDBTableView.Columns[ 0 ].DataBinding.FieldName := 'CI_CODIGO';
     ZetaDBGridDBTableView.Columns[ 1 ].DataBinding.FieldName := 'CI_NOMBRE';
     ZetaDBGridDBTableView.Columns[ 2 ].DataBinding.FieldName := 'PC_DIAS';
     ZetaDBGridDBTableView.Columns[ 3].DataBinding.FieldName := 'PC_OPCIONA';
     ZetaDBGridDBTableView.ApplyBestFit();

end;

procedure TMatrizCertificaPuesto_DevEx.LookUpPuestoValidKey(Sender: TObject);
begin
     inherited;
     DoRefresh;
end;

procedure TMatrizCertificaPuesto_DevEx.Agregar;
begin
     dmCatalogos.cdsMatrizCertificPuesto.Agregar;
end;

procedure TMatrizCertificaPuesto_DevEx.Borrar;
begin
     dmCatalogos.cdsMatrizCertificPuesto.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TMatrizCertificaPuesto_DevEx.Modificar;
begin
     dmCatalogos.cdsMatrizCertificPuesto.Modificar;
end;

function TMatrizCertificaPuesto_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeAgregar( sMensaje );
end;

function TMatrizCertificaPuesto_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeModificar( sMensaje );
end;

function TMatrizCertificaPuesto_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeBorrar( sMensaje );
end;

procedure TMatrizCertificaPuesto_DevEx.ConfigAgrupamiento;
begin
     //Cambio de configuracion
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //Desactiva la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Esconde la caja de agrupamiento
     //Determinar las columnas por las que se podra agrupar
     CI_CODIGO.Options.Grouping:= FALSE;
     CI_NOMBRE.Options.Grouping:= FALSE;
     PC_DIAS.Options.Grouping:= FALSE;
end;

procedure TMatrizCertificaPuesto_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     {***Banda Sumatoria***}
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
     inherited;
     ConfigAgrupamiento;
end;

procedure TMatrizCertificaPuesto_DevEx.SetEditarSoloActivos;
begin
     LookUpPuesto.EditarSoloActivos := TRUE;
end;

end.
