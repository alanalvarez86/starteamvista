unit FPreguntaPassWordSAT_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx, StdCtrls, Mask, ZetaNumero, ZetaDBTextBox, Buttons,
  ExtCtrls, ZetaEdit, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, ImgList, cxButtons, cxControls, cxContainer,
  cxEdit, cxGroupBox;

type
  TPreguntaPassWordSAT_DevEx = class(TZetaDlgModal_DevEx)
    GroupBox1: TcxGroupBox;
    ePassWord: TZetaEdit;
    procedure FormShow(Sender: TObject);
  private
    function GetPassWord: string;
    { Private declarations }
  public
    property Clave : string read GetPassWord;
  end;

var
  PreguntaPassWordSAT_DevEx: TPreguntaPassWordSAT_DevEx;

  function GetPassWord: Boolean;

implementation

uses ZetaCommonClasses, ZetaDialogo;

{$R *.DFM}

function GetPassWord: Boolean;
begin
     if ( PreguntaPassWordSAT_DevEx = nil ) then
        PreguntaPassWordSAT_DevEx := TPreguntaPassWordSAT_DevEx.Create(Application);
     with PreguntaPassWordSAT_DevEx do
     begin
          ShowModal;
          Result := ( ModalResult = mrOk );
     end;
end;

procedure TPreguntaPassWordSAT_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     ePassWord.SetFocus;
     ePassWord.Text := '';
end;

function TPreguntaPassWordSAT_DevEx.GetPassWord: string;
begin
     Result := ePassWord.Text;
end;

end.
