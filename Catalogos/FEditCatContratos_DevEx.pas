unit FEditCatContratos_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, Buttons, DBCtrls, StdCtrls, Mask,
     ZBaseEdicion_DevEx,
     ZetaNumero,
     ZetaEdit, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC, dxBarExtItems, dxBar,
  cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons, dxBarBuiltInMenu,
  ZetaKeyLookup_DevEx;

type
  TEditCatContratos_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    PageControl: TcxPageControl;
    TabGenerales: TcxTabSheet;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    TB_CODIGO: TZetaDBEdit;
    TB_ELEMENT: TDBEdit;
    TB_INGLES: TDBEdit;
    TB_NUMERO: TZetaDBNumero;
    TB_TEXTO: TDBEdit;
    TB_DIAS: TZetaDBNumero;
    TB_ACTIVO: TDBCheckBox;
    TabConfidencialidad: TcxTabSheet;
    gbConfidencialidad: TGroupBox;
    listaConfidencialidad: TListBox;
    rbConfidenTodas: TRadioButton;
    rbConfidenAlgunas: TRadioButton;
    btSeleccionarConfiden_DevEx: TcxButton;
    SAT: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    ZetaDBEdit1: TZetaDBEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    ZetaDBNumero1: TZetaDBNumero;
    DBEdit3: TDBEdit;
    ZetaDBNumero2: TZetaDBNumero;
    DBCheckBox1: TDBCheckBox;
    cxTabSheet2: TcxTabSheet;
    GroupBox1: TGroupBox;
    ListBox1: TListBox;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    cxButton1: TcxButton;
    cxTabSheet3: TcxTabSheet;
    lo: TGroupBox;
    Label15: TLabel;
    TIPO_CONTRATO_SAT: TZetaDBKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    //procedure btSeleccionarConfidenClick(Sender: TObject);
    procedure rbConfidenTodasClick(Sender: TObject);
    procedure rbConfidenAlgunasClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
    procedure btSeleccionarConfiden_DevExClick(Sender: TObject);
  private
    { Private declarations }
    lValoresConfidencialidad : TStringList; 
    procedure FillListaConfidencialidad;
    procedure SetListConfidencialidad( sValores : string );
    procedure GetConfidencialidad;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  EditCatContratos_DevEx: TEditCatContratos_DevEx;

implementation

uses DCatalogos,
     ZetaBuscador_DevEx,
     ZetaCommonClasses,
     ZAccesosTress,
     FSeleccionarConfidencialidad_DevEx, dSistema, DCliente, ZetaCommonTools, DTablas;

{$R *.DFM}

procedure TEditCatContratos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_CONT_CONTRATO;
     FirstControl := TB_CODIGO;
     HelpContext:= H60617_Tipo_contrato; 
     lValoresConfidencialidad := TStringList.Create;

end;

procedure TEditCatContratos_DevEx.Connect;
begin
     dmSistema.cdsNivel0.Conectar;

     with dmCatalogos do
     begin
          DataSource.DataSet:= cdsContratos;
     end;


     with dmTablas do
     begin
          cdsTipoContratoSat.Conectar;
          TIPO_CONTRATO_SAT.LookUpDataSet := dmTablas.cdsTipoContratoSat;
     end;

end;

procedure TEditCatContratos_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  FillListaConfidencialidad;
end;

procedure TEditCatContratos_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', 'Contrato', 'TB_CODIGO', dmCatalogos.cdsContratos );
end;

{procedure TEditCatContratos_DevEx.btSeleccionarConfidenClick(Sender: TObject);
var
   selConfi : TSeleccionarConfidencialidad;
   sCM_NIVEL0Nuevo : string;
begin
  inherited;

  selConfi := TSeleccionarConfidencialidad.Create( Self );
  sCM_NIVEL0Nuevo := selConfi.GetConfidencialidad( DataSource.DataSet.FieldByName('TB_NIVEL0').AsString, lValoresConfidencialidad, True, dmCliente.Confidencialidad );

  if (  sCM_NIVEL0Nuevo <>  DataSource.DataSet.FieldByName('TB_NIVEL0').AsString ) then
  begin
     with DataSource.DataSet do
     begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

           FieldByName('TB_NIVEL0').AsString  := sCM_NIVEL0Nuevo;
           GetConfidencialidad;
     end;
  end;

  FreeAndNil ( selConfi ) ;
end; }

procedure TEditCatContratos_DevEx.FillListaConfidencialidad;
begin
     with dmSistema.cdsNivel0 do
     begin
          First;
          lValoresConfidencialidad.Clear;
          lValoresConfidencialidad.NameValueSeparator := '=' ;
          While( not Eof )do
          begin
               lValoresConfidencialidad.Add(FieldByName('TB_CODIGO').AsString +'='+ FieldByName('TB_ELEMENT').AsString);
               Next;
          end;
     end;
end;

procedure TEditCatContratos_DevEx.GetConfidencialidad;
begin
   if (  DataSource.DataSet  <> nil ) then
   with DataSource.DataSet do
   begin
        SetListConfidencialidad(  FieldByName('TB_NIVEL0').AsString );
   end;
end;

procedure TEditCatContratos_DevEx.SetListConfidencialidad(sValores: string);
var
   Lista : TStringList;
   i, j : integer;
begin
    Lista := TStringList.Create;

    sValores := Trim( sValores );
    if StrLleno( sValores ) then
        Lista.CommaText  := sValores;

    listaConfidencialidad.Items.Clear;

    rbConfidenTodas.OnClick := nil;
    rbConfidenAlgunas.OnClick := nil;
    rbConfidenTodas.Checked := False;
    rbConfidenAlgunas.Checked := False;
                                                 
    if lValoresConfidencialidad.Count = 0 then
       FillListaConfidencialidad;

    for i:= 0 to lValoresConfidencialidad.Count - 1 do
    begin
        if ( Lista.Count > 0 ) then
        begin
                j := Lista.IndexOf( Trim( lValoresConfidencialidad.Names[i] )  );
                if ( j >= 0 ) then
                   listaConfidencialidad.Items.Add( lValoresConfidencialidad[i] )
        end
    end;

    rbConfidenTodas.Checked :=   listaConfidencialidad.Items.Count = 0;
    rbConfidenAlgunas.Checked := not rbConfidenTodas.Checked;

    if ( gbConfidencialidad.Enabled ) then
    begin
        listaConfidencialidad.Enabled := rbConfidenAlgunas.Checked;
        btSeleccionarConfiden_DevEx.Enabled := rbConfidenAlgunas.Checked;
    end;

    rbConfidenTodas.OnClick := rbConfidenTodasClick;
    rbConfidenAlgunas.OnClick := rbConfidenAlgunasClick;

    FreeAndNil( Lista );

end;

procedure TEditCatContratos_DevEx.rbConfidenTodasClick(Sender: TObject);
begin
  inherited;

   with DataSource.DataSet do
   begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

        FieldByName('TB_NIVEL0').AsString  :=  '';
   end;
   GetConfidencialidad;
end;

procedure TEditCatContratos_DevEx.rbConfidenAlgunasClick(Sender: TObject);
begin
  inherited;
     if ( rbConfidenAlgunas.Checked ) then
     begin
        btSeleccionarConfiden_DevExClick( Sender );
        GetConfidencialidad;
     end;

end;

procedure TEditCatContratos_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
     if ( Field = nil ) then
     begin
          with DataSource.DataSet do
          begin
               if ( State in [dsBrowse] ) then
                  GetConfidencialidad
               else if (State in [dsInsert]) then
                    rbConfidenTodas.Checked := TRUE;
          end;

     end;
end;

procedure TEditCatContratos_DevEx.btSeleccionarConfiden_DevExClick(
  Sender: TObject);
var
   selConfi : TSeleccionarConfidencialidad_DevEx;
   sCM_NIVEL0Nuevo : string;
begin
  inherited;

  selConfi := TSeleccionarConfidencialidad_DevEx.Create( Self );
  sCM_NIVEL0Nuevo := selConfi.GetConfidencialidad( DataSource.DataSet.FieldByName('TB_NIVEL0').AsString, lValoresConfidencialidad, True, dmCliente.Confidencialidad );

  if (  sCM_NIVEL0Nuevo <>  DataSource.DataSet.FieldByName('TB_NIVEL0').AsString ) then
  begin
     with DataSource.DataSet do
     begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

           FieldByName('TB_NIVEL0').AsString  := sCM_NIVEL0Nuevo;
           GetConfidencialidad;
     end;
  end;

  FreeAndNil ( selConfi ) ;
end;

end.
