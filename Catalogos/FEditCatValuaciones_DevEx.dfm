inherited EditCatValuaciones_DevEx: TEditCatValuaciones_DevEx
  Left = 495
  Top = 257
  Width = 475
  Height = 543
  BorderIcons = [biSystemMenu, biMaximize]
  BorderStyle = bsSizeable
  Caption = 'Valuaci'#243'n de Puesto'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 469
    Width = 459
    TabOrder = 4
    inherited OK_DevEx: TcxButton
      Left = 295
      TabOrder = 2
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 374
      Cancel = True
      TabOrder = 3
    end
    object Anterior: TcxButton
      Left = 6
      Top = 6
      Width = 80
      Height = 26
      Hint = 'Anterior'
      Anchors = [akLeft, akBottom]
      Caption = '&Anterior'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = AnteriorClick
      OptionsImage.Glyph.Data = {
        36090000424D3609000000000000360000002800000018000000180000000100
        20000000000000090000000000000000000000000000000000008B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFA69FA7FFB1ABB2FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFFB1ABB2FFFAF9FAFFB7B0B7FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFFB7B0B7FFFDFDFDFFFFFFFFFFB7B0B7FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFFC0BAC0FFFFFFFFFFFFFFFFFFFFFFFFFFB7B0B7FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFFCCC8CCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7B0B7FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8D83
        8EFFD5D2D6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7B0B7FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF908791FFDBD7
        DBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7B0B7FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF928993FFE4E1E4FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7B0B7FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8D838EFFD9D5D9FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7B0B7FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8D838EFFCECA
        CEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7B0B7FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFFC7C2C7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7B0B7FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFFBEB8BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7B0B7FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFFB1ABB2FFFDFDFDFFFFFFFFFFFFFFFFFFB7B0B7FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFFAAA3ABFFFAF9FAFFFFFFFFFFB7B0B7FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFFA69FA7FFF4F3F4FFB7B0B7FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF9D959EFFACA5ACFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
      OptionsImage.Margin = 1
    end
    object Siguiente: TcxButton
      Left = 90
      Top = 6
      Width = 80
      Height = 26
      Hint = 'Siguiente'
      Anchors = [akLeft, akBottom]
      Caption = '&Siguiente'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = SiguienteClick
      OptionsImage.Glyph.Data = {
        36090000424D3609000000000000360000002800000018000000180000000100
        20000000000000090000000000000000000000000000000000008B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFB5AEB5FFB1ABB2FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFB7B0B7FFFDFDFDFFBCB6
        BDFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFB7B0B7FFFFFFFFFFFFFF
        FFFFC3BEC4FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFB7B0B7FFFFFFFFFFFFFF
        FFFFFFFFFFFFCCC8CCFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFB7B0B7FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFD9D5D9FF8D838EFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFB7B0B7FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFE0DDE0FF908791FF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFB7B0B7FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE4E1E4FF968D97FF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFB7B0B7FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDEBEDFF968D97FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFB7B0B7FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE4E1E4FF908791FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFB7B0B7FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD9D5D9FF908791FF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFB7B0B7FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFD3D0D4FF8D838EFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFB7B0B7FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFCAC6CBFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFB7B0B7FFFFFFFFFFFFFF
        FFFFFFFFFFFFBEB8BFFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFB7B0B7FFFFFFFFFFFDFD
        FDFFB5AEB5FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFB7B0B7FFFAF9FAFFAFA9
        B0FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFAFA9B0FFA69FA7FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
      OptionsImage.Layout = blGlyphRight
      OptionsImage.Margin = 1
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 459
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 133
      inherited textoValorActivo2: TLabel
        Width = 127
      end
    end
  end
  object PCPuntos: TcxPageControl [3]
    Left = 0
    Top = 131
    Width = 459
    Height = 338
    Align = alClient
    TabOrder = 1
    Properties.ActivePage = tsValuacion
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 336
    ClientRectLeft = 2
    ClientRectRight = 457
    ClientRectTop = 2
    object tsPuntos: TcxTabSheet
      Caption = 'tsPuntos'
      TabVisible = False
      object PageControl2: TcxPageControl
        Left = 0
        Top = 217
        Width = 455
        Height = 117
        Align = alClient
        TabOrder = 0
        Properties.ActivePage = TabSheet4
        Properties.CustomButtons.Buttons = <>
        ClientRectBottom = 115
        ClientRectLeft = 2
        ClientRectRight = 453
        ClientRectTop = 28
        object TabSheet3: TcxTabSheet
          Caption = 'Descripci'#243'n de Factor'
          object VF_DESCRIP: TcxMemo
            Left = 0
            Top = 0
            Align = alClient
            Lines.Strings = (
              'VF_DESCRIP')
            Properties.ReadOnly = True
            TabOrder = 0
            Height = 87
            Width = 451
          end
        end
        object TabSheet4: TcxTabSheet
          Caption = 'Descripci'#243'n de Subfactor'
          ImageIndex = 1
          object VS_DESCRIP: TcxMemo
            Left = 0
            Top = 0
            Align = alClient
            Lines.Strings = (
              'VF_DESCRIP')
            Properties.ReadOnly = True
            TabOrder = 0
            Height = 87
            Width = 451
          end
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 455
        Height = 217
        Align = alTop
        TabOrder = 1
        object Label1: TLabel
          Left = 57
          Top = 5
          Width = 25
          Height = 13
          Alignment = taRightJustify
          Caption = 'Folio:'
        end
        object VT_ORDEN: TZetaDBTextBox
          Left = 88
          Top = 3
          Width = 81
          Height = 17
          AutoSize = False
          Caption = 'VT_ORDEN'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'VT_ORDEN'
          DataSource = dsPuntos
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label2: TLabel
          Left = 49
          Top = 26
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Factor:'
        end
        object Label4: TLabel
          Left = 33
          Top = 47
          Width = 49
          Height = 13
          Alignment = taRightJustify
          Caption = 'Subfactor:'
        end
        object Label8: TLabel
          Left = 55
          Top = 69
          Width = 27
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nivel:'
        end
        object Label9: TLabel
          Left = 46
          Top = 94
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = 'Puntos:'
        end
        object Label10: TLabel
          Left = 8
          Top = 119
          Width = 74
          Height = 13
          Alignment = taRightJustify
          Caption = 'Observaciones:'
        end
        object VF_CODIGO: TZetaTextBox
          Left = 88
          Top = 24
          Width = 81
          Height = 17
          AutoSize = False
          Caption = 'VF_CODIGO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object VF_NOMBRE: TZetaTextBox
          Left = 172
          Top = 24
          Width = 266
          Height = 17
          AutoSize = False
          Caption = 'VF_NOMBRE'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object VS_CODIGO: TZetaTextBox
          Left = 88
          Top = 44
          Width = 81
          Height = 17
          AutoSize = False
          Caption = 'VS_CODIGO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object VS_NOMBRE: TZetaDBTextBox
          Left = 172
          Top = 44
          Width = 266
          Height = 17
          AutoSize = False
          Caption = 'VS_NOMBRE'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'VS_NOMBRE'
          DataSource = dsPuntos
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object VT_PUNTOS: TZetaDBTextBox
          Left = 88
          Top = 92
          Width = 81
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          BiDiMode = bdLeftToRight
          Caption = 'VT_PUNTOS'
          ParentBiDiMode = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'VT_PUNTOS'
          DataSource = dsPuntos
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object VT_COMENTA: TcxDBMemo
          Left = 88
          Top = 115
          DataBinding.DataField = 'VT_COMENTA'
          DataBinding.DataSource = dsPuntos
          Properties.ScrollBars = ssVertical
          TabOrder = 1
          Height = 90
          Width = 353
        end
        object VT_CUAL: TZetaDBKeyLookup_DevEx
          Left = 88
          Top = 64
          Width = 353
          Height = 21
          LookupDataset = dmCatalogos.cdsPuntosNivel
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 81
          DataField = 'VT_CUAL'
          DataSource = dsPuntos
        end
      end
    end
    object tsValuacion: TcxTabSheet
      Caption = 'tsValuacion'
      ImageIndex = 1
      TabVisible = False
      OnShow = tsValuacionShow
      object Label11: TLabel
        Left = 48
        Top = 6
        Width = 33
        Height = 13
        Caption = 'Status:'
      end
      object Label14: TLabel
        Left = 148
        Top = 318
        Width = 73
        Height = 13
        Alignment = taRightJustify
        Caption = 'Modificado por:'
      end
      object Label15: TLabel
        Left = 46
        Top = 107
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Puntos:'
      end
      object Label16: TLabel
        Left = 50
        Top = 131
        Width = 32
        Height = 13
        Alignment = taRightJustify
        Caption = 'Grado:'
      end
      object Label20: TLabel
        Left = 8
        Top = 31
        Width = 74
        Height = 13
        Alignment = taRightJustify
        Caption = 'Observaciones:'
      end
      object US_NOMBRE: TZetaDBTextBox
        Left = 223
        Top = 316
        Width = 210
        Height = 17
        AutoSize = False
        Caption = 'US_NOMBRE'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'US_DESCRIP'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object VP_PUNTOS: TZetaTextBox
        Left = 88
        Top = 104
        Width = 75
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object VP_NUM_FIN: TZetaTextBox
        Left = 256
        Top = 104
        Width = 75
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object Label21: TLabel
        Left = 192
        Top = 107
        Width = 59
        Height = 13
        Caption = 'Respuestas:'
      end
      object BtnRecalculo: TcxButton
        Left = 165
        Top = 127
        Width = 21
        Height = 21
        Hint = 'Recalcular Grado'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = BtnRecalculoClick
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A4050000000000000000000000000000000000008B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFD0CCD0FFFDFDFDFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDCD9DDFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF948B95FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFA39BA3FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF9A919AFFFFFFFFFFB8B2B9FFBEB8BFFFFAF9FAFF948B95FFF6F5
          F6FFCCC8CCFFAFA9B0FFFFFFFFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF9A919AFFFFFFFFFFF6F5F6FFF6F5
          F6FFFFFFFFFFEFEDEFFFFFFFFFFFFAF9FAFFF2F1F2FFFFFFFFFFA8A1A9FF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF9A91
          9AFFFFFFFFFFBEB8BFFFCAC6CBFFF8F7F8FFA199A2FFF6F5F6FFD7D4D7FFBEB8
          BFFFFFFFFFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF9A919AFFFFFFFFFFE9E7E9FFEFEDEFFFFFFFFFFFDBD7
          DBFFFDFDFDFFEFEDEFFFE4E1E4FFFFFFFFFFA8A1A9FF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF9A919AFFFFFFFFFFD3D0
          D4FFD7D4D7FFFBFBFBFFBEB8BFFFFAF9FAFFE0DDE0FFCECACEFFFFFFFFFFA8A1
          A9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF9A919AFFFFFFFFFFD9D5D9FFDCD9DDFFFBFBFBFFC2BCC2FFFBFBFBFFE6E3
          E6FFD3D0D4FFFFFFFFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF9A919AFFFFFFFFFFFAF9FAFFF0EFF1FFF0EF
          F1FFF0EFF1FFF0EFF1FFF0EFF1FFFAF9FAFFFFFFFFFFA8A1A9FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF9A919AFFFFFF
          FFFFA49DA5FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFA49DA5FFFFFF
          FFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF9A919AFFFFFFFFFF9B939CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF9A919AFFFFFFFFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF928993FFFDFDFDFFDCD9DDFFC5C0
          C6FFC5C0C6FFC5C0C6FFC5C0C6FFC5C0C6FFD5D2D6FFFFFFFFFF9F97A0FF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFFC3BEC4FFF8F7F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAF9
          FAFFCECACEFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
        OptionsImage.Margin = 1
      end
      object GroupBox1: TcxGroupBox
        Left = 32
        Top = 247
        Caption = ' Per'#237'odo de Valuaci'#243'n '
        TabOrder = 4
        Height = 54
        Width = 401
        object Label12: TLabel
          Left = 20
          Top = 25
          Width = 30
          Height = 13
          Alignment = taRightJustify
          Caption = 'Inicial:'
        end
        object Label13: TLabel
          Left = 195
          Top = 25
          Width = 25
          Height = 13
          Alignment = taRightJustify
          Caption = 'Final:'
        end
        object VP_FEC_INI: TZetaDBFecha
          Left = 56
          Top = 22
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 0
          Text = '21/may/07'
          Valor = 39223.000000000000000000
          DataField = 'VP_FEC_INI'
          DataSource = DataSource
        end
        object VP_FEC_FIN: TZetaDBFecha
          Left = 226
          Top = 22
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 1
          Text = '21/may/07'
          Valor = 39223.000000000000000000
          DataField = 'VP_FEC_FIN'
          DataSource = DataSource
        end
      end
      object VP_GRADO: TZetaDBNumero
        Left = 88
        Top = 127
        Width = 75
        Height = 21
        Mascara = mnDias
        TabOrder = 2
        Text = '0'
        DataField = 'VP_GRADO'
        DataSource = DataSource
      end
      object GroupBox2: TcxGroupBox
        Left = 32
        Top = 155
        Caption = ' Puesto Tipo Mercado'
        TabOrder = 3
        Height = 90
        Width = 401
        object Label17: TLabel
          Left = 14
          Top = 20
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = 'C'#243'digo:'
        end
        object Label18: TLabel
          Left = 10
          Top = 42
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre:'
        end
        object Label19: TLabel
          Left = 18
          Top = 66
          Width = 32
          Height = 13
          Alignment = taRightJustify
          Caption = 'Grado:'
        end
        object VP_PT_NOM: TDBEdit
          Left = 56
          Top = 39
          Width = 275
          Height = 21
          DataField = 'VP_PT_NOM'
          DataSource = DataSource
          TabOrder = 1
        end
        object VP_PT_GRAD: TZetaDBNumero
          Left = 56
          Top = 61
          Width = 75
          Height = 21
          Mascara = mnDias
          TabOrder = 2
          Text = '0'
          DataField = 'VP_PT_GRAD'
          DataSource = DataSource
        end
        object VP_PT_CODE: TDBEdit
          Left = 56
          Top = 16
          Width = 75
          Height = 21
          DataField = 'VP_PT_CODE'
          DataSource = DataSource
          TabOrder = 0
        end
      end
      object VP_STATUS: TZetaDBKeyCombo
        Left = 88
        Top = 0
        Width = 145
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 0
        OnChange = VP_STATUSChange
        ListaFija = lfStatusValuacion
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'VP_STATUS'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object VP_COMENTA: TcxDBMemo
        Left = 88
        Top = 24
        DataBinding.DataField = 'VP_COMENTA'
        DataBinding.DataSource = DataSource
        Properties.ScrollBars = ssVertical
        TabOrder = 1
        Height = 75
        Width = 345
      end
    end
  end
  object PanelValuacion: TPanel [4]
    Left = 0
    Top = 50
    Width = 459
    Height = 81
    Align = alTop
    TabOrder = 2
    object Label6: TLabel
      Left = 43
      Top = 32
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = 'Plantilla:'
    end
    object Label3: TLabel
      Left = 57
      Top = 10
      Width = 25
      Height = 13
      Alignment = taRightJustify
      Caption = 'Folio:'
    end
    object Label5: TLabel
      Left = 46
      Top = 54
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'Puesto:'
    end
    object Label7: TLabel
      Left = 326
      Top = 10
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha:'
    end
    object VP_FOLIO: TZetaDBTextBox
      Left = 88
      Top = 8
      Width = 81
      Height = 17
      AutoSize = False
      Caption = 'VP_FOLIO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'VP_FOLIO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object VL_CODIGO: TZetaDBTextBox
      Left = 88
      Top = 30
      Width = 81
      Height = 17
      AutoSize = False
      Caption = 'VL_CODIGO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'VL_CODIGO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object PU_CODIGO: TZetaDBTextBox
      Left = 88
      Top = 52
      Width = 81
      Height = 17
      AutoSize = False
      Caption = 'PU_CODIGO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'PU_CODIGO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object VL_NOMBRE: TZetaDBTextBox
      Left = 172
      Top = 32
      Width = 266
      Height = 17
      AutoSize = False
      Caption = 'VL_NOMBRE'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'VL_NOMBRE'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object PU_DESCRIP: TZetaDBTextBox
      Left = 172
      Top = 52
      Width = 266
      Height = 17
      AutoSize = False
      Caption = 'PU_DESCRIP'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'PU_DESCRIP'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object VP_FECHA: TZetaDBTextBox
      Left = 363
      Top = 8
      Width = 75
      Height = 17
      AutoSize = False
      Caption = 'VP_FECHA'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'VP_FECHA'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
  end
  inherited DataSource: TDataSource
    Left = 396
    Top = 1
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_AgregarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_BorrarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_ExportarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarControlContainerItem_DBNavigator: TdxBarControlContainerItem
      Visible = ivInCustomizing
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  object dsPuntos: TDataSource
    OnDataChange = dsPuntosDataChange
    Left = 424
  end
end
