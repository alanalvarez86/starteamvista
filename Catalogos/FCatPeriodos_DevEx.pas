unit FCatPeriodos_devEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Grids, DBGrids, Db, StdCtrls, ExtCtrls, Buttons, DBCtrls,
     ZBaseConsulta,
     ZetaDBGrid,
     ZetaDBTextBox,
     ZetaKeyCombo, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, Menus, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxButtons, ActnList, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, System.Actions,
  cxTextEdit;

type
  TCatPeriodos_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    ReCalculaBtn: TcxButton;
    PeriodoTipoKCB: TZetaKeyCombo;
    Label2: TLabel;
    PE_MES_FON: TcxGridDBColumn;
    PE_MES: TcxGridDBColumn;
    PE_TIMBRO: TcxGridDBColumn;
    procedure PeriodoTipoKCBChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ReCalculaBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Agregar;override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure Connect; override;
    procedure Disconnect; override;
    procedure Refresh; override;
  public
    { Public declarations }
  end;

var
  CatPeriodos_DevEx: TCatPeriodos_DevEx;

implementation

uses DCliente,
     DCatalogos,
     DProcesos,
     DNomina,
     DGlobal,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZGlobalTress,
     ZetaCommonTools;

{$R *.DFM}

{ TCatPeriodos }

//acl Para proteger cuando no exista el enumerado.

procedure TCatPeriodos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     PeriodoTipoKCB.Enabled := dmCliente.GetDatosPeriodoActivo.Tipo > K_PERIODO_VACIO;
     PeriodoTipoKCB.LlaveEntero:= Ord( dmCliente.GetDatosPeriodoActivo.Tipo );
    // PeriodoTipoKCB.ItemIndex := Ord( dmCliente.PeriodoTipo );      //acl Para proteger cuando no exista el enumerado
     TipoValorActivo1 := stSistema;
     HelpContext := H60623_Periodos_nomina;
     {$ifdef PRESUPUESTOS}
              PE_TIMBRO.Visible := False;
     {$endif}
end;

procedure TCatPeriodos_DevEx.Connect;
begin
     with dmCatalogos do
     begin
//          FormaTipoPeriodo := eTipoPeriodo( PeriodoTipoKCB.LlaveEntero );
          with cdsPeriodo do
          begin
               if not Active then
                  GetDatosPeriodo( dmCliente.YearDefault, eTipoPeriodo ( PeriodoTipoKCB.LlaveEntero ) )//acl
               else
               begin
                    if ( PeriodoTipoKCB.LlaveEntero <> FieldByName( 'PE_TIPO' ).AsInteger ) or //acl
                       ( dmCliente.YearDefault <> FieldByName( 'PE_YEAR' ).AsInteger ) then
                    begin
                         GetDatosPeriodo( dmCliente.YearDefault, eTipoPeriodo( PeriodoTipoKCB.LlaveEntero ) ); //acl
                    end;
               end;
          end;
          DataSource.DataSet := cdsPeriodo;
          FormaCatPeriodos := True;
     end;
end;

procedure TCatPeriodos_DevEx.Disconnect;
begin
     inherited;
     dmCatalogos.FormaCatPeriodos := FALSE;
end;

procedure TCatPeriodos_DevEx.Refresh;
begin
     {***(@am): Este cambio es temporal, mientras se define como realizar el ordenamiento despues de Refrescar en formas especiales*** }
     if  dmCatalogos.cdsPeriodo.IndexName <> '' then
         dmCatalogos.cdsPeriodo.IndexName := '';
     {***}
     dmCatalogos.GetDatosPeriodo( dmCliente.YearDefault, eTipoPeriodo( PeriodoTipoKCB.LlaveEntero ) ); //acl
     DataSource.DataSet := dmCatalogos.cdsPeriodo;  //WA para forzar que lea el # de Records y aparezca el scrollbar
end;

procedure TCatPeriodos_DevEx.Modificar;
begin
     dmCatalogos.cdsPeriodo.Modificar;
end;

procedure TCatPeriodos_DevEx.Agregar;
begin
     dmCatalogos.cdsPeriodo.Agregar;
end;

procedure TCatPeriodos_DevEx.Borrar;
{var
   oCursor: TCursor;
   iNumero: Integer; }
begin
     dmCatalogos.cdsPeriodo.Borrar;
{     oCursor := Screen.Cursor;
     Screen.Cursor := oCursor;
     try
        iNumero := dmCatalogos.cdsPeriodo.FieldByName( 'PE_NUMERO' ).AsInteger;
        dmNomina.BorrarNominaActiva( False );
        dmCatalogos.cdsPeriodo.Locate( 'PE_NUMERO', iNumero, [] );  //Si no se borro intenta reposicionarlo
     finally
            Screen.Cursor := oCursor;
     end; }
end;

procedure TCatPeriodos_DevEx.PeriodoTipoKCBChange(Sender: TObject);
begin
     inherited;
     dmCatalogos.FormaTipoPeriodo := eTipoPeriodo( PeriodoTipoKCB.LlaveEntero ); //acl
     Refresh;
end;

procedure TCatPeriodos_DevEx.ReCalculaBtnClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := oCursor;
     try
        dmProcesos.RecalculoDias( PeriodoTipoKCB.LlaveEntero );   //acl
     finally
            Screen.Cursor := oCursor;
     end;
     Refresh;
end;

procedure TCatPeriodos_DevEx.FormShow(Sender: TObject);
var lVisible : Boolean;
begin
     CreaColumaSumatoria(ZetaDbGridDBtableView.Columns[0],0 , '' , SkCount );
     inherited;
     with PeriodoTipoKCB do
     begin
          ListaFija:=lfTipoPeriodoConfidencial; //acl
          LlaveEntero:= Ord( dmCliente.GetDatosPeriodoActivo.Tipo );
          dmCatalogos.FormaTipoPeriodo := eTipoPeriodo( PeriodoTipoKCB.LlaveEntero );
     end;
     Self.Refresh;
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
     ZetaDBGridDBTableView.OptionsView.GroupByBox := true;

     lVisible := False;

     if Global.GetGlobalString (K_GLOBAL_FONACOT_PRESTAMO) <> VACIO then
     begin
          if Global.GetGlobalBooleano (K_GLOBAL_FONACOT_PRESTAMO_CONCILIA) then
             lVisible := True;
     end;

     PE_MES_FON.Visible := lVisible;
end;

end.
