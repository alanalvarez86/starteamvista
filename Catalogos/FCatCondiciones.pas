unit FCatCondiciones;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Grids, DBGrids, Db, ExtCtrls,
     ZBaseConsulta,
     ZetaDBGrid;

type
  TCatCondiciones = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  CatCondiciones: TCatCondiciones;

implementation

{$R *.DFM}

uses DCatalogos,
     ZetaBuscador,
     ZetaCommonClasses;

{ TCatCondiciones }

{En seleccion y visitantes no existen los campos QU_NAVEGA,QU_ORDEN,QU_CANDADO
 tomar en cuenta para futuras programaciones sobre esta forma}

procedure TCatCondiciones.FormCreate(Sender: TObject);
const
     K_COLUMNA_ORDEN = 3;
     K_COLUMNA_NAVEGA = 2;
begin
     inherited;
     CanLookup := True;
     {$ifdef VISITANTES}
     HelpContext := H_VISMGR_CONS_TCONDICION;
     {$else}
     HelpContext := H60634_Condiciones;
     {$endif}
     {$ifndef TRESS}
     with ZetaDBGrid1 do
     begin
          Columns[K_COLUMNA_ORDEN].Visible:= FALSE;
          Columns[K_COLUMNA_NAVEGA].Visible:= FALSE;
     end;
     {$endif}
end;

procedure TCatCondiciones.Connect;
begin
     with dmCatalogos do
     begin
          cdsCondiciones.Conectar;
          DataSource.DataSet := cdsCondiciones;
     end;
end;

procedure TCatCondiciones.Refresh;
begin
     dmCatalogos.cdsCondiciones.Refrescar;
end;

procedure TCatCondiciones.Agregar;
begin
     dmCatalogos.cdsCondiciones.Agregar;
end;

procedure TCatCondiciones.Borrar;
begin
     dmCatalogos.cdsCondiciones.Borrar;
end;

procedure TCatCondiciones.Modificar;
begin
     dmCatalogos.cdsCondiciones.Modificar;
end;

procedure TCatCondiciones.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Condición', 'Condición', 'QU_CODIGO', dmCatalogos.cdsCondiciones );
end;

end.

