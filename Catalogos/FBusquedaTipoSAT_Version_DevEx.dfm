inherited BusquedaTipoSAT_Version_DevEx: TBusquedaTipoSAT_Version_DevEx
  Left = 656
  Top = 352
  Caption = 'BusquedaTipoSAT_Version_DevEx'
  ClientWidth = 664
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 664
    inherited Cancelar_DevEx: TcxButton
      Left = 585
    end
    inherited OK_DevEx: TcxButton
      Left = 505
    end
  end
  inherited PanelSuperior: TPanel
    Width = 664
    inherited MostrarActivos: TCheckBox
      Left = 508
    end
  end
  inherited DBGrid_DevEx: TZetaCXGrid
    Width = 664
    inherited DBGrid_DevExDBTableView: TcxGridDBTableView
      object DBGrid_DevExDBTableViewColumn1: TcxGridDBColumn
        Caption = 'N'#250'mero SAT'
        DataBinding.FieldName = 'TB_SAT_NUM'
        Width = 77
      end
    end
  end
  inherited cxStyleRepository1: TcxStyleRepository
    PixelsPerInch = 96
  end
end
