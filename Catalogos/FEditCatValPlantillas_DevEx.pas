unit FEditCatValPlantillas_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicion_DevEx, DB, ExtCtrls, DBCtrls, 
  StdCtrls, ZetaKeyCombo, ZetaKeyLookup_DevEx, Mask, ZetaNumero,
  ZetaCommonLists, ZetaFecha, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, cxContainer, cxEdit, cxTextEdit, cxMemo, cxDBEdit, cxGroupBox;

type
  TEditCatValPlantillas_DevEx = class(TBaseEdicion_DevEx)
    VL_CODIGO: TZetaDBNumero;
    VL_NOMBRE: TDBEdit;
    VL_TAB_PTS: TZetaDBKeyLookup_DevEx;
    GroupBox1: TcxGroupBox;
    VL_COMENTA: TcxDBMemo;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    VL_STATUS: TZetaDBKeyCombo;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    VL_NUM_PTO: TZetaDBNumero;
    VL_NUM_FAC: TZetaDBNumero;
    VL_NUM_SUB: TZetaDBNumero;
    VL_MIN_PTS: TZetaDBNumero;
    VL_MAX_PTS: TZetaDBNumero;
    VL_FECHA: TZetaDBFecha;
    Label12: TLabel;
    Label13: TLabel;
    VL_INGLES: TDBEdit;
    Label14: TLabel;
    Label15: TLabel;
    VL_TEXTO: TDBEdit;
    VL_NUMERO: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
    procedure VL_STATUSChange(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  EditCatValPlantillas_DevEx: TEditCatValPlantillas_DevEx;

implementation

uses dCatalogos,
     dTablas,
     ZetaCommonClasses,
     ZAccesosTress,
     ZetaDialogo,
     ZetaBuscador_DevEx;

{$R *.dfm}

procedure TEditCatValPlantillas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_VAL_PLANTILLAS;
     FirstControl := VL_NOMBRE;
     HelpContext:= H_VALUACION_PLANTILLA;
end;

procedure TEditCatValPlantillas_DevEx.Connect;
begin
     dmTablas.cdsNumericas.Conectar;
     DataSource.DataSet:= dmCatalogos.cdsValPlantilla;
end;

procedure TEditCatValPlantillas_DevEx.VL_STATUSChange(Sender: TObject);
begin
     if ( eStatusPlantilla( VL_STATUS.ItemIndex ) = spDiseno ) and
        ( VL_NUM_PTO.Valor > 0 ) then
     begin
          VL_STATUS.ItemIndex := DataSource.DataSet.FieldByName( 'VL_STATUS' ).AsInteger;
          ZetaDialogo.ZError('Error', 'No se puede poner en dise�o, porque ya existen valuaciones hechas ', 0);
     end;
end;

procedure TEditCatValPlantillas_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Plantillas de Valulaci�n', Self.Caption, 'VL_CODIGO', dmCatalogos.cdsValPlantilla );
end;

end.
