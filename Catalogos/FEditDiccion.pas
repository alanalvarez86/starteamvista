unit FEditDiccion;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, Db, ExtCtrls,
  Buttons, DBCtrls, StdCtrls, Mask, ZetaKeyCombo, ZetaNumero,
  ZetaSmartLists;

type
  TEditDiccion = class(TBaseEdicion)
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    DI_CLASIFI: TZetaDBKeyCombo;
    DI_NOMBRE: TDBEdit;
    DI_TITULO: TDBEdit;
    DI_MASCARA: TDBEdit;
    DI_ANCHO: TZetaDBNumero;
    Label2: TLabel;
    DI_CLAVES: TDBEdit;
    Label5: TLabel;
    DI_TCORTO: TDBEdit;
    CBConfidencial: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
  private
    procedure LlenaListaEntidad;
  protected
    procedure Connect;override;
  public
    function PuedeAgregar( var sMensaje: String ): Boolean;override;
    function PuedeBorrar( var sMensaje: String ): Boolean;override;
  end;

var
  EditDiccion: TEditDiccion;

implementation

uses DDiccionario,
     ZetaCommonClasses,
     ZAccesosTress,
     ZetaTipoEntidad;

{$R *.DFM}

procedure TEditDiccion.LlenaListaEntidad;
 var i:TipoEntidad;
begin
     with TStringList(DI_CLASIFI.Lista) do
     begin
          BeginUpdate;
          try
             Clear;
             for i:= Low(TipoEntidad) to High(TipoEntidad) do
                 Add( Format( '%d=%s', [ ord(i), aTipoEntidad[i] ] ) );
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TEditDiccion.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := ZAccesosTress.D_CAT_CONFI_DICCIONARIO;
     FirstControl :=  DI_CLASIFI;
     LlenaListaEntidad;
     {$ifdef VISITANTES}
     HelpContext := H_VISMGR_EDIT_DICCION;
     {$ELSE}
     HelpContext:= H60653_Diccionario_datos;
     {$endif}      
end;

procedure TEditDiccion.Connect;
begin
     with dmDiccionario do
     begin
          ConectaDiccion;
          DataSource.DataSet := cdsDiccion;
     end;
end;

function TEditDiccion.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result:= False;
     sMensaje := 'No Se Puede Agregar Al Diccionario De Datos';
end;

function TEditDiccion.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result:= False;
     sMensaje := 'No Se Puede Borrar En El Diccionario De Datos';
end;

end.
