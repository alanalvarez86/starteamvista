unit FEditMatrizCertificaPuesto_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, ZetaDBTextBox, StdCtrls, Mask, ZetaNumero, DBCtrls,
  Db, ExtCtrls, FCatCerNivel_DevEx,
  Buttons, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, ZetaKeyLookup_DevEx;

type
  TEditMatrizCertificaPuesto_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    Label2: TLabel;
    ZPuesto: TZetaTextBox;
    Label1: TLabel;
    CI_CODIGO: TZetaDBKeyLookup_DevEx;
    Label3: TLabel;
    EN_DIAS: TZetaDBNumero;
    Label4: TLabel;
    PC_OPCIONA: TDBCheckBox;
    gbProgramacion: TGroupBox;
    gbProgramarA: TDBRadioGroup;
    bModifica_DevEx: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure gbProgramarAChange(Sender: TObject);
    procedure bModifica_DevExClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure SetEditarSoloActivos;
  private
  protected
    procedure Connect; override;
  public
  end;

var
  EditMatrizCertificaPuesto_DevEx: TEditMatrizCertificaPuesto_DevEx;

implementation

uses FTressShell,ZetaCommonClasses, dCatalogos, dGlobal, ZBaseDlgModal_DevEx,
     ZAccesosTress, ZGlobalTress, ZetaClientTools;

{$R *.DFM}

{ TEditMatrizPuesto }

procedure TEditMatrizCertificaPuesto_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_CAPA_MATRIZ_CERT_PUESTO;
     HelpContext := H_Matriz_Edt_CertificaPuesto;
     FirstControl := CI_CODIGO;
     CI_CODIGO.LookupDataset := dmCatalogos.cdsCertificaciones;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TEditMatrizCertificaPuesto_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     ZPuesto.Caption := Datasource.DataSet.FieldByName( 'PU_CODIGO' ).AsString + ' = ' + dmCatalogos.cdsPuestos.FieldByName( 'PU_DESCRIP' ).AsString;
     gbProgramacion.Visible := Global.GetGlobalInteger( K_NIVEL_PROGRAMACION_CERTIFIC ) > 0;
     if gbProgramacion.Visible then
     begin
          gbProgramacion.Caption := 'Programación por ' + Global.GetDescNivelProg(K_NIVEL_PROGRAMACION_CERTIFIC);
          Height :=  GetScaledHeight( 325 );//+40 Se aumentó la altura debido a los Large Fonts;
     end
     else
          Height :=  GetScaledHeight( 218 );//+40 Se aumentó la altura debido a los Large Fonts; //DevEx (by am): Se aumenta width por nueva imagen
end;

procedure TEditMatrizCertificaPuesto_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsCertificaciones.Conectar;
          cdsMatrizCertificPuesto.Conectar;
          Datasource.Dataset := cdsMatrizCertificPuesto;
     end;
end;

procedure TEditMatrizCertificaPuesto_DevEx.gbProgramarAChange(Sender: TObject);
begin
     inherited;
     bModifica_DevEx.Enabled := ( gbProgramarA.Value = 'S' );
end;

procedure TEditMatrizCertificaPuesto_DevEx.bModifica_DevExClick(
  Sender: TObject);
begin
  inherited;
     ShowDlgModal( CatCerNivel_DevEx, TCatCerNivel_DevEx );

end;

procedure TEditMatrizCertificaPuesto_DevEx.OK_DevExClick(Sender: TObject);
begin
  if ( gbProgramarA.Value = 'N' ) then
        with dmCatalogos.cdsCerNivel do
        begin
             First;
             while not EOF do
                   Delete;
        end;
     inherited;

end;

procedure TEditMatrizCertificaPuesto_DevEx.SetEditarSoloActivos;
begin
     CI_CODIGO.EditarSoloActivos := TRUE;
end;

end.



