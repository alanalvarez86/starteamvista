inherited CatTiposPoliza_DevEx: TCatTiposPoliza_DevEx
  Left = 188
  Top = 353
  Caption = 'Tipos de P'#243'liza'
  ClientWidth = 779
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 779
    inherited ValorActivo2: TPanel
      Width = 520
      inherited textoValorActivo2: TLabel
        Width = 514
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 779
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      PopUpMenu = PopupMenu1
      DataController.DataSource = DataSource
      object PT_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'PT_CODIGO'
        MinWidth = 75
        Width = 75
      end
      object PT_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PT_NOMBRE'
        MinWidth = 100
        Width = 100
      end
      object PT_INGLES: TcxGridDBColumn
        Caption = 'Ingl'#233's'
        DataBinding.FieldName = 'PT_INGLES'
        MinWidth = 75
      end
      object PT_REPORTE: TcxGridDBColumn
        Caption = 'Reporte'
        DataBinding.FieldName = 'PT_REPORTE'
        MinWidth = 75
        Width = 86
      end
      object PT_NUMERO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'PT_NUMERO'
        MinWidth = 75
      end
      object PT_TEXTO: TcxGridDBColumn
        Caption = 'Texto'
        DataBinding.FieldName = 'PT_TEXTO'
        MinWidth = 75
        Width = 75
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 576
    Top = 224
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end