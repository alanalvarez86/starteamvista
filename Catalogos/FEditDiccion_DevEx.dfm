inherited EditDiccion_DevEx: TEditDiccion_DevEx
  Left = 312
  Top = 131
  Caption = 'Diccionario de Datos'
  ClientHeight = 245
  ClientWidth = 398
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 73
    Top = 40
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tabla:'
  end
  object Label3: TLabel [1]
    Left = 10
    Top = 62
    Width = 93
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nombre del Campo:'
  end
  object Label4: TLabel [2]
    Left = 44
    Top = 84
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descripci'#243'n:'
  end
  object Label7: TLabel [3]
    Left = 69
    Top = 150
    Width = 34
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ancho:'
  end
  object Label8: TLabel [4]
    Left = 59
    Top = 172
    Width = 44
    Height = 13
    Alignment = taRightJustify
    Caption = 'M'#225'scara:'
  end
  object Label2: TLabel [5]
    Left = 24
    Top = 130
    Width = 79
    Height = 13
    Alignment = taRightJustify
    Caption = 'Palabras Claves:'
  end
  object Label5: TLabel [6]
    Left = 44
    Top = 106
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'T'#237'tulo Corto:'
  end
  inherited PanelBotones: TPanel
    Top = 209
    Width = 398
    TabOrder = 10
    inherited OK_DevEx: TcxButton
      Left = 235
      Top = 4
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 314
      Top = 4
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 398
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 72
    end
  end
  object DI_CLASIFI: TZetaDBKeyCombo [9]
    Left = 111
    Top = 36
    Width = 260
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    Enabled = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 1
    ListaFija = lfNinguna
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'DI_CLASIFI'
    DataSource = DataSource
    LlaveNumerica = True
    ReadOnly = True
  end
  object DI_NOMBRE: TDBEdit [10]
    Left = 111
    Top = 58
    Width = 118
    Height = 21
    CharCase = ecUpperCase
    DataField = 'DI_NOMBRE'
    DataSource = DataSource
    Enabled = False
    ReadOnly = True
    TabOrder = 2
  end
  object DI_TITULO: TDBEdit [11]
    Left = 111
    Top = 80
    Width = 257
    Height = 21
    DataField = 'DI_TITULO'
    DataSource = DataSource
    MaxLength = 30
    TabOrder = 3
  end
  object DI_MASCARA: TDBEdit [12]
    Left = 111
    Top = 168
    Width = 121
    Height = 21
    DataField = 'DI_MASCARA'
    DataSource = DataSource
    TabOrder = 7
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 14
  end
  object DI_ANCHO: TZetaDBNumero [14]
    Left = 111
    Top = 146
    Width = 40
    Height = 21
    Mascara = mnDias
    TabOrder = 6
    Text = '0'
    DataField = 'DI_ANCHO'
    DataSource = DataSource
  end
  object DI_CLAVES: TDBEdit [15]
    Left = 111
    Top = 124
    Width = 257
    Height = 21
    CharCase = ecUpperCase
    DataField = 'DI_CLAVES'
    DataSource = DataSource
    MaxLength = 40
    TabOrder = 5
  end
  object DI_TCORTO: TDBEdit [16]
    Left = 111
    Top = 102
    Width = 257
    Height = 21
    DataField = 'DI_TCORTO'
    DataSource = DataSource
    MaxLength = 40
    TabOrder = 4
  end
  object CBConfidencial: TDBCheckBox [17]
    Left = 40
    Top = 190
    Width = 84
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Confidencial'
    DataField = 'DI_CONFI'
    DataSource = DataSource
    TabOrder = 8
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  inherited DataSource: TDataSource
    Top = 74
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
