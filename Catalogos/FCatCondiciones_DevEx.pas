unit FCatCondiciones_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Grids, DBGrids, Db, ExtCtrls,
     ZBaseConsulta,
     ZetaDBGrid, StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, System.Actions;

type
  TCatCondiciones_DevEx = class(TBaseGridLectura_DevEx)
    QU_CODIGO: TcxGridDBColumn;
    QU_DESCRIP: TcxGridDBColumn;
    QU_NAVEGA: TcxGridDBColumn;
    QU_ORDEN: TcxGridDBColumn;
    US_CODIGO: TcxGridDBColumn;
    QU_NIVEL: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  CatCondiciones_DevEx: TCatCondiciones_DevEx;

implementation

{$R *.DFM}

uses DCatalogos,
     ZetaBuscador_DevEx,
     ZetaCommonClasses;

{ TCatCondiciones }

{En seleccion y visitantes no existen los campos QU_NAVEGA,QU_ORDEN,QU_CANDADO
 tomar en cuenta para futuras programaciones sobre esta forma}

procedure TCatCondiciones_DevEx.FormCreate(Sender: TObject);
const
     K_COLUMNA_ORDEN = 3;
     K_COLUMNA_NAVEGA = 2;
begin
     inherited;

     //Desactivar gridmode (FormCreate)
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= FALSE;

     CanLookup := True;
     {$ifdef VISITANTES}
     HelpContext := H_VISMGR_CONS_TCONDICION;
     {$else}
     HelpContext := H60634_Condiciones;
     {$endif}
     {$ifndef TRESS}
     with ZetaDBGridDBTableView do
     begin
          Columns[K_COLUMNA_ORDEN].Visible:= FALSE;
          Columns[K_COLUMNA_NAVEGA].Visible:= FALSE;
     end;
     {$endif}

end;

procedure TCatCondiciones_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsCondiciones.Conectar;
          DataSource.DataSet := cdsCondiciones;
     end;
end;

procedure TCatCondiciones_DevEx.Refresh;
begin
     dmCatalogos.cdsCondiciones.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatCondiciones_DevEx.Agregar;
begin
     dmCatalogos.cdsCondiciones.Agregar;
end;

procedure TCatCondiciones_DevEx.Borrar;
begin
     dmCatalogos.cdsCondiciones.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatCondiciones_DevEx.Modificar;
begin
     dmCatalogos.cdsCondiciones.Modificar;
end;

procedure TCatCondiciones_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Condición', 'Condición', 'QU_CODIGO', dmCatalogos.cdsCondiciones );
end;

procedure TCatCondiciones_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     Refresh;

    //columna sumatoria: cuantos
   CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('QU_CODIGO'), K_SIN_TIPO , '', skCount);
   ZetaDBGridDBTableView.ApplyBestFit();


   //agrupar
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := FALSE;
  //ver la caja de agrupamiento
  ZetaDBGridDBTableView.OptionsView.GroupByBox := FALSE;

end;

end.

