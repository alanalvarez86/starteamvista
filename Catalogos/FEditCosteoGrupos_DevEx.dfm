inherited EditCosteoGrupos_DevEx: TEditCosteoGrupos_DevEx
  Left = 173
  Top = 119
  Caption = 'Grupos de Costeo'
  ClientHeight = 132
  ClientWidth = 419
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel [0]
    Left = 64
    Top = 43
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = '&C'#243'digo:'
    FocusControl = GpoCostoCodigo
  end
  object Label1: TLabel [1]
    Left = 11
    Top = 67
    Width = 89
    Height = 13
    Alignment = taRightJustify
    Caption = '&Nombre del Grupo:'
    FocusControl = GpoCostoNombre
  end
  inherited PanelBotones: TPanel
    Top = 96
    Width = 419
    TabOrder = 3
    inherited OK_DevEx: TcxButton
      Left = 256
      Top = 4
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 335
      Top = 4
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 419
    TabOrder = 5
    inherited ValorActivo2: TPanel
      Width = 93
      inherited textoValorActivo2: TLabel
        Width = 87
      end
    end
  end
  object GpoCostoNombre: TDBEdit [5]
    Left = 104
    Top = 64
    Width = 257
    Height = 21
    Hint = 'Nombre de Grupo de Costeo'
    DataField = 'GpoCostoNombre'
    DataSource = DataSource
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
  end
  object GpoCostoCodigo: TZetaDBEdit [6]
    Left = 104
    Top = 40
    Width = 97
    Height = 21
    Hint = 'C'#243'digo de Grupo de Costeo'
    CharCase = ecUpperCase
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    DataField = 'GpoCostoCodigo'
    DataSource = DataSource
  end
  object TB_ACTIVO: TDBCheckBox [7]
    Left = 306
    Top = 41
    Width = 53
    Height = 17
    Hint = #191'Est'#225' Activo?'
    Alignment = taLeftJustify
    Caption = '&Activo:'
    DataField = 'GpoCostoActivo'
    DataSource = DataSource
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  inherited DataSource: TDataSource
    Left = 396
    Top = 1
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    DesignInfo = 3014734
  end
end
