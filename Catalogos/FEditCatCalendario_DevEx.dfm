inherited EditCatCalendario_DevEx: TEditCatCalendario_DevEx
  Left = 242
  Top = 171
  Caption = 'Calendario de Cursos'
  ClientHeight = 133
  ClientWidth = 397
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 97
    Width = 397
    inherited OK_DevEx: TcxButton
      Left = 229
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 308
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 397
    TabOrder = 2
    inherited ValorActivo2: TPanel
      Width = 71
      inherited textoValorActivo2: TLabel
        Width = 65
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 4
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 50
    Width = 397
    Height = 47
    Align = alClient
    BevelInner = bvRaised
    BevelOuter = bvNone
    TabOrder = 7
    object Label1: TLabel
      Left = 29
      Top = 9
      Width = 30
      Height = 13
      Caption = 'Curso:'
    end
    object Label2: TLabel
      Left = 26
      Top = 29
      Width = 33
      Height = 13
      Caption = 'Fecha:'
    end
    object CU_CODIGO: TZetaDBKeyLookup_DevEx
      Left = 72
      Top = 5
      Width = 300
      Height = 21
      LookupDataset = dmCatalogos.cdsCursos
      Opcional = False
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      DataField = 'CU_CODIGO'
      DataSource = DataSource
    end
    object CC_FECHA: TZetaDBFecha
      Left = 72
      Top = 28
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 1
      Text = '20/dic/97'
      UseEnterKey = True
      Valor = 35784.000000000000000000
      DataField = 'CC_FECHA'
      DataSource = DataSource
    end
  end
  inherited DataSource: TDataSource
    Left = 388
    Top = 57
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 7340176
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    Left = 360
    Top = 80
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    DesignInfo = 5243272
  end
end
