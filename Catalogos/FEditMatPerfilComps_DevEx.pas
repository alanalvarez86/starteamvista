unit FEditMatPerfilComps_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls,
  Buttons, DBCtrls, StdCtrls, Mask, ZetaNumero, 
  ZetaDBTextBox,
  zBaseEdicion_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  cxControls, dxSkinsdxBarPainter, ZetaKeyLookup_DevEx,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, TressMorado2013, dxSkinsDefaultPainters;

type
  TEditMatPerComps_DevEx = class(TBaseEdicion_DevEx)
    CC_CODIGO: TZetaDBKeyLookup_DevEx;
    Label2: TLabel;
    Label1: TLabel;
    ZPerfil: TZetaTextBox;
    PC_PESO: TZetaDBNumero;
    NC_NIVEL: TZetaDBKeyLookup_DevEx;
    Label3: TLabel;
    Label4: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CC_CODIGOValidKey(Sender: TObject);
  protected
    procedure Connect; override;
  public
  end;

var
  EditMatPerComps_DevEx: TEditMatPerComps_DevEx;

implementation

uses FTressShell, ZetaCommonClasses, dCatalogos, dGlobal, ZGlobalTress,
     ZAccesosTress, ZetaClientTools,ZetaCommonTools;

{$R *.DFM}

procedure TEditMatPerComps_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_MAT_PERFILES_COMP;
     HelpContext:= H_MatrizFunciones;
     FirstControl := CC_CODIGO;
     CC_CODIGO.LookupDataset := dmCatalogos.cdsCompetencias;
     NC_NIVEL.LookupDataset := dmCatalogos.cdsCompNiveles;
end;

procedure TEditMatPerComps_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     ZPerfil.Caption := Datasource.DataSet.FieldByName( 'CP_CODIGO' ).AsString + ' = ' + dmCatalogos.cdsCatPerfiles.FieldByName( 'CP_ELEMENT' ).AsString;
end;

procedure TEditMatPerComps_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsCompetencias.Conectar;
          //cdsMatrizCurso.Conectar;
          Datasource.Dataset := cdsMatPerfilComps;
     end;
end;

procedure TEditMatPerComps_DevEx.CC_CODIGOValidKey(Sender: TObject);
begin
     inherited;

     NC_NIVEL.Enabled := (CC_CODIGO.Llave <> VACIO );
     if StrLleno(CC_CODIGO.Llave) then
     begin
          NC_NIVEL.Filtro := Format(' CC_CODIGO = ''%s''',[dmCatalogos.cdsCompetencias.FieldByName('CC_CODIGO').AsString]);
     end
     else
         NC_NIVEL.Filtro := VACIO;

     with dmCatalogos.cdsMatPerfilComps.FieldByName('CC_CODIGO') do
     begin
          if ( OldValue <> NewValue )then
          begin
               NC_NIVEL.Llave := VACIO;
          end;
     end;

end;

end.



