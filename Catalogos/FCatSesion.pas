unit FCatSesion;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, 
  StdCtrls, ZetaNumero, Mask, ZetaFecha, Buttons;

type
  TCatSesiones = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    Panel1: TPanel;
    Label1: TLabel;
    CU_CURSO: TZetaKeyLookup;
    lblFecIni: TLabel;
    lblFecFin: TLabel;
    zFechaIni: TZetaFecha;
    zFechaFin: TZetaFecha;
    lblFolio: TLabel;
    sbBuscar: TSpeedButton;
    znFolio: TZetaNumero;
    procedure FormCreate(Sender: TObject);
    procedure CU_CURSOValidKey(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure zFechaIniValidDate(Sender: TObject);
    procedure zFechaFinValidDate(Sender: TObject);
    procedure sbBuscarClick(Sender: TObject);
  protected
    procedure Agregar;override;
    procedure Borrar; override;
    procedure Connect;override;
    procedure Modificar;override;
    procedure Refresh;override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CatSesiones: TCatSesiones;

implementation

{$R *.DFM}

uses dRecursos, dCatalogos, ZetaCommonLists, ZetaCommonClasses, ZetaCommonTools, ZetaDialogo;

{ THisCursos }
procedure TCatSesiones.FormShow(Sender: TObject);
begin
     inherited;
     with dmCatalogos do
     begin
          FecIniSesion := DiaDelMes( Date, True );
          FecFinSesion := DiaDelMes( Date, False );
          FolioSesion := 0;
          CodigoCursoSesion := VACIO;
          CU_CURSO.Llave := CodigoCursoSesion;
          zFechaIni.Valor := FecIniSesion;
          zFechaFin.Valor := FecFinSesion;
          znFolio.Valor := FolioSesion;
     end;
end;

procedure TCatSesiones.Connect;
begin
     CU_CURSO.LookUpDataSet := dmCatalogos.cdsCursos;
     with dmCatalogos do
     begin
          cdsCursos.Conectar;
          cdsSesiones.Conectar;
          cdsHisCursos.Conectar;
          DataSource.DataSet:= cdsSesiones;
     end;
end;

procedure TCatSesiones.Refresh;
begin
     dmCatalogos.cdsSesiones.Refrescar;
end;

procedure TCatSesiones.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H60647_Sesiones;
end;

procedure TCatSesiones.Agregar;
begin
     dmCatalogos.cdsSesiones.Agregar;
end;

procedure TCatSesiones.Borrar;
begin
     dmCatalogos.cdsSesiones.Borrar;
end;

procedure TCatSesiones.Modificar;
begin
     dmCatalogos.cdsSesiones.Modificar;
end;

procedure TCatSesiones.CU_CURSOValidKey(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        with dmCatalogos do
        begin
             CodigoCursoSesion := CU_CURSO.Llave;
             ObtieneSesiones;
             FolioSesion := 0;
             znFolio.Valor := FolioSesion;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

function TCatSesiones.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeAgregar( sMensaje );
     if Result then
     begin
          if strVacio( CU_CURSO.Llave )then
          begin
               sMensaje := 'Es Necesario Seleccionar Un Curso';
               Result := False;
          end;
     end;
end;

procedure TCatSesiones.zFechaIniValidDate(Sender: TObject);
begin
     inherited;
     with dmCatalogos do
     begin
          FecIniSesion := zFechaIni.Valor;
          ObtieneSesiones;
     end;
end;


procedure TCatSesiones.zFechaFinValidDate(Sender: TObject);
begin
     inherited;
     with dmCatalogos do
     begin
          FecFinSesion := zFechaFin.Valor;
          ObtieneSesiones;
     end;
end;

procedure TCatSesiones.sbBuscarClick(Sender: TObject);
var
   iSE_Folio: Integer;
begin
     inherited;
     CU_CURSO.SetFocus;
     iSE_FOLIO := znFolio.ValorEntero;
     if( iSE_Folio > 0 )then
     begin
          with dmCatalogos do
          begin
               FolioSesion := iSE_Folio;
               ObtieneSesiones;
               if( cdsSesiones.IsEmpty )then
               begin
                    znFolio.SetFocus;
                    ZInformation( Self.Caption, 'No Se Encontraron Sesiones Asociadas Con El Folio ' + InttoStr( iSE_FOLIO ), 0 );
               end
               else
               begin
                    CU_CURSO.Llave := cdsSesiones.FieldByName('CU_CODIGO').AsString;
                    CodigoCursoSesion := CU_CURSO.Llave;
               end;
          end;
     end;
end;

end.
