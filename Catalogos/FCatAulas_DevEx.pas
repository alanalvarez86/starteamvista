unit FCatAulas_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, StdCtrls,
  ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid;

type
  TCatAulas_DevEx = class(TBaseGridLectura_DevEx)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
   { Public declarations }
   procedure DoLookup; override;
  end;

var
  CatAulas_DevEx: TCatAulas_DevEx;

implementation

uses DCatalogos,
     ZetaCommonClasses,
     ZetaBuscador_DevEx;

{$R *.DFM}

procedure TCatAulas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H_CATALOGO_AULAS;
end;

procedure TCatAulas_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsAulas.Conectar;
          DataSource.DataSet:= cdsAulas;
     end;
end;

procedure TCatAulas_DevEx.Refresh;
begin
     dmCatalogos.cdsAulas.Refrescar;
end;

procedure TCatAulas_DevEx.Agregar;
begin
     dmCatalogos.cdsAulas.Agregar;
end;

procedure TCatAulas_DevEx.Borrar;
begin
     dmCatalogos.cdsAulas.Borrar;
end;

procedure TCatAulas_DevEx.Modificar;
begin
     dmCatalogos.cdsAulas.Modificar;
end;

procedure TCatAulas_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Aulas', 'Aulas', 'AL_CODIGO', dmCatalogos.cdsAulas );
end;


procedure TCatAulas_DevEx.FormShow(Sender: TObject);
begin
CreaColumaSumatoria(ZetaDBGridDBTAbleView.Columns[0],0 ,'' ,SkCount ); 
  inherited;

end;

end.
