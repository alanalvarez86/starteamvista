{$HINTS OFF}
unit FMatrizPuestoCertifica_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls, StdCtrls,
  Buttons, ZBaseConsulta, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, ZetaKeyLookup_DevEx, cxButtons,
  System.Actions;

type
  TMatrizPuestosCertificacion_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    LookUpCertificaciones: TZetaKeyLookup_DevEx;
    PU_CODIGO: TcxGridDBColumn;
    PU_NOMBRE: TcxGridDBColumn;
    PC_DIAS: TcxGridDBColumn;
    PC_OPCIONA: TcxGridDBColumn;
    PC_LISTA: TcxGridDBColumn;
    BAgregaPuestos_DevEx: TcxButton;
    procedure LookUpCertificacionesValidKey(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BAgregaPuestosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BAgregaPuestos_DevExClick(Sender: TObject);
    procedure LookUpCertificacionesClick(Sender: TObject);
    procedure SetEditarSoloActivos;
  private
    procedure ConfigAgrupamiento;
  protected
    procedure Connect; override;
    procedure DisConnect;override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
  public
  end;

var
  MatrizPuestosCertificacion_DevEx: TMatrizPuestosCertificacion_DevEx;

implementation

uses dCatalogos, dCliente, ZetaCommonClasses, FAutoClasses, FListaPuestos_DevEx;

{$R *.DFM}

{ TCatMatrizCurso }

procedure TMatrizPuestosCertificacion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H_Matriz_PuestosCertificacion;
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= TRUE;
     ZetaDBGridDBTableView.DataController.Filter.AutoDataSetFilter := TRUE;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TMatrizPuestosCertificacion_DevEx.DisConnect;
begin
     LookUpCertificaciones.LookUpDataSet := NIL;
end;

procedure TMatrizPuestosCertificacion_DevEx.Connect;
begin
     LookUpCertificaciones.LookUpDataSet := dmCatalogos.cdsCertificaciones;
     with dmCatalogos do
     begin
          with cdsCertificaciones do
          begin
               Conectar;
               LookUpCertificaciones.Llave := FieldByName('CI_CODIGO').AsString;
          end;
          MatrizPorCertific := True;
          cdsPuestos.Conectar;
          cdsMatrizCertificPuesto.Conectar;
          DataSource.DataSet:= cdsMatrizCertificPuesto;
          DoRefresh;
     end;
end;

procedure TMatrizPuestosCertificacion_DevEx.Refresh;
begin
     dmCatalogos.cdsMatrizCertificPuesto.Refrescar;
     // No s� por qu�, pero se pierde al activar la otra Matriz de Cursos
     ZetaDBGridDBTableView.Columns[ 0 ].DataBinding.FieldName := 'PU_CODIGO';
     ZetaDBGridDBTableView.Columns[ 2 ].DataBinding.FieldName := 'PC_DIAS';
     ZetaDBGridDBTableView.Columns[ 3 ].DataBinding.FieldName := 'PC_OPCIONA';
     ZetaDBGridDBTableView.Columns[ 4 ].DataBinding.FieldName := 'PC_LISTA';
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TMatrizPuestosCertificacion_DevEx.LookUpCertificacionesClick(
  Sender: TObject);
begin
  inherited;
   DoRefresh;
end;

procedure TMatrizPuestosCertificacion_DevEx.LookUpCertificacionesValidKey(Sender: TObject);
begin
     inherited;
     DoRefresh;
end;

procedure TMatrizPuestosCertificacion_DevEx.Agregar;
begin
     dmCatalogos.cdsMatrizCertificPuesto.Agregar;
end;

procedure TMatrizPuestosCertificacion_DevEx.Borrar;
begin
     dmCatalogos.cdsMatrizCertificPuesto.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TMatrizPuestosCertificacion_DevEx.Modificar;
begin
     dmCatalogos.cdsMatrizCertificPuesto.Modificar;
end;

function TMatrizPuestosCertificacion_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeAgregar( sMensaje );
end;

function TMatrizPuestosCertificacion_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeModificar( sMensaje );
end;

function TMatrizPuestosCertificacion_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeBorrar( sMensaje );
end;

procedure TMatrizPuestosCertificacion_DevEx.BAgregaPuestosClick(Sender: TObject);
var
  lFiltrado : Boolean;
begin
  inherited;
  with dmCatalogos.cdsMatrizCertificPuesto do
  begin
    lFiltrado := Filtered;
    Filtered := False;
    if SetListaPuestos( LookUpCertificaciones.Llave,False ) then
      DoRefresh;

    Filtered := lFiltrado;
  end;
end;

procedure TMatrizPuestosCertificacion_DevEx.ConfigAgrupamiento;
begin
     //Cambio de configuracion
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //Desactiva la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Esconde la caja de agrupamiento
     //Determinar las columnas por las que se podra agrupar
     PU_CODIGO.Options.Grouping:= FALSE;
     PU_NOMBRE.Options.Grouping:= FALSE;
     PC_DIAS.Options.Grouping:= FALSE;
     PC_LISTA.Options.Grouping:= FALSE;
end;

procedure TMatrizPuestosCertificacion_DevEx.FormShow(Sender: TObject);
begin
   ApplyMinWidth;
   CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
   inherited;
   ConfigAgrupamiento;

end;

procedure TMatrizPuestosCertificacion_DevEx.BAgregaPuestos_DevExClick(
  Sender: TObject);
var
  lFiltrado : Boolean;
begin
  inherited;
  with dmCatalogos.cdsMatrizCertificPuesto do
  begin
    lFiltrado := Filtered;
    Filtered := False;
     if SetListaPuestos( LookUpCertificaciones.Llave,False ) then
        DoRefresh;

    Filtered := lFiltrado;
  end;
end;

procedure TMatrizPuestosCertificacion_DevEx.SetEditarSoloActivos;
begin
     LookUpCertificaciones.EditarSoloActivos := TRUE;
end;

end.
