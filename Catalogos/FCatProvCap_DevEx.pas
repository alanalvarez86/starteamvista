unit FCatProvCap_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseGridLectura_DevEx, DB, ExtCtrls, Grids, DBGrids, ZetaDBGrid,
  StdCtrls, ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid;

type
  TCatProvCap_DevEx = class(TBaseGridLectura_DevEx)
    PC_CODIGO: TcxGridDBColumn;
    PC_NOMBRE: TcxGridDBColumn;
    PC_DIRECCI: TcxGridDBColumn;
    PC_CIUDAD: TcxGridDBColumn;
    PC_TEL: TcxGridDBColumn;
    PC_ACTIVO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ConfigAgrupamiento;
  protected
   { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;

  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  CatProvCap_DevEx: TCatProvCap_DevEx;

implementation

uses dCatalogos,
     ZetaBuscador_DevEx,
     ZetaCommonClasses;

{$R *.dfm}

{ TCatProvCap }

procedure TCatProvCap_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H_PROV_CAPACITACION;
end;

procedure TCatProvCap_DevEx.Agregar;
begin
     dmCatalogos.cdsProvCap.Agregar;
end;

procedure TCatProvCap_DevEx.Borrar;
begin
     dmCatalogos.cdsProvCap.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatProvCap_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsProvCap.Conectar;
          DataSource.DataSet:= cdsProvCap;
     end;
end;

procedure TCatProvCap_DevEx.Modificar;
begin
     dmCatalogos.cdsProvCap.Modificar;
end;

procedure TCatProvCap_DevEx.Refresh;
begin
     dmCatalogos.cdsProvCap.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatProvCap_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Proveedor:', 'Proveedores de Capacitación', 'PC_CODIGO', dmCatalogos.cdsProvCap );
end;

procedure TCatProvCap_DevEx.ConfigAgrupamiento;
begin
     //Cambio de configuracion
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //Desactiva la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Esconde la caja de agrupamiento
     //Determinar las columnas por las que se podra agrupar
     PC_CODIGO.Options.Grouping:= FALSE;
     PC_NOMBRE.Options.Grouping:= FALSE;
     PC_DIRECCI.Options.Grouping:= FALSE;
     PC_TEL.Options.Grouping:= FALSE;
     PC_ACTIVO.Options.Grouping:= FALSE;
end;

procedure TCatProvCap_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     {***Banda Sumatoria***}
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
  inherited;
     //DevEx (by am):Configuracion Especial para agrupamiento
     ConfigAgrupamiento;
end;

end.
