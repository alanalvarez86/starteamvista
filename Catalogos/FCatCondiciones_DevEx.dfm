inherited CatCondiciones_DevEx: TCatCondiciones_DevEx
  Left = 320
  Top = 177
  Caption = 'Condiciones'
  ClientHeight = 271
  ClientWidth = 538
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 538
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 212
      inherited textoValorActivo2: TLabel
        Width = 206
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 538
    Height = 252
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object QU_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'QU_CODIGO'
        Options.Grouping = False
      end
      object QU_DESCRIP: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'QU_DESCRIP'
        Options.Grouping = False
      end
      object QU_NAVEGA: TcxGridDBColumn
        Caption = #191'Navegaci'#243'n?'
        DataBinding.FieldName = 'QU_NAVEGA'
      end
      object QU_ORDEN: TcxGridDBColumn
        Caption = 'Orden'
        DataBinding.FieldName = 'QU_ORDEN'
        Options.Grouping = False
      end
      object US_CODIGO: TcxGridDBColumn
        Caption = 'Modific'#243
        DataBinding.FieldName = 'US_CODIGO'
        Options.Grouping = False
      end
      object QU_NIVEL: TcxGridDBColumn
        Caption = 'Nivel'
        DataBinding.FieldName = 'QU_NIVEL'
        Options.Grouping = False
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
