unit FCatConceptos_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, DBGrids, Db, ExtCtrls, Grids,
     ZBaseConsulta,
     ZetaDBGrid, StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, Buttons, cxButtons, System.Actions, dConsultas, dCliente;


  const
     K_COLOR_ROJO = $002715DD; //dd1527
     K_COLOR_VERDE = $9FFFB8;
     K_COLOR_AMARILLO = $00BFFF;
     K_COLOR_AZUL = $FFA500;
     K_DIAS_ADVERTIR = 30;
     K_EVALUAR_CURSOS = 1;
     K_VER_HISTORIAL = 2;
     K_COL_FEC_PROG = 17;
     K_COL_FEC_TOMADO = 18;

function AbreDialogo( OpenDialog: TOpenDialog; sExt: String): Boolean;
type
  TCatConceptos_devEx = class(TBaseGridLectura_DevEx)
    CO_USO_NOM: TcxGridDBColumn;
    CO_NUMERO: TcxGridDBColumn;
    Tipo_SAT: TcxGridDBColumn;
    CO_DESCRIP: TcxGridDBColumn;
    CO_ACTIVO: TcxGridDBColumn;
    CO_TIPO: TcxGridDBColumn;
    pnlImportarConceptos: TPanel;
    btnImportarConceptos: TcxButton;
    OpenDialog: TOpenDialog;
    Tipo_SAT_2: TcxGridDBColumn;
    Tratamiento: TcxGridDBColumn;
    CO_Exento_Gravado: TcxGridDBColumn;
    cxStyleRepRojo: TcxStyleRepository;
    CO_TIMBRA: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnImportarConceptosClick(Sender: TObject);
    procedure ObtenerValoresComparar(var iTressPlazo: Integer; var  AViewInfo: TcxGridTableDataCellViewInfo);
    procedure CambiarPropiedadesGrid(var ACanvas: TcxCanvas);
    procedure ZetaDBGridDBTableViewCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
  private
    FStyleRojo: TcxStyle;
    FStyleVerde: TcxStyle;
    FStyleAmarillo: TcxStyle;
    FStyleAzul: TcxStyle;
    procedure InitColoresCeldas;
    //function ConceptoValido( var sMensaje : string ): Boolean;
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma; override;
  public
    { Public declarations }
    procedure DoLookup; override;
    //function PuedeModificar( var sMensaje: String ): Boolean; override;
  end;

var
  CatConceptos_devEx: TCatConceptos_devEx;

implementation

uses DCatalogos,
     ZetaDialogo,
     ZImprimeForma,
     ZetaTipoEntidad,
     ZetaBuscaEntero_DevEx,
     ZetaCommonClasses,
     ZetaClientTools,
     ZetaCommonTools,
     ZetaClientDataSet,
     FImportarConceptos_DevEx,
     ZAccesosMgr,
     ZAccesosTress;

{$R *.DFM}

{ TCatConceptos }
function AbreDialogo( OpenDialog: TOpenDialog; sExt: String ): Boolean;
begin
     with OpenDialog do
     begin
          DefaultExt := sExt;
          if StrVacio( Filter ) then
             Filter := sExt + '|' + '*.' + sExt + '|' + 'Todos' + '|' + '*.*' ;
          FileName := ExtractFileName( ExtractFilePath (Application.Name) + 'Archivo.xml' );
          InitialDir := ExtractFilePath( Application.Name );
          Result := Execute;
     end;
end;
procedure TCatConceptos_devEx.FormCreate(Sender: TObject);
begin
     inherited;
     //Desactivar gridmode (FormCreate)
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= FALSE;
     CanLookup := True;
     HelpContext := H60621_Catalogo_conceptos;
     InitColoresCeldas;
end;

procedure TCatConceptos_devEx.Connect;
var
  iValueConceptos : integer;
begin
	{$ifdef SISNOM_CLAVES}
          with dmCatalogos do
          begin
               cdsConceptosSeguridad.Conectar;
               DataSource.DataSet:= cdsConceptosSeguridad;
          end;
	{$else}
     with dmCatalogos do
     begin
          cdsConceptos.Conectar;
          cdsConceptosLookUp.Conectar;
          DataSource.DataSet:= cdsConceptos;
          //cdsConceptosPrioridades.Conectar;
     end;
	{$endif}
     {iValueConceptos := dmCatalogos.cdsConceptos.FieldByName('CO_NUMERO').AsInteger;
     dmCatalogos.cdsConceptos.Refrescar;
     dmCatalogos.cdsConceptos.Locate('CO_NUMERO', iValueConceptos, []);}
end;

procedure TCatConceptos_devEx.Refresh;
begin
	{$ifdef SISNOM_CLAVES}
     dmCatalogos.cdsConceptosSeguridad.Refrescar;
	{$else}
     dmCatalogos.cdsConceptos.Refrescar;
     dmCatalogos.cdsConceptosLookup.Refrescar;
	{$endif}
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatConceptos_devEx.ZetaDBGridDBTableViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   iTressPlazo: Integer;

   aRecords: TcxCustomGridTableView;
   dFecha: TDate;
begin
  inherited;
  try
        ObtenerValoresComparar(iTressPlazo, AViewInfo);
        if iTressPlazo <> 0 then
        begin
             CambiarPropiedadesGrid( ACanvas );
        end;

           if AViewInfo.RecordViewInfo.Selected then
           begin
                if iTressPlazo <> 0 then
                begin
                         ACanvas.Font.Color := clBlack;
                         ACanvas.FillRect(AViewInfo.Bounds, TColor( $00ccccff ));   //$00e6e6ff
                end;
           end;
  finally

  end;

end;

procedure TCatConceptos_devEx.CambiarPropiedadesGrid(var ACanvas: TcxCanvas);
begin
     with ACanvas do
     begin
          Font.Color := clWhite;
          Brush.Color :=  TColor( K_COLOR_ROJO );
     end;
end;


procedure TCatConceptos_devEx.Agregar;
begin
	{$ifdef SISNOM_CLAVES}
     dmCatalogos.cdsConceptosSeguridad.Agregar;
	{$else}
     dmCatalogos.cdsConceptos.Agregar;
     //dmCatalogos.cdsConceptosPrioridades.Refrescar;
     dmCatalogos.cdsConceptosLookup.Refrescar;
	{$endif}
end;

procedure TCatConceptos_devEx.Borrar;
begin
	{$ifdef SISNOM_CLAVES}
     dmCatalogos.cdsConceptosSeguridad.Borrar;
	{$else}
     dmCatalogos.cdsConceptos.Borrar;
     //dmCatalogos.cdsConceptosPrioridades.Refrescar;
     dmCatalogos.cdsConceptosLookup.Refrescar;
	{$endif}
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatConceptos_devEx.Modificar;
begin
	{$ifdef SISNOM_CLAVES}
     dmCatalogos.cdsConceptosSeguridad.Modificar;
	{$else}
     dmCatalogos.cdsConceptos.Modificar;
     //dmCatalogos.cdsConceptosPrioridades.Refrescar;
     dmCatalogos.cdsConceptosLookup.Refrescar;
	{$endif}
end;

procedure TCatConceptos_devEx.DoLookup;
begin
     inherited;
     ZetaBuscaEntero_DevEx.BuscarCodigo( 'N�mero', 'Concepto de N�mina', 'CO_NUMERO', {$ifdef SISNOM_CLAVES}dmCatalogos.cdsConceptosSeguridad{$else}dmCatalogos.cdsConceptos{$endif} );
end;

{
function TCatConceptos.ConceptoValido( var sMensaje : string ) : Boolean;
begin
     Result := DataSource.DataSet.FieldByName('CO_NUMERO').AsInteger < 1000;
     if NOT Result then
        sMensaje := 'Los Conceptos Mayores a 1000 son para uso Exclusivo del Sistema';
end;

function TCatConceptos.PuedeModificar( var sMensaje: String ): Boolean;
begin
     Result := ConceptoValido(sMensaje);
end;
}

procedure TCatConceptos_devEx.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enConcepto, dmCatalogos.cdsConceptos );
end;

procedure TCatConceptos_devEx.FormShow(Sender: TObject);
begin
CreaColumaSumatoria(ZetaDbGridDBtableView.Columns[0],0 , '' , SkCount );
  ApplyMinWidth;
  inherited;
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := true;
  ZetaDBGridDBTableView.OptionsView.GroupByBox := true;
end;

procedure TCatConceptos_devEx.ObtenerValoresComparar(var iTressPlazo: Integer; var AViewInfo: TcxGridTableDataCellViewInfo);
var
  sValueTipoSat, sValueTipoSat2 : string;
  sTipoConcepto : string;
  sConceptoActivo : string;
  sConceptoAplicaTimbrar : string;
begin
      try
           iTressPlazo := 0;
           sValueTipoSat := VACIO;
           sTipoConcepto := VACIO;
           sValueTipoSat2 := VACIO;
           sConceptoActivo := VACIO;
           sConceptoAplicaTimbrar := VACIO;
           sTipoConcepto := AViewInfo.GridRecord.Values[CO_TIPO.Index];
           sValueTipoSat := AViewInfo.GridRecord.Values[Tipo_SAT.Index];
           sValueTipoSat2 := AViewInfo.GridRecord.Values[Tipo_SAT_2.Index];
           sConceptoActivo := AViewInfo.GridRecord.Values[CO_ACTIVO.Index];
           sConceptoAplicaTimbrar := AViewInfo.GridRecord.Values[CO_TIMBRA.Index];

           if (sValueTipoSat = VACIO) and (sValueTipoSat2 = VACIO) and ((sTipoConcepto = '1') or (sTipoConcepto = '4')) and (sConceptoActivo = 'S') and (sConceptoAplicaTimbrar = 'S') then
           begin
                iTressPlazo := AViewInfo.GridRecord.Values[CO_NUMERO.Index];
           end;
      except
          iTressPlazo := 0;
      end
end;

procedure TCatConceptos_devEx.btnImportarConceptosClick(Sender: TObject);
var
   cdsXML : TZetaClientDataSet;
   sStringPrueba: String;
   impConceptos_DevEx: TImportarConceptos_DevEx;
begin
  inherited;
 cdsXML := TZetaClientDataSet.Create(Self);
     with cdsXML do
     begin
          Active:= False;
          Filtered := False;
          Filter := '';
          IndexFieldNames := '';
     end;
     try
         if (AbreDialogo( OpenDialog, 'xml' )) then
         begin
              cdsXML.LoadFromFile(OpenDialog.FileName);
              sStringPrueba := cdsXML.FieldByName ('CO_FORMULA').AsString;

              impConceptos_DevEx := TImportarConceptos_DevEx.Create(Self);
              impConceptos_DevEx.mostrarConceptos(cdsXML);
         end;
     except
           ZError(Caption, Format ('Archivo inv�lido (''%s'')' + CR_LF + 'Favor de seleccionar otro archivo', [OpenDialog.FileName]), 0);
     end;
end;


procedure TCatConceptos_devEx.InitColoresCeldas;
begin
     // Colores de Celdas
     try
           FStyleRojo := cxStyleRepRojo.CreateItem(TcxStyle) as TcxStyle;
           FStyleRojo.Color := TColor( K_COLOR_ROJO );
     finally

     end;
end;

end.

