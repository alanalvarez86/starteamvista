{$HINTS OFF}
unit FCatFestTurno_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, Grids, DBGrids, StdCtrls,
     ZBaseConsulta,
     ZetaDBGrid,
     ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, ZetaKeyLookup_DevEx;

type
  TCatFestTurno_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    dsTurnos: TDataSource;
    LookupTurnos: TZetaKeyLookup_DevEx;
    FE_MES: TcxGridDBColumn;
    FE_DIA: TcxGridDBColumn;
    FE_YEAR: TcxGridDBColumn;
    FE_DESCRIP: TcxGridDBColumn;
    FE_TIPO: TcxGridDBColumn;
    FE_CAMBIO: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LookupTurnosValidKey(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure DisConnect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  CatFestTurno_DevEx: TCatFestTurno_DevEx;

implementation

uses DCatalogos,
     ZetaCommonClasses;

{$R *.DFM}

{ TCatFestTurno }

procedure TCatFestTurno_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H391000_Festivos_Turno;
end;

procedure TCatFestTurno_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     Refresh;

    //columna sumatoria: cuantos
    CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('FE_MES'), K_SIN_TIPO , '', skCount);
    ZetaDBGridDBTableView.ApplyBestFit();

    //agrupar
    ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
    //ver la caja de agrupamiento
    ZetaDBGridDBTableView.OptionsView.GroupByBox := True;



end;

procedure TCatFestTurno_DevEx.Connect;
begin
     LookUpTurnos.LookUpDataSet := dmCatalogos.cdsTurnos;
     with dmCatalogos do
     begin
          with cdsTurnos do
          begin
               Conectar;
               //First;  No se requiere posicionar al turno en el primero
               LookUpTurnos.Llave := FieldByName('TU_CODIGO').AsString;
          end;
          TurnoFestivo:= LookUpTurnos.Llave;
          cdsFestTurno.Conectar;
          DataSource.DataSet:= cdsFestTurno;
          Refresh;
     end;
end;

procedure TCatFestTurno_DevEx.DisConnect;
begin
     LookUpTurnos.LookUpDataSet := NIL;
end;

procedure TCatFestTurno_DevEx.Refresh;
begin
     with dmCatalogos do
     begin
          TurnoFestivo := LookUpTurnos.Llave;
          cdsFestTurno.Refrescar;
     end;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatFestTurno_DevEx.LookupTurnosValidKey(Sender: TObject);
begin
     inherited;
     Refresh;
end;

procedure TCatFestTurno_DevEx.Agregar;
begin
     dmCatalogos.cdsFestTurno.Agregar;
end;

procedure TCatFestTurno_DevEx.Borrar;
begin
     dmCatalogos.cdsFestTurno.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatFestTurno_DevEx.Modificar;
begin
     dmCatalogos.cdsFestTurno.Modificar;
end;

end.

