unit FEditCatPresta_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, Db, Buttons,
  DBCtrls, StdCtrls, ExtCtrls, Mask, ZetaDBTextBox, ZetaNumero,
  ZetaSmartLists, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons;

type
  TEditCatPresta_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    TB_CODIGOlbl: TLabel;
    TB_CODIGO: TZetaDBTextBox;
    PT_YEAR: TZetaDBNumero;
    TB_ELEMENTlbl: TLabel;
    Label3: TLabel;
    PT_DIAS_VA: TZetaDBNumero;
    PT_PRIMAVA: TZetaDBNumero;
    TB_PRIMAlbl: TLabel;
    TB_DIAS_AGUIlbl: TLabel;
    PT_DIAS_AG: TZetaDBNumero;
    PT_PAGO_7: TDBCheckBox;
    PT_PRIMA_7: TDBCheckBox;
    TB_DOMINICALlbl: TLabel;
    PT_PRIMADO: TZetaDBNumero;
    PT_DIAS_AD: TZetaDBNumero;
    Label2: TLabel;
    Label6: TLabel;
    PT_FACTOR: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
  private
  protected
    procedure Connect; override;
    procedure EscribirCambios; override;
    procedure DoCancelChanges; override;
  public
  end;

var
  EditCatPresta_DevEx: TEditCatPresta_DevEx;

implementation

uses dCatalogos, ZetaCommonClasses, ZAccesosTress;

{$R *.DFM}

procedure TEditCatPresta_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H66162_Modificar_renglon;
     IndexDerechos := ZAccesosTress.D_CAT_CONT_PRESTA;
     FirstControl := PT_YEAR;
end;

procedure TEditCatPresta_DevEx.Connect;
begin
//     dmCatalogos.cdsPrestaci.Conectar;       No se ocupa, es DataSetField de cdsSocial
     DataSource.Dataset := dmCatalogos.cdsPrestaci;
end;

procedure TEditCatPresta_DevEx.EscribirCambios;
begin
     ClientDataSet.Post;
end;

procedure TEditCatPresta_DevEx.DoCancelChanges;
begin
     ClientDataset.Cancel;        // Si se deja la herencia cancela todos los post que se hayan hecho
end;

end.
