unit FEditCatAulas_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls, DBCtrls, Buttons, StdCtrls,
  ZetaNumero, Mask, ZetaEdit, ZBaseEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons, cxContainer, cxEdit,
  cxTextEdit, cxMemo, cxDBEdit;

type
  TEditCatAulas_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    AL_DESCRIP: TcxDBMemo;
    lblDescrip: TLabel;
    AL_CUPO: TZetaDBNumero;
    lblCupo: TLabel;
    AL_TEXTO: TDBEdit;
    Label1: TLabel;
    AL_NUMERO: TZetaDBNumero;
    Label2: TLabel;
    AL_INGLES: TDBEdit;
    DBInglesLBL: TLabel;
    AL_NOMBRE: TDBEdit;
    lblNombre: TLabel;
    AL_CODIGO: TZetaDBEdit;
    DBCodigoLBL: TLabel;
    AL_ACTIVA: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
     procedure Connect; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  EditCatAulas_DevEx: TEditCatAulas_DevEx;

implementation

uses DCatalogos,
     ZAccesosTress,
     ZetaCommonClasses,
     ZetaBuscador_DevEx;

{$R *.DFM}

procedure TEditCatAulas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := AL_CODIGO;
     IndexDerechos := D_CAT_CAPA_AULAS;
     HelpContext:= H_CATALOGO_AULAS;
end;

procedure TEditCatAulas_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          DataSource.DataSet := cdsAulas;
          Self.Caption := cdsAulas.LookupName;
     end;
end;

procedure TEditCatAulas_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Self.Caption, 'AL_CODIGO', dmCatalogos.cdsAulas );
end;

end.
