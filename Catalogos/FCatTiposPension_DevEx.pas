unit FCatTiposPension_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, DB, ExtCtrls,
  StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid;

type
  TCatTiposPension_DevEx = class(TBaseGridLectura_DevEx)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure DoLookup; override;
  protected
    { Public declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;

  end;

var
  CatTiposPension_DevEx: TCatTiposPension_DevEx;

implementation                  

uses ZetaDialogo,
     ZImprimeForma,
     ZetaTipoEntidad,
     ZetaCommonClasses,
     ZetaBuscador_DevEx,
     dCatalogos;

{$R *.dfm}

procedure TCatTiposPension_DevEx.FormCreate(Sender: TObject);
begin
  inherited;
  CanLookup := True;
  HelpContext:= H_Cat_TiposPension;
end;

procedure TCatTiposPension_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsTiposPension.Conectar;
          DataSource.DataSet := cdsTiposPension;
     end;
end;

procedure TCatTiposPension_DevEx.Refresh;
begin
     dmCatalogos.cdsTiposPension.Refrescar;
end;

procedure TCatTiposPension_DevEx.Agregar;
begin
     dmCatalogos.cdsTiposPension.Agregar;
end;

procedure TCatTiposPension_DevEx.Borrar;
begin
     dmCatalogos.cdsTiposPension.Borrar;
end;

procedure TCatTiposPension_DevEx.Modificar;
begin
     dmCatalogos.cdsTiposPension.Modificar;
end;

procedure TCatTiposPension_DevEx.DoLookup;
begin
     ZetaBuscador_DevEx.BuscarCodigo( 'Tipos de Pensi�n', 'Tipos de Pensi�n', 'TP_CODIGO', dmCatalogos.cdsTiposPension );
end;

procedure TCatTiposPension_DevEx.FormShow(Sender: TObject);
begin
    CreaColumaSumatoria(ZetaDbGridDBTableView.Columns[0],0,'',SkCount );
    inherited;

end;

end.
