inherited EditCatEventos_DevEx: TEditCatEventos_DevEx
  Left = 671
  Top = 154
  Caption = 'Eventos'
  ClientHeight = 517
  ClientWidth = 660
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 0
    Top = 104
    Width = 660
    Height = 4
    Cursor = crVSplit
    Align = alTop
  end
  inherited PanelBotones: TPanel
    Top = 481
    Width = 660
    TabOrder = 3
    inherited OK_DevEx: TcxButton
      Left = 496
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 575
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 660
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 334
      inherited textoValorActivo2: TLabel
        Width = 328
      end
    end
  end
  object Panel1: TPanel [4]
    Left = 0
    Top = 50
    Width = 660
    Height = 54
    Align = alTop
    TabOrder = 4
    object Label1: TLabel
      Left = 36
      Top = 8
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'digo:'
    end
    object Label2: TLabel
      Left = 13
      Top = 32
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descripci'#243'n:'
    end
    object Label3: TLabel
      Left = 320
      Top = 8
      Width = 3
      Height = 13
    end
    object Label4: TLabel
      Left = 328
      Top = 28
      Width = 44
      Height = 13
      Caption = 'Prioridad:'
    end
    object EV_ACTIVO: TDBCheckBox
      Left = 337
      Top = 4
      Width = 56
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Activo:'
      DataField = 'EV_ACTIVO'
      DataSource = DataSource
      TabOrder = 2
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
    object EV_CODIGO: TZetaDBEdit
      Left = 80
      Top = 4
      Width = 97
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 10
      TabOrder = 0
      ConfirmEdit = True
      DataField = 'EV_CODIGO'
      DataSource = DataSource
    end
    object EV_DESCRIP: TDBEdit
      Left = 80
      Top = 28
      Width = 230
      Height = 21
      DataField = 'EV_DESCRIP'
      DataSource = DataSource
      TabOrder = 1
    end
    object EV_PRIORID: TZetaDBKeyCombo
      Left = 380
      Top = 24
      Width = 145
      Height = 21
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 3
      ListaFija = lfNivelUsuario
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
      DataField = 'EV_PRIORID'
      DataSource = DataSource
      LlaveNumerica = True
    end
  end
  object PageControl: TcxPageControl [5]
    Left = 0
    Top = 108
    Width = 660
    Height = 373
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Properties.ActivePage = TabCambios
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 371
    ClientRectLeft = 2
    ClientRectRight = 658
    ClientRectTop = 27
    object TabFiltros: TcxTabSheet
      Caption = 'Filtros'
      object Label5: TLabel
        Left = 125
        Top = 85
        Width = 25
        Height = 13
        Alignment = taRightJustify
        Caption = 'Filtro:'
        Transparent = True
      end
      object Label6: TLabel
        Left = 100
        Top = 154
        Width = 50
        Height = 13
        Alignment = taRightJustify
        Caption = 'Condici'#243'n:'
        Transparent = True
      end
      object Label7: TLabel
        Left = 78
        Top = 41
        Width = 72
        Height = 13
        Alignment = taRightJustify
        Caption = 'Antig'#252'edad de:'
        Transparent = True
      end
      object Label8: TLabel
        Left = 220
        Top = 41
        Width = 6
        Height = 13
        Caption = 'a'
        Transparent = True
      end
      object Label12: TLabel
        Left = 113
        Top = 16
        Width = 37
        Height = 13
        Caption = 'Incluye:'
        Transparent = True
      end
      object Label9: TLabel
        Left = 300
        Top = 41
        Width = 23
        Height = 13
        Caption = 'D'#237'as'
        Transparent = True
      end
      object sbFiltro: TcxButton
        Left = 539
        Top = 62
        Width = 25
        Height = 25
        Hint = 'Constructor de F'#243'rmulas'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = sbFiltroClick
        OptionsImage.Glyph.Data = {
          7A080000424D7A08000000000000360000002800000017000000170000000100
          2000000000004408000000000000000000000000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFF000000000000000000000000D9AB81FFD8AA7FFF0000000000000000D8AA
          7FFFD8AA7FFF000000002E241B36D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000
          0000DCB38DFF0000000000000000D8AA7FFFDAAF87FF00000000000000000000
          00000000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD9AD83FF00000000DAAE85FFD8AA7FFFD8AA7FFF00000000000000000302
          0203D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFF00000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000
          000000000000D8AA7FFFD8AA7FFF010000010000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF000000000000
          0000D8AA7FFF0000000000000000D8AA7FFF0000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000DCB3
          8DFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFF0000000000000000000000000000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD9AD83FF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFDCB28BFF00000000D9AD83FF0000000000000000D8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFF00000000000000000000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
        OptionsImage.Margin = 1
      end
      object EV_FILTRO: TcxDBMemo
        Left = 164
        Top = 61
        DataBinding.DataField = 'EV_FILTRO'
        DataBinding.DataSource = DataSource
        ParentFont = False
        Properties.MaxLength = 255
        Properties.ScrollBars = ssVertical
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Courier New'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 3
        Height = 84
        Width = 373
      end
      object EV_INCLUYE: TZetaDBKeyCombo
        Left = 164
        Top = 12
        Width = 145
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 0
        ListaFija = lfIncluyeEvento
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'EV_INCLUYE'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object EV_ANT_INI: TZetaDBNumero
        Left = 164
        Top = 37
        Width = 50
        Height = 21
        Mascara = mnDias
        TabOrder = 1
        Text = '0'
        UseEnterKey = True
        DataField = 'EV_ANT_INI'
        DataSource = DataSource
      end
      object EV_ANT_FIN: TZetaDBNumero
        Left = 235
        Top = 37
        Width = 50
        Height = 21
        Mascara = mnDias
        TabOrder = 2
        Text = '0'
        UseEnterKey = True
        DataField = 'EV_ANT_FIN'
        DataSource = DataSource
      end
      object EV_QUERY: TZetaDBKeyLookup_DevEx
        Left = 164
        Top = 150
        Width = 400
        Height = 21
        LookupDataset = dmCatalogos.cdsCondiciones
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 80
        DataField = 'EV_QUERY'
        DataSource = DataSource
      end
    end
    object TabCambios: TcxTabSheet
      Caption = 'Cambios'
      object EV_PUESTOLbl: TLabel
        Left = 190
        Top = 16
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Puesto:'
      end
      object CB_CLASIFIlbl: TLabel
        Left = 164
        Top = 40
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'Clasificaci'#243'n:'
      end
      object CB_HORARIOlbl: TLabel
        Left = 195
        Top = 112
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Turno:'
      end
      object Label10: TLabel
        Left = 150
        Top = 88
        Width = 76
        Height = 13
        Alignment = taRightJustify
        Caption = 'Kardex General:'
      end
      object Label11: TLabel
        Left = 117
        Top = 64
        Width = 109
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tabla de Prestaciones:'
      end
      object EV_PUESTO: TZetaDBKeyLookup_DevEx
        Left = 234
        Top = 12
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsPuestos
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        DataField = 'EV_PUESTO'
        DataSource = DataSource
      end
      object EV_CLASIFI: TZetaDBKeyLookup_DevEx
        Left = 234
        Top = 36
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsClasifi
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        DataField = 'EV_CLASIFI'
        DataSource = DataSource
      end
      object EV_TURNO: TZetaDBKeyLookup_DevEx
        Left = 234
        Top = 108
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsTurnos
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        DataField = 'EV_TURNO'
        DataSource = DataSource
      end
      object EV_KARDEX: TZetaDBKeyLookup_DevEx
        Left = 234
        Top = 84
        Width = 300
        Height = 21
        Filtro = 'TB_SISTEMA='#39'N'#39
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        DataField = 'EV_KARDEX'
        DataSource = DataSource
      end
      object EV_TABLASS: TZetaDBKeyLookup_DevEx
        Left = 234
        Top = 60
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        DataField = 'EV_TABLASS'
        DataSource = DataSource
      end
      object GroupBox1: TcxGroupBox
        Left = 128
        Top = 216
        Caption = 'Renovaci'#243'n de Contrato'
        TabOrder = 7
        Height = 73
        Width = 417
        object CB_CONTRATlbl: TLabel
          Left = 23
          Top = 22
          Width = 82
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo de Contrato:'
        end
        object EV_CONTRAT: TZetaDBKeyLookup_DevEx
          Left = 114
          Top = 20
          Width = 300
          Height = 21
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 60
          DataField = 'EV_CONTRAT'
          DataSource = DataSource
        end
        object EV_FEC_CON: TDBCheckBox
          Left = 23
          Top = 47
          Width = 198
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Renovar a la Fecha de Vencimiento:'
          DataField = 'EV_FEC_CON'
          DataSource = DataSource
          Enabled = False
          TabOrder = 1
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
      end
      object EV_TIPONOM_gb: TcxGroupBox
        Left = 128
        Top = 160
        TabOrder = 6
        Height = 52
        Width = 417
        object EV_TIPNOM_lbl: TLabel
          Left = 27
          Top = 22
          Width = 81
          Height = 13
          Caption = 'Tipo de N'#243'mina: '
        end
        object EV_TIPNOM: TZetaDBKeyCombo
          Left = 114
          Top = 17
          Width = 190
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          Enabled = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 0
          ListaFija = lfTipoNomina
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = True
          DataField = 'EV_TIPNOM'
          DataSource = DataSource
          LlaveNumerica = True
        end
      end
      object EV_CAMBNOM: TDBCheckBox
        Left = 128
        Top = 137
        Width = 137
        Height = 17
        Caption = 'Cambio Tipo de N'#243'mina'
        DataField = 'EV_CAMBNOM'
        DataSource = DataSource
        TabOrder = 5
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        OnClick = EV_CAMBNOMClick
      end
    end
    object TabArea: TcxTabSheet
      Caption = 'Area'
      object CB_NIVEL1lbl: TLabel
        Left = 146
        Top = 4
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel1:'
      end
      object CB_NIVEL2lbl: TLabel
        Left = 146
        Top = 28
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel2:'
      end
      object CB_NIVEL3lbl: TLabel
        Left = 146
        Top = 52
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel3:'
      end
      object CB_NIVEL4lbl: TLabel
        Left = 146
        Top = 76
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel4:'
      end
      object CB_NIVEL5lbl: TLabel
        Left = 146
        Top = 100
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel5:'
      end
      object CB_NIVEL6lbl: TLabel
        Left = 146
        Top = 124
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel6:'
      end
      object CB_NIVEL7lbl: TLabel
        Left = 146
        Top = 148
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel7:'
      end
      object CB_NIVEL8lbl: TLabel
        Left = 146
        Top = 172
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel8:'
      end
      object CB_NIVEL9lbl: TLabel
        Left = 146
        Top = 196
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel9:'
      end
      object CB_NIVEL10lbl: TLabel
        Left = 140
        Top = 220
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel10:'
        Visible = False
      end
      object CB_NIVEL11lbl: TLabel
        Left = 140
        Top = 244
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel11:'
        Visible = False
      end
      object CB_NIVEL12lbl: TLabel
        Left = 140
        Top = 268
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel12:'
        Visible = False
      end
      object EV_NIVEL9: TZetaDBKeyLookup_DevEx
        Left = 192
        Top = 192
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel9
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 8
        TabStop = True
        WidthLlave = 60
        OnValidLookup = EV_NIVEL9ValidLookup
        DataField = 'EV_NIVEL9'
        DataSource = DataSource
      end
      object EV_NIVEL8: TZetaDBKeyLookup_DevEx
        Left = 192
        Top = 168
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel8
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 7
        TabStop = True
        WidthLlave = 60
        OnValidLookup = EV_NIVEL8ValidLookup
        DataField = 'EV_NIVEL8'
        DataSource = DataSource
      end
      object EV_NIVEL7: TZetaDBKeyLookup_DevEx
        Left = 192
        Top = 144
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel7
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 60
        OnValidLookup = EV_NIVEL7ValidLookup
        DataField = 'EV_NIVEL7'
        DataSource = DataSource
      end
      object EV_NIVEL6: TZetaDBKeyLookup_DevEx
        Left = 192
        Top = 120
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel6
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 60
        OnValidLookup = EV_NIVEL6ValidLookup
        DataField = 'EV_NIVEL6'
        DataSource = DataSource
      end
      object EV_NIVEL5: TZetaDBKeyLookup_DevEx
        Left = 192
        Top = 96
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel5
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        OnValidLookup = EV_NIVEL5ValidLookup
        DataField = 'EV_NIVEL5'
        DataSource = DataSource
      end
      object EV_NIVEL4: TZetaDBKeyLookup_DevEx
        Left = 192
        Top = 72
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel4
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        OnValidLookup = EV_NIVEL4ValidLookup
        DataField = 'EV_NIVEL4'
        DataSource = DataSource
      end
      object EV_NIVEL3: TZetaDBKeyLookup_DevEx
        Left = 192
        Top = 48
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel3
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        OnValidLookup = EV_NIVEL3ValidLookup
        DataField = 'EV_NIVEL3'
        DataSource = DataSource
      end
      object EV_NIVEL2: TZetaDBKeyLookup_DevEx
        Left = 192
        Top = 24
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel2
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        OnValidLookup = EV_NIVEL2ValidLookup
        DataField = 'EV_NIVEL2'
        DataSource = DataSource
      end
      object EV_NIVEL1: TZetaDBKeyLookup_DevEx
        Left = 192
        Top = 0
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel1
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        DataField = 'EV_NIVEL1'
        DataSource = DataSource
      end
      object EV_NIVEL10: TZetaDBKeyLookup_DevEx
        Left = 192
        Top = 216
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 9
        TabStop = True
        Visible = False
        WidthLlave = 60
        OnValidLookup = EV_NIVEL10ValidLookup
        DataField = 'EV_NIVEL10'
      end
      object EV_NIVEL11: TZetaDBKeyLookup_DevEx
        Left = 192
        Top = 240
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 10
        TabStop = True
        Visible = False
        WidthLlave = 60
        OnValidLookup = EV_NIVEL11ValidLookup
        DataField = 'EV_NIVEL11'
      end
      object EV_NIVEL12: TZetaDBKeyLookup_DevEx
        Left = 192
        Top = 264
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 11
        TabStop = True
        Visible = False
        WidthLlave = 60
        OnValidLookup = EV_NIVEL12ValidLookup
        DataField = 'EV_NIVEL12'
      end
    end
    object TabSalarios: TcxTabSheet
      Caption = 'Salarios'
      object CB_SALARIOlbl: TLabel
        Left = 75
        Top = 76
        Width = 99
        Height = 13
        Alignment = taRightJustify
        Caption = 'F'#243'rmula para Salario:'
      end
      object Label13: TLabel
        Left = 79
        Top = 127
        Width = 95
        Height = 13
        Alignment = taRightJustify
        Caption = 'Percepci'#243'n Fija # 1:'
      end
      object Label14: TLabel
        Left = 79
        Top = 150
        Width = 95
        Height = 13
        Alignment = taRightJustify
        Caption = 'Percepci'#243'n Fija # 2:'
      end
      object Label15: TLabel
        Left = 79
        Top = 173
        Width = 95
        Height = 13
        Alignment = taRightJustify
        Caption = 'Percepci'#243'n Fija # 3:'
      end
      object Label16: TLabel
        Left = 79
        Top = 195
        Width = 95
        Height = 13
        Alignment = taRightJustify
        Caption = 'Percepci'#243'n Fija # 4:'
      end
      object Label17: TLabel
        Left = 79
        Top = 218
        Width = 95
        Height = 13
        Alignment = taRightJustify
        Caption = 'Percepci'#243'n Fija # 5:'
      end
      object SBCO_FORMULA: TcxButton
        Left = 558
        Top = 51
        Width = 25
        Height = 25
        Hint = 'Constructor de F'#243'rmulas'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 8
        OnClick = SBCO_FORMULAClick
        OptionsImage.Glyph.Data = {
          7A080000424D7A08000000000000360000002800000017000000170000000100
          2000000000004408000000000000000000000000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFF000000000000000000000000D9AB81FFD8AA7FFF0000000000000000D8AA
          7FFFD8AA7FFF000000002E241B36D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000
          0000DCB38DFF0000000000000000D8AA7FFFDAAF87FF00000000000000000000
          00000000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD9AD83FF00000000DAAE85FFD8AA7FFFD8AA7FFF00000000000000000302
          0203D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFF00000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000
          000000000000D8AA7FFFD8AA7FFF010000010000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF000000000000
          0000D8AA7FFF0000000000000000D8AA7FFF0000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000DCB3
          8DFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFF0000000000000000000000000000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD9AD83FF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFDCB28BFF00000000D9AD83FF0000000000000000D8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFF00000000000000000000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
      end
      object EV_AUTOSAL: TDBCheckBox
        Left = 69
        Top = 32
        Width = 138
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Salario por Tabulador:'
        DataField = 'EV_AUTOSAL'
        DataSource = DataSource
        TabOrder = 1
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object EV_SALARIO: TcxDBMemo
        Left = 192
        Top = 50
        DataBinding.DataField = 'EV_SALARIO'
        DataBinding.DataSource = DataSource
        Properties.MaxLength = 255
        Properties.ScrollBars = ssVertical
        TabOrder = 2
        Height = 70
        Width = 364
      end
      object TB_OP1: TZetaDBKeyLookup_DevEx
        Left = 192
        Top = 123
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 50
        DataField = 'EV_OTRAS_1'
        DataSource = DataSource
      end
      object TB_OP2: TZetaDBKeyLookup_DevEx
        Left = 192
        Top = 146
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 50
        DataField = 'EV_OTRAS_2'
        DataSource = DataSource
      end
      object TB_OP3: TZetaDBKeyLookup_DevEx
        Left = 192
        Top = 169
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 50
        DataField = 'EV_OTRAS_3'
        DataSource = DataSource
      end
      object TB_OP4: TZetaDBKeyLookup_DevEx
        Left = 192
        Top = 191
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 50
        DataField = 'EV_OTRAS_4'
        DataSource = DataSource
      end
      object TB_OP5: TZetaDBKeyLookup_DevEx
        Left = 192
        Top = 214
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 7
        TabStop = True
        WidthLlave = 50
        DataField = 'EV_OTRAS_5'
        DataSource = DataSource
      end
      object EV_TABULA: TDBCheckBox
        Left = 69
        Top = 14
        Width = 138
        Height = 17
        Alignment = taLeftJustify
        Caption = '    Cambiar Tabulador:'
        DataField = 'EV_TABULA'
        DataSource = DataSource
        TabOrder = 0
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        OnClick = EV_TABULAClick
      end
    end
    object TabBaja: TcxTabSheet
      Caption = 'Baja'
      object EV_FEC_BSSLbl: TLabel
        Left = 139
        Top = 45
        Width = 77
        Height = 13
        Alignment = taRightJustify
        Caption = 'Baja ante IMSS:'
      end
      object EV_MOT_BAJLbl: TLabel
        Left = 142
        Top = 69
        Width = 74
        Height = 13
        Alignment = taRightJustify
        Caption = 'Motivo de Baja:'
      end
      object EV_BAJA: TDBCheckBox
        Left = 149
        Top = 19
        Width = 94
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Generar Baja:'
        DataField = 'EV_BAJA'
        DataSource = DataSource
        TabOrder = 0
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        OnClick = EV_BAJAClick
      end
      object EV_FEC_BSS: TZetaDBFecha
        Left = 230
        Top = 40
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 1
        Text = '23/dic/97'
        Valor = 35787.000000000000000000
        DataField = 'EV_FEC_BSS'
        DataSource = DataSource
      end
      object EV_MOT_BAJ: TZetaDBKeyLookup_DevEx
        Left = 230
        Top = 65
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        DataField = 'EV_MOT_BAJ'
        DataSource = DataSource
      end
      object GBUltimaNomina: TcxGroupBox
        Left = 139
        Top = 97
        Caption = 'Ultima N'#243'mina'
        TabOrder = 3
        Height = 110
        Width = 330
        object EV_NOMTIPOLbl: TLabel
          Left = 52
          Top = 44
          Width = 24
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo:'
        end
        object EV_NOMNUMELbl: TLabel
          Left = 36
          Top = 68
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
        end
        object EV_NOMYEARLbl: TLabel
          Left = 54
          Top = 19
          Width = 22
          Height = 13
          Alignment = taRightJustify
          Caption = 'A'#241'o:'
        end
        object EV_NOMTIPO: TZetaDBKeyCombo
          Left = 90
          Top = 40
          Width = 190
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 1
          ListaFija = lfTipoPeriodo
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = True
          DataField = 'EV_NOMTIPO'
          DataSource = DataSource
          LlaveNumerica = True
        end
        object EV_NOMYEAR: TDBEdit
          Left = 90
          Top = 15
          Width = 60
          Height = 21
          DataField = 'EV_NOMYEAR'
          DataSource = DataSource
          TabOrder = 0
        end
        object EV_NOMNUME: TZetaDBNumero
          Left = 90
          Top = 64
          Width = 105
          Height = 21
          Mascara = mnDias
          TabOrder = 2
          Text = '0'
          DataField = 'EV_NOMNUME'
          DataSource = DataSource
        end
      end
    end
    object TabReingreso: TcxTabSheet
      Caption = 'Reingreso'
      object EV_PATRONLbl: TLabel
        Left = 139
        Top = 51
        Width = 84
        Height = 13
        Caption = 'Registro Patronal:'
      end
      object EV_ALTA: TDBCheckBox
        Left = 129
        Top = 27
        Width = 118
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Generar Reingreso:'
        DataField = 'EV_ALTA'
        DataSource = DataSource
        TabOrder = 0
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        OnClick = EV_ALTAClick
      end
      object EV_PATRON: TZetaDBKeyLookup_DevEx
        Left = 233
        Top = 47
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        DataField = 'EV_PATRON'
        DataSource = DataSource
      end
      object EV_M_ANTIG: TDBCheckBox
        Left = 115
        Top = 71
        Width = 132
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Conservar antig'#252'edad:'
        DataField = 'EV_M_ANTIG'
        DataSource = DataSource
        TabOrder = 2
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        OnClick = EV_M_ANTIGClick
      end
      object RGTipoSaldo: TDBRadioGroup
        Left = 233
        Top = 112
        Width = 300
        Height = 41
        Columns = 2
        DataField = 'EV_M_TVAC'
        DataSource = DataSource
        Items.Strings = (
          'Saldar en Cero'
          'Saldar a la Baja')
        TabOrder = 4
        Values.Strings = (
          '0'
          '1')
      end
      object EV_M_SVAC: TDBCheckBox
        Left = 232
        Top = 90
        Width = 169
        Height = 17
        Caption = 'Saldar Vacaciones pendientes'
        DataField = 'EV_M_SVAC'
        DataSource = DataSource
        TabOrder = 3
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        OnClick = EV_M_ANTIGClick
      end
    end
    object TabGenerales: TcxTabSheet
      Caption = 'Adicionales'
      object Label18: TLabel
        Left = 144
        Top = 26
        Width = 91
        Height = 13
        Caption = 'Campo a Modificar:'
      end
      object Label19: TLabel
        Left = 186
        Top = 52
        Width = 49
        Height = 13
        Caption = 'Expresi'#243'n:'
      end
      object sbFormula: TcxButton
        Left = 430
        Top = 48
        Width = 25
        Height = 25
        Hint = 'Constructor de F'#243'rmulas'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = sbFormulaClick
        OptionsImage.Glyph.Data = {
          7A080000424D7A08000000000000360000002800000017000000170000000100
          2000000000004408000000000000000000000000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFF000000000000000000000000D9AB81FFD8AA7FFF0000000000000000D8AA
          7FFFD8AA7FFF000000002E241B36D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000
          0000DCB38DFF0000000000000000D8AA7FFFDAAF87FF00000000000000000000
          00000000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD9AD83FF00000000DAAE85FFD8AA7FFFD8AA7FFF00000000000000000302
          0203D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFF00000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000
          000000000000D8AA7FFFD8AA7FFF010000010000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF000000000000
          0000D8AA7FFF0000000000000000D8AA7FFF0000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000DCB3
          8DFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFF0000000000000000000000000000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD9AD83FF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFDCB28BFF00000000D9AD83FF0000000000000000D8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFF00000000000000000000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
        OptionsImage.Margin = 1
      end
      object EV_FORMULA: TcxDBMemo
        Left = 240
        Top = 48
        DataBinding.DataField = 'EV_FORMULA'
        DataBinding.DataSource = DataSource
        Properties.MaxLength = 255
        Properties.ScrollBars = ssVertical
        TabOrder = 1
        Height = 73
        Width = 185
      end
      object EV_CAMPO: TZetaDBKeyCombo
        Left = 240
        Top = 24
        Width = 185
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 0
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'EV_CAMPO'
        DataSource = DataSource
        LlaveNumerica = False
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 548
    Top = 57
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 3145936
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_BuscarBtn: TdxBarButton
      Visible = ivAlways
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
