inherited CatTurnos_DevEx: TCatTurnos_DevEx
  Left = 389
  Top = 389
  Caption = 'Turnos'
  ClientWidth = 506
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 506
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 180
      inherited textoValorActivo2: TLabel
        Width = 174
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 506
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object TU_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'TU_CODIGO'
      end
      object TU_DESCRIP: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'TU_DESCRIP'
      end
      object TU_DIAS: TcxGridDBColumn
        Caption = 'D'#237'as'
        DataBinding.FieldName = 'TU_DIAS'
      end
      object TU_JORNADA: TcxGridDBColumn
        Caption = 'Horas'
        DataBinding.FieldName = 'TU_JORNADA'
      end
      object TU_SUB_CTA: TcxGridDBColumn
        Caption = 'Subcuenta'
        DataBinding.FieldName = 'TU_SUB_CTA'
      end
      object TU_ACTIVO: TcxGridDBColumn
        Caption = 'Activo'
        DataBinding.FieldName = 'TU_ACTIVO'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
