unit FEditCatPrestaciones_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicionRenglon_DevEx, StdCtrls, DBCtrls, Mask, Db,
  Grids, DBGrids, ZetaDBGrid, ComCtrls,
  ExtCtrls, Buttons, ZetaNumero, ZetaEdit, ZetaCommonClasses, Menus,
  ZetaSmartLists, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxPC, cxNavigator, cxDBNavigator, cxButtons;

type
  TEditCatPrestaciones_DevEx = class(TBaseEdicionRenglon_DevEx)
    TB_CODIGO: TZetaDBEdit;
    TB_CODIGOlbl: TLabel;
    TB_ELEMENTlbl: TLabel;
    TB_ELEMENT: TDBEdit;
    TB_ACTIVO: TDBCheckBox;
    TabSheet1: TcxTabSheet;
    gbConfidencialidad: TGroupBox;
    listaConfidencialidad: TListBox;
    rbConfidenTodas: TRadioButton;
    rbConfidenAlgunas: TRadioButton;
    btSeleccionarConfiden_DevEx: TcxButton;
    GroupBox1: TGroupBox;
    TB_PRIMAlbl: TLabel;
    TB_DIAS_AGUIlbl: TLabel;
    TB_DOMINICALlbl: TLabel;
    Label1: TLabel;
    TB_PRIMAVA: TZetaDBNumero;
    TB_DIAS_AG: TZetaDBNumero;
    TB_PAGO_7: TDBCheckBox;
    TB_PRIMA_7: TDBCheckBox;
    TB_PRIMADO: TZetaDBNumero;
    TB_DIAS_AD: TZetaDBNumero;
    BBAplicaGenerales_DevEx: TcxButton;
    Label3: TLabel;
    TB_INGLES: TDBEdit;
    TB_TEXTO: TDBEdit;
    Label4: TLabel;
    Label5: TLabel;
    TB_NUMERO: TZetaDBNumero;
    GrupoGeneral: TGroupBox;
    //procedure BBAplicaGeneralesClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GridRenglonesDblClick(Sender: TObject);
    //procedure btSeleccionarConfidenClick(Sender: TObject);
    procedure rbConfidenTodasClick(Sender: TObject);
    procedure rbConfidenAlgunasClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure BBAplicaGenerales_DevExClick(Sender: TObject);
    procedure btSeleccionarConfiden_DevExClick(Sender: TObject);
    
  private   
    lValoresConfidencialidad : TStringList;
    procedure FillListaConfidencialidad;
    procedure SetListConfidencialidad( sValores : string );
    procedure GetConfidencialidad;
  protected
    procedure Connect;override;
    procedure DoLookup; override;
  public

  end;

var
  EditCatPrestaciones_DevEx: TEditCatPrestaciones_DevEx;

implementation

uses dCatalogos, FEditCatPresta_DevEx, ZAccesosTress, ZBaseEdicion_DevEx, ZetaBuscador_DevEx, ZetaCommonTools,
     FSeleccionarConfidencialidad_DevEx, DCliente, dSistema;

{$R *.DFM}

procedure TEditCatPrestaciones_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H66161_Agregar_renglon;
     IndexDerechos := ZAccesosTress.D_CAT_CONT_PRESTA;
     FirstControl := TB_CODIGO;   
     lValoresConfidencialidad := TStringList.Create;
end;

procedure TEditCatPrestaciones_DevEx.Connect;
begin  
     dmSistema.cdsNivel0.Conectar;
     with dmCatalogos do
     begin
          cdsSSocial.Conectar;
          Datasource.Dataset := cdsSSocial;

          cdsPrestaci.Conectar;
          dsRenglon.DataSet := cdsPrestaci;
     end;
end;

procedure TEditCatPrestaciones_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage := Tabla;
     FillListaConfidencialidad;
end;

procedure TEditCatPrestaciones_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', 'Prestaciones', 'TB_CODIGO', dmCatalogos.cdsSSocial );
end;

procedure TEditCatPrestaciones_DevEx.GridRenglonesDblClick(Sender: TObject);
begin
     inherited;
     ZBaseEdicion_DevEx.ShowFormaEdicion(EditCatPresta_DevEx, TEditCatPresta_DevEx );
end;

{procedure TEditCatPrestaciones_DevEx.BBAplicaGeneralesClick(Sender: TObject);
begin
     inherited;
     dmCatalogos.AplicaSSocial;
end; }

{procedure TEditCatPrestaciones_DevEx.btSeleccionarConfidenClick(Sender: TObject);
var
   selConfi : TSeleccionarConfidencialidad;
   sCM_NIVEL0Nuevo : string;
begin
  inherited;

  selConfi := TSeleccionarConfidencialidad.Create( Self );
  sCM_NIVEL0Nuevo := selConfi.GetConfidencialidad( DataSource.DataSet.FieldByName('TB_NIVEL0').AsString, lValoresConfidencialidad, True, dmCliente.Confidencialidad );

  if (  sCM_NIVEL0Nuevo <>  DataSource.DataSet.FieldByName('TB_NIVEL0').AsString ) then
  begin
     with DataSource.DataSet do
     begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

           FieldByName('TB_NIVEL0').AsString  := sCM_NIVEL0Nuevo;
           GetConfidencialidad;
     end;
  end;

  FreeAndNil ( selConfi ) ;
end; }

procedure TEditCatPrestaciones_DevEx.FillListaConfidencialidad;
begin
     with dmSistema.cdsNivel0 do
     begin
          First;
          lValoresConfidencialidad.Clear;
          lValoresConfidencialidad.NameValueSeparator := '=' ;
          While( not Eof )do
          begin
               lValoresConfidencialidad.Add(FieldByName('TB_CODIGO').AsString +'='+ FieldByName('TB_ELEMENT').AsString);
               Next;
          end;
     end;
end;

procedure TEditCatPrestaciones_DevEx.GetConfidencialidad;
begin
   if (  DataSource.DataSet  <> nil ) then
   with DataSource.DataSet do
   begin
        SetListConfidencialidad(  FieldByName('TB_NIVEL0').AsString );
   end;
end;

procedure TEditCatPrestaciones_DevEx.SetListConfidencialidad(sValores: string);
var
   Lista : TStringList;
   i, j : integer;
begin
    Lista := TStringList.Create;

    sValores := Trim( sValores );
    if StrLleno( sValores ) then
        Lista.CommaText  := sValores;

    listaConfidencialidad.Items.Clear;

    rbConfidenTodas.OnClick := nil;
    rbConfidenAlgunas.OnClick := nil;
    rbConfidenTodas.Checked := False;
    rbConfidenAlgunas.Checked := False;
                                                 
    if lValoresConfidencialidad.Count = 0 then
       FillListaConfidencialidad;

    for i:= 0 to lValoresConfidencialidad.Count - 1 do
    begin
        if ( Lista.Count > 0 ) then
        begin
                j := Lista.IndexOf( Trim( lValoresConfidencialidad.Names[i] )  );
                if ( j >= 0 ) then
                   listaConfidencialidad.Items.Add( lValoresConfidencialidad[i] )
        end
    end;

    rbConfidenTodas.Checked :=   listaConfidencialidad.Items.Count = 0;
    rbConfidenAlgunas.Checked := not rbConfidenTodas.Checked;

    if ( gbConfidencialidad.Enabled ) then
    begin
        listaConfidencialidad.Enabled := rbConfidenAlgunas.Checked;
        btSeleccionarConfiden_DevEx.Enabled := rbConfidenAlgunas.Checked;
    end;

    rbConfidenTodas.OnClick := rbConfidenTodasClick;
    rbConfidenAlgunas.OnClick := rbConfidenAlgunasClick;

    FreeAndNil( Lista );

end;

procedure TEditCatPrestaciones_DevEx.rbConfidenTodasClick(Sender: TObject);
begin
  inherited;

   with DataSource.DataSet do
   begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

        FieldByName('TB_NIVEL0').AsString  :=  '';
   end;
   GetConfidencialidad;
end;

procedure TEditCatPrestaciones_DevEx.rbConfidenAlgunasClick(Sender: TObject);
begin
  inherited;
     if ( rbConfidenAlgunas.Checked ) then
     begin
        btSeleccionarConfiden_DevExClick( Sender );
        GetConfidencialidad;
     end;

end;

procedure TEditCatPrestaciones_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
     if ( Field = nil ) then
     begin
          with DataSource.DataSet do
          begin
               if ( State in [dsBrowse] ) then
                  GetConfidencialidad
               else if (State in [dsInsert]) then
                    rbConfidenTodas.Checked := TRUE;
          end;

     end;
end;

procedure TEditCatPrestaciones_DevEx.BBAplicaGenerales_DevExClick(
  Sender: TObject);
begin
  inherited;
     dmCatalogos.AplicaSSocial;
end;

procedure TEditCatPrestaciones_DevEx.btSeleccionarConfiden_DevExClick(
  Sender: TObject);
var
   selConfi : TSeleccionarConfidencialidad_DevEx;
   sCM_NIVEL0Nuevo : string;
begin
  inherited;

  selConfi := TSeleccionarConfidencialidad_DevEx.Create( Self );
  sCM_NIVEL0Nuevo := selConfi.GetConfidencialidad( DataSource.DataSet.FieldByName('TB_NIVEL0').AsString, lValoresConfidencialidad, True, dmCliente.Confidencialidad );

  if (  sCM_NIVEL0Nuevo <>  DataSource.DataSet.FieldByName('TB_NIVEL0').AsString ) then
  begin
     with DataSource.DataSet do
     begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

           FieldByName('TB_NIVEL0').AsString  := sCM_NIVEL0Nuevo;
           GetConfidencialidad;
     end;
  end;

  FreeAndNil ( selConfi ) ;
end;

end.
