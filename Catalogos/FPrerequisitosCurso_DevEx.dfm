inherited PrerequisitosCurso_DevEx: TPrerequisitosCurso_DevEx
  Left = 395
  Top = 309
  Caption = 'Prerrequisitos por Curso'
  ClientWidth = 713
  ExplicitWidth = 713
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 713
    ExplicitWidth = 713
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 454
      ExplicitWidth = 454
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 448
        ExplicitLeft = 3
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 59
    Width = 713
    Height = 165
    ExplicitTop = 59
    ExplicitWidth = 713
    ExplicitHeight = 165
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CP_CURSO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CP_CURSO'
        MinWidth = 80
      end
      object CU_NOMBRE: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'CU_NOMBRE'
        MinWidth = 100
        Width = 250
      end
      object CP_OPCIONA: TcxGridDBColumn
        Caption = 'Opcional'
        DataBinding.FieldName = 'CP_OPCIONA'
        MinWidth = 80
        Width = 80
      end
      object CP_COMENTA: TcxGridDBColumn
        Caption = 'Comentario'
        DataBinding.FieldName = 'CP_COMENTA'
        MinWidth = 100
        Width = 250
      end
    end
    object ZetaDBGridLevel1: TcxGridLevel
    end
  end
  object pnCurso: TPanel [2]
    Left = 0
    Top = 19
    Width = 713
    Height = 40
    Align = alTop
    TabOrder = 2
    object lblCurso: TLabel
      Left = 23
      Top = 13
      Width = 30
      Height = 13
      Alignment = taRightJustify
      Caption = 'Curso:'
    end
    object CU_CODIGO: TZetaKeyLookup_DevEx
      Left = 58
      Top = 9
      Width = 367
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 70
      OnValidKey = CU_CODIGOValidKey
    end
  end
  inherited DataSource: TDataSource
    Left = 630
    Top = 60
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
