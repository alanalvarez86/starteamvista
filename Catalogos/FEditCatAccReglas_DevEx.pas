unit FEditCatAccReglas_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls, Buttons, DBCtrls, StdCtrls, ComCtrls,
  ZetaNumero, Mask, ZetaTipoEntidad, ZetaKeyCombo,
  ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, ZetaKeyLookup_DevEx, dxSkinscxPCPainter, cxPCdxBarPopupMenu,
  cxPC, cxContainer, cxEdit, cxTextEdit, cxMemo, cxDBEdit, cxCheckBox;

type
  TEditCatAccReglas_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    PageControl: TcxPageControl;
    TabGenerales: TcxTabSheet;
    TabSheet3: TcxTabSheet;
    TabSheet4: TcxTabSheet;
    Label1: TLabel;
    Label3: TLabel;
    AE_ACTIVO: TDBCheckBox;
    AE_LETRERO: TDBEdit;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    AE_FORMULA: TcxDBMemo;
    btnFormula: TcxButton;
    Label7: TLabel;
    AE_FILTRO: TcxDBMemo;
    FiltroBtn: TcxButton;
    QU_CODIGO: TZetaDBKeyLookup_DevEx;
    Label8: TLabel;
    AE_CODIGO: TZetaDBNumero;
    EA_TIPO: TZetaDBKeyCombo;
    AE_SEVERI: TZetaDBKeyCombo;
    procedure FormCreate(Sender: TObject);
    procedure btnFormulaClick(Sender: TObject);
    procedure FiltroBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);

  private
  protected
    procedure Connect; override;
    procedure DoLookup; override;
  public
  end;

var
  EditCatAccReglas_DevEx: TEditCatAccReglas_DevEx;

implementation

uses dCatalogos, dSistema, ZAccesosTress, ZConstruyeFormula, ZetaCommonLists,
     ZetaCommonClasses, ZetaBuscaEntero_DevEx;

{$R *.DFM}

procedure TEditCatAccReglas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_ACCESOS_REGLAS;
     HelpContext:= H_REGLAS_CASETA;
     FirstControl := AE_CODIGO;
     QU_CODIGO.LookupDataset := dmCatalogos.cdsCondiciones;
end;

procedure TEditCatAccReglas_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     dmCatalogos.cdsCondiciones.Conectar;
     Datasource.Dataset := dmCatalogos.cdsAccReglas;
end;

procedure TEditCatAccReglas_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscaEntero_DevEx.BuscarCodigo( 'N�mero', 'Regla de Caseta', 'AE_CODIGO', dmCatalogos.cdsAccReglas );
end;

procedure TEditCatAccReglas_DevEx.btnFormulaClick(Sender: TObject);
begin
     inherited;
     with dmCatalogos.cdsAccReglas do
     begin
          if not ( Editing or Inserting ) then
             Edit;
          FieldByName( 'AE_FORMULA').AsString := GetFormulaConst( enEmpleado, AE_FORMULA.Lines.Text, AE_FORMULA.SelStart, evBase );
     end;
end;

procedure TEditCatAccReglas_DevEx.FiltroBtnClick(Sender: TObject);
begin
     inherited;
     with dmCatalogos.cdsAccReglas do
     begin
          if not ( Editing or Inserting ) then
             Edit;
          FieldByName( 'AE_FILTRO' ).AsString := GetFormulaConst( enEmpleado , AE_FILTRO.Lines.Text, AE_FILTRO.SelStart, evBase );
     end;
end;

procedure TEditCatAccReglas_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  PageControl.ActivePage := TabGenerales;
end;

end.
