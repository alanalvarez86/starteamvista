unit FEditCatFolios_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls, Buttons, DBCtrls, StdCtrls, Mask, dbclient,
  ZetaNumero, ComCtrls, ZetaEdit, Grids, ZetaCommonClasses,
  DBGrids, ZetaDBGrid, ZReportTools, ZetaTipoEntidad, ZBaseEdicion_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxSkinscxPCPainter, cxPCdxBarPopupMenu,
  cxPC, dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator,
  cxDBNavigator, cxButtons, ZetaSmartLists_DevEx, ZetaKeyLookup_DevEx,
  cxContainer, cxEdit, cxListBox;

type
  TEditCatFolios_DevEx = class(TBaseEdicion_DevEx)
    PageControl: TcxPageControl;
    tsCriterio: TcxTabSheet;
    tsOrden: TcxTabSheet;
    Label1: TLabel;
    FL_CODIGO: TZetaDBNumero;
    FL_DESCRIP: TDBEdit;
    FL_MONTO: TDBMemo;
    Label3: TLabel;
    Label2: TLabel;
    FL_CEROS: TDBCheckBox;
    FL_REPITE: TDBCheckBox;
    FL_MONEDALbl: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    FL_FINAL: TZetaDBNumero;
    FL_INICIAL: TZetaDBNumero;
    FL_MONEDA: TZetaDBNumero;
    FL_MONTOBtn: TcxButton;
    tsFiltros: TcxTabSheet;
    sCondicion: TZetaDBKeyLookup_DevEx;
    lbCondicion: TLabel;
    lbFiltroUsuario: TLabel;
    bConstructorFiltros: TcxButton;
    FL_FILTRO: TDBMemo;
    GroupBox2: TGroupBox;
    LbOrden: TZetaSmartListBox_DevEx;
    BAgregaOrden: TcxButton;
    BBorraOrden: TcxButton;
    BArribaOrden: TZetaSmartListsButton_DevEx;
    BAbajoOrden: TZetaSmartListsButton_DevEx;
    EFormulaOrden: TEdit;
    RGOrden: TRadioGroup;
    SmartListOrden: TZetaSmartLists_DevEx;
    procedure FL_REPITEClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FL_MONTOBtnClick(Sender: TObject);
    procedure bConstructorFiltrosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RGOrdenClick(Sender: TObject);
    procedure BAgregaOrdenClick(Sender: TObject);
    procedure BBorraOrdenClick(Sender: TObject);
    procedure AgregarBtnClick(Sender: TObject);
    procedure SmartListOrdenAlSeleccionar(Sender: TObject; var Objeto: TObject; Texto: String);
    procedure SmartListOrdenAlSubir(Sender: TObject; var Objeto: TObject; Texto: String);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure OKClick(Sender: TObject);
    procedure DataSourceStateChange(Sender: TObject);
    procedure LbOrdenDblClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ModificarBtnClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    procedure ActualizaOrdenObjeto;
    procedure HayCambio;
    procedure ActualizaOrden( oOrden : TOrdenOpciones );
    procedure EnabledOrden( bEnable : Boolean );
    procedure AsignaProcOrden( oProc : TNotifyEvent );
    procedure CargaListaOrden;
    procedure LimpiaOrdenLista;
    procedure AgregaOrden;
    procedure Refresh;
  protected
    procedure Connect; override;
    procedure EscribirCambios; override;
  public
  end;

var
  EditCatFolios_DevEx: TEditCatFolios_DevEx;

implementation


uses dCatalogos, ZetaCommonTools, ZetaCommonLists, ZAccesosTress, ZConstruyeFormula;

{$R *.DFM}

procedure TEditCatFolios_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_NOMINA_FOLIOS;
     FirstControl := FL_CODIGO;
     HelpContext:= H60624_Folios_recibos;
     sCondicion.LookupDataset := dmCatalogos.cdsCondiciones;
end;

procedure TEditCatFolios_DevEx.FormShow(Sender: TObject);
begin
     PageControl.ActivePage := tsCriterio;
     inherited;
end;

procedure TEditCatFolios_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsCondiciones.Conectar;
          cdsFolios.Conectar;
          DataSource.DataSet:= cdsFolios;
     end;
     FL_REPITEClick( Self );
     CargaListaOrden;
end;

procedure TEditCatFolios_DevEx.FormDestroy(Sender: TObject);
begin
     with LBOrden do
          while Items.Count > 0 do
          begin
                TOrdenOpciones(Items.Objects[0]).Free;
                Items.Delete(0);
          end;
     inherited;
end;

procedure TEditCatFolios_DevEx.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     inherited;
     LimpiaOrdenLista;
end;

procedure TEditCatFolios_DevEx.FL_MONTOBtnClick(Sender: TObject);
begin
     inherited;
     with DataSource.DataSet do
     begin
          if not ( Editing or Inserting ) then
             Edit;
          FieldByName( 'FL_MONTO' ).AsString := GetFormulaConst( enNomina , FL_MONTO.Lines.Text, FL_MONTO.SelStart, evBase );
     end;
end;

procedure TEditCatFolios_DevEx.bConstructorFiltrosClick(Sender: TObject);
begin
     inherited;
     with DataSource.DataSet do
     begin
          if not ( Editing or Inserting ) then
             Edit;
          FieldByName( 'FL_FILTRO').AsString := GetFormulaConst( enNomina , FL_FILTRO.Lines.Text, FL_FILTRO.SelStart, evBase );
     end;
end;

procedure TEditCatFolios_DevEx.FL_REPITEClick(Sender: TObject);
begin
     inherited;
     // Habilita o Deshabilita captura de moneda a repetir
     FL_MONEDA.Enabled    := FL_REPITE.Checked;
     FL_MONEDALbl.Enabled := FL_REPITE.Checked;
end;

procedure TEditCatFolios_DevEx.RGOrdenClick(Sender: TObject);
begin
     inherited;
     ActualizaOrdenObjeto;
end;

procedure TEditCatFolios_DevEx.ActualizaOrdenObjeto;
var
   Objeto : TOrdenOpciones;
begin
     Objeto := TOrdenOpciones( TListaObjetos( LBOrden.Items ).Objects[ LBOrden.ItemIndex ] );
     if Assigned( Objeto ) then
     begin
          Objeto.DireccionOrden := RGOrden.ItemIndex;
          HayCambio;
     end;
end;

procedure TEditCatFolios_DevEx.HayCambio;
begin
     if not Editing or Inserting then
        Modificar;
end;

procedure TEditCatFolios_DevEx.CargaListaOrden;
begin
     dmCatalogos.CargaOrdenFolio( TListaObjetos( LBOrden.Items ) );
     SmartListOrden.SelectEscogido( 0 );
     EnabledOrden( LbOrden.Items.Count > 0 )
end;

procedure TEditCatFolios_DevEx.AgregaOrden;
 var oOrden : TOrdenOpciones;
     oDiccion : TDiccionRecord;
begin
     if AntesDeAgregarDato( enNomina, FALSE, FALSE, oDiccion, SmartListOrden ) then
     begin
          oOrden := TOrdenOpciones.Create;
          DiccionToCampoMaster(oDiccion,oOrden);
          DespuesDeAgregarDato( oOrden,
                                SmartListOrden,
                                BBorraOrden,
                                BAgregaOrden );
          Modo := dsEdit;
     end;
end;

procedure TEditCatFolios_DevEx.BAgregaOrdenClick(Sender: TObject);
begin
     inherited;
     AgregaOrden;
end;

procedure TEditCatFolios_DevEx.BBorraOrdenClick(Sender: TObject);
var
   i : integer;
begin
     inherited;
     with SmartListOrden do
     begin
          i := ListaDisponibles.ItemIndex;
          if i >= 0 then
          begin
               ListaDisponibles.Items.Objects[ i ].Free;
               ListaDisponibles.Items.Delete( i );
          end;

          BBorraOrden.Enabled := ListaDisponibles.Items.Count > 0;

          if BBorraOrden.Enabled then
          begin
               if i >= ListaDisponibles.Items.Count then i := ListaDisponibles.Items.Count - 1;
               SelectEscogido( i );
               if RgOrden.Enabled then RgOrden.SetFocus
               else ListaDisponibles.SetFocus;
          end
          else BAgregaOrden.SetFocus;
     end;
     EnabledOrden( LBOrden.Items.Count > 0 );
     Modo := dsEdit;
end;

procedure TEditCatFolios_DevEx.LimpiaOrdenLista;
 var i : integer;
begin
     with LBOrden.Items do
     begin
          for i := 0 to LBOrden.Items.Count - 1 do
              Objects[ i ].Free;
     end;
     LBOrden.Clear;
end;

procedure TEditCatFolios_DevEx.AgregarBtnClick(Sender: TObject);
begin
     LimpiaOrdenLista;
     PageControl.ActivePage := tsCriterio;
     inherited;
end;

procedure TEditCatFolios_DevEx.EnabledOrden( bEnable : Boolean );
begin
     RGOrden.Enabled := bEnable;
     if NOT bEnable then EFormulaOrden.Text := '';
     BBorraOrden.Enabled := bEnable;
end;

procedure TEditCatFolios_DevEx.AsignaProcOrden( oProc : TNotifyEvent );
begin
     RgOrden.OnClick := oProc;
end;

procedure TEditCatFolios_DevEx.SmartListOrdenAlSubir(Sender: TObject;
  var Objeto: TObject; Texto: String);
begin
     inherited;
     HayCambio;
end;

procedure TEditCatFolios_DevEx.SmartListOrdenAlSeleccionar(Sender: TObject;
  var Objeto: TObject; Texto: String);
begin
     inherited;
     if Objeto <> NIL then
     begin
          ActualizaOrden( TOrdenOpciones( Objeto ) );
          SmartListOrden.Control := RGOrden;
     end
     else EnabledOrden( False );
end;


procedure TEditCatFolios_DevEx.OKClick(Sender: TObject);
begin
     with dmCatalogos do
     begin
          if ( cdsFolios.State in [ dsEdit, dsInsert ] ) then
             cdsFolios.Post;
          AgregaOrdenFolio( FL_CODIGO.ValorEntero, TListaObjetos( LBOrden.Items ) );
     end;
     inherited;
     if TClientDataSet( DataSource.DataSet ).ChangeCount = 0 then    //Si se aplicaron los cambios
        Close;
end;

procedure TEditCatFolios_DevEx.ActualizaOrden(oOrden: TOrdenOpciones);
begin
     AsignaProcOrden( nil );
     EFormulaOrden.Text := oOrden.Formula;
     RGOrden.ItemIndex := oOrden.DireccionOrden;
     AsignaProcOrden( RGOrdenClick );
     EnabledOrden( True );
end;

procedure TEditCatFolios_DevEx.ModificarBtnClick(Sender: TObject);
begin
     PageControl.ActivePage := tsCriterio;
     inherited;
end;

procedure TEditCatFolios_DevEx.DataSourceStateChange(Sender: TObject);
begin
     inherited;
     { Si no se activa este evento no se habilitan adecuadamente los botones OK-CANCELAR ( ? )}
end;

procedure TEditCatFolios_DevEx.LbOrdenDblClick(Sender: TObject);
begin
     inherited;
     //No hace nada - No quitar!!!!
end;

procedure TEditCatFolios_DevEx.EscribirCambios;
begin
     inherited;
     Refresh;
end;

procedure TEditCatFolios_DevEx.Refresh;
begin
     dmCatalogos.cdsFolios.Refrescar;
end;

procedure TEditCatFolios_DevEx.OK_DevExClick(Sender: TObject);
begin
   with dmCatalogos do
     begin
          if ( cdsFolios.State in [ dsEdit, dsInsert ] ) then
             cdsFolios.Post;
          AgregaOrdenFolio( FL_CODIGO.ValorEntero, TListaObjetos( LBOrden.Items ) );
     end;
     inherited;
     if TClientDataSet( DataSource.DataSet ).ChangeCount = 0 then    //Si se aplicaron los cambios
        Modo := dsBrowse;
        //Close;

end;

end.

