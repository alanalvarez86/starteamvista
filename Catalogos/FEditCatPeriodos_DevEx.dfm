inherited EditCatPeriodos_DevEx: TEditCatPeriodos_DevEx
  Left = 277
  Top = 187
  Caption = 'Per'#237'odos'
  ClientHeight = 531
  ClientWidth = 576
  ExplicitWidth = 582
  ExplicitHeight = 560
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 495
    Width = 576
    TabOrder = 2
    ExplicitTop = 495
    ExplicitWidth = 576
    inherited OK_DevEx: TcxButton
      Left = 410
      ExplicitLeft = 410
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 489
      ExplicitLeft = 489
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 576
    TabOrder = 0
    ExplicitWidth = 576
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 317
      end
    end
    inherited ValorActivo2: TPanel
      Width = 250
      ExplicitWidth = 250
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 244
        ExplicitLeft = 164
      end
    end
  end
  object Panel2: TPanel [3]
    Left = 0
    Top = 50
    Width = 576
    Height = 445
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object gbTotales: TGroupBox
      Left = 7
      Top = 242
      Width = 263
      Height = 108
      Caption = ' Totales '
      Ctl3D = True
      ParentCtl3D = False
      TabOrder = 3
      object lblEmpleados: TLabel
        Left = 54
        Top = 79
        Width = 55
        Height = 13
        Alignment = taRightJustify
        Caption = 'Empleados:'
      end
      object PE_NUM_EMP: TZetaDBTextBox
        Left = 113
        Top = 79
        Width = 85
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'PE_NUM_EMP'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'PE_NUM_EMP'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object lblPercepciones: TLabel
        Left = 41
        Top = 20
        Width = 68
        Height = 13
        Alignment = taRightJustify
        Caption = 'Percepciones:'
      end
      object PE_TOT_PER: TZetaDBTextBox
        Left = 113
        Top = 19
        Width = 85
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'PE_TOT_PER'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'PE_TOT_PER'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object lblDeducciones: TLabel
        Left = 43
        Top = 40
        Width = 66
        Height = 13
        Alignment = taRightJustify
        Caption = 'Deducciones:'
      end
      object PE_TOT_DED: TZetaDBTextBox
        Left = 113
        Top = 39
        Width = 85
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'PE_TOT_DED'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'PE_TOT_DED'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object lblNetoAPagar: TLabel
        Left = 43
        Top = 59
        Width = 66
        Height = 13
        Alignment = taRightJustify
        Caption = 'Neto a Pagar:'
      end
      object PE_TOT_NET: TZetaDBTextBox
        Left = 113
        Top = 59
        Width = 85
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'PE_TOT_NET'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'PE_TOT_NET'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
    object Panel1: TPanel
      Left = 7
      Top = 5
      Width = 555
      Height = 136
      TabOrder = 0
      object lblDescripcion: TLabel
        Left = 25
        Top = 107
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descripci'#243'n:'
      end
      object lblNumero: TLabel
        Left = 44
        Top = 57
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'N'#250'mero:'
      end
      object lblAnio: TLabel
        Left = 62
        Top = 11
        Width = 22
        Height = 13
        Alignment = taRightJustify
        Caption = 'A'#241'o:'
      end
      object PE_YEAR: TZetaDBTextBox
        Left = 88
        Top = 9
        Width = 67
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'PE_YEAR'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'PE_YEAR'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object lblTipo: TLabel
        Left = 60
        Top = 33
        Width = 24
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo:'
      end
      object lblUsoNomina: TLabel
        Left = 8
        Top = 82
        Width = 76
        Height = 13
        Caption = 'Uso de N'#243'mina:'
      end
      object PE_TIPO: TZetaDBTextBox
        Left = 88
        Top = 31
        Width = 104
        Height = 17
        AutoSize = False
        Caption = 'PE_TIPO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'PE_TIPO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object PE_DESCRIP: TDBEdit
        Left = 88
        Top = 105
        Width = 252
        Height = 21
        DataField = 'PE_DESCRIP'
        DataSource = DataSource
        TabOrder = 2
      end
      object PE_NUMERO: TZetaDBNumero
        Left = 88
        Top = 53
        Width = 67
        Height = 21
        Mascara = mnDias
        TabOrder = 0
        Text = '0'
        ConfirmEdit = True
        ConfirmMsg = #191' Desea Cambiar Este N'#250'mero ?'
        DataField = 'PE_NUMERO'
        DataSource = DataSource
      end
      object PE_USO: TZetaDBKeyCombo
        Left = 88
        Top = 79
        Width = 80
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        Enabled = False
        ParentCtl3D = False
        TabOrder = 1
        ListaFija = lfUsoPeriodo
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'PE_USO'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object gbAsistencia: TGroupBox
        Left = 387
        Top = 4
        Width = 153
        Height = 70
        Caption = ' Fechas de asistencia '
        TabOrder = 4
        object Label24: TLabel
          Left = 11
          Top = 20
          Width = 19
          Height = 13
          Alignment = taRightJustify
          Caption = 'Del:'
        end
        object Label25: TLabel
          Left = 19
          Top = 44
          Width = 11
          Height = 13
          Alignment = taRightJustify
          Caption = 'al:'
        end
        object PE_ASI_INI: TZetaDBFecha
          Left = 32
          Top = 15
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 0
          Text = '23/Dec/97'
          Valor = 35787.000000000000000000
          OnChange = FECHASChange
          DataField = 'PE_ASI_INI'
          DataSource = DataSource
        end
        object PE_ASI_FIN: TZetaDBFecha
          Left = 32
          Top = 39
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 1
          Text = '23/Dec/97'
          Valor = 35787.000000000000000000
          OnChange = FECHASChange
          DataField = 'PE_ASI_FIN'
          DataSource = DataSource
        end
      end
      object gbPeriodo: TGroupBox
        Left = 210
        Top = 4
        Width = 161
        Height = 91
        Caption = 'Fechas del per'#237'odo '
        TabOrder = 3
        object Label8: TLabel
          Left = 19
          Top = 19
          Width = 19
          Height = 13
          Alignment = taRightJustify
          Caption = 'Del:'
        end
        object Label7: TLabel
          Left = 27
          Top = 43
          Width = 11
          Height = 13
          Alignment = taRightJustify
          Caption = 'al:'
        end
        object Label18: TLabel
          Left = 10
          Top = 67
          Width = 28
          Height = 13
          Alignment = taRightJustify
          Caption = 'Pago:'
        end
        object PE_FEC_INI: TZetaDBFecha
          Left = 40
          Top = 15
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 0
          Text = '23/Dec/97'
          Valor = 35787.000000000000000000
          OnChange = FECHASChange
          DataField = 'PE_FEC_INI'
          DataSource = DataSource
        end
        object PE_FEC_FIN: TZetaDBFecha
          Left = 40
          Top = 39
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 1
          Text = '23/Dec/97'
          Valor = 35787.000000000000000000
          OnChange = FECHASChange
          DataField = 'PE_FEC_FIN'
          DataSource = DataSource
        end
        object PE_FEC_PAG: TZetaDBFecha
          Left = 40
          Top = 63
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 2
          Text = '23/Dec/97'
          Valor = 35787.000000000000000000
          DataField = 'PE_FEC_PAG'
          DataSource = DataSource
        end
      end
    end
    object gbAcumulados: TGroupBox
      Left = 282
      Top = 242
      Width = 280
      Height = 107
      Caption = ' Acumulados '
      TabOrder = 4
      object lblPeriodoMes: TLabel
        Left = 13
        Top = 48
        Width = 90
        Height = 13
        Alignment = taRightJustify
        Caption = 'Per'#237'odo en el Mes:'
      end
      object lblPeriodoMesDe: TLabel
        Left = 200
        Top = 48
        Width = 15
        Height = 13
        Alignment = taRightJustify
        Caption = 'de:'
      end
      object lblPeriodoAnio: TLabel
        Left = 14
        Top = 72
        Width = 89
        Height = 13
        Alignment = taRightJustify
        Caption = 'Per'#237'odo en el A'#241'o:'
      end
      object lblAcumula: TLabel
        Left = 50
        Top = 23
        Width = 53
        Height = 13
        Alignment = taRightJustify
        Caption = 'Acumula a:'
      end
      object ZNumero: TZetaDBTextBox
        Left = 106
        Top = 71
        Width = 45
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'ZNumero'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'PE_NUMERO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object lblPeriodoAnioDe: TLabel
        Left = 200
        Top = 72
        Width = 15
        Height = 13
        Alignment = taRightJustify
        Caption = 'de:'
      end
      object PE_POS_MES: TZetaDBNumero
        Left = 106
        Top = 45
        Width = 45
        Height = 21
        Mascara = mnDias
        TabOrder = 1
        Text = '0'
        UseEnterKey = True
        DataField = 'PE_POS_MES'
        DataSource = DataSource
      end
      object PE_PER_TOT: TZetaDBNumero
        Left = 222
        Top = 71
        Width = 45
        Height = 21
        Mascara = mnDias
        TabOrder = 3
        Text = '0'
        UseEnterKey = True
        DataField = 'PE_PER_TOT'
        DataSource = DataSource
      end
      object PE_PER_MES: TZetaDBNumero
        Left = 222
        Top = 45
        Width = 45
        Height = 21
        Mascara = mnDias
        TabOrder = 2
        Text = '0'
        UseEnterKey = True
        DataField = 'PE_PER_MES'
        DataSource = DataSource
      end
      object PE_MES: TZetaDBKeyCombo
        Left = 106
        Top = 19
        Width = 161
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 0
        OnChange = PE_MESChange
        ListaFija = lfMes13
        ListaVariable = lvPuesto
        Offset = 1
        Opcional = False
        EsconderVacios = False
        DataField = 'PE_MES'
        DataSource = DataSource
        LlaveNumerica = True
      end
    end
    object gbDias: TGroupBox
      Left = 7
      Top = 355
      Width = 263
      Height = 87
      Caption = ' D'#237'as '
      TabOrder = 5
      object lblEstaNomina: TLabel
        Left = 45
        Top = 16
        Width = 63
        Height = 13
        Alignment = taRightJustify
        Caption = 'Esta N'#243'mina:'
      end
      object lblAcumuladoMes: TLabel
        Left = 9
        Top = 41
        Width = 99
        Height = 13
        Alignment = taRightJustify
        Caption = 'Acumulados en Mes:'
      end
      object lblAcumuladoMesDe: TLabel
        Left = 170
        Top = 41
        Width = 15
        Height = 13
        Alignment = taRightJustify
        Caption = 'de:'
      end
      object PE_DIAS: TZetaDBNumero
        Left = 113
        Top = 12
        Width = 50
        Height = 21
        Mascara = mnDias
        TabOrder = 0
        Text = '0'
        UseEnterKey = True
        DataField = 'PE_DIAS'
        DataSource = DataSource
      end
      object PE_DIAS_AC: TZetaDBNumero
        Left = 113
        Top = 38
        Width = 50
        Height = 21
        Mascara = mnDias
        TabOrder = 1
        Text = '0'
        UseEnterKey = True
        DataField = 'PE_DIAS_AC'
        DataSource = DataSource
      end
      object PE_DIA_MES: TZetaDBNumero
        Left = 195
        Top = 38
        Width = 50
        Height = 21
        Mascara = mnDias
        TabOrder = 2
        Text = '0'
        UseEnterKey = True
        DataField = 'PE_DIA_MES'
        DataSource = DataSource
      end
      object PE_CAL: TDBCheckBox
        Left = 55
        Top = 63
        Width = 72
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Calendario:'
        DataField = 'PE_CAL'
        DataSource = DataSource
        TabOrder = 3
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
    end
    object gbOpciones: TGroupBox
      Left = 282
      Top = 147
      Width = 280
      Height = 89
      Caption = ' Opciones '
      ParentShowHint = False
      ShowHint = False
      TabOrder = 2
      object PE_AHORRO: TDBCheckBox
        Left = 85
        Top = 14
        Width = 111
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Descontar Ahorros:'
        DataField = 'PE_AHORRO'
        DataSource = DataSource
        TabOrder = 0
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object PE_PRESTAM: TDBCheckBox
        Left = 72
        Top = 30
        Width = 124
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Descontar Pr'#233'stamos:'
        DataField = 'PE_PRESTAM'
        DataSource = DataSource
        TabOrder = 1
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object PE_SOLO_EX: TDBCheckBox
        Left = 88
        Top = 46
        Width = 108
        Height = 17
        Alignment = taLeftJustify
        Caption = 'S'#243'lo Excepciones:'
        DataField = 'PE_SOLO_EX'
        DataSource = DataSource
        TabOrder = 2
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object PE_INC_BAJ: TDBCheckBox
        Left = 116
        Top = 62
        Width = 80
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Incluir Bajas:'
        DataField = 'PE_INC_BAJ'
        DataSource = DataSource
        TabOrder = 3
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
    end
    object gbEstatus: TGroupBox
      Left = 7
      Top = 147
      Width = 263
      Height = 89
      Caption = ' Status '
      TabOrder = 1
      object Label10: TLabel
        Left = 50
        Top = 42
        Width = 55
        Height = 13
        Alignment = taRightJustify
        Caption = 'Modificado:'
      end
      object Label4: TLabel
        Left = 72
        Top = 21
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Status:'
      end
      object Label9: TLabel
        Left = 62
        Top = 64
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Modific'#243':'
      end
      object PE_FEC_MOD: TZetaDBTextBox
        Left = 112
        Top = 41
        Width = 130
        Height = 17
        AutoSize = False
        Caption = 'PE_FEC_MOD'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'PE_FEC_MOD'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object PE_STATUS: TZetaDBTextBox
        Left = 112
        Top = 19
        Width = 130
        Height = 17
        AutoSize = False
        Caption = 'PE_STATUS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'PE_STATUS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object US_CODIGO: TZetaDBTextBox
        Left = 112
        Top = 63
        Width = 130
        Height = 17
        AutoSize = False
        Caption = 'US_CODIGO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'US_CODIGO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
    object gbFonacot: TGroupBox
      Left = 282
      Top = 355
      Width = 280
      Height = 86
      Caption = ' Fonacot '
      TabOrder = 6
      object lblCedulaPago: TLabel
        Left = 22
        Top = 40
        Width = 78
        Height = 13
        Caption = 'C'#233'dula de pago:'
      end
      object PE_MES_FON: TZetaDBKeyCombo
        Left = 106
        Top = 37
        Width = 161
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 0
        ListaFija = lfMesesFonacot
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'PE_MES_FON'
        DataSource = DataSource
        LlaveNumerica = True
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 356
    Top = 1
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 384
    ImageInfo = <
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF98A1E2FFF6F7FDFFB7BEEBFF4959CDFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFB1B8
          E9FFF6F7FDFFACB3E8FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFFE2E5F7FFFFFFFFFFFFFFFFFFC3C8EEFF4959CDFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFF
          FFFFFFFFFFFFF6F7FDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF9BA4E3FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4959
          CDFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFA6AEE6FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4959CDFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4959CDFF4959CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFC3C8EEFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6
          E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8
          EEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4959CDFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4959CDFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFAFB6E9FFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4959
          CDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4959CDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4959CDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFFA9B0E7FFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFB7BEEBFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFFE2E5F7FFFFFFFFFFFFFFFFFFAFB6E9FF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFF6F7FDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF8792DEFFDFE2F6FFA1A9E5FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF9BA4
          E3FFDFE2F6FF98A1E2FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF64D8
          A1FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF8CE2B8FFE9F9
          F1FF53D396FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF64D8A1FFF4FCF8FFFFFF
          FFFFADEACCFF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF51D395FFD6F4E6FFFFFFFFFFFFFF
          FFFFFCFEFDFF6FDAA7FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FFA5E8C8FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFD9F5E7FF51D395FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF77DDACFFFCFEFDFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF9AE5C1FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF59D59AFFE9F9F1FFFFFFFFFFFFFFFFFFC3EFDAFFBDEE
          D6FFFFFFFFFFFFFFFFFFF4FCF8FF61D79FFF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FFC3EFDAFFFFFFFFFFF7FDFAFF8CE2B8FF4ED293FF53D3
          96FFE1F7ECFFFFFFFFFFFFFFFFFFC8F1DDFF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF8AE1B7FFE6F9F0FF69D9A4FF4ED293FF4ED293FF4ED2
          93FF5ED69DFFE4F8EEFFFFFFFFFFFFFFFFFF8FE2BAFF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF51D395FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF64D8A1FFEFFBF5FFFFFFFFFFF7FDFAFF67D8A2FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF69D9A4FFEFFBF5FFFFFFFFFFDEF7EBFF51D395FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF77DDACFFF7FDFAFFFFFFFFFFB8EDD3FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF6FDAA7FFEFFBF5FFFFFFFFFFA0E6
          C4FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF64D8A1FFE1F7ECFFFFFF
          FFFF8FE2BAFF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF56D498FFBDEE
          D6FFFCFEFDFF8AE1B7FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF81DFB1FF8FE2BAFF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF5160CFFF6B78D6FF8D97DFFFACB3E8FFC8CD
          F0FFE8EAF9FFBAC0ECFF6E7BD7FF7482D9FF6370D4FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF848FDDFF8D97DFFFACB3E8FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFF969FE2FF6370D4FF969FE2FF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFCFC
          FEFFD4D8F3FFBAC0ECFF4656CCFF4656CCFF969FE2FFF9FAFDFFFFFFFFFFFFFF
          FFFFA4ACE6FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE5E7F8FFBAC0ECFF4656CCFF4656CCFF6E7BD7FFD1D5F2FFE5E7F8FFBAC0
          ECFF818CDCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF6875D6FF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFFACB3E8FFBAC0ECFFB1B8E9FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF5463D0FF7482D9FF969FE2FFB1B8
          E9FFD1D5F2FFACB3E8FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end>
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    Left = 448
    Top = 0
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    DesignInfo = 416
  end
end
