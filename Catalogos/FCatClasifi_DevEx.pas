unit FCatClasifi_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Db, ExtCtrls, Grids, DBGrids, Forms, Dialogs,
     ZBaseGridLectura_DevEx,
     ZetaDBGrid, StdCtrls, ZBaseConsulta, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TCatClasifi_DevEx = class(TBaseGridLectura_DevEx)
    TB_CODIGO: TcxGridDBColumn;
    TB_ELEMENT: TcxGridDBColumn;
    TB_SALARIO: TcxGridDBColumn;
    TB_OP1: TcxGridDBColumn;
    TB_OP2: TcxGridDBColumn;
    TB_OP3: TcxGridDBColumn;
    TB_OP4: TcxGridDBColumn;
    TB_OP5: TcxGridDBColumn;
    TB_SUB_CTA: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  CatClasifi_DevEx: TCatClasifi_DevEx;

implementation

uses DCatalogos,
     ZetaBuscador_DevEx,
     ZetaCommonClasses;

{$R *.DFM}

{ TCatClasifi }

procedure TCatClasifi_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     //Desactivar gridmode (FormCreate)
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= FALSE;
     CanLookup := True;
     HelpContext := H60612_Clasificaciones;
end;

procedure TCatClasifi_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsClasifi.Conectar;
          DataSource.DataSet:= cdsClasifi;
     end;
end;

procedure TCatClasifi_DevEx.Refresh;
begin
     dmCatalogos.cdsClasifi.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatClasifi_DevEx.Agregar;
begin
     dmCatalogos.cdsClasifi.Agregar;
end;

procedure TCatClasifi_DevEx.Borrar;
begin
     dmCatalogos.cdsClasifi.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatClasifi_DevEx.Modificar;
begin
     dmCatalogos.cdsClasifi.Modificar;
end;

procedure TCatClasifi_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', 'Clasificaci�n', 'TB_CODIGO', dmCatalogos.cdsClasifi );
end;

procedure TCatClasifi_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     {***Banda Sumatoria***}
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
  inherited;

end;

end.
