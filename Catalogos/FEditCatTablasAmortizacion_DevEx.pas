unit FEditCatTablasAmortizacion_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZetaNumero, ZetaDBTextBox, StdCtrls,
  Mask, ZetaFecha, DBCtrls, Db,
  {$ifdef CAJAAHORRO}
  dCajaAhorro,
  {$else}
  dRecursos,
  {$endif}
  Grids, DBGrids, ZetaDBGrid, ComCtrls, ExtCtrls, Buttons, ZetaKeyCombo,
  ZetaMessages, ZetaEdit, ZBaseEdicionRenglon_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters, cxControls,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxPC, cxNavigator, cxDBNavigator, cxButtons;

type                                            
  TEditTablaAmortizacion_DevEx = class(TBaseEdicionRenglon_DevEx)
    DedLbl: TLabel;
    Label8: TLabel;
    AT_CODIGO: TDBEdit;
    Label1: TLabel;
    AT_DESCRIP: TDBEdit;
    AT_INGLES: TDBEdit;
    Label7: TLabel;
    AT_NUMERO: TZetaDBNumero;
    Label2: TLabel;
    Label3: TLabel;
    AT_TEXTO: TDBEdit;
    AT_ACTIVO: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure BBModificarClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure BBBorrarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure AplicaFiltro;
    procedure HabilitaControls(const lHabilita: Boolean);

    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  EditTablaAmortizacion_DevEx: TEditTablaAmortizacion_DevEx;

implementation

uses ZetaCommonClasses,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaDialogo,
     ZetaTipoEntidad,
     ZAccesosTress,
     dSistema,
     ZAccesosMgr,
     dCatalogos;
{$R *.DFM}


procedure TEditTablaAmortizacion_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsTablasAmortizacion.Conectar;
          Datasource.Dataset := cdsTablasAmortizacion;
          dsRenglon.DataSet := cdsCostosAmortizacion;
     end;
end;

procedure TEditTablaAmortizacion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= ZetaCommonClasses.H_Cat_TablasAmortizacionEdicion;
     IndexDerechos := ZAccesosTress.D_CAT_GRAL_TABLA_AMORTIZACION;
     FirstControl := AT_CODIGO;
end;


procedure TEditTablaAmortizacion_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage := Tabla;
     dmCatalogos.ListaCostoSGM := TList.Create;
     dmCatalogos.LlenarListaCostosSGM;
end;

procedure TEditTablaAmortizacion_DevEx.BBAgregarClick(Sender: TObject);
begin
     //inherited;
     IF  ZAccesosMgr.CheckDerecho( D_CAT_GRAL_TABLA_AMORTIZACION , K_DERECHO_CAMBIO ) then
     begin
          DataSource.DataSet.Edit;
          dsRenglon.DataSet.Append;
     end
     else
         ZInformation('Tabla de Cotizaci�n','No Tiene Permiso Para Modificar Registros',0);
end;

procedure TEditTablaAmortizacion_DevEx.BBModificarClick(Sender: TObject);
begin
     //inherited;
     IF ZAccesosMgr.CheckDerecho( D_CAT_GRAL_TABLA_AMORTIZACION , K_DERECHO_CAMBIO ) THEN
     begin
          DataSource.DataSet.Edit;
          dsRenglon.DataSet.Edit;
     end
     else
         ZInformation('Tabla de Cotizaci�n','No Tiene Permiso Para Modificar Registros',0);
end;

procedure TEditTablaAmortizacion_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
     Procedure HabilitaCostos( bHabilita : Boolean );
     begin
          BBAgregar_DevEx.Enabled := bHabilita;
          BBBorrar_DevEx.Enabled := bHabilita;
          BBModificar_DevEx.Enabled := bHabilita;
     end;
begin
     inherited;
     if Field = nil then
     begin

          with dmCatalogos.cdsTablasAmortizacion do
          begin
               AplicaFiltro;
                  
               if State = dsInsert then
               begin
                    HabilitaControls( ZAccesosMgr.CheckDerecho( D_CAT_GRAL_TABLA_AMORTIZACION , K_DERECHO_ALTA )  );
                    HabilitaCostos( False );
               end;
               if State = dsBrowse then
               begin
                    HabilitaControls( ZAccesosMgr.CheckDerecho( D_CAT_GRAL_TABLA_AMORTIZACION , K_DERECHO_CAMBIO )  );
                    HabilitaCostos( True );
               end;
          end;
     end;
end;

procedure TEditTablaAmortizacion_DevEx.AplicaFiltro;
begin
     with dmCatalogos.cdsCostosAmortizacion do
     begin
          if State = dsBrowse then
          begin
               Filtered := False;
               Filter := Format('AT_CODIGO = ''%s''',[dmCatalogos.cdsTablasAmortizacion.FieldByName('AT_CODIGO').AsString]);
               Filtered := True;
          end;
     end;
end;


procedure TEditTablaAmortizacion_DevEx.HabilitaControls(const lHabilita:Boolean);
begin
     AT_CODIGO.Enabled := lHabilita;
     AT_NUMERO.Enabled := lHabilita;
     AT_DESCRIP.Enabled := lHabilita;
     AT_NUMERO.Enabled := lHabilita;
     AT_TEXTO.Enabled := lHabilita;
end;

procedure TEditTablaAmortizacion_DevEx.BBBorrarClick(Sender: TObject);
begin
     IF ( ZAccesosMgr.CheckDerecho( D_CAT_GRAL_TABLA_AMORTIZACION  , K_DERECHO_CAMBIO ) ) then
     begin
          if ZConfirm('Tabla de Cotizaci�n','�Seguro de Borrar Registro?',0,mbOk) then
          begin
               DataSource.DataSet.Edit;
               inherited;
          end;

     end
     else
         ZInformation('Tabla de Cotizaci�n','No Tiene Permiso Para Modificar Registros',0);

end;

procedure TEditTablaAmortizacion_DevEx.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     inherited;
     dmCatalogos.ListaCostoSGM.Free;
end;

end.
