unit FCatInvitadores_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, Grids, DBGrids,
     ZBaseConsulta,
     ZetaDBGrid, StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
     cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
     
     TressMorado2013,
     dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
     cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
     cxGridTableView, cxGridDBTableView, Menus, ActnList, ImgList,
     cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid,
     dxSkinsDefaultPainters, System.Actions;

type
  TCatInvitadores_DevEx = class(TBaseGridLectura_DevEx)
    IV_CODIGO: TcxGridDBColumn;
    IV_NOMBRE: TcxGridDBColumn;
    CB_CODIGO: TcxGridDBColumn;
    IV_ACTIVO: TcxGridDBColumn;
    IV_FEC_INI: TcxGridDBColumn;
    IV_FEC_FIN: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  CatInvitadores_DevEx: TCatInvitadores_DevEx;

implementation

uses DCatalogos,
     ZetaBuscaEntero_DevEx,
     ZetaCommonClasses;

{$R *.DFM}

{ TCatInvitadores_DevEx }

procedure TCatInvitadores_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext:= H60662_invitadores;
end;

procedure TCatInvitadores_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsInvitadores.Conectar;
          DataSource.DataSet:= cdsInvitadores;
     end;

end;

procedure TCatInvitadores_DevEx.Refresh;
begin
     dmCatalogos.cdsInvitadores.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatInvitadores_DevEx.Agregar;
begin
     dmCatalogos.cdsInvitadores.Agregar;
end;

procedure TCatInvitadores_DevEx.Borrar;
begin
     dmCatalogos.cdsInvitadores.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();

end;

procedure TCatInvitadores_DevEx.Modificar;
begin
     dmCatalogos.cdsInvitadores.Modificar;
end;

procedure TCatInvitadores_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscaEntero_DevEx.BuscarCodigo( 'N�mero', 'Invitador', 'IV_CODIGO', dmCatalogos.cdsInvitadores );
end;

procedure TCatInvitadores_DevEx.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
  inherited;

   //columna sumatoria: cuantos
   CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('IV_CODIGO'), K_SIN_TIPO , '', skCount);
   ZetaDBGridDBTableView.ApplyBestFit();
  
end;

end.
