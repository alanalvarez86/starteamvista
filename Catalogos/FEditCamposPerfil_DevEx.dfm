inherited EditCamposPerfil_DevEx: TEditCamposPerfil_DevEx
  Left = 254
  Top = 200
  Caption = 'Campo de Perfil'
  ClientHeight = 356
  ClientWidth = 455
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 68
    Top = 72
    Width = 42
    Height = 13
    Alignment = taRightJustify
    Caption = 'Secci'#243'n:'
  end
  object Label4: TLabel [1]
    Left = 78
    Top = 91
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Orden:'
  end
  object Label2: TLabel [2]
    Left = 79
    Top = 112
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'T'#237'tulo:'
  end
  object Label3: TLabel [3]
    Left = 74
    Top = 135
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Campo:'
  end
  object Label5: TLabel [4]
    Left = 38
    Top = 157
    Width = 72
    Height = 13
    Alignment = taRightJustify
    Caption = 'Capturar como:'
  end
  object LblLimite: TLabel [5]
    Left = 48
    Top = 180
    Width = 62
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ancho l'#237'mite:'
  end
  object LblValores: TLabel [6]
    Left = 31
    Top = 200
    Width = 79
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valores posibles:'
  end
  object DT_NOMBRE: TZetaDBTextBox [7]
    Left = 163
    Top = 70
    Width = 229
    Height = 17
    AutoSize = False
    Caption = 'DT_NOMBRE'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'DT_NOMBRE'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object DT_CODIGO: TZetaDBTextBox [8]
    Left = 112
    Top = 70
    Width = 50
    Height = 17
    AutoSize = False
    Caption = 'DT_CODIGO'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'DT_CODIGO'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object DF_ORDEN: TZetaDBTextBox [9]
    Left = 112
    Top = 89
    Width = 50
    Height = 17
    AutoSize = False
    Caption = 'DF_ORDEN'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'DF_ORDEN'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  inherited PanelBotones: TPanel
    Top = 320
    Width = 455
    inherited OK_DevEx: TcxButton
      Left = 291
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 370
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 455
    inherited ValorActivo2: TPanel
      Width = 129
      inherited textoValorActivo2: TLabel
        Width = 123
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 11
  end
  object DF_TITULO: TDBEdit [13]
    Left = 112
    Top = 108
    Width = 281
    Height = 21
    DataField = 'DF_TITULO'
    DataSource = DataSource
    TabOrder = 3
  end
  object DF_LIMITE: TZetaDBNumero [14]
    Left = 112
    Top = 174
    Width = 50
    Height = 21
    Mascara = mnDias
    TabOrder = 6
    Text = '0'
    UseEnterKey = True
    DataField = 'DF_LIMITE'
    DataSource = DataSource
  end
  object DF_VALORES: TcxDBMemo [15]
    Left = 112
    Top = 197
    DataBinding.DataField = 'DF_VALORES'
    DataBinding.DataSource = DataSource
    Properties.ScrollBars = ssVertical
    TabOrder = 7
    Height = 92
    Width = 281
  end
  object DF_CONTROL: TZetaDBKeyCombo [16]
    Left = 112
    Top = 152
    Width = 145
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 5
    OnChange = DF_CONTROLChange
    ListaFija = lfControles
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'DF_CONTROL'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object DF_CAMPO: TDBComboBox [17]
    Left = 112
    Top = 130
    Width = 145
    Height = 21
    DataField = 'DF_CAMPO'
    DataSource = DataSource
    ItemHeight = 13
    Items.Strings = (
      'DP_TEXT_01'
      'DP_TEXT_02'
      'DP_TEXT_03'
      'DP_MEMO_01'
      'DP_MEMO_02'
      'DP_MEMO_03'
      'DP_NUME_01'
      'DP_FECH_01')
    TabOrder = 4
  end
  inherited DataSource: TDataSource
    Left = 444
    Top = 57
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_ExportarBtn: TdxBarButton
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
