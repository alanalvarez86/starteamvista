inherited AgregaCopiaVal_DevEx: TAgregaCopiaVal_DevEx
  Left = 479
  Top = 527
  Caption = 'Valuaci'#243'n de Puestos'
  ClientHeight = 246
  ClientWidth = 439
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 210
    Width = 439
    TabOrder = 3
    inherited OK_DevEx: TcxButton
      Left = 275
      ModalResult = 0
      OnClick = OK_DevExClick
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 354
      Cancel = True
    end
  end
  object PanelPlantilla: TPanel [1]
    Left = 0
    Top = 0
    Width = 439
    Height = 41
    Align = alTop
    TabOrder = 0
    object Label6: TLabel
      Left = 5
      Top = 17
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = 'Plantilla:'
    end
    object VL_CODIGO: TZetaDBTextBox
      Left = 48
      Top = 16
      Width = 75
      Height = 17
      AutoSize = False
      Caption = 'VL_CODIGO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'VL_CODIGO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object VL_NOMBRE: TZetaDBTextBox
      Left = 128
      Top = 16
      Width = 275
      Height = 17
      AutoSize = False
      Caption = 'VL_NOMBRE'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'VL_NOMBRE'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
  end
  object GBCopia: TcxGroupBox [2]
    Left = 0
    Top = 41
    Align = alTop
    Caption = ' Copiar de '
    TabOrder = 1
    Visible = False
    Height = 85
    Width = 439
    object Label4: TLabel
      Left = 6
      Top = 38
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'Puesto:'
    end
    object Label1: TLabel
      Left = 9
      Top = 57
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha:'
    end
    object Label2: TLabel
      Left = 17
      Top = 18
      Width = 25
      Height = 13
      Alignment = taRightJustify
      Caption = 'Folio:'
    end
    object PU_CODIGOC: TZetaTextBox
      Left = 48
      Top = 38
      Width = 75
      Height = 17
      AutoSize = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object PU_DESCRIPC: TZetaTextBox
      Left = 128
      Top = 38
      Width = 275
      Height = 17
      AutoSize = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object VP_FECHAC: TZetaTextBox
      Left = 48
      Top = 57
      Width = 75
      Height = 17
      AutoSize = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object VP_FOLIO: TZetaNumero
      Left = 48
      Top = 16
      Width = 75
      Height = 21
      Color = clBtnFace
      Enabled = False
      Mascara = mnDias
      TabOrder = 0
      Text = '0'
    end
  end
  object GBCrearVal: TcxGroupBox [3]
    Left = 0
    Top = 126
    Align = alClient
    Caption = ' Valuaci'#243'n a Crear '
    TabOrder = 2
    Height = 84
    Width = 439
    object Label3: TLabel
      Left = 6
      Top = 26
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'Puesto:'
    end
    object Label5: TLabel
      Left = 9
      Top = 52
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha:'
    end
    object PU_CODIGO: TZetaDBKeyLookup_DevEx
      Left = 48
      Top = 22
      Width = 361
      Height = 21
      LookupDataset = dmCatalogos.cdsPuestos
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      DataField = 'PU_CODIGO'
      DataSource = DataSource
    end
    object VP_FECHA: TZetaDBFecha
      Left = 48
      Top = 48
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 1
      Text = '18/may/07'
      Valor = 39220.000000000000000000
      DataField = 'VP_FECHA'
      DataSource = DataSource
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  object DataSource: TDataSource
    Left = 416
  end
end
