inherited EditSeguroGastosMedicos_DevEx: TEditSeguroGastosMedicos_DevEx
  Left = 423
  Top = 45
  Caption = 'Seguro de Gastos M'#233'dicos'
  ClientHeight = 736
  ClientWidth = 556
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl: TcxPageControl [0]
    Top = 472
    Width = 556
    Height = 228
    ClientRectBottom = 226
    ClientRectRight = 554
    inherited Datos: TcxTabSheet
      Caption = 'Resumen'
      TabVisible = False
      object DedLbl: TLabel
        Left = 182
        Top = 156
        Width = 64
        Height = 13
        Alignment = taRightJustify
        Caption = '# de Abonos:'
      end
    end
    inherited Tabla: TcxTabSheet
      Caption = 'Vigencias'
      inherited Panel2: TPanel [0]
        Width = 552
        inherited BBAgregar_DevEx: TcxButton
          Left = 57
        end
        inherited BBBorrar_DevEx: TcxButton
          Left = 197
          Caption = ' Borrar Rengl'#243'n'
        end
        inherited BBModificar_DevEx: TcxButton
          Left = 337
        end
      end
      inherited GridRenglones: TZetaDBGrid [1]
        Width = 552
        Height = 169
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs]
        ReadOnly = True
        OnDblClick = GridRenglonesDblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'PV_REFEREN'
            Title.Caption = 'Referencia'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PV_FEC_INI'
            Title.Caption = 'Inicio'
            Width = 75
            Visible = True
          end
          item
            Alignment = taRightJustify
            Expanded = False
            FieldName = 'PV_FEC_FIN'
            Title.Caption = 'Final'
            Width = 71
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PV_CFIJO'
            Title.Caption = 'Costo Fijo'
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'PV_STITULA'
            Title.Caption = 'Titulares'
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'PV_SDEPEND'
            Title.Caption = 'Dependientes'
            Width = 77
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PV_CONDIC'
            Title.Caption = 'Condiciones'
            Visible = False
          end>
        object btnBuscarEmp: TSpeedButton
          Left = 248
          Top = 131
          Width = 25
          Height = 17
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            33033333333333333F7F3333333333333000333333333333F777333333333333
            000333333333333F777333333333333000333333333333F77733333333333300
            033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
            33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
            3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
            33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
            333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
            333333773FF77333333333370007333333333333777333333333}
          NumGlyphs = 2
          Visible = False
        end
      end
    end
  end
  inherited PanelBotones: TPanel [1]
    Top = 700
    Width = 556
    inherited OK_DevEx: TcxButton
      Left = 392
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 471
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel [3]
    Width = 556
    inherited ValorActivo2: TPanel
      Width = 230
      inherited textoValorActivo2: TLabel
        Width = 224
      end
    end
  end
  inherited Panel1: TPanel [4]
    Width = 556
    Height = 422
    object Label2: TLabel
      Left = 40
      Top = 348
      Width = 74
      Height = 13
      Alignment = taRightJustify
      Caption = 'Observaciones:'
    end
    object Label8: TLabel
      Left = 79
      Top = 11
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'digo:'
    end
    object Label1: TLabel
      Left = 75
      Top = 33
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'mero:'
    end
    object Label7: TLabel
      Left = 56
      Top = 55
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descripci'#243'n:'
    end
    object Label9: TLabel
      Left = 52
      Top = 77
      Width = 63
      Height = 13
      Alignment = taRightJustify
      Caption = 'Aseguradora:'
    end
    object Label11: TLabel
      Left = 81
      Top = 99
      Width = 34
      Height = 13
      Alignment = taRightJustify
      Caption = 'Broker:'
    end
    object Label14: TLabel
      Left = 36
      Top = 120
      Width = 79
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tel'#233'fono Broker:'
    end
    object Label15: TLabel
      Left = 288
      Top = 33
      Width = 24
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo:'
    end
    object PM_OBSERVA: TcxDBMemo
      Left = 118
      Top = 347
      DataBinding.DataField = 'PM_OBSERVA'
      DataBinding.DataSource = DataSource
      ParentFont = False
      Properties.MaxLength = 255
      Properties.ScrollBars = ssVertical
      TabOrder = 9
      Height = 66
      Width = 402
    end
    object PM_CODIGO: TDBEdit
      Left = 118
      Top = 8
      Width = 83
      Height = 21
      CharCase = ecUpperCase
      DataField = 'PM_CODIGO'
      DataSource = DataSource
      TabOrder = 0
    end
    object GroupBox2: TcxGroupBox
      Left = 16
      Top = 142
      Caption = 'Contacto #1'
      TabOrder = 7
      Height = 97
      Width = 505
      object Label19: TLabel
        Left = 59
        Top = 23
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nombre:'
      end
      object Label10: TLabel
        Left = 9
        Top = 45
        Width = 90
        Height = 13
        Alignment = taRightJustify
        Caption = 'Correo Electr'#243'nico:'
      end
      object Label12: TLabel
        Left = 45
        Top = 67
        Width = 54
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tel'#233'fono 1:'
      end
      object Label13: TLabel
        Left = 265
        Top = 67
        Width = 54
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tel'#233'fono 2:'
      end
      object PM_NOM_CT1: TDBEdit
        Left = 103
        Top = 19
        Width = 377
        Height = 21
        DataField = 'PM_NOM_CT1'
        DataSource = DataSource
        TabOrder = 0
      end
      object PM_COR_CT1: TDBEdit
        Left = 103
        Top = 41
        Width = 377
        Height = 21
        DataField = 'PM_COR_CT1'
        DataSource = DataSource
        TabOrder = 1
      end
      object PM_TEL1CT1: TDBEdit
        Left = 103
        Top = 63
        Width = 157
        Height = 21
        DataField = 'PM_TEL1CT1'
        DataSource = DataSource
        TabOrder = 2
      end
      object PM_TEL2CT1: TDBEdit
        Left = 322
        Top = 63
        Width = 157
        Height = 21
        DataField = 'PM_TEL2CT1'
        DataSource = DataSource
        TabOrder = 3
      end
    end
    object PM_NUMERO: TDBEdit
      Left = 118
      Top = 30
      Width = 164
      Height = 21
      DataField = 'PM_NUMERO'
      DataSource = DataSource
      TabOrder = 1
    end
    object GroupBox1: TcxGroupBox
      Left = 16
      Top = 246
      Caption = 'Contacto #2'
      TabOrder = 8
      Height = 97
      Width = 505
      object Label3: TLabel
        Left = 59
        Top = 23
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nombre:'
      end
      object Label4: TLabel
        Left = 9
        Top = 45
        Width = 90
        Height = 13
        Alignment = taRightJustify
        Caption = 'Correo Electr'#243'nico:'
      end
      object Label5: TLabel
        Left = 45
        Top = 67
        Width = 54
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tel'#233'fono 1:'
      end
      object Label6: TLabel
        Left = 265
        Top = 67
        Width = 54
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tel'#233'fono 2:'
      end
      object PM_NOM_CT2: TDBEdit
        Left = 103
        Top = 19
        Width = 377
        Height = 21
        DataField = 'PM_NOM_CT2'
        DataSource = DataSource
        TabOrder = 0
      end
      object PM_COR_CT2: TDBEdit
        Left = 103
        Top = 41
        Width = 377
        Height = 21
        DataField = 'PM_COR_CT2'
        DataSource = DataSource
        TabOrder = 1
      end
      object PM_TEL1CT2: TDBEdit
        Left = 103
        Top = 63
        Width = 157
        Height = 21
        DataField = 'PM_TEL1CT2'
        DataSource = DataSource
        TabOrder = 2
      end
      object PM_TEL2CT2: TDBEdit
        Left = 322
        Top = 63
        Width = 157
        Height = 21
        DataField = 'PM_TEL2CT2'
        DataSource = DataSource
        TabOrder = 3
      end
    end
    object PM_DESCRIP: TDBEdit
      Left = 118
      Top = 52
      Width = 377
      Height = 21
      DataField = 'PM_DESCRIP'
      DataSource = DataSource
      TabOrder = 3
    end
    object PM_ASEGURA: TDBEdit
      Left = 118
      Top = 74
      Width = 377
      Height = 21
      DataField = 'PM_ASEGURA'
      DataSource = DataSource
      TabOrder = 4
    end
    object PM_BROKER: TDBEdit
      Left = 118
      Top = 96
      Width = 377
      Height = 21
      DataField = 'PM_BROKER'
      DataSource = DataSource
      TabOrder = 5
    end
    object PM_TEL_BRK: TDBEdit
      Left = 118
      Top = 117
      Width = 163
      Height = 21
      DataField = 'PM_TEL_BRK'
      DataSource = DataSource
      TabOrder = 6
    end
    object PM_TIPO: TZetaDBKeyCombo
      Left = 313
      Top = 30
      Width = 183
      Height = 21
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 2
      ListaFija = lfStatusSujeto
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
      DataField = 'PM_TIPO'
      DataSource = DataSource
      LlaveNumerica = True
    end
  end
  inherited DataSource: TDataSource
    Left = 329
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  inherited dsRenglon: TDataSource
    Left = 389
    Top = 0
  end
end
