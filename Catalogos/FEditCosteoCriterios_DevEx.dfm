inherited EditCosteoCriterios_DevEx: TEditCosteoCriterios_DevEx
  Left = 0
  Top = 0
  Caption = 'Criterios de Costeo'
  ClientHeight = 124
  ClientWidth = 430
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 8
    Top = 67
    Width = 92
    Height = 13
    Alignment = taRightJustify
    Caption = '&Nombre del Criterio:'
    FocusControl = CritCostoNombre
  end
  object Label2: TLabel [1]
    Left = 64
    Top = 43
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = '&C'#243'digo:'
    FocusControl = CritCostoID
  end
  inherited PanelBotones: TPanel
    Top = 88
    Width = 430
    TabOrder = 3
    inherited OK_DevEx: TcxButton
      Left = 266
      Top = 4
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 345
      Top = 4
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 430
    TabOrder = 5
    inherited ValorActivo2: TPanel
      Width = 104
      inherited textoValorActivo2: TLabel
        Width = 98
      end
    end
  end
  object CritCostoNombre: TDBEdit [5]
    Left = 104
    Top = 64
    Width = 257
    Height = 21
    Hint = 'Nombre de Criterio de Costeo'
    DataField = 'CritCostoNombre'
    DataSource = DataSource
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
  end
  object TB_ACTIVO: TDBCheckBox [6]
    Left = 304
    Top = 39
    Width = 53
    Height = 17
    Hint = #191'Est'#225' Activo?'
    Alignment = taLeftJustify
    Caption = '&Activo:'
    DataField = 'CritCostoActivo'
    DataSource = DataSource
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  object CritCostoID: TZetaDBNumero [7]
    Left = 104
    Top = 40
    Width = 73
    Height = 21
    Hint = 'C'#243'digo Numerico de Criterio de Costeo'
    Mascara = mnDias
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    Text = '0'
    DataField = 'CritCostoID'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 396
    Top = 1
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
