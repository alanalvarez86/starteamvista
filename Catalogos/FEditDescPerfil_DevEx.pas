unit FEditDescPerfil_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicion_DevEx, ComCtrls, ZetaDBTextBox, StdCtrls, DB, ExtCtrls,
  DBCtrls, ZetaNumero, ZetaFecha, ZetaKeyLookup_DevEx,
  ZetaKeyCombo, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC, cxMemo;

type
  TEditDescPerfil_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    Label8: TLabel;
    PU_CODIGO: TZetaDBTextBox;
    PU_DESCRIP: TZetaDBTextBox;
    PageControl: TcxPageControl;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    function GetFirstControlAdicional(oControl: TWinControl): TWinControl;
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  EditDescPerfil_DevEx: TEditDescPerfil_DevEx;

implementation

uses dCatalogos,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZAccesosTress;

{$R *.dfm}

procedure TEditDescPerfil_DevEx.FormCreate(Sender: TObject);
const
     K_ALTURA_PANELES  = 150;
     K_ALTURA_MAX_PAGE = 600;
var
   iMaxAltura: Integer;
begin
     inherited;
     iMaxAltura := dmCatalogos.CamposAdic.ContruyeFormaEdicion( dmCatalogos.Clasificaciones, self.DataSource, self, PageControl, nil, FALSE );
     self.Height := K_ALTURA_PANELES + iMin( iMaxAltura, K_ALTURA_MAX_PAGE );  // Ajusta forma a la altura de controles o tope de 750 pixeles
     IndexDerechos := D_CAT_DESC_PERF_PUESTOS;
     HelpContext:= H_EDIT_DESC_PERFIL; // PENDIENTE
end;

procedure TEditDescPerfil_DevEx.FormShow(Sender: TObject);
begin
     Self.Caption := dmCatalogos.Clasificaciones.Clasificacion[0].Nombre;
     PageControl.ActivePageIndex := 0; // Primer tabsheet
     FirstControl := GetFirstControlAdicional( PageControl.ActivePage );
     inherited;
end;

procedure TEditDescPerfil_DevEx.Connect;
begin
     DataSource.DataSet:= dmCatalogos.cdsDescPerfil;
end;

// Candidato a ser funcion generica, junto con la de FEditEmpAdicionales
function TEditDescPerfil_DevEx.GetFirstControlAdicional( oControl: TWinControl ): TWinControl;
var
   i : Integer;
   oControlHijo : TWinControl;
   lOk : Boolean;
begin
     Result := nil;
     if Assigned( oControl ) then
     begin
          for i:= 0 to oControl.ControlCount - 1 do
          begin
               if ( oControl.Controls[i] is TWinControl ) then  // Solo controles que hereden de TWinControl
               begin
                    oControlHijo := TWinControl( oControl.Controls[i] );

                    lOk := ( oControlHijo is TDBEdit ) or ( oControlHijo is TZetaDBNumero ) or
                           ( oControlHijo is TDBCheckBox ) or ( oControlHijo is TZetaDBFecha ) or
                           ( oControlHijo is TZetaDBKeyLookup_DevEx ) or ( oControlHijo is TDBMemo ) or
                           ( oControlHijo is TZetaDBKeyCombo ) or ( oControlHijo is TDBListBox );

                    if lOk then
                    begin
                         Result := oControlHijo;
                         Break;                  // Solo el primer control de ese tipo
                    end
                    else
                    begin
                         if ( oControlHijo.ControlCount > 0 ) then
                            Result := GetFirstControlAdicional( oControlHijo );
                    end;
               end;
          end;
     end;
end;

end.
