inherited CatEventos_DevEx: TCatEventos_DevEx
  Left = 205
  Top = 169
  Caption = 'Eventos'
  ClientHeight = 272
  ClientWidth = 570
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 570
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 244
      inherited textoValorActivo2: TLabel
        Width = 238
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 570
    Height = 253
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object EV_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'EV_CODIGO'
      end
      object EV_DESCRIP: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'EV_DESCRIP'
      end
      object EV_ACTIVO: TcxGridDBColumn
        Caption = 'Activo'
        DataBinding.FieldName = 'EV_ACTIVO'
      end
      object EV_PRIORID: TcxGridDBColumn
        Caption = 'Prioridad'
        DataBinding.FieldName = 'EV_PRIORID'
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 16
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
