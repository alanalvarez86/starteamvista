unit MockUnits_TRESS;

interface
uses
ZEvaluador,
     Messages, Classes, SysUtils, Dialogs, DB, Controls, Mask,Forms,
     ZetaQRExpr,
     ZetaTipoEntidad,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZCreator,
     DExisteCampo,
     DZetaServerProvider;

type
  TZetaCreatorMock = class(TZetaCreator)
  public
        procedure PreparaTablasISPT; override;
        procedure PreparaSalMin; override;
  end;

  TZetaEvaluadorMock = class(TZetaEvaluador)
  private
    { Private Declarations }
    FCreator : TZetaCreatorMock;
    FEntidad : TipoEntidad;
    FListaRequeridos: TStringList;
    FListaVariables : TStringList;
    FTransformada : String;
    FCalcNomina: TObject;
    FVarAux : TPesos;
    FListaSubTotal : String;
    function GetExisteCampo: TdmExisteCampo;

  protected
    { Protected Declarations }
    function  EvalVariable( strVariable: String ): TQREvResult; override;
    function  GetCreator : TZetaCreatorMock;
  public
    { Public Declarations }
    constructor Create( oCreator : TZetaCreator );overload;
    constructor Create( oCreator : TZetaCreator; oCalcNomina : TObject );overload;
    destructor  Destroy; override;
    property oZetaCreator :TZetaCreatorMock  read GetCreator;
    property dmExisteCampo: TdmExisteCampo read GetExisteCampo;
    procedure AgregaDataset( Dataset: TZetaCursor );
    procedure Prepare(const StrExpr : String);
    function oZetaProvider: TdmZetaServerProvider;

    function Rastreador: TRastreador;
    function  Calculate(const StrExpr : string) : TQREvResult;
    function  GetRequiere( const Expresion: String; var eTipo : eTipoGlobal; var Transformada : String; var lConstante : Boolean ) : Boolean;
    function  ValorPesos: TPesos;
    function  ValorEntero: integer;
    function  ValorTexto: String;
    function  ValorBooleano: Boolean;
    function  GetEmpleado : Integer;
    function  CalculaString( const Expresion : String; var eTipo : eTipoGlobal;
                             var Resultado : String ) : Boolean;
    function CalculandoNomina : Boolean;
    function Rastreando : Boolean;
    function RastreandoNomina: Boolean;
    property  CalcNomina : TObject read FCalcNomina write FCalcNomina;
    property  ListaRequeridos : TStringList read FListaRequeridos;
    property  Entidad : TipoEntidad read FEntidad write FEntidad;
    function  NoHayRequeridos : Boolean;
    property  VarAux : TPesos read FVarAux write FVarAux;
    function  FiltroSQLValido( var sFiltro, sError : String ) : Boolean;
    property  ListaSubTotal : String read FListaSubTotal;
    procedure AgregaSubTotal( const nPos : Integer );
    function  LibreriaFunciones: TQRFunctionLibrary;
    procedure AgregaRequeridoAlias( sExpresion, sAlias : String; const TipoCampo : eTipoGlobal; const oEntidad : TipoEntidad );

  end;


implementation

{ TZetaEvaluadorMock }

procedure TZetaEvaluadorMock.AgregaDataset(Dataset: TZetaCursor);
begin
//
end;

procedure TZetaEvaluadorMock.AgregaRequeridoAlias(sExpresion,
  sAlias: String; const TipoCampo: eTipoGlobal;
  const oEntidad: TipoEntidad);
begin
 //
end;

procedure TZetaEvaluadorMock.AgregaSubTotal(const nPos: Integer);
begin
  //
end;

function TZetaEvaluadorMock.CalculandoNomina: Boolean;
begin
    Result := False;
end;

function TZetaEvaluadorMock.CalculaString(const Expresion: String;
  var eTipo: eTipoGlobal; var Resultado: String): Boolean;
begin
    //
end;

function TZetaEvaluadorMock.Calculate(const StrExpr: string): TQREvResult;
begin
//
end;

constructor TZetaEvaluadorMock.Create(oCreator: TZetaCreator;
  oCalcNomina: TObject);
begin
   Self.FCreator :=  TZetaCreatorMock( oCreator );
   Self.CalcNomina := oCalcNomina;
end;

constructor TZetaEvaluadorMock.Create(oCreator: TZetaCreator);
begin
   Self.FCreator := TZetaCreatorMock( oCreator );
end;

destructor TZetaEvaluadorMock.Destroy;
begin
    //
  inherited;
end;

function TZetaEvaluadorMock.EvalVariable(strVariable: String): TQREvResult;
begin
//
end;

function TZetaEvaluadorMock.FiltroSQLValido(var sFiltro,
  sError: String): Boolean;
begin
   Result := True;
end;

function TZetaEvaluadorMock.GetCreator: TZetaCreatorMock;
begin
  Result := FCreator;
end;

function TZetaEvaluadorMock.GetEmpleado: Integer;
begin
   Result := 1;
end;

function TZetaEvaluadorMock.GetExisteCampo: TdmExisteCampo;
begin
   Result := nil;
end;

function TZetaEvaluadorMock.GetRequiere(const Expresion: String;
  var eTipo: eTipoGlobal; var Transformada: String;
  var lConstante: Boolean): Boolean;
begin
   Result := True;
end;

function TZetaEvaluadorMock.LibreriaFunciones: TQRFunctionLibrary;
begin
   Result := nil;
end;

function TZetaEvaluadorMock.NoHayRequeridos: Boolean;
begin
    Result := True;
end;

function TZetaEvaluadorMock.oZetaProvider: TdmZetaServerProvider;
begin
    Result := nil;
end;

procedure TZetaEvaluadorMock.Prepare(const StrExpr: String);
begin
      //
end;

function TZetaEvaluadorMock.Rastreador: TRastreador;
begin
     Result := nil;
end;

function TZetaEvaluadorMock.Rastreando: Boolean;
begin
     Result := False;
end;

function TZetaEvaluadorMock.RastreandoNomina: Boolean;
begin
     Result := False;
end;

function TZetaEvaluadorMock.ValorBooleano: Boolean;
begin
    //
end;

function TZetaEvaluadorMock.ValorEntero: integer;
begin
    //
end;

function TZetaEvaluadorMock.ValorPesos: TPesos;
begin
   //
end;

function TZetaEvaluadorMock.ValorTexto: String;
begin
     //
end;

{ TZetaCreatorMock }

procedure TZetaCreatorMock.PreparaSalMin;
begin
  //inherited;

end;

procedure TZetaCreatorMock.PreparaTablasISPT;
begin
  //inherited;

end;

end.
