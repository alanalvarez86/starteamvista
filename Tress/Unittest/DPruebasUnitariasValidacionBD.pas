unit DPruebasUnitariasValidacionBD;

interface

uses
  SysUtils, Classes,
  TestFrameWork,
  ZetaCommonClasses, ZetaCommonTools,  ZetaCommonLists,
  DB, DBClient, ZetaLicenseClasses, gtWidestrings, FAutoServer;

type
  TDmPruebasUnitariasValidacionBD = class(TDataModule)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  TLicencia  = class ( TGlobalLicenseValues )
  public
        FMeses : integer;
        FMaxConfig : integer;
        FNoSentinel : integer;
        FMaxEmpleados : integer;
  public
    function GetMesesRelativos : integer; override;
    function GetMaxConfigBD : integer; override;
    function GetNoSentinel : integer; override;
    function GetMaxEmpleados : integer; override;
  end;


  TStoreHandler = class ( TObject, IDBConfigStoreHandler )
  private
       FKeyArray : array [0..9] of string;
       FRegValue : string;
       FComparteBD : string;
       FEsInicial : boolean;
  public
     function EsDbConfigInicial : boolean;
     function ReadDBConfigFromDatabase( iKey : integer) : string;
     function WriteDBConfigFromDatabase( iKey : integer; encryptedValue : string ) : boolean;
     function ReadRegistryComparteBD : string;
     function ReadRegistryChecksum : string;
     function WriteRegistryChecksum( encryptedValue : string ) : boolean;
     function _AddRef: Integer; stdcall;
     function _Release: Integer; stdcall;
     function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
  end;


  TTestTRESSCFG_ValidacionBD_PruebasLicencia1 = class(TTestCase)
  private
        oLicencia : TLicencia;
        oStoreHndlr : TStoreHandler;
        oStoreHndlrOtro : TStoreHandler;
        oConfig : TDbConfigManager;
  protected
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure Validaciones_Al_Iniciar;
    procedure AgregarBD_Antes_de_Validar;
    procedure AgregarBD_Antes_de_Topar;
    procedure AgregarBD_Despues_de_Topar;
    procedure Cambiar_Fecha_Inicio;
    procedure Cambiar_Comparte;
  end;


 TTestTRESSCFG_ValidacionBD_PruebasLicencia2 = class(TTestCase)
  private
        oLicencia : TLicencia;
        oStoreHndlr : TStoreHandler;
        oConfig : TDbConfigManager;
  protected
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure Validaciones_Al_Iniciar;
    procedure AgregarBD_Antes_de_Validar;
    procedure AgregarBD_Antes_de_Topar;
    procedure AgregarBD_Despues_de_Topar;
    procedure Extender_Cantidad_10;
    procedure AgregarBD_Despues_de_Extender;
    procedure Empleados_Silencio;
    procedure Empleados_Advertencia;
    procedure Empleados_Restringe;
    procedure Empleados_Restringe_Demo;
    procedure Empleados_Restringe_NoDemo;
  end;




var
  DmPruebasUnitariasValidacionBD: TDmPruebasUnitariasValidacionBD;

implementation

{$R *.dfm}

 { TTestTRESSCFG_ValidacionBD_Tope }
procedure TTestTRESSCFG_ValidacionBD_PruebasLicencia1.SetUp;
begin
  inherited;
  oLicencia := TLicencia.Create( nil);
  oLicencia.FMeses := 0;
  oLicencia.FMaxConfig := 10;
  oLicencia.FNoSentinel := 1020;
  oStoreHndlr := TStoreHandler.Create();
  oStoreHndlr.FEsInicial := True;
  oStoreHndlr.FComparteBD := 'SERVIDOR.COMPARTE';
  oStoreHndlrOtro := TStoreHandler.Create();
  oStoreHndlrOtro.FEsInicial := True;
  oStoreHndlrOtro.FComparteBD := 'SERVIDOR.COMPARTEOTRO';
end;

  // La fecha inicial es Sin Validacion
  // La licencia 1 inicia hasta el 1/Abril/2013
procedure TTestTRESSCFG_ValidacionBD_PruebasLicencia1.Validaciones_Al_Iniciar;
begin

  oLicencia.FechaActual := EncodeDate( 2013, 01, 01 );
  oConfig := TDbConfigManager.Create( oLicencia, oStoreHndlr );
  Check( oLicencia.GetGroupID = 0 , 'El MOD GROUP ID Debe Ser 0') ;
  Check(not oLicencia.DebeAdvertir, 'Al 1/1/13 No Debe Advertir');
  Check(not oLicencia.DebeValidar, 'Al 1/1/13 No Validara');
  oConfig.LoadDbConfig;
  Check(oConfig.PuedeAgregarDB, 'Al 1/1/13 De poder Agregar ( no valida )');
  OConfig.SaveDbConfig;
  oStoreHndlr.FEsInicial := False;
  oStoreHndlr.FComparteBD := 'SERVIDOR.COMPARTE';
  FreeAndNil( oConfig);
end;


  // La fecha inicial es Sin Validacion
  // La licencia 1 inicia hasta el 1/Abril/2013
procedure TTestTRESSCFG_ValidacionBD_PruebasLicencia1.AgregarBD_Antes_de_Validar;
var
 iDia : integer;
begin
  Validaciones_Al_Iniciar;

  oLicencia.FechaActual := EncodeDate( 2013, 02, 15 );
  oConfig := TDbConfigManager.Create( oLicencia, oStoreHndlr );
  oConfig.StoreHandler := oStoreHndlr;
  Check(not oLicencia.DebeAdvertir, 'Al 2/15/13 No Debe Advertir');
  Check(not oLicencia.DebeValidar, 'Al 2/15/13 No Validara');
  oConfig.LoadDbConfig;
  Check(oConfig.GetDbConfigRec.Cantidad = 0 , 'No Debe existir Conteo') ;
  Check(oConfig.PuedeAgregarDB, 'Al 2/15/13 Puede Agregar ( no cuenta , ni valida )');
  oConfig.IncrementarDB;
  OConfig.SaveDbConfig;
  FreeAndNil( oConfig);
  /////

  for iDia := 1 to 15 do
  begin
      oLicencia.FechaActual := EncodeDate( 2013, 03, iDia );
      oConfig := TDbConfigManager.Create( oLicencia, oStoreHndlr );
      oConfig.StoreHandler := oStoreHndlr;
      oConfig.LoadDbConfig;
      oConfig.IncrementarDB;
      OConfig.SaveDbConfig;
      Check(oConfig.GetDbConfigRec.Cantidad = 0 , 'Despues de Agregar, No Debe existir Conteo') ;
      FreeAndNil( oConfig);
  end;
end;

  // La fecha inicial es Con Validacion
  // La licencia 1 inicia hasta el 1/Abril/2013

procedure TTestTRESSCFG_ValidacionBD_PruebasLicencia1.AgregarBD_Antes_de_Topar;
var
 iDia : integer;
begin
     //Esta en el status posterior  .. Situado al 15 Junio,
     AgregarBD_Antes_de_Validar;
     oLicencia.FechaActual := EncodeDate( 2013, 06, 15);
     //Ya debe advertir
     oConfig := TDbConfigManager.Create( oLicencia, oStoreHndlr );
     oConfig.StoreHandler := oStoreHndlr;
     oConfig.LoadDbConfig;
     Check( oLicencia.DebeAdvertir , 'Al 15/Junio/2013 Debe Advertir' );
     Check( oLicencia.DebeValidar = False , 'Al 15/Junio/2013 No Debe Validar' );
     oConfig.IncrementarDB;
     OConfig.SaveDbConfig;
     Check(oConfig.GetDbConfigRec.Cantidad = 0 , 'Despues de Agregar, No Debe existir Conteo') ;
     FreeAndNil( oConfig);

     //La Licencia 1 solo tiene 10 Intentos
     for iDia := 1 to 10 do
     begin
         oLicencia.FechaActual := EncodeDate( 2013, 07, iDia );
         oConfig := TDbConfigManager.Create( oLicencia, oStoreHndlr );
         oConfig.StoreHandler := oStoreHndlr;
         oConfig.LoadDbConfig;
         oConfig.IncrementarDB;
         OConfig.SaveDbConfig;
         Check(oConfig.GetDbConfigRec.Cantidad = iDia , 'Despues de Agregar De Aumentar') ;
         FreeAndNil( oConfig);
     end;
end;

procedure TTestTRESSCFG_ValidacionBD_PruebasLicencia1.AgregarBD_Despues_de_Topar;
begin
     AgregarBD_Antes_de_Topar;
     //YA tiene los 10 Intentos
     oLicencia.FechaActual := EncodeDate( 2013, 07, 10 );
     oConfig := TDbConfigManager.Create( oLicencia, oStoreHndlr );
     oConfig.StoreHandler := oStoreHndlr;
     oConfig.LoadDbConfig;
     Check(oConfig.PuedeAgregarDB = False , 'No debera permitir Aumentar los intentos') ;
     oConfig.IncrementarDB;
     OConfig.SaveDbConfig;
     Check(oConfig.GetDbConfigRec.Cantidad = 10 , 'No debera permitir Aumentar los intentos ') ;
     FreeAndNil( oConfig);
end;


procedure TTestTRESSCFG_ValidacionBD_PruebasLicencia1.Cambiar_Fecha_Inicio;
begin
     AgregarBD_Antes_de_Validar;


end;


procedure TTestTRESSCFG_ValidacionBD_PruebasLicencia1.Cambiar_Comparte;


  procedure Incrementar( esperado : integer ; _oStoreHndlr : TStoreHandler  );
  begin
       //Incrementa

         oConfig := TDbConfigManager.Create( oLicencia, _oStoreHndlr );
         oConfig.LoadDbConfig;
         oConfig.IncrementarDB;
         OConfig.SaveDbConfig;
         Check(oConfig.GetDbConfigRec.Cantidad = esperado , 'Despues de Agregar De Aumentar') ;
         FreeAndNil( oConfig);
  end;

  procedure CrearKeysEnOtraBD;
  begin
       //Esta en el status posterior  .. Situado al 15 Marzo,
     oLicencia.FechaActual := EncodeDate( 2013, 06, 15);
     //Ya debe advertir
     oConfig := TDbConfigManager.Create( oLicencia, oStoreHndlrOtro );
     oConfig.LoadDbConfig;
     Check( oLicencia.DebeAdvertir , 'Al 15/Junio/2013 Debe Advertir' );
     Check( oLicencia.DebeValidar = False , 'Al 15/Junio/2013 No Debe Validar' );
     oConfig.IncrementarDB;
     OConfig.SaveDbConfig;
     Check(oConfig.GetDbConfigRec.Cantidad = 0 , 'Despues de Agregar, No Debe existir Conteo') ;
     FreeAndNil( oConfig);
     //A esta fecha ya valida
     oLicencia.FechaActual := EncodeDate( 2013, 07, 05 );
     Incrementar(1, oStoreHndlrOtro);
     Incrementar(2, oStoreHndlrOtro);
  end;

var
   iKey : integer;

begin
     //2013/01/01
     AgregarBD_Antes_de_Validar;
     //2013/03/15
     CrearKeysEnOtraBD;
     //2013/04/05

     //Reemplazamos la BD , conserva el mismo Registro
     for iKey := 0 to 9 do
          oStoreHndlr.FKeyArray[iKey] := oStoreHndlrOtro.FKeyArray[iKey];

     oConfig := TDbConfigManager.Create( oLicencia, oStoreHndlr );
     oConfig.StoreHandler := oStoreHndlr;
     oConfig.LoadDbConfig;
     oConfig.SaveDbConfig;
     FreeAndNil( oConfig );

     oConfig := TDbConfigManager.Create( oLicencia, oStoreHndlr );
     oConfig.StoreHandler := oStoreHndlr;
     oConfig.LoadDbConfig;
     Check(oConfig.GetDbConfigRec.Cantidad = 3 , 'Despues Cambiar de Comparte, De Aumentarse +1') ;
     FreeAndNil( oConfig );
     Incrementar(4, oStoreHndlr);
     Incrementar(5, oStoreHndlr);
end;


procedure TTestTRESSCFG_ValidacionBD_PruebasLicencia1.TearDown;
begin
  inherited;
  FreeAndNil( oLicencia );
  FreeAndNil( oStoreHndlr );
end;




{ TTestTRESSCFG_ValidacionBD_PruebasLicencia2  }

{
MaxConfigBD = 30
MesesRelativos = 0
NoSentinel = 1029 ,  mod 10 = 9
}

procedure TTestTRESSCFG_ValidacionBD_PruebasLicencia2.SetUp;
begin
  inherited;
  oLicencia := TLicencia.Create( nil);
  oLicencia.FMeses := 0;
  oLicencia.FMaxConfig := 30;
  oLicencia.FNoSentinel := 1049;
  oLicencia.FMaxEmpleados := 1000;
  oStoreHndlr := TStoreHandler.Create();
  oStoreHndlr.FEsInicial := True;
  oStoreHndlr.FComparteBD := 'SERVIDOR.COMPARTE';
end;

  // La fecha inicial es Sin Validacion
  // La licencia 1 inicia hasta el 1/Abril/2013
procedure TTestTRESSCFG_ValidacionBD_PruebasLicencia2.Validaciones_Al_Iniciar;
begin

  oLicencia.FechaActual := EncodeDate( 2013, 01, 01 );
  oConfig := TDbConfigManager.Create( oLicencia, oStoreHndlr );
  oConfig.StoreHandler := oStoreHndlr;
  Check( oLicencia.GetGroupID = 9 , 'El MOD GROUP ID Debe Ser 9') ;
  Check(not oLicencia.DebeAdvertir, 'Al 1/1/13 No Debe Advertir');
  Check(not oLicencia.DebeValidar, 'Al 1/1/13 No Validara');
  oConfig.LoadDbConfig;
  Check(oConfig.PuedeAgregarDB, 'Al 1/1/13 De poder Agregar ( no valida )');
  OConfig.SaveDbConfig;
  oStoreHndlr.FEsInicial := False;
  oStoreHndlr.FComparteBD := 'SERVIDOR.COMPARTE';  
  FreeAndNil( oConfig);

end;


  // La fecha inicial es Sin Validacion
  // La licencia 1 inicia hasta el 1/Abril/2013
procedure TTestTRESSCFG_ValidacionBD_PruebasLicencia2.AgregarBD_Antes_de_Validar;
var
 iDia : integer;
begin
  Validaciones_Al_Iniciar;

  oLicencia.FechaActual := EncodeDate( 2013, 02, 15 );
  oConfig := TDbConfigManager.Create( oLicencia, oStoreHndlr );
  oConfig.StoreHandler := oStoreHndlr;
  Check(not oLicencia.DebeAdvertir, 'Al 2/15/13 No Debe Advertir');
  Check(not oLicencia.DebeValidar, 'Al 2/15/13 No Validara');
  oConfig.LoadDbConfig;
  Check(oConfig.GetDbConfigRec.Cantidad = 0 , 'No Debe existir Conteo') ;
  Check(oConfig.PuedeAgregarDB, 'Al 2/15/13 Puede Agregar ( no cuenta , ni valida )');
  oConfig.IncrementarDB;
  OConfig.SaveDbConfig;
  FreeAndNil( oConfig);
  /////

  for iDia := 1 to 10 do
  begin
      oLicencia.FechaActual := EncodeDate( 2013, 06, iDia );
      oConfig := TDbConfigManager.Create( oLicencia, oStoreHndlr );
      oConfig.StoreHandler := oStoreHndlr;
      oConfig.LoadDbConfig;
      oConfig.IncrementarDB;
      OConfig.SaveDbConfig;
      Check(oConfig.GetDbConfigRec.Cantidad = 0 , 'Despues de Agregar, No Debe existir Conteo en Marzo') ;
      FreeAndNil( oConfig);
  end;

  for iDia := 1 to 10 do
  begin
      oLicencia.FechaActual := EncodeDate( 2013, 06, iDia );
      oConfig := TDbConfigManager.Create( oLicencia, oStoreHndlr );
      oConfig.StoreHandler := oStoreHndlr;
      oConfig.LoadDbConfig;
      oConfig.IncrementarDB;
      OConfig.SaveDbConfig;
      Check(oConfig.GetDbConfigRec.Cantidad = 0 , 'Despues de Agregar, No Debe existir Conteo en Junio') ;
      FreeAndNil( oConfig);
  end;

end;

  // La fecha inicial es Con Validacion
  // La licencia 1 inicia hasta el 1/Abril/2013

procedure TTestTRESSCFG_ValidacionBD_PruebasLicencia2.AgregarBD_Antes_de_Topar;
var
 iDia : integer;
begin
     //Esta en el status posterior  .. Situado al 15 Junio,
     AgregarBD_Antes_de_Validar;
     oLicencia.FechaActual := EncodeDate( 2013, 06, 15);
     //Ya debe advertir
     oConfig := TDbConfigManager.Create( oLicencia, oStoreHndlr );
     oConfig.StoreHandler := oStoreHndlr;
     oConfig.LoadDbConfig;
     Check( oLicencia.DebeAdvertir , 'Al 15/Junio/2013 Debe Advertir' );
     Check( oLicencia.DebeValidar = False , 'Al 15/Junio/2013 No Debe Validar' );
     oConfig.IncrementarDB;
     OConfig.SaveDbConfig;
     Check(oConfig.GetDbConfigRec.Cantidad = 0 , 'Despues de Agregar, No Debe existir Conteo en Junio') ;
     FreeAndNil( oConfig);

     //Se usan 10 de 30  Intentos en Julio
     for iDia := 1 to 10 do
     begin
         oLicencia.FechaActual := EncodeDate( 2013, 7, iDia );
         oConfig := TDbConfigManager.Create( oLicencia, oStoreHndlr );
         oConfig.StoreHandler := oStoreHndlr;
         oConfig.LoadDbConfig;
         oConfig.IncrementarDB;
         OConfig.SaveDbConfig;
         Check(oConfig.GetDbConfigRec.Cantidad = iDia , 'Despues de Agregar De Aumentar') ;
         FreeAndNil( oConfig);
     end;

       //Se usan 20 de 30  Intentos en Enero 2014
     for iDia := 1 to 10 do
     begin
         oLicencia.FechaActual := EncodeDate( 2014, 1, iDia );
         oConfig := TDbConfigManager.Create( oLicencia, oStoreHndlr );
         oConfig.StoreHandler := oStoreHndlr;
         oConfig.LoadDbConfig;
         oConfig.IncrementarDB;
         OConfig.SaveDbConfig;
         FreeAndNil( oConfig);
     end;

       //Se usan 30 de 30  Intentos en Agosto 2014
     for iDia := 1 to 30  do
     begin
         oLicencia.FechaActual := EncodeDate( 2014, 8, iDia );
         oConfig := TDbConfigManager.Create( oLicencia, oStoreHndlr );
         oConfig.StoreHandler := oStoreHndlr;
         oConfig.LoadDbConfig;
         oConfig.IncrementarDB;
         OConfig.SaveDbConfig;
         FreeAndNil( oConfig);
     end;

     oConfig := TDbConfigManager.Create( oLicencia, oStoreHndlr );
     oConfig.StoreHandler := oStoreHndlr;
     oConfig.LoadDbConfig;
     Check(oConfig.GetDbConfigRec.Cantidad = 30 , 'Despues de Agregar De Aumentar deben ser 30') ;
     FreeAndNil( oConfig);
end;

procedure TTestTRESSCFG_ValidacionBD_PruebasLicencia2.AgregarBD_Despues_de_Topar;
begin
     AgregarBD_Antes_de_Topar;
     //YA tiene los 30 Intentos
     oLicencia.FechaActual := EncodeDate( 2014, 9, 1 );
     oConfig := TDbConfigManager.Create( oLicencia, oStoreHndlr );
     oConfig.StoreHandler := oStoreHndlr;
     oConfig.LoadDbConfig;
     Check(oConfig.PuedeAgregarDB = False , 'No debera permitir Aumentar los intentos') ;
     oConfig.IncrementarDB;
     OConfig.SaveDbConfig;
     Check(oConfig.GetDbConfigRec.Cantidad = 30 , 'No debera permitir Aumentar los intentos ') ;
     FreeAndNil( oConfig);
end;

procedure TTestTRESSCFG_ValidacionBD_PruebasLicencia2.Extender_Cantidad_10;
var
 iDia : integer;
begin
     AgregarBD_Despues_de_Topar;
     oLicencia.FMaxConfig := oLicencia.FMaxConfig + 10;

     oLicencia.FechaActual := EncodeDate( 2014, 9, 3 );

     //Se usan 40 de 40  Intentos en Agosto 2014
     for iDia := 1 to 10 do
     begin
         oLicencia.FechaActual := EncodeDate( 2014, 10, iDia );
         oConfig := TDbConfigManager.Create( oLicencia, oStoreHndlr );
         oConfig.StoreHandler := oStoreHndlr;
         oConfig.LoadDbConfig;
         oConfig.IncrementarDB;
         OConfig.SaveDbConfig;
         FreeAndNil( oConfig);
     end;

     oConfig := TDbConfigManager.Create( oLicencia, oStoreHndlr );
     oConfig.StoreHandler := oStoreHndlr;
     oConfig.LoadDbConfig;
     Check(oConfig.GetDbConfigRec.Cantidad = 40 , 'Deber� permitir Aumentar los nuevos Intentos ') ;
     FreeAndNil( oConfig);

end;

procedure TTestTRESSCFG_ValidacionBD_PruebasLicencia2.AgregarBD_Despues_de_Extender;
begin
     Extender_Cantidad_10;
     oConfig := TDbConfigManager.Create( oLicencia, oStoreHndlr );
     oConfig.StoreHandler := oStoreHndlr;
     oConfig.LoadDbConfig;
     Check( oConfig.PuedeAgregarDB = false , 'Ya sobrepas� los 40 intentos no deber� permitir Aumentar los nuevos Intentos ') ;
     FreeAndNil( oConfig);
end;





procedure TTestTRESSCFG_ValidacionBD_PruebasLicencia2.TearDown;
begin
  inherited;
  FreeAndNil( oLicencia );
  FreeAndNil( oStoreHndlr );
end;



{ TLicencia }
function TLicencia.GetMaxConfigBD: integer;
begin
   Result := FMaxConfig;
end;

function TLicencia.GetMaxEmpleados: integer;
begin
  Result := FMaxEmpleados;
end;

function TLicencia.GetMesesRelativos: integer;
begin
   Result := FMeses;
end;

function TLicencia.GetNoSentinel: integer;
begin
   Result := FNoSentinel;
end;


{ TStoreHandler }

function TStoreHandler.EsDbConfigInicial: boolean;
begin
   result := FEsInicial;
end;

function TStoreHandler.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  Result := 0;
end;

function TStoreHandler.ReadDBConfigFromDatabase(iKey: integer): string;
begin
     result := FKeyArray[iKey];
end;

function TStoreHandler.ReadRegistryChecksum: string;
begin
    result := FRegValue;
end;

function TStoreHandler.ReadRegistryComparteBD: string;
begin
     Result := FComparteBD;
end;

function TStoreHandler.WriteDBConfigFromDatabase(iKey: integer;
  encryptedValue: string): boolean;
begin
    FKeyArray[iKey]:= encryptedValue;
    Result := True;
end;

function TStoreHandler.WriteRegistryChecksum(
  encryptedValue: string): boolean;
begin
    FRegValue := encryptedValue;
    Result := True;
end;

function TStoreHandler._AddRef: Integer;
begin
  Result := 0;
end;

function TStoreHandler._Release: Integer;
begin
  Result := 0;
end;




procedure TTestTRESSCFG_ValidacionBD_PruebasLicencia2.Empleados_Silencio;
begin
  Validaciones_Al_Iniciar;
  oLicencia.FechaActual := EncodeDate( 2013, 05, 15 );
  Check( oLicencia.DebeAdvertir = false , 'No debe advertir') ;
  Check( oLicencia.DebeValidar = false , 'No debe validar') ;
  //evIgnorar, evDebajoUmbral, evEnUmbral, evRebasaAdvierte, evRebasaRestringe

  oLicencia.TotalGlobal := 1;
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evIgnorar , 'Evaluar Empleados = Ignorar');
  oLicencia.TotalGlobal := 100;
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evIgnorar , 'Evaluar Empleados = Ignorar');
  oLicencia.TotalGlobal := 990;
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evIgnorar , 'Evaluar Empleados = Ignorar');
  oLicencia.TotalGlobal := 1000;
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evIgnorar , 'Evaluar Empleados = Ignorar');
  oLicencia.TotalGlobal := 1500;
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evIgnorar , 'Evaluar Empleados = Ignorar');
  oLicencia.TotalGlobal := 10500;
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evIgnorar , 'Evaluar Empleados = Ignorar');

end;


procedure TTestTRESSCFG_ValidacionBD_PruebasLicencia2.Empleados_Advertencia;
var
   sMensajeAdvertencia, sMensajeError : string;
begin
  Validaciones_Al_Iniciar;
  oLicencia.FMaxEmpleados := 1000;
  oLicencia.FechaActual := EncodeDate( 2013, 06, 15 );
  Check( oLicencia.DebeAdvertir = true , 'Si debe advertir') ;
  Check( oLicencia.DebeValidar = false , 'No debe validar') ;

  oLicencia.TotalGlobal := 1;
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evDebajoUmbral , 'Evaluar Empleados 1 = evDebajoUmbral');

  oLicencia.TotalGlobal := 100;
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evDebajoUmbral , 'Evaluar Empleados  100 = evDebajoUmbral');

  oLicencia.TotalGlobal := 950;
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evEnUmbral , 'Evaluar Empleados 950 = evEnUmbral');

  oLicencia.TotalGlobal := 980;
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evEnUmbral , 'Evaluar Empleados 980 = evEnUmbral');

  sMensajeAdvertencia := '';
  sMensajeError := '';
  oLicencia.MostrarAdvertencia := TRUE;
  oLicencia.EvaluarEmpleadosMensaje( evOpAlta, sMensajeAdvertencia, sMensajeError );
  Check( (sMensajeAdvertencia <> '' ) and (sMensajeError =  '') , 'Debe Advertir UMBRAL Empleados Mensaje 980 = evEnUmbral');

  oLicencia.TotalGlobal := 1000;
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evEnUmbral , 'Evaluar Empleados 1000 = evEnUmbral');

  oLicencia.TotalGlobal := 1001;
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evRebasaAdvierte , 'Evaluar Empleados 1001= evRebasaAdvierte');

  oLicencia.TotalGlobal := 10500;
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evRebasaAdvierte , 'Evaluar Empleados 10500 = evRebasaAdvierte');

  sMensajeAdvertencia := '';
  sMensajeError := '';
  oLicencia.EvaluarEmpleadosMensaje( evOpAlta, sMensajeAdvertencia, sMensajeError );
  Check( (sMensajeAdvertencia <> '' ) and (sMensajeError = '') , 'Debe Advertir Empleados Mensaje 10500 = evRebasaAdvierte');


end;

procedure TTestTRESSCFG_ValidacionBD_PruebasLicencia2.Empleados_Restringe;
var
   sMensajeAdvertencia, sMensajeError : string;
begin
  Validaciones_Al_Iniciar;
  oLicencia.FechaActual := EncodeDate( 2013, 07, 15 );
  oLicencia.FMaxEmpleados := 1000;
  oLicencia.UTEsDemo := false;
  Check( oLicencia.DebeAdvertir = true , 'Si debe advertir') ;
  Check( oLicencia.DebeValidar = true , 'Si debe validar') ;

  oLicencia.TotalGlobal := 1;
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evDebajoUmbral , 'Evaluar Empleados 1 = evDebajoUmbral');

  oLicencia.TotalGlobal := 100;
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evDebajoUmbral , 'Evaluar Empleados  100 = evDebajoUmbral');

  oLicencia.TotalGlobal := 950;
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evEnUmbral , 'Evaluar Empleados 950 = evEnUmbral');

  oLicencia.TotalGlobal := 980;
  sMensajeAdvertencia := '';
  sMensajeError := '';
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evEnUmbral , 'Evaluar Empleados 980 = evEnUmbral');

    oLicencia.MostrarAdvertencia := TRUE;
  oLicencia.EvaluarEmpleadosMensaje( evOpAlta, sMensajeAdvertencia, sMensajeError );
  Check( (sMensajeAdvertencia <> '' ) and (sMensajeError = '') , 'Debe Advertir Evaluar Empleados Mensaje 980 = evEnUmbral');


  oLicencia.TotalGlobal := 1000;
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evEnUmbral , 'Evaluar Empleados 1000 = evEnUmbral');

  oLicencia.TotalGlobal := 1001;
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evRebasaRestringe , 'Evaluar Empleados 1001= evRebasaRestringe');

  oLicencia.TotalGlobal := 10500;
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evRebasaRestringe , 'Evaluar Empleados 10500 = evRebasaRestringe');

  oLicencia.TotalGlobal := 10500;
  sMensajeAdvertencia := '';
  sMensajeError := '';
  oLicencia.EvaluarEmpleadosMensaje( evOpAlta, sMensajeAdvertencia, sMensajeError );
  Check( (sMensajeAdvertencia = '' ) and (sMensajeError<> '') , 'Debe Restringir Empleados Mensaje 10500 = evRebasaRestringe');

end;


procedure TTestTRESSCFG_ValidacionBD_PruebasLicencia2.Empleados_Restringe_Demo;
var
   sMensajeAdvertencia, sMensajeError : string;
begin
  Validaciones_Al_Iniciar;
  oLicencia.FechaActual := EncodeDate( 2013, 07, 15 );
  oLicencia.UTEsDemo := True;
  oLicencia.FMaxEmpleados := 300;

  Check( oLicencia.DebeAdvertir = true , 'Si debe advertir') ;
  Check( oLicencia.DebeValidar = true , 'Si debe validar') ;

  oLicencia.TotalGlobal := 1;
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evDebajoUmbral , 'Evaluar Empleados 1 = evDebajoUmbral');

  oLicencia.TotalGlobal := 100;
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evDebajoUmbral , 'Evaluar Empleados  100 = evDebajoUmbral');

  oLicencia.TotalGlobal := 285;
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evEnUmbral , 'Evaluar Empleados 280 = evEnUmbral');

  oLicencia.TotalGlobal := 295;
  sMensajeAdvertencia := '';
  sMensajeError := '';
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evEnUmbral , 'Evaluar Empleados 295 = evEnUmbral');

    oLicencia.MostrarAdvertencia := TRUE;
  oLicencia.EvaluarEmpleadosMensaje( evOpAlta, sMensajeAdvertencia, sMensajeError );
  Check( (sMensajeAdvertencia <> '' ) and (sMensajeError = '') , 'Debe Advertir Evaluar Empleados Mensaje 295 = evEnUmbral');


  oLicencia.TotalGlobal := 300;
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evEnUmbral , 'Evaluar Empleados 300 = evEnUmbral');

  oLicencia.TotalGlobal := 301;
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evRebasaRestringeDemo , 'Evaluar Empleados 301= evRebasaRestringe Cuando ALTA');
  sMensajeAdvertencia := '';
  sMensajeError := '';
  oLicencia.EvaluarEmpleadosMensaje( evOpAlta, sMensajeAdvertencia, sMensajeError );
  Check( (sMensajeAdvertencia = '' ) and (sMensajeError = K_LIC_MSG_REBASA_RESTRINGE_DEMO ) , 'Mensaje Restriccion' );

  oLicencia.TotalGlobal := 301;
  Check( oLicencia.EvaluarEmpleados( evOpNomina ) = evRebasaAdvierteDemo , 'Evaluar Empleados 301= evRebasaAdvierte Cuando NOMINA');


  oLicencia.TotalGlobal := 10500;
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evRebasaRestringeDemo , 'Evaluar Empleados 10500 = evRebasaRestringeDEMO');

  oLicencia.TotalGlobal := 10500;
  sMensajeAdvertencia := '';
  sMensajeError := '';
  oLicencia.EvaluarEmpleadosMensaje( evOpAlta, sMensajeAdvertencia, sMensajeError );
  Check( (sMensajeAdvertencia = '' ) and (sMensajeError<> '') , 'Debe Restringir Empleados Mensaje 10500 = evRebasaRestringeDEMO');
  Check( (sMensajeAdvertencia = '' ) and (sMensajeError = K_LIC_MSG_REBASA_RESTRINGE_DEMO ) , 'Mensaje Restriccion' );

end;


procedure TTestTRESSCFG_ValidacionBD_PruebasLicencia2.Empleados_Restringe_NoDEMO;
var
   sMensajeAdvertencia, sMensajeError : string;
begin
  Validaciones_Al_Iniciar;
  oLicencia.FechaActual := EncodeDate( 2013, 07, 15 );
  oLicencia.UTEsDemo := False;
  oLicencia.FMaxEmpleados := 300;

  Check( oLicencia.DebeAdvertir = true , 'Si debe advertir') ;
  Check( oLicencia.DebeValidar = true , 'Si debe validar') ;

  oLicencia.TotalGlobal := 1;
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evDebajoUmbral , 'Evaluar Empleados 1 = evDebajoUmbral');

  oLicencia.TotalGlobal := 100;
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evDebajoUmbral , 'Evaluar Empleados  100 = evDebajoUmbral');

  oLicencia.TotalGlobal := 285;
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evEnUmbral , 'Evaluar Empleados 280 = evEnUmbral');

  oLicencia.TotalGlobal := 295;
  sMensajeAdvertencia := '';
  sMensajeError := '';
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evEnUmbral , 'Evaluar Empleados 295 = evEnUmbral');

    oLicencia.MostrarAdvertencia := TRUE;
  oLicencia.EvaluarEmpleadosMensaje( evOpAlta, sMensajeAdvertencia, sMensajeError );
  Check( (sMensajeAdvertencia <> '' ) and (sMensajeError = '') , 'Debe Advertir Evaluar Empleados Mensaje 295 = evEnUmbral');


  oLicencia.TotalGlobal := 300;
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evEnUmbral , 'Evaluar Empleados 300 = evEnUmbral');

  oLicencia.TotalGlobal := 301;
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evRebasaRestringe , 'Evaluar Empleados 301= evRebasaRestringe Cuando ALTA');

  oLicencia.TotalGlobal := 301;
  Check( oLicencia.EvaluarEmpleados( evOpNomina ) = evRebasaRestringe , 'Evaluar Empleados 301= evRebasaAdvierte Cuando NOMINA');

  oLicencia.TotalGlobal := 10500;
  Check( oLicencia.EvaluarEmpleados( evOpAlta ) = evRebasaRestringe , 'Evaluar Empleados 10500 = evRebasaRestringe');

  oLicencia.TotalGlobal := 10500;
  sMensajeAdvertencia := '';
  sMensajeError := '';
  oLicencia.EvaluarEmpleadosMensaje( evOpAlta, sMensajeAdvertencia, sMensajeError );
  Check( (sMensajeAdvertencia = '' ) and (sMensajeError<> '') , 'Debe Restringir Empleados Mensaje 10500 = evRebasaRestringe');

end;


initialization
  RegisterTest('Pruebas de Validacion a Licencia 1', TTestTRESSCFG_ValidacionBD_PruebasLicencia1.Suite);
  RegisterTest('Pruebas de Validacion a Licencia 2', TTestTRESSCFG_ValidacionBD_PruebasLicencia2.Suite);


end.
