unit DMonitorService;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      DMonitorService.pas                        ::
  :: Descripci�n: Programa principal de Tress Monitor.exe    ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, SvcMgr, Dialogs,
     DMonitorTress, //DinterBase,
     DMonitor;

type
  TMonitorService = class(TService)
    procedure ServiceCreate(Sender: TObject);
    procedure ServiceDestroy(Sender: TObject);
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceExecute(Sender: TService);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
    procedure ServicePause(Sender: TService; var Paused: Boolean);
    procedure ServiceContinue(Sender: TService; var Continued: Boolean);
    procedure ServiceShutdown(Sender: TService);
  private
    { Private declarations }
    FMonitor: TMonitor;
    procedure EscribeBitacora(const sMensaje: String);
  public
    { Public declarations }
    function GetServiceController: TServiceController; override;
  end;

var
  MonitorService: TMonitorService;

implementation

{$R *.DFM}

procedure ServiceController( CtrlCode: DWord ); stdcall;
begin
     MonitorService.Controller( CtrlCode );
end;

function TMonitorService.GetServiceController: TServiceController;
begin
     Result := ServiceController;
end;

{ ******* TSentinel ******** }

procedure TMonitorService.ServiceCreate(Sender: TObject);
begin
     dmMonitorTress := tdmMonitorTress.Create( Self );
     //dmInterbase := TdmInterbase.Create( self );
     FMonitor := TMonitor.Create;
end;

procedure TMonitorService.ServiceDestroy(Sender: TObject);
begin
     FreeAndNil( dmMonitorTress ); //dmInterbase.Free;
     FreeAndNil( FMonitor );
end;

procedure TMonitorService.ServiceExecute(Sender: TService);
const
     K_INCREMENTO_MILISEGUNDOS = 1000;
var
   iMSeg : Integer;
begin
     while not Terminated do
     begin
          try
             FMonitor.RunChecks;
          except
                on Error: Exception do
                begin
                     EscribeBitacora( 'Error Al Monitorear: ' + Error.Message );
                end;
          end;
          iMSeg := 0;
          Repeat
                Sleep( K_INCREMENTO_MILISEGUNDOS );
                ServiceThread.ProcessRequests( False );
                iMSeg := iMSeg + K_INCREMENTO_MILISEGUNDOS;
          Until ( Terminated or ( iMSeg >= FMonitor.WaitInterval ) );
     end;
end;

procedure TMonitorService.EscribeBitacora(const sMensaje: String);
begin
     try
        LogMessage( sMensaje, EVENTLOG_INFORMATION_TYPE, 0, 0 );
     except
           LogMessage( 'Error al Escribir a Bit�cora', EVENTLOG_ERROR_TYPE, 0, 0 );
     end;
end;

procedure TMonitorService.ServiceStart(Sender: TService; var Started: Boolean);
begin
     Started := FMonitor.Start;
     EscribeBitacora( 'Tress Monitor Starting' );
end;

procedure TMonitorService.ServiceStop(Sender: TService; var Stopped: Boolean);
begin
     Stopped := FMonitor.Stop;
     EscribeBitacora( 'Tress Monitor Stopping' );
end;

procedure TMonitorService.ServicePause(Sender: TService; var Paused: Boolean);
begin
     FMonitor.Pause;
     EscribeBitacora( 'Tress Monitor Pausing' );
end;

procedure TMonitorService.ServiceContinue(Sender: TService; var Continued: Boolean);
begin
     FMonitor.Continue;
     EscribeBitacora( 'Tress Monitor Continues Executing' );
end;

procedure TMonitorService.ServiceShutdown(Sender: TService);
begin
     FMonitor.Shutdown;
     EscribeBitacora( 'Tress Monitor Shutting Down' );
end;

end.
