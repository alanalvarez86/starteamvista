unit DMonitorRegistry;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Registry;

type
  TMonitorRegistry = class(TObject)
  private
    { Private declarations }
    FRegistry: TRegIniFile;
    FRegistryIB: TRegIniFile;
    function GetCreateOutputFile: Boolean;
    function GetHardDiskSpaceLimitRed: Word;
    function GetHardDiskSpaceLimitYellow: Word;
    function GetCopyDirectory: String;
    function GetHardDiskFrequency: string;
    function GetHardDisks: string;
    function GetInterbaseServicesFrequency: string;
    function GetInterbaseLogFrequency: string;
    function GetPollHoras: string;
    function GetPollCompanys: string;
    function GetInterbaseGBKFrequency: string;
    function GetInterbaseGBKHoras: string;
    function GetDirectoryGBK: String;
    function GetDBConfigurationFrequency: string;
    function GetDBProcessesFrequency: string;
    function GetDBProcessesTime: Word;
    function GetDBProcessesEmpleados: Word;
    function GetDBLimit: Word;
    function GetLicenseSentinelFrequency: string;
    function GetLicenseUseFrequency: string;
    function GetLicenseUseDaysLimit: Word;
    function GetODBCFrequency: string;
    function GetRootDirectory: string;
    function GetWaitInterval: Word;
    function GetOutputDirectory: String;
    function GetHardDisksAvail: boolean;
    function GetPollAvail: boolean;
    function GetProcessAvail: boolean;
    function GetServicesAvail: boolean;
    function GetArchivosGBKAvail: boolean;
    function GetInterbaseLogAvail: boolean;
    function GetLicenciaUsoAvail: boolean;
    function GetODBCAvail: boolean;
    function GetSentinelSuperProAvail: boolean;
    function GetHost: string;
    function GetPort: word;
    function GetRemitente: string;
    function GetUserID: string;
    function GetListaCorreos: string;
    function GetListaTestsIB: string;
    function GetListaTestsMSSQL: string;
    function GetCopyDirectoryAvail: boolean;
    function GetEmailAvail: boolean;
    function GetFechaArchivo: TDateTime;
    function GetIdioma: boolean;
    function GetAdvertencia: boolean;
    function GetHTML: boolean;
    function GetOK: boolean;
    function GetProblema: boolean;
    procedure SetHardDiskFrequency(const Value: string);
    procedure SetHardDisks(const Value: string);
    procedure SetInterbaseServicesFrequency(const Value: string);
    procedure SetInterbaseLogFrequency(const Value: string);
    procedure SetPollHoras(const Value: string);
    procedure SetPollCompanys(const Value: string);
    procedure SetInterbaseGBKFrequency(const Value: string);
    procedure SetInterbaseGBKHoras(const Value: string);
    procedure SetDirectoryGBK(const Value: string);
    procedure SetDBConfigurationFrequency(const Value: string);
    procedure SetDBProcessesFrequency(const Value: string);
    procedure SetDBProcessesTime(const Value: Word);
    procedure SetDBProcessesEmpleados(const Value: Word);
    procedure SetDBLimit(const Value: Word);
    procedure SetLicenseSentinelFrequency(const Value: string);
    procedure SetLicenseUseFrequency(const Value: string);
    procedure SetLicenseUseDaysLimit(const Value: Word);
    procedure SetODBCFrequency(const Value: string);
    procedure SetCreateOutputFile(const Value: Boolean);
    procedure SetHardDiskSpaceLimitRed(const Value: Word);
    procedure SetHardDiskSpaceLimitYellow(const Value: Word);
    procedure SetCopyDirectory(const Value: String);
    procedure SetWaitInterval(const Value: Word);
    procedure SetOutputDirectory(const Value: String);
    procedure SetHardDisksAvail(const Value: boolean);
    procedure SetPollAvail(const Value: boolean);
    procedure SetProcessAvail(const Value: boolean);
    procedure SetServicesAvail(const Value: boolean);
    procedure SetArchivosGBKAvail(const Value: boolean);
    procedure SetInterbaseLogAvail(const Value: boolean);
    procedure SetLicenciaUsoAvail(const Value: boolean);
    procedure SetODBCAvail(const Value: boolean);
    procedure SetSentinelSuperProAvail(const Value: boolean);
    procedure SetListaCorreos(const Value: string);
    procedure SetListaTestsIB(const Value: string);
    procedure SetHost(const Value: string);
    procedure SetPort(const Value: word);
    procedure SetRemitente(const Value: string);
    procedure SetUserID(const Value: string);
    procedure SetListaTestsMSSQL(const Value: string);
    procedure SetCopyDirectoryAvail(const Value: boolean);
    procedure SetEmailAvail(const Value: boolean);
    procedure SetFechaArchivo(const Value: TDateTime);
    procedure SetIdioma(const Value: boolean);
    procedure SetAdvertencia(const Value: boolean);
    procedure SetHTML(const Value: boolean);
    procedure SetOK(const Value: boolean);
    procedure SetProblema(const Value: boolean);
  protected
    { Protected declarations }
    property Registry: TRegIniFile read FRegistry;
    property RegistryIB: TRegIniFile read FRegistryIB;
  public
    { Public declarations }
    constructor Create; virtual;
    destructor Destroy; override;
    property OutputDirectory: String read GetOutputDirectory write SetOutputDirectory;
    property CopyDirectory: String read GetCopyDirectory write SetCopyDirectory;
    property CreateOutputFile: Boolean read GetCreateOutputFile write SetCreateOutputFile;
    property HardDiskFrequency: string read GetHardDiskFrequency write SetHardDiskFrequency;
    property HardDiskSpaceLimitYellow: Word read GetHardDiskSpaceLimitYellow write SetHardDiskSpaceLimitYellow;
    property HardDiskSpaceLimitRed: Word read GetHardDiskSpaceLimitRed write SetHardDiskSpaceLimitRed;
    property HardDisks: string read GetHardDisks write SetHardDisks;
    property HardDisksAvail: boolean read GetHardDisksAvail write SetHardDisksAvail;
    property InterbaseServicesFrequency: string read GetInterbaseServicesFrequency write SetInterbaseServicesFrequency;
    property InterbaseLogFrequency: string read GetInterbaseLogFrequency write SetInterbaseLogFrequency;
    property PollHoras: string read GetPollHoras write SetPollHoras;
    property PollCompanys: string read GetPollCompanys write SetPollCompanys;
    property PollAvail: boolean read GetPollAvail write SetPollAvail;
    property ServicesAvail: boolean read GetServicesAvail write SetServicesAvail;
    property InterbaseLogAvail: boolean read GetInterbaseLogAvail write SetInterbaseLogAvail;
    property ArchivosGBKAvail: boolean read GetArchivosGBKAvail write SetArchivosGBKAvail;
    property SentinelSuperProAvail: boolean read GetSentinelSuperProAvail write SetSentinelSuperProAvail;
    property LicenciaUsoAvail: boolean read GetLicenciaUsoAvail write SetLicenciaUsoAvail;
    property CopyDirectoryAvail: boolean read GetCopyDirectoryAvail write SetCopyDirectoryAvail;
    property EmailAvail: boolean read GetEmailAvail write SetEmailAvail;
    property ODBCAvail: boolean read GetODBCAvail write SetODBCAvail;
    property ProcessAvail: boolean read GetProcessAvail write SetProcessAvail;
    property InterbaseGBKFrequency: string read GetInterbaseGBKFrequency write SetInterbaseGBKFrequency;
    property InterbaseGBKHoras: string read GetInterbaseGBKHoras write SetInterbaseGBKHoras;
    property DirectoryGBK: String read GetDirectoryGBK write SetDirectoryGBK;
    property DBConfigurationFrequency: string read GetDBConfigurationFrequency write SetDBConfigurationFrequency;
    property DBProcessesFrequency: string read GetDBProcessesFrequency write SetDBProcessesFrequency;
    property DBProcessesTime: Word read GetDBProcessesTime write SetDBProcessesTime;
    property DBProcessesEmpleados: Word read GetDBProcessesEmpleados write SetDBProcessesEmpleados;
    property DBLimit: Word read GetDBLimit write SetDBLimit;
    property LicenseSentinelFrequency: string read GetLicenseSentinelFrequency write SetLicenseSentinelFrequency;
    property LicenseUseFrequency: string read GetLicenseUseFrequency write SetLicenseUseFrequency;
    property LicenseUseDaysLimit: Word read GetLicenseUseDaysLimit write SetLicenseUseDaysLimit;
    property ODBCFrequency: string read GetODBCFrequency write SetODBCFrequency;
    property ListaCorreos: string read GetListaCorreos write SetListaCorreos;
    property ListaTestsIB: string read GetListaTestsIB write SetListaTestsIB;
    property ListaTestsMSSQL: string read GetListaTestsMSSQL write SetListaTestsMSSQL;
    property WaitInterval: Word read GetWaitInterval write SetWaitInterval;
    property RootDirectory: string read GetRootDirectory;
    property Host: string read GetHost write SetHost;
    property Port: word read GetPort write SetPort;
    property UserID: string read GetUserID write SetUserID;
    property Remitente: string read GetRemitente write SetRemitente;
    property FechaArchivo: TDateTime read GetFechaArchivo write SetFechaArchivo;
    property Idioma: boolean read GetIdioma write SetIdioma;
    property OK: boolean read GetOK write SetOK;
    property Advertencia: boolean read GetAdvertencia write SetAdvertencia;
    property Problema: boolean read GetProblema write SetProblema;
    property HTML: boolean read GetHTML write SetHTML;
  end;

implementation

uses ZetaServerTools,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaWinAPITools;

const
     { ***** Contantes del Registry ***** }
     MONITOR_REGISTRY_PATH                = 'Software\Grupo Tress\TressWin\Monitor';
     MONITOR_OUTPUT_FOLDER                = 'OutputDirectory';
     MONITOR_COPY_FOLDER                  = 'CopyDirectory';
     MONITOR_CREATE_OUTPUT_FOLDER         = 'CreateOutputFolder';
     MONITOR_HARD_DISK_LIMIT_YELLOW       = 'HardDiskLimitYellow';
     MONITOR_HARD_DISK_LIMIT_RED          = 'HardDiskLimitRed';
     MONITOR_HARD_DISK_FREQUENCY          = 'HardDiskFrequency';
     MONITOR_HARD_DISKS                   = 'HardDisks';
     MONITOR_HARD_DISKS_AVAIL             = 'HardDisksAvail';
     MONITOR_INTERBASE_SERVICES_FREQUENCY = 'InterbaseServicesFrequency';
     MONITOR_INTERBASE_LOG_FREQUENCY      = 'InterbaseLogFrequency';
     MONITOR_POLL_HORAS                   = 'PollHoras';
     MONITOR_POLL_COMPANYS                = 'PollCompanys';
     MONITOR_POLL_AVAIL                   = 'PollAvail';
     MONITOR_PROCESS_AVAIL                = 'ProcessAvail';
     MONITOR_SERVICES_AVAIL               = 'ServicesAvail';
     MONITOR_INTERBASELOG_AVAIL           = 'InterBaseLogAvail';
     MONITOR_ARCHIVOSGBK_AVAIL            = 'ArchivosGBKAvail';
     MONITOR_SENTINELSUPERPRO_AVAIL       = 'SentinelSuperProAvail';
     MONITOR_LICENCIAUSO_AVAIL            = 'LicenciaUsoAvail';
     MONITOR_ODBC_AVAIL                   = 'ODBCAvail';
     MONITOR_COPY_DIRECTORY_AVAIL         = 'CopyDirectoryAvail';
     MONITOR_EMAIL_AVAIL                  = 'EmailAvail';
     MONITOR_INTERBASE_GBK_FREQUENCY      = 'InterbaseGBKFrequency';
     MONITOR_INTERBASE_GBK_HORAS          = 'InterbaseGBKHoras';
     MONITOR_DIRECTORY_GBK                = 'DirectoryGBK';
     MONITOR_DB_CONFIGURATION_FREQUENCY   = 'DBConfigurationFrequency';
     MONITOR_DB_PROCESSES_FREQUENCY       = 'DBProcessesFrequency';
     MONITOR_DB_PROCESSES_TIME            = 'DBProcessesTime';
     MONITOR_DB_PROCESSES_EMPLEADOS       = 'DBProcessesEmpleados';
     MONITOR_DB_LIMIT                     = 'DBLimit';
     MONITOR_LICENSE_SENTINEL_FREQUENCY   = 'LicenseSentinelFrequency';
     MONITOR_LICENSE_USE_FREQUENCY        = 'LicenseUseFrequency';
     MONITOR_LICENSE_USE_DAYS_LIMIT       = 'LicenseUseDaysLimit';
     MONITOR_ODBC_FREQUENCY               = 'ODBCFrequency';
     MONITOR_WAIT_INTERVAL                = 'WaitInterval';
     MONITOR_LISTA_CORREOS                = 'ListaCorreos';
     MONITOR_LISTA_TESTS_IB               = 'ListaTestsIB';
     MONITOR_LISTA_TESTS_MSSQL            = 'ListaTestsMSSQL';
     MONITOR_CORREO_HOST                  = 'Host';
     MONITOR_CORREO_PORT                  = 'Port';
     MONITOR_CORREO_USERID                = 'UserID';
     MONITOR_CORREO_REMITENTE             = 'Remitente';
     MONITOR_ARCHIVO_DIA                  = 'Dia';
     MONITOR_ARCHIVO_MES                  = 'Mes';
     MONITOR_ARCHIVO_ANIO                 = 'Anio';
     MONITOR_IDIOMA                       = 'Idioma';
     MONITOR_TEST_OK                      = 'OK';
     MONITOR_TEST_ADVERTENCIA             = 'Advertencia';
     MONITOR_TEST_PROBLEMA                = 'Problema';
     MONITOR_TEST_HTML                    = 'HTML';
     IB_REGISTRY_PATH                     = 'Software\InterBase Corp\InterBase\CurrentVersion';
     IB_REGISTRY_ROOT                     = 'RootDirectory';

     { ****** Constantes de uso Com�n ******}
     K_HORA                               = '0015';
     K_LIMITE_MINIMO_EMPLEADOS            = 200;
     K_LIMITE_MAYOR_EMPLEADOS             = 300;
     K_TIEMPO_PROCESOS                    = 120;
     K_RED_LIMIT                          = 85;
     K_YELLOW_LIMIT                       = 75;
     K_DIAS_LICENCIA                      = 7;
     K_TIEMPO_ESPERA                      = 60;
     K_POLL_HORAS                         = '1600';
     K_DIA_COMPLETO                       = '2400';
     K_EMPRESA_DEFAULT                    = 'SINDICO';
     K_HOST_DEFAULT                       = '209.205.207.194';
     K_PORT_DEFAULT                       = 25;
     K_REMITENTE_DEFAULT                  = 'monitor@tress.com.mx';
     K_USERID_DEFAULT                     = 'Tress';

{ ************ TZetaRegistryServer *********** }

constructor TMonitorRegistry.Create;
begin
     FRegistry := TRegIniFile.Create;
     with FRegistry do
     begin
          RootKey := HKEY_LOCAL_MACHINE;
          OpenKey( MONITOR_REGISTRY_PATH, TRUE );
          LazyWrite := False;
     end;
     FRegistryIB := TRegIniFile.Create;
     with FRegistryIB do
     begin
          RootKey := HKEY_LOCAL_MACHINE;
          OpenKey( IB_REGISTRY_PATH, TRUE );
          LazyWrite := False;
     end;
end;

destructor TMonitorRegistry.Destroy;
begin
     FRegistryIB.Free;
     FRegistry.Free;
     inherited Destroy;
end;

function TMonitorRegistry.GetCreateOutputFile: Boolean;
begin
     Result := FRegistry.ReadBool( VACIO, MONITOR_CREATE_OUTPUT_FOLDER, TRUE );
end;

function TMonitorRegistry.GetDBConfigurationFrequency: string;
begin
     Result := FRegistry.ReadString( VACIO, MONITOR_DB_CONFIGURATION_FREQUENCY, K_HORA );
end;

function TMonitorRegistry.GetRootDirectory: string;
begin
     Result := FRegistryIB.ReadString( VACIO, IB_REGISTRY_ROOT, VACIO );
end;

function TMonitorRegistry.GetDBLimit: Word;
begin
     Result := FRegistry.ReadInteger( VACIO, MONITOR_DB_LIMIT, K_LIMITE_MINIMO_EMPLEADOS );
end;

function TMonitorRegistry.GetDBProcessesEmpleados: Word;
begin
     Result := FRegistry.ReadInteger( VACIO, MONITOR_DB_PROCESSES_EMPLEADOS, K_LIMITE_MAYOR_EMPLEADOS );
end;

function TMonitorRegistry.GetDBProcessesFrequency: string;
begin
     Result := FRegistry.ReadString( VACIO, MONITOR_DB_PROCESSES_FREQUENCY, K_HORA );
end;

function TMonitorRegistry.GetDBProcessesTime: Word;
begin
     Result := FRegistry.ReadInteger( VACIO, MONITOR_DB_PROCESSES_TIME, K_TIEMPO_PROCESOS );
end;

function TMonitorRegistry.GetDirectoryGBK: String;
begin
     Result := ZetaCommonTools.VerificaDir( FRegistry.ReadString( VACIO, MONITOR_DIRECTORY_GBK, ExtractFilePath( Application.ExeName ) ) );
end;

function TMonitorRegistry.GetHardDiskFrequency: string;
begin
     Result := FRegistry.ReadString( VACIO, MONITOR_HARD_DISK_FREQUENCY, K_HORA );
end;

function TMonitorRegistry.GetHardDisks: string;
begin
     Result := FRegistry.ReadString( VACIO, MONITOR_HARD_DISKS, chr( K_DISK_C ) );
end;

function TMonitorRegistry.GetHardDiskSpaceLimitRed: Word;
begin
     Result := FRegistry.ReadInteger( VACIO, MONITOR_HARD_DISK_LIMIT_RED, K_RED_LIMIT );
end;

function TMonitorRegistry.GetHardDiskSpaceLimitYellow: Word;
begin
     Result := FRegistry.ReadInteger( VACIO, MONITOR_HARD_DISK_LIMIT_YELLOW, K_YELLOW_LIMIT );
end;

function TMonitorRegistry.GetInterbaseGBKFrequency: string;
begin
     Result := FRegistry.ReadString( VACIO, MONITOR_INTERBASE_GBK_FREQUENCY, K_HORA );
end;

function TMonitorRegistry.GetInterbaseLogFrequency: string;
begin
     Result := FRegistry.ReadString( VACIO, MONITOR_INTERBASE_LOG_FREQUENCY, K_HORA );
end;

function TMonitorRegistry.GetInterbaseServicesFrequency: string;
begin
     Result := FRegistry.ReadString( VACIO, MONITOR_INTERBASE_SERVICES_FREQUENCY, K_HORA );
end;

function TMonitorRegistry.GetLicenseSentinelFrequency: string;
begin
     Result := FRegistry.ReadString( VACIO, MONITOR_LICENSE_SENTINEL_FREQUENCY, K_HORA );
end;

function TMonitorRegistry.GetLicenseUseDaysLimit: Word;
begin
     Result := FRegistry.ReadInteger( VACIO, MONITOR_LICENSE_USE_DAYS_LIMIT, K_DIAS_LICENCIA );
end;

function TMonitorRegistry.GetLicenseUseFrequency: string;
begin
     Result := FRegistry.ReadString( VACIO, MONITOR_LICENSE_USE_FREQUENCY, K_HORA );
end;

function TMonitorRegistry.GetODBCFrequency: string;
begin
     Result := FRegistry.ReadString( VACIO, MONITOR_ODBC_FREQUENCY, K_HORA );
end;

function TMonitorRegistry.GetOutputDirectory: String;
begin
     Result := ZetaCommonTools.VerificaDir( FRegistry.ReadString( VACIO, MONITOR_OUTPUT_FOLDER, ExtractFilePath( Application.ExeName ) ) );
end;

function TMonitorRegistry.GetWaitInterval: Word;
begin
     Result := FRegistry.ReadInteger( VACIO, MONITOR_WAIT_INTERVAL, K_TIEMPO_ESPERA );
end;

procedure TMonitorRegistry.SetCreateOutputFile(const Value: Boolean);
begin
     FRegistry.WriteBool( VACIO, MONITOR_CREATE_OUTPUT_FOLDER, Value );
end;

procedure TMonitorRegistry.SetDBConfigurationFrequency(const Value: string);
begin
     FRegistry.WriteString( VACIO, MONITOR_DB_CONFIGURATION_FREQUENCY, Value );
end;

procedure TMonitorRegistry.SetDBLimit(const Value: Word);
begin
     FRegistry.WriteInteger( VACIO, MONITOR_DB_LIMIT, Value );
end;

procedure TMonitorRegistry.SetDBProcessesEmpleados(const Value: Word);
begin
     FRegistry.WriteInteger( VACIO, MONITOR_DB_PROCESSES_EMPLEADOS, Value );
end;

procedure TMonitorRegistry.SetDBProcessesFrequency(const Value: string);
begin
     FRegistry.WriteString( VACIO, MONITOR_DB_PROCESSES_FREQUENCY, Value );
end;

procedure TMonitorRegistry.SetDBProcessesTime(const Value: Word);
begin
     FRegistry.WriteInteger( VACIO, MONITOR_DB_PROCESSES_TIME, Value );
end;

procedure TMonitorRegistry.SetDirectoryGBK(const Value: string);
begin
     FRegistry.WriteString( VACIO, MONITOR_DIRECTORY_GBK, ZetaCommonTools.VerificaDir( Value ) );
end;

procedure TMonitorRegistry.SetHardDiskFrequency(const Value: string);
begin
     FRegistry.WriteString( VACIO, MONITOR_HARD_DISK_FREQUENCY, Value );
end;

procedure TMonitorRegistry.SetHardDisks(const Value: string);
begin
     FRegistry.WriteString( VACIO, MONITOR_HARD_DISKS, Value );
end;

procedure TMonitorRegistry.SetHardDiskSpaceLimitRed(const Value: Word);
begin
     FRegistry.WriteInteger( VACIO, MONITOR_HARD_DISK_LIMIT_RED, Value );
end;

procedure TMonitorRegistry.SetHardDiskSpaceLimitYellow(const Value: Word);
begin
     FRegistry.WriteInteger( VACIO, MONITOR_HARD_DISK_LIMIT_YELLOW, Value );
end;

procedure TMonitorRegistry.SetInterbaseGBKFrequency(const Value: string);
begin
     FRegistry.WriteString( VACIO, MONITOR_INTERBASE_GBK_FREQUENCY, Value );
end;

procedure TMonitorRegistry.SetInterbaseLogFrequency(const Value: string);
begin
     FRegistry.WriteString( VACIO, MONITOR_INTERBASE_LOG_FREQUENCY, Value );
end;

procedure TMonitorRegistry.SetInterbaseServicesFrequency( const Value: string);
begin
     FRegistry.WriteString( VACIO, MONITOR_INTERBASE_SERVICES_FREQUENCY, Value );
end;

procedure TMonitorRegistry.SetLicenseSentinelFrequency(const Value: string);
begin
     FRegistry.WriteString( VACIO, MONITOR_LICENSE_SENTINEL_FREQUENCY, Value );
end;

procedure TMonitorRegistry.SetLicenseUseDaysLimit(const Value: Word);
begin
     FRegistry.WriteInteger( VACIO, MONITOR_LICENSE_USE_DAYS_LIMIT, Value );
end;

procedure TMonitorRegistry.SetLicenseuseFrequency(const Value: string);
begin
     FRegistry.WriteString( VACIO, MONITOR_LICENSE_USE_FREQUENCY, Value );
end;

procedure TMonitorRegistry.SetODBCFrequency(const Value: string);
begin
     FRegistry.WriteString( VACIO, MONITOR_ODBC_FREQUENCY, Value );
end;

procedure TMonitorRegistry.SetOutputDirectory( const Value: String );
begin
     FRegistry.WriteString( VACIO, MONITOR_OUTPUT_FOLDER, ZetaCommonTools.VerificaDir( Value ) );
end;

procedure TMonitorRegistry.SetWaitInterval(const Value: Word);
begin
     FRegistry.WriteInteger( VACIO, MONITOR_WAIT_INTERVAL, Value );
end;

function TMonitorRegistry.GetCopyDirectory: String;
begin
     Result := ZetaCommonTools.VerificaDir( FRegistry.ReadString( VACIO, MONITOR_COPY_FOLDER, ExtractFilePath( Application.ExeName ) ) );
end;

procedure TMonitorRegistry.SetCopyDirectory(const Value: String);
begin
     FRegistry.WriteString( VACIO, MONITOR_COPY_FOLDER, ZetaCommonTools.VerificaDir( Value ) );
end;

function TMonitorRegistry.GetPollHoras: string;
begin
     Result := FRegistry.ReadString( VACIO, MONITOR_POLL_HORAS, K_POLL_HORAS );
end;

procedure TMonitorRegistry.SetPollHoras(const Value: string);
begin
     FRegistry.WriteString( VACIO, MONITOR_POLL_HORAS, Value );
end;

function TMonitorRegistry.GetInterbaseGBKHoras: string;
begin
     Result := FRegistry.ReadString( VACIO, MONITOR_INTERBASE_GBK_HORAS, K_DIA_COMPLETO );
end;

procedure TMonitorRegistry.SetInterbaseGBKHoras(const Value: string);
begin
     FRegistry.WriteString( VACIO, MONITOR_INTERBASE_GBK_HORAS, Value );
end;

function TMonitorRegistry.GetPollCompanys: string;
begin
     Result := FRegistry.ReadString( VACIO, MONITOR_POLL_COMPANYS, K_EMPRESA_DEFAULT );
end;

procedure TMonitorRegistry.SetPollCompanys(const Value: string);
begin
     FRegistry.WriteString( VACIO, MONITOR_POLL_COMPANYS, Value );
end;

function TMonitorRegistry.GetHardDisksAvail: boolean;
begin
     Result := FRegistry.ReadBool( VACIO, MONITOR_HARD_DISKS_AVAIL, TRUE );
end;

procedure TMonitorRegistry.SetHardDisksAvail(const Value: boolean);
begin
     FRegistry.WriteBool( VACIO, MONITOR_HARD_DISKS_AVAIL, Value );
end;

function TMonitorRegistry.GetPollAvail: boolean;
begin
     Result := FRegistry.ReadBool( VACIO, MONITOR_POLL_AVAIL, TRUE );
end;

procedure TMonitorRegistry.SetPollAvail(const Value: boolean);
begin
     FRegistry.WriteBool( VACIO, MONITOR_POLL_AVAIL, Value );
end;

function TMonitorRegistry.GetProcessAvail: boolean;
begin
     Result := FRegistry.ReadBool( VACIO, MONITOR_PROCESS_AVAIL, TRUE );
end;

procedure TMonitorRegistry.SetProcessAvail(const Value: boolean);
begin
     FRegistry.WriteBool( VACIO, MONITOR_PROCESS_AVAIL, Value );
end;

function TMonitorRegistry.GetServicesAvail: boolean;
begin
     Result := FRegistry.ReadBool( VACIO, MONITOR_SERVICES_AVAIL, TRUE );
end;

procedure TMonitorRegistry.SetServicesAvail(const Value: boolean);
begin
     FRegistry.WriteBool( VACIO, MONITOR_SERVICES_AVAIL, Value );
end;

function TMonitorRegistry.GetArchivosGBKAvail: boolean;
begin
     Result := FRegistry.ReadBool( VACIO, MONITOR_ARCHIVOSGBK_AVAIL, TRUE );
end;

function TMonitorRegistry.GetInterbaseLogAvail: boolean;
begin
     Result := FRegistry.ReadBool( VACIO, MONITOR_INTERBASELOG_AVAIL, TRUE );
end;

function TMonitorRegistry.GetLicenciaUsoAvail: boolean;
begin
     Result := FRegistry.ReadBool( VACIO, MONITOR_LICENCIAUSO_AVAIL, TRUE );
end;

function TMonitorRegistry.GetODBCAvail: boolean;
begin
     Result := FRegistry.ReadBool( VACIO, MONITOR_ODBC_AVAIL, TRUE );
end;

function TMonitorRegistry.GetSentinelSuperProAvail: boolean;
begin
     Result := FRegistry.ReadBool( VACIO, MONITOR_SENTINELSUPERPRO_AVAIL, TRUE );
end;

procedure TMonitorRegistry.SetArchivosGBKAvail(const Value: boolean);
begin
     FRegistry.WriteBool( VACIO, MONITOR_ARCHIVOSGBK_AVAIL, Value );
end;

procedure TMonitorRegistry.SetInterbaseLogAvail(const Value: boolean);
begin
     FRegistry.WriteBool( VACIO, MONITOR_INTERBASELOG_AVAIL, Value );
end;

procedure TMonitorRegistry.SetLicenciaUsoAvail(const Value: boolean);
begin
     FRegistry.WriteBool( VACIO, MONITOR_LICENCIAUSO_AVAIL, Value );
end;

procedure TMonitorRegistry.SetODBCAvail(const Value: boolean);
begin
     FRegistry.WriteBool( VACIO, MONITOR_ODBC_AVAIL, Value );
end;

procedure TMonitorRegistry.SetSentinelSuperProAvail(const Value: boolean);
begin
     FRegistry.WriteBool( VACIO, MONITOR_SENTINELSUPERPRO_AVAIL, Value );
end;

function TMonitorRegistry.GetListaCorreos: string;
begin
     Result := FRegistry.ReadString( VACIO, MONITOR_LISTA_CORREOS, VACIO );
end;

procedure TMonitorRegistry.SetListaCorreos(const Value: string);
begin
     FRegistry.WriteString( VACIO, MONITOR_LISTA_CORREOS, Value );
end;

function TMonitorRegistry.GetListaTestsIB: string;
begin
     Result := FRegistry.ReadString( VACIO, MONITOR_LISTA_TESTS_IB, VACIO );
end;

procedure TMonitorRegistry.SetListaTestsIB(const Value: string);
begin
     FRegistry.WriteString( VACIO, MONITOR_LISTA_TESTS_IB, Value );
end;

function TMonitorRegistry.GetHost: string;
begin
     Result := FRegistry.ReadString( VACIO, MONITOR_CORREO_HOST, K_HOST_DEFAULT );
end;

function TMonitorRegistry.GetPort: word;
begin
     Result := FRegistry.ReadInteger( VACIO, MONITOR_CORREO_PORT, K_PORT_DEFAULT );
end;

function TMonitorRegistry.GetRemitente: string;
begin
     Result := FRegistry.ReadString( VACIO, MONITOR_CORREO_REMITENTE, K_REMITENTE_DEFAULT );
end;

function TMonitorRegistry.GetUserID: string;
begin
     Result := FRegistry.ReadString( VACIO, MONITOR_CORREO_USERID, K_USERID_DEFAULT );
end;

procedure TMonitorRegistry.SetHost(const Value: string);
begin
     FRegistry.WriteString( VACIO, MONITOR_CORREO_HOST, Value );
end;

procedure TMonitorRegistry.SetPort(const Value: word);
begin
     FRegistry.WriteInteger( VACIO, MONITOR_CORREO_PORT, Value );
end;

procedure TMonitorRegistry.SetRemitente(const Value: string);
begin
     FRegistry.WriteString( VACIO, MONITOR_CORREO_REMITENTE, Value );
end;

procedure TMonitorRegistry.SetUserID(const Value: string);
begin
     FRegistry.WriteString( VACIO, MONITOR_CORREO_USERID, Value );
end;

function TMonitorRegistry.GetListaTestsMSSQL: string;
begin
     Result := FRegistry.ReadString( VACIO, MONITOR_LISTA_TESTS_MSSQL, VACIO );
end;

procedure TMonitorRegistry.SetListaTestsMSSQL(const Value: string);
begin
     FRegistry.WriteString( VACIO, MONITOR_LISTA_TESTS_MSSQL, Value );
end;

function TMonitorRegistry.GetCopyDirectoryAvail: boolean;
begin
     Result := FRegistry.ReadBool( VACIO, MONITOR_COPY_DIRECTORY_AVAIL, FALSE );
end;

function TMonitorRegistry.GetEmailAvail: boolean;
begin
     Result := FRegistry.ReadBool( VACIO, MONITOR_EMAIL_AVAIL, TRUE );
end;

procedure TMonitorRegistry.SetCopyDirectoryAvail(const Value: boolean);
begin
     FRegistry.WriteBool( VACIO, MONITOR_COPY_DIRECTORY_AVAIL, Value );
end;

procedure TMonitorRegistry.SetEmailAvail(const Value: boolean);
begin
     FRegistry.WriteBool( VACIO, MONITOR_EMAIL_AVAIL, Value );
end;

function TMonitorRegistry.GetFechaArchivo: TDateTime;
var
   wAnio, wMes, wDia: word;

begin
     with FRegistry do
     begin
          wAnio := ReadInteger( VACIO, MONITOR_ARCHIVO_ANIO, 1 );
          wMes := ReadInteger( VACIO, MONITOR_ARCHIVO_MES, 1 );
          wDia := ReadInteger( VACIO, MONITOR_ARCHIVO_DIA, 1 );
     end;
     Result := EncodeDate( wAnio, wMes, wDia );
end;

procedure TMonitorRegistry.SetFechaArchivo(const Value: TDateTime);
var
   wAnio, wMes, wDia: word;

begin
     DecodeDate( Value, wAnio, wMes, wDia );
     with FRegistry do
     begin
          WriteInteger( VACIO, MONITOR_ARCHIVO_ANIO, wAnio );
          WriteInteger( VACIO, MONITOR_ARCHIVO_MES, wMes );
          WriteInteger( VACIO, MONITOR_ARCHIVO_DIA, wDia );
     end;
end;

function TMonitorRegistry.GetIdioma: boolean;
begin
     Result := FRegistry.ReadBool( VACIO, MONITOR_IDIOMA, TRUE );
end;

procedure TMonitorRegistry.SetIdioma(const Value: boolean);
begin
     FRegistry.WriteBool( VACIO, MONITOR_IDIOMA, Value );
end;

function TMonitorRegistry.GetAdvertencia: boolean;
begin
     Result := FRegistry.ReadBool( VACIO, MONITOR_TEST_ADVERTENCIA, TRUE );
end;

function TMonitorRegistry.GetHTML: boolean;
begin
     Result := FRegistry.ReadBool( VACIO, MONITOR_TEST_HTML, FALSE );
end;

function TMonitorRegistry.GetOK: boolean;
begin
     Result := FRegistry.ReadBool( VACIO, MONITOR_TEST_OK, TRUE );
end;

function TMonitorRegistry.GetProblema: boolean;
begin
     Result := FRegistry.ReadBool( VACIO, MONITOR_TEST_PROBLEMA, TRUE );
end;

procedure TMonitorRegistry.SetAdvertencia(const Value: boolean);
begin
     FRegistry.WriteBool( VACIO, MONITOR_TEST_ADVERTENCIA, Value );
end;

procedure TMonitorRegistry.SetHTML(const Value: boolean);
begin
     FRegistry.WriteBool( VACIO, MONITOR_TEST_HTML, Value );
end;

procedure TMonitorRegistry.SetOK(const Value: boolean);
begin
     FRegistry.WriteBool( VACIO, MONITOR_TEST_OK, Value );
end;

procedure TMonitorRegistry.SetProblema(const Value: boolean);
begin
     FRegistry.WriteBool( VACIO, MONITOR_TEST_PROBLEMA, Value );
end;

end.


