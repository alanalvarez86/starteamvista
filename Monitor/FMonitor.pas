unit FMonitor;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ComCtrls, ExtCtrls, FileCtrl, Mask,
     DMonitorRegistry,
     DZetaServerProvider,
     ZetaNumero,
     ZetaHora,
     ZetaEdit,
     CheckLst;

type
  eMonitorStatus = ( emStopped,
                     emStopping,
                     emRunning );
                     
  TMonitorCFG = class(TForm)
    PanelStartStop: TPanel;
    PageControl: TPageControl;
    OutputFile: TTabSheet;
    Start: TBitBtn;
    Stop: TBitBtn;
    CreateOutputFile: TCheckBox;
    HardDisk: TTabSheet;
    Grabar: TBitBtn;
    Salir: TBitBtn;
    DB: TTabSheet;
    gbConfiguracion: TGroupBox;
    gbProcesos: TGroupBox;
    DBConfigurationLBL: TLabel;
    FrecProcesos: TLabel;
    DBConfigurationFrequency: TZetaHora;
    DBProcessesFrequency: TZetaHora;
    DBHorasLBL: TLabel;
    lblDBHorasProcesos: TLabel;
    Interbase: TTabSheet;
    gpInterbaseServicios: TGroupBox;
    lblFrecInterbaseServicios: TLabel;
    InterbaseServicesFrequency: TZetaHora;
    lblInterbaseHoras: TLabel;
    License: TTabSheet;
    gbLicenseSentinel: TGroupBox;
    lblFrecLicenseSentinel: TLabel;
    lblLicenseSentinelHoras: TLabel;
    LicenseSetinelFrequency: TZetaHora;
    gbLicenseUse: TGroupBox;
    lblLicenseUseHoras: TLabel;
    lblFrecLicenseUso: TLabel;
    LicenseUseFrequency: TZetaHora;
    LicenseUseDaysLimit: TZetaNumero;
    lblLicenseDiasUso: TLabel;
    lblLicenseDias: TLabel;
    gbRespaldo: TGroupBox;
    lblFrecGBKFiles: TLabel;
    lblGBKFilesHoras: TLabel;
    lblGBKDirectory: TLabel;
    tDirectoryGBK: TSpeedButton;
    InterbaseGBKFrequency: TZetaHora;
    GbkDirectory: TEdit;
    Tejecucion: TLabel;
    LimEmpleados: TLabel;
    EmployeLimit: TZetaNumero;
    EjecutionTime: TZetaNumero;
    lblDBMinutos: TLabel;
    lblProcessesMinimo: TLabel;
    DBProcessesEmpleados: TZetaNumero;
    gbInterbaseLog: TGroupBox;
    lblInterbaseLog: TLabel;
    lblInterbaseLogHoras: TLabel;
    InterbaseLogFrequency: TZetaHora;
    StatusBar: TStatusBar;
    lblLapso: TLabel;
    PollHoras: TZetaHora;
    lblHoras: TLabel;
    lblHorasGBK: TLabel;
    InterbaseGBKHoras: TZetaHora;
    lblHoras_GBK: TLabel;
    PollCompanysLBL: TLabel;
    PollCompanys: TZetaEdit;
    gbODBC: TGroupBox;
    lblFrecODBC: TLabel;
    lblODBCHoras: TLabel;
    ODBCFrequency: TZetaHora;
    gbDiscos: TGroupBox;
    HardDiskFrequencyLBL: TLabel;
    HardDiskFrequency: TZetaHora;
    HardDiskHorasLBL: TLabel;
    HardDiskYellow: TZetaNumero;
    Porcentaje2: TLabel;
    HardDiskYellowLBL: TLabel;
    HardDiskRedLBL: TLabel;
    HardDiskRed: TZetaNumero;
    Porcentaje1: TLabel;
    HardDisks: TZetaEdit;
    HardDisksLBL: TLabel;
    PollAvail: TCheckBox;
    ProcessAvail: TCheckBox;
    Subscripcion: TTabSheet;
    gbEmails: TGroupBox;
    ListaCorreos: TTabSheet;
    gbCorreosElectronicos: TGroupBox;
    ListaEmails: TListBox;
    BorrarTodos: TSpeedButton;
    BorraUno: TSpeedButton;
    gbDireccionCorreo: TGroupBox;
    edCorreo: TZetaEdit;
    Agregar: TSpeedButton;
    PruebasDisponibles: TCheckListBox;
    PrendeTodos: TSpeedButton;
    ApagaTodos: TSpeedButton;
    ConfEmails: TTabSheet;
    lblHost: TLabel;
    Host: TEdit;
    UserID: TEdit;
    lblUser: TLabel;
    lblDirRemitente: TLabel;
    Remitente: TEdit;
    lblPuerto: TLabel;
    EmailSender: TSpeedButton;
    Port: TZetaNumero;
    gbDirCopy: TGroupBox;
    CopyDirectory: TEdit;
    CopyDirectorySeek: TSpeedButton;
    gbDirOutput: TGroupBox;
    OutputDirectory: TEdit;
    OutputDirectorySeek: TSpeedButton;
    CopyDirectoryAvail: TCheckBox;
    HardDisksAvail: TCheckBox;
    ServicesAvail: TCheckBox;
    InterbaseLogAvail: TCheckBox;
    ArchivosGBKAvail: TCheckBox;
    SentinelSuperProAvail: TCheckBox;
    LicenciaUsoAvail: TCheckBox;
    ODBCAvail: TCheckBox;
    PrendeTodas: TSpeedButton;
    ApagaTodas: TSpeedButton;
    gbResultados: TGroupBox;
    Ok: TCheckBox;
    Advertencia: TCheckBox;
    Problema: TCheckBox;
    gbConfEmail: TGroupBox;
    EmailAvail: TCheckBox;
    Idioma: TCheckBox;
    HTML: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OutputDirectorySeekClick(Sender: TObject);
    procedure CopyDirectorySeekClick(Sender: TObject);
    procedure GrabarClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure StartClick(Sender: TObject);
    procedure StopClick(Sender: TObject);
    procedure tDirectoryGBKClick(Sender: TObject);
    procedure HardDisksAvailClick(Sender: TObject);
    procedure PollAvailClick(Sender: TObject);
    procedure ProcessAvailClick(Sender: TObject);
    procedure ServicesAvailClick(Sender: TObject);
    procedure InterbaseLogAvailClick(Sender: TObject);
    procedure ArchivosGBKAvailClick(Sender: TObject);
    procedure SentinelSuperProAvailClick(Sender: TObject);
    procedure LicenciaUsoAvailClick(Sender: TObject);
    procedure ODBCAvailClick(Sender: TObject);
    procedure AgregarClick(Sender: TObject);
    procedure edCorreoKeyPress(Sender: TObject; var Key: Char);
    procedure BorrarTodosClick(Sender: TObject);
    procedure BorraUnoClick(Sender: TObject);
    procedure PrendeTodosClick(Sender: TObject);
    procedure ApagaTodosClick(Sender: TObject);
    procedure EmailSenderClick(Sender: TObject);
    procedure CopyDirectoryAvailClick(Sender: TObject);
    procedure PrendeTodasClick(Sender: TObject);
    procedure ApagaTodasClick(Sender: TObject);
  private
    { Private declarations }
    FCursor: TCursor;
    FStatus: eMonitorStatus;
    FRegistry: TMonitorRegistry;
    FCtr: Integer;
    function GetCorreos: string;
    function GetTests: string;
    procedure SetStatus( eValue: eMonitorStatus );
    procedure UpdateStatus;
    procedure ShowStatus(const iPanel: Integer; const sMensaje: String);
    procedure HabilitaDeshabilita( gbGrupo: TGroupBox; lAccion: boolean );
    procedure SetCorreos( sLista: string );
    procedure SetTests( sLista: string );
    procedure LlenaTests;
    procedure PrendeApagaReportes( const lState: boolean );
    procedure PrendeApagaPruebas( const lTipo: boolean );
  public
    { Public declarations }
    function WaitInterval: TDateTime;
    procedure ShowMessage( const sMensaje: String );
  end;

var
  MonitorCFG: TMonitorCFG;

implementation

uses DMonitor,
     DMonitorTress,
     ZetaDialogo,
     ZetaCommonClasses,
     ZetaCommonTools;

{$R *.DFM}

{ ******** TMonitorCFG ********* }

procedure TMonitorCFG.FormCreate(Sender: TObject);
const
     K_INDEX = 3;
     K_TITULO = 'Configurador Monitor de Tress %s';
     
begin
     {$ifdef INTERBASE}
             {$ifdef CORPORATIVA}
                     Self.Caption := Format( K_TITULO, [ 'InterBase Corporativo' ] );
             {$endif}
             {$ifdef DOS_CAPAS}
                     Self.Caption := Format( K_TITULO, [ 'InterBase Profesional' ] );
             {$endif}
     {$endif}
     {$ifdef MSSQL}
     Self.Caption := Format( K_TITULO, [ 'SQL Server' ] );
     PageControl.Pages[ K_INDEX ].TabVisible := FALSE;
     {$endif}
     FRegistry := TMonitorRegistry.Create;
     FStatus := emStopped;
     FCtr := 0;
     Environment;
end;

procedure TMonitorCFG.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
     case FStatus of
          emRunning:
          begin
               CanClose := False;
               ZetaDialogo.zError( '� Monitor Est� Corriendo !', 'Hay Que Terminar El Monitoreo Antes De Poder Salir De Este Programa', 0 );
          end;
          emStopping: CanClose := True;
          emStopped: CanClose := True;
     end;
end;

procedure TMonitorCFG.FormShow(Sender: TObject);
begin
     LlenaTests;
     with FRegistry do
     begin
          Self.OutputDirectory.Text := OutputDirectory;
          Self.CopyDirectory.Text := CopyDirectory;
          Self.CreateOutputFile.Checked := CreateOutputFile;
          Self.HardDiskFrequency.Valor := HardDiskFrequency;
          Self.HardDiskYellow.Valor := HardDiskSpaceLimitYellow;
          Self.HardDiskRed.Valor := HardDiskSpaceLimitRed;
          Self.HardDisks.Valor := HardDisks;
          Self.HardDisksAvail.Checked := HardDisksAvail;
          Self.DBProcessesFrequency.Valor := DBProcessesFrequency;
          Self.EjecutionTime.Valor := DBProcessesTime;
          Self.EmployeLimit.Valor := DBLimit;
          Self.DBConfigurationFrequency.Valor := DBConfigurationFrequency;
          Self.DBProcessesEmpleados.Valor := DBProcessesEmpleados;
          Self.LicenseSetinelFrequency.Valor := LicenseSentinelFrequency;
          Self.LicenseUseFrequency.Valor := LicenseUseFrequency;
          Self.LicenseUseDaysLimit.Valor := LicenseUseDaysLimit;
          Self.ODBCFrequency.Valor := ODBCFrequency;
          Self.PollHoras.Valor := PollHoras;
          Self.PollCompanys.Valor := PollCompanys;
          Self.PollAvail.Checked := PollAvail;
          Self.ProcessAvail.Checked := ProcessAvail;
          Self.SentinelSuperProAvail.Checked := SentinelSuperProAvail;
          Self.LicenciaUsoAvail.Checked := LicenciaUsoAvail;
          Self.ODBCAvail.Checked := ODBCAvail;
          SetCorreos( ListaCorreos );
          Self.Host.Text := Host;
          Self.Port.Valor := Port;
          Self.UserID.Text := UserID;
          Self.Remitente.Text := Remitente;
          Self.CopyDirectoryAvail.Checked := CopyDirectoryAvail;
          Self.EmailAvail.Checked := EmailAvail;
          Self.Idioma.Checked := Idioma;
          Self.Ok.Checked := OK;
          Self.Advertencia.Checked := Advertencia;
          Self.Problema.Checked := Problema;
          Self.HTML.Checked := HTML;
          {$ifdef MSSQL}
          SetTests( ListaTestsMSSQL );
          {$endif}
          {$ifdef INTERBASE}
          SetTests( ListaTestsIB );
          Self.ArchivosGBKAvail.Checked := ArchivosGBKAvail;
          Self.InterbaseLogAvail.Checked := InterBaseLogAvail;
          Self.ServicesAvail.Checked := ServicesAvail;
          Self.GbkDirectory.Text := DirectoryGBK;
          Self.InterbaseServicesFrequency.Valor := InterbaseServicesFrequency;
          Self.InterbaseGBKFrequency.Valor := InterbaseGBKFrequency;
          Self.InterbaseGBKHoras.Valor := InterbaseGBKHoras;
          Self.InterbaseLogFrequency.Valor := InterbaseLogFrequency;
          {$Endif}
     end;
     SetStatus( emStopped );
     FCursor := Screen.Cursor;
     Pagecontrol.ActivePage := OutPutFile;
end;

procedure TMonitorCFG.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FRegistry );
end;

function TMonitorCFG.WaitInterval: TDateTime;
begin
     Result := EncodeTime( 0, 0, 59, 0 );
end;

procedure TMonitorCFG.ShowStatus( const iPanel: Integer; const sMensaje: String );
begin
     StatusBar.Panels[ iPanel ].Text := sMensaje;
end;

procedure TMonitorCFG.ShowMessage(const sMensaje: String);
begin
     ShowStatus( 2, sMensaje );
     Application.ProcessMessages;
end;

procedure TMonitorCFG.UpdateStatus;
begin
     case FStatus of
          emRunning: ShowStatus( 1, Format( 'Ciclo # %d', [ FCtr ] ) );
          emStopping: ShowStatus( 1, '' );
          emStopped: ShowStatus( 1, '' );
     end;
end;

procedure TMonitorCFG.SetStatus( eValue: eMonitorStatus );
begin
     FStatus := eValue;
     case FStatus of
          emRunning:
          begin
               Start.Enabled := False;
               Stop.Enabled := True;
               Grabar.Enabled := False;
               Salir.Enabled := False;
               Screen.Cursor := crAppStart;
               ShowStatus( 0, 'Monitoreando' );
          end;
          emStopping:
          begin
               ShowStatus( 0, 'Deteniendo Monitoreo' );
               SetStatus( emStopped );
          end;
          emStopped:
          begin
               Start.Enabled := True;
               Stop.Enabled := False;
               Grabar.Enabled := True;
               Salir.Enabled := True;
               Screen.Cursor := FCursor;
               ShowStatus( 0, 'Inactivo' );
          end;
     end;
     UpdateStatus;
     Application.ProcessMessages;
end;

procedure TMonitorCFG.OutputDirectorySeekClick(Sender: TObject);
var
   sDirectory: String;
begin
     with OutputDirectory do
     begin
          sDirectory := Text;
          if FileCtrl.SelectDirectory( sDirectory, [], 0 ) then
             Text := sDirectory
     end;
end;

procedure TMonitorCFG.CopyDirectorySeekClick(Sender: TObject);
var
   sDirectory: String;
begin
     with CopyDirectory do
     begin
          sDirectory := Text;
          if FileCtrl.SelectDirectory( sDirectory, [], 0 ) then
             Text := sDirectory
     end;
end;

procedure TMonitorCFG.tDirectoryGBKClick(Sender: TObject);
var
   sDirectory: String;
begin
     with GbkDirectory do
     begin
          sDirectory := Text;
          if FileCtrl.SelectDirectory( sDirectory, [], 0 ) then
             Text := sDirectory
     end;
end;

procedure TMonitorCFG.SetCorreos( sLista: string );
begin
     with ListaEmails.Items do
     begin
          CommaText := sLista
     end;
end;

function TMonitorCFG.GetCorreos: string;
const
     K_MAIL = '@tress.com.mx';
var
   sLista: string;

begin
     with ListaEmails.Items do
          sLista := CommaText;
          //Result := CommaText;
     if EmailAvail.Checked then
     begin
          if ( pos( K_MAIL, sLista ) > 0 ) then
             Result := sLista
          else
              ZetaDialogo.ZError( 'Monitor Tress', 'La Lista de Correos debe contener al menos ' + CR_LF +
                                                   'Una direcci�n de Correo de Grupo Tress ' + CR_LF +
                                                   '( @tress.com.mx )', 0 );
     end;
end;

procedure TMonitorCFG.GrabarClick(Sender: TObject);
var
   oCursor: TCursor;

begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with FRegistry do
        begin
             CreateOutputFile := Self.CreateOutputFile.Checked;
             OutputDirectory := Self.OutputDirectory.Text;
             CopyDirectory := Self.CopyDirectory.Text;
             HardDiskFrequency := Self.HardDiskFrequency.Valor;
             HardDiskSpaceLimitYellow := Self.HardDiskYellow.ValorEntero;
             HardDiskSpaceLimitRed := Self.HardDiskRed.ValorEntero;
             HardDisks := Self.HardDisks.Valor;
             HardDisksAvail := Self.HardDisksAvail.Checked;
             DBConfigurationFrequency := Self.DBConfigurationFrequency.Valor;
             DBProcessesFrequency := Self.DBProcessesFrequency.Valor;
             DBProcessesTime := Self.EjecutionTime.ValorEntero;
             DBLimit := Self.EmployeLimit.ValorEntero;
             DBProcessesEmpleados := Self.DBProcessesEmpleados.ValorEntero;
             LicenseSentinelFrequency := Self.LicenseSetinelFrequency.Valor;
             LicenseUseFrequency := Self.LicenseUseFrequency.Valor;
             LicenseUseDaysLimit := Self.LicenseUseDaysLimit.ValorEntero;
             ODBCFrequency := Self.ODBCFrequency.Valor;
             PollHoras := Self.PollHoras.Valor;
             PollCompanys := Self.PollCompanys.Valor;
             PollAvail := Self.PollAvail.Checked;
             ProcessAvail := Self.ProcessAvail.Checked;
             SentinelSuperProAvail := Self.SentinelSuperProAvail.Checked;
             LicenciaUsoAvail := Self.LicenciaUsoAvail.Checked;
             ODBCAvail := Self.ODBCAvail.Checked;
             ListaCorreos := GetCorreos;
             Host := Self.Host.Text;
             Port := Self.Port.ValorEntero;
             UserID := Self.UserID.Text;
             Remitente := Self.Remitente.Text;
             CopyDirectoryAvail := Self.CopyDirectoryAvail.Checked;
             EmailAvail := Self.EmailAvail.Checked;
             Idioma := Self.Idioma.Checked;
             OK := Self.Ok.Checked;
             Advertencia := Self.Advertencia.Checked;
             Problema := Self.Problema.Checked;
             HTML := Self.HTML.Checked;
             {$ifdef MSSQL}
             ListaTestsMSSQL := GetTests;
             {$endif}
             {$ifdef INTERBASE}
             ListaTestsIB := GetTests;
             InterBaseLogAvail := Self.InterbaseLogAvail.Checked;
             ServicesAvail := Self.ServicesAvail.Checked;
             ArchivosGBKAvail := Self.ArchivosGBKAvail.Checked;
             DirectoryGBK := Self.GbkDirectory.text;
             InterbaseServicesFrequency := Self.InterbaseServicesFrequency.Valor;
             InterbaseGBKFrequency := Self.InterbaseGBKFrequency.Valor;
             InterbaseGBKHoras := Self.InterbaseGBKHoras.Valor;
             InterbaseLogFrequency := Self.InterbaseLogFrequency.Valor;
             {$Endif}
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TMonitorCFG.StartClick(Sender: TObject);
var
   FChecker: TMonitor;
   dStart: TDateTime;
begin
     if EmailAvail.Checked and ( ListaEmails.Items.Count <> 0 ) then
     begin
          FCtr := 1;
          SetStatus( emRunning );
          FChecker := TMonitor.Create;
          try
             try
                with FChecker do
                begin
                     OnCallBack := ShowMessage;
                     Start;
                end;
                dStart := NullDateTime;
                while not ( FStatus in [ emStopping, emStopped ] ) do
                begin
                     Sleep( 1000 );
                     if ( ( Now - dStart ) > WaitInterval ) then
                     begin
                          with FChecker do
                          begin
                               RunChecks;
                          end;
                          dStart := Now;
                     end;
                     UpdateStatus;
                     Application.ProcessMessages;
                     Inc( FCtr );
                end;
                with FChecker do
                begin
                     Stop;
                end;
             except
                   on Error: Exception do
                   begin
                        Application.HandleException( Error );
                   end;
             end;
          finally
                 FreeAndNil( FChecker );
          end;
     end
     else
         ZetaDialogo.ZError( 'Monitor', 'Es necesario tener al menos una Direcci�n de Correo Electr�nico,' + CR_LF +
                                        'para poder ser enviado', 0 );
end;

procedure TMonitorCFG.StopClick(Sender: TObject);
begin
     SetStatus( emStopping );
end;

procedure TMonitorCFG.HardDisksAvailClick(Sender: TObject);
begin
     if HardDisksAvail.Checked then
        HabilitaDeshabilita( gbDiscos, True )
     else
         HabilitaDeshabilita( gbDiscos, False );
end;

procedure TMonitorCFG.PollAvailClick(Sender: TObject);
begin
     if PollAvail.Checked then
        HabilitaDeshabilita( gbConfiguracion, True )
     else
         HabilitaDeshabilita( gbConfiguracion, False );
end;

procedure TMonitorCFG.ProcessAvailClick(Sender: TObject);
begin
     if ProcessAvail.Checked then
        HabilitaDeshabilita( gbProcesos, True )
     else
         HabilitaDeshabilita( gbProcesos, False );
end;

procedure TMonitorCFG.ServicesAvailClick(Sender: TObject);
begin
     if ServicesAvail.Checked then
        HabilitaDeshabilita( gpInterbaseServicios, True )
     else
         HabilitaDeshabilita( gpInterbaseServicios, False );
end;

procedure TMonitorCFG.InterbaseLogAvailClick(Sender: TObject);
begin
     if InterbaseLogAvail.Checked then
        HabilitaDeshabilita( gbInterbaseLog, True )
     else
         HabilitaDeshabilita( gbInterbaseLog, False );
end;

procedure TMonitorCFG.ArchivosGBKAvailClick(Sender: TObject);
begin
     if ArchivosGBKAvail.Checked then
        HabilitaDeshabilita( gbRespaldo, True )
     else
         HabilitaDeshabilita( gbRespaldo, False );
end;

procedure TMonitorCFG.SentinelSuperProAvailClick(Sender: TObject);
begin
     if SentinelSuperProAvail.Checked then
        HabilitaDeshabilita( gbLicenseSentinel, True )
     else
         HabilitaDeshabilita( gbLicenseSentinel, False );
end;

procedure TMonitorCFG.LicenciaUsoAvailClick(Sender: TObject);
begin
     if LicenciaUsoAvail.Checked then
        HabilitaDeshabilita( gbLicenseUse, True )
     else
         HabilitaDeshabilita( gbLicenseUse, False );
end;

procedure TMonitorCFG.ODBCAvailClick(Sender: TObject);
begin
     if ODBCAvail.Checked then
        HabilitaDeshabilita( gbODBC, True )
     else
         HabilitaDeshabilita( gbODBC, False );
end;

procedure TMonitorCFG.CopyDirectoryAvailClick(Sender: TObject);
begin
     if CopyDirectoryAvail.Checked then
        HabilitaDeshabilita( gbDirCopy, True )
     else
         HabilitaDeshabilita( gbDirCopy, False );
end;

procedure TMonitorCFG.HabilitaDeshabilita( gbGrupo: TGroupBox; lAccion: boolean );
var
   i: integer;

begin
     with gbGrupo do
     begin
          for i := 0 to ( ControlCount - 1 ) do
          begin
               Controls[ i ].Enabled := lAccion;
          end;
     end;
end;

procedure TMonitorCFG.AgregarClick(Sender: TObject);
var
   sCorreo: string;

begin
     sCorreo := edCorreo.Text;
     with ListaEmails.Items do
     begin
          if StrLleno( sCorreo ) then
             Add( sCorreo )
          else
              ZetaDialogo.ZInformation( 'Monitor', 'Debe Escribir Una Direcci�n de Correo Electr�nico', 0 )
     end;
     sCorreo := VACIO;
     edCorreo.Text := sCorreo;
end;

procedure TMonitorCFG.edCorreoKeyPress(Sender: TObject; var Key: Char);
begin
     if ( key = char( 13 ) ) then
        Agregarclick( Sender );
end;

procedure TMonitorCFG.LlenaTests;
var
   i: eTests;

begin
     with PruebasDisponibles do
     begin
          with Items do
          begin
               BeginUpdate;
               try
                  for i := Low( eTests ) to High( eTests ) do
                  begin
                       Add( dmMonitorTress.ObtieneElemento( ord( i ) ) );
                       Checked[ ord( i ) ] := FALSE;
                  end;
               finally
                      EndUpdate;
               end;
          end;
     end;
end;

procedure TMonitorCFG.SetTests( sLista: string );
var
   i, j, iTotal: integer;

begin
     with PruebasDisponibles do
     begin
          with Items do
          begin
               BeginUpdate;
               try
                  iTotal := Length( sLista );
                  j := 0;
                  for i := 1 to ( iTotal ) do
                  begin
                       if ( sLista[ i ] <> ',' ) then
                       begin
                            if ( sLista[ i ] <> Chr( 13 ) ) then
                            begin
                                 Checked [ j ] := zStrToBool( sLista[ i ] );
                                 j := j + 1;
                            end;
                       end;
                  end;
               finally
                      EndUpdate;
               end;
          end;
     end;
end;

function TMonitorCFG.GetTests: string;
var
   i: integer;
begin
     with PruebasDisponibles do
     begin
          for i := 0 to ( Items.Count - 1 ) do
          begin
              if Checked[ i ] then
                  Result := ZetaCommonTools.ConcatString( Result, zBoolToStr( True ), ',' )
              else
                  Result := ZetaCommonTools.ConcatString( Result, zBoolToStr( False ), ',' );
          end;
     end;
end;

procedure TMonitorCFG.BorrarTodosClick(Sender: TObject);
begin
     with ListaEmails do
     begin
          Clear;
     end;
end;

procedure TMonitorCFG.PrendeApagaReportes( const lState: boolean );
var
   i: Integer;
begin
     with PruebasDisponibles do
     begin
          with Items do
          begin
               try
                  BeginUpdate;
                  for i := 0 to ( Count - 1 ) do
                      Checked[ i ] := lState;
               finally
                      EndUpdate;
               end;
          end;
     end;
end;

procedure TMonitorCFG.BorraUnoClick(Sender: TObject);
const
     NO_INDEX = -1;

var
   iEmailsIndex: integer;
   
begin
     with ListaEmails do
     begin
          iEmailsIndex := ItemIndex;
          if iEmailsIndex = NO_INDEX then
             ZetaDialogo.ZInformation( 'Monitor', 'Debe Tener Seleccionado un Elemento de la Lista de Correos', 0 )
          else
               Items.Delete( iEmailsIndex );
     end;
end;

procedure TMonitorCFG.PrendeTodosClick(Sender: TObject);
begin
     PrendeApagaReportes( True );
end;

procedure TMonitorCFG.ApagaTodosClick(Sender: TObject);
begin
     PrendeApagaReportes( False );
end;

procedure TMonitorCFG.EmailSenderClick(Sender: TObject);
var
   oCursor: TCursor;
   Lista: TStrings;
begin
     Lista := TStringlist.Create;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        Lista.Add( 'Prueba de Correo' );
        dmMonitorTress.SendEmail( Lista );
     finally
            Screen.Cursor := oCursor;
            FreeAndNil( Lista );
     end;
end;

procedure TMonitorCFG.PrendeApagaPruebas( const lTipo: boolean );
begin
    HardDisksAvail.Checked := lTipo;
    PollAvail.Checked := lTipo;
    ProcessAvail.Checked := lTipo;
    ServicesAvail.Checked := lTipo;
    InterbaseLogAvail.Checked := lTipo;
    ArchivosGBKAvail.Checked := lTipo;
    SentinelSuperProAvail.Checked := lTipo;
    LicenciaUsoAvail.Checked := lTipo;
    ODBCAvail.Checked := lTipo;
end;

procedure TMonitorCFG.PrendeTodasClick(Sender: TObject);
begin
     PrendeApagaPruebas( True );
end;

procedure TMonitorCFG.ApagaTodasClick(Sender: TObject);
begin
     PrendeApagaPruebas( False );
end;

end.
