unit DMonitor;



{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.x                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      DMonitor.pas                               ::
  :: Descripci�n: Clases para Tress Monitor.exe              ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface
{$ifdef MSSQL}
{$define SENTINELVIRTUAL}
{$endif}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs,
     {$ifndef VER130}Variants,{$endif}
     FileCtrl,
     FDBRegistry,
     FMonitor,
     FAutoServer,
     DMonitorRegistry,
     DZetaServerProvider,
     DMonitorTress,
     ZetaAsciiFile;

type
    eQuerys = ( eAbiertos,
                eErrores,
                eProcesos,
                eEmpresas );

  TMonitor = class;
  TCheck = class( TObject )
  private
    { Private declarations }
    FActualDate: TDateTime;
    FMonitor: TMonitor;
    FStart: TDateTime;
    FFrequency: TDateTime;
    FAvail: boolean;
    FTest: integer;
    FMessageHeader: string;
    oZetaProvider: TdmZetaServerProvider;
    FEmpresas: TList;
    function GetEmpresas( Index: Integer ): TEmpresaData;
    function GetEmpresasCount: Integer;
  protected
    { Protected declarations }
    property Frequency: TDateTime read FFrequency write FFrequency;
    property Monitor: TMonitor read FMonitor;
    property MessageHeader: String read FMessageHeader write FMessageHeader;
    function StrToTime(const sValue: String): TDateTime;
    function GetListaEmpresas: Boolean;
    procedure ClearEmpresas;
    procedure Init; virtual; abstract;
    procedure DoCheck; virtual; abstract;
  public
    { Public declarations }
    constructor Create( Monitor: TMonitor );
    destructor Destroy; override;
    property Empresas[ Index: Integer ]: TEmpresaData read GetEmpresas;
    property EmpresasCount: Integer read GetEmpresasCount;
    procedure Start;
    procedure Stop;
    procedure Pause;
    procedure Continue;
    procedure ShutDown;
    procedure RunCheck;
  end;

  TCheckHardDiskSpace = class( TCheck )
  private
    { Private declarations }
    FLimitYellow: Word;
    FLimitRed: Word;
    FDisk: string;
    {$ifdef INTERBASE}
    function SetListaTemporal( Lista: TStrings ): Boolean;
    {$endif}
  protected
    { Protected declarations }
    procedure Init; override;
    procedure DoCheck; override;
  public
    { Public declarations }
  end;

  TCheckIBServices = class( TCheck )
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Init; override;
    procedure DoCheck; override;
  public
    { Public declarations }
  end;

  TCheckDataBases = class( TCheck )
  private
    { Private declarations }
  protected
    { Protected declarations }
    FDataBaseName: String;
    FProcesarComparte: Boolean;
    lCheck: Boolean;
    procedure Init; override;
    procedure DoCheck; override;
    procedure RevisaDBInfo( const lComparte: Boolean; sCodigo : string ); virtual; abstract;
  public
    { Public declarations }
  end;

  TCheckDataBasesDaily = class( TCheckDataBases )
  private
    { Private declarations }
    FEmployeesRate: Word;
    FMinimumEmployees: Word;
    FHours: TDatetime;
    FCompanys: string;
    procedure CheckProcesses( const sProcesos, sStatus, sCodigo: String );
  protected
    { Protected declarations }
    procedure RevisaDBSize( const sDBFile: String );
    procedure RevisaDBInfo( const lComparte: Boolean; sCodigo : string ); override;
    procedure Init; override;
  public
    { Public declarations }
  end;

  TCheckDataBasesHourly = class( TCheckDataBases )
  private
    { Private declarations }
    FMaximumTime: Word;
  protected
    { Protected declarations }
    procedure RevisaDBInfo( const lComparte: Boolean; sCodigo : string ); override;
    procedure Init; override;
  public
    { Public declarations }
  end;

  TCheckGBK = class( TCheck )
  private
    { Private declarations }
    {$ifdef INTERBASE}
    FDirectoryGBK: string;
    FHoras: TDateTime;
    FFechaIni: TDateTime;
    FFechaFin: TDateTime;
    {$endif}
  protected
    { Protected declarations }
    procedure Init; override;
    procedure DoCheck; override;
  public
    { Public declarations }
    destructor Destroy; override;
  end;

  TCheckSentinel = class( TCheck )
  private
    { Private declarations }
    FDirectory: string;
    FDaysLicenseUse: Word;
    FBitacora: TAsciiLog;
  protected
    { Protected declarations }
  {$ifdef SENTINELVIRTUAL}
    procedure SentinelLog(const sMensaje: String);
    procedure SentinelError( const sMensaje: String );
  {$endif}
    procedure Init; override;
    procedure DoCheck; override;
  public
    { Public declarations }
    destructor Destroy; override;
  end;

  TCheckODBCPooling = class( TCheck )
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Init; override;
    procedure DoCheck; override;
  public
    { Public declarations }
  end;

  TCheckODBCTracing = class( TCheck )
  private
    { Private declarations }
    FSize: Integer;
    FTraceFile: String;
  protected
    { Protected declarations }
    procedure Init; override;
    procedure DoCheck; override;
  public
    { Public declarations }
  end;

  TCheckIBLog = class( TCheck )
  private
    { Private declarations }
    {$ifdef INTERBASE}
    FDirectoryIB: string;
    FAscii : TAsciiLog;
    {$endif}
  protected
    { Protected declarations }
    procedure Init; override;
    procedure DoCheck; override;
  public
    { Public declarations }
    destructor Destroy; override;
  end;

  TMonitorCallBack = procedure( const Mensaje: String ) of object;
  TMonitor = class( TObject )
  private
    { Private declarations }
    FChecks: TList;
    FOutput: TStrings;
    FMensajeEmail: TStrings;
    FRegistry: TMonitorRegistry;
    FCallBack: TMonitorCallBack;
    function GetCount: Integer;
    function GetChecks(Index: Integer): TCheck;
    function ChecaDemo: boolean;
    procedure Add( oCheck: TCheck );
    procedure Clear;
    procedure Delete(const Index: Integer);
    procedure Init;
    procedure ReportAlert( const sMessage, sDescription, sFlag: string; const iPrueba: integer );
    procedure EscribeLog;
    procedure CreaArchivosSalida;
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Checks[ Index: Integer ]: TCheck read GetChecks;
    property Registry: TMonitorRegistry read FRegistry;
    property Count: Integer read GetCount;
    property OnCallBack: TMonitorCallBack read FCallBack write FCallBack;
    function Start: Boolean;
    function Stop: Boolean;
    function WaitInterval: Integer;
    procedure CallMeBack( const sMensaje: String );
    procedure Continue;
    procedure Pause;
    procedure ReportGreenAlert( const sMessage, sDescription, sCodigo: string; iPrueba: integer );
    procedure ReportRedAlert( const sMessage, sDescription, sCodigo: string; iPrueba: integer );
    procedure ReportYellowAlert( const sMessage, sDescription: String; const iPrueba: integer );
    procedure RunChecks;
    procedure Shutdown;
  end;

implementation

uses WinSvc,
     FAutoClasses,
{$ifdef MSSQL}
     FSentinelRegistry,
{$endif}
     //DCliente,
     ZetaWinAPITools,
     ZetaClientTools,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaODBCTools,
     ZetaDialogo,
     ZetaServerTools;

Var
   dtFecha: TDateTime;
   lFecha: boolean;

function GetFileSize( const sFileName : String ): Integer;
var
   Archivo: TSearchRec;
begin
     if ( StrLleno( SFileName ) ) and ( FindFirst( sFileName, faAnyFile, Archivo) = 0 ) then
     begin
          Result := Archivo.Size;
          FindClose( Archivo );
     end
     else
         Result := 0;
end;

function GetScript( const eScript: eQuerys ): string;
begin
     case eScript of
          eAbiertos: Result := 'select PC_PROC_ID, PC_FEC_INI, PC_HOR_INI, PC_FEC_FIN, PC_HOR_FIN, PC_MAXIMO, PC_ERROR ' +
                               'from PROCESO where PC_ERROR = %s and ( ( PC_FEC_INI < %s ) or ( PC_FEC_INI = %s and PC_HOR_INI <= %s ) ) ' +
                               'order by PC_FEC_INI, PC_HOR_INI';
          eErrores:    Result := 'select PC_PROC_ID, PC_FEC_INI, PC_HOR_INI, PC_FEC_FIN, PC_HOR_FIN ' +
                               'from PROCESO where ( ( PC_FEC_INI > %s ) or ( PC_FEC_INI = %s and PC_HOR_INI >= %s ) ) and ' +
                               '( ( PC_FEC_INI < %s ) or ( PC_FEC_FIN = %s and PC_HOR_FIN <= %s ) ) and ' +
                               '( ( select count(*) from BITACORA where BITACORA.BI_NUMERO = PROCESO.PC_NUMERO and BITACORA.BI_TIPO = %d ) > 0 ) ' +
                               'order by PC_FEC_INI, PC_HOR_INI';
          eProcesos: Result := 'select PC_PROC_ID, PC_FEC_INI, PC_HOR_INI, PC_FEC_FIN, PC_HOR_FIN, PC_MAXIMO, PC_ERROR ' +
                               'from PROCESO where PC_ERROR in ( %s ) and ' +
                               '( ( PC_PROC_ID = %d ) or ( PC_PROC_ID in ( %s ) and PC_MAXIMO >= %d ) ) and ' +
                               '( ( PC_FEC_FIN > %s ) or ( PC_FEC_FIN = %s and PC_HOR_FIN >= %s ) ) and ' +
                               '( ( PC_FEC_FIN < %s ) or ( PC_FEC_FIN = %s and PC_HOR_FIN <= %s ) ) ' +
                               'order by PC_FEC_INI, PC_HOR_INI';
          eEmpresas: Result := 'select CM_CODIGO, CM_NOMBRE, CM_ALIAS, CM_USRNAME, CM_PASSWRD, CM_DATOS '+
                               'from COMPANY order by CM_NOMBRE';
     else
         Result := VACIO;
     end;
end;

{ ********* TCheck ********* }

constructor TCheck.Create(Monitor: TMonitor);
var
   Objecto: TComponent;
begin
     Objecto := nil;
     oZetaProvider := TdmZetaServerProvider.Create( Objecto );
     FEmpresas := TList.Create;
     FMonitor := Monitor;
     FMonitor.Add( Self );
     FStart := NullDateTime;
     FFrequency := NullDateTime;
     FAvail := True;
     FTest := -1;
     FMessageHeader := VACIO;
     Init;
end;

destructor TCheck.Destroy;
begin
     inherited Destroy;
     FEmpresas.Free;
     FreeAndNil( oZetaProvider );
end;

procedure TCheck.Start;
begin
     FStart := NullDateTime;
end;

procedure TCheck.Stop;
begin
end;

procedure TCheck.RunCheck;
begin
     FActualDate := Now;
     if ( FFrequency <> NullDateTime ) and ( ( FActualDate - FStart ) > FFrequency ) then
     begin
          DoCheck;
          if lFecha then
          begin
               FStart := FActualDate;
               dtFecha := FStart;
               lFecha := FALSE;
          end
          else
              FStart := dtFecha;
     end;
end;

procedure TCheck.Continue;
begin
     FStart := dtFecha;
end;

procedure TCheck.Pause;
begin
end;

procedure TCheck.ShutDown;
begin
end;

function TCheck.StrToTime( const sValue: String ): TDateTime;
const
     K_DIA = 24;
     K_24HORAS = '0000';
var
   iHoras, iDias : Integer;
begin
     if ( sValue <> K_24HORAS ) and StrLleno( sValue ) then
     begin
          iHoras := StrToIntDef( Copy( sValue, 1, 2 ), 0 );
          iDias := Trunc( iHoras / K_DIA );
          iHoras := iHoras - ( iDias * K_DIA );
          Result := EncodeTime( iHoras, StrToIntDef( Copy( sValue, 3, 2 ), 0 ), 0, 0 );
          if ( iDias > 0 ) then
             Result := Result + iDias;
     end
     else
         Result := NullDateTime;
end;


function TCheck.GetEmpresasCount: Integer;
begin
     Result := FEmpresas.Count;
end;

function TCheck.GetEmpresas( Index: Integer ): TEmpresaData;
begin
     Result := TEmpresaData( FEmpresas.Items[ Index ] );
end;

function TCheck.GetListaEmpresas: Boolean;
var
   qEmpresas: TZetaCursor;
begin
     Result := False;
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Comparte;
        end;
        qEmpresas := oZetaProvider.CreateQuery( GetScript( eEmpresas ) );
        with qEmpresas do
        begin
             try
                Active := True;
                while not EOF do
                begin
                     TEmpresaData.Create( FEmpresas,
                                          FieldByName( 'CM_CODIGO' ).AsString,
                                          FieldByName( 'CM_NOMBRE' ).AsString,
                                          FieldByName( 'CM_ALIAS' ).AsString,
                                          FieldByName( 'CM_USRNAME' ).AsString,
                                          FieldByName( 'CM_PASSWRD' ).AsString,
                                          FieldByName( 'CM_DATOS' ).AsString,
                                          FALSE );
                     Next;
                     Result := TRUE;
                end;
                Active := False;
             finally
                    Free;
             end;
        end;
     except
           on Error: Exception do
           begin
                ZetaDialogo.ZError( 'Monitor', Format( 'Error con las Bases de Datos' + CR_LF + '%s', [ Error.Message ] ), 0 ); //raise
           end;
     end;
end;

procedure TCheck.ClearEmpresas;
var
   i: Integer;
begin
     for i := ( EmpresasCount - 1 ) downto 0 do
     begin
          Empresas[ i ].Free;
     end;
end;

procedure TCheckHardDiskSpace.Init;
begin
     with Monitor.Registry do
     begin
          FFrequency := StrToTime( HardDiskFrequency );
          FLimitYellow := HardDiskSpaceLimitYellow;
          FLimitRed := HardDiskSpaceLimitRed;
          FDisk := HardDisks;
          FAvail := HardDisksAvail;
          FMessageHeader := dmMonitorTress.ObtieneElemento( Ord( eEspacio ) );
     end;
end;

procedure TCheckHardDiskSpace.DoCheck;
const
     K_PORCENTAJE = '%';
     K_CIEN = 100;
     K_DOBLE = 2;

var
   sDisk: String;
   cDisk: Char;
   i, iValue: Word;
   iSize, iAvailable, iUsed: Int64;
   FLista, FDisks: TStrings;
{$ifdef INTERBASE}
   sTitulo: string;
   iTemporal: Int64;
   lCheckTemp: Boolean;
   rValue: Double;
{$endif}

begin
     if FAvail then
     Begin
          Monitor.CallMeBack( 'Checking Hard Disk Space' );
          FLista := TStringList.Create;
          FDisks := TStringList.Create;
          try
             with FDisks do
             begin
                  Clear;
                  CommaText := FDisk;
                  for i := 0 to ( Count - 1 ) do
                  begin
                       FTest := Ord( eEspacio );
                       sDisk := Copy( Strings[ i ], 1, 1 );
                       if ZetaCommonTools.StrLleno( sDisk ) then
                       begin
                            with dmMonitorTress do
                            begin
                                 cDisk := sDisk[ 1 ];
                                 if ZetaWinAPITools.DriveIsFixed( cDisk ) then
                                 begin
                                      // Checa Espacio en HD
                                      ZetaWinAPITools.GetDiskSpace( Ord( cDisk ) - K_DISK_A, iSize, iAvailable );
                                      iUsed := iSize - iAvailable;
                                      iValue := Trunc( K_CIEN * ( iUsed / iSize ) );
                                      if ( iValue >= FLimitRed ) then
                                         Monitor.ReportRedAlert( MessageHeader, Format( GetComentario( eError, eEspacio ), [ cDisk, IntToStr( FLimitRed ), K_PORCENTAJE ] ), ObtieneResultado( Ord( eError ) ), FTest )
                                      else
                                          if ( iValue >= FLimitYellow ) then
                                             Monitor.ReportYellowAlert( MessageHeader, Format( GetComentario( eWarning, eEspacio ), [ cDisk, IntToStr( FLimitYellow ), K_PORCENTAJE ] ), FTest )
                                          else
                                              Monitor.ReportGreenAlert( MessageHeader, Format( GetComentario( eOk, eEspacio ), [ cDisk ] ), ObtieneResultado( Ord( eOk ) ), FTest );

                                      {$ifdef INTERBASE}
                                      //Checa Espacio para Temporal
                                      FTest := Ord( eDirectorioTemp );
                                      lCheckTemp := SetListaTemporal( FLista );
                                      sTitulo := dmMonitorTress.ObtieneElemento( FTest );
                                      if lCheckTemp then
                                      begin
                                           iTemporal := StrToInt64Def( FLista.Values[ cDisk ], 0 );
                                           if ( iTemporal > 0 ) then
                                           begin
                                                rValue := ( iAvailable / iTemporal );
                                                if ( rValue >= K_DOBLE ) then            // El Doble
                                                   Monitor.ReportGreenAlert( sTitulo, GetComentario( eOk, eDirectorioTemp), ObtieneResultado( Ord( eOk ) ), FTest )
                                                else if ( rValue >= 1 ) then       // Entre el doble y lo requerido
                                                   Monitor.ReportYellowAlert( sTitulo, GetComentario( eWarning, eDirectorioTemp), FTest )
                                                else                               // Menos de lo Requerido
                                                   Monitor.ReportRedAlert( sTitulo, GetComentario( eError, eDirectorioTemp), ObtieneResultado( Ord( eError ) ), FTest ); 
                                           end;
                                      end
                                      else
                                          Monitor.ReportGreenAlert( sTitulo, GetComentario( eOK, eDirectorioTemp ), ObtieneResultado( Ord( eOk ) ), FTest );
                                      {$endif}
                                 end;
                            end;
                       end;
                  end;
             end;
          finally
                 FreeAndNil( FLista );
                 FreeAndNil( FDisks );
          end;
    end;
end;

{$ifdef INTERBASE}
function TCheckHardDiskSpace.SetListaTemporal( Lista: TStrings ): Boolean;
const
     K_FILE_CONFIG = 'ibconfig';
     K_DISABLED_CHAR = '#';
     K_CONFIG_PARAM = 'TMP_DIRECTORY';
     K_ESPACIO = ' ';

var
   iValue : Int64;
   FAsciiServer: TAsciiServer;
   sArchivo, sRenglon, sDrive, sSize: String;

   procedure SetRowValues;
   var
      iPos: Integer;
      sTemp : String;
   begin
        sDrive := VACIO;
        sSize := VACIO;
        sTemp := Trim( strRight( sRenglon, Length( sRenglon ) - Pos( K_ESPACIO, sRenglon ) ) );
        iPos := Pos( K_ESPACIO, sTemp );
        sSize := strLeft( sTemp, iPos - 1 );
        sDrive := strLeft( ExtractFileDrive( strTransAll( Trim( strRight( sTemp, Length( sTemp ) - iPos ) ), '"', VACIO ) ), 1 );
   end;

begin
     Lista.Clear;
     sArchivo := Monitor.Registry.RootDirectory + K_FILE_CONFIG;
     if FileExists( sArchivo ) then
     begin
          FAsciiServer := TAsciiServer.Create;
          with FAsciiServer do
          begin
               try
                  if Open( sArchivo ) then
                  begin
                       repeat
                             sRenglon := Trim( strTransAll( Data, Chr( 9 ), ' ' ) );
                             if ( Copy( sRenglon, 1, 1 ) <> K_DISABLED_CHAR ) and ( Pos( K_CONFIG_PARAM, sRenglon ) > 0 ) then
                             begin
                                  SetRowValues;
                                  iValue := StrToInt64Def( Lista.Values[ sDrive ], 0 );
                                  if ( iValue > 0 ) then
                                     Lista.Values[ sDrive ] := IntToStr( iValue + StrToInt64Def( sSize, 0 ) )
                                  else
                                     Lista.Add( sDrive + '=' + sSize );
                             end;
                       until not Read;
                       Close;
                  end;
               finally
                      FreeAndNil( FAsciiServer );
               end;
          end;
     end;
     Result := ( Lista.Count > 0 );
end;
{$endif}

{ ********* TCheckIBServices *********** }

procedure TCheckIBServices.Init;
begin
     {$ifdef INTERBASE}
     with Monitor.Registry do
     begin
          FFrequency := StrToTime( InterbaseServicesFrequency );
          FAvail := ServicesAvail;
          FMessageHeader := dmMonitorTress.ObtieneElemento( Ord( eInterbaseServices ) );
     end;
     {$endif}
end;

procedure TCheckIBServices.DoCheck;
{$ifdef INTERBASE}
const
     K_IBGUARDIAN = 'InterBaseGuardian';
     K_IBSERVER = 'InterBaseServer';
{$endif}
begin
     {$ifdef INTERBASE}
     if FAvail then
     begin
          Monitor.CallMeBack( 'Checking Interbase Services' );
          FTest := Ord( eInterbaseServices );
          with dmMonitorTress do
          begin
               if ZetaWinAPITools.OpenServiceManager then
               begin
                    try
                       if ZetaWinAPITools.FindService( K_IBGUARDIAN ) then
                       begin
                            if ZetaWinAPITools.FindService( K_IBSERVER ) then
                            begin
                                 if ( ZetaWinAPITools.GetServiceStatus( K_IBGUARDIAN ) = WinSvc.SERVICE_RUNNING ) and
                                    ( ZetaWinAPITools.GetServiceStatus( K_IBGUARDIAN ) = WinSvc.SERVICE_RUNNING ) then
                                    Monitor.ReportGreenAlert( MessageHeader, GetComentario( eOk, eInterbaseServices ), ObtieneResultado( Ord( eOk ) ), FTest )
                                 else
                                    Monitor.ReportRedAlert( MessageHeader, GetComentario ( eError, eInterbaseServices ), ObtieneResultado( Ord( eError ) ), FTest );
                            end
                            else
                                Monitor.ReportRedAlert( MessageHeader, GetComentario ( eError, eInterbaseServices ), ObtieneResultado( Ord( eError ) ), FTest );
                       end
                       else
                           Monitor.ReportRedAlert( MessageHeader, GetComentario ( eError, eInterbaseServices ), ObtieneResultado( Ord( eError ) ), FTest );
                    finally
                           ZetaWinAPITools.CloseServiceManager;
                    end;
               end
               else
                   Monitor.ReportRedAlert( MessageHeader, GetComentario ( eError, eInterbaseServices ), ObtieneResultado( Ord( eError ) ), FTest );
          end;
     end;
     {$endif}
end;

{ ********* TCheckDataBases *********** }

procedure TCheckDataBases.Init;
begin
     FDataBaseName := VACIO;
     FProcesarComparte := FALSE;
end;

procedure TCheckDataBases.DoCheck;
var
   i: Integer;

begin
     if Monitor.Registry.ProcessAvail or Monitor.Registry.PollAvail then
     begin
          FDataBaseName := VACIO;
          Monitor.CallMeBack( 'Checking Tress Databases' );
          if Not GetListaEmpresas then
             Monitor.ReportRedAlert( 'DatabaseConnection', 'Connection to Database', dmMonitorTress.ObtieneResultado( Ord( eError ) ), FTest )
          else
          begin
               for i := 0 to ( EmpresasCount - 1 ) do
               begin
                    with Empresas[ i ] do
                    begin
                         if ( FProcesarComparte or ( not EsComparte ) ) then
                         begin
                              Monitor.CallMeBack( Format( 'Checking Tress DB ( %s )', [ Nombre ] ) );
                              oZetaProvider.EmpresaActiva := VarArrayOf( [ Datos, UserName, Password ] );
                              FDataBaseName := Nombre;
                              {$ifdef MARCO}
                              showmessage( 'Base de datos: ' + FDataBaseName );
                              {$endif}
                              RevisaDBInfo( EsComparte, Codigo );
                         end;
                    end;
               end;
               ClearEmpresas;
          end;
     end;
end;

{ ********* TCheckDataBasesDaily *********** }

procedure TCheckDataBasesDaily.Init;
begin
     inherited;
     with Monitor.Registry do
     begin
          FFrequency := StrToTime( DBConfigurationFrequency );
          FEmployeesRate := DBLimit;
          FMinimumEmployees := DBProcessesEmpleados;
          FHours := strToTime( PollHoras );
          FCompanys := PollCompanys;
          FAvail := PollAvail;
     end;
     FProcesarComparte := TRUE;
end;

procedure TCheckDataBasesDaily.RevisaDBInfo( const lComparte: Boolean; sCodigo : string );
const
     K_SWEEP_INTERVAL = 0;
     K_BUFFERS_DATA = 10000;
     K_BUFFERS_COMPARTE = 5000;
     K_PAGE_SIZE = 4096;

{$ifdef INTERBASE}
var
   iMinBuffers : Word;
   sSweepInterval, sForcedWrites, sDataBuffers, sPageSize: string;
{$endif}

   function GetListaProcesos: String;
   begin
        Result := VACIO;
        Result := ConcatString( Result, IntToStr( Ord( prASISProcesarTarjetas ) ), ',' );
        Result := ConcatString( Result, IntToStr( Ord( prASISCalculoPreNomina ) ), ',' );
        Result := ConcatString( Result, IntToStr( Ord( prNOCalcular ) ), ',' );
   end;

   function GetListaStatus: String;
   begin
        Result := VACIO;
        Result := ConcatString( Result, ZetaCommonTools.EntreComillas( B_PROCESO_OK ), ',' );
        Result := ConcatString( Result, ZetaCommonTools.EntreComillas( B_PROCESO_ERROR ), ',' );
   end;

begin
     if FAvail then
     begin
          lCheck := True;

          {$ifdef INTERBASE}
          with oZetaProvider.ibEmpresa do
          begin
               with dmMonitorTress do
               begin
                    // Revisa Sweep Interval
                    FTest := Ord( eSweepInterval );
                    sSweepInterval := dmMonitorTress.ObtieneElemento( FTest );
                    if ( SweepInterval > K_SWEEP_INTERVAL ) then
                       Monitor.ReportRedAlert( sSweepInterval, Format( GetComentario( eError, eSweepInterval ), [ sCodigo ] ), ObtieneResultado( Ord( eError ) ), FTest )
                    else
                        Monitor.ReportGreenAlert( sSweepInterval, Format( GetComentario( eOk, eSweepInterval ), [ sCodigo ] ), ObtieneResultado( Ord( eOk ) ), FTest );

                    // Revisa Enabled Force Writes
                    FTest := Ord( eForcedWrites );
                    sForcedWrites := dmMonitorTress.ObtieneElemento( FTest );
                    if ( Characteristics.dbForced_Writes = 1 ) then
                       Monitor.ReportRedAlert( sForcedWrites, Format( GetComentario( eError, eForcedWrites ), [ sCodigo ] ), ObtieneResultado( Ord( eError ) ), FTest )
                    else
                        Monitor.ReportGreenAlert( sForcedWrites, Format( GetComentario( eOk, eForcedWrites ), [ sCodigo ] ), ObtieneResultado( Ord( eOk ) ), FTest );

                    // Revisa Buffers
                    FTest := Ord( eBuffersData );
                    sDataBuffers := dmMonitorTress.ObtieneElemento( FTest );
                    if ( Characteristics.dbPage_Buffers > K_BUFFERS_DATA ) then
                       Monitor.ReportRedAlert( sDataBuffers, Format( GetComentario( eError, eBuffersData ), [ sCodigo ] ), ObtieneResultado( Ord( eError ) ), FTest )
                    else
                    begin
                         iMinBuffers := K_BUFFERS_DATA;
                         if ( Characteristics.dbPage_Buffers < iMinBuffers ) then
                            Monitor.ReportRedAlert( sDataBuffers, Format( GetComentario( eError, eBuffersData ), [ sCodigo ] ), ObtieneResultado( Ord( eError ) ), FTest )
                         else
                             Monitor.ReportGreenAlert( sDataBuffers, Format( GetComentario( eOk, eBuffersData ), [ sCodigo ] ), ObtieneResultado( Ord( eOk ) ), FTest );
                    end;

                    // Revisa Page Size
                    FTest := Ord( ePageSize );
                    sPageSize := dmMonitorTress.ObtieneElemento( FTest );
                    if ( Characteristics.dbPage_Size <> K_PAGE_SIZE ) then
                       Monitor.ReportRedAlert( sPageSize, Format( GetComentario( eError, ePageSize ), [ sCodigo ] ), ObtieneResultado( Ord( eError ) ), FTest )
                    else
                        Monitor.ReportGreenAlert( sPageSize, Format( GetComentario( eOk, ePageSize ), [ sCodigo ] ), ObtieneResultado( Ord( eOk ) ), FTest );

                    // Revisi�n del Tama�o de Archivo gbk
                    FTest := Ord( eDBArchivos );
                    RevisaDBSize( Path );
               end;
          end;
          {$endif}
          // Revisi�n de Procesos
          if not lComparte then
             CheckProcesses( GetListaProcesos, GetListaStatus, sCodigo );
     end;
end;

procedure TCheckDataBasesDaily.RevisaDBSize( const sDBFile: String );
const
     K_IB_FILESIZE_LIMIT = 4294967296;
     K_WARNING_SIZE = 75;
     K_CIEN = 100;
     K_PORCENTAJE = '%';

{$ifdef INTERBASE}
var
   iSize : Int64;
   sInterBaseFileSize: string;
{$endif}

begin
     {$ifdef INTERBASE}
     sInterBaseFileSize := dmMonitorTress.ObtieneElemento( FTest );
     iSize := GetFileSize( sDBFile );
     with dmMonitorTress do
     begin
          if ( iSize > Trunc( K_IB_FILESIZE_LIMIT * ( K_WARNING_SIZE / K_CIEN ) ) ) then
             Monitor.ReportRedAlert( sInterBaseFileSize, Format( GetComentario( eError, eDBArchivos ), [ K_WARNING_SIZE, K_PORCENTAJE, FDataBaseName ] ), ObtieneResultado( Ord( eError ) ), FTest )
          else
              Monitor.ReportGreenAlert( sInterBaseFileSize, Format( GetComentario( eOk, eDBArchivos ), [ FDataBaseName ] ), ObtieneResultado( Ord( eOk ) ), FTest );
     end;
     {$endif}
end;

procedure TCheckDataBasesDaily.CheckProcesses( const sProcesos, sStatus, sCodigo: String );
var
   dStartProc : TDateTime;
   i : Integer;
   lValidaPoll, lEncontrado: Boolean;
   Companys: TStrings;
   qProcesos: TzetaCursor;
   sPollRelojes: string;

begin
     FTest := Ord( ePollRelojes );
     lEncontrado := FALSE;
     if ( FStart = NullDateTime ) then
        dStartProc := FActualDate - FHours
     else
        dStartProc := FActualDate - FHours;
     Companys := TStringList.Create;
     try
        if FCompanys <> VACIO then
        begin
             with Companys do
             begin
                  Commatext := FCompanys;
                  for i := 0 to ( Count -1 ) do
                  begin
                       if ( strings[ i ] = sCodigo ) then
                       begin
                            lEncontrado := TRUE;
                            Break;
                       end;
                  end;
             end;
        end
        else
            lEncontrado := TRUE;
        if lEncontrado then
        begin
             qProcesos := oZetaProvider.CreateQuery( Format( GetScript( eProcesos ), [ sStatus, Ord( prASISProcesarTarjetas ), sProcesos, FMinimumEmployees, DateToStrSQLC( dStartProc ),
                                         DateToStrSQLC( dStartProc ), EntreComillas( FormatDateTime( 'hh:nn:ss', dStartProc ) ),
                                         DateToStrSQLC( FActualDate ), DateToStrSQLC( FActualDate ),
                                         EntreComillas( FormatDateTime( 'hh:nn:ss', FActualDate ) ) ] ) );
             with qProcesos do
             begin
                  with dmMonitorTress do
                  begin
                       sPollRelojes := ObtieneElemento( FTest );
                       Active := TRUE;
                       try
                          lValidaPoll := FALSE;
                          while not EOF do
                          begin
                               lValidaPoll := lValidaPoll or ( Procesos( FieldByName( 'PC_PROC_ID' ).AsInteger ) = prASISProcesarTarjetas );
                               Next;
                          end;
                          if not lValidaPoll then
                             Monitor.ReportRedAlert( sPollRelojes, Format( GetComentario( eError, ePollRelojes ), [ sCodigo ] ), ObtieneResultado( Ord( eError ) ), FTest )
                          else
                              Monitor.ReportGreenAlert( sPollRelojes, Format( GetComentario( eOk, ePollRelojes ), [ sCodigo ] ), ObtieneResultado( Ord( eOk ) ), FTest );
                       finally
                          Active := FALSE;
                       end;
                  end;
             end;
        end;
     finally
            FreeAndNil( Companys );
     end;
end;

{ ********* TCheckDataBasesHourly *********** }

procedure TCheckDataBasesHourly.Init;
begin
     inherited;
     with Monitor.Registry do
     begin
          FFrequency := StrToTime( DBProcessesFrequency );
          FMaximumTime := DBProcessesTime;
          FAvail := ProcessAvail;
     end;
end;

procedure TCheckDataBasesHourly.RevisaDBInfo( const lComparte: Boolean; sCodigo: string );
var
   dFecha: TDateTime;
   qAbiertos, qError: TZetaCursor;
   lAbiertos, lError: boolean;
   sOpenProcesses, sErrorProcesses: string;

begin
     if FAvail then
     begin
          lAbiertos := False;
          lError := False;
          Ftest := Ord( eProcAbiertos );
          lCheck := False;
          // Revisa Procesos Abiertos por mas de FMaximumTime
          dFecha := FActualDate - FMaximumTime;
          qAbiertos := oZetaProvider.CreateQuery( Format( GetScript( eAbiertos ), [ EntreComillas( B_PROCESO_ABIERTO ), DateToStrSQLC( dFecha ),
                                 DateToStrSQLC( dFecha ), EntreComillas( FormatDateTime( 'hh:nn:ss', dFecha ) ) ] ) );
          with dmMonitorTress do
          begin
               sOpenProcesses := ObtieneElemento( FTest );
               with qAbiertos do
               begin
                    Active := TRUE;
                    try
                       while not EOF do
                       begin
                            lAbiertos := True;
                            Next;
                       end;
                       if lAbiertos then
                          Monitor.ReportRedAlert( sOpenProcesses, Format( GetComentario( eError, eProcAbiertos ), [ sCodigo ] ), ObtieneResultado( Ord( eError ) ), FTest )
                       else
                           Monitor.ReportGreenAlert( sOpenProcesses, Format( GetComentario( eOK, eProcAbiertos ), [ sCodigo ] ), ObtieneResultado( Ord( eOk ) ) , FTest );  
                    finally
                       Active := FALSE;
                    end;
               end;
               // Revisar Procesos con Errores Graves desde la ultima revisi�n, sin importar status
               FTest := Ord( eProcesosError );
               sErrorProcesses := ObtieneElemento( FTest );
               if ( FStart = NullDateTime ) then
                  dFecha := FActualDate - FFrequency
               else
                  dFecha := FStart;
               qError := oZetaProvider.CreateQuery( Format( GetScript( eErrores ), [ DateToStrSQLC( dFecha ), DateToStrSQLC( dFecha ),
                                              EntreComillas( FormatDateTime( 'hh:nn:ss', dFecha ) ),
                                              DateToStrSQLC( FActualDate ), DateToStrSQLC( FActualDate ),
                                              EntreComillas( FormatDateTime( 'hh:nn:ss', FActualDate ) ),
                                              Ord( tbErrorGrave ) ] ) );
               with qError do
               begin
                    Active := TRUE;
                    try
                       while not EOF do
                       begin
                            lError := True;
                            Next;
                       end;
                       if lError then
                          Monitor.ReportRedAlert( sErrorProcesses, Format( GetComentario( eError, eProcesosError ), [ sCodigo ] ), ObtieneResultado( Ord( eError ) ), FTest )
                       else
                           Monitor.ReportGreenAlert( sErrorProcesses, Format( GetComentario( eOK, eProcesosError ), [ sCodigo ] ), ObtieneResultado( Ord( eOk ) ), FTest );
                    finally
                       Active := FALSE;
                    end;
               end;
          end;
     end;
end;

{ ************** TCheckGBK ************* }

procedure TCheckGBK.Init;
begin
     {$ifdef INTERBASE}
     with Monitor.Registry do
     begin
          FFrequency := StrToTime( InterbaseGBKFrequency );
          FDirectoryGBK := ZetaCommonTools.VerificaDir( DirectoryGBK );
          FHoras := StrToTime( InterbaseGBKHoras );
          FAvail := ArchivosGBKAvail;
          FMessageHeader := dmMonitorTress.ObtieneElemento( Ord( eArchivosGBK ) );
     end;
     {$endif}
end;

procedure TCheckGBK.DoCheck;
{$ifdef INTERBASE}
     function FindAllFilesOfType( const sFileSpec: String ) : boolean;
     var
        iFound: Integer;
        FileInfo: TSearchRec;
        dFecha: TDateTime;

     begin
          Result := FALSE;
          iFound := FindFirst( sFileSpec, faAnyFile, FileInfo );
          try
             while ( iFound = 0 ) do
             begin
                  dFecha := FileDateToDateTime( FileInfo.Time );
                  if ( dFecha > fFechaIni ) and ( dFecha < FFechaFin ) then
                     Result := TRUE;
                  iFound := FindNext( FileInfo );
             end;
          finally
                 FindClose( FileInfo );
          end;
     end;
{$endif}
begin
     {$ifdef INTERBASE}
     if FAvail then
     begin
          Monitor.CallMeBack( 'Checking Interbase Backups' );
          FTest := Ord( eArchivosGBK );
          FFechaFin := FActualDate;
          FFechaIni := FFechaFin - FHoras;
          with dmMonitorTress do
          begin
               if ( FindAllFilesOfType( FDirectoryGBK + '*.GBK' ) ) then
                  Monitor.ReportGreenAlert( MessageHeader, GetComentario( eOK, eArchivosGBK ), ObtieneResultado( Ord( eOk ) ), FTest )
               else
                   Monitor.ReportRedAlert( MessageHeader, GetComentario( eError, eArchivosGBK ), ObtieneResultado( Ord( eError ) ), FTest );
          end;
     end;
     {$endif}
end;

destructor TCheckGBK.Destroy;
begin
  inherited;
end;

{ ************* TCheckSentinel **************** }

procedure TCheckSentinel.Init;
begin
     with Monitor.Registry do
     begin
          FFrequency := StrToTime( LicenseSentinelFrequency );
          FDirectory := CopyDirectory;
          FDaysLicenseUse := LicenseUseDaysLimit;
          FAvail := LicenciaUsoAvail;
          FMessageHeader := dmMonitorTress.ObtieneElemento( Ord( eSentinelSuperPro ) );
     end;
end;

{$ifdef SENTINELVIRTUAL}
procedure TCheckSentinel.SentinelLog( const sMensaje: String );
begin
     FBitacora.WriteTimeStamp( sMensaje );
end;

procedure TCheckSentinel.SentinelError( const sMensaje: String );
begin
     FBitacora.WriteTimeStamp( sMensaje );
end;
{$endif}

procedure TCheckSentinel.DoCheck;
var
   //Variables de la prueba de Sentinel
   FAutoServer: TAutoServer;
   //FBitacora: TAsciiLog;

   //Variables de la Prueba de Licencia
   dDiferencia: TDate;
   sMensaje, sDescripcion: string;
begin
     if FAvail then
     begin
          Monitor.CallMeBack( 'Checking Sentinel' );
          FTest := Ord( eSentinelSuperPro );
          FBitacora := TasciiLog.Create;
          FAutoServer := TAutoServer.Create;
          try
             with FAutoServer do
             begin
                  {$ifdef INTERBASE}
                          {$ifdef CORPORATIVA}
                          SQLType := engInterbase;
                          AppType := atCorporativa;
                          {$endif}
                  {$endif}
                  {$ifdef INTERBASE}
                          {$ifdef DOS_CAPAS}
                          SQLType := engInterbase;
                          AppType := atProfesional;
                          {$endif}
                  {$endif}
                  {$ifdef MSSQL}
                  SQLType := engMSSQL;
                  AppType := atCorporativa;
                  {$endif}
                  Cargar;

                  with dmMonitorTress do
                  begin
                       //Prueba del Sentinel
                       if ( EsDemo ) then
                       begin
                       {$ifdef SENTINELVIRTUAL}
                            if ExisteSentinelVirtual then
                            begin
                               Monitor.ReportGreenAlert( MessageHeader, GetComentario( eWarning, eSentinelSuperPro ), ObtieneResultado( Ord( eWarning ) ), FTest );
                               FBitacora.Init( ZetaCommonTools.VerificaDir( FDirectory ) + 'TressMonitor.Log' );
                               FBitacora.WriteTimeStamp( '%s --> ' + StatusMsg );
                               //Inicia Prueba de  Sentinel Virtual
                               Monitor.CallMeBack( 'Checking Tress Virtual Sentinel' );

                               if ValidarSentinelVirtual( SentinelLog, SentinelError ) then
                                  Monitor.ReportGreenAlert( MessageHeader, GetComentario( eOk, eSentinelSuperPro ), ObtieneResultado( Ord( eOk ) ), FTest )
                               else
                                  Monitor.ReportRedAlert( MessageHeader, GetComentario( eError, eSentinelSuperPro ), ObtieneResultado( Ord( eError ) ), FTest );

                            end
                            else
                            begin
                       {$endif}
                               Monitor.ReportRedAlert( MessageHeader, GetComentario( eError, eSentinelSuperPro ), ObtieneResultado( Ord( eError ) ), FTest );
                               FBitacora.Init( ZetaCommonTools.VerificaDir( FDirectory ) + 'TressMonitor.Log' );
                               FBitacora.WriteTimeStamp( '%s --> ' + StatusMsg );
                       {$ifdef SENTINELVIRTUAL}
                            end;
                       {$endif}
                       end
                       else
                           Monitor.ReportGreenAlert( MessageHeader, GetComentario( eOk, eSentinelSuperPro ), ObtieneResultado( Ord( eOk ) ), FTest );


                       //Inicia Prueba de Licencia
                       Monitor.CallMeBack( 'Checking Tress License' );
                       FTest := Ord( eLicenciaUso );
                       sMensaje := dmMonitorTress.ObtieneElemento( FTest );
                       sDescripcion := 'Tress License %s';
                       if Not( EsDemo ) then
                       begin
                            if ( Vencido ) then
                               Monitor.ReportRedAlert( sMensaje, GetComentario( eError, eLicenciaUso ), ObtieneResultado( Ord( eError ) ), FTest )
                            else
                            begin
                                 dDiferencia := Vencimiento - FActualDate;
                                 if ( dDiferencia < FDaysLicenseUse ) then
                                    Monitor.ReportYellowAlert( sMensaje, GetComentario( eWarning, eLicenciaUso ), FTest )
                                 else
                                     Monitor.ReportGreenAlert( sMensaje, GetComentario( eOk, eLicenciaUso ), ObtieneResultado( Ord( eOK ) ), FTest );
                            end;
                       end
                       else
                       begin
                           dDiferencia := Vencimiento - FActualDate;
                           if ( dDiferencia < FDaysLicenseUse ) then
                              Monitor.ReportYellowAlert( sMensaje, GetComentario( eWarning, eLicenciaUso ), FTest )
                           else
                               Monitor.ReportGreenAlert( sMensaje, GetComentario( eOk, eLicenciaUso ), ObtieneResultado( Ord( eOK ) ), FTest );
                       end;
                  end;
             end;
          finally
                 FreeAndNil( FBitacora );
                 FreeAndNil( FAutoServer );
          end;
     end;
end;

destructor TCheckSentinel.Destroy;
begin
     inherited;
end;

{ **************** TCheckODBCPooling **************** }
procedure TCheckODBCPooling.Init;
begin
     with Monitor.Registry do
     begin
          FFrequency := StrToTime( ODBCFrequency );
          FAvail := ODBCAvail;
          FMessageHeader := dmMonitorTress.ObtieneElemento( Ord( eConnectionPooling ) ); 
     end;
end;

procedure TCheckODBCPooling.DoCheck;
begin
     if FAvail then
     begin
          Monitor.CallMeBack( 'Checking ODBC Connection Pooling' );
          FTest := Ord( eConnectionPooling );
          with dmMonitorTress do
          begin
               if ( ZetaODBCTools.GetIntersolvPooling <= 0 ) then
                  Monitor.ReportRedAlert( MessageHeader, GetComentario( eError, eConnectionPooling ), ObtieneResultado( Ord( eError ) ), FTest )
               else
                   Monitor.ReportGreenAlert( MessageHeader, GetComentario( eOk, eConnectionPooling ), ObtieneResultado( Ord( eOk ) ), FTest );
          end;
     end;
end;

{ ********************** TCheckODBCTracing ********************* }

procedure TCheckODBCTracing.Init;
begin
     with Monitor.Registry do
     begin
          FFrequency := StrToTime( ODBCFrequency );
          FAvail := ODBCAvail;
          FMessageHeader := dmMonitorTress.ObtieneElemento( Ord( eTracing ) ); 
     end;
     FTRaceFile := ZetaODBCTools.GetODBCTraceFile;
     FSize := GetFileSize( FTraceFile );
end;


procedure TCheckODBCTracing.DoCheck;
var
   sFile : string;
   iSize : integer;

begin
     if FAvail then
     begin
          Monitor.CallMeBack( 'Checking ODBC Tracing' );
          FTest := Ord( eTracing );
          sFile := ZetaODBCTools.GetODBCTraceFile;
          with dmMonitorTress do
          begin
               if ( StrLleno( sFile ) ) then
               begin
                    iSize := GetFileSize( sFile );
                    if ( sFile = FTraceFile ) then
                    begin
                         if ( iSize <> FSize ) then
                            Monitor.ReportRedAlert( MessageHeader, GetComentario( eError, eTracing ), ObtieneResultado( Ord( eError ) ), FTest )
                         else
                             Monitor.ReportGreenAlert( MessageHeader, GetComentario( eOk, eTracing ), ObtieneResultado( Ord( eOk ) ), FTest );
                    end
                    else
                    begin
                         FTraceFile := sFile;
                         FSize := iSize;
                         Monitor.ReportGreenAlert( MessageHeader, GetComentario( eOk, eTracing ), ObtieneResultado( Ord( eOk ) ), FTest );
                    end;
               end
               else
                   Monitor.ReportGreenAlert( MessageHeader, GetComentario( eOk, eTracing ), ObtieneResultado( Ord( eOk ) ), FTest );
          end;
     end;
end;

{ ************** TCheckIBLog ******************* }

procedure TCheckIBLog.Init;
begin
     {$ifdef INTERBASE}
     with Monitor.Registry do
     begin
          FFrequency := StrToTime( InterbaseLogFrequency );
          FDirectoryIB := RootDirectory;
          FAvail := InterbaselogAvail;
          FMessageHeader := dmMonitorTress.ObtieneElemento( Ord( eInterbaseLog ) );
     end;
     FAscii := TAsciiLog.Create
     {$endif}
end;

procedure TCheckIBLog.DoCheck;
{$ifdef INTERBASE}
const
     K_LOG = 'Interbase.log';
     K_MENSAJE = 'consistency';
     K_BAK = 'InterBase.BKP';
     K_I = 0;
     K_S = 9;
     K_VACIO = ' ';
     K_TAB = Chr( 09 );
     aMonthFecha: array[ 1..12 ] of PChar =( 'Jan',
                                             'Feb',
                                             'Mar',
                                             'Apr',
                                             'May',
                                             'Jun',
                                             'Jul',
                                             'Aug',
                                             'Sep',
                                             'Oct',
                                             'Nov',
                                             'Dec' );
var
   FAsciiIB, FASciiB: TAsciiServer;
   sFile, sTemp: string;
   aLineas: array[K_I..K_S] of string;
   i, iCont: integer;
   dFecha: TDateTime;
   lEncontrado: boolean;

   function GetMonth( const sMes: String ): Integer;
   var
      i : Integer;
   begin
        Result := 0;
        for i := 1 to 12 do
            if ( aMonthFecha[ i ] = sMes ) then
            begin
                 Result := i;
                 Break;
            end;
   end;

   function GetFecha( const sLinea: String ): TDateTime;
   var
      iPosicion, iLargo, iMes, iDia, iAnio, i : integer;
      sFechaStr,  sHora : string;
   begin
        Result := EncodeDate( 1, 1, 1 );
        for i := 1 to 12 do
        begin
            iPosicion := AnsiPos( aMonthFecha[ i ], sLinea );
            if ( iPosicion <> 0 ) then
            begin
                 iLargo := length( sLinea );
                 iLargo := iLargo - iPosicion;
                 sFechaStr := copy( sLinea, iPosicion, iLargo + 1 );
                 iMes := GetMonth( copy( sFechaStr, 1, 3 ) );
                 iDia := StrToint( copy( sFechaStr, 5, 2 ) );
                 iAnio := StrToInt( copy( sFechaStr, 17, 4 ) );
                 sHora := copy( sFechaStr, 8 , 8 );
                 Result := EncodeDate( iAnio, iMes, iDia );
                 Result := ZetaClientTools.AddDateTime( Result, sHora );
                 Break;
            end;
        end;
   end;

   function CopyTempFile( const sFile: String ): boolean;
   var
      sLinea: string;
   begin
        Result := False;
        FAsciiB := TAsciiServer.Create;
        try
           with FAsciiB do
           begin
                Try
                   if ( Open( sFile ) ) then
                   begin
                        repeat
                        sLinea := Data;
                        FAScii.WriteTexto( sLinea );
                        until Not Read;
                        Close;
                        Result := True;
                   end;
                except
                      On Error : Exception Do
                      begin
                           with dmMonitorTress do
                                Monitor.ReportRedAlert( MessageHeader, GetComentario( eError, eInterbaseLog ), ObtieneResultado( Ord( eError ) ), FTest );
                      end;
                End;
           end;
        finally
               FreeAndNil( FasciiB );
        end;
   end;

   function ReplaceFile( const sFile: String ): string;
   var
      sBak: string;
   begin
        if ( FileExists( sFile ) ) then
        begin
             sBak := FDirectoryIB + K_BAK;
             if ( FileExists( sBak ) ) then
             begin
                sTemp := ChangeFileExt( sFile, '.TMP' );
                if ( FileExists( sTemp ) ) then
                begin
                     if ( DeleteFile( sTemp ) ) then
                     begin
                          if ( RenameFile( sFile, sTemp ) ) then
                          begin
                               if not ( FAscii.Used ) then
                                  FAscii.Init( sBak );
                               Result := sTemp;
                          end
                          else
                              Result := sFile;
                     end
                     else
                         Result := sFile;
                end
                else
                begin
                     if ( RenameFile( sFile, sTemp ) ) then
                     begin
                          if not ( FAscii.Used ) then
                             FAscii.Init( sBak );
                          Result := sTemp;
                     end
                     else
                         Result := sFile;
                end;
             end
             else
             begin
                  sTemp := ChangeFileExt( sFile, '.BKP' );
                  if ( RenameFile( sFile, sTemp ) ) then
                  begin
                       if not ( FAscii.Used ) then
                          FAscii.Init( sBak );
                       Result := sTemp;
                  end
                  else
                      Result := sFile;
             end;
        end;
   end;
{$endif}

begin
{$ifdef INTERBASE}
     if Favail then
     begin
          lEncontrado := FALSE;
          Monitor.CallMeBack( 'Checking Interbase Database Integrity' );
          FTest := Ord( eInterbaseLog );
          FAsciiIB := TAsciiServer.Create;
          try
             with FAsciiIB do
             begin
                  sFile := ZetaWinAPITools.FormatDirectory( FDirectoryIB ) + K_LOG;
                  with dmMonitorTress do
                  begin
                       if FileExists( sFile ) then
                       begin
                            try
                               try
                                  if ( Open( sFile ) ) then
                                  begin
                                       repeat
                                       for i := K_I to K_S do
                                       begin
                                            aLineas[ i ] := Data;
                                            if ( aLineas[ i ] = VACIO ) then
                                            begin
                                                 dFecha := GetFecha( aLineas[ 0 ] );
                                                 if ( dFecha > FStart ) then //FStart
                                                 begin
                                                      for iCont := 1 to i do
                                                      begin
                                                           sTemp := aLineas[ iCont ];
                                                           if ( Pos( K_MENSAJE, LowerCase( sTemp ) ) <> 0 ) then
                                                           begin
                                                                Monitor.ReportRedAlert( MessageHeader, GetComentario( eError, eInterbaseLog ), ObtieneResultado( Ord( eError ) ), FTest );
                                                                lEncontrado := TRUE;
                                                           end;
                                                      end;
                                                      Break;
                                                 end
                                                 else
                                                     Break;
                                            end;
                                            Read;
                                       end;
                                       Until Not Read;
                                  end;
                               Except
                                     on Error : Exception do
                                        Monitor.ReportRedAlert( MessageHeader, GetComentario( eError, eInterbaseLog ), ObtieneResultado( Ord( eError ) ), FTest );
                               end;
                            finally
                                   FreeAndNil( FAsciiIB );
                            end;
                            sFile := ReplaceFile( sFile );
                            if Not ( ExtractFileExt( sFile ) = '.BKP' ) then
                            begin
                                 CopyTempFile( sFile );
                                 if Not ( DeleteFile( sFile ) ) then
                                    Monitor.ReportRedAlert( MessageHeader, GetComentario( eError,eInterbaseLog ), ObtieneResultado( Ord( eError ) ), FTest )
                                 else if not( lEncontrado ) then
                                         Monitor.ReportGreenAlert( MessageHeader, GetComentario( eOk, eInterbaseLog ), ObtieneResultado( Ord( eOk ) ), FTest );
                            end
                            else if not ( lEncontrado ) then
                                    Monitor.ReportGreenAlert( MessageHeader, GetComentario( eError, eInterbaseLog ), ObtieneResultado( Ord( eError ) ), FTest );
                       end
                       else
                           Monitor.ReportGreenAlert( MessageHeader, GetComentario( eError, eInterbaseLog ), ObtieneResultado( Ord( eError ) ), FTest );
                  end;
             end;
          finally
                 FAscii.Close;
          end;
     end;
{$endif}
end;

destructor TCheckIBLog.Destroy;
begin
     inherited;
     {$ifdef INTERBASE}
     FreeAndNil( FAscii );
     {$endif}
end;

{ ********* TMonitor *********** }

constructor TMonitor.Create;
begin
     FRegistry := TMonitorRegistry.Create;
     FOutput := TStringList.Create;
     FMensajeEmail := TStringList.Create;
     FChecks := TList.Create;
     Init;
end;

destructor TMonitor.Destroy;
begin
     Clear;
     FreeAndNil( FChecks );
     FreeAndNil( FOutput );
     FreeAndNil( FMensajeEmail );
     FreeAndNil( FRegistry );
     inherited Destroy;
end;

function TMonitor.WaitInterval: Integer;
const
     K_UN_SEGUNDO = 1000;
begin
     Result := FRegistry.WaitInterval * K_UN_SEGUNDO;
end;

function TMonitor.Start: Boolean;
var
   i: Integer;
begin
     for i := 0 to ( Self.Count - 1 ) do
     begin
          Checks[ i ].Start;
     end;
     Result := True;
end;

function TMonitor.Stop: Boolean;
var
   i: Integer;
begin
     with FOutput do
     begin
          BeginUpdate;
          try
             Clear;
             for i := 0 to ( Self.Count - 1 ) do
             begin
                  Checks[ i ].Stop;
             end;
             ReportRedAlert( 'TRESS_MONITOR', 'Tress Monitor Service', dmMonitorTress.ObtieneResultado( Ord( eError ) ), -1 );
          finally
                 EndUpdate;
          end;
     end;
     EscribeLog;
     Result := TRUE;
end;

procedure TMonitor.Continue;
var
   i: Integer;
begin
     for i := 0 to ( Self.Count - 1 ) do
     begin
          Checks[ i ].Continue;
     end;
     lFecha := TRUE;
end;

procedure TMonitor.Pause;
begin
end;

procedure TMonitor.Shutdown;
var
   i: Integer;
begin
     with FOutput do
     begin
          BeginUpdate;
          try
             Clear;
             for i := 0 to ( Self.Count - 1 ) do
             begin
                  Checks[ i ].ShutDown;
             end;
             ReportRedAlert( 'TRESS_MONITOR', 'Tress Monitor Service', dmMonitorTress.ObtieneResultado( Ord( eError ) ), -1 );
          finally
                 EndUpdate;
          end;
     end;
     EscribeLog;
end;

procedure TMonitor.Add( oCheck: TCheck );
begin
     FChecks.Add( oCheck );
end;

procedure TMonitor.Delete(const Index: Integer);
begin
     Checks[ Index ].Free;
     FChecks.Delete( Index );
end;

procedure TMonitor.Clear;
var
   i: Integer;
begin
     for i := ( Count - 1 ) downto 0 do
     begin
          Delete( i );
     end;
end;

function TMonitor.GetChecks(Index: Integer): TCheck;
begin
     Result := TCheck( FChecks[ Index ] );
end;

function TMonitor.GetCount: Integer;
begin
     Result := FChecks.Count;
end;

procedure TMonitor.CallMeBack(const sMensaje: String);
begin
     if Assigned( FCallBack ) then
        FCallBack( sMensaje );
end;

procedure TMonitor.Init;
begin
     TCheckHardDiskSpace.Create( Self );
     {$ifdef INTERBASE}
     TCheckIBServices.Create( Self );
     {$endif}
     TCheckDataBasesDaily.Create( Self );
     TCheckDataBasesHourly.Create( Self );
     TCheckSentinel.Create( Self );
     TCheckODBCPooling.Create( Self );
     TCheckODBCTracing.Create( Self );
     {$ifdef INTERBASE}
     TCheckGBK.Create( Self );
     TCheckIBLog.Create( Self );
     {$endif}
end;

function GetTimeStamp: TDateTime;
begin
     //Result := ZetaWinApiTools.GetGMTDateTime;
     Result := Now;
end;

procedure TMonitor.ReportAlert( const sMessage, sDescription, sFlag: string; const iPrueba: integer );
const
     K_MITAD = 128;
     K_MAX = 255;
     K_EXCLUIR = 'TRESS_MONITOR';
     K_LISTA_IB = 'N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,S,S';
     K_LISTA_MSSQL = 'N,N,N,N,N,N,N,S,S';

var
   sLista, sTest: string;
   FAutoServer: TAutoServer;

   procedure LlenaResultados( Lista: TStrings );
   var
      sOk, sAdvertencia, sProblema: string;

   begin
        with Lista do
        begin
             BeginUpdate;
             try
                with FRegistry do
                begin
                     with dmMonitorTress do
                     begin
                          if ( Lista = FMensajeEmail ) then
                          begin
                               sOk := ObtieneResultado( Ord( eOk ) );
                               sAdvertencia := ObtieneResultado( Ord( eWarning ) );
                               sProblema := ObtieneResultado( Ord( eError ) );
                               if ( ( Ok and ( sFlag = sOk ) ) or ( Advertencia and ( sFlag = sAdvertencia ) ) or ( Problema and ( sFlag = sProblema ) ) ) then
                                    Add( Format( ObtieneEtiquetas, [ Copy( sMessage, 1, K_MITAD ),
                                                                                    FormatDateTime( 'dd/mmm/yyyy', GetTimeStamp ),
                                                                                    FormatDateTime( 'hh:nn:ss AM/PM', GetTimeStamp ),
                                                                                    sFlag,
                                                                                    Copy( sDescription, 1, K_MAX ) ] ) );
                          end
                          else
                          begin
                               Add( Format( ObtieneEtiquetas, [ Copy( sMessage, 1, K_MITAD ),
                                                                               FormatDateTime( 'dd/mmm/yyyy', GetTimeStamp ),
                                                                               FormatDateTime( 'hh:nn:ss AM/PM', GetTimeStamp ),
                                                                               sFlag,
                                                                               Copy( sDescription, 1, K_MAX ) ] ) );
                          end;
                     end;
                end;
             finally
                    EndUpdate;
             end;
        end;
   end;

begin
     if ( ChecaDemo ) then
     begin
          {$ifdef INTERBASE}
          sLista := K_LISTA_IB;
          {$endif}
          {$ifdef MSSQL}
          sLista := K_LISTA_MSSQL;
          {$endif}
     end
     else
     begin
          FAutoServer := TAutoServer.Create;
          try
             with FAutoServer do
             begin
                  Cargar;
                  if OkModulo( okMonitor ) then
                     {$ifdef INTERBASE}
                     sLista := FRegistry.ListaTestsIB
                     {$endif}
                     {$ifdef MSSQL}
                     sLista := FRegistry.ListaTestsMSSQL
                     {$endif}
                  else
                      {$ifdef INTERBASE}
                      sLista := K_LISTA_IB;
                      {$endif}
                      {$ifdef MSSQL}
                      sLista := K_LISTA_MSSQL;
                      {$endif}
             end;
          finally
                 FreeAndNil( FAutoServer );
          end;
     end;
     if ( sMessage <> K_EXCLUIR ) then
     begin
          sLista := ZetaCommonTools.StrTransAll( sLista, ',', VACIO );
          sTest := sLista[ iPrueba + 1 ];
          if zStrToBool( sTest ) then
             LlenaResultados( FMensajeEmail );
          LlenaResultados( FOutput );
     end;
end;

procedure TMonitor.ReportGreenAlert( const sMessage, sDescription, sCodigo: string; iPrueba: integer );
begin
     ReportAlert( sMessage, sDescription, sCodigo, iPrueba );
end;

procedure TMonitor.ReportYellowAlert( const sMessage, sDescription: String; const iPrueba: integer );
begin
     ReportAlert( sMessage, sDescription, dmMonitorTress.ObtieneResultado( Ord( eWarning ) ), iPrueba );
end;

procedure TMonitor.ReportRedAlert( const sMessage, sDescription, sCodigo: string; iPrueba: integer );
begin
     ReportAlert( sMessage, sDescription, sCodigo, iPrueba );
end;

function TMonitor.ChecaDemo: boolean;
var
   FAutoServer: TAutoServer;

begin
     FAutoServer := TAutoServer.Create;
     try
        with FAutoServer do
        begin
             {$ifdef INTERBASE}
                     {$ifdef CORPORATIVA}
                     SQLType := engInterbase;
                     AppType := atCorporativa;
                     {$endif}
             {$endif}
             {$ifdef INTERBASE}
                     {$ifdef DOS_CAPAS}
                     SQLType := engInterbase;
                     AppType := atProfesional;
                     {$endif}
             {$endif}
             {$ifdef MSSQL}
             SQLType := engMSSQL;
             AppType := atCorporativa;
             {$endif}
             Cargar;
             if EsDemo then
                Result := True
             else
                 Result := False;
        end;
     finally
            FreeAndNil( FAutoServer );
     end;
end;

procedure TMonitor.RunChecks;
const
     K_PRUEBA_IB = 4;
     K_PRUEBA_MSSQL = 3;
var
   i: Integer;
   FAutoServer: TAutoServer;

begin
     CallMeBack( VACIO );
     with FOutput do
     begin
          BeginUpdate;
          try
             Clear;
             FMensajeEmail.Clear;
             lFecha := TRUE;
             if ( ChecaDemo ) then
                {$ifdef INTERBASE}
                        Checks[ K_PRUEBA_IB ].RunCheck
                {$endif}
                {$ifdef MSSQL}
                        Checks[ K_PRUEBA_MSSQL ].RunCheck
                {$endif}
             else
             begin
                  FAutoServer := TAutoServer.Create;
                  try
                     with FAutoServer do
                     begin
                          Cargar;
                          if OkModulo( okMonitor ) then
                          begin
                               for i := 0 to ( Self.Count - 1 ) do
                               begin
                                    Checks[ i ].RunCheck;
                               end;
                          end
                          else
                          begin
                               {$ifdef INTERBASE}
                                       Checks[ K_PRUEBA_IB ].RunCheck
                               {$endif}
                               {$ifdef MSSQL}
                                       Checks[ K_PRUEBA_MSSQL ].RunCheck
                               {$endif}
                          end;
                     end;
                  finally
                         FreeAndNil( FAutoServer );
                  end;
             end;
          finally
                 EndUpdate;
                 lFecha := TRUE;
          end;
     end;
     EscribeLog;
     CallMeBack( VACIO );
end;

procedure TMonitor.CreaArchivosSalida;
const
     aEncabezadoInicio: array[ eIdioma ] of PChar = ( '********** Inicio de las Pruebas - %s **********',
                                                      '********** Begin of Tests - %s **********' );

     aEncabezadoFinal: array[ eIdioma ] of PChar = ( '**********   Fin de las Pruebas - %s **********',
                                                      '**********   End of Tests - %s **********' );

var
   TArchivoSalida, TArchivoCopia: TAsciiLog;

begin
     TArchivoSalida := TAsciiLog.Create;
     TArchivoCopia := TAsciiLog.Create;
     try
        with FOutput do
        begin
             with FRegistry do
             begin
                  if CreateOutputFile and FileCtrl.DirectoryExists( OutputDirectory ) then
                  begin
                       if FRegistry.FechaArchivo = Trunc( GetTimeStamp ) then //Date
                       begin
                            TArchivoSalida.Init( Format( '%sTRESS_%s.txt', [ ZetaCommonTools.VerificaDir( OutPutDirectory ), FormatDateTime( 'yyyymmdd', GetTimeStamp ) ] ) );
                            if CopyDirectoryAvail and FileCtrl.DirectoryExists( CopyDirectory ) then
                               TArchivoCopia.Init( Format( '%sTRESS_%s.txt', [ ZetaCommonTools.VerificaDir( CopyDirectory ), FormatDateTime( 'yyyymmdd', GetTimeStamp ) ] ) );
                       end
                       else
                       begin
                            FRegistry.FechaArchivo := Trunc( GetTimeStamp );
                            TArchivoSalida.Init( Format( '%sTRESS_%s.txt', [ ZetaCommonTools.VerificaDir( OutPutDirectory ), FormatDateTime( 'yyyymmdd', GetTimeStamp ) ] ) );
                            if CopyDirectoryAvail and FileCtrl.DirectoryExists( CopyDirectory ) then
                               TArchivoCopia.Init( Format( '%sTRESS_%s.txt', [ ZetaCommonTools.VerificaDir( CopyDirectory ), FormatDateTime( 'yyyymmdd', GetTimeStamp ) ] ) );
                       end;
                       with TArchivoSalida do
                       begin
                            WriteTimeStamp( aEncabezadoInicio[ eIdioma( Idioma ) ] );
                            WriteTexto( Text );
                            WriteTimeStamp( aEncabezadoFinal[ eIdioma( Idioma ) ] );
                       end;
                       if CopyDirectoryAvail then
                       begin
                            with TArchivoCopia do
                            begin
                                 WriteTimeStamp( aEncabezadoInicio[ eIdioma( Idioma ) ] );
                                 WriteTexto( Text );
                                 WriteTimeStamp( aEncabezadoFinal[ eIdioma( Idioma ) ] );
                            end;
                       end;
                       if EmailAvail then
                          dmMonitorTress.SendEmail( FMensajeEmail );
                  end;
             end;
        end;
     finally
            FreeAndNil( TArchivoCopia );
            FreeAndNil( TArchivoSalida );
     end;
end;

procedure TMonitor.EscribeLog;
begin
     with FOutput do
     begin
          if ( Count > 0 ) then
             CreaArchivosSalida;
     end;
end;

end.



