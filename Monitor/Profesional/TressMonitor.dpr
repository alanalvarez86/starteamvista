program TressMonitor;

uses
  SvcMgr,
  DMonitor in '..\DMonitor.pas',
  DMonitorService in '..\DMonitorService.pas' {MonitorService: TService},
  DEmailService in '..\..\DataModules\DEmailService.pas' {dmEmailService: TDataModule};

{$R *.RES}

begin
     Application.Initialize;
     Application.Title := 'Monitor de Tress';
     Application.CreateForm(TMonitorService, MonitorService);
  Application.Run;
end.
