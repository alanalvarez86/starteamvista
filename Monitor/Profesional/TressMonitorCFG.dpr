program TressMonitorCFG;

uses
  Forms,
  FMonitor in '..\FMonitor.pas' {MonitorCFG},
  DMonitorTress in '..\DMonitorTress.pas' {dmMonitorTress: TDataModule},
  DEmailService in '..\..\DataModules\DEmailService.pas' {dmEmailService: TDataModule};

{$R *.RES}
{$R WindowsXP.res}

begin
  Application.Initialize;
  Application.Title := 'Tress Monitor CFG InterBase Profesional';
  Application.CreateForm(TMonitorCFG, MonitorCFG);
  Application.CreateForm(TdmMonitorTress, dmMonitorTress);
  Application.Run;
end.
