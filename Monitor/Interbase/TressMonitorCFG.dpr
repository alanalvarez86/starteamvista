program TressMonitorCFG;

uses
  Forms,
  FMonitor in '..\FMonitor.pas' {MonitorCFG},
  DMonitorTress in '..\DMonitorTress.pas' {dmMonitorTress: TDataModule},
  DEmailService in '..\..\DataModules\DEmailService.pas' {dmEmailService: TDataModule};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Tress Monitor CFG InterBase Corporativo';
  Application.CreateForm(TMonitorCFG, MonitorCFG);
  Application.CreateForm(TdmMonitorTress, dmMonitorTress);
  Application.Run;
end.
