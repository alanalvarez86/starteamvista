object MonitorCFG: TMonitorCFG
  Left = 269
  Top = 241
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Configurador del Monitor de Tress'
  ClientHeight = 323
  ClientWidth = 564
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanelStartStop: TPanel
    Left = 478
    Top = 0
    Width = 86
    Height = 304
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 0
    object Start: TBitBtn
      Left = 7
      Top = 20
      Width = 75
      Height = 73
      Hint = 'Iniciar Monitoreo'
      Anchors = [akTop, akRight]
      Caption = '&Iniciar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = StartClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333FFFFF3333333333000003333333333F777773FF333333008877700
        33333337733FFF773F33330887000777033333733F777FFF73F330880FAFAF07
        703337F37733377FF7F33080F00000F07033373733777337F73F087F00A2200F
        77037F3737333737FF7F080A0A2A220A07037F737F3333737F7F0F0F0AAAA20F
        07037F737F3333737F7F0F0A0FAA2A0A08037F737FF33373737F0F7F00FFA00F
        780373F737FFF737F3733080F00000F0803337F73377733737F330F80FAFAF08
        8033373F773337733733330F8700078803333373FF77733F733333300FFF8800
        3333333773FFFF77333333333000003333333333377777333333}
      Layout = blGlyphTop
      NumGlyphs = 2
    end
    object Stop: TBitBtn
      Left = 7
      Top = 96
      Width = 75
      Height = 73
      Hint = 'Terminar Monitoreo'
      Anchors = [akTop, akRight]
      Caption = '&Terminar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = StopClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333FFFFF3333333333000003333333333F777773FF333333008877700
        33333337733FFF773F33330887000777033333733F777FFF73F330880F9F9F07
        703337F37733377FF7F33080F00000F07033373733777337F73F087F0091100F
        77037F3737333737FF7F08090919110907037F737F3333737F7F0F0F0999910F
        07037F737F3333737F7F0F090F99190908037F737FF33373737F0F7F00FF900F
        780373F737FFF737F3733080F00000F0803337F73377733737F330F80F9F9F08
        8033373F773337733733330F8700078803333373FF77733F733333300FFF8800
        3333333773FFFF77333333333000003333333333377777333333}
      Layout = blGlyphTop
      NumGlyphs = 2
    end
    object Grabar: TBitBtn
      Left = 8
      Top = 248
      Width = 75
      Height = 25
      Hint = 'Grabar Configuraci�n'
      Anchors = [akRight, akBottom]
      Caption = '&Grabar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = GrabarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        555555FFFFFFFFFF5F5557777777777505555777777777757F55555555555555
        055555555555FF5575F555555550055030555555555775F7F7F55555550FB000
        005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
        B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
        305555577F555557F7F5550E0BFBFB003055557575F55577F7F550EEE0BFB0B0
        305557FF575F5757F7F5000EEE0BFBF03055777FF575FFF7F7F50000EEE00000
        30557777FF577777F7F500000E05555BB05577777F75555777F5500000555550
        3055577777555557F7F555000555555999555577755555577755}
      NumGlyphs = 2
    end
    object Salir: TBitBtn
      Left = 8
      Top = 277
      Width = 75
      Height = 25
      Hint = 'Salir Del Programa'
      Anchors = [akRight, akBottom]
      Caption = '&Salir'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      Kind = bkClose
    end
  end
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 478
    Height = 304
    ActivePage = OutputFile
    Align = alClient
    MultiLine = True
    TabOrder = 1
    object OutputFile: TTabSheet
      Caption = '&Archivo de Salida'
      object CreateOutputFile: TCheckBox
        Left = 19
        Top = 17
        Width = 145
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Generar Archivo de Salida:'
        TabOrder = 0
      end
      object gbDirCopy: TGroupBox
        Left = 10
        Top = 96
        Width = 449
        Height = 49
        TabOrder = 1
        object CopyDirectorySeek: TSpeedButton
          Left = 415
          Top = 17
          Width = 23
          Height = 22
          Hint = 'Buscar Directorio de Archivo de Salida'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
            300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
            330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
            333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
            339977FF777777773377000BFB03333333337773FF733333333F333000333333
            3300333777333333337733333333333333003333333333333377333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = CopyDirectorySeekClick
        end
        object CopyDirectory: TEdit
          Left = 11
          Top = 18
          Width = 398
          Height = 21
          TabOrder = 0
        end
      end
      object gbDirOutput: TGroupBox
        Left = 10
        Top = 38
        Width = 449
        Height = 49
        Caption = ' Directorio de Archivo de Salida '
        TabOrder = 2
        object OutputDirectorySeek: TSpeedButton
          Left = 415
          Top = 16
          Width = 23
          Height = 22
          Hint = 'Buscar Directorio de Archivo de Salida'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
            300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
            330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
            333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
            339977FF777777773377000BFB03333333337773FF733333333F333000333333
            3300333777333333337733333333333333003333333333333377333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = OutputDirectorySeekClick
        end
        object OutputDirectory: TEdit
          Left = 11
          Top = 17
          Width = 398
          Height = 21
          TabOrder = 0
        end
      end
      object CopyDirectoryAvail: TCheckBox
        Left = 20
        Top = 92
        Width = 151
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Directorio Copia del  Archivo'
        Checked = True
        State = cbChecked
        TabOrder = 3
        OnClick = CopyDirectoryAvailClick
      end
      object gbResultados: TGroupBox
        Left = 210
        Top = 153
        Width = 161
        Height = 88
        Caption = ' Resultado de las Pruebas '
        TabOrder = 4
        object Ok: TCheckBox
          Left = 52
          Top = 20
          Width = 34
          Height = 17
          Alignment = taLeftJustify
          Caption = 'OK:'
          TabOrder = 0
        end
        object Advertencia: TCheckBox
          Left = 10
          Top = 40
          Width = 76
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Advertencia:'
          TabOrder = 1
        end
        object Problema: TCheckBox
          Left = 23
          Top = 60
          Width = 63
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Problema:'
          TabOrder = 2
        end
      end
      object gbConfEmail: TGroupBox
        Left = 10
        Top = 152
        Width = 194
        Height = 89
        Caption = ' Configuraci�n Correo Electr�nico '
        TabOrder = 5
        object EmailAvail: TCheckBox
          Left = 10
          Top = 20
          Width = 153
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Env�o de Correo Electr�nico:'
          TabOrder = 0
        end
        object Idioma: TCheckBox
          Left = 58
          Top = 40
          Width = 105
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Pruebas en Ingles:'
          TabOrder = 1
        end
        object HTML: TCheckBox
          Left = 72
          Top = 60
          Width = 91
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Formato HTML:'
          Enabled = False
          TabOrder = 2
        end
      end
    end
    object HardDisk: TTabSheet
      Caption = '&Discos Duros'
      ImageIndex = 1
      object gbDiscos: TGroupBox
        Left = 0
        Top = 0
        Width = 470
        Height = 113
        Align = alTop
        TabOrder = 0
        object HardDiskFrequencyLBL: TLabel
          Left = 32
          Top = 21
          Width = 121
          Height = 13
          Alignment = taRightJustify
          Caption = 'Frecuencia de Monitoreo:'
        end
        object HardDiskHorasLBL: TLabel
          Left = 196
          Top = 21
          Width = 28
          Height = 13
          Caption = 'Horas'
        end
        object Porcentaje2: TLabel
          Left = 188
          Top = 42
          Width = 8
          Height = 13
          Caption = '%'
        end
        object HardDiskYellowLBL: TLabel
          Left = 9
          Top = 42
          Width = 144
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nivel de Utilizaci�n Aceptable:'
          FocusControl = HardDiskYellow
        end
        object HardDiskRedLBL: TLabel
          Left = 13
          Top = 63
          Width = 140
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nivel de Utilizaci�n Problema:'
          FocusControl = HardDiskRed
        end
        object Porcentaje1: TLabel
          Left = 188
          Top = 63
          Width = 8
          Height = 13
          Caption = '%'
        end
        object HardDisksLBL: TLabel
          Left = 56
          Top = 84
          Width = 97
          Height = 13
          Alignment = taRightJustify
          Caption = 'Discos a Monitorear:'
        end
        object HardDiskFrequency: TZetaHora
          Left = 154
          Top = 17
          Width = 40
          Height = 21
          EditMask = '99:99;0'
          TabOrder = 0
          Tope = 48
        end
        object HardDiskYellow: TZetaNumero
          Left = 154
          Top = 38
          Width = 31
          Height = 21
          Hint = 'Arriba De Este Nivel Se Reporta Amarillo Para Un Disco Duro'
          Mascara = mnDias
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Text = '0'
        end
        object HardDiskRed: TZetaNumero
          Left = 154
          Top = 59
          Width = 31
          Height = 21
          Hint = 'Arriba De Este Nivel Se Reporta Rojo Para Un Disco Duro'
          Mascara = mnDias
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Text = '0'
        end
        object HardDisks: TZetaEdit
          Left = 154
          Top = 80
          Width = 71
          Height = 21
          TabOrder = 3
        end
      end
      object HardDisksAvail: TCheckBox
        Left = 8
        Top = -2
        Width = 84
        Height = 17
        Alignment = taLeftJustify
        BiDiMode = bdLeftToRight
        Caption = 'Discos Duros'
        Checked = True
        ParentBiDiMode = False
        State = cbChecked
        TabOrder = 1
        OnClick = HardDisksAvailClick
      end
    end
    object DB: TTabSheet
      Caption = '&TRESS'
      ImageIndex = 3
      object gbConfiguracion: TGroupBox
        Left = 0
        Top = 0
        Width = 470
        Height = 81
        Align = alTop
        TabOrder = 0
        object DBConfigurationLBL: TLabel
          Left = 57
          Top = 22
          Width = 121
          Height = 13
          Caption = 'Frecuencia de Monitoreo:'
        end
        object DBHorasLBL: TLabel
          Left = 221
          Top = 22
          Width = 28
          Height = 13
          Caption = 'Horas'
        end
        object PollCompanysLBL: TLabel
          Left = 32
          Top = 44
          Width = 146
          Height = 13
          Alignment = taRightJustify
          Caption = 'Empresas con Poll de Tarjetas:'
        end
        object DBConfigurationFrequency: TZetaHora
          Left = 179
          Top = 18
          Width = 40
          Height = 21
          EditMask = '99:99;0'
          TabOrder = 0
          Tope = 48
        end
        object PollCompanys: TZetaEdit
          Left = 179
          Top = 40
          Width = 238
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 1
        end
      end
      object gbProcesos: TGroupBox
        Left = 0
        Top = 81
        Width = 470
        Height = 177
        Align = alClient
        TabOrder = 1
        object FrecProcesos: TLabel
          Left = 57
          Top = 19
          Width = 121
          Height = 13
          Caption = 'Frecuencia de Monitoreo:'
        end
        object lblDBHorasProcesos: TLabel
          Left = 221
          Top = 19
          Width = 28
          Height = 13
          Caption = 'Horas'
        end
        object Tejecucion: TLabel
          Left = 75
          Top = 40
          Width = 103
          Height = 13
          Caption = 'Tiempo de Ejecuci�n:'
        end
        object LimEmpleados: TLabel
          Left = 69
          Top = 82
          Width = 108
          Height = 13
          Caption = 'Empleados por Minuto:'
        end
        object lblDBMinutos: TLabel
          Left = 221
          Top = 40
          Width = 37
          Height = 13
          Caption = 'Minutos'
        end
        object lblProcessesMinimo: TLabel
          Left = 11
          Top = 61
          Width = 167
          Height = 13
          Caption = 'M�nimo de Empleados Procesados:'
        end
        object lblLapso: TLabel
          Left = 84
          Top = 103
          Width = 94
          Height = 13
          Alignment = taRightJustify
          Caption = 'Rango de Revisi�n:'
        end
        object lblHoras: TLabel
          Left = 222
          Top = 103
          Width = 28
          Height = 13
          Caption = 'Horas'
        end
        object DBProcessesFrequency: TZetaHora
          Left = 179
          Top = 15
          Width = 40
          Height = 21
          EditMask = '99:99;0'
          TabOrder = 0
          Tope = 48
        end
        object EmployeLimit: TZetaNumero
          Left = 179
          Top = 78
          Width = 40
          Height = 21
          Hint = 'L�mite de Empleados'
          Mascara = mnDias
          TabOrder = 3
          Text = '0'
        end
        object EjecutionTime: TZetaNumero
          Left = 179
          Top = 36
          Width = 40
          Height = 21
          Hint = 'Tiempo de Ejecuci�n'
          Mascara = mnDias
          TabOrder = 1
          Text = '0'
        end
        object DBProcessesEmpleados: TZetaNumero
          Left = 179
          Top = 57
          Width = 40
          Height = 21
          Hint = 'L�mite de Empleados'
          Mascara = mnDias
          TabOrder = 2
          Text = '0'
        end
        object PollHoras: TZetaHora
          Left = 179
          Top = 99
          Width = 40
          Height = 21
          EditMask = '99:99;0'
          TabOrder = 4
          Tope = 48
        end
      end
      object PollAvail: TCheckBox
        Left = 3
        Top = -2
        Width = 179
        Height = 17
        Alignment = taLeftJustify
        BiDiMode = bdRightToLeft
        Caption = ' Configuraci�n de Bases de Datos'
        Checked = True
        ParentBiDiMode = False
        State = cbChecked
        TabOrder = 2
        OnClick = PollAvailClick
      end
      object ProcessAvail: TCheckBox
        Left = 3
        Top = 80
        Width = 123
        Height = 17
        Alignment = taLeftJustify
        Caption = ' Tiempos de Procesos '
        Checked = True
        State = cbChecked
        TabOrder = 3
        OnClick = ProcessAvailClick
      end
    end
    object Interbase: TTabSheet
      Caption = 'I&nterBase'
      ImageIndex = 4
      object gpInterbaseServicios: TGroupBox
        Left = 8
        Top = 8
        Width = 222
        Height = 57
        TabOrder = 0
        object lblFrecInterbaseServicios: TLabel
          Left = 11
          Top = 24
          Width = 121
          Height = 13
          Caption = 'Frecuencia de Monitoreo:'
        end
        object lblInterbaseHoras: TLabel
          Left = 176
          Top = 24
          Width = 28
          Height = 13
          Caption = 'Horas'
        end
        object InterbaseServicesFrequency: TZetaHora
          Left = 133
          Top = 20
          Width = 40
          Height = 21
          EditMask = '99:99;0'
          TabOrder = 0
          Tope = 48
        end
      end
      object gbRespaldo: TGroupBox
        Left = 8
        Top = 72
        Width = 451
        Height = 97
        TabOrder = 1
        object lblFrecGBKFiles: TLabel
          Left = 9
          Top = 25
          Width = 121
          Height = 13
          Caption = 'Frecuencia de Monitoreo:'
        end
        object lblGBKFilesHoras: TLabel
          Left = 173
          Top = 25
          Width = 28
          Height = 13
          Caption = 'Horas'
        end
        object lblGBKDirectory: TLabel
          Left = 13
          Top = 69
          Width = 117
          Height = 13
          Caption = 'Directorio Archivos GBK:'
        end
        object tDirectoryGBK: TSpeedButton
          Left = 422
          Top = 64
          Width = 23
          Height = 22
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            777777777777777770000070000077777000E00BFBFB07777777E0BFBF000777
            7700E0FBFBFBF0777700E0BFBF0000007777E0FBFBFBFBFB0799E0BF00000000
            7799000BFB077777777777700077777777007777777777777700777777777777
            7777777777777777700077777777777770007777777777777777}
          OnClick = tDirectoryGBkClick
        end
        object lblHorasGBK: TLabel
          Left = 36
          Top = 47
          Width = 94
          Height = 13
          Alignment = taRightJustify
          Caption = 'Rango de Revisi�n:'
        end
        object lblHoras_GBK: TLabel
          Left = 173
          Top = 47
          Width = 28
          Height = 13
          Caption = 'Horas'
        end
        object InterbaseGBKFrequency: TZetaHora
          Left = 131
          Top = 21
          Width = 40
          Height = 21
          EditMask = '99:99;0'
          TabOrder = 0
          Tope = 48
        end
        object GbkDirectory: TEdit
          Left = 131
          Top = 65
          Width = 288
          Height = 21
          TabOrder = 2
        end
        object InterbaseGBKHoras: TZetaHora
          Left = 131
          Top = 43
          Width = 40
          Height = 21
          EditMask = '99:99;0'
          TabOrder = 1
          Tope = 48
        end
      end
      object gbInterbaseLog: TGroupBox
        Left = 239
        Top = 8
        Width = 219
        Height = 58
        TabOrder = 2
        object lblInterbaseLog: TLabel
          Left = 11
          Top = 24
          Width = 121
          Height = 13
          Caption = 'Frecuencia de Monitoreo:'
        end
        object lblInterbaseLogHoras: TLabel
          Left = 176
          Top = 24
          Width = 28
          Height = 13
          Caption = 'Horas'
        end
        object InterbaseLogFrequency: TZetaHora
          Left = 133
          Top = 20
          Width = 40
          Height = 21
          EditMask = '99:99;0'
          TabOrder = 0
          Tope = 48
        end
      end
      object ServicesAvail: TCheckBox
        Left = 17
        Top = 6
        Width = 60
        Height = 17
        Alignment = taLeftJustify
        BiDiMode = bdLeftToRight
        Caption = 'Servicios'
        Checked = True
        ParentBiDiMode = False
        State = cbChecked
        TabOrder = 3
        OnClick = ServicesAvailClick
      end
      object InterbaseLogAvail: TCheckBox
        Left = 246
        Top = 6
        Width = 86
        Height = 17
        Alignment = taLeftJustify
        Caption = ' InterBase.Log '
        Checked = True
        State = cbChecked
        TabOrder = 4
        OnClick = InterbaseLogAvailClick
      end
      object ArchivosGBKAvail: TCheckBox
        Left = 13
        Top = 70
        Width = 87
        Height = 17
        Alignment = taLeftJustify
        Caption = ' Archivos GBK '
        Checked = True
        State = cbChecked
        TabOrder = 5
        OnClick = ArchivosGBKAvailClick
      end
    end
    object License: TTabSheet
      Caption = '&Licencia'
      ImageIndex = 5
      object gbLicenseSentinel: TGroupBox
        Left = 8
        Top = 16
        Width = 217
        Height = 89
        TabOrder = 0
        object lblFrecLicenseSentinel: TLabel
          Left = 11
          Top = 24
          Width = 121
          Height = 13
          Caption = 'Frecuencia de Monitoreo:'
        end
        object lblLicenseSentinelHoras: TLabel
          Left = 174
          Top = 24
          Width = 28
          Height = 13
          Caption = 'Horas'
        end
        object LicenseSetinelFrequency: TZetaHora
          Left = 133
          Top = 20
          Width = 40
          Height = 21
          EditMask = '99:99;0'
          TabOrder = 0
          Tope = 48
        end
      end
      object gbLicenseUse: TGroupBox
        Left = 232
        Top = 16
        Width = 225
        Height = 89
        TabOrder = 1
        object lblLicenseUseHoras: TLabel
          Left = 188
          Top = 23
          Width = 28
          Height = 13
          Caption = 'Horas'
        end
        object lblFrecLicenseUso: TLabel
          Left = 25
          Top = 23
          Width = 121
          Height = 13
          Caption = 'Frecuencia de Monitoreo:'
        end
        object lblLicenseDiasUso: TLabel
          Left = 12
          Top = 44
          Width = 134
          Height = 13
          Caption = 'D�as Antes del Vencimiento:'
        end
        object lblLicenseDias: TLabel
          Left = 188
          Top = 44
          Width = 23
          Height = 13
          Caption = 'D�as'
        end
        object LicenseUseFrequency: TZetaHora
          Left = 147
          Top = 19
          Width = 40
          Height = 21
          EditMask = '99:99;0'
          TabOrder = 0
          Tope = 48
        end
        object LicenseUseDaysLimit: TZetaNumero
          Left = 147
          Top = 40
          Width = 40
          Height = 21
          Hint = 'Tiempo de Ejecuci�n'
          Mascara = mnDias
          TabOrder = 1
          Text = '0'
        end
      end
      object gbODBC: TGroupBox
        Left = 8
        Top = 108
        Width = 217
        Height = 57
        TabOrder = 2
        object lblFrecODBC: TLabel
          Left = 13
          Top = 24
          Width = 121
          Height = 13
          Caption = 'Frecuencia de Monitoreo:'
        end
        object lblODBCHoras: TLabel
          Left = 177
          Top = 24
          Width = 28
          Height = 13
          Caption = 'Horas'
        end
        object ODBCFrequency: TZetaHora
          Left = 135
          Top = 20
          Width = 40
          Height = 21
          EditMask = '99:99;0'
          TabOrder = 0
          Tope = 48
        end
      end
      object SentinelSuperProAvail: TCheckBox
        Left = 14
        Top = 14
        Width = 105
        Height = 17
        Alignment = taLeftJustify
        Caption = ' Sentinel SuperPro '
        Checked = True
        State = cbChecked
        TabOrder = 3
        OnClick = SentinelSuperProAvailClick
      end
      object LicenciaUsoAvail: TCheckBox
        Left = 239
        Top = 14
        Width = 97
        Height = 17
        Alignment = taLeftJustify
        Caption = ' Licencia de Uso '
        Checked = True
        State = cbChecked
        TabOrder = 4
        OnClick = LicenciaUsoAvailClick
      end
      object ODBCAvail: TCheckBox
        Left = 19
        Top = 106
        Width = 48
        Height = 17
        Alignment = taLeftJustify
        Caption = 'ODBC'
        Checked = True
        State = cbChecked
        TabOrder = 5
        OnClick = ODBCAvailClick
      end
    end
    object Subscripcion: TTabSheet
      Caption = 'Subscri&pci�n de Pruebas'
      ImageIndex = 5
      object gbEmails: TGroupBox
        Left = 0
        Top = 0
        Width = 470
        Height = 258
        Align = alClient
        Caption = ' Pruebas Disponibles '
        TabOrder = 0
        object PrendeTodos: TSpeedButton
          Left = 443
          Top = 16
          Width = 23
          Height = 22
          Hint = 'Subscribirse a Todas las Pruebas'
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
            333B3BB3888BB8888BB333B0000000000B333330FFFFFFFF08333330FFFFFFFF
            08333330FFFFFFFF08333330FFFFFFFF0833BBB0FFFFFFFF0BB33BB0FFFFFFFF
            0BBB3330FFFF000003333330FFFF0FF033333330FFFF0F0B33333330FFFF003B
            B33333B000000333BB333BB3333BB3333BB3B333333B3333333B}
          ParentShowHint = False
          ShowHint = True
          OnClick = PrendeTodosClick
        end
        object ApagaTodos: TSpeedButton
          Left = 443
          Top = 40
          Width = 23
          Height = 22
          Hint = 'No Subscribirse a Ninguna Prueba'
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            55558888888585599958555555555550305555555550055BB0555555550FB000
            0055555550FB0BF0F05555550FBFBF0FB05555550BFBF0FB005555500FBFBFB0
            B055550E0BFBFB00B05550EEE0BFB0B0B055000EEE0BFBF0B0550000EEE00000
            B05500000E055550705550000055555505555500055555550555}
          ParentShowHint = False
          ShowHint = True
          OnClick = ApagaTodosClick
        end
        object PrendeTodas: TSpeedButton
          Left = 443
          Top = 91
          Width = 23
          Height = 24
          Hint = 'Prende Todas Las Pruebas'
          Glyph.Data = {
            06020000424D0602000000000000760000002800000028000000140000000100
            0400000000009001000000000000000000001000000000000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            3333333333333333333333333333333333333333333333333333333333333333
            33333333377777777773333333333FFFFFFFFFF3333333330000000000733333
            33337777777777F3333333330AAAAAAAA073333333337F33333F37F333333333
            0AAAA00AA073333333337F33377FF7F3333333330AAA0000A073333333337F33
            7777F7F3333333330AA70000A073333333337F337777F7F3333333330A700000
            0073333333337F37777777F3333333330A000A000073333333337F77737777F3
            333333330A00AAA00073333333337F77333777F3333333330AAAAA0000733333
            33337F33337777FF333333330AAAAA0A0003333333337F33337F777F33333333
            0AAAAA003007333333337FFFFF77377F33333333000000033700333333337777
            77733377F333333333333333330073333333333333333377FF33333333333333
            3330033333333333333333377F33333333333333333303333333333333333333
            7333333333333333333333333333333333333333333333333333333333333333
            33333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = PrendeTodasClick
        end
        object ApagaTodas: TSpeedButton
          Left = 443
          Top = 117
          Width = 23
          Height = 24
          Hint = 'Apaga Todas Las Pruebas'
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777777777177777777777777117777717777777711177717777777777117
            7117000000000001157707777777775117770700700707151177077777777117
            7115044444451444475174FFFFF15FFF477774F00F00F00F477774FFFFFFFFFF
            4777744444444444477774744744744747777444444444444777}
          ParentShowHint = False
          ShowHint = True
          OnClick = ApagaTodasClick
        end
        object PruebasDisponibles: TCheckListBox
          Left = 2
          Top = 15
          Width = 439
          Height = 241
          ItemHeight = 13
          TabOrder = 0
        end
      end
    end
    object ListaCorreos: TTabSheet
      Caption = 'Lis&ta de Correos'
      ImageIndex = 6
      object gbCorreosElectronicos: TGroupBox
        Left = 0
        Top = 53
        Width = 471
        Height = 205
        Caption = ' Lista de Correos Electr�nicos '
        TabOrder = 0
        object BorrarTodos: TSpeedButton
          Left = 444
          Top = 16
          Width = 23
          Height = 22
          Hint = 'Borrar Todos los Correos Electr�nicos'
          Glyph.Data = {
            42010000424D4201000000000000760000002800000011000000110000000100
            040000000000CC00000000000000000000001000000010000000000000000000
            BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777700000007777770000000000700000007777770FFFFFFFF0700000007777
            770FFFFFFFF0700000007777770FFFFFFFF07000000088888800000000007000
            00008FFFFF0FCCCCCCC0700000008FFFFF0000000000700000008FFFFFFFF877
            7777700000008888888888888887700000008F88888888FFFF87700000008888
            888888FFFF8770000000777778FFFFFFFF877000000077777888888888877000
            0000777778F88888888770000000777778888888888770000000777777777777
            777770000000}
          ParentShowHint = False
          ShowHint = True
          OnClick = BorrarTodosClick
        end
        object BorraUno: TSpeedButton
          Left = 444
          Top = 40
          Width = 23
          Height = 22
          Hint = 'Borra Correo Seleccionado'
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            55558888888585599958555555555550305555555550055BB0555555550FB000
            0055555550FB0BF0F05555550FBFBF0FB05555550BFBF0FB005555500FBFBFB0
            B055550E0BFBFB00B05550EEE0BFB0B0B055000EEE0BFBF0B0550000EEE00000
            B05500000E055550705550000055555505555500055555550555}
          ParentShowHint = False
          ShowHint = True
          OnClick = BorraUnoClick
        end
        object ListaEmails: TListBox
          Left = 2
          Top = 15
          Width = 439
          Height = 188
          Align = alLeft
          ItemHeight = 13
          TabOrder = 0
        end
      end
      object gbDireccionCorreo: TGroupBox
        Left = 0
        Top = 2
        Width = 470
        Height = 49
        Caption = 'Correo Electr�nico'
        TabOrder = 1
        object Agregar: TSpeedButton
          Left = 443
          Top = 16
          Width = 23
          Height = 22
          Hint = 'Agregar Direcci�n de Correo'
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
            333B3BB3888BB8888BB333B0000000000B333330FFFFFFFF08333330FFFFFFFF
            08333330FFFFFFFF08333330FFFFFFFF0833BBB0FFFFFFFF0BB33BB0FFFFFFFF
            0BBB3330FFFF000003333330FFFF0FF033333330FFFF0F0B33333330FFFF003B
            B33333B000000333BB333BB3333BB3333BB3B333333B3333333B}
          ParentShowHint = False
          ShowHint = True
          OnClick = AgregarClick
        end
        object edCorreo: TZetaEdit
          Left = 8
          Top = 17
          Width = 432
          Height = 21
          TabOrder = 0
          OnKeyPress = edCorreoKeyPress
        end
      end
    end
    object ConfEmails: TTabSheet
      Caption = 'Con&figuraci�n Servidor de Correos'
      ImageIndex = 7
      object lblHost: TLabel
        Left = 38
        Top = 17
        Width = 90
        Height = 13
        Alignment = taRightJustify
        Caption = 'Servidor de SMTP:'
      end
      object lblUser: TLabel
        Left = 89
        Top = 61
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Usuario:'
      end
      object lblDirRemitente: TLabel
        Left = 12
        Top = 83
        Width = 116
        Height = 13
        Alignment = taRightJustify
        Caption = 'Direcci�n del Remitente:'
      end
      object lblPuerto: TLabel
        Left = 94
        Top = 39
        Width = 34
        Height = 13
        Alignment = taRightJustify
        Caption = 'Puerto:'
      end
      object EmailSender: TSpeedButton
        Left = 130
        Top = 105
        Width = 199
        Height = 30
        Hint = 'Enviar Prueba de Correo Electr�nico'
        Caption = 'Prueba de Correo Eletr�nico'
        Glyph.Data = {
          96010000424D9601000000000000760000002800000020000000120000000100
          04000000000020010000130B0000130B00001000000010000000FFFFFF000000
          FF00C0C0C0008080800000008000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000222233333333
          3333333333333333332222223333333333333333333333333322222233555555
          5555555555555555555522223350000000000000000000000005222233500000
          0000000000000000000522223350000000000330000000000005333333333330
          0033330033300000000555555555550000000000000000000005222233500000
          0033333333300000000522333333300000000000000000000005225555555500
          0033333333300000000522223350000000000000000000000005233333333333
          0333000000000000000525555555555550555000000404041405222233500000
          0000000000000001310533333350333300000000000404041405555555500000
          0000000000000000000522222255555555555555555555555555}
        ParentShowHint = False
        ShowHint = True
        OnClick = EmailSenderClick
      end
      object Host: TEdit
        Left = 130
        Top = 13
        Width = 200
        Height = 21
        TabOrder = 0
      end
      object UserID: TEdit
        Left = 130
        Top = 57
        Width = 97
        Height = 21
        TabOrder = 1
      end
      object Remitente: TEdit
        Left = 130
        Top = 79
        Width = 200
        Height = 21
        TabOrder = 2
      end
      object Port: TZetaNumero
        Left = 130
        Top = 35
        Width = 31
        Height = 21
        Hint = 'Arriba De Este Nivel Se Reporta Amarillo Para Un Disco Duro'
        Mascara = mnDias
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        Text = '0'
      end
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 304
    Width = 564
    Height = 19
    Panels = <
      item
        Width = 125
      end
      item
        Width = 75
      end
      item
        Width = 100
      end>
    SimplePanel = False
  end
end
