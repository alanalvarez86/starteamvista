<%@ Language=VBScript %>
<%option explicit%>
<!-- #include file="Rolodex/Tools.asp" -->
<%
	Response.Expires=0
	Response.Buffer=false

     'Declaramos constantes
     const K_RESULTADO  = 0
     const K_USERNAME   = 1
     const K_CLAVE      = 2
     const K_US_CODIGO  = 3
     const K_PAGINA     = 4
     const K_NOMBRE     = 5
     const K_CB_CODIGO  = 6
     const K_CM_EMPRESA = 7

     const lrOK              = "4"
     const lrChangePassword  = "5"
     const lrExpiredPassword = "7"

	'Declaramos Variables para Iniciar el COmponente y recibir el XML generado
	dim PortalCOM
     dim ParametrosXML
     dim NodoResultado
   	dim strLogin
	dim xmlLogin
     dim xslLogin

	dim UserName
	dim Clave
	dim Encriptado
     dim Accion
     dim iResultado

     Accion = Request("btnAccion")
     'Determina el Usuario y Password a Utilizar
     if ( Accion = "Entrar" ) then                                     ' Se Presiona el Bot�n de Entrar
        UserName = Request("Usuario")
        Clave =  Request("Clave")
        Encriptado = "N"
     else if ( Accion = "Salir" ) then                                 ' Se Presiona el Bot�n de Salir
             UserName = Application("UserName")
             Clave = Application("Clave")
          else
             UserName = Session("UserName")
             Clave = Session("Clave")
          end if
          Encriptado = "S"
     end if

     'Documento XML para enviar Parametros
     CreaDocumentoXML ParametrosXML, true

     AgregaParametro ParametrosXML, "ACCION", Accion
     AgregaParametro ParametrosXML, "US_CORTO", UserName
     AgregaParametro ParametrosXML, "US_PASSWRD", Clave
     AgregaParametro ParametrosXML, "ENCRIPTADO", Encriptado
     if ( Accion = "Registrar" ) then                                            ' Se Presiona el Bot�n de Cambiar Clave
        AgregaParametro ParametrosXML, "USUARIO", Request("hdUsuario")           ' Campo Hidden
        AgregaParametro ParametrosXML, "CLAVEACTUAL", Request("hdClaveActual")   ' Campo Hidden
        AgregaParametro ParametrosXML, "CLAVENUEVA", Request("Clave")
        AgregaParametro ParametrosXML, "CONFIRMACION", Request("Confirmacion")
     end if

     'Login de Usuario
     set PortalCOM = Server.CreateObject("Portal.dmServerPortal")
     strLogin = PortalCOM.UsuarioLogin( ParametrosXML.XML )

     'Crear Documento para Cargar el XML Generado
     CreaDocumentoXML xmlLogin, false
     xmlLogin.loadXML( strLogin )
     'xmlLogin.load( Server.MapPath( "fijocambia.xml" ) )
     'Response.write( xmlLogin.xml )
     
     'Asignar valores de la sesi�n en base a resultado de login
     set NodoResultado = xmlLogin.selectSingleNode("//DESCRIPCIONES/RESULTADO")
     iResultado = NodoResultado.childNodes(K_RESULTADO).text
     if ( iResultado = lrOK ) or ( iResultado = lrChangePassword ) or ( iResultado = lrExpiredPassword ) then   ' El login fu� exitoso
	   Session("UserName") = NodoResultado.childNodes(K_USERNAME).text
	   Session("Clave") = NodoResultado.childNodes(K_CLAVE).text
        Session("us_codigo") = NodoResultado.childNodes(K_US_CODIGO).text
	   Session("Pagina") = NodoResultado.childNodes(K_PAGINA).text
	   Session("Nombre")= NodoResultado.childNodes(K_NOMBRE).text
        Session("cb_codigo") = NodoResultado.childNodes(K_CB_CODIGO).text
        Session("cm_empresa") = NodoResultado.childNodes(K_CM_EMPRESA).text
     end if

     'Crear Documento para Cargar el XSL con que se presenta la informaci�n
     CreaDocumentoXML xslLogin, false
     xslLogin.load( Server.MapPath( "fijo.xsl" ) )

     Response.Write( xmlLogin.transformNode( xslLogin ) )

%>

