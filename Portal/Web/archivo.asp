<% 
'******************************************************************************
Sub Conn() 
'******************************************************************************

Set Connect = Server.CreateObject("ADODB.Connection")
Connect.Open "PORTAL","sa","m"

End Sub %>

<% 
'******************************************************************************
Sub Footer(nParam) 
'******************************************************************************
%>
  <center>
  <table border="0" cellpadding="2" width="100%">
    <tr>
      <td width="100%" align="center" valign="bottom"><hr>
	  <% If nParam=0 Then %>
      	<img border="0" src="imagenes/tress_small.gif">
	  <% Else %>
		<img border="0" src="../imagenes/tress_small.gif">
	  <% End If %>
	  <br>
      <font face="Arial" size="1">Portal Empresarial<br>
      Grupo Tress Internacional<br>
      <a href="http://www.tress.com.mx" target="_new">http://www.tress.com.mx</a></font></td>
    </tr>
  </table>
  </center>
<% End Sub %>

<% 
'******************************************************************************
Sub MenuFijo()
'******************************************************************************
%>
<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 width="100%" bgcolor="#438ABC" height="55">
  <TR bgcolor="#438ABC">
<td align="left" bgcolor="#003366"><img src="IMAGENES/portal.gif"></td>
<td align="right" width="80%" bgcolor="#003366">
<font face="Tahoma" size="1"><b>
 <a href="inicio.asp" target="portal"><img src="IMAGENES/home.jpg" border=0 alt="Ir al Inicio"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 <a href="empleados/emp100.asp" target="portal"><img src="IMAGENES/empleado.gif" border=0></a>&nbsp;
 <a href="documentos/doc101.asp" target="portal"><img src="IMAGENES/documento.gif" border=0></a>&nbsp;
 <a href="sugerencias/sug100.asp" target="portal"><img src="IMAGENES/sugerencia.gif" border=0></a> 
 &nbsp;
</font></b>
</td></tr>
</table>
<p>
<% End Sub %>

<%
Function DiaLongFecha(fecha)
Dim Mes, StrDia
Select Case Month(fecha)
 Case 1
  Mes = "enero"
 Case 2
  Mes = "febrero"
 Case 3
  Mes = "marzo"
 Case 4
  Mes = "abril"
 Case 5
  Mes = "mayo"
 Case 6
  Mes = "junio"
 Case 7
  Mes = "julio"
 Case 8
  Mes = "agosto"
 Case 9
  Mes = "septiembre"
 Case 10
  Mes = "octubre"
 Case 11
  Mes = "noviembre"
 Case 12
  Mes = "diciembre"
End Select

Select Case WeekDay(fecha)
Case 1
	StrDia="Domingo"
Case 2
	StrDia="Lunes"
Case 3
	StrDia="Martes"
Case 4
	StrDia="Miercoles"
Case 5
	StrDia="Jueves"
Case 6
	StrDia="Viernes"
Case 7
	StrDia="Sabado"
End Select

DiaLongFecha= StrDia & " " & Day(fecha) & " de " & Mes & " de " & Year(fecha)
End Function
%>

<%
Function MesLetras(fecha)
Dim Mes
Select Case Month(fecha)
 Case 1
  Mes = "Ene"
 Case 2
  Mes = "Feb"
 Case 3
  Mes = "Mar"
 Case 4
  Mes = "Abr"
 Case 5
  Mes = "May"
 Case 6
  Mes = "Jun"
 Case 7
  Mes = "Jul"
 Case 8
  Mes = "Ago"
 Case 9
  Mes = "Sep"
 Case 10
  Mes = "Oct"
 Case 11
  Mes = "Nov"
 Case 12
  Mes = "Dic"
End Select
MesLetras = Day(fecha) & "/" & Mes & "/" & Year(fecha)
End Function
%>

<% Function GetPath(cParam)
Dim cStringPath, dbGlo

Select Case cParam
Case "5"
	Set dbGlo=Connect.Execute("select GL_FORMULA from GLOBAL where GL_CODIGO = '" & cParam & "'")
	cStringPath=dbGlo("GL_FORMULA")
Case Else
	Set dbGlo=Connect.Execute("select GL_FORMULA from GLOBAL where GL_CODIGO = '2'")
	cStringPath=dbGlo("GL_FORMULA")
End Select 
GetPath=cStringPath
End Function %>


<% 
'******************************************************************************
Sub Style()
'******************************************************************************
%>
<style type="text/css">

.alfa {  font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 8pt; font-style: normal; color: #ffffff; font-weight: bold}
.gama {  font-family: "Verdana, Sans Serif", geneva, helvetica; font-size: 9pt; color: #ffffff; font-weight: bold}
a.gama:hover {  color: #ffffff; text-decoration: none }
a:hover {  color: #ffffff; text-decoration: none }

.texto{ font-family: Verdana, Arial, Helvetica; font-size: 12px}

.nav{ font-family: Verdana, Arial, Helvetica; font-size: 11px; font-weight: normal ; color: #ffffff}

A.nav:link { color: #ffffff; text-decoration: none }
A.nav:visited { color: #ffffff; text-decoration: none }
A.nav:active { color: #ffffff; text-decoration: none }
A.nav:hover { color: #ffffff; text-decoration: none }

.subtitulo{ font-falimy: Verdana,Arial,Helvetica; font-size: 8pt; font-weight: bold ; font-family: Verdana, Arial, Helvetica, sans-serif;}

A.subtitulo:link { color: #000000; text-decoration: none }
A.subtitulo:visited { color: #000000; text-decoration: none }
A.subtitulo:active { color: #CBA248; text-decoration: none }
A.subtitulo:hover { color: #CBA248; text-decoration: none }

A:link { color: #000000; text-decoration: none }
A:visited { color: #000000; text-decoration: none }
A:active { color: #0000ff; text-decoration: none }
A:hover { color: #0000ff; text-decoration: none }
BODY { BEHAVIOR: url(#default#homePage) }
</style>
<% End Sub %>