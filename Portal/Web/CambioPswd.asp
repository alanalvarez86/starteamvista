<%@ Language=VBScript %>
<%option explicit%>
<!-- #include file="Rolodex/Tools.asp" -->
<%
	Response.Expires=0
	Response.Buffer=false

	'Declaramos Variables para Iniciar el COmponente y recibir el XML generado
	dim PortalCOM
    dim ParametrosXML
    dim WarningsXML
	dim docXML
	dim docXSL
	dim strCambio
	
     'Documento XML para enviar Parametros
     CreaDocumentoXML ParametrosXML, true
     AgregaParametro ParametrosXML, "CLAVEACTUAL", Request("CLAVEACTUAL")
     AgregaParametro ParametrosXML, "CLAVENUEVA", Request("CLAVENUEVA")
 	 AgregaParametro ParametrosXML, "CLAVECONF", Request("CLAVECONF")     
     AgregaParametro ParametrosXML, "USUARIO", Request("USUARIO")
     AgregaParametro ParametrosXML, "NOMBRE", Request("NOMBRE")
	 	
	 if ( Session("us_codigo")<>Application("UserDef") )then
		AgregaParametro ParametrosXML, "VALIDA", ""
	 else
		 AgregaParametro ParametrosXML, "VALIDA", "Invalido"
	 end if	 	

     'Response.write( ParametrosXML.XML )
	 'Invocar al Componente COM+ y recibir el documento de XML generado
	 set PortalCOM = Server.CreateObject("Portal.dmServerPortal")
     strCambio = PortalCOM.UsuarioCambiaPswd( ParametrosXML.XML )
     'Response.write( strCambio )

     'Crear Documento para Cargar el XML Generado
     CreaDocumentoXML docXML, false
     docXML.loadXML( strCambio )

     'Crear Documento para Cargar el XSL con que se presenta la información
     CreaDocumentoXML docXSL, false
	 docXSL.load( Server.MapPath( "Portal.xsl" ) )
     Response.Write( docXML.transformNode( docXSL ) )
%>

