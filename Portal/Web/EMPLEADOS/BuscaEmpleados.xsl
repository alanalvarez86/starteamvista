<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template match="/">
  <xsl:element name="link">
		<xsl:attribute name="rel">stylesheet</xsl:attribute>
		<xsl:attribute name="href">../Portal.css</xsl:attribute>
		<xsl:attribute name="type">text/css</xsl:attribute>
	</xsl:element>

        <center>
Empleados Encontrados con el criterio:
          <table border="0" width="90%" height="100%">
            <tr>
              <td valign="top">

                <!--<table bgcolor="#A5BACE" width="100%">-->
					 <table width="100%" border="1" cellpadding="2" cellspacing="2" bordercolor="#acacac" bordercolorlight="#C5C3C3" bordercolordark="#ffffff" bgcolor="#A5BACE">
                  <tr>
                    <td>
                      <font face="Arial" size="2">
                        <b>Buscar Empleado</b>
                      </font>
                    </td>
                  </tr>
                </table>
                <!--<table border="0" width="100%" cellpadding="2" cellspacing="1" bgcolor="#CECFCE">-->
                <table width="100%" border="1" cellpadding="2" cellspacing="2" bordercolor="#acacac" bordercolorlight="#C5C3C3" bordercolordark="#ffffff" bgcolor="#E6E4E4">
                  <tr>
                    <td bgcolor="#EFEFEF" align="center">
                      <font face="arial" size="2">
                        <b>Numero</b>
                      </font>
                    </td>
                    <td bgcolor="#EFEFEF">
                      <font face="arial" size="2">
                        <b>Apellidos</b>
                      </font>
                    </td>
                    <td bgcolor="#EFEFEF">
                      <font face="arial" size="2">
                        <b>Nombre(s)</b>
                      </font>
                    </td>
                  </tr>
                  <xsl:for-each select="//empleados/empleado"> 
                    <tr>
                      <td>
                        <a>
                          <xsl:attribute name="href">GetEmpleado.asp?CB_CODIGO=<xsl:value-of select="@CB_CODIGO"/><xsl:text>&#38;</xsl:text>EMPRESA=<xsl:value-of select="EMPRESA"/></xsl:attribute>
                          <xsl:value-of select="CB_CODIGO"/>
                        </a>
                      </td>
                      <td>
                        <xsl:value-of select="CB_APE_PAT"/>
			<xsl:text>  </xsl:text>
			<xsl:value-of select="CB_APE_MAT"/>
                      </td>
                      <td>
                        <xsl:value-of select="CB_NOMBRES"/>
                      </td>
                    </tr>
                  </xsl:for-each>
                </table>
				<p></p>		

<a><xsl:attribute name="href">emp100.asp</xsl:attribute>
<img>
<xsl:attribute name="src">../IMAGENES/back.gif</xsl:attribute>
<xsl:attribute name="border">0</xsl:attribute>
</img>
</a>

</td></tr>
</table>
        </center>
  </xsl:template>
</xsl:stylesheet>