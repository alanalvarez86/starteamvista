<%@ Language=VBScript %>
<%option explicit%>
<%
	Response.Expires=0
	Response.Buffer=false

	'Declaramos Variables
	dim docXSL
	dim docXML

    set docXML = Server.CreateObject("Msxml2.DOMDocument.4.0")
	 docXML.async = false
	 
     docXML.load(Server.MapPath("Extensiones.xml"))
	
    set docXSL = Server.CreateObject("Msxml2.DOMDocument.4.0")
	 docXSL.async = false
	 docXSL.load(Server.MapPath("Extensiones.xsl"))

	Response.Write(docXML.transformNode(docXSL))

   
%>

