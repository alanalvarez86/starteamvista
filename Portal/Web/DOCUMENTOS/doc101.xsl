<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>
	<xsl:template match="/">
<!--
	<xsl:element name="link">
		<xsl:attribute name="rel">stylesheet</xsl:attribute>
		<xsl:attribute name="href">../Portal.css</xsl:attribute>
		<xsl:attribute name="type">text/css</xsl:attribute>
	</xsl:element>
-->
	<xsl:element name="body">
		<center>
			<table border="0" width="90%" height="100%">
				<tr><td height="90%" valign="top">
					<table bgcolor="#A5BACE" width="100%">
						<tr><td>
							<font face="Arial" size="2"><b>Consultar Documento</b></font>
						</td></tr>
					</table>
					<xsl:element name="P"/>
					<center>
						<form method="GET" action="doc101.asp" name="frmdoc" onsubmit="return ValDatos()">
							<table border="0" cellpadding="2" cellspacing="2" bgcolor="#ffffff">
  								<tr>
								    <td align="right"><font face="Arial" size="2"><b>Tema:</b></font></td>
								    <td align="left"><xsl:element name="input"><xsl:attribute name="type">text</xsl:attribute><xsl:attribute name="name">dc_palabra</xsl:attribute><xsl:attribute name="size">30</xsl:attribute><xsl:attribute name="value"><xsl:value-of select="RAIZ/PAGINA/@PALABRA"/></xsl:attribute><xsl:attribute name="onChange">javascript:this.value=this.value.toUpperCase();</xsl:attribute></xsl:element> <!--<input type="text" name="dc_palabra" size="30" value="" onChange="javascript:this.value=this.value.toUpperCase();"/>--><xsl:text> </xsl:text><input type="submit" value="  Buscar  "/>
								    </td>
								</tr>
							</table>
						</form>
						<xsl:element name="p"/>
					</center>

                    <p></p>
  <xsl:variable name="number"><xsl:value-of select="RAIZ/PAGINA/@NUMERO"/></xsl:variable>
  <xsl:if	test="$number!=0">
	<table width="100%" border="0" bgcolor="#E2E2E2">
		<tr><td align="left">
				<font face="Arial" size="2"><b><xsl:value-of select="RAIZ/PAGINA/@TOTAL"/></b> Documentos, visualizando del <b><xsl:value-of select="RAIZ/PAGINA/@INICIAL"/></b> al <b><xsl:value-of select="RAIZ/PAGINA/@FINAL"/></b></font>
    		 </td>
			 <td align="right">
    			<font face="Arial" size="2">P�gina <xsl:value-of select="RAIZ/PAGINA/@NUMERO"/> de <xsl:value-of select="RAIZ/OTRAS_PAGINAS/@CUANTAS"/></font>
    		 </td>
		</tr>
	</table>
	<xsl:element name="p"/>
	<font face="arial" size="2">
		<xsl:variable name="memo"><xsl:value-of select="@DC_MEMO"/></xsl:variable>
		<xsl:variable name="longitud"><xsl:value-of select="string-length(RAIZ/PAGINA/DOCUMENT/ROWS/ROW/@DC_MEMO)"/></xsl:variable>		
		<xsl:variable name="path"><xsl:value-of select="RAIZ/GLOBAL/@G3"/></xsl:variable>
		<xsl:for-each select="RAIZ/PAGINA/DOCUMENT/ROWS/ROW">
			<b><xsl:text>&gt; </xsl:text><xsl:value-of select="@DC_TITULO"/></b><xsl:text> </xsl:text><i>(Autor: <xsl:value-of select="@DC_AUTOR"/>,<xsl:text> </xsl:text><xsl:value-of select="@DC_FECHA"/><xsl:text> </xsl:text><xsl:value-of select="@DC_HORA"/>)</i><xsl:element name="br"/>
			<xsl:if test="$longitud!=0">
				<xsl:value-of select="@DC_MEMO"/><xsl:element name="br"/>		
			</xsl:if>
			<xsl:element name="a">
				<xsl:attribute name="href"><xsl:value-of select="$path"/><xsl:value-of select="@DC_RUTA"/></xsl:attribute><b><xsl:value-of select="RAIZ/GLOBAL/@G3"/><xsl:value-of select="@DC_RUTA"/></b>
			</xsl:element>			
			<xsl:element name="p"/>
		</xsl:for-each>
	</font>

	<xsl:variable name="paginas"><xsl:value-of select="RAIZ/OTRAS_PAGINAS/@CUANTAS"/></xsl:variable>	
	<xsl:variable name="anterior"><xsl:value-of select="RAIZ/OTRAS_PAGINAS/@ANTERIOR"/></xsl:variable>
	<xsl:variable name="siguiente"><xsl:value-of select="RAIZ/OTRAS_PAGINAS/@SIGUIENTE"/></xsl:variable>
	<xsl:variable name="palabra"><xsl:value-of select="RAIZ/PAGINA/@PALABRA"/></xsl:variable>
	
	<xsl:if test="$paginas!=0">
		<xsl:if test="$paginas!=1">	
			<table bgcolor="#E2E2E2" border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
	 			<td align="center">
				<font face="arial" size="2">
				Seleccione una P�gina:<xsl:element name="br"/>				
				<xsl:for-each select="RAIZ/OTRAS_PAGINAS/PAGE">
					<xsl:element name="a">
						<xsl:attribute name="href">doc101.asp?NAV=<xsl:value-of select="@NUMERO"/><xsl:text>&#38;</xsl:text>dc_palabra=<xsl:value-of select="$palabra"/></xsl:attribute><xsl:value-of select="@NUMERO"/>
					</xsl:element>
					<xsl:text> </xsl:text>
				</xsl:for-each>
					<table width="100%" border="0" cellpadding="0" cellspacing="0"><tr>			
						<xsl:if test="$anterior!=0">
							<td align="left">
							<xsl:element name="a">						
								<xsl:attribute name="href">doc101.asp?NAV=<xsl:value-of select="RAIZ/OTRAS_PAGINAS/@ANTERIOR"/><xsl:text>&#38;</xsl:text>dc_palabra=<xsl:value-of select="RAIZ/PAGINA/@PALABRA"/></xsl:attribute><font face="arial" size="2">&lt;&lt; P�gina Anterior</font>				
							</xsl:element>
							</td>
						</xsl:if>		
						<xsl:if test="$siguiente!=0">
							<td align="right">					
							<xsl:element name="a">
								<xsl:attribute name="href">doc101.asp?NAV=<xsl:value-of select="RAIZ/OTRAS_PAGINAS/@SIGUIENTE"/><xsl:text>&#38;</xsl:text>dc_palabra=<xsl:value-of select="RAIZ/PAGINA/@PALABRA"/></xsl:attribute><font face="arial" size="2">P�gina Siguiente &gt;&gt;</font>
							</xsl:element>
							</td>						
						</xsl:if>					
					</tr>
					</table>				
				</font>
			</td></tr>
			</table>			
		</xsl:if>
	</xsl:if>				
	<xsl:element name="p"/>
  </xsl:if>	
	
  <xsl:if test="$number=0">
  		<center><font face="Arial" size="2">No hay documentos con el tema: <b><u><xsl:value-of select="RAIZ/PAGINA/@PALABRA"/></u></b></font></center>
  </xsl:if>
</td></tr>
<tr><td valign="top" height="10%">
  <center>
  <table border="0" cellpadding="2" width="100%">
    <tr>
      <td width="100%" align="center" valign="bottom"><xsl:element name="hr"/>
      
<!--		<img border="0" src="../imagenes/tress_small.gif"/> -->
		<xsl:element name="img">
			<xsl:attribute name="border">0</xsl:attribute>
			<xsl:attribute name="src"><xsl:value-of select="RAIZ/GLOBAL/@G11"/></xsl:attribute>
		</xsl:element>
		
		<xsl:element name="br"/>

      <font face="Arial" size="1">Portal Empresarial<xsl:element name="br"/>
      
<!--      Grupo Tress Internacional<xsl:element name="br"/>
      <a href="http://www.tress.com.mx" target="_new">http://www.tress.com.mx</a> -->
   			<xsl:value-of select="RAIZ/GLOBAL/@G6"/><xsl:element name="br"/>
   			<xsl:element name="a">
   				<xsl:attribute name="href"><xsl:value-of select="RAIZ/GLOBAL/@G19"/></xsl:attribute>
 				<xsl:attribute name="target">"_new"><xsl:value-of select="RAIZ/GLOBAL/@G19"/></xsl:attribute>
				<xsl:value-of select="RAIZ/GLOBAL/@G19"/> 	
			</xsl:element>		

      </font>
      </td>
    </tr>
  </table>
  </center>

</td></tr>
</table>
</center>
</xsl:element>
</xsl:template>
</xsl:stylesheet>