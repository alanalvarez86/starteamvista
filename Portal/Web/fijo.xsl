<?xml version='1.0' encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>

<!-- TEMPLATE RAIZ -->

<xsl:template match="/">
<html>
  <head>
    <link rel="stylesheet" href="Portal.css" type="text/css"/>
    <script language="JavaScript" SRC="Rolodex/CommonTools.js"/>
    <xsl:apply-templates select="RAIZ/DESCRIPCIONES/META"/>
    <title>
       <xsl:apply-templates select="RAIZ/DESCRIPCIONES/MODULO"/> -
	  <xsl:apply-templates select="RAIZ/DESCRIPCIONES/PANTALLA"/>
    </title>
  </head>
  <body bgcolor="#428ABD" text="#FFFFFF" onLoad="{RAIZ/DESCRIPCIONES/CARGAR}">
    <center>
       <xsl:apply-templates/>
    </center>
  </body>
</html>
</xsl:template>

<!-- TEMPLATE DE ELEMENTO RAIZ -->

<xsl:template match="RESULTADO">
     <!-- <xsl:apply-templates/> -->
</xsl:template>

<xsl:template match="RAIZ">
     <xsl:apply-templates/>
</xsl:template>

<!-- TEMPLATE DE ELEMENTO DESCRIPCIONES -->

<xsl:template match="DESCRIPCIONES">
     <xsl:apply-templates select="modulo"/>
</xsl:template>

<!-- TEMPLATE DESPLIEGUE MODULO -->

<xsl:template match="MODULO">
    <xsl:value-of select="."/>
</xsl:template>

<!-- TEMPLATE PARA AGREGAR META INFORMATION -->

<xsl:template match="META">
  <meta http-equiv="{@equiv}" content="{.}"/>
</xsl:template>

<!-- TEMPLATE DESPLIEGUE PANTALLA -->

<xsl:template match="PANTALLA">
     <xsl:value-of select="."/>
</xsl:template>

<!-- TEMPLATE DESPLIEGUE FORMA -->

<xsl:template match="FORMA">
   <form name="{@nombre}" id="{@nombre}" method="{@metodo}" action="{@accion}" target="{@destino}" onSubmit="{@validar}" onLoad="{@cargar}">
       <xsl:apply-templates/>
   </form>
</xsl:template>

<xsl:template match="TITULO">
   <font class="F_TITULO">
      <xsl:value-of select="."/>
   </font>
</xsl:template>

<xsl:template match="TEXTO">
   <font class="F_TEXTO">
      <xsl:value-of select="."/>
   </font>
</xsl:template>

<xsl:template match="LABEL">
   <font class="F_LABEL">
      <xsl:value-of select="."/>
   </font>
</xsl:template>

<!-- TEMPLATE DE LINEAS  -->
<xsl:template match="LINEA">
	<hr/>
</xsl:template>

<!-- TEMPLATE DE SECCION  -->
<xsl:template match="SECCION">
    <p>
       <xsl:apply-templates/>
    </p>
</xsl:template>

<xsl:template match="ENCABEZADO|RENGLON">
    <tr>
      <xsl:apply-templates/>
    </tr>
</xsl:template>

<!-- TEMPLATES PARA COLUMNAS DE TABLAS CONTENIDO MASTER -->
 <xsl:template match="CONTENIDO">
    <xsl:choose>
       <xsl:when test="string-length(@alineacion)!=0">
           <xsl:if test="string-length(@clasefuente)!=0">
          	<td class="{@clasefuente}" align="{@alineacion}">
                   <xsl:apply-templates/>
               </td>
    		</xsl:if>
    		<xsl:if test="string-length(@clasefuente)=0">
    			<td align="{@alineacion}">
                   <xsl:apply-templates/>
               </td>
    		</xsl:if>
       </xsl:when>
       <xsl:otherwise>
          <xsl:if test="string-length(@clasefuente)!=0">
               <td class="{@clasefuente}">
                   <xsl:apply-templates/>
               </td>
    		</xsl:if>
    		<xsl:if test="string-length(@clasefuente)=0">
    			<td>
                   <xsl:apply-templates/>
               </td>
    		</xsl:if>
       </xsl:otherwise>
    </xsl:choose>
 </xsl:template>

<!-- TEMPLATE DESPLIEGUE ESPACIO -->

<xsl:template match="ESPACIO">
     <br/>
</xsl:template>

<!-- TEMPLATE PARA LIGA HTML -->

<xsl:template match="LIGA">
  	<a href="{@href}" target="{@destino}" class="F_TEXTO" alt="{@hint}"><xsl:value-of select="."/></a>
</xsl:template>

<xsl:template match="LIGAIMG">
	<a href="{@href}" target="{@destino}" ><img src="{@fuente}" border="0" alt="{@hint}"/></a>
</xsl:template>

<!-- TEMPLATE PARA BOTONES DE SUBMIT -->
<xsl:template match="BTNSUBMIT">
    <input type="submit" name="{@nombre}" value="{.}"/>
</xsl:template>

<!-- TEMPLATE PARA BOTONES DE RESET -->
<xsl:template match="BTNRESET">
    <input type="reset" name="{@nombre}" value="{.}"/> 
</xsl:template>

<!-- TEMPLATE PARA BOTONES DE USO GENERAL -->
<xsl:template match="BOTON">
    <input type="button" name="{@nombre}" value="{.}" onclick="{@onclick}"/>
</xsl:template>

<!-- TEMPLATE PARA EDITS -->
<xsl:template match="EDIT">
    <input type="Text" size="{@tamano}" maxlength="{@longitud}" name="{@nombre}" value="{.}"/>
</xsl:template>

<!-- TEMPLATE PARA EDITS -->
<xsl:template match="EDITPSWD">
    <input type="password" size="{@tamano}" maxlength="{@longitud}" name="{@nombre}" value="{.}"/>
</xsl:template>

<!-- TEMPLATE PARA EDITS HIDDEN -->
<xsl:template match="HIDDEN">
    <input type="hidden" name="{@nombre}" value="{.}"/>
</xsl:template>

<xsl:template match="IMAGEN">
    <IMG src="{@fuente}" alt="{@hint}"/>
</xsl:template>

<xsl:template match="MASTER">
     <hr/>
     <table border="0" align="center" width="100%" cellspacing="0" cellpadding="0">
            <xsl:apply-templates/>
     </table>
     <hr/>
</xsl:template>

<xsl:template match="FOOTER">
   <xsl:element name="a">
     <xsl:attribute name="href">http://www.tress.com.mx/Boletin/<xsl:value-of select="//RAIZ/DESCRIPCIONES/MES"/><xsl:value-of select="//RAIZ/DESCRIPCIONES/ANNO"/>/Boletin.htm</xsl:attribute>
     <xsl:attribute name="target">new</xsl:attribute>
     <xsl:attribute name="class">F_TEXTO</xsl:attribute>
     Boletin <xsl:value-of select="//RAIZ/DESCRIPCIONES/MES"/> <xsl:value-of select="//RAIZ/DESCRIPCIONES/ANNO"/>
  </xsl:element>
  <hr/>
  <font face="Arial" size="2"><b>Asesores Certificados</b></font>
  <p>
     <a href="http://www.tress.com.mx/default.asp?id=4&amp;ACT=5&amp;content=9&amp;mnu=4" target="portal" class="F_TEXTO" >Nivel I</a> 
     <a href="http://www.tress.com.mx/default.asp?id=4&amp;ACT=5&amp;content=10&amp;mnu=4" target="portal" class="F_TEXTO" >Nivel II</a><br/>
     <hr/>
  </p>
  <p>
     <a href="http://www.tress.com.mx/portal/docs/Extensiones.asp" target="portal" class="F_TEXTO" >Extensiones Telefónicas</a><br/>
     <a href="https://e-commerce.soffront.com/TrackWeb/TrackWeb.htm" target="new" class="F_TEXTO" >TrackWeb</a><br/>
     <hr/>
  </p>
</xsl:template>

</xsl:stylesheet>


