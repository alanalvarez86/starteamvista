<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="text"/>
<xsl:template match="/">

	<xsl:variable name="cMemo"><xsl:value-of select="RAIZ/NOTICIA/ROWS/ROW/@NT_MEMO"/></xsl:variable>

	<xsl:element name="body">
		<xsl:attribute name="vLink">#000000</xsl:attribute>
		<xsl:attribute name="aLink">#000000</xsl:attribute>
		<xsl:attribute name="link">#000000</xsl:attribute>
		<xsl:attribute name="bgColor">#FFFFFF</xsl:attribute>
	
	
	<center>
		<table width="90%">
			<xsl:value-of select="$cMemo"/>
		</table>	
	<xsl:element name="br"/>
		
	</center>
	
	</xsl:element>
</xsl:template>

</xsl:stylesheet>