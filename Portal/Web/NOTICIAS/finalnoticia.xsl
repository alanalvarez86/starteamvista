<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>
<xsl:template match="/">
<xsl:element name="link">
		<xsl:attribute name="rel">stylesheet</xsl:attribute>
		<xsl:attribute name="href">../Portal.css</xsl:attribute>
		<xsl:attribute name="type">text/css</xsl:attribute>
</xsl:element>

<xsl:element name="br"/>
<xsl:element name="br"/>

<center>
	<table width="90%" border="0" cellspacing="0" cellpadding="0">
 					<tr>
 						<td align="left">
 							<xsl:element name="a">
 								<xsl:attribute name="href">javascript:window.history.back()</xsl:attribute>
 							
 							<xsl:element name="img">
 								<xsl:attribute name="src">../IMAGENES/back.gif</xsl:attribute>
 								<xsl:attribute name="border">0</xsl:attribute>
								<xsl:attribute name="alt">Pagina Anterior</xsl:attribute>
 							</xsl:element>
 							
 							</xsl:element>
 						</td>	
 						<td align="center">
     						<xsl:element name="a">
 								<xsl:attribute name="href">noticias100.asp</xsl:attribute>
 							
 							<xsl:element name="img">
 								<xsl:attribute name="src">../IMAGENES/masnoticias.gif</xsl:attribute>
 								<xsl:attribute name="border">0</xsl:attribute>
								<xsl:attribute name="alt">Noticias Anteriores</xsl:attribute>
 							</xsl:element>
 								
		   					</xsl:element>  					
     					</td>
     					<td align="Right">
     						<xsl:element name="a">
 								<xsl:attribute name="href">../inicio.asp</xsl:attribute>
 							
 							<xsl:element name="img">
 								<xsl:attribute name="src">../IMAGENES/inicio.gif</xsl:attribute>
 								<xsl:attribute name="border">0</xsl:attribute>
								<xsl:attribute name="alt">Inicio</xsl:attribute>
 							</xsl:element>
 							
 							</xsl:element>
      					</td>
     					
     				</tr>
			
				</table>
			
  		<table border="0" cellpadding="2" width="90%">
   			<tr>
   				<td width="100%" align="center" valign="bottom">
    				<xsl:element name="hr"/>
					<xsl:element name="img">
						<xsl:attribute name="border">0</xsl:attribute>
						<!--<xsl:attribute name="src">../imagenes/tress_small.gif</xsl:attribute>-->
						<xsl:attribute name="src"><xsl:value-of select="RAIZ/GLOBAL/@G11"/></xsl:attribute>
					</xsl:element>
				</td>			 	 				
			</tr>
			<tr>
				<td width="100%" align="center" valign="bottom">	
   					<font face="Arial" size="1">Portal Empresarial
   					<xsl:element name="br"/>
   							<xsl:value-of select="RAIZ/GLOBAL/@G6"/><!--Grupo Tress Internacional-->
   					<xsl:element name="a">
   						<xsl:attribute name="href"><xsl:value-of select="RAIZ/GLOBAL/@G19"/></xsl:attribute>
 						<xsl:attribute name="target">"_new"><xsl:value-of select="RAIZ/GLOBAL/@G19"/></xsl:attribute>
 						<xsl:element name="br"/>
 						<xsl:value-of select="RAIZ/GLOBAL/@G19"/> 	
 						<!--<xsl:attribute name="href">http://www.tress.com.mx</xsl:attribute>
 						<xsl:attribute name="target">"_new">http://www.tress.com.mx</xsl:attribute>
 						<xsl:element name="br"/>
 						http://www.tress.com.mx-->
 					</xsl:element>		
   					</font>
   				</td>
   			</tr>
  		</table>
  	</center>
  	
 	</xsl:template>

</xsl:stylesheet>