<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>
	<xsl:template match="/">
	<xsl:element name="link">
		<xsl:attribute name="rel">stylesheet</xsl:attribute>
		<xsl:attribute name="href">../Portal.css</xsl:attribute>
		<xsl:attribute name="type">text/css</xsl:attribute>
	</xsl:element>
	<xsl:element name="body">
		<xsl:attribute name="vLink">#000000</xsl:attribute>
		<xsl:attribute name="aLink">#000000</xsl:attribute>
		<xsl:attribute name="link">#000000</xsl:attribute>
		<xsl:attribute name="bgColor">#FFFFFF</xsl:attribute>						
<!--	<BODY vLink=#00000 aLink=#000000 link=#000000 bgColor="#FFFFFF">-->	
	<center>
		<table border="0" width="90%" height="100%">
		  	<tr>
				<td valign="top">		
					<table width="100%" border="1" height="20" bgcolor="#08498C">
						<tr><td height="20"><img src="../imagenes/noticiaanterior.gif" border="0" /></td></tr>
					</table>
					<xsl:element name="p"/>
					<table width="100%" bgcolor="#E6E4E4" border="1" bordercolor="#acacac" bordercolorlight="#C5C3C3" bordercolordark="#ffffff">
					<xsl:for-each select="RAIZ/NOTICIAS/ROWS/ROW">										
					<tr><td align="center" valign="top"><font size="1" face="Verdana"><b><xsl:value-of select="@NT_FECHA"/></b></font></td>
						 <td><font class="subtitulo">
						 		<xsl:element name="a">
									<xsl:attribute name="href">vnoticia.asp?folio=<xsl:value-of select="@NT_FOLIO"/></xsl:attribute>
									<xsl:value-of select="@NT_TITULO"/>									
								</xsl:element>
								</font></td>
					</tr>
					</xsl:for-each>
					<!--<% dbNoticia.MoveNext
					Loop %>-->
					</table>	
					<xsl:element name="br"/>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr><td align="left"><a href="javascript:window.history.back()"><img src="../IMAGENES/back.gif" border="0" alt="P�gina Anterior" width="79" height="30"/></a></td>
   		  					 <td align="Right"><a href="../inicio.asp"><img src="../imagenes/inicio.gif" border="0" alt="Inicio" width="56" height="21"/></a></td>
     					</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td valign="bottom">
					<center>
	 		 			<table border="0" cellpadding="2" width="100%">
    						<tr><td width="100%" align="center" valign="bottom"><xsl:element name="hr"/>
    						<!--<img border="0" src="../imagenes/tress_small.gif"/>-->
    						<xsl:element name="img">
    							<xsl:attribute name="src"><xsl:value-of select="RAIZ/GLOBAL/@G11"/></xsl:attribute>
    						</xsl:element>
    						<xsl:element name="br"/><font face="Arial" size="1">Portal Empresarial<xsl:element name="br"/><xsl:value-of select="RAIZ/GLOBAL/@G6"/><!--Grupo Tress Internacional--><xsl:element name="br"/>
    							<!--<a href="http://www.tress.com.mx" target="_new">http://www.tress.com.mx</a>-->
    						<xsl:element name="a">
    							<xsl:attribute name="href"><xsl:value-of select="RAIZ/GLOBAL/@G19"/></xsl:attribute>
    							<xsl:attribute name="target">"_new"><xsl:value-of select="RAIZ/GLOBAL/@G19"/></xsl:attribute>
    							<xsl:value-of select="RAIZ/GLOBAL/@G19"/>
    						</xsl:element>	
    						</font></td></tr>
  						</table>
  					</center>				
					<!--<% Footer(1) %>-->
				</td>
			</tr>
		</table>
	</center>
	</xsl:element>
<!--	</body>-->
</xsl:template>
</xsl:stylesheet>