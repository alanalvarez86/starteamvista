<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>
<xsl:template match="/">

	<xsl:variable name="cTitular"><xsl:value-of select="RAIZ/NOTICIA/ROWS/ROW/@NT_TITULO"/></xsl:variable>
	<xsl:variable name="cSubtitular"><xsl:value-of select="RAIZ/NOTICIA/ROWS/ROW/@NT_SUB_TIT"/></xsl:variable>
	<xsl:variable name="dFecha"><xsl:value-of select="RAIZ/NOTICIA/ROWS/ROW/@NT_FECHA"/></xsl:variable>
	<xsl:variable name="cHora"><xsl:value-of select="RAIZ/NOTICIA/ROWS/ROW/@NT_HORA"/></xsl:variable>
	<xsl:variable name="dAutor"><xsl:value-of select="RAIZ/NOTICIA/ROWS/ROW/@NT_AUTOR"/></xsl:variable>
	<xsl:variable name="cMemo"><xsl:value-of select="RAIZ/NOTICIA/ROWS/ROW/@NT_MEMO"/></xsl:variable>
	<xsl:variable name="cImagenDelTitular"><xsl:value-of select="RAIZ/NOTICIA/ROWS/ROW/@NT_IMAGEN"/></xsl:variable>



	<xsl:element name="body">
		<xsl:attribute name="vLink">#000000</xsl:attribute>
		<xsl:attribute name="aLink">#000000</xsl:attribute>
		<xsl:attribute name="link">#000000</xsl:attribute>
		<xsl:attribute name="bgColor">#FFFFFF</xsl:attribute>
	 </xsl:element>	
	<!--<table border="0" width="90%" >
  		<tr>
    		<td bgcolor="#ffffff" valign="top">-->
			
   			 	<xsl:if test="string-length($cSubtitular)!=0">
   			 		<center>
					<table bgcolor="#08498C" width="90%">
						<tr>
							<td align="center">
								<font face="Verdana" size="2" color="#FFCC00">
									<b><xsl:value-of select="$cSubtitular"/></b>
									<xsl:element name="br"/>
								</font>
							</td>
						</tr>
					</table>
					</center>
				</xsl:if>	
				
				<xsl:choose>
				<xsl:when test="string-length($cImagenDelTitular)!=0">
					<xsl:element name="p">
						<xsl:attribute name="align">right</xsl:attribute>
					</xsl:element>
					<center>
					<table width="90%" border="1" cellpadding="2" cellspacing="2" bordercolor="#acacac" bordercolorlight="#C5C3C3" bordercolordark="#ffffff" bgcolor="#E6E4E4">
					<tr>
						<td valign="top" width="20%">
							<xsl:element name="img">
								<xsl:attribute name="border">0</xsl:attribute>
								<xsl:attribute name="src"><xsl:value-of select="$cImagenDelTitular"/></xsl:attribute>
								<xsl:attribute name="alt"><xsl:value-of select="$cImagenDelTitular"/></xsl:attribute>
							</xsl:element>	
						</td>
						<td align="center" valign="middle">
							<font size="3" face="Verdana">
								<b><xsl:value-of select="$cTitular"/></b>
							</font>
						</td>
					</tr>
					</table>
					</center>	
				</xsl:when>	
				
				<xsl:when test="string-length($cImagenDelTitular)=0">
					<xsl:element name="p">
						<xsl:attribute name="align">center</xsl:attribute>
					</xsl:element>
					<center>
					<table width="90%" border="1" cellpadding="2" cellspacing="2" bordercolor="#acacac" bordercolorlight="#C5C3C3" bordercolordark="#ffffff" bgcolor="#E6E4E4">
						<tr>
							<td align="center" valign="middle">
								<font size="3" face="Verdana">
									<b><xsl:value-of select="$cTitular"/></b>
								</font>
							</td>
						</tr>
					</table>
					</center>		
				</xsl:when>	
						
				</xsl:choose>
				<center>
				<table width="90%">
				<font size="1" face="Verdana">
				<xsl:element name="i">(Por:
					<xsl:value-of select="$dAutor"/>)
				</xsl:element>
				</font>
				<xsl:element name="br"/>
				<xsl:element name="br"/>
				<font size="2" face="Verdana">
				<xsl:element name="i">
					(<xsl:value-of select="$dFecha"/>,  <xsl:value-of select="$cHora"/>)
				</xsl:element>
				</font>
				</table>
				</center>
			<!--</td>
		</tr>
	</table>-->
 
	
				<!--<xsl:element name="p">
					<xsl:attribute name="align">justify</xsl:attribute>
				</xsl:element>
				<font size="2" face="Verdana">
				<xsl:element name="i">
					(<xsl:value-of select="$dFecha"/>,  <xsl:value-of select="$cHora"/>)
				</xsl:element>
						<xsl:value-of select="$cMemo"/>
				</font>
				
				<xsl:element name="p">
					<xsl:attribute name="align">center</xsl:attribute>
				</xsl:element>
				<table width="90%" border="0" cellspacing="0 cellpadding=0">
 					<tr>
 						<td align="left">
 							<xsl:element name="a">
 								<xsl:attribute name="href">javascript:window.history.back()</xsl:attribute>
 							</xsl:element>
 							<xsl:element name="img">
 								<xsl:attribute name="src">../IMAGENES/back.gif</xsl:attribute>
 								<xsl:attribute name="border">0</xsl:attribute>
								<xsl:attribute name="alt">Pagina Anterior</xsl:attribute>
 							</xsl:element>
 						</td>	
 						<td align="center">
     						<xsl:element name="a">
 								<xsl:attribute name="href">noticias.asp</xsl:attribute>
 							</xsl:element>
 							<xsl:element name="img">
 								<xsl:attribute name="src">../IMAGENES/masnoticias.gif</xsl:attribute>
 								<xsl:attribute name="border">0</xsl:attribute>
								<xsl:attribute name="alt">Noticias Anteriores</xsl:attribute>
 							</xsl:element>     					
     					</td>
     					<td align="Right">
     						<xsl:element name="a">
 								<xsl:attribute name="href">../inicio.asp</xsl:attribute>
 							</xsl:element>
 							<xsl:element name="img">
 								<xsl:attribute name="src">../IMAGENES/inicio.gif</xsl:attribute>
 								<xsl:attribute name="border">0</xsl:attribute>
								<xsl:attribute name="alt">Inicio</xsl:attribute>
 							</xsl:element>
      					</td>
     					
     				</tr>
			
						</table>-->
				
				<!--<center>
  				<table border="0" cellpadding="2" width="100%">
    			<tr>
      				<td width="100%" align="center" valign="bottom">
      				<xsl:element name="hr"/>
	  					<xsl:element name="img">
	  						<xsl:attribute name="border">0</xsl:attribute>
	  						<xsl:attribute name="src">../imagenes/tress_small.gif</xsl:attribute>
	  					</xsl:element>		 	 				
      					<font face="Arial" size="1">Portal Empresarial
      							Grupo Tress Internacional
      					<xsl:element name="a">
 								<xsl:attribute name="href">http://www.tress.com.mx</xsl:attribute>
 								<xsl:attribute name="target">"_new">http://www.tress.com.mx</xsl:attribute>
 						</xsl:element>		
      					</font>
      				</td>
    			</tr>
  				</table>
  				</center>-->
			
				
			
  			<!--</td>
		</tr>
	</table>-->

</xsl:template>

</xsl:stylesheet>