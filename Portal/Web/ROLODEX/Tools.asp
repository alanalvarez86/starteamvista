<% Sub CreaDocumentoXML( docXML, lEditar )
   set docXML = Server.CreateObject("Msxml2.DOMDocument.4.0")
   docXML.async = false
   if lEditar = true then
      set docXML.documentElement = docXML.createElement( "RAIZ" )
      docXML.documentElement.appendChild docXML.createElement( "PARAMETROS" )
   end if
End Sub %>
<% Sub AgregaParametro( docXML, sTagName, sContenido )
   dim NodoParametros
   set NodoParametros = docXML.selectSingleNode("//PARAMETROS")
   'if NodoSeccion = null then
   '   set NodoSeccion = docXML.createElement( "PARAMETROS" )
   'end if
   NodoParametros.appendChild docXML.createElement( sTagName )
   NodoParametros.lastChild.text = sContenido
End Sub %>

<% Sub AgregaDato( docXML, NodoTarjeta, sCampo )
   NodoTarjeta.appendChild docXML.createElement( sCampo )
   NodoTarjeta.lastChild.text = Request(sCampo)
   NodoTarjeta.lastChild.setAttribute "Anterior", Request(sCampo +"_ACTUAL")
End Sub %>

<% Sub SetTarjetaXML( docXML )
   dim NodoParametros, NodoTarjeta
   set NodoParametros = docXML.selectSingleNode("//PARAMETROS")

   set NodoTarjeta = docXML.createElement( "TARJETA" )
   AgregaDato docXML, NodoTarjeta, "EMPRESA"
   AgregaDato docXML, NodoTarjeta, "NOMBRES"
   AgregaDato docXML, NodoTarjeta, "TELEFONOS"
   AgregaDato docXML, NodoTarjeta, "DIRECCION"
   AgregaDato docXML, NodoTarjeta, "CIUDAD"
   AgregaDato docXML, NodoTarjeta, "COMENTARIO"
   AgregaDato docXML, NodoTarjeta, "EMAIL"

   NodoParametros.appendChild NodoTarjeta
End Sub %>

<%
Function MesLetras(fecha)
Dim Mes
Select Case Month(fecha)
 Case 1
  Mes = "Ene"
 Case 2
  Mes = "Feb"
 Case 3
  Mes = "Mar"
 Case 4
  Mes = "Abr"
 Case 5
  Mes = "May"
 Case 6
  Mes = "Jun"
 Case 7
  Mes = "Jul"
 Case 8
  Mes = "Ago"
 Case 9
  Mes = "Sep"
 Case 10
  Mes = "Oct"
 Case 11
  Mes = "Nov"
 Case 12
  Mes = "Dic"
End Select
MesLetras = Day(fecha) & "/" & Mes & "/" & Year(fecha)
End Function
%>

<% Function GetPath(cParam)
Dim cStringPath, dbGlo

Select Case cParam
Case "5"
	Set dbGlo=Connect.Execute("select GL_FORMULA from GLOBAL where GL_CODIGO = '" & cParam & "'")
	cStringPath=dbGlo("GL_FORMULA")
Case Else
	Set dbGlo=Connect.Execute("select GL_FORMULA from GLOBAL where GL_CODIGO = '2'")
	cStringPath=dbGlo("GL_FORMULA")
End Select
GetPath=cStringPath
End Function %>

<%
   Sub IniciaUserDef( oUserName, oClave, oEncriptado )
     set LoginXML = Server.CreateObject("Portal.dmServerPortal")
  	  strLogin = LoginXML.UsuarioLogin("<RAIZ><PARAMETROS><US_CORTO>"+oUserName+"</US_CORTO><US_PASSWRD>"+oClave+"</US_PASSWRD><ENCRIPTADO>"+oEncriptado+"</ENCRIPTADO></PARAMETROS></RAIZ>")					
	  set xmlLogin=  server.CreateObject("Msxml2.DOMDocument.4.0")
	  xmlLogin.loadXML( strLogin )
		
	  Session("us_codigo") = xmlLogin.documentelement.childNodes(0).text	
	  Session("Pagina") = xmlLogin.documentelement.childNodes(1).text
	  Session("Nombre")= xmlLogin.documentelement.childNodes(2).text
			
     Session("cb_codigo") = xmlLogin.documentelement.childNodes(6).text
     Session("cm_empresa") = xmlLogin.documentelement.childNodes(7).text
 
  end Sub
%>


