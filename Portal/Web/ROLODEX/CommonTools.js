function ComboSubmit( oCombo, oControl, oForma )
{
  var sOld;
  sOld = oControl.value;
  if ( sOld != oCombo.value )
  {
    oControl.value = oCombo.value;
    oCombo.value = sOld;
    oForma.submit();
  }
}

function MenuTemp( oCombo, oControl )
{
  oControl.value = oCombo.value;
}

function CheckVacio( oControl )
{
  if ( oControl.value != '' )
  {
    return true;
  }
  else
  {
    alert( 'El Campo ' + oControl.name + ' No Puede Quedar Vacio.' );
    oControl.focus();
    return false;
  }
}

function ValidaTarjeta( oForma )
{
  return CheckVacio( oForma.EMPRESA ) && CheckVacio( oForma.NOMBRES ) && CheckVacio( oForma.TELEFONOS )
}

function deshacerCambios( oForma ) 
{ 
 return true;
}

function Borra( oForma )
{
  if (confirm( 'Seguro de Borrar Tarjeta ?' )) { 
   oForma.ACCION.value = oForma.BORRAR.value;
   oForma.submit();
  } else {
   return false;
  }
}

function LlamaEdicion( oForma )
{
 sPalabra = oForma.PALABRA.value;
 location.href='Roledit.asp?PALABRA=' + sPalabra + '&BTNAGREGAR=' + oForma.BTNAGREGAR.value + '&ACCION=Agregar&PAGINA=' + oForma.PAGINA.value;
 sPalabra = '';
}

function Regresa( oForma )
{
 location.href='javascript:window.history.back()';
}

function ValidaForma( oForma )
{
	if ( CheckVacio( oForma.CLAVEACTUAL ) && CheckVacio( oForma.CLAVENUEVA ) && CheckVacio( oForma.CLAVECONF ) )
	{
		if ( oForma.CLAVENUEVA.value != oForma.CLAVECONF.value )
		{
    		alert( 'Las Claves no son iguales. La Nueva Clave debe ser igual a la Confirmación' );
    		oForma.CLAVENUEVA.focus();
    		return false;
		}
		else
		{
			return true;
		}
	}
	else
	{
  		return false;
  	}
}

function PonerFocus( oControl )
{
	oControl.focus();
}

function ShowInicio()
{
	document.frminicio.submit();
}

function ValidaLogin( oForma )
{
 return CheckVacio( oForma.Usuario ) && CheckVacio( oForma.Clave )
}

function isLetter(theChar)
{
   var charArray = new Array( 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
                              'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
                              'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z')
   for (j = 0; j < charArray.length; j++)
       if (theChar == charArray[j])
          return true;
   return false;
}

function isDigit(theDigit)
{
   var digitArray = new Array('0','1','2','3','4','5','6','7','8','9')
   for (j = 0; j < digitArray.length; j++)
       if (theDigit == digitArray[j])
          return true;
   return false;
}

function ValidaCaracteresPassword( oForma )
{
   var sClave = new String( oForma.Clave.value );
   var theChar;
   var iDigitos = 0;
   var iLetras  = 0;
   var PasswordLetras = oForma.MinLetras.value;
   var PasswordDigitos = oForma.MinDigitos.value;

   if ( ( PasswordLetras > 0 ) || ( PasswordDigitos > 0 ) )
   {
      for (var i = 0; i < sClave.length; i++)
      {
         theChar = sClave.charAt(i)
         if ( isDigit(theChar) )
         {
            iDigitos = iDigitos + 1;
         }
         else
         {
            if ( isLetter(theChar) )
            {
               iLetras = iLetras + 1;
            }
         }
      }
      if ( ( PasswordDigitos > 0 ) && ( iDigitos < PasswordDigitos ) )
      {
         alert( 'Clave De Acceso Debe Incluir Por Lo Menos ' + PasswordDigitos + ' Dígitos' );
         oForma.Clave.focus();
         return false;
      }
      else
      {
         if ( ( PasswordLetras > 0 ) && ( iLetras < PasswordLetras ) )
         {
            alert( 'Clave De Acceso Debe Incluir Por Lo Menos ' + PasswordLetras + ' Letras' );
            oForma.Clave.focus();
            return false;
         }
         else
         {
            return true;
         }
      }
   }
   else
   {
      return true;
   }
}

function ValidaCambioClave( oForma )
{
     if ( CheckVacio( oForma.Clave ) && CheckVacio( oForma.Confirmacion ) )
     {
        if ( oForma.Clave.value.length >= oForma.MinLongitud.value )
        {
           if ( ValidaCaracteresPassword( oForma ) )
           {
              if ( oForma.Clave.value == oForma.Confirmacion.value )
              {
                 return true;
              }
              else
              {
                 alert( 'Las Claves no son Iguales. La Nueva Clave debe ser igual a la Confirmación' );
                 oForma.Clave.focus();
                 return false;
              }
           }
           else
           {
              return false;
           }
           return true;
        }
        else
        {
           alert( 'Clave De Acceso Debe Tener Por Lo Menos ' + oForma.MinLongitud.value + ' Caracteres' );
    		 oForma.Clave.focus();
    		 return false;
        }
     }
     else
     {
          return false;
     }
}

