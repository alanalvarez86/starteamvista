<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>
	<xsl:template match="/">
	<xsl:element name="link">
		<xsl:attribute name="rel">stylesheet</xsl:attribute>
		<xsl:attribute name="href">../Portal.css</xsl:attribute>
		<xsl:attribute name="type">text/css</xsl:attribute>
	</xsl:element>
	<xsl:element name="body">
		<xsl:attribute name="vLink">#000000</xsl:attribute>
		<xsl:attribute name="aLink">#000000</xsl:attribute>
		<xsl:attribute name="link">#000000</xsl:attribute>
		<xsl:attribute name="bgColor">#FFFFFF</xsl:attribute>						
		<center>
			<xsl:element name="font">
				<xsl:attribute name="Color">#FF0000</xsl:attribute>			
				<xsl:attribute name="face">Verdana</xsl:attribute>				
				<xsl:element name="h2">��� Se Encontraron Errores !!!</xsl:element>
			</xsl:element>
			<!--<xsl:element name="br"/>-->
			<h3><font face="Verdana">Mensajes de Error</font></h3>
			<table width="70%" bgcolor="#E6E4E4" border="1" bordercolor="#acacac" bordercolorlight="#C5C3C3" bordercolordark="#ffffff" cellspacing="2" cellpadding="2">
				<xsl:for-each select="RAIZ/ERRORES">
					<tr align="center">
						 <td><font class="subtitulo">				
								<xsl:value-of select="ERROR"/>
						 </font></td></tr>						
				</xsl:for-each>
			</table>
			<xsl:element name="br"/>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr><td align="left"><a href="javascript:window.history.back()"><img src="../IMAGENES/back.gif" border="0" alt="P�gina Anterior" width="79" height="30"/></a></td>
   		  			<td align="Right"><a href="../inicio.asp"><img src="../IMAGENES/inicio.gif" border="0" alt="Inicio" width="56" height="21"/></a></td>
	     		</tr>
			</table>
  		</center>
	</xsl:element>
</xsl:template>
</xsl:stylesheet>