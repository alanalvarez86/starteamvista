<%@ Language=VBScript %>
<%option explicit%>
<!-- #include file="Errores.asp" -->
<%
	Response.Expires=0
	Response.Buffer=false
	
	'Declaramos Variables para Iniciar el COmponente y recibir el XML generado
	dim InicioXML	
	dim strInicio
	dim strLogin
	dim xmlInicio
	dim xmlLogin
	dim docXSL
	dim dFecha
	dim strUsuario
	dim Nodo
	dim sMes

	'Declaramos Variables para Iniciar el COmponente y recibir el XML generado
	set InicioXML = Server.CreateObject("Portal.dmServerPortal")
	
	sMes = Month(Date())
	
	if (sMes < 10 ) then
	   sMes = "0" & Month(Date())
	end if
	
	dFecha = year(Date()) & sMes & day(Date())
	
	strInicio = InicioXML.GetInicio("<	RAIZ><PARAMETROS><USUARIO>"+ Session("us_codigo") +"</USUARIO><PAGINA>"+ Session("Pagina") +"</PAGINA><FECHAINI>"+ dFecha +"</FECHAINI></PARAMETROS></RAIZ>")
    'Response.write(dFecha)    
    
	'Para Transformar el XML con el XSL tenemos que pasarl el string recibido  a documento XML
	set xmlInicio =  server.CreateObject("Msxml2.DOMDocument.4.0")
	xmlInicio.loadXML(strInicio)	
	
	set Nodo = xmlInicio.documentElement.selectNodes( "//PAGINA/COLUMNA" )
	if ( Nodo.Length = 0 ) then
		strInicio = InicioXML.GetInicio("<	RAIZ><PARAMETROS><USUARIO>"+Application("UserDef")+"</USUARIO><PAGINA>1</PAGINA><FECHAINI>"+ dFecha +"</FECHAINI></PARAMETROS></RAIZ>")
       set xmlInicio =  server.CreateObject("Msxml2.DOMDocument.4.0")
		xmlInicio.loadXML(strInicio)	
		'Response.Write( " NO HAY " )
	end if
	
	'Load the XSL para mostrar en pantalla los Combos
	set docXSL = Server.CreateObject("Msxml2.DOMDocument.4.0")
	docXSL.async = false
	docXSL.load(Server.MapPath("inicio.xsl"))	
	
%>	
<%
Function FechaLarga(fecha)
Dim Mes, StrDia, oYear
Select Case Month(fecha)
 Case 1
  Mes = "Enero"
 Case 2
  Mes = "Febrero"
 Case 3
  Mes = "Marzo"
 Case 4
  Mes = "Abril"
 Case 5
  Mes = "Mayo"
 Case 6
  Mes = "Junio"
 Case 7
  Mes = "Julio"
 Case 8
  Mes = "Agosto"
 Case 9
  Mes = "Septiembre"
 Case 10
  Mes = "Octubre"
 Case 11
  Mes = "Noviembre"
 Case 12
  Mes = "Diciembre"
End Select

Select Case WeekDay(fecha)
Case 1
	StrDia="Domingo"
Case 2
	StrDia="Lunes"
Case 3
	StrDia="Martes"
Case 4
	StrDia="Miercoles"
Case 5
	StrDia="Jueves"
Case 6
	StrDia="Viernes"
Case 7
	StrDia="Sabado"
End Select
oYear = Year(fecha)
Application("Year")=oYear
Application("strMes")=Mes
FechaLarga= StrDia & " " & Day(fecha) & " de " & Mes & " de " & oYear
End Function
%>

<html>
<head>
	<meta http-equiv="Refresh" content="<%=Application("Tiempo")%>"; URL="inicio.asp">
</head>

<form name="frminicio" id="frminicio">
	<center>
    <table width="90%">
   		<tr>
   			<td align="right">
   				<font face="tahoma" size="1" color="red">
					<b><%=FechaLarga(Date())%></b>
			</td>
		</tr>
	</table>
		<%ChecaRespuesta strInicio, "Inicio.xsl", "Errores.xsl"%>	
	</center>
</form>
</html>