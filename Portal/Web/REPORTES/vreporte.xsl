<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>
	<xsl:template match="/">
	<xsl:element name="link">
		<xsl:attribute name="rel">stylesheet</xsl:attribute>
		<xsl:attribute name="href">../Portal.css</xsl:attribute>
		<xsl:attribute name="type">text/css</xsl:attribute>
	</xsl:element>
	<xsl:element name="body">
		<center>
			<table border="0" cellpadding="0" cellspacing="0" width="90%">
  			<tr>
  				<td>
  					<p align="left"/>
					<table width="100%">
						<tr>
							<td align="left">
								<font face="Verdana" size="2">
									<b>
										<xsl:value-of select="RAIZ/GLOBAL/@G6"/>									
									</b>
									<xsl:element name="br"/>
									<xsl:value-of select="RAIZ/GLOBAL/@G1"/>
									<xsl:element name="br"/>
									<xsl:value-of select="RAIZ/REPORTAL/@RP_TITULO"/>, <xsl:value-of select="RAIZ/REPORTAL/@RP_DESCRIP"/>
									<xsl:element name="br"/>
									Generado El <xsl:value-of select="RAIZ/REPORTAL/@RP_FECHA"/><xsl:text> </xsl:text><xsl:value-of select="RAIZ/REPORTAL/@RP_HORA"/>
									<xsl:element name="br"/>
								</font>
							</td>
							<xsl:variable name="longitud"><xsl:value-of select="string-length(RAIZ/GLOBAL/@G11)"/></xsl:variable>
							<xsl:if test="$longitud!=0">
								<td align="right" valign="top"><font size="1" face="Verdana"><b>
									<xsl:element name="img">
										<xsl:attribute name="src"><xsl:value-of select="RAIZ/GLOBAL/@G11"/></xsl:attribute>
									</xsl:element>
									<xsl:element name="BR"/>
									</b>
									</font>
								</td>
							</xsl:if>					
						</tr>
					</table>
					<xsl:element name="p"/>

	<xsl:variable name="COLS"><xsl:value-of select="RAIZ/REPORTAL/@RP_COLS"/></xsl:variable>
	<table width="100%" border="1" cellpadding="2" cellspacing="2" bordercolor="#acacac" bordercolorlight="#C5C3C3" bordercolordark="#ffffff" bgcolor="#E6E4E4">
		<xsl:for-each select="RAIZ/REP_DATA/LABELS">	
		<tr>		
			<xsl:if test="$COLS>=1">
				<td align="center"><font size="2" face="Verdana"><b><xsl:value-of select="@RD_DATO1"/></b></font></td>
				<xsl:if test="$COLS>=2">
					<td align="center"><font size="2" face="Verdana"><b><xsl:value-of select="@RD_DATO2"/></b></font></td>
					<xsl:if test="$COLS>=3">
						<td align="center"><font size="2" face="Verdana"><b><xsl:value-of select="@RD_DATO3"/></b></font></td>
						<xsl:if test="$COLS>=4">						
							<td align="center"><font size="2" face="Verdana"><b><xsl:value-of select="@RD_DATO4"/></b></font></td>
							<xsl:if test="$COLS>=5">
								<td align="center"><font size="2" face="Verdana"><b><xsl:value-of select="@RD_DATO5"/></b></font></td>
								<xsl:if test="$COLS>=6">
									<td align="center"><font size="2" face="Verdana"><b><xsl:value-of select="@RD_DATO6"/></b></font></td>
									<xsl:if test="$COLS>=7">
										<td align="center"><font size="2" face="Verdana"><b><xsl:value-of select="@RD_DATO7"/></b></font></td>
										<xsl:if test="$COLS>=8">
											<td align="center"><font size="2" face="Verdana"><b><xsl:value-of select="@RD_DATO8"/></b></font></td>
											<xsl:if test="$COLS>=9">
												<td align="center"><font size="2" face="Verdana"><b><xsl:value-of select="@RD_DATO9"/></b></font></td>
												<xsl:if test="$COLS>=10">
													<td align="center"><font size="2" face="Verdana"><b><xsl:value-of select="@RD_DATO10"/></b></font></td>
												</xsl:if>												
											</xsl:if>											
										</xsl:if>
									</xsl:if>									
								</xsl:if>								
							</xsl:if>							
						</xsl:if>
					</xsl:if>					
				</xsl:if>								
			</xsl:if>		
		</tr>
		</xsl:for-each>
	
		<xsl:variable name="COLS2"><xsl:value-of select="RAIZ/REPORTAL/@RP_COLS"/></xsl:variable>

		<xsl:variable name="tipo1"><xsl:value-of select="RAIZ/REP_COLS/@RC_TIPO1"/></xsl:variable>					
		<xsl:variable name="tipo2"><xsl:value-of select="RAIZ/REP_COLS/@RC_TIPO2"/></xsl:variable>					
		<xsl:variable name="tipo3"><xsl:value-of select="RAIZ/REP_COLS/@RC_TIPO3"/></xsl:variable>					
		<xsl:variable name="tipo4"><xsl:value-of select="RAIZ/REP_COLS/@RC_TIPO4"/></xsl:variable>					
		<xsl:variable name="tipo5"><xsl:value-of select="RAIZ/REP_COLS/@RC_TIPO5"/></xsl:variable>					
		<xsl:variable name="tipo6"><xsl:value-of select="RAIZ/REP_COLS/@RC_TIPO6"/></xsl:variable>					
		<xsl:variable name="tipo7"><xsl:value-of select="RAIZ/REP_COLS/@RC_TIPO7"/></xsl:variable>					
		<xsl:variable name="tipo8"><xsl:value-of select="RAIZ/REP_COLS/@RC_TIPO8"/></xsl:variable>					
		<xsl:variable name="tipo9"><xsl:value-of select="RAIZ/REP_COLS/@RC_TIPO9"/></xsl:variable>					
		<xsl:variable name="tipo10"><xsl:value-of select="RAIZ/REP_COLS/@RC_TIPO10"/></xsl:variable>

		<xsl:variable name="varcorreo1"><xsl:value-of select="RAIZ/REP_DATA/ROWS/ROW/@RD_DATO1"/></xsl:variable>
		<xsl:variable name="varcorreo2"><xsl:value-of select="RAIZ/REP_DATA/ROWS/ROW/@RD_DATO2"/></xsl:variable>
		<xsl:variable name="varcorreo3"><xsl:value-of select="RAIZ/REP_DATA/ROWS/ROW/@RD_DATO3"/></xsl:variable>
		<xsl:variable name="varcorreo4"><xsl:value-of select="RAIZ/REP_DATA/ROWS/ROW/@RD_DATO4"/></xsl:variable>
		<xsl:variable name="varcorreo5"><xsl:value-of select="RAIZ/REP_DATA/ROWS/ROW/@RD_DATO5"/></xsl:variable>
		<xsl:variable name="varcorreo6"><xsl:value-of select="RAIZ/REP_DATA/ROWS/ROW/@RD_DATO6"/></xsl:variable>
		<xsl:variable name="varcorreo7"><xsl:value-of select="RAIZ/REP_DATA/ROWS/ROW/@RD_DATO7"/></xsl:variable>
		<xsl:variable name="varcorreo8"><xsl:value-of select="RAIZ/REP_DATA/ROWS/ROW/@RD_DATO8"/></xsl:variable>
		<xsl:variable name="varcorreo9"><xsl:value-of select="RAIZ/REP_DATA/ROWS/ROW/@RD_DATO9"/></xsl:variable>
		<xsl:variable name="varcorreo10"><xsl:value-of select="RAIZ/REP_DATA/ROWS/ROW/@RD_DATO10"/></xsl:variable>
							
		<xsl:variable name="correo1"><xsl:value-of select="substring-after($varcorreo1, ':')"/></xsl:variable>
		<xsl:variable name="correo2"><xsl:value-of select="substring-after($varcorreo2, ':')"/></xsl:variable>
		<xsl:variable name="correo3"><xsl:value-of select="substring-after($varcorreo3, ':')"/></xsl:variable>				
		<xsl:variable name="correo4"><xsl:value-of select="substring-after($varcorreo4, ':')"/></xsl:variable>
		<xsl:variable name="correo5"><xsl:value-of select="substring-after($varcorreo5, ':')"/></xsl:variable>
		<xsl:variable name="correo6"><xsl:value-of select="substring-after($varcorreo6, ':')"/></xsl:variable>
		<xsl:variable name="correo7"><xsl:value-of select="substring-after($varcorreo7, ':')"/></xsl:variable>
		<xsl:variable name="correo8"><xsl:value-of select="substring-after($varcorreo8, ':')"/></xsl:variable>
		<xsl:variable name="correo9"><xsl:value-of select="substring-after($varcorreo9, ':')"/></xsl:variable>
		<xsl:variable name="correo10"><xsl:value-of select="substring-after($varcorreo10, ':')"/></xsl:variable>
			
		<xsl:for-each select="RAIZ/REP_DATA/ROWS/ROW">	
			<tr bgcolor="#ffffff">
				<xsl:if test="$COLS2>=1">
					<xsl:choose>
						<xsl:when test="$tipo1=1 or $tipo1=2">
							<td align="right"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO1"/></font></td>
						</xsl:when>
						<xsl:when test="$tipo1=3">
							<td align="center"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO1"/></font></td>						
						</xsl:when>
						<xsl:when test="$tipo1=7">
							<td align="left"><font size="1" face="Verdana">
								<xsl:element name="a">
									<xsl:attribute name="href"><xsl:value-of select="@RD_DATO1"/></xsl:attribute><xsl:value-of select="$correo1"/>
								</xsl:element></font>
							</td>						
						</xsl:when>
						<xsl:when test="$tipo1=8">
							<td align="left"><font size="1" face="Verdana">
								<xsl:element name="a">
									<xsl:attribute name="href"><xsl:value-of select="@RD_DATO1"/></xsl:attribute><xsl:value-of select="@RD_DATO1"/>
								</xsl:element></font>
							</td>						
						</xsl:when>						
						<xsl:otherwise>
							<td align="left"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO1"/></font></td>												
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
				<xsl:if test="$COLS2>=2">
					<xsl:choose>
						<xsl:when test="$tipo2=1 or $tipo2=2">
							<td align="right"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO2"/></font></td>
						</xsl:when>
						<xsl:when test="$tipo2=3">
							<td align="center"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO2"/></font></td>						
						</xsl:when>
						<xsl:when test="$tipo2=7">
							<td align="left"><font size="1" face="Verdana">
								<xsl:element name="a">
									<xsl:attribute name="href"><xsl:value-of select="@RD_DATO2"/></xsl:attribute><xsl:value-of select="$correo2"/>
								</xsl:element></font>
							</td>						
						</xsl:when>
						<xsl:when test="$tipo2=8">
							<td align="left"><font size="1" face="Verdana">
								<xsl:element name="a">
									<xsl:attribute name="href"><xsl:value-of select="@RD_DATO2"/></xsl:attribute><xsl:value-of select="@RD_DATO2"/>
								</xsl:element></font>
							</td>						
						</xsl:when>
						
						<xsl:otherwise>
							<td align="left"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO2"/></font></td>												
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>		
				<xsl:if test="$COLS2>=3">
					<xsl:choose>
						<xsl:when test="$tipo3=1 or $tipo3=2">
							<td align="right"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO3"/></font></td>
						</xsl:when>
						<xsl:when test="$tipo2=3">
							<td align="center"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO3"/></font></td>						
						</xsl:when>
						<xsl:when test="$tipo3=7">
							<td align="left"><font size="1" face="Verdana">
								<xsl:element name="a">
									<xsl:attribute name="href"><xsl:value-of select="@RD_DATO3"/></xsl:attribute><xsl:value-of select="$correo3"/>
								</xsl:element></font>
							</td>						
						</xsl:when>
						<xsl:when test="$tipo3=8">
							<td align="left"><font size="1" face="Verdana">
								<xsl:element name="a">
									<xsl:attribute name="href"><xsl:value-of select="@RD_DATO3"/></xsl:attribute><xsl:value-of select="@RD_DATO3"/>
								</xsl:element></font>
							</td>						
						</xsl:when>						
						<xsl:otherwise>
							<td align="left"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO3"/></font></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
				<xsl:if test="$COLS2>=4">
					<xsl:choose>
						<xsl:when test="$tipo4=1 or $tipo4=2">
							<td align="right"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO4"/></font></td>
						</xsl:when>
						<xsl:when test="$tipo4=3">
							<td align="center"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO4"/></font></td>
						</xsl:when>
						<xsl:when test="$tipo4=7">
							<td align="left"><font size="1" face="Verdana">
								<xsl:element name="a">
									<xsl:attribute name="href"><xsl:value-of select="@RD_DATO4"/></xsl:attribute><xsl:value-of select="$correo4"/>
								</xsl:element>
								</font>
							</td>						
						</xsl:when>
						<xsl:when test="$tipo4=8">
							<td align="left"><font size="1" face="Verdana">
								<xsl:element name="a">
									<xsl:attribute name="href"><xsl:value-of select="@RD_DATO4"/></xsl:attribute><xsl:value-of select="@RD_DATO4"/>								</xsl:element>
								</font>
							</td>						
						</xsl:when>						
						<xsl:otherwise>
							<td align="left"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO4"/></font></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
				<xsl:if test="$COLS2>=5">
					<xsl:choose>
						<xsl:when test="$tipo5=1 or $tipo5=2">
							<td align="right"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO5"/></font></td>
						</xsl:when>
						<xsl:when test="$tipo5=3">
							<td align="center"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO5"/></font></td>						
						</xsl:when>
						<xsl:when test="$tipo5=7">
							<td align="left"><font size="1" face="Verdana">
								<xsl:element name="a">
									<xsl:attribute name="href"><xsl:value-of select="@RD_DATO5"/></xsl:attribute><xsl:value-of select="correo5"/>
								</xsl:element></font>
							</td>						
						</xsl:when>
						<xsl:when test="$tipo5=8">
							<td align="left"><font size="1" face="Verdana">
								<xsl:element name="a">
									<xsl:attribute name="href"><xsl:value-of select="@RD_DATO5"/></xsl:attribute><xsl:value-of select="@RD_DATO5"/>
								</xsl:element></font>
							</td>						
						</xsl:when>						
						<xsl:otherwise>
							<td align="left"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO5"/></font></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
				<xsl:if test="$COLS2>=6">
					<xsl:choose>
						<xsl:when test="$tipo6=1 or $tipo6=2">
							<td align="right"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO6"/></font></td>
						</xsl:when>
						<xsl:when test="$tipo6=3">
							<td align="center"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO6"/></font></td>						
						</xsl:when>
						<xsl:when test="$tipo6=7">
							<td align="left"><font size="1" face="Verdana">
								<xsl:element name="a">
									<xsl:attribute name="href"><xsl:value-of select="@RD_DATO6"/></xsl:attribute><xsl:value-of select="$correo6"/>
								</xsl:element></font>
							</td>						
						</xsl:when>
						<xsl:when test="$tipo6=8">
							<td align="left"><font size="1" face="Verdana">
								<xsl:element name="a">
									<xsl:attribute name="href"><xsl:value-of select="@RD_DATO6"/></xsl:attribute><xsl:value-of select="@RD_DATO6"/>
								</xsl:element></font>
							</td>						
						</xsl:when>						
						<xsl:otherwise>
							<td align="left"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO6"/></font></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
				<xsl:if test="$COLS2>=7">
					<xsl:choose>
						<xsl:when test="$tipo7=1 or $tipo7=2">
							<td align="right"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO7"/></font></td>
						</xsl:when>
						<xsl:when test="$tipo7=3">
							<td align="center"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO7"/></font></td>
						</xsl:when>
						<xsl:when test="$tipo7=7">
							<td align="left"><font size="1" face="Verdana">
								<xsl:element name="a">
									<xsl:attribute name="href"><xsl:value-of select="@RD_DATO7"/></xsl:attribute><xsl:value-of select="$correo7"/>
								</xsl:element></font>
							</td>						
						</xsl:when>
						<xsl:when test="$tipo7=8">
							<td align="left"><font size="1" face="Verdana">
								<xsl:element name="a">
									<xsl:attribute name="href"><xsl:value-of select="@RD_DATO7"/></xsl:attribute><xsl:value-of select="@RD_DATO7"/>
								</xsl:element></font>
							</td>						
						</xsl:when>						
						<xsl:otherwise>
							<td align="left"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO7"/></font></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
				<xsl:if test="$COLS2>=8">
					<xsl:choose>
						<xsl:when test="$tipo8=1 or $tipo8=2">
							<td align="right"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO8"/></font></td>
						</xsl:when>
						<xsl:when test="$tipo8=3">
							<td align="center"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO8"/></font></td>
						</xsl:when>
						<xsl:when test="$tipo8=7">
							<td align="left"><font size="1" face="Verdana">
								<xsl:element name="a">
									<xsl:attribute name="href"><xsl:value-of select="@RD_DATO8"/></xsl:attribute><xsl:value-of select="correo8"/>
								</xsl:element></font>
							</td>						
						</xsl:when>
						<xsl:when test="$tipo8=8">
							<td align="left"><font size="1" face="Verdana">
								<xsl:element name="a">
									<xsl:attribute name="href"><xsl:value-of select="@RD_DATO8"/></xsl:attribute><xsl:value-of select="@RD_DATO8"/>
								</xsl:element></font>
							</td>						
						</xsl:when>						
						<xsl:otherwise>
							<td align="left"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO8"/></font></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
				<xsl:if test="$COLS2>=9">
					<xsl:choose>
						<xsl:when test="$tipo9=1 or $tipo9=2">
							<td align="right"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO9"/></font></td>
						</xsl:when>
						<xsl:when test="$tipo9=3">
							<td align="center"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO9"/></font></td>						
						</xsl:when>
						<xsl:when test="$tipo9=7">
							<td align="left"><font size="1" face="Verdana">
								<xsl:element name="a">
									<xsl:attribute name="href"><xsl:value-of select="@RD_DATO9"/></xsl:attribute><xsl:value-of select="$correo9"/>
								</xsl:element></font>
							</td>						
						</xsl:when>
						<xsl:when test="$tipo9=8">
							<td align="left"><font size="1" face="Verdana">
								<xsl:element name="a">
									<xsl:attribute name="href"><xsl:value-of select="@RD_DATO9"/></xsl:attribute><xsl:value-of select="@RD_DATO9"/>
								</xsl:element></font>
							</td>						
						</xsl:when>						
						<xsl:otherwise>
							<td align="left"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO9"/></font></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
				<xsl:if test="$COLS2>=10">
					<xsl:choose>
						<xsl:when test="$tipo10=1 or $tipo10=2">
							<td align="right"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO10"/></font></td>
						</xsl:when>
						<xsl:when test="$tipo10=3">
							<td align="center"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO10"/></font></td>						
						</xsl:when>
						<xsl:when test="$tipo10=7">
							<td align="left"><font size="1" face="Verdana">
								<xsl:element name="a">
									<xsl:attribute name="href"><xsl:value-of select="@RD_DATO10"/></xsl:attribute><xsl:value-of select="$correo10"/>
								</xsl:element></font>
							</td>						
						</xsl:when>
						<xsl:when test="$tipo10=8">
							<td align="left"><font size="1" face="Verdana">
								<xsl:element name="a">
									<xsl:attribute name="href"><xsl:value-of select="@RD_DATO10"/></xsl:attribute><xsl:value-of select="@RD_DATO10"/>
								</xsl:element></font>
							</td>						
						</xsl:when>						
						<xsl:otherwise>
							<td align="left"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO10"/></font></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>		
			</tr>
		</xsl:for-each>
</table>
<p align="center"/>
<center>
<xsl:element name="br"/>
<table width="90%" border="0" cellspacing="0" cellpadding="0">
 	<tr>
 		<td align="left"><a href="javascript:window.history.back()"><img src="../IMAGENES/back.gif" border="0" alt="P�gina Anterior"/></a></td>
     	<td align="Right"><a href="../inicio.asp"><img src="../imagenes/inicio.gif" border="0" alt="Inicio"/></a></td>
   </tr>
</table>
<table border="0" cellpadding="2" width="90%">
   			<tr>
   				<td width="100%" align="center" valign="bottom">
    				<xsl:element name="hr"/>
					<xsl:element name="img">
						<xsl:attribute name="border">0</xsl:attribute>
						<!--<xsl:attribute name="src">../imagenes/tress_small.gif</xsl:attribute>-->
						<xsl:attribute name="src"><xsl:value-of select="RAIZ/GLOBAL/@G11"/></xsl:attribute>
					</xsl:element>
				</td>			 	 				
			</tr>
			<tr>
				<td width="100%" align="center" valign="bottom">	
   					<font face="Arial" size="1">Portal Empresarial
   					<xsl:element name="br"/>
   							<xsl:value-of select="RAIZ/GLOBAL/@G6"/><!--Grupo Tress Internacional-->
   					<xsl:element name="a">
   						<xsl:attribute name="href"><xsl:value-of select="RAIZ/GLOBAL/@G19"/></xsl:attribute>
 						<xsl:attribute name="target">"_new"><xsl:value-of select="RAIZ/GLOBAL/@G19"/></xsl:attribute>
 						<xsl:element name="br"/>
 						<xsl:value-of select="RAIZ/GLOBAL/@G19"/> 	
 						<!--<xsl:attribute name="href">http://www.tress.com.mx</xsl:attribute>
 						<xsl:attribute name="target">"_new">http://www.tress.com.mx</xsl:attribute>
 						<xsl:element name="br"/>
 						http://www.tress.com.mx-->
 					</xsl:element>		
   					</font>
   				</td>
   			</tr>
</table>

</center>
  </td>
</tr>
</table>

</center>
</xsl:element>
</xsl:template>
</xsl:stylesheet>