unit LaborTress_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 4/3/2006 5:09:32 PM from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3Win_20\Portal\LaborWeb\LaborTress.tlb (1)
// LIBID: {F44B9DE4-22B5-45D6-B686-1C1DF5AF90CB}
// LCID: 0
// Helpfile: 
// HelpString: Tress Web Labor
// DepndLst: 
//   (1) v1.0 Midas, (C:\WINDOWS\system32\midas.dll)
//   (2) v2.0 stdole, (C:\WINDOWS\system32\STDOLE2.TLB)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, Midas, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  LaborTressMajorVersion = 1;
  LaborTressMinorVersion = 0;

  LIBID_LaborTress: TGUID = '{F44B9DE4-22B5-45D6-B686-1C1DF5AF90CB}';

  IID_IdmServerLaborTress: TGUID = '{F8819BFB-BF6A-4526-B9D6-B4AE37797CE9}';
  CLASS_dmServerLaborTress: TGUID = '{8A4CF790-715C-4290-8749-9AB4EB9D46A7}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerLaborTress = interface;
  IdmServerLaborTressDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerLaborTress = IdmServerLaborTress;


// *********************************************************************//
// Interface: IdmServerLaborTress
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {F8819BFB-BF6A-4526-B9D6-B4AE37797CE9}
// *********************************************************************//
  IdmServerLaborTress = interface(IAppServer)
    ['{F8819BFB-BF6A-4526-B9D6-B4AE37797CE9}']
    function GrabaLaborDia(const Datos: WideString; out Mensaje: WideString): WordBool; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerLaborTressDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {F8819BFB-BF6A-4526-B9D6-B4AE37797CE9}
// *********************************************************************//
  IdmServerLaborTressDisp = dispinterface
    ['{F8819BFB-BF6A-4526-B9D6-B4AE37797CE9}']
    function GrabaLaborDia(const Datos: WideString; out Mensaje: WideString): WordBool; dispid 6;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer; 
                             out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                           Options: Integer; const CommandText: WideString; var Params: OleVariant; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerLaborTress provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerLaborTress exposed by              
// the CoClass dmServerLaborTress. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerLaborTress = class
    class function Create: IdmServerLaborTress;
    class function CreateRemote(const MachineName: string): IdmServerLaborTress;
  end;

implementation

uses ComObj;

class function CodmServerLaborTress.Create: IdmServerLaborTress;
begin
  Result := CreateComObject(CLASS_dmServerLaborTress) as IdmServerLaborTress;
end;

class function CodmServerLaborTress.CreateRemote(const MachineName: string): IdmServerLaborTress;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerLaborTress) as IdmServerLaborTress;
end;

end.
