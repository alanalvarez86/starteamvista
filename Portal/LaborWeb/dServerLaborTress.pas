unit DServerLaborTress;

interface

uses Windows, Messages, SysUtils, Classes, Controls, Forms,
     Dialogs, ComServ, ComObj, VCLCom, StdVcl, BdeMts, DataBkr,
     {$ifndef DOS_CAPAS}LaborTress_TLB,{$endif}
     MtsRdm, Mtx;

type
  TdmServerLaborTress = class(TMtsDataModule, IdmServerLaborTress)
    procedure MtsDataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    {$ifdef DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
    {$endif}
    function GrabaLaborDia(const Datos: WideString; out Mensaje: WideString): WordBool; safecall;
  end;

var
  dmServerLaborTress: TdmServerLaborTress;

implementation

uses DLaborTress;

{$R *.DFM}

{ ***** TdmServerLaborTress ***** }

class procedure TdmServerLaborTress.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

procedure TdmServerLaborTress.MtsDataModuleDestroy(Sender: TObject);
begin
end;

{$ifdef DOS_CAPAS}
procedure TdmServerLaborTress.CierraEmpresa;
begin
end;
{$endif}

function TdmServerLaborTress.GrabaLaborDia(const Datos: WideString; out Mensaje: WideString): WordBool;
var
   FLaborTress: TdmLaborTress;
begin
     Result := TRUE;
     Mensaje := '';
     FLaborTress := TdmLaborTress.Create( Self );
     try
        Result := FLaborTress.GrabaDia( Datos, Mensaje );
     finally
            FreeAndNil( FLaborTress );
     end;
     SetComplete;
end;

{$ifndef DOS_CAPAS}
initialization
  TComponentFactory.Create(ComServer, TdmServerLaborTress, Class_dmServerLaborTress, ciMultiInstance, tmApartment);
{$endif}
end.