unit FLaborTressTest;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DB, Grids, DBGrids, DBClient, ZetaClientDataSet;

type
  TTestLaborTress = class(TForm)
    Button1: TButton;
    ZetaClientDataSet1: TZetaClientDataSet;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TestLaborTress: TTestLaborTress;

implementation

uses DLaborTress,
     ZetaCommonTools;

{$R *.dfm}

procedure TTestLaborTress.Button1Click(Sender: TObject);
var
   FDatos: TStrings;
   FLaborTress: TdmLaborTress;
   lOk: Boolean;
   Datos, Mensaje: WideString;
begin
     FDatos := TStringList.Create;
     try
        with FDatos do
        begin
             LoadFromFile( 'D:\3Win_20\Portal\LaborWeb\PruebaLabor.xml' );
             Datos := ZetaCommonTools.BorraCReturn( Text );
        end;
        FLaborTress := TdmLaborTress.Create( Self );
        try
           lOk := FLaborTress.GrabaDia( Datos, Mensaje );
           if lOk then
              ShowMessage( 'OK: ' + Mensaje )
           else
               ShowMessage( 'NOT OK: ' + Mensaje );
        finally
               FreeAndNil( FLaborTress );
        end;
     finally
            FreeAndNil( FDatos );
     end;
end;

end.
