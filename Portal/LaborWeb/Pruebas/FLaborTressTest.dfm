object TestLaborTress: TTestLaborTress
  Left = 285
  Top = 114
  Width = 787
  Height = 450
  Caption = 'Testing Of LaborTress'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 256
    Top = 176
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 0
    OnClick = Button1Click
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 272
    Width = 779
    Height = 144
    Align = alBottom
    DataSource = DataSource1
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object ZetaClientDataSet1: TZetaClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    Left = 376
    Top = 192
    Data = {
      AB0100009619E0BD01000000180000000C0002000000030000004B010943425F
      434F4449474F0400010000000000094F505F4E554D4245520100490000000100
      055749445448020002000A0009574F5F4E554D42455201004900000001000557
      494454480200020014000941525F434F4449474F010049000000010005574944
      5448020002000F000743425F4152454101004900000001000557494454480200
      020006000841555F4645434841040006000000000007574B5F5449504F040001
      00000000000A574B5F544D554552544F01004900000001000557494454480200
      0200030009574B5F5449454D504F080004000000000008574B5F4D4F445F3101
      0049000000010005574944544802000200010008574B5F4D4F445F3201004900
      0000010005574944544802000200010008574B5F4D4F445F3301004900000001
      00055749445448020002000100000000000000A5000000044445534108504F52
      54414C5632010003444553D02A0B000000000001000000000000003E40010001
      00010000000000A500000001000100010003444553D02A0B0002000000034F54
      520000000000003E40010001000100}
  end
  object DataSource1: TDataSource
    DataSet = ZetaClientDataSet1
    Left = 320
    Top = 240
  end
end
