unit FEditarPagina;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal, StdCtrls, Buttons, ExtCtrls, FCamposMgr;

type
  TEditarPagina = class(TZetaDlgModal)
    NoColLBL: TLabel;
    Label1: TLabel;
    PA_COLS: TComboBox;
    PA_TITULO: TEdit;
    procedure OKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FPagina: TPagina;
    procedure SetPagina(const Value: TPagina);
  public
    { Public declarations }
    property Pagina: TPagina read FPagina write SetPagina;
  end;

var
  EditarPagina: TEditarPagina;

function EditaPaginas( oValue: TPagina ): Boolean;

implementation

uses FPaginaCFG;

{$R *.DFM}

function EditaPaginas( oValue: TPagina ): Boolean;
begin
     if not Assigned( EditarPagina ) then
        EditarPagina := TEditarPagina.Create( Application );
     with EditarPagina do
     begin
          Pagina := oValue;
          ShowModal;
          Result := ( ModalResult = mrOk );
     end;
end;

{ TEditarPagina }

procedure TEditarPagina.FormShow(Sender: TObject);
begin
     inherited;
     ActiveControl := PA_TITULO;
end;

procedure TEditarPagina.SetPagina(const Value: TPagina);
begin
     FPagina := Value;
     With FPagina do
     begin
          PA_COLS.ItemIndex := Columnas - 1;
          PA_TITULO.Text := Titulo;
     end;
end;

procedure TEditarPagina.OKClick(Sender: TObject);
begin
     inherited;
     with FPagina do
     begin
          Columnas := PA_COLS.ItemIndex + 1;
          Titulo := PA_TITULO.Text;
     end;
end;

end.
