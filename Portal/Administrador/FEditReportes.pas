unit FEditReportes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ExtDlgs,
  ZetaKeyLookup, ZetaFecha, ZetaEdit, Mask, ZetaHora, ZetaDBTextBox,
  ComCtrls;

type
  TEditReportes = class(TBaseEdicion)
    Titulo_Pn: TPanel;
    Label1: TLabel;
    RP_FOLIO: TZetaDBTextBox;
    Label4: TLabel;
    RP_TITULO: TZetaDBEdit;
    PageControl: TPageControl;
    Generales_Ts: TTabSheet;
    Label8: TLabel;
    Label7: TLabel;
    RP_USUARIO: TZetaDBKeyLookup;
    RP_IMAGEN: TZetaDBEdit;
    RP_IMAGEN_Find: TSpeedButton;
    Descripcion_Ts: TTabSheet;
    RP_TIPO: TDBRadioGroup;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    RP_HORA: TZetaDBTextBox;
    RP_FECHA: TZetaDBTextBox;
    Label2: TLabel;
    Label5: TLabel;
    RP_COLS: TZetaDBTextBox;
    RP_ROWS: TZetaDBTextBox;
    Label6: TLabel;
    RP_DESCRIP: TDBMemo;
    Panel1: TPanel;
    Find_Html_Btn: TSpeedButton;
    DlgFileOpen: TOpenDialog;
    procedure FormCreate(Sender: TObject);
    procedure RP_IMAGEN_FindClick(Sender: TObject);
    procedure Find_Html_BtnClick(Sender: TObject);
  private
    { Private declarations }
  protected
  { Protected declarations }
  procedure Connect; override;
  procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  EditReportes: TEditReportes;

implementation
uses DCliente, DGlobal, ZetaBuscador, FDialogoImagenes;

{$R *.DFM}

{ TEditReportes }
procedure TEditReportes.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := RP_TITULO;
     //MA:Pendiente
     //HelpContext:= H60617_Tipo_contrato;
end;

procedure TEditReportes.Connect;
begin
     with dmCliente do
     begin
          cdsReportes.Conectar;
          DataSource.DataSet:= cdsReportes;
     end;
end;

procedure TEditReportes.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Folio', 'Reportes', 'RP_FOLIO', dmCliente.cdsReportes );
end;

procedure TEditReportes.RP_IMAGEN_FindClick(Sender: TObject);
begin
     with dmCliente do
     begin
          RutaImagenGlobal := Global.GetGlobalString( FindBtnImagen );
          with DialogoImagenes do
          begin
               LlamaDialogo(K_TIPO_IMAGEN,RP_IMAGEN.Text);
               if ( ModalResult = mrOk ) then
               begin
                    cdsReportes.Edit;
                    RP_IMAGEN.Text := ExtractFileName(ListaArchivos.FileName);
               end;
          end;
     end;
end;

procedure TEditReportes.Find_Html_BtnClick(Sender: TObject);
begin
     with dlgFileOpen do
     begin
          with dmCliente do
          begin
               InitialDir := RutaImagenGlobal;
               if Execute then
               begin
                    with RP_DESCRIP do
                    begin
                         Clear;
                         cdsReportes.Edit;
                         Lines.LoadFromFile(dlgFileOpen.FileName);
                    end;
               end;
          end;
     end;
end;

end.
