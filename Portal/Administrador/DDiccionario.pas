unit DDiccionario;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ZetaTipoEntidad;

type
  TdmDiccionario = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure CamposPorEntidad( const oEntidad: TipoEntidad; const lTodos: Boolean; oLista: TStrings );
    procedure SetLookupNames;
    procedure CambioGlobales;
  end;

var
   dmDiccionario: TdmDiccionario;

implementation

{ TdmDiccionario }

procedure TdmDiccionario.CambioGlobales;
begin
end;

procedure TdmDiccionario.CamposPorEntidad(const oEntidad: TipoEntidad;const lTodos: Boolean; oLista: TStrings);
begin
end;

procedure TdmDiccionario.SetLookupNames;
begin
end;

end.
