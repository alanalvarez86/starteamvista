unit FDialogoImagenes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, FileCtrl, jpeg, TMultiP, ZetaDBTextBox,
  ToolWin, ComCtrls;

type
  TDialogoImagenes = class(TForm)
    Panel1: TPanel;
    OK: TBitBtn;
    Cancelar: TBitBtn;
    Imagen_Gb: TGroupBox;
    Panel2: TPanel;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label1: TLabel;
    Ext_Img_Cb: TFilterComboBox;
    Directorio_Ztb: TZetaTextBox;
    Imagen: TImage;
    Ext_Doc_Cb: TFilterComboBox;
    Ext_Web_Cb: TFilterComboBox;
    ListaArchivos: TFileListBox;
    OpenFolder_Btn: TSpeedButton;
    Refrescar_Btn: TSpeedButton;
    procedure ListaArchivosClick(Sender: TObject);
    procedure ListaArchivosDblClick(Sender: TObject);
    procedure OpenFolder_BtnClick(Sender: TObject);
    procedure Refrescar_BtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FTipo: String;
    lRefrescar: Boolean;
  public
    { Public declarations }
    procedure LlamaDialogo(const TipoDlg: String; Archivo_Img: String);
    property Tipo: String read FTipo write FTipo;
  end;

const
     K_TIPO_IMAGEN = 'Imagen';
     K_TIPO_DOCS   = 'Documento';
     K_TIPO_WEB    = 'Web';

var
  DialogoImagenes: TDialogoImagenes;

implementation
uses FEditNoticias, ZetaCommonTools, DCliente, FTressShell, ZetaDialogo;

{$R *.DFM}


procedure TDialogoImagenes.LlamaDialogo(const TipoDlg: String; Archivo_Img: String);
begin
     Tipo := TipoDlg;
     with dmCliente do
     begin
          Directorio_Ztb.Caption := RutaImagenGlobal;
          ListaArchivos.ApplyFilePath( RutaImagenGlobal );
     end;
     Ext_Doc_Cb.Visible := False;
     Ext_Img_Cb.Visible := False;
     Ext_Web_Cb.Visible := False;
     Imagen_Gb.Visible := False;
     if ( Tipo = K_TIPO_IMAGEN ) then
     begin
          Width := 478;
          ListaArchivos.Mask := '*.jpg;*.jpeg;*.bmp;*.gif';
          Ext_Img_Cb.Visible := True;
          Ext_Img_Cb.ItemIndex := 4;
          Imagen_Gb.Visible := True;
          try
             ListaArchivos.FileName := Archivo_Img;
          except
                 ListaArchivos.FileName := '';
          end;
          if ( ExtractFileExt(ListaArchivos.FileName) <> '.gif' ) then
               Imagen.Picture.LoadFromFile( ListaArchivos.FileName )
          else
          begin
               Imagen.Picture.LoadFromFile( '' );
               if ZetaDialogo.ZConfirm('Portal Tress','El archivo esta en formato *.GIF desea abrirlo con Windows?',0,mbOk) then
                  TressShell.ExecuteFile( ExtractFileName(ListaArchivos.FileName), '', Directorio_Ztb.Caption );
          end;
     end;
     if ( Tipo = K_TIPO_DOCS ) then
     begin
          Width := 240;
          Ext_Doc_Cb.Visible := True;
          Ext_Doc_Cb.ItemIndex := 4;
          ListaArchivos.Mask := '*.doc;*.txt;*.html;*.htm';
     end;
     if ( Tipo = K_TIPO_WEB ) then
     begin
          Width := 240;
          Ext_Web_Cb.Visible := True;
          Ext_Web_Cb.ItemIndex := 2;
          ListaArchivos.Mask := '*.html;*.htm';
     end;
     if not lRefrescar then
        ShowModal;
end;

procedure TDialogoImagenes.ListaArchivosClick(Sender: TObject);
begin
     if (Tipo = K_TIPO_IMAGEN) then
     begin
          if ( ExtractFileExt(ListaArchivos.FileName) <> '.gif' ) then
               Imagen.Picture.LoadFromFile( ExtractFileName(ListaArchivos.FileName) )
          else
          begin
               //Imagen.Picture.LoadFromFile( '' );
               if ZetaDialogo.ZConfirm('Portal Tress','El archivo esta en formato *.GIF desea abrirlo con Windows?',0,mbOk) then
                  TressShell.ExecuteFile( ExtractFileName(ListaArchivos.FileName), '', Directorio_Ztb.Caption );
          end;
     end;

end;

procedure TDialogoImagenes.ListaArchivosDblClick(Sender: TObject);
begin
      Ok.Click;
end;

procedure TDialogoImagenes.OpenFolder_BtnClick(Sender: TObject);
begin
     TressShell.ExecuteFile( Directorio_Ztb.Caption, '', Directorio_Ztb.Caption );
end;

procedure TDialogoImagenes.Refrescar_BtnClick(Sender: TObject);
begin
     lRefrescar := True;
     if Ext_Web_Cb.Visible then
     begin
          ListaArchivos.Clear;
          Ext_Web_Cb.ItemIndex := 0;
          LlamaDialogo(K_TIPO_WEB,'');
     end;
     if Ext_Img_Cb.Visible then
     begin
          ListaArchivos.Clear;
          Ext_Img_Cb.ItemIndex := 4;
          LlamaDialogo(K_TIPO_IMAGEN,'');
     end;
     if Ext_Doc_Cb.Visible then
     begin
          ListaArchivos.Clear;
          Ext_Doc_Cb.ItemIndex := 4;
          LlamaDialogo(K_TIPO_DOCS,'');
     end;
end;

procedure TDialogoImagenes.FormShow(Sender: TObject);
begin
     lRefrescar := False;
     listaArchivos.SetFocus;
end;

procedure TDialogoImagenes.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     lRefrescar := False;
end;

end.
