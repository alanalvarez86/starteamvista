unit FEventos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid;

type
  TEventos = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  Eventos: TEventos;

implementation

uses DCliente, ZetaBuscador;

{$R *.DFM}

{ TEventos }
procedure TEventos.FormCreate(Sender: TObject);
begin
  inherited;
  CanLookup := True;
  // PENDIENTE : HelpContext := H60612_Clasificaciones;
end;

procedure TEventos.Connect;
begin
  with dmCliente do
     begin
          cdsTipoEvento.Conectar;
          cdsEventos.Conectar;
          DataSource.DataSet:= cdsEventos;
     end;
end;

procedure TEventos.Agregar;
begin
  dmCliente.cdsEventos.Agregar;
end;

procedure TEventos.Borrar;
begin
  dmCliente.cdsEventos.Borrar;
end;

procedure TEventos.Modificar;
begin
  dmCliente.cdsEventos.Modificar;
end;

procedure TEventos.Refresh;
begin
  dmCliente.cdsEventos.Refrescar;
end;

procedure TEventos.DoLookup;
begin
  inherited;
  ZetaBuscador.BuscarCodigo( 'Folio', 'Eventos', 'EV_FOLIO', dmCliente.cdsEventos );
end;

end.
