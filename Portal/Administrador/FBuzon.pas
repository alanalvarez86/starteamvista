unit FBuzon;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid;

type
  TBuzon = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  Buzon: TBuzon;

implementation

uses DCliente, ZetaBuscador;

{$R *.DFM}

{ TBuzon }
procedure TBuzon.FormCreate(Sender: TObject);
begin
  inherited;
    CanLookup := True;
  // PENDIENTE : HelpContext := H60612_Clasificaciones;
end;

procedure TBuzon.Connect;
begin
     with dmCliente do
     begin
          cdsBuzon.Conectar;
          cdsBuzon.Conectar;
          DataSource.DataSet:= cdsBuzon;
     end;
end;

procedure TBuzon.Agregar;
begin
     dmCliente.cdsBuzon.Agregar;
end;

procedure TBuzon.Borrar;
begin
     dmCliente.cdsBuzon.Borrar;
end;

procedure TBuzon.Modificar;
begin
     dmCliente.cdsBuzon.Modificar;
end;

procedure TBuzon.Refresh;
begin
     dmCliente.cdsBuzon.Refrescar;
end;

procedure TBuzon.DoLookup;
begin
  inherited;
  ZetaBuscador.BuscarCodigo( 'Folio', 'Buzon', 'BU_FOLIO', dmCliente.cdsBuzon );
end;

end.
