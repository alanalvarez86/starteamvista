inherited GlobalEdit: TGlobalEdit
  Left = 214
  Top = 207
  Caption = 'Editar Valores Globales'
  ClientHeight = 338
  ClientWidth = 529
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel [0]
    Left = 3
    Top = 108
    Width = 84
    Height = 13
    Alignment = taRightJustify
    Caption = 'URL de Empresa:'
  end
  inherited PanelBotones: TPanel
    Top = 302
    Width = 529
    inherited OK: TBitBtn
      Left = 361
    end
    inherited Cancelar: TBitBtn
      Left = 446
    end
  end
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 529
    Height = 302
    ActivePage = DatosPortal
    Align = alClient
    TabOrder = 1
    object DatosPortal: TTabSheet
      Caption = 'Portal'
      object NombreLBL: TLabel
        Left = 34
        Top = 6
        Width = 104
        Height = 13
        Alignment = taRightJustify
        Caption = '&Nombre Del Proyecto:'
        FocusControl = Nombre
      end
      object PaginadosLBL: TLabel
        Left = 36
        Top = 197
        Width = 102
        Height = 13
        Alignment = taRightJustify
        Caption = 'Registros Por P�gina:'
      end
      object TitularLBL: TLabel
        Left = 70
        Top = 245
        Width = 68
        Height = 13
        Alignment = taRightJustify
        Caption = 'Noticia Titular:'
        Visible = False
      end
      object PageRefreshLBL: TLabel
        Left = 27
        Top = 221
        Width = 111
        Height = 13
        Alignment = taRightJustify
        Caption = 'Refrescado De P�gina:'
      end
      object FotosFolderLBL: TLabel
        Left = 58
        Top = 77
        Width = 80
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ruta Para &Fotos:'
        FocusControl = FotosFolder
      end
      object LogoBigLBL: TLabel
        Left = 73
        Top = 125
        Width = 65
        Height = 13
        Alignment = taRightJustify
        Caption = 'L&ogo Grande:'
        FocusControl = LogoBig
      end
      object LogoChicoLBL: TLabel
        Left = 81
        Top = 149
        Width = 57
        Height = 13
        Alignment = taRightJustify
        Caption = 'Logo C&hico:'
        FocusControl = LogoChico
      end
      object DocFolderLBL: TLabel
        Left = 24
        Top = 101
        Width = 114
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ruta Para &Documentos:'
        FocusControl = DocFolder
      end
      object lbldirvirtual: TLabel
        Left = 56
        Top = 29
        Width = 80
        Height = 13
        Alignment = taRightJustify
        BiDiMode = bdLeftToRight
        Caption = 'Directorio Virtual:'
        FocusControl = edEmpvirtual
        ParentBiDiMode = False
      end
      object lblempvirtual: TLabel
        Left = 11
        Top = 53
        Width = 125
        Height = 13
        Alignment = taRightJustify
        BiDiMode = bdLeftToRight
        Caption = 'Fotos Empleados( Virtual ):'
        FocusControl = edDirVirtual
        ParentBiDiMode = False
      end
      object Label4: TLabel
        Left = 62
        Top = 173
        Width = 76
        Height = 13
        Alignment = taRightJustify
        Caption = 'Usuario Default:'
      end
      object Nombre: TEdit
        Tag = 1
        Left = 140
        Top = 2
        Width = 330
        Height = 21
        TabOrder = 0
      end
      object Titular: TZetaKeyLookup
        Left = 140
        Top = 241
        Width = 300
        Height = 21
        Enabled = False
        LookupDataset = dmCliente.cdsNoticias
        TabOrder = 10
        TabStop = True
        Visible = False
        WidthLlave = 60
      end
      object Paginados: TZetaNumero
        Left = 140
        Top = 193
        Width = 60
        Height = 21
        Mascara = mnDias
        TabOrder = 8
        Text = '0'
      end
      object PageRefresh: TZetaNumero
        Left = 140
        Top = 217
        Width = 60
        Height = 21
        Mascara = mnDias
        TabOrder = 9
        Text = '0'
      end
      object FotosFolder: TEdit
        Tag = 4
        Left = 140
        Top = 73
        Width = 330
        Height = 21
        TabOrder = 3
      end
      object LogoBig: TEdit
        Tag = 12
        Left = 140
        Top = 121
        Width = 330
        Height = 21
        TabOrder = 5
      end
      object LogoChico: TEdit
        Tag = 61
        Left = 140
        Top = 145
        Width = 330
        Height = 21
        TabOrder = 6
      end
      object DocFolder: TEdit
        Tag = 3
        Left = 140
        Top = 97
        Width = 330
        Height = 21
        TabOrder = 4
      end
      object edEmpvirtual: TEdit
        Left = 140
        Top = 49
        Width = 330
        Height = 21
        TabOrder = 2
      end
      object edDirVirtual: TEdit
        Left = 140
        Top = 25
        Width = 330
        Height = 21
        TabOrder = 1
      end
      object UserDeflkp: TZetaKeyLookup
        Left = 140
        Top = 169
        Width = 300
        Height = 21
        LookupDataset = dmCliente.cdsUsuarios
        TabOrder = 7
        TabStop = True
        WidthLlave = 60
      end
    end
    object DatosEmpresa: TTabSheet
      Caption = 'Empresa'
      ImageIndex = 1
      object EmpresaLBL: TLabel
        Left = 43
        Top = 16
        Width = 44
        Height = 13
        Alignment = taRightJustify
        Caption = '&Empresa:'
        FocusControl = Empresa
      end
      object CiudadLBL: TLabel
        Left = 51
        Top = 62
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = '&Ciudad:'
        FocusControl = Ciudad
      end
      object EstadoLBL: TLabel
        Left = 51
        Top = 85
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'E&stado:'
        FocusControl = Estado
      end
      object DireccionLBL: TLabel
        Left = 39
        Top = 39
        Width = 48
        Height = 13
        Alignment = taRightJustify
        Caption = 'D&irecci�n:'
        FocusControl = Direccion
      end
      object lblURL: TLabel
        Left = 3
        Top = 108
        Width = 84
        Height = 13
        Alignment = taRightJustify
        Caption = 'URL de Empresa:'
      end
      object Empresa: TEdit
        Tag = 6
        Left = 89
        Top = 13
        Width = 391
        Height = 21
        TabOrder = 0
      end
      object Direccion: TEdit
        Tag = 7
        Left = 89
        Top = 36
        Width = 391
        Height = 21
        TabOrder = 1
      end
      object Ciudad: TEdit
        Tag = 7
        Left = 89
        Top = 59
        Width = 391
        Height = 21
        TabOrder = 2
      end
      object Estado: TEdit
        Tag = 7
        Left = 89
        Top = 82
        Width = 391
        Height = 21
        TabOrder = 3
      end
      object edURL: TEdit
        Left = 89
        Top = 104
        Width = 391
        Height = 21
        TabOrder = 4
      end
    end
    object DatosMail: TTabSheet
      Caption = 'e-Mail'
      ImageIndex = 2
      object eMailsLBL: TLabel
        Left = 26
        Top = 17
        Width = 105
        Height = 13
        Alignment = taRightJustify
        Caption = 'Direcciones de &e-Mail:'
        FocusControl = eMails
      end
      object eMailServerLBL: TLabel
        Left = 3
        Top = 40
        Width = 128
        Height = 13
        Alignment = taRightJustify
        Caption = '&Servidor de e-Mail ( POP3):'
        FocusControl = eMailServer
      end
      object AutoResponseLBL: TLabel
        Left = 6
        Top = 63
        Width = 125
        Height = 13
        Alignment = taRightJustify
        Caption = 'e-Mail de Auto-&Respuesta:'
        FocusControl = AutoResponse
      end
      object AutoResponse: TEdit
        Tag = 7
        Left = 133
        Top = 59
        Width = 355
        Height = 21
        TabOrder = 2
      end
      object eMailServer: TEdit
        Tag = 7
        Left = 133
        Top = 36
        Width = 355
        Height = 21
        TabOrder = 1
      end
      object eMails: TEdit
        Tag = 6
        Left = 133
        Top = 13
        Width = 355
        Height = 21
        TabOrder = 0
      end
    end
    object TS_Directorios: TTabSheet
      Caption = 'Directorios'
      ImageIndex = 3
      object Label1: TLabel
        Left = 72
        Top = 14
        Width = 32
        Height = 13
        Caption = ' &Fotos:'
        FocusControl = FotosDir_Ed
      end
      object Label2: TLabel
        Left = 41
        Top = 38
        Width = 63
        Height = 13
        Caption = 'Documentos:'
        FocusControl = DocsDir_Ed
      end
      object DirFotos_Btn: TSpeedButton
        Left = 439
        Top = 9
        Width = 23
        Height = 22
        Hint = 'Buscar Directorio Ra�z'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
          300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
          330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
          333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
          339977FF777777773377000BFB03333333337773FF733333333F333000333333
          3300333777333333337733333333333333003333333333333377333333333333
          333333333333333333FF33333333333330003333333333333777333333333333
          3000333333333333377733333333333333333333333333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = DirFotos_BtnClick
      end
      object DirDocs_Btn: TSpeedButton
        Left = 439
        Top = 33
        Width = 23
        Height = 22
        Hint = 'Buscar Directorio Ra�z'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
          300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
          330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
          333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
          339977FF777777773377000BFB03333333337773FF733333333F333000333333
          3300333777333333337733333333333333003333333333333377333333333333
          333333333333333333FF33333333333330003333333333333777333333333333
          3000333333333333377733333333333333333333333333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = DirDocs_BtnClick
      end
      object lblEmpdisco: TLabel
        Left = 9
        Top = 62
        Width = 95
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fotos Emp.( Disco ):'
        FocusControl = edEmpDisco
      end
      object btnempdir: TSpeedButton
        Left = 439
        Top = 56
        Width = 23
        Height = 22
        Hint = 'Buscar Directorio Ra�z'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
          300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
          330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
          333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
          339977FF777777773377000BFB03333333337773FF733333333F333000333333
          3300333777333333337733333333333333003333333333333377333333333333
          333333333333333333FF33333333333330003333333333333777333333333333
          3000333333333333377733333333333333333333333333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = btnempdirClick
      end
      object RootFolderLBL: TLabel
        Left = 32
        Top = 86
        Width = 74
        Height = 13
        Alignment = taRightJustify
        Caption = 'Directorio &Ra�z:'
        FocusControl = RootFolder
      end
      object RootFolderFind: TSpeedButton
        Left = 440
        Top = 80
        Width = 23
        Height = 22
        Hint = 'Buscar Directorio Ra�z'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
          300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
          330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
          333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
          339977FF777777773377000BFB03333333337773FF733333333F333000333333
          3300333777333333337733333333333333003333333333333377333333333333
          333333333333333333FF33333333333330003333333333333777333333333333
          3000333333333333377733333333333333333333333333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = RootFolderFindClick
      end
      object FotosDir_Ed: TEdit
        Tag = 4
        Left = 108
        Top = 10
        Width = 330
        Height = 21
        TabOrder = 0
      end
      object DocsDir_Ed: TEdit
        Tag = 12
        Left = 108
        Top = 34
        Width = 330
        Height = 21
        TabOrder = 1
      end
      object edEmpDisco: TEdit
        Left = 108
        Top = 58
        Width = 330
        Height = 21
        TabOrder = 2
      end
      object RootFolder: TEdit
        Tag = 2
        Left = 108
        Top = 82
        Width = 330
        Height = 21
        TabOrder = 3
      end
    end
  end
  object OpenDialog: TOpenDialog
    DefaultExt = 'jpg'
    Filter = 
      'Archivos JPG (*.jpg)|*.jpg|Archivos JPEG (*.jpeg)|*.jpeg|Bitmaps' +
      ' (*.bmp)|*.bmp|Todos ( *.* )|*.*'
    FilterIndex = 0
    Title = 'Seleccione La Imagen Deseada'
    Left = 480
    Top = 32
  end
end
