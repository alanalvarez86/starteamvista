inherited EditNoticias: TEditNoticias
  Left = 242
  Top = 183
  Caption = 'Noticias'
  ClientHeight = 371
  ClientWidth = 447
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 335
    Width = 447
    TabOrder = 4
    inherited OK: TBitBtn
      Left = 279
    end
    inherited Cancelar: TBitBtn
      Left = 364
    end
  end
  inherited PanelSuperior: TPanel
    Width = 447
    TabOrder = 0
  end
  inherited PanelIdentifica: TPanel
    Width = 447
    TabOrder = 1
    inherited ValorActivo2: TPanel
      Width = 121
    end
  end
  object PageControl1: TPageControl [3]
    Left = 0
    Top = 117
    Width = 447
    Height = 218
    ActivePage = Generales_TS
    Align = alClient
    TabOrder = 3
    object Generales_TS: TTabSheet
      Caption = 'Generales'
      object Label5: TLabel
        Left = 32
        Top = 143
        Width = 43
        Height = 13
        Caption = 'Modific�:'
      end
      object NT_USUARIO: TZetaDBTextBox
        Left = 77
        Top = 141
        Width = 190
        Height = 17
        AutoSize = False
        Caption = 'NT_USUARIO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'US_NOMBRE'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label3: TLabel
        Left = 49
        Top = 7
        Width = 26
        Height = 13
        Alignment = taRightJustify
        Caption = 'Hora:'
      end
      object Label2: TLabel
        Left = 42
        Top = 32
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha:'
      end
      object Label6: TLabel
        Left = 47
        Top = 54
        Width = 28
        Height = 13
        Alignment = taRightJustify
        Caption = 'Autor:'
      end
      object Label7: TLabel
        Left = 51
        Top = 78
        Width = 24
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo:'
      end
      object Label8: TLabel
        Left = 37
        Top = 102
        Width = 38
        Height = 13
        Alignment = taRightJustify
        Caption = 'Imagen:'
      end
      object NT_IMAGEN_Find: TSpeedButton
        Left = 395
        Top = 98
        Width = 20
        Height = 21
        Hint = 'Buscar Archivo de Imagen'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
          300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
          330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
          333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
          339977FF777777773377000BFB03333333337773FF733333333F333000333333
          3300333777333333337733333333333333003333333333333377333333333333
          333333333333333333FF33333333333330003333333333333777333333333333
          3000333333333333377733333333333333333333333333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = NT_IMAGEN_FIndClick
      end
      object Label10: TLabel
        Left = 43
        Top = 123
        Width = 32
        Height = 13
        Caption = 'Titular:'
      end
      object NT_HORA: TZetaDBHora
        Left = 77
        Top = 3
        Width = 41
        Height = 21
        EditMask = '99:99;0'
        TabOrder = 0
        Text = '    '
        Tope = 24
        Valor = '    '
        DataField = 'NT_HORA'
        DataSource = DataSource
      end
      object NT_FECHA: TZetaDBFecha
        Left = 77
        Top = 27
        Width = 105
        Height = 22
        Cursor = crArrow
        TabOrder = 1
        Text = '04/Apr/02'
        Valor = 37350
        DataField = 'NT_FECHA'
        DataSource = DataSource
      end
      object NT_AUTOR: TZetaDBEdit
        Left = 77
        Top = 50
        Width = 241
        Height = 21
        MaxLength = 30
        TabOrder = 2
        Text = 'NT_AUTOR'
        DataField = 'NT_AUTOR'
        DataSource = DataSource
      end
      object NT_CLASIFI: TZetaDBKeyLookup
        Left = 77
        Top = 74
        Width = 340
        Height = 20
        LookupDataset = dmCliente.cdsTipoNoticias
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        DataField = 'NT_CLASIFI'
        DataSource = DataSource
      end
      object NT_IMAGEN: TZetaDBEdit
        Left = 77
        Top = 98
        Width = 317
        Height = 21
        TabOrder = 4
        Text = 'NT_IMAGEN'
        DataField = 'NT_IMAGEN'
        DataSource = DataSource
      end
      object EsTitular: TCheckBox
        Left = 77
        Top = 121
        Width = 25
        Height = 17
        TabOrder = 5
        OnClick = EsTitularClick
      end
    end
    object Nota_Corta_TS: TTabSheet
      Caption = 'Nota Corta'
      ImageIndex = 1
      object NT_CORTA: TDBMemo
        Left = 0
        Top = 0
        Width = 439
        Height = 190
        Align = alClient
        DataField = 'NT_CORTA'
        DataSource = DataSource
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object Nota_Cmp_TS: TTabSheet
      Caption = 'Nota Completa'
      ImageIndex = 2
      object NT_MEMO: TDBMemo
        Left = 0
        Top = 0
        Width = 412
        Height = 190
        Align = alClient
        DataField = 'NT_MEMO'
        DataSource = DataSource
        ScrollBars = ssVertical
        TabOrder = 0
      end
      object Panel1: TPanel
        Left = 412
        Top = 0
        Width = 27
        Height = 190
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object Find_Html_Btn: TSpeedButton
          Left = 2
          Top = 2
          Width = 24
          Height = 25
          Hint = 'Buscar Archivos Html'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
            300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
            330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
            333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
            339977FF777777773377000BFB03333333337773FF733333333F333000333333
            3300333777333333337733333333333333003333333333333377333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          OnClick = Find_Html_BtnClick
        end
      end
    end
  end
  object Titulo_Pn: TPanel [4]
    Left = 0
    Top = 51
    Width = 447
    Height = 66
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object Label4: TLabel
      Left = 48
      Top = 24
      Width = 31
      Height = 13
      Alignment = taRightJustify
      Caption = 'T�tulo:'
    end
    object Label9: TLabel
      Left = 33
      Top = 46
      Width = 46
      Height = 13
      Alignment = taRightJustify
      Caption = 'Subt�tulo:'
    end
    object Label1: TLabel
      Left = 54
      Top = 4
      Width = 25
      Height = 13
      Alignment = taRightJustify
      Caption = 'Folio:'
    end
    object NT_FOLIO: TZetaDBTextBox
      Left = 81
      Top = 2
      Width = 105
      Height = 17
      AutoSize = False
      Caption = 'NT_FOLIO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'NT_FOLIO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object NT_TITULO: TZetaDBEdit
      Left = 81
      Top = 20
      Width = 337
      Height = 21
      MaxLength = 70
      TabOrder = 0
      Text = 'NT_TITULO'
      DataField = 'NT_TITULO'
      DataSource = DataSource
    end
    object NT_SUB_TIT: TZetaDBEdit
      Left = 81
      Top = 42
      Width = 337
      Height = 21
      MaxLength = 70
      TabOrder = 1
      Text = 'NT_SUB_TIT'
      DataField = 'NT_SUB_TIT'
      DataSource = DataSource
    end
  end
  inherited DataSource: TDataSource
    Left = 372
    Top = 193
  end
  object DlgFileOpen: TOpenDialog
    Filter = 
      'Archivos HTM* (*.htm*)|*.htm*|Archivos TXT (*.txt)|*.txt|Todos (' +
      '*.*)|*.*'
    Left = 332
    Top = 161
  end
end
