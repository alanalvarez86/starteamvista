inherited ContenidoEditarNoticia: TContenidoEditarNoticia
  Left = 392
  Top = 336
  ActiveControl = Noticia
  Caption = 'Noticia'
  ClientWidth = 388
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 2
    Top = 14
    Width = 77
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo De Noticia:'
  end
  object MostrarLBL: TLabel [1]
    Left = 43
    Top = 36
    Width = 38
    Height = 13
    Caption = 'Mostrar:'
  end
  inherited PanelBotones: TPanel
    Width = 388
    TabOrder = 2
    inherited OK: TBitBtn
      Left = 220
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 305
    end
  end
  object Noticia: TZetaKeyLookup
    Left = 83
    Top = 10
    Width = 300
    Height = 21
    LookupDataset = dmCliente.cdsTipoNoticias
    TabOrder = 0
    TabStop = True
    WidthLlave = 60
  end
  object Mostrar: TZetaNumero
    Left = 83
    Top = 32
    Width = 41
    Height = 21
    Mascara = mnDias
    TabOrder = 1
    Text = '0'
  end
end
