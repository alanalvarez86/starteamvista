inherited Noticias: TNoticias
  Left = 309
  Top = 282
  Caption = 'Noticias'
  ClientWidth = 663
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 663
    inherited ValorActivo2: TPanel
      Width = 404
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 663
    Height = 254
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'NT_FOLIO'
        Title.Caption = 'Folio'
        Width = 55
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'NT_FECHA'
        Title.Caption = 'Fecha'
        Width = 80
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'NT_HORA'
        Title.Alignment = taRightJustify
        Title.Caption = 'Hora'
        Width = 49
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NT_TITULAR'
        Title.Caption = 'Titular'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NT_TITULO'
        Title.Caption = 'Titulo'
        Width = 250
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NT_AUTOR'
        Title.Caption = 'Autor'
        Width = 140
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TB_ELEMENT'
        Title.Caption = 'Clasificación'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_NOMBRE'
        Title.Caption = 'Usuario'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NT_IMAGEN'
        Title.Caption = 'Ruta Imagen'
        Width = 450
        Visible = True
      end>
  end
end
