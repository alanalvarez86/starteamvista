unit FContenidoEditarEvento;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls,
     FContenidoEditar, Mask, ZetaNumero, ZetaKeyLookup;

type
  TContenidoEditarEvento = class(TContenidoEditar)
    EventoLBL: TLabel;
    Evento: TZetaKeyLookup;
    Mostrar: TZetaNumero;
    MostrarLBL: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ContenidoEditarEvento: TContenidoEditarEvento;

implementation
uses ZetaDialogo;

{$R *.DFM}

procedure TContenidoEditarEvento.FormCreate(Sender: TObject);
begin
     inherited;
     //HelpContext:= H00006_Catalogo_de_Diagnosticos;
end;

procedure TContenidoEditarEvento.FormShow(Sender: TObject);
begin
     inherited;
     Caption := GetCaption( 'Evento' );
     with Contenido do
     begin
          Self.Mostrar.Valor := Mostrar;
          with Self.Evento do
          begin
               LookupDataset.Conectar;
               Llave := Clase;
          end;
     end;
     ActiveControl := Evento;
end;

procedure TContenidoEditarEvento.OKClick(Sender: TObject);
begin
     inherited;
     if ( Mostrar.Valor < 1 ) then
     begin
          ZetaDialogo.ZError( Caption, 'El Valor A Mostrar No Puede Ser Menor de 1', 0 );
          ActiveControl := Evento;
     end
     else
     begin
          with Contenido do
          begin
               Mostrar := Self.Mostrar.ValorEntero;
               Clase := Self.Evento.Llave;
               Descripcion := Self.Evento.Descripcion;
          end;
          ModalResult := mrOk;
     end;     
end;

end.
