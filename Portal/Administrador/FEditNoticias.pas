unit FEditNoticias;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask, ExtDlgs,
     ZBaseEdicion,
     ZetaEdit,
     ZetaHora,
     ZetaFecha,
     ZetaNumero,
     ZetaDBTextBox,
     ZetaKeyLookup, ComCtrls;

type
  TEditNoticias = class(TBaseEdicion)
    PageControl1: TPageControl;
    Generales_TS: TTabSheet;
    Label5: TLabel;
    NT_USUARIO: TZetaDBTextBox;
    Label3: TLabel;
    NT_HORA: TZetaDBHora;
    Label2: TLabel;
    NT_FECHA: TZetaDBFecha;
    Label6: TLabel;
    NT_AUTOR: TZetaDBEdit;
    Titulo_Pn: TPanel;
    Label4: TLabel;
    NT_TITULO: TZetaDBEdit;
    Label9: TLabel;
    NT_SUB_TIT: TZetaDBEdit;
    Label7: TLabel;
    NT_CLASIFI: TZetaDBKeyLookup;
    Label8: TLabel;
    NT_IMAGEN: TZetaDBEdit;
    NT_IMAGEN_Find: TSpeedButton;
    Nota_Corta_TS: TTabSheet;
    Nota_Cmp_TS: TTabSheet;
    EsTitular: TCheckBox;
    Label10: TLabel;
    DlgFileOpen: TOpenDialog;
    NT_CORTA: TDBMemo;
    NT_MEMO: TDBMemo;
    Panel1: TPanel;
    Find_Html_Btn: TSpeedButton;
    Label1: TLabel;
    NT_FOLIO: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure EsTitularClick(Sender: TObject);
    procedure Find_Html_BtnClick(Sender: TObject);
    procedure NT_IMAGEN_FindClick(Sender: TObject);
  private
    
  protected
    { Protected declarations }
    FBrowsing: Boolean;
    procedure Connect; override;
    procedure DoLookup; override;
    procedure EscribirCambios; override;
  public
    { Public declarations }
  end;

var
  EditNoticias: TEditNoticias;

implementation

uses FDialogoImagenes,
     DGlobal,
     DCliente,
     ZetaBuscador,
     ZetaDialogo;

{$R *.DFM}

{ TEditNoticias }

procedure TEditNoticias.FormCreate(Sender: TObject);
begin
     inherited;
     FBrowsing := False;
     FirstControl := NT_TITULO;
     NT_CLASIFI.LookupDataset := dmCliente.cdsTipoNoticias;
     //MA:Pendiente
     //HelpContext:= H60617_Tipo_contrato;
end;

procedure TEditNoticias.Connect;
begin
     with dmCliente do
     begin
          cdsNoticias.Conectar;
          DataSource.DataSet:= cdsNoticias;
     end;
end;

procedure TEditNoticias.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if not FBrowsing then
     begin
          FBrowsing := True;
          try
             EsTitular.Checked := dmCliente.cdsNoticias.FieldByName( 'NT_TITULAR' ).AsBoolean;
          finally
                 FBrowsing := False;
          end;
     end;
end;

procedure TEditNoticias.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Folio', 'Noticias', 'NT_FOLIO', dmCliente.cdsNoticias );
end;

procedure TEditNoticias.EsTitularClick(Sender: TObject);
begin
     inherited;
     if not FBrowsing then
     begin
          with Datasource do
          begin
               if Assigned( Dataset ) then
               begin
                    with Dataset do
                    begin
                         if not ( State in [ dsEdit, dsInsert ] ) then
                         begin
                              Edit;
                              EsTitular.Checked := not EsTitular.Checked; 
                         end;
                    end;
               end;
          end;
     end;
end;

procedure TEditNoticias.EscribirCambios;
begin
     dmCliente.SetTitular( EsTitular.Checked );
     inherited EscribirCambios;
end;

procedure TEditNoticias.Find_Html_BtnClick(Sender: TObject);
begin
     with dlgFileOpen do
     begin
          with dmCliente do
          begin
               InitialDir := RutaImagenGlobal;
               if Execute then
               begin
                    with NT_MEMO do
                    begin
                         Clear;
                         cdsNoticias.Edit;
                         Lines.LoadFromFile(dlgFileOpen.FileName);
                    end;
               end;
          end;
     end;
end;

procedure TEditNoticias.NT_IMAGEN_FindClick(Sender: TObject);
begin
     with dmCliente do
     begin
          RutaImagenGlobal := Global.GetGlobalString( FindBtnImagen );
          with DialogoImagenes do
          begin
               LlamaDialogo(K_TIPO_IMAGEN,NT_IMAGEN.Text);
               if ( ModalResult = mrOk ) then
               begin
                    cdsNoticias.Edit;
                    NT_IMAGEN.Text := ExtractFileName(ListaArchivos.FileName);
               end;
          end;
     end;
end;

end.
