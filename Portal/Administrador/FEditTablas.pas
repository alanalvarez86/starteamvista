unit FEditTablas;

interface

uses ZBaseTablas;

type
  TTOEditTipoNoticias = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEditTipoEvento = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEditTDocumento = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;

var
  EditTablas: TEditTablas;

implementation

uses dCliente,
     ZetaBuscador;

{ TTOEditTipoNoticias }

procedure TTOEditTipoNoticias.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmCliente.cdsTipoNoticias;
     //HelpContext:= H70711_Estado_civil;
     //IndexDerechos := ZAccesosTress.D_TAB_PER_EDOCIVIL;
end;

{ TTOEditTipoEvento }

procedure TTOEditTipoEvento.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmCliente.cdsTipoEvento;
     //HelpContext:= H70712_En_donde_vive;
     //IndexDerechos := ZAccesosTress.D_TAB_PER_VIVE_EN;
end;

{ TTOEditTDocumento }

procedure TTOEditTDocumento.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmCliente.cdsTipoDocumento;
     //HelpContext:= H70713_Con_quien_vive;
     //IndexDerechos := ZAccesosTress.D_TAB_PER_VIVE_CON;
end;

end.
