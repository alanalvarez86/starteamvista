inherited Reportes: TReportes
  Caption = 'Consulta De Reportes'
  ClientHeight = 224
  ClientWidth = 520
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 520
    inherited ValorActivo2: TPanel
      Width = 261
    end
  end
  object ReportesDBG: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 520
    Height = 205
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'RP_FOLIO'
        Title.Caption = 'Folio'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RP_T_DESC'
        Title.Caption = 'Tipo'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RP_TITULO'
        Title.Caption = 'Titulo'
        Width = 180
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RP_FECHA'
        Title.Caption = 'Fecha'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RP_HORA'
        Title.Caption = 'Hora'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_NOMBRE'
        Title.Caption = 'Usuario'
        Width = 180
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RP_COLS'
        Title.Caption = 'Columnas'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RP_ROWS'
        Title.Caption = 'Renglónes'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RP_IMAGEN'
        Title.Caption = 'Imagen'
        Width = 450
        Visible = True
      end>
  end
end
