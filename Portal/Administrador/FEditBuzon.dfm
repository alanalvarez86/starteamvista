inherited EditBuzon: TEditBuzon
  Left = 276
  Top = 136
  Caption = 'Buz�n de Sugerencias'
  ClientHeight = 290
  ClientWidth = 427
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 254
    Width = 427
    TabOrder = 4
    inherited OK: TBitBtn
      Left = 259
    end
    inherited Cancelar: TBitBtn
      Left = 344
    end
  end
  inherited PanelSuperior: TPanel
    Width = 427
    TabOrder = 0
  end
  inherited PanelIdentifica: TPanel
    Width = 427
    TabOrder = 1
    inherited ValorActivo2: TPanel
      Width = 101
    end
  end
  object Titulo_Pn: TPanel [3]
    Left = 0
    Top = 51
    Width = 427
    Height = 48
    Align = alTop
    TabOrder = 2
    object Label1: TLabel
      Left = 55
      Top = 8
      Width = 25
      Height = 13
      Caption = 'Folio:'
    end
    object BU_FOLIO: TZetaDBTextBox
      Left = 83
      Top = 4
      Width = 105
      Height = 17
      AutoSize = False
      Caption = 'BU_FOLIO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'BU_FOLIO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label4: TLabel
      Left = 51
      Top = 26
      Width = 31
      Height = 13
      Caption = 'T�tulo:'
    end
    object BU_TITULO: TZetaDBEdit
      Left = 83
      Top = 22
      Width = 337
      Height = 21
      MaxLength = 70
      TabOrder = 0
      Text = 'BU_TITULO'
      DataField = 'BU_TITULO'
      DataSource = DataSource
    end
  end
  object PageControl: TPageControl [4]
    Left = 0
    Top = 99
    Width = 427
    Height = 155
    ActivePage = Descripcion_Ts
    Align = alClient
    TabOrder = 3
    object Generales_Ts: TTabSheet
      Caption = 'Generales'
      object Label3: TLabel
        Left = 50
        Top = 8
        Width = 26
        Height = 13
        Caption = 'Hora:'
      end
      object Label5: TLabel
        Left = 32
        Top = 95
        Width = 43
        Height = 13
        Caption = 'Modific�:'
      end
      object BU_USUARIO: TZetaDBTextBox
        Left = 79
        Top = 93
        Width = 190
        Height = 17
        AutoSize = False
        Caption = 'BU_USUARIO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'US_NOMBRE'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label6: TLabel
        Left = 48
        Top = 53
        Width = 28
        Height = 13
        Caption = 'Autor:'
      end
      object Label2: TLabel
        Left = 43
        Top = 31
        Width = 33
        Height = 13
        Caption = 'Fecha:'
      end
      object Label7: TLabel
        Left = 0
        Top = 75
        Width = 76
        Height = 13
        Caption = 'Direcci�n eMail:'
      end
      object BU_HORA: TZetaDBHora
        Left = 79
        Top = 4
        Width = 41
        Height = 21
        EditMask = '99:99;0'
        TabOrder = 0
        Text = '    '
        Tope = 24
        Valor = '    '
        DataField = 'BU_HORA'
        DataSource = DataSource
      end
      object BU_AUTOR: TZetaDBEdit
        Left = 79
        Top = 49
        Width = 241
        Height = 21
        MaxLength = 30
        TabOrder = 2
        Text = 'BU_AUTOR'
        DataField = 'BU_AUTOR'
        DataSource = DataSource
      end
      object BU_FECHA: TZetaDBFecha
        Left = 79
        Top = 26
        Width = 105
        Height = 22
        Cursor = crArrow
        TabOrder = 1
        Text = '04/Apr/02'
        Valor = 37350
        DataField = 'BU_FECHA'
        DataSource = DataSource
      end
      object BU_EMAIL: TZetaDBEdit
        Left = 79
        Top = 71
        Width = 337
        Height = 21
        MaxLength = 70
        TabOrder = 3
        Text = 'BU_EMAIL'
        DataField = 'BU_EMAIL'
        DataSource = DataSource
      end
    end
    object Descripcion_Ts: TTabSheet
      Caption = 'Sugerencias'
      ImageIndex = 1
      object BU_MEMO: TDBMemo
        Left = 0
        Top = 0
        Width = 419
        Height = 127
        Align = alClient
        DataField = 'BU_MEMO'
        DataSource = DataSource
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 372
    Top = 161
  end
end
