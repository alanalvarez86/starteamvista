inherited Documentos: TDocumentos
  Left = 360
  Top = 294
  Caption = 'Documentos'
  ClientWidth = 675
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 675
    inherited ValorActivo2: TPanel
      Width = 416
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 675
    Height = 254
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'DC_FOLIO'
        Title.Caption = 'Folio'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DC_FECHA'
        Title.Caption = 'Fecha'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DC_HORA'
        Title.Caption = 'Hora'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DC_TITULO'
        Title.Caption = 'Titulo'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DC_AUTOR'
        Title.Caption = 'Autor'
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_NOMBRE'
        Title.Caption = 'Usuario'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TB_ELEMENT'
        Title.Caption = 'Tipo'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DC_RUTA'
        Title.Caption = 'Ruta'
        Width = 400
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DC_IMAGEN'
        Title.Caption = 'Imagen'
        Width = 400
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DC_PALABRA'
        Title.Caption = 'Palabra'
        Width = 400
        Visible = True
      end>
  end
end
