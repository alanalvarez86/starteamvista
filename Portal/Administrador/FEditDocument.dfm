inherited EditDocumento: TEditDocumento
  Left = 252
  Top = 198
  Caption = 'Documentos'
  ClientHeight = 345
  ClientWidth = 445
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 309
    Width = 445
    TabOrder = 4
    inherited OK: TBitBtn
      Left = 277
    end
    inherited Cancelar: TBitBtn
      Left = 362
    end
  end
  inherited PanelSuperior: TPanel
    Width = 445
    TabOrder = 0
  end
  inherited PanelIdentifica: TPanel
    Width = 445
    TabOrder = 1
    inherited ValorActivo2: TPanel
      Width = 119
    end
  end
  object Titulo_Pn: TPanel [3]
    Left = 0
    Top = 51
    Width = 445
    Height = 52
    Align = alTop
    TabOrder = 2
    object Label1: TLabel
      Left = 40
      Top = 7
      Width = 25
      Height = 13
      Caption = 'Folio:'
    end
    object DC_FOLIO: TZetaDBTextBox
      Left = 69
      Top = 5
      Width = 113
      Height = 17
      AutoSize = False
      Caption = 'DC_FOLIO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'DC_FOLIO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label4: TLabel
      Left = 36
      Top = 29
      Width = 31
      Height = 13
      Caption = 'T�tulo:'
    end
    object DC_TITULO: TZetaDBEdit
      Left = 69
      Top = 25
      Width = 337
      Height = 21
      MaxLength = 70
      TabOrder = 0
      Text = 'DC_TITULO'
      DataField = 'DC_TITULO'
      DataSource = DataSource
    end
  end
  object PageControl: TPageControl [4]
    Left = 0
    Top = 103
    Width = 445
    Height = 206
    ActivePage = Generales_Ts
    Align = alClient
    TabOrder = 3
    object Generales_Ts: TTabSheet
      Caption = 'Generales'
      object Label3: TLabel
        Left = 34
        Top = 5
        Width = 26
        Height = 13
        Caption = 'Hora:'
      end
      object Label9: TLabel
        Left = 32
        Top = 28
        Width = 28
        Height = 13
        Caption = 'Inicio:'
      end
      object Label6: TLabel
        Left = 32
        Top = 50
        Width = 28
        Height = 13
        Caption = 'Autor:'
      end
      object Label11: TLabel
        Left = 16
        Top = 159
        Width = 43
        Height = 13
        Caption = 'Modific�:'
      end
      object DC_USUARIO: TZetaDBTextBox
        Left = 64
        Top = 157
        Width = 190
        Height = 17
        AutoSize = False
        Caption = 'DC_USUARIO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'US_NOMBRE'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label2: TLabel
        Left = 21
        Top = 138
        Width = 39
        Height = 13
        Caption = 'Palabra:'
      end
      object Label10: TLabel
        Left = 21
        Top = 116
        Width = 39
        Height = 13
        Caption = 'Archivo:'
      end
      object Label7: TLabel
        Left = 36
        Top = 72
        Width = 24
        Height = 13
        Caption = 'Tipo:'
      end
      object Label8: TLabel
        Left = 22
        Top = 94
        Width = 38
        Height = 13
        Caption = 'Imagen:'
      end
      object DC_RUTA_Find: TSpeedButton
        Left = 378
        Top = 113
        Width = 22
        Height = 22
        Hint = 'Buscar Archivo de Imagen'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
          300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
          330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
          333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
          339977FF777777773377000BFB03333333337773FF733333333F333000333333
          3300333777333333337733333333333333003333333333333377333333333333
          333333333333333333FF33333333333330003333333333333777333333333333
          3000333333333333377733333333333333333333333333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = DC_RUTA_FindClick
      end
      object DC_IMAGEN_Find: TSpeedButton
        Left = 378
        Top = 90
        Width = 22
        Height = 22
        Hint = 'Buscar Archivo de Imagen'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
          300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
          330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
          333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
          339977FF777777773377000BFB03333333337773FF733333333F333000333333
          3300333777333333337733333333333333003333333333333377333333333333
          333333333333333333FF33333333333330003333333333333777333333333333
          3000333333333333377733333333333333333333333333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = DC_IMAGEN_FindClick
      end
      object DC_HORA: TZetaDBHora
        Left = 64
        Top = 1
        Width = 41
        Height = 21
        EditMask = '99:99;0'
        TabOrder = 0
        Text = '    '
        Tope = 24
        Valor = '    '
        DataField = 'DC_HORA'
        DataSource = DataSource
      end
      object DC_FECHA: TZetaDBFecha
        Left = 64
        Top = 23
        Width = 113
        Height = 22
        Cursor = crArrow
        TabOrder = 1
        Text = '04/Apr/02'
        Valor = 37350
        DataField = 'DC_FECHA'
        DataSource = DataSource
      end
      object DC_AUTOR: TZetaDBEdit
        Left = 64
        Top = 46
        Width = 241
        Height = 21
        MaxLength = 30
        TabOrder = 2
        Text = 'DC_AUTOR'
        DataField = 'DC_AUTOR'
        DataSource = DataSource
      end
      object DC_PALABRA: TZetaDBEdit
        Left = 64
        Top = 134
        Width = 312
        Height = 21
        MaxLength = 255
        TabOrder = 6
        Text = 'DC_PALABRA'
        DataField = 'DC_PALABRA'
        DataSource = DataSource
      end
      object DC_RUTA: TZetaDBEdit
        Left = 64
        Top = 112
        Width = 312
        Height = 21
        MaxLength = 255
        TabOrder = 5
        Text = 'DC_RUTA'
        DataField = 'DC_RUTA'
        DataSource = DataSource
      end
      object DC_CLASIFI: TZetaDBKeyLookup
        Left = 64
        Top = 68
        Width = 337
        Height = 21
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        DataField = 'DC_CLASIFI'
        DataSource = DataSource
      end
      object DC_IMAGEN: TZetaDBEdit
        Left = 64
        Top = 90
        Width = 312
        Height = 21
        MaxLength = 255
        TabOrder = 4
        Text = 'DC_IMAGEN'
        DataField = 'DC_IMAGEN'
        DataSource = DataSource
      end
    end
    object Descripcion_Ts: TTabSheet
      Caption = 'Descripci�n del Documento'
      ImageIndex = 1
      object DC_MEMO: TDBMemo
        Left = 0
        Top = 0
        Width = 410
        Height = 178
        Align = alClient
        DataField = 'DC_MEMO'
        DataSource = DataSource
        ScrollBars = ssVertical
        TabOrder = 0
      end
      object Panel1: TPanel
        Left = 410
        Top = 0
        Width = 27
        Height = 178
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object Find_Html_Btn: TSpeedButton
          Left = 2
          Top = 2
          Width = 24
          Height = 25
          Hint = 'Buscar Archivos Html'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
            300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
            330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
            333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
            339977FF777777773377000BFB03333333337773FF733333333F333000333333
            3300333777333333337733333333333333003333333333333377333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          OnClick = Find_Html_BtnClick
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 156
    Top = 121
  end
  object DlgFileOpen: TOpenDialog
    Filter = 
      'Archivos HTM* (*.htm*)|*.htm*|Archivos TXT (*.txt)|*.txt|Todos (' +
      '*.*)|*.*'
    Left = 348
    Top = 156
  end
end
