inherited EditEventos: TEditEventos
  Left = 260
  Top = 194
  Caption = 'Eventos'
  ClientHeight = 382
  ClientWidth = 420
  PixelsPerInch = 96
  TextHeight = 13
  object Label5: TLabel [0]
    Left = 240
    Top = 418
    Width = 43
    Height = 13
    Caption = 'Modific�:'
  end
  inherited PanelBotones: TPanel
    Top = 346
    Width = 420
    TabOrder = 4
    inherited OK: TBitBtn
      Left = 252
    end
    inherited Cancelar: TBitBtn
      Left = 337
    end
  end
  inherited PanelSuperior: TPanel
    Width = 420
    TabOrder = 0
  end
  inherited PanelIdentifica: TPanel
    Width = 420
    TabOrder = 1
    inherited ValorActivo2: TPanel
      Width = 94
    end
  end
  object Titulo_Pn: TPanel [4]
    Left = 0
    Top = 51
    Width = 420
    Height = 49
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object Label4: TLabel
      Left = 31
      Top = 27
      Width = 31
      Height = 13
      Caption = 'T�tulo:'
    end
    object Label1: TLabel
      Left = 35
      Top = 6
      Width = 25
      Height = 13
      Caption = 'Folio:'
    end
    object EV_FOLIO: TZetaDBTextBox
      Left = 62
      Top = 4
      Width = 113
      Height = 17
      AutoSize = False
      Caption = 'EV_FOLIO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'EV_FOLIO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object EV_TITULO: TZetaDBEdit
      Left = 62
      Top = 23
      Width = 337
      Height = 21
      MaxLength = 70
      TabOrder = 0
      Text = 'EV_TITULO'
      DataField = 'EV_TITULO'
      DataSource = DataSource
    end
  end
  object Eventos_Pc: TPageControl [5]
    Left = 0
    Top = 100
    Width = 420
    Height = 246
    ActivePage = Generales_Ts
    Align = alClient
    TabOrder = 3
    object Generales_Ts: TTabSheet
      Caption = 'Generales'
      object Label3: TLabel
        Left = 29
        Top = 6
        Width = 26
        Height = 13
        Caption = 'Hora:'
      end
      object Label6: TLabel
        Left = 27
        Top = 78
        Width = 28
        Height = 13
        Caption = 'Autor:'
      end
      object Label7: TLabel
        Left = 31
        Top = 99
        Width = 24
        Height = 13
        Caption = 'Tipo:'
      end
      object Label8: TLabel
        Left = 17
        Top = 149
        Width = 38
        Height = 13
        Caption = 'Imagen:'
      end
      object Label10: TLabel
        Left = 25
        Top = 125
        Width = 30
        Height = 13
        Caption = 'Lugar:'
      end
      object Label11: TLabel
        Left = 12
        Top = 194
        Width = 43
        Height = 13
        Caption = 'Modific�:'
      end
      object EV_USUARIO: TZetaDBTextBox
        Left = 57
        Top = 192
        Width = 190
        Height = 17
        AutoSize = False
        Caption = 'EV_USUARIO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'US_NOMBRE'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NT_IMAGEN_Find: TSpeedButton
        Left = 369
        Top = 145
        Width = 22
        Height = 21
        Hint = 'Buscar Archivo de Imagen'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
          300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
          330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
          333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
          339977FF777777773377000BFB03333333337773FF733333333F333000333333
          3300333777333333337733333333333333003333333333333377333333333333
          333333333333333333FF33333333333330003333333333333777333333333333
          3000333333333333377733333333333333333333333333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = NT_IMAGE_FindClick
      end
      object Label2: TLabel
        Left = 38
        Top = 55
        Width = 17
        Height = 13
        Caption = 'Fin:'
      end
      object Label9: TLabel
        Left = 27
        Top = 31
        Width = 28
        Height = 13
        Caption = 'Inicio:'
      end
      object Label12: TLabel
        Left = 19
        Top = 173
        Width = 36
        Height = 13
        Caption = 'P�gina:'
      end
      object EV_URL_Find: TSpeedButton
        Left = 369
        Top = 169
        Width = 22
        Height = 21
        Hint = 'Ir a la Pagina'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          33333FFFFFFFFFFFFFFF000000000000000077777777777777770FFFFFFFFFFF
          FFF07F3FF3FF3FFF3FF70F00F00F000F00F07F773773777377370FFFFFFFFFFF
          FFF07F3FF3FF33FFFFF70F00F00FF00000F07F773773377777F70FEEEEEFF0F9
          FCF07F33333337F7F7F70FFFFFFFF0F9FCF07F3FFFF337F737F70F0000FFF0FF
          FCF07F7777F337F337370F0000FFF0FFFFF07F777733373333370FFFFFFFFFFF
          FFF07FFFFFFFFFFFFFF70CCCCCCCCCCCCCC07777777777777777088CCCCCCCCC
          C880733777777777733700000000000000007777777777777777333333333333
          3333333333333333333333333333333333333333333333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = EV_URL_FindClick
      end
      object EV_HORA: TZetaDBHora
        Left = 57
        Top = 2
        Width = 41
        Height = 21
        EditMask = '99:99;0'
        TabOrder = 0
        Text = '    '
        Tope = 24
        Valor = '    '
        DataField = 'EV_HORA'
        DataSource = DataSource
      end
      object EV_AUTOR: TZetaDBEdit
        Left = 57
        Top = 74
        Width = 241
        Height = 21
        MaxLength = 30
        TabOrder = 3
        Text = 'EV_AUTOR'
        DataField = 'EV_AUTOR'
        DataSource = DataSource
      end
      object EV_CLASIFI: TZetaDBKeyLookup
        Left = 57
        Top = 97
        Width = 337
        Height = 21
        LookupDataset = dmCliente.cdsTipoEvento
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        DataField = 'EV_CLASIFI'
        DataSource = DataSource
      end
      object EV_IMAGEN: TZetaDBEdit
        Left = 57
        Top = 145
        Width = 309
        Height = 21
        TabOrder = 6
        Text = 'EV_IMAGEN'
        DataField = 'EV_IMAGEN'
        DataSource = DataSource
      end
      object EV_LUGAR: TZetaDBEdit
        Left = 57
        Top = 121
        Width = 309
        Height = 21
        MaxLength = 70
        TabOrder = 5
        Text = 'EV_LUGAR'
        DataField = 'EV_LUGAR'
        DataSource = DataSource
      end
      object EV_FEC_INI: TZetaDBFecha
        Left = 57
        Top = 26
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 1
        Text = '04/Apr/02'
        Valor = 37350
        DataField = 'EV_FEC_INI'
        DataSource = DataSource
      end
      object EV_FEC_FIN: TZetaDBFecha
        Left = 57
        Top = 50
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 2
        Text = '05/Apr/02'
        Valor = 37351
        DataField = 'EV_FEC_FIN'
        DataSource = DataSource
      end
      object EV_URL: TZetaDBEdit
        Left = 57
        Top = 169
        Width = 310
        Height = 21
        TabOrder = 7
        Text = 'EV_URL'
        DataField = 'EV_URL'
        DataSource = DataSource
      end
    end
    object DescEv_TS: TTabSheet
      Caption = 'Descripci�n del Evento'
      ImageIndex = 1
      object EV_MEMO: TDBMemo
        Left = 0
        Top = 0
        Width = 385
        Height = 218
        Align = alClient
        DataField = 'EV_MEMO'
        DataSource = DataSource
        ScrollBars = ssVertical
        TabOrder = 0
      end
      object Panel1: TPanel
        Left = 385
        Top = 0
        Width = 27
        Height = 218
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object Find_Html_Btn: TSpeedButton
          Left = 3
          Top = 2
          Width = 24
          Height = 25
          Hint = 'Buscar Archivos Html'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
            300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
            330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
            333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
            339977FF777777773377000BFB03333333337773FF733333333F333000333333
            3300333777333333337733333333333333003333333333333377333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          OnClick = Find_Html_BtnClick
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 188
    Top = 121
  end
  object DlgFileOpen: TOpenDialog
    Filter = 
      'Archivos HTM* (*.htm*)|*.htm*|Archivos TXT (*.txt)|*.txt|Todos (' +
      '*.*)|*.*'
    Left = 332
    Top = 196
  end
end
