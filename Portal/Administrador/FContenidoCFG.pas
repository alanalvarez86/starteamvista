unit FContenidoCFG;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls, Spin, Outlook, ImgList,
     ZetaPortalClasses,
     FCamposMgr,
     ZBaseDlgModal;

type
  TContenidoCFG = class(TZetaDlgModal)
    PanelCentral: TPanel;
    PaginaLBL: TLabel;
    ColumnaLBL: TLabel;
    Pagina: TComboBox;
    Columna: TComboBox;
    ContenidoGB: TGroupBox;
    Contenidos: TListBox;
    PanelAcciones: TPanel;
    AgregarGB: TGroupBox;
    AgregarTitular: TBitBtn;
    AgregarReporte: TBitBtn;
    AgregarGrafica: TBitBtn;
    AgregarEvento: TBitBtn;
    AgregarNoticia: TBitBtn;
    PaginaEdit: TBitBtn;
    Borrar: TBitBtn;
    Modificar: TBitBtn;
    Subir: TBitBtn;
    Bajar: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure SubirClick(Sender: TObject);
    procedure BajarClick(Sender: TObject);
    procedure ContenidosClick(Sender: TObject);
    procedure AgregarTitularClick(Sender: TObject);
    procedure AgregarReporteClick(Sender: TObject);
    procedure AgregarGraficaClick(Sender: TObject);
    procedure AgregarEventoClick(Sender: TObject);
    procedure AgregarNoticiaClick(Sender: TObject);
    procedure BorrarClick(Sender: TObject);
    procedure ModificarClick(Sender: TObject);
    procedure PaginaEditClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure PaginaClick(Sender: TObject);
    procedure ColumnaClick(Sender: TObject);
  private
    { Private declarations }
    FChanged: Boolean;
    function EditarContenido(oValue: TContenido; const lAgregar: Boolean; const iUsuario: Integer): Boolean;
    function GetListaContenidos: TListaContenidos;
    procedure AgregarContenido(const eTipo: eContenido);
    procedure Conectar;
    procedure ConectarColumnas;
    procedure SetControls;
    procedure SetColumnaActiva;
  protected
    { Protected declarations }
    property ListaContenidos: TListaContenidos read GetListaContenidos;
  public
    { Public declarations }
  end;

var
  ContenidoCFG: TContenidoCFG;

implementation

uses DCliente,
     ZetaCommonTools,
     FPaginaCFG,
     FContenidoEditar,
     FContenidoEditarEvento,
     FContenidoEditarNoticia,
     FContenidoEditarGrafica,
     FContenidoEditarReporte;

{$R *.DFM}

{ *********  TContenidoCFG  ************ }

procedure TContenidoCFG.FormShow(Sender: TObject);
begin
     Conectar;
end;

procedure TContenidoCFG.Conectar;
var
   i: Integer;
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with Pagina do
        begin
             with Items do
             begin
                  BeginUpdate;
                  try
                     Clear;
                     with ListaContenidos.Paginas do
                     begin
                          for i := 0 to ( Count - 1 ) do
                          begin
                               Add( Pagina[ i ].Titulo );
                          end;
                     end;
                  finally
                         EndUpdate;
                  end;
             end;
             ItemIndex := ListaContenidos.PaginaActiva - 1;
        end;
        ConectarColumnas;
        FChanged := False;
        SetControls;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TContenidoCFG.ConectarColumnas;
var
   i: Integer;
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with Columna do
        begin
             with Items do
             begin
                  BeginUpdate;
                  try
                     Clear;
                     with ListaContenidos do
                     begin
                          with Paginas.Pagina[ PaginaActiva - 1 ] do
                          begin
                               for i := 1 to Columnas do
                               begin
                                    Add( Format( 'Columna # %d', [ i ] ) );
                               end;
                          end;
                          SetColumnaActiva;
                     end;
                  finally
                         EndUpdate;
                  end;
             end;
        end;
        with Contenidos do
        begin
             ListaContenidos.Cargar( Items );
             if ( Items.Count > 0 ) then
                ItemIndex := 0;
        end;
        FChanged := False;
        SetControls;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TContenidoCFG.SetColumnaActiva;
begin
     Self.Columna.ItemIndex := ListaContenidos.ColumnaActiva - 1;
end;

function TContenidoCFG.GetListaContenidos: TListaContenidos;
begin
     Result := dmCliente.ListaContenidos;
end;

procedure TContenidoCFG.SetControls;
begin
     with Contenidos do
     begin
          with Items do
          begin
               Subir.Enabled := ( ItemIndex > 0 );
               Bajar.Enabled := ( ItemIndex < ( Count - 1 ) );
               Borrar.Enabled := ( Count > 0 );
               Modificar.Enabled := Borrar.Enabled;
          end;
     end;
     PaginaLBl.Enabled := not FChanged;
     Pagina.Enabled := not FChanged;
     PaginaEdit.Enabled := not FChanged;
     ColumnaLBl.Enabled := not FChanged;
     Columna.Enabled := not FChanged;
     if FChanged then
     begin
          OK.Visible := True;
          with Cancelar do
          begin
               Kind := bkCancel;
               ModalResult := mrNone;
               Cancel := False;
               Caption := '&Cancelar';
               Hint := 'Cancelar Cambios';
          end;
     end
     else
     begin
          OK.Visible := False;
          with Cancelar do
          begin
               Kind := bkClose;
               ModalResult := mrNone;
               Cancel := True;
               Caption := '&Salir';
               Hint := 'Cerrar Pantalla y Salir';
          end;
     end;
     Application.ProcessMessages;
end;

procedure TContenidoCFG.SubirClick(Sender: TObject);
var
   i: Integer;
begin
     inherited;
     with Contenidos do
     begin
          i := ItemIndex;
          if ( i > 0 ) then
          begin
               Items.Exchange( i, i - 1 );
          end;
          ItemIndex := i - 1;
          FChanged := True;
          SetControls;
     end;
end;

procedure TContenidoCFG.BajarClick(Sender: TObject);
var
   i: Integer;
begin
     inherited;
     with Contenidos do
     begin
          i := ItemIndex;
          with Items do
          begin
               if ( i < ( Count - 1 ) ) then
               begin
                    Exchange( i, i + 1 );
               end;
          end;
          ItemIndex := i + 1;
          FChanged := True;
          SetControls;
     end;
end;

procedure TContenidoCFG.ContenidosClick(Sender: TObject);
begin
     inherited;
     SetControls;
end;

function TContenidoCFG.EditarContenido( oValue: TContenido; const lAgregar: Boolean; const iUsuario: Integer ): Boolean;
var
   oForma: TContenidoEditar;

function GetForma: TContenidoEditarClass;
begin
     case oValue.Tipo of
          ecReporte: Result := TContenidoEditarReporte;
          ecGrafica: Result := TContenidoEditarGrafica;
          ecNoticia: Result := TContenidoEditarNoticia;
          ecEvento: Result := TContenidoEditarEvento;
     else
         Result := nil;
     end;
end;

begin
     if ( oValue.Tipo = ecTitular ) then
        Result := True
     else
     begin
          oForma := GetForma.Create( Self ) as TContenidoEditar;
          try
             with oForma do
             begin
                  Agregando := lAgregar;
                  Usuario := iUsuario;
                  Contenido := oValue;
                  ShowModal;
                  Result := ( ModalResult = mrOk );
             end;
          finally
                 FreeAndNil( oForma );
          end;
     end;
end;

procedure TContenidoCFG.AgregarContenido( const eTipo: eContenido );
var
   oContenido: TContenido;
   iPtr: Integer;
begin
     inherited;
     oContenido := TContenido.Create;
     try
        with oContenido do
        begin
             Tipo := eTipo;
        end;
        with ListaContenidos do
        begin
             if EditarContenido( oContenido, True, Usuario ) then
             begin
                  iPtr := AgregaContenido( oContenido );
                  if ( iPtr >= 0 ) then
                  begin
                       with Self.Contenidos do
                       begin
                            Items.AddObject( Contenido[ iPtr ].Nombre, Contenido[ iPtr ] );
                            ItemIndex := Items.Count - 1;
                       end;
                       FChanged := True;
                       SetControls;
                  end;
             end;
        end;
     finally
            FreeAndNil( oContenido );
     end;
end;

procedure TContenidoCFG.AgregarTitularClick(Sender: TObject);
begin
     inherited;
     AgregarContenido( ecTitular );
end;

procedure TContenidoCFG.AgregarReporteClick(Sender: TObject);
begin
     inherited;
     AgregarContenido( ecReporte );
end;

procedure TContenidoCFG.AgregarGraficaClick(Sender: TObject);
begin
     inherited;
     AgregarContenido( ecGrafica );
end;

procedure TContenidoCFG.AgregarEventoClick(Sender: TObject);
begin
     inherited;
     AgregarContenido( ecEvento );
end;

procedure TContenidoCFG.AgregarNoticiaClick(Sender: TObject);
begin
     inherited;
     AgregarContenido( ecNoticia );
end;

procedure TContenidoCFG.BorrarClick(Sender: TObject);
var
   i, iPtr: Integer;
begin
     inherited;
     with Contenidos do
     begin
          i := ItemIndex;
          if ( i >= 0 ) then
          begin
               with Items do
               begin
                    iPtr := TContenido( Objects[ i ] ).Orden;
                    if ( iPtr >= 0 ) then
                    begin
                         ListaContenidos.Borrar( iPtr );
                         Delete( i );
                         ItemIndex := ZetaCommonTools.iMax( 0, i - 1 );
                         FChanged := True;
                         SetControls;
                    end;
               end;
          end;
     end;
end;

procedure TContenidoCFG.ModificarClick(Sender: TObject);
var
   i: Integer;
   oContenido: TContenido;
begin
     inherited;
     with Contenidos do
     begin
          i := ItemIndex;
          if ( i >= 0 ) then
          begin
               with Items do
               begin
                    oContenido := TContenido( Objects[ i ] );
                    if EditarContenido( oContenido, False, ListaContenidos.Usuario ) then
                    begin
                         Strings[ i ] := oContenido.Nombre;
                         FChanged := True;
                         SetControls;
                    end;
               end;
          end;
     end;
end;

procedure TContenidoCFG.PaginaEditClick(Sender: TObject);
begin
     inherited;
     if dmCliente.EditarPaginasUsuario then
        Conectar;
end;

procedure TContenidoCFG.PaginaClick(Sender: TObject);
var
   iValor: Integer;
begin
     inherited;
     iValor := Self.Pagina.ItemIndex + 1; { � + 1 es Cr�tico ! }
     with ListaContenidos do
     begin
          if ( iValor <> PaginaActiva ) then
          begin
               PaginaActiva := iValor;
               ColumnaActiva := K_PRIMERA_COLUMNA;
               SetColumnaActiva;
               if dmCliente.ContenidoRecargar( PaginaActiva, ColumnaActiva ) then
                  Conectar;
          end;
     end;
end;

procedure TContenidoCFG.ColumnaClick(Sender: TObject);
var
   iValor: Integer;
begin
     inherited;
     iValor := Self.Columna.ItemIndex + 1; { � + 1 es Cr�tico ! }
     with ListaContenidos do
     begin
          if ( iValor <> ColumnaActiva ) then
          begin
               ColumnaActiva := iValor;
               if dmCliente.ContenidoRecargar( PaginaActiva, ColumnaActiva ) then
                  Conectar;
          end;
     end;
end;

procedure TContenidoCFG.OKClick(Sender: TObject);
var
   oCursor: TCursor;
   i: Integer;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with Self.Contenidos.Items do
        begin
             for i := 0 to ( Count - 1 ) do
             begin
                  TContenido( Objects[ i ] ).Orden := i + 1; { � + 1 es Cr�tico ! }
             end;
        end;
        if dmCliente.DescargarContenido then
        begin
             FChanged := False;
             SetControls;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TContenidoCFG.CancelarClick(Sender: TObject);
begin
     inherited;
     if FChanged then
     begin
          with ListaContenidos do
          begin
               if dmCliente.ContenidoRecargar( PaginaActiva, ColumnaActiva ) then
                  Conectar;
          end;
     end;
end;

end.
