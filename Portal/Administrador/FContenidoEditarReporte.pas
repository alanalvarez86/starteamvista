unit FContenidoEditarReporte;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls,
     FContenidoEditar, Mask, ZetaNumero, ZetaKeyLookup;

type
  TContenidoEditarReporte = class(TContenidoEditar)
    ReporteLBL: TLabel;
    MostrarLBL: TLabel;
    Reporte: TZetaKeyLookup;
    Mostrar: TZetaNumero;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
    function GetFiltro: String;
  public
    { Public declarations }
  end;

var
  ContenidoEditarReporte: TContenidoEditarReporte;

implementation

uses DCliente,
     ZetaDialogo;

{$R *.DFM}

procedure TContenidoEditarReporte.FormCreate(Sender: TObject);
begin
     inherited;
     //HelpContext:= H00006_Catalogo_de_Diagnosticos;
end;

procedure TContenidoEditarReporte.FormShow(Sender: TObject);
begin
     inherited;
     Caption := GetCaption( 'Reporte' );
     with Contenido do
     begin
          Self.Mostrar.Valor := 3;
          with Self.Reporte do
          begin
               LookupDataset.Conectar;
               Filtro := GetFiltro;
               Valor := Reporte;
          end;
     end;
     ActiveControl := Reporte;
end;

function TContenidoEditarReporte.GetFiltro: String;
begin
     Result := Format('( ( RP_USUARIO = 0 ) or ( RP_USUARIO = %d ) ) and ( RP_TIPO = %d )',[ Usuario, Ord( eTipoReporte ) ] );
end;

procedure TContenidoEditarReporte.OKClick(Sender: TObject);
begin
     inherited;
     if Reporte.Valor = 0 then
     begin
          ZetaDialogo.ZError( Caption, 'Reporte No Puede Quedar Vac�o', 0 );
          ActiveControl := Reporte;
     end
     else
     begin
          if ( Mostrar.Valor < 3 ) then
          begin
               ZetaDialogo.ZError( Caption, 'El N�mero De Rengl�nes No Puede Ser Menor de 3', 0 );
               ActiveControl := Reporte;
          end
          else
          begin
               with Contenido do
               begin
                    Mostrar := Self.Mostrar.ValorEntero;
                    Reporte := Self.Reporte.Valor;
                    Descripcion := Self.Reporte.Descripcion;
               end;
               ModalResult := mrOk;
          end;
     end;
end;

end.
