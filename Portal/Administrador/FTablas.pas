unit FTablas;

interface

uses ZBaseTablasConsulta;

type
  TTOTipoNoticias = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOTipoEvento = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOTipoDocumento = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;

implementation

uses DCliente,
     ZetaBuscador;

{ ****** TTOTipoNoticias ****** }

procedure TTOTipoNoticias.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmCliente.cdsTipoNoticias;
     //HelpContext := H70712_En_donde_vive;
end;

{ TTOTipoEvento }

procedure TTOTipoEvento.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmCliente.cdsTipoEvento;
     //HelpContext := H70711_Estado_civil;
end;

{ TTOTipoDocumento }

procedure TTOTipoDocumento.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmCliente.cdsTipoDocumento;
     //HelpContext := H70713_Con_quien_vive;
end;

end.
