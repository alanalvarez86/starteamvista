unit FEditUsuarios;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaNumero, Mask;

type
  TEditUsuarios = class(TBaseEdicion)
    Label1: TLabel;
    Label2: TLabel;
    US_NOMBRE: TDBEdit;
    US_CODIGO: TZetaDBNumero;
    Label3: TLabel;
    Label4: TLabel;
    US_CORTO: TDBEdit;
    US_PASSWRD: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure DoLookup; override;
    function PuedeAgregar(var sMensaje: String): Boolean;override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    function PuedeModificar(var sMensaje: String): Boolean; override;
  public
    { Public declarations }
  end;

var
  EditUsuarios: TEditUsuarios;

implementation

uses DCliente,
     ZetaBuscador;

{$R *.DFM}

{ TEditUsuarios }

procedure TEditUsuarios.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := US_CODIGO;
     //MA:Pendiente
     //HelpContext:= H60617_Tipo_contrato;
end;

procedure TEditUsuarios.Connect;
begin
     with dmCliente do
     begin
          cdsUsuarios.Conectar;
          DataSource.DataSet:= cdsUsuarios;
     end;
end;

procedure TEditUsuarios.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'N�mero', 'Usuarios', 'US_CODIGO', dmCliente.cdsUsuarios );
end;

procedure TEditUsuarios.FormShow(Sender: TObject);
begin
     inherited;
     agregarbtn.Enabled := False;
     borrarbtn.Enabled := False;
     modificarbtn.Enabled := False;
end;

function TEditUsuarios.PuedeAgregar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se pueden agregar usuarios';
     Result := False;
end;

function TEditUsuarios.PuedeBorrar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se pueden borrar usuarios';
     Result := False;
end;

function TEditUsuarios.PuedeModificar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se pueden modificar usuarios';
     Result := False;
end;

end.
