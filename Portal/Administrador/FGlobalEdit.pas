unit FGlobalEdit;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, ComCtrls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, FileCtrl,
     ZBaseGlobal,
     ZetaKeyLookup,
     ZetaNumero;

type
  TGlobalEdit = class(TBaseGlobal)
    OpenDialog: TOpenDialog;
    PageControl: TPageControl;
    DatosPortal: TTabSheet;
    DatosEmpresa: TTabSheet;
    DatosMail: TTabSheet;
    NombreLBL: TLabel;
    Nombre: TEdit;
    PaginadosLBL: TLabel;
    EmpresaLBL: TLabel;
    CiudadLBL: TLabel;
    EstadoLBL: TLabel;
    DireccionLBL: TLabel;
    Empresa: TEdit;
    Direccion: TEdit;
    Ciudad: TEdit;
    Estado: TEdit;
    TitularLBL: TLabel;
    Titular: TZetaKeyLookup;
    Paginados: TZetaNumero;
    PageRefreshLBL: TLabel;
    PageRefresh: TZetaNumero;
    eMailsLBL: TLabel;
    eMailServerLBL: TLabel;
    AutoResponseLBL: TLabel;
    AutoResponse: TEdit;
    eMailServer: TEdit;
    eMails: TEdit;
    TS_Directorios: TTabSheet;
    FotosFolderLBL: TLabel;
    FotosFolder: TEdit;
    LogoBig: TEdit;
    LogoBigLBL: TLabel;
    LogoChicoLBL: TLabel;
    LogoChico: TEdit;
    Label1: TLabel;
    FotosDir_Ed: TEdit;
    DocsDir_Ed: TEdit;
    Label2: TLabel;
    DirFotos_Btn: TSpeedButton;
    DirDocs_Btn: TSpeedButton;
    DocFolderLBL: TLabel;
    DocFolder: TEdit;
    lblURL: TLabel;
    edURL: TEdit;
    Label3: TLabel;
    lblEmpdisco: TLabel;
    edEmpDisco: TEdit;
    btnempdir: TSpeedButton;
    RootFolderLBL: TLabel;
    RootFolder: TEdit;
    RootFolderFind: TSpeedButton;
    lbldirvirtual: TLabel;
    edEmpvirtual: TEdit;
    lblempvirtual: TLabel;
    edDirVirtual: TEdit;
    Label4: TLabel;
    UserDeflkp: TZetaKeyLookup;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RootFolderFindClick(Sender: TObject);
    procedure DocFolderFindClick(Sender: TObject);
    procedure FotosFolderFindClick(Sender: TObject);
    procedure LogoBigFindClick(Sender: TObject);
    procedure LogoChicoFindClick(Sender: TObject);
    procedure DirFotos_BtnClick(Sender: TObject);
    procedure DirDocs_BtnClick(Sender: TObject);
    procedure btnempdirClick(Sender: TObject);
  private
    { Private declarations }
    function BuscaDirectorio( sValor: String): String;
    //function BuscaLogo(const sFileName: String): String;
  public
    { Public declarations }
    procedure Descargar; override;
  end;

var
  GlobalEdit: TGlobalEdit;

implementation

uses DCliente,
     ZetaCommonTools,
     ZGlobalTress;

{$R *.DFM}

procedure TGlobalEdit.FormCreate(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsNoticias.Conectar;
     end;
     Nombre.Tag := K_GLOBAL_NOMBRE;
     RootFolder.Tag := K_GLOBAL_ROOT;
     DocFolder.Tag := K_GLOBAL_RUTA_DOC;
     FotosFolder.Tag := K_GLOBAL_FOTOS;
     Empresa.Tag := K_GLOBAL_EMPRESA;
     Direccion.Tag := K_GLOBAL_DIRECCION;
     Ciudad.Tag := K_GLOBAL_CIUDAD;
     Estado.Tag := K_GLOBAL_ESTADO;
     LogoBig.Tag := K_GLOBAL_LOGOBIG;
     LogoChico.Tag := K_GLOBAL_LOGOSMALL;
     Titular.Tag := K_GLOBAL_TITULAR;
     Paginados.Tag := K_GLOBAL_PAGINADOS;
     PageRefresh.Tag := K_GLOBAL_PAGE_REFRESH;
     eMails.Tag := K_GLOBAL_EMAIL_DEFAULT;
     eMailServer.Tag := K_GLOBAL_EMAIL_SERVER;
     AutoResponse.Tag := K_GLOBAL_EMAIL_AUTO;
     FotosDir_Ed.Tag := K_GLOBAL_DIR_FOTOS;
     DocsDir_Ed.Tag := K_GLOBAL_DIR_DOCS;
     edURL.Tag := K_GLOBAL_RUTA_PAGINA;
     edDirVirtual.Tag := K_GLOBAL_RUTA_PORTAL;
     edEmpvirtual.Tag := K_GLOBAL_RUTA_EMPLEADOS;
     edEmpDisco.Tag := K_GLOBAL_DIR_EMPLEADOS;
     //eduserDef.Tag := K_GLOBAL_USER_DEFAULT;
     UserDeflkp.Tag := K_GLOBAL_USER_DEFAULT;
     inherited;
     UserDeflkp.LookupDataset := dmCliente.cdsUsuarios;
     HelpContext:= 0;
end;

procedure TGlobalEdit.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage := DatosPortal;
     Nombre.SetFocus;
end;

{function TGlobalEdit.BuscaLogo( const sFileName: String ): String;
begin
     Result := sFileName;
     with OpenDialog do
     begin
          InitialDir := ExtractFilePath( Text );
          FileName := ExtractFileName( Text );
          if Execute then
             Result := FileName;
     end;
end;}

function TGlobalEdit.BuscaDirectorio( sValor: String ): String;
var
   sDirectory: String;
begin
     Result := sValor;
     if FileCtrl.SelectDirectory('Escoja Un Directorio', sValor, sDirectory ) then//( sValor, [], 0 ) then
        Result := sDirectory;
end;

procedure TGlobalEdit.RootFolderFindClick(Sender: TObject);
begin
     inherited;
     with RootFolder do
     begin
          Text := BuscaDirectorio( Text );
     end;
end;

procedure TGlobalEdit.DocFolderFindClick(Sender: TObject);
begin
     {inherited;
     with DocFolder do
     begin
          Text := BuscaDirectorio( Text );
     end;}
end;

procedure TGlobalEdit.FotosFolderFindClick(Sender: TObject);
begin
     {inherited;
     with FotosFolder do
     begin
          Text := BuscaDirectorio( Text );
     end;}
end;

procedure TGlobalEdit.LogoBigFindClick(Sender: TObject);
begin
     {inherited;
     with LogoBig do
     begin
          Text := BuscaLogo( Text );
     end;}
end;

procedure TGlobalEdit.LogoChicoFindClick(Sender: TObject);
begin
     {inherited;
     with LogoChico do
     begin
          Text := BuscaLogo( Text );
     end;}
end;

procedure TGlobalEdit.DirFotos_BtnClick(Sender: TObject);
var
   sRuta_New: String;
begin
     with FotosDir_Ed do
     begin
          sRuta_New := BuscaDirectorio( '' );
          if not strVacio( sRuta_New ) then
             Text := sRuta_New;
     end;
end;

procedure TGlobalEdit.DirDocs_BtnClick(Sender: TObject);
var
   sRuta_New: String;
begin
     with DocsDir_Ed do
     begin
          sRuta_New := BuscaDirectorio( '' );
          if not strVacio( sRuta_New ) then
             Text := sRuta_New;
     end;
end;

procedure TGlobalEdit.btnempdirClick(Sender: TObject);
var
   sRuta_New: String;
begin
     with edEmpDisco do
     begin
          sRuta_New := BuscaDirectorio( '' );
          if not strVacio( sRuta_New ) then
             Text := sRuta_New;
     end;
end;

procedure TGlobalEdit.Descargar;

procedure TrimEdit( Control: TEdit );
begin
     with Control do
     begin
          Text := Trim( Text );
     end;
end;

begin
     TrimEdit( Nombre );
     TrimEdit( RootFolder );
     TrimEdit( DocFolder );
     TrimEdit( FotosFolder );
     TrimEdit( Empresa );
     TrimEdit( Direccion );
     TrimEdit( Ciudad );
     TrimEdit( Estado );
     TrimEdit( LogoBig );
     TrimEdit( LogoChico );
     TrimEdit( eMails );
     TrimEdit( eMailServer );
     TrimEdit( AutoResponse );
     TrimEdit( FotosDir_Ed );
     TrimEdit( DocsDir_Ed );
     TrimEdit( edURL );
     TrimEdit( edDirVirtual );
     TrimEdit( edEmpvirtual );
     TrimEdit( edEmpDisco );
     inherited Descargar;
end;

end.
