unit FPaginaCFG;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls,
     ZBaseDlgModal,
     FCamposMgr;

type
  TPaginaCFG = class(TZetaDlgModal)
    PanelAcciones: TPanel;
    Agregar: TBitBtn;
    Borrar: TBitBtn;
    Modificar: TBitBtn;
    Subir: TBitBtn;
    Bajar: TBitBtn;
    Paginas: TListBox;
    procedure FormShow(Sender: TObject);
    procedure AgregarClick(Sender: TObject);
    procedure BorrarClick(Sender: TObject);
    procedure ModificarClick(Sender: TObject);
    procedure SubirClick(Sender: TObject);
    procedure BajarClick(Sender: TObject);
    procedure PaginasClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
  private
    { Private declarations }
    FPaginas: TListaPaginas;
    FChanged: Boolean;
    procedure SetControls;
  protected
   { Protected declarations }
  public
    { Public declarations }
    property ListaPaginas: TListaPaginas read FPaginas;
    procedure Conectar;
  end;

var
  PaginaCFG: TPaginaCFG;

implementation

uses FEditarPagina,
     DCliente,
     ZetaPortalClasses,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo;

{$R *.DFM}

{ TPaginaCFG }

procedure TPaginaCFG.FormCreate(Sender: TObject);
begin
     FPaginas := TListaPaginas.Create;
     inherited;
end;

procedure TPaginaCFG.FormShow(Sender: TObject);
begin
     Conectar;
end;

procedure TPaginaCFG.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FPaginas );
     inherited;
end;

procedure TPaginaCFG.Conectar;
begin
     inherited;
     FPaginas.Assign( dmCliente.ListaContenidos.Paginas );
     with Self.Paginas do
     begin
          FPaginas.Cargar( Items );
          if ( Items.Count > 0 ) then
             ItemIndex := 0;
     end;
     FChanged := False;
     SetControls;
end;

procedure TPaginaCFG.SetControls;
begin
     with Paginas do
     begin
          with Items do
          begin
               Subir.Enabled := ( ItemIndex > 0 );
               Bajar.Enabled := ( ItemIndex < ( Count - 1 ) );
               Borrar.Enabled := ( Count > 0 );
               Modificar.Enabled := Borrar.Enabled;
          end;
     end;
     Agregar.Enabled := ( Paginas.Items.Count < 5 );
     if FChanged then
     begin
          OK.Visible := True;
          with Cancelar do
          begin
               Kind := bkCancel;
               ModalResult := mrNone;
               Cancel := False;
               Caption := '&Cancelar';
               Hint := 'Cancelar Cambios';
          end;
     end
     else
     begin
          OK.Visible := False;
          with Cancelar do
          begin
               Kind := bkClose;
               ModalResult := mrNone;
               Cancel := True;
               Caption := '&Salir';
               Hint := 'Cerrar Pantalla y Salir';
          end;
     end;
     Application.ProcessMessages;
end;

procedure TPaginaCFG.AgregarClick(Sender: TObject);
var
   oPagina: TPagina;
   iPtr: Integer;
begin
     oPagina := TPagina.Create;
     try
        if FEditarPagina.EditaPaginas( oPagina ) then
        begin
             with FPaginas do
             begin
                  iPtr := AgregaPagina( oPagina );
                  if ( iPtr >= 0 ) then
                  begin
                       with Self.Paginas do
                       begin
                            Items.AddObject( Pagina[ iPtr ].Titulo, Pagina[ iPtr ] );
                            ItemIndex := Items.Count - 1;
                       end;
                       FChanged := True;
                  end;
             end;
             SetControls;
        end;
     finally
            FreeAndNil( oPagina );
     end;
end;

procedure TPaginaCFG.BorrarClick(Sender: TObject);
var
   i, iPtr: Integer;
   lOk: Boolean;
begin
     inherited;
     with Paginas do
     begin
          i := ItemIndex;
          if ( i >= 0 ) then
          begin
               with Items do
               begin
                    with TPagina( Objects[ i ] ) do
                    begin
                         iPtr := Orden;
                         lOk := ( iPtr >= 0 ) and ( EsNueva or ZetaDialogo.zConfirm( '� Atenci�n !', 'Al Borrar Esta P�gina Tambi�n Se Borrar�n Todas Sus Columnas' + CR_LF + '� Desea Borrarla ?', 0, mbNo ) );
                    end;
                    if lOk then
                    begin
                         FPaginas.Borrar( iPtr );
                         Delete( i );
                         ItemIndex := ZetaCommonTools.iMax( 0, i - 1 );
                         FChanged := True;
                         SetControls;
                    end;
               end;
          end;
     end;
end;

procedure TPaginaCFG.ModificarClick(Sender: TObject);
var
   i: Integer;
   oPagina: TPagina;
begin
     inherited;
     with Paginas do
     begin
          i := ItemIndex;
          if ( i >= 0 ) then
          begin
               with Items do
               begin
                    oPagina := TPagina( Objects[ i ] );
                    if FEditarPagina.EditaPaginas( oPagina ) then
                    begin
                         Strings[ i ] := oPagina.Titulo;
                         FChanged := True;
                         SetControls;
                    end;
               end;
          end;
     end;
end;

procedure TPaginaCFG.SubirClick(Sender: TObject);
var
   i: Integer;
begin
     inherited;
     with Paginas do
     begin
          i := ItemIndex;
          if ( i > 0 ) then
          begin
               Items.Exchange( i, i - 1 );
          end;
          ItemIndex := i - 1;
          FChanged := True;
          SetControls;
     end;
end;

procedure TPaginaCFG.BajarClick(Sender: TObject);
var
   i: Integer;
begin
     inherited;
     with Paginas do
     begin
          i := ItemIndex;
          with Items do
          begin
               if ( i < ( Count - 1 ) ) then
               begin
                    Exchange( i, i + 1 );
               end;
          end;
          ItemIndex := i + 1;
          FChanged := True;
          SetControls;
     end;
end;

procedure TPaginaCFG.PaginasClick(Sender: TObject);
begin
     inherited;
     SetControls;
end;

procedure TPaginaCFG.OKClick(Sender: TObject);
var
   i: Integer;
begin
     inherited;
     with Self.Paginas.Items do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               TPagina( Objects[ i ] ).Orden := i + 1; { � + 1 es Cr�tico ! }
          end;
     end;
end;

procedure TPaginaCFG.CancelarClick(Sender: TObject);
begin
     inherited;
     if FChanged then
     begin
          Conectar;
     end;
end;

end.
