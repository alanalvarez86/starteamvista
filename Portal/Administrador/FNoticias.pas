unit FNoticias;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid;

type
  TNoticias = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  Noticias: TNoticias;

implementation

uses DCliente,
     ZetaBuscador;

{$R *.DFM}

procedure TNoticias.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     // PENDIENTE : HelpContext := H60612_Clasificaciones;
end;

procedure TNoticias.Connect;
begin
     with dmCliente do
     begin
          cdsTipoNoticias.Conectar;
          cdsNoticias.Conectar;
          DataSource.DataSet:= cdsNoticias;
     end;
end;

procedure TNoticias.Refresh;
begin
     dmCliente.cdsNoticias.Refrescar;
end;

procedure TNoticias.Agregar;
begin
     dmCliente.cdsNoticias.Agregar;
end;

procedure TNoticias.Modificar;
begin
     dmCliente.cdsNoticias.Modificar;
end;

procedure TNoticias.Borrar;
begin
     dmCliente.cdsNoticias.Borrar;
end;

procedure TNoticias.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Folio', 'Noticias', 'NT_FOLIO', dmCliente.cdsNoticias );
end;

end.
