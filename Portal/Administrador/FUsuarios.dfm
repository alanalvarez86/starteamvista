inherited Usuarios: TUsuarios
  Left = 482
  Top = 311
  Caption = 'Usuarios'
  ClientHeight = 299
  ClientWidth = 445
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 445
    inherited ValorActivo2: TPanel
      Width = 186
    end
  end
  object PanelSuperior: TPanel [1]
    Left = 0
    Top = 19
    Width = 445
    Height = 35
    Align = alTop
    TabOrder = 1
    object EditarContenido: TBitBtn
      Left = 4
      Top = 5
      Width = 114
      Height = 25
      Hint = 'Especificar Contenido De P�gina'
      Caption = 'Editar Contenido'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = EditarContenidoClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        5555555FFFFFFFFFF5555550000000000555557777777777F5555550FFFFFFFF
        0555557F5FFFF557F5555550F0000FFF0555557F77775557F5555550FFFFFFFF
        0555557F5FFFFFF7F5555550F000000F0555557F77777757F5555550FFFFFFFF
        0555557F5FFFFFF7F5555550F000000F0555557F77777757F5555550FFFFFFFF
        0555557F5FFF5557F5555550F000FFFF0555557F77755FF7F5555550FFFFF000
        0555557F5FF5777755555550F00FF0F05555557F77557F7555555550FFFFF005
        5555557FFFFF7755555555500000005555555577777775555555555555555555
        5555555555555555555555555555555555555555555555555555}
      NumGlyphs = 2
    end
  end
  object DBGrid1: TDBGrid [2]
    Left = 0
    Top = 54
    Width = 445
    Height = 245
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = EditarContenidoClick
    Columns = <
      item
        Expanded = False
        FieldName = 'US_CODIGO'
        Title.Caption = 'N�mero'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_NOMBRE'
        Title.Caption = 'Nombre'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_PAGINAS'
        Title.Caption = 'P�ginas'
        Width = 50
        Visible = True
      end>
  end
end
