unit FContenidoEditar;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ExtCtrls,
     FCamposMgr,
     ZBaseDlgModal;

type
  TContenidoEditar = class(TZetaDlgModal)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FAgregando: Boolean;
    FContenido: TContenido;
    FUsuario: Integer;
    procedure SetContenido(const Value: TContenido);
    procedure SetAgregando(const Value: Boolean);
  protected
    { Protected declarations }
    function GetCaption(const sItem: String): String;
  public
    { Public declarations }
    property Agregando: Boolean write SetAgregando;
    property Contenido: TContenido read FContenido write SetContenido;
    property Usuario: Integer read FUsuario write FUsuario;
  end;
  TContenidoEditarClass = class of TContenidoEditar;

implementation

uses DCliente,
     ZetaDialogo,
     FContenidoCFG;

{$R *.DFM}

{ ***** TContenidoEditar ***** }

procedure TContenidoEditar.FormCreate(Sender: TObject);
begin
     inherited;
     FAgregando := False;
end;

procedure TContenidoEditar.SetAgregando(const Value: Boolean);
begin
     FAgregando := Value;
end;

function TContenidoEditar.GetCaption( const sItem: String ): String;
begin
     if FAgregando then
        Result := Format( 'Agregar %s', [ sItem ] )
     else
        Result := Format( 'Editar %s', [ sItem ] );
end;

procedure TContenidoEditar.SetContenido(const Value: TContenido);
begin
     FContenido := Value;
end;

end.
