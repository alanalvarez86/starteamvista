unit FContenidoEditarNoticia;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Mask,
     FContenidoEditar,
     ZetaKeyLookup,
     ZetaNumero;

type
  TContenidoEditarNoticia = class(TContenidoEditar)
    Label1: TLabel;
    Noticia: TZetaKeyLookup;
    MostrarLBL: TLabel;
    Mostrar: TZetaNumero;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ContenidoEditarNoticia: TContenidoEditarNoticia;

implementation

uses ZetaDialogo;

{$R *.DFM}

procedure TContenidoEditarNoticia.FormCreate(Sender: TObject);
begin
     inherited;
     //HelpContext:= H00006_Catalogo_de_Diagnosticos;
end;

procedure TContenidoEditarNoticia.FormShow(Sender: TObject);
begin
     inherited;
     Caption := GetCaption( 'Noticia' );
     with Contenido do
     begin
          Self.Mostrar.Valor := Mostrar;
          with Self.Noticia do
          begin
               LookupDataset.Conectar;
               Llave := Clase;
          end;
     end;
     ActiveControl := Noticia;
end;

procedure TContenidoEditarNoticia.OKClick(Sender: TObject);
begin
     inherited;
     if ( Mostrar.Valor < 1 ) then
     begin
          ZetaDialogo.ZError( Caption, 'El Valor A Mostrar No Puede Ser Menor de 1', 0 );
          ActiveControl := Noticia;
     end
     else
     begin
          with Contenido do
          begin
               Mostrar := Self.Mostrar.ValorEntero;
               Clase := Self.Noticia.Llave;
               Descripcion := Self.Noticia.Descripcion;
          end;
          ModalResult := mrOk;
     end;
end;

end.
