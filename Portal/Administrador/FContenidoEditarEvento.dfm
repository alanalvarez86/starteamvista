inherited ContenidoEditarEvento: TContenidoEditarEvento
  Left = 392
  Top = 336
  ActiveControl = Evento
  Caption = 'Evento'
  ClientWidth = 387
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object EventoLBL: TLabel [0]
    Left = 8
    Top = 14
    Width = 78
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo De Evento:'
  end
  object MostrarLBL: TLabel [1]
    Left = 48
    Top = 36
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'Mostrar:'
  end
  inherited PanelBotones: TPanel
    Width = 387
    TabOrder = 2
    inherited OK: TBitBtn
      Left = 219
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 304
    end
  end
  object Evento: TZetaKeyLookup
    Left = 88
    Top = 10
    Width = 300
    Height = 21
    LookupDataset = dmCliente.cdsTipoEvento
    TabOrder = 0
    TabStop = True
    WidthLlave = 60
  end
  object Mostrar: TZetaNumero
    Left = 88
    Top = 32
    Width = 41
    Height = 21
    Mascara = mnDias
    TabOrder = 1
    Text = '0'
  end
end
