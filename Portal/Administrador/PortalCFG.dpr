program PortalCFG;

uses
  Forms,
  ZetaSplash,
  ZetaClientTools,
  ZBaseEdicion in '..\..\Tools\ZBaseEdicion.pas' {BaseEdicion},
  ZBaseConsulta in '..\..\Tools\ZBaseConsulta.pas' {BaseConsulta},
  ZBaseDlgModal in '..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  FTressShell in 'FTressShell.pas' {TressShell},
  FContenidoEditar in 'FContenidoEditar.pas' {ContenidoEditar},
  DCliente in 'DCliente.pas' {dmCliente: TDataModule},
  ZBaseGlobal in '..\..\Tools\ZBaseGlobal.pas' {BaseGlobal},
  DDiccionario in 'DDiccionario.pas',
  FDialogoImagenes in 'FDialogoImagenes.pas' {DialogoImagenes};

{$R *.RES}

procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;
  begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  SplashScreen := TSplashScreen.Create( Application );
  with SplashScreen do
  begin
       Show;
       Update;
  end;
  Application.Title := 'Portal Tress';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
  begin
       if Login( True ) then
       begin
            CierraSplash;
            Show;
            Update;
            //BeforeRun;
            Application.Run;
       end
       else
       begin
            CierraSplash;
       end;
       Free;
  end;
end.
