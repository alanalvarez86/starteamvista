inherited Eventos: TEventos
  Left = 291
  Top = 233
  Caption = 'Eventos'
  ClientWidth = 673
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 673
    inherited ValorActivo2: TPanel
      Width = 414
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 673
    Height = 254
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'EV_FOLIO'
        Title.Caption = 'Folio'
        Width = 85
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EV_FEC_INI'
        Title.Caption = 'Inicio'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EV_FEC_FIN'
        Title.Caption = 'Fin'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EV_HORA'
        Title.Caption = 'Hora'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EV_LUGAR'
        Title.Caption = 'Lugar'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EV_TITULO'
        Title.Caption = 'Titulo'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_NOMBRE'
        Title.Caption = 'Usuario'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EV_AUTOR'
        Title.Caption = 'Autor'
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TB_ELEMENT'
        Title.Caption = 'Tipo'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EV_IMAGEN'
        Title.Caption = 'Ruta Imagen'
        Width = 450
        Visible = True
      end>
  end
end
