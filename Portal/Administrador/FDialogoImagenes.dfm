object DialogoImagenes: TDialogoImagenes
  Left = 240
  Top = 203
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Seleccion de Archivos'
  ClientHeight = 278
  ClientWidth = 468
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 241
    Width = 468
    Height = 37
    Align = alBottom
    TabOrder = 1
    object OpenFolder_Btn: TSpeedButton
      Left = 4
      Top = 8
      Width = 32
      Height = 21
      Hint = 'Abrir el folder de fotografias'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        5555555555555555555555555555555555555555555555555555555555555555
        555555555555555555555555555555555555555FFFFFFFFFF555550000000000
        55555577777777775F55500B8B8B8B8B05555775F555555575F550F0B8B8B8B8
        B05557F75F555555575F50BF0B8B8B8B8B0557F575FFFFFFFF7F50FBF0000000
        000557F557777777777550BFBFBFBFB0555557F555555557F55550FBFBFBFBF0
        555557F555555FF7555550BFBFBF00055555575F555577755555550BFBF05555
        55555575FFF75555555555700007555555555557777555555555555555555555
        5555555555555555555555555555555555555555555555555555}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = OpenFolder_BtnClick
    end
    object Refrescar_Btn: TSpeedButton
      Left = 38
      Top = 8
      Width = 32
      Height = 21
      Hint = 'Refrescar Archivos'
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333333333333333333333338083333333333380083333
        3333333300833333333333380833333333333330033333333333333003330000
        0333333008333800033333380083800003333333000000080333333338000833
        0333333333333333333333333333333333333333333333333333}
      ParentShowHint = False
      ShowHint = True
      OnClick = Refrescar_BtnClick
    end
    object OK: TBitBtn
      Left = 309
      Top = 5
      Width = 75
      Height = 25
      Hint = 'Aceptar y Escribir Datos'
      Anchors = [akRight, akBottom]
      Caption = '&OK'
      ModalResult = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object Cancelar: TBitBtn
      Left = 387
      Top = 5
      Width = 75
      Height = 25
      Hint = 'Cancelar Cambios'
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Cancelar'
      ModalResult = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333333333000033338833333333333333333F333333333333
        0000333911833333983333333388F333333F3333000033391118333911833333
        38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
        911118111118333338F3338F833338F3000033333911111111833333338F3338
        3333F8330000333333911111183333333338F333333F83330000333333311111
        8333333333338F3333383333000033333339111183333333333338F333833333
        00003333339111118333333333333833338F3333000033333911181118333333
        33338333338F333300003333911183911183333333383338F338F33300003333
        9118333911183333338F33838F338F33000033333913333391113333338FF833
        38F338F300003333333333333919333333388333338FFF830000333333333333
        3333333333333333333888330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
  end
  object Imagen_Gb: TGroupBox
    Left = 233
    Top = 0
    Width = 235
    Height = 241
    Align = alClient
    Caption = ' Vista Previa '
    TabOrder = 0
    object Imagen: TImage
      Left = 2
      Top = 15
      Width = 231
      Height = 224
      Align = alClient
      AutoSize = True
      Center = True
      Stretch = True
      Transparent = True
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 233
    Height = 241
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 2
    object Label3: TLabel
      Left = 9
      Top = 8
      Width = 48
      Height = 13
      Caption = 'Directorio:'
    end
    object Label1: TLabel
      Left = 8
      Top = 29
      Width = 49
      Height = 13
      Alignment = taRightJustify
      Caption = 'Extension:'
    end
    object Directorio_Ztb: TZetaTextBox
      Left = 64
      Top = 6
      Width = 164
      Height = 17
      AutoSize = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 48
      Width = 233
      Height = 193
      Align = alBottom
      Caption = ' Archivos :'
      TabOrder = 1
      object ListaArchivos: TFileListBox
        Left = 2
        Top = 15
        Width = 229
        Height = 176
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemHeight = 13
        ParentFont = False
        TabOrder = 0
        OnClick = ListaArchivosClick
        OnDblClick = ListaArchivosDblClick
      end
    end
    object Ext_Img_Cb: TFilterComboBox
      Left = 63
      Top = 25
      Width = 168
      Height = 21
      FileList = ListaArchivos
      Filter = 
        'JPG Imagen (*.jpg)|*.jpg|JPEG Imagen (*.jpeg)|*.jpeg|BMP Bitmap ' +
        '(*.bmp)|*.bmp|GIF Imagen (*.gif)|*.gif|Todos (*.*)|*.*'
      TabOrder = 0
    end
    object Ext_Doc_Cb: TFilterComboBox
      Left = 63
      Top = 25
      Width = 168
      Height = 21
      FileList = ListaArchivos
      Filter = 
        'DOC Texto (*.doc)|*.doc|TXT Texto (*.txt)|*.txt|HTML Archivo (*.' +
        'html)|*.html|HTM Archivo (*.htm)|*.htm|Todos (*.*)|*.*'
      TabOrder = 2
    end
    object Ext_Web_Cb: TFilterComboBox
      Left = 63
      Top = 25
      Width = 168
      Height = 21
      FileList = ListaArchivos
      Filter = 
        'HTML Archivo (*.html)|*.html|HTM Archivo (*.htm)|*.htm|Todos (*.' +
        '*)|*.*'
      TabOrder = 3
    end
  end
end
