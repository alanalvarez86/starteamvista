inherited EditReportes: TEditReportes
  Left = 249
  Top = 191
  Caption = 'Reportes'
  ClientHeight = 286
  ClientWidth = 437
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 250
    Width = 437
    TabOrder = 4
    inherited OK: TBitBtn
      Left = 269
    end
    inherited Cancelar: TBitBtn
      Left = 354
    end
  end
  inherited PanelSuperior: TPanel
    Width = 437
    TabOrder = 0
  end
  inherited PanelIdentifica: TPanel
    Width = 437
    TabOrder = 1
    Visible = False
    inherited ValorActivo2: TPanel
      Width = 111
    end
  end
  object Titulo_Pn: TPanel [3]
    Left = 0
    Top = 51
    Width = 437
    Height = 54
    Align = alTop
    TabOrder = 2
    object Label1: TLabel
      Left = 35
      Top = 10
      Width = 25
      Height = 13
      Caption = 'Folio:'
    end
    object RP_FOLIO: TZetaDBTextBox
      Left = 66
      Top = 8
      Width = 105
      Height = 17
      AutoSize = False
      Caption = 'RP_FOLIO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'RP_FOLIO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label4: TLabel
      Left = 31
      Top = 29
      Width = 31
      Height = 13
      Caption = 'T�tulo:'
    end
    object RP_TITULO: TZetaDBEdit
      Left = 66
      Top = 25
      Width = 337
      Height = 21
      MaxLength = 70
      TabOrder = 0
      Text = 'RP_TITULO'
      DataField = 'RP_TITULO'
      DataSource = DataSource
    end
  end
  object PageControl: TPageControl [4]
    Left = 0
    Top = 105
    Width = 437
    Height = 145
    ActivePage = Generales_Ts
    Align = alClient
    TabOrder = 3
    object Generales_Ts: TTabSheet
      Caption = 'Generales'
      object Label8: TLabel
        Left = 22
        Top = 96
        Width = 38
        Height = 13
        Caption = 'Imagen:'
      end
      object Label7: TLabel
        Left = 21
        Top = 72
        Width = 39
        Height = 13
        Caption = 'Usuario:'
      end
      object RP_IMAGEN_Find: TSpeedButton
        Left = 378
        Top = 92
        Width = 21
        Height = 23
        Hint = 'Buscar Archivo de Imagen'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
          300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
          330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
          333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
          339977FF777777773377000BFB03333333337773FF733333333F333000333333
          3300333777333333337733333333333333003333333333333377333333333333
          333333333333333333FF33333333333330003333333333333777333333333333
          3000333333333333377733333333333333333333333333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = RP_IMAGEN_FindClick
      end
      object RP_USUARIO: TZetaDBKeyLookup
        Left = 62
        Top = 69
        Width = 337
        Height = 19
        LookupDataset = dmCliente.cdsUsuarios
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        DataField = 'RP_USUARIO'
        DataSource = DataSource
      end
      object RP_IMAGEN: TZetaDBEdit
        Left = 62
        Top = 92
        Width = 314
        Height = 21
        TabOrder = 2
        Text = 'RP_IMAGEN'
        DataField = 'RP_IMAGEN'
        DataSource = DataSource
      end
      object RP_TIPO: TDBRadioGroup
        Left = 15
        Top = 1
        Width = 106
        Height = 63
        Caption = ' Tipo '
        DataField = 'RP_TIPO'
        DataSource = DataSource
        Items.Strings = (
          'Reporte'
          'Gr�fica')
        TabOrder = 0
        Values.Strings = (
          '0'
          '1')
      end
      object GroupBox1: TGroupBox
        Left = 128
        Top = 1
        Width = 299
        Height = 64
        Caption = ' Ultima Generaci�n '
        TabOrder = 3
        object Label3: TLabel
          Left = 16
          Top = 40
          Width = 26
          Height = 13
          Caption = 'Hora:'
        end
        object RP_HORA: TZetaDBTextBox
          Left = 44
          Top = 38
          Width = 89
          Height = 17
          AutoSize = False
          Caption = 'RP_HORA'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'RP_HORA'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object RP_FECHA: TZetaDBTextBox
          Left = 44
          Top = 17
          Width = 89
          Height = 17
          AutoSize = False
          Caption = 'RP_FECHA'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'RP_FECHA'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label2: TLabel
          Left = 9
          Top = 19
          Width = 33
          Height = 13
          Caption = 'Fecha:'
        end
        object Label5: TLabel
          Left = 151
          Top = 40
          Width = 49
          Height = 13
          Caption = 'Columnas:'
        end
        object RP_COLS: TZetaDBTextBox
          Left = 202
          Top = 38
          Width = 89
          Height = 17
          AutoSize = False
          Caption = 'RP_COLS'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'RP_COLS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object RP_ROWS: TZetaDBTextBox
          Left = 202
          Top = 17
          Width = 89
          Height = 17
          AutoSize = False
          Caption = 'RP_ROWS'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'RP_ROWS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label6: TLabel
          Left = 146
          Top = 19
          Width = 54
          Height = 13
          Caption = 'Renglones:'
        end
      end
    end
    object Descripcion_Ts: TTabSheet
      Caption = 'Descripci�n'
      ImageIndex = 1
      object RP_DESCRIP: TDBMemo
        Left = 0
        Top = 0
        Width = 402
        Height = 117
        Align = alClient
        DataField = 'RP_DESCRIP'
        DataSource = DataSource
        ScrollBars = ssVertical
        TabOrder = 0
      end
      object Panel1: TPanel
        Left = 402
        Top = 0
        Width = 27
        Height = 117
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object Find_Html_Btn: TSpeedButton
          Left = 2
          Top = 2
          Width = 24
          Height = 25
          Hint = 'Buscar Archivos Html'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
            300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
            330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
            333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
            339977FF777777773377000BFB03333333337773FF733333333F333000333333
            3300333777333333337733333333333333003333333333333377333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          OnClick = Find_Html_BtnClick
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 188
    Top = 113
  end
  object DlgFileOpen: TOpenDialog
    Filter = 
      'Archivos HTM* (*.htm*)|*.htm*|Archivos TXT (*.txt)|*.txt|Todos (' +
      '*.*)|*.*'
    Left = 332
    Top = 196
  end
end
