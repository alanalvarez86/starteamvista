inherited EditarPagina: TEditarPagina
  Left = 285
  Top = 341
  Caption = 'Editar P�gina'
  ClientHeight = 105
  ClientWidth = 299
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object NoColLBL: TLabel [0]
    Left = 5
    Top = 41
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Columnas:'
  end
  object Label1: TLabel [1]
    Left = 23
    Top = 18
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'T�tulo:'
  end
  inherited PanelBotones: TPanel
    Top = 69
    Width = 299
    BevelOuter = bvNone
    TabOrder = 2
    inherited OK: TBitBtn
      Left = 131
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 216
    end
  end
  object PA_COLS: TComboBox
    Left = 56
    Top = 37
    Width = 137
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 1
    Items.Strings = (
      '1 Columna'
      '2 Columnas'
      '3 Columnas')
  end
  object PA_TITULO: TEdit
    Left = 56
    Top = 15
    Width = 233
    Height = 21
    MaxLength = 30
    TabOrder = 0
  end
end
