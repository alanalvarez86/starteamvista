unit FEditDocument;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaKeyLookup,
  ZetaEdit, ZetaHora, Mask, ZetaFecha, ZetaDBTextBox, ExtDlgs, ComCtrls;

type
  TEditDocumento = class(TBaseEdicion)
    Titulo_Pn: TPanel;
    Label1: TLabel;
    DC_FOLIO: TZetaDBTextBox;
    Label4: TLabel;
    DC_TITULO: TZetaDBEdit;
    PageControl: TPageControl;
    Generales_Ts: TTabSheet;
    Label3: TLabel;
    DC_HORA: TZetaDBHora;
    DC_FECHA: TZetaDBFecha;
    Label9: TLabel;
    Label6: TLabel;
    DC_AUTOR: TZetaDBEdit;
    Label11: TLabel;
    DC_USUARIO: TZetaDBTextBox;
    Label2: TLabel;
    DC_PALABRA: TZetaDBEdit;
    DC_RUTA: TZetaDBEdit;
    Label10: TLabel;
    Label7: TLabel;
    DC_CLASIFI: TZetaDBKeyLookup;
    DC_IMAGEN: TZetaDBEdit;
    Label8: TLabel;
    DC_RUTA_Find: TSpeedButton;
    DC_IMAGEN_Find: TSpeedButton;
    Descripcion_Ts: TTabSheet;
    DC_MEMO: TDBMemo;
    Panel1: TPanel;
    Find_Html_Btn: TSpeedButton;
    DlgFileOpen: TOpenDialog;
    procedure DC_IMAGEN_FindClick(Sender: TObject);
    procedure DC_RUTA_FindClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Find_Html_BtnClick(Sender: TObject);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  EditDocumento: TEditDocumento;

implementation

uses DCliente, DGlobal, ZetaBuscador, FDialogoImagenes, FTressShell;

{$R *.DFM}

procedure TEditDocumento.FormCreate(Sender: TObject);
begin
  inherited;
  FirstControl := DC_TITULO;
  DC_CLASIFI.LookupDataset := dmCliente.cdsTipoDocumento;
  //MA:Pendiente
  //HelpContext:= H60617_Tipo_contrato;
end;

procedure TEditDocumento.Connect;
begin
     with dmCliente do
     begin
          cdsDocumentos.Conectar;
          DataSource.DataSet:= cdsDocumentos;
     end;
end;

procedure TEditDocumento.DC_IMAGEN_FindClick(Sender: TObject);
begin
     with dmCliente do
     begin
          RutaImagenGlobal := Global.GetGlobalString( FindBtnImagen );
          with DialogoImagenes do
          begin
               LlamaDialogo(K_TIPO_IMAGEN,DC_IMAGEN.Text);
               if ( ModalResult = mrOk ) then
               begin
                    cdsDocumentos.Edit;
                    DC_IMAGEN.Text := ExtractFileName(ListaArchivos.FileName);
               end;
          end;
     end;
end;

procedure TEditDocumento.DC_RUTA_FindClick(Sender: TObject);
begin
     with dmCliente do
     begin
          RutaImagenGlobal := Global.GetGlobalString( FindBtnDocs );
     end;
     with DialogoImagenes do
     begin
          LlamaDialogo(K_TIPO_DOCS,'');
          if ( ModalResult = mrOk ) then
            DC_RUTA.Text := ExtractFileName(DialogoImagenes.ListaArchivos.FileName);
     end;
end;

procedure TEditDocumento.DoLookup;
begin
  inherited;
  ZetaBuscador.BuscarCodigo( 'Folio', 'Documentos', 'DC_FOLIO', dmCliente.cdsDocumentos );
end;

procedure TEditDocumento.Find_Html_BtnClick(Sender: TObject);
begin
     with dlgFileOpen do
     begin
          with dmCliente do
          begin
               InitialDir := RutaImagenGlobal;
               if Execute then
               begin
                    with DC_MEMO do
                    begin
                         Clear;
                         cdsDocumentos.Edit;
                         Lines.LoadFromFile(dlgFileOpen.FileName);
                    end;
               end;
          end;
     end;
end;

end.
