unit FPortalReportTress;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ImgList,
     Buttons, StdCtrls, Mask, ShellApi, CheckLst, ComObj, ActiveX, ExtCtrls, ComCtrls,
     {$ifdef DOS_CAPAS}
     DZetaServerProvider,
     {$endif}
     DReportesPortal,
     ZetaFecha,
     ZetaHora,
     ZetaKeyCombo,
     ZetaDBTextBox;

type
  TReportesTress = class(TForm)
    FechaDia: TLabel;
    ImageList: TImageList;
    PanelInferior: TPanel;
    Salir: TBitBtn;
    PageControl: TPageControl;
    Reportes: TTabSheet;
    Companies: TTabSheet;
    ReportalGB: TGroupBox;
    ListReportal: TTreeView;
    PanelBotoncitos: TPanel;
    ReporteBorrar: TSpeedButton;
    ReporteEditar: TSpeedButton;
    EmpresasGB: TGroupBox;
    Empresas: TCheckListBox;
    PanelReportes: TPanel;
    Generar: TBitBtn;
    Grabar: TBitBtn;
    Panel1: TPanel;
    GrabarEmpresas: TBitBtn;
    Refrescar: TSpeedButton;
    ServerPortalLBL: TLabel;
    ServerPortal: TZetaTextBox;
    ServerPortalFind: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ServerPortalFindClick(Sender: TObject);
    procedure GenerarClick(Sender: TObject);
    procedure RefrescarClick(Sender: TObject);
    procedure GrabarClick(Sender: TObject);
    procedure ReporteEditarClick(Sender: TObject);
    procedure ReporteBorrarClick(Sender: TObject);
    procedure EmpresasChange(Sender: TObject);
    procedure SalirClick(Sender: TObject);
    procedure GrabarEmpresasClick(Sender: TObject);
    procedure EmpresasClickCheck(Sender: TObject);
  private
    { Private declarations }
    FConnected: Boolean;
    FReportalLoad: Boolean;
    FPortalReportes: TListaReportesPortal;
    FCompanies: TStrings;
    FPortalChanged: Boolean;
    function GetEmpresas: String;
    procedure ListReportalBuild;
    procedure SetControls;
    procedure SetEmpresas( const sLista: String );
    procedure SetPortalServer(const sValue: String);
  protected
    { Protected declarations }
  public
    { Public declarations }
    procedure ProcesarBatch;
  end;

var
  ReportesTress: TReportesTress;

implementation

uses FViewLog,
     FPortalEdit,
     FPortalServer,
     DCliente,
     DPortalCaller,
     ZetaNetworkBrowser,
     ZetaWinAPITools,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaRegistryCliente,
     ZetaDialogo,
     ZetaMessages;

{$R *.DFM}

{ ********** TReportesMailTest ******** }

procedure TReportesTress.FormCreate(Sender: TObject);
begin
     FConnected := False;
     ZetaCommonTools.Environment;
     ZetaRegistryCliente.InitClientRegistry;
     Application.UpdateFormatSettings := FALSE;
     FPortalReportes := TListaReportesPortal.Create;
     FCompanies := TStringList.Create;
     FReportalLoad := False;
     FPortalChanged := False;
     {$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
     {$endif}
end;

procedure TReportesTress.FormShow(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        dmCliente := TdmCliente.Create( Self );
        try
           FConnected := dmCliente.ListaCompanies( FCompanies );
        finally
               FreeAndNil( dmCliente );
        end;
        if FConnected then
        begin
             ListReportalBuild;
        end;
        SetControls;
        PageControl.ActivePage := Reportes;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TReportesTress.FormDestroy(Sender: TObject);
begin
     {$ifdef DOS_CAPAS}
     DZetaServerProvider.FreeAll;
     {$endif}
     ZetaRegistryCliente.ClearClientRegistry;
     FreeAndNil( FCompanies );
     FreeAndNil( FPortalReportes );
end;

procedure TReportesTress.ProcesarBatch;
begin
     dmReportesPortal := TdmReportesPortal.Create( Self );
     try
        with dmReportesPortal do
        begin
             if FindCmdLineSwitch( 'PORTAL', [ '/', '-' ], True ) then
             begin
                  ReportesPortalProcesarTodos;
             end;
        end;
     finally
            FreeAndNil( dmReportesPortal );
     end;
end;

function TReportesTress.GetEmpresas: String;
var
   i: Integer;
begin
     Result := VACIO;
     with Empresas do
     begin
          with Items do
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    if Checked[ i ] then
                       Result := Result + FCompanies.Names[ i ] + ',';
               end;
          end;
     end;
     Result := ZetaCommonTools.CortaUltimo( Result );
end;

procedure TReportesTress.SetEmpresas( const sLista: String );
var
   i: Integer;
   sEmpresa: String;
   FEscogidas: TStrings;
begin
     FEscogidas := TStringList.Create;
     try
        FEscogidas.CommaText := sLista;
        with Empresas do
        begin
             with Items do
             begin
                  Clear;
                  BeginUpdate;
                  try
                     for i := 0 to ( FCompanies.Count - 1 ) do
                     begin
                          sEmpresa := FCompanies.Names[ i ];
                          Add( FCompanies.Values[ sEmpresa ] );
                          Checked[ i ] := ( FEscogidas.IndexOf( sEmpresa ) >= 0 );
                     end;
                  finally
                         EndUpdate;
                  end;
             end;
        end;
     finally
            FreeAndNil( FEscogidas );
     end;
end;

procedure TReportesTress.SetPortalServer( const sValue: String );
begin
     Self.ServerPortal.Caption := sValue;
end;

procedure TReportesTress.SetControls;
begin
     ServerPortal.Enabled := FConnected;
     ServerPortalLBL.Enabled := FConnected;
     ServerPortalFind.Enabled := FConnected;
     Empresas.Enabled := FConnected;
     Refrescar.Enabled := FConnected and FReportalLoad;
     ReporteBorrar.Enabled := FConnected and FReportalLoad;
     ReporteEditar.Enabled := FConnected and FReportalLoad;
     with FPortalReportes do
     begin
          Grabar.Enabled := FConnected and FReportalLoad and Changed or FPortalChanged;
          Generar.Enabled := FConnected and FReportalLoad and not Grabar.Enabled;
     end;
end;

{ ********** Controles para Portal *********** }

procedure TReportesTress.ServerPortalFindClick(Sender: TObject);
var
   FServerForm: TPortalServer;
   oCursor: TCursor;
begin
     FServerForm := TPortalServer.Create( Self );
     try
        with FServerForm do
        begin
             ShowModal;
             if ( ModalResult = mrOk ) then
             begin
                  oCursor := Screen.Cursor;
                  Screen.Cursor := crHourglass;
                  try
                     SetPortalServer( ServerName );
                     ListReportalBuild;
                     SetControls;
                  finally
                         Screen.Cursor := oCursor;
                  end;
             end;
        end;
     finally
            FreeAndNil( FServerForm );
     end;
end;

procedure TReportesTress.ListReportalBuild;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           with FPortalReportes do
           begin
                FReportalLoad := BuildList( ListReportal.Items );
                SetEmpresas( Empresas );
           end;
        except
              on Error: Exception do
              begin
                   Application.HandleException( Error );
              end;
        end;
        DPortalCaller.InitPortalCaller;
        try
           SetPortalServer( PortalCaller.PortalServer );
        finally
               DPortalCaller.ClosePortalCaller;
        end;
        FPortalChanged := False;
        SetControls;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TReportesTress.GenerarClick(Sender: TObject);
var
   FViewer: TViewLog;
begin
     FViewer := TViewLog.Create( Self );
     try
        dmReportesPortal := TdmReportesPortal.Create( Self );
        try
           with FViewer do
           begin
                Portal := True;
                ShowModal;
           end;
        finally
               FreeAndNil( dmReportesPortal );
        end;
     finally
            FreeAndNil( FViewer );
     end;
end;

procedure TReportesTress.RefrescarClick(Sender: TObject);
begin
     ListReportalBuild;
end;

procedure TReportesTress.GrabarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with FPortalReportes do
        begin
             Unload;
        end;
        FPortalChanged := False;
        SetControls;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TReportesTress.EmpresasClickCheck(Sender: TObject);
begin
     GrabarEmpresas.Enabled := True;
end;

procedure TReportesTress.GrabarEmpresasClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with FPortalReportes do
        begin
             Empresas := Self.GetEmpresas;
        end;
        GrabarEmpresas.Enabled := False;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TReportesTress.ReporteEditarClick(Sender: TObject);
var
   Nodo: TTreeNode;
   oForma: TPortalReportEdit;
begin
     Nodo := ListReportal.Selected;
     if Assigned( Nodo ) then
     begin
          oForma := TPortalReportEdit.Create( Self );
          try
             with oForma do
             begin
                  Empresas.Lista.Assign( FCompanies );
                  Item := TPortalReporte( Nodo.Data );
                  ShowModal;
                  if ( ModalResult = mrOk ) then
                  begin
                       with Item do
                       begin
                            if Lista.Changed then
                            begin
                                 Nodo.Text := GetTitulo;
                                 SetControls;
                            end;
                       end;
                  end;
             end;
          finally
                 FreeAndNil( oForma );
          end;
     end
     else
         ZetaDialogo.zInformation( '� Atenci�n !', 'No Se Ha Escogido Ning�n Reporte', 0 );
end;

procedure TReportesTress.ReporteBorrarClick(Sender: TObject);
var
   Nodo: TTreeNode;
begin
     Nodo := ListReportal.Selected;
     if Assigned( Nodo ) then
     begin
          with TPortalReporte( Nodo.Data ) do
          begin
               SetValues( '', 0 );
               Nodo.Text := GetTitulo;
          end;
          SetControls;
     end
     else
         ZetaDialogo.zInformation( '� Atenci�n !', 'No Se Ha Escogido Ning�n Reporte', 0 );
end;

procedure TReportesTress.EmpresasChange(Sender: TObject);
begin
     FPortalChanged := True;
     SetControls;
end;

procedure TReportesTress.SalirClick(Sender: TObject);
begin
     Close
end;

end.
