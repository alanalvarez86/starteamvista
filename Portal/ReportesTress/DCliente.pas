unit DCliente;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, DBClient,
     ZetaClientDataSet,
     DBasicoCliente,
     {$ifdef DOS_CAPAS}
     DServerCatalogos,
     DServerGlobal,
     DServerReportes,
     DServerCalcNomina,
     DServerLogin,
     DServerSuper,
     {$else}
     Reportes_TLB,
     {$endif}
     ZetaCommonLists,
     ZetaCommonClasses;


type
  TdmCliente = class(TBasicoCliente)
    cdsPeriodo: TZetaClientDataSet;
    cdsLookupReportes: TZetaLookupDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsLookupReportesAlAdquirirDatos(Sender: TObject);
    procedure cdsLookupReportesLookupSearch(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String; var sKey, sDescription: String);
  private
    { Private declarations }
    FParametros: TZetaParams;
    {$ifdef DOS_CAPAS}
    FServerCatalogos: TdmServerCatalogos;
    FServerGlobal: TdmServerGlobal;
    FServerReportes: TdmServerReportes;
    FServerCalcNomina: TdmServerCalcNomina;
    FServerLogin: TdmServerLogin;
    FServerSuper: TdmServerSuper;
    {$else}
    FServerReportes: IdmServerReportesDisp;
    function GetServerReportes: IdmServerReportesDisp;
    {$endif}
    function GetPeriodoInicial: Boolean;
    function GetIMSSMes: Integer;
    function GetIMSSPatron: String;
    function GetIMSSTipo: eTipoLiqIMSS;
    function GetIMSSYear: Integer;
    function LoadCompanies: OleVariant;
    procedure CargaActivosIMSS( Parametros: TZetaParams );
    procedure CargaActivosPeriodo( Parametros: TZetaParams );
    procedure CargaActivosSistema( Parametros: TZetaParams );
  public
    { Public declarations }
    property Parametros: TZetaParams read FParametros;
    property IMSSPatron: String read GetIMSSPatron;
    property IMSSYear: Integer read GetIMSSYear;
    property IMSSTipo: eTipoLiqIMSS read GetIMSSTipo;
    property IMSSMes: Integer read GetIMSSMes;
    {$ifdef DOS_CAPAS}
    property ServerCatalogos: TdmServerCatalogos read FServerCatalogos;
    property ServerGlobal: TdmServerGlobal read FServerGlobal;
    property ServerReportes: TdmServerReportes read FServerReportes;
    property ServerCalcNomina: TdmServerCalcNomina read FServerCalcNomina;
    property ServerLogin: TdmServerLogin read FServerLogin;
    property ServerSuper: TdmServerSuper read FServerSuper;
    {$else}
    property ServerReportes: IdmServerReportesDisp read GetServerReportes;
    {$endif}
    function BuscaCompany( const Codigo: String ): Boolean;
    function Empresa: OleVariant;
    function GetDatosPeriodoActivo: TDatosPeriodo;
    function GetValorActivo( const sValor: String ): String; override;
    function GetValorActivoStr( const eTipo: TipoEstado ): String; override;
    function ListaCompanies( Lista: TStrings ): Boolean;
    procedure CargaActivosTodos( Parametros: TZetaParams ); override;
    procedure LoadReportsEmpresa( const sEmpresa: String );
  end;

var
  dmCliente: TdmCliente;

implementation

{$R *.DFM}

uses ZetaRegistryCliente,
     ZetaBusqueda,
     ZetaDialogo,
     ZetaCommonTools;

procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
     inherited;
     FParametros := TZetaParams.Create( Self );
     {$ifdef DOS_CAPAS}
     FServerCatalogos := TdmServerCatalogos.Create( Self );
     FServerGlobal := TdmServerGlobal.Create( Self );
     FServerReportes := TdmServerReportes.Create( Self );
     FServerCalcNomina := TdmServerCalcNomina.Create( Self );
     FServerLogin := TdmServerLogin.Create( Self );
     FServerSuper := TdmServerSuper.Create( Self );
     {$endif}
end;

procedure TdmCliente.DataModuleDestroy(Sender: TObject);
begin
     {$ifdef DOS_CAPAS}
     FreeAndNil( FServerSuper );
     FreeAndNil( FServerLogin );
     FreeAndNil( FServerReportes );
     FreeAndNil( FServerCalcNomina );
     FreeAndNil( FServerCatalogos );
     FreeAndNil( FServerGlobal );
     {$endif}
end;

{$ifndef DOS_CAPAS}
function TdmCliente.GetServerReportes: IdmServerReportesDisp;
begin
     Result := IdmServerReportesDisp( CreaServidor( CLASS_dmServerReportes, FServerReportes ) );
end;
{$endif}

function TdmCliente.GetPeriodoInicial: Boolean;
begin
     with cdsPeriodo do
     begin
          Data := Servidor.GetPeriodoInicial( Empresa, TheYear(Date), Ord( tpSemanal ) );
          Result := not IsEmpty;
     end;
end;

function TdmCliente.ListaCompanies( Lista: TStrings ): Boolean;
begin
     Result := False;
     try
        LoadCompanies;
        Result := True;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
     if Result then
     begin
          with Lista do
          begin
               BeginUpdate;
               try
                  Clear;
                  with cdsCompany do
                  begin
                       while not Eof do
                       begin
                            Add( Format( '%s=%s', [ FieldByName( 'CM_CODIGO' ).AsString, FieldByName( 'CM_NOMBRE' ).AsString ] ) );
                            Next;
                       end;
                  end;
               finally
                      EndUpdate;
               end;
               Result := ( Count > 0 );
          end;
     end;
end;

function TdmCliente.LoadCompanies: OleVariant;
begin
     with cdsCompany do
     begin
          if not Active or IsEmpty then
          begin
               Data := Servidor.GetCompanys( 0, Ord( tc3Datos ) );
          end;
     end;
end;

function TdmCliente.BuscaCompany( const Codigo: String ): Boolean;
begin
     LoadCompanies;
     with cdsCompany do
     begin
          if Active and not IsEmpty then
             Result := Locate( 'CM_CODIGO', Codigo, [] )
          else
              Result := False;;
     end;
end;

function TdmCliente.Empresa: OleVariant; {Creo que este metodo repite logica que ya se esta haciendo en DBaseCliente}
begin
     Result := BuildEmpresa( cdsCompany );
end;

procedure TdmCliente.CargaActivosSistema;
begin
     with Parametros do
     begin
          AddDate( 'FechaAsistencia', FechaDefault );
          AddDate( 'FechaDefault', FechaDefault );
          AddInteger( 'YearDefault', TheYear( FechaDefault ) );
          AddInteger( 'EmpleadoActivo', 0 );
          AddString( 'NombreUsuario', 'REP_EMAIL' );
          AddString( 'CodigoEmpresa', GetDatosEmpresaActiva.Codigo );
     end;
end;

procedure TdmCliente.CargaActivosTodos( Parametros: TZetaParams );
begin
     CargaActivosIMSS( Parametros );
     CargaActivosPeriodo( Parametros );
     CargaActivosSistema( Parametros );
end;

procedure TdmCliente.CargaActivosIMSS(Parametros: TZetaParams);
begin
     with Parametros do
     begin
          AddString( 'RegistroPatronal', ImssPatron );
          AddInteger( 'IMSSYear', ImssYear );
          AddInteger( 'IMSSMes', ImssMes );
          AddInteger( 'IMSSTipo', Ord( ImssTipo ) );
     end;
end;

procedure TdmCliente.CargaActivosPeriodo(Parametros: TZetaParams);
begin
     with cdsPeriodo do
     begin
          if IsEmpty then
             GetPeriodoInicial;
          with Parametros do
          begin
               AddInteger( 'Year', FieldByName( 'PE_YEAR' ).AsInteger );
               AddInteger( 'Tipo', FieldByName( 'PE_TIPO' ).AsInteger );
               AddInteger( 'Numero', FieldByName( 'PE_NUMERO' ).AsInteger );
          end;
     end;
end;

function TdmCliente.GetDatosPeriodoActivo: TDatosPeriodo;
begin
     with Result do
     begin
          with cdsPeriodo do
          begin
               Year := FieldByName( 'PE_YEAR' ).AsInteger;
               Tipo := eTipoPeriodo(FieldByName( 'PE_TIPO' ).AsInteger);
               Numero := FieldByName( 'PE_NUMERO' ).AsInteger;
               Inicio := FieldByName( 'PE_FEC_INI' ).AsDateTime;
               Fin := FieldByName( 'PE_FEC_FIN' ).AsDateTime;
               Pago := FieldByName( 'PE_FEC_PAG' ).AsDateTime;
               Dias := FieldByName( 'PE_DIAS' ).AsInteger;
               Status := eStatusPeriodo( FieldByName( 'PE_STATUS' ).AsInteger );
               Uso := eUsoPeriodo( FieldByName( 'PE_USO' ).AsInteger );
               Mes := ZetaCommonTools.iMax( 1, FieldByName( 'PE_MES' ).AsInteger );
               SoloExcepciones := ZetaCommonTools.zStrToBool( FieldByName( 'PE_SOLO_EX' ).AsString );
               IncluyeBajas := ZetaCommonTools.zStrToBool( FieldByName( 'PE_INC_BAJ' ).AsString );
          end;
     end;
end;

function TdmCliente.GetValorActivo( const sValor: String ): String;
const
     K_EMPLEADO = '@EMPLEADO';

     K_IMSS_PATRON = '@PATRON';
     K_IMSS_YEAR = '@IMSS_YEAR';
     K_IMSS_MES = '@IMSS_MES';
     K_IMSS_TIPO = '@IMSS_TIPO';

     K_YEAR = '@YEAR';
     K_TIPO = '@TIPO';
     K_NUMERO = '@NUMERO';
begin
     if ( sValor = K_EMPLEADO ) then
        Result := '0'
     else
         if ( sValor = K_IMSS_PATRON ) then
            Result := Comillas( IMSSPatron )
         else
             if ( sValor = K_IMSS_YEAR ) then
                Result := IntToStr( IMSSYear )
             else
                 if ( sValor = K_IMSS_MES ) then
                    Result := IntToStr( IMSSMes )
                 else
                     if ( sValor = K_IMSS_TIPO ) then
                        Result := IntToStr( Ord( IMSSTipo ) )
                     else
                         if ( sValor = K_YEAR ) then
                            Result := IntToStr( GetDatosPeriodoActivo.Year )
                         else
                             if ( sValor = K_TIPO ) then
                                Result := IntToStr( Ord( GetDatosPeriodoActivo.Tipo ) )
                             else
                                 if ( sValor = K_NUMERO ) then
                                    Result := IntToStr( GetDatosPeriodoActivo.Numero )
                                 else
                                     Result := sValor;
end;

function TdmCliente.GetValorActivoStr( const eTipo: TipoEstado ): String;
begin
     Result := '';
end;

function TdmCliente.GetIMSSMes: Integer;
begin
     Result := TheMonth(Date);
end;

function TdmCliente.GetIMSSPatron: String;
begin
     Result := '';
end;

function TdmCliente.GetIMSSTipo: eTipoLiqIMSS;
begin
     Result := tlOrdinaria;
end;

function TdmCliente.GetIMSSYear: Integer;
begin
     Result := TheYear( Date );
end;

{ ********** Eventos del Lookup de Reportes ******** }

procedure TdmCliente.cdsLookupReportesAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     cdsLookUpReportes.Data := ServerReportes.GetLookUpReportes( Empresa, True );
end;

procedure TdmCliente.cdsLookupReportesLookupSearch( Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String; var sKey, sDescription: String);
begin
     inherited;
     lOk := ZetaBusqueda.ShowSearchForm( Sender, sFilter, sKey, sDescription );
end;

procedure TdmCliente.LoadReportsEmpresa( const sEmpresa: String );
begin
     if BuscaCompany( sEmpresa ) then
     begin
          cdsLookupReportes.Refrescar;
     end;
end;

end.
