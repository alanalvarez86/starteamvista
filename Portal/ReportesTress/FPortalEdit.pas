unit FPortalEdit;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ExtCtrls, Mask,
     DReportesPortal,
     ZBaseDlgModal,
     ZetaNumero,
     ZetaKeyCombo, ZetaDBTextBox, ZetaKeyLookup;

type
  TPortalReportEdit = class(TZetaDlgModal)
    EmpresaLBL: TLabel;
    ReporteLBL: TLabel;
    Empresas: TZetaKeyCombo;
    Nombre: TZetaTextBox;
    Folio: TZetaTextBox;
    NombreLBL: TLabel;
    FolioLBL: TLabel;
    Reporte: TZetaKeyLookup;
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure EmpresasChange(Sender: TObject);
  private
    { Private declarations }
    FItem: TPortalReporte;
    procedure LoadReportsEmpresa;
  public
    { Public declarations }
    property Item: TPortalReporte read FItem write FItem;
  end;

var
  PortalReportEdit: TPortalReportEdit;

implementation

{$R *.DFM}

uses DCliente,
     ZetaCommonLists;

procedure TPortalReportEdit.FormCreate(Sender: TObject);
begin
     inherited;
     dmCliente := TdmCliente.Create( Self );
     with Reporte do
     begin
          Filtro := Format( '( RE_TIPO = %d )', [ Ord( trListado ) ] );
          LookupDataset := dmCliente.cdsLookupReportes;
     end;
end;

procedure TPortalReportEdit.FormShow(Sender: TObject);
begin
     inherited;
     Folio.Caption := Format( '%d', [ FItem.Folio ] );
     Nombre.Caption := FItem.Nombre;
     with Empresas do
     begin
          ItemIndex := Lista.IndexOfName( FItem.Empresa );
     end;
     LoadReportsEmpresa;
     Reporte.Valor := FItem.Reporte;
end;

procedure TPortalReportEdit.FormDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( dmCliente );
end;

procedure TPortalReportEdit.LoadReportsEmpresa;
begin
     dmCliente.LoadReportsEmpresa( Empresas.Llave );
end;

procedure TPortalReportEdit.OKClick(Sender: TObject);
var
   sEmpresa: String;
begin
     inherited;
     with Empresas do
     begin
          if ( ItemIndex < 0 ) then
             sEmpresa := ''
          else
              sEmpresa := Lista.Names[ ItemIndex ];
     end;
     FItem.SetValues( sEmpresa, Reporte.Valor );
end;

procedure TPortalReportEdit.EmpresasChange(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     inherited;
     try
        dmCliente.LoadReportsEmpresa( Empresas.Llave );
     finally
            Screen.Cursor := oCursor;
     end;
end;

end.
