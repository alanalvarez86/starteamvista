unit DTotaliza;

interface

uses SysUtils, Classes, Controls, DB, Contnrs, Math,
     ZAgenteSQL,
     ZetaCommonClasses,
     ZetaCommonLists;

const
     K_MAX_GRUPOS = 9;
type
  TListaTotaliza = class;
  TTotaliza = class(TObject)
  private
    { Private declarations }
    FCampo: TField;
    FCriterio: eTipoOperacionCampo;
    FSuma: array [ 0..K_MAX_GRUPOS ] of TPesos;
    FNumero: array [ 0..K_MAX_GRUPOS ] of Integer;
    FTipoFormula: eTipoGlobal;
    FConstante: TPesos;
    FListaTotaliza: TListaTotaliza;
    function BooleanoVerdadero: Boolean;
    function ValorNumero: TPesos;
  public
    { Public declarations }
    constructor Create( oLista: TListaTotaliza; const iTotal: Integer; oColumna: TSQLColumna );
    function Resultado( const iGrupo: Integer ): TPesos;
    procedure Inicializa( const iGrupo: Integer );
    procedure Acumula;
  end;
  TListaTotaliza = class( TObjectList )
  private
    { Private declarations }
    FNumGrupos: Integer;
    FDataset: TDataset;
    FNumero: array [ 0..K_MAX_GRUPOS ] of Integer;
  public
    { Public declarations }
    property  NumGrupos: Integer read FNumGrupos;
    function  AgregaTotal( const iPos: Integer; oColumna: TSQLColumna ): Integer;
    function  Cuantos( const iGrupo: Integer ): Integer;
    function  GetResultado( const iPos, iGrupo: Integer ): TPesos;
    function  GetTotaliza( const iPos: Integer ): TTotaliza;
    procedure AcumulaTotales;
    procedure InicializaTotales( const iGrupo: Integer );
    procedure PreparaTotales( const iGrupos: Integer; oDataset: TDataset );
  end;


implementation

{ ****** TTotaliza ******* }

constructor TTotaliza.Create(oLista: TListaTotaliza; const iTotal: Integer; oColumna: TSQLColumna );
begin
     FListaTotaliza := oLista;
     with oColumna do
     begin
          if EsConstante then
          begin
               if ( TipoFormula = tgBooleano ) then
               begin
                    if ( ValorConstante = K_BOOLEANO_SI ) then
                       FConstante := 1
                    else
                        FConstante := 0;
               end
               else
               begin
                    try
                       FConstante := StrToCurr( ValorConstante );
                    except
                          FConstante := 0;
                    end;
               end;
          end
          else
              FCampo := FListaTotaliza.FDataset.Fields[ PosCampo ];
          FCriterio := Totalizacion;
          FTipoFormula := TipoFormula;
          Inicializa( 0 );
     end;
end;

function TTotaliza.ValorNumero: TPesos;
begin
    if ( FCampo = NIL ) then
        Result := FConstante
    else
        Result := FCampo.AsFloat;
end;

function TTotaliza.BooleanoVerdadero: Boolean;
begin
    if ( FCampo = NIL ) then
        Result := ( FConstante > 0 )
    else
        Result := ( FCampo.AsString = K_BOOLEANO_SI );
end;

procedure TTotaliza.Acumula;
var
   i: Integer;
begin
     for i := 0 to FListaTotaliza.NumGrupos do
     begin
          try
             case FCriterio of
                 ocCuantos: if ( FTipoFormula <> tgBooleano ) or BooleanoVerdadero then Inc( FNumero[ i ] );
                 ocSuma: FSuma[ i ] := FSuma[ i ] + ValorNumero;
                 ocPromedio:
                 begin
                      Inc( FNumero[ i ] );
                      FSuma[ i ] := FSuma[ i ] + ValorNumero;
                 end;
                 ocMin: if ( ValorNumero < FSuma[ i ] ) then FSuma[ i ] := ValorNumero;
                 ocMax: if ( ValorNumero > FSuma[ i ] ) then FSuma[ i ] := ValorNumero;
                 ocRepite: FSuma[ i ] := ValorNumero;
             end;
          except
                // Por si quieren totalizar campos que no tienen un AsFloat v�lido
                FSuma[ i ]   := 0;
                FNumero[ i ] := 0;
          end;
     end;
end;

procedure TTotaliza.Inicializa( const iGrupo: Integer );
var
   i: Integer;
begin
     for i := iGrupo to FListaTotaliza.NumGrupos do
     begin
          FNumero[ i ] := 0;
          if ( FCriterio = ocMin ) then
             FSuma[ i ] := IntPower( 10, 10 )
          else
              if ( FCriterio = ocMax ) then
                 FSuma[ i ] := -IntPower( 10, 10 )
              else
                  FSuma[ i ] := 0;
     end;
end;

function TTotaliza.Resultado( const iGrupo: Integer ): TPesos;
begin
    case FCriterio of
        ocCuantos: Result := FNumero[ iGrupo ];
        ocPromedio:
        begin
             if ( FNumero[ iGrupo ] > 0 ) then
                Result := FSuma[ iGrupo ] / FNumero[ iGrupo ]
             else
                 Result := 0;
        end;
        ocSobreTotales: Result := 99999;  // Temporal
    else
        Result := FSuma[ iGrupo ];
    end;
end;

{ ********* TListaTotaliza ********** }

procedure TListaTotaliza.AcumulaTotales;
var
    i: Integer;
begin
    for i := 0 to FNumGrupos do
        Inc( FNumero[ i ] );

    for i := 0 to Count-1 do
        GetTotaliza( i ).Acumula;
end;

function TListaTotaliza.GetTotaliza( const iPos: Integer ): TTotaliza;
begin
    Result := TTotaliza( Items[ iPos ] );
end;

function TListaTotaliza.GetResultado( const iPos, iGrupo: Integer ): TPesos;
begin
    Result := GetTotaliza( iPos ).Resultado( iGrupo );
end;

procedure TListaTotaliza.InicializaTotales( const iGrupo: Integer );
var
   i: Integer;
begin
     for i := iGrupo to FNumGrupos do
     begin
          FNumero[ i ] := 0;
     end;
     for i := 0 to ( Count - 1 ) do
     begin
          GetTotaliza( i ).Inicializa( iGrupo );
     end;
end;

procedure TListaTotaliza.PreparaTotales( const iGrupos: Integer; oDataset: TDataset );
begin
     Clear;
     if ( iGrupos > K_MAX_GRUPOS ) then
        raise Exception.Create( Format( 'M�ximo %d Grupos', [ K_MAX_GRUPOS ] ) );
     FNumGrupos := iGrupos;
     FDataset := oDataset;
     InicializaTotales( 0 );
end;

function TListaTotaliza.AgregaTotal( const iPos: Integer; oColumna: TSQLColumna ): Integer;
begin
     Result := -1;
     if ( oColumna <> NIL ) then
        Result := Add( TTotaliza.Create( Self, iPos, oColumna ) );
end;

function TListaTotaliza.Cuantos( const iGrupo: Integer ): Integer;
begin
     Result := FNumero[ iGrupo ];
end;

end.
