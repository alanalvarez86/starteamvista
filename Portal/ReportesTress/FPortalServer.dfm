inherited PortalServer: TPortalServer
  Left = 298
  Top = 229
  ActiveControl = Server
  Caption = 'Especificar Servidor del Portal'
  ClientHeight = 108
  ClientWidth = 319
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ServerLBL: TLabel [0]
    Left = 14
    Top = 30
    Width = 42
    Height = 13
    Alignment = taRightJustify
    Caption = '&Servidor:'
    FocusControl = Server
  end
  object ServerFind: TSpeedButton [1]
    Left = 287
    Top = 26
    Width = 23
    Height = 22
    Hint = 'Buscar Servidor Del Portal'
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000130B0000130B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
      300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
      330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
      333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
      339977FF777777773377000BFB03333333337773FF733333333F333000333333
      3300333777333333337733333333333333003333333333333377333333333333
      333333333333333333FF33333333333330003333333333333777333333333333
      3000333333333333377733333333333333333333333333333333}
    NumGlyphs = 2
    ParentShowHint = False
    ShowHint = True
    OnClick = ServerFindClick
  end
  inherited PanelBotones: TPanel
    Top = 72
    Width = 319
    BevelOuter = bvNone
    inherited OK: TBitBtn
      Left = 151
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 236
    end
  end
  object Server: TEdit
    Left = 58
    Top = 26
    Width = 223
    Height = 21
    TabOrder = 1
  end
end
