inherited PortalReportEdit: TPortalReportEdit
  Left = 298
  Top = 229
  ActiveControl = Empresas
  Caption = 'Editar Reporte De Portal'
  ClientHeight = 141
  ClientWidth = 369
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object EmpresaLBL: TLabel [0]
    Left = 17
    Top = 55
    Width = 44
    Height = 13
    Alignment = taRightJustify
    Caption = '&Empresa:'
  end
  object ReporteLBL: TLabel [1]
    Left = 10
    Top = 77
    Width = 51
    Height = 13
    Alignment = taRightJustify
    Caption = '&Reporte #:'
  end
  object Nombre: TZetaTextBox [2]
    Left = 64
    Top = 32
    Width = 274
    Height = 17
    AutoSize = False
    Caption = 'Nombre'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object Folio: TZetaTextBox [3]
    Left = 64
    Top = 13
    Width = 59
    Height = 17
    AutoSize = False
    Caption = 'Folio'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object NombreLBL: TLabel [4]
    Left = 21
    Top = 34
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = '&Nombre:'
  end
  object FolioLBL: TLabel [5]
    Left = 36
    Top = 15
    Width = 25
    Height = 13
    Alignment = taRightJustify
    Caption = '&Folio:'
  end
  inherited PanelBotones: TPanel
    Top = 105
    Width = 369
    BevelOuter = bvNone
    TabOrder = 1
    inherited OK: TBitBtn
      Left = 201
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 286
    end
  end
  object Empresas: TZetaKeyCombo
    Left = 64
    Top = 51
    Width = 275
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
    OnChange = EmpresasChange
    ListaFija = lfNinguna
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
  end
  object Reporte: TZetaKeyLookup
    Left = 64
    Top = 73
    Width = 300
    Height = 21
    Opcional = False
    TabOrder = 2
    TabStop = True
    WidthLlave = 60
  end
end
