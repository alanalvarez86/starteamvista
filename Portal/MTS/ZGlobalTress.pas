unit ZGlobalTress;

interface

uses ZetaCommonLists,
     ZetaCommonClasses;

const
     K_NUM_GLOBALES = 23;
     K_NUM_DEFAULTS = 0;
     K_TOT_GLOBALES = K_NUM_GLOBALES + K_NUM_DEFAULTS;
     K_BASE_DEFAULTS = 2000;

     { Globales DUMMYS que se requiren para poder compilar }

     K_GLOBAL_PRIMER_ADICIONAL = 0;
     K_GLOBAL_LAST_ADICIONAL = 0;
     K_GLOBAL_TEXTO_BASE               = 0;
     K_GLOBAL_NUM_BASE                 = 0;
     K_GLOBAL_LOG_BASE                 = 0;
     K_GLOBAL_FECHA_BASE               = 0;
     K_GLOBAL_TAB_BASE                 = 0;

     {Constantes para Globales}

     K_GLOBAL_NOMBRE           = 1; { Texto: Nombre Del Proyecto }
     K_GLOBAL_ROOT             = 2; { Texto: Ruta Ra�z }
     K_GLOBAL_RUTA_DOC         = 3; { Texto: Ruta Para Documentos }
     K_GLOBAL_PAGINADOS        = 4; { Integer: Registros Por P�gina }
     K_GLOBAL_FOTOS            = 5; { Texto: Ruta Para Im�genes de Noticias }
     K_GLOBAL_EMPRESA          = 6; { Texto: Nombre de la Empresa  }
     K_GLOBAL_DIRECCION        = 7; { Texto: Direcci�n  }
     K_GLOBAL_CIUDAD           = 8; { Texto: Ciudad }
     K_GLOBAL_ESTADO           = 9; { Texto: Estado }
     K_GLOBAL_LOGOBIG          = 10; { Texto: Ruta del Logo Grande }
     K_GLOBAL_LOGOSMALL        = 11; { Texto: Ruta del Logo Chico }
     K_GLOBAL_TITULAR          = 12; { Integer: Integer: Art�culo Titular: Noticia Titular }
     K_GLOBAL_EMAIL_DEFAULT    = 13; { Texto: Lista de eMails default }
     K_GLOBAL_EMAIL_SERVER     = 14; { Texto: Servidor de eMail POP3 }
     K_GLOBAL_EMAIL_AUTO       = 15; { Texto: eMail para Auto Response }
     K_GLOBAL_PAGE_REFRESH     = 16; { Integer: Refrescado de P�gina }
     K_GLOBAL_DIR_FOTOS        = 17; {Texto: Directorio de Fotografias para las Noticias}
     K_GLOBAL_DIR_DOCS         = 18; {Texto: Directorio de Documentos}
     K_GLOBAL_RUTA_PAGINA      = 19; { Texto: Ruta de la P�gina de la Empresa }
     K_GLOBAL_RUTA_PORTAL      = 20; { Texto: Ruta del Directorio Virtual del Portal }
     K_GLOBAL_DIR_EMPLEADOS    = 21; { Texto: Ruta virtual de las fotos de empleados}
     K_GLOBAL_RUTA_EMPLEADOS   = 22; { Texto: Ruta en disco de las fotos de empleados}
     K_GLOBAL_USER_DEFAULT     = 23; { Numero del Usuario que se tomar� como Default }
     K_GLOBAL_GRABA_BITACORA   = 24;

     { ��� OJO !!!   CUANDO SE AGREGUE UNA CONSTANTE NUEVA

     1) SE TIENE QUE AGREGAR EN EL METODO DGLOBAL.GETTIPOGLOBAL CUANDO EL GLOBAL NO SEA TEXTO
     2) Se debe aumentar en 1 la constante K_NUM_GLOBALES ( est� al principio de esta unidad )
     3) Se debe considerar la constante en la funci�n GetTipoGlobal ( est� al final de esta unidad )
     }

function GetTipoGlobal( const iCodigo : Integer ) : eTipoGlobal;
function GlobalToPos( const iCodigo : Integer ) : Integer;
function PosToGlobal( const iPos : Integer ) : Integer;
function GetDescripcionGlobal( const iCodigo : integer ): string;

implementation

{ Permite compactar el arreglo de Globales }
{ Los globales arriba de 2000, les resta un offset }

function GlobalToPos( const iCodigo : Integer ) : Integer;
begin
     if ( iCodigo <= K_NUM_GLOBALES ) then
        Result := iCodigo
     else
        Result := iCodigo - K_BASE_DEFAULTS + K_NUM_GLOBALES;
end;

function PosToGlobal( const iPos : Integer ) : Integer;
begin
     if ( iPos <= K_NUM_GLOBALES ) then
        Result := iPos
     else
        Result := iPos + K_BASE_DEFAULTS - K_NUM_GLOBALES;
end;

function GetTipoGlobal( const iCodigo : Integer ) : eTipoGlobal;
begin
     // Si se tienen Globales que no sean de Texto deber�n condicionarse para regresar otros
     // tipos Globales: tgBooleano, tgFloat, tgNumero, tgFecha, etc.
     case iCodigo of
          K_GLOBAL_PAGINADOS: Result := tgNumero;
          K_GLOBAL_TITULAR: Result := tgNumero;
          K_GLOBAL_PAGE_REFRESH: Result := tgNumero;
          K_GLOBAL_USER_DEFAULT: Result := tgNumero;
     else
         Result := tgTexto;
     end;
end;

function GetDescripcionGlobal( const iCodigo : integer ): string;
begin
     case iCodigo of
          K_GLOBAL_NOMBRE : Result := 'Nombre Del Proyecto';
          K_GLOBAL_ROOT : Result := 'Ruta Ra�z';
          K_GLOBAL_RUTA_DOC : Result := 'Ruta Para Documentos';
          K_GLOBAL_PAGINADOS : Result := 'Registros Por P�gina';
          K_GLOBAL_FOTOS : Result := 'Ruta Para Im�genes de Noticias';
          K_GLOBAL_EMPRESA : Result := 'Nombre de la Empresa';
          K_GLOBAL_DIRECCION : Result := 'Direcci�n';
          K_GLOBAL_CIUDAD : Result := 'Ciudad';
          K_GLOBAL_ESTADO : Result := 'Estado';
          K_GLOBAL_LOGOBIG : Result := 'Ruta del Logo Grande';
          K_GLOBAL_LOGOSMALL : Result := 'Ruta del Logo Chico';
          K_GLOBAL_TITULAR : Result := 'Art�culo Titular: Noticia Titular';
          K_GLOBAL_EMAIL_DEFAULT : Result := 'Lista de eMails default';
          K_GLOBAL_EMAIL_SERVER : Result := 'Servidor de eMail POP3';
          K_GLOBAL_EMAIL_AUTO : Result := 'eMail para Auto Response';
          K_GLOBAL_PAGE_REFRESH : Result := 'Refrescado de P�gina';
          K_GLOBAL_DIR_FOTOS : Result := 'Directorio de Fotografias para las Noticias';
          K_GLOBAL_DIR_DOCS : Result := 'Directorio de Documentos';
          K_GLOBAL_RUTA_PAGINA : Result := 'Ruta de la P�gina de la Empresa';
          K_GLOBAL_RUTA_PORTAL : Result := 'Ruta del Directorio Virtual del Portal';
          K_GLOBAL_DIR_EMPLEADOS : Result := 'Ruta virtual de las fotos de empleados';
          K_GLOBAL_RUTA_EMPLEADOS : Result := 'Ruta en disco de las fotos de empleados';
          K_GLOBAL_USER_DEFAULT : Result := 'N�mero del Usuario que se tomar� como Default';
          K_GLOBAL_GRABA_BITACORA : Result := 'Movimientos que se registran en Bit�cora';
     end;
end;

end.
