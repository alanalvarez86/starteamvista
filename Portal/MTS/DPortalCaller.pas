unit DPortalCaller;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Registry, ComObj, Db, DBClient, ComCtrls,
     {$ifdef DOS_CAPAS}
     DServerPortal,
     {$else}
     Portal_TLB,
     {$endif}
     ZetaCommonLists,
     ZetaClientDataset;

type
  TdmPortalCaller = class(TDataModule)
    cdsRepCols: TZetaClientDataSet;
    cdsRepData: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsRepColsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
  private
    { Private declarations }
    FColumnas: TStrings;
    FRegistry: TRegIniFile;
    FLoaded: Boolean;
    FStatusMsg: String;
    FEmpty: Boolean;
    function GetPortalServer: String;
    procedure SetPortalServer( const sValue: String );
  private
    { Private declarations }
    {$ifdef DOS_CAPAS}
    FServerPortal: TdmServerPortal;
    {$else}
    function GetServerPortal: IdmServerPortalDisp;
    {$endif}
    function GetDataFieldName( const iPtr: Integer ): String;
  protected
    { Protected declarations }
    {$ifdef DOS_CAPAS}
    property ServerPortal: TdmServerPortal read FServerPortal;
    {$else}
    property ServerPortal: IdmServerPortalDisp read GetServerPortal;
    {$endif}
  public
    { Public declarations }
    property PortalServer: String read GetPortalServer write SetPortalServer;
    function BuildReportEnd( const iFolio: Integer ): Boolean;
    function ReportsLoad: Boolean;
    function ReportsDataset: TDataset;
    procedure BuildReportBegin;
    procedure BuildAddColumn( const iFolio, iColumna, iAncho: Integer; const eTipo: eTipoGlobal; const sTitulo: String );
    procedure BuildDataBegin;
    procedure BuildDataAdd( const iColumna: Integer; const sData: String );
    procedure BuildDataEnd( const iFolio, iRenglon: Integer );
    procedure ReportIsEmpty( const sMsg: String );
  end;

var
   PortalCaller: TdmPortalCaller;

procedure InitPortalCaller;
procedure ClosePortalCaller;

implementation

uses ZetaPortalClasses,
     ZetaWinAPITools,
     ZetaRegistryCliente,
     ZReconcile;

const
     PORTAL_SERVER = 'Portal Server Computer Name';

procedure InitPortalCaller;
begin
     if not Assigned( PortalCaller ) then
     begin
          PortalCaller := TdmPortalCaller.Create( nil );
     end;
end;

procedure ClosePortalCaller;
begin
     FreeAndNil( PortalCaller );
end;

{$R *.DFM}

procedure TdmPortalCaller.DataModuleCreate(Sender: TObject);
begin
     FRegistry := TRegIniFile.Create;
     FColumnas := TStringList.Create;
     FLoaded := False;
     FStatusMsg := '';
     FEmpty := False;
     with FRegistry do
     begin
          RootKey := ZetaRegistryCliente.ClientRegistry.RegistryRoot;
          LazyWrite := False;
          OpenKey( Format( '%sPortal', [ ZetaWinAPITools.FormatDirectory( ZetaRegistryCliente.ClientRegistry.RegistryKey ) ] ), False );
     end;
end;

procedure TdmPortalCaller.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FColumnas );
     FreeAndNil( FRegistry );
end;

procedure TdmPortalCaller.cdsRepColsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError( Dataset, UpdateKind, E );
end;

{$ifndef DOS_CAPAS}
function TdmPortalCaller.GetServerPortal: IdmServerPortalDisp;

function CreatePortalServer(const ClassID: TGUID): IDispatch;
begin
     try
        Result := CreateRemoteComObject( GetPortalServer, ClassID ) as IDispatch;
     except
           Result := nil;
     end;
end;

begin
     Result := IdmServerPortalDisp( CreatePortalServer( CLASS_dmServerPortal ) );
end;
{$endif}

function TdmPortalCaller.GetPortalServer: String;
begin
     with FRegistry do
     begin
          Result := ReadString( '', PORTAL_SERVER, '' );
     end;
end;

procedure TdmPortalCaller.SetPortalServer(const sValue: String);
begin
     with FRegistry do
     begin
          WriteString( '', PORTAL_SERVER, sValue );
     end;
end;

procedure TdmPortalCaller.BuildReportBegin;
var
   oRepData: OleVariant;
begin
     if not FLoaded then
     begin
          cdsRepCols.Data := ServerPortal.GetReportalMetadata( oRepData );
          cdsRepData.Data := oRepData;
          FLoaded := True;
     end;
     with cdsRepCols do
     begin
          EmptyDataset;
          MergeChangeLog;
     end;
     with cdsRepData do
     begin
          EmptyDataset;
          MergeChangeLog;
     end;
     FStatusMsg := '';
     FEmpty := False;
end;

function TdmPortalCaller.BuildReportEnd( const iFolio: Integer ): Boolean;
var
   iErrores: Integer;
   oColumnas, oDatos: OleVariant;
begin
     Result := False;
     if FEmpty then
     begin
          oColumnas := cdsRepCols.DeltaNull;
          oDatos := cdsRepData.DeltaNull;
          iErrores := ServerPortal.GrabaReportal( iFolio, oColumnas, oDatos );
          Result := ( iErrores = 0 );
     end
     else
     begin
          with cdsRepCols do
          begin
               {$ifdef CAROLINA}
               SaveToFile( ExtractFileDir( ( Application.ExeName ) ) + '\cdsRepCols.cds', dfBinary );
               {$endif}
               if ( ChangeCount > 0 ) then
               begin
                    oColumnas := Delta;
                    Result := True;
               end;
          end;
          if Result then
          begin
               with cdsRepData do
               begin
                    {$ifdef CAROLINA}
                    SaveToFile( ExtractFileDir( ( Application.ExeName ) ) + '\cdsRepData.cds', dfBinary );
                    {$endif}
                    if ( ChangeCount > 0 ) then
                       oDatos := Delta
                    else
                        Result := False;
               end;
               if Result then
               begin
                    iErrores := ServerPortal.GrabaReportal( iFolio, oColumnas, oDatos );
                    if ( ( iErrores mod K_ERROR_OFFSET ) > 0 ) then
                    begin
                         cdsRepCols.Reconcile( oColumnas );
                         Result := False;
                    end;
                    if ( ( iErrores div K_ERROR_OFFSET ) > 0 ) then
                    begin
                         cdsRepData.Reconcile( oDatos );
                         Result := False;
                    end;
               end;
          end;
     end;
end;

procedure TdmPortalCaller.BuildAddColumn( const iFolio, iColumna, iAncho: Integer; const eTipo: eTipoGlobal; const sTitulo: String );
begin
     if ( iColumna <= K_REPORTAL_MAX_COLUMNAS ) then
     begin
          with cdsRepCols do
          begin
               Append;
               try
                  FieldByName( 'RP_FOLIO' ).AsInteger := iFolio;
                  FieldByName( 'RC_TITULO' ).AsString := sTitulo;
                  FieldByName( 'RC_ORDEN' ).AsInteger := iColumna;
                  FieldByName( 'RC_TIPO' ).AsInteger := Ord( eTipo );
                  FieldByName( 'RC_ANCHO' ).AsInteger := iAncho;
                  Post;
               except
                     Cancel;
                     raise;
               end;
          end;
     end;
end;

function TdmPortalCaller.GetDataFieldName( const iPtr: Integer ): String;
begin
     Result := Format( 'RD_DATO%d', [ iPtr ] );
end;

procedure TdmPortalCaller.BuildDataBegin;
var
   i: Integer;
begin
     with FColumnas do
     begin
          Clear;
          for i := 1 to ( K_REPORTAL_MAX_COLUMNAS ) DO
          begin
               Add( Format( '%s=', [ GetDataFieldName( i ) ] ) );
          end;
     end;
end;

procedure TdmPortalCaller.BuildDataAdd( const iColumna: Integer; const sData: String );
var
   sCampo: String;
begin
     sCampo := GetDataFieldName( iColumna );
     with FColumnas do
     begin
          if ( IndexOfName( sCampo ) >= 0 ) then
          begin
               Values[ sCampo ] := sData;
          end;
     end;
end;

procedure TdmPortalCaller.BuildDataEnd( const iFolio, iRenglon: Integer );
var
   i: Integer;
   sCampo: String;
begin
     with FColumnas do
     begin
          if ( Count > 0 ) then
          begin
               with cdsRepData do
               begin
                    Append;
                    try
                       FieldByName( 'RP_FOLIO' ).AsInteger := iFolio;
                       FieldByName( 'RD_ORDEN' ).AsInteger := iRenglon;
                       with FColumnas do
                       begin
                            for i := 0 to ( Count - 1 ) DO
                            begin
                                 sCampo := Names[ i ];
                                 FieldByName( sCampo ).AsString := Values[ sCampo ];
                            end;
                       end;
                       Post;
                    except
                          Cancel;
                          raise;
                    end;
               end;
          end;
     end;
end;

function TdmPortalCaller.ReportsDataset: TDataset;
begin
     Result := cdsRepData;
end;

function TdmPortalCaller.ReportsLoad: Boolean;
begin
     with cdsRepData do
     begin
          Init;
          Data := ServerPortal.GetTabla( K_TAG_REPORTES );
          Result := not IsEmpty;
     end;
end;

procedure TdmPortalCaller.ReportIsEmpty(const sMsg: String);
begin
     FStatusMsg := sMsg;
     FEmpty := True;
end;

end.
