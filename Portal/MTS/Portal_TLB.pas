unit Portal_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision: 9$
// File generated on 11/27/2003 5:31:09 PM from Type Library described below.

// ************************************************************************ //
// Type Lib: D:\3win_20\Portal\MTS\Portal.tlb (1)
// IID\LCID: {2172B9D5-3611-4F6D-8231-CF585E6FB9E3}\0
// Helpfile: 
// DepndLst: 
//   (1) v1.0 Midas, (C:\WINDOWS\system32\midas.dll)
//   (2) v2.0 stdole, (C:\WINDOWS\System32\stdole2.tlb)
//   (3) v4.0 StdVCL, (C:\WINDOWS\System32\STDVCL40.DLL)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, OleCtrls, StdVCL, 
  MIDAS;

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  PortalMajorVersion = 1;
  PortalMinorVersion = 0;

  LIBID_Portal: TGUID = '{2172B9D5-3611-4F6D-8231-CF585E6FB9E3}';

  IID_IdmServerPortal: TGUID = '{FF50A9A8-2A03-4CAC-B4C2-25779D17CF1E}';
  CLASS_dmServerPortal: TGUID = '{88BF650B-800B-41A5-BCFB-3C4CCA82B515}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerPortal = interface;
  IdmServerPortalDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerPortal = IdmServerPortal;


// *********************************************************************//
// Interface: IdmServerPortal
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {FF50A9A8-2A03-4CAC-B4C2-25779D17CF1E}
// *********************************************************************//
  IdmServerPortal = interface(IAppServer)
    ['{FF50A9A8-2A03-4CAC-B4C2-25779D17CF1E}']
    function  GetTabla(iTabla: Integer): OleVariant; safecall;
    function  GrabaTabla(iTabla: Integer; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function  GetContenidos(Usuario: Integer; Pagina: Integer; Columna: Integer; 
                            out Paginas: OleVariant): OleVariant; safecall;
    function  GrabaPaginas(Delta: OleVariant; Usuario: Integer; out ErrorCount: Integer): OleVariant; safecall;
    function  GrabaContenidos(Delta: OleVariant; Usuario: Integer; Pagina: Integer; 
                              Columna: Integer; out ErrorCount: Integer): OleVariant; safecall;
    function  GetGlobales: OleVariant; safecall;
    function  GrabaGlobales(Delta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function  GetReportalMetadata(out RepData: OleVariant): OleVariant; safecall;
    function  GrabaReportal(Folio: Integer; var Columnas: OleVariant; var Datos: OleVariant): Integer; safecall;
    function  AgregaSugerencia(const Texto: WideString; const Titulo: WideString; 
                               const Autor: WideString; const Correo: WideString; Usuario: Integer): Integer; safecall;
    function  GetNoticias100(const Parametros: WideString): WideString; safecall;
    function  GetNoticia(const Parametros: WideString): WideString; safecall;
    function  GetEventos100(const Parametros: WideString): WideString; safecall;
    function  GetEvento(const Parametros: WideString): WideString; safecall;
    function  GetReporte(const Parametros: WideString): WideString; safecall;
    function  GetInicio(const Parametros: WideString): WideString; safecall;
    function  GetDocumentos(const Parametros: WideString): WideString; safecall;
    function  UsuarioLogin(const Parametros: WideString): WideString; safecall;
    function  GetInitPortal(const Parametros: WideString): WideString; safecall;
    function  GetDirectorios(const Parametros: WideString): WideString; safecall;
    function  GetMatchTarjetas(const Parametros: WideString): WideString; safecall;
    function  GetTarjeta(const Parametros: WideString): WideString; safecall;
    function  UsuarioCambiaPswd(const Parametros: WideString): WideString; safecall;
    function  ExpiraLogin(const Parametros: WideString): WideString; safecall;
    function  GetBuscaTarjetas(const Parametros: WideString): WideString; safecall;
    function  GetTarjetaNET(const Parametros: WideString): WideString; safecall;
    function  BorraTarjetaNET(const Parametros: WideString): WideString; safecall;
    function  AgregaTarjetaNET(const Parametros: WideString): WideString; safecall;
    function  ModificaTarjetaNET(const Parametros: WideString): WideString; safecall;
    procedure EscribeArchivo(const Parametros: WideString); safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerPortalDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {FF50A9A8-2A03-4CAC-B4C2-25779D17CF1E}
// *********************************************************************//
  IdmServerPortalDisp = dispinterface
    ['{FF50A9A8-2A03-4CAC-B4C2-25779D17CF1E}']
    function  GetTabla(iTabla: Integer): OleVariant; dispid 1;
    function  GrabaTabla(iTabla: Integer; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 2;
    function  GetContenidos(Usuario: Integer; Pagina: Integer; Columna: Integer; 
                            out Paginas: OleVariant): OleVariant; dispid 3;
    function  GrabaPaginas(Delta: OleVariant; Usuario: Integer; out ErrorCount: Integer): OleVariant; dispid 4;
    function  GrabaContenidos(Delta: OleVariant; Usuario: Integer; Pagina: Integer; 
                              Columna: Integer; out ErrorCount: Integer): OleVariant; dispid 5;
    function  GetGlobales: OleVariant; dispid 6;
    function  GrabaGlobales(Delta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 7;
    function  GetReportalMetadata(out RepData: OleVariant): OleVariant; dispid 8;
    function  GrabaReportal(Folio: Integer; var Columnas: OleVariant; var Datos: OleVariant): Integer; dispid 9;
    function  AgregaSugerencia(const Texto: WideString; const Titulo: WideString; 
                               const Autor: WideString; const Correo: WideString; Usuario: Integer): Integer; dispid 10;
    function  GetNoticias100(const Parametros: WideString): WideString; dispid 11;
    function  GetNoticia(const Parametros: WideString): WideString; dispid 12;
    function  GetEventos100(const Parametros: WideString): WideString; dispid 13;
    function  GetEvento(const Parametros: WideString): WideString; dispid 14;
    function  GetReporte(const Parametros: WideString): WideString; dispid 15;
    function  GetInicio(const Parametros: WideString): WideString; dispid 16;
    function  GetDocumentos(const Parametros: WideString): WideString; dispid 17;
    function  UsuarioLogin(const Parametros: WideString): WideString; dispid 18;
    function  GetInitPortal(const Parametros: WideString): WideString; dispid 19;
    function  GetDirectorios(const Parametros: WideString): WideString; dispid 20;
    function  GetMatchTarjetas(const Parametros: WideString): WideString; dispid 21;
    function  GetTarjeta(const Parametros: WideString): WideString; dispid 22;
    function  UsuarioCambiaPswd(const Parametros: WideString): WideString; dispid 24;
    function  ExpiraLogin(const Parametros: WideString): WideString; dispid 23;
    function  GetBuscaTarjetas(const Parametros: WideString): WideString; dispid 25;
    function  GetTarjetaNET(const Parametros: WideString): WideString; dispid 26;
    function  BorraTarjetaNET(const Parametros: WideString): WideString; dispid 27;
    function  AgregaTarjetaNET(const Parametros: WideString): WideString; dispid 28;
    function  ModificaTarjetaNET(const Parametros: WideString): WideString; dispid 29;
    procedure EscribeArchivo(const Parametros: WideString); dispid 30;
    function  AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; 
                              MaxErrors: Integer; out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function  AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                            Options: Integer; const CommandText: WideString; 
                            var Params: OleVariant; var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function  AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function  AS_GetProviderNames: OleVariant; dispid 20000003;
    function  AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function  AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                            var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerPortal provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerPortal exposed by              
// the CoClass dmServerPortal. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerPortal = class
    class function Create: IdmServerPortal;
    class function CreateRemote(const MachineName: string): IdmServerPortal;
  end;

implementation

uses ComObj;

class function CodmServerPortal.Create: IdmServerPortal;
begin
  Result := CreateComObject(CLASS_dmServerPortal) as IdmServerPortal;
end;

class function CodmServerPortal.CreateRemote(const MachineName: string): IdmServerPortal;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerPortal) as IdmServerPortal;
end;

end.
