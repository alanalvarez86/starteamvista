library Portal;

uses
  ComServ,
  Portal_TLB in 'Portal_TLB.pas',
  DServerPortal in 'DServerPortal.pas' {dmServerPortal: TMtsDataModule} {dmServerPortal: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
