program PortalServerCFG;

uses
  Forms,
  ZBaseDlgModal in '..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  FRegistryServerEditor in '..\..\Configuracion\MSSQL\FRegistryServerEditor.pas' {ServerRegistryEditor};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Configurador Del Servidor del Portal';
  Application.CreateForm(TServerRegistryEditor, ServerRegistryEditor);
  with ServerRegistryEditor do
  begin
       Caption := 'Especificar Base De Datos de SQL Server Para Portal';
       ShowModal;
  end;
  if False then
     Application.Run;
end.
