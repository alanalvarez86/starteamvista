unit FFormaNomina;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, Buttons, ExtCtrls, Grids, DBGrids, StdCtrls, Mask, DBCtrls,
     FFormaBasePeriodo,
     ZetaDBTextBox,
     ZetaDBGrid;

type
  TFormaNomina = class(TFormaBasePeriodo)
    dsConceptos: TDataSource;
    NO_PERCEPClbl: TLabel;
    NO_DEDUCCIlbl: TLabel;
    NO_NETOlbl: TLabel;
    NO_NETO: TZetaDBTextBox;
    NO_DEDUCCI: TZetaDBTextBox;
    NO_PERCEPC: TZetaDBTextBox;
    ZetaDBGrid: TZetaDBGrid;
  private
    { Private declarations }
    function GetPeriodoStatus: String;
    procedure SetDatasets;
  protected
    { Protected declarations }
    procedure CargarInit; override;
    procedure CargarAnterior; override;
    procedure CargarSiguiente; override;
    procedure SetControlsFull; override;
    procedure SetControlsEmpty; override;
  public
    { Public declarations }
  end;

var
  FormaNomina: TFormaNomina;

implementation

uses DCliente,
     ZetaCommonLists,
     ZetaCommonTools;

{$R *.DFM}

{ ********** TFormaNomina ********* }

function TFormaNomina.GetPeriodoStatus: String;
const
     K_SIN_CALCULAR = '( Sin Calcular )';
     K_CALCULADA = '';
     K_INDEFINIDA = '( ??? )';
begin
     case eStatusPeriodo( Dataset.FieldByName( 'NO_STATUS' ).AsInteger ) of
          spNueva: Result := K_SIN_CALCULAR;
          spPreNomina: Result := K_SIN_CALCULAR;
          spSinCalcular: Result := K_SIN_CALCULAR;
          spCalculadaParcial: Result := K_CALCULADA;
          spCalculadaTotal: Result := K_CALCULADA;
          spAfectadaParcial: Result := K_CALCULADA;
          spAfectadaTotal: Result := K_CALCULADA;
     else
         Result := K_INDEFINIDA;
     end;
end;

procedure TFormaNomina.SetControlsEmpty;
begin
     inherited SetControlsEmpty;
     with Titulo do
     begin
          Caption := 'No Hay Datos';
     end;
end;

procedure TFormaNomina.SetControlsFull;
begin
     inherited SetControlsFull;
     with Titulo do
     begin
          with Dataset do
          begin
               Caption := Format( 'Del %s Al %s %s', [ ZetaCommonTools.FechaCorta( FieldByName( 'PE_FEC_INI' ).AsDateTime ),
                                                       ZetaCommonTools.FechaCorta( FieldByName( 'PE_FEC_FIN' ).AsDateTime ),
                                                       GetPeriodoStatus ] );
          end;
     end;
end;

procedure TFormaNomina.SetDatasets;
begin
     with dmCliente do
     begin
          Datasource.Dataset := cdsNomina;
          dsConceptos.Dataset := cdsConceptos;
     end;
end;

procedure TFormaNomina.CargarAnterior;
begin
     with dmCliente do
     begin
          AbreNominaAnterior( Year, Periodo );
     end;
     SetDatasets;
end;

procedure TFormaNomina.CargarInit;
begin
     with dmCliente do
     begin
          AbreNomina( Year );
     end;
     SetDatasets;
end;

procedure TFormaNomina.CargarSiguiente;
begin
     with dmCliente do
     begin
          AbreNominaSiguiente( Year, Periodo );
     end;
     SetDatasets;
end;

end.
