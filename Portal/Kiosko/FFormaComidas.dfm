inherited FormaComidas: TFormaComidas
  Caption = 'Comidas de Un Per�odo de N�mina'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelSuperior: TPanel
    Height = 149
    Align = alClient
    object ZetaDBGrid: TZetaDBGrid
      Left = 1
      Top = 1
      Width = 696
      Height = 147
      Align = alClient
      DataSource = dsComidas
      FixedColor = clInfoBk
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      ReadOnly = True
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'CF_FECHA'
          Title.Caption = 'Fecha'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CF_HORA'
          Title.Caption = 'Hora'
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CF_TIPO'
          Title.Caption = 'Tipo'
          Width = 34
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CF_COMIDAS'
          Title.Alignment = taRightJustify
          Title.Caption = 'Comidas'
          Width = 59
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CF_EXTRAS'
          Title.Alignment = taRightJustify
          Title.Caption = 'Extras'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CF_RELOJ'
          Title.Caption = 'Reloj'
          Width = 57
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'US_CODIGO'
          Title.Caption = 'Modific�'
          Width = 67
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CL_CODIGO'
          Title.Alignment = taRightJustify
          Title.Caption = 'Regla'
          Width = 54
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CF_REG_EXT'
          Title.Caption = 'T.Extra'
          Width = 61
          Visible = True
        end>
    end
  end
  inherited PanelInferior: TPanel
    Top = 197
    Height = 136
    Align = alBottom
    object DBCtrlGrid: TDBCtrlGrid
      Left = 1
      Top = 1
      Width = 696
      Height = 134
      Align = alClient
      ColCount = 1
      DataSource = dsTotales
      PanelHeight = 33
      PanelWidth = 680
      TabOrder = 0
      RowCount = 4
      object TipoLbl: TLabel
        Left = 28
        Top = 12
        Width = 24
        Height = 13
        Caption = 'Tipo:'
      end
      object CF_TIPO: TZetaDBTextBox
        Left = 65
        Top = 10
        Width = 65
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'CF_TIPO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CF_TIPO'
        DataSource = dsTotales
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object ComidasLbl: TLabel
        Left = 175
        Top = 12
        Width = 43
        Height = 13
        Caption = 'Comidas:'
      end
      object CF_COMIDAS: TZetaDBTextBox
        Left = 232
        Top = 10
        Width = 65
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'CF_COMIDAS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CF_COMIDAS'
        DataSource = dsTotales
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object ExtrasLbl: TLabel
        Left = 341
        Top = 12
        Width = 32
        Height = 13
        Caption = 'Extras:'
      end
      object CF_EXTRAS: TZetaDBTextBox
        Left = 384
        Top = 10
        Width = 65
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'CF_EXTRAS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CF_EXTRAS'
        DataSource = dsTotales
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
  end
  object dsComidas: TDataSource
    Left = 208
    Top = 80
  end
  object dsTotales: TDataSource
    Left = 256
    Top = 80
  end
end
