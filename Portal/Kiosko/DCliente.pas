unit DCliente;

interface

{$DEFINE REPORTING}
{$INCLUDE DEFINES.INC}


uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,
     {$ifdef DOS_CAPAS}
     DServerPortalTress,
     DServerRecursos,
     DServerAsistencia,
     DServerReportes,
     DserverGlobal,
   {$IFDEF REPORTING}
     DServerReporting,
   {$else}
     DServerCalcNomina,
   {$endif}
     DServerSuper,
     DServerLogin,
     DServerCatalogos,
     {$else}
     Asistencia_TLB,
     PortalTress_TLB,
     Recursos_TLB,
     Reportes_TLB,
     Global_TLB,
     DCalcNomina_TLB,
     Super_TLB,
     Login_TLB,
     Catalogos_TLB,
     {$endif}
     DBasicoCliente,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaClientDataSet;

const
     K_CAMPO_DIGITO = 'CM_ALIAS'; { Ojo si se usa COMPANYS.CM_ALIAS en el OleVariant de Empresa Activa }
type
  TdmCliente = class(TBasicoCliente)
    cdsEmpleado: TZetaClientDataSet;
    cdsCursos: TZetaClientDataSet;
    cdsVacHistorial: TZetaClientDataSet;
    cdsVacTotal: TZetaClientDataSet;
    cdsAhorros: TZetaClientDataSet;
    cdsPrestamos: TZetaClientDataSet;
    cdsCalendario: TZetaClientDataSet;
    cdsTarjeta: TZetaClientDataSet;
    cdsChecadas: TZetaClientDataSet;
    cdsNomina: TZetaClientDataSet;
    cdsConceptos: TZetaClientDataSet;
    cdsPrenomina: TZetaClientDataSet;
    cdsAusencia: TZetaClientDataSet;
    cdsCafPeriodo: TZetaClientDataSet;
    cdsCafPeriodoTotales: TZetaClientDataSet;
    cdsPeriodo: TZetaClientDataSet;
    cdsHisCursos: TZetaClientDataSet;
    cdsReportes: TZetaClientDataSet;
    cdsPeriodoActivo: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsEmpleadoAlCrearCampos(Sender: TObject);
    procedure cdsEmpleadoCalcFields(DataSet: TDataSet);
    procedure cdsCursosAfterOpen(DataSet: TDataSet);
    procedure cdsVacHistorialAfterOpen(DataSet: TDataSet);
    procedure cdsVacTotalAfterOpen(DataSet: TDataSet);
    procedure cdsAhorrosAfterOpen(DataSet: TDataSet);
    procedure cdsPrestamosAfterOpen(DataSet: TDataSet);
    procedure cdsCalendarioAfterOpen(DataSet: TDataSet);
    procedure cdsCalendarioAlCrearCampos(Sender: TObject);
    procedure cdsCalendarioCalcFields(DataSet: TDataSet);
    procedure cdsChecadasAfterOpen(DataSet: TDataSet);
    procedure cdsTarjetaAfterOpen(DataSet: TDataSet);
    procedure cdsConceptosAfterOpen(DataSet: TDataSet);
    procedure cdsAusenciaAfterOpen(DataSet: TDataSet);
    procedure cdsCafPeriodoAfterOpen(DataSet: TDataSet);
    procedure cdsHisCursosAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    FEmpleadoActivo: TNumEmp;
    FTipoNomina: eTipoPeriodo;
    FParametros: TZetaParams;

    {$ifdef DOS_CAPAS}
    FServerAsistencia: TdmServerAsistencia;
    FServerPortalTress: TdmPortalTress;
    FServerRecursos: TdmServerRecursos;
    FServerReportes: TdmServerReportes;
    FServerGlobal: TdmServerGlobal;
    {$ifdef REPORTING}
    FServerReporteador: TdmServerReporting;
    {$else}
    FServerReporteador: TdmServerCalcNomina;
    {$endif}
    FServerSuper : TdmServerSuper;
    FServerLogin: TdmServerLogin;
    FServerCatalogos: TdmServerCatalogos;
    {$else}
    FServerAsistencia: IdmServerAsistenciaDisp;
    FServerPortalTress: IdmPortalTress;
    FServerRecursos : IdmServerRecursosDisp;
    FServerReportes : IdmServerReportesDisp;
    FServerGlobal : IdmServerGlobalDisp;
    FServerReporteador : IdmServerCalcNominaDisp;
    FServerSuper : IdmServerSuperDisp;
    FServerLogin : IdmServerLoginDisp;
    FServerCatalogos: IdmServerCatalogosDisp;

    function GetServerAsistencia: IdmServerAsistenciaDisp;
    function GetServerPortalTress: IdmPortalTressDisp;
    function GetServerRecursos: IdmServerRecursosDisp;
    function GetServerReportes: IdmServerReportesDisp;
    function GetServerGlobal: IdmServerGlobalDisp;
    function GetServerReporteador: IdmServerCalcNominaDisp;
    //function GetServerLogin: IdmServerLoginDisp;
    //function GetServerCatalogos: IdmServerCatalogosDisp;
    {$endif}

    procedure AU_FECHAGetText( Sender: TField; var Text: String; DisplayText: Boolean);

    function GetIMSSMes: Integer;
    function GetIMSSPatron: String;
    function GetIMSSTipo: eTipoLiqIMSS;
    function GetIMSSYear: Integer;


  protected
    { Protected declarations }

    {$ifdef DOS_CAPAS}
    property ServerAsistencia: TdmServerAsistencia read FServerAsistencia;
    property ServerPortalTress: TdmPortalTress read FServerPortalTress;
    property ServerRecursos: TdmServerRecursos read FServerRecursos;
    {$else}
    property ServerAsistencia: IdmServerAsistenciaDisp read GetServerAsistencia;
    property ServerPortalTress: IdmPortalTressDisp read GetServerPortalTress;
    property ServerRecursos: IdmServerRecursosDisp read GetServerRecursos;
    {$endif}
    procedure CompanyFind( const sDigito: String );

    procedure CargaActivosIMSS( Parametros: TZetaParams );
    procedure CargaActivosPeriodo( Parametros: TZetaParams );
    procedure CargaActivosSistema( Parametros: TZetaParams );


    property Parametros: TZetaParams read FParametros;

  public
    { Public declarations }
    {$ifdef DOS_CAPAS}
    property ServerGlobal: TdmServerGlobal read FServerGlobal;
    property ServerReportes: TdmServerReportes read FServerReportes;
    {$ifdef REPORTING}
    property ServerReporteador: TdmServerReporting read FServerReporteador;
    {$else}
    property ServerReporteador: TdmServerCalcNomina read FServerReporteador;
    {$endif}
    property ServerSuper: TdmServerSuper read FServerSuper;
    property ServerLogin: TdmServerLogin read FServerLogin;
    property ServerCatalogos: TdmServerCatalogos read FServerCatalogos;
    {$else}
    property ServerGlobal: IdmServerGlobalDisp read GetServerGlobal;
    property ServerReportes: IdmServerReportesDisp read GetServerReportes;
    property ServerReporteador: IdmServerCalcNominaDisp read GetServerReporteador;
    property ServerSuper: IdmServerSuperDisp read FServerSuper;
    property ServerLogin: IdmServerLoginDisp read FServerLogin;
    property ServerCatalogos: IdmServerCatalogosDisp read FServerCatalogos;
    {$endif}

    property IMSSPatron: String read GetIMSSPatron;
    property IMSSYear: Integer read GetIMSSYear;
    property IMSSTipo: eTipoLiqIMSS read GetIMSSTipo;
    property IMSSMes: Integer read GetIMSSMes;
    function GetDatosPeriodoActivo: TDatosPeriodo;


    property EmpleadoActivo: TNumEmp read FEmpleadoActivo;
    property Empleado: TNumEmp read FEmpleadoActivo;
    property TipoNomina: eTipoPeriodo read FTipoNomina;
    function PosicionaEmpleado(const sDigito, sCredencial: String; const iEmpleado: TNumEmp; var sTexto: String): Boolean;
    function CompanyLoad: Boolean;
    procedure CompanySet(const sEmpresa, sDigito: String);
    procedure CompanyUnload;

    procedure CargaActivosTodos( Parametros: TZetaParams );override;

    procedure AbreAhorros;
    procedure AbreCalendario( const iMes, iYear: Word );
    procedure AbreComidas( const iYear: Integer );
    procedure AbreComidasAnterior( const iYear, iNumero: Integer );
    procedure AbreComidasSiguiente( const iYear, iNumero: Integer );
    procedure AbreCursos;
    procedure AbrePersonales;
    procedure AbrePrestamos;
    procedure AbreTarjeta( const dFecha: TDate );
    procedure AbreVacaciones;
    procedure AbreNomina( const iYear: Integer );
    procedure AbreNominaAnterior( const iYear, iNumero: Integer );
    procedure AbreNominaSiguiente( const iYear, iNumero: Integer );
    procedure AbrePrenomina( const iYear: Integer );
    procedure AbrePrenominaAnterior( const iYear, iNumero: Integer );
    procedure AbrePrenominaSiguiente( const iYear, iNumero: Integer );
    procedure CargaReportes;
    procedure GeneraReporte(const iReporte: integer);
    function  GetValorActivo( const eValor : {$ifdef RDD}eRDDValorActivo{$else}string{$endif} ): String; override;

  end;

var
  dmCliente: TdmCliente;

implementation

uses
     DReportes,
     DDiccionario,
     ZReportConst,
     ZetaCommonTools,
     ZetaRegistryCliente,
     FKioscoRegistry;

{$R *.DFM}

const
     K_LEN_ANTIGUO = 40;

{ ******** TdmCliente ******** }

procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
     inherited;
     {$ifdef DOS_CAPAS}
     FServerPortalTress := TdmPortalTress.Create( Self );
     FServerAsistencia := TdmServerAsistencia.Create( Self );
     FServerRecursos := TdmServerRecursos.Create( self );
     FServerReportes := TdmServerReportes.Create( self );
     FServerGlobal := TdmServerGlobal.Create( self );
     {$ifdef REPORTING}
     FServerReporteador := TdmServerReporting.Create( self );
     {$else}
     FServerReporteador := TdmServerCalcNomina.Create( self );
     {$endif}
     FServerCatalogos := TdmServerCatalogos.Create( self );
     {$endif}
     ZetaRegistryCliente.InitClientRegistry;
     FKioscoRegistry.InitKioskoRegistry;

     FParametros:= TZetaParams.Create;

     Usuario := 999;
end;

procedure TdmCliente.DataModuleDestroy(Sender: TObject);
begin
     FKioscoRegistry.ClearKioskoRegistry;
     ZetaRegistryCliente.ClearClientRegistry;

     FreeAndNil( FParametros );

     {$ifdef DOS_CAPAS}
     FreeAndNil( FServerCatalogos );
     FreeAndNil( FServerReporteador );
     FreeAndNil( FServerGlobal );
     FreeAndNil( FServerReportes );
     FreeAndNil( FServerRecursos );
     FreeAndNil( FServerAsistencia );
     FreeAndNil( FServerPortalTress );
     {$endif}
     inherited;
end;

{$ifndef DOS_CAPAS}
function TdmCliente.GetServerAsistencia: IdmServerAsistenciaDisp;
begin
     Result := IdmServerAsistenciaDisp( CreaServidor( CLASS_dmServerAsistencia, FServerAsistencia ) );
end;

function TdmCliente.GetServerPortalTress: IdmPortalTressDisp;
begin
     Result := IdmPortalTressDisp( CreaServidor( Class_dmPortalTress, FServerPortalTress ) );
end;

function TdmCliente.GetServerRecursos: IdmServerRecursosDisp;
begin
     Result:= IdmServerRecursosDisp( CreaServidor( CLASS_dmServerRecursos, FServerRecursos ) );
end;

function TdmCliente.GetServerReportes: IdmServerReportesDisp;
begin
     Result:= IdmServerReportesDisp( CreaServidor( CLASS_dmServerReportes, FServerReportes ) );
end;

function TdmCliente.GetServerReporteador: IdmServerCalcNominaDisp;
begin
     Result:= IdmServerCalcNominaDisp( CreaServidor( CLASS_dmServerCalcNomina, FServerReporteador ) );
end;

function TdmCliente.GetServerGlobal: IdmServerGlobalDisp;
begin
     Result:= IdmServerGlobalDisp( CreaServidor( CLASS_dmServerGlobal, FServerGlobal ) );
end;

{$endif}

{ ***** M�todos ******** }

function TdmCliente.PosicionaEmpleado(const sDigito, sCredencial: String; const iEmpleado: TNumEmp; var sTexto: String): Boolean;
begin
     Result := False;
     try
        CompanyFind( sDigito );
        if EmpresaAbierta then
        begin
             with cdsEmpleado do
             begin
                  Data := ServerPortalTress.KioskoEmpleado( Empresa, iEmpleado );
                  if IsEmpty then
                  begin
                       sTexto := Format( 'Empleado %d No Existe', [ iEmpleado ] );
                  end
                  else
                  begin
                       Result := ( sCredencial = FieldByName( 'CB_CREDENC' ).AsString );
                       if Result then
                       begin
                            FEmpleadoActivo := iEmpleado;
                            //FTipoNomina := eTipoPeriodo( FieldByName( 'TU_NOMINA' ).AsInteger );
                            FTipoNomina := eTipoPeriodo( FieldByName( 'CB_NOMINA' ).AsInteger );
                            //Result := True;
                       end
                       else
                           sTexto := 'Letra De Credencial Incorrecta';
                  end;
             end;
        end
        else
            sTexto := Format( 'No Hay Empresas Con D�gito %s', [ sDigito ] );
     except
           on Error: Exception do
           begin
                sTexto := Format( 'Error Al Buscar Empleado %d: %s', [ iEmpleado, Error.Message ] );
           end;
     end;
end;

procedure TdmCliente.CompanyFind( const sDigito: String );
begin
     EmpresaAbierta := False;
     if cdsCompany.Locate( K_CAMPO_DIGITO, sDigito, [] ) then
     begin
          SetCompany;
          EmpresaAbierta := True;
     end;
end;

function TdmCliente.CompanyLoad: Boolean;
begin
     try
        with cdsCompany do
        begin
             Data := Servidor.GetCompanys( 0, Ord( tc3Datos ) );
             while not Eof do
             begin
                  Edit;
                  FieldByName( K_CAMPO_DIGITO ).AsString := KioskoRegistry.GetLetraCredencial( FieldByName( 'CM_CODIGO' ).AsString );
                  Post;
                  Next;
             end;
             MergeChangeLog;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                Result := False;
           end;
     end;
end;

procedure TdmCliente.CompanySet( const sEmpresa, sDigito: String );
begin
     with cdsCompany do
     begin
          if Locate( 'CM_CODIGO', sEmpresa, [] ) then
          begin
               Edit;
               FieldByName( K_CAMPO_DIGITO ).AsString := sDigito;
               Post;
          end;
     end;
end;

procedure TdmCliente.CompanyUnload;
begin
     with KioskoRegistry do
     begin
          ResetLetraCredencial;
          with cdsCompany do
          begin
               First;
               while not Eof do
               begin
                    SetLetraCredencial( FieldByName( 'CM_CODIGO' ).AsString, FieldByName( K_CAMPO_DIGITO ).AsString );
                    Next;
               end;
          end;
     end;
end;

function TdmCliente.GetIMSSMes: Integer;
begin
     Result := TheMonth(Date);
end;

function TdmCliente.GetIMSSPatron: String;
begin
     Result := '';
end;

function TdmCliente.GetIMSSTipo: eTipoLiqIMSS;
begin
     Result := tlOrdinaria;
end;

function TdmCliente.GetIMSSYear: Integer;
begin
     Result := TheYear( Date );
end;


procedure TdmCliente.CargaActivosSistema;
begin
     with Parametros do
     begin
          AddDate( 'FechaAsistencia', FechaDefault );
          AddDate( 'FechaDefault', FechaDefault );
          AddInteger( 'YearDefault', TheYear( FechaDefault ) );
          AddInteger( 'EmpleadoActivo',  EmpleadoActivo );
          AddString( 'NombreUsuario', 'REP_KIOSKO' );
          AddString( 'CodigoEmpresa', GetDatosEmpresaActiva.Codigo );
     end;
end;

procedure TdmCliente.CargaActivosIMSS(Parametros: TZetaParams);
begin
     with Parametros do
     begin
          AddString( 'RegistroPatronal', ImssPatron );
          AddInteger( 'IMSSYear', ImssYear );
          AddInteger( 'IMSSMes', ImssMes );
          AddInteger( 'IMSSTipo', Ord( ImssTipo ) );
     end;
end;

procedure TdmCliente.CargaActivosPeriodo(Parametros: TZetaParams);
begin
     with cdsPeriodoActivo do
     begin
          if NOT Active or IsEmpty then
             Data := Servidor.GetPeriodoInicial( Empresa, TheYear(Date), Ord( tpSemanal ) );

          with Parametros do
          begin
               AddInteger( 'Year', FieldByName( 'PE_YEAR' ).AsInteger );
               AddInteger( 'Tipo', FieldByName( 'PE_TIPO' ).AsInteger );
               AddInteger( 'Numero', FieldByName( 'PE_NUMERO' ).AsInteger );
          end;
     end;
end;

procedure TdmCliente.CargaActivosTodos( Parametros: TZetaParams );
begin
     CargaActivosIMSS( Parametros );
     CargaActivosPeriodo( Parametros );
     CargaActivosSistema( Parametros );
end;

function TdmCliente.GetDatosPeriodoActivo: TDatosPeriodo;
begin
     with Result do
     begin
          with cdsPeriodoActivo do
          begin
               Year := FieldByName( 'PE_YEAR' ).AsInteger;
               Tipo := eTipoPeriodo(FieldByName( 'PE_TIPO' ).AsInteger);
               Numero := FieldByName( 'PE_NUMERO' ).AsInteger;
               Inicio := FieldByName( 'PE_FEC_INI' ).AsDateTime;
               Fin := FieldByName( 'PE_FEC_FIN' ).AsDateTime;
               Pago := FieldByName( 'PE_FEC_PAG' ).AsDateTime;
               Dias := FieldByName( 'PE_DIAS' ).AsInteger;
               Status := eStatusPeriodo( FieldByName( 'PE_STATUS' ).AsInteger );
               Uso := eUsoPeriodo( FieldByName( 'PE_USO' ).AsInteger );
               Mes := ZetaCommonTools.iMax( 1, FieldByName( 'PE_MES' ).AsInteger );
               SoloExcepciones := ZetaCommonTools.zStrToBool( FieldByName( 'PE_SOLO_EX' ).AsString );
               IncluyeBajas := ZetaCommonTools.zStrToBool( FieldByName( 'PE_INC_BAJ' ).AsString );
          end;
     end;
end;


{ ******** Datos Personales ********** }

procedure TdmCliente.AbrePersonales;
begin
end;

procedure TdmCliente.cdsEmpleadoAlCrearCampos(Sender: TObject);
begin
     inherited;
     with cdsEmpleado do
     begin
          CreateCalculated( 'CB_ANTIGUO', ftString, K_LEN_ANTIGUO );
          CreateCalculated( 'CB_VENCE', ftDate, 0 );
     end;
end;

procedure TdmCliente.cdsEmpleadoCalcFields(DataSet: TDataSet);

function CalculaVencimiento( const dFecha: TDateTime; const iDuracion: Integer ): TDateTime;
{ Tomada de D:\3Win_20\Datamodules\DRecursos.pas }
begin
     if ( iDuracion = 0 ) then
         Result := 0
     else
         Result := dFecha + iDuracion - 1;
end;

begin
     inherited;
     with cdsEmpleado do
     begin
          FieldByName( 'CB_ANTIGUO' ).AsString := Copy( ZetaCommonTools.TiempoDias( Date - FieldByName( 'CB_FEC_ANT' ).AsDateTime + 1, etDias ), 1, K_LEN_ANTIGUO );
          FieldByName( 'CB_VENCE' ).AsDateTime := CalculaVencimiento( FieldByName( 'CB_FEC_CON').AsDateTime, FieldByName( 'TB_DIAS').AsInteger );
     end;
end;

{ ******** Cursos ******** }

procedure TdmCliente.AbreCursos;
begin
     cdsCursos.Data := ServerRecursos.GetHisCursosProg( Empresa, EmpleadoActivo,FALSE );
     cdsHisCursos.Data := ServerPortalTress.KioscoCursosTomados( Empresa, EmpleadoActivo );
end;

procedure TdmCliente.cdsCursosAfterOpen(DataSet: TDataSet);
begin
     inherited;
     with cdsCursos do
     begin
          MaskFecha( 'KC_FEC_PRO' );
          MaskFecha( 'KC_FEC_TOM' );
          MaskFecha( 'KC_PROXIMO' );
     end;
end;

procedure TdmCliente.cdsHisCursosAfterOpen(DataSet: TDataSet);
begin
     inherited;
     with cdsHisCursos do
     begin
          MaskFecha( 'KC_FEC_TOM' );
          MaskNumerico( 'SE_FOLIO', '0;-0;#' );
     end;
end;

{ ******** Vacaciones ******** }

procedure TdmCliente.AbreVacaciones;
var
   DatosEmpleado: OleVariant;
   dFecha: TDate;
begin
     with cdsEmpleado do
     begin
          if ZetaCommonTools.zStrToBool( FieldByName( 'CB_ACTIVO' ).AsString ) then
             dFecha := Date
          else
              dFecha := FieldByName( 'CB_FEC_BAJ' ).AsDateTime;
     end;
     cdsVacHistorial.Data := ServerRecursos.GetHisVacacion( Empresa, EmpleadoActivo, dFecha, DatosEmpleado );
     cdsVacTotal.Data := DatosEmpleado;
end;

procedure TdmCliente.cdsVacHistorialAfterOpen(DataSet: TDataSet);
begin
     inherited;
     with cdsVacHistorial do
     begin
          ListaFija( 'VA_TIPO', lfTipoVacacion );
          MaskHoras( 'VA_D_PAGO' );
          MaskHoras( 'VA_PAGO' );
          MaskHoras( 'VA_S_PAGO' );
          MaskHoras( 'VA_D_GOZO' );
          MaskHoras( 'VA_GOZO' );
          MaskHoras( 'VA_S_GOZO' );
          MaskFecha( 'VA_FEC_INI' );
          MaskFecha( 'VA_FEC_FIN' );
     end;
end;

procedure TdmCliente.cdsVacTotalAfterOpen(DataSet: TDataSet);
begin
     inherited;
     with cdsVacTotal do
     begin
         // Dias Pagados
         MaskPesos( 'CB_DER_PAG' );
         MaskPesos( 'CB_V_PAGO' );
         MaskPesos( 'CB_SUB_PAG' );
         MaskPesos( 'CB_PRO_PAG' );
         MaskPesos( 'CB_TOT_PAG' );
         // Dias Gozados
         MaskPesos( 'CB_DER_GOZ' );
         MaskPesos( 'CB_V_GOZO' );
         MaskPesos( 'CB_SUB_GOZ' );
         MaskPesos( 'CB_PRO_GOZ' );
         MaskPesos( 'CB_TOT_GOZ' );
         // Fechas
         MaskFecha( 'CB_FEC_VAC' );
         MaskFecha( 'CB_DER_FEC' );
         MaskFecha( 'CB_FEC_ANT' );
     end;
end;

{ ******** Comidas ******** }

procedure TdmCliente.AbreComidas(const iYear: Integer);
var
   oPeriodo, oTotales: OleVariant;
begin
     cdsCafPeriodo.Data := ServerPortalTress.KioskoComidasInit( Empresa, EmpleadoActivo, iYear, Ord( TipoNomina ), oPeriodo, oTotales );
     cdsPeriodo.Data := oPeriodo;
     cdsCafPeriodoTotales.Data := oTotales;
end;

procedure TdmCliente.AbreComidasAnterior(const iYear, iNumero: Integer);
var
   oPeriodo, oTotales: OleVariant;
begin
     cdsCafPeriodo.Data := ServerPortalTress.KioskoComidasAnterior( Empresa, EmpleadoActivo, iYear, Ord( TipoNomina ), iNumero, oPeriodo, oTotales );
     cdsPeriodo.Data := oPeriodo;
     cdsCafPeriodoTotales.Data := oTotales;
end;

procedure TdmCliente.AbreComidasSiguiente(const iYear, iNumero: Integer);
var
   oPeriodo, oTotales: OleVariant;
begin
     cdsCafPeriodo.Data := ServerPortalTress.KioskoComidasSiguiente( Empresa, EmpleadoActivo, iYear, Ord( TipoNomina ), iNumero, oPeriodo, oTotales );
     cdsPeriodo.Data := oPeriodo;
     cdsCafPeriodoTotales.Data := oTotales;
end;

procedure TdmCliente.cdsCafPeriodoAfterOpen(DataSet: TDataSet);
begin
     inherited;
     with cdsCafPeriodo do
     begin
          MaskTime( 'CF_HORA' );
     end;
end;

{ ******** Ahorros ******** }

procedure TdmCliente.AbreAhorros;
begin
     cdsAhorros.Data := ServerPortalTress.KioskoAhorros( Empresa, EmpleadoActivo );
end;

procedure TdmCliente.cdsAhorrosAfterOpen(DataSet: TDataSet);
begin
     inherited;
     with cdsAhorros do
     begin
          ListaFija( 'AH_STATUS', lfStatusAhorro );
     end;
end;

{ ******** Prestamos ******** }

procedure TdmCliente.AbrePrestamos;
begin
     cdsPrestamos.Data := ServerPortalTress.KioskoPrestamos( Empresa, EmpleadoActivo );
end;

procedure TdmCliente.cdsPrestamosAfterOpen(DataSet: TDataSet);
begin
     inherited;
     with cdsPrestamos do
     begin
          ListaFija( 'PR_STATUS', lfStatusPrestamo );
     end;
end;

{ ******** Calendario ******** }

procedure TdmCliente.AbreCalendario(const iMes, iYear: Word);
var
   dInicial, dFinal: TDate;
begin
     dInicial := EncodeDate( iYear, iMes, 1 );
     dFinal := ZetaCommonTools.LastDayOfMonth( dInicial );
     cdsCalendario.Data := ServerAsistencia.GetCalendario( Empresa, EmpleadoActivo, dInicial, dFinal );
end;

procedure TdmCliente.cdsCalendarioAlCrearCampos(Sender: TObject);
begin
     inherited;
     with cdsCalendario do
     begin
          CreateCalculated( 'AU_DIA_SEM', ftString, 10 );
     end;
end;

procedure TdmCliente.cdsCalendarioAfterOpen(DataSet: TDataSet);
begin
     inherited;
     with cdsCalendario do
     begin
          MaskFecha( 'AU_FECHA' );
          ListaFija( 'AU_STATUS', lfStatusAusencia );
          ListaFija( 'AU_TIPODIA', lfTipoDiaAusencia );
          MaskHoras( 'AU_HORASCK');
          MaskHoras( 'AU_HORAS');
          MaskHoras( 'AU_EXTRAS');
          MaskHoras( 'AU_PER_CG');
          MaskHoras( 'AU_PER_SG');
          MaskHoras( 'AU_DES_TRA');
          MaskHoras( 'AU_TARDES');
     end;
end;

procedure TdmCliente.cdsCalendarioCalcFields(DataSet: TDataSet);
begin
     inherited;
     with Dataset do
     begin
          FieldByName( 'AU_DIA_SEM' ).AsString := ZetaCommonTools.DiaSemana( FieldByName( 'AU_FECHA' ).AsDateTime );
     end;
end;

{ ******** Asistencia ******** }

procedure TdmCliente.AbreTarjeta( const dFecha: TDate );
var
   oChecadas: OleVariant;
begin
     cdsTarjeta.Data := ServerPortalTress.KioskoTarjeta( Empresa, EmpleadoActivo, dFecha, oChecadas );
     cdsChecadas.Data := oChecadas;
end;

procedure TdmCliente.cdsTarjetaAfterOpen(DataSet: TDataSet);
begin
     inherited;
     with cdsChecadas do
     begin
          MaskTime( 'HO_HOR_IN' );
          MaskTime( 'HO_HOR_OU' );
     end;
end;

procedure TdmCliente.cdsChecadasAfterOpen(DataSet: TDataSet);
begin
     inherited;
     with cdsChecadas do
     begin
          ListaFija( 'CH_TIPO', lfTipoChecadas );
          ListaFija( 'CH_DESCRIP', lfDescripcionChecadas );
          MaskTime( 'CH_H_REAL' );
          MaskTime( 'CH_H_AJUS' );
          MaskHoras( 'CH_HOR_ORD' );
          MaskHoras( 'CH_HOR_EXT' );
          MaskHoras( 'CH_HOR_DES' );
     end;
end;

{ ********** N�minas ********** }

procedure TdmCliente.AbreNomina( const iYear: Integer );
var
   oConceptos: OleVariant;
begin
     cdsNomina.Data := ServerPortalTress.KioskoNominaInit( Empresa, EmpleadoActivo, iYear, Ord( TipoNomina ), oConceptos );
     cdsConceptos.Data := oConceptos;
end;

procedure TdmCliente.AbreNominaAnterior( const iYear, iNumero: Integer );
var
   oConceptos: OleVariant;
begin
     cdsNomina.Data := ServerPortalTress.KioskoNominaAnterior( Empresa, EmpleadoActivo, iYear, Ord( TipoNomina ), iNumero, oConceptos );
     cdsConceptos.Data := oConceptos;
end;

procedure TdmCliente.AbreNominaSiguiente( const iYear, iNumero: Integer );
var
   oConceptos: OleVariant;
begin
     cdsNomina.Data := ServerPortalTress.KioskoNominaSiguiente( Empresa, EmpleadoActivo, iYear, Ord( TipoNomina ), iNumero, oConceptos );
     cdsConceptos.Data := oConceptos;
end;

procedure TdmCliente.cdsConceptosAfterOpen(DataSet: TDataSet);
const
     K_MASK_NUMEROS = '#,0.00;;#';
begin
     inherited;
     with cdsConceptos do
     begin
          MaskNumerico( 'MO_PERCEPC', K_MASK_NUMEROS );
          MaskNumerico( 'MO_DEDUCCI', K_MASK_NUMEROS );
          MaskNumerico( 'MO_X_ISPT', K_MASK_NUMEROS );
          MaskNumerico( 'MO_IMP_CAL', K_MASK_NUMEROS );
          ListaFija( 'CO_TIPO', lfTipoConcepto );
     end;
end;

{ ********** Pren�minas ********** }

procedure TdmCliente.AbrePrenomina( const iYear: Integer );
var
   oAusencias: OleVariant;
begin
     cdsPrenomina.Data := ServerPortalTress.KioskoPrenominaInit( Empresa, EmpleadoActivo, iYear, Ord( TipoNomina ), oAusencias );
     cdsAusencia.Data := oAusencias;
end;

procedure TdmCliente.AbrePrenominaAnterior( const iYear, iNumero: Integer );
var
   oAusencias: OleVariant;
begin
     cdsPrenomina.Data := ServerPortalTress.KioskoPrenominaAnterior( Empresa, EmpleadoActivo, iYear, Ord( TipoNomina ), iNumero, oAusencias );
     cdsAusencia.Data := oAusencias;
end;

procedure TdmCliente.AbrePrenominaSiguiente( const iYear, iNumero: Integer );
var
   oAusencias: OleVariant;
begin
     cdsPrenomina.Data := ServerPortalTress.KioskoPrenominaSiguiente( Empresa, EmpleadoActivo, iYear, Ord( TipoNomina ), iNumero, oAusencias );
     cdsAusencia.Data := oAusencias;
end;

procedure TdmCliente.cdsAusenciaAfterOpen(DataSet: TDataSet);
begin
     inherited;
     with cdsAusencia do
     begin
          FieldByName( 'AU_FECHA' ).OnGetText := AU_FECHAGetText;
          ListaFija( 'AU_STATUS', lfStatusAusencia );
          ListaFija( 'AU_TIPODIA', lfTipoDiaAusencia );
          MaskHoras( 'AU_HORAS' );
          MaskHoras( 'AU_EXTRAS' );
          MaskHoras( 'AU_DOBLES' );
          MaskHoras( 'AU_TRIPLES' );
          MaskHoras( 'AU_TARDES' );
          MaskHoras( 'AU_PER_CG' );
          MaskHoras( 'AU_PER_SG' );
          MaskHoras( 'AU_DES_TRA' );
     end;
end;

procedure TdmCliente.AU_FECHAGetText( Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if Sender.DataSet.IsEmpty then
        Text := ''
     else
         Text := Sender.AsString + ' -  ' + ZetaCommonLists.ObtieneElemento( lfDiasSemana, DayOfWeek( Sender.AsDateTime ) - 1 );
end;


procedure TdmCliente.CargaReportes;
begin
     cdsReportes.Data :=  ServerReportes.GetReportes( Empresa, ord(crKiosco), ord(crFavoritos),ord(crSuscripciones), VACIO , TRUE );
end;

procedure TdmCliente.GeneraReporte( const iReporte: integer );
begin
     if dmReportes = NIL then
        dmReportes := TDmReportes.Create( self );

     dmReportes.GeneraUnReporte( iReporte );

end;

function TdmCliente.GetValorActivo( const eValor : {$ifdef RDD}eRDDValorActivo{$else}string{$endif} ): String;
begin
     Result := dmDiccionario.GetValorActivo( eValor );
end;

end.
