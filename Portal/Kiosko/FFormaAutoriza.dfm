�
 TFORMAAUTORIZA 0�.  TPF0�TFormaAutorizaFormaAutorizaLeft� Top� CaptionFormaAutorizaClientHeight�ClientWidth.PixelsPerInch`
TextHeight �TPanelPanelSuperiorTop/Width.Height�  TPanel
PanelDatosLeftTopWidth,Height� AlignalClientParentColor	TabOrder  	TGroupBox	GBHorarioLeftTopWidth� Height� AlignalLeftCaption	 Horario Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  TZetaDBTextBox	HO_HOR_INLeftGTop*Width<Height	AlignmenttaRightJustifyAutoSizeCaption	HO_HOR_INColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataField	HO_HOR_IN
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox	HO_HOR_OULeftGTop@Width<Height	AlignmenttaRightJustifyAutoSizeCaption	HO_HOR_OUColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataField	HO_HOR_OU
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TLabelHO_HOR_INlblLeftTop.Width1Height	AlignmenttaRightJustifyCaptionEntrada:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelHO_HOR_OUlblLeftTopDWidth(Height	AlignmenttaRightJustifyCaptionSalida:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelHO_DESCRIPlblLeftTopWidth0Height	AlignmenttaRightJustifyCaptionNombre:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TZetaDBTextBox
HO_DESCRIPLeftGTopWidth� HeightAutoSizeCaption
HO_DESCRIPColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataField
HO_DESCRIP
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m   	TGroupBoxGBHorasLeft� TopWidth� Height� AlignalLeftCaption Horas Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder TZetaDBTextBox
AU_HORASCKLeftWTopWidth4Height	AlignmenttaRightJustifyAutoSizeCaption
AU_HORASCKColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataField
AU_HORASCK
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox	AU_TARDESLeftWTop*Width4Height	AlignmenttaRightJustifyAutoSizeCaption	AU_TARDESColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataField	AU_TARDES
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TLabelAU_HORASCKlblLeftTopWidth>Height	AlignmenttaRightJustifyCaptionOrdinarias:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelAU_TARDESlblLeft)Top.Width,Height	AlignmenttaRightJustifyCaptionTardes:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont   	TGroupBoxGBHorasExtrasLeft�TopWidth� Height� AlignalLeftCaption Horas Extras EnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder TLabel	SinExtrasLeftTopWidth� Height� AlignalClient	AlignmenttaCenterAutoSizeCaption(Sin Tiempo Extra Autorizado Ni TrabajadoFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontWordWrap	  TLabelAU_AUT_EXTlblLeftBTopWidthGHeight	AlignmenttaRightJustifyCaptionAutorizadas:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelAU_EXTRASlblLeftWTop.Width2Height	AlignmenttaRightJustifyCaptionA Pagar:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TZetaDBTextBox
AU_AUT_EXTLeft� TopWidth4Height	AlignmenttaRightJustifyAutoSizeCaption
AU_AUT_EXTColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataField
AU_AUT_EXT
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox	AU_EXTRASLeft� Top*Width4Height	AlignmenttaRightJustifyAutoSizeCaption	AU_EXTRASColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataField	AU_EXTRAS
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TLabelAU_AUT_TRAlblLeftTopDWidth}Height	AlignmenttaRightJustifyCaptionDescanso Autorizado:  TZetaDBTextBox
AU_AUT_TRALeft� Top@Width4Height	AlignmenttaRightJustifyAutoSizeCaption
AU_AUT_TRAColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataField
AU_AUT_TRA
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox
AU_DES_TRALeft� TopVWidth4Height	AlignmenttaRightJustifyAutoSizeCaption
AU_DES_TRAColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataField
AU_DES_TRA
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TLabelAU_DES_TRAlblLeftTopZWidthzHeight	AlignmenttaRightJustifyCaptionDescanso Trabajado:   	TGroupBox
GBPermisosLeftTTopWidth� Height� AlignalClientCaption
 Permisos EnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder TLabelSinPermisosLeftTopWidth� Height� AlignalClient	AlignmenttaCenterAutoSizeCaptionSin Permisos AutorizadosFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontWordWrap	  TLabelAU_CG_ENTlblLeft.TopWidth� Height	AlignmenttaRightJustifyCaptionCon Goce A La Entrada:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelAU_SG_ENTlblLeft2Top.Width� Height	AlignmenttaRightJustifyCaptionSin Goce A La Entrada:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelAU_PER_CGlblLeftATopDWidthxHeight	AlignmenttaRightJustifyCaptionCon Goce En El D�a:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelAU_PER_SGlblLeftETopYWidthtHeight	AlignmenttaRightJustifyCaptionSin Goce En El D�a:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TZetaDBTextBox	AU_CG_ENTLeft� TopWidth4Height	AlignmenttaRightJustifyAutoSizeCaption	AU_CG_ENTColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataField	AU_CG_ENT
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox	AU_SG_ENTLeft� Top*Width4Height	AlignmenttaRightJustifyAutoSizeCaption	AU_SG_ENTColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataField	AU_SG_ENT
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox	AU_PER_CGLeft� Top@Width4Height	AlignmenttaRightJustifyAutoSizeCaption	AU_PER_CGColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataField	AU_PER_CG
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox	AU_PER_SGLeft� TopVWidth4Height	AlignmenttaRightJustifyAutoSizeCaption	AU_PER_SGColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataField	AU_PER_SG
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m     �TPanelPanelInferiorTop� Width.Height 	TGroupBox
GBChecadasLeftTopWidth,HeightAlignalClientCaption
 Checadas Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  TZetaDBGridZDBGridLeftTopWidth(Height� AlignalClient
DataSource
dsChecadas
FixedColorclInfoBkFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold OptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit 
ParentFontTabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclBlackTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.StylefsBold ColumnsExpanded	FieldName	CH_H_REALTitle.CaptionRealWidth,Visible	 Expanded	FieldName	CH_H_AJUSTitle.CaptionAjustadaWidth5Visible	 Expanded	FieldNameCH_TIPOTitle.CaptionTipoWidthzVisible	 Expanded	FieldName
CH_DESCRIPTitle.CaptionDescripci�nWidthKVisible	 Expanded	FieldName
CH_HOR_ORDTitle.AlignmenttaRightJustifyTitle.Caption
OrdinariasWidthCVisible	 Expanded	FieldName
CH_HOR_EXTTitle.AlignmenttaRightJustifyTitle.CaptionExtrasWidth+Visible	 Expanded	FieldName
CH_HOR_DESTitle.AlignmenttaRightJustifyTitle.CaptionDescansoWidth>Visible	 Expanded	FieldName	US_CODIGOTitle.AlignmenttaRightJustifyTitle.CaptionModific�Width;Visible	 Expanded	FieldName
CH_SISTEMATitle.CaptionSistWidthVisible	 Expanded	FieldName	CH_GLOBALTitle.CaptionAutoWidthVisible	 Expanded	FieldNameCH_RELOJTitle.CaptionRelojWidthFVisible	      �TPanelTituloWidth.Height/Caption$Tarjeta De Asistencia De 02/Jun/2002 TSpeedButton	DayBeforeLeftTopWidth!Height*HintMes Anterior
Glyph.Data
�   �   BM�       v   (               h                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� wwwwwwp wwwwwp wwpwwp wwwwp wpf  p wfffp pffffp wfffp wpf  p wwwwp wwpwwp wwwwwp wwwwwwp ParentShowHintShowHint	OnClickDayBeforeClick  TSpeedButtonDayAfterLeftTopWidth!Height*HintMes SiguienteAnchorsakTopakRight 
Glyph.Data
�   �   BM�       v   (               h                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� wwwwwwp wwwwwp www wwp wwwwp w  `wp wfffp wfff`p wfffp w  `wp wwwwp www wwp wwwwwp wwwwwwp ParentShowHintShowHint	OnClickDayAfterClick   TDataSource
dsChecadasLeft� Top   