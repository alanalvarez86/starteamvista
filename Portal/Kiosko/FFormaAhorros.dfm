�
 TFORMAAHORROS 0  TPF0�TFormaAhorrosFormaAhorrosLeft� TopNCaptionAhorrosClientHeight_PixelsPerInch`
TextHeight �TPanelPanelSuperiorHeight@ TLabelAH_DESCRIPCIONlblLeft� TopWidthjHeight	AlignmenttaRightJustifyCaptionEstado de Cuenta:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TZetaDBTextBoxAH_DESCRIPCIONLeft[TopWidth� HeightAutoSizeCaptionAH_DESCRIPCIONColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataFieldAH_DESCRIPCION
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TDBNavigatorDBNavigatorLeftHTopWidth� Height(
DataSource
DataSourceVisibleButtonsnbFirstnbPriornbNextnbLast TabOrder OnClickDBNavigatorClick   �TPanelPanelInferiorTop`Height�  TLabelAH_TOTALlblLeft� TopWidthrHeight	AlignmenttaRightJustifyCaptionAhorrado en el A�o:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelAH_SALDO_IlblLeft� TopWidth� Height	AlignmenttaRightJustifyCaption (+) Ahorrado en A�os Anteriores:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelAH_ABONOSlblLeft� Top3WidthdHeight	AlignmenttaRightJustifyCaption(+) Otros Abonos:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelAH_CARGOSlblLeft� TopIWidth^Height	AlignmenttaRightJustifyCaption(-) Otros Cargos:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelAH_SALDOlblLeft� Top_Width`Height	AlignmenttaRightJustifyCaption(=) Saldo Actual:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelAH_FECHAlblLeft� Top� Width]Height	AlignmenttaRightJustifyCaptionFecha de Inicio:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelAH_NUMEROlblLeftTop� WidthMHeight	AlignmenttaRightJustifyCaption# de Abonos:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelAH_STATUSlblLeft/Top� Width)Height	AlignmenttaRightJustifyCaptionStatus:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TZetaDBTextBoxAH_TOTALLeftZTopWidthyHeightAutoSizeCaptionAH_TOTALColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataFieldAH_TOTAL
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox
AH_SALDO_ILeftZTopWidthyHeightAutoSizeCaption
AH_SALDO_IColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataField
AH_SALDO_I
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox	AH_ABONOSLeftZTop/WidthyHeightAutoSizeCaption	AH_ABONOSColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataField	AH_ABONOS
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox	AH_CARGOSLeftZTopEWidthyHeightAutoSizeCaption	AH_CARGOSColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataField	AH_CARGOS
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBoxAH_SALDOLeftZTop[WidthyHeightAutoSizeCaptionAH_SALDOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataFieldAH_SALDO
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBoxAH_FECHALeftZTop� WidthyHeightAutoSizeCaptionAH_FECHAColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataFieldAH_FECHA
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox	AH_NUMEROLeftZTop� WidthyHeightAutoSizeCaption	AH_NUMEROColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataField	AH_NUMERO
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox	AH_STATUSLeftZTop� WidthyHeightAutoSizeCaption	AH_STATUSColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataField	AH_STATUS
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m    