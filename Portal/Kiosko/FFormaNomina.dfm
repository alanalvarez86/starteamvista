inherited FormaNomina: TFormaNomina
  Caption = 'FormaNomina'
  ClientHeight = 407
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelSuperior: TPanel
    Height = 86
    object NO_PERCEPClbl: TLabel
      Left = 19
      Top = 15
      Width = 113
      Height = 13
      Alignment = taRightJustify
      Caption = '( + )  Percepciones:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object NO_DEDUCCIlbl: TLabel
      Left = 28
      Top = 36
      Width = 103
      Height = 13
      Alignment = taRightJustify
      Caption = '( - ) Deducciones:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object NO_NETOlbl: TLabel
      Left = 18
      Top = 58
      Width = 114
      Height = 13
      Alignment = taRightJustify
      Caption = '( = ) Neto a Recibir:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object NO_NETO: TZetaDBTextBox
      Left = 134
      Top = 53
      Width = 121
      Height = 21
      AutoSize = False
      Caption = 'NO_NETO'
      Color = clWhite
      ParentColor = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'NO_NETO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object NO_DEDUCCI: TZetaDBTextBox
      Left = 134
      Top = 32
      Width = 121
      Height = 21
      AutoSize = False
      Caption = 'NO_DEDUCCI'
      Color = clWhite
      ParentColor = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'NO_DEDUCCI'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object NO_PERCEPC: TZetaDBTextBox
      Left = 134
      Top = 11
      Width = 121
      Height = 21
      AutoSize = False
      Caption = 'NO_PERCEPC'
      Color = clWhite
      ParentColor = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'NO_PERCEPC'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
  end
  inherited PanelInferior: TPanel
    Top = 134
    Height = 273
    object ZetaDBGrid: TZetaDBGrid
      Left = 1
      Top = 1
      Width = 696
      Height = 271
      Align = alClient
      DataSource = dsConceptos
      FixedColor = clInfoBk
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
      ReadOnly = True
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clBlack
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'CO_NUMERO'
          Title.Alignment = taCenter
          Title.Caption = '#'
          Width = 35
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CO_DESCRIP'
          Title.Caption = 'Concepto'
          Width = 280
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MO_REFEREN'
          Title.Caption = 'Referencia'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MO_PERCEPC'
          Title.Alignment = taRightJustify
          Title.Caption = 'Percepciones'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MO_DEDUCCI'
          Title.Alignment = taRightJustify
          Title.Caption = 'Deducciones'
          Width = 100
          Visible = True
        end>
    end
  end
  inherited DataSource: TDataSource
    Left = 290
    Top = 8
  end
  object dsConceptos: TDataSource
    Left = 322
    Top = 8
  end
end
