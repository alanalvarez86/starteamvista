unit FFormaAhorros;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, ExtCtrls, StdCtrls, DBCtrls, Mask, Db,
     FFormaBase,
     ZetaDBTextBox;

type
  TFormaAhorros = class(TFormaBase)
    AH_DESCRIPCION: TZetaDBTextBox;
    AH_DESCRIPCIONlbl: TLabel;
    DBNavigator: TDBNavigator;
    AH_TOTALlbl: TLabel;
    AH_TOTAL: TZetaDBTextBox;
    AH_SALDO_Ilbl: TLabel;
    AH_SALDO_I: TZetaDBTextBox;
    AH_ABONOSlbl: TLabel;
    AH_ABONOS: TZetaDBTextBox;
    AH_CARGOSlbl: TLabel;
    AH_CARGOS: TZetaDBTextBox;
    AH_SALDOlbl: TLabel;
    AH_SALDO: TZetaDBTextBox;
    AH_FECHAlbl: TLabel;
    AH_FECHA: TZetaDBTextBox;
    AH_NUMEROlbl: TLabel;
    AH_NUMERO: TZetaDBTextBox;
    AH_STATUSlbl: TLabel;
    AH_STATUS: TZetaDBTextBox;
    procedure FormShow(Sender: TObject);
    procedure DBNavigatorClick(Sender: TObject; Button: TNavigateBtn);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure SetControlsFull; override;
    procedure SetControlsEmpty; override;
  public
    { Public declarations }
    procedure Connect; override;
  end;

var
  FormaAhorros: TFormaAhorros;

implementation

uses DCliente,
     FFormaEmpleado;

{$R *.DFM}

procedure TFormaAhorros.FormShow(Sender: TObject);
begin
     inherited;
     SetTitulo( 'Ahorros' );
end;

procedure TFormaAhorros.Connect;
begin
     with dmCliente do
     begin
          AbreAhorros;
          Datasource.Dataset := cdsAhorros;
     end;
end;

procedure TFormaAhorros.DBNavigatorClick(Sender: TObject; Button: TNavigateBtn);
begin
     inherited;
     FormaEmpleado.TimerStart;
end;

procedure TFormaAhorros.SetControlsEmpty;
begin
     inherited SetControlsEmpty;
     ControlsHide( PanelSuperior );
     ControlsHide( PanelInferior );
     MessageShow( 'No Hay Ahorros' );
end;

procedure TFormaAhorros.SetControlsFull;
begin
     inherited SetControlsFull;
     ControlsShow( PanelSuperior );
     ControlsShow( PanelInferior );
     MessageClear;
end;

end.
