unit FEmpresas;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ExtCtrls, Grids, DBGrids, ComCtrls,
     ZBaseDlgModal;

type
  TEmpresas = class(TZetaDlgModal)
    StringGrid: TStringGrid;
    HeaderControl: THeaderControl;
    procedure FormShow(Sender: TObject);
    procedure StringGridGetEditMask(Sender: TObject; ACol, ARow: Integer;
      var Value: String);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Empresas: TEmpresas;

function EditEmpresas: Boolean;

implementation

uses DCliente;

{$R *.DFM}

const
     K_CODIGO = 0;
     K_NOMBRE = 1;
     K_DIGITO = 2;

function EditEmpresas: Boolean;
begin
     if dmCliente.CompanyLoad then
     begin
          Empresas := TEmpresas.Create( Application );
          try
             with Empresas do
             begin
                  ShowModal;
                  Result := ( ModalResult = mrOk );
             end;
          finally
                 FreeAndNil( Empresas );
          end;
     end
     else
         Result := False;
end;

{ ****** TEmpresas ****** }

procedure TEmpresas.FormShow(Sender: TObject);
var
   i: Integer;
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with StringGrid do
        begin
             with dmCliente.cdsCompany do
             begin
                  First;
                  RowCount := RecordCount;
                  i := 0;
                  while not Eof do
                  begin
                       Cells[ K_CODIGO, i ] := FieldByName( 'CM_CODIGO' ).AsString;
                       Cells[ K_NOMBRE, i ] := FieldByName( 'CM_NOMBRE' ).AsString;
                       Cells[ K_DIGITO, i ] := FieldByName( K_CAMPO_DIGITO ).AsString;
                       Inc( i );
                       Next;
                  end;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TEmpresas.StringGridGetEditMask(Sender: TObject; ACol, ARow: Integer; var Value: String);
begin
     inherited;
     if ( ACol = K_DIGITO ) then
     begin
          Value := 'A';
     end
     else
         Value := '';
end;

procedure TEmpresas.OKClick(Sender: TObject);
var
   i: Integer;
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with StringGrid do
        begin
             with dmCliente do
             begin
                  for i := 0 to ( RowCount - 1 ) do
                  begin
                       CompanySet( Cells[ K_CODIGO, i ], Cells[ K_DIGITO, i ] );
                  end;
                  CompanyUnLoad;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

end.
