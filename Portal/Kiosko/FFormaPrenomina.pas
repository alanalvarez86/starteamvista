unit FFormaPrenomina;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, Buttons, ExtCtrls, Grids, DBGrids, StdCtrls, Mask, DBCtrls,
     FFormaBasePeriodo,
     ZetaDBTextBox,
     ZetaDBGrid;

type
  TFormaPrenomina = class(TFormaBasePeriodo)
    dsAusencia: TDataSource;
    DiasGB: TGroupBox;
    NO_DIASlbl: TLabel;
    NO_DIAS_ASlbl: TLabel;
    NO_DIAS_INlbl: TLabel;
    NO_DIAS_FIlbl: TLabel;
    NO_DIAS_FJlbl: TLabel;
    NO_DIAS: TZetaDBTextBox;
    NO_DIAS_AS: TZetaDBTextBox;
    NO_DIAS_FI: TZetaDBTextBox;
    NO_DIAS_FJ: TZetaDBTextBox;
    NO_DIAS_IN: TZetaDBTextBox;
    HorasGB: TGroupBox;
    NO_HORASlbl: TLabel;
    NO_EXTRASlbl: TLabel;
    NO_DOBLESlbl: TLabel;
    NO_TRIPLESlbl: TLabel;
    NO_HORAS: TZetaDBTextBox;
    NO_EXTRAS: TZetaDBTextBox;
    NO_DOBLES: TZetaDBTextBox;
    NO_TRIPLES: TZetaDBTextBox;
    ZetaDBGrid: TZetaDBGrid;
  private
    { Private declarations }
    procedure SetDatasets;
  protected
    { Protected declarations }
    procedure CargarInit; override;
    procedure CargarAnterior; override;
    procedure CargarSiguiente; override;
    procedure SetControlsFull; override;
    procedure SetControlsEmpty; override;
  public
    { Public declarations }
  end;

var
  FormaPrenomina: TFormaPrenomina;

implementation

uses DCliente,
     ZetaCommonTools;

{$R *.DFM}

{ ********** TFormaNomina ********* }

procedure TFormaPrenomina.SetControlsEmpty;
begin
     inherited SetControlsEmpty;
     with Titulo do
     begin
          Caption := 'Sin Asistencia';
     end;
end;

procedure TFormaPrenomina.SetControlsFull;
begin
     inherited SetControlsFull;
     with Titulo do
     begin
          with Dataset do
          begin
               Caption := Format( 'Del %s Al %s', [ ZetaCommonTools.FechaCorta( FieldByName( 'PE_FEC_INI' ).AsDateTime ),
                                                    ZetaCommonTools.FechaCorta( FieldByName( 'PE_FEC_FIN' ).AsDateTime ) ] );
          end;
     end;
end;

procedure TFormaPrenomina.SetDatasets;
begin
     with dmCliente do
     begin
          Datasource.Dataset := cdsPrenomina;
          dsAusencia.Dataset := cdsAusencia;
     end;
end;

procedure TFormaPrenomina.CargarAnterior;
begin
     with dmCliente do
     begin
          AbrePrenominaAnterior( Year, Periodo );
     end;
     SetDatasets;
end;

procedure TFormaPrenomina.CargarInit;
begin
     with dmCliente do
     begin
          AbrePrenomina( Year );
     end;
     SetDatasets;
end;

procedure TFormaPrenomina.CargarSiguiente;
begin
     with dmCliente do
     begin
          AbrePrenominaSiguiente( Year, Periodo );
     end;
     SetDatasets;
end;

end.
