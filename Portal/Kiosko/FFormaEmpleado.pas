unit FFormaEmpleado;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ExtCtrls, Db, Mask, DBCtrls,
     Buttons, ToolWin, ComCtrls, TDMULTIP, TMultiP,
     FFormaBase,
     ZetaDBTextBox;

type
  TFormaEmpleado = class;
  TFormManager = class(TObject)
  private
    { Private declarations }
    FShell: TFormaEmpleado;
    FFormas: TList;
    FFormaActiva: TFormaBase;
    function GetFormas( Index: Integer ): TFormaBase;
    procedure Clear;
    procedure SetFormaActiva( Value: TFormaBase );
  public
    { Public declarations }
    constructor Create( Shell: TFormaEmpleado );
    destructor Destroy; override;
    property FormaActiva: TFormaBase read FFormaActiva write SetFormaActiva;
    property Formas[ Index: Integer ]: TFormaBase read GetFormas;
    function Count: Integer;
    function GetForm( InstanceClass: TFormaBaseClass ): TFormaBase;
  end;
  TFormaEmpleado = class(TForm)
    dsEmpleado: TDataSource;
    ToolBar: TToolBar;
    SalirBtn: TSpeedButton;
    VacacionesBtn: TSpeedButton;
    CursosBtn: TSpeedButton;
    PrenominaBtn: TSpeedButton;
    NominaBtn: TSpeedButton;
    AhorrosBtn: TSpeedButton;
    PersonalesBtn: TSpeedButton;
    PrestamosBtn: TSpeedButton;
    ComidasBtn: TSpeedButton;
    PanelBase: TPanel;
    PanelEmpresa: TPanel;
    Timer: TTimer;
    PanelIdentificacion: TPanel;
    CalendarioBtn: TSpeedButton;
    AsistenciaBtn: TSpeedButton;
    IdentificacionGB: TGroupBox;
    PRETTYNAMElbl: TLabel;
    CB_RFClbl: TLabel;
    CB_SEGSOClbl: TLabel;
    CB_CURPlbl: TLabel;
    CB_CURP: TZetaDBTextBox;
    CB_SEGSOC: TZetaDBTextBox;
    CB_RFC: TZetaDBTextBox;
    PRETTYNAME: TZetaDBTextBox;
    FotoGB: TGroupBox;
    GafeteGB: TGroupBox;
    Empleado: TEdit;
    PanelFoto: TPanel;
    IM_BLOB: TPDBMultiImage;
    ReportesBtn: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure ToolBarResize(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
    procedure dsEmpleadoDataChange(Sender: TObject; Field: TField);
    procedure PersonalesBtnClick(Sender: TObject);
    procedure VacacionesBtnClick(Sender: TObject);
    procedure CursosBtnClick(Sender: TObject);
    procedure PrenominaBtnClick(Sender: TObject);
    procedure NominaBtnClick(Sender: TObject);
    procedure AhorrosBtnClick(Sender: TObject);
    procedure PrestamosBtnClick(Sender: TObject);
    procedure ComidasBtnClick(Sender: TObject);
    procedure CalendarioBtnClick(Sender: TObject);
    procedure AsistenciaBtnClick(Sender: TObject);
    procedure SalirBtnClick(Sender: TObject);
    procedure ReportesBtnClick(Sender: TObject);
  private
    { Private declarations }
    FManager: TFormManager;
    FCodigoBarra: String;
    FCursor: TCursor;
    FEsShow: Boolean;
    procedure ActivaForma( ClaseForma: TFormaBaseClass );
    procedure BotonesEnable;
    procedure BotonesInit;
    procedure Clear;
    procedure DesactivaFormas;
    procedure CursorReset;
    procedure CursorHourglass;
    procedure Espere;
    procedure MuestraLetrero( const sLetrero: String );
    procedure MuestraPersonales;
    procedure Reset;
    procedure ResetAll;
    procedure Salir( Modo: TModalResult );
    procedure SelectEmpleado;
  public
    { Public declarations }
    property EsShow: Boolean read FEsShow write FEsShow;
    procedure Posiciona( const sGafete: String );
    procedure TimerStart;
    procedure TimerStop;
    procedure ShowEmpleado( const sGafete: String );
  end;


var
  FormaEmpleado: TFormaEmpleado;

implementation

uses FSalida,
     FKioscoRegistry,
     FLetrero,
     FFormaAhorros,
     FFormaAutoriza,
     FFormaCalendario,
     FFormaComidas,
     FFormaCursos,
     FFormaPersonales,
     FFormaPrenomina,
     FFormaNomina,
     FFormaPrestamos,
     FFormaVacaciones,
     FFormaReportes,
     {$ifdef DOS_CAPAS}
     DZetaServerProvider,
     ZetaSQLBroker,
     {$endif}
     ZetaCommonClasses,
     ZetaDialogo,
     DCliente;

{$R *.DFM}

{ ************** TFormaMgr ******************* }

constructor TFormManager.Create( Shell: TFormaEmpleado );
begin
     FShell := Shell;
     FFormas := TList.Create;
     FFormaActiva := nil;
end;

destructor TFormManager.Destroy;
begin
     Clear;
     FFormas.Free;
     inherited Destroy;
end;

function TFormManager.Count: Integer;
begin
     Result := FFormas.Count;
end;

function TFormManager.GetFormas(Index: Integer): TFormaBase;
begin
     Result := TFormaBase( FFormas.Items[ Index ] );
end;

function TFormManager.GetForm( InstanceClass: TFormaBaseClass ): TFormaBase;
var
   i: Integer;
begin
     Result := nil;
     with FFormas do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               if Formas[ i ].ClassNameIs( InstanceClass.ClassName ) then
               begin
                    Result := Formas[ i ];
                    Break;
               end;
          end;
     end;
end;

procedure TFormManager.SetFormaActiva( Value: TFormaBase );
begin
     with FFormas do
     begin
          if ( Value <> nil ) and ( IndexOf( Value ) < 0 ) then
             Add( Value );
     end;
     FFormaActiva := Value;
end;

procedure TFormManager.Clear;
var
   i: Integer;
begin
     for i := ( Count - 1 ) downto 0 do
     begin
          Formas[ i ].Free;
          FFormas.Delete( i );
     end;
     FFormaActiva := nil;
end;

{ ********** TFormaEmpleado ************* }

procedure TFormaEmpleado.FormCreate(Sender: TObject);
begin
     CursorHourglass;
     try
        FManager := TFormManager.Create( Self );
        {$ifdef DOS_CAPAS}
        DZetaServerProvider.InitAll;
        {$endif}
        dmCliente := TdmCliente.Create( Self );
     finally
            CursorReset;
     end;
end;

procedure TFormaEmpleado.FormDestroy(Sender: TObject);
begin
     FreeAndNil( dmCliente );
     {$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll(TRUE);
     DZetaServerProvider.FreeAll;
     {$endif}
     FreeAndNil( FManager );
end;

procedure TFormaEmpleado.FormShow(Sender: TObject);
const
     K_UN_SEGUNDO = 1000;
var
   i: Integer;
begin
     with PanelEmpresa do
     begin
          Caption := KioskoRegistry.NombreCompania;
          KioskoRegistry.NombreCompaniaFontLoad( Font );
          Color := KioskoRegistry.BarraNombreBkColor;
     end;
     Empleado.Color := KioskoRegistry.BarraNombreBkColor;
     PanelIdentificacion.Color := KioskoRegistry.PanelSuperiorBkColor;
     PanelBase.Color := KioskoRegistry.PanelIntermedioBkColor;
     Timer.Interval := KioskoRegistry.PantallasTimeout * K_UN_SEGUNDO;
     { TLabels }
     for i := ( Self.ComponentCount - 1 ) downto 0 do
     begin
          if ( Self.Components[ i ] is TZetaDBTextBox ) then
          begin
               TZetaDBTextBox( Self.Components[ i ] ).Caption := VACIO;
          end
          else
              if ( Self.Components[ i ] is TLabel ) then
              begin
                   with TLabel( Self.Components[ i ] ) do
                   begin
                        if ( Parent = PanelIdentificacion ) then
                           Font.Color := KioskoRegistry.PanelSuperiorFontColor
                        else
                            if ( Parent = PanelBase ) then
                               Font.Color := KioskoRegistry.PanelIntermedioFontColor;
                   end;
              end;
     end;
     { Botones }
     BotonesInit;
     { Primer Control }
     SelectEmpleado;
     TimerStart;
end;

procedure TFormaEmpleado.FormKeyPress(Sender: TObject; var Key: Char);
const
     K_ESC = 27;
     K_LF = 13;
begin
     case Ord( Key ) of
          K_ESC: Salir( mrCancel );
          K_LF:
          begin
               FCodigoBarra := Empleado.Text;
               if ( Length( FCodigoBarra ) > 0 ) then
               begin
                    Posiciona( FCodigoBarra );
                    FCodigoBarra := VACIO;
               end
               else
                   Salir( mrCancel );
           end;
     end;
end;

procedure TFormaEmpleado.dsEmpleadoDataChange(Sender: TObject; Field: TField);
const
     aImagen: array[ False..True ] of pChar = ( 'Fotograf�a ', 'No Hay Foto' );
var
   lNoHayFoto: Boolean;
begin
     BotonesEnable;
     lNoHayFoto := dsEmpleado.Dataset.FieldByName( 'IM_BLOB' ).IsNull;
     IM_BLOB.Visible := not lNoHayFoto;
     FotoGB.Caption := Format( ' %s ', [ aImagen[ lNoHayFoto ] ] );
end;

procedure TFormaEmpleado.CursorHourglass;
begin
     FCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
end;

procedure TFormaEmpleado.CursorReset;
begin
     Screen.Cursor :=FCursor;
end;

procedure TFormaEmpleado.SelectEmpleado;
begin
     with Empleado do
     begin
          SelectAll;
     end;
     ActiveControl := Empleado;
end;

procedure TFormaEmpleado.Clear;
begin
     FCodigoBarra := VACIO;
     Empleado.Text := VACIO;
     dsEmpleado.Dataset := nil;
     IM_BLOB.Visible := False;
end;

procedure TFormaEmpleado.Reset;
begin
     Clear;
     SelectEmpleado;
     TimerStop;
end;

procedure TFormaEmpleado.ResetAll;
begin
     Reset;
     DesactivaFormas;
     BotonesEnable;
end;

procedure TFormaEmpleado.BotonesInit;
begin
     PersonalesBtn.Visible := KioskoRegistry.VerPersonales;
     VacacionesBtn.Visible := KioskoRegistry.VerVacaciones;
     CursosBtn.Visible := KioskoRegistry.VerCursos;
     AsistenciaBtn.Visible := KioskoRegistry.VerAsistencia;
     CalendarioBtn.Visible := KioskoRegistry.VerCalendario;
     PrenominaBtn.Visible := KioskoRegistry.VerPrenomina;
     NominaBtn.Visible := KioskoRegistry.VerNomina;
     AhorrosBtn.Visible := KioskoRegistry.VerAhorros;
     PrestamosBtn.Visible := KioskoRegistry.VerPrestamos;
     ComidasBtn.Visible := KioskoRegistry.VerComidas;
     ReportesBtn.Visible := KioskoRegistry.VerReportes;
     BotonesEnable;
end;

procedure TFormaEmpleado.BotonesEnable;
var
   lConnected: Boolean;
begin
     lConnected := Assigned( dsEmpleado.Dataset );
     PersonalesBtn.Enabled := lConnected;
     VacacionesBtn.Enabled := lConnected;
     CursosBtn.Enabled := lConnected;
     AsistenciaBtn.Enabled := lConnected;
     CalendarioBtn.Enabled := lConnected;
     PrenominaBtn.Enabled := lConnected;
     NominaBtn.Enabled := lConnected;
     AhorrosBtn.Enabled := lConnected;
     PrestamosBtn.Enabled := lConnected;
     ComidasBtn.Enabled := lConnected;
     ReportesBtn.Enabled := lConnected;
end;

procedure TFormaEmpleado.ToolBarResize(Sender: TObject);
var
   i, iWidth, iBotones: Integer;
begin
     with ToolBar do
     begin
          iBotones := ControlCount;
          iWidth := ( Width div iBotones ) - 1;
          for i := 0 to ( iBotones - 2 ) do
          begin
               with Controls[ i ] as TSpeedButton do
               begin
                    Width := iWidth;
               end;
          end;
          with Controls[ iBotones - 1 ] as TSpeedButton do
          begin
               Width := ToolBar.Width - ( ( iBotones - 1 ) * ( iWidth - 1 ) );
          end;
     end;
end;

procedure TFormaEmpleado.ActivaForma( ClaseForma: TFormaBaseClass );
var
   Forma: TFormaBase;
begin
     Forma := FManager.GetForm( ClaseForma );
     if not Assigned( Forma ) then
     begin
          try
             Forma := ClaseForma.Create( Self ) as TFormaBase;
          except
                on Error: Exception do
                begin
                     ZetaDialogo.zError( '� Error Al Crear Forma !', '� Ventana No Pudo Ser Creada !', 0 );
                     Forma := nil;
                end;
          end;
     end;
     if Assigned( Forma ) then
     begin
          with Forma do
          begin
               if ( Parent = NIL ) then
               begin
                    Parent := PanelBase;
                    Align := alClient;
                    Prepara;
                    DoConnect;
                    Show;
               end
               else
               begin
                    Prepara;
                    DoConnect;
                    Visible := True;
                    BringToFront;
               end;
          end;
          FManager.FormaActiva := Forma;
          TimerStart;
     end;
end;

procedure TFormaEmpleado.DesactivaFormas;
var
   i: Integer;
begin
     with FManager do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               with Formas[ i ] do
               begin
                    SendToBack;
                    Visible := False;
               end;
          end;
          FormaActiva := nil;
     end;
end;

procedure TFormaEmpleado.TimerStart;
begin
     TimerStop;
     with Timer do
     begin
          Enabled := True;
     end;
end;

procedure TFormaEmpleado.TimerStop;
begin
     with Timer do
     begin
          Enabled := False;
     end;
end;

procedure TFormaEmpleado.TimerTimer(Sender: TObject);
begin
     ResetAll;
     if FEsShow then
        Salir( mrOK );
end;

procedure TFormaEmpleado.Espere;
const
    K_LETRERO_ESPERE = 'Leyendo Informaci�n: Espere un Momento...';
begin
     MuestraLetrero( K_LETRERO_ESPERE );
     Application.ProcessMessages;
end;

procedure TFormaEmpleado.ShowEmpleado( const sGafete: String );
var
   lLoaded: Boolean;
begin
     CursorHourglass;
     try
        {
        Show;
        Update;
        }
        Application.ProcessMessages;
        lLoaded := dmCliente.CompanyLoad;
     finally
            CursorReset;
     end;
     if lLoaded then
     begin
          Reset;
          Empleado.Text := sGafete;
          Posiciona( sGafete );
          {
          ShowModal;
          }
     end
     else
         ZetaDialogo.zError( '� Atenci�n !', 'No Hay Lista De Empresas', 0 );
end;

procedure TFormaEmpleado.Posiciona( const sGafete: String );
var
   iLength, iEmpleado: Integer;
   sDigito, sEmpleado, sCredencial, sMensaje: String;
   oCursor: TCursor;
begin
     CursorHourglass;
     try
        iLength := Length( sGafete );
        if ( iLength > 0 ) then
        begin
             if ( iLength > 2 ) then { Empresa + # Empleado + # Credencial = 3+ caracteres }
             begin
                  sEmpleado := Copy( sGafete, 2, ( iLength - 2 ) );
                  iEmpleado := StrToIntDef( sEmpleado, 0 );
                  if ( iEmpleado > 0 ) then
                  begin
                       sDigito := Copy( sGafete, 1, 1 );
                       sCredencial := Copy( sGafete, iLength, 1 );
                       Espere;
                       oCursor := Screen.Cursor;
                       Screen.Cursor := crHourglass;
                       try
                          with dmCliente do
                          begin
                               if PosicionaEmpleado( sDigito, sCredencial, iEmpleado, sMensaje ) then
                               begin
                                    dsEmpleado.Dataset := dmCliente.cdsEmpleado;
                                    case KioskoRegistry.DefaultDatos of
                                         0: PersonalesBtnClick( Self );
                                         1: VacacionesBtnClick( Self );
                                         2: CursosBtnClick( Self );
                                         3: PrenominaBtnClick(Self );
                                         4: AsistenciaBtnClick( Self );
                                         5: CalendarioBtnClick( Self );
                                         6: NominaBtnClick( Self );
                                         7: AhorrosBtnClick( Self );
                                         8: PrestamosBtnClick( Self );
                                         9: ComidasBtnClick( Self );
                                         10: ReportesBtnClick( self );
                                    end;
                                    SelectEmpleado;
                               end
                               else
                               begin
                                    MuestraLetrero( sMensaje );
                                    Reset;
                               end
                          end;
                       finally
                              Screen.Cursor := oCursor;
                       end;
                  end
                  else
                  begin
                       MuestraLetrero( Format( '# De Empleado Inv�lido ( %s )', [ sEmpleado ] ) );
                       Reset;
                  end;
             end
             else
             begin
                  MuestraLetrero( Format( '# De Gafete Inv�lido ( %s )', [ sGafete ] ) );
                  Reset;
             end;
        end;
     finally
            CursorReset;
     end;
end;

procedure TFormaEmpleado.MuestraLetrero( const sLetrero: String );
begin
     ActivaForma( TFormaLetrero );
     with FManager do
     begin
          if Assigned( FormaActiva ) then
             TFormaLetrero( FormaActiva ).Letrero := sLetrero;
     end;
end;

procedure TFormaEmpleado.MuestraPersonales;
begin
     ActivaForma( TFormaPersonales );
end;

procedure TFormaEmpleado.Salir( Modo: TModalResult );
begin
     if FEsShow or FSalida.OKSalida then
     begin
          {
          ModalResult := Modo;
          }
          ResetAll;
          Close;
     end;
end;

procedure TFormaEmpleado.PersonalesBtnClick(Sender: TObject);
begin
     MuestraPersonales;
end;

procedure TFormaEmpleado.VacacionesBtnClick(Sender: TObject);
begin
     Espere;
     ActivaForma( TFormaVacaciones );
end;

procedure TFormaEmpleado.CursosBtnClick(Sender: TObject);
begin
     Espere;
     ActivaForma( TFormaCursos );
end;

procedure TFormaEmpleado.PrenominaBtnClick(Sender: TObject);
begin
     Espere;
     ActivaForma( TFormaPreNomina );
end;

procedure TFormaEmpleado.CalendarioBtnClick(Sender: TObject);
begin
     Espere;
     ActivaForma( TFormaCalendario );
end;

procedure TFormaEmpleado.AsistenciaBtnClick(Sender: TObject);
begin
     Espere;
     ActivaForma( TFormaAutoriza );
end;

procedure TFormaEmpleado.NominaBtnClick(Sender: TObject);
begin
     Espere;
     ActivaForma( TFormaNomina );
end;

procedure TFormaEmpleado.AhorrosBtnClick(Sender: TObject);
begin
     Espere;
     ActivaForma( TFormaAhorros );
end;

procedure TFormaEmpleado.PrestamosBtnClick(Sender: TObject);
begin
     Espere;
     ActivaForma( TFormaPrestamos );
end;

procedure TFormaEmpleado.ComidasBtnClick(Sender: TObject);
begin
     Espere;
     ActivaForma( TFormaComidas );
end;

procedure TFormaEmpleado.ReportesBtnClick(Sender: TObject);
begin
     Espere;
     ActivaForma( TFormaReportes );
end;

procedure TFormaEmpleado.SalirBtnClick(Sender: TObject);
begin
     Salir( mrCancel );
end;

end.


