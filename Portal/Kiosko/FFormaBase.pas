unit FFormaBase;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,Forms, Dialogs,
     Stdctrls, ExtCtrls, Db, DBGrids,
     ZetaDBTextBox;

type
  TFormaBase = class(TForm)
    PanelSuperior: TPanel;
    PanelInferior: TPanel;
    DataSource: TDataSource;
    Titulo: TPanel;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FCursor: TCursor;
    FSize: Integer;
    function GetDataset: TDataset;
    function GetDatasetVacio: Boolean;
    procedure SetVisibleControls(Control: TWinControl; const lVisible: Boolean);
    procedure SizeReset;
    procedure SizeSet(const iValue: Integer);
  protected
    { Protected declarations }
    property Dataset: TDataset read GetDataset;
    property DatasetVacio: Boolean read GetDatasetVacio;
    function GetDayAndDate( const dValue: TDate ): String;
    procedure Connect; virtual;
    procedure ControlsHide(Control: TWinControl);
    procedure ControlsShow(Control: TWinControl);
    procedure CursorHourglass;
    procedure CursorReset;
    procedure MessageClear;
    procedure MessageShow(const sMensaje: String);
    procedure SetColorSubControles(Control: TWinControl; const iColor: TColor);
    procedure SetColorControl(Control: TWinControl; const iColor: TColor);
    procedure SetControls; virtual;
    procedure SetControlsEmpty; virtual;
    procedure SetControlsFull; virtual;
    procedure SetTitulo( const sMensaje: String );
    procedure TimerReset;
  public
    { Public declarations }
    procedure Prepara; dynamic;
    procedure DoConnect;
  end;
  TFormaBaseClass = class of TFormaBase;

const
     K_FONT_SIZE_LETRERO = 14;
var
  FormaBase: TFormaBase;

implementation

uses ZetaCommonClasses,
     ZetaCommonTools,
     FFormaEmpleado,
     FKioscoRegistry;

{$R *.DFM}

procedure TFormaBase.FormShow(Sender: TObject);
begin
     with KioskoRegistry do
     begin
          PanelSuperior.Color := PanelIntermedioBkColor;
          SetColorSubControles( PanelSuperior, PanelIntermedioFontColor );
          PanelInferior.Color := PanelInferiorBkColor;
          SetColorSubControles( PanelInferior, PanelInferiorFontColor );
     end;
     with PanelSuperior do
     begin
          Titulo.Color := Color;
          SetColorSubControles( Titulo, Color );
     end;
     SizeSet( PanelInferior.Font.Size );
     FCursor := Screen.Cursor;
end;

function TFormaBase.GetDataset: TDataset;
begin
     Result := Datasource.Dataset;
end;

function TFormaBase.GetDatasetVacio: Boolean;
begin
     Result := Assigned( Dataset ) and Dataset.IsEmpty;
end;

function TFormaBase.GetDayAndDate( const dValue: TDate ): String;
begin
     Result := Format( '%s %s', [ ZetaCommonTools.DiaSemana( dValue ), ZetaCommonTools.FechaCorta( dValue ) ] );
end;

procedure TFormaBase.SizeSet( const iValue: Integer );
begin
     with PanelInferior.Font do
     begin
          FSize := Size;
          Size := iValue;
     end;
end;

procedure TFormaBase.SizeReset;
begin
     with PanelInferior.Font do
     begin
          Size := FSize;
     end;
end;

procedure TFormaBase.SetControls;
begin
     if DatasetVacio or not Assigned( Dataset ) then
        SetControlsEmpty
     else
         SetControlsFull;
end;

procedure TFormaBase.SetControlsEmpty;
begin
end;

procedure TFormaBase.SetControlsFull;
begin
end;

procedure TFormaBase.SetVisibleControls( Control: TWinControl; const lVisible: Boolean );
var
   i: Integer;
begin
     with Control do
     begin
          for i := ( ControlCount - 1 ) downto 0 do
          begin
               Controls[ i ].Visible := lVisible;
          end;
     end;
end;

procedure TFormaBase.MessageShow( const sMensaje: String );
begin
     SizeSet( K_FONT_SIZE_LETRERO );
     with PanelInferior do
     begin
          Caption := sMensaje;
     end;
end;

procedure TFormaBase.MessageClear;
begin
     SizeReset;
     with PanelInferior do
     begin
          Caption := '';
     end;
end;

procedure TFormaBase.ControlsHide( Control: TWinControl );
begin
     SetVisibleControls( Control, False );
end;

procedure TFormaBase.ControlsShow( Control: TWinControl );
begin
     SetVisibleControls( Control, True );
end;

procedure TFormaBase.SetColorSubControles( Control: TWinControl; const iColor: TColor );
var
   i: Integer;
begin
     with Control do
     begin
          for i := ( ControlCount - 1 ) downto 0 do
          begin
               if ( Controls[ i ] is TLabel ) then
               begin
                    TLabel( Controls[ i ] ).Font.Color := iColor;
               end
               else
                   if ( Controls[ i ] is TDBGrid ) then
                   begin
                        with TDBGrid( Controls[ i ] ) do
                        begin
                             FixedColor := clInfoBk;
                             with TitleFont do
                             begin
                                  Color := clBlack;
                                  Style := [ fsBold ];
                             end;
                             with Font do
                             begin
                                  Color := clBlack;
                                  Style := [];
                             end;
                        end;
                   end;
          end;
     end;
end;

procedure TFormaBase.SetColorControl( Control: TWinControl; const iColor: TColor );
begin
     { Asigna color a los subcontroles }
     SetColorSubControles( Control, iColor );
     { Asigna color al Control }
     with Control do
     begin
          Font.Color:= iColor;
     end;
end;

procedure TFormaBase.SetTitulo( const sMensaje: String );
begin
     with Titulo do
     begin
          Caption := sMensaje;
     end;
end;

procedure TFormaBase.CursorHourglass;
begin
     FCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
end;

procedure TFormaBase.CursorReset;
begin
     Screen.Cursor :=FCursor;
end;

procedure TFormaBase.Prepara;
var
   i: Integer;
begin
     for i := ( Self.ComponentCount - 1 ) downto 0 do
     begin
          if ( Self.Components[ i ] is TZetaDBTextBox ) then
          begin
               TZetaDBTextBox( Self.Components[ i ] ).Caption := '';
          end;
     end;
end;

procedure TFormaBase.Connect;
begin
end;

procedure TFormaBase.DoConnect;
begin
     CursorHourglass;
     try
        Connect;
        SetControls;
     finally
            CursorReset;
     end;
end;

procedure TFormaBase.TimerReset;
begin
     FormaEmpleado.TimerStart;
end;

end.
