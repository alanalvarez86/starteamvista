unit FSalida;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ExtCtrls,
     ZBaseDlgModal;

type
  TSalida = class(TZetaDlgModal)
    ClaveLBL: TLabel;
    Clave: TEdit;
    procedure OKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FIntentos: Word;
  public
    { Public declarations }
  end;

function OKSalida: Boolean;

var
  Salida: TSalida;

implementation

uses ZetaCommonTools,
     FKioscoRegistry;

{$R *.DFM}

function OKSalida: Boolean;
begin
     if ZetaCommonTools.StrLleno( KioskoRegistry.PasswordSalida ) then
     begin
          Salida := TSalida.Create( Application );
          try
             with Salida do
             begin
                  ShowModal;
                  Result := ( ModalResult = mrOk );
             end;
          finally
                 FreeAndNil( Salida );
          end;
     end
     else
         Result := True;
end;


{ ********* TSalida ********* }

procedure TSalida.FormShow(Sender: TObject);
begin
     inherited;
     FIntentos := 0;
end;

procedure TSalida.OKClick(Sender: TObject);
const
     K_TOPE = 3;
begin
     inherited;
     if ( Clave.Text = KioskoRegistry.PasswordSalida ) then
     begin
          ModalResult := mrOk;
     end
     else
     begin
          Inc( FIntentos );
          if ( FIntentos >= K_TOPE ) then
          begin
               ModalResult := mrCancel;
          end
          else
          begin
               Beep;
               ActiveControl := Clave;
          end;
     end;
end;

end.
