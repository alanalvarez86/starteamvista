unit FFormaVacaciones;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Grids, DBGrids, StdCtrls, ExtCtrls, Db, Mask, DBCtrls,
     FFormaBase,
     ZetaDBTextBox,
     ZetaDBGrid;

type
  TFormaVacaciones = class(TFormaBase)
    dsVacaciones: TDataSource;
    DBGrid: TZetaDBGrid;
    PagoGB: TGroupBox;
    CB_DER_PAGlbl: TLabel;
    Pago3: TLabel;
    CB_PRO_PAGlbl: TLabel;
    Pago1: TLabel;
    CB_V_PAGOlbl: TLabel;
    Pago4: TLabel;
    CB_TOT_PAGlbl: TLabel;
    CB_DER_PAG: TZetaDBTextBox;
    CB_V_PAGO: TZetaDBTextBox;
    Pago2: TLabel;
    CB_SUB_PAGlbl: TLabel;
    CB_SUB_PAG: TZetaDBTextBox;
    CB_PRO_PAG: TZetaDBTextBox;
    CB_TOT_PAG: TZetaDBTextBox;
    GozoGB: TGroupBox;
    CB_DER_GOZlbl: TLabel;
    Gozo3: TLabel;
    CB_PRO_GOZlbl: TLabel;
    Gozo1: TLabel;
    CB_V_GOZOlbl: TLabel;
    Gozo4: TLabel;
    CB_TOT_GOZlbl: TLabel;
    CB_DER_GOZ: TZetaDBTextBox;
    CB_V_GOZO: TZetaDBTextBox;
    Gozo2: TLabel;
    CB_SUB_GOZlbl: TLabel;
    CB_SUB_GOZ: TZetaDBTextBox;
    CB_PRO_GOZ: TZetaDBTextBox;
    CB_TOT_GOZ: TZetaDBTextBox;
    GroupBox2: TGroupBox;
    CB_FEC_VAClbl: TLabel;
    CB_FEC_VAC: TZetaDBTextBox;
    CB_DER_FEClbl: TLabel;
    CB_DER_FEC: TZetaDBTextBox;
    CB_FEC_ANTlbl: TLabel;
    CB_FEC_ANT: TZetaDBTextBox;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Connect; override;
  end;

var
  FormaVacaciones: TFormaVacaciones;

implementation

uses DCliente;

{$R *.DFM}

procedure TFormaVacaciones.FormShow(Sender: TObject);
begin
     inherited;
     SetTitulo( 'Vacaciones' );
end;

procedure TFormaVacaciones.Connect;
begin
     with dmCliente do
     begin
          AbreVacaciones;
          Datasource.Dataset := cdsVacTotal;
          dsVacaciones.Dataset := cdsVacHistorial;
     end;
end;

end.
