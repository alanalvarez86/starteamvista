unit FKioskoRegistryEditor;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Spin, Buttons, ComCtrls, ExtCtrls,
     ZBaseDlgModal,
     FKioscoRegistry;

type
  TKioskoRegistryEditor = class(TZetaDlgModal)
    Configuraciones: TPageControl;
    TabConfiguracion: TTabSheet;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    SelecFont: TSpeedButton;
    TimeOutUnitLBL: TLabel;
    Password: TEdit;
    PasswordChk: TEdit;
    Timeout: TSpinEdit;
    GrupoConsultar: TGroupBox;
    Label7: TLabel;
    VerPersonales: TCheckBox;
    VerCursos: TCheckBox;
    VerPrenomina: TCheckBox;
    VerNomina: TCheckBox;
    VerAhorros: TCheckBox;
    VerPrestamos: TCheckBox;
    VerComidas: TCheckBox;
    DatosDefault: TComboBox;
    VerVacaciones: TCheckBox;
    VerAsistencia: TCheckBox;
    VerCalendario: TCheckBox;
    Secuencia: TSpinEdit;
    NombreCompania: TEdit;
    TabApariencia: TTabSheet;
    PanelBarraNombre: TPanel;
    PanelSuperior: TPanel;
    BotonSuperior: TBitBtn;
    PanelInferior: TPanel;
    BotonInferior: TBitBtn;
    PanelIntermedio: TPanel;
    BotonIntermedio: TBitBtn;
    DialogoColor: TColorDialog;
    DialogoFont: TFontDialog;
    VerReportes: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure PasswordChange(Sender: TObject);
    procedure PanelBarraNombreDblClick(Sender: TObject);
    procedure PanelSuperiorDblClick(Sender: TObject);
    procedure PanelIntermedioDblClick(Sender: TObject);
    procedure PanelInferiorDblClick(Sender: TObject);
    procedure BotonSuperiorClick(Sender: TObject);
    procedure SelecFontClick(Sender: TObject);
    procedure NombreCompaniaChange(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
    function GetRegistry: TZetaKioskoRegistry;
  public
    { Public declarations }
  end;

var
  KioskoRegistryEditor: TKioskoRegistryEditor;

implementation

uses ZetaDialogo;

{$R *.DFM}

procedure TKioskoRegistryEditor.FormCreate(Sender: TObject);
begin
     inherited;
     Tag := 0;
end;

procedure TKioskoRegistryEditor.FormShow(Sender: TObject);
begin
     inherited;
     with GetRegistry do
     begin
          Self.Password.Text := PasswordSalida;
          Self.PasswordChk.Text := Self.Password.Text;
          with Self.NombreCompania do
          begin
               Text := NombreCompania;
               NombreCompaniaFontLoad( Font );
          end;
          with Self.PanelBarraNombre do
          begin
               Caption := NombreCompania;
               NombreCompaniaFontLoad( Font );
               Color := BarraNombreBkColor;
          end;
          Self.DatosDefault.ItemIndex := DefaultDatos;
          Self.PanelSuperior.Color := PanelSuperiorBkColor;
          Self.PanelIntermedio.Color := PanelIntermedioBkColor;
          Self.PanelInferior.Color := PanelInferiorBkColor;
          Self.BotonSuperior.Font.Color := PanelSuperiorFontColor;
          Self.BotonIntermedio.Font.Color := PanelIntermedioFontColor;
          Self.BotonInferior.Font.Color := PanelInferiorFontColor;
          Self.Secuencia.Value := InicioSecuencia;
          Self.TimeOut.Value := PantallasTimeOut;
          Self.VerPersonales.Checked := VerPersonales;
          Self.VerVacaciones.Checked := VerVacaciones;
          Self.VerCursos.Checked := VerCursos;
          Self.VerPrenomina.Checked := VerPrenomina;
          Self.VerAsistencia.Checked := VerAsistencia;
          Self.VerCalendario.Checked := VerCalendario;
          Self.VerNomina.Checked := VerNomina;
          Self.VerAhorros.Checked := VerAhorros;
          Self.VerPrestamos.Checked := VerPrestamos;
          Self.VerComidas.Checked := VerComidas;
          self.VerReportes.Checked := VerReportes;
     end;
     Configuraciones.ActivePage := TabConfiguracion;
     Self.ActiveControl:= Password;
end;

procedure TKioskoRegistryEditor.FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
const
     LETRA_F = 70;
begin
     inherited;
     if ( ssCtrl in Shift ) then
     begin
          if ( Self.ActiveControl = NombreCompania ) and ( Key = LETRA_F ) then
          begin
               Key:= 0;
               SelecFontClick( Sender );
          end;
     end;
end;

function TKioskoRegistryEditor.GetRegistry: TZetaKioskoRegistry;
begin
     Result := FKioscoRegistry.KioskoRegistry;
end;

procedure TKioskoRegistryEditor.PasswordChange(Sender: TObject);
begin
     inherited;
     PasswordChk.Text := '';
end;

procedure TKioskoRegistryEditor.PanelBarraNombreDblClick(Sender: TObject);
begin
     inherited;
     with PanelBarraNombre do
     begin
          DialogoColor.Color := Color;
          if DialogoColor.Execute then
             Color := DialogoColor.Color;
     end;
end;

procedure TKioskoRegistryEditor.PanelSuperiorDblClick(Sender: TObject);
begin
     inherited;
     with PanelSuperior do
     begin
          DialogoColor.Color := Color;
          if DialogoColor.Execute then
             Color := DialogoColor.Color;
     end;
end;

procedure TKioskoRegistryEditor.PanelIntermedioDblClick(Sender: TObject);
begin
     inherited;
     with PanelIntermedio do
     begin
          DialogoColor.Color := Color;
          if DialogoColor.Execute then
             Color := DialogoColor.Color;
     end;
end;

procedure TKioskoRegistryEditor.PanelInferiorDblClick(Sender: TObject);
begin
     inherited;
     with PanelInferior do
     begin
          DialogoColor.Color := Color;
          if DialogoColor.Execute then
             Color := DialogoColor.Color;
     end;
end;

procedure TKioskoRegistryEditor.BotonSuperiorClick(Sender: TObject);
begin
     inherited;
     with TBitBtn( Sender ) do
     begin
          DialogoColor.Color := Font.Color;
          if DialogoColor.Execute then
             Font.Color := DialogoColor.Color;
     end;
end;

procedure TKioskoRegistryEditor.SelecFontClick(Sender: TObject);
begin
     inherited;
     with NombreCompania do
     begin
          DialogoFont.Font := Font;
          if DialogoFont.Execute then
          begin
             Font := DialogoFont.Font;
             PanelBarraNombre.Font := Font;
          end;
     end;
end;

procedure TKioskoRegistryEditor.NombreCompaniaChange(Sender: TObject);
begin
     inherited;
     PanelBarraNombre.Caption := NombreCompania.Text;
end;

procedure TKioskoRegistryEditor.OKClick(Sender: TObject);
begin
     inherited;
     if ( Password.Text = PasswordChk.Text ) then
     begin
          with GetRegistry do
          begin
               PasswordSalida := Self.Password.Text;
               NombreCompania := Self.NombreCompania.Text;
               NombreCompaniaFontUnLoad( Self.NombreCompania.Font );
               BarraNombreBkColor := Self.PanelBarraNombre.Color;
               PanelSuperiorBkColor := Self.PanelSuperior.Color;
               PanelIntermedioBkColor := Self.PanelIntermedio.Color;
               PanelInferiorBkColor := Self.PanelInferior.Color;
               PanelSuperiorFontColor := Self.BotonSuperior.Font.Color;
               PanelIntermedioFontColor := Self.BotonIntermedio.Font.Color;
               PanelInferiorFontColor := Self.BotonInferior.Font.Color;
               InicioSecuencia := Self.Secuencia.Value;
               PantallasTimeOut := Self.TimeOut.Value;
               VerPersonales := Self.VerPersonales.Checked;
               VerVacaciones := Self.VerVacaciones.Checked;
               VerCursos := Self.VerCursos.Checked;
               VerPrenomina := Self.VerPrenomina.Checked;
               VerAsistencia := Self.VerAsistencia.Checked;
               VerCalendario := Self.VerCalendario.Checked;
               VerNomina := Self.VerNomina.Checked;
               VerAhorros := Self.VerAhorros.Checked;
               VerPrestamos := Self.VerPrestamos.Checked;
               VerComidas := Self.VerComidas.Checked;
               VerReportes := Self.VerReportes.Checked;
               DefaultDatos := Self.DatosDefault.ItemIndex;
          end;
          ModalResult:= mrOk;
          Close;
     end
     else
     begin
          ZetaDialogo.zError( '� Atenci�n !', 'Password De Salida No Fu� Capturado Correctamente', 0 );
          Configuraciones.ActivePage := TabConfiguracion;
          ActiveControl := Password;
     end;
end;

end.
