unit FFormaBasePeriodo;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, Grids, DBGrids, ExtCtrls, ComCtrls, StdCtrls, Buttons,
     ZetaDBGrid,
     FFormaBase;

type
  TFormaBasePeriodo = class(TFormaBase)
    PanelPeriodo: TPanel;
    PreviousPeriod: TSpeedButton;
    NextPeriod: TSpeedButton;
    PanelYear: TPanel;
    YearAfter: TSpeedButton;
    YearBefore: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure YearBeforeClick(Sender: TObject);
    procedure PreviousPeriodClick(Sender: TObject);
    procedure NextPeriodClick(Sender: TObject);
    procedure YearAfterClick(Sender: TObject);
  private
    { Private declarations }
    FPeriodo: Word;
    FYear: Word;
    FYearInicial: Word;
    function GetPeriodoNombre: String;
    procedure NominaInit;
    procedure NominaAnterior;
    procedure NominaSiguiente;
    procedure PeriodoSiguiente;
    procedure PeriodoAnterior;
    procedure YearSiguiente;
    procedure YearAnterior;
  protected
    { Protected declarations }
    property Periodo: Word read FPeriodo;
    property Year: Word read FYear;
    procedure CargarInit; virtual; abstract;
    procedure CargarAnterior; virtual; abstract;
    procedure CargarSiguiente; virtual; abstract;
    procedure SetControls; override;
    procedure SetControlsFull; override;
    procedure SetControlsEmpty; override;
  public
    { Public declarations }
    procedure Prepara; override;
    procedure Connect; override;
  end;

var
  FormaBasePeriodo: TFormaBasePeriodo;

implementation

uses DCliente,
     ZetaCommonLists,
     ZetaCommonTools;

{$R *.DFM}

const
     K_PERIODO_FIRST = 1;
     K_PERIODO_LAST = 999;

procedure TFormaBasePeriodo.FormShow(Sender: TObject);
begin
     inherited;
     PanelPeriodo.Color := PanelSuperior.Color;
     PanelYear.Color := PanelSuperior.Color;
     Titulo.Color := PanelSuperior.Color;
end;

procedure TFormaBasePeriodo.Prepara;
var
   iMonth, iDia: Word;
begin
     inherited Prepara;
     DecodeDate( Date, FYear, iMonth, iDia );
     FYearInicial := FYear;
     FPeriodo := K_PERIODO_FIRST;
end;

procedure TFormaBasePeriodo.Connect;
begin
     CargarInit;
end;

function TFormaBasePeriodo.GetPeriodoNombre: String;
begin
     Result := ZetaCommonLists.ObtieneElemento( lfTipoNomina, Ord( dmCliente.TipoNomina ) );
end;

procedure TFormaBasePeriodo.SetControls;
const
     K_LIMIT_BEFORE = 50;
     K_LIMIT_AFTER = 1;
     K_PERIODO_CERO = 0;
begin
     if DatasetVacio or not Assigned( Dataset ) then
     begin
          FPeriodo := K_PERIODO_CERO;
          SetControlsEmpty;
     end
     else
     begin
          FPeriodo := Dataset.FieldByName( 'PE_NUMERO' ).AsInteger;
          SetControlsFull;
     end;
     PanelYear.Caption := IntToStr( FYear );
     YearBefore.Enabled := ( FYear > ( FYearInicial - K_LIMIT_BEFORE ) );
     YearAfter.Enabled := ( FYear < ( FYearInicial + K_LIMIT_AFTER ) );
     PreviousPeriod.Enabled := ( FPeriodo > K_PERIODO_FIRST );
     NextPeriod.Enabled := ( FPeriodo <= K_PERIODO_LAST );
end;

procedure TFormaBasePeriodo.SetControlsEmpty;
begin
     inherited SetControlsEmpty;
     with PanelPeriodo do
     begin
          Caption := Format( 'No Hay %s', [ GetPeriodoNombre, FPeriodo ] );
     end;
     ControlsHide( PanelInferior );
end;

procedure TFormaBasePeriodo.SetControlsFull;
begin
     inherited SetControlsFull;
     with PanelPeriodo do
     begin
          Caption := Format( '%s # %d', [ GetPeriodoNombre, FPeriodo ] );
     end;
     ControlsShow( PanelInferior );
end;

procedure TFormaBasePeriodo.NominaInit;
begin
     Connect;
     SetControls;
end;

procedure TFormaBasePeriodo.NominaAnterior;
begin
     CargarAnterior;
     SetControls;
end;

procedure TFormaBasePeriodo.NominaSiguiente;
begin
     CargarSiguiente;
     SetControls;
end;

procedure TFormaBasePeriodo.YearAnterior;
begin
     CursorHourglass;
     try
        Dec( FYear );
        NominaInit;
     finally
            CursorReset;
     end;
end;

procedure TFormaBasePeriodo.YearSiguiente;
begin
     CursorHourglass;
     try
        Inc( FYear );
        NominaInit;
     finally
            CursorReset;
     end;
end;

procedure TFormaBasePeriodo.PeriodoAnterior;
begin
     CursorHourglass;
     try
        NominaAnterior;
     finally
            CursorReset;
     end;
end;

procedure TFormaBasePeriodo.PeriodoSiguiente;
begin
     CursorHourglass;
     try
        NominaSiguiente;
     finally
            CursorReset;
     end;
end;

procedure TFormaBasePeriodo.YearBeforeClick(Sender: TObject);
begin
     inherited;
     YearAnterior;
     TimerReset;
end;

procedure TFormaBasePeriodo.PreviousPeriodClick(Sender: TObject);
begin
     inherited;
     PeriodoAnterior;
     TimerReset;
end;

procedure TFormaBasePeriodo.NextPeriodClick(Sender: TObject);
begin
     inherited;
     PeriodoSiguiente;
     TimerReset;
end;

procedure TFormaBasePeriodo.YearAfterClick(Sender: TObject);
begin
     inherited;
     YearSiguiente;
     TimerReset;
end;

end.
