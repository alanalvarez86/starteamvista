inherited KioskoRegistryEditor: TKioskoRegistryEditor
  Caption = 'Configurar Kiosco de Tress'
  ClientHeight = 409
  ClientWidth = 395
  OldCreateOrder = True
  OnCreate = FormCreate
  OnKeyUp = FormKeyUp
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 373
    Width = 395
    inherited OK: TBitBtn
      Left = 227
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 312
    end
  end
  object Configuraciones: TPageControl
    Left = 0
    Top = 0
    Width = 395
    Height = 373
    ActivePage = TabConfiguracion
    Align = alClient
    TabOrder = 1
    object TabConfiguracion: TTabSheet
      Caption = 'Configuraci�n'
      object Label2: TLabel
        Left = 29
        Top = 9
        Width = 77
        Height = 13
        Caption = 'Clave de Salida:'
      end
      object Label3: TLabel
        Left = 42
        Top = 31
        Width = 64
        Height = 13
        Caption = 'Confirmaci�n:'
      end
      object Label4: TLabel
        Left = 9
        Top = 53
        Width = 97
        Height = 13
        Caption = 'Inicio de Secuencia:'
      end
      object Label5: TLabel
        Left = 2
        Top = 76
        Width = 104
        Height = 13
        Caption = 'TimeOut de Pantallas:'
      end
      object Label6: TLabel
        Left = 7
        Top = 98
        Width = 99
        Height = 13
        Caption = 'Nombre de Empresa:'
      end
      object SelecFont: TSpeedButton
        Left = 344
        Top = 95
        Width = 25
        Height = 32
        Hint = 'Seleccionar Font del Nombre de Empresa [ Ctrl + F ]'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333333333333333333FF3FFFFFFF33FFF003000000033
          0000773777777733777703330033033330037FFF77F37F33377F700007330333
          3003777777337FFFF77F303003337000007337F77F3377777773303073333033
          003337F77F3337F377F337007333303300333777733337FF77F3330033333703
          07333377F333377F7733330733333300033333773FF3F3777F33333399393300
          033333337737FF777F333333339993307333333333777FF77333333333999930
          3333333333777737333333333333333333333333333333333333333333333333
          3333333333333333333333333333333333333333333333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = SelecFontClick
      end
      object TimeOutUnitLBL: TLabel
        Left = 162
        Top = 76
        Width = 48
        Height = 13
        Caption = 'Segundos'
      end
      object Password: TEdit
        Left = 109
        Top = 5
        Width = 106
        Height = 21
        PasswordChar = '*'
        TabOrder = 0
        Text = 'Edit1'
        OnChange = PasswordChange
      end
      object PasswordChk: TEdit
        Left = 109
        Top = 27
        Width = 106
        Height = 21
        PasswordChar = '*'
        TabOrder = 1
        Text = 'Edit1'
      end
      object Timeout: TSpinEdit
        Left = 109
        Top = 72
        Width = 49
        Height = 22
        MaxValue = 0
        MinValue = 0
        TabOrder = 3
        Value = 0
      end
      object GrupoConsultar: TGroupBox
        Left = 0
        Top = 129
        Width = 387
        Height = 216
        Align = alBottom
        Caption = ' Consultar: '
        TabOrder = 4
        object Label7: TLabel
          Left = 10
          Top = 194
          Width = 68
          Height = 13
          Alignment = taRightJustify
          Caption = 'Datos Default:'
        end
        object VerPersonales: TCheckBox
          Left = 81
          Top = 11
          Width = 105
          Height = 17
          Caption = 'Datos Personales'
          TabOrder = 0
        end
        object VerCursos: TCheckBox
          Left = 81
          Top = 43
          Width = 105
          Height = 17
          Caption = 'Cursos'
          TabOrder = 2
        end
        object VerPrenomina: TCheckBox
          Left = 81
          Top = 59
          Width = 105
          Height = 17
          Caption = 'Pren�mina'
          TabOrder = 3
        end
        object VerNomina: TCheckBox
          Left = 81
          Top = 108
          Width = 105
          Height = 17
          Caption = 'N�minas'
          TabOrder = 6
        end
        object VerAhorros: TCheckBox
          Left = 81
          Top = 124
          Width = 105
          Height = 17
          Caption = 'Ahorros'
          TabOrder = 7
        end
        object VerPrestamos: TCheckBox
          Left = 81
          Top = 141
          Width = 100
          Height = 17
          Caption = 'Prest�mos'
          TabOrder = 8
        end
        object VerComidas: TCheckBox
          Left = 81
          Top = 157
          Width = 100
          Height = 17
          Caption = 'Comidas'
          TabOrder = 9
        end
        object DatosDefault: TComboBox
          Left = 81
          Top = 191
          Width = 200
          Height = 21
          ItemHeight = 13
          TabOrder = 11
          Items.Strings = (
            'Datos Personales'
            'Vacaciones'
            'Cursos'
            'Asistencia'
            'Extras'
            'Calendario'
            'N�minas'
            'Ahorros'
            'Pr�stamos'
            'Comidas')
        end
        object VerVacaciones: TCheckBox
          Left = 81
          Top = 27
          Width = 105
          Height = 17
          Caption = 'Vacaciones'
          TabOrder = 1
        end
        object VerAsistencia: TCheckBox
          Left = 81
          Top = 75
          Width = 105
          Height = 17
          Caption = 'Asistencia'
          TabOrder = 4
        end
        object VerCalendario: TCheckBox
          Left = 81
          Top = 91
          Width = 105
          Height = 17
          Caption = 'Calendario'
          TabOrder = 5
        end
        object VerReportes: TCheckBox
          Left = 81
          Top = 173
          Width = 100
          Height = 17
          Caption = 'Reportes'
          TabOrder = 10
        end
      end
      object Secuencia: TSpinEdit
        Left = 109
        Top = 49
        Width = 49
        Height = 22
        MaxValue = 0
        MinValue = 0
        TabOrder = 2
        Value = 0
      end
      object NombreCompania: TEdit
        Left = 109
        Top = 95
        Width = 232
        Height = 33
        AutoSize = False
        Ctl3D = True
        ParentCtl3D = False
        TabOrder = 5
        OnChange = NombreCompaniaChange
      end
    end
    object TabApariencia: TTabSheet
      Caption = 'Apariencia'
      object PanelBarraNombre: TPanel
        Left = 0
        Top = 0
        Width = 387
        Height = 33
        Hint = 'Doble Click para Cambiar Color de Fondo'
        Align = alTop
        BevelOuter = bvLowered
        BorderStyle = bsSingle
        Caption = 'Nombre de Compa�ia'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnDblClick = PanelBarraNombreDblClick
      end
      object PanelSuperior: TPanel
        Left = 0
        Top = 33
        Width = 387
        Height = 72
        Hint = 'Doble Click para Cambiar Color de Fondo'
        Align = alTop
        BorderStyle = bsSingle
        Color = clPurple
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnDblClick = PanelSuperiorDblClick
        object BotonSuperior: TBitBtn
          Left = 120
          Top = 23
          Width = 133
          Height = 26
          Hint = 'Cambiar Font del Panel'
          Caption = 'Color del Font'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BotonSuperiorClick
        end
      end
      object PanelInferior: TPanel
        Left = 0
        Top = 169
        Width = 387
        Height = 160
        Hint = 'Doble Click para Cambiar Color de Fondo'
        Align = alClient
        BorderStyle = bsSingle
        Color = clPurple
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnDblClick = PanelInferiorDblClick
        object BotonInferior: TBitBtn
          Left = 120
          Top = 33
          Width = 133
          Height = 26
          Hint = 'Cambiar Font del Panel'
          Caption = 'Color del Font'
          TabOrder = 0
          OnClick = BotonSuperiorClick
        end
      end
      object PanelIntermedio: TPanel
        Left = 0
        Top = 105
        Width = 387
        Height = 64
        Hint = 'Doble Click para Cambiar Color de Fondo'
        Align = alTop
        BorderStyle = bsSingle
        Color = clPurple
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnDblClick = PanelIntermedioDblClick
        object BotonIntermedio: TBitBtn
          Left = 120
          Top = 19
          Width = 133
          Height = 26
          Hint = 'Cambiar Font del Panel'
          Caption = 'Color del Font'
          TabOrder = 0
          OnClick = BotonSuperiorClick
        end
      end
    end
  end
  object DialogoColor: TColorDialog
    Ctl3D = True
    Left = 240
    Top = 76
  end
  object DialogoFont: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    MinFontSize = 0
    MaxFontSize = 0
    Left = 272
    Top = 76
  end
end
