�
 TFORMAPRESTAMOS 0�  TPF0�TFormaPrestamosFormaPrestamosLeft� Top� Caption	Pr�stamosClientHeight�ClientWidth�PixelsPerInch`
TextHeight �TPanelPanelSuperiorWidth�HeightH TLabelPR_DESCRIPCIONlblLeft� TopWidthjHeight	AlignmenttaRightJustifyCaptionEstado de Cuenta:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelPR_REFERENlblLeftTop(WidthCHeight	AlignmenttaRightJustifyCaptionReferencia:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TZetaDBTextBoxPR_DESCRIPCIONLeft\TopWidth� HeightAutoSizeCaptionPR_DESCRIPCIONColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataFieldPR_DESCRIPCION
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox
PR_REFERENLeft\Top$Width� HeightAutoSizeCaption
PR_REFERENColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataField
PR_REFEREN
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TDBNavigatorDBNavigatorLeftKTopWidth� Height+
DataSource
DataSourceVisibleButtonsnbFirstnbPriornbNextnbLast TabOrder OnClickDBNavigatorClick   �TPanelPanelInferiorTophWidth�Height*TabOrder TLabelPR_MONTOlblLeft� TopJWidthrHeight	AlignmenttaRightJustifyCaptionMonto de Pr�stamo:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelPR_CARGOSlblLeft� Top� WidthaHeight	AlignmenttaRightJustifyCaption(+) Otros Cargos:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelPR_SALDOlblLeft� Top� Width`Height	AlignmenttaRightJustifyCaption(=) Saldo Actual:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelPR_FECHAlblLeft� TopWidth]Height	AlignmenttaRightJustifyCaptionFecha de Inicio:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelPR_NUMEROlblLeftTopWidthMHeight	AlignmenttaRightJustifyCaption# de Abonos:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelPR_STATUSlblLeft3Top� Width)Height	AlignmenttaRightJustifyCaptionStatus:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TZetaDBTextBoxPR_MONTOLeft\TopFWidthQHeightAutoSizeCaptionPR_MONTOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataFieldPR_MONTO
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox	PR_CARGOSLeft\Top� WidthQHeightAutoSizeCaption	PR_CARGOSColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataField	PR_CARGOS
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBoxPR_SALDOLeft\Top� WidthQHeightAutoSizeCaptionPR_SALDOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataFieldPR_SALDO
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBoxPR_FECHALeft\TopWidthQHeightAutoSizeCaptionPR_FECHAColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataFieldPR_FECHA
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox	PR_NUMEROLeft\TopWidthQHeightAutoSizeCaption	PR_NUMEROColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataField	PR_NUMERO
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox	PR_STATUSLeft\Top� WidthQHeightAutoSizeCaption	PR_STATUSColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataField	PR_STATUS
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TLabelPR_SALDO_IlblLeft� Top`Width� Height	AlignmenttaRightJustifyCaption( - ) Abonos A�o Anterior:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TZetaDBTextBox
PR_SALDO_ILeft\Top\WidthQHeightAutoSizeCaption
PR_SALDO_IColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataField
PR_SALDO_I
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TLabelPR_TOTALlblLeft� TopvWidth� Height	AlignmenttaRightJustifyCaption( - ) Abonos En el A�o:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TZetaDBTextBoxPR_TOTALLeft\ToprWidthQHeightAutoSizeCaptionPR_TOTALColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataFieldPR_TOTAL
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox	PR_ABONOSLeft\Top� WidthQHeightAutoSizeCaption	PR_ABONOSColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataField	PR_ABONOS
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TLabelPR_ABONOSlblLeft� Top� WidthiHeight	AlignmenttaRightJustifyCaption( - ) Otros Abonos:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont   �TPanelTituloWidth�TabOrder   