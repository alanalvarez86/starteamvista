inherited FormaPrenomina: TFormaPrenomina
  Caption = 'FormaPrenomina'
  ClientHeight = 407
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelSuperior: TPanel
    Height = 94
    object DiasGB: TGroupBox
      Left = 1
      Top = 1
      Width = 320
      Height = 92
      Align = alLeft
      Caption = ' D�as: '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      object NO_DIASlbl: TLabel
        Left = 24
        Top = 16
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ordinarios:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object NO_DIAS_ASlbl: TLabel
        Left = 23
        Top = 39
        Width = 63
        Height = 13
        Alignment = taRightJustify
        Caption = 'Asistencia:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object NO_DIAS_INlbl: TLabel
        Left = 11
        Top = 62
        Width = 75
        Height = 13
        Alignment = taRightJustify
        Caption = 'Incapacidad:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object NO_DIAS_FIlbl: TLabel
        Left = 145
        Top = 16
        Width = 118
        Height = 13
        Alignment = taRightJustify
        Caption = 'Faltas Injustificadas:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object NO_DIAS_FJlbl: TLabel
        Left = 153
        Top = 40
        Width = 110
        Height = 13
        Alignment = taRightJustify
        Caption = 'Faltas Justificadas:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object NO_DIAS: TZetaDBTextBox
        Left = 88
        Top = 12
        Width = 50
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_DIAS_AS: TZetaDBTextBox
        Left = 88
        Top = 35
        Width = 50
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_AS'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_AS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_DIAS_FI: TZetaDBTextBox
        Left = 265
        Top = 12
        Width = 50
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_FI'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_FI'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_DIAS_FJ: TZetaDBTextBox
        Left = 265
        Top = 36
        Width = 50
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_FJ'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_FJ'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_DIAS_IN: TZetaDBTextBox
        Left = 88
        Top = 58
        Width = 50
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_IN'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_IN'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
    object HorasGB: TGroupBox
      Left = 321
      Top = 1
      Width = 376
      Height = 92
      Align = alClient
      Caption = ' Horas: '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      object NO_HORASlbl: TLabel
        Left = 40
        Top = 16
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ordinarias:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object NO_EXTRASlbl: TLabel
        Left = 185
        Top = 15
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'Extras:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object NO_DOBLESlbl: TLabel
        Left = 181
        Top = 38
        Width = 44
        Height = 13
        Alignment = taRightJustify
        Caption = 'Dobles:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object NO_TRIPLESlbl: TLabel
        Left = 182
        Top = 60
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Triples:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object NO_HORAS: TZetaDBTextBox
        Left = 104
        Top = 12
        Width = 50
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_HORAS'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_HORAS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_EXTRAS: TZetaDBTextBox
        Left = 227
        Top = 11
        Width = 50
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_EXTRAS'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_EXTRAS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_DOBLES: TZetaDBTextBox
        Left = 227
        Top = 34
        Width = 50
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DOBLES'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DOBLES'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_TRIPLES: TZetaDBTextBox
        Left = 227
        Top = 56
        Width = 50
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_TRIPLES'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_TRIPLES'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
  end
  inherited PanelInferior: TPanel
    Top = 142
    Height = 265
    object ZetaDBGrid: TZetaDBGrid
      Left = 1
      Top = 1
      Width = 696
      Height = 263
      Align = alClient
      DataSource = dsAusencia
      FixedColor = clInfoBk
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clBlack
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'AU_FECHA'
          Title.Caption = 'Fecha'
          Width = 128
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'AU_HORAS'
          Title.Alignment = taRightJustify
          Title.Caption = 'Ordinarias'
          Width = 66
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'AU_DOBLES'
          Title.Alignment = taRightJustify
          Title.Caption = 'Dobles'
          Width = 62
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'AU_TRIPLES'
          Title.Alignment = taRightJustify
          Title.Caption = 'Triples'
          Width = 60
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'AU_TARDES'
          Title.Alignment = taRightJustify
          Title.Caption = 'Tardes'
          Width = 59
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'AU_PER_CG'
          Title.Alignment = taRightJustify
          Title.Caption = 'Permisos C/G'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AU_PER_SG'
          Title.Alignment = taRightJustify
          Title.Caption = 'Permiso S/G'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AU_DES_TRA'
          Title.Alignment = taRightJustify
          Title.Caption = 'Desc Trab'
          Width = 76
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TIPODIA'
          Title.Caption = 'Tipo'
          Width = 75
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'INCIDE'
          Title.Caption = 'Incidencia'
          Width = 85
          Visible = True
        end>
    end
  end
  inherited DataSource: TDataSource
    Left = 176
    Top = 112
  end
  object dsAusencia: TDataSource
    Left = 234
    Top = 112
  end
end
