�
 TFORMACALENDARIO 0�  TPF0�TFormaCalendarioFormaCalendarioLeft� TopwCaptionCalendario de AsistenciaClientWidth PixelsPerInch`
TextHeight �TPanelPanelSuperiorTop0Width Height   �TPanelPanelInferiorTop0Width Height} TZetaDBGrid
ZetaDBGridLeftTopWidthHeight{AlignalClient
DataSource
DataSource
FixedColorclInfoBkFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style OptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit 
ParentFontReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclGreenTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.StylefsBold ColumnsExpanded	FieldNameAU_FECHATitle.CaptionFechaWidthDVisible	 Expanded	FieldName	AU_STATUSTitle.CaptionD�aWidth=Visible	 Expanded	FieldName
AU_TIPODIATitle.CaptionTipoWidthTVisible	 Expanded	FieldName
AU_HORASCKTitle.AlignmenttaRightJustifyTitle.Caption
OrdinariasWidthBVisible	 Expanded	FieldNameAU_HORASTitle.Caption	Por PagarWidthAVisible	 Expanded	FieldName	AU_EXTRASTitle.AlignmenttaRightJustifyTitle.CaptionExtrasWidth,Visible	 Expanded	FieldName	AU_PER_CGTitle.AlignmenttaRightJustifyTitle.CaptionPermiso C/GWidthNVisible	 Expanded	FieldName	AU_PER_SGTitle.AlignmenttaRightJustifyTitle.CaptionPermiso S/GWidthLVisible	 Expanded	FieldName
AU_DES_TRATitle.AlignmenttaRightJustifyTitle.CaptionDescansoWidthDVisible	 Expanded	FieldName	AU_TARDESTitle.AlignmenttaRightJustifyTitle.CaptionTardesWidth0Visible	 Expanded	FieldNameAU_TIPOTitle.Caption
IncidenciaWidthFVisible	     �TPanelTituloWidth Height0CaptionCalendario de Asistencia TPanelPanelMesLeftTopWidth� Height,AlignalLeft
BevelOuterbvNoneCaption	NoviembreColorclTealFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  TSpeedButtonMonthBeforeLeftTopWidth!Height*HintMes Anterior
Glyph.Data
�   �   BM�       v   (               h                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� wwwwwwp wwwwwp wwpwwp wwwwp wpf  p wfffp pffffp wfffp wpf  p wwwwp wwpwwp wwwwwp wwwwwwp ParentShowHintShowHint	OnClickMonthBeforeClick  TSpeedButton
MonthAfterLeft� TopWidth!Height*HintMes SiguienteAnchorsakTopakRight 
Glyph.Data
�   �   BM�       v   (               h                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� wwwwwwp wwwwwp www wwp wwwwp w  `wp wfffp wfff`p wfffp w  `wp wwwwp www wwp wwwwwp wwwwwwp ParentShowHintShowHint	OnClickMonthAfterClick   TPanel	PanelYearLeft�TopWidth� Height,AlignalRight
BevelOuterbvNoneCaption2002ColorclTealFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder TSpeedButton	YearAfterLeftbTopWidth!Height)HintA�o SiguienteAnchorsakTopakRight 
Glyph.Data
�   �   BM�       v   (               h                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� wwwwwwp wwpwwp ww pwp ww `wp   `fp fff`p fff`f  fff`p   `fp ww `wp ww pwp wwpwwp wwwwwwp ParentShowHintShowHint	OnClickYearAfterClick  TSpeedButton
YearBeforeLeftTopWidth!Height)HintA�o Anterior
Glyph.Data
�   �   BM�       v   (               h   �  �               �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� wwwwwwp wwpwwp ww pwp wp`wp w`f    pffff  `ffff  pffff  w`f    wp`wp ww pwp wwpwwp wwwwwwp ParentShowHintShowHint	OnClickYearBeforeClick     