�
 TFORMAPERSONALES 0�.  TPF0�TFormaPersonalesFormaPersonalesLeftTop� CaptionFormaPersonalesClientHeight,ClientWidth�
Font.ColorclBlackPixelsPerInch`
TextHeight �TPanelPanelSuperiorWidth�Height 
Font.ColorclBlackFont.Height�
Font.StylefsBold 
ParentFont  �TPanelPanelInferiorTop Width�Height TLabelPU_DESCRIPlblLeft4Top� Width,Height	AlignmenttaRightJustifyCaptionPuesto:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelTB_ELEMENTlblLeftTop� WidthMHeight	AlignmenttaRightJustifyCaptionClasificaci�n:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelTU_DESCRIPlblLeft:Top� Width&Height	AlignmenttaRightJustifyCaptionTurno:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelCB_CALLElblLeft?TopWidth!Height	AlignmenttaRightJustifyCaptionCalle:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelCB_COLONIAlblLeft1TopWidth/Height	AlignmenttaRightJustifyCaptionColonia:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelCB_CODPOSTlblLeftTop1WidthSHeight	AlignmenttaRightJustifyCaptionC�digo Postal:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabel	CB_TELlblLeft)TopEWidth7Height	AlignmenttaRightJustifyCaption	Tel�fono:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelCB_FEC_NAClblLeftTopZWidthSHeight	AlignmenttaRightJustifyCaptionFecha Nacim.:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelCB_LUG_NAClblLeftTopnWidthMHeight	AlignmenttaRightJustifyCaptionOriginario de:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelCB_NIVEL1lblLefthTopWidthPHeight	AlignmenttaRightJustifyCaptionCB_NIVEL1lblFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelCB_NIVEL2lblLefthTopWidthPHeight	AlignmenttaRightJustifyCaptionCB_NIVEL2lblFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelCB_NIVEL3lblLefthTop0WidthPHeight	AlignmenttaRightJustifyCaptionCB_NIVEL3lblFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelCB_NIVEL4lblLefthTopDWidthPHeight	AlignmenttaRightJustifyCaptionCB_NIVEL4lblFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelCB_NIVEL5lblLefthTopXWidthPHeight	AlignmenttaRightJustifyCaptionCB_NIVEL5lblFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelCB_NIVEL6lblLefthTopmWidthPHeight	AlignmenttaRightJustifyCaptionCB_NIVEL6lblFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelCB_NIVEL7lblLefthTop� WidthPHeight	AlignmenttaRightJustifyCaptionCB_NIVEL7lblFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelCB_NIVEL8lblLefthTop� WidthPHeight	AlignmenttaRightJustifyCaptionCB_NIVEL8lblFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelCB_NIVEL9lblLefthTop� WidthPHeight	AlignmenttaRightJustifyCaptionCB_NIVEL9lblFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelCB_ANTIGUOlblLeftTop� WidthEHeight	AlignmenttaRightJustifyCaptionAntig�edad:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelCB_VENCElblLeftTop� Width]Height	AlignmenttaRightJustifyCaptionContrato Vence:Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TZetaDBTextBox
PU_DESCRIPLeftbTop� Width� HeightAutoSizeCaption
PU_DESCRIPColorclWhiteParentColorShowAccelCharBrush.Color	clBtnFaceBorder		DataField
PU_DESCRIP
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBoxCB_CALLELeftbTopWidth� HeightAutoSizeCaptionCB_CALLEColorclWhiteParentColorShowAccelCharBrush.Color	clBtnFaceBorder		DataFieldCB_CALLE
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox
CB_COLONIALeftbTopWidth� HeightAutoSizeCaption
CB_COLONIAColorclWhiteParentColorShowAccelCharBrush.Color	clBtnFaceBorder		DataField
CB_COLONIA
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox
CB_CODPOSTLeftbTop-Width� HeightAutoSizeCaption
CB_CODPOSTColorclWhiteParentColorShowAccelCharBrush.Color	clBtnFaceBorder		DataField
CB_CODPOST
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox
CB_FEC_NACLeftbTopVWidth� HeightAutoSizeCaption
CB_FEC_NACColorclWhiteParentColorShowAccelCharBrush.Color	clBtnFaceBorder		DataField
CB_FEC_NAC
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox
CB_LUG_NACLeftbTopjWidth� HeightAutoSizeCaption
CB_LUG_NACColorclWhiteParentColorShowAccelCharBrush.Color	clBtnFaceBorder		DataField
CB_LUG_NAC
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBoxCB_TELLeftbTopAWidth� HeightAutoSizeCaptionCB_TELColorclWhiteParentColorShowAccelCharBrush.Color	clBtnFaceBorder		DataFieldCB_TEL
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox
TB_ELEMENTLeftbTop� Width� HeightAutoSizeCaption
TB_ELEMENTColorclWhiteParentColorShowAccelCharBrush.Color	clBtnFaceBorder		DataField
TB_ELEMENT
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox
TU_DESCRIPLeftbTop� Width� HeightAutoSizeCaption
TU_DESCRIPColorclWhiteParentColorShowAccelCharBrush.Color	clBtnFaceBorder		DataField
TU_DESCRIP
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBoxTB_ELEMENT1Left�TopWidth� HeightAutoSizeCaptionTB_ELEMENT1ColorclWhiteParentColorShowAccelCharBrush.Color	clBtnFaceBorder		DataFieldTB_ELEMENT1
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBoxTB_ELEMENT4Left�Top@Width� HeightAutoSizeCaptionTB_ELEMENT4ColorclWhiteParentColorShowAccelCharBrush.Color	clBtnFaceBorder		DataFieldTB_ELEMENT4
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBoxTB_ELEMENT5Left�TopUWidth� HeightAutoSizeCaptionTB_ELEMENT5ColorclWhiteParentColorShowAccelCharBrush.Color	clBtnFaceBorder		DataFieldTB_ELEMENT5
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBoxTB_ELEMENT6Left�TopiWidth� HeightAutoSizeCaptionTB_ELEMENT6ColorclWhiteParentColorShowAccelCharBrush.Color	clBtnFaceBorder		DataFieldTB_ELEMENT6
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBoxTB_ELEMENT7Left�Top}Width� HeightAutoSizeCaptionTB_ELEMENT7ColorclWhiteParentColorShowAccelCharBrush.Color	clBtnFaceBorder		DataFieldTB_ELEMENT7
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBoxTB_ELEMENT8Left�Top� Width� HeightAutoSizeCaptionTB_ELEMENT8ColorclWhiteParentColorShowAccelCharBrush.Color	clBtnFaceBorder		DataFieldTB_ELEMENT8
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBoxTB_ELEMENT9Left�Top� Width� HeightAutoSizeCaptionTB_ELEMENT9ColorclWhiteParentColorShowAccelCharBrush.Color	clBtnFaceBorder		DataFieldTB_ELEMENT9
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBoxTB_ELEMENT2Left�TopWidth� HeightAutoSizeCaptionTB_ELEMENT2ColorclWhiteParentColorShowAccelCharBrush.Color	clBtnFaceBorder		DataFieldTB_ELEMENT2
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox	CB_NIVEL1Left�TopWidthAHeightAutoSizeCaption	CB_NIVEL1ColorclWhiteParentColorShowAccelCharBrush.Color	clBtnFaceBorder		DataField	CB_NIVEL1
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox	CB_NIVEL2Left�TopWidthAHeightAutoSizeCaption	CB_NIVEL2ColorclWhiteParentColorShowAccelCharBrush.Color	clBtnFaceBorder		DataField	CB_NIVEL2
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox	CB_NIVEL3Left�Top,WidthAHeightAutoSizeCaption	CB_NIVEL3ColorclWhiteParentColorShowAccelCharBrush.Color	clBtnFaceBorder		DataField	CB_NIVEL3
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox	CB_NIVEL4Left�Top@WidthAHeightAutoSizeCaption	CB_NIVEL4ColorclWhiteParentColorShowAccelCharBrush.Color	clBtnFaceBorder		DataField	CB_NIVEL4
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox	CB_NIVEL5Left�TopUWidthAHeightAutoSizeCaption	CB_NIVEL5ColorclWhiteParentColorShowAccelCharBrush.Color	clBtnFaceBorder		DataField	CB_NIVEL5
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox	CB_NIVEL6Left�TopiWidthAHeightAutoSizeCaption	CB_NIVEL6ColorclWhiteParentColorShowAccelCharBrush.Color	clBtnFaceBorder		DataField	CB_NIVEL6
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox	CB_NIVEL7Left�Top}WidthAHeightAutoSizeCaption	CB_NIVEL7ColorclWhiteParentColorShowAccelCharBrush.Color	clBtnFaceBorder		DataField	CB_NIVEL7
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox	CB_NIVEL8Left�Top� WidthAHeightAutoSizeCaption	CB_NIVEL8ColorclWhiteParentColorShowAccelCharBrush.Color	clBtnFaceBorder		DataField	CB_NIVEL8
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox	CB_NIVEL9Left�Top� WidthAHeightAutoSizeCaption	CB_NIVEL9ColorclWhiteParentColorShowAccelCharBrush.Color	clBtnFaceBorder		DataField	CB_NIVEL9
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBoxTB_ELEMENT3Left�Top,Width� HeightAutoSizeCaptionTB_ELEMENT3ColorclWhiteParentColorShowAccelCharBrush.Color	clBtnFaceBorder		DataFieldTB_ELEMENT3
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox
CB_ANTIGUOLeftbTop� Width� HeightAutoSizeCaption
CB_ANTIGUOColorclWhiteParentColorShowAccelCharBrush.Color	clBtnFaceBorder		DataField
CB_ANTIGUO
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBoxCB_VENCELeftbTop� Width� HeightAutoSizeCaptionCB_VENCEColorclWhiteParentColorShowAccelCharBrush.Color	clBtnFaceBorder		DataFieldCB_VENCE
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m   �TPanelTituloWidth�
Font.ColorclBlackFont.Height�   