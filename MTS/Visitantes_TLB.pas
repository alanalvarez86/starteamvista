unit Visitantes_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 12-02-2016 4:17:43 PM from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3win_30_Vsn_2016\MTS\Visitantes.tlb (1)
// LIBID: {2828EF25-8000-499D-A1B3-0443769CE1A1}
// LCID: 0
// Helpfile: 
// HelpString: Visitantes Library
// DepndLst: 
//   (1) v1.0 Midas, (C:\Program Files (x86)\Embarcadero\RAD Studio\12.0\bin64\midas.dll)
//   (2) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, Midas, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  VisitantesMajorVersion = 1;
  VisitantesMinorVersion = 0;

  LIBID_Visitantes: TGUID = '{2828EF25-8000-499D-A1B3-0443769CE1A1}';

  IID_IdmServerVisitantes: TGUID = '{F05B37A4-AC2E-452C-8E24-C13C4B6A3BEE}';
  CLASS_dmServerVisitantes: TGUID = '{10423A0C-77E0-4BE1-9636-69FB0BE0C1BA}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerVisitantes = interface;
  IdmServerVisitantesDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerVisitantes = IdmServerVisitantes;


// *********************************************************************//
// Interface: IdmServerVisitantes
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {F05B37A4-AC2E-452C-8E24-C13C4B6A3BEE}
// *********************************************************************//
  IdmServerVisitantes = interface(IAppServer)
    ['{F05B37A4-AC2E-452C-8E24-C13C4B6A3BEE}']
    function GetCatalogo(Empresa: OleVariant; Catalogo: Integer): OleVariant; safecall;
    function GrabaCatalogo(Empresa: OleVariant; oDelta: OleVariant; Catalogo: Integer; 
                           out ErrorCount: Integer): OleVariant; safecall;
    function GetCondiciones(Empresa: OleVariant): OleVariant; safecall;
    function GetCitas(Empresa: OleVariant; FechaInicio: TDateTime; FechaFin: TDateTime; 
                      const sFiltros: WideString; const HoraInicio: WideString; 
                      const HoraFin: WideString): OleVariant; safecall;
    function BuscaAnfitrion(Empresa: OleVariant; oParams: OleVariant): OleVariant; safecall;
    function BuscaVisitante(Empresa: OleVariant; oParams: OleVariant): OleVariant; safecall;
    function GrabaCitas(Empresa: OleVariant; oDelta: OleVariant; var ErrorCount: Integer; 
                        var iCita: Integer): OleVariant; safecall;
    function CorteSiguiente(Empresa: OleVariant; const Caseta: WideString; Usuario: Integer; 
                            Corte: Integer; out Datos: OleVariant): OleVariant; safecall;
    function CorteAnterior(Empresa: OleVariant; const Caseta: WideString; Usuario: Integer; 
                           Corte: Integer; out Datos: OleVariant): OleVariant; safecall;
    function CorteActivo(Empresa: OleVariant; Corte: Integer; out Datos: OleVariant): OleVariant; safecall;
    function GetCortes(Empresa: OleVariant; FechaInicio: TDateTime; FechaFin: TDateTime; 
                       const sFiltros: WideString; iStatus: Integer): OleVariant; safecall;
    function GetLibroActivo(Empresa: OleVariant; Corte: Integer): OleVariant; safecall;
    function GetLibros(Empresa: OleVariant; FechaInicio: TDateTime; FechaFin: TDateTime; 
                       const sFiltros: WideString): OleVariant; safecall;
    function GetLibroSalidas(Empresa: OleVariant): OleVariant; safecall;
    function GrabaCorte(Empresa: OleVariant; oDelta: OleVariant; out iCorte: Integer; 
                        out ErrorCount: Integer): OleVariant; safecall;
    function GetAlertas(Empresa: OleVariant; const Caseta: WideString): OleVariant; safecall;
    function GetFoto(Empresa: OleVariant; Numero: Integer): OleVariant; safecall;
    function GrabaLibro(Empresa: OleVariant; oDelta: OleVariant; Catalogo: Integer; 
                        HoraFecha: TDateTime; out ErrorCount: Integer): OleVariant; safecall;
    function GrabaVisita(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer; 
                         out iVisitante: Integer): OleVariant; safecall;
    function GetLibroActivoFoto(Empresa: OleVariant; Corte: Integer): OleVariant; safecall;
    function GetSQL(Empresa: OleVariant; OPC: Integer; const Filtro: WideString): OleVariant; safecall;
    function GetCatalogosVisitantes(Empresa: OleVariant; Catalogo: Integer; 
                                    const Pista: WideString; out MaxVisit: WideString): OleVariant; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerVisitantesDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {F05B37A4-AC2E-452C-8E24-C13C4B6A3BEE}
// *********************************************************************//
  IdmServerVisitantesDisp = dispinterface
    ['{F05B37A4-AC2E-452C-8E24-C13C4B6A3BEE}']
    function GetCatalogo(Empresa: OleVariant; Catalogo: Integer): OleVariant; dispid 1;
    function GrabaCatalogo(Empresa: OleVariant; oDelta: OleVariant; Catalogo: Integer; 
                           out ErrorCount: Integer): OleVariant; dispid 2;
    function GetCondiciones(Empresa: OleVariant): OleVariant; dispid 3;
    function GetCitas(Empresa: OleVariant; FechaInicio: TDateTime; FechaFin: TDateTime; 
                      const sFiltros: WideString; const HoraInicio: WideString; 
                      const HoraFin: WideString): OleVariant; dispid 4;
    function BuscaAnfitrion(Empresa: OleVariant; oParams: OleVariant): OleVariant; dispid 5;
    function BuscaVisitante(Empresa: OleVariant; oParams: OleVariant): OleVariant; dispid 6;
    function GrabaCitas(Empresa: OleVariant; oDelta: OleVariant; var ErrorCount: Integer; 
                        var iCita: Integer): OleVariant; dispid 7;
    function CorteSiguiente(Empresa: OleVariant; const Caseta: WideString; Usuario: Integer; 
                            Corte: Integer; out Datos: OleVariant): OleVariant; dispid 8;
    function CorteAnterior(Empresa: OleVariant; const Caseta: WideString; Usuario: Integer; 
                           Corte: Integer; out Datos: OleVariant): OleVariant; dispid 9;
    function CorteActivo(Empresa: OleVariant; Corte: Integer; out Datos: OleVariant): OleVariant; dispid 10;
    function GetCortes(Empresa: OleVariant; FechaInicio: TDateTime; FechaFin: TDateTime; 
                       const sFiltros: WideString; iStatus: Integer): OleVariant; dispid 11;
    function GetLibroActivo(Empresa: OleVariant; Corte: Integer): OleVariant; dispid 12;
    function GetLibros(Empresa: OleVariant; FechaInicio: TDateTime; FechaFin: TDateTime; 
                       const sFiltros: WideString): OleVariant; dispid 13;
    function GetLibroSalidas(Empresa: OleVariant): OleVariant; dispid 14;
    function GrabaCorte(Empresa: OleVariant; oDelta: OleVariant; out iCorte: Integer; 
                        out ErrorCount: Integer): OleVariant; dispid 15;
    function GetAlertas(Empresa: OleVariant; const Caseta: WideString): OleVariant; dispid 16;
    function GetFoto(Empresa: OleVariant; Numero: Integer): OleVariant; dispid 301;
    function GrabaLibro(Empresa: OleVariant; oDelta: OleVariant; Catalogo: Integer; 
                        HoraFecha: TDateTime; out ErrorCount: Integer): OleVariant; dispid 302;
    function GrabaVisita(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer; 
                         out iVisitante: Integer): OleVariant; dispid 303;
    function GetLibroActivoFoto(Empresa: OleVariant; Corte: Integer): OleVariant; dispid 304;
    function GetSQL(Empresa: OleVariant; OPC: Integer; const Filtro: WideString): OleVariant; dispid 305;
    function GetCatalogosVisitantes(Empresa: OleVariant; Catalogo: Integer; 
                                    const Pista: WideString; out MaxVisit: WideString): OleVariant; dispid 306;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer; 
                             out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                           Options: Integer; const CommandText: WideString; var Params: OleVariant; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerVisitantes provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerVisitantes exposed by              
// the CoClass dmServerVisitantes. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerVisitantes = class
    class function Create: IdmServerVisitantes;
    class function CreateRemote(const MachineName: string): IdmServerVisitantes;
  end;

implementation

uses ComObj;

class function CodmServerVisitantes.Create: IdmServerVisitantes;
begin
  Result := CreateComObject(CLASS_dmServerVisitantes) as IdmServerVisitantes;
end;

class function CodmServerVisitantes.CreateRemote(const MachineName: string): IdmServerVisitantes;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerVisitantes) as IdmServerVisitantes;
end;

end.
