library PlanCarrera;

uses
  ComServ,
  PlanCarrera_TLB in 'PlanCarrera_TLB.pas',
  DServerPlanCarrera in 'DServerPlanCarrera.pas' {dmServerPlanCarrera: TMtsDataModule} {dmServerPlanCarrera: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
