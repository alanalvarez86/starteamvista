object dmServerLabor: TdmServerLabor
  OldCreateOrder = False
  OnCreate = MtsDataModuleCreate
  OnDestroy = MtsDataModuleDestroy
  Pooled = False
  Left = 204
  Top = 58
  Height = 479
  Width = 741
  object cdsLog: TServerDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'cdsWorksIndex1'
      end>
    IndexFieldNames = 'AU_FECHA'
    Params = <>
    StoreDefs = True
    Left = 32
    Top = 96
    object IntegerField1: TIntegerField
      FieldName = 'CB_CODIGO'
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'AU_FECHA'
    end
  end
  object cdsLista: TServerDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'cdsWorksIndex1'
      end>
    IndexFieldNames = 'AU_FECHA'
    Params = <>
    StoreDefs = True
    Left = 32
    Top = 24
  end
  object cdsAdicional: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 96
    Top = 24
  end
  object cdsAdicional2: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 96
    Top = 96
  end
end
