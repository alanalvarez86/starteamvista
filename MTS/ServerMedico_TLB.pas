unit ServerMedico_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision: 4$
// File generated on 04/07/2006 04:20:26 p.m. from Type Library described below.

// ************************************************************************ //
// Type Lib: D:\3win_20\MTS\ServicioMedico.tlb (1)
// IID\LCID: {BC375F6D-CF2B-41E5-96DF-0DEEE263BF8F}\0
// Helpfile: 
// DepndLst: 
//   (1) v1.0 Midas, (C:\WINDOWS\system32\midas.dll)
//   (2) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
//   (3) v4.0 StdVCL, (C:\WINDOWS\system32\stdvcl40.dll)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, OleCtrls, StdVCL, 
  MIDAS;

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  ServerMedicoMajorVersion = 1;
  ServerMedicoMinorVersion = 0;

  LIBID_ServerMedico: TGUID = '{BC375F6D-CF2B-41E5-96DF-0DEEE263BF8F}';

  IID_IdmServerMedico: TGUID = '{9CCF315B-D122-44AB-8A99-4F0A02D7F495}';
  CLASS_dmServerMedico: TGUID = '{A659B426-B0A9-4CE1-BA66-4262DBCD3DC9}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerMedico = interface;
  IdmServerMedicoDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerMedico = IdmServerMedico;


// *********************************************************************//
// Interface: IdmServerMedico
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9CCF315B-D122-44AB-8A99-4F0A02D7F495}
// *********************************************************************//
  IdmServerMedico = interface(IAppServer)
    ['{9CCF315B-D122-44AB-8A99-4F0A02D7F495}']
    function  GetTabla(Empresa: OleVariant; iTabla: Integer): OleVariant; safecall;
    function  GrabaTabla(Empresa: OleVariant; iTabla: Integer; oDelta: OleVariant; 
                         out ErrorCount: Integer): OleVariant; safecall;
    function  GetKardexConsulta(Empresa: OleVariant; iEmpleado: Integer): OleVariant; safecall;
    function  GetKardexEmbarazo(Empresa: OleVariant; iEmpleado: Integer): OleVariant; safecall;
    function  GetKardexAccidente(Empresa: OleVariant; iEmpleado: Integer): OleVariant; safecall;
    function  GetMedicinaEntr(Empresa: OleVariant; iEmpleado: Integer): OleVariant; safecall;
    function  GrabaGruposEx(Empresa: OleVariant; oDelta: OleVariant; oCamposEx: OleVariant; 
                            var oCamposExResult: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function  GetGruposEx(Empresa: OleVariant; var oCamposEx: OleVariant): OleVariant; safecall;
    function  GetExpediente(Empresa: OleVariant; iExpediente: Integer; Parametros: OleVariant; 
                            var oConsulta: OleVariant): OleVariant; safecall;
    function  GetHistorial(Empresa: OleVariant; iEmpleado: Integer): OleVariant; safecall;
    function  GrabaMedicinaEntr(Empresa: OleVariant; oDelta: OleVariant; var ErrorCount: Integer): OleVariant; safecall;
    function  GrabaKardexEmbarazo(Empresa: OleVariant; oDelta: OleVariant; var ErrorCount: Integer): OleVariant; safecall;
    function  GrabakardexConsultas(Empresa: OleVariant; oDelta: OleVariant; var ErrorCount: Integer): OleVariant; safecall;
    function  GrabaKardexAccidente(Empresa: OleVariant; oDelta: OleVariant; var ErrorCount: Integer): OleVariant; safecall;
    function  GrabaExpediente(Empresa: OleVariant; oDelta: OleVariant; var ErrorCount: Integer; 
                              var iExpediente: Integer): OleVariant; safecall;
    function  GetSoloExpediente(Empresa: OleVariant; iExpediente: Integer): OleVariant; safecall;
    function  GetDatosEmpleado(Empresa: OleVariant; Empleado: Integer): OleVariant; safecall;
    function  ExpedienteActivo(Empresa: OleVariant; iExpediente: Integer; out oDelta: OleVariant): OleVariant; safecall;
    function  ExpedienteAnterior(Empresa: OleVariant; iExpediente: Integer; out oDelta: OleVariant): OleVariant; safecall;
    function  ExpedienteSiguiente(Empresa: OleVariant; iExpediente: Integer; out oDelta: OleVariant): OleVariant; safecall;
    function  BuscarEnTablas(Empresa: OleVariant; oParams: OleVariant): OleVariant; safecall;
    function  GetCodigos(Empresa: OleVariant): OleVariant; safecall;
    function  GetConsultaGlobal(Empresa: OleVariant): OleVariant; safecall;
    function  GrabaConsultaGlobal(Empresa: OleVariant; oDelta: OleVariant; oParams: OleVariant; 
                                  var ErrorCount: Integer): OleVariant; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerMedicoDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9CCF315B-D122-44AB-8A99-4F0A02D7F495}
// *********************************************************************//
  IdmServerMedicoDisp = dispinterface
    ['{9CCF315B-D122-44AB-8A99-4F0A02D7F495}']
    function  GetTabla(Empresa: OleVariant; iTabla: Integer): OleVariant; dispid 1;
    function  GrabaTabla(Empresa: OleVariant; iTabla: Integer; oDelta: OleVariant; 
                         out ErrorCount: Integer): OleVariant; dispid 2;
    function  GetKardexConsulta(Empresa: OleVariant; iEmpleado: Integer): OleVariant; dispid 4;
    function  GetKardexEmbarazo(Empresa: OleVariant; iEmpleado: Integer): OleVariant; dispid 3;
    function  GetKardexAccidente(Empresa: OleVariant; iEmpleado: Integer): OleVariant; dispid 5;
    function  GetMedicinaEntr(Empresa: OleVariant; iEmpleado: Integer): OleVariant; dispid 6;
    function  GrabaGruposEx(Empresa: OleVariant; oDelta: OleVariant; oCamposEx: OleVariant; 
                            var oCamposExResult: OleVariant; out ErrorCount: Integer): OleVariant; dispid 1610809350;
    function  GetGruposEx(Empresa: OleVariant; var oCamposEx: OleVariant): OleVariant; dispid 7;
    function  GetExpediente(Empresa: OleVariant; iExpediente: Integer; Parametros: OleVariant; 
                            var oConsulta: OleVariant): OleVariant; dispid 8;
    function  GetHistorial(Empresa: OleVariant; iEmpleado: Integer): OleVariant; dispid 9;
    function  GrabaMedicinaEntr(Empresa: OleVariant; oDelta: OleVariant; var ErrorCount: Integer): OleVariant; dispid 10;
    function  GrabaKardexEmbarazo(Empresa: OleVariant; oDelta: OleVariant; var ErrorCount: Integer): OleVariant; dispid 11;
    function  GrabakardexConsultas(Empresa: OleVariant; oDelta: OleVariant; var ErrorCount: Integer): OleVariant; dispid 12;
    function  GrabaKardexAccidente(Empresa: OleVariant; oDelta: OleVariant; var ErrorCount: Integer): OleVariant; dispid 13;
    function  GrabaExpediente(Empresa: OleVariant; oDelta: OleVariant; var ErrorCount: Integer; 
                              var iExpediente: Integer): OleVariant; dispid 14;
    function  GetSoloExpediente(Empresa: OleVariant; iExpediente: Integer): OleVariant; dispid 15;
    function  GetDatosEmpleado(Empresa: OleVariant; Empleado: Integer): OleVariant; dispid 16;
    function  ExpedienteActivo(Empresa: OleVariant; iExpediente: Integer; out oDelta: OleVariant): OleVariant; dispid 17;
    function  ExpedienteAnterior(Empresa: OleVariant; iExpediente: Integer; out oDelta: OleVariant): OleVariant; dispid 18;
    function  ExpedienteSiguiente(Empresa: OleVariant; iExpediente: Integer; out oDelta: OleVariant): OleVariant; dispid 19;
    function  BuscarEnTablas(Empresa: OleVariant; oParams: OleVariant): OleVariant; dispid 20;
    function  GetCodigos(Empresa: OleVariant): OleVariant; dispid 21;
    function  GetConsultaGlobal(Empresa: OleVariant): OleVariant; dispid 22;
    function  GrabaConsultaGlobal(Empresa: OleVariant; oDelta: OleVariant; oParams: OleVariant; 
                                  var ErrorCount: Integer): OleVariant; dispid 23;
    function  AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; 
                              MaxErrors: Integer; out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function  AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                            Options: Integer; const CommandText: WideString; 
                            var Params: OleVariant; var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function  AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function  AS_GetProviderNames: OleVariant; dispid 20000003;
    function  AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function  AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                            var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerMedico provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerMedico exposed by              
// the CoClass dmServerMedico. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerMedico = class
    class function Create: IdmServerMedico;
    class function CreateRemote(const MachineName: string): IdmServerMedico;
  end;

implementation

uses ComObj;

class function CodmServerMedico.Create: IdmServerMedico;
begin
  Result := CreateComObject(CLASS_dmServerMedico) as IdmServerMedico;
end;

class function CodmServerMedico.CreateRemote(const MachineName: string): IdmServerMedico;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerMedico) as IdmServerMedico;
end;

end.
