unit DServerVisitantes;

interface

{$define DEBUGSENTINEL}
{$define FIX_1817}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComServ, ComObj, VCLCom, StdVcl, {BdeMts,} DataBkr, DBClient,
  MtsRdm, Mtx, db,
  Variants,
  {$ifndef DOS_CAPAS}
  Visitantes_TLB,
  {$endif}
  DZetaServerProvider,
  ZetaSqlBroker,
  ZCreator,
{$ifdef DEBUGSENTINEL}
  FAutoServer,
  FAutoClasses,
{$endif}
  ZVisitantesTools;

type

   eAllScripts = ( eBuscaAnfitrion,
                  eBuscaVisitante,
                  eMaxCitas,
                  eCorteAnterior,
                  eCorteSiguiente,
                  eMaxCorte,
                  eUpdateAbiertaCorte,
                  eCorteLibro,
                  eMaxLibro,
                  eLibroSalidas,
                  eEntradasLibro,
                  eAlertas,
                  eMaxVisita,
                  eMaxEmpresa,
                  eMaxAnfitrion,
                  eMuestraFoto,
                  eGetCitas,
                  eGetLibros,
                  eGetLibrosFoto,
                  eGetTotalesAutos  ); //acl

  TdmServerVisitantes = class(TMtsDataModule{$ifndef DOS_CAPAS}, IdmServerVisitantes {$endif})
    procedure MtsDataModuleCreate(Sender: TObject);
    procedure MtsDataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FCita: Integer;
    FVisita: Integer;
    FCorte: integer;
    FTipoCatalogo: eTipoCatalogo;
    oZetaProvider: TdmZetaServerProvider;
    FHoraFecha: TDateTime;
{$ifndef DOS_CAPAS}
    oZetaCreator: TZetaCreator;
    oZetaSQLBroker: TSQLBroker;
    {$ifdef DEBUGSENTINEL}
    oAutoServer: TAutoServer;
    {$endif}
{$endif}

    function GetNombreTabla( const eCatalogo: eTipoCatalogo): String;
    function GetScript( const eScript: eAllScripts ): String;
    function ObtenerNombres( const sNombre: String; const sPrefijo: String ): String;
    function GetMaxCodigo( const eScript: eAllScripts ): integer;
    procedure SetTablaInfo(const eCatalogo: eTipoCatalogo);
    procedure BeforeUpdateCitas(Sender: TObject; SourceDS: TDataSet;DeltaDS: TzProviderClientDataset; UpdateKind: TUpdateKind;var Applied: Boolean);
    procedure BeforeUpdateCorte(Sender: TObject; SourceDS: TDataSet;DeltaDS: TzProviderClientDataset; UpdateKind: TUpdateKind;var Applied: Boolean);
    procedure BeforeUpdateLibro(Sender: TObject; SourceDS: TDataSet;DeltaDS: TzProviderClientDataset; UpdateKind: TUpdateKind;var Applied: Boolean);
    procedure BeforeUpdateVisitante(Sender: TObject; SourceDS: TDataSet;DeltaDS: TzProviderClientDataset; UpdateKind: TUpdateKind;var Applied: Boolean);
    procedure AfterUpdateVisitante(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataset; UpdateKind: TUpdateKind);
    procedure BeforeUpdateEmpVisita(Sender: TObject; SourceDS: TDataSet;DeltaDS: TzProviderClientDataset; UpdateKind: TUpdateKind;var Applied: Boolean);
    procedure BeforeUpdateAnfitrion(Sender: TObject; SourceDS: TDataSet;DeltaDS: TzProviderClientDataset; UpdateKind: TUpdateKind;var Applied: Boolean);
    procedure BeforeUpdate( DeltaDS: TzProviderClientDataset; const sCampo:string; const eScript: eAllScripts );
    procedure ActualizaCaseta(const lAbierta: Boolean;const sCaseta: string);
    procedure GrabaCambiosBitacora( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataset; UpdateKind: TUpdateKind );
    function ModuloAutorizado: Boolean;
    function GetSQLBroker: TSQLBroker;
    procedure ClearBroker;
    procedure InitBroker;
    procedure InitCreator;
    function getScriptOpc( const opc:integer; const filtro:string ):string;
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    property SQLBroker: TSQLBroker read GetSQLBroker;
    function GetCatalogosVisitantes(Empresa: OleVariant; Catalogo: Integer;
      const Pista: WideString; out MaxVisit: WideString): OleVariant;
      safecall;
{$ifdef DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
{$endif}
    function GetCatalogo(Empresa: OleVariant; Catalogo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaCatalogo(Empresa, oDelta: OleVariant; Catalogo: Integer; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCondiciones(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCitas(Empresa: OleVariant; FechaInicio, FechaFin: TDateTime; const sFiltros, HoraInicio, HoraFin: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function BuscaAnfitrion(Empresa: OleVariant; oParams: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function BuscaVisitante(Empresa: OleVariant; oParams: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaCitas(Empresa: OleVariant; oDelta: OleVariant; var ErrorCount: Integer; var iCita: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function CorteSiguiente(Empresa: OleVariant; const Caseta: WideString; Usuario, Corte: Integer; out Datos: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function CorteAnterior(Empresa: OleVariant; const Caseta: WideString; Usuario, Corte: Integer; out Datos: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function CorteActivo(Empresa: OleVariant; Corte: Integer;out Datos: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCortes(Empresa: OleVariant; FechaInicio, FechaFin: TDateTime; const sFiltros: WideString; iStatus: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetLibroActivo(Empresa: OleVariant; Corte: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetLibros(Empresa: OleVariant; FechaInicio, FechaFin: TDateTime;const sFiltros: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetLibroSalidas(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaCorte(Empresa, oDelta: OleVariant; out iCorte,ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetAlertas(Empresa: OleVariant; const Caseta: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetFoto(Empresa: OleVariant; Numero: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaLibro(Empresa, oDelta: OleVariant; Catalogo: Integer; HoraFecha: TDateTime; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaVisita(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer;out iVisitante: Integer): OleVariant;{$ifndef DOS_CAPAS} safecall; {$endif}
    function GetTotales(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetLibroActivoFoto(Empresa: OleVariant; Corte: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetSQL(Empresa: OleVariant; OPC: Integer; const Filtro: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
  end;

var
  dmServerVisitantes: TdmServerVisitantes;

const
     K_CAMPOS_ANFITRION = 'AN_NUMERO,AN_DEPTO,AN_APE_PAT,AN_APE_MAT,AN_NOMBRES,AN_STATUS,AN_TEL ';
     K_CAMPOS_VISITANTE = 'VI_NUMERO, VI_TIPO, AN_NUMERO, VI_ASUNTO, VI_APE_PAT, VI_APE_MAT, VI_NOMBRES, VI_NACION, VI_STATUS, EV_NUMERO, VI_SEXO, VI_FEC_NAC, VI_MIGRA, VI_FOTO ';
     K_CAMPOS_VISITANTE2 = 'VI_NUMERO, VI_TIPO, AN_NUMERO, VI_ASUNTO, VI_APE_PAT, VI_APE_MAT, VI_NOMBRES, VI_NACION, VI_STATUS, visita.EV_NUMERO, VI_SEXO, VI_FEC_NAC, VI_MIGRA, VI_FOTO ';
     K_CAMPOS_LIBRO = 'LI_FOLIO,LI_TIPO_ID,LI_CAR_TIP,L.CI_FOLIO,LI_ASUNTO,LI_CDEPTO,L.AN_NUMERO,L.EV_NUMERO,L.VI_NUMERO,LI_SAL_VIG,LI_ENT_CAS,LI_SAL_CAS,LI_ENT_FEC,LI_ENT_HOR,LI_ENT_VIG,LI_SAL_FEC,LI_SAL_HOR,LI_NOMBRE,' +
                      'LI_EMPRESA,LI_ANFITR,LI_DEPTO,LI_GAFETE,LI_ID,LI_CAR_PLA,LI_CAR_DES,LI_CAR_EST,LI_STATUS,LI_TEXTO1,LI_TEXTO2,LI_TEXTO3,LI_OBSERVA,LI_ENT_COR,LI_SAL_COR, LI_DEVGAF';

     K_PRETTY_ANFI = PRETTY_ANFITRIO + ' ' + P_ANFITRION_NAME + ' ';
     K_PRETTY_VISIT = PRETTY_VISIT + ' ' +  P_VISITA_NAME + ' ';

implementation

uses
{$ifdef DEBUGSENTINEL}
     {$ifndef DOS_CAPAS}
     ZetaLicenseMgr,
     {$endif}
{$else}
     FAutoServer,
     FAutoClasses,
{$endif}
     ZGlobalTress,
     ZetaTipoEntidad,
     ZetaServerTools,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaCommonClasses;

{$R *.DFM}

class procedure TdmServerVisitantes.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

{$ifdef DEBUGSENTINEL}
procedure TdmServerVisitantes.MtsDataModuleCreate(Sender: TObject);
{$ifndef DOS_CAPAS}
var
   oLicenseMgr : TLicenseMgr;
{$endif}
begin
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
     oAutoServer := ZetaSQLBroker.GetAutoServer;
     {$ifdef DOS_CAPAS}
     ZetaSQLBroker.ReadAutoServer( oAutoServer, NIL, NIL );
     {$else}
     oLicenseMgr := TLicenseMgr.Create( oZetaProvider );
     try
        ZetaSQLBroker.ReadAutoServer( oAutoServer, oLicenseMgr.AutoGetData, oLicenseMgr.AutoSetData );
     finally
            FreeAndNil( oLicenseMgr );
     end;
     {$endif}
end;
{$else}
procedure TdmServerVisitantes.MtsDataModuleCreate(Sender: TObject);
begin
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
end;
{$endif}

procedure TdmServerVisitantes.MtsDataModuleDestroy(Sender: TObject);
begin
{$ifdef DEBUGSENTINEL}
     ZetaSQLBroker.FreeAutoServer( oAutoServer );
{$endif}
     ZetaSQLBroker.FreeZetaCreator( oZetaCreator );
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
end;

function TdmServerVisitantes.GetSQLBroker: TSQLBroker;
begin
{$ifdef DOS_CAPAS}
     Result := ZetaSQLBroker.oZetaSQLBroker;
{$else}
     Result := Self.oZetaSQLBroker;
{$endif}
end;

procedure TdmServerVisitantes.InitCreator;
begin
     if not Assigned( oZetaCreator ) then
        oZetaCreator := ZetaSQLBroker.GetZetaCreator( oZetaProvider );
end;

procedure TdmServerVisitantes.InitBroker;
begin
     InitCreator;
     oZetaCreator.RegistraFunciones([efComunes]);
     oZetaSQLBroker := ZetaSQLBroker.GetSQLBroker( oZetaCreator );
end;

procedure TdmServerVisitantes.ClearBroker;
begin
     ZetaSQLBroker.FreeSQLBroker( oZetaSQLBroker );
end;

{$ifdef DEBUGSENTINEL}
function TdmServerVisitantes.ModuloAutorizado: Boolean;
begin
     with oAutoServer do
          Result := OKModulo( okVisitantes, True );
end;
{$else}
function TdmServerVisitantes.ModuloAutorizado: Boolean;
var
   oAutoServer: TAutoServer;
begin
     oAutoServer := ZetaSQLBroker.GetAutoServer;
     try
        ZetaSQLBroker.ReadAutoServer( oAutoServer, nil, nil );
        with oAutoServer do
             Result := OKModulo( okVisitantes, True );
     finally
            ZetaSQLBroker.FreeAutoServer( oAutoServer );
     end;
end;
{$endif}

{$ifdef DOS_CAPAS}
procedure TdmServerVisitantes.CierraEmpresa;
begin
end;
{$endif}

function TdmServerVisitantes.GetNombreTabla( const eCatalogo: ETipoCatalogo ): String;
begin
    case eCatalogo of
         eTipoId: Result := 'TIPO_ID';
         eTipoCarro: Result := 'TCARRO';
         eTipoAsunto: Result := 'TASUNTO';
         eTipoVisita: Result := 'TVISITA';
         eDepto: Result := 'DEPTO';
    end;
end;

procedure TdmServerVisitantes.SetTablaInfo(const eCatalogo: eTipoCatalogo);
begin
     with oZetaProvider.TablaInfo do
     begin
          //AfterUpdateRecord := GrabaCambiosBitacora;
          BeforeUpdateRecord := NIL;
          case eCatalogo of
               eTipoId, eTipoCarro, eTipoVisita, eDepto, eTipoAsunto:
               begin
                    SetInfo( GetNombreTabla( eCatalogo ),
                             'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO',
                             'TB_CODIGO' );
               end;
               eCaseta : SetInfo( 'CASETA', 'CA_CODIGO,RE_CODIGO,CA_NOMBRE,CA_UBICA,CA_STATUS,CA_CORTE,CA_ABIERTA', 'CA_CODIGO' );
               eAnfitrion:
               begin
                    SetInfo( 'ANFITRIO', K_CAMPOS_ANFITRION, 'AN_NUMERO' );
                    BeforeUpdateRecord :=  BeforeUpdateAnfitrion;
               end;
               eCita: SetInfo( 'CITA','CI_FOLIO,CI_ASUNTO,AN_NUMERO,VI_NUMERO,CI_FECHA,CI_HORA,US_CODIGO,CI_FEC_MOD,CI_HOR_MOD,CI_STATUS,CI_OBSERVA', 'CI_FOLIO' );
               eCompVisita:
               begin
                    SetInfo( 'EMP_VISI','EV_NUMERO,EV_NOMBRE,EV_RAZ_SOC,EV_TEXTO1,EX_TEXTO2,EV_TEXTO3,EV_TEXTO4,EV_TEXTO5,EV_STATUS', 'EV_NUMERO' );
                    BeforeUpdateRecord := BeforeUpdateEmpVisita;
               end;
               eCorte:
               begin
                    SetInfo( 'CORTE','CO_FOLIO,CA_CODIGO,CO_INI_HOR,CO_INI_FEC,CO_INI_VIG,CO_FIN_FEC,CO_FIN_HOR,CO_FIN_VIG,CO_OBSERVA', 'CO_FOLIO' );
                    BeforeUpdateRecord := BeforeUpdateCorte;
               end;
               eVisita,eVisitaMgr:
               begin
                    SetInfo( 'VISITA', K_CAMPOS_VISITANTE , 'VI_NUMERO' );
                    BeforeUpdateRecord := BeforeUpdateVisitante;
                    AfterUpdateRecord := AfterUpdateVisitante;
               end;
               eLibro:
               begin
                    SetInfo( 'LIBRO', 'LI_FOLIO,LI_TIPO_ID,LI_CAR_TIP,CI_FOLIO,LI_ASUNTO,LI_CDEPTO,AN_NUMERO,EV_NUMERO,VI_NUMERO,LI_SAL_VIG,LI_ENT_CAS,LI_SAL_CAS,LI_ENT_FEC,LI_ENT_HOR,LI_ENT_VIG,LI_SAL_FEC,LI_SAL_HOR,LI_NOMBRE,' +
                                      'LI_EMPRESA,LI_ANFITR,LI_DEPTO,LI_GAFETE,LI_ID,LI_CAR_PLA,LI_CAR_DES,LI_CAR_EST,LI_STATUS,LI_TEXTO1,LI_TEXTO2,LI_TEXTO3,LI_OBSERVA,LI_ENT_COR,LI_SAL_COR, LI_DEVGAF', 'LI_FOLIO' );
                    BeforeUpdateRecord := BeforeUpdateLibro;
               end;
               eCondiciones : SetInfo( 'QUERYS', 'QU_CODIGO,QU_DESCRIP,QU_FILTRO,QU_NIVEL,US_CODIGO', 'QU_CODIGO' );
          end;
     end;
end;

function TdmServerVisitantes.GetScript( const eScript: eAllScripts ): String;
begin
     case eScript of
          eBuscaAnfitrion: Result := 'select AN_NUMERO, ' + K_PRETTY_ANFI + ', AN_DEPTO, AN_STATUS from ANFITRIO Where %s order by AN_NOMBRES';
          eBuscaVisitante: Result := 'select VI_NUMERO, ' + K_PRETTY_VISIT + ',VI_NOMBRES,VI_APE_PAT,VI_APE_MAT,VI_TIPO, VI_ASUNTO, AN_NUMERO, EV_NUMERO, ' +
                                     'VI_STATUS from VISITA Where %s order by VI_NOMBRES, VI_APE_PAT, VI_APE_MAT';
          eMaxCitas:       Result := 'select MAX(CI_FOLIO) MAXIMO from CITA';
          eCorteAnterior:  Result := 'select * from CORTE C ' +
                                     'where ( C.CO_FOLIO = ( select MAX( C2.CO_FOLIO ) from CORTE C2 where ( C2.CO_FOLIO < %d ) and ( CA_CODIGO = %s ) and ( CO_INI_VIG = %d )) ) order by CO_FOLIO ';
          eCorteSiguiente: Result := 'select * from CORTE C ' +
                                     'where ( C.CO_FOLIO = ( select MIN( C2.CO_FOLIO ) from CORTE C2 where ( C2.CO_FOLIO > %d ) and ( CA_CODIGO = %s ) and ( CO_INI_VIG = %d )) ) order by CO_FOLIO';
          eMaxCorte: Result := 'select MAX(CO_FOLIO) MAXIMO from CORTE';
          eUpdateAbiertaCorte: Result := 'update CASETA set CA_ABIERTA = %s where CA_CODIGO = %s';
          eCorteLibro: Result := 'select CO_INI_FEC,CO_INI_HOR,CO_FIN_FEC,CO_FIN_HOR from CORTE where CO_FOLIO = %d';
          eMaxLibro: Result := 'select MAX(LI_FOLIO) MAXIMO from LIBRO';
          eLibroSalidas: Result := 'select * from LIBRO where LI_SAL_COR = 0 order by LI_ENT_FEC, LI_ENT_HOR, LI_NOMBRE';
          eEntradasLibro: Result := 'select COUNT(*) CUANTOS from LIBRO where LI_ENT_COR = %d';
          eAlertas: Result := ' select * from LIBRO where LI_SAL_COR = 0 and LI_ENT_CAS  = %s ';
          eMaxVisita: Result := 'SELECT MAX(VI_NUMERO) MAXIMO FROM VISITA';
          eMaxEmpresa: Result := 'SELECT MAX(EV_NUMERO) MAXIMO FROM EMP_VISI';
          eMaxAnfitrion: Result := 'SELECT MAX(AN_NUMERO) MAXIMO FROM ANFITRIO';
          eMuestraFoto: Result := 'select VI_NUMERO,VI_FOTO, VI_TIPO from VISITA Where VI_NUMERO = %d';
          eGetCitas : Result := 'select CI_FOLIO,CI_ASUNTO,C.AN_NUMERO,C.VI_NUMERO,CI_FECHA,CI_HORA,C.US_CODIGO,CI_FEC_MOD,' +
                                'CI_HOR_MOD,CI_STATUS,CI_OBSERVA,V.VI_NOMBRES,V.VI_APE_PAT,V.VI_APE_MAT, D.TB_ELEMENT as Depto, A.TB_ELEMENT as Asunto, EM.EV_NOMBRE from CITA C ' +
                               'left outer join VISITA V on C.VI_NUMERO = V.VI_NUMERO left outer join ANFITRIO AN on AN.AN_NUMERO = C.AN_NUMERO '+
                               'left outer join DEPTO D on AN.AN_DEPTO = D.TB_CODIGO  left outer join TASUNTO A on A.TB_CODIGO = C.CI_ASUNTO '+
                               ' left outer join EMP_VISI EM on EM.EV_NUMERO = V.EV_NUMERO Where %s';
          eGetLibros: Result := 'select '+K_CAMPOS_LIBRO+',V.VI_NOMBRES, V.VI_FOTO,V.VI_APE_MAT,V.VI_APE_PAT, A.TB_ELEMENT AS ASUNTO from LIBRO L  '+
                                'left outer join VISITA V on V.VI_NUMERO = L.VI_NUMERO left outer join TASUNTO A on A.TB_CODIGO = L.LI_ASUNTO Where %s';
          eGetLibrosFoto: Result := 'select '+K_CAMPOS_LIBRO+',V.VI_NOMBRES,V.VI_APE_MAT,V.VI_APE_PAT, V.VI_FOTO, A.TB_ELEMENT AS ASUNTO from LIBRO L  '+
                                'left outer join VISITA V on V.VI_NUMERO = L.VI_NUMERO left outer join TASUNTO A on A.TB_CODIGO = L.LI_ASUNTO Where %s';
          eGetTotalesAutos : Result := 'select count(*) from LIBRO where LI_SAL_HOR <> '' and (LI_CAR_PLA <> '' or LI_CAR_DES <> '' )';
     else
         Result := VACIO;
     end;
end;

function TdmServerVisitantes.getScriptOpc( const opc: integer; const filtro:string ): string;
var
   sSQL:string;
   sNombre, sTipo, sAnfitrion, sEmpresa: String;
   sWhere: String;
begin
     case opc of
          1: sSQL := 'Select '+K_CAMPOS_VISITANTE2+' ,EV_NOMBRE as EMPRESA from VISITA left outer join EMP_VISI on VISITA.EV_NUMERO = EMP_VISI.EV_NUMERO '+
                     'Where %s order by VI_NOMBRES, VI_APE_PAT, VI_APE_MAT';
          2: sSQL := 'Select '+K_CAMPOS_VISITANTE2+' ,EV_NOMBRE as EMPRESA from VISITA left outer join EMP_VISI on VISITA.EV_NUMERO = EMP_VISI.EV_NUMERO '+
                     'Where %s and  VI_NUMERO = '+filtro+' order by VI_NOMBRES, VI_APE_PAT, VI_APE_MAT';
          3: sSQL := 'Select count(*) as Tot from LIBRO where LI_ENT_FEC = '+ filtro;
          4: sSQL := 'Select count(*) as Tot from LIBRO where LI_ENT_FEC = '+ filtro + ' and LI_SAL_FEC = '+filtro;
          5: sSQL := 'Select count(*) as Tot from LIBRO where LI_ENT_FEC = '+ filtro + ' and ((LEN(LI_CAR_TIP)>0) or (LEN(LI_CAR_DES)>0) or (LEN(LI_CAR_EST)>0))';
          6: sSQL := 'Select count(*) as Tot from LIBRO where LI_ENT_FEC = '+ filtro + ' and LI_SAL_FEC = '+filtro+ ' and ((LEN(LI_CAR_TIP)>0) or (LEN(LI_CAR_DES)>0) or (LEN(LI_CAR_EST)>0))';
          7: sSQL := 'select TOP 5 count(*) as tops, a.TB_ELEMENT from libro l left outer join TASUNTO a on a.TB_CODIGO = l.LI_ASUNTO where LI_ENT_FEC = '+ filtro + ' group by a.TB_ELEMENT order by tops desc';
          8: sSQL := 'select top 5 v.VI_NOMBRES + '+quotedstr(' ')+' + v.VI_APE_PAT as Nombre,DATEDIFF(MINUTE, CONVERT(DATETIME, (SUBSTRING(CONVERT(VARCHAR, LI_ENT_FEC),1,11) + '+quotedstr(' ')+' + SUBSTRING(LI_ENT_HOR,1,2) + '+quotedstr(':')+' + SUBSTRING(LI_ENT_HOR,3,2) + '+quotedstr(':00:000')+' ))  '+
                     ', CONVERT(DATETIME, (SUBSTRING(CONVERT(VARCHAR, LI_SAL_FEC),1,11) + '+quotedstr(' ')+' + SUBSTRING(LI_SAL_HOR,1,2) + '+quotedstr(':')+' + SUBSTRING(LI_SAL_HOR,3,2) + '+quotedstr(':00:000')+'))) as tiempo'+
                     ' from libro l left outer join visita v on l.VI_NUMERO = v.VI_NUMERO where LI_ENT_FEC ='+ filtro +' AND not IsNull(LI_SAL_HOR, '+quotedstr(' ')+' ) = ' +quotedstr(' ');
     end;
     sNombre:= filtro;

     if opc = 1 then
     begin
          sWhere := obtenerNombres(sNombre, 'VI' );

          if( strLleno( sTipo ) )then
              sWhere := '( ' + sWhere + ' ) and VI_TIPO = ' + UpperCase( EntreComillas( sTipo ) );
          if( strLleno( sAnfitrion ) )then
              sWhere := '( ' + sWhere + ' ) and AN_NUMERO = ' + UpperCase( EntreComillas( sAnfitrion ) );
          if( strLleno( sEmpresa ) )then
              sWhere := '( ' + sWhere + ' ) and EV_NUMERO = ' + UpperCase( EntreComillas( sEmpresa ) );
        end
     else
     begin
          sWhere := ' VI_NUMERO = '+ filtro;
     end;

     Result :=  Format( sSQL , [ sWhere ] );
end;

function TdmServerVisitantes.GetCatalogo(Empresa: OleVariant; Catalogo: Integer): OleVariant;
const
     K_FILTROS_ANFITRIO = K_CAMPOS_ANFITRION + ' ,' + K_PRETTY_ANFI;
     K_FILTROS_VISITANTE = K_CAMPOS_VISITANTE + ' ,' + K_PRETTY_VISIT;
     GET_CATALOGOS_ESP = 'select %s from %s';
var
   sqlText: String;
   lGetCatEspecial: Boolean;
begin
     lGetCatEspecial := False;
     Case eTipoCatalogo( Catalogo ) of
          eAnfitrion : begin
                            lGetCatEspecial := True;
                            sqlText := Format( GET_CATALOGOS_ESP, [ K_FILTROS_ANFITRIO, 'ANFITRIO' ] );
                       end;
   eVisita,eVisitaMgr: begin
                            lGetCatEspecial := True;
                            sqlText := Format( GET_CATALOGOS_ESP, [ K_FILTROS_VISITANTE, 'VISITA' ] );
                       end
                       else
                            SetTablaInfo( eTipoCatalogo( Catalogo ) );
     end;
     if lGetCatEspecial then
         if eTipoCatalogo(Catalogo) = eVisita then
            Result := oZetaProvider.OpenSQL( Empresa, sqlText, False )
         else
             Result := oZetaProvider.OpenSQL( Empresa, sqlText, True )
     else
         Result := oZetaProvider.GetTabla( Empresa );

     SetComplete;
end;

function TdmServerVisitantes.GrabaCatalogo(Empresa, oDelta: OleVariant; Catalogo: Integer; out ErrorCount: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FTipoCatalogo := eTipoCatalogo( Catalogo );
          SetTablaInfo( FTipoCatalogo );
          TablaInfo.AfterUpdateRecord := GrabaCambiosBitacora;
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
     end;
     SetComplete;
end;

procedure TdmServerVisitantes.BeforeUpdateLibro( Sender: TObject; SourceDS: TDataSet;DeltaDS: TzProviderClientDataset; UpdateKind: TUpdateKind; var Applied: Boolean);
 const K_MAX_ENTRADAS_DEMO = 5;
 var
    FDataSet : TZetaCursor;
    dLibro, dCorte: TDateTime;
    {$ifndef FIX_1817}
    dSistema : TDateTime;
    {$endif}
    sHoraLibro, sHoraCorte: string;
    lAgregaRegistros: Boolean;
    iCorte: integer;
begin
     {$ifndef FIX_1817}
     dSistema := Date;
     {$endif}
     iCorte := Integer( CampoAsVar( DeltaDS.FieldByName('LI_ENT_COR') ) );

     FDataset := oZetaProvider.CreateQuery( Format( GetScript( eCorteLibro ),
                                                    [ iCorte ] ) );
     try
        FDataSet.Open;
        dCorte := FDataSet.FieldByName('CO_INI_FEC').AsDateTime;

        with DeltaDS do
        begin
             dLibro := CampoAsVar( FieldByName('LI_ENT_FEC') );
             sHoraLibro := CampoAsVar( FieldByName('LI_ENT_HOR') );
             sHoraCorte := FDataSet.FieldByName('CO_INI_HOR').AsString;

             if ( dLibro < dCorte ) then
                  DatabaseError( 'La fecha de entrada no puede ser menor que la fecha del corte' )
             else
                 if ( dLibro = dCorte ) and
                    ( sHoraLibro < sHoraCorte ) then
                    DatabaseError( 'La hora de entrada no puede ser menor que la hora del corte' );

             {$ifndef FIX_1817}
             {Ya no se valida contra la fecha y hora del servidor ya que cada caseta puede estar en un horario distinto}
             if dLibro > dSistema then
                DatabaseError( 'La fecha de entrada no puede ser mayor que la fecha del sistema' )
             else
                 if ( dLibro = dSistema ) and
                    ( sHoraLibro > HoraAsStr( Now ) ) then
                    DatabaseError( 'La hora de entrada no puede ser mayor que la hora del sistema' );
             {$endif}

        end;

        FDataSet.Close;

        with oZetaProvider do
        begin
             if ( UpdateKind = ukInsert ) then
             begin
                  lAgregaRegistros := ModuloAutorizado;

                  if NOT lAgregaRegistros then
                  begin
                       AbreQueryScript(FDataset,  Format( GetScript( eEntradasLibro ), [ iCorte ] ) );
                       lAgregaRegistros := ( FDataset.FieldByName('CUANTOS').AsInteger <= K_MAX_ENTRADAS_DEMO );
                  end;

                  if lAgregaRegistros then
                  begin
                       DeltaDS.Edit;
                       DeltaDS.FieldByName( 'LI_FOLIO' ).AsInteger := GetMaxCodigo( eMaxLibro );
                       DeltaDS.Post;
                  end
                  else
                      DatabaseError( 'Solo se permiten registrar 5 entradas en modo demo ' );

             end
             else if (UpdateKind = ukModify ) then
             begin
                  with DeltaDS do
                  begin
                       if (FieldByName( 'LI_SAL_COR' ).AsInteger <> 0) AND
                          (FieldByName( 'LI_SAL_FEC' ).AsDateTime = NullDateTime) then
                       begin
                            Edit;
                            FieldByName( 'LI_SAL_FEC' ).AsString := FechaAsStr(FHoraFecha );
                            FieldByName( 'LI_SAL_HOR' ).AsString   := HoraAsStr( FHoraFecha );
                            Post;
                       end;
                  end;
             end;
        end;
     finally
            FreeAndNil(FDataSet);
     end;

end;

procedure TdmServerVisitantes.ActualizaCaseta( const lAbierta: Boolean; const sCaseta: string );
begin
     oZetaProvider.ExecSQL( oZetaProvider.EmpresaActiva, Format( GetScript( eUpdateAbiertaCorte ), [EntreComillas( zBoolToStr(lAbierta) ), EntreComillas( sCaseta ) ] ) );
end;

procedure TdmServerVisitantes.BeforeUpdateCorte( Sender: TObject; SourceDS: TDataSet;DeltaDS: TzProviderClientDataset; UpdateKind: TUpdateKind; var Applied: Boolean );
var
   sHoraIni, sHoraFin: String;
   dFechaIni, dFechaFin: TDateTime;
begin
     with oZetaProvider do
     begin
          if ( UpdateKind = ukInsert ) then
          begin
               FCorte := GetMaxCodigo(eMaxCorte);
               DeltaDS.Edit;
               DeltaDS.FieldByName( 'CO_FOLIO' ).AsInteger := FCorte;
               DeltaDS.Post;
               ActualizaCaseta( TRUE,  DeltaDS.FieldByName( 'CA_CODIGO' ).AsString );
          end
          else if (UpdateKind = ukModify ) then
          begin
               with DeltaDS do
               begin
                    if (FieldByName( 'CO_FIN_FEC' ).AsDateTime = NullDateTime) then
                    begin
                         Edit;
                         FieldByName( 'CO_FIN_FEC' ).AsDateTime := Date;
                         FieldByName( 'CO_FIN_HOR' ).AsString   := HoraAsStr( Now );
                         Post;
                    end;

                    dFechaIni := CampoAsVar( FieldByName('CO_INI_FEC') );
                    dFechaFin := CampoAsVar( FieldByName('CO_FIN_FEC') );
                    sHoraIni := CampoAsVar( FieldByName('CO_INI_HOR') );
                    sHoraFin := CampoAsVar( FieldByName('CO_FIN_HOR') );

                    if( dFechaIni = dFechaFin )then
                    begin
                         if( sHoraFin <= sHoraIni )then
                         DatabaseError( 'La hora de fin no puede ser menor que la hora de inicio' );
                    end
                    else
                    begin
                         if( dFechaIni > dFechaFin )then
                             DatabaseError( 'La fecha de cierre de corte no puede ser menor que su fecha de inicio' );
                    end;
                    if( dFechaFin = NullDateTime )then
                        DatabaseError( 'Se debe especificar una fecha para cierre de corte');


                    ActualizaCaseta( FALSE, CampoAsVar( FieldByName( 'CA_CODIGO' ) ) );
               end;
          end;
     end;
end;

procedure TdmServerVisitantes.BeforeUpdateCitas(Sender: TObject; SourceDS: TDataSet;DeltaDS: TzProviderClientDataset;
                                    UpdateKind: TUpdateKind;var Applied: Boolean);
begin
     with oZetaProvider do
     begin
          if ( UpdateKind = ukInsert ) then
          begin
               FCita := GetMaxCodigo(eMaxCitas);
               DeltaDS.Edit;
               DeltaDS.FieldByName( 'CI_FOLIO' ).AsInteger := FCita;
               DeltaDS.Post;
          end;
     end;
end;

function TdmServerVisitantes.GetMaxCodigo( const eScript: eAllScripts ): Integer;
var
   FDataSet : TZetaCursor;
begin
     FDataset := oZetaProvider.CreateQuery( GetScript( eScript ) );
     try
        FDataSet.Open;
        Result := FDataSet.FieldByName('MAXIMO').AsInteger + 1; //se le suma 1 al numero maximo.
        FDataSet.Close;
     finally
            FreeAndNil(FDataSet);
     end;
end;

function TdmServerVisitantes.GetCondiciones(Empresa: OleVariant): OleVariant;
begin
     Result := GetCatalogo( Empresa, Ord( eCondiciones ) );
     SetComplete;
end;

function TdmServerVisitantes.GetCitas(Empresa: OleVariant; FechaInicio, FechaFin: TDateTime; const sFiltros, HoraInicio,
  HoraFin: WideString): OleVariant;
var
   sFiltro: string;
begin
     //SetTablaInfo( eCita );

     sFiltro := ConcatFiltros( Format( '( CI_FECHA >= %s ) and ( CI_FECHA <= %s )',
                                       [ DateToStrSQLC( FechaInicio ),
                                         DateToStrSQLC( FechaFin ) ] ), sFiltros );

     if StrLleno(HoraInicio) and StrLleno(HoraFin) then
     begin
          sFiltro := ConcatFiltros( sFiltro, Format( '( CI_HORA >= %s ) and ( CI_HORA <= %s )',
                                                     [ EntreComillas( HoraInicio ),
                                                       EntreComillas( HoraFin ) ] ) );
     end
     else if StrLleno(HoraInicio) then
     begin
          sFiltro := ConcatFiltros( sFiltro, Format( '( CI_HORA >= %s )',
                                                     [ EntreComillas( HoraInicio ) ] ) );
     end
     else if StrLleno(HoraFin) then
     begin
          sFiltro := ConcatFiltros( sFiltro, Format( '( CI_HORA <= %s )',
                                                     [ EntreComillas( HoraFin ) ] ) );
     end;

     Result := oZetaProvider.OpenSQL( Empresa, Format( GetScript( eGetCitas ) , [ sFiltro ] ), True );
     SetComplete;
end;

function TdmServerVisitantes.ObtenerNombres( const sNombre: String; const sPrefijo: String ): String;
         function ConcatenaNombres( sToken: String ): String;
         begin
              if( sPrefijo = 'AN') then
                  Result := ' ( ( UPPER(AN_APE_PAT) like ' + EntreComillas( '%'+ sToken + '%' ) + ' ) or ' +
                            '( UPPER(AN_APE_MAT) like ' + EntreComillas( '%'+ sToken + '%' ) + ' ) or ' +
                            '( UPPER(AN_NOMBRES) like ' + EntreComillas( '%'+ sToken + '%' ) + ' ) ) '
              else
                  Result := ' ( ( UPPER(VI_APE_PAT) like ' + EntreComillas( '%'+ sToken + '%' ) + ' ) or ' +
                            '( UPPER(VI_APE_MAT) like ' + EntreComillas( '%'+ sToken + '%' ) + ' ) or ' +
                            '( UPPER(VI_NOMBRES) like ' + EntreComillas( '%'+ sToken + '%' ) + ' ) ) '
         end;
var
   sListNombre: TStrings;
   i: Integer;
const
     K_COMODIN = '1=1';
begin
     Result := K_COMODIN;
     try
        sListNombre := TStringList.Create;
        sListNombre.CommaText := sNombre;
        for i := 0 to ( sListNombre.Count - 1 ) do
            Result := Result + ' and ' + ConcatenaNombres( UpperCase( sListNombre.Strings[ i ] ) );
     finally
            FreeAndNil( sListNombre );
     end;
end;

function TdmServerVisitantes.BuscaAnfitrion(Empresa: OleVariant; oParams: OleVariant): OleVariant;
var
   sNombre, sDepto: String;
   sWhere: String;
   iStatus: Integer;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( oParams );
          with ParamList do
          begin
               sNombre := UpperCase( ParamByName('NOMBRE').AsString );
               sDepto := UpperCase( ParamByName('DEPTO').AsString );
               iStatus := ( ParamByName('STATUS').AsInteger - 1 ); //Se le resta 1 porque se agreg� el "TODOS"
          end;
          sWhere := ObtenerNombres( sNombre, 'AN' );
          if( strLleno( sDepto ) )then
              sWhere := '( ' + sWhere + ' ) and AN_DEPTO = ' + UpperCase( EntreComillas( sDepto ) );
          if( iStatus <> -1 )then
              sWhere := sWhere + ' and AN_STATUS = ' + InttoStr( iStatus );
          Result := OpenSQL( Empresa, Format( GetScript( eBuscaAnfitrion ) , [ sWhere ] ), TRUE );
     end;
     SetComplete;
end;

function TdmServerVisitantes.BuscaVisitante(Empresa: OleVariant; oParams: OleVariant): OleVariant;
var
   sNombre, sTipo, sAnfitrion, sEmpresa: String;
   sWhere: String;
   iStatus: Integer;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( oParams );
          with ParamList do
          begin
               sNombre := UpperCase( ParamByName('NOMBRE').AsString );
               sTipo := UpperCase( ParamByName('TIPO').AsString );
               sAnfitrion := UpperCase( ParamByName('ANFITRION').AsString );
               sEmpresa := UpperCase( ParamByName('EMPRESA').AsString );
               iStatus := ( ParamByName('STATUS').AsInteger - 1 ); //Se le resta 1 porque se agreg� el "TODOS"
          end;
          sWhere := ObtenerNombres( sNombre, 'VI' );
          if( strLleno( sTipo ) )then
              sWhere := '( ' + sWhere + ' ) and VI_TIPO = ' + UpperCase( EntreComillas( sTipo ) );
          if( strLleno( sAnfitrion ) )then
              sWhere := '( ' + sWhere + ' ) and AN_NUMERO = ' + UpperCase( EntreComillas( sAnfitrion ) );
          if( strLleno( sEmpresa ) )then
              sWhere := '( ' + sWhere + ' ) and EV_NUMERO = ' + UpperCase( EntreComillas( sEmpresa ) );
          if( iStatus <> -1 )then
              sWhere := sWhere + ' and VI_STATUS = ' + InttoStr( iStatus );
          Result := OpenSQL( Empresa, Format( GetScript( eBuscaVisitante ) , [ sWhere ] ), TRUE );
     end;
     SetComplete;
end;

function TdmServerVisitantes.GrabaCitas(Empresa: OleVariant; oDelta: OleVariant; var ErrorCount: Integer; var iCita: Integer): OleVariant;
begin
     FCita := iCita;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FTipoCatalogo:= eCita;
          SetTablaInfo( eCita );
          TablaInfo.BeforeUpdateRecord := BeforeUpdateCitas;
          TablaInfo.AfterUpdateRecord:= GrabaCambiosBitacora;
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
     end;
     iCita := FCita;
     SetComplete;
end;

function TdmServerVisitantes.CorteActivo(Empresa: OleVariant;  Corte: Integer; out Datos: OleVariant): OleVariant;
begin
     SetTablaInfo( eCorte );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( '( CO_FOLIO = %d )',
                                      [ Corte ] );
          Datos := GetTabla( Empresa );
          Result := ( RecsOut > 0 );
     end;
     SetComplete;
end;

function TdmServerVisitantes.CorteAnterior(Empresa: OleVariant; const Caseta: WideString; Usuario, Corte: Integer; out Datos: OleVariant): OleVariant;
var
   sSQL: String;
begin
     sSQL := Format( GetScript( eCorteAnterior ), [ Corte, EntreComillas( Caseta ), Usuario ] );
     Datos := oZetaProvider.OpenSQL( Empresa, sSQL, True );
     Result := ( oZetaProvider.RecsOut > 0 );
     SetComplete;
end;

function TdmServerVisitantes.CorteSiguiente(Empresa: OleVariant; const Caseta: WideString; Usuario, Corte: Integer; out Datos: OleVariant): OleVariant;
var
   sSQL: String;
begin
     sSQL := Format( GetScript( eCorteSiguiente ), [ Corte, EntreComillas( Caseta ), Usuario ] );
     Datos := oZetaProvider.OpenSQL( Empresa, sSQL, True );
     Result := ( oZetaProvider.RecsOut > 0 );
     SetComplete;
end;

function TdmServerVisitantes.GetCortes(Empresa: OleVariant; FechaInicio, FechaFin: TDateTime; const sFiltros: WideString;
  iStatus: Integer): OleVariant;
var
   FFiltro: String;
begin
     FFiltro := VACIO;
     SetTablaInfo( eCorte );
     with oZetaProvider do
     begin
          if( eCorteStatus( iStatus ) in [ Low( eCorteStatus )..High( eCorteStatus ) ] )then
          begin
               Case eCorteStatus( iStatus ) of
                    ecAbierto: FFiltro := ' and ( CO_FIN_FEC = ' + DateToStrSQLC( NullDateTime ) + ' )';
                    ecCerrado: FFiltro := ' and ( CO_FIN_FEC <> ' + DatetoStrSQLC( NullDateTime ) + ' )';
               end;
          end;
          TablaInfo.Filtro := ConcatFiltros( Format( '( ( CO_INI_FEC >= %s ) and ( CO_INI_FEC <= %s ) ) %s',
                                                     [ DateToStrSQLC( FechaInicio ),
                                                       DateToStrSQLC( FechaFin ), FFiltro ] ) , sFiltros );
          Result := GetTabla( Empresa );
    end;
    SetComplete;
end;

function TdmServerVisitantes.GetLibroActivo(Empresa: OleVariant; Corte: Integer): OleVariant;
var
   sFiltro:string;
begin
     //SetTablaInfo( eLibro );
     with oZetaProvider do
     begin
          sFiltro := Format( '( LI_ENT_COR = %0:d ) or ( LI_SAL_COR = %0:d )', [ Corte ] );
          Result := OpenSQL( Empresa, Format( GetScript( eGetLibros ) , [ sFiltro ] ), True );
     end;
     SetComplete;
end;

function TdmServerVisitantes.GetLibros(Empresa: OleVariant; FechaInicio, FechaFin: TDateTime; const sFiltros: WideString): OleVariant;
 var
    sFiltro: string;
begin
     //SetTablaInfo( eLibro );
     with oZetaProvider do
     begin
          if ( FechaInicio = NullDateTime ) AND ( FechaFin = NullDateTime ) then
             sFiltro := sFiltros
          else if ( FechaInicio = NullDateTime ) then
              sFiltro := ConcatFiltros( Format( '( LI_ENT_FEC <= %s ) ',
                                                [ DateToStrSQLC( FechaFin ) ] ), sFiltros )
          else if ( FechaFin = NullDateTime ) then
              sFiltro := ConcatFiltros( Format( '( LI_ENT_FEC >= %s ) ',
                                                [ DateToStrSQLC( FechaInicio ) ] ), sFiltros )
          else
              sFiltro := ConcatFiltros( Format( '( ( LI_ENT_FEC >= %s ) and ( LI_ENT_FEC <= %s ) )',
                                                [ DateToStrSQLC( FechaInicio ),
                                                DateToStrSQLC( FechaFin ) ] ), sFiltros );

          Result := OpenSQL( Empresa, Format( GetScript( eGetLibros ) , [ sFiltro ] ), True );
    end;
    SetComplete;
end;

function TdmServerVisitantes.GetLibroSalidas(Empresa: OleVariant): OleVariant;
     //Result := oZetaProvider.OpenSQL( Empresa, GetScript(eLibroSalidas), True )
var
   sFiltro: String;
begin
     sFiltro := VACIO;
     SetTablaInfo( eLibro );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := 'LI_SAL_COR = 0' ;
          TablaInfo.SetOrder( 'LI_ENT_FEC, LI_ENT_HOR, LI_NOMBRE' );
          Result := GetTabla( Empresa );
    end;
    SetComplete;
end;

procedure TdmServerVisitantes.GrabaCambiosBitacora(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataset; UpdateKind: TUpdateKind);
var
   eClase: eClaseBitacora;
   sTitulo: String;

 procedure DatosBitacora( eClaseCatalogo:eClaseBitacora; sDescripcion, sKeyField : string );
  var
     oField : TField;
 begin
      eClase  := eClaseCatalogo;
      sTitulo := sDescripcion;

      if StrLleno(sKeyField) then
      begin
           oField := DeltaDS.FieldByName( sKeyField );

           if oField is TNumericField then
              sTitulo := sTitulo + ' ' + IntToStr( ZetaServerTools.CampoOldAsVar( oField ) )
           else
               sTitulo := sTitulo + ' ' + ZetaServerTools.CampoOldAsVar( oField );
      end;
 end;

begin
     if UpdateKind in [ ukModify, ukDelete ] then
     begin
          eClase := clbNinguno;

          case FTipoCatalogo of
                    eDepto     : DatosBitacora( clbDepto, 'Departamento :', 'TB_CODIGO' );
                    eTipoId    : DatosBitacora( clbTipoID, 'Tipo De Identificaci�n :', 'TB_CODIGO' );
                    eTipoCarro : DatosBitacora( clbTipoCarro, 'Tipo De Veh�culo :', 'TB_CODIGO' );
                    eTipoAsunto: DatosBitacora( clbTipoAsunto, 'Tipo De Asunto :', 'TB_CODIGO' );
                    eTipoVisita: DatosBitacora( clbTipoVisit, 'Tipo De Visitante :', 'TB_CODIGO' );
                    eCaseta    : DatosBitacora( clbCaseta, 'Caseta :', 'CA_CODIGO' );
                    eAnfitrion : DatosBitacora( clbAnfitrion, 'Anfitri�n :', 'AN_NUMERO' );
                    eCita      : DatosBitacora( clbCita, 'Cita :', 'CI_FOLIO' );
                    eCompVisita: DatosBitacora( clbCompVisita, 'Compa��a :', 'EV_NUMERO' );
                    eCorte     : DatosBitacora( clbCorte, 'Corte :', 'CO_FOLIO' );
                    eVisita,eVisitaMgr : DatosBitacora( clbVisita, 'Visitante :', 'VI_NUMERO' );
                    eLibro     : DatosBitacora( clbLibro, 'Libro :', 'LI_FOLIO' );
                    eCondiciones: DatosBitacora( clbCondiciones, 'Condici�n :', 'QU_CODIGO' );
                    else raise Exception.Create( 'No ha sido Integrado el Cat�logo al Case de <DServerVisitanes.GrabaCambiosBitacora>' )
          end;

          with oZetaProvider do
            if UpdateKind = ukModify then
                CambioCatalogo( sTitulo, eClase, DeltaDS )
            else
                BorraCatalogo( sTitulo, eClase, DeltaDS );
     end;
end;

function TdmServerVisitantes.GrabaCorte(Empresa, oDelta: OleVariant; out iCorte, ErrorCount: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FTipoCatalogo := eCorte;
          SetTablaInfo( FTipoCatalogo );
          AfterUpdateRecord:= GrabaCambiosBitacora;
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
          iCorte := FCorte;
     end;
     SetComplete;
end;

procedure TdmServerVisitantes.BeforeUpdateVisitante(Sender: TObject; SourceDS: TDataSet;DeltaDS: TzProviderClientDataset; UpdateKind: TUpdateKind;var Applied: Boolean);
var
   i: Integer;
   lHuboCambios: Boolean;
begin
     if ( UpdateKind = ukModify ) then
     begin
          lHuboCambios := FALSE;
          with DeltaDS do
               for i := 0 to FieldCount-1 do
                   if CambiaCampo( Fields[i] ) then
                   begin
                        lHuboCambios := TRUE;
                        Break;
                   end;
          Applied := ( not lHuboCambios );
     end
     else if ( UpdateKind = ukInsert ) then
     begin
          if CampoAsVar( DeltaDS.FieldByName('VI_NUMERO') ) = 0 then
          begin
               FVisita :=  GetMaxCodigo( eMaxVisita );
               DeltaDS.Edit;
               DeltaDS.FieldByName( 'VI_NUMERO' ).AsInteger := FVisita;
               DeltaDS.Post;
          end;
     end;
end;

procedure TdmServerVisitantes.BeforeUpdate( DeltaDS: TzProviderClientDataset; const sCampo:string; const eScript: eAllScripts );
 var
    iCodigo : integer;
begin
     if CampoAsVar( DeltaDS.FieldByName( sCampo ) ) = 0 then
     begin
          iCodigo :=  GetMaxCodigo( eScript );
          DeltaDS.Edit;
          DeltaDS.FieldByName( sCampo ).AsInteger := iCodigo;
          DeltaDS.Post;
     end;
end;

procedure TdmServerVisitantes.BeforeUpdateEmpVisita(Sender: TObject; SourceDS: TDataSet;DeltaDS: TzProviderClientDataset; UpdateKind: TUpdateKind;var Applied: Boolean);
begin
     if ( UpdateKind = ukInsert ) then
     begin
          BeforeUpdate( DeltaDS, 'EV_NUMERO', eMaxEmpresa );
     end;
end;

procedure TdmServerVisitantes.BeforeUpdateAnfitrion(Sender: TObject; SourceDS: TDataSet;DeltaDS: TzProviderClientDataset; UpdateKind: TUpdateKind;var Applied: Boolean);
begin
     if ( UpdateKind = ukInsert ) then
     begin
          BeforeUpdate( DeltaDS, 'AN_NUMERO', eMaxAnfitrion );
     end;
end;

procedure TdmServerVisitantes.AfterUpdateVisitante(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataset; UpdateKind: TUpdateKind);
const
     K_UPDATE_FOTO = 'UPDATE VISITA SET VI_FOTO = NULL WHERE VI_NUMERO = %d';
begin
     if ( UpdateKind = ukModify ) then
     begin
          with DeltaDS.FieldByName('VI_FOTO') do
          begin
               if( VarIsNull( NewValue ) and (OldValue <> NewValue) )then
               begin
                     with oZetaProvider do
                       ExecSQL( EmpresaActiva, Format( K_UPDATE_FOTO, [ Integer( CampoAsVar( DeltaDS.FieldByName('VI_NUMERO') ) ) ] ) );
               end;
          end;
     end;
end;

function TdmServerVisitantes.GetAlertas(Empresa: OleVariant; const Caseta: WideString): OleVariant;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     oZetaProvider.InitGlobales;
     InitBroker;
     try
        with SQLBroker do
        begin
             Init( enLibro );
             with Agente do
             begin
                  AgregaColumna( 'LIBRO.LI_FOLIO', True, Entidad, tgNumero, 0, 'LI_FOLIO' );
                  AgregaColumna( 'LIBRO.LI_ENT_HOR', True, Entidad, tgNumero, 0, 'LI_ENT_HOR' );
                  AgregaColumna( PRETTY_ANFITRIO, True, enAnfitrio, tgTexto, 100, 'NOMBRE_ANFITRIO' );
                  AgregaColumna( PRETTY_VISIT, True, enVisita, tgTexto, 100, 'NOMBRE_VISITANTE' );
                  AgregaColumna( 'EMP_VISI.EV_NOMBRE', True, enEmpVisita, tgTexto, 30, 'EV_NOMBRE' );
                  AgregaColumna( 'DEPTO.TB_ELEMENT', True, enDepto, tgTexto, 30, 'NOMBRE_DEPTO' );
                  AgregaColumna( 'ANFITRIO.AN_TEL', True, enAnfitrio, tgTexto, 30, 'AN_TEL' );
                  AgregaFiltro( Format( 'LI_ENT_CAS = %s', [ EntreComillas( Caseta ) ] ), True, Entidad );
                  AgregaFiltro( Format( 'LI_ENT_FEC = %s', [ DateToStrSqlC( Date ) ] ), True, Entidad );
                  AgregaFiltro( 'LI_SAL_COR = 0', True, Entidad ); //Visitantes que no han salido de la empresa
                  AgregaFiltro( Format( '(A_MINUTOS(%s) - A_MINUTOS(LIBRO.LI_ENT_HOR) ) > %d', [ EntreComillas( HoraAsStr( Time )),
                                                                                           oZetaProvider.GetGlobalInteger(K_GLOBAL_ALERTAS_TIEMPO_MINIMO) ] ),
                                FALSE, Entidad );
                  AgregaOrden( 'LI_FOLIO', True, Entidad );
             end;
             BuildDataset( oZetaProvider.EmpresaActiva );
             Result := SuperReporte.GetReporte;
        end;
     finally
        ClearBroker;
     end;
end;

//acl
function TdmServerVisitantes.GetFoto(Empresa: OleVariant; Numero: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          Result := OpenSQL( Empresa, Format( GetScript( eMuestraFoto ) , [ Numero ] ), TRUE );
          SetComplete;
     end;
end;

{ACL280409}
function TdmServerVisitantes.GrabaLibro(Empresa, oDelta: OleVariant; Catalogo: Integer; HoraFecha: TDateTime; out ErrorCount: Integer): OleVariant;
begin
     FHoraFecha := HoraFecha;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FTipoCatalogo := eTipoCatalogo( Catalogo );
          SetTablaInfo( FTipoCatalogo );
          AfterUpdateRecord:= GrabaCambiosBitacora;
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
     end;
     SetComplete;
end;

function TdmServerVisitantes.GrabaVisita(Empresa, oDelta: OleVariant;out ErrorCount, iVisitante: Integer): OleVariant;
begin
     FVisita := iVisitante;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FTipoCatalogo:= eVisita;
          SetTablaInfo( eVisita );
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
     end;
     iVisitante := FVisita;
     SetComplete;
end;

function TdmServerVisitantes.GetTotales(Empresa: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          Result := OpenSQL( Empresa, GetScript( eGetTotalesAutos ), True );
     end;
     SetComplete;
end;

function TdmServerVisitantes.GetLibroActivoFoto(Empresa: OleVariant; Corte: Integer): OleVariant;
var
   sFiltro:string;
begin
     with oZetaProvider do
     begin
          sFiltro := Format( '( LI_ENT_COR = %0:d ) or ( LI_SAL_COR = %0:d )', [ Corte ] );
          Result := OpenSQL( Empresa, Format( GetScript( eGetLibrosFoto ) , [ sFiltro ] ), True );
     end;
     SetComplete;
end;

function TdmServerVisitantes.GetSQL(Empresa: OleVariant; OPC: Integer; const Filtro: WideString): OleVariant;
begin
     with oZetaProvider do
     begin
         Result := OpenSQL( Empresa, getScriptOpc(OPC, filtro), True );
     end;
     SetComplete;
end;


function TdmServerVisitantes.GetCatalogosVisitantes(
  Empresa: OleVariant; Catalogo: Integer; const Pista: WideString;
  out MaxVisit: WideString): OleVariant;
const
     K_FILTROS_VISITANTE = K_CAMPOS_VISITANTE + ' ,' + K_PRETTY_VISIT;
//     K_CONDICION_BUSQUEDA = ' EV_NUMERO = ' + filtro;
//     GET_CATALOGOS_ESP = 'select %s from %s WHERE (VI_NOMBRES LIKE '%%s%') or (VI_NUMERO LIKE '%%s%') or (EV_NUMERO LIKE '%%s%')';
     GET_CATALOGOS_ESP = 'select %s from %s WHERE ' +
              '(VI_NOMBRES LIKE %s) or ' +
              '(VI_NUMERO LIKE %s) or ' +
              '(VI_APE_PAT LIKE %s) or ' +
              '(VI_APE_MAT LIKE %s) or ' +
              '(VI_STATUS LIKE %s) or ' +
              '(VI_NACION LIKE %s) or ' +
              '(VI_MIGRA LIKE %s) or ' +
              '(EV_NUMERO LIKE %s)';

var
   sqlText: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FVisita:= GetMaxCodigo( eMaxVisita )
     end;
     MaxVisit := IntToStr(  FVisita );
     sqlText := Format( GET_CATALOGOS_ESP, [ K_FILTROS_VISITANTE, 'VISITA', Pista, Pista, Pista, Pista, Pista, Pista, Pista, Pista] );
     Result := oZetaProvider.OpenSQL( Empresa, sqlText, True );
     SetComplete;
end;

 {$ifndef DOS_CAPAS}

initialization
  TComponentFactory.Create(ComServer, TdmServerVisitantes, Class_dmServerVisitantes, ciMultiInstance, ZetaServerTools.GetThreadingModel);
{$endif}
end.
