unit DPoliza;

{$define QUINCENALES}
{$INCLUDE DEFINES.INC}
interface

uses
    Windows, SysUtils, Classes, DB,
    Variants,
    ZCreator,
    ZAgenteSQLClient,
    ZSuperSQL,
    ZetaSQlBroker,
    ZEvaluador,
    ZetaTipoEntidad,
    ZetaCommonLists,
    ZetaCommonClasses,
    DSuperReporte,
    DZetaServerProvider,
    ZetaServerDataSet, DBClient;

const
     K_GRUPO_NULO = 'X';
     K_POLIZA_CARGO = 'C';
     K_POLIZA_ABONO = 'A';
     K_MAX_GRUPOS = 4;
     K_ANCHO_CAR_ABO = 1;
     K_ANCHO_COMENTARIO = 30;
     K_ANCHO_CUENTA = 30;

     K_DISPLAY_FORMAT = '$#,0.00;-$#,0.00';
     CargoAbono: array[0..1] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( K_POLIZA_CARGO, K_POLIZA_ABONO );
     K_ANCHO_CUENTA_PCONCEPTO = 255;


type
    TdmPoliza = class(TObject)
     private
        oZetaProvider: TdmZetaServerProvider;
        oZetaCreator: TZetaCreator;
        SQLBroker,DetalleBroker, NominaBroker: TSQLBroker;
        PolizaEval : TZetaEvaluador;
        FMovimien,FDetalle,FDetalleTotal: TZetaCursor;
        FInsertPoliza, {FUpdatePoliza, }FPolizaHead{, FPolizaExiste} : TZetaCursor;
        FNominaDataset : TServerDataSet;
        FRes : TQREvResult;
        Filtros: OleVariant;

        eTipoPoliza : TipoEntidad;
        eStatusPol : eStatusPoliza;
        iReporte, iMaxGrupos, iFolio: Integer;
        FListaParametros: string;
        sRangoNominas, sCuenta, sTipoPoliza: String;
        rMonto, rCargos, rTotCar, rAbonos, rTotAbo: Double;
        sTabla  : String;
        cdsLista: TServerDataSet;
        FUsarHorasNomina: Boolean;
        FGlobalUsarHorasNomina: Boolean;

        procedure InitVariables;
        procedure SetParametrosPoliza(Dataset: TZetaCursor);
        procedure PreparaPolizaHead;
        procedure DespreparaPolizaHead;
        procedure SetEntidad( iEntidad: integer );
        procedure PreparaPoliza;Virtual;abstract;


        procedure AgregaBrokerDatosPeriodo;
        procedure AgregaBrokerFiltros(const i: integer; oBroker: TSQLBroker);
        procedure BorraPoliza(const sTipoPoliza: string);

     protected
        procedure RegistraFunciones;virtual;
        procedure PolizaDatasetDetalle;virtual;abstract;
        procedure PolizaDatasetNomina;virtual;abstract;
        function GeneraPoliza: Boolean;virtual;abstract;
        function GetSQLScript(const iScript: Integer): String;
        function GetColumnaTotal: string;
        procedure AbreQuerys(const iEmpleado: integer);virtual;
        procedure AbreQueryDetalle(oDetalle: TZetaCursor;const iEmpleado: integer; const lHayFechas: Boolean = TRUE);
     public
        constructor Create( oProvider: TdmZetaServerProvider; oCreator: TZetaCreator);
        destructor Destroy; override;
        function Calcula( oFiltros: OleVariant; const sParametros: string ): OleVariant;
    end;

    TGrupoStr = String[ 20 ];

    TPolizaGrupo = class(TdmPoliza)
     private
       FDataset: TZetaCursor;
       aGrupos: array[ 0..K_MAX_GRUPOS ] of TGrupoStr;
       procedure AgregaTemporalNomina(oDataset: TServerDataset; const iConcepto: Integer; const rTotMonto: Double);
       procedure AgregaBrokerGrupo(const i: integer; oBroker: TSQLBroker);
       function CargaGruposPoliza: integer;
       function IsNotNull(oField: TField; const iPos: integer): Boolean;
     protected
       procedure RegistraFunciones;override;
       procedure PreparaPoliza;override;
       procedure PolizaDatasetDetalle;override;
       procedure PolizaDatasetNomina;override;
       function GeneraPoliza:Boolean;override;
       procedure AbreQuerys(const iEmpleado : integer);override;

     public
       destructor Destroy;override;
    end;

    TPolizaConcepto = class(TdmPoliza)
     private
       FLongitud : integer;
       FDataset,cdsPoliza: TServerDataset;
       function ProrrateaMonto( const rMonto: TPesos ):TPesos;
       //function ExisteCuenta( const sCuenta,sCargoAbono: string ): Boolean;
       procedure AgregaColumnas(Broker: TSQLBroker);
       function FiltroMovimien: string;
     protected
       procedure RegistraFunciones;override;
       procedure PreparaPoliza;override;
       procedure PolizaDatasetDetalle;override;
       procedure PolizaDatasetNomina;override;
       function GeneraPoliza:Boolean;override;
       procedure AbreQuerys(const iEmpleado : integer);override;

     public
       destructor Destroy;override;
    end;

implementation

uses ZToolsPe,
     ZetaCommonTools;

{ TPoliza }

const
     Q_POLIZA_BORRAR_TEMPORAL = 0;
     Q_POLIZA_BORRAR_NOMINA = 1;
     Q_POLIZA_REPORTE = 2;
     Q_POLIZA_REPORTE_CONCEPTO = 3;
     Q_POLIZA_INSERT = 4;
     Q_POLIZA_UPDATE = 5;
     Q_POLIZA_EXISTE = 6;
     Q_POLIZA_HEAD_EXISTE = 7;
     Q_POLIZA_HEAD_INSERT = 8;
     Q_POLIZA_HEAD_UPDATE = 9;


constructor TdmPoliza.Create(oProvider: TdmZetaServerProvider; oCreator: TZetaCreator);
begin
     oZetaProvider := oProvider;
     oZetaCreator := oCreator;
     cdsLista := TServerDataSet.Create(nil);
end;

destructor TdmPoliza.Destroy;
begin
     FreeAndNil(cdsLista);
     {oZetaCreator.SuperReporte := NIL;
     {FreeAndNil(SQLBroker);
     FreeAndNil(DetalleBroker);
     FreeAndNil(NominaBroker);
     FreeAndNil(PolizaEval);}

     {FreeAndNil( FMovimien );
     FreeAndNil( FDetalle );
     FreeAndNil( FDetalleTotal );
     FreeAndNil( FPolizaExiste );
     FreeAndNil( FUpdatePoliza );
     FreeAndNil( FInsertPoliza );
     FreeAndNil( FPolizaHead );}
end;



function TdmPoliza.Calcula( oFiltros: OleVariant; const sParametros: string ): OleVariant;
begin
     PolizaEval := TZetaEvaluador.Create( oZetaCreator );
     SQLBroker := TSQLBroker.Create( oZetaCreator );
     NominaBroker := TSQLBroker.Create( oZetaCreator );
     DetalleBroker := TSQLBroker.Create( oZetaCreator );
     Filtros := oFiltros;
     FListaParametros := sParametros;
     RegistraFunciones;
     InitVariables;
     try
        with oZetaProvider do
        begin
             InitGlobales;
             FUsarHorasNomina := FALSE;
             FGlobalUsarHorasNomina := oZetaProvider.OK_ProyectoEspecial( pePolizaCardinal );
             BorraPoliza(sTipoPoliza);
             PreparaPolizaHead;

             PreparaPoliza;
             try
                if eTipoPoliza = enNomina then PolizaDatasetNomina
                else PolizaDatasetDetalle;
                if GeneraPoliza then
                begin
                     with Log do
                     begin
                          if ( HayErrores ) then
                             eStatusPol := spError;
                          Evento( clbNinguno, 0, Date(), 'Suma Total de Cargos ' + FormatFloat( K_DISPLAY_FORMAT, rTotCar ) );
                          Evento( clbNinguno, 0, Date(), 'Suma Total de Abonos ' + FormatFloat( K_DISPLAY_FORMAT, rTotAbo ) );
                     end;
                     Result := CloseProcess;
                end;
             except
                   On Error:Exception do
                   begin
                        eStatusPol := spError;

                        if ( Log = NIL ) then
                           OpenProcess( prNOPolizaContable, 0, FListaParametros );

                        if ( Log <> NIL ) then
                        begin

                             Log.ErrorGrave( 0, Error.Message );
                             Result := CloseProcess;
                        end;
                   end;
             end;
        end;
     finally
            DespreparaPolizaHead;
     end;
end;

function TdmPoliza.GetSQLScript( const iScript: Integer ): String;
begin
     case iScript of
          Q_POLIZA_BORRAR_TEMPORAL: Result := 'delete from TMPTOTAL where ( US_CODIGO = %d )';
          Q_POLIZA_BORRAR_NOMINA: Result := 'delete from POLIZA where '+
                                            '( PE_YEAR = %d ) and '+
                                            '( PE_TIPO = %d ) and '+
                                            '( PE_NUMERO = %d ) and '+
                                            '( PT_CODIGO = %s )';
          Q_POLIZA_REPORTE: Result := 'select CR_TITULO, CR_FORMULA, CR_MASCARA, CR_OPER, RE_ENTIDAD, CR_DESCRIP, CR_ANCHO '+
                                      'from CAMPOREP  '+
                                      'LEFT OUTER JOIN REPORTE on REPORTE.RE_CODIGO = CAMPOREP.RE_CODIGO '+
                                      'where ( CAMPOREP.RE_CODIGO = :Reporte ) and ( CR_TIPO = :Tipo ) '+
                                      'order by CR_POSICIO';
          {$IFDEF PRORRATEO100}
          Q_POLIZA_REPORTE_CONCEPTO: Result := 'select CR_TITULO, CR_FORMULA, CR_MASCARA, CR_OPER, RE_ENTIDAD, CR_DESCRIP, CR_SHOW, CR_ANCHO, CR_CALC CO_NUMERO, '+
                                      'RE_HOJA RE_LONGITUD, 0 POSICION '+
                                      {$ifdef DEFECTO_FILTROS_POLIZA_DETALLE}
                                      ', RE_FILTRO, QU_FILTRO ' +
                                      {$endif}
                                      'from CAMPOREP  '+
                                      'LEFT OUTER JOIN REPORTE on REPORTE.RE_CODIGO = CAMPOREP.RE_CODIGO '+
                                      {$ifdef DEFECTO_FILTROS_POLIZA_DETALLE}
                                      'LEFT OUTER JOIN QUERYS ON QUERYS.QU_CODIGO = REPORTE.QU_CODIGO '+
                                      {$endif}
                                      'where ( CAMPOREP.RE_CODIGO = %d ) and ( CR_TIPO = %d ) '+
                                      'and (CAMPOREP.CR_FORMULA <> '''' ) '+
                                      'order by CR_CALC,CR_OPER';
          {$ELSE}
          Q_POLIZA_REPORTE_CONCEPTO: Result := 'select CR_TITULO, CR_FORMULA, CR_MASCARA, CR_OPER, RE_ENTIDAD, CR_DESCRIP, CR_SHOW, CR_ANCHO, CR_CALC CO_NUMERO, '+
                                      'RE_HOJA RE_LONGITUD, 0 POSICION '+
                                      {$ifdef DEFECTO_FILTROS_POLIZA_DETALLE}
                                      ', RE_FILTRO, QU_FILTRO ' +
                                      {$endif}
                                      'from CAMPOREP  '+
                                      'LEFT OUTER JOIN REPORTE on REPORTE.RE_CODIGO = CAMPOREP.RE_CODIGO '+
                                      {$ifdef DEFECTO_FILTROS_POLIZA_DETALLE}
                                      'LEFT OUTER JOIN QUERYS ON QUERYS.QU_CODIGO = REPORTE.QU_CODIGO '+
                                      {$endif}
                                      'where ( CAMPOREP.RE_CODIGO = %d ) and ( CR_TIPO = %d ) '+
                                      'and (CAMPOREP.CR_FORMULA <> '''' ) '+
                                      'order by CR_CALC';
          {$ENDIF}

          Q_POLIZA_INSERT: Result := 'insert into POLIZA ( '+
                                     'PE_YEAR, '+
                                     'PE_TIPO, '+
                                     'PE_NUMERO, '+
                                     'PT_CODIGO, '+
                                     'TP_FOLIO, '+
                                     'TP_CUENTA, '+
                                     'TP_CARGO, '+
                                     'TP_ABONO, '+
                                     'TP_CAR_ABO, '+
                                     'TP_COMENTA, '+
                                     'TP_TEXTO, '+
                                     'TP_NUMERO ) values ('+
                                     ':Year, '+
                                     ':Tipo, '+
                                     ':Numero, '+
                                     ':TipoPoliza, '+
                                     ':Folio, '+
                                     ':Cuenta, '+
                                     ':Cargo, '+
                                     ':Abono, '+
                                     ':CargoAbono, '+
                                     ':Comentario, '+
                                     ':Texto, '+
                                     ':Numadicional )';

          Q_POLIZA_UPDATE: Result := 'UPDATE POLIZA SET '+
                                     'TP_CARGO = TP_CARGO + :Cargo, '+
                                     'TP_ABONO = TP_ABONO + :Abono '+
                                     'WHERE PE_YEAR = :Year and '+
                                     'PE_TIPO = :Tipo and '+
                                     'PE_NUMERO = :Numero and '+
                                     'PT_CODIGO = :TipoPoliza and '+
                                     'TP_CUENTA = :Cuenta and ' +
                                     'TP_CAR_ABO = :CargoAbono';

          Q_POLIZA_EXISTE: Result := 'SELECT COUNT(*) CUANTOS FROM POLIZA ' +
                                     'WHERE PE_YEAR = :Year and '+
                                     'PE_TIPO = :Tipo and '+
                                     'PE_NUMERO = :Numero and '+
                                     'PT_CODIGO = :TipoPoliza and '+
                                     'TP_CUENTA = :Cuenta and ' +
                                     'TP_CAR_ABO = :CargoAbono';


          Q_POLIZA_HEAD_EXISTE: Result := 'select * from POL_HEAD where '+
                                          '( PE_YEAR = :Year ) and '+
                                          '( PE_TIPO = :Tipo ) and '+
                                          '( PE_NUMERO = :Numero ) and ' +
                                          '( PT_CODIGO = :TipoPoliza )';
          Q_POLIZA_HEAD_INSERT: Result := 'INSERT INTO POL_HEAD ( '+
                                          'PE_YEAR, '+
                                          'PE_TIPO, '+
                                          'PE_NUMERO, '+
                                          'PT_CODIGO ) '+
                                          'VALUES ( '+
                                          ':Year, '+
                                          ':Tipo, '+
                                          ':Numero, '+
                                          ':TipoPoliza )';

          Q_POLIZA_HEAD_UPDATE: Result := 'UPDATE POL_HEAD set '+
                                          'US_CODIGO = :Usuario, '+
                                          'PH_FECHA = :Fecha, '+
                                          'PH_HORA  = :Hora, '+
                                          'PH_VECES = PH_VECES + 1, '+
                                          'PH_REPORTE = :Reporte, '+
                                          'PH_STATUS = :Status '+
                                          'where PE_YEAR = :Year '+
                                          'and PE_TIPO = :Tipo '+
                                          'and PE_NUMERO = :Numero '+
                                          'and PT_CODIGO = :TipoPoliza ';
          else
              Result := VACIO;
     end;
end;

procedure TdmPoliza.RegistraFunciones;
begin
     oZetaCreator.RegistraFunciones([efGenerales]);
end;

procedure TdmPoliza.InitVariables;
begin
     rCargos := 0;
     rTotCar := 0;
     rAbonos := 0;
     rTotAbo := 0;
     rMonto := 0;

     iFolio := 1;
     eStatusPol := spNormal;

     with oZetaProvider.ParamList do
     begin
          sTipoPoliza := ParamByName( 'CodigoTipo' ).AsString;
          iReporte := ParamByName( 'CodigoReporte' ).AsInteger;
          sRangoNominas := ParamByName('Numero').AsString;
          sRangoNominas := ConcatString( sRangoNominas, ParamByName('RangoNominas').AsString, ',' );
     end;
end;


procedure TdmPoliza.BorraPoliza( const sTipoPoliza: string );
begin
     with oZetaProvider do
     begin
          EjecutaAndFree( Format( GetSQLScript( Q_POLIZA_BORRAR_TEMPORAL ), [ UsuarioActivo ] ) );
          with DatosPeriodo do
               EjecutaAndFree( Format( GetSQLScript( Q_POLIZA_BORRAR_NOMINA ), [ Year, Ord( Tipo ), Numero, EntreComillas(sTipoPoliza) ] ) );
     end;
end;

procedure TdmPoliza.AgregaBrokerDatosPeriodo;
begin
     with oZetaProvider.DatosPeriodo do
     begin
          with SQLBroker.Agente do
          begin
               AgregaFiltro( Format('MOVIMIEN.PE_YEAR = %d',[Year]), TRUE, Entidad );
               AgregaFiltro( Format('MOVIMIEN.PE_TIPO = %d',[Ord(Tipo)]), TRUE, Entidad );
               AgregaFiltro( Format('MOVIMIEN.PE_NUMERO in ( %s )',[sRangoNominas]), TRUE, Entidad );
          end;

          with NominaBroker.Agente do
          begin
               AgregaFiltro( Format('NOMINA.PE_YEAR = %d',[Year]), TRUE, Entidad );
               AgregaFiltro( Format('NOMINA.PE_TIPO = %d',[Ord(Tipo)]), TRUE, Entidad );
               AgregaFiltro( Format('NOMINA.PE_NUMERO in ( %s )',[sRangoNominas]), TRUE, Entidad );
          end;


     end;
end;

procedure TdmPoliza.AgregaBrokerFiltros( const i: integer; oBroker : TSQLBroker );
begin
     with oBroker.Agente do
     begin
          AgregaFiltro( Filtros[ i ][ 1 ], Filtros[ i ][ 2 ], TipoEntidad(Filtros[i][3]) );
     end;
end;

procedure TdmPoliza.SetParametrosPoliza( Dataset:TZetaCursor );
begin
     with oZetaProvider do
     begin
          with DatosPeriodo do
           begin
                ParamAsInteger( Dataset, 'Year', Year );
                ParamAsInteger( Dataset, 'Tipo', Ord( Tipo ));
                ParamAsInteger( Dataset, 'Numero', Numero );
                ParamAsString( Dataset, 'TipoPoliza', sTipoPoliza );
           end;
     end;
end;

procedure TdmPoliza.PreparaPolizaHead;
begin
     with oZetaProvider do
     begin
          FPolizaHead := CreateQuery(GetSQLScript( Q_POLIZA_HEAD_EXISTE ) );
          SetParametrosPoliza( FPolizaHead );

          FPolizaHead.Active := TRUE;

          if FPolizaHead.IsEmpty then
          begin
               FPolizaHead.Active := FALSE;
               PreparaQuery( FPolizaHead, GetSQLScript( Q_POLIZA_HEAD_INSERT ) );
               SetParametrosPoliza( FPolizaHead );
               Ejecuta(FPolizaHead);
          end;

          FInsertPoliza := CreateQuery( GetSQLScript( Q_POLIZA_INSERT ) );
          SetParametrosPoliza(FInsertPoliza);

          {FUpdatePoliza := CreateQuery( GetSQLScript( Q_POLIZA_UPDATE ) );
          SetParametrosPoliza(FUpdatePoliza);

          FPolizaExiste:= CreateQuery( GetSQLScript( Q_POLIZA_EXISTE ) );
          SetParametrosPoliza(FPolizaExiste);}
     end;
end;

procedure TdmPoliza.DespreparaPolizaHead;
begin
     with oZetaProvider do
     begin
          PreparaQuery(FPolizaHead, GetSQLScript( Q_POLIZA_HEAD_UPDATE ) );
          SetParametrosPoliza( FPolizaHead );

          //Parametros
          ParamAsInteger(FPolizaHead, 'Usuario', UsuarioActivo );
          ParamAsDate(FPolizaHead, 'Fecha', Date );
          ParamAsString(FPolizaHead, 'Hora', FormatDateTime( 'hhnn', Time ) );
          ParamAsInteger(FPolizaHead, 'Reporte', iReporte );
          ParamAsInteger(FPolizaHead, 'Status', Ord( eStatusPol ) );

          Ejecuta(FPolizaHead);
     end;
end;

procedure TdmPoliza.SetEntidad(iEntidad: integer);
begin
     eTipoPoliza := TipoEntidad( iEntidad );

     if not ( eTipoPoliza in TipoPoliza ) then
        eTipoPoliza := enNomina;
end;

function TdmPoliza.GetColumnaTotal : string;
begin
     if eTipoPoliza = enAusencia then
        Result := 'AUSENCIA.AU_HORAS'
     else Result := 'WORKS.WK_TIEMPO'
end;

procedure TdmPoliza.AbreQueryDetalle(oDetalle: TZetaCursor;const iEmpleado : integer; const lHayFechas: Boolean );
begin
     if ( oDetalle <> NIL ) then
     begin
          with oZetaProvider do
          begin
               oDetalle.Active := FALSE;
               {$ifdef DOS_CAPAS}
               oDetalle.ParamByName('EMPLEADO').AsInteger := iEmpleado;
               if lHayFechas then
               begin
                    oDetalle.ParamByName('FECHAINI').AsDateTime := DatosPeriodo.InicioAsis;
                    oDetalle.ParamByName('FECHAFIN').AsDateTime := DatosPeriodo.FinAsis;
               end;
               {$else}
               ParamAsInteger(oDetalle,'EMPLEADO',iEmpleado );
               if lHayFechas then
               begin
                    ParamAsDate(oDetalle,'FECHAINI', DatosPeriodo.InicioAsis );
                    ParamAsDate(oDetalle,'FECHAFIN', DatosPeriodo.FinAsis );
               end;
               {$endif}
               oDetalle.Active := TRUE;
          end;
     end;
end;

procedure TdmPoliza.AbreQuerys(const iEmpleado : integer);
begin
     with oZetaProvider do
     begin
          AbreQueryDetalle( FDetalle, iEmpleado );
     end;
end;


{ TPolizaConcepto }
function TPolizaConCepto.ProrrateaMonto( const rMonto: TPesos ):TPesos;
begin
     //Este es el prorrateo para polizas de Asistencia y Labor
     if ( FDetalleTotal.FieldByName('TOT_TIEMPO').AsFloat <> 0 ) then
        Result := rMonto * (FDetalle.FieldByName('TOT_TIEMPO').AsFloat / FDetalleTotal.FieldByName('TOT_TIEMPO').AsFloat)
     else
         Result := 0;
end;

{function TPolizaConcepto.ExisteCuenta( const sCuenta,sCargoAbono: string ): Boolean;
begin
     with oZetaProvider do
     begin
          FPolizaExiste.Active := FALSE;
          ParamAsString( FPolizaExiste, 'Cuenta', sCuenta );
          ParamAsString( FPolizaExiste, 'CargoAbono', sCargoAbono );
          FPolizaExiste.Active := TRUE;
          Result := ( FPolizaExiste.Fields[0].AsInteger <> 0 )
     end;
end;}

{$ifdef PRORRATEO100}
function TPolizaConcepto.GeneraPoliza:Boolean;
 var
    i, iConcepto, iEmpleado, rProrrateo: integer;
    rMontoTotal,rMontoAcum   : TPesos;
    sCargoAbono: string;
    oColumna : TSQLColumna;
    FListaProrrateo: TStrings;
 procedure MensajeError( const sMensaje : string );
 begin
      raise Exception.Create( sMensaje + CR_LF +
                              Format( 'Cuenta : %s, Empleado: %d, Concepto: %d %s F�rmula: %s ',
                              [ sCuenta, iEmpleado, iConcepto, CR_LF, FDataset.FieldByName('CR_FORMULA').AsString] ) );

 end;
begin
     Result := FALSE;
     FListaProrrateo:= TStringList.Create;
     try

        if ( eTipoPoliza = enNomina ) then
           PolizaEval.AgregaDataset(FMovimien)
        else
            PolizaEval.AgregaDataset(FDetalle);

        with oZetaProvider, NominaBroker.SuperReporte.DataSetReporte do
        begin
             if OpenProcess( prNOPolizaContable, RecordCount + FDataSet.RecordCount, FListaParametros ) then
             begin
                  try
                     FDataSet.First;
                     while NOT FDataSet.EOF do
                     begin
                          if ( eTipoPoliza = enNomina ) then
                             oColumna := SQLBroker.Agente.GetColumna(FDataSet.FieldByName('POSICION').AsInteger)
                          else
                              oColumna := DetalleBroker.Agente.GetColumna(FDataSet.FieldByName('POSICION').AsInteger);
                          if ( oColumna <> NIL ) then
                          begin
                               FDataSet.Edit;
                               FDataSet.FieldByName('CR_FORMULA').AsString := oColumna.Formula;
                               FDataSet.Post;
                          end;
                          CanContinue;
                          FDataSet.Next;
                     end;

                     while not Eof and CanContinue do
                     begin
                          iEmpleado := FieldByName('CB_CODIGO').AsInteger;

                          AbreQuerys( iEmpleado );

                          if (eTipoPoliza = enNomina) or
                             {$ifdef DOS_CAPAS}
                             ( FDetalle.RecordCount <> 0 )
                             {$ELSE}
                             ( NOT (FDetalle.BOF and FDetalle.EOF) )
                             {$endif}
                             then
                          begin
                               while NOT FMovimien.EOF do
                               begin
                                    iConcepto := FMovimien.FieldByName( 'CO_NUMERO' ).AsInteger;
                                    rMontoTotal := FMovimien.FieldByName('TOT_MONTO').AsFloat;
                                    rMonto := rMontoTotal;

                                    for i:= 0 to High( CargoAbono ) do
                                    begin
                                         rProrrateo := 0;
                                         rMontoAcum := 0;

                                         FDataset.Filtered := FALSE;
                                         FDataset.Filter := Format( 'CO_NUMERO = %d AND CR_OPER=%d', [iConcepto,i] );
                                         FDataset.Filtered := TRUE;
                                         FDataset.First;

                                         if ( FDataSet.RecordCount > 0 ) then
                                         begin
                                              while not FDataSet.EOF do
                                              begin
                                                   repeat

                                                         if ( FDataset.FieldByName( 'CR_SHOW' ).AsInteger <> 0 ) and
                                                            ( FDataset.FieldByName( 'CR_SHOW' ).AsInteger <> 100 ) then
                                                         begin

                                                              rProrrateo := rProrrateo + FDataset.FieldByName( 'CR_SHOW' ).AsInteger;

                                                              if NOT FDataset.EOF and (FDataset.recNo < FDataSet.RecordCount){and ( sCargoAbonoAnterior = sCargoAbono ) } then
                                                              begin
                                                                   rMonto := Redondea( rMontoTotal * (FDataset.FieldByName( 'CR_SHOW' ).AsInteger/100.0) );
                                                                   rMontoAcum := rMontoAcum + rMonto

                                                              end
                                                              else
                                                              begin
                                                                   if ( rProrrateo = 100 ) then
                                                                      rMonto := Redondea( rMontoTotal - rMontoAcum )
                                                                   else
                                                                   begin
                                                                        rMonto := Redondea( rMontoTotal * (FDataset.FieldByName( 'CR_SHOW' ).AsInteger/100.0) );
                                                                        if FListaProrrateo.IndexOF( IntToStr(iConcepto) ) < 0 then
                                                                           FListaProrrateo.AddObject( IntToStr(iConcepto), TObject( rProrrateo ) );
                                                                   end;

                                                              end;
                                                         end
                                                         else
                                                             rMonto := rMontoTotal;

                                                         if ( eTipoPoliza <> enNomina ) then
                                                         begin
                                                              rMonto := ProrrateaMonto(rMonto);
                                                         end;


                                                         FRes := PolizaEval.Calculate( FDataset.FieldByName('CR_FORMULA').AsString );
                                                         with FRes do
                                                         begin
                                                              if ( Kind = resString ) then
                                                              begin
                                                                   sCuenta := strResult;
                                                                   if ( FLongitud > 0 ) then
                                                                   begin
                                                                        if ( Length( sCuenta ) < FLongitud ) then
                                                                           MensajeError( 'Longitud de la Cuenta es mayor a la longitud especificada. ' );
                                                                   end;

                                                              end
                                                              else if (Kind = resError ) then
                                                                   MensajeError( 'Error en la f�rmula de la Cuenta: ' + FRes.StrResult )
                                                              else
                                                                  MensajeError( 'Formula NO es Tipo Texto' );
                                                         end;

                                                         if StrLleno( sCuenta ) then
                                                         begin
                                                              sCargoAbono := CargoAbono[ FDataset.FieldByName( 'CR_OPER' ).AsInteger ];

                                                              if cdsPoliza.Locate('TP_CUENTA;TP_CAR_ABO',  VarArrayOf([sCuenta,sCargoAbono]), [] ) then
                                                              begin
                                                                   cdsPoliza.Edit;
                                                              end
                                                              else
                                                              begin
                                                                   cdsPoliza.Insert;
                                                                   cdsPoliza.FieldByName('TP_CUENTA').AsString := sCuenta;
                                                                   cdsPoliza.FieldByName('TP_CAR_ABO').AsString := sCargoAbono;
                                                                   cdsPoliza.FieldByName('TP_COMENTA').AsString :=  FDataset.FieldByName( 'CR_MASCARA' ).AsString;
                                                                   cdsPoliza.FieldByName('TP_TEXTO').AsString := FDataset.FieldByName( 'CR_DESCRIP' ).AsString;
                                                                   cdsPoliza.FieldByName('TP_NUMERO').AsInteger :=  FDataset.FieldByName( 'CR_ANCHO' ).AsInteger ;
                                                              end;


                                                              if ( sCargoAbono = K_POLIZA_CARGO ) then
                                                              begin
                                                                   cdsPoliza.FieldByName('TP_CARGO').AsFloat := cdsPoliza.FieldByName('TP_CARGO').AsFloat + rMonto;
                                                                   rCargos := rCargos + rMonto;
                                                                   rTotCar := rTotCar + rMonto;
                                                              end
                                                              else
                                                              begin
                                                                   cdsPoliza.FieldByName('TP_ABONO').AsFloat := cdsPoliza.FieldByName('TP_ABONO').AsFloat + rMonto;
                                                                   rAbonos := rAbonos + rMonto;
                                                                   rTotAbo := rTotAbo + rMonto;
                                                              end;
                                                              cdsPoliza.Post;
                                                         end;

                                                         oZetaCreator.PolizaCuadre := ( rTotCar - rTotAbo );

                                                         if (eTipoPoliza <> enNomina) then
                                                            if NOT FDetalle.EOF then
                                                               FDetalle.Next;

                                                   until ((eTipoPoliza = enNomina) or (FDetalle.Eof));


                                                   FDataset.Next;


                                                   if (eTipoPoliza <> enNomina) then
                                                      FDetalle.First;
                                              end;
                                         end;//IF
                                    end; //for
                                    FMovimien.Next;
                               end;
                          end;
                          Next;
                     end;

                     with cdsPoliza do
                     begin
                          IndexFieldNames := 'TP_CUENTA;TP_CAR_ABO';
                          First;
                          while NOT EOF do
                          begin
                               ParamAsInteger( FInsertPoliza, 'Folio', iFolio );
                               ParamAsVarChar( FInsertPoliza, 'Cuenta', FieldByName('TP_CUENTA').AsString, K_ANCHO_CUENTA_PCONCEPTO );
                               ParamAsChar( FInsertPoliza, 'CargoAbono', FieldByName('TP_CAR_ABO' ).AsString, K_ANCHO_CAR_ABO );
                               ParamAsVarChar( FInsertPoliza, 'Comentario', FieldByName( 'TP_COMENTA' ).AsString, K_ANCHO_COMENTARIO );
                               ParamAsVarChar( FInsertPoliza, 'Texto', FieldByName( 'TP_TEXTO' ).AsString, 10 );
                               ParamAsInteger( FInsertPoliza, 'Numadicional', FieldByName( 'TP_NUMERO' ).AsInteger );
                               ParamAsFloat( FInsertPoliza, 'Cargo', FieldByName('TP_CARGO').AsFloat );
                               ParamAsFloat( FInsertPoliza, 'Abono', FieldByName('TP_ABONO').AsFloat );
                               EmpiezaTransaccion;
                               Ejecuta(FInsertPoliza);
                               TerminaTransaccion( True );

                               iFolio := iFolio + 1;
                               Next;
                          end;
                     end;


                     for i := 0 to FListaProrrateo.Count -1 do
                     begin
                          Log.Advertencia( 0, Format( 'Suma del Prorrateo no da 100%s : Concepto %s', ['%', FListaProrrateo[i]] ),
                                              Format( 'La suma del Prorrateo da %d%s en el Concepto #%s' , [Integer( FListaProrrateo.Objects[i]), '%', FListaProrrateo[i]] ) );
                     end;


                  except
                        on Error: Exception do
                           Log.Excepcion( 0, 'Error Al Generar Poliza Contable ', Error, DescripcionParams );
                  end;
                  Result := True;
             end;
        end;
     finally
            FreeAndNil( FListaProrrateo );

     end;
end;
{$ELSE}
function TPolizaConcepto.GeneraPoliza:Boolean;
 var
    iConcepto, iEmpleado: integer;
    //FPOliza : TZetaCursor;
    rMontoTotal : TPesos;
    sCargoAbono: string;
    oColumna : TSQLColumna;
 procedure MensajeError( const sMensaje : string );
 begin
      raise Exception.Create( sMensaje + CR_LF +
                              Format( 'Cuenta : %s, Empleado: %d, Concepto: %d %s F�rmula: %s ',
                              [ sCuenta, iEmpleado, iConcepto, CR_LF, FDataset.FieldByName('CR_FORMULA').AsString] ) );

 end;
begin
     Result := FALSE;
     if ( eTipoPoliza = enNomina ) then
        PolizaEval.AgregaDataset(FMovimien)
     else
         PolizaEval.AgregaDataset(FDetalle);

     with oZetaProvider, NominaBroker.SuperReporte.DataSetReporte do
     begin
          if OpenProcess( prNOPolizaContable, RecordCount + FDataSet.RecordCount, FListaParametros ) then
          begin
               try

                  FDataSet.First;
                  while NOT FDataSet.EOF do
                  begin
                       if ( eTipoPoliza = enNomina ) then
                          oColumna := SQLBroker.Agente.GetColumna(FDataSet.FieldByName('POSICION').AsInteger)
                       else
                           oColumna := DetalleBroker.Agente.GetColumna(FDataSet.FieldByName('POSICION').AsInteger);
                       if ( oColumna <> NIL ) then
                       begin
                            FDataSet.Edit;
                            FDataSet.FieldByName('CR_FORMULA').AsString := oColumna.Formula;
                            FDataSet.Post;
                       end;
                       CanContinue;
                       FDataSet.Next;
                  end;

                  while not Eof and CanContinue do
                  begin
                       iEmpleado := FieldByName('CB_CODIGO').AsInteger;

                       AbreQuerys( iEmpleado );

                       if (eTipoPoliza = enNomina) or
                          {$ifdef DOS_CAPAS}
                          ( FDetalle.RecordCount <> 0 )
                          {$ELSE}
                          ( NOT (FDetalle.BOF and FDetalle.EOF) )
                          {$endif}
                          then
                       begin
                            while NOT FMovimien.EOF do
                            begin
                                 iConcepto := FMovimien.FieldByName( 'CO_NUMERO' ).AsInteger;
                                 rMontoTotal := FMovimien.FieldByName('TOT_MONTO').AsFloat;
                                 rMonto := rMontoTotal;

                                 FDataset.Filtered := FALSE;
                                 FDataset.Filter := Format( 'CO_NUMERO = %d', [iConcepto] );
                                 FDataset.Filtered := TRUE;
                                 FDataset.First;

                                 if ( FDataSet.RecordCount > 0 ) then
                                 begin
                                      while not FDataSet.EOF do
                                      begin
                                           repeat
                                                 if ( FDataset.FieldByName( 'CR_SHOW' ).AsInteger <> 0 ) then
                                                    rMonto := rMontoTotal * (FDataset.FieldByName( 'CR_SHOW' ).AsInteger/100.0)
                                                 else
                                                     rMonto := rMontoTotal;

                                                 if ( eTipoPoliza <> enNomina ) then
                                                 begin
                                                      rMonto := ProrrateaMonto(rMonto);
                                                 end;


                                                 FRes := PolizaEval.Calculate( FDataset.FieldByName('CR_FORMULA').AsString );
                                                 with FRes do
                                                 begin
                                                      if ( Kind = resString ) then
                                                      begin
                                                           sCuenta := strResult;
                                                           if ( FLongitud > 0 ) then
                                                           begin
                                                                if ( Length( sCuenta ) < FLongitud ) then
                                                                   MensajeError( 'Longitud de la Cuenta es mayor a la longitud especificada. ' );
                                                           end;

                                                      end
                                                      else if (Kind = resError ) then
                                                           MensajeError( 'Error en la f�rmula de la Cuenta: ' + FRes.StrResult )
                                                      else
                                                          MensajeError( 'Formula NO es Tipo Texto' );
                                                 end;

                                                 if StrLleno( sCuenta ) then
                                                 begin
                                                      sCargoAbono := CargoAbono[ FDataset.FieldByName( 'CR_OPER' ).AsInteger ];

                                                      if cdsPoliza.Locate('TP_CUENTA;TP_CAR_ABO',  VarArrayOf([sCuenta,sCargoAbono]), [] ) then
                                                      begin
                                                           cdsPoliza.Edit;
                                                      end
                                                      else
                                                      begin
                                                           cdsPoliza.Insert;
                                                           cdsPoliza.FieldByName('TP_CUENTA').AsString := sCuenta;
                                                           cdsPoliza.FieldByName('TP_CAR_ABO').AsString := sCargoAbono;
                                                           cdsPoliza.FieldByName('TP_COMENTA').AsString :=  FDataset.FieldByName( 'CR_MASCARA' ).AsString;
                                                           cdsPoliza.FieldByName('TP_TEXTO').AsString := FDataset.FieldByName( 'CR_DESCRIP' ).AsString;
                                                           cdsPoliza.FieldByName('TP_NUMERO').AsInteger :=  FDataset.FieldByName( 'CR_ANCHO' ).AsInteger ;
                                                      end;

                                                      if ( sCargoAbono = K_POLIZA_CARGO ) then
                                                      begin
                                                           cdsPoliza.FieldByName('TP_CARGO').AsFloat := cdsPoliza.FieldByName('TP_CARGO').AsFloat + rMonto;
                                                           rCargos := rCargos + rMonto;
                                                           rTotCar := rTotCar + rMonto;
                                                      end
                                                      else
                                                      begin
                                                           cdsPoliza.FieldByName('TP_ABONO').AsFloat := cdsPoliza.FieldByName('TP_ABONO').AsFloat + rMonto;
                                                           rAbonos := rAbonos + rMonto;
                                                           rTotAbo := rTotAbo + rMonto;
                                                      end;
                                                      cdsPoliza.Post;
                                                 end;

                                                 oZetaCreator.PolizaCuadre := ( rTotCar - rTotAbo );

                                                 if (eTipoPoliza <> enNomina) then
                                                    if NOT FDetalle.EOF then
                                                       FDetalle.Next;

                                           until ((eTipoPoliza = enNomina) or (FDetalle.Eof));

                                           FDataset.Next;

                                           if (eTipoPoliza <> enNomina) then
                                              FDetalle.First;
                                      end;
                                 end;//IF
                                 FMovimien.Next;
                            end;
                       end;
                       Next;
                  end;

                  with cdsPoliza do
                  begin
                       IndexFieldNames := 'TP_CUENTA;TP_CAR_ABO';
                       First;
                       while NOT EOF do
                       begin
                            ParamAsInteger( FInsertPoliza, 'Folio', iFolio );
                            ParamAsVarChar( FInsertPoliza, 'Cuenta', FieldByName('TP_CUENTA').AsString, K_ANCHO_CUENTA_PCONCEPTO );
                            ParamAsChar( FInsertPoliza, 'CargoAbono', FieldByName('TP_CAR_ABO' ).AsString, K_ANCHO_CAR_ABO );
                            ParamAsVarChar( FInsertPoliza, 'Comentario', FieldByName( 'TP_COMENTA' ).AsString, K_ANCHO_COMENTARIO );
                            ParamAsVarChar( FInsertPoliza, 'Texto', FieldByName( 'TP_TEXTO' ).AsString, 10 );
                            ParamAsInteger( FInsertPoliza, 'Numadicional', FieldByName( 'TP_NUMERO' ).AsInteger );
                            ParamAsFloat( FInsertPoliza, 'Cargo', FieldByName('TP_CARGO').AsFloat );
                            ParamAsFloat( FInsertPoliza, 'Abono', FieldByName('TP_ABONO').AsFloat );
                            EmpiezaTransaccion;
                            Ejecuta(FInsertPoliza);
                            TerminaTransaccion( True );

                            iFolio := iFolio + 1;
                            Next;
                       end;
                  end;

               except
                     on Error: Exception do
                        Log.Excepcion( 0, 'Error Al Generar Poliza Contable ', Error, DescripcionParams );
               end;
               Result := True;
          end;
     end;
end;
{$ENDIF}

procedure TPolizaConcepto.PolizaDatasetDetalle;
 var
    sCampo, sTabla: string;
{$ifdef DEFECTO_FILTROS_POLIZA_DETALLE}
    i: integer;
{$endif}
begin
     sCampo := GetColumnaTotal;

     case eTipoPoliza of
          enAusencia: sTabla := 'AUSENCIA';
          enWorks: sTabla := 'WORKS'
     else
         sTabla := 'NOMINA'
     end;

     PolizaDatasetNomina;

     with DetalleBroker do
     begin
          if FGlobalUsarHorasNomina and FUsarHorasNomina then
          begin
               sTabla := 'NOMINA';
               sCampo := 'NOMINA.NO_HORAS+NOMINA.NO_EXTRAS';
               Init( enNomina );

               with Agente do
               begin
                    AgregaColumna( 'SUM('+sCampo+')',TRUE, Entidad, tgFloat, 0, 'TOT_TIEMPO' );
                    AgregaFiltro( sTabla+'.CB_CODIGO = :EMPLEADO', TRUE, Entidad );
                    AgregaFiltro( sCampo+'>0', TRUE, Entidad );
                    //AgregaFiltro( sTabla+'.AU_FECHA BETWEEN :FECHAINI AND :FECHAFIN', TRUE, Entidad );

                    with oZetaProvider.DatosPeriodo do
                    begin
                         AgregaFiltro( Format('NOMINA.PE_YEAR = %d',[Year]), TRUE, Entidad );
                         AgregaFiltro( Format('NOMINA.PE_TIPO = %d',[Ord(Tipo)]), TRUE, Entidad );
                         AgregaFiltro( Format('NOMINA.PE_NUMERO in ( %s )',[sRangoNominas]), TRUE, Entidad );
                    end;
               end;
          end
          else
          begin
               Init( eTipoPoliza );
               with Agente do
               begin
                    AgregaColumna( 'SUM('+sCampo+')',TRUE, Entidad, tgFloat, 0, 'TOT_TIEMPO' );
                    AgregaFiltro( sTabla+'.CB_CODIGO = :EMPLEADO', TRUE, Entidad );
                    AgregaFiltro( sCampo+'>0', TRUE, Entidad );
                    AgregaFiltro( sTabla+'.AU_FECHA BETWEEN :FECHAINI AND :FECHAFIN', TRUE, Entidad );
                    {$ifdef DEFECTO_FILTROS_POLIZA_DETALLE}
                    if ( eTipoPoliza <> enNomina ) then
                       AgregaRangoCondicion( oZetaProvider.ParamList );
                    {$else}
                       AgregaRangoCondicion( oZetaProvider.ParamList );
                    {$endif}
               end;
          end;

          if SuperSQL.Construye(  Agente ) then
          begin
               {$ifdef CAROLINA}
               Agente.SQL.SaveTofile('d:\temp\FDetalleTotal.sql');
               {$ENDIF}

               FDetalleTotal := oZetaProvider.CreateQuery( Agente.SQL.Text )
          end
          else
          begin
               raise Exception.Create( 'Calculando P�liza, error al calcular Totales de Asistencia Diaria.' + CR_LF +
                                       Agente.ListaErrores.Text );
          end;
     end;

     with DetalleBroker do
     begin
          Init( eTipoPoliza );

          with Agente do
          begin
               if FGlobalUsarHorasNomina and FUsarHorasNomina then
               begin
                    sCampo := '(WORKS.WK_HRS_ORD + WORKS.WK_HRS_2EX + WORKS.WK_HRS_3EX)/60';
                    sTabla := 'WORKS';
                    AgregaColumna( sCampo,TRUE, Entidad, tgFloat, 0, 'TOT_TIEMPO' );
                    AgregaColumna( 'AU_FECHA',TRUE, Entidad, tgFecha, 0, 'AU_FECHA' );
                    AgregaColumnas( DetalleBroker );
                    AgregaFiltro( sTabla+'.CB_CODIGO = :EMPLEADO', TRUE, Entidad );
                    AgregaFiltro( sCampo+'>0', TRUE, Entidad );
                    AgregaFiltro( sTabla+'.AU_FECHA BETWEEN :FECHAINI AND :FECHAFIN', TRUE, Entidad );
                    {$ifdef DEFECTO_FILTROS_POLIZA_DETALLE}
                    if ( eTipoPoliza <> enNomina ) then
                       AgregaRangoCondicion( oZetaProvider.ParamList );
                    {$else}
                       AgregaRangoCondicion( oZetaProvider.ParamList );
                    {$endif}
               end
               else
               begin
                    AgregaColumna( sCampo,TRUE, Entidad, tgFloat, 0, 'TOT_TIEMPO' );
                    AgregaColumna( 'AU_FECHA',TRUE, Entidad, tgFecha, 0, 'AU_FECHA' );
                    AgregaColumnas( DetalleBroker );
                    AgregaFiltro( sTabla+'.CB_CODIGO = :EMPLEADO', TRUE, Entidad );
                    AgregaFiltro( sCampo+'>0', TRUE, Entidad );
                    AgregaFiltro( sTabla+'.AU_FECHA BETWEEN :FECHAINI AND :FECHAFIN', TRUE, Entidad );
                    {$ifdef DEFECTO_FILTROS_POLIZA_DETALLE}
                    if ( eTipoPoliza <> enNomina ) then
                       AgregaRangoCondicion( oZetaProvider.ParamList );
                    {$else}
                       AgregaRangoCondicion( oZetaProvider.ParamList );
                    {$endif}
               end;

          end;

          {$ifdef DEFECTO_FILTROS_POLIZA_DETALLE}
          if ( eTipoPoliza <> enNomina ) then
          begin
               if NOT VarIsNull( Filtros ) then
               for i := VarArrayLowBound( Filtros, 1 ) to VarArrayHighBound( Filtros, 1 ) do
               begin
                    AgregaBrokerFiltros(i, DetalleBroker );
               end;
          end;
          {$ENDIF}

          if SuperSQL.Construye(  Agente ) then
          begin
               {$ifdef CAROLINA}
               Agente.SQL.SaveTofile('d:\temp\FDetalle.sql');
               {$ENDIF}
               FDetalle := oZetaProvider.CreateQuery( Agente.SQL.Text )

          end
          else
          begin
               raise Exception.Create( 'Error al Calcular P�liza Contable de Asistencia Diaria.' + CR_LF +
                                       Agente.ListaErrores.Text );
          end;
     end;



end;

procedure TPolizaConcepto.AbreQuerys(const iEmpleado : integer);
 var lHayFechas: Boolean;
begin
     inherited;
     FMovimien.Active := FALSE;
     oZetaProvider.ParamAsInteger(FMovimien,'EMPLEADO',iEmpleado);
     FMovimien.Active := TRUE;

     if FGlobalUsarHorasNomina then
        lHayFechas := NOT( ( eTipoPoliza = enWorks ) and FUsarHorasNomina  )
     else
         lHayFechas := TRUE;

     AbreQueryDetalle( FDetalleTotal, iEmpleado, lHayFechas );
end;

function TPolizaConcepto.FiltroMovimien: string;
begin
     Result := Format( 'MOVIMIEN.CO_NUMERO IN ( select DISTINCT( CR_CALC ) from CAMPOREP ' +
                       ' where RE_CODIGO = %d ' +
                       ' and CR_TIPO = 0 '+
                       ' and CR_FORMULA <> '''' )', [iReporte] );
end;

procedure TPolizaConcepto.PolizaDatasetNomina;
 var
    i: integer;
begin
     with SQLBroker do
     begin
          Init( enMovimien );
          with Agente do
          begin
               if ( eTipoPoliza = enNomina ) then
                  AgregaColumnas( SQLBroker );
               AgregaColumna( 'MOVIMIEN.CO_NUMERO', TRUE, Entidad, tgNUmero, 0, 'CO_NUMERO' );
               AgregaColumna( 'MOVIMIEN.MO_PERCEPC + MOVIMIEN.MO_DEDUCCI', TRUE, Entidad, tgFloat, 0, 'TOT_MONTO' );
               AgregaColumna( 'MOVIMIEN.CB_CODIGO', TRUE, Entidad, tgNUmero, 0, 'CB_CODIGO' );
               AgregaFiltro( 'MOVIMIEN.CB_CODIGO = :Empleado', TRUE, Entidad );
               AgregaFiltro( 'MOVIMIEN.MO_ACTIVO = ''S''', TRUE, Entidad );
               AgregaFiltro( 'MOVIMIEN.MO_PERCEPC + MOVIMIEN.MO_DEDUCCI <> 0', TRUE, Entidad );
               AgregaFiltro( FiltroMovimien,TRUE, Entidad );
               {$ifdef DEFECTO_FILTROS_POLIZA_DETALLE}
               if ( eTipoPoliza = enNomina ) then
                  AgregaRangoCondicion( oZetaProvider.ParamList );
               {$else}
                  AgregaRangoCondicion( oZetaProvider.ParamList );
               {$endif}
          end;
     end;

     with NominaBroker do
     begin
          Init( enNomina );
          with Agente do
          begin
               AgregaColumna( 'DISTINCT(NOMINA.CB_CODIGO)',TRUE, Entidad, tgFloat, 0, 'CB_CODIGO' );
          end;
          {$ifdef DEFECTO_FILTROS_POLIZA_DETALLE}
          if ( eTipoPoliza = enNomina ) then
             AgregaRangoCondicion( oZetaProvider.ParamList );
          {$else}
             AgregaRangoCondicion( oZetaProvider.ParamList );
          {$endif}
     end;

     AgregaBrokerDatosPeriodo;

     if ( eTipoPoliza = enNomina ) then
     begin
          if NOT VarIsNull( Filtros ) then
             for i := VarArrayLowBound( Filtros, 1 ) to VarArrayHighBound( Filtros, 1 ) do
             begin
                  AgregaBrokerFiltros(i, SQLBroker );
                  AgregaBrokerFiltros(i, NominaBroker );
             end;
     end;

     //SQLBroker.BuildDataset( oZetaProvider.EmpresaActiva );
     with SQLBroker do
          if SuperSQL.Construye(  Agente ) then
          begin
               {$ifdef CAROLINA}
               Agente.SQL.SaveTofile('d:\temp\Movimien.sql');
               {$ENDIF}

               FMovimien := oZetaProvider.CreateQuery(Agente.SQL.Text);
          end
          else
          begin
               raise Exception.Create( 'Error al Calcular P�liza de Movimientos.' + CR_LF +
                                       Agente.ListaErrores.Text);
          end;
     NominaBroker.BuildDataset( oZetaProvider.EmpresaActiva );
     oZetaCreator.SuperReporte := SQLBroker.SuperReporte;
     // FALTA VALIDAR SI FALLO EL BuildDataSet.
     // EN MSSQL inluye CB_CODIGO en SELECT y no en el GROUP BY y marca error al abrir
end;

procedure TPolizaConcepto.PreparaPoliza;
begin
     with oZetaProvider do
     begin
          FDataset:= TServerDataset.Create(oZetaProvider);
          FDataSet.Data := OpenSQL(EmpresaActiva, Format( GetSQlScript(Q_POLIZA_REPORTE_CONCEPTO),[ iReporte, Ord( tcCampos )] ), True );
          if FGlobalUsarHorasNomina then
          begin
               with FDataset do
               begin
                    ParamList.AddString('Filtro', FieldByName('RE_FILTRO').AsString );
                    ParamList.AddString('Condicion', FieldByName('QU_FILTRO').AsString );
               end;
          end;
     end;

     cdsPoliza := TServerDataset.Create(oZetaProvider);
     with cdsPoliza do
     begin
          InitTempDataset;
          AddStringField( 'TP_CUENTA' , K_ANCHO_CUENTA_PCONCEPTO );
          AddFloatField( 'TP_CARGO' );
          AddFloatField( 'TP_ABONO' );
          AddStringField( 'TP_COMENTA', 30 );
          AddStringField( 'TP_CAR_ABO', 1 );
          AddStringField( 'TP_TEXTO', 10 );
          AddIntegerField( 'TP_NUMERO' );
          CreateTempDataset;
     end;

     FLongitud := FDataset.FieldByName('RE_LONGITUD').AsInteger;

     SetEntidad(FDataset.FieldByName('RE_ENTIDAD').AsInteger);
     case eTipoPoliza  of
          enNomina : PolizaEval.Entidad := enMovimien;
          enAusencia : PolizaEval.Entidad := enAusencia
          else
              PolizaEval.Entidad := enWorks;
     end;

     oZetaProvider.InitGlobales;
     if  FGlobalUsarHorasNomina then
     begin
          if FDataset.Active and ( FDataset.RecordCount > 0 ) then
          begin
               FUsarHorasNomina := (eTipoPOliza = enWorks );
          end;
     end
end;

procedure TPolizaConcepto.RegistraFunciones;
begin
     oZetaCreator.RegistraFunciones([efGenerales,efTress,efPolizaGral]);
end;

procedure TPolizaConcepto.AgregaColumnas( Broker: TSQLBroker );
begin
     with FDataSet do
     begin
          while NOT EOF do
          begin
               Edit;
               with Broker.Agente do
                    FieldByName('POSICION').AsInteger := AgregaColumna( FieldByName('CR_FORMULA').AsString, FALSE, Entidad, tgTexto, 255 );
               Next;
          end;
          First;
     end;
end;


destructor TPolizaConcepto.Destroy;
begin
     FreeAndNil( FDataSet );
     inherited;
end;


{ TPolizaGrupo }

procedure TPolizaGrupo.PreparaPoliza;
begin
     FDataSet := oZetaProvider.CreateQuery( GetSQLScript( Q_POLIZA_REPORTE ) );

     PolizaEval.Entidad := enNomina;

     with oZetaProvider do
     begin
          ParamAsInteger( FDataset, 'Reporte', iReporte );
          ParamAsInteger( FDataset, 'Tipo', Ord( tcOrden ) );
     end;

     FDataset.Active := True;

     iMaxGrupos := CargaGruposPoliza;
     with oZetaProvider do
     begin
          ParamList.AddInteger('MaxGrupos',iMaxGrupos+1);

          with FDataset do
          begin
               Active:= FALSE;
               ParamAsInteger( FDataset, 'Reporte', iReporte );
               ParamAsInteger( FDataset, 'Tipo', Ord( tcCampos ) );
               Active := True;
          end;
     end;
end;

procedure TPolizaGrupo.RegistraFunciones;
begin
     oZetaCreator.RegistraFunciones([efPoliza,efGenerales]);
end;

function TPolizaGrupo.IsNotNull(oField : TField; const iPos : integer ): Boolean;
begin
     with oField do
          Result := NOT (IsNull or (AsString = '')) ;
          if NOT Result then
             raise Exception.Create( Format( 'El Empleado #%s Tiene Vac�o el Campo %s',
                                              [FNominaDataSet.FieldByName('CB_CODIGO').AsString,
                                               Copy( aGrupos[iPos], Pos('.',aGrupos[iPos])+1, 20 ) ]) +CR_LF+
                                               'Revisar los Datos de Clasificaci�n de la N�mina. ')

end;

function TPolizaGrupo.CargaGruposPoliza: integer;
var
   i: Integer;
   sFormula: String;
begin
     with FDataset do
     begin
          SetEntidad(FDataset.FieldByName('RE_ENTIDAD').AsInteger);

          case eTipoPoliza of
               enAusencia: sTabla := 'AUSENCIA';
               enWorks: sTabla := 'WORKS'
          else
              sTabla := 'NOMINA'
          end;
          i := 0;
          while not Eof and ( i <= K_MAX_GRUPOS ) do
          begin
               sFormula := FieldByName( 'CR_FORMULA' ).AsString;
               if ( Pos( '.', sFormula ) = 0 ) then
                  sFormula := sTabla + '.' + sFormula;
               aGrupos[ i ] := sFormula;
               i := i + 1;
               Next;
          end;
          Result := i - 1;
          while ( i <= K_MAX_GRUPOS ) do
          begin
               aGrupos[ i ] := K_GRUPO_NULO;
               i := i + 1;
          end;
          Active := False;
     end;
end;

procedure TPolizaGrupo.PolizaDatasetNomina;
 var i: integer;
begin
     with SQLBroker do
     begin
          Init( enMovimien );
          with Agente do
          begin
               //AgregaColumna( IntToStr(iUsuario), TRUE, Entidad, tgNumero, 0, 'US_CODIGO' );
               AgregaColumna( 'MOVIMIEN.CO_NUMERO', TRUE, Entidad, tgNUmero, 0, 'CO_NUMERO' );
               AgregaColumna( 'SUM(MOVIMIEN.MO_PERCEPC + MOVIMIEN.MO_DEDUCCI)', TRUE, Entidad, tgFloat, 0, 'TOT_MONTO' );
               AgregaFiltro( 'MOVIMIEN.MO_ACTIVO = ''S''', TRUE, Entidad );
               AgregaGrupo( 'MOVIMIEN.CO_NUMERO', TRUE, Entidad );
               AgregaRangoCondicion( oZetaProvider.ParamList );
          end;
     end;
     with NominaBroker do
     begin
          Init( enNomina );
          with Agente do
          begin
               AgregaColumna( 'SUM(NOMINA.NO_PERCEPC)',TRUE, Entidad, tgFloat, 0, 'NO_PERCEPC' );
               AgregaColumna( 'SUM(NOMINA.NO_DEDUCCI)',TRUE, Entidad, tgFloat, 0, 'NO_DEDUCCI' );
               AgregaColumna( 'SUM(NOMINA.NO_NETO)',TRUE, Entidad, tgFloat, 0, 'NO_NETO' );
          end;
          AgregaRangoCondicion( oZetaProvider.ParamList );
     end;

     AgregaBrokerDatosPeriodo;

     if NOT VarIsNull( Filtros ) then
        for i := VarArrayLowBound( Filtros, 1 ) to VarArrayHighBound( Filtros, 1 ) do
        begin
             AgregaBrokerFiltros(i, SQLBroker );
             AgregaBrokerFiltros(i, NominaBroker );
        end;

     for i := 0 to iMaxGrupos do
     begin
          AgregaBrokerGrupo( i, SQLBroker );
          AgregaBrokerGrupo( i, NominaBroker );
     end;

     SQLBroker.BuildDataset( oZetaProvider.EmpresaActiva );
     oZetaCreator.SuperReporte := SQLBroker.SuperReporte;
     // FALTA VALIDAR SI FALLO EL BuildDataSet.
     // EN MSSQL inluye CB_CODIGO en SELECT y no en el GROUP BY y marca error al abrir
     NominaBroker.BuildDataset( oZetaProvider.EmpresaActiva );
     FNominaDataset := NominaBroker.SuperReporte.DataSetReporte;

     with FNominaDataSet do
     begin
          while not Eof do
          begin
               with SQLBroker.SuperReporte do
               begin
                    AgregaTemporalNomina( DatasetReporte, 1000, FieldByName( 'NO_PERCEPC' ).AsFloat );
                    AgregaTemporalNomina( DatasetReporte, 1001, FieldByName( 'NO_DEDUCCI' ).AsFloat );
                    AgregaTemporalNomina( DatasetReporte, 1002, FieldByName( 'NO_NETO' ).AsFloat );
               end;
               Next;
          end;
     end;
end;

procedure TPolizaGrupo.PolizaDatasetDetalle;
 var
    iCuantosDetalle : integer;
    //FDetalle : TZetaCursorLocate;
    FSuma,FResta,FProporcion : TPesos;
    procedure CreatecdsLista;
     var i: integer;
    begin
         with cdsLista do
         begin
              InitTempDataSet;
              AddIntegerField( 'CO_NUMERO' );
              AddFloatField( 'TOT_MONTO' );
              for i := 0 to iMaxGrupos do
                  AddStringField( 'GRUPO' + IntToStr(i+1), 20 );
              CreateTempDataset;
         end;
    end;
    procedure CopiaTFields(const nFactor : TPesos);
     var i: integer;
         sGrupo : string;
     function GetGrupo : string;
     begin
          if (FDetalle.FieldByName( sGrupo ).IsNull or (FDetalle.FieldByName( sGrupo ).AsString = '')) then
          begin
               if IsNotNull(FNominaDataSet.FieldByName( sGrupo ),i ) then
                  Result := FNominaDataSet.FieldByName( sGrupo ).AsString;
          end
          else Result := FDetalle.FieldByName( sGrupo ).AsString;
     end;
    begin
         if nFactor <> 0 then
         begin
              i:=1;
              with cdsLista do
              begin
                   Append;
                   FieldByName( 'CO_NUMERO' ).Value := FMovimien.FieldByName( 'CO_NUMERO' ).Value;
                   FieldByName( 'TOT_MONTO' ).Value := nFactor;
                   {if iMaxGrupos = 0 then
                   begin
                        sGrupo := 'GRUPO1';
                        FieldByName( 'GRUPO1' ).AsString := GetGrupo
                   end
                   else}
                       for i:=0 to iMaxGrupos do
                       begin
                            sGrupo := 'GRUPO' + IntToStr(i+1);
                            FieldByName( sGrupo ).AsString := GetGrupo;
                       end;
                   Post;
              end;
         end;
    end;


    var
       i: integer;
       sCampo: String;
begin
     sCampo := GetColumnaTotal;
     with DetalleBroker do
     begin
          Init( eTipoPoliza );
          with Agente do
          begin
               AgregaColumna( 'SUM('+sCampo+')',TRUE, Entidad, tgFloat, 0, 'TOT_TIEMPO' );
               AgregaGrupo( sTabla+'.CB_CODIGO', TRUE, Entidad );
               AgregaOrden( sTabla+'.CB_CODIGO', TRUE, Entidad );
               AgregaFiltro( sTabla+'.CB_CODIGO = :EMPLEADO', TRUE, Entidad );
               AgregaFiltro( sCampo+'>0', TRUE, Entidad );
               AgregaFiltro( sTabla+'.AU_FECHA BETWEEN :FECHAINI AND :FECHAFIN', TRUE, Entidad );
               AgregaRangoCondicion( oZetaProvider.ParamList );
          end;
     end;

     with SQLBroker do
     begin
          Init( enMovimien );
          with Agente do
          begin
               AgregaColumna( 'MOVIMIEN.CO_NUMERO', TRUE, Entidad, tgNUmero, 0, 'CO_NUMERO' );
               AgregaColumna( 'SUM(MOVIMIEN.MO_PERCEPC + MOVIMIEN.MO_DEDUCCI)', TRUE, Entidad, tgFloat, 0, 'TOT_MONTO' );
               AgregaFiltro( 'MOVIMIEN.MO_ACTIVO = ''S''', TRUE, Entidad );
               AgregaFiltro( 'MOVIMIEN.CB_CODIGO = :EMPLEADO', TRUE, Entidad );
               AgregaGrupo( 'MOVIMIEN.CO_NUMERO', TRUE, Entidad );
          end;
     end;

     with NominaBroker do
     begin
          Init( enNomina );
          with Agente do
          begin
               AgregaColumna( 'NOMINA.CB_CODIGO',TRUE, Entidad, tgFloat, 0, 'CB_CODIGO' );
               AgregaColumna( 'SUM(NOMINA.NO_PERCEPC)',TRUE, Entidad, tgFloat, 0, 'NO_PERCEPC' );
               AgregaColumna( 'SUM(NOMINA.NO_DEDUCCI)',TRUE, Entidad, tgFloat, 0, 'NO_DEDUCCI' );
               AgregaColumna( 'SUM(NOMINA.NO_NETO)',TRUE, Entidad, tgFloat, 0, 'NO_NETO' );
               AgregaGrupo( 'NOMINA.CB_CODIGO', TRUE, Entidad );
          end;
          AgregaRangoCondicion( oZetaProvider.ParamList );
     end;

     AgregaBrokerDatosPeriodo;


     if NOT VarIsNull( Filtros ) then
        for i := VarArrayLowBound( Filtros, 1 ) to VarArrayHighBound( Filtros, 1 ) do
        begin
             AgregaBrokerFiltros(i, NominaBroker);
             AgregaBrokerFiltros(i, DetalleBroker);
        end;
     for i := 0 to iMaxGrupos do
     begin
          AgregaBrokerGrupo( i, NominaBroker );
          AgregaBrokerGrupo( i, DetalleBroker );
     end;
     with SQLBroker do
          if not SuperSQL.Construye(  Agente ) then
          begin
               raise Exception.Create( 'Error al crear Broker de Movimientos.' + CR_LF +
                                       Agente.ListaErrores.Text);
          end;
     with DetalleBroker do
          if not SuperSQL.Construye(  Agente ) then
          begin
               raise Exception.Create( 'Error al crear Broker de Detalle.' + CR_LF +
                                       Agente.ListaErrores.Text);
          end;

     NominaBroker.BuildDataset( oZetaProvider.EmpresaActiva );
     FNominaDataset := NominaBroker.SuperReporte.DatasetReporte;
     CreatecdsLista;

     FMovimien := oZetaProvider.CreateQuery( SQLBroker.Agente.SQL.Text );
     FDetalle := oZetaProvider.CreateQuery;
     oZetaProvider.PreparaQuery(FDetalle,DetalleBroker.Agente.SQL.Text );

     with FNominaDataSet do
     begin
          while not Eof do
          begin
               AgregaTemporalNomina( cdsLista, 1000, FieldByName( 'NO_PERCEPC' ).AsFloat );
               AgregaTemporalNomina( cdsLista, 1001, FieldByName( 'NO_DEDUCCI' ).AsFloat );
               AgregaTemporalNomina( cdsLista, 1002, FieldByName( 'NO_NETO' ).AsFloat );

               AbreQuerys(FieldByName('CB_CODIGO').AsInteger);

               FSuma := 0;
               iCuantosDetalle := 0;
               while NOT FDetalle.EOF do
               begin
                    Inc(iCuantosDetalle);
                    FSuma := FSuma + FDetalle.FieldByName('TOT_TIEMPO').AsFloat;
                    FDetalle.Next;
               end;
               if FDetalle.Active then
               begin
                    FDetalle.First;
                    if iCuantosDetalle <= 1 then {Quiere decir que el empleado nada mas tuvo un solo grupo en toda el rango de fechas}
                    begin
                         {if iCuantosDetalle = 0 then oFields := FNominaDataset.Fields
                         else oFields := FDetalle.Fields ;}

                         while NOT FMovimien.EOF do
                         begin
                              CopiaTFields(FMovimien.FieldByName('TOT_MONTO').AsFloat );
                              FMovimien.Next;
                         end;
                    end
                    else
                    begin
                         while NOT FMovimien.EOF do
                         begin
                              FResta := 0;
                              FDetalle.First;
                              while NOT FDetalle.EOF do
                              begin
                                   FProporcion := Redondea(FMovimien.FieldByName('TOT_MONTO').AsFloat * (FDetalle.FieldByName('TOT_TIEMPO').AsFloat/FSuma));
                                   if FDetalle.RecNo < iCuantosDetalle then
                                   begin
                                        FResta := FProporcion + FResta;
                                        CopiaTFields(FProporcion);
                                   end;
                                   FDetalle.Next;
                              end;

                              CopiaTFields(FMovimien.FieldByName('TOT_MONTO').AsFloat- FResta);
                              FMovimien.Next;
                         end;
                    end;
               end;
               Next;
          end;
     end;

     {*-*-*-*-*-*-*-*-*-*-*-*-}
     SQLBroker.SuperReporte.DataSetReporte.Lista := cdsLista.Data;
     oZetaCreator.SuperReporte := SQLBroker.SuperReporte;
     {*-*-*-*-*-*-*-*-*-*-*-*-}
end;

function TPolizaGrupo.GeneraPoliza: Boolean;
begin
     Result := FALSE;
     with oZetaProvider, FDataset do
          if OpenProcess( prNOPolizaContable, RecordCount, FListaParametros ) then
          begin
               EmpiezaTransaccion;
               while not Eof and CanContinue do
               begin
                    try
                       sCuenta := FieldByName( 'CR_TITULO' ).AsString;
                       FRes := PolizaEval.Calculate(FieldByName('CR_FORMULA').AsString);
                       with FRes do
                       begin
                            case Kind of
                                 resDouble: if ( resFecha ) then
                                               raise Exception.Create( 'Formula NO Num�rica. Cuenta: ' + sCuenta )
                                            else
                                                rMonto := Redondea( dblResult );
                                 resInt: rMonto := intResult;
                            else
                                raise Exception.Create( 'Error en la Cuenta: ' + sCuenta + CR_LF +
                                                        FRes.StrResult );
                            end;
                       end;

                       ParamAsInteger( FInsertPoliza, 'Folio', iFolio );
                       ParamAsVarChar( FInsertPoliza, 'Cuenta', sCuenta, K_ANCHO_CUENTA );
                       if ( FieldByName( 'CR_OPER' ).AsInteger = 0 ) then
                       begin
                            ParamAsFloat( FInsertPoliza, 'Cargo', rMonto );
                            ParamAsFloat( FInsertPoliza, 'Abono', 0 );
                            ParamAsChar( FInsertPoliza, 'CargoAbono', K_POLIZA_CARGO, K_ANCHO_CAR_ABO );
                            rCargos := rCargos + rMonto;
                            rTotCar := rTotCar + rMonto;
                       end
                       else
                       begin
                            ParamAsFloat( FInsertPoliza, 'Cargo', 0 );
                            ParamAsFloat( FInsertPoliza, 'Abono', rMonto );
                            ParamAsChar( FInsertPoliza, 'CargoAbono', K_POLIZA_ABONO, K_ANCHO_CAR_ABO );
                            rAbonos := rAbonos + rMonto;
                            rTotAbo := rTotAbo + rMonto;
                       end;
                       ParamAsVarChar( FInsertPoliza, 'Comentario', FieldByName( 'CR_MASCARA' ).AsString, K_ANCHO_COMENTARIO );
                       ParamAsVarChar( FInsertPoliza, 'Texto', FieldByName( 'CR_DESCRIP' ).AsString, 10 );
                       ParamAsInteger( FInsertPoliza, 'Numadicional', FieldByName( 'CR_ANCHO' ).AsInteger );
                       Ejecuta( FInsertPoliza );
                       iFolio := iFolio + 1;
                       oZetaCreator.PolizaCuadre := ( rTotCar - rTotAbo );
                    except
                          on Error: Exception do
                          begin
                               Log.Excepcion( 0, 'Error Al Generar Poliza Contable ', Error, DescripcionParams );
                          end;
                    end;
                    Next;
               end;
               Result := True;
               TerminaTransaccion( True );
          end;
end;

procedure TPolizaGrupo.AgregaTemporalNomina( oDataset : TServerDataset; const iConcepto: Integer;  const rTotMonto: Double );
var
   j: Integer;
   sGrupo: String;
begin
     with oDataset do
     begin
          Append;
          for j := 0 to iMaxGrupos do
          begin
               sGrupo := 'GRUPO'+IntToStr(j+1);
               FieldByName(sGrupo).AsString := FNominaDataset.FieldByName(sGrupo).AsString;
          end;

          FieldByName('CO_NUMERO' ).AsInteger := iConcepto;
          FieldByName('TOT_MONTO' ).AsFloat := rTotMonto;
          Post;
     end;
end;

procedure TPolizaGrupo.AgregaBrokerGrupo( const i: integer; oBroker: TSQLBroker );
 function Transforma( const sGrupo : string ) : string;
 begin
      Result := sGrupo;
      if oBroker.Agente.Entidad = enNomina then
      begin
           if eTipoPoliza = enWorks then
           begin
                if Pos('AREA.CB_NIVEL',Result) > 0 then
                   Result := StrTransform(Result,'AREA','NOMINA')
                else if Pos('WORKS.CB_PUESTO',Result) > 0 then
                   Result := StrTransform(Result,'WORKS','NOMINA');
           end;

           if Pos('AUSENCIA.CB_NIVEL',Result) > 0 then
              Result := StrTransform(Result,'AUSENCIA','NOMINA')
           else if (Pos('AUSENCIA.CB_PUESTO',Result) > 0)
                or (Pos('AUSENCIA.CB_TURNO',Result) > 0)
                or (Pos('AUSENCIA.CB_CLASIFI',Result) > 0) then
                   Result := StrTransform(Result,'AUSENCIA','NOMINA');
      end;
 end;
begin
     with oBroker.Agente do
     begin
          AgregaColumna( Transforma(aGrupos[i]), TRUE, Entidad, tgTexto, 50, 'GRUPO' + IntToStr(i+1) );
          AgregaGrupo( Transforma(aGrupos[i]), TRUE, enNinguno );
     end;
end;


destructor TPolizaGrupo.Destroy;
begin
     FreeAndNil( FDataSet );
     inherited;
end;

procedure TPolizaGrupo.AbreQuerys(const iEmpleado: integer);
begin
     inherited;
     FMovimien.Active := FALSE;
     {$ifdef DOS_CAPAS}
     FMovimien.ParamByName('EMPLEADO').AsInteger := iEmpleado;
     {$else}
     oZetaProvider.ParamAsInteger(FMovimien,'EMPLEADO',iEmpleado);
     {$endif}
     FMovimien.Active := TRUE;
end;

end.
