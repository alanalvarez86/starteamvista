unit DServerPortalTress;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.7 / XE3                           ::
  :: Unidad:      DServerPortalTress.pas                     ::
  :: Descripci�n: Programa para servicios del Portal         ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

//{$define XML}
{$undef XML}

{$INCLUDE JEDI.INC}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ComServ, ComObj, VCLCom,
  StdVcl, {BdeMts,} DataBkr, DBClient, MtsRdm, Mtx, Db, {DBTables,}
  {$IFDEF DELPHIXE3_UP}vcl.imaging.Jpeg{$ELSE}Jpeg{$ENDIF}, IniFiles, Variants,
  {$IFNDEF DOS_CAPAS}
  PortalTress_TLB,
  {$ENDIF}
  {$IFDEF XML}
  ZetaXMLTools,
  {$ENDIF}
  DZetaServerProvider, ZetaCommonClasses, ZetaCommonLists, ZetaRegistryServer, ZetaServerDataSet;

type
  eBusqueda = ( ebApellido, ebNombre, ebNumero );

  TZetaPortalRegistryServer = class( TZetaRegistryServer )
  private
    function GetEmpresas: String;
    procedure SetEmpresas(const Value: String);
    { Private declarations }
  protected
    { Protected declarations }
    property Empresas: String read GetEmpresas write SetEmpresas;
  end;
  TdmPortalTress = class(TMtsDataModule {$ifndef DOS_CAPAS}, IdmPortalTress {$endif})
    cdsLista: TServerDataSet;
    cdsOutput: TServerDataSet;
    procedure MtsDataModuleCreate(Sender: TObject);
    procedure MtsDataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
    FPeriodo: TDatosPeriodo;
    FIniFile: TIniFile;
    function BuildEmpresa(DataSet: TZetaCursor): OleVariant;
    function Fix( const sValue: String): String;
    function GetComidas(Empresa: OleVariant; const iEmpleado: TNumEmp): OleVariant;
    function GetComidasTotales(Empresa: OleVariant; const iEmpleado: TNumEmp): OleVariant;
    function GetCompanyData(const sEmpresa: String; var oDatos: OleVariant): Boolean;
    function GetNominaConceptos(Empresa: OleVariant; const iEmpleado: TNumEmp ): OleVariant;
    function GetNominaDatos(Empresa: OleVariant; const iEmpleado: TNumEmp ): OleVariant;
    function GetPeriodoDatos( Empresa: OleVariant; const sScript: String ): Boolean;
    function GetPrenominaDatos(Empresa: OleVariant; const iEmpleado: TNumEmp ): OleVariant;
    function GetPrenominaTarjetas(Empresa: OleVariant; const iEmpleado: TNumEmp ): OleVariant;
    function TransformImage( const sOutput: String; Campo: TBlobField ): String;
    {$ifdef XML}
    function GetEmpleadoScript( const iEmpleado: Integer ): String;
    {$endif}
    procedure GetPeriodoAnterior(Empresa: OleVariant; const iEmpleado, iYear, iTipo, iNumero: Integer );
    procedure GetPeriodoInicial(Empresa: OleVariant; const iEmpleado, iYear, iTipo: Integer );
    procedure GetPeriodoSiguiente(Empresa: OleVariant; const iEmpleado, iYear, iTipo, iNumero: Integer );
    {$ifndef DOS_CAPAS}
    procedure ServerActivate(Sender: TObject);
    procedure ServerDeactivate(Sender: TObject);
    {$endif}
  protected
    { Protected declarations }
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
{$ifdef DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
{$endif}
    function BuscaEmpleados(const Texto: WideString; Tipo: Integer; const Archivo: WideString): WideString; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetEmpleado(const Empresa: WideString; Empleado: Integer; const Archivo: WideString): WideString; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetFoto(const Empresa: WideString; Empleado: Integer; const Archivo: WideString): WideString; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetEmpresas: WideString; {$ifndef DOS_CAPAS} safecall; {$endif}
    function SetEmpresas(const Empresas: WideString): Integer; {$ifndef DOS_CAPAS} safecall; {$endif}
    function KioskoEmpleado(Empresa: OleVariant; Empleado: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function KioskoPrestamos(Empresa: OleVariant; Empleado: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function KioskoAhorros(Empresa: OleVariant; Empleado: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function KioskoTarjeta(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime; out Checadas: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function KioskoNominaAnterior(Empresa: OleVariant; Empleado, Year, Tipo, Numero: Integer; out Conceptos: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function KioskoNominaInit(Empresa: OleVariant; Empleado, Year, Tipo: Integer; out Conceptos: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function KioskoNominaSiguiente(Empresa: OleVariant; Empleado, Year, Tipo, Numero: Integer; out Conceptos: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function KioskoPrenominaInit(Empresa: OleVariant; Empleado, Year, Tipo: Integer; out Tarjetas: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function KioskoPrenominaAnterior(Empresa: OleVariant; Empleado, Year, Tipo, Numero: Integer; out Tarjetas: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function KioskoPrenominaSiguiente(Empresa: OleVariant; Empleado, Year, Tipo, Numero: Integer; out Tarjetas: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function KioskoComidasInit(Empresa: OleVariant; Empleado, Year, Tipo: Integer; out Periodo, Totales: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function KioskoComidasAnterior(Empresa: OleVariant; Empleado, Year, Tipo, Numero: Integer; out Periodo, Totales: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function KioskoComidasSiguiente(Empresa: OleVariant; Empleado, Year, Tipo, Numero: Integer; out Periodo, Totales: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function GetEmpleado2(const Parametros: WideString): WideString; {$ifndef DOS_CAPAS} safecall;{$endif}
    function KioscoCursosTomados(Empresa: OleVariant; Empleado: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function SetClaveEmpleadoKiosko(Empresa: OleVariant; Empleado: Integer; const Clave: WideString): Integer; {$ifndef DOS_CAPAS} safecall;{$endif}
    function ReseteaPassword(Empresa, Empleados: OleVariant): Integer;{$ifndef DOS_CAPAS} safecall;{$endif}
    function EnrolDataGet(Empresa: OleVariant; Empleado: Integer): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function EnrolDataSet(Empresa: OleVariant; Empleado: Integer; Valores: OleVariant): Smallint;{$ifndef DOS_CAPAS} safecall;{$endif}
    function GetServerData: OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
end;

var
   dmPortalTress: TdmPortalTress;

implementation

uses ZetaCommonTools,
     ZetaServerTools,
     ZetaWinAPITools,
     ZGlobalTress;

const
     SERVER_EMPRESAS = 'EmpresasPortal';
     K_STR_FALSE = '';
     K_INT_FALSE = 0;
     K_INT_TRUE = 1;
     Q_NIVEL_COLABORA = 'CB_NIVEL%d';
     Q_NIVEL_DESCRIPCION = 'TB_ELEMENT%d';
     Q_NIVEL_CAMPO = ', C.CB_NIVEL%0:d, N%0:d.TB_ELEMENT as TB_ELEMENT%0:d';
     Q_NIVEL_NO_JOIN = ', C.CB_NIVEL%0:d, ''%1:s'' as TB_ELEMENT%0:d';
     Q_NIVEL_JOIN = 'left outer join NIVEL%0:d N%0:d on ( N%0:d.TB_CODIGO = C.CB_NIVEL%0:d ) ';
     Q_COMPANYS_FIND = 1;
     Q_COMPANYS_LIST = 2;
     Q_GLOBAL_NIVELES = 3;
     Q_EMPLEADO_DATA = 4;
     Q_EMPLEADO_FOTO = 5;
     Q_EMPLEADO_FIND = 6;
     Q_KIOSKO_EMPLEADO = 7;
     Q_KIOSKO_AHORROS = 8;
     Q_KIOSKO_PRESTAMOS = 9;
     Q_KIOSKO_TARJETA = 10;
     Q_KIOSKO_CHECADAS = 11;
     Q_KIOSKO_PERIODO_INICIAL = 12;
     Q_KIOSKO_PERIODO_ANTERIOR = 13;
     Q_KIOSKO_PERIODO_SIGUIENTE = 14;
     Q_KIOSKO_NOMINA = 15;
     Q_KIOSKO_CONCEPTOS = 16;
     Q_KIOSKO_PRENOMINA = 17;
     Q_KIOSKO_AUSENCIAS = 18;
     Q_KIOSKO_COMIDAS = 19;
     Q_KIOSKO_COMIDAS_TOTALES = 20;
     Q_KIOSKO_CURSOS_TOMADOS = 21;
     Q_KIOSKO_UPDATE_CLAVE = 22;

     Q_KIOSCO2_GET_BOTONES = 23;
     Q_KIOSCO2_GET_CARRUSEL = 24;
     Q_KIOSCO2_GET_ESTILO = 25;
     Q_KIOSCO2_GET_PANTALLA = 26;
     Q_KIOSCO2_GET_SHOW = 27;
     Q_KIOSCO2_GET_KIOSCOS = 28;

     Q_EMPLEADOS_ACTUALIZA_PASS = 29;
     Q_EMPLEADOS_ENROL_GET = 30;
     Q_EMPLEADOS_ENROL_SET = 31;

const
     NAVEGACION = 'NAVEGACION';
     ValoresActivos = 'valoresactivosURL';

{$R *.DFM}

function GetSQLScript( const iScript: Integer ): String;
begin
     case iScript of
          Q_COMPANYS_FIND: Result := 'select CM_NOMBRE, CM_ALIAS, CM_USRNAME, CM_PASSWRD, CM_NIVEL0, CM_DATOS from COMPANY where ( CM_CODIGO = ''%s'' )';
          Q_COMPANYS_LIST: Result := 'select CM_CODIGO, CM_NOMBRE, CM_ALIAS, CM_USRNAME, CM_PASSWRD, CM_NIVEL0, CM_DATOS from COMPANY where '+
                                     '( %s ) and ( CM_NIVEL0 = '''' ) %s order by CM_CODIGO';
          Q_GLOBAL_NIVELES: Result := 'select GL_CODIGO, GL_FORMULA from GLOBAL where ( GL_CODIGO > %d ) and ( GL_CODIGO <= %d ) order by GL_CODIGO';
          Q_EMPLEADO_DATA: Result := 'select C.CB_CODIGO, C.CB_APE_PAT, C.CB_APE_MAT, C.CB_NOMBRES, '+
                                     'C.CB_PUESTO, P.PU_DESCRIP, C.CB_TURNO, T.TU_DESCRIP, C.CB_CLASIFI, CL.TB_ELEMENT, I.IM_BLOB %s ' +
                                     'from COLABORA C '+
                                     'left outer join PUESTO P on ( P.PU_CODIGO = C.CB_PUESTO ) '+
                                     'left outer join CLASIFI CL on ( CL.TB_CODIGO = C.CB_CLASIFI ) '+
                                     'left outer join IMAGEN I on ( I.CB_CODIGO = C.CB_CODIGO ) '+
                                     'left outer join TURNO T on ( T.TU_CODIGO = C.CB_TURNO ) %s '+
                                     'where ( C.CB_CODIGO = %d )';
          Q_EMPLEADO_FOTO: Result := 'select IM_BLOB from IMAGEN where ( CB_CODIGO = %d ) and ( IM_TIPO = ''%s'' )';
          Q_EMPLEADO_FIND: Result := 'select CAST( ''%s'' as CHAR(10) ) EMPRESA, CB_CODIGO, CB_APE_PAT, CB_APE_MAT, CB_NOMBRES '+
                                     'from COLABORA where ( CB_ACTIVO = ''%s'' ) and %s order by CB_APE_PAT, CB_APE_MAT, CB_NOMBRES';
          Q_KIOSKO_EMPLEADO: Result := 'select C.CB_CODIGO, ' + K_PRETTYNAME + ' as PRETTYNAME, C.CB_APE_PAT, C.CB_APE_MAT, C.CB_NOMBRES, I.IM_BLOB, '+
                                       'C.CB_ACTIVO, C.CB_FEC_ING, C.CB_FEC_BAJ, C.CB_DER_GOZ, C.CB_DER_PAG, C.CB_FEC_VAC, C.CB_CREDENC, '+
                                       'C.CB_CALLE, C.CB_COLONIA, C.CB_CODPOST, C.CB_TEL, C.CB_RFC, C.CB_SEGSOC, C.CB_CURP, C.CB_FEC_NAC, C.CB_LUG_NAC, '+
                                       'C.CB_FEC_ANT, C.CB_FEC_CON, C.CB_CONTRAT, CO.TB_DIAS, C.CB_PUESTO, P.PU_DESCRIP, C.CB_TURNO, C.CB_PASSWRD, T.TU_DESCRIP, C.CB_NOMINA, C.CB_CLASIFI, CL.TB_ELEMENT %s ' +
                                       'from COLABORA C '+
                                       'left outer join IMAGEN I on ( I.CB_CODIGO = C.CB_CODIGO ) and ( I.IM_TIPO = ''%s'' ) '+
                                       'left outer join CONTRATO CO on ( CO.TB_CODIGO = C.CB_CONTRAT ) '+
                                       'left outer join PUESTO P on ( P.PU_CODIGO = C.CB_PUESTO ) '+
                                       'left outer join CLASIFI CL on ( CL.TB_CODIGO = C.CB_CLASIFI ) '+
                                       'left outer join TURNO T on ( T.TU_CODIGO = C.CB_TURNO ) %s '+
                                       'where ( C.CB_CODIGO = %d )';
          Q_KIOSKO_AHORROS: Result := 'select A.AH_ABONOS, A.AH_CARGOS, A.AH_FECHA, A.AH_FORMULA, A.AH_NUMERO, A.AH_SALDO_I, A.AH_STATUS, '+
                                      'A.AH_SALDO, A.AH_TIPO, A.AH_TOTAL, A.US_CODIGO, A.CB_CODIGO, TA.TB_ELEMENT AH_DESCRIPCION '+
                                      'from AHORRO A left outer join TAHORRO TA on ( TA.TB_CODIGO = A.AH_TIPO ) '+
                                      'where ( A.CB_CODIGO = %d ) order by A.AH_TIPO';
          Q_KIOSKO_PRESTAMOS: Result := 'select P.PR_ABONOS, P.PR_CARGOS, P.PR_FECHA, P.PR_FORMULA, P.PR_MONTO, P.PR_NUMERO, P.PR_REFEREN, '+
                                        'P.PR_SALDO_I, P.PR_SALDO, P.PR_TOTAL, P.PR_STATUS, P.PR_TIPO, P.US_CODIGO, P.CB_CODIGO, TP.TB_ELEMENT PR_DESCRIPCION '+
                                        'from PRESTAMO P left outer join TPRESTA TP on ( TP.TB_CODIGO = P.PR_TIPO ) '+
                                        'where ( P.CB_CODIGO = %d ) order by P.PR_TIPO, P.PR_REFEREN';
          Q_KIOSKO_TARJETA: Result := 'select A.AU_STATUS, A.AU_TIPO, A.AU_TIPODIA, A.HO_CODIGO, '+
                                      'A.AU_HORASCK, A.AU_HORAS, A.AU_TARDES, A.AU_NUM_EXT, A.AU_DOBLES, A.AU_TRIPLES, A.AU_EXTRAS, '+
                                      'A.AU_AUT_EXT, A.AU_AUT_TRA, A.AU_DES_TRA, '+
                                      {$ifdef INTERBASE}
                                      '( select RESULTADO from AUTORIZACIONES_SELECT( A.CB_CODIGO, A.AU_FECHA, %d ) ) AU_PER_CG,'+
                                      '( select RESULTADO from AUTORIZACIONES_SELECT( A.CB_CODIGO, A.AU_FECHA, %d ) ) AU_PER_SG,'+
                                      '( select RESULTADO from AUTORIZACIONES_SELECT( A.CB_CODIGO, A.AU_FECHA, %d ) ) AU_CG_ENT,'+
                                      '( select RESULTADO from AUTORIZACIONES_SELECT( A.CB_CODIGO, A.AU_FECHA, %d ) ) AU_SG_ENT,'+
                                      {$endif}
                                      {$ifdef MSSQL}
                                      '( dbo.AUTORIZACIONES_SELECT_HORAS( A.CB_CODIGO, A.AU_FECHA, %d ) ) AU_PER_CG,'+
                                      '( dbo.AUTORIZACIONES_SELECT_HORAS( A.CB_CODIGO, A.AU_FECHA, %d ) ) AU_PER_SG,'+
                                      '( dbo.AUTORIZACIONES_SELECT_HORAS( A.CB_CODIGO, A.AU_FECHA, %d ) ) AU_CG_ENT,'+
                                      '( dbo.AUTORIZACIONES_SELECT_HORAS( A.CB_CODIGO, A.AU_FECHA, %d ) ) AU_SG_ENT,'+
                                      {$endif}
                                      'H.HO_INTIME HO_HOR_IN, H.HO_OUTTIME HO_HOR_OU, H.HO_DESCRIP '+
                                      'from AUSENCIA A left outer join HORARIO H on ( H.HO_CODIGO = A.HO_CODIGO ) ' +
                                      'where ( A.CB_CODIGO = %d ) and ( A.AU_FECHA = ''%s'' )';
          Q_KIOSKO_CHECADAS: Result := 'select CH_H_REAL, CH_H_AJUS, CH_DESCRIP, CH_TIPO, CH_HOR_ORD, CH_HOR_EXT,'+
                                       'CH_HOR_DES, US_CODIGO, CH_SISTEMA, CH_RELOJ, CH_GLOBAL '+
                                       'from CHECADAS where ( CB_CODIGO = %d ) and ( AU_FECHA = ''%s'' ) '+
                                       'order by CH_H_REAL';
          Q_KIOSKO_PERIODO_INICIAL: Result := 'select N.PE_YEAR, N.PE_TIPO, N.PE_NUMERO, P.PE_FEC_INI, P.PE_FEC_FIN '+
                                              'from NOMINA N left outer join PERIODO P on ( P.PE_YEAR = N.PE_YEAR ) and ( P.PE_TIPO = N.PE_TIPO ) and ( P.PE_NUMERO = N.PE_NUMERO ) where ' +
                                              '( N.CB_CODIGO = %0:d ) and ( N.PE_YEAR = %1:d ) and ( N.PE_TIPO = %2:d ) and ' +
                                              '( N.PE_NUMERO = ( select MAX( N1.PE_NUMERO ) from NOMINA N1 where ' +
                                              '( N1.CB_CODIGO = %0:d ) and ( N1.PE_YEAR = %1:d ) and ( N1.PE_TIPO = %2:d ) and ( N1.PE_NUMERO < %3:d ) and ( N1.NO_STATUS > %4:d ) ) )';
          Q_KIOSKO_PERIODO_ANTERIOR: Result := 'select N.PE_YEAR, N.PE_TIPO, N.PE_NUMERO, P.PE_FEC_INI, P.PE_FEC_FIN '+
                                               'from NOMINA N left outer join PERIODO P on ( P.PE_YEAR = N.PE_YEAR ) and ( P.PE_TIPO = N.PE_TIPO ) and ( P.PE_NUMERO = N.PE_NUMERO ) where ' +
                                               '( N.CB_CODIGO = %0:d ) and ( N.PE_YEAR = %1:d ) and ( N.PE_TIPO = %2:d ) and ' +
                                               '( N.PE_NUMERO = ( select MAX( N1.PE_NUMERO ) from NOMINA N1 where ' +
                                               '( N1.CB_CODIGO = %0:d ) and ( N1.PE_YEAR = %1:d ) and ( N1.PE_TIPO = %2:d ) and ( N1.PE_NUMERO < %3:d ) ) )';
          Q_KIOSKO_PERIODO_SIGUIENTE: Result := 'select N.PE_YEAR, N.PE_TIPO, N.PE_NUMERO, P.PE_FEC_INI, P.PE_FEC_FIN '+
                                                'from NOMINA N left outer join PERIODO P on ( P.PE_YEAR = N.PE_YEAR ) and ( P.PE_TIPO = N.PE_TIPO ) and ( P.PE_NUMERO = N.PE_NUMERO ) where ' +
                                                '( N.CB_CODIGO = %0:d ) and ( N.PE_YEAR = %1:d ) and ( N.PE_TIPO = %2:d ) and ' +
                                                '( N.PE_NUMERO = ( select MIN( N1.PE_NUMERO ) from NOMINA N1 where ' +
                                                '( N1.CB_CODIGO = %0:d ) and ( N1.PE_YEAR = %1:d ) and ( N1.PE_TIPO = %2:d ) and ( N1.PE_NUMERO > %3:d ) ) )';
          Q_KIOSKO_NOMINA: Result := 'select N.PE_YEAR, N.PE_TIPO, N.PE_NUMERO, N.NO_STATUS, P.PE_FEC_INI, P.PE_FEC_FIN, '+
                                     'N.NO_PERCEPC, N.NO_DEDUCCI, N.NO_NETO '+
                                     'from NOMINA N left outer join PERIODO P on ( P.PE_YEAR = N.PE_YEAR ) and ( P.PE_TIPO = N.PE_TIPO ) and ( P.PE_NUMERO = N.PE_NUMERO ) where ' +
                                     '( N.CB_CODIGO = %0:d ) and ( N.PE_YEAR = %1:d ) and ( N.PE_TIPO = %2:d ) and ( N.PE_NUMERO = %3:d  )';
          Q_KIOSKO_CONCEPTOS: Result := 'select M.MO_ACTIVO, M.CO_NUMERO, M.MO_IMP_CAL, M.US_CODIGO, M.MO_PER_CAL, M.MO_REFEREN, '+
                                        'M.MO_X_ISPT, M.MO_PERCEPC, M.MO_DEDUCCI, C.CO_DESCRIP '+
                                        'from MOVIMIEN M left outer join CONCEPTO C on ( C.CO_NUMERO = M.CO_NUMERO ) where '+
                                        '( M.CB_CODIGO = %0:d ) and ( M.PE_YEAR = %1:d ) and ( M.PE_TIPO = %2:d ) and ( M.PE_NUMERO = %3:d  ) '+
                                        'order by M.CO_NUMERO, M.MO_REFEREN';
          Q_KIOSKO_PRENOMINA: Result := 'select N.PE_YEAR, N.PE_TIPO, N.PE_NUMERO, P.PE_FEC_INI, P.PE_FEC_FIN, '+
                                        'N.NO_FOLIO_1, N.NO_FOLIO_2, N.NO_FOLIO_3, N.NO_FOLIO_4, N.NO_FOLIO_5, N.NO_OBSERVA, N.NO_STATUS, N.US_CODIGO, N.NO_USER_RJ, '+
                                        'N.CB_CODIGO, N.CB_CLASIFI, N.CB_TURNO, N.CB_PATRON, N.CB_PUESTO, N.CB_ZONA_GE, N.CB_SAL_INT, N.CB_SALARIO, '+
                                        'N.CB_NIVEL1, N.CB_NIVEL2, N.CB_NIVEL3, N.CB_NIVEL4, N.CB_NIVEL5, N.CB_NIVEL6, N.CB_NIVEL7, N.CB_NIVEL8, N.CB_NIVEL9,'+
                                        'N.NO_DIAS, N.NO_ADICION, N.NO_DEDUCCI, N.NO_NETO, N.NO_DES_TRA, N.NO_DIAS_AG, N.NO_DIAS_VA, N.NO_DOBLES, N.NO_EXTRAS, '+
                                        'N.NO_FES_PAG, N.NO_FES_TRA, N.NO_HORA_CG, N.NO_HORA_SG, N.NO_HORAS, N.NO_IMP_CAL, N.NO_JORNADA, N.NO_PER_CAL, N.NO_PER_MEN, '+
                                        'N.NO_PERCEPC, N.NO_TARDES, N.NO_TRIPLES, N.NO_TOT_PRE, N.NO_VAC_TRA, N.NO_X_CAL, N.NO_X_ISPT, N.NO_X_MENS, N.NO_D_TURNO, '+
                                        'N.NO_DIAS_AJ, N.NO_DIAS_AS, N.NO_DIAS_CG, N.NO_DIAS_EM, N.NO_DIAS_FI, N.NO_DIAS_FJ, N.NO_DIAS_FV, N.NO_DIAS_IN, '+
                                        'N.NO_DIAS_NT, N.NO_DIAS_OT, N.NO_DIAS_RE, N.NO_DIAS_SG, N.NO_DIAS_SS, N.NO_DIAS_SU, N.NO_HORA_PD, N.NO_LIQUIDA, '+
                                        'N.NO_FUERA, N.NO_EXENTAS, N.NO_FEC_PAG, N.NO_USR_PAG '+
                                        'from NOMINA N left outer join PERIODO P on ( P.PE_YEAR = N.PE_YEAR ) and ( P.PE_TIPO = N.PE_TIPO ) and ( P.PE_NUMERO = N.PE_NUMERO ) where ' +
                                        '( N.CB_CODIGO = %0:d ) and ( N.PE_YEAR = %1:d ) and ( N.PE_TIPO = %2:d ) and ( N.PE_NUMERO = %3:d  )';
          Q_KIOSKO_AUSENCIAS: Result := 'select CB_CODIGO, AU_FECHA, AU_DES_TRA, AU_DOBLES, AU_EXTRAS, '+
                                        'AU_HORAS, AU_PER_CG, AU_PER_SG, AU_TARDES, AU_TIPO, AU_TIPODIA, '+
                                        'CB_CLASIFI, CB_TURNO, CB_PUESTO, HO_CODIGO, '+
                                        'CB_NIVEL1, CB_NIVEL2, CB_NIVEL3, CB_NIVEL4, CB_NIVEL5, CB_NIVEL6, CB_NIVEL7, CB_NIVEL8, CB_NIVEL9, '+
                                        'AU_AUT_EXT, AU_AUT_TRA, AU_HOR_MAN, AU_STATUS, AU_NUM_EXT, '+
                                        'AU_TRIPLES, US_CODIGO, AU_POSICIO, AU_HORASCK, PE_YEAR, PE_TIPO, PE_NUMERO, ' +
                                        {$ifdef INTERBASE}
                                        '( select RESULTADO from SP_CHECADAS( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 1 ) ) CHECADA1, ' +
                                        '( select RESULTADO from SP_CHECADAS( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 2 ) ) CHECADA2, ' +
                                        '( select RESULTADO from SP_CHECADAS( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 3 ) ) CHECADA3, ' +
                                        '( select RESULTADO from SP_CHECADAS( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 4 ) ) CHECADA4 ' +
                                        {$endif}
                                        {$ifdef MSSQL}
                                        'dbo.SP_CHECADAS( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 1 ) CHECADA1, ' +
                                        'dbo.SP_CHECADAS( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 2 ) CHECADA2, ' +
                                        'dbo.SP_CHECADAS( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 3 ) CHECADA3, ' +
                                        'dbo.SP_CHECADAS( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 4 ) CHECADA4 ' +
                                        {$endif}
                                        'from AUSENCIA where ' +
                                        '( CB_CODIGO = %d ) and ( AU_FECHA between ''%s'' and ''%s'' ) ' +
                                        'order by AU_FECHA';
          Q_KIOSKO_COMIDAS: Result := 'select CB_CODIGO, CF_FECHA, CF_HORA, CF_TIPO, CF_COMIDAS, CF_EXTRAS, '+
                                      'CF_RELOJ, US_CODIGO, CL_CODIGO, CF_REG_EXT from CAF_COME where '+
                                      '( CB_CODIGO = %d ) and ( CF_FECHA between ''%s'' and ''%s'' ) '+
                                      'order by CB_CODIGO, CF_FECHA, CF_HORA';
          Q_KIOSKO_COMIDAS_TOTALES: Result  := 'select CF_TIPO, SUM( CF_COMIDAS ) as CF_COMIDAS, SUM( CF_EXTRAS ) as CF_EXTRAS ' +
                                               'from CAF_COME where ( CB_CODIGO = %d ) and ( CF_FECHA between ''%s'' and ''%s'' ) ' +
                                               'group by CF_TIPO order by CF_TIPO';
          Q_KIOSKO_CURSOS_TOMADOS: Result := 'select K.CB_CODIGO, K.CU_CODIGO, K.KC_EVALUA, K.KC_FEC_TOM, K.KC_HORAS, K.KC_REVISIO, ' +
                                             'K.SE_FOLIO, C.CU_NOMBRE AS NOMBRE ' +
                                             'from KARCURSO K left outer join CURSO C on ( K.CU_CODIGO = C.CU_CODIGO ) '+
                                             'where ( K.CB_CODIGO = %d ) order by K.CB_CODIGO, K.KC_FEC_TOM, K.CU_CODIGO';
          Q_KIOSKO_UPDATE_CLAVE: Result := 'update COLABORA set CB_PASSWRD = %s where ( CB_CODIGO = %d )';

          Q_KIOSCO2_GET_BOTONES: Result := ' select * from KBOTON '+
                                           ' where( KS_CODIGO = %s ) '+
                                           ' order by KB_ORDEN ';
          Q_KIOSCO2_GET_CARRUSEL: Result := 'select * '+
                                            'from KSHOWINF '+
                                            'where(KW_CODIGO = %s) '+
                                            'order by KI_ORDEN';
          Q_KIOSCO2_GET_ESTILO: Result := 'select * from KESTILO where(KE_CODIGO = %s)';
          Q_KIOSCO2_GET_PANTALLA: Result := 'select * from KSCREEN where(KS_CODIGO = %s)';
          Q_KIOSCO2_GET_SHOW: Result := 'select * from KSHOW where(KW_CODIGO = %s)';
          Q_KIOSCO2_GET_KIOSCOS: Result := 'select KW_CODIGO, KW_NOMBRE from KSHOW order by KW_CODIGO ';

          Q_EMPLEADOS_ACTUALIZA_PASS: Result := 'UPDATE COLABORA SET CB_PASSWRD = %s WHERE CB_CODIGO = %d';
          Q_EMPLEADOS_ENROL_GET: Result := 'select CB_CODIGO, CB_APE_PAT, CB_APE_MAT, CB_NOMBRES, CB_PORTAL, CB_E_MAIL, CB_USRNAME, CB_PASSWRD from COLABORA where ( CB_CODIGO = %d )';
          Q_EMPLEADOS_ENROL_SET: Result := 'update COLABORA set '+
                                           'CB_PORTAL = ''%1:s'', '+
                                           'CB_E_MAIL = ''%2:s'', '+
                                           'CB_USRNAME = ''%3:s'', '+
                                           'CB_PASSWRD = ''%4:s'' '+
                                           'where ( CB_CODIGO = %0:d )';

     end;
end;

{ ********* TZetaPortalRegistryServer ********* }

function TZetaPortalRegistryServer.GetEmpresas: String;
begin
     Result := Registry.ReadString( '', SERVER_EMPRESAS, '' );
end;

procedure TZetaPortalRegistryServer.SetEmpresas(const Value: String);
begin
     Registry.WriteString( '', SERVER_EMPRESAS, Value );
end;

{ ********* TdmPortalTress ********** }

class procedure TdmPortalTress.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
     if Register then
     begin
       inherited UpdateRegistry(Register, ClassID, ProgID);
       EnableSocketTransport(ClassID);
       EnableWebTransport(ClassID);
     end else
     begin
       DisableSocketTransport(ClassID);
       DisableWebTransport(ClassID);
       inherited UpdateRegistry(Register, ClassID, ProgID);
     end;
end;

procedure TdmPortalTress.MtsDataModuleCreate(Sender: TObject);
begin
     {$ifndef DOS_CAPAS}
     OnActivate := ServerActivate;
     OnDeactivate := ServerDeactivate;
     {$endif}
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
     FIniFile := TIniFile.Create( 'C:\Program Files\Grupo Tress\Kiosco2\Kiosco2.Ini' );
end;

procedure TdmPortalTress.MtsDataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FIniFile );
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
end;

{$ifdef DOS_CAPAS}
procedure TdmPortalTress.CierraEmpresa;
begin
end;
{$else}
procedure TdmPortalTress.ServerActivate(Sender: TObject);
begin
     oZetaProvider.Activate;
end;

procedure TdmPortalTress.ServerDeactivate(Sender: TObject);
begin
     oZetaProvider.Deactivate;
end;
{$endif}

function TdmPortalTress.Fix( const sValue: String): String;
const
     LETRA_A_MINUSCULA = Ord( '�' );
     LETRA_A_MAYUSCULA = Ord( '�' );
     LETRA_E_MINUSCULA = Ord( '�' );
     {
     LETRA_E_MAYUSCULA = Ord( '�' );
     }
     LETRA_I_MINUSCULA = Ord( '�' );
     {
     LETRA_I_MAYUSCULA = Ord( '�' );
     }
     LETRA_O_MINUSCULA = Ord( '�' );
     {
     LETRA_O_MAYUSCULA = Ord( '�' );
     }
     LETRA_U_MINUSCULA = Ord( '�' );
     {
     LETRA_U_MAYUSCULA = Ord( '�' );
     }
     LETRA_ENE_MINUSCULA = Ord( '�' );
     LETRA_ENE_MAYUSCULA = Ord( '�' );
     LETRA_U_DIERESIS = Ord( '�' );
     LETRA_ADMIRACION = Ord( '�' );
     LETRA_PREGUNTA = Ord( '�' );
var
   i, iLen: Integer;
   cValue: Char;
begin
     iLen  := Length( sValue );
     Result := '';
     for i := 1 to iLen do
     begin
        cValue := sValue[ i ];
        case Ord( cValue ) of
               LETRA_A_MINUSCULA: cValue := 'a';
               LETRA_A_MAYUSCULA: cValue := 'a';
               LETRA_E_MINUSCULA: cValue := 'e';
               {
               LETRA_E_MAYUSCULA: cValue := 'e';
               }
               LETRA_I_MINUSCULA: cValue := 'i';
               {
               LETRA_I_MAYUSCULA: cValue := 'i';
               }
               LETRA_O_MINUSCULA: cValue := 'o';
               {
               LETRA_O_MAYUSCULA: cValue := 'o';
               }
               LETRA_U_MINUSCULA: cValue := 'u';
               {
               LETRA_U_MAYUSCULA: cValue := 'u';
               }
               LETRA_ENE_MINUSCULA: cValue := 'n';
               LETRA_ENE_MAYUSCULA: cValue := 'N';
               LETRA_U_DIERESIS: cValue := 'u';
               LETRA_ADMIRACION: cValue := '!';
               LETRA_PREGUNTA: cValue := '?';
        end;
        Result := Result + cValue;
     end;
end;

function TdmPortalTress.BuildEmpresa(DataSet: TZetaCursor): OleVariant;
     { Es una copia de DBaseCliente.BuildEmpresa }
     {
     Las posiciones de este arreglo se encuentran en ZetaCommonClasses:

     P_ALIAS = 0;
     P_DATABASE = 0;
     P_USER_NAME = 1;
     P_PASSWORD = 2;
     P_USUARIO = 3;
     P_NIVEL_0 = 4;
     P_CODIGO = 5;
     }
begin
     with DataSet do
     begin
          Result := VarArrayOf( [ FieldByName( 'CM_DATOS' ).AsString,
                                  FieldByName( 'CM_USRNAME' ).AsString,
                                  FieldByName( 'CM_PASSWRD' ).AsString,
                                  0,
                                  FieldByName( 'CM_NIVEL0' ).AsString,
                                  FieldByName( 'CM_CODIGO' ).AsString ] );
     end;
end;

function TdmPortalTress.GetCompanyData( const sEmpresa: String; var oDatos: OleVariant ): Boolean;
var
   FDataset: TZetaCursor;
begin
     Result := False;
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          FDataset := CreateQuery( Format( GetSQLScript( Q_COMPANYS_FIND ), [ sEmpresa ] ) );
          try
             with FDataset do
             begin
                  Active := True;
                  if not IsEmpty then
                  begin
                       oDatos := BuildEmpresa( FDataSet );
                       Result := True;
                  end;
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataset );
          end;
     end;
end;

function TdmPortalTress.GetEmpleado(const Empresa: WideString; Empleado: Integer; const Archivo: WideString): WideString;
var
   oEmpresa: OleVariant;
   FDataset: TZetaCursor;
   FNiveles: TStrings;
   iNivel: Integer;
   sNivel, sCampos, sJoin, sQuery: String;

    function SaveAsXML( const sFileName: String ): String;
    var
       i: Integer;
       Campo: TField;
       {$ifdef XML}
       FXML: TZetaXML;
       XMLNiveles: TZetaXMLNode;
       {$else}
       FData: TStrings;
       sOutput: String;
       {$endif}
    begin
         {$ifndef XML}
         sOutput := Format( '%s%s%d.xml', [ FormatDirectory( ExtractFilePath( sFileName ) ), Empresa, Empleado ] );
         if FileExists( sOutput ) then
            DeleteFile( sOutput );
         {$endif}
         with cdsLista do
         begin
              if IsEmpty then
                 Result := K_STR_FALSE
              else
              begin
                   {$ifdef XML}
                   FXML := TZetaXML.Create( oZetaProvider );
                   try
                      with FXML do
                      begin
                           with Documento do
                           begin
                                StandAlone := 'yes';
                                Tag := 'empleado';
                                NewChild( 'EMPRESA', Empresa  );
                                NewChild( 'CB_CODIGO', IntToStr( FieldByName( 'CB_CODIGO' ).AsInteger ) );
                                NewChild( 'CB_APE_PAT', Fix( FieldByName( 'CB_APE_PAT' ).AsString ) );
                                NewChild( 'CB_APE_MAT', Fix( FieldByName( 'CB_APE_MAT' ).AsString ) );
                                NewChild( 'CB_NOMBRES', Fix( FieldByName( 'CB_NOMBRES' ).AsString ) );
                                NewChild( 'CB_TURNO', Format( '%s = %s', [ FieldByName( 'CB_TURNO' ).AsString, Fix( FieldByName( 'TU_DESCRIP' ).AsString ) ] ) );
                                NewChild( 'CB_PUESTO', Format( '%s = %s', [ FieldByName( 'CB_PUESTO' ).AsString, Fix( FieldByName( 'PU_DESCRIP' ).AsString ) ] ) );
                                NewChild( 'CB_CLASIFI', Format( '%s = %s',  [ FieldByName( 'CB_CLASIFI' ).AsString, Fix( FieldByName( 'TB_ELEMENT' ).AsString ) ] ) );
                                XMLNiveles := FindOrAddNewChild( 'niveles' );
                                with XMLNiveles do
                                begin
                                     for i := 1 to K_GLOBAL_NIVEL_MAX do
                                     begin
                                          Campo := FindField( Format( Q_NIVEL_COLABORA, [ i ] ) );
                                          if Assigned( Campo ) then
                                          begin
                                               with Campo do
                                               begin
                                                    XMLNiveles.NewChild( 'nivel', Fix( FieldByName( Format( Q_NIVEL_DESCRIPCION, [ i ] ) ).AsString ) ).AddAttribute( 'nombre', Fix( DisplayLabel ) );
                                               end;
                                          end;
                                     end;
                                end;
                                Result := Documento.GetXml;
                           end;
                      end;
                   finally
                          FreeAndNil( FXML );
                   end;
                   {$else}
                   FData := TStringList.Create;
                   try
                      with FData do
                      begin
                           Add( '<?xml version="1.0"?>' );
                           Add( Format( '<?xml-stylesheet href="%s" type="text/xsl"?>', [ ExtractFileName( sFileName ) ] ) ); { Se debe quitar el directorio }
                           First;
                           while not Eof do
                           begin
                                Add( '<empleado>' );
                                Add( Format( '<EMPRESA>%s</EMPRESA>', [ Empresa ] ) );
                                Add( Format( '<CB_CODIGO>%d</CB_CODIGO>', [ FieldByName( 'CB_CODIGO' ).AsInteger ] ) );
                                Add( Format( '<CB_APE_PAT>%s</CB_APE_PAT>', [ Fix( FieldByName( 'CB_APE_PAT' ).AsString ) ] ) );
                                Add( Format( '<CB_APE_MAT>%s</CB_APE_MAT>', [ Fix( FieldByName( 'CB_APE_MAT' ).AsString ) ] ) );
                                Add( Format( '<CB_NOMBRES>%s</CB_NOMBRES>', [ Fix( FieldByName( 'CB_NOMBRES' ).AsString ) ] ) );
                                Add( Format( '<CB_TURNO>%s = %s</CB_TURNO>', [ FieldByName( 'CB_TURNO' ).AsString, Fix( FieldByName( 'TU_DESCRIP' ).AsString ) ] ) );
                                Add( Format( '<CB_PUESTO>%s = %s</CB_PUESTO>', [ FieldByName( 'CB_PUESTO' ).AsString, Fix( FieldByName( 'PU_DESCRIP' ).AsString ) ] ) );
                                Add( Format( '<CB_CLASIFI>%s = %s</CB_CLASIFI>', [ FieldByName( 'CB_CLASIFI' ).AsString, Fix( FieldByName( 'TB_ELEMENT' ).AsString ) ] ) );
                                Add( '<niveles>' );
                                for i := 1 to K_GLOBAL_NIVEL_MAX do
                                begin
                                     Campo := FindField( Format( Q_NIVEL_COLABORA, [ i ] ) );
                                     if Assigned( Campo ) then
                                     begin
                                          with Campo do
                                          begin
                                               Add( Format( '<nivel nombre = "%s">%s = %s</nivel>', [ Fix( DisplayLabel ), AsString, Fix( FieldByName( Format( Q_NIVEL_DESCRIPCION, [ i ] ) ).AsString ) ] ) );
                                          end;
                                     end;
                                end;
                                Add( '</niveles>' );
                                Add( '</empleado>' );
                                Next;
                           end;
                           SaveToFile( sOutput );
                      end;
                   finally
                          FreeAndNil( FData );
                   end;
                   if FileExists( sOutput ) then
                      Result := sOutput
                   else
                       Result := K_STR_FALSE;
                   {$endif}
              end;
         end;
    end;

begin
     Result := K_STR_FALSE;
     if GetCompanyData( Empresa, oEmpresa ) then
     begin
          with oZetaProvider do
          begin
               EmpresaActiva := oEmpresa;
               FNiveles := TStringList.Create;
               try
                  FDataset := CreateQuery( Format( GetSQLScript( Q_GLOBAL_NIVELES ), [ K_GLOBAL_NIVEL_BASE, K_GLOBAL_NIVEL_BASE + K_GLOBAL_NIVEL_MAX ] ) );
                  try
                     with FDataset do
                     begin
                          Active := True;
                          sCampos := '';
                          sJoin := '';
                          while not Eof do
                          begin
                               iNivel := FieldByName( 'GL_CODIGO' ).AsInteger - K_GLOBAL_NIVEL_BASE;
                               sNivel := Trim( Fix( FieldByName( 'GL_FORMULA' ).AsString ) );
                               if ZetaCommonTools.StrLleno( sNivel ) then
                               begin
                                    with FNiveles do
                                    begin
                                         Add( Format( '%s=%s', [ Format( Q_NIVEL_COLABORA, [ iNivel ] ), sNivel ] ) );
                                         Add( Format( '%s=Descripci�n De %s', [ Format( Q_NIVEL_DESCRIPCION, [ iNivel ] ), sNivel ] ) );
                                    end;
                                    sCampos := sCampos + Format( Q_NIVEL_CAMPO, [ iNivel ] );
                                    sJoin := sJoin + Format( Q_NIVEL_JOIN, [ iNivel ] );
                               end;
                               Next;
                          end;
                          Active := False;
                     end;
                  finally
                         FreeAndNil( FDataset );
                  end;
                  sQuery := Format( GetSQLScript( Q_EMPLEADO_DATA ), [ sCampos, sJoin, Empleado ] );
                  with cdsLista do
                  begin
                       Data := OpenSQL( oEmpresa, sQuery, True );
                       if Active then
                       begin
                            FieldByName( 'CB_CODIGO' ).DisplayLabel := 'N�mero';
                            FieldByName( 'CB_APE_PAT' ).DisplayLabel := 'Apellido Paterno';
                            FieldByName( 'CB_APE_MAT' ).DisplayLabel := 'Apellido Materno';
                            FieldByName( 'CB_NOMBRES' ).DisplayLabel := 'Nombres';
                            FieldByName( 'CB_PUESTO' ).DisplayLabel := 'Puesto';
                            FieldByName( 'PU_DESCRIP' ).DisplayLabel := 'Nombre del Puesto';
                            FieldByName( 'CB_TURNO' ).DisplayLabel := 'Turno';
                            FieldByName( 'TU_DESCRIP' ).DisplayLabel := 'Nombre del Turno';
                            FieldByName( 'CB_CLASIFI' ).DisplayLabel := 'Clasificaci�n';
                            FieldByName( 'TB_ELEMENT' ).DisplayLabel := 'Nombre de la Clasificaci�n';
                            with FNiveles do
                            begin
                                 for iNivel := 0 to ( Count - 1 ) do
                                 begin
                                      sNivel := Names[ iNivel ];
                                      FieldByName( sNivel ).DisplayLabel := Values[ sNivel ];
                                 end;
                            end;
                       end;
                       Result := SaveAsXML( Archivo );
                  end;
               finally
                      FreeAndNil( FNiveles );
               end;
          end;
     end;
     SetComplete;
end;

function TdmPortalTress.TransformImage( const sOutput: String; Campo: TBlobField ): String;
const
     K_ANCHO = 250;
     K_ALTO = 180;
var
   oJpeg: TJPEGImage;
   oBitmap: TBitmap;
   Zona: TRect;
   lChange: Boolean;
begin
     if FileExists( sOutput ) then
        DeleteFile( sOutput );
     Campo.SaveToFile( sOutput );
     oJpeg := TJPEGImage.Create;
     try
        with oJpeg do
        begin
             ProgressiveDisplay := False;
             ProgressiveEncoding := False;
             Scale := jsFullSize;
             Smoothing := False;
             GrayScale := False;
             CompressionQuality := 100; //JpegCompQual;
             Performance := jpBestQuality;
             LoadFromFile( sOutput );
             lChange := ( Width > K_ANCHO ) or ( Height > K_ALTO );
        end;
        if lChange then
        begin
             oBitmap := TBitmap.Create;
             try
                with oBitmap do
                begin
                     Monochrome := False;
                     PixelFormat := pf24bit;
                     Height := K_ALTO;
                     Width := K_ANCHO;
                     with Zona do
                     begin
                          Left := 0;
                          Right := K_ANCHO;
                          Top := 0;
                          Bottom := K_ALTO;
                     end;
                     Canvas.StretchDraw( Zona, oJpeg );
                end;
                with oJpeg do
                begin
                     Assign( oBitmap );
                     {
                     Compress;
                     }
                     SaveToFile( sOutput );
                end;
             finally
                    FreeAndNil( oBitmap );
             end;
        end;
     finally
            FreeAndNil( oJpeg );
     end;
     if FileExists( sOutput ) then
        Result := sOutput
     else
         Result := K_STR_FALSE;
end;

function TdmPortalTress.GetFoto(const Empresa: WideString; Empleado: Integer; const Archivo: WideString): WideString;
var
   oEmpresa: OleVariant;
   sOutput: String;
begin
     Result := K_STR_FALSE;
     if GetCompanyData( Empresa, oEmpresa ) then
     begin
          with oZetaProvider do
          begin
               EmpresaActiva := oEmpresa;
               with cdsLista do
               begin
                    Data := OpenSQL( oEmpresa, Format( GetSQLScript( Q_EMPLEADO_FOTO ), [ Empleado, K_FOTO ] ), True );
                    if not IsEmpty then
                    begin
                         sOutput := Format( '%s%s%d.jpg', [ FormatDirectory( ExtractFilePath( Archivo ) ), Empresa, Empleado ] );
                         try
                            Result := TransformImage( sOutput, TBlobField( FieldByName( 'IM_BLOB' ) ) );
                         except
                               { Para impedir que una excepcion al manejar la imagen marque error en el cliente}
                               Result := K_STR_FALSE;
                         end;
                    end;
               end;
          end;
     end;
     SetComplete;
end;

{$ifdef XML}
function TdmPortalTress.GetEmpleadoScript( const iEmpleado: Integer ): String;
var
   FCursor: TZetaCursor;
   iNivel: Integer;
   sNivel, sCampos, sJoin: String;
begin
     FCursor := oZetaProvider.CreateQuery( Format( GetSQLScript( Q_GLOBAL_NIVELES ), [ K_GLOBAL_NIVEL_BASE, K_GLOBAL_NIVEL_BASE + K_GLOBAL_NIVEL_MAX ] ) );
     try
        with FCursor do
        begin
             Active := True;
             sCampos := '';
             sJoin := '';
             while not Eof do
             begin
                  iNivel := FieldByName( 'GL_CODIGO' ).AsInteger - K_GLOBAL_NIVEL_BASE;
                  sNivel := Trim( Fix( FieldByName( 'GL_FORMULA' ).AsString ) );
                  if ZetaCommonTools.StrLleno( sNivel ) then
                  begin
                       sCampos := sCampos + Format( Q_NIVEL_CAMPO, [ iNivel ] );
                       sJoin := sJoin + Format( Q_NIVEL_JOIN, [ iNivel ] );
                  end;
                  Next;
             end;
             Active := False;
        end;
     finally
        FreeAndNil( FCursor );
     end;
     Result := Format( GetSQLScript( Q_EMPLEADO_DATA ), [ sCampos, sJoin, iEmpleado ] );
end;
{$endif}

function TdmPortalTress.GetEmpleado2(const Parametros: WideString): WideString;
{$ifdef XML}
var
   FXML: TZetaXML;
   FCursor: TZetaCursor;
   oEmpresa: OleVariant;
   oEmpleado, oNodoInfo, oNiveles: TZetaXMLNode;
   i: Integer;

   function GetPath( const sDir: String ): String;
   begin
        with FXML do
             Result := Format( '%s%s%d.jpg', [ ZetaCommonTools.VerificaDir( sDir ), ParamAsString('EMPRESA'), ParamAsInteger('EMPLEADO') ] );
   end;

   function GetFoto( oCampo: TBlobField ): String;
   begin
        Result := VACIO;
        with FXML do
             if Assigned( oCampo ) and ( not oCampo.IsNull ) and strLleno( TransformImage( GetPath( ParamAsString('DIRECTORIO') ), oCampo ) ) then
                Result := GetPath( ParamAsString('DIRVIRTUAL') );
   end;

{$endif}
begin
{$ifdef XML}
     FXML := TZetaXML.Create( oZetaProvider );
     try
        with FXML do
        begin
             if LoadParametros( Parametros ) then
             begin
                  if GetCompanyData( ParamAsString( 'EMPRESA' ), oEmpresa ) then
                  begin
                       try
                          with oZetaProvider do
                          begin
                               EmpresaActiva := oEmpresa;
                               InitGlobales;
                          end;
                          FCursor := oZetaProvider.CreateQuery( GetEmpleadoScript( ParamAsInteger( 'EMPLEADO' ) ) );
                          try
                             try
                                with FCursor do
                                begin
                                     Active := True;
                                     if not IsEmpty then
                                     begin
                                          oEmpleado := Documento.GetRoot.FindOrAddNewChild( 'EMPLEADO' );
                                          oEmpleado.AddAttribute( 'EMPRESA', ParamAsString( 'EMPRESA' ) );
                                          oNodoInfo := oEmpleado.NewChild( 'DATOS', '' );
                                          with oNodoInfo do
                                          begin
                                               AddAttribute( 'CB_CODIGO', Fix( FieldByName( 'CB_CODIGO' ).AsString ) );
                                               AddAttribute( 'CB_APE_PAT', Fix( FieldByName( 'CB_APE_PAT' ).AsString ) );
                                               AddAttribute( 'CB_APE_MAT', Fix( FieldByName( 'CB_APE_MAT' ).AsString ) );
                                               AddAttribute( 'CB_NOMBRES', Fix( FieldByName( 'CB_NOMBRES' ).AsString ) );
                                               AddAttribute( 'CB_TURNO', Format( '%s = %s', [ FieldByName( 'CB_TURNO' ).AsString, Fix( FieldByName( 'TU_DESCRIP' ).AsString ) ] ) );
                                               AddAttribute( 'CB_PUESTO', Format( '%s = %s', [ FieldByName( 'CB_PUESTO' ).AsString, Fix( FieldByName( 'PU_DESCRIP' ).AsString ) ] ) );
                                               AddAttribute( 'CB_CLASIFI', Format( '%s = %s',  [ FieldByName( 'CB_CLASIFI' ).AsString, Fix( FieldByName( 'TB_ELEMENT' ).AsString ) ] ) );
                                               AddAttribute( 'CB_FOTO', GetFoto( TBlobField( FieldByName( 'IM_BLOB' ) ) ) );
                                          end;
                                          oNiveles := oEmpleado.NewChild( 'NIVELES', '' );
                                          with oNiveles do
                                          begin
                                               for i := 1 to K_GLOBAL_NIVEL_MAX do
                                               begin
                                                    if Assigned( FindField( Format( Q_NIVEL_COLABORA, [ i ] ) ) ) then
                                                       NewChild( 'NIVEL', Fix( FieldByName( Format( Q_NIVEL_DESCRIPCION, [ i ] ) ).AsString ) ).AddAttribute( 'NOMBRE', oZetaProvider.GetGlobalString( K_GLOBAL_NIVEL_BASE + i ) );
                                               end;
                                          end;
                                     end
                                     else
                                         BuildErrorXML( Format( 'Los Datos Del Empleado # %d No Est�n Disponibles, Intente de Nuevo. Gracias', [ ParamAsInteger( 'EMPLEADO' ) ] ) );
                                     Active := False;
                                end;
                             except
                                   on Error: Exception do
                                   begin
                                        BuildErrorXML( 'Error Al Consultar Datos de Empleado: ' + Error.Message );
                                   end;
                             end;
                          finally
                             FreeAndNil( FCursor );
                          end;
                       except
                           on Error: Exception do
                           begin
                                BuildErrorXML( 'Error Al Conectar Empresa: ' + Error.Message );
                           end;
                       end;
                  end;
             end
             else
                 BuildErrorXML;
             Result := GetDocXML;
        end;
     finally
        FreeAndNil( FXML );
     end;
{$else}
     Result := K_STR_FALSE;
{$endif}
     SetComplete;
end;

function TdmPortalTress.BuscaEmpleados(const Texto: WideString; Tipo: Integer; const Archivo: WideString): WideString;
const
     Q_FILTRO_APELLIDO = '( ( UPPER( CB_APE_PAT ) like ''%0:s'' ) or ( UPPER( CB_APE_MAT ) like ''%0:s'' ) )';
     Q_FILTRO_NOMBRES = '( UPPER( CB_NOMBRES ) like ''%s'' )';
     Q_FILTRO_NUMERO = '( CB_CODIGO = %d )';
     Q_FILTRO_EMPRESAS = '( CM_CODIGO = ''%s'' )';
var
   sLista, sEmpresa, sFiltro: String;
   oEmpresa: OleVariant;
   lFirst: Boolean;
   FEmpresas: TZetaCursor;

    function GetScriptBuscaEmpleados: String;
    begin
         Result := Format( GetSQLScript( Q_EMPLEADO_FIND ), [ sEmpresa, K_GLOBAL_SI, sFiltro ] );
    end;

    procedure FindEmployees;
    var
       oZetaProviderLookup: TdmZetaServerProvider;
       FDataset: TZetaCursor;
    begin
         oZetaProviderLookup := TdmZetaServerProvider.Create( Self );
         try
            with oZetaProviderLookup do
            begin
                 EmpresaActiva := oEmpresa;
                 FDataset := CreateQuery( GetScriptBuscaEmpleados );
                 try
                    with FDataset do
                    begin
                         Active := True;
                         while not Eof do
                         begin
                              cdsOutput.Append;
                              cdsOutput.FieldByName( 'EMPRESA' ).AsString := FieldByName( 'EMPRESA' ).AsString;
                              cdsOutput.FieldByName( 'CB_CODIGO' ).AsInteger := FieldByName( 'CB_CODIGO' ).AsInteger;
                              cdsOutput.FieldByName( 'CB_APE_PAT' ).AsString := Fix( FieldByName( 'CB_APE_PAT' ).AsString );
                              cdsOutput.FieldByName( 'CB_APE_MAT' ).AsString := Fix( FieldByName( 'CB_APE_MAT' ).AsString );
                              cdsOutput.FieldByName( 'CB_NOMBRES' ).AsString := Fix( FieldByName( 'CB_NOMBRES' ).AsString );
                              cdsOutput.Post;
                              Next;
                         end;
                         Active := False;
                    end;
                 finally
                        FreeAndNil( FDataset );
                 end;
            end;
         finally
                FreeAndNil( oZetaProviderLookup );
         end;
    end;

    function GetFiltroEmpresas: String;
    var
       FLista: TStrings;
       i: Integer;
    begin
         Result := GetEmpresas;
         if ZetaCommonTools.StrLleno( Result ) then
         begin
              FLista := TStringList.Create;
              try
                 with FLista do
                 begin
                      CommaText := Result;
                      Result := ' and ( ';
                      for i := 0 to ( Count - 1 ) do
                      begin
                           if ( i > 0 ) then
                              Result := Result + ' or ';
                           Result := Result + Format( Q_FILTRO_EMPRESAS, [ Strings[ i ] ] );
                      end;
                      Result := Result + ' )';
                 end;
              finally
                     FreeAndNil( FLista );
              end;
         end;
    end;

    function SaveAsXML( const sFileName: String ): String;
    var
       {$ifdef XML}
       FXML: TZetaXML;
       XMLEmpleado: TZetaXMLNode;
       {$else}
       FData: TStrings;
       sOutput: String;
       {$endif}
       i: Integer;
    begin
         {$ifndef XML}
         sOutput := Format( '%s%d.xml', [ FormatDirectory( ExtractFilePath( sFileName ) ), Trunc( 1000000 * Time ) ] );
         if FileExists( sOutput ) then
            DeleteFile( sOutput );
         {$endif}
         with cdsOutput do
         begin
              if IsEmpty then
                 Result := ''
              else
              begin
                   {$ifdef XML}
                   FXML := TZetaXML.Create( oZetaProvider );
                   try
                      with FXML do
                      begin
                           with Documento do
                           begin
                                StandAlone := 'yes';
                                Tag := 'empleados';
                                First;
                                while not Eof do
                                begin
                                     XMLEmpleado := Documento.NewChild( 'empleado', VACIO );
                                     with XMLEmpleado do
                                     begin
                                          AddAttribute( 'CB_CODIGO', Format( '%d', [ FieldByName( 'CB_CODIGO' ).AsInteger ] ) );
                                          for i := 0 to ( FieldCount - 1 ) do
                                          begin
                                               with Fields[ i ] do
                                               begin
                                                    case DataType of
                                                         ftString: NewChild( FieldName, AsString );
                                                         ftInteger, ftWord: NewChild( FieldName, Format( '%d', [ AsInteger ] ) );
                                                    end;
                                               end;
                                          end;
                                          NewChild( 'XX_FILENAME', VACIO );
                                     end;
                                     Next;
                                end;
                           end;
                           Result := Documento.GetXml;
                      end;
                   finally
                          FreeAndNil( FXML );
                   end;
                   {$else}
                   FData := TStringList.Create;
                   try
                      with FData do
                      begin
                           Add( '<?xml version="1.0"?>' );
                           Add( Format( '<?xml-stylesheet href="%s" type="text/xsl"?>', [ ExtractFileName( sFileName ) ] ) );
                           Add( '<empleados>' );
                           First;
                           while not Eof do
                           begin
                                Add( Format( '<empleado %s="%d">', [ 'CB_CODIGO', FieldByName( 'CB_CODIGO' ).AsInteger ] ) );
                                for i := 0 to ( FieldCount - 1 ) do
                                begin
                                     with Fields[ i ] do
                                     begin
                                          case DataType of
                                               ftString: Add( Format( '<%s>%s</%s>', [ FieldName, AsString, FieldName ] ) );
                                               ftInteger, ftWord: Add( Format( '<%s>%d</%s>', [ FieldName, AsInteger, FieldName ] ) );
                                          end;
                                     end;
                                end;
                                Add( Format( '<XX_FILENAME>%s</XX_FILENAME>', [ ExtractFileName( sOutput ) ] ) );
                                Add( '</empleado>' );
                                Next;
                           end;
                           Add( '</empleados>' );
                           SaveToFile( sOutput );
                      end;
                   finally
                          FreeAndNil( FData );
                   end;
                   if FileExists( sOutput ) then
                      Result := sOutput
                   else
                       Result := '';
                   {$endif}
              end;
         end;
    end;

begin
     Result := VACIO;
     lFirst := True;
     case eBusqueda( Tipo ) of
          ebApellido: sFiltro := Format( Q_FILTRO_APELLIDO, [ AnsiUpperCase( Texto ) + '%' ] );
          ebNombre: sFiltro := Format( Q_FILTRO_NOMBRES, [ '%' + AnsiUpperCase( Texto ) + '%' ] );
          ebNumero: sFiltro := Format( Q_FILTRO_NUMERO, [ StrToIntDef( Texto, 0 ) ] );
     else
         sFiltro := Format( Q_FILTRO_NUMERO, [ 0 ] );
     end;
     sLista := GetFiltroEmpresas;
     with cdsOutput do
     begin
          if Active then
          begin
               EmptyDataset;
               Active := False;
          end;
     end;
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          FEmpresas := CreateQuery( Format( GetSQLScript( Q_COMPANYS_LIST ), [ ZetaServerTools.GetTipoCompany( Ord( tc3Datos ) ), sLista ] ) );
          try
             with FEmpresas do
             begin
                  Active := True;
                  while not Eof do
                  begin
                       oEmpresa := BuildEmpresa( FEmpresas );
                       sEmpresa := FieldByName( 'CM_CODIGO' ).AsString;
                       if lFirst then
                       begin
                            { Trae el Metadata }
                            cdsOutput.Data := oZetaProvider.OpenSQL( oEmpresa, GetScriptBuscaEmpleados, False);
                            lFirst := False;
                       end;
                       FindEmployees;
                       Next;
                  end;
             end;
          finally
                 FreeAndNil( FEmpresas );
          end;
     end;
     with cdsOutput do
     begin
          if Active then
          begin
               FieldByName( 'EMPRESA' ).DisplayLabel := 'Empresa';
               FieldByName( 'CB_CODIGO' ).DisplayLabel := 'N�mero';
               FieldByName( 'CB_APE_PAT' ).DisplayLabel := 'Apellido Paterno';
               FieldByName( 'CB_APE_MAT' ).DisplayLabel := 'Apellido Materno';
               FieldByName( 'CB_NOMBRES' ).DisplayLabel := 'Nombres';
          end;
     end;
     Result := SaveAsXML( Archivo );
     SetComplete;
end;

function TdmPortalTress.GetEmpresas: WideString;
var
   FRegistry: TZetaPortalRegistryServer;
begin
     FRegistry := TZetaPortalRegistryServer.Create;
     try
        Result := FRegistry.Empresas;
     finally
            FreeAndNil( FRegistry );
     end;
     SetComplete;
end;

function TdmPortalTress.SetEmpresas(const Empresas: WideString): Integer;
var
   FRegistry: TZetaPortalRegistryServer;
begin
     FRegistry := TZetaPortalRegistryServer.Create;
     try
        FRegistry.Empresas := Empresas;
     finally
            FreeAndNil( FRegistry );
     end;
     Result := K_INT_TRUE;
     SetComplete;
end;

function TdmPortalTress.KioskoEmpleado(Empresa: OleVariant; Empleado: Integer): OleVariant;
var
   FDataset: TZetaCursor;
   iNivel: Integer;
   sNivel, sCampos, sJoin: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FDataset := CreateQuery( Format( GetSQLScript( Q_GLOBAL_NIVELES ), [ K_GLOBAL_NIVEL_BASE, K_GLOBAL_NIVEL_BASE + K_GLOBAL_NIVEL_MAX ] ) );
          try
             with FDataset do
             begin
                  Active := True;
                  sCampos := '';
                  sJoin := '';
                  while not Eof do
                  begin
                       iNivel := FieldByName( 'GL_CODIGO' ).AsInteger - K_GLOBAL_NIVEL_BASE;
                       sNivel := Trim( Fix( FieldByName( 'GL_FORMULA' ).AsString ) );
                       if ZetaCommonTools.StrVacio( sNivel ) then
                       begin
                            sNivel := K_TOKEN_NIL;
                            { Si el nivel est� deshabilitado, enviar K_TOKEN_NIL en el valor de TB_ELEMENT%d }
                            sCampos := sCampos + Format( Q_NIVEL_NO_JOIN, [ iNivel, K_TOKEN_NIL ] );
                       end
                       else
                       begin
                            sCampos := sCampos + Format( Q_NIVEL_CAMPO, [ iNivel ] );
                       end;
                       { Concatenar la descripci�n del Nivel }
                       sCampos := sCampos + Format( ', ''%s'' as GL_NIVEL%d', [ sNivel, iNivel ] );
                       sJoin := sJoin + Format( Q_NIVEL_JOIN, [ iNivel ] );
                       Next;
                  end;
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataset );
          end;
          cdsLista.Data := OpenSQL( Empresa, Format( GetSQLScript( Q_KIOSKO_EMPLEADO ), [ sCampos, K_FOTO, sJoin, Empleado ] ), True );
          with cdsLista do
          begin
               if not IsEmpty then
               begin
                    Edit;
                    with FieldByName( 'CB_PASSWRD' ) do
                    begin
                         AsString := ZetaServerTools.Decrypt( AsString );
                    end;
                    Post;
               end;
          end;
          Result := cdsLista.Data;
     end;
     SetComplete;
end;

function TdmPortalTress.KioskoPrestamos(Empresa: OleVariant; Empleado: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          Result := OpenSQL( Empresa, Format( GetSQLScript( Q_KIOSKO_PRESTAMOS ), [ Empleado ] ), True );
     end;
     SetComplete;
end;

function TdmPortalTress.KioskoAhorros(Empresa: OleVariant; Empleado: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          Result := OpenSQL( Empresa, Format( GetSQLScript( Q_KIOSKO_AHORROS ), [ Empleado ] ), True );
     end;
     SetComplete;
end;

function TdmPortalTress.KioskoTarjeta(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime; out Checadas: OleVariant): OleVariant;
var
   sFecha: String;
begin
     sFecha := ZetaCommonTools.DateToStrSQL( Fecha );
     with oZetaProvider do
     begin
          Checadas := OpenSQL( Empresa, Format( GetSQLScript( Q_KIOSKO_CHECADAS ), [ Empleado, sFecha ] ), True );
          Result := OpenSQL( Empresa, Format( GetSQLScript( Q_KIOSKO_TARJETA ), [ Ord( chConGoce ), Ord( chSinGoce ), Ord( chConGoceEntrada ), Ord( chSinGoceEntrada ), Empleado, sFecha ] ), True );
     end;
     SetComplete;
end;

function TdmPortalTress.GetPeriodoDatos( Empresa: OleVariant; const sScript: String ): Boolean;
var
   FQuery: TZetaCursor;
begin
     Result := False;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FQuery := CreateQuery( sScript );
          try
             with FQuery do
             begin
                  Active := True;
                  with FPeriodo do
                  begin
                       if IsEmpty then
                       begin
                            Year := 0;
                            Tipo := eTipoPeriodo( 0 );
                            Numero := 0;
                            Inicio := NullDateTime;
                            Fin := NullDateTime;
                       end
                       else
                       begin
                            Year := FieldByName( 'PE_YEAR' ).AsInteger;
                            Tipo := eTipoPeriodo( FieldByName( 'PE_TIPO' ).AsInteger );
                            Numero := FieldByName( 'PE_NUMERO' ).AsInteger;
                            Inicio := FieldByName( 'PE_FEC_INI' ).AsDateTime;
                            Fin := FieldByName( 'PE_FEC_FIN' ).AsDateTime;
                            Result := True;
                       end;
                  end;
                  Active := False;
             end;
          finally
                 FreeAndNil( FQuery );
          end;
     end;
end;

function TdmPortalTress.GetNominaDatos( Empresa: OleVariant; const iEmpleado: TNumEmp): OleVariant;
begin
     with oZetaProvider do
     begin
          with FPeriodo do
          begin
               Result := OpenSQL( Empresa, Format( GetSQLScript( Q_KIOSKO_NOMINA ), [ iEmpleado, Year, Ord( Tipo ), Numero ] ), True );
          end;
     end;
end;

function TdmPortalTress.GetNominaConceptos( Empresa: OleVariant; const iEmpleado: TNumEmp ): OleVariant;
begin
     with oZetaProvider do
     begin
          with FPeriodo do
          begin
               Result := OpenSQL( Empresa, Format( GetSQLScript( Q_KIOSKO_CONCEPTOS ), [ iEmpleado, Year, Ord( Tipo ), Numero ] ), True );
          end;
     end;
end;

function TdmPortalTress.GetPrenominaDatos( Empresa: OleVariant; const iEmpleado: TNumEmp ): OleVariant;
begin
     with oZetaProvider do
     begin
          with FPeriodo do
          begin
               Result := OpenSQL( Empresa, Format( GetSQLScript( Q_KIOSKO_PRENOMINA ), [ iEmpleado, Year, Ord( Tipo ), Numero ] ), True );
          end;
     end;
end;

function TdmPortalTress.GetPrenominaTarjetas( Empresa: OleVariant; const iEmpleado: TNumEmp ): OleVariant;
begin
     with oZetaProvider do
     begin
          with FPeriodo do
          begin
               Result := OpenSQL( Empresa, Format( GetSQLScript( Q_KIOSKO_AUSENCIAS ), [ iEmpleado, ZetaCommonTools.DateToStrSQL( Inicio ), ZetaCommonTools.DateToStrSQL( Fin ) ] ), True );
          end;
     end;
end;

function TdmPortalTress.GetComidas( Empresa: OleVariant; const iEmpleado: TNumEmp ): OleVariant;
begin
     with oZetaProvider do
     begin
          with FPeriodo do
          begin
               Result := OpenSQL( Empresa, Format( GetSQLScript( Q_KIOSKO_COMIDAS ), [ iEmpleado, ZetaCommonTools.DateToStrSQL( Inicio ), ZetaCommonTools.DateToStrSQL( Fin ) ] ), True );
          end;
     end;
end;

function TdmPortalTress.GetComidasTotales( Empresa: OleVariant; const iEmpleado: TNumEmp ): OleVariant;
begin
     with oZetaProvider do
     begin
          with FPeriodo do
          begin
               Result := OpenSQL( Empresa, Format( GetSQLScript( Q_KIOSKO_COMIDAS_TOTALES ), [ iEmpleado, ZetaCommonTools.DateToStrSQL( Inicio ), ZetaCommonTools.DateToStrSQL( Fin ) ] ), True );
          end;
     end;
end;

procedure TdmPortalTress.GetPeriodoAnterior(Empresa: OleVariant; const iEmpleado, iYear, iTipo, iNumero: Integer );
begin
     with FPeriodo do
     begin
          Tipo := eTipoPeriodo( iTipo );
          GetPeriodoDatos( Empresa, Format( GetSQLScript( Q_KIOSKO_PERIODO_ANTERIOR ), [ iEmpleado, iYear, Ord( Tipo ), iNumero ] ) );
     end;
end;

procedure TdmPortalTress.GetPeriodoInicial(Empresa: OleVariant; const iEmpleado, iYear, iTipo: Integer );
begin
     with FPeriodo do
     begin
          Tipo := eTipoPeriodo( iTipo );
          GetPeriodoDatos( Empresa, Format( GetSQLScript( Q_KIOSKO_PERIODO_INICIAL ), [ iEmpleado, iYear, Ord( Tipo ), K_LIMITE_NOM_NORMAL, Ord( spSinCalcular ) ] ) );
     end;
end;

procedure TdmPortalTress.GetPeriodoSiguiente(Empresa: OleVariant; const iEmpleado, iYear, iTipo, iNumero: Integer );
begin
     with FPeriodo do
     begin
          Tipo := eTipoPeriodo( iTipo );
          GetPeriodoDatos( Empresa, Format( GetSQLScript( Q_KIOSKO_PERIODO_SIGUIENTE ), [ iEmpleado, iYear, Ord( Tipo ), iNumero ] ) );
     end;
end;

function TdmPortalTress.KioskoNominaAnterior(Empresa: OleVariant; Empleado, Year, Tipo, Numero: Integer; out Conceptos: OleVariant): OleVariant;
begin
     GetPeriodoAnterior( Empresa, Empleado, Year, Tipo, Numero );
     Result := GetNominaDatos( Empresa, Empleado );
     Conceptos := GetNominaConceptos( Empresa, Empleado );
     SetComplete;
end;

function TdmPortalTress.KioskoNominaInit(Empresa: OleVariant; Empleado, Year, Tipo: Integer; out Conceptos: OleVariant): OleVariant;
begin
     GetPeriodoInicial( Empresa, Empleado, Year, Tipo );
     Result := GetNominaDatos( Empresa, Empleado );
     Conceptos := GetNominaConceptos( Empresa, Empleado );
     SetComplete;
end;

function TdmPortalTress.KioskoNominaSiguiente(Empresa: OleVariant; Empleado, Year, Tipo, Numero: Integer; out Conceptos: OleVariant): OleVariant;
begin
     GetPeriodoSiguiente( Empresa, Empleado, Year, Tipo, Numero );
     Result := GetNominaDatos( Empresa, Empleado );
     Conceptos := GetNominaConceptos( Empresa, Empleado );
     SetComplete;
end;

function TdmPortalTress.KioskoPrenominaInit(Empresa: OleVariant; Empleado, Year, Tipo: Integer; out Tarjetas: OleVariant): OleVariant;
begin
     GetPeriodoInicial( Empresa, Empleado, Year, Tipo );
     Result := GetPrenominaDatos( Empresa, Empleado );
     Tarjetas := GetPrenominaTarjetas( Empresa, Empleado );
     SetComplete;
end;

function TdmPortalTress.KioskoPrenominaAnterior(Empresa: OleVariant; Empleado, Year, Tipo, Numero: Integer; out Tarjetas: OleVariant): OleVariant;
begin
     GetPeriodoAnterior( Empresa, Empleado, Year, Tipo, Numero );
     Result := GetPrenominaDatos( Empresa, Empleado );
     Tarjetas := GetPrenominaTarjetas( Empresa, Empleado );
     SetComplete;
end;

function TdmPortalTress.KioskoPrenominaSiguiente(Empresa: OleVariant; Empleado, Year, Tipo, Numero: Integer; out Tarjetas: OleVariant): OleVariant;
begin
     GetPeriodoSiguiente( Empresa, Empleado, Year, Tipo, Numero );
     Result := GetPrenominaDatos( Empresa, Empleado );
     Tarjetas := GetPrenominaTarjetas( Empresa, Empleado );
     SetComplete;
end;

function TdmPortalTress.KioskoComidasInit(Empresa: OleVariant; Empleado, Year, Tipo: Integer; out Periodo, Totales: OleVariant): OleVariant;
begin
     GetPeriodoInicial( Empresa, Empleado, Year, Tipo );
     Result := GetComidas( Empresa, Empleado );
     Periodo := GetNominaDatos( Empresa, Empleado );
     Totales := GetComidasTotales( Empresa, Empleado );
     SetComplete;
end;

function TdmPortalTress.KioskoComidasAnterior(Empresa: OleVariant; Empleado, Year, Tipo, Numero: Integer; out Periodo, Totales: OleVariant): OleVariant;
begin
     GetPeriodoAnterior( Empresa, Empleado, Year, Tipo, Numero );
     Result := GetComidas( Empresa, Empleado );
     Periodo := GetNominaDatos( Empresa, Empleado );
     Totales := GetComidasTotales( Empresa, Empleado );
     SetComplete;
end;

function TdmPortalTress.KioskoComidasSiguiente(Empresa: OleVariant; Empleado, Year, Tipo, Numero: Integer; out Periodo, Totales: OleVariant): OleVariant;
begin
     GetPeriodoSiguiente( Empresa, Empleado, Year, Tipo, Numero );
     Result := GetComidas( Empresa, Empleado );
     Periodo := GetNominaDatos( Empresa, Empleado );
     Totales := GetComidasTotales( Empresa, Empleado );
     SetComplete;
end;

function TdmPortalTress.KioscoCursosTomados(Empresa: OleVariant; Empleado: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          Result := OpenSQL( Empresa, Format( GetSQLScript( Q_KIOSKO_CURSOS_TOMADOS ), [ Empleado ] ), True );
     end;
     SetComplete;
end;

function TdmPortalTress.SetClaveEmpleadoKiosko(Empresa: OleVariant; Empleado: Integer; const Clave: WideString): Integer;
var
   Encriptada: string;
begin
     Result := 0;
     Encriptada := ZetaServerTools.Encrypt( Clave );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          ExecSQL( Empresa, Format( GetSQLScript( Q_KIOSKO_UPDATE_CLAVE ), [ EntreComillas( Encriptada ), Empleado ] ) );
     end;
     SetComplete;
end;

function TdmPortalTress.ReseteaPassword(Empresa, Empleados: OleVariant): Integer;
begin
     Result := 0;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          EjecutaAndFree( Format( GetSQLScript( Q_EMPLEADOS_ACTUALIZA_PASS ), [ EntreComillas( ' ' ), Integer( Empleados ) ] ) );
     end;
     SetComplete;
end;

function TdmPortalTress.EnrolDataGet( Empresa: OleVariant; Empleado: Integer ): OleVariant;
var
   Parametros: TZetaParams;
   FCursor: TZetaCursor;
begin
     Parametros := TZetaParams.Create;
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             FCursor := CreateQuery( Format( GetSQLScript( Q_EMPLEADOS_ENROL_GET ), [ Empleado ] ) );
        end;
        try
           with FCursor do
           begin
                Active := True;
                if not IsEmpty then
                begin
                     with Parametros do
                     begin
                          AddInteger( 'CB_CODIGO', FieldByName( 'CB_CODIGO' ).AsInteger );
                          AddString( 'CB_APE_PAT', FieldByName( 'CB_APE_PAT' ).AsString );
                          AddString( 'CB_APE_MAT', FieldByName( 'CB_APE_MAT' ).AsString );
                          AddString( 'CB_NOMBRES', FieldByName( 'CB_NOMBRES' ).AsString );
                          AddString( 'CB_PORTAL', FieldByName( 'CB_PORTAL' ).AsString );
                          AddString( 'CB_E_MAIL', FieldByName( 'CB_E_MAIL' ).AsString );
                          AddString( 'CB_USRNAME', FieldByName( 'CB_USRNAME' ).AsString );
                          AddString( 'CB_PASSWRD', ZetaServerTools.Decrypt( FieldByName( 'CB_PASSWRD' ).AsString ) );
                     end;
                end;
                Active := False;
           end;
        finally
               FreeAndNil( FCursor );
        end;
        Result := Parametros.VarValues;
     finally
            FreeAndNil( Parametros );
     end;
     SetComplete;
end;

function TdmPortalTress.EnrolDataSet( Empresa: OleVariant; Empleado: Integer; Valores: OleVariant ): Smallint;
var
   Parametros: TZetaParams;
   FCursor: TZetaCursor;
begin
     Result := 0;
     Parametros := TZetaParams.Create;
     try
        Parametros.VarValues := Valores;
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             FCursor := CreateQuery( Format( GetSQLScript( Q_EMPLEADOS_ENROL_SET ), [ Empleado,
                                                                                      Parametros.ParamByName( 'CB_PORTAL' ).AsString,
                                                                                      Parametros.ParamByName( 'CB_E_MAIL' ).AsString,
                                                                                      Parametros.ParamByName( 'CB_USRNAME' ).AsString,
                                                                                      ZetaServerTools.Encrypt( Parametros.ParamByName( 'CB_PASSWRD' ).AsString ) ] ) );
             try
                EmpiezaTransaccion;
                try
                   Ejecuta( FCursor );
                   TerminaTransaccion( True );
                except
                      on Error: Exception do
                      begin
                           RollbackTransaccion;
                           raise;
                      end;
                end;
             finally
                    FreeAndNil( FCursor );
             end;
        end;
     finally
            FreeAndNil( Parametros );
     end;
     SetComplete;
end;

function TdmPortalTress.GetServerData: OleVariant;
begin
     Result := VarArrayOf( [ Now ] );
     SetComplete;
end;

{$ifndef DOS_CAPAS}
initialization
  TComponentFactory.Create(ComServer, TdmPortalTress, Class_dmPortalTress, ciMultiInstance, ZetaServerTools.GetThreadingModel);
{$endif}
end.
