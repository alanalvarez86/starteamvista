unit DBuzon;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComServ, ComObj, VCLCom, StdVcl, {BdeMts,}
     DataBkr, DBClient, MtsRdm, Mtx, Db, Variants,
     DSuperGlobal,
     DZetaServerProvider,
     ZetaServerDataSet,
     ZetaCommonClasses;

type
  eAllScripts = ( eInsertaWOutBox,
                  eListaUsuarios,
                  eCorreos,
                  eObtieneGlobal,
                  eBorraGlobales,
                  eGrabaGlobales,
                  eUsuariosRoles,
                  eUserRolInsert,
                  eUsuarioRolesDelete,
                  eSistUsuarios );

  eTipoTabla =( eUsuarios, eRol );

  TBuzon = class( TObject )
  private
    { Private declarations }
    FInsertaWOutbox: TZetaCursor;
    oZetaProvider: TdmZetaServerProvider;
    function GetAllDay( const dValue: TDateTime ): TDateTime;
    function DateTimeToStrSQL( const dFecha: TDateTime ): String;
    function GetServidorCorreos: String;
    function GetUsuarioCorreos: String;
    function GetUsuarioPassword: String;
    function GetPuertoCorreos: Integer;
    function GetTimeoutCorreos: Integer;
    function GetNombreTabla(const eTabla: eTipoTabla): string;
    function SetFiltroGrupos( const iGrupo: Integer; const sPrefix: String = '' ): String;
  protected
    { Protected declarations }
    function LeerGlobales( const iNumero: Integer ): String;
    procedure EscribeGlobales( Empresa:OleVariant; const iNumero: Integer; const sTexto: String );
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    function GetRutaVirtual: String;
    function GetScript( const eScript: eAllScripts ): String;
    function ListaUsuarios: OleVariant;
    function GrabaBuzon( const Parametros: TZetaParams ): Integer;
    function ConsultaBitacoraCorreos( const Parametros: OleVariant ): OleVariant;
    function GetGlobalesEvaluacion: OleVariant;
    function GrabaGlobalesEvaluacion( Valores: OleVariant ): Integer;
    function GetRolesUsuario(Usuario: Integer): OleVariant;
    function GetUsuarios(Grupo: Integer): OleVariant;
    function GrabaUsuarios(Usuario: Integer; Delta: OleVariant; out ErrorCount: Integer; oDataSet: TServerDataSet ): OleVariant;
    procedure SetTablaInfo(const eTabla: eTipoTabla);
    procedure GetParametrosURL( const sEmpresa: String; const Usuario: Integer; var Empleado: Integer; var Nombre: WideString );
  end;

const
     K_USUARIOS_PREFIX = 'U1.';
     K_CAMPOS_USUARIO = '%0:sUS_CODIGO, %0:sUS_CORTO, %0:sGR_CODIGO, %0:sUS_NIVEL, %0:sUS_NOMBRE, %0:sUS_PASSWRD, %0:sUS_BLOQUEA, %0:sUS_CAMBIA, '+
                        '%0:sUS_FEC_IN, %0:sUS_FEC_OUT, %0:sUS_FEC_SUS, %0:sUS_DENTRO, %0:sUS_ARBOL, %0:sUS_FORMA, %0:sUS_BIT_LST, '+
                        '%0:sUS_FEC_PWD, %0:sUS_EMAIL, %0:sUS_FORMATO, %0:sUS_LUGAR, %0:sUS_MAQUINA, %0:sCM_CODIGO, %0:sCB_CODIGO, %0:sUS_JEFE, '+
                        '%0:sUS_BIO_ID, %0:sUS_FALLAS, %0:sUS_PAGINAS, %0:sUS_PAG_DEF, %0:sUS_ANFITRI, %0:sUS_DOMAIN';

implementation

uses ZetaCommonTools;

{ ********** TBuzon ************** }

constructor TBuzon.Create;
begin
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Nil );
end;

destructor TBuzon.Destroy;
begin
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
     inherited Destroy;
end;

function TBuzon.GetAllDay( const dValue: TDateTime ): TDateTime;
begin
     Result := Trunc( dValue ) + EncodeTime( 23, 59, 59, 0 );
end;

function TBuzon.DateTimeToStrSQL( const dFecha: TDateTime ): String;
begin
     Result := FormatDateTime( 'mm/dd/yyyy hh:nn:ss', dFecha );
end;

function TBuzon.GetNombreTabla(const eTabla: eTipoTabla): string;
begin
     case eTabla of
          eUsuarios: Result := 'USUARIO';
          eRol: Result := 'ROL';
     end;
end;

procedure TBuzon.SetTablaInfo(const eTabla: eTipoTabla);
begin
     with oZetaProvider.TablaInfo do
     begin
          case eTabla of
              eUsuarios: SetInfo( 'USUARIO', Format( K_CAMPOS_USUARIO, [ '' ] ), 'US_CODIGO' );
          else
              SetInfo( GetNombreTabla( eTabla ), 'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO', 'TB_CODIGO' );
          end;
     end;
end;

function TBuzon.GetScript( const eScript: eAllScripts ): String;
begin
     case eScript of
          eInsertaWOutBox:       Result := 'insert into WOUTBOX( WO_GUID, WO_TO, WO_CC, WO_SUBJECT, WO_BODY, WO_FROM_NA, WO_FROM_AD, WO_SUBTYPE, WO_FEC_IN, US_ENVIA, US_RECIBE )'+
                                           'values( NewId(), :WO_TO, :WO_CC, :WO_SUBJECT, :WO_BODY, :WO_FROM_NA, :WO_FROM_AD, :WO_SUBTYPE, :WO_FEC_IN, :US_ENVIA, :US_RECIBE )';
          eListaUsuarios:        Result := 'select US_CODIGO, US_NOMBRE, US_EMAIL, CB_CODIGO, CM_CODIGO, US_JEFE from USUARIO ';
          eCorreos:              Result := 'select TOP %d WO_FROM_NA, WO_FROM_AD, WO_TO, WO_CC, WO_SUBJECT, WO_BODY, WO_SUBTYPE, '+
                                           'WO_FEC_IN, WO_ENVIADO, WO_FEC_OUT, WO_GUID, WP_FOLIO, WO_STATUS, WO_LOG, US_ENVIA, US_RECIBE '+
                                           'from WOUTBOX '+
                                           'where %s order by WO_FEC_IN';
          eObtieneGlobal:        Result := 'select GL_FORMULA from GLOBAL where( GL_CODIGO = %d ) ';
          eBorraGlobales:        Result := 'delete from GLOBAL where ( GL_CODIGO = %d )';
          eGrabaGlobales:        Result := 'insert into GLOBAL ( GL_CODIGO, GL_FORMULA ) ' +
                                           'values ( %d, ''%s'' )';
          eUsuariosRoles:        Result := 'select USER_ROL.RO_CODIGO, USER_ROL.US_CODIGO, ROL.RO_NOMBRE, USUARIO.US_NOMBRE from USER_ROL '+
                                           'join ROL on ( ROL.RO_CODIGO = USER_ROL.RO_CODIGO ) '+
                                           'join USUARIO on ( USUARIO.US_CODIGO = USER_ROL.US_CODIGO ) '+
                                           'where ( USER_ROL.US_CODIGO = %d ) order by USER_ROL.RO_CODIGO';
          eUserRolInsert:        Result := 'insert into USER_ROL( RO_CODIGO, US_CODIGO ) values ( :RO_CODIGO, :US_CODIGO )';
          eUsuarioRolesDelete:   Result := 'delete from USER_ROL where ( USER_ROL.US_CODIGO = %d )';
          eSistUsuarios:         Result := 'select ' + Format( K_CAMPOS_USUARIO, [ K_USUARIOS_PREFIX ] ) + ','+
                                           '( select U2.US_NOMBRE from USUARIO U2 where ( U2.US_CODIGO = U1.US_JEFE ) ) as NOM_JEFE, '+
                                           'GRUPO.GR_DESCRIP GRUPO '+
                                           'from USUARIO U1 '+
                                           'left outer join GRUPO on ( GRUPO.GR_CODIGO = U1.GR_CODIGO ) '+
                                           'where ( U1.US_CODIGO <> 0 ) %s '+
                                           'order by U1.US_CODIGO';
          else
         Result := VACIO;
     end;
end;

function TBuzon.GrabaBuzon( const Parametros: TZetaParams ): Integer;
begin
     Result := 0;
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          FInsertaWOutbox := CreateQuery( GetScript( eInsertaWOutBox ) );
          try
             ParamAsString( FInsertaWOutbox, 'WO_TO', Parametros.ParamByName('WO_TO').AsString );
             ParamAsString( FInsertaWOutbox, 'WO_CC', Parametros.ParamByName('WO_CC').AsString );
             ParamAsString( FInsertaWOutbox, 'WO_SUBJECT', Parametros.ParamByName('WO_SUBJECT').AsString );
             //ParamAsBlob( FInsertaWOutbox, 'WO_BODY', Parametros.ParamByName('WO_BODY').AsBlob );
             ParamAsBlob( FInsertaWOutbox, 'WO_BODY', Parametros.ParamByName('WO_BODY').AsString );
             ParamAsString( FInsertaWOutbox, 'WO_FROM_NA', Parametros.ParamByName('WO_FROM_NA').AsString );
             ParamAsString( FInsertaWOutbox, 'WO_FROM_AD', Parametros.ParamByName('WO_FROM_AD').AsString );
             ParamAsInteger( FInsertaWOutbox, 'WO_SUBTYPE', Parametros.ParamByName('WO_SUBTYPE').AsInteger );
             ParamAsDate( FInsertaWOutbox, 'WO_FEC_IN', Parametros.ParamByName('WO_FEC_IN').AsDate );
             ParamAsInteger( FInsertaWOutbox, 'US_ENVIA', Parametros.ParamByName('US_ENVIA').AsInteger );
             ParamAsInteger( FInsertaWOutbox, 'US_RECIBE', Parametros.ParamByName('US_RECIBE').AsInteger );
             EmpiezaTransaccion;
             try
                Result := Ejecuta( FInsertaWOutbox );
                TerminaTransaccion( TRUE );
             except
                   on Error: Exception do
                   begin
                        RollBackTransaccion;
                        raise;
                   end;
             end;
          finally
                 FreeAndNil( FInsertaWOutbox );
          end;
     end;
end;

function TBuzon.ListaUsuarios: OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          EmpiezaTransaccion;
          try
             Result:= OpenSQL( EmpresaActiva, GetScript( eListaUsuarios ), TRUE );
             TerminaTransaccion( TRUE );
          except
               on Error: Exception do
               begin
                    RollBackTransaccion;
                    raise;
               end;
          end;
     end;
end;

function TBuzon.ConsultaBitacoraCorreos( const Parametros: OleVariant ): OleVariant;
const
     K_MAX_RECORDS = 1000;
     K_CORREO_ENVIADO = '( WO_ENVIADO = ''%s'' )';
     K_CORREO_REMITENTE = '( WO_FROM_NA like ''%s'' )';
     K_CORREO_TEMA = '( WO_SUBJECT like ''%s'' )';
     K_CORREO_FECHA_IN = '( WO_FEC_IN between ''%s'' and ''%s'' )';
     K_CORREO_FECHA_OUT = '( WO_FEC_OUT between ''%s'' and ''%s'' )';
var
   sFiltro, sRemitente, sTema, sEnviado: String;
   lEnviado, lFechaGeneracion: Boolean;
   dInicio, dFinal: TDate;
begin
      with oZetaProvider do
      begin
           EmpresaActiva := Comparte;
           AsignaParamList( Parametros );
           with ParamList do
           begin
                dInicio := ParamByName( 'Inicio' ).AsDate;
                dFinal := GetAllDay( ParamByName( 'Final' ).AsDate );
                lFechaGeneracion := ParamByName( 'FechaGeneracion' ).AsBoolean;
                sRemitente := ParamByName( 'Remitente' ).AsString;
                sTema := ParamByName( 'Tema' ).AsString;
                lEnviado := ParamByName( 'FiltrarStatus' ).AsBoolean;
                sEnviado := ZetaCommonTools.zBoolToStr( ParamByName( 'Enviado' ).AsBoolean );
           end;
           if lFechaGeneracion then
              sFiltro := K_CORREO_FECHA_IN
           else
               sFiltro := K_CORREO_FECHA_OUT;
           sFiltro := Format( sFiltro, [ DateTimeToStrSQL( dInicio ), DateTimeToStrSQL( dFinal ) ] );
           if ZetaCommonTools.StrLleno( sRemitente ) then
              sFiltro := ZetaCommonTools.ConcatFiltros( sFiltro, Format( K_CORREO_REMITENTE, [ '%' + sRemitente + '%' ] ) );
           if ZetaCommonTools.StrLleno( sTema ) then
              sFiltro := ZetaCommonTools.ConcatFiltros( sFiltro, Format( K_CORREO_TEMA, [ '%' + sTema + '%' ] ) );
           if lEnviado then
              sFiltro := ZetaCommonTools.ConcatFiltros( sFiltro, Format( K_CORREO_ENVIADO, [ sEnviado ] ) );
           Result := OpenSQL( Comparte, Format( GetScript( eCorreos ), [ K_MAX_RECORDS, sFiltro ] ), True );
      end;
end;

function TBuzon.GetServidorCorreos: String;
begin
     Result := '';
     //Result := LeerGlobales( ZGlobalComparte.K_WORKFLOW_EMAIL_SERVER );
end;

function TBuzon.GetUsuarioCorreos: String;
begin
     Result := '';
     //Result := LeerGlobales( ZGlobalComparte.K_WORKFLOW_EMAIL_USER );
end;

function TBuzon.GetUsuarioPassword: String;
begin
     Result := '';
     //Result := LeerGlobales( ZGlobalComparte.K_WORKFLOW_EMAIL_PASSWORD );
end;

function TBuzon.GetPuertoCorreos: Integer;
begin
     Result := 0;
    // Result := StrToIntDef( LeerGlobales( ZGlobalComparte.K_WORKFLOW_EMAIL_PORT ), 0 );
end;

function TBuzon.GetTimeoutCorreos: Integer;
begin
     Result := 0;
     //Result := StrToIntDef( LeerGlobales( ZGlobalComparte.K_WORKFLOW_EMAIL_TIMEOUT ), 0 );
end;

function TBuzon.GetRutaVirtual: String;
begin
     Result := '';
     //Result := LeerGlobales( ZGlobalComparte.K_EVALUACION_RUTA_WEB );
end;

function TBuzon.LeerGlobales( const iNumero: Integer ): String;
var
   FDataset: TZetaCursor;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          FDataset := CreateQuery( Format( GetScript( eObtieneGlobal ), [ iNumero ] ) );
          try
             with FDataset do
             begin
                  Active := True;
                  if EOF then
                     Result := VACIO
                  else
                      Result := FieldByName( 'GL_FORMULA' ).AsString;
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataset );
          end;
     end;
end;

function TBuzon.GetGlobalesEvaluacion: OleVariant;
var
  oArreglo: Variant;
begin
     oArreglo := VarArrayCreate( [0, 5], varVariant );
     oArreglo[ 0 ] := GetServidorCorreos;
     oArreglo[ 1 ] := GetUsuarioCorreos;
     oArreglo[ 2 ] := GetUsuarioPassword;
     oArreglo[ 3 ] := GetPuertoCorreos;
     oArreglo[ 4 ] := GetTimeoutCorreos;
     oArreglo[ 5 ] := GetRutaVirtual;
     Result := oArreglo;
     {Result := VarArrayCreate( [ GetServidorCorreos,
                             GetUsuarioCorreos,
                             GetUsuarioPassword,
                             GetPuertoCorreos,
                             GetTimeoutCorreos ] );}
end;

procedure TBuzon.EscribeGlobales( Empresa:OleVariant; const iNumero: Integer; const sTexto: String );
begin
     //EscribeBitacora( Empresa, iNumero, sTexto );
     with oZetaProvider do
     begin
          ExecSQL( Empresa, Format( GetScript( eBorraGlobales ), [ iNumero ] ) );
          ExecSQL( Empresa, Format( GetScript( eGrabaGlobales ), [ iNumero, sTexto ] ) );
     end;
end;

function TBuzon.GrabaGlobalesEvaluacion( Valores: OleVariant ): Integer;
begin
     Result := 0;
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          EmpiezaTransaccion;
          try
             //EscribeGlobales( EmpresaActiva, ZGlobalComparte.K_WORKFLOW_EMAIL_SERVER, Valores[ EMAIL_SERVER ] );
             //EscribeGlobales( EmpresaActiva, ZGlobalComparte.K_WORKFLOW_EMAIL_USER, Valores[ EMAIL_USER ] );
             //EscribeGlobales( EmpresaActiva, ZGlobalComparte.K_WORKFLOW_EMAIL_PASSWORD, Valores[ EMAIL_PASSWORD ] );
             //EscribeGlobales( EmpresaActiva, ZGlobalComparte.K_WORKFLOW_EMAIL_PORT, IntToStr( Valores[ EMAIL_PORT ] ) );
             //EscribeGlobales( EmpresaActiva, ZGlobalComparte.K_WORKFLOW_EMAIL_TIMEOUT, IntToStr( Valores[ EMAIL_TIMEOUT ] ) );
             //EscribeGlobales( EmpresaActiva, ZGlobalComparte.K_EVALUACION_RUTA_WEB, Valores[ RUTA_VIRTUAL ] );
             TerminaTransaccion( True );
          except
                on Error: Exception do
                begin
                     Result := 1;
                     TerminaTransaccion( False );
                end;
          end;
     end;
end;

function TBuzon.GetRolesUsuario(Usuario: Integer): OleVariant;
begin
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Comparte;
             Result := OpenSQL( Comparte, Format( GetScript( eUsuariosRoles ), [ Usuario ] ), True );
        end;
     except
           raise;
     end;
end;

function TBuzon.GrabaUsuarios(Usuario: Integer; Delta: OleVariant; out ErrorCount: Integer; oDataSet: TServerDataSet ): OleVariant;
begin
     if VarIsNull( Delta ) then
     begin
          ErrorCount := 0;
          Result := Null;
     end
     else
         with oZetaProvider do
         begin
              SetTablaInfo( eUsuarios );
              Result := GrabaTabla( Comparte, Delta, ErrorCount );
         end;
end;

function TBuzon.SetFiltroGrupos( const iGrupo: Integer; const sPrefix: String = '' ): String;
begin
     if ( iGrupo <> ZetaCommonClasses.D_GRUPO_SIN_RESTRICCION ) then
     begin
          {$ifdef INTERBASE}
          Result := Format( '( %0:sGR_CODIGO in ( select GR_HIJO from GET_DESCENDENCIA( %1:d ) ) )', [ sPrefix, iGrupo ] );
          {$endif}
          {$ifdef MSSQL}
          Result := Format( '( %0:sGR_CODIGO in ( select GR_HIJO from dbo.GET_DESCENDENCIA( %1:d ) ) )', [ sPrefix, iGrupo ] );
          {$endif}
     end;
end;

function TBuzon.GetUsuarios(Grupo: Integer): OleVariant;
var
   sFiltro: String;
begin
     sFiltro := SetFiltroGrupos( Grupo, K_USUARIOS_PREFIX );
     if ZetaCommonTools.StrLleno( sFiltro ) then
        sFiltro := Format( 'and ( %s )', [ sFiltro ] );
     with oZetaProvider do
     begin
          Result := OpenSQL( Comparte, Format( GetScript( eSistUsuarios ), [ sFiltro ] ), True );
     end;
end;

procedure TBuzon.GetParametrosURL( const sEmpresa: String; const Usuario: Integer; var Empleado: Integer; var Nombre: WideString );
const
     Q_OBTENER_EMPLEADO = 'select CB_CODIGO, US_NOMBRE, US_CODIGO from USUARIO where US_CODIGO = :Usuario ';
var
   FRutaVirtual: TZetaCursor;
begin
     //RutaVirtual := GetRutaVirtual;
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          FRutaVirtual := CreateQuery( Q_OBTENER_EMPLEADO );
          try
              ParamAsInteger( FRutaVirtual, 'Usuario', Usuario );
              FRutaVirtual.Open;
              Empleado := FRutaVirtual.FieldByName('CB_CODIGO').AsInteger;
              Nombre := FRutaVirtual.FieldByName('US_NOMBRE').AsString;
              FRutaVirtual.Close;
          finally
                 FreeAndNil( FRutaVirtual );
          end;
     end;
end;

end.
