unit DAnualNomina_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 01/16/2012 1:30:00 PM from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3win_20_Vsn_ISR\MTS\DAnualNomina.tlb (1)
// LIBID: {9C27C751-B4B6-11D3-8E68-0050DA04EAA0}
// LCID: 0
// Helpfile: 
// HelpString: DAnualNomina Library
// DepndLst: 
//   (1) v1.0 Midas, (C:\Windows\SysWOW64\midas.dll)
//   (2) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, Midas, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  DAnualNominaMajorVersion = 1;
  DAnualNominaMinorVersion = 0;

  LIBID_DAnualNomina: TGUID = '{9C27C751-B4B6-11D3-8E68-0050DA04EAA0}';

  IID_IdmServerAnualNomina: TGUID = '{9C27C752-B4B6-11D3-8E68-0050DA04EAA0}';
  CLASS_dmServerAnualNomina: TGUID = '{9C27C754-B4B6-11D3-8E68-0050DA04EAA0}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerAnualNomina = interface;
  IdmServerAnualNominaDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerAnualNomina = IdmServerAnualNomina;


// *********************************************************************//
// Interface: IdmServerAnualNomina
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9C27C752-B4B6-11D3-8E68-0050DA04EAA0}
// *********************************************************************//
  IdmServerAnualNomina = interface(IAppServer)
    ['{9C27C752-B4B6-11D3-8E68-0050DA04EAA0}']
    function CalcularAguinaldo(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CalcularPTU(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CalcularRepartoAhorro(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function ISPTAnual(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function DeclaraCredito(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function PagarAguinaldoGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function PagarAguinaldoLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function PagarAguinaldo(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function PTUPagoGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function PTUPagoLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function PTUPago(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function ISPTPagoGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function ISPTPagoLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function ISPTPago(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function PagarAhorroGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function PagarAhorroLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function PagarAhorro(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CerrarAhorros(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CerrarPrestamos(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CalcularTiempos(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CalcularTiemposGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CreditoAplicado(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CreditoAplicadoGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CreditoAplicadoLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function AfectarLabor(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function DeclaracionAnual(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function DeclaracionAnualGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function DeclaracionAnualLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetDeclaracionGlobales(Empresa: OleVariant): OleVariant; safecall;
    function GrabaDeclaracionGlobales(Empresa: OleVariant; oDelta: OleVariant; 
                                      out ErrorCount: Integer): OleVariant; safecall;
    function RecalculaTotPerGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function ActualizaTotPerLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function DeclaracionAnualCierre(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function PrevioISR(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerAnualNominaDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9C27C752-B4B6-11D3-8E68-0050DA04EAA0}
// *********************************************************************//
  IdmServerAnualNominaDisp = dispinterface
    ['{9C27C752-B4B6-11D3-8E68-0050DA04EAA0}']
    function CalcularAguinaldo(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 1;
    function CalcularPTU(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 2;
    function CalcularRepartoAhorro(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 3;
    function ISPTAnual(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 4;
    function DeclaraCredito(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 5;
    function PagarAguinaldoGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 6;
    function PagarAguinaldoLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 7;
    function PagarAguinaldo(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 8;
    function PTUPagoGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 9;
    function PTUPagoLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 10;
    function PTUPago(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 11;
    function ISPTPagoGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 12;
    function ISPTPagoLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 13;
    function ISPTPago(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 14;
    function PagarAhorroGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 15;
    function PagarAhorroLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 16;
    function PagarAhorro(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 17;
    function CerrarAhorros(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 18;
    function CerrarPrestamos(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 19;
    function CalcularTiempos(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 20;
    function CalcularTiemposGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 21;
    function CreditoAplicado(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 22;
    function CreditoAplicadoGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 23;
    function CreditoAplicadoLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 24;
    function AfectarLabor(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 25;
    function DeclaracionAnual(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 26;
    function DeclaracionAnualGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 27;
    function DeclaracionAnualLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 28;
    function GetDeclaracionGlobales(Empresa: OleVariant): OleVariant; dispid 29;
    function GrabaDeclaracionGlobales(Empresa: OleVariant; oDelta: OleVariant; 
                                      out ErrorCount: Integer): OleVariant; dispid 30;
    function RecalculaTotPerGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 301;
    function ActualizaTotPerLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 302;
    function DeclaracionAnualCierre(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 303;
    function PrevioISR(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 320;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer; 
                             out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                           Options: Integer; const CommandText: WideString; var Params: OleVariant; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerAnualNomina provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerAnualNomina exposed by              
// the CoClass dmServerAnualNomina. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerAnualNomina = class
    class function Create: IdmServerAnualNomina;
    class function CreateRemote(const MachineName: string): IdmServerAnualNomina;
  end;

implementation

uses ComObj;

class function CodmServerAnualNomina.Create: IdmServerAnualNomina;
begin
  Result := CreateComObject(CLASS_dmServerAnualNomina) as IdmServerAnualNomina;
end;

class function CodmServerAnualNomina.CreateRemote(const MachineName: string): IdmServerAnualNomina;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerAnualNomina) as IdmServerAnualNomina;
end;

end.
