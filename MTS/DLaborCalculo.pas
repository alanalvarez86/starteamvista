unit DLaborCalculo;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, DB, DBClient,
     Variants,
     DQueries,
     DZetaServerProvider,
     ZCreator,
     ZetaServerDataset,
     ZetaCommonClasses,
     ZetaCommonLists;

{$define CAMBIO_AREA}
{$define IMSS_VACA}   //Corregir d�as IMSS cuando hay vacaciones

const
     K_ANCHO_TERMINAL_ID = 10;
     K_ANCHO_MODULADOR = 1;
     K_ANCHO_TMUERTO = 3;
     K_ANCHO_HH_MM = 4;
     K_ANCHO_OPERACION = 10;
     K_ANCHO_ORDEN = 20;
     K_ANCHO_PARTE = 15;
     K_ANCHO_PUESTO = 6;
     {$ifdef HYUNDAI}
     K_ANCHO_AREA = 10;
     K_ANCHO_PARTE18 = 18;
     {$else}
     K_ANCHO_AREA = 6;
     {$endif}
     K_OFFSET_PRODUCCION = 0;
     K_OFFSET_ESPECIALES = 1000;
     K_OFFSET_TIE_MUERTO = 2000;
     K_OFFSET_MULTI_EXC = 10000;
     K_SUFIJO_AREA = 'x9!#M';

type
  eKindLectura = ( ktDesconocido,
                   ktChecada,
                   ktPrecalculada,
                   ktFija,
                   ktEntrada,
                   ktSalida,
                   ktTiempoMuerto,
                   ktTiempoMuertoChecado,
                   ktBreak,
                   ktArea );
  THoraLabor = String[ 4 ];
  TAgregaBreak = procedure( const sFinal: THoraLabor; const iDuracion: Integer; const sTipo: TCodigo ) of object;
  TDatosTarjeta = class(TObject)
    Ordinarias: TDiasHoras;
    Extras: TDiasHoras;
    Dobles: TDiasHoras;
    Triples: TDiasHoras;
  end;
  TBreak = class( TObject )
  private
    { Private declarations }
    FDuracion: Integer;
    FTipo: TCodigo;
    FCodigo: TCodigo;
    FInicio: THoraLabor;
    FFinal: THoraLabor;
    procedure SetDuracion(const Value: Integer);
    procedure SetFinal;
    procedure SetInicio(const Value: THoraLabor);
  public
    { Public declarations }
    property Codigo: TCodigo read FCodigo write FCodigo;
    property Duracion: Integer read FDuracion write SetDuracion;
    property Inicio: THoraLabor read FInicio write SetInicio;
    property Final: THoraLabor read FFinal;
    property Tipo: TCodigo read FTipo write FTipo;
    constructor Create;
  end;
  TBreakList = class( TObject )
  private
    { Private Declarations }
    oZetaProvider: TdmZetaServerProvider;
    FItems: TList;
    function Add: TBreak;
    function GetCount: Integer;
    function GetItems(Index: Integer): TBreak;
    procedure Clear;
    procedure Delete(const Index: Integer);
    procedure Init;
  public
    { Public declarations }
    property Items[ Index: Integer ]: TBreak read GetItems;
    property Count: Integer read GetCount;
    constructor Create(oProvider: TdmZetaServerProvider);
    destructor Destroy; override;
    function AgregaBreaks( const sCodigo: TCodigo; const sInicial, sFinal: THoraLabor; Metodo: TAgregaBreak ): Boolean;
  end;
  TEmployeeChange = class( TObject )
  private
    { Private declarations }
    FArea: TCodigo;
    FHora: THoraLabor;
    FFecha: TDate;
  public
    { Public declarations }
    property Area: TCodigo read FArea write FArea;
    property Hora: THoraLabor read FHora write FHora;
    property Fecha: TDate read FFecha write FFecha;
  end;
  TEmployee = class( TObject )
  private
    { Private Declarations }
    oZetaProvider: TdmZetaServerProvider;
    FChanges: TList;
    FNumero: TNumEmp;
    function Add: TEmployeeChange;
    function GetCount: Integer;
    function GetChanges(Index: Integer): TEmployeeChange;
    procedure Clear;
    procedure Delete(const Index: Integer);
  public
    { Public declarations }
    property Numero: TNumEmp read FNumero write FNumero;
    property Changes[ Index: Integer ]: TEmployeeChange read GetChanges;
    property Count: Integer read GetCount;
    constructor Create(oProvider: TdmZetaServerProvider);
    destructor Destroy; override;
    procedure Init( const Dataset: TZetaCursor );
    function IsInArea( const sArea: TCodigo; const dValue: TDate; const sHora: THoraLabor ): Boolean;
  end;
  TEmployeeList = class( TObject )
  private
    { Private Declarations }
    oZetaProvider: TdmZetaServerProvider;
    FEmployees: TList;
    function Add: TEmployee;
    function GetCount: Integer;
    function GetEmployees(Index: Integer): TEmployee;
    procedure Clear;
    procedure Delete(const Index: Integer);
  public
    { Public declarations }
    property Employees[ Index: Integer ]: TEmployee read GetEmployees;
    property Count: Integer read GetCount;
    constructor Create(oProvider: TdmZetaServerProvider);
    destructor Destroy; override;
    procedure Init( const dValue: TDate );
  end;
  TArea = class( TObject )
  private
    { Private declarations }
    FCodigo: TCodigo;
    FDomingo: TCodigo;
    FLunes: TCodigo;
    FMartes: TCodigo;
    FMiercoles: TCodigo;
    FJueves: TCodigo;
    FViernes: TCodigo;
    FSabado: TCodigo;
    FOperacion: String;
    FTurno: eTurnoLabor;
    FPrimeraHora: THoraLabor;
    FTipo: eTipoArea;
  public
    { Public declarations }
    property Codigo: TCodigo read FCodigo write FCodigo;
    property Domingo: TCodigo read FDomingo write FDomingo;
    property Lunes: TCodigo read FLunes write FLunes;
    property Martes: TCodigo read FMartes write FMartes;
    property Miercoles: TCodigo read FMiercoles write FMiercoles;
    property Jueves: TCodigo read FJueves write FJueves;
    property Viernes: TCodigo read FViernes write FViernes;
    property Sabado: TCodigo read FSabado write FSabado;
    property Operacion: String read FOperacion write FOperacion;
    property Turno: eTurnoLabor read FTurno write FTurno;
    property Tipo: eTipoArea read FTipo write FTipo;
    property PrimeraHora: THoraLabor read FPrimeraHora write FPrimeraHora;
    constructor Create;
    function GetBreak(const dValue: TDate): TCodigo;
  end;
  TAreaList = class( TObject )
  private
    { Private Declarations }
    oZetaProvider: TdmZetaServerProvider;
    FItems: TList;
    function Add: TArea;
    function GetCount: Integer;
    function GetItems(Index: Integer): TArea;
    procedure Clear;
    procedure Delete(const Index: Integer);
    procedure Init;
  public
    { Public declarations }
    property Items[ Index: Integer ]: TArea read GetItems;
    property Count: Integer read GetCount;
    constructor Create(oProvider: TdmZetaServerProvider);
    destructor Destroy; override;
    function Find( const sArea: TCodigo ): Integer;
  end;
  TAChecada = class( TObject ) { Checadas de Asistencia }
  private
    { Private Declarations }
    FTipo: eTipoChecadas;
    FDescripcion: eDescripcionChecadas;
    FHoraReal: String;
    FHoraAjustada: String;
    function GetKind: eKindLectura;
  public
    { Public Declarations }
    property Tipo: eTipoChecadas read FTipo write FTipo;
    property Kind: eKindLectura read GetKind;
    property Descripcion: eDescripcionChecadas read FDescripcion write FDescripcion;
    property HoraReal: String read FHoraReal write FHoraReal;
    property HoraAjustada: String read FHoraAjustada write FHoraAjustada;
    function EsExtra: Boolean;
  end;
  TLKardex = class;
  TLChecada = class( TObject )  { Checadas de Labor }
  private
    { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
    FFechaA: TDate;
    FFechaR: TDate;
    FHoraA: THoraLabor;
    FHoraR: THoraLabor;
    FHoraI: THoraLabor;
    FOperacion: String;
    FOrden: String;
    FParte: String;
    FTerminalID: String;
    FDuracion: TDiasHoras;
    FTiempo: TDiasHoras;
    FCalculable: Boolean;
    FTipo: eTipoLectura;
    FKind: eKindLectura;
    FStatus: eStatusLectura;
    FModula1: TCodigo;
    FModula2: TCodigo;
    FModula3: TCodigo;
    FMuerto: TCodigo;
    FArea: TCodigo;
    FPuesto: TCodigo;
    FOrdinarias: TDiasHoras;
    FDobles: TDiasHoras;
    FTriples: TDiasHoras;
    FManual: Boolean;
    FEscribir: Boolean;
    FPiezas: TDiasHoras;
    FFolio: Integer;
    FCedula: Integer;
    function ChecadasCompara(Value: TLChecada): Integer;
    function ChecadasComparaAjustadas(Value: TLChecada): Integer;
    function ChecadasComparaAreas( Value: TLChecada ): Integer;
    function ChecadasComparaReales(Value: TLChecada): Integer;
    function DetermineKind( const eTipo: eTipoLectura ): eKindLectura;
    function GetDuracion: TDiasHoras;
    function GetDuracionDefinitiva: TDiasHoras;
    function GetExcepcion: Boolean;
    function GetKindText: String;
    function GetHoraA: THoraLabor;
    function GetHoraI: THoraLabor;
    function GetHoraR: THoraLabor;
    function GetInicio: THoraLabor;
    procedure SetDuracion(const Value: TDiasHoras);
    procedure SetDobles(const Value: TDiasHoras);
    procedure SetOrdinarias(const Value: TDiasHoras);
    procedure SetTriples(const Value: TDiasHoras);
    procedure AjustarExcepcionPrecalculada( Lista: TLKardex; Excepcion: TLChecada; const iPos: Integer );
    procedure AjustarExcepcionProduccion( Lista: TLKardex; Excepcion: TLChecada; const iPos: Integer;sEntrada:THoraLabor  );
  public
    { Public declarations }
    property FechaA: TDate read FFechaA write FFechaA;
    property FechaR: TDate read FFechaR write FFechaR;
    property HoraA: THoraLabor read GetHoraA write FHoraA;
    property HoraR: THoraLabor read GetHoraR write FHoraR;
    property HoraI: THoraLabor read GetHoraI write FHoraI; { Hora inicial de la operaci�n }
    property Inicio: THoraLabor read GetInicio;            { Hora Ajustada - Tiempo = probable hora de inicio para Precalculadas }
    property Operacion: String read FOperacion write FOperacion;
    property Orden: String read FOrden write FOrden;
    property Parte: String read FParte write FParte;
    property Piezas: TDiasHoras read FPiezas write FPiezas;
    property TerminalID: String read FTerminalID write FTerminalID;
    property Duracion: TDiasHoras read GetDuracion write SetDuracion;
    property Tiempo: TDiasHoras read FTiempo write FTiempo;
    property Calculable: Boolean read FCalculable write FCalculable;
    property Tipo: eTipoLectura read FTipo write FTipo;
    property Kind: eKindLectura read FKind write FKind;
    property KindText: String read GetKindText;
    property Status: eStatusLectura read FStatus write FStatus;
    property Modula1: TCodigo read FModula1 write FModula1;
    property Modula2: TCodigo read FModula2 write FModula2;
    property Modula3: TCodigo read FModula3 write FModula3;
    property TMuerto: TCodigo read FMuerto write FMuerto;
    property Area: TCodigo read FArea write FArea;
    property Puesto: TCodigo read FPuesto write FPuesto;
    property Ordinarias: TDiasHoras read FOrdinarias write SetOrdinarias;
    property Dobles: TDiasHoras read FDobles write SetDobles;
    property Triples: TDiasHoras read FTriples write SetTriples;
    property Manual: Boolean read FManual write FManual;
    property Escribir: Boolean read FEscribir write FEscribir;
    property Folio: Integer read FFolio write FFolio;
    property Cedula: Integer read FCedula write FCedula;
    property Excepcion: Boolean read GetExcepcion;
    constructor Create(oProvider: TdmZetaServerProvider);
    function EsArea: Boolean;
    function EsBreak: Boolean;
    function EsChecada: Boolean;
    function EsEntrada: Boolean;
    function EsMultilote: Boolean;
    function EsPrecalculada: Boolean;
    function EsSalida: Boolean;
    function EsTiempoMuerto: Boolean;
    function EsValida: Boolean;
    function FormatHora(const sValue: THoraLabor): THoraLabor;
    function PuedeEscribir: Boolean;
    function TieneDuracion: Boolean;
    procedure Assign(Value: TLChecada);
    procedure Ignorar;
  end;
  {Orden de Multilotes }
  TLOrden = class( TObject )
  private
    FCodigo:string;
    FPiezas: TDiasHoras;
    FProducto:string;
    FPosicion:Integer;
  protected
    constructor Create;
  public
    property Codigo : string read FCodigo write FCodigo;
    property Piezas : TDiasHoras read FPiezas write FPiezas;
    property Producto : string read FProducto write FProducto;
    property Posicion : Integer read FPosicion write FPosicion;
  end;

  TDatosCedula = class( TObject )
  private
    { Private declarations }
    FHayEmpleados: Boolean;
    FMultiLote: Boolean;
    FStatus: eStatusLectura;
    FTipo: eTipoCedula;
    FUsuario: Integer;
    FFolio: Integer;
    FOrden: String;
    FOperacion: String;
    FParte: String;
    FModula1: TCodigo;
    FModula3: TCodigo;
    FArea: TCodigo;
    FTiempoMuerto: TCodigo;
    FTiempo: TDiasHoras;
    FModula2: TCodigo;
    FFecha: TDate;
    FHora: THoraLabor;
    FFechaWorks: TDate;
    FHoraWorks: THoraLabor;
    FFechaBusqueda: TDate;
    FHoraBusqueda: THoraLabor;
    FFound: Boolean;
    FPorExplicar: TDiasHoras;
    FLotes:TList;
    FPiezas:TDiasHoras;
    function GetChecada: eTipoLectura;
    function GetFechaReal: TDate;
    function GetHoraReal: THoraLabor;
    procedure SetFecha(const dValue: TDate);
    procedure SetHora(const sValue: THoraLabor);
    function GetOrden( Index: Integer ): TLOrden;
    function AddOrden: TLOrden;

  public
    { Public declarations }
    property Folio: Integer read FFolio write FFolio;
    property Orden: String read FOrden write FOrden;
    property Parte: String read FParte write FParte;
    property Operacion: String read FOperacion write FOperacion;
    property Area: TCodigo read FArea write FArea;
    property Tipo: eTipoCedula read FTipo write FTipo;
    property Fecha: TDate read FFecha write SetFecha;
    property FechaReal: TDate read GetFechaReal;
    property FechaWorks: TDate read FFechaWorks;
    property FechaBusqueda: TDate read FFechaBusqueda;
    property Hora: THoraLabor read FHora write SetHora;
    property HoraReal: THoraLabor read GetHoraReal;
    property HoraWorks: THoraLabor read FHoraWorks;
    property HoraBusqueda: THoraLabor read FHoraBusqueda;
    property Status: eStatusLectura read FStatus write FStatus;
    property Modula1: TCodigo read FModula1 write FModula1;
    property Modula2: TCodigo read FModula2 write FModula2;
    property Modula3: TCodigo read FModula3 write FModula3;
    property TiempoMuerto: TCodigo read FTiempoMuerto write FTiempoMuerto;
    property Tiempo: TDiasHoras read FTiempo write FTiempo;
    property MultiLote: Boolean read FMultiLote write FMultiLote;
    property HayEmpleados: Boolean read FHayEmpleados write FHayEmpleados;
    property Usuario: Integer read FUsuario write FUsuario;
    property Checada: eTipoLectura read GetChecada;
    property Found: Boolean read FFound write FFound;
    property PorExplicar: TDiasHoras read FPorExplicar write FPorExplicar;
    property Piezas: TDiasHoras read FPiezas write FPiezas;
    property Lotes[ Index: Integer ]: TLOrden read GetOrden;
    function FaltaExplicar: Boolean;
    function PuedeExplicar: Boolean;
    procedure InitBusqueda( const rDuracion: TDiasHoras; const dBusqueda: TDate; const sHoraBusqueda: THoraLabor );
    procedure AjustarFechaHoraWorks( const eTurno: eTurnoLabor; const sHora: THoraLabor );
  end;
  TLKardex = class( TObject )
  private
    { Private Declarations }
    FCreator: TZetaCreator;
    FItems: TList;
    FChecadas: TList;
    FAreas: TAreaList;
    FBreaks: TBreakList;
    FBreakAdded: Boolean;
    FEmpleado: TNumEmp;
    FFecha: TDate;
    {$ifdef CAMBIO_AREA}
    FPendiente: TDiasHoras;
    FUltimaChecada: THoraLabor;
    FAreaActiva: TCodigo;
    {$else}
    FMaxAreaChange: Integer;
    {$endif}
    FProcesarExcepciones: Boolean;
    FProcesarExcecionDifAreas :Boolean;
    FPuesto: TCodigo;
    FChecaTarjeta: Boolean;
    FCheco: Boolean;
    FChecoInOut: Boolean;
    FAsistio: Boolean;
    FStatus: eStatusAusencia;
    FExtraSabado: eHorasExtras;
    FExtraDescanso: eHorasExtras;
    {
    FParte: String;
    FOperacion: String;
    FOrden: String;
    }
    FAreaDefault: Integer;
    FHorario: TCodigo;
    FTipoTMuerto: TCodigo;
    FDatosHorario: TDatosHorarioDiario;
    FDatosTarjeta: TDatosTarjeta;
    FDatosCedula: TDatosCedula;
    FQChecadasLee: TZetaCursor;
    FQChecadasAgrega: TZetaCursor;
    FQChecadasBorra: TZetaCursor;
    FQAsistenciaLee: TZetaCursor;
    FQAreasLee: TZetaCursor;
    FQCedulas: TZetaCursor;
    FLotesCedula: TZetaCursor;
    function Add: TLChecada;
    function AddChecada: TAChecada;
    function BuscaCedula(const sArea: TCodigo; const dFecha: TDate; const sHora: THoraLabor; const rDuracion: TDiasHoras): Boolean;
    function CalculaExtras( const sFinal, sInicial: THoraLabor; const iChecada: Integer ): Integer;
    function CheckForDuplicate( const iPtr: Integer; const sHora: THoraLabor; const eKind: eKindLectura; const sArea: TCodigo ): Boolean;
    function CheckForValidEntries: Boolean;
    function DeterminarPuesto(const sPuesto: String): String;
    function ExisteArea( const sArea: TCodigo ): Boolean;
    function GetAreaDefault: TCodigo;
    function GetAreaPtr(const sArea: TCodigo): Integer;
    function GetCambioAreaAnterior(const iPos: Integer): Integer;
    function GetCambioAreaPosterior(const iPos: Integer): Integer;
    function GetChecada(Index: Integer): TAChecada;
    function GetCount: Integer;
    function GetCountChecadas: Integer;
    function GetItem( Index: Integer ): TLChecada;
    function GetQueries: TCommonQueries;
    function HayAreaDefault: Boolean;
    function oZetaProvider: TdmZetaServerProvider;
    function RevisaCambiosArea: Boolean;
    procedure Advertencia( const iEmpleado: TNumEmp; const sMensaje: String );
    procedure AgregaBreaks( const sCodigo: TCodigo; const sInicial, sFinal: THoraLabor );
    procedure AgregaChecadaAsistencia(const sAjustada, sReal: THoraLabor; const eKind: eKindLectura);
    procedure AgregaEntradaSalida;
    procedure AgregaHorarioEntrada;
    procedure AgregaHorarioSalida;
    procedure AgregarArea( const sHora: THoraLabor; const sArea: String );
    procedure AgregarAutomaticaChecada(const sFinal: THoraLabor; const sArea: TCodigo );
    procedure AgregarAutomaticaFijo(const sFinal: THoraLabor; const rDuracion: TDiasHoras; const sArea: TCodigo );
    procedure AgregarBreaks;
    procedure AgregarUnBreak(const sFinal: THoraLabor; const iDuracion: Integer; const sTipo: TCodigo);
    procedure AgregarTiempoMuertoChecada(const sFinal: THoraLabor; const sTipo, sArea: TCodigo);
    procedure AgregarTiempoMuertoFijo(const sFinal: THoraLabor; const rDuracion: TDiasHoras; const sTipo, sArea: TCodigo);
    procedure AgregaChecadaCedula;
    procedure AgregaChecadaCedulaMulti(WKFolio:Integer;dPiezas:TDiasHoras);
    procedure AjustarExtras;
    procedure AjustarPosterior( var dValue: TDate; var sHora: THoraLabor );
    procedure AjustarPosterioresSalida;
    procedure BuscarCedulas(const sArea: TCodigo; const dInicial, dFinal: TDate; const sInicial, sFinal: THoraLabor);
    procedure Calcular;
    procedure CalcularInicios;
    procedure CalcularTiempos;
    procedure CargarAreas;
    procedure CargarAsistencia;
    procedure CargarAsistenciaIgnorando(var iEntradas, iSalidas: Integer);
    procedure CargarAsistenciaNormal(var iEntradas, iSalidas: Integer);
    procedure CargarManuales;
    procedure Clear;
    procedure Delete( const Index: Integer );
    procedure DeleteChecada(const Index: Integer);
    procedure Descargar;
    procedure Desempata;
    procedure GetDatosHorarioDiario;
    procedure Ordena;
    procedure ProcesarExcepciones;
    procedure ProcesarExcepcionesVariables;
    procedure ProcesarExcepcionesPrecalculadas;
  protected
    { Protected declarations }
    property Asistio: Boolean read FAsistio write FAsistio;
    property Checo: Boolean read FCheco;
    property ChecoInOut: Boolean read FChecoInOut;
    property Items[ Index: Integer ]: TLChecada read GetItem;
    property Checadas[ Index: Integer ]: TAChecada read GetChecada;
    property Queries: TCommonQueries read GetQueries;
  public
    { Public Declarations }
    property Count: Integer read GetCount;
    property CountChecadas: Integer read GetCountChecadas;
    property ChecaTarjeta: Boolean read FChecaTarjeta write FChecaTarjeta;
    property DatosTarjeta: TDatosTarjeta read FDatosTarjeta write FDatosTarjeta;
    property Empleado: TNumEmp read FEmpleado write FEmpleado;
    property Fecha: TDate read FFecha write FFecha;
    property Horario: TCodigo read FHorario write FHorario;
    property Puesto: TCodigo read FPuesto write FPuesto;
    property Status: eStatusAusencia read FStatus write FStatus;
    property TipoTiempoMuerto: TCodigo read FTipoTMuerto write FTipoTMuerto;
    constructor Create( oCreator: TZetaCreator );
    destructor Destroy; override;
    function GetDataDump: String;
    procedure Procesar;
    procedure ProcesarBegin;
    procedure ProcesarEnd;
    procedure SetAreaDefault( const sArea: TCodigo );
  end;
  TLCalculo = class( TObject )
  private
    { Private Declarations }
    FCreator: TZetaCreator;
    FKardex: TLKardex;
    FCedula: TDatosCedula;
    FEmpleadoBuffer: TNumEmp;
    FFechaBuffer: TDate;
{$ifdef IMSS_VACA}
    FDiasCotizaSinFV: Boolean;
{$endif}
    FFestivoEnDescanso: Boolean;
    FTarjeta: TZetaCursor;
    FTarjetaAgrega: TZetaCursor;
    FLeePuesto: TZetaCursor;
    FCedulaEmpleados: TZetaCursor;
    FCedulaDelWorks: TZetaCursor;
    FCedulaInsWorks: TZetaCursor;
    FCedulaSetEmp: TZetaCursor;
    FCedulaPiezas: TZetaCursor;
    FBreakAgrega: TZetaCursor;
    FBreakArea: TZetaCursor;
    FBreakTarjeta: TZetaCursor;
    FIndirectos: TZetaCursor;
    FIndirectosTarjeta: TZetaCursor;
    {$ifdef DOS_CAPAS}
    FEmpleados: TZetaCursorLocate;
    {$else}
    {$ifdef INTERBASE}
    FEmpleados: TZetaCursorLocate;
    {$else}
    FEmpleados: TZetaCursor;
    {$endif}
    {$endif}
    FStart: TZetaCursor;
    FAplicar: TZetaCursor;
    FWorksDelete: TZetaCursor;
    FWorksDelNoAplica: TZetaCursor;
    FWorksDelSistema: TZetaCursor;
    FWorksStdTime: TZetaCursor;
    FFestivos: TListaFestivos;
    FBuffer: TServerDataset;
    FLogKardex: Boolean;
    FEmployeeList: TEmployeeList;
    FLotesCedula :TZetaCursor;
    FExisteWorks: TZetaCursor;
    function CheckFestivo(const sTurno: String; const dFestivo: TDate): Boolean;
    function GetAsistio( Dataset: TZetaCursor ): Boolean;
    function GetDatosTurno(const iEmpleado: TNumEmp; const dValue: TDate): TEmpleadoDatosTurno;
    function GetRitmos: TRitmos;
    function GetStatusHorario( const sTurno: String; const dReferencia: TDate ): TStatusHorario;
    function oZetaProvider: TdmZetaServerProvider;
    procedure GeneraCedulaBegin( const dInicial, dFinal: TDateTime );
    procedure GeneraCedulaCargar;
    procedure GeneraCedula;
    procedure GeneraCedulaEmpleado( const iEmpleado: TNumEmp );
    procedure GeneraCedulaEnd;
    procedure InsertaTarjetaBegin(const dInicial, dFinal: TDate);
    procedure InsertaTarjetaEnd;
  public
    { Public Declarations }
    property Ritmos: TRitmos read GetRitmos;
    constructor Create( oCreator: TZetaCreator );
    destructor Destroy; override;
    function CuantasCedulas( const dInicial, dFinal: TDateTime ): Integer;
    function GetDataDump: String;
    function GetDatosHorario(const iEmpleado: TNumEmp; const dValue: TDate): TDatosHorarioDiario;
    function ProcesarPendientes: OleVariant;
    function RegistrarBreak(const iEmpleado: TNumEmp; const iDuracion: Integer; dFecha: TDate; sHora: THoraLabor; var sTexto: String): Boolean;
    function GetAreasDiariasDataset( const iEmpleado: TNumEmp; const dFecha: TDate ): OleVariant;
    procedure AgregaTarjeta(const iEmpleado: TNumEmp; var dValue: TDate; var sHora: String);
    procedure AgregaTarjetaBegin(const dInicial, dFinal: TDate);
    procedure AgregaTarjetaEnd;
    procedure InsertaTarjeta(const iEmpleado: TNumEmp; const dValue: TDate);
    procedure AgregaUnaTarjeta(const iEmpleado: TNumEmp; const dValue: TDate; const StatusHorario: TStatusHorario );
    procedure CalcularTiempos(const dValue: TDate);
    procedure CalcularTiemposCargar( const iEmpleado: TNumEmp; const lCheca: Boolean; const sArea: String );
    procedure CalcularTiemposBegin( const dInicial, dFinal: TDate; Buffer: TServerDataset; const lLogKardex: Boolean );
    procedure CalcularTiemposEmpleado(const dValue: TDate; const iEmpleado: TNumEmp); overload;
    procedure CalcularTiemposEmpleado(const dValue: TDate); overload;
    procedure CalcularTiemposEnd;
    procedure RegistrarBreakManualBegin(const iUsuario: Integer; const dInicial, dFinal: TDateTime);
    procedure RegistrarBreakManualEnd;
  end;



function Minutes2Hour( const iValue: Integer ): THoraLabor;
function ValidaCambioLabor( const dValue, dFechaCorte: Tdate; const lValidaFechaCorte: boolean; const UpdateKind: TUpdateKind; var sMensaje: string ): boolean;

implementation

uses DTarjeta,
     DQueriesLabor,
     ZGlobalTress,
     ZetaCommonTools,
     ZetaServerTools;

const
     K_HORA_DUMMY = '----';
     K_MINUTO = 60;
     K_SEGUNDO = 1;
     K_AREA_INDEFINIDA = '??!!XX';
     K_FOLIO_MAXIMO = $1FFF;
     K_FOLIO_TIEMPO_MUERTO = $FFF;
     K_FOLIO_TIEMPO_MUERTO_EXC = $FFB;
     K_FOLIO_AREA = $FFE;
     K_FOLIO_CEDULA = $FFD;
     K_FOLIO_CEDULA_CAPTURADO = $FFC;
     K_FOLIO_CEDULA_PRECALC = $FF8;
     K_FOLIO_CEDULA_PRECALC_EXC = $FF9;
     K_FOLIO_TIEMPO_MUERTO_CHECADA = $FFA;
     K_FOLIO_TOPE_MULTILOTE = $32C8;
     K_LINX_ID_CEDULA = 'CEDULA';
     K_MENOR = -1;
     K_IGUAL = 0;
     K_MAYOR = 1;
     K_48HORAS = K_24HORAS * 2; { 24 horas * 2 = 48 horas }
     K_MEDIANOCHE = '2400';


function GetSQLScript( const eValor: eScripts ): String;
begin
     Result := GetSQLScriptLabor( eValor );
end;

function ComparaHoras( Item1, Item2: Pointer ): Integer;
begin
     Result := TLChecada( Item1 ).ChecadasCompara( TLChecada( Item2 ) );
end;

function HorasAMinutos( const rValue: TDiasHoras ): Integer;
begin
     Result := Trunc( ZetaCommonTools.MiRound( 60 * rValue, 0 ) );
end;

function Hour2Minutes( const sValue: THoraLabor ): Integer;
begin
     Result := 60 * StrToIntDef( Copy( sValue, 1, 2 ), 0 ) + StrToIntDef( Copy( sValue, 3, 2 ), 0 );
end;

function Minutes2Hour( const iValue: Integer ): THoraLabor;
begin
     Result := Format( '%2.2d', [ iValue div 60 ] ) +
               Format( '%2.2d', [ iValue mod 60 ] );
end;

function CalculaMinutos( const sFinal, sInicial: THoraLabor ): Integer;
begin
     Result := Hour2Minutes( sFinal ) - Hour2Minutes( sInicial );
end;

function HourPlusMinutes( const sValue: THoraLabor; const iMinutes: Integer ): THoraLabor;
begin
     Result := Minutes2Hour( Hour2Minutes( sValue ) + iMinutes );
end;

function HourPlusHours( const sValue: THoraLabor; const iHours: Integer ): THoraLabor;
begin
     Result := HourPlusMinutes( sValue, 60 * iHours );
end;

function EsDeMadrugada( const sHora: THoraLabor ): Boolean;
begin
     Result := ( Hour2Minutes( sHora ) >= K_24HORAS );
end;

procedure AjustarMadrugada( var dFecha: TDate; var sHora: THoraLabor );
begin
     if EsDeMadrugada( sHora ) then
     begin
          sHora := HourPlusMinutes( sHora, -K_24HORAS );
          dFecha := dFecha + 1;
     end;
end;

procedure AjustarFechaHora( const eTurno: eTurnoLabor; const sPrimera: THoraLabor; var dFecha: TDate; var sHora: THoraLabor );
begin
     case eTurno of
          tlNocturnoEntrada:
          begin
               { Ajustar Fecha y Hora de Madrugada }
               if ( sHora < sPrimera ) then
               begin
                    dFecha := dFecha - 1;
                    sHora := HourPlusMinutes( sHora, K_24HORAS );
               end;
          end;
          tlNocturnoSalida:
          begin
               if ( sHora > sPrimera ) then
               begin
                    { Ajustar Fecha al D�a Siguiente }
                    dFecha := dFecha + 1;
               end
               else
               begin
                    { La Hora se debe ajustar para que quede de 2400 a 4800 }
                    sHora := HourPlusMinutes( sHora, K_24HORAS );
               end;
          end;
     end;
end;

function AplicaLabor( const sAplica, sCalcula: String ): Boolean;
begin
     Result := ( ZetaCommonTools.zStrToBool( sAplica ) or ZetaCommonTools.zStrToBool( sCalcula ) );
end;

function SujetoLabor( const sArea, sAplica, sCalcula: String ): Boolean;
begin
     Result := ZetaCommonTools.StrLleno( sArea ) and AplicaLabor( sAplica, sCalcula );
end;

function TipoAsistencia2TipoLabor( const eChecada: eTipoChecadas ): eKindLectura;
begin
     case eChecada of
          chEntrada: Result := ktEntrada;
          chSalida: Result := ktSalida;
          chInicio: Result := ktSalida;
          chFin: Result := ktEntrada;
     else
         Result := ktDesconocido;
     end;
end;

function Tipo2Kind( const eTipo: eTipoLectura ): eKindLectura;
begin
     case eTipo of
          wtChecada: Result := ktChecada;
          wtFija: Result := ktFija;
          wtLibre: Result := ktTiempoMuerto;
          wtPrecalculada: Result := ktPrecalculada;
          wtBreak: Result := ktBreak;
     else
         Result := ktDesconocido;
     end;
end;

function ValidaCambioLabor( const dValue, dFechaCorte: Tdate; const lValidaFechaCorte: boolean; const UpdateKind: TUpdateKind; var sMensaje: string ): boolean;
const
     K_MENSAJE = '� No se Permite %s Informaci�n de Labor !' + CR_LF + 'La Fecha de Corte es : %s';
begin
     Result := ( not lValidaFechaCorte ) or ( dValue > dFechaCorte );
     if not Result then
        sMensaje := Format( K_MENSAJE, [ ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord( UpdateKind ) ), FechaCorta( dFechaCorte ) ] );
end;

{ ******** TDatosCedula ******** }

function TDatosCedula.GetChecada: eTipoLectura;
begin
     case Tipo of
          tcOperacion: Result := wtChecada;
          tcEspeciales: Result := wtPrecalculada;
          tcTMuerto: Result := wtLibre;
     else
         Result := wtChecada;
     end;
end;

procedure TDatosCedula.SetFecha( const dValue: TDate );
begin
     FFecha := dValue;
     FFechaWorks := FFecha;
end;

procedure TDatosCedula.SetHora( const sValue: THoraLabor );
begin
     FHora := sValue;
     FHoraWorks := FHora;
end;

function TDatosCedula.GetFechaReal: TDate;
begin
     Result := Fecha;
     if EsDeMadrugada( Hora ) then
        Result := Result + 1;
end;

function TDatosCedula.GetHoraReal: THoraLabor;
begin
     Result := Hora;
     if EsDeMadrugada( Hora ) then
        Result := HourPlusMinutes( Result, -K_24HORAS );
end;

function TDatosCedula.PuedeExplicar: Boolean;
begin
     { La c�dula no puede explicar el tiempo a menos que demuestre lo contrario }
     Result := False;
     case Tipo of
          tcOperacion:
          begin
               { Las c�dulas de producci�n siempre pueden explicar el tiempo }
               Result := True;
               { Dado que es una c�dula de producci�n todo el tiempo est� explicado }
               FPorExplicar := 0;
          end;
          tcEspeciales, tcTMuerto:
          begin
               { Puede explicar si el tiempo precalculado de la c�dula }
               { hace que la hora de inicio de la misma sea anterior a la }
               { hora de la b�squeda }
               if ( HoraBusqueda > HourPlusMinutes( HoraWorks, Trunc( -Tiempo ) ) ) then
               begin
                    Result := True;
                    { Ajustar el tiempo de la c�dula al tiempo disponible }
                    { dado que la hora en la que la c�dula termin� ser� modificada }
                    Tiempo := Tiempo - CalculaMinutos( HoraWorks, HoraBusqueda );
                    { Restar del tiempo por Explicar el tiempo de la c�dula }
                    FPorExplicar := FPorExplicar - Tiempo;
               end;
          end;
     else
         Result := False;
     end;
end;

function TDatosCedula.FaltaExplicar: Boolean;
begin
     Result := ( FPorExplicar > 0 );
end;

procedure TDatosCedula.AjustarFechaHoraWorks( const eTurno: eTurnoLabor; const sHora: THoraLabor );
begin
     AjustarFechaHora( eTurno, sHora, FFechaWorks, FHoraWorks );
end;

procedure TDatosCedula.InitBusqueda( const rDuracion: TDiasHoras; const dBusqueda: TDate; const sHoraBusqueda: THoraLabor );
begin
     { Inicializar valores para poder buscar c�dulas con horas +- 24 }
     FFound := False;
     Folio := 0;
     Fecha := EncodeDate( 2200, 1, 1 ); { Inicializa al 01/Ene/2200 }
     Hora := VACIO;
     FFechaBusqueda := dBusqueda;
     FHoraBusqueda := sHoraBusqueda;
     FPorExplicar := rDuracion;
end;

function TDatosCedula.GetOrden( Index: Integer ): TLOrden;
begin
     Result := TLOrden( FLotes[ Index ] );
end;

function TDatosCedula.AddOrden: TLOrden;
begin
     Result := TLOrden.Create;
     FLotes.Add(Result);
end;





{ ******** TBreak ********* }

constructor TBreak.Create;
begin
     FCodigo := '';
     FTipo := '';
     FDuracion := 0;
     FInicio := K_CERO_HORAS;
     FFinal := K_CERO_HORAS;
end;

procedure TBreak.SetDuracion(const Value: Integer);
begin
     FDuracion := Value;
     SetFinal;
end;

procedure TBreak.SetInicio(const Value: THoraLabor);
begin
     FInicio := Value;
     SetFinal;
end;

procedure TBreak.SetFinal;
begin
     FFinal := HourPlusMinutes( FInicio, Duracion );
end;

{ ******** TBreakList ******** }

constructor TBreakList.Create(oProvider: TdmZetaServerProvider);
begin
     FItems := TList.Create;
     oZetaProvider := oProvider;
end;

destructor TBreakList.Destroy;
begin
     Clear;
     FItems.Free;
     inherited Destroy;
end;

function TBreakList.Add: TBreak;
begin
     Result := TBreak.Create;
     FItems.Add( Result );
end;

procedure TBreakList.Clear;
var
   i: Integer;
begin
     for i := ( Count - 1 ) downto 0 do
         Delete( i );
end;

procedure TBreakList.Delete(const Index: Integer);
begin
     Items[ Index ].Free;
     FItems.Delete( Index );
end;

function TBreakList.GetCount: Integer;
begin
     Result := FItems.Count;
end;

function TBreakList.GetItems(Index: Integer): TBreak;
begin
     Result := TBreak( FItems[ Index ] );
end;

procedure TBreakList.Init;
var
   FDataset: TZetaCursor;
begin
     with oZetaProvider do
     begin
          FDataset := CreateQuery( GetSQLScript( Q_BREAKS_LEE ) );
     end;
     try
        with FDataset do
        begin
             Active := True;
             while not Eof do
             begin
                  with Add do
                  begin
                       Codigo := FieldByName( 'BR_CODIGO' ).AsString;
                       Duracion := FieldByName( 'BH_TIEMPO' ).AsInteger;
                       Inicio := FieldByName( 'BH_INICIO' ).AsString;
                       Tipo := FieldByName( 'BH_MOTIVO' ).AsString;
                  end;
                  Next;
             end;
             Active := False;
        end;
     finally
            FreeAndNil( FDataset );
     end;
end;

function TBreakList.AgregaBreaks( const sCodigo: TCodigo; const sInicial, sFinal: THoraLabor; Metodo: TAgregaBreak ): Boolean;
var
   i: Integer;
begin
     Result := False;
     i := 0;
     { Busca el primer break }
     while ( i < Count ) and ( Items[ i ].Codigo <> sCodigo ) do
     begin
          Inc( i )
     end;
     while ( i < Count ) and ( Items[ i ].Codigo = sCodigo ) do
     begin
          with Items[ i ] do
          begin
               if ( Inicio >= sInicial ) and ( Final <= sFinal ) then
               begin
                    Metodo( Final, Duracion, Tipo );
                    Result := True;
               end;
          end;
          Inc( i );
     end;
end;

{ ********** TEmployee ********* }

constructor TEmployee.Create(oProvider: TdmZetaServerProvider);
begin
     FChanges := TList.Create;
     oZetaProvider := oProvider;
end;

destructor TEmployee.Destroy;
begin
     Clear;
     FreeAndNil( FChanges );
     inherited Destroy;
end;

procedure TEmployee.Clear;
var
   i: Integer;
begin
     for i := ( Count - 1 ) downto 0 do
     begin
          Delete( i );
     end;
end;

procedure TEmployee.Delete(const Index: Integer);
begin
     Changes[ Index ].Free;
     FChanges.Delete( Index );
end;

function TEmployee.Add: TEmployeeChange;
begin
     Result := TEmployeeChange.Create;
     FChanges.Add( Result );
end;

function TEmployee.GetChanges(Index: Integer): TEmployeeChange;
begin
     Result := TEmployeeChange( FChanges[ Index ] );
end;

function TEmployee.GetCount: Integer;
begin
     Result := FChanges.Count;
end;

procedure TEmployee.Init( const Dataset: TZetaCursor );
begin
     with oZetaProvider do
     begin
          ParamAsInteger( Dataset, 'Empleado', Numero );
     end;
     with Dataset do
     begin
          Active := True;
          while not Eof do
          begin
               with Add do
               begin
                    Area := FieldByName( 'CB_AREA' ).AsString;
                    Fecha := FieldByName( 'KA_FECHA' ).AsDateTime;
                    Hora := FieldByName( 'KA_HORA' ).AsString;
               end;
               Next;
          end;
          Active := False;
     end;
end;

function TEmployee.IsInArea( const sArea: TCodigo; const dValue: TDate; const sHora: THoraLabor ): Boolean;
var
   i: Integer;
begin
     Result := False;
     for i := 0 to ( Count - 1 ) do
     begin
          with Changes[ i ] do
          begin
               if ( Fecha > dValue ) or ( ( Fecha = dValue ) and ( Hora >= sHora ) ) then
                  Break;
               Result := ( Area = sArea );
          end;
     end;
end;

{ ********* TEmployeeList *********** }

constructor TEmployeeList.Create(oProvider: TdmZetaServerProvider);
begin
     FEmployees := TList.Create;
     oZetaProvider := oProvider;
end;

destructor TEmployeeList.Destroy;
begin
     Clear;
     FreeAndNil( FEmployees );
     inherited Destroy;
end;

function TEmployeeList.Add: TEmployee;
begin
     Result := TEmployee.Create( oZetaProvider );
     FEmployees.Add( Result );
end;

procedure TEmployeeList.Clear;
var
   i: Integer;
begin
     for i := ( Count - 1 ) downto 0 do
     begin
          Delete( i );
     end;
end;

procedure TEmployeeList.Delete(const Index: Integer);
begin
     Employees[ Index ].Free;
     FEmployees.Delete( Index );
end;

function TEmployeeList.GetCount: Integer;
begin
     Result := FEmployees.Count;
end;

function TEmployeeList.GetEmployees(Index: Integer): TEmployee;
begin
     Result := TEmployee( FEmployees[ Index ] );
end;

procedure TEmployeeList.Init( const dValue: TDate );
var
   FEmpleados, FAreas: TZetaCursor;
begin
     Clear;
     with oZetaProvider do
     begin
          FEmpleados := CreateQuery( Format( GetSQLScript( Q_TMPLABOR_LISTAR ), [ UsuarioActivo ] ) );
          try
             FAreas := CreateQuery( GetSQLScript( Q_EMPLEADO_AREAS_LISTA ) );
             try
                ParamAsDate( FAreas, 'FechaInicial', dValue - 1 );
                ParamAsChar( FAreas, 'HoraInicial', K_CERO_HORAS, K_ANCHO_HH_MM );
                ParamAsDate( FAreas, 'FechaFinal', dValue + 1 );
                ParamAsChar( FAreas, 'HoraFinal', K_MEDIANOCHE, K_ANCHO_HH_MM );
                with FEmpleados do
                begin
                     Active := True;
                     while not Eof and CanContinue do
                     begin
                          if AplicaLabor( FieldByName( 'TL_APLICA' ).AsString, FieldByName( 'TL_CALCULA' ).AsString ) then
                          begin
                               with Add do
                               begin
                                    Numero := FieldByName( 'CB_CODIGO' ).AsInteger;
                                    Init( FAreas );
                               end;
                          end;
                          Next;
                     end;
                     Active := False;
                end;
             finally
                    FreeAndNil( FAreas );
             end;
          finally
                 FreeAndNil( FEmpleados );
          end;
     end;
end;

{ ******** TArea ******** }

constructor TArea.Create;
begin
     FCodigo := '';
     FDomingo := '';
     FLunes := '';
     FMartes := '';
     FMiercoles := '';
     FJueves := '';
     FViernes := '';
     FSabado := '';
end;

function TArea.GetBreak( const dValue: TDate ): TCodigo;
begin
     case DayOfWeek( dValue ) of
          1: Result := Domingo;
          2: Result := Lunes;
          3: Result := Martes;
          4: Result := Miercoles;
          5: Result := Jueves;
          6: Result := Viernes;
          7: Result := Sabado;
     else
         Result := '';
     end;
end;

{ ********* TAreaList ******* }

constructor TAreaList.Create(oProvider: TdmZetaServerProvider);
begin
     FItems := TList.Create;
     oZetaProvider := oProvider;
end;

destructor TAreaList.Destroy;
begin
     Clear;
     FItems.Free;
     inherited Destroy;
end;

function TAreaList.Add: TArea;
begin
     Result := TArea.Create;
     FItems.Add( Result );
end;

procedure TAreaList.Clear;
var
   i: Integer;
begin
     for i := ( Count - 1 ) downto 0 do
         Delete( i );
end;

procedure TAreaList.Delete(const Index: Integer);
begin
     Items[ Index ].Free;
     FItems.Delete( Index );
end;

function TAreaList.Find( const sArea: TCodigo ): Integer;
var
   i: Integer;
begin
     Result := -1;
     for i := 0 to ( Count - 1 ) do
     begin
          with Items[ i ] do
          begin
               if ( Codigo = sArea ) then
               begin
                    Result := i;
                    Break;
               end;
          end;
     end;
end;

function TAreaList.GetCount: Integer;
begin
     Result := FItems.Count;
end;

function TAreaList.GetItems(Index: Integer): TArea;
begin
     Result := TArea( FItems[ Index ] );
end;

procedure TAreaList.Init;
var
   FDataset: TZetaCursor;
begin
     with oZetaProvider do
     begin
          FDataset := CreateQuery( GetSQLScript( Q_AREA_LEE ) );
     end;
     try
        with FDataset do
        begin
             Active := True;
             while not Eof do
             begin
                  with Add do
                  begin
                       Codigo := FieldByName( 'TB_CODIGO' ).AsString;
                       Domingo := FieldByName( 'TB_BREAK_1' ).AsString;
                       Lunes := FieldByName( 'TB_BREAK_2' ).AsString;
                       Martes := FieldByName( 'TB_BREAK_3' ).AsString;
                       Miercoles := FieldByName( 'TB_BREAK_4' ).AsString;
                       Jueves := FieldByName( 'TB_BREAK_5' ).AsString;
                       Viernes := FieldByName( 'TB_BREAK_6' ).AsString;
                       Sabado := FieldByName( 'TB_BREAK_7' ).AsString;
                       Operacion := FieldByName( 'TB_OPERA' ).AsString;
                       Turno := eTurnoLabor( FieldByName( 'AR_SHIFT' ).AsInteger );
                       PrimeraHora := FieldByName( 'AR_PRI_HOR' ).AsString;
                       Tipo := eTipoArea( FieldByName( 'AR_TIPO' ).AsInteger );
                  end;
                  Next;
             end;
             Active := False;
        end;
     finally
            FreeAndNil( FDataset );
     end;
end;

{ ********* TAChecada ******** }

function TAChecada.GetKind: eKindLectura;
begin
     Result := TipoAsistencia2TipoLabor( FTipo );
end;

function TAChecada.EsExtra: Boolean;
begin
     Result := ( FDescripcion = dcExtras );
end;

{ ******** TLChecada ********* }

constructor TLChecada.Create(oProvider: TdmZetaServerProvider);
begin
     oZetaProvider := oProvider;
     FFechaA := NullDateTime;
     FFechaR := NullDateTime;
     FHoraA := '';
     FHoraR := '';
     FOperacion := '';
     FOrden := '';
     FParte := '';
     FTerminalID := '';
     FDuracion := 0;
     FTiempo := 0;
     FCalculable := True;
     FTipo := eTipoLectura( 0 );
     FKind := eKindLectura( 0 );
     FStatus := eStatusLectura( 0 );
     FModula1 := '';
     FModula2 := '';
     FModula3 := '';
     FMuerto := '';
     FArea := '';
     FPuesto := '';
     FDobles := 0;
     FTriples := 0;
     FManual := False;
     FEscribir := False;
     FPiezas := 0;
     FFolio := 0;
     FCedula := 0;
end;

procedure TLChecada.Assign( Value: TLChecada );
begin
     FechaA := Value.FechaA;
     FechaR := Value.FechaR;
     HoraA := Value.HoraA;
     HoraR := Value.HoraR;
     Operacion := Value.Operacion;
     Orden := Value.Orden;
     Parte := Value.Parte;
     TerminalID := Value.TerminalID;
     Duracion := Value.Duracion;
     Tiempo := Value.Tiempo;
     Tipo := Value.Tipo;
     Kind := Value.Kind;
     Status := Value.Status;
     Modula1 := Value.Modula1;
     Modula2 := Value.Modula2;
     Modula3 := Value.Modula3;
     TMuerto := Value.TMuerto;
     Area := Value.Area;
     Puesto := Value.Puesto;
     Dobles := Value.Dobles;
     Triples := Value.Triples;
     Manual := Value.Manual;
     Escribir := Value.Escribir;
     Piezas := Value.Piezas;
     Folio := Value.Folio;
     Cedula := Value.Cedula;
end;

function TLChecada.ChecadasComparaReales( Value: TLChecada ): Integer;
begin
     Result := K_IGUAL;
     if ( HoraR < Value.HoraR ) then
        Result := K_MENOR
     else
         if ( HoraR > Value.HoraR ) then
            Result := K_MAYOR
         else
             if( Excepcion and Value.Excepcion )then
             begin
                  if ( Cedula > Value.Cedula )then
                     Result := K_MAYOR
                  else
                      if ( Cedula < Value.Cedula )then
                         Result := K_MENOR
                      else
                          Result := K_IGUAL;
             end
             else
                 if( Excepcion and ( not Value.Excepcion ) )then
                     Result := K_MENOR
                 else
                     if( ( not Excepcion ) and ( Value.Excepcion ) )then
                         Result := K_MAYOR;

end;

function TLChecada.ChecadasComparaAjustadas( Value: TLChecada ): Integer;
begin
     if ( HoraA < Value.HoraA ) then
        Result := K_MENOR
     else
         if ( HoraA > Value.HoraA ) then
            Result := K_MAYOR
         else
             Result := K_IGUAL;
end;

function TLChecada.ChecadasComparaAreas( Value: TLChecada ): Integer;
begin
     if EsArea and not Value.EsArea then
     begin
          { Cuando son iguales las �reas, el cambio siempre es menor ( anterior ) }
          if ( Value.Area = Self.Area ) then
             Result := K_MENOR
          else
              { Cuando son diferentes las �reas, el cambio siempre es mayor ( posterior ) }
              Result := K_MAYOR;
     end
     else
         if not EsArea and Value.EsArea then
         begin
              { Cuando son iguales las �reas, el cambio siempre es menor ( anterior ) }
              if ( Value.Area = Self.Area ) then
                 Result := K_MAYOR
              else
                  { Cuando son diferentes las �reas, el cambio siempre es mayor ( posterior ) }
                  Result := K_MENOR;
         end
         else
             Result := K_IGUAL;
end;

function TLChecada.ChecadasCompara( Value: TLChecada ): Integer;
begin
     { Decide como ordenar dos checadas en base a la hora Ajustada }
     Result := ChecadasComparaAjustadas( Value );
     if ( Result = K_IGUAL ) then
     begin
         { Si las horas son iguales, entonces hay que ordenar }
         { en base al tipo de las checadas }
         { � Alguna idea de como hacer esto m�s eficientemente ? }
         case Kind of
               ktDesconocido:
               begin
                    if ( Value.Kind = ktDesconocido ) then
                       Result := K_IGUAL
                    else
                        Result := K_MAYOR;
               end;
               ktChecada:
               begin
                    case Value.Kind of
                         ktDesconocido: Result := K_MENOR;
                         ktChecada:
                         begin
                              if EsMultilote and Value.EsMultilote  then //Comparando 2 cedulas con multilote
                              begin
                                   if Cedula < Value.Cedula then
                                      Result := K_MENOR
                                   else
                                       if ( Cedula > Value.Cedula ) then
                                          Result := K_MAYOR
                                       else
                                           Result := K_IGUAL;
                              end;
                              if Result = K_IGUAL then  // si es la misma cedula 
                              begin
                                    if ( Folio < Value.Folio ) then
                                       Result := K_MENOR
                                    else
                                        if ( Folio > Value.Folio ) then
                                           Result := K_MAYOR
                                        else
                                            Result := K_IGUAL;
                              end;
                         end;
                         ktPrecalculada: Result := K_MAYOR;
                         ktFija: Result := K_MENOR;
                         ktEntrada: Result := K_MAYOR;
                         ktSalida: Result := K_MENOR;
                         ktTiempoMuerto: Result := K_MAYOR;
                         ktTiempoMuertoChecado: Result := K_MAYOR;
                         ktBreak: Result := K_MAYOR;
                         ktArea: Result := ChecadasComparaAreas( Value );
                    else
                        Result := K_IGUAL;
                    end;
               end;
               ktPrecalculada:
               begin
                    case Value.Kind of
                         ktDesconocido: Result := K_MENOR;
                         ktChecada: Result := K_MENOR;
                         ktPrecalculada: Result := K_IGUAL;
                         ktFija: Result := K_MENOR;
                         ktEntrada: Result := K_MAYOR;
                         ktSalida: Result := K_MENOR;
                         ktTiempoMuerto: Result := K_MENOR;//{$IFDEF HYUNDAI}K_MAYOR{$ELSE}K_MENOR{$ENDIF};
                         ktTiempoMuertoChecado: Result := K_MENOR;
                         ktBreak: Result := K_MAYOR;
                         ktArea: Result := ChecadasComparaAreas( Value );
                    else
                        Result := K_IGUAL;
                    end;
               end;
               ktFija:
               begin
                    case Value.Kind of
                         ktDesconocido: Result := K_MENOR;
                         ktChecada: Result := K_MAYOR;
                         ktPrecalculada: Result := K_MAYOR;
                         ktFija: Result := K_IGUAL;
                         ktEntrada: Result := K_MAYOR;
                         ktSalida: Result := K_MENOR;
                         ktTiempoMuerto: Result := K_MAYOR;
                         ktTiempoMuertoChecado: Result := K_MAYOR;
                         ktBreak: Result := K_MAYOR;
                         ktArea: Result := ChecadasComparaAreas( Value );
                    else
                        Result := K_IGUAL;
                    end;
               end;
               ktEntrada:
               begin
                    case Value.Kind of
                         ktDesconocido: Result := K_MENOR;
                         ktChecada: Result := K_MENOR;
                         ktPrecalculada: Result := K_MENOR;
                         ktFija: Result := K_MENOR;
                         ktEntrada: Result := K_IGUAL;
                         ktSalida: Result := K_MENOR;
                         ktTiempoMuerto: Result := K_MENOR;
                         ktTiempoMuertoChecado: Result := K_MENOR;
                         ktBreak: Result := K_MENOR;
                         ktArea: Result := K_MAYOR;
                    else
                        Result := K_IGUAL;
                    end;
               end;
               ktSalida:
               begin
                    case Value.Kind of
                         ktDesconocido: Result := K_MENOR;
                         ktChecada: Result := K_MAYOR;
                         ktPrecalculada: Result := K_MAYOR;
                         ktFija: Result := K_MAYOR;
                         ktEntrada: Result := K_MAYOR;
                         ktSalida: Result := K_IGUAL;
                         ktTiempoMuerto: Result := K_MAYOR;
                         ktTiempoMuertoChecado: Result := K_MAYOR;
                         ktBreak: Result := K_MAYOR;
                         ktArea: Result := K_MAYOR;
                    else
                        Result := K_IGUAL;
                    end;
               end;
               ktTiempoMuerto, ktTiempoMuertoChecado:
               begin
                    case Value.Kind of
                         ktDesconocido: Result := K_MENOR;
                         ktChecada: Result := K_MENOR;
                         ktPrecalculada: Result := K_MAYOR;//{$IFDEF HYUNDAI}K_MENOR{$ELSE}K_MAYOR{$ENDIF};
                         ktFija: Result := K_MENOR;
                         ktEntrada: Result := K_MAYOR;
                         ktSalida: Result := K_MENOR;
                         ktTiempoMuerto: Result := K_IGUAL;
                         ktTiempoMuertoChecado: Result := K_IGUAL;
                         ktBreak: Result := K_MAYOR;
                         ktArea: Result := ChecadasComparaAreas( Value );
                    else
                        Result := K_IGUAL;
                    end;
               end;
               ktBreak:
               begin
                    case Value.Kind of
                         ktDesconocido: Result := K_MENOR;
                         ktChecada: Result := K_MENOR;
                         ktPrecalculada: Result := K_MENOR;
                         ktFija: Result := K_MENOR;
                         ktEntrada: Result := K_MAYOR;
                         ktSalida: Result := K_MENOR;
                         ktTiempoMuerto: Result := K_MENOR;
                         ktTiempoMuertoChecado: Result := K_MENOR;
                         ktBreak: Result := K_IGUAL;
                         ktArea: Result := ChecadasComparaAreas( Value );
                    else
                        Result := K_IGUAL;
                    end;
               end;
               ktArea:
               begin
                    case Value.Kind of
                         ktDesconocido: Result := K_MAYOR;
                         ktEntrada: Result := K_MENOR;
                         ktSalida: Result := K_MENOR;
                         ktArea: Result := K_IGUAL;
                    else
                         Result := ChecadasComparaAreas( Value );
                    end;
                    {
                    if ( Value.Kind = ktArea ) then
                       Result := K_IGUAL
                    else
                        Result := K_MENOR;
                    }
               end;
          else
              Result := K_IGUAL;
          end;
     end;
    // else
      //   Result := K_IGUAL;

     if ( Result = K_IGUAL ) then
     begin
          { Decide como ordenar dos checadas en base a la hora Real }
          Result := ChecadasComparaReales( Value );
     end;
end;

procedure TLChecada.Ignorar;
begin
     { Indica que esta checada no debe serle calculado su duraci�n }
     Calculable := False;
     { Dado que no es calculabe, tampoco se debe escribir }
     Escribir := False;
end;

procedure TLChecada.AjustarExcepcionProduccion( Lista: TLKardex; Excepcion: TLChecada; const iPos: Integer;sEntrada:THoraLabor );
begin
     { La checada termina durante el intervalo de la Excepcion }
     if ( HoraA > Excepcion.Inicio ) and ( HoraA <= Excepcion.HoraA ) then
     begin
          { Ajustar hora en la que termina la checada de producci�n }
          {EZ: Topo en la hora de entrada de la tarjeta }
          if Excepcion.Inicio > sEntrada then
            HoraA:= Excepcion.Inicio
          else
               HoraA:= sEntrada;

          { Si ya existee una checada igual en la misma hora }
          { entonces se ignora esta checada }
          if Lista.CheckForDuplicate( iPos, HoraA, Kind, Area ) then
          begin
               Ignorar;
          end;
     end
end;

procedure TLChecada.AjustarExcepcionPrecalculada( Lista: TLKardex; Excepcion: TLChecada; const iPos: Integer );
var
   sInicio: THoraLabor;
   Gemela: TLChecada;
begin
     { Cae totalmente dentro del intervalo de la Excepcion: no calcular }
     if ( Inicio >= Excepcion.Inicio ) and ( HoraA <= Excepcion.HoraA ) then
     begin

          Ignorar;
     end
     else
         { Empieza antes y termina durante el intervalo de la Excepci�n }
         if ( Inicio < Excepcion.Inicio ) and ( HoraA > Excepcion.Inicio ) and ( HoraA <= Excepcion.HoraA ) then
         begin
              { Ajustar Tiempo acorde a la diferencia }
              Tiempo := ZetaCommonTools.rMax( 0, Tiempo - CalculaMinutos( HoraA, Excepcion.Inicio ) );
              { Ahora Termina cuando la Excepci�n Empieza }
              HoraA := Excepcion.Inicio;
              { Si ya existe una checada igual en la misma hora }
              { entonces se ignora esta checada }
              if Lista.CheckForDuplicate( iPos, HoraA, Kind, Area ) then
              begin
                   Ignorar;
              end;
         end
         else
             { Empieza durante y termina despu�s de la checada anterior }
             if ( Inicio >= Excepcion.Inicio ) and ( Inicio < Excepcion.HoraA ) and ( HoraA > Excepcion.HoraA ) then
             begin
                  { Ajustar Duraci�n }
                  Tiempo := CalculaMinutos( HoraA, Excepcion.HoraA );
             end
             else
                 { Abarca completamente el Intervalo de la Excepci�n }
                 if ( Inicio < Excepcion.Inicio ) and ( HoraA > Excepcion.HoraA ) then
                 begin
                      if not EsPrecalculada or EsTiempoMuerto then
                      begin
                           { Almacenar Hora Inicial }
                           sInicio := Inicio;
                           { Ajustar Duraci�n }
                           Tiempo := CalculaMinutos( HoraA, Excepcion.HoraA );
                      end;

                      { Se debe partir la checada en dos cuando es de Tiempo Muerto }
                      if EsPrecalculada and EsTiempoMuerto then
                      begin
                           { Primero valida que no exista una checada del mismo tipo }
                           if not Lista.CheckForDuplicate( -1, Excepcion.Inicio, Kind, Area ) then
                           begin
                                Gemela := Lista.Add;
                                { Copia checada para poder partirla en dos }
                                Gemela.Assign( Self );
                                { La nueva checada termina cuando empieza la excepci�n }
                                Gemela.HoraA := Excepcion.Inicio;
                                { Calcular la nueva duraci�n }
                                Gemela.Tiempo := CalculaMinutos( Gemela.HoraA, sInicio );
                                { Las checadas manuales se deben copiar como no manuales }
                                { para evitar que dichas checadas no sean generadas por }
                                { el sistema en un c�lculo posterior a este }
                                Gemela.Manual := False;
                           end;
                      end;
                 end;
end;

function TLChecada.GetDuracion: TDiasHoras;
begin
     Result := FDuracion;
end;

function TLChecada.GetDuracionDefinitiva: TDiasHoras;
begin
     if FEscribir and FCalculable then
        Result := FDuracion
     else
         Result := 0;
end;

function TLChecada.PuedeEscribir: Boolean;
begin
     if FManual then
        Result := True
     else
         Result := ( FEscribir {$ifndef HYUNDAI} and ( GetDuracionDefinitiva > 0 ){$endif} );
end;

function TLChecada.FormatHora( const sValue: THoraLabor ): THoraLabor;
begin
     case Length( sValue ) of
          0: Result := K_CERO_HORAS;
          1: Result := '0' + sValue + '00';
          2: Result := sValue + '00';
          3: Result := '0' + sValue;
     else
         Result := sValue;
     end;
end;

function TLChecada.GetHoraA: THoraLabor;
begin
     Result := FHoraA;
end;

function TLChecada.GetHoraI: THoraLabor;
begin
     Result := FHoraI;
end;

function TLChecada.GetHoraR: THoraLabor;
begin
     Result := FHoraR;
end;

function TLChecada.GetInicio: THoraLabor;
begin
     case Kind of
          ktPrecalculada: Result := HourPlusMinutes( HoraA, -Trunc( Tiempo ) );
          ktTiempoMuerto: Result := HourPlusMinutes( HoraA, -Trunc( Tiempo ) );
     else
         Result := VACIO;
     end;
end;

procedure TLChecada.SetDuracion(const Value: TDiasHoras);
begin
     FDuracion := Value;
end;

procedure TLChecada.SetDobles(const Value: TDiasHoras);
begin
     FDobles := ZetaCommonTools.rMax( 0, Value );
end;

procedure TLChecada.SetOrdinarias(const Value: TDiasHoras);
begin
     FOrdinarias := ZetaCommonTools.rMax( 0, Value );
end;

procedure TLChecada.SetTriples(const Value: TDiasHoras);
begin
     FTriples := ZetaCommonTools.rMax( 0, Value );
end;

function TLChecada.EsArea: Boolean;
begin
     Result := ( Kind in [ ktArea ] );
end;

function TLChecada.EsEntrada: Boolean;
begin
     Result := ( Kind in [ ktEntrada ] );
end;

function TLChecada.EsSalida: Boolean;
begin
     Result := ( Kind in [ ktSalida ] );
end;

function TLChecada.EsChecada: Boolean;
begin
     Result := ( Kind in [ ktChecada, ktFija, ktTiempoMuertoChecado ] );
end;

function TLChecada.EsPrecalculada: Boolean;
begin
     Result := ( Kind in [ ktTiempoMuerto, ktPrecalculada ] );
end;

function TLChecada.EsTiempoMuerto: Boolean;
begin
     Result := ( Kind in [ ktTiempoMuerto, ktTiempoMuertoChecado ] );
end;

function TLChecada.EsMultilote: Boolean;
begin
     Result := ( Folio > 0 ) and ( Folio < K_FOLIO_TOPE_MULTILOTE ) and ( Folio <> K_FOLIO_TIEMPO_MUERTO )
                                                                    and ( Folio <> K_FOLIO_TIEMPO_MUERTO_EXC  )
                                                                    and ( Folio <> K_FOLIO_CEDULA )
                                                                    and ( Folio <> K_FOLIO_CEDULA_PRECALC  )
                                                                    and ( Folio <> K_FOLIO_CEDULA_PRECALC_EXC  )
                                                                    and ( Folio <> K_FOLIO_TIEMPO_MUERTO_CHECADA )
                                                                    and ( Folio <> K_FOLIO_CEDULA_CAPTURADO )
                                                                    and ( Folio <> K_FOLIO_AREA );
end;

function TLChecada.EsBreak: Boolean;
begin
     Result := ( Kind in [ ktBreak ] );
end;

function TLChecada.EsValida: Boolean;
begin
     Result := EsPrecalculada or EsChecada;
     if not Result then
        Result := EsBreak and Manual;
end;

function TLChecada.DetermineKind( const eTipo: eTipoLectura ): eKindLectura;
begin
     Result := Tipo2Kind( eTipo );
end;

function TLChecada.GetKindText: String;
const
     aKindLectura: array[ eKindLectura ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'Desconocido',
                                                      'Producci�n',
                                                      'Precalculada',
                                                      'Fija',
                                                      'Entrada',
                                                      'Salida',
                                                      'T.Muerto',
                                                      'T.Muerto Ch',
                                                      'Break',
                                                      'Cambio Area' );
begin
     Result := Format( '%-12.12s', [ aKindLectura[ Kind ] ] );
end;

function TLChecada.GetExcepcion: Boolean;
begin
     Result := Manual or ( ( Cedula > 0 ) and ( Folio = K_FOLIO_CEDULA_CAPTURADO )  )or (Folio > K_OFFSET_MULTI_EXC) or (Folio = K_FOLIO_CEDULA_PRECALC_EXC ) or ( Folio = K_FOLIO_TIEMPO_MUERTO_EXC );
end;

function TLChecada.TieneDuracion: Boolean;
begin
     Result := FCalculable and ( Duracion > 0 ) ;
end;

{ ******** TLKardex ********* }

constructor TLKardex.Create( oCreator: TZetaCreator );
begin
     FCreator := oCreator;
     FItems := TList.Create;
     FChecadas := TList.Create;
     FBreaks := TBreakList.Create( oZetaProvider );
     FAreas := TAreaList.Create( oZetaProvider );

     FDatosTarjeta := TDatosTarjeta.Create
end;

destructor TLKardex.Destroy;
begin
     Clear;
     FAreas.Free;
     FBreaks.Free;
     FChecadas.Free;
     FItems.Free;

     FreeAndNil(FDatosTarjeta);

     inherited;
end;

function TLKardex.GetQueries: TCommonQueries;
begin
     Result := FCreator.Queries;
end;

function TLKardex.oZetaProvider: TdmZetaServerProvider;
begin
     Result := FCreator.oZetaProvider;
end;

function TLKardex.Add: TLChecada;
begin
     Result := TLChecada.Create( oZetaProvider );
     FItems.Add( Result );
end;

procedure TLKardex.Clear;
var
   i: Integer;
begin
     for i := ( Count - 1 ) downto 0 do
     begin
          Delete( i );
     end;
     for i := ( CountChecadas - 1 ) downto 0 do
     begin
          DeleteChecada( i );
     end;
end;

procedure TLKardex.Delete(const Index: Integer);
begin
     Items[ Index ].Free;
     FItems.Delete( Index );
end;

function TLKardex.GetCount: Integer;
begin
     Result := FItems.Count;
end;

function TLKardex.GetItem(Index: Integer): TLChecada;
begin
     Result := TLChecada( FItems[ Index ] );
end;

function TLKardex.AddChecada: TAChecada;
begin
     Result := TAChecada.Create;
     FChecadas.Add( Result );
end;

function TLKardex.GetChecada(Index: Integer): TAChecada;
begin
     Result := TAChecada( FChecadas[ Index ] );
end;

function TLKardex.GetCountChecadas: Integer;
begin
     Result := FChecadas.Count;
end;

procedure TLKardex.DeleteChecada(const Index: Integer);
begin
     Checadas[ Index ].Free;
     FChecadas.Delete( Index );
end;

procedure TLKardex.GetDatosHorarioDiario;
begin
     FDatosHorario := Queries.GetDatosHorarioDiario( Horario );
end;

function TLKardex.GetAreaDefault: TCodigo;
begin
     if HayAreaDefault then
        Result := FAreas.Items[ FAreaDefault ].Codigo
     else
         Result := K_AREA_INDEFINIDA;
end;

procedure TLKardex.SetAreaDefault( const sArea: TCodigo );
begin
     FAreaDefault := FAreas.Find( sArea );
end;

function TLKardex.GetAreaPtr( const sArea: TCodigo ): Integer;
begin
     Result := FAreas.Find( sArea );
end;

function TLKardex.ExisteArea( const sArea: TCodigo ): Boolean;
begin
     Result := ( FAreas.Find( sArea ) >= 0 );
end;

function TLKardex.HayAreaDefault: Boolean;
begin
     Result := ( FAreaDefault >= 0 ) and ( FAreaDefault < FAreas.Count );
end;

function TLKardex.DeterminarPuesto( const sPuesto: String ): String;
begin
     if ZetaCommonTools.StrVacio( sPuesto ) then
        Result := Self.Puesto
     else
         Result := sPuesto;
end;

procedure TLKardex.Advertencia(const iEmpleado: TNumEmp; const sMensaje: String);
begin
     with oZetaProvider do
     begin
          if Assigned( Log ) then
             Log.Advertencia( iEmpleado, sMensaje );
     end;
end;

procedure TLKardex.Ordena;
begin
     FItems.Sort( ComparaHoras );
end;

procedure TLKardex.Desempata;
var
   i, j, iFolio: Integer;
   sHora: THoraLabor;

function NoMulitlote(Folio:Integer):Boolean;
begin
     Result := ( Folio = K_FOLIO_TIEMPO_MUERTO) or
               ( Folio = K_FOLIO_CEDULA ) or
               ( Folio = K_FOLIO_CEDULA_PRECALC ) or
               ( Folio = K_FOLIO_CEDULA_PRECALC_EXC ) or
               ( Folio = K_FOLIO_TIEMPO_MUERTO_EXC  ) or
               ( Folio = K_FOLIO_TIEMPO_MUERTO_CHECADA ) or
               ( Folio = K_FOLIO_CEDULA_CAPTURADO );
end;
begin
     for i := 0 to ( Count - 1 ) do
     begin
          if Items[ i ].Escribir and (  NoMulitlote( Items[ i ].Folio ) )then
          begin
               sHora := Items[ i ].HoraA;
               iFolio := Items[ i ].Folio;
               for j := ( i + 1 ) to ( Count - 1 ) do
               begin
                    with Items[ j ] do
                    begin
                         if PuedeEscribir and ( HoraA = sHora ) and ( Folio = iFolio ) then
                         begin
                              Folio := ZetaCommonTools.iMax( iFolio, K_FOLIO_MAXIMO ) + 1;
                         end;
                    end;
               end;
          end;
     end;
end;

procedure TLKardex.AjustarPosterior( var dValue: TDate; var sHora: THoraLabor );
begin
     if ( dValue > Fecha ) then
     begin
          sHora := HourPlusMinutes( sHora, K_24HORAS );
          dValue := Fecha;
     end;
end;

procedure TLKardex.CargarManuales;
begin
     Clear;
     with FQChecadasLee do
     begin
          Active := False;
     end;
     with oZetaProvider do
     begin
          ParamAsInteger( FQChecadasLee, 'Empleado', Empleado );
          ParamAsDate( FQChecadasLee, 'Fecha', Fecha );
     end;
     with FQChecadasLee do
     begin
          Active := True;
          while not Eof do
          begin
               with Add do
               begin
                    FechaA := FieldByName( 'AU_FECHA' ).AsDateTime;
                    FechaR := FieldByName( 'WK_FECHA_R' ).AsDateTime;
                    Manual := ZetaCommonTools.zStrToBool( FieldByName( 'WK_MANUAL' ).AsString );
                    HoraR := FieldByName( 'WK_HORA_R' ).AsString;
                    { Se inicializa la hora ajustada a la hora real }
                    { �nicamente con las checadas manuales }
                    if Manual then
                       HoraA := HoraR
                    else
                        HoraA := FieldByName( 'WK_HORA_A' ).AsString;
                    Operacion := FieldByName( 'OP_NUMBER' ).AsString;
                    Orden := FieldByName( 'WO_NUMBER' ).AsString;
                    Parte := FieldByName( 'AR_CODIGO' ).AsString;
                    Piezas := FieldByName( 'WK_PIEZAS' ).AsFloat;
                    Folio := FieldByName( 'WK_FOLIO' ).AsInteger;
                    TerminalID := FieldByName( 'WK_LINX_ID' ).AsString;
                    Tipo := eTipoLectura( FieldByName( 'WK_TIPO' ).AsInteger );
                    Kind := DetermineKind( Tipo );
                    Status := eStatusLectura( FieldByName( 'WK_STATUS' ).AsInteger );
                    Modula1 := FieldByName( 'WK_MOD_1' ).AsString;
                    Modula2 := FieldByName( 'WK_MOD_2' ).AsString;
                    Modula3 := FieldByName( 'WK_MOD_3' ).AsString;
                    TMuerto := FieldByName( 'WK_TMUERTO' ).AsString;
                    Area := FieldByName( 'CB_AREA' ).AsString;
                    Puesto := DeterminarPuesto( FieldByName( 'CB_PUESTO' ).AsString );
                    Cedula := FieldByName( 'WK_CEDULA' ).AsInteger;
                    Escribir := True; { Las manuales SIEMPRE se deben escribir }
                    if EsPrecalculada then
                    begin
                         Tiempo := FieldByName( 'WK_PRE_CAL' ).AsFloat;
                         Duracion := Tiempo;
                    end
                    else
                        if EsBreak then { Es un Break capturado manualmente }
                        begin
                             Tiempo := FieldByName( 'WK_PRE_CAL' ).AsFloat;
                             Duracion := Tiempo;
                             { Indicar que ya se agregaron los breaks de la jornada }
                             FBreakAdded := True;
                        end
                        else
                        begin
                             Duracion := 0;
                             Tiempo := 0;
                        end;
               end;
               Next;
          end;
          Active := False;
     end;
end;

procedure TLKardex.AgregaChecadaAsistencia( const sAjustada, sReal: THoraLabor; const eKind: eKindLectura );
begin
     with Add do
     begin
          FechaA := Fecha;
          HoraA := sAjustada;
          FechaR := Fecha;
          HoraR := sReal;
          Kind := eKind;
          Manual := False;
     end;
end;

procedure TLKardex.AgregaHorarioEntrada;
begin
     with FDatosHorario do
     begin
          AgregaChecadaAsistencia( Minutes2Hour( Entra24 ),
                                   Minutes2Hour( Entrada ),
                                   ktEntrada );
     end;
end;

procedure TLKardex.AgregaHorarioSalida;
begin
     with FDatosHorario do
     begin
          AgregaChecadaAsistencia( Minutes2Hour( Sale24 ),
                                   Minutes2Hour( Salida ),
                                   ktSalida );
     end;
end;

procedure TLKardex.AgregarUnBreak( const sFinal: THoraLabor; const iDuracion: Integer; const sTipo: TCodigo );
begin
     with Add do
     begin
          FechaA := Fecha;
          HoraA := sFinal;
          FechaR := Fecha;
          HoraR := sFinal;
          Duracion := iDuracion;
          Tipo := wtBreak;
          Kind := DetermineKind( Tipo );
          TMuerto := sTipo;
          Area := K_AREA_INDEFINIDA;
          Puesto := Self.Puesto;
          Manual := False;  { Para que no se carguen la siguiente ocasi�n que se calcule Kardex }
          Escribir := True; { Los Breaks SIEMPRE se deben escribir }
     end;
end;

procedure TLKardex.AgregaBreaks( const sCodigo: TCodigo; const sInicial, sFinal: THoraLabor );
begin
     if ZetaCommonTools.StrLleno( sCodigo ) then
     begin
          FBreakAdded := FBreaks.AgregaBreaks( sCodigo, sInicial, sFinal, AgregarUnBreak );
     end;
end;

procedure TLKardex.AgregarTiempoMuertoChecada(const sFinal: THoraLabor; const sTipo, sArea: TCodigo);
begin
     { Agregar TiempoMuerto cuando se especifica el Tipo }
     { Al agregar este Tiempo Muerto se debe volver a ejecutar }
     { el m�todo TLKardex.Calcular despu�s, dado que no esta checada tiene duraci�n }
     if ZetaCommonTools.StrLleno( FTipoTMuerto ) then
     begin
          with Add do
          begin
               FechaA := Fecha;
               HoraA := sFinal;
               FechaR := Fecha;
               HoraR := sFinal;
               Tipo := wtLibre;
               Kind := ktTiempoMuertoChecado;
               TMuerto := sTipo;
               Area := sArea;
               Puesto := Self.Puesto;
               Manual := False;  { Para que no se carguen la siguiente ocasi�n que se calcule Kardex }
               Escribir := True; { Los Tiempos Muertos SIEMPRE se deben escribir }
               Folio := K_FOLIO_TIEMPO_MUERTO_CHECADA;    { Evita violacion de llave primaria }
          end;
     end;
end;

procedure TLKardex.AgregarTiempoMuertoFijo( const sFinal: THoraLabor; const rDuracion: TDiasHoras; const sTipo, sArea: TCodigo );
begin
     { Solo agrega TiempoMuerto cuando se especifica el Tipo }
     if ( rDuracion > 0 ) and ZetaCommonTools.StrLleno( FTipoTMuerto ) then
     begin
          with Add do
          begin
               FechaA := Fecha;
               HoraA := sFinal;
               FechaR := Fecha;
               HoraR := sFinal;
               Duracion := rDuracion;
               Tipo := wtLibre;
               Kind := DetermineKind( Tipo );
               TMuerto := sTipo;
               Area := sArea;
               Puesto := Self.Puesto;
               Manual := False;  { Para que no se carguen la siguiente ocasi�n que se calcule Kardex }
               Escribir := True; { Los Tiempos Muertos SIEMPRE se deben escribir }
               Folio := K_FOLIO_TIEMPO_MUERTO;    { Evita violacion de llave primaria }
          end;
     end;
end;

procedure TLKardex.AgregarAutomaticaChecada( const sFinal: THoraLabor; const sArea: TCodigo );
begin
     { Agrega una checada Automatica Fija con duracion 0 }
     { Para que la duraci�n sea calculada posteriormente }
     AgregarAutomaticaFijo( sFinal, 0, sArea );
end;

procedure TLKardex.AgregarAutomaticaFijo( const sFinal: THoraLabor; const rDuracion: TDiasHoras; const sArea: TCodigo );
var
   sOperacion: String;
   iArea: Integer;
begin
     iArea := GetAreaPtr( sArea );
     sOperacion := FAreas.Items[ iArea ].Operacion;
     with Add do
     begin
          FechaA := Fecha;
          HoraA := sFinal;
          FechaR := Fecha;
          HoraR := sFinal;
          Duracion := rDuracion; //ZetaCommonTools.rMax( 0, ( rDuracion - FBreakTotal ) );
          Tipo := wtChecada;
          Kind := DetermineKind( Tipo );
          Area := FAreas.Items[ iArea ].Codigo;
          Puesto := Self.Puesto;
          Operacion := sOperacion;
          Manual := False;  { Para que no se carguen la siguiente ocasi�n que se calcule Kardex }
          Escribir := True; { Las Autom�ticas SIEMPRE se deben escribir }
          Folio := K_FOLIO_AREA;    { Evita violacion de llave primaria }
     end;
end;

procedure TLKardex.AgregarArea( const sHora: THoraLabor; const sArea: String );
begin
     with Add do
     begin
          FechaA := Fecha;
          HoraA := sHora;
          FechaR := Fecha;
          HoraR := sHora;
          Kind := ktArea;
          Area := sArea;
     end;
end;

procedure TLKardex.CargarAsistenciaNormal( var iEntradas, iSalidas: Integer );
var
   eKind: eKindLectura;
   lOk: Boolean;
begin
     with FQAsistenciaLee do
     begin
          Active := True;
          while not Eof do
          begin
               FCheco := True;
               eKind := TipoAsistencia2TipoLabor( eTipoChecadas( FieldByName( 'CH_TIPO' ).AsInteger ) );
               case eKind of
                    ktEntrada:
                    begin
                         lOk := True;
                         Inc( iEntradas );
                    end;
                    ktSalida:
                    begin
                         lOk := True;
                         Inc( iSalidas );
                    end;
               else
                   lOk := False;
               end;
               if lOk then
               begin
                    AgregaChecadaAsistencia( FieldByName( 'CH_H_AJUS' ).AsString,
                                             FieldByName( 'CH_H_REAL' ).AsString,
                                             eKind );
               end;
               Next;
          end;
          Active := False;
     end;
end;

procedure TLKardex.CargarAsistenciaIgnorando( var iEntradas, iSalidas: Integer );
var
   i: Integer;
   lOrdinaria: Boolean;
begin
     { Cargar checadas de Asistencia a FChecadas }
     with FQAsistenciaLee do
     begin
          Active := True;
          while not Eof do
          begin
               FCheco := True;
               with AddChecada do
               begin
                    Tipo := eTipoChecadas( FieldByName( 'CH_TIPO' ).AsInteger );
                    Descripcion := eDescripcionChecadas( FieldByName( 'CH_DESCRIP' ).AsInteger );
                    HoraReal := FieldByName( 'CH_H_REAL' ).AsString;
                    HoraAjustada := FieldByName( 'CH_H_AJUS' ).AsString;
               end;
               Next;
          end;
          Active := False;
     end;
     { Indica que no se ha encontrado una checada ordinaria }
     lOrdinaria := False;
     { Cargar todas las checadas extras ( Descripcion = Extras ) }
     { barriendo la lista de arriba hacia abajo }
     for i := 0 to ( CountChecadas - 1 ) do
     begin
          with Checadas[ i ] do
          begin
               if EsExtra then
               begin
                    { Todas las checadas extras se deben cargar }
                    AgregaChecadaAsistencia( HoraAjustada, HoraReal, Kind );
                    case Kind of
                         ktEntrada: Inc( iEntradas );
                         ktSalida: Inc( iSalidas );
                    end;
               end
               else
               begin
                    if not lOrdinaria then
                    begin
                         { Cuando la primera checada ordinaria }
                         { es una entrada hay que agregarla }
                         if ( Kind = ktEntrada ) then
                         begin
                              AgregaChecadaAsistencia( HoraAjustada, HoraReal, ktEntrada );
                              Inc( iEntradas );
                         end;
                    end;
                    { Indica que ya se proces� la primera checada ordinaria }
                    lOrdinaria := True;
               end;
          end;
     end;
     { Cargar la �ltima salida ordinaria }
     { barriendo la lista de abajo hacia arriba }
     for i := ( CountChecadas - 1 ) downto 0 do
     begin
          with Checadas[ i ] do
          begin
               if not EsExtra then
               begin
                    { Cuando la primera checada ordinaria }
                    { es una salida hay que agregarla }
                    if ( Kind = ktSalida ) then
                    begin
                         AgregaChecadaAsistencia( HoraAjustada, HoraReal, ktSalida );
                         Inc( iSalidas );
                    end;
                    { Como ya se proces� la primera checada ordinaria }
                    { no es necesario continuar el ciclo }
                    Break;
               end;
          end;
     end;
end;

procedure TLKardex.CargarAsistencia;
var
   iEntradas, iSalidas: Integer;
begin
     FCheco := False;
     FChecoInOut := False;
     iEntradas := 0;
     iSalidas := 0;
     with FQAsistenciaLee do
     begin
          Active := False;
     end;
     with oZetaProvider do
     begin
          ParamAsInteger( FQAsistenciaLee, 'Empleado', Empleado );
          ParamAsDate( FQAsistenciaLee, 'Fecha', Fecha );
     end;
     if FDatosHorario.IgnoraComer then
        CargarAsistenciaIgnorando( iEntradas, iSalidas )
     else
         CargarAsistenciaNormal( iEntradas, iSalidas );
     { El empleado checo de entrada y salida si hay m�s de una checada }
     { de entrada y de salida y ambos totales son iguales }
     FChecoInOut := ( iEntradas > 0 ) and ( iSalidas > 0 ) and ( iEntradas = iSalidas );
end;

procedure TLKardex.AgregarBreaks;
var
   sArea: TCodigo;
   sInicial, sFinal: THoraLabor;
   i, iArea: Integer;
begin
     if not FBreakAdded and ( Count > 0 ) then
     begin
          Ordena; { Ordena por hora ajustada }
          i := Count - 1;
          sFinal := Items[ i ].HoraA;
          while ( i >= 0 ) do
          begin
               with Items[ i ] do
               begin
                    if EsArea then { Buscar el primer cambio de area en el d�a }
                    begin
                         sArea := Area;
                         sInicial := Items[ i ].HoraA;
                         with FAreas do
                         begin
                              iArea := Find( sArea );
                              if ( iArea >= 0 ) then
                              begin
                                   AgregaBreaks( Items[ iArea ].GetBreak( Self.Fecha ), sInicial, sFinal );
                              end;
                         end;
                         sFinal := sInicial;
                    end;
               end;
               Dec( i );
          end;
     end;
end;

procedure TLKardex.AgregaEntradaSalida;
var
   lAddIn, lAddOut: Boolean;
begin
     if not ChecoInOut then //and ( ( iCount > 0 ) or Checo or Asistio ) then
     begin
          Ordena;           { Ordena por hora ajustada }
          if ( Count > 0 ) then
          begin
               with FDatosHorario do
               begin
                    { La hora de la primera checada es posterior a la hora de entrada }
                    lAddIn := ( Hour2Minutes( Items[ 0 ].HoraA ) > ( Entra24 + GraciaInTarde ) );
                    { La hora de la �ltima checada es anterior a la hora de salida }
                    lAddOut := ( Hour2Minutes( Items[ Count - 1 ].HoraA ) < ( Sale24 - GraciaOutTemprano ) );
               end;
          end
          else
          begin
               lAddIn := True;
               lAddOut := True;
          end;
          if lAddIn then
          begin
               AgregaHorarioEntrada;
          end;
          if lAddOut then
          begin
               AgregaHorarioSalida;
          end;
     end;
end;

procedure TLKardex.CargarAreas;
var
   sInicial {sFinal }, sEntrada,sSalida: THoraLabor;
   dValue,dEntrada,dSalida: TDate;
   sArea, sUltima: TCodigo;

procedure GetEntradasSalidas(var sEntrada:THoraLabor; var dEntrada:TDate; var sSalida:THoraLabor;var dSalida:TDate );
var i:Integer;
 bPrimera : Boolean;
begin
     i:= 0;
     bPrimera := False;
     while (  Count  > i )do
     begin
          with Items[i]do
          begin
               if EsEntrada and not bPrimera then
               begin
                    sEntrada := HoraA;
                    dEntrada := FechaA;
                    bPrimera := True;
               end;
               if EsSalida then
               begin
                    sSalida := HoraA;
                    dSalida := FechaA;
               end;
               Inc(i);
          end;
     end;
     AjustarMadrugada( dEntrada, sEntrada );
     AjustarMadrugada( dSalida, sSalida );
end;

begin
     if ( Count > 1 ) then { Hay por lo menos dos checadas }
     begin
          Ordena;       { Ordena por hora ajustada }
          GetEntradasSalidas(sEntrada,dEntrada,sSalida,dSalida);
          {with Items[ 0 ] do
          begin
               sInicial := HoraA;
               dInicial := FechaA;
          end;
          sEntrada := sInicial;
          dEntrada := dInicial;
          AjustarMadrugada( dInicial, sInicial );
          with Items[ Count - 1 ] do
          begin
               sFinal := HoraA;
               dFinal := FechaA;
          end;
          AjustarMadrugada( dFinal, sFinal );}

          with oZetaProvider do
          begin
               ParamAsInteger( FQAreasLee, 'Empleado', Empleado );
               ParamAsDate( FQAreasLee, 'FechaInicial', dEntrada );
               ParamAsChar( FQAreasLee, 'HoraInicial', sEntrada, K_ANCHO_HH_MM );
               ParamAsDate( FQAreasLee, 'FechaFinal', dSalida );
               ParamAsChar( FQAreasLee, 'HoraFinal', sSalida, K_ANCHO_HH_MM );
               sUltima := '';
               with FQAreasLee do
               begin
                    Active := True;
                    while not Eof do
                    begin
                         sArea := FieldByName( 'CB_AREA' ).AsString;
                         { Evita Asignaciones A La Misma Area }
                         if ( sArea <> sUltima )  then
                         begin
                              dValue := FieldByName( 'KA_FECHA' ).AsDateTime;
                              sInicial := FieldByName( 'KA_HORA' ).AsString;
							  { Movimiento del d�a posterior: hay que ajustar }
                              AjustarPosterior(dValue,sInicial);
							  {Agregar Checada de Cambio de Area }
                              AgregarArea( sInicial, sArea );
                              sUltima := sArea;
                         end;
                         Next;
                    end;
                    Active := False;
               end;
          end;
     end;
end;

{$define BUG_EXTRAS_VS_ORDINARIO}

function TLKardex.CalculaExtras( const sFinal, sInicial: THoraLabor; const iChecada: Integer ): Integer;
var
   j: Integer;
   sInicioEntrada: THoraLabor;
begin
     Result := CalculaMinutos( sFinal, sInicial );
     j := iChecada - 1;
     while ( j >= 0 ) do
     begin
          with Items[ j ] do
          begin
               if Calculable then
               begin
                    if ( HoraA <= sInicial ) then
                    begin
                         { Esta checada termina fuera del rango de extras a revisar }
                         Break;
                    end
                    else if ( HoraA < sFinal ) then   // La Checada termina en el periodo extra a revisar
                    begin
                         if EsEntrada then
                         begin
                              { Si hay una salida anterior se considera un break en la jornada }
                              sInicioEntrada := HoraA;
                              while ( j > 0 ) and ( not Items[ j - 1 ].Calculable ) do
                                    j := j - 1;                                        // Si no es calculable se ignora
                              if ( j > 0 ) and Items[ j - 1 ].EsSalida then
                              begin
                                   Result := Result - ( Hour2Minutes( sInicioEntrada ) -
                                                        iMax( Hour2Minutes( Items[ j - 1 ].HoraA ),
                                                              Hour2Minutes( sInicial )
                                                              )
                                                        );
                                   { Brinca la checada de salida para que esta no sea procesada }
                                   j := j - 1;
                              end;
                         end
                         else
                         if EsBreak then
                         begin
                              { Descuenta la Duraci�n del Break }
                              Result := Result - Trunc( Duracion );
                         end;
                    end;
               end;
          end;
          Dec( j );
     end;
     Result := iMax( Result, 0 );
end;

procedure TLKardex.AjustarExtras;
var
   i: Integer;
   rExtras, rOrdinarias, rTotalOrdinarias, rDobles, rTriples: TDiasHoras;
   sEntrada, sSalida: THoraLabor;
   lNoEsHabil: Boolean;
begin

     if ( Count > 0 ) then
     begin
          with FDatosTarjeta do
          begin
               rTotalOrdinarias := HorasAMinutos( Ordinarias );
               rDobles := HorasAMinutos( Dobles );
               rTriples := HorasAMinutos( Triples );
          end;
          with FDatosHorario do
          begin
               sEntrada := Minutes2Hour( Entra24 ); { No se considera - GraciaInTemprano xq Asistencia ajusta la hora en base a esto };
               sSalida := Minutes2Hour( Sale24 );  { No se considera + GraciaOutTarde xq Asistencia ajusta la hora en base a esto };
          end;
          lNoEsHabil := ZetaServerTools.NoEsHabil( Status, FExtraSabado, FExtraDescanso );

          for i := 0 to ( Count - 1 ) do
          begin
               with Items[ i ] do
               begin
                    if ( EsChecada or EsPrecalculada ) and TieneDuracion then { Antes si se consideraban los breaks }
                    begin
                         rExtras := 0;
                         if ( HoraI < sEntrada ) then
                         begin
                              if ( HoraA > sEntrada ) then { Entrada cae dentro del intervalo trabajado }
                              begin
                                   { Calcula el tiempo extra }
                                   {$ifdef BUG_EXTRAS_VS_ORDINARIO}
                                   if ( CalculaMinutos( sEntrada, HoraI ) < Duracion ) then   // Hay E/S
                                      rExtras := rExtras + CalculaExtras( sEntrada, HoraI, i )
                                   else
                                   {$endif}
                                   rExtras := rExtras + CalculaMinutos( sEntrada, HoraI );
                              end
                              else
                              begin
                                   { Todo es tiempo extra }
                                   {$ifdef BUG_EXTRAS_VS_ORDINARIO}
                                   if ( CalculaMinutos( HoraA, HoraI ) < Duracion ) then   // Hay E/S
                                      rExtras := rExtras + CalculaExtras( HoraA, HoraI, i )
                                   else
                                   {$endif}
                                   rExtras := rExtras + Duracion;
                              end;
                         end;
                         if ( HoraA > sSalida ) then
                         begin
                              if ( HoraI > sSalida ) then { Salida cae fuera del intervalo trabajado }
                              begin
                                   { Todo es tiempo extra }
                                   {$ifdef BUG_EXTRAS_VS_ORDINARIO}
                                   if ( CalculaMinutos( HoraA, HoraI ) < Duracion ) then   // Hay E/S
                                      rExtras := rExtras + CalculaExtras( HoraA, HoraI, i )
                                   else
                                   {$endif}
                                   rExtras := rExtras + Duracion;
                              end
                              else
                              begin
                                   { Calcula el tiempo extra }
                                   {$ifdef BUG_EXTRAS_VS_ORDINARIO}
                                   if ( CalculaMinutos( HoraA, sSalida ) < Duracion ) then  // Hay E/S
                                      rExtras := rExtras + CalculaExtras( HoraA, sSalida, i )
                                   else
                                   {$endif}
                                   rExtras := rExtras + CalculaMinutos( HoraA, sSalida );
                              end;
                         end;
                         { Primero asigna el total de posibles horas Ordinarias para la checada }

                         if ( Duracion >= rTotalOrdinarias )then
                            rOrdinarias := ZetaCommonTools.rMax( rTotalOrdinarias, ( Duracion - rExtras ) )
                         else
                             rOrdinarias := Duracion - rExtras;


                         if lNoEsHabil then
                         begin
                              { Si hay ordinarias en un d�a no h�bil }
                              { hay que descontarlas antes de sumar las }
                              { ordinarias a las extras por descontar }
                              if ( rTotalOrdinarias > 0 ) then
                              begin
                                   { Las horas ordinarias son mayores que las que hay que repartir }
                                   if ( rOrdinarias > rTotalOrdinarias ) then
                                   begin
                                        { Se topan a las ordinarias disponibles }
                                        Ordinarias := rTotalOrdinarias;
                                        { Ya no hay ordinarias por repartir }
                                        rTotalOrdinarias := 0;
                                        { Hay que descontar las horas ordinarias asignadas }
                                        { de las horas ordinarias calculadas }
                                        rOrdinarias := ZetaCommonTools.rMax( 0, ( rOrdinarias - Ordinarias ) );
                                   end
                                   else
                                   begin
                                        { Las horas ordinarias de la checada son las horas ordinarias calculadas }
                                        Ordinarias := rOrdinarias;
                                        { Se quitan las ordinarias de la checada de las ordinarias por repartir }
                                        rTotalOrdinarias := ZetaCommonTools.rMax( 0, ( rTotalOrdinarias - Ordinarias ) );
                                        { Ya no hay horas ordinarias calculadas por repartir }
                                        rOrdinarias := 0;
                                   end;
                              end;
                              { Si no es h�bil, hay que acumular las ordinarias }
                              { posibles a las horas extras por asignar a la checada }
                              rExtras := rExtras + rOrdinarias;
                         end
                         else
                         begin
                              { Las horas ordinarias son mayores que las que hay que repartir }
                              if ( rOrdinarias > rTotalOrdinarias ) then
                              begin
                                   { Se topan a las ordinarias disponibles }
                                   Ordinarias := rTotalOrdinarias;
                                   { Ya no hay ordinarias por repartir }
                                   rTotalOrdinarias := 0;
                              end
                              else
                              begin
                                   { Las horas ordinarias de la checada son las horas ordinarias calculadas }
                                   Ordinarias := rOrdinarias;
                                   { Se quitan las ordinarias de la checada de las ordinarias por repartir }
                                   rTotalOrdinarias := ZetaCommonTools.rMax( 0, ( rTotalOrdinarias - Ordinarias ) );
                              end;
                         end;
                         { Asignar Horas Dobles y Triples cuando hay horas extras por asignar }
                         if ( rExtras > 0 ) then
                         begin
                              { Hay que quitarle a las Extras encontradas las Extras Autorizadas }
                              if ( rDobles > rExtras ) then
                              begin
                                   { Las horas dobles cubren todo el tiempo de la checada }
                                   rDobles := ZetaCommonTools.rMax( 0, ( rDobles - rExtras ) );
                                   Dobles := rExtras;
                                   { No hay horas triples }
                                   Triples := 0;
                              end
                              else
                              begin
                                   { Las horas dobles cubren parcialmente el tiempo de la checada }
                                   Dobles := rDobles;
                                   { Ya no hay horas dobles por repartir en el d�a }
                                   rDobles := 0;
                                   { Se descuentan las dobles asignadas de las horas extras de la checada }
                                   rExtras := ZetaCommonTools.rMax( 0, ( rExtras - Dobles ) );
                                   if ( rTriples > rExtras ) then
                                   begin
                                        { Las horas triples cubren todo el tiempo restante de la checada }
                                        rTriples := ZetaCommonTools.rMax( 0, ( rTriples - rExtras ) );
                                        Triples := rExtras;
                                   end
                                   else
                                   begin
                                        { Ya no hay horas triples por repartir en el d�a }
                                        Triples := rTriples;
                                        rTriples := 0;
                                   end;
                              end;
                         end;
                         { Ajustar Tiempos Muertos }
                         if EsTiempoMuerto then
                         begin
                              if lNoEsHabil then
                              begin
                                   { Cuando el d�a no es h�bil, se topan las Ordinarias de }
                                   { una checada de tiempo muerto a las que hab�a disponibles }
                                   rOrdinarias := Ordinarias;
                                   { Cuando el d�a es h�bil, las ordinarias son las que se calcularon }
                                   { de acuerdo a la posici�n de la checada en el horario }
                              end;
                              if ( ( rOrdinarias > 0 ) or ( Dobles > 0 ) or ( Triples > 0 ) ) then
                              begin
                                   { Ajusta la duraci�n del Tiempo Muerto a la suma de Ordinarias No Topadas + Dobles + Triples }
                                   Duracion := ZetaCommonTools.rMin( Duracion, ( rOrdinarias + Dobles + Triples ) );
                              end
                              else
                              begin
                                   { Los tiempos muertos deben desaparecer si no hay Ordinarias No Topadas ni Dobles Ni Triples }
                                   Escribir := False;
                              end;
                         end;
                    end;
               end;
          end;
     end;
end;

function TLKardex.CheckForDuplicate( const iPtr: Integer; const sHora: THoraLabor; const eKind: eKindLectura; const sArea: TCodigo ): Boolean;
var
   i: Integer;
begin
     Result := False;
     for i := 0 to ( Count - 1 ) do
     begin
          if ( i <> iPtr ) then
          begin
               with Items[ i ] do
               begin
                    if ( HoraA = sHora ) and ( Kind = eKind ) and ( Area = sArea )and (not EsMultilote) then
                    begin
                         Result := True;
                         Break;
                    end;
               end;
          end;
     end;
end;

function TLKardex.GetCambioAreaAnterior( const iPos: Integer ): Integer;
var
   i: Integer;
begin
     {
     L�mite inferior: primera checada � el �ltimo cambio de
     �rea anterior a la checada cuya posici�n se pasa como par�metro
     }
     Result := 0;
     for i := ( iPos - 1 ) downto 0 do
     begin
          with Items[ i ] do
          begin
               if EsArea and ( Area <> Items[ iPos ].Area ) then
               begin
                    { El l�mite inferior es la primera checada }
                    { posterior a este cambio de �rea }
                    Result := i + 1;
                    Break;
               end;
          end;
     end;
end;

function TLKardex.GetCambioAreaPosterior( const iPos: Integer ): Integer;
var
   i: Integer;
begin
     {
     L�mite superior: �ltima checada o el primer cambio de
     �rea posterior a la excepci�n
     }
     Result := ( Count - 1 );
     for i := ( iPos + 1 ) to ( Count - 1 ) do
     begin
          with Items[ i ] do
          begin
               if EsArea and ( Area <> Items[ iPos ].Area ) then
               begin
                    { El l�mite inferior es la primera checada }
                    { anterior a este cambio de �rea }
                    Result := i - 1;
                    Break;
               end;
          end;
     end;
end;

procedure TLKardex.ProcesarExcepcionesPrecalculadas;
var
   i, j, iLower, iUpper: Integer;
   sEntrada:string;

function GetPrimeraEntradaTarjeta:string;
var index:Integer;
begin
     Result :=  Minutes2Hour( FDatosHorario.Entra24 );
     index := 0;
     while( index < (Count - 1) )do
     begin
          if Items[index].EsEntrada then
          begin
               Result := Items[index].HoraA;
               break;
          end
          else
              Inc(index);
     end;
end;

begin
     { Inicializar contador en la checada m�s grande }
     i := Count - 1;
     {
     Repasar de abajo hacia arriba ( m�s tarde a menos tarde ) el kardex
     para hacer valer la prioridad de las excepciones sobre las checadas ordinarias
     }
     sEntrada := GetPrimeraEntradaTarjeta;
     while ( i >= 0 ) do
     begin
          with Items[ i ] do
          begin
               { Unicamente procesar las excepciones precalculadas calculables }
               if ( (Calculable and Excepcion and EsPrecalculada) or ( EsTiempoMuerto and Calculable and (Cedula > 0)  ) )  then
               begin
                    {
                    Encontrar los l�mites inferiores y superiores para los
                    cuales es efectiva esta excepci�n:
                    }
                    iLower := GetCambioAreaAnterior( i );
                    iUpper := GetCambioAreaPosterior( i );
                    {
                    Primero checar que las Precalculadas Normales no tengan
                    conflictos con esta checada Precalculada Excepci�n
                    }
                    for j := iLower to iUpper do
                    begin
                         if ( j <> i ) then
                         begin
                              with Items[ j ] do
                              begin
                                   { Unicamente procesar las checadas de la misma �rea que la excepci�n }
                                   if Calculable and (not Excepcion {$ifdef HYUNDAI}or Items[i].EsTiempoMuerto{$ENDIF} ) and  EsPrecalculada and ( ( Area = Items[ i ].Area ) or ( FProcesarExcecionDifAreas )) {$ifdef HYUNDAI}and ( not EsTiempoMuerto ){$ENDIF}then
                                   begin
                                        Items[ j ].AjustarExcepcionPrecalculada( Self, Items[ i ], j );
                                   end;
                              end;
                         end;
                    end;
                    {
                    Luego hay que revisar las checadas normales de producci�n
                    Inicia buscando la checada anterior inmediata
                    El l�mite inferior es el mismo que se encontr� anteriormente
                    }
                    j := i - 1;
                    while ( j >= iLower ) do
                    begin
                         with Items[ j ] do
                         begin
                              if Calculable and not Excepcion and EsChecada then
                              begin
                                   Items[ j ].AjustarExcepcionProduccion( Self, Items[ i ], j ,sEntrada);
                              end;
                         end;
                         Dec( j );
                    end;
               end;
          end;
          Dec( i );
     end;
end;

procedure TLKardex.ProcesarExcepcionesVariables;
var
   i, j, iLower: Integer;
begin
     { Inicializar contador en la checada m�s grande }
     i := Count - 1;
     {
     Repasar de abajo hacia arriba ( m�s tarde a menos tarde ) el kardex
     para hacer valer la prioridad de las excepciones sobre las checadas ordinarias
     }
     while ( i >= 0 ) do
     begin
          with Items[ i ] do
          begin
               { Unicamente procesar las excepciones de producci�n calculables }
               if Calculable and Excepcion and EsChecada then
               begin
                    {
                    Encontrar el l�mite inferiores para
                    el cual es efectiva esta excepci�n:
                    }
                    iLower := GetCambioAreaAnterior( i );
                    {
                    Empezar a buscar las checadas anteriores para
                    marcar como NoCalculables aquellas que no tienen
                    la prioridad suficiente
                    }
                    { Inicia buscando la checada anterior inmediata }
                    j := i - 1;
                    while ( j >= iLower ) do
                    begin
                         with Items[ j ] do
                         begin
                              {
                              Si es calculable y no es excepcion entonces
                              no puede ser procesada dado que la checada
                              excepcion Items[ i ] tiene jerarqu�a sobre
                              todo lo que le precede, hasta que se encuentra
                              otra excepci�n de producci�n
                              }
                              { Unicamente procesar las checadas de la misma �rea que la excepci�n }
                              if Calculable and ( ( Area = Items[ i ].Area ) or FProcesarExcecionDifAreas )  then
                              begin
                                   if Excepcion  then
                                   begin
                                        {
                                        Se debe ignorar hasta que se encuentra
                                        otra excepcion de producci�n o hasta
                                        que se llega al inicio de la lista de checadas
                                        }
                                        if EsChecada then
                                           Break;
                                   end
                                   else
                                   begin
                                        {
                                        Unicamente ignorar ciertos tipos de checadas
                                        }
                                        case Kind of
                                             ktChecada: Ignorar;
                                             ktPrecalculada: Ignorar;
                                             ktFija: Ignorar;
                                             {$ifndef HYUNDAI}
                                             ktTiempoMuerto: Ignorar;
                                             ktTiempoMuertoChecado: Ignorar;
                                             {$endif}
                                        end;
                                   end;
                              end;
                         end;
                         Dec( j );
                    end;
               end;
          end;
          Dec( i );
     end;
end;

procedure TLKardex.ProcesarExcepciones;
begin
     if FProcesarExcepciones then
     begin
          { Ordena por hora ajustada }
          Ordena;
          { Primero revisar excepciones de tiempo fijo: tiempo muerto y precalculadas }
          ProcesarExcepcionesPrecalculadas;
          { Ordena por hora ajustada, dado que al procesar excepciones puede cambiar la hora de ciertas checadas }
          Ordena;
          { Luego revisar excepciones de tiempo variable: producci�n, fijas y tiempo muerto checado }
          ProcesarExcepcionesVariables;
     end;
end;

procedure TLKardex.AjustarPosterioresSalida;
type
    eLapseMode = ( emNormal, emExitAvail, emExitUnAvail );
var
   eMode: eLapseMode;
   i, j:Integer;
   sHora: THoraLabor;
begin
     if ( Count > 0 ) then
     begin
          Ordena;
          { Primero se empieza marcando que no hay salidas disponibles }
          { dado que todav�a no se ha encontrada la entrada que hace }
          { las checadas posteriores }
          eMode := emExitUnAvail;
          sHora := '';
          i := 0;
          while ( i < Count ) do
          begin
               with Items[ i ] do
               begin
                    if EsSalida then
                    begin
                         { Marcar que se entra en una zona posterior a una salida }
                         eMode := emExitAvail;
                         sHora := HoraA;
                    end
                    else
                        if EsEntrada then
                        begin
                             eMode := emNormal;
                        end
                        else
                        begin
                             case eMode of
                                  { Indica que esta checada no debe serle calculado su duraci�n }
                                  { ya que es posterior a una salida y tambi�n es posterior a la }
                                  { primera checada posterior a la salida }
                                  emExitUnAvail: Ignorar;
                                  emExitAvail:
                                  begin
                                       if ( EsChecada or EsPrecalculada ) then
                                       begin
                                            { Hay que preguntar si las precalculadas iniciaron }
                                            { despu�s de la hora de la �ltima salida, en cuyo }
                                            { caso no deben ser calculadas porque caen en un per�odo }
                                            { de tiempo en el cual el empleado no estaba en la empresa }
                                            if EsPrecalculada then
                                            begin
                                                 { Compara la hora de la �ltima salida con la }
                                                 { hora de inicio de la checada precalculada }
                                                 { Se usa la propiedad Tiempo porque Duraci�n todav�a }
                                                 { no est� determinada. Tiempo indica la duraci�n total }
                                                 { del intervalo, sin importar que hayan existido breaks }
                                                 if ( sHora < HourPlusMinutes( HoraA, Trunc( -Tiempo ) ) ) then
                                                 begin
                                                      Ignorar;
                                                 end;
                                            end;
                                            if Calculable then
                                            begin
                                                 { Si existe una checada del mismo tipo a la misma hora }
                                                 { debe ser marcada como no calculable }
                                                 if CheckForDuplicate( i, sHora, Kind, Area ) and (not Excepcion )then
                                                 begin
                                                      Ignorar;
                                                 end;
                                            end;
                                            if Calculable then
                                            begin
                                                 { Cancela el modo de Salida Con Tiempo Disponible }
                                                 { para indicar que cualquier checada posterior a esta }
                                                 { y anterior a una entrada no se le debe calcular tiempo }
                                                 {
                                                 eMode := emExitUnAvail;
                                                 }
                                                 if EsPrecalculada then
                                                 begin
                                                      { Hay que ajustar la duraci�n de la checada dado que se }
                                                      { adelanta la hora en la que termina }
                                                      Tiempo := ZetaCommonTools.rMax( 0, Tiempo - CalculaMinutos( HoraA, sHora ) );
                                                 end;
                                                 { Ajusta la hora de la checada a la hora de la salida }
                                                 HoraA := sHora;
                                                 if EsMultilote then
                                                 begin
                                                      { Inicializa el apuntador de Folio m�ximo del multilote }
                                                      //iFolio := Folio;
                                                      j := i + 1;
                                                      while ( j < Count ) do
                                                      begin
                                                           with Items[ j ] do
                                                           begin
                                                                if EsMultilote  then
                                                                begin
                                                                     HoraA := sHora;
                                                                     Inc( j );
                                                                end
                                                                else
                                                                    break;
                                                           end;
                                                      end;
                                                      { Posiciona en la �ltima checada del }
                                                      { multilote para que se procese la }
                                                      { siguiente checada }
                                                      i := j - 1;
                                                 end;
                                            end;
                                       end
                                       else
                                       begin
                                            Ignorar;
                                       end;
                                  end;
                             end;
                        end;
               end;
               Inc( i );
          end;
     end;
end;

procedure TLKardex.AgregaChecadaCedula;
begin
     with FDatosCedula do
     begin
          Fecha := FechaBusqueda;
          Hora := HoraBusqueda;
     end;
     with Add do
     begin
          FechaA := FDatosCedula.FechaWorks;
          HoraA := FDatosCedula.HoraWorks;
          FechaR := FDatosCedula.Fecha;
          HoraR := FDatosCedula.Hora;
          Tiempo := FDatosCedula.Tiempo;
          Tipo := FDatosCedula.Checada;
          Kind := DetermineKind( Tipo );
          TMuerto := FDatosCedula.TiempoMuerto;
          Area := FDatosCedula.Area;
          Cedula := FDatosCedula.Folio;
          Operacion := FDatosCedula.Operacion;
          Orden := FDatosCedula.Orden;
          Parte := FDatosCedula.Parte;
          Status := FDatosCedula.Status;
          Modula1 := FDatosCedula.Modula1;
          Modula2 := FDatosCedula.Modula2;
          Modula3 := FDatosCedula.Modula3;
          Puesto := Self.Puesto;
          TerminalID := K_LINX_ID_CEDULA;
          Manual := False;  { Para que no se carguen la siguiente ocasi�n que se calcule Kardex }
          Escribir := True; { Los Tiempos Muertos SIEMPRE se deben escribir }
          Folio := K_FOLIO_CEDULA;    { Evita violacion de llave primaria }
     end;
end;

procedure TLKardex.AgregaChecadaCedulaMulti(WKFolio:Integer;dPiezas:TDiasHoras);
begin
     with FDatosCedula do
     begin
          Fecha := FechaBusqueda;
          Hora := HoraBusqueda;
     end;
     with Add do
     begin
          FechaA := FDatosCedula.FechaWorks;
          HoraA := FDatosCedula.HoraWorks;
          FechaR := FDatosCedula.Fecha;
          HoraR := FDatosCedula.Hora;
          Tiempo := FDatosCedula.Tiempo;
          Tipo := FDatosCedula.Checada;
          Kind := DetermineKind( Tipo );
          TMuerto := FDatosCedula.TiempoMuerto;
          Area := FDatosCedula.Area;
          Cedula := FDatosCedula.Folio;
          Operacion := FDatosCedula.Operacion;
          Orden := FDatosCedula.Orden;
          Parte := FDatosCedula.Parte;
          Status := FDatosCedula.Status;
          Modula1 := FDatosCedula.Modula1;
          Modula2 := FDatosCedula.Modula2;
          Modula3 := FDatosCedula.Modula3;
          Puesto := Self.Puesto;
          TerminalID := K_LINX_ID_CEDULA;
          Piezas := dPiezas;
          Manual := False;  { Para que no se carguen la siguiente ocasi�n que se calcule Kardex }
          Escribir := True; { Los Tiempos Muertos SIEMPRE se deben escribir }
          Folio := WKFolio;   { Evita violacion de llave primaria }
     end;
end;


procedure TLKardex.BuscarCedulas(const sArea: TCodigo; const dInicial, dFinal: TDate; const sInicial, sFinal: THoraLabor);
var
   dFecha: TDate;
   sHora: THoraLabor;
 procedure CargarMultilotes;
 begin
      with FDatosCedula do
      begin
           if MultiLote then
           begin
                try
                   with FLotesCedula do
                   begin
                        {Buscar las ordenes de multilote}
                        Active := False;
                        oZetaProvider.ParamAsInteger( FLotesCedula, 'CE_FOLIO', Folio );
                        Active := True;
                        First;
                        FLotes := TList.Create;
                        while( not Eof )do
                        begin
                             {Agregamos los multilotes}
                             with AddOrden do
                             begin
                                  Codigo := FieldByName('WO_NUMBER').AsString;
                                  Piezas := FieldByName('CW_PIEZAS').AsFloat;
                                  Producto := FieldByName('AR_CODIGO').AsString;
                                  Posicion := FieldByName('CW_POSICIO').AsInteger;
                             end;
                             Next;
                        end;
                   end;
                except
                      on Error: Exception do
                      begin
                           oZetaProvider.Log.Excepcion( 0, Format( 'Error Agregar Ordenes del MultiLote de la C�dula: %d', [ Folio ] ), Error );
                           //TerminaTransaccion( True );
                      end;
                end;
           end;
      end;
 end;

 procedure AgregarChecadasMultilote;
 var i,OffSet:Integer;
 begin
      with FDatosCedula do
      begin
            OffSet := 0;
          if Tipo = tcOperacion then
             OffSet := K_OFFSET_PRODUCCION
             else
                 if Tipo = tcEspeciales then
                    OffSet := K_OFFSET_ESPECIALES
                    else
                        if Tipo = tcTMuerto then
                           OffSet := K_OFFSET_TIE_MUERTO;
           for i := 0 to FLotes.Count - 1 do
           begin
                Orden  :=  Lotes[i].Codigo;
                Parte  :=  Lotes[i].Producto;
                Piezas :=  Lotes[i].Piezas;
                AgregaChecadaCedulaMulti(Lotes[i].Posicion + OffSet,Piezas);
           end;
      end;
 end;


begin
     with oZetaProvider do
     begin
          ParamAsChar( FQCedulas, 'Area', sArea, K_ANCHO_AREA );
          ParamAsDate( FQCedulas, 'FechaInicial', dInicial );
          ParamAsChar( FQCedulas, 'HoraInicial', sInicial, K_ANCHO_HH_MM );
          ParamAsDate( FQCedulas, 'FechaFinal', dFinal );
          ParamAsChar( FQCedulas, 'HoraFinal', sFinal, K_ANCHO_HH_MM );
     end;
     with FQCedulas do
     begin
          Active := True;
          if not IsEmpty then
          begin
               while not Eof and FDatosCedula.FaltaExplicar do
               begin
                    dFecha := FieldByName( 'CE_FECHA' ).AsDateTime;
                    sHora := FieldByName( 'CE_HORA' ).AsString;
                    { Ajusta la fecha y hora cuando la fecha de la c�dula }
                    { encontrada es mayor a la fecha del c�lculo de tiempos }
                    AjustarPosterior( dFecha, sHora );
                    with FDatosCedula do
                    begin
                         Folio := FieldByName( 'CE_FOLIO' ).AsInteger;
                         Orden := FieldByName( 'WO_NUMBER' ).AsString;
                         Parte := FieldByName( 'AR_CODIGO' ).AsString;
                         Operacion := FieldByName( 'OP_NUMBER' ).AsString;
                         Area := FieldByName( 'CE_AREA' ).AsString;
                         Tipo := eTipoCedula( FieldByName( 'CE_TIPO' ).AsInteger );
                         Fecha := dFecha;
                         Hora := sHora;
                         Status := eStatusLectura( FieldByName( 'CE_STATUS' ).AsInteger );
                         Modula1 := FieldByName( 'CE_MOD_1' ).AsString;
                         Modula2 := FieldByName( 'CE_MOD_2' ).AsString;
                         Modula3 := FieldByName( 'CE_MOD_3' ).AsString;
                         TiempoMuerto := FieldByName( 'CE_TMUERTO' ).AsString;
                         MultiLote := ZetaCommonTools.zStrToBool( FieldByName( 'CE_MULTI' ).AsString );
						 Piezas := FieldByName( 'CE_PIEZAS' ).AsFloat;
                         CargarMultilotes;
                         HayEmpleados := False;
                         Usuario := FieldByName( 'US_CODIGO' ).AsInteger;
                         Tiempo := FieldByName( 'CE_TIEMPO' ).AsFloat;
                         { Marca que ya encontr� una c�dula }
                         { si no existe una checada del mismo tipo que la c�dula }
                         { a la hora del cambio de �rea }
                         { y si dicha c�dula puede explicar el tiempo }
                         Found := not CheckForDuplicate( -1, HoraBusqueda, Tipo2Kind( Checada ), Area ) and PuedeExplicar;
                    end;
                    if FDatosCedula.Found then
                    begin
                         if FDatosCedula.MultiLote then
                         begin
                              AgregarChecadasMultilote; 
                         end
                         else
                             AgregaChecadaCedula;
                    end;
                    Next;
               end;
          end;
          Active := False;
     end;
end;

function TLKardex.BuscaCedula( const sArea: TCodigo; const dFecha: TDate; const sHora: THoraLabor; const rDuracion: TDiasHoras): Boolean;
var
   iArea: Integer;
   sInicial: THoraLabor;
   dInicial: TDate;
begin
     sInicial := sHora;
     dInicial := dFecha;
     { Inicializar la c�dula para la b�squeda }
     FDatosCedula.InitBusqueda( rDuracion, dFecha, sHora );
     with FAreas do
     begin
          iArea := Find( sArea );
          if ( iArea >= 0 ) then
          begin
               { Primero hay que encontrar las c�dulas registradas }
               { en el mismo d�a que la checada del cambio de �rea }
               BuscarCedulas( sArea, dInicial, dInicial, sInicial, Minutes2Hour( K_48HORAS ) );
               { Preguntar si la hora inicial es de madrugada }
               if FDatosCedula.FaltaExplicar and EsDeMadrugada( sInicial ) then
               begin
                    { Ajustar la fecha al d�a siguiente y hora a formato de 24 horas }
                    AjustarMadrugada( dInicial, sInicial );
                    { Buscar al d�a siguiente con l�mite de la primera hora }
                    BuscarCedulas( sArea, dInicial, dInicial, sInicial, Items[ iArea ].PrimeraHora );
               end;
               if FDatosCedula.FaltaExplicar then
               begin
                    with Items[ iArea ] do
                    begin
                         case Turno of
                              tlNocturnoEntrada:
                              begin
                                   { Buscar al d�a siguiente entre las 00:00 horas y la primera hora }
                                   dInicial := dFecha + 1;
                                   BuscarCedulas( sArea, dInicial, dInicial, K_CERO_HORAS, Items[ iArea ].PrimeraHora );
                              end;
                              tlNocturnoSalida:
                              begin
                                   { Buscar al d�a Anterior entre la primera hora y las 24:00 horas }
                                   dInicial := dFecha - 1;
                                   BuscarCedulas( sArea, dInicial, dInicial, Items[ iArea ].PrimeraHora, K_MEDIANOCHE );
                              end;
                              tlMatutino:
                              begin
                                   { Buscar al d�a Anterior entre la primera hora y las 24:00 horas }
                                   BuscarCedulas( sArea, dInicial, dInicial, sInicial, Minutes2Hour( K_48HORAS ) );
                              end;
                         end;
                    end;
               end;
          end;
     end;
     Result := not FDatosCedula.FaltaExplicar;
end;

function TLKardex.RevisaCambiosArea: Boolean;
var
   i: Integer;
   sArea: TCodigo;

function EsUltimaSalida(index:Integer) :Boolean;
var indice:Integer;
    ultimo:Boolean;
begin
     indice := index;
     ultimo := Items[ indice ].EsSalida;
     while index < Count do
     begin
          with Items[ index ] do
          begin
               if EsSalida then
               begin
                    if ( EsSalida and ( indice < index ) )then
                       ultimo := False;
               end;
          end;
          Inc(index);
     end;
     Result:= ultimo;
end;

begin
     Result := False;
     { Primero hay que calcular los tiempos }
     Calcular;
     { Buscar el primer registro de Area para saber }
     { cual es el valor original para este d�a }
     for i := 0 to ( Count - 1 ) do
     begin
          with Items[ i ] do
          begin
               if EsArea then
               begin
                    sArea := Area;
                    Break;
               end;
          end;
     end;
     Inc( i );
     while ( i < Count ) do
     begin
          with Items[ i ] do
          begin
               {$ifdef CAMBIO_AREA}
               if (  EsArea and ( i > 0 ) and ( Items[ i - 1 ].HoraA <= HoraA ) ) or(  EsUltimaSalida(i) and (Duracion > 0) and ExisteArea(Area) )   then
               {$else}
               if EsArea and ( Duracion > FMaxAreaChange ) then
               {$endif}
               begin
                    { Buscar las c�dulas con las cuales poder explicar el tiempo }
                    BuscaCedula( sArea, FechaA, HoraA, Duracion );
                    { Si todav�a hay tiempo sin explicar }
                    { agregar checadas de tiempo muerto � de operaci�n default }
                    if FDatosCedula.FaltaExplicar then
                    begin
                         if ExisteArea( sArea ) then
                         begin
                              case FAreas.Items[ GetAreaPtr( sArea ) ].Tipo of
                                   taDirecto: AgregarTiempoMuertoChecada( HoraA, TipoTiempoMuerto, sArea );
                              else
                                  AgregarAutomaticaChecada( HoraA, sArea );
                              end;
                         end
                         else
                             { No Existe El Area, Simplemente agregar un tiempo muerto }
                             AgregarTiempoMuertoChecada( HoraA, TipoTiempoMuerto, sArea );
                    end;
                    Result := True;
                    { Cambia el �rea de Salida }
                    sArea := Area;
               end;
          end;
          Inc( i );
     end;
end;

{$define BUG_AVENT_HONDURAS}
{$define BUG_BREAKS_FUERA_JORNADA}

procedure TLKardex.CalcularInicios;
var
   i, j: Integer;
   sInicio: THoraLabor;
{$ifdef BUG_BREAKS_FUERA_JORNADA}
   sInicioEntrada: THoraLabor;
{$endif}
begin
     if ( Count > 0 ) then
     begin
          i := 1;
          while ( i < Count ) do
          begin
               with Items[ i ] do
               begin
                    HoraI := VACIO;
                    { Procesar �nicamente las checadas que son calculables }
                    if Calculable then
                    begin
                         if ( EsPrecalculada or EsChecada ) then
                         begin
                              sInicio := HourPlusMinutes( HoraA, Trunc( -Duracion ) ); { Inicio del intervalo cubierto por la checada }
                              j := i - 1;
                              while ( j >= 0 ) do
                              begin
                                   with Items[ j ] do
                                   begin
                                        {$ifndef BUG_AVENT_HONDURAS}
                                        { Si es una entrada que sucede despu�s de la hora de inicio propuesta }
                                        if EsEntrada and ( HoraA > sInicio ) then
                                        begin
                                             { Determina que la checada anterior existe y que es una Salida }
                                             if ( j > 0 ) and Items[ j - 1 ].EsSalida then
                                             begin
                                                  sInicio := HourPlusMinutes( sInicio, Trunc( -CalculaMinutos( HoraA, Items[ j - 1 ].HoraA ) ) );
                                                  { Brinca la checada anterior para que esta no sea procesada }
                                                  j := j - 1;
                                             end;
                                        end
                                        else
                                        {$endif}
                                        if Calculable then
                                        begin
                                             if ( HoraA <= sInicio ) then
                                             begin
                                                  { Esta checada termina antes que empiece la checada examinada }
                                                  { Terminar de buscar }
                                                  Break;
                                             end
                                             else
                                             begin
                                                  {$ifdef BUG_AVENT_HONDURAS}
                                                  {$ifdef BUG_BREAKS_FUERA_JORNADA}
                                                  if EsEntrada then
                                                  begin
                                                       { Si hay una salida anterior se considera un break en la jornada }
                                                       sInicioEntrada := HoraA;
                                                       while ( j > 0 ) and ( not Items[ j - 1].Calculable ) do
                                                             j := j - 1;                                        // Si no es calculable se ignora
                                                       if ( j > 0 ) and Items[ j - 1 ].EsSalida then
                                                       begin
                                                            sInicio := HourPlusMinutes( sInicio, Trunc( -CalculaMinutos( sInicioEntrada, Items[ j - 1 ].HoraA ) ) );
                                                            { Brinca la checada de salida para que esta no sea procesada }
                                                            j := j - 1;
                                                       end;
                                                  end
                                                  else
                                                  {$endif}
                                                  {$endif}
                                                  if EsBreak then
                                                  begin
                                                       { La nueva hora de inicio es el inicio del Break }
                                                       { Descuenta la Duraci�n del Break }
                                                       sInicio := HourPlusMinutes( sInicio, Trunc( -Duracion ) );
                                                  end;
                                             end;
                                        end;
                                   end;
                                   Dec( j );
                              end;
                              HoraI := sInicio;
                         end
                         else
                             if EsBreak then
                             begin
                                  HoraI := HourPlusMinutes( HoraA, -Trunc( Duracion ) );
                             end
                             else
                             begin
                                  HoraI := HoraA;
                             end;
                    end;
               end;
               Inc( i );
          end;
     end;
end;

procedure TLKardex.Calcular;
var
   sInicio,sPrimeraEntrada: THoraLabor;
   i, j, k, iUltima, iTope, iFolio, iCuantos,iCedulaAnterior: Integer;
   rMinutos, rAsignados, rTotal: TDiasHoras;
   lMismaCedula,lEsPrimera,bInOut:Boolean;

procedure CalcularMultiLotes;
begin
     with Items[ i ] do
     begin
          if EsMultilote then
          begin
               rTotal := 0;
               iCedulaAnterior := Cedula;
               lMismaCedula := True;
               { Averiguar el total de piezas en el Multilote }
               {$ifdef HYUNDAI}
               rTotal := ZetaCommonTools.rMax( 0, Piezas );
               {$else}
               rTotal := rTotal + ZetaCommonTools.rMax( 1, Piezas );
               {$endif}
               { Averiguar el total de checadas en el Multilote }
               iCuantos := 1;
               { Inicializa el apuntador de Folio m�ximo del multilote }
               iFolio := Folio;
               j := i + 1;
               { Inicializa el apuntador Tope del multilote }
               iTope := j;
               iCedulaAnterior := Cedula;
               while ( j < Count ) do
               begin
                    with Items[ j ] do
                    begin
                         lMismaCedula := iCedulaAnterior = Cedula;
                         if Calculable and EsMultilote and  lMismaCedula then
                         begin
                              { Si hay piezas, sumar al total }
                              { Si no hay piezas, suponer PIEZAS = 1 }
                              {$ifdef HYUNDAI}
                              rTotal := rTotal + ZetaCommonTools.rMax( 0, Piezas );
                              {$else}
                              rTotal := rTotal + ZetaCommonTools.rMax( 1, Piezas );
                              {$endif}
                              { Actualiza el apuntador Tope del multilote }
                              iTope := j;
                              { Actualiza el apuntador de Folio m�ximo del multilote }
                              iFolio := Folio;
                              { Incrementa el total de checadas }
                              Inc( iCuantos );
                              Inc( j );
                         end
                         else
                             break;
                    end;
               end;
               j := i;
               rAsignados := 0;
               while ( j <= iTope ) do
               begin
                    with Items[ j ] do
                    begin
                         if Calculable then
                         begin
                              if ZetaCommonTools.StrVacio( Area ) or ( Area = K_AREA_INDEFINIDA ) then
                                 Area := FAreaActiva;  { Especifica el Area }
                              if ( rTotal > 0 ) then
                                 {$ifdef HYUNDAI}
                                 Duracion := rMinutos * ( Piezas / rTotal ) { Asignar Proporcionalmente al # de piezas }
                                 {$else}
                                 Duracion := rMinutos * ( ZetaCommonTools.rMax( 1, Piezas ) / rTotal ) { Asignar Proporcionalmente al # de piezas }
                                 {$endif}
                              else
                                  Duracion := rMinutos / iCuantos; { Asignar proporcionalmene al # de checadas }
                              rAsignados := rAsignados + Duracion;
                              //HoraA := aHoraString ( Round2Integer ( aMinutos(HoraR) - ( rMinutos - ( ( rTotal - (j-1) ) * Duracion ) ) ) );
                         end;
                    end;
                    Inc( j );
               end;
               {Calcula el tiempo que falta para que la suma de la duracion de las Checadas
                cuadre con la duracion Total}
               with Items[ iTope ] do
               begin
                    Duracion := Duracion + ZetaCommonTools.rMax( 0, rMinutos - rAsignados );
               end;
               { Posiciona en la �ltima checada del }
               { multilote para que se procese la }
               { siguiente checada }
               i := j - 1;
          end
          else
              Duracion := rMinutos;
     end;
end;


function EsPrimeraEntrada( index:Integer ) :Boolean;
begin
     Result := Items[index].EsEntrada;
     if Result then
     begin
          index := index - 1;
          while index >= 0 do
          begin
               Result := not Items[index].EsEntrada;
               if not Result then
                  break
               else
                   index := index - 1;
          end;
     end;
end;

begin
     FPendiente := 0;         { Guarda los minutos pendientes de asignar }
     FUltimaChecada := '';    { Hora Ajustada de Ultima Checada ( usada para asignar Tiempo Muerto ) }
     FAreaActiva := '';{ Inicializar Area Activa }
     sPrimeraEntrada:= '';
     lEsPrimera:= True;
     if ( Count > 0 ) then
     begin
          Ordena;                  { Ordena por hora ajustada }
          iUltima := -1;           { Indice al Ultimo elemento procesado }
          with Items[ 0 ] do
          begin
               if EsArea then
               begin
                  FAreaActiva := Area;  { Inicializar Area Activa }
                  i:=1;
               end
               else
               begin
                    FAreaActiva := GetAreaDefault;
                    i:=0;
               end;
          end;
          while ( i < Count ) do
          begin
               with Items[ i ] do
               begin
                    if EsArea then
                    begin
                         FAreaActiva := Area;  { Cambiar Area Activa }
                         { Asigna Pendientes al Cambio de Area }
                         { para luego poder buscar C�dulas que }
                         { puedan explicar este tiempo }
                         if ( iUltima >= 0 ) then
                            Duracion := FPendiente + CalculaMinutos( HoraA, Items[ iUltima ].HoraA );
                    end
                    else
                    begin                                    
                         if ZetaCommonTools.StrVacio( Area ) or ( Area = K_AREA_INDEFINIDA ) then
                            Area := FAreaActiva;  { Especifica el Area }
                         if Calculable then
                         begin
                              { Validar que la primera checada calculable tenga siempre duraci�n cero }
                              if ( iUltima < 0 ) then
                                 rMinutos := 0
                              else
                                  rMinutos := rMax(0, CalculaMinutos( HoraA, Items[ iUltima ].HoraA ));
                              if EsEntrada or EsSalida then
                              begin
                                   if EsSalida then
                                   begin
                                        { Si Es Salida siempre se acumula a Pendientes }
                                        FPendiente := FPendiente + rMinutos;
                                        { Asigna Pendientes a la Salida }
                                        { para luego poder buscar Checadas posteriores }
                                        { a las cuales haya que ajustarles el tiempo }
                                        Duracion := rMinutos;
                                   end
                                   else
                                   begin
                                        { Si Es Entrada y la anterior es Salida: es un break }
                                        { por lo que no se debe acumular a Pendientes }
                                        { En caso contrario, siempre se acumula a Pendientes }
                                        if ( iUltima >= 0 ) and not Items[ iUltima ].EsSalida then
                                           FPendiente := FPendiente + rMinutos;
                                        if lEsPrimera then
                                        begin
                                             sPrimeraEntrada := HoraA;
                                             lEsPrimera:= False;
                                        end;
                                   end;
                              end
                              else
                                  if EsBreak then
                                  begin
                                       if ( rMinutos < Duracion ) then
                                       begin
                                            { Ajustar el Break al tiempo disponible }
                                            Duracion := rMinutos;
                                       end
                                       else
                                       begin
                                            { Solo se acumula la diferencia entre el tiempo del break }
                                            { y la duraci�n entre la checada y su predecesor }
                                            FPendiente := FPendiente + ( rMinutos - Duracion );
                                       end;
                                  end
                                  else
                                      if EsPrecalculada then
                                      begin
                                           rAsignados := 0;   { Acumulador del tiempo de breaks }
                                           sInicio := Inicio; { Inicio del intervalo cubierto por la checada }
                                           bInOut := False;
                                           j := i - 1;
                                           while ( j >= 0 ) do
                                           begin
                                                with Items[ j ] do
                                                begin
                                                     { Ignorar las checadas no calculables y las que son Cambios de Area }
                                                     if Calculable and not EsArea then
                                                     begin
                                                          if ( HoraA <= sInicio ) then
                                                          begin
                                                               { Esta checada termina antes que empiece la checada }
                                                               { Precalculada: hay que terminar de buscar }
                                                               Break;
                                                          end
                                                          else if (Items[j].Inicio >= sInicio)then
                                                          begin
                                                               rAsignados := rAsignados + Duracion
                                                          end
                                                          else
                                                          begin
                                                               if EsBreak or EsTiempoMuerto then
                                                               begin
                                                                    if ( sInicio >= HourPlusMinutes( HoraA, Trunc( -Duracion ) ) ) then
                                                                    begin
                                                                         { La checada Precalculada empieza durante el Break }
                                                                         { Hay que ajustar la hora de la checada Precalculada }
                                                                         { a la hora en la cual termina el Break y terminar}
                                                                         sInicio := HoraA;
                                                                         Break;
                                                                    end
                                                                    else
                                                                    begin
                                                                         { El Break cae totalmente durante el intervalo }
                                                                         { asignado a la checada Precalculada: hay que }
                                                                         { acumular el tiempo del break }
                                                                         rAsignados := rAsignados + Duracion;
                                                                    end;
                                                               end
                                                               else
                                                               begin
                                                                    { Si la hora es posterior al inicio del intervalo, fijar }
                                                                    { el inicio a la hora de esta checada y terminar el ciclo}
                                                                    //CalcularMultiLotes;
                                                                    sInicio := HoraA;
                                                                    Break;
                                                                    //Procesar Checadas  en In/Out - In/Out
                                                                    if Calculable and (not EsEntrada )and (Not EsSalida )then
                                                                    begin
                                                                         sInicio := HoraA;
                                                                         Break;
                                                                    end
                                                                    else
                                                                    if ( ( not Items[ j - 1 ].EsSalida ) and ( not EsPrimeraEntrada(j) ) ) then
                                                                    begin
                                                                         if Items[ j - 1 ].Calculable and (not Items[ j - 1 ].EsEntrada)then
                                                                         begin
                                                                              sInicio := HoraA;
                                                                              Break;
                                                                         end;
                                                                         k := j - 2;
                                                                         while( k >= 0 )do
                                                                         begin
                                                                              if Items[k].EsSalida then
                                                                              begin
                                                                                   rAsignados := rAsignados + CalculaMinutos( HoraA ,Items[ K ].HoraA );
                                                                                   Dec(j);
                                                                                   bInOut := True;
                                                                                   break;
                                                                              end
                                                                              else
                                                                                  Dec(k);
                                                                         end;
                                                                         //Si al terminar de buscar IN-Out se acaban las checadas conservar la hora de la anterior
                                                                         if ( ( not EsSalida ) and ( not bInOut ) ) then
                                                                         begin
                                                                              sInicio := HoraA;
                                                                              Break;
                                                                         end;
                                                                    end
                                                                    else
                                                                    if ( (EsEntrada )and (Items[ j - 1 ].EsSalida) and (Items[ j - 1 ].HoraA >= sInicio) )  then
                                                                    begin
                                                                         rAsignados := rAsignados + CalculaMinutos( HoraA ,Items[ j - 1 ].HoraA );
                                                                         Dec(j);
                                                                    end
                                                                    else if (not EsSalida )then
                                                                    begin
                                                                         { Si la hora es posterior al inicio del intervalo, fijar }
                                                                         { el inicio a la hora de esta checada y terminar el ciclo}
                                                                         //CalcularMultiLotes;
                                                                         sInicio := HoraA;
                                                                         Break;
                                                                    end;
                                                               end;
                                                          end;
                                                     end;
                                                end;
                                                Dec( j );
                                           end;

                                           { La duraci�n es la diferencia entre la hora de la checada }
                                           { y la hora de inicio del Intervalo menos la suma de los breaks }
                                           Duracion := CalculaMinutos( HoraA, sInicio ) - rAsignados;
                                           FPendiente := FPendiente + rMinutos - Duracion;
                                           if EsMultiLote then
                                           begin
                                                rMinutos := Duracion;
                                                CalcularMultiLotes;
                                           end;
                                           { Se acumula la diferencia entre el tiempo de la checada precalculada }
                                           { y la duraci�n entre la checada y su predecesor }
                                           //FPendiente := FPendiente + rMinutos - Duracion;

                                      end
                                      else
                                          if EsChecada then
                                          begin
                                               rMinutos := rMinutos + FPendiente; { Asigna el tiempo calculado m�s los minutos pendientes }
                                               CalcularMultiLotes; 
                                               FPendiente := 0; { Inicializa Acumulador de Pendientes }
                                          end
                                          else
                                              Advertencia( Empleado, 'Checada A Las ' + HoraR + ' De Tipo Desconocido' );
                              { Asigna el apuntador hacia la �ltima checada procesada }
                              iUltima := i;
                              FUltimaChecada := Items[ iUltima ].HoraA;
                         end;
                    end;
               end;
               Inc( i );
          end;
     end;
end;

procedure TLKardex.CalcularTiempos;
begin
     { Calcula la duraci�n de cada checada }
     if ( Count > 0 ) then
     begin
          Calcular;
          {$ifndef CAMBIO_AREA}
          { Averiguar si hay cambios de Area con tiempo pendiente }
          { de explicar para intentar explicarlo mediante una c�dula }
          if RevisaCambiosArea then
             Calcular;
          {$endif}
          if ( FPendiente > 0 ) and ( FUltimaChecada <> '' ) then
          begin
               { Especificar que el Area Default es la �ltima }
               { a la cual estuvo asignado el empleado en el d�a }
               if ExisteArea( FAreaActiva ) then
               begin
                    case FAreas.Items[ GetAreaPtr( FAreaActiva ) ].Tipo of
                         taDirecto: AgregarTiempoMuertoFijo( FUltimaChecada, FPendiente, TipoTiempoMuerto, FAreaActiva );
                    else
                         AgregarAutomaticaFijo( FUltimaChecada, FPendiente, FAreaActiva );
                    end;
               end
               else
                   if ( FAreaActiva = VACIO ) then
                   begin
                        AgregarTiempoMuertoFijo( FUltimaChecada, FPendiente, TipoTiempoMuerto, FAreaActiva );
                   end
                   else
                       AgregarTiempoMuertoFijo( FUltimaChecada, FPendiente, TipoTiempoMuerto, GetAreaDefault );
          end;
     end;
end;

function TLKardex.CheckForValidEntries: Boolean;
var
   i: Integer;
begin
     Result := False;
     for i := 0 to ( Count - 1 ) do
     begin
          if Items[ i ].EsValida then
          begin
               Result := True;
               Break;
          end;
     end;
end;

procedure TLKardex.Descargar;
var
   i: Integer;
begin
     { Verifica que si haya checadas de Labor }
     { De lo contrario, se estar�an grabando �nicamente checadas generadas por el Sistema }
     if CheckForValidEntries then
     begin
          { Reduce la posibilidad de choques en la llave primaria }
          Desempata;
          with oZetaProvider do
          begin
               ParamAsInteger( FQChecadasBorra, 'Empleado', Empleado );
               ParamAsDate( FQChecadasBorra, 'Fecha', Fecha );
               Ejecuta( FQChecadasBorra );
               ParamAsInteger( FQChecadasAgrega, 'CB_CODIGO', Empleado );
               for i := 0 to ( Count - 1 ) do
               begin
                    with Items[ i ] do
                    begin
                         if PuedeEscribir then
                         begin
                              ParamAsDate( FQChecadasAgrega, 'AU_FECHA', FechaA );
                              ParamAsChar( FQChecadasAgrega, 'WK_HORA_A', HoraA, K_ANCHO_HH_MM );
                              ParamAsDate( FQChecadasAgrega, 'WK_FECHA_R', FechaR );
                              ParamAsChar( FQChecadasAgrega, 'WK_HORA_R', HoraR, K_ANCHO_HH_MM );
                              ParamAsChar( FQChecadasAgrega, 'WK_HORA_I', HoraI, K_ANCHO_HH_MM );
                              ParamAsVarChar( FQChecadasAgrega, 'OP_NUMBER', Operacion, K_ANCHO_OPERACION );
                              ParamAsVarChar( FQChecadasAgrega, 'WO_NUMBER', Orden, K_ANCHO_ORDEN );
                              {$ifdef HYUNDAI}
                              ParamAsVarChar( FQChecadasAgrega, 'AR_CODIGO', Parte, K_ANCHO_PARTE18 );
                              {$else}
                                     {$ifdef INTERRUPTORES}
                                     ParamAsVarChar( FQChecadasAgrega, 'AR_CODIGO', Parte, K_ANCHO_PARTE20 );
                                     {$else}
                                     ParamAsVarChar( FQChecadasAgrega, 'AR_CODIGO', Parte, K_ANCHO_PARTE );
                                     {$endif}
                              {$endif}
                              ParamAsVarChar( FQChecadasAgrega, 'WK_LINX_ID', TerminalID, K_ANCHO_TERMINAL_ID );
                              ParamAsInteger( FQChecadasAgrega, 'WK_TIPO', Ord( Tipo ) );
                              ParamAsInteger( FQChecadasAgrega, 'WK_STATUS', Ord( Status ) );
                              ParamAsChar( FQChecadasAgrega, 'WK_MOD_1', Modula1, K_ANCHO_MODULADOR );
                              ParamAsChar( FQChecadasAgrega, 'WK_MOD_2', Modula2, K_ANCHO_MODULADOR );
                              ParamAsChar( FQChecadasAgrega, 'WK_MOD_3', Modula3, K_ANCHO_MODULADOR );
                              ParamAsChar( FQChecadasAgrega, 'WK_TMUERTO', TMuerto, K_ANCHO_TMUERTO );
                              ParamAsChar( FQChecadasAgrega, 'CB_AREA', Area, K_ANCHO_AREA );
                              ParamAsChar( FQChecadasAgrega, 'CB_PUESTO', Puesto, K_ANCHO_PUESTO );
                              ParamAsFloat( FQChecadasAgrega, 'WK_PIEZAS', Piezas );
                              ParamAsInteger( FQChecadasAgrega, 'WK_FOLIO', Folio );
                              ParamAsFloat( FQChecadasAgrega, 'WK_PRE_CAL', Tiempo ); { Tiempo capturado de la checada Break o Precalculada }
                              ParamAsFloat( FQChecadasAgrega, 'WK_TIEMPO', GetDuracionDefinitiva ); { Duraci�n calculada de la checada }
                              ParamAsFloat( FQChecadasAgrega, 'WK_HRS_ORD', Ordinarias );
                              ParamAsFloat( FQChecadasAgrega, 'WK_HRS_2EX', Dobles );
                              ParamAsFloat( FQChecadasAgrega, 'WK_HRS_3EX', Triples );
                              ParamAsBoolean( FQChecadasAgrega, 'WK_MANUAL', Manual );
                              ParamAsInteger( FQChecadasAgrega, 'WK_CEDULA', Cedula );
                              Ejecuta( FQChecadasAgrega );
                         end;
                    end;
               end;
          end;
     end;
end;

procedure TLKardex.ProcesarBegin;
begin
     with oZetaProvider do
     begin
          InitGlobales;
          FTipoTMuerto := GetGlobalString( K_GLOBAL_LABOR_TIPO_TMUERTO );
          FExtraSabado:= eHorasExtras( GetGlobalInteger( K_GLOBAL_TRATO_X_SAB ) );
          FExtraDescanso:= eHorasExtras( GetGlobalInteger( K_GLOBAL_TRATO_X_DESC ) );
          {$ifndef CAMBIO_AREA}
          FMaxAreaChange := 1; { 1 Minutos es el tope para poder explicar un cambio de area }
          {$endif}
//          FProcesarExcepciones := True; { Si/No Procesar Excepciones }
          FProcesarExcepciones := not GetGlobalBooleano( K_GLOBAL_LABOR_SIN_EXCEPCIONES ); { Si/No Procesar Excepciones }
          FProcesarExcecionDifAreas := GetGlobalBooleano( K_GLOBAL_LABOR_EXCEPCIONES_AREA );
          FQChecadasLee := CreateQuery( GetSQLScript( Q_WORKS_LEE ) );
          FQChecadasAgrega := CreateQuery( GetSQLScript( Q_WORKS_INSERTA ) );
          FQChecadasBorra := CreateQuery( GetSQLScript( Q_WORKS_BORRA ) );
          FQAsistenciaLee := CreateQuery( GetSQLScript( Q_CHECADAS_LEE ) );
          FQAreasLee := CreateQuery( GetSQLScript( Q_EMPLEADO_AREAS_LISTA ) );
          FQCedulas := CreateQuery( GetSQLScript( Q_CEDULA_LISTA ) );
          FLotesCedula := CreateQuery( GetSQLScript( Q_LOTES_CEDULA ) );
     end;
     Queries.GetDatosHorarioDiarioBegin;
     FBreaks.Init;
     FAreas.Init;
     FDatosCedula := TDatosCedula.Create;
end;

procedure TLKardex.ProcesarEnd;
begin
     Queries.GetDatosHorarioDiarioEnd;
     FreeAndNil( FDatosCedula );
     FreeAndNil( FQCedulas );
     FreeAndNil( FLotesCedula );
     FreeAndNil( FQAreasLee );
     FreeAndNil( FQAsistenciaLee );
     FreeAndNil( FQChecadasBorra );
     FreeAndNil( FQChecadasAgrega );
     FreeAndNil( FQChecadasLee );
end;

procedure TLKardex.Procesar;
begin
     { Inicializa Bandera que indica si ya fueron agregados los breaks }
     FBreakAdded := False;
     { Lee los datos del horario en la tarjeta AUSENCIA }
     GetDatosHorarioDiario;
     { Carga las checadas manuales de WORKS }
     CargarManuales;
     { Carga las checadas de Asistencia }
     CargarAsistencia;
     { Agrega Entrada / Salida Autom�ticas }
     AgregaEntradaSalida;
     { Cargar Areas A Las Cuales Pertenece El Empleado Durante La Jornada ( Depende de AgregaEntradaSalida ) }
     CargarAreas;
     { Agrega los Breaks correspondientes }
     AgregarBreaks;
     { Ajusta la primera checada de producci�n despu�s de una salida y cancela el c�lculo de las siguientes }
     AjustarPosterioresSalida;
     {$ifdef CAMBIO_AREA}
     { Averiguar si hay cambios de Area con tiempo pendiente de explicar para intentar explicarlo mediante una c�dula }
     RevisaCambiosArea;
     {$endif}
     { Establece jerarqu�a de checadas provenientes de c�dulas con listas de empleados, etc.}
     ProcesarExcepciones;
     { Calcula la duraci�n de las checadas }
     //Ordena;
     CalcularTiempos;
     { Calcular Horas de Inicio de las Checadas }
     CalcularInicios;
     { Ajustar por horas extras }
     AjustarExtras;
     { Escribe el kardex a la tabla WORKS }
     Descargar;
end;

function TLKardex.GetDataDump: String;
const
     K_ANCHO_LINEA = 85;
     aSiNo: array[ False..True ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'NO', 'SI' );
var
   i: Integer;
begin
     Result := VACIO;
     Result := Result + Format( 'Area Default: %s', [ GetAreaDefault ] ) + CR_LF;
     Result := Result + Format( 'Checadas: %d', [ Count ] ) + CR_LF;
     if ( Count > 0 ) then
     begin
          Result := Result + StringOfChar( '-', K_ANCHO_LINEA ) + CR_LF;
              Result := Result + '#  HReal HAjus HInit Cedula Folio Manual Duraci�n Area   Clase        ESC CAL EXC' + CR_LF;
          Result := Result + StringOfChar( '-', K_ANCHO_LINEA ) + CR_LF;
          for i := 0 to ( Count - 1 ) do
          begin
               with Items[ i ] do
               begin
                    Result := Result + Format( '%2.2d %s  %s  %s  %6.6d %5.5d %s       %6.2n %s %s %s  %s  %s', [ i + 1, FormatHora( HoraR ), FormatHora( HoraA ), FormatHora( HoraI ), Cedula, Folio, aSiNo[ Manual ], Duracion, Format( '%-6.6s', [ Area ] ), KindText, aSiNo[ Escribir ], aSiNo[ Calculable ], aSiNo[ Excepcion ] ] ) + CR_LF;
               end;
          end;
     end;
end;

{ ************* TLCalculo ************* }

constructor TLCalculo.Create( oCreator: TZetaCreator );
begin
     FCreator := oCreator;
     FKardex := TLKardex.Create( FCreator );
     FLogKardex := False;
end;

destructor TLCalculo.Destroy;
begin
     FKardex.Free;
     inherited;
end;

function TLCalculo.GetRitmos: TRitmos;
begin
     Result := FCreator.Ritmos;
end;

function TLCalculo.oZetaProvider: TdmZetaServerProvider;
begin
     Result := FCreator.oZetaProvider;
end;

function TLCalculo.GetAsistio( Dataset: TZetaCursor ): Boolean;
begin
     with Dataset do
     begin
          Result := ( ( FieldByName( 'AU_HORAS' ).AsFloat + FieldByName( 'AU_EXTRAS' ).AsFloat + FieldByName( 'AU_DES_TRA' ).AsFloat ) > 0 );
     end;
end;

procedure TLCalculo.InsertaTarjetaBegin( const dInicial, dFinal: TDate );
begin
     with oZetaProvider do
     begin
          InitGlobales;
          FTarjetaAgrega := CreateQuery( GetSQLScript( Q_TARJETA_AGREGA ) );
     end;
     with FCreator do
     begin
          PreparaQueries;
          PreparaRitmos;
     end;
     Ritmos.RitmosBegin( dInicial, dFinal );
end;

procedure TLCalculo.InsertaTarjetaEnd;
begin
     FreeAndNil( FTarjetaAgrega );
     Ritmos.RitmosEnd;
end;

procedure TLCalculo.InsertaTarjeta( const iEmpleado: TNumEmp; const dValue: TDate );
begin
     { Obtiene el Horario Correspondiente A La Fecha }
     with GetDatosTurno( iEmpleado, dValue ) do
     begin
          { Agrega La Tarjeta En Ausencia }
          AgregaUnaTarjeta( iEmpleado, dValue, StatusHorario );
     end;
end;

procedure TLCalculo.AgregaTarjetaBegin( const dInicial, dFinal: TDate );
begin
     FEmpleadoBuffer := -1;
     FFechaBuffer := NullDateTime;
     InsertaTarjetaBegin( dInicial, dFinal );
end;

procedure TLCalculo.AgregaTarjetaEnd;
begin
     InsertaTarjetaEnd;
end;

function TLCalculo.GetDataDump: String;
begin
     Result := FKardex.GetDataDump;
end;

function TLCalculo.GetDatosHorario( const iEmpleado: TNumEmp; const dValue: TDate ): TDatosHorarioDiario;
begin
     Result := FCreator.Queries.GetDatosHorarioDiario( GetDatosTurno( iEmpleado, dValue ).StatusHorario.Horario );
end;

function TLCalculo.GetStatusHorario( const sTurno: String; const dReferencia: TDate ): TStatusHorario;
begin
     { Implica haber llamado Ritmos.RitmosBegin() con anterioridad }
     Result := Ritmos.GetStatusHorario( sTurno, dReferencia );
end;

function TLCalculo.CheckFestivo(const sTurno: String; const dFestivo: TDate): Boolean;
begin
     { Supone que ya fu� ejecutado CreaListaFestivos }
     Result := FFestivos.CheckFestivo( sTurno, dFestivo );
end;

function TLCalculo.GetDatosTurno( const iEmpleado: TNumEmp; const dValue: TDate ): TEmpleadoDatosTurno;
begin
     { Implica haber llamado Ritmos.RitmosBegin() con anterioridad }
     Result := Ritmos.GetEmpleadoDatosTurno( iEmpleado, dValue );
end;

procedure TLCalculo.AgregaUnaTarjeta( const iEmpleado: TNumEmp; const dValue: TDate; const StatusHorario: TStatusHorario );
begin
     {$ifdef INTERBASE}
     with FTarjetaAgrega do
     begin
          Active := False;
     end;
     {$endif}
     with oZetaProvider do
     begin
          ParamAsInteger( FTarjetaAgrega, 'Empleado', iEmpleado );
          ParamAsDate( FTarjetaAgrega, 'Fecha', dValue );
          with StatusHorario do
          begin
               ParamAsChar( FTarjetaAgrega, 'Horario', Horario, K_ANCHO_HORARIO );
               ParamAsInteger( FTarjetaAgrega, 'Status', Ord( Status ) );
          end;
     end;
     {$ifdef INTERBASE}
     with FTarjetaAgrega do
     begin
          Active := True;
          while not Eof do
          begin
               Next;
          end;
          Active := False;
     end;
     {$endif}
     {$ifdef MSSQL}
     oZetaProvider.Ejecuta( FTarjetaAgrega );
     {$endif}
end;

procedure TLCalculo.AgregaTarjeta( const iEmpleado: TNumEmp; var dValue: TDate; var sHora: String );
var
   oDatosTurno: TEmpleadoDatosTurno;
begin
     if ( iEmpleado <> FEmpleadoBuffer ) or ( dValue <> FFechaBuffer ) then
     begin
          oDatosTurno := GetDatosTurno( iEmpleado, dValue );
          FEmpleadoBuffer := iEmpleado;
          FFechaBuffer := dValue;
     end;
     if ZetaCommonTools.StrLleno( oDatosTurno.Codigo ) then
     begin
          if ZetaCommonTools.StrLleno( oDatosTurno.StatusHorario.Horario ) then
          begin
               Ritmos.DeterminaDia( iEmpleado, dValue, sHora, oDatosTurno );
               AgregaUnaTarjeta( iEmpleado, dValue, oDatosTurno.StatusHorario );
          end;
     end;
end;

procedure TLCalculo.CalcularTiemposBegin( const dInicial, dFinal: TDate; Buffer: TServerDataset; const lLogKardex: Boolean );
begin
     FLogKardex := lLogKardex;
     with oZetaProvider do
     begin
          FLeePuesto := CreateQuery( GetSQLScript( Q_EMPLEADO_PUESTO ) );
          FTarjeta := CreateQuery( GetSQLScript( Q_TARJETA_LEE ) );
          FStart := CreateQuery( Format( GetSQLScript( Q_TMPLABOR_START ), [ UsuarioActivo ] ) );
          FIndirectos := CreateQuery( Format( GetSQLScript( Q_TMPLABOR_INDIRECTOS ), [ UsuarioActivo, K_GLOBAL_SI ] ) );
          FIndirectosTarjeta := CreateQuery( GetSQLScript( Q_TMPLABOR_TARJETA ) );
          {$ifdef DOS_CAPAS}
          FEmpleados := CreateQueryLocate;
          {$else}
          {$ifdef INTERBASE}
          FEmpleados := CreateQueryLocate;
          {$endif}
          {$ifdef MSSQL}
          FEmpleados := CreateQuery;
          {$endif}
          {$endif}
          PreparaQuery( FEmpleados, Format( GetSQLScript( Q_TMPLABOR_LISTAR ), [ UsuarioActivo ] ) );
          FAplicar := CreateQuery( Format( GetSQLScript( Q_TMPLABOR_APLICAR ), [ K_GLOBAL_SI, UsuarioActivo ] ) );
          FCedulaPiezas := CreateQuery( GetSQLScript( Q_CEDULA_PIEZAS ) );
          FWorksDelete := CreateQuery( GetSQLScript( Q_WORKS_DELETE_SIN_CEDULA ) );
          FWorksDelNoAplica := CreateQuery( Format( GetSQLScript( Q_WORKS_DELETE_NO_APLICADOS ), [ K_GLOBAL_SI, UsuarioActivo ] ) );
          FWorksDelSistema := CreateQuery( Format( GetSQLScript( Q_WORKS_DELETE_SISTEMA ), [ K_GLOBAL_SI, UsuarioActivo ] ) );
          FWorksStdTime := CreateQuery( Format( GetSQLScript( Q_WORKS_STD_TIME ), [ 'PARTES', 'AR_CODIGO', 'AR_STD_HR', 'AR_STD_CST', 'AR_CODIGO' ] ) );
          {$ifdef IMSS_VACA}
          FDiasCotizaSinFV := GetGlobalBooleano( K_GLOBAL_DIAS_COTIZADOS_FV );
          {$endif}
          FFestivoEnDescanso := GetGlobalBooleano( K_GLOBAL_FESTIVO_EN_DESCANSOS );
     end;
     AgregaTarjetaBegin( dInicial, dFinal );
     with FKardex do
     begin
          ProcesarBegin;
     end;
     FFestivos := Ritmos.CreaFestivos( dInicial, dFinal, tfFestivo );
     FBuffer := Buffer;
     GeneraCedulaBegin( dInicial, dFinal );
end;

procedure TLCalculo.CalcularTiemposEnd;
begin
     GeneraCedulaEnd;
     FreeAndNil( FFestivos );
     with FKardex do
     begin
          ProcesarEnd;
     end;
     AgregaTarjetaEnd;
     FreeAndNil( FWorksStdTime );
     FreeAndNil( FWorksDelSistema );
     FreeAndNil( FWorksDelNoAplica );
     FreeAndNil( FWorksDelete );
     FreeAndNil( FCedulaPiezas );
     FreeAndNil( FAplicar );
     FreeAndNil( FEmpleados );
     FreeAndNil( FIndirectosTarjeta );
     FreeAndNil( FIndirectos );
     FreeAndNil( FStart );
     FreeAndNil( FTarjeta );
     FreeAndNil( FLeePuesto );
end;

procedure TLCalculo.CalcularTiemposCargar( const iEmpleado: TNumEmp; const lCheca: Boolean; const sArea: String );
begin
     with FKardex do
     begin
          Clear;
          Empleado := iEmpleado;
          ChecaTarjeta := lCheca;
          SetAreaDefault( sArea );
     end;
end;

procedure TLCalculo.CalcularTiemposEmpleado(const dValue: TDate; const iEmpleado: TNumEmp);
var
   lProcesar: Boolean;
begin
     with oZetaProvider do
     begin
          FLeePuesto.Active := False;
          ParamAsInteger( FLeePuesto, 'Empleado', iEmpleado );
          ParamAsDate( FLeePuesto, 'Fecha', dValue );
          FTarjeta.Active := False;
          ParamAsInteger( FTarjeta, 'Empleado', iEmpleado );
          ParamAsDate( FTarjeta, 'Fecha', dValue );
     end;
     lProcesar := False;
     with FTarjeta do
     begin
          Active := True;
          if not IsEmpty then
          begin
               lProcesar := True;
               with FKardex do
               begin
                    { El empleado asisti� si sus horas ordinarias + extras + descanso_trabajado son > 0 }
                    Asistio := GetAsistio( FTarjeta );
                    Horario := FieldByName( 'HO_CODIGO' ).AsString;
                    Status := eStatusAusencia( FieldByName( 'AU_STATUS' ).AsInteger );
                    with DatosTarjeta do
                    begin
                         Ordinarias := FieldByName( 'AU_HORAS' ).AsFloat; { GA: Antes era AUSENCIA.AU_HORASCK }
                         Extras := FieldByName( 'AU_EXTRAS' ).AsFloat;
                         Dobles := FieldByName(  'AU_DOBLES' ).AsFloat;
                         Triples := FieldByName(  'AU_TRIPLES' ).AsFloat;
                    end;
               end;
          end;
          Active := False;
     end;
     if lProcesar then
     begin
          with FKardex do
          begin
               Empleado := iEmpleado;
               Fecha := dValue;
               { Lee el puesto del empleado a la fecha seg�n su Kardex de R.H.}
               with FLeePuesto do
               begin
                    Active := True;
                    if IsEmpty then
                    begin
                         Puesto := '';
                    end
                    else
                    begin
                         Puesto := FieldByName( 'CB_PUESTO' ).AsString;
                    end;
                    Active := False;
               end;
               Procesar;
          end;
     end;
end;

procedure TLCalculo.CalcularTiemposEmpleado(const dValue: TDate);
begin
     CalcularTiemposEmpleado( dValue, FKardex.Empleado );
end;

procedure TLCalculo.CalcularTiempos( const dValue: TDate );
var
   lOk, lAplica, lFestivo, lHayTarjeta: Boolean;
   iEmpleado, iCedula: Integer;
   sArea, sTurno: String;
   eDia: eStatusAusencia;
   eStatus: eStatusEmpleado;
   DatosHorario: TStatusHorario;
begin
     lOk := True;
     with oZetaProvider do
     begin
          { Inicializar tabla TMPLABOR con los valores del d�a para todos los empleados }
          EmpiezaTransaccion;
          try
             ParamAsDate( FStart, 'Fecha', dValue );
             Ejecuta( FStart );
             TerminaTransaccion( True );
          except
                on Error: Exception do
                begin
                     TerminaTransaccion( False );
                     Log.Excepcion( 0, Format( 'Error Al Inicializar Lista De Empleados Del %s', [ ZetaCommonTools.FechaCorta( dValue ) ] ), Error );
                     lOk := False;
                end;
          end;
          if lOk then
          begin
               { Revisar Lista de Empleados Para Una Fecha y Determinar Quienes Son Sujetos a Labor }
               EmpiezaTransaccion;
               try
                  with FIndirectos do
                  begin
                       Active := True;
                       while not Eof do
                       begin
                            iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                            sTurno := FieldByName( 'CB_TURNO' ).AsString;
                            eStatus := eStatusEmpleado( FieldByName( 'TL_STATUS' ).AsInteger );
                            ParamAsInteger( FIndirectosTarjeta, 'Empleado', iEmpleado );
                            ParamAsDate( FIndirectosTarjeta, 'Fecha', dValue );
                            with FIndirectosTarjeta do
                            begin
                                 Active := True;
                                 if IsEmpty then
                                 begin
                                      lHayTarjeta := False;
                                      eDia := eStatusAusencia( 0 );
                                 end
                                 else
                                 begin
                                      lHayTarjeta := True;
                                      eDia := eStatusAusencia( FieldByName( 'AU_STATUS' ).AsInteger );
                                 end;
                                 Active := False;
                            end;
                            if not lHayTarjeta then
                            begin
                                 DatosHorario := GetStatusHorario( sTurno, dValue );
                                 eDia := DatosHorario.Status;
                            end;
                            lFestivo := CheckFestivo( sTurno, dValue );
                            case ZetaServerTools.GetTipoDia( eStatus, lFestivo, FFestivoEnDescanso{$ifdef IMSS_VACA}, FDiasCotizaSinFV{$endif}, eDia ) of
                                 daNormal: lAplica := True;
                            else
                                lAplica := False;
                            end;
                            if lAplica then { Son TL_APLICA = 'N' por Default en SP_TMPLABOR_START }
                            begin
                                 try
                                    if not lHayTarjeta then
                                    begin
                                         { Asegurar que todos los TL_APLICA = 'S' tienen su AUSENCIA para el d�a }
                                         AgregaUnaTarjeta( iEmpleado, dValue, DatosHorario );
                                    end;
                                    ParamAsInteger( FAplicar, 'Empleado', iEmpleado );
                                    Ejecuta( FAplicar );
                                 except
                                       on Error: Exception do
                                       begin
                                            Log.Excepcion( iEmpleado, Format( 'Error Al Determinar Sujetos De Labor En %s', [ ZetaCommonTools.FechaCorta( dValue ) ] ) , Error );
                                       end;
                                 end;
                            end;
                            Next;
                       end;
                       Active := False;
                  end;
               finally
                      TerminaTransaccion( True );
               end;
               { Borrar los WORKS de C�dulas que ya no existen }
               { Esto es algo que un trigger pudiera resolver al borrar una c�dula }
               EmpiezaTransaccion;
               try
                  ParamAsDate( FWorksDelete, 'Fecha', dValue );
                  Ejecuta( FWorksDelete );
                  TerminaTransaccion( True );
               except
                     on Error: Exception do
                     begin
                          TerminaTransaccion( False );
                          Log.Excepcion( 0, Format( 'Error Al Borrar Operaciones De C�dulas Inexistentes Del %s', [ ZetaCommonTools.FechaCorta( dValue ) ] ), Error );
                     end;
               end;
               { Borrar WORKS No Manuales y WORKS de c�dulas para empleados a quienes SI se les aplica LABOR }
               EmpiezaTransaccion;
               try
                  ParamAsDate( FWorksDelSistema, 'Fecha', dValue );
                  Ejecuta( FWorksDelSistema );
                  TerminaTransaccion( True );
               except
                     on Error: Exception do
                     begin
                          TerminaTransaccion( False );
                          Log.Excepcion( 0, Format( 'Error Al Borrar Operaciones No Manuales Al %s', [ ZetaCommonTools.FechaCorta( dValue ) ] ), Error );
                     end;
               end;
               { Generar WORKS a partir de las c�dulas registradas }
               { 2 * ( select COUNT(*) from CEDULA ) pasos }
               with FEmployeeList do
               begin
                    Init( dValue );
               end;
               with FBuffer do
               begin
                    InitTempDataset;
                    Data := OpenSQL( EmpresaActiva, Format( GetSQLScript( Q_CEDULA_1_DIA ), [ Ord( tlNocturnoEntrada ),
                                                                                              Ord( tlNocturnoSalida ),
                                                                                              ZetaCommonTools.DateToStrSQL( dValue ),
                                                                                              ZetaCommonTools.DateToStrSQL( dValue + 1 ),
                                                                                              ZetaCommonTools.DateToStrSQL( dValue - 1 ) ] ), True );
               end;
               with FBuffer do
               begin
                    First;
                    while not Eof and CanContinue do
                    begin
                         iCedula := FieldByName( 'CE_FOLIO' ).AsInteger;
                         try
                            GeneraCedulaCargar;
                            GeneraCedula;
                         except
                               on Error: Exception do
                               begin
                                    Log.Excepcion( 0, Format( 'Error Al Generar Empleados De C�dula %d', [ iCedula ] ), Error );
                               end;
                         end;
                         Next;
                    end;
               end;
               { Calcular Tiempos a los empleados a quienes si se les aplica LABOR }
               { n pasos }
               with FEmpleados do
               begin
                    Active := True;
                    while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                    begin
                         iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                         { Este filtro pudiera estar en el query }
                         { Pero es mejor tenerlo aqu� porque el CanContinue barre todo }
                         { la tabla TMPLABOR }
                         sArea := FieldByName( 'CB_AREA' ).AsString;
                         if SujetoLabor( sArea, FieldByName( 'TL_APLICA' ).AsString, FieldByName( 'TL_CALCULA' ).AsString ) then
                         begin
                              EmpiezaTransaccion;
                              try
                                 CalcularTiemposCargar( FieldByName( 'CB_CODIGO' ).AsInteger,
                                                        ZetaCommonTools.zStrToBool( FieldByName( 'CB_CHECA' ).AsString ),
                                                        sArea );
                                 CalcularTiemposEmpleado( dValue );
                                 TerminaTransaccion( True );
                                 if FLogKardex then
                                    Log.Evento( clbNinguno, iEmpleado, Date, Format( 'C�lculo de Tiempos En %s', [ ZetaCommonTools.FechaCorta( dValue ) ] ), GetDataDump );
                              except
                                    on Error: Exception do
                                    begin
                                         RollbackTransaccion;
                                         Log.Excepcion( iEmpleado, Format( 'Error Al Calcular Tiempos En %s', [ ZetaCommonTools.FechaCorta( dValue ) ] ), Error, GetDataDump );
                                    end;
                              end;
                         end;
                         Next;
                    end;
                    Active := False;
               end;
               { Borrar WORKS para empleados a quienes no se les aplica LABOR }
               EmpiezaTransaccion;
               try
                  ParamAsDate( FWorksDelNoAplica, 'Fecha', dValue );
                  Ejecuta( FWorksDelNoAplica );
                  TerminaTransaccion( True );
               except
                     on Error: Exception do
                     begin
                          TerminaTransaccion( False );
                          Log.Excepcion( 0, Format( 'Error Al Borrar Empleados No Aplicables Al %s', [ ZetaCommonTools.FechaCorta( dValue ) ] ), Error );
                     end;
               end;
               { Proratear Piezas a partir de las c�dulas }
               with FBuffer do
               begin
                    First;
                    while not Eof and CanContinue do
                    begin
                         iCedula := FieldByName( 'CE_FOLIO' ).AsInteger;
                         EmpiezaTransaccion;
                         try
                            ParamAsInteger( FCedulaPiezas, 'Cedula', iCedula );
                            Ejecuta( FCedulaPiezas );
                            TerminaTransaccion( True );
                         except
                               on Error: Exception do
                               begin
                                    RollbackTransaccion;
                                    Log.Excepcion( 0, Format( 'Error Al Proratear Piezas De C�dula %d', [ iCedula ] ), Error );
                               end;
                         end;
                         Next;
                    end;
                    Active := False;
               end;
               { Asigna tiempos est�ndard a los WORKS generados para la fecha }
               EmpiezaTransaccion;
               try
                  ParamAsDate( FWorksStdTime, 'Fecha', dValue );
                  Ejecuta( FWorksStdTime );
                  TerminaTransaccion( True );
               except
                     on Error: Exception do
                     begin
                          TerminaTransaccion( False );
                          Log.Excepcion( 0, Format( 'Error Al Asignar Tiempos Est�ndard Al %s', [ ZetaCommonTools.FechaCorta( dValue ) ] ), Error );
                     end;
               end;
          end;
     end;
end;

function TLCalculo.ProcesarPendientes: OleVariant;
var
   FLecturas, FBorra, FEscribe, FLeeDatosEmp: TZetaCursor;
   dInicial, dFinal, dFecha, dAjuste: TDate;
   sHora, sAjuste, sOrden, sOperacion, sTerminalID, sTMuerto: String;
   sModulador1, sModulador2, sModulador3: String;
   sPuesto, sArea : String;
   iEmpleado, iStatus: TNumEmp;
   iFolio, iLecturas: Integer;
   rPiezas: TDiasHoras;
begin
     with oZetaProvider do
     begin
          FLecturas := CreateQuery( GetSQLScript( Q_LECTURAS_PERIODO ) );
     end;
     try
        with FLecturas do
        begin
             Active := True;
             if IsEmpty then
             begin
                  iLecturas := 0;
                  dInicial := NullDateTime;
                  dFinal := NullDateTime;
             end
             else
             begin
                  dInicial := Fields[ 0 ].AsDateTime;
                  dFinal := Fields[ 1 ].AsDateTime;
                  iLecturas := Fields[ 2 ].AsInteger;
             end;
             Active := False;
        end;
     finally
            FreeAndNil( FLecturas );
     end;
     if ( iLecturas > 0 ) and oZetaProvider.OpenProcess( prLabLecturas, iLecturas ) then
     begin
          AgregaTarjetaBegin( dInicial, dFinal );
          try
             with oZetaProvider do
             begin
                  FLecturas := CreateQuery( GetSQLScript( Q_LECTURAS_LEE ) );
                  FBorra := CreateQuery( GetSQLScript( Q_LECTURAS_BORRA ) );
                  FEscribe := CreateQuery( GetSQLScript( Q_WORKS_AGREGA ) );
                  FLeeDatosEmp := CreateQuery( GetSQLScript( Q_EMPLEADO_AREA ) );
                  ParamAsBoolean( FEscribe, 'WK_MANUAL', True );
             end;
             try
                with FLecturas do
                begin
                     Active := True;
                     while not Eof do
                     begin
                          iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                          dFecha := FieldByName( 'LX_FECHA' ).AsDateTime;
                          sHora := FieldByName( 'LX_HORA' ).AsString;
                          sOrden := FieldByName( 'LX_WORDER' ).AsString;
                          sOperacion := FieldByName( 'LX_OPERA' ).AsString;
                          sTerminalID := FieldByName( 'LX_LINX_ID' ).AsString;
                          iStatus := FieldByName( 'LX_STATUS' ).AsInteger;
                          iFolio := FieldByName( 'LX_FOLIO' ).AsInteger;
                          sModulador1 := FieldByName( 'LX_MODULA1' ).AsString;
                          sModulador2 := FieldByName( 'LX_MODULA2' ).AsString;
                          sModulador3 := FieldByName( 'LX_MODULA3' ).AsString;
                          sTMuerto := FieldByName( 'LX_TMUERTO' ).AsString;
                          rPiezas := FieldByName( 'LX_PIEZAS' ).AsFloat;
                          dAjuste := dFecha;
                          sAjuste := sHora;
                          // Leer Datos de Colabora
                             if ( not FLeeDatosEmp.Active ) or ( FLeeDatosEmp.IsEmpty ) or
                                ( FLeeDatosEmp.FieldByName( 'CB_CODIGO' ).AsInteger <> iEmpleado ) then
                             begin
                                  FLeeDatosEmp.Active := FALSE;
                                  oZetaProvider.ParamAsInteger( FLeeDatosEmp, 'Empleado', iEmpleado );
                                  FLeeDatosEmp.Active := TRUE;
                             end;
                             if not FLeeDatosEmp.IsEmpty then
                             begin
                                  sArea   := FLeeDatosEmp.FieldByName( 'CB_AREA' ).AsString;
                                  sPuesto := FLeeDatosEmp.FieldByName( 'CB_PUESTO' ).AsString;
                             end;
                          with oZetaProvider do
                          begin
                               EmpiezaTransaccion;
                               try
                                  AgregaTarjeta( iEmpleado, dAjuste, sAjuste );
                                  ParamAsInteger( FEscribe, 'CB_CODIGO', iEmpleado );
                                  ParamAsVarChar( FEscribe, 'OP_NUMBER', sOperacion, K_ANCHO_OPERACION );
                                  ParamAsVarChar( FEscribe, 'WO_NUMBER', sOrden, K_ANCHO_ORDEN );
                                  ParamAsDate( FEscribe, 'AU_FECHA', dAjuste );
                                  ParamAsChar( FEscribe, 'WK_HORA_A', sAjuste, K_ANCHO_HH_MM );
                                  ParamAsDate( FEscribe, 'WK_FECHA_R', dFecha );
                                  ParamAsChar( FEscribe, 'WK_HORA_R', sAjuste, K_ANCHO_HH_MM );
                                  ParamAsVarChar( FEscribe, 'WK_LINX_ID', sTerminalID, K_ANCHO_TERMINAL_ID );
                                  ParamAsInteger( FEscribe, 'WK_TIPO', Ord( wtChecada ) );
                                  ParamAsInteger( FEscribe, 'WK_STATUS', iStatus );
                                  ParamAsInteger( FEscribe, 'WK_FOLIO', iFolio );
                                  ParamAsVarChar( FEscribe, 'WK_MOD_1', sModulador1, K_ANCHO_MODULADOR );
                                  ParamAsVarChar( FEscribe, 'WK_MOD_2', sModulador2, K_ANCHO_MODULADOR );
                                  ParamAsVarChar( FEscribe, 'WK_MOD_3', sModulador3, K_ANCHO_MODULADOR );
                                  ParamAsVarChar( FEscribe, 'WK_TMUERTO', sTMuerto, K_ANCHO_TMUERTO );
                                  ParamAsFloat( FEscribe, 'WK_PIEZAS', rPiezas );
                                  ParamAsChar( FEscribe, 'CB_AREA', sArea, K_ANCHO_AREA );
                                  ParamAsChar( FEscribe, 'CB_PUESTO', sPuesto, K_ANCHO_PUESTO );
                                  Ejecuta( FEscribe );
                                  ParamAsInteger( FBorra, 'Empleado', iEmpleado );
                                  ParamAsDate( FBorra, 'Fecha', dFecha );
                                  ParamAsVarChar( FBorra, 'Hora', sHora, K_ANCHO_HH_MM );
                                  ParamAsVarChar( FBorra, 'Orden', sOrden, K_ANCHO_ORDEN );
                                  ParamAsVarChar( FBorra, 'Operacion', sOperacion, K_ANCHO_OPERACION );
                                  Ejecuta( FBorra );
                                  TerminaTransaccion( True );
                               except
                                     on Error: Exception do
                                     begin
                                          TerminaTransaccion( False );
                                          Log.Excepcion( iEmpleado, 'Error Al Grabar Checada Del ' + FechaCorta( dAjuste ) + ' Hora: ' + sHora, Error );
                                     end;
                               end;
                          end;
                          Next;
                     end;
                     Active := False;
                end;
             finally
                    FreeAndNil( FLeeDatosEmp );
                    FreeAndNil( FEscribe );
                    FreeAndNil( FBorra );
                    FreeAndNil( FLecturas );
             end;
          finally
                 AgregaTarjetaEnd;
          end;
          Result := oZetaProvider.CloseProcess;
     end;
end;

{ **** Generar WORKS de las C�dulas **** }

function TLCalculo.CuantasCedulas( const dInicial, dFinal: TDateTime ): Integer;
var
   FQuery: TZetaCursor;
begin
     with oZetaProvider do
     begin
          FQuery := CreateQuery( Format( GetSQLScript( Q_CEDULA_CUENTA ), [ Ord( tlNocturnoEntrada ),
                                                                            Ord( tlNocturnoSalida ),
                                                                            ZetaCommonTools.DateToStrSQL( dInicial ),
                                                                            ZetaCommonTools.DateToStrSQL( dFinal ),
                                                                            ZetaCommonTools.DateToStrSQL( dFinal + 1 ),
                                                                            ZetaCommonTools.DateToStrSQL( dInicial - 1 ) ] ) );
          try
             with FQuery do
             begin
                  Active := True;
                  Result := Fields[ 0 ].AsInteger;
                  Active := False;
             end;
          finally
                 FreeAndNil( FQuery );
          end;
     end;
end;

procedure TLCalculo.GeneraCedulaBegin( const dInicial, dFinal: TDateTime );
begin
     FCedula := TDatosCedula.Create;
     with oZetaProvider do
     begin
          FCedulaEmpleados := CreateQuery( Format( GetSQLScript( Q_CEDULA_LISTA_EMPLEADOS ), [ UsuarioActivo, K_GLOBAL_SI ] ) );
          FCedulaDelWorks := CreateQuery( GetSQLScript( Q_CEDULA_WORKS_DELETE ) );
          FCedulaInsWorks := CreateQuery( GetSQLScript( Q_CEDULA_WORKS_INSERT ) );
          FCedulaSetEmp := CreateQuery( Format( GetSQLScript( Q_CEDULA_SET_EMPLEADOS ), [ K_GLOBAL_SI, UsuarioActivo ] ) );
          FLotesCedula := CreateQuery( GetSQLScript( Q_LOTES_CEDULA ) );
          FExisteWorks := CreateQuery( Format ( GetSQLScript( Q_EXISTE_WORKS ),[ K_FOLIO_CEDULA_CAPTURADO, K_FOLIO_CEDULA_PRECALC_EXC, K_FOLIO_TIEMPO_MUERTO_EXC, K_OFFSET_MULTI_EXC ] ) );
     end;
     FEmployeeList := TEmployeeList.Create( oZetaProvider );
end;

procedure TLCalculo.GeneraCedulaEnd;
begin
     FreeAndNil( FEmployeeList );
     FreeAndNil( FCedulaSetEmp );
     FreeAndNil( FCedulaInsWorks );
     FreeAndNil( FCedulaDelWorks );
     FreeAndNil( FCedulaEmpleados );
     FreeAndNil( FLotesCedula );
     FreeAndNil( FExisteWorks );
     FreeAndNil( FCedula );
end;

procedure TLCalculo.GeneraCedulaCargar;
const
     aFolio: array[ False..True ] of Integer = ( K_FOLIO_CEDULA, K_FOLIO_CEDULA_CAPTURADO );
     aFolioPrecalculada: array[ False..True ] of Integer = ( K_FOLIO_CEDULA_PRECALC , K_FOLIO_CEDULA_PRECALC_EXC );
     aFolioTiempoMuerto: array[ False..True ] of Integer = ( K_FOLIO_TIEMPO_MUERTO  , K_FOLIO_TIEMPO_MUERTO_EXC );

function GetFolioExcepcion:Integer;
begin
     Result:= K_FOLIO_CEDULA;
     with FCedula do
     begin
          if Tipo = tcOperacion then
          begin
                Result :=  aFolio[ HayEmpleados ]
          end
          else if Tipo = tcEspeciales then
          begin
                Result :=  aFolioPrecalculada[ HayEmpleados ]
          end
          else if Tipo = tcTMuerto then
          begin
               Result :=  aFolioTiempoMuerto [ HayEmpleados ];
          end;
     end;
end;

begin
     with FCedula do
     begin
          with FBuffer do
          begin
               Folio := FieldByName( 'CE_FOLIO' ).AsInteger;
               Orden := FieldByName( 'WO_NUMBER' ).AsString;
               Parte := FieldByName( 'AR_CODIGO' ).AsString;
               Operacion := FieldByName( 'OP_NUMBER' ).AsString;
               Area := FieldByName( 'CE_AREA' ).AsString;
               Tipo := eTipoCedula( FieldByName( 'CE_TIPO' ).AsInteger );
               Status := eStatusLectura( FieldByName( 'CE_STATUS' ).AsInteger );
               Modula1 := FieldByName( 'CE_MOD_1' ).AsString;
               Modula2 := FieldByName( 'CE_MOD_2' ).AsString;
               Modula3 := FieldByName( 'CE_MOD_3' ).AsString;
               TiempoMuerto := FieldByName( 'CE_TMUERTO' ).AsString;
               MultiLote := ZetaCommonTools.zStrToBool( FieldByName( 'CE_MULTI' ).AsString );
               HayEmpleados := ( FieldByName( 'CE_EMPS' ).AsInteger > 0 );
               Usuario := FieldByName( 'US_CODIGO' ).AsInteger;
               Tiempo := FieldByName( 'CE_TIEMPO' ).AsFloat;
               Fecha := FieldByName( 'CE_FECHA' ).AsDateTime;
               Hora := FieldByName( 'CE_HORA' ).AsString;
               AjustarFechaHoraWorks( eTurnoLabor( FieldByName( 'AR_SHIFT' ).AsInteger ), FieldByName( 'AR_PRI_HOR' ).AsString );
               Piezas := FieldByName( 'CE_PIEZAS' ).AsFloat;
          end;
          with oZetaProvider do
          begin
               ParamAsInteger( FCedulaInsWorks, 'WK_CEDULA', Folio );
               ParamAsDate( FCedulaInsWorks, 'AU_FECHA', FechaWorks );
               ParamAsChar( FCedulaInsWorks, 'WK_HORA_A', HoraWorks, K_ANCHO_HH_MM );
               ParamAsDate( FCedulaInsWorks, 'WK_FECHA_R', Fecha );
               ParamAsChar( FCedulaInsWorks, 'WK_HORA_R', Hora, K_ANCHO_HH_MM );
               ParamAsInteger( FCedulaInsWorks, 'WK_FOLIO', GetFolioExcepcion );
               ParamAsVarChar( FCedulaInsWorks, 'OP_NUMBER', Operacion, K_ANCHO_OPERACION );
               ParamAsVarChar( FCedulaInsWorks, 'WO_NUMBER', Orden, K_ANCHO_ORDEN );
               {$ifdef HYUNDAI}
               ParamAsVarChar( FCedulaInsWorks, 'AR_CODIGO', Parte, K_ANCHO_PARTE18 );
               {$else}
                      {$ifdef INTERRUPTORES}
                      ParamAsVarChar( FCedulaInsWorks, 'AR_CODIGO', Parte, K_ANCHO_PARTE20 );
                      {$else}
                      ParamAsVarChar( FCedulaInsWorks, 'AR_CODIGO', Parte, K_ANCHO_PARTE );
                      {$endif}
               {$endif}
               ParamAsVarChar( FCedulaInsWorks, 'WK_LINX_ID', K_LINX_ID_CEDULA, K_ANCHO_TERMINAL_ID );
               ParamAsInteger( FCedulaInsWorks, 'WK_TIPO', Ord( Checada ) );
               ParamAsInteger( FCedulaInsWorks, 'WK_STATUS', Ord( Status ) );
               ParamAsChar( FCedulaInsWorks, 'WK_MOD_1', Modula1, K_ANCHO_MODULADOR );
               ParamAsChar( FCedulaInsWorks, 'WK_MOD_2', Modula2, K_ANCHO_MODULADOR );
               ParamAsChar( FCedulaInsWorks, 'WK_MOD_3', Modula3, K_ANCHO_MODULADOR );
               ParamAsChar( FCedulaInsWorks, 'WK_TMUERTO', TiempoMuerto, K_ANCHO_TMUERTO );
               ParamAsChar( FCedulaInsWorks, 'CB_AREA', Area, K_ANCHO_AREA );
               ParamAsChar( FCedulaInsWorks, 'CB_PUESTO', VACIO, K_ANCHO_PUESTO );
               ParamAsBoolean( FCedulaInsWorks, 'WK_MANUAL', False );
               ParamAsFloat( FCedulaInsWorks, 'WK_PIEZAS', Piezas );
               ParamAsFloat( FCedulaInsWorks, 'WK_PRE_CAL', Tiempo ); { Tiempo capturado de la checada cuando es precalculada }
               //Parametros para ver si existe Wokrs de Multilote
               ParamAsDate( FExisteWorks, 'AU_FECHA', FechaWorks );
               ParamAsChar( FExisteWorks, 'WK_HORA_A', HoraWorks, K_ANCHO_HH_MM );
               ParamAsInteger( FExisteWorks, 'WK_CEDULA', Folio );
               ParamAsInteger( FExisteWorks, 'WK_TIPO', Ord( Checada ) );
               if MultiLote then
               begin
                    try
                       with FLotesCedula do
                       begin
                            {Buscar las ordenes de multilote}
                            Active := False;
                            ParamAsInteger( FLotesCedula, 'CE_FOLIO', Folio );
                            Active := True;
                            First;
                            FLotes := TList.Create;
                            while( not Eof )do
                            begin
                                 {Agregamos los multilotes}
                                 with AddOrden do
                                 begin
                                      Codigo := FieldByName('WO_NUMBER').AsString;
                                      Piezas := FieldByName('CW_PIEZAS').AsFloat;
                                      Producto := FieldByName('AR_CODIGO').AsString;
                                      Posicion := FieldByName('CW_POSICIO').AsInteger;
                                 end;
                                 Next;
                            end;
                       end;
                    except
                          on Error: Exception do
                          begin
                               Log.Excepcion( 0, Format( 'Error Agregar Ordenes del MultiLote de la C�dula: %d', [ Folio ] ), Error );
                               //TerminaTransaccion( True );
                          end;
                    end;
               end;
          end;
     end;
end;

procedure TLCalculo.GeneraCedula;
var
   i, iEmpleado: Integer;
begin
     with oZetaProvider do
     begin
          { Primero hay que borrar todos los WORKS asignados a la c�dula }
          EmpiezaTransaccion;
          with FCedula do
          begin
               try
                  ParamAsInteger( FCedulaDelWorks, 'Cedula', Folio  );
                  ParamAsDateTime( FCedulaDelWorks, 'Fecha', Fecha );
                  Ejecuta( FCedulaDelWorks );
                  TerminaTransaccion( True );
               except
                     on Error: Exception do
                     begin
                          Log.Excepcion( 0, Format( 'Error Al Borrar Datos Previos De C�dula %d', [ Folio ] ), Error );
                          TerminaTransaccion( True );
                     end;
               end;
          end;
          { Luego hay que generar los WORKS de la c�dula }
          EmpiezaTransaccion;
          try
             with FCedula do
             begin
                  if HayEmpleados then
                  begin
                       ParamAsInteger( FCedulaEmpleados, 'Cedula', FCedula.Folio );
                       with FCedulaEmpleados do
                       begin
                            Active := True;
                            while not Eof do
                            begin
                                 iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                                 GeneraCedulaEmpleado( iEmpleado );
                                 Next;
                            end;
                            Active := False;
                       end;
                  end
                  else
                  begin
                       with FEmployeeList do
                       begin
                            for i := 0 to ( Count - 1 ) do
                            begin
                                 with Employees[ i ] do
                                 begin
                                      if IsInArea( Area, FechaReal, HoraReal ) then
                                      begin
                                           GeneraCedulaEmpleado( Numero );
                                      end;
                                 end;
                            end;
                       end;
                  end;
             end;
             TerminaTransaccion( True );
          except
                on Error: Exception do
                begin
                     Log.Excepcion( 0, Format( 'Error Al Agregar Empleados A C�dula %d', [ FCedula.Folio ] ), Error );
                     TerminaTransaccion( True );
                end;
          end;
          { Al final hay que marcar con TMPLABOR.TL_CALCULA = 'S' a todos }
          { los empleados de la c�dula que tengan TMPLABOR.TL_CALCULA = 'N' }
          EmpiezaTransaccion;
          with FCedula do
          begin
               try
                  ParamAsInteger( FCedulaSetEmp, 'Cedula', Folio );
                  Ejecuta( FCedulaSetEmp );
                  TerminaTransaccion( True );
               except
                     on Error: Exception do
                     begin
                          Log.Excepcion( 0, Format( 'Error Al Marcar Empleados De C�dula %d', [ Folio ] ), Error );
                          TerminaTransaccion( True );
                     end;
               end;
          end;
     end;
end;

procedure TLCalculo.GeneraCedulaEmpleado( const iEmpleado: TNumEmp );
var i,OffSet:Integer;
function ChecaWorksIguales : Boolean;
begin
     with oZetaProvider do
     begin
          ParamAsString( FExisteWorks, 'EXCEPCION', zBoolToStr(FCedula.HayEmpleados) );
          ParamAsString( FExisteWorks, 'EXCEPCION2', zBoolToStr(FCedula.HayEmpleados) );
     end;
     with FExisteWorks do
     begin
          Active := True;
          Result := IsEmpty;
          Active := False;
     end;
end;

begin
     with FCedula do
     begin
          OffSet := 0;
          if Tipo = tcOperacion then
               OffSet := K_OFFSET_PRODUCCION
          else
              if Tipo = tcEspeciales then
                   OffSet := K_OFFSET_ESPECIALES
              else
                  if Tipo = tcTMuerto then
                       OffSet := K_OFFSET_TIE_MUERTO;


          if HayEmpleados then
             OffSet := OffSet + K_OFFSET_MULTI_EXC;
          with oZetaProvider do
          begin
               try
                  ParamAsInteger( FCedulaInsWorks, 'CB_CODIGO', iEmpleado );
                  ParamAsInteger( FExisteWorks, 'CB_CODIGO', iEmpleado );
                  if MultiLote then
                  begin
                        {Por cada orden en el MultiLote se agrega un registro a WORKS para leerla como una checada al Calcular tiempos }
                       for i := 0 to FLotes.Count - 1 do
                       begin
                            ParamAsInteger( FCedulaInsWorks, 'WK_FOLIO', Lotes[i].Posicion + OffSet );
                            ParamAsVarChar( FCedulaInsWorks, 'WO_NUMBER', Lotes[i].Codigo , K_ANCHO_ORDEN );
                            {$ifdef HYUNDAI}
                            ParamAsVarChar( FCedulaInsWorks, 'AR_CODIGO', Lotes[i].Producto , K_ANCHO_PARTE18 );
                            {$else}
                                   {$ifdef INTERRUPTORES}
                                   ParamAsVarChar( FCedulaInsWorks, 'AR_CODIGO', Lotes[i].Producto , K_ANCHO_PARTE20 );
                                   {$else}
                                   ParamAsVarChar( FCedulaInsWorks, 'AR_CODIGO', Lotes[i].Producto , K_ANCHO_PARTE );
                                   {$endif}
                            {$endif}
                            ParamAsFloat( FCedulaInsWorks, 'WK_PIEZAS', Lotes[i].Piezas );
                            Ejecuta( FCedulaInsWorks );
                            //if not ChecaWorksIguales then
                            //   Log.Error( iEmpleado, Format( 'Error En C�dula %d Para Empleado %d', [ Folio, iEmpleado ] ),'� C�digo � Registro Ya Existe !');
                       end
                  end
                  else
                  begin
                       Ejecuta( FCedulaInsWorks );
                       //if not ChecaWorksIguales then
                       //    Log.Error( iEmpleado, Format( 'Error En C�dula %d Para Empleado %d', [ Folio, iEmpleado ] ),'� C�digo � Registro Ya Existe !');
                  end;
               except
                     on Error: Exception do
                     begin
                          Log.Excepcion( iEmpleado, Format( 'Error En C�dula %d Para Empleado %d', [ Folio, iEmpleado ] ), Error );
                     end;
               end;
          end;
     end;
end;

{ **** Registrar Breaks Manuales **** }

procedure TLCalculo.RegistrarBreakManualBegin( const iUsuario: Integer; const dInicial, dFinal: TDateTime );
var
   lTieneAreas: Boolean;
begin
     with oZetaProvider do
     begin
          FBreakTarjeta := CreateQuery( GetSQLScript( Q_BREAK_HAY_TARJETA ) );
          FBreakAgrega := CreateQuery( GetSQLScript( Q_BREAK_AGREGA ) );
          FBreakArea := CreateQuery( Format( GetSQLScript( Q_BREAK_TIENE_AREAS ), [ iUsuario ] ) );
          try
             with FBreakArea do
             begin
                  Active := True;
                  lTieneAreas := ( FieldByName('CUANTOS').AsInteger > 0 );
                  Active := False;
             end;
          finally
                 FreeAndNil( FBreakArea );
          end;
          if lTieneAreas then
             FBreakArea := CreateQuery( Format( GetSQLScript( Q_BREAK_AREA ), [ iUsuario ] ) );
     end;
     InsertaTarjetaBegin( dInicial, dFinal );
end;

procedure TLCalculo.RegistrarBreakManualEnd;
begin
     InsertaTarjetaEnd;
     FreeAndNil( FBreakArea );
     FreeAndNil( FBreakAgrega );
     FreeAndNil( FBreakTarjeta );
end;

function TLCalculo.RegistrarBreak( const iEmpleado: TNumEmp; const iDuracion: Integer; dFecha: TDate; sHora: THoraLabor; var sTexto: String ): Boolean;
var
   lHayTarjeta: Boolean;

   { ********** Pendiente de Pasar esta funci�n a ZCommonTools *********** }
             { Esta siendo usada por DServerLabor y DServerSuper}
   {
   function FormateaHora( const sHora: String ): String;
   begin
        if ( sHora <> Space( 4 ) ) then
           Result := Copy( sHora, 1, 2 ) + ':' + Copy( sHora, 3, 2 );
   end;
   }

begin
     with oZetaProvider do
     begin
          if Assigned( FBreakArea ) then
          begin
               ParamAsInteger( FBreakArea, 'Empleado', iEmpleado );
               ParamAsDate( FBreakArea, 'Fecha', dFecha );
               ParamAsChar( FBreakArea, 'Hora', sHora, K_ANCHO_HH_MM );
               with FBreakArea do
               begin
                    Active := True;
                    if IsEmpty then
                    begin
                         Result := False;
                         sTexto := Format( 'Empleado %d No Asignado A Las %s De %s', [ iEmpleado, FormateaHora( sHora ), FormatDateTime( 'dd/mmm/yyyy', dFecha ) ] );
                    end
                    else
                    begin
                         Result := True;
                         AjustarFechaHora( eTurnoLabor( FieldByName( 'AR_SHIFT' ).AsInteger ), FieldByName( 'AR_PRI_HOR' ).AsString, dFecha, sHora );
                    end;
                    Active := False;
               end;
          end
          else
              Result := True;
          if Result then
          begin
               ParamAsInteger( FBreakTarjeta, 'Empleado', iEmpleado );
               ParamAsDate( FBreakTarjeta, 'Fecha', dFecha );
               with FBreakTarjeta do
               begin
                    Active := True;
                    lHayTarjeta := ( Fields[ 0 ].AsInteger > 0 );
                    Active := False;
               end;
               if not lHayTarjeta then
                  InsertaTarjeta( iEmpleado, dFecha );
               ParamAsInteger( FBreakAgrega, 'CB_CODIGO', iEmpleado );
               ParamAsDate( FBreakAgrega, 'AU_FECHA', dFecha );
               ParamAsChar( FBreakAgrega, 'WK_HORA_A', sHora, K_ANCHO_HH_MM );
               ParamAsDate( FBreakAgrega, 'WK_FECHA_R', dFecha );
               ParamAsChar( FBreakAgrega, 'WK_HORA_R', sHora, K_ANCHO_HH_MM );
               ParamAsInteger( FBreakAgrega, 'WK_TIPO', Ord( wtBreak ) );
               ParamAsFloat( FBreakAgrega, 'WK_PRE_CAL', iDuracion ); { Duraci�n del Break }
               ParamAsBoolean( FBreakAgrega, 'WK_MANUAL', True );
               Ejecuta( FBreakAgrega );
          end;
     end;
end;

function TLCalculo.GetAreasDiariasDataset( const iEmpleado: TNumEmp; const dFecha: TDate ): OleVariant;
var
   FDataset: TServerDataset;
   FQuery, FKardex: TZetaCursor;
   sHora, sHorMov, sHoraEntrada: string;
   dFecMov: TDate;
   iCodigo: integer;

begin
     Result := NULL;
     sHoraEntrada := VACIO;
     FDataset := TServerDataSet.Create( nil );
     try
        with FDataset do
        begin
             AddIntegerField( 'CB_CODIGO' );
             AddDateField( 'KA_FECHA' );
             AddStringField( 'KA_HORA', K_ANCHO_HH_MM );
             AddStringField( 'KA_HOR_FIN', K_ANCHO_HH_MM );
             AddStringField( 'CB_AREA', K_ANCHO_AREA );
             AddIntegerField( 'US_CODIGO' );
             AddDateField( 'KA_FEC_MOV' );
             AddStringField( 'KA_HOR_MOV', K_ANCHO_HH_MM );
             AddIntegerField( 'CH_TIPO' );
             AddIntegerField( 'KA_TIEMPO' );
             CreateTempDataSet;
             MergeChangeLog;
             //MergeChangeLog := False;
             IndexFieldNames := 'KA_HORA;CH_TIPO';
        end;

        with oZetaProvider do
        begin
             { Leer las checadas de Asistencia }
             FQuery := CreateQuery( GetSQLScript( Q_CHECADAS_LEE ) );
             try
                ParamAsInteger( FQuery, 'Empleado', iEmpleado );
                ParamAsDate( FQuery, 'Fecha', dFecha );
                with FQuery do
                begin
                     Active := True;
                     while not Eof do
                     begin
                          if strVacio( sHoraEntrada ) then
                             sHoraEntrada := FieldByName( 'CH_H_AJUS' ).AsString;
                          FDataset.Append;
                          FDataset.FieldByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
                          FDataset.FieldByName( 'KA_FECHA' ).AsDateTime := dFecha;
                          FDataset.FieldByName( 'KA_HORA' ).AsString := FieldByName( 'CH_H_AJUS' ).AsString;
                          FDataset.FieldByName( 'CH_TIPO' ).AsInteger := FieldByName( 'CH_TIPO' ).AsInteger;
                          FDataset.FieldByName( 'CB_AREA' ).AsString := K_SUFIJO_AREA + IntToStr(FieldByName( 'CH_TIPO' ).AsInteger);
                          FDataset.Post;
                          Next;
                     end;
                     Active := False;
                end;
             finally
                    FreeAndNil( FQuery );
             end;
             if strVacio( sHoraEntrada ) then
                sHoraEntrada := K_CERO_HORAS;
             FQuery := CreateQuery( GetSQLScript( Q_EMPLEADO_AREAS_LISTA ) );
             try
                ParamAsInteger( FQuery, 'Empleado', iEmpleado );
                ParamAsDate( FQuery, 'FechaInicial', dFecha );
                ParamAsChar( FQuery, 'HoraInicial', sHoraEntrada, K_ANCHO_HH_MM );
                ParamAsDate( FQuery, 'FechaFinal', dFecha );
                ParamAsChar( FQuery, 'HoraFinal', K_MEDIANOCHE, K_ANCHO_HH_MM );
                FKardex := CreateQuery( Format( GetSQLScript( Q_KARDEX_AREAS ), [ iEmpleado ] ) );
                try
                   with FQuery do
                   begin
                        Active := True;
                        while not Eof do
                        begin
                             ParamAsDate( FKardex, 'KA_FECHA', dFecha );
                             ParamAsChar( FKardex, 'KA_HORA', FieldByName( 'KA_HORA' ).AsString, K_ANCHO_HH_MM );
                             ParamAsChar( FKardex, 'CB_AREA', FieldByName( 'CB_AREA' ).AsString, K_ANCHO_AREA );
                             FKardex.Active := True;
                             if not FKardex.IsEmpty then
                             begin
                                  dFecMov := FKardex.FieldByName( 'KA_FEC_MOV' ).AsDateTime;
                                  sHorMov := FKardex.FieldByName( 'KA_HOR_MOV' ).AsString;
                                  iCodigo := FKardex.FieldByName( 'US_CODIGO' ).AsInteger;
                             end
                             else
                             begin
                                  dFecMov := NullDateTime;
                                  sHorMov := VACIO;
                                  iCodigo := 0;
                             end;
                             FKardex.Active := False;
                             if ( sHoraEntrada <> K_CERO_HORAS ) or ( FieldByName( 'KA_HORA' ).AsString <> K_CERO_HORAS ) then
                             begin
                                  FDataset.Append;
                                  FDataset.FieldByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
                                  FDataset.FieldByName( 'KA_FECHA' ).AsDateTime := dFecha;
                                  FDataset.FieldByName( 'KA_HORA' ).AsString := FieldByName( 'KA_HORA' ).AsString;
                                  FDataset.FieldByName( 'CB_AREA' ).AsString := FieldByName( 'CB_AREA' ).AsString;
                                  FDataset.FieldByName( 'KA_FEC_MOV' ).AsDateTime := dFecMov;
                                  FDataset.FieldByName( 'KA_HOR_MOV' ).AsString := sHorMov;
                                  FDataset.FieldByName( 'US_CODIGO' ).AsInteger := iCodigo;
                                  FDataset.FieldByName( 'CH_TIPO' ).AsInteger := Ord( chDesconocido ); { eTipoChecadas( 0 ) }
                                  FDataset.Post;
                             end;
                             Next;                             
                        end;
                        Active := False;
                   end;
                finally
                       FreeAndNil( FKardex );
                end;
             finally
                    FreeAndNil( FQuery );
             end;
             { Calcular Las Horas Finales }
             { Se Supone Que FDataset Ya Est� Ordenado Por Hora }
        end;
        if not FDataSet.IsEmpty then
        begin
             with FDataset do
             begin
                  { Se Posiciona Sobre El Ultimo Registro }
                  Last;
                  { Guarda La Ultima Hora De Inicio }
                  sHora := FieldByName( 'KA_HORA' ).AsString;
                  { Se Posiciona Sobre El Pen�ltimo Registro }
                  Prior;
                  { Para Empezar A Navegar Hacia Arriba }
                  while not Bof do
                  begin
                       if ( FieldByName( 'CH_TIPO' ).AsInteger = 0 ) then
                       begin
                            Edit;
                            { La Hora Final De Este Registro Es La Hora Inicial Del Registro Siguiente ( en order ascendente ) }
                            FieldByName( 'KA_HOR_FIN' ).AsString := sHora;
                            { Calcular La Duraci�n De Este Registro }
                            FieldByName( 'KA_TIEMPO' ).AsInteger := CalculaMinutos( sHora, FieldByName( 'KA_HORA' ).AsString );
                            { La Hora Inicial De Este Registro Ser� La Hora Final Del Registro Anterior ( en order ascendente ) }
                            sHora := FieldByName( 'KA_HORA' ).AsString;
                            Post;
                       end;
                       Prior;
                  end;
                  First;
             end;
        end;
        Result := FDataset.Data;
     finally
            FreeAndNil( FDataset );
     end;
end;

{ TLOrden }

constructor TLOrden.Create;
begin
     FCodigo := '';
     FPiezas := 0.0;
end;


end.


