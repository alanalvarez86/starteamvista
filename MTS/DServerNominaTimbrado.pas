unit DServerNominaTimbrado;

{$define CUENTA_EMPLEADOS}
{$define VALIDAEMPLEADOSGLOBAL}

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      DServerNomina.pas                          ::
  :: Descripci�n: Programa principal de Nomina.dll           ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComServ, ComObj, VCLCom, StdVcl, DataBkr, Db, DBClient, MtsRdm, Mtx, Provider,
     {$ifndef VER130}Variants,{$endif}
     DNominaClass,
     {$ifndef DOS_CAPAS}
     TimbradoNomina_TLB,
     {$endif}
     ZCreator,
     ZetaServerDataSet,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaQRExpr,
     ZetaSQLBroker,
     ZAgenteSQL,
     DQueries,
     DZetaServerProvider;

{$define QUINCENALES}
{.$undefine QUINCENALES}

{.$define FLEXIBLES}
{.$undef FLEXIBLES}

{$define DEBUGSENTINEL}
{$define CAMBIO_TNOM}

type
  eFormaNomina = ( fnDatosClasifi,
                   fnDiasHoras,
                   fnExcepciones,
                   fnExcepGlobales,
                   fnExcepMontos,
                   fnMontos,
                   fnTotales,
                   fnDatosNomina,
                   fnDatosAsist,
                   fnFaltas,
                   fnMovMontos,
                   fnPagoRecibos,
                   fnValidaNomina,
                   fnUpdateRecibos,
                   fnLiquidacion,
                   fnBorraNominas,
                   fnBorraPeriodo,
                   fnBorraPoliza,
                   fnUpdatePeriodo,
                   fnBorraPeriodoLista,
                   fnExisteEmpleado,
                   fnExisteNomina,
                   fnInsertaPago,
                   fnMovDatosAsist,
                   fnDatosPoliza,
                   fnFonTot,
                   fnFonEmp,
                   fnFonCre,
                   fnDeclaracion
                   );

  TTotalesFonacotEmpleado = record
    Retencion : TPesos;
    Nomina    : TPesos;
    Ajuste    : TPesos;
    Creditos  : Integer;
  end;

  TTotalesFonacotEmpresa = record
    Retencion : TPesos;
    Nomina    : TPesos;
    Ajuste    : TPesos;
    Creditos  : Integer;
    Empleados : Integer;
    Bajas     : Integer;
    Incapacidades : Integer;
  end;
  TCalculoFonacot = record
     ReportarIncapa : Boolean;
     ReportarPrimera: Boolean;
     EsActivo: Boolean;
     EsBajaEnMes: Boolean;
     EsEmpleadoDif: Boolean;
     TipoNominaDif: Boolean;
     FiltraporNom: Boolean;
     FechaFinalMes: TDate;
     FechaReferencia: TDate;
     FechaIncidencia: TDate;
     FechaFinalIncapa: TDate;
  end;

  {$ifdef CAMBIO_TNOM}
   TDatosNominaEmpleado = record
    Empleado               : Integer;
    FecCambioTNomina       : TDate;
    EsCambioEnPeriodo      : Boolean;
    TNominaInicio          : eTipoPeriodo;
    TNominaFin             : eTipoPeriodo;
    FecNominaInicio           : TDate;
    FecNominaFin              : TDate;
  end;
  {$endif}

  TdmServerNominaTimbrado = class(TMtsDataModule {$ifndef DOS_CAPAS}, IdmServerNominaTimbrado {$endif})
    cdsTempo: TServerDataSet;
    cdsLista: TServerDataSet;
    cdsAuxiliar: TServerDataSet;
    procedure dmServerNominaCreate(Sender: TObject);
    procedure dmServerNominaDestroy(Sender: TObject);
  private
    { Private declarations }
    {$ifdef BITACORA_DLLS}
    FListaLog : TAsciiLog;
    FPaletita : string;
    FArchivoLog: string;
    FEmpresa:string;
    {$endif}
    oZetaProvider: TdmZetaServerProvider;
{$ifndef DOS_CAPAS}
    oZetaCreator: TZetaCreator;
    oZetaSQLBroker: TSQLBroker;
{$endif}
    FNomina: TNomina;
    FEmpleado: TNumEmp; { Para optimizar procesamiento de Grids de Edici�n }
    FRecibosEmpresa: OleVariant;
    FImportar: eImportacion;
    FOperacion: eOperacionConflicto;
    FConceptoTipo: TZetaCursor;
    FEmpleadoLee: TZetaCursor;
    FNominaLiquidacion: TZetaCursor;
    FVacaciones: TZetaCursor;
    FPrestaciones: TZetaCursor;
    FExisteEmp: TZetaCursor;
    FExisteNom: TZetaCursor;
    FHayLiquidacion: TZetaCursor;
    FAguinaldoEmp: TZetaCursor;
    FQryConflictoMontos: TZetaCursor;
    FQrySumarMontos: TZetaCursor;
    FQrySustituirMontos: TZetaCursor;
    FQryBorrarMontos: TZetaCursor;

    FQryConflictoFaltas: TZetaCursor;
    FQrySumarFaltas: TZetaCursor;
    FQrySustituirFaltas: TZetaCursor;
    FQryBorrarFaltas: TZetaCursor;


    FListaParametros: string;
    FListaFormulas: string;
{$ifdef VALIDAEMPLEADOSGLOBAL}
    FMsgAdvertencia: string;
    FMsgError: string;
{$endif}

    FFormaNomina : eFormaNomina;
    FQryPrestamo, FInsCargo, FInsPrestamo, FUpdPrestamo, FDelCargo : TZetaCursor;
    {$ifdef DOS_CAPAS}
    FSaldoActualAjuste: TZetaCursor;
    {$endif}
    {$ifdef CAMBIO_TNOM}
    FNominaEmpleado: TDatosNominaEmpleado;
    {$endif}
    FQryConcepto:TZetaCursor;

    property Nomina: TNomina read FNomina;
    function GetSQLBroker: TSQLBroker;
    function AfectarDesafectarNomina(const eStatus, eStatusNuevo: eStatusPeriodo; const eProceso : Procesos ): OleVariant;
    function BorrarPeriodosGetLista(Empresa, Parametros: OleVariant): OleVariant;
    function BorrarPeriodosLista(Empresa, Parametros, Lista: OleVariant): OleVariant;
    function BorraUnPeriodoDataset(Empresa: OleVariant): TZetaCursor;
    function CalcularDiferenciasDataset(DataSet: TDataset; var FPeriodoNuevo: TDatosPeriodo): OleVariant;
    function CopiarNominasDataset(DataSet: TDataSet;FPeriodoOriginal: TDatosPeriodo; const lCancelarNomina: Boolean; const sAdvertencia: String ): Olevariant;
    function FoliarRecibosDataset: OleVariant;
    function GetDatosLiquidacion( DataSetEmpleado : TDataset; const iEmpleado: TNumEmp; dBaja: TDate = 0 ): TDatosLiquidacion;
    function GetScript( const eTipo: eFormaNomina ): String;
    function ImportarPagoRecibosDataset(Dataset: TDataset): OleVariant;
    function LimpiarAcumuladoDataset(Dataset: TDataset): OleVariant;
    function LiquidacionGlobalDataset( Dataset: TDataset ): OleVariant;
    function PagarPorFueraDataset(Dataset: TDataset): OleVariant;
    function PuedeAgregarLiquidacion(const Empleado: integer; const dFecha, dBaja: TDate; var iPeriodo: Integer): Boolean;
    function RecalculoAcumuladoDataset(DataSet: TDataSet): OleVariant;
    function ReFoliarRecibosDataset: OleVariant;
    function GetNavegacion( Empresa: OleVariant; sNavegacion: String ): String;
    //** Poliza */
    function PolizaDataset(Filtros : Olevariant): OleVariant;
    function GetBitacoraMasInfo(DataSet: TzProviderClientDataSet; eClase: eClaseBitacora): string;
    function AplicaFiniquitoSimulacion( const iEmpleado: Integer; const oPeriodoSimulacion: TDatosPeriodo ): OleVariant;
    procedure BeforeUpdatePagoRecibos(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure ErrorDiasHoras(Sender: TObject; DataSet: TzProviderClientDataSet; E: EUpdateError; UpdateKind: TUpdateKind; var Response: TResolverResponse);
    procedure ErrorMontos(Sender: TObject; DataSet: TzProviderClientDataSet; E: EUpdateError; UpdateKind: TUpdateKind; var Response: TResolverResponse);
    procedure PrepararDiasHoras(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BorraUnPeriodo( Dataset: TZetaCursor; const iYear, iTipo, iPeriodo: Integer; const dMovimiento: TDate );
    procedure CalcularDiferenciasBuildDataset( var FPeriodoNuevo: TDatosPeriodo);
    procedure ClearBroker;
    procedure ClearNomina;
    procedure CopiarNominasBuildDataset(var FPeriodoOriginal: TDatosPeriodo);
    procedure GetDatosLiquidacionBegin;
    procedure GetDatosLiquidacionEnd;
    procedure ImportarMovimientosValidaASCII(DataSet: TDataset; var nProblemas: Integer; var sErrorMsg: String );
    procedure ImportarPagoRecibosBuildDataSet;
    procedure ImportarPagoRecibosCrearDataSet;
    procedure ImportarPagoRecibosValidaASCII(DataSet: TDataset; var nProblemas: Integer; var sErrorMsg: String);
    procedure InitBufferEmpleado;
    procedure InitBroker;
    procedure InitCreator;
    procedure InitNomina;
    procedure InitPrestaciones;
    procedure InitQueries;
   // procedure InitQueryConflicto(const iSQL: Integer);
    procedure InitRitmos;
    procedure LimpiarAcumuladoBuildDataset;
    procedure LiquidacionGlobalBuildDataset;
    procedure GetLiquidacionBuildDataset( const iEmpleado: integer );
    procedure PagarPorFueraBuildDataset;
    procedure RecalculoAcumuladoBuildDataset;
    procedure RecalculoDiasDataset( const iYear: Integer; const eTipo: eTipoPeriodo );
    procedure SetDetailInfo( const eDetail: EFormaNomina );
    procedure SetOperacionConflicto;
    procedure SetPrenominaValues(Sender: TObject; DataSet: TzProviderClientDataSet);
    procedure VerificarNomina(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure ActualizarStatusNomina(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    procedure CheckStatusNomina(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    procedure SetStatusPeriodo(ODatosPeriodo: TDatosPeriodo);
    procedure SetTablaInfo( const eTipo: EFormaNomina );
    procedure ErrorStatusNomina( const lCancelarNomina: Boolean; var sAdvertencia : String );
    procedure CreateErrorMontosDataSet;
    procedure CreateErrorDiasHorasDataSet;
    procedure InitQueryNomina( var FQueryNomina: TZetaCursor; const iSQL: Integer);
    procedure RevisaAutorizacion;
    procedure ActualizaFolios(const iFolio, iInicial, iFinal: Integer);
    procedure BorrarPeriodosParametros;
    procedure AfectarNominaParametros;
    procedure DesafectarNominaParametros;
    procedure DefinirPeriodosParametros;
    procedure LimpiarAcumuladoParametros;
    procedure RecalculoAcumuladoParametros;
    procedure CopiarNominaParametros;
    procedure CalcularDiferenciasParametros;
    procedure FoliarRecibosParametros;
    procedure ReFoliarRecibosParametros;
    procedure PagarPorFueraParametros;
    procedure PolizaParametros;
    function LiquidacionGlobalSimulacion(Dataset: TzProviderClientDataSet):Olevariant;
    Procedure LiquidacionGlobalParametros;
    procedure ImportarMovimientosParametros;
    procedure ExportarMovimientosParametros;
    procedure ImportarPagoRecibosListaParametros;
    procedure AgregaAgenteFunciones( oSQLAgente: TSQLAgente );

    {M�todos de C�lculo y Ajuste de Fonacot}
    procedure AjusteRetFonacotParametros;
    procedure AjusteRetFonacotBuildDataSet;
    function  AjusteRetFonacotDataset(Dataset: TDataset): OleVariant;
    procedure PreparaPrestamosFonacot;
    procedure CalcularPagoFonacotParametros;
    procedure CalcularPagoFonacotBuildDataSet;
    function  CalcularPagoFonacotDataset( Dataset: TDataset ): OleVariant;
    function  GetQryMontos( const sFiltroPeriodo: String; const iYear, iConcepto, iMes: Integer ): String;
    function GetFiltroPeriodo ( const sPeriodosConcat: String ): String;
    function GetSaldoFonacotAnt( const rTotalAbonoFonacot, rMontoRetenido, rSaldoAjuste, rMontoAjuste: TPesos ): TPesos;

    {Metodos para la conciliacion de Timbrado de nomina}
    procedure CopiarConciliacionTimbradoParametros;

    {M�todos para Grabar a Bitacora}
    //function GetBitacoraMensaje(eClase: eClaseBitacora): string;
    procedure GrabaCambiosBitacora( DataSet: TzProviderClientDataSet; UpdateKind: TUpdateKind);overload;
    procedure GrabaCambiosBitacora( DataSet : TzProviderClientDataSet; const sField1, sField2: string; const eClase: EClaseBitacora; const eOperacion : eOperacionConflicto; const rMonto: Currency );overload;
    procedure GrabaCambiosBitacoraEvent( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    {$IFDEF DOS_CAPAS}
    function GetSaldoAjuste( const iEmpleado: Integer; const sReferencia: String ): TPesos;
    {$ENDIF}

    function GetRangosPeriodos( const sScript: String; const iMaxTiposNom, iYear, iMes: Integer; const oArregloRango: OleVariant  ): String;
    function GetParamRangosPeriodos( const oArregloRango: OleVariant; const iTipoNomina, iParam, iMaxRangos: Integer ): String;
    procedure InitLog( Empresa: Olevariant; const sPaletita: string );
    procedure EndLog;
    procedure LiquidacionSimulacionGlobalBuildDataset;
    procedure AplicacionGlobalFiniquitosBuildDataSet;
    function AplicacionGlobalFiniquitosDataset( Dataset: TDataset): OleVariant;
    procedure TimbrarNominasBuildDataset(  sRS_CODIGO : string; statusAnterior, statusNuevo : eStatusTimbrado ; sOrden : string);
    procedure SetStatusAplicado(Empleado: Integer;  const oPeriodoSimulacion: TDatosPeriodo);

    {Timbrado}
    function TimbrarNominasListaDataset( Dataset: TDataset): OleVariant;
    function NominasPendientesListaDataset( Dataset: TDataSet): OleVariant;
  protected
    { Protected declarations }
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    property SQLBroker: TSQLBroker read GetSQLBroker;
    function GetFoliosPendientes(EMPRESA, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function ActualizaFoliosFiscales(Empresa, Parameters: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetEmpleadosBuscados_DevExTimbradoAvanzada(Empresa: OleVariant;
      const sPaterno, sMaterno, sNombre, sRFC, sNSS,
      sBanca: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetEmpleadoTransferidoParaActivo(Empresa: OleVariant;
      Empleado: Integer; out Datos: OleVariant): WordBool; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetEmpleadoTransferidoAnterior(Empresa: OleVariant;
      Empleado: Integer; const sNavega: WideString;
      out Datos: OleVariant): WordBool; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetEmpleadoTransferidoSiguiente(Empresa: OleVariant;
      Empleado: Integer; const sNavega: WideString;
      out Datos: OleVariant): WordBool; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetValidacionEmpleadoTransferido(Empresa: OleVariant;
      Empleado: Integer; out Datos: OleVariant): WordBool; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetPeriodoAnteriorTimbrado(Empresa: OleVariant; Year, Tipo,
      Numero: Integer; out Datos: OleVariant): WordBool; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetPeriodoSiguienteTimbrado(Empresa: OleVariant; Year, Tipo,
      Numero: Integer; out Datos: OleVariant): WordBool; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetPeriodoTimbrado(Empresa: OleVariant; Year, Tipo,
      Numero: Integer; out Datos: OleVariant): WordBool; {$ifndef DOS_CAPAS} safecall;{$endif}


{$ifdef DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
{$endif}
    procedure GrabaLiquidacion(Empresa, Parametros, Datos, Ahorros, Prestamos: OleVariant); {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetEmpleadoClasifi(Empresa: OleVariant; iEmpleado, iYear, iTipo, iPeriodo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetDatosTotales(Empresa: OleVariant; iYear, iTipo,iPeriodo: Integer; const sConfiden: WideString): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function GetDatosExcepGlobales(Empresa: OleVariant; iYear, iTipo, iPeriodo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetDatosExcepciones(Empresa: OleVariant; iYear, iTipo, iPeriodo: Integer; lSoloAltas: WordBool): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetDatosExcepMontos(Empresa: OleVariant; iYear, iTipo, iPeriodo: Integer; lSoloAltas: WordBool): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetDatosJornada(Empresa: OleVariant; const Turno: WideString; FechaInicial, FechaFinal: TDateTime): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetDatosNomina(Empresa: OleVariant; iEmpleado, iYear, iTipo, iPeriodo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetNomDatosAsist(Empresa, Parametros: OleVariant; out Tarjetas: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetMontos(Empresa: OleVariant; iEmpleado, iYear, iTipo, iPeriodo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetMovMontos(Empresa: OleVariant; iEmpleado, iYear, iTipo, iPeriodo: Integer; out oTotales: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetDiasHoras(Empresa: OleVariant; iEmpleado, iYear, iTipo, iPeriodo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetFaltas(Empresa: OleVariant; iEmpleado, iYear, iTipo, iPeriodo: Integer; out oTotales: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetPagoRecibos(Empresa: OleVariant; iYear, iTipo, iPeriodo: Integer; lSoloAltas: WordBool): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaEmpleadoClasifi(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaExcepGlobales(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaMovMontos(Empresa, Parametros, oDelta: OleVariant; out ErrorCount: Integer; out ErrorData: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaFaltas(Empresa, Parametros, oDelta: OleVariant; out ErrorCount: Integer; out ErrorData: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaNomina(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}    function GrabaPrenomina(Empresa, Parametros, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaDiasHoras(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaPagoRecibos(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function ValidaNominaEmpleado(Empresa: OleVariant; iEmpleado, iYear, iTipo, iPeriodo: Integer; out dFecha: TDateTime; out iUsuario: Integer): WordBool; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetLiquidacion(Empresa: OleVariant; Empleado: Integer; Parametros: OleVariant; out Ahorros, Prestamos: OleVariant; Ingreso: TDateTime): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function BorraNomina(Empresa: OleVariant; iUsuario: Integer;dFecha: TDateTime; iYear, iTipo, iPeriodo: Integer;BorraRegistros: WordBool; const Filtro: WideString): OleVariant;{$ifndef DOS_CAPAS} safecall; {$endif}
    function BorrarPeriodos(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function AfectarNomina(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function DesafectarNomina(Empresa, Parametros: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall; {$endif}
    function DefinirPeriodos(Empresa, Parametros: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall; {$endif}
    function LimpiarAcumulado(Empresa, Parametros: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall; {$endif}
    function RecalculoAcumulado(Empresa, Parametros: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall; {$endif}
    function RecalculoDias(Empresa, Parametros: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall; {$endif}
    function CalcularDiferencias(Empresa, Parametros: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall; {$endif}
    function CopiarNomina(Empresa, Parametros: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall; {$endif}
    function FoliarRecibos(Empresa, Parametros: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall; {$endif}
    function ReFoliarRecibos(Empresa, Parametros: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall; {$endif}
    function PagarPorFuera(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function PagarPorFueraLista(Empresa, Lista,Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function PagarPorFueraGetLista(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function Poliza(Empresa, Parametros, Filtros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function LiquidacionGlobal(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function LiquidacionGlobalGetLista(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function LiquidacionGlobalLista(Empresa, Lista, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function ImportarMovimientos(Empresa, Parametros, Datos: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function ImportarMovimientosGetASCII(Empresa, Parametros, ListaASCII: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function ExportarMovimientos(Empresa, Parametros: OleVariant; out Datos: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function ImportarPagoRecibosGetASCII(Empresa, Parametros, ListaASCII: OleVariant; var ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function ImportarPagoRecibosLista(Empresa, Lista, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function ImportarPagoRecibosGetLista(Empresa, Parametros, Lista: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function RevisaLiquidacion(Empresa, Parametros: OleVariant; Empleado: Integer; Fecha: TDateTime; var lPuedeAgregar: WordBool): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetDatosPoliza(Empresa, Parametros: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaDatosPoliza(Empresa, oDelta: OleVariant;out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function AplicaFiniquito(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function AjusteRetFonacot(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function AjusteRetFonacotGetLista(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function AjusteRetFonacotLista(Empresa, Lista, Parametros: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall; {$endif}
    function CalcularPagoFonacot(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetTotalesFonacot(Empresa: OleVariant; iYear, iMes: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaTotalesFonacot(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetDatosFonacot(Empresa: OleVariant; iYear, iMes, iEmpleado, iTabla: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetTNomEmpleado(Empresa: OleVariant; iEmpleado: Integer; dFecha: TDateTime): Integer; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetFecCambioTNom(Empresa: OleVariant; iEmpleado: Integer; dFecha: TDateTime): TDateTime; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetSimulacionesGlobales(Empresa, Params: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall; {$endif}
    function GetTotalesConceptos(Empresa, Parametros: OleVariant;out Movimientos: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetSimulacionesAprobar(Empresa,Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function AplicacionGlobalFiniquitos(Empresa,Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function AplicacionGlobalFiniquitosGetLista(Empresa,Parametros: OleVariant): OleVariant;  {$ifndef DOS_CAPAS} safecall; {$endif}
    function AplicacionGlobalFiniquitosLista(Empresa, Lista,Parametros: OleVariant): OleVariant;  {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetDeclaracion(Empresa: OleVariant): OleVariant;  {$ifndef DOS_CAPAS} safecall; {$endif}
    function TimbrarNominasGetLista(Empresa,   Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function TimbrarNominasLista(Empresa, Lista,  Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function CancelarTimbrado(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function MarcarTimbrado(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;  {$endif}
    function AplicarConciliacionTimbradoPeriodos(var Parametros: OleVariant): WordBool; {$ifndef DOS_CAPAS} safecall; {$endif}
    function IniciarConciliacionTimbradoPeriodos(var Parametros: OleVariant): WordBool; {$ifndef DOS_CAPAS} safecall;{$endif}
    function ObtenerPeriodosAConciliarTimbrado(Empresa: OleVariant; iYear, iMes: Integer; const sRsCodigo: WideString): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetMotorAvanceProceso(var Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function ObtenerXMLEmpleadosAConciliarTimbrado(Empresa: OleVariant; var Params: OleVariant; var XML, Error: WideString): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function ObtenerPeriodosConciliados(Empresa: OleVariant;var Params: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetEmpleadosConciliados(Empresa: OleVariant; var Params: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function SetEmpleadosPendienteTimbrado(Empresa, Lista, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetXMLAutidoriaConceptoTimbrado(Empresa: OleVariant;var Params: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetLookupEmpleado(Empresa: OleVariant; Empleado: Integer; out Datos: OleVariant;
          TipoLookup: Integer): WordBool; safecall;


    function GetEmpleadosBuscados_DevExTimbrado(Empresa: OleVariant; const sPista: WideString): OleVariant;
          safecall;


  end;

implementation

uses ZEvaluador,
     ZGlobalTress,
     ZFuncsPoliza,
     ZetaTipoEntidad,
     ZetaCommonTools,
     ZetaServerTools,
     DSuperASCII,
     DPrestaciones,
     DPoliza,
     FAutoServer,
     ZFiltroSQLTools,
{$ifdef DEBUGSENTINEL}
     {$ifndef DOS_CAPAS}
     ZetaLicenseMgr,
     {$endif}
{$endif}
     FAutoClasses , ZAgenteSQLClient;

{$R *.DFM}

type
  eTipoExcepcion = ( teMonto, teDias, teHoras, teTotales );
const
     K_FILTRO_PERIODO = '( PE_YEAR = %d ) and ( PE_TIPO = %d ) and ( PE_NUMERO = %d )';
     K_FILTRO_PERIODO_EMPLEADO = '( CB_CODIGO = %d ) and ( PE_YEAR = %d ) and ( PE_TIPO = %d ) and ( PE_NUMERO = %d )';
     K_FILTRO_CALCULO_FONACOT = '( FT_YEAR = %d ) and ( FT_MONTH = %d ) and ( PR_TIPO = %s ) %s';
     K_FILTRO_PERIODO_RANGOS = '( PE_YEAR = %0:d ) AND ( PE_STATUS = %1:d ) AND ( %2:s  OR ( PE_NUMERO >= %3:d and PE_MES = %4:d ) ) ';

     K_PARAM_FECHA_FIN = 4;
     K_PARAM_FECHA_INI = 3;

     K_GRUPO_NULO = 'X';
     K_MAX_GRUPOS = 4;
     K_ANCHO_REFERENCIA = 8;
     K_ANCHO_TABLA_SS = 1;
     K_ANCHO_DIA_HOR = 1;
     Q_AFECTA_PERIODO = 1;
     Q_AFECTA_PERIODO_CUANTOS = 2;
     Q_STATUS_PERIODO = 3;
     Q_DATOS_PERIODO = 4;
     Q_DEFINIR_PERIODOS_INSERT = 5;
     Q_RECALCULO_DIAS_PERIODO = 6;
     Q_RECALCULO_DIAS_UPDATE = 7;
     Q_LIMPIA_ACUMULADOS_BORRA = 8;
     Q_LIMPIA_ACUMULADOS_UPDATE = 9;
     Q_DIFERENCIAS_AGREGA_NOMINA = 10;
     Q_DIFERENCIAS_BORRA_NOMINA = 11;
     Q_DIFERENCIAS_SET_STATUS = 12;
     Q_DIFERENCIAS_COMPARA_NOMINAS = 13;
     Q_DIFERENCIAS_INSERT = 14;
     Q_FOLIOS_SELECT_ORDEN = 15;
     Q_FOLIOS_UPDATE_CAMPO = 16;
     Q_FOLIOS_SELECT = 17;
     Q_FOLIOS_UPDATE_LIMITES = 18;
     Q_REFOLIAR_UPDATE = 19;
     Q_PAGAR_FUERA_UPDATE = 20;
     Q_LIQUIDACION_HAY_NOMINA = 27;
     Q_LIQUIDACION_NOMINA = 28;
     Q_LIQUIDACION_AHORROS = 29;
     Q_LIQUIDACION_PRESTAMOS = 30;
     Q_LIQUIDACION_VACACIONES = 31;
     Q_LIQUIDACION_AGUINALDO = 32;
     Q_EMPLEADO_STATUS = 33;
     Q_CONCEPTO_TIPO = 34;
     Q_NOMINA_COPIA = 35;
     Q_ACUMULADO_EXISTE = 36;
     Q_ACUMULADO_AGREGA = 37;
     Q_ACUMULADO_MODIFICA = 38;
     Q_ACUMULADO_MODIFICA_ANUAL = 39;
     Q_EXPORTAR_FALTAS = 40;
     Q_EXPORTAR_MOVIMIENTOS_TODOS = 41;
     Q_EXPORTAR_MOVIMIENTOS_EXCEP = 42;
     Q_FALTAS_SUMAR = 43;
     Q_FALTAS_SUSTITUIR = 44;
     Q_MONTOS_SUMAR = 45;
     Q_MONTOS_SUSTITUIR = 46;
     Q_PRE_NOMINA = 47;
     Q_NOMINA_CANCELA = 48;
     Q_ENCABEZADO_PERIODO = 49;
     Q_MONTOS_BORRAR = 50;
     Q_MONTOS_EXISTE = 51;
     Q_FALTAS_BORRAR = 52;
     Q_FALTAS_EXISTE = 53;
     Q_EXCEP_GLOBAL_SET_STATUS = 54;
     Q_LIQUIDACION_AGUINALDO_EMP = 55;
     Q_BITACORA_BORRA_NOMINA = 56;
     Q_BITACORA_BORRA_PERIODO = 57;
     Q_DATOS_EMPLEADO_PRENOMINA = 58;
     Q_SET_MOVIMIEN_EXCEPCIONES = 59;
     Q_MONTOS_RETENIDOS = 60;
     Q_REEMPLAZAR_AJUSTE = 61;
     Q_MONTOS_OTROS_AJUSTES = 62;
     Q_MONTO_AJUSTE_ULTIMO_MES = 63;
     Q_SALDO_ACTUAL_AJUSTE = 64;
     Q_FILTRO_PERIODO_MEN = 65;
     Q_FILTRO_X_TIPNOM = 66;
     Q_FILTRO_COL_NOMINA = 67;
     Q_MONTOS_RETENIDOS_EMP = 68;
     Q_FILTRO_X_TIPNOM_FEC = 69;
     Q_FILTRO_NIVEL_CONFI = 70;
     Q_EMP_INCA = 71;{OP: 12/06/08}
     Q_GET_SIM_GLOBALES = 72;
     Q_GET_SIM_TOTALES_CONCEPTOS = 73;
     Q_GET_SIM_TOTALES_NOMINA = 74;
     Q_GET_SIM_A_APROBAR = 75;
     Q_UPDATE_APLICADOS = 76;
     Q_CONCEPTO = 77;
     Q_TIMBRAR_EMPLEADO = 78;
     Q_TIMBRAR_PERIODO = 79;
     Q_CAMPOS_VISTA_TRANSFERIDOS = 80;
     Q_VA_PERIODO = 81;
     Q_VA_PERIODO_ANTERIOR = 82;
     Q_VA_PERIODO_SIGUIENTE = 83;
     Q_PERIODOS_X_MES_YEAR = 84;//COnciliacion Timbrado De nomina
     Q_COCILIACION_NOMINTA_TIMBRAMEX = 85;
     Q_CONCILIACION_EMPLEADOS_TIMBRADO = 86;
     Q_PENDIENTE_EMPLEADOS_TIMBRADO = 87;


function GetMesAcumulado( const iMes: Word ): String;
const
     SET_MES = 'AC_MES_%s';
begin
     Result := Format( SET_MES, [ ZetaCommonTools.FormateaNumero( ZetaCommonTools.iMax( 1, ZetaCommonTools.iMin( 13, iMes ) ), 2, '0' ) ] );
end;

function GetSQLScript( const iScript: Integer ): String;
const
    K_WHERE_NOMINA = 'where PE_YEAR = :Year and PE_TIPO = :Tipo and PE_NUMERO = :Numero and CB_CODIGO = :Empleado ';
    K_WHERE_FALTAS =  K_WHERE_NOMINA +
                     'and FA_DIA_HOR = :DiaHor and FA_FEC_INI = :Fecha and FA_MOTIVO = :Motivo';
    K_WHERE_MONTOS =  K_WHERE_NOMINA +
                     'and CO_NUMERO = :Concepto and MO_REFEREN = :Referencia';
begin
     case iScript of
{$ifdef INTERBASE}
          Q_AFECTA_PERIODO: Result := 'select EMPLEADO from AFECTA_PERIODO( :Year, :Tipo, :Numero, :Usuario, :StatusViejo, :StatusNuevo, :Factor )';
{$else}
          Q_AFECTA_PERIODO: Result := '{CALL AFECTA_PERIODO(:Year, :Tipo, :Numero, :Usuario, :StatusViejo, :StatusNuevo, :Factor )}';
{$endif}
          Q_AFECTA_PERIODO_CUANTOS: Result := 'select COUNT(*) Cuantos from NOMINA where '+
                                              '( PE_YEAR = :Year ) and '+
                                              '( PE_TIPO = :Tipo ) and '+
                                              '( PE_NUMERO = :Numero ) and '+
                                              '( NO_STATUS = :StatusViejo )';

          //MA:03/ABR/2003: Query para la nueva Version
{$ifdef INTERBASE}
{        Q_AFECTA_PERIODO_CUANTOS: Result := 'select COUNT(*) from COLABORA ' +
                                            'where ( ( select RESULTADO from SP_STATUS_ACT ( %s, COLABORA.CB_CODIGO ) ) = 1 ) ';}
{$else}
{       Q_AFECTA_PERIODO_CUANTOS: Result := 'select COUNT(*) from COLABORA '+
                                           'where ( ( select DBO.SP_STATUS_ACT ( %s, COLABORA.CB_CODIGO ) ) = 1 ) ';}
{$endif}
          {//CV: nadie lo esta usando
          Q_STATUS_PERIODO: Result := 'select PE_STATUS from PERIODO where '+
                                      '( PE_YEAR = :Year ) and '+
                                      '( PE_TIPO = :Tipo ) and '+
                                      '( PE_NUMERO = :Numero )';
          }
          Q_DATOS_PERIODO: Result := 'select PE_NUMERO from PERIODO where ( PE_YEAR = %d ) and ( PE_TIPO = %d )';
Q_DEFINIR_PERIODOS_INSERT: Result := 'insert into PERIODO ( '+
                                               'PE_YEAR, '+
                                               'PE_TIPO, '+
                                               'PE_NUMERO, '+
                                               'PE_USO, '+
                                               'PE_STATUS, '+
                                               'PE_FEC_INI, '+
                                               'PE_FEC_FIN, '+
                                               'PE_FEC_PAG, '+
                                               {$ifdef QUINCENALES}
                                               'PE_ASI_INI, '+
                                               'PE_ASI_FIN, '+
                                               {$endif}
                                               'PE_DIAS, '+
                                               'PE_MES, '+
                                               'US_CODIGO, '+
                                               'PE_AHORRO, '+
                                               'PE_PRESTAM, '+
                                               'PE_SOLO_EX, '+
                                               'PE_INC_BAJ,'+
                                               'PE_CAL ) values ( '+
                                               ':PE_YEAR, '+
                                               ':PE_TIPO, '+
                                               ':PE_NUMERO, '+
                                               ':PE_USO, '+
                                               ':PE_STATUS, '+
                                               ':PE_FEC_INI, '+
                                               ':PE_FEC_FIN, '+
                                               ':PE_FEC_PAG, '+
                                               {$ifdef QUINCENALES}
                                               ':PE_ASI_INI, '+
                                               ':PE_ASI_FIN, '+
                                               {$endif}
                                               ':PE_DIAS, '+
                                               ':PE_MES, '+
                                               ':US_CODIGO, '+
                                               ':PE_AHORRO, '+
                                               ':PE_PRESTAM, '+
                                               ':PE_SOLO_EX, '+
                                               ':PE_INC_BAJ, '+
                                               ':PE_CAL )';
          Q_RECALCULO_DIAS_PERIODO: Result := 'select * from PERIODO where '+
                                               '( PE_YEAR = %d ) and '+
                                               '( PE_TIPO = %d ) and '+
                                               '( PE_MES >= 1 ) and '+
                                               '( PE_MES <= 12 ) and '+
                                               '( PE_NUMERO < %2:d ) '+
                                               'order by PE_MES, PE_NUMERO ';
          Q_RECALCULO_DIAS_UPDATE: Result := 'update PERIODO set '+
                                             'PE_DIAS_AC = :PE_DIAS_AC, '+
                                             'PE_DIA_MES = :PE_DIA_MES, '+
                                             'PE_POS_MES = :PE_POS_MES, '+
                                             'PE_PER_MES = :PE_PER_MES, '+
                                             'PE_PER_TOT = :PE_PER_TOT where '+
                                             '( PE_YEAR = :Year ) and '+
                                             '( PE_TIPO = :Tipo ) and ' +
                                             '( PE_NUMERO = :Numero ) ';
          Q_LIMPIA_ACUMULADOS_BORRA: Result := 'update ACUMULA set %s '+
                                               'where ( AC_YEAR = :Year ) %s '+
                                               'and ( CB_CODIGO = :Empleado )';
          Q_LIMPIA_ACUMULADOS_UPDATE: Result := 'update ACUMULA set AC_ANUAL = (  '+
                                                'AC_MES_01 + AC_MES_02 + AC_MES_03 + '+
                                                'AC_MES_04 + AC_MES_05 + AC_MES_06 + '+
                                                'AC_MES_07 + AC_MES_08 + AC_MES_09 + '+
                                                'AC_MES_10 + AC_MES_11 + AC_MES_12 + '+
                                                'AC_MES_13 ) where  '+
                                                '( AC_YEAR = :Year ) and  '+
                                                '( CB_CODIGO = :Empleado )';
{$ifdef INTERBASE}
          Q_DIFERENCIAS_AGREGA_NOMINA: Result := 'select ROTATIVO from SP_ADD_NOMINA( :YEAR, :TIPO, :NUMERO, :EMPLEADO )';
{$else}
          Q_DIFERENCIAS_AGREGA_NOMINA: Result := '{CALL SP_ADD_NOMINA( :YEAR, :TIPO, :NUMERO, :EMPLEADO, :ROTATIVO )}';
{$endif}

          Q_DIFERENCIAS_BORRA_NOMINA: Result := 'delete from NOMINA '+
                                                'where ( PE_YEAR = :Year ) '+
                                                'and ( PE_TIPO = :Tipo ) '+
                                                'and ( PE_NUMERO = :Numero ) ';
          Q_DIFERENCIAS_SET_STATUS: Result := 'update NOMINA set '+
                                              'NO_STATUS = :Status where '+
                                              '( PE_YEAR = :Year ) and '+
                                              '( PE_TIPO = :Tipo ) and '+
                                              '( PE_NUMERO = :Numero ) and '+
                                              '( CB_CODIGO = :Empleado )';
          // Q_RETROACTIVO_COMPARA_NOMINAS //
          {CV-CAMBIOS PARA ODBC}
          Q_DIFERENCIAS_COMPARA_NOMINAS: Result := 'select M1.CO_NUMERO, M1.MO_REFEREN, M1.MO_PERCEPC, '+
                                                   'M1.MO_DEDUCCI, M2.MO_PERCEPC PERCEPC, M2.MO_DEDUCCI DEDUCCI ' +
                                                   'from MOVIMIEN M1 left outer join MOVIMIEN M2 on ' +
                                                   '( M2.PE_YEAR = :YearNuevo ) and ' +
                                                   '( M2.PE_TIPO = :TipoNuevo ) and ' +
                                                   '( M2.PE_NUMERO = :NumeroNuevo ) and ' +
                                                   '( M2.CB_CODIGO = M1.CB_CODIGO ) and ' +
                                                   '( M2.CO_NUMERO = M1.CO_NUMERO ) and ' +
                                                   '( M2.MO_REFEREN = M1.MO_REFEREN ) where %s ' +
                                                   '( M1.PE_YEAR = :YearOriginal ) and ' +
                                                   '( M1.PE_TIPO = :TipoOriginal ) and ' +
                                                   '( M1.PE_NUMERO = :NumeroOriginal ) and ' +
                                                   '( M1.CB_CODIGO = :Empleado ) and ' +
                                                   '( M1.MO_ACTIVO = ''S'' ) and ' +
                                                   '( ( M1.MO_PERCEPC <> M2.MO_PERCEPC ) or '+
                                                   '( M1.MO_DEDUCCI <> M2.MO_DEDUCCI ) or '+
                                                   '( M2.CO_NUMERO is NULL ) ) and ' +
                                                   '( ( 1 > :Valor ) or ( M2.CO_NUMERO is NULL ) )';
          Q_DIFERENCIAS_INSERT: Result := 'insert into MOVIMIEN( '+
                                          'PE_YEAR, '+
                                          'PE_TIPO, '+
                                          'PE_NUMERO, '+
                                          'CB_CODIGO, '+
                                          'CO_NUMERO, '+
                                          'MO_REFEREN, '+
                                          'MO_PERCEPC, '+
                                          'MO_DEDUCCI, '+
                                          'MO_ACTIVO, '+
                                          'US_CODIGO ) values ( '+
                                          ':Year, '+
                                          ':Tipo, '+
                                          ':Numero, '+
                                          ':Empleado, '+
                                          ':Concepto, '+
                                          ':Referencia, '+
                                          ':Percepcion, '+
                                          ':Deduccion, '+
                                          ':Activo, '+
                                          ':Usuario )';
          Q_FOLIOS_SELECT_ORDEN: Result := 'select OF_CAMPO, OF_TITULO, OF_DESCEND ' +
                                           'from ORDFOLIO where ( FL_CODIGO = %d )' +
                                           'order by OF_POSICIO';
          Q_FOLIOS_UPDATE_CAMPO: Result := 'update NOMINA set NO_FOLIO_%d = :Valor where '+
                                           '( PE_YEAR = :YEAR ) and '+
                                           '( PE_TIPO = :TIPO ) and '+
                                           '( PE_NUMERO = :NUMERO ) and '+
                                           '( CB_CODIGO = :Empleado )';
          Q_FOLIOS_SELECT: Result := 'select F.FL_DESCRIP, F.FL_MONTO, F.FL_REPITE, '+
                                     'F.FL_MONEDA, F.FL_CEROS, F.FL_INICIAL, F.FL_FINAL, '+
                                     'F.FL_FILTRO, F.QU_CODIGO from FOLIO F where '+
                                     '( F.FL_CODIGO = %d ) ';
          Q_FOLIOS_UPDATE_LIMITES: Result := 'update FOLIO set '+
                                             'FL_INICIAL = :Inicial, '+
                                             'FL_FINAL = :Final '+
                                             'where ( FL_CODIGO = :Folio )';
          Q_REFOLIAR_UPDATE: Result := 'update NOMINA set %s = :Folio where ' +
                                       '( PE_YEAR = :Year ) and ' +
                                       '( PE_TIPO = :Tipo ) and ' +
                                       '( PE_NUMERO = :Numero ) and ' +
                                       '( CB_CODIGO = :Empleado ) and ' +
                                       '%s = :no_folio_1 ';
          Q_PAGAR_FUERA_UPDATE: Result := 'update NOMINA set NO_FUERA = ''S'' where '+
                                          '( PE_YEAR = %d ) and '+
                                          '( PE_TIPO = %d ) and '+
                                          '( PE_NUMERO = %d ) and '+
                                          '( CB_CODIGO = :Empleado )';
          Q_LIQUIDACION_HAY_NOMINA: Result := 'select NOMINA.PE_NUMERO from NOMINA left outer join PERIODO on '+
                                              '( PERIODO.PE_YEAR = NOMINA.PE_YEAR ) and '+
                                              '( PERIODO.PE_TIPO = NOMINA.PE_TIPO ) and '+
                                              '( PERIODO.PE_NUMERO = NOMINA.PE_NUMERO ) where '+
                                              '( NOMINA.PE_YEAR = :Year ) and '+
                                              '( NOMINA.PE_TIPO = :Tipo ) and '+
                                              '( NOMINA.PE_NUMERO <> :Simulacion ) and '+
                                              '( NOMINA.CB_CODIGO = :Empleado ) and '+
                                              '( NOMINA.NO_LIQUIDA > 0 ) and '+
                                              '( PERIODO.PE_FEC_FIN >= :Fecha ) and ' +
                                              '( ( not :FechaIngreso between PERIODO.PE_FEC_INI and PERIODO.PE_FEC_FIN ) or ' +
                                              '( :FechaBaja between PERIODO.PE_FEC_INI and PERIODO.PE_FEC_FIN ) )';
          Q_LIQUIDACION_NOMINA: Result := 'select NO_HORAS, NO_DOBLES, NO_TRIPLES, '+
                                          'NO_TARDES, NO_ADICION, NO_HORA_PD, NO_LIQUIDA, '+
                                          'NO_DIAS_VA, NO_DIAS_AG, NO_OBSERVA '+
                                          'from NOMINA where '+
                                          '( NOMINA.PE_YEAR = :Year ) and '+
                                          '( NOMINA.PE_TIPO = :Tipo ) and '+
                                          '( NOMINA.PE_NUMERO = :Numero ) and '+
                                          '( NOMINA.CB_CODIGO = :Empleado )';
          Q_LIQUIDACION_AHORROS: Result := 'select A.AH_TIPO, A.AH_SALDO, ''S'' as SALDAR, '+
                                           'T.TB_ELEMENT, T.TB_LIQUIDA, T.TB_RELATIV, T.TB_CONCEPT '+
                                           'from AHORRO A join TAHORRO T on ( T.TB_CODIGO = A.AH_TIPO ) where '+
                                           '( A.AH_STATUS = 0 ) and '+
                                           '( A.AH_SALDO <> 0 ) and '+
                                           '( T.TB_LIQUIDA > 0 ) and '+
                                           '( A.CB_CODIGO = %s )';
          Q_LIQUIDACION_PRESTAMOS: Result := 'select P.PR_TIPO, P.PR_SALDO, P.PR_REFEREN, ''S'' as SALDAR, '+
                                             'T.TB_ELEMENT, T.TB_LIQUIDA, T.TB_CONCEPT '+
                                             'from PRESTAMO P join TPRESTA T on ( T.TB_CODIGO = P.PR_TIPO ) where '+
                                             '( P.PR_STATUS = 0 ) and '+
                                             '( P.PR_SALDO <> 0 ) and '+
                                             '( T.TB_LIQUIDA > 0 ) and '+
                                             '( P.CB_CODIGO = %s )';
          Q_LIQUIDACION_VACACIONES: Result := 'select CB_DER_FEC, CB_V_PAGO, CB_V_GOZO, CB_V_PRIMA,'+
                                              'CB_DER_PAG, CB_DER_GOZ, CB_DER_PV, CB_TABLASS, CB_FEC_ING, CB_FEC_BAJ, CB_FEC_ANT '+
                                              'from COLABORA where ( CB_CODIGO = :Empleado )';
          Q_LIQUIDACION_AGUINALDO: Result := 'select PT_DIAS_AG from PRESTACI where '+
                                             '( TB_CODIGO = :Tabla ) and ( PT_YEAR >= :Year ) '+
                                             'order by PT_YEAR';
          Q_LIQUIDACION_AGUINALDO_EMP: Result := 'select AC_ANUAL from ACUMULA where '+
                                                 '( CO_NUMERO = 1115 ) and ( CB_CODIGO = :Empleado ) and ' +
                                                 '( AC_YEAR = :Year )';
{
          Q_EMPLEADO_STATUS: Result := 'select A.CB_ACTIVO, A.CB_FEC_BAJ, A.CB_FEC_ING, A.CB_NOMYEAR, A.CB_NOMTIPO, A.CB_NOMNUME, B.TU_NOMINA '+
                                       'from COLABORA A ' +
                                       'left outer join TURNO B on A.CB_TURNO = B.TU_CODIGO ' +
                                       'where ( A.CB_CODIGO = :Empleado ) %s';
}
          Q_EMPLEADO_STATUS: Result := 'select A.CB_ACTIVO, A.CB_FEC_BAJ, A.CB_FEC_ING, A.CB_NOMYEAR, A.CB_NOMTIPO, A.CB_NOMNUME, A.CB_FEC_NOM, ' +
{$ifdef INTERBASE}
                                       '( select TIPNOM from SP_GET_NOMTIPO( %0:s, A.CB_CODIGO )  ) TU_NOMINA '+
{$endif}
{$ifdef MSSQL}
                                       '( select DBO.SP_GET_NOMTIPO( %0:s, A.CB_CODIGO ) ) TU_NOMINA ' +
{$endif}
                                       'from COLABORA A ' +
                                       'where ( A.CB_CODIGO = :Empleado ) %s';
          Q_CONCEPTO_TIPO: Result := 'select CO_TIPO from CONCEPTO where ( CO_NUMERO = :Concepto )';
          Q_NOMINA_COPIA: Result := 'execute procedure SP_COPIA_NOMINA ('+
                                    ':YEARORIGINAL, '+
                                    ':TIPOORIGINAL, '+
                                    ':NUMEROORIGINAL, '+
                                    ':YEARNUEVO, '+
                                    ':TIPONUEVO, '+
                                    ':NUMERONUEVO, '+
                                    ':EMPLEADO )';
          Q_ACUMULADO_EXISTE: Result := 'select %s from ACUMULA where '+
                                        '( AC_YEAR = :AC_YEAR ) and '+
                                        '( CB_CODIGO = :CB_CODIGO ) and '+
                                        '( CO_NUMERO = :CO_NUMERO )';
          Q_ACUMULADO_AGREGA: Result := 'insert into ACUMULA( ' +
                                        'AC_YEAR, CB_CODIGO, CO_NUMERO, AC_ANUAL, %s ) values ( ' +
                                        ':AC_YEAR, :CB_CODIGO, :CO_NUMERO, :AC_ANUAL, :AC_MES )';
          Q_ACUMULADO_MODIFICA: Result := 'update ACUMULA set %s = :AC_MES ' +
                                          'where ( AC_YEAR = :AC_YEAR ) and ' +
                                          '( CB_CODIGO = :CB_CODIGO ) and ' +
                                          '( CO_NUMERO = :CO_NUMERO )';
          Q_ACUMULADO_MODIFICA_ANUAL: Result := 'update ACUMULA set AC_ANUAL = AC_MES_01 + ' +
                                                'AC_MES_02 + AC_MES_03 + AC_MES_04 + AC_MES_05 + ' +
                                                'AC_MES_06 + AC_MES_07 + AC_MES_08 + AC_MES_09 + ' +
                                                'AC_MES_10 + AC_MES_11 + AC_MES_12 + AC_MES_13 ' +
                                                'where ( AC_YEAR = :AC_YEAR ) and ' +
                                                '( CB_CODIGO = :CB_CODIGO ) and ' +
                                                '( CO_NUMERO = :CO_NUMERO )';
          Q_EXPORTAR_FALTAS: Result := 'select FALTAS.CB_CODIGO, FA_DIA_HOR, FA_MOTIVO, FA_DIAS, FA_HORAS, FA_FEC_INI '+
                                       'from FALTAS ' +
                                       'left outer join COLABORA on COLABORA.CB_CODIGO = FALTAS.CB_CODIGO ' +
                                       'where ( PE_YEAR = :Year ) and '+
                                       '( PE_TIPO = :Tipo ) and '+
                                       '( PE_NUMERO = :Numero ) %s ' +
                                       'order by FALTAS.CB_CODIGO';
          Q_EXPORTAR_MOVIMIENTOS_TODOS: Result := 'select MOVIMIEN.CB_CODIGO, CO_NUMERO, MO_PERCEPC, MO_DEDUCCI, MO_REFEREN '+
                                                  'from MOVIMIEN '+
                                                  'left outer join COLABORA on COLABORA.CB_CODIGO = MOVIMIEN.CB_CODIGO ' +
                                                  'where ( PE_YEAR = :Year ) and '+
                                                  '( PE_TIPO = :Tipo ) and '+
                                                  '( PE_NUMERO = :Numero ) %s ' +
                                                  'order by MOVIMIEN.CB_CODIGO, CO_NUMERO, MO_REFEREN';
          Q_EXPORTAR_MOVIMIENTOS_EXCEP: Result := 'select MOVIMIEN.CB_CODIGO, CO_NUMERO, MO_PERCEPC, MO_DEDUCCI, MO_REFEREN '+
                                                  'from MOVIMIEN '+
                                                  'left outer join COLABORA on COLABORA.CB_CODIGO = MOVIMIEN.CB_CODIGO ' +
                                                  'where ( PE_YEAR = :Year ) and '+
                                                  '( PE_TIPO = :Tipo ) and '+
                                                  '( PE_NUMERO = :Numero ) and ' +
                                                  '( MOVIMIEN.US_CODIGO > 0 ) %s '+
                                                  'order by MOVIMIEN.CB_CODIGO, CO_NUMERO, MO_REFEREN';
          Q_FALTAS_SUSTITUIR: Result := 'update FALTAS set FA_DIAS = :Dias, FA_HORAS = :Horas '+
                                        K_WHERE_FALTAS;
          Q_FALTAS_SUMAR: Result := 'update FALTAS set FA_DIAS = FA_DIAS + :Dias, FA_HORAS = FA_HORAS + :Horas '+
                                    K_WHERE_FALTAS;
          Q_FALTAS_BORRAR: Result := 'delete from FALTAS ' + K_WHERE_FALTAS;
          {CV-CAMBIOS PARA ODBC}
          Q_FALTAS_EXISTE: Result := 'select FA_DIAS + FA_HORAS FA_DIAS_HORAS from FALTAS ' + K_WHERE_FALTAS;
          Q_MONTOS_SUSTITUIR: Result := 'update MOVIMIEN set MO_PERCEPC = :Percepcion, MO_DEDUCCI = :Deduccion, US_CODIGO = :Usuario ' +
                                         K_WHERE_MONTOS;
          Q_MONTOS_SUMAR: Result := 'update MOVIMIEN set MO_PERCEPC = MO_PERCEPC + :Percepcion, MO_DEDUCCI = MO_DEDUCCI + :Deduccion, US_CODIGO = :Usuario ' +
                                    K_WHERE_MONTOS;
          Q_MONTOS_BORRAR: Result := 'delete from MOVIMIEN ' + K_WHERE_MONTOS;
          {CV-CAMBIOS PARA ODBC}
          Q_MONTOS_EXISTE: Result := 'select MO_PERCEPC + MO_DEDUCCI MO_MONTO from MOVIMIEN ' + K_WHERE_MONTOS;
          Q_PRE_NOMINA: Result := 'select CB_CODIGO, AU_FECHA, AU_DES_TRA, AU_DOBLES, AU_EXTRAS, '+
                                  'AU_HORAS, AU_PER_CG, AU_PER_SG, AU_TARDES, AU_TIPO, AU_TIPODIA, '+
{$ifdef QUINCENALES}
                                  'AU_HORASNT, ' +
{$endif}
                                  'CB_CLASIFI, CB_TURNO, CB_PUESTO, HO_CODIGO, '+
                                  'CB_NIVEL1, CB_NIVEL2, CB_NIVEL3, CB_NIVEL4, CB_NIVEL5, CB_NIVEL6, CB_NIVEL7, CB_NIVEL8, CB_NIVEL9, '+
                                  {$ifdef ACS}'CB_NIVEL10, CB_NIVEL11, CB_NIVEL12, '+{$endif}
                                  'AU_AUT_EXT, AU_AUT_TRA, AU_HOR_MAN, AU_STATUS, AU_NUM_EXT, '+
                                  'AU_TRIPLES, US_CODIGO, AU_POSICIO, AU_HORASCK, PE_YEAR, PE_TIPO, PE_NUMERO, ' +
{$ifdef INTERBASE}
                                  '( select RESULTADO from SP_CHECADAS( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 1 ) ) CHECADA1, ' +
                                  '( select RESULTADO from SP_CHECADAS( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 2 ) ) CHECADA2, ' +
                                  '( select RESULTADO from SP_CHECADAS( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 3 ) ) CHECADA3, ' +
                                  '( select RESULTADO from SP_CHECADAS( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 4 ) ) CHECADA4 ' +
{$endif}
{$ifdef MSSQL}
                                  'dbo.SP_CHECADAS( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 1 ) CHECADA1, ' +
                                  'dbo.SP_CHECADAS( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 2 ) CHECADA2, ' +
                                  'dbo.SP_CHECADAS( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 3 ) CHECADA3, ' +
                                  'dbo.SP_CHECADAS( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO, 4 ) CHECADA4 ' +
{$endif}
                                  'from AUSENCIA where ' +
                                  '( CB_CODIGO = %d ) and ( AU_FECHA between ''%s'' and ''%s'' ) ' +

{$ifdef CAMBIO_TNOM}
                                  'AND ( CB_NOMINA = %d )'+

{$endif}
                                  'order by AU_FECHA';
          Q_NOMINA_CANCELA: Result := 'execute procedure SP_CANCELA_NOMINA ('+
                                      ':YEARORIGINAL, '+
                                      ':TIPOORIGINAL, '+
                                      ':NUMEROORIGINAL, '+
                                      ':YEARNUEVO, '+
                                      ':TIPONUEVO, '+
                                      ':NUMERONUEVO, '+
                                      ':EMPLEADO, '+
                                      ':USUARIO )';
          Q_ENCABEZADO_PERIODO : Result := 'select PE_STATUS, PE_FEC_INI, PE_FEC_FIN from PERIODO where '+
                                           '( PE_YEAR = :Year ) and ( PE_TIPO = :Tipo ) and ( PE_NUMERO = :Numero )';
          Q_EXCEP_GLOBAL_SET_STATUS: Result := 'update NOMINA set '+
                                               'NO_STATUS = %d, ' +
                                               'NO_APROBA = %d where '+
                                               '( NO_STATUS > %d ) and '+
                                               '( PE_YEAR = %d ) and '+
                                               '( PE_TIPO = %d ) and '+
                                               '( PE_NUMERO = %d )';
                    //MA:Se agrego el siguiente query para obtener el usuario para registro en bit�cora
          Q_BITACORA_BORRA_NOMINA: Result := 'select CB_CODIGO from NOMINA where ( PE_YEAR = %d ) and ( PE_TIPO = %d ) and ( PE_NUMERO = %d )';
{$ifdef QUINCENALES}
          Q_DATOS_EMPLEADO_PRENOMINA: Result := 'select CB_FEC_ING, CB_FEC_BAJ from COLABORA where ( CB_CODIGO = %d )';
{$endif}
          Q_SET_MOVIMIEN_EXCEPCIONES: Result := 'update MOVIMIEN set ' +
                                                'US_CODIGO = %d where ' +
                                                '( PE_YEAR = %d ) and '+
                                                '( PE_TIPO = %d ) and '+
                                                '( PE_NUMERO = %d ) and ' +
                                                '( CB_CODIGO = %d )';
          {$ifndef DOS_CAPAS}
          Q_MONTOS_RETENIDOS: Result:= '( select SUM( MO_PERCEPC + MO_DEDUCCI )'+
                                        ' from MOVIMIEN'+
                                        ' where ( CB_CODIGO = %0:s ) and ' +
                                        '( CO_NUMERO = ( %1:s ) ) and '+
                                        '( MO_REFEREN = %2:s ) and ' +
                                        '( PE_YEAR = %3:d ) and ' +
                                        '( %s ) )';
          {$else}
          Q_MONTOS_RETENIDOS: Result:= '( select SUM( MO_PERCEPC + MO_DEDUCCI )'+
                                        ' from MOVIMIEN'+
                                        ' where ( PE_YEAR = %3:d ) and '+
                                        '( %4:s ) and ' +
                                        '( CB_CODIGO = %0:s ) and ' +
                                        '( CO_NUMERO = ( %1:s ) ) and ' +
                                        '( MO_REFEREN = %2:s ) )';
          {$endif}
          Q_MONTOS_RETENIDOS_EMP: Result:= ' select SUM( MO_PERCEPC + MO_DEDUCCI ) RETENCION'+
                                        ' from MOVIMIEN'+
                                        ' where ( PE_YEAR = :PE_YEAR ) and ' +
                                        '( %0:s ) and '+
                                        '( CB_CODIGO = :CB_CODIGO ) and ' +
                                        '( CO_NUMERO = :CO_NUMERO ) and '+
                                        '( MO_REFEREN = :MO_REFEREN ) ';

         // {$else}
          {Q_MONTOS_RETENIDOS: Result:= '( select SUM( MO_PERCEPC + MO_DEDUCCI )'+
                                        ' from MOVIMIEN'+
                                        ' where ( PE_YEAR = %3:d ) and '+
                                        ' ( %s ) and '+
                                        ' ( CB_CODIGO = %0:s ) and ' +
                                        ' ( CO_NUMERO = ( %1:s ) ) and ' +
                                        ' ( MO_REFEREN = %2:s ) ';}

           {select COUNT(*), SUM( M.MO_DEDUCCI ) from MOVIMIEN M where
                      ( M.PE_YEAR = :Anio ) and
                      ( M.PE_TIPO = :Tipo ) and
                      ( M.PE_NUMERO = :Numero ) and
                      ( M.CB_CODIGO= :Empleado ) and
                      ( M.CO_NUMERO = :Concepto ) and
                      ( M.MO_ACTIVO = 'S' ) and
                      ( M.MO_REFEREN = :Referencia ) }
          // {$ENDIF}

          Q_REEMPLAZAR_AJUSTE: Result:= '( select COUNT(*)' +
                                        ' from PCAR_ABO' +
                                        ' where ( CB_CODIGO = %s ) and ' +
                                        '( PR_TIPO = %s ) and ' +
                                        '( PR_REFEREN = %s ) and ' +
                                        '( CR_FECHA = %s ) ) = 0';

          Q_MONTOS_OTROS_AJUSTES:  Result:= '( select SUM( CR_CARGO - CR_ABONO )'+
                                            ' from PCAR_ABO'+
                                            ' where ( CB_CODIGO = %s ) and ' +
                                            '( PR_TIPO = %s ) and ' +
                                            '( PR_REFEREN = %s ) and '+
                                            '( CR_FECHA between %s and %s ) )';

          Q_MONTO_AJUSTE_ULTIMO_MES: Result:= '( select CR_CARGO - CR_ABONO '+
                                              ' from PCAR_ABO'+
                                              ' where ( CB_CODIGO = %s ) and '+
                                              '( PR_TIPO = %s ) and ' +
                                              '( PR_REFEREN = %s ) and ' +
                                              '( CR_FECHA = %s ) )';

         {$ifdef MSSQL}
         Q_SALDO_ACTUAL_AJUSTE: Result:= '( select PRESTAMOAJUS.PR_SALDO'+
                                            ' from PRESTAMO PRESTAMOAJUS'+
                                            ' where ( PRESTAMOAJUS.CB_CODIGO = %s ) and ' +
                                            '( PRESTAMOAJUS.PR_TIPO = %s ) and ' +
                                            '( PRESTAMOAJUS.PR_REFEREN = %s ) )';
         {$endif}

         {$ifdef INTERBASE }
         Q_SALDO_ACTUAL_AJUSTE: Result:= 'select PRESTAMOAJUS.PR_SALDO SALDO'+
                                            ' from PRESTAMO PRESTAMOAJUS'+
                                            ' where ( PRESTAMOAJUS.CB_CODIGO = :CB_CODIGO ) and ' +
                                            '( PRESTAMOAJUS.PR_TIPO = :PR_TIPO ) and ' +
                                            '( PRESTAMOAJUS.PR_REFEREN = :PR_REFEREN ) ';

         {$endif}



         Q_FILTRO_PERIODO_MEN: Result:=  'select PE_NUMERO, PE_TIPO from periodo where ( PE_YEAR = %0:d ) and ( PE_MES = %1:d ) ' +
                                     'and ( PE_STATUS = %2:d ) order by PE_TIPO, PE_NUMERO ';


         Q_FILTRO_X_TIPNOM: Result:= ' select PE_NUMERO, PE_TIPO, PE_YEAR, PE_STATUS, PE_MES from PERIODO where ' +
                                      '( PE_YEAR = %0:d ) AND ( PE_STATUS = %1:d ) AND ( %2:s  OR ( PE_NUMERO >= %3:d and PE_MES = %4:d ) ) ';

         Q_FILTRO_X_TIPNOM_FEC: Result:= ' ( select %0:s from PERIODO where ( PERIODO.PE_TIPO = COLABORA.CB_NOMINA ) AND %1:s ) )';

         {Q_FILTRO_X_TIPNOM: Result:= ' select PE_NUMERO, PE_TIPO from PERIODO where ( ( PE_YEAR = %0:d AND PE_STATUS = %1:d AND PE_TIPO = %2:d ) and '+
                                     '( ( PE_NUMERO >= %3:d and PE_NUMERO <= %4:d ) OR ( PE_NUMERO > %5:d and PE_MES = %6:d  ) ) ) ';  }

         Q_FILTRO_COL_NOMINA: Result:= '(  select SUM( %s ) from NOMINA where NOMINA.CB_CODIGO = COLABORA.CB_CODIGO and PE_YEAR = %d and ( %s ) ) ';

         Q_FILTRO_NIVEL_CONFI: Result:= 'select PE_YEAR,PE_TIPO,PE_NUMERO,PE_DESCRIP,PE_USO,PE_STATUS,PE_INC_BAJ,PE_SOLO_EX,PE_FEC_INI,PE_FEC_FIN, '+
                                        {$ifdef QUINCENALES}
                                        'PE_ASI_INI,PE_ASI_FIN, ' +
                                        {$endif}
                                        'PE_FEC_PAG,PE_MES,PE_DIAS,PE_DIAS_AC,PE_DIA_MES,PE_POS_MES,PE_PER_MES,PE_PER_TOT,PE_FEC_MOD, '+
                                        'PE_AHORRO,PE_PRESTAM,PE_LOG,US_CODIGO, '+
                                        '(select SUM(N.NO_NETO) from Nomina N where N.PE_TIPO = P.PE_TIPO and P.PE_YEAR = N.PE_YEAR and P.PE_NUMERO = N.PE_NUMERO %3:s )AS PE_TOT_NET ,'+
			                '(select SUM(N.NO_DEDUCCI) from Nomina N where N.PE_TIPO = P.PE_TIPO and P.PE_YEAR = N.PE_YEAR and P.PE_NUMERO = N.PE_NUMERO %3:s )AS PE_TOT_DED, '+
			                '(select SUM(N.NO_PERCEPC) from Nomina N where N.PE_TIPO = P.PE_TIPO and P.PE_YEAR = N.PE_YEAR and P.PE_NUMERO = N.PE_NUMERO %3:s )AS PE_TOT_PER, '+
			                '(select COUNT(N.CB_CODIGO) from Nomina N where N.PE_TIPO = P.PE_TIPO and P.PE_YEAR = N.PE_YEAR and P.PE_NUMERO = N.PE_NUMERO %3:s )AS PE_NUM_EMP '+
                                        'from PERIODO P where P.PE_TIPO = %0:d AND P.PE_YEAR = %1:d AND P.PE_NUMERO = %2:d ';
         Q_EMP_INCA:          Result:= {OP: 12/06/08}
                              {$ifdef INTERBASE}
                                      'select RESULTADO from SP_EMP_INCA( %s, %s, %d )';
                              {$endif}
                              {$ifdef MSSQL}
                                     'select dbo.SP_EMP_INCA( %s, %s, %d ) as RESULTADO';
                              {$endif}
         Q_GET_SIM_GLOBALES : Result := 'select N.CB_CODIGO, '+K_PRETTYNAME + ' as PrettyName '+',C.CB_ACTIVO,C.CB_FEC_ANT,C.CB_SALARIO,N.PE_TIPO,N.PE_NUMERO,N.PE_YEAR,N.NO_PERCEPC,N.NO_DEDUCCI,N.NO_NETO,T.TP_NOMBRE,N.NO_STATUS,NO_APROBA,NO_FEC_LIQ '+
                                        ' FROM NOMINA N '+
                                        ' left outer join COLABORA C on C.CB_CODIGO = N.CB_CODIGO '+
                                        ' left outer join TPERIODO T on T.TP_TIPO = N.PE_TIPO '+
                                        ' where N.PE_YEAR = %d and '+
                                        ' N.PE_NUMERO = %d and '+
                                        ' N.NO_GLOBAL = ''%s''' +
                                        ' order by N.CB_CODIGO ';
         Q_GET_SIM_TOTALES_CONCEPTOS : Result := ' select CO.CO_NUMERO,CO.CO_DESCRIP,sum(M.MO_PERCEPC)as PERCEPCIONES ,sum(M.MO_DEDUCCI) as DEDUCCIONES,'+
                                                 ' CO.CO_TIPO,SUM(M.MO_IMP_CAL)as IMPUESTOS_CAL,SUM(M.MO_X_ISPT)as IMPUESTOS_ISPT from MOVIMIEN M'+
                                                 ' left outer join CONCEPTO CO on CO.CO_NUMERO = M.CO_NUMERO'+
                                                 ' left outer join NOMINA N on N.PE_YEAR = M.PE_YEAR and N.PE_TIPO = M.PE_TIPO and N.PE_NUMERO = M.PE_NUMERO and N.CB_CODIGO = M.CB_CODIGO '+
                                                 ' where M.PE_YEAR = %d'+
                                                 ' and M.PE_NUMERO = %d'+
                                                 ' and N.NO_GLOBAL = ''%s'''+
                                                 ' %s ' +
                                                 ' group by CO.CO_NUMERO,CO.CO_DESCRIP,CO.CO_TIPO';
         Q_GET_SIM_TOTALES_NOMINA : Result := ' select '+
                                              //Totales
                                              ' sum(NO_PERCEPC)as PERCEPCIONES, '+
                                              ' sum(NO_DEDUCCI)as DEDUCCIONES, '+
                                              ' sum(NO_NETO)as NETO, '+
                                              //Impuestos
                                              ' sum(NO_X_ISPT)as ISPT_SUM,'+
                                              ' sum(NO_X_MENS)as MENS_SUM,'+
                                              ' sum(NO_X_CAL)as CAL_SUM, '+
                                              ' sum(NO_IMP_CAL)as IMP_CAL_SUM,'+
                                              ' sum(NO_PER_MEN)as PER_MEN_SUM,'+
                                              ' sum(NO_PER_CAL)as PER_CAL_SUM, '+
                                              ' sum(NO_TOT_PRE)as TOT_PRE_SUM, '+
                                              ' sum(NO_PER_ISN)as PER_ISN_SUM, '+
                                              //Dias
                                              ' sum(NO_DIAS )as DIAS_SUM, '+
                                              ' sum(NO_DIAS_AS)as DIAS_AS_SUM, '+
                                              ' sum(NO_DIAS_NT)as DIAS_NT_SUM, '+
                                              ' sum(NO_DIAS_SU)as DIAS_SU_SUM, '+
                                              ' sum(NO_DIAS_IN)as DIAS_INI_SUM, '+
                                              ' sum(NO_DIAS_SG)as DIAS_SG_SUM, '+
                                              ' sum(NO_DIAS_CG)as DIAS_CG_SUM, '+
                                              ' sum(NO_DIAS_OT)as DIAS_OT_SUM, '+
                                              ' sum(NO_DIAS_AJ)as DIAS_AJ_SUM, '+
                                              ' sum(NO_DIAS_RE)as DIAS_RE_SUM, '+
                                              ' sum(NO_DIAS_SS)as DIAS_SS_SUM, '+
                                              ' sum(NO_DIAS_EM)as DIAS_EM_SUM, '+
                                              ' sum(NO_DIAS_FI)as DIAS_FI_SUM, '+
                                              ' sum(NO_DIAS_FJ)as DIAS_FJ_SUM, '+
                                              ' sum(NO_DIAS_FV)as DIAS_FV_SUM, '+
                                              ' sum(NO_DIAS_VA)as DIAS_VA_SUM, '+
                                              ' sum(NO_DIAS_AG)as DIAS_AG_SUM, '+
                                              //Horas
                                              ' sum(NO_HORAS)as HORAS_SUM, '+
                                              ' sum(NO_EXTRAS)as EXTRAS_SUM, '+
                                              ' sum(NO_DOBLES)as DOBLES_SUM, '+
                                              ' sum(NO_TRIPLES)as TRIPLES_SUM, '+
                                              ' sum(NO_TARDES)as TARDES_SUM, '+
                                              ' sum(NO_ADICION)as ADICION_SUM, '+
                                              ' sum(NO_HORA_PD)as HORA_PD_SUM, '+
                                              ' sum(NO_DES_TRA)as DES_TRA_SUM, '+
                                              ' sum(NO_FES_TRA)as FES_TRA_SUM, '+
                                              ' sum(NO_VAC_TRA)as VAC_TRA_SUM, '+
                                              ' sum(NO_HORA_CG)as HORA_CG_SUM, '+
                                              ' sum(NO_HORA_SG)as HORA_SG_SUM, '+
                                              ' sum(NO_FES_PAG)as FES_PAG_SUM '+
                                              ' from NOMINA N'+
                                              ' where PE_YEAR = %d'+
                                              ' and PE_NUMERO = %d'+
                                              ' and NO_GLOBAL =''%s'''+
                                              ' %s';
         Q_GET_SIM_A_APROBAR : Result := 'select N.CB_CODIGO,N.PE_NUMERO,N.PE_TIPO,N.PE_YEAR, '+ K_PRETTYNAME + ' as PrettyName ' +',C.CB_ACTIVO,N.NO_STATUS,N.NO_APROBA '+
                                         ' FROM NOMINA N '+
                                         ' left outer join COLABORA C on C.CB_CODIGO = N.CB_CODIGO '+
                                         ' left outer join TPERIODO T on T.TP_TIPO = N.PE_TIPO '+
                                         ' where N.PE_YEAR = %d and '+
                                         ' N.PE_NUMERO = %d and '+
                                         ' N.NO_GLOBAL = ''%s'' and' +
                                         ' N.NO_APROBA <> %d and'+
                                         ' N.NO_STATUS = %d and'+
                                         ' C.CB_ACTIVO = ''%s'' '+
                                         ' order by N.CB_CODIGO ';
         Q_UPDATE_APLICADOS : Result := 'Update NOMINA set NO_APROBA = %d WHERE PE_YEAR = :Year and PE_NUMERO = :Numero and PE_TIPO = :Tipo and CB_CODIGO = :Empleado ';

         Q_CONCEPTO: Result := 'select CO_VER_INF,CO_VER_SUP,CO_LIM_SUP,CO_LIM_INF from CONCEPTO where CO_NUMERO = :Concepto ';

  //SP_TIMBRAR_EMPLEADO( @PE_YEAR Anio, @PE_TIPO NominaTipo, @PE_NUMERO NominaNumero, @CB_CODIGO NumeroEmpleado, @CB_RFC Descripcion, @Status status, @Timbre Formula, @FolioTimbramex FolioGrande, @Usuario usuario )
{$ifdef INTERBASE}
          Q_TIMBRAR_EMPLEADO:   Result := 'execute procedure SP_TIMBRAR_EMPLEADO( :Year, :Tipo, :Numero, :Empleado, :RFC,  :Status, :Timbre, :Folio, :Usuario, :FacturaUUID )';
{$else}
         Q_TIMBRAR_EMPLEADO :  Result := '{CALL SP_TIMBRAR_EMPLEADO(:Year, :Tipo, :Numero, :Empleado, :RFC,  :Status, :Timbre, :Folio, :Usuario ,:FacturaUUID)}';

{$endif}
{
AFECTAR_STATUS_TIMBRADO( @PE_YEAR Anio, @PE_TIPO NominaTipo, @PE_NUMERO NominaNumero, @StatusActual status, @StatusNuevo status ) }
{$ifdef INTERBASE}
         Q_TIMBRAR_PERIODO:   Result := 'execute procedure AFECTAR_STATUS_TIMBRADO( :Year, :Tipo, :Numero, :StatusActual, :StatusNuevo )';
{$else}
         Q_TIMBRAR_PERIODO :  Result := '{CALL AFECTAR_STATUS_TIMBRADO(:Year, :Tipo, :Numero, :StatusActual, :StatusNuevo)}';
{$endif}
     Q_CAMPOS_VISTA_TRANSFERIDOS :  Result := 'select ' + K_PRETTYNAME + ' as PrettyName, ' +
                       'CB_CODIGO,CB_ACTIVO,CB_APE_MAT,CB_APE_PAT,CB_AUTOSAL,CB_BAN_ELE,CB_CARRERA,CB_CHECA,CB_CIUDAD,CB_CLASIFI,CB_CODPOST,CB_CONTRAT,CB_CREDENC,CB_CURP,CB_CALLE,'+
                       'CB_COLONIA,CB_EDO_CIV,CB_FEC_RES,CB_EST_HOR,CB_EST_HOY,CB_ESTADO,CB_ESTUDIO,CB_EVALUA,CB_EXPERIE,CB_FEC_ANT,CB_FEC_BAJ,CB_FEC_BSS,CB_FEC_CON,CB_FEC_ING,CB_FEC_INT,'+
                       'CB_FEC_NAC,CB_FEC_REV,CB_FEC_VAC,CB_G_FEC_1,CB_G_FEC_2,CB_G_FEC_3,CB_G_LOG_1,CB_G_LOG_2,CB_G_LOG_3,CB_G_NUM_1,CB_G_NUM_2,CB_G_NUM_3,CB_G_TAB_1,CB_G_TAB_2,CB_G_TAB_3,'+
                       'CB_G_TAB_4,CB_G_TEX_1,CB_G_TEX_2,CB_G_TEX_3,CB_G_TEX_4,CB_HABLA,CB_TURNO,CB_IDIOMA,CB_INFCRED,CB_INFMANT,CB_INFTASA,CB_LA_MAT,CB_LAST_EV,CB_LUG_NAC,CB_MAQUINA,'+
                       'CB_MED_TRA,CB_PASAPOR,CB_FEC_INC,CB_FEC_PER,CB_NOMYEAR,CB_NACION,CB_NOMTIPO,CB_NEXT_EV,CB_NOMNUME,CB_NOMBRES,CB_PATRON,CB_PUESTO,CB_RFC,CB_SAL_INT,CB_SALARIO,'+
                       'CB_SEGSOC,CB_SEXO,CB_TABLASS,CB_TEL,CB_VIVECON,CB_VIVEEN,CB_ZONA,CB_ZONA_GE,CB_NIVEL1,CB_NIVEL2,CB_NIVEL3,CB_NIVEL4,CB_NIVEL5,CB_NIVEL6,CB_NIVEL7,CB_INFTIPO,'+
                       'CB_NIVEL8,CB_NIVEL9,'+
                       {$ifdef ACS}'CB_NIVEL10, CB_NIVEL11, CB_NIVEL12,'+{$endif}
                       'CB_DER_FEC,CB_DER_PAG,CB_V_PAGO,CB_DER_GOZ,CB_V_GOZO,CB_TIP_REV,CB_MOT_BAJ,CB_FEC_SAL,CB_INF_INI,CB_INF_OLD,CB_OLD_SAL,CB_OLD_INT,CB_PRE_INT,'+
                       'CB_PER_VAR, CB_SAL_TOT, CB_TOT_GRA, CB_FAC_INT, CB_RANGO_S, CB_CLINICA, CB_NIVEL0, CB_AREA, CB_CANDIDA, CB_ID_NUM, CB_ENT_NAC, CB_COD_COL,CB_G_FEC_4,CB_G_FEC_5,'+
                       {$ifndef DOS_CAPAS}'CB_ID_BIO, CB_GP_COD, '+{$endif} // SYNERGY
                       'CB_G_FEC_6,CB_G_FEC_7,CB_G_FEC_8,CB_G_LOG_4,CB_G_LOG_5,CB_G_LOG_6,CB_G_LOG_7,CB_G_LOG_8,CB_G_NUM_4,CB_G_NUM_5,CB_G_NUM_6,CB_G_NUM_7,CB_G_NUM_8,CB_G_NUM_9,CB_G_NUM10,'+
                       'CB_G_NUM11,CB_G_NUM12,CB_G_NUM13,CB_G_NUM14,CB_G_NUM15,CB_G_NUM16,CB_G_NUM17,CB_G_NUM18,CB_G_TAB_5,CB_G_TAB_6,CB_G_TAB_7,CB_G_TAB_8,CB_G_TAB_9,CB_G_TAB10,CB_G_TAB11,'+
                       'CB_G_TAB12,CB_G_TAB13,CB_G_TAB14,CB_G_TEX_5,CB_G_TEX_6,CB_G_TEX_7,CB_G_TEX_8,CB_G_TEX_9,CB_G_TEX10,CB_G_TEX11,CB_G_TEX12,CB_G_TEX13,CB_G_TEX14,CB_G_TEX15,CB_G_TEX16,'+
                       'CB_G_TEX17,CB_G_TEX18,CB_G_TEX19,CB_SUB_CTA,CB_NETO, CB_DER_PV, CB_V_PRIMA, CB_PLAZA, CB_NOMINA,CB_RECONTR,CB_DISCAPA,CB_INDIGE,CB_FONACOT,CB_EMPLEO,US_CODIGO, CB_FEC_COV, ' +
                       'CB_G_TEX20,CB_G_TEX21,CB_G_TEX22,CB_G_TEX23,CB_G_TEX24,CB_INFACT,CB_INFDISM,CB_FEC_NOM,CB_NUM_INT, CB_NUM_EXT,CB_INF_ANT,CB_TDISCAP,CB_ESCUELA,CB_TESCUEL,CB_TITULO,CB_YTITULO,'+
                       'CB_CTA_GAS,CB_CTA_VAL,CB_MUNICIP,CB_TSANGRE,CB_ALERGIA,CB_BRG_ACT,CB_BRG_TIP,CB_BRG_ROL,CB_BRG_JEF,CB_BRG_CON,CB_BRG_PRA,CB_BRG_NOP,CB_E_MAIL,CB_BANCO,CB_REGIMEN'+
                       ' from V_EMP_TIMB ';

          Q_VA_PERIODO: Result := 'select PE_YEAR, PE_TIPO, PE_NUMERO, PE_USO, PE_FEC_INI, PE_FEC_FIN, PE_NUM_EMP, '+
{$ifdef QUINCENALES}
                                  'PE_ASI_INI, PE_ASI_FIN, '+
{$endif}
                                  'PE_FEC_PAG, PE_SOLO_EX, PE_INC_BAJ, PE_DIAS, PE_MES, PE_STATUS, PE_TIMBRO, PE_DESCRIP from PERIODO where ' +
                                  '( PE_YEAR = %d ) and ( PE_TIPO = %d ) and ( PE_NUMERO = %d ) and ( PE_STATUS > 4 )';

          Q_VA_PERIODO_ANTERIOR: Result := 'select PE_YEAR, PE_TIPO, PE_NUMERO, PE_USO, PE_FEC_INI, PE_FEC_FIN, PE_NUM_EMP, ' +
{$ifdef QUINCENALES}
                                           'PE_ASI_INI, PE_ASI_FIN, '+
{$endif}
                                           'PE_FEC_PAG, PE_SOLO_EX, PE_INC_BAJ, PE_DIAS, PE_MES, PE_STATUS, PE_TIMBRO, PE_DESCRIP from PERIODO where ' +
                                           '( PE_YEAR = %0:d ) and ( PE_TIPO = %1:d ) and ( PE_NUMERO = ' +
                                           '( select MAX( P1.PE_NUMERO ) from PERIODO P1 where ' +
                                           '( P1.PE_YEAR = %0:d ) and ( P1.PE_TIPO = %1:d ) and ( P1.PE_NUMERO < %2:d ) and ( P1.PE_STATUS > 4 )) )';


          Q_VA_PERIODO_SIGUIENTE: Result := 'select PE_YEAR, PE_TIPO, PE_NUMERO, PE_USO, PE_FEC_INI, PE_FEC_FIN, PE_NUM_EMP, ' +
{$ifdef QUINCENALES}
                                            'PE_ASI_INI, PE_ASI_FIN, '+
{$endif}
                                            'PE_FEC_PAG, PE_SOLO_EX, PE_INC_BAJ, PE_DIAS, PE_MES, PE_STATUS,PE_TIMBRO,  PE_DESCRIP from PERIODO where ' +
                                            '( PE_YEAR = %0:d ) and ( PE_TIPO = %1:d ) and ( PE_NUMERO = ' +
                                            '( select MIN( P1.PE_NUMERO ) from PERIODO P1 where ' +
                                            '( P1.PE_YEAR = %0:d ) and ( P1.PE_TIPO = %1:d ) and ( P1.PE_NUMERO > %2:d ) and ( P1.PE_STATUS > 4 )) )';

          Q_PERIODOS_X_MES_YEAR: Result := 'select * from dbo.TimbradoConcilia_GetPeriodos(%s, %d, %d) where PE_TIMBRO not in (3,4)  order by  PE_YEAR,PE_TIPO,PE_NUMERO';
          Q_COCILIACION_NOMINTA_TIMBRAMEX: Result :=  'select * from dbo.TimbradoConcilia_GetNominasTRESSTabla(%s, %d, %d, %d) order by Numero';
          Q_CONCILIACION_EMPLEADOS_TIMBRADO : Result := 'select * from dbo.TimbradoConcilia_GetDetalle(%s, %d, %d, %d) order by CB_CODIGO';
          Q_PENDIENTE_EMPLEADOS_TIMBRADO: Result := '{CALL SP_PENDIENTE_TIMBRADO_EMPLEADO(:Year, :Tipo, :Numero, :Empleado, :Status, :Usuario )}';
     end;
end;

{ ***************** TdmServerNomina *************** }

procedure TdmServerNominaTimbrado.InitLog( Empresa: Olevariant; const sPaletita: string );
begin
    {$ifdef BITACORA_DLLS}
     FPaletita := sPaletita;
     FArchivoLog := 'c:\BitacoraTress '+ FormatDateTime('dd_mmm_yy', Date()) +'.txt';

     if FListaLog = NIL then
     begin
          try
             FListaLog :=  TAsciiLog.Create;
             FListaLog.Init(FArchivoLog);
             try
                if NOT varisNull( Empresa ) then
                   try
                      FEmpresa :=  ' -- Alias: ' + Empresa[P_CODIGO] +
                                   ' Usuario: ' + InTToStr(Empresa[P_USUARIO])
                   except
                         FEmpresa := ' Error al leer Empresa[]';
                   end
                else
                    FEmpresa := '';
             except
                   FEmpresa := ' Error al leer Empresa[]';
             end;

             FListaLog.WriteTexto( FormatDateTime('dd/mmm/yy hh:nn:ss',Now) +
                                   ' DServerNomina :: INICIA :: '+  FPaletita +
                                   FEmpresa );
             FListaLog.Close;
          except
          end;
     end;
     {$endif}
end;

procedure TdmServerNominaTimbrado.EndLog;
begin
     {$ifdef BITACORA_DLLS}
     try
        if FArchivoLog= '' then
           FArchivoLog := 'c:\BitacoraTress '+ FormatDateTime('dd_mmm_yy', Date()) +'.txt';
        if FListaLog = NIL then
        begin
             FListaLog :=  TAsciiLog.Create;
             FListaLog.Init(FArchivoLog);
             FListaLog.WriteTexto( FormatDateTime('dd/mmm/yy hh:nn:ss',Now) + ' DServerNomina :: TERMINA :: '+ FPaletita +' '+ FEmpresa);
        end
        else
        begin
             //FListaLog.Open(FArchivoLog);
             FListaLog.Init(FArchivoLog);
             FListaLog.WriteTexto( FormatDateTime('dd/mmm/yy hh:nn:ss',NOw) + ' DServerNomina :: TERMINA :: '+ FPaletita +' '+ FEmpresa  );
             //FListaLog.Close;
        end;
        FListaLog.Close;
        FreeAndNil(FListaLog);
     except

     end;
     {$endif}
end;




class procedure TdmServerNominaTimbrado.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
     if Register then
     begin
          inherited UpdateRegistry(Register, ClassID, ProgID);
          EnableSocketTransport(ClassID);
          EnableWebTransport(ClassID);
     end
     else
     begin
          DisableSocketTransport(ClassID);
          DisableWebTransport(ClassID);
          inherited UpdateRegistry(Register, ClassID, ProgID);
     end;
end;

procedure TdmServerNominaTimbrado.dmServerNominaCreate(Sender: TObject);
begin
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
end;

procedure TdmServerNominaTimbrado.dmServerNominaDestroy(Sender: TObject);
begin
     ZetaSQLBroker.FreeZetaCreator( oZetaCreator );
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
end;

{ *********** Clases Auxiliares ************ }

function TdmServerNominaTimbrado.GetSQLBroker: TSQLBroker;
begin
{$ifdef DOS_CAPAS}
     Result := ZetaSQLBroker.oZetaSQLBroker;
{$else}
     Result := Self.oZetaSQLBroker;
{$endif}
end;

{$ifdef DOS_CAPAS}
procedure TdmServerNominaTimbrado.CierraEmpresa;
begin
end;
{$endif}

procedure TdmServerNominaTimbrado.InitCreator;
begin
     if not Assigned( oZetaCreator ) then
        oZetaCreator := ZetaSQLBroker.GetZetaCreator( oZetaProvider );
end;

procedure TdmServerNominaTimbrado.InitBroker;
begin
     InitCreator;
     oZetaCreator.RegistraFunciones([efComunes,efAnuales,efAguinaldo]);
     oZetaSQLBroker := ZetaSQLBroker.GetSQLBroker( oZetaCreator );
end;

procedure TdmServerNominaTimbrado.ClearBroker;
begin
     ZetaSQLBroker.FreeSQLBroker( oZetaSQLBroker );
end;

procedure TdmServerNominaTimbrado.InitNomina;
begin
     InitCreator;
     InitQueries;
     FNomina := TNomina.Create( oZetaCreator );
end;

procedure TdmServerNominaTimbrado.ClearNomina;
begin
     FreeAndNil( FNomina );
end;

procedure TdmServerNominaTimbrado.InitPrestaciones;
begin
     InitCreator;
     oZetaCreator.PreparaPrestaciones;
end;

procedure TdmServerNominaTimbrado.InitQueries;
begin
     InitCreator;
     oZetaCreator.PreparaQueries;
end;

procedure TdmServerNominaTimbrado.InitRitmos;
begin
     InitCreator;
     InitQueries;
     oZetaCreator.PreparaRitmos;
end;

{ **************** Funciones y Procedimientos Privados **************** }

procedure TdmServerNominaTimbrado.SetTablaInfo(const eTipo: EFormaNomina);
begin
     FFormaNomina := eTipo;
     with oZetaProvider.TablaInfo do
     begin
          case eTipo of
               fnDatosNomina, fnDatosAsist, fnDiasHoras, fnMontos, fnDatosClasifi, fnPagoRecibos, fnLiquidacion : SetInfo( 'NOMINA', 'PE_YEAR,PE_TIPO,PE_NUMERO,CB_CODIGO,CB_CLASIFI,CB_TURNO,CB_PATRON,CB_PUESTO,CB_ZONA_GE,NO_FOLIO_1,NO_FOLIO_2,NO_FOLIO_3,NO_FOLIO_4,NO_FOLIO_5,NO_OBSERVA, '+
                                       'NO_STATUS,US_CODIGO,NO_USER_RJ,CB_NIVEL1,CB_NIVEL2,CB_NIVEL3,CB_NIVEL4,CB_NIVEL5,CB_NIVEL6,CB_NIVEL7,CB_NIVEL8,CB_NIVEL9,'+
                                       {$ifdef ACS}'CB_NIVEL10, CB_NIVEL11, CB_NIVEL12, '+{$endif}
                                       'CB_SAL_INT,CB_SALARIO,NO_DIAS,NO_ADICION, '+
                                       'NO_DEDUCCI,NO_NETO,NO_DES_TRA,NO_DIAS_AG,NO_DIAS_VA,NO_DOBLES,NO_EXTRAS,NO_FES_PAG,NO_FES_TRA,NO_HORA_CG,NO_HORA_SG,NO_HORAS,NO_IMP_CAL,NO_JORNADA,NO_PER_CAL,NO_PER_MEN, '+
                                       'NO_PERCEPC,NO_TARDES,NO_TRIPLES,NO_TOT_PRE,NO_VAC_TRA,NO_X_CAL,NO_X_ISPT,NO_X_MENS,NO_D_TURNO,NO_DIAS_AJ,NO_DIAS_AS,NO_DIAS_CG,NO_DIAS_EM,NO_DIAS_FI,NO_DIAS_FJ, '+
{$ifdef QUINCENALES}
                                       'NO_HORASNT,NO_HORAPDT,' +
{$endif}
                                       'NO_DIAS_FV,NO_DIAS_IN,NO_DIAS_NT,NO_DIAS_OT,NO_DIAS_RE,NO_DIAS_SG,NO_DIAS_SS,NO_DIAS_SU,NO_HORA_PD,NO_LIQUIDA,NO_FUERA,NO_EXENTAS,NO_FEC_PAG,NO_USR_PAG,CB_NIVEL0,NO_DIAS_SI,NO_SUP_OK ,'+
                                       'NO_FEC_OK,NO_HOR_OK,CB_BAN_ELE,NO_APROBA,NO_GLOBAL,CB_CTA_GAS,CB_CTA_VAL,NO_PER_ISN', 'PE_YEAR,PE_TIPO,PE_NUMERO,CB_CODIGO' );
               fnFaltas, fnExcepciones : SetInfo( 'FALTAS', 'PE_YEAR,PE_TIPO,PE_NUMERO,CB_CODIGO,FA_FEC_INI,FA_DIA_HOR,FA_MOTIVO,FA_DIAS,FA_HORAS', 'PE_YEAR,PE_TIPO,PE_NUMERO,CB_CODIGO,FA_DIA_HOR,FA_FEC_INI,FA_MOTIVO' );
               fnMovMontos, fnExcepMontos : SetInfo( 'MOVIMIEN', 'PE_YEAR,PE_TIPO,PE_NUMERO,CB_CODIGO,MO_ACTIVO,CO_NUMERO,MO_IMP_CAL,US_CODIGO,MO_PER_CAL,MO_REFEREN,MO_X_ISPT,MO_PERCEPC,MO_DEDUCCI', 'PE_YEAR,PE_TIPO,PE_NUMERO,CB_CODIGO,CO_NUMERO,MO_REFEREN' );
               fnTotales : Setinfo( 'PERIODO', 'PE_YEAR,PE_TIPO,PE_NUMERO,PE_DESCRIP,PE_USO,PE_STATUS,PE_INC_BAJ,PE_SOLO_EX,PE_FEC_INI,PE_FEC_FIN,'+
{$ifdef QUINCENALES}
                                    'PE_ASI_INI,PE_ASI_FIN,' +
{$endif}
                                    'PE_FEC_PAG,PE_MES,PE_DIAS,PE_DIAS_AC,PE_DIA_MES,PE_POS_MES,PE_PER_MES,PE_PER_TOT,PE_FEC_MOD, '+
                                    'PE_AHORRO,PE_PRESTAM,PE_NUM_EMP,PE_TOT_PER,PE_TOT_NET,PE_TOT_DED,PE_LOG,US_CODIGO', 'PE_YEAR,PE_TIPO,PE_NUMERO' );
               fnExcepGlobales : SetInfo( 'MOV_GRAL', 'PE_YEAR,PE_TIPO,PE_NUMERO,CO_NUMERO,MG_FIJO,MG_MONTO,US_CODIGO', 'PE_YEAR,PE_TIPO,PE_NUMERO,CO_NUMERO' );
               fnMovDatosAsist : SetInfo( 'AUSENCIA',
                                          'CB_CODIGO,AU_DES_TRA,AU_DOBLES,AU_EXTRAS,AU_FECHA,AU_HORAS,AU_PER_CG,AU_PER_SG,AU_TARDES,AU_TIPO,AU_TIPODIA,CB_CLASIFI,CB_TURNO, '+
{$ifdef QUINCENALES}
                                          'AU_HORASNT, ' +
{$endif}
                                          'CB_PUESTO,HO_CODIGO,CB_NIVEL1,CB_NIVEL2,CB_NIVEL3,CB_NIVEL4,CB_NIVEL5,CB_NIVEL6,CB_NIVEL7,CB_NIVEL8,CB_NIVEL9,'+
                                          {$ifdef ACS}'CB_NIVEL10, CB_NIVEL11, CB_NIVEL12,'+{$endif}
                                          'AU_AUT_EXT,AU_AUT_TRA,AU_HOR_MAN,AU_STATUS,AU_NUM_EXT, '+
                                          'AU_TRIPLES,US_CODIGO,AU_POSICIO,AU_HORASCK,PE_YEAR,PE_TIPO,PE_NUMERO',
                                          'CB_CODIGO,AU_FECHA' );
               fnDatosPoliza   : SetInfo( 'POL_HEAD','PE_YEAR,PE_TIPO,PE_NUMERO,PT_CODIGO,US_CODIGO,PH_FECHA,PH_HORA,PH_VECES,PH_REPORTE,PH_STATUS','PE_YEAR,PE_TIPO,PE_NUMERO,PT_CODIGO');
               fnFonTot        : SetInfo( 'FON_TOT','FT_YEAR,FT_MONTH,PR_TIPO,FT_STATUS,FT_RETENC,FT_NOMINA,FT_TAJUST,FT_AJUSTE,FT_EMPLEAD, FT_CUANTOS, FT_BAJAS, FT_INCAPA, FT_DETALLE', 'FT_YEAR,FT_MONTH,PR_TIPO');
               fnFonEmp        : SetInfo( 'FON_EMP','FT_YEAR,FT_MONTH,PR_TIPO,CB_CODIGO,FE_RETENC,FE_NOMINA,FE_AJUSTE,FE_CUANTOS,FE_INCIDE,FE_FECHA1,FE_FECHA2','FT_YEAR,FT_MONTH,CB_CODIGO');
               fnFonCre        : SetInfo( 'FON_CRE','FT_YEAR,FT_MONTH,PR_TIPO,CB_CODIGO,PR_REFEREN,FC_RETENC,FC_NOMINA,FC_AJUSTE','FT_YEAR,FT_MONTH,PR_TIPO,CB_CODIGO,PR_REFEREN');
               fnDeclaracion   : SetInfo( 'TMPDIMMTOT', 'TD_YEAR, TD_STATUS', 'TD_YEAR' );

          end;
          if ( eTipo = fnPagoRecibos ) then
               BeforeUpdateRecord := BeforeUpdatePagoRecibos;
     end
end;

procedure TdmServerNominaTimbrado.SetDetailInfo( const eDetail: EFormaNomina );
begin
     with oZetaProvider.DetailInfo do
     begin
        case eDetail of
             {
             fnDatosAsist: SetInfoDetail( 'AUSENCIA',
                                          'CB_CODIGO,AU_DES_TRA,AU_DOBLES,AU_EXTRAS,AU_FECHA,AU_HORAS,AU_PER_CG,AU_PER_SG,AU_TARDES,AU_TIPO,AU_TIPODIA,CB_CLASIFI,CB_TURNO, '+
                                          'CB_PUESTO,HO_CODIGO,CB_NIVEL1,CB_NIVEL2,CB_NIVEL3,CB_NIVEL4,CB_NIVEL5,CB_NIVEL6,CB_NIVEL7,CB_NIVEL8,CB_NIVEL9,AU_AUT_EXT,AU_AUT_TRA,AU_HOR_MAN,AU_STATUS,AU_NUM_EXT, '+
                                          'AU_TRIPLES,US_CODIGO,AU_POSICIO,AU_HORASCK,PE_YEAR,PE_TIPO,PE_NUMERO',
                                          'CB_CODIGO,AU_FECHA',
                                          'PE_YEAR=PE_YEAR,PE_TIPO=PE_TIPO,PE_NUMERO=PE_NUMERO,CB_CODIGO=CB_CODIGO' );
             }
             fnDiasHoras: SetInfoDetail( 'FALTAS',
                                         'PE_YEAR,PE_TIPO,PE_NUMERO,CB_CODIGO,FA_FEC_INI,FA_DIA_HOR,FA_MOTIVO,FA_DIAS,FA_HORAS', 'PE_YEAR,PE_TIPO,PE_NUMERO,CB_CODIGO,FA_DIA_HOR,FA_FEC_INI,FA_MOTIVO',
                                         'PE_YEAR,PE_TIPO,PE_NUMERO,CB_CODIGO' )
        end;
     end;
end;

function TdmServerNominaTimbrado.GetScript(const eTipo: eFormaNomina): String;
begin
     case eTipo of
          fnExcepciones:
          Result := 'select A.PE_YEAR, A.PE_TIPO, A.PE_NUMERO, A.CB_CODIGO, A.FA_FEC_INI, ' +
                    'A.FA_DIA_HOR, A.FA_MOTIVO, A.FA_DIAS, A.FA_HORAS, 0 as OPERACION,' +
                    K_PRETTYNAME + ' as PrettyName '+
                    'from FALTAS A '+
                    'left outer join COLABORA on ( A.CB_CODIGO = COLABORA.CB_CODIGO ) '+
                    'where ( A.PE_YEAR = %d ) and ( A.PE_TIPO = %d ) and ( A.PE_NUMERO = %d ) %s'+
                    'order by A.CB_CODIGO, A.FA_FEC_INI';
          fnExcepMontos:
          Result := 'select A.PE_YEAR, A.PE_TIPO, A.PE_NUMERO, A.CB_CODIGO, '+
                    'A.MO_ACTIVO, A.CO_NUMERO, A.MO_IMP_CAL, A.US_CODIGO, '+
                    'A.MO_PER_CAL, A.MO_REFEREN, A.MO_X_ISPT, A.MO_PERCEPC, A.MO_DEDUCCI, '+
                    'A.MO_PERCEPC + A.MO_DEDUCCI as MO_MONTO, 0 as OPERACION, ' +
                    'B.CO_DESCRIP, B.CO_TIPO, '+
                    K_PRETTYNAME + ' as PrettyName '+
                    'from MOVIMIEN A '+
                    'left outer join CONCEPTO B on ( A.CO_NUMERO = B.CO_NUMERO ) '+
                    'left outer join COLABORA on ( A.CB_CODIGO = COLABORA.CB_CODIGO ) where '+
                    '( A.PE_YEAR = %d ) and ( A.PE_TIPO = %d ) and ( A.PE_NUMERO = %d ) and ( A.US_CODIGO > 0 ) %s'+
                    'order by A.CB_CODIGO, A.CO_NUMERO';
          fnPagoRecibos:
          Result := 'select A.CB_CODIGO, A.PE_YEAR, A.PE_TIPO, A.PE_NUMERO, ' +
                    'A.NO_FEC_PAG, A.NO_USR_PAG, A.US_CODIGO, ' +
                    K_PRETTYNAME + ' as PrettyName '+
                    'from NOMINA A ' +
                    'left outer join COLABORA on ( COLABORA.CB_CODIGO = A.CB_CODIGO ) ' +
                    'where ( A.PE_YEAR = %d ) and ( A.PE_TIPO = %d ) and ( A.PE_NUMERO = %d ) %s'+
                    'order by A.CB_CODIGO';
          fnValidaNomina:
          Result := 'select CB_CODIGO, NO_FEC_PAG, NO_USR_PAG from NOMINA where ' +
                    '( CB_CODIGO = %d ) and ( PE_YEAR = %d ) and ( PE_TIPO = %d ) and ( PE_NUMERO = %d )';
          fnUpdateRecibos:
          Result := 'update NOMINA set NO_FEC_PAG = ''%s'', NO_USR_PAG = %d where ' +
                    '( CB_CODIGO = %d ) and ( PE_YEAR = %d ) and ( PE_TIPO = %d ) and ( PE_NUMERO = %d )';
          fnLiquidacion:
          Result := 'select CB_CODIGO, PE_YEAR, PE_TIPO, PE_NUMERO, NO_HORAS, NO_DOBLES, NO_TRIPLES, ' +
                    'NO_TARDES, NO_ADICION, NO_HORA_PD, NO_LIQUIDA, NO_DIAS_VA, NO_DIAS_AG, NO_OBSERVA, '+
                    ''' '' as VA_ANTES, '' '' as VA_ACTUAL, '' '' as VA_FORMULA, '' '' as AG_FORMULA ' +
                    'from NOMINA where '+
                    '( CB_CODIGO = %d ) and ( PE_YEAR = %d ) and ( PE_TIPO = %d ) and ( PE_NUMERO = %d )';
          //MA:Se modific� fnBorraNominas con el numero del empleado
          fnBorraNominas:
          Result := 'delete from NOMINA where ( PE_YEAR = %d ) and ( PE_TIPO = %d ) and ( PE_NUMERO = %d ) and ( CB_CODIGO = %d ) %s';
          fnBorraPoliza:
          Result := 'delete from POLIZA where ( PE_YEAR = %d ) and ( PE_TIPO = %d ) and ( PE_NUMERO = %d )';
          fnUpdatePeriodo:
          Result := 'update PERIODO set US_CODIGO = %d, PE_TOT_NET = 0, PE_TOT_DED = 0, PE_TOT_PER = 0, '+
                    'PE_NUM_EMP = 0, PE_FEC_MOD = %s, PE_STATUS = 0 where '+
                    '( PE_YEAR = %d ) and ( PE_TIPO = %d ) and ( PE_NUMERO = %d )';
          fnBorraPeriodo:
          Result := 'delete from PERIODO where ( PE_YEAR = :Year ) and ( PE_TIPO = :Tipo ) and ( PE_NUMERO = :Numero )';
          fnBorraPeriodoLista:
          Result := 'select PE_YEAR, PE_TIPO, PE_NUMERO from PERIODO where '+
                    '( PE_YEAR = %d ) and ( PE_TIPO = %d ) and ( PE_NUMERO between %d and %d )';
          fnExisteEmpleado:
          Result := 'select CB_CODIGO,' + K_PRETTYNAME + ' as PrettyName ' +
                    'from COLABORA where ( CB_CODIGO = :Empleado ) %s';
          fnExisteNomina:
          Result := 'select CB_CODIGO from NOMINA where ' +
                    '( PE_YEAR = :Year ) and ( PE_TIPO = :Tipo ) and ( PE_NUMERO = :Numero ) and ' +
                    '( CB_CODIGO = :Empleado )';
          fnDatosPoliza:
          Result := ' Select PE_YEAR, PE_TIPO, PE_NUMERO, POL_HEAD.PT_CODIGO, PT_NOMBRE, PH_FECHA, PH_HORA, US_CODIGO, PH_VECES, PH_STATUS, PH_REPORTE, '+
                            '(SELECT COUNT(*) '+
                            'FROM POLIZA '+
                            'WHERE POLIZA.PE_YEAR   = POL_HEAD.PE_YEAR AND '+
                                 'POLIZA.PE_TIPO   = POL_HEAD.PE_TIPO AND '+
                                 'POLIZA.PE_NUMERO = POL_HEAD.PE_NUMERO AND '+
                                 'POLIZA.PT_CODIGO = POL_HEAD.PT_CODIGO ) '+
                                 'POL_MOVS' +
                    ' From   POL_HEAD left outer join POL_TIPO on POL_HEAD.PT_CODIGO = POL_TIPO.PT_CODIGO '+
                    ' Where  PE_YEAR   = %d AND '+
                            'PE_TIPO   = %d AND '+
                            'PE_NUMERO = %d     '+
                    ' Order by PH_FECHA, PH_HORA; ';
          fnInsertaPago:
{$ifdef INTERBASE}
          Result := 'execute procedure UPDATE_NOM_RECIBO( :Empleado, :Year, :Tipo, :Numero, :Fecha, :Usuario )';
{$else}
          Result := 'update NOMINA set NO_FEC_PAG = :Fecha, NO_USR_PAG = :Usuario ' +
                    'where CB_CODIGO = :Empleado and PE_YEAR = :Year and PE_TIPO = :Tipo and PE_NUMERO = :Numero';

{$endif}
     end;
end;

procedure TdmServerNominaTimbrado.BeforeUpdatePagoRecibos(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
     { PENDIENTE: Usar CampoAsVar en lugar de FieldByName }
     { PENDIENTE: � Que hay de UpdateKind ukModified, ukInsert, ukDelete ? }
     with DeltaDS do
     begin
          oZetaProvider.ExecSQL( FRecibosEmpresa, Format( GetScript( fnUpdateRecibos ),
                                                          [ DateToStrSQL( FieldByName( 'NO_FEC_PAG' ).AsDateTime ),
                                                            FieldByName( 'NO_USR_PAG' ).AsInteger,
                                                            FieldByName( 'CB_CODIGO' ).AsInteger,
                                                            FieldByName( 'PE_YEAR' ).AsInteger,
                                                            FieldByName( 'PE_TIPO' ).AsInteger,
                                                            FieldByName( 'PE_NUMERO' ).AsInteger ] ) );
     end;
     Applied := True;
end;

procedure TdmServerNominaTimbrado.SetPrenominaValues(Sender: TObject; DataSet: TzProviderClientDataSet);
{$ifdef FALSE}
var
   sIncidencia: String;
{$endif}
begin
{$ifdef FALSE}
     sIncidencia := oZetaProvider.GetGlobalString( K_INCIDENCIA_FI );
{$endif}
     with DataSet do
     begin
          First;
          FEmpleado := CampoAsVar( FieldByName( 'CB_CODIGO' ) );
{$ifdef FALSE}
          while not Eof do
          begin
               Edit;
               FieldByName( 'AU_EXTRAS' ).AsFloat := ZetaServerTools.CampoAsVar( FieldByName( 'AU_DOBLES' ) ) + ZetaServerTools.CampoAsVar( FieldByName( 'AU_TRIPLES' ) );
               if ( ZetaServerTools.CampoAsVar( FieldByName( 'US_CODIGO' ) ) <> 0 ) and { Tarjeta Es Manual }
                  ( eStatusAusencia( ZetaServerTools.CampoAsVar( FieldByName( 'AU_STATUS' ) ) ) = auHabil ) and
                  ( ZetaServerTools.CampoAsVar( FieldByName( 'AU_HORAS' ) ) + FieldByName( 'AU_EXTRAS' ).AsFloat + ZetaServerTools.CampoAsVar( FieldByName( 'AU_PER_SG' ) ) = 0.0 ) then
               begin
                    FieldByName( 'AU_TIPO' ).AsString := sIncidencia;
               end;
               Post;
               Next;
          end;
{$endif}
     end;
end;

procedure TdmServerNominaTimbrado.InitBufferEmpleado;
begin
     FEmpleado := -1;
end;

procedure TdmServerNominaTimbrado.VerificarNomina(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
var
   iEmpleado: TNumEmp;
begin
     iEmpleado := CampoAsVar( DeltaDS.FieldByName( 'CB_CODIGO' ) );
     if ( iEmpleado <> FEmpleado ) then
     begin
          with Nomina do
          begin
               case UpdateKind of
                    ukModify: VerificaStatus( iEmpleado );
                    ukInsert: VerificaRegistro( iEmpleado, TRUE );
                    ukDelete: VerificaStatus( iEmpleado );
               end;
          end;
          FEmpleado := iEmpleado;
     end;
end;

procedure TdmServerNominaTimbrado.SetOperacionConflicto;
begin
     FOperacion := eOperacionConflicto( oZetaProvider.ParamList.ParamByName( 'Operacion' ).AsInteger );
end;

procedure TdmServerNominaTimbrado.InitQueryNomina( var FQueryNomina: TZetaCursor; const iSQL: Integer );
begin
     with oZetaProvider do
     begin
          if ( FQueryNomina = nil ) then
          begin
               //CV: CreateQuery--ok
               //Query Global, se destruye hasta que el ZetaProvider lo destruye
               //Solamente se cierra
               FQueryNomina := CreateQuery( GetSQLScript( iSQL ) );
               { Estos par�metros no cambian de un registro a otro }
               with DatosPeriodo do
               begin
                    ParamAsInteger( FQueryNomina, 'Year', Year );
                    ParamAsInteger( FQueryNomina, 'Tipo', Ord( Tipo ) );
                    ParamAsInteger( FQueryNomina, 'Numero', Numero );
               end;
          end;
     end;
end;

procedure TdmServerNominaTimbrado.CreateErrorMontosDataSet;
begin
     with cdsLista do
     begin
          InitTempDataSet;
          AddIntegerField( 'CB_CODIGO' );
          AddIntegerField( 'CO_NUMERO' );
          AddStringField( 'MO_REFEREN', K_ANCHO_REFERENCIA );
          AddFloatField( 'MO_MONTO' );
          CreateTempDataset;
     end;
end;

procedure TdmServerNominaTimbrado.ErrorMontos(Sender: TObject;  DataSet: TzProviderClientDataSet; E: EUpdateError; UpdateKind: TUpdateKind;  var Response: TResolverResponse);
var
   BorrarRegistro: Boolean;
   rMonto: TPesos;
   eOperacion : eOperacionConflicto;
   ExcedeLimites :Boolean;

   function GetOperacion : eOperacionConflicto;
   var
      oCampo: TField;
   begin
        oCampo := DataSet.FindField( 'OPERACION' );
        if ( oCampo <> nil ) and ( oCampo.AsInteger > 0 ) then
           Result := eOperacionConflicto( oCampo.AsInteger )
        else
           Result := FOperacion;
   end;

   procedure SetInfoMontos( FQryNomina: TZetaCursor );
   begin
        with oZetaProvider do
        begin
             with Dataset do
             begin
                  ParamAsInteger( FQryNomina, 'Empleado', CampoAsVar( FieldByName( 'CB_CODIGO' ) ) );
                  ParamAsInteger( FQryNomina, 'Concepto', CampoAsVar( FieldByName( 'CO_NUMERO' ) ) );
                  ParamAsInteger( FQryNomina, 'Usuario', CampoAsVar( FieldByName( 'US_CODIGO' ) ) );
                  ParamAsVarChar( FQryNomina, 'Referencia', CampoAsVar( FieldByName( 'MO_REFEREN' ) ), K_ANCHO_REFERENCIA );
                  ParamAsFloat( FQryNomina, 'Percepcion', CampoAsVar( FieldByName( 'MO_PERCEPC' ) ) );
                  ParamAsFloat( FQryNomina, 'Deduccion', CampoAsVar( FieldByName( 'MO_DEDUCCI' ) ) );
             end;
        end;
   end;

   function ExisteMonto: Boolean;
   begin
        InitQueryNomina( FQryConflictoMontos, Q_MONTOS_EXISTE );
        with FQryConflictoMontos do
        begin
             //CV-Active Revisado
             Active:= FALSE;
             with oZetaProvider, Dataset do
             begin
                  ParamAsInteger( FQryConflictoMontos, 'Empleado', CampoAsVar( FieldByName( 'CB_CODIGO' ) ) );
                  ParamAsInteger( FQryConflictoMontos, 'Concepto', CampoAsVar( FieldByName( 'CO_NUMERO' ) ) );
                  ParamAsVarChar( FQryConflictoMontos, 'Referencia', CampoAsVar( FieldByName( 'MO_REFEREN' ) ), K_ANCHO_REFERENCIA );
             end;
             Active := TRUE;
             Result := not EOF;
             rMonto := Fields[ 0 ].AsFloat;
             Active := FALSE;
        end;
   end;

   function PasaLimite(iConcepto:Integer;rMontoEval:TPesos):Boolean;
   begin
         if FQryConcepto = nil then
                FQryConcepto := oZetaProvider.CreateQuery( GetSQLScript( Q_CONCEPTO ));
        with FQryConcepto do
        begin
             Active := False;
             oZetaProvider.ParamAsInteger(FQryConcepto,'Concepto',iConcepto );
             Active := True;
             //Limite Inferior
             Result := ( ( zStrToBool( FieldByName('CO_VER_INF').AsString ) ) and ( rMontoEval < FieldByName('CO_LIM_INF').AsFloat ) );
             if not Result then
             begin
                  //Limite Superior
                  Result := ( ( zStrToBool( FieldByName('CO_VER_SUP').AsString ) ) and ( rMontoEval > FieldByName('CO_LIM_SUP').AsFloat ) );
             end;
             Close;
        end;
   end;

   procedure GrabaBitacora;
   begin
        if ExisteMonto then
           GrabaCambiosBitacora( DataSet, 'MO_PERCEPC', 'MO_DEDUCCI', clbExcepcionMonto, eOperacion, rMonto );

        {sField := 'MO_PERCEPC';
        rMontoNuevo := CampoAsVar( FieldByName( sField ) );
        if rMonto = 0 then
        begin
             sField := 'MO_DEDUCCI';
             rMontoNuevo := CampoAsVar( FieldByName( sField ) );
        end;

        if ExisteMonto then
        begin
             if eOperacion = ocSumar then
                rMonto := rMonto + rMontoNuevo;

       end; }
   end;


begin
     if PK_Violation( E ) then
     begin
          Response := rrIgnore;
          BorrarRegistro:= FALSE;
          ExcedeLimites := True;
          if oZetaProvider.ParamList.FindParam('ExcederLimites') <> nil then
             ExcedeLimites := oZetaProvider.ParamList.ParamByName('ExcederLimites').AsBoolean;

          try
             if ( UpdateKind = ukModify ) then
                BorrarRegistro:= TRUE;
             eOperacion := GetOperacion;
             case eOperacion of
                  ocSumar     : begin
                                     if ( (ExisteMonto) and
                                          (PasaLimite( DataSet.FieldByName('CO_NUMERO').AsInteger,rMonto + ( DataSet.FieldByName('MO_PERCEPC').AsFloat + DataSet.FieldByName('MO_DEDUCCI').AsFloat ) ) ) and
                                          (not ExcedeLimites) )then
                                     begin
                                          Response := rrSkip;
                                          BorrarRegistro:= FALSE;
                                          cdsLista.AppendRecord( [ CampoAsVar( DataSet.FieldByName( 'CB_CODIGO' ) ),
                                                       CampoAsVar( DataSet.FieldByName( 'CO_NUMERO' ) ),
                                                       CampoAsVar( DataSet.FieldByName( 'MO_REFEREN' ) ),
                                                       rMonto ] );
                                     end
                                     else
                                     begin
                                          GrabaBitacora;
                                          InitQueryNomina( FQrySumarMontos, Q_MONTOS_SUMAR );
                                          SetInfoMontos( FQrySumarMontos );
                                          oZetaProvider.Ejecuta( FQrySumarMontos );
                                     end;
                                end;
                  ocSustituir : begin
                                     GrabaBitacora;

                                     InitQueryNomina( FQrySustituirMontos, Q_MONTOS_SUSTITUIR );
                                     SetInfoMontos( FQrySustituirMontos );
                                     oZetaProvider.Ejecuta( FQrySustituirMontos );
                                end;
                  ocIgnorar   : BorrarRegistro:= FALSE;
                  ocReportar  : Begin
                                     Response := rrSkip;
                                     BorrarRegistro:= FALSE;
                                     if ExisteMonto then
                                        with DataSet do
                                             cdsLista.AppendRecord( [ CampoAsVar( FieldByName( 'CB_CODIGO' ) ),
                                             CampoAsVar( FieldByName( 'CO_NUMERO' ) ),
                                             CampoAsVar( FieldByName( 'MO_REFEREN' ) ),
                                             rMonto ] );
                                end;
             end;
             if BorrarRegistro then
                with oZetaProvider, DataSet do
                begin
                     InitQueryNomina( FQryBorrarMontos, Q_MONTOS_BORRAR );
                     ParamAsInteger( FQryBorrarMontos, 'Empleado', FieldByName( 'CB_CODIGO' ).OldValue );
                     ParamAsInteger( FQryBorrarMontos, 'Concepto', FieldByName( 'CO_NUMERO' ).OldValue );
                     ParamAsVarChar( FQryBorrarMontos, 'Referencia', FieldByName( 'MO_REFEREN' ).OldValue, K_ANCHO_REFERENCIA );
                     Ejecuta( FQryBorrarMontos );

                     GrabaCambiosBitacora( Dataset, ukDelete );
                end;
          except
             Response := rrSkip;
             raise
          end;
     end;
     {
     if ( FOperacion <> ocReportar ) and ( UpdateKind = ukInsert ) and PK_Violation( E ) then
     begin
          Response := rrIgnore;
          if ( FOperacion in [ ocSumar, ocSustituir ] ) then
          begin
               try
                  if ( FQryConflicto = nil ) then
                  begin
                       if ( FOperacion = ocSumar ) then
                          InitQueryConflicto( Q_MONTOS_SUMAR )
                       else
                           InitQueryConflicto( Q_MONTOS_SUSTITUIR );
                  end;
                  with oZetaProvider do
                  begin
                       with Dataset do
                       begin
                            ParamAsInteger( FQryConflicto, 'Empleado', FieldByName( 'CB_CODIGO' ).AsInteger );
                            ParamAsInteger( FQryConflicto, 'Concepto', FieldByName( 'CO_NUMERO' ).AsInteger );
                            ParamAsInteger( FQryConflicto, 'Usuario', FieldByName( 'US_CODIGO' ).AsInteger );
                            ParamAsVarChar( FQryConflicto, 'Referencia', FieldByName( 'MO_REFEREN' ).AsString, K_ANCHO_REFERENCIA );
                            ParamAsFloat( FQryConflicto, 'Percepcion', FieldByName( 'MO_PERCEPC' ).AsFloat );
                            ParamAsFloat( FQryConflicto, 'Deduccion', FieldByName( 'MO_DEDUCCI' ).AsFloat );
                       end;
                       Ejecuta( FQryConflicto );
                  end;
               except
                     Response := rrSkip;
                     raise
               end;
          end;
     end;
     }
end;

procedure TdmServerNominaTimbrado.PrepararDiasHoras(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
var
   iEmpleado: TNumEmp;
begin
     iEmpleado := CampoAsVar( DeltaDS.FieldByName( 'CB_CODIGO' ) );
     if ( iEmpleado <> FEmpleado ) then
     begin
          if ( FEmpleado > 0 ) then { Calcula D�as / Horas para el Empleado Anterior }
          begin
               Nomina.PreparaDiasHoras( FEmpleado );
          end;
          if ( iEmpleado > 0 ) then { Cambia Status / Agrega Nomina para el Empleado Actual }
          begin
               with Nomina do
               begin
                    case UpdateKind of
                         ukModify: VerificaStatus( iEmpleado );
                         ukInsert: VerificaRegistro( iEmpleado );
                         ukDelete: VerificaStatus( iEmpleado );
                    end;
               end;
          end;
          FEmpleado := iEmpleado;
     end;
end;

procedure TdmServerNominaTimbrado.CreateErrorDiasHorasDataSet;
begin
     with cdsLista do
     begin
          InitTempDataSet;
          AddIntegerField( 'CB_CODIGO' );
          AddDateField( 'FA_FEC_INI' );
          AddStringField( 'FA_DIA_HOR', K_ANCHO_DIA_HOR );
          AddIntegerField( 'FA_MOTIVO' );
          AddFloatField( 'FA_VALOR' );
          CreateTempDataset;
     end;
end;

procedure TdmServerNominaTimbrado.ErrorDiasHoras(Sender: TObject; DataSet: TzProviderClientDataSet; E: EUpdateError; UpdateKind: TUpdateKind; var Response: TResolverResponse);
var
   BorrarRegistro: Boolean;
   rDiasHoras: Currency;
   eOperacion: eOperacionConflicto;

   function GetOperacion : eOperacionConflicto;
   var
      oCampo: TField;
   begin
        oCampo := DataSet.FindField( 'OPERACION' );
        if ( oCampo <> nil ) and ( oCampo.AsInteger > 0 ) then
           Result := eOperacionConflicto( oCampo.AsInteger )
        else
           Result := FOperacion;
   end;

   procedure SetInfoFaltas( FQryNomina: TZetaCursor );
   begin
        with oZetaProvider do
        begin
             with Dataset do
             begin
                  ParamAsInteger( FQryNomina, 'Empleado', CampoAsVar( FieldByName( 'CB_CODIGO' ) ) );
                  ParamAsInteger( FQryNomina, 'Motivo', CampoAsVar( FieldByName( 'FA_MOTIVO' ) ) );
                  ParamAsChar( FQryNomina, 'DiaHor', CampoAsVar( FieldByName( 'FA_DIA_HOR' ) ), K_ANCHO_DIA_HOR );
                  ParamAsDate( FQryNomina, 'Fecha', CampoAsVar( FieldByName( 'FA_FEC_INI' ) ) );
                  ParamAsFloat( FQryNomina, 'Dias', CampoAsVar( FieldByName( 'FA_DIAS' ) ) );
                  ParamAsFloat( FQryNomina, 'Horas', CampoAsVar( FieldByName( 'FA_HORAS' ) ) );
             end;
        end;
   end;

   function ExisteFalta: Boolean;
   begin
        InitQueryNomina( FQryConflictoFaltas, Q_FALTAS_EXISTE );
        with FQryConflictoFaltas do
        begin
             Active:= FALSE;
             with oZetaProvider, Dataset do
             begin
                  ParamAsInteger( FQryConflictoFaltas, 'Empleado', CampoAsVar( FieldByName( 'CB_CODIGO' ) ) );
                  ParamAsChar( FQryConflictoFaltas, 'DiaHor', CampoAsVar( FieldByName( 'FA_DIA_HOR' ) ), K_ANCHO_DIA_HOR );
                  ParamAsDate( FQryConflictoFaltas, 'Fecha', CampoAsVar( FieldByName( 'FA_FEC_INI' ) ) );
                  ParamAsInteger( FQryConflictoFaltas, 'Motivo', CampoAsVar( FieldByName( 'FA_MOTIVO' ) ) );
             end;
             Active := TRUE;
             Result := not EOF;
             rDiasHoras := Fields[ 0 ].AsFloat;
             Active := FALSE;
        end;
   end;

   procedure GrabaBitacora;
   begin
        if ExisteFalta then
           GrabaCambiosBitacora( DataSet, 'FA_HORAS', 'FA_DIAS', clbExcepcionDias, eOperacion, rDiasHoras );
   end;

begin
     if PK_Violation( E ) then
     begin
          Response := rrIgnore;
          BorrarRegistro:= FALSE;
          try
             if ( UpdateKind = ukModify ) then
                BorrarRegistro:= TRUE;
             eOperacion := GetOperacion;
             case eOperacion of
                  ocSumar     : begin
                                     GrabaBitacora;

                                     InitQueryNomina( FQrySumarFaltas, Q_FALTAS_SUMAR );
                                     SetInfoFaltas( FQrySumarFaltas );
                                     oZetaProvider.Ejecuta( FQrySumarFaltas );
                                end;
                  ocSustituir : begin
                                     GrabaBitacora;

                                     InitQueryNomina( FQrySustituirFaltas, Q_FALTAS_SUSTITUIR );
                                     SetInfoFaltas( FQrySustituirFaltas );
                                     oZetaProvider.Ejecuta( FQrySustituirFaltas );
                                end;
                  ocIgnorar   : BorrarRegistro:= FALSE;
                  ocReportar  : Begin
                                     Response := rrSkip;
                                     BorrarRegistro:= FALSE;
                                     if ExisteFalta then
                                        with DataSet do
                                             cdsLista.AppendRecord( [ CampoAsVar( FieldByName( 'CB_CODIGO' ) ),
                                             CampoAsVar( FieldByName( 'FA_FEC_INI' ) ),
                                             CampoAsVar( FieldByName( 'FA_DIA_HOR' ) ),
                                             CampoAsVar( FieldByName( 'FA_MOTIVO' ) ),
                                             rDiasHoras ] );
                                end;
             end;
             if BorrarRegistro then
                with oZetaProvider, DataSet do
                begin
                     InitQueryNomina( FQryBorrarFaltas, Q_FALTAS_BORRAR );
                     ParamAsInteger( FQryBorrarFaltas, 'Empleado', FieldByName( 'CB_CODIGO' ).OldValue );
                     ParamAsChar( FQryBorrarFaltas, 'DiaHor', FieldByName( 'FA_DIA_HOR' ).OldValue, K_ANCHO_DIA_HOR );
                     ParamAsDate( FQryBorrarFaltas, 'Fecha', FieldByName( 'FA_FEC_INI' ).OldValue );
                     ParamAsInteger( FQryBorrarFaltas, 'Motivo', FieldByName( 'FA_MOTIVO' ).OldValue );
                     Ejecuta( FQryBorrarFaltas );

                     GrabaCambiosBitacora( Dataset, ukDelete );
                end;
          except
             Response := rrSkip;
             raise
          end;
     end;
     {
     if ( FOperacion <> ocReportar ) and ( UpdateKind = ukInsert ) and PK_Violation( E ) then
     begin
          Response := rrIgnore;
          if ( FOperacion in [ ocSumar, ocSustituir ] ) then
          begin
               try
                  if ( FQryConflicto = nil ) then
                  begin
                       if ( FOperacion = ocSumar ) then
                          InitQueryConflicto( Q_FALTAS_SUMAR )
                       else
                           InitQueryConflicto( Q_FALTAS_SUSTITUIR );
                  end;
                  with oZetaProvider do
                  begin
                       with Dataset do
                       begin
                            ParamAsInteger( FQryConflicto, 'Empleado', FieldByName( 'CB_CODIGO' ).AsInteger );
                            ParamAsInteger( FQryConflicto, 'Motivo', FieldByName( 'FA_MOTIVO' ).AsInteger );
                            ParamAsChar( FQryConflicto, 'DiaHor', FieldByName( 'FA_DIA_HOR' ).AsString, K_ANCHO_DIA_HOR );
                            ParamAsDate( FQryConflicto, 'Fecha', FieldByName( 'FA_FEC_INI' ).AsDateTime );
                            ParamAsFloat( FQryConflicto, 'Dias', FieldByName( 'FA_DIAS' ).AsFloat );
                            ParamAsFloat( FQryConflicto, 'Horas', FieldByName( 'FA_HORAS' ).AsFloat );
                       end;
                       Ejecuta( FQryConflicto );
                  end;
               except
                     Response := rrSkip;
                     raise
               end;
          end;
     end;
     }
end;

procedure TdmServerNominaTimbrado.GetDatosLiquidacionEnd;
begin
     with Nomina do
     begin
          VerificaNominaEnd;
          PreparaDiasHorasEnd;
     end;
     FreeAndNil( FNominaLiquidacion );
     FreeAndNil( FVacaciones );
     FreeAndNil( FPrestaciones );
     FreeAndNil( FAguinaldoEmp );
end;

procedure TdmServerNominaTimbrado.GetDatosLiquidacionBegin;
begin
     InitPrestaciones;
     InitNomina;
     with Nomina do
     begin
          VerificaNominaBegin;  //ya se hace en liquidacionbegin
          PreparaDiasHorasBegin;
     end;
     with oZetaProvider do
     begin
          //CV: CreateQuery--ok
          //Los 3 datasets se destruyen en GetDatosLiquidacionEnd,
          //y nada mas se usan para la liquidacion.
          FNominaLiquidacion := CreateQuery( GetSQLScript( Q_LIQUIDACION_NOMINA ) );
          FVacaciones := CreateQuery( GetSQLScript( Q_LIQUIDACION_VACACIONES ) );
          FPrestaciones := CreateQuery( GetSQLScript( Q_LIQUIDACION_AGUINALDO ) );
          FAguinaldoEmp := CreateQuery( GetSQLScript( Q_LIQUIDACION_AGUINALDO_EMP ) );
          with DatosPeriodo do
          begin
               ParamAsInteger( FNominaLiquidacion, 'Year', Year );
               ParamAsInteger( FNominaLiquidacion, 'Tipo', Ord( Tipo ) );
               ParamAsInteger( FNominaLiquidacion, 'Numero', Numero );
          end;
     end;
end;

function TdmServerNominaTimbrado.GetDatosLiquidacion( DataSetEmpleado: TDataset; const iEmpleado: TNumEmp; dBaja: TDate = 0 ): TDatosLiquidacion;
const
     K_LIQUIDACION = 'Liquidaci�n';
     K_MES_DICIEMBRE = 12;
var
   rDiasTabla, rDiasBase, rDiasCurso, rPrima,rPendiente,rPendientePV,rProp: Currency;
   sTablaSS: String;
   dIngreso, dAntiguedad,dAniversario: TDate;

begin
     with FNominaLiquidacion do
     begin
          Active := False;
     end;
     with FVacaciones do
     begin
          Active := False;
     end;
     FAguinaldoEmp.Active := FALSE;
     with oZetaProvider do
     begin
          ParamAsInteger( FNominaLiquidacion, 'Empleado', iEmpleado );
          ParamAsInteger( FVacaciones, 'Empleado', iEmpleado );
          ParamAsInteger( FAguinaldoEmp, 'Empleado', iEmpleado );
     end;
     with Result do
     begin
          Empleado := iEmpleado;
          Global := True;
          with FNominaLiquidacion do
          begin
               //CV-Active Revisado
               Active := True;
               if EOF then
               begin
                    Active := False;
                    with Nomina do
                    begin
                         VerificaRegistro( iEmpleado );
                         PreparaDiasHoras( iEmpleado );
                    end;
                    Active := True;
               end;
               if EOF then
               begin
                    Ordinarias := 0;
                    Dobles := 0;
                    Triples := 0;
                    Tardes := 0;
                    Adicionales := 0;
                    PrimaDominical := 0;
                    Tipo := lnLiquidacion;
                    Observaciones := K_LIQUIDACION;
                    NominaExiste := false;
               end
               else
               begin
                    Ordinarias := FieldByName( 'NO_HORAS' ).AsFloat;
                    Dobles := FieldByName( 'NO_DOBLES' ).AsFloat;
                    Triples := FieldByName( 'NO_TRIPLES' ).AsFloat;
                    Tardes := FieldByName( 'NO_TARDES' ).AsFloat;
                    Adicionales := FieldByName( 'NO_ADICION'  ).AsFloat;
                    PrimaDominical := FieldByName( 'NO_HORA_PD' ).AsFloat;
                    Tipo := eLiqNomina( FieldByName( 'NO_LIQUIDA' ).AsInteger );
                    Observaciones := FieldByName( 'NO_OBSERVA' ).AsString;
                    NominaExiste := true;
                    if ( Observaciones = '' ) then
                       Observaciones := K_LIQUIDACION;
               end;
               Active := False;
          end;
          with FVacaciones do
          begin
               //CV-Active Revisado
               Active := True;
               sTablaSS := FieldByName( 'CB_TABLASS' ).AsString;
               dAntiguedad := FieldByName( 'CB_FEC_ANT' ).AsDateTime;
               dIngreso := FieldByName( 'CB_FEC_ING' ).AsDateTime;
               if ( dBaja = NullDateTime ) then
               begin
                    dBaja := FieldByName( 'CB_FEC_BAJ' ).AsDateTime;
               end;
               Baja := dBaja;
               dAniversario:= NextDate( dAntiguedad, dBaja );
               rProp:=  oZetaCreator.dmPrestaciones.VacaProporcional( dAntiguedad,
                                                                           FieldByName( 'CB_DER_FEC' ).AsDateTime,
                                                                           dBaja,
                                                                           sTablaSS,
                                                                           rDiasTabla,
                                                                           rDiasBase,
                                                                           rPrima
                                                                           {$ifdef FLEXIBLES}
                                                                           , iEmpleado
                                                                           {$endif}
                                                                            );
               Vacaciones := rProp +  FieldByName( 'CB_DER_PAG' ).AsFloat - FieldByName( 'CB_V_PAGO' ).AsFloat;
               PrimaVaca := rPrima + FieldByName( 'CB_DER_PV' ).AsFloat - FieldByName( 'CB_V_PRIMA' ).AsFloat;
               if oZetaProvider.GetGlobalBooleano( K_GLOBAL_AVENT ) then
                  VacacionesActual := Redondea( rDiasTabla * ( rDiasBase / K_DIAS_ANNO_AVENT ) )
               else
               begin
                    {$ifdef ANTES}
                    VacacionesActual := Redondea( rDiasTabla * ( rDiasBase / 365 ) );
                    {$else}
                    if (  dAniversario > FieldByName( 'CB_DER_FEC' ).AsDateTime) then
                    begin
                         rPendiente := oZetaCreator.dmPrestaciones.VacaProporcional( dAntiguedad, FieldByName( 'CB_DER_FEC' ).AsDateTime, dAniversario,
                                                                           sTablaSS, rDiasTabla, rDiasBase, rPendientePV{$ifdef FLEXIBLES}, iEmpleado{$endif} );
                    end
                    else
                    begin
                         rPendiente := 0;
                    end;
                    VacacionesActual:= rProp - rPendiente;
                    {$endif}
               end;
               VacacionesAntes := Vacaciones - VacacionesActual;
               if oZetaProvider.GetGlobalBooleano( K_GLOBAL_AVENT ) then
                  VacacionesFormula := '[ ' +
                                       FormatFloat( '#,0.00;-#,0.00', rDiasTabla ) +
                                       ' ] X [ ' +
                                       FormatFloat( '#,0;-#,0', rDiasBase ) +
                                       ' / 360 ]'
               else
                   VacacionesFormula := '[ ' +
                                        FormatFloat( '#,0.00;-#,0.00', rDiasTabla ) +
                                        ' ] X [ ' +
                                        FormatFloat( '#,0;-#,0', rDiasBase ) +
                                        ' / 365 ]';

               Active := False;
          end;

          with FPrestaciones  do
          begin
               Active := False;
          end;
          with oZetaProvider do
          begin
               ParamAsChar( FPrestaciones, 'Tabla', sTablaSS, K_ANCHO_TABLA_SS );
               // EZM: 11/Oct/99: Se tiene que redondear la antiguedad //
               ParamAsInteger( FPrestaciones, 'Year', Trunc( ZetaCommonTools.rMax( ZetaCommonTools.Years( dAntiguedad, dBaja ), 0 ) ) + 1 );
               ParamAsInteger( FAguinaldoEmp, 'Year', TheYear( dBaja ) );   //Aqu� se asigna el a�o por que al investigar Vacaciones^ puede cambiar
          end;

          { *** Nuevos Datos Agregados *** }
          with DataSetEmpleado do
          begin
               Incapacidades := FieldByName( 'NO_DIAS_IN' ).AsInteger;
               Faltas := FieldByName( 'NO_DIAS_FI' ).AsInteger;
          end;

          with FAguinaldoEmp do
          begin
               Active := TRUE;
               AguinaldoPagado := FieldByName( 'AC_ANUAL' ).AsFloat;
               Active := FALSE;
          end;
          { Cuando ya se ha pagado Aguinaldo y es mes de Diciembre }
          ConfirmarAguinaldo := ( ( ZetaCommonTools.TheMonth( Baja ) = K_MES_DICIEMBRE ) and ( AguinaldoPagado > 0 ) );

          with FPrestaciones  do
          begin
               //CV-Active Revisado
               Active := True;
               if EOF then
               begin
                    Aguinaldo := 0;
                    AguinaldoFormula := '';
                    DiasEnCurso := 0;
                    DiasConsiderados := 0;
               end
               else
               begin
                    // Dias Proporcionales de Aguinaldo //
                    rDiasCurso  := ( dBaja - ZetaServerTools.dMax( ZetaCommonTools.FirstDayOfYear( ZetaCommonTools.TheYear( dBaja ) ), dIngreso ) + 1 );
                    DiasEnCurso := rDiasCurso;
                    rDiasCurso  := ( rDiasCurso - Faltas - Incapacidades );
                    DiasConsiderados := rDiasCurso;
                    Aguinaldo   := Redondea( FieldByName( 'PT_DIAS_AG' ).AsFloat * ( rDiasCurso ) / 365 );
                    AguinaldoFormula := '[ ' +
                                        FormatFloat( '#,0.00;-#,0.00', FieldByName( 'PT_DIAS_AG' ).AsFloat ) +
                                        ' ] X [ ' +
                                        FormatFloat( '#,0;-#,0', rDiasCurso ) +
                                        ' / 365 ]'
               end;
               Active := False;
          end;
     end;
end;

{ **************** Interfases ( Paletitas ) ***************** }

function TdmServerNominaTimbrado.GetDatosJornada(Empresa: OleVariant; const Turno: WideString; FechaInicial, FechaFinal: TDateTime): OleVariant;
begin
     InitLog(Empresa,'GetDatosJornada');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
          InitRitmos;
          InitNomina;
          try
             with Nomina do
             begin
                  try
                     GetDatosJornadaBegin;
                     Result := ZetaCommonTools.DatosJornadaToVariant( GetDatosJornada( Turno, FechaInicial, FechaFinal ) );
                  finally
                         GetDatosJornadaEnd;
                  end;
             end;
          finally
                 ClearNomina;
          end;
     end;
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.GetEmpleadoClasifi(Empresa: OleVariant; iEmpleado, iYear, iTipo, iPeriodo: Integer): OleVariant;
begin
     InitLog(Empresa,'GetEmpleadoClasifi');
     SetTablainfo( fnDatosClasifi );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          TablaInfo.Filtro := Format( K_FILTRO_PERIODO_EMPLEADO, [ iEmpleado, iYear, iTipo, iPeriodo ] );
          Result := GetTabla( Empresa );
     end;
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.GetDatosTotales(Empresa: OleVariant; iYear, iTipo,iPeriodo: Integer; const sConfiden: WideString): OleVariant;
const
     K_FILTRO_CONFID = 'AND CB_NIVEL0 = ''%s''';
var
   sFiltroConfid :string;
begin
     InitLog(Empresa,'GetDatosTotales');
     sFiltroConfid := VACIO;
     with oZetaProvider do
     begin
          if sConfiden <> VACIO then
             sFiltroConfid := Format( K_FILTRO_CONFID,[ sConfiden ] );
          Result := OpenSQL(Empresa, Format( GetSQLScript ( Q_FILTRO_NIVEL_CONFI), [  iTipo,iYear, iPeriodo, sFiltroConfid ] ),True);
     end;
     EndLog;SetComplete;
end;


function TdmServerNominaTimbrado.GetDatosExcepGlobales(Empresa: OleVariant; iYear, iTipo, iPeriodo: Integer): OleVariant;
begin
     InitLog(Empresa,'GetDatosExcepGlobales');
     SetTablaInfo( fnExcepGlobales );
     with oZetaProvider do
     begin
          Tablainfo.Filtro := Format( K_FILTRO_PERIODO, [ iYear, iTipo, iPeriodo ] );
          Result := GetTabla( Empresa );
     end;
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.GetDatosExcepciones(Empresa: OleVariant; iYear, iTipo, iPeriodo: Integer; lSoloAltas: WordBool): OleVariant;
begin
     InitLog(Empresa,'GetDatosExcepciones');
     Result:= oZetaProvider.OpenSQL( Empresa, Format( GetScript( fnExcepciones ), [iYear, itipo, iPeriodo, Nivel0( Empresa )] ), not lSoloAltas );
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.GetMontos(Empresa: OleVariant; iEmpleado, iYear, iTipo, iPeriodo: Integer): OleVariant;
begin
     InitLog(Empresa,'GetMontos');
     SetTablaInfo( fnMontos );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( K_FILTRO_PERIODO_EMPLEADO, [ iEmpleado, iYear, iTipo, iPeriodo ] );
          Result := GetTabla( Empresa );
     end;
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.GetDatosExcepMontos(Empresa: OleVariant; iYear, iTipo, iPeriodo: Integer; lSoloAltas: WordBool): OleVariant;
begin
     InitLog(Empresa,'GetDatosExcepMontos');
     Result:= oZetaProvider.OpenSQL( Empresa, Format( GetScript( fnExcepMontos ), [ iYear, iTipo, iPeriodo, Nivel0( Empresa ) ] ), not lSoloAltas );
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.GetDatosNomina(Empresa: OleVariant; iEmpleado, iYear, iTipo, iPeriodo: Integer): OleVariant;
begin
     InitLog(Empresa,'GetDatosNomina');
     SetTablaInfo(fnDatosNomina);
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( K_FILTRO_PERIODO_EMPLEADO, [ iEmpleado, iYear, iTipo, iPeriodo ] );
          Result := GetTabla( Empresa );
     end;
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.GetNomDatosAsist(Empresa, Parametros: OleVariant; out Tarjetas: OleVariant): OleVariant;
var
   iEmpleado, iYear, iTipo, iNumero: Integer;
   dInicial, dFinal: TDate;
   sSQL: String;
{$ifdef QUINCENALES}
   FDatosEmpleadoPrenomina: TZetaCursor;
{$endif}
begin
     InitLog(Empresa,'GetNomDatosAsist');
     with oZetaProvider do
     begin
          AsignaParamList( Parametros );
          with ParamList do
          begin
               iYear     := ParamByName( 'Year' ).AsInteger;
               iTipo     := ParamByName( 'Tipo' ).AsInteger;
               iNumero   := ParamByName( 'Numero' ).AsInteger;
               iEmpleado := ParamByName( 'Empleado' ).AsInteger;
               dInicial  := ParamByName( 'FechaIni' ).AsDateTime;
               dFinal    := ParamByName( 'FechaFin' ).AsDateTime;
          end;
{$ifdef QUINCENALES}
          EmpresaActiva := Empresa;
          GetDatosPeriodo;
          if ( DatosPeriodo.Inicio <> DatosPeriodo.InicioAsis ) or
             ( DatosPeriodo.Fin <> DatosPeriodo.FinAsis ) then     // Si no hay desfase no se puede presentar el cambio de fechas de inicio y fin
          begin
               FDatosEmpleadoPrenomina := CreateQuery;
               try
                  if AbreQueryScript( FDatosEmpleadoPrenomina, Format( GetSQLScript( Q_DATOS_EMPLEADO_PRENOMINA ), [ iEmpleado ] ) ) then
                  begin
                       with FDatosEmpleadoPrenomina do
                       begin
                            if ( not IsEmpty ) then
                            begin
                                 ZetaCommonTools.EsBajaenPeriodo( FieldByName( 'CB_FEC_ING' ).AsDateTime,
                                                                  FieldByName( 'CB_FEC_BAJ' ).AsDateTime,
                                                                  DatosPeriodo, dInicial, dFinal );       // Si la baja cae en el periodo revisa si hay que cambiar fechas
                            end;;
                       end;
                  end;
               finally
                  FreeAndNil( FDatosEmpleadoPrenomina );
               end;
          end;
{$endif}
          SetTablaInfo( fnDatosAsist );
          TablaInfo.Filtro := Format( K_FILTRO_PERIODO_EMPLEADO, [ iEmpleado, iYear, iTipo, iNumero ] );
          Result := GetTabla( Empresa );
          sSQL := Format( GetSQLScript( Q_PRE_NOMINA ), [ iEmpleado, DateToStrSQL( dInicial ), DateToStrSQL( dFinal )
          {$ifdef CAMBIO_TNOM}
          ,Ord( DatosPeriodo.Tipo )
          {$endif}
           ] );
          Tarjetas := oZetaProvider.OpenSQL( Empresa, sSQL, TRUE );
     end;
     EndLog;SetComplete;
end;


function TdmServerNominaTimbrado.GetMovMontos(Empresa: OleVariant; iEmpleado, iYear, iTipo,
         iPeriodo: Integer; out oTotales: OleVariant): OleVariant;
begin
     InitLog(Empresa,'GetMovMontos');
     SetTablaInfo(fnMovMontos);
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( K_FILTRO_PERIODO_EMPLEADO, [ iEmpleado, iYear, iTipo, iPeriodo ] );
          Result := GetTabla( Empresa );
          oTotales := GetMontos( Empresa, iEmpleado, iYear, iTipo, iPeriodo );
     end;
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.GetDiasHoras(Empresa: OleVariant; iEmpleado, iYear, iTipo, iPeriodo: Integer): OleVariant;
begin
     InitLog(Empresa,'GetDiasHoras');
     SetTablaInfo( fnDiasHoras );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( K_FILTRO_PERIODO_EMPLEADO, [ iEmpleado, iYear, iTipo, iPeriodo ] );
          SetDetailInfo( fnDiasHoras );
          Result := GetMasterDetail( Empresa );
     end;
     EndLog;SetComplete;
end;


function TdmServerNominaTimbrado.GetFaltas(Empresa: OleVariant; iEmpleado, iYear, iTipo, iPeriodo: Integer; out oTotales: OleVariant): OleVariant;
begin
     InitLog(Empresa,'GetFaltas');
     SetTablaInfo(fnFaltas);
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( K_FILTRO_PERIODO_EMPLEADO, [ iEmpleado, iYear, iTipo, iPeriodo ] );
          Result := GetTabla( Empresa );
          oTotales := GetDatosNomina( Empresa, iEmpleado, iYear, iTipo, iPeriodo );
     end;
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.GetPagoRecibos(Empresa: OleVariant; iYear, iTipo, iPeriodo: Integer; lSoloAltas: WordBool): OleVariant;
begin
     InitLog(Empresa,'GetPagoRecibos');
     Result := oZetaProvider.OpenSQL( Empresa, Format( GetScript( fnPagoRecibos ), [iYear, iTipo, iPeriodo, Nivel0( Empresa, 'COLABORA' )] ), not lSoloAltas );
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.GrabaEmpleadoClasifi(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     InitLog(Empresa,'GrabaEmpleadoClasifi');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          SetTablaInfo( fnDatosClasifi );
          TablaInfo.AfterUpdateRecord := CheckStatusNomina;
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
     end;
     EndLog;SetComplete;
end;

procedure TdmServerNominaTimbrado.CheckStatusNomina(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
var
   oPeriodo: TDatosPeriodo;
   sPago:string;
begin
     oZetaProvider.initArregloTPeriodo;
     with oPeriodo, DeltaDS do
     begin
          Year := ZetaServerTools.CampoAsVar( FieldByName( 'PE_YEAR' ) );
          Tipo := eTipoPeriodo( ZetaServerTools.CampoAsVar( FieldByName( 'PE_TIPO' ) ) );
          Numero := ZetaServerTools.CampoAsVar( FieldByName( 'PE_NUMERO' ) );
          if ( UpdateKind = ukModify ) and ZetaServerTools.CambiaCampo( FieldByName( 'CB_BAN_ELE' ) ) then
          begin
               if StrVacio( FieldByName( 'CB_BAN_ELE' ).AsString )then
                  sPago:= 'pago en efectivo '
               else
                   sPago:= 'pago con banca electr�nica ';
               oZetaProvider.EscribeBitacora( tbNormal, clbMontoNomina, ZetaServerTools.CampoAsVar(FieldByName('CB_CODIGO')), Date, 'Cambi� la forma de pago a '+sPago+ GetPeriodoInfo( Year, Numero, Tipo ), VACIO );
          end;
          if ( UpdateKind = ukDelete ) then
             oZetaProvider.EscribeBitacora( tbNormal, clbNomina, ZetaServerTools.CampoAsVar(FieldByName('CB_CODIGO')), Date, 'N�mina Borrada ' + GetPeriodoInfo( Year, Numero, Tipo ), VACIO);
     end;
     SetStatusPeriodo( oPeriodo );
end;

{
function TdmServerNomina.GetBitacoraMensaje(eClase: eClaseBitacora) : string;
begin
     case eClase of
          clbExcepcionMonto: Result:= 'Excep Monto';
          clbExcepcionDias: Result := 'Excep D�as/Hrs';
          clbExcepcionGlobal: Result := 'Excep Global';
          clbMontoNomina: Result := 'Monto de N�mina';
          else Result := 'Desconocido';
     end;
end;
}

function TdmServerNominaTimbrado.GetBitacoraMasInfo(DataSet : TzProviderClientDataSet; eClase: eClaseBitacora) : string;
begin
     with Dataset do
          case eClase of
               clbExcepcionMonto, clbMontoNomina: Result := GetMontosInfo( CampoOldAsVar( FieldByName( 'CO_NUMERO' ) ),
                                                                           CampoOldAsVar( FieldByName( 'MO_REFEREN' ) ) );
               clbExcepcionDias: Result := GetFaltasInfo( CampoOldAsVar( FieldByName( 'FA_DIA_HOR' ) ),
                                                          CampoOldAsVar( FieldByName( 'FA_FEC_INI' ) ),
                                                          CampoOldAsVar( FieldByName( 'FA_MOTIVO' ) ) );
               clbExcepcionGlobal: Result := GetMontosInfo( CampoOldAsVar( FieldByName( 'CO_NUMERO' ) ), VACIO );
               else Result := VACIO;
          end;

     if StrLleno(Result) then
        Result := Result + CR_LF + CR_LF;
end;


procedure TdmServerNominaTimbrado.GrabaCambiosBitacora( DataSet : TzProviderClientDataSet; const sField1, sField2: string; const eClase: EClaseBitacora; const eOperacion : eOperacionConflicto; const rMonto: Currency );
 var sField : string;
     rMontoNuevo : Currency;
begin
     oZetaProvider.InitArregloTPeriodo;
     with DataSet do
     begin
          sField := sField1;
          rMontoNuevo := CampoAsVar( FieldByName( sField ) );
          if rMontoNuevo = 0 then
          begin
               sField := sField2;
               rMontoNuevo := CampoAsVar( FieldByName( sField ) );
          end;

          if eOperacion = ocSumar then
             rMontoNuevo := rMontoNuevo + rMonto;

          oZetaProvider.EscribeBitacora( tbNormal,
                                         eClase,
                                         CampoAsVar( FieldByName( 'CB_CODIGO' ) ),
                                         NullDateTime,
                                         'Modificaci�n de ' + Nomina.GetBitacoraMensaje(eClase) + ' '+
                                         GetPeriodoInfo( ZetaServerTools.CampoOldAsVar( FieldByName( 'PE_YEAR' ) ),
                                                         ZetaServerTools.CampoOldAsVar( FieldByName( 'PE_NUMERO' ) ),
                                                         eTipoPeriodo( ZetaServerTools.CampoOldAsVar( FieldByName( 'PE_TIPO' ) ) ) ),
                                         GetBitacoraMasInfo(Dataset, eClase) +
                                         sField + CR_LF + ' De: ' + FloatToStr( rMonto ) + CR_LF +
                                                          ' A : ' + FloatToStr( rMontoNuevo ) );
     end;
end;

procedure TdmServerNominaTimbrado.GrabaCambiosBitacora( DataSet: TzProviderClientDataSet; UpdateKind: TUpdateKind);
 var
      eClase : eClaseBitacora;
      sTitulo : string;
      iEmpleado : integer;

 procedure DatosBitacora( const eClaseNomina : eClaseBitacora; const lEmpleado : Boolean );
 begin
      eClase  := eClaseNomina;
      oZetaProvider.InitArregloTPeriodo;
      with DataSet do
      begin
           sTitulo := Nomina.GetBitacoraMensaje(eClase) + ' ' +
                      GetPeriodoInfo( ZetaServerTools.CampoOldAsVar( FieldByName( 'PE_YEAR' ) ),
                                      ZetaServerTools.CampoOldAsVar( FieldByName( 'PE_NUMERO' ) ),
                                      eTipoPeriodo( ZetaServerTools.CampoOldAsVar( FieldByName( 'PE_TIPO' ) ) ) );
           if lEmpleado then
              iEmpleado := ZetaServerTools.CampoOldAsVar( FieldByName( 'CB_CODIGO' ) )
           else
               iEmpleado := 0;

      end;
 end;
 var
    iUsuario : integer;
    sMasInfo : string;
begin
     if UpdateKind in [ ukModify, ukDelete ] then
     begin
          eClase := clbNinguno;

          case FFormaNomina of
               fnFaltas: DatosBitacora( clbExcepcionDias, TRUE );
               fnExcepGlobales: DatosBitacora( clbExcepcionGlobal, FALSE );
               fnMovMontos :
               begin
                    iUsuario := ZetaServerTools.CampoOldAsVar( DataSet.FieldByName( 'US_CODIGO' ) );
                    if iUsuario > 0 then
                       DatosBitacora( clbExcepcionMonto, TRUE )
                    else
                        DatosBitacora( clbMontoNomina, TRUE )
               end;
          end;

          sMasInfo := GetBitacoraMasInfo(DataSet, eClase);

          with oZetaProvider do
          begin
               if UpdateKind = ukModify then
                  CambioCatalogo( sTitulo, eClase, DataSet, iEmpleado, sMasInfo )
               else
                   BorraCatalogo( sTitulo, eClase, DataSet, iEmpleado, sMasInfo );
          end;
     end;
end;

procedure TdmServerNominaTimbrado.GrabaCambiosBitacoraEvent( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
begin
     GrabaCambiosBitacora( DeltaDS, UpdateKind );
end;

procedure TdmServerNominaTimbrado.ActualizarStatusNomina(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
var
   FPeriodo: TDatosPeriodo;
   FUpdateStatus: TZetaCursor;
   FStatusSimula:eStatusSimFiniquitos;
begin
     with DeltaDS do
     begin
          with FPeriodo do
          begin
               case UpdateKind of
                    ukInsert:
                    begin
                         Year := FieldByName( 'PE_YEAR' ).AsInteger;
                         Tipo := eTipoPeriodo( FieldByName( 'PE_TIPO' ).AsInteger );
                         Numero := FieldByName( 'PE_NUMERO' ).AsInteger;
                    end;
               else
                   begin
                        Year := ZetaServerTools.CampoAsVar( FieldByName( 'PE_YEAR' ) );
                        Tipo := eTipoPeriodo( ZetaServerTools.CampoAsVar( FieldByName( 'PE_TIPO' ) ) );
                        Numero := ZetaServerTools.CampoAsVar( FieldByName( 'PE_NUMERO' ) );
                   end;
               end;
          end;
     end;
     with oZetaProvider do
     begin
          InitGlobales;
          FStatusSimula := ssfSinAprobar;
          if ( not (GetGlobalBooleano(K_GLOBAL_SIM_FINIQ_APROBACION ) ))then
          begin
               FStatusSimula := ssfAprobada;
          end;
          { Cambia el Status de los registros de NOMINA a SinCalcular para }
          { aquellos registros cuyo Status es mayor a SinCalcular }
          with FPeriodo do
          begin

               //CV: CreateQuery--ok
               //Se destruye 2 lineas abajo, es un Dataset local
               FUpdateStatus := CreateQuery( Format( GetSQLScript( Q_EXCEP_GLOBAL_SET_STATUS ),
                                             [ Ord( spSinCalcular ),
                                               Ord( FStatusSimula ),
                                               Ord( spSinCalcular ),
                                               Year,
                                               Ord( Tipo ),
                                               Numero ] ) );
          end;
          Ejecuta( FUpdateStatus );
          FreeAndNil(FUpdateStatus);
          { Actualiza el Status del Periodo }
          SetStatusPeriodo( FPeriodo );
     end;

     GrabaCambiosBitacora( DeltaDS, UpdateKind );

end;

function TdmServerNominaTimbrado.GrabaExcepGlobales(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     InitLog(Empresa,'GrabaExcepGlobales');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          SetTablaInfo( fnExcepGlobales );
          with TablaInfo do
          begin
               AfterUpdateRecord := ActualizarStatusNomina;
          end;
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
     end;
     EndLog;SetComplete;
end;


function TdmServerNominaTimbrado.GrabaMovMontos(Empresa, Parametros, oDelta: OleVariant; out ErrorCount: Integer; out ErrorData: OleVariant): OleVariant;
begin
     InitLog(Empresa,'GrabaMovMontos');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
          AsignaParamList( Parametros );
          SetOperacionConflicto;
          GetDatosPeriodo;
          InitNomina;
          CreateErrorMontosDataSet;
          try
             with Nomina do
             begin
                  try
                     InitBufferEmpleado;
                     VerificaNominaBegin;
                     SetTablaInfo( fnMovMontos );
                     with TablaInfo do
                     begin
                          BeforeUpdateRecord := VerificarNomina;
                          AfterUpdateRecord := GrabaCambiosBitacoraEvent;
                          OnUpdateError := ErrorMontos;
                     end;
                     Result := GrabaTablaGrid( Empresa, oDelta, ErrorCount );
                     CalculaStatusPeriodo;
                  finally
                         VerificaNominaEnd;
                  end;
             end;
          finally
                 ClearNomina;
                 ErrorData := cdsLista.Data;
                 FreeAndNil( FQryConflictoMontos );
                 FreeAndNil( FQrySumarMontos );
                 FreeAndNil( FQrySustituirMontos );
                 FreeAndNil( FQryBorrarMontos );
                 FreeAndNil( FQryConcepto );
          end;
     end;
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.GrabaFaltas(Empresa, Parametros, oDelta: OleVariant; out ErrorCount: Integer; out ErrorData: OleVariant): OleVariant;
begin
     InitLog(Empresa,'GrabaFaltas');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
          AsignaParamList( Parametros );
          SetOperacionConflicto;
          GetDatosPeriodo;
          InitNomina;
          CreateErrorDiasHorasDataSet;
          try
             with Nomina do
             begin
                  try
                     InitBufferEmpleado;
                     PreparaDiasHorasBegin;
                     try
                        VerificaNominaBegin;
                        SetTablaInfo( fnFaltas );
                        with TablaInfo do
                        begin
                             BeforeUpdateRecord := PrepararDiasHoras;
                             AfterUpdateRecord := GrabaCambiosBitacoraEvent;
                             OnUpdateError      := ErrorDiasHoras;
                        end;
                        Result := GrabaTablaGrid( Empresa, oDelta, ErrorCount );
                        if ( FEmpleado > 0 ) then
                        begin
                             EmpiezaTransaccion;
                             try
                                PreparaDiasHoras( FEmpleado ); { Procesa Ultimo Empleado }
                                TerminaTransaccion( True );
                             except
                                   on Error: Exception do
                                   begin
                                        TerminaTransaccion( False );
                                   end;
                             end;
                             CalculaStatusPeriodo;
                        end;
                     finally
                            VerificaNominaEnd;
                     end;
                  finally
                         PreparaDiasHorasEnd;
                  end;
             end;
          finally
                 ClearNomina;
                 ErrorData:= cdsLista.Data;
                 FreeAndNil( FQryConflictoFaltas );
                 FreeAndNil( FQrySumarFaltas );
                 FreeAndNil( FQrySustituirFaltas );
                 FreeAndNil( FQryBorrarFaltas );
          end;
     end;
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.GrabaNomina(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     InitLog(Empresa,'GrabaNomina');
     SetTablaInfo( fnDatosNomina );
     Result := oZetaProvider.GrabaTabla( Empresa, oDelta, ErrorCount );
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.GrabaPrenomina(Empresa, Parametros, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     InitLog(Empresa,'GrabaPrenomina');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          GetDatosPeriodo;
          InitGlobales;
          InitRitmos;
          InitNomina;
          try
             with Nomina do
             begin
                  try
                     RefrescaPrenominaBegin;
                     SetTablaInfo( fnMovDatosAsist );
                     {
                     SetDetailInfo( fnDatosAsist );
                     }
                     with TablaInfo do
                     begin
                          OnUpdateData := SetPrenominaValues;
                          {
                          AfterUpdateRecord := RecalcularPrenomina;
                          }
                     end;
                     Result := GrabaTablaGrid( Empresa, oDelta, ErrorCount );
                     RefrescaPrenomina( FEmpleado );
                  finally
                         RefrescaPrenominaEnd;
                  end;
             end;
          finally
                 ClearNomina;
          end;
     end;
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.GrabaDiasHoras(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     InitLog(Empresa,'GrabaDiasHoras');
     SetTablaInfo( fnDiasHoras );
     SetDetailInfo( fnDiasHoras );
     Result := oZetaProvider.GrabaMasterDetail( Empresa, oDelta, ErrorCount );
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.GrabaPagoRecibos(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     InitLog(Empresa,'GrabaPagoRecibos');
     SetTablaInfo( fnPagoRecibos );
     FRecibosEmpresa:= Empresa;
     Result := oZetaProvider.GrabaTablaGrid( Empresa, oDelta, ErrorCount );
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.ValidaNominaEmpleado(Empresa: OleVariant; iEmpleado, iYear, iTipo, iPeriodo: Integer; out dFecha: TDateTime; out iUsuario: Integer): WordBool;
begin
     InitLog(Empresa,'ValidaNominaEmpleado');
     with cdsLista do
     begin
          Lista := oZetaProvider.OpenSQL( Empresa, Format( GetScript( fnValidaNomina ), [ iEmpleado, iYear, iTipo, iPeriodo ] ), True );
          Result := ( RecordCount > 0 );
          if Result then
          begin
               dFecha := FieldByName( 'NO_FEC_PAG' ).AsDateTime;
               iUsuario := FieldByName( 'NO_USR_PAG' ).AsInteger;
          end
          else
          begin
               dFecha := NullDateTime;
               iUsuario := 0;
          end;
     end;
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.GetLiquidacion(Empresa: OleVariant; Empleado: Integer; Parametros: OleVariant; out Ahorros, Prestamos: OleVariant; Ingreso: TDateTime): OleVariant;
var
   dFechaBaja: TDate;
   iPeriodo: Integer;
begin
     InitLog(Empresa,'GetLiquidacion');
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             InitBroker;
             InitGlobales;
             AsignaParamList( Parametros );
             GetDatosPeriodo;
             dFechaBaja := ParamList.ParamByName( 'FechaBaja' ).AsDateTime;
             GetLiquidacionBuildDataSet( Empleado );
             GetDatosLiquidacionBegin;
             with cdsLista do
             begin
                  InitTempDataset;
                  AddIntegerField( 'CB_CODIGO' );
                  AddIntegerField( 'NO_LIQUIDA' );
                  AddFloatField( 'NO_HORAS' );
                  AddFloatField( 'NO_DOBLES' );
                  AddFloatField( 'NO_TRIPLES' );
                  AddFloatField( 'NO_TARDES' );
                  AddFloatField( 'NO_ADICION' );
                  AddFloatField( 'NO_HORA_PD' );
                  AddFloatField( 'NO_DIAS_VA' );
                  AddFloatField( 'NO_DIAS_AG' );
                  AddFloatField( 'VA_ANTES' );
                  AddFloatField( 'VA_ACTUAL' );
                  AddStringField( 'NO_OBSERVA', 30 );
                  AddStringField( 'VA_FORMULA', 30 );
                  AddStringField( 'AG_FORMULA', 30 );
                  AddBooleanField( 'NO_EXISTE_LIQ' );
                  AddDateField( 'CB_FEC_BAJ' );    { ** Campos Nuevos Agregados en la versi�n 2.4.97 ** }
                  AddBooleanField( 'NO_ASK_AG' );
                  AddFloatField( 'NO_DIAS_IN' );
                  AddFloatField( 'NO_DIAS_FI' );
                  AddFloatField( 'NO_ANT_AG' );
                  AddFloatField( 'NO_DIAS_CU' );
                  AddFloatField( 'NO_DIAS_CO' );
                  AddFloatField( 'NO_DIAS_PV' );
                  CreateTempDataset;
                  Append;
                  FieldByName( 'CB_CODIGO' ).AsInteger := Empleado;
                  with GetDatosLiquidacion( SQLBroker.SuperReporte.DataSetReporte, Empleado, dFechaBaja ) do
                  begin
                       FieldByName( 'NO_LIQUIDA' ).AsInteger := ZetaCommonTools.iMax( 0, Ord( Tipo ) - 1 ); { Para usar TDBRadioButton en Cliente sin el primer elemento del Ennumerado }
                       FieldByName( 'NO_HORAS' ).AsFloat := Ordinarias;
                       FieldByName( 'NO_DOBLES' ).AsFloat := Dobles;
                       FieldByName( 'NO_TRIPLES' ).AsFloat := Triples;
                       FieldByName( 'NO_TARDES' ).AsFloat := Tardes;
                       FieldByName( 'NO_ADICION' ).AsFloat := Adicionales;
                       FieldByName( 'NO_HORA_PD' ).AsFloat := PrimaDominical;
                       FieldByName( 'NO_DIAS_VA' ).AsFloat := Vacaciones;
                       FieldByName( 'NO_DIAS_AG' ).AsFloat := Aguinaldo;
                       FieldByName( 'VA_ANTES' ).AsFloat := VacacionesAntes;
                       FieldByName( 'VA_ACTUAL' ).AsFloat := VacacionesActual;
                       FieldByName( 'NO_OBSERVA' ).AsString := Observaciones;
                       FieldByName( 'VA_FORMULA' ).AsString := VacacionesFormula;
                       FieldByName( 'AG_FORMULA' ).AsString := AguinaldoFormula;

                       FieldByName( 'CB_FEC_BAJ' ).AsDateTime := Baja;
                       FieldByName( 'NO_ASK_AG' ).AsBoolean := ConfirmarAguinaldo;
                       FieldByName( 'NO_DIAS_IN' ).AsFloat := Incapacidades;
                       FieldByName( 'NO_DIAS_FI' ).AsFloat := Faltas;
                       FieldByName( 'NO_ANT_AG' ).AsFloat := AguinaldoPagado;
                       FieldByName( 'NO_DIAS_CU' ).AsFloat := DiasEnCurso;
                       FieldByName( 'NO_DIAS_CO' ).AsFloat := DiasConsiderados;
                       FieldByName( 'NO_DIAS_PV' ).AsFloat := PrimaVaca;
                  end;
                  FieldByName( 'NO_EXISTE_LIQ' ).AsBoolean := PuedeAgregarLiquidacion( Empleado,Ingreso, dFechaBaja, iPeriodo );
                  Post;
                  Result := Data;
             end;
             Ahorros := OpenSQL( Empresa, Format( GetSQLScript( Q_LIQUIDACION_AHORROS ), [ IntToStr( Empleado ) ] ), True );
             Prestamos := OpenSQL( Empresa, Format( GetSQLScript( Q_LIQUIDACION_PRESTAMOS ), [ IntToStr( Empleado ) ] ), True );
             GetDatosLiquidacionEnd;
        end;
     finally
            ClearBroker;
     end;
     EndLog;SetComplete;
end;

procedure TdmServerNominaTimbrado.GetLiquidacionBuildDataset( const iEmpleado: integer );
begin
     with SQLBroker do
     begin
          Init( enEmpleado );
          with Agente do
          begin
               AgregaColumna( 'CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
               AgregaAgenteFunciones( Agente );
               AgregaFiltro( Format( '( CB_CODIGO = %d )', [ iEmpleado ] ), True, Entidad );
          end;
          FiltroConfidencial( oZetaProvider.EmpresaActiva );
          BuildDataset( oZetaProvider.EmpresaActiva );
     end;
end;

procedure TdmServerNominaTimbrado.AgregaAgenteFunciones( oSQLAgente: TSQLAgente );
var
   dFechaInicial, dFechaFinal: TDate;
   oParam : TParam;
begin
     with oZetaProvider.ParamList do
     begin
          oParam := FindParam( 'Simulacion');
          if ( oParam <> NIL ) and ( ParamByName('Simulacion').AsString = K_GLOBAL_SI ) then
          begin
               dFechaInicial := FirstDayOfYear( ParamByName('Year').AsInteger );
               dFechaFinal := LastDayofYEar( ParamByName('Year').AsInteger );
          end
          else
          begin
               dFechaInicial := FirstDayOfYear( oZetaProvider.DatosPeriodo.Year );
               dFechaFinal := LastDayofYEar( oZetaProvider.DatosPeriodo.Year );
          end;
     end;

     with oSQLAgente do
     begin
          AgregaColumna( StrDef( oZetaProvider.GetGlobalString( K_GLOBAL_DEF_AGUINALDO_FALTAS ), '0' ), False, Entidad, tgFloat, 0, 'NO_DIAS_FI' );
          AgregaColumna( StrDef( oZetaProvider.GetGlobalString( K_GLOBAL_DEF_AGUINALDO_INCAPACIDADES ), '0' ), False, Entidad, tgFloat, 0, 'NO_DIAS_IN' );
          AgregaColumna( Comillas( FechaAsStr( dFechaInicial ) ), True, Entidad, tgFecha, 10, 'FechaInicial');
          AgregaColumna( Comillas( FechaAsStr( dFechaFinal ) ), True, Entidad, tgFecha, 10, 'FechaFinal');
     end;
end;

procedure TdmServerNominaTimbrado.GrabaLiquidacion(Empresa, Parametros, Datos, Ahorros, Prestamos: OleVariant);
var
   FLiquidacion: TDatosLiquidacion;
begin
     InitLog(Empresa,'GrabaLiquidacion');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
          AsignaParamList( Parametros );
          GetDatosPeriodo;
          InitNomina;
          try
             with Nomina do
             begin
                  LiquidacionBegin;
                  try
                     with cdsLista do
                     begin
                          Lista := Datos;
                          with FLiquidacion do
                          begin
                               Empleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                               Baja := FieldByName( 'CB_FEC_BAJ' ).AsDateTime;
                               Ordinarias := FieldByName( 'NO_HORAS' ).AsFloat;
                               Dobles := FieldByName( 'NO_DOBLES' ).AsFloat;
                               Triples := FieldByName( 'NO_TRIPLES' ).AsFloat;
                               Tardes := FieldByName( 'NO_TARDES' ).AsFloat;
                               Adicionales := FieldByName( 'NO_ADICION' ).AsFloat;
                               PrimaDominical := FieldByName( 'NO_HORA_PD' ).AsFloat;
                               Tipo := eLiqNomina( FieldByName( 'NO_LIQUIDA' ).AsInteger + 1 );  { Para usar TDBRadioButton en Cliente sin el primer elemento del Ennumerado }
                               Vacaciones := FieldByName( 'NO_DIAS_VA' ).AsFloat;
                               VacacionesAntes := FieldByName( 'VA_ANTES' ).AsFloat;
                               VacacionesActual := FieldByName( 'VA_ACTUAL' ).AsFloat;
                               VacacionesFormula := FieldByName( 'VA_FORMULA' ).AsString;
                               Aguinaldo := FieldByName( 'NO_DIAS_AG' ).AsFloat;
                               AguinaldoFormula := FieldByName( 'AG_FORMULA' ).AsString;
                               Observaciones := FieldByName( 'NO_OBSERVA' ).AsString;
                               PrimaVaca := FieldByName( 'NO_DIAS_PV' ).AsFloat;
                               Global := False;
                          end;
                          Lista := Ahorros;
                     end;
                     with cdsAuxiliar do
                     begin
                          Lista := Prestamos;
                     end;
                     EmpiezaTransaccion;
                     try
                        Liquidacion( FLiquidacion, cdsLista, cdsAuxiliar );
                        SetStatusPeriodo( DatosPeriodo );
                        TerminaTransaccion( True );
                     except
                           on Error: Exception do
                           begin
                                //TerminaTransaccion( False );
                                RollBackTransaccion;
                                raise;
                           end;
                     end;
                  finally
                         LiquidacionEnd;
                  end;
             end;
          finally
                 ClearNomina;
          end;
     end;
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.BorraUnPeriodoDataset(Empresa: OleVariant): TZetaCursor;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          //CV: CreateQuery--ok
          //Este metodo se manda llamar en
          //BorraNomina y BorrarPeriodosLista
          //aqui se destruye este dataset
          Result := CreateQuery;
          PreparaQuery( Result, GetScript( fnBorraPeriodo ) );
     end;
end;

procedure TdmServerNominaTimbrado.BorraUnPeriodo( Dataset: TZetaCursor; const iYear, iTipo, iPeriodo: Integer; const dMovimiento: TDate );
begin
     with oZetaProvider do
     begin
          InitArregloTPeriodo;
          EmpiezaTransaccion;
          try
             ParamAsInteger( Dataset, 'Year', iYear );
             ParamAsInteger( Dataset, 'Tipo', iTipo );
             ParamAsInteger( Dataset, 'Numero', iPeriodo );
             Ejecuta( Dataset );
             if ( Log = Nil ) then
                EscribeBitacora( tbNormal, clbPeriodoNomina, 0, dMovimiento, 'Definici�n de Periodo Borrada',
                              'Se Borr� la definici�n del periodo: ' + GetPeriodoInfo( iYear, iPeriodo, eTipoPeriodo( iTipo ) ) )
             else
                 Log.Evento( clbNinguno, 0, NullDateTime, 'Periodo ' + GetPeriodoInfo( iYear, iPeriodo, eTipoPeriodo( iTipo ) ) + ' Fu� Borrado' );
             TerminaTransaccion( True );
          except
                on Error: Exception do
                begin
                     TerminaTransaccion( False );
                     raise;
                end;
          end;
     end;
end;

function TdmServerNominaTimbrado.BorraNomina(Empresa: OleVariant;iUsuario: Integer; dFecha: TDateTime; iYear, iTipo, iPeriodo: Integer;BorraRegistros: WordBool; const Filtro: WideString): OleVariant;
var
   sScript: String;
   FDataSet: TZetaCursor;
   sFilter:string;
begin
      InitLog(Empresa,'BorraNomina');
     if BorraRegistros then
     begin
          with oZetaProvider do
          begin
               if StrLleno(Filtro )then
               begin
                    sFilter := ' and ( '+Filtro+' )';
               end;
               EmpresaActiva := Empresa;
               EmpiezaTransaccion;
               InitArregloTPeriodo;
               try
                  FDataSet := CreateQuery;
                  try
                     AbreQueryScript( FDataSet, Format( GetSQLScript( Q_BITACORA_BORRA_NOMINA ), [ iYear, iTipo, iPeriodo ] ) );
                     while not FDataSet.Eof do
                     begin
                          sScript := Format( GetScript( fnBorraNominas ), [ iYear, iTipo, iPeriodo, FDataSet.FieldByName('CB_CODIGO').AsInteger ,sFilter]);
                          ExecSQL( Empresa, sScript );
                          EscribeBitacora( tbNormal,clbNomina, FDataSet.FieldByName('CB_CODIGO').AsInteger, dFecha, 'N�mina Borrada: ' + GetPeriodoInfo( iYear, iPeriodo, eTipoPeriodo( iTipo ) ), Vacio );
                          FDataSet.Next;
                     end;
                     sScript := Format( GetScript( fnBorraPoliza ), [ iYear, iTipo, iPeriodo ] );
                     ExecSQL( Empresa, sScript );
                     sScript := Format( GetScript( fnUpdatePeriodo ), [ iUsuario, Comillas( DateToStrSql( dFecha ) ), iYear, iTipo, iPeriodo ] );
                     ExecSQL( Empresa, sScript );
                     TerminaTransaccion( True );
                  finally
                         FreeAndNil( FDataSet );
                  end;
               except
                     on Error: Exception do
                     begin
                          //TerminaTransaccion( False );
                          RollBackTransaccion;
                          raise;
                     end;
               end;
          end;
     end
     else
     begin
          //cv
          FDataSet := BorraUnPeriodoDataset( Empresa );
          try
             BorraUnPeriodo( FDataSet, iYear, iTipo, iPeriodo, dFecha );
          finally
             FreeAndNil(FDataSet);
          end;
     end;
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.BorrarPeriodosGetLista(Empresa, Parametros: OleVariant): OleVariant;
var
   iYear, iTipo, iInicial, iFinal: Integer;
begin
     with oZetaProvider do
     begin
          AsignaParamList( Parametros );
          with ParamList do
          begin
               iYear := ParamByName( 'Year' ).AsInteger;
               iTipo := ParamByName( 'Tipo' ).AsInteger;
               iInicial := ParamByName( 'NumeroInicial' ).AsInteger;
               iFinal := ParamByName( 'NumeroFinal' ).AsInteger;
          end;
          Result := OpenSQL( Empresa, Format( GetScript( fnBorraPeriodoLista ), [ iYear, iTipo, iInicial, iFinal ] ), True );
     end;
end;

procedure TdmServerNominaTimbrado.BorrarPeriodosParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'A�o: ' + IntToStr( ParamByName('Year').AsInteger ) +
                         K_PIPE + 'Tipo: ' + ZetaCommonLists.ObtieneElemento( lfTipoPeriodo, ParamByName('Tipo').AsInteger ) +
                         K_PIPE + 'Inicial: ' + IntToStr( ParamByName('NumeroInicial').AsInteger ) +
                         K_PIPE + 'Final: ' + IntToStr( ParamByName('NumeroFinal').AsInteger );
     end;
end;

function TdmServerNominaTimbrado.BorrarPeriodosLista(Empresa, Parametros, Lista: OleVariant): OleVariant;
var
   iYear, iTipo, iNumero: Integer;
   FDataset: TZetaCursor;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitArregloTPeriodo;
          BorrarPeriodosParametros;
          FDataset := BorraUnPeriodoDataset( Empresa );
          cdsLista.Lista := Lista;
          with cdsLista do
          begin
               if OpenProcess( prSISTBorrarNominas, RecordCount, FListaParametros ) then
               begin
                    while not Eof and CanContinue do
                    begin
                         iYear := FieldByName( 'PE_YEAR' ).AsInteger;
                         iTipo := FieldByName( 'PE_TIPO' ).AsInteger;
                         iNumero := FieldByName( 'PE_NUMERO' ).AsInteger;
                         try
                            BorraUnPeriodo( FDataset, iYear, iTipo, iNumero, Date );
                            //Log.Evento( clbNinguno, 0, NullDateTime, 'Periodo ' + GetPeriodoInfo( iYear, iNumero, eTipoPeriodo( iTipo ) ) + ' Fu� Borrado' );
                         except
                               on Error: Exception do
                               begin
                                    Log.Excepcion( 0, 'Error Al Borrar Periodo ' + GetPeriodoInfo( iYear, iNumero, eTipoPeriodo( iTipo ) ), Error );
                               end;
                         end;
                         Next;
                    end;
               end;
          end;
          //cv
          FreeAndNil(FDataset);
          Result := CloseProcess;
     end;
end;

function TdmServerNominaTimbrado.BorrarPeriodos(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'BorrarPeriodos');
     Result := BorrarPeriodosLista( Empresa, Parametros, BorrarPeriodosGetLista( Empresa, Parametros ) );
     EndLog;SetComplete;
end;

procedure TdmServerNominaTimbrado.AfectarNominaParametros;
begin
     with oZetaProvider.DatosPeriodo do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Periodo: ' + ZetacommonTools.ShowNomina( Year, Ord( Tipo ), Numero ) +
                              K_PIPE + 'Status: ' + ObtieneElemento( lfStatusPeriodo, Ord( Status ) );
     end;
end;

function TdmServerNominaTimbrado.AfectarNomina(Empresa,Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'AfectarNomina');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          InitArregloTPeriodo;
          GetDatosPeriodo;
          with DatosPeriodo do
          begin
               if ( Status < spCalculadaTotal ) then
                  DataBaseError( 'La N�mina No Ha Sido Calculada Completamente' )
               else if ( Status = spAfectadaTotal ) then
                      DataBaseError( 'La N�mina Ya Fu� Afectada' );
          end;
          AfectarNominaParametros;
          Result := AfectarDesafectarNomina( spCalculadaTotal, spAfectadaTotal, prNOAfectar );
     end;
     EndLog;SetComplete;
end;




{$ifdef VALIDAEMPLEADOSGLOBAL}
procedure TdmServerNominaTimbrado.RevisaAutorizacion;
{$ifndef DOS_CAPAS}
var
{$endif}
 {$ifndef DOS_CAPAS}
   oAutoServer: TAutoServer;
   {$ifdef DEBUGSENTINEL}
   oLicenseMgr : TLicenseMgr;
   {$endif}
 {$endif}
   {$ifdef CUENTA_EMPLEADOS}
   {$else}
   iEmpleados: Integer;
   {$endif}

begin
     FMsgError := VACIO;
     FMsgAdvertencia := VACIO;

     {$ifdef CUENTA_EMPLEADOS}
     {$else}
     iEmpleados := oZetaCreator.GetMaxEmpleadosActivos( oZetaProvider.DatosPeriodo.Inicio );
     {$endif}
     oAutoServer := ZetaSQLBroker.GetAutoServer;
     try
     {$ifdef DEBUGSENTINEL}
        {$ifdef DOS_CAPAS}
        ZetaSQLBroker.ReadAutoServer( oAutoServer, nil, nil );
        {$else}
        oLicenseMgr := TLicenseMgr.Create( oZetaProvider );
        try
           ZetaSQLBroker.ReadAutoServer( oAutoServer, oLicenseMgr.AutoGetData, oLicenseMgr.AutoSetData );
        finally
               FreeAndNil( oLicenseMgr );
        end;
        {$endif}
     {$else}
        ZetaSQLBroker.ReadAutoServer( oAutoServer, nil, nil );
     {$endif}
        with oAutoServer do
        begin
             if not OKModulo( okNomina, True ) then
                FMsgError :=  'M�dulo NO Autorizado';

             if StrVacio( FMsgError ) then
             begin
                  oZetaCreator.ValidaLimiteEmpleados( oAutoServer, evOpNomina,  FMsgAdvertencia, FMsgError  );
             end;

             with oZetaProvider do
             begin
                  if ( Log <> nil ) then
                  begin
                       if StrLLeno ( FMsgAdvertencia ) then
                          Log.Advertencia(0,'L�mite de Empleados', FMsgAdvertencia);
                  end;

                  if StrLleno ( FMsgError ) then
                     DatabaseError(FMsgError);


             end;

        end;
     finally
            ZetaSQLBroker.FreeAutoServer( oAutoServer );
     end;
end;

{$else}

procedure TdmServerNominaTimbrado.RevisaAutorizacion;
var
 {$ifndef DOS_CAPAS}
   oAutoServer: TAutoServer;
   {$ifdef DEBUGSENTINEL}
   oLicenseMgr : TLicenseMgr;
   {$endif}
 {$endif}
   {$ifdef CUENTA_EMPLEADOS}
   sMensaje: String;
   {$else}
   iEmpleados: Integer;
   {$endif}

begin
     {$ifdef CUENTA_EMPLEADOS}
     {$else}
     iEmpleados := oZetaCreator.GetMaxEmpleadosActivos( oZetaProvider.DatosPeriodo.Inicio );
     {$endif}
     oAutoServer := ZetaSQLBroker.GetAutoServer;
     try
     {$ifdef DEBUGSENTINEL}
        {$ifdef DOS_CAPAS}
        ZetaSQLBroker.ReadAutoServer( oAutoServer, nil, nil );
        {$else}
        oLicenseMgr := TLicenseMgr.Create( oZetaProvider );
        try
           ZetaSQLBroker.ReadAutoServer( oAutoServer, oLicenseMgr.AutoGetData, oLicenseMgr.AutoSetData );
        finally
               FreeAndNil( oLicenseMgr );
        end;
        {$endif}
     {$else}
        ZetaSQLBroker.ReadAutoServer( oAutoServer, nil, nil );
     {$endif}
        with oAutoServer do
        begin
             if not OKModulo( okNomina, True ) then
                DataBaseError( 'M�dulo NO Autorizado' );
             {$ifdef CUENTA_EMPLEADOS}
             if not oZetaCreator.ValidaLimiteEmpleados( oAutoServer, sMensaje ) then
                DatabaseError( sMensaje );
             {$else}
             if ( iEmpleados > Empleados ) then
                DataBaseError( Format( 'Intentando Proceso Para %d Empleados' + CR_LF + 'Se Excedi� El L�mite De %d Empleados Autorizados', [ iEmpleados, Empleados ] ) );
             {$endif}
        end;
     finally
            ZetaSQLBroker.FreeAutoServer( oAutoServer );
     end;
end;

{$endif}

function TdmServerNominaTimbrado.AfectarDesafectarNomina( const eStatus, eStatusNuevo: eStatusPeriodo; const eProceso : Procesos ): OleVariant;
var
   FAfectaPeriodo, FCuenta: TZetaCursor;
   iFactor, iCuantos: Integer;
   lBloqueo: boolean;
   sMsg: String;
begin
     lBloqueo := False;
     InitBroker;
     if ( eProceso = prNOAfectar ) then
     begin
        iFactor := 1;
        sMsg    := 'Afectar';
     end
     else
     begin
        iFactor := -1;
        sMsg    := 'Desafectar';
     end;
     with oZetaProvider do
     begin
          //CV: CreateQuery--ok
          //Es una variable local
          FCuenta := CreateQuery( GetSQLScript( Q_AFECTA_PERIODO_CUANTOS ) );
          FCuenta := CreateQuery( Format( GetSQLScript( Q_AFECTA_PERIODO_CUANTOS ), [ DatetoStrSQLC(DatosPeriodo.Inicio) ] ) );
          AsignaDataSetParams( FCuenta );
          ParamAsInteger( FCuenta, 'StatusViejo', Ord( eStatus ) );
          with FCuenta do
          begin
               //CV-Active Revisado
               Active := True;
               iCuantos := Fields[ 0 ].AsInteger;
               Active := False;
          end;

{$ifdef VALIDAEMPLEADOSGLOBAL}
          FMsgAdvertencia := VACIO;
{$endif}

          // Si se permite exceder el L�mite en N�minas Especiales
          if ( DatosPeriodo.Numero < GetGlobalInteger( K_GLOBAL_LIMITE_NOMINAS_ORDINARIAS ) ) then
             RevisaAutorizacion;
            //MA:7/04/2003:Validacion anterior == > RevisaAutorizacion( iCuantos );
          if OpenProcess( eProceso, iCuantos, FListaParametros ) then
          begin
               { Antes de empezar }
               //CV: CreateQuery--ok
               //Es una variable local
               FAfectaPeriodo := CreateQuery( GetSQLScript( Q_AFECTA_PERIODO ) );
               AsignaDataSetParams( FAfectaPeriodo );
               ParamAsInteger( FAfectaPeriodo, 'Usuario', UsuarioActivo );
               ParamAsInteger( FAfectaPeriodo, 'StatusViejo', Ord( eStatus ) );
               ParamAsInteger( FAfectaPeriodo, 'StatusNuevo', Ord( eStatusNuevo ) );
               ParamAsInteger( FAfectaPeriodo, 'Factor', iFactor );
               EmpiezaTransaccion;
               try
{$ifdef INTERBASE}
                  with FAfectaPeriodo do
                  begin
                       //CV-Active Revisado
                       Active := True;
                       while not Eof and CanContinue( FieldByName( 'Empleado' ).AsInteger, False ) do
                       begin
                            Next;
                       end;
                       Active := False;
                  end;
{$else}
                  // ParamSalida( FAfectaPeriodo, 'Empleado' );
                  Ejecuta( FAfectaPeriodo );
{$endif}
                  TerminaTransaccion( True );
                  lBloqueo := True;
               except
                     on Error: Exception do
                     begin
                          //TerminaTransaccion( False );
                          RollBackTransaccion;
                          Log.Excepcion( 0, 'Error Al ' + sMsg, Error, DescripcionParams );
                     end;
               end;
               FreeAndNil(FCuenta);
               FreeAndNil(FAfectaPeriodo);

{$ifdef VALIDAEMPLEADOSGLOBAL}
               if StrLLeno ( FMsgAdvertencia ) then
                  Log.Advertencia(0,'L�mite de Empleados', FMsgAdvertencia);
{$endif}
               Result := CloseProcess;
          end;
          if lBloqueo then
             DQueries.ValidaLimiteBloqueo( DatosPeriodo, oZetaProvider, eStatusNuevo );
     end;
end;

procedure TdmServerNominaTimbrado.DesafectarNominaParametros;
begin
     with oZetaprovider.DatosPeriodo do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Periodo: ' + ZetacommonTools.ShowNomina( Year, Ord( Tipo ), Numero ) +
                              K_PIPE + 'Status: ' + ObtieneElemento( lfStatusPeriodo, Ord( Status ) );
     end;
end;

function TdmServerNominaTimbrado.DesafectarNomina(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'DesafectarNomina');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          GetDatosPeriodo;
          InitArregloTPeriodo;
          with DatosPeriodo do
          begin
               if not ( DatosPeriodo.Status in [ spAfectadaTotal] ) then
                  DataBaseError( 'La N�mina no ha sido Afectada' );
          end;
          DesafectarNominaParametros;
          Result := AfectarDesafectarNomina( spAfectadaTotal, spCalculadaTotal, prNODesafectar );
     end;
     EndLog;SetComplete;
end;

procedure TdmServerNominaTimbrado.DefinirPeriodosParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'A�o: ' + ParamByName( 'Year' ).AsString +
                         K_PIPE + 'Tipo: ' + ObtieneElemento( lfTipoPeriodo, ParamByName( 'Tipo' ).AsInteger ) +
                         K_PIPE + 'Fecha Inicio: ' + FechaCorta( ParamByName( 'Fecha' ).AsDateTime ) +
                         K_PIPE + 'D�as De Fondo: ' + ParamByName( 'Dias' ).AsString +
                         K_PIPE + 'Periodo Inicial: ' + ParamByName( 'PeriodoInicial' ).AsString +
                         K_PIPE + 'Periodo Final: ' + ParamByName( 'PeriodoFinal' ).AsString;
     end;
end;

function TdmServerNominaTimbrado.DefinirPeriodos(Empresa, Parametros: OleVariant): OleVariant;
var
   FDatasetPeriodo: TZetaCursorLocate;
   FInsertPeriodo: TZetaCursor;
   iPeriodoInicial, iPeriodoFinal, iPeriodo, iDiasFondo, iYear, iDecenas, iDiaFecha, iMes, iPeriodoFiniquitos : Integer;
   eTipo: eTipoPeriodo;
   eClasifi: eClasifiPeriodo; //@(am):Tipos de periodo
   lInicioMes, lFinMes, lFinYear: Boolean;
   dInicio, dInicial, dFinal,dIniSimulacion: TDate;
   iDiaDecena, iMesDecena, iYearDecena: Word;

   //Registro de un periodo especial de Simulaci�n de Finiquitos
   procedure RegistrarPeriodo(const iPeriodo,iUso,iDiasFondo,iMes:Integer;
                              const dInicial,dFinal:TDate;
                              const lAhorro,lPrestamo,lSoloExcepciones,lIncBajas,lCalendario:Boolean
                              );
   begin
        with oZetaProvider do
        begin
             EmpiezaTransaccion;
             try
                { Agrega registro }
                ParamAsInteger( FInsertPeriodo, 'PE_YEAR', iYear );
                ParamAsInteger( FInsertPeriodo, 'PE_TIPO', Ord( eTipo ) );
                ParamAsInteger( FInsertPeriodo, 'PE_NUMERO', iPeriodo );
                ParamAsInteger( FInsertPeriodo, 'PE_USO', iUso  );
                ParamAsInteger( FInsertPeriodo, 'PE_STATUS',Ord( spNueva )  );
                ParamAsDate( FInsertPeriodo, 'PE_FEC_INI', dInicial );
                ParamAsDate( FInsertPeriodo, 'PE_FEC_FIN', dFinal );
                ParamAsDate( FInsertPeriodo, 'PE_FEC_PAG', dFinal + iDiasFondo );
                {$ifdef QUINCENALES}
                ParamAsDate( FInsertPeriodo, 'PE_ASI_INI', dInicial );
                ParamAsDate( FInsertPeriodo, 'PE_ASI_FIN', dFinal );
                {$endif}
                ParamAsFloat( FInsertPeriodo, 'PE_DIAS', dFinal - dInicial + 1 );
                ParamAsInteger( FInsertPeriodo, 'PE_MES', iMes );
                ParamAsInteger( FInsertPeriodo, 'US_CODIGO', UsuarioActivo );
                ParamAsBoolean( FInsertPeriodo, 'PE_AHORRO', lAhorro );
                ParamAsBoolean( FInsertPeriodo, 'PE_PRESTAM', lPrestamo );
                ParamAsBoolean( FInsertPeriodo, 'PE_SOLO_EX', lSoloExcepciones );
                ParamAsBoolean( FInsertPeriodo, 'PE_INC_BAJ', lIncBajas );
                ParamAsBoolean( FInsertPeriodo, 'PE_CAL', lCalendario );

                Ejecuta( FInsertPeriodo );
                TerminaTransaccion( True );
             except
                   on Error: Exception do
                   begin
                        TerminaTransaccion( False );
                        Log.Excepcion( 0, 'Error Al Definir Per�odo ', Error );
                   end;
             end;
        end;
   end;

   //Registro de un periodo especial de Simulaci�n de Finiquitos
   procedure RegistrarPeriodoFiniquitos;
   const
        K_DIAS_FONDO = 0;
        K_MES = 13;
   begin
        RegistrarPeriodo( oZetaProvider.GetGlobalInteger(K_GLOBAL_SIMULACION_FINIQUITOS),
                          Ord( upEspecial ),
                          K_DIAS_FONDO,
                          K_MES,
                          dIniSimulacion,dIniSimulacion,
                          False,False,True,True,False);
   end;


begin
     InitLog(Empresa,'DefinirPeriodos');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          InitGlobales;
          InitArregloTPeriodo;
          with ParamList do
          begin
               iYear := ParamByName( 'Year' ).AsInteger;
               eTipo := eTipoPeriodo( ParamByName( 'Tipo' ).AsInteger );
               iPeriodoInicial := ParamByName( 'PeriodoInicial' ).AsInteger;
               iPeriodoFinal := ParamByName( 'PeriodoFinal' ).AsInteger;
               dInicio := ParamByName( 'Fecha' ).AsDate;
               iDiasFondo := ParamByName( 'Dias' ).AsInteger;
          end;
          DefinirPeriodosParametros;
          if OpenProcess( prNODefinirPeriodos, 0, FListaParametros ) then
          begin
               try
                  //CV: CreateQuery--ok
                  //Son variables locales
                  FDatasetPeriodo := CreateQueryLocate;
                  AbreQueryScript( FDataSetPeriodo, Format( GetSQLScript( Q_DATOS_PERIODO ), [ iYear, Ord( eTipo ) ] ) );
                  FInsertPeriodo := CreateQuery( GetSQLScript( Q_DEFINIR_PERIODOS_INSERT ) );
                  iPeriodo	:= 1;
                  lFinYear	:= ( eClasifi = tpDiario );	// No hay algoritmo para tipo diario
                  iMes	:= 0;
                  iDecenas	:= 1;
                  dInicial	:= dInicio;
                  dFinal := 0;   // CV
                  iPeriodoFiniquitos := GetGlobalInteger( K_GLOBAL_SIMULACION_FINIQUITOS );//EZ
                  if ( iPeriodoFinal = 0 ) then
                     iPeriodoFinal := 999;
                  while not lFinYear and CanContinue do
                  begin
                       lInicioMes := False;
                       lFinMes	:= False;

                       case eClasifi of
                            tpSemanal:
                            begin
                                 dFinal := dInicial + 6;
                                 iDiaFecha := ZetaCommonTools.TheDay( dFinal + 4 );
                                 if ( iDiaFecha <= 14 ) or ( iPeriodo = 1 ) then
                                 begin
                                      if ( iDiaFecha >= 8 ) or ( ( iPeriodo = 1 ) and ( ZetaCommonTools.TheYear( dInicial ) < ZetaCommonTools.TheYear( dFinal ) ) ) then
                                         lInicioMes := ( iPeriodo <> 2 )
                                      else
                                          lFinMes := True;
                                 end;
                            end;
                            tpCatorcenal:
                            begin
                                 dFinal := dInicial + 13;
                                 iDiaFecha := ZetaCommonTools.TheDay( dFinal + 7 );
                                 if ( iDiaFecha <= 28 ) then
                                 begin
                                      if ( iDiaFecha >= 15 ) or ( ( iPeriodo = 1 ) and ( ZetaCommonTools.TheYear( dInicial ) < ZetaCommonTools.TheYear( dFinal ) ) ) then
                                         lInicioMes := ( iPeriodo <> 2 )
                                      else
                                          lFinMes := True;
                                 end;
                            end;
                            tpQuincenal:
                            begin
                                 iDiaFecha := ZetaCommonTools.TheDay( dInicial );
                                 if ( iDiaFecha = 1 ) then
                                 begin
                                      dFinal := dInicial + 14;
                                      lInicioMes := True;
                                 end
                                 else
                                 begin
                                      dFinal := ZetaCommonTools.LastDayOfMonth ( dInicial ); // <UltimoDiaDelMes> de la fecha dInicial;
                                      lFinMes := True;
                                 end;
                            end;
                            tpMensual:
                            begin
                                 dFinal := ZetaCommonTools.LastDayOfMonth ( dInicial ); //<UltimoDiaDelMes> de la fecha dInicial;
                                 lInicioMes := True;
                                 lFinMes := True;
                            end;
                            tpDecenal:
                            begin
                                 if ( iDecenas = 1 ) then
                                 begin
                                      dFinal := dInicial + 9;
                                      lInicioMes := True;   // Aqui hubo un error
                                      DecodeDate( dInicial, iYearDecena, iMesDecena, iDiaDecena );
                                 end
                                 else
                                     if ( iDecenas = 2 ) then
                                        dFinal := dInicial + 9
                                     else
                                     begin
                                          if ( iMesDecena = 12 ) then
                                          begin
                                               iMesDecena := 1;
                                               iYearDecena := iYearDecena + 1;
                                          end
                                          else
                                              iMesDecena := iMesDecena + 1;
                                          dFinal := EncodeDate( iYearDecena, iMesDecena, iDiaDecena ) - 1;
                                          lFinMes := True;
                                          iDecenas := 0;
                                     end;
                                 iDecenas := iDecenas + 1;
                            end;
                       end;
                       { Revisa inicio y fin de mes }
                       if lInicioMes then
                          iMes := ZetaCommonTools.TheMonth( dFinal );
                       if ( lFinMes ) then
                       begin
                            iMes := ZetaCommonTools.TheMonth( dInicial );
                            if ( iMes = 12 ) then		// Es el final del �ltimo mes
                               lFinYear := True;
                       end;
                       if ( iPeriodo >= iPeriodoInicial ) and ( iPeriodo <= iPeriodoFinal ) and
                          not FDataSetPeriodo.Locate( 'PE_NUMERO', iPeriodo, [] ) then
                       begin
                            RegistrarPeriodo( iPeriodo,
                                              Ord( upOrdinaria ),
                                              iDiasFondo,
                                              iMes,
                                              dInicial,dFinal,
                                              True,True,False,False,True);
                            dIniSimulacion := dInicial;
                       end;
                       { Siguiente per�odo }
                       iPeriodo := iPeriodo + 1;
                       dInicial := dFinal + 1;
                  end;
                  if ( iPeriodoFiniquitos >= GetGlobalInteger( K_GLOBAL_LIMITE_NOMINAS_ORDINARIAS ) ) and
                     ( not FDataSetPeriodo.Locate( 'PE_NUMERO', iPeriodoFiniquitos, [] ) ) then //se agrega un periodo especial de finiquitos al final
                  begin
                       RegistrarPeriodoFiniquitos;
                  end;
                  RecalculoDiasDataset( iYear, eTipo );
               except
                     on Error: Exception do
                     begin
                          Log.Excepcion( 0, 'Error Al Definir Per�odos', Error );
                     end;
               end;
               Result := CloseProcess;
               FreeAndNil( FDataSetPeriodo );
               FreeAndNil( FInsertPeriodo );
          end;
     end;
     EndLog;SetComplete;
end;


procedure TdmServerNominaTimbrado.LimpiarAcumuladoBuildDataset;
begin
     with SQLBroker do
     begin
          Init( enEmpleado );
          with Agente do
          begin
               AgregaColumna( 'CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
               AgregaOrden( 'CB_CODIGO', True, Entidad );
          end;
          AgregaRangoCondicion( oZetaProvider.ParamList );
          FiltroConfidencial( oZetaProvider.EmpresaActiva );
          BuildDataset( oZetaProvider.EmpresaActiva );
     end;
end;

function TdmServerNominaTimbrado.LimpiarAcumuladoDataset( Dataset: TDataset): OleVariant;
var
   i, iYear, iMesInicial, iMesFinal: Integer;
   iEmpleado: TNumEmp;
   sConceptos, sExpresion: String;
   FAcumulaBorra, FAcumulaUpdate: TZetaCursor;
begin
     with oZetaProvider.ParamList do
     begin
          iYear := ParamByName( 'Year' ).AsInteger;
          iMesInicial := ParamByName( 'MesInicial' ).AsInteger;
          iMesFinal := ParamByName( 'MesFinal' ).AsInteger;
          sConceptos := ParamByName( 'Conceptos' ).AsString;
     end;
     if ZetaCommonTools.StrLleno( sConceptos ) then
        sConceptos := ' and ' + sConceptos;
     sExpresion := '';
     for i := iMesInicial to iMesFinal do
         sExpresion := sExpresion + ' ' + GetMesAcumulado( i ) + ' = 0,';
     sExpresion := Trim( ZetaCommonTools.CortaUltimo( sExpresion ) ); // Elimina Ultima Coma //
     with oZetaProvider do
     begin
          //InitGlobales????
          if OpenProcess( prNOLimpiarAcum, Dataset.RecordCount, FListaParametros ) then
          begin
               { Antes de empezar }
               //CV: CreateQuery--ok
               FAcumulaBorra := CreateQuery( Format( GetSQLScript( Q_LIMPIA_ACUMULADOS_BORRA ), [ sExpresion, sConceptos ] ) );
               FAcumulaUpdate := CreateQuery( GetSQLScript( Q_LIMPIA_ACUMULADOS_UPDATE ) );
               ParamAsInteger( FAcumulaBorra, 'Year', iYear );
               ParamAsInteger( FAcumulaUpdate, 'Year', iYear );
               { Ejecuci�n }
               with Dataset do
               begin
                    while not Eof and CanContinue( Fields[ 0 ].AsInteger ) do
                    begin
                         EmpiezaTransaccion;
                         iEmpleado := Fields[ 0 ].AsInteger;
                         try
                            ParamAsInteger( FAcumulaBorra, 'Empleado', iEmpleado );
                            Ejecuta( FAcumulaBorra );
                         except
                               on Error:Exception do
                               begin
                                    Log.Excepcion( iEmpleado,  'Error Al Borrar Acumulados', Error );
                               end;
                         end;
                         try
                            ParamAsInteger( FAcumulaUpdate,'Empleado', iEmpleado );
                            Ejecuta( FAcumulaUpdate );
                         except
                               on Error: Exception do
                               begin
                                    Log.Excepcion( iEmpleado, 'Error Al Recalcular Acumulado Anual', Error );
                               end;
                         end;
                         TerminaTransaccion( True );
                         Next;
                    end;
               end;
               Result := CloseProcess;
               FreeAndNil(FAcumulaBorra);
               FreeAndNil(FAcumulaUpdate);
          end;
     end;
end;

procedure TdmServerNominaTimbrado.LimpiarAcumuladoParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'A�o: ' + ParamByName( 'Year' ).AsString +
                         K_PIPE + 'Inicio: ' + ZetaCommonLists.ObtieneElemento( lfMes13, ( ParamByName( 'MesInicial' ).AsInteger - 1 ) ) +
                         K_PIPE + 'Fin: ' + ZetaCommonLists.ObtieneElemento( lfMes13, ( ParamByName( 'MesFinal' ).AsInteger - 1 ) );
          if strLleno( ParamByName( 'Conceptos' ).AsString ) then
             FListaParametros := FListaParametros + K_PIPE + 'Conceptos: ' + ParamByName( 'Conceptos' ).AsString
          else
             FListaParametros := FListaParametros + K_PIPE + 'Conceptos: Todos';
     end;

end;

function TdmServerNominaTimbrado.LimpiarAcumulado(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'LimpiarAcumulado');
     try
        InitBroker;
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList(Parametros);
        end;
        LimpiarAcumuladoParametros;
        LimpiarAcumuladoBuildDataset;
        Result := LimpiarAcumuladoDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
            ClearBroker;
     end;
     EndLog;SetComplete;
end;

procedure TdmServerNominaTimbrado.RecalculoAcumuladoParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Recalcular Acumulados: ' + ZetaCommonTools.zBoolToStr( ParamByName( 'Acumulados' ).AsBoolean );
          if ParamByName( 'Acumulados' ).AsBoolean then
             FListaParametros := FListaParametros + K_PIPE + 'A�o: ' + ParamByName( 'Year' ).AsString +
                                 K_PIPE + 'Mes: ' + ZetaCommonLists.ObtieneElemento( lfMes13, ( ParamByName( 'Mes' ).AsInteger - 1 ) );
          FListaParametros := FListaParametros + K_PIPE + 'Recalcular Ahorros: ' + ZetaCommonTools.zBoolToStr( ParamByName( 'Ahorros' ).AsBoolean ) +
                              K_PIPE + 'Recalcular Prestamos: ' + ZetaCommonTools.zBoolToStr( ParamByName( 'Prestamos' ).AsBoolean );
     end;
end;

function TdmServerNominaTimbrado.RecalculoAcumulado(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'RecalculoAcumulado');
     try
        InitBroker;
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList(Parametros);
        end;
        RecalculoAcumuladoParametros;
        RecalculoAcumuladoBuildDataset;
        Result := RecalculoAcumuladoDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
            ClearBroker;
     end;
     EndLog;SetComplete;
end;

procedure TdmServerNominaTimbrado.RecalculoAcumuladoBuildDataset;
begin
     with SQLBroker do
     begin
          //'select @CAMPOS from COLABORA C where @CONDICION '+
          //'order by C.CB_CODIGO';
          Init( enEmpleado );
          with Agente do
          begin
               AgregaColumna( 'CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
               AgregaOrden( 'CB_CODIGO', True, Entidad );
          end;
          AgregaRangoCondicion( oZetaProvider.ParamList );
          FiltroConfidencial( oZetaProvider.EmpresaActiva );
          BuildDataset( oZetaProvider.EmpresaActiva );
     end;
end;

function TdmServerNominaTimbrado.RecalculoAcumuladoDataset( DataSet: TDataSet ): OleVariant;
const
     Q_SP_RECALCULO = 'EXECUTE PROCEDURE RECALCULA_ACUMULADOS ( '+
                      ':ANIO, :EMPLEADO,:MES, :STATUS )';
     Q_SP_AHORRO = 'EXECUTE PROCEDURE RECALCULA_AHORROS ( '+
                   ':EMPLEADO, :STATUS )';
     Q_SP_PRESTAMO = 'EXECUTE PROCEDURE RECALCULA_PRESTAMOS ( '+
                     ':EMPLEADO, :STATUS )' ;
var
   FRecalcula, FAhorros, FPrestamos: TZetaCursor;
   iYear, iMes, iEmpleado: Integer;
   lRecalcula, lAhorros, lPrestamos: Boolean;
begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               iYear := ParamByName( 'Year' ).AsInteger;
               iMes := ParamByName( 'Mes' ).AsInteger;
               lRecalcula := ParamByName( 'Acumulados' ).AsBoolean;
               lAhorros := ParamByName( 'Ahorros' ).AsBoolean;
               lPrestamos := ParamByName( 'Prestamos' ).AsBoolean;
          end;
          if lRecalcula then
          begin
               //CV: CreateQuery--ok
               //Variable local
               FRecalcula := CreateQuery( Q_SP_RECALCULO );
               ParamAsInteger( FRecalcula, 'Anio', iYear );
               ParamAsInteger( FRecalcula, 'Mes', iMes );
               ParamAsInteger( FRecalcula, 'Status', Ord( spAfectadaTotal ) );
          end
          else
              FRecalcula := nil;  { Evita WARNING del Compilador }
          if lAhorros then
          begin
               FAhorros := CreateQuery( Q_SP_AHORRO );
               ParamAsInteger( FAhorros, 'Status', Ord( spAfectadaTotal ) );
          end
          else
              FAhorros := nil;   { Evita WARNING del Compilador }
          if lPrestamos then
          begin
               FPrestamos := CreateQuery( Q_SP_PRESTAMO );
               ParamAsInteger( FPrestamos, 'Status', Ord( spAfectadaTotal ) );
          end
          else
              FPrestamos := nil;    { Evita WARNING del Compilador }
          if OpenProcess( prNORecalcAcum, Dataset.RecordCount, FListaParametros ) then
          begin
               { Ejecuci�n }
               with Dataset do
               begin
                    while not Eof and CanContinue( Fields[ 0 ].AsInteger ) do
                    begin
                         EmpiezaTransaccion;
                         iEmpleado := Fields[ 0 ].AsInteger;
                         try
                            { Recalcular Acumulados }
                            if lRecalcula then
                            begin
                                 ParamAsInteger( FRecalcula, 'Empleado', iEmpleado );
                                 Ejecuta( FRecalcula );
                            end;
                         except
                               on Error: Exception do
                               begin
                                    Log.Excepcion( iEmpleado, 'Error Al Recalcular Acumulado ', Error, DescripcionParams );
                               end;
                         end;
                         try
                            { Recalcular Ahorros }
                            if lAhorros then
                            begin
                                 ParamAsInteger( FAhorros, 'Empleado', iEmpleado );
                                 Ejecuta( FAhorros );
                            end;
                         except
                               on Error: Exception do
                               begin
                                    Log.Excepcion( iEmpleado, 'Error Al Recalcular Ahorros ', Error, DescripcionParams );
                               end;
                         end;
                         try
                            { Recalcular Ahorros }
                            if lPrestamos then
                            begin
                                 ParamAsInteger( FPrestamos, 'Empleado', iEmpleado );
                                 Ejecuta( FPrestamos );
                            end;
                         except
                               on Error: Exception do
                               begin
                                    Log.Excepcion( iEmpleado, 'Error Al Recalcular Pr�stamos ', Error, DescripcionParams );
                               end;
                         end;
                         TerminaTransaccion( True );
                         Next;
                    end;
               end;
               Result := CloseProcess;
               FreeAndNil(FRecalcula);
               FreeAndNil(FAhorros);
               FreeAndNil(FPrestamos);
          end;
     end;
end;

procedure TdmServerNominaTimbrado.RecalculoDiasDataset( const iYear: Integer; const eTipo: eTipoPeriodo );
var
   FDatasetPeriodo: TZetaCursor;
   FInsertPeriodo: TZetaCursor;
   aDiasMes, aPeriodosMes: array [ 1..12 ] of Integer;
   iMes, iTotPeriodos, iDiasAC, iPosMes: Integer;
begin
     with oZetaProvider do
     begin
          // Inicializar en ceros
          iTotPeriodos := 0;
          for iMes := 1 to 12 do
          begin
               aDiasMes[ iMes ] := 0;
               aPeriodosMes[ iMes ] := 0;
          end;
          // Nota: Indispensable el SELECT * from PERIODO
          //CV: CreateQuery--ok
          //Variable local
          FDatasetPeriodo := CreateQuery;
          AbreQueryScript( FDataSetPeriodo, Format( GetSQLScript( Q_RECALCULO_DIAS_PERIODO ), [ iYear,
                                                                                              Ord( eTipo ),
                                                                                              GetGlobalInteger( K_GLOBAL_LIMITE_NOMINAS_ORDINARIAS ) ] ) );
          // Contar totales mensuales y anual
          with FDatasetPeriodo do
          begin
               while not Eof do
               begin
                    iMes := FieldByName( 'PE_MES' ).AsInteger;
                    aDiasMes[ iMes ] := aDiasMes[ iMes ] + FieldByName( 'PE_DIAS' ).AsInteger;
                    Inc( aPeriodosMes[ iMes ] );
                    Inc( iTotPeriodos );
                    Next;
               end;
          end;
          // Calcular y asignar acumulados y posiciones
          //CV: CreateQuery--ok
          //Variable local
          FInsertPeriodo := CreateQuery( GetSQLScript( Q_RECALCULO_DIAS_UPDATE ) );
          iMes := 0;
          iDiasAC := 0;
          iPosMes := 0;
          EmpiezaTransaccion;
          try
             with FDatasetPeriodo do
             begin
                  {
                  First;
                  }
                  { GA: Para que se vuelva a posicionar al principo }
                  { dado que TDataset.First no se puede usar con ODBCExpress }
                  Active := False;
                  Active := True;
                  while not Eof do
                  begin
                       if ( FieldByName( 'PE_MES' ).AsInteger <> iMes ) then
                       begin
                            iMes := FieldByName( 'PE_MES' ).AsInteger;
                            iDiasAC := 0;
                            iPosMes := 0;
                       end;
                       iDiasAC := iDiasAC + FieldByName( 'PE_DIAS' ).AsInteger;
                       iPosMes := iPosMes + 1;
                       ParamAsInteger( FInsertPeriodo, 'PE_DIAS_AC', iDiasAC );
                       ParamAsInteger( FInsertPeriodo, 'PE_DIA_MES', aDiasMes[ iMes ] );
                       ParamAsInteger( FInsertPeriodo, 'PE_POS_MES', iPosMes );
                       ParamAsInteger( FInsertPeriodo, 'PE_PER_MES', aPeriodosMes[ iMes ] );
                       ParamAsInteger( FInsertPeriodo, 'PE_PER_TOT', iTotPeriodos );
                       ParamAsInteger( FInsertPeriodo, 'Year', iYear );
                       ParamAsInteger( FInsertPeriodo, 'Tipo', Ord( eTipo ) );
                       ParamAsInteger( FInsertPeriodo, 'Numero', FieldByName( 'PE_NUMERO' ).AsInteger );
                       Ejecuta( FInsertPeriodo );
                       Next;
                  end;
                  Active := False;
             end;
             TerminaTransaccion( True );
          except
                on Error: Exception do
                begin
                     TerminaTransaccion( False );
                end;
          end;
          FreeAndNil( FDataSetPeriodo );
          FreeAndNil( FInsertPeriodo );
     end;
end;

function TdmServerNominaTimbrado.RecalculoDias(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'RecalculoDias');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
          AsignaParamList( Parametros );
          with ParamList do
          begin
               RecalculoDiasDataset( ParamByName( 'Year' ).AsInteger, eTipoPeriodo( ParamByName( 'Tipo' ).AsInteger ) );
          end;
     end;
     EndLog;SetComplete;
end;

{ **** Copiar Nominas *** }

procedure TdmServerNominaTimbrado.CopiarNominaParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Tipo: ' + ZetaCommonLists.ObtieneElemento( lfTipoNomina, ParamByName( 'Tipo' ).AsInteger ) +
                         K_PIPE + 'N�mina Original' +
                         K_PIPE + 'A�o: ' + ParamByName( 'YearOriginal' ).AsString +
                         K_PIPE + 'N�mero: ' + ParamByName( 'NumeroOriginal' ).AsString +
                         K_PIPE + 'N�mina Nueva' +
                         K_PIPE + 'A�o: ' + ParamByName( 'YearNuevo' ).AsString +
                         K_PIPE + 'N�mero: ' + ParamByName( 'NumeroNuevo' ).AsString;
     end;
end;

function TdmServerNominaTimbrado.CopiarNomina(Empresa, Parametros: OleVariant): OleVariant;
var
   FPeriodoOriginal: TDatosPeriodo;
   lCancelarNomina: Boolean;
   sAdvertencia : String;
begin
     InitLog(Empresa,'CopiarNomina');
     try
        InitBroker;
        sAdvertencia := VACIO;
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList(Parametros);
             InitArregloTPeriodo;
             lCancelarNomina:= ParamList.ParamByName( 'CancelarNomina' ).AsBoolean;
             ErrorStatusNomina( lCancelarNomina, sAdvertencia );
        end;
        CopiarNominaParametros;
        CopiarNominasBuildDataset( FPeriodoOriginal );
        Result := CopiarNominasDataset( SQLBroker.SuperReporte.DataSetReporte, FPeriodoOriginal, lCancelarNomina, sAdvertencia );  // sAdvertencia Lleva la Descripcion de Advertencia para Bit�cora
     finally
            ClearBroker;
     end;
     EndLog;SetComplete;
end;

procedure TdmServerNominaTimbrado.ErrorStatusNomina( const lCancelarNomina: Boolean; var sAdvertencia : String );
const
     K_FECHAS_DIFERENTES = {'Las Fechas De Inicio y Fin De Las N�minas Son Diferentes' +
                           CR_LF + }
                           'N�mina Original del %s al %s' +
                           CR_LF +
                           'N�mina Nueva del %s al %s';
var
   FConsultaPeriodo: TZetaCursor;
   dOrigenInicio, dOrigenFinal, dDestinoInicio, dDestinoFinal : TDate;
begin
     dOrigenInicio := 0;
     dOrigenFinal  := 0;
     dDestinoInicio:= 0;
     dDestinoFinal := 0;

     with oZetaProvider, oZetaProvider.ParamList do
     begin
          //CV: CreateQuery--ok
          //Variable local
          FConsultaPeriodo := CreateQuery( GetSQLScript( Q_ENCABEZADO_PERIODO ) );
          try
             // Periodo Original
             ParamAsInteger( FConsultaPeriodo, 'Year', ParamByName( 'YearOriginal' ).AsInteger );
             ParamAsInteger( FConsultaPeriodo, 'Tipo', Ord( ParamByName( 'Tipo' ).AsInteger ) );
             ParamAsInteger( FConsultaPeriodo, 'Numero', ParamByName( 'NumeroOriginal' ).AsInteger );
             with FConsultaPeriodo do
             begin
                  Active := TRUE;
                  if EOF then
                     DataBaseError( 'No Est� Definida la N�mina Original' )
                  else if ( not lCancelarNomina ) and ( eStatusPeriodo( FieldByName( 'PE_STATUS' ).AsInteger ) < spCalculadaTotal ) then
                     DataBaseError( 'La N�mina Original No ha sido Calculada Completamente' )
                  else if lCancelarNomina and ( eStatusPeriodo( FieldByName( 'PE_STATUS' ).AsInteger ) < spAfectadaTotal ) then
                     DataBaseError( 'La N�mina Original No ha sido Afectada Completamente' )
                  else
                  begin
                       dOrigenInicio := FieldByName( 'PE_FEC_INI' ).AsDateTime;
                       dOrigenFinal  := FieldByName( 'PE_FEC_FIN' ).AsDateTime;
                  end;
                  Active := FALSE;
             end;
             // Periodo Nuevo
             ParamAsInteger( FConsultaPeriodo, 'Year', ParamByName( 'YearNuevo' ).AsInteger );
             ParamAsInteger( FConsultaPeriodo, 'Tipo', Ord( ParamByName( 'Tipo' ).AsInteger ) );
             ParamAsInteger( FConsultaPeriodo, 'Numero', ParamByName( 'NumeroNuevo' ).AsInteger );
             with FConsultaPeriodo do
             begin
                  Active := TRUE;
                  if EOF then
                     DataBaseError( 'No Est� Definida la N�mina Nueva' )
                  else if ( FieldByName( 'PE_STATUS' ).AsInteger > Ord( spCalculadaTotal ) ) then
                     DataBaseError( 'La N�mina Nueva No Debe Estar Afectada' )
                  else
                  begin
                       dDestinoInicio := FieldByName( 'PE_FEC_INI' ).AsDateTime;
                       dDestinoFinal  := FieldByName( 'PE_FEC_FIN' ).AsDateTime;
                  end;
                  Active := FALSE;
             end;
             // Fechas de Periodos
             if ( not lCancelarNomina ) and ( ( dOrigenInicio <> dDestinoInicio ) or ( dOrigenFinal <> dDestinoFinal ) ) then
                sAdvertencia := Format( K_FECHAS_DIFERENTES, [ FechaCorta( dOrigenInicio ), FechaCorta( dOrigenFinal ), FechaCorta( dDestinoInicio ), FechaCorta( dDestinoFinal ) ] );
          finally
                 FreeAndNil(FConsultaPeriodo);
          end;
     end;
end;

procedure TdmServerNominaTimbrado.CopiarNominasBuildDataset( var FPeriodoOriginal : TDatosPeriodo );
begin
     with FPeriodoOriginal do
     begin
          with oZetaProvider.ParamList do
          begin
               Year := ParamByName( 'YearOriginal' ).AsInteger;
               Tipo := eTipoPeriodo( ParamByName( 'Tipo' ).AsInteger );
               Numero := ParamByName( 'NumeroOriginal' ).AsInteger;
          end;
     end;
     with SQLBroker do
     begin
          Init( enNomina );
          with Agente do
          begin
               AgregaColumna( 'CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
               with FPeriodoOriginal do
               begin
                    AgregaFiltro( Format( '( PE_YEAR = %d )', [ Year ] ), True, Entidad );
                    AgregaFiltro( Format( '( PE_TIPO = %d )', [ Ord( Tipo ) ] ), True, Entidad );
                    AgregaFiltro( Format( '( PE_NUMERO = %d )', [ Numero ] ), True, Entidad );
               end;
               AgregaOrden( 'CB_CODIGO', True, Entidad );
          end;
          AgregaRangoCondicion( oZetaProvider.ParamList );
          FiltroConfidencial( oZetaProvider.EmpresaActiva );
          BuildDataset( oZetaProvider.EmpresaActiva );
     end;
end;

function TdmServerNominaTimbrado.CopiarNominasDataset( DataSet: TDataSet; FPeriodoOriginal: TDatosPeriodo; const lCancelarNomina: Boolean; const sAdvertencia : String ): Olevariant;
var
   FPeriodoNuevo: TDatosPeriodo;
   FCopiaNomina, FBorraNomina: TZetaCursor;
   lOk: Boolean;
   sError : String;
begin
     with oZetaProvider do
     begin
          with FPeriodoNuevo do
          begin
               with ParamList do
               begin
                    Year := ParamByName( 'YearNuevo' ).AsInteger;
                    Tipo := eTipoPeriodo( ParamByName( 'Tipo' ).AsInteger );
                    Numero := ParamByName( 'NumeroNuevo' ).AsInteger;
               end;
          end;
          if lCancelarNomina then
          begin
               //CV: CreateQuery--ok
               //Variable local
               FCopiaNomina := CreateQuery( GetSQLScript( Q_NOMINA_CANCELA ) );
               ParamAsInteger( FCopiaNomina, 'Usuario', UsuarioActivo );
               sError := 'Error al Cancelar N�mina Pasada';
          end
          else
          begin
               //CV: CreateQuery--ok
               //Variable local
               FCopiaNomina := CreateQuery( GetSQLScript( Q_NOMINA_COPIA ) );
               sError := 'Error al Copiar N�mina';
          end;
          with FPeriodoOriginal do
          begin
               ParamAsInteger( FCopiaNomina, 'YearOriginal', Year );
               ParamAsInteger( FCopiaNomina, 'TipoOriginal', Ord( Tipo ) );
               ParamAsInteger( FCopiaNomina, 'NumeroOriginal', Numero );
          end;
          with FPeriodoNuevo do
          begin
               ParamAsInteger( FCopiaNomina, 'YearNuevo', Year );
               ParamAsInteger( FCopiaNomina, 'TipoNuevo', Ord( Tipo ) );
               ParamAsInteger( FCopiaNomina, 'NumeroNuevo', Numero );
          end;
          if lCancelarNomina then
             lOk := OpenProcess( prNOCancelarPasadas, Dataset.RecordCount, FListaParametros )
          else
             lOk := OpenProcess( prNOCopiarNominas, Dataset.RecordCount, FListaParametros );
          if lOk then
          begin
               { Antes de empezar }
               if not lCancelarNomina and StrLleno( sAdvertencia ) then
                  Log.Advertencia( 0, 'Fechas Diferentes De Inicio y Fin De Las N�minas', sAdvertencia );
               EmpiezaTransaccion;
               try
                  //CV: CreateQuery--ok
                  //Variable local
                  FBorraNomina := CreateQuery( GetSQLScript( Q_DIFERENCIAS_BORRA_NOMINA ) );
                  with FPeriodoNuevo do
                  begin
                       ParamAsInteger( FBorraNomina, 'Year', Year );
                       ParamAsInteger( FBorraNomina, 'Tipo', Ord( Tipo ) );
                       ParamAsInteger( FBorraNomina, 'Numero', Numero );
                  end;
                  Ejecuta( FBorraNomina );
                  TerminaTransaccion( True );
                  lOk := True;
               except
                     on Error: Exception do
                     begin
                          TerminaTransaccion( False );
                          Log.Excepcion( 0, 'Error Al Borrar Nomina ', Error, DescripcionParams );
                          lOk := False;
                     end;
               end;
               { Ejecuci�n }
               if lOk then
               begin
                    with Dataset do
                    begin
                         while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                         begin
                              EmpiezaTransaccion;
                              try
                                 ParamAsInteger( FCopiaNomina, 'Empleado', FieldByName( 'CB_CODIGO' ).AsInteger );
                                 Ejecuta( FCopiaNomina );
                                 TerminaTransaccion( True );
                              except
                                    on Error: Exception do
                                    begin
                                         //TerminaTransaccion( False );
                                         RollBackTransaccion;
                                         Log.Excepcion( FieldByName( 'CB_CODIGO' ).AsInteger, sError, Error, DescripcionParams );
                                    end;
                              end;
                              Next;
                         end;
                         EmpiezaTransaccion;
                         try
                            SetStatusPeriodo( FPeriodoNuevo );
                            TerminaTransaccion( True );
                         except
                               on Error: Exception do
                               begin
                                    TerminaTransaccion( False );
                                    Log.Excepcion( 0, 'Error Al Cambiar Status del Periodo', Error, DescripcionParams );
                               end;
                         end;
                    end;
               end;
               Result := CloseProcess;
               FreeAndNil(FCopiaNomina);
               FreeAndNil(FBorraNomina);
          end;

     end;
end;

{ **** CALCULAR DIFERENCIAS **** }

procedure TdmServerNominaTimbrado.CalcularDiferenciasParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Tipo: ' + ZetaCommonLists.ObtieneElemento( lfTipoNomina, ParamByName( 'Tipo' ).AsInteger ) +
                         K_PIPE + 'N�mina Original' +
                         K_PIPE + 'A�o: ' + ParamByName( 'YearOriginal' ).AsString +
                         K_PIPE + 'N�mero: ' + ParamByName( 'NumeroOriginal' ).AsString +
                         K_PIPE + 'N�mina Nueva' +
                         K_PIPE + 'A�o: ' + ParamByName( 'YearNuevo' ).AsString +
                         K_PIPE + 'N�mero: ' + ParamByName( 'NumeroNuevo' ).AsString +
                         K_PIPE + 'Diferencias en N�mina' +
                         K_PIPE + 'A�o: ' + ParamByName( 'YearDiferencias' ).AsString +
                         K_PIPE + 'N�mero: ' + ParamByName( 'NumeroDiferencias' ).AsString +
                         K_PIPE + 'Conceptos: ' + ParamByName( 'Conceptos' ).AsString;
     end;
end;

function TdmServerNominaTimbrado.CalcularDiferencias(Empresa, Parametros: OleVariant): OleVariant;
var
   FPeriodoNuevo: TDatosPeriodo;
begin
     InitLog(Empresa,'CalcularDiferencias');
     try
        InitBroker;
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList(Parametros);
             InitArregloTPeriodo;
        end;
        CalcularDiferenciasParametros;
        CalcularDiferenciasBuildDataset( FPeriodoNuevo );
        Result := CalcularDiferenciasDataset( SQLBroker.SuperReporte.DataSetReporte, FPeriodoNuevo );
     finally
            ClearBroker;
     end;
     EndLog;SetComplete;
end;

procedure TdmServerNominaTimbrado.CalcularDiferenciasBuildDataset( var FPeriodoNuevo : TDatosPeriodo );
begin
     with FPeriodoNuevo do
     begin
          with oZetaProvider.ParamList do
          begin
               Year := ParamByName( 'YearNuevo' ).AsInteger;
               Tipo := eTipoPeriodo( ParamByName( 'Tipo' ).AsInteger );
               Numero := ParamByName( 'NumeroNuevo' ).AsInteger;
          end;
     end;
     with SQLBroker do
     begin
          Init( enNomina );
          with Agente do
          begin
               AgregaColumna( 'CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
               //AgregaColumna( 'CB_CHECA', True, enEmpleado, tgBooleano, 0, 'CB_CHECA' );
               //AgregaColumna( 'NO_FUERA', True, Entidad, tgBooleano, 0, 'NO_FUERA' );
               with FPeriodoNuevo do
               begin
                    AgregaFiltro( Format( '( PE_YEAR = %d )', [ Year ] ), True, Entidad );
                    AgregaFiltro( Format( '( PE_TIPO = %d )', [ Ord(Tipo) ] ), True, Entidad );
                    AgregaFiltro( Format( '( PE_NUMERO = %d )', [ Numero ] ), True, Entidad );
               end;
               AgregaOrden( 'CB_CODIGO', True, Entidad );
          end;
          AgregaRangoCondicion( oZetaProvider.ParamList );
          FiltroConfidencial( oZetaProvider.EmpresaActiva );
          BuildDataset( oZetaProvider.EmpresaActiva );
     end;
end;

procedure TdmServerNominaTimbrado.SetStatusPeriodo( oDatosPeriodo: TDatosPeriodo );
const
{$ifdef INTERBASE}
     Q_STATUS = 'select MAXSTATUS from SET_STATUS_PERIODO( :ANIO, :TIPO, :NUMERO, :USUARIO )';
{$else}
     Q_STATUS = '{CALL SET_STATUS_PERIODO( :ANIO,:TIPO,:NUMERO, :USUARIO, :MAXSTATUS )}';
{$endif}
var
   FDataset: TZetaCursor;
   eStatus: eStatusPeriodo;
begin
     with oZetaProvider do
     begin
          InitArregloTPeriodo;
          //CV: CreateQuery--ok
          //Variable local
          FDataset := CreateQuery( Q_STATUS );
          try
             with ODatosPeriodo do
             begin
                  ParamAsInteger( FDataset, 'Anio', Year );
                  ParamAsInteger( FDataset, 'Tipo', Ord( Tipo ) );
                  ParamAsInteger( FDataset, 'Numero', Numero );
                  ParamAsInteger( FDataset, 'Usuario', UsuarioActivo );
             end;
{$ifdef INTERBASE}
             FDataSet.Active:=TRUE;
             eStatus := eStatusPeriodo( FDataSet.FieldByName( 'MaxStatus' ).AsInteger );
{$else}
             ParamSalida( FDataSet, 'MaxStatus' );
             Ejecuta( FDataSet );
             eStatus := eStatusPeriodo( GetParametro( FDataset, 'MaxStatus' ).AsInteger );
{$endif}
          finally
                 FreeAndNil(FDataSet);
          end;
          DQueries.ValidaLimiteBloqueo( DatosPeriodo, oZetaProvider, eStatus );
     end;
end;

function TdmServerNominaTimbrado.CalcularDiferenciasDataset( DataSet: TDataset; var FPeriodoNuevo: TDatosPeriodo ): OleVariant;
var
   sConceptos: String;
   lOk: Boolean;
   FPeriodoOriginal, FPeriodoDiferencias: TDatosPeriodo;
   iEmpleado: TNumEmp;
   iNominas, iMovimientos: Integer;
   FAgregaNomina, FBorraNomina, FRetroactivo, FStatusNomina: TZetaCursor;
   FComparaNominas: TZetaCursor;

procedure ProcesaDiferencias( const Original, Nueva: TDatosPeriodo; const lEsInicial: Boolean );
var
   rPercepcion, rDeduccion: TPesos;
   iConcepto: Integer;
   sReferencia: String;
begin
     with oZetaProvider do
     begin
          FComparaNominas.Active := FALSE;
          with Original do
          begin
               ParamAsInteger( FComparaNominas, 'YearOriginal', Year );
               ParamAsInteger( FComparaNominas, 'TipoOriginal', Ord( Tipo ) );
               ParamAsInteger( FComparaNominas, 'NumeroOriginal', Numero );
          end;
          with Nueva do
          begin
               ParamAsInteger( FComparaNominas, 'YearNuevo', Year );
               ParamAsInteger( FComparaNominas, 'TipoNuevo', Ord( Tipo ) );
               ParamAsInteger( FComparaNominas, 'NumeroNuevo' , Numero );
          end;
          ParamAsInteger( FComparaNominas, 'Empleado', iEmpleado );
          if lEsInicial then
             ParamAsInteger( FComparaNominas, 'Valor' , 0 )
          else
              ParamAsInteger( FComparaNominas, 'Valor', 1 );
          with FComparaNominas do
          begin
               Active := True;
               if not EOF then
               begin
                    iNominas := iNominas + 1;
                    //First;
                    while not Eof do        { Se agregan los registros a MOVIMIEN }
                    begin
                         iConcepto := Fields[ 0 ].AsInteger;
                         sReferencia := Fields[ 1 ].AsString;
                         if lEsInicial then { Diferencia: Nueva-Original }
                         begin
                              rPercepcion := Fields[ 4 ].AsFloat - Fields[ 2 ].AsFloat;
                              rDeduccion := Fields[ 5 ].AsFloat - Fields[ 3 ].AsFloat;
                         end
                         else
                         begin	{ Movimientos que s�lo aparecen en la Nueva }
                              rPercepcion := Fields[ 2 ].AsFloat;
                              rDeduccion := Fields[ 3 ].AsFloat;
                         end;
                         ParamAsInteger( FRetroactivo, 'Empleado', iEmpleado );
                         ParamAsInteger( FRetroactivo, 'Concepto', iConcepto );
                         ParamAsVarChar( FRetroactivo, 'Referencia', sReferencia, K_ANCHO_REFERENCIA );
                         ParamAsFloat( FRetroactivo, 'Percepcion', rPercepcion );
                         ParamAsFloat( FRetroactivo, 'Deduccion', rDeduccion );
                         Ejecuta( FRetroactivo );
                         iMovimientos := iMovimientos + 1;
                         Next;
                    end;
               end;
               Active := False;
          end;
     end;
end;

begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               with FPeriodoOriginal do
               begin
                    Year := ParamByName( 'YearOriginal' ).AsInteger;
                    Tipo := eTipoPeriodo( ParamByName( 'Tipo' ).AsInteger );
                    Numero := ParamByName( 'NumeroOriginal' ).AsInteger;
               end;
               with FPeriodoDiferencias do
               begin
                    Year := ParamByName( 'YearDiferencias' ).AsInteger;
                    Tipo := eTipoPeriodo( ParamByName( 'Tipo' ).AsInteger );
                    Numero := ParamByName( 'NumeroDiferencias' ).AsInteger;
               end;
               sConceptos := ParamByName( 'Conceptos' ).AsString;
               if ZetaCommonTools.StrLleno( sConceptos ) then
                  sConceptos := Format( '%s and ', [ sConceptos ] );
          end;
          {CV-CAMBIOS PARA ODBC}
          //CV: CreateQuery--ok
          //Variable local
          FComparaNominas := CreateQuery;
          PreparaQuery( FComparaNominas, Format( GetSQLScript( Q_DIFERENCIAS_COMPARA_NOMINAS ), [ sConceptos ] ) );
     end;
     iEmpleado := 0;
     iNominas := 0;
     iMovimientos := 0;
     with oZetaProvider do
     begin
          if OpenProcess( prNOCalcularDiferencias, Dataset.RecordCount, FListaParametros ) then
          begin
               { Antes de empezar }
               EmpiezaTransaccion;
               try
                  {1}
                  //CV: CreateQuery--ok
                  //Variable local
                  FBorraNomina := CreateQuery( GetSQLScript( Q_DIFERENCIAS_BORRA_NOMINA ) );
                  try
                     with FPeriodoDiferencias do
                     begin
                          ParamAsInteger( FBorraNomina, 'Year', Year );
                          ParamAsInteger( FBorraNomina, 'Tipo', Ord( Tipo ) );
                          ParamAsInteger( FBorraNomina, 'Numero', Numero );
                     end;
                     Ejecuta( FBorraNomina );
                  finally
                         FreeAndNil(FBorraNomina);
                  end;
                  TerminaTransaccion( True );
                  lOk := True;
               except
                     on Error: Exception do
                     begin
                          TerminaTransaccion( False );
                          Log.Excepcion( 0, 'Error Al Borrar Nomina', Error, DescripcionParams );
                          lOk := False;
                     end;
               end;
               if lOk then
               begin
                    { Para Actualizar El Status De Las N�minas Creadas }
                    //CV: CreateQuery--ok
                    //Variable local
                    FStatusNomina := CreateQuery( GetSQlScript( Q_DIFERENCIAS_SET_STATUS ) );
                    with FPeriodoDiferencias do
                    begin
                         ParamAsInteger( FStatusNomina, 'Year', Year );
                         ParamAsInteger( FStatusNomina, 'Tipo', Ord( Tipo ) );
                         ParamAsInteger( FStatusNomina, 'Numero', Numero );
                         ParamAsInteger( FStatusNomina, 'Status', Ord( spCalculadaParcial ) );
                    end;

                    //CV: CreateQuery--ok
                    //Variable local
                    FRetroactivo := CreateQuery( GetSQLScript( Q_DIFERENCIAS_INSERT ) );
                    ParamAsBoolean( FRetroactivo, 'Activo' , True );
                    ParamAsInteger( FRetroactivo, 'Usuario', UsuarioActivo );
                    with FPeriodoDiferencias do
                    begin
                         ParamAsInteger( FRetroactivo, 'Year',  Year );
                         ParamAsInteger( FRetroactivo, 'Tipo', Ord( Tipo ) );
                         ParamAsInteger( FRetroactivo, 'Numero', Numero );
                    end;
                    { Para Agregar N�minas }
                    //CV: CreateQuery--ok
                    //Variable local
                    FAgregaNomina := CreateQuery( GetSQLScript( Q_DIFERENCIAS_AGREGA_NOMINA ) );
                    with FPeriodoDiferencias do
                    begin
                         ParamAsInteger( FAgregaNomina, 'Year', Year );
                         ParamAsInteger( FAgregaNomina, 'Tipo', Ord( Tipo ) );
                         ParamAsInteger( FAgregaNomina, 'Numero', Numero );
                    end;

                    with Dataset do
                    begin
                         while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                         begin
                              EmpiezaTransaccion;
                              try
                                 iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;

                                 { Nos aseguramos que hay un registro padre NOMINA }
                                 ParamAsInteger( FAgregaNomina, 'Empleado', iEmpleado );
{$ifdef INTERBASE}
                                 with FAgregaNomina do
                                 begin
                                      Active := True;
                                      Active := False;
                                 end;
{$else}
                                 ParamSalida( FAgregaNomina, 'Rotativo' );
                                 Ejecuta( FAgregaNomina );
{$endif}
                                 {Original y Nueva se refieren a los
                                 Periodos especificados en el Wizard
                                 Aqu� estamos calculando diferencias
                                 entre conceptos que aparecen en las
                                 dosNominas y conceptos que s�lo
                                 aparecen en la Original y no aparecen
                                 en la Nueva}
                                 ProcesaDiferencias( FPeriodoOriginal, FPeriodoNuevo, True );
                                 { NOTA: el orden de los primeros 2
                                 par�metros es inverso a la primera
                                 llamada Conceptos que s�lo aparecen
                                 en la Nueva y no aparecen en la Original}
                                 ProcesaDiferencias( FPeriodoNuevo, FPeriodoOriginal, False );

                                 ParamAsInteger( FStatusNomina,'Empleado', iEmpleado );
                                 Ejecuta( FStatusNomina );
                                 TerminaTransaccion( True );
                              except
                                    on Error: Exception do
                                    begin
                                         //TerminaTransaccion( False );
                                         RollBackTransaccion;
                                         Log.Excepcion( iEmpleado, 'Error Al  ', Error, DescripcionParams );
                                    end;
                              end;
                              Next;
                         end;
                    end;
                    EmpiezaTransaccion;
                    try
                       SetStatusPeriodo( FPeriodoDiferencias );
                       TerminaTransaccion( True );
                    except
                          on Error: Exception do
                          begin
                               TerminaTransaccion( False );
                               Log.Excepcion( 0, 'Error Al Cambiar Status del Periodo', Error, DescripcionParams );
                          end;
                    end;
                    { PENDIENTE }
                    {
                    TerminaTransaccion( True );
                    with Sender.Parametros do
                    begin
                         ParamByName( 'iNumeroDiferencias' ).AsInteger := iNominas;
                         ParamByName( 'iStatusDiferencias' ).AsInteger := iMovimientos;
                    end;
                    except
                          on Error:Exception do
                          begin
                               TerminaTransaccion( False );
                               Sender.Bitacora.Excepcion( iEmpleado, 'Error al Copiar N�minas', Error );
                          end;
                    end;
                    }
               end;
               Result := CloseProcess;
               FreeAndNil( FAgregaNomina );
               FreeAndNil( FComparaNominas );
               FreeAndNil( FRetroActivo );
               FreeAndNil( FStatusNomina );
          end;
     end;
end;

procedure TdmServerNominaTimbrado.ActualizaFolios( const iFolio, iInicial, iFinal: Integer );
var
   FDataset: TZetaCursor;
begin
     with oZetaProvider do
     begin
          FDataset := CreateQuery( GetSQLScript( Q_FOLIOS_UPDATE_LIMITES ) );
          try
             EmpiezaTransaccion;
             try
                ParamAsInteger( FDataset, 'Inicial', iInicial );
                ParamAsInteger( FDataset, 'Final', iFinal );
                ParamAsInteger( FDataset, 'Folio', iFolio );
                Ejecuta( FDataset );
                TerminaTransaccion( True );
             except
                   on Error: Exception do
                   begin
                        TerminaTransaccion( False );
                        Log.Excepcion( 0, 'Error Al Actualizar Limites', Error, DescripcionParams );
                   end;
             end;
          finally
                 FreeAndNil( FDataset );
          end;
     end;
end;

procedure TdmServerNominaTimbrado.FoliarRecibosParametros;
begin
     with oZetaProvider, ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Periodo: ' + ZetacommonTools.ShowNomina( DatosPeriodo.Year, Ord( DatosPeriodo.Tipo ), DatosPeriodo.Numero ) +
                         K_PIPE + 'F�lio: ' + ParamByName( 'Folio' ).AsString +
                         K_PIPE + 'Orden: ' + ParamByName( 'Orden' ).AsString +
                         K_PIPE + 'F�lio Inicial: ' + ParamByName( 'FolioInicial' ).AsString;
          FListaFormulas :=    VACIO;
          FListaFormulas :=    K_PIPE + 'F�rmula de Monto: ' + ParamByName( 'Formula' ).AsString;
     end;
end;

function TdmServerNominaTimbrado.FoliarRecibos(Empresa,Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'FoliarRecibos');
     try
        InitBroker;
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList(Parametros);
             GetDatosPeriodo;
             InitArregloTPeriodo;
        end;
        FoliarRecibosParametros;
        Result := FoliarRecibosDataset;
     finally
            ClearBroker;
     end;
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.FoliarRecibosDataset: OleVariant;
var
   iFolio, iFolioInicial, iConsecutivo, iBrinco, iFinal, iEmpleado: Integer;
   rMoneda, rMonto, rBrincoFloat: TPesos;
   lRepite, lCeros, lActualizaFolio, lAgregoOrdenNumEmp: Boolean;
   sCampo, sFormula: String;
   FOrdenFolios, FUpdateFolio, FDataSetFolio: TZetaCursor;
begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               iFolio := ParamByName( 'Folio' ).AsInteger;
               sFormula := ParamList.ParamByName( 'Formula' ).AsString;
               iFolioInicial := ParamByName( 'FolioInicial' ).AsInteger;
               lActualizaFolio := False;
          end;
          if ZetaCommonTools.StrVacio( sFormula ) then
            DataBaseError( 'F�rmula de Monto est� Vac�a' );
          //CV: CreateQuery--ok
          //Variable local
          FDatasetFolio := CreateQuery;
          try
             AbreQueryScript( FDataSetFolio, Format( GetSQLScript( Q_FOLIOS_SELECT ), [ iFolio ] ) );
             with FDataSetFolio do
             begin
                  iConsecutivo := iFolioInicial;
                  iFinal := iConsecutivo;
                  lCeros := ZetaCommonTools.zStrToBool( FieldByName( 'FL_CEROS' ).AsString );
                  lRepite := ZetaCommonTools.zStrToBool( FieldByName( 'FL_REPITE' ).AsString );
                  rMoneda := FieldByName( 'FL_MONEDA' ).AsFloat;
             end;
          finally
                 FreeAndNil(FDataSetFolio);
          end;
          with SQLBroker do
          begin
               Init( enNomina );
               with Agente do
               begin
                    AgregaColumna( 'CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
                    AgregaColumna( sFormula, False, enNinguno, tgAutomatico, 0, 'FOLIOS_MONTO' );
                    with oZetaProvider.DatosPeriodo do
                    begin
                         AgregaFiltro( Format( '( PE_YEAR = %d )', [ Year ] ), True, Entidad );
                         AgregaFiltro( Format( '( PE_TIPO = %d )', [ Ord( Tipo ) ] ), True, Entidad );
                         AgregaFiltro( Format( '( PE_NUMERO = %d )', [ Numero ] ), True, Entidad );
                    end;
                    //CV: CreateQuery--ok
                    //Variable local
                    FOrdenFolios := CreateQuery;
                    lAgregoOrdenNumEmp := False;
                    try
                       AbreQueryScript( FOrdenFolios, Format( GetSQLScript( Q_FOLIOS_SELECT_ORDEN ), [iFolio]));
                       with FOrdenFolios do
                       begin
                            while not Eof do
                            begin
                                 sCampo := FieldByName( 'OF_CAMPO' ).AsString;
                                 if ( Pos('CB_CODIGO', UpperCase( sCampo ) ) > 0 ) then
                                    lAgregoOrdenNumEmp := True;
                                 AgregaOrden( sCampo,
                                              not ZetaCommonTools.zStrToBool( FieldByName( 'OF_DESCEND' ).AsString ),
                                              enNinguno );
                                 Next;
                            end;
                       end;
                    finally
                           FreeAndNil(FOrdenFolios);
                    end;
                    if not lAgregoOrdenNumEmp then
                        AgregaOrden( 'CB_CODIGO', True, Entidad );
               end;
               AgregaRangoCondicion( oZetaProvider.ParamList );
               FiltroConfidencial( oZetaProvider.EmpresaActiva );
               if not BuildDataset( oZetaProvider.EmpresaActiva ) then
                 DatabaseError( 'Error en Reporte: ' + Agente.GetError( 0 ) );
          end;

          with SQLBroker.SuperReporte.DataSetReporte do
          begin
               if not lCeros then
               begin
                    Filtered := False;
                    Filter := '( FOLIOS_MONTO > 0 )';
                    Filtered := True;
               end;
          end;
          if OpenProcess( prNOFoliarRecibos, SQLBroker.SuperReporte.DataSetReporte.RecordCount, FListaParametros, FListaFormulas ) then
          begin
               { Antes de empezar }
               //CV: CreateQuery--ok
               //Variable local
               FUpdateFolio := CreateQuery( Format( GetSQLScript( Q_FOLIOS_UPDATE_CAMPO ), [iFolio] ) );
               with DatosPeriodo do
               begin
                    ParamAsInteger( FUpdateFolio, 'Year', Year );
                    ParamAsInteger( FUpdateFolio, 'Tipo', Ord( Tipo ) );
                    ParamAsInteger( FUpdateFolio, 'Numero', Numero );
               end;
               { Ejecuci�n }
               with SQLBroker.SuperReporte.DataSetReporte do
               begin
                    while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                    begin
                         iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                         EmpiezaTransaccion;
                         try
                            rMonto := FieldByName( 'FOLIOS_MONTO' ).AsFloat;
                            if lCeros or ( rMonto <> 0 ) then
                            begin
                                 ParamAsInteger( FUpdateFolio, 'Empleado', iEmpleado );
                                 ParamAsInteger( FUpdateFolio, 'Valor', iConsecutivo );
                                 Ejecuta( FUpdateFolio );

                                 lActualizaFolio:= TRUE;  // Con uno solo que actualice
                                 iFinal := iConsecutivo;
                                 //iBrinco := 1; //El algoritmo para folios repetidos no es correcto

                                 if not lRepite then
                                    iBrinco := 1
                                 else
                                 begin
                                      if ( rMonto = 0 ) then
                                         iBrinco := 1
                                      else
                                      begin
                                           rBrincoFloat := rMonto / rMoneda;
                                           iBrinco := Trunc( rBrincoFloat );
                                           if ( Frac( rBrincoFloat ) >= 0.01 ) then
                                              iBrinco := iBrinco + 1;
                                      end;
                                 end;
                                 Inc( iConsecutivo, iBrinco );
                            end;
                            TerminaTransaccion( True );
                         except
                               on Error: Exception do
                               begin
                                    TerminaTransaccion( False );
                                    Log.Excepcion( iEmpleado, 'Error Al Foliar Recibos', Error, DescripcionParams );
                               end;
                         end;
                         Next;
                    end;
                    if lActualizaFolio then
                    begin
                         ActualizaFolios( iFolio, iFolioInicial, iFinal );
                    end;
               end;
               Result := CloseProcess;
               FreeAndNil( FUpdateFolio );
          end;
     end;
end;

{RE-FOLIAR RECIBOS}

procedure TdmServerNominaTimbrado.ReFoliarRecibosParametros;
begin
     with oZetaProvider, ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Periodo: ' + ZetacommonTools.ShowNomina( DatosPeriodo.Year, Ord( DatosPeriodo.Tipo ), DatosPeriodo.Numero ) +
                         K_PIPE + 'F�lio: ' + ParamByName( 'Folio' ).AsString +
                         K_PIPE + 'Renumerar:' +
                         K_PIPE + 'Del: ' + ParamByName( 'RenumerarDel' ).AsString +
                         K_PIPE + 'Al: ' + ParamByName( 'RenumerarAl' ).AsString +
                         K_PIPE + 'F�lio Inicial: ' + ParamByName( 'FolioInicial' ).AsString;
     end;
end;


function TdmServerNominaTimbrado.ReFoliarRecibos(Empresa,Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'ReFoliarRecibos');
     try
        InitBroker;
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList(Parametros);
             GetDatosPeriodo;
             InitArregloTPeriodo;
        end;
        ReFoliarRecibosParametros;
        Result := ReFoliarRecibosDataset;
     finally
            ClearBroker;
     end;
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.ReFoliarRecibosDataset : OleVariant;
var
   iFolio, iRenumerarDel, iRenumerarAl, iFolioInicial, iStart, iEnd: Integer;
   sCampoFolio: String;
   FUpdateFolio: TZetaCursor;
begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               iFolio := ParamByName( 'Folio' ).AsInteger;
               iRenumerarDel := ParamByName( 'RenumerarDel' ).AsInteger;
               iRenumerarAl := ParamByName( 'RenumerarAl' ).AsInteger;
               iFolioInicial := ParamByName( 'FolioInicial' ).AsInteger;
               iStart := iFolioInicial;
               iEnd := iStart;
          end;
          {'select @FOLIO from NOMINA ' +
           'where pe_year = :Year ' +
           'and pe_tipo= :Tipo ' +
           'and pe_numero = :Numero ' +
           'and @FOLIO between :Inicial and :Final ';}
          sCampoFolio := 'NO_FOLIO_' + IntToStr( iFolio );
          with SQLBroker do
          begin
               Init( enNomina );
               with Agente do
               begin
                    AgregaColumna( 'CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
                    AgregaColumna( sCampoFolio, True, Entidad, tgNumero, 0, sCampoFolio );
                    with oZetaProvider.DatosPeriodo do
                    begin
                         AgregaFiltro( Format( '( PE_YEAR = %d )', [ Year ] ), True, Entidad );
                         AgregaFiltro( Format( '( PE_TIPO = %d )', [ Ord( Tipo ) ] ), True, Entidad );
                         AgregaFiltro( Format( '( PE_NUMERO = %d )', [ Numero ] ), True, Entidad );
                         AgregaFiltro( Format( sCampoFolio + ' between %d and %d', [ iRenumerarDel, iRenumerarAl ] ), True, Entidad );
                    end;
                    AgregaOrden( sCampoFolio, TRUE, Entidad );
               end;
               AgregaRangoCondicion( oZetaProvider.ParamList );
               FiltroConfidencial( oZetaProvider.EmpresaActiva );
               BuildDataset( oZetaProvider.EmpresaActiva );
          end;
          if OpenProcess( prNOReFoliarRecibos, SQLBroker.SuperReporte.DataSetReporte.RecordCount, FListaParametros ) then
          begin
               { Antes de empezar }
               //CV: CreateQuery--ok
               //Variable local
               FUpdateFolio := CreateQuery( Format( GetSQLScript( Q_REFOLIAR_UPDATE ), [ sCampoFolio, sCampoFolio ] ) );
               try
                  with DatosPeriodo do
                  begin
                       ParamAsInteger( FUpdateFolio, 'Year', Year );
                       ParamAsInteger( FUpdateFolio, 'Tipo', Ord( Tipo ) );
                       ParamAsInteger( FUpdateFolio, 'Numero', Numero );
                  end;
                  { Ejecuci�n }
                  with SQLBroker.SuperReporte.DataSetReporte do
                  begin
                       while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                       begin
                            EmpiezaTransaccion;
                            try
                               iEnd := iFolioInicial;
                               ParamAsInteger( FUpdateFolio, 'Empleado', FieldByName( 'CB_CODIGO' ).AsInteger );
                               ParamAsInteger( FUpdateFolio, 'NO_FOLIO_1', Fields[ 1 ].AsInteger );
                               ParamAsInteger( FUpdateFolio, 'Folio', iFolioInicial );
                               Ejecuta( FUpdateFolio );
                               iFolioInicial := iFolioInicial + 1;
                               TerminaTransaccion( True );
                            except
                                  on Error: Exception do
                                  begin
                                       TerminaTransaccion( False );
                                       Log.Excepcion( Fields[ 0 ].AsInteger , 'Error Al Re Foliar Recibos ', Error, DescripcionParams );
                                  end;
                            end;
                            Next;
                       end;
                  end;
               finally
                      FreeAndNil( FUpdateFolio ) ;
               end;
               ActualizaFolios( iFolio, iStart, iEnd );
               Result := CloseProcess;
          end;
     end;
end;

{ ******* PAGOS POR FUERA ********* }

procedure TdmServerNominaTimbrado.PagarPorFueraParametros;
begin
     with oZetaProvider.DatosPeriodo do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Periodo: ' + ZetacommonTools.ShowNomina( Year, Ord( Tipo ), Numero );
     end;
end;

function TdmServerNominaTimbrado.PagarPorFuera(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'PagarPorFuera');
     InitBroker;
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList( Parametros );
             GetDatosPeriodo;
             InitArregloTPeriodo;
        end;
        PagarPorFueraParametros;
        PagarPorFueraBuildDataset;
        Result := PagarPorFueraDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
            ClearBroker;
     end;
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.PagarPorFueraLista(Empresa, Lista, Parametros : OleVariant): OleVariant;
begin
     InitLog(Empresa,'PagarPorFueraLista');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          GetDatosPeriodo;
          InitArregloTPeriodo;
     end;
     PagarPorFueraParametros;
     cdsLista.Lista := Lista;
     Result := PagarPorFueraDataset( cdsLista );
     cdsLista.Close;
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.PagarPorFueraGetLista(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'PagarPorFueraGetLista');
     try
        InitBroker;
        with oZetaProvider do
        begin
             EmpresaActiva:= Empresa;
             AsignaParamList( Parametros );
             GetDatosPeriodo;
        end;
        PagarPorFueraBuildDataset;
        Result := SQLBroker.SuperReporte.GetReporte;
     finally
            ClearBroker;
     end;
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.PagarPorFueraDataset(Dataset: TDataset ): OleVariant;
var
   FDatasetPeriodo: TZetaCursor;
   iEmpleado: Integer;
begin
     with oZetaProvider do
     begin
          if OpenProcess( prNOPagosPorFuera, Dataset.RecordCount, FListaParametros ) then
          begin
               { Antes de empezar }
               with DatosPeriodo do
               begin
                    //CV: CreateQuery--ok
                    //Variable local
                    FDatasetPeriodo := CreateQuery( Format( GetSqlScript( Q_PAGAR_FUERA_UPDATE ), [ Year, Ord( Tipo ), Numero ] ) );
               end;
               { Ejecuci�n }
               with Dataset do
               begin
                    while not Eof and CanContinue( Fields[ 0 ].AsInteger ) do
                    begin
                         iEmpleado := Fields[ 0 ].AsInteger;
                         EmpiezaTransaccion;
                         try
                            ParamAsInteger( FDataSetPeriodo, 'EMPLEADO', iEmpleado );
                            Ejecuta( FDataSetPeriodo );
                            TerminaTransaccion( True );
                         except
                               on Error: Exception do
                               begin
                                    TerminaTransaccion( False );
                                    Log.Excepcion( iEmpleado, 'Error Al Procesar Pagos Por Fuera ', Error, DescripcionParams );
                               end;
                         end;
                         Next;
                    end;
               end;
               Result := CloseProcess;
               FreeAndNil(FDataSetPeriodo);
          end;
     end;
end;

procedure TdmServerNominaTimbrado.PagarPorFueraBuildDataset;
begin
     with SQLBroker do
     begin
          Init( enNomina );
          with Agente do
          begin
               AgregaColumna( 'CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
               AgregaColumna( K_PRETTYNAME, TRUE, enEmpleado, tgTexto, 50, 'PrettyName' );
               with oZetaProvider.DatosPeriodo do
               begin
                    AgregaFiltro( Format( '( PE_YEAR = %d )', [ Year ] ), True, Entidad );
                    AgregaFiltro( Format( '( PE_TIPO = %d )', [ Ord(Tipo) ] ), True, Entidad );
                    AgregaFiltro( Format( '( PE_NUMERO = %d )', [ Numero ] ), True, Entidad );
                    AgregaFiltro( Format( '( NO_STATUS = %d )', [ Ord( spCalculadaTotal ) ] ), True, Entidad );
                    AgregaFiltro( '( NO_FUERA = ''N'' )', True, Entidad );
               end;
               AgregaOrden( 'CB_CODIGO', TRUE, Entidad );
          end;
          AgregaRangoCondicion( oZetaProvider.ParamList );
          FiltroConfidencial( oZetaProvider.EmpresaActiva );
          BuildDataset( oZetaProvider.EmpresaActiva );
     end;
end;

{ *********** GENERACION DE POLIZA CONTABLE ************* }

procedure TdmServerNominaTimbrado.PolizaParametros;
 var
    sNomina: string;
begin
     with oZetaProvider, DatosPeriodo do
     begin
          FListaParametros := VACIO;

          with ParamList do
          begin
               if StrVacio( ParamByName('RangoNominas').AsString ) then
               begin
                    sNomina := 'Periodo: ' + ZetacommonTools.ShowNomina( ParamByName('Year').AsInteger, ParamByName('Tipo').AsInteger, ParamByName('Numero').AsInteger );
               end
               else
               begin
                    sNomina := 'Nomina: ' + ParamByName('Year').AsString + ' ' +ZetaCommonLists.ObtieneElemento( lfTipoPeriodo, ParamByName('Tipo').AsInteger ) + CR_LF +
                                 'Rango de per�odos calculados: ' + ConcatString( ParamByName('Numero').AsString, ParamByName('RangoNominas').AsString, ',' );
               end;
          end;
          FListaParametros := sNomina + CR_LF + 'Tipo de p�liza: ' + ParamList.ParamByName( 'CodigoTipo' ).AsString
                              + CR_LF + 'Formato de reporte: ' + ParamList.ParamByName( 'CodigoReporte' ).AsString;
     end;
end;

function TdmServerNominaTimbrado.Poliza(Empresa, Parametros, Filtros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'Poliza');
     try
        InitBroker;
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList(Parametros);
             GetDatosPeriodo;
             InitArregloTPeriodo;
        end;
        PolizaParametros;
        Result := PolizaDataset(Filtros);
     finally
            ClearBroker;
     end;
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.PolizaDataset(Filtros: OleVariant): OleVariant;
 var
    oPoliza: TdmPoliza;
begin
     if ( eTipoReporte( oZetaprovider.ParamList.ParamByName('TipoPolizaReporte').AsInteger ) = trPolizaConcepto ) then
     begin
          oPoliza := TPolizaConcepto.Create( oZetaProvider, oZetaCreator )
     end
     else
     begin
          oPoliza := TPolizaGrupo.Create( oZetaProvider, oZetaCreator );
     end;

     try
        Result := oPoliza.Calcula(Filtros,FListaParametros);
     finally
            FreeAndNil( oPoliza );
     end;
end;

{*************** Liquidacion Global ********************}

Procedure TdmServerNominaTimbrado.LiquidacionGlobalParametros;
    function GetTextEncimar(Encimar:string):string;
    begin
         Result := 'Dejar la Anterior';
         if zStrToBool(Encimar) then
            Result := 'Encimar Nueva';
    end;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := FListaParametros + K_PIPE + 'Tipo Liquidaci�n : ' + ZetaCommonLists.ObtieneElemento(ZetaCommonLists.lfLiqNomina, ParamByName('TipoLiquidacion').AsInteger);
          FListaParametros := FListaParametros + K_PIPE + 'Observaciones: ' + ParamByName('Observaciones').AsString;
          if ParamByName('Simulacion').AsString  = K_GLOBAL_SI  then
          begin
               if oZetaProvider.GetGlobalBooleano(K_GLOBAL_SIM_FINIQ_APROBACION )then
                  FListaParametros := FListaParametros + K_PIPE + 'En Simulaciones Aprobadas: '+GetTextEncimar(ParamByName('EncimarAprobada').AsString);
               FListaParametros := FListaParametros + K_PIPE + 'En Simulaciones Aplicadas: '+GetTextEncimar(ParamByName('EncimarAplicada').AsString);
               if ParamByName('FechaBaja').AsDate = NullDateTime then
                     FListaParametros := FListaParametros + K_PIPE + 'Incluir Empleados Dados de Baja: No'
               else
                     FListaParametros := FListaParametros + K_PIPE + 'Incluir Empleados Dados de Baja desde: '+DateToStr(ParamByName('FechaBaja').AsDate);
          end;
     end;
     with oZetaProvider.DatosPeriodo do
     begin

          FListaParametros := FListaParametros + K_PIPE + 'Periodo: ' + ZetacommonTools.ShowNomina( Year, Ord( Tipo ), Numero );
     end;
end;

function TdmServerNominaTimbrado.LiquidacionGlobalSimulacion(Dataset: TzProviderClientDataSet):Olevariant;
 var
    i: eTipoPeriodo;
    oProcess: OleVariant;
begin
     SetOLEVariantToNull( Result );
     Result := GetEmptyProcessResultFecha( prNOLiquidacion );

     {*** US 15500: Los c�lculos de n�mina desde Simulaci�n de finiquitos tarda mucho tiempo ***}
     for i:= 0 to FArregloPeriodo.Count do
     begin
          with Dataset do
          begin
               Filtered := FALSE;
               Filter := Format( 'CB_NOMINA = %d', [Ord(i)] );
               Filtered := TRUE;
               if ( Dataset.RecordCount > 0 ) then
               begin
                    with oZetaProvider do
                    begin
                         ParamList.ParamByName('TIPO').AsInteger := Ord(i);
                         ParamList.ParamByName('NUMERO').AsInteger := GetGlobalInteger( K_GLOBAL_SIMULACION_FINIQUITOS );
                         InicializaValoresActivos;
                         GetDatosPeriodo;
                         LiquidacionGlobalParametros;
                    end;

                    oProcess := LiquidacionGlobalDataset( Dataset );
                    SumaProcessResult( Result, Result, oProcess );

               end;
          end;
     end;
end;

function TdmServerNominaTimbrado.LiquidacionGlobal(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'LiquidacionGlobal');
     InitBroker;
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList( Parametros );
             InitGlobales;
             InitArregloTPeriodo;
        end;
        if oZetaProvider.ParamList.ParamByName('Simulacion').AsString = K_GLOBAL_SI then
        begin
             LiquidacionSimulacionGlobalBuildDataset;
             Result := LiquidacionGlobalSimulacion(SQLBroker.SuperReporte.DataSetReporte);
        end
        else
        begin
             //Liquidacion que se realiza por nomina.
             oZetaProvider.GetDatosPeriodo;
             LiquidacionGlobalParametros;
             LiquidacionGlobalBuildDataset;
             Result := LiquidacionGlobalDataset( SQLBroker.SuperReporte.DataSetReporte );
        end;
     finally
            ClearBroker;
     end;
     EndLog;SetComplete;
end;

procedure TdmServerNominaTimbrado.LiquidacionGlobalBuildDataset;
begin
     with oZetaProvider do
     begin
          with SQLBroker do
          begin
               Init( enEmpleado );
               with Agente do
               begin
                    AgregaColumna( 'CB_CODIGO', True, Entidad, tgNumero, 7, 'CB_CODIGO' );
                    AgregaColumna( K_PRETTYNAME, TRUE, Entidad, tgTexto, 50, 'PrettyName' );
                    AgregaColumna( 'CB_FEC_BAJ', True, Entidad, tgFecha, 0, 'CB_FEC_BAJ' );
                    AgregaColumna( 'CB_FEC_ING', True, Entidad, tgFecha, 0, 'CB_FEC_ING' );
                    AgregaColumna( 'CB_NOMINA', True, Entidad, tgNumero, 0, 'CB_NOMINA' );

                    AgregaAgenteFunciones( Agente );
                    AgregaFiltro( 'CB_ACTIVO = ''N''', True, Entidad );

                    with DatosPeriodo do
                    begin
                         AgregaFiltro( Format( 'CB_NOMYEAR = %d', [ Year ] ), True, Entidad );
                         AgregaFiltro( Format( 'CB_NOMTIPO = %d', [ Ord( Tipo ) ] ), True, Entidad );
                         AgregaFiltro( Format( 'CB_NOMNUME = %d', [ Numero ] ), True, Entidad );
                    end;
                    AgregaOrden( 'CB_CODIGO', TRUE, Entidad );
               end;
               AgregaRangoCondicion( ParamList );
               FiltroConfidencial( EmpresaActiva );
               BuildDataset( EmpresaActiva );
          end;
     end;
end;

procedure TdmServerNominaTimbrado.LiquidacionSimulacionGlobalBuildDataset;
var
   PeriodoSimulacion :Integer;
   EncimarAprobada :string;
   EncimarAplicada:string;
   FiltroBaja:string;
const
     K_FILTRO_SIM_MSSQL = '( ( ( colabora.CB_ACTIVO = ''%0:s'') %4:s )'+
                          '   and ( ( not ( colabora.cb_codigo in ( select nomina.cb_codigo from nomina where pe_numero = %1:d and pe_year = %5:d and pe_tipo = colabora.cb_nomina and NO_GLOBAL = ''S'' ) ) )   '+
                          '         or ( '+
                          //'                 colabora.cb_codigo in (select nomina.cb_codigo from nomina where pe_numero = %1:d and pe_year = %5:d and pe_tipo = colabora.cb_nomina and NO_GLOBAL = ''N'' )'+
                          '               colabora.cb_codigo in (select nomina.cb_codigo from nomina where pe_numero = %1:d and pe_year = %5:d and pe_tipo = colabora.cb_nomina and NO_GLOBAL = ''S'' and NO_APROBA = 0  ) '+
                          '              or colabora.cb_codigo in (select nomina.cb_codigo from nomina where pe_numero = %1:d and pe_year = %5:d and pe_tipo = colabora.cb_nomina and NO_GLOBAL = ''S'' and NO_APROBA = 1 and ''%2:s'' = ''S'' ) '+
                          '              or colabora.cb_codigo in (select nomina.cb_codigo from nomina where pe_numero = %1:d and pe_year = %5:d and pe_tipo = colabora.cb_nomina and NO_GLOBAL = ''S'' and NO_APROBA = 2 and ''%3:s'' = ''S'' ) '+
                          '            ) '+
                          '       ) )';
     K_FILTRO_DESDE = ' or ( CB_FEC_BAJ >= ''%s'' ) ';
begin
     with oZetaProvider do
     begin
          with SQLBroker do
          begin
               Init( enEmpleado );
               with Agente do
               begin
                    AgregaColumna( 'CB_CODIGO', True, Entidad, tgNumero, 7, 'CB_CODIGO' );
                    AgregaColumna( K_PRETTYNAME, TRUE, Entidad, tgTexto, 50, 'PrettyName' );
                    AgregaColumna( 'CB_FEC_BAJ', True, Entidad, tgFecha, 0, 'CB_FEC_BAJ' );
                    AgregaColumna( 'CB_ACTIVO', True, Entidad, tgTexto, 1, 'CB_ACTIVO' );
                    AgregaColumna( 'CB_FEC_ING', True, Entidad, tgFecha, 0, 'CB_FEC_ING' );
                    AgregaColumna( 'CB_NOMINA', True, Entidad, tgNumero, 7, 'CB_NOMINA' );

                    AgregaAgenteFunciones( Agente );

                    PeriodoSimulacion := GetGlobalInteger(K_GLOBAL_SIMULACION_FINIQUITOS );
                    EncimarAprobada := ParamList.ParamByName('EncimarAprobada').AsString;
                    EncimarAplicada := ParamList.ParamByName('EncimarAplicada').AsString;

                    FiltroBaja := VACIO;
                    if ParamList.ParamByName('FechaBaja').AsDate <> NullDateTime then
                       FiltroBaja := Format ( K_FILTRO_DESDE,[ DateToStrSQL(ParamList.ParamByName('FechaBaja').AsDate ) ] );

                    AgregaFiltro( Format(K_FILTRO_SIM_MSSQL,[K_GLOBAL_SI,PeriodoSimulacion,EncimarAprobada,EncimarAplicada,FiltroBaja,ParamList.ParamByName('Year').AsInteger ]),True,enNinguno );
                    //AgregaFiltro( Format('COLABORA.CB_NOMINA=%d',[ParamList.ParamByName('Tipo').AsInteger ]),True,enNinguno );

                    AgregaOrden( 'CB_CODIGO', TRUE, Entidad );
               end;
               AgregaRangoCondicion( ParamList );
               FiltroConfidencial( EmpresaActiva );
               BuildDataset( EmpresaActiva );

          end;
     end;
end;


function TdmServerNominaTimbrado.PuedeAgregarLiquidacion( const Empleado : integer;
                                                  const dFecha, dBaja : TDate; var iPeriodo: Integer ):Boolean;
var
   iPerSimulacion: Integer;
begin
     with oZetaProvider do
     begin
          iPerSimulacion := GetGlobalInteger( K_GLOBAL_SIMULACION_FINIQUITOS );
          Result := ( DatosPeriodo.Numero = iPerSimulacion );   // Siempre se debe poder agregar liquidaci�n en simulaci�n
          if ( not Result ) then
          begin
               //CV: CreateQuery--ok
               //No se vuelve a crear el Query, no tiene FreeAndNil
               if FHayLiquidacion = NIL then
                  FHayLiquidacion := CreateQuery( GetSQLScript( Q_LIQUIDACION_HAY_NOMINA ) );

               { Correcci�n defecto 1493: Ya no se trae la n�mina en la que se ingreso el empleado, al menos que la �ltima baja
               haya caido en esa misma n�mina}

               with DatosPeriodo do
               begin
                    ParamAsInteger( FHayLiquidacion, 'Year', Year );
                    ParamAsInteger( FHayLiquidacion, 'Tipo', Ord( Tipo ) );
               end;
               ParamAsInteger( FHayLiquidacion, 'Simulacion', iPerSimulacion );
               ParamAsInteger( FHayLiquidacion, 'Empleado', Empleado );
               ParamAsDate( FHayLiquidacion, 'Fecha', dFecha );
               ParamAsDate( FHayLiquidacion, 'FechaIngreso', dFecha );
               ParamAsDate( FHayLiquidacion, 'FechaBaja', dBaja );

               with FHayLiquidacion do
               begin
                    Active := True;
                    if EOF then
                       Result := True
                    else
                    begin
                         iPeriodo := FieldByName( 'PE_NUMERO' ).AsInteger;
                         Result := ( iPeriodo = DatosPeriodo.Numero ) or ( iPeriodo = 0 );
                    end;
                    Active := False;
               end;
          end;
     end;
end;

function TdmServerNominaTimbrado.LiquidacionGlobalDataset( Dataset: TDataset ): OleVariant;
var
   FAhorros, FPrestamos: TZetaCursorLocate;
   FLiquidacion: TDatosLiquidacion;
   iEmpleado, iPeriodo: TNumEmp;
   //lPuedeAgregar: Boolean;
begin
     with oZetaProvider do
     begin
          with Dataset do
          begin
               if OpenProcess( prNOLiquidacion, RecordCount, FListaParametros ) then
               begin
                    try
                       GetDatosLiquidacionBegin;
                       //InitNomina;
                       try
                          { Antes de empezar }
                          Nomina.LiquidacionBegin;
                          FAhorros := CreateQueryLocate;
                          PreparaQuery(FAhorros,Format( GetSQLScript( Q_LIQUIDACION_AHORROS ), [ ':Empleado' ] ) );
                          FPrestamos := CreateQueryLocate;
                          PreparaQuery(FPrestamos, Format( GetSQLScript( Q_LIQUIDACION_PRESTAMOS ), [ ':Empleado' ] ) );
                          {
                          FHayNomina := CreateQuery( GetSQLScript( Q_LIQUIDACION_HAY_NOMINA ) );
                          with DatosPeriodo do
                          begin
                               ParamAsInteger( FHayNomina, 'Year', Year );
                               ParamAsInteger( FHayNomina, 'Tipo', Ord( Tipo ) );
                          end;
                          }
                          {Ejecuci�n }
                          while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                          begin
                               iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                               {
                               ParamAsInteger( FHayNomina, 'Empleado', iEmpleado );
                               ParamAsDate( FHayNomina, 'Fecha', FieldByName( 'CB_FEC_ING' ).AsDateTime );
                               with FHayNomina do
                               begin
                                    Active := True;
                                    if IsEmpty then
                                       lPuedeAgregar := True
                                    else
                                    begin
                                         with FieldByName( 'PE_NUMERO' ) do
                                         begin
                                              lPuedeAgregar := ( AsInteger = DatosPeriodo.Numero ) or ( AsInteger = 0 );
                                         end;
                                    end;
                                    Active := False;
                               end;
                               if lPuedeAgregar then
                               }
                               if PuedeAgregarLiquidacion(iEmpleado,FieldByName( 'CB_FEC_ING' ).AsDateTime,FieldByName('CB_FEC_BAJ').AsDateTime, iPeriodo) then
                               begin
                                    if zStrToBool(ParamList.ParamByName('Simulacion').AsString ) then
                                    begin
                                         if zStrToBool(FieldByName( 'CB_ACTIVO' ).AsString) then
                                         begin
                                              FLiquidacion := GetDatosLiquidacion( Dataset, iEmpleado, ParamList.ParamByName('FechaSimulacion').AsDateTime);
                                         end
                                         else
                                             FLiquidacion := GetDatosLiquidacion( Dataset, iEmpleado );
                                    end
                                    else
                                        FLiquidacion := GetDatosLiquidacion( Dataset, iEmpleado );

                                    if ( FLiquidacion.ConfirmarAguinaldo = False ) then
                                    begin
                                         if ( FLiquidacion.Tipo = lnNormal ) OR ( not FLiquidacion.NominaExiste )  then
                                         begin
                                              with ParamList do
                                              begin
                                                   Fliquidacion.Tipo := eLiqNomina( ParamByName( 'TipoLiquidacion').AsInteger );
                                                   FLiquidacion.Observaciones := ParamByName('Observaciones').AsString;
                                              end;
                                         end;
                                         {PENDIENTE}

                                         {$ifdef DOS_CAPAS}
                                         FAhorros.ParamByName('Empleado').AsInteger := iEmpleado;
                                         {$else}
                                         ParamAsInteger( FAhorros, 'Empleado', iEmpleado );
                                         {$endif}
                                         FAhorros.Active := True;
                                         {PENDIENTE}

                                         {$ifdef DOS_CAPAS}
                                         FPrestamos.ParamByName('Empleado').AsInteger := iEmpleado;
                                         {$else}
                                         ParamAsInteger( FPrestamos, 'Empleado', iEmpleado );
                                         {$endif}
                                         FPrestamos.Active := True;
                                         FLiquidacion.Global := (ParamList.ParamByName('Simulacion').AsString = K_GLOBAL_SI );

                                         EmpiezaTransaccion;
                                         try
                                            Nomina.Liquidacion( FLiquidacion, FAhorros, FPrestamos );
                                            TerminaTransaccion( True );
                                         except
                                               on Error: Exception do
                                               begin
                                                    //TerminaTransaccion( False );
                                                    RollBackTransaccion;
                                                    Log.Excepcion( iEmpleado, 'Error Al Procesar Liquidaci�n ', Error, DescripcionParams );
                                               end;
                                         end;
                                         FPrestamos.Active := False;
                                         FAhorros.Active := False;
                                    end
                                    else
                                        Log.Error( iEmpleado, 'Liquidaci�n No Generada', 'Ya Existe Pago De Aguinaldo Previo. Se Deber� Usar Liquidaci�n Individual'  );
                               end
                               else
                                   Log.Error( iEmpleado, 'Error al Calcular Liquidaci�n', 'El Empleado ya fu� Liquidado en el Periodo #' + IntToStr(iPeriodo) );
                               Next;
                          end;
                          Nomina.CalculaStatusPeriodo;
                       finally
                              Nomina.LiquidacionEnd;
                       end;
                    finally
                           //ClearNomina;
                           GetDatosLiquidacionEnd;
                           FreeAndNil(FAhorros);
                           FreeAndNil(FPrestamos);
                    end;
               end;
          end;
          Result := CloseProcess;
     end;
end;

function TdmServerNominaTimbrado.LiquidacionGlobalGetLista(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'LiquidacionGlobalGetLista');
     InitBroker;
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList( Parametros );
             GetDatosPeriodo;
             InitGlobales;
        end;
        if oZetaProvider.ParamList.ParamByName('Simulacion').AsString = K_GLOBAL_SI then
        begin
             LiquidacionSimulacionGlobalBuildDataset;
        end
        else
            LiquidacionGlobalBuildDataset;

        Result := SQLBroker.SuperReporte.GetReporte;
     finally
            ClearBroker;
     end;
     EndLog;SetComplete;

end;

function TdmServerNominaTimbrado.LiquidacionGlobalLista( Empresa, Lista, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'LiquidacionGlobalLista');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          InitGlobales;
          InitArregloTPeriodo;
     end;
     cdsLista.Lista := Lista;

     if (oZetaProvider.ParamList.ParamByName('Simulacion').AsString = K_GLOBAL_SI) then
        Result := LiquidacionGlobalSimulacion( cdsLista )
     else
     begin
          oZetaProvider.GetDatosPeriodo;
          LiquidacionGlobalParametros;
          Result := LiquidacionGlobalDataset( cdsLista );
     end;
     EndLog;SetComplete;
end;

{ *********** Importar Movimientos de Nomina ********** }

procedure TdmServerNominaTimbrado.ImportarMovimientosParametros;
begin
     with oZetaProvider, ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Periodo: ' + ZetacommonTools.ShowNomina( DatosPeriodo.Year, Ord( DatosPeriodo.Tipo ), DatosPeriodo.Numero ) +
                         K_PIPE + 'Importar Hacia: ' + ObtieneElemento( lfImportacion, ParamByName( 'Importacion' ).AsInteger );
          if ( eimportacion( ParamByName( 'Importacion' ).AsInteger ) = eiAcumulados ) then
             FListaParametros := FListaParametros + K_PIPE + 'A�o: ' + ParamByName( 'YearAcum' ).AsString +
                              K_PIPE + 'Mes: ' + ZetaCommonLists.ObtieneElemento( lfMes13, ( ParamByName( 'MesAcum' ).AsInteger - 1 ) );
          FListaParametros := FListaParametros + K_PIPE + 'Archivo: ' + ParamByName( 'Archivo' ).AsString +
                                                 K_PIPE + 'Operaci�n: ' + ObtieneElemento( lfOperacionMontos, ParamByName( 'Operacion' ).AsInteger ) +
                                                 K_PIPE + 'Formato: ' + ObtieneElemento( lfFormatoASCII, ParamByName( 'Formato' ).AsInteger );
     end;
end;

function TdmServerNominaTimbrado.ImportarMovimientos(Empresa, Parametros, Datos: OleVariant): OleVariant;
var
   iEmpleado: TNumEmp;
   iConcepto, iMotivo: Integer;
   eConcepto: eTipoConcepto;
   eExcepcion: eTipoExcepcion;
   eImportar: eImportacion;
   eOperacion: eOperacionMontos;
   sReferencia, sDiaHora: String;
   dValue: TDate;
   rValue: TPesos;
   lCalculaDias, lNoIncapacitados: Boolean;{OP: 12/06/08}

{
procedure PreparaEmpleadoAnterior;
begin
     if ( FEmpleado > 0 ) then
     begin
          Nomina.PreparaDiasHoras( FEmpleado );
     end;
end;
}

{OP: 24/06/08}
function EstaIncapacitado: Boolean;
var
   FEmpInca: TZetaCursor;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FEmpInca := CreateQuery
          ( Format( GetSQLScript( Q_EMP_INCA ), [ ZetaCommonTools.DateToStrSQLC( DatosPeriodo.InicioAsis ),
                                                                      ZetaCommonTools.DateToStrSQLC( DatosPeriodo.FinAsis ),
                                                                      iEmpleado ] ) );
          try
             FEmpInca.Active := True;
             if ( not FEmpInca.EOF ) then
             begin
                   Result := ( zStrToBool( FEmpInca.FieldByName( 'RESULTADO' ).AsString ) )
             end
             else
                 Result := False;
             FEmpInca.Active := False;
          finally
                 FreeAndNil( FEmpInca );
          end;
     end;
end;

function ValidaDerechosLimiteConcepto:Boolean;
const
     K_QRY_CONCEPTO = 'select CO_DESCRIP,CO_VER_INF,CO_VER_SUP,CO_VER_ACC,CO_LIM_SUP,CO_LIM_INF,CO_GPO_ACC from CONCEPTO where CO_NUMERO = :Concepto ';
     K_QRY_GRUPO =    'select GR_CODIGO from USUARIO where US_CODIGO = :Usuario';
var
   FConcepto : TZetaCursor;
   FGrupoActivo : TZetaCursor;
   GrupoActivo :Integer;
   Empresa:OleVariant;
   Usuario :Integer;
   ValorTemp,rDeduccion,rPercepcion:TPesos;

   procedure VerificaGrupoRegistroExcepciones(iConcepto:Integer);
   begin
        with FConcepto do
        begin
             if ( ( not( GrupoActivo = D_GRUPO_SIN_RESTRICCION ) ) and ( zStrToBool( FieldByName('CO_VER_ACC').AsString ) and ( not ( Pos( IntToStr( GrupoActivo),FieldByName('CO_GPO_ACC').AsString ) > 0 ) ) ) ) then
             begin
                  Result:= False;
                  oZetaProvider.Log.Error(iEmpleado,'Accesos a registrar excepciones al concepto', Format( 'No tiene permisos para capturar excepciones al concepto #%d - %s',[iConcepto,FieldByName('CO_DESCRIP').AsString ] ) );
             end;
        end;
   end;

   procedure VerificaRegistroExcepciones(iConcepto:Integer;Monto:TPesos);
   begin
        with FConcepto do
        begin
             if ( ( zStrToBool( FieldByName('CO_VER_INF').AsString ) ) and ( Monto < FieldByName('CO_LIM_INF').AsFloat ) )then
             begin
                  if oZetaProvider.ParamList.ParamByName('ExcederLimites').AsBoolean then
                  begin
                       oZetaProvider.Log.Advertencia(iEmpleado,Format('No alcanz� el l�mite m�nimo concepto %d',[iConcepto]), Format ( 'Monto m�nimo permitido en el concepto # %d-%s es: %s',[iConcepto,FieldByName('CO_DESCRIP').AsString,FormatFloat('$#,###.00',FieldByName('CO_LIM_INF').AsFloat ) ] ) );
                  end
                  else
                  begin
                       Result:= False;
                       oZetaProvider.Log.Error(iEmpleado,Format('No alcanz� el l�mite m�nimo concepto %d',[iConcepto]), Format ( 'Monto m�nimo permitido en el concepto # %d-%s es: %s',[iConcepto,FieldByName('CO_DESCRIP').AsString,FormatFloat('$#,###.00',FieldByName('CO_LIM_INF').AsFloat ) ] ) );
                  end;
             end;

             if ( ( zStrToBool( FieldByName('CO_VER_SUP').AsString ) ) and ( Monto > FieldByName('CO_LIM_SUP').AsFloat ) )then
             begin
                  if oZetaProvider.ParamList.ParamByName('ExcederLimites').AsBoolean then
                  begin
                       oZetaProvider.Log.Advertencia(iEmpleado,Format('Se excedi� el l�mite m�ximo concepto %d',[iConcepto]),Format ( 'Monto m�ximo permitido en el concepto # %d-%s es: %s',[iConcepto,FieldByName('CO_DESCRIP').AsString, FormatFloat('$#,###.00',FieldByName('CO_LIM_SUP').AsFloat )] ));
                  end
                  else
                  begin
                       Result:= False;
                       oZetaProvider.Log.Error(iEmpleado,Format('Se excedi� el l�mite m�ximo concepto %d',[iConcepto]), Format ( 'Monto m�ximo permitido en el concepto # %d-%s es: %s',[iConcepto,FieldByName('CO_DESCRIP').AsString, FormatFloat('$#,###.00',FieldByName('CO_LIM_SUP').AsFloat )] ) );
                  end;
             end;
        end;
   end;

begin
     with oZetaProvider do
     begin
          Result := True;
          try
             try
                FConcepto := CreateQuery( K_QRY_CONCEPTO );
                ParamAsInteger(FConcepto,'Concepto',iConcepto);
                Usuario := UsuarioActivo;
                FConcepto.Active := True;
                try
                   Empresa := EmpresaActiva;
                   EmpresaActiva := Comparte;
                   FGrupoActivo := CreateQuery( K_QRY_GRUPO );
                   ParamAsInteger(FGrupoActivo,'Usuario',Usuario);
                   Ejecuta(FGrupoActivo);
                   GrupoActivo := FGrupoActivo.FieldByName('GR_CODIGO').AsInteger;
                finally
                       Result := True;
                       EmpresaActiva := Empresa;
                       FreeAndNil(FGrupoActivo);
                end;
                ValorTemp := rValue;

                VerificaGrupoRegistroExcepciones(iConcepto);

                with Nomina.MovimientExiste do
                begin
                     Active := False;
                     ParamAsInteger( Nomina.MovimientExiste, 'Empleado', iEmpleado );
                     ParamAsInteger( Nomina.MovimientExiste, 'Concepto', iConcepto );
                     ParamAsVarChar( Nomina.MovimientExiste, 'Referencia', sReferencia, K_ANCHO_REFERENCIA );
                     Active := True;
                     rPercepcion := FieldByName( 'MO_PERCEPC' ).AsFloat;
                     rDeduccion := FieldByName( 'MO_DEDUCCI' ).AsFloat;
                end;

                if Result then
                begin
                     case eOperacion of
                       omSumar: ValorTemp := ( rPercepcion + rDeduccion ) + ValorTemp;
                       omRestar: ValorTemp := ( rPercepcion + rDeduccion ) - ValorTemp;
                       omSustituir: ValorTemp := ValorTemp;
                     end;
                     VerificaRegistroExcepciones(iConcepto,ValorTemp);
                end;
             finally
                    FreeAndNil(FConcepto);
             end;
          except
                on Error: Exception do
                begin
                     Result := False;
                     Log.Excepcion(iEmpleado,'Excepci�n al validar Limites/Acceso ',Error );
                end;
          end;
     end;
end;

procedure ImportaExcepciones;
begin
     with Nomina do
     begin
          PreparaDiasHorasBegin;
          try
             VerificaNominaBegin;
             try
                ExcepcionDiaHoraBegin;
                ExcepcionMontoBegin;
                InitBufferEmpleado;
                with cdsLista do
                begin
                     FEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                     lCalculaDias := FALSE;
                     while not EOF and oZetaProvider.CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                     begin
                          iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                          iConcepto := FieldByName( 'CO_NUMERO' ).AsInteger;
                          iMotivo := FieldByName( 'FA_MOTIVO' ).AsInteger;
                          sReferencia := FieldByName( 'MO_REFEREN' ).AsString;
                          sDiaHora := FieldByName( 'FA_DIA_HOR' ).AsString;
                          dValue := FieldByName( 'FA_FEC_INI' ).AsDateTime;
                          rValue := FieldByName( 'MO_MONTO' ).AsFloat;
                          eConcepto := eTipoConcepto( FieldByName( 'CO_TIPO' ).AsInteger );
                          eExcepcion := eTipoExcepcion( FieldByName( 'MO_TIPO' ).AsInteger );
                          with oZetaProvider do
                          begin
                               EmpiezaTransaccion;
                               try
                                  {OP: 12/06/08}
                                  if( Not lNoIncapacitados ) or ( Not EstaIncapacitado ) then
                                  begin
                                       VerificaRegistro( iEmpleado, ( eExcepcion = teMonto ) );
                                       if ( eExcepcion = teMonto ) then
                                       begin
                                            if ValidaDerechosLimiteConcepto then
                                            begin
                                                 ExcepcionMonto( iEmpleado, iConcepto, sReferencia, rValue, eOperacion, not ( eConcepto in [ coDeduccion, coObligacion ] ) );
                                            end
                                       end
                                       else
                                       begin
                                            ExcepcionDiaHora( iEmpleado, dValue, sDiaHora, iMotivo, rValue, eOperacion );
                                            lCalculaDias:= TRUE;
                                       end;
                                  end
                                  else
                                       Log.Error( iEmpleado, 'Empleado Incapacitado, no se import� el monto' ,'El empleado #' + IntToStr( iEmpleado ) + ' no se import� por estar incapacitado' );

                               except
                                   on Error: Exception do
                                   begin
                                        Log.Excepcion( iEmpleado, 'Error Al Importar Movimiento Concepto ' + IntToStr( iConcepto ), Error );
                                   end;
                               end;

                               Next;

                               if lCalculaDias and ( EOF or ( FEmpleado <> FieldByName( 'CB_CODIGO' ).AsInteger ) ) then
                               begin
                                    try
                                       PreparaDiasHoras( FEmpleado );  // Calcula D�as / Horas para el Empleado Anterior
                                       FEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                                       lCalculaDias:= FALSE;
                                    except
                                       on Error: Exception do
                                       begin
                                            Log.Excepcion( iEmpleado, 'Error Al Calcular Dias / Horas ', Error );
                                       end;
                                    end;
                               end;
                               TerminaTransaccion( TRUE );
                          end;
                     end;
                end;
                ExcepcionMontoEnd;
                ExcepcionDiaHoraEnd;
             finally
                VerificaNominaEnd;
             end;
          finally
             PreparaDiasHorasEnd;
          end;
     end;
end;

procedure ImportaAcumulados;
var
   iYear, iMes : Integer;
   FExiste, FAgrega, FModifica, FModificaAnual: TZetaCursor;
   sMes: String;
   lExiste: Boolean;
   rAnterior: TPesos;
begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               iYear := ParamByName( 'YearAcum' ).AsInteger;
               iMes := ParamByName( 'MesAcum' ).AsInteger;
          end;
          sMes := GetMesAcumulado( iMes );
          //CV: CreateQuery--ok
          //Variable local
          FExiste := CreateQuery( Format( GetSQLScript( Q_ACUMULADO_EXISTE ), [ sMes ] ) );
          FAgrega := CreateQuery( Format( GetSQLScript( Q_ACUMULADO_AGREGA ), [ sMes ] ) );
          FModifica := CreateQuery( Format( GetSQLScript( Q_ACUMULADO_MODIFICA ), [ sMes ] ) );
          FModificaAnual := CreateQuery( GetSQLScript( Q_ACUMULADO_MODIFICA_ANUAL ) );
          ParamAsInteger( FExiste, 'AC_YEAR', iYear );
          ParamAsInteger( FAgrega, 'AC_YEAR', iYear );
          ParamAsInteger( FModifica, 'AC_YEAR', iYear );
          ParamAsInteger( FModificaAnual, 'AC_YEAR', iYear );
          with cdsLista do
          begin
               while not EOF and oZetaProvider.CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
               begin
                    iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                    iConcepto := FieldByName( 'CO_NUMERO' ).AsInteger;
                    rValue := FieldByName( 'MO_MONTO' ).AsFloat;
                    EmpiezaTransaccion;
                    try
                       ParamAsInteger( FExiste, 'CB_CODIGO', iEmpleado );
                       ParamAsInteger( FExiste, 'CO_NUMERO', iConcepto );
                       with FExiste do
                       begin
                            Active := True;
                            if IsEmpty then
                            begin
                                 lExiste := False;
                                 rAnterior := 0;
                            end
                            else
                            begin
                                 lExiste := True;
                                 rAnterior := FieldByName( sMes ).AsFloat;
                            end;
                            Active := False;
                       end;
                       if lExiste  then
                       begin
                            case eOperacion of
                                 omSumar: rValue := rAnterior + rValue;
                                 omRestar: rValue := rAnterior - rValue;
                            end;
                            // Actualiza Acumulado del Mes
                            ParamAsInteger( FModifica, 'CB_CODIGO', iEmpleado );
                            ParamAsInteger( FModifica, 'CO_NUMERO', iConcepto );
                            ParamAsFloat( FModifica, 'AC_MES', rValue );
                            Ejecuta( FModifica );
                            // Actualiza Acumulado Anual
                            ParamAsInteger( FModificaAnual, 'CB_CODIGO', iEmpleado );
                            ParamAsInteger( FModificaAnual, 'CO_NUMERO', iConcepto );
                            Ejecuta( FModificaAnual );
                       end
                       else
                       begin
                            ParamAsInteger( FAgrega, 'CB_CODIGO', iEmpleado );
                            ParamAsInteger( FAgrega, 'CO_NUMERO', iConcepto );
                            ParamAsFloat( FAgrega, 'AC_MES', rValue );
                            ParamAsFloat( FAgrega, 'AC_ANUAL', rValue );
                            Ejecuta( FAgrega );
                       end;
                       TerminaTransaccion( True );
                    except
                          on Error: Exception do
                          begin
                               TerminaTransaccion( False );
                               Log.Excepcion( iEmpleado, 'Error Al Importar Acumulado Concepto ' + IntToStr( iConcepto ), Error );
                          end;
                    end;
                    Next;
               end;
          end;
     end;
     FreeAndNil(FExiste);
     FreeAndNil(FAgrega);
     FreeAndNil(FModifica);
     FreeAndNil(FModificaAnual);
end;

begin
     InitLog(Empresa,'ImportaAcumulados');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
          AsignaParamList( Parametros );
          GetDatosPeriodo;
          InitArregloTPeriodo;
          with ParamList do
          begin
               eImportar := eImportacion( ParamByName( 'Importacion' ).AsInteger );
               eOperacion := eOperacionMontos( ParamByName( 'Operacion' ).AsInteger );
               lNoIncapacitados := ParamByName( 'NoIncapacitados' ).AsBoolean;
          end;
          ImportarMovimientosParametros;
          with cdsLista do
          begin
               Lista := Datos;
               if OpenProcess( prNOImportarMov, RecordCount, FListaParametros ) then
               begin
                    InitNomina;
                    try
                       try
                          if ( eImportar = eiExcepciones ) then
                             ImportaExcepciones
                          else
                             ImportaAcumulados;
                       except
                             on Error: Exception do
                             begin
                                  Log.Excepcion( 0, 'Error Al Importar Movimientos', Error );
                             end;
                       end;
                       Nomina.CalculaStatusPeriodo;
                    finally
                           ClearNomina;
                    end;
               end;
               Result := CloseProcess;
          end;
     end;
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.ImportarMovimientosGetASCII(Empresa, Parametros, ListaASCII: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     InitLog(Empresa,'ImportarMovimientosGetASCII');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          InitArregloTPeriodo;
          GetDatosPeriodo;
          FImportar := eImportacion( ParamList.ParamByName( 'Importacion' ).AsInteger );
          //CV: CreateQuery--ok
          //Querys globales que se utilizan en  IMportarMovimientosValidaASCII
          //el cual solo se manda llamar aqui
          {$ifndef CAMBIO_TNOM}
          FEmpleadoLee := CreateQuery( Format( GetSQLScript( Q_EMPLEADO_STATUS ), [ ZetaCommonTools.DateToStrSQLC( DatosPeriodo.Inicio ),
                                                                                    Nivel0( Empresa ) ] ) );
          {$endif}
          FConceptoTipo := CreateQuery( GetSQLScript( Q_CONCEPTO_TIPO ) );
          oSuperASCII := TdmSuperASCII.Create( Self );
          try
             with oSuperASCII do
             begin
                  OnValidar := ImportarMovimientosValidaASCII;
                  Formato := eFormatoASCII( ParamList.ParamByName( 'Formato' ).AsInteger );
                  AgregaColumna( 'CB_CODIGO', tgNumero, 9, True );
                  AgregaColumna( 'CO_NUMERO', tgNumero, 5, True );
                  AgregaColumna( 'MO_MONTO', tgFloat, 18, True );
                  AgregaColumna( 'MO_REFEREN', tgTexto, 10, False );
                  { Estas columnas no se importan }
                  { Se usan para guardar Resultados de la validacion }
                  AgregaColumna( 'CO_TIPO', tgNumero, 1, False );
                  AgregaColumna( 'MO_TIPO', tgNumero, 1, False );
                  AgregaColumna( 'FA_FEC_INI', tgFecha, 10, False );
                  AgregaColumna( 'FA_DIA_HOR', tgTexto, 1, False );
                  AgregaColumna( 'FA_MOTIVO', tgNumero, 1, False );
                  Result := Procesa( ListaASCII );
                  ErrorCount := Errores;
             end;
          finally
                 oSuperASCII.Free;
                 FreeAndNil(FEmpleadoLee);
                 FreeAndNil(FConceptoTipo);
          end;
     end;
     EndLog;SetComplete;
end;

procedure TdmServerNominaTimbrado.ImportarMovimientosValidaASCII(DataSet: TDataset; var nProblemas: Integer; var sErrorMsg: String );
var
   iEmpleado: TNumEmp;
   iConcepto, iMotivo: Integer;
   sReferencia, sDiaHora: String;
   rMonto: TPesos;
   eConcepto: eTipoConcepto;
   eExcepcion: eTipoExcepcion;
   eMotivoDias: eMotivoFaltaDias;
   eMotivoHoras: eMotivoFaltaHoras;
   dReferencia, dFinal: TDate;
   lOk: Boolean;
   TipoPer: eTipoPeriodo;
   sMsgError : String;

procedure Error( const sMensaje: String );
begin
     Inc( nProblemas );
     sErrorMsg := sErrorMsg + '| ' + sMensaje + ' |';
end;

function GetTipoExcepcion( const iConcepto: Integer ): eTipoExcepcion;
begin
     if ( iConcepto <= 999 ) then
        Result := teMonto
     else
         if ( iConcepto <= 1099 ) then
             Result := teTotales
         else
             if ( iConcepto <= 1199 ) then
                Result := teDias
             else
                 Result := teHoras;
end;

function GetEquivaleDias( const iConcepto: Integer; var lOk: Boolean): eMotivoFaltaDias;
begin
     lOk := True;
     case iConcepto of
          1100:	Result := mfdInjustificada;
          1101:	Result := mfdJustificada;
          1102:	Result := mfdConGoce;
          1103:	Result := mfdSinGoce;
          1104:	Result := mfdSuspension;
          1105:	Result := mfdOtrosPermisos;
          1106:	Result := mfdIncapacidad;
          {
          1107:
          }
          1108:	Result := mfdRetardo;
          {
          1109:
          1110:
          }
          1111:	Result := mfdAsistencia;
          1112:	Result := mfdNoTrabajados;
          {
          1113:
          }
          1114:	Result := mfdVacaciones;
          1115:	Result := mfdAguinaldo;
          1116:	Result := mfdAjuste;
          1117:	Result := mfdIMSS;
          1118:	Result := mfdEM;
          1121: Result := mfdPrimaVacacional;
     else
         lOk := False;
         Result := mfdInjustificada;
     end;
end;

function GetEquivaleHoras( const iConcepto: Integer; var lOk: Boolean): eMotivoFaltaHoras;
begin
     lOk := True;
     case iConcepto of
          {
          1200:
          }
          1201: Result := mfhOrdinarias;
          1202:	Result := mfhExtras;
          1203:	Result := mfhDobles;
          1204:	Result := mfhTriples;
          1205:	Result := mfhAdicionales;
          1206:	Result := mfhRetardo;
          1207:	Result := mfhDomingo;
          1208:	Result := mfhConGoce;
          1209:	Result := mfhSinGoce;
          1210:	Result := mfhFestivo;
          1211:	Result := mfhDescanso;
          1212:	Result := mfhVacaciones;
          {
          1213:
          }
     else
         lOk := False;
         Result := mfhOrdinarias;
     end;
end;

function GetImportedDate( const sData: String ): TDate;
begin
     if ( Pos( '/', sData ) > 0 ) then  { Formato DD/MM/YY }
        Result := ZetaCommonTools.StrAsFecha( sData )
     else                               { Formato DDMMYYYY }
         Result := ZetaCommonTools.CodificaFecha( StrAsInteger( Copy( sData, 5, 4 ) ),
                                                  StrAsInteger( Copy( sData, 3, 2 ) ),
                                                  StrAsInteger( Copy( sData, 1, 2 ) ) );
end;

procedure ValidaFechaEnPeriodo;
begin
     with oZetaProvider.DatosPeriodo do
     begin
          {$ifdef CAMBIO_TNOM}
          if ( dReferencia < FNominaEmpleado.FecNominaInicio ) or ( dReferencia > FNominaEmpleado.FecNominaFin ) then
          {$ifdef QUINCENALES}
          if ( dReferencia < InicioAsis ) or ( dReferencia > FinAsis ) then
          {$else}
          if ( dReferencia < Inicio ) or ( dReferencia > Fin ) then
          {$endif}
          {$endif}
             Error( 'Fecha Fuera Del Per�odo De N�mina' );
     end;
end;

function TipoNominaDiferente: Boolean;
var
   oDatosPeriodo: TDatosPeriodo;

   procedure ValidaFechaCambioTNom;
   begin
        with FNominaEmpleado do
        begin
             if ( FecCambioTNomina > oDatosPeriodo.Fin ) then
             begin
                  FecCambioTNomina:= GetFecCambioTNom( oZetaProvider.EmpresaActiva, Empleado, oDatosPeriodo.Fin );
             end;
        end;
   end;

   procedure SetValoresNomina;
   begin
        { Se inicializan los valores del empleado respecto a su posible cambio de tipo de n�mina }
        with FNominaEmpleado do
        begin
             Empleado:= iEmpleado;
             FecCambioTNomina:= FEmpleadoLee.FieldByName( 'CB_FEC_NOM' ).AsDateTime;

             ValidaFechaCambioTNom;

             EsCambioEnPeriodo:= ( EsCambioTNomPeriodo( FecCambioTNomina, oDatosPeriodo ) );
             TNominaFin:= TipoPer;
             TNominaInicio:= TipoPer;
             FecNominaInicio:= oDatosPeriodo.InicioAsis;
             FecNominaFin:= oDatosPeriodo.FinAsis;

             if ( EsCambioEnPeriodo ) then
             begin
                  with oZetaProvider do
                  begin
                       FEmpleadoLee := CreateQuery( Format( GetSQLScript( Q_EMPLEADO_STATUS ), [ ZetaCommonTools.DateToStrSQLC( DatosPeriodo.Fin ),

                                                                                                  Nivel0( EmpresaActiva ) ] ) );
                       FEmpleadoLee.Active:= False;
                       ParamAsInteger( FEmpleadoLee, 'Empleado', iEmpleado );
                       FEmpleadoLee.Active:= True;
                       TNominaFin:= eTipoPeriodo( FEmpleadoLee.FieldByName('TU_NOMINA').AsInteger );
                  end;

                  if ( TNominaInicio <> TNominaFin ) then
                  begin
                       { En caso que se este calculando la n�mina a la que cambio }
                       if ( TNominaInicio = oDatosPeriodo.Tipo ) then
                       begin
                            FecNominaFin:= FecCambioTNomina - 1;
                       end
                       else
                           { En caso que NO se este calculando la n�mina a la que cambio }
                           FecNominaInicio:= FecCambioTNomina;
                  end;
             end;
        end;
   end;

begin
     with DataSet do
     begin
          //eTipo := dmCatalogos.GetTipoNomina( FieldByName( 'CB_TURNO' ).AsString );
          oDatosPeriodo:= oZetaProvider.DatosPeriodo;
          Result := ( TipoPer <> oDatosPeriodo.Tipo );
          SetValoresNomina;

          if Result then
          begin
               with FNominaEmpleado do
               begin
                    if ( EsCambioEnPeriodo ) then
                       Result:= ( TNominaFin <> oDatosPeriodo.Tipo );
               end;
          end;

     end;
end;

begin
     with DataSet do
     begin
          iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
          iConcepto := FieldByName( 'CO_NUMERO' ).AsInteger;
          rMonto := FieldByName( 'MO_MONTO' ).AsFloat;
          sReferencia := FieldByName( 'MO_REFEREN' ).AsString;
          iMotivo := 0;
          sDiaHora := '';
          dReferencia := NullDateTime;
          eConcepto := coCalculo;
          eExcepcion := teMonto;

          if ( iEmpleado = 0 ) then
             Error( 'N�mero de Empleado Es Cero' )
          else
          begin
               with oZetaProvider do
               begin
                    InitGlobales;
                    {$ifdef CAMBIO_TNOM}
                    FEmpleadoLee := CreateQuery( Format( GetSQLScript( Q_EMPLEADO_STATUS ), [ ZetaCommonTools.DateToStrSQLC( DatosPeriodo.Inicio ),
                                                                                              Nivel0( EmpresaActiva ) ] ) );
                    {$endif}
                    with FEmpleadoLee do
                    begin
                         Active:= False;
                         ParamAsInteger( FEmpleadoLee, 'Empleado', iEmpleado );
                         //ParamAsDateTime( FEmpleadoLee, 'Fecha', DatosPeriodo.Inicio );
                         Active := True;
                         if IsEmpty then
                            Error( 'Empleado ' + IntToStr( iEmpleado ) + ' No Existe' )
                         else if ( FImportar = eiExcepciones ) then
                         begin
                              TipoPer := eTipoPeriodo( FieldByName( 'TU_NOMINA' ).AsInteger );
                              {$ifdef CAMBIO_TNOM}
                              if ( TipoNominaDiferente ) then
                              {$else}
                              if ( TipoPer <> DatosPeriodo.Tipo ) then
                              {$endif}
                                 Error( 'El Empleado ' + IntToStr( iEmpleado ) + ' pertenece a una N�mina ' + ObtieneElemento( lfTipoPeriodo, Ord( TipoPer ) ) )
                              else
                              begin
                                   if ZetaCommonTools.EsBajaNominaAnterior( zStrToBool( FieldByName( 'CB_ACTIVO' ).AsString ),
                                                                            FieldByName( 'CB_NOMYEAR' ).AsInteger,
                                                                            FieldByName( 'CB_NOMTIPO' ).AsInteger,
                                                                            FieldByName( 'CB_NOMNUME' ).AsInteger,
                                                                            GetGlobalInteger( K_GLOBAL_LIMITE_NOMINAS_ORDINARIAS ),
                                                                            FieldByName( 'CB_FEC_BAJ' ).AsDateTime,
                                                                            DatosPeriodo, FALSE, sMsgError ) then
                                      Error( sMsgError );
                              end;

                         end;
                         Active := False;
                    end;
                    ParamAsInteger( FConceptoTipo, 'Concepto', iConcepto );
                    with FConceptoTipo do
                    begin
                         Active := True;
                         if IsEmpty then
                            Error( 'Concepto ' + IntToStr( iConcepto ) + ' No Existe' )
                         else
                             eConcepto := eTipoConcepto( FieldByName( 'CO_TIPO' ).AsInteger );
                         Active := False;
                    end;
                    if ( FImportar = eiExcepciones ) then //Excepciones de N�mina //
                    begin
                         eExcepcion := GetTipoExcepcion( iConcepto );
                         case eExcepcion of
                              teMonto:
                              begin
                                   if ( eConcepto = coCalculo ) then
                                      Error( 'Concepto ' + IntToStr( iConcepto ) + ' Es De Tipo ' + ZetaCommonLists.ObtieneElemento( lfTipoConcepto, Ord( eConcepto ) ) );
                                   //ValidaDerechosLimiteConcepto;
                              end;
                              teDias:
                              begin
                                   eMotivoDias := GetEquivaleDias( iConcepto, lOk );
                                   if lOk then
                                   begin
                                        iMotivo := Ord( eMotivoDias );
                                        sDiaHora := K_TIPO_DIA;
                                        if ZetaCommonTools.StrVacio( sReferencia ) then
                                        begin
                                             if ( eMotivoDias in [ mfdAjuste, mfdInjustificada, mfdJustificada, mfdSinGoce, mfdConGoce, mfdSuspension, mfdIncapacidad ] ) then
                                                Error( 'Fecha No Puede Quedar Vac�a' )
                                             else
{$ifdef CAMBIO_TNOM}
                                                 dReferencia := FNominaEmpleado.FecNominaInicio;
{$else}

{$ifdef QUINCENALES}
                                                 dReferencia := DatosPeriodo.InicioAsis;  // No viene una fecha en el archivo ASCII, damos default = FechaIni //
{$else}
                                                 dReferencia := DatosPeriodo.Inicio;  // No viene una fecha en el archivo ASCII, damos default = FechaIni //
{$endif}
{$endif}
                                        end
                                        else
                                        begin
                                             dReferencia := GetImportedDate( sReferencia );
                                             if ( dReferencia = NullDateTime ) then
                                                Error( 'Fecha Inv�lida: ' + sReferencia );
                                        end;
                                        { GA (17/Sep/03: Calcular Final de Excepcion }

                                        dFinal := ( dReferencia + rMonto - 1 );
                                        if ( eMotivoDias = mfdAjuste ) then
                                        begin
                                             with DatosPeriodo do
                                             begin
{$ifdef CAMBIO_TNOM}
                                                  if ( dReferencia >= FNominaEmpleado.FecNominaInicio ) or ( dFinal >= FNominaEmpleado.FecNominaInicio ) then { GA 17/Sep/03: Agregar Validacion del Final de la Excepcion (igual que en DNomina.pas - TdmNomina.ValidaFechaDiasHoras ) }
                                                     Error( Format( 'Ajuste ( %s al %s ) Posterior Al Inicio De la N�mina ( %s )', [ FechaCorta( dReferencia ), FechaCorta( FNominaEmpleado.FecNominaFin ), FechaCorta( FNominaEmpleado.FecNominaInicio ) ] ) );
{$else}
{$ifdef QUINCENALES}
                                                  if ( dReferencia >= InicioAsis ) or ( dFinal >= InicioAsis ) then { GA 17/Sep/03: Agregar Validacion del Final de la Excepcion (igual que en DNomina.pas - TdmNomina.ValidaFechaDiasHoras ) }
                                                     Error( Format( 'Ajuste ( %s al %s ) Posterior Al Inicio Del Per�odo ( %s )', [ FechaCorta( dReferencia ), FechaCorta( dFinal ), FechaCorta( InicioAsis ) ] ) );
{$else}
                                                  if ( dReferencia >= Inicio ) or ( dFinal >= Inicio ) then { GA 17/Sep/03: Agregar Validacion del Final de la Excepcion (igual que en DNomina.pas - TdmNomina.ValidaFechaDiasHoras ) }
                                                     Error( Format( 'Ajuste ( %s al %s ) Posterior Al Inicio Del Per�odo ( %s )', [ FechaCorta( dReferencia ), FechaCorta( dFinal ), FechaCorta( Inicio ) ] ) );
{$endif}
{$endif}
                                             end;
                                        end
                                        else
                                            ValidaFechaEnPeriodo;
                                            if ( eMotivoDias in [ mfdInjustificada, mfdJustificada, mfdSinGoce, mfdConGoce, mfdSuspension, mfdIncapacidad ] ) then
                                            begin
                                                 with DatosPeriodo do
                                                 begin
{$ifdef CAMBIO_TOM}
                                                      if ( dReferencia < FNominaEmpleado.FecNominaInicio ) or ( dFinal > FNominaEmpleado.FecNominaFin ) then { GA 17/Sep/03: Usar dFinal en lugar de ( dReferencia + rMonto - 1 ) }
{$else}
{$ifdef QUINCENALES}
                                                      if ( dReferencia < InicioAsis ) or ( dFinal > FinAsis ) then { GA 17/Sep/03: Usar dFinal en lugar de ( dReferencia + rMonto - 1 ) }
{$else}
                                                      if ( dReferencia < Inicio ) or ( dFinal > Fin ) then { GA 17/Sep/03: Usar dFinal en lugar de ( dReferencia + rMonto - 1 ) }
{$endif}
{$endif}
                                                         Error( 'Fecha ( ' + FechaCorta( dReferencia ) + ' ) Fuera Del Per�odo De N�mina' );
                                                 end;
                                            end;
                                   end
                                   else
                                       Error( 'Concepto ' + IntToStr( iConcepto ) + ' No Es De DIAS' );
                              end;
                              teHoras:
                              begin
                                   eMotivoHoras := GetEquivaleHoras( iConcepto, lOk );
                                   if lOk then
                                   begin
                                        iMotivo := Ord( eMotivoHoras );
                                        sDiaHora := K_TIPO_HORA;
//                                        dReferencia := DatosPeriodo.Inicio;  // No viene una fecha en el archivo ASCII, damos default = FechaIni //
                                        if ZetaCommonTools.StrVacio( sReferencia ) then
{$ifdef CAMBIO_TNOM}
                                           dReferencia := FNominaEmpleado.FecNominaInicio  // No viene una fecha en el archivo ASCII, damos default = FechaIni //
{$else}
{$ifdef QUINCENALES}
                                           dReferencia := DatosPeriodo.InicioAsis  // No viene una fecha en el archivo ASCII, damos default = FechaIni //
{$else}
                                           dReferencia := DatosPeriodo.Inicio  // No viene una fecha en el archivo ASCII, damos default = FechaIni //
{$endif}
{$endif}
                                        else
                                        begin
                                             dReferencia := GetImportedDate( sReferencia );
                                             if ( dReferencia = NullDateTime ) then
                                                Error( 'Fecha Inv�lida: ' + sReferencia );
                                        end;
                                        ValidaFechaEnPeriodo;
                                   end
                                   else
                                       Error( 'Concepto ' + IntToStr( iConcepto ) + ' Es De HORAS' );
                              end;
                              teTotales: Error( 'No Se Permite Importar Totales A Una N�mina' );
                         end;
                    end;
               end;
          end;
          if ( nProblemas = 0 ) then
          begin
               FieldByName( 'CO_TIPO' ).AsInteger := Ord( eConcepto );
               FieldByName( 'MO_TIPO' ).AsInteger := Ord( eExcepcion );
               FieldByName( 'FA_FEC_INI' ).AsDateTime := dReferencia;
               FieldByName( 'FA_DIA_HOR' ).AsString := sDiaHora;
               FieldByName( 'FA_MOTIVO' ).AsInteger := iMotivo;
          end;
     end;
end;

{ **** Exportar Movimientos ******* }

procedure TdmServerNominaTimbrado.ExportarMovimientosParametros;
begin
     with oZetaProvider, ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Periodo: ' + ZetacommonTools.ShowNomina( DatosPeriodo.Year, Ord( DAtosPeriodo.Tipo ), DAtosPeriodo.Numero ) +
                         K_PIPE + 'Archivo: ' + ParamByName( 'Archivo' ).AsString +
                         K_PIPE + 'D�as/Horas: ' + ZetaCommonTools.zBoolToStr( ParamByName( 'DiasHoras' ).AsBoolean ) +
                         K_PIPE + 'Tipo de Monto: ' + ObtieneElemento( lfHorasDias, ParamByName( 'DiasHoras' ).ASinteger ) +
                         K_PIPE + 'Formato: ' + ObtieneElemento( lfFormatoASCII, ParamByName( 'Formato' ).ASinteger );
     end;
end;

function TdmServerNominaTimbrado.ExportarMovimientos(Empresa, Parametros: OleVariant; out Datos: OleVariant): OleVariant;
var
   lDiasHoras: Boolean;
   eMontos: eHorasDias;
   rValor: Currency;
   iConcepto: Integer;
   sReferencia: String;
   FDataset: TZetaCursor;

function GetConceptoDias( eTipo: eMotivoFaltaDias ): Integer;
begin
     case eTipo of
          mfdInjustificada: Result := 1100;
          mfdJustificada: Result := 1101;
          mfdConGoce: Result := 1102;
          mfdSinGoce: Result := 1103;
          mfdSuspension: Result := 1104;
          mfdOtrosPermisos: Result := 1105;
          mfdIncapacidad: Result := 1106;
          {
          1107:
          }
          mfdRetardo: Result := 1108;
          {
          1109:
          1110:
          }
          mfdAsistencia: Result := 1111;
          mfdNoTrabajados: Result := 1112;
          {
          1113:
          }
          mfdVacaciones: Result := 1114;
          mfdAguinaldo: Result := 1115;
          mfdAjuste: Result := 1116;
          mfdIMSS: Result := 1117;
          mfdEM: Result := 1118;
          mfdPrimaVacacional : Result := 1121;
     else
         Result := 0;
     end;
end;

function GetConceptoHoras( const eTipo: eMotivoFaltaHoras ): Integer;
begin
     case eTipo of
          mfhOrdinarias: Result := 1201;
          mfhExtras: Result := 1202;
          mfhDobles: Result := 1203;
          mfhTriples: Result := 1204;
          mfhAdicionales: Result := 1205;
          mfhRetardo: Result := 1206;
          mfhDomingo: Result := 1207;
          mfhConGoce: Result := 1208;
          mfhSinGoce: Result := 1209;
          mfhFestivo: Result := 1210;
          mfhDescanso: Result := 1211;
          mfhVacaciones: Result := 1212;
     else
         Result := 0;
     end;
end;

function FormateaFecha( const dValue: TDate ): String;
var
   wDia, wMes, wYear: Word;
begin
     if ( dValue = NullDateTime ) then
        Result := ''
     else
     begin
          DecodeDate( dValue, wYear, wMes, wDia );
          Result := ZetaCommonTools.FormateaNumero( wDia, 2, '0' ) +
                    '/' +
                    ZetaCommonTools.FormateaNumero( wMes, 2, '0' ) +
                    '/' +
                    IntToStr( wYear );
     end;
end;

procedure Agregar( const iEmpleado: TNumEmp; const iConcepto: Integer; const rMonto: Currency; const sReferencia: String );
begin
     with cdsLista do
     begin
          Append;
          FieldByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
          FieldByName( 'CO_NUMERO' ).AsInteger := iConcepto;
          FieldByName( 'MO_MONTO' ).AsFloat := rMonto;
          FieldByName( 'MO_REFEREN' ).AsString := sReferencia;
          Post;
     end;
end;

begin
     InitLog(Empresa,'Agregar');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          GetDatosPeriodo;
          InitArregloTPeriodo;
          with ParamList do
          begin
               lDiasHoras := ParamByName( 'DiasHoras' ).AsBoolean;
               eMontos := eHorasDias( ParamByName( 'TipoMonto' ).AsInteger );
          end;
          ExportarMovimientosParametros;
          if OpenProcess( prNOExportarMov, 0, FListaParametros ) then
          begin
               try
                  with cdsLista do
                  begin
                       InitTempDataset;
                       AddIntegerField( 'CB_CODIGO' );
                       AddIntegerField( 'CO_NUMERO' );
                       AddFloatField( 'MO_MONTO' );
                       AddStringField( 'MO_REFEREN', 10 );
                       IndexFieldNames := 'CB_CODIGO;CO_NUMERO';
                       CreateTempDataset;
                  end;
                  if lDiasHoras then
                  begin
                       //CV: CreateQuery--ok
                       //Variable local
                       FDataset := CreateQuery( Format( GetSQLScript( Q_EXPORTAR_FALTAS ), [ Nivel0( Empresa ) ] ) );
                       with DatosPeriodo do
                       begin
                            ParamAsInteger( FDataset, 'Year', Year );
                            ParamAsInteger( FDataset, 'Tipo', Ord( Tipo ) );
                            ParamAsInteger( FDataset, 'Numero', Numero );
                       end;
                       with FDataset do
                       begin
                            Active := True;
                            while not Eof do
                            begin
                                 if ( FieldByName( 'FA_DIA_HOR' ).AsString = K_TIPO_DIA ) then
                                 begin
                                      iConcepto := GetConceptoDias( eMotivoFaltaDias( FieldByName( 'FA_MOTIVO' ).AsInteger ) );
                                      rValor := FieldByName( 'FA_DIAS' ).AsFloat;
                                 end
                                 else
                                 begin
                                      iConcepto := GetConceptoHoras( eMotivoFaltaHoras( FieldByName( 'FA_MOTIVO' ).AsInteger ) );
                                      rValor := FieldByName( 'FA_HORAS' ).AsFloat;
                                 end;
                                 Agregar( FieldByName( 'CB_CODIGO' ).AsInteger,
                                          iConcepto,
                                          rValor,
                                          FormateaFecha( FieldByName( 'FA_FEC_INI' ).AsDateTime ) );
                                 Next;
                            end;
                            Active := False;
                       end;
                  end;
                  if ( eMontos in [ hdTodos, hdExcepcion ] ) then
                  begin
                       case eMontos of
                            hdTodos: FDataset := CreateQuery( Format( GetSQLScript( Q_EXPORTAR_MOVIMIENTOS_TODOS ), [ Nivel0( Empresa ) ] ) );
                            hdExcepcion: FDataset := CreateQuery( Format( GetSQLScript( Q_EXPORTAR_MOVIMIENTOS_EXCEP ), [ Nivel0( Empresa ) ] ) );
                       else
                           FDataset := nil;
                       end;
                       with DatosPeriodo do
                       begin
                            ParamAsInteger( FDataset, 'Year', Year );
                            ParamAsInteger( FDataset, 'Tipo', Ord( Tipo ) );
                            ParamAsInteger( FDataset, 'Numero', Numero );
                       end;
                       with FDataset do
                       begin
                            Active := True;
                            while not Eof do
                            begin
                                 iConcepto := FieldByName( 'CO_NUMERO' ).AsInteger;
                                 if ( iConcepto <= 999 ) then
                                    sReferencia := FieldByName( 'MO_REFEREN' ).AsString
                                 else
                                     sReferencia := FormateaFecha( Date ); // Fecha de Hoy //
                                 Agregar( FieldByName( 'CB_CODIGO' ).AsInteger,
                                          iConcepto,
                                          FieldByName( 'MO_PERCEPC' ).AsFloat + FieldByName( 'MO_DEDUCCI' ).AsFloat,
                                          sReferencia );
                                 Next;
                            end;
                            Active := False;
                       end;
                  end;
                  Datos := cdsLista.Data;
               except
                     on Error: Exception do
                     begin
                          Log.Excepcion( 0, 'Error Al Exportar Movimientos', Error );
                     end;
               end;
          end;
          Result := CloseProcess;
          FreeAndNil(FDataSet);
     end;
     EndLog;SetComplete;
end;

{ WizNomImportarPagoRecibos }

procedure TdmServerNominaTimbrado.ImportarPagoRecibosListaParametros;
const
     aPagados: array[ FALSE..TRUE ] of Pchar = ( 'No Pagados', 'Pagados' );
begin
     with oZetaProvider, DatosPeriodo do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Periodo: ' + ZetacommonTools.ShowNomina( Year, Ord( Tipo ), Numero ) +
                           K_PIPE + 'D�gito Empresa: ' + ParamList.ParamByname( 'DigitoEmpresa' ).AsString +
                           K_PIPE + 'Archivo: ' + ParamList.ParamByName( 'Archivo' ).AsString +
                           K_PIPE + 'Formato: ' + ObtieneElemento( lfFormatoASCII, ParamList.ParamByName( 'Formato' ).AsInteger ) +
                           K_PIPE + 'Marcar Como: ' + aPagados[ ParamList.ParamByname( 'Pagado' ).AsBoolean ];
          if ParamList.ParamByname( 'Pagado' ).AsBoolean then
              FListaParametros := FListaParametros + K_PIPE + 'Fecha: ' + FechaCorta( ParamList.ParamByname( 'Fecha' ).AsDatetime );
     end;
end;

function TdmServerNominaTimbrado.ImportarPagoRecibosLista(Empresa, Lista, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'ImportarPagoRecibosLista');
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          GetDatosPeriodo;
          InitArregloTPeriodo;
     end;
     ImportarPagoRecibosListaParametros;
     cdsLista.Lista := Lista;
     Result := ImportarPagoRecibosDataset( cdsLista );
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.ImportarPagoRecibosGetLista(Empresa, Parametros, Lista: OleVariant): OleVariant;
begin
     InitLog(Empresa,'ImportarPagoRecibosGetLista');
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          GetDatosPeriodo;
     end;
     cdsAuxiliar.Lista := Lista;
     ImportarPagoRecibosCrearDataSet;
     ImportarPagoRecibosBuildDataSet;
     Result := cdsLista.Data;
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.ImportarPagoRecibosDataset(Dataset: TDataset): OleVariant;
var
   iEmpleado: Integer;
   dFecha: TDate;
   lPagado: Boolean;
   FInsertPago: TZetaCursor;

   procedure AsignaParametrosPago;
   begin
        with oZetaProvider do
        begin
             ParamAsInteger( FInsertPago, 'Empleado', iEmpleado );
             ParamAsDate( FInsertPago, 'Fecha', dFecha );
             with Dataset do
             begin
                  ParamAsInteger( FInsertPago, 'Year', FieldByName( 'PE_YEAR' ).AsInteger );
                  ParamAsInteger( FInsertPago, 'Tipo', FieldByName( 'PE_TIPO' ).AsInteger );
                  ParamAsInteger( FInsertPago, 'Numero', FieldByName( 'PE_NUMERO' ).AsInteger );
                  ParamAsInteger( FInsertPago, 'Usuario', FieldByName( 'NO_USR_PAG' ).AsInteger );
             end;
        end;
   end;

begin
     with oZetaProvider do
     begin
          if OpenProcess( prNOPagoRecibos, Dataset.RecordCount, FListaParametros ) then
          begin
               lPagado := ParamList.ParamByName( 'Pagado' ).AsBoolean;
               try
                  //CV: CreateQuery--ok
                  //Variable local
                  FInsertPago := CreateQuery( GetScript( fnInsertaPago ) );
                  with Dataset do
                  begin
                       First;
                       while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                       begin
                            iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                            dFecha := FieldByName( 'NO_FEC_PAG' ).AsDateTime;
                            EmpiezaTransaccion;
                            try
                               AsignaParametrosPago;
                               Ejecuta( FInsertPago );
                               TerminaTransaccion( True );
                            except
                                  on Error: Exception do
                                  begin
                                       TerminaTransaccion( False );
                                       if lPagado then
                                          Log.Excepcion( iEmpleado, 'Error Al Registrar Recibo Pagado ' + FechaCorta( dFecha ), Error )
                                       else
                                          Log.Excepcion( iEmpleado, 'Error Al Registrar Recibo No Pagado ', Error );
                                  end;
                            end;
                            Next;
                       end;
                  end;
               except
                     on Error: Exception do
                     begin
                          if lPagado then
                             Log.Excepcion( 0, 'Error Al Registrar Recibos Pagados', Error )
                          else
                             Log.Excepcion( 0, 'Error Al Registrar Recibos No Pagados', Error );
                     end;
               end;
          end;
          Result := CloseProcess;
          FreeAndNil(FInsertPago);
     end;
end;

function TdmServerNominaTimbrado.ImportarPagoRecibosGetASCII(Empresa, Parametros, ListaASCII: OleVariant; var ErrorCount: Integer): OleVariant;
begin
     InitLog(Empresa,'ImportarPagoRecibosGetASCII');
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          GetDatosPeriodo;
          //CV: CreateQuery--ok
          //Variable globales, que solamente se utilizan en este metodo
          FExisteEmp:= CreateQuery( Format( GetScript( fnExisteEmpleado ), [ Nivel0( Empresa ) ] ) );
          FExisteNom:= CreateQuery( GetScript( fnExisteNomina ) );
          ParamAsInteger( FExisteNom, 'Year', ParamList.ParamByName( 'YearDefault' ).AsInteger );
     end;
     oSuperASCII := TdmSuperASCII.Create( Self );
     try
        with oSuperASCII do
        begin
             OnValidar := ImportarPagoRecibosValidaASCII;
             Formato := eFormatoASCII( oZetaProvider.ParamList.ParamByName( 'Formato' ).AsInteger );
             AgregaColumna( 'PE_TIPO', tgNumero, 1, TRUE );
             AgregaColumna( 'PE_NUMERO', tgNumero, 3, TRUE );
             AgregaColumna( 'CB_DIGITO', tgTexto, 1, TRUE );
             AgregaColumna( 'CB_CODIGO', tgNumero, 5, TRUE );
             AgregaColumna( 'PRETTYNAME', tgTexto, 50, FALSE );
             Result := Procesa( ListaASCII );
             ErrorCount := Errores;
        end;
     finally
            oSuperASCII.Free;
            FreeAndNil(FExisteEmp);
            FreeAndNil(FExisteNom);
     end;
     EndLog;SetComplete;
end;

procedure TdmServerNominaTimbrado.ImportarPagoRecibosValidaASCII(DataSet: TDataset; var nProblemas: Integer; var sErrorMsg: String);
var
   iEmpleado: Integer;

    function NoExisteEmpleado: Boolean;
    begin
        with FExisteEmp do
        begin
             Close;
             oZetaProvider.ParamAsInteger( FExisteEmp, 'Empleado', iEmpleado );
             Open;
             Result := EOF;
             if not Result then
                DataSet.FieldByName( 'PRETTYNAME' ).AsString := FieldByName( 'PRETTYNAME' ).AsString;
        end;
    end;

    function NoExisteNomina: Boolean;
    begin
        with FExisteNom do
        begin
             Close;
             with oZetaProvider do
             begin
                  ParamAsInteger( FExisteNom, 'Empleado', iEmpleado );
                  ParamAsInteger( FExisteNom, 'Tipo', DataSet.FieldByName( 'PE_TIPO' ).AsInteger );
                  ParamAsInteger( FExisteNom, 'Numero', DataSet.FieldByName( 'PE_NUMERO' ).AsInteger );
             end;
             Open;
             Result := EOF;
        end;
    end;

begin
     with DataSet do
     begin
          iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
          // Valida Que Exista Empleado
          if NoExisteEmpleado then
          begin
               Inc( nProblemas );
               sErrorMsg := sErrorMsg + '| Empleado No Existe |';
          end;
          // Valida Que Exista Nomina
          if NoExisteNomina then
          begin
               Inc( nProblemas );
               sErrorMsg := sErrorMsg + '| Empleado no Tiene Registro de N�mina |';
          end;
          // Valida Tipo de Periodo de N�mina
          {$ifdef ANTES}
          if ( FieldByName( 'PE_TIPO' ).AsInteger < 0 ) or ( FieldByName( 'PE_TIPO' ).AsInteger > 5 ) then
          {$else}
          if ( FieldByName( 'PE_TIPO' ).AsInteger < Ord( Low(eTipoPeriodo) ) ) or ( FieldByName( 'PE_TIPO' ).AsInteger > Ord( High( eTipoPeriodo ) ) ) then
          {$endif}
          begin
               Inc( nProblemas );
               sErrorMsg := sErrorMsg + '| Tipo de Periodo Inv�lido |';
          end;
          // Valida Digito de Empresa
          if ( FieldByName( 'CB_DIGITO' ).AsString <> oZetaProvider.ParamList.ParamByName( 'DigitoEmpresa' ).AsString ) then
          begin
               Inc( nProblemas );
               sErrorMsg := sErrorMsg + '| D�gito de Empresa Diferente |';
          end;
     end;
end;

procedure TdmServerNominaTimbrado.ImportarPagoRecibosBuildDataSet;
var
   iEmpleado, iTipo, iPeriodo: Integer;
   sPrettyName: String;

begin
     cdsAuxiliar.First;
     while not cdsAuxiliar.EOF do
     begin
          with cdsAuxiliar do
          begin
               iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
               iTipo := FieldByName( 'PE_TIPO' ).AsInteger;
               iPeriodo := FieldByName( 'PE_NUMERO' ).AsInteger;
               sPrettyName := FieldByName( 'PRETTYNAME' ).AsString;
          end;
          with cdsLista, oZetaProvider do
          begin
               Append;
               FieldByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
               FieldByName( 'PRETTYNAME' ).AsString := sPrettyName;
               FieldByName( 'PE_YEAR' ).AsInteger := ParamList.ParamByName( 'YearDefault' ).AsInteger;
               FieldByName( 'PE_TIPO' ).AsInteger := iTipo;
               FieldByName( 'PE_NUMERO' ).AsInteger := iPeriodo;
               FieldByName( 'NO_USR_PAG' ).AsInteger := UsuarioActivo;
               FieldByName( 'NO_FEC_PAG' ).AsDateTime := ParamList.ParamByName( 'Fecha' ).AsDateTime;
               Post;
          end;
          cdsAuxiliar.Next;
     end;
end;

procedure TdmServerNominaTimbrado.ImportarPagoRecibosCrearDataSet;
begin
     with cdsLista do
     begin
          InitTempDataSet;
          AddIntegerField( 'CB_CODIGO' );
          AddStringField( 'PRETTYNAME', 50 );
          AddDateField( 'NO_FEC_PAG' );
          AddIntegerField( 'PE_YEAR' );
          AddIntegerField( 'PE_TIPO' );
          AddIntegerField( 'PE_NUMERO' );
          AddIntegerField( 'NO_USR_PAG' );
          // Agrega Indices
          IndexFieldNames:= 'CB_CODIGO';
          CreateTempDataset;
     end;
end;

function TdmServerNominaTimbrado.RevisaLiquidacion(Empresa, Parametros: OleVariant;
  Empleado: Integer; Fecha: TDateTime;
  var lPuedeAgregar: WordBool): OleVariant;
begin
end;

function TdmServerNominaTimbrado.GetDatosPoliza(Empresa,Parametros: OleVariant): OleVariant;
var
   iYear, iTipo, iNumero: Integer;
begin
     InitLog(Empresa,'GetDatosPoliza');
     with oZetaProvider do
     begin
          AsignaParamList( Parametros );
          with ParamList do
          begin
               iYear := ParamByName( 'Year' ).AsInteger;
               iTipo := ParamByName( 'Tipo' ).AsInteger;
               iNumero := ParamByName( 'Numero' ).AsInteger;
          end;
          Result := OpenSQL( Empresa, Format( GetScript( fnDatosPoliza ), [ iYear, iTipo, iNumero] ), True );
     end;
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.GrabaDatosPoliza(Empresa, oDelta: OleVariant;out ErrorCount: Integer): OleVariant;
begin
     InitLog(Empresa,'GrabaDatosPoliza');
     SetTablaInfo(fnDatosPoliza);
     Result := oZetaProvider.GrabaTabla( Empresa, oDelta, ErrorCount );
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.AplicaFiniquito(Empresa, Parametros: OleVariant): OleVariant;
var
   oPeriodoSimulacion : TDatosPeriodo;
   iEmpleado: Integer;
begin
     InitLog(Empresa,'AplicaFiniquito');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList(Parametros);
          GetDatosPeriodo;   // Asigna el periodo de la baja como activo
          //if ( DatosPeriodo.Status = spAfectadaTotal ) then
           //  DataBaseError( 'No se puede aplicar el finiquito a una n�mina afectada' );
          with ParamList do
          begin
               iEmpleado := ParamByName('Empleado').AsInteger;
               with oPeriodoSimulacion do
               begin
                    Year := ParamByName('YearFiniquito').AsInteger;
                    Tipo := eTipoPeriodo( ParamByName('TipoFiniquito').AsInteger );
                    Numero := ParamByName('NumeroFiniquito').AsInteger;
               end;
          end;
     end;
     Result := AplicaFiniquitoSimulacion( iEmpleado, oPeriodoSimulacion );
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.AplicaFiniquitoSimulacion( const iEmpleado: Integer; const oPeriodoSimulacion: TDatosPeriodo ): OleVariant;
var
   FCopiaNomina : TZetaCursor;
begin
     with oZetaProvider do
     begin
          FCopiaNomina := CreateQuery( GetSQLScript( Q_NOMINA_COPIA ) );
          try
             EmpiezaTransaccion;
             try
                with DatosPeriodo do
                begin
                     // Borrar la n�mina destino
                     if ( DatosPeriodo.Status = spAfectadaTotal ) then
                        DataBaseError( 'No se puede aplicar el finiquito a una n�mina afectada' );
                     ExecSQL( EmpresaActiva, Format( GetScript( fnBorraNominas ), [ Year,
                                                                                    Ord( Tipo ),
                                                                                    Numero,
                                                                                    iEmpleado,VACIO ] ) );

                     ParamAsInteger( FCopiaNomina, 'Empleado', iEmpleado );
                     ParamAsInteger( FCopiaNomina, 'YearNuevo', Year );
                     ParamAsInteger( FCopiaNomina, 'TipoNuevo', Ord( Tipo ) );
                     ParamAsInteger( FCopiaNomina, 'NumeroNuevo', Numero );
                end;

                with oPeriodoSimulacion do
                begin
                     ParamAsInteger( FCopiaNomina, 'YearOriginal', Year );
                     ParamAsInteger( FCopiaNomina, 'TipoOriginal', Ord( Tipo ) );
                     ParamAsInteger( FCopiaNomina, 'NumeroOriginal', Numero );
                end;

                // Copia la N�mina
                Ejecuta( FCopiaNomina );
                // Poner movimien como excepciones
                with DatosPeriodo do
                begin
                     ExecSQL( EmpresaActiva, Format( GetSQLScript( Q_SET_MOVIMIEN_EXCEPCIONES ), [ UsuarioActivo,
                                                                                                   Year,
                                                                                                   Ord( Tipo ),
                                                                                                   Numero,
                                                                                                   iEmpleado ] ) );
                end;

                // Actualiza Status del periodo copiado
                SetStatusPeriodo( DatosPeriodo );
                SetStatusAplicado(iEmpleado,oPeriodoSimulacion);
                TerminaTransaccion( TRUE );
             except
                   on Error: Exception do
                   begin
                        RollBackTransaccion;
                        raise;
                   end;
             end;
          finally
                 FreeAndNil(FCopiaNomina);
          end;
     end;

     SetOLEVariantToNull( Result );    // No se si deba regresar otra cosa
end;

function TdmServerNominaTimbrado.AjusteRetFonacot(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'AjusteRetFonacot');
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          InitGlobales;
          AsignaParamList( Parametros );
          InitArregloTPeriodo;
     end;
     AjusteRetFonacotParametros;
     InitBroker;
     try
        AjusteRetFonacotBuildDataSet;
        Result := AjusteRetFonacotDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
        ClearBroker;
     end;
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.AjusteRetFonacotGetLista(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'AjusteRetFonacotGetLista');
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          InitArregloTPeriodo;
     end;
     AjusteRetFonacotParametros;
     InitBroker;
     try
        AjusteRetFonacotBuildDataSet;
        Result := SQLBroker.SuperReporte.GetReporte;
     finally
            ClearBroker;
     end;
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.AjusteRetFonacotLista(Empresa, Lista, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'AjusteRetFonacotLista');
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          InitArregloTPeriodo;
     end;
     AjusteRetFonacotParametros;
     cdsLista.Lista := Lista;
     Result := AjusteRetFonacotDataset( cdsLista );
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.AjusteRetFonacotDataset( Dataset: TDataset): OleVariant;
const
     aStatusPresFonacot: array[ FALSE..TRUE ] of Integer = ( Ord(spSaldado), Ord(spActivo) );
var
   dFechaFinalMes, dFechaFonacot: TDate;
   iEmpleado: Integer;
   sReferencia, sTextoBitacora, sTPrestaAjus: String;
   lStatusActivo, lEsCargo: Boolean;
   rMontoAjuste, rSaldoAjuste, rMontoUltimoDiaMes, rAbono, rCargo: Currency;

   function ExistePrestamo: Boolean;
   begin
        with FQryPrestamo do
        begin
             Active := FALSE;
             with oZetaProvider do
             begin
                  ParamAsInteger( FQryPrestamo, 'CB_CODIGO', iEmpleado );
                  ParamAsString( FQryPrestamo, 'PR_TIPO', sTPrestaAjus);
                  ParamAsString( FQryPrestamo, 'PR_REFEREN', sReferencia);
             end;
             Active := TRUE;
             Result := ( not IsEmpty );
        end;
   end;

   procedure AgregaCargoAbono;
   const
        K_COMENTA = 'Ajuste Fonacot';
   begin
        rAbono:= 0;
        rCargo:= 0;
        with oZetaProvider do
        begin
             ParamAsInteger( FInsCargo, 'CB_CODIGO', iEmpleado );
             ParamAsString( FInsCargo, 'PR_TIPO', sTPrestaAjus );
             ParamAsString( FInsCargo, 'PR_REFEREN', sReferencia );
             ParamAsDate( FInsCargo, 'CR_CAPTURA', Date );
             ParamAsString( FInsCargo, 'CR_OBSERVA', K_COMENTA );
             ParamAsDate( FInsCargo, 'CR_FECHA', dFechaFinalMes );
             if ( lEsCargo ) then
                rCargo:= Abs( rMontoAjuste )
             else
                 rAbono:= Abs( rMontoAjuste );
             ParamAsFloat( FInsCargo, 'CR_ABONO', rAbono );
             ParamAsFloat( FInsCargo, 'CR_CARGO', rCargo );
             sTextoBitacora:= Format('Cargo: %s'+ CR_LF + 'Abono: %s ',[ FormatFloat('#0.00', rCargo), FormatFloat('#0.00', rAbono) ]);
             ParamAsInteger( FInsCargo, 'US_CODIGO', UsuarioActivo );
             Ejecuta( FInsCargo );
             EscribeBitacora( tbNormal,
                                   clbHisPrestamos,
                                   iEmpleado,
                                   dFechaFonacot,
                                   Format('Agreg� ajuste de cr�dito #%s',[sReferencia]) ,
                                   sTextoBitacora ) ;
        end;
   end;


   procedure AgregaPrestamo;
   begin
        with oZetaProvider do
        begin
             ParamAsDate( FInsPrestamo, 'PR_FECHA', dFechaFonacot );
             ParamAsString( FInsPrestamo, 'PR_REFEREN', sReferencia );
             ParamAsInteger( FInsPrestamo, 'PR_STATUS', aStatusPresFonacot[lStatusActivo] );
             ParamAsString( FInsPrestamo, 'PR_TIPO', sTPrestaAjus );
             ParamAsInteger( FInsPrestamo, 'US_CODIGO', UsuarioActivo );
             ParamAsInteger( FInsPrestamo, 'CB_CODIGO', iEmpleado );
             ParamAsFloat( FInsPrestamo, 'PR_SALDO', rMontoAjuste );
             if ( lEsCargo ) then
             begin
                  ParamAsFloat( FInsPrestamo, 'PR_CARGOS', Abs( rMontoAjuste ) );
                  ParamAsFloat( FInsPrestamo, 'PR_ABONOS', 0 );
             end
             else
             begin
                  ParamAsFloat( FInsPrestamo, 'PR_ABONOS', Abs( rMontoAjuste ) );
                  ParamAsFloat( FInsPrestamo, 'PR_CARGOS', 0 );
             end;
             Ejecuta ( FInsPrestamo );
        end;
   end;

   procedure ActualizaPrestamo;
   var
      rSaldo: TPesos;
      lAntesEraCargo: Boolean;
   begin
        with oZetaProvider do
        begin
             ParamAsInteger( FUpdPrestamo, 'CB_CODIGO', iEmpleado );
             ParamAsString( FUpdPrestamo, 'PR_TIPO', sTPrestaAjus );
             ParamAsString( FUpdPrestamo, 'PR_REFEREN', sReferencia );
             rCargo := FQryPrestamo.FieldByName( 'PR_CARGOS' ).AsFloat;
             rAbono := FQryPrestamo.FieldByName( 'PR_ABONOS' ).AsFloat;
             rSaldo := FQryPrestamo.FieldByName('PR_SALDO').AsFloat;
             lAntesEraCargo:= ( rMontoUltimoDiaMes >= 0 );

             if ( lAntesEraCargo )  then
                rCargo := ( rCargo - rMontoUltimoDiaMes )
             else
                 rAbono := ( rAbono - Abs( rMontoUltimoDiaMes ) );

             if lEsCargo then
                rCargo := rCargo + rMontoAjuste
             else
                 rAbono := rAbono + Abs( rMontoAjuste );

             ParamAsFloat( FUpdPrestamo, 'PR_CARGOS', rCargo );
             ParamAsFloat( FUpdPrestamo, 'PR_ABONOS', rAbono );

             ParamAsFloat( FUpdPrestamo, 'PR_SALDO', rSaldo - rMontoUltimoDiaMes + rMontoAjuste );
             ParamAsInteger( FUpdPrestamo, 'PR_STATUS', aStatusPresFonacot[lStatusActivo] );       // Activar Prestamo
             ParamAsInteger( FUpdPrestamo, 'US_CODIGO', UsuarioActivo );
             Ejecuta( FUpdPrestamo );
        end;
   end;

   procedure DeleteCargoAbono;
   begin
        with oZetaProvider do
        begin
             ParamAsInteger( FDelCargo, 'CB_CODIGO', iEmpleado );
             ParamAsString( FDelCargo, 'PR_TIPO', sTPrestaAjus );
             ParamAsString( FDelCargo, 'PR_REFEREN', sReferencia );
             ParamAsDate( FDelCargo, 'CR_FECHA', dFechaFinalMes );
             Ejecuta( FDelCargo );
        end;
   end;

begin
     with oZetaProvider do
     begin
          if OpenProcess( prNOAjusteRetFonacot, DataSet.RecordCount, FListaParametros ) then
          begin
               PreparaPrestamosFonacot;
               try
                  with DataSet do
                  begin
                       with ParamList do
                       begin
                            dFechaFinalMes:= LastDayofMonth( EncodeDate( ParamByName( 'Year' ).AsInteger, ParamByName( 'Mes' ).AsInteger, 1 ) );
                            sTPrestaAjus:= ParamByName('TPrestamoAjus').AsString;
                       end;
                       First;
                       while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                       begin
                            iEmpleado:= FieldByName('CB_CODIGO').AsInteger;
                            sReferencia:= FieldByName('K_PR_REFEREN').AsString;
                            rSaldoAjuste:= FieldByName('K_SALDO').AsFloat;
                            lStatusActivo:= ( rSaldoAjuste > 0 );
                            rMontoAjuste:= FieldByName('K_MONTO_AJUSTE').AsFloat;
                            lEsCargo:= ( rMontoAjuste > 0 );
                            dFechaFonacot:= FieldByName('K_PR_FECHA').AsDateTime;
                            rMontoUltimoDiaMes:= FieldByName('K_AJUS_ULIMO_D_MES').AsFloat ;
                            EmpiezaTransaccion;
                            try
                               if ( ExistePrestamo ) then
                               begin
                                    DeleteCargoAbono;
                                    AgregaCargoAbono;
                                    ActualizaPrestamo;
                               end
                               else
                               begin
                                    AgregaPrestamo;
                                    AgregaCargoAbono;
                               end;
                               TerminaTransaccion( True );
                            except
                               on Error: Exception do
                               begin
                                    RollBackTransaccion;
                                    Log.Excepcion( iEmpleado, 'Error Al Agregar Pr�stamo de Ajuste Fonacot', Error );
                               end;
                            end;
                            Next;

                    end;
               end;
               finally
                      FreeAndNil( FQryPrestamo );
                      FreeAndNil( FInsCargo );
                      FreeAndNil( FInsPrestamo );
                      FreeAndNil( FUpdPrestamo );
                      FreeAndNil( FDelCargo );
               end;
          end;
          Result := CloseProcess;
     end;

end;

procedure TdmServerNominaTimbrado.PreparaPrestamosFonacot;
const
     INFO_PRESTAMO = 'select PR_ABONOS,PR_CARGOS,PR_FECHA,PR_FORMULA,PR_MONTO,PR_NUMERO,PR_REFEREN,PR_SALDO_I,PR_STATUS,PR_TIPO,PR_TOTAL,PR_SALDO,US_CODIGO,CB_CODIGO from PRESTAMO ' +
                     'where CB_CODIGO = :CB_CODIGO and PR_TIPO = :PR_TIPO and PR_REFEREN = :PR_REFEREN';
     AGREGA_CARGO_PRESTAMO = 'insert into PCAR_ABO ( CB_CODIGO,CR_CAPTURA,PR_TIPO,CR_FECHA,PR_REFEREN,CR_OBSERVA,CR_ABONO,CR_CARGO,US_CODIGO ) ' +
                             'values ( :CB_CODIGO,:CR_CAPTURA,:PR_TIPO,:CR_FECHA,:PR_REFEREN,:CR_OBSERVA,:CR_ABONO,:CR_CARGO,:US_CODIGO )';
     AGREGA_PRESTAMO = 'insert into PRESTAMO (PR_FECHA,PR_REFEREN,PR_STATUS,PR_TIPO,US_CODIGO,CB_CODIGO, PR_SALDO, PR_CARGOS, PR_ABONOS ) '+
                                                'values ( :PR_FECHA,:PR_REFEREN,:PR_STATUS,:PR_TIPO,:US_CODIGO,:CB_CODIGO, :PR_SALDO, :PR_CARGOS, :PR_ABONOS )';
     ACTUALIZA_PRESTAMO = 'update PRESTAMO set PR_STATUS = :PR_STATUS, PR_CARGOS = :PR_CARGOS, PR_ABONOS = :PR_ABONOS, PR_SALDO = :PR_SALDO, US_CODIGO = :US_CODIGO ' +
                          'where CB_CODIGO = :CB_CODIGO and PR_TIPO = :PR_TIPO and PR_REFEREN = :PR_REFEREN';
     DELETE_CARGO_ABONO = 'delete from PCAR_ABO where ( PR_TIPO = :PR_TIPO and PR_REFEREN = :PR_REFEREN and CR_FECHA = :CR_FECHA and CB_CODIGO = :CB_CODIGO )';


begin
     with oZetaProvider do
     begin
         if FQryPrestamo = nil then
            FQryPrestamo := CreateQuery( INFO_PRESTAMO );
         if FInsCargo = nil then
            FInsCargo := CreateQuery( AGREGA_CARGO_PRESTAMO );
         if FInsPrestamo = nil then
            FInsPrestamo := CreateQuery( AGREGA_PRESTAMO );
         if FUpdPrestamo = nil then
            FUpdPrestamo := CreateQuery( ACTUALIZA_PRESTAMO );
         if FDelCargo = nil then
            FDelCargo := CreateQuery ( DELETE_CARGO_ABONO );
    end;
end;



procedure TdmServerNominaTimbrado.AjusteRetFonacotParametros;
var
   sListaParamsNom: String;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          sListaParamsNom:= VACIO;
          {
          if ( ParamByName('FiltraTipNom').AsBoolean ) then
             sListaParamsNom:= K_PIPE + 'Tipo de N�mina:' + ObtieneElemento( lfTipoPeriodo, ParamByName('TipoNom').AsInteger )  +
                               K_PIPE + 'Rango Inicial:' + ParamByName('RangoIni').AsString +
                               K_PIPE + 'Rango Final:' + ParamByName('RangoFin').AsString;   }

          FListaParametros := 'A�o: ' + ParamByName( 'Year' ).AsString +
                              K_PIPE + 'Mes: ' + ObtieneElemento( lfMeses, ParamByName( 'Mes' ).AsInteger - GetOffSet( lfMeses ) ) +
                              K_PIPE + 'Tipo Pr�stamo Fonacot: ' + ParamByName('TPrestamo').AsString +
                              K_PIPE + 'Tipo Pr�stamo Ajuste: ' + ParamByName('TPrestamoAjus').AsString +
                              K_PIPE + 'Ajuste Anterior: ' + BoolToSiNo( ParamByName('AjusAnt').AsBoolean ) +
                              K_PIPE + 'Incluir Incapacitados: ' + BoolToSiNo( ParamByName('Incapacitados').AsBoolean ) +
                              K_PIPE + 'Saldar Pr�stamos a Favor ' + BoolToSiNo( ParamByName('SaldarPresta').AsBoolean ) +
                              sListaParamsNom ;
     end;
end;

{$IFDEF DOS_CAPAS}
function TdmServerNominaTimbrado.GetSaldoAjuste( const iEmpleado: Integer; const sReferencia: String ): TPesos;
begin
     with  oZetaProvider do
     begin
          ParamAsInteger( FSaldoActualAjuste, 'CB_CODIGO', iEmpleado );
          ParamAsString( FSaldoActualAjuste, 'PR_REFEREN', sReferencia );
          with FSaldoActualAjuste do
          begin
               Active:= TRUE;
               Result:= FieldByName('SALDO').AsFloat;
               Active:= FALSE;
          end;
     end;
end;
{$ENDIF}

function TdmServerNominaTimbrado.GetRangosPeriodos( const sScript: String; const iMaxTiposNom, iYear, iMes: Integer; const oArregloRango: OleVariant  ): String;
const
     K_FILTRO_TIPO_RANGO = '( PE_TIPO = %0:s AND PE_NUMERO >= %1:s and PE_NUMERO <= %2:s )';
var
   i: Integer;
   sFiltroRango: String;
begin
     for i:= 0 to iMaxTiposNom - 1 do
     begin
          sFiltroRango:= ConcatString( sFiltroRango, Format( K_FILTRO_TIPO_RANGO, [ oArregloRango[i][0], oArregloRango[i][1], oArregloRango[i][2] ] ), ' OR ' );
     end;
     Result:= Format( sScript, [ iYear, Ord( spAfectadaTotal ), sFiltroRango, oZetaProvider.GetGlobalInteger( K_GLOBAL_LIMITE_NOMINAS_ORDINARIAS ), iMes ]);
end;


function TdmServerNominaTimbrado.GetParamRangosPeriodos( const oArregloRango: OleVariant; const iTipoNomina, iParam, iMaxRangos: Integer ): String;
var
   i: Integer;
begin
     for i:= 0 to iMaxRangos - 1 do
     begin
          if ( oArregloRango[i][0] = IntToStr(iTipoNomina) ) then
          begin
               Result:= oArregloRango[i][iParam];
               Exit;
          end;
     end;
end;

procedure TdmServerNominaTimbrado.AjusteRetFonacotBuildDataSet;
const
     K_NINGUNO = -1;
     aFiltroActivos: array[FALSE..TRUE] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = (VACIO,' COLABORA.CB_ACTIVO = ''N'' OR ');
var
   iYear, iMes, iConcepto, iMaxTiposNom : Integer;
   dFechaReferencia,dFechaFinalMes,dFechaInicialMes, dFechaParamIni, dFechaParamFin: TDate;
   lIncluirIncapa,lReemplazar, lFiltraporNom, lSaldarPresta, lSaldarBajas, lPagoFonacot, lEmpBajaASaldar,
   lEmpBaja, lPrestaASaldar, lEmpActivoASaldar, lFecPrestamoPost, lEsIncapacitado: Boolean;
   sTPrestamoAjus, sTPrestamo, sQryMontosRetenidos: String;
   sPeriodosConcat, sFiltroPeriodo: String;
   rSaldoAnteriorFonacot, rComparativoAjuste, rTolerancia: TPesos;
   oArreglo: OleVariant;

   function GetColumnaNomina( const sColumna: String ): String;
   begin
        Result:= Format( GetSQLScript( Q_FILTRO_COL_NOMINA ), [ sColumna, iYear, sFiltroPeriodo ] );
   end;

   procedure PreparaAjusteFonacot;
   begin
         with SQLBroker.SuperReporte.DataSetReporte do
         begin
              //Trae el pago que se le dio a Fonacot el mes anterior.
              rSaldoAnteriorFonacot:= GetSaldoFonacotAnt( FieldByName('K_TOTAL_ABONO_FONACOT').AsFloat,
                                                                          FieldByName('K_RETENCION').AsFloat,
                                                                          FieldByName('K_SALDO_ACTUAL_AJUSTE').AsFloat,
                                                                          0 );

              //Cuando el filtro es por n�mina las fechas se traen del arreglo.
              if ( lFiltraporNom ) then
              begin
                   dFechaParamIni:=  StrAsInteger( GetParamRangosPeriodos( oArreglo, FieldByName('CB_NOMINA').AsInteger, K_PARAM_FECHA_INI, iMaxTiposNom ) );
                   dFechaParamFin:=  StrAsInteger( GetParamRangosPeriodos( oArreglo, FieldByName('CB_NOMINA').AsInteger, K_PARAM_FECHA_FIN, iMaxTiposNom ) );
              end
              else
              begin
                   dFechaParamIni:= dFechaInicialMes;
                   dFechaParamFin:= dFechaFinalMes;
              end;

              //Dice con cual va a comparar: Objetivo - Si el empleado ya termino de pagar a fonacot el mes anterior, va a tomar el total de retenci�n.
              rComparativoAjuste:= rMin( FieldByName('K_MONTO_PRESTADO').AsFloat - rSaldoAnteriorFonacot,
                                                 FieldByName('K_PR_PAG_PER').AsFloat );
              //El empleado ya termino de pagar si el saldo anterior es igual a lo que monto prestado. Para asegurarnos lo comparamos tambi�n contra el >

              lPagoFonacot:= ( rSaldoAnteriorFonacot >= FieldByName('K_MONTO_PRESTADO').AsFloat );

              lEmpBaja:= ( FieldByName('CB_ACTIVO').AsString = K_GLOBAL_NO );

              //El pr�stamo es candidato a saldarse si es menor a 0
              lPrestaASaldar:= ( FieldByName('K_SALDO_ACTUAL_AJUSTE').AsFloat < 0 );

              //Una baja solamente se salda si tiene palomeada la opci�n de saldar bajas, el empleado es baja y el pr�stamo es candidato a saldarse.
              lEmpBajaASaldar:= lSaldarBajas and lEmpBaja and lPrestaASaldar;

              //Un activo solamente se salda si tiene palomeada la opci�n de saldar activos y el pr�stamo es candidato a saldarse.
              lEmpActivoASaldar:= ( lSaldarPresta ) and ( lPrestaASaldar );

             // La fecha del pr�stamo mayor a la fecha inicial del periodo de n�mina que le corresponde.
              lFecPrestamoPost:=  FieldByName('K_PR_FECHA').AsDateTime > dFechaParamIni;
               // AND ( PR_FECHA <= %s )

              //Incapacidades en rango de fechas
              lEsIncapacitado:= ( ( FieldByName('K_NO_DIAS_IN').AsInteger > 0 ) and ( not ( lSaldarBajas and lEmpBaja ) ) );
         end;
   end;

begin
     with oZetaProvider.ParamList do
     begin
          iYear:= ParamByName('Year').AsInteger;
          iMes:= ParamByName('Mes').AsInteger;
          sTPrestamo:= ParamByName('TPrestamo').AsString;
          sTPrestamoAjus:= ParamByName('TPrestamoAjus').AsString;
          dFechaReferencia:= EncodeDate( iYear, iMes, 1 );
          dFechaFinalMes:= LastDayofMonth( dFechaReferencia );
          dFechaInicialMes:= FirstDayOfMonth(dFechaReferencia);
          lIncluirIncapa:= ParamByName('Incapacitados').AsBoolean;
          lReemplazar:= ParamByName('AjusAnt').AsBoolean;
          lFiltraporNom:= ParamByName('FiltraTipNom').AsBoolean;
          iConcepto:= ParamByName('NoConcepto').AsInteger;
          lSaldarPresta:= ParamByName('SaldarPresta').AsBoolean;
          lSaldarBajas:= ParamByName('SaldarBajas').AsBoolean;
          rTolerancia:= ParamByName('Tolerancia').AsFloat;
          oArreglo:= ParamByName('RangosNomina').Value;
          iMaxTiposNom:= ParamByName('MaxTiposNom').AsInteger;


          {Esta fecha viene del par�metro sirve para agregar el filtro sobre pr�stamos,
          ya que no se toma del mes sino del rango de n�minas. Se toma en cuenta para investigar si el pr�stamo debe de considerar:
          incapacidades y bajas}
          //dFechaParamIni:= ParamByName('FechaIniRef').AsDateTime;
         // dFechaParamFin:= ParamByName('FechaFinRef').AsDateTime;

        //  sFechaParamIni:= GetRangosPeriodos( Format( GetSQLScript ( Q_FILTRO_X_TIPNOM_FEC ), [ 'MIN( PERIODO.PE_FEC_INI )', K_FILTRO_PERIODO_RANGOS ] ),iMaxTiposNom,
                                     //         iYear, iMes, oArreglo );
        //  sFechaParamFin:= GetRangosPeriodos( Format( GetSQLScript ( Q_FILTRO_X_TIPNOM_FEC ), [ 'MAX( PERIODO.PE_FEC_FIN )', K_FILTRO_PERIODO_RANGOS ] ),iMaxTiposNom,
                                      //        iYear, iMes, oArreglo );

          if ( lFiltraporNom ) then
          begin
               sPeriodosConcat:= GetRangosPeriodos( GetSQLScript( Q_FILTRO_X_TIPNOM ), iMaxTiposNom,  iYear, iMes, oArreglo);
          end
          else
              sPeriodosConcat:= Format( GetSQLScript( Q_FILTRO_PERIODO_MEN ), [ iYear, iMes, Ord( spAfectadaTotal ) ] );

          sFiltroPeriodo:= GetFiltroPeriodo( sPeriodosConcat );  // Dispara este evento solamente una vez para traerse el filtro sobre los periodos
          sQryMontosRetenidos:= GetQryMontos( sFiltroPeriodo, iYear, iConcepto, iMes  );


     end;
     with SQLBroker do
     begin
          Init( enPrestamo );
          with Agente do
          begin
               AgregaColumna( 'CB_CODIGO', TRUE, Entidad, tgNumero, 0, 'CB_CODIGO' );
               AgregaColumna( K_PRETTYNAME, TRUE, enEmpleado, tgTexto, K_ANCHO_PRETTYNAME, 'K_PRETTYNAME' );
               AgregaColumna( 'CB_ACTIVO', TRUE, enEmpleado, tgBooleano, 0, 'CB_ACTIVO' );
               if lFiltraporNom then
                  AgregaColumna( 'CB_NOMINA', TRUE, enEmpleado, tgNumero, 0, 'CB_NOMINA' );
               AgregaColumna( 'PR_REFEREN', TRUE, Entidad, tgTexto, K_ANCHO_REFERENCIA, 'K_PR_REFEREN');
               AgregaColumna( 'PR_PAG_PER', TRUE, Entidad, tgFloat, K_ANCHO_PESOS, 'K_PR_PAG_PER');
               AgregaColumna( 'PR_FECHA', TRUE, Entidad, tgFecha, 0, 'K_PR_FECHA' );

               AgregaColumna(  sQryMontosRetenidos , TRUE, enNinguno, tgFloat, K_ANCHO_PESOS, 'K_RETENCION');
               AgregaColumna(  GetColumnaNomina('NO_DIAS_VA'), TRUE, Entidad, tgFloat, K_ANCHO_PESOS, 'K_NO_DIAS_VA' );
               AgregaColumna(  GetColumnaNomina('NO_DIAS_FV'), TRUE, Entidad, tgFloat, K_ANCHO_PESOS, 'K_NO_DIAS_FV' );
               AgregaColumna(  GetColumnaNomina('NO_DIAS_IN'), TRUE, Entidad, tgFloat, K_ANCHO_PESOS, 'K_NO_DIAS_IN' );
               AgregaColumna(  GetColumnaNomina(' NO_DIAS_FI + NO_DIAS_FJ + NO_DIAS_SG + NO_DIAS_OT '), TRUE, Entidad, tgFloat, K_ANCHO_PESOS, 'K_AUSENTISMOS' );

               { Se cambi� para optimizar el query
               AgregaColumna( Format( GetSQLScript( Q_MONTOS_RETENIDOS ), ['PRESTAMO.CB_CODIGO',
                                                                                EntreComillas( sTPrestamo ),
                                                                                'PRESTAMO.PR_REFEREN',
                                                                                iYear,
                                                                                 sFiltroMonto] ) , TRUE, enNinguno, tgFloat, K_ANCHO_PESOS, 'K_RETENCION'); }

               AgregaColumna( Format( GetSQLScript( Q_MONTOS_OTROS_AJUSTES ), ['PRESTAMO.CB_CODIGO',
                                                                                EntreComillas( sTPrestamoAjus ),
                                                                                'PRESTAMO.PR_REFEREN',
                                                                                DateToStrSQLC(dFechaInicialMes),
                                                                                DateToStrSQLC(dFechaFinalMes - 1)
                                                                                ] ) ,TRUE, enNinguno, tgFloat, K_ANCHO_PESOS, 'K_AJUSTE' );
               AgregaColumna( '0', TRUE, enNinguno, tgFloat, K_ANCHO_PESOS, 'K_MONTO_AJUSTE' );
               AgregaColumna( '0', TRUE, enNinguno, tgFloat, K_ANCHO_PESOS, 'K_SALDO' );

               AgregaColumna( Format ( GetSQLScript( Q_MONTO_AJUSTE_ULTIMO_MES ), ['PRESTAMO.CB_CODIGO',
                                                                                   EntreComillas( sTPrestamoAjus ),
                                                                                   'PRESTAMO.PR_REFEREN',
                                                                                   DateToStrSQLC(dFechaFinalMes )] ), TRUE, enNinguno, tgFloat, K_ANCHO_PESOS, 'K_AJUS_ULIMO_D_MES' );

               {$IFNDEF DOS_CAPAS}
               AgregaColumna( Format( GetSQLScript( Q_SALDO_ACTUAL_AJUSTE ), [ 'PRESTAMO.CB_CODIGO',
                                                                                EntreComillas( sTPrestamoAjus ),
                                                                                'PRESTAMO.PR_REFEREN' ] ), TRUE, enNinguno, tgFloat, K_ANCHO_PESOS, 'K_SALDO_ACTUAL_AJUSTE');

               {$ELSE}
               AgregaColumna( '0', TRUE, enNinguno, tgFloat, K_ANCHO_PESOS, 'K_SALDO_ACTUAL_AJUSTE' );
               {$ENDIF}

               AgregaColumna( 'PR_STATUS', TRUE, Entidad, tgNumero, 0, 'PR_STATUS');

               AgregaColumna( 'PR_SALDO_I + PR_TOTAL + PR_ABONOS', TRUE, Entidad, tgFloat, K_ANCHO_PESOS, 'K_TOTAL_ABONO_FONACOT' );

               AgregaColumna( 'PR_MONTO + PR_CARGOS', TRUE, Entidad, tgFloat, K_ANCHO_PESOS, 'K_MONTO_PRESTADO' );

               if not ( lSaldarBajas ) then
               begin
                    AgregaFiltro( 'COLABORA.CB_ACTIVO = ''S'' ', True, enEmpleado );
               end;

               AgregaFiltro( Format('( PR_TIPO = %s ) AND ( PR_STATUS <> %d ) ',[ EntreComillas( sTPrestamo ),
                                                                                                        Ord(spCancelado){,
                                                                                                        DateToStrSQLC(dFechaParamIni)} ]),True, Entidad );

            {   if not ( lIncluirIncapa ) then
                  AgregaFiltro( Format( '( %s ( SUMA_INCA(1, %s, %s ) = 0 ) )',[ aFiltroActivos[lSaldarBajas],
                                                                             EntreComillas( FechaAsStr( dFechaParamIni) ),
                                                                             EntreComillas( FechaAsStr(dFechaParamFin)),VACIO]), FALSE, enNinguno );  }
               if not( lReemplazar ) then
                  AgregaFiltro( Format( GetSQLScript( Q_REEMPLAZAR_AJUSTE ), [ 'PRESTAMO.CB_CODIGO',
                                                                               EntreComillas( sTPrestamoAjus ),
                                                                               'PRESTAMO.PR_REFEREN',
                                                                               DateToStrSQLC(dFechaFinalMes )] ) , True, Entidad);

               AgregaOrden( 'CB_CODIGO', True, Entidad );
          end;
          AgregaRangoCondicion( oZetaProvider.ParamList );
          FiltroConfidencial( oZetaProvider.EmpresaActiva );
          BuildDataset( oZetaProvider.EmpresaActiva );
          with SuperReporte.DataSetReporte do
          begin
               {$IFDEF DOS_CAPAS}
               try
                  with oZetaProvider do
                  begin

                       FSaldoActualAjuste:= CreateQuery( GetSQLScript( Q_SALDO_ACTUAL_AJUSTE ) );
                       ParamAsString( FSaldoActualAjuste, 'PR_TIPO', sTPrestamoAjus );

                  end;
               {$ENDIF}
                  while not EOF do
                  begin
                       {$IFDEF DOS_CAPAS}
                       Edit;
                       FieldByName('K_SALDO_ACTUAL_AJUSTE').AsFloat:= GetSaldoAjuste( FieldByName('CB_CODIGO').AsInteger,
                                                                                      FieldByName('K_PR_REFEREN').AsString ) ;
                       Post;
                       {$ENDIF}

                       PreparaAjusteFonacot;

                       { El pr�stamo de ajuste se puede borrar si:
                         1. El empleado ya pago a fonacot y a la empresa, no en ese mes.
                         2. El empleado es baja y pr�stamo de ajuste esta activo.
                         3. La retencion del mes es igual al comparativo.
                         4. La fecha del prestamo es mayor a la fecha inicial de las n�minas.
                         5. Si el usuario no desea incluir incapacitados y el empleado esta incapacitado.  }

                       if ( ( lPagoFonacot and ( FieldByName('K_RETENCION').AsFloat = 0 ) ) or
                            ( lEmpBaja ) and ( not lPrestaASaldar ) or
                            ( PesosIgualesP( FieldByName('K_RETENCION').AsFloat + FieldByName('K_AJUSTE').AsFloat, rComparativoAjuste, rTolerancia ) ) or
                            ( lFecPrestamoPost or ( not lIncluirIncapa and lEsIncapacitado ) ) ) then
                       begin
                            Delete;
                       end
                       else
                       begin
                            Edit;
                            if  not ( lEmpBajaASaldar or lEmpActivoASaldar )  then
                            begin
                                 FieldByName('K_MONTO_AJUSTE').AsFloat:= rComparativoAjuste -
                                                                         FieldByName('K_RETENCION').AsFloat -
                                                                         FieldByName('K_AJUSTE').AsFloat;
                            end
                            else
                            begin
                                 FieldByName('K_MONTO_AJUSTE').AsFloat:= - FieldByName('K_SALDO_ACTUAL_AJUSTE').AsFloat;
                            end;

                            FieldByName('K_SALDO').AsFloat:= FieldByName('K_SALDO_ACTUAL_AJUSTE').AsFloat -
                                                             FieldByName('K_AJUS_ULIMO_D_MES').AsFloat +
                                                             FieldByName('K_MONTO_AJUSTE').AsFloat ;
                            Next;
                       end;
                  end;
               {$IFDEF DOS_CAPAS}
               finally
                      FreeAndNil(FSaldoActualAjuste);
               end;
               {$ENDIF}
          end;
     end;

end;


function TdmServerNominaTimbrado.GetFiltroPeriodo( const sPeriodosConcat: String ): String;
const
     K_NINGUNO = '-1';
     QRY_CONCAT_FILTRO = '( ( PE_TIPO = %d ) AND ( PE_NUMERO IN ( %s ) ) )';
var
   FQryFiltroMonto: TZetaCursor;
   iTipoNomina: Integer;

     function GetListaPeriodos: String;
     begin
          Result:= VACIO;
          with oZetaProvider do
          begin
               with FQryFiltroMonto do
               begin
                    Repeat
                          Result:= ConcatString( Result,FieldByName('PE_NUMERO').AsString,',');
                          iTipoNomina:= FieldByName('PE_TIPO').AsInteger;
                          Next;
                    Until ( Eof or ( iTipoNomina <> FieldByName('PE_TIPO').AsInteger ) );
               end;
          end;
     end;

begin
     with oZetaProvider do
     begin
          FQryFiltroMonto:= CreateQuery ( sPeriodosConcat );
          try
             with FQryFiltroMonto do
             begin
                  Active:= TRUE;
                  if not ( IsEmpty ) then
                  begin
                       Repeat
                             iTipoNomina:= FieldByName('PE_TIPO').AsInteger;  //Se asigna al principio de concatenar
                             Result:= ConcatString( Result, Format( QRY_CONCAT_FILTRO, [ iTipoNomina, GetListaPeriodos ] ), ' OR ' );
                       Until ( Eof );
                  end;
                  Active:= FALSE;
                  //Si el query sobre peridos no da ningun periodo se concatenar� ( PE_NUMERO = -1 )
                  if ( StrVacio ( Result ) ) then
                     Result:= '( PE_NUMERO = -1 )';
             end;
          finally
                FreeAndNil( FQryFiltroMonto );
          end;
     end;
end;


function TdmServerNominaTimbrado.GetQryMontos( const sFiltroPeriodo: String; const iYear, iConcepto, iMes : Integer ): String;
begin
     Result:= Format( GetSQLScript( Q_MONTOS_RETENIDOS ), ['PRESTAMO.CB_CODIGO',
                                                            IntToStr(iConcepto),
                                                            'PRESTAMO.PR_REFEREN',
                                                            iYear,
                                                            sFiltroPeriodo ] );
end;

function TdmServerNominaTimbrado.CalcularPagoFonacot(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'CalcularPagoFonacot');
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          InitGlobales;
          AsignaParamList( Parametros );
          InitArregloTPeriodo;
     end;
     CalcularPagoFonacotParametros;
     InitBroker;
     try
        CalcularPagoFonacotBuildDataSet;
        Result := CalcularPagoFonacotDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
        ClearBroker;
     end;
     EndLog;SetComplete;
end;



procedure TdmServerNominaTimbrado.CalcularPagoFonacotBuildDataSet;
const
     K_FILTRO_PRESTA_ACTIVO  = '( PR_TIPO = %s ) AND ( PR_STATUS = %d or %s > 0 %s  )';


     K_FILTRO_TIENE_CARGOS =  'or ( select COUNT(*) from PCAR_ABO ' +
                                               ' where ( PCAR_ABO.CB_CODIGO = %s ) and  ' +
                                               '( PCAR_ABO.PR_TIPO = %s ) and ' +
                                               '( PCAR_ABO.PR_REFEREN = %s ) and ' +
                                               '( PCAR_ABO.CR_FECHA between %s and %s ) ) > 0';
     K_FILTRO_TIPO_NOM = '( CB_NOMINA = %d )';



     K_NINGUNO = -1;

var
   iYear, iMes, iConcepto, iMaxTiposNom : Integer;
   dFechaReferencia,dFechaFinalMes,dFechaInicialMes, dFechaParamFin: TDate;
   sTPrestamoAjus, sTPrestamo, sFiltro, sFiltroCargos, sQryMontosRetenidos, sPeriodosConcat: String;
   lFiltraporNom, lFecPrestamoPost: Boolean;
   rSaldoAnteriorFonacot: TPesos;
   oArreglo: OleVariant;

   procedure InicializaCalculo;
   begin
        rSaldoAnteriorFonacot:= 0;
        lFecPrestamoPost:= FALSE;
        if ( lFiltraporNom ) then
        begin
             dFechaParamFin:=  StrAsInteger( GetParamRangosPeriodos( oArreglo, SQLBroker.SuperReporte.DataSetReporte.FieldByName('CB_NOMINA').AsInteger,
                                                                     K_PARAM_FECHA_FIN, iMaxTiposNom ) );
        end
        else
        begin
             dFechaParamFin:= dFechaFinalMes;
        end;
   end;

begin
     with oZetaProvider.ParamList do
     begin
          iYear:= ParamByName('Year').AsInteger;
          iMes:= ParamByName('Mes').AsInteger;
          sTPrestamo:= ParamByName('TPrestamo').AsString;
          sTPrestamoAjus:= ParamByName('TPrestamoAjus').AsString;
          dFechaReferencia:= EncodeDate( iYear, iMes, 1 );
          dFechaFinalMes:= LastDayofMonth( dFechaReferencia );
          dFechaInicialMes:= FirstDayOfMonth(dFechaReferencia);
          lFiltraporNom:= ParamByName('FiltraTipNom').AsBoolean;
          iConcepto:= ParamByName('NoConcepto').AsInteger;
          oArreglo:= ParamByName('RangosNomina').Value;
          iMaxTiposNom:= ParamByName('MaxTiposNom').AsInteger;

          if ( lFiltraporNom ) then
          begin
               sPeriodosConcat:= GetRangosPeriodos( GetSQLScript( Q_FILTRO_X_TIPNOM ), iMaxTiposNom,  iYear, iMes, oArreglo);
          end
          else
              sPeriodosConcat:= Format( GetSQLScript( Q_FILTRO_PERIODO_MEN ), [ iYear, iMes, Ord( spAfectadaTotal ) ] );

     end;
     with SQLBroker do
     begin
          Init( enPrestamo );
          { Se cambi� para optimizar el query
          sQryMontosRetenidos:= Format( GetSQLScript( Q_MONTOS_RETENIDOS ), ['PRESTAMO.CB_CODIGO',
                                                                            EntreComillas( sTPrestamo),
                                                                            'PRESTAMO.PR_REFEREN',
                                                                            iYear,
                                                                            sFiltroMonto ] ); }

          sQryMontosRetenidos:= GetQryMontos( GetFiltroPeriodo( sPeriodosConcat ), iYear, iConcepto, iMes ) ;
          with Agente do
          begin
               AgregaColumna( 'CB_CODIGO', TRUE, Entidad, tgNumero, 0, 'CB_CODIGO' );
               AgregaColumna( 'CB_NOMINA', TRUE, enEmpleado, tgNumero, 0, 'CB_NOMINA' );
               AgregaColumna( 'PR_REFEREN', TRUE, Entidad, tgTexto, K_ANCHO_REFERENCIA, 'PR_REFEREN');
               AgregaColumna( 'PR_PAG_PER', TRUE, Entidad, tgFloat, K_ANCHO_PESOS, 'FC_RETENC');

               AgregaColumna( 'PR_STATUS', TRUE, Entidad, tgNumero, 0, 'PR_STATUS' );
               AgregaColumna( 'PR_FECHA', TRUE, Entidad, tgFecha, 0, 'K_PR_FECHA' );

               AgregaColumna(  sQryMontosRetenidos , TRUE, enNinguno, tgFloat, K_ANCHO_PESOS, 'FC_NOMINA');


               AgregaColumna( Format( GetSQLScript( Q_MONTOS_OTROS_AJUSTES ), ['PRESTAMO.CB_CODIGO',
                                                                                EntreComillas( sTPrestamoAjus ),
                                                                                'PRESTAMO.PR_REFEREN',
                                                                                DateToStrSQLC(dFechaInicialMes),
                                                                                DateToStrSQLC(dFechaFinalMes)
                                                                                ] ) ,TRUE, enNinguno, tgFloat, K_ANCHO_PESOS, 'FC_AJUSTE' );


               {$IFNDEF DOS_CAPAS}
               AgregaColumna( Format( GetSQLScript( Q_SALDO_ACTUAL_AJUSTE ), [ 'PRESTAMO.CB_CODIGO',
                                                                                EntreComillas( sTPrestamoAjus ),
                                                                                'PRESTAMO.PR_REFEREN' ] ), TRUE, enNinguno, tgFloat, K_ANCHO_PESOS, 'K_SALDO_ACTUAL_AJUSTE');

               {$ELSE}
               AgregaColumna( '0', TRUE, enNinguno, tgFloat, K_ANCHO_PESOS, 'K_SALDO_ACTUAL_AJUSTE' );
               {$ENDIF}

               AgregaColumna( 'PR_SALDO_I + PR_TOTAL + PR_ABONOS', TRUE, Entidad, tgFloat, K_ANCHO_PESOS, 'K_TOTAL_ABONO_FONACOT' );

               AgregaColumna( 'PR_MONTO + PR_CARGOS', TRUE, Entidad, tgFloat, K_ANCHO_PESOS, 'K_MONTO_PRESTADO' );

               if ZetaCommonTools.StrLleno( sTPrestamoAjus ) then
               begin
                    sFiltroCargos:= Format( K_FILTRO_TIENE_CARGOS , [ 'PRESTAMO.CB_CODIGO',
                                                                    ZetaCommonTools.EntreComillas( sTPrestamoAjus ),
                                                                    'PRESTAMO.PR_REFEREN',
                                                                    DateToStrSQLC(dFechaInicialMes), DateToStrSQLC(dFechaFinalMes)  ] );

               end
               else
               begin
                    sFiltroCargos := VACIO;
               end;


               sFiltro:= Format( K_FILTRO_PRESTA_ACTIVO, [ EntreComillas( sTPrestamo ),
                                                           Ord(spActivo),
                                                           sQryMontosRetenidos,
                                                           sFiltroCargos] );


               AgregaFiltro( sFiltro, True, Entidad );
               AgregaOrden( 'CB_CODIGO', True, Entidad );
               AgregaOrden( 'PR_REFEREN', True, Entidad );


          end;
          AgregaRangoCondicion( oZetaProvider.ParamList );
          FiltroConfidencial( oZetaProvider.EmpresaActiva );
          BuildDataset( oZetaProvider.EmpresaActiva );


          InicializaCalculo;
          with SuperReporte.DataSetReporte do
          begin
              {$IFDEF DOS_CAPAS}
              try
                 with oZetaProvider do
                 begin
                      FSaldoActualAjuste:= CreateQuery( GetSQLScript( Q_SALDO_ACTUAL_AJUSTE ) );
                      ParamAsString( FSaldoActualAjuste, 'PR_TIPO', sTPrestamoAjus );
                 end;
                 {$ENDIF}

                 while not EOF do
                 begin
                      if StrLleno( sTPrestamoAjus ) then
                      begin
                           {$IFDEF DOS_CAPAS}
                           Edit;
                           FieldByName('K_SALDO_ACTUAL_AJUSTE').AsFloat:= GetSaldoAjuste( FieldByName('CB_CODIGO').AsInteger,
                                                                                           FieldByName('PR_REFEREN').AsString ) ;
                           Post;
                           {$ENDIF}

                           lFecPrestamoPost:= ( FieldbyName('K_PR_FECHA').AsDateTime > dFechaParamFin );

                           rSaldoAnteriorFonacot:= GetSaldoFonacotAnt( FieldByName('K_TOTAL_ABONO_FONACOT').AsFloat,
                                                                               FieldByName('FC_NOMINA').AsFloat,
                                                                               FieldByName('K_SALDO_ACTUAL_AJUSTE').AsFloat,
                                                                               FieldByName('FC_AJUSTE').AsFloat );

                      end;

                      if ( lFecPrestamoPost ) or ( rSaldoAnteriorFonacot >= FieldByName('K_MONTO_PRESTADO').AsFloat ) then
                         Delete;

                      Next;
                 end;
              {$IFDEF DOS_CAPAS}
              finally
                     FreeAndNil(FSaldoActualAjuste);
              end;
              {$ENDIF}
          end;
     end;

end;


function TdmServerNominaTimbrado.CalcularPagoFonacotDataset( Dataset: TDataset): OleVariant;
const
     K_INCIDENCIA_BAJA = 'B';
     K_INCIDENCIA_INCAPA = 'I';
     K_NINGUNA_INCIDENCIA = '0';
     aStatusCalculo : array [ FALSE..TRUE ] of Integer = ( Ord( slCalculadaParcial ) , Ord( slCalculadaTotal ) );
var
   iEmpleado, iYear, iMes, iStatusCalculo, iTipoNomina, iMaxTiposNom: Integer;
   sTPrestaAjus, sIncidencia, sReferencia, sTPrestamo: String;
   rMontoMensual, rMontoRetencion, rMontoCargoAbono: TPesos;
   oQryIncapa, oInsInfoCred, oInsInfoEmpCred, oInsInfoCalcFon: TZetaCursor;
   oDelInfoCredito, oUpdInfoCalcFon, oFechaBajaMes, oUpdInfoEmpCred : TZetaCursor;
   oCalculoFonacot: TCalculoFonacot;
   oTotalesEmpleado: TTotalesFonacotEmpleado;
   oTotalesEmpresa: TTotalesFonacotEmpresa;
   oArreglo: OleVariant;

   procedure SetCamposProceso( oCursor: TZetaCursor );
   begin
        with oZetaProvider do
        begin
             ParamAsInteger( oCursor, 'FT_YEAR', iYear );
             ParamAsInteger( oCursor, 'FT_MONTH', iMes );
             ParamAsString( oCursor, 'PR_TIPO', sTPrestamo );
        end;
   end;

   procedure InicializaFechas;
   begin
        //Limpia fechas de incidencia antes de empezar el c�lculo
        with oCalculoFonacot do
        begin
             FechaIncidencia:= NullDateTime;
             FechaFinalIncapa:= NullDateTime;
        end;
   end;


   procedure PreparaCalculoFonacot;
   const
        INFO_INCAPACIDADES = 'SELECT IN_FEC_INI, IN_FEC_FIN, IN_DIAS from INCAPACI where ( CB_CODIGO = :CB_CODIGO ) and ' +
                             '( IN_FEC_INI <= :IN_FEC_FIN ) and ( IN_FEC_FIN > :IN_FEC_INI  )  ORDER BY IN_FEC_INI';

        UPDATE_INFO_CALC_FON = 'UPDATE FON_TOT set FT_STATUS = :FT_STATUS, FT_RETENC = :FT_RETENC, FT_NOMINA = :FT_NOMINA, FT_AJUSTE = :FT_AJUSTE, FT_EMPLEAD = :FT_EMPLEAD, FT_DETALLE = :FT_DETALLE, ' +
                               'FT_CUANTOS = :FT_CUANTOS, FT_BAJAS = :FT_BAJAS, FT_INCAPA = :FT_INCAPA WHERE ( FT_YEAR = :FT_YEAR AND FT_MONTH = :FT_MONTH AND PR_TIPO = :PR_TIPO )';

        UPDATE_INFO_EMP_CRED = 'UPDATE FON_EMP set FE_RETENC = :FE_RETENC, FE_NOMINA = :FE_NOMINA, FE_AJUSTE = :FE_AJUSTE, FE_CUANTOS = :FE_CUANTOS ' +
                               'WHERE ( FT_YEAR = :FT_YEAR AND FT_MONTH = :FT_MONTH AND PR_TIPO = :PR_TIPO AND CB_CODIGO = :CB_CODIGO )';

        AGREGA_INFO_CREDITO  = 'INSERT INTO FON_CRE ( FT_YEAR, FT_MONTH, PR_TIPO, CB_CODIGO, PR_REFEREN, FC_RETENC, FC_NOMINA, FC_AJUSTE ) '+
                               'VALUES ( :FT_YEAR, :FT_MONTH, :PR_TIPO, :CB_CODIGO, :PR_REFEREN, :FC_RETENC, :FC_NOMINA, :FC_AJUSTE )';

        AGREGA_INFO_EMP_CRED = 'INSERT INTO FON_EMP ( FT_YEAR, FT_MONTH, PR_TIPO, CB_CODIGO, FE_INCIDE, FE_FECHA1, FE_FECHA2 ) '+
                                            'VALUES ( :FT_YEAR, :FT_MONTH, :PR_TIPO, :CB_CODIGO, :FE_INCIDE, :FE_FECHA1, :FE_FECHA2 )';

        AGREGA_INFO_CALC_FON = 'INSERT INTO FON_TOT ( FT_YEAR, FT_MONTH, PR_TIPO, FT_STATUS, FT_TAJUST  )' +
                               'VALUES ( :FT_YEAR, :FT_MONTH, :PR_TIPO, :FT_STATUS, :FT_TAJUST )';

        DELETE_INFO_CREDITO = 'delete from FON_TOT where ( FT_YEAR = :FT_YEAR and FT_MONTH = :FT_MONTH and PR_TIPO = :PR_TIPO )';

   begin
        with oZetaProvider do
        begin
             oQryIncapa := CreateQuery( INFO_INCAPACIDADES );
             //ParamAsDate( oQryIncapa, 'IN_FEC_INI', dFechaReferencia );
            // ParamAsDate( oQryIncapa, 'IN_FEC_FIN', dFechaFinalMes );

             oUpdInfoCalcFon := CreateQuery ( UPDATE_INFO_CALC_FON );
             SetCamposProceso( oUpdInfoCalcFon );

             oInsInfoCred := CreateQuery( AGREGA_INFO_CREDITO );
             SetCamposProceso( oInsInfoCred );

             oInsInfoEmpCred := CreateQuery( AGREGA_INFO_EMP_CRED );
             SetCamposProceso( oInsInfoEmpCred );

             oInsInfoCalcFon := CreateQuery( AGREGA_INFO_CALC_FON );
             SetCamposProceso( oInsInfoCalcFon );

             oDelInfoCredito := CreateQuery ( DELETE_INFO_CREDITO );
             SetCamposProceso( oDelInfoCredito );

             oUpdInfoEmpCred := CreateQuery( UPDATE_INFO_EMP_CRED );
             SetCamposProceso( oUpdInfoEmpCred );

       end;
   end;

   procedure DespreparaCalculoFonacot;
   begin
        FreeAndNil( oQryIncapa );
        FreeAndNil( oInsInfoCred );
        FreeAndNil( oInsInfoEmpCred );
        FreeAndNil( oInsInfoCalcFon );
        FreeAndNil( oUpdInfoCalcFon );
        FreeAndNil( oDelInfoCredito );
        FreeAndNil( oUpdInfoEmpCred );
   end;

   function GetIncidencias: Integer;
   var
      iDias, iIncapa: Integer;
      dFechaInicial, dFechaFinal, dFechaTempFin: TDate;
      //lEsPrimera: Boolean;

      procedure SetResultado;
      begin
           Result := iDias;
           with oCalculoFonacot do
           begin
                FechaIncidencia := dFechaInicial;
                FechaFinalIncapa := dFechaFinal;
           end;
      end;

   begin
        with oZetaProvider do
        begin
             dFechaTempFin:= NullDateTime;
             dFechaInicial:= NullDateTime;
             dFechaFinal := NullDateTime;
             iDias:= 0;
             iIncapa:= 0;
             SetResultado;
             ParamAsInteger( oQryIncapa, 'CB_CODIGO', iEmpleado );
             ParamAsDate( oQryIncapa, 'IN_FEC_INI', oCalculoFonacot.FechaReferencia );
             ParamAsDate( oQryIncapa, 'IN_FEC_FIN', oCalculoFonacot.FechaFinalMes );

            // Ejecuta( oQryIncapa );
             with oQryIncapa do
             begin
                  Active:= TRUE;
                  if not IsEmpty then
                  begin
                       First;
                      // lEsPrimera := TRUE;
                       while not EOF do
                       begin
                            //Lleva el control de las incapacidades
                            Inc ( iIncapa );
                            if ( dFechaTempFin = FieldByName('IN_FEC_INI').AsDateTime ) then
                            begin
                                 //Significa que va a sumar una con otra y por lo tanto la va a tomar como UNA sola
                                 Dec ( iIncapa );
                                 iDias:= iDias + FieldByName('IN_DIAS').AsInteger;
                                 dFechaFinal:= FieldByName('IN_FEC_FIN').AsDateTime;
                            end
                            else
                            begin
                                 if ( dFechaTempFin <> NullDateTime ) then
                                 begin
                                      if ( oCalculoFonacot.ReportarPrimera ) {and lEsPrimera} then
                                      begin
                                           SetResultado;
                                           Break;
                                      end
                                      else if {( not lReportarPrimera ) and} ( Result < iDias ) then
                                      begin
                                           SetResultado;
                                      end;
                                 end;
                                 iDias:= FieldByName('IN_DIAS').AsInteger;
                                 dFechaInicial:= FieldByName('IN_FEC_INI').AsDateTime;
                                 dFechaFinal:= FieldByName('IN_FEC_FIN').AsDateTime;
                            end;
                            dFechaTempFin:= FieldByName('IN_FEC_FIN').AsDateTime;
                            Next;
                       end;
                       if ( ( not oCalculoFonacot.ReportarPrimera ) and ( Result < iDias ) ) or
                          ( ( oCalculoFonacot.ReportarPrimera ) and ( iIncapa = 1  ) ) then
                          SetResultado;


                  end;
                  Active:= FALSE;
             end;
        end;

   end;

   function BajaEnMes( const iEmpleadoTemp: Integer ): Boolean;
   const
        K_STATUS_EMPLEADO_KARDEX  = 'select CB_FECHA from kardex WHERE CB_CODIGO = :CB_CODIGO AND CB_TIPO = :CB_TIPO AND CB_FECHA between :CB_FECHA_INI and :CB_FECHA_FIN';
   begin
        try
           with oFechaBajaMes do
           begin
                with oZetaProvider do
                begin
                     oFechaBajaMes:= CreateQuery ( K_STATUS_EMPLEADO_KARDEX );
                     ParamAsInteger( oFechaBajaMes, 'CB_CODIGO', iEmpleadoTemp );
                     ParamAsString( oFechaBajaMes, 'CB_TIPO', K_T_BAJA );
                     ParamAsDate( oFechaBajaMes, 'CB_FECHA_INI', oCalculoFonacot.FechaReferencia );
                     ParamAsDate( oFechaBajaMes, 'CB_FECHA_FIN', oCalculoFonacot.FechaFinalMes );
                end;
                Active:= TRUE;
                Result:= not IsEmpty;
                if Result then
                begin
                     oCalculoFonacot.FechaIncidencia:= FieldByName('CB_FECHA').AsDateTime;
                end;
                Active:= FALSE;
           end;
        finally
               FreeAndNil( oFechaBajaMes );
        end;
   end;

   procedure InsertaInfoEmpCred;
   begin
        //if ( oZetaCreator.Queries.GetStatusActEmpleado( iEmpleado, dFechaFinalMes ) = steBaja ) and ( BajaEnMes ) then
        //Si esta dado de baja y la baja es en el mes toma nota de la inicidencia
        with oCalculoFonacot do
        begin
             if ( not ( EsActivo ) and ( EsBajaEnMes ) ) then
             begin
                  sIncidencia:= K_INCIDENCIA_BAJA;
                  Inc( oTotalesEmpresa.Bajas );
             end
             else if ( ReportarIncapa ) and ( GetIncidencias <> 0 ) then
             begin
                  sIncidencia:= K_INCIDENCIA_INCAPA;
                  Inc( oTotalesEmpresa.Incapacidades );
             end
             else
                 sIncidencia:= K_NINGUNA_INCIDENCIA;
        end;

        with oZetaProvider do
        begin
             with oInsInfoEmpCred do
             begin
                  ParamAsInteger( oInsInfoEmpCred, 'CB_CODIGO', iEmpleado );
                  ParamAsString( oInsInfoEmpCred, 'FE_INCIDE', sIncidencia );
                  ParamAsDate( oInsInfoEmpCred, 'FE_FECHA1', oCalculoFonacot.FechaIncidencia );
                  ParamAsDate( oInsInfoEmpCred, 'FE_FECHA2', oCalculoFonacot.FechaFinalIncapa );
                  Ejecuta( oInsInfoEmpCred )
             end;
        end;
        InicializaFechas;
        Inc( oTotalesEmpresa.Empleados );
   end;

   procedure DelCalculosAnt;
   begin

        with oZetaProvider do
        begin
             oZetaProvider.Ejecuta( oDelInfoCredito );
        end;
   end;

   procedure InsertaInfoCalcFon;
   begin
        with oZetaProvider do
        begin
             ParamAsInteger ( oInsInfoCalcFon , 'FT_STATUS', Ord( slSinCalcular ) );
             ParamAsString ( oInsInfoCalcFon , 'FT_TAJUST', sTPrestaAjus );
             Ejecuta ( oInsInfoCalcFon );
        end;
   end;

   procedure InsertaInfoCredito;
   begin
        with oZetaProvider do
        begin
             if ( ( rMontoRetencion + rMontoCargoAbono  ) >= 0 ) then
             begin
                  ParamAsInteger( oInsInfoCred, 'CB_CODIGO', iEmpleado );
                  ParamAsString( oInsInfoCred, 'PR_REFEREN', sReferencia );
                  ParamAsFloat( oInsInfoCred, 'FC_RETENC', rMontoMensual );
                  ParamAsFloat( oInsInfoCred, 'FC_NOMINA', rMontoRetencion );
                  ParamAsFloat( oInsInfoCred, 'FC_AJUSTE', rMontoCargoAbono );
                  Ejecuta ( oInsInfoCred );
             end
             else
                 Raise EDataBaseError.Create( Format( '# Cr�dito: %s - La retenci�n mensual no puede ser menor a 0',[ sReferencia ] ));
        end;

   end;

   procedure UpdateCalculoFonacot;

        function GetDetalle: String;
        var
           i, iTipo: Integer;
           sValores,sRangoIni, sRangoFin: String;
        begin
             Result:= VACIO;
             if ( oCalculoFonacot.FiltraporNom ) then
             begin
                  for i:= 0 to iMaxTiposNom - 1 do
                  begin
                       iTipo:= ZetaCommonTools.StrAsInteger( oArreglo[i][0] );
                       sRangoIni:=  oArreglo[i][1];
                       sRangoFin:= oArreglo[i][2];
                       sValores:= ObtieneElemento( lfTipoPeriodo, iTipo ) + ':  ' + sRangoIni + ' - ' + sRangoFin + CR_LF + sValores ;
                  end;
                  Result:= 'N�minas: ' + CR_LF + CR_LF + sValores;
             end;
        end;

   begin
        with oZetaProvider , oTotalesEmpresa do
        begin
             ParamAsInteger( oUpdInfoCalcFon, 'FT_STATUS', iStatusCalculo );
             ParamAsFloat( oUpdInfoCalcFon, 'FT_RETENC', Retencion );
             ParamAsFloat( oUpdInfoCalcFon, 'FT_NOMINA', Nomina );
             ParamAsFloat( oUpdInfoCalcFon, 'FT_AJUSTE', Ajuste );
             ParamAsInteger( oUpdInfoCalcFon, 'FT_CUANTOS', Creditos );
             ParamAsInteger( oUpdInfoCalcFon, 'FT_EMPLEAD', Empleados );
             ParamAsInteger( oUpdInfoCalcFon, 'FT_BAJAS', Bajas );
             ParamAsInteger( oUpdInfoCalcFon, 'FT_INCAPA', Incapacidades );
             ParamAsString( oUpdInfoCalcFon, 'FT_DETALLE', GetDetalle );
             Ejecuta( oUpdInfoCalcFon );
             Log.Evento( clbNinguno, 0, Date,
                         Format( 'Cr�ditos Procesados: %d', [ Creditos ] ),
                         Format( 'Suma de retenciones mensuales: %s'+ CR_LF + 'Suma de retenciones por n�mina: %s' + CR_LF +
                                 'Suma de ( Cargos - Abonos ) de Ajuste: %s', [ FormatFloat('#0.00', Retencion ),
                                                                                FormatFloat('#0.00', Nomina ),
                                                                                FormatFloat('#0.00', Ajuste )] ) ) ;


        end;
   end;

   procedure IniciaTotalesEmpresa;
   begin
        with oTotalesEmpresa do
        begin
             Retencion := 0;
             Nomina    := 0;
             Ajuste    := 0;
             Creditos  := 0;
             Empleados := 0;
             Bajas     := 0;
             Incapacidades := 0;
        end;
   end;

   procedure IniciaTotalesEmpleado;
   begin
        with oTotalesEmpleado do
        begin
             Retencion := 0;
             Nomina    := 0;
             Ajuste    := 0;
             Creditos  := 0;
        end;
   end;

   procedure IncrementaTotales;
   begin
        with oTotalesEmpresa do
        begin
             Retencion := Retencion + rMontoMensual;
             Nomina    := Nomina + rMontoRetencion;
             Ajuste    := Ajuste + rMontoCargoAbono;
             Inc( Creditos );
        end;

        with oTotalesEmpleado do
        begin
             Retencion := Retencion + rMontoMensual;
             Nomina    := Nomina + rMontoRetencion;
             Ajuste    := Ajuste + rMontoCargoAbono;
             Inc( Creditos );
        end;
   end;

   procedure ActualizaTotalesEmpleado;
   begin
        if ( iEmpleado <> 0 ) then
        begin
             with oZetaProvider, oTotalesEmpleado do
             begin
                  ParamAsInteger( oUpdInfoEmpCred, 'CB_CODIGO', iEmpleado );
                  ParamAsFloat( oUpdInfoEmpCred, 'FE_RETENC', Retencion  );
                  ParamAsFloat( oUpdInfoEmpCred, 'FE_NOMINA', Nomina );
                  ParamAsFloat( oUpdInfoEmpCred, 'FE_AJUSTE', Ajuste );
                  ParamAsInteger( oUpdInfoEmpCred, 'FE_CUANTOS', Creditos );
                  Ejecuta( oUpdInfoEmpCred );
             end;
        end;
   end;

   procedure InicializaCalculo;
   begin
        with DataSet do
        begin
             with oCalculoFonacot do
             begin
                  {$ifdef ANTES}
                  if ( FiltraporNom ) then
                  begin
                  {$endif}
                       FechaReferencia:=  StrAsInteger( GetParamRangosPeriodos( oArreglo, FieldByName('CB_NOMINA').AsInteger, K_PARAM_FECHA_INI, iMaxTiposNom ) );
                       FechaFinalMes:=  StrAsInteger( GetParamRangosPeriodos( oArreglo, FieldByName('CB_NOMINA').AsInteger, K_PARAM_FECHA_FIN, iMaxTiposNom ) );
                  {$ifdef ANTES}
                  end
                  else
                  begin
                       FechaReferencia:= EncodeDate( iYear, iMes, 1 );
                       FechaFinalMes:= LastDayofMonth( FechaReferencia );
                  end;
                  {$endif}
             end;
        end;
   end;


begin
     with oZetaProvider do
     begin
          if OpenProcess( prNOCalcularPagoFonacot, DataSet.RecordCount, FListaParametros ) then
          begin
               with ParamList do
               begin
                    iMes:= ParamByName('Mes').AsInteger;
                    iYear:= ParamByName('Year').AsInteger;
                    sTPrestaAjus:= ParamByName('TPrestamoAjus').AsString;
                    sTPrestamo:= ParamByName('TPrestamo').AsString;
                    with oCalculoFonacot do
                    begin
                         ReportarIncapa:= ParamByName('ReportarIncapa').AsBoolean;
                         ReportarPrimera:= ParamByName('RepPrimeraIncapa').AsBoolean;
                         FiltraporNom:= ParamByName('FiltraTipNom').AsBoolean;
                    end;
                    iMaxTiposNom:= ParamByName('MaxTiposNom').AsInteger;
                    oArreglo:= ParamByName('RangosNomina').Value;
                    InicializaFechas;
               end;

               PreparaCalculoFonacot;
               InitQueries;
               oZetaCreator.Queries.GetStatusActEmpleadoBegin;
               try
                  DelCalculosAnt;
                  InsertaInfoCalcFon;
                  iEmpleado := 0;
                  iTipoNomina:= 0;
                  IniciaTotalesEmpresa;
                  with DataSet do
                  begin
                       First;
                       EmpiezaTransaccion;
                       try
                          while not Eof and CanContinue( FieldByName('CB_CODIGO').AsInteger ) do
                          begin                                 sReferencia:= FieldByName('PR_REFEREN').AsString;
                               rMontoMensual:= FieldByName('FC_RETENC').AsFloat;
                               rMontoRetencion:= FieldByName('FC_NOMINA').AsFloat;
                               rMontoCargoAbono:= FieldByName('FC_AJUSTE').AsFloat;

                               with oCalculoFonacot do
                               begin
                                    EsEmpleadoDif:= ( iEmpleado <> FieldByName('CB_CODIGO').AsInteger );
                                    TipoNominaDif:= ( iTipoNomina <> FieldByName('CB_NOMINA').AsInteger );

                                    //Se desea registrar el cr�dito SOLAMENTE para aquellos empleados activos o dados de baja en el mes
                                    if ( EsEmpleadoDif ) then
                                    begin
                                         if ( TipoNominaDif  ) then
                                            InicializaCalculo;
                                         EsActivo:= ( oZetaCreator.Queries.GetStatusActEmpleado( FieldByName('CB_CODIGO').AsInteger, FechaFinalMes + 1 ) <> steBaja );
                                         EsBajaEnMes:= ( BajaEnMes( FieldByName('CB_CODIGO').AsInteger ) );
                                    end;
                                    if ( EsActivo or EsBajaEnMes ) then
                                    begin
                                        // EmpiezaTransaccion;
                                         if ( EsEmpleadoDif  ) then
                                         begin
                                              ActualizaTotalesEmpleado; //Actualiza cuando cambia de empleado los totales del empleado anterior
                                              iEmpleado:= FieldByName('CB_CODIGO').AsInteger;
                                              iTipoNomina:= FieldByName('CB_NOMINA').AsInteger;
                                              InsertaInfoEmpCred;
                                              IniciaTotalesEmpleado;
                                         end;
                                         InsertaInfoCredito;
                                         IncrementaTotales;
                                        // TerminaTransaccion( True );
                                    end;
                               end;
                               Next;
                          end;
                          TerminaTransaccion( True );
                       except
                            on Error: Exception do
                            begin
                                 RollBackTransaccion;
                                 Log.Excepcion( iEmpleado, 'Error Al Calcular Pago de Fonacot', Error );
                            end;
                       end;
                       iStatusCalculo:= aStatusCalculo[EOF];
                       EmpiezaTransaccion;
                       try
                          ActualizaTotalesEmpleado; //Al final actualiza el �ltimo registro
                          UpdateCalculoFonacot;
                          TerminaTransaccion( TRUE );
                       except
                             on Error: Exception do
                             begin
                                  TerminaTransaccion( False );
                                  Log.Excepcion( 0, 'Error al actualizar los totales de Fonacot', Error );
                             end;
                       end;
                  end;
               finally
                      oZetaCreator.Queries.GetStatusActEmpleadoEnd;
                      DespreparaCalculoFonacot;
               end;
          end;
          Result := CloseProcess;
     end;
end;

procedure TdmServerNominaTimbrado.CalcularPagoFonacotParametros;
var
   sListaParamsNom: String;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          {
          if ( ParamByName('FiltraTipNom').AsBoolean ) then
             sListaParamsNom:= K_PIPE + 'Tipo de N�mina:' + ObtieneElemento( lfTipoPeriodo, ParamByName('TipoNom').AsInteger )  +
                               K_PIPE + 'Rango Inicial:' + ParamByName('RangoIni').AsString +
                               K_PIPE + 'Rango Final:' + ParamByName('RangoFin').AsString;    }

          FListaParametros := 'A�o: ' + ParamByName( 'Year' ).AsString +
                              K_PIPE + 'Mes: ' + ObtieneElemento( lfMeses, ParamByName( 'Mes' ).AsInteger - GetOffSet( lfMeses ) ) +
                              K_PIPE + 'Tipo Pr�stamo Fonacot: ' + ParamByName('TPrestamo').AsString +
                              K_PIPE + 'Tipo Pr�stamo Ajuste: ' + ParamByName('TPrestamoAjus').AsString +
                              K_PIPE + 'Ajuste Anterior: ' + BoolToSiNo( ParamByName('ReportarIncapa').AsBoolean ) +
                              K_PIPE + 'Incluir Incapacitados: ' + BoolToSiNo( ParamByName('RepPrimeraIncapa').AsBoolean ) + sListaParamsNom;
     end;
end;

function TdmServerNominaTimbrado.GetSaldoFonacotAnt( const rTotalAbonoFonacot, rMontoRetenido, rSaldoAjuste, rMontoAjuste: TPesos ): TPesos;
begin
     Result:= ( rTotalAbonoFonacot - rMontoRetenido ) + ( rSaldoAjuste - rMontoAjuste );
end;

function TdmServerNominaTimbrado.GetTotalesFonacot(Empresa: OleVariant; iYear, iMes: Integer): OleVariant;
var
   sPrestamoFonacot: String;
begin
     InitLog(Empresa,'GetTotalesFonacot');
     SetTablainfo( fnFonTot );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
          sPrestamoFonacot:= oZetaProvider.GetGlobalString( K_GLOBAL_FONACOT_PRESTAMO );
          if StrLleno ( sPrestamoFonacot ) then
             TablaInfo.Filtro := Format( K_FILTRO_CALCULO_FONACOT, [ iYear, iMes, EntreComillas( sPrestamoFonacot ), VACIO  ] );
          Result := GetTabla( Empresa );
     end;
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.GrabaTotalesFonacot(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     InitLog(Empresa,'GrabaTotalesFonacot');
     SetTablaInfo( fnFonTot );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
     end;
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.GetDatosFonacot(Empresa: OleVariant; iYear, iMes, iEmpleado, iTabla: Integer): OleVariant;
const
     K_FILTRO_EMPLEADO = ' and ( CB_CODIGO = %d )';
var
   sFiltro,sPrestamoFonacot: String;
begin
     InitLog(Empresa,'GetDatosFonacot');
     SetTablaInfo( eFormaNomina ( iTabla ) );
     if ( iEmpleado <> 0 ) then
     begin
          sFiltro:= Format ( K_FILTRO_EMPLEADO, [iEmpleado] );
     end;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
          sPrestamoFonacot:= oZetaProvider.GetGlobalString( K_GLOBAL_FONACOT_PRESTAMO );
          if ( StrLleno ( sPrestamoFonacot ) ) then
             TablaInfo.Filtro := Format( K_FILTRO_CALCULO_FONACOT, [ iYear, iMes, EntreComillas( sPrestamoFonacot ) , sFiltro  ] );
          Result := GetTabla( Empresa );
     end;
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.GetTNomEmpleado(Empresa: OleVariant; iEmpleado: Integer; dFecha: TDateTime): Integer;
begin
     Result:= -1;
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          FEmpleadoLee := CreateQuery( Format( GetSQLScript( Q_EMPLEADO_STATUS ), [ ZetaCommonTools.DateToStrSQLC( dFecha ), VACIO ] ) );
          try
             with FEmpleadoLee do
             begin
                  Active:= False;

                  ParamAsInteger( FEmpleadoLee, 'Empleado', iEmpleado );
                  //ParamAsDateTime( FEmpleadoLee, 'Fecha', dFecha );

                  Active:= True;
                  if not EOF then
                  begin
                       Result:= FieldByName('TU_NOMINA').AsInteger;
                  end;
             end;
          finally
                 FreeAndNil( FEmpleadoLee );
          end;
     end;
     SetComplete;
end;

function TdmServerNominaTimbrado.GetFecCambioTNom(Empresa: OleVariant; iEmpleado: Integer; dFecha: TDateTime): TDateTime;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          InitQueries;
          with  oZetaCreator.Queries do
          begin
               GetFechaCambioTNomBegin;
               try
                  Result:= GetFechaCambioTNom( iEmpleado, dFecha );
               finally
                      GetFechaCambioTNomEnd;
               end;
          end;
     end;
     SetComplete;
end;

function TdmServerNominaTimbrado.GetSimulacionesGlobales(Empresa, Params: OleVariant): OleVariant;
var
   iYear, iNumero: Integer;
begin
     InitLog(Empresa,'GetSimulacionesGlobales');
     with oZetaProvider do
     begin
          AsignaParamList( Params );
          with ParamList do
          begin
               iYear     := ParamByName( 'Year' ).AsInteger;
               iNumero   := ParamByName( 'Numero' ).AsInteger;
          end;
          Result:= OpenSQL( Empresa, Format( GetSQLScript( Q_GET_SIM_GLOBALES ), [iYear, iNumero,K_GLOBAL_SI ] ), True );
          EndLog;
          SetComplete;
     end;
end;

function TdmServerNominaTimbrado.GetTotalesConceptos(Empresa,Parametros: OleVariant; out Movimientos: OleVariant): OleVariant;
var
   iYear, iFiltroAproba, iNumero: Integer;
   sFiltro : string;
const
     K_FILTRO_SIM = 'and N.NO_APROBA = %d ';

begin
     sFiltro := VACIO;
     InitLog(Empresa,'GetTotalesConceptos');
     with oZetaProvider do
     begin
          AsignaParamList( Parametros );
          with ParamList do
          begin
               iYear     := ParamByName( 'Year' ).AsInteger;
               iNumero   := ParamByName( 'Numero' ).AsInteger;
               iFiltroAproba := ParamByName( 'Filtro' ).AsInteger;
          end;

          if iFiltroAproba > -1 then
          begin
               sFiltro := Format(K_FILTRO_SIM , [iFiltroAproba]);
          end;
          Result := OpenSQL( Empresa, Format( GetSQLScript( Q_GET_SIM_TOTALES_NOMINA ), [iYear, iNumero,K_GLOBAL_SI, sFiltro ] ), True );

          Movimientos:= OpenSQL( Empresa, Format( GetSQLScript( Q_GET_SIM_TOTALES_CONCEPTOS ), [iYear, iNumero,K_GLOBAL_SI,sFiltro ] ), True );

          EndLog;
          SetComplete;
     end;
end;


function TdmServerNominaTimbrado.GetSimulacionesAprobar(Empresa,Parametros: OleVariant): OleVariant;
var
   iYear,iNumero:Integer;
begin
     InitLog(Empresa,'GetSimulacionesAprobar');
     with oZetaProvider do
     begin
          AsignaParamList( Parametros );
          with ParamList do
          begin
               iYear     := ParamByName( 'Year' ).AsInteger;
               iNumero   := ParamByName( 'Numero' ).AsInteger;
          end;
          Result:= OpenSQL( Empresa, Format( GetSQLScript( Q_GET_SIM_A_APROBAR ), [iYear, iNumero,K_GLOBAL_SI,Ord(ssfLiquidada),Ord(spCalculadaTotal),K_GLOBAL_NO ] ), True );
          EndLog;
          SetComplete;
     end;
end;

function TdmServerNominaTimbrado.AplicacionGlobalFiniquitos(Empresa,Parametros: OleVariant): OleVariant;
var
   oPeriodoSimulacion:TDatosPeriodo;
   procedure CargarParametrosBitacora;
   begin
   with oZetaProvider.ParamList do
   begin
          FListaParametros := VACIO;
          FListaParametros := 'Periodo de Simulaci�n :'+CR_LF+
                             'A�o: ' + IntToStr( ParamByName('Year').AsInteger ) +
                              K_PIPE + 'Tipo: ' + ZetaCommonLists.ObtieneElemento( lfTipoPeriodo, ParamByName('Tipo').AsInteger ) +
                              K_PIPE + 'N�mero: ' + IntToStr( ParamByName('Numero').AsInteger ) +
                              K_PIPE + 'Re-Enviar Simulaciones Aplicadas: '+zBoolToStr(ParamByName('ReEnviarAplicadas').AsBoolean);
   end;
end;
begin
     InitLog(Empresa,'AplicacionGlobalFiniquitos');
     InitBroker;
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList(Parametros);
             CargarParametrosBitacora;
             GetDatosPeriodo;   // Asigna el periodo de la baja como activo
             if ( DatosPeriodo.Status = spAfectadaTotal ) then
                DataBaseError( 'No se puede aplicar el finiquito a una n�mina afectada' );
             with ParamList do
             begin
                  with oPeriodoSimulacion do
                  begin
                       Year := ParamByName('Year').AsInteger;
                       Numero := ParamByName('Numero').AsInteger;
                  end;
             end;
             AplicacionGlobalFiniquitosBuildDataSet;
             Result := AplicacionGlobalFiniquitosDataset( SQLBroker.SuperReporte.DataSetReporte );
        end;
     finally
            ClearBroker;
     end;
end;

procedure TdmServerNominaTimbrado.SetStatusAplicado(Empleado:Integer;const oPeriodoSimulacion: TDatosPeriodo);
var
   FUpdateAprobacion : TZetaCursor;
begin
     with oZetaProvider do
     begin
          FUpdateAprobacion := CreateQuery( Format ( GetSQLScript( Q_UPDATE_APLICADOS ),[Ord(ssfLiquidada)]) );
          with oPeriodoSimulacion do
          begin
               ParamAsInteger( FUpdateAprobacion, 'Year', Year );
               ParamAsInteger( FUpdateAprobacion, 'Tipo', Ord( Tipo ) );
               ParamAsInteger( FUpdateAprobacion, 'Numero', Numero );
               ParamAsInteger( FUpdateAprobacion, 'Empleado', Empleado );
          end;
          Ejecuta( FUpdateAprobacion );
     end;
end;


function TdmServerNominaTimbrado.AplicacionGlobalFiniquitosDataset(Dataset: TDataset): OleVariant;
var
   oPeriodoSimulacion: TDatosPeriodo;
   iEmpleado : Integer;
   iYearSim,iNumeroSim :Integer;
begin
     with oZetaProvider do
     begin
          InitGlobales;
          if OpenProcess( prEmpAplicacionFiniGlobal , Dataset.RecordCount, FListaParametros ) then
          begin
               { Ejecuci�n }
               with Dataset do
               begin
                    //Se copia el periodo de simulaci�n
                    with ParamList do
                    begin
                         iYearSim := ParamByName('Year').AsInteger;
                         iNumeroSim := GetGlobalInteger(K_GLOBAL_SIMULACION_FINIQUITOS );
                    end;

                    while not Eof and CanContinue( FieldByName('CB_CODIGO').AsInteger ) do
                    begin
                         InicializaValoresActivos;
                         iEmpleado := FieldByName('CB_CODIGO').AsInteger;
                         with ParamList do
                         begin
                              with oPeriodoSimulacion do
                              begin
                                   Year := iYearSim;
                                   Tipo := eTipoPeriodo( FieldByName('CB_NOMTIPO').AsInteger );
                                   Numero := iNumeroSim;
                              end;
                              //Datos del periodo de la baja
                              ParamByName('Year').AsInteger := FieldByName('CB_NOMYEAR').AsInteger;
                              ParamByName('Numero').AsInteger := FieldByName('CB_NOMNUME').AsInteger;
                              ParamByName('Tipo').AsInteger := FieldByName('CB_NOMTIPO').AsInteger;
                              GetDatosPeriodo;
                         end;
                         try
                            Result := AplicaFiniquitoSimulacion(iEmpleado,oPeriodoSimulacion);
                            Log.Evento(clbNinguno,iEmpleado,Now,Format('Se aplic� el Finiquito al Empleado : %d en la N�mina de Baja : A�o:%d Numero:%d Tipo:%s ',[iEmpleado,FieldByName('CB_NOMYEAR').AsInteger,FieldByName('CB_NOMNUME').AsInteger,ObtieneElemento(lfTipoPeriodo,FieldByName('CB_NOMTIPO').AsInteger) ] ) );
                         except
                               on Error: Exception do
                               begin
                                    Log.Excepcion( 0, 'Error Al Liquidar Empleado ' + GetPeriodoInfo( iYearSim, iNumeroSim, eTipoPeriodo( FieldByName('CB_NOMTIPO').AsInteger ) ), Error );
                               end;
                         end;
                         Next;
                    end;
               end;
               Result := CloseProcess;
          end;
     end;
end;

procedure TdmServerNominaTimbrado.AplicacionGlobalFiniquitosBuildDataSet;
var
   iYear,iNumero: Integer;
   sFiltro:string;
const
     K_FILTRO_LIQ_MSSQL = 'CB_CODIGO in (select CB_CODIGO from NOMINA where NO_GLOBAL = ''S'' and NO_STATUS = %d and ( NO_APROBA = %d %s ) and PE_NUMERO = %d and PE_YEAR = %d )';
begin
     with oZetaProvider do
     begin
          with SQLBroker do
          begin
               Init( enEmpleado );
               with Agente do
               begin
                    AgregaColumna( 'CB_CODIGO', True, Entidad, tgNumero, 7, 'CB_CODIGO' );
                    AgregaColumna( K_PRETTYNAME, TRUE, Entidad, tgTexto, 50, 'PrettyName' );
                    AgregaColumna( 'CB_FEC_ING', True, Entidad, tgFecha, 0, 'CB_FEC_ING' );
                    AgregaColumna( 'CB_NOMNUME', True, Entidad, tgNumero, 7, 'CB_NOMNUME' );
                    AgregaColumna( 'CB_NOMYEAR', True, Entidad, tgNumero, 7, 'CB_NOMYEAR' );
                    AgregaColumna( 'CB_NOMTIPO', True, Entidad, tgNumero, 7, 'CB_NOMTIPO' );

                    //AgregaAgenteFunciones( Agente );
                    AgregaFiltro( 'CB_ACTIVO = ''N''', True, Entidad );
                    with ParamList do
                    begin
                         iYear := ParamByName('Year').AsInteger;
                         iNumero := GetGlobalInteger( K_GLOBAL_SIMULACION_FINIQUITOS );
                         if ParamByName('ReEnviarAplicadas').AsBoolean then
                         begin
                              sFiltro := Format('or NO_APROBA = %d',[Ord(ssfLiquidada)]);
                         end;
                    end;
                    AgregaFiltro( Format( K_FILTRO_LIQ_MSSQL,[ Ord(spCalculadaTotal),Ord(ssfAprobada),sFiltro,iNumero,iYear ] ),True,enNinguno);

                    AgregaOrden( 'CB_CODIGO', TRUE, Entidad );
               end;
               AgregaRangoCondicion( ParamList );
               FiltroConfidencial( EmpresaActiva );
               BuildDataset( EmpresaActiva );
          end;
     end;
end;

function TdmServerNominaTimbrado.AplicacionGlobalFiniquitosGetLista(Empresa,Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'AplicacionGlobalFiniquitosGetLista');
     try
        InitBroker;
        with oZetaProvider do
        begin
             EmpresaActiva:= Empresa;
             AsignaParamList( Parametros );
        end;
        AplicacionGlobalFiniquitosBuildDataSet;
        Result := SQLBroker.SuperReporte.GetReporte;
     finally
            ClearBroker;
     end;
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.AplicacionGlobalFiniquitosLista(Empresa, Lista,
  Parametros: OleVariant): OleVariant;
var
   oPeriodoSimulacion:TDatosPeriodo;
   procedure CargarParametrosBitacora;
   begin
   with oZetaProvider.ParamList do
   begin
          FListaParametros := VACIO;
          FListaParametros := 'Periodo de Simulaci�n :'+CR_LF+
                             'A�o: ' + IntToStr( ParamByName('Year').AsInteger ) +
                              K_PIPE + 'Tipo: ' + ZetaCommonLists.ObtieneElemento( lfTipoPeriodo, ParamByName('Tipo').AsInteger ) +
                              K_PIPE + 'N�mero: ' + IntToStr( ParamByName('Numero').AsInteger ) +
                              K_PIPE + 'Re-Enviar Simulaciones Aplicadas: '+zBoolToStr(ParamByName('ReEnviarAplicadas').AsBoolean);
   end;
end;
begin
     InitLog(Empresa,'AplicacionGlobalFiniquitos');
     InitBroker;
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList(Parametros);
             CargarParametrosBitacora;
             GetDatosPeriodo;   // Asigna el periodo de la baja como activo
             if ( DatosPeriodo.Status = spAfectadaTotal ) then
                DataBaseError( 'No se puede aplicar el finiquito a una n�mina afectada' );
             with ParamList do
             begin
                  with oPeriodoSimulacion do
                  begin
                       Year := ParamByName('Year').AsInteger;
                       Numero := ParamByName('Numero').AsInteger;
                  end;
             end;
             cdsLista.Lista := Lista;
             Result := AplicacionGlobalFiniquitosDataset( cdsLista );
        end;
     finally
            ClearBroker;
     end;


end;


function TdmServerNominaTimbrado.GetDeclaracion(Empresa: OleVariant): OleVariant;
begin
     InitLog(Empresa,'GetDeclaracion');
     SetTablaInfo(fnDeclaracion);
     with oZetaProvider do
     begin
          Result := GetTabla( Empresa );
     end;
     EndLog;SetComplete;
end;


procedure TdmServerNominaTimbrado.TimbrarNominasBuildDataset( sRS_CODIGO : string; statusAnterior, statusNuevo : eStatusTimbrado; sOrden  : string );
var
   slGrupoOrden : TStringList;
   sCampoGrupo, sCampoOrden, sQueryOrderBy : string;

   procedure GetGrupoOrden( sTexto : string;  var sCampoGrupo, sCampoOrden, sOrderBy : string  );
   var
     slGrupoOrden : TStringList;
   begin

       slGrupoOrden := TStringList.Create;
       try
          slGrupoOrden.CommaText := sTexto;
          if (slGrupoOrden.Count = 1 ) then
          begin
               sCampoGrupo := slGrupoOrden[0];
               sCampoOrden := 'NOMINA.CB_CODIGO';
          end
          else
          if ( slGrupoOrden.Count = 2 ) then
          begin
               sCampoGrupo := slGrupoOrden[0];
               sCampoOrden := slGrupoOrden[1];
          end
          else
          begin
              sCampoGrupo := 'NOMINA.CB_NIVEL1';
              sCampoOrden := 'NOMINA.CB_CODIGO';
          end;
       finally
              FreeAndNil( slGrupoOrden );
       end ;

      if ( sCampoGrupo <> sCampoOrden ) then
            sOrderBY := sCampoGrupo + ','+ sCampoOrden
      else
          sOrderBY := sCampoGrupo;

   end;

begin

     GetGrupoOrden( sOrden, sCampoGrupo, sCampoOrden, sQueryOrderBy );

     oZetaProvider.InitGlobales;
     with SQLBroker do
     begin
          Init( enNomina );
          with Agente do
          begin
               AgregaColumna( 'NOMINA.CB_CODIGO', True, Entidad, tgNumero, 0, 'Empleado' );
               if( oZetaProvider.GetGlobalBooleano( K_GLOBAL_TIMBRADO_EMP_TRANSFERIDOS_1 ))then
               begin
                   AgregaColumna( 'V_EMP_TIMB.PRETTYNAME', TRUE, enEmpTimb, tgTexto, 50, 'Nombre' );
				   AgregaColumna( 'V_EMP_TIMB.CB_CURP', TRUE, enEmpleado, tgTexto, 30, 'CURP' );
               end
               else
               begin
                  AgregaColumna( K_PRETTYNAME, TRUE, enEmpleado, tgTexto, 50, 'Nombre' );
				  AgregaColumna( 'COLABORA.CB_CURP', TRUE, enEmpleado, tgTexto, 30, 'CURP' );
               end;
               if StrVacio( sOrden ) then
                  AgregaColumna( 'NOMINA.CB_CODIGO', True, Entidad, tgTexto, 10, 'Orden' )
               else
               begin
                  AgregaColumna( sCampoOrden, True, Entidad, tgTexto, 10, 'Orden' );
               end;

               AgregaColumna( sCampoGrupo, True, Entidad, tgTexto, 10, 'Grupo' );

               with oZetaProvider.DatosPeriodo do
               begin
                    AgregaFiltro( Format( '( PE_YEAR = %d )', [ Year ] ), True, Entidad );
                    AgregaFiltro( Format( '( PE_TIPO = %d )', [ Ord(Tipo) ] ), True, Entidad );
                    AgregaFiltro( Format( '( PE_NUMERO = %d )', [ Numero ] ), True, Entidad );
                    AgregaFiltro( Format( '( NO_STATUS = %d )', [ Ord( spAfectadaTotal ) ] ), True, Entidad );
//                    AgregaFiltro( Format( '( NO_TIMBRO = %d )', [ Ord( statusAnterior ) ] ), True, Entidad );
                    AgregaFiltro( Format( '( NO_TIMBRO IN ( %d, %d ) )', [ Ord( statusAnterior ), Ord( estiCancelacionPendiente ) ] ), True, Entidad );
                    AgregaFiltro( Format( '( RPATRON.RS_CODIGO = ''%s'' )', [ sRS_CODIGO ] ), True,  enRPatron);
               end;

               AgregaOrden( sQueryOrderBy, TRUE, Entidad );
               {if StrVacio( sOrden ) or ( sOrden = 'NOMINA.CB_CODIGO' )  then
                  AgregaOrden( 'NOMINA.CB_CODIGO', TRUE, Entidad )
               else
               begin
                  AgregaOrden( sOrden + ',NOMINA.CB_CODIGO', TRUE, Entidad );
               end;}

          end;
          AgregaRangoCondicion( oZetaProvider.ParamList );
          if( oZetaProvider.GetGlobalBooleano( K_GLOBAL_TIMBRADO_EMP_TRANSFERIDOS_1 )) then
          begin
                FiltroConfidencialTransferidos( oZetaProvider.EmpresaActiva );
          end
          else
          begin
                FiltroConfidencial( oZetaProvider.EmpresaActiva );
          end;
          BuildDataset( oZetaProvider.EmpresaActiva );
     end;
 end;

function TdmServerNominaTimbrado.TimbrarNominasGetLista(Empresa,
  Parametros: OleVariant): OleVariant;
var
   statusAnterior, statusNuevo : eStatusTimbrado;
   sRazonSocial, sOrden : string;
begin
  InitLog(Empresa,'TimbrarNominasGetLista');
     try
        InitBroker;
        with oZetaProvider do
        begin
             EmpresaActiva:= Empresa;
             AsignaParamList( Parametros );
             GetDatosPeriodo;
             statusAnterior := eStatusTimbrado( ParamList.ParamByName( 'StatusAnterior').Asinteger ) ;
             statusNuevo := eStatusTimbrado( ParamList.ParamByName( 'StatusNuevo').Asinteger ) ;
             sRazonSocial := ParamList.ParamByName( 'RazonSocial').AsString;
             sOrden := ParamList.ParamByName( 'Orden' ).AsString;

        end;
        TimbrarNominasBuildDataset( sRazonSocial, statusAnterior, statusNuevo, sOrden);
        Result := SQLBroker.SuperReporte.GetReporte;
     finally
            ClearBroker;
     end;
     EndLog;SetComplete;
end;


function TdmServerNominaTimbrado.TimbrarNominasListaDataset(Dataset: TDataset): OleVariant;
var
   oPeriodoTimbrado: TDatosPeriodo;
   iEmpleado : Integer;
   iFolio, iUsuario:Integer;
   sRFC,sFolioUUID, sTimbre : string;
   FTimbrar: TZetaCursor;
   lOK : boolean;
   statusActual, statusNuevo : eStatusTimbrado;
begin
     FTimbrar := nil;
     with oZetaProvider do
     begin
          InitGlobales;
          if OpenProcess( prNoTimbrarNomina , Dataset.RecordCount, FListaParametros ) then
          begin
               { Ejecuci�n }
               with Dataset do
               begin

                     with ParamList do
                     begin
                          with oPeriodoTimbrado do
                          begin
                               Year := ParamByName('Year').AsInteger;
                               Tipo := eTipoPeriodo( ParamByName( 'Tipo').AsInteger );
                               Numero := ParamByName('Numero').AsInteger;
                          end;
                          statusActual := eStatusTimbrado(  ParamByName('StatusAnterior').AsInteger ) ;
                          statusNuevo := eStatusTimbrado(  ParamByName('StatusNuevo').AsInteger ) ;
                     end;

               iUsuario := UsuarioActivo;
               //'execute procedure SP_TIMBRAR_EMPLEADO( :Year, :Tipo, :Numero, :Empleado, :RFC,  :Status, :Timbre, :Folio, :Usuario )';
               FTimbrar := CreateQuery( GetSQLScript( Q_TIMBRAR_EMPLEADO ) );
               lOK := FALSE;
                    while not Eof and CanContinue( FieldByName('NUMERO').AsInteger ) do
                    begin

                         iEmpleado := FieldByName('NUMERO').AsInteger;
                         //sRFC := FieldByName('RFC').AsString;
                         sRFC := VACIO;
                         if statusNuevo = estiTimbrado then
                         begin
                              sTimbre :=  FieldByName('SELLO').AsString;
                              iFolio :=  FieldByName('FACTURAID').AsInteger;
                              sFolioUUID := FieldByName('FACTURAUUID').AsString;
                         end
                         else
                         begin
                              sTimbre := VACIO;
                              iFolio := 0;
                         end;
                         EmpiezaTransaccion;
                         try
                            ParamAsInteger( FTimbrar, 'Year', oPeriodoTimbrado.Year );
                            ParamAsInteger( FTimbrar, 'Tipo', Ord( oPeriodoTimbrado.Tipo )  );
                            ParamAsInteger( FTimbrar, 'Numero', oPeriodoTimbrado.Numero );
                            ParamAsInteger( FTimbrar, 'Empleado', iEmpleado );
                            ParamAsString( FTimbrar, 'RFC' , sRFC ) ;
                            ParamAsInteger( FTimbrar, 'Status', ord( statusNuevo ) );
                            ParamAsString( FTimbrar, 'Timbre', sTimbre );
                            ParamAsInteger( FTimbrar, 'Folio', iFolio);
                            ParamAsInteger( FTimbrar, 'Usuario', iUsuario);
                            ParamAsString(ftimbrar,'FacturaUUID',sFolioUUID);
                            Ejecuta( FTimbrar );
                            TerminaTransaccion( True );
                            lOk := True;
                         except
                               on Error: Exception do
                               begin
                                    TerminaTransaccion( False );
                                    Log.Excepcion( 0, 'Error Al Timbrar/Cancelar Timbrado Nomina ', Error, DescripcionParams );
                                    lOk := False;
                               end;
                         end;

                         try

                            if ( lOK ) then
                            begin
                                 Log.Evento(clbNinguno,iEmpleado,Now,Format('Se Actualiz� el Status de Timbrado:  %s , Folio Timbrado #%d la Nomina al Empleado : %d , A�o:%d Numero:%d Tipo:%s ',[ObtieneElemento(lfstatusTimbrado,ord( statusNuevo )),iFolio,  iEmpleado,oPeriodoTimbrado.Year, oPeriodoTimbrado.NUmero,ObtieneElemento(lfTipoPeriodo,ord( oPeriodoTimbrado.Tipo))  ] ) );
                            end;

                         except
                               on Error: Exception do
                               begin
                                    Log.Excepcion( 0, 'Error Al Timbra N�mina de Empleado ' + GetPeriodoInfo(oPeriodoTimbrado.Year, oPeriodoTimbrado.NUmero, oPeriodoTimbrado.Tipo ),Error) ;
                               end;
                         end;
                         Next;
                    end;
               end;

               FreeAndNil( FTimbrar );

               //Afectar Status del Periodo
               FTimbrar := CreateQuery( GetSQLScript( Q_TIMBRAR_PERIODO  ) );
               EmpiezaTransaccion;
               try
                  ParamAsInteger( FTimbrar, 'Year', oPeriodoTimbrado.Year );
                  ParamAsInteger( FTimbrar, 'Tipo', Ord( oPeriodoTimbrado.Tipo )  );
                  ParamAsInteger( FTimbrar, 'Numero', oPeriodoTimbrado.Numero );
                  ParamAsInteger( FTimbrar, 'StatusActual', ord( statusActual ) );
                  ParamAsInteger( FTimbrar, 'StatusNuevo', ord( statusNuevo ) );

                  Ejecuta( FTimbrar );
                  TerminaTransaccion( True );

               except
                     on Error: Exception do
                     begin
                          TerminaTransaccion( False );
                          Log.Excepcion( 0, 'Error Al  Calcular Status de  Timbrado', Error, DescripcionParams );

                     end;
               end;

               FreeAndNil( FTimbrar );

               Result := CloseProcess;
          end;
     end;
end;

function TdmServerNominaTimbrado.TimbrarNominasLista(Empresa, Lista,
  Parametros: OleVariant): OleVariant;
var
   oPeriodoTimbrado:TDatosPeriodo;
   procedure CargarParametrosBitacora;
   begin
   with oZetaProvider.ParamList do
   begin
          FListaParametros := VACIO;
          FListaParametros := 'Periodo de Timbrado :'+CR_LF+
                             'A�o: ' + IntToStr( ParamByName('Year').AsInteger ) +
                              K_PIPE + 'Nuevo Status: ' + ZetaCommonLists.ObtieneElemento( lfStatusTimbrado, ParamByName('StatusNuevo').AsInteger ) +
                              K_PIPE + 'Tipo: ' + ZetaCommonLists.ObtieneElemento( lfTipoPeriodo, ParamByName('Tipo').AsInteger ) +
                              K_PIPE + 'N�mero: ' + IntToStr( ParamByName('Numero').AsInteger ) +
                              K_PIPE + 'Raz�n Social: '+ ParamByName('RazonSocial').AsString  +
                              K_PIPE + 'Contribuyente ID: '+ ParamByName('ContribuyenteID').AsString ;
   end;
end;
begin
     InitLog(Empresa,'TimbrarNominasLista');
     InitBroker;
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList(Parametros);
             InitArregloTPeriodo; //@(am):Tipos de periodo
             CargarParametrosBitacora;
             GetDatosPeriodo;   // Asigna el periodo de la baja como activo
             if ( DatosPeriodo.Status < spAfectadaParcial ) then
                DataBaseError( 'La nomina debe estar Afectada para poder Timbrar/Cancelar Timbrado' );
             with ParamList do
             begin
                  with oPeriodoTimbrado do
                  begin
                       Year := ParamByName('Year').AsInteger;
                       Tipo := eTipoPeriodo( ParamByName( 'Tipo').AsInteger);
                       Numero := ParamByName('Numero').AsInteger;
                  end;
             end;
             cdsLista.Lista := Lista;
             Result := TimbrarNominasListaDataset( cdsLista );
        end;
     finally
            ClearBroker;
     end;

end;

function TdmServerNominaTimbrado.CancelarTimbrado(Empresa, Parametros: OleVariant): OleVariant;
  var
   oPeriodoTimbrado:TDatosPeriodo;

  procedure CancelarTimbradoBuildDataset;
  begin
     with SQLBroker do
     begin
          Init( enNomina );
          with Agente do
          begin
               AgregaColumna( 'CB_CODIGO', True, enNomina, tgNumero, 0, 'NUMERO' );
               with oZetaProvider.DatosPeriodo do
               begin
                    AgregaFiltro( Format( '( PE_YEAR = %d )', [ Year ] ), True, Entidad );
                    AgregaFiltro( Format( '( PE_TIPO = %d )', [ Ord(Tipo) ] ), True, Entidad );
                    AgregaFiltro( Format( '( PE_NUMERO = %d )', [ Numero ] ), True, Entidad );
                    AgregaFiltro( Format( '( NO_STATUS = %d )', [ Ord( spAfectadaTotal ) ] ), True, Entidad );
                    AgregaFiltro( Format( '( NO_TIMBRO = %d )', [ Ord( estiTimbrado ) ] ), True, Entidad );
               end;
               AgregaOrden( 'CB_CODIGO', TRUE, Entidad );
          end;
          //AgregaRangoCondicion( oZetaProvider.ParamList );
          //FiltroConfidencial( oZetaProvider.EmpresaActiva );
          BuildDataset( oZetaProvider.EmpresaActiva );
     end;
 end;



   procedure CargarParametrosBitacora;
   begin
       with oZetaProvider.ParamList do
       begin
              FListaParametros := VACIO;
              FListaParametros := 'Periodo de Timbrado :'+CR_LF+
                                 'A�o: ' + IntToStr( ParamByName('Year').AsInteger ) +
                                  K_PIPE + 'Nuevo Status: ' + ZetaCommonLists.ObtieneElemento( lfStatusTimbrado, ParamByName('StatusNuevo').AsInteger ) +
                                  K_PIPE + 'Tipo: ' + ZetaCommonLists.ObtieneElemento( lfTipoPeriodo, ParamByName('Tipo').AsInteger ) +
                                  K_PIPE + 'N�mero: ' + IntToStr( ParamByName('Numero').AsInteger ) +
                                  K_PIPE + 'Contribuyente ID: '+ ParamByName('ContribuyenteID').AsString ;
       end;
   end;

begin
     InitLog(Empresa,'CancelarTimbrado');
     try
        InitBroker;
        with oZetaProvider do
        begin
             EmpresaActiva:= Empresa;
             AsignaParamList( Parametros );
             ParamList.AddInteger('StatusAnterior', ord(estiTimbrado)  );
             ParamList.AddInteger('StatusNuevo',ord(estiPendiente)  );
             CargarParametrosBitacora;
             GetDatosPeriodo;

             if ( DatosPeriodo.Status < spAfectadaParcial ) then
                   DataBaseError( 'La nomina debe estar Afectada para poder Cancelar Timbrado' );

             with ParamList do
             begin
                  with oPeriodoTimbrado do
                  begin
                       Year := ParamByName('Year').AsInteger;
                       Tipo := eTipoPeriodo( ParamByName( 'Tipo').AsInteger);
                       Numero := ParamByName('Numero').AsInteger;
                  end;
             end;

             CancelarTimbradoBuildDataset;
             cdsLIsta.Lista := SQLBroker.SuperReporte.GetReporte;

             Result := TimbrarNominasListaDataset( cdsLista );
        end;

     finally
            ClearBroker;
     end;
     EndLog;SetComplete;

end;

function TdmServerNominaTimbrado.MarcarTimbrado(Empresa,
  Parametros: OleVariant): OleVariant;
var
   oPeriodoTimbrado:TDatosPeriodo;
   statusActual, statusNuevo : eStatusTimbrado;
    FTimbrar: TZetaCursor;

   procedure CargarParametrosBitacora;
   begin
   with oZetaProvider.ParamList do
   begin
          FListaParametros := VACIO;
          FListaParametros := 'Periodo de Timbrado :'+CR_LF+
                             'A�o: ' + IntToStr( ParamByName('Year').AsInteger ) +
                              K_PIPE + 'Nuevo Status: ' + ZetaCommonLists.ObtieneElemento( lfStatusTimbrado, ParamByName('StatusNuevo').AsInteger ) +
                              K_PIPE + 'Tipo: ' + ZetaCommonLists.ObtieneElemento( lfTipoPeriodo, ParamByName('Tipo').AsInteger ) +
                              K_PIPE + 'N�mero: ' + IntToStr( ParamByName('Numero').AsInteger ) +
                              K_PIPE + 'Raz�n Social: '+ ParamByName('RazonSocial').AsString  +
                              K_PIPE + 'Contribuyente ID: '+ ParamByName('ContribuyenteID').AsString ;
   end;
end;
begin
     InitLog(Empresa,'MarcarTimbrado');
     InitBroker;
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList(Parametros);
             CargarParametrosBitacora;
             GetDatosPeriodo;
             if ( DatosPeriodo.Status < spAfectadaParcial ) then
                DataBaseError( 'La nomina debe estar Afectada para poder Timbrar/Cancelar Timbrado' );
             with ParamList do
             begin
                  with oPeriodoTimbrado do
                  begin
                       Year := ParamByName('Year').AsInteger;
                       Tipo := eTipoPeriodo( ParamByName( 'Tipo').AsInteger);
                       Numero := ParamByName('Numero').AsInteger;
                  end;

                     statusActual := eStatusTimbrado(  ParamByName('StatusAnterior').AsInteger ) ;
                          statusNuevo := eStatusTimbrado(  ParamByName('StatusNuevo').AsInteger ) ;
             end;

            //Afectar Status del Periodo
             FTimbrar := CreateQuery( GetSQLScript( Q_TIMBRAR_PERIODO  ) );
               EmpiezaTransaccion;
               try
                  ParamAsInteger( FTimbrar, 'Year', oPeriodoTimbrado.Year );
                  ParamAsInteger( FTimbrar, 'Tipo', Ord( oPeriodoTimbrado.Tipo )  );
                  ParamAsInteger( FTimbrar, 'Numero', oPeriodoTimbrado.Numero );
                  ParamAsInteger( FTimbrar, 'StatusActual', ord( statusActual ) );
                  ParamAsInteger( FTimbrar, 'StatusNuevo', ord( statusNuevo ) );

                  Ejecuta( FTimbrar );
                  TerminaTransaccion( True );

               except
                     on Error: Exception do
                     begin
                          TerminaTransaccion( False );
                          Log.Excepcion( 0, 'Error Al  Calcular Status de  Timbrado', Error, DescripcionParams );

                     end;
               end;

               FreeAndNil( FTimbrar );

        end;
     finally
            ClearBroker;
     end;
end;

function TdmServerNominaTimbrado.GetFoliosPendientes(EMPRESA, Parametros: OleVariant): OleVariant;

var
fechaini,FechaFin: Tdate;
const
  {$ifdef INTERBASE}
  K_GETFOLIOSPENDIENTES = 'select NO_FACTURA from FN_GetFacturasPendientes( %s, %s )';
  {$endif}
  {$ifdef MSSQL}
  K_GETFOLIOSPENDIENTES = 'select NO_FACTURA from FN_GetFacturasPendientes( %s, %s )';
  {$endif}
begin
     InitLog(Empresa,'GetFoliosPendientes');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
         AsignaParamList( Parametros );
          with ParamList do
          begin
               fechaini     := ParamByName('FechaIni').AsDate;
               FechaFin     := ParamByName('FechaFin').AsDate;
          end;
           Result := OpenSQL(Empresa, format(K_GETFOLIOSPENDIENTES,[DateToStrSqlC(fechaini),DateToStrsqlC(fechafin)]),true);
     end
end;

function TdmServerNominaTimbrado.ActualizaFoliosFiscales(Empresa, Parameters: OleVariant): OleVariant;

var
FolioID,FolioUUID: string;
const
  {$ifdef INTERBASE}
  Q_UPDATEFOLIOSPENDIENTES = 'SP_ACTUALIZA_FACTURA_UUID %s, %s ';
  {$endif}
  {$ifdef MSSQL}
  Q_UPDATEFOLIOSPENDIENTES = 'SP_ACTUALIZA_FACTURA_UUID %s, ''%s'' ';
  {$endif}
begin
     InitLog(Empresa,'ActualizaFoliosFiscales');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
         AsignaParamList( Parameters );
          with ParamList do
          begin
               FolioID     := ParamByName('FacturaID').Asstring;
               FolioUUID     := ParamByName('FacturaUUID').Asstring;
          end;
          try
            OpenSQL(Empresa, format(Q_UPDATEFOLIOSPENDIENTES,[folioID,FolioUUID]),true);
            result := true;
          Except
           result := false;
          end;
     end
end;




 function TdmServerNominaTimbrado.GetLookupEmpleado(Empresa: OleVariant; Empleado: Integer;
          out Datos: OleVariant; TipoLookup: Integer): WordBool;
var
   sSQL: String;

   function GetCampos: String;
   begin
        Result := VACIO;
        case TTipoLookupEmpleado( TipoLookup ) of
             eLookEmpNominas:    Result := ',CB_NOMYEAR,CB_NOMTIPO,CB_NOMNUME,CB_FEC_BAJ,CB_TURNO,CB_NOMINA,CB_FEC_NOM';
             eLookEmpCursos:     Result := ',CB_TURNO,CB_PUESTO,CB_CLASIFI,CB_NIVEL1,CB_NIVEL2,CB_NIVEL3' +
                                           ',CB_NIVEL4,CB_NIVEL5,CB_NIVEL6,CB_NIVEL7,CB_NIVEL8,CB_NIVEL9'
                                           {$ifdef ACS}+',CB_NIVEL10, CB_NIVEL11, CB_NIVEL12'{$endif};
             eLookEmpCedulas:    Result := ',CB_PUESTO';
             eLookEmpComidas:    Result := ',CB_FEC_BAJ';
             eLookEmpAsignaArea: Result := ',CB_AREA,CB_AR_FEC,CB_AR_HOR';
             eLookEmpMedicos:    Result := ',CB_NOMBRES,CB_APE_PAT,CB_APE_MAT,CB_SEXO,CB_FEC_NAC,CB_CALLE,CB_COLONIA,'+
                                           'CB_CIUDAD,CB_CODPOST,CB_ESTADO,CB_TEL,CB_NOMINA, CB_NUM_EXT, CB_NUM_INT,CB_TSANGRE,CB_ALERGIA';
             eLookEmpBancaElec:  Result := ',CB_BAN_ELE,CB_CTA_GAS,CB_CTA_VAL';
             eLookEmpInfonavit:  Result := ',CB_INFTIPO, CB_INFCRED, CB_INFTASA, CB_INF_OLD, CB_INFMANT, CB_INF_INI,CB_INFACT';
             eLookEmpPlaza:      Result := ',CB_PLAZA,CB_AUTOSAL';
             eLookEmpTransfer:   Result := ',CB_FEC_NIV,CB_NIVEL1,CB_NIVEL2,CB_NIVEL3' +
                                           ',CB_NIVEL4,CB_NIVEL5,CB_NIVEL6,CB_NIVEL7,CB_NIVEL8,CB_NIVEL9'
                                           {$ifdef ACS}+',CB_NIVEL10, CB_NIVEL11, CB_NIVEL12'{$endif}
        end;
   end;

begin
     InitLog(Empresa,'GetLookupEmpleado');
     sSQL := Format( 'select TABLA,CB_CODIGO,CB_ACTIVO,' + K_PRETTYNAME + ' as PrettyName %s '+
                                        'from V_EMP_TIMB where ( CB_CODIGO = %d ) %s', [ GetCampos, Empleado, Nivel0( Empresa ) ] );
     Datos := oZetaProvider.OpenSQL( Empresa, sSQL, True );
     //Value := ( oZetaProvider.RecsOut > 0 );
     EndLog;SetComplete;
end;


function TdmServerNominaTimbrado.GetEmpleadosBuscados_DevExTimbrado(Empresa: OleVariant;
          const sPista: WideString): OleVariant;
function filtrarParametro( sParam : string ) : string;
         begin
                Result := Trim( FiltrarTextoSQL( sParam ) ) ;
         end;

const
      {$ifdef MSSQL}
     QRY_STATUS_ACT = ' ( dbo.SP_STATUS_ACT( %s ,CB_CODIGO ) )as STATUS,';
      {$endif}
      {$ifdef INTERBASE}
     QRY_STATUS_ACT = ' ( Select Resultado from SP_STATUS_ACT( %s ,COLABORA.CB_CODIGO ) )as STATUS,';
      {$endif}
      Q_EMPLEADOS_BUSCADOS_DEV_EX = 'select TABLA, CB_CODIGO, CB_APE_PAT, CB_APE_MAT, CB_NOMBRES, CB_RFC, CB_SEGSOC,CB_ACTIVO, CB_BAN_ELE,CB_CTA_GAS,CB_CTA_VAL, CB_PLAZA, %s '+
                                          K_PRETTYNAME + ' as PrettyName from V_EMP_TIMB where ('+
                                          '( UPPER( CB_APE_PAT ) like ''%s'' ) or '+
                                          '( UPPER( CB_APE_MAT ) like ''%s'' ) or '+
                                          '( UPPER( CB_NOMBRES ) like ''%s'' ) or '+
                                          '( UPPER( CB_RFC ) like ''%s'' ) or '+
                                          '( UPPER( CB_SEGSOC ) like ''%s'' ) or '+
                                          ' CB_CODIGO like ''%s'' or '+
                                          '( UPPER( CB_BAN_ELE ) like ''%s'' )) %s '+
                                          'order by CB_APE_PAT, CB_APE_MAT, CB_NOMBRES';
var
   sSQL: String;
   sStatusEmpleado :string;
begin
     InitLog(Empresa,'GetEmpleadosBuscados');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
     end;
     sStatusEmpleado := VACIO;
     {$ifndef SELECCION}
     {$ifndef VISITANTES}

     if oZetaProvider.GetGlobalBooleano(K_GLOBAL_CB_ACTIVO_AL_DIA )then
             sStatusEmpleado := Format(QRY_STATUS_ACT,[ DateToStrSQLC(Now)]);
     {$endif}
     {$endif}
     sSQL := Format( Q_EMPLEADOS_BUSCADOS_DEV_EX ,
                                     [ sStatusEmpleado,
                                       '%' + filtrarParametro( sPista ) + '%',
                                       '%' + filtrarParametro( sPista ) + '%',
                                       '%' + filtrarParametro( sPista )  + '%',
                                       '%' + filtrarParametro( sPista ) + '%',
                                       '%' + filtrarParametro( sPista ) + '%',
                                       '%' + filtrarParametro( sPista ) + '%',
                                       '%' + filtrarParametro( sPista ) + '%',
                                       Nivel0( Empresa ) ] );

     Result := oZetaProvider.OpenSQL( Empresa, sSQL, True );
     EndLog;SetComplete;

end;







function TdmServerNominaTimbrado.GetEmpleadosBuscados_DevExTimbradoAvanzada(
  Empresa: OleVariant; const sPaterno, sMaterno, sNombre, sRFC, sNSS,
  sBanca: WideString): OleVariant;
function filtrarParametro( sParam : string ) : string;
         begin
                Result := Trim( FiltrarTextoSQL( sParam ) ) ;
         end;

const
      {$ifdef MSSQL}
     QRY_STATUS_ACT = ' ( dbo.SP_STATUS_ACT( %s ,CB_CODIGO ) )as STATUS,';
      {$endif}
      {$ifdef INTERBASE}
     QRY_STATUS_ACT = ' ( Select Resultado from SP_STATUS_ACT( %s ,COLABORA.CB_CODIGO ) )as STATUS,';
      {$endif}
     Q_EMPLEADOS_BUSCADOS_AVANZADOS_DEV_EX = 'select TABLA, CB_CODIGO, CB_APE_PAT, CB_APE_MAT, CB_NOMBRES, CB_RFC, CB_SEGSOC,CB_ACTIVO, CB_BAN_ELE,CB_CTA_GAS,CB_CTA_VAL, CB_PLAZA, %s '+
                                          K_PRETTYNAME + ' as PrettyName from V_EMP_TIMB where ('+
                                          '( UPPER( CB_APE_PAT ) like ''%s'' ) and '+
                                          '( UPPER( CB_APE_MAT ) like ''%s'' ) and '+
                                          '( UPPER( CB_NOMBRES ) like ''%s'' ) and '+
                                          '( UPPER( CB_RFC ) like ''%s'' ) and '+
                                          '( UPPER( CB_SEGSOC ) like ''%s'' ) and '+
                                          '( UPPER( CB_BAN_ELE ) like ''%s'' ) ) %s '+
                                          'order by CB_APE_PAT, CB_APE_MAT, CB_NOMBRES';
var
   sSQL: String;
   sStatusEmpleado :string;
begin
     InitLog(Empresa,'GetEmpleadosBuscadosAvanzada');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
     end;
     sStatusEmpleado := VACIO;
     {$ifndef SELECCION}
     {$ifndef VISITANTES}

     if oZetaProvider.GetGlobalBooleano(K_GLOBAL_CB_ACTIVO_AL_DIA )then
             sStatusEmpleado := Format(QRY_STATUS_ACT,[ DateToStrSQLC(Now)]);
     {$endif}
     {$endif}

	sSQL := Format(  Q_EMPLEADOS_BUSCADOS_AVANZADOS_DEV_EX ,
                                     [ sStatusEmpleado,
                                       '%' + filtrarParametro( sPaterno ) + '%',
                                       '%' + filtrarParametro( sMaterno ) + '%',
                                       '%' + filtrarParametro( sNombre )  + '%',
                                       '%' + filtrarParametro( sRFC ) + '%',
                                       '%' + filtrarParametro( sNSS ) + '%',
                                       '%' + filtrarParametro( sBanca ) + '%',
                                       Nivel0( Empresa ) ] );

     Result := oZetaProvider.OpenSQL( Empresa, sSQL, True );
     EndLog;SetComplete;
end;



function TdmServerNominaTimbrado.GetEmpleadoTransferidoParaActivo(
  Empresa: OleVariant; Empleado: Integer; out Datos: OleVariant): WordBool;
const
     Q_EMPLEADO_TRANSFERIDO = ' where ( CB_CODIGO = %d ) %s order by CB_APE_PAT, CB_APE_MAT, CB_NOMBRES';

var
   sSQL: String;
   sStatusEmpleado :string;
begin
     InitLog(Empresa,'GetEmpleadoTransferidoParaActivo');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
     end;
     try
         sStatusEmpleado := VACIO;
         sSQL := Format( GetSQLScript (Q_CAMPOS_VISTA_TRANSFERIDOS) + Q_EMPLEADO_TRANSFERIDO ,[ Empleado, Nivel0( Empresa ) ] );

         Datos := oZetaProvider.OpenSQL( Empresa, sSQL, True );

         Result := True;
     Except  on Error: Exception do
              begin
                   Result := False;
              end;
     end;

     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.GetEmpleadoTransferidoAnterior(
  Empresa: OleVariant; Empleado: Integer; const sNavega: WideString;
  out Datos: OleVariant): WordBool;
const
     Q_EMPLEADO_ANTERIOR = ' where ( CB_CODIGO = ( select MAX( C2.CB_CODIGO ) from V_EMP_TIMB C2 where ( C2.CB_CODIGO < %d ) %s %s ) )';
     var
        sQuery : string;
        sResult: String;
        sSQL: String;
begin


     try
         sQuery := GetSQLScript (Q_CAMPOS_VISTA_TRANSFERIDOS) + Q_EMPLEADO_ANTERIOR;
         InitLog(Empresa,'GetEmpleadoTransferidoAnterior');
         with oZetaProvider do
         begin
              EmpresaActiva := Empresa;
              InitGlobales;
         end;
         sResult := GetNavegacion( Empresa, sNavega );
         sSQL := Format( sQuery , [ Empleado, Nivel0( Empresa ),  ( sResult )  ] );
         Datos := oZetaProvider.OpenSQL( Empresa, sSQL, True );
         Result := True;
     Except  on Error: Exception do
              begin
                   Result := False;
              end;
     end;

     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.GetEmpleadoTransferidoSiguiente(
  Empresa: OleVariant; Empleado: Integer; const sNavega: WideString;
  out Datos: OleVariant): WordBool;
const
     Q_EMPLEADO_SIGUIENTE = ' where ( CB_CODIGO = ( select MIN( C2.CB_CODIGO ) from V_EMP_TIMB C2 where ( C2.CB_CODIGO > %d ) %s %s ) )';
     var
        sQuery : string;
        sResult: String;
           sSQL: String;
begin
     try
         sQuery := GetSQLScript (Q_CAMPOS_VISTA_TRANSFERIDOS) + Q_EMPLEADO_SIGUIENTE;
         InitLog(Empresa,'GetEmpleadoTransferidoSiguiente');
         with oZetaProvider do
         begin
              EmpresaActiva := Empresa;
              InitGlobales;
         end;
         sResult := GetNavegacion( Empresa, sNavega );
         sSQL := Format( sQuery , [ Empleado, Nivel0( Empresa ),  ( sResult )  ] );
         Datos := oZetaProvider.OpenSQL( Empresa, sSQL, True );
         Result := True;
     Except  on Error: Exception do
              begin
                   Result := False;
              end;
     end;
end;


function TdmServerNominaTimbrado.GetNavegacion( Empresa: OleVariant; sNavegacion: String ): String;
const
     Q_VA_NAVEGACION = 'select QU_FILTRO from QUERYS where QU_CODIGO= ''%s'' ';
var
   sSQL: String;
begin
     Result := VACIO;
     if StrLleno( sNavegacion ) then
     begin
          sSQL := Format( Q_VA_NAVEGACION , [ sNavegacion ] );
          with oZetaProvider do
          begin
               cdsTempo.Lista := OpenSQL( Empresa, sSQL, True );
               //SE DEJA COMO COLABORA  PARA QUE LO SUSTITUYA EL FILTRO QUE VIENE DE LA DB A C2 YA DESPUES SE HACE EL REPLACE A DONDE SEA NECESARIO
               Result:= StrTransAll( cdsTempo.FieldByName( 'QU_FILTRO' ).AsString, 'COLABORA', 'C2' );
               if StrLleno(Result) then
               begin
                           Result := StringReplace(Result, 'COLABORA', 'V_EMP_TIMB',
                          [rfReplaceAll, rfIgnoreCase]);
                    if EsFormulaSQL( Result ) then
                       Result := Copy( Result, 2, Length(Result) );
                  Result:= 'AND ' + Result;
               end
          end;
     end;
     EndLog;
     SetComplete;
end;

function TdmServerNominaTimbrado.GetValidacionEmpleadoTransferido(
  Empresa: OleVariant; Empleado: Integer; out Datos: OleVariant): WordBool;
const
     Q_EMPLEADO_ES_TRANSFERIDO = ' Select Tabla from V_EMP_TIMB where ( CB_CODIGO = %d ) %s';
var
   sSQL: String;
   sStatusEmpleado, sTabla, sQuery :string;
begin
     InitLog(Empresa,'GetValidacionEmpleadoTransferido');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
     end;
     try
         sSQL := Format( Q_EMPLEADO_ES_TRANSFERIDO,  [ Empleado, Nivel0( Empresa ) ] );

         Datos := oZetaProvider.OpenSQL( Empresa, sSQL, True );
	       Result := True;
     Except  on Error: Exception do
              begin
                   Result := False;
              end;
     end;
     EndLog;SetComplete;
end;


function TdmServerNominaTimbrado.GetPeriodoTimbrado(Empresa: OleVariant; Year, Tipo, Numero: Integer; out Datos: OleVariant): WordBool;
var
   sSQL: String;
begin
     InitLog(Empresa,'GetPeriodo');
     sSQL := Format( GetSQLScript ( Q_VA_PERIODO ), [ Year, Tipo, Numero ] );
     Datos := oZetaProvider.OpenSQL( Empresa, sSQL, True );
     Result := ( oZetaProvider.RecsOut > 0 );
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.GetPeriodoAnteriorTimbrado(Empresa: OleVariant; Year, Tipo, Numero: Integer; out Datos: OleVariant): WordBool;
var
   sSQL: String;
begin
     InitLog(Empresa,'GetPeriodoAnterior');
     sSQL := Format( GetSQLScript ( Q_VA_PERIODO_ANTERIOR ), [ Year, Tipo, Numero ] );
     Datos := oZetaProvider.OpenSQL( Empresa, sSQL, True );
     Result := ( oZetaProvider.RecsOut > 0 );
     EndLog;SetComplete;
end;

function TdmServerNominaTimbrado.GetPeriodoSiguienteTimbrado(Empresa: OleVariant; Year, Tipo, Numero: Integer; out Datos: OleVariant): WordBool;
var
   sSQL: String;
begin
     InitLog(Empresa,'GetPeriodoSiguiente');
     sSQL := Format( GetSQLScript ( Q_VA_PERIODO_SIGUIENTE ), [ Year, Tipo, Numero ] );
     Datos := oZetaProvider.OpenSQL( Empresa, sSQL, True );
     Result := ( oZetaProvider.RecsOut > 0 );
     EndLog;SetComplete;
end;


procedure TdmServerNominaTimbrado.CopiarConciliacionTimbradoParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Conciliaci�n timbrado nomina de Tipo: ' + ZetaCommonLists.ObtieneElemento( lfTipoNomina, ParamByName( 'PE_TIPO' ).AsInteger ) +
          K_PIPE + 'A�o: ' + ParamByName( 'PE_YEAR' ).AsString +
          K_PIPE + 'N�mero: ' + ParamByName( 'PE_NUMERO' ).AsString;
     end;
end;
//US Conciliacion Timbrado de Nomina
function TdmServerNominaTimbrado.IniciarConciliacionTimbradoPeriodos(var Parametros: OleVariant): WordBool;
const
     Q_GET_TABLEROS = '{CALL TimbradoConcilia_Conciliar(:PE_YEAR, :PE_TIPO, :PE_NUMERO, :Xml, :US_CODIGO, :US_FEC_MOD, :PC_PROC_ID, :Error )}';
var
   iExitos, iErrores, iAdvertencias: integer;
   FTableros: TZetaCursor;
   sErrores: String;
begin
     // Inicia el Proceso
     Result := True;
     sErrores := VACIO;
     with oZetaProvider do
       try
          AsignaParamList( Parametros );
          CopiarConciliacionTimbradoParametros;
          EmpresaActiva := ParamList.ParamByName( 'Empresa' ).Value;
          InitArregloTPeriodo;
         if OpenProcess(prIniciarConciliacionTimbrado, 100, FListaParametros) then
         begin
              ParamList.ParamValues['DB_PROCESO'] := oZetaProvider.Log.Folio;
              //Inicia Conciliacion con respuesta de la Nube
              FTableros:= CreateQuery( Q_GET_TABLEROS );
              try
                 EmpiezaTransaccion;
                 try
                    ParamAsInteger ( FTableros, 'PE_YEAR', ParamList.ParamByName( 'PE_YEAR' ).AsInteger );
                    ParamAsInteger ( FTableros, 'PE_TIPO',  ParamList.ParamByName( 'PE_TIPO' ).AsInteger );
                    ParamAsInteger ( FTableros, 'PE_NUMERO',  ParamList.ParamByName( 'PE_NUMERO' ).AsInteger );
                    ParamAsString  ( FTableros, 'Xml',  ParamList.ParamByName( 'XML_CONCILIACION_NUBE' ).AsString );
                    ParamAsInteger ( FTableros, 'US_CODIGO',  ParamList.ParamByName( 'USUARIO' ).AsInteger );
                    ParamAsDate( FTableros, 'US_FEC_MOD', ParamList.ParamByName( 'US_FEC_MOD' ).AsDate );
                    ParamAsInteger ( FTableros, 'PC_PROC_ID',  oZetaProvider.Log.Folio );

                    ParamSalida( FTableros, 'Error');
                    Ejecuta( FTableros );
                    TerminaTransaccion( True );
                 except
                       on Error: Exception do
                       begin
                            Result := false;
                            sErrores := Error.Message;
                            RollBackTransaccion;
                            if ( Pos( 'UNIQUE INDEX', UpperCase( Error.Message ) ) > 0 ) then
                            begin
                            end;
                            raise;
                       end;
                 end;
                 sErrores := GetParametro( FTableros, 'Error').AsString;
                 if not StrVacio( sErrores ) then
                    Result := false;
              finally
                     FreeAndNil( FTableros );
              end;
              SetComplete;
              iErrores := 0;
              iExitos := 0;
              iAdvertencias := 0;
         end;
         CloseProcess;
       finally
       begin
         ParamList.AddInteger('PRO_EXITOS', iExitos);
         ParamList.AddInteger('PRO_ERRORES', iErrores);
         ParamList.AddInteger('PRO_ADVERTENCIAS', iAdvertencias);
         ParamList.AddString('ERRORES_MENSAJE', sErrores);
         Parametros := ParamList.VarValues;
         Result := (iErrores = 0);
       end;
     end;
end;

function TdmServerNominaTimbrado.AplicarConciliacionTimbradoPeriodos(var Parametros: OleVariant): WordBool;
const
     Q_GET_TABLEROS = '{CALL TimbradoConcilia_Aplicar(:PE_YEAR, :PE_TIPO, :PE_NUMERO, :US_CODIGO, :US_FEC_MOD, :PC_PROC_ID, :Error )}';
var
   iExitos, iErrores, iAdvertencias: integer;
   FTableros: TZetaCursor;
   sErrores: String;
begin
     // Inicia el Proceso
     Result := True;
     sErrores := VACIO;
     with oZetaProvider do
       try
          AsignaParamList( Parametros );
          CopiarConciliacionTimbradoParametros;
          EmpresaActiva := ParamList.ParamByName( 'Empresa' ).Value;
          InitArregloTPeriodo;
         if OpenProcess(prAplicarConciliacionTimbrado, 100, FListaParametros) then
         begin
              ParamList.ParamValues['DB_PROCESO'] := oZetaProvider.Log.Folio;
              //Inicia Conciliacion con respuesta de la Nube
              FTableros:= CreateQuery( Q_GET_TABLEROS );
              try
                 EmpiezaTransaccion;
                 try
                    ParamAsInteger ( FTableros, 'PE_YEAR', ParamList.ParamByName( 'PE_YEAR' ).AsInteger );
                    ParamAsInteger ( FTableros, 'PE_TIPO',  ParamList.ParamByName( 'PE_TIPO' ).AsInteger );
                    ParamAsInteger ( FTableros, 'PE_NUMERO',  ParamList.ParamByName( 'PE_NUMERO' ).AsInteger );
                    ParamAsInteger ( FTableros, 'US_CODIGO',  ParamList.ParamByName( 'USUARIO' ).AsInteger );
                    ParamAsDate( FTableros, 'US_FEC_MOD', ParamList.ParamByName( 'US_FEC_MOD' ).AsDate );
                    ParamAsInteger ( FTableros, 'PC_PROC_ID',  oZetaProvider.Log.Folio );

                    ParamSalida( FTableros, 'Error');
                    Ejecuta( FTableros );
                    TerminaTransaccion( True );
                 except
                       on Error: Exception do
                       begin
                            Result := false;
                            sErrores := Error.Message;
                            RollBackTransaccion;
                            if ( Pos( 'UNIQUE INDEX', UpperCase( Error.Message ) ) > 0 ) then
                            begin
                            end;
                            raise;
                       end;
                 end;
                 sErrores := GetParametro( FTableros, 'Error').AsString;
                 if not StrVacio( sErrores ) then
                    Result := false;
              finally
                     FreeAndNil( FTableros );
              end;
              SetComplete;
              iErrores := 0;
              iExitos := 0;
              iAdvertencias := 0;
         end;
         CloseProcess;
       finally
       begin
         ParamList.AddInteger('PRO_EXITOS', iExitos);
         ParamList.AddInteger('PRO_ERRORES', iErrores);
         ParamList.AddInteger('PRO_ADVERTENCIAS', iAdvertencias);
         ParamList.AddString('ERRORES_MENSAJE', sErrores);
         Parametros := ParamList.VarValues;
         Result := (iErrores = 0);
       end;
     end;
end;

function TdmServerNominaTimbrado.ObtenerPeriodosAConciliarTimbrado(  Empresa: OleVariant; iYear, iMes: Integer; const sRsCodigo: WideString): OleVariant;
begin
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             Result := OpenSQL( EmpresaActiva, Format( GetSQLScript( Q_PERIODOS_X_MES_YEAR ), [ Comillas(sRsCodigo), iYear, iMes ] ), True );
        end;
        SetComplete;
     except
           SetAbort;
           raise;
     end;
end;


function TdmServerNominaTimbrado.GetMotorAvanceProceso(var Parametros: OleVariant): OleVariant;
var
   oQuery    : TZetaCursor;
   Params    : TZetaParams;
   sTop      : string;
   sWhere    : string;
   sSentencia: string;
begin
     Result := 0;
     oQuery := nil;
     Params := TZetaParams.Create(Self);
     Params.VarValues := Parametros;
     with oZetaProvider, Params do
     try
        AsignaParamList( Parametros );
        CopiarConciliacionTimbradoParametros;
        EmpresaActiva := Params.ParamByName( 'Empresa' ).Value;
        InitArregloTPeriodo;
        try
           sSentencia := 'select %s PC_NUMERO, PC_MAXIMO, PC_PASO, PC_ERROR from PROCESO where %s';

           if ParamByName('DB_PROCESO').AsInteger = 0 then {Considerar cuando no se tenga nuemro de proceso}
           begin
                sTop := 'top 1';
                sWhere :='PC_PARAMS = ' + QuotedStr(FListaParametros) + ' order by PC_FEC_INI + PC_HOR_INI desc';
           end
           else
           begin
                sTop := 'top 1';
                sWhere := 'PC_NUMERO = ' + ParamByName('DB_PROCESO').AsString;
           end;
           sSentencia := Format(sSentencia, [sTop, sWhere]);
           Result := OpenSQL(EmpresaActiva, sSentencia, True);
        except
              on e: Exception do
                raise Exception.Create( Format ('No es posible conectarse a: %s. Por favor, verifique su informaci�n. %s', ['EMPRESA_DEMO', sLineBreak + e.Message]) );
        end;
     finally
            FreeAndNil(oQuery);
            SetComplete;
     end;
end;

function TdmServerNominaTimbrado.ObtenerXMLEmpleadosAConciliarTimbrado(Empresa: OleVariant; var Params: OleVariant; var XML, Error: WideString): OleVariant;
begin
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList( Params );
             Result := OpenSQL( EmpresaActiva, Format( GetSQLScript( Q_COCILIACION_NOMINTA_TIMBRAMEX ), [ Comillas(ParamList.ParamByName( 'RS_CODIGO' ).AsString), ParamList.ParamByName( 'PE_YEAR' ).AsInteger, ParamList.ParamByName( 'PE_TIPO' ).AsInteger, ParamList.ParamByName( 'PE_NUMERO' ).AsInteger ] ), True );
        end;
        SetComplete;
     except
           SetAbort;
           raise;
     end;
end;

function TdmServerNominaTimbrado.ObtenerPeriodosConciliados(Empresa: OleVariant; var Params: OleVariant): OleVariant;
begin
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList( Params );
             Result := OpenSQL( EmpresaActiva, Format( GetSQLScript( Q_COCILIACION_NOMINTA_TIMBRAMEX ), [ ParamList.ParamByName( 'PE_YEAR' ).AsInteger, ParamList.ParamByName( 'PE_TIPO' ).AsInteger, ParamList.ParamByName( 'PE_NUMERO' ).AsInteger ] ), True );
        end;
        SetComplete;
     except
           SetAbort;
           raise;
     end;
end;

function TdmServerNominaTimbrado.GetEmpleadosConciliados( Empresa: OleVariant; var Params: OleVariant ): OleVariant;
begin
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList( Params );
             Result := OpenSQL( EmpresaActiva, Format( GetSQLScript( Q_CONCILIACION_EMPLEADOS_TIMBRADO ), [Comillas( ParamList.ParamByName( 'RAZON_SOCIAL' ).AsString ), ParamList.ParamByName( 'PE_YEAR' ).AsInteger, ParamList.ParamByName( 'PE_TIPO' ).AsInteger, ParamList.ParamByName( 'PE_NUMERO' ).AsInteger ] ), True );
        end;
        SetComplete;
     except
           SetAbort;
           raise;
     end;
end;

function TdmServerNominaTimbrado.SetEmpleadosPendienteTimbrado(Empresa, Lista, Parametros: OleVariant): OleVariant;
var
   oPeriodoTimbrado:TDatosPeriodo;
   procedure CargarParametrosBitacora;
   begin
        with oZetaProvider.ParamList do
        begin
             FListaParametros := VACIO;
             FListaParametros := 'Periodo de Timbrado :'+CR_LF+
                                'A�o: ' + IntToStr( ParamByName('Year').AsInteger ) +
                                 K_PIPE + 'Nuevo Status: ' + ZetaCommonLists.ObtieneElemento( lfStatusTimbrado, ParamByName('StatusNuevo').AsInteger ) +
                                 K_PIPE + 'Tipo: ' + ZetaCommonLists.ObtieneElemento( lfTipoPeriodo, ParamByName('Tipo').AsInteger ) +
                                 K_PIPE + 'N�mero: ' + IntToStr( ParamByName('Numero').AsInteger ) +
                                 K_PIPE + 'Raz�n Social: '+ ParamByName('RazonSocial').AsString  +
                                 K_PIPE + 'Contribuyente ID: '+ ParamByName('ContribuyenteID').AsString ;
        end;
end;
begin
     InitLog(Empresa,'SetEmpleadosPendienteTimbrado');
     InitBroker;
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList(Parametros);
             InitArregloTPeriodo; //@(am):Tipos de periodo
             CargarParametrosBitacora;
             GetDatosPeriodo;   // Asigna el periodo de la baja como activo
             if ( DatosPeriodo.Status < spAfectadaParcial ) then
                DataBaseError( 'La nomina debe estar Afectada para poder cambiar el estatus de timbrado' );
             with ParamList do
             begin
                  with oPeriodoTimbrado do
                  begin
                       Year := ParamByName('Year').AsInteger;
                       Tipo := eTipoPeriodo( ParamByName( 'Tipo').AsInteger);
                       Numero := ParamByName('Numero').AsInteger;
                  end;
             end;
             cdsLista.Lista := Lista;
             Result := NominasPendientesListaDataset( cdsLista );
        end;
     finally
            ClearBroker;
     end;

end;

function TdmServerNominaTimbrado.NominasPendientesListaDataset(Dataset: TDataSet): OleVariant;
var
   oPeriodoTimbrado: TDatosPeriodo;
   iEmpleado : Integer;
   iFolio, iUsuario:Integer;
   sRFC,sFolioUUID, sTimbre : string;
   FTimbrar: TZetaCursor;
   lOK : boolean;
   statusActual, statusNuevo : eStatusTimbrado;

   function ObtenerNombreCampoConsulta( statusTimbrado: eStatusTimbrado) : String;
   begin
        Result := VACIO;
        if ( statusTimbrado = estiCancelacionPendienteLimpiar ) then
        begin
             Result := 'Empleado';
        end
        else if ( statusTimbrado = estiTimbradoPendienteLimpiar ) then
        begin
             Result := 'COLUMNA0';
        end
        else if ( statusTimbrado = estiTimbradoPendiente ) then
        begin
             Result := 'COLUMNA0';
        end
        else if ( statusTimbrado = estiCancelacionPendiente ) then
        begin
             Result := 'Empleado';
        end;
   end;

   //Obtener el nombre del campo
   function dwGetFieldName( ds : TDataSet; sDisplayLab  : string ) : string ;
   var
      iField, nFields : integer;
      sDsDisplayLab : string;
   begin
        Result := VACIO;
        nFields := ds.FieldCount;

        for iField := 0 to nFields -1 do
        begin
             sDsDisplayLab := UpperCase(ds.Fields[iField].DisplayLabel ) ;

             if ( sDisplayLab  = sDsDisplayLab ) then
             begin
                 Result  :=  ds.Fields[iField].FieldName;
                 break;
             end;
        end;
   end;
begin
     FTimbrar := nil;
     with oZetaProvider do
     begin
          InitGlobales;
          if OpenProcess( prNoPendienteNominaEmpleado , Dataset.RecordCount, FListaParametros ) then
          begin
               { Ejecuci�n }
               with Dataset do
               begin
                    with ParamList do
                    begin
                         with oPeriodoTimbrado do
                         begin
                              Year := ParamByName('Year').AsInteger;
                              Tipo := eTipoPeriodo( ParamByName( 'Tipo').AsInteger );
                              Numero := ParamByName('Numero').AsInteger;
                         end;
                         statusActual := eStatusTimbrado(  ParamByName('StatusAnterior').AsInteger ) ;
                         statusNuevo := eStatusTimbrado(  ParamByName('StatusNuevo').AsInteger ) ;
                    end;

                    iUsuario := UsuarioActivo;
                    FTimbrar := CreateQuery( GetSQLScript( Q_TIMBRAR_EMPLEADO ) );
                    lOK := FALSE;
                    //Poner el valor cancelacion.
                    while not Eof and CanContinue( Dataset.FieldByName( ObtenerNombreCampoConsulta( statusNuevo) ).AsInteger ) do
                    begin
                         iEmpleado := Dataset.FieldByName( ObtenerNombreCampoConsulta( statusNuevo) ).AsInteger;
                         EmpiezaTransaccion;
                         try
                            ParamAsInteger( FTimbrar, 'Year', oPeriodoTimbrado.Year );
                            ParamAsInteger( FTimbrar, 'Tipo', Ord( oPeriodoTimbrado.Tipo )  );
                            ParamAsInteger( FTimbrar, 'Numero', oPeriodoTimbrado.Numero );
                            ParamAsInteger( FTimbrar, 'Empleado', iEmpleado );
                            ParamAsString( FTimbrar, 'RFC' , VACIO ) ;
                            ParamAsInteger( FTimbrar, 'Status', ord( statusNuevo ) );
                            ParamAsString( FTimbrar, 'Timbre', VACIO );
                            ParamAsInteger( FTimbrar, 'Folio', 0);
                            ParamAsInteger( FTimbrar, 'Usuario', iUsuario);
                            ParamAsString(FTimbrar,'FacturaUUID', VACIO);

                            Ejecuta( FTimbrar );
                            TerminaTransaccion( True );
                            lOk := True;
                         except
                               on Error: Exception do
                               begin
                                    TerminaTransaccion( False );
                                    Log.Excepcion( 0, 'Error al cambiar status del empleado ', Error, DescripcionParams );
                                    lOk := False;
                               end;
                         end;

                         try
                            if ( lOK ) then
                            begin
                                 Log.Evento(clbNinguno,iEmpleado,Now,Format('Se Actualiz� el Status de Timbrado:  %s , Empleado : %d , A�o:%d Numero:%d Tipo:%s ',[ObtieneElemento(lfstatusTimbrado,ord( statusNuevo )), iEmpleado,oPeriodoTimbrado.Year, oPeriodoTimbrado.NUmero,ObtieneElemento(lfTipoPeriodo,ord( oPeriodoTimbrado.Tipo))  ] ) );
                            end;

                         except
                               on Error: Exception do
                               begin
                                    Log.Excepcion( 0, 'Error al cambiar status del Empleado ' + GetPeriodoInfo(oPeriodoTimbrado.Year, oPeriodoTimbrado.NUmero, oPeriodoTimbrado.Tipo ),Error) ;
                               end;
                         end;
                         Next;
                    end;
               end;

               FreeAndNil( FTimbrar );

               //Afectar Status del Periodo
               FTimbrar := CreateQuery( GetSQLScript( Q_TIMBRAR_PERIODO  ) );
               EmpiezaTransaccion;
               try
                  ParamAsInteger( FTimbrar, 'Year', oPeriodoTimbrado.Year );
                  ParamAsInteger( FTimbrar, 'Tipo', Ord( oPeriodoTimbrado.Tipo )  );
                  ParamAsInteger( FTimbrar, 'Numero', oPeriodoTimbrado.Numero );
                  ParamAsInteger( FTimbrar, 'StatusActual', ord( statusActual ) );
                  ParamAsInteger( FTimbrar, 'StatusNuevo', ord( statusNuevo ) );

                  Ejecuta( FTimbrar );
                  TerminaTransaccion( True );

               except
                     on Error: Exception do
                     begin
                          TerminaTransaccion( False );
                          Log.Excepcion( 0, 'Error Al  Calcular Status de  Timbrado', Error, DescripcionParams );

                     end;
               end;

               FreeAndNil( FTimbrar );

               Result := CloseProcess;
          end;
     end;
end;

function TdmServerNominaTimbrado.GetXMLAutidoriaConceptoTimbrado( Empresa: OleVariant; var Params: OleVariant ): OleVariant;
const
     Q_CONSULTA_XML_AUDITORIA_CONCEPTO = 'exec dbo.AuditoriaConceptosTimbradoTRESS';
begin
     with oZetaProvider do
     begin
         EmpresaActiva := Empresa;
         Result := oZetaProvider.OpenSQL( EmpresaActiva, Q_CONSULTA_XML_AUDITORIA_CONCEPTO, True );
         SetComplete;
         EmpresaActiva := Empresa;
     end;
end;

{$ifndef DOS_CAPAS}

initialization
  TComponentFactory.Create(ComServer, TdmServerNominaTimbrado, Class_dmServerNominaTimbrado, ciMultiInstance, ZetaServerTools.GetThreadingModel);
{$endif}
end.



