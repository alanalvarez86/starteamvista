unit DServerSistemaTimbrado;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      DServerSistema.pas                         ::
  :: Descripci�n: Programa principal de Sistema.dll          ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

{$DEFINE SISNOM_CLAVES}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComServ, ComObj, VCLCom, StdVcl, DataBkr, DBClient, MtsRdm, Mtx, Db,
     {$ifndef VER130}Variants,{$endif}
{$ifndef DOS_CAPAS}
     TimbradoSistema_TLB,
{$endif}
     DZetaServerProvider,
     ZetaCommonClasses,
     ZetaServerDataSet,
     ZCreator,
     ZetaSQLBroker,
     ZetaCommonLists,
     DSeguridad,
     FAutoServer,
     FAutoClasses;

type
  eTipoSistema = ( eArbol,
                   eEmpresas,
                   eGruposUsuarios,
                   eUsuarios,
                   ePoll,
                   eImpresoras,
                   eAccesos,
                   eUserSuper,
                   eNivel0,
                   eSuscrip,
                   eUsuarioAreas,
                   eGruposAdic,
                   eCamposAdic,
                   eRoles,
                   eUserRol,
                   eAccesosAdicionales,
                   eSistLstDispositivos,
                   eDisxCom,
                   eAccArbol,
                   eUsuarioCCosto,
                   eSistListaGrupos, // SYNERGY
                   eTermPorGrupo, // SYNERGY
                   eMensajes, // SYNERGY
                   eMensajesTerm, // SYNERGY
                   eTerminales, // SYNERGY
                   eCuentasTimbrado 
                    );

  TdmServerSistemaTimbrado = class(TMtsDataModule {$ifndef DOS_CAPAS}, IdmServerSistemaTimbrado {$endif} )
    cdsLista: TServerDataSet;
    procedure dmServerSistemaTimbradoCreate(Sender: TObject);
    procedure dmServerSistemaTimbradoDestroy(Sender: TObject);
  private
    { Private declarations }
    oZetaProviderComparte: TdmZetaServerProvider;
    oZetaProvider: TdmZetaServerProvider;
    //FSeguridadInfo: TSeguridadInfo;
    FListaParametros: string;
    FListaFormulas: string;
    FTipoSistema: eTipoSistema;
    FUsuario: Integer;
    FUsuarioData : TUsuarioData;
    {$ifndef DOS_CAPAS}
    FSinBiometrico: Boolean;
    {$endif}
    {$ifndef DOS_CAPAS}
    oZetaCreator: TZetaCreator;
    oZetaSQLBroker: TSQLBroker;
    oAutoServer: TAutoServer;
    {$endif}
    FEmpleado : TZetaCursor;
    FAccionAlEnrolar : eAccionRegistros;
    FModoGrupos:TUpdateKind;
    FCodigoAnterior:Integer;
    FCodigoNuevo:Integer;
    FCM_CODIGOAnt:string;
    FCM_CODIGONuevo:string;
    FDataSetEmpresa:TZetaCursor;
    function GetUsuarioData : TUsuarioData;
    property UsuarioData : TUsuarioData read GetUsuarioData;
    function ActivaDesActivaUsuarios(const Usuario, Grupo:Integer;const lActiva: Boolean): OleVariant;
    function DecodificaClaves( Datos: OleVariant; const sCampo: String ): OleVariant;
    function GetScript( const iScript: Integer ): String;
    function LeerClaves(const iNumero: Integer): String;
    function BuildEmpresa(DataSet: TZetaCursor): OleVariant;
    procedure Codifica( const sCampo: String; DeltaDS: TzProviderClientDataSet );
    procedure CodificaClaveEmpresa(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure AfterUpdateGrupos(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    procedure SetTablaInfo( const eTabla: eTipoSistema );
    procedure BorrarBitacoraParametros;
    procedure AfterUpdateSuscripciones( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind );
    {$ifndef DOS_CAPAS}
    procedure ServerActivate(Sender: TObject);
    procedure ServerDeactivate(Sender: TObject);
    {$endif}
    procedure SetFiltroGrupos( const iGrupo: integer );
    procedure GrabaCambiosBitacora( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind );
    function GetSuscripciones(const sCampo: string; const iCodigo: integer): Olevariant;
    procedure EnrolamientoMasivoBuildDataSet;
    function EnrolamientoMasivoDataset(Dataset: TDataset;Proceso: Procesos): OleVariant;
    procedure EnrolamientoMasivoParametros;
    function GetSQLBroker: TSQLBroker;
    procedure ClearBroker;
    procedure InitBroker;
    procedure InitCreator;
    {$ifndef DOS_CAPAS}
    procedure DescargarUsuarioRoles(const iUsuario: Integer;const sRoles: string);
    {$endif}
    procedure ImportarEnrolamientoMasivoValidaASCII(DataSet: TDataset;var nProblemas: Integer; var sErrorMsg: String);
    procedure ImportarEnrolamientoMasivoParametros;
    function  GetTipoLogin:Integer;
    {$ifdef TRESS}
    function ModuloAutorizado( const eModulo : TModulos ):Boolean;
    {$endif}
    procedure ActualizarEmpresasGrupo(const iUsuario:Integer);
    procedure ActualizaCodigoEmpresa(const CodigoAnterior,NuevoCodigo:string);
    procedure ActualizaDerechos(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    {$ifndef DOS_CAPAS}
    procedure AsignaNumBiometricosBuildDataset; // SYNERGY
    function AsignaNumBiometricosDataset( Dataset: TDataset ): OleVariant; // SYNERGY
    function AsignaGrupoTerminalesDataset( Dataset: TDataset ): OleVariant; // SYNERGY
    procedure SetAfterUpdateGruposDisp( Sender: TObject; SourceDS: TDataSet; DeltaDS: TZProviderClientDataSet; UpdateKind: TUpdateKind );
    procedure SetAfterUpdateMensajesPorTerminal( Sender: TObject; SourceDS: TDataSet; DeltaDS: TZProviderClientDataSet; UpdateKind: TUpdateKind );
    procedure SetAfterUpdateTerminales( Sender: TObject; SourceDS: TDataSet; DeltaDS: TZProviderClientDataSet; UpdateKind: TUpdateKind );
    {$endif}
    function ValidarPatchTimbrado: Boolean;
    function ExistsProcedure(sProc: string): Boolean;
    function ExistsTabla(Tabla, Campo: string): Boolean;
    Function ExistsFunction ( sProc:string) :Boolean;
    Function ExistsIndex(Tabla,Indice:string) :Boolean;
  protected
    { Protected declarations }
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    property SQLBroker: TSQLBroker read GetSQLBroker;



{$ifdef DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
{$endif}
    function GetAccesos(iGrupo: Integer; const sCompany: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    //function GetBitacora(Empresa: OleVariant; FechaInicial, FechaFinal: TDateTime; TipoBitacora, Empleado, Usuario: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetEmpresas(Tipo, GrupoActivo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetEmpresasAccesos(Tipo, Grupo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetGrupos(Grupo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetImpresoras: OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetPoll: OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetPollVacio: OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetUsuarios(out Longitud: Integer; Grupo: Integer; out BlockSistema: WordBool): OleVariant; {$ifndef DOS_CAPAS} safecall;
    function GetUsuarioArbol(iUsuario: Integer): OleVariant; safecall;
    function GetUsuarioArbolApp(iUsuario, iExe: Integer): OleVariant; safecall;{$endif}
    function GetUsuarioSupervisores(Empresa: OleVariant; iUsuario: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaAccesos(oDelta, oParams: OleVariant;out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;
    function GrabaArbol(iUsuario: Integer; oDelta: OleVariant;
      out ErrorCount: Integer): OleVariant; safecall;
    function GrabaArbolApp(iUsuario, iExe: Integer; oDelta: OleVariant;
      out ErrorCount: Integer): OleVariant; safecall;{$endif}
    function GrabaEmpresas(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaGrupos(oDelta: OleVariant; iUsuario: Integer; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaImpresoras(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaPoll(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaUsuarios(oDelta: OleVariant; iUsuario: Integer; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaUsuariosSupervisores(Empresa, oDelta: OleVariant;out ErrorCount: Integer; iUsuario: Integer; const sTexto: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function ActivaUsuarios(Usuario, Grupo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function SuspendeUsuarios(Usuario, Grupo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function BorrarBitacora(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetNivel0: OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaNivel0(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetUsuarioSuscrip(Empresa: OleVariant; Usuario: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaUsuarioSuscrip(Empresa, oDelta: OleVariant; out ErrorCount: Integer; Usuario: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetGruposAccesos(iGrupo: Integer; const sCompany: WideString; iTipo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetUsuarioAreas(Empresa: OleVariant; Usuario: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaUsuarioAreas(Empresa: OleVariant; Usuario: Integer;oDelta: OleVariant; out ErrorCount: Integer;const sTexto: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetNuevoUsuario: Integer; {$ifndef DOS_CAPAS} safecall;{$endif}
    function ActualizaNumeroTarjeta(out Bitacora: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function GetGruposAdic(Empresa: OleVariant; out oCamposAdic: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaGruposAdic(Empresa, oDelta, oDeltaCampos: OleVariant; out ErrorCount, ErrorCampos: Integer; out CamposResult: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetBitacora(Empresa, Parametros: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function GetReporteSuscrip(Empresa: OleVariant; iReporte: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetRoles: OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function GetDatosEmpleadoUsuario(Empresa, Datos: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaRoles(const Rol: WideString; Delta, Usuarios, Modelos: OleVariant; var ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetUsuariosRol(const Rol: WideString; out Modelos: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetUserRoles(Empresa: OleVariant; iUsuario: Integer): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaUsuarioRoles(Empresa: OleVariant; iUsuario: Integer;Delta: OleVariant; var ErrorCount: Integer):OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaEmpleadoUsuario(oEmpresa: OleVariant;iUsuario, iEmpleado: Integer): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function EnrolamientoMasivo(Empresa, Parametros: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function EnrolamientoMasivoGetLista(oEmpresa,oParametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function ImportarEnrolamientoMasivoGetASCII(oEmpresa, Parametros, ListaASCII: OleVariant; out ErrorCount: Integer; LoginAD: WordBool): OleVariant; {$ifndef DOS_CAPAS}  safecall;{$endif}
    function ImportarEnrolamientoMasivo(Empresa, Parametros,Datos: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetClasifiEmpresa(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetDerechosAdicionales(Empresa: OleVariant; Grupo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaAccesosAdicionales(Empresa: OleVariant; Grupo: Integer; Delta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetSistLstDispositivos(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaSistLstDispositivos(Empresa, oDelta: OleVariant;out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetGlobalLstDispositivos(Empresa: OleVariant; var Disponibles, Asignados: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaGlobalLstDispositivos(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetAccArbol(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    procedure ActualizaUsuariosEmpresa(Empresa, Delta: OleVariant); {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetUsuarioCCosto(Empresa: OleVariant; iUsuario: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaUsuarioCCosto(Empresa, oDelta: OleVariant; ErrorCount, iUsuario: Integer; const sTexto, sNombreCosteo: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    {$ifndef DOS_CAPAS}
    function ConsultaBitacoraBiometrica(Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function DepuraBitacoraBiometrica(Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function ObtenIdBioMaximo: Integer;{$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function ActualizaIdBio(const CodigoEmpresaLocal: WideString; Empleado, Id: Integer; const Grupo: WideString; Invitador: Integer): WordBool; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function ConsultaListaGrupos: OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function GrabaSistListaGrupos(oDelta: OleVariant; UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function ConsultaTermPorGrupo(const Grupo: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function GrabaTermPorGrupo(oDelta: OleVariant; UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    procedure GrabaListaTermPorGrupo(const Terminales: WideString; UsuarioTerminal: Integer; const Grupo: WideString); {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function AsignaNumBiometricos(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    procedure ReiniciaSincTerminales(const Lista: WideString); {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function AsignaGrupoTerminales(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function EmpleadosBiometricosGetLista(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function EmpleadosSinBiometricosGetLista(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function AsignaGrupoTerminalesLista(Empresa, Lista, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function AsignaNumBiometricosLista(Empresa, Lista, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    procedure ActualizaTerminalesGTI(const Lista: WideString); {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function ConsultaMensaje(const Filtro: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function ConsultaMensajeTerminal(const Filtro: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function ConsultaTerminal(const Filtro: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function GrabaMensaje(oDelta: OleVariant; UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function GrabaMensajeTerminal(oDelta: OleVariant; UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function GrabaTerminal(oDelta: OleVariant; UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function ImportaTerminales(Parametros: OleVariant; UsuarioTerminal: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function ExisteHuellaRegistrada(Empleado: Integer; const Empresa: WideString): WordBool; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    procedure ReiniciaTerminal(Terminal: Integer; const Empresa: WideString); {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function ExisteEmpleadoEnGrupo(const Grupo: WideString): WordBool; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function ObtieneTemplates(Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    procedure EliminaHuellas(const Empresa: WideString; Empleado, Invitador, Usuario: Integer); {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    procedure InsertaHuella(Parametros: OleVariant); {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function ReportaHuella(Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function ExisteHuellaInvitador(const Empresa: WideString; Invitador: Integer): WordBool; {$ifndef DOS_CAPAS} safecall;  {$endif}
    {$endif}
    function ExistenEstructurasTimbradoComparte: WordBool; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function CrearEstructurasTimbradoComparte: WordBool; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function ExistenEstructurasTimbradoEmpresa(Empresa: OleVariant): WordBool; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function CrearEstructurasTimbradoEmpresa(Empresa: OleVariant): WordBool; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function GetCuentasTimbrado: OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaCuentasTimbrado(oDelta: OleVariant;out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    procedure CrearBitacora(Empresa: OleVariant; Tipo: Integer;Parametros: OleVariant); {$ifndef DOS_CAPAS} safecall; {$endif}
 end;

implementation

uses ZetaCommonTools,
     ZetaServerTools,
     ZEvaluador,
     ZetaQrExpr,
     ZGlobalTress,
     DSuperASCII,
     ZetaTipoEntidad,
    {$ifdef DEBUGSENTINEL}
     {$ifndef DOS_CAPAS}
     ZetaLicenseMgr,
     {$endif}
     {$endif}
     ZetaRegistryServer;
     //DSuperReporte;

const
     {$ifdef ANTES }
     Q_SIST_BITACORA = 1;
     {$endif }
     Q_SIST_EMPRESAS = 2;
     Q_SIST_GRUPOS = 3;
     Q_SIST_POLL = 4;
     Q_SIST_USUARIOS = 5;
     Q_SIST_IMPRESORAS = 6;
     Q_SIST_USER_SUPER = 7;
     Q_SIST_ACCESOS = 8;
     Q_SIST_USER_ARBOL = 9;
     Q_ACTIVA_APAGA_USUARIOS_SIN_RESTRICCION = 10;
     Q_BORRA_PROCESOS = 11;
     Q_BORRA_BITACORA = 12;
     Q_GET_USUARIO_SUSCRIP = 13;
     Q_CLAVES_LEER = 14;
     Q_GET_GRUPOS_ACCESOS = 15;
     Q_GET_USUARIO_AREAS = 16;
     Q_DEL_USUARIO_AREAS = 17;
     Q_BORRA_BITACORA_PROCESOS = 18;
     Q_GET_EMPRESAS = 19;
     Q_GET_EMPRESAS_ACCESOS = 20;
     Q_ACTIVA_APAGA_USUARIOS = 21;
     Q_CHECAGRUPOS = 22;
     Q_NUEVO_USUARIO = 23;
     Q_GET_EMPRESAS_SIN_RESTRICCION = 24;
     Q_COMPANYS_COUNT = 25;
     Q_COMPANYS_LIST = 26;
     Q_EMPLEADOS_COMPANY = 27;
     Q_BORRA_EMPLEADOS_COMPARTE = 28;
     Q_INSERTA_EMP_ID = 29;
     Q_INVITADORES_COMPANY = 30;
     Q_INSERTA_INV_EMP_ID = 31;
     Q_CODIGO_REPETIDO = 32;
     Q_BORRA_TABLA = 33;
     Q_ACTUALIZA_DICCION = 34;
     Q_NIVEL0 = 35;
     Q_BITACORA = 36;
     Q_UPDATE_USER_COLABORA = 37;
     Q_USER_ROL_INSERT = 38;
     Q_ROL_USUARIOS_DELETE = 39;
     Q_ROL_USUARIOS = 40;
     Q_GET_USER_ROLES = 41;
     Q_DEL_USUARIO_ROLES = 42;
     Q_CLASIFI_EMPRESA = 43;
     Q_ACCESOS_ADICIONALES = 44;
     Q_BORRA_GRUPO_AD = 45;
     Q_GRUPO_ACCESOS = 46;
     Q_BORRA_BITACORA_KIOSCO = 47; {OP: 06/06/08}
     Q_BORRA_BITACORA_CAFETERIA = 48; {OP: 06/06/08}
     Q_ACTUALIZA_CODIGO_EMPRESA = 49;
     Q_COMPANYS_LIST_DISTINCT = 50;
     Q_BORRA_CODIGO_EMPRESA = 51;
     Q_SIST_LST_DISPOSITIVOS = 52;
     Q_DISP_DISPONIBLES = 53;
     Q_DISP_ASIGNADOS = 54;
     Q_BORRA_DISP = 55;
     Q_ACCARBOL = 56;
     Q_ACTUALIZA_EMPRESA_DER = 57;
     Q_BORRA_EMPRESA_DER = 58;
     Q_COMPANY = 59;
     Q_SIST_OBTEN_MAX_BIO = 60; // SYNERGY
     Q_SIST_BIT_BIO = 61; // SYNERGY
     Q_SIST_DEPURA_BIT_BIO = 62; // SYNERGY
     Q_SIST_ACTUALIZA_BIO_BORRA = 63; // SYNERGY
     Q_SIST_ACTUALIZA_BIO_AGREGA = 64; // SYNERGY
     Q_CONSULTA_LISTA_GRUPOS = 65; // SYNERGY
     Q_CONSULTA_TERM_POR_GRUPO = 66; // SYNERGY
     Q_AGREGA_LISTA_TERM_POR_GRUPO = 67; // SYNERGY
     Q_BORRA_LISTA_TERM_POR_GRUPO = 68; // SYNERGY
     Q_AGREGA_NUM_BIOMETRICO = 69; // SYNERGY
     Q_EXISTE_HUELLA_EMP = 70; // SYNERGY
     Q_AGREGA_TERMINALES = 71; // SYNERGY
     Q_REINICIA_TERMINAL = 72; // SYNERGY
     Q_REINICIA_TERM_MSG_FECHA = 73; // SYNERGY
     Q_EXISTE_EMPLEADO_EN_GRUPO = 74; // SYNERGY
     Q_BORRA_HUELLAGTI = 75; // SYNERGY
     Q_HUELLA_OBTENER_TODOS = 76; // BIOMETRICO
     Q_HUELLA_INSERTA = 77; // BIOMETRICO
     Q_HUELLA_BORRA = 78; // BIOMETRICO
     Q_SIST_ACTUALIZA_EMPLEADO = 79;
     Q_HUELLA_OBTENER_MAQUINA = 80;
     Q_HUELLA_REPORTA_INSERTA = 81;
     Q_HUELLA_REPORTA_ELIMINA = 82;
     Q_EXISTE_HUELLA_INV = 83;

     Q_SIST_GET_CUENTAS_TRIMBRADO = 84;
     Q_PATCH_TIMBRADO_COMPARTE =85;     



{$R *.DFM}

{ ************ TdmServerSistema ************ }
 class procedure TdmServerSistemaTimbrado.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
     if Register then
     begin
          inherited UpdateRegistry(Register, ClassID, ProgID);
          EnableSocketTransport(ClassID);
          EnableWebTransport(ClassID);
     end
     else
     begin
          DisableSocketTransport(ClassID);
          DisableWebTransport(ClassID);
          inherited UpdateRegistry(Register, ClassID, ProgID);
     end;
end;

{$ifdef DEBUGSENTINEL}
procedure TdmServerSistemaTimbrado.dmServerSistemaCreate(Sender: TObject);
{$ifndef DOS_CAPAS}
var
   oLicenseMgr : TLicenseMgr;
{$endif}
begin
     oAutoServer := ZetaSQLBroker.GetAutoServer;
     {$ifndef DOS_CAPAS}
     OnActivate := ServerActivate;
     OnDeactivate := ServerDeactivate;
     {$endif}
     {$ifdef DOS_CAPAS}
     ZetaSQLBroker.ReadAutoServer( oAutoServer, NIL, NIL );
     {$else}
     oLicenseMgr := TLicenseMgr.Create( oZetaProvider );
     try
        ZetaSQLBroker.ReadAutoServer( oAutoServer, oLicenseMgr.AutoGetData, oLicenseMgr.AutoSetData );
     finally
            FreeAndNil( oLicenseMgr );
     end;
     {$endif}
     oZetaProviderComparte := DZetaServerProvider.GetZetaProvider( Self );
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
end;
{$else}


procedure TdmServerSistemaTimbrado.dmServerSistemaTimbradoCreate(Sender: TObject);
begin
     {$ifndef DOS_CAPAS}
     OnActivate := ServerActivate;
     OnDeactivate := ServerDeactivate;
     {$endif}
     oZetaProviderComparte := DZetaServerProvider.GetZetaProvider( Self );
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
     oAutoServer := ZetaSQLBroker.GetAutoServer;
     ZetaSQLBroker.ReadAutoServer( oAutoServer, nil, nil );
end;
{$endif}


procedure TdmServerSistemaTimbrado.dmServerSistemaTimbradoDestroy(Sender: TObject);
begin
     FreeAndNil( FUsuarioData );
     ZetaSQLBroker.FreeZetaCreator( oZetaCreator );
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
     DZetaServerProvider.FreeZetaProvider( oZetaProviderComparte );
     ZetaSQLBroker.FreeAutoServer( oAutoServer );
end;

{$ifdef DOS_CAPAS}
procedure TdmServerSistemaTimbrado.CierraEmpresa;
begin
end;
{$else}
procedure TdmServerSistemaTimbrado.ServerActivate(Sender: TObject);
begin
     oZetaProviderComparte.Activate;
     oZetaProvider.Activate;
end;

procedure TdmServerSistemaTimbrado.ServerDeactivate(Sender: TObject);
begin
     oZetaProviderComparte.Deactivate;
     oZetaProvider.Deactivate;
end;                                                                                                                                                  
{$endif}

{ ***************** PARTE PRIVADA *******************************}

function TdmServerSistemaTimbrado.GetScript( const iScript: Integer ): String;
begin
     case iScript of
     {$ifdef ANTES}
          Q_SIST_BITACORA: Result := 'select BI_FECHA, BI_HORA, BI_TIPO, BI_NUMERO, '+
                                     'BI_TEXTO, BI_PROC_ID, US_CODIGO, '+
                                     'CB_CODIGO, BI_DATA, BI_CLASE, BI_FEC_MOV ' +
                                     'from BITACORA where ' +
                                     '( BI_FECHA BETWEEN ''%s'' and ''%s'' ) '+
                                     '@BITACORA @EMPLEADO @USUARIO '+
                                     'order by BI_FECHA DESC, BI_HORA desc';
     {$endif}
          Q_SIST_EMPRESAS: Result := 'select CM_CODIGO, CM_NOMBRE, CM_ALIAS, CM_USRNAME, '+
                                     'CM_PASSWRD, CM_ACUMULA, CM_CONTROL, CM_EMPATE, CM_USACAFE, CM_USACASE '+
                                     'from COMPANY order by CM_CODIGO';
          Q_SIST_GRUPOS: Result := 'select GR_CODIGO, GR_DESCRIP from GRUPO order by GR_CODIGO';
          Q_SIST_POLL: Result := 'select PO_EMPRESA,PO_NUMERO,PO_LETRA,PO_FECHA,PO_HORA, PO_LINX '+
                                 'from POLL order by PO_EMPRESA, PO_NUMERO, PO_FECHA, PO_HORA';
          Q_SIST_USUARIOS: Result := 'select US_CODIGO, US_CORTO, US_NOMBRE, US_PASSWRD, US_DENTRO, '+
                                     'GRUPO.GR_CODIGO, GRUPO.GR_DESCRIP, US_NIVEL, US_BLOQUEA, US_CAMBIA, '+
                                     'US_FEC_IN, US_FEC_OUT, US_ARBOL, US_FORMA, US_BIT_LST, '+
                                     'US_DOMAIN, US_ACTIVO, US_PORTAL, CB_CODIGO '+
                                     'from USUARIO '+
                                     'left outer join GRUPO on ( GRUPO.GR_CODIGO = USUARIO.GR_CODIGO ) '+
                                     'order by US_CODIGO';
          Q_SIST_IMPRESORAS: Result := 'select PI_NOMBRE, PI_EJECT, '+
                                       'PI_CHAR_10, PI_CHAR_12, PI_CHAR_17, '+
                                       'PI_EXTRA_1, PI_EXTRA_2, PI_RESET, '+
                                       'PI_UNDE_ON, PI_UNDE_OF, '+
                                       'PI_BOLD_ON, PI_BOLD_OF, '+
                                       'PI_6_LINES, PI_8_LINES, '+
                                       'PI_ITAL_ON, PI_ITAL_OF, '+
                                       'PI_LANDSCA from PRINTER order by PI_NOMBRE';
          Q_SIST_USER_SUPER: Result := 'select US_CODIGO, CB_NIVEL from SUPER where ( US_CODIGO = :Usuario ) order by US_CODIGO, CB_NIVEL';
          Q_SIST_ACCESOS: Result := 'select GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO from ACCESO where '+
                                    '( GR_CODIGO = :Grupo ) and ( CM_CODIGO = :Empresa )';
          Q_SIST_USER_ARBOL: Result := 'select US_CODIGO, AB_ORDEN, AB_NIVEL, AB_NODO, AB_DESCRIP from ARBOL where ( US_CODIGO = :Usuario ) order by AB_ORDEN';
          Q_ACTIVA_APAGA_USUARIOS_SIN_RESTRICCION: Result := 'update USUARIO set US_BLOQUEA = ''%s'' '+
                                                             'where ( US_CODIGO <> %d )';
          Q_BORRA_PROCESOS: Result := 'delete from PROCESO where ( PC_FEC_INI < ''%s'' ) and PC_PROC_ID in( %s )';
          Q_BORRA_BITACORA: Result:= 'delete from BITACORA where ( BI_FECHA < ''%s'' ) %s';
          Q_GET_USUARIO_SUSCRIP : Result := 'select SUSCRIP.US_CODIGO, SUSCRIP.RE_CODIGO, REPORTE.RE_NOMBRE, SUSCRIP.SU_FRECUEN, SUSCRIP.SU_VACIO, SUSCRIP.SU_ENVIAR ' +
                                            'from SUSCRIP ' +
                                            'left outer join REPORTE ON REPORTE.RE_CODIGO = SUSCRIP.RE_CODIGO '+
                                            'where SUSCRIP.%s = %d   ' +
                                            'order BY REPORTE.RE_NOMBRE';
          Q_CLAVES_LEER: Result := 'select CL_TEXTO from CLAVES where ( CL_NUMERO = %d )';
          Q_GET_GRUPOS_ACCESOS: Result := 'select A.GR_CODIGO, A.CM_CODIGO, G.GR_DESCRIP, C.CM_NOMBRE from ACCESO A '+
                                          'left outer join GRUPO G on ( G.GR_CODIGO = A.GR_CODIGO ) '+
                                          'left outer join COMPANY C on ( C.CM_CODIGO = A.CM_CODIGO ) '+
                                          'where ( ( A.GR_CODIGO <> %d ) or ( A.CM_CODIGO <> ''%s'' ) ) and ( %s ) '+
                                          'group by A.GR_CODIGO, G.GR_DESCRIP, A.CM_CODIGO, C.CM_NOMBRE ' +
                                          'ORDER by A.GR_CODIGO, G.GR_DESCRIP, A.CM_CODIGO, C.CM_NOMBRE ';
          Q_GET_USUARIO_AREAS: Result := 'select TB_CODIGO CB_AREA, TB_ELEMENT, '+
                                          '( select US_CODIGO from SUP_AREA S where ( S.US_CODIGO = %d ) and ( S.CB_AREA = A.TB_CODIGO ) ) US_CODIGO '+
                                          'from AREA A Order by A.TB_ELEMENT';
          Q_DEL_USUARIO_AREAS: Result := 'delete from SUP_AREA where ( US_CODIGO = %d )';
          Q_BORRA_BITACORA_PROCESOS: Result := 'delete from BITACORA where BI_NUMERO in( select PC_NUMERO from PROCESO '+
                                               'where ( PC_FEC_INI < ''%s'' ) and ( PC_PROC_ID in( %s ) ) )';
          Q_BORRA_BITACORA_KIOSCO: Result := 'delete from BITKIOSCO where BI_FECHA < ''%s'' ';{OP: 07/06/08}
          Q_BORRA_BITACORA_CAFETERIA: Result := 'delete from BITCAFE where BC_FECHA < ''%s'' ';{OP: 07/06/08}
          Q_GET_EMPRESAS: Result := 'select C.CM_CODIGO, C.CM_NOMBRE, C.CM_ACUMULA, C.CM_CONTROL, C.CM_EMPATE, '+
                                    'C.CM_ALIAS, C.CM_PASSWRD, C.CM_USRNAME, C.CM_USACAFE, C.CM_USACASE, C.CM_NIVEL0, C.CM_DATOS, C.CM_DIGITO, C.CM_KCLASIFI, ' +
                                    'C.CM_KCONFI, C.CM_KUSERS '+
                                    'from COMPANY C '+
                                    'where ( %0:s ) and ( ( %1:d = %2:d ) or ( C.CM_CODIGO in( select DISTINCT( CM_CODIGO ) '+
                                    'from ACCESO where ( AX_DERECHO > 0 ) and '+
{$ifdef INTERBASE}
                                    '( GR_CODIGO in ( select GR_HIJO from GET_DESCENDENCIA( %1:d ) ) ) '+
{$endif}
{$ifdef MSSQL}
                                    '( GR_CODIGO in ( select GR_HIJO from dbo.GET_DESCENDENCIA( %1:d ) ) ) '+
{$endif}
                                    ') ) ) order by C.CM_CODIGO';
          Q_GET_EMPRESAS_ACCESOS: Result := 'select C.CM_CODIGO, C.CM_NOMBRE, C.CM_ALIAS, C.CM_USRNAME, '+
                                                   'C.CM_PASSWRD, C.CM_ACUMULA, C.CM_CONTROL, C.CM_EMPATE, C.CM_USACAFE, C.CM_USACASE, '+
                                                   'C.CM_NIVEL0, C.CM_DATOS, C.CM_DIGITO, '+
                                                    '( select count(*) from ACCESO A where ( A.GR_CODIGO = %d ) and ( A.CM_CODIGO = C.CM_CODIGO ) '+
                                                    'and ( A.AX_DERECHO > 0 ) ) ACCESOS '+
                                                    'from company C '+
                                                    'where ( %s ) order by C.CM_CODIGO';
          Q_ACTIVA_APAGA_USUARIOS: Result := 'update USUARIO set US_BLOQUEA = ''%s'' '+
                                             'where ( US_CODIGO <> %d ) and ' +
{$ifdef INTERBASE}
                                             'GR_CODIGO in ( select GR_HIJO from GET_DESCENDENCIA( %d ) )';
{$endif}
{$ifdef MSSQL}
                                             'GR_CODIGO in ( select GR_HIJO from dbo.GET_DESCENDENCIA( %d ) )';
{$endif}
{$ifdef INTERBASE}
          Q_CHECAGRUPOS: Result := 'execute procedure CHECK_ASCENDENCIA( %d )';
{$endif}
{$ifdef MSSQL}
          Q_CHECAGRUPOS: Result := 'exec dbo.CHECK_ASCENDENCIA %d';
{$endif}
          Q_NUEVO_USUARIO: Result := 'Select MAX(US_CODIGO) US_CODIGO from USUARIO';
          Q_GET_EMPRESAS_SIN_RESTRICCION: Result := 'select CM_CODIGO, CM_NOMBRE, CM_ACUMULA, CM_CONTROL, CM_EMPATE, '+
                                                    'CM_ALIAS, CM_PASSWRD, CM_USRNAME, CM_USACAFE, CM_USACASE, CM_NIVEL0, CM_DATOS '+
                                                    'from COMPANY '+
                                                    'where ( %s ) order by CM_CODIGO';
          Q_COMPANYS_COUNT: Result := 'select COUNT(*) CUANTOS '+
                                      'from COMPANY '+
                                      'where ( %s ) and '+
                                      '( CM_DIGITO is NOT NULL ) and '+
                                      '( CM_DIGITO <> '' '' )';
          Q_COMPANYS_LIST: Result := 'select CM_NOMBRE, CM_ALIAS, CM_USRNAME, CM_PASSWRD, CM_NIVEL0, CM_DATOS, CM_CODIGO '+
                                     'from COMPANY '+
                                     'where ( %s ) and '+
                                     '( CM_DIGITO is NOT NULL ) and '+
                                     '( CM_DIGITO <> '' '' ) '+
                                     'order by CM_CODIGO';
          Q_EMPLEADOS_COMPANY: Result := 'select CB_CODIGO, CB_ID_NUM from COLABORA '+
                                        'where ( CB_ID_NUM is NOT NULL ) and '+
                                        '( CB_ID_NUM <> '' '' ) '+
                                        'order by CB_CODIGO';
          Q_BORRA_EMPLEADOS_COMPARTE: Result := 'delete from EMP_ID '+
                                               'where ( CM_CODIGO = :CM_CODIGO )';
          Q_INSERTA_EMP_ID: Result := 'insert into EMP_ID( ID_NUMERO, CM_CODIGO, CB_CODIGO, IV_CODIGO ) '+
                                      'values( :ID_NUMERO, :CM_CODIGO, :CB_CODIGO, 0 )';
          Q_INVITADORES_COMPANY: Result := 'select IV_CODIGO, IV_ID_NUM from INVITA '+
                                         'where ( IV_ID_NUM is NOT NULL ) and '+
                                         '( IV_ID_NUM <> '' '' ) '+
                                         'order by IV_CODIGO';
          Q_INSERTA_INV_EMP_ID: Result := 'insert into EMP_ID( ID_NUMERO, CM_CODIGO, CB_CODIGO, IV_CODIGO ) '+
                                          'values( :ID_NUMERO, :CM_CODIGO, 0, :IV_CODIGO )';
          Q_CODIGO_REPETIDO: Result := 'select CM_CODIGO, CB_CODIGO, IV_CODIGO from EMP_ID '+
                                        'where ( ID_NUMERO = %s )';
          Q_BORRA_TABLA: Result := 'delete from %s';
          Q_ACTUALIZA_DICCION: Result := 'execute procedure SP_REFRESH_DICCION';
          Q_NIVEL0: Result := 'SELECT TB_CODIGO, TB_ELEMENT,%s TB_SUB_CTA FROM NIVEL0';
          Q_BITACORA: Result := 'select BI_FECHA, BI_HORA, BI_TIPO, BI_NUMERO, '+
                                'BI_TEXTO, BI_PROC_ID, US_CODIGO, CB_CODIGO, '+
                                'BI_DATA, BI_CLASE, BI_FEC_MOV ' +
                                'from BITACORA where ' +
                                '( %0:s >= %1:s and %0:s < %2:s ) and '+
                                '( ( %3:d = -1 ) or ( BI_TIPO = %3:d ) ) and '+
                                '( ( %4:d <= 0 ) or ( BI_CLASE = %4:d  ) ) and '+
                                '( ( %5:d = 0 ) or ( US_CODIGO = %5:d ) ) and '+
                                '( ( %6:d = -1 ) or ( BI_NUMERO = %6:d ) ) '+
                                'order by BI_FECHA DESC, BI_HORA DESC';
          
          Q_USER_ROL_INSERT: Result := 'insert into USER_ROL( RO_CODIGO, US_CODIGO ) values ( :RO_CODIGO, :US_CODIGO )';
          Q_ROL_USUARIOS_DELETE: Result := 'delete from USER_ROL where ( USER_ROL.RO_CODIGO = ''%s'' )';
          Q_ROL_USUARIOS: Result := 'select USER_ROL.RO_CODIGO, USER_ROL.US_CODIGO, ROL.RO_NOMBRE, USUARIO.US_NOMBRE from USER_ROL '+
                                    'left outer join ROL on ( ROL.RO_CODIGO = USER_ROL.RO_CODIGO ) '+
                                    'left outer join USUARIO on ( USUARIO.US_CODIGO = USER_ROL.US_CODIGO ) '+
                                    'where ( USER_ROL.RO_CODIGO = ''%s'' ) order by USER_ROL.US_CODIGO';
          Q_GET_USER_ROLES: Result := 'select UR.US_CODIGO,UR.RO_CODIGO,UR.UR_PPAL,R.RO_NOMBRE from USER_ROL UR '+
                                      'left outer join ROL R on ( R.RO_CODIGO = UR.RO_CODIGO ) '+
                                      'where UR.US_CODIGO = %d';
          Q_DEL_USUARIO_ROLES: Result := 'delete from USER_ROL where US_CODIGO = %d';
          Q_CLASIFI_EMPRESA: Result := 'select RC_ORDEN, RC_CODIGO, RC_NOMBRE from R_CLASIFI order by RC_ORDEN';
          Q_ACCESOS_ADICIONALES: Result := 'select * from GR_AD_ACC where CM_CODIGO = ''%s'' and GR_CODIGO= %d';
          Q_BORRA_GRUPO_AD: Result := 'delete from gr_ad_acc where GX_CODIGO NOT IN (select GX_CODIGO from grupo_ad)';
          Q_GRUPO_ACCESOS:  Result := 'select GRUPO_AD.*, GR_AD_ACC.GX_DERECHO from GRUPO_AD '+
                                      'left outer join GR_AD_ACC ON GR_AD_ACC.CM_CODIGO = %0:s '+
                                            'and GR_AD_ACC.GR_CODIGO = %1:d '+
                                            'and GR_AD_ACC.GX_CODIGO = GRUPO_AD.GX_CODIGO '+
                                            'where GRUPO_AD.GX_CODIGO IN ( select GR_AD_ACC.GX_CODIGO from GR_AD_ACC '+
                                      'where CM_CODIGO = %0:s and GR_CODIGO = %1:d and ( GX_DERECHO > 0 %2:s ) ) ';
          Q_ACTUALIZA_CODIGO_EMPRESA: Result :=
                                               {$IFDEF MSSQL}
                                               'if ( DBO.SP_EXISTE_TABLA(''%0:s'') = 1 ) '+
                                               'BEGIN '+
                                               {$endif}
                                               ' update %0:s set GR_CODIGO = %1:d where GR_CODIGO = %2:d '{$IFDEF MSSQL}+{$ELSE};{$ENDIF}
                                               {$IFDEF MSSQL}
                                               'END ';
                                               {$ENDIF}
          Q_COMPANYS_LIST_DISTINCT :  Result := 'select DISTINCT(CM_DATOS),CM_NOMBRE, CM_ALIAS, CM_USRNAME, CM_PASSWRD, CM_NIVEL0,  CM_CODIGO '+
                                                'from COMPANY '+
                                                'where ( %s ) and '+
                                                '( CM_DIGITO is NOT NULL ) and '+
                                                '( CM_DIGITO <> '' '' ) '+
                                                'order by CM_CODIGO';
          Q_BORRA_CODIGO_EMPRESA : Result :=   {$IFDEF MSSQL}
                                               'if ( DBO.SP_EXISTE_TABLA(''%0:s'') = 1 ) '+
                                               'BEGIN '+
                                               {$endif}
                                               ' delete from %0:s where GR_CODIGO = %1:d '{$IFDEF MSSQL}+{$ELSE};{$ENDIF}
                                               {$IFDEF MSSQL}
                                               'END ';
                                               {$ENDIF}

          Q_SIST_LST_DISPOSITIVOS : Result := 'SELECT DI_NOMBRE, DI_TIPO, DI_DESCRIP, DI_NOTA, DI_IP from DISPOSIT order by DI_TIPO, DI_NOMBRE asc';
          Q_DISP_DISPONIBLES : Result := 'select DI_NOMBRE, DI_TIPO, DI_DESCRIP, DI_NOTA, DI_IP from DISPOSIT where DI_NOMBRE not in( select DI_NOMBRE from DISXCOM where CM_CODIGO = ''%s''  )';
          Q_DISP_ASIGNADOS : Result := 'select CM_CODIGO, DI_NOMBRE, DI_TIPO from DISXCOM where CM_CODIGO = ''%s'' ';
          Q_BORRA_DISP : Result := 'delete from DISXCOM where CM_CODIGO = ''%s'' ';
          Q_ACCARBOL : Result := 'select dbo.TF(288,V_ACCARBOL.AA_SOURCE) AA_SOURCE, AX_NUMERO,AA_DESCRIP,dbo.QuitaAcentos(AA_DESCRIP) FILTRO_DES, '+
                                 'MO_NOMBRE from V_ACCARBOL                  '+
                                 'left outer join R_MODULO on R_MODULO.MO_CODIGO = AA_MODULO ';
          Q_ACTUALIZA_EMPRESA_DER : Result :=  'update %s set CM_CODIGO = ''%s'' where CM_CODIGO = ''%s'' ';
          Q_BORRA_EMPRESA_DER : Result :=  'delete from %s where CM_CODIGO = ''%s'' ';
          Q_COMPANY : Result := 'Select CM_DATOS,CM_NOMBRE, CM_ALIAS, CM_USRNAME, CM_PASSWRD, CM_NIVEL0, CM_CODIGO from COMPANY WHERE CM_CODIGO = ''%s''';
          Q_SIST_OBTEN_MAX_BIO : Result := 'select max(ID_NUMERO) as MAXIMO from EMP_BIO'; // SYNERGY
          Q_SIST_BIT_BIO : Result := 'select WS_FECHA, CH_RELOJ, WS_CHECADA, CB_CODIGO, WS_MENSAJE, CM_CODIGO, WS_TIPO from WS_BITAC %s order by WS_FECHA desc, CM_CODIGO'; // SYNERGY
          Q_SIST_DEPURA_BIT_BIO : Result := 'delete from WS_BITAC where ( WS_FECHA >= ''%s'' ) and ( WS_FECHA < ''%s'' ) %s'; // SYNERGY
          Q_SIST_ACTUALIZA_BIO_BORRA : Result := 'delete from EMP_BIO where CM_CODIGO=''%s'' '; // SYNERGY
          Q_SIST_ACTUALIZA_BIO_AGREGA : Result := 'insert into EMP_BIO (ID_NUMERO, CB_CODIGO, CM_CODIGO, GP_CODIGO, IV_CODIGO) values (%d, %d, ''%s'', ''%s'', %d)'; // SYNERGY
          Q_CONSULTA_LISTA_GRUPOS : Result := 'select GP_CODIGO, GP_DESCRIP from GRUPOTERM order by GP_CODIGO'; // SYNERGY
          Q_CONSULTA_TERM_POR_GRUPO : Result := 'select TE_CODIGO, GP_CODIGO from TERM_GPO %s order by TE_CODIGO'; // SYNERGY
          Q_AGREGA_LISTA_TERM_POR_GRUPO : Result := 'insert into TERM_GPO (TE_CODIGO, GP_CODIGO) values (''%s'', ''%s'')'; // SYNERGY
          Q_BORRA_LISTA_TERM_POR_GRUPO : Result := 'delete TERM_GPO where GP_CODIGO = ''%s'''; // SYNERGY
          Q_AGREGA_NUM_BIOMETRICO: Result := 'update COLABORA set CB_ID_BIO=:CB_ID_BIO, CB_GP_COD=:CB_GP_COD where CB_CODIGO=:CB_CODIGO'; // SYNERGY
          Q_EXISTE_HUELLA_EMP: Result := 'select top 1 CB_CODIGO from HUELLAGTI where upper(CM_CODIGO)=upper(''%s'') and CB_CODIGO=%d'; // SYNERGY
          Q_AGREGA_TERMINALES: Result := 'exec dbo.WS_ALTA_TERMINAL ''%s'''; // SYNERGY
          Q_REINICIA_TERMINAL: Result := 'delete from TERM_HUELLA %s'; // SYNERGY
          Q_REINICIA_TERM_MSG_FECHA: Result:= 'update TERM_MSG set TM_NEXT=''18991230'' where DM_CODIGO=4 %s'; // SYNERGY
          Q_EXISTE_EMPLEADO_EN_GRUPO: Result := 'select top 1 GP_CODIGO from EMP_BIO where GP_CODIGO=''%s'''; // SYNERGY
          //BIOMETRICO
          Q_BORRA_HUELLAGTI: Result := 'delete from HUELLAGTI where CB_CODIGO=%d and CM_CODIGO=''%s'''; // SYNERGY
          Q_HUELLA_OBTENER_TODOS: Result := 'SELECT HU_ID, ID_NUMERO, CM_CODIGO, CB_CODIGO, HU_INDICE, HU_HUELLA, IV_CODIGO, HU_TIMEST, TIPO FROM V_HUELLAS'; //BIOMETRICO
          Q_HUELLA_INSERTA: Result := 'exec SP_HUELLAGTI_INSERTA ''%s'',%d,%d,''%s'',%d, ''%s'''; //BIOMETRICO
          Q_HUELLA_BORRA: Result := 'delete from HUELLAGTI where CM_CODIGO=''%s'' '; //BIOMETRICO
          Q_SIST_ACTUALIZA_EMPLEADO: Result := 'update HUELLAGTI set CB_CODIGO = %d where CM_CODIGO = ''%s'' and IV_CODIGO = %d'; //BIOMETRICO
          Q_HUELLA_OBTENER_MAQUINA: Result := 'SELECT V.HU_ID, V.ID_NUMERO, V.CM_CODIGO, V.CB_CODIGO, V.HU_INDICE, V.HU_HUELLA, V.IV_CODIGO, V.HU_TIMEST, V.TIPO, C.CH_CODIGO FROM V_HUELLAS AS V LEFT OUTER JOIN COM_HUELLA AS C ON V.HU_ID = C.HU_ID AND C.CH_CODIGO = ''%s'' WHERE (C.CH_CODIGO IS NULL) '; //Mejoras de Biometrico
          Q_HUELLA_REPORTA_INSERTA: Result := 'exec SP_REPORTAHUELLA ''%s'', %s';
          Q_HUELLA_REPORTA_ELIMINA: Result := 'delete from COM_HUELLA where CH_CODIGO = ''%s''';
          Q_EXISTE_HUELLA_INV: Result := 'select top 1 IV_CODIGO from HUELLAGTI where upper(CM_CODIGO)=upper(''%s'') and IV_CODIGO=%d'; // BIOMETRICO
          Q_SIST_GET_CUENTAS_TRIMBRADO : Result := 'select CT_CODIGO,CT_DESCRIP,CT_ID,CT_PASSWRD,CT_ACTIVO,US_CODIGO,US_FEC_MOD from CUENTATIM order by CT_CODIGO';
          Q_PATCH_TIMBRADO_COMPARTE : Result := 'CREATE TABLE CuentaTim(CT_CODIGO Codigo, CT_DESCRIP Descripcion, CT_ID FolioGrande, CT_PASSWRD Formula, CT_ACTIVO Booleano, US_CODIGO Usuario, US_FEC_MOD Fecha, CONSTRAINT PK_CuentaTim  PRIMARY KEY( CT_CODIGO)) ';
     else
         Result := '';
     end;
end;

procedure TdmServerSistemaTimbrado.SetTablaInfo( const eTabla: ETipoSistema );
begin
     with oZetaProvider.TablaInfo do
     begin
          case eTabla of
              eArbol: SetInfo( 'ARBOL',
                               'US_CODIGO, AB_ORDEN, AB_NIVEL, AB_NODO, AB_DESCRIP',
                               'US_CODIGO, AB_ORDEN' );
              eEmpresas: SetInfo( 'COMPANY',
                                  'CM_CODIGO, CM_NOMBRE, CM_ACUMULA, CM_CONTROL, CM_EMPATE, CM_ALIAS, CM_PASSWRD, CM_USRNAME, CM_USACAFE, CM_USACASE, CM_NIVEL0, CM_DATOS, CM_DIGITO, CM_KCLASIFI, CM_KCONFI, CM_KUSERS',
                                  'CM_CODIGO' );
              eGruposUsuarios: SetInfo( 'GRUPO',
                                        'GR_CODIGO, GR_DESCRIP, GR_PADRE',
                                        'GR_CODIGO' );
              eUsuarios: SetInfo( 'USUARIO',
                                  'US_CODIGO, US_CORTO, GR_CODIGO, US_NIVEL, US_NOMBRE, US_PASSWRD, US_BLOQUEA, US_CAMBIA, US_FEC_IN, US_FEC_OUT, US_FEC_SUS, US_DENTRO, US_ARBOL, US_FORMA, US_BIT_LST, US_FEC_PWD, US_EMAIL, US_FORMATO, US_LUGAR, US_MAQUINA, CM_CODIGO, CB_CODIGO, ' +
                                  'US_DOMAIN, US_PORTAL, US_ACTIVO, US_JEFE ',
                                  'US_CODIGO' );
              ePoll: Setinfo( 'POLL',
                              'PO_LINX, PO_EMPRESA, PO_NUMERO, PO_FECHA, PO_HORA, PO_LETRA',
                              'PO_EMPRESA, PO_NUMERO, PO_FECHA, PO_HORA' );
              eImpresoras: SetInfo( 'PRINTER',
                                    'PI_NOMBRE, PI_EJECT, PI_CHAR_10, PI_CHAR_12, PI_CHAR_17, PI_EXTRA_1, PI_EXTRA_2, PI_RESET, PI_UNDE_ON, PI_UNDE_OF, PI_BOLD_ON, PI_BOLD_OF, PI_6_LINES, PI_8_LINES, PI_ITAL_ON, PI_ITAL_OF, PI_LANDSCA',
                                    'PI_NOMBRE' );
              eAccesos: SetInfo( 'ACCESO',
                                 'GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO',
                                 'GR_CODIGO, CM_CODIGO, AX_NUMERO' );
              eUserSuper: SetInfo( 'SUPER',
                                   'US_CODIGO, CB_NIVEL',
                                   'US_CODIGO, CB_NIVEL' );
              eNivel0 : SetInfo( 'NIVEL0',
                                 'TB_CODIGO, TB_ELEMENT',
                                 'TB_CODIGO' );
              eSuscrip: SetInfo( 'SUSCRIP',
                                 'US_CODIGO, RE_CODIGO, SU_FRECUEN, SU_ENVIAR, SU_VACIO',
                                 'US_CODIGO, RE_CODIGO' );
              eUsuarioAreas: SetInfo( 'SUP_AREA',
                                      'US_CODIGO, CB_AREA',
                                      'US_CODIGO, CB_AREA' );
              eGruposAdic: SetInfo( 'GRUPO_AD',
                                    'GX_CODIGO, GX_TITULO, GX_POSICIO',
                                    'GX_CODIGO' );
              eCamposAdic: SetInfo( 'CAMPO_AD',
                                    'CX_NOMBRE,GX_CODIGO,CX_POSICIO,CX_TITULO,CX_TIPO,CX_DEFAULT,CX_MOSTRAR,CX_ENTIDAD,CX_OBLIGA',
                                    'CX_NOMBRE' );
              eRoles: SetInfo(    'ROL',
                                  'RO_CODIGO, RO_NOMBRE, RO_DESCRIP, RO_NIVEL ',
                                  'RO_CODIGO' );
              eUserRol: SetInfo(  'USER_ROL',
                                  'RO_CODIGO, US_CODIGO, UR_PPAL',
                                  'RO_CODIGO, US_CODIGO' );
              eAccesosAdicionales: SetInfo(  'GR_AD_ACC',
                                             'CM_CODIGO, GR_CODIGO, GX_CODIGO, GX_DERECHO',
                                             'CM_CODIGO, GR_CODIGO, GX_CODIGO' );
              eSistLstDispositivos: SetInfo( 'DISPOSIT',
                                             'DI_NOMBRE,DI_TIPO,DI_DESCRIP,DI_NOTA,DI_IP',
                                             'DI_NOMBRE,DI_TIPO');
              eDisxCom: SetInfo( 'DISXCOM',
                                 'CM_CODIGO,DI_NOMBRE,DI_TIPO',
                                 'CM_CODIGO,DI_NOMBRE,DI_TIPO' );
              //eAccArbol: SetInfo('ACC_ARBOL', 'AA_SOURCE,AX_NUMERO,AA_DESCRI,AA_INGLES,AA_VERSIO,AA_POSICI,AA_MODULO,AA_SCREEN', 'AA_SOURCE,AX_NUMERO' );
              eUsuarioCCosto: SetInfo( 'SUP_COSTEO',
                                       'CB_NIVEL, US_CODIGO',
                                       'CB_NIVEL, US_CODIGO' );
              {$ifndef DOS_CAPAS}
              eSistListaGrupos:
              begin
                   SetInfo( 'GRUPOTERM',
                                         'GP_CODIGO, GP_DESCRIP',
                                        'GP_CODIGO' ); // SYNERGY
                   AfterUpdateRecord := SetAfterUpdateGruposDisp;
              end;
              eTermPorGrupo:
              begin
                   SetInfo( 'TERM_GRUPO',
                                      'TE_NOMBRE, GP_CODIGO',
                                      'TE_NOMBRE, GP_CODIGO' ); // SYNERGY
              end;
              eMensajes: // SYNERGY
              begin
                   SetInfo( 'MENSAJE',
                            'DM_CODIGO, DM_MENSAJE, DM_ORDEN, DM_SLEEP, DM_ACTIVO',
                            'DM_CODIGO' );
                   SetOrder( 'DM_CODIGO' );
              end;
              eMensajesTerm: // SYNERGY
              begin
                   SetInfo( 'TERM_MSG',
                            'DM_CODIGO, TE_CODIGO, TM_SLEEP, TM_NEXT',
                            'DM_CODIGO, TE_CODIGO' );
                   SetOrder( 'TE_CODIGO, DM_CODIGO' );
                   AfterUpdateRecord := SetAfterUpdateMensajesPorTerminal;
              end;
              eTerminales: // SYNERGY
              begin
                   SetInfo( 'TERMINAL',
                            'TE_CODIGO, TE_NOMBRE, TE_NUMERO, TE_TEXTO, TE_MAC, TE_APP, TE_BEAT, TE_ZONA, TE_ACTIVO, TE_XML',
                            'TE_CODIGO' );
                   SetOrder( 'TE_CODIGO' );
                   AfterUpdateRecord := SetAfterUpdateTerminales;
              end;
              {$endif}
              {CuentaTim(
	CT_CODIGO Codigo,
	CT_DESCRIP Descripcion,
	CT_ID FolioGrande,
	CT_PASSWRD Formula,
	CT_ACTIVO Booleano,
	US_CODIGO Usuario,
	US_FEC_MOD Fecha,}
              eCuentasTimbrado:
              begin
                   SetInfo( 'CUENTATIM',
                            'CT_CODIGO,CT_DESCRIP,CT_ID,CT_PASSWRD,CT_ACTIVO,US_CODIGO,US_FEC_MOD',
                            'CT_CODIGO' );
                   SetOrder( 'CT_CODIGO' );
              end;

          end;
     end;
end;

function TdmServerSistemaTimbrado.DecodificaClaves( Datos: OleVariant; const sCampo: String ): OleVariant;
begin
     with cdsLista do
     begin
          Active := False;
          Data := Datos;
          while not Eof do
          begin
               try
                  Edit;
                  with FieldByName( sCampo ) do
                  begin
                       AsString := ZetaServerTools.Decrypt( AsString );
                  end;
                  Post;
               except
                     on Error: Exception do
                     begin
                          Cancel;
                     end;
               end;
               Next;
          end;
          MergeChangeLog;
          Result := Data;
          Active := False;
     end;
end;

procedure TdmServerSistemaTimbrado.Codifica( const sCampo: String; DeltaDS: TzProviderClientDataSet );
begin
     with DeltaDS do
     begin
          Edit;
          FieldByName( sCampo ).AsString := ZetaServerTools.Encrypt( ZetaServerTools.CampoAsVar( FieldByName( sCampo ) ) );
          Post;
     end;
end;

function TdmServerSistemaTimbrado.LeerClaves( const iNumero: Integer ): String;
var
   FDataset: TZetaCursor;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          FDataset := CreateQuery( Format( GetScript( Q_CLAVES_LEER ), [ iNumero ] ) );
          try
             with FDataset do
             begin
                  Active := True;
                  if EOF then
                     Result := ''
                  else
                      Result := FieldByName( 'CL_TEXTO' ).AsString;
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataset );
          end;
     end;
end;

procedure TdmServerSistemaTimbrado.SetFiltroGrupos( const iGrupo: integer );
var
   sFiltro: string;
begin
     if ( iGrupo <> ZetaCommonClasses.D_GRUPO_SIN_RESTRICCION ) then
     begin
          {$ifdef INTERBASE}
          sFiltro := Format( '( GR_CODIGO in ( select GR_HIJO from GET_DESCENDENCIA( %d ) ) )', [ iGrupo ] );
          {$endif}
          {$ifdef MSSQL}
          sFiltro := Format( '( GR_CODIGO in ( select GR_HIJO from dbo.GET_DESCENDENCIA( %d ) ) )', [ iGrupo ] );
          {$endif}
          oZetaProvider.TablaInfo.Filtro := sFiltro;
     end;
end;
{
function TdmServerSistemaTimbrado.ChecaUsuarios( const sNombreCorto: string; const iCodigo: integer; var sMensaje: string ): boolean;
const
     K_MENSAJE = '� Nombre De Usuario Ya Existe !';
var
   sNombre: string;
   FUsers: TZetaCursor;
   iUsuario: integer;

begin
     Result := True;
     with oZetaProvider do
     begin
          FUsers := CreateQuery( GetScript( Q_CHECAUSUARIOS ) );
          try
             with FUsers do
             begin
                  Active := True;
                  while not EOF do
                  begin
                       iUsuario := FieldByName( 'US_CODIGO' ).AsInteger;
                       sNombre := UpperCase( FieldByName( 'US_CORTO' ).AsString );
                       if iCodigo <> iUsuario then
                       begin
                            if sNombreCorto = sNombre then
                            begin
                                 Result := False;
                                 sMensaje := K_MENSAJE;
                                 Break;
                            end;
                       end;
                       Next;
                  end;
                  Active := False;
             end;
          finally
                 FreeAndNil( FUsers );
          end;
     end;
end;
}

function TdmServerSistemaTimbrado.BuildEmpresa(DataSet: TZetaCursor): OleVariant;
     { Es una copia de DBaseCliente.BuildEmpresa }
     {
     Las posiciones de este arreglo se encuentran en ZetaCommonClasses:

     P_ALIAS = 0;
     P_DATABASE = 0;
     P_USER_NAME = 1;
     P_PASSWORD = 2;
     P_USUARIO = 3;
     P_NIVEL_0 = 4;
     P_CODIGO = 5;
     }
begin
     with DataSet do
     begin
          Result := VarArrayOf( [ FieldByName( 'CM_DATOS' ).AsString,
                                  FieldByName( 'CM_USRNAME' ).AsString,
                                  FieldByName( 'CM_PASSWRD' ).AsString,
                                  0,
                                  FieldByName( 'CM_NIVEL0' ).AsString,
                                  FieldByName( 'CM_CODIGO' ).AsString ] );
     end;
end;

{ ************ Interfases ( Paletitas ) ****************** }

{$IFDEF ANTES}
// AP,CV: 30/Mar/2007 Este c�digo no es utilizado en otro lado
function TdmServerSistemaTimbrado.GetBitacora( Empresa: OleVariant; FechaInicial, FechaFinal: TDateTime; TipoBitacora, Empleado, Usuario: Integer): OleVariant;
var
   sScript, sEmpleado, sBitacora, sUsuario: String;
begin
     if ( TipoBitacora < 0 ) then
        sBitacora := ''
     else
         sBitacora := ' and ( BI_TIPO = :TipoBitacora ) ';
     if ( Empleado = 0 ) then
        sEmpleado := ''
     else
         sEmpleado := ' and ( CB_CODIGO = :EmpleadoNumero ) ';
     if ( Usuario = 0 ) then
        sUsuario := ''
     else
         sUsuario := ' AND ( US_CODIGO = :Usuario ) ';
     sScript := StrTransform( StrTransform( StrTransform(
                      Format( GetScript( Q_SIST_BITACORA ), [DateToStrSQL( FechaInicial ), DateToStrSQL( FechaFinal )]),
                      '@BITACORA', sBitacora ),
                      '@EMPLEADO', sEmpleado ),
                      '@USUARIO', sUsuario );
     with oZetaProvider do
     begin
          Result:= OpenSQL( Comparte, sScript, True );
     end;
     SetComplete;
end;

{$ENDIF}
function TdmServerSistemaTimbrado.GetEmpresas( Tipo, GrupoActivo: Integer ): OleVariant;
begin
     with oZetaProvider do
     begin
          Result:= DecodificaClaves( OpenSQL( Comparte, Format( GetScript( Q_GET_EMPRESAS ), [ GetTipoCompany( Tipo ), GrupoActivo, ZetaCommonClasses.D_GRUPO_SIN_RESTRICCION ] ), True ), K_CLAVE_EMPRESA )
     end;
     SetComplete;
end;

function TdmServerSistemaTimbrado.GetEmpresasAccesos(Tipo, Grupo: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          Result:= OpenSQL( Comparte, Format( GetScript( Q_GET_EMPRESAS_ACCESOS ), [ Grupo, GetTipoCompanyDerechos( Tipo ) ] ), True );
     end;
     SetComplete;
end;

function TdmServerSistemaTimbrado.GetGrupos(Grupo: Integer): OleVariant;
begin
     SetTablaInfo( eGruposUsuarios );
     SetFiltroGrupos( Grupo );
     with oZetaProvider do
     begin
          Result := GetTabla( Comparte );
     end;
     SetComplete;
end;

function TdmServerSistemaTimbrado.GrabaEmpresas(oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     //FCampo := K_CLAVE_EMPRESA;
     SetTablaInfo( eEmpresas );
     with oZetaProvider do
     begin

          with TablaInfo do
          begin
               BeforeUpdateRecord := CodificaClaveEmpresa;
               AfterUpdateRecord := ActualizaDerechos;
          end;
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
          ActualizaCodigoEmpresa(FCM_CODIGOAnt,FCM_CODIGONuevo );

     end;
     SetComplete;
end;

procedure TdmServerSistemaTimbrado.ActualizaCodigoEmpresa(const CodigoAnterior,NuevoCodigo:string);
var
   FDataset: TZetaCursor;
   oZetaProviderCompany: TdmZetaServerProvider;
   FEmpresasLog:string;

    procedure CambiarCodigoGrupoEmpresa(const sTabla:string);
    begin
        try
           with oZetaProviderCompany do
           begin
                 EmpiezaTransaccion;
                 if FModoGrupos = ukModify then
                    FDataSet := CreateQuery( Format( GetScript( Q_ACTUALIZA_EMPRESA_DER ),[sTabla,NuevoCodigo,CodigoAnterior ]))
                 else if FModoGrupos = ukDelete then
                      FDataSet := CreateQuery( Format( GetScript( Q_BORRA_EMPRESA_DER ),[sTabla,CodigoAnterior ]));
                 try
                    Ejecuta( FDataSet );
                 finally
                        TerminaTransaccion(True);
                        FreeAndNil( FDataSet );
                 end;
           end;
        except
              FEmpresasLog := ConcatString(FEmpresasLog,FDataSetEmpresa.FieldByName('CM_CODIGO').AsString,',');
        end;
    end;

   {Actualizar empresa}
   procedure ActualizarEmpresa;
   var
      bConexion:Boolean;
   begin
        oZetaProviderCompany := TdmZetaServerProvider.Create( nil );
        try
           with oZetaProviderCompany do
           begin
                 //Conexion ala empresa
                 try
                    EmpresaActiva := BuildEmpresa(FDataSetEmpresa);
                    bConexion := True;
                 except
                       bConexion := False;
                 end;
                 if( bConexion )then
                 begin
                      {$ifdef MSSQL}
                      //Checar las tablas creadas
                      CambiarCodigoGrupoEmpresa('R_ENT_ACC');
                      CambiarCodigoGrupoEmpresa('R_CLAS_ACC');
                      {$endif}
                      CambiarCodigoGrupoEmpresa('GR_AD_ACC');
                 end;
           end;
        Finally
               FreeAndNil( oZetaProviderCompany );
        end;
   end;

   procedure SetEmpresa;
   begin
        with oZetaProviderComparte do
        begin
             FDataSetEmpresa := CreateQuery( Format( GetScript( Q_COMPANY ), [ FCM_CODIGONuevo ] ) );
             FDataSetEmpresa.Active := True;
        end;
   end;
begin
      try
         FEmpresasLog := VACIO;
         with FDataSetEmpresa do
         begin
              SetEmpresa;
              ActualizarEmpresa;
              Active := False;
         end;
      finally
             FreeAndNil( FDataSetEmpresa );
      end;
end;

procedure TdmServerSistemaTimbrado.CodificaClaveEmpresa(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
     with oZetaProviderComparte do
     begin
          EmpresaActiva := Comparte;
          FDataSetEmpresa := CreateQuery( Format( GetScript( Q_COMPANY ), [ CampoAsVar(DeltaDS.FieldByName( 'CM_CODIGO' )) ] ) );
          FDataSetEmpresa.Active := True;
     end;
     if ( UpdateKind in [ ukModify, ukInsert ] ) then
        Codifica( K_CLAVE_EMPRESA, DeltaDS );
     Applied := False;

end;

procedure TdmServerSistemaTimbrado.ActualizaDerechos(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind );
begin
     FModoGrupos := UpdateKind;

     with DeltaDS.FieldByName( 'CM_CODIGO' ) do
     begin
          FCM_CODIGOAnt := OldValue;
          FCM_CODIGONuevo := NewValue;
     end;


end;

function TdmServerSistemaTimbrado.GrabaGrupos(oDelta: OleVariant; iUsuario: Integer; out ErrorCount: Integer): OleVariant;
begin
     FUsuario:= iUsuario;
     FTipoSistema:= eGruposUsuarios;
     SetTablaInfo( eGruposUsuarios );
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          TablaInfo.AfterUpdateRecord := AfterUpdateGrupos;
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
          ActualizarEmpresasGrupo(iUsuario);
     end;
     SetComplete;
end;

procedure TdmServerSistemaTimbrado.ActualizarEmpresasGrupo(const iUsuario:Integer);
var
   FDataset: TZetaCursor;
   FDataSetComparte:TZetaCursor;
   oZetaProviderCompany: TdmZetaServerProvider;
   FEmpresasLog:string;

   {Actualizar el codigo de empresa en cada tabla de accesos }
   procedure CambiarCodigoGrupoEmpresa(const CodigoAnterior,NuevoCodigo:Integer;const sTabla:string);
   begin
        try
           with oZetaProviderCompany do
           begin
                 EmpiezaTransaccion;
                 if FModoGrupos = ukModify then
                    FDataSet := CreateQuery( Format( GetScript( Q_ACTUALIZA_CODIGO_EMPRESA ),[sTabla,NuevoCodigo,CodigoAnterior ]))
                 else if FModoGrupos = ukDelete then
                      FDataSet := CreateQuery( Format( GetScript( Q_BORRA_CODIGO_EMPRESA ),[sTabla,CodigoAnterior ]));
                 try
                    Ejecuta( FDataSet );
                 finally
                        TerminaTransaccion(True);
                        FreeAndNil( FDataSet );
                 end;
           end;
        except
              FEmpresasLog := ConcatString(FEmpresasLog,FDataSetComparte.FieldByName('CM_CODIGO').AsString,',');
        end;
   end;

   {Actualizar empresa}
   procedure ActualizarEmpresa(const CodigoAnterior,NuevoCodigo:Integer);
   var
      bConexion:Boolean;
   begin
        oZetaProviderCompany := TdmZetaServerProvider.Create( nil );
        try
           with oZetaProviderCompany do
           begin
                 //Conexion ala empresa
                 try
                    EmpresaActiva := BuildEmpresa(FDataSetComparte);
                    bConexion := True;
                 except
                       bConexion := False;
                 end;
                 if( bConexion )then
                 begin
                      {$ifdef MSSQL}
                      //Checar las tablas creadas
                      CambiarCodigoGrupoEmpresa(CodigoAnterior,NuevoCodigo,'R_ENT_ACC');
                      CambiarCodigoGrupoEmpresa(CodigoAnterior,NuevoCodigo,'R_CLAS_ACC');
                      {$endif}
                      CambiarCodigoGrupoEmpresa(CodigoAnterior,NuevoCodigo,'GR_AD_ACC');
                 end;
           end;
        Finally
               FreeAndNil( oZetaProviderCompany );
        end;
   end;

   {Obtener la lista de empresas y Actualizar el codigo para cada Tabla}
   procedure ActualizarCodigoGrupo(const NuevoCodigo,CodigoAnterior:Integer );
   begin
        with oZetaProviderComparte do
        begin
             EmpresaActiva := Comparte;
             FDataSetComparte := CreateQuery( Format( GetScript( Q_COMPANYS_LIST_DISTINCT ), [ GetTipoCompany( Ord( tc3Datos ) )] ) );
              try
                 FEmpresasLog := VACIO;
                 with FDataSetComparte do
                 begin
                      Active := True;
                      First;
                      while not Eof do
                      begin
                           ActualizarEmpresa( CodigoAnterior,NuevoCodigo );
                           Next;
                      end;
                      Active := False;
                 end;
              finally
                     FreeAndNil( FDataSetComparte );
              end;
        end;
   end;
   
begin
     if ( FModoGrupos = ukModify ) then
     begin
          ActualizarCodigoGrupo( FCodigoNuevo ,FCodigoAnterior );
     end;
     if ( FModoGrupos = ukDelete ) then
     begin
          ActualizarCodigoGrupo( 0,FCodigoAnterior );
     end;
     //Grabar a bitacora aquellas empresas en las que no se actualizaron los grupos � ocurri� una excepci�n
     if StrLleno(FEmpresasLog)then
     begin
          oZetaProviderComparte.EscribeBitacoraComparte(tbAdvertencia,clbDerechosAcceso,Now,'Excepci�n al Modificar Grupo de Acceso ','Ocurri� una excepci�n al grabar Accesos en las siguientes empresas: '+FEmpresasLog ,iUsuario);
     end;
end;


procedure TdmServerSistemaTimbrado.AfterUpdateGrupos(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind );
var
   FGrupos: TZetaCursor;

begin
     FModoGrupos := UpdateKind;
     if ( UpdateKind in [ ukInsert, ukModify ] ) then
     begin
          if CambiaCampo( DeltaDS.FieldByName( 'GR_PADRE' ) ) then
          begin
               with oZetaProvider do
               begin
                    FGrupos := CreateQuery( Format( GetScript( Q_CHECAGRUPOS ), [ DeltaDS.FieldByName( 'GR_PADRE' ).AsInteger ] ) );
                    try
                       Ejecuta( FGrupos );
                    finally
                           FreeAndNil( FGrupos );
                    end;
               end;
          end;
          {Actualizar el codigo de grupos en las dem�s bases de datos si se cambia el codigo del grupo }
          if ( UpdateKind in [ ukModify ] ) then
          begin
               if CambiaCampo( DeltaDS.FieldByName( 'GR_CODIGO' ) ) then
               begin
                    FCodigoAnterior := DeltaDS.FieldByName( 'GR_CODIGO' ).OldValue;
                    FCodigoNuevo := DeltaDS.FieldByName( 'GR_CODIGO' ).NewValue;
               end;
          end;
     end;
     if ( UpdateKind in [ ukDelete ] ) then
        FCodigoAnterior:= DeltaDS.FieldByName( 'GR_CODIGO' ).AsInteger;
        
     GrabaCambiosBitacora( Sender, SourceDS, DeltaDS, UpdateKind );
end;


function TdmServerSistemaTimbrado.GetImpresoras: OleVariant;
begin
     SetTablaInfo( eImpresoras );
     with oZetaProvider do
     begin
          Result := GetTabla( Comparte );
     end;
     SetComplete;
end;

function TdmServerSistemaTimbrado.GetAccesos(iGrupo: Integer; const sCompany: WideString): OleVariant;
const
     K_FILTRO = '( GR_CODIGO = %d ) and ( CM_CODIGO = ''%s'' )';
begin
     SetTablaInfo( eAccesos );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( K_FILTRO, [ iGrupo, sCompany ] );
          Result := GetTabla( Comparte );
     end;
     SetComplete;
end;

function TdmServerSistemaTimbrado.GetUsuarios(out Longitud: Integer;Grupo: Integer; out BlockSistema: WordBool): OleVariant;
begin
     SetTablaInfo( eUsuarios );
     SetFiltroGrupos( Grupo );
     with oZetaProvider do
     begin
	{$ifdef SISNOM_CLAVES}
          Result := GetTabla( Comparte );      // Se manda encriptado al cliente.
	{$else}
          Result := DecodificaClaves( GetTabla( Comparte ), K_CLAVE_USUARIO );
	{$endif}
     end;
     Longitud := StrToIntDef( LeerClaves( CLAVE_LIMITE_PASSWORD ), 0 );
     BlockSistema := zstrToBool( LeerClaves( CLAVE_SISTEMA_BLOQUEADO ) );
     SetComplete;
end;

function TdmServerSistemaTimbrado.GetUsuarioSupervisores(Empresa: OleVariant; iUsuario: Integer): OleVariant;
const
     K_FILTRO = 'US_CODIGO = %d';
begin
     SetTablaInfo( eUserSuper );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( K_FILTRO, [ iUsuario ] );
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerSistemaTimbrado.GrabaAccesos(oDelta, oParams: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( eAccesos );
     with oZetaProvider do
     begin
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
          if ErrorCount = 0 then
          begin
               EmpresaActiva:= Comparte;
               AsignaParamList( oParams );
               with ParamList do
               begin
                    EscribeBitacoraComparte( tbNormal, clbDerechosAcceso, NullDateTime,ParamByName('Descripcion').AsString, ParamByName('TextoBitacora').AsString, ParamByName('Usuario').AsInteger  );
               end;
          end;
     end;
     SetComplete;
end;

function TdmServerSistemaTimbrado.GrabaImpresoras(oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( eImpresoras );
     with oZetaProvider do
     begin
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
     end;
     SetComplete;
end;

function TdmServerSistemaTimbrado.GrabaUsuarios(oDelta: OleVariant; iUsuario: Integer; out ErrorCount: Integer): OleVariant;
begin
{$ifdef SISNOM_CLAVES}
     UsuarioData.EncriptaPass := FALSE;   // Ya viene encriptado del cliente - no se requiere encriptar
     try
{$endif}
     Result := UsuarioData.GrabaUsuarios(oDelta, iUsuario, ErrorCount);
{$ifdef SISNOM_CLAVES}
     finally
            UsuarioData.EncriptaPass := TRUE;   // Lo regresa a TRUE para que el resto de los procesos si lo encripten - Impacto principal en Profesional
     end;
{$endif}
     SetComplete;
end;

procedure TdmServerSistemaTimbrado.GrabaCambiosBitacora(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;UpdateKind: TUpdateKind);
var
   eClase: eClaseSistBitacora;
   sTitulo: String;

 procedure DatosBitacora( eClaseCatComparte: eClaseSistBitacora; sDescripcion, sKeyField : string );
  var
     oField : TField;
 begin
      eClase  := eClaseCatComparte;
      sTitulo := sDescripcion;

      if StrLleno(sKeyField) then
      begin
           oField := DeltaDS.FieldByName( sKeyField );

           if oField is TNumericField then
              sTitulo := sTitulo + ' ' + IntToStr( ZetaServerTools.CampoOldAsVar( oField ) )
           else
               sTitulo := sTitulo + ' ' + ZetaServerTools.CampoOldAsVar( oField );
      end;
 end;

begin
     if UpdateKind in [ ukModify, ukDelete ] then
     begin
          eClase := clbNingunoSist;

          case  FTipoSistema of
                    eGruposUsuarios              : DatosBitacora( clbCatalogoGrupoUsuarios, 'Grupo :', 'GR_CODIGO' );


                    else raise Exception.Create( 'No ha sido Integrado el Cat�logo al Case de <DServerSistema.GrabaCambiosBitacora>' )
          end;

          with oZetaProvider do
            if UpdateKind = ukModify then
                CambioCatalogoComparte( sTitulo, eClase, DeltaDS, FUsuario )
            else
                BorraCatalogoComparte( sTitulo,  eClase, DeltaDS, FUsuario );
     end;
end;


function TdmServerSistemaTimbrado.GrabaUsuariosSupervisores(Empresa,oDelta: OleVariant; out ErrorCount: Integer; iUsuario: Integer; const sTexto: WideString): OleVariant;
{$ifdef TRESS}
var
   sDescripcion: String;
const
     K_DELETE = 'delete from SUPER where ( US_CODIGO = %d )';
{$else}
{$endif}
begin
     {$ifdef TRESS}
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          EmpiezaTransaccion;
          try
             ExecSQL( Empresa, Format( K_DELETE, [ iUsuario ] ) );
             TerminaTransaccion( TRUE );
          except
                on Error: Exception do
                begin
                     TerminaTransaccion( FALSE );
                     raise;
                end;
          end;
          if VarIsNull( oDelta ) then
             Result := oDelta
          else
          begin
               SetTablaInfo( eUserSuper );
               Result := GrabaTabla( Empresa, oDelta, ErrorCount );
          end;
          sDescripcion:= 'Se cambiaron Supervisores de Usuario: '+ IntToStr( iUsuario );
          if ( ErrorCount = 0 ) then
          begin
               EscribeBitacora( tbNormal, clbSupervisores, 0, NullDateTime, sDescripcion, sTexto );
          end;
     end;
     {$else}
     Result := NULL;
     {$endif}

     SetComplete;
end;

function TdmServerSistemaTimbrado.GetUsuarioArbol(iUsuario: Integer): OleVariant;
const
     K_TRESS_EXE = 0; // 0 = Tress
begin
     Result := GetUsuarioArbolApp( iUsuario, K_TRESS_EXE );
     SetComplete;
end;

function TdmServerSistemaTimbrado.GrabaArbol(iUsuario: Integer; oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
const
     K_TRESS_EXE = 0; // 0 = Tress
begin
     Result := GrabaArbolApp( iUsuario, K_TRESS_EXE, oDelta, ErrorCount );
     SetComplete;
end;
     { Complemente el 'EmptyDataSet' }
   {  with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          EmpiezaTransaccion;
          try
             ExecSQL( Comparte, Format( K_DELETE, [ iUsuario, iExe ] ) );
             TerminaTransaccion( TRUE );
          except
                on Error: Exception do
                begin
                     TerminaTransaccion( FALSE );
                     raise;
                end;
          end;
          SetTablaInfo( eArbol );
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
     end;
     SetComplete;
end;}

function TdmServerSistemaTimbrado.GetPoll: OleVariant;
begin
     SetTablaInfo( ePoll );
     with oZetaProvider do
     begin
          Result := GetTabla( Comparte );
     end;
     SetComplete;
end;

function TdmServerSistemaTimbrado.GrabaPoll(oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( ePoll );
     with oZetaProvider do
     begin
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
     end;
     SetComplete;
end;

function TdmServerSistemaTimbrado.GetPollVacio: OleVariant;
const
     K_FILTRO = 'PO_NUMERO = 0';
begin
     SetTablaInfo( ePoll );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := K_FILTRO; { Forzar un query vacio para obtener Metadata }
          Result := GetTabla( Comparte );
     end;
     SetComplete;
end;

function TdmServerSistemaTimbrado.ActivaDesActivaUsuarios( const Usuario, Grupo: integer; const lActiva : Boolean ): OleVariant;
var
   FDataset: TZetaCursor;
   iLongitud: Integer;
   lBloqueoSistema: WordBool;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          EmpiezaTransaccion;
          if ( Grupo = ZetaCommonClasses.D_GRUPO_SIN_RESTRICCION ) then
             FDataSet := CreateQuery( Format( GetScript( Q_ACTIVA_APAGA_USUARIOS_SIN_RESTRICCION ), [ zBoolToStr( lActiva ), Usuario ] ) )
          else
              FDataSet := CreateQuery( Format( GetScript( Q_ACTIVA_APAGA_USUARIOS ), [ zBoolToStr( lActiva ), Usuario, Grupo ] ) );
          try
             try
                Ejecuta( FDataSet );
                TerminaTransaccion( TRUE );
             except
                   on Error: Exception do
                   begin
                        RollBackTransaccion;
                        raise;
                   end;
             end;
          finally
                 FreeAndNil( FDataset );
          end;
     end;
     Result := GetUsuarios( iLongitud, Grupo, lBloqueoSistema );
end;

function TdmServerSistemaTimbrado.ActivaUsuarios(Usuario, Grupo: Integer): OleVariant;
begin
     Result := ActivaDesActivaUsuarios( Usuario, Grupo, False );
     SetComplete;
end;

function TdmServerSistemaTimbrado.SuspendeUsuarios(Usuario, Grupo: Integer): OleVariant;
begin
     Result := ActivaDesActivaUsuarios( Usuario, Grupo, True );
     SetComplete;
end;

procedure TdmServerSistemaTimbrado.BorrarBitacoraParametros;
const
     K_PROCESOS = 'Procesos';
     K_INDIVIDUALES = 'Individuales';
var
   sTipo: String;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          if ParamByName( 'Procesos' ).AsBoolean then
             sTipo := K_PROCESOS
          else
             sTipo := K_INDIVIDUALES;
          FListaParametros := 'Borrar Registros de Bit�cora: ' + sTipo;
          FListaParametros := FListaParametros + K_PIPE + 'Anteriores a: ' + FechaCorta( ParamByName( 'Fecha' ).AsDateTime );
          FListaFormulas   := VACIO;
          FListaFormulas   := FListaFormulas + K_PIPE + Format( 'Lista de %s: ', [ sTipo ] ) + ParamByName( 'Lista' ).AsString;
     end;
end;

function TdmServerSistemaTimbrado.BorrarBitacora(Empresa, Parametros: OleVariant): OleVariant;
var
   lProcesos, lIndividuales, lEmpresa, lSistema, lKiosco, lCafeteria, lBorraBitacoraOk : Boolean;{OP: 06/06/08}
   dFecha : TDate;
   sFecha, sBitacora, sLista, sMessageError: String;{OP: 06/06/08}

   function GetFiltroNumero: String;
   begin
        if ( lIndividuales or lSistema )then
        begin
             Result := Format( 'and BI_CLASE in ( %s )', [ sLista ] ); //and BI_NUMERO = 0
             sBitacora := 'Individual ';
        end;
   end;

begin
     {$ifndef ADUANAS}
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          lBorraBitacoraOk := False;{OP: 06/06/08}
          sMessageError := VACIO;{OP: 06/06/08}
          AsignaParamList( Parametros );
          with ParamList do
          begin
               lProcesos := ParamByName( 'Procesos' ).AsBoolean;
               lIndividuales := ParamByName( 'Individuales' ).AsBoolean;
               lEmpresa := ParamByName( 'Empresa' ).AsBoolean;{OP: 06/06/08}
               lSistema := ParamByName( 'Sistema' ).AsBoolean;{OP: 06/06/08}
               lKiosco := ParamByName( 'Kiosco' ).AsBoolean;{OP: 06/06/08}
               lCafeteria := ParamByName( 'Cafeteria' ).AsBoolean;{OP: 06/06/08}
               dFecha := ParamByName( 'Fecha' ).AsDateTime;
               sFecha := DateToStrSQL( dFecha );
               sLista := ParamByName( 'Lista' ).AsString;
          end;
          BorrarBitacoraParametros;
          if OpenProcess( prSISTDepurar, 1, FListaParametros, FListaFormulas ) then
          begin
               Result := DZetaServerProvider.GetEmptyProcessResult( prSISTDepurar );
               {OP: 06/06/08}
               if lEmpresa then
               begin
                    EmpiezaTransaccion;
                    try
                       if lProcesos then
                       begin
                            sBitacora := 'de Procesos ';
                            ExecSQL( Empresa, Format( GetScript( Q_BORRA_BITACORA_PROCESOS ), [ sFecha, sLista ] ) );
                            ExecSQL( Empresa, Format( GetScript( Q_BORRA_PROCESOS ), [ sFecha, sLista ] ) );
                       end
                       else if ( lEmpresa and lIndividuales ) then
                       begin
                            ExecSQL( Empresa, Format( GetScript( Q_BORRA_BITACORA ), [ sFecha, GetFiltroNumero ] ) )
                       end;
                       TerminaTransaccion( True );
                       lBorraBitacoraOk := True;
                    except
                          on Error: Exception do
                          begin
                               TerminaTransaccion( False );
                               Log.Excepcion( 0, 'Error Al Depurar Bit�cora %s', Error );
                          end;
                    end;
               end
               else
               begin
                    EmpresaActiva := Comparte;
                    try
                       EmpiezaTransaccion;
                       try
                          if lSistema then
                          begin
                               sBitacora := 'de Sistemas ';
                               ExecSQL( Comparte, Format( GetScript( Q_BORRA_BITACORA ), [ sFecha, GetFiltroNumero ] ) );
                          end
                          else if( lKiosco ) then
                          begin
                               sBitacora := 'de Kiosko ';
                               ExecSQL( Comparte, Format( GetScript( Q_BORRA_BITACORA_KIOSCO ), [ sFecha ] ) );
                          end
                          else if( lCafeteria ) then
                          begin
                               sBitacora := 'de Cafeteria ';
                               ExecSQL( Comparte, Format( GetScript( Q_BORRA_BITACORA_CAFETERIA ), [ sFecha ] ) );
                          end;
                          TerminaTransaccion( True );
                          lBorraBitacoraOk := True;
                       except
                             on Error: Exception do
                             begin
                                  TerminaTransaccion( False );
                                  sMessageError := Format( 'Error Al Depurar Bit�cora %s', [ Error.Message ] );
                             end;
                       end;
                    finally
                           EmpresaActiva := Empresa;
                    end;
               end;

               { Bit�cora }
               if lBorraBitacoraOk then
                  Log.Evento( clbNinguno, 0, Now, Format( 'Depur� Bit�cora %s Anterior al %s', [ sBitacora, FechaCorta( dFecha ) ] ) )
               else
                   Log.Error( 0, 'Error Al Depurar Bit�cora', sMessageError );
          end;
          Result := CloseProcess;
     end;
     {$endif}
     SetComplete;
end;

function TdmServerSistemaTimbrado.GetNivel0: OleVariant;
begin
     {$ifdef ANTES}
     SetTablaInfo( eNivel0 );
     with oZetaProvider do
     begin
          Result := GetTabla( Comparte );
     end;
     {$endif}
     with oZetaProvider do
     begin
          Result := OpenSQL( Comparte, Format( GetScript( Q_NIVEL0 ), [ EntreComillas(' ') ] ), True );
     end;

     SetComplete;
end;

function TdmServerSistemaTimbrado.GrabaNivel0(oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( eNivel0 );
     with oZetaProvider do
     begin
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
     end;
     SetComplete;
end;

function TdmServerSistemaTimbrado.GetSuscripciones(const sCampo: string; const iCodigo:integer ): Olevariant;
var
   sScript: String;
begin
     with oZetaProvider do
     begin
          sScript := Format( GetScript( Q_GET_USUARIO_SUSCRIP ), [ sCampo, iCodigo ] );
          Result:= OpenSQL( EmpresaActiva, sScript, True );
     end;
end;

function TdmServerSistemaTimbrado.GetUsuarioSuscrip(Empresa: OleVariant; Usuario: Integer): OleVariant;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     Result:= GetSuscripciones( 'US_CODIGO', Usuario );
     SetComplete;
end;

function TdmServerSistemaTimbrado.GetReporteSuscrip(Empresa: OleVariant; iReporte: Integer): OleVariant;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     Result:= GetSuscripciones( 'RE_CODIGO', iReporte );
     SetComplete;
end;

function TdmServerSistemaTimbrado.GrabaUsuarioSuscrip(Empresa, oDelta: OleVariant; out ErrorCount: Integer; Usuario: Integer): OleVariant;
const
     K_DELETE = 'delete from SUSCRIP where ( US_CODIGO = %d )';
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;             
          SetTablaInfo( eSuscrip );
          TablaInfo.AfterUpdateRecord:= AfterUpdateSuscripciones;
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
     end;
     SetComplete;

end;

procedure TdmServerSistemaTimbrado.AfterUpdateSuscripciones(Sender: TObject;SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;UpdateKind: TUpdateKind);
{$ifdef TRESS}
var
   sTitulo: String;
   iUsuario: Integer;
   iReporte: Integer;
{$else}
{$endif}

begin
     {$ifdef TRESS}
     with oZetaProvider do
     begin
           with DeltaDs do
           begin
                if ( UpdateKind in [ ukModify, ukDelete ]) then
                begin
                     iUsuario := CampoOldAsVar( FieldByName('US_CODIGO') );
                     iReporte := CampoOldAsVar( FieldByName('RE_CODIGO') );
                end
                else
                begin
                     iUsuario := CampoAsVar( FieldByName('US_CODIGO') );
                     iReporte := CampoAsVar( FieldByName('RE_CODIGO') );
                end;
           end;
           sTitulo:= Format('Suscrip. Reporte %d Usuario %d', [iReporte, iUsuario] );

           case UpdateKind of
                ukModify:  CambioCatalogo( sTitulo, clbSuscripcionReportes, DeltaDS );
                ukDelete:  BorraCatalogo( sTitulo, clbSuscripcionReportes, DeltaDS );
                ukInsert:  EscribeBitacora( tbNormal, clbSuscripcionReportes, 0, NullDateTime, 'Agreg� '+ sTitulo, '' );
           end;
     end;
     {$else}
     {$endif}

end;

function TdmServerSistemaTimbrado.GetGruposAccesos(iGrupo: Integer; const sCompany: WideString; iTipo: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          Result:= OpenSQL( Comparte, Format( GetScript( Q_GET_GRUPOS_ACCESOS ), [ iGrupo, sCompany, ZetaServerTools.GetTipoCompany( iTipo ) ] ), True );
     end;
     SetComplete;
end;

function TdmServerSistemaTimbrado.GetUsuarioAreas(Empresa: OleVariant; Usuario: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          Result:= OpenSQL( Empresa, Format( GetScript( Q_GET_USUARIO_AREAS ), [ Usuario ] ), True );
     end;
     SetComplete;
end;

function TdmServerSistemaTimbrado.GrabaUsuarioAreas(Empresa: OleVariant; Usuario: Integer; oDelta: OleVariant; out ErrorCount: Integer; const sTexto: WideString): OleVariant;
{$ifdef TRESS}
var
   sDescripcion: String;
{$endif}
begin
     {$ifdef TRESS}
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          EmpiezaTransaccion;
          try
             ExecSQL( Empresa, Format( GetScript( Q_DEL_USUARIO_AREAS ), [ Usuario ] ) );
             TerminaTransaccion( TRUE );
          except
                on Error: Exception do
                begin
                     TerminaTransaccion( FALSE );
                     raise;
                end;
          end;
          if VarIsNull( oDelta ) then
             Result := oDelta
          else
          begin
               SetTablaInfo( eUsuarioAreas );
               Result := GrabaTabla( Empresa, oDelta, ErrorCount );
          end;
          InitGlobales;
          sDescripcion:= 'Se cambiaron '+ GetGlobalString( K_GLOBAL_LABOR_AREAS )+ ' de usuario: '+ IntToStr( Usuario );
          if ( ErrorCount = 0 ) then
          begin
               EscribeBitacora( tbNormal, clbAreasLabor, 0, NullDateTime, sDescripcion, sTexto );
          end;
     end;
     {$ELSE}
     Result := NULL;
     {$ENDIF}
     SetComplete;
end;

function TdmServerSistemaTimbrado.GetNuevoUsuario: Integer;
var
   FCodigo: TZetaCursor;
   oEmpresaTemp: OleVariant;
begin
     with oZetaProvider do
     begin
          oEmpresaTemp:= EmpresaActiva;
          try
             EmpresaActiva := Comparte;
             FCodigo := CreateQuery( GetScript( Q_NUEVO_USUARIO ) );
             try
                with FCodigo do
                begin
                     Active := True;
                     Result := FieldByName( 'US_CODIGO' ).AsInteger + 1;
                     Active := False;
                end;
             finally
                    FreeAndNil( FCodigo );
             end;
          finally
                 if NOT VarIsNull(oEmpresaTemp) then
                    EmpresaActiva := oEmpresaTemp;

          end;
     end;
     SetComplete;
end;

function TdmServerSistemaTimbrado.ActualizaNumeroTarjeta(out Bitacora: OleVariant): OleVariant;
const
     K_ANCHO_EMPRESA = 10;
     K_ANCHO_PROBLEMA = 80;
var
   iEmpleado, iInvitador, i, iLow, iHigh, iEmpresas: Integer;
   oLista, oEmpresa: OleVariant;
   sTarjeta, sEmpresa: string;
   FDataSet, FEmpleados, FInvitadores, FEmpID, FRepetido: TZetaCursor;
   oProvider: TdmZetaServerProvider;
   FLog: TStrings;

   procedure InitLog;
   begin
        {
        with cdsLista do
        begin
             InitTempDataSet;
             AddStringField( 'PROBLEMA', K_ANCHO_PROBLEMA );
             CreateTempDataset;
             Active := True;
        end;
        }
        FLog := TStringList.Create;
   end;

   procedure CloseLog;
   begin
        {
        with cdsLista do
        begin
             Active := False;
        end;
        }
        FreeAndNil( FLog );
   end;

   procedure WriteLog( const iEmpleado: Integer; const sMensaje, sError: String );
   begin
        {
        with cdsLista do
        begin
             Insert;
             try
                FieldByName( 'PROBLEMA' ).AsString := Format( '%s: %s', [ sMensaje, sError ] );
                Post;
             except
                   Cancel;
             end;
        end;
        }
        FLog.Add( Format( '%s: %s', [ sMensaje, sError ] ) );
        {$ifndef ADUANAS}
        if Assigned( oProvider ) then
        begin
             oProvider.EscribeBitacora( tbError,
                                        clbNumTarjeta,
                                        iEmpleado,
                                        Date,
                                        sMensaje,
                                        sError );
        end;
        {$endif}
   end;

   procedure WriteException( const iEmpleado: Integer; const sMensaje: String; Error: Exception );
   begin
        WriteLog( iEmpleado, sMensaje, Error.Message );
   end;

   procedure ObtieneListaEmpresas;
   begin
        with oZetaProviderComparte do
        begin
             EmpresaActiva := Comparte;
             FDataSet := CreateQuery( Format( GetScript( Q_COMPANYS_COUNT ), [ GetTipoCompany( Ord( tc3Datos ) ) ] ) );
             try
                with FDataSet do
                begin
                     Active := True;
                     iEmpresas := Fields[ 0 ].AsInteger;
                     Active := False;
                end;
             finally
                    FreeAndNil( FDataset );
             end;
             if ( iEmpresas = 0 ) then
                ZetaCommonTools.SetOLEVariantToNull( oLista )
             else
             begin
                  oLista := VarArrayCreate( [ 1, iEmpresas, 1, 2 ], varVariant );
                  VarArrayLock( oLista );
                  try
                     iEmpresas := 1;
                     FDataset := CreateQuery( Format( GetScript( Q_COMPANYS_LIST ), [ GetTipoCompany( Ord( tc3Datos ) ) ] ) );
                     try
                        with FDataSet do
                        begin
                             Active := True;
                             while not Eof do
                             begin
                                  oLista[ iEmpresas, 1 ] := FieldByName( 'CM_NOMBRE' ).AsString;
                                  oLista[ iEmpresas, 2 ] := BuildEmpresa( FDataset );
                                  Inc( iEmpresas );
                                  Next;
                             end;
                             Active := False;
                        end;
                     finally
                            FreeAndNil( FDataSet );
                     end;
                  finally
                         VarArrayUnlock( oLista );
                  end;
             end;
        end;
   end;

   procedure BorrarRegistrosTabla( const sEmpresa: String );
   begin
        { Se Borran los Registro de la Tabla }
        with oZetaProviderComparte do
        begin
             EmpiezaTransaccion;
             try
                ParamAsString( FEmpId, 'CM_CODIGO', sEmpresa );
                Ejecuta( FEmpId );
                TerminaTransaccion( TRUE );
             except
                   on Error: Exception do
                   begin
                        TerminaTransaccion( FALSE );
                        WriteException( iEmpleado, 'Error al Borrar Informaci�n de Comparte', Error );
                   end;
             end;
        end;
   end;

   procedure EscribeRepetidos( const iCodigo: integer; const lEmpleado: boolean = TRUE );
   const
        aCampo: array[ FALSE..TRUE ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'IV_CODIGO', 'CB_CODIGO' );
        aMensaje: array[ FALSE..TRUE ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'Invitador %d e Invitador %d de Empresa %s', 'Empleado  %d y Empleado  %d de Empresa %s' );
   var
      sCampo: string;
   begin
        sCampo := aCampo[ lEmpleado ];
        FRepetido := oZetaProviderComparte.CreateQuery( Format( GetScript( Q_CODIGO_REPETIDO ), [ EntreComillas( sTarjeta ) ] ) );
        try
           with FRepetido do
           begin
                Active := True;
                try
                   WriteLog( iCodigo, Format( 'Tarjeta %s Repetida', [ sTarjeta ] ), Format( aMensaje[ lEmpleado ], [ iCodigo, FieldByName( sCampo ).AsInteger, FieldByName( 'CM_CODIGO' ).AsString ] ) );
                finally
                       Active := False;
                end;
           end;
        finally
               FreeAndNil( FRepetido );
        end;
   end;

   procedure ActualizaEmpleados;
   begin
        { Se buscan los Empleados de la Empresa }
        FDataSet := oProvider.CreateQuery( GetScript( Q_EMPLEADOS_COMPANY ) );
        try
           with FDataSet do
           begin
                try
                   try
                      Active := True;
                      while not EOF do
                      begin
                           iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                           sTarjeta  := FieldByName( 'CB_ID_NUM' ).Asstring;
                           sEmpresa  := oProvider.CodigoEmpresaActiva;
                           with oZetaProviderComparte do
                           begin
                                EmpiezaTransaccion;
                                try
                                   ParamAsString( FEmpleados, 'ID_NUMERO', sTarjeta );
                                   ParamAsString( FEmpleados, 'CM_CODIGO', sEmpresa );
                                   ParamAsInteger( FEmpleados, 'CB_CODIGO', iEmpleado );
                                   Ejecuta( FEmpleados );
                                   TerminaTransaccion( TRUE );
                                except
                                      on Error: Exception do
                                      begin
                                           TerminaTransaccion( FALSE );
                                           if PK_VIOLATION( Error )then
                                              EscribeRepetidos( iEmpleado )
                                           else
                                               WriteException( iEmpleado, 'Error Al Actualizar Empleados En Tabla EMP_ID', Error );
                                      end;
                                end;
                           end;
                           Next;
                      end;
                   except
                         on Error: Exception do
                         begin
                              WriteException( iEmpleado, 'Error Al Actualizar # Tarjeta de Empleados', Error );
                              SetProcessError( Result );
                         end;
                   end;
                finally
                       Active := False;
                end;
           end;
        finally
               FreeAndNil( FDataset );
        end;
   end;

   procedure ActualizaInvitadores;
   begin
        { Se buscan los invitadores de la Empresa }
        FDataSet := oProvider.CreateQuery( GetScript( Q_INVITADORES_COMPANY ) );
        try
           with FDataSet do
           begin
                try
                   try
                      Active := True;
                      while not EOF do
                      begin
                           iInvitador := FieldByName( 'IV_CODIGO' ).AsInteger;
                           sTarjeta  := FieldByName( 'IV_ID_NUM' ).Asstring;
                           sEmpresa  := oProvider.CodigoEmpresaActiva;
                           with oZetaProviderComparte do
                           begin
                                EmpiezaTransaccion;
                                try
                                   ParamAsString( FInvitadores, 'ID_NUMERO', sTarjeta );
                                   ParamAsString( FInvitadores, 'CM_CODIGO', sEmpresa );
                                   ParamAsInteger( FInvitadores, 'IV_CODIGO', iInvitador );
                                   Ejecuta( FInvitadores );
                                   TerminaTransaccion( TRUE );
                                except
                                      on Error: Exception do
                                      begin
                                           TerminaTransaccion( FALSE );
                                           if PK_VIOLATION( Error )then
                                              EscribeRepetidos( iInvitador, FALSE )
                                           else
                                               WriteException( iInvitador, 'Error al Actualizar Invitadores en Tabla EMP_ID', Error );
                                      end;
                                end;
                           end;
                           Next;
                      end;
                   except
                         on Error: Exception do
                         begin
                              WriteException( iInvitador, 'Error Al Actualizar # Tarjeta Invitadores', Error );
                              SetProcessError( Result );
                         end;
                   end;
                finally
                       Active := False;
                end;
           end;
        finally
               FreeAndNil( FDataset );
        end;
   end;

   procedure InitQuerys;
   begin
        with oZetaProviderComparte do
        begin
             FEmpId := CreateQuery( GetScript( Q_BORRA_EMPLEADOS_COMPARTE ) );
             FEmpleados := CreateQuery( GetScript( Q_INSERTA_EMP_ID ) );
             FInvitadores := CreateQuery( GetScript( Q_INSERTA_INV_EMP_ID ) );
        end;
   end;

   procedure FreeQuerys;
   begin
        FreeAndNil( FInvitadores );
        FreeAndNil( FEmpleados );
        FreeAndNil( FEmpId );
   end;

begin
     {$ifndef ADUANAS}
     InitLog;
     with FLog do
     begin
          Add( Format( '**** Inicio de Actualizaci�n de Tarjetas: %s ****', [ FormatDateTime( 'dd/mmm/yyyy hh:nn:ss AM/PM', Now ) ] ) );
          Add( '' );
          Bitacora := Text;
     end;
     Result := GetEmptyProcessResult( prSISTNumeroTarjeta );
     try
          { Se llena el OleVariant oLista }
          try
               ObtieneListaEmpresas;
          except
               on Error: Exception do
               begin
                    WriteException( 0, 'Error Al Leer Lista De Empresas', Error );
                    ZetaCommonTools.SetOLEVariantToNull( oLista )
               end;
          end;

          { Se Verifica que se tengan empresas con Digito }
          if not VarIsNull( oLista ) then
          begin
               iLow := VarArrayLowBound( oLista, 1 );
               iHigh := VarArrayHighBound( oLista, 1 );
               InitQuerys;
               try
                    for i := iLow to iHigh do
                    begin
                         iEmpleado := 0;
                         iInvitador := 0;
                         oEmpresa := oLista[ i, 2 ];
                         oProvider := TdmZetaServerProvider.Create( Self );
                         try
                              try
                                   oProvider.EmpresaActiva := oEmpresa;
                                   with FLog do
                                   begin
                                        Add( Format( '--- Actualizaci�n Tarjetas De La Empresa: %s ---', [ oProvider.CodigoEmpresaActiva ] ) );
                                        Add('');
                                        Bitacora := Text;
                                   end;
                                   BorrarRegistrosTabla( oProvider.CodigoEmpresaActiva ); { Borrar los Registro de la Tabla EMP_ID por Empresa }
                                   ActualizaEmpleados;                                    { M�todo para Actualizar los Empleados de Cada Empresa }
                                   ActualizaInvitadores;                                  { M�todo para Actualizar los Invitadores de Cada Empresa }

                              finally
                                     FreeAndNil( oProvider );
                              end;
                         except                                                           //Si falla el EmpresaActiva escribe error en log pero contin�a
                              on Error: Exception do                                      //Escribe error en log
                                   WriteException( 0, '--- Error Al Actualizar Tarjetas --- ', Error );
                         end;
                    end;
                    SetProcessOK( Result );                                               //Termina thread en OK
               finally
                      FreeQuerys;                                                              //Termina thread en OK
               end;
          end;
     except
           on Error: Exception do
           begin
                SetProcessError( Result );
                FreeAndNil( oProvider );
                WriteException( 0, 'Error Al Actualizar Tarjetas', Error );
           end;
     end;
     with FLog do
     begin
          Add( '' );
          Add( Format( '**** Fin de Actualizaci�n de Tarjetas:    %s ****', [ FormatDateTime( 'dd/mmm/yyyy hh:nn:ss AM/PM', Now ) ] ) );
          Bitacora := Text;
     end;
     CloseLog;
     {$endif}
     SetComplete;
end;

function TdmServerSistemaTimbrado.GetGruposAdic(Empresa: OleVariant; out oCamposAdic: OleVariant): OleVariant;
var sFiltroDerecho :string;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          if ( oZetaProvider.CodigoGrupo = D_GRUPO_SIN_RESTRICCION ) then
          begin
               SetTablaInfo( eGruposAdic );
               TablaInfo.SetOrder( 'GX_POSICIO' );
               Result := GetTabla( Empresa );
          end
          else
          begin
               sFiltroDerecho := 'and ( ( GX_DERECHO % 2 ) = 1 )';
               {$ifdef DOS_CAPAS}
               sFiltroDerecho := 'and ( GX_DERECHO in ( 1, 9, 17, 25 ) )';
               {$endif}
               Result := OpenSQL(Empresa, Format(GetScript(Q_GRUPO_ACCESOS),[ EntreComillas( CodigoEmpresaActiva ) ,CodigoGrupo,sFiltroDerecho  ] ),True );
          end;
          SetTablaInfo( eCamposAdic );
          TablaInfo.SetOrder( 'GX_CODIGO,CX_POSICIO' );
          oCamposAdic := GetTabla( Empresa );
     end;
end;


function TdmServerSistemaTimbrado.GrabaGruposAdic(Empresa, oDelta, oDeltaCampos: OleVariant;
         out ErrorCount, ErrorCampos: Integer; out CamposResult: OleVariant): OleVariant;

   function BorraTabla( const sTabla: String ): Boolean;
   begin
        with oZetaProvider do
        begin
             EmpiezaTransaccion;
             try
                ExecSQL( Empresa, Format( GetScript( Q_BORRA_TABLA ), [ sTabla ] ) );
                TerminaTransaccion( TRUE );
                Result := TRUE;
             except
                   on Error: Exception do
                   begin
                        TerminaTransaccion( FALSE );
                        raise;
                   end;
             end;
        end;
   end;

begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;

          if BorraTabla( 'GRUPO_AD' ) then
          begin
               if VarIsNull( oDelta ) then
                  Result := oDelta
               else
               begin
                    SetTablaInfo( eGruposAdic );
                    Result := GrabaTabla( Empresa, oDelta, ErrorCount );
               end;

               if ( ErrorCount = 0 ) and BorraTabla( 'CAMPO_AD' ) then
               begin
                    //Borra los grupos adicionales que ya no existen
                    ExecSQL( Empresa, GetScript( Q_BORRA_GRUPO_AD ) );

                    if VarIsNull( oDeltaCampos ) then
                       Result := oDeltaCampos
                    else
                    begin
                         SetTablaInfo( eCamposAdic );
                         CamposResult := GrabaTabla( Empresa, oDeltaCampos, ErrorCampos );
                    end;

                    if ( ErrorCampos = 0 ) then
                       EjecutaAndFree( GetScript( Q_ACTUALIZA_DICCION ) );
               end;
          end;
     end;
end;

function TdmServerSistemaTimbrado.GetBitacora(Empresa, Parametros: OleVariant): OleVariant;
var
   sSQL, CampoFecha: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          AsignaParamList( Parametros );
          with ParamList do
          begin
               if ParamByName('FechaCaptura').AsBoolean then
                  CampoFecha := 'BI_FECHA'
               else
                  CampoFecha := 'BI_FEC_MOV';

               sSQL := Format( GetScript( Q_BITACORA ),
                             [ CampoFecha,
                               DateToStrSQLC( ParamByName( 'FechaInicial' ).AsDate ),
                               DateToStrSQLC( ParamByName( 'FechaFinal' ).AsDate + 1 ),
                               ParamByName( 'Tipo' ).AsInteger,
                               ParamByName( 'Clase' ).AsInteger,
                               ParamByName( 'Usuario' ).AsInteger,
                               ParamByName( 'NumProceso' ).AsInteger ] );
          end;
          Result := OpenSQL( EmpresaActiva, sSQL, True );
     end;
     SetComplete;
end;

function TdmServerSistemaTimbrado.GetClasifiEmpresa( Empresa: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Result := OpenSQL( EmpresaActiva, GetScript( Q_CLASIFI_EMPRESA ), True );
     end;
     SetComplete;
end;

function TdmServerSistemaTimbrado.GetRoles: OleVariant;
begin
     SetTablaInfo( eRoles );
     with oZetaProvider do
     begin
          Result := GetTabla( Comparte );
     end;
     SetComplete;
end;

function TdmServerSistemaTimbrado.GrabaRoles(const Rol: WideString; Delta,
  Usuarios, Modelos: OleVariant; var ErrorCount: Integer): OleVariant;
var
   FAgregar: TZetaCursor;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          SetTablaInfo( eRoles );
          if Delta <> NULL then
             Result := GrabaTabla( Comparte, Delta, ErrorCount )
          else
              Result := NULL;
              
          if ( ErrorCount <= 0 ) then
          begin
               { Grabar Usuarios del Rol }
               FAgregar := CreateQuery( GetScript( Q_USER_ROL_INSERT ) );
               try
                  EmpiezaTransaccion;
                  try
                     ExecSQL( Comparte, Format( GetScript( Q_ROL_USUARIOS_DELETE ), [ Rol ] ) );
                     with cdsLista do
                     begin
                          Lista := Usuarios;
                          First;
                          while not Eof do
                          begin
                               ParamAsString( FAgregar, 'RO_CODIGO', Rol );
                               ParamAsInteger( FAgregar, 'US_CODIGO', FieldByName( 'US_CODIGO' ).AsInteger );
                               Ejecuta( FAgregar );
                               Next;
                          end;
                     end;
                     TerminaTransaccion( TRUE );
                  except
                        on Error: Exception do
                        begin
                             TerminaTransaccion( FALSE );
                             raise;
                        end;
                  end;
               finally
                      FreeAndNil( FAgregar );
               end;
               { Grabar Modelos del Rol }
               {Revisar GrabaRoles del DServerComparte}
          end;
     end;
     SetComplete;
end;



function TdmServerSistemaTimbrado.GetDatosEmpleadoUsuario(Empresa, Datos: OleVariant): OleVariant;
{$ifndef SELECCION}
 {$ifndef VISITANTES}
var
    oParametros: TZetaParams;
    oEvaluador: TZetaEvaluador;
    oZetaCreator : TZetaCreator;
    sError: string;
    {$ifdef DOS_CAPAS}
    oDataSet: TZetaCursor;
    {$endif}

  function CalculaFormula( const iGlobal: integer; const sTitulo: string ): string;
   var
      EVResultado : TQREvResult;
      ErrorNumero: integer;
      ErrorDescrip, ErrorMsg, ErrorExp: string;
  begin
       Result := VACIO;
       EVResultado := oEvaluador.Calculate( oZetaProvider.GetGlobalString(iGlobal) );
       if HuboErrorEvaluador( EVResultado, ErrorNumero, ErrorDescrip, ErrorMsg, ErrorExp ) then
       begin
            sError := 'Error al calcular la f�rmula de ' + sTitulo + ':' + CR_LF +
                      'Error N�mero: ' + IntToStr( ErrorNumero ) + CR_LF +
                      ErrorDescrip + CR_LF + ErrorMsg + CR_LF + ErrorExp + CR_LF + CR_LF +
                      'Revisar las f�rmulas en la forma de Globales\Acceso a Tress';
       end
       else
       begin
            with EvResultado do
            begin
                 case Kind of
                      resInt: Result := IntToStr( intResult );
                      resDouble : Result := FloatToStr( dblResult );
                      resBool: Result := zBoolToStr(booResult);
                      resString: Result := strResult;
                 end;
            end;
       end;
  end;
   {$endif}
 {$endif}

begin
     {$ifndef SELECCION}
        {$ifndef VISITANTES}
     sError := VACIO;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
     end;
     cdsLista.Data := Datos;
     {$ifdef DOS_CAPAS}
     oDataSet:= oZetaProvider.CreateQuery(Format( 'select * from COLABORA where CB_CODIGO = %d', [cdsLista.FieldByName('CB_CODIGO').AsInteger] ));
     oDataSet.Open;
     {$endif}

     oZetaCreator := TZetaCreator.Create( oZetaProvider );
     with oZetaCreator do
     begin
          //PreparaEntidades;
          RegistraFunciones( [ efComunes ] );
     end;

     oEvaluador := TZetaEvaluador.Create( oZetaCreator, self );

     with oEvaluador do
     begin
          {$ifdef DOS_CAPAS}
          AgregaDataSet( TZetaCursor( oDataSet ) );
          {$else}
          AgregaDataSet( TZetaCursor( cdsLista ) );
          {$endif}
          Entidad := enEmpleado;
     end;


     //Resultado
     oParametros:= TZetaParams.Create;

     oParametros.AddString('US_CORTO', CalculaFormula(ZGlobalTress.K_GLOBAL_ENROLL_FORMULA_USER, 'Usuario' ));
     oParametros.AddString('US_PASSWRD',{$ifdef SISNOM_CLAVES}ZetaServerTools.Encrypt({$endif}CalculaFormula(ZGlobalTress.K_GLOBAL_ENROLL_FORMULA_PWD, 'Contrase�a' ){$ifdef SISNOM_CLAVES}){$endif});
     oParametros.AddString('US_DOMAIN', CalculaFormula(ZGlobalTress.K_GLOBAL_ENROLL_FORMULA_AD, 'UsuarioDominio' ));
     oParametros.AddString('US_EMAIL',CalculaFormula(ZGlobalTress.K_GLOBAL_ENROLL_FORMULA_MAIL, 'Correo Electr�nico' ));
     oParametros.AddString('Error',sError);

     Result := oParametros.VarValues;

     FreeAndNil( oParametros );
     {$ifdef DOS_CAPAS}
     FreeAndNil( oDataset );
     {$endif}
      {$endif}
     {$endif}

end;

function TdmServerSistemaTimbrado.GetUsuariosRol(const Rol: WideString;  out Modelos: OleVariant): OleVariant;
begin
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Comparte;
             Result := OpenSQL( Comparte, Format( GetScript( Q_ROL_USUARIOS ), [ Rol ] ), True );
             //Modelos := OpenSQL( Comparte, Format( GetScript( Q_ROL_MODELOS ), [ Rol ] ), True );
        end;
        SetComplete;
     except
           SetAbort;
           raise;
     end;
end;

function TdmServerSistemaTimbrado.GetUserRoles(Empresa: OleVariant;iUsuario: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result:= OpenSQL( EmpresaActiva, Format( GetScript( Q_GET_USER_ROLES ), [ iUsuario ] ), True );
     end;
     SetComplete;
end;

function TdmServerSistemaTimbrado.GrabaUsuarioRoles(Empresa: OleVariant;iUsuario: Integer; Delta: OleVariant;var ErrorCount: Integer): OleVariant;
{$ifdef TRESS}
  var
   sDescripcion: String;
{$endif}
begin
     {$ifdef TRESS}
     with oZetaProvider do
     begin
          if FUsuario = 0 then
          begin
               EmpresaActiva := Empresa;
               FUsuario := UsuarioActivo;
          end;
          EmpresaActiva := Comparte;
          EmpiezaTransaccion;
          try
             ExecSQL( EmpresaActiva, Format( GetScript( Q_DEL_USUARIO_ROLES ), [ iUsuario ] ) );
             TerminaTransaccion( TRUE );
          except
                on Error: Exception do
                begin
                     TerminaTransaccion( FALSE );
                     raise;
                end;
          end;
          if VarIsNull( Delta ) then
             Result := Delta
          else
          begin
               SetTablaInfo( eUserRol );
               Result := GrabaTabla( EmpresaActiva, Delta, ErrorCount );
          end;
          InitGlobales;
          sDescripcion:= 'Se cambiaron '+ 'los Roles' + ' al usuario: '+ IntToStr( iUsuario );
          if ( ErrorCount = 0 ) then
          begin
               EscribeBitacoraComparte ( tbNormal,clbAsignacionRoles ,NullDateTime, sDescripcion,VACIO,FUsuario);
          end;
     end;
     {$ELSE}
     Result := NULL;
     {$ENDIF}
     SetComplete;
end;

function TdmServerSistemaTimbrado.GrabaEmpleadoUsuario(oEmpresa: OleVariant; iUsuario, iEmpleado: Integer): OleVariant;
begin
     oZetaProvider.EmpresaActiva := oEmpresa;
     UsuarioData.GrabaEmpleadoTress(iEmpleado,iUsuario, oEmpresa[P_CODIGO]);
     SetComplete;
end;

function TdmServerSistemaTimbrado.GetUsuarioData: TUsuarioData;
begin
     if FUsuarioData = NIL then
        FUsuarioData := TUsuarioData.Create(oZetaProvider);
     Result := FUsuarioData;
end;

procedure TdmServerSistemaTimbrado.ClearBroker;
begin
     ZetaSQLBroker.FreeSQLBroker( oZetaSQLBroker );
end;


procedure TdmServerSistemaTimbrado.InitCreator;
begin
     if not Assigned( oZetaCreator ) then
        oZetaCreator := ZetaSQLBroker.GetZetaCreator( oZetaProvider );
end;

procedure TdmServerSistemaTimbrado.InitBroker;
begin
     InitCreator;
     oZetaCreator.RegistraFunciones([efComunes]);
     oZetaSQLBroker := ZetaSQLBroker.GetSQLBroker( oZetaCreator );
end;


function TdmServerSistemaTimbrado.EnrolamientoMasivo(Empresa,Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
          AsignaParamList( Parametros );
     end;
     EnrolamientoMasivoParametros;
     InitBroker;
     try
        EnrolamientoMasivoBuildDataSet;
        Result := EnrolamientoMasivoDataSet( SQLBroker.SuperReporte.DataSetReporte,prSistEnrolarUsuario );
     finally
        ClearBroker;
     end;
     SetComplete;
end;


procedure TdmServerSistemaTimbrado.EnrolamientoMasivoBuildDataSet ;
begin
     {$ifndef SELECCION}
      {$ifndef VISITANTES}
     with SQLBroker do
     begin
          Init( enEmpleado );
          with Agente do
          begin
               AgregaColumna( 'COLABORA.CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
               AgregaColumna( 'COLABORA.US_CODIGO', True, Entidad, tgNumero, 0, 'US_CODIGO' );
               AgregaColumna( K_PRETTYNAME, True, Entidad, tgTexto,100 , 'US_NOMBRE' );
               AgregaColumna( oZetaProvider.ParamList.ParamByName('Usuario').AsString, False, Entidad, tgTexto, 15, 'US_CORTO' );
               AgregaColumna( oZetaProvider.ParamList.ParamByName('Clave').AsString, False, Entidad, tgTexto, 30, 'US_PASSWRD' );
               AgregaColumna( oZetaProvider.ParamList.ParamByName('CorreoElectr�nico').AsString, False, Entidad, tgTexto, 50, 'US_EMAIL' );
               AgregaColumna( oZetaProvider.ParamList.ParamByName('FormulaAD').AsString, False, Entidad, tgTexto, 50, 'US_DOMAIN' );
               AgregaFiltro( 'COLABORA.CB_ACTIVO = ''S'' ' , FALSE, Entidad );
               AgregaOrden( 'COLABORA.CB_CODIGO', True, Entidad );
          end;
          AgregaRangoCondicion( oZetaProvider.ParamList );
          FiltroConfidencial( oZetaProvider.EmpresaActiva );
          BuildDataset( oZetaProvider.EmpresaActiva );
     end;
      {$endif}
     {$endif}
end;

function TdmServerSistemaTimbrado.EnrolamientoMasivoDataset(Dataset: TDataset;Proceso: Procesos): OleVariant;
var
   iEmpleado: TNumEmp;
   iUsuario :Integer;

function AgregarUsuario(const iGrupo,iReportaA:Integer;const lUsaPortal:Boolean):Boolean;
begin
     with oZetaProvider do
     begin
          Result:= True;
          with Dataset do
          begin
               if( not UsuarioData.AgregarUsuario( FieldByName('US_CORTO').AsString,
                                           FieldByName('US_PASSWRD').AsString,
                                           FieldByName('US_DOMAIN').AsString,
                                           FieldByName('US_EMAIL').AsString,
                                           FieldByName('US_NOMBRE').AsString,
                                           iEmpleado,
                                           iGrupo,
                                           iReportaA,
                                           iUsuario,
                                           lUsaPortal
                                           ) )then
               begin
                    Log.Advertencia(iEmpleado,'No Se Le Di� Acceso a Sistema TRESS',Format('Usuario repetido. Verificar que el usuario: %s � el usuario del dominio: %s no existan',[ UpperCase( FieldByName('US_CORTO').AsString),UpperCase( FieldByName('US_DOMAIN').AsString ) ]) );
                    Result:= False;
               end;
          end;
     end;
end;

procedure RegistrarBitacora(const sRoles:string);
begin
     with oZetaProvider do
     begin
          EmpiezaTransaccion;
          with Dataset do
          begin
               try
                  Log.Evento( clbNinguno, iEmpleado, NullDateTime, 'Se Le Di� Acceso a Sistema TRESS ' );
                  if( StrLleno( sRoles ) )then
                  begin
                       Log.Evento( clbNinguno, iEmpleado, NullDateTime, Format( 'Se Asignar�n los roles: %s al Usuario: %s ',[sRoles,FieldByName('US_CORTO').AsString ] ) );
                  end;
                  TerminaTransaccion( TRUE );
               except
                     on Error: Exception do
                     begin
                          RollBackTransaccion;
                     end;
               end;
          end;
     end;
end;

{$ifndef DOS_CAPAS}
function  VerificarSupervisorEnrolado(const iCB_CODIGO : integer ) : Boolean;
const
    Q_GET_SUPERVISOR_ENROLADO_POR_EMPLEADO = 'SELECT CB_NIVEL_COD, US_JEFE FROM dbo.SP_GET_SUPERVISOR_ENROLADO_POR_EMPLEADO( %d ) ';
var
 FSupEnrolado: TZetaCursor;
 sAreaSup, sNombreNivelSupervisor : string;
 iUS_JEFE, iNivelSupervisor : integer;
begin
     iUS_JEFE := 0;
     sAreaSup := VACIO;
     with oZetaProvider do
     begin
          if not GetGlobalBooleano( K_GLOBAL_ENROLAMIENTO_SUPERVISORES ) then
          begin
               Result := True;
          end
          else
          begin
             FSupEnrolado := CreateQuery( Format(Q_GET_SUPERVISOR_ENROLADO_POR_EMPLEADO, [iCB_CODIGO])  );
             try
                with FSupEnrolado do
                begin
                     Active := True;
                     if ( not IsEmpty )then
                     begin
                          sAreaSup  := FieldByName( 'CB_NIVEL_COD' ).AsString;
                          iUS_JEFE  := FieldByName( 'US_JEFE' ).AsInteger;
                     end;
                     Active := False;
                end;
             finally
                    FreeAndNil( FSupEnrolado );
             end;

             if ( iUS_JEFE <= 0 ) then
             begin
                try
                   EmpiezaTransaccion;
                   sNombreNivelSupervisor := VACIO;
                   iNivelSupervisor := GetGlobalInteger( K_GLOBAL_NIVEL_SUPERVISOR );
                   if ( iNivelSupervisor > 0 ) and ( iNivelSupervisor <= 9) then
                      sNombreNivelSupervisor := GetGlobalString( K_GLOBAL_NIVEL1 + iNivelSupervisor -1 );

                   {$ifdef ACS}
                   if ( iNivelSupervisor >=10 ) and ( iNivelSupervisor <= 12) then
                      sNombreNivelSupervisor := GetGlobalString( K_GLOBAL_NIVEL10 + iNivelSupervisor -1 );
                   {$endif}

                   Log.Advertencia(iEmpleado,'No se le asign� un Reporta A',Format('El empleado no tiene asignado un Reporta A: es necesario enrolar al %s %s ',[ sNombreNivelSupervisor, UpperCase( sAreaSup ) ]) );
                   TerminaTransaccion( TRUE );
                except
                        on Error: Exception do
                        begin
                             RollBackTransaccion;
                        end;
                end;

                Result := False;
             end
             else
                 Result := True;
          end;

     end;


end;
{$endif}

begin
     with oZetaProvider do
     begin
          if OpenProcess(prSistEnrolarUsuario, Dataset.RecordCount, FListaParametros ) or  OpenProcess(prSistImportarEnrolamientoMasivo , Dataset.RecordCount, FListaParametros )then
          begin
               try
                  try
                     with Dataset do
                     begin
                          First;
                          while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                          begin
                               iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                               try
                                  //Wizard de enrolar empleados
                                  iUsuario := GetNuevoUsuario;
                                  if ( Proceso = prSistEnrolarUsuario ) then
                                  begin
                                       if (FieldByName( 'US_CODIGO' ).AsInteger > 0)then
                                          Log.Evento( clbNinguno, iEmpleado, NullDateTime, Format ('Empleado Ya Tiene Acceso a Sistema TRESS por el Usuario: %s',[ IntToStr( FieldByName( 'US_CODIGO' ).AsInteger) ]) )
                                       else
                                       begin
                                            if ( AgregarUsuario( ParamList.ParamByName('GrupoUsuarios').AsInteger,ParamList.ParamByName('ReportaA').AsInteger,ParamList.ParamByName('UsaPortal').AsBoolean ) )then
                                            begin
                                                 {$ifndef DOS_CAPAS}
                                                 DescargarUsuarioRoles(iUsuario,ParamList.ParamByName('RolesAsignados').AsString);
                                                 VerificarSupervisorEnrolado(iEmpleado);
                                                 {$endif}
                                                 RegistrarBitacora( ParamList.ParamByName('RolesAsignados').AsString );

                                            end;
                                       end;
                                  end   ///Wizard de Importacion de usuarios
                                  else if (Proceso = prSistImportarEnrolamientoMasivo) then
                                  begin
                                       if (zStrToBool(FieldByName('REPETIDO').AsString ) )then
                                       begin
                                            if FAccionAlEnrolar = arIgnorar then
                                               Log.Evento( clbNinguno, iEmpleado, NullDateTime, Format('Usuario: %s ya existe',[FieldByName('US_CORTO').AsString ] ) )
                                            else if( FAccionAlEnrolar = arSobreescribir )then
                                            begin
                                                 UsuarioData.EditarAgregarUsuario( Dataset,False,iUsuario );
                                                 {$ifndef DOS_CAPAS}
                                                 DescargarUsuarioRoles( iUsuario,FieldByName('US_ROLES').AsString );
                                                 VerificarSupervisorEnrolado(iEmpleado);
                                                 {$endif}
                                                 RegistrarBitacora( FieldByName('US_ROLES').AsString );
                                            end;
                                       end
                                       else
                                       begin
                                           UsuarioData.EditarAgregarUsuario(Dataset,True,iUsuario);
                                           {$ifndef DOS_CAPAS}
                                           DescargarUsuarioRoles( iUsuario,FieldByName('US_ROLES').AsString );
                                           VerificarSupervisorEnrolado(iEmpleado);
                                           {$endif}
                                           RegistrarBitacora( FieldByName('US_ROLES').AsString );
                                       end;
                                  end;

                               except
                                     on Error: Exception do
                                     begin
                                          EmpiezaTransaccion;
                                          try
                                             Log.Excepcion( iEmpleado, 'Error Al Dar Acceso a Sistema TRESS ', Error );
                                             TerminaTransaccion( TRUE );
                                          except
                                                on Error: Exception do
                                                begin
                                                     RollBackTransaccion;
                                                end;
                                          end;
                                     end;
                               end;
                               Next;
                          end;
                     end;
                  finally
                      //FreeAndNil( FDataSet );
                  end;
               except
                     on Error: Exception do
                     begin
                          Log.Excepcion( 0, 'Error Al Enrolar Masivamente', Error );
                     end;
               end;
          end;
          Result := CloseProcess;
     end;
end;

{$ifndef DOS_CAPAS}
procedure TdmServerSistemaTimbrado.DescargarUsuarioRoles (const iUsuario:Integer;const sRoles:string);
var
   oEmpresaTemp : OleVariant;
   ErrorCount : Integer;
   Lista :TStrings;
   i:Integer;
   UserRoles : TzProviderClientDataSet;
begin
     with oZetaProvider do
     begin
          if( StrLleno( sRoles ) )then
          begin
                oEmpresaTemp := EmpresaActiva;
                Lista := TStringList.Create;
                UserRoles := TzProviderClientDataSet.Create(Self);
                try
                   EmpresaActiva := Comparte;
                   SetTablaInfo(eUserRol);
                   UserRoles.Data := GetTabla( Comparte );
                   Lista.CommaText := sRoles;
                   with Lista do
                   begin
                        for i := 0 to ( Count - 1 ) do
                        begin
                             with UserRoles do
                             begin
                                   if( Lista[i] <> '' )then
                                   begin
                                        Append;
                                        FieldByName( 'US_CODIGO' ).AsInteger := iUsuario;
                                        FieldByName( 'RO_CODIGO' ).AsString := Lista[i];
                                        Post;
                                   end;
                             end;
                        end;
                   end;
                   GrabaUsuarioRoles(EmpresaActiva,iUsuario,UserRoles.Delta, ErrorCount);
                finally
                       EmpresaActiva := oEmpresaTemp;
                       Lista.Free;
                       FreeAndNil(UserRoles);
                end;
          end;
     end;
end;
{$endif}

procedure TdmServerSistemaTimbrado.EnrolamientoMasivoParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'F�rmula Usuario: ' +  ParamByName( 'Usuario' ).AsString + K_PIPE +
                              'F�rmula Clave: ' + ParamByName('Clave').AsString  + K_PIPE +
                              'F�rmula Correo Electr�nico: ' +  ParamByName( 'CorreoElectr�nico' ).AsString + K_PIPE +
                              'Grupo de Usuarios: ' + IntToStr( ParamByName( 'GrupoUsuarios' ).AsInteger) + K_PIPE +
                              'Usa Portal: ' + ZetaCommonTools.zBoolToStr( ParamByName( 'UsaPortal' ).AsBoolean ) + K_PIPE +
                              'Roles Asignados: ' + ParamByName( 'RolesAsignados' ).AsString ;

          {$ifdef TRESS}
          if  oZetaProvider.GetGlobalBooleano( K_GLOBAL_ENROLAMIENTO_SUPERVISORES ) then
          begin
                FListaParametros := FListaParametros + K_PIPE +
                              'Reporta a: seg�n Enrolamiento de Supervisores';
          end
          else
          begin
                    FListaParametros := FListaParametros + K_PIPE +
                              'Reporta a: ' + IntToStr( ParamByName( 'ReportaA' ).AsInteger );
          end;
          {$endif}
     FUsuario := oZetaProvider.UsuarioActivo;
     end;
end;

function TdmServerSistemaTimbrado.EnrolamientoMasivoGetLista(oEmpresa,oParametros: OleVariant): OleVariant;
begin
      with oZetaProvider do
     begin
          EmpresaActiva := oEmpresa;
          AsignaParamList( oParametros );
     end;
     InitBroker;
     try
        EnrolamientoMasivoBuildDataSet;
        Result := SQLBroker.SuperReporte.GetReporte;
     finally
        ClearBroker;
     end;
     SetComplete;
end;


function TdmServerSistemaTimbrado.GetSQLBroker: TSQLBroker;
begin
{$ifdef DOS_CAPAS}
     Result := ZetaSQLBroker.oZetaSQLBroker;
{$else}
     Result := Self.oZetaSQLBroker;
{$endif}
end;


function TdmServerSistemaTimbrado.ImportarEnrolamientoMasivo(Empresa, Parametros,Datos: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
          AsignaParamList( Parametros );
          FAccionAlEnrolar := arIgnorar;
          FAccionAlEnrolar := eAccionRegistros(ParamList.ParamByName('Accion').AsInteger);
          ImportarEnrolamientoMasivoParametros;
          with cdsLista do
          begin
               Lista := Datos;
               Result:= EnrolamientoMasivoDataset( cdsLista ,prSistImportarEnrolamientoMasivo);
          end;
     end;
     SetComplete;
end;


procedure TdmServerSistemaTimbrado.ImportarEnrolamientoMasivoParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := FListaParametros + K_PIPE + 'Archivo: ' + ParamByName( 'Archivo' ).AsString +
                                                 K_PIPE + 'Formato: ' + ObtieneElemento( lfFormatoASCII, ParamByName( 'Formato' ).AsInteger )+
                                                 K_PIPE + 'Acci�n a usuarios existentes: ' + ObtieneElemento( lfAccionRegistros, ParamByName( 'Accion' ).AsInteger);
     end;
end;

function TdmServerSistemaTimbrado.GetTipoLogin:Integer;
var
   oRegistry : TZetaRegistryServer;
begin
     oRegistry := TZetaRegistryServer.Create();
     try
        Result := oRegistry.TipoLogin;
     finally
            oRegistry.Free;
     end;
end;


function TdmServerSistemaTimbrado.ImportarEnrolamientoMasivoGetASCII(oEmpresa,Parametros, ListaASCII: OleVariant; out ErrorCount: Integer;LoginAD: WordBool): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := oEmpresa;
          InitGlobales;
          AsignaParamList( Parametros );
          oSuperASCII := TdmSuperASCII.Create( Self );
          try
             with oSuperASCII do
             begin
                  OnValidar := ImportarEnrolamientoMasivoValidaASCII;
                  FEmpleado:= oZetaProvider.CreateQuery( Format('select CB_CODIGO,CB_ACTIVO,%s from COLABORA where CB_CODIGO = :Empleado' ,[K_PRETTYNAME ]) );
                  Formato := eFormatoASCII( ParamList.ParamByName( 'Formato' ).AsInteger );

                  AgregaColumna( 'US_CORTO', tgTexto, K_ANCHO_CODIGOPARTE, True);
                  AgregaColumna( 'US_NOMBRE', tgTexto, K_ANCHO_OBSERVACIONES);
                  if ( ( eTipoLogin(GetTipoLogin) = tlAD ) and ( eAccionRegistros(ParamList.ParamByName('Accion').AsInteger) = arIgnorar ) )then
                     AgregaColumna( 'US_DOMAIN', tgTexto, K_ANCHO_OBSERVACIONES, True)
                  else
                      AgregaColumna( 'US_DOMAIN', tgTexto, K_ANCHO_OBSERVACIONES);

                  AgregaColumna( 'US_PASSWRD', tgTexto, K_ANCHO_PASSWD, True);
                  AgregaColumna( 'GR_CODIGO', tgNumero, K_ANCHO_PESOS,True);
                  AgregaColumna( 'CM_CODIGO', tgTexto, K_ANCHO_PASSWD);
                  AgregaColumna( 'CB_CODIGO', tgNumero, K_ANCHO_NUMEROEMPLEADO);
                  AgregaColumna( 'US_EMAIL', tgTexto, K_ANCHO_OBSERVACIONES);
                  AgregaColumna( 'US_CAMBIA', tgTexto,K_ANCHO_BOOLEANO,False,0,True,'S');
                  AgregaColumna( 'US_ROLES', tgTexto, K_ANCHO_FORMULA );
                  AgregaColumna( 'US_JEFE', tgNumero, K_ANCHO_PESOS );
                  AgregaColumna( 'US_FORMATO', tgNumero,K_ANCHO_PESOS);
                  AgregaColumna( 'US_ACTIVO', tgTexto,K_ANCHO_BOOLEANO,False,0,True,'S');
                  AgregaColumna( 'US_PORTAL', tgTexto,K_ANCHO_BOOLEANO,False,0,True,'S');
                  AgregaColumna( 'US_BLOQUEA', tgTexto,K_ANCHO_BOOLEANO,False,0,True,'N');
                  AgregaColumna( 'REPETIDO', tgTexto, K_ANCHO_BOOLEANO,False,0,True,'N');

                  Result := Procesa( ListaASCII );
                  ErrorCount := Errores;
             end;
          Finally
                 oSuperASCII.Free;
                 FreeAndNil(FEmpleado);
          end;
     end;
     SetComplete;
end;



procedure TdmServerSistemaTimbrado.ImportarEnrolamientoMasivoValidaASCII(DataSet: TDataset; var nProblemas: Integer; var sErrorMsg: String );
var
   iEmpleado : Integer;
   cdsGrupos,cdsUsuarios,cdsRoles,cdsCompanys : TServerDataSet;

   function ExisteEmpleado( const iEmpleado: Integer ): Boolean;
   begin
        with FEmpleado do
        begin
             Close;
             oZetaProvider.ParamAsInteger( FEmpleado, 'Empleado', iEmpleado );
             Open;
             Result := ( not IsEmpty );
        end;
   end;
   {
   function ValidaExiste(  ) : Boolean;
   begin
        with FExisteRegistro do
        begin
             Active := TRUE;
             Result := ( not IsEmpty);
             Active := FALSE;
        end;
   end;
    }
   procedure CargarData;
   begin
        cdsGrupos := TServerDataSet.Create(Self);
        cdsUsuarios := TServerDataSet.Create(Self);
        cdsRoles := TServerDataSet.Create(Self);
        cdsCompanys := TServerDataSet.Create(Self);


        with oZetaProvider do
        begin
             cdsGrupos.Data := OpenSQL(Comparte,'select GR_CODIGO from GRUPO',True);
             cdsUsuarios.Data := OpenSQL(Comparte,'select US_CORTO,US_CODIGO,US_DOMAIN,CB_CODIGO from USUARIO',True);
             {$ifndef DOS_CAPAS}
             cdsRoles.Data := OpenSQL(Comparte,'select RO_CODIGO from ROL',True);
             {$endif}
             cdsCompanys.Data := OpenSQL(Comparte,'select CM_CODIGO from COMPANY',True);
        end;

   end;

   function ExisteGrupo( const iGrupo: Integer ) : Boolean;
   begin
         Result := cdsGrupos.Locate('GR_CODIGO',iGrupo,[]);
   end;

   function ExisteUsuario( const sCampo,sValor: string ) : Boolean;
   begin
         if ( StrLleno( sValor ) )then
         begin
              Result := cdsUsuarios.Locate(sCampo,sValor,[])
         end
         else
             Result := False;
   end;


   function ExisteCodigoUsuario( const iCodigo: Integer ) : Boolean;
   begin
        Result := cdsUsuarios.Locate('US_CODIGO',iCodigo,[]);
   end;

   function ExisteRol (const sCodigo:string):Boolean;
   begin
        Result := cdsRoles.Locate('RO_CODIGO',sCodigo,[]);
   end;


   procedure ChecarRoles ( sRoles :string );
   var
      Lista:TStrings;
      i:Integer;
   begin
        Lista := TStringList.Create;
        Lista.CommaText := sRoles;
        with Lista do
        begin
             for i := 0 to ( Count - 1 ) do
             begin
                  if not ExisteRol(Lista[i])then
                  begin
                       Inc( nProblemas );
                       sErrorMsg := sErrorMsg + K_PIPE + Format(' El Rol con el c�digo: %s no existe',[Lista[i]]) + K_PIPE;
                  end;
             end;
        end;
   end;


   function ExisteEmpresa(const sCodigo:string) :Boolean;
   begin
        Result := cdsCompanys.Locate('CM_CODIGO',sCodigo,[]);
   end;

   function ExisteEmpleadoEnrolado(Const Empleado:Integer ):Boolean;
   begin
        Result := cdsUsuarios.Locate('CB_CODIGO',Empleado,[]);
   end;



begin
     with DataSet do
     begin
          CargarData;
          //Nombres de usuarios repetidos
          if ( ExisteUsuario('US_CORTO',UpperCase( FieldByName('US_CORTO').AsString ) ) ) then
          begin
               FieldByName('REPETIDO').AsString := K_GLOBAL_SI;
          end;
          //Nombres de usuarios del dominio repetidos
          if ( FAccionAlEnrolar = arSobreescribir )then
          begin
               if ( ExisteUsuario('US_DOMAIN',UpperCase( FieldByName('US_DOMAIN').AsString ) ) ) then
               begin
                    if ( not ( FieldByName('US_CORTO').AsString = cdsUsuarios.FieldByName('US_CORTO').AsString ) )then
                    begin
                         Inc( nProblemas );
                         sErrorMsg := sErrorMsg + K_PIPE + Format(' El nombre del Usuario del dominio: %s ya existe',[FieldByName('US_DOMAIN').AsString]) + K_PIPE;
                    end;
               end;
          end
          else if ( FAccionAlEnrolar = arIgnorar )then
          begin
               if ( ExisteUsuario('US_DOMAIN',FieldByName('US_DOMAIN').AsString ) ) then
               begin
                    Inc( nProblemas );
                    sErrorMsg := sErrorMsg + K_PIPE + Format(' El nombre del Usuario del dominio: %s ya existe',[FieldByName('US_DOMAIN').AsString]) + K_PIPE;
               end;
          end;
          //Que exista el Grupo
          if ( not ExisteGrupo(FieldByName('GR_CODIGO').AsInteger ) ) then
          begin
               Inc( nProblemas );
               sErrorMsg := sErrorMsg + K_PIPE + Format(' Grupo con el c�digo: %d no existe',[ FieldByName('GR_CODIGO').AsInteger ] ) + K_PIPE;
          end;

          //Si NO se tiene el global de Enrolamiento de Supervisores Se revisa US_JEFE
          {$ifdef TRESS}
          if not oZetaProvider.GetGlobalBooleano( K_GLOBAL_ENROLAMIENTO_SUPERVISORES ) then
          begin
             //Que exista el US_JEFE
             if ( not ExisteCodigoUsuario(FieldByName('US_JEFE').AsInteger ) and ( ModuloAutorizado( okWorkflow ) or ModuloAutorizado( okEvaluacion ) ) )then
             begin
                  Inc( nProblemas );
                  sErrorMsg := sErrorMsg + K_PIPE + Format(' El Usuario con el c�digo: %d al cual va reportar no existe',[ FieldByName('US_JEFE').AsInteger ] ) + K_PIPE;
             end;
          end;
          {$endif}
          //si quieren usar portal validar que tengan el modulo

          if ( not ExisteEmpresa(FieldByName('CM_CODIGO').AsString ) )then
          begin
               Inc( nProblemas );
               sErrorMsg := sErrorMsg + K_PIPE + Format('La empresa con el c�digo: %s no existe',[ FieldByName('CM_CODIGO').AsString ] ) + K_PIPE;
          end;

          iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
          if  ( ( not ExisteEmpleado (iEmpleado) ) and ( iEmpleado > 0 ) ) then
          begin
               Inc( nProblemas );
               sErrorMsg := sErrorMsg + K_PIPE + ' Empleado no existe ' + K_PIPE;
          end;

          //Checar si el empleado ya esta asignado a otro Usuario
          if ExisteEmpleadoEnrolado(iEmpleado)then
          begin
               Inc( nProblemas );
               sErrorMsg := sErrorMsg + K_PIPE + ' El empleado ya esta asignado al usuario #'+IntToStr( cdsUsuarios.FieldByName('US_CODIGO').AsInteger ) + K_PIPE;
          end;

          {$ifndef DOS_CAPAS}
          //Checar los roles
          ChecarRoles( FieldByName('US_ROLES').AsString);
          {$endif}


     end;
end;

{$ifdef TRESS}
function TdmServerSistemaTimbrado.ModuloAutorizado(const eModulo: TModulos):Boolean;
begin
     {$ifndef DOS_CAPAS}
     Result := oAutoServer.OkModulo( eModulo, False);
     {$else}
     Result := False;
     {$endif}
end;
{$endif}


function TdmServerSistemaTimbrado.GetDerechosAdicionales(Empresa: OleVariant; Grupo: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Result:= OpenSQL( Empresa, Format( GetScript( Q_ACCESOS_ADICIONALES ),[CodigoEmpresaActiva,Grupo] ),  True );
     end;
     SetComplete;
end;

function TdmServerSistemaTimbrado.GrabaAccesosAdicionales(Empresa: OleVariant; Grupo: Integer; Delta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     if VarIsNull( Delta ) then
     begin
          ErrorCount := 0;
          Result := Null;
     end
     else
         with oZetaProvider do
         begin
              EmpresaActiva := Empresa;
              SetTablaInfo( eAccesosAdicionales );
              Result := GrabaTabla( EmpresaActiva, Delta, ErrorCount );
         end;
end;

//acl
function TdmServerSistemaTimbrado.GetSistLstDispositivos(Empresa: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := OpenSQL( EmpresaActiva, GetScript( Q_SIST_LST_DISPOSITIVOS ), True );
     end;
     SetComplete;
end;

function TdmServerSistemaTimbrado.GrabaSistLstDispositivos(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( eSistLstDispositivos );
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
     end;
     SetComplete;
end;

function TdmServerSistemaTimbrado.GetGlobalLstDispositivos(Empresa: OleVariant;
  var Disponibles, Asignados: OleVariant): OleVariant;
begin
     SetOLEVariantToNull( Disponibles );
     SetOLEVariantToNull( Asignados );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;

          Disponibles:= OpenSQL( Comparte, Format(GetScript( Q_DISP_DISPONIBLES ),[ CodigoEmpresaActiva ] ), TRUE );
          Asignados:= OpenSQL( Comparte, Format(GetScript( Q_DISP_ASIGNADOS ),[ CodigoEmpresaActiva ] ), TRUE );
     end;
     SetComplete;
end;

function TdmServerSistemaTimbrado.GrabaGlobalLstDispositivos(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          ExecSQL( Comparte, Format( GetScript( Q_BORRA_DISP ), [ CodigoEmpresaActiva ] ) );

          if NOT varisNull(oDelta) then
          begin
               SetTablaInfo( eDisxCom );
               Result := GrabaTabla( Comparte, oDelta, ErrorCount );
          end
          else
              Result := NULL;
     end;
     SetComplete;
end;

function TdmServerSistemaTimbrado.GetAccArbol(Empresa: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Result := OpenSQL( Empresa, GetScript(Q_ACCARBOL), TRUE  )
     end;
     SetComplete;
end;

procedure TdmServerSistemaTimbrado.ActualizaUsuariosEmpresa(Empresa,
  Delta: OleVariant);
begin
  {$ifndef DOS_CAPAS}
  UsuarioData.ActualizaUsuariosEmpresa(Empresa, Delta);
  SetComplete;
  {$endif}
end;

function TdmServerSistemaTimbrado.GetUsuarioCCosto(Empresa: OleVariant;  iUsuario: Integer): OleVariant;
const
     K_FILTRO = 'US_CODIGO = %d';
begin
     SetTablaInfo( eUsuarioCCosto );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( K_FILTRO, [ iUsuario ] );
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;


function TdmServerSistemaTimbrado.GrabaUsuarioCCosto(Empresa, oDelta: OleVariant; ErrorCount, iUsuario: Integer; const sTexto, sNombreCosteo: WideString): OleVariant;
{$ifdef TRESS}
var
   sDescripcion: String;
const
     K_DELETE = 'delete from SUP_COSTEO where ( US_CODIGO = %d )';
{$else}
{$endif}
begin
     {$ifdef TRESS}
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          EmpiezaTransaccion;
          try
             ExecSQL( Empresa, Format( K_DELETE, [ iUsuario ] ) );
             TerminaTransaccion( TRUE );
          except
                on Error: Exception do
                begin
                     TerminaTransaccion( FALSE );
                     raise;
                end;
          end;

          if VarIsNull( oDelta ) then
             Result := oDelta
          else
          begin
               SetTablaInfo( eUsuarioCCosto );
               Result := GrabaTabla( Empresa, oDelta, ErrorCount );
          end;
          sDescripcion:= Format( 'Se cambiaron %s de Usuario: ', [sNombreCosteo] )+ IntToStr( iUsuario );
          if ( ErrorCount = 0 ) then
          begin
               EscribeBitacora( tbNormal, clbSupervisores, 0, NullDateTime, sDescripcion, sTexto );
          end;
     end;
     {$else}
     Result := NULL;
     {$endif}

     SetComplete;

end;

{$ifndef DOS_CAPAS}
// SYNERGY
function TdmServerSistemaTimbrado.ConsultaBitacoraBiometrica(Parametros: OleVariant): OleVariant;
var
   dFechaInicio, dFechaFin: TDateTime;
   sFiltro, sEmpresa, sMensaje, sTerminal: String;
begin
     with oZetaProvider do
     begin
          sFiltro := VACIO;
          EmpresaActiva := Comparte;
          AsignaParamList( Parametros );

          with ParamList do
          begin
               dFechaInicio := ParamByName( 'FechaInicio' ).AsDateTime;
               dFechaFin := ParamByName( 'FechaFin' ).AsDateTime;
               sEmpresa := ParamByName( 'Empresa' ).AsString;
               sTerminal := ParamByName( 'Terminal' ).AsString;
               sMensaje := ParamByName( 'Mensaje' ).AsString;
          end;

          if( dFechaInicio <> NullDateTime )then
              sFiltro := Format( '(WS_FECHA >= ''%s'')', [ DateToStrSQL( dFechaInicio ) ] );

          if( dFechaFin <> NullDateTime )then
              sFiltro := ConcatFiltros( sFiltro, Format( '(WS_FECHA < ''%s'')', [ DateToStrSQL( dFechaFin + 1 ) ] ) ); // Por el time stamp

          if StrLleno( sEmpresa )then
             sFiltro := ConcatFiltros( sFiltro, Format( '(CM_CODIGO = ''%s'')', [ sEmpresa ] ) );

          if StrLleno( sTerminal )then
             sFiltro := ConcatFiltros( sFiltro, Format( '(CH_RELOJ = ''%s'')', [ sTerminal ] ) );

          if StrLleno( sMensaje )then
             sFiltro := ConcatFiltros( sFiltro, Format( '( Upper( WS_MENSAJE ) like ''%s%s%s'')', [ '%', ansiUpperCase( sMensaje ), '%' ] ) );

          if StrLleno( sFiltro )then
             sFiltro := 'where ' + sFiltro;

          Result := OpenSQL( EmpresaActiva, Format( GetScript( Q_SIST_BIT_BIO ), [ sFiltro ] ), true );
     end;
     SetComplete;
end;

// SYNERGY
function TdmServerSistemaTimbrado.DepuraBitacoraBiometrica(Parametros: OleVariant): OleVariant;
var
   dInicio, dFin: TDateTime;
   sMensajeError, sEmpresa: string;
const
     K_FILTRO_EMPRESA = 'and CM_CODIGO=''%s''';
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          sMensajeError := VACIO;

          AsignaParamList( Parametros );
          with ParamList do
          begin
               dInicio := ParamByName( 'FechaInicio' ).AsDateTime;
               dFin := ParamByName( 'FechaFin' ).AsDateTime;
               sEmpresa := ParamByName( 'Empresa' ).AsString;
          end;

          Result := DZetaServerProvider.GetEmptyProcessResult( prSISTDepuraBitBiometrico );

          EmpiezaTransaccion;
          try
             if StrLleno( sEmpresa )then
                ExecSQL( EmpresaActiva, Format( GetScript( Q_SIST_DEPURA_BIT_BIO ), [ DateToStrSQL( dInicio ), DateToStrSQL( dFin + 1 ), Format( K_FILTRO_EMPRESA, [ sEmpresa ] ) ] ) )
             else
                 ExecSQL( EmpresaActiva, Format( GetScript( Q_SIST_DEPURA_BIT_BIO ), [ DateToStrSQL( dInicio ), DateToStrSQL( dFin + 1 ), VACIO ] ) );
             TerminaTransaccion( true );
             DZetaServerProvider.SetProcessOK( Result );
          except
                on Error: Exception do
                begin
                     TerminaTransaccion( false );
                     DZetaServerProvider.SetProcessError( Result );
                end;
          end;
     end;
     SetComplete;
end;

// SYNERGY
function TdmServerSistemaTimbrado.ObtenIdBioMaximo: Integer;
var
   oDs: TZetaCursor;
begin
     Result := 0;
     with oZetaProviderComparte do
     begin
          EmpresaActiva := Comparte;
          oDs := CreateQuery( GetScript( Q_SIST_OBTEN_MAX_BIO ) );
          try
             with oDs do
             begin
                  Active := true;
                  if not Eof then
                     Result := FieldByName( 'MAXIMO' ).AsInteger + 1
                  else
                      Result := 1;
                  Active := false;
             end;
          finally
                 FreeAndNil( oDs );
          end;
     end;
end;

// SYNERGY
function TdmServerSistemaTimbrado.ActualizaIdBio(const CodigoEmpresaLocal: WideString; Empleado, Id: Integer; const Grupo: WideString; Invitador: Integer): WordBool;
begin
     Result := True;
     with oZetaProviderComparte do
     begin
          EmpresaActiva := Comparte;
          EmpiezaTransaccion;
          try
             // Borrando registro del empleado

             // Borrando registro del empleado
             if ( Invitador = 0 ) then
             begin
                  ExecSQL( EmpresaActiva, Format( GetScript( Q_SIST_ACTUALIZA_BIO_BORRA ), [ CodigoEmpresaLocal ] ) + ' and CB_CODIGO = ' + IntToStr( Empleado ) + 'and IV_CODIGO = 0' );
                  if( Id > 0 )then
                      ExecSQL( EmpresaActiva, Format( GetScript( Q_SIST_ACTUALIZA_BIO_AGREGA ), [ Id, Empleado, CodigoEmpresaLocal, Grupo, 0 ] ) )
                  else if( Id = 0 )then
                  begin
                       ExecSQL( EmpresaActiva, Format( GetScript( Q_REINICIA_TERMINAL ), [ Format( 'where CB_CODIGO=%d and CM_CODIGO=''%s''',[ Empleado, CodigoEmpresaLocal ] ) ] ) );
                       ExecSQL( EmpresaActiva, Format( GetScript( Q_BORRA_HUELLAGTI ), [ Empleado, CodigoEmpresaLocal ] ) );
                  end;
             end
             else if ( Invitador > 0 ) then
             begin
                  ExecSQL( EmpresaActiva, Format( GetScript( Q_SIST_ACTUALIZA_BIO_BORRA ), [ CodigoEmpresaLocal ] ) + ' and IV_CODIGO = ' + IntToStr( Invitador ) );
                  if( Id > 0 )then
                  begin
                       ExecSQL( EmpresaActiva, Format( GetScript( Q_SIST_ACTUALIZA_BIO_AGREGA ), [ Id, Empleado, CodigoEmpresaLocal, Grupo, Invitador ] ) );
                       ExecSQL( EmpresaActiva, Format( GetScript( Q_SIST_ACTUALIZA_EMPLEADO ), [ Empleado, CodigoEmpresaLocal, Invitador ] ) );
                  end;
             end;

             TerminaTransaccion( true );
          except
                on Error : Exception do
                begin
                     RollBackTransaccion;
                     Result := False;
                end
          end;
     end;
     SetComplete;
end;

// SYNERGY
function TdmServerSistemaTimbrado.ConsultaListaGrupos: OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := OpenSQL( EmpresaActiva, GetScript( Q_CONSULTA_LISTA_GRUPOS ), true );
     end;
     SetComplete;
end;

// SYNERGY
function TdmServerSistemaTimbrado.GrabaSistListaGrupos(oDelta: OleVariant; UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant;
begin
     FUsuario := UsuarioTerminal;
     SetTablaInfo( eSistListaGrupos );
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
     end;
     SetComplete;
end;

// SYNERGY
function TdmServerSistemaTimbrado.ConsultaTermPorGrupo(const Grupo: WideString): OleVariant;
var
   sSQL: string;
const
     K_FILTRO = 'where GP_CODIGO = ''%s''';
begin
     with oZetaProvider do
     begin
          if StrLleno( Grupo )then
             sSQL := Format( GetScript( Q_CONSULTA_TERM_POR_GRUPO ), [ Format( K_FILTRO, [ Grupo ] )] )
          else
              sSQL := Format( GetScript( Q_CONSULTA_TERM_POR_GRUPO ), [ VACIO ] );
              
          EmpresaActiva := Comparte;
          Result := OpenSQL( EmpresaActiva, sSQL, true );
     end;
     SetComplete;
end;

// SYNERGY
function TdmServerSistemaTimbrado.GrabaTermPorGrupo(oDelta: OleVariant; UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant;
begin
     FUsuario := UsuarioTerminal;
     SetTablaInfo( eTermPorGrupo );
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
     end;
     SetComplete;
end;

// SYNERGY
procedure TdmServerSistemaTimbrado.GrabaListaTermPorGrupo(const Terminales: WideString; UsuarioTerminal: Integer; const Grupo: WideString);
var
   sLista: TStringList;
   i: integer;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          EmpiezaTransaccion;
          try
             // Limpia grupo
             ExecSQL( EmpresaActiva, Format( GetScript( Q_BORRA_LISTA_TERM_POR_GRUPO ), [ Grupo ] ) );

             // Agrega nuevos registros
             sLista := TStringList.Create;
             sLista.CommaText := Terminales;

             for i := 0 to ( sLista.Count - 1 ) do
             begin
                  ExecSQL( EmpresaActiva, Format( GetScript( Q_AGREGA_LISTA_TERM_POR_GRUPO ), [ sLista[ i ], Grupo ] ) );
             end;

             TerminaTransaccion( True );
          except
                on Error : Exception do
                   RollBackTransaccion;
          end;
     end;
     SetComplete;
end;

// SYNERGY
function TdmServerSistemaTimbrado.AsignaNumBiometricos(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );

          FListaParametros := VACIO;
          FListaParametros := 'Asignaci�n de N�meros Biom�tricos y Grupo de Dispositivos [' + Paramlist.ParamByName( 'Grupo' ).AsString + ']';
     end;

     InitBroker;
     try
        FSinBiometrico := true;
        AsignaNumBiometricosBuildDataset;
        Result := AsignaNumBiometricosDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
        ClearBroker;
     end;
     SetComplete;
end;

// SYNERGY
function TdmServerSistemaTimbrado.AsignaNumBiometricosLista(Empresa, Lista, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );

          FListaParametros := VACIO;
          FListaParametros := 'Asignaci�n de N�meros Biom�tricos y Grupo de Dispositivos [' + Paramlist.ParamByName( 'Grupo' ).AsString + ']';
     end;

     cdsLista.Lista := Lista;
     Result := AsignaNumBiometricosDataset( cdsLista );
     SetComplete;
end;


// SYNERGY
function TdmServerSistemaTimbrado.EmpleadosSinBiometricosGetLista(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
     end;
     InitBroker;
     try
        FSinBiometrico := true;
        AsignaNumBiometricosBuildDataset;
        Result := SQLBroker.SuperReporte.GetReporte;
     finally
        ClearBroker;
     end;
     SetComplete;
end;

// SYNERGY
procedure TdmServerSistemaTimbrado.AsignaNumBiometricosBuildDataset;
begin
     with SQLBroker do
     begin
          Init( enEmpleado );
          with Agente do
          begin
               AgregaColumna( 'COLABORA.CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
               AgregaColumna( 'COLABORA.PRETTYNAME', True, Entidad, tgTexto, 93, 'PRETTYNAME' );
               AgregaColumna( 'COLABORA.CB_ACTIVO', True, Entidad, tgTexto, 1, 'CB_ACTIVO' );
               AgregaColumna( 'COLABORA.CB_ID_BIO', True, Entidad, tgNumero, 0, 'CB_ID_BIO' );
               AgregaColumna( 'COLABORA.CB_GP_COD', True, Entidad, tgTexto, 6, 'CB_GP_COD' );
               AgregaFiltro( '( COLABORA.CB_ACTIVO = ''S'' )', True, Entidad );
               if FSinBiometrico then
                  AgregaFiltro( '( COLABORA.CB_ID_BIO = 0 )', True, Entidad )
               else
                   AgregaFiltro( '( COLABORA.CB_ID_BIO > 0 )', True, Entidad );
               AgregaOrden( 'COLABORA.CB_CODIGO', True, Entidad );
          end;
          AgregaRangoCondicion( oZetaProvider.ParamList );
          FiltroConfidencial( oZetaProvider.EmpresaActiva );
          BuildDataset( oZetaProvider.EmpresaActiva );
     end;
end;

// SYNERGY
function TdmServerSistemaTimbrado.AsignaNumBiometricosDataset(Dataset: TDataset): OleVariant;
var
   iEmpleado: TNumEmp;
   FDataset: TZetaCursor;
   iNumBio: Integer;
   sGrupo, sEmpresa: string;
begin
     with oZetaProvider do
     begin
          if OpenProcess( prSISTAsignaNumBiometricos, Dataset.RecordCount, FListaParametros ) then
          begin
               try
                  FDataset := CreateQuery( GetScript( Q_AGREGA_NUM_BIOMETRICO ) );
                  with ParamList do
                  begin
                       sGrupo := ParamByName( 'Grupo' ).AsString;
                       sEmpresa := ParamByName( 'Empresa' ).AsString;
                  end;
                  try
                     with Dataset do
                     begin
                          First;
                          while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                          begin
                               iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                               iNumBio := ObtenIdBioMaximo;
                               if( iNumBio <> 0 )then
                               begin
                                    EmpiezaTransaccion;
                                    try
                                       ParamAsInteger( FDataset, 'CB_CODIGO', iEmpleado );
                                       ParamAsInteger( FDataset, 'CB_ID_BIO', iNumBio );
                                       ParamAsString( FDataset, 'CB_GP_COD', sGrupo );
                                       Ejecuta( FDataset );

                                       if( ActualizaIdBio( sEmpresa, iEmpleado, iNumBio, sGrupo, 0 ) )then
                                       begin
                                            TerminaTransaccion( TRUE );
                                            Log.Evento( clbSisBorrarBajas, iEmpleado, NullDateTime, Format( 'Se asign� el N�mero Biom�trico [%d] y Grupo de Dispositivos [%s] al Empleado', [ iNumBio, sGrupo ] ) );
                                       end
                                       else
                                       begin
                                            RollBackTransaccion;
                                            Log.ErrorGrave( iEmpleado, 'Error Al Asignar el N�mero Biom�trico del Empleado' );
                                       end;
                                    except
                                          on Error: Exception do
                                          begin
                                               RollBackTransaccion;
                                               Log.Excepcion( iEmpleado, 'Error Al Asignar el N�mero Biom�trico del Empleado', Error );
                                          end;
                                    end;
                               end;
                               Next;
                          end;
                     end;
                  finally
                      FreeAndNil( FDataSet );
                  end;
               except
                     on Error: Exception do
                     begin
                          Log.Excepcion( 0, 'Error Al Asignar el N�mero Biom�trico del Empleado', Error );
                     end;
               end;
          end;
          Result := CloseProcess;
     end;
     SetComplete;
end;

// SYNERGY
procedure TdmServerSistemaTimbrado.ReiniciaSincTerminales(const Lista: WideString);
begin
     {with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          EmpiezaTransaccion;
          try
             ExecSQL( EmpresaActiva, Format( GetScript( Q_REINICIA_SINC_TERMINALES ), [ Lista ] ) );
             TerminaTransaccion( True );
          except
                on Error : Exception do
                begin
                     TerminaTransaccion( False );
                     raise;
                end;
          end;
     end;
     SetComplete;}

     //OBSOLETO
end;

// SYNERGY
procedure TdmServerSistemaTimbrado.ActualizaTerminalesGTI(const Lista: WideString);
begin
     {with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          EmpiezaTransaccion;
          try
             ExecSQL( EmpresaActiva, Format( GetScript( Q_ACTUALIZA_TERMINALES_GTI ), [ Lista ] ) );
             TerminaTransaccion( True );
          except
                on Error : Exception do
                begin
                     TerminaTransaccion( False );
                     raise;
                end;
          end;
     end;
     SetComplete;}

     //OBSOLETO
end;

// SYNERGY
function TdmServerSistemaTimbrado.AsignaGrupoTerminales(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );

          FListaParametros := VACIO;
          FListaParametros := 'Asignaci�n de Grupo de Dispositivos [' + Paramlist.ParamByName( 'Grupo' ).AsString + ']';
     end;

     InitBroker;
     try
        FSinBiometrico := false;
        AsignaNumBiometricosBuildDataset;
        Result := AsignaGrupoTerminalesDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
        ClearBroker;
     end;
     SetComplete;
end;

// SYNERGY
function TdmServerSistemaTimbrado.AsignaGrupoTerminalesLista(Empresa, Lista, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );

          FListaParametros := VACIO;
          FListaParametros := 'Asignaci�n de Grupo de Dispositivos [' + Paramlist.ParamByName( 'Grupo' ).AsString + ']';
     end;

     cdsLista.Lista := Lista;
     Result := AsignaGrupoTerminalesDataset( cdsLista );
     SetComplete;
end;

// SYNERGY
function TdmServerSistemaTimbrado.EmpleadosBiometricosGetLista(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
     end;
     InitBroker;
     try
        FSinBiometrico := false;
        AsignaNumBiometricosBuildDataset;
        Result := SQLBroker.SuperReporte.GetReporte;
     finally
        ClearBroker;
     end;
     SetComplete;
end;

// SYNERGY
function TdmServerSistemaTimbrado.AsignaGrupoTerminalesDataset( Dataset: TDataset ): OleVariant;
var
   iEmpleado: TNumEmp;
   FDataset: TZetaCursor;
   iNumBio: Integer;
   sGrupo, sEmpresa: string;
begin
     with oZetaProvider do
     begin
          if OpenProcess( prSISTAsignaGrupoTerminales, Dataset.RecordCount, FListaParametros ) then
          begin
               try
                  FDataset := CreateQuery( GetScript( Q_AGREGA_NUM_BIOMETRICO ) );
                  with ParamList do
                  begin
                       sGrupo := ParamByName( 'Grupo' ).AsString;
                       sEmpresa := ParamByName( 'Empresa' ).AsString;
                  end;

                  try
                     with Dataset do
                     begin
                          First;
                          while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                          begin
                               // En lugar de obtener el n�mero, se actualiza con el existente (solo se actualiza el grupo)
                               iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                               iNumBio := FieldByName( 'CB_ID_BIO' ).AsInteger;
                               EmpiezaTransaccion;
                               try
                                  ParamAsInteger( FDataset, 'CB_CODIGO', iEmpleado );
                                  ParamAsInteger( FDataset, 'CB_ID_BIO', iNumBio );
                                  ParamAsString( FDataset, 'CB_GP_COD', sGrupo );
                                  Ejecuta( FDataset );

                                  if( ActualizaIdBio( sEmpresa, iEmpleado, iNumBio, sGrupo, 0 ) )then
                                  begin
                                       TerminaTransaccion( TRUE );
                                       Log.Evento( clbSisBorrarBajas, iEmpleado, NullDateTime, Format( 'Se asign� el Grupo de Terminales [%s] al Empleado con N�mero Biom�trico [%d]', [ sGrupo, iNumBio ] ) );
                                  end
                                  else
                                  begin
                                       RollBackTransaccion;
                                       Log.ErrorGrave( iEmpleado, 'Error Al Asignar el Grupo de Terminales' );
                                  end;
                               except
                                     on Error: Exception do
                                     begin
                                          RollBackTransaccion;
                                          Log.Excepcion( iEmpleado, 'Error Al Asignar el Grupo de Terminales', Error );
                                     end;
                               end;
                               Next;
                          end;
                     end;
                  finally
                      FreeAndNil( FDataSet );
                  end;
               except
                     on Error: Exception do
                     begin
                          Log.Excepcion( 0, 'Error Al Asignar el Grupo de Terminales', Error );
                     end;
               end;
          end;
          Result := CloseProcess;
     end;
     SetComplete;
     oZetaProvider.EscribeBitacoraComparte( tbNormal, clbTerminalesGTI, NullDateTime, 'Relaci�n de Grupo-Terminales', 'Se actualiz� las relaciones del cat�logo de Grupo-Terminales', FUsuario);
end;

// SYNERGY
function TdmServerSistemaTimbrado.ConsultaMensaje(const Filtro: WideString): OleVariant;
begin
     SetTablaInfo( eMensajes );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Filtro;
          Result := GetTabla( Comparte );
     end;
     SetComplete;
end;

// SYNERGY
function TdmServerSistemaTimbrado.ConsultaMensajeTerminal(const Filtro: WideString): OleVariant;
begin
     SetTablaInfo( eMensajesTerm );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Filtro;
          Result := GetTabla( Comparte );
     end;
     SetComplete;
end;

// SYNERGY
function TdmServerSistemaTimbrado.ConsultaTerminal(const Filtro: WideString): OleVariant;
begin
     SetTablaInfo( eTerminales );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Filtro;
          Result := GetTabla( Comparte );
     end;
     SetComplete;
end;

// SYNERGY
function TdmServerSistemaTimbrado.GrabaMensaje(oDelta: OleVariant; UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant;
begin
     FUsuario := UsuarioTerminal;
     SetTablaInfo( eMensajes );
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
     end;
     SetComplete;
end;

// SYNERGY
function TdmServerSistemaTimbrado.GrabaMensajeTerminal(oDelta: OleVariant; UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant;
begin
     FUsuario := UsuarioTerminal;
     SetTablaInfo( eMensajesTerm );
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
     end;
     SetComplete;
end;

// SYNERGY
function TdmServerSistemaTimbrado.GrabaTerminal(oDelta: OleVariant; UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant;
begin
     FUsuario := UsuarioTerminal;
     SetTablaInfo( eTerminales );
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
     end;
     SetComplete;
end;

// SYNERGY
function TdmServerSistemaTimbrado.ImportaTerminales(Parametros: OleVariant; UsuarioTerminal: Integer): OleVariant;
var
   FDataSet: TZetaCursor;
   sListaTerminales: String;
   oListaTerminalesNuevas, oListaTerminalesAgregadas: TStringList;
   i, iTotal: Integer;
begin
     iTotal := 0;
     with oZetaProvider do
     begin
          EmpresaActiva:= Comparte;
          AsignaParamList( Parametros );
          with ParamList do
          begin
               sListaTerminales := ParamByName( 'ListaTerminales' ).AsString;
               FListaParametros := 'Importando terminales';
          end;

          Result := GetEmptyProcessResult( prSISTImportaTerminales );

          //if OpenProcess( prSISTImportaTerminales, 1, FListaParametros )then
          //begin
               try
                  oListaTerminalesAgregadas := TStringList.Create;
                  try
                     oListaTerminalesNuevas := TStringList.Create;
                     FDataSet := CreateQuery( 'select TE_MAC from TERMINAL order by TE_CODIGO' );
                     try
                        with FDataSet do
                        begin
                             Open;
                             while not EOF do
                             begin
                                  oListaTerminalesAgregadas.Append( Decrypt( Trim( FieldByName( 'TE_MAC' ).AsString ) ) );
                                  Next;
                             end;
                             Close;
                        end;
                     finally
                            FreeAndNil( FDataSet );
                     end;

                     oListaTerminalesNuevas.CommaText := sListaTerminales;
                     for i:=0 to (oListaTerminalesNuevas.Count - 1)do
                     begin
                          if( oListaTerminalesAgregadas.IndexOf( Trim( oListaTerminalesNuevas[i] ) ) < 0 )then
                          begin
                               FDataSet := CreateQuery( Format( 'insert into TERMINAL (TE_MAC, TE_ZONA, TE_APP, TE_ACTIVO) values (''%s'', ''Pacific Standard Time (Mexico)'', 1, ''S'')', [ Encrypt( Trim( oListaTerminalesNuevas[ i ] ) ) ] ) );
                               try
                                  Ejecuta( FDataSet );
                                  oListaTerminalesAgregadas.Append( Trim( oListaTerminalesNuevas[i] ) );
                                  Inc( iTotal );
                               finally
                                      FreeAndNil( FDataSet )
                               end;
                          end;
                     end;
                     TerminaTransaccion( True );
                     SetProcessOK( Result );
                  finally
                         FreeAndNil( oListaTerminalesNuevas );
                  end;
               finally
                      FreeAndNil( oListaTerminalesAgregadas );
               end;
          //end;
          //Result := CloseProcess;
     end;
     SetComplete;
     oZetaProvider.EscribeBitacoraComparte( tbNormal, clbTerminalesGTI, NullDateTime, 'Importaci�n de terminales', Format( 'Se importaron %d terminales a los cat�logos del Sistema', [ iTotal ] ), FUsuario);
end;

// SYNERGY
function TdmServerSistemaTimbrado.ExisteHuellaRegistrada(Empleado: Integer; const Empresa: WideString): WordBool;
var
   FDataSet: TZetaCursor;
begin
     Result := False;
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          if( Empleado > 0 )then
          begin
               FDataSet := CreateQuery( Format( GetScript( Q_EXISTE_HUELLA_EMP ), [ Empresa, Empleado ] ) );
               try
                  with FDataset do
                  begin
                       Active := True;
                       if not EOF then
                          Result := True;
                       Active := False;
                  end;
               finally
                      FreeAndNil( FDataSet );
               end;
          end;
     end;
     SetComplete;
end;

// SYNERGY
procedure TdmServerSistemaTimbrado.ReiniciaTerminal(Terminal: Integer; const Empresa: WideString);
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          if( Terminal > 0 )then
          begin
               // Limpia la relacion de la terminal con las huellas en TERM_HUELLA
               EjecutaAndFree( Format( GetScript( Q_REINICIA_TERMINAL ), [ Format( 'where TE_CODIGO=%d', [ Terminal ] ) ] ) );
               // Reinicia la fecha (nula) de la tabla de TERM_MSG
               EjecutaAndFree( Format( GetScript( Q_REINICIA_TERM_MSG_FECHA ), [ Format( 'and TE_CODIGO=%d', [ Terminal ] ) ] ) );
               oZetaProvider.EscribeBitacoraComparte( tbNormal, clbTerminalesGTI, Now, 'Reinicio de Terminales', Format( 'Se reinicio la terminal %d', [ Terminal ] ), FUsuario);
          end
          else if ( Terminal = -1 )then // -1 representa reiniciar TODAS las terminales
          begin
               // Limpia la relacion de la terminal con las huellas en TERM_HUELLA
               EjecutaAndFree( Format( GetScript( Q_REINICIA_TERMINAL ), [ VACIO ] ) );
               // Reinicia la fecha (nula) de la tabla de TERM_MSG
               EjecutaAndFree( Format( GetScript( Q_REINICIA_TERM_MSG_FECHA ), [ VACIO ] ) );
               oZetaProvider.EscribeBitacoraComparte( tbNormal, clbTerminalesGTI, Now, 'Reinicio de Terminales', 'Se reiniciaron todas las terminales', FUsuario);
          end;
     end;
     SetComplete;
end;

// SYNERGY
procedure TdmServerSistemaTimbrado.SetAfterUpdateGruposDisp( Sender: TObject; SourceDS: TDataSet; DeltaDS: TZProviderClientDataSet; UpdateKind: TUpdateKind );
var
   sTitulo, sMensaje: string;
   lCambiosBit: Boolean;
begin
     lCambiosBit := False;
     if( UpdateKind = ukDelete )then
     begin
          sTitulo := 'Borrado de cat�logo de Grupos de Terminales';
          sMensaje := Format( 'Se elimin� el registro %s del cat�logo', [ ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'GP_CODIGO' ) ) ] );
          lCambiosBit := True;
     end
     else if( UpdateKind = ukModify )then
     begin
          sTitulo := 'Edici�n de cat�logo de Grupos de Terminales';
          sMensaje := Format( 'Se modific� el registro %s del cat�logo', [ ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'GP_CODIGO' ) ) ] );
          if( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'GP_CODIGO' ) ) <> ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'GP_CODIGO' ) ) )then
          begin
               sMensaje := sMensaje + CR_LF + 'C�digo' +
                           CR_LF + 'De: ' + ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'GP_CODIGO' ) ) +
                           CR_LF + 'A: ' + ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'GP_CODIGO' ) ) + CR_LF;
               lCambiosBit := True;
          end;
          if( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'GP_DESCRIP' ) ) <> ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'GP_DESCRIP' ) ) )then
          begin
               sMensaje := sMensaje + 'Descripci�n' +
                           CR_LF + 'De: ' + ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'GP_DESCRIP' ) ) +
                           CR_LF + 'A: ' + ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'GP_DESCRIP' ) );
               lCambiosBit := True;
          end;
     end
     else // UpdateKind = ukInsert
     begin
          sTitulo := 'Nuevo registro en cat�logo de Grupos de Terminales';
          sMensaje := 'Se agreg� registro al cat�logo' +
                      CR_LF + 'C�digo: ' + ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'GP_CODIGO' ) ) +
                      CR_LF + 'Descripci�n: ' + ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'GP_DESCRIP' ) );
          lCambiosBit := True;
     end;
     if lCambiosBit then
        oZetaProvider.EscribeBitacoraComparte( tbNormal, clbTerminalesGTI, Now, sTitulo, sMensaje, FUsuario);
end;

// SYNERGY
procedure TdmServerSistemaTimbrado.SetAfterUpdateMensajesPorTerminal( Sender: TObject; SourceDS: TDataSet; DeltaDS: TZProviderClientDataSet; UpdateKind: TUpdateKind );
var
   sTitulo, sMensaje: string;
   lCambiosBit: Boolean;
begin
     lCambiosBit := False;
     if( UpdateKind = ukDelete )then
     begin
          sTitulo := 'Borrado de cat�logo de Mensajes por Terminal';
          sMensaje := 'Se elimin� el mensaje ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'DM_CODIGO' ) ) ) +
                      ' de la terminal ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TE_CODIGO' ) ) );
          lCambiosBit := True;
     end
     else if( UpdateKind = ukModify )then
     begin
          sTitulo := 'Edici�n de cat�logo de Mensajes por Terminal';
          sMensaje := 'Se modific� el registro del mensaje ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'DM_CODIGO' ) ) ) +
                      ' de la terminal ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TE_CODIGO' ) ) );
          if( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TE_CODIGO' ) ) <> ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TE_CODIGO' ) ) )then
          begin
               sMensaje := sMensaje + CR_LF + 'Terminal' +
                           CR_LF + 'De: ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TE_CODIGO' ) ) ) +
                           CR_LF + 'A: ' + IntToStr( ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TE_CODIGO' ) ) ) + CR_LF;
               lCambiosBit := True;
          end;
          if( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'DM_CODIGO' ) ) <> ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'DM_CODIGO' ) ) )then
          begin
               sMensaje := sMensaje + CR_LF + 'Mensaje' +
                           CR_LF + 'De: ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'DM_CODIGO' ) ) ) +
                           CR_LF + 'A: ' + IntToStr( ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'DM_CODIGO' ) ) ) + CR_LF;
               lCambiosBit := True;
          end;
          if( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TM_SLEEP' ) ) <> ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TM_SLEEP' ) ) )then
          begin
               sMensaje := sMensaje + CR_LF + 'Espera' +
                           CR_LF + 'De: ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TM_SLEEP' ) ) ) +
                           CR_LF + 'A: ' + IntToStr( ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TM_SLEEP' ) ) ) + CR_LF;
               lCambiosBit := True;
          end;
          if( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TM_NEXT' ) ) <> ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TM_NEXT' ) ) )then
          begin
               sMensaje := sMensaje + CR_LF + 'Siguiente mensaje' +
                           CR_LF + 'De: ' + FechaCorta( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TM_NEXT' ) ) ) +
                           CR_LF + 'A: ' + FechaCorta( ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TM_NEXT' ) ) );
               lCambiosBit := True;
          end;
     end
     else // UpdateKind = ukInsert
     begin
          sTitulo := 'Nuevo registro en cat�logo de Mensajes por Terminal';
          sMensaje := 'Se agreg� registro al cat�logo' +
                      CR_LF + 'Terminal: ' + IntToStr( ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TE_CODIGO' ) ) ) +
                      CR_LF + 'Mensaje: ' + IntToStr( ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'DM_CODIGO' ) ) );
          lCambiosBit := True;
     end;
     if lCambiosBit then
        oZetaProvider.EscribeBitacoraComparte( tbNormal, clbTerminalesGTI, Now, sTitulo, sMensaje, FUsuario);
end;

// SYNERGY
procedure TdmServerSistemaTimbrado.SetAfterUpdateTerminales( Sender: TObject; SourceDS: TDataSet; DeltaDS: TZProviderClientDataSet; UpdateKind: TUpdateKind );
var
   sTitulo, sMensaje: string;
   lCambiosBit: Boolean;
begin
     lCambiosBit := False;
     if( UpdateKind = ukDelete )then
     begin
          sTitulo := 'Borrado de cat�logo de Terminales';
          sMensaje := 'Se elimin� la terminal ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TE_CODIGO' ) ) );
          lCambiosBit := True;
     end
     else if( UpdateKind = ukModify )then
     begin
          sTitulo := 'Edici�n de cat�logo de Terminales';
          sMensaje := 'Se modific� el registro de la terminal ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TE_CODIGO' ) ) );
          if( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TE_CODIGO' ) ) <> ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TE_CODIGO' ) ) )then
          begin
               sMensaje := sMensaje + CR_LF + 'C�digo' +
                           CR_LF + 'De: ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TE_CODIGO' ) ) ) +
                           CR_LF + 'A: ' + IntToStr( ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TE_CODIGO' ) ) ) + CR_LF;
               lCambiosBit := True;
          end;
          if( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TE_NOMBRE' ) ) <> ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TE_NOMBRE' ) ) )then
          begin
               sMensaje := sMensaje + CR_LF + 'Descripci�n' +
                           CR_LF + 'De: ' + ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TE_NOMBRE' ) ) +
                           CR_LF + 'A: ' + ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TE_NOMBRE' ) ) + CR_LF;
               lCambiosBit := True;
          end;
          if( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TE_APP' ) ) <> ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TE_APP' ) ) )then
          begin
               sMensaje := sMensaje + CR_LF + 'Aplicaci�n' +
                           CR_LF + 'De: ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TE_APP' ) ) ) +
                           CR_LF + 'A: ' + IntToStr( ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TE_APP' ) ) ) + CR_LF;
               lCambiosBit := True;
          end;
          if( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TE_ZONA' ) ) <> ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TE_ZONA' ) ) )then
          begin
               sMensaje := sMensaje + CR_LF + 'Zona' +
                           CR_LF + 'De: ' + ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TE_ZONA' ) ) +
                           CR_LF + 'A: ' + ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TE_ZONA' ) );
               lCambiosBit := True;
          end;
     end
     else // UpdateKind = ukInsert
     begin
          sTitulo := 'Nuevo registro en cat�logo de Terminales';
          sMensaje := 'Se agreg� registro al cat�logo' +
                      CR_LF + 'Terminal: ' + IntToStr( ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TE_CODIGO' ) ) ) +
                      CR_LF + 'Descripci�n: ' + IntToStr( ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TE_NOMBRE' ) ) );
          lCambiosBit := True;
     end;
     if lCambiosBit then
        oZetaProvider.EscribeBitacoraComparte( tbNormal, clbTerminalesGTI, Now, sTitulo, sMensaje, FUsuario);
end;

// SYNERGY
function TdmServerSistemaTimbrado.ExisteEmpleadoEnGrupo(const Grupo: WideString): WordBool;
var
   FDataSet: TZetaCursor;
begin
     Result := True;
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          if StrLleno( Grupo )then
          begin
               FDataSet := CreateQuery( Format( GetScript( Q_EXISTE_EMPLEADO_EN_GRUPO ), [ Grupo ] ) );
               try
                  with FDataset do
                  begin
                       Active := True;
                       if not EOF then
                          Result := True
                       else
                           Result := False;
                       Active := False;
                  end;
               finally
                      FreeAndNil( FDataSet );
               end;
          end;
     end;
     SetComplete;
end;

// BIOMETRICO
function TdmServerSistemaTimbrado.ObtieneTemplates(Parametros: OleVariant): OleVariant;
var
   sScript, sMensaje, sTitle : String;
begin
     sMensaje := VACIO;
     sTitle := VACIO;

     with oZetaProvider do
     begin
          AsignaParamList( Parametros );
          if (ParamList.IndexOf('ComputerName') > 0) then
               sScript := Format( GetScript( Q_HUELLA_OBTENER_MAQUINA ) , [ ParamList.ParamByName('ComputerName').AsString ] )
          else
          begin
              sScript := GetScript( Q_HUELLA_OBTENER_TODOS );
              {if (ParamList.IndexOf('FechaPivote') > 0) then
                   sScript := sScript + ' WHERE HU_TIMEST >= ' + DateToStrSQLC( ParamList.ParamByName('FechaPivote').AsDateTime ) ;}
          end;

          EmpresaActiva := Comparte;
          Result := OpenSQL( EmpresaActiva, sScript, true );
     end;
     SetComplete;
end;

// BIOMETRICO
procedure TdmServerSistemaTimbrado.EliminaHuellas(const Empresa: WideString; Empleado, Invitador, Usuario: Integer);
var
   sQuery, sTitle, sMensaje : String;
begin
     sTitle := 'Eliminaci�n de Huellas';
     sMensaje := ' Empleado: ' + IntToStr( Empleado );
     sQuery := Format( GetScript( Q_HUELLA_BORRA ),[ Empresa ]);

     if ( Invitador <> 0 ) then
     begin
          sMensaje := sMensaje + CR_LF + ' Invitador: ' + IntToStr( Invitador );
          sQuery := sQuery + ' and IV_CODIGO = ' + IntToStr( Invitador );
     end
     else
          sQuery := sQuery + ' and CB_CODIGO = ' + IntToStr( Empleado ) + ' and IV_CODIGO = 0';

     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          EjecutaAndFree( sQuery );
          EscribeBitacoraComparte(tbNormal, clbBiometrico, Now, sTitle, sTitle + CR_LF + sMensaje, Usuario);
     end;
     SetComplete;
end;

// BIOMETRICO
procedure TdmServerSistemaTimbrado.InsertaHuella(Parametros: OleVariant);
var
   sQuery, sTitle, sMensaje, sEmpresa, sHuella, sComputadora : String;
   iEmpleado, iDedo, iInvitador, iUsuario : Integer;
begin
     sTitle := 'Enrolamiento de Huella';
     with oZetaProvider do
     begin
          AsignaParamList( Parametros );

          sEmpresa     := ParamList.ParamByName('Empresa').AsString;
          iEmpleado    := ParamList.ParamByName('Empleado').AsInteger;
          iDedo        := ParamList.ParamByName('Dedo').AsInteger;
          sHuella    := ParamList.ParamByName('Huella').AsString;
          iInvitador   := ParamList.ParamByName('Invitador').AsInteger;
          sComputadora := ParamList.ParamByName('Computadora').AsString;
          iUsuario     := ParamList.ParamByName('Usuario').AsInteger;

          sMensaje := ' Empleado: ' + IntToStr( iEmpleado );
          sQuery := Format( GetScript( Q_HUELLA_INSERTA ), [ sEmpresa, iEmpleado, iDedo, sHuella, iInvitador, sComputadora ] );

          if ( iInvitador <> 0 ) then
             sMensaje := sMensaje + CR_LF + ' Invitador: ' + IntToStr( iInvitador );

          sMensaje := sMensaje + CR_LF + ' �ndice: ' + IntToStr( iDedo );

          with oZetaProvider do
          begin
               EmpresaActiva := Comparte;
               EjecutaAndFree( sQuery );
               EscribeBitacoraComparte(tbNormal, clbBiometrico, Now, sTitle, sTitle + CR_LF + sMensaje, iUsuario);
          end;
     end;
     SetComplete;
end;

function TdmServerSistemaTimbrado.ReportaHuella( Parametros: OleVariant ): OleVariant;
var
   sQuery : String;
   sRenglones : TStringList;
   i : Integer;

   function ExisteParametro( sNombreParametro : String ) : Boolean;
   begin
        Result := ( oZetaProvider.ParamList.IndexOf(sNombreParametro) >= 0 );
   end;

   procedure Split (const Delimiter: Char; Input: string; const Strings: TStrings) ;
   begin
        Assert(Assigned(Strings)) ;
        Strings.Clear;
        Strings.Delimiter := Delimiter;
        Strings.DelimitedText := Input;
   end;

begin
     with oZetaProvider do
     begin
          AsignaParamList( Parametros );

          if ( ExisteParametro( 'ComputerName' ) ) then
          begin
               EmpresaActiva := Comparte;
               if ( ExisteParametro( 'HuellaId' ) ) then
               begin
                    sRenglones := TStringList.Create;
                    try
                       Split(',', ParamList.ParamByName('HuellaId').AsString, sRenglones) ;
                       if ( sRenglones.Count > 0 ) then
                            for i := 0 to sRenglones.Count - 1 do
                            begin
                                 sQuery := Format( GetScript( Q_HUELLA_REPORTA_INSERTA ), [ ParamList.ParamByName('ComputerName').AsString, sRenglones[i] ]);
                                 EjecutaAndFree( sQuery );
                            end;
                    finally
                           sRenglones.Free;
                    end;
               end
               else
               begin
                    sQuery := Format( GetScript( Q_HUELLA_REPORTA_ELIMINA ), [ ParamList.ParamByName('ComputerName').AsString ]);
                    EjecutaAndFree( sQuery );
               end;
          end
     end;
     SetComplete;
end;

function TdmServerSistemaTimbrado.ExisteHuellaInvitador(const Empresa: WideString; Invitador: Integer): WordBool;
var
   FDataSet: TZetaCursor;
begin
     Result := False;
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          if( Invitador > 0 )then
          begin
               FDataSet := CreateQuery( Format( GetScript( Q_EXISTE_HUELLA_INV ), [ Empresa, Invitador ] ) );
               try
                  with FDataset do
                  begin
                       Active := True;
                       if not EOF then
                          Result := True;
                       Active := False;
                  end;
               finally
                      FreeAndNil( FDataSet );
               end;
          end;
     end;
     SetComplete;
end;

{$endif}


function TdmServerSistemaTimbrado.GetCuentasTimbrado: OleVariant;
begin
   with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          EmpiezaTransaccion;
          Result := OpenSQL( EmpresaActiva, GetScript( Q_SIST_GET_CUENTAS_TRIMBRADO ), True );
          TerminaTransaccion(True);
     end;
     SetComplete;
end;


function TdmServerSistemaTimbrado.GrabaCuentasTimbrado(oDelta: OleVariant;
  out ErrorCount: Integer): OleVariant;
begin
      SetTablaInfo( eCuentasTimbrado );
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
     end;
     SetComplete;
end;

function TdmServerSistemaTimbrado.ExistenEstructurasTimbradoComparte: WordBool;
const
{$ifdef INTERBASE}
     Q_EXISTE_CUENTATIM = 'select count(*) as CUANTOS from RDB$RELATION_FIELDS where RDB$RELATION_Name = ''CUENTATIM'' ';
{$endif}
{$ifdef MSSQL}
     Q_EXISTE_CUENTATIM = 'select count(*) as CUANTOS from DBO.SYSCOLUMNS where id = object_id(N''CUENTATIM'')';
{$endif}
var
   FDataSet: TZetaCursor;
begin
     Result := False;
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          FDataSet:= CreateQuery( Q_EXISTE_CUENTATIM );
          try
             with FDataset do
             begin
                  Active := True;
                  if( FieldByName('CUANTOS').AsInteger > 0 ) then
                  begin
                       Result := True;
                  end;
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataSet );
          end;
     end;
end;

function TdmServerSistemaTimbrado.CrearEstructurasTimbradoComparte: WordBool;
begin
  with oZetaProvider do
  begin
       try
          EmpresaActiva := Comparte;
          EmpiezaTransaccion;
          if not (ExistenEstructurasTimbradoComparte) then
          begin
               ExecSQL( Comparte, GetScript( Q_PATCH_TIMBRADO_COMPARTE ));
          end;
          TerminaTransaccion( TRUE );
       except
             on Error: Exception do
             begin
                  TerminaTransaccion( FALSE );
                  raise;
             end;
       end;
  end;
end;

function TdmServerSistemaTimbrado.ExistenEstructurasTimbradoEmpresa(
  Empresa: OleVariant): WordBool;
const
{$ifdef INTERBASE}
     Q_EXISTE_NO_TIMBRO = 'select count(*) as CUANTOS from RDB$RELATION_FIELDS where RDB$RELATION_Name = ''NOMINA'' and RDB$Field_name = ''NO_TIMBRO'' ';
{$endif}
{$ifdef MSSQL}
     Q_EXISTE_NO_TIMBRO = 'select count(*) as CUANTOS from DBO.SYSCOLUMNS where NAME = ''NO_TIMBRO'' and id = object_id(N''NOMINA'')';
{$endif}
var
   FDataSet: TZetaCursor;
begin
     Result := False;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FDataSet:= CreateQuery( Q_EXISTE_NO_TIMBRO );
          try
             with FDataset do
             begin
                  Active := True;
                  if( FieldByName('CUANTOS').AsInteger > 0 ) then
                  begin
                       Result := True;
                  end;
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataSet );
          end;
     end;
end;

function TdmServerSistemaTimbrado.CrearEstructurasTimbradoEmpresa(Empresa: OleVariant): WordBool;
var Scripts,Lista :  TStringList;
  i,j : Integer;
  Tabla,Campo:string;
  EsNuevaTabla,EsTabla,EsDiccion,EsProcedure,EsOtro,EsIndice,EsFuncion,EsAlterStoreProcedure :Boolean;

CONST

     Q_EXISTE_DICCION = 'select count(*) as CUANTOS from DICCION where DI_NOMBRE = ''%s''';


  function VerificaExists( Query:string ):Boolean;
  begin
       Result := Pos('|',Query) > 0;
       if Result then
       begin
            Lista := TStringList.Create;
            try
               Lista.CommaText := Query;
               Tabla := Lista.Strings[1];
               EsNuevaTabla:= Pos('|N',Query) > 0;
               EsTabla := Pos('|T',Query) > 0;
               EsDiccion := Pos('|D',Query) > 0;
               EsOtro := Pos('|O',Query)>0;
               EsIndice := Pos('|I',Query)>0;
               EsFuncion := pos('|F',Query)>0;
               EsAlterStoreProcedure := pos('|ASP',Query)>0;
               if EsTabla or  EsNuevaTabla or EsIndice then
                  Campo := Lista.Strings[2]; //campo sera tambien el nombre del indice
               EsProcedure := Not EsNuevaTabla and Not EsTabla and Not EsDiccion and not esIndice and not Esfuncion and not EsOtro and not EsAlterStoreProcedure;
            finally
                   Lista.Free;
            end;
       end;
  end;


  function ExistsDiccion :Boolean;
  var
     FDataSet: TZetaCursor;
  begin
       Result := False;
       with oZetaProvider do
       begin
          EmpresaActiva := Empresa;
          FDataSet:= CreateQuery( Format(Q_EXISTE_DICCION,[Tabla]) );
          try
             with FDataset do
             begin
                  Active := True;
                  if( FieldByName('CUANTOS').AsInteger > 0 ) then
                  begin
                       Result := True;
                  end;
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataSet );
          end;
       end;
  end;

  procedure EjecutaScript (Script :string);
  begin
       with oZetaProvider do
       begin
            EmpiezaTransaccion;
            try
               ExecSQL( EmpresaActiva, Scripts[i] );
            finally
                   TerminaTransaccion(True);
            end;
       end;
  end;

  procedure AplicarPatch;
  begin
       i:= 0;
       j:= 0;
       while ( i <= Scripts.Count - 1 )  do
       begin
            if VerificaExists(Scripts[i]) then
            begin
                 i:= i + 1;

                 if EsNuevaTabla then
                 begin
                    if ExistsTabla(Tabla,Campo)   then
                    begin
                         i := i + 58;
                    end
                    else
                    begin
                         EjecutaScript(Scripts[i]);
                         i:= i + 1;
                         if ( Tabla = 'TIPO_SAT' ) then
                         begin
                              while( j < 57 )do
                              begin
                                   EjecutaScript(Scripts[i]);
                                   i:= i + 1;
                                   j:= j+1;
                              end;
                              j:= 0;
                         end;

                    end;
                 end
                 else
                 if  EsTabla then
                 begin
                      if ExistsTabla(Tabla,Campo) then
                      begin
                           i := i + 1;
                         {$ifdef MSSQL}
                         if (Tabla = 'RSOCIAL' )or (Tabla = 'PERIODO') or ( Tabla = 'CONCEPTO')  then
                              i:= i + 1;
                         {$endif}
                      end
                      else
                      begin
                         EjecutaScript(Scripts[i]);
                         i:= i + 1;
                         EjecutaScript(Scripts[i]);
                         {$ifdef MSSQL}
                         if (Tabla = 'RSOCIAL' )or (Tabla = 'PERIODO') or ( Tabla = 'CONCEPTO')  then
                         begin
                              i:= i + 1;
                              EjecutaScript(Scripts[i]);
                         end;
                         {$endif}
                      end;
                 end
                 else if ( EsDiccion )then
                 begin
                      if ExistsDiccion then
                      begin
                           if( Tabla = 'NO_FACUUID' ) then
                           begin
                           end
                           else
                           begin
                                i:= i + 5;
                           end;
                      end
                      else
                      begin
                           if( Tabla = 'NO_FACUUID' ) then
                           begin
                                EjecutaScript(Scripts[i]);
                                i:= i + 1;
                           end
                           else
                           begin
                                while( j < 6 )do
                                begin
                                     EjecutaScript(Scripts[i]);
                                     i:= i + 1;
                                     j:= j+1;
                                end;
                           end;
                           {$ifdef DOS_CAPAS}
                           i := i - 1;  //Ajusta para el SP
                           {$endif}
                      end;

                      j := 0;
                 end
                 else if ( EsProcedure ) then
                 begin
                      if ( Not ExistsProcedure(Tabla) )then
                      begin
                           EjecutaScript(Scripts[i]);
                      end;
                 end
                 else if ( EsFuncion ) then
                 begin
                      if ( Not ExistsFunction(Tabla) )then
                      begin
                           EjecutaScript(Scripts[i])
                      end;
                 end
                 else if ( EsIndice ) then
                 begin
                      if ( Not ExistsIndex(Tabla,Campo) )then
                      begin
                           EjecutaScript(Scripts[i])
                      end;
                 end
                 else if(EsOtro) then
                 begin

                 end
                 else if ( EsAlterStoreProcedure ) then
                 begin
                      if ( ExistsProcedure(Tabla) )then
                      begin
                           EjecutaScript(Scripts[i])//Ejecutar Alter Procedure
                      end;
                 end
                 else
                     i:= i + 1;
            end
            else
            begin
                     EjecutaScript(Scripts[i]);
            end;
            i:= i + 1;
       end;
  end;


begin

   Scripts :=  TStringList.Create();

   with oZetaProvider do
    begin
    try
       try
          EmpresaActiva := Empresa;
          AplicarPatch;
          //Validar Patch
          if Not ValidarPatchTimbrado then
             AplicarPatch;
          Result := ValidarPatchTimbrado;
       except
             on Error: Exception do
             begin
                  raise;
             end;
       end;
       finally
           Scripts.Free;
       end;
    end;
end;

function TdmServerSistemaTimbrado.ExistsProcedure ( sProc:string) :Boolean;
  var
     FDataSet: TZetaCursor;
  const
     {$IFDEF INTERBASE}
      Q_EXISTE_PROC = 'select count(*) as CUANTOS from RDB$procedures where RDB$procedure_NAME = ''%s''';
     {$ENDIF}
     {$IFDEF MSSQL}
      Q_EXISTE_PROC = 'select count(*) as CUANTOS FROM sysobjects WHERE type = ''P'' AND name = ''%s''';
      {$ENDIF}

  begin
       Result := False;
       with oZetaProvider do
       begin
          //EmpresaActiva := Empresa;
          FDataSet:= CreateQuery( Format(Q_EXISTE_PROC,[sProc]) );
          try
             with FDataset do
             begin
                  Active := True;
                  if( FieldByName('CUANTOS').AsInteger > 0 ) then
                  begin
                       Result := True;
                  end;
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataSet );
          end;
       end;
  end;
  function TdmServerSistemaTimbrado.ExistsFunction ( sProc:string) :Boolean;
  var
     FDataSet: TZetaCursor;
  const
     {$IFDEF INTERBASE}
      Q_EXISTE_PROC = 'select count(*) as CUANTOS from RDB$Functions where RDB$Function_NAME = ''%s''';
     {$ENDIF}

     {$IFDEF MSSQL}
       Q_EXISTE_PROC = 'select count(*) as CUANTOS FROM sysobjects WHERE NAME = ''%s'' AND type IN ( N''FN'', N''IF'', N''TF'', N''FS'', N''FT'' )';
     {$ENDIF}

  begin
       Result := False;
       with oZetaProvider do
       begin
          //EmpresaActiva := Empresa;
          FDataSet:= CreateQuery( Format(Q_EXISTE_PROC,[sProc]) );
          try
             with FDataset do
             begin
                  Active := True;
                  if( FieldByName('CUANTOS').AsInteger > 0 ) then
                  begin
                       Result := True;
                  end;
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataSet );
          end;
       end;
  end;

  function TdmServerSistemaTimbrado.ExistsIndex(Tabla,Indice:string) :Boolean;
  var
     FDataSet: TZetaCursor;
  const
       Q_EXISTE = 'select count(*) as CUANTOS FROM sys.indexes where Object_id = OBJECT_ID(''%s'') and NAME = ''%s'' ';
  begin
       Result := False;
       with oZetaProvider do
       begin
          //EmpresaActiva := Empresa;
          FDataSet:= CreateQuery( Format(Q_EXISTE,[Tabla,Indice]) );
          try
             with FDataset do
             begin
                  Active := True;
                  if( FieldByName('CUANTOS').AsInteger > 0 ) then
                  begin
                       Result := True;
                  end;
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataSet );
          end;
       end;
  end;


  function TdmServerSistemaTimbrado.ExistsTabla(Tabla,Campo:string) :Boolean;
  var
     FDataSet: TZetaCursor;
  const
       {$IFDEF INTERBASE}
       Q_EXISTE = 'select count(*) as CUANTOS from RDB$RELATION_FIELDS where RDB$RELATION_Name = ''%s'' and RDB$Field_name = ''%s'' ';
       {$endif}

       {$IFDEF MSSQL}
       Q_EXISTE = 'select count(*) as CUANTOS FROM INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = ''%s'' and COLUMN_NAME = ''%s'' ';
       {$ENDIF}
  begin
       Result := False;
       with oZetaProvider do
       begin
          //EmpresaActiva := Empresa;
          FDataSet:= CreateQuery( Format(Q_EXISTE,[Tabla,Campo]) );
          try
             with FDataset do
             begin
                  Active := True;
                  if( FieldByName('CUANTOS').AsInteger > 0 ) then
                  begin
                       Result := True;
                  end;
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataSet );
          end;
       end;
  end;



function TdmServerSistemaTimbrado.ValidarPatchTimbrado:Boolean;
var
   Tablas,Procs,Campos:TStringList;
   i:Integer;
   Tabla,Campo:string;

  function ValidarTablas:Boolean;
  begin
       Result := False;
       i:= 0;
       While( i < Tablas.Count )  do
       begin
            Campos := TStringList.Create;
            try
               Campos.CommaText := Tablas.Strings[i];

               Tabla := Campos.Strings[0];
               Campo := Campos.Strings[1];

               Result := ExistsTabla(Tabla,Campo );
               if not Result then
                 i := Tablas.Count;
               i:= i + 1;
            Finally
                   Campos.Free;
            end;
       end;
  end;

  function ValidarProcedures:Boolean;
  begin
       Result := False;
       i:= 0;
       While( i < Procs.Count )  do
       begin
            Result := ExistsProcedure(Procs.Strings[i] );
            if not Result then
              i := Procs.Count;
            i:= i + 1;
       end;
  end;

begin
     Tablas := TStringList.Create;
     Procs := TStringList.Create;
     try
		 Result := TRUE;
     finally
            Tablas.Free;
            Procs.Free;
     end;
end;

procedure TdmServerSistemaTimbrado.CrearBitacora(Empresa: OleVariant;Tipo: Integer; Parametros: OleVariant);
var
   Titulo,Descripcion:string;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
          AsignaParamList( Parametros );
          //Asignar Parametros
          with ParamList do
          begin
               Titulo :=  ParamByName('Titulo').AsString;
               Descripcion := ParamByName('Descripcion').AsString;
          end;

          //AbreBitacora; // Se destruye al limpiar el zetaprovider

          If Tipo = 0 then  //Evento
          begin
               EscribeBitacora(tbNormal,clbMovCajaAhorro,0,Now,Titulo,Descripcion);
               //Log.Evento(clbNinguno,0,NullDateTime,Titulo,Descripcion);
          end else
          If Tipo = 1 then  //Advertencia
          begin
               EscribeBitacora(tbAdvertencia ,clbMovCajaAhorro,0,Now,Titulo,Descripcion);
          end else
          If Tipo = 2 then  //Error
          begin
               EscribeBitacora(tbError,clbMovCajaAhorro,0,Now,Titulo,Descripcion);
          end;
          SetComplete;
     end
end;


{$ifndef DOS_CAPAS}
function TdmServerSistemaTimbrado.GetUsuarioArbolApp(iUsuario,
  iExe: Integer): OleVariant;
const
     K_FILTRO = ' ( US_CODIGO = %d ) and ( AB_MODULO = %d )';
begin
     SetTablaInfo( eArbol );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( K_FILTRO, [ iUsuario, iExe ] );
          Result := GetTabla( Comparte );
     end;
     SetComplete;
end;

function TdmServerSistemaTimbrado.GrabaArbolApp(iUsuario, iExe: Integer;
  oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
const
     K_DELETE = 'delete from ARBOL where ( US_CODIGO = %d ) and ( AB_MODULO = %d ) ';
begin
     { Complemente el 'EmptyDataSet' }
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          EmpiezaTransaccion;
          try
             ExecSQL( Comparte, Format( K_DELETE, [ iUsuario, iExe ] ) );
             TerminaTransaccion( TRUE );
          except
                on Error: Exception do
                begin
                     TerminaTransaccion( FALSE );
                     raise;
                end;
          end;
          SetTablaInfo( eArbol );
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
     end;
     SetComplete;
end;


initialization
  TComponentFactory.Create(ComServer, TdmServerSistemaTimbrado, Class_dmServerSistemaTimbrado, ciMultiInstance, ZetaServerTools.GetThreadingModel);
{$endif}
end.
