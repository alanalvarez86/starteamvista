unit DCalculoNomina;

interface

{$define CUENTA_EMPLEADOS}
{$define VALIDAEMPLEADOSGLOBAL}
{$define IMSS_UMA}
{.$define FLEXIBLES}
{.$undefine FLEXIBLES}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, DB,
     {$ifdef CUENTA_EMPLEADOS}
     FAutoServer, 
     {$endif}
     ZetaTipoEntidad,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZEvaluador,
     ZetaQRExpr,
     ZetaSQLBroker,
     ZCreator,
     DNominaClass,
     ZetaServerTools,
     ZetaServerDataSet,
     ZFuncsTools,
     DZetaServerProvider;

const
     //K_MAX_CONCEPTO = 999; <-- se movio hacia ZetaCommonClasses
     K_FACTOR_MENSUAL = 30.4;

     Q_RETRO_CONCEPTO = 0;
     Q_RETRO_TIENE_NOMINA = 1;
     Q_RETRO_SALARIO = 2;
     Q_RETRO_TIPO_CONCEPTO = 3;
     K_SALARIO_DUMMY = 'KSUELDOALIASDUMMY';
     K_SALINT_DUMMY = 'KSUELDOINTEGRADOALIASDUMMY';
     {$IFDEF CAROLINA}
     K_MAX_VUELTAS = 10;
     {$ELSE}
     K_MAX_VUELTAS = 500; //Maximo de iteraciones para calcular la piramidaci�n
     {$ENDIF}
     K_PROP_SUBSIDIO = 'Proporci�n Del Subsidio: %s';
     K_SEPARA_SUBSIDIO = 'Separaci�n de Subsidio al Empleo: %s';


type
     TTiposConcepto = Set of eTipoConcepto;
     TPeriodosIrregulares = Set of eClasifiPeriodo;
const
     TiposPercepcion: TTiposConcepto = [ coPercepcion, coPrestacion, coCalculo, coCaptura ];
     TiposDeduccion: TTiposConcepto = [ coDeduccion, coObligacion ];
     PeriodosIrregulares: TPeriodosIrregulares = [ tpQuincenal, tpMensual, tpDecenal ];

type
     eStatusConcepto = (stcDormido, stcPendiente, stcCalculado );
     eTipoAjuste = (etaEntregaCredito, etaAjustaSiempre, etaSuaviza, etaTopaSUBE );
     eAhorroPresta = ( eAhorro, ePrestamo );
     TTiposAJuste = Set of eTipoAjuste;
     TConceptoNomina = class;
     TCalculoNeto = record
       // Estos son de Entrada
          Bruto : TPesos;
          Neto : TPesos;
          TipoPeriodo : eTipoPeriodo;
          Subsidio : TPesos;
          TablaPrestaciones : TCodigo;
          Antiguedad : Integer;
          Fecha : TDate;
          Zona : TCodigo;
          ListaOtrasP : String;
          Redondeo : TPesos;
       // Estos son de Salida
          SalMin : TPesos;
          SMGDF : TPesos;
          {$ifdef IMSS_UMA}
          ValUMA : Tpesos;
          {$endif}
          Factor : TPesos;
          TopeEyM : TPesos;
          ISPT : TPesos;
          IMSS : TPesos;
          GravaISPT : TPesos;
          GravaIMSS : TPesos;
          MontoOtrasP : TPesos;
          Diario : TPesos;
          Integrado : TPesos;
          Clasifi: eClasifiPeriodo;
     end;
     TipoTotalNomina = ( ttnPercepciones,
                         ttnDeducciones,
                         ttnNeto,
                         ttnMensuales,
                         ttnExentas,
                         ttnPerCal,
                         ttnImpCal,
                         ttnExentoCal,
                         ttnExentoMensual,
                         ttnPrestaciones,
                         ttnGravadoPrev,
                         ttnGravadoISN,
                         ttnExentoPS,
                         ttnSalarioPS );
     EErrorCalculo = class(EErrorEvaluador)
     public
       { Public declarations }
       Titulo: String;
       constructor Create( const sTitulo, sMsg: String );
     end;

     TConceptoCalculado = class( TObject )
     public
       { Public declarations }
        Concepto : TConceptoNomina;
        Activo : Boolean;
        ConISPT : Boolean;
        ISPT : TPesos;
        UsuarioMod : Integer;
        Referencia : String;
        Exento : TPesos;
        Monto : TPesos;
        ImpuestosOK : Boolean;
        Siguiente : TConceptoCalculado;
        SumaTotales : Boolean;
        constructor Create;
     end;
     TConceptoNomina = class( TObject )
     private
        FMonto : TPesos;
        FStatus : eStatusConcepto;
        FEvaluador : TZetaEvaluador;
        FEvaluadorQuery : TZetaEvaluador;
        FEvaluadorISPTInd : TZetaEvaluador;
        FEvaluadorExentoISPT : TZetaEvaluador;
        Global : Boolean;
        MontoGlobal : TPesos;
        EsAhorroPrestamo : Boolean;
        FDataModule : TObject;
        FColFormula, FColISPT, FColExento, FColQuery  : Integer;

        procedure CalculaImpuestos( const oMovimien : TConceptoCalculado );
        procedure AplicaMovimiento( const oMovimien : TConceptoCalculado );
        procedure Calcular;
        procedure ResetRastreo;
        procedure RegistrarRastreo(const lExcepcion: Boolean = FALSE);
        function CalcularPiramidacion: TPesos; //Metodo exclusivo para cuando tiene piramidaci�n el calculo de la nomina Sug#789
        procedure CreaMovimiento( const nMonto : TPesos; sReferencia : String ) ;
        procedure Registrar( const nMonto : TPesos; sReferencia : String; Obliga : Boolean );
        function  EvaluaQuery : Boolean;
        function  TopeMonto( const nMonto : TPesos ) : TPesos;
        procedure MarcarCalculado( nMonto : TPesos );
     public
       { Public declarations }
        Numero : Integer;
        Tipo : eTipoConcepto;
        CalcularSiempre : Boolean;
        Formula : String;
        GravaISPT : Boolean;
        ExentoISPT : String;
        Condicion : String;
        Mensual : Boolean;
        ISPTIndividual : String;
        Movimiento : TConceptoCalculado;
        PrevisionSocial : ePrevisionSocialClasifi;
        Tiempo : Double;
        Descripcion: String;
        GravaISN : Boolean;

        function  EsDeduccion : Boolean;
        procedure PreparaEvaluadores;
        procedure Reset;
        function  EvaluaFormula( oEvaluador : TZetaEvaluador; var lFormulaOK : Boolean ) : TPesos;
        destructor Destroy;override;
        property  Evaluador : TZetaEvaluador read FEvaluador write FEvaluador;
        property  EvaluadorQuery : TZetaEvaluador read FEvaluadorQuery write FEvaluadorQuery;
        property  EvaluadorExentoISPT : TZetaEvaluador read FEvaluadorExentoISPT write FEvaluadorExentoISPT;
        property  EvaluadorISPTInd : TZetaEvaluador read FEvaluadorISPTInd write FEvaluadorISPTInd;
        property  DataModule : TObject read FDataModule write FDataModule;
        property  Monto : TPesos read FMonto;
        property  Status : eStatusConcepto read FStatus;
     end;
     TTipoAhorro = class( TObject )
     public
       { Public declarations }
        Codigo : TCodigo;
        Descripcion : String[30];
        Concepto : TConceptoNomina;
        Relativo : TConceptoNomina;
        Liquidacion : eTipoAhorro;

     end;
     TRetroactivo = class( TObject )
     private
           FFecha : TDate;
           FRetroConceptos: TStrings;
           FConcepto, FNominaAPagar, FYearAPagar, FNominaDesde, FNominaHasta: integer;
           FNombre : string;
           FEmpleado: integer;
           FSalarioNuevo : TPesos;
           FSalIntNuevo : TPesos;
           FSalarioNomina: TPesos;
           FDataset : TServerDataSet;
           FQConcepto : TZetaCursor;
           FQTieneNomina: TZetaCursor;
           FQTipoConcepto: TZetaCursor;
           FQSalario: TZetaCursor;
           FRastreo : TRastreador;
           FExcepcion : eOperacionMontos;
           oZetaProvider: TdmZetaServerProvider;
           oZetaCreator: TZetaCreator;

           FNominaActiva: integer;
           FYear: integer;
           FSQLOriginal: string;
           FYearAd: integer;

           function GetScript( const iScript : integer ): string;
           function FormateaFloat(const rValor: TPesos): string;
           procedure SetNominaActiva(const Value: integer);
           procedure CreaDataset;
           procedure CreateQuerys;
           procedure PreparaQuerys;
           function  TransformaCampo( const sOriginal, sCampo: string; const rValor: TPesos ): string;
     public

           property SQLOriginal : string read FSQLOriginal write FSQLOriginal;
           property NominaActiva: integer read FNominaActiva write SetNominaActiva;
           property YearAd: integer read FYearAd write FYearAd;
           constructor Create( oCreator: TZetaCreator );
           destructor Destroy;override;

           function SQLTransformado: string;
           function TransformaFormula( const sFormula : string ): string;
           function TieneNomina(const iEmpleado: integer ):Boolean;
           procedure CalculaRetroactivo( oListaCalculados : TStringList );
           procedure GrabaExcepciones;
           procedure SetDatosEmpleado( oDataSet : TDataSet );
     end;

     TBitacoraDescuentos = class( TObject )
       EsAhorro :Boolean;
       Tipo : string;
       Referencia : string;
       Empleado : integer;

       TopeLimite : TPesos;
       SumaAhorrosPrestamos : TPesos;
       DescuentoAplicado : TPesos;
       Monto : TPesos;
       Neto: TPEsos;
     end;

     TCalcNomina = class( TObject )
     public
            function GetConcepto(const NumConcepto: Integer): TConceptoNomina;
     private
       { Private declarations }
       FBroker : TSQLBroker;
       FCreator : TZetaCreator;
       FNomina : TNomina;
       FEvaluadorGral : TZetaEvaluador;
       FUsuario : Integer;
       FEmpleado : Integer;
       FParametros : TZetaParams;
       FCalculandoNomina : Boolean;
       dPagoRecibo : TDate;
       FDatosPeriodo : TDatosPeriodo;
       FYear, FTipo, FNumero : Integer;
       FDataSetLista : TDataset;
       FQGeneral : TZetaCursor;
       FListaParametros: string;
       FListaFormulas: string;
       FQGetExcepciones, FQBorraCalculados : TZetaCursor;
       FQAhorrosPrestamos : TZetaCursor;
       FQEmpleado, FQInsertMovimien, FQUpdateNomina : TZetaCursor;
       aConceptos : Array[ 0..K_MAX_CONCEPTO ] of SmallInt;	// Para ahorrar memoria
       oListaConceptos : TList;
       oListaCalculados : TStringList;
       oListaAhorros, oListaPrestamos : TStringList;
       FMaxConcepto : Integer;
       oConceptoCredito, oConceptoISPT, oConceptoDiferen, oConceptoAjuste, oConceptoPuentePiramida : TConceptoNomina;
       //***************************************************
       // Variables para Topar los descuentos - Sug 328:V2007
       oConceptoTopeDescuentos: TConceptoNomina;
       FAcumuladoDescuentos: TPesos;
       FTopeLimite: TPesos;
       FExcedioDesctos: Boolean;
       FBitacoraDescuentos: TList;
       //***************************************************
       nConceptoAjuste : Integer;
       lSaldoRedondeo : Boolean;
       lPendiente : Boolean;
       nMonedaRedondeo : TPesos;
       lSeparaCredito , lFinalFeliz : Boolean;
       // Totales de NOMINA por Empleado
       nNomina_NO_PERCEPC, nNomina_NO_DEDUCCI, nNomina_NO_IMP_CAL, nNomina_NO_PER_CAL : TPesos;
       nNomina_NO_X_CAL, nNomina_NO_PER_MEN, nNomina_NO_X_MENS, nNomina_NO_X_ISPT, nNomina_NO_TOT_PRE, nNomina_NO_PREV_GR,nNomina_NO_PER_ISN : TPesos;
       nNomina_NO_X_PS, nNomina_NO_SAL_PS : TPesos;
       nNomina_NO_VER_CON : Integer;
       FConceptosPreparados : Boolean;
       nCreditoMensual : TPesos; {Monto Cr�dito al Salario APLICADO del mes}
       nCreditoPeriodo : TPesos; {Monto Cr�dito al Salario APLICADO del periodo de nomina}
       nSubeMensual : TPesos; {Monto SUBE APLICADO del mes}
       {Para el calculo de Retroactivos}
       FRetroactivo : TRetroactivo;
       FNominaRetroactiva: Boolean;
       {$ifdef CUENTA_EMPLEADOS}
       FAutoServer: TAutoServer;
       {$else}
       FMaxEmpleados : Integer;
       {$endif}

       lFinPiramida: Boolean;
       lPiramidaOk: Boolean;
       rTolerancia: TTasa;
       rMontoPiramida: TPesos;
       nVueltas: integer;
       PiramidaPrestamos : ePiramidacionPrestamos;
       FTotalRastroNomina: String;

       function AbreQueryEmpleado( Query: TZetaCursor ): Boolean;
       function BuscaReferencia(oConcepto: TConceptoNomina; const Referencia: String): Integer;
       function CalculaNominaDataset: OleVariant;
       function EvaluaFormula(const sFormula, sTitulo: string): TPesos;
       function HayCampo(const sNombre: String): Boolean;
       function GetCampo(const sNombre: String): TZetaField;

       function GetScript(const iScript: Integer): String;
       function GetScriptPeriodo(const iScript: Integer): String;
       function Neto: TPesos;
       function PreparaNominaDataSet: OleVariant;
       function oZetaProvider: TdmZetaServerProvider;
       function GetTiemposCalculo: String;
       function GetPeriodoData: string;
       procedure AgregaFiltroPeriodo;
       procedure GuardaExcepciones;
       procedure CalculaNominaCreaLista( const sRango, sFiltro, sCondicion : String; const TipoCalculo : eTipoCalculo; bRastreo: Boolean = False );
       procedure CalculaUnEmpleado(const iEmpleado: Integer; const iVersionConceptoActual : Integer = 0);
       procedure CalculoBegin;
       procedure ClearBroker;
       procedure ClearNomina;
       // procedure GetDatosPeriodo;
       procedure GetParametrosCalculo(Parametros: Variant);
       procedure InitBroker;
       procedure InitNomina;
       procedure PreparaConceptos;
       procedure PreparaNominaCreaLista;
       procedure SincronizaEmpleado( const iEmpleado : Integer );
       procedure SumaTotalesMovimiento(const oConcepto: TConceptoNomina; oMovimien: TConceptoCalculado; Factor: Integer);
       procedure SetRastroNomina( const oConcepto: TConceptoNomina; sTextoRastreo: String; const lExcepcion: Boolean );
       { Rastreo de Funciones }
       function Rastrear: Boolean;
       procedure RastreoHeader( const sTitulo: String );
       procedure RastreoPesos( const sTexto: String; rMonto: TPesos );
       procedure RastreoEspacio;
       procedure RastreoTexto( const sTexto, sValor: String );
       procedure RastreoTextoLibre( const sTexto : String );
       procedure CalculaNominaParametros;
       procedure RastreaConceptoParametros;
       procedure ActualizaLogPeriodo(eStatus: eStatusPeriodo);

{$ifdef VALIDAEMPLEADOSGLOBAL}
       procedure ValidaStatusNomina(var sMensajeAdvertencia : string );
{$else}
       procedure ValidaStatusNomina;
{$endif}
       procedure ValidaNominaAnterior;
       procedure PreparaNomina(const TipoCalculo: ETipoCalculo);
       procedure RastreoNoVacio( const sTexto : String );

       {Calculo de Retroactivos}
       procedure RetroactivoBegin;
       procedure SetRetroactivo(const Value: TRetroactivo);
       procedure SetNominaRetroactiva(const Value: Boolean);
       function  PreparaFormula( const sOriginal: string ): string;

       property NominaRetroactiva : Boolean read FNominaRetroactiva write SetNominaRetroactiva default FALSE;

       property Nomina: TNomina read FNomina;
       property SQLBroker: TSQLBroker read FBroker;
     public
       { Public declarations }
       FConceptoActual : integer;
       nMontoFormula: TPesos;
       nExentoFormula: TPesos;
       nEmpleadoSalMin: TPesos;
       nEmpleadoUMA: TPesos;
       nEmpleadoUMA_Anual: TPesos;
       nRegistraIspt: TPesos;
       nRegistraCredito: TPesos;
       ReferenciaActiva: String;

       constructor Create( oCreator: TZetaCreator );
       destructor Destroy; override;
       function ConceptoCalculado( const NumConcepto : Integer ) : TPesos;
       {*** US 12365: Timbrado/Declaraciones : Mejorar calculo de exentos en n�mina ***}
       function ConceptoCalculadoEx( const NumConcepto : Integer ) : TPesos;
       function CalExento(const NumConcepto: Integer; const TopeExento: TPesos): TPesos;
       function AcumuladoConceptoExento( const NumConcepto: Integer ): TPesos;
       {*** FIN ***}
       function TotNomina( const eTotal : TipoTotalNomina ) : TPesos;
       { ISPT / Credito }
       function CalculaNomina(Empresa, Parametros: OleVariant ): OleVariant;
       function RastreaConcepto(Empresa, Parametros: OleVariant; out Rastreo : OleVariant ) : OleVariant;
       function RastreaConceptoDataSet( const iConcepto : Integer; out Rastreo : OleVariant ) : OleVariant;
       function CalculaNetoBruto(Empresa: OleVariant; var Parametros: OleVariant): OleVariant;
       procedure RegistraISPT( const nMonto : TPesos );
       procedure RegistraCredito( const nMonto : TPesos );
       procedure RegistraCreditoPeriodo( const nMonto : TPesos );
       procedure RegistraCreditoMensual( const nMonto : TPesos );
       procedure RegistraSubeMensual( const nMonto : TPesos );
       function  CreditoAplicado( const lCreditoPeriodo: Boolean ): TPesos;
       function  SubeAplicado: TPesos;
       procedure InitParametros(Empresa, Parametros: OleVariant);

       function CalculoRetroactivoBuildDataset : OleVariant;
       function CalculoRetroactivoDataset( DataSet : TDataSet ): OleVariant;
       property Retroactivo : TRetroactivo read FRetroactivo write SetRetroactivo;
       {$ifdef CUENTA_EMPLEADOS}
       property AutoServer: TAutoServer read FAutoServer write FAutoServer;
       {$else}
       property LimiteEmpleados : Integer read FMaxEmpleados write FMaxEmpleados;
       {$endif}
       function ConceptoTipo( const NumConcepto : Integer ) : eTipoConcepto;
       //US 17266: Almacenar la clave SAT cuando se timbra la n�mina para obtener reportes y auditar timbrado
       function ConsultarVersionConceptoActual: Integer;
       function ObtenerStatusDelPeriodo( Year, Tipo, Numero: Integer): eStatusPeriodo;
       
     end;
     TCalcNetoBruto = class(TObject)
     private
       { Private declarations }
       FCreator: TZetaCreator;
       FDatos: TCalculoNeto;
       { C�lculo de Salario Neto/Bruto }
       function DeBrutoANeto: TPesos;
       function DeNetoABruto: TPesos;
       function PruebaCalculoNeto: TPesos;
       function IMSSObrero(const nDiasEym, nDiasIvcm, nSalBase, nSalMin,
                  nSalario, nImssAjusta: TPesos; const dLey: TDate;
                  const nSMGDF: TPesos): TPesos;
       function CalcImp(const nValor, nFactorSubsidio, nFactor, nSalMin: TPesos; nT1, nT2, nT3: Integer; const dFecha: TDate ): TPesos;

       procedure CalcDiarioBrutoNeto;
       procedure CalcParamBrutoNeto;
     public
       { Public declarations }


       constructor Create( oDatos: TCalculoNeto; oCreator: TZetaCreator );
       destructor Destroy; override;
       property Datos: TCalculoNeto read FDatos;
     end;

implementation

uses ZetaCommonTools,
     ZGlobalTress,
     DPrimasIMSS,
     DTablasISPT;

const K_QRY_PERIODO                = 1;
      K_QRY_CONCEPTOS              = 2;
      K_SP_ACUM_ANUAL              = 3;
      K_QRY_MOVIMIEN               = 4;
      K_QRY_BORRA_CALC             = 5;
      K_QRY_AHORROS                = 6;
      K_QRY_PRESTAMOS              = 7;
      K_QRY_INSERT_MOV             = 8;
      K_QRY_UPDATE_NOM             = 9;
      K_QRY_T_AHORRO               = 10;
      K_QRY_T_PRESTA               = 11;
      K_QRY_GLOBALES               = 12;
      K_QRY_MOVIMIEN_TODOS         = 13;
      K_QRY_STATUS_ANTERIOR        = 14;
      K_QRY_GET_EMPLEADOS          = 15;
      K_QRY_UPDATE_LOG_PERIODO     = 16;
      {$IFDEF MSSQL}
      K_QRY_AHO_PRE                = 17;
      {$endif}
      K_QRY_ACUMULADO_CONCEPTO_EXENTO = 18;
      //US 17266: Almacenar la clave SAT cuando se timbra la n�mina para obtener reportes y auditar timbrado
      K_QRY_KAR_CONCEPTO_VERSION_ACTUAL = 19;

      K_ORDEN_AHORROPRESTA         = ' ORDER BY 1 DESC, 2, 3, 4 ';
      K_SEPARADOR_RASTREO = '=======================================================================';

{******************** FUNCIONES DE UTILERIA ******************}

function NuevosPesos( const Monto, Moneda : TPesos ) : TPesos;
begin
     Result := Redondea( MiRound( Monto / Moneda, 0 ) * Moneda );
end;

procedure ValidaError( EvResultado: TQREvResult; const sMensaje: String );
var
   ErrorNumero: Integer;
   ErrorDescrip, ErrorMsg, ErrorExp: String;
begin
     if HuboErrorEvaluador( EvResultado, ErrorNumero, ErrorDescrip, ErrorMsg, ErrorExp ) then
     begin
          raise EErrorCalculo.Create( sMensaje,
                                      sMensaje + CR_LF +
                                      'Error N�mero: ' + IntToStr( ErrorNumero ) + CR_LF +
                                      ErrorDescrip + CR_LF +
                                      ErrorMsg + CR_LF +
                                      'En la Expresi�n: ' + ErrorExp );
     end;
end;

procedure AgregaSQLError( var Error: Exception; const sMensaje : string );
begin
     Error.Message := Error.Message + CR_LF +
                      'SQL:' + CR_LF +
                      sMensaje;

end;

{ ***** EErrorConcepto ******* }

constructor EErrorCalculo.Create(const sTitulo, sMsg: String );
begin
     inherited Create( sMsg );
     Titulo := sTitulo;
end;

{ ************ TCalcNomina ************ }

constructor TCalcNomina.Create(oCreator : TZetaCreator);
begin
     FCreator := oCreator;
     FParametros := TZetaParams.Create;
     FQGeneral := oZetaProvider.CreateQuery;
     Self.FConceptoActual := 0;
end;

destructor TCalcNomina.Destroy;
var
   i: Integer;
begin
     FreeAndNil(FQGetExcepciones);
     FreeAndNil(FQBorraCalculados);
     FreeAndNil(FQAhorrosPrestamos);
     FreeAndNil(FQEmpleado);
     FreeAndNil(FQInsertMovimien);
     FreeAndNil(FQUpdateNomina);
     FreeAndNil(FQGeneral);
     ClearBroker;
     FEvaluadorGral.Free;
     if ( oListaConceptos <> nil ) then
     begin
          with oListaConceptos do
          begin
               for i := ( Count - 1 ) downto 0 do
                   TConceptoNomina( Items[ i ] ).Free;
               Free;
          end;
     end;
     if ( oListaCalculados <> nil ) then
     begin
          with oListaCalculados do
          begin
               for i := ( Count - 1 ) downto 0 do
                   TConceptoCalculado( Objects[ i ] ).Free;
               Free;
          end;
     end;
     if ( oListaAhorros <> nil ) then
     begin
          with oListaAhorros do
          begin
               for i := ( Count - 1 ) downto 0 do
                   TTipoAhorro( Objects[ i ] ).Free;
               Free;
          end;
     end;
     if ( oListaPrestamos <> nil ) then
     begin
          with oListaPrestamos do
          begin
               for i := ( Count - 1 ) downto 0 do
                   TTipoAhorro( Objects[ i ] ).Free;
               Free;
          end;
     end;

     if ( FBitacoraDescuentos <> nil ) then
     begin
          with FBitacoraDescuentos do
          begin
               for i := ( Count - 1 ) downto 0 do
                   TBitacoraDescuentos( Items[ i ] ).Free;
               Free;
          end;
     end;
     inherited;
end;

function TCalcNomina.oZetaProvider: TdmZetaServerProvider;
begin
     Result := FCreator.oZetaProvider;
end;

procedure TCalcNomina.InitNomina;
begin
     FNomina := TNomina.Create(FCreator);
end;

procedure TCalcNomina.ClearNomina;
begin
     FreeAndNil(FNomina);
end;

procedure TCalcNomina.InitBroker;
begin
     if FBroker = NIL then
        FBroker := TSQLBroker.Create( FCreator );
end;

procedure TCalcNomina.ClearBroker;
begin
     FreeAndNil( FBroker );
end;

procedure TCalcNomina.AgregaFiltroPeriodo;
begin
     with SQLBroker.Agente do
     begin
          AgregaFiltro( Format( 'NOMINA.PE_YEAR = %d', [FYear] ), TRUE, Entidad );
          AgregaFiltro( Format( 'NOMINA.PE_TIPO = %d', [FTipo] ), TRUE, Entidad );
          AgregaFiltro( Format( 'NOMINA.PE_NUMERO = %d', [FNumero] ), TRUE, Entidad );
     end;
end;

procedure TCalcNomina.CalculaNominaCreaLista( const sRango, sFiltro, sCondicion : String; const TipoCalculo : eTipoCalculo; bRastreo: Boolean = False );
begin
     with SQLBroker do
     begin
          Init(enNomina);
          with Agente do
          begin
               // El orden de las columnas importa en CalculaUnEmpleado
               AgregaColumna( 'NOMINA.CB_CODIGO', TRUE, enNomina, tgNumero, 0, '' );
               AgregaColumna( 'COLABORA.CB_CHECA', TRUE, enEmpleado, tgTexto, 1, '' );
               AgregaColumna( 'NOMINA.NO_FUERA', TRUE, enNomina, tgTexto, 1, '' );


               AgregaFiltroPeriodo;
               with FParametros do
               begin
                    AgregaFiltro( sRango, TRUE, enNomina );
                    AgregaFiltro( sFiltro, FALSE, enNomina );
                    AgregaFiltro( sCondicion, TRUE, enEmpleado );
                    { Calcular Pendientes: Filtra a Status sea <= CalculadaParcial }
                    if ( TipoCalculo = tcPendiente ) then
                        AgregaFiltro( Format( 'NO_STATUS <= %d', [ Ord( spCalculadaParcial ) ] ), TRUE, enNomina );

                    //Afectada Parcial
                    if not ( bRastreo ) then
                    begin
                         if ( TipoCalculo in [ tcTodos, tcRango ] ) then
                            AgregaFiltro( Format( 'NO_STATUS <= %d', [ Ord( spCalculadaTotal ) ] ), TRUE, enNomina );
                    end;
               end;
               FiltroConfidencial( oZetaProvider.EmpresaActiva );
               AgregaOrden( 'NOMINA.CB_CODIGO', TRUE, enNomina );
          end;
          if SuperSQL.Construye( Agente ) then
             SuperReporte.Construye( Agente )
          else
               DataBaseError( Agente.ListaErrores[ 0 ] );
     end;
end;


function TCalcNomina.CalculaNominaDataset: OleVariant;
var
    iEmpleado :Integer;
    //iNumEmpleados : Integer;
    eStatus : eStatusPeriodo;
    iVersionConceptoActual: Integer;

{$ifdef VALIDAEMPLEADOSGLOBAL}
    sMensajeAdvertencia : string;
{$endif}
begin
     FDataSetLista := SQLBroker.SuperReporte.DataSetReporte;



{$ifdef VALIDAEMPLEADOSGLOBAL}
     ValidaStatusNomina( sMensajeAdvertencia );
{$else}
	 ValidaStatusNomina;
{$endif}
     //US 17266: Almacenar la clave SAT cuando se timbra la n�mina para obtener reportes y auditar timbrado - Consultar Version
     iVersionConceptoActual := 0;
     with oZetaProvider do
     begin
          with FDataSetLista do
          begin
               if OpenProcess( prNOCalcular, RecordCount, FListaParametros ) then
               begin
                    iEmpleado := Fields[ 0 ].AsInteger;
                    try
                      CalculoBegin;
                      //US 17266: Almacenar la clave SAT cuando se timbra la n�mina para obtener reportes y auditar timbrado - Consultar Version
                      iVersionConceptoActual := ConsultarVersionConceptoActual;
                      while ( not Eof ) and CanContinue( iEmpleado ) and ( not Log.HayErrores ) do
                      begin
                           EmpiezaTransaccion;
                           try
                              CalculaUnEmpleado( iEmpleado, iVersionConceptoActual );
                           except
                                 on Error: Exception do
                                 begin
                                      Log.Excepcion( iEmpleado, 'Error al Calcular N�mina', Error );
                                 end;
                           end;
                           TerminaTransaccion( True );   //Siempre se hace Commit

                           Next;
                           iEmpleado := Fields[ 0 ].AsInteger;
                      end;
                    except
                          on Error: EErrorCalculo do
                          begin
                               with Error as EErrorCalculo do
                               begin
                                    Log.Error( iEmpleado, Titulo, Message );
                               end;
                          end;
                          on Error: EAbort do;  { Excepci�n Silenciosa }
                          on Error: Exception do
                          begin
                               Log.Excepcion( iEmpleado, 'Error al Calcular N�mina', Error );
                          end;
                    end;
               end;
          end;
          InitNomina;
          try
             eStatus := Nomina.CalculaStatusPeriodo;
             ActualizaLogPeriodo( eStatus );
          finally
                 ClearNomina;
          end;
          // Mostrar Tiempos
          with FParametros do
               if ( FindParam( 'MuestraTiempos' ) <> nil ) and ParamByName( 'MuestraTiempos' ).AsBoolean then
                  Log.Evento( clbNinguno, 0, Date, 'Tiempos de C�lculo', GetTiemposCalculo );


{$ifdef VALIDAEMPLEADOSGLOBAL}
          if strLLeno( sMensajeAdvertencia ) then
             Log.Advertencia(clbNinguno, 0, Date, 'Licencia de Empleados', sMensajeAdvertencia);
{$endif}

          Result := CloseProcess;
     end;
end;

procedure TCalcNomina.ActualizaLogPeriodo( eStatus: eStatusPeriodo );
 var
    sSQL, sLog : string;
begin
     {if ( eStatus = spCalculadaTotal ) then
     begin}
          with oZetaProvider do
          begin
               sLog := Vacio;
               // Cambio por reforma fiscal del 2008. Si es igual o mayor a la constante
               // K_REFORMA_FISCAL_2008 se omite 'Proporci�n Del Subsidio'
               // y se cambia 'Separaci�n de Cr�dito al Salario' por 'Separaci�n de Subsidio al Empleo'.
               if DatosPeriodo.Year < ZetaCommonClasses.K_REFORMA_FISCAL_2008 then
                  sLog := Format( 'Proporci�n Del Subsidio: %s' + CR_LF +
                               'Redondeo Neto: %s' + CR_LF +
                               'Separaci�n de Cr�dito al Salario: %s',
                               [ FormatFloat( '0.0000', GetGlobalReal( K_GLOBAL_PORCEN_SUBSIDIO ) ),
                                 FormatFloat( '0.00', GetGlobalReal( K_GLOBAL_REDONDEO_NETO ) ),
                                 BoolToSiNo( GetGlobalBooleano( K_GLOBAL_SEPARAR_CRED_SAL ) )] )
               else
                  sLog := Format( 'Redondeo Neto: %s' + CR_LF +
                               K_SEPARA_SUBSIDIO,
                               [ FormatFloat( '0.00', GetGlobalReal( K_GLOBAL_REDONDEO_NETO ) ),
                                 BoolToSiNo( GetGlobalBooleano( K_GLOBAL_SEPARAR_CRED_SAL ) )] );

          end;

     {end
     else
     begin
          sLog := VACIO;
     end;}

     with FDatosPeriodo do
          sSQL := Format( GetScript( K_QRY_UPDATE_LOG_PERIODO ), [ EntreComillas( sLog ), Year, Ord(Tipo), Numero ]);

     oZetaProvider.EjecutaAndFree( sSQL );

end;

function TCalcNomina.GetTiemposCalculo: String;
const
     K_SHOW_TIEMPO = '   %s   | %s' + CR_LF;
var
   i : Integer;
   nTiempoTotal : TTime;
   oConcepto : TConceptoNomina;
   oListaTiempos : TList;
   nTope : Integer;

   function OrdenaTiempos(Item1, Item2: Pointer): Integer;
   var
      t1, t2 : TTime;
   begin
        t1 := TConceptoNomina( Item1 ).Tiempo;
        t2 := TConceptoNomina( Item2 ).Tiempo;
        if ( t1 > t2 ) then
           Result := -1
        else if ( t1 < t2 ) then
           Result := 1
        else
           Result := 0;
   end;

begin
     Result := VACIO;
     nTiempoTotal := 0;
     oListaTiempos := TList.Create;
     try
        if Assigned( oListaConceptos ) then
        begin
             oListaTiempos.Capacity := oListaConceptos.Capacity;
             with oListaConceptos do
                  for i := 0 to Count-1 do
                  begin
                       oConcepto := Items[ i ];
                       nTiempoTotal := nTiempoTotal + oConcepto.Tiempo;
                       oListaTiempos.Add( oConcepto );
                  end;
             if ( nTiempoTotal > 0 ) then
                with oListaTiempos do
                begin
                     Sort( @OrdenaTiempos );
                     Result := 'Tiempo Total : ' + FormatFloat( '##0.00', nTiempoTotal * 100000 ) + CR_LF + CR_LF;
                     Result := Result + 'Concepto | Porcentaje' + CR_LF;
                     if ( Count < 10 ) then
                        nTope := Count-1
                     else
                        nTope := 9;
                     for i := 0 to nTope do
                     begin
                          oConcepto := Items[i];
                          Result := Result + Format( K_SHOW_TIEMPO, [ FormatFloat( '000', oConcepto.Numero ),
                                    FormatFloat( '#0.00 %', oConcepto.Tiempo / nTiempoTotal * 100 ) ] );
                     end;
                end;
        end
        else
            Result := 'NO existe Lista de Conceptos Calculados !';
     finally
        FreeAndNil( oListaTiempos );
     end;
end;

procedure TCalcNomina.SincronizaEmpleado( const iEmpleado : Integer );
const
     K_MAYOR_EDAD = 18;
var
   sSQLTransformado: string;
   dFechaNac: TDate;
begin
    FEmpleado := iEmpleado;

    {Calculo de Retroactivo}
    if NominaRetroactiva then
    begin
         try
            sSQLTransformado := FRetroactivo.SQLTransformado;
            oZetaProvider.PreparaQuery( FQEmpleado, sSQLTransformado );
            {$ifdef CAROLINA}
            oZetaProvider.Log.Evento( clbNinguno, 0, Date, 'SQL MODIFICADO Usado para Calcular el Retroactivo', sSQLTransformado );
            {$ENDIF}
         except
               oZetaProvider.Log.Error( iEmpleado, 'SQL MODIFICADO Usado para Calcular el Retroactivo', sSQLTransformado );
               raise;
         end;

         {$ifdef CAROLINA}
                 {$ifdef INTERBASE}
                 FQEmpleado.SQL.SaveToFile('d:\temp\Retroactivo.SQL');
                 {$endif}
         {$endif}
    end;

    if not AbreQueryEmpleado( FQEmpleado ) then
       DataBaseError( Format( 'Problemas Con Los Par�metros De N�mina, Empleado #%d', [ iEmpleado ] ));
     // Variables propias del empleado. Se almacenan por eficiencia
     // Pendiente: Preguntar si se desea promediar el salario

     nEmpleadoSalMin    := FCreator.dmSalMin.GetSalMin( FDatosPeriodo.Fin, GetCampo( 'ZCBZONA_GE' ).AsString );
     nEmpleadoUMA       := FCreator.dmValUma.GetValUma( FDatosPeriodo.Fin, 0);
     nEmpleadoUMA_Anual := FCreator.dmValUma.GetValUma( FDatosPeriodo.Fin, K_T_ANUAL);

     // Verifica si el empleado cumple 18 a�os y se notifica que debe revisarse la tabla de prestaciones
     dFechaNac := GetCampo( 'ZCBFEC_NAC' ).AsDateTime;
     if ( dFechaNac <> NullDateTime ) and                                                          // Tiene capturada fecha de Nac.
        ( ZetaCommonTools.Aniversario( dFechaNac, FDatosPeriodo.Inicio, FDatosPeriodo.Fin ) ) and  // Su aniversario es en esta N�mina
        ( Trunc( DaysToYears( Trunc( FDatosPeriodo.Fin - dFechaNac ) + 1 ) ) = K_MAYOR_EDAD ) then // Tiene 18 a�os al final del periodo calculado
     begin
          oZetaProvider.Log.Advertencia( iEmpleado, 'El empleado cumple 18 a�os en esta n�mina',
                                         'El empleado cumple 18 a�os en �sta n�mina, por lo tanto sus prestaciones cambian.' + CR_LF +
                                         'Verifique que se realicen los cambios correspondientes a su tabla de prestaciones.' + CR_LF +
                                         'Sugerimos' + CR_LF +
                                         '1. Hacer el cambio de prestaciones de ley en su Kardex' + CR_LF +
                                         '2. Con este cambio se recalcula el salario integrado' + CR_LF +
                                         '3. Reportar al IMSS el cambio de salario integrado' );
     end;
end;

function TCalcNomina.AbreQueryEmpleado( Query : TZetaCursor ) : Boolean;
begin
    with Query do
    begin
         Active := FALSE;

         oZetaProvider.ParamAsInteger( Query, 'Empleado', FEmpleado );
         try
            Active := TRUE;
         except
               on Error: Exception do
               begin
                    AgregaSQLError( Error, SQLBroker.Agente.SQL.Text );
                    raise;
               end;
         end;
         Result := not Eof;
    end;
end;

procedure TCalcNomina.PreparaConceptos;
var
i : Integer;
begin
    // Pone montos en 0 y Status de Concepto como "Pendiente" para aquellos que se van a calcular
        // y "Dormido" para aquellos que NO se van a calcular
    with oListaConceptos do
          for i := Count-1 downto 0 do
            TConceptoNomina( Items[ i ] ).Reset;

    oListaCalculados.Clear;
    ReferenciaActiva := VACIO;
end;

procedure TCalcNomina.GuardaExcepciones;
var
    oMovimien : TConceptoCalculado;
    oConcepto : TConceptoNomina;
    nConcepto : Integer;
begin
    with FQGetExcepciones do
    begin
         AbreQueryEmpleado( FQGetExcepciones );
         while not Eof do
         begin
              nConcepto := FieldByName( 'CO_NUMERO' ).AsInteger;
              oConcepto := GetConcepto( nConcepto );
              if ( oConcepto = NIL ) then
                 DatabaseError( Format( 'Concepto Inactivo # %d', [ nConcepto ] ));
              with ( oConcepto ) do
              begin
                   oMovimien := TConceptoCalculado.Create;
                   with oMovimien do
                   begin
                        Concepto      := oConcepto;
                        Activo	  := zStrToBool( FieldByName( 'MO_ACTIVO' ).AsString );
                        ConISPT	  := zStrToBool( FieldByName( 'MO_PER_CAL' ).AsString );
                        ISPT 	  := FieldByName( 'MO_IMP_CAL' ).AsFloat;
                        UsuarioMod	  := FieldByName( 'US_CODIGO' ).AsInteger;
                        Referencia	  := FieldByName( 'MO_REFEREN' ).AsString;
                        Exento 	  := FieldByName( 'MO_X_ISPT' ).AsFloat;
                        if Concepto.EsDeduccion then
                           Monto := FieldByName( 'MO_DEDUCCI' ).AsFloat
                        else
                            Monto := FieldByName( 'MO_PERCEPC' ).AsFloat;
                   end;
                   ResetRastreo;
                   AplicaMovimiento( oMovimien );
                   RegistrarRastreo(TRUE);
              end;
              Next;
         end;
      // Si hubo una excepci�n de Cr�dito al Salario, entonces ya no calcula ISPT
      if ( oConceptoCredito <> NIL ) AND ( oConceptoISPT <> NIL ) AND
             ( oConceptoCredito.Monto <> 0 ) then
          oConceptoISPT.Registrar( 0, '', FALSE );
     end;
end;

procedure TCalcNomina.CalculaUnEmpleado( const iEmpleado : Integer; const iVersionConceptoActual : Integer = 0 );
var
   StatusSimulacion : eStatusSimFiniquitos;

    procedure InitTotales;
    begin
       nNomina_NO_PERCEPC  := 0;    // TOT_PER
       nNomina_NO_DEDUCCI  := 0;    // TOT_DED
       nNomina_NO_X_ISPT   := 0;    // TOT_EXENTO
       nNomina_NO_X_MENS   := 0;    // TOT_EX_MEN
       nNomina_NO_X_CAL    := 0;    // INDI_EXE
       nNomina_NO_PER_MEN  := 0;    // TOT_MENS
       nNomina_NO_IMP_CAL  := 0;    // INDI_IMP
       nNomina_NO_PER_CAL  := 0;    // INDI_PER
       nNomina_NO_TOT_PRE  := 0;    // TOT_PRESTA
       nNomina_NO_PREV_GR  := 0;    // TOT_PREVGR
       nNomina_NO_X_PS     := 0;    // EXENTO PREVISION SOCIAL
       nNomina_NO_SAL_PS   := 0;    // TOT SALARIOS PREV SOCIAL
       FAcumuladoDescuentos := 0;
       FExcedioDesctos := FALSE;
       nNomina_NO_PER_ISN  := 0;    // NO_PER_ISN

       RegistraISPT( 0 );
       RegistraCredito( 0 );
       RegistraCreditoPeriodo( 0 );
       RegistraCreditoMensual( 0 );
       FTotalRastroNomina:= '';
    end;

    procedure PreparaEvaluador;
    var i : integer;
    begin
         if NOT FConceptosPreparados then
         begin
              with oListaConceptos do
                 for i := 0 to Count-1 do
                     TConceptoNomina( Items[i] ).PreparaEvaluadores;
              FConceptosPreparados := TRUE;
         end;
    end;

   procedure CalculaConceptos( const TiposCalculables : TTiposConcepto );
   var
      i : Integer;
      nPendientes, nOldPendientes : Cardinal;
      nConceptos : Cardinal;
      oConcepto : TConceptoNomina;
      sPendientes : String;
      t0 : TTime;
   begin
        nConceptos  := oListaConceptos.Count;	// Cuantos hay
        nPendientes := nConceptos;		// Todos pendientes

        while ( nPendientes > 0 ) do
        begin
             nOldPendientes := nPendientes;
             nPendientes := 0;
             for i := 0 to nConceptos-1 do
             begin
                  oConcepto := TConceptoNomina( oListaConceptos[i] );
                  Self.FConceptoActual := oConcepto.Numero;
                  with oConcepto do
                          if ( Status = stcPendiente ) AND ( Tipo in TiposCalculables ) then
                          begin
                              t0 := Time;
                              ResetRastreo;
                              Calcular;
                              RegistrarRastreo;
                              nPendientes := nPendientes + 1;
                              Tiempo := Tiempo + ( Time - t0 );
                          end;
             end;

             if ( nPendientes = nOldPendientes ) then // No hubo avance, estamos estancados
             begin
                         sPendientes := '';
                         for i := 0 to nConceptos - 1 do
                         begin
                            oConcepto := TConceptoNomina( oListaConceptos[i] );
                            if ( oConcepto.Status = stcPendiente ) AND ( oConcepto.Tipo in TiposCalculables ) then
                               sPendientes := sPendientes + IntToStr( oConcepto.Numero ) + ';';
                         end;
                         DataBaseError( 'Error en F�rmulas: Referencia Circular: ' + sPendientes );
             end;
        end;
   end;

   function GetTipoAhorro( oLista : TStringList; Codigo : TCodigo ) : TTipoAhorro;
   var
	     nIndex : Integer;
   begin
	       nIndex := oLista.IndexOf( Codigo );
	       if nIndex >= 0 then
           Result := TTipoAhorro( oLista.Objects[ nIndex ] )
        else
        begin
            Result := NIL; // Evita Warning
	           DataBaseError( Format(  'Tipo Ahorro No existe: %s', [ Codigo ] ));	// Esto no debe suceder;
        end;
   end;

  procedure RegistraAcumuladoDescuentos( const rMonto:TPesos );
  begin
       FAcumuladoDescuentos := FAcumuladoDescuentos + rMonto;
  end;

  function GetTopeLimiteDesctos( nMontoDescto, nSaldo: TPesos;
                                 const sTipo, sReferencia: string;
                                 const lEsAhorro: Boolean ): TPesos;

   var oBitacora: TBitacoraDescuentos;
  begin
       oBitacora := TBitacoraDescuentos.Create;
       oBitacora.EsAhorro := lEsAhorro;
       oBitacora.Tipo := sTipo;
       oBitacora.Referencia := sReferencia;
       oBitacora.Empleado := iEmpleado;
       oBitacora.Neto := Neto;

       if (oConceptoTopeDescuentos <> NIL) then
       begin
            //Si el Concepto trae creado el Movimiento, quiere decir que es una excepcion de Monto
            //por lo que ya no necesito calcularlo.
            with oConceptoTopeDescuentos do
            begin
                 if ( ( Movimiento = NIL ) or  ( Movimiento.UsuarioMod = 0 )) AND
                    (Status <> stcCalculado) then
                 begin
                      Calcular;
                      FTopeLimite := rMax( rMin( Monto, Neto), 0 );
                 end;
                 //Si por alguna razon el concepto se excede del Neto, el limite es el Neto.
                 if ( FAcumuladoDescuentos > FTopeLimite ) then
                 begin
                      {
                      Esta operacion me da el remanente de lo que puedo descontar.
                      Ejemplo:
                      Tope Limite: 520 pesos
                      Primer Descto: 500 pesos
                      Segundo Descto: 50 pesos

                      Si ya voy en el 2do Descto:

                      nMontoDescto= 50
                      FAcumuadoDescuentos=550

                      entonces:

                      Result := 50 - (550-520) =  20
                      }
                      FExcedioDesctos := TRUE;
                      Result := rMax( nMontoDescto - ( FAcumuladoDescuentos - Monto ), 0);
                      Result := rMin( Result, FTopeLimite );
                 end
                 else
                     Result := nMontoDescto;

                 if ( NOT lEsAhorro ) and ( Result > nSaldo ) then
                 begin
                      Result := nSaldo;
                      RegistraAcumuladoDescuentos( nSaldo - nMontoDescto );
                      nMontoDescto := Result;
                 end
            end;
       end
       else
       begin
            FExcedioDesctos := ( nMontoDescto > rMax( Neto, 0 ) );
            Result := rMin( nMontoDescto, rMax( Neto, 0 ));
            if ( NOT lEsAhorro ) and ( Result > nSaldo ) then
            begin
                 Result := nSaldo;
                 RegistraAcumuladoDescuentos( nSaldo - nMontoDescto );
                 nMontoDescto := Result;
            end
       end;


       oBitacora.TopeLimite := FTopeLimite;
       oBitacora.SumaAhorrosPrestamos := FAcumuladoDescuentos;
       oBitacora.DescuentoAplicado := Result;
       oBitacora.Monto := nMontoDescto;

       FBitacoraDescuentos.Add( oBitacora ) ;
  end;

  procedure EscribeBitacoraDescuentos;
   procedure ConcatData( var sData: string; const sTexto: string );
   begin
        sData := ConcatString( sData, sTexto, CR_LF );
   end;

   const EsAhorroPresta: array [False..True] of String = ('P','A');

   var i: integer;
       sData, sMsj: string;
       oBitacora: TBitacoraDescuentos;
       rTopeLimite: TPesos;
  begin
       if FExcedioDesctos and ( FBitacoraDescuentos <> NIL ) and ( FBitacoraDescuentos.Count >0 )then
       begin
            sData := VACIO;

            with FBitacoraDescuentos do
            begin
                 oBitacora := TBitacoraDescuentos( Items[ Count - 1 ] );
                 rTopeLimite:= TBitacoraDescuentos( Items[ 0 ] ).TopeLimite;
            end;

            if oConceptoTopeDescuentos = NIL then
               sMsj := 'NETO'
            else
            begin
                 ConcatData( sData,Padl( 'Total Neto del Empleado : ', 40 ) + FormatFloat( '#,0.00', TBitacoraDescuentos( FBitacoraDescuentos.Items[ 0 ] ).Neto ) );
                 sMsj := Format( 'C(%d)' , [oConceptoTopeDescuentos.Numero] );
            end;


            ConcatData( sData, Padl( Format( 'M�ximo a descontar topado al %s : ', [sMsj] ), 40 ) + FormatFloat( '#,0.00', rTopeLimite ) + CR_LF );
            ConcatData( sData, 'Tipo  C�digo  Referencia  Calculado  Descontado  Pendiente' );

            with FBitacoraDescuentos do
            begin

                 for i :=0 to ( Count - 1 ) do
                     with TBitacoraDescuentos( Items[ i ] ) do
                     begin
                                             //Tipo  C�digo  Referencia  Calculado  Descontado  Pendiente
                                             //12345678901234567890123456
                                             //                          _________  __________  _________

                                             //1123451234561212345678123412345678912123456789123123456789
                          ConcatData( sData, EsAhorroPresta[EsAhorro] + Replicate(' ',5)+
                                             PadL(Tipo,6) + Replicate(' ',2)+
                                             PadL(Referencia,8) + Replicate(' ',4)+
                                             PadL(FormatFloat('#,0.00', Monto),9) + Replicate(' ',2)+
                                             PadL(FormatFloat('#,0.00', DescuentoAplicado),9) + Replicate(' ',2)+
                                             PadL(FormatFloat('#,0.00', Monto - DescuentoAplicado),9) );
                     end;

                ConcatData( sData, Replicate(' ',26) + '_________  __________  __________' );

                with oBitacora do
                     ConcatData( sData, Replicate(' ',14)+
                                 'Totales:' + Replicate(' ',4)+
                                 PadL(FormatFloat('#,0.00',SumaAhorrosPrestamos ),9) + Replicate(' ',2)+
                                 PadL(FormatFloat('#,0.00',rTopeLimite),9) + Replicate(' ',2)+
                                 PadL(FormatFloat('#,0.00',SumaAhorrosPrestamos - rTopeLimite), 9 ) );

                oZetaProvider.Log.Advertencia( oBitacora.Empleado,
                                               'Ahorros/Pr�stamos no se descontaron completos',
                                               sData );
            end;
       end;
  end;


   procedure DescuentaAhorros( const sTipo: string );
   var
      sFormula, sTitulo : String;
      nMonto : TPesos;
      lFormulaOK : Boolean;
   begin
        with FQAhorrosPrestamos do
        begin
             with GetTipoAhorro( oListaAhorros, sTipo ) do
             begin
               if ( Concepto.Status <> stcCalculado  ) then
               begin
                    if Concepto.Global then
                       nMonto := Concepto.MontoGlobal
                    else
                    begin
                         sFormula := PreparaFormula( FieldByName( 'PR_FORMULA' ).AsString );
                         //sFormula := BorraCReturn( FieldByName( 'AH_FORMULA' ).AsString );

                         if not StrVacio( sFormula ) then
                         begin
                             sTitulo := 'Error en F�rmula de Ahorro Tipo ' + sTipo;
                             if NominaRetroactiva then
                                sFormula := FRetroactivo.TransformaFormula( sFormula );

                             nMonto := EvaluaFormula( sFormula, sTitulo );
                         end
                         else if Concepto.Evaluador <> NIL then
                              nMonto := Concepto.EvaluaFormula( Concepto.Evaluador, lFormulaOK )
                         else
                             nMonto := 0;

                    end;

                    //nMonto := rMin( nMonto, rMax( Neto, 0 ));	// Que no exceda al Neto
                    RegistraAcumuladoDescuentos( nMonto );
                    nMonto := GetTopeLimiteDesctos(nMonto, 0, sTipo, '', TRUE) ; // Que no exceda al Neto o el Tope L�mite
                    Concepto.Registrar( nMonto, '', TRUE );
               end;
               if ( Relativo <> NIL ) AND ( Relativo.Status <> stcCalculado ) then
                       Relativo.Calcular;
             end;
        end;
   end;

   procedure DescuentaPrestamos( const sTipo: string );
   var
      sFormula, sTitulo : String;
      nMonto : TPesos;
      //sReferencia : String;
      lFormulaOK : Boolean;
   begin
	       with FQAhorrosPrestamos do
	       begin
             with GetTipoAhorro( oListaPrestamos, sTipo ) do
             begin
                  // Si no hay una excepci�n de monto capturada para esa referencia
                  ReferenciaActiva := FieldByName( 'PR_REFEREN' ).AsString;
                  if  ( BuscaReferencia( Concepto, ReferenciaActiva ) < 0 ) then
                  begin
                       if Concepto.Global then
                          nMonto := Concepto.MontoGlobal
                       else
                       begin
                           nMontoFormula := FieldByName( 'PR_MONTO' ).AsFloat;
                           sFormula := PreparaFormula( FieldByName( 'PR_FORMULA' ).AsString );

                           if not StrVacio( sFormula ) then
                           begin
                               sTitulo := 'Error en F�rmula Pr�stamo Tipo ' + sTipo + '-' + ReferenciaActiva;
                               if NominaRetroactiva then
                                  sFormula := FRetroactivo.TransformaFormula( sFormula );
                               nMonto := EvaluaFormula( sFormula, sTitulo );
                           end
                           else if Concepto.Evaluador <> NIL then
                                nMonto := Concepto.EvaluaFormula( Concepto.Evaluador, lFormulaOK )
                           else
                               nMonto := 0;
                       end;
                       // Topar al Saldo
                       if ( nMonto <> 0 ) then
                       begin
                           if ( nMonto < 0 ) AND ( FieldByName( 'PR_MONTO' ).AsFloat < 0 ) then		// Pr�stamo negativo
                                 nMonto := rMax( nMonto, FieldByName( 'PR_SALDO' ).AsFloat )
                           else
                           begin
                                // Pr�stamo normal
                                RegistraAcumuladoDescuentos( nMonto );
                                nMonto := GetTopeLimiteDesctos(nMonto, FieldByName( 'PR_SALDO' ).AsFloat, sTipo, ReferenciaActiva, FALSE) ;		// Que no exceda al Neto ni el tope limite
                                //nMonto := rMin( nMonto, FieldByName( 'PR_SALDO' ).AsFloat );		// Que no exceda el Saldo

                           end;
                       end;
                       Concepto.Registrar( nMonto, ReferenciaActiva, TRUE );
                  end;
                  if ( Relativo <> NIL ) AND ( Relativo.Status <> stcCalculado ) then
                     Relativo.Calcular;
             end;
        end;
   end;

   procedure PreparaParamsAhorroPresta;
   begin
        with oZetaProvider do
        begin
             if FDatosPeriodo.DescuentaAhorros then
                ParamAsInteger( FQAhorrosPrestamos, 'EmpleadoAhorro', FEmpleado );

             if FDatosPeriodo.DescuentaPrestamos then
                ParamAsInteger( FQAhorrosPrestamos, 'EmpleadoPrestamo', FEmpleado );
        end;
   end;

   procedure DescuentaAhorrosPrestamos;
   begin
        {Si es NIL, quiere decir que no se estan descontando NI prestamos ni ahorros}
        if ( FQAhorrosPrestamos <> NIL ) then
	       begin
             with FQAhorrosPrestamos do
             begin
                  Active := FALSE;

                  with oZetaProvider do
                  begin
                       with FDatosPeriodo do
                       begin
                            {$ifdef INTERBASE}
                            PreparaParamsAhorroPresta;
                            {$endif}

                            {$ifdef MSSQL}
                            if ( DescuentaAhorros AND DescuentaPrestamos ) then
                            begin
                                 ParamAsInteger( FQAhorrosPrestamos, 'Empleado', FEmpleado )
                            end
                            else
                            begin
                                 PreparaParamsAhorroPresta;
                            end;
                            {$endif}
                       end;
                  end;

                  try
                     Active := TRUE;
                     FTopeLimite := Neto; //De esta forma se sabe cual es el Neto antes de cualquier descuento
                     FBitacoraDescuentos.Clear; //Se limpia la bitacora. En el caso de la piramidacion se pasa varias veces por aqui
                     while NOT Eof do
                     begin
                          if ( eAhorroPresta( FieldByName( 'TIPO' ).AsInteger ) = eAhorro ) then
                             DescuentaAhorros( FieldByName( 'PR_TIPO' ).AsString )
                          else
                              DescuentaPrestamos( FieldByName( 'PR_TIPO' ).AsString );
                          Next;
                     end;

                  except
                        on Error: Exception do
                        begin
                             AgregaSQLError( Error, SQLBroker.Agente.SQL.Text );
                             raise;
                        end;
                  end;
             end;
        end;
   end;

   procedure NetoNegativo;
   begin
        // En Liquidaciones/Indemnizaciones puede enviar a diferencias incobrables
        if ( Neto < 0 ) AND ( oConceptoDiferen <> NIL ) AND
           ( eLiqNomina( GetCampo( 'ZNOLIQUIDA' ).AsInteger ) in [ lnLiquidacion, lnIndemnizacion ] ) then

        oConceptoDiferen.Registrar( Neto, '', FALSE );

        // Si persiste la condici�n, deja n�mina como calculada parcial.
        if ( Neto < 0 ) and lFinPiramida and ( not lPiramidaOk ) then
        begin
             oZetaProvider.Log.Advertencia( iEmpleado, 'Deducciones mayores que percepciones' ); // + CR_LF + 'No se permiten Netos negativos.' + CR_LF + 'Es necesario quitar algunas deducciones fijas' );
             lFinalFeliz := FALSE;
        end;
   end;

   procedure CalculaRedondeo;
   var
	     nNeto, nRedondeado, nDiferencia, nAcumulado : TPesos;
   begin
	       nNeto := Neto;
	       nRedondeado := NuevosPesos( nNeto, nMonedaRedondeo );
	       if ( nNeto <> nRedondeado ) then
	       begin
              nDiferencia := Redondea( nNeto - nRedondeado );
              if ( lSaldoRedondeo ) then
              begin
                    nAcumulado := Redondea( GetCampo( 'SALDO_RED' ).AsFloat );
                    if ( nAcumulado <> 0 ) then
                            if ( nDiferencia + nAcumulado ) >= nMonedaRedondeo then
                                    nDiferencia := nDiferencia - nMonedaRedondeo
                            else if ( nDiferencia + nAcumulado ) <= -nMonedaRedondeo then
                                    nDiferencia := nDiferencia + nMonedaRedondeo;

                    oConceptoAjuste.Registrar(  nDiferencia, '', FALSE );
              end
              else if ( oConceptoISPT <> NIL ) then	// Enviar diferencia a ISPT
              begin
                    with oConceptoISPT do
                    begin
                         if Monto <> 0 then
                         begin
                             if ( Monto + nDiferencia ) < 0 then // ISPT Negativo
                                 nDiferencia := nDiferencia + nMonedaRedondeo;
                         end
                         else if ( nDiferencia < 0 ) then
                                    nDiferencia := nDiferencia + nMonedaRedondeo;

                         Registrar( nDiferencia, 'REDONDEO', TRUE );
                    end;
              end;
        end;
   end;

   procedure CreditoAlSalario;
   var
      oMovimienCredito : TConceptoCalculado;

      procedure EnviaTodoCredito;
      var
         oMovimien : TConceptoCalculado;
      begin
            oMovimien := oConceptoISPT.Movimiento;
            oConceptoISPT.Movimiento := NIL;           // Quita de ISPT
            oConceptoCredito.Movimiento := oMovimien;  // Liga a Cr�dito

            // A todos los conceptos calculados de ISPT les cambia a concepto de credito
            // Puede haber varios cuando tuvo REDONDEO a ISPT
            while oMovimien <> NIL do
            begin
                 oMovimien.Concepto := oConceptoCredito;
                 oMovimien := oMovimien.Siguiente;
            end;
      end;


   begin
        // CreditoAlSalario
        if ( oConceptoISPT = NIL ) then
{$ifdef ANTES}
           DataBaseError( 'No se ha definido un Concepto de ISPT' )
{$else}
           DataBaseError( 'No se ha definido un Concepto de ISR' );
{$endif}

        if ( oConceptoCredito = NIL ) then
        begin
             if ( FDatosPeriodo.Year < ZetaCommonClasses.K_REFORMA_FISCAL_2008 ) then
                DataBaseError( 'No se ha definido un Concepto de Cr�dito al Salario' )
             else
                 DataBaseError( 'No se ha definido un Concepto de Subsidio al Empleo' );
        end;

        if ( lSeparaCredito ) then
        begin
             if ( nRegistraCredito <> 0 ) AND
                ( oConceptoCredito.Status = stcDormido ) AND
                ( oConceptoISPT.Monto <> 0 ) then
             begin
                  // Si s�lo hubo Cr�dito al Salario, pasa todo al 53
                  if ( oConceptoISPT.Monto = nRegistraCredito ) then
                     EnviaTodoCredito
                  else // Deja en 51 el ISPT, pasa Cr�dito al salario al 53
                  begin
                       oConceptoISPT.FMonto := nRegistraISPT;
                       oConceptoISPT.Movimiento.Monto := nRegistraISPT;

                       oMovimienCredito := TConceptoCalculado.Create;
                       with oMovimienCredito do
                       begin
                            Concepto        := oConceptoCredito;
                            Monto           := nRegistraCredito;
                            SumaTotales     := FALSE;
                            oConceptoCredito.AplicaMovimiento( oMovimienCredito );
                       end;
                  end;
             end
        end
        // Si hay ISPT Calculado negativo, se env�a a Cr�dito al Salario
        else if ( oConceptoISPT.Monto < 0 ) AND ( oConceptoCredito.Status = stcDormido ) then
             EnviaTodoCredito;
   end;

   procedure BorraCalculados;
   begin
        with FQBorraCalculados, oZetaProvider do
        begin
             ParamAsInteger( FQBorraCalculados, 'Empleado', iEmpleado );
             Ejecuta( FQBorraCalculados );
        end;
   end;

   procedure GrabaMovimientos;
   var
	     i : Integer;
      begin
           // Se asignan los Par�metros al Query de INSERT into MOVIMIEN
           // y luego se ejecuta el Query
           // Usamos ParamVariant en lugar de ParamByName para mayor velocidad
           for i := 0 to oListaCalculados.Count - 1 do
            with TConceptoCalculado( oListaCalculados.Objects[i] ), oZetaProvider, TZetaComando( FQInsertMovimien ) do
            begin
                 try
                    {$ifdef INTERBASE}
                    Params[ 0 ].AsInteger := iEmpleado;
                    Params[ 1 ].AsInteger := Concepto.Numero;
                    Params[ 2 ].AsString  := Referencia;
                    Params[ 3 ].AsString  := zBoolToStr( Activo );
                    Params[ 4 ].AsInteger := UsuarioMod;
                    if Concepto.EsDeduccion then
                    begin
                        Params[ 5 ].AsFloat := 0.0;
                        Params[ 6 ].AsFloat := Monto;
                    end
                    else
                    begin
                        Params[ 5 ].AsFloat := Monto;
                        Params[ 6 ].AsFloat := 0.0;
                    end;
                    Params[ 7 ].AsFloat  := ISPT;
                    Params[ 8 ].AsString := zBoolToStr( ConISPT );
                    Params[ 9 ].AsFloat  := Exento;
                 {$endif}
                 {$ifdef MSSQL}
                    ParamVariant( FQInsertMovimien, 0, iEmpleado );
                    ParamVariant( FQInsertMovimien, 1, Concepto.Numero );
                    ParamAsVarChar( FQInsertMovimien, 'Refe', Referencia, 8 );
                    // ParamVariant( FQInsertMovimien, 2, Referencia );
                    ParamAsBoolean( FQInsertMovimien, 'Act', Activo );
                    // ParamVariant( FQInsertMovimien, 3, zBoolToStr( Activo ));
                    ParamVariant( FQInsertMovimien, 4, UsuarioMod );
                    if Concepto.EsDeduccion then
                    begin
                        // Es necesario el 0.0 para que ParamVariant lo identifique como Float
                        ParamVariant( FQInsertMovimien, 5, 0.0 );
                        ParamVariant( FQInsertMovimien, 6, Monto );
                    end
                    else
                    begin
                        ParamVariant( FQInsertMovimien, 5, Monto );
                        ParamVariant( FQInsertMovimien, 6, 0.0 );
                    end;
                    ParamVariant( FQInsertMovimien, 7, ISPT );
                    ParamAsBoolean( FQInsertMovimien, 'Calc', ConIspt );
                    // ParamVariant( FQInsertMovimien, 8, zBoolToStr( ConIspt ));
                    ParamVariant( FQInsertMovimien, 9, Exento );
                 {$endif}

                    Ejecuta( FQInsertMovimien );
                 except
                       On Error : Exception do
                       begin
                            Error.Message := 'Error al Actualizar Movimientos de N�mina' +CR_LF +
                                             'En el Concepto #' +
                                             IntToStr(Concepto.Numero) + CR_LF +
                                             Error.Message;
                            raise;
                       end;
                 end;

            end;
   end;

   procedure UpdateNomina( const Status: eStatusPeriodo; const lHayUserPago, lHayFechaPago: Boolean ;const StatusSimula: eStatusSimFiniquitos );
   var
      nUserPag : Integer;
      dPago : TDate;
   begin
     { GA ( 08/Ene/2003 ): Fu� necesario proteger las llamadas a GetCampo }
     { ya que cuando el query de n�mina ( FQEmpleado ) tiene problemas, }
     { los TFields no son inicializados correctamente y GetCampo produce }
     { una excepci�n. Esto causaba que una de las llamadas a UpdateNomina }
     { (ver el try..except principal de TCalcNomina.CalculaUnEmpleado ) }
     { fallara y se comiera la exepci�n que causaba que el query FQEmpleado }
     { no pudiera ser abierto correctamente }

     {$ifdef COMO_ESTABA_ANTES}
     nUserPag := GetCampo( 'ZNOUSR_PAG' ).AsInteger;
     {$else}
     if lHayUserPago then
        nUserPag := GetCampo( 'ZNOUSR_PAG' ).AsInteger
     else
         nUserPag := 0;
     {$endif}
     if ( nUserPag = 0 ) then
     begin
          nUserPag := FUsuario;
          dPago    := dPagoRecibo;
     end
     else
     begin
          {$ifdef COMO_ESTABA_ANTES}
          dPago := GetCampo( 'ZNOFEC_PAG' ).AsDateTime;
          {$else}
          if lHayFechaPago then
             dPago := GetCampo( 'ZNOFEC_PAG' ).AsDateTime
          else
              dPago := dPagoRecibo;
          {$endif}
     end;

     // Se hace por posici�n para mayor velocidad
     with oZetaProvider, TZetaComando( FQUpdateNomina ) do
     begin
{$ifdef INTERBASE}
        Params[ 0 ].AsInteger := iEmpleado;
        Params[ 1 ].AsFloat   := nNomina_NO_PERCEPC;
        Params[ 2 ].AsFloat   := nNomina_NO_DEDUCCI;
        Params[ 3 ].AsFloat   := Neto;
        Params[ 4 ].AsFloat   := nNomina_NO_IMP_CAL;
        Params[ 5 ].AsFloat   := nNomina_NO_PER_CAL;
        Params[ 6 ].AsFloat   := nNomina_NO_X_CAL;
        Params[ 7 ].AsFloat   := nNomina_NO_PER_MEN;
        Params[ 8 ].AsFloat   := nNomina_NO_X_MENS;
        Params[ 9 ].AsFloat   := nNomina_NO_X_ISPT;
        Params[ 10].AsFloat   := nNomina_NO_TOT_PRE;
        Params[ 11].AsInteger := Ord( Status );
        Params[ 12].AsInteger := nUserPag;
        Params[ 13].AsDateTime:= dPago;
        Params[ 14].AsFloat   := nNomina_NO_PREV_GR;
        Params[ 15].AsInteger := Ord( StatusSimula );
        Params[ 16].AsString  := FTotalRastroNomina;
        Params[ 17].AsFloat   := nNomina_NO_PER_ISN;
{$endif}
{$ifdef MSSQL}
        if ( Length(FTotalRastroNomina) = 0 ) then
           FTotalRastroNomina := ' ';
        ParamVariant( FQUpdateNomina, 0, nNomina_NO_PERCEPC );
        ParamVariant( FQUpdateNomina, 1, nNomina_NO_DEDUCCI );
        ParamVariant( FQUpdateNomina, 2, Neto );
        ParamVariant( FQUpdateNomina, 3, nNomina_NO_IMP_CAL );
        ParamVariant( FQUpdateNomina, 4, nNomina_NO_PER_CAL );
        ParamVariant( FQUpdateNomina, 5, nNomina_NO_X_CAL );
        ParamVariant( FQUpdateNomina, 6, nNomina_NO_PER_MEN );
        ParamVariant( FQUpdateNomina, 7, nNomina_NO_X_MENS );
        ParamVariant( FQUpdateNomina, 8, nNomina_NO_X_ISPT );
        ParamVariant( FQUpdateNomina, 9, nNomina_NO_TOT_PRE );
        ParamVariant( FQUpdateNomina, 10, Ord( Status ));
        ParamVariant( FQUpdateNomina, 11, nUserPag );
        ParamAsDate(  FQUpdateNomina, 'NO_FEC_PAG', dPago );
        ParamVariant( FQUpdateNomina, 13, nNomina_NO_PREV_GR );
        ParamVariant( FQUpdateNomina, 18, iEmpleado );
        ParamVariant( FQUpdateNomina, 14, Ord( StatusSimula ));
        ParamAsBlob( FQUpdateNomina, 'NO_RASTREO', FTotalRastroNomina );
        ParamVariant( FQUpdateNomina, 16, nNomina_NO_PER_ISN );
        //US 17266: Almacenar la clave SAT cuando se timbra la n�mina para obtener reportes y auditar timbrado
        ParamVariant( FQUpdateNomina, 17, nNomina_NO_VER_CON );
      //ParamVariant( FQUpdateNomina, 'NO_X_PS', nNomina_NO_X_PS );
{$endif}
        Ejecuta( FQUpdateNomina );
     end;
   end;

  procedure MarcaPiramide;
  begin
       if (oConceptoPuentePiramida <> NIL) then
       begin
            //Asigna un nuevo Monto al Concepto
            oConceptoPuentePiramida.Registrar( rMontoPiramida, VACIO, TRUE );
       end;
  end;

  procedure CalculaPiramidacion;
   var
      rBrinco: TTasa;
      rCalculado : TPesos;
  begin
       if (oConceptoPuentePiramida <> NIL) then
       begin
            //Si el Concepto trae creado el Movimiento, quiere decir que es una excepcion de Monto
            //por lo que ya no necesito calcularlo.
            if ( oConceptoPuentePiramida.Movimiento = NIL ) or
               ( oConceptoPuentePiramida.Movimiento.UsuarioMod = 0 ) then
            begin
                 rCalculado := oConceptoPuentePiramida.CalcularPiramidacion;

                 // Si ya estamos por debajo de la tolerancia, termina
                 lFinPiramida := ( abs( rCalculado ) <= rTolerancia ) or
                                 ( nVueltas >= K_MAX_VUELTAS );

                 if lFinPiramida then
                 begin
                      {oConceptoPuentePiramida.Reset;
                      oConceptoPuentePiramida.Registrar( rMontoPiramida, VACIO, TRUE );}
                      {$ifdef CAROLINA}
                      oZetaProvider.Log.Evento( clbNinguno, iEmpleado, Date, 'N�mero de iteraciones',  IntToStr( nVueltas ) );
                      {$endif}
                      if (nVueltas >= K_MAX_VUELTAS) then
                      begin
                           oZetaProvider.Log.Error( iEmpleado, 'Error al calcular Concepto de Piramidaci�n',
                                                               'No es posible obtener el monto del Concepto de Piramidaci�n' + CR_LF+
                                                               'Posible causas: ' +CR_LF +
                                                               '    *la tolerancia es muy peque�a '+CR_LF +
                                                               '    *se excedio el l�mite de iteraciones (50) '+CR_LF +
                                                               '    *la f�rmula del concepto esta mal dise�ada o tiene referencias circulares ');
                           lPiramidaOk := FALSE;
                      end
                      else
                          lPiramidaOk := TRUE;
                 end
                 else
                 begin
                      // Esto supone que el concepto es PERCEPCION
                      // Intenta acercarse al resultado aumentano o disminuyendo la diferencia
                      // Si es la primera vez, nValor es 0

                      if ( rCalculado <= 0 ) then
                         rBrinco := rCalculado * (-1.1)
                      else
                          rBrinco := rCalculado * (-0.9);

                      rMontoPiramida := rMontoPiramida + rBrinco;
                      Inc( nVueltas );

                 end;
            end
            else
            begin
                 lFinPiramida := TRUE;
            end;
       end;
  end;

begin  // .CalculaUnEmpleado
     lFinalFeliz := TRUE;
     //Inicializacion de Variables de Piramidacion.
     rTolerancia := oZetaProvider.GetGlobalReal( K_GLOBAL_PIRAMIDA_TOLERANCIA );
     lFinPiramida := TRUE;
     lPiramidaOk := FALSE;
     nVueltas := 0;
     rMontoPiramida := 0;
     StatusSimulacion := ssfSinAprobar;
     //US 17266: Almacenar la clave SAT cuando se timbra la n�mina para obtener reportes y auditar timbrado
     nNomina_NO_VER_CON := iVersionConceptoActual;
     try

        if ( oZetaProvider.GetGlobalBooleano( K_GLOBAL_SIM_FINIQ_APROBACION ) )then
           StatusSimulacion := ssfSinAprobar
        else
            StatusSimulacion := ssfAprobada;

        SincronizaEmpleado( iEmpleado );
        PreparaEvaluador;
        repeat
              InitTotales;
              PreparaConceptos;
              MarcaPiramide;
              GuardaExcepciones;							// Primero excepciones
              CalculaConceptos( TiposPercepcion ); // Segundo percepciones
              CalculaConceptos( TiposDeduccion ); // Tercero deducciones

              PiramidaPrestamos:= ePiramidacionPrestamos( oZetaProvider.GetGlobalInteger ( K_GLOBAL_PIRAMIDA_PRESTAMOS ));

              If ( PiramidaPrestamos = eAntesPiramidacion )  then
              begin
                 DescuentaAhorrosPrestamos;
              end;
              CalculaPiramidacion;
              If ( PiramidaPrestamos = eDespuesPiramidacion )  then
              begin
                 DescuentaAhorrosPrestamos;
              end;

              NetoNegativo;								// Sexto Neto Negativo
              CalculaRedondeo;							// S�ptimo Redondeo Moneda
              CreditoAlSalario;
              CalculaConceptos( [coResultados] );

        until lFinPiramida;

        EscribeBitacoraDescuentos;

        if NOT NominaRetroactiva then
        begin
            {Si no Es Nomina Retroactiva quiere decir que los cambios quedan grabados en las Tablas NOMINA y MOVIMIEN}
            BorraCalculados;
            GrabaMovimientos;

            if lFinalFeliz then
            begin
                 UpdateNomina( spCalculadaTotal, True, True ,StatusSimulacion )
            end
            else
                // EJ. Referencia Circular
                UpdateNomina( spCalculadaParcial, True, True,StatusSimulacion  );
        end;

     except
           on Error: Exception do
           begin
                if NOT NominaRetroactiva then
                begin
                     { Para que guarde CEROS en totales }
                     InitTotales;
                     UpdateNomina( spSinCalcular, HayCampo( 'ZNOUSR_PAG' ), HayCampo( 'ZNOFEC_PAG' ),StatusSimulacion);
                end;
                raise;
           end;
     end;
end;

procedure TCalcNomina.CalculoBegin;

  procedure InicializaEvaluador;
  begin
       FQEmpleado := oZetaProvider.CreateQuery;
       FCreator.RegistraFunciones( [efComunes,efNomina] );
       oZetaProvider.InicializaValoresNomINFO;
  end;

  procedure FillConceptos;

     procedure InitConceptos;
     var
       i : Integer;
     begin
       for i := 0 to K_MAX_CONCEPTO do
           aConceptos[ i ] := -1;

       oListaCalculados := TStringList.Create;
       oListaConceptos  := TList.Create;
       FMaxConcepto     := 0;
     end;

     procedure CreaConceptos;
     var
        oConcepto : TConceptoNomina;
     begin
       oZetaProvider.AbreQueryScript( FQGeneral, Format( GetScript( K_QRY_CONCEPTOS ), [ Ord( FDatosPeriodo.Uso ) + 1 ] ) );
       with FQGeneral do
         while not Eof do
         begin
              oConcepto := TConceptoNomina.Create;
              with oConcepto do
              begin
                   Numero          := FieldByName( 'CO_NUMERO' ).AsInteger;
                   Tipo		   := eTipoConcepto( FieldByName( 'CO_TIPO' ).AsInteger );
                   CalcularSiempre := zStrToBool( FieldByName( 'CO_CALCULA' ).AsString );
                   Formula	     := StrDef( PreparaFormula( FieldByName( 'CO_FORMULA' ).AsString ) , '0' );
                   if (FDatosPeriodo.Clasifi in PeriodosIrregulares ) then
                      Formula := StrDef( PreparaFormula( FieldByName( 'CO_FRM_ALT' ).AsString ), Formula );
                   ExentoISPT	   := PreparaFormula( FieldByName( 'CO_X_ISPT' ).AsString );
                   Condicion	   := PreparaFormula( FieldByName( 'QU_FILTRO' ).AsString );
                   ISPTIndividual  := PreparaFormula( FieldByName( 'CO_IMP_CAL' ).AsString );
                   PrevisionSocial :=  ePrevisionSocialClasifi( FieldByName( 'CO_PS_TIPO' ).AsInteger );
                   {Antes de Retroactivos
                   Formula	   := BorraCReturn( FieldByName( 'CO_FORMULA' ).AsString );
                   ExentoISPT	   := BorraCReturn( FieldByName( 'CO_X_ISPT' ).AsString );
                   Condicion	   := BorraCReturn( FieldByName( 'QU_FILTRO' ).AsString );
                   ISPTIndividual  := BorraCReturn( FieldByName( 'CO_IMP_CAL' ).AsString );
                   }

                   GravaISPT	   := zStrToBool( FieldByName( 'CO_G_ISPT' ).AsString );
                   Mensual	   := zStrToBool( FieldByName( 'CO_MENSUAL' ).AsString );
                   GravaISN	   := zStrToBool( FieldByName( 'CO_ISN' ).AsString );
                   Global          := FALSE;
                   MontoGlobal     := 0;
                   EsAhorroPrestamo:= FALSE;
                   Tiempo          := 0;
                   DataModule      := self;
                   Descripcion     := FieldByName('CO_DESCRIP').AsString;
                   Reset;
                   // Relaciona el arreglo de #'s de Concepto con la posici�n del Concepto en la Lista
                   aConceptos[ Numero ] := oListaConceptos.Add( oConcepto );
                   // Vienen ordenados por CO_NUMERO, para saber cual es el MAYOR.
                   FMaxConcepto := Numero;

                   if StrLleno( Formula ) then
                   begin
                        Evaluador := TZetaEvaluador.Create( FCreator, self );
                        Evaluador.Entidad := enNomina;
                        FColFormula := SQLBroker.Agente.AgregaColumna( Formula, FALSE, enNomina, tgFloat, 0, 'Concepto #' + IntToStr( Numero ));
                   end;
                   if StrLleno( ExentoISPT ) then
                   begin
                        EvaluadorExentoISPT := TZetaEvaluador.Create( FCreator, self );
                        EvaluadorExentoISPT.Entidad := enNomina;
                        FColExento := SQLBroker.Agente.AgregaColumna( ExentoISPT, FALSE, enNomina, tgFloat, 0, 'Exento de Concepto #' + IntToStr( Numero ));
                   end;
                   if StrLleno( ISPTIndividual ) then
                   begin
                        EvaluadorISPTInd := TZetaEvaluador.Create( FCreator, self );
                        EvaluadorISPTInd.Entidad := enNomina;
                        FColISPT := SQLBroker.Agente.AgregaColumna( ISPTIndividual, FALSE, enNomina, tgFloat, 0, 'ISPT Individual de Concepto #' + IntToStr( Numero ));
                   end;
                   if StrLleno( Condicion ) then
                   begin
                        EvaluadorQuery := TZetaEvaluador.Create( FCreator, self );
                        EvaluadorQuery.Entidad   := enEmpleado;
                        FColQuery := SQLBroker.Agente.AgregaColumna( Condicion, FALSE, enEmpleado, tgBooleano, 0, 'Condici�n de Concepto #' + IntToStr( Numero ));
                   end;
              end;
              Next;
         end;

     end;

     procedure CamposRequeridos;
     var
        sAcumAjuste : String;
     begin
          with SQLBroker.Agente do
          begin
               AgregaColumna( 'NOMINA.NO_LIQUIDA', TRUE, enNomina, tgTexto, 1, 'ZNOLIQUIDA' );
               AgregaColumna( 'COLABORA.CB_ZONA_GE', TRUE, enEmpleado, tgTexto, 1, 'ZCBZONA_GE' );
               AgregaColumna( 'NOMINA.NO_FEC_PAG', TRUE, enNomina, tgFecha, 0, 'ZNOFEC_PAG' );
               AgregaColumna( 'NOMINA.NO_USR_PAG', TRUE, enNomina, tgNumero, 0, 'ZNOUSR_PAG' );
               AgregaColumna( 'PERIODO.PE_FEC_INI', TRUE, enPeriodo, tgFecha, 0 );
               sAcumAjuste := Format( GetScript( K_SP_ACUM_ANUAL ),
                              [ oZetaProvider.GetGlobalInteger( K_GLOBAL_AJUSTE ), FDatosPeriodo.Year ] );
               AgregaColumna( sAcumAjuste, TRUE, enNomina, tgFloat, 0, 'SALDO_RED' );
               AgregaColumna( 'COLABORA.CB_FEC_NAC', TRUE, enEmpleado, tgFecha, 0, 'ZCBFEC_NAC' );
          end;
     end;

  begin  { FillConceptos }
       InitConceptos;
       CreaConceptos;
       CamposRequeridos;
  end;

  procedure SetConceptosGlobales;
  var
     nGlobalSubsidio : TPesos;
     iConceptoPiramida: integer;
  begin
     with oZetaProvider do
     begin
        oConceptoCredito := GetConcepto( GetGlobalInteger( K_GLOBAL_CREDITO ));    // Credito al salario   53
        oConceptoISPT    := GetConcepto( GetGlobalInteger( K_GLOBAL_ISPT ));       // Concepto de ISPT 51
        oConceptoDiferen := GetConcepto( GetGlobalInteger( K_GLOBAL_INCOBRABLES ));//Concepto de Diferencias Incobrables 85

        nConceptoAjuste := GetGlobalInteger( K_GLOBAL_AJUSTE );  //Concepto Ajuste por Diferencia de Moneda 99;
        oConceptoAjuste := GetConcepto( nConceptoAjuste );

        lSaldoRedondeo := ( oConceptoAjuste <> NIL ) AND GetGlobalBooleano( K_GLOBAL_LLEVAR_SALDO_REDONDEO ) ; // 'Llevar Saldo de Redondeo FALSE

        oConceptoTopeDescuentos := GetConcepto( GetGlobalInteger( K_GLOBAL_TOPE_PRESTAMOS ) );
        iConceptoPiramida := GetGlobalInteger( K_GLOBAL_PIRAMIDA_CONCEPTO );
        if ( iConceptoPiramida <> 0 ) then
        begin
             oConceptoPuentePiramida := GetConcepto( iConceptoPiramida );//Concepto para hacer el Calculo con Piramidacion.
             if oConceptoPuentePiramida = NIL then
                DataBaseError( 'El C�lculo de N�mina con Piramidaci�n no se puede realizar.' + CR_LF +
                               Format( 'El concepto %0:d, est� inactivo o fu� borrado.' + CR_LF +
                                       'Soluciones:' + CR_LF +
                                       '1) Activar/agregar el concepto %0:d' + CR_LF +
                                       '   � ' + CR_LF +
                                       '2) Desactivar la Piramidaci�n en Globales de N�mina', [iConceptoPiramida] )  );
        end
        else
        begin
             oConceptoPuentePiramida := NIL ;
        end;

        nMonedaRedondeo := Redondea( GetGlobalReal( K_GLOBAL_REDONDEO_NETO )) ; //Redondeo de Moneda 0.50
        if nMonedaRedondeo = 0 then
           DataBaseError( 'Moneda de Redondeo no Puede ser 0' );
        lSeparaCredito  := GetGlobalBooleano( K_GLOBAL_SEPARAR_CRED_SAL ) ; // Separar Cr�dito al Salario FALSE

        nGlobalSubsidio := GetGlobalReal( K_GLOBAL_PORCEN_SUBSIDIO ); // Factor de Subsidio .7654;
        if nGlobalSubsidio > 1 then
           raise EDataBaseError.Create( 'Subsidio se captura como fracci�n, no puede ser > 1' );
     end;
  end;

  procedure ReportaErrores;
  var
     i : Integer;
  begin
       with SQLBroker.Agente do
         for i := 0 to NumErrores-1 do
              oZetaProvider.Log.Error( 0, 'Error en F�rmula', ListaErrores[ i ]);
  end;

  procedure PreparaAhorrosPrestamos;
   var
      sSQLAhorroPresta: string;
  begin
       {$ifdef INTERBASE}
       if FDatosPeriodo.DescuentaAhorros then
          sSQLAhorroPresta := Format( GetScript( K_QRY_AHORROS ),
                                      [ DateToStrSQL( FDatosPeriodo.Fin ) ] );

       if FDatosPeriodo.DescuentaPrestamos then
          sSQLAhorroPresta := ConcatString( sSQLAhorroPresta,
                                            Format( GetScript( K_QRY_PRESTAMOS ),
                                                    [ DateToStrSQL( FDatosPeriodo.Fin ) ] ),
                                            ' UNION ' ) ;
       {$ENDIF}
       {$IFDEF MSSQL}
       with FDatosPeriodo do
       begin
            if (DescuentaAhorros AND DescuentaPrestamos) then
            begin
                 sSQLAhorroPresta := Format( GetScript( K_QRY_AHO_PRE ),
                                             [ DateToStrSQL( FDatosPeriodo.Fin ) ] );
            end
            else if DescuentaAhorros then
            begin
                 sSQLAhorroPresta := Format( GetScript( K_QRY_AHORROS ),
                                             [ DateToStrSQL( FDatosPeriodo.Fin ) ] );
            end
            else if DescuentaPrestamos then
            begin
                 sSQLAhorroPresta := Format( GetScript( K_QRY_PRESTAMOS ),
                                             [ DateToStrSQL( FDatosPeriodo.Fin ) ] );
            end;
       end;
       {$ENDIF}

       if StrLleno( sSQLAhorroPresta ) then
       begin
            sSQLAhorroPresta := sSQLAhorroPresta + K_ORDEN_AHORROPRESTA;
            FQAhorrosPrestamos := oZetaProvider.CreateQuery( sSQLAhorroPresta );
       end;

  end;

  procedure PreparaQuerys;
   var
      sTextoSQL: string;

  begin
    // Estos querys se van a ejectuar por cada empleado. Vale la pena tener un TQuery preparado por cada uno.
    with oZetaProvider do
    begin
         if Rastrear then
         begin
           FQGetExcepciones := CreateQuery( GetScriptPeriodo( K_QRY_MOVIMIEN_TODOS ));
         end
         else
         begin
           FQGetExcepciones := CreateQuery( GetScriptPeriodo( K_QRY_MOVIMIEN ));
           FQBorraCalculados := CreateQuery( GetScriptPeriodo( K_QRY_BORRA_CALC ));

           PreparaAhorrosPrestamos;


           FQInsertMovimien := CreateQuery( GetScriptPeriodo( K_QRY_INSERT_MOV ));
           FQUpdateNomina := CreateQuery( GetScriptPeriodo( K_QRY_UPDATE_NOM ));
         end;

         with SQLBroker do
         begin
              with Agente do
              begin
                   AgregaFiltroPeriodo;
                   AgregaFiltro( 'NOMINA.CB_CODIGO = :Empleado', TRUE, enNomina );
                   if Rastrear then
                   begin
                        AgregaColumna( 'NOMINA.NO_PERCEPC', TRUE, enNomina, tgFloat, 0, 'NO_PERCEPC' );
                        AgregaColumna( 'NOMINA.NO_DEDUCCI', TRUE, enNomina, tgFloat, 0, 'NO_DEDUCCI' );
                        AgregaColumna( 'NOMINA.NO_X_ISPT', TRUE, enNomina, tgFloat, 0, 'NO_X_ISPT' );
                        AgregaColumna( 'NOMINA.NO_X_MENS', TRUE, enNomina, tgFloat, 0, 'NO_X_MENS' );
                        AgregaColumna( 'NOMINA.NO_X_CAL', TRUE, enNomina, tgFloat, 0, 'NO_X_CAL' );
                        AgregaColumna( 'NOMINA.NO_PER_MEN', TRUE, enNomina, tgFloat, 0, 'NO_PER_MEN' );
                        AgregaColumna( 'NOMINA.NO_IMP_CAL', TRUE, enNomina, tgFloat, 0, 'NO_IMP_CAL' );
                        AgregaColumna( 'NOMINA.NO_PER_CAL', TRUE, enNomina, tgFloat, 0, 'NO_PER_CAL' );
                        AgregaColumna( 'NOMINA.NO_TOT_PRE', TRUE, enNomina, tgFloat, 0, 'NO_TOT_PRE' );
                        AgregaColumna( 'NOMINA.NO_PREV_GR', TRUE, enNomina, tgFloat, 0, 'NO_PREV_GR' );
                        AgregaColumna( 'NOMINA.NO_PER_ISN', TRUE, enNomina, tgFloat, 0, 'NO_PER_ISN' );
                        AgregaColumna( K_PRETTYNAME, TRUE, enEmpleado, tgTexto, 35, 'PRETTYNAME' );
                   end;
              end;
              if not SuperSQL.Construye(  Agente, SELF ) then
              begin
                 ReportaErrores;
                 Abort; { Silencioso: No agrega otro Error }
              end;

              if NominaRetroactiva then
                 Log.Evento( clbNinguno, 0, Date, format ('SQL ORIGINAL para C�lculo de Retroactivo %s', [ IntToStr( FYear ) ] ), Agente.SQL.Text );

              // Crear el Query NOMINA.SQL
              if ParamList.ParamByName('MuestraScript').AsBoolean then
                 Log.Evento( clbNinguno, 0, Date, 'SQL Usado para Calcular la N�mina', Agente.SQL.Text );

              sTextoSQL := Agente.SQL.Text;
              try
                 if NOT NominaRetroactiva then
                    PreparaQuery( FQEmpleado, sTextoSQL );
              except
                    On Error : Exception do
                    begin
                         AgregaSQLError( Error, sTextoSQL );
                         raise;
                    end;
              end;
         end;
    end;
  end;

  procedure PreparaEvalGral;
  begin
       FEvaluadorGral := TZetaEvaluador.Create( FCreator, self );
       with FEvaluadorGral do
       begin
            AgregaDataSet( FQEmpleado );
            AgregaDataSet( FQAhorrosPrestamos );
            Entidad := enNomina;
       end;
  end;

  procedure FillAhorroPresta( var oLista : TStringList; const iScript : Integer );
  var
     nConcepto : Integer;
     sCodigo   : String;
     oConcepto, oRelativo : TConceptoNomina;
     oTipoAhorro : TTipoAhorro;
  begin
       oLista := TStringList.Create;
       oZetaProvider.AbreQueryScript( FQGeneral, GetScript( iScript ));
       with FQGeneral do
         while not Eof do
         begin
	      // Obtiene concepto de N�mina
              sCodigo   := FieldByName( 'TB_CODIGO' ).AsString;
	      nConcepto := FieldByName( 'TB_CONCEPT' ).AsInteger;
	      if ( nConcepto <= 0 ) then
                 DataBaseError( Format( 'Pr�stamo/Ahorro %s : Falta especificar Concepto', [ sCodigo ] ));

              oConcepto := GetConcepto( nConcepto );
              if ( oConcepto = NIL ) then
                 DatabaseError( Format( 'Pr�stamo/Ahorro %s : Concepto # %d no est� activo o no existe', [ sCodigo, nConcepto ] ));

              oConcepto.EsAhorroPrestamo := TRUE;

              // Obtiene Concepto de N�mina Relativo
	      nConcepto := FieldByName( 'TB_RELATIV' ).AsInteger;
              if ( nConcepto > 0 ) then
              begin
                   oRelativo := GetConcepto( nConcepto );
                   if ( oRelativo = NIL ) then
                      DataBaseError( Format( 'Pr�stamo/Ahorro %s : Relativo # %d no est� activo o no existe', [ sCodigo, nConcepto ] ));
              end
              else
                  oRelativo := NIL;

              oTipoAhorro := TTipoAhorro.Create;
              with oTipoAhorro do
              begin
                  Codigo	:= sCodigo;
                  Descripcion	:= FieldByName( 'TB_ELEMENT' ).AsString;
                  Concepto	:= oConcepto;
                  Relativo	:= oRelativo;
                  Liquidacion	:= eTipoAhorro( FieldByName( 'TB_LIQUIDA' ).AsInteger );
              end;
              oLista.AddObject( sCodigo, oTipoAhorro );
              Next;
         end;
       oLista.Capacity := oLista.Count;	// Ya no va a crecer;
  end;

  procedure FillAhorros;
  begin
       if ( FDatosPeriodo.DescuentaAhorros ) then
          FillAhorroPresta( oListaAhorros,  K_QRY_T_AHORRO );
  end;

  procedure FillPrestamos;
  begin
       if ( FDatosPeriodo.DescuentaPrestamos ) then
          FillAhorroPresta( oListaPrestamos,  K_QRY_T_PRESTA );
  end;

  procedure CreaBitacoraDescuentos;
  begin
       if ( FDatosPeriodo.DescuentaPrestamos or FDatosPeriodo.DescuentaAhorros ) then
          FBitacoraDescuentos := TList.Create;
  end;

  procedure FillGlobales;
  var
     oConcepto : TConceptoNomina;
     nConcepto : Integer;
  begin
       oZetaProvider.AbreQueryScript( FQGeneral, GetScriptPeriodo( K_QRY_GLOBALES ));
       with FQGeneral do
         while not Eof do
         begin
              // Obtiene concepto de N�mina
	      nConcepto := FieldByName( 'CO_NUMERO' ).AsInteger;
	      oConcepto := GetConcepto( nConcepto );
	      if ( oConcepto = NIL ) then
	         DataBaseError( Format( 'Movimiento Global: Concepto # %d no est� activo o no existe', [ nConcepto ] ));

              if  zStrToBool( FieldByName( 'MG_FIJO' ).AsString ) then
              begin
                   oConcepto.Global := TRUE;
                   oConcepto.MontoGlobal := FieldByName( 'MG_MONTO' ).AsCurrency;
              end;
              if ( NOT oConcepto.EsAhorroPrestamo ) then
                 oConcepto.CalcularSiempre := TRUE;
              Next;
        end;
  end;


  procedure SetPagoRecibos;
  begin
     if oZetaProvider.GetGlobalBooleano(K_DEFAULT_RECIBOS_PAGADOS) then
        dPagoRecibo:= FDatosPeriodo.Pago
     else
         dPagoRecibo:= 0;
  end;

begin     // CalculoBegin
     FCalculandoNomina := TRUE;
     FConceptosPreparados := FALSE;
     SQLBroker.Init(enNomina);
     InicializaEvaluador;    // Debe ir ANTES de PreparaQuerys //
     FillConceptos;		// Crea Lista de Conceptos y relaciona con Arreglo de Conceptos //
     SetConceptosGlobales;	// Asigna valores de Globales //
     PreparaQuerys;
     PreparaEvalGral;
     FillAhorros;		// Crea Lista de Tipos de Ahorro //
     FillPrestamos;		// Crea Lista de Tipos de Pr�stamo //
     CreaBitacoraDescuentos; //Crea la bitacora para los descuentos.
     FillGlobales;
     SetPagoRecibos;
     RetroactivoBegin;

end;


{
procedure TCalcNomina.GetDatosPeriodo;
begin
    oZetaProvider.AbreQueryScript( FQGeneral,
                                  Format( GetScript( K_QRY_PERIODO ), [ FYear, FTipo, FNumero ] ));
    with FDatosPeriodo, FQGeneral do
    begin
        Year           := FYear;
        Tipo           := eTipoPeriodo( FTipo );
        Numero         := FNumero;
        Inicio         := FieldByName( 'PE_FEC_INI' ).AsDateTime;
        Fin            := FieldByName( 'PE_FEC_FIN' ).AsDateTime;
        Pago           := FieldByName( 'PE_FEC_PAG' ).AsDateTime;
        Dias           := FieldByName( 'PE_DIAS' ).AsInteger;
        Status         := eStatusPeriodo( FieldByName( 'PE_STATUS' ).AsInteger );
        Mes            := FieldByName( 'PE_MES' ).AsInteger;
        SoloExcepciones:= zStrToBool( FieldByName( 'PE_SOLO_EX' ).AsString );
        IncluyeBajas   := zStrToBool( FieldByName( 'PE_INC_BAJ' ).AsString );
        // Estos no est�n en el FDatosPeriodo
        lDescuentaAhorros   := zStrToBool( FieldByName( 'PE_AHORRO' ).AsString );
        lDescuentaPrestamos := zStrToBool( FieldByName( 'PE_PRESTAM' ).AsString );
    end;
end;
}

function TCalcNomina.GetScriptPeriodo(const iScript: Integer): String;
begin
     Result := Format( GetScript( iScript ), [ FYear, FTipo, FNumero ] );
end;

function TCalcNomina.GetScript(const iScript: Integer): String;
const
     K_WHERE_PERIODO = 'PE_YEAR = %d AND PE_TIPO = %d AND PE_NUMERO = %d ';
     K_MOVIMIEN = 'SELECT MO_ACTIVO,CO_NUMERO,MO_IMP_CAL,US_CODIGO,MO_PER_CAL,MO_REFEREN,MO_X_ISPT,MO_PERCEPC,MO_DEDUCCI FROM MOVIMIEN where ' + K_WHERE_PERIODO +
                                   'AND CB_CODIGO = :Empleado ';
begin
     case ( iScript ) of
       //K_QRY_PERIODO : Result := 'select * from PERIODO where ' + K_WHERE_PERIODO;
       K_QRY_CONCEPTOS : Result := 'SELECT CO_NUMERO, CO_TIPO, CO_CALCULA, CO_FORMULA, CO_FRM_ALT, ' +
                                   'CO_X_ISPT, CO_G_ISPT, CO_MENSUAL, CO_IMP_CAL, CO_DESCRIP,CO_ISN, CO_PS_TIPO,' +
                                   'QUERYS.QU_FILTRO ' +
                                   'FROM CONCEPTO '+
                                   'LEFT OUTER JOIN QUERYS ON CO_QUERY = QU_CODIGO  '+
                                   'where CO_NUMERO >= 1 AND CO_NUMERO <= 999 '+
                                   'AND CO_ACTIVO = ''S'' AND CO_TIPO > 0  AND '+
                                   '( ( CO_USO_NOM = 0 ) OR ( CO_USO_NOM = %d ) ) ' +
                                   'ORDER BY ( case CO_PS_TIPO when 1 then 0 else 1 end ) desc,  CO_NUMERO';
{$ifdef INTERBASE}
       K_SP_ACUM_ANUAL : Result := '( select RESULTADO from sp_as( %d, 1, 13, %d, NOMINA.CB_CODIGO ))';
{$else}
       K_SP_ACUM_ANUAL : Result := 'dbo.sp_as( %d, 1, 13, %d, NOMINA.CB_CODIGO )';
{$endif}
       K_QRY_MOVIMIEN_TODOS  : Result := K_MOVIMIEN;
       K_QRY_MOVIMIEN   : Result := K_MOVIMIEN +  ' AND US_CODIGO > 0'; { S�lo Excepciones }
       K_QRY_BORRA_CALC : Result :='DELETE FROM MOVIMIEN where ' + K_WHERE_PERIODO +
                                   'AND CB_CODIGO = :Empleado';
       {Se tiene que ordenar por TB_PRIORID, TIPO(Ahorro o Prestamo), PR_TIPO O AH_TIPO, REFERENCIA}
       K_QRY_AHORROS : Result := 'SELECT TAHORRO.TB_PRIORID PRIORIDAD, '+
                                 'CAST ((0) AS INTEGER)  TIPO, '+
                                 'AH_TIPO PR_TIPO, '+
                                 'CAST(('' '') AS varchar(8)) PR_REFEREN, '+
                                 'CAST ((0) AS numeric(15,2)) PR_MONTO, '+
                                 'CAST ((0) AS numeric(15,2))  PR_SALDO, '+
                                 'AH_FORMULA PR_FORMULA '+
                                 'FROM AHORRO '+
                                 'left outer join TAHORRO on (TAHORRO.TB_CODIGO = AHORRO.AH_TIPO) '+
                                 'WHERE CB_CODIGO = :EmpleadoAhorro AND AH_STATUS = 0 AND AH_FECHA <= ''%s''  ';
       K_QRY_PRESTAMOS : Result := 'SELECT TPRESTA.TB_PRIORID PRIORIDAD,  '+
                                   'CAST ((1) AS INTEGER)  TIPO, '+
                                   'PR_TIPO, '+
                                   'PR_REFEREN, '+
                                   'PR_MONTO, '+
                                   'PR_SALDO, '+
                                   'PR_FORMULA '+
                                   'FROM PRESTAMO '+
                                   'left outer join TPRESTA on (TPRESTA.TB_CODIGO = PRESTAMO.PR_TIPO) '+
                                   'WHERE CB_CODIGO = :EmpleadoPrestamo AND PR_STATUS = 0 AND PR_FECHA <= ''%s'' ';
{$IFDEF MSSQL}
       K_QRY_AHO_PRE : Result := 'select PRIORIDAD, TIPO, PR_TIPO, PR_REFEREN, PR_MONTO, PR_SALDO, PR_FORMULA '+
                                 'from AHO_PRE where '+
                                 '( CB_CODIGO = :Empleado ) and '+
                                 '( FECHA <= ''%s'' ) ';
{$ENDIF}

       K_QRY_INSERT_MOV : Result :='INSERT INTO MOVIMIEN ( PE_YEAR, PE_TIPO, PE_NUMERO, CB_CODIGO, CO_NUMERO, MO_REFEREN, MO_ACTIVO, US_CODIGO, '+
                                   'MO_PERCEPC, MO_DEDUCCI, MO_IMP_CAL, MO_PER_CAL, MO_X_ISPT )  '+
                                   'VALUES ( %d, %d, %d, :Empleado, :Concepto, :Refe, :Act, :Us, :Per, :Ded, :Imp, :Calc, :Exento )';
{$ifdef INTERBASE}
       K_QRY_UPDATE_NOM : Result :='execute procedure UPDATE_NOM_TOTALES( ' +
                                    ':Empleado, %d, %d, %d, ' +
                                    ':NO_PERCEPC, :NO_DEDUCCI, :NO_NETO, ' +
                                    ':NO_IMP_CAL, :NO_PER_CAL, :NO_X_CAL, ' +
                                    ':NO_PER_MEN, :NO_X_MENS,  :NO_X_ISPT, ' +
                                    ':NO_TOT_PRE, :NO_STATUS,  :NO_USR_PAG, '+
                                    ':NO_FEC_PAG, :NO_PREV_GR, :NO_APROBA, '+
                                    ':NO_RASTREO, :NO_PER_ISN )';
{$else}
       K_QRY_UPDATE_NOM : Result :='update NOMINA set '+
                                        'NO_PERCEPC = :NO_PERCEPC,'+  //0
                                        'NO_DEDUCCI = :NO_DEDUCCI,'+  //1
                                        'NO_NETO    = :NO_NETO,'+      //2
                                        'NO_IMP_CAL = :NO_IMP_CAL,'+   //3
                                        'NO_PER_CAL = :NO_PER_CAL,'+   //4
                                        'NO_X_CAL   = :NO_X_CAL,'+     //5
                                        'NO_PER_MEN = :NO_PER_MEN,'+   //6
                                        'NO_X_MENS  = :NO_X_MENS,'+    //7
                                        'NO_X_ISPT  = :NO_X_ISPT,'+    //8
                                        'NO_TOT_PRE = :NO_TOT_PRE,'+   //9
                                        'NO_STATUS  = :NO_STATUS,'+    //10
                                        'NO_USR_PAG = :NO_USR_PAG,'+   //11
                                        'NO_FEC_PAG = :NO_FEC_PAG,'+   //12
                                        'NO_PREV_GR = :NO_PREV_GR,'+   //13
                                        'NO_APROBA  = :NO_APROBA,'+     //14
                                        'NO_RASTREO = :NO_RASTREO, '+   //15
                                        'NO_PER_ISN = :NO_PER_ISN '+   //16
                                        //US 17266: Almacenar la clave SAT cuando se timbra la n�mina para obtener reportes y auditar timbrado
                                        ',NO_VER_CON = :NO_VER_CON '+   //18

                                        ' where ' +
                                        '( PE_YEAR = %d ) and '+
                                        '( PE_TIPO = %d ) and '+
                                        '( PE_NUMERO = %d ) and '+
                                        '( CB_CODIGO = :Empleado )';    //17

{$endif}
       K_QRY_T_AHORRO : Result :=   'select TB_CODIGO, TB_ELEMENT, TB_CONCEPT, TB_RELATIV, TB_LIQUIDA  '+
                                    'from TAHORRO order by TB_CODIGO';
       K_QRY_T_PRESTA : Result :=   'select TB_CODIGO, TB_ELEMENT, TB_CONCEPT, TB_RELATIV, TB_LIQUIDA  '+
                                    'from TPRESTA order by TB_CODIGO';
       K_QRY_GLOBALES : Result :=   'select CO_NUMERO, MG_FIJO, MG_MONTO from MOV_GRAL  '+
                                    'where ' + K_WHERE_PERIODO;
       K_QRY_STATUS_ANTERIOR : Result := 'select PE_STATUS from PERIODO where ' + K_WHERE_PERIODO;
{$ifdef INTERBASE}
       K_QRY_GET_EMPLEADOS: Result := 'select COUNT(*) as CUANTOS from COLABORA '+
                                      'where ( ( select RESULTADO from SP_STATUS_ACT ( %s, COLABORA.CB_CODIGO ) ) = 1 ) ';
{$else}
       K_QRY_GET_EMPLEADOS: Result := 'select COUNT(*) as CUANTOS from COLABORA '+
                                      'where ( ( select DBO.SP_STATUS_ACT ( %s, COLABORA.CB_CODIGO ) ) = 1 ) ';
{$endif}
       K_QRY_UPDATE_LOG_PERIODO: Result := 'Update PERIODO set PE_LOG = %s ' +
                                           'where PE_YEAR = %d ' +
                                           'and PE_TIPO = %d ' +
                                           'and PE_NUMERO = %d ';
       {*** US 12365: Timbrado/Declaraciones : Mejorar calculo de exentos en n�mina ***}
       K_QRY_ACUMULADO_CONCEPTO_EXENTO: Result := 'SELECT RESULTADO = DBO.SP_AS( %s,%s,%s,%s,%s )';
       //US 17266: Almacenar la clave SAT cuando se timbra la n�mina para obtener reportes y auditar timbrado
       K_QRY_KAR_CONCEPTO_VERSION_ACTUAL: Result := 'SELECT VERSION_CONCEPTO = DBO.KardexConceptos_GetVersionActual()';
   end;
end;

function TCalcNomina.GetConcepto( const NumConcepto : Integer )  :  TConceptoNomina;
var
	  nPos : Integer;
begin
        Result := NIL;
        if NumConcepto > 0 then
        begin
        	    nPos :=	aConceptos[ NumConcepto ];
             if nPos >= 0 then
                Result := TConceptoNomina( oListaConceptos.Items[ nPos ] );
        end;
end;

procedure TCalcNomina.GetParametrosCalculo( Parametros : Variant );
begin
    with FParametros do
    begin
        VarValues    := Parametros;
        FYear        := ParamByName( 'Year' ).AsInteger;
        FTipo        := ParamByName( 'Tipo' ).AsInteger;
        FNumero      := ParamByName( 'Numero' ).AsInteger;
    end;
end;

procedure TCalcNomina.RegistraISPT( const nMonto : TPesos );
begin
     nRegistraIspt := nMonto;
end;

function TCalcNomina.CreditoAplicado( const lCreditoPeriodo: Boolean ): TPesos;
begin
     if lCreditoPeriodo then
        Result := nCreditoPeriodo
     else
         Result := nCreditoMensual;
end;

procedure TCalcNomina.RegistraCredito( const nMonto : TPesos );
begin
     nRegistraCredito := nMonto;
end;

procedure TCalcNomina.RegistraCreditoPeriodo( const nMonto : TPesos );
begin
     nCreditoPeriodo := nMonto; {Monto Cr�dito al Salario APLICADO del periodo de nomina}
end;

procedure TCalcNomina.RegistraCreditoMensual( const nMonto : TPesos );
begin
     nCreditoMensual := nMonto; {Monto Cr�dito MENSUAL al Salario APLICADO}
end;

function TCalcNomina.SubeAplicado: TPesos;
begin
     Result := nSubeMensual;
end;

procedure TCalcNomina.RegistraSubeMensual( const nMonto : TPesos );
begin
     nSubeMensual := nMonto; {Monto SUBE MENSUAL al Salario APLICADO}
end;

function TCalcNomina.EvaluaFormula(const sFormula, sTitulo: string ) : TPesos;
var
   EVResultado : TQREvResult;
begin
     EVResultado := FEvaluadorGral.Calculate( sFormula );
     //if EvResultado.Kind = resError then
     //   oZetaProvider.Log.Error( 0, 'Error en la formula:'+sFormula, sTitulo );
     //EvResultado.strResult := EvResultado.strResult + CR_LF + sFormula;
     ValidaError( EvResultado, sTitulo  );
     case EVResultado.Kind of
          resInt    : Result := EVResultado.intResult;
          resDouble : Result := Redondea( EVResultado.dblResult );
          else        Result := 0;
     end;
end;

function TCalcNomina.Neto : TPesos;
begin
     Result := Redondea( nNomina_NO_PERCEPC - nNomina_NO_DEDUCCI );
end;

function TCalcNomina.HayCampo(const sNombre: String): Boolean;
begin
     Result := Assigned( FQEmpleado.FindField( sNombre ) );
end;

function TCalcNomina.GetCampo( const sNombre : String ) : TZetaField;
begin
     Result := FQEmpleado.FindField( sNombre );
     if ( Result = NIL ) then
        DataBaseError( 'Campo ''' + sNombre + ''' no existe' );
end;

procedure TCalcNomina.SumaTotalesMovimiento( const oConcepto : TConceptoNomina; oMovimien : TConceptoCalculado; Factor : Integer );
begin
   // Acumula totales en NOMINA
    with oConcepto do
	case Tipo of
          coPercepcion :
          begin
              nNomina_NO_PERCEPC := nNomina_NO_PERCEPC + oMovimien.Monto*Factor;
              if ( oMovimien.ConISPT ) then	// Si tiene ISPT Individual
              begin
                  nNomina_NO_IMP_CAL := nNomina_NO_IMP_CAL + oMovimien.ISPT;
                  nNomina_NO_PER_CAL := nNomina_NO_PER_CAL + oMovimien.Monto;
                  nNomina_NO_X_CAL := nNomina_NO_X_CAL + oMovimien.Exento;
              end
              else
              if ( Mensual ) then		// Si es percepci�n mensual
              begin
                  nNomina_NO_PER_MEN  := nNomina_NO_PER_MEN + oMovimien.Monto;
                  nNomina_NO_X_MENS   := nNomina_NO_X_MENS + oMovimien.Exento;
              end
              else					// Es percepci�n peri�dica
                  nNomina_NO_X_ISPT := nNomina_NO_X_ISPT + oMovimien.Exento;
              if ( GravaISN ) then //Impuesto Estatal V2011
              begin
                   nNomina_NO_PER_ISN := nNomina_NO_PER_ISN + oMovimien.Monto;
              end;
              with DataModule as TCalcNomina do
              begin
                if ( oMovimien.Concepto.PrevisionSocial = pscPrevisioSocial )  then
                  nNomina_NO_X_PS :=   nNomina_NO_X_PS + oMovimien.Exento
                else
                if ( oMovimien.Concepto.PrevisionSocial = pscSumaSalarios )  then
                  nNomina_NO_SAL_PS :=   nNomina_NO_SAL_PS + oMovimien.Monto*Factor;
              end;
          end;
          coDeduccion  : nNomina_NO_DEDUCCI := nNomina_NO_DEDUCCI + oMovimien.Monto;
          coPrestacion :
          begin
               nNomina_NO_TOT_PRE := nNomina_NO_TOT_PRE + oMovimien.Monto;
               //Se tiene que condicionar con el IF,
               //porque si no GravaIspt el Exento es el Total de la Prestacion
               //Si grava a Ispt, nNomina_NO_PREV_GR es la Prestacion - Exento
               if oConcepto.GravaISPT then
               begin
                    nNomina_NO_PREV_GR := nNomina_NO_PREV_GR + oMovimien.Monto - oMovimien.Exento;
                    if ( oMovimien.ConISPT ) then	// Si tiene ISPT Individual
                    begin
                        nNomina_NO_IMP_CAL := nNomina_NO_IMP_CAL + oMovimien.ISPT;
                        nNomina_NO_PER_CAL := nNomina_NO_PER_CAL + oMovimien.Monto;
                        nNomina_NO_X_CAL := nNomina_NO_X_CAL + oMovimien.Exento;
                    end;
               end;
               if ( GravaISN ) then //Impuesto Estatal V2011
               begin
                    nNomina_NO_PER_ISN := nNomina_NO_PER_ISN + oMovimien.Monto;
               end;

              with DataModule as TCalcNomina do
              begin
                if ( oMovimien.Concepto.PrevisionSocial = pscPrevisioSocial )  then
                  nNomina_NO_X_PS :=   nNomina_NO_X_PS + oMovimien.Exento
                else
                if ( oMovimien.Concepto.PrevisionSocial = pscSumaSalarios )  then
                  nNomina_NO_SAL_PS :=   nNomina_NO_SAL_PS + oMovimien.Monto;
              end;

          end;
	end;
end;

procedure TCalcNomina.SetRastroNomina( const oConcepto: TConceptoNomina; sTextoRastreo: String; const lExcepcion: Boolean );
var
   iPos: Integer;

     procedure RastreoTitulo;
     begin
          oConcepto.Evaluador.Rastreador.RastreoSeparadorPag( 'RASTREO DEL CALCULO DE CONCEPTO DE NOMINA' );
          RastreoEspacio;
          RastreoTextoLibre( 'Concepto: ' + IntToStr( oConcepto.Numero ) + ' = ' + oConcepto.Descripcion );
          RastreoTextoLibre( 'F�rmula : ' + Copy( oConcepto.Formula,  1, 60 ));
          RastreoNoVacio(    '          ' + Copy( oConcepto.Formula, 61, 60 ));
          RastreoNoVacio(    '          ' + Copy( oConcepto.Formula,121, 60 ));
          RastreoNoVacio(    '          ' + Copy( oConcepto.Formula,181, 60 ));
          RastreoHeader( '' );
          if ( lExcepcion ) then
          begin
               RastreoHeader( 'EXCEPCION DE MONTO' );
               RastreoPesos( 'Monto Capturado : ', oConcepto.Monto );
          end
          else
          begin
               RastreoPesos( 'Monto Calculado del Concepto : ', oConcepto.Monto );
          end;
          oConcepto.Evaluador.Rastreador.RastreoTextoLibre('', iPos);
     end;

     procedure RastreoPiePagina;
     begin
          RastreoEspacio;
          RastreoEspacio;
     end;

begin
     iPos:= 0;
     if StrLleno( sTextoRastreo ) then
     begin
          with oConcepto do
          begin
               Evaluador.Rastreador.RastreoBegin;
               RastreoTitulo;
               Evaluador.Rastreador.RastreoInsertaTexto( sTextoRastreo, iPos );
               RastreoPiePagina;
               FTotalRastroNomina:= FTotalRastroNomina + Evaluador.Rastreador.Rastro;
          end;
     end;
end;

function TCalcNomina.BuscaReferencia( oConcepto : TConceptoNomina; const Referencia: String): Integer;
begin
     Result := oListaCalculados.IndexOf( IntToStr( oConcepto.Numero ) + '@' + Referencia );
end;

function TCalcNomina.ConceptoCalculado(const NumConcepto: Integer): TPesos;
var
   oConcepto : TConceptoNomina;
begin
     oConcepto := GetConcepto( NumConcepto );
     if ( oConcepto <> NIL ) then
     begin
          if oConcepto.Status = stcCalculado then
          begin
               Result := oConcepto.Monto
          end
          else if oConcepto.Status = stcDormido then
             Result := 0
          else
          begin
               lPendiente := TRUE;  // Hay pendientes
               Result := 0;
          end;
     end
     else
         Result := 0;
end;

{*** US 12365: Timbrado/Declaraciones : Mejorar calculo de exentos en n�mina ***}
function TCalcNomina.ConceptoCalculadoEx(const NumConcepto: Integer): TPesos;
var
   oConcepto : TConceptoNomina;
begin
     oConcepto := GetConcepto( NumConcepto );
     if ( oConcepto <> NIL ) then
     begin
          if oConcepto.Status = stcCalculado then
          begin
               if ( oConcepto.Movimiento <> nil )  then
               begin
                   Result := oConcepto.Movimiento.Exento
               end
               else
                   Result := 0;
          end
          else if oConcepto.Status = stcDormido then
             Result := 0
          else
          begin
               lPendiente := TRUE;  // Hay pendientes
               Result := 0;
          end;
     end
     else
         Result := 0;
end;

{*** US 12365: Timbrado/Declaraciones : Mejorar calculo de exentos en n�mina ***}
function TCalcNomina.CalExento(const NumConcepto: Integer; const TopeExento: TPesos): TPesos;
var
   dMontoExentoCalculado: TPesos;
   dMonto : TPesos;
   dTope : TPesos;
   dAcumuladoConcepto, dSalMin : TPesos;
   oConcepto, oConceptoAcumulado : TConceptoNomina;
   iConcepto, iPos: Integer;

   function ObtenerConpetoActual: Integer;
   begin
        Result := 0;
        with FParametros do
        begin
               Result := ParamByName( 'NumeroConcepto' ).AsInteger;
        end;
   end;
begin
     iPos := 0;
     dMonto := TCalcNomina(FEvaluadorGral.CalcNomina).nMontoFormula;
     dSalMin := TCalcNomina(FEvaluadorGral.CalcNomina).nEmpleadoSalMin;
     dAcumuladoConcepto := AcumuladoConceptoExento( NumConcepto );
     dTope := TopeExento - dAcumuladoConcepto;
     Result := rMin( dTope, dMonto );
     if FEvaluadorGral.Rastreando then
     begin
          iConcepto := ObtenerConpetoActual;
          oConcepto := GetConcepto( iConcepto );
          oConceptoAcumulado := GetConcepto( NumConcepto );
          if ( oConceptoAcumulado <> NIL ) then
          begin
               with oConcepto.Evaluador do
               begin
                    RastreoEspacio;
                    RastreoHeader( 'Calculo de exento' );
                    RastreoHeader( '' );
                    RastreoTextoLibre( 'Concepto: ' + IntToStr( oConceptoAcumulado.Numero ) + ' = ' + oConceptoAcumulado.Descripcion );
                    RastreoTextoLibre( 'F�rmula Exento ISPT : ' + Copy( oConcepto.ExentoISPT,  1, 60 ));
                    RastreoNoVacio(    '          ' + Copy( oConcepto.ExentoISPT, 61, 60 ));
                    RastreoNoVacio(    '          ' + Copy( oConcepto.ExentoISPT,121, 60 ));
                    RastreoNoVacio(    '          ' + Copy( oConcepto.ExentoISPT,181, 60 ));

                    RastreoPesos( ' Monto Calculado del Concepto:', dMonto );
                    RastreoPesos( '               Salario M�nimo:', dSalMin );

                    RastreoHeader('');
                    RastreoPesos( ' Tope Exento (Param. #2 funci�n ):', TopeExento );
                    RastreoPesos( ' (-) Monto Acumulado del Concepto:', dAcumuladoConcepto );
                    RastreoPesos( ' (=)   Tope Exento ISPT Calculado:', dTope );
                    RastreoEspacio;
                    RastreoPesos( '         Tope Exento ISPT Calculado:', dTope );
                    RastreoPesos( ' (MIN) Monto Calculado del Concepto:', dMonto);
                    RastreoPesos( ' (=)               Exento Calculado:', Result );

                    Rastreador.RastreoTextoLibre('', iPos);
                    RastreoEspacio;
                    FTotalRastroNomina:= FTotalRastroNomina + Rastreador.Rastro;
               end;
          end;
     end;
end;

function TCalcNomina.AcumuladoConceptoExento( const NumConcepto: Integer ): Tpesos;
const
     K_MES_INI = 1;
     K_MES_FIN = 13;
var
   sConcepto, sMesini, sMesFin, sAnio, sEmpleado : String;
begin
     sConcepto := IntToStr( NumConcepto );
     sMesIni := IntToStr( K_MES_INI );
     sMesFin := IntToStr( K_MES_FIN );
     sAnio := IntToStr( FYear );
     sEmpleado := IntToStr( FEmpleado );
     with oZetaProvider do
     begin
          AbreQueryScript( FQGeneral, Format( GetScript( K_QRY_ACUMULADO_CONCEPTO_EXENTO ), [ sConcepto, sMesIni, sMesFin, sAnio, sEmpleado ] ));
          with FQGeneral do
          begin
               if not Eof then
                  Result :=  FieldByName( 'RESULTADO' ).AsCurrency
               else
                   Result := 0;
          end;
     end;
end;

//US 17266: Almacenar la clave SAT cuando se timbra la n�mina para obtener reportes y auditar timbrado
function TCalcNomina.ConsultarVersionConceptoActual: Integer;
begin
     with oZetaProvider do
     begin
          AbreQueryScript( FQGeneral, GetScript( K_QRY_KAR_CONCEPTO_VERSION_ACTUAL ));
          with FQGeneral do
          begin
               if not Eof then
                  Result :=  FieldByName( 'VERSION_CONCEPTO' ).AsInteger
               else
                   Result := 0;
          end;
     end;
end;

{*** FIN ***}

function TCalcNomina.ConceptoTipo(const NumConcepto: Integer): eTipoConcepto;
var
   oConcepto : TConceptoNomina;
begin
     Result := coPercepcion;
     oConcepto := GetConcepto(NumConcepto);
     if ( oConcepto <> NIL ) then
     begin
          Result := oConcepto.Tipo;
     end;
end;

function TCalcNomina.TotNomina(const eTotal: TipoTotalNomina): TPesos;
begin
   case eTotal of
     ttnPercepciones : Result := nNomina_NO_PERCEPC;
     ttnDeducciones  : Result := nNomina_NO_DEDUCCI;
     ttnNeto         : Result := Neto;
     ttnMensuales    : Result := nNomina_NO_PER_MEN;
     ttnExentas      : Result := nNomina_NO_X_ISPT;
     ttnPerCal       : Result := nNomina_NO_PER_CAL;
     ttnImpCal       : Result := nNomina_NO_IMP_CAL;
     ttnExentoCal    : Result := nNomina_NO_X_CAL;
     ttnExentoMensual: Result := nNomina_NO_X_MENS;
     ttnPrestaciones : Result := nNomina_NO_TOT_PRE;
     ttnGravadoPrev  : Result := nNomina_NO_PREV_GR;
     ttnGravadoISN   : Result := nNomina_NO_PER_ISN;
     ttnExentoPS     : Result := nNomina_NO_X_PS;
     ttnSalarioPS    : Result := nNomina_NO_SAL_PS;
     else              Result := 0;
   end;
end;

function TCalcNomina.GetPeriodoData: string;
begin
     with oZetaProvider.DatosPeriodo do
     begin
          Result := ZetaCommonTools.GetPeriodoInfo( Year, Numero, Tipo );
     end;
end;

{*****************************************************************************}
{************************ Calculo de Retroactivos ****************************}


procedure TCalcNomina.RetroactivoBegin;
begin
     {Para el calculo de Retroactivos}
     if NominaRetroactiva then
     begin
          with FRetroactivo do
          begin
               SQLOriginal := SQLBroker.Agente.SQL.Text;
          end;
     end;
end;

procedure TCalcNomina.SetRetroactivo(const Value: TRetroactivo);
begin
     FRetroactivo := Value;
     NominaRetroactiva := TRUE;
end;

procedure TCalcNomina.SetNominaRetroactiva(const Value: Boolean);
begin
     FNominaRetroactiva := Value;
end;

function TCalcNomina.PreparaFormula( const sOriginal: string ): string;
begin
     Result := ZetaCommonTools.BorraCReturn( sOriginal );
     if NominaRetroactiva then
     begin
          Result := ZetaServerTools.CambiaCamposRetroactivos( Result );
     end;
end;


function TCalcNomina.CalculoRetroactivoBuildDataset : OleVariant;
begin
     InitBroker;
     with SQLBroker do
     begin
          Init(enKardex);
          with Agente do
          begin
               // El orden de las columnas importa en CalculaUnEmpleado
               //AgregaColumna( 'COLABORA.CB_CODIGO', TRUE, enEmpleado, tgNumero,0 );
               AgregaColumna( 'KARDEX.CB_CODIGO', TRUE, Entidad, tgNumero, 0, 'CB_CODIGO' );
               AgregaColumna( K_PRETTYNAME, TRUE, enEmpleado, tgTexto, K_ANCHO_PRETTYNAME, 'PRETTYNAME' );
               AgregaColumna( 'KARDEX.CB_SALARIO', TRUE, Entidad, tgFloat, 0, 'CB_SALARIO' );
               AgregaColumna( 'KARDEX.CB_SAL_INT', TRUE, Entidad, tgFloat, 0, 'CB_SAL_INT' );
               AgregaFiltro( Format( 'KARDEX.CB_TIPO = ''%s''', [ K_T_CAMBIO ] ), TRUE, Entidad );
               with FParametros do
               begin
                    AgregaFiltro( Format( 'KARDEX.CB_FECHA = ''%s''', [ DateToStrSQL( ParamByName('CambioSalario').AsDate ) ] ), TRUE, Entidad );
                    AgregaFiltro( GetTablaRangoLista( ParamByName( 'RangoLista' ).AsString, Entidad ), TRUE, Entidad );
                    AgregaFiltro( ParamByName( 'Filtro' ).AsString, FALSE, Entidad );
                    AgregaFiltro( ParamByName( 'Condicion' ).AsString, TRUE, enEmpleado );
               end;
               FiltroConfidencial( oZetaProvider.EmpresaActiva );
               AgregaOrden( 'KARDEX.CB_CODIGO', TRUE, Entidad );
          end;
          if SuperSQL.Construye( Agente ) then
             SuperReporte.Construye( Agente )
          else
               DataBaseError( Agente.ListaErrores[ 0 ] );

          Result := SuperReporte.GetReporte;
     end;
end;

function TCalcNomina.CalculoRetroactivoDataset( DataSet : TDataSet ): OleVariant;
var
    iEmpleado :Integer;
    lOk: Boolean;
{$ifdef VALIDAEMPLEADOSGLOBAL}
    sMensajeAdvertencia : string;
{$endif}
begin
     FDataSetLista := DataSet;
     with oZetaProvider do
     begin
          InitArregloTPeriodo;
          {Validacion de la Nomina Actual}
          with DatosPeriodo do
          begin
               if ( Status < spCalculadaTotal ) then
                  DatabaseError( Format('La N�mina De %s No Ha Sido Calculada Totalmente',[GetPeriodoData])  );
          end;
     end;

     ValidaNominaAnterior;

     PreparaNomina( eTipoCalculo( FParametros.ParamByName( 'TipoCalculo' ).AsInteger ) );

{$ifdef VALIDAEMPLEADOSGLOBAL}
     ValidaStatusNomina( sMensajeAdvertencia );
{$else}
	 ValidaStatusNomina;
{$endif}

     with oZetaProvider do
     begin
          with FDataSetLista do
          begin
               First;
               iEmpleado := Fields[ 0 ].AsInteger;
               try
                 CalculoBegin;
                 while ( not Eof ) and CanContinue( iEmpleado ) and ( not Log.HayErrores ) do
                 begin
                      if Retroactivo.TieneNomina( iEmpleado ) then
                      begin
                           lOK:= TRUE;
                           Retroactivo.SetDatosEmpleado( FDataSetLista );
                           EmpiezaTransaccion;
                           try
                              CalculaUnEmpleado( iEmpleado );
                           except
                                 on Error: Exception do
                                 begin
                                      lOk := FALSE;
                                      Log.Excepcion( iEmpleado, 'Error al Calcular N�mina', Error );
                                 end;
                           end;
                           TerminaTransaccion( True );   //Siempre se hace Commit

                           if lOK then
                           begin
                                FRetroactivo.CalculaRetroactivo( oListaCalculados );
                           end;
                      end;


                      Next;
                      iEmpleado := Fields[ 0 ].AsInteger;
                 end;
               except
                     on Error: EErrorCalculo do
                     begin
                          with Error as EErrorCalculo do
                          begin
                               Log.Error( iEmpleado, Titulo, Message );
                          end;
                     end;
                     on Error: EAbort do;  { Excepci�n Silenciosa }
                     on Error: Exception do
                     begin
                          Log.Excepcion( iEmpleado, 'Error al Calcular N�mina', Error );
                     end;
               end;

{$ifdef VALIDAEMPLEADOSGLOBAL}
               if strLLeno( sMensajeAdvertencia ) then
                  Log.Advertencia(clbNinguno, 0, Date, 'Licencia de Empleados', sMensajeAdvertencia);
{$endif}
          end;
     end;
end;

{*****************************************************************************}
{************************ Clase Tetroactivo **********************************}

constructor TRetroactivo.Create( oCreator: TZetaCreator );
begin
     oZetaCreator := oCreator;
     oZetaProvider := oZetaCreator.oZetaProvider;

     FRastreo := TRastreador.Create;
     FRetroConceptos := TStringList.Create;
     FYearAd := 0;
     with oZetaProvider.ParamList do
     begin
          FRetroConceptos.CommaText:= ParamByName('ListaConceptos').AsString;
          FFecha:= ParamByName('CambioSalario').AsDateTime;
          FConcepto:= ParamByName('ConceptoRetroactivo').AsInteger;
          FYearAPagar := oZetaProvider.YearDefault;
          FNominaAPagar:=ParamByName('NominaAPagar').AsInteger;
          FNominaDesde := ParamByName('NominaDesde').AsInteger;
          FNominaHasta := ParamByName('NominaHasta').AsInteger;
          FExcepcion := eOperacionMontos( ParamByName('ExcepcionOp').AsInteger );
     end;

     CreaDataset;
     CreateQuerys;
end;

procedure TRetroactivo.CreateQuerys;
begin
     with oZetaProvider do
     begin
          FQConcepto := CreateQuery( GetScript( Q_RETRO_CONCEPTO ) );
          FQTieneNomina := CreateQuery( GetScript( Q_RETRO_TIENE_NOMINA ) );
          FQSalario := CreateQuery( GetScript( Q_RETRO_SALARIO ) );
          FQTipoConcepto := CreateQuery( GetScript( Q_RETRO_TIPO_CONCEPTO ) );
     end;
end;

procedure TRetroactivo.PreparaQuerys;
 procedure SetParams( oDataset : TZetaCursor );
 begin
      with oZetaProvider do
      begin
           ParamAsInteger( oDataset, 'Year', FYear );
           ParamAsInteger( oDataset, 'Tipo', Ord( DatosPeriodo.Tipo ) );
           ParamAsInteger( oDataset, 'Numero', FNominaActiva );
      end;
 end;
begin
     SetParams( FQConcepto );
     SetParams( FQTieneNomina );
     SetParams( FQSalario );
     oZetaProvider.ParamAsInteger( FQTipoConcepto, 'Numero', FConcepto );
end;

procedure TRetroactivo.CreaDataset;
begin
     FDataset := TServerDataSet.Create( oZetaProvider );
     with FDataset do
     begin
          AddIntegerField( 'CB_CODIGO' );
          //AddIntegerField( 'CO_TIPO' );
          AddFloatField( 'RETROACTIVO' );
          AddBlobField( 'RASTREO', 0 );
          CreateTempDataSet;
     end;
end;

destructor TRetroactivo.Destroy;
begin
     FreeAndNil( FRastreo );
     FreeAndNil( FDataset );
     FreeAndNil( FRetroConceptos );
     FreeAndNil( FQTieneNomina );
     FreeAndNil( FQConcepto );
     FreeAndNil( FQSalario );
     FreeAndNil( FQTipoConcepto );
     inherited;
end;


function TRetroactivo.TransformaCampo( const sOriginal, sCampo: string;
                          const rValor: TPesos ): string;
 const
     aTablas: array[0..1] of string = ('NOMINA','RETROACTIVO');
 var
   i: integer;
   sValor: string;
begin
     Result := sOriginal;
     {Se est� multiplicando por 1.0 porque al hacer el FieldByName de una constante regresa Cero.
     Ejemplo, en el query tenemos:

     100.63 COLUMNA8

     al hacer el FielByName('COLUMNA8') regresa CERO

     si tenemos

     100.63 * 1.0 COLUMNA8

     regresa el valor correcto.

     Se usa el 1.0 (Valor Flotante) y NO 1 (Valor entero),
     para que los enteros los convierta a Flotantes }

     sValor := ZetaServerTools.FormateaFlotanteEvaluador( rValor );

     for i:= 0 to High(aTablas) do
     begin
          Result := StrTransAll( Result, aTablas[i] + '.' + sCampo, sValor );
     end;
end;

function TRetroactivo.TransformaFormula( const sFormula : string ): string;
begin
     Result := sFormula;
     Result := TransformaCampo( Result, 'CB_SALARIO', FSalarioNuevo );
     Result := TransformaCampo( Result, 'CB_SAL_INT', FSalIntNuevo );
end;

function TRetroactivo.SQLTransformado;

{var
   sSalario,sSalInt : string;}
begin
     {PENDIENTE el usar El formateo directo que da la funcion FORMAT, enves del FormatToStr
     El FormatToStr regresa numero entero pero necesitamos que el dato quede con dos decimales.
     sSalario := Format( '%s * 1', [FloatToStr( FSalarioNuevo )] );
     sSalInt := Format( '%s * 1', [FloatToStr( FSalIntNuevo )] );  }

     {sSalario := Format( K_FACTOR_CAST, [ FloatToStr( FSalarioNuevo ) ] );
     sSalInt := Format( K_FACTOR_CAST, [ FloatToStr( FSalIntNuevo ) ] );}

     {sSalario := FormatFloat( K_FORMAT_FLOAT, FSalarioNuevo );
     sSalInt := FormatFloat( K_FORMAT_FLOAT, FSalIntNuevo );}

     Result := SQLOriginal;
     //Result := ZetaServerTools.TransformaCampo( Result, 'CB_SALARIO', FSalarioNuevo );
     //Result := ZetaServerTools.TransformaCampo( Result, 'CB_SAL_INT', FSalIntNuevo );

     Result := TransformaFormula( Result );
     //Result := StrTransAll( Result, K_SALARIO_DUMMY, 'CB_SALARIO' );
     //Result := StrTransAll( Result, K_SALINT_DUMMY, 'CB_SAL_INT' );
end;


function TRetroactivo.FormateaFloat( const rValor : TPesos ): string;
begin
     Result := PadL( FormatFloat( '$#,0.00;-$#,0.00', rValor ), 10 );
end;

function TRetroactivo.TieneNomina(const iEmpleado: integer ): Boolean;
begin
     oZetaProvider.ParamAsInteger( FQTieneNomina, 'Empleado', iEmpleado );
     with FQTieneNomina do
     begin
          Active := TRUE;
          Result := FieldByName('CUANTOS').AsInteger > 0 ;
          Active := FALSE;
     end;
end;

procedure TRetroactivo.CalculaRetroactivo( oListaCalculados : TStringList );
 var
    rRetroSemana, rRetroMonto, rMonto, rDiferencia : TPesos;
    i, iConcepto, iPtr : integer;
    lDeduccion : Boolean;
    sSigno: string;
begin
     oZetaProvider.InitArregloTPeriodo;
     FRastreo.RastreoBegin; //Inicializa el Rastreo;
     with FDataset do
     begin
          if NOT Locate('CB_CODIGO', FEmpleado, [] ) then
          begin
               Append;
               FieldByName('CB_CODIGO').AsInteger := FEmpleado;
               Post;

               with FRastreo do
               begin
                    RastreoTextoLibre( Format( 'Empleado %d: %s', [ FEmpleado, FNombre] ) );
                    RastreoTextoLibre( PadL( Format( 'Salario Diario Nuevo: %s', [FormateaFloat( FSalarioNuevo)] ) , 36 ) );
                    RastreoTextoLibre( PadL( Format( 'Salario Integrado Nuevo: %s', [FormateaFloat( FSalIntNuevo)] ), 36 ) );
                    RastreoEspacio;
               end;
          end;
     end;

     with FRastreo do
     begin
          with oZetaProvider.DatosPeriodo do
               RastreoTextoLibre( GetPeriodoInfo( Year, Numero, Tipo ) );
          RastreoTextoLibre( PadL( Format( 'Salario Anterior: %s', [FormateaFloat( FSalarioNomina )] ) , 36 ) );
     end;

     oZetaProvider.ParamAsInteger( FQConcepto, 'Empleado', FEmpleado );
     rRetroSemana := 0;
     for i:= 0  to FRetroConceptos.Count - 1 do
     begin
          iConcepto := StrToInt( FRetroConceptos[i] );

          with FQConcepto do
          begin
               Active := FALSE;
               oZetaProvider.ParamAsInteger( FQConcepto, 'Concepto', iConcepto );
               Active := TRUE;

               rMonto := FieldByName('RESULTADO').AsFloat;
               Active := FALSE;
          end;

          with oListaCalculados do
          begin
               iPtr := IndexOf(FRetroConceptos[i]+'@');
               if ( iPtr >= 0 ) then
               begin
                    with TConceptoCalculado( Objects[iPtr] ) do
                    begin
                         rRetroMonto := Monto;
                         //eTipo := Concepto.Tipo;
                         {if Concepto.Tipo in TiposDeduccion then
                            rRetroMonto := rRetroMonto * (-1);}
                         lDeduccion := Concepto.Tipo in TiposDeduccion;

                    end;
               end
               else
               begin
                    lDeduccion := FALSE;
                    rRetroMonto := 0;
               end;
          end;

          rDiferencia := rRetroMonto - rMonto;

          if lDeduccion then sSigno := '-'
          else sSigno := '+';

          FRastreo.RastreoTextoLibre( Format( 'C(%s)=  Ant: %s  Nvo: %s  Dif: %s %s', [ PadL( IntToStr( iConcepto), 3), FormateaFloat( rMonto ), FormateaFloat( rRetroMonto ), sSigno, FormateaFloat( rDiferencia ) ] ) );

          if lDeduccion then
             rDiferencia := rDiferencia * (-1);

          rRetroSemana := rRetroSemana + rDiferencia;

     end;

     with FDataset do
     begin
          Edit;
          //FieldByName( 'CO_TIPO' ).AsInteger := Ord( eTipo );
          with FieldByName('RETROACTIVO') do
          begin
               AsFloat := AsFloat + rRetroSemana;
          end;

          with FRastreo do
          begin
               //pendiente
               //implementar el GetPeriodoData
               RastreoTextoLibre( PadL(Format( 'Total de la N�mina #%d: = %s', [ oZetaProvider.DatosPeriodo.Numero, FormateaFloat( rRetroSemana ) ] ) ,60 ) );
               RastreoEspacio;

          end;

          with FieldByName('RASTREO') do
          begin
               AsString := AsString + FRastreo.Rastro;
          end;
          Post;
     end;
end;

procedure TRetroactivo.GrabaExcepciones;
 var
    oNomina : TNomina;
    iEmpleado: integer;
    sRastro: string;
    rMonto : TPesos;
    lPercepcion : Boolean;
begin
     with oZetaProvider do
     begin
          InitArregloTPeriodo;
          InicializaValoresActivos;
          ParamList.AddInteger( 'Numero', FNominaAPagar );
          ParamList.AddInteger( 'Year', FYearAPagar );
          GetDatosPeriodo;

          with DatosPeriodo do
          begin
               if Status > spCalculadaTotal then
               DatabaseError( 'La N�mina ' + ZetaCommonTools.GetPeriodoInfo( Year, Numero, Tipo ) + ' No Puede Ser Calculada' + CR_LF +
                              'El Status de la N�mina es ' + ObtieneElemento( lfStatusPeriodo, Ord(Status) ) );

          end;
     end;

     with FQTipoConcepto do
     begin
          Active := TRUE;
          lPercepcion := ( eTipoConcepto(FieldByName('CO_TIPO').AsInteger) in TiposPercepcion );
          Active := FALSE;
     end;

     oNomina := TNomina.Create( oZetaCreator );
     try
        with oNomina do
        begin
             VerificaNominaBegin;
             ExcepcionMontoBegin;
             try
                with FDataSet do
                begin
                     First; //Se tiene que hacer el first, xq esta posicionado en el ultimo registro, por todos los appends que se le hicieron.
                     while NOT EOF do
                     begin
                          iEmpleado := FieldByName('CB_CODIGO').AsInteger;

                          sRastro := FieldByName( 'Rastreo' ).AsString +
                                     Padl( Format( 'Retroactivo Total de las N�minas = %s', [ FormateaFloat( FieldByName('RETROACTIVO').AsFloat ) ] ), 60 );

                          rMonto := FieldByName('RETROACTIVO').AsFloat;
                          
                          if ( oZetaProvider.ParamList.ParamByName('IncluyeAnio').AsBoolean ) and ( FYearAd = 1 ) OR ( ( NOT oZetaProvider.ParamList.ParamByName('IncluyeAnio').AsBoolean ) and ( FYearAd = 0 ) ) then
                          begin
                               if PesosIguales( rMonto, 0 )  then
                               begin
                                    oZetaProvider.Log.Evento( clbNinguno, iEmpleado, Date, 'Rastreo del C�lculo de Retroactivo',  sRastro );
                               end
                               else
                               begin
                                    oZetaProvider.EmpiezaTransaccion;
                                    try
                                       if not lPercepcion then
                                          rMonto := rMonto * (-1);

                                       if ( NOT lPercepcion ) OR (rMonto <> 0) then
                                       begin
                                            VerificaRegistro( iEmpleado, TRUE );
                                            ExcepcionMonto( iEmpleado, FConcepto, VACIO, rMonto, FExcepcion, lPercepcion );
                                            oZetaProvider.Log.Evento( clbNinguno, iEmpleado, Date, 'Rastreo del C�lculo de Retroactivo',  sRastro );
                                       end
                                       else
                                       begin
                                            oZetaProvider.Log.Advertencia( iEmpleado, 'Retroactivo Calculado Negativo',  sRastro );
                                       end;
                                    except
                                          on Error: Exception do
                                          begin
                                               oZetaProvider.Log.Excepcion( iEmpleado, 'Error Al Agregar Excepci�n de Monto', Error );
                                          end;
                                    end;
                                    oZetaProvider.TerminaTransaccion( True );
                               end;
                          end;
                          Next;
                     end;
                end;
            finally
                   CalculaStatusPeriodo;

                   ExcepcionMontoEnd;
                   VerificaNominaEnd;
            end;
        end;
     finally
            FreeAndNil(oNomina);
     end;
end;


function TRetroactivo.GetScript( const iScript : integer ): string;
begin
     case iScript of
          Q_RETRO_CONCEPTO:
                           {$ifdef INTERBASE}
                           Result := 'SELECT RESULTADO from SP_C( :Concepto, :Numero, :Tipo, :Year, :Empleado )';
                           {$else}
                           Result := 'SELECT RESULTADO = dbo.SP_C( :Concepto, :Numero, :Tipo, :Year, :Empleado )';
                           {$endif}

          Q_RETRO_TIENE_NOMINA:
                               Result := ' select COUNT(*) CUANTOS from NOMINA N '+
                                         'where (N.PE_YEAR = :Year ) '+
                                         'and ( N.PE_TIPO = :Tipo ) '+
                                         'and ( N.PE_NUMERO = :Numero ) '+
                                         'and ( N.CB_CODIGO = :Empleado )';
          Q_RETRO_SALARIO:
                          Result := ' select N.CB_SALARIO from NOMINA N '+
                                    'where (N.PE_YEAR = :Year ) '+
                                    'and ( N.PE_TIPO = :Tipo ) '+
                                    'and ( N.PE_NUMERO = :Numero ) '+
                                    'and ( N.CB_CODIGO = :Empleado )';
          Q_RETRO_TIPO_CONCEPTO:
                                Result := 'select CO_TIPO from CONCEPTO where CO_NUMERO = :Numero'; 
     end;
end;

procedure TRetroactivo.SetNominaActiva(const Value: integer);
begin
     FNominaActiva := Value;
     with oZetaProvider.ParamList do
     begin
          AddInteger( 'Numero', FNominaActiva );
          if FYearAd = 0 then
             FYear := ParamByName( 'NominaYear' ).AsInteger
          else
             FYear := ParamByName( 'NominaYearAd' ).AsInteger;
          AddInteger( 'Year', FYear );
     end;

     PreparaQuerys;
end;

procedure TRetroActivo.SetDatosEmpleado( oDataSet : TDataSet );
begin
     with oDataSet do
     begin
          FEmpleado := FieldByName('CB_CODIGO').AsInteger;
          FNombre := FieldByName('PRETTYNAME').AsString;
          FSalarioNuevo := ZetaCommonTools.Redondea( FieldByName('CB_SALARIO').AsFloat );
          FSalIntNuevo := ZetaCommonTools.Redondea( FieldByName('CB_SAL_INT').AsFloat );
     end;

     oZetaProvider.ParamAsInteger( FQSalario, 'Empleado', FEmpleado );
     with FQSalario do
     begin
          Active := TRUE;
          FSalarioNomina := ZetaCommonTools.Redondea( FieldByName('CB_SALARIO').AsFloat );
          Active := FALSE;
     end;
end;

{ *************************** TConceptoCalculado ************************* }
constructor TConceptoCalculado.Create;
begin
     inherited;
	Activo := TRUE;
	ConISPT := FALSE;
	ISPT := 0;
	UsuarioMod := 0;
	Referencia := '';
	Exento := 0;
        Monto := 0;
        ImpuestosOK := TRUE;
        Siguiente := NIL;
        SumaTotales := TRUE;
end;

{ TConceptoNomina }

procedure TConceptoNomina.AplicaMovimiento( const oMovimien: TConceptoCalculado);
begin
    FStatus := stcCalculado;

    // Pendiente calcular impuestos para percepciones manuales y forzar Status = stCalculado
    if ( Tipo in [coPercepcion, coPrestacion] ) then
       CalculaImpuestos( oMovimien );

    // Pudo haber pendientes en el c�lculo de impuestos
    // Aunque los manuales se procesan a pesar de lo pendiente
    if ( oMovimien.ImpuestosOK ) OR ( oMovimien.UsuarioMod > 0 ) then
    with DataModule as TCalcNomina do
    begin
        oMovimien.Siguiente := Movimiento;
        Movimiento := oMovimien;
        oListaCalculados.AddObject( IntToStr( Numero ) + '@' + oMovimien.Referencia, oMovimien );

        if ( oMovimien.Activo ) AND ( oMovimien.Monto <> 0 ) then
        begin
             FMonto := FMonto + oMovimien.Monto;
             if oMovimien.SumaTotales then
                    SumaTotalesMovimiento( self, oMovimien, 1 );
        end;
    end;
end;


procedure TConceptoNomina.CalculaImpuestos( const oMovimien: TConceptoCalculado);
var
   lFormulaOK : Boolean;
begin
    oMovimien.ImpuestosOK := TRUE;
    if ( GravaISPT ) then
    begin
      TCalcNomina( DataModule ).nMontoFormula := oMovimien.Monto;

      // Parte Exenta de ISPT
      if ( FEvaluadorExentoISPT = NIL ) then
              oMovimien.Exento := 0
      else
      begin
              // Por si se hace referencia en FORMULA
              oMovimien.Exento := rMax( rMin( EvaluaFormula( FEvaluadorExentoISPT, lFormulaOK ), oMovimien.Monto ), 0 );
              if NOT lFormulaOK then
                 oMovimien.ImpuestosOK := FALSE;
      end;

      // Si tiene su propia f�rmula de ISPT
      oMovimien.ConISPT := ( FEvaluadorISPTInd <> NIL );
      if oMovimien.ConISPT then
      begin
              // Por si se hace referencia en FORMULA
              TCalcNomina( DataModule ).nExentoFormula := oMovimien.Exento;
              oMovimien.ISPT := rMax( rMin( EvaluaFormula( FEvaluadorISPTInd, lFormulaOK ), oMovimien.Monto ), 0 );
              if NOT lFormulaOK then
                 oMovimien.ImpuestosOK := FALSE;
      end
      else
              oMovimien.ISPT := 0;
    end
    else
      with oMovimien do
      begin
              Exento := Monto;
              ConISPT := FALSE;
              ISPT := 0;
      end;
end;

procedure TConceptoNomina.Calcular;
var
   nMontoCalculado : TPesos;
   lFormulaOK : Boolean;
begin
     if ( Global ) then
        Registrar( TopeMonto( MontoGlobal ), '', TRUE )
     else  // Si no se cumple la condici�n para este empleado, el concepto se marca calculado con monto = 0
     begin
	         if (  NOT StrVacio( Condicion )) AND ( NOT EvaluaQuery ) then
              Registrar( 0, '', TRUE )
          else
          begin
               nMontoCalculado := TopeMonto( EvaluaFormula( FEvaluador, lFormulaOK ));
               if ( lFormulaOK ) then            // Si no hubo factor pendiente
                  Registrar( nMontoCalculado, '', TRUE );
          end;
     end;
end;

function TConceptoNomina.CalcularPiramidacion: TPesos;
var
   nMontoCalculado : TPesos;
   lFormulaOK : Boolean;
begin
     if (  NOT StrVacio( Condicion )) AND ( NOT EvaluaQuery ) then
         Result :=  0
     else
     begin
          nMontoCalculado := TopeMonto( EvaluaFormula( FEvaluador, lFormulaOK ));
          if ( lFormulaOK ) then            // Si no hubo factor pendiente
             Result :=  nMontoCalculado
          else
              Result := 0;
     end;
end;

procedure TConceptoNomina.CreaMovimiento(const nMonto: TPesos; sReferencia: String);
var
   oMovimien : TConceptoCalculado;
begin
    oMovimien := TConceptoCalculado.Create;
    oMovimien.Concepto := self;
    oMovimien.Monto := nMonto;
    oMovimien.Referencia := sReferencia;
    AplicaMovimiento( oMovimien );
end;

destructor TConceptoNomina.Destroy;
begin
     FEvaluador.Free;
     FEvaluadorQuery.Free;
     FEvaluadorISPTInd.Free;
     FEvaluadorExentoISPT.Free;
     inherited Destroy;
end;

function TConceptoNomina.EsDeduccion: Boolean;
begin
     Result := Tipo in [ coDeduccion, coObligacion, coResultados ];
end;

function TConceptoNomina.EvaluaFormula(oEvaluador: TZetaEvaluador; var lFormulaOK: Boolean): TPesos;
var
   EVResultado : TQREvResult;
begin
     lFormulaOK := TRUE;
     Result := 0;
     if oEvaluador <> NIL then
     begin
          TCalcNomina( DataModule ).lPendiente := FALSE;
          EVResultado := oEvaluador.Value;
          //if EvResultado.Kind = resError then
          //   raise Exception.create('Error en Concepto#:'+IntToStr( Numero ));
          try
             ValidaError( EvResultado, 'Error en Concepto #' + IntToStr( Numero ) );
             case EVResultado.Kind of
                  resInt : Result := EVResultado.intResult;
                  resDouble : Result := Redondea( EVResultado.dblResult );
             end;
          except
                On E:Exception do
                   Raise Exception.Create( 'Error Al Evaluar Concepto # ' +
                                           IntToStr(Numero) + CR_LF +
                                           E.Message);
          end;

          if TCalcNomina( DataModule ).lPendiente then
          begin
               lFormulaOK := FALSE;
               Result := 0;
          end;
     end;
     // El resultado debe ser redondo a 2 decimales
     // Si hay alg�n factor pendiente y la f�rmula no se puede calcular, marcar Status := stcPendiente, regresar 0
end;

function TConceptoNomina.EvaluaQuery : Boolean;
var
   EVResultado: TQREvResult;
begin
     Result := False;
     if ( FEvaluadorQuery <> NIL ) then
     begin
          EVResultado := EvaluadorQuery.Value;
          ValidaError( EvResultado, 'Error en Condici�n del Concepto #' + IntToStr( Numero ) );
          if EVResultado.Kind = resBool then
             Result := EVResultado.BooResult;
     end;
end;

procedure TConceptoNomina.MarcarCalculado(nMonto: TPesos);
begin
     FStatus := stcCalculado;
     FMonto := nMonto;
end;

procedure TConceptoNomina.PreparaEvaluadores;

  procedure PreparaUnEvaluador( oEvaluador : TZetaEvaluador; const iColumna : Integer );
  var
     sTransformada : String;
  begin
       if ( oEvaluador <> NIL ) then
         with DataModule as TCalcNomina do
         begin
            // En la 'Formula' guardo la posici�n de la columna en el agente
            sTransformada := SQLBroker.Agente.GetColumna( iColumna ).Formula;
            oEvaluador.AgregaDataset( FQEmpleado );
            oEvaluador.Prepare( sTransformada );
         end;
  end;

begin { PreparaEvaluadores }
     PreparaUnEvaluador( FEvaluador, FColFormula );
     PreparaUnEvaluador( FEvaluadorExentoISPT, FColExento );
     PreparaUnEvaluador( FEvaluadorISPTInd, FColISPT );
     PreparaUnEvaluador( FEvaluadorQuery, FColQuery );
end;

procedure TConceptoNomina.Registrar(const nMonto: TPesos;
  sReferencia: String; Obliga: Boolean);
begin
     if ( Obliga ) OR ( FStatus <> stcCalculado ) then
     begin
        if ( nMonto = 0 ) OR ( Tipo = coCalculo ) then      // Solo marca 0. No almacena
           MarcarCalculado( nMonto )
        else
            CreaMovimiento( nMonto, sReferencia );
     end;
end;

procedure TConceptoNomina.Reset;
begin
    if CalcularSiempre then
       FStatus := stcPendiente
    else
        FStatus := stcDormido;
    FMonto := 0;
    Movimiento := NIL;
end;

procedure TConceptoNomina.ResetRastreo;
begin
     if ( Evaluador <> NIL ) and ( Evaluador.RastreandoNomina ) then
     begin
          Evaluador.Rastreador.RastreoBegin;
     end;
end;

procedure TConceptoNomina.RegistrarRastreo(const lExcepcion: Boolean);
begin
     if ( Evaluador <> NIL ) and ( Evaluador.RastreandoNomina ) then
     begin
          TCalcNomina(DataModule).SetRastroNomina( Self, Evaluador.Rastreador.Rastro, lExcepcion );
     end;
end;

function TConceptoNomina.TopeMonto(const nMonto: TPesos): TPesos;
begin
    //  Si es deducci�n y NETO = 0, Monto se hace 0
    if  ( Tipo = coDeduccion ) AND ( nMonto > 0 ) AND ( TCalcNomina( DataModule ).Neto = 0 ) then
            Result := 0
    else
            Result := nMonto;
end;

procedure TCalcNomina.CalculaNominaParametros;
begin
     with oZetaProvider, DatosPeriodo do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Per�odo: ' + ZetacommonTools.ShowNomina( Year, Ord( Tipo ), Numero ) +
                         K_PIPE + 'Tipo C�lculo: ' + ObtieneElemento( lfTipoCalculo, Ord( ParamList.ParamByName( 'TipoCalculo' ).AsInteger ) ) +
                         K_PIPE + 'Status Inicial: ' + ObtieneElemento( lfStatusPeriodo, Ord( Status ) ) +
                         K_PIPE + 'Muestra Tiempos: ' + zBoolToStr( ParamList.ParamByName( 'MuestraTiempos' ).AsBoolean );
     end;
end;


procedure TCalcNomina.InitParametros(Empresa, Parametros: OleVariant);
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList(Parametros);
          GetDatosActivos;                    // Por si existen conceptos con funciones que requieren Activos y no tienen un GetDatosActivos
          GetDatosPeriodo;
          FDatosPeriodo := DatosPeriodo;
          GetParametrosCalculo( Parametros );
     end;
end;

procedure TCalcNomina.PreparaNomina(const TipoCalculo: eTipoCalculo);
begin
     InitBroker;
     FCreator.PreparaSalMin;
     FCreator.PreparaValUma;
     FCreator.PreparaRastreoNomina;
     oZetaProvider.InitGlobales;

     FUsuario := oZetaProvider.UsuarioActivo;
     if TipoCalculo = tcNueva then
     begin
        PreparaNominaCreaLista;
        PreparaNominaDataSet;
     end;
end;

{$define CUENTA_EMPLEADOS}

{$ifdef VALIDAEMPLEADOSGLOBAL}
procedure TCalcNomina.ValidaStatusNomina( var sMensajeAdvertencia : string );
var
   sMensajeError: String;
begin

     sMensajeAdvertencia := VACIO;
     sMensajeError := VACIO;

     oZetaProvider.InitArregloTPeriodo;
     if FDataSetLista.IsEmpty then
        sMensajeError :=  Format('No Hay Empleados En La N�mina de %s', [GetPeriodoData] );

     if StrVacio( sMensajeError ) then
     begin
         if ( FNumero < oZetaProvider.GetGlobalInteger( K_GLOBAL_LIMITE_NOMINAS_ORDINARIAS ) ) then
         begin
               FCreator.ValidaLimiteEmpleados( AutoServer, evOpNomina,  sMensajeAdvertencia, sMensajeError   );
         end;
     end;

     with oZetaProvider do
     begin
          if StrLleno ( sMensajeError ) then
             DatabaseError(sMensajeError);


     end;

end;
{$else}

procedure TCalcNomina.ValidaStatusNomina;
var
   {$ifdef CUENTA_EMPLEADOS}
   sMensaje: String;
   {$else}
   iNumEmpleados: integer;
   {$endif}
begin
     oZetaProvider.InitArregloTPeriodo;
     if FDataSetLista.IsEmpty then
        DataBaseError( Format('No Hay Empleados En La N�mina de %s', [GetPeriodoData] ) );
     {
     Query para obtener el numero de empleados activos en el sistema
     Condicion anterior( FDataSetLista.RecordCount > FMaxEmpleados )
     }
     {$ifdef CUENTA_EMPLEADOS}
     //if ( FNumero < K_LIMITE_NOM_NORMAL ) then
     //oZetaProvider.InitGlobales;
     if ( FNumero < oZetaProvider.GetGlobalInteger( K_GLOBAL_LIMITE_NOMINAS_ORDINARIAS ) ) then

     begin
          {$ifndef CAROLINA}
          if not FCreator.ValidaLimiteEmpleados( AutoServer, sMensaje ) then
             DatabaseError( sMensaje );
          {$endif}
     end;
     {$else}
     iNumEmpleados := FCreator.GetMaxEmpleadosActivos( FDatosPeriodo.Inicio );
     {$ifndef CAROLINA}
     //if ( FNumero < K_LIMITE_NOM_NORMAL ) and ( iNumEmpleados > FMaxEmpleados ) then { Anteriormente se tenia FDataSetLista.RecordCount, FMaxEmpleados }
     //oZetaProvider.InitGlobales;
     if ( FNumero < oZetaProvider.GetGlobalInteger( K_GLOBAL_LIMITE_NOMINAS_ORDINARIAS ) ) and ( iNumEmpleados > FMaxEmpleados ) then { Anteriormente se tenia FDataSetLista.RecordCount, FMaxEmpleados }
        DataBaseError( Format( 'Intentando Proceso Para %d Empleados' + CR_LF + 'Se Excedi� El l�mite De %d Empleados Autorizados', [ iNumEmpleados, FMaxEmpleados ] ) );
     {$ENDIF}
     {$endif}
end;

{$endif}

procedure TCalcNomina.ValidaNominaAnterior;
var
   iPeriodoAnterior: integer;
   StatusAnterior : eStatusPeriodo;
begin
     with oZetaProvider do
     begin
          {Validacion de la Nomina Anterior}
          if ( DatosPeriodo.Numero > 1 ) AND ( eUsoPeriodo( DatosPeriodo.Uso )= upOrdinaria  )then
          begin
               iPeriodoAnterior := DatosPeriodo.Numero-1;
               AbreQueryScript( FQGeneral, Format( GetScript( K_QRY_STATUS_ANTERIOR ), [ FYear, FTipo, iPeriodoAnterior ] ));
               if not FQGeneral.Eof then  { Si existe el per�odo }
               begin
                 StatusAnterior := eStatusPeriodo( FQGeneral.Fields[0].AsInteger );
                 {Cuando la nomina anterior es nueva, si se permite: Ej. Implantaci�n de sistema a mitad de a�o }
                 if ( StatusAnterior <> spNueva ) and ( StatusAnterior < spAfectadaTotal ) then
                    DatabaseError( 'La N�mina ' + IntToStr(iPeriodoAnterior) + CR_LF + 'No Ha Sido Afectada' );
               end;
          end;
     end;
end;

{function TCalcNomina.CalculaRetroactivo(Empresa, Parametros: OleVariant ): OleVariant;
var
    TipoCalculo : eTipoCalculo;
    iPeriodoAnterior: Integer;
    StatusAnterior : eStatusPeriodo;
begin
     //CalculaNomina
     InitParametros(Empresa,Parametros);

     with oZetaProvider do
     begin
          {Validacion de la Nomina Actual}
{          with DatosPeriodo do
          begin
               if ( Status < spCalculadaTotal ) then
                  DatabaseError( Format('La N�mina De %s No Ha Sido Calculada Totalmente',[GetPeriodoData])  );
          end;
          ValidaNominaAnterior;
     end;

     PreparaNomina;

     Result := CalculaRetroactivoDataset(  CalculaRetroactivoBuildDataSet );
end;
 }
function TCalcNomina.CalculaNomina(Empresa, Parametros: OleVariant ): OleVariant;
var
    TipoCalculo : eTipoCalculo;
    //iPeriodoAnterior: Integer;
    //StatusAnterior : eStatusPeriodo;

begin
     //CalculaNomina
     InitParametros(Empresa,Parametros);
     oZetaProvider.InitArregloTPeriodo;
     CalculaNominaParametros;

     {Validacion de la Nomina Actual}
     with oZetaProvider.DatosPeriodo do
     begin
          if(  eStatusPeriodo( ObtenerStatusDelPeriodo( Year, Tipo, Numero )  ) in [ spAfectadaTotal ] ) then
               DatabaseError( 'La N�mina ya fu� Afectada.' + CR_LF + 'No se puede volver a Calcular' );
     end;

     TipoCalculo := eTipoCalculo( FParametros.ParamByName( 'TipoCalculo' ).AsInteger );

     ValidaNominaAnterior;
     PreparaNomina(TipoCalculo);

     with FParametros do
     begin
          CalculaNominaCreaLista( SQLBroker.GetTablaRangoLista( ParamByName( 'RangoLista' ).AsString, enNomina ),
                                  ParamByName( 'Filtro' ).AsString,
                                  ParamByName( 'Condicion' ).AsString,
                                  TipoCalculo );
     end;

     Result := CalculaNominaDataset;
end;

function TCalcNomina.ObtenerStatusDelPeriodo( Year, Tipo, Numero: Integer): eStatusPeriodo;
const
     Q_ENCABEZADO_PERIODO = 1;
var
   FConsultaPeriodo: TZetaCursor;
   function GetSQLScript( const iScript: Integer ): String;
   begin
        case iScript of
             Q_ENCABEZADO_PERIODO : Result := 'select PE_STATUS, PE_FEC_INI, PE_FEC_FIN, PE_TIMBRO from PERIODO where '+
                                           '( PE_YEAR = :Year ) and ( PE_TIPO = :Tipo ) and ( PE_NUMERO = :Numero )';
        end;
   end;
begin
     with oZetaProvider, oZetaProvider.ParamList do
     begin
          Result := spAfectadaTotal;
          FConsultaPeriodo := CreateQuery( GetSQLScript( Q_ENCABEZADO_PERIODO ) );
          try
             // Periodo Original
             ParamAsInteger( FConsultaPeriodo, 'Year', Year );
             ParamAsInteger( FConsultaPeriodo, 'Tipo', Tipo );
             ParamAsInteger( FConsultaPeriodo, 'Numero', Numero );
             with FConsultaPeriodo do
             begin
                  Active := TRUE;
                  if EOF then
                     DataBaseError( 'Error al consultar periodo.' )
                  else
                  begin
                       Result := eStatusPeriodo ( FieldByName( 'PE_STATUS' ).AsInteger );
                  end;
                  Active := FALSE;
             end;
          finally
                 FreeAndNil(FConsultaPeriodo);
          end;
     end;
end;

procedure TCalcNomina.PreparaNominaCreaLista;
begin

{
'select COLABORA.CB_CODIGO, COLABORA.CB_APE_PAT||'' ''||COLABORA.CB_APE_MAT||'', ''||COLABORA.CB_NOMBRES as PrettyName, @REQUERIDOS '+
'COLABORA.CB_FEC_ING, COLABORA.CB_ACTIVO, COLABORA.CB_FEC_BAJ, COLABORA.CB_NOMYEAR, COLABORA.CB_NOMTIPO, '+
'COLABORA.CB_NOMNUME, TURNO.TU_NOMINA, NOMINA.CB_CODIGO as EXISTE, COLABORA.CB_CHECA, NOMINA.NO_FUERA '+
'from COLABORA join TURNO on '+
'( CB_TURNO = TU_CODIGO ) '+
'left join NOMINA on '+
'( NOMINA.PE_YEAR = :Year ) and '+
'( NOMINA.PE_TIPO = :Tipo ) and '+
'( NOMINA.PE_NUMERO = :Numero ) and '+
'( NOMINA.CB_CODIGO = COLABORA.CB_CODIGO ) @WHERE_CONDICION '+
'order by COLABORA.CB_CODIGO';
}
     with SQLBroker do
     begin
          Init( enEmpleado );
          with Agente do
          begin
               AgregaColumna( 'COLABORA.CB_CODIGO', TRUE, enEmpleado, tgNumero, 0 );
               with FParametros do
               begin
                    AgregaFiltro( GetTablaRangoLista( ParamByName( 'RangoLista' ).AsString, enEmpleado ), TRUE, enEmpleado );
                    AgregaFiltro( ParamByName( 'Filtro' ).AsString, FALSE, enEmpleado );
                    AgregaFiltro( ParamByName( 'Condicion' ).AsString, TRUE, enEmpleado );
               end;
               //FiltroConfidencial( oZetaProvider.EmpresaActiva );   ... Mismo caso de Prenomina, se deben crear todas las nominas ( Problema de Status de Periodo )
               AgregaOrden( 'COLABORA.CB_CODIGO', TRUE, enEmpleado );
          end;
          if SuperSQL.Construye( Agente ) then
             SuperReporte.Construye( Agente )
          else
               DataBaseError( Agente.ListaErrores[ 0 ] );
     end;
end;

function TCalcNomina.PreparaNominaDataset: OleVariant;
var
   iEmpleado: Integer;
begin
     FDataSetLista := SQLBroker.SuperReporte.DataSetReporte;
     if FDataSetLista.IsEmpty then
        DataBaseError( 'No Hay Empleados En Ese Rango' );
     iEmpleado := 0;
     with oZetaProvider do
     begin
          with FDataSetLista do
          begin
               if OpenProcess( prNOCalcularNueva, RecordCount, FListaParametros ) then
               begin
                    try
                       InitNomina;
                       with Nomina do
                       begin
                            NominaPreparaBegin;
                            iEmpleado := Fields[ 0 ].AsInteger;
                            while not Eof and CanContinue( iEmpleado ) do
                            begin
                                 EmpiezaTransaccion;
                                 try
                                    if NominaPrepara( iEmpleado ) then
                                       NominaTotaliza( iEmpleado );
                                    TerminaTransaccion( True );
                                 except
                                       on Error: Exception do
                                       begin
                                            TerminaTransaccion( False );
                                            Log.Excepcion( iEmpleado, 'Error al Preparar C�lculo de N�mina', Error );
                                       end;
                                 end;
                                 Next;
                                 iEmpleado := Fields[ 0 ].AsInteger;
                            end;
                            NominaPreparaEnd;
                       end;
                    except
                          on Error: Exception do
                          begin
                               Log.Excepcion( iEmpleado, 'Error al Preparar C�lculo de N�mina', Error );
                          end;
                    end;
               end;
               ClearNomina;
               Result := CloseProcess;
               if ( eProcessStatus( Result[ 1 ] ) <> epsOK ) then
                 DataBaseError( 'Errores Al Preparar N�mina Nueva' );
          end;
     end;
end;

procedure TCalcNomina.RastreaConceptoParametros;
begin
     with oZetaProvider, ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Periodo: ' + ZetacommonTools.ShowNomina( DatosPeriodo.Year, Ord( DatosPeriodo.Tipo ), DAtosPeriodo.Numero ) +
                         K_PIPE + 'Empleado: ' + ParamByName( 'Empleado' ).AsString + ' - ' + ParamByName( 'Nombre' ).AsString +
                         K_PIPE + 'Concepto: ' + ParamByName( 'NumeroConcepto' ).AsString + ' - ' + ParamByName( 'DesConcepto' ).AsString +
                         K_PIPE + 'Archivo: ' + ParamByName( 'Archivo' ).AsString +
                         K_PIPE + 'Parametros N�mina: ' + ZetaCommonTools.zBoolToStr( ParamByName( 'MostrarNomParam' ).AsBoolean );
          FListaFormulas :=        VACIO;
          FListaFormulas :=        K_PIPE + 'F�rmula Concepto: ' + ParamByName( 'Formula' ).AsString;
     end;
end;

function TCalcNomina.RastreaConcepto(Empresa, Parametros: OleVariant;  out Rastreo: OleVariant): OleVariant;
var
    iEmpleado, iConcepto : Integer;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList(Parametros);
          GetDatosPeriodo;
          FDatosPeriodo := DatosPeriodo;
          GetParametrosCalculo( Parametros );
          InitArregloTPeriodo;          
     end;

     RastreaConceptoParametros;
     InitBroker;
     with FCreator do
     begin
          PreparaSalMin;
          PreparaRastreo; { Prende el Rastreador }
     end;
     oZetaProvider.InitGlobales;
     try
        FUsuario := oZetaProvider.UsuarioActivo;
        with FParametros do
        begin
           iEmpleado := ParamByName( 'Empleado' ).AsInteger;
           iConcepto := ParamByName( 'NumeroConcepto' ).AsInteger;
        end;
        CalculaNominaCreaLista( '', Format( 'CB_CODIGO = %d', [ iEmpleado ] ), '', tcRango, TRUE );
        Result := RastreaConceptoDataSet( iConcepto, Rastreo );
     finally
            FCreator.DesPreparaRastreo;
     end;
end;

function TCalcNomina.RastreaConceptoDataSet(const iConcepto: Integer; out Rastreo : OleVariant): OleVariant;
var
    oConcepto : TConceptoNomina;
    lExcepcion, lFormulaOK : Boolean;

    procedure LeeTotales;
    begin
         with FQEmpleado do
         begin
           nNomina_NO_PERCEPC  := FieldByName( 'NO_PERCEPC' ).AsFloat;
           nNomina_NO_DEDUCCI  := FieldByName( 'NO_DEDUCCI' ).AsFloat;
           nNomina_NO_X_ISPT   := FieldByName( 'NO_X_ISPT' ).AsFloat;
           nNomina_NO_X_MENS   := FieldByName( 'NO_X_MENS' ).AsFloat;
           nNomina_NO_X_CAL    := FieldByName( 'NO_X_CAL' ).AsFloat;
           nNomina_NO_PER_MEN  := FieldByName( 'NO_PER_MEN' ).AsFloat;
           nNomina_NO_IMP_CAL  := FieldByName( 'NO_IMP_CAL' ).AsFloat;
           nNomina_NO_PER_CAL  := FieldByName( 'NO_PER_CAL' ).AsFloat;
           nNomina_NO_TOT_PRE  := FieldByName( 'NO_TOT_PRE' ).AsFloat;
           nNomina_NO_PREV_GR  := FieldByName( 'NO_PREV_GR' ).AsFloat;
           //nNomina_NO_X_PS    := FieldByName( 'NO_X_PS' ).AsFloat;
         end;
    end;

    procedure MarcaCeros;
    var
        i : Integer;
    begin
        with oListaConceptos do
          for i := Count-1 downto 0 do
            TConceptoNomina( Items[ i ] ).MarcarCalculado( 0 );

        oListaCalculados.Clear;
    end;

    procedure GuardaCalculados;
    begin
         lExcepcion := FALSE;
         AbreQueryEmpleado( FQGetExcepciones );
         with FQGetExcepciones do
          while NOT Eof do
          begin
               oConcepto := GetConcepto( FieldByName( 'CO_NUMERO' ).AsInteger );
               if oConcepto <> NIL then
               begin
                    oConcepto.MarcarCalculado( FieldByName( 'MO_PERCEPC' ).AsFloat +
                                               FieldByName( 'MO_DEDUCCI' ).AsFloat );
                    if ( oConcepto.Numero = iConcepto ) AND
                       ( FieldByName('US_CODIGO').AsInteger > 0 ) then
                       lExcepcion := TRUE;
               end;

               Next;
         end;
    end;

    {Ahora es publico..procedure RastreoNoVacio( const sTexto : String );
    begin
        if StrLleno( sTexto ) then
            RastreoTextoLibre( sTexto );
    end;}

    procedure RastreoTitulo( const sFormula : String );
    begin   // RastreoTitulo
         RastreoTextoLibre( '********      Rastreo del C�lculo de Concepto de N�mina      *********' );
         RastreoEspacio;
         RastreoTextoLibre( 'Concepto: ' + IntToStr( iConcepto ) + ' = ' + FParametros.ParamByName( 'DesConcepto' ).AsString );
         RastreoTextoLibre( 'F�rmula : ' + Copy( sFormula,  1, 60 ));
         RastreoNoVacio(    '          ' + Copy( sFormula, 61, 60 ));
         RastreoNoVacio(    '          ' + Copy( sFormula,121, 60 ));
         RastreoNoVacio(    '          ' + Copy( sFormula,181, 60 ));
         RastreoTextoLibre( 'Empleado: ' + IntToStr( FEmpleado ) + ' = ' + FQEmpleado.FieldByName( 'PRETTYNAME' ).AsString );
         with FDatosPeriodo do
{$ifdef QUINCENALES}
              RastreoTextoLibre( 'Per�odo : ' + ZetaCommonLists.ObtieneElemento( lfTipoNomina, Ord( Tipo ) ) + ' # ' +
                                 IntToStr( Numero ) + ', del ' + FechaCorta( Inicio ) + ' al ' + FechaCorta( Fin ) );
{$else}
         RastreoTextoLibre( 'Per�odo : ' + IntToStr( Numero ) + ', del ' + FechaAsStr( Inicio ) + ' al ' + FechaAsStr( Fin ) );
{$endif}
         RastreoEspacio;
    end;

    procedure RastreoNomParam;
     var i : integer;
    begin
       if  ( FParametros.ParamByName( 'MostrarNomParam' ).AsBoolean ) then
       begin
            RastreoHeader( 'VALORES DE PARAMETROS DE NOMINA' );
            with FQEmpleado do
            begin
                 for i := 0 to FieldCount - 1 do
                   with Fields[ i ] do
                   begin
                      if Pos( 'COLUMNA', FieldName ) <> 1 then
                        case TipoCampo( Fields[ i ] ) of
                            tgNumero, tgFloat : RastreoPesos( FieldName + ': ', AsFloat );
                            tgFecha : RastreoTexto( FieldName + ': ', FechaCorta( AsDateTime ));
                            else RastreoTexto( FieldName + ': ', AsString );
                        end;
                   end;
            end;
       end;
    end;


begin  { RastreaConceptoDataSet }
     FDataSetLista := SQLBroker.SuperReporte.DataSetReporte;
     if FDataSetLista.IsEmpty then
        DataBaseError( 'La N�mina de ese Empleado NO existe' );

     with oZetaProvider do
     begin
          with FDataSetLista do
          begin
               if OpenProcess( prNORastrearCalculo, 1, FListaParametros, FListaFormulas ) then
               begin
                    try
                      CalculoBegin;
                      SincronizaEmpleado( Fields[ 0 ].AsInteger );
                      LeeTotales;
                      MarcaCeros;
                      GuardaCalculados;
                      oConcepto := GetConcepto( iConcepto );

                      if ( oConcepto <> NIL ) then
                          with oConcepto do
                          begin
                            RastreoTitulo( Formula );
                            PreparaEvaluadores;
                            if ( lExcepcion ) then
                            begin
                                RastreoHeader( '   EXCEPCION DE MONTO   ' );
                                nMontoFormula := Monto;
                                RastreoPesos(  'Monto Capturado: ', nMontoFormula );
                            end
                            else
                                nMontoFormula := EvaluaFormula( Evaluador, lFormulaOK );
                            RastreoHeader( '' );
                            RastreoPesos( 'Monto Calculado del Concepto : ', nMontoFormula );
                            nExentoFormula := EvaluaFormula( EvaluadorExentoISPT, lFormulaOK );
                            EvaluaFormula( EvaluadorISPTInd, lFormulaOK );
                            RastreoNomParam;
                          end
                      else
                        RastreoTitulo( 'NO Encontr� Concepto' );
                      Rastreo := FCreator.Rastreador.Rastro;
                    except
                          on Error: EErrorCalculo do
                          begin
                               with Error as EErrorCalculo do
                               begin
                                    Log.Error( Fields[ 0 ].AsInteger, Titulo, Message );
                               end;
                          end;
                          on Error: EAbort do;  { Excepci�n Silenciosa }
                          on Error: Exception do
                          begin
                               Log.Excepcion( Fields[ 0 ].AsInteger, 'Error al Rastrear Concepto', Error );
                          end;
                    end;
               end;
          end;
          Result := CloseProcess;
     end;
end;

function TCalcNomina.CalculaNetoBruto(Empresa: OleVariant;  var Parametros: OleVariant): OleVariant;
var
   oDatosNeto: TCalculoNeto;
   lBruto: Boolean;
   oCalcNetoBruto: TCalcNetoBruto;
begin
     Result := DZetaServerProvider.GetEmptyProcessResult( prNOCalculoNetoBruto );
     oZetaProvider.EmpresaActiva := Empresa;
     { PARAMETROS DE ENTRADA }
     with FParametros, oDatosNeto do
     begin
         VarValues         := Parametros;
         lBruto            := ParamByName( 'CalcularBruto' ).AsBoolean;
         Bruto             := ParamByName( 'Salario' ).AsFloat;
         Neto              := Bruto;
         TipoPeriodo       := eTipoPeriodo ( ParamByName( 'Periodo' ).AsInteger );
         Subsidio          := ParamByName( 'Subsidio' ).AsFloat;
         TablaPrestaciones := ParamByName( 'TablaPrestaciones' ).AsString;
         Antiguedad        := ParamByName( 'Antiguedad' ).AsInteger;
         Fecha             := ParamByName( 'Fecha' ).AsDateTime;
         Zona              := ParamByName( 'ZonaGeografica' ).AsString;
         ListaOtrasP       := ParamByName( 'OtrasPercepciones' ).AsString;
         Redondeo          := rMax( ParamByName( 'Redondeo' ).AsFloat, 0.01 );
         Clasifi           := eClasifiPeriodo(ParamByName( 'Clasifi' ).AsInteger);
     end;

     oCalcNetoBruto := TCalcNetoBruto.Create( oDatosNeto, FCreator );
     try
        with FCreator do
        begin
             PreparaSalMin;
             {$ifdef IMSS_UMA}
             PreparaValUMA;
             {$endif}
             PreparaPrimasIMSS;
             PreparaPrestaciones;
             PreparaTablasISPT;
        end;
        if lBruto then
           oCalcNetoBruto.DeNetoABruto
        else
           oCalcNetoBruto.DeBrutoANeto;
        { PARAMETROS DE SALIDA }
        with FParametros, oCalcNetoBruto.Datos do
        begin
             AddFloat( 'Bruto', Bruto );
             AddFloat( 'Neto', Neto );
             AddFloat( 'SalMin', SalMin );
             AddFloat( 'Factor', Factor );
             AddFloat( 'Integrado', Integrado );
             AddFloat( 'Diario', Diario );
             AddFloat( 'MontoOtrasP', MontoOtrasP );
             AddFloat( 'ISPT', ISPT );
             AddFloat( 'IMSS', IMSS );
             Parametros := VarValues;
        end;
        DZetaServerProvider.SetProcessOK( Result );
     finally
            oCalcNetoBruto.Free;
     end;
end;

{ * RASTREO * }

function TCalcNomina.Rastrear: Boolean;
begin
     Result := NOT NominaRetroactiva AND FCreator.Rastreando;
end;

procedure TCalcNomina.RastreoHeader( const sTitulo: String );
begin
     FCreator.Rastreador.RastreoHeader( sTitulo );
end;

procedure TCalcNomina.RastreoPesos( const sTexto: String; rMonto: TPesos );
begin
     FCreator.Rastreador.RastreoPesos( sTexto, rMonto );
end;

procedure TCalcNomina.RastreoTexto( const sTexto, sValor: String );
begin
     FCreator.Rastreador.RastreoTexto( sTexto, sValor );
end;

procedure TCalcNomina.RastreoTextoLibre( const sTexto: String );
begin
     FCreator.Rastreador.RastreoTextoLibre( sTexto );
end;


procedure TCalcNomina.RastreoEspacio;
begin
     FCreator.Rastreador.RastreoEspacio;
end;

procedure TCalcNomina.RastreoNoVacio( const sTexto : String );
begin
    if StrLleno( sTexto ) then
        RastreoTextoLibre( sTexto );
end;

{ ********** C�lculo de Salario Neto - Bruto ********* }

constructor TCalcNetoBruto.Create(oDatos: TCalculoNeto; oCreator: TZetaCreator );
begin
     FCreator := oCreator;
     FDatos := oDatos;
     with FDatos do
     begin
          if StrLleno( ListaOtrasP ) then
          begin
               FCreator.PreparaSalarioIntegrado;
          end;
    end;
end;

destructor TCalcNetoBruto.Destroy;
begin
     inherited;
end;

procedure TCalcNetoBruto.CalcParamBrutoNeto;
var
   oPrimasIMSS: TPrimasIMSS;
begin { CalcParamBrutoNeto }
     with FDatos do
     begin
          with FCreator do
          begin
               SalMin := dmSalMin.GetSalMin( Fecha, Zona);
               SMGDF  := dmSalMin.GetSMGDF( Fecha );
               ValUMA := dmValUma.GetVUDefaultDiario( Fecha );
               Factor := dmPrestaciones.GetRenglonPresta( TablaPrestaciones, Antiguedad{$ifdef FLEXIBLES}, 0{$endif} ).FieldByName( 'PT_FACTOR' ).AsFloat;
               oPrimasIMSS := dmPrimasIMSS.GetPrimasIMSS( Fecha );
          end;
          {$ifdef IMSS_UMA}
          TopeEyM := oPrimasIMSS.SS_L_A25 * ValUMA;
          {$else}
          TopeEyM := oPrimasIMSS.SS_L_A25 * SMGDF;
          {$endif}
          Neto := Neto * ( K_FACTOR_MENSUAL / ZetaCommonTools.Dias7Tipo( Clasifi ) );
          Bruto := Bruto * ( K_FACTOR_MENSUAL / ZetaCommonTools.Dias7Tipo( Clasifi ) );
     end;
end;

procedure TCalcNetoBruto.CalcDiarioBrutoNeto;
begin
     with FDatos do
     begin
          Diario := Redondea( Bruto / K_FACTOR_MENSUAL );
          if StrLleno( ListaOtrasP ) then
          begin
               with FCreator.SalarioIntegrado do
               begin
                    CalculaOtrasPer( Diario, 0, Factor, Fecha, ListaOtrasP );
                    MontoOtrasP  := OtrasFijas  * K_FACTOR_MENSUAL;
                    GravaIMSS    := GravadoFijo * K_FACTOR_MENSUAL;
                    GravaISPT    := GravadoISPT * K_FACTOR_MENSUAL;
               end
          end
          else
          begin
               MontoOtrasP   := 0;
               GravaIMSS     := 0;
               GravaISPT     := 0;
          end;
     end;
end;

function  TCalcNetoBruto.DeBrutoANeto: TPesos;
begin
     CalcParamBrutoNeto;
     with FDatos do
     begin
          Neto := PruebaCalculoNeto;
          Result := Neto;
     end;
end;

function  TCalcNetoBruto.PruebaCalculoNeto: TPesos;
begin
     CalcDiarioBrutoNeto;
     with FDatos do
     begin
          //ISPT      := CalcImp( Bruto + GravaISPT, Subsidio, K_FACTOR_MENSUAL, SalMin, 1, 2, 3, Fecha );
          //Se cambio el parametro T2 a -2, sugerencia vb808 - st652
                   //Se cambio el parametro T2 a -2, sugerencia vb808 - st652
          ISPT      := CalcImp( Bruto + GravaISPT, Subsidio, K_FACTOR_MENSUAL, SalMin, 1, -2, 3, Fecha );
          Integrado := Redondea( ZetaCommonTools.rMin( Diario * Factor + GravaIMSS/K_FACTOR_MENSUAL, TopeEyM ) );
          IMSS      := IMSSObrero( K_FACTOR_MENSUAL, K_FACTOR_MENSUAL, Integrado, SalMin, Diario, 0, Fecha, {$ifdef IMSS_UMA}ValUMA{$else}SMGDF{$endif} );
          Result    := NuevosPesos( Bruto + MontoOtrasP - ISPT - IMSS, Redondeo );
     end;
end;

function  TCalcNetoBruto.DeNetoABruto: TPesos;
var
   nPrueba, nBrinco: TPesos;
begin
     CalcParamBrutoNeto;
     with FDatos do
     begin
          Bruto := 3 * Neto;
          nBrinco := ( Bruto - Neto );
          repeat
                nPrueba := PruebaCalculoNeto;
                if ( nPrueba > Bruto ) then
                    nBrinco := nBrinco*0.9
                else
                    nBrinco := nBrinco/2;
                if ( nPrueba > Neto ) then
	  	               Bruto := Redondea( Bruto - nBrinco )
                else
                    Bruto := Redondea( Bruto + nBrinco );
          until ( Abs( nPrueba-Neto ) <= 0.01 ) or ( nBrinco <= 0.01 ) or ( Redondea( Bruto ) = 0 );
          Result := Bruto;
     end;
end;

function TCalcNetoBruto.IMSSObrero( const nDiasEym, nDiasIvcm, nSalBase, nSalMin, nSalario, nImssAjusta: TPesos; const dLey: TDate; const nSMGDF: TPesos ): TPesos;
var
   nEym, nIV, nCV: TPesos;
   oPrimas: TPrimasIMSS;

   function CalcCuota( const nVeces: TPesos; const nPrima: TTasa; const nBase, nDias, nSMGDF: TPesos ): TPesos;
   var
      nTopado: TPesos;
   begin
        if ( nVeces > 0 ) then
            nTopado := rMin( nBase, nVeces*nSMGDF )
        else
            nTopado := nBase;
        Result := Redondea( nTopado * nDias * nPrima / 100 );
   end;

begin { IMSSObrero }
     if ( nSalario = nSalMin ) then
         Result := 0
     else
     begin
          oPrimas := FCreator.dmPrimasIMSS.GetPrimasIMSS( dLey );
          nEym    := CalcCuota( oPrimas.SS_L_A25, oPrimas.SS_O_A25, nSalBase, nDiasEyM, nSMGDF ) +
                     CalcCuota( 0, oPrimas.SS_O_A1062, ZetaCommonTools.rMax( nSalBase-oPrimas.SS_MAXIMO * nSMGDF, 0 ), nDiasEyM, nSMGDF ) +
                     CalcCuota( oPrimas.SS_L_A107, oPrimas.SS_O_A107, nSalBase, nDiasEyM, nSMGDF );
          nIV     := CalcCuota( oPrimas.SS_L_IV, oPrimas.SS_O_IV, nSalBase, nDiasIVCM, nSMGDF );
          nCV     := CalcCuota( oPrimas.SS_L_CV, oPrimas.SS_O_CV, nSalBase, nDiasIVCM, nSMGDF );
          Result := nEym + nIV + nCV;
     end;
end;

function TCalcNetoBruto.CalcImp( const nValor, nFactorSubsidio, nFactor, nSalMin: TPesos;
                                 nT1, nT2, nT3: Integer; const dFecha: TDate ): TPesos;
var
   oRenglon, oRenglon2: TRenglonNumerica;
   nExcedente, nMarginal, nImpuesto, nCredito, nSubsidio: TPesos;
   nExcedenteSubsidio, nMarginalSubsidio : TPesos;
   lExcedenteSubsidio, lMarginalSubsidio : Boolean;
   lReformaFiscal2008: Boolean;
begin
     lReformaFiscal2008 := ( TheYear(dFecha) >= K_REFORMA_FISCAL_2008 );

     // Si valor <= 0, ni pierdo tiempo investigando y calculando
     if ( nValor <= 0 ) then
     begin
          Result := 0;
          Exit;
     end;
     // Impuesto Art. 80
     oRenglon := FCreator.dmTablasISPT.ConsultaTablaNumerica( nT1, nValor, dFecha );
     with oRenglon do
     begin
          nExcedente := nValor - Inferior;
          nMarginal := nExcedente * ( Tasa/100 );
          nImpuesto := Cuota + nMarginal;
     end;
     // Cr�dito al Salario: Art. 80-B
     if ( nT3 = 0 ) then
        nCredito := nSalMin / 10 * nFactor
     else
         if ( nT3 < 0 ) then
            nCredito := 0
         else
             nCredito := FCreator.dmTablasISPT.ConsultaTablaNumerica( nT3, nValor, dFecha ).Cuota;
     // Subsidio: Art. 80-A
     if ( not lReformaFiscal2008 ) and ( ( nFactorSubsidio > 0 ) AND ( nT2 <> 0 ) ) then
     begin
          lExcedenteSubsidio := FALSE;
           // Si la tabla 2 es negativa, vuelve a calcular el impuesto marginal
           if ( nT2 < 0 ) then
           begin
                lMarginalSubsidio := TRUE;
                nT2 := -nT2;
                if ( nT2 > 1000 ) then
                begin
                     lExcedenteSubsidio := TRUE;
                     nT2 := nT2 - 1000;
                end;
           end
           else
               lMarginalSubsidio := FALSE;
           oRenglon2 := FCreator.dmTablasISPT.ConsultaTablaNumerica( nT2, nValor, dFecha );
           with oRenglon2 do
           begin
               {$ifdef ANTES}
               nSubsidio := (( nMarginal * ( Tasa/100 ) ) +  Cuota ) * nFactorSubsidio;
               {$else}
               if ( lMarginalSubsidio ) then
               begin
                  nExcedenteSubsidio := nValor - Inferior;
                  if ( lExcedenteSubsidio ) then
                        nMarginalSubsidio := nExcedenteSubsidio
                  else
                        nMarginalSubsidio  := nExcedenteSubsidio * ( oRenglon.Tasa/100 );
               end
               else
                   nMarginalSubsidio := nMarginal;
               nSubsidio := (( nMarginalSubsidio * ( Tasa/100 ) ) +  Cuota ) * nFactorSubsidio;
               // Lo topa para que no sea mayor que el impuesto
               if lExcedenteSubsidio and ( nSubsidio > nImpuesto ) then
                 nSubsidio := nImpuesto;

               {$endif}
           end;
     end
     else
          nSubsidio := 0;
     // Impuesto NETO
     Result := ZetaCommonTools.Redondea( nImpuesto - nSubsidio - nCredito );

     // Compatiblidad con Ley de 1991
     if ( nT3 <= 0 ) AND ( Result < 0 ) then
        Result := 0;
end;

end.


