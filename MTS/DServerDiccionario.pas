unit DServerDiccionario;
{$INCLUDE DEFINES.INC}
{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      DServerDiccionario.pas                     ::
  :: Descripci�n: Programa principal de Diccionario.dll      ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComServ, ComObj, VCLCom, StdVcl, DataBkr, DBClient, MtsRdm, Mtx,
     Variants,
{$ifndef DOS_CAPAS}
     Diccionario_TLB,
{$endif}
     DZetaServerProvider,
     ZCreator,
     ZetaTipoEntidad,
     ZetaCommonLists, DB, ZetaServerDataSet;

type
  TdmServerDiccionario = class(TMtsDataModule {$ifndef DOS_CAPAS}, IdmServerDiccionario {$endif} )
    cdsTemporal: TServerDataSet;
    procedure dmServerDiccionarioCreate(Sender: TObject);
    procedure dmServerDiccionarioDestroy(Sender: TObject);
  private
    { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
{$ifndef DOS_CAPAS}
    oZetaCreator: TZetaCreator;
{$endif}
    procedure InitCreator;
  protected
    { Protected declarations }
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
{$ifdef DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
{$endif}
    function GrabaDiccion(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetListaFunciones(Empresa: OleVariant): OleVariant; safecall;
    function GetListaGlobal(Empresa: OleVariant): OleVariant; safecall;
    function PruebaFormula(Empresa, Parametros: OleVariant;var Formula: WideString; Entidad: Integer;ParamsRep: OleVariant): WordBool; safecall;
    function GetDiccionario(Empresa: OleVariant; lverConfidencial: WordBool; const Filtro: WideString): OleVariant; safecall;
    function SQLValido(Empresa: OleVariant; const Formula: WideString; Entidad: Integer; var MsgError: WideString): OleVariant; safecall;
  end;

implementation

uses DEntidadesTress,
     DSuperReporte,
     ZBaseCreator,
     ZetaSQLBroker,
     ZetaSQL,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaServerTools,
     ZReportConst,
     ZEvaluador,
     ZetaEntidad;

const
      //CONSTANTES PARA LOS SCRIPTS
      Q_DICCIONARIO = 5;
      Q_GET_TABLAS = 6;
      Q_GETLISTA_FUNCIONES = 7;
      Q_GETLISTA_GLOBAL = 8;
      Q_GET_DESCR_GRUPO = 9;

{$R *.DFM}

{----------------------------------------------------------------}

class procedure TdmServerDiccionario.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
     if Register then
     begin
          inherited UpdateRegistry(Register, ClassID, ProgID);
          EnableSocketTransport(ClassID);
          EnableWebTransport(ClassID);
     end
     else
     begin
          DisableSocketTransport(ClassID);
          DisableWebTransport(ClassID);
          inherited UpdateRegistry(Register, ClassID, ProgID);
     end;
end;

procedure TdmServerDiccionario.dmServerDiccionarioCreate(Sender: TObject);
begin
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
end;

procedure TdmServerDiccionario.dmServerDiccionarioDestroy(Sender: TObject);
begin
     ZetaSQLBroker.FreeZetaCreator( oZetaCreator );
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
end;

procedure TdmServerDiccionario.InitCreator;
begin
     if not Assigned( oZetaCreator ) then
        oZetaCreator := ZetaSQLBroker.GetZetaCreator( oZetaProvider );
end;

{$ifdef DOS_CAPAS}
procedure TdmServerDiccionario.CierraEmpresa;
begin
end;
{$endif}

{----------------------------------------------------------------}
{ ***************** PARTE PRIVADA *******************************}

function GetSQL( const iSQLNumero: Integer ): String;
begin
     case iSQLNumero of

          Q_DICCIONARIO:
          Result := 'SELECT '+
                    '( SELECT D2.DI_NOMBRE FROM DICCION D2 '+
                    '  WHERE ( D2.DI_CLASIFI = -1 ) '+
                    '  AND ( D2.DI_CALC = D1.DI_CLASIFI ) ) DI_TABLA, '+
                    'D1.DI_CLASIFI , D1.DI_NOMBRE , D1.DI_TITULO , '+
                    'D1.DI_CALC , D1.DI_ANCHO , D1.DI_MASCARA , '+
                    'D1.DI_TFIELD , D1.DI_ORDEN , D1.DI_REQUIER , '+
                    'D1.DI_TRANGO , D1.DI_NUMERO , D1.DI_VALORAC , '+
                    'D1.DI_RANGOAC , D1.DI_CLAVES , D1.DI_TCORTO , '+
                    'D1.DI_CONFI '+
                    'FROM DICCION D1   '+
                    'WHERE D1.DI_CLASIFI<>156 %s '+
                    'ORDER BY D1.DI_CLASIFI,D1.DI_NOMBRE ';
          Q_GETLISTA_FUNCIONES:
          Result:= 'select DI_NOMBRE, DI_TITULO, DI_CLAVES, DI_TITULO '+
                   'from   DICCION ' +
                   'where  ( DI_CLASIFI = 104 ) ' +
                   'order by DI_NOMBRE';
          Q_GETLISTA_GLOBAL:
          Result:= 'select GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO, US_CODIGO, GL_CAPTURA, GL_NIVEL '+
                   'from   GLOBAL ' +
                   'order by GL_DESCRIP';
     end;
end;

{ *********** Interfases ( Paletitas ) ************ }

function TdmServerDiccionario.GrabaDiccion(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     with oZetaProvider,TablaInfo do
     begin
          SetInfo( 'DICCION',
                   'DI_CLASIFI,DI_NOMBRE,DI_TITULO,DI_CALC,DI_ANCHO,DI_MASCARA,DI_TFIELD,DI_ORDEN,DI_REQUIER,DI_TRANGO,DI_NUMERO,DI_VALORAC,DI_RANGOAC,DI_CLAVES,DI_TCORTO,DI_CONFI',
                   'DI_CLASIFI,DI_NOMBRE' );
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
     end;
end;

function TdmServerDiccionario.GetListaFunciones( Empresa: OleVariant): OleVariant;
var
    oEvaluador : TZetaEvaluador;
    i: integer;
begin
     with cdsTemporal do
     begin
          AddStringField('DI_NOMBRE', 10);
          AddStringField('DI_TITULO',90) ;
          AddStringField('DI_CLAVES',10) ;
          CreateDataset;
     end;
     InitCreator;
     with oZetaCreator do
     begin
          RegistraFunciones( [efComunes,efReporte] );
     end;
     oEvaluador := TZetaEvaluador.Create( oZetaCreator );
     try
         for i := 0 to oEvaluador.LibreriaFunciones.EntryList.Count - 1 do
         begin
              with cdsTemporal do
              begin
                   Append;
                   with oEvaluador.LibreriaFunciones.Entry[i] do
                   begin
                        FieldByName('DI_NOMBRE').AsString := Name;
                        FieldByName('DI_TITULO').AsString := Description;
                   end;
                   Post;
              end;

         end;
     finally
            FreeAndNil(oEvaluador);
     end;
     cdsTEmporal.INdexFieldNames := 'DI_NOMBRE';
     Result := cdsTemporal.Data;
end;
{$ifdef FALSE}
function TdmServerDiccionario.GetListaFunciones( Empresa: OleVariant): OleVariant;
begin
     Result := oZetaProvider.OpenSQL( Empresa, GetSQL( Q_GETLISTA_FUNCIONES ), TRUE );
end;
{$endif}

function TdmServerDiccionario.GetListaGlobal( Empresa: OleVariant): OleVariant;
begin
     Result := oZetaProvider.OpenSQL( Empresa, GetSQL( Q_GETLISTA_GLOBAL ), TRUE );
end;

function TdmServerDiccionario.PruebaFormula(Empresa, Parametros: OleVariant; var Formula: WideString; Entidad: Integer; ParamsRep: OleVariant): WordBool;
var
   oEvaluador: TZetaEvaluador;
   oSuperReporte : TdmSuperReporte;
   TipoFormula: eTipoGlobal;
   sTransformada: String;
   lConstante: Boolean;
   eFunciones: TipoFunciones;
   eEvaluador: eTipoEvaluador;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          InitGlobales;
          eEvaluador := eTipoEvaluador( ParamList.ParamByName('TipoEvaluador').AsInteger );
     end;
     oSuperReporte := NIL;

     InitCreator;
     if not (VarIsNull(ParamsRep) OR VarIsEmpty(ParamsRep)) then
     begin
          oSuperReporte := TdmSuperReporte.Create(oZetaCreator);
          oSuperReporte.Parametros := ParamsRep;
     end;

     with oZetaCreator do
     begin
          eFunciones := [ efComunes,efAnuales, efNomina, efReporte, efPoliza ];
          if ( eEvaluador = evAguinaldo ) then
             eFunciones := eFunciones + [ efAguinaldo ];
          if ( eEvaluador = evReglasPrestamo ) then
             eFunciones := eFunciones + [ efReglasPrestamos ];
          PreparaEntidades;
          RegistraFunciones( eFunciones );

          SuperReporte := oSuperReporte;
     end;
     oEvaluador := TZetaEvaluador.Create( oZetaCreator );

     {CV: 28-Nov-2001.
     Lo que se esta logrando aqui, es que el Objecto CalcNomina tenga un valor DIFERENTE de
     NIL. Esto es para que cuando se evalue una formula que nada mas pertenezca al calculo de nomina,
     esta funcion tenga acceso a este objeto.}
     if ( eEvaluador = evNomina ) then
        oEvaluador.CalcNomina := self;

     {Para que funcione con PARAM():
     -Se tiene que crear un oSuperReporte, y este asignarselo a oZetaCreator
     -Al oSuperReporte, se le asignan los parametros
     -Destruir al final el oSuperReporte}
     try
        oEvaluador.Entidad := TipoEntidad( Entidad );
        sTransformada := Formula;
        Result := oEvaluador.GetRequiere( Formula, TipoFormula, sTransformada, lConstante );
        if not Result then
           Formula := sTransformada; // Regresa el mensaje de Error en 'Formula'
     finally
            FreeAndNil(oEvaluador);
            FreeAndNil(oSuperReporte);
     end;
end;

function TdmServerDiccionario.GetDiccionario(Empresa: OleVariant;lverConfidencial: WordBool; const Filtro: WideString): OleVariant;
var
   sFiltro: String;
begin
     //Los Confidenciales se Resuelven en el Cliente.
     //Por que tiene cache el Diccion.
     //No se pueden refrescar los datos dependiendo los derechos
     //de usuario. Se mandan todos los datos y que el cliente
     //se haga cargo de los confidenciales.
     sFiltro := Filtro;
     if not lVerConfidencial then
        sFiltro := ConcatFiltros( sFiltro, 'D1.DI_CONFI = ''N''');
     if strLleno(sFiltro) then
        sFiltro := 'and ' + sFiltro;
     Result := oZetaprovider.OpenSQL( Empresa, Format( GetSQL( Q_DICCIONARIO ), [ sFiltro ] ), True );
end;

function TdmServerDiccionario.SQLValido(Empresa: OleVariant; const Formula: WideString; Entidad: Integer; var MsgError: WideString): OleVariant;
var
   oEvaluador : TZetaEvaluador;
   sFormula,sMsgError: string;
begin
     Result := TRUE;
     MsgError:= VACIO;
     oZetaProvider.EmpresaActiva := Empresa;
     InitCreator;
     oZetaCreator.PreparaEntidades;
     oEvaluador := TZetaEvaluador.Create( oZetaCreator );
     with oEvaluador do
     try
          oEvaluador.Entidad := Entidad;	// LA TABLA PRINCIPAL
          if ( strLleno( Formula ) ) then
          begin
               sFormula := Formula;
               Result := FiltroSQLValido( sFormula , sMsgError ) ;
               MsgError:= sMsgError;
          end;
     finally
            FreeAndNil(oEvaluador);
     end;
end;

{$ifndef DOS_CAPAS}
initialization
  TComponentFactory.Create(ComServer, TdmServerDiccionario, Class_dmServerDiccionario, ciMultiInstance, ZetaServerTools.GetThreadingModel);
{$endif}
end.




