unit Cafeteria_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 10/20/2008 3:17:13 PM from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3win_20\MTS\Cafeteria.tlb (1)
// LIBID: {AD707C11-88D9-11D3-8DEC-0050DA04EAA0}
// LCID: 0
// Helpfile: 
// HelpString: Tress Cafeteria
// DepndLst: 
//   (1) v1.0 Midas, (C:\WINDOWS\system32\midas.dll)
//   (2) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, Midas, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  CafeteriaMajorVersion = 1;
  CafeteriaMinorVersion = 0;

  LIBID_Cafeteria: TGUID = '{AD707C11-88D9-11D3-8DEC-0050DA04EAA0}';

  IID_IdmServerCafeteria: TGUID = '{AD707C12-88D9-11D3-8DEC-0050DA04EAA0}';
  CLASS_dmServerCafeteria: TGUID = '{AD707C14-88D9-11D3-8DEC-0050DA04EAA0}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerCafeteria = interface;
  IdmServerCafeteriaDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerCafeteria = IdmServerCafeteria;


// *********************************************************************//
// Interface: IdmServerCafeteria
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {AD707C12-88D9-11D3-8DEC-0050DA04EAA0}
// *********************************************************************//
  IdmServerCafeteria = interface(IAppServer)
    ['{AD707C12-88D9-11D3-8DEC-0050DA04EAA0}']
    function GetCafDiarias(Empresa: OleVariant; Empleado: Integer; dFecha: TDateTime): OleVariant; safecall;
    function GetCafInvita(Empresa: OleVariant; Invitador: Integer; dFecha: TDateTime): OleVariant; safecall;
    function GetCafPeriodo(Empresa: OleVariant; Empleado: Integer; dFechaIni: TDateTime; 
                           dFechaFin: TDateTime; out Totales: OleVariant): OleVariant; safecall;
    function GetComidas(Empresa: OleVariant; Fecha: TDateTime; lSoloAltas: WordBool): OleVariant; safecall;
    function GrabaCafDiarias(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GrabaCafInvita(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GrabaComidas(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function ComidasGrupales(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function ComidasGrupalesGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function ComidasGrupalesLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CorregirFechasCafe(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetCasDiarias(Empresa: OleVariant; Empleado: Integer; dFecha: TDateTime): OleVariant; safecall;
    function GrabaCasDiarias(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetCaseta(Empresa: OleVariant; Fecha: TDateTime; lSoloAltas: WordBool): OleVariant; safecall;
    function GrabaCaseta(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerCafeteriaDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {AD707C12-88D9-11D3-8DEC-0050DA04EAA0}
// *********************************************************************//
  IdmServerCafeteriaDisp = dispinterface
    ['{AD707C12-88D9-11D3-8DEC-0050DA04EAA0}']
    function GetCafDiarias(Empresa: OleVariant; Empleado: Integer; dFecha: TDateTime): OleVariant; dispid 1;
    function GetCafInvita(Empresa: OleVariant; Invitador: Integer; dFecha: TDateTime): OleVariant; dispid 2;
    function GetCafPeriodo(Empresa: OleVariant; Empleado: Integer; dFechaIni: TDateTime; 
                           dFechaFin: TDateTime; out Totales: OleVariant): OleVariant; dispid 3;
    function GetComidas(Empresa: OleVariant; Fecha: TDateTime; lSoloAltas: WordBool): OleVariant; dispid 4;
    function GrabaCafDiarias(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 5;
    function GrabaCafInvita(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 6;
    function GrabaComidas(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 7;
    function ComidasGrupales(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 8;
    function ComidasGrupalesGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 9;
    function ComidasGrupalesLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 10;
    function CorregirFechasCafe(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 11;
    function GetCasDiarias(Empresa: OleVariant; Empleado: Integer; dFecha: TDateTime): OleVariant; dispid 301;
    function GrabaCasDiarias(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 302;
    function GetCaseta(Empresa: OleVariant; Fecha: TDateTime; lSoloAltas: WordBool): OleVariant; dispid 303;
    function GrabaCaseta(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 304;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer; 
                             out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                           Options: Integer; const CommandText: WideString; var Params: OleVariant; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerCafeteria provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerCafeteria exposed by              
// the CoClass dmServerCafeteria. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerCafeteria = class
    class function Create: IdmServerCafeteria;
    class function CreateRemote(const MachineName: string): IdmServerCafeteria;
  end;

implementation

uses ComObj;

class function CodmServerCafeteria.Create: IdmServerCafeteria;
begin
  Result := CreateComObject(CLASS_dmServerCafeteria) as IdmServerCafeteria;
end;

class function CodmServerCafeteria.CreateRemote(const MachineName: string): IdmServerCafeteria;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerCafeteria) as IdmServerCafeteria;
end;

end.
