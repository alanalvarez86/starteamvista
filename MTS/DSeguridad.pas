unit DSeguridad;

interface

{$DEFINE SISNOM_CLAVES}

uses SysUtils, Classes, DB, Variants, DBClient,
     DZetaServerProvider, ZetaServerDataSet;

const
     K_CLAVE_USUARIO = 'US_PASSWRD';
     K_CLAVE_EMPRESA = 'CM_PASSWRD';

type
  TSeguridadInfo = class(TObject)
  private
    {Private Declarations}
    FProvider: TdmZetaServerProvider;
    FQryPasswords: TZetaCursor;
    FDelPasswords: TZetaCursor;
    FInsPasswords: TZetaCursor;
    FUsuario, FPasswordLog: Integer;
    FCambiaPasswordLog: Boolean;
    FListaPasswords: TStrings;
    function GetUsaPasswordLog: Boolean;
    function ExisteClave( const sClave: String ): Boolean;
    procedure SetPasswordLog;
    procedure CheckPasswordLogClave;
  public
    {Public Declarations}
    property oZetaProvider: TdmZetaServerProvider read FProvider;
    constructor Create( oProvider: TdmZetaServerProvider );
    destructor Destroy; override;
    property  UsaPasswordLog: Boolean read GetUsaPasswordLog;
    property  CambiaPasswordLog: Boolean read FCambiaPasswordLog write FCambiaPasswordLog;
    function  ValidaPasswordLog( const sClave: String; const iUsuario: Integer; var sMensaje: String ): Boolean;
    procedure PasswordLogBegin;
    procedure PasswordLogEnd;
    procedure GrabaPasswordLog;
  end;

  TUsuarioData = class(TObject)
  private
    {Private Declarations}
    cdsLista : TClientDataset;
    cdsTemporal : TClientDataset;
    cdsCompanys:TClientDataset;

    FProvider: TdmZetaServerProvider;
    //FQuery: TZetaCursor;
    //FDelete: TZetaCursor;
    //FInsert: TZetaCursor;
    FUsuario: Integer;
    FTextoBitacora: string;
    FSeguridadInfo : TSeguridadInfo;
    FErrores: TStringlist;
    FEmpresaOrigen : OleVariant;
{$ifdef SISNOM_CLAVES}
    FEncriptaPass: Boolean;
{$endif}
    procedure SetTablaInfo;
    procedure CodificaClaveUsuario(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure GrabaClaveUsuario(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    procedure CargaCompanys;
    procedure Codifica(const sCampo: String; DeltaDS: TzProviderClientDataSet);
    procedure GetCompany( const sCompany : string;  var lEncontro : boolean; var oEmpresa : OleVariant );
{$ifndef DOS_CAPAS}
    procedure ActualizaUsuarioEnCatalogos(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
{$endif}
    function DecodificaClaves(Datos: OleVariant; const sCampo: String): OleVariant;
  public
    {Public Declarations}
    property oZetaProvider: TdmZetaServerProvider read FProvider;
    property Errores: TStringlist read FErrores;
{$ifdef SISNOM_CLAVES}
    property EncriptaPass: Boolean read FEncriptaPass write FEncriptaPass;
{$endif}
    constructor Create( oProvider: TdmZetaServerProvider );
    destructor Destroy; override;
    //function GetUsuarios(out Longitud: Integer; Grupo: Integer; out BlockSistema: WordBool): OleVariant;
    function GrabaUsuarios(oDelta: OleVariant; iUsuario: Integer; out ErrorCount: Integer): OleVariant;
    procedure GrabaEmpleadoTress(const iEmpleado, iUsuario: integer; const sCompany: string);
    procedure TransfiereEmpleado( Empresa, EmpresaDestino: OleVariant; const iEmpleado, iNewEmpleado: integer );
    procedure ActivaDesactiva(Empresa: OleVariant;const iEmpleado: integer; const lActivar: Boolean);
    function AgregarUsuario(const sUsuario,sPassword, sDominioUsuario, sCorreo,sNombre:string; const iEmpleado,GrupoUsuarios, iJefe,iCodigo: Integer; const lUsaPortal: Boolean):Boolean;
    procedure EditarAgregarUsuario( DataSet: TDataset ;const lAgregar:Boolean ;var iCodigo:Integer);
{$ifndef DOS_CAPAS}
    procedure AjustarSupervisorEnrolado(DeltaDS: TzProviderClientDataSet);
    procedure ActualizaUsuariosEmpresa(Empresa, oDelta: OleVariant);
{$endif}
  end;

implementation

uses ZetaCommonClasses,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaServerTools;

type
  eQryScripts = ( e_PASSWRD_LOG, e_PASSWRD_LOG_DEL, e_PASSWRD_LOG_INS, e_PASSWRD_LOG_CLAVE,
                  e_UPDATE_USER_COLABORA, e_COMPANYS_LIST, e_GET_SUPERVISOR_ENROLADO );

function GetSQLScript( const eScript: eQryScripts ): String;
begin
     case eScript of
          e_PASSWRD_LOG: Result := 'select PL_PASSWRD from PASSLOG where ( US_CODIGO = :Usuario ) order by PL_FOLIO desc';
          e_PASSWRD_LOG_DEL: Result := 'delete from PASSLOG where ( US_CODIGO = :Usuario )';
          e_PASSWRD_LOG_INS: Result := 'insert into PASSLOG ( US_CODIGO, PL_FOLIO, PL_PASSWRD ) values ( :Usuario, :Folio, :Password )';
          e_PASSWRD_LOG_CLAVE: Result := 'select CL_TEXTO from CLAVES where ( CL_NUMERO = %d )';
          e_UPDATE_USER_COLABORA: Result := 'update COLABORA set US_CODIGO = %d where CB_CODIGO = %d';
          e_COMPANYS_LIST: Result := 'select CM_NOMBRE, CM_ALIAS, CM_USRNAME, CM_PASSWRD, CM_NIVEL0, CM_DATOS, CM_CODIGO '+
                                     'from COMPANY '+
                                     'where ( %s ) and '+
                                     '( CM_DIGITO is NOT NULL ) and '+
                                     '( CM_DIGITO <> '' '' ) '+
                                     'order by CM_CODIGO';

          e_GET_SUPERVISOR_ENROLADO: Result := 'select US_JEFE, USA_ENROL from dbo.SP_GET_SUPERVISOR_ENROLADO_POR_EMPLEADO(%d)';
     end;
end;

{ ************* TSeguridadInfo ************ }

constructor TSeguridadInfo.Create( oProvider: TdmZetaServerProvider );
begin
     FProvider := oProvider;
     FListaPasswords := TStringList.Create;
     FCambiaPasswordLog := FALSE;
end;

destructor TSeguridadInfo.Destroy;
begin
     FListaPasswords.Free;
     inherited Destroy;
end;

procedure TSeguridadInfo.PasswordLogBegin;
begin
     with oZetaProvider do
     begin
          CheckPasswordLogClave;
          if UsaPasswordLog then                 // Si no se usa no crea los querys
          begin
               FQryPasswords := CreateQuery( GetSQLScript( e_PASSWRD_LOG ) );
               FDelPasswords := CreateQuery( GetSQLScript( e_PASSWRD_LOG_DEL ) );
               FInsPasswords := CreateQuery( GetSQLScript( e_PASSWRD_LOG_INS ) );
          end;
     end;
end;

procedure TSeguridadInfo.PasswordLogEnd;
begin
     FreeAndNil( FQryPasswords );
     FreeAndNil( FDelPasswords );
     FreeAndNil( FInsPasswords );
end;

procedure TSeguridadInfo.CheckPasswordLogClave;
var
   FDataset: TZetaCursor;
begin
     FDataset := oZetaProvider.CreateQuery( Format( GetSQLScript( e_PASSWRD_LOG_CLAVE ), [ CLAVE_MAX_LOG_PASSWORD ] ) );
     try
        with FDataset do
        begin
             Active := True;
             if EOF then
                FPasswordLog := 0
             else
                FPasswordLog := StrToIntDef( FieldByName( 'CL_TEXTO' ).AsString, 0 );
             Active := False;
        end;
     finally
        FreeAndNil( FDataset );
     end;
end;

function TSeguridadInfo.GetUsaPasswordLog: Boolean;
begin
     Result := ( FPasswordLog > 0 );
end;

procedure TSeguridadInfo.SetPasswordLog;
begin
     FListaPasswords.Clear;
     oZetaProvider.ParamAsInteger( FQryPasswords, 'Usuario', FUsuario );
     with FQryPasswords do
     begin
          Active := TRUE;
          while not EOF do
          begin
               FListaPasswords.Add( FieldByName ( 'PL_PASSWRD' ).AsString );
               Next;
          end;
          Active := FALSE;
     end;
end;

function TSeguridadInfo.ValidaPasswordLog( const sClave: String; const iUsuario: Integer; var sMensaje: String ): Boolean;
const
     K_MESS_ERROR = 'La Clave De Acceso Ya Ha Sido Utilizada Anteriormente';
begin
     if UsaPasswordLog and FCambiaPasswordLog then
     begin
          FUsuario := iUsuario;
          SetPasswordLog;
          Result := ( not ExisteClave( sClave ) );
          if Result then
             FListaPasswords.Add( sClave )
          else
             sMensaje := K_MESS_ERROR
     end
     else
         Result := TRUE;
end;

function TSeguridadInfo.ExisteClave( const sClave: String ): Boolean;
var
   i : Integer;
begin
     Result := FALSE;
     with FListaPasswords do
     begin
          for i := 0 to Count - 1 do
          begin
               // OJO: Encrypt() / Decrypt() da un resultado diferente cada vez
               if ( ZetaServerTools.Decrypt( sClave ) = ZetaServerTools.Decrypt( Strings[i] ) ) then
               begin
                    Result := TRUE;
                    Break;
               end;
          end;
     end;
end;

procedure TSeguridadInfo.GrabaPasswordLog;
var
   i, iCuantos : Integer;
begin
     if UsaPasswordLog and FCambiaPasswordLog then
     begin
          if ( FUsuario = 0 ) then
             raise Exception.Create( 'Se debe Indicar el Usuario Antes de Grabar la Bit�cora de Claves de Acceso' );
          with oZetaProvider do
          begin
               ParamAsInteger( FDelPasswords, 'Usuario', FUsuario );
               Ejecuta( FDelPasswords );
               ParamAsInteger( FInsPasswords, 'Usuario', FUsuario );
               iCuantos := 1;
               for i := ( FListaPasswords.Count - 1 ) downto 0 do
               begin
                    ParamAsInteger( FInsPasswords, 'Folio', iCuantos );
                    ParamAsVarChar( FInsPasswords, 'Password', FListaPasswords[i], K_ANCHO_PASSWD );
                    Ejecuta( FInsPasswords );
                    Inc( iCuantos );
                    if ( iCuantos > FPasswordLog ) then
                       Break;
               end;
          end;
     end;
end;

{ TUsuarioData }

constructor TUsuarioData.Create(oProvider: TdmZetaServerProvider);
begin
     FProvider := oProvider;
     cdsLista := TClientDataset.Create(oZetaProvider);
     cdsCompanys := TClientDataset.Create(oZetaProvider);
     cdsTemporal := TClientDataset.Create(oZetaProvider);

     FErrores:= TStringlist.Create;
{$ifdef SISNOM_CLAVES}
     FEncriptaPass := TRUE;
{$endif}
end;

destructor TUsuarioData.Destroy;
begin
     FreeAndNil( FErrores );
     FreeAndNil( cdsTemporal );
     FreeAndNil( cdsLista );
     FreeAndNil( cdsCompanys );
     inherited Destroy;
end;

function TUsuarioData.GrabaUsuarios(oDelta: OleVariant; iUsuario: Integer;  out ErrorCount: Integer): OleVariant;
var
   oEmpresaTemp: OleVariant;
begin
     FUsuario:= iUsuario;
     with oZetaProvider do
     begin
          oEmpresaTemp := EmpresaActiva;
          FEmpresaOrigen := EmpresaActiva;
          EmpresaActiva := Comparte;
     end;
     FSeguridadInfo := TSeguridadInfo.Create( oZetaProvider );
     try
        FSeguridadInfo.PasswordLogBegin;
        try
           SetTablaInfo;
           with oZetaProvider do
           begin
                with TablaInfo do
                begin
                     BeforeUpdateRecord := CodificaClaveUsuario;
                     AfterUpdateRecord := GrabaClaveUsuario;
                end;
                Result := GrabaTabla( Comparte, oDelta, ErrorCount );
           end;
        finally
           FSeguridadInfo.PasswordLogEnd;
           if NOT VarIsNull(oEmpresaTemp) then
              oZetaProvider.EmpresaActiva := oEmpresaTemp;
        end;
     finally
        FreeAndNil( FSeguridadInfo );
     end;
end;

procedure TUsuarioData.SetTablaInfo;
begin
     with oZetaProvider.TablaInfo do
          SetInfo( 'USUARIO',
                   'US_CODIGO, US_CORTO, GR_CODIGO, US_NIVEL, US_NOMBRE, US_PASSWRD, US_BLOQUEA, US_CAMBIA, US_FEC_IN, US_FEC_OUT, US_FEC_SUS, US_DENTRO, US_ARBOL, US_FORMA, US_BIT_LST, US_FEC_PWD, US_EMAIL, US_FORMATO, US_LUGAR, US_MAQUINA, CM_CODIGO, CB_CODIGO, ' +
                   'US_DOMAIN, US_PORTAL, US_ACTIVO, US_JEFE, US_CLASICA',
                   'US_CODIGO' );

end;

procedure TUsuarioData.CodificaClaveUsuario(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
var
   sMensaje: String;

begin
     if ( UpdateKind in [ ukModify, ukDelete ] ) then
     begin
          FTextoBitacora:= oZetaProvider.GetDatasetInfo( DeltaDS, ( UpdateKind = ukDelete ) , 'US_PASSWRD' ) ;
     end;

     if ( UpdateKind in [ ukModify, ukInsert ] ) then
     begin
          with DeltaDS do
          begin
               { *** Seguridad *** }

               FSeguridadInfo.CambiaPasswordLog := ZetaServerTools.CambiaCampo( FieldByName( K_CLAVE_USUARIO ) );
{$ifdef SISNOM_CLAVES}
               if FEncriptaPass then
{$endif}
                  Codifica( K_CLAVE_USUARIO, DeltaDS );
               if not FSeguridadInfo.ValidaPasswordLog( ZetaServerTools.CampoAsVar( FieldByName( K_CLAVE_USUARIO ) ),
                                                        ZetaServerTools.CampoAsVar( FieldByName( 'US_CODIGO' ) ),
                                                        sMensaje ) then
                  DataBaseError( sMensaje );

{$ifndef DOS_CAPAS}
               { ** Obtiene US_JEFE de TRESS ** }
               AjustarSupervisorEnrolado( DeltaDS );
{$endif}

          end;
     end
     else
     begin
          FSeguridadInfo.CambiaPasswordLog := FALSE;
     end;
     Applied := False;
end;

procedure TUsuarioData.GrabaClaveUsuario(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
var
   sTitulo: String;
   iUsuario, iEmpleadoNuevo, iEmpleadoAnt: Integer;
   sNombre,sCompanyNuevo,sCompanyAnt: String;
   lCambiaEmpleado, lCambiaEmpresa: Boolean;
const
     aTipoRegistro: array[ TUpdateKind ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'Modific�','Agreg�','Borr�');
begin

     lCambiaEmpresa := FALSE;
     lCambiaEmpleado := FALSE;
     iEmpleadoNuevo := 0;
     iEmpleadoAnt := 0;


     with FSeguridadInfo do
     begin
          if UsaPasswordLog  then
             GrabaPasswordLog; // Se Debe Invocar antes la funci�n TSeguridadInfo.ValidaPasswordLog()
     end;

     with DeltaDs do
     begin
          if ( UpdateKind in [ ukModify, ukDelete ]) then
          begin
               iUsuario := CampoOldAsVar( FieldByName('US_CODIGO') );
               sNombre := CampoOldAsVar( FieldByName('US_CORTO') );

               {Queda pendiente como se, que viene desde Tress el Cambio}
                    lCambiaEmpresa := CambiaCampo(FieldByName('CM_CODIGO'));
                    if NOT lCambiaEmpresa then
                       lCambiaEmpleado := CambiaCampo(FieldByName('CB_CODIGO'));


                    if lCambiaEmpresa or lCambiaEmpleado or ( UpdateKind = ukDelete ) then
                    begin
                         iEmpleadoAnt := CampoOldAsVar(FieldByName('CB_CODIGO'));
                         iEmpleadoNuevo := CampoAsVar(FieldByName('CB_CODIGO'));
                         sCompanyAnt := CampoOldAsVar(FieldByName('CM_CODIGO'));
                         sCompanyNuevo := CampoAsVar(FieldByName('CM_CODIGO'));
                    end;
               {*********}


          end
          else
          begin
               iUsuario := CampoAsVar( FieldByName('US_CODIGO') );
               sNombre := CampoAsVar( FieldByName('US_CORTO') );

               {Queda pendiente como se, que viene desde Tress el Cambio}
                    iEmpleadoNuevo := CampoAsVar(FieldByName('CB_CODIGO'));
                    sCompanyNuevo := CampoAsVar(FieldByName('CM_CODIGO'));
               {*****}
          end;

          case UpdateKind of
               ukModify:
               begin
                    if lCambiaEmpresa then
                    begin
                         //Si cambia la empresa, tambien cambia el empleado.
                         //Al Empleado anterior de la empresa anterior, hay que quitarle el Usuario
                         GrabaEmpleadoTress( iEmpleadoAnt, 0, sCompanyAnt );
                         //Al empleado nuevo en la empresa nueva, hay que ponerle el usuario
                         GrabaEmpleadoTress( iEmpleadoNuevo, iUsuario, sCompanyNuevo );
                    end
                    else if lCambiaEmpleado then
                    begin
                         //Si cambio el Empleado, al Empleado anterior hay que quitarle el Usuario
                         GrabaEmpleadoTress( iEmpleadoAnt, 0, sCompanyAnt );
                         //Al empleado nuevo, hay que ponerle el usuario
                         GrabaEmpleadoTress( iEmpleadoNuevo, iUsuario, sCompanyNuevo );
                    end;
               end;
               ukInsert:
               begin
                    //Cuando el registro de Usuario es nuevo solo hay que asignar el empleado.
                    GrabaEmpleadoTress( iEmpleadoNuevo, iUsuario, sCompanyNuevo );
               end;
               ukDelete:
               begin
                    //Al borrar un registro de usuario, hay que desasignar el usuario del empleado.
                    GrabaEmpleadoTress( iEmpleadoAnt, 0, sCompanyAnt );
               end;
          end;

     end;

     sTitulo := Format( '%s Usuario: %d:%s', [ aTipoRegistro[ UpdateKind ], iUsuario, sNombre] );
     oZetaProvider.EscribeBitacoraComparte( tbNormal, clbCatalogoUsuarios, NullDateTime, sTitulo, FTextoBitacora, FUsuario);
     FTextoBitacora:= VACIO;
end;

procedure TUsuarioData.GetCompany( const sCompany : string;  var lEncontro : boolean; var oEmpresa : OleVariant );
begin
     lEncontro := FALSE;
     with cdsCompanys do
     begin
          if ( Active = FALSE ) then
             CargaCompanys;
          //Si no existe la empresa probablemente la cambiaron, borraron ... entonces no hacemos nada.
          if Locate( 'CM_CODIGO', sCompany, [] ) then
          begin
               lEncontro := TRUE;
               oEmpresa := VarArrayOf( [ FieldByName( 'CM_DATOS' ).AsString,
                                         FieldByName( 'CM_USRNAME' ).AsString,
                                         FieldByName( 'CM_PASSWRD' ).AsString,
                                         0,
                                         FieldByName( 'CM_NIVEL0' ).AsString,
                                         FieldByName( 'CM_CODIGO' ).AsString,
                                         1 ] );

          end;
     end;


end;


procedure TUsuarioData.GrabaEmpleadoTress( const iEmpleado, iUsuario: integer; const sCompany : string );
 var
    FEmpresaTemp, FEmpresa: OleVariant;
    lEncontroEmpresa : boolean;
begin
     //Este metodo hace lo siguiente:
     //Para la Empresa sCompany, al Empleado iEmpleado se le asigna el US_CODIGO:= iUsuario;

     GetCompany( sCompany, lEncontroEmpresa, FEmpresa );

     if lEncontroEmpresa then
     begin
        with oZetaProvider do
        begin
             FEmpresaTemp := EmpresaActiva;
             EmpresaActiva := FEmpresa;
             try
                EmpiezaTransaccion;
                try
                   ExecSQL( FEmpresa, Format( GetSQLScript( e_UPDATE_USER_COLABORA ), [ iUsuario, iEmpleado ] ) );
                   TerminaTransaccion( TRUE );
                except
                      on Error: Exception do
                      begin
                           RollBackTransaccion;
                           raise;
                      end;
                end;
             finally
                    //Se regresa a la empresa de Comparte;
                    EmpresaActiva := FEmpresaTemp;
             end;
        end;
     end;
end;

procedure TUsuarioData.CargaCompanys;
begin
      with oZetaProvider do
           cdsCompanys.Data := OpenSQL( Comparte, Format( GetSQLScript( e_COMPANYS_LIST ), [ GetTipoCompany( Ord( tc3Datos ) ) ] ), True );
end;

procedure TUsuarioData.Codifica( const sCampo: String; DeltaDS: TzProviderClientDataSet );
begin
     with DeltaDS do
     begin
          Edit;
          FieldByName( sCampo ).AsString := ZetaServerTools.Encrypt( ZetaServerTools.CampoAsVar( FieldByName( sCampo ) ) );
          Post;
     end;
end;

{function TUsuarioData.GetUsuarios(out Longitud: Integer; Grupo: Integer; out BlockSistema: WordBool): OleVariant;
begin
     SetTablaInfo( eUsuarios );
     SetFiltroGrupos( Grupo );
     with oZetaProvider do
     begin
          Result := DecodificaClaves( GetTabla( Comparte ), K_CLAVE_USUARIO );
     end;
     Longitud := StrToIntDef( LeerClaves( CLAVE_LIMITE_PASSWORD ), 0 );
     BlockSistema := zstrToBool( LeerClaves( CLAVE_SISTEMA_BLOQUEADO ) );
end;}

function TUsuarioData.DecodificaClaves( Datos: OleVariant; const sCampo: String ): OleVariant;
begin
     with cdsTemporal do
     begin
          Active := False;
          Data := Datos;
          while not Eof do
          begin
               try
                  Edit;
                  with FieldByName( sCampo ) do
                  begin
                       AsString := ZetaServerTools.Decrypt( AsString );
                  end;
                  Post;
               except
                     on Error: Exception do
                     begin
                          Cancel;
                     end;
               end;
               Next;
          end;
          MergeChangeLog;
          Result := Data;
          Active := False;
     end;
end;

procedure TUsuarioData.TransfiereEmpleado(Empresa, EmpresaDestino: OleVariant; const iEmpleado, iNewEmpleado: integer);
 var ErrorCount: integer;
     oEmpresaTemp: Olevariant;
begin
     SetTablaInfo;
     with oZetaProvider do
     begin
          FEmpresaOrigen := EmpresaActiva;
          oEmpresaTemp := EmpresaActiva;
          EmpresaActiva := Comparte;
     end;
     try
        with oZetaProvider do
        begin
             TablaInfo.Filtro := Format( 'CM_CODIGO= %s AND CB_CODIGO=%d', [ Comillas( Empresa[P_CODIGO] ), iEmpleado ] );
             cdsLista.Data := DecodificaClaves( GetTabla( Comparte ), K_CLAVE_USUARIO );
        end;

        with cdsLista do
        begin
             if IsEmpty then
                FErrores.Add(Format( 'El Empleado %d no ten�a acceso a la Empresa %s', [iEmpleado, Comillas( Empresa[P_CODIGO] )] ))
             else
             begin
                  Edit;
                  FieldByName('CM_CODIGO').AsString := EmpresaDestino[P_CODIGO];
                  FieldByName('CB_CODIGO').AsInteger := iNewEmpleado;
                  Post;
                  GrabaUsuarios( cdsLista.Delta, oZetaProvider.UsuarioActivo, ErrorCount);
             end;
        end;
     finally
            with oZetaProvider do
            begin
                 if NOT VarIsNull(oEmpresaTemp) then
                    oZetaProvider.EmpresaActiva := oEmpresaTemp;
            end;
     end;
end;


procedure TUsuarioData.ActivaDesactiva(Empresa: OleVariant; const iEmpleado: integer; const lActivar: Boolean);
 var ErrorCount: integer;
begin
     SetTablaInfo;
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( 'CM_CODIGO= %s AND CB_CODIGO=%d', [ Comillas( Empresa[P_CODIGO] ), iEmpleado ] );
          cdsLista.Data := DecodificaClaves( GetTabla( Comparte ), K_CLAVE_USUARIO );

     end;

     with cdsLista do
     begin
          if IsEmpty then
             FErrores.Add(Format( 'El Empleado %d no ten�a acceso a la Empresa %s', [iEmpleado, Comillas( Empresa[P_CODIGO] )] ))
          else
          begin
               Edit;
               if ( ( not zStrToBool(FieldByName('US_ACTIVO').AsString ) ) and ( not lActivar ) )then
               begin
                    FieldByName('CB_CODIGO').AsInteger := 0;
                    FieldByName('CM_CODIGO').AsString := VACIO;
               end;
               FieldByName('US_ACTIVO').AsString := zBoolToStr(lActivar);

               Post;
               if ( cdsLista.ChangeCount > 0 ) then
                  GrabaUsuarios( cdsLista.Delta, oZetaProvider.UsuarioActivo, ErrorCount);
          end;
     end;
     oZetaProvider.EmpresaActiva := Empresa;
end;

function TUsuarioData.AgregarUsuario( const sUsuario,sPassword,sDominioUsuario,sCorreo,sNombre:string; const iEmpleado,GrupoUsuarios,iJefe,iCodigo: Integer; const lUsaPortal: Boolean):Boolean;
 var ErrorCount: integer;
begin
     SetTablaInfo;
     with oZetaProvider do
     begin
          cdsLista.Data := DecodificaClaves( GetTabla( Comparte ), K_CLAVE_USUARIO );
     end;
     Result := False;
     with cdsLista do
     begin
          if ( ( not Locate('US_CORTO',UpperCase(sUsuario),[] ) ) or ( not Locate ('US_DOMAIN',UpperCase( sDominioUsuario ),[] ) ) )then
          begin
               Append;
               FieldByName('US_ACTIVO').AsString := K_GLOBAL_SI;
               FieldByName('CM_CODIGO').AsString := oZetaProvider.CodigoEmpresaActiva;
               FieldByName('CB_CODIGO').AsInteger := iEmpleado;
               FieldByName('US_CORTO').AsString := UpperCase( sUsuario );
               FieldByName('US_PASSWRD').AsString := sPassword;
               FieldByName('US_EMAIL').AsString := sCorreo;
               FieldByName('US_DOMAIN').AsString := UpperCase( sDominioUsuario );
               FieldByName('US_JEFE').AsInteger := iJefe;
               FieldByName('GR_CODIGO').AsInteger := GrupoUsuarios;
               FieldByName('US_PORTAL').AsString := zBoolToStr(lUsaPortal);
               FieldByName('US_NOMBRE').AsString := sNombre;
               ///Valores default
               FieldByName( 'US_CODIGO' ).AsInteger := iCodigo;
               FieldByName( 'US_CAMBIA' ).AsString := K_GLOBAL_SI;
               FieldByName( 'US_NIVEL' ).AsInteger := 1;
               FieldByName( 'US_BLOQUEA' ).AsString := K_GLOBAL_NO;
               FieldByName( 'US_DENTRO' ).AsString := K_GLOBAL_NO;
               Post;
               GrabaUsuarios( cdsLista.Delta, oZetaProvider.UsuarioActivo, ErrorCount);
               Result := True;
          end;
     end;
end;

procedure TUsuarioData.EditarAgregarUsuario( DataSet: TDataset ;const lAgregar:Boolean ; var iCodigo:Integer);
 var ErrorCount: Integer;
     oField:TField;
     i:Integer;
begin
     SetTablaInfo;
     with oZetaProvider do
     begin
          if (not lAgregar)then
             TablaInfo.Filtro := Format( 'US_CORTO = ''%s'' ', [ UpperCase( DataSet.FieldByName('US_CORTO').AsString ) ] );
          cdsLista.Data := DecodificaClaves( GetTabla( Comparte ), K_CLAVE_USUARIO );
     end;
     with cdsLista do
     begin
          if (not lAgregar)then
          begin
               Edit;
               iCodigo := FieldByName('US_CODIGO').AsInteger
          end
          else
          begin
               Append;
               FieldByName('US_CODIGO').AsInteger := iCodigo;
          end;

          for i:= 0 to DataSet.Fields.Count - 1 do
          begin
               oField := DataSet.Fields[i];
               if( ( Fields.FindField( oField.FieldName ) <> nil ) and ( FieldByName(oField.FieldName) <> nil )  )then
                   if not DataSet.FieldByName(oField.FieldName).IsNull then
                          FieldByName(oField.FieldName).Value := DataSet.FieldByName(oField.FieldName).Value;
              FieldByName('US_CORTO').AsString := UpperCase( DataSet.FieldByName('US_CORTO').AsString );
          end;
          Post;
          GrabaUsuarios( cdsLista.Delta, oZetaProvider.UsuarioActivo, ErrorCount);
     end;
end;

{$ifndef DOS_CAPAS}
procedure TUsuarioData.AjustarSupervisorEnrolado(DeltaDS: TzProviderClientDataSet);
var
    FDataset: TZetaCursor;
    FEmpresaTemp, FEmpresa: OleVariant;
    sCompany : string;
    iCB_CODIGO : integer;
    iUS_JEFE, iUsaEnrolamiento : integer;
    lEncontroEmpresa : boolean;
begin
     sCompany := VACIO;
     iCB_CODIGO := 0;
     iUsaEnrolamiento:=0;
     iUS_JEFE := 0;

     with DeltaDS  do
     begin
          if not isEmpty then
          begin
               sCompany := ZetaServerTools.CampoAsVar( FieldByName( 'CM_CODIGO' ) );
               iCB_CODIGO := ZetaServerTools.CampoAsVar( FieldByName( 'CB_CODIGO' ) );
          end;
     end;

     if ( sCompany <> VACIO ) and ( iCB_CODIGO > 0 ) then
     begin
        GetCompany( sCompany, lEncontroEmpresa, FEmpresa );

        if lEncontroEmpresa then
        begin
           with oZetaProvider do
           begin
                FEmpresaTemp := EmpresaActiva;
                EmpresaActiva := FEmpresa;
                try
                   try
                        FDataset := oZetaProvider.CreateQuery( Format( GetSQLScript( e_GET_SUPERVISOR_ENROLADO ), [ iCB_CODIGO ] ) );
                        try
                           with FDataset do
                           begin
                                Active := True;
                                iUS_JEFE := FieldByName( 'US_JEFE' ).AsInteger;
                                iUsaEnrolamiento := FieldByName( 'USA_ENROL' ).AsInteger;
                                Active := False;
                           end;
                        finally
                           FreeAndNil( FDataset );
                        end;
                   except
                         on Error: Exception do
                         begin
                              raise;
                         end;
                   end;
                finally
                       //Se regresa a la empresa de Comparte;
                       EmpresaActiva := FEmpresaTemp;
                end;
           end;
        end;

        if ( iUsaEnrolamiento = 1  ) then
        begin
          with DeltaDS  do
          begin
               Edit;
               FieldByName('US_JEFE').AsInteger := iUS_JEFE;
               Post;
          end;
        end;

     end;
end;



procedure TUsuarioData.ActualizaUsuariosEmpresa(Empresa, oDelta: OleVariant);
var
   ErrorCount : integer;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
     end;
     SetTablaInfo;
     with oZetaProvider do
     begin
          with TablaInfo do
          begin
               BeforeUpdateRecord := ActualizaUsuarioEnCatalogos;
          end;
          GrabaTabla( Empresa, oDelta, ErrorCount );
     end;
end;


procedure TUsuarioData.ActualizaUsuarioEnCatalogos(Sender: TObject;
  SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
  UpdateKind: TUpdateKind; var Applied: Boolean);
const
     Q_UPDATE_USER_TRESS = 'exec SP_ACTUALIZA_USUARIO %d, %d';
var
  iUsuarioAnt, iUsuarioNuevo : integer;
begin
     iUsuarioAnt := 0;
     iUsuarioNuevo := 0;

     with DeltaDS do
     begin
        if ( UpdateKind = ukModify ) then
        begin
             if CambiaCampo( FieldByName('US_CODIGO') )  then
             begin
                     iUsuarioNuevo := CampoAsVar(FieldByName('US_CODIGO'));
                     iUsuarioAnt   := CampoOldAsVar(FieldByName('US_CODIGO'));
             end;
        end;

        if ( UpdateKind = ukDelete ) then
        begin
             iUsuarioAnt   := CampoOldAsVar(FieldByName('US_CODIGO'));
        end;

        if ( iUsuarioAnt > 0 ) then
        begin
            with oZetaProvider do
            begin
               try
                  ExecSQL( EmpresaActiva, Format( Q_UPDATE_USER_TRESS, [ iUsuarioNuevo, iUsuarioAnt ] ) );
               except
                     on Error: Exception do
                     begin
                          Raise;
                     end;
               end;
            end;
        end;
     end;

     Applied := True;

end;

{$endif}
end.
