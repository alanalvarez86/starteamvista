unit DServerGlobalSeleccion;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComServ, ComObj, VCLCom, StdVcl, {BdeMts,} DataBkr, DBClient, MtsRdm, Mtx,
  {$ifndef DOS_CAPAS}
  GlobalSeleccion_TLB,
  {$endif}
  DSuperGlobal,
  DZetaServerProvider;

type
  TdmServerGlobalSeleccion = class(TMtsDataModule {$ifndef DOS_CAPAS}, IdmServerGlobalSeleccion {$endif} )
    procedure MtsDataModuleCreate(Sender: TObject);
    procedure MtsDataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
    oSuperGlobal: TdmSuperGlobal;
    {$ifndef DOS_CAPAS}
    procedure ServerActivate(Sender: TObject);
    procedure ServerDeactivate(Sender: TObject);
    {$endif}
  protected
    { Protected declarations }
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    {$ifdef DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
    {$endif}
    function GetGlobales(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GrabaGlobales(Empresa, oDelta: OleVariant; lActualizaDiccion: WordBool; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
  end;

var
  dmServerGlobalSeleccion: TdmServerGlobalSeleccion;

implementation

uses ZGlobalTress,
     ZetaServerTools;

{$R *.DFM}

class procedure TdmServerGlobalSeleccion.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

procedure TdmServerGlobalSeleccion.MtsDataModuleCreate(Sender: TObject);
begin
     {$ifndef DOS_CAPAS}
     OnActivate := ServerActivate;
     OnDeactivate := ServerDeactivate;
     {$endif}
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
     oSuperGlobal := TdmSuperGlobal.Create;
     with oSuperGlobal do
     begin
          Provider := oZetaProvider;
     end;
end;

procedure TdmServerGlobalSeleccion.MtsDataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( oSuperGlobal );
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
end;

{$ifdef DOS_CAPAS}
procedure TdmServerGlobalSeleccion.CierraEmpresa;
begin
end;
{$else}
procedure TdmServerGlobalSeleccion.ServerActivate(Sender: TObject);
begin
     oZetaProvider.Activate;
end;

procedure TdmServerGlobalSeleccion.ServerDeactivate(Sender: TObject);
begin
     oZetaProvider.Deactivate;
end;
{$endif}

function TdmServerGlobalSeleccion.GetGlobales(Empresa: OleVariant): OleVariant;
begin
     try
        oZetaProvider.EmpresaActiva := Empresa;
        with oSuperGlobal do
        begin
             Result := ProcesaLectura;
        end;
        SetComplete;
     except
           SetAbort;
           raise;
     end;
end;

function TdmServerGlobalSeleccion.GrabaGlobales(Empresa,oDelta: OleVariant; lActualizaDiccion: WordBool; out ErrorCount: Integer): OleVariant;
begin
     try
        oZetaProvider.EmpresaActiva := Empresa;
        with oSuperGlobal do
        begin
             ActualizaDiccion := lActualizaDiccion;
             Result := ProcesaEscritura( oDelta );
             ErrorCount := Errores;
        end;
        SetComplete;
     except
           SetAbort;
           raise;
     end;
end;

{$ifndef DOS_CAPAS}
initialization
  TComponentFactory.Create(ComServer, TdmServerGlobalSeleccion, Class_dmServerGlobalSeleccion, ciMultiInstance, ZetaServerTools.GetThreadingModel);
{$endif}
end.