unit DIMSSClass;

{$define EVALUA_LIQ_IMSS}
{.$define SUA_F_JREDUCIDA}
{.$undef EVALUA_LIQ_IMSS}
{$define SUA_14}
{$define FIX_CR1757}
{$define IMSS_UMA}
{.$define DESCUENTO_SUA}

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, DB, Variants,
     ZetaServerDataSet,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZCreator,
     DServerAsistencia,
     DZetaServerProvider;

const
     K_ANCHO_TIPO_MOV = 1;
     K_ANCHO_CLAVE = 2;
     K_ANCHO_TIPO_KARDEX = 6;
type
  TEmpleadoIMSS = record
    Codigo: TNumEmp;
    HayTurno: Boolean;
    HayPatron: Boolean;
    TipoJornada: eTipoJornada;
    RegistroPatronal: String;
    PatronInicial: string;        { Registro Patronal Al Inicio Del Periodo }
    PatronMovimientos: integer;   { Cuantos Movimientos De Kardex Del Registro Patronal }
    NumeroSeguro: String;
    CURP: String;
    RFC: String;
    CreditoINFONAVIT: String;
    ModuloPatron: erPatronModulo;
    Prestamo: Extended;
    PrestamoTipo: eTipoINFONAVIT;
    PrestamoInicio: TDate;
    SalarioIntegrado: TPesos;
    TasaAnterior: Extended;
    FechaBSS: TDate;
    FechaSalarioIntegrado: TDate;
    Ingreso: TDate;
    Baja: TDate;
    ApellidoPaterno: String;
    ApellidoMaterno: String;
    Nombres: String;
    Nivel1: String;
    FaltasTope: Integer;
    FaltasAC: Integer;
    FaltasACOld: Integer;
    FaltasMes: Integer;
    FaltasMesOld: Integer;
    //Datos Afilliatorios
    CodigoPostal: String;
    FechaNacimiento: TDate;
    EntidadNacimiento: String;
    Clinica:String;
    Sexo:string;
    TipoMovimiento: eTipoMovInfonavit;
    AplicaDisminucion: Boolean;
    FechaMovimiento: TDate;
    FechaSuspension: TDate;
  end;

  {$ifdef EVALUA_LIQ_IMSS}
  TLiquidacionEmpleado = record
    LE_IMSS_OB: Extended;  { IMSS Obrero - Empleado }
    LE_IMSS_PA: Extended;  { IMSS Obrero - Patron }
    LE_EYM_FIJ: Extended;  { Enfermedad y Maternidad - Fijo }
    LE_EYM_EXC: Extended;  { Enfermedad y Maternidad - Excedente }
    LE_EYMEXCP: Extended;  { Enfermedad y Maternidad - Excedente / Patron }
    LE_EYM_DIN: Extended;  { Enfermedad y Maternidad - Dinero }
    LE_EYMDINP: Extended;  { Enfermedad y Maternidad - Dinero / Patron }
    LE_EYM_ESP: Extended;  { Enfermedad y Maternidad - Especie }
    LE_EYMESPP: Extended;  { Enfermedad y Maternidad - Especie / Patron }
    LE_RIESGOS: Extended;  { Riesgos }
    LE_INV_VID: Extended;  { Invalidez y Vida }
    LE_INVVIDP: Extended;  { Invalidez y Vida / Patron }
    LE_GUARDER: Extended;  { Guarderia }
    LE_DIAS_CO: Integer;   { Dias Cotizados }
    LE_RETIRO: Extended;   { Retiro }
    LE_CES_VEJ: Extended;  { Cesantia y Vejez }
    LE_CESVEJP: Extended;  { Cesantia y Vejez / Patron }
    LE_INF_PAT: Extended;  { INFONAVIT - Patron }
    LE_INF_AMO: Extended;  { INFONAVIT - Amortizacion del Empleado }
    LE_DIAS_BM: Integer;   { Dias BM ??? }
    LE_APO_VOL: Extended;  { Aportacion Voluntaria }
    LE_TOT_IMS: Extended;  { Total IMSS }
    LE_TOT_RET: Extended;  { Total Retiro }
    LE_TOT_INF: Extended;  { Total INFONAVIT }
    LE_ACUMULA: Extended;  { Acumulado N�mina }
    LE_BIM_ANT: Extended;  { Monto bimestre anterior }
    LE_BIM_SIG: Extended;  { Monto siguiente bimestre }
    LE_VA_PAGO: Extended;  { Vacaciones pagadas }
    LE_VA_GOZO: Extended;  { Vacaciones gozadas }
    LE_PROV: Extended; { Provisi�n de vacaciones }
    LE_INF_DIAS: Integer;  //Temporal: Dias cotizados con Credito Infonavit, no se guarda en BD    
  end;
  {$endif}
  TLiquidacionIMSS = record
    LS_NUM_TRA: Integer;
    LS_DIAS_CO: Integer;
    LS_EYM_DIN: Extended;
    LS_EYM_ESP: Extended;
    LS_EYM_EXC: Extended;
    LS_EYM_FIJ: Extended;
    LS_RIESGOS: Extended;
    LS_INV_VID: Extended;
    LS_GUARDER: Extended;
    LS_RETIRO: Extended;
    LS_CES_VEJ: Extended;
    LS_APO_VOL: Extended;
    LS_INF_ACR: Extended;
    LS_INF_AMO: Extended;
    LS_INF_NAC: Extended;
    LS_INF_NUM: Integer;
    LS_ACT_IMS: Extended;
    LS_ACT_RET: Extended;
    LS_ACT_INF: Extended;
    LS_REC_IMS: Extended;
    LS_REC_INF: Extended;
    LS_REC_RET: Extended;
    LS_DIAS_BM: Integer;
    LS_NUM_BIM: Integer;
    LS_SUB_IMS: Extended;
    LS_TOT_IMS: Extended;
    LS_TOT_MES: Extended;
    LS_SUB_RET: Extended;
    LS_TOT_RET: Extended;
    LS_SUB_INF: Extended;
    LS_TOT_INF: Extended;
    LS_IMSS_PA: Extended;
    LS_IMSS_OB: Extended;
    { Para uso futuro }
    LS_ACT_AMO: Extended;
    LS_ACT_APO: Extended;
    LS_REC_AMO: Extended;
    LS_REC_APO: Extended;
  end;
  TIMSS = class( TObject )
  private
    { Private declarations }
    FCreator: TZetaCreator;
    FPatron: String;
    FTextoTempRastreo: String;
    FTipoIMSS: eTipoLiqIMSS;
    FTipoPeriodo: eTipoLiqMov;
    FBimInicio: TDate;
    FBimMedio: TDate;
    FBimFin: TDate;
    FLiqInicial: TDate;
    FLiqFinal: TDate;
    FDiasBim: Integer;
    FLiqIMSS: TLiquidacionIMSS;
    {$ifdef EVALUA_LIQ_IMSS}
    FLiqEmpleado: TLiquidacionEmpleado;
    {$endif}
    FYear: Integer;
    FMes: Integer;
    FPrestamoTope: Extended;
    FUsarUMA: Boolean; // DES US #13122 Infonavit - Descuento tipo # veces UMA...(Liquidaci�n IMSS)
    FFormulaPercepciones: String;
    FFormulaDias: String;
    FPrimaRiesgo: Extended;
    FEmpleado: TEmpleadoIMSS;
    FMovtosFechaBaja: Boolean;
    FConHomoclave: Boolean;
    FCuotaFija: Boolean;
    FValidarCURPvsRFC: Boolean;
    FBorrarEmpleados: Boolean;
    FEsBimestre: Boolean;
    FEnviarSUA: Boolean;
    FIncluirFI: Boolean;
    FSuspenBimMed: Boolean;
    {$ifdef EVALUA_LIQ_IMSS}
    FEvaluandoIMSS: Boolean;
    FRastreoINFO: Boolean;
    FFechaKardexInicial: TDate;
    FFechaKardexFinal: TDate;
    FHuboReinicioCredito: Boolean;
    {$endif}
    FSeguroVivienda: TPesos;
    FBorrarMovimientos: eBorrarMovimSUA;
    {$ifdef SUA_14}
    FUsarAcumuladoAmortiza: Boolean;
    FAmortizacionAcumulada: TZetaCursor;
    {$endif}
    FEnviaSuspenFinBim: Boolean;

    FUsaPermisoDiasHabiles : Boolean;
    {$ifndef ASISTENCIA_DLL}
    FServerAsistencia  : TdmServerAsistencia;
    {$endif}

    { Datasets }
    FIngreso: TZetaCursor;
    FAportacion: TZetaCursor;

    {$ifdef DOS_CAPAS}
    FFaltas: TZetaCursorLocate;
    {$else}
    {$ifdef FIREBIRD}
    FFaltas: TZetaCursorLocate;
    {$else}
    FFaltas: TZetaCursor;
    {$endif}
    {$endif}
    FFaltasSinPrenomina: TZetaCursor;
    FIncapacidadPermiso: TZetaCursor;
    FKardex: TZetaCursor;
    FKardexAnterior: TZetaCursor;
    FKardexSalario: TZetaCursor;
    FKardexInfonavit: TZetaCursor;
    FMovtoEscribe: TZetaCursor;
    FEmpleadoAgrega: TZetaCursor;
    FEmpleadoModifica: TZetaCursor;
    FTurno: TZetaCursor;
    FRPatron: TZetaCursor;
    FLeeTasa: TZetaCursor;

    { Usado para ExportarSUA }
    FSUADataset: TServerDataset;
    FSUAMovtos: TServerDataset;
    FSUAIncapaci: TServerDataset;
    FSUAInfonavit: TServerDataset;

    FCursorHistInfonavit: TZetaCursor;
    FCursorInfonavitVigente: TZetaCursor;
    FTasaInfonavit: TZetaCursor;

    FInicioAmortizacion: eInicioAmortizacion;

    {Mejora descuento INFONAVIT en nomina AV Marzo/2011  Version 2011-B4 }
    FFactorNomInfo : TPesos;
    FUsarFactorNomInfo  : Boolean;
    {Mejora descuento INFONAVIT en nomina AV Julio/2011  Version 2011 }
    FFechaLimiteFaltas : TDate;
    FUsarLimiteRevisionFaltas : boolean;

    function oZetaProvider: TdmZetaServerProvider;
    function FaltasCheckTope: Boolean;
    function GetAportacionVoluntaria(const iEmpleado: Integer): Extended;
    function PuedeProcesar: Boolean;
    function GetAusentismoSUA( const iFaltas: Integer ): Integer;
    function GetSalarioKardex( const dFecha: TDate ): TPesos;
    function GetSalarioSua( const dIntegrado, dFecha: TDate ): TPesos;
    function GetSUAOrden( const sTipo: string ): string;
    {$ifdef EVALUA_LIQ_IMSS}
    function GetFechaInicial: TDate;
    function GetFechaFinal: TDate;
    function Rastreador: TRastreador;
    procedure ObtienePrimaRiesgo( const sPatron: String; const dFecha: TDate );
    procedure IMSSPagosBegin( const iYear, iMes: Integer; const Tipo: eTipoLiqIMSS; const lMovtosFechaBaja, lCuotaFija: Boolean; const rSeguroVivienda: TPesos );
    procedure IMSSPagosEnd;
    {$endif}
    procedure InitPrimas;
    procedure InitSalMin;
    // DES US #13122 Infonavit - Descuento tipo # veces UMA...(Liquidaci�n IMSS)
    procedure InitUMA;
    procedure PreparaQuerysBegin;
    procedure PreparaQuerysEnd;
    procedure PreparaKardex( const dFecInicial: TDate );{$ifdef EVALUA_LIQ_IMSS}overload;{$endif}
    {$ifdef EVALUA_LIQ_IMSS}
    procedure PreparaKardex( const dFecInicial, dFecFinal: TDate ); overload;
    {$endif}
    procedure PreparaKardexInfonavit( const dFecFinal: TDate );
    procedure EscribeMovtoSUA(const sTipo, sIncapacidad: String; const dMovimiento: TDate; const rDias, rMonto: TPesos);

    procedure FaltasInc;
    procedure FaltasInit;
    procedure FaltasReset( const iMes: Integer );
    procedure FaltasRestore;
    procedure FaltasStart( const dValue: TDate );
    procedure FaltasStore;
    procedure SetFechasPeriodo( const Tipo: eTipoLiqMov );
    procedure SetGlobalEmpleadoInfo( const sTurno, sPatron: String );
    procedure SetHistorialEmpleado( DataSet: TZetaCursor );
    {$ifdef EVALUA_LIQ_IMSS}
    procedure SetGlobalPatron( const sPatron: String );
    procedure SetGlobales( const iYear, iMes: Integer );
    procedure SetGlobalesPagos( const iYear, iMes: Integer; const Tipo: eTipoLiqIMSS );
    {$else}
    procedure SetGlobales(const sPatron: String; const iYear, iMes: Integer);
    procedure SetGlobalesPagos(const sPatron: String; const iYear, iMes: Integer; const Tipo: eTipoLiqIMSS);
    {$endif}
    procedure PreparaInfonavitEmpleado( const iEmpleado: Integer; dFechaFin: TDateTime );
    procedure PreparaHistInfonavitEmp( const iEmpleado: Integer; dInicial, dFinal: TDateTime );

    function GetDisminucionTasa( const dFecha: TDate ): TPesos;
    function GetPrestamoInicio( const dFecha: TDate ): TDate;
  protected
    { Protected declarations }
    {$ifdef EVALUA_LIQ_IMSS}
    property FechaInicial: TDate read GetFechaInicial;
    property FechaFinal: TDate read GetFechaFinal;
    {$endif}
  public
    { Public declarations }
    constructor Create( oCreator: TZetaCreator );
    {$ifdef EVALUA_LIQ_IMSS}
    property LiquidacionEmpleado: TLiquidacionEmpleado read FLiqEmpleado;
    property Empleado: TEmpleadoIMSS read FEmpleado;

   {Mejora descuento INFONAVIT en nomina AV Marzo/2011  Version 2011-B4 }
    property FactorNomInfo : TPesos read FFactorNomInfo;
    property UsarFactorNomInfo : boolean read FUsarFactorNomInfo;

    property FechaLimiteFaltas : TDate read FFechaLimiteFaltas;
    property UsarLimiteRevisionFaltas : boolean read FUsarLimiteRevisionFaltas;

    function CalculaMontoVacaciones( const rDiasVaca: TPesos ): TPesos;
    procedure EvaluarIMSSPagosBegin( const lMovtosFechaBaja, lCuotaFija: Boolean; const rSeguroVivienda: TPesos; const iYear, iMes: Integer );
    procedure EvaluarIMSSPagosSetFechas( const dInicial, dFinal: TDateTime; const lRastreoINFO: Boolean );
    procedure EvaluarIMSSPagosEnd;
    {$endif}
    procedure SetGlobalEmpleado(Dataset: TDataset; const dFechaKardex: TDate = 0 );

    {Mejora descuento INFONAVIT en nomina AV Mayo/2011  Version 2011-B4, se agrega el Codigo de Reg.Patronal Inicial como parametro opcional }
    procedure SetGlobalEmpleadoCursor(Dataset: TZetaCursor; const dFechaKardex: TDate = 0; const sPatronInicial : string = '' );
    procedure SetVariablesConciliacion( const r_Acumula, r_bim_ant, r_bim_sig, r_vac_gozo, r_vac_pago, r_provision: TPesos );

    {Mejora descuento INFONAVIT en nomina AV Marzo/2011  Version 2011-B4 }
    procedure UsarFactorNominaInfonavit( const r_FactorNominaInfo : TPesos );
    procedure RemoverFactorNominaInfonavit;

    {Mejora descuento INFONAVIT en nomina AV Julio/2011  Version 2011 }
    procedure UsarLimiteRevisionFaltasInfonavit( const d_FechaLimite : TDate );
    procedure RemoverLimiteRevisionFaltasInfonavit;

    procedure CalcularIMSSPagos;
    procedure CalcularIMSSPagosBegin(const sPatron: String; const iYear, iMes: Integer; const Tipo: eTipoLiqIMSS; const lMovtosFechaBaja, lCuotaFija: Boolean; const rSeguroVivienda: TPesos );
    procedure CalcularIMSSPagosEnd;
    procedure ExportarSUA( const lEmpleados, lMovimientos: Boolean );
    procedure ExportarSUABegin( const sPatron: String; const iYear, iMes: Integer; const lHomoClave, lEnviarFaltaBaja, lCuotaFija, lValidarCURPvsRFC, lBorrarEmpleados, lSuspenFinBim: Boolean; const BorrarMovimientos: eBorrarMovimSUA; Empleados, Movimientos, Incapacidades,Infonavit: TServerDataset );
    procedure ExportarSUAEnd;
    procedure RevisionSUA( Dataset: TDataset );
    procedure RevisionSUABegin;
    procedure RevisionSUAEnd;
    procedure EscribeIncapaSUA(const dIN_SUA_INI,dIN_SUA_FIN:TDate;const iDias:Integer;const TipoIncapacidad: eIncidenciaIncapacidad;const fiSecuela : eFinIncapacidad;const miControl:eMotivoIncapacidad;const fTasa:TTasa;const sFolio,sIN_TIPO:string);

    procedure RevisaFaltas(const dInicial, dFinal: TDate; var iFaltas, iIncapacidades: Integer);
  end;

procedure FechasBimestre( const iYear, iMes: Word; var dInicial, dFinal: TDate );

implementation

uses ZetaCommonTools,
     ZetaServerTools,
     ZetaSQLBroker,
     ZGlobalTress,
     DQueries,
     DPrimasIMSS,
     DSalMin;

const
     aTipoMov: array[ eTipoMovInfonavit ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ('Inicio   ','Suspensi�n','Reinicio  ','Cambio   ','Cambio   ','Cambio   ' );
     K_FACTOR_INTEGRACION = 1.0452;
{
     ER ( 02/Mar/06 ): Se pasaron a ZetaCommonClasses.pas
     K_TIPO_SUA_BAJA = '02';
     K_TIPO_SUA_CAMBIO = '07';
     K_TIPO_SUA_REINGRESO = '08';
     K_TIPO_SUA_VOLUNTARIA = '09';
     K_TIPO_SUA_AUSENTISMO = '11';
     K_TIPO_SUA_INCAPACIDAD = '12';
}
     K_M_OK = VACIO;
     K_M_ALTA = 'A';
     K_M_BAJA = 'B';
     K_M_MODIFICACION = 'MS';
     K_M_REINGRESO = 'R ';
     Q_APORTACION_VOLUNTARIA = 0;
     Q_IMSS_GET_INGRESO = 1;
     Q_IMSS_GET_PRIMA = 2;
     Q_IMSS_KARDEX = 3;
     Q_IMSS_KARDEX_ANTERIOR = 4;
     Q_IMSS_SALARIO_KARDEX = 5;
     Q_IMSS_INCAPACI_PERM = 6;
     Q_IMSS_FALTAS = 7;
     Q_LIQUIDACION_EXISTE = 8;
     Q_LIQUIDACION_AGREGA = 9;
     Q_LIQUIDACION_PRESET = 10;
     Q_LIQUIDACION_MODIFICA = 11;
     Q_EMPLEADO_AGREGA = 12;
     Q_EMPLEADO_BORRA = 13;
     Q_EMPLEADO_MODIFICA = 14;
     Q_MOVIMIENTO_ESCRIBE = 15;
     Q_TURNO_LEE = 16;
     Q_PATRON_LEE = 17;
     Q_TASA_INFONAVIT = 18;
     Q_ACUM_AMORTIZACION = 19;
     Q_IMSS_FALTAS_SIN_PRENOMINA = 20;
     Q_IMSS_LEE_HIST_INFONAVIT = 21;
     Q_IMSS_INFONAVIT_VIGENTE = 22;
     Q_LEE_DISM_TASA_INFONAVIT = 23;
     Q_KARDEX_INFONAVIT = 24;

function GetNombreSUA( sNombre: String ): String;
var
   sChar, sUltimo: String[ 1 ];
   i: Integer;
begin
     { Si vienen minusculuas regresa vacio .... }
     sUltimo := 'X';
     sNombre := UpperCase( Trim( sNombre ) );
     Result := '';
     for i := 1 to Length( sNombre ) do
     begin
          sChar := Copy( sNombre, i, 1 );
          case sChar[ 1 ] of
               '#': sChar := '�';
               '�', '�': sChar := 'A';
               '�', '�': sChar := 'E';
               '�', '�': sChar := 'I';
               '�', '�': sChar := 'O';
               '�', '�': sChar := 'U';
               '�': sChar := '�';
          end;
          if ( ( sChar >= 'A' ) and ( sChar <= 'Z' ) ) or ( sChar = '�' ) or ( sChar = ' ' ) then
          begin
               if not ( ( sChar = ' ' ) and ( sUltimo = ' ' ) ) then
                  Result := Result + sChar;
               sUltimo := sChar;
          end;
     end;
end;

{
ER ( 02/Mar/06 ): Se pas� a ZetaCommonTools.pas

function EsBimestre( const iMes: Word ): Boolean;
begin
     Result := ( iMes in [ 2, 4, 6, 8, 10, 12 ] );
end;
}

function EsteMes( const dValue: TDate ): TDate;
begin
     {
     Aunque en apariencia no hace nada, aqu� se puede realizar en un solo lugar
     la interpretaci�n de eInicioAmortizacion.iaEsteMes
     }
     Result := dValue;
end;

function SiguienteMes( const dValue: TDate ): TDate;
var
   iDia, iMes, iYear: Word;
begin
     DecodeDate( dValue, iYear, iMes, iDia );
     if ( iDia = 1 ) then
        Result := dValue
     else
     begin
          iDia := 1;
          case iMes of
               1..11: iMes := iMes + 1;
               else { 12 }
               begin
                    iMes := 1;
                    iYear := iYear + 1;
               end;
          end;
          Result := EncodeDate( iYear, iMes, iDia );
     end;
end;

function SiguienteBimestre( const dValue: TDate ): TDate;
var
   iDia, iMes, iYear: Word;
begin
     DecodeDate( dValue, iYear, iMes, iDia );
     if ( iDia = 1 ) and ( iMes in [ 1, 3, 5, 7, 9, 11 ] ) then
        Result := dValue
     else
     begin
          iDia := 1;
          case iMes of
               1, 3, 5, 7, 9: iMes := iMes + 2;
               2, 4, 6, 8, 10: iMes := iMes + 1;
               else { 11, 12 }
               begin
                    iMes := 1;
                    iYear := iYear + 1;
               end;
          end;
          Result := EncodeDate( iYear, iMes, iDia );
     end;
end;

procedure FechasMes( const iYear, iMes: Word; var dInicial, dFinal: TDate );
begin
     { D�a primero del mes }
     dInicial := EncodeDate( iYear, iMes, 1 );
     { D�a �ltimo del mes, es el d�a anterior al d�a primero del siguiente mes }
     { Excepto Diciembre por el cambio de a�o }
     if ( iMes = 12 ) then
        dFinal := EncodeDate( iYear, iMes, 31 )
     else
         dFinal := EncodeDate( iYear, ( iMes + 1 ), 1 ) - 1;
end;

procedure FechasBimestre( const iYear, iMes: Word; var dInicial, dFinal: TDate );
begin
     FechasMes( iYear, iMes, dInicial, dFinal );
     if EsBimestre( iMes ) then
        dInicial := EncodeDate( iYear, ( iMes - 1 ), 1 )
     else
        dFinal :=  EncodeDate( iYear, iMes, ZetaCommonTools.MaxDay( iYear, iMes ) );
end;

// DES US #13122 Infonavit - Descuento tipo # veces UMA...(Liquidaci�n IMSS)
function CalculaCuota( const rVeces, rBase, rDias, rUnidadTope: TPesos; const rPrima: TTasa; var rTopado: TPesos ): TPesos;
begin
     if ( rVeces > 0 ) then
         rTopado := ZetaCommonTools.rMin( rBase, ( rVeces * rUnidadTope ) )
     else
         rTopado := rBase;
     Result := ZetaCommonTools.Redondea( rTopado * rDias * rPrima / 100 );
end;

function CalculaCuotaFija( const rPrestamo, rDiasPresta: TPesos; const iDiasBim: Integer ): TPesos;
begin
     Result := ( ( rPrestamo * 2 ) / iDiasBim ) * rDiasPresta;
end;

function ValidaCURPyRFC( sCurp, sRFC: String; var sSignos: String ): Boolean;
begin
     sSignos := '';
     sCurp := Trim( UpperCase( sCurp ) );
     sRFC := Trim( UpperCase( sRFC ) );

     if ( CompareStr( Copy( sCurp, 1, 10 ), Copy( sRFC, 1, 10 ) ) <> 0 ) then
        sSignos := 'Primeros 10 Diferente De RFC';

     Result := StrVacio( sSignos );
end;

function ValidaCURP( sCurp: String; var sSignos: String ): Boolean;
begin
     sSignos := '';
     sCurp := Trim( UpperCase( sCurp ) );

     if not ( ( Copy( sCurp, 11, 1 ) = 'H' ) or ( Copy( sCurp, 11, 1 ) = 'M' ) ) then
        sSignos := 'Caracter 11 Debe Ser H/M'
     else if ( Length( sCurp ) < 18 ) then
        sSignos := 'Tiene blancos'
     else if ( ( Length( sCurp ) = 18 ) and ( Copy( sCurp, 18, 1 ) <> ZetaCommonTools.GetDigitoCurp( sCurp ) ) ) then
        sSignos := 'D�gito Verificador';

     Result := StrVacio( sSignos );
end;

function rMinSinNulos( const rValue1, rValue2: Real ): Real;
begin
     if ( rValue1 = 0 ) then
        Result:= rValue2
     else if ( rValue2 = 0 ) then
         Result:= rValue1
     else
         Result:= rMin( rValue1, rValue2 );
end;

function GetSQLScript( const iScript: Integer ): String;
begin
     case iScript of
          {$ifdef INTERBASE}
          Q_APORTACION_VOLUNTARIA: Result := 'select RESULTADO from SP_AS( :Concepto, :MesIni, :MesFin, :Year, :Empleado )';
          Q_ACUM_AMORTIZACION: Result := 'select RESULTADO from SP_AS( :Concepto, :MesIni, :MesFin, :Year, :Empleado )';
          {$endif}
          {$ifdef MSSQL}
          Q_APORTACION_VOLUNTARIA: Result := 'select DBO.SP_AS( :Concepto, :MesIni, :MesFin, :Year, :Empleado ) AS RESULTADO';
          Q_ACUM_AMORTIZACION: Result := 'select DBO.SP_AS( :Concepto, :MesIni, :MesFin, :Year, :Empleado ) AS RESULTADO';
          {$endif}
          Q_IMSS_GET_INGRESO: Result := 'select MIN( A.CB_ALTA ) CB_FEC_ING from ALTABAJA A '+
                                        'left outer join KARDEX K on '+
                                        '( K.CB_CODIGO = A.CB_CODIGO ) and '+
                                        '( K.CB_TIPO = ''%s'' ) and '+
                                        '( K.CB_FECHA = A.CB_BAJA ) where '+
                                        '( A.CB_CODIGO = :Empleado ) and '+
                                        '( A.CB_PATRON = :Patron ) and '+
                                        '( A.CB_ALTA <= :FechaFinal ) and '+
                                        '( ( A.CB_BAJA is NULL ) or ( K.CB_FECHA_2 >= :FechaInicial ) )';
          Q_IMSS_GET_PRIMA: Result := 'select RT_PRIMA from PRIESGO where '+
                                      '( TB_CODIGO = :Patron ) and ( RT_FECHA <= :Fecha )'+
                                      'order by RT_FECHA DESC';
          Q_IMSS_KARDEX: Result := 'select CB_FECHA, CB_TIPO, CB_SAL_INT, CB_FECHA_2, CB_REINGRE, CB_NOMINA ' +
{$ifdef DESCUENTO_SUA}
                                   ', CB_ZONA_GE ' +
{$endif}
                                   'from KARDEX where '+
                                   '( CB_CODIGO = :Empleado ) and '+
                                   '( CB_PATRON = :Patron ) and '+
                                   '( ( ( CB_TIPO = ''ALTA'' ) and ( CB_FECHA between :FechaInicial1 and :FechaFinal1 ) ) or '+
                                   '( ( CB_TIPO IN ( ''BAJA'', ''CAMBIO'' ) ) and ( CB_FECHA_2 between :FechaInicial2 and :FechaFinal2 ) ) ) '+
                                   'order by CB_FECHA, CB_NIVEL';
          Q_IMSS_KARDEX_ANTERIOR: Result := 'select CB_FECHA, CB_TIPO, CB_SAL_INT ' +
{$ifdef DESCUENTO_SUA}
                                            ', CB_ZONA_GE ' +
{$endif}
                                            'from KARDEX where '+
                                            '( CB_CODIGO = :Empleado ) and '+
                                            '( CB_PATRON = :Patron ) and '+
                                            '( ( ( CB_TIPO = ''ALTA'' ) and ( CB_FECHA < :FechaInicial1 ) ) or '+
                                            '( ( CB_TIPO IN ( ''BAJA'', ''CAMBIO'' ) ) and ( CB_FECHA_2 < :FechaInicial2 ) ) ) '+
{$ifdef ANTES}

                                            'order by CB_FECHA desc';
{$else}
                                            'order by CB_FECHA desc, CB_NIVEL desc';
{$endif}

{$ifdef INTERBASE}
          Q_IMSS_SALARIO_KARDEX: Result := 'select CB_SAL_INT from SP_FECHA_KARDEX( :FECHA, :EMPLEADO )';
{$endif}
{$ifdef MSSQL}
          Q_IMSS_SALARIO_KARDEX: Result := 'select DBO.SP_KARDEX_CB_SAL_INT( :FECHA, :EMPLEADO ) AS CB_SAL_INT';
{$endif}
          Q_IMSS_INCAPACI_PERM: Result := 'select 1 TIPO, IN_SUA_INI, IN_SUA_FIN, IN_DIAS, IN_NUMERO,TB_INCIDEN,IN_TIPO,IN_TASA_IP,IN_FIN,IN_MOTIVO,TB_INCAPA from INCAPACI '+
                                          'join INCIDEN on ( TB_CODIGO = IN_TIPO ) where '+
                                          '( CB_CODIGO = :Empleado1 ) and '+
                                          '( IN_SUA_INI <= :FechaFinal1 ) and '+
                                          '( IN_SUA_FIN > :FechaInicial1 ) '+
                                          'union '+
                                          'select 2 TIPO, PM_FEC_INI, PM_FEC_FIN, PM_DIAS, CAST( '''' AS VARCHAR( 30 ) ),PM_CLASIFI,CAST( '''' AS CHAR( 3 ) ),CAST( 0.0 AS DECIMAL( 15,5 ) ),CAST( 0 AS SMALLINT  ),CAST( 0 AS SMALLINT  ),CAST( 0 AS SMALLINT  ) from PERMISO '+
                                          'join INCIDEN on ( TB_CODIGO = PM_TIPO ) where '+
                                          '( CB_CODIGO = :Empleado2 ) and '+
                                          '( PM_FEC_INI <= :FechaFinal2 ) and '+
                                          '( PM_FEC_FIN > :FechaInicial2 )';

          Q_IMSS_FALTAS: Result := 'select AU_FECHA from AUSENCIA '+
                                   'join INCIDEN on ( AU_TIPO = TB_CODIGO ) where '+
                                   '( CB_CODIGO = :Empleado ) and '+
                                   '( TB_INCIDEN = 1 ) and '+
                                   '( AU_FECHA between :FechaInicial and :FechaFinal )'+
                                   'order by AU_FECHA';
          Q_IMSS_FALTAS_SIN_PRENOMINA: Result := 'select FA_FEC_INI AU_FECHA, FA_DIAS DIAS  from FALTAS '+
                                                 'where FA_FEC_INI between :FechaInicial and :FechaFinal '+
                                                 'and CB_CODIGO = :Empleado '+
                                                 'and FA_DIA_HOR = ''D'' '+
                                                 'and FA_MOTIVO = 0 ';
          Q_LIQUIDACION_EXISTE: Result := 'select COUNT(*) CUANTOS from LIQ_IMSS where '+
                                          '( LS_PATRON = :Patron ) and '+
                                          '( LS_YEAR = :Year ) and '+
                                          '( LS_MONTH = :Mes ) and '+
                                          '( LS_TIPO = :Tipo )';
          Q_LIQUIDACION_AGREGA: Result := 'insert into LIQ_IMSS( '+
                                          'LS_PATRON, '+
                                          'LS_YEAR, '+
                                          'LS_MONTH, '+
                                          'LS_TIPO, '+
                                          'LS_STATUS, '+
                                          'US_CODIGO ) values ( '+
                                          ':LS_PATRON, '+
                                          ':LS_YEAR, '+
                                          ':LS_MONTH, '+
                                          ':LS_TIPO, '+
                                          ':LS_STATUS, '+
                                          ':US_CODIGO )';
          Q_LIQUIDACION_PRESET: Result := 'update LIQ_IMSS set '+
                                          'LS_STATUS = :LS_STATUS, '+
                                          'US_CODIGO = :US_CODIGO where '+
                                          '( LS_PATRON = :LS_PATRON ) and '+
                                          '( LS_YEAR = :LS_YEAR ) and '+
                                          '( LS_MONTH = :LS_MONTH ) and '+
                                          '( LS_TIPO = :LS_TIPO )';
          Q_LIQUIDACION_MODIFICA: Result := 'update LIQ_IMSS set '+
                                            'LS_NUM_TRA = :LS_NUM_TRA, '+
                                            'LS_DIAS_CO = :LS_DIAS_CO, '+
                                            'LS_EYM_DIN = :LS_EYM_DIN, '+
                                            'LS_EYM_ESP = :LS_EYM_ESP, '+
                                            'LS_EYM_EXC = :LS_EYM_EXC, '+
                                            'LS_EYM_FIJ = :LS_EYM_FIJ, '+
                                            'LS_RIESGOS = :LS_RIESGOS, '+
                                            'LS_INV_VID = :LS_INV_VID, '+
                                            'LS_GUARDER = :LS_GUARDER, '+
                                            'LS_RETIRO = :LS_RETIRO, '+
                                            'LS_CES_VEJ = :LS_CES_VEJ, '+
                                            'LS_APO_VOL = :LS_APO_VOL, '+
                                            'LS_INF_ACR = :LS_INF_ACR, '+
                                            'LS_INF_AMO = :LS_INF_AMO, '+
                                            'LS_INF_NAC = :LS_INF_NAC, '+
                                            'LS_INF_NUM = :LS_INF_NUM, '+
                                            'LS_ACT_IMS = :LS_ACT_IMS, '+
                                            'LS_ACT_RET = :LS_ACT_RET, '+
                                            'LS_ACT_INF = :LS_ACT_INF, '+
                                            'LS_REC_IMS = :LS_REC_IMS, '+
                                            'LS_REC_INF = :LS_REC_INF, '+
                                            'LS_REC_RET = :LS_REC_RET, '+
                                            'LS_DIAS_BM = :LS_DIAS_BM, '+
                                            'LS_NUM_BIM = :LS_NUM_BIM, '+
                                            'LS_SUB_IMS = :LS_SUB_IMS, '+
                                            'LS_TOT_IMS = :LS_TOT_IMS, '+
                                            'LS_TOT_MES = :LS_TOT_MES, '+
                                            'LS_SUB_RET = :LS_SUB_RET, '+
                                            'LS_TOT_RET = :LS_TOT_RET, '+
                                            'LS_SUB_INF = :LS_SUB_INF, '+
                                            'LS_TOT_INF = :LS_TOT_INF, '+
                                            'LS_IMSS_PA = :LS_IMSS_PA, '+
                                            'LS_IMSS_OB = :LS_IMSS_OB, '+
                                            'LS_ACT_AMO = :LS_ACT_AMO, '+
                                            'LS_ACT_APO = :LS_ACT_APO, '+
                                            'LS_REC_AMO = :LS_REC_AMO, '+
                                            'LS_REC_APO = :LS_REC_APO, '+
                                            'LS_STATUS = :LS_STATUS where '+
                                            '( LS_PATRON = :Patron ) and '+
                                            '( LS_YEAR = :Year ) and '+
                                            '( LS_MONTH = :Mes ) and '+
                                            '( LS_TIPO = :Tipo )';
          Q_EMPLEADO_AGREGA: Result := 'insert into LIQ_EMP( '+
                                       'LS_PATRON, '+
                                       'LS_YEAR, '+
                                       'LS_MONTH, '+
                                       'LS_TIPO, '+
                                       'CB_CODIGO, '+
                                       'LE_STATUS ) values ( '+
                                       ':LS_PATRON, '+
                                       ':LS_YEAR, '+
                                       ':LS_MONTH, '+
                                       ':LS_TIPO, '+
                                       ':CB_CODIGO, '+
                                       ':LE_STATUS )';
          Q_EMPLEADO_BORRA: Result := 'delete from LIQ_EMP where '+
                                      '( LS_PATRON = :Patron ) and '+
                                      '( LS_YEAR = :Year ) and '+
                                      '( LS_MONTH = :Mes ) and '+
                                      '( LS_TIPO = :Tipo )';
          Q_EMPLEADO_MODIFICA: Result := 'update LIQ_EMP set '+
                                         'LE_IMSS_OB = :LE_IMSS_OB, '+
                                         'LE_IMSS_PA = :LE_IMSS_PA, '+
                                         'LE_EYM_FIJ = :LE_EYM_FIJ, '+
                                         'LE_EYM_EXC = :LE_EYM_EXC, '+
                                         'LE_EYM_DIN = :LE_EYM_DIN, '+
                                         'LE_EYM_ESP = :LE_EYM_ESP, '+
                                         'LE_RIESGOS = :LE_RIESGOS, '+
                                         'LE_INV_VID = :LE_INV_VID, '+
                                         'LE_GUARDER = :LE_GUARDER, '+
                                         'LE_DIAS_CO = :LE_DIAS_CO, '+
                                         'LE_RETIRO = :LE_RETIRO, '+
                                         'LE_CES_VEJ = :LE_CES_VEJ, '+
                                         'LE_INF_PAT = :LE_INF_PAT, '+
                                         'LE_INF_AMO = :LE_INF_AMO, '+
                                         'LE_DIAS_BM = :LE_DIAS_BM, '+
                                         'LE_APO_VOL = :LE_APO_VOL, '+
                                         'LE_TOT_IMS = :LE_TOT_IMS, '+
                                         'LE_TOT_RET = :LE_TOT_RET, '+
                                         'LE_TOT_INF = :LE_TOT_INF, '+
                                         'LE_ACUMULA = :LE_ACUMULA, '+
                                         'LE_BIM_ANT = :LE_BIM_ANT, '+
                                         'LE_BIM_SIG = :LE_BIM_SIG, '+
                                         'LE_VA_PAGO = :LE_VA_PAGO, '+
                                         'LE_VA_GOZO = :LE_VA_GOZO, ' +
                                         'LE_PROV = :LE_PROV ' +
                                         ' where '+
                                         '( LS_PATRON = :LS_PATRON ) and '+
                                         '( LS_YEAR = :LS_YEAR ) and '+
                                         '( LS_MONTH = :LS_MONTH ) and '+
                                         '( LS_TIPO = :LS_TIPO ) and '+
                                         '( CB_CODIGO = :CB_CODIGO )';
{$ifdef INTERBASE}
          Q_MOVIMIENTO_ESCRIBE: Result := 'execute procedure INSERT_LIQ_MOV( '+
                                          ':LS_PATRON, '+
                                          ':LS_YEAR, '+
                                          ':LS_MONTH, '+
                                          ':LS_TIPO, '+
                                          ':CB_CODIGO, '+
                                          ':LM_CLAVE, '+
                                          ':LM_FECHA, '+
                                          ':LM_KAR_FEC, '+
                                          ':LM_KAR_TIP, '+
                                          ':LM_BASE, '+
                                          ':LM_TIPO, '+
                                          ':LM_DIAS, '+
                                          ':LM_INCAPAC, '+
                                          ':LM_AUSENCI, '+
                                          ':LM_INF_AMO, '+
                                          ':LM_EYM_ESP, '+
                                          ':LM_EYMESPP, '+
                                          ':LM_EYM_FIJ, '+
                                          ':LM_EYM_EXC, '+
                                          ':LM_EYMEXCP, '+
                                          ':LM_EYM_DIN, '+
                                          ':LM_EYMDINP, '+
                                          ':LM_RIESGOS, '+
                                          ':LM_INV_VID, '+
                                          ':LM_INVVIDP, '+
                                          ':LM_GUARDER, '+
                                          ':LM_RETIRO, '+
                                          ':LM_CES_VEJ, '+
                                          ':LM_CESVEJP, '+
                                          ':LM_INF_PAT )';
{$endif}
{$ifdef MSSQL}
          Q_MOVIMIENTO_ESCRIBE: Result :=   'insert into LIQ_MOV( LS_PATRON, LS_YEAR, LS_MONTH, LS_TIPO, ' +
                                            'CB_CODIGO, LM_CLAVE, LM_FECHA, LM_KAR_FEC, ' +
                                            'LM_KAR_TIP, LM_BASE, LM_TIPO, LM_DIAS, ' +
                                            'LM_INCAPAC, LM_AUSENCI, LM_INF_AMO, LM_EYM_ESP, LM_EYMESPP, ' +
                                            'LM_EYM_FIJ, LM_EYM_EXC, LM_EYMEXCP, LM_EYM_DIN, LM_EYMDINP, LM_RIESGOS, ' +
                                            'LM_INV_VID, LM_INVVIDP, LM_GUARDER, LM_RETIRO, LM_CES_VEJ, LM_CESVEJP, LM_INF_PAT ) ' +
                                            'values ( :LS_PATRON, :LS_YEAR, :LS_MONTH, :LS_TIPO, ' +
                                            ':CB_CODIGO, :LM_CLAVE, :LM_FECHA, :LM_KAR_FEC, ' +
                                            ':LM_KAR_TIP, :LM_BASE, :LM_TIPO, :LM_DIAS, ' +
                                            ':LM_INCAPAC, :LM_AUSENCI, :LM_INF_AMO, :LM_EYM_ESP, :LM_EYMESPP,  ' +
                                            ':LM_EYM_FIJ, :LM_EYM_EXC, :LM_EYMEXCP, :LM_EYM_DIN, :LM_EYMDINP, :LM_RIESGOS, ' +
                                            ':LM_INV_VID, :LM_INVVIDP, :LM_GUARDER, :LM_RETIRO, :LM_CES_VEJ, :LM_CESVEJP, :LM_INF_PAT )';
{$endif}
          Q_TURNO_LEE: Result := 'select TU_TIP_JOR from TURNO where ( TU_CODIGO = :Turno )';
          Q_PATRON_LEE: Result := 'select TB_NUMREG, TB_MODULO from RPATRON where ( TB_CODIGO = :Patron )';
          Q_TASA_INFONAVIT: Result := 'select CB_MONTO from KARDEX where ( CB_CODIGO = :Empleado ) and '+
                                      '( CB_TIPO = %s ) and ( CB_FECHA = :Fecha )';

          Q_IMSS_LEE_HIST_INFONAVIT: Result:= 'select CB_CODIGO, CB_INFTIPO, CB_INFCRED, CB_INFTASA, CB_INFDISM, KI_FECHA, KI_TIPO ' +
                                              'from KARINF where ( CB_CODIGO = :CB_CODIGO ) and ( KI_FECHA between :FECHA_INI and :FECHA_FIN ) order by KI_FECHA';

          { Se trae el Kardex de Infonavit con fecha menor al fin del bimestre, solamente se trae el kardex que cayo en fin de bimestre si este
            es diferente a suspensi�n }

          Q_IMSS_INFONAVIT_VIGENTE: Result:= 'select ' +
           {$ifdef MSSQL }
                                             'TOP 1 ' +
           {$endif}
                                             'CB_INFCRED, CB_INFTASA, CB_INFTIPO, CB_INFDISM, KI_FECHA, KI_TIPO ' +
                                             'from KARINF WHERE CB_CODIGO = :CB_CODIGO ' +
                                             'and ( ( KI_FECHA < :BIM_FIN ) OR ( ( KI_FECHA = :BIM_FIN2 ) AND ( KI_TIPO <> %d ) ) ) order by KI_FECHA desc';
          {$ifdef MSSQL }
          Q_LEE_DISM_TASA_INFONAVIT: Result:= 'select DBO.SP_DISM_TASA_INFONAVIT(:CB_CODIGO,:FECHA) AS CB_INFTASA';
          {$else}
          Q_LEE_DISM_TASA_INFONAVIT: Result:= 'select TASANUEVA  CB_INFTASA From SP_DISM_TASA_INFONAVIT(:CB_CODIGO,:FECHA)';
          {$endif}

          // DES US #20045 Skyworks - Diferencia en c�lculo de amortizaci�n de sistema TRESS vs sistema SUA
          // cuando existe modificacion de Valor Descuento en el historial de Creditos INFONAVIT y cambio de salario en el Bimestre.
          Q_KARDEX_INFONAVIT: Result:= 'select KI_TIPO, KI_FECHA from KARINF where ( CB_CODIGO = :Empleado ) and KI_TIPO in ( 0, 1, 2, 4 ) and ( KI_FECHA <= :FechaFinal )'+
                                       'order by KI_FECHA';

     else
         Result := '';
     end;
end;

{ ********** TIMSS ******** }

constructor TIMSS.Create( oCreator: TZetaCreator );
begin
     FCreator := oCreator;
     FSeguroVivienda := 0;
     {$ifdef EVALUA_LIQ_IMSS}
     FEnviarSUA := False;
     FEvaluandoIMSS := False;
     FRastreoINFO := False;
     {$endif}
     {$ifdef SUA_14}
     FUsarAcumuladoAmortiza := FALSE;
     {$endif}
     //Incluir Faltas Injustificadas(Ausentismos) se inicializa en TRUE
     FIncluirFI := True;
     FHuboReinicioCredito:= FALSE;
     FUsaPermisoDiasHabiles := FALSE;
     {$ifndef ASISTENCIA_DLL}
     FServerAsistencia := nil;
     {$endif}

     RemoverFactorNominaInfonavit;
     RemoverLimiteRevisionFaltasInfonavit;
end;

function TIMSS.oZetaProvider: TdmZetaServerProvider;
begin
     Result := FCreator.oZetaProvider;
end;

{$ifdef EVALUA_LIQ_IMSS}
function TIMSS.Rastreador: TRastreador;
begin
     Result := FCreator.Rastreador;
end;

procedure TIMSS.SetGlobalPatron( const sPatron: String );
begin
     FPatron := sPatron;
end;

procedure TIMSS.SetGlobales( const iYear, iMes: Integer );
begin
     FYear := iYear;
     FMes := iMes;
     { Temporal: Aqu� se deducen de MES y A�o de Liquidaci�n }
     FechasMes( FYear, FMes, FBimMedio, FBimFin );
     FEsBimestre := EsBimestre( FMes );
     if FEsBimestre then { El D�a primero del Mes Anterior }
     begin
          FBimInicio := EncodeDate( FYear, ( FMes - 1 ), 1 );
          FDiasBim   := ( Trunc( FBimFin ) - Trunc( FBimInicio ) + 1 );
     end
     else                         { El D�a Primero del mismo mes }
     begin
          FBimInicio := FBimMedio;
          FDiasBim   := ( Trunc( EncodeDate( iYear, iMes + 1, ZetaCommonTools.MaxDay( iYear, iMes + 1 ) ) ) -
                          Trunc( FBimInicio ) + 1 );
     end;
     FSuspenBimMed:= TRUE;
end;

procedure TIMSS.SetGlobalesPagos( const iYear, iMes: Integer; const Tipo: eTipoLiqIMSS );
begin
     SetGlobales( iYear, iMes );
     FTipoIMSS := Tipo;
     with oZetaProvider do
     begin
          InitGlobales;
          // Correcci�n de bug #13564: No aplica tope de amortizaci�n cuando el periodo cubre d�as de dos meses/bimestres
          FPrestamoTope := GetGlobalReal( K_GLOBAL_TOPE_INFONAVIT );
          // DES US #13122 Infonavit - Descuento tipo # veces UMA...(Liquidaci�n IMSS)
          FUsarUMA := IntToBool(oZetaProvider.GetGlobalInteger(K_GLOBAL_TIPO_TOPE_AMORTIZACION_INFONAVIT))
     end
end;

function TIMSS.GetFechaInicial: TDate;
begin
     if FEvaluandoIMSS then
        Result := Self.FFechaKardexInicial
     else
         Result := Self.FLiqInicial;
end;

function TIMSS.GetFechaFinal: TDate;
begin
     if FEvaluandoIMSS then
        Result := Self.FFechaKardexFinal
     else
         Result := Self.FLiqFinal;
end;
{$else}
procedure TIMSS.SetGlobales(const sPatron: String; const iYear, iMes: Integer );
begin
     FPatron := sPatron;
     FYear := iYear;
     FMes := iMes;
     { Temporal: Aqu� se deducen de MES y A�o de Liquidaci�n }
     FechasMes( FYear, FMes, FBimMedio, FBimFin );
     FEsBimestre := EsBimestre( FMes );
     if FEsBimestre then { El D�a primero del Mes Anterior }
        FBimInicio := EncodeDate( FYear, ( FMes - 1 ), 1 )
     else                         { El D�a Primero del mismo mes }
         FBimInicio := FBimMedio;
end;

procedure TIMSS.SetGlobalesPagos(const sPatron: String; const iYear, iMes: Integer; const Tipo: eTipoLiqIMSS );
begin
     SetGlobales( sPatron, iYear, iMes );
     FTipoIMSS := Tipo;
     with oZetaProvider do
     begin
          InitGlobales;
          if FEsBimestre then { El D�a primero del Mes Anterior }
             FPrestamoTope := GetGlobalReal( K_GLOBAL_TOPE_INFONAVIT )
          else
              FPrestamoTope := 0;
     end
end;
{$endif}

procedure TIMSS.SetGlobalEmpleadoCursor( Dataset: TZetaCursor; const dFechaKardex: TDate; const sPatronInicial : string );
begin
     with Dataset do
     begin
          with FEmpleado do
          begin
               Codigo := FieldByName( 'CB_CODIGO' ).AsInteger;
               NumeroSeguro := FieldByName( 'CB_SEGSOC' ).AsString;
               {$ifdef EVALUA_LIQ_IMSS}
               if FEvaluandoIMSS then
               begin
                    if StrLleno( sPatronInicial ) then
                       PatronInicial := sPatronInicial
                    else
                        PatronInicial := FieldByName( 'CB_PATRON' ).AsString;

                    PatronMovimientos := 0;
                    SetGlobalPatron( PatronInicial );
                    ApellidoPaterno := VACIO;
                    ApellidoMaterno := VACIO;
                    Nombres := VACIO;
                    Nivel1 := VACIO;
                    RFC := VACIO;
                    CURP := VACIO;
                    CodigoPostal := VACIO;
                    FechaNacimiento := NullDateTime;
                    EntidadNacimiento := VACIO;
                    Clinica := VACIO;
                    Sexo := VACIO;
                    ObtienePrimaRiesgo( PatronInicial, GetFechaFinal );
               end
               else
               begin
                    PatronInicial := FieldByName( 'PATRON_INI' ).AsString;
                    PatronMovimientos := FieldByName( 'PATRON_MOV' ).AsInteger;
                    ApellidoPaterno := FieldByName( 'CB_APE_PAT' ).AsString;
                    ApellidoMaterno := FieldByName( 'CB_APE_MAT' ).AsString;
                    Nombres := FieldByName( 'CB_NOMBRES' ).AsString;
                    with FieldByName( 'CB_NIVEL1' ) do
                    begin //CV
                         Nivel1 := ZetaCommonTools.PadR( AsString, 6 );
                    end;
                    RFC := FieldByName( 'CB_RFC' ).AsString;
                    CURP := FieldByName( 'CB_CURP' ).AsString;
                    //Datos afiliatorios
                    CodigoPostal := FieldByName( 'CB_CODPOST' ).AsString;
                    FechaNacimiento := FieldByName( 'CB_FEC_NAC' ).AsDateTime;
                    EntidadNacimiento := FieldByName( 'ENT_NAC' ).AsString;
                    Clinica := FieldByName( 'CB_CLINICA' ).AsString;
                    Sexo := FieldByName( 'CB_SEXO' ).AsString;
               end;
               {$endif}

               PrestamoInicio := FieldByName('CB_INF_INI').AsDateTime ;
               { Limpia datos de Infonavit, menos el inicio del cr�dito �ste no cambia}
               CreditoINFONAVIT := VACIO;
               Prestamo := 0;
               PrestamoTipo := eTipoInfonavit( tiNoTiene );
               TasaAnterior := 0;
               AplicaDisminucion:= FALSE;
               FechaMovimiento:= NullDateTime;
               FechaSuspension:= NullDateTime;

               FechaBSS  := FieldByName( 'CB_FEC_BSS' ).AsDateTime;
               SalarioIntegrado := FieldByName( 'CB_SAL_INT' ).AsFloat;
               FechaSalarioIntegrado := FieldByName( 'CB_FEC_INT' ).AsDateTime;

               { Toma los datos de Infonavit a inicio de Bimestre }
               PreparaInfonavitEmpleado( Codigo, FechaDef( dFechaKardex, FBimFin ) );
               { Asigna los datos de Infonavit a inicio de Bimestre }
               with FCursorInfonavitVigente do
               begin
                    { 24/Feb/2009: Pueden existir suspensiones a medio bimestre}
                    if ( not IsEmpty ) then
                    begin
                         if ( FieldByName('KI_TIPO').AsInteger <> Ord( infoSuspen ) ) or
                            ( FSuspenBimMed and ( FieldByName('KI_FECHA').AsDateTime > FBimInicio ) and ( FieldByName('KI_FECHA').AsDateTime < FBimFin ) ) then
                         begin
                              SetHistorialEmpleado( FCursorInfonavitVigente );
                         end;
                    end;
               end;

               { AP(22/May/2008): Ya no se usa el proceso de reducci�n de Tasa, ahora se investiga en cada c�lculo }
               if ( PrestamoTipo = tiPorcentaje ) and ( AplicaDisminucion ) then
               begin
                    Prestamo:= GetDisminucionTasa(FBimInicio); //Se lo trae siempre a principio de bimestre
               end;

               // Si la fecha de inicio es despues de la fecha final traersela a esa fecha
               FEmpleado.PrestamoInicio:= GetPrestamoInicio( FechaDef( dFechaKardex, FBimFin ) );

          end;
          SetGlobalEmpleadoInfo( FieldByName( 'CB_TURNO' ).AsString, FieldByName( 'CB_PATRON' ).AsString );
     end;
end;

procedure TImss.SetHistorialEmpleado( Dataset: TZetaCursor );
begin
     with Dataset do
     begin
          with FEmpleado do
          begin
               CreditoINFONAVIT := FieldByName( 'CB_INFCRED' ).AsString;
               Prestamo := FieldByName( 'CB_INFTASA' ).AsFloat;
               PrestamoTipo := eTipoInfonavit( FieldByName( 'CB_INFTIPO' ).AsInteger );
               TipoMovimiento:= eTipoMovInfonavit( FieldByName('KI_TIPO').AsInteger );
               AplicaDisminucion:= zStrToBool( FieldByName('CB_INFDISM').AsString );
               FechaMovimiento:= FieldByName('KI_FECHA').AsDateTime;

               if ( FSuspenBimMed ) and ( TipoMovimiento = infosuspen ) then
               begin
                    FechaSuspension:= FieldByName('KI_FECHA').AsDateTime;
               end;
          end;
     end;
end;

function TImss.GetPrestamoInicio( const dFecha: TDate ): TDate;
begin
     Result:= FEmpleado.PrestamoInicio;
     if ( Result <> NullDateTime ) and ( Result > dFecha ) then
     begin
          PreparaKardexInfonavit( dFecha );
          with FKardexInfonavit do
          begin
               Active:= True;
               while not EOF do
               begin
                    if ( eTipoMovInfonavit( FieldByName('KI_TIPO').AsInteger ) in [ infoInicio, infoReinicio ] ) then
                       Result:= FieldByName('KI_FECHA').AsDateTime
                    else
                        {$ifdef EVALUA_LIQ_IMSS}
                        if ( FieldByName('KI_FECHA').AsDateTime < self.FechaInicial ) then
                        {$endif}
                        Result:= FEmpleado.PrestamoInicio;          // Si hay suspensi�n se regresa a fecha actual de inicio
                    Next;
               end;
          end;
     end;
end;

procedure TIMSS.SetVariablesConciliacion( const r_Acumula, r_bim_ant, r_bim_sig, r_vac_gozo, r_vac_pago, r_provision: TPesos );
begin
     with FLiqEmpleado  do
     begin
          LE_ACUMULA := r_Acumula;
          LE_BIM_ANT := r_bim_ant;
          LE_BIM_SIG := r_bim_sig;
          LE_VA_PAGO := r_vac_pago;
          LE_VA_GOZO := r_vac_gozo;
          LE_PROV:= r_provision;
     end;
end;

procedure TIMSS.UsarFactorNominaInfonavit( const r_FactorNominaInfo : TPesos );
begin
     FFactorNomInfo := r_FactorNominaInfo;
     FUsarFactorNomInfo := True;
end;

procedure TIMSS.RemoverFactorNominaInfonavit;
begin
     FFactorNomInfo := 0.00;
     FUsarFactorNomInfo := False;
end;


procedure TIMSS.UsarLimiteRevisionFaltasInfonavit( const d_FechaLimite : TDate );
begin
     FFechaLimiteFaltas := d_FechaLimite;
     FUsarLimiteRevisionFaltas := True;
end;

procedure TIMSS.RemoverLimiteRevisionFaltasInfonavit;
begin
     FFechaLimiteFaltas := 0.00;
     FUsarLimiteRevisionFaltas := False;
end;

 
procedure TIMSS.SetGlobalEmpleado( Dataset: TDataset; const dFechaKardex: TDate );
begin
     with Dataset do
     begin
          with FEmpleado do
          begin
               Codigo := FieldByName( 'CB_CODIGO' ).AsInteger;
               NumeroSeguro := FieldByName( 'CB_SEGSOC' ).AsString;
               {$ifdef EVALUA_LIQ_IMSS}
               if FEvaluandoIMSS then
               begin
                    PatronInicial := FieldByName( 'CB_PATRON' ).AsString;
                    PatronMovimientos := 0;
                    SetGlobalPatron( PatronInicial );
                    ApellidoPaterno := VACIO;
                    ApellidoMaterno := VACIO;
                    Nombres := VACIO;
                    Nivel1 := VACIO;
                    RFC := VACIO;
                    CURP := VACIO;
                    CodigoPostal := VACIO;
                    FechaNacimiento := NullDateTime;
                    EntidadNacimiento := VACIO;
                    Clinica := VACIO;
                    Sexo := VACIO;
                    ObtienePrimaRiesgo( PatronInicial, GetFechaFinal );
               end
               else
               begin
                    PatronInicial := FieldByName( 'PATRON_INI' ).AsString;
                    PatronMovimientos := FieldByName( 'PATRON_MOV' ).AsInteger;
                    ApellidoPaterno := FieldByName( 'CB_APE_PAT' ).AsString;
                    ApellidoMaterno := FieldByName( 'CB_APE_MAT' ).AsString;
                    Nombres := FieldByName( 'CB_NOMBRES' ).AsString;
                    with FieldByName( 'CB_NIVEL1' ) do
                    begin //CV
                         Nivel1 := ZetaCommonTools.PadR( AsString, 6 );
                    end;
                    RFC := FieldByName( 'CB_RFC' ).AsString;
                    CURP := FieldByName( 'CB_CURP' ).AsString;
                    //Datos afiliatorios
                    CodigoPostal := FieldByName( 'CB_CODPOST' ).AsString;
                    FechaNacimiento := FieldByName( 'CB_FEC_NAC' ).AsDateTime;
                    EntidadNacimiento := FieldByName( 'ENT_NAC' ).AsString;
                    Clinica := FieldByName( 'CB_CLINICA' ).AsString;
                    Sexo := FieldByName( 'CB_SEXO' ).AsString;
               end;
               {$endif}
               Baja := NullDateTime;
               PrestamoInicio := FieldByName('CB_INF_INI').AsDateTime ;


               { Limpia datos de Infonavit }
               CreditoINFONAVIT := VACIO;
               Prestamo := 0;
               PrestamoTipo := eTipoInfonavit( tiNoTiene );
               TasaAnterior := 0;
               AplicaDisminucion:= FALSE;
               FechaMovimiento:= NullDateTime;
               FechaSuspension:= NullDateTime;

               FechaBSS  := FieldByName( 'CB_FEC_BSS' ).AsDateTime;
               SalarioIntegrado := FieldByName( 'CB_SAL_INT' ).AsFloat;
               FechaSalarioIntegrado := FieldByName( 'CB_FEC_INT' ).AsDateTime;

               { Toma los datos de Infonavit a fin de Bimestre }
               PreparaInfonavitEmpleado( Codigo,  FechaDef( dFechaKardex, FBimFin ) );
               { Asigna los datos de Infonavit a inicio de Bimestre }
               with FCursorInfonavitVigente do
               begin
                    { 24/Feb/2009: Pueden existir suspensiones a medio bimestre}
                    if ( not IsEmpty ) then
                    begin
                         if ( FieldByName('KI_TIPO').AsInteger <> Ord( infoSuspen ) ) or
                            ( FSuspenBimMed and ( FieldByName('KI_FECHA').AsDateTime > FBimInicio ) and ( FieldByName('KI_FECHA').AsDateTime < FBimFin ) ) then
                         begin
                              SetHistorialEmpleado( FCursorInfonavitVigente );
                         end;
                    end;
               end;

               { AP(22/May/2008): Ya no se usa el proceso de reducci�n de Tasa, ahora se investiga cada que calcula  }
               if ( PrestamoTipo = tiPorcentaje ) and ( AplicaDisminucion ) then
               begin
                    Prestamo:= GetDisminucionTasa( FBimInicio );
               end;
               // Si el inicio del cr�dito es mayor a al fecha final se la trae a la fecha
               FEmpleado.PrestamoInicio:= GetPrestamoInicio( FechaDef( dFechaKardex, FBimFin ) );
          end;
          SetGlobalEmpleadoInfo( FieldByName( 'CB_TURNO' ).AsString, FieldByName( 'CB_PATRON' ).AsString );
     end;
end;


procedure TIMSS.PreparaHistInfonavitEmp( const iEmpleado: Integer; dInicial, dFinal: TDateTime );
begin
     {PreparaHistInfonavitEmp}

      with FCursorHistInfonavit do
      begin
           Active:= False;
           with oZetaProvider do
           begin
                ParamAsInteger( FCursorHistInfonavit, 'CB_CODIGO', iEmpleado );
                ParamAsDate( FCursorHistInfonavit, 'FECHA_INI', dInicial );
                ParamAsDate( FCursorHistInfonavit, 'FECHA_FIN', dFinal );
           end;
           Active:= True;
      end;
end;

procedure TIMSS.PreparaInfonavitEmpleado( const iEmpleado: Integer; dFechaFin: TDateTime );
begin
     {PreparaInfonavitEmpleado}
     with FCursorInfonavitVigente do
     begin
          Active:= False;
          with oZetaProvider do
          begin
               ParamAsInteger( FCursorInfonavitVigente, 'CB_CODIGO', iEmpleado );
               ParamAsDate( FCursorInfonavitVigente, 'BIM_FIN', dFechaFin );
               ParamAsDate( FCursorInfonavitVigente, 'BIM_FIN2', dFechaFin );
          end;
          Active:= True;
     end;
end;

procedure TIMSS.SetGlobalEmpleadoInfo( const sTurno, sPatron: String );
var
   rAcumulado: TPesos;
   dBajaImss, dEmpFinal, dPrestamoInicio: TDate;
   iFaltas, iIncapaci, iDiasPrestamo: Integer;
   lExportandoSUA: Boolean;
begin
     with FEmpleado do
     begin
          with FTurno do
          begin
               Active := False;
          end;
          with FRPatron do
          begin
               Active := False;
          end;
          with FIngreso do
          begin
               Active := False;
          end;
          with oZetaProvider do
          begin
               ParamAsChar( FTurno, 'Turno', sTurno, K_ANCHO_TURNO );
               ParamAsChar( FRPatron, 'Patron', sPatron, K_ANCHO_PATRON );
               ParamAsInteger( FIngreso, 'Empleado', Codigo );
               {$ifdef EVALUA_LIQ_IMSS}
               ParamAsChar( FIngreso, 'Patron', FPatron, K_ANCHO_PATRON );
               {$endif}
          end;
          with FTurno do
          begin
               Active := True;
               if EOF then   { Para identificar a los empleados cuyo CB_TURNO no existe en la tabla TURNO }
                  HayTurno := False
               else
               begin
                    HayTurno := True;
                    TipoJornada := eTipoJornada( FieldByName( 'TU_TIP_JOR' ).AsInteger );
               end;
               Active := False;
          end;
          FaltasTope := ZetaServerTools.DiasSemanaJornada( TipoJornada ); { Default es los d�as de una semana }
          if ( FYear < K_YEAR_REFORMA ) then { Tope de 14 faltas por mes ( 2 Semanas ) }
             FaltasTope := 2 * FaltasTope;
          FaltasInit;
          with FRPatron do
          begin
               Active := True;
               if EOF then  { Para identificar a los empleados cuyo CB_PATRON no existe en la tabla RPATRON }
                  HayPatron := False
               else
               begin
                    HayPatron := True;
                    RegistroPatronal := FieldByName( 'TB_NUMREG' ).AsString;
                    ModuloPatron := erPatronModulo( FieldByName( 'TB_MODULO' ).AsInteger );
               end;
               Active := False;
          end;
          with FIngreso do
          begin
               Active := True;
               if EOF then
                  Ingreso := NullDateTime
               else
                   Ingreso := FieldByName( 'CB_FEC_ING' ).AsDateTime;
               Active := False;
          end;

          with FCursorHistInfonavit do
          begin
               if ( FechaSuspension = NullDateTime ) then 
               begin
                    PreparaHistInfonavitEmp( Codigo, FBimInicio, FBimFin - 1); //Un dia antes del final del bimestre
                    while not EOF do
                    begin
                         if ( eTipoMovInfonavit( FieldByName('KI_TIPO').AsInteger ) in [ infoSuspen ] ) then
                            FechaSuspension:= FieldByName('KI_FECHA').AsDateTime;   // Se trae las suspensiones a medio bimestre
                         Next;
                    end;
               end;
          end;

          {$ifdef SUA_14}
          if FEsBimestre and FUsarAcumuladoAmortiza then
          begin
               oZetaProvider.ParamAsInteger( FAmortizacionAcumulada, 'Empleado', Codigo );
               with FAmortizacionAcumulada do
               begin
                    Active := TRUE;
                    rAcumulado := ZetaCommonTools.Redondea( FieldByName( 'RESULTADO' ).AsFloat );
                    Active := FALSE;
               end;
               if ( rAcumulado > 0 ) then
               begin
                    dPrestamoInicio := PrestamoInicio;
                    if ( dPrestamoInicio >= FBimInicio ) and ( dPrestamoInicio <= FBimFin ) then
                    begin
                         { Se checa el Global de Amortizaci�n }
                         case FInicioAmortizacion of
                              iaBimestre: dPrestamoInicio := SiguienteBimestre( dPrestamoInicio );
                              iaMes: dPrestamoInicio := SiguienteMes( dPrestamoInicio );
                              iaEsteMes: dPrestamoInicio := EsteMes( dPrestamoInicio );
                         else
                             dPrestamoInicio := SiguienteBimestre( dPrestamoInicio );
                         end;
                    end;
                    dBajaIMSS := FechaBSS;

                    if ( dBajaIMSS < Ingreso ) and ( FechaSuspension = NullDateTime ) then
                       dEmpFinal := FBimFin
                    else
                        dEmpFinal := rMin( rMinSinNulos( dBajaIMSS, FechaSuspension ), FBimFin );

                    dPrestamoInicio := rMax( dPrestamoInicio, FBimInicio );
                    dPrestamoInicio := rMax( dPrestamoInicio, Ingreso );   // Si el ingreso es a mitad del bimestre
                    lExportandoSUA := FEnviarSUA;
                    FEnviarSUA := FALSE;           // Para que no exporte la revisi�n sobre Faltas, esto lo har� ExportaMovimientos
                    RevisaFaltas( dPrestamoInicio, dEmpFinal, iFaltas, iIncapaci );
                    FEnviarSUA := lExportandoSUA;
                    FaltasInit;     // Volver a iniciar las faltas
                    iDiasPrestamo := ( Trunc( dEmpFinal ) - Trunc( dPrestamoInicio ) ) + 1 - iFaltas - iIncapaci;
{
                    if ( Codigo = 21280 ) then
                       ShowMessage( FechaAsStr( dPrestamoInicio ) + '|' + FechaAsStr( dEmpFinal ) + '|' +
                                    FechaAsStr( dBajaIMSS ) + '|' + FechaAsStr( Ingreso ) + '|' +
                                    FloatToStr( rAcumulado ) + '|' + FechaAsStr( dEmpFinal ) + '|' +
                                    IntToStr( iFaltas + iIncapaci ) + '|' + IntToStr( iDiasPrestamo ) );
}
                    if ( iDiasPrestamo > 0 ) then
                    begin
                         Prestamo := ZetaCommonTools.Redondea( rAcumulado / iDiasPrestamo * FDiasBim / 2 );
                         PrestamoTipo := tiCuotaFija;
                         {  Ya no aplica el veces, siempre se manda como Cuota Fija
                         if ( PrestamoTipo = tiVeces ) then
                         begin
                              rSMGDF := FCreator.dmSalMin.GetSMGDF( dEmpFinal );  // Usar� el SMGDF vigente al final del bimestre
                              Prestamo := ZetaCommonTools.MiRound( Prestamo / rSMGDF, 4 );
                         end;
                         }
                    end;
               end;
          end;
          {$endif}
          if ( PrestamoTipo = tiPorcentaje ) then
          begin
               with oZetaProvider do
               begin
                    ParamAsInteger( FLeeTasa, 'Empleado', Codigo );
                    PAramAsDate( FLeeTasa, 'Fecha', FBimInicio );
               end;
               with FLeeTasa do
               begin
                    Active := True;
                    if not IsEmpty then
                       Prestamo := FieldByName( 'CB_MONTO' ).AsFloat;
                    Active := False;
               end;
          end;
     end;
end;

procedure TIMSS.SetFechasPeriodo( const Tipo: eTipoLiqMov );
begin
     FTipoPeriodo := Tipo;
     FLiqFinal := FBimFin;
     if ( Tipo = tlmMensual ) then
        FLiqInicial := FBimMedio
     else
         FLiqInicial := FBimInicio;
end;

procedure TIMSS.InitPrimas;
begin
     with FCreator do
     begin
          PreparaPrimasIMSS;
     end;
end;

procedure TIMSS.InitSalMin;
begin
     with FCreator do
     begin
          PreparaSalMin;
     end;
end;

// DES US #13122 Infonavit - Descuento tipo # veces UMA...(Liquidaci�n IMSS)
procedure TIMSS.InitUMA;
begin
     with FCreator do
     begin
          PreparaValUma;
     end;
end;

procedure TIMSS.PreparaQuerysBegin;
var
   iMesIni, iMesFin: Integer;
begin
     with oZetaProvider do
     begin
          FIngreso := CreateQuery( Format( GetSQLScript( Q_IMSS_GET_INGRESO ), [ K_T_BAJA ] ) );
          {$ifndef EVALUA_LIQ_IMSS}
          ParamAsChar( FIngreso, 'Patron', FPatron, K_ANCHO_PATRON );
          {$endif}
          ParamAsDate( FIngreso, 'FechaInicial', FBimInicio );
          ParamAsDate( FIngreso, 'FechaFinal', FBimFin );
          FTurno := CreateQuery( GetSQLScript( Q_TURNO_LEE ) );
          FRPatron := CreateQuery( GetSQLScript( Q_PATRON_LEE ) );
          FLeeTasa := CreateQuery( Format( GetSQLScript( Q_TASA_INFONAVIT ), [ Entrecomillas( K_T_TAS_INF ) ] ) );
          FKardex := CreateQuery( GetSQLScript( Q_IMSS_KARDEX ) );
          FKardexAnterior := CreateQuery( GetSQLScript( Q_IMSS_KARDEX_ANTERIOR ) );
          FKardexSalario := CreateQuery( GetSQLScript( Q_IMSS_SALARIO_KARDEX ) );
          FIncapacidadPermiso := CreateQuery( GetSQLScript( Q_IMSS_INCAPACI_PERM ) );
          {$ifdef DOS_CAPAS}
          FFaltas := CreateQueryLocate;
          PreparaQuery( FFaltas, GetSQLScript( Q_IMSS_FALTAS ) );
          {$else}
          {$ifdef FIREBIRD}
          FFaltas := CreateQueryLocate;
          PreparaQuery( FFaltas, GetSQLScript( Q_IMSS_FALTAS ) );
          {$else}
          FFaltas := CreateQuery( GetSQLScript( Q_IMSS_FALTAS ) );
          {$endif}
          {$endif}

          FFaltasSinPrenomina := CreateQuery( GetSQLScript( Q_IMSS_FALTAS_SIN_PRENOMINA ) );
          FAportacion := CreateQuery( GetSQLScript( Q_APORTACION_VOLUNTARIA ) );
          ParamAsInteger( FAportacion, 'MesIni', FMes );
          ParamAsInteger( FAportacion, 'MesFin', FMes );
          ParamAsInteger( FAportacion, 'Year', FYear );
          ParamAsInteger( FAportacion, 'Concepto', GetGlobalInteger( K_GLOBAL_ADICIONAL_RETIRO ) );
          FInicioAmortizacion := eInicioAmortizacion( GetGlobalInteger( K_GLOBAL_INICIO_AMORTIZACION ) );

          {Cursor de Historial de Infonavit }
          FCursorHistInfonavit:= CreateQuery( GetSQLScript( Q_IMSS_LEE_HIST_INFONAVIT ) );
          FCursorInfonavitVigente:= CreateQuery( Format( GetSQLScript( Q_IMSS_INFONAVIT_VIGENTE ), [Ord(infoSuspen)] ) );

          { Reducci�n de Tasa Infonavit }
          FTasaInfonavit:= CreateQuery( GetSQLScript(  Q_LEE_DISM_TASA_INFONAVIT  ) );
          { Inicios y suspensiones de Infonavit }
          FKardexInfonavit:= CreateQuery( GetSQLScript( Q_KARDEX_INFONAVIT ) );

          FUsaPermisoDiasHabiles :=  GetGlobalBooleano( K_GLOBAL_PERMISOS_DIAS_HABILES );
          {$ifndef ASISTENCIA_DLL}
          if ( FUsaPermisoDiasHabiles ) then
          begin
              FServerAsistencia := TdmServerAsistencia.Create( nil );
          end
          else
              FServerAsistencia := nil;
          {$endif}


          {$ifdef SUA_14}
          FUsarAcumuladoAmortiza := {$ifdef EVALUA_LIQ_IMSS}( not FEvaluandoIMSS ) and {$endif} GetGlobalBooleano( K_GLOBAL_USAR_ACUM_AMORTIZACION );
          if FUsarAcumuladoAmortiza then
          begin
               FAmortizacionAcumulada := CreateQuery( GetSQLScript( Q_ACUM_AMORTIZACION ) );
               if EsBimestre( FMes ) then
               begin
                    iMesIni := FMes - 1;
                    iMesFin := FMes;
               end
               else
               begin
                    iMesIni := FMes;
                    iMesFin := FMes + 1;
               end;
               ParamAsInteger( FAmortizacionAcumulada, 'MesIni', iMesIni );
               ParamAsInteger( FAmortizacionAcumulada, 'MesFin', iMesFin );
               ParamAsInteger( FAmortizacionAcumulada, 'Year', FYear );
               ParamAsInteger( FAmortizacionAcumulada, 'Concepto', GetGlobalInteger( K_GLOBAL_CONCEPTO_AMORTIZACION ) );
          end;
          {$endif}
     end;
end;

procedure TIMSS.PreparaQuerysEnd;
begin
     FreeAndNil( FIngreso );
     FreeAndNil( FTurno );
     FreeAndNil( FRPatron );
     FreeAndNil( FLeeTasa );
     FreeAndNil( FKardex );
     FreeAndNil( FKardexAnterior );
     FreeAndNil( FKardexSalario );
     FreeAndNil( FIncapacidadPermiso );
     FreeAndNil( FFaltas );
     FreeAndNil( FFaltasSinPrenomina );
     FreeAndNil( FAportacion );
     {$ifdef SUA_14}
     FreeAndNil( FAmortizacionAcumulada );
     {$endif}
     FreeAndNil(FTasaInfonavit);
     FreeAndNil(FCursorInfonavitVigente);
     FreeAndNil(FCursorHistInfonavit);
     FreeAndNil(FKardexInfonavit);
     {$ifndef ASISTENCIA_DLL}
     FreeAndNil(FServerAsistencia);
     {$endif}

end;

{$ifdef EVALUA_LIQ_IMSS}
procedure TIMSS.PreparaKardex( const dFecInicial, dFecFinal: TDate );
begin
     FKardex.Active := False;
     with oZetaProvider do
     begin
          ParamAsInteger( FKardex, 'Empleado', FEmpleado.Codigo );
          ParamAsChar( FKardex, 'Patron', FPatron, K_ANCHO_PATRON );
          ParamAsDate( FKardex, 'FechaInicial1', dFecInicial );
          ParamAsDate( FKardex, 'FechaInicial2', dFecInicial );
          ParamAsDate( FKardex, 'FechaFinal1', dFecFinal );
          ParamAsDate( FKardex, 'FechaFinal2', dFecFinal );
     end;
end;
{$endif}

procedure TIMSS.PreparaKardex( const dFecInicial: TDate );
begin
     {$ifdef EVALUA_LIQ_IMSS}
     PreparaKardex( dFecInicial, FLiqFinal );
     {$else}
     FKardex.Active := False;
     with oZetaProvider do
     begin
          ParamAsInteger( FKardex, 'Empleado', FEmpleado.Codigo );
          ParamAsChar( FKardex, 'Patron', FPatron, K_ANCHO_PATRON );
          ParamAsDate( FKardex, 'FechaInicial1', dFecInicial );
          ParamAsDate( FKardex, 'FechaInicial2', dFecInicial );
          ParamAsDate( FKardex, 'FechaFinal1', FLiqFinal );
          ParamAsDate( FKardex, 'FechaFinal2', FLiqFinal );
     end;
     {$endif}
end;

procedure TIMSS.PreparaKardexInfonavit( const dFecFinal: TDate );
begin
     FKardexInfonavit.Active := False;
     with oZetaProvider do
     begin
          ParamAsInteger( FKardexInfonavit, 'Empleado', FEmpleado.Codigo );
          ParamAsDate( FKardexInfonavit, 'FechaFinal', dFecFinal );
     end;
end;

function TIMSS.PuedeProcesar: Boolean;
begin
     with FEmpleado do
     begin
          Result := HayTurno and HayPatron and ( Ingreso <> NullDateTime ) and ( ( PatronInicial = FPatron ) or ( PatronMovimientos > 0 ) );
     end;
end;

function TIMSS.GetSalarioKardex(const dFecha: TDate): TPesos;
begin
     with FKardexSalario do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FKardexSalario, 'EMPLEADO', FEmpleado.Codigo );
               ParamAsDate( FKardexSalario, 'FECHA', dFecha );
          end;
          Active := True;
          Result := Redondea( FieldByName( 'CB_SAL_INT' ).AsFloat );
          Active := False;
     end;
end;

function TIMSS.GetDisminucionTasa( const dFecha: TDate ): TPesos;
begin
     with FTasaInfonavit do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FTasaInfonavit, 'CB_CODIGO', FEmpleado.Codigo );
               ParamAsDate( FTasaInfonavit, 'FECHA', dFecha );
          end;
          Active := True;
          Result := FieldByName( 'CB_INFTASA' ).AsFloat;
          Active := False;
     end;
end;

function TIMSS.GetSalarioSua(const dIntegrado, dFecha: TDate): TPesos;
{ Si el salario integrado ACTUAL del empleado tiene }
{ fecha anterior al per�odo en cuesti�n, ya tenemos el salario }
{ de lo contrario, se tiene que tomar del KARDEX }
begin
     with FEmpleado do
     begin
          if ( dIntegrado <= dFecha ) then
             Result := Redondea( SalarioIntegrado )
          else
             Result := GetSalarioKardex( dFecha );
     end;
end;

function TIMSS.GetSUAOrden( const sTipo: string ): string;
const
     K_ORD_SUA_REINGRESO   = '1';
     K_ORD_SUA_CAMBIO      = '2';
     K_ORD_SUA_INCAPACIDAD = '3';
     K_ORD_SUA_AUSENTISMO  = '4';
     K_ORD_SUA_VOLUNTARIA  = '5';
     K_ORD_INDEFINIDO      = '8';
     K_ORD_SUA_BAJA        = '9';
begin
     if ( sTipo = K_TIPO_SUA_BAJA ) then
           Result := K_ORD_SUA_BAJA
     else if ( sTipo = K_TIPO_SUA_CAMBIO ) then
             Result := K_ORD_SUA_CAMBIO
          else if ( sTipo = K_TIPO_SUA_REINGRESO ) then
                  Result := K_ORD_SUA_REINGRESO
               else if ( sTipo = K_TIPO_SUA_VOLUNTARIA ) then
                       Result := K_ORD_SUA_VOLUNTARIA
                    else if ( sTipo = K_TIPO_SUA_AUSENTISMO ) then
                            Result := K_ORD_SUA_AUSENTISMO
                         else if ( sTipo = K_TIPO_SUA_INCAPACIDAD ) then
                                 Result := K_ORD_SUA_INCAPACIDAD
                              else
                                  Result := K_ORD_INDEFINIDO;
end;

function TIMSS.GetAportacionVoluntaria( const iEmpleado: Integer ): Extended;
begin
     { Pendiente }
     with FAportacion do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FAportacion, 'Empleado', iEmpleado );
          end;
          Active := True;
          Result := ZetaCommonTools.Redondea( FieldByName( 'RESULTADO' ).AsFloat );
          Active := False;
     end;
end;

{$ifdef false}
function TIMSS.GetAusentismoSUA( const iFaltas: Integer ): Integer;
type
    TReducida = array[ 0..7 ] of Integer;
const
     aReducida1: TReducida = ( 0, 7, 0, 0, 0, 0, 0, 0 );
     aReducida2: TReducida = ( 0, 4, 7, 0, 0, 0, 0, 0 );
     aReducida3: TReducida = ( 0, 2, 5, 7, 0, 0, 0, 0 );
     aReducida4: TReducida = ( 0, 2, 4, 5, 7, 0, 0, 0 );
     aReducida5: TReducida = ( 0, 1, 3, 4, 6, 7, 0, 0 );
     aReducida7: TReducida = ( 0, 1, 2, 3, 4, 5, 6, 7 );
var
   aReducida: TReducida;
begin
     if FAumentaAusentismos then
     begin
          with FEmpleado do
          begin
               case TipoJornada of
                    tjReducida_1: aReducida := aReducida1;
                    tjReducida_2: aReducida := aReducida2;
                    tjReducida_3: aReducida := aReducida3;
                    tjReducida_4: aReducida := aReducida4;
                    tjReducida_5: aReducida := aReducida5;
               else
                   aReducida := aReducida7;
               end;
               if ( FaltasAC in [ Low( aReducida )..High( aReducida ) ] ) then
                  Result := ZetaCommonTools.iMax( 0, aReducida[ FaltasAC ] - aReducida[ ( FaltasAC - iFaltas ) ] )
               else
                   Result := 0;
          end;
     end
     else
         Result := iFaltas;
end;
{$endif}

function TIMSS.GetAusentismoSUA( const iFaltas: Integer ): Integer;
begin
     with FEmpleado do
          Result := ZetaCommonTools.iMax( 0, GetAusentismosJornadaReducida( TipoJornada, FaltasAC ) - GetAusentismosJornadaReducida( TipoJornada,  FaltasAC - iFaltas ) );
end;

procedure TIMSS.FaltasReset( const iMes: Integer );
begin
     with FEmpleado do
     begin
          FaltasAC := 0; { Inicializar Acumulador de Faltas en un Mes }
          FaltasMes := iMes;     { Mes Al Cual Pertenecen las Faltas }
     end;
end;

procedure TIMSS.FaltasInc;
begin
     with FEmpleado do
     begin
          FaltasAC := FaltasAC + 1; { Incrementar Acumulador de Faltas }
     end;
end;

procedure TIMSS.FaltasInit;
begin
     FaltasReset( 0 );
end;

procedure TIMSS.FaltasStart( const dValue: TDate );
var
   iMes: Integer;
begin
     iMes := ZetaCommonTools.TheMonth( dValue ) ;
     if ( iMes <> FEmpleado.FaltasMes ) then
        FaltasReset( iMes );
end;

procedure TIMSS.FaltasStore;
begin
     with FEmpleado do
     begin
          FaltasACOld := FaltasAC;
          FaltasMesOld := FaltasMes;
     end;
     FaltasInit;
end;

procedure TIMSS.FaltasRestore;
begin
     with FEmpleado do
     begin
          FaltasAC := FaltasACOld;
          FaltasMes := FaltasMesOld;
     end;
end;

function TIMSS.FaltasCheckTope: Boolean;
begin
     with FEmpleado do
     begin
          Result := ( FaltasAC < FaltasTope );
     end;
end;

procedure TIMSS.RevisaFaltas( const dInicial, dFinal: TDate; var iFaltas, iIncapacidades: Integer );
const
     K_MAX_FALTAS_MES = 31;
type
    eTipoCalendar = (calHabil, calIncapacidad, calPermiso, calFalta );
    TMesFaltas = array[ 1..K_MAX_FALTAS_MES ] of eTipoCalendar;
var
   iMesFaltas, iMesIncas: Integer;
   iTotFaltas, iTotInca: Word;
{$ifdef SUA_F_JREDUCIDA}
   dFaltaInicial: TDateTime;
{$endif}
   dFaltaJorRed: TDateTime;
   iDiasJorRed: Integer;
   aCalendar: TMesFaltas;

procedure EscribeSUAFaltaIncremento( const dMovimiento: TDate; const rDias: TPesos );
begin
     with FSUAMovtos do
     begin
          if Locate( 'SUA_M_PAT;SUA_M_SS;SUA_M_TIPO;SUA_M_DATE', VarArrayOf( [ FEmpleado.RegistroPatronal,
                     FEmpleado.NumeroSeguro, K_TIPO_SUA_AUSENTISMO, dMovimiento ] ), [] ) then
          begin
               try
                  Edit;
                  FieldByName( 'SUA_M_DIAS' ).AsFloat := FieldByName( 'SUA_M_DIAS' ).AsFloat + rDias;
                  Post;
               except
                     on Error: Exception do
                     begin
                          Cancel;
                     end;
               end;
          end;
     end;
end;

procedure SUAFalta( const dFalta: TDate );
begin
     if FEnviarSUA then
     begin
          with FEmpleado do
          begin
               {Cuando el empledo es de tipo jornada reducida, los ausentismos se mandan en un solo movimiento al final, con la conversi�n de las
               las faltas totales. SUA no calcula bien si se mandan por separado. }
               {$ifdef SUA_F_JREDUCIDA}
               if ( ( ( dFalta <> Baja ) or FMovtosFechaBaja ) and not ( TipoJornada in [tjReducida_1,tjReducida_2,tjReducida_3,tjReducida_4,tjReducida_5 ]  )  ) then
               {$else}
                if ( dFalta <> Baja ) or FMovtosFechaBaja then
               {$endif}
               begin
{$ifdef ANTES}
                    EscribeMovtoSUA( K_TIPO_SUA_AUSENTISMO, '', dFalta, GetAusentismoSUA( 1 ), 0 );
{$else}
                    if ( TipoJornada in [tjReducida_1,tjReducida_2,tjReducida_3,tjReducida_4,tjReducida_5 ]  ) and ( ( dFalta - iDiasJorRed ) = dFaltaJorRed ) then   // Jornada reducida y faltas consecutivas no se envia, se edita el registro anterior
                    begin
                         EscribeSUAFaltaIncremento( dFaltaJorRed, 1 );
                         Inc( iDiasJorRed );
                    end
                    else
                    begin
                         EscribeMovtoSUA( K_TIPO_SUA_AUSENTISMO, '', dFalta, 1, 0 );    // ER: SUA 2006 ya determina los dias al subir ASCII
                         dFaltaJorRed := dFalta;
                         iDiasJorRed := 1;
                    end;
{$endif}
                    if ( dFalta = Baja ) then
                       oZetaProvider.Log.Advertencia( Codigo, 'Ausentismo en Fecha de BAJA ( ' + FechaCorta( dFalta ) + ' )' );
               end;
          end;
     end;
end;

procedure CountFaltas( const dInicial, dFinal: TDate );
var
   dFalta: TDate;
   i : integer;

begin
     with FFaltas do
     begin
          Active := False;
          with oZetaProvider do
          begin
               {$ifdef DOS_CAPAS}
               ParamAsIntegerLocate( FFaltas, 'Empleado', FEmpleado.Codigo );
               ParamAsDateLocate( FFaltas, 'FechaInicial', dInicial );
               ParamAsDateLocate( FFaltas, 'FechaFinal', dFinal );
               {$else}
               {$ifdef FIREBIRD}
               ParamAsIntegerLocate( FFaltas, 'Empleado', FEmpleado.Codigo );
               ParamAsDateLocate( FFaltas, 'FechaInicial', dInicial );
               ParamAsDateLocate( FFaltas, 'FechaFinal', dFinal );
               {$else}
               ParamAsInteger( FFaltas, 'Empleado', FEmpleado.Codigo );
               ParamAsDate( FFaltas, 'FechaInicial', dInicial );
               ParamAsDate( FFaltas, 'FechaFinal', dFinal );
               {$endif}
               {$endif}


          end;
     end;

     with FFaltas do
     begin
          Active := True;
          while not Eof and FaltasCheckTope do
          begin
               dFalta := FieldByName( 'AU_FECHA' ).AsDateTime;
               { Valida que la fecha no haya sido Incapacidad o Permiso }
               if ( aCalendar[ ZetaCommonTools.TheDay( dFalta ) ] = calHabil ) then
               begin
                    if ( dFalta <> FEmpleado.Baja ) or FMovtosFechaBaja then  { VL }
                    begin
                         iTotFaltas := iTotFaltas + 1;
                         {$ifdef SUA_F_JREDUCIDA}
                         if ( iTotFaltas = 1 ) then
                            dFaltaInicial:= dFalta;
                         {$endif}
                         FaltasInc;
                    end;
                    SUAFalta( dFalta );
               end;
               Next;
          end;
          //Active := False;   //Se necesita que este abierto
     end;

     { Las excepciones de faltas no se envian a sua debido a que la afectaci�n de n�mina resuelve �ste punto,
       por eso la condici�n de jornada reducida de hace antes }

     if FEValuandoIMSS then //Solo me interesa cuando las funciones de IMSS utilizan este c�digo
     begin
          with FFaltasSinPrenomina do
          begin
               Active := False;
               with oZetaProvider do
               begin
                    ParamAsInteger( FFaltasSinPrenomina, 'Empleado', FEmpleado.Codigo );
                    ParamAsDate( FFaltasSinPrenomina, 'FechaInicial', dInicial );
                    ParamAsDate( FFaltasSinPrenomina, 'FechaFinal', dFinal );
               end;
          end;

          with FFaltasSinPrenomina do
          begin
               Active := True;
               while not Eof and FaltasCheckTope do
               begin
                    for i:= 0 to FieldByName('DIAS').AsInteger -1 do
                    begin
                         if FaltasCheckTope then
                         begin
                              dFalta := FieldByName( 'AU_FECHA' ).AsDateTime + i;
                              { Valida que la fecha no haya sido Incapacidad o Permiso }
                              if ( aCalendar[ ZetaCommonTools.TheDay( dFalta ) ] = calHabil ) and
                                 NOT FFaltas.Locate('AU_FECHA', dFalta, [] ) then //Quiero validar que no estoy contando la falta 2 veces
                              begin
                                   if ( dFalta <> FEmpleado.Baja ) or FMovtosFechaBaja then  { VL }
                                   begin
                                        iTotFaltas := iTotFaltas + 1;
                                        {$ifdef SUA_F_JREDUCIDA}
                                        if ( iTotFaltas = 1 ) then
                                           dFaltaInicial:= dFalta; //Esta variable hasta ahorita se usa solamente en exportaci�n
                                        {$endif}
                                        FaltasInc;
                                   end;
                                   SUAFalta( dFalta );
                              end;
                         end
                         else
                         begin
                              Break;
                         end;
                    end;
                    Next;
               end;
               Active := False;
          end;
     end;

     FFaltas.Active := FALSE;

end;

procedure CountIncas( const dInicial, dFinal: TDate );
const
     K_TIPO = 0;
     K_INICIO = 1;
     K_FIN = 2;
     K_DIAS = 3;
     K_NUMERO = 4;
     K_CLASIFICACION = 5;
     K_IN_TIPO = 6;
     K_IN_TASA = 7;
     K_IN_FIN = 8;
     K_IN_MOTIVO = 9;
     K_TB_INCAPA = 10;

    function GetDiasPermiso( iEmpleado : integer; dFecIni, dFecFin : TDateTime )  : Double;
    begin
         if ( FUsaPermisoDiasHabiles )
         {$ifndef ASISTENCIA_DLL}
         and ( FServerAsistencia <> nil)
         {$endif}
         then
         begin
             try
                Result := 0;
                {$ifndef ASISTENCIA_DLL}
                Result :=  FServerAsistencia.GetPermisoDiasHabiles( oZetaProvider.EmpresaActiva, iEmpleado, dFecIni, dFecFin ) ;
                {$endif}
             finally

             end;
         end
         else
             Result := 1.0;

    end;

var
   dFalta, dPerIni, dPerFin: TDate;
   i: Integer;
begin
     with FIncapacidadPermiso do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FIncapacidadPermiso, 'Empleado1', FEmpleado.Codigo );
               ParamAsInteger( FIncapacidadPermiso, 'Empleado2', FEmpleado.Codigo );
               ParamAsDate( FIncapacidadPermiso, 'FechaInicial1', dInicial );
               ParamAsDate( FIncapacidadPermiso, 'FechaInicial2', dInicial );
               ParamAsDate( FIncapacidadPermiso, 'FechaFinal1', dFinal );
               ParamAsDate( FIncapacidadPermiso, 'FechaFinal2', dFinal );
          end;
          Active := True;
          while not Eof do
          begin
               { Nota: Uso Fields[ 0 ] porque es un UNION }
               if ( Fields[ K_TIPO ].AsInteger = 1 ) then { INCAPACIDAD }
               begin
                    dPerIni := ZetaServerTools.dMax( dInicial, Fields[ K_INICIO ].AsDateTime );
                    dPerFin := ZetaServerTools.dMin( dFinal, ( Fields[ K_FIN ].AsDateTime - 1 ) );
                    { Marca calendario como incapacidades }
                    for i := ZetaCommonTools.TheDay( dPerIni ) to ZetaCommonTools.TheDay( dPerFin ) do
                    begin
                         if ( aCalendar[ i ] = calHabil ) then { Evita contar doble en Empalmes }
                         begin
                              aCalendar[ i ] := calIncapacidad;
                              iTotInca := iTotInca + 1;
                         end;
                    end;
                    if FEnviarSUA then
                    begin
                         EscribeMovtoSUA( K_TIPO_SUA_INCAPACIDAD,
                                  Fields[ K_NUMERO ].AsString,
                                  Fields[ K_INICIO ].AsDateTime,
                                  Fields[ K_DIAS ].AsInteger,
                                  0 );
                         EscribeIncapaSUA( Fields[K_INICIO].AsDateTime,
                                           Fields[K_FIN].AsDateTime - 1,//Se cambio a que fuera la fecha en que termina, defecto 1655
                                           Fields[ K_DIAS ].AsInteger,
                                           eIncidenciaIncapacidad(Fields[K_TB_INCAPA].AsInteger),
                                           eFinIncapacidad(Fields[ K_IN_FIN ].AsInteger),
                                           eMotivoIncapacidad(Fields[ K_IN_MOTIVO ].AsInteger),
                                           Fields[K_IN_TASA].AsFloat,
                                           Fields[ K_NUMERO ].AsString,
                                           Fields[K_IN_TIPO].AsString);
                    end;
               end
               else { � PERMISO que descuente IMSS ? }
                   if ( eTipoPermiso( Fields[ K_CLASIFICACION ].AsInteger ) in [ tpSinGoce, tpFaltaJustificada, tpSuspension ] ) then
                   begin
                        dPerIni := ZetaServerTools.dMax( dInicial, Fields[ K_INICIO ].AsDateTime );
                        dPerFin := ZetaServerTools.dMin( dFinal, ( Fields[ K_FIN ].AsDateTime - 1 ) );
                        dFalta := dPerIni;
                        while ( dFalta <= dPerFin ) do
                        begin
                             { Pendiente: Validar contra AUSENCIA.AU_TIPODIA }
                             if FaltasCheckTope then
                             begin
								                  //Valida que Sea un Dia Habil segun el turno, festivos y ausencia  del Empleado
                                  if ( GetDiasPermiso(FEmpleado.Codigo, dFalta, dFalta + 1 ) > 0 ) then
                                  begin
                                  	  { Marca calendario como Permiso }
                                      i := ZetaCommonTools.TheDay( dFalta );

                                      { Evita contar doble en empalmes }
                                      if ( aCalendar[ i ] = calHabil ) then
                                      begin
                                           aCalendar[ i ] := calPermiso;
                                           iTotFaltas := iTotFaltas + 1;
                                           {$ifdef SUA_F_JREDUCIDA}
                                           if ( iTotFaltas = 1 ) then
                                              dFaltaInicial:= dFalta;
                                           {$endif}
                                           FaltasInc;
                                      end;
                                      SUAFalta( dFalta );
                                  end;
                             end;
                             dFalta := dFalta + 1;
                        end;
                   end;
               Next;
          end;
          Active := False;
     end;
end;

{$ifdef SUA_F_JREDUCIDA}
procedure SUAFaltaJReducida(const iFaltas: Integer);
begin
     with FEmpleado do
      begin
           if FEnviarSUA and ( iFaltas > 0 ) and ( TipoJornada in [tjReducida_1,tjReducida_2,tjReducida_3,tjReducida_4,tjReducida_5 ] ) then
           begin
                if  ( ( dFaltaInicial <> Baja ) or FMovtosFechaBaja ) then
                begin
                     EscribeMovtoSUA( K_TIPO_SUA_AUSENTISMO, '', dFaltaInicial, GetAusentismoSUA( iFaltas ), 0 );
                     
                     if ( dFaltaInicial = Baja ) then
                       oZetaProvider.Log.Advertencia( Codigo, 'Ausentismo en Fecha de BAJA ( ' + FechaCorta( dFaltaInicial ) + ' )' );
                end;
           end;
      end;
end;
{$endif}

procedure OutFaltas( const dInicial, dFinal: TDate );
var
   i: Integer;
   dFinalFaltas : TDate;
begin
     iTotFaltas := 0;
     {$ifdef SUA_F_JREDUCIDA}
     dFaltaInicial:= NullDateTime;
     {$endif}
     iTotInca := 0;
     FaltasStart( dInicial );
     for i := 1 to K_MAX_FALTAS_MES do
     begin
          aCalendar[ i ] := calHabil;
     end;
     CountIncas( dInicial, dFinal );
     //Se cambi� para evaular el valor del global K_GLOBA_INCLUIR_FI_SUA solamente en c�lculo de pago imss y exportaci�n a SUA
     if ( UsarLimiteRevisionFaltas ) and ( FechaLimiteFaltas > 0 ) and ( FechaLimiteFaltas <= dFinal) then
        dFinalFaltas := FechaLimiteFaltas
     else
         dFinalFaltas := dFinal;

     if FIncluirFI and FaltasCheckTope then
        CountFaltas( dInicial, dFinalFaltas );

     {$ifdef SUA_F_JREDUCIDA}
     SUAFaltaJReducida(iTotFaltas);
     {$endif}
     iMesFaltas := GetAusentismoSUA( iTotFaltas );
     iMesIncas := iTotInca;
end;

begin  { RevisaFaltas }
     if {$ifdef EVALUA_LIQ_IMSS}FEvaluandoIMSS or {$endif}( FTipoPeriodo = tlmMensual ) or ( ZetaCommonTools.TheMonth( dInicial ) = ZetaCommonTools.TheMonth( dFinal ) )  then
     begin
          OutFaltas( dInicial, dFinal );
          iFaltas := iMesFaltas;
          iIncapacidades := iMesIncas;
     end
     else
     begin
          OutFaltas( dInicial, ( FBimMedio - 1 ) );
          iFaltas := iMesFaltas;
          iIncapacidades := iMesIncas;
          OutFaltas( FBimMedio, dFinal );
          iFaltas := iFaltas + iMesFaltas;
          iIncapacidades := iIncapacidades + iMesIncas;
     end;
end;

{$ifdef EVALUA_LIQ_IMSS}
procedure TIMSS.IMSSPagosBegin( const iYear, iMes: Integer; const Tipo: eTipoLiqIMSS; const lMovtosFechaBaja, lCuotaFija: Boolean; const rSeguroVivienda: TPesos );
begin
     FMovtosFechaBaja := lMovtosFechaBaja;
     FCuotaFija := lCuotaFija;
     FSeguroVivienda := rSeguroVivienda;
     SetGlobalesPagos( iYear, iMes, Tipo );
     with oZetaProvider do
     begin
          FFormulaPercepciones := GetGlobalString( K_GLOBAL_FORM_PERCEP );
          FFormulaDias := GetGlobalString( K_GLOBAL_FORM_DIAS );
     end;
     with FLiqIMSS do
     begin { Poner en Cero TODOS los valores totales de LIQ_IMSS }
          LS_NUM_TRA := 0;
          LS_DIAS_CO := 0;
          LS_EYM_DIN := 0;
          LS_EYM_ESP := 0;
          LS_EYM_EXC := 0;
          LS_EYM_FIJ := 0;
          LS_RIESGOS := 0;
          LS_INV_VID := 0;
          LS_GUARDER := 0;
          LS_RETIRO := 0;
          LS_CES_VEJ := 0;
          LS_APO_VOL := 0;
          LS_INF_ACR := 0;
          LS_INF_AMO := 0;
          LS_INF_NAC := 0;
          LS_INF_NUM := 0;
          LS_ACT_IMS := 0;
          LS_ACT_RET := 0;
          LS_ACT_INF := 0;
          LS_REC_IMS := 0;
          LS_REC_INF := 0;
          LS_REC_RET := 0;
          LS_DIAS_BM := 0;
          LS_NUM_BIM := 0;
          LS_SUB_IMS := 0;
          LS_TOT_IMS := 0;
          LS_TOT_MES := 0;
          LS_SUB_RET := 0;
          LS_TOT_RET := 0;
          LS_SUB_INF := 0;
          LS_TOT_INF := 0;
          LS_IMSS_PA := 0;
          LS_IMSS_OB := 0;
          LS_ACT_AMO := 0;
          LS_ACT_APO := 0;
          LS_REC_AMO := 0;
          LS_REC_APO := 0;
     end;
     PreparaQuerysBegin;
     InitPrimas;
     InitSalMin;
     InitUMA; // DES US #13122 Infonavit - Descuento tipo # veces UMA...(Liquidaci�n IMSS)
end;

{$ifdef DESCUENTO_SUA}
procedure TIMSS.ObtienePrimaRiesgo( const sPatron: String; const dFecha: TDate );
begin
     FPrimaRiesgo := FCreator.dmPrimasIMSS.GetPrimaRiesgo( dFecha, sPatron );
end;
{$else}
procedure TIMSS.ObtienePrimaRiesgo( const sPatron: String; const dFecha: TDate );
var
   FDataset: TZetaCursor;
begin
     with oZetaProvider do
     begin
          FDataset := CreateQuery( GetSQLScript( Q_IMSS_GET_PRIMA ) );
          try
             with FDataset do { Obtiene Prima ( � de Riesgo ? ) }
             begin
                  Active := False;
                  ParamAsChar( FDataset, 'Patron', sPatron, K_ANCHO_PATRON );
                  ParamAsDate( FDataset, 'Fecha', dFecha );
                  Active := True;
                  FPrimaRiesgo := Fields[ 0 ].AsFloat;
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataset );
          end;
     end;
end;
{$endif}

procedure TIMSS.EvaluarIMSSPagosBegin( const lMovtosFechaBaja, lCuotaFija: Boolean; const rSeguroVivienda: TPesos; const iYear, iMes: Integer );
begin
     FEvaluandoIMSS := True;
     //En las funciones siempre se inicializa en TRUE
     FIncluirFI:= True;
     IMSSPagosBegin( iYear, iMes, tlOrdinaria, lMovtosFechaBaja, lCuotaFija, rSeguroVivienda );
     EvaluarIMSSPagosSetFechas( FBimMedio, FBimFin, FALSE );
end;

procedure TIMSS.EvaluarIMSSPagosSetFechas( const dInicial, dFinal: TDateTime; const lRastreoINFO: Boolean );
begin
     FFechaKardexInicial := dInicial;
     FFechaKardexFinal := dFinal;
     FRastreoINFO := lRastreoINFO;
end;

procedure TIMSS.CalcularIMSSPagosBegin(const sPatron: String; const iYear, iMes: Integer; const Tipo: eTipoLiqIMSS; const lMovtosFechaBaja, lCuotaFija: Boolean; const rSeguroVivienda: TPesos );
var
   FDataset: TZetaCursor;
   lExiste: Boolean;
begin
     SetGlobalPatron( sPatron );
     IMSSPagosBegin( iYear, iMes, Tipo, lMovtosFechaBaja, lCuotaFija, rSeguroVivienda );
     //SetGlobalPatron( sPatron );
     ObtienePrimaRiesgo( FPatron, FBimFin );
     with oZetaProvider do
     begin
          InitGlobales;
          FIncluirFI:= GetGlobalBooleano( K_GLOBAL_INCLUYE_FI_SUA );
          { Validar que Liquidaci�n Exista antes que otra cosa }
          FDataset := CreateQuery( GetSQLScript( Q_LIQUIDACION_EXISTE ) );
          try
             with FDataset do
             begin
                  Active := False;
                  ParamAsChar( FDataset, 'Patron', sPatron, K_ANCHO_PATRON );
                  ParamAsInteger( FDataset, 'Year', iYear );
                  ParamAsInteger( FDataset, 'Mes', iMes );
                  ParamAsInteger( FDataset, 'Tipo',Ord( Tipo ) );
                  Active := True;
                  lExiste := ( Fields[ 0 ].AsInteger > 0 );
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataset );
          end;
          if lExiste then
             FDataset := CreateQuery( GetSQLScript( Q_LIQUIDACION_PRESET ) )
          else
              FDataset := CreateQuery( GetSQLScript( Q_LIQUIDACION_AGREGA ) );
          try
             ParamAsChar( FDataset, 'LS_PATRON', sPatron, K_ANCHO_PATRON );
             ParamAsInteger( FDataset, 'LS_YEAR', iYear );
             ParamAsInteger( FDataset, 'LS_MONTH', iMes );
             ParamAsInteger( FDataset, 'LS_TIPO', Ord( Tipo ) );
             ParamAsInteger( FDataset, 'LS_STATUS', Ord( slCalculadaParcial ) );
             ParamAsInteger( FDataset, 'US_CODIGO', UsuarioActivo );
             Ejecuta( FDataset ); { Crea Liquidaci�n Y/O Inicializa su Status }
          finally
                 FreeAndNil( FDataset );
          end;
          FDataset := CreateQuery( GetSQLScript( Q_EMPLEADO_BORRA ) );
          try
             ParamAsChar( FDataset, 'Patron', FPatron, K_ANCHO_PATRON );
             ParamAsInteger( FDataset, 'Year', FYear );
             ParamAsInteger( FDataset, 'Mes', FMes );
             ParamAsInteger( FDataset, 'Tipo', Ord( FTipoIMSS ) );
             Ejecuta( FDataset ); { Borra TODO LIQ_EMP y el trigger borra TODO LIQ_MOV }
          finally
                 FreeAndNil( FDataset );
          end;
          FMovtoEscribe := CreateQuery( GetSQLScript( Q_MOVIMIENTO_ESCRIBE ) );
          ParamAsChar( FMovtoEscribe, 'LS_PATRON', FPatron, K_ANCHO_PATRON );
          ParamAsInteger( FMovtoEscribe, 'LS_YEAR', FYear );
          ParamAsInteger( FMovtoEscribe, 'LS_MONTH', FMes );
          ParamAsInteger( FMovtoEscribe, 'LS_TIPO', Ord( FTipoIMSS ) );
          FEmpleadoAgrega := CreateQuery( GetSQLScript( Q_EMPLEADO_AGREGA ) );
          ParamAsChar( FEmpleadoAgrega, 'LS_PATRON', FPatron, K_ANCHO_PATRON );
          ParamAsInteger( FEmpleadoAgrega, 'LS_YEAR', FYear );
          ParamAsInteger( FEmpleadoAgrega, 'LS_MONTH', FMes );
          ParamAsInteger( FEmpleadoAgrega, 'LS_TIPO', Ord( FTipoIMSS ) );
          ParamAsInteger( FEmpleadoAgrega, 'LE_STATUS', Ord( slCalculadaParcial ) );
          FEmpleadoModifica := CreateQuery( GetSQLScript( Q_EMPLEADO_MODIFICA ) );
          ParamAsChar( FEmpleadoModifica, 'LS_PATRON', FPatron, K_ANCHO_PATRON );
          ParamAsInteger( FEmpleadoModifica, 'LS_YEAR', FYear );
          ParamAsInteger( FEmpleadoModifica, 'LS_MONTH', FMes );
          ParamAsInteger( FEmpleadoModifica, 'LS_TIPO', Ord( FTipoIMSS ) );
          ParamAsInteger( FEmpleadoAgrega, 'LE_STATUS', Ord( slCalculadaTotal ) );
     end;
end;
{$else}
procedure TIMSS.CalcularIMSSPagosBegin(const sPatron: String; const iYear, iMes: Integer; const Tipo: eTipoLiqIMSS; const lMovtosFechaBaja, lCuotaFija: Boolean; const rSeguroVivienda: TPesos );
var
   FDataset: TZetaCursor;
   lExiste: Boolean;
begin
     FMovtosFechaBaja := lMovtosFechaBaja;
     FCuotaFija := lCuotaFija;
     FSeguroVivienda := rSeguroVivienda;
     with oZetaProvider do
     begin
          { Validar que Liquidaci�n Exista antes que otra cosa }
          FDataset := CreateQuery( GetSQLScript( Q_LIQUIDACION_EXISTE ) );
          try
             with FDataset do
             begin
                  Active := False;
                  ParamAsChar( FDataset, 'Patron', sPatron, K_ANCHO_PATRON );
                  ParamAsInteger( FDataset, 'Year', iYear );
                  ParamAsInteger( FDataset, 'Mes', iMes );
                  ParamAsInteger( FDataset, 'Tipo',Ord( Tipo ) );
                  Active := True;
                  lExiste := ( Fields[ 0 ].AsInteger > 0 );
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataset );
          end;
          if lExiste then
             FDataset := CreateQuery( GetSQLScript( Q_LIQUIDACION_PRESET ) )
          else
              FDataset := CreateQuery( GetSQLScript( Q_LIQUIDACION_AGREGA ) );
          try
             ParamAsChar( FDataset, 'LS_PATRON', sPatron, K_ANCHO_PATRON );
             ParamAsInteger( FDataset, 'LS_YEAR', iYear );
             ParamAsInteger( FDataset, 'LS_MONTH', iMes );
             ParamAsInteger( FDataset, 'LS_TIPO', Ord( Tipo ) );
             ParamAsInteger( FDataset, 'LS_STATUS', Ord( slCalculadaParcial ) );
             ParamAsInteger( FDataset, 'US_CODIGO', UsuarioActivo );
             Ejecuta( FDataset ); { Crea Liquidaci�n Y/O Inicializa su Status }
          finally
                 FreeAndNil( FDataset );
          end;
          SetGlobalesPagos( sPatron, iYear, iMes, Tipo );
          FFormulaPercepciones := GetGlobalString( K_GLOBAL_FORM_PERCEP );
          FFormulaDias := GetGlobalString( K_GLOBAL_FORM_DIAS );
          with FLiqIMSS do
          begin { Poner en Cero TODOS los valores totales de LIQ_IMSS }
               LS_NUM_TRA := 0;
               LS_DIAS_CO := 0;
               LS_EYM_DIN := 0;
               LS_EYM_ESP := 0;
               LS_EYM_EXC := 0;
               LS_EYM_FIJ := 0;
               LS_RIESGOS := 0;
               LS_INV_VID := 0;
               LS_GUARDER := 0;
               LS_RETIRO  := 0;
               LS_CES_VEJ := 0;
               LS_APO_VOL := 0;
               LS_INF_ACR := 0;
               LS_INF_AMO := 0;
               LS_INF_NAC := 0;
               LS_INF_NUM := 0;
               LS_ACT_IMS := 0;
               LS_ACT_RET := 0;
               LS_ACT_INF := 0;
               LS_REC_IMS := 0;
               LS_REC_INF := 0;
               LS_REC_RET := 0;
               LS_DIAS_BM := 0;
               LS_NUM_BIM := 0;
               LS_SUB_IMS := 0;
               LS_TOT_IMS := 0;
               LS_TOT_MES := 0;
               LS_SUB_RET := 0;
               LS_TOT_RET := 0;
               LS_SUB_INF := 0;
               LS_TOT_INF := 0;
               LS_IMSS_PA := 0;
               LS_IMSS_OB := 0;
               LS_ACT_AMO := 0;
               LS_ACT_APO := 0;
               LS_REC_AMO := 0;
               LS_REC_APO := 0;
          end;
          FDataset := CreateQuery( GetSQLScript( Q_EMPLEADO_BORRA ) );
          try
             ParamAsChar( FDataset, 'Patron', FPatron, K_ANCHO_PATRON );
             ParamAsInteger( FDataset, 'Year', FYear );
             ParamAsInteger( FDataset, 'Mes', FMes );
             ParamAsInteger( FDataset, 'Tipo', Ord( FTipoIMSS ) );
             Ejecuta( FDataset ); { Borra TODO LIQ_EMP y el trigger borra TODO LIQ_MOV }
          finally
                 FreeAndNil( FDataset );
          end;
          FDataset := CreateQuery( GetSQLScript( Q_IMSS_GET_PRIMA ) );
          try
             with FDataset do { Obtiene Prima ( � de Riesgo ? ) }
             begin
                  Active := False;
                  ParamAsChar( FDataset, 'Patron', FPatron, K_ANCHO_PATRON );
                  ParamAsDate( FDataset, 'Fecha', FBimFin );
                  Active := True;
                  FPrimaRiesgo := Fields[ 0 ].AsFloat;
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataset );
          end;
          FMovtoEscribe := CreateQuery( GetSQLScript( Q_MOVIMIENTO_ESCRIBE ) );
          ParamAsChar( FMovtoEscribe, 'LS_PATRON', FPatron, K_ANCHO_PATRON );
          ParamAsInteger( FMovtoEscribe, 'LS_YEAR', FYear );
          ParamAsInteger( FMovtoEscribe, 'LS_MONTH', FMes );
          ParamAsInteger( FMovtoEscribe, 'LS_TIPO', Ord( FTipoIMSS ) );
          FEmpleadoAgrega := CreateQuery( GetSQLScript( Q_EMPLEADO_AGREGA ) );
          ParamAsChar( FEmpleadoAgrega, 'LS_PATRON', FPatron, K_ANCHO_PATRON );
          ParamAsInteger( FEmpleadoAgrega, 'LS_YEAR', FYear );
          ParamAsInteger( FEmpleadoAgrega, 'LS_MONTH', FMes );
          ParamAsInteger( FEmpleadoAgrega, 'LS_TIPO', Ord( FTipoIMSS ) );
          ParamAsInteger( FEmpleadoAgrega, 'LE_STATUS', Ord( slCalculadaParcial ) );
          FEmpleadoModifica := CreateQuery( GetSQLScript( Q_EMPLEADO_MODIFICA ) );
          ParamAsChar( FEmpleadoModifica, 'LS_PATRON', FPatron, K_ANCHO_PATRON );
          ParamAsInteger( FEmpleadoModifica, 'LS_YEAR', FYear );
          ParamAsInteger( FEmpleadoModifica, 'LS_MONTH', FMes );
          ParamAsInteger( FEmpleadoModifica, 'LS_TIPO', Ord( FTipoIMSS ) );
          ParamAsInteger( FEmpleadoAgrega, 'LE_STATUS', Ord( slCalculadaTotal ) );
     end;
     PreparaQuerysBegin;
     InitPrimas;
     InitSalMin;
end;
{$endif}

procedure TIMSS.CalcularIMSSPagos;
const
     K_LM_EYM_ESP = 0;
     K_LM_EYM_FIJ = 1;
     K_LM_EYM_EXC = 2;
     K_LM_EYM_DIN = 3;
     K_LM_RIESGOS = 4;
     K_LM_INV_VID = 5;
     K_LM_GUARDER = 6;
type
    TLiquidacionMovimiento = record
      LM_CLAVE: String;
      LM_FECHA: TDate;
      LM_KAR_FEC: TDate;
      LM_KAR_TIP: String;
      LM_BASE: Extended;
      LM_TIPO: String;
      LM_DIAS: Integer;
      LM_INCAPAC: Integer;
      LM_AUSENCI: Integer;
      LM_INF_AMO: Extended;
      LM_EYM_ESP: Extended;
      LM_EYMESPP: Extended;
      LM_EYM_FIJ: Extended;
      LM_EYM_EXC: Extended;
      LM_EYMEXCP: Extended;
      LM_EYM_DIN: Extended;
      LM_EYMDINP: Extended;
      LM_RIESGOS: Extended;
      LM_INV_VID: Extended;
      LM_INVVIDP: Extended;
      LM_GUARDER: Extended;
      LM_RETIRO: Extended;
      LM_CES_VEJ: Extended;
      LM_CESVEJP: Extended;
      LM_INF_PAT: Extended;
      LM_COTIZA: Integer;  //Temporal: Dias cotizados, no se guarda en BD
    end;
    {$ifndef EVALUA_LIQ_IMSS}
    TLiquidacionEmpleado = record
      LE_IMSS_OB: Extended;
      LE_IMSS_PA: Extended;
      LE_EYM_FIJ: Extended;
      LE_EYM_EXC: Extended;
      LE_EYM_DIN: Extended;
      LE_EYM_ESP: Extended;
      LE_RIESGOS: Extended;
      LE_INV_VID: Extended;
      LE_GUARDER: Extended;
      LE_DIAS_CO: Integer;
      LE_RETIRO: Extended;
      LE_CES_VEJ: Extended;
      LE_INF_PAT: Extended;
      LE_INF_AMO: Extended;
      LE_DIAS_BM: Integer;
      LE_APO_VOL: Extended;
      LE_TOT_IMS: Extended;
      LE_TOT_RET: Extended;
      LE_TOT_INF: Extended;
      LE_ACUMULA: Extended;
      LE_BIM_ANT: Extended;
      LE_BIM_SIG: Extended;
      LE_VA_PAGO: Extended;
      LE_VA_GOZO: Extended;
      LE_PROV: Extended;
    end;
    {$endif}
var
   dUltimo: TDate;
   sFormulaDias, sFormulaPercepciones: String;
   {$ifndef EVALUA_LIQ_IMSS}
   FLiqEmpleado: TLiquidacionEmpleado;
   {$endif}
   FLiqMovimiento: TLiquidacionMovimiento;

{$ifdef EVALUA_LIQ_IMSS}
procedure InitLiquidacionEmpleado;
begin
     with FLiqEmpleado do
     begin
          LE_IMSS_OB := 0;
          LE_IMSS_PA := 0;
          LE_EYM_FIJ := 0;
          LE_EYM_EXC := 0;
          LE_EYMEXCP := 0;
          LE_EYM_DIN := 0;
          LE_EYMDINP := 0;
          LE_EYM_ESP := 0;
          LE_EYMESPP := 0;
          LE_RIESGOS := 0;
          LE_INV_VID := 0;
          LE_INVVIDP := 0;
          LE_GUARDER := 0;
          LE_DIAS_CO := 0;
          LE_RETIRO := 0;
          LE_CES_VEJ := 0;
          LE_CESVEJP := 0;
          LE_INF_PAT := 0;
          LE_INF_AMO := 0;
          LE_DIAS_BM := 0;
          LE_APO_VOL := 0;
          LE_TOT_IMS := 0;
          LE_TOT_RET := 0;
          LE_TOT_INF := 0;
          LE_INF_DIAS := 0;
     end;
end;
{$endif}

procedure AgregaLiquidacionEmpleado;
begin
     { Inicializa registro de Liquidacion Empleado }
     with oZetaProvider do
     begin
          ParamAsInteger( FEmpleadoAgrega, 'CB_CODIGO', FEmpleado.Codigo );
          Ejecuta( FEmpleadoAgrega );
     end;
     {$ifdef EVALUA_LIQ_IMSS}
     InitLiquidacionEmpleado;
     {$else}
     with FLiqEmpleado do
     begin
          LE_IMSS_OB := 0;
          LE_IMSS_PA := 0;
          LE_EYM_FIJ := 0;
          LE_EYM_EXC := 0;
          LE_EYM_DIN := 0;
          LE_EYM_ESP := 0;
          LE_RIESGOS := 0;
          LE_INV_VID := 0;
          LE_GUARDER := 0;
          LE_DIAS_CO := 0;
          LE_RETIRO := 0;
          LE_CES_VEJ := 0;
          LE_INF_PAT := 0;
          LE_INF_AMO := 0;
          LE_DIAS_BM := 0;
          LE_APO_VOL := 0;
          LE_TOT_IMS := 0;
          LE_TOT_RET := 0;
          LE_TOT_INF := 0;
     end;
     {$endif}
end;

procedure GetKardexAnterior( var dMovimiento: TDate; var sTipo: String; var rSalarioIntegrado: TPesos {$ifdef DESCUENTO_SUA}; var sZonaGeo: String {$endif} );
const
     K_CB_FECHA = 0;
     K_CB_TIPO = 1;
     K_CB_SAL_INT = 2;
begin
     with FKardexAnterior do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FKardexAnterior, 'Empleado', FEmpleado.Codigo );
               ParamAsChar( FKardexAnterior, 'Patron', FPatron, K_ANCHO_PATRON );
               ParamAsDate( FKardexAnterior, 'FechaInicial1', {$ifdef EVALUA_LIQ_IMSS}FechaInicial{$else}FLiqInicial{$endif} );
               ParamAsDate( FKardexAnterior, 'FechaInicial2', {$ifdef EVALUA_LIQ_IMSS}FechaInicial{$else}FLiqInicial{$endif} );
          end;
          Active := True;
          if EOF then
          begin
               dMovimiento := NullDateTime;
               sTipo := VACIO;
               rSalarioIntegrado := 0;
{$ifdef DESCUENTO_SUA}
               sZonaGeo := VACIO;
{$endif}
          end
          else
          begin
               dMovimiento := Fields[ K_CB_FECHA ].AsDateTime;
               sTipo := Trim( Fields[ K_CB_TIPO ].AsString );
               rSalarioIntegrado := Fields[ K_CB_SAL_INT ].AsFloat;
{$ifdef DESCUENTO_SUA}
               sZonaGeo := FieldByName( 'CB_ZONA_GE' ).AsString;
{$endif}
          end;
          Active := False;
     end;
end;

procedure CalculaLiquidacionEmpleado( const Tipo: eTipoLiqMov );
var
   sTipo: String;
   sClaveMov: String[2];
   rSalario, rSalarioAnterior: TPesos;
   dMovimiento, dMovimientoIMSS: TDate;
   iMovimientos: Word;
   lAmortizado: boolean;
   lCerrar, lHayBaja: Boolean;
{$ifdef DESCUENTO_SUA}
   sZonaGeo: String;
{$endif}
   // DES US #13101 Infonavit - Descuento tipo # veces UMA...(Funciones)
   bIndicarUMA: Boolean;
   bIndicarfactorDescuento: Boolean;

procedure ObreroPatronal( const iCampo: Integer; const rObrero, rPatronal: TPesos );
begin
     with FLiqMovimiento do
     begin
          case iCampo of
               K_LM_EYM_ESP: begin //LM_EYM_ESP := rObrero + rPatronal;
                                  LM_EYM_ESP := rObrero + rPatronal;
                                  LM_EYMESPP := rPatronal;
                             end;
               K_LM_EYM_FIJ: LM_EYM_FIJ := rObrero + rPatronal;
               K_LM_EYM_EXC: begin //LM_EYM_EXC := rObrero + rPatronal;
                                  LM_EYM_EXC := rObrero + rPatronal;
                                  LM_EYMEXCP := rPatronal;
                             end;
               K_LM_EYM_DIN: begin //LM_EYM_DIN := rObrero + rPatronal;
                                  LM_EYM_DIN := rObrero + rPatronal;
                                  LM_EYMDINP := rPatronal;
                             end;
               K_LM_RIESGOS: LM_RIESGOS := rObrero + rPatronal;
               K_LM_INV_VID: begin //LM_INV_VID := rObrero + rPatronal;
                                  LM_INV_VID := rObrero + rPatronal;
                                  LM_INVVIDP := rPatronal;
                             end;
               K_LM_GUARDER: LM_GUARDER := rObrero + rPatronal;
          end;
     end;
     with FLiqEmpleado do
     begin
		  LE_IMSS_OB := LE_IMSS_OB + rObrero;
		  LE_IMSS_PA := LE_IMSS_PA + rPatronal;
     end;
end;

procedure CierraMovimiento( sClaveCierre: String; dCierre: TDate );
var
   dInicial: TDate;
   iDias, iDiasEyM, iDiasIVCM, iDiasSAR, iFaltas, iIncapaci, iCurrentPos: Integer;
   sClave: String;
   rSalBase: TPesos;
   sFormatoRastreo: String;

procedure CierraDias;
begin
     with FLiqMovimiento do
     begin
          dInicial := LM_FECHA;
          { Corte un d�a antes del cambio de salario }
          if ( sClaveCierre = K_M_MODIFICACION ) then
             dCierre := dCierre - 1;
          if ( FTipoPeriodo = tlmMensual ) then
             dUltimo := dCierre;
          if ( FEmpleado.ModuloPatron <> pmPermanente ) then
          begin
               iDias := Trunc( dCierre - dInicial ) + 1;
               iFaltas := 0;
               iIncapaci := 0;
          end
          else
              if ( sClave = K_M_BAJA ) then
              begin
                   iDias := 0;
                   iFaltas := 0;
                   iIncapaci := 0;
                   rSalBase := 0;
                   LM_BASE := rSalBase;
              end
              else
              begin
                   iDias := Trunc( dCierre - dInicial ) + 1;
                   RevisaFaltas( dInicial, dCierre, iFaltas, iIncapaci );
              end;
          if ( sClave = K_M_OK ) then
             LM_FECHA := NullDateTime;
          LM_DIAS := iDias;
          LM_INCAPAC := iIncapaci;
          LM_AUSENCI := iFaltas;
     end;
end;

// DES US #13122 Infonavit - Descuento tipo # veces UMA...(Liquidaci�n IMSS)
// Agregar nuevo par�metro rValUMA.
procedure SetDiasInfonavit(var iFaltasInfo, iIncapaInfo: Integer; var iDiasPrestaInf: Word;
          const rSMGDF: TPesos; var rTopado, rTotalPresta: TPesos; const rValUMA, rValFactorDescuento: TPesos);
var
   dMovInfonavit, dInicioPresta: TDate;
   eTipoMov: eTipoMovInfonavit;
   lCerroMontos: Boolean;
   {$ifdef FIX_CR1757}
   iFirstDayInfo: TDate;
   iFaltasAntes, iIncapaAntes: Integer;
   {$endif}

   // DES US #13122 Infonavit - Descuento tipo # veces UMA...(Liquidaci�n IMSS)
   rValMenorUMA_SMGDF: TPesos;
   rTiVeces_Factor, rTiVecesUMA_Factor: TPesos;

   procedure CierraFaltasInfonavit( const dIni, dFin: TDate; var iCountFaltas, iCountIncapa: Integer );
   var
      iFaltasPer, iIncapaPer: Integer;
   begin
        if ( dIni >= dInicial ) and ( dIni <= dFin )  then
        begin
             {$ifndef FIX_CR1757}
             FaltasStore;
             {$endif}
             { Llamar RevisaFaltas con acumulados en cero}
             RevisaFaltas( dIni, dFin, iFaltasPer, iIncapaPer );
             { Restaurar los acumulados de Faltas anteriores }
             {$ifndef FIX_CR1757}
             FaltasRestore;
             {$endif}

             iCountFaltas:= iCountFaltas + iFaltasPer;
             iCountIncapa:= iCountIncapa + iIncapaPer;
        end;
   end;

   procedure ResetAcumuladosDias;
   begin
        iDiasPrestaInf:= 0;
        iFaltasInfo:=0;
        iIncapaInfo:= 0;
   end;

   procedure ResetAcumuladosMonto;
   begin
        with FLiqMovimiento do
        begin
             LM_INF_AMO:= 0;
             LM_COTIZA:= 0;
        end;
   end;

   procedure CierraMontosInfo;
   var
      rTasa: TPesos;
   begin
        PreparaInfonavitEmpleado( FEmpleado.Codigo, dMovInfonavit ); //Se cambian las reglas al movimiento
        with FCursorInfonavitVigente, FLiqMovimiento do
        begin
             if ( iDiasPrestaInf > 0 ) then
             begin
                  rTasa:= FieldByName('CB_INFTASA').AsFloat;
                  case eTipoInfonavit( FieldByName('CB_INFTIPO').AsInteger ) of
                       tiPorcentaje:
                       begin
                            if zStrToBool( FieldByName('CB_INFDISM').AsString) then
                            begin
                                 rTasa:= GetDisminucionTasa(rMax(FBimInicio,dMovInfonavit));
                            end;

                            // DES US #13122 Infonavit - Descuento tipo # veces UMA...(Liquidaci�n IMSS)
                            if FUsarUMA then
                               rTotalPresta := CalculaCuota( FPrestamoTope, rSalBase, iDiasPrestaInf, rValUMA, rTasa, rTopado )
                            else
                                rTotalPresta := CalculaCuota( FPrestamoTope, rSalBase, iDiasPrestaInf, rSMGDF, rTasa, rTopado );
                       end;
                       tiCuotaFija: rTotalPresta := CalculaCuotaFija( rTasa, iDiasPrestaInf, FDiasBim );
                       // DES US #13122 Infonavit - Descuento tipo # veces UMA...(Liquidaci�n IMSS)
                       tiVeces: rTotalPresta := CalculaCuotaFija( rTasa * rTiVeces_Factor, iDiasPrestaInf, FDiasBim );
                       tiVecesUMA: rTotalPresta := CalculaCuotaFija( rTasa * rTiVecesUMA_Factor, iDiasPrestaInf, FDiasBim );
                  end;
                  LM_COTIZA:=  LM_COTIZA + iDiasPrestaInf;
                  LM_INF_AMO:= LM_INF_AMO + rTotalPresta;
                  lCerroMontos:= TRUE;


                  if ( FEvaluandoIMSS and FCreator.Rastreando ) then
                  begin
                       FTextoTempRastreo:= FTextoTempRastreo + CR_LF + Format( '%9.9s %s  %.3d    %s %9.2n %9.2n %9.2n',
                                                                      [ SetDateTimeText( dMovInfonavit, True ),
                                                                      aTipoMov[eTipoMov],
                                                                      iDiasPrestaInf,
                                                                      ZetaCommonLists.ObtieneElemento( lfTipoInfonavit, FieldByName('CB_INFTIPO').AsInteger ),
                                                                      rTasa,
                                                                      rTotalPresta/rMax(iDiasPrestaInf,1),
                                                                      rTotalPresta ] );
                  end;


             end;
        end;
        ResetAcumuladosDias;
   end;

begin
     ResetAcumuladosMonto;
     ResetAcumuladosDias;
     dInicioPresta:= dInicial;
     PreparaKardexInfonavit(dCierre);
     lCerroMontos:= FALSE;
     {$ifdef FIX_CR1757}
     { Primero hay que guardar los acumulados de faltas anteriores }
     FaltasStore;
     {$endif}

     // DES US #13122 Infonavit - Descuento tipo # veces UMA...(Liquidaci�n IMSS)
     // Establecer rValMenorUMA_SMGDF.
     rValMenorUMA_SMGDF := rSMGDF;
     if (rValUMA <> 0) and (rValUMA < rSMGDF) then
        rValMenorUMA_SMGDF := rValUMA;

     if rValFactorDescuento = 0 then
     begin
          rTiVeces_Factor := rValMenorUMA_SMGDF;
          rTiVecesUMA_Factor := rValUMA;
     end
     else
     begin
          rTiVeces_Factor := rValFactorDescuento;
          rTiVecesUMA_Factor := rValFactorDescuento;
     end;

     with FKardexInfonavit do
     begin
          Active:= True;
          while not EOF do   //Barre los movimientos de Inicio y Suspensi�n
          begin
               dMovInfonavit:= FieldByName('KI_FECHA').AsDateTime;
               eTipoMov:= eTipoMovInfonavit( FieldByName('KI_TIPO').AsInteger );
               case eTipoMov of
                    infoInicio:
                    begin
                         dInicioPresta:= dMovInfonavit;
                    end;
                    infoSuspen:
                    begin
                         iFirstDayInfo := rMax( dInicioPresta, dInicial );
                         iDiasPrestaInf:= iDiasPrestaInf + iMax( Trunc( ( dMovInfonavit + 1 ) - iFirstDayInfo ), 0);
                         FEmpleado.FechaSuspension:= dMovInfonavit;
                         {$ifdef FIX_CR1757} // No descontar las faltas anteriores al inicio del prestamo
                         if ( TheDay( iFirstDayInfo ) > 1 ) then
                            RevisaFaltas( FirstDayOfMonth(iFirstDayInfo), iFirstDayInfo-1, iFaltasAntes, iIncapaAntes );
                         {$endif}
                         CierraFaltasInfonavit( iFirstDayInfo, dMovInfonavit, iFaltasInfo, iIncapaInfo);
                         iDiasPrestaInf:= iDiasPrestaInf - iFaltasInfo - iIncapaInfo;
                         dInicioPresta:= dCierre + 1;
                         CierraMontosInfo; //Acumula montos y Resetea los d�as
                    end;
                    infoReinicio:
                    begin
                         dInicioPresta:= dMovInfonavit;
                         FHuboReinicioCredito:= lCerroMontos;
                    end;
               end;
               Next;
          end;

          if ( dInicioPresta <= dCierre ) then   //Si no hubo suspensiones
          begin
               iFirstDayInfo := rMax( dInicioPresta, dInicial );
               iDiasPrestaInf:= iDiasPrestaInf + ( Trunc( dCierre - iFirstDayInfo ) + 1 );  //Ve los dias entre el inicio y el cierre
               {$ifdef FIX_CR1757} // No descontar las faltas anteriores al inicio del prestamo
               if ( TheDay( iFirstDayInfo ) > 1 ) then
                  RevisaFaltas( FirstDayOfMonth(iFirstDayInfo), iFirstDayInfo-1, iFaltasAntes, iIncapaAntes );
               {$endif}
               CierraFaltasInfonavit( iFirstDayInfo, dCierre, iFaltasInfo, iIncapaInfo);
               iDiasPrestaInf:= iDiasPrestaInf - iFaltasInfo - iIncapaInfo;
               CierraMontosInfo; // Tambien cierra montos para revisar como estaba el prestamo al cierre
          end;
     end;
     {$ifdef FIX_CR1757}
     { Restaurar los acumulados de Faltas anteriores }
     FaltasRestore;
     {$endif}
end;

procedure CierraMontos;
var
   rTopado, rSMGDF: TPesos;

   // DES US #13122 Infonavit - Descuento tipo # veces UMA...(Liquidaci�n IMSS)
   rValUMA: TPesos;
   rValMenorUMA_SMGDF: TPesos;
   rValFactorDescuento: TPesos;
   rTiVeces_Factor, rTiVecesUMA_Factor: TPesos;

   iDiasPresta{$ifndef SUA_14}, iDiasNoHay, iMesesCotiza{$endif}: Word;
   dBajaImss, dEmpFinal : TDate;
{$ifdef DESCUENTO_SUA}
   rFactorDescto: TTasa;
{$endif}
   rTotalPresta: TPesos;
begin
     iDiasEyM := iMax( iDias - iIncapaci, 0 );
     iDiasIVCM := iMax( iDiasEyM - iFaltas, 0 );
     iDiasSAR := iMax( iDias - iFaltas, 0 );
     rSMGDF := FCreator.dmSalMin.GetSMGDF( FBimFin );

     // DES US #13122 Infonavit - Descuento tipo # veces UMA...(Liquidaci�n IMSS)
     rValUMA := FCreator.dmValUMA.GetVUDefaultDiario ( FBimFin );
     rValFactorDescuento := FCreator.dmValUMA.GetFactorDescuento ( FBimFin );

     rValMenorUMA_SMGDF := rSMGDF;
     if (rValUMA <> 0) and (rValUMA < rSMGDF) then
        rValMenorUMA_SMGDF := rValUMA;

     if rValFactorDescuento = 0 then
     begin
          rTiVeces_Factor := rValMenorUMA_SMGDF;
          rTiVecesUMA_Factor := rValUMA;
     end
     else
     begin
          rTiVeces_Factor := rValFactorDescuento;
          rTiVecesUMA_Factor := rValFactorDescuento;
     end;

     {$ifdef IMSS_UMA}
     rSMGDF := rValUMA;   // A partir del 2017 se utiliza el valor de UMA en vez de SMGDF - Se conserva la variable rSMGDF para cambiar menos c�digo
     {$endif}
     FHuboReinicioCredito:= FALSE;
     FTextoTempRastreo:= VACIO;
{$ifdef DESCUENTO_SUA}

     if FCreator.dmPrimasIMSS.GetAplicaDescuento( FEmpleado.PatronInicial, rFactorDescto ) then  // Revisa Salario minimo
     begin
          if ( not PesosIguales( rSalBase, ZetaCommonTools.Redondea( FCreator.dmSalMin.GetSalMin( FBimFin, sZonaGeo ) * K_FACTOR_INTEGRACION ) ) ) then
             rFactorDescto := 1;    // Solo aplicar el factor de descuento si el salario es igual al m�nimo
     end;
{$endif}
     with FCreator.dmPrimasIMSS.GetPrimasIMSS( dCierre {$ifdef DESCUENTO_SUA}, FEmpleado.PatronInicial {$endif} ) do
     begin
          if ( FTipoPeriodo = tlmMensual ) then
          begin
               ObreroPatronal( K_LM_EYM_ESP,
                               CalculaCuota( SS_L_A25, rSalBase, iDiasEyM, rSMGDF, SS_O_A25 {$ifdef DESCUENTO_SUA} * rFactorDescto {$endif}, rTopado ),
                               CalculaCuota( SS_L_A25, rSalBase, iDiasEyM, rSMGDF, SS_P_A25, rTopado ) );
               ObreroPatronal( K_LM_EYM_FIJ,
                               0,
                               CalculaCuota( 1, rSMGDF, iDiasEyM, rSMGDF, SS_P_A1061, rTopado ) );
               ObreroPatronal( K_LM_EYM_EXC,
                               CalculaCuota( 0, rMax( rSalBase - SS_MAXIMO * rSMGDF, 0 ), iDiasEyM, rSMGDF, SS_O_A1062, rTopado ),
                               CalculaCuota( 0, rMax( rSalBase - SS_MAXIMO * rSMGDF, 0 ), iDiasEyM, rSMGDF, SS_P_A1062, rTopado ) );
               ObreroPatronal( K_LM_EYM_DIN,
                               CalculaCuota( SS_L_A107, rSalBase, iDiasEyM, rSMGDF, SS_O_A107 {$ifdef DESCUENTO_SUA} * rFactorDescto {$endif}, rTopado ),
                               CalculaCuota( SS_L_A107, rSalBase, iDiasEyM, rSMGDF, SS_P_A107, rTopado ) );
               ObreroPatronal( K_LM_RIESGOS,
                               0,
                               CalculaCuota( SS_L_RT, rSalBase, iDiasIVCM, rSMGDF, FPrimaRiesgo, rTopado ) );
               ObreroPatronal( K_LM_INV_VID,
                               CalculaCuota( SS_L_IV, rSalBase, iDiasIVCM, rSMGDF, SS_O_IV {$ifdef DESCUENTO_SUA} * rFactorDescto {$endif}, rTopado ),
                               CalculaCuota( SS_L_IV, rSalBase, iDiasIVCM, rSMGDF, SS_P_IV, rTopado ) );
               ObreroPatronal( K_LM_GUARDER,
                               0,
                               CalculaCuota( SS_L_GUARD, rSalBase, iDiasIVCM, rSMGDF, SS_P_GUARD, rTopado ) );
          {$ifdef EVALUA_LIQ_IMSS}
          end;
          if FEvaluandoIMSS or ( FTipoPeriodo = tlmBimestral ) then
          {$else}
          end
          else
          {$endif}
          begin
               with FLiqMovimiento do
               begin
                    LM_RETIRO := CalculaCuota( SS_L_SAR, rSalBase, iDiasSAR, rSMGDF, SS_P_SAR, rTopado );
                    {LM_CES_VEJ := CalculaCuota( SS_L_CV, rSalBase, iDiasIVCM, rSMGDF, SS_P_CV, rTopado ) +
                                  CalculaCuota( SS_L_CV, rSalBase, iDiasIVCM, rSMGDF, SS_O_CV, rTopado );}
                    LM_CESVEJP := CalculaCuota( SS_L_CV, rSalBase, iDiasIVCM, rSMGDF, SS_P_CV, rTopado );
                    LM_CES_VEJ := CalculaCuota( SS_L_CV, rSalBase, iDiasIVCM, rSMGDF, SS_O_CV, rTopado ) + LM_CESVEJP;
                    LM_INF_PAT := CalculaCuota( SS_L_INFO, rSalBase, iDiasSAR, rSMGDF, SS_P_INFO, rTopado );
                    // Descuento de pr�stamos de Infonavit
                    {$ifdef IMSS_UMA}
                    // Se vuelve a asignar el SMGDF para no afectar la l�gica existente en Infonavit
                    rSMGDF := FCreator.dmSalMin.GetSMGDF( FBimFin );
                    {$endif}
                    with FEmpleado do
                    begin
                         {
                          Cuando el inicio del pr�stamo ocurre durante el bimestre y el pr�stamo
                          es de cuota fija o veces SMGDF hay que ajustar la fecha de arranque de la amortizaci�n
                         }
                         PrestamoInicio:= GetPrestamoInicio(dCierre);

                         // DES US #13122 Infonavit - Descuento tipo # veces UMA...(Liquidaci�n IMSS)
                         if ( PrestamoTipo in [ tiCuotaFija, tiVeces, tiVecesUMA ] ) and ( Prestamo > 0 )then
                         begin
                              if ( PrestamoInicio >= FBimInicio ) and ( PrestamoInicio <= FBimFin ) then
                              begin
                                   { Se checa el Global de Amortizaci�n }
                                   case FInicioAmortizacion of
                                        iaBimestre: PrestamoInicio := SiguienteBimestre( PrestamoInicio );
                                        iaMes: PrestamoInicio := SiguienteMes( PrestamoInicio );
                                        iaEsteMes: PrestamoInicio := EsteMes( PrestamoInicio );
                                   else
                                       PrestamoInicio := SiguienteBimestre( PrestamoInicio );
                                   end;
                              end;
                         end;
                         if ( PrestamoTipo <> tiNoTiene ) and ( Prestamo > 0 ) and ( PrestamoInicio <= dCierre ) then
                         begin
                              // Por Bug en SUA, se env�a como CuotaFija
                              if FCuotaFija and ( PrestamoTipo = tiPorcentaje ) and ( TasaAnterior > 0 ) then
                              begin
                                   dBajaIMSS := FechaBSS;
                                   if ( dBajaIMSS < Ingreso ) and ( FechaSuspension = NullDateTime ) then
                                      dEmpFinal := {$ifdef EVALUA_LIQ_IMSS}FechaFinal{$else}FLiqFinal{$endif}
                                   else
                                       // Minimo entre la suspension y la baja dEmpFinal := rMin( dBajaIMSS, {$ifdef EVALUA_LIQ_IMSS}FechaFinal{$else}FLiqFinal{$endif} );
                                       dEmpFinal := rMin( rMinSinNulos( dBajaIMSS, FechaSuspension ), {$ifdef EVALUA_LIQ_IMSS}FechaFinal{$else}FLiqFinal{$endif} );

                                   PrestamoInicio := rMax( PrestamoInicio, FBimInicio );
                                   { Primero hay que guardar los acumulados de faltas anteriores }
                                   FaltasStore;
                                   { Llamar RevisaFaltas con acumulados en cero }
                                   RevisaFaltas( PrestamoInicio, dEmpFinal, iFaltas, iIncapaci );
                                   { Restaurar los acumulados de Faltas anteriores }
                                   FaltasRestore;
                                   Prestamo := Redondea( GetSalarioSua( FechaSalarioIntegrado, FBimInicio ) *
                                               ( Prestamo / 100 ) * rMax( dEmpFinal - PrestamoInicio + 1 - iFaltas - iIncapaci, 0 ) );
                                   if {$ifdef EVALUA_LIQ_IMSS}not FEvaluandoIMSS and {$endif}( FEsBimestre ) then
                                   begin
                                        if ( TheMonth( PrestamoInicio ) <> TheMonth( dEmpFinal ) ) then // Cuando El Empleado Cotiz� los dos Meses //
                                           Prestamo := Prestamo / 2.0;
                                   end;
                                   PrestamoTipo := tiCuotaFija;
                                   PrestamoInicio := FBimInicio;
                              end;
                              {$ifndef SUA_14}
                              if ( PrestamoTipo = tiPorcentaje ) then
                              begin
                              {$endif}
                                   if not lHayBaja then
                                   begin
                                        if  ( FechaSuspension = NullDateTime ) or ( not FSuspenBimMed ) then
                                        begin
                                             { Inicio de Pr�stamo a medio movimiento, resta d�as }
                                             if ( PrestamoInicio <= dInicial ) then
                                                iDiasPresta := iDiasIVCM
                                             else
                                             begin
                                                  {$ifdef SUA_14}
                                                  iDiasPresta := Trunc( dCierre - PrestamoInicio ) + 1;
                                                  if ( iDiasPresta > 0 ) then
                                                  begin
                                                       { Primero hay que guardar los acumulados de faltas anteriores }
                                                       FaltasStore;
                                                       { Llamar RevisaFaltas con acumulados en cero }
                                                       {$ifdef FIX_CR1757} // No descontar las faltas anteriores al inicio del prestamo
                                                       if ( TheDay( PrestamoInicio ) > 1 ) then
                                                          RevisaFaltas( FirstDayOfMonth(PrestamoInicio), PrestamoInicio-1, iFaltas, iIncapaci );
                                                       {$endif}
                                                       RevisaFaltas( PrestamoInicio, dCierre, iFaltas, iIncapaci );  //No reinicia Faltas para conservar el tope del mes
                                                       { Restaurar los acumulados de Faltas anteriores }
                                                       FaltasRestore;
                                                       iDiasPresta := ( iDiasPresta - iFaltas - iIncapaci );
                                                  end;
                                                  {$else}
                                                  iDiasPresta := iDiasIVCM;
                                                  iDiasNoHay  := Trunc( PrestamoInicio - dInicial );
                                                  if ( iDiasNoHay >= iDiasPresta ) then
                                                     iDiasPresta := 0
                                                  else
                                                      iDiasPresta := iDiasPresta - iDiasNoHay;
                                                  {$endif}
                                             end;

                                             case PrestamoTipo of
                                                  // DES US #13122 Infonavit - Descuento tipo # veces UMA...(Liquidaci�n IMSS)
                                                  tiPorcentaje:
                                                  begin
                                                       if FUsarUMA then
                                                          rTotalPresta := CalculaCuota( FPrestamoTope, rSalBase, iDiasPresta, rValUMA, Prestamo, rTopado )
                                                       else
                                                           rTotalPresta := CalculaCuota( FPrestamoTope, rSalBase, iDiasPresta, rSMGDF, Prestamo, rTopado );
                                                  end;
                                                  tiCuotaFija: rTotalPresta := CalculaCuotaFija( Prestamo, iDiasPresta, FDiasBim );
                                                  // DES US #13122 Infonavit - Descuento tipo # veces UMA...(Liquidaci�n IMSS)
                                                  tiVeces: rTotalPresta := CalculaCuotaFija( Prestamo * rTiVeces_Factor, iDiasPresta, FDiasBim );
                                                  tiVecesUMA: rTotalPresta := CalculaCuotaFija( Prestamo * rTiVecesUMA_Factor, iDiasPresta, FDiasBim );
                                                  else rTotalPresta:= 0;
                                             end;

                                             LM_COTIZA:= LM_COTIZA + iDiasPresta;
                                             LM_INF_AMO:= LM_INF_AMO + rTotalPresta;

                                        end
                                        else
                                        begin
                                             // DES US #13122 Infonavit - Descuento tipo # veces UMA...(Liquidaci�n IMSS)
                                             // Agregar rValUMA como par�metro al m�todo SetDiasInfonavit.
                                             SetDiasInfonavit(iFaltas, iIncapaci, iDiasPresta, rSMGDF, rTopado, rTotalPresta, rValUMA, rValFactorDescuento ); //Se trae los d�as de Infonavit y hace los cortes x suspensi�n
                                        end;

                                        {$ifdef SUA_14}
{
                                        if ( Codigo = 21280 ) then
                                           ShowMessage( IntToStr( iDiasPresta ) + '|' + IntToStr( FDiasBim ) + '|' +
                                                        FloatToStr( Prestamo ) + '|' + IntToStr( iDiasIVCM ) + '|' +
                                                        FloatToStr( PrestamoInicio ) + '|' + FloatToStr( dInicial ) + '|' + CR_LF +
                                                        FechaAsStr( PrestamoInicio ) + '|' + FechaAsStr( dInicial ) );
}

                                        if ( FEvaluandoIMSS and FCreator.Rastreando ) and FHuboReinicioCredito then
                                        begin
                                                                                              //Solamente si hubo reinicio y suspensi�n concatena los dos movimientos
                                              FTextoTempRastreo:= CR_LF + Format( '%0:s Movimientos Infonavit %0:s', [ StringOfChar( '-', 10 ) ] ) + CR_LF + CR_LF +
                                                                    '  Fecha   Tipo       Cotiza  Tipo Desc.    V.Desc.   V.Diario    Total' + CR_LF +
                                                                    '--------- ---------- ------  ---------- ----------  --------    ----- ' + CR_LF  +
                                                                    FTextoTempRastreo;

                                        end
                                        else
                                            FTextoTempRastreo:= VACIO;

                                        {$else}

                                        LM_INF_AMO := CalculaCuota( FPrestamoTope, rSalBase, iDiasPresta, rSMGDF, Prestamo, rTopado );

                                        {$endif}
                                        {ER (01/Mar/2006) - Se suma a LM_INF_AMO el monto del seguro de vivienda }
                                        if ( LM_INF_AMO > 0 ) and ( not lAmortizado ) then   // Solo se agrega el monto del seguro a la primera amortizaci�n
                                        begin
                                             LM_INF_AMO := LM_INF_AMO + FSeguroVivienda;
                                             lAmortizado := TRUE;
                                        end;
                                   end;
                              {$ifndef SUA_14}
                              end
                              else
                                  if ( iMovimientos = 1 ) or not lAmortizado then   { S�lo el primer movimiento }
                                  begin
                                       lAmortizado := True; //Mas de un movimiento en el bimestre
                                       {$ifdef EVALUA_LIQ_IMSS}
                                       if FEvaluandoIMSS then
                                            iMesesCotiza := 1
                                       else
                                       {$endif}
                                       { Cuantos meses del bimestre cotiz� }
                                       { dUltimo es fecha Ultimo movimiento del Segundo Mes }
                                       if ( dUltimo = NullDateTime ) or { Si el empleado no cotiz� en el segundo mes, es igual a 0 - NullDateTime }
                                          ( ZetaCommonTools.TheMonth( dInicial ) = ZetaCommonTools.TheMonth( dUltimo ) ) or
                                          ( ( ZetaCommonTools.TheMonth( PrestamoInicio ) = ZetaCommonTools.TheMonth( dUltimo ) ) and ( ZetaCommonTools.TheYear( PrestamoInicio ) = ZetaCommonTools.TheYear( dUltimo ) ) ) then
                                       begin
                                            iMesesCotiza := 1
                                       end
                                       else
                                           iMesesCotiza := 2;
                                       if ( PrestamoTipo = tiCuotaFija ) then
                                          LM_INF_AMO := Redondea( Prestamo * iMesesCotiza )
                                       else
                                       begin
                                            LM_INF_AMO := Redondea( Prestamo * rSMGDF * iMesesCotiza );
                                       end;
                                       {ER (01/Mar/2006) - Se suma a LM_INF_AMO el monto del seguro de vivienda }
                                       if ( LM_INF_AMO > 0 ) then
                                          LM_INF_AMO := LM_INF_AMO + FSeguroVivienda;
                                  end;
                              {$endif}
                              if ( sClave = K_M_BAJA ) then
                                 lHayBaja := True;
                         end;
                    end;
               end;
          end;
     end;
end;

procedure SumaMontos;
begin
     if ( FTipoPeriodo = tlmMensual ) then
     begin
          with FLiqEmpleado do
          begin
               with FLiqMovimiento do
               begin
                    LE_EYM_FIJ := LE_EYM_FIJ + LM_EYM_FIJ;
                    LE_EYM_EXC := LE_EYM_EXC + LM_EYM_EXC;
                    LE_EYM_DIN := LE_EYM_DIN + LM_EYM_DIN;
                    LE_EYM_ESP := LE_EYM_ESP + LM_EYM_ESP;
                    LE_RIESGOS := LE_RIESGOS + LM_RIESGOS;
                    LE_INV_VID := LE_INV_VID + LM_INV_VID;
                    LE_GUARDER := LE_GUARDER + LM_GUARDER;
                    {$ifdef EVALUA_LIQ_IMSS}
                    LE_EYMEXCP := LE_EYMEXCP + LM_EYMEXCP;
                    LE_EYMDINP := LE_EYMDINP + LM_EYMDINP;
                    LE_EYMESPP := LE_EYMESPP + LM_EYMESPP;
                    LE_INVVIDP := LE_INVVIDP + LM_INVVIDP;

                    LE_RETIRO := LE_RETIRO + LM_RETIRO;
                    LE_CES_VEJ := LE_CES_VEJ + LM_CES_VEJ;
                    LE_INF_PAT := LE_INF_PAT + LM_INF_PAT;
                    LE_INF_AMO := LE_INF_AMO + LM_INF_AMO;

                    LE_CESVEJP := LE_CESVEJP + LM_CESVEJP;
                    LE_INF_DIAS := LE_INF_DIAS + LM_COTIZA;
                    {$endif}
               end;
               LE_DIAS_CO := LE_DIAS_CO + iDiasIVCM;
          end;
     end
     else
     begin
          with FLiqEmpleado do
          begin
               with FLiqMovimiento do
               begin
                    LE_RETIRO := LE_RETIRO + LM_RETIRO;
                    LE_CES_VEJ := LE_CES_VEJ + LM_CES_VEJ;
                    LE_INF_PAT := LE_INF_PAT + LM_INF_PAT;
                    LE_INF_AMO := LE_INF_AMO + LM_INF_AMO;
                    {$ifdef EVALUA_LIQ_IMSS}
                    LE_CESVEJP := LE_CESVEJP + LM_CESVEJP;
                    LE_INF_DIAS := LE_INF_DIAS + LM_COTIZA;
                    {$endif}
               end;
               LE_DIAS_BM := LE_DIAS_BM + iDiasIVCM;
          end;
     end;
end;


{Mejora descuento INFONAVIT en nomina AV Marzo/2011  Version 2011-B4 }
function GetTotalPorMovimiento( iLM_COTIZA: Integer; rLM_INF_AMO : Extended ) : TPesos;
begin
 if ( UsarFactorNomInfo ) then
      Result := iLM_COTIZA * FactorNomInfo
 else
     Result := rLM_INF_AMO;
end;


begin   { CierraMov }
     with FLiqMovimiento do
     begin
          sClave := LM_CLAVE;
          rSalBase := LM_BASE;
     end;
     CierraDias;
     if ( rSalBase > 0 ) then
     begin
          CierraMontos;
          SumaMontos;
     end;
     {$ifdef EVALUA_LIQ_IMSS}
     if FEvaluandoIMSS then
     begin
          if FCreator.Rastreando then
          begin
               with FLiqMovimiento do
               begin
                    // DES US #13101 Infonavit - Descuento tipo # veces UMA...(Funciones)
                    if ( FEmpleado.PrestamoTipo in [ tiCuotaFija,tiVeces,tiVecesUMA ] ) then
                    begin
                         if ( FEmpleado.PrestamoTipo = tiCuotaFija ) then
                            sFormatoRastreo := '%9.9s %9.9s  %2.2s  %10.2n  %.3d  %.3d  %.3d  %.3d   %.3d %9.2n'
                         else
                             sFormatoRastreo := '%9.9s %9.9s  %2.2s  %10.4n  %.3d  %.3d  %.3d  %.3d   %.3d %9.2n';
                         Rastreador.RastreoTextoLibre( Format( sFormatoRastreo,
                                                       [ SetDateTimeText( LM_FECHA, True ),
                                                         SetDateTimeText( LM_KAR_FEC, True ),
                                                         LM_KAR_TIP,
                                                         FEmpleado.Prestamo*2,
                                                         FDiasBim,
                                                         LM_DIAS,
                                                         LM_INCAPAC,
                                                         LM_AUSENCI,
                                                         LM_COTIZA,
                                                         GetTotalPorMovimiento( LM_COTIZA, LM_INF_AMO) ] ), iCurrentPos );
                    end
                    else
                    begin
                         Rastreador.RastreoTextoLibre( Format( '%9.9s %9.9s  %2.2s  $%8.2n  %.3d  %.3d  %.3d  %.3d   %3.2n %9.2n',
                                                       [ SetDateTimeText( LM_FECHA, True ),
                                                         SetDateTimeText( LM_KAR_FEC, True ),
                                                         LM_KAR_TIP,
                                                         LM_BASE,
                                                         LM_DIAS,
                                                         LM_INCAPAC,
                                                         LM_AUSENCI,
                                                         LM_COTIZA,
                                                         FEmpleado.Prestamo,
                                                         GetTotalPorMovimiento( LM_COTIZA, LM_INF_AMO)  ] ), iCurrentPos );
                    end;
                    Rastreador.RastreoInsertaTexto(FTextoTempRastreo, iCurrentPos);
               end;
          end;
     end
     else
     begin
     {$endif}
     with oZetaProvider do
     begin
          ParamAsInteger( FMovtoEscribe, 'CB_CODIGO', FEmpleado.Codigo );
          with FLiqMovimiento do
          begin
               ParamAsChar( FMovtoEscribe, 'LM_CLAVE', LM_CLAVE, K_ANCHO_CLAVE );
               ParamAsDate( FMovtoEscribe, 'LM_FECHA', LM_FECHA );
               ParamAsDate( FMovtoEscribe, 'LM_KAR_FEC', LM_KAR_FEC );
               ParamAsChar( FMovtoEscribe, 'LM_KAR_TIP', LM_KAR_TIP, K_ANCHO_TIPO_KARDEX );
               ParamAsFloat( FMovtoEscribe, 'LM_BASE', LM_BASE );
               ParamAsChar( FMovtoEscribe, 'LM_TIPO', LM_TIPO, K_ANCHO_TIPO_MOV );
               ParamAsInteger( FMovtoEscribe, 'LM_DIAS', LM_DIAS );
               ParamAsInteger( FMovtoEscribe, 'LM_INCAPAC', LM_INCAPAC );
               ParamAsInteger( FMovtoEscribe, 'LM_AUSENCI', LM_AUSENCI );
               ParamAsFloat( FMovtoEscribe, 'LM_INF_AMO', LM_INF_AMO );
               ParamAsFloat( FMovtoEscribe, 'LM_EYM_ESP', LM_EYM_ESP );
               ParamAsFloat( FMovtoEscribe, 'LM_EYMESPP', LM_EYMESPP );
               ParamAsFloat( FMovtoEscribe, 'LM_EYM_FIJ', LM_EYM_FIJ );
               ParamAsFloat( FMovtoEscribe, 'LM_EYM_EXC', LM_EYM_EXC );
               ParamAsFloat( FMovtoEscribe, 'LM_EYMEXCP', LM_EYMEXCP );
               ParamAsFloat( FMovtoEscribe, 'LM_EYM_DIN', LM_EYM_DIN );
               ParamAsFloat( FMovtoEscribe, 'LM_EYMDINP', LM_EYMDINP );
               ParamAsFloat( FMovtoEscribe, 'LM_RIESGOS', LM_RIESGOS );
               ParamAsFloat( FMovtoEscribe, 'LM_INV_VID', LM_INV_VID );
               ParamAsFloat( FMovtoEscribe, 'LM_INVVIDP', LM_INVVIDP );
               ParamAsFloat( FMovtoEscribe, 'LM_GUARDER', LM_GUARDER );
               ParamAsFloat( FMovtoEscribe, 'LM_RETIRO', LM_RETIRO );
               ParamAsFloat( FMovtoEscribe, 'LM_CES_VEJ', LM_CES_VEJ );
               ParamAsFloat( FMovtoEscribe, 'LM_CESVEJP', LM_CESVEJP );
               ParamAsFloat( FMovtoEscribe, 'LM_INF_PAT', LM_INF_PAT );
          end;
          Ejecuta( FMovtoEscribe );
     end;
     {$ifdef EVALUA_LIQ_IMSS}
     end;
     {$endif}
end;

procedure AgregaMovimiento( const sClave: String; const dLiqMovimiento: TDate );

procedure AgregaLiquidacionMovimiento;
const
     K_MENSUAL = 'M';
     K_BIMESTRAL = 'B';
begin
     iMovimientos := iMovimientos + 1;
     with FLiqMovimiento do
     begin
          LM_CLAVE := sClave;
          LM_FECHA := dLiqMovimiento;
          LM_KAR_FEC := dMovimiento;
          LM_KAR_TIP := sTipo;
          LM_BASE := rSalario;
          if ( FTipoPeriodo = tlmMensual ) then
             LM_TIPO := K_MENSUAL
          else
              LM_TIPO := K_BIMESTRAL;
          LM_DIAS := 0;
          LM_INCAPAC := 0;
          LM_AUSENCI := 0;
          LM_INF_AMO := 0;
          LM_EYM_ESP := 0;
          LM_EYMESPP := 0;
          LM_EYM_FIJ := 0;
          LM_EYM_EXC := 0;
          LM_EYMEXCP := 0;
          LM_EYM_DIN := 0;
          LM_EYMDINP := 0;
          LM_RIESGOS := 0;
          LM_INV_VID := 0;
          LM_INVVIDP := 0;
          LM_GUARDER := 0;
          LM_RETIRO := 0;
          LM_CES_VEJ := 0;
          LM_CESVEJP := 0;
          LM_INF_PAT := 0;
          LM_COTIZA := 0;
     end;
end;

begin  { AgregaMov }
     {  Calcula DIAS para movimiento anterior, }
     { excepto REINGRESOS pues es un hueco}
     if ( iMovimientos > 0 ) then
        CierraMovimiento( sClave, dLiqMovimiento );
     AgregaLiquidacionMovimiento;
end;

procedure CalculaModulo18;
var
   iDias, iTopeDias: Integer;
   rPercepciones: TPesos;
begin
     { Pendiente Evaluador de F�rmulas }
     if ZetaCommonTools.StrVacio( sFormulaDias ) then
        iDias := 0
     else
     begin
          { EvaluaFormula( sFormulaDias ); }
          iDias := 0;
     end;
     if ZetaCommonTools.StrVacio( sFormulaPercepciones ) then
        rPercepciones := 0
     else
     begin
          { EvaluaFormula( sFormulaPercepciones ); }
          rPercepciones := 0;
     end;
     if ( iDias > 0 ) and ( rPercepciones > 0 ) then
     begin
          iTopeDias := Trunc( {$ifdef EVALUA_LIQ_IMSS}FechaFinal{$else}FLiqFinal{$endif} - {$ifdef EVALUA_LIQ_IMSS}FechaInicial{$else}FLiqInicial{$endif} ) + 1;
          if ( iDias > iTopeDias ) then
             iDias := iTopeDias;
          if ( iDias = 0 ) then
             rSalario := 0
          else
              rSalario := ZetaCommonTools.Redondea( rPercepciones / iDias );
          dMovimiento := 0;
          sTipo := '';
          AgregaMovimiento( K_M_OK, {$ifdef EVALUA_LIQ_IMSS}FechaInicial{$else}FLiqInicial{$endif} );
          CierraMovimiento( K_M_OK, {$ifdef EVALUA_LIQ_IMSS}FechaInicial{$else}FLiqInicial{$endif} + iDias - 1 );
     end;
end;

begin    { CalculaLiquidacionEmpleado }
     SetFechasPeriodo( Tipo );
     FaltasInit;
     iMovimientos := 0;
     lAmortizado := False;
     {$ifdef EVALUA_LIQ_IMSS}
     if FEvaluandoIMSS then
     begin
          if FCreator.Rastreando then
          begin

               // DES US #13101 Infonavit - Descuento tipo # veces UMA...(Funciones)
               bIndicarUMA := FALSE;
               bIndicarFactorDescuento := FALSE;

               if FCreator.dmValUMA.GetFactorDescuento( FechaFinal ) <> 0 then
               begin
                    bIndicarFactorDescuento := TRUE;
               end
               else
               begin
                    if FEmpleado.PrestamoTipo = tiVecesUMA then
                       bIndicarUMA := TRUE
                    else if FEmpleado.PrestamoTipo = tiVeces then
                    begin
                         bIndicarUMA := (FCreator.dmValUMA.GetVUDefaultDiario( FechaFinal ) <> 0)
                                     and(FCreator.dmValUMA.GetVUDefaultDiario( FechaFinal ) < FCreator.dmSalMin.GetSMGDF( FechaFinal ));
                    end;
               end;

               with Rastreador do
               begin
                    RastreoTextoLibre( 'Fecha Inicial: ' + FormatDateTime( 'dd/mmm/yyyy', FechaInicial ) );
                    RastreoTextoLibre( 'Fecha Final  : ' + FormatDateTime( 'dd/mmm/yyyy', FechaFinal ) );

                    {Mejora descuento INFONAVIT en nomina AV Marzo/2011  Version 2011-B4 }
                    if ( UsarFactorNomInfo ) then
                       RastreoTextoLibre( Format( 'Factor diario N�mina:%9.2n', [FactorNomInfo] ) )
                    else
                        RastreoTextoLibre( Format( 'Valor diario INFONAVIT:%9.2n', [CalculaMontoVacaciones(1)] ) );

                    RastreoEspacio;
                    RastreoTextoLibre( Format( '%0:s Movimientos %0:s', [ StringOfChar( '-', 10 ) ] ) );
                    RastreoEspacio;

                    // DES US #13101 Infonavit - Descuento tipo # veces UMA...(Funciones)
                    if FEmpleado.PrestamoTipo in [tiCuotaFija,tiVeces,tiVecesUMA]  then
                    begin
                         if ( FEmpleado.PrestamoTipo = tiCuotaFija ) then
                         begin
                              RastreoTextoLibre(  '  Fecha   F.Kardex  Tipo Cuota Fija Bim. D�as  Inc. Aus. Cotiza  Total' )
                         end
                         else
                         begin
                              if bIndicarFactorDescuento then
                              begin
                                   RastreoTextoLibre( '  Fecha   F.Kardex  Tipo  #V.F.Desc   Bim. D�as  Inc. Aus. Cotiza  Total' )
                              end
                                 // DES US #13101 Infonavit - Descuento tipo # veces UMA...(Funciones)
                                 // else if ( FEmpleado.PrestamoTipo = tiVeces)  then
                                 else if ( FEmpleado.PrestamoTipo = tiVeces) and (not bIndicarUMA)then
                                     RastreoTextoLibre( '  Fecha   F.Kardex  Tipo  #VSMGDF   Bim. D�as  Inc. Aus. Cotiza  Total' )
                                 else
                                     RastreoTextoLibre( '  Fecha   F.Kardex  Tipo  #VUMA   Bim. D�as  Inc. Aus. Cotiza  Total' );
                         end;
                         RastreoTextoLibre( '--------- --------- ---- ---------- ----- ---- ---- ---- ------ -------' );
                    end
                    else
                    begin
                         RastreoTextoLibre( '  Fecha   F.Kardex  Tipo Sal. Base Dias Inc. Aus. Cotiza %Desc.    Total' );
                         RastreoTextoLibre( '--------- --------- ---- --------- ---- ---- ---- ------ -----  --------' );
                    end;
               end;
          end;
     end;
     {$endif}
     { Si es M�dulo 18 }
     if ( FEmpleado.ModuloPatron <> pmPermanente ) then
        CalculaModulo18
     else
     begin
          lHayBaja := False;
          lCerrar := True;
          {$ifdef EVALUA_LIQ_IMSS}
          if FEvaluandoIMSS then
          begin
               PreparaKardex( FechaInicial, FechaFinal );
          end
          else
          begin
          {$endif}
          PreparaKardex( FLiqInicial );
          {$ifdef EVALUA_LIQ_IMSS}
          end;
          {$endif}
          with FKardex do
          begin
               Active := True;
               { Si no hay movimientos en el mes, }
               { se va al registro inmediato anterior }
               if EOF then
               begin
                    GetKardexAnterior( dMovimiento, sTipo, rSalario {$ifdef DESCUENTO_SUA}, sZonaGeo {$endif} );
                    { Cuando se calcula el segundo mes de bimestre, puede ser }
                    { que en tlMensual no aparezca el empleado }
                    if ZetaCommonTools.StrVacio( sTipo ) or ( sTipo = K_T_BAJA ) then
                       lCerrar := False
                    else
                        AgregaMovimiento( K_M_OK, {$ifdef EVALUA_LIQ_IMSS}FechaInicial{$else}FLiqInicial{$endif} );
               end
               else { Revisa el primer movimiento }
               begin
                    sTipo := Trim( FieldByName( 'CB_TIPO' ).AsString );
                    dMovimiento := FieldByName( 'CB_FECHA' ).AsDateTime;
                    rSalario := Redondea( FieldByName( 'CB_SAL_INT' ).AsFloat );
{$ifdef DESCUENTO_SUA}
                    sZonaGeo := FieldByName( 'CB_ZONA_GE' ).AsString;
{$endif}
                    { Revisa el Primer Movimiento }
                    { Si es ALTA, inicia con Alta }
                    if ( sTipo = K_T_ALTA ) then
                    begin
                         AgregaMovimiento( K_M_ALTA, dMovimiento );
                         Next;
                    end
                    { Si el primer movimiento coincide con FechaInicial }
                    else
                    begin
                         dMovimiento := ZetaServerTools.dMax( dMovimiento, FieldByName( 'CB_FECHA_2' ).AsDateTime); { Modificado el 04/Dic/2001 Sug. x GA }
                         if ( dMovimiento = {$ifdef EVALUA_LIQ_IMSS}FechaInicial{$else}FLiqInicial{$endif} ) then
                         begin
                              { Si es CAMBIO, inicia con CAMBIO }
                              if ( sTipo = K_T_CAMBIO ) then
                              begin
                                   sClaveMov := K_M_MODIFICACION;
                                   Next;
                              end
                              else
                                  sClaveMov := K_M_OK;
                              AgregaMovimiento( sClaveMov, {$ifdef EVALUA_LIQ_IMSS}FechaInicial{$else}FLiqInicial{$endif} );
                         end
                         else   { El primer movimiento es dentro del mes }
                         begin
                              GetKardexAnterior( dMovimiento, sTipo, rSalario {$ifdef DESCUENTO_SUA}, sZonaGeo {$endif} );
                              AgregaMovimiento( K_M_OK, {$ifdef EVALUA_LIQ_IMSS}FechaInicial{$else}FLiqInicial{$endif} );
                         end;
                    end;
                    rSalarioAnterior := rSalario;
                    { Procesa el resto de los movimientos }
                    while not Eof do
                    begin
                         sTipo := Trim( FieldByName( 'CB_TIPO' ).AsString );
                         dMovimiento  := FieldByName( 'CB_FECHA' ).AsDateTime;
                         dMovimientoIMSS := ZetaServerTools.dMax( dMovimiento, FieldByName( 'CB_FECHA_2' ).AsDateTime); { Modificado el 13/Abr/1998 Sug. x Edgar }
                         { La fecha de Baja de RH no puede ser menor que la del IMSS }
                         rSalario := Redondea( FieldByName( 'CB_SAL_INT' ).AsFloat );
{$ifdef DESCUENTO_SUA}
                         sZonaGeo := FieldByName( 'CB_ZONA_GE' ).AsString;
{$endif}
                         if ( sTipo = K_T_BAJA ) then
                         begin
                              FEmpleado.Baja := dMovimientoIMSS;  { VL }
                              AgregaMovimiento( K_M_BAJA, dMovimientoIMSS );
                         end
                         else
                             if ( sTipo = K_T_CAMBIO ) and ( rSalario <> rSalarioAnterior ) then
                                AgregaMovimiento( K_M_MODIFICACION, dMovimientoIMSS )
                             else
                                 if ( sTipo = K_T_ALTA ) then { Reingreso }
                                    AgregaMovimiento( K_M_REINGRESO, dMovimiento );
                         rSalarioAnterior := rSalario;
                         Next;
                    end;
               end;
               Active := False;
               { Si el �ltimo movimiento no fu� baja, haz cierre hasta final }
               if lCerrar then
                  CierraMovimiento( K_M_OK, {$ifdef EVALUA_LIQ_IMSS}FechaFinal{$else}FLiqFinal{$endif} );
          end;
     end;
     {$ifdef EVALUA_LIQ_IMSS}
     if FEvaluandoIMSS then
     begin
          if FCreator.Rastreando then
          begin
               with Rastreador do
               begin
                    RastreoEspacio;
               end;
          end;
     end;
     {$endif}
end;

procedure AportacionVoluntaria;
begin
     FLiqEmpleado.LE_APO_VOL := GetAportacionVoluntaria( FEmpleado.Codigo );
end;

procedure EscribeLiquidacionEmpleado;
begin
     with FLiqEmpleado do
     begin
          LE_TOT_IMS := LE_EYM_FIJ +
                        LE_EYM_EXC +
                        LE_EYM_DIN +
                        LE_EYM_ESP +
                        LE_INV_VID +
                        LE_RIESGOS +
                        LE_GUARDER +  
                        // SOP 4527
                        // Sumar Aportaci�n voluntaria al total.
                        // Aportaciones voluntarias se calculan mensualmente.
                        LE_APO_VOL;
          if FEsBimestre then
          begin
               // SOP 4527
               // No sumar Aportaci�n voluntaria al total.
               // Aportaciones voluntarias se calculan mensualmente.
               // LE_TOT_RET := LE_RETIRO + LE_CES_VEJ + LE_APO_VOL;
               LE_TOT_RET := LE_RETIRO + LE_CES_VEJ;

               LE_TOT_INF := LE_INF_PAT + LE_INF_AMO;
          end
          else
          begin
               LE_TOT_RET := 0;
               LE_TOT_INF := 0;
          end;
          with oZetaProvider do
          begin
               ParamAsInteger( FEmpleadoModifica, 'CB_CODIGO', FEmpleado.Codigo );
               ParamAsFloat( FEmpleadoModifica, 'LE_IMSS_OB', LE_IMSS_OB );
               ParamAsFloat( FEmpleadoModifica, 'LE_IMSS_PA', LE_IMSS_PA );
               ParamAsFloat( FEmpleadoModifica, 'LE_EYM_FIJ', LE_EYM_FIJ );
               ParamAsFloat( FEmpleadoModifica, 'LE_EYM_EXC', LE_EYM_EXC );
               ParamAsFloat( FEmpleadoModifica, 'LE_EYM_DIN', LE_EYM_DIN );
               ParamAsFloat( FEmpleadoModifica, 'LE_EYM_ESP', LE_EYM_ESP );
               ParamAsFloat( FEmpleadoModifica, 'LE_RIESGOS', LE_RIESGOS );
               ParamAsFloat( FEmpleadoModifica, 'LE_INV_VID', LE_INV_VID );
               ParamAsFloat( FEmpleadoModifica, 'LE_GUARDER', LE_GUARDER );
               ParamAsInteger( FEmpleadoModifica, 'LE_DIAS_CO', LE_DIAS_CO );
               ParamAsFloat( FEmpleadoModifica, 'LE_RETIRO', LE_RETIRO );
               ParamAsFloat( FEmpleadoModifica, 'LE_CES_VEJ', LE_CES_VEJ );
               ParamAsFloat( FEmpleadoModifica, 'LE_INF_PAT', LE_INF_PAT );
               ParamAsFloat( FEmpleadoModifica, 'LE_INF_AMO', LE_INF_AMO );
               ParamAsInteger( FEmpleadoModifica, 'LE_DIAS_BM', LE_DIAS_BM );
               ParamAsFloat( FEmpleadoModifica, 'LE_APO_VOL', LE_APO_VOL );
               ParamAsFloat( FEmpleadoModifica, 'LE_TOT_IMS', LE_TOT_IMS );
               ParamAsFloat( FEmpleadoModifica, 'LE_TOT_RET', LE_TOT_RET );
               ParamAsFloat( FEmpleadoModifica, 'LE_TOT_INF', LE_TOT_INF );

               ParamAsFloat( FEmpleadoModifica, 'LE_ACUMULA', LE_ACUMULA );
               ParamAsFloat( FEmpleadoModifica, 'LE_BIM_ANT', LE_BIM_ANT );
               ParamAsFloat( FEmpleadoModifica, 'LE_BIM_SIG', LE_BIM_SIG );
               ParamAsFloat( FEmpleadoModifica, 'LE_VA_PAGO', LE_VA_PAGO );
               ParamAsFloat( FEmpleadoModifica, 'LE_VA_GOZO', LE_VA_GOZO );
               ParamAsFloat( FEmpleadoModifica, 'LE_PROV', LE_PROV );
               Ejecuta( FEmpleadoModifica );
          end;
     end;
end;

procedure AcumulaLiquidacion;
begin
     { Optimizaci�n: Hacer esto en un s�lo Query hasta el final }
     { UPDATE LIQ_IMSS .. SELECT SUM( ..) FROM LIQ_EMP }
     { GA: Dado que se usan variables locales, � para que optimizar ? }
     with FLiqIMSS do
     begin
          LS_NUM_TRA := LS_NUM_TRA + 1;
          with FLiqEmpleado do
          begin
               LS_DIAS_CO := LS_DIAS_CO + LE_DIAS_CO;
               LS_EYM_FIJ := LS_EYM_FIJ + LE_EYM_FIJ;
               LS_EYM_EXC := LS_EYM_EXC + LE_EYM_EXC;
               LS_EYM_DIN := LS_EYM_DIN + LE_EYM_DIN;
               LS_EYM_ESP := LS_EYM_ESP + LE_EYM_ESP;
               LS_RIESGOS := LS_RIESGOS + LE_RIESGOS;
               LS_INV_VID := LS_INV_VID + LE_INV_VID;
               LS_GUARDER := LS_GUARDER + LE_GUARDER;
               LS_IMSS_PA := LS_IMSS_PA + LE_IMSS_PA;
               LS_IMSS_OB := LS_IMSS_OB + LE_IMSS_OB;
               // SOP 4527
               LS_APO_VOL := LS_APO_VOL + LE_APO_VOL;
               
               if FEsBimestre then
               begin
                    LS_NUM_BIM := LS_NUM_BIM + 1;
                    LS_DIAS_BM := LS_DIAS_BM + LE_DIAS_BM;
                    LS_RETIRO := LS_RETIRO  + LE_RETIRO;
                    LS_CES_VEJ := LS_CES_VEJ + LE_CES_VEJ;
                    // SOP 4527
                    // LS_APO_VOL := LS_APO_VOL + LE_APO_VOL;
                    
                    if ( LE_INF_AMO > 0 ) then
                    begin
                         LS_INF_ACR := LS_INF_ACR + LE_INF_PAT;
                         LS_INF_AMO := LS_INF_AMO + LE_INF_AMO;
                         LS_INF_NUM := LS_INF_NUM + 1;
                    end
                    else
                        LS_INF_NAC := LS_INF_NAC + LE_INF_PAT;
               end;
          end;
     end;
end;


begin   { CalcularIMSSPagos }
     if {$ifdef EVALUA_LIQ_IMSS}FEvaluandoIMSS or {$endif}PuedeProcesar then
     begin
          dUltimo := NullDateTime;    { Fecha de Ultimo Movimiento de Mes }
          {$ifdef EVALUA_LIQ_IMSS}
          if FEvaluandoIMSS then
          begin
               InitLiquidacionEmpleado;
          end
          else
          begin
          {$endif}

          AgregaLiquidacionEmpleado;  { Crea Registro LIQ_EMP }
          {$ifdef EVALUA_LIQ_IMSS}
          end;
          {$endif}
          CalculaLiquidacionEmpleado( tlmMensual );
          {$ifdef EVALUA_LIQ_IMSS}
          if FEvaluandoIMSS then
          begin
          end
          else
          begin
          {$endif}

          // SOP 4527
          AportacionVoluntaria;

          if FEsBimestre then
          begin
               CalculaLiquidacionEmpleado( tlmBimestral );
               // SOP 4527
               // AportacionVoluntaria;
          end;
          EscribeLiquidacionEmpleado; { Escribe Registro LIQ_EMP }
          AcumulaLiquidacion;         { Acumula LIQ_EMP a LIQ_IMSS }
          {$ifdef EVALUA_LIQ_IMSS}
          end;
          {$endif}
     end;
end;

{$ifdef EVALUA_LIQ_IMSS}
procedure TIMSS.IMSSPagosEnd;
begin
     PreparaQuerysEnd;
end;

procedure TIMSS.EvaluarIMSSPagosEnd;
begin
     IMSSPagosEnd;
     FEvaluandoIMSS := False;
end;

procedure TIMSS.CalcularIMSSPagosEnd;
var
   FDataset: TZetaCursor;
begin
     with oZetaProvider do
     begin
          FDataset := CreateQuery( GetSQLScript( Q_LIQUIDACION_MODIFICA ) );
          try
             ParamAsChar( FDataset, 'Patron', FPatron, K_ANCHO_PATRON );
             ParamAsInteger( FDataset, 'Year', FYear );
             ParamAsInteger( FDataset, 'Mes', FMes );
             ParamAsInteger( FDataset, 'Tipo',Ord( FTipoIMSS ) );
             with FLiqIMSS do
             begin
                  { Subtotales }
                  LS_SUB_IMS := LS_EYM_FIJ +
                                LS_EYM_EXC +
                                LS_EYM_DIN +
                                LS_EYM_ESP +
                                LS_INV_VID +
                                LS_RIESGOS +
                                LS_GUARDER;
                  LS_TOT_IMS := LS_SUB_IMS + LS_ACT_IMS + LS_REC_IMS;
                  LS_SUB_RET := LS_RETIRO + LS_CES_VEJ;
                  LS_TOT_RET := LS_SUB_RET + LS_ACT_RET + LS_REC_RET + LS_APO_VOL;
                  LS_SUB_INF := LS_INF_NAC + LS_INF_ACR + LS_INF_AMO;
                  LS_TOT_INF := LS_SUB_INF + LS_ACT_INF + LS_REC_INF;
                  LS_TOT_MES := LS_TOT_IMS + LS_TOT_RET + LS_TOT_INF;
                  { Parametros del Query }
                  ParamAsInteger( FDataset, 'LS_NUM_TRA', LS_NUM_TRA );
                  ParamAsInteger( FDataset, 'LS_DIAS_CO', LS_DIAS_CO );
                  ParamAsFloat( FDataset, 'LS_EYM_DIN', LS_EYM_DIN );
                  ParamAsFloat( FDataset, 'LS_EYM_ESP', LS_EYM_ESP );
                  ParamAsFloat( FDataset, 'LS_EYM_EXC', LS_EYM_EXC );
                  ParamAsFloat( FDataset, 'LS_EYM_FIJ', LS_EYM_FIJ );
                  ParamAsFloat( FDataset, 'LS_RIESGOS', LS_RIESGOS );
                  ParamAsFloat( FDataset, 'LS_INV_VID', LS_INV_VID );
                  ParamAsFloat( FDataset, 'LS_GUARDER', LS_GUARDER );
                  ParamAsFloat( FDataset, 'LS_RETIRO', LS_RETIRO );
                  ParamAsFloat( FDataset, 'LS_CES_VEJ', LS_CES_VEJ );
                  ParamAsFloat( FDataset, 'LS_APO_VOL', LS_APO_VOL );
                  ParamAsFloat( FDataset, 'LS_INF_ACR', LS_INF_ACR );
                  ParamAsFloat( FDataset, 'LS_INF_AMO', LS_INF_AMO );
                  ParamAsFloat( FDataset, 'LS_INF_NAC', LS_INF_NAC );
                  ParamAsInteger( FDataset, 'LS_INF_NUM', LS_INF_NUM );
                  ParamAsFloat( FDataset, 'LS_ACT_IMS', LS_ACT_IMS );
                  ParamAsFloat( FDataset, 'LS_ACT_RET', LS_ACT_RET );
                  ParamAsFloat( FDataset, 'LS_ACT_INF', LS_ACT_INF );
                  ParamAsFloat( FDataset, 'LS_REC_IMS', LS_REC_IMS );
                  ParamAsFloat( FDataset, 'LS_REC_INF', LS_REC_INF );
                  ParamAsFloat( FDataset, 'LS_REC_RET', LS_REC_RET );
                  ParamAsInteger( FDataset, 'LS_DIAS_BM', LS_DIAS_BM );
                  ParamAsInteger( FDataset, 'LS_NUM_BIM', LS_NUM_BIM );
                  ParamAsFloat( FDataset, 'LS_SUB_IMS', LS_SUB_IMS );
                  ParamAsFloat( FDataset, 'LS_TOT_IMS', LS_TOT_IMS );
                  ParamAsFloat( FDataset, 'LS_TOT_MES', LS_TOT_MES );
                  ParamAsFloat( FDataset, 'LS_SUB_RET', LS_SUB_RET );
                  ParamAsFloat( FDataset, 'LS_TOT_RET', LS_TOT_RET );
                  ParamAsFloat( FDataset, 'LS_SUB_INF', LS_SUB_INF );
                  ParamAsFloat( FDataset, 'LS_TOT_INF', LS_TOT_INF );
                  ParamAsFloat( FDataset, 'LS_IMSS_PA', LS_IMSS_PA );
                  ParamAsFloat( FDataset, 'LS_IMSS_OB', LS_IMSS_OB );
                  ParamAsFloat( FDataset, 'LS_ACT_AMO', LS_ACT_AMO );
                  ParamAsFloat( FDataset, 'LS_ACT_APO', LS_ACT_APO );
                  ParamAsFloat( FDataset, 'LS_REC_AMO', LS_REC_AMO );
                  ParamAsFloat( FDataset, 'LS_REC_APO', LS_REC_APO );
             end;
             ParamAsInteger( FDataset, 'LS_STATUS', Ord( slCalculadaTotal ) );
             Ejecuta( FDataset );
          finally
                 FreeAndNil( FDataset );
          end;
     end;
     FreeAndNil( FMovtoEscribe );
     FreeAndNil( FEmpleadoAgrega );
     FreeAndNil( FEmpleadoModifica );
     IMSSPagosEnd;
end;
{$else}
procedure TIMSS.CalcularIMSSPagosEnd;
var
   FDataset: TZetaCursor;
begin
     with oZetaProvider do
     begin
          FDataset := CreateQuery( GetSQLScript( Q_LIQUIDACION_MODIFICA ) );
          try
             ParamAsChar( FDataset, 'Patron', FPatron, K_ANCHO_PATRON );
             ParamAsInteger( FDataset, 'Year', FYear );
             ParamAsInteger( FDataset, 'Mes', FMes );
             ParamAsInteger( FDataset, 'Tipo',Ord( FTipoIMSS ) );
             with FLiqIMSS do
             begin
                  { Subtotales }
                  LS_SUB_IMS := LS_EYM_FIJ +
                                LS_EYM_EXC +
                                LS_EYM_DIN +
                                LS_EYM_ESP +
                                LS_INV_VID +
                                LS_RIESGOS +
                                LS_GUARDER;
                  LS_TOT_IMS := LS_SUB_IMS + LS_ACT_IMS + LS_REC_IMS;
                  LS_SUB_RET := LS_RETIRO + LS_CES_VEJ;
                  LS_TOT_RET := LS_SUB_RET + LS_ACT_RET + LS_REC_RET + LS_APO_VOL;
                  LS_SUB_INF := LS_INF_NAC + LS_INF_ACR + LS_INF_AMO;
                  LS_TOT_INF := LS_SUB_INF + LS_ACT_INF + LS_REC_INF;
                  LS_TOT_MES := LS_TOT_IMS + LS_TOT_RET + LS_TOT_INF;
                  { Parametros del Query }
                  ParamAsInteger( FDataset, 'LS_NUM_TRA', LS_NUM_TRA );
                  ParamAsInteger( FDataset, 'LS_DIAS_CO', LS_DIAS_CO );
                  ParamAsFloat( FDataset, 'LS_EYM_DIN', LS_EYM_DIN );
                  ParamAsFloat( FDataset, 'LS_EYM_ESP', LS_EYM_ESP );
                  ParamAsFloat( FDataset, 'LS_EYM_EXC', LS_EYM_EXC );
                  ParamAsFloat( FDataset, 'LS_EYM_FIJ', LS_EYM_FIJ );
                  ParamAsFloat( FDataset, 'LS_RIESGOS', LS_RIESGOS );
                  ParamAsFloat( FDataset, 'LS_INV_VID', LS_INV_VID );
                  ParamAsFloat( FDataset, 'LS_GUARDER', LS_GUARDER );
                  ParamAsFloat( FDataset, 'LS_RETIRO', LS_RETIRO );
                  ParamAsFloat( FDataset, 'LS_CES_VEJ', LS_CES_VEJ );
                  ParamAsFloat( FDataset, 'LS_APO_VOL', LS_APO_VOL );
                  ParamAsFloat( FDataset, 'LS_INF_ACR', LS_INF_ACR );
                  ParamAsFloat( FDataset, 'LS_INF_AMO', LS_INF_AMO );
                  ParamAsFloat( FDataset, 'LS_INF_NAC', LS_INF_NAC );
                  ParamAsInteger( FDataset, 'LS_INF_NUM', LS_INF_NUM );
                  ParamAsFloat( FDataset, 'LS_ACT_IMS', LS_ACT_IMS );
                  ParamAsFloat( FDataset, 'LS_ACT_RET', LS_ACT_RET );
                  ParamAsFloat( FDataset, 'LS_ACT_INF', LS_ACT_INF );
                  ParamAsFloat( FDataset, 'LS_REC_IMS', LS_REC_IMS );
                  ParamAsFloat( FDataset, 'LS_REC_INF', LS_REC_INF );
                  ParamAsFloat( FDataset, 'LS_REC_RET', LS_REC_RET );
                  ParamAsInteger( FDataset, 'LS_DIAS_BM', LS_DIAS_BM );
                  ParamAsInteger( FDataset, 'LS_NUM_BIM', LS_NUM_BIM );
                  ParamAsFloat( FDataset, 'LS_SUB_IMS', LS_SUB_IMS );
                  ParamAsFloat( FDataset, 'LS_TOT_IMS', LS_TOT_IMS );
                  ParamAsFloat( FDataset, 'LS_TOT_MES', LS_TOT_MES );
                  ParamAsFloat( FDataset, 'LS_SUB_RET', LS_SUB_RET );
                  ParamAsFloat( FDataset, 'LS_TOT_RET', LS_TOT_RET );
                  ParamAsFloat( FDataset, 'LS_SUB_INF', LS_SUB_INF );
                  ParamAsFloat( FDataset, 'LS_TOT_INF', LS_TOT_INF );
                  ParamAsFloat( FDataset, 'LS_IMSS_PA', LS_IMSS_PA );
                  ParamAsFloat( FDataset, 'LS_IMSS_OB', LS_IMSS_OB );
                  ParamAsFloat( FDataset, 'LS_ACT_AMO', LS_ACT_AMO );
                  ParamAsFloat( FDataset, 'LS_ACT_APO', LS_ACT_APO );
                  ParamAsFloat( FDataset, 'LS_REC_AMO', LS_REC_AMO );
                  ParamAsFloat( FDataset, 'LS_REC_APO', LS_REC_APO );
             end;
             ParamAsInteger( FDataset, 'LS_STATUS', Ord( slCalculadaTotal ) );
             Ejecuta( FDataset );
          finally
                 FreeAndNil( FDataset );
          end;
     end;
     FreeAndNil( FMovtoEscribe );
     FreeAndNil( FEmpleadoAgrega );
     FreeAndNil( FEmpleadoModifica );
     PreparaQuerysEnd;
end;
{$endif}

{ ********* Revision de SUA *********** }

procedure TIMSS.RevisionSUABegin;
begin
     InitSalMin;
end;

procedure TIMSS.RevisionSUA( Dataset: TDataset );
var
   iEmpleado: TNumEmp;
   sSegSoc, sInfCred, sSignos, sPaterno, sPaternoSUA, sMaterno, sMaternoSUA, sGenerado, sRFC: String;

function StrZero( const sValor: String; const iAncho: Integer ): String;
begin
     Result := Trim( sValor );
     while ( Length( Result ) < iAncho ) do
     begin
          Result := '0' + Result ;
     end;
end;

function GetNombreRaro( sNombre: String; var sSignos: String ): Boolean;
var
   i: Integer;
   sLetra: String[ 1 ];
begin
     { � Acepta � ? � Acepta ' ' ? }
     sNombre := UpperCase( Trim( sNombre ) );
     sSignos := '';
     for  i := 1 to Length( sNombre ) do
     begin
          sLetra := Copy( sNombre, i, 1 );
          if not ( ( sLetra >= 'A' )  and ( sLetra <= 'Z' ) or ( sLetra = ' ' ) or ( sLetra = '�' ) ) then
             sSignos := sSignos + sLetra;
     end;
     Result := StrLleno( sSignos );
end;

function ValidaPrestamo( const iTipo: Integer; const rMonto,rInfOld: TPesos; var sSignos: String ): Boolean;
begin
     sSignos := '';
     case eTipoInfonavit( iTipo ) of
          tiPorcentaje: if ( rInfOld  = 0 ) and ( rMonto >= 100 ) then
                           sSignos := 'Tasa Fuera De Rango';
          tiCuotaFija: if ( ZetaCommonTools.MiRound( rMonto,4 ) <> ZetaCommonTools.MiRound( rMonto, 2 ) ) then
                          sSignos :='Muchos Decimales';
          tiVeces: if ( rMonto >= 1000 ) then
                      sSignos := '# SMGDF Fuera Rango';
          tiVecesUMA: if ( rMonto >= 1000 ) then
                      sSignos := '# UMA Fuera Rango';
     end;
     Result := ZetaCommonTools.StrVacio( sSignos );
end;

function GetDigitoRFC( const sRFC: String ): String;
var
   sCaracter: String[ 1 ];
   i, iCaracter, iValCaracter: Integer;
   rDigito: Real;
begin
     rDigito := 0;
     for i := 1 to 12 do
     begin
          sCaracter := Copy( sRFC, i , 1 );
          iCaracter := Ord( sCaracter[ 1 ] );
       	  if ( iCaracter < 65 ) then
          begin
               iValCaracter := StrToInt( sCaracter );
               if ( iValCaracter > 0 ) then
                  rDigito := rDigito + ( iValCaracter * ( 14 - i ) )
             else
                 if ( sCaracter = ' ' ) then
                    rDigito := rDigito + ( 37 * ( 14 - i ) )
                 else
                     rDigito := rDigito + ( 99 * ( 14 - i ) );
          end
          else
          begin
               if ( iCaracter > 78 ) then
                  iCaracter := iCaracter - 54
               else
                   iCaracter := iCaracter - 55;
               rDigito := rDigito + ( iCaracter * ( 14 - i ) );
          end;
      end;
      rDigito := 11 - ( Trunc( rDigito ) mod 11 );
      if ( rDigito = 10 ) then
         Result := 'A'
      else
          if ( rDigito = 11 ) then
             Result := '0'
          else
              STR( rDigito:1:0, Result );
end;

function ValidaRFC( sRFC: String; var sSignos: String ): Boolean;
begin
     { No valida que homoclave no venga o no sea longitud 13 }
     sSignos := '';
     sRFC := Trim( UpperCase( sRFC ) );
     if StrVacio( sRFC ) then
        sSignos := 'VACIO'
     else
         if ( Pos( ' ', sRFC ) > 0 ) then
            sSignos := 'Contiene Espacios'
         else
             if GetNombreRaro( Copy( sRFC, 1, 4 ), sSignos ) then
                sSignos := 'Contiene ' + sSignos
             else
                 if StrVacio( Copy( sRFC, 5, 6 ) ) then { EMPTY( CTOD( SUBSTR( cRFC, 9, 2 ) + "/" + SUBSTR( cRFC, 7, 2 ) + "/" + SUBSTR( cRFC, 5, 2 ) ) ) }
                    sSignos :='Fecha No Es V�lida'
                 else
                     if ( ( Length( sRFC ) = 13 ) and ( Copy( sRFC, 13, 1 ) <> GetDigitoRFC( sRFC ) ) ) then
                        sSignos := 'D�gito Verificador';
     Result := StrVacio( sSignos );
end;

procedure WarningConHeader( const sHeader, sMensaje: String );
begin
     oZetaProvider.Log.Error( iEmpleado, sHeader, sMensaje );
end;

procedure Warning( const sMensaje: String );
begin
     WarningConHeader( sMensaje, sMensaje );
end;

begin
     with Dataset do
     begin
          iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
          sSegSoc := FieldByName( 'CB_SEGSOC' ).AsString;
          sInfCred := Trim( FieldByName( 'CB_INFCRED' ).AsString );
          sPaterno := FieldByName( 'CB_APE_PAT' ).AsString;
          sMaterno := FieldByName( 'CB_APE_MAT' ).AsString;
          if ZetaCommonTools.StrVacio( sSegSoc ) then
             Warning( '# de Seguro Social VACIO' )
          else
              if ( Length( sSegSoc ) <> 11 ) then
                 Warning( '# de Seguro Social Incompleto' )
              else
                  if not ZetaCommonTools.ValidaDigito( sSegSoc ) then
                     Warning( '# de Seguro Social INCORRECTO');
          if ( FieldByName( 'CB_SAL_INT' ).AsFloat = 0  ) then
             Warning( 'Salario Integrado es CERO' )
          else
              if ( FieldByName( 'CB_ZONA_GE' ).AsString = '' ) then
                 Warning( 'Zona Geogr�fica Vac�a' )
              else
                  if ( FieldByName( 'CB_SAL_INT' ).AsFloat < ( K_FACTOR_INTEGRACION * FCreator.dmSalMin.GetSalMin( FieldByName( 'CB_FEC_INT' ).AsDateTime, FieldByName( 'CB_ZONA_GE' ).AsString ) ) ) then
                     Warning( 'Salario Integrado Menor a MINIMO' );
          if ( FieldByName( 'CB_INFTASA' ).AsFloat > 0  ) then
          begin
               if ZetaCommonTools.StrVacio ( sInfCred ) then
                  Warning( '# de Cr�dito INFONAVIT VACIO')
               else
                   if ( Length( sInfCred ) <> 10 ) then
                      Warning( '# de Cr�dito INFONAVIT INCOMPLETO')
                   else
                       if not ZetaCommonTools.ValidaCredito( sInfCred, sSignos ) then
                          Warning( '# de Cr�dito INFONAVIT: '+ sSignos )
                       else
                           if not ValidaPrestamo( FieldByName( 'CB_INFTIPO' ).AsInteger,
                                                  FieldByName( 'CB_INFTASA' ).AsFloat,
                                                  FieldByName( 'CB_INF_OLD' ).AsFloat,
                                                  sSignos ) then
		                            Warning( 'Pr�stamo INFONAVIT: ' + sSignos );
          end;
          sPaternoSUA := GetNombreSUA( sPaterno );
          sMaternoSUA := GetNombreSUA( sMaterno );
          if ( ZetaCommonTools.StrVacio( sPaternoSUA ) and not ZetaCommonTools.StrVacio( sMaternoSUA ) ) then
	            Warning( 'Necesario Cambiar Apellido Paterno por Materno' );
          if ZetaCommonTools.StrVacio( sPaternoSUA ) then
             Warning( 'Apellido PATERNO VACIO');
          if ( not ZetaCommonTools.StrVacio( sMaterno ) and ZetaCommonTools.StrVacio( Copy( sMaternoSUA, 1, 1 ) ) ) then
             Warning( 'Apellido MATERNO Empieza c/Espacio' );
          { La segunda parte de la condicion se cumple solo si es minusculas, }
          { de otra manera algoritmo de GetNombreSUA hace Trim..... }
          if ZetaCommonTools.StrVacio( FieldByName( 'CB_NOMBRES' ).AsString ) then
             Warning( 'Nombre(s) VACIO' );
          if GetNombreRaro( sPaterno, sSignos ) then
             Warning( 'Apellido PATERNO con Signos: ' + sSignos );
          if GetNombreRaro( sMaterno, sSignos ) then
             Warning( 'Apellido MATERNO con Signos: ' + sSignos );
          if GetNombreRaro( FieldByName( 'CB_NOMBRES' ).AsString, sSignos ) then
             Warning( 'Nombre(s) con Signos: ' + sSignos );
          if ValidaRFC( FieldByName( 'CB_RFC' ).AsString, sSignos ) then
          begin
               sRFC := FieldByName( 'CB_RFC' ).AsString;
               sGenerado := ZetaCommonTools.CalcRFC( FieldByName( 'CB_APE_PAT' ).AsString,
                                                     FieldByName( 'CB_APE_MAT' ).AsString,
                                                     FieldByName( 'CB_NOMBRES' ).AsString,
                                                     FieldByName( 'CB_FEC_NAC' ).ASDateTime );
               if ( sRFC <> sGenerado ) then
                  WarningConHeader( 'RFC Almacenado Es Diferente Del Generado',
                                    Format( 'RFC Almacenado: %s' + CR_LF + 'RFC Generado  : %s', [ sRFC, sGenerado ] ) );
          end
          else
              Warning( 'RFC con Signos: ' + sSignos );
          if not ZetaCommonTools.StrVacio( FieldByName( 'CB_CURP' ).AsString ) and
             not ValidaCURP( FieldByName( 'CB_CURP' ).AsString, sSignos ) then
             Warning( 'CURP con Signos: ' + sSignos );
          if ( FieldByName('CB_FEC_BSS').AsDateTime > FieldByName('CB_FEC_ING').AsDateTime ) and zStrToBool( FieldByName('CB_ACTIVO').AsString ) then
          begin
               Warning( Format( 'Empleado Tiene Fecha De Baja En Seguro Social (''%s'') Posterior Al Ingreso  (''%s'') ', [ FechaCorta( FieldByName('CB_FEC_BSS').AsDateTime ), FechaCorta( FieldByName('CB_FEC_ING').AsDateTime ) ] ) );
          end;
     end;
end;

procedure TIMSS.RevisionSUAEnd;
begin
end;

{ ********* Exportaci�n de SUA *********** }

procedure TIMSS.ExportarSUABegin( const sPatron: String; const iYear, iMes: Integer; const lHomoClave, lEnviarFaltaBaja, lCuotaFija, lValidarCURPvsRFC, lBorrarEmpleados, lSuspenFinBim: Boolean; const BorrarMovimientos: eBorrarMovimSUA; Empleados, Movimientos, Incapacidades,Infonavit: TServerDataset);
begin
     {$ifdef EVALUA_LIQ_IMSS}
     SetGlobalPatron( sPatron );
     SetGlobales( iYear, iMes );
     {$else}
     SetGlobales( sPatron, iYear, iMes );
     {$endif}
     if FEsBimestre then
        SetFechasPeriodo( tlmBimestral )
     else
         SetFechasPeriodo( tlmMensual );
     PreparaQuerysBegin;

     FEnviarSUA := True;
     FMovtosFechaBaja := lEnviarFaltaBaja;
     FConHomoclave := lHomoclave;
     FCuotaFija := lCuotaFija;
     FValidarCURPvsRFC := lValidarCURPvsRFC;
     FBorrarEmpleados := lBorrarEmpleados;
     FBorrarMovimientos := BorrarMovimientos;
     FEnviaSuspenFinBim:= lSuspenFinBim;

     with oZetaProvider do
     begin
          InitGlobales;
          FIncluirFI:= GetGlobalBooleano( K_GLOBAL_INCLUYE_FI_SUA );
     end;

     with Empleados do
     begin
          InitTempDataset;
          AddStringField( 'SUA_E_PAT', 11 );  { RegistroPatronal }
          AddStringField( 'SUA_E_SS', 11 );   { NumeroSeguro }
          AddStringField( 'SUA_E_RFC', 13 );  { Registro Federal de Causantes }
          AddStringField( 'SUA_E_CUR', 18 );  { # CURP }
          AddStringField( 'SUA_E_NOMB', 50 ); { Nombre del Asegurado }
          AddStringField( 'SUA_E_ID', 17 );   { Indentificaci�n }
          AddStringField( 'SUA_E_CRED', 10 ); { # Cr�dito INFONAVIT }
          AddIntegerField( 'SUA_E_MODP' );    { M�dulo Patr�n }
          AddIntegerField( 'SUA_E_JORN' );    { Tipo Jornada }  {TENTATIVAMENTE NECESITA UN MENOS 1 PARA CUMPLIR CON EL STD DE IMSS }
          AddIntegerField( 'SUA_E_TIPO' );    { Tipo Pr�stamo INFONAVIT }
          AddDateTimeField( 'SUA_E_INGR' );   { Fecha Ingreso }
          AddDateTimeField( 'SUA_E_FE_P' );   { Fecha Pr�stamo INFONAVIT }
          AddFloatField( 'SUA_E_WAGE' );      { Salario SUA }
          AddFloatField( 'SUA_E_LOAN' );      { Monto Pr�stamo INFONAVIT }
          //Datos afiliatorios
          AddStringField( 'SUA_E_CODP',5 );   { C�digo Postal }
          AddDateTimeField( 'SUA_E_FE_N' );   { Fecha de Nacimiento }
          AddStringField( 'SUA_E_LNAC', 25 ); { Lugar de Nacimiento }
          AddStringField( 'SUA_E_CNAC',2 );   { Clave de Lugar de Nacimiento }
          AddStringField( 'SUA_E_CLIN',3 );   { Cl�nica [Unidad de medicina Familiar UMF] }
          AddStringField( 'SUA_E_OCUP',12 );  { Ocupaci�n 'Empleado'}
          AddStringField( 'SUA_E_SEXO', 1 );  { Sexo [M � F] }
          AddIntegerField( 'SUA_E_T_SA' );    { Tipo de salario }
          AddStringField( 'SUA_E_HORA',1 );   { Hora  }

          CreateTempDataset;
     end;
     with Movimientos do
     begin
          InitTempDataset;
          AddStringField( 'SUA_M_PAT', 11 ); { RegistroPatronal }
          AddStringField( 'SUA_M_SS', 11 );  { NumeroSeguro }
          AddStringField( 'SUA_M_TIPO', 2 ); { Tipo }
          AddStringField( 'SUA_M_INCA', 8 ); { Incapacidad }
          AddDateTimeField( 'SUA_M_DATE' )   { Fecha };
          AddFloatField( 'SUA_M_DIAS' );     { Dias };
          AddFloatField( 'SUA_M_MONT' );     { Monto };
          AddStringField( 'SUA_M_ORD', 1 );  { Para indicar orden, no se debe exportar al ASCII }
          CreateTempDataset;
          LogChanges := False;
          IndexFieldNames := 'SUA_M_PAT;SUA_M_SS;SUA_M_DATE;SUA_M_ORD';
     end;
     with Incapacidades do
     begin
          InitTempDataset;
          AddStringField( 'SUA_I_PAT', 11 ); { RegistroPatronal }
          AddStringField( 'SUA_I_SS', 11 );  { NumeroSeguro }
          AddStringField( 'SUA_I_TIPO', 1 ); { Tipo }
          AddDateTimeField( 'SUA_I_FINI');   { Fecha de Inicio }
          AddStringField( 'SUA_I_FOLIO',8 )  { Folio };
          AddIntegerField( 'SUA_I_DIAS' );   { Dias };
          AddFloatField( 'SUA_I_INCA' );     { % Incapacidad si no tiene 0 };
          AddIntegerField( 'SUA_I_RAMA');    { Rama de incapacidad }
          AddIntegerField( 'SUA_I_RSGO');    { Tipo de Riesgo }
          AddIntegerField( 'SUA_I_SCLA');    { Secuela o consecuencia }
          AddIntegerField( 'SUA_I_CTIN');    { Control de incapacidad [Motivo] }
          AddDateTimeField( 'SUA_I_FFIN');   { Fecha Final }

          CreateTempDataset;
     end;

     with Infonavit do
     begin
          InitTempDataset;
          AddStringField( 'SUA_F_PAT', 11 ); { RegistroPatronal }
          AddStringField( 'SUA_F_SS', 11 );  { NumeroSeguro }
          AddStringField( 'SUA_F_CRED',10 );   { # Credito Infonavit }
          AddStringField( 'SUA_F_TMOV',2);    { Tipo de Movimiento }
          AddDateTimeField( 'SUA_F_FMOV' )   { Fecha de Movimiento };
          AddIntegerField( 'SUA_F_TDES' );   { Tipo de Descuento };
          AddFloatField( 'SUA_F_VDES' );     { Valor de Descuento };
          AddStringField( 'SUA_F_DISM',1);   { Aplica Dism % }

          CreateTempDataset;
     end;
     FSUADataset := Empleados;
     FSUAMovtos := Movimientos;
     FSUAIncapaci := Incapacidades;
     FSUAInfonavit := Infonavit;
end;

procedure TIMSS.EscribeMovtoSUA( const sTipo, sIncapacidad: String; const dMovimiento: TDate; const rDias, rMonto: TPesos );
begin
     with FSUAMovtos do
     begin
          try
             Append;
             with FEmpleado do
             begin
                  FieldByName( 'SUA_M_PAT' ).AsString := RegistroPatronal;
                  FieldByName( 'SUA_M_SS' ).AsString := NumeroSeguro;
             end;
             FieldByName( 'SUA_M_TIPO' ).AsString := sTipo;
             FieldByName( 'SUA_M_INCA' ).AsString := sIncapacidad;
             FieldByName( 'SUA_M_DATE' ).AsDateTime := dMovimiento;
             FieldByName( 'SUA_M_DIAS' ).AsFloat := rDias;
             FieldByName( 'SUA_M_MONT' ).AsFloat := rMonto;
             FieldByName( 'SUA_M_ORD' ).AsString := GetSUAOrden( sTipo );
             Post;
          except
                on Error: Exception do
                begin
                     Cancel;
                end;
          end;
     end;
end;

procedure TIMSS.EscribeIncapaSUA( const dIN_SUA_INI,dIN_SUA_FIN:TDate;const iDias:Integer;const TipoIncapacidad: eIncidenciaIncapacidad;
                                  const fiSecuela : eFinIncapacidad;const miControl:eMotivoIncapacidad;
                                  const fTasa:TTasa;const sFolio,sIN_TIPO:string
                                  );

const
     K_TIPO_INCIDENCIA = 0;
     K_ACCIDENTE_TRBJO = 'IAC';
     K_NO_INC = 0;
     K_ENFERM = 1;
     K_MATERN = 2;
     K_ACC_TR = 3;
     K_ACC_TY = 4;
     K_ENF_TR = 5;

  //Secuela o consecuencia
     K_NINGUNA    = 0;
     K_TEMPORAL   = 1;
     K_VALUA_PROV = 2;
     K_VALUA_DEF  = 3;
     K_DEFUNCION  = 4;
     K_RECAIDA    = 5;
     K_VALUA_POS  = 6;
     K_REVALUA_PROV  = 7;
     K_REVALUA_DEF   = 8;

   //control de incapacidad
     K_UNICA       = 1;
     K_INICIAL     = 2;
     K_SUBSECUENTE = 3;
     K_ALTA_MEDICA = 4;
     K_VALUACION   = 5;
     K_DEFUNCION_I = 6;
     K_PRENATAL    = 7;
     K_ENLACE      = 8;
     K_POSTNATAL   = 9;

function GetRamaSUA( const TipoIncapaci: eIncidenciaIncapacidad ) : Integer;
begin
     case TipoIncapaci of
          inGeneral: Result := 2;
          inMaternidad : Result := 3;
          inAccidenteTrabajo..inEnfermedadTrabajo : Result := 1;
     else
         Result := 0;   // inNoIncapacidad
     end
end;

function GetRiesgoSUA( const TipoIncapaci: eIncidenciaIncapacidad ) : Integer;
begin
     case TipoIncapaci of
          inAccidenteTrabajo: Result := 1;
          inAccidenteTrayecto : Result := 2;
          inEnfermedadTrabajo : Result := 3;
     else
         Result := 0;   // No es riesgo de trabajo
     end
end;

function GetSecuelaSUA( const fiSecuela: eFinIncapacidad ) : Integer;
begin
     case fiSecuela of
          fiAltaMedica      : Result := K_NINGUNA;
          fiNoTermina       : Result := K_TEMPORAL;
          fiPermanente      : Result := K_VALUA_POS;
          fiMuerte          : Result := K_DEFUNCION;
          fiRecaida         : Result := K_RECAIDA;
          fiValuacionProv   : Result := K_VALUA_PROV;
          fiRevaluacionProv : Result := K_REVALUA_PROV;
          fiRevaluacionDef  : Result := K_REVALUA_DEF;
         
     else
         Result := K_NINGUNA;
     end
end;

function GetControlSUA( const miControl: eMotivoIncapacidad ) : Integer;
begin
     case miControl of
          miInicial      : Result := K_INICIAL;
          miSubsecuente  : Result := K_SUBSECUENTE;
          miRecaida      : Result := K_NINGUNA;
          miNinguno      : Result := K_NINGUNA;
          miUnica        : Result := K_UNICA;
          miAltaMedica   : Result := K_ALTA_MEDICA;
          miValuacion    : Result := K_VALUACION;
          miDefuncion    : Result := K_DEFUNCION_I;
          miPrenatal     : Result := K_PRENATAL;
          miPostnatal    : Result := K_POSTNATAL;
          miEnlace       : Result := K_ENLACE;
     else
         Result := K_NINGUNA;
     end
end;

begin
     with FSUAIncapaci do
     begin
          try
             Append;
             with FEmpleado do
             begin
                  FieldByName( 'SUA_I_PAT' ).AsString := RegistroPatronal;
                  FieldByName( 'SUA_I_SS' ).AsString := NumeroSeguro;
             end;
             FieldByName( 'SUA_I_TIPO').AsInteger := K_TIPO_INCIDENCIA;
             FieldByName( 'SUA_I_FINI').AsDateTime := dIN_SUA_INI;
             FieldByName( 'SUA_I_FOLIO' ).AsString :=  sFolio;
             FieldByName( 'SUA_I_DIAS' ).AsInteger := iDias;
             if ( sIN_TIPO = K_ACCIDENTE_TRBJO )then
             begin
                  FieldByName( 'SUA_I_INCA' ).AsFloat := fTasa;
             end
             else FieldByName( 'SUA_I_INCA' ).AsFloat := 0;

             FieldByName( 'SUA_I_RAMA').AsInteger := GetRamaSUA(TipoIncapacidad);
             FieldByName( 'SUA_I_RSGO').AsInteger := GetRiesgoSUA(TipoIncapacidad);
             FieldByName( 'SUA_I_SCLA').AsInteger := GetSecuelaSUA(fiSecuela);
             FieldByName( 'SUA_I_CTIN').AsInteger := GetControlSUA(miControl);
             FieldByName( 'SUA_I_FFIN').AsDateTime := dIN_SUA_FIN;
             Post;
          except
                on Error: Exception do
                begin
                     Cancel;
                end;
          end;
     end;
end;

procedure TIMSS.ExportarSUA( const lEmpleados, lMovimientos: Boolean );
var
   dEmpInicial,dEmpFinal: TDate;
   iFaltas, iIncapacidades: Integer;
   lHuboBaja, lExportaIngreso, lSinCreditoAlReingreso: Boolean;

procedure ExportaMovimientos;
var
   sTipo: String;
   dFecha, dFechaFaltas, dFechaKardex: TDate;

   procedure OutKardex( const sTipo: String; const dMovimiento: TDate; const rMonto: TPesos );
   begin
        EscribeMovtoSUA( sTipo, '', dMovimiento, 0, rMonto );
   end;

   procedure RevisaConflictoJornadaReducida;
   const
        K_MSG_AUSENTISMO_SUA = '%s - %d d�a(s)';
   var
      dFalta, dFaltaInicial, {dFaltaInicio,} dFaltaFinal: TDate;
      iFaltas, iFaltasSUA, iFaltasAC, iFaltasCal: Integer;
      lConflicto: Boolean;
      sMensaje: String;
   begin            //          IndexFieldNames := 'SUA_M_PAT;SUA_M_SS;SUA_M_DATE;SUA_M_ORD';
        with FSUAMovtos do
        begin
             if Locate( 'SUA_M_PAT;SUA_M_SS', VarArrayOf( [ FEmpleado.RegistroPatronal, FEmpleado.NumeroSeguro ] ), [] ) then
             begin
                  lConflicto := FALSE;
                  iFaltasCal := 0;
                  dFaltaInicial := NullDateTime;
                  dFaltaFinal := NullDateTime;
                  while ( not EOF ) and ( FieldByName( 'SUA_M_PAT' ).AsString = FEmpleado.RegistroPatronal ) and
                        ( FieldByName( 'SUA_M_SS' ).AsString = FEmpleado.NumeroSeguro ) do
                  begin
                       if ( FieldByName( 'SUA_M_TIPO' ).AsString = K_TIPO_SUA_AUSENTISMO ) then
                       begin
                            dFalta := FieldByName( 'SUA_M_DATE' ).AsDateTime;
                            iFaltas := FieldByName( 'SUA_M_DIAS' ).AsInteger;

                            if ( dFaltaInicial = NullDateTime ) then
                               dFaltaInicial := dFalta;                // Asigna fecha de primera falta por si se requiere enviar todas las faltas juntas

                            if ( iFaltasCal > 0 ) then                                    // Ya hay faltas reportadas por registros anteriores
                               iFaltasAC := GetAusentismosJornadaReducida( FEmpleado.TipoJornada, iFaltasCal ); // Faltas de SUA antes de agregar nueva falta

                            iFaltasCal := ( iFaltasCal + iFaltas );
                            lConflicto := lConflicto or ( dFalta <= dFaltaFinal ); // Tiene conflicto con fin de ausentismo anterior

                            if ( not lConflicto ) then
                            begin
                                 iFaltasSUA := GetAusentismosJornadaReducida( FEmpleado.TipoJornada, iFaltasCal ) - iFaltasAC;  // Se obtienen las faltas de SUA restando las consideradas anteriormente
                                 dFaltaFinal := ( dFalta + iFaltasSUA - 1 );
                                 lConflicto := lConflicto or ( dFaltaFinal > dEmpFinal );   // Tiene conflicto con fin de mes
                            end;
                       end;
                       Next;
                  end;
                  if lConflicto then      //Hubo conflicto en faltas se edita el registro de la primer falta
                  begin
                       if Locate( 'SUA_M_PAT;SUA_M_SS;SUA_M_TIPO;SUA_M_DATE', VarArrayOf( [ FEmpleado.RegistroPatronal,
                                  FEmpleado.NumeroSeguro, K_TIPO_SUA_AUSENTISMO, dFaltaInicial ] ), [] ) then
                       begin
                            sMensaje := Format( K_MSG_AUSENTISMO_SUA, [ ZetaCommonTools.FechaCorta( dFaltaInicial ), FieldByName( 'SUA_M_DIAS' ).AsInteger ] ) + CR_LF;
                            try
                               Edit;
                               iFaltasSUA := GetAusentismosJornadaReducida( FEmpleado.TipoJornada, iFaltasCal );   // Vuelve a revisar el valor de Faltas para SUA para revisar si se desfasa al sig. mes
                               dFaltaFinal := ( dFaltaInicial + iFaltasSUA - 1 );
                               if ( dFaltaFinal > dEmpFinal ) then     // Se ajusta fecha de la falta para que no se desfase a sig. mes
                               begin
                                    dFaltaInicial := dFaltaInicial - ( dFaltaFinal - dEmpFinal );
                                    FieldByName( 'SUA_M_DATE' ).AsDateTime := dFaltaInicial;
                               end;
                               FieldByName( 'SUA_M_DIAS' ).AsFloat := iFaltasCal;
                               Post;
                               sMensaje := 'El empleado tiene Jornada: ' + ZetaCommonLists.ObtieneElemento( lfTipoJornada, Ord( FEmpleado.TipoJornada ) ) + CR_LF +
                                           'Se envian todos los ausentismos en una sola fecha:' + CR_LF +
                                           Format( K_MSG_AUSENTISMO_SUA, [ ZetaCommonTools.FechaCorta( dFaltaInicial ), iFaltasCal ] ) + CR_LF +
                                           '=================================' + CR_LF +
                                           'Registros originales:' + CR_LF + sMensaje;
                               Next;
                               // barre el resto de los movimientos para borrar los ausentismos que se enviar�n con la fecha inicial
                               while ( not EOF ) and ( FieldByName( 'SUA_M_PAT' ).AsString = FEmpleado.RegistroPatronal ) and
                                     ( FieldByName( 'SUA_M_SS' ).AsString = FEmpleado.NumeroSeguro ) do
                               begin
                                    if ( FieldByName( 'SUA_M_TIPO' ).AsString = K_TIPO_SUA_AUSENTISMO ) then
                                    begin
                                         sMensaje := sMensaje + Format( K_MSG_AUSENTISMO_SUA, [ ZetaCommonTools.FechaCorta( FieldByName( 'SUA_M_DATE' ).AsDateTime ), FieldByName( 'SUA_M_DIAS' ).AsInteger ] ) + CR_LF;
                                         Delete;
                                    end
                                    else
                                        Next;     // Brincar el registro
                               end;
                               MergeChangeLog;
                               oZetaProvider.Log.Advertencia( FEmpleado.Codigo, 'Conflicto en ausentismos de Jornada reducida', sMensaje );
                            except
                                  on Error: Exception do
                                  begin
                                       CancelUpdates;
                                       oZetaProvider.Log.Error( FEmpleado.Codigo, 'Conflicto en ausentismos de Jornada reducida',
                                                                'No se pudo realizar el ajuste para enviar los ausentismos en una sola fecha.' + CR_LF +
                                                                'Favor de verificar manualmente los ausentismos del empleado ya que se pueden' + CR_LF +
                                                                'tener errores al intentar subir los movimientos a SUA.'  );
                                  end;
                            end;
                       end;
                  end;
             end;
        end;
   end;

   procedure AportacionVoluntaria;
   var
      rAportacion: TPesos;
   begin
        rAportacion := GetAportacionVoluntaria( FEmpleado.Codigo );
        if ( rAportacion > 0 ) then
   {$ifdef ANTES}
           EscribeMovtoSUA( K_TIPO_SUA_VOLUNTARIA, '', FLiqInicial, 0, rAportacion );
   {$else}
           EscribeMovtoSUA( K_TIPO_SUA_VOLUNTARIA, '', FBimMedio, 0, rAportacion );
   {$endif}
   end;

begin    { ExportaMovimientos }
     { Supone que el empleado cotiz� todo el mes, a menos que se }
     { encuentre un movimiento de baja que diga lo contrario }
     if FEsBimestre and ( FBorrarMovimientos <> bsTodo ) then    // Si es mes par y no se borra todo de SUA, solo deben enviarse movimientos del mes en curso
     begin
          dFechaKardex := FBimMedio;
          dFechaFaltas := ZetaServerTools.dMax( dEmpInicial, FBimMedio );
     end
     else
     begin
          dFechaKardex := FLiqInicial;
          dFechaFaltas := dEmpInicial;
     end;
     dEmpFinal := FLiqFinal;
     lHuboBaja := False;
     FaltasInit;
     PreparaKardex( dFechaKardex );
     with FKardex do
     begin
          Active := True;
          while not Eof do
          begin
               sTipo := Trim( FieldByName( 'CB_TIPO' ).AsString );
               if ( sTipo = K_T_ALTA ) and ( ZetaCommonTools.zStrToBool( FieldByName( 'CB_REINGRE' ).AsString ) ) and
                  ( lHuboBaja or ( not FBorrarEmpleados ) ) then
               begin                        { Supone 'sar_zap_sua' = TRUE }
                    dEmpFinal := FLiqFinal; { De nuevo, supone hasta el final }
                    OutKardex( K_TIPO_SUA_REINGRESO,
                               FieldByName( 'CB_FECHA' ).AsDateTime,
                               Redondea( FieldByName( 'CB_SAL_INT' ).AsFloat ) );
               end
               else
               if ( sTipo = K_T_CAMBIO ) then
               begin
                    dFecha := FieldByName( 'CB_FECHA_2' ).AsDateTime;
                    if not lHuboBaja or ( dFecha <> FEmpleado.Baja ) or FMovtosFechaBaja then
                    begin
                         OutKardex( K_TIPO_SUA_CAMBIO,
                                    dFecha,
                                    Redondea( FieldByName( 'CB_SAL_INT' ).AsFloat ) );
                         if lHuboBaja and ( dFecha = FEmpleado.Baja ) then
                            oZetaProvider.Log.Advertencia( FEmpleado.Codigo, 'Mod. Sal. en Fecha de BAJA ( ' + FechaCorta( FEmpleado.Baja ) + ' )' );
                    end;
               end
               else
               if ( sTipo = K_T_BAJA ) then
               begin
                    FEmpleado.Baja := FieldByName( 'CB_FECHA_2' ).AsDateTime;
                    dEmpFinal := FEmpleado.Baja;
                    OutKardex( K_TIPO_SUA_BAJA,
                               FEmpleado.Baja,
                               0 );
                    lHuboBaja := True;
               end;
               Next;
          end;
          Active := False;
     end;
     { Faltas e Incapacidades }
     { Se llama una sola vez por el l�mite mensual de 14 faltas }
     { Est� limitado a un s�lo rango de fechas, por lo que no }
     { funciona para empleados que tuvieron PAT1-PAT2-PAT1 en el mismo mes }
     RevisaFaltas( dFechaFaltas, dEmpFinal, iFaltas, iIncapacidades );
     if ( FEmpleado.TipoJornada in [tjReducida_1,tjReducida_2,tjReducida_3,tjReducida_4,tjReducida_5 ]  )  then   // Revisa si las faltas tienen conflicto con faltas subsecuentes del mismo mes o meses siguientes
        RevisaConflictoJornadaReducida;
     AportacionVoluntaria;
end;

procedure CalculaDatosInfonavit(var dPrestamo:TDate);
var
   dBajaIMSS,dEmpFinal:TDate;
   iFaltas,iIncapacidades: Integer;
begin
     with FEmpleado do
     begin
          PrestamoInicio := ZetaServerTools.dMax( PrestamoInicio, Ingreso );
          // Por Bug en SUA, se env�a como CuotaFija
          if FCuotaFija and ( PrestamoTipo = tiPorcentaje ) and ( TasaAnterior > 0 ) then
          begin
               dBajaIMSS := FechaBSS;
               if ( dBajaIMSS < Ingreso ) then
               dEmpFinal := FLiqFinal
               else
                   dEmpFinal := rMin( dBajaIMSS, FLiqFinal );
               PrestamoInicio := rMax( PrestamoInicio, FBimInicio );
               FEnviarSUA := FALSE;           // Para que no exporte la revisi�n sobre Faltas, esto lo har� ExportaMovimientos
               RevisaFaltas( PrestamoInicio, dEmpFinal, iFaltas, iIncapacidades );
               FEnviarSUA := TRUE;
               Prestamo := Redondea( GetSalarioSua( FechaSalarioIntegrado, FBimInicio ) *
                           ( Prestamo / 100 ) * rMax( dEmpFinal - PrestamoInicio + 1 - iFaltas - iIncapacidades, 0 ) );
               if ( FEsBimestre ) then
               begin
                    if ( TheMonth( PrestamoInicio ) <> TheMonth( dEmpFinal ) ) then // Cuando El Empleado Cotiz� los dos Meses //
                       Prestamo := Prestamo / 2.0;
               end;
               PrestamoTipo := tiCuotaFija;
               PrestamoInicio := FBimInicio;
          end;
          if ( PrestamoTipo = tiPorcentaje ) then
             dPrestamo := ZetaServerTools.dMax( PrestamoInicio, EncodeDate( 1997, 04, 30 ) ) { StrToDate( '30/04/1997' ) }
          else
          begin
               dPrestamo := ZetaServerTools.dMax( PrestamoInicio, EncodeDate( 1997, 05, 01 ) ); { StrToDate( '01/05/1997' ) }
               { Se checa el Global de Amortizaci�n }
               Case FInicioAmortizacion of
                    iaBimestre: dPrestamo := SiguienteBimestre( dPrestamo );
                    iaMes: dPrestamo := SiguienteMes( dPrestamo );
                    iaEsteMes: dPrestamo := EsteMes( dPrestamo );
               else
                   dPrestamo := SiguienteBimestre( dPrestamo );
               end;
          end;
      { AP(09/02/2009): Ya no aplica
       dPrestamo := ZetaServerTools.dMin( dPrestamo, EncodeDate( 2008, 12, 31 ) ); { VALIDACION SUA } { StrToDate( '31/12/2008' ) }
     end;
end;

procedure CalculaDatosHistInfonavit( var dPrestamo: TDate );
begin
     with FEmpleado do
     begin
          if ( TipoMovimiento = infoInicio ) then
          begin
               FechaMovimiento := ZetaServerTools.dMax( FechaMovimiento, Ingreso );
               if ( PrestamoTipo = tiPorcentaje ) then
                  dPrestamo := ZetaServerTools.dMax( FechaMovimiento, EncodeDate( 1997, 04, 30 ) ) { StrToDate( '30/04/1997' ) }
               else
               begin
                    dPrestamo := ZetaServerTools.dMax( FechaMovimiento, EncodeDate( 1997, 05, 01 ) ); { StrToDate( '01/05/1997' ) }
                    { Se checa el Global de Amortizaci�n }
                    Case FInicioAmortizacion of
                         iaBimestre: dPrestamo := SiguienteBimestre( dPrestamo );
                         iaMes: dPrestamo := SiguienteMes( dPrestamo );
                         iaEsteMes: dPrestamo := EsteMes( dPrestamo );
                    else
                        dPrestamo := SiguienteBimestre( dPrestamo );
                    end;
               end;
          end
          else if ( TipoMovimiento = infoSuspen ) and FEnviaSuspenFinBim then
          begin
               dPrestamo:= rMax( FechaMovimiento, LastDayOfBimestre( FechaMovimiento ));
          end
          else
          begin
               dPrestamo:= FechaMovimiento;
               if lSinCreditoAlReingreso then
                  TipoMovimiento:= infoInicio;
          end;
          lSinCreditoAlReingreso:= FALSE;
         { AP(09/02/2009): Ya no aplica
         dPrestamo := ZetaServerTools.dMin( dPrestamo, EncodeDate( 2008, 12, 31 ) ); { VALIDACION SUA } { StrToDate( '31/12/2008' ) }
     end;
end;


procedure ExportaEmpleado;
const
     K_OCUPACION ='Empleado';
     K_CLAVE_LNAC = '00';
     K_TIPO_SALARIO_MIXTO = 2;
var                                                          
   dPrestamo :TDate;
   rSalSua: TPesos;
   sSignos: String;
begin
     FaltasInit;
     with FEmpleado do
     begin
          { rSalSua := GetSalarioSua( dIntegrado, dEmpInicial ); }
          { SUA2000: No acepta Cambios en la misma fecha de la alta }
          { por eso se tiene que reportar el salario del ingreso }
          if ( Ingreso < dEmpInicial ) then
             rSalSua := GetSalarioKardex( dEmpInicial - 1 )
          else
             rSalSua := GetSalarioSua( FechaSalarioIntegrado, dEmpInicial );
          {Se mandan los datos vac�os de Infonavit debido a que ya no vienen de Colabora.}
          if FUsarAcumuladoAmortiza and ( FEmpleado.PrestamoTipo <> tiPorcentaje ) and ( Prestamo > 0 ) then
          begin
               CalculaDatosInfonavit(dPrestamo);
          end
          else
          begin
               CreditoInfonavit := ''; { Pone en Blanco }
               dPrestamo := NullDateTime;
               Prestamo := 0;
               PrestamoTipo := eTipoInfonavit( 0 );
          end;

          if not FConHomoclave then
             RFC := Copy( RFC, 1, 10 );

          if ZetaCommonTools.StrLleno( CURP ) then
          begin
               if ( not ValidaCURP( CURP, sSignos ) ) then
                  oZetaProvider.Log.Advertencia( FEmpleado.Codigo, 'CURP: ' + sSignos, 'CURP: ' + CURP );

               if FValidarCURPvsRFC and ( not ValidaCURPyRFC( CURP, RFC, sSignos ) ) then
               begin
                    oZetaProvider.Log.Advertencia( FEmpleado.Codigo, 'CURP: ' + sSignos, 'CURP: ' + CURP );
                    CURP := ZetaCommonClasses.VACIO;
               end;
          end;

          with FSUADataset do
          begin
               try
                  { Se manda vacios en los datos de infonavit debido a que ahora los manda en ExportaInfonvit }
                  Append;
                  FieldByName( 'SUA_E_PAT' ).AsString := RegistroPatronal;
                  FieldByName( 'SUA_E_SS' ).AsString := NumeroSeguro;
                  FieldByName( 'SUA_E_RFC' ).AsString := RFC;
                  FieldByName( 'SUA_E_CUR' ).AsString := CURP;
                  FieldByName( 'SUA_E_NOMB' ).AsString := GetNombreSUA( ApellidoPaterno ) + '$' + GetNombreSUA( ApellidoMaterno ) + '$' + GetNombreSUA( Nombres );
                  FieldByName( 'SUA_E_ID' ).AsString := Nivel1 + ': ' + IntToStr( Codigo );
                  FieldByName( 'SUA_E_CRED' ).AsString := CreditoINFONAVIT;
                  FieldByName( 'SUA_E_MODP' ).AsInteger := Ord( ModuloPatron );
                  FieldByName( 'SUA_E_JORN' ).AsInteger := Ord( TipoJornada ) - 1; { TENTATIVAMENTE NECESITA UN MENOS 1 PARA CUMPLIR CON EL STD DE IMSS }

                  { US 13142 }
                  if PrestamoTipo = tiVecesUMA then
                        FieldByName( 'SUA_E_TIPO' ).AsInteger :=  oZetaProvider.GetGlobalInteger( K_GLOBAL_CREDITO_UMA )
                  else
                        FieldByName( 'SUA_E_TIPO' ).AsInteger := Ord(PrestamoTipo);
                  //

                  FieldByName( 'SUA_E_INGR' ).AsDateTime := FEmpleado.Ingreso;
                  FieldByName( 'SUA_E_FE_P' ).AsDateTime := dPrestamo;
                  FieldByName( 'SUA_E_WAGE' ).AsFloat := rSalSua;
                  FieldByName( 'SUA_E_LOAN' ).AsFloat := Prestamo;
                  //Datos Afiliatorios
                  FieldByName( 'SUA_E_CODP').AsString    := CodigoPostal;         { C�digo Postal }
                  FieldByName( 'SUA_E_FE_N' ).AsDateTime := FechaNacimiento;      { Fecha de Nacimiento }
                  FieldByName( 'SUA_E_LNAC').AsString    := EntidadNacimiento;    { Lugar de Nacimiento }
                  FieldByName( 'SUA_E_CNAC' ).AsString  := K_CLAVE_LNAC;         { Clave de Lugar de Nacimiento }
                  FieldByName( 'SUA_E_CLIN').AsString    := ZetaCommonTools.StrZero( Clinica, 3 );              { Cl�nica [Unidad de medicina Familiar UMF] }
                  FieldByName( 'SUA_E_OCUP' ).AsString   := K_OCUPACION;          { Ocupaci�n 'Empleado'}
                  FieldByName( 'SUA_E_SEXO' ).AsString   := Sexo;                 { Sexo [M � F] }
                  FieldByName( 'SUA_E_T_SA' ).AsInteger  := K_TIPO_SALARIO_MIXTO; { Tipo de salario }
                  FieldByName( 'SUA_E_HORA' ).AsString   := VACIO;                { Hora  }
                  Post;
               except
                     on Error: Exception do
                     begin
                          Cancel;
                     end;
               end;
          end;
     end;
end;

procedure ExportaInfonavit;
const
     //K_TIPO_MOV = '15';{ Inicio de Credito de Vivienda (ICV)}
     //K_DISM_N = 'N';
     eSUATipoMov: array[ eTipoMovInfonavit ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ('15','16','17','18','19','20' );
var
   dPrestamo, dInicial, dFinal :TDate;
   lUsaAmortizacion: Boolean;

   {procedure PreparaFechasHistorial;
   begin
        { Si se se borra empleados o se importa la alta de cr�dito se trae todo el historial hasta el final del mes o bimestre
        {if FBorrarEmpleados or ( FBorrarMovimientos = bsTodo) or ( ( FEmpleado.Ingreso >= FBimMedio ) and ( FEmpleado.Ingreso <= FLiqFinal ) ) then
        begin
             dInicial:= NullDateTime;
        end
        else
            dInicial:= FBimMedio;

        dFinal:= FLiqFinal;
   end;}

   procedure GrabaHistorial;
   begin
        with FSUAInfonavit do
        begin
             try
                Append;

                with FEmpleado do
                begin
                     FieldByName( 'SUA_F_PAT' ).AsString := RegistroPatronal;
                     FieldByName( 'SUA_F_SS' ).AsString := NumeroSeguro;
                     FieldByName( 'SUA_F_CRED' ).AsString := CreditoINFONAVIT;
                     FieldByName( 'SUA_F_TMOV' ).AsString :=  eSUATipoMov[TipoMovimiento];
                     FieldByName( 'SUA_F_FMOV' ).AsDateTime := dPrestamo;
                     { US 13142 }
                     if PrestamoTipo = tiVecesUMA then
                        FieldByName( 'SUA_F_TDES' ).AsInteger :=  oZetaProvider.GetGlobalInteger( K_GLOBAL_CREDITO_UMA )
                     else
                        FieldByName( 'SUA_F_TDES' ).AsInteger := Ord(PrestamoTipo);
                     //
                     FieldByName( 'SUA_F_VDES' ).AsFloat := Prestamo;
                     FieldByName( 'SUA_F_DISM' ).AsString := zBoolToStr(AplicaDisminucion );
                end;

                Post;
             except
                   on Error: Exception do
                   begin
                        Cancel;
                   end;
             end;
        end;
   end;

   procedure SetFechasHistorial;

      procedure SetHistorialAlReingreso;
      begin
           with FEmpleado do
           begin
                { Toma los datos de Infonavit a al Reingreso }
                PreparaInfonavitEmpleado( Codigo, Ingreso );
                { Asigna los datos de Infonavit a inicio de Bimestre }
                with FCursorInfonavitVigente do
                begin
                     lSinCreditoAlReingreso:= ( IsEmpty ) or ( FieldByName('KI_TIPO').AsInteger = Ord( infoSuspen ) );
                     if not lSinCreditoAlReingreso then
                     begin
                          SetHistorialEmpleado( FCursorInfonavitVigente );
                          dPrestamo:= ZetaServerTools.dMax( FechaMovimiento, Ingreso );
                          TipoMovimiento:= infoInicio;
                          GrabaHistorial;
                     end;
                end;
           end;
      end;

   begin
        if FBorrarEmpleados or (FBorrarMovimientos = bsTodo) then
        begin
             if ( FEmpleado.FechaBSS <> NullDateTime ) then
             begin
                  SetHistorialAlReingreso;  //Importa solamente el mov. a como estaban las cosas al reingreso
                  dInicial:= FEmpleado.Ingreso + 1;  //prepara fecha inicial para los mov. posteriores al reingreso
             end
             else
                 dInicial:= NullDateTime;
        end
        else if ( FEmpleado.Ingreso >= FBimMedio ) and ( FEmpleado.Ingreso <= FLiqFinal ) and ( FEmpleado.FechaBSS = NullDateTime  ) then
        begin
             dInicial:= NullDateTime;
        end
        else
            dInicial:= FBimMedio;

        dFinal:= FLiqFinal;
   end;

   function ExpedienteValido: Boolean;
   var
      dFechaSuspen: TDateTime;
      dTipoMov: eTipoMovInfonavit;
   begin
        Result:= TRUE;
        dFechaSuspen:= NullDateTime;
        if FEnviaSuspenFinBim then   // Valida que el reinicio sea despues de la suspensi�n
        begin
             with FCursorHistInfonavit do
             begin
                  while not Eof  do
                  begin
                       dTipoMov:= eTipoMovInfonavit( FieldByName('KI_TIPO').AsInteger );
                       if ( dTipoMov = infoSuspen  ) then
                            dFechaSuspen:= dMax( FieldByName('KI_FECHA').AsDateTime, LastDayOfBimestre(FieldByName('KI_FECHA').AsDateTime ) )
                       else
                            Result:= not ( ( dTipoMov = infoReinicio ) and ( dFechaSuspen <> NullDateTime ) and
                                           ( FieldByName('KI_FECHA').AsDateTime < dFechaSuspen ) );

                       if not Result then
                       begin
                            oZetaProvider.Log.Error( FEmpleado.Codigo, 'Error al exportar datos de Infonavit',
                                                     Format( 'No se Export� Expediente de Infonavit, Movimiento de Reinicio con fecha anterior a la suspensi�n [%s]', [ FechaAsStr(dFechaSuspen) ] ) );
                            Break;
                       end;

                       Next;
                  end;
                  First;     //Regresa el cursor al principio
             end;
        end;
   end;


begin
     with FEmpleado do
     begin
          lSinCreditoAlReingreso:= FALSE;
          lUsaAmortizacion:= ( FUsarAcumuladoAmortiza and ( PrestamoTipo <> tiPorcentaje ) and ( Prestamo > 0 ) );

          { Si Usa el acumulado de amortizaci�n no manda historial sino los datos de colabora
            SIEMPRE debe enviar el monto del prestamo}
          if ( lUsaAmortizacion )  then
          begin
               { Si exporto el ingreso el cr�dito se exporta con �l ya no es necesario volverlo a exportar }
               if ( not lExportaIngreso ) and ( PrestamoTipo <> tiNoTiene ) then
               begin
                    CalculaDatosInfonavit(dPrestamo);
                    GrabaHistorial;
               end;
          end
          else
          begin
               SetFechasHistorial; //Prepara fechas...Todo..lo del mes o despues de reingreso
               PreparaHistInfonavitEmp( Codigo, dInicial, dFinal);
               if ( ExpedienteValido ) then
               begin
                    with FCursorHistInfonavit do
                    begin
                         while not Eof do
                         begin
                              { Asigna a los datos del empleado los datos del historial  }
                              SetHistorialEmpleado( FCursorHistInfonavit);
                              { trae la fecha del movimiento validada }
                              CalculaDatosHistInfonavit(dPrestamo);
                              GrabaHistorial;
                              Next;
                         end;
                    end;
               end;
          end;
     end;
end;

begin     { ExportaSUA }
     if PuedeProcesar then
     begin
          dEmpInicial := ZetaServerTools.dMax( FLiqInicial, FEmpleado.Ingreso );
          lExportaIngreso:= FBorrarEmpleados or ( ( FEmpleado.Ingreso >= FBimMedio ) and ( FEmpleado.Ingreso <= FLiqFinal ) );

          if lEmpleados and lExportaIngreso then
          begin
               ExportaEmpleado;
          end;

          if lMovimientos then
          begin
               ExportaMovimientos;
               ExportaInfonavit;
          end;
     end;
end;

procedure TIMSS.ExportarSUAEnd;
begin
     FEnviarSUA := False;
     FEnviaSuspenFinBim:= False;
     PreparaQuerysEnd;
end;

{$ifdef EVALUA_LIQ_IMSS}

function TIMSS.CalculaMontoVacaciones( const rDiasVaca: TPesos ): TPesos;
{
 Esta funci�n solo puede ser invocada despues de haber calculado la liquidaci�n
}
var
   rTopado, rSMGDF: TPesos;
   // DES US #13122 Infonavit - Descuento tipo # veces UMA...(Liquidaci�n IMSS)
   rValUMA, rValMenorUMA_SMGDF: TPesos;
   rValFactorDescuento: TPesos;
   rTiVeces_Factor, rTiVecesUMA_Factor: TPesos;
begin
     Result := 0;
     rSMGDF := FCreator.dmSalMin.GetSMGDF( FechaFinal );

     // DES US #13122 Infonavit - Descuento tipo # veces UMA...(Liquidaci�n IMSS)
     rValUMA := FCreator.dmValUMA.GetVUDefaultDiario( FechaFinal );

     rValFactorDescuento := FCreator.dmValUMA.GetFactorDescuento ( FechaFinal );

     rValMenorUMA_SMGDF := rSMGDF;
     if (rValUMA <> 0) and (rValUMA < rSMGDF) then
        rValMenorUMA_SMGDF := rValUMA;

     if rValFactorDescuento = 0 then
     begin
          rTiVeces_Factor := rValMenorUMA_SMGDF;
          rTiVecesUMA_Factor := rValUMA;
     end
     else
     begin
          rTiVeces_Factor := rValFactorDescuento;
          rTiVecesUMA_Factor := rValFactorDescuento;
     end;

     with FEmpleado do
     begin
          case PrestamoTipo of
               // DES US #13122 Infonavit - Descuento tipo # veces UMA...(Liquidaci�n IMSS)
               tiPorcentaje:
               begin
                    if FUsarUMA then
                       Result := CalculaCuota( FPrestamoTope, GetSalarioSua( FechaSalarioIntegrado, FechaFinal ), rDiasVaca, rValUMA, Prestamo, rTopado )
                    else
                        Result := CalculaCuota( FPrestamoTope, GetSalarioSua( FechaSalarioIntegrado, FechaFinal ), rDiasVaca, rSMGDF, Prestamo, rTopado )
               end;
               tiCuotaFija:  Result := CalculaCuotaFija( Prestamo, rDiasVaca, FDiasBim );
               // DES US #13122 Infonavit - Descuento tipo # veces UMA...(Liquidaci�n IMSS)
               tiVeces:      Result := CalculaCuotaFija( Prestamo * rTiVeces_Factor, rDiasVaca, FDiasBim );
               tiVecesUMA:   Result := CalculaCuotaFija( Prestamo * rTiVecesUMA_Factor, rDiasVaca, FDiasBim );
          end;
     end;
end;
{$endif}

end.


