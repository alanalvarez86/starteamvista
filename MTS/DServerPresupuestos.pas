unit DServerPresupuestos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComServ, ComObj, VCLCom, StdVcl, {bdemts,} DataBkr, DBClient,DB,
  Variants,
  MtsRdm,
  {$ifndef DOS_CAPAS}
   Presupuestos_TLB,
  {$endif}
  Mtx,
  ZetaServerTools,
  ZCreator,
  ZetaTipoEntidad,
  ZetaSQLBroker,
  DZetaServerProvider;
type
  // (JB) Escenarios de presupuestos T1060 CR1872
  eInfoPresupuestos = ( eEventosAltas, eSupuestosRH, eEscenarios );     {0}
  TdmServerPresupuestos = class(TMtsDataModule {$ifndef DOS_CAPAS}, IdmServerPresupuestos {$endif})
    cdsLista: TClientDataSet;
    procedure MtsDataModuleDestroy(Sender: TObject);
    procedure MtsDataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    FDataSet: TZetaCursor;
    FListaParametros: String;
    oZetaProvider: TdmZetaServerProvider;
  {$ifndef DOS_CAPAS}
    oZetaCreator: TZetaCreator;
    oZetaSQLBroker: TSQLBroker;
  {$endif}
    FEmpleados:TDataSet;
    FFiltroSimulacion:string;
   function GetSQLScript( const iScript: Integer ): String;
   function GetSQLBroker: TSQLBroker;
   function DepuraNominasDataset: OleVariant;
   procedure DepuraNominasParametros;
   procedure DepuraSupuestosRHParametros;
   procedure CalculaCuboParametros;
   function CalculaCuboDataSet: OleVariant;
   procedure SetTablaInfo( eInfo : eInfoPresupuestos );
   procedure EjecutaDataSet( const iScript: integer; const sMensaje: string );overload;
   procedure EjecutaDataSet( const sScript, sMensaje: string );overload;
   procedure SimulacionPresupuestosBuildDataSet;
    procedure InitBroker(const lRegistraFuncionesAnuales: Boolean);
    procedure InitCreator;
   function GetFiltroSimulacion:string;
    procedure ClearBroker;
  protected
   class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
   property SQLBroker: TSQLBroker read GetSQLBroker;
{$ifdef DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
{$endif}
   function DepuraSupuestosRH(Empresa, Parametros: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall; {$endif}
   function DepuraSupuestosRHDataSet: OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
   function CalculaCubo(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
   function GetEventosAltas(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
   function GrabaEventosAltas(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
   function GetSupuestosRH(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
   function GrabaSupuestosRH(Empresa, oDelta: OleVariant;out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
   function DepuraNominas(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
   function GetPeriodosBaja(Empresa: OleVariant; FechaIni, FechaFin: TDateTime): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
   function GetPeriodosSimulacion(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
   // (JB) Escenarios de presupuestos T1060 CR1872    
   function GetEscenarios(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
   function GrabaEscenarios(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;{$ifndef DOS_CAPAS} safecall; {$endif}

  end;

var
  dmServerPresupuestos: TdmServerPresupuestos;

implementation

uses
    ZetaCommonClasses,
    ZGlobalTress,
    ZetaCommonLists,
    ZetaCommonTools;

const
     K_BORRA_ALTAS_MULTIPLES = 1;
     K_BORRA_KARDEX = 2;
     K_RECALCULA_TODOS = 3;
     K_BORRA_ACUMULA = 4;
     K_BORRA_NOMINAS = 5;
     K_BORRA_CUBO = 6;
     K_CALCULA_CUBO = 7;
     K_UPDATE_PERIODO = 8;
     K_BORRA_POLIZA = 9;
     K_QRY_PERIODOS = 10;
     K_QRY_PERIODO_BAJAS = 11;
     K_UPDATE_ACUMULA = 12;
     Q_RECALCULA_AHORROS_TODOS = 13;
     Q_RECALCULA_PRESTAMOS_TODOS = 14;
     Q_UPDATE_NOMINA = 15;
     K_BORRA_ACUMULA_RS = 16;
     K_UPDATE_ACUMULA_RS = 17;

{$R *.DFM}

class procedure TdmServerPresupuestos.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

procedure TdmServerPresupuestos.MtsDataModuleCreate(Sender: TObject);
begin
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
end;

procedure TdmServerPresupuestos.MtsDataModuleDestroy(Sender: TObject);
begin
    ZetaSQLBroker.FreeZetaCreator( oZetaCreator );
    DZetaServerProvider.FreeZetaProvider( oZetaProvider );
end;

{ *********** Clases Auxiliares ************ }

function TdmServerPresupuestos.GetSQLBroker: TSQLBroker;
begin
{$ifdef DOS_CAPAS}
     Result := ZetaSQLBroker.oZetaSQLBroker;
{$else}
     Result := Self.oZetaSQLBroker;
{$endif}
end;

{$ifdef DOS_CAPAS}
procedure TdmServerPresupuestos.CierraEmpresa;
begin
end;
{$endif}

procedure TdmServerPresupuestos.SetTablaInfo(eInfo: eInfoPresupuestos);
begin
     with oZetaProvider.TablaInfo do
     begin
          case eInfo of
               eEventosAltas : SetInfo( 'EV_ALTA', 'EV_CODIGO,EV_DESCRIP,EV_ACTIVO,CB_CODIGO,EV_CONTRAT,EV_BAJA,EV_PUESTO,EV_CLASIFI,EV_TURNO,EV_PATRON,' +
                                                   'EV_NIVEL1,EV_NIVEL2,EV_NIVEL3,EV_NIVEL4,EV_NIVEL5,EV_NIVEL6,EV_NIVEL7,EV_NIVEL8,EV_NIVEL9,EV_NIVEL0,EV_AUTOSAL,' +
                                                   'EV_SALARIO,EV_PER_VAR,EV_ZONA_GE,EV_TABLASS,EV_OTRAS_1,EV_OTRAS_2,EV_OTRAS_3,EV_OTRAS_4,EV_OTRAS_5,EV_BAN_ELE,EV_NOMINA', 'EV_CODIGO' );
			   // (JB) Escenarios de presupuestos T1060 CR1872
               eSupuestosRH  : SetInfo( 'SUPUEST_RH', 'SR_FECHA,SR_TIPO,EV_CODIGO,SR_CUANTOS,US_CODIGO,ES_CODIGO', 'SR_FECHA,SR_TIPO,EV_CODIGO,ES_CODIGO' );
               eEscenarios   : SetInfo( 'ESCENARIO', 'ES_CODIGO, ES_ELEMENT, ES_INGLES, ES_NUMERO, ES_TEXTO', 'ES_CODIGO' );
          end;
     end;
end;

function TdmServerPresupuestos.GetSQLScript( const iScript: Integer ): String;
begin
     case iScript of
          K_BORRA_ALTAS_MULTIPLES : Result :=  'delete from COLABORA ' +
                                               'where COLABORA.CB_CODIGO in ( select K.CB_CODIGO from KARDEX K '+
                                                                      'where %s ( CB_TIPO = %s ) and '+
                                                                      '( CB_REINGRE = %s ) and '+
                                                                      '( US_CODIGO <> 0 ) ) %s';
          K_BORRA_KARDEX : Result := 'delete from KARDEX where %s ( US_CODIGO <> 0 ) %s ';
          K_RECALCULA_TODOS: Result := 'execute procedure RECALCULA_TODOS';
          K_BORRA_ACUMULA : Result := 'delete from ACUMULA where AC_YEAR = %d %s';
          {*** US 16218: Acumulados en Presupuestos no se limpian***}
          K_BORRA_ACUMULA_RS : Result := 'delete from ACUMULA_RS where AC_YEAR = %d %s';
          K_UPDATE_ACUMULA  : Result := 'update ACUMULA set AC_MES_%s = 0 where AC_YEAR = %d %s';
          {*** US 16218: Acumulados en Presupuestos no se limpian***}
          K_UPDATE_ACUMULA_RS  : Result := 'update ACUMULA_RS set AC_MES_%s = 0 where AC_YEAR = %d %s';
          K_BORRA_NOMINAS : Result := 'delete from NOMINA where PE_YEAR = %d %s %s';
          K_BORRA_CUBO : Result := 'delete from TOTPRESUP where ES_CODIGO = %s';
          K_CALCULA_CUBO : Result := 'EXECUTE CALCULA_CUBO %d, %s';
          K_UPDATE_PERIODO : Result := 'update PERIODO set US_CODIGO = 0, PE_TOT_NET = 0, PE_TOT_DED = 0, PE_TOT_PER = 0, '+
                                       'PE_NUM_EMP = 0, PE_STATUS = 0 where '+
                                       '( PE_YEAR = %d ) %s';
          K_BORRA_POLIZA : Result := 'delete from POL_HEAD where PE_YEAR = %d %s';
          K_QRY_PERIODOS : Result := 'select PE_MES, PE_TIPO, PE_NUMERO, PE_POS_MES from PERIODO ' +
                                     'where ( PE_YEAR = %d ) and ( PE_MES >= %d ) and ( PE_MES <= %d )' +
                                     'order by PE_MES, PE_TIPO, PE_NUMERO';
          K_QRY_PERIODO_BAJAS : Result := 'select PE_YEAR, PE_TIPO, PE_NUMERO, PE_FEC_INI, PE_FEC_FIN from PERIODO ' +
                                          'where ( PE_NUMERO <= %d ) and ( PE_FEC_INI <= %s ) and ( PE_FEC_FIN >= %s ) ' +
                                          'order by PE_TIPO, PE_FEC_INI';
          // (JB) Escenarios de presupuestos T1060 CR1872
          Q_RECALCULA_AHORROS_TODOS : Result := 'EXECUTE SP_RECALCULA_AHORROS_TODOS';
          Q_RECALCULA_PRESTAMOS_TODOS : Result := 'EXECUTE SP_RECALCULA_PRESTAMOS_TODOS';
          Q_UPDATE_NOMINA: Result  := 'Update NOMINA SET NO_STATUS = %d where PE_YEAR = %d %s';
     end;
end;

procedure TdmServerPresupuestos.EjecutaDataSet( const iScript: integer; const sMensaje: string );
begin
     EjecutaDataSet( GetSQLScript( iScript ), sMensaje );
end;

procedure TdmServerPresupuestos.EjecutaDataSet( const sScript, sMensaje: string );
begin
     with oZetaProvider do
     begin
          PreparaQuery( FDataset, sScript );
          EmpiezaTransaccion;
          try
             Ejecuta( FDataset );
             TerminaTransaccion( True );
          except
                on Error: Exception do
                begin
                     Log.Excepcion( 0, sMensaje, Error );
                end;
          end;
          CanContinue;
     end;
end;

{ ************************** DepuraSupuestosRH ******************************}

function TdmServerPresupuestos.DepuraSupuestosRH(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList(Parametros);
     end;
     //Solo se carga una vez ...
     SimulacionPresupuestosBuildDataSet;
     DepuraSupuestosRHParametros;
     Result := DepuraSupuestosRHDataset;
     SetComplete;
end;

procedure TdmServerPresupuestos.DepuraSupuestosRHParametros;
begin
  with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'A�o: ' + ParamByName( 'Year' ).AsString;
     end;
end;

function TdmServerPresupuestos.DepuraSupuestosRHDataset: OleVariant;
 const K_PASOS = 3;
 var
    dFechaRH1, dFechaRH2: TDate;
    sRangoFechas: string;
begin
     with oZetaProvider do
     begin
          if OpenProcess( prPREDepuraSupuestosRH, K_PASOS, FListaParametros ) then
          begin
               FDataSet := CreateQuery;
               if NOT ParamList.ParamByName('TodosSupuestosRH').AsBoolean then
               begin
                    dFechaRH1:= ParamList.ParamByName('FechaRH1').AsDate;
                    dFechaRH2:= ParamList.ParamByName('FechaRH2').AsDate;
                    sRangoFechas := Format( '( CB_FECHA >= %s and CB_FECHA <= %s ) and ',
                                            [ DateToStrSQLC( dFechaRH1 ),
                                              DateToStrSQLC( dFechaRH2 )] );

               end
               else
               begin
                    sRangoFechas := VACIO;
               end;
               try
                  //Se borraran los empleados dados de alta por la simulacion.
                  EjecutaDataSet( Format( GetSQLScript( K_BORRA_ALTAS_MULTIPLES ),
                                          [ sRangoFechas,
                                            EntreComillas( K_T_ALTA ),
                                            EntreComillas( K_GLOBAL_NO ), FFiltroSimulacion  ] ),
                                          'Error al borrar contrataciones' );
                  //Se borran todos los movimientos de Kardex realizados por la simulacion
                  EjecutaDataSet( Format( GetSQLScript( K_BORRA_KARDEX ),
                                                        [sRangoFechas,FFiltroSimulacion] ) , 'Error al borrar cambios y recortes de personal' );
                  //Se recalcula nuevamente el Kardex para garantizar que todos los datos estan normalizados
                  EjecutaDataSet( K_RECALCULA_TODOS, 'Error al recalcular Kardex de todos los empleados' );
               finally
                      FreeAndNil(FDataset);
               end;
               Result := CloseProcess;
          end;
     end;
end;

{ ************************* Depuracion de Nominas *************************}

function TdmServerPresupuestos.DepuraNominas(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList(Parametros);
     end;
     SimulacionPresupuestosBuildDataSet; 
     DepuraNominasParametros;
     Result := DepuraNominasDataset;

     SetComplete;
end;

procedure TdmServerPresupuestos.DepuraNominasParametros;
begin
  with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'A�o: ' + ParamByName( 'Year' ).AsString;
     end;
end;

function TdmServerPresupuestos.DepuraNominasDataset: OleVariant;
 const K_PASOS = 4;
 var
    sRangoNominas: string;
    lTodasNominas: Boolean;
    i, iMesInicial, iMesFinal,iYear: integer;
begin
     with oZetaProvider do
     begin
          if OpenProcess( prPREDepuraNominas, K_PASOS, FListaParametros ) then
          begin
               FDataSet := CreateQuery;
               lTodasNominas := ParamList.ParamByName( 'TodasNominas' ).AsBoolean;
               iMesInicial := ParamList.ParamByName( 'MesInicial' ).AsInteger;
               iMesFinal := ParamList.ParamByName( 'MesFinal' ).AsInteger;
               iYear := ParamList.ParamByName( 'Year' ).AsInteger;

               if lTodasNominas then
               begin
                    sRangoNominas := VACIO;
               end
               else
               begin
                    sRangoNominas := Format( ' AND ( PE_NUMERO in( select P.PE_NUMERO from PERIODO P ' +
                                             ' where P.PE_YEAR = %d and P.PE_MES between %d and %d ) ) ' ,
                                             [ iYear, iMesInicial,iMesFinal ] );
               end;

               try
                  if lTodasNominas then
                  begin
                       //Se borraran todos los acumulados.
                       EjecutaDataSet( format( GetSQLScript( K_BORRA_ACUMULA ), [ ParamList.ParamByName( 'Year' ).AsInteger,FFiltroSimulacion ] ), 'Error al borrar acumulados anuales' );
                       {*** US 16218: Acumulados en Presupuestos no se limpian***}
                       EjecutaDataSet( format( GetSQLScript( K_BORRA_ACUMULA_RS ), [ ParamList.ParamByName( 'Year' ).AsInteger,FFiltroSimulacion ] ), 'Error al borrar acumulados anuales' );
                  end
                  else
                  begin
                       for i:= iMesInicial to iMesFinal do
                       begin
                            EjecutaDataSet( Format( GetSQLScript( K_UPDATE_ACUMULA ), [ StrToZero( i,2,0 ),  iYear,FFiltroSimulacion ] ), 'Error al borrar acumulados anuales' );
                            {*** US 16218: Acumulados en Presupuestos no se limpian***}
                            EjecutaDataSet( Format( GetSQLScript( K_UPDATE_ACUMULA_RS ), [ StrToZero( i,2,0 ),  iYear,FFiltroSimulacion ] ), 'Error al borrar acumulados anuales' );
                       end;
                  end;

                  //Se borran todos las nominas, sus movimientos y excepciones
                  EjecutaDataSet( Format( GetSQLScript( K_BORRA_NOMINAS), [ iYear, sRangoNominas, FFiltroSimulacion ] ), 'Error al borrar n�minas' );

                  //Se borran todos las POLIZAS CONTABLES
                  EjecutaDataSet( Format ( GetSQLScript( K_BORRA_POLIZA ), [ iYear, sRangoNominas  ] ), 'Error al borrar p�lizas contables' );

                  //Se actualiza el status y totales de los periodos
                  EjecutaDataSet( format( GetSQLScript( K_UPDATE_PERIODO ), [ iYear, sRangoNominas ] ), 'Error al borrar n�minas' );

                   //Se actualiza el status y totales de los periodos
                  if StrLleno(FFiltroSimulacion )then
                     EjecutaDataSet( format( GetSQLScript( Q_UPDATE_NOMINA ), [ Ord(spCalculadaTotal), iYear, sRangoNominas  ] ), 'Error al borrar n�minas' );

                  // (JB) Escenarios de presupuestos T1060 CR1872
                  //      Se recalculan todos los ahorros de todos los empleados
                  EjecutaDataSet( Q_RECALCULA_AHORROS_TODOS , 'Error al recalcular todos los ahorros' );
                  
                  //      Se recalculan todos los prestamos de todos los empleados                  
                  EjecutaDataSet( Q_RECALCULA_PRESTAMOS_TODOS , 'Error al recalcular todos los pr�stamos' );
                  
               finally
                      FreeAndNil(FDataset);
               end;
               Result := CloseProcess;
          end;
     end;
end;

function TdmServerPresupuestos.CalculaCubo(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList(Parametros);
     end;
     //SimulacionPresupuestosBuildDataSet;
     CalculaCuboParametros;
     Result := CalculaCuboDataset;

     SetComplete;
end;

procedure TdmServerPresupuestos.CalculaCuboParametros;
begin
  with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'A�o: ' + ParamByName( 'Year' ).AsString;
     end;
end;

function TdmServerPresupuestos.CalculaCuboDataset: OleVariant;
const
     K_PASOS = 2;
var
     sEscenario : String;
     iYear : Integer;
begin
     with oZetaProvider do
     begin
          if OpenProcess( prPRECalculaCubo, K_PASOS, FListaParametros ) then
          begin
               FDataSet := CreateQuery;
               try
                  // (JB) Escenarios de presupuestos T1060 CR1872
                  sEscenario := Comillas( ParamList.ParamByName( 'Escenario' ).AsString );
                  iYear := ParamList.ParamByName( 'Year' ).AsInteger;

                  //Se borraran todos el cubo.
                  EjecutaDataSet(  format( GetSQLScript( K_BORRA_CUBO ), [ sEscenario ] ), 'Error al borrar el cubo' );
                  //Se borran todos las nominas, sus movimientos y excepciones
                  EjecutaDataSet( format ( GetSQLScript( K_CALCULA_CUBO ), [ iYear, sEscenario ] ), 'Error al calcular el cubo' );

               finally                                 
                      FreeAndNil(FDataset);
               end;
               Result := CloseProcess;
          end;
     end;
end;

function TdmServerPresupuestos.GetEventosAltas( Empresa: OleVariant ): OleVariant;
begin
     SetTablaInfo( eEventosAltas );
     Result := oZetaProvider.GetTabla( Empresa );
     SetComplete;
end;

// (JB) Escenarios de presupuestos T1060 CR1872
function TdmServerPresupuestos.GetEscenarios( Empresa: OleVariant ): OleVariant;
begin
     SetTablaInfo( eEscenarios );
     Result := oZetaProvider.GetTabla( Empresa );
     SetComplete;
end;

function TdmServerPresupuestos.GrabaEventosAltas(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( eEventosAltas );
     Result := oZetaProvider.GrabaTabla( Empresa, oDelta, ErrorCount );
     SetComplete;
end;

function TdmServerPresupuestos.GetSupuestosRH(Empresa: OleVariant): OleVariant;
begin
     SetTablaInfo( eSupuestosRH );
     Result := oZetaProvider.GetTabla( Empresa );
     SetComplete;
end;

function TdmServerPresupuestos.GrabaSupuestosRH(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( eSupuestosRH );
     Result := oZetaProvider.GrabaTablaGrid( Empresa, oDelta, ErrorCount );
     SetComplete;
end;

function TdmServerPresupuestos.GetPeriodosBaja(Empresa: OleVariant; FechaIni, FechaFin: TDateTime): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          InitGlobales;
          Result := OpenSQL( Empresa, Format( GetSQLScript( K_QRY_PERIODO_BAJAS ), [ //K_LIMITE_NOM_NORMAL,
                                                            GetGlobalInteger( K_GLOBAL_LIMITE_NOMINAS_ORDINARIAS ),
                                                            DateToStrSQLC( FechaFin ),
                                                            DateToStrSQLC( FechaIni ) ] ), TRUE );
     end;
     SetComplete;
end;

function TdmServerPresupuestos.GetPeriodosSimulacion(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          with ParamList do
          begin
               Result:= OpenSQL( Empresa, Format( GetSQLScript( K_QRY_PERIODOS ), [ ParamByName( 'Year' ).AsInteger,
                                                                                    ParamByName( 'MesInicial' ).AsInteger,
                                                                                    ParamByName( 'MesFinal' ).AsInteger ]  ), TRUE );
          end;
     end;
     SetComplete;
end;

// (JB) Escenarios de presupuestos T1060 CR1872
function TdmServerPresupuestos.GrabaEscenarios(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( eEscenarios );
     Result := oZetaProvider.GrabaTabla( Empresa, oDelta, ErrorCount );
     SetComplete;
end;

procedure TdmServerPresupuestos.SimulacionPresupuestosBuildDataSet;
begin
     InitBroker( False );
     try
        with oZetaProvider do
        begin
             with SQLBroker do
             begin
                  Init( enEmpleado );
                  with Agente do
                  begin
                       AgregaColumna( 'COLABORA.CB_CODIGO', True, Entidad, tgNumero, 10, 'CB_CODIGO' );

                  end;
                  AgregaRangoCondicion( ParamList );
                  BuildDataset( EmpresaActiva );
                  if SuperReporte.Agente.NumFiltros > 0 then
                  begin
                       FEmpleados := SuperReporte.DataSetReporte;
                       FFiltroSimulacion := GetFiltroSimulacion;
                  end
                  else
                      FFiltroSimulacion := VACIO;
             end;
        end;
     finally
            ClearBroker;
     end;
end;

procedure TdmServerPresupuestos.InitBroker( const lRegistraFuncionesAnuales: Boolean );
begin
     InitCreator;
     with oZetaCreator do
     begin
          if lRegistraFuncionesAnuales then
             RegistraFunciones( [ efComunes,efAnuales ] )
          else
              RegistraFunciones([efComunes,efAguinaldo]);
     end;
     oZetaSQLBroker := ZetaSQLBroker.GetSQLBroker( oZetaCreator );
end;

procedure TdmServerPresupuestos.InitCreator;
begin
     if not Assigned( oZetaCreator ) then
        oZetaCreator := ZetaSQLBroker.GetZetaCreator( oZetaProvider );
end;

function TdmServerPresupuestos.GetFiltroSimulacion: string;
begin
     Result := VACIO;
     with FEmpleados do
     begin
          First;
          while not eof do
          begin
               Result := ConcatString(Result,FieldByName('CB_CODIGO').AsString,',' );
               Next;
          end;
     end;
     if Result <> VACIO then
        Result := Format(' and CB_CODIGO in ( %s )',[Result] );
end;

procedure TdmServerPresupuestos.ClearBroker;
begin
     ZetaSQLBroker.FreeSQLBroker( oZetaSQLBroker );
end;

{$ifndef DOS_CAPAS}
initialization
  TComponentFactory.Create(ComServer, TdmServerPresupuestos,Class_dmServerPresupuestos, ciMultiInstance, ZetaServerTools.GetThreadingModel);
{$endif}
end.

