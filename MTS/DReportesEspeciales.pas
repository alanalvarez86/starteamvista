unit DReportesEspeciales;

interface
uses Windows, Messages, SysUtils, Classes,Controls,
     DB,
     Variants,
     DZetaServerProvider,
     ZAgenteSQL,
     ZCreator,
     ZetaSQLBroker,
     ZetaServerDataSet,
     ZetaCommonClasses,
     ZetaTipoEntidad,
     ZReportConst;

const
      Q_MAX_GRUPOS = 5; //El numero maximo de grupos permitidos en este tipo de reporte.
      {$IFDEF AMEX}
      K_REP_ANCHO_CODIGO = 10;
      {$ELSE}
      K_REP_ANCHO_CODIGO = 6;
      {$ENDIF}

type
    TArreglo = array[1..Q_MAX_GRUPOS] of string;
    TListadoNomina = Class
     private
       FNomina: TDataset;
       FSQLAgente : TSQLAgente;
       MovimienBroker : TSQLBroker;
       FSP_Balanza : TZetaCursor;
       FInsertTmpLista : TZetaCursor;
       FUpdateDatosEmpl : TZetaCursor;
       FInsertBalanza : TZetaCursor;
       FMovimien : TZetaCursor;
       FCountRegistros : TZetaCursor;
       FCountDatosEmpleado : integer;
       FSoloTotales, FTotalizarNeto : Boolean;
       Conceptos : TStrings;
       oListaDatosEmpleados : TStrings;
       oZetaProvider: TdmZetaServerProvider;
       oZetaCreator: TZetaCreator;
       oSQLBroker : TSQLBroker;
       property SQLBroker: TSQLBroker read oSQLBroker;
       function DatosEmplField(const i: integer): string;
       function TA_NIVEL(const i: integer): string;
       function TP_NIVEL(const i: integer): string;
       function TIT_NIVEL(const i: integer): string;
       function DES_NIVEL(const i: integer): string;
       procedure ClearBroker;
       procedure InitBroker;
       procedure PreparaBrokerMovimien;
       procedure PreparaBrokerNomina;
       procedure DesPrepara;
       procedure Prepara;

       procedure ActualizaDatosEmpleado( Dataset, oTotales: TDataset;
                                         const iEmpleado : integer;
                                         const sFiltro : string = '' );
       procedure ActualizaDatosEmpleadoGrupo( Dataset, oTotales: TDataset; const iEmpleado : integer;
                                              //const Niveles : array of string;
                                              const Niveles : TArreglo;
                                              const sFiltro : string = '' );
       procedure AgregaRegistroBalanza(DataSet: TDataset;const iColumna, iEmpleado: integer);
       procedure AgregaRegistroNomina(DataSet: TDataset; const iEmpleado{,iCuantos}: integer);
       procedure AgregaRegistros(DataSet: TDataset; const iColumna,iEmpleado: integer);
       procedure AgregaRegistroTmpLista(DataSet: TDataSet;const iEmpleado: integer);
       function AgregaDetalleBalanza: Boolean;
       procedure AgregaTotalesEmpresa;
       procedure AsignaNiveles(Destino: TZetaCursor; Fuente: TDataset;const sFieldDestino, sFieldFuente: string);
       procedure BorraTabla(const sTabla, sUserCampo: string);
       procedure AgregaConceptos(oBroker: TSQLBroker; oConceptos: TStrings;const sFiltro: string);
       procedure AgregaDatosEmpleados(oAgente: TSQLAgente;const oEntidad: TipoEntidad; const sTabla: string);
       procedure AgregaDatosPeriodo(oAgente: TSQLAgente;const oEntidad: TipoEntidad; const sTabla: string);
       procedure AgregaTA_NIVELES(oAgente: TSQLAgente;const oEntidad: TipoEntidad);
       procedure AgregaDES_NIVELES(oAgente: TSQLAgente;const oEntidad: TipoEntidad);
       procedure EjecutaBalanza( oBalanza: TZetaCursor; DataSet: TDataset;
                                 const sNivelSource, sNivelDestino, sDescrip, sReferencia: string; const iEmpleado, iConcepto, iColumna: integer;
                                 const rMonto, rHoras: TPesos);
       procedure BalanceaTotales( oTotales, oRegistros: TDataSet;
                                  const sFieldDestino, sFieldSource : string;
                                  const Niveles:TArreglo);
       procedure InsertBalanza( DataSet: TDataset;
                                const iEmpleado, iCuantos,iPerConcepto, iDedConcepto : integer;
                                const sDatosEmpl, sPerDescrip,sDedDescrip,sPerRef,sDedRef : string;
                                const rPercepcion, rDeduccion, rPerHoras, rDedHoras: TPesos );
       function GetHorasConcepto( Dataset: TDataset ): TPesos;
       function GetFiltroTipoConceptosBalanza: string;

     public
       constructor Create(oProvider : TdmZetaServerProvider; oCreator : TZetaCreator );
       function Calcula(oSQLAgente: TSQLAgente; var Error: string): TSQLAgente;
    end;

const

     {SCRIPTS DE USO GENERAL, SE UTILIZAN PARA VARIOS DE LOS REPORTES ESPECIALES}
     Q_DELETE_TABLA = 'DELETE FROM %s WHERE %s = %d';

     {Scripts que se utiliza tanto para Reportes de
     -Totales de Movimientos
     -Listado de Nomina }
     Q_TOTALES_INSERT_LISTA =
       'INSERT INTO TMPLISTA '+
       '(TA_USER,TA_NIVEL1,TA_NIVEL2,TA_NIVEL3,  '+
       'TA_NIVEL4,TA_NIVEL5,CB_CODIGO,CO_NUMERO, '+
       'TA_REFEREN,TA_PERCEPC,TA_DEDUCCI,TA_TOTAL, TA_HORAS,'+
       'PE_YEAR ,PE_TIPO ,PE_NUMERO ) VALUES '+
       '(:TA_USER,:TA_NIVEL1,:TA_NIVEL2,:TA_NIVEL3,  '+
       ':TA_NIVEL4,:TA_NIVEL5,:CB_CODIGO,:CO_NUMERO, '+
       ':TA_REFEREN,:TA_PERCEPC,:TA_DEDUCCI,:TA_TOTAL, :TA_HORAS,'+
       ':PE_YEAR ,:PE_TIPO ,:PE_NUMERO ) ';
     Q_MAX_ORDEN = 9999;

     {Scripts para Reportes de
     -Balanza de Movimientos
     -Listado de Nomina}
     Q_FILTRO_LISTADO = 'CONCEPTO.CO_LISTADO = ''S''';
     Q_CONCEPTO_SELECT = 'SELECT CO_NUMERO, CO_RECIBO, CO_SUMRECI ' +
                         'FROM CONCEPTO ' +
                         'WHERE ( CONCEPTO.CO_TIPO IN (1,2,3,4,6,7) ) AND ' +
                         '(NOT(CONCEPTO.CO_RECIBO =''''))  %s ';


implementation
uses ZetaCommonTools,
     ZetaCommonLists,
     ZGlobalTress;

const
      //SQL PARA LISTADO DE NOMINA
      Q_TABLA = 'TMPBALA2';
      Q_FIELD_USUARIO = 'TP_USER';

      Q_PERCEPCION = 1;
      Q_DEDUCCION = 2;

      Q_SP_LISTADO_NOMINA = 'EXECUTE PROCEDURE SP_LISTADO_BALANZA ( '+
                             ':USUARIO, '+
                             ':YEAR, '+
                             ':TIPO, '+
                             ':NUMERO, '+
                             ':EMPLEADO, '+
                             ':CONCEPTO, '+
                             ':COLUMNA, '+
                             ':DESCRIPCION, '+
                             ':MONTO, '+
                             ':HORAS, '+
                             ':REFERENCIA, '+
                             ':TP_NIVEL1, '+
                             ':TP_NIVEL2, '+
                             ':TP_NIVEL3, '+
                             ':TP_NIVEL4, '+
                             ':TP_NIVEL5 )';
      Q_LIST_NOM_INSERT = 'INSERT INTO TMPBALA2 ( TP_USER,TP_ORDEN,CB_CODIGO, '+
                          'TP_NIVEL1,TP_NIVEL2,TP_NIVEL3,TP_NIVEL4,TP_NIVEL5, '+
                          'TP_NUM_PER,TP_DES_PER,TP_MON_PER,TP_HOR_PER,TP_REF_PER, '+
                          'TP_NUM_DED,TP_DES_DED,TP_MON_DED,TP_REF_DED,TP_HOR_DED, '+
                          'PE_YEAR,PE_TIPO,PE_NUMERO, '+
                          'TP_TOTAL,TP_EMPLEAD ) '+
                          'VALUES ( :TP_USER,:TP_ORDEN,:CB_CODIGO, '+
                          ':TP_NIVEL1,:TP_NIVEL2,:TP_NIVEL3,:TP_NIVEL4,:TP_NIVEL5, '+
                          ':TP_NUM_PER,:TP_DES_PER,:TP_MON_PER,:TP_HOR_PER,:TP_REF_PER, '+
                          ':TP_NUM_DED,:TP_DES_DED,:TP_MON_DED,:TP_REF_DED,:TP_HOR_DED, '+
                          ':PE_YEAR,:PE_TIPO,:PE_NUMERO, '+
                          ':TP_TOTAL,:TP_EMPLEAD )';
      Q_UPDATE_DATOS_EMPLEADO = 'UPDATE TMPBALA2 SET '+
                                'TP_EMPLEAD=:TP_EMPLEAD  '+
                                'WHERE TP_USER=:TP_USER '+
                                'AND CB_CODIGO=:CB_CODIGO '+
                                'AND TP_ORDEN=:TP_ORDEN ';

      Q_UPDATE_DATOS_EMPLEADO_GRUPO = 'UPDATE TMPBALA2 SET '+
                                'TP_EMPLEAD=:TP_EMPLEAD  '+
                                'WHERE TP_USER=:TP_USER '+
                                'AND CB_CODIGO=:CB_CODIGO '+
                                'AND TP_ORDEN=:TP_ORDEN '+
                                'AND TP_NIVEL1=:TP_NIVEL1 '+
                                'AND TP_NIVEL2=:TP_NIVEL2 '+
                                'AND TP_NIVEL3=:TP_NIVEL3 '+
                                'AND TP_NIVEL4=:TP_NIVEL4 '+
                                'AND TP_NIVEL5=:TP_NIVEL5 ';

      Q_BALANZA_COUNT = 'SELECT COUNT(*) CUANTOS FROM TMPBALA2 '+
                        'WHERE TP_USER=:TP_USER AND '+
                        'CB_CODIGO=:CB_CODIGO %s';

       Q_TOTALES_EMPRESA_X_CONCEPTO=
       'SELECT %s , '+
       'TMPLISTA.CO_NUMERO, '+
       'CONCEPTO.CO_TIPO, '+
       'CONCEPTO.CO_DESCRIP, '+
       'CONCEPTO.CO_CAMBIA, '+
       'SUM(TA_PERCEPC) TA_PERCEPC, SUM(TA_DEDUCCI) TA_DEDUCCI, '+
       'SUM(TA_PERCEPC + TA_DEDUCCI) TA_MONTO, '+
       'SUM(TA_HORAS) TA_HORAS '+
       'FROM TMPLISTA '+
       'LEFT OUTER JOIN CONCEPTO ON CONCEPTO.CO_NUMERO = TMPLISTA.CO_NUMERO '+
       'WHERE TA_USER = %d %s '+
       'GROUP BY %s TMPLISTA.CO_NUMERO,CONCEPTO.CO_TIPO,CONCEPTO.CO_DESCRIP,CONCEPTO.CO_CAMBIA '+
       'ORDER BY %s TMPLISTA.CO_NUMERO ';

       Q_COUNT_EMPLEADOS=
       'SELECT %s , '+
       'COUNT(DISTINCT(CB_CODIGO)) EMPLEADOS, '+
       'SUM(TP_MON_PER) NO_PERCEPC, '+
       'SUM(TP_MON_DED) NO_DEDUCCI, '+
       'SUM(TP_MON_PER)-SUM(TP_MON_DED) NO_NETO '+
       'FROM TMPBALA2 '+
       'WHERE TP_USER = %d AND TP_ORDEN = 9999 '+
       '%s ';


Constructor TListadoNomina.Create( oProvider : TdmZetaServerProvider;
                                   oCreator : TZetaCreator );
begin
     oZetaProvider := oProvider;
     oZetaCreator:= oCreator;
end;

function TListadoNomina.GetFiltroTipoConceptosBalanza: string;
begin
     if oZetaProvider.GetGlobalBooleano( K_GLOBAL_IMPRIMIR_CONCEPTOS_APOYO ) then
        Result := 'CONCEPTO.CO_TIPO IN (1,2,3,4,6,7)'
     else
         Result := 'CONCEPTO.CO_TIPO IN (1,2,3,4)';
end;

procedure TListadoNomina.AgregaConceptos( oBroker : TSQLBroker;
                                             oConceptos: TStrings;
                                             const sFiltro : string );
 var FConcepto : TZetaCursor;
     sRecibo,sConcepto : string;
     i:integer;
begin
     FConcepto := oZetaProvider.CreateQuery( Format( Q_CONCEPTO_SELECT, [' AND (' + sFiltro + ')' ] ));
     with FConcepto do
     begin
          Active := TRUE;

          if NOT EOF then
          begin
               while NOT Eof do
               begin
                    sRecibo := FieldByName( 'CO_RECIBO' ).AsString;
                    if StrVacio( sRecibo ) then sRecibo := '0';
                    sConcepto := 'CONCEPTO'+FieldByName( 'CO_NUMERO' ).AsString;
                    with oBroker.Agente do
                    begin
                         i:=AgregaColumna( sRecibo, FALSE, enNomina, tgFloat, 10, sConcepto );
                         with GetColumna(i) do
                         begin
                              MsgError := Format( 'Error en F�rmula de Recibo en el Concepto #%d',
                                                  [FieldByName( 'CO_NUMERO' ).AsInteger] );
                         end;
                    end;
                    oConceptos.Add(sConcepto);

                    Next;
               end;
          end;
          Active := FALSE;
     end;
end;

procedure TListadoNomina.AsignaNiveles( Destino: TZetaCursor;
                                        Fuente:TDataset;
                                        const sFieldDestino, sFieldFuente: string );
 var i : integer;
     sField: string;
begin
     for i:=1 to Q_MAX_GRUPOS do
     begin
          {if TOEQuery( Destino ).ParamByName(Format(sFieldDestino+'%d',[i])).IsNull then
             Raise EDatabaseError.Create('NO existe ' + Format(sFieldDestino+'%d',[i]) + ' en ' + destino.Name);

          if Fuente.FieldByName(Format(sFieldFuente+'%d',[i])).isNull then
             Raise EDatabaseError.Create('NO existe ' + Format(sFieldFuente+'%d',[i]) + ' en ' + fuente.Name);
          }
          sField := Fuente.FieldByName(Format(sFieldFuente+'%d',[i])).AsString;
          if Length( sField )> K_REP_ANCHO_CODIGO then
             raise Exception.Create( Format( 'Los campos de agrupacion no deben de exceder los %d caracteres' + CR_LF +
                                     'Es necesario cambiar la agrupaci�n #%d, para que el reporte salga correctamente', [K_REP_ANCHO_CODIGO, i] ) );

          oZetaProvider.ParamAsChar( Destino, Format(sFieldDestino+'%d',[i]),
                                     sField, K_REP_ANCHO_CODIGO );
     end;

end;

function TListadoNomina.DatosEmplField(const i: integer): string;
begin
     Result := Format('DatosEmpl%d',[i]);
end;

function TListadoNomina.TP_NIVEL(const i: integer): string;
begin
     Result := Format('TP_NIVEL%d',[i]);
end;

function TListadoNomina.TA_NIVEL(const i: integer): string;
begin
     Result := Format('TA_NIVEL%d',[i]);
end;

function TListadoNomina.TIT_NIVEL(const i: integer): string;
begin
     Result := Format('TIT_NIVEL%d',[i]);
end;

function TListadoNomina.DES_NIVEL(const i: integer): string;
begin
     Result := Format('DES_NIVEL%d',[i]);
end;

procedure TListadoNomina.AgregaRegistros( DataSet : TDataset;
                                          const iColumna,iEmpleado: integer);
begin
     AgregaRegistroTmpLista( DataSet, iEmpleado );
     AgregaRegistroBalanza( DataSet,iColumna,iEmpleado );
end;

function TListadoNomina.GetHorasConcepto( Dataset: TDataset ): TPesos;
 var sConcepto: string;
begin
     with Dataset do
     begin
          sConcepto := 'CONCEPTO'+FieldByName('CO_NUMERO').AsString;
          if Conceptos.IndexOf( sConcepto ) < 0 then
             Result := 0
          else
              Result := DataSet.FieldByName(sConcepto).AsFloat;
     end;
end;

procedure TListadoNomina.AgregaRegistroBalanza( DataSet : TDataset;
                                                const iColumna,iEmpleado : integer );
 var rHoras: TPesos;
begin
     with DataSet do
     begin
          rHoras := GetHorasConcepto(Dataset);
          {$ifdef ANTES}
          sConcepto := 'CONCEPTO'+FieldByName('CO_NUMERO').AsString;
          if Conceptos.IndexOf( sConcepto ) < 0 then
             rHoras := 0
          else
              rHoras := DataSet.FieldByName(sConcepto).AsFloat;
          {$endif}
          EjecutaBalanza( FSp_Balanza, FNomina,
                          'TP_NIVEL',
                          'TP_NIVEL',
                          FieldByName('CO_DESCRIP').AsString,
                          FieldByName('MO_REFEREN').AsString,
                          iEmpleado,
                          FieldByName('CO_NUMERO').AsInteger,
                          iColumna,
                          FieldByName('MO_MONTO').AsFloat,
                          rHoras );
     end;
end;

procedure TListadoNomina.AgregaRegistroTmpLista( DataSet : TDataSet; const iEmpleado : integer );
begin
      with oZetaProvider, Dataset do
      begin

           AsignaNiveles( FInsertTmpLista, FNomina, 'TA_NIVEL', 'TP_NIVEL' );

           ParamAsInteger(FInsertTmpLista,'TA_USER',UsuarioActivo);
           ParamAsInteger(FInsertTmpLista,'PE_YEAR', DatosPeriodo.Year ) ;
           ParamAsInteger(FInsertTmpLista,'PE_TIPO', Ord( DatosPeriodo.Tipo ) );
           ParamAsInteger(FInsertTmpLista,'PE_NUMERO', DatosPeriodo.Numero );
           ParamAsInteger(FInsertTmpLista,'CB_CODIGO',iEmpleado);
           ParamAsInteger(FInsertTmpLista,'CO_NUMERO',FieldByName('CO_NUMERO').AsInteger);
           ParamAsVarChar(FInsertTmpLista,'TA_REFEREN',FieldByName('MO_REFEREN').AsString,8);
           ParamAsFloat(FInsertTmpLista,'TA_TOTAL', 0 );
           ParamAsFloat(FInsertTmpLista,'TA_PERCEPC',FieldByName('MO_PERCEPC').AsFloat);
           ParamAsFloat(FInsertTmpLista,'TA_DEDUCCI',FieldByName('MO_DEDUCCI').AsFloat);
           ParamAsFloat(FInsertTmpLista,'TA_HORAS', GetHorasConcepto(Dataset) );
           Ejecuta(FInsertTmpLista);
      end;
end;

procedure TListadoNomina.InsertBalanza( DataSet: TDataset;
                                        const iEmpleado, iCuantos,iPerConcepto, iDedConcepto : integer;
                                        const sDatosEmpl, sPerDescrip,sDedDescrip,sPerRef,sDedRef : string;
                                        const rPercepcion, rDeduccion, rPerHoras, rDedHoras: TPesos );
begin
     with oZetaProvider do
     begin
          AsignaNiveles( FInsertBalanza, DataSet, 'TP_NIVEL', 'TP_NIVEL' );

          ParamAsInteger( FInsertBalanza, 'TP_USER', UsuarioActivo );
          ParamAsInteger( FInsertBalanza, 'TP_ORDEN', iCuantos );

          //Percepciones
          ParamAsInteger( FInsertBalanza, 'TP_NUM_PER', iPerConcepto);
          ParamAsVarChar( FInsertBalanza, 'TP_DES_PER', sPerDescrip, 30 );
          ParamAsFloat(   FInsertBalanza, 'TP_MON_PER', rPercepcion );
          ParamAsVarChar( FInsertBalanza, 'TP_REF_PER' ,sPerRef, 8 );
          ParamAsFloat(   FInsertBalanza, 'TP_HOR_PER', rPerHoras );

          //Deducciones
          ParamAsInteger( FInsertBalanza, 'TP_NUM_DED', iDedConcepto);
          ParamAsVarChar( FInsertBalanza, 'TP_DES_DED', sDedDescrip, 30 );
          ParamAsFloat(   FInsertBalanza, 'TP_MON_DED', rDeduccion );
          ParamAsVarChar( FInsertBalanza, 'TP_REF_DED' ,sDedRef, 8 );
          ParamAsFloat(   FInsertBalanza, 'TP_HOR_DED', rDedHoras );

          ParamAsVarChar( FInsertBalanza, 'TP_EMPLEAD', sDatosEmpl, 50 );
          ParamAsInteger( FInsertBalanza, 'TP_TOTAL', 0 );

          ParamAsInteger( FInsertBalanza, 'CB_CODIGO', iEmpleado);
          ParamAsInteger( FInsertBalanza, 'PE_YEAR', DatosPeriodo.Year ) ;
          ParamAsInteger( FInsertBalanza, 'PE_TIPO', Ord( DatosPeriodo.Tipo ) );
          ParamAsInteger( FInsertBalanza, 'PE_NUMERO', DatosPeriodo.Numero );
          Ejecuta( FInsertBalanza );
     end;
end;

procedure TListadoNomina.AgregaRegistroNomina( DataSet: TDataset;
                                               const iEmpleado{, iCuantos} : integer );
 var i: integer;
begin
     with oZetaProvider do
     begin
          InsertBalanza( DataSet,
                         iEmpleado, 0, 0, 0,
                         '','','','','',
                         0,0,0,0 );
     end;

     for i:= 0 to FCountDatosEmpleado - 1 do
         oListaDatosEmpleados.Add( Dataset.FieldByName(DatosEmplField(i)).AsString);

     //ActualizaDatosEmpleado( Dataset, FNomina, iEmpleado );

end;

procedure TListadoNomina.EjecutaBalanza( oBalanza:TZetaCursor;
                                         DataSet: TDataset;
                                         const sNivelSource, sNivelDestino, sDescrip, sReferencia : string;
                                         const iEmpleado, iConcepto, iColumna: integer;
                                         const rMonto, rHoras : TPesos );
begin
     with oZetaProvider, DataSet do
     begin
          AsignaNiveles( oBalanza, Dataset, sNivelSource, sNivelDestino );

          ParamAsInteger( oBalanza, 'USUARIO', UsuarioActivo );
          ParamAsInteger( oBalanza, 'YEAR', DatosPeriodo.Year );
          ParamAsInteger( oBalanza, 'TIPO', Ord( DatosPeriodo.Tipo ) );
          ParamAsInteger( oBalanza, 'NUMERO', DatosPeriodo.Numero );
          ParamAsInteger( oBalanza, 'EMPLEADO', iEmpleado );
          ParamAsInteger( oBalanza, 'CONCEPTO', iConcepto );
          ParamAsInteger( oBalanza, 'COLUMNA', iColumna );
          ParamAsChar( oBalanza, 'DESCRIPCION', sDescrip, 30 );
          ParamAsVarChar( oBalanza, 'REFERENCIA', sReferencia, 30 );
          ParamAsFloat( oBalanza, 'MONTO', rMonto );
          ParamAsFloat( oBalanza, 'HORAS', rHoras );
          Ejecuta(oBalanza);
     end;
end;

function taNivel( const sTexto : string ) : string;
begin
     Result := StrTransAll(sTexto, 'TP_NIVEL', 'TA_NIVEL');
end;

function tpNivel( const sTexto : string ) : string;
begin
     Result := StrTransAll(sTexto, 'TA_NIVEL', 'TP_NIVEL');
end;

procedure TListadoNomina.AgregaTotalesEmpresa;

 function GetDescripcion( const iNivel : integer;
                          const sNivel: string): string;
 begin
      with FNomina do
           if Locate(TP_NIVEL(iNivel), VarArrayOf([sNivel]),[]) then
              Result := FieldByName( DES_NIVEL(iNivel) ).AsString
           else Result := '';
 end;

 var FTotales, FRegistros: TZetaCursorLocate;
     iCountEmpleados, j, i,k, iGrupos: integer;
     sNivel,sFiltro, sGrupos, sGruposSelect : string;
     aNiveles: TArreglo;
begin
     with oZetaProvider do
     begin
          FTotales := CreateQueryLocate;
          FRegistros := CreateQueryLocate;
          oZetaProvider.PreparaQuery( FUpdateDatosEmpl, Q_UPDATE_DATOS_EMPLEADO_GRUPO );
     end;
     try

        {********** TOTALES DE GRUPOS ***********}

        iGrupos := oZetaProvider.ParamList.ParamByName('CountGrupos').AsInteger;

        if iGrupos > 0  then
        begin
             sGrupos := '';

             for i:= 1 to iGrupos do
             begin
                  sGrupos := ConcatString( sGrupos, TP_NIVEL(i), ',' );
                  sGruposSelect := '';
                  for j:= i+1 to Q_MAX_GRUPOS do
                  begin
                       sGruposSelect := ConcatString( sGruposSelect, Comillas(Q_GRUPOS) + ' '+ TP_NIVEL(j), ',' );
                       aNiveles[j] := Q_GRUPOS;
                  end;


                  oZetaProvider.PreparaQuery( fRegistros, Format( Q_COUNT_EMPLEADOS,
                                                                  [ sGrupos + ',' +
                                                                    sGruposSelect,
                                                                    oZetaProvider.UsuarioActivo,
                                                                    'GROUP BY ' + sGrupos]) );

                  fRegistros.Active := TRUE;

                  while NOT fRegistros.EOF do
                  begin
                       if (fRegistros.FieldByName(TP_NIVEL(i)).AsString <> Q_GRUPOS) then
                       begin
                            iCountEmpleados := fRegistros.FieldByName('EMPLEADOS').AsInteger;
                            if ( FSoloTotales ) or
                               ( iCountEmpleados > 1 ) then
                            begin
                                 sNivel := fRegistros.FieldByName(TP_NIVEL(i)).AsString;
                                 oListaDatosEmpleados.Clear;
                                 oListaDatosEmpleados.Add( 'Total de ' + oZetaProvider.ParamList.ParamByName(TIT_NIVEL(i)).AsString + ':' + sNivel );
                                 oListaDatosEmpleados.Add( GetDescripcion(i,sNivel) );
                                 oListaDatosEmpleados.Add( Format('Empleados: %d', [iCountEmpleados]));
                                 sFiltro := '';
                                 for k:=1 to i do
                                 begin
                                      sFiltro := ConcatFiltros(sFiltro,TP_NIVEL(k) + ' = ' + Comillas(fRegistros.FieldByName(TP_NIVEL(k)).AsString));
                                      aNiveles[k] := fRegistros.FieldByName(TP_NIVEL(k)).AsString;
                                 end;
                                 with oZetaProvider do
                                 begin
                                      PreparaQuery( FTotales, Format( Q_TOTALES_EMPRESA_X_CONCEPTO,
                                                                      [ taNivel(sGrupos) + ',' + taNivel(sGruposSelect),
                                                                        UsuarioActivo,
                                                                        'AND ' + taNivel(sFiltro),
                                                                        tanivel(sGrupos)+ ',',
                                                                        tanivel(sGrupos)+ ','] ) );
                                 end;


                                 FTotales.Active := TRUE;
                                 BalanceaTotales(FTotales,FRegistros,'TP_NIVEL','TA_NIVEL',aNiveles);
                                 FTotales.Active := FALSE;
                            end;
                       end;
                       fRegistros.Next;
                  end;
             end;
        end;

        { ******************* TOTALES DE EMPRESA ****************** }
        sGrupos := '';
        for i:= 1 to Q_MAX_GRUPOS do
        begin
             sGrupos := ConcatString( sGrupos, Comillas(Q_EMPRESA) + ' ' + TP_NIVEL(i), ',' );
             aNiveles[i] := Q_EMPRESA;
        end;

        with oZetaProvider do
        begin
             PreparaQuery( fRegistros, Format( Q_COUNT_EMPLEADOS,
                                               [sGrupos, UsuarioActivo, Format( 'AND CB_CODIGO <> %d',[Q_IMAX_EMPLEADO]) ]) );
             PreparaQuery( FTotales, Format( Q_TOTALES_EMPRESA_X_CONCEPTO,
                                             [ sGrupos,UsuarioActivo, '', '', ''] ) );

        end;
        fRegistros.Active := TRUE;

        oListaDatosEmpleados.Clear;
        oListaDatosEmpleados.Add( '***GRAN  TOTAL***' );
        oListaDatosEmpleados.Add( '  E M P R E S A  ' );
        oListaDatosEmpleados.Add( Format('Total de Empleados: %d', [fRegistros.FieldByName('EMPLEADOS').AsInteger]));

        FTotales.Active := TRUE;
        BalanceaTotales(FTotales,FRegistros,'TP_NIVEL','TP_NIVEL',aNiveles);
        FTotales.Active := FALSE;

     finally
            FreeAndNil( FRegistros );
            FreeAndNil( FTotales );
     end;
end;

procedure TListadoNomina.BalanceaTotales( oTotales,oRegistros : TDataSet;
                                          const sFieldDestino, sFieldSource : string;
                                          const Niveles:TArreglo );
 var iColumna : integer;
     rMonto : Double;
begin
     oZetaProvider.EmpiezaTransaccion;
     try
        with oTotales do
        begin
             while ( NOT EOF ) do
             begin
                  //if ( FieldByName('TA_PERCEPC').AsFloat > 0 ) then
                  if ( eTipoConcepto(FieldByName('CO_TIPO').AsInteger) in [coPercepcion,coPrestacion,coCaptura] ) then
                     iColumna := Q_PERCEPCION
                  else iColumna := Q_DEDUCCION;

                  rMonto := FieldByName('TA_MONTO').AsFloat;

                  if  zStrToBool( FieldByName('CO_CAMBIA').AsString ) and
                      ( FieldByName('TA_MONTO').AsFloat < 0 ) then
                  begin
                       if ( iColumna = Q_PERCEPCION ) then iColumna := Q_DEDUCCION
                       else iColumna := Q_PERCEPCION;

                       rMonto := rMonto * -1;
                  end;

                  EjecutaBalanza( FSP_Balanza, oTotales,
                                  sFieldDestino, sFieldSource,
                                  FieldByName('CO_DESCRIP').AsString, '',
                                  Q_IMAX_EMPLEADO,
                                  FieldByName('CO_NUMERO').AsInteger, iColumna,
                                  rMonto,
                                  FieldByName('TA_HORAS').AsFloat );

                  Next;
             end;//while
        end;

        ActualizaDatosEmpleadoGrupo( oRegistros, oTotales, Q_IMAX_EMPLEADO, Niveles );
     finally
            oZetaProvider.TerminaTransaccion( TRUE );
     end;
end;

procedure TListadoNomina.ActualizaDatosEmpleadoGrupo( Dataset, oTotales: TDataset; const iEmpleado : integer;
                                                      //const Niveles: array of string;
                                                      const Niveles : TArreglo;
                                                      const sFiltro : string = '' );
 var i: integer;
begin
     for i:=1 to Q_MAX_GRUPOS do
         oZetaProvider.ParamAsVarChar( FUpdateDatosEmpl, TP_NIVEL(i), Niveles[i], K_REP_ANCHO_CODIGO );
     ActualizaDatosEmpleado( Dataset, oTotales, iEmpleado, sFiltro );
end;

procedure TListadoNomina.ActualizaDatosEmpleado( Dataset, oTotales:TDataset;
                                                 const iEmpleado : integer;
                                                 const sFiltro : string );
 var i,iCountRegistros : integer;
begin
     with oZetaProvider do
     begin
          PreparaQuery( fCountRegistros, Format( Q_BALANZA_COUNT, [sFiltro] ) );
          ParamAsInteger(fCountRegistros,'TP_USER',UsuarioActivo);
          ParamAsInteger(fCountRegistros,'CB_CODIGO',iEmpleado);
          fCountRegistros.Active := TRUE;
          iCountRegistros := fCountRegistros.Fields[0].AsInteger;
          fCountRegistros.Active := FALSE;

          for i:=0 to iMin(oListaDatosEmpleados.Count, iCountRegistros)-1 do
          begin
               ParamAsInteger(FUpdateDatosEmpl,'TP_USER',UsuarioActivo);
               ParamAsInteger(FUpdateDatosEmpl,'CB_CODIGO',iEmpleado);
               ParamAsInteger(FUpdateDatosEmpl,'TP_ORDEN',i);
               ParamAsVarChar(FUpdateDatosEmpl,'TP_EMPLEAD', oListaDatosEmpleados[i], 50 );
               Ejecuta(FUpdateDatosEmpl);
          end;

          if oListaDatosEmpleados.Count > iCountRegistros then
             for i:=iCountRegistros to oListaDatosEmpleados.Count-1 do
             begin
                  InsertBalanza( DataSet,
                                 iEmpleado, i,0,0,
                                 oListaDatosEmpleados[i], '','','','',
                                 0,0,0,0 );
             end;
             with Dataset do
                  InsertBalanza( Dataset,
                                 iEmpleado, Q_MAX_ORDEN,
                                 0,0,
                                 Format( 'NETO: %s', [FormatFloat('#,0.00', FieldByName('NO_NETO').AsFloat)]),
                                 'PERCEPCIONES','DEDUCCIONES','','',
                                 FieldByName('NO_PERCEPC').AsFloat,
                                 FieldByName('NO_DEDUCCI').AsFloat,
                                 0,0 );

     end;
end;


procedure TListadoNomina.InitBroker;
begin
     if NOT Assigned(oSQLBroker) then
        oSQLBroker := ZetaSQLBroker.GetSQLBroker( oZetaCreator );
end;

procedure TListadoNomina.ClearBroker;
begin
     ZetaSQLBroker.FreeSQLBroker( oSQLBroker );
end;

procedure TListadoNomina.Prepara;
begin
     Conceptos := TStringlist.Create;
     oListaDatosEmpleados := TStringList.Create;

     with oZetaProvider do
     begin
          fCountRegistros := CreateQuery;
          FSP_Balanza := CreateQuery( Q_SP_LISTADO_NOMINA );
          FInsertBalanza := CreateQuery( Q_LIST_NOM_INSERT );
          FInsertTmpLista := CreateQuery( Q_TOTALES_INSERT_LISTA );
          FUpdateDatosEmpl := CreateQuery( Q_UPDATE_DATOS_EMPLEADO );
     end;

     BorraTabla(Q_TABLA,Q_FIELD_USUARIO);
     BorraTabla('TMPLISTA','TA_USER');

end;

procedure TListadoNomina.DesPrepara;
begin
     FreeAndNil( fCountRegistros );
     FreeAndNil( FUpdateDatosEmpl );
     FreeAndNil( FInsertTmpLista );
     FreeAndNil( FInsertBalanza );
     FreeAndNil( FSP_Balanza );
     FreeAndNil( oListaDatosEmpleados );
     FreeAndNil( Conceptos );
end;

procedure TListadoNomina.AgregaTA_NIVELES( oAgente : TSQLAgente;
                                           const oEntidad : TipoEntidad );
 var i: integer;
begin
     {Aqui se agregan los criterios por los que se esta agrupando el reporte.
     Tener en cuenta que si agruparon por EMPLEADO (CB_CODIGO) o por
     CONCEPTO (CO_NUMERO), no se agregaron a esta lista}
     with oZetaProvider.ParamList do
          for i:= 1 to Q_MAX_GRUPOS do
              oAgente.AgregaColumna( StrDef( ParamByName(TA_NIVEL(i)).AsString,EntreComillas(Q_CHR_RELLENO)), False,
                                     oEntidad, tgTexto, 30, TP_NIVEL(i) );
end;

procedure TListadoNomina.AgregaDES_NIVELES( oAgente : TSQLAgente;
                                           const oEntidad : TipoEntidad );
 var i: integer;
begin
     with oZetaProvider.ParamList do
          for i:= 1 to Q_MAX_GRUPOS do
              oAgente.AgregaColumna( ParamByName(DES_NIVEL(i)).AsString, False,
                                     oEntidad, tgTexto, 100, DES_NIVEL(i) );
end;

procedure TListadoNomina.AgregaDatosEmpleados( oAgente : TSQLAgente;
                                               const oEntidad : TipoEntidad;
                                               const sTabla : string );
 var i: integer;
begin
     {Aqui se agregan las formulas que el usuario indic� que quiere que aparezcan a la derecha
     del Listado de Nomina}
     with oZetaProvider.ParamList do
          for i:= 0 to ParamByName('CountDatosEmpleado').AsInteger - 1 do
              oAgente.AgregaColumna( StrTransAll( ParamByName(DatosEmplField(i)).AsString, Q_TABLA, sTabla),False,
                                     oEntidad, tgTexto, 100, DatosEmplField(i) );

end;

procedure TListadoNomina.AgregaDatosPeriodo( oAgente : TSQLAgente;
                                             const oEntidad : TipoEntidad;
                                             const sTabla : string );
begin
     with oZetaProvider.DatosPeriodo do
     begin
          oAgente.AgregaFiltro( Format( sTabla + '.PE_YEAR = %d', [Year] ) , TRUE, oEntidad );
          oAgente.AgregaFiltro( Format( sTabla + '.PE_TIPO = %d', [Ord(Tipo)] ) , TRUE, oEntidad );
          oAgente.AgregaFiltro( Format( sTabla + '.PE_NUMERO = %d', [Numero] ) , TRUE, oEntidad );
     end;
end;

procedure TListadoNomina.PreparaBrokerNomina;
 var i: integer;
begin
     InitBroker;
     SQLBroker.Init(enNomina);
     with SQLBroker.Agente do
     begin
          AgregaColumna( 'NOMINA.CB_CODIGO', True, Entidad, tgNumero, 10, 'CB_CODIGO' );
          AgregaColumna( 'NOMINA.NO_PERCEPC', True, Entidad, tgFloat, 10, 'NO_PERCEPC' );
          AgregaColumna( 'NOMINA.NO_DEDUCCI', True, Entidad, tgFloat, 10, 'NO_DEDUCCI' );
          AgregaColumna( 'NOMINA.NO_NETO', True, Entidad, tgFloat, 10, 'NO_NETO' );

          AgregaTA_NIVELES(SQLBroker.Agente,enNomina);
          AgregaDES_NIVELES(SQLBroker.Agente,enNomina);
          AgregaDatosEmpleados(SQLBroker.Agente,enNomina,'NOMINA');
          AgregaDatosPeriodo(SQLBroker.Agente,enNomina,'NOMINA');

          AgregaFiltro( 'NOMINA.NO_FUERA = ''N''' , TRUE, enNomina );
     end;

     for i:=0 to fSQLAgente.NumFiltros-1 do
         with fSQLAgente.GetFiltro(i) do
         begin
              Formula := StrTransAll( fSQLAgente.GetFiltro(i).Formula, Q_TABLA,'NOMINA');
              if ( Entidad = enListadoNomina ) then
                 Entidad := enNomina;
         end;

     SQLBroker.Agente.AsignaFiltros( fSQLAgente );
     SQLBroker.BuildDataSet(oZetaProvider.EmpresaActiva);
end;

procedure TListadoNomina.PreparaBrokerMovimien;
begin
     MovimienBroker := TSQLBroker.Create(oZetaCreator);
     MovimienBroker.Init(enMovimien);

     with MovimienBroker.Agente do
     begin
          AgregaColumna( 'MOVIMIEN.CB_CODIGO', True, enMovimien, tgNumero, 10, 'CB_CODIGO' );
          AgregaColumna( 'MOVIMIEN.MO_PERCEPC ', True, enMovimien, tgFloat, 10, 'MO_PERCEPC');
          AgregaColumna( 'MOVIMIEN.MO_DEDUCCI ', True, enMovimien, tgFloat, 10, 'MO_DEDUCCI');
          AgregaColumna( 'MOVIMIEN.MO_PERCEPC + MOVIMIEN.MO_DEDUCCI ', True, enMovimien, tgFloat, 10, 'MO_MONTO');
          AgregaColumna( 'MOVIMIEN.MO_REFEREN', True, enMovimien, tgTexto, 8, 'MO_REFEREN' );
          AgregaColumna( 'CONCEPTO.CO_NUMERO', True, enConcepto, tgNumero, 10, 'CO_NUMERO' );
          AgregaColumna( 'CONCEPTO.CO_DESCRIP', True, enConcepto, tgTexto, 30, 'CO_DESCRIP' );
          AgregaColumna( 'CONCEPTO.CO_TIPO', True, enConcepto, tgNumero, 10, 'CO_TIPO' );
          //Temporal - por que no se genera el join hacia nomina
          AgregaColumna( 'NOMINA.CB_CODIGO', True, enNomina, tgNumero, 10  );
          AgregaColumna( 'CONCEPTO.CO_CAMBIA', True, enConcepto, tgTexto, 1, 'CO_CAMBIA' );
          AgregaConceptos(MovimienBroker, Conceptos, Q_FILTRO_LISTADO);

          //AgregaTA_NIVELES(MovimienBroker.Agente,enMovimien);
          //AgregaDatosEmpleados(MovimienBroker.Agente,enMovimien,'MOVIMIEN');
          AgregaDatosPeriodo(MovimienBroker.Agente,enMovimien,'MOVIMIEN');

          AgregaFiltro( 'MOVIMIEN.CB_CODIGO = :EMPLEADO', TRUE, enMovimien );
          AgregaFiltro( 'MOVIMIEN.MO_ACTIVO = ''S''', TRUE, enMovimien );
          AgregaFiltro( 'MOVIMIEN.MO_PERCEPC+MOVIMIEN.MO_DEDUCCI <> 0', TRUE, enMovimien );
          AgregaFiltro( GetFiltroTipoConceptosBalanza , TRUE, enMovimien );
          AgregaFiltro( Q_FILTRO_LISTADO , TRUE, enMovimien );

          AgregaOrden( 'MOVIMIEN.CB_CODIGO', TRUE, enMovimien );
          AgregaOrden( 'MOVIMIEN.CO_NUMERO', TRUE, enMovimien );
     end;
end;

function TListadoNomina.AgregaDetalleBalanza : Boolean;
 var {iCuantos,} iEmpleado, iNomEmpleado, iLado : integer;
begin
     Result := TRUE;
     if MovimienBroker.SuperSQL.Construye( MovimienBroker.Agente ) then
     begin
        if MovimienBroker.SuperReporte.Prepara( MovimienBroker.Agente ) then
        begin
             //iCuantos :=0;
             FNomina := SQLBroker.SuperReporte.DataSetReporte;

             //--oZetaProvider.EmpiezaTransaccion;
             try
                while NOT FNomina.EOF do
                begin
                     iNomEmpleado := FNomina.FieldByName('CB_CODIGO').AsInteger;
                     iEmpleado := iNomEmpleado;

                     {FMovimien, se utiliza nada mas para parametrizar el query,
                     el query que se barre es el DatasetReporte}
                     FMovimien := MovimienBroker.SuperReporte.QueryReporte;
                     FMovimien.Close;
                     oZetaProvider.ParamAsInteger( FMovimien, 'Empleado', iNomEmpleado );

                     oListaDatosEmpleados.Clear;
                     oZetaProvider.EmpiezaTransaccion;//--//
                     with MovimienBroker.SuperReporte do
                     begin
                          if AbreDataSet then
                          begin
                               while ( NOT DatasetReporte.EOF ) do
                               begin
                                    if ( eTipoConcepto(DatasetReporte.FieldByName('CO_TIPO').AsInteger) in [coPercepcion,coPrestacion, coCaptura] ) then
                                    begin
                                         iLado := Q_PERCEPCION;
                                    end
                                    else
                                        iLado := Q_DEDUCCION;

                                    if  zStrToBool( DatasetReporte.FieldByName('CO_CAMBIA').AsString ) and
                                        ( DatasetReporte.FieldByName('MO_MONTO').AsFloat < 0 ) then
                                    begin
                                         if ( iLado = Q_PERCEPCION ) then iLado := Q_DEDUCCION
                                         else iLado := Q_PERCEPCION;

                                         DatasetReporte.Edit;
                                         DatasetReporte.FieldByName('MO_MONTO').AsFloat := MovimienBroker.SuperReporte.DatasetReporte.FieldByName('MO_MONTO').AsFloat * -1;
                                         DatasetReporte.Post;
                                    end;

                                    AgregaRegistros( DatasetReporte, iLado, iEmpleado );

                                    if oListaDatosEmpleados.Count < fCountDatosEmpleado then
                                       oListaDatosEmpleados.Add(FNomina.FieldByName(DatosEmplField(oListaDatosEmpleados.Count)).AsString);

                                    DatasetReporte.Next;
                               end;//while
                          end//if
                          else
                          begin
                               AgregaRegistroNomina( FNomina, iEmpleado {,iCuantos} );
                               //inc(iCuantos);
                          end;//else
                     end; // with

                     ActualizaDatosEmpleado(FNomina, FNomina, iEmpleado);
                     oZetaProvider.TerminaTransaccion(True);   //--//
                     FNomina.Next;
                end;//while
                //--oZetaProvider.TerminaTransaccion(True);
             except
                   On Error : Exception do
                   begin
                        oZetaProvider.TerminaTransaccion(False);
                        raise ;
                   end;
             end;
        end
        else
        begin
             raise Exception.Create(MovimienBroker.Agente.ListaErrores.Text)
        end;
     end
     else
     begin
          raise Exception.Create(MovimienBroker.Agente.ListaErrores.Text);
     end;
end;

function TListadoNomina.Calcula(oSQLAgente: TSQLAgente; var Error: string): TSQLAgente;
  var i, iColumna: integer;

begin //.CalculaListadoNomina
     Result := NIL;

     Prepara;

     try
        FSQLAgente := oSQLAgente;


        with oZetaProvider.ParamList do
        begin
             FCountDatosEmpleado := ParamByName('CountDatosEmpleado').AsInteger;
             FSoloTotales := ParamByName('SoloTotales').AsBoolean;
             FTotalizarNeto := ParamByName('TotalesNeto').AsBoolean;
        end;

        PreparaBrokerNomina;
        PreparaBrokerMovimien;
        try
           if AgregaDetalleBalanza then
           begin
                AgregaTotalesEmpresa;

                with oZetaProvider do
                begin
                     if FSoloTotales then
                        EjecutaAndFree( Format('DELETE FROM TMPBALA2 WHERE TP_USER= %d AND CB_CODIGO < %d',
                                        [UsuarioActivo,Q_iMAX_EMPLEADO]));
                     if NOT FTotalizarNeto then
                        EjecutaAndFree( Format('DELETE FROM TMPBALA2 WHERE TP_USER= %d AND TP_ORDEN = %d',
                                        [UsuarioActivo,Q_MAX_ORDEN]));
                end;

                Result := TSQLAgente.Create;
                Result.Entidad := enListadoNomina;
                   with oSQLAgente do
                        for i := 0 to NumColumnas - 1 do
                            with GetColumna(i) do
                            begin
                                 iColumna := Result.AgregaColumna( Formula,
                                                                   DeDiccionario,
                                                                   Entidad,
                                                                   TipoFormula,
                                                                   Ancho,
                                                                   Alias,
                                                                   Totalizacion,
                                                                   GroupExp );
                                 Result.GetColumna(iColumna).ValorConstante := ValorConstante;

                            end;
                   Result.AgregaFiltro( Format(Q_TABLA+'.TP_USER=%d',[oZetaProvider.UsuarioActivo]),TRUE,enListadoNomina );
                   //Result.AsignaOrdenes( oSQLAgente );
                   Result.AgregaOrden( Q_TABLA+'.TP_NIVEL1',TRUE,enListadoNomina );
                   Result.AgregaOrden( Q_TABLA+'.TP_NIVEL2',TRUE,enListadoNomina );
                   Result.AgregaOrden( Q_TABLA+'.TP_NIVEL3',TRUE,enListadoNomina );
                   Result.AgregaOrden( Q_TABLA+'.TP_NIVEL4',TRUE,enListadoNomina );
                   Result.AgregaOrden( Q_TABLA+'.TP_NIVEL5',TRUE,enListadoNomina );
                   Result.AgregaOrden( Q_TABLA+'.CB_CODIGO',TRUE,enListadoNomina );
                   Result.AgregaOrden( Q_TABLA+'.TP_ORDEN',TRUE,enListadoNomina );
           end
        finally
               FreeAndNil(MovimienBroker);
               ClearBroker; //Destruye el Broker que se usa para la nomina
        end;
     finally
            Desprepara;
     end;
end;

procedure TListadoNomina.BorraTabla( const sTabla, sUserCampo : string );
 var FBorra : TZetaCursor;
begin
     with oZetaProvider do
     begin
          FBorra := CreateQuery( Format(Q_DELETE_TABLA, [sTabla,sUserCampo,UsuarioActivo]) );
          try
             EmpiezaTransaccion;
             try
                Ejecuta(FBorra);
                TerminaTransaccion(True);
             except
                   TerminaTransaccion(False);
             end;
          finally
                 FreeAndNil(FBorra);
          end;
     end;
end;

end.






