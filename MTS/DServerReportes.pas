unit DServerReportes;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.7 / XE3                           ::
  :: Unidad:      DServerReportes.pas                        ::
  :: Descripci�n: Programa principal de Reportes.dll         ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ComServ, ComObj, VCLCom, StdVcl, DataBkr,
  DBClient, MtsRdm, Mtx, Db,
  Variants,
  {$IFNDEF DOS_CAPAS}
  Reportes_TLB,
  {$ENDIF}
  ZSuperSQL, ZAgenteSQL, ZCreator, ZetaSQLBroker, DZetaServerProvider, DSuperReporte, ZetaServerDataSet;

type
  TdmServerReportes = class(TMtsDataModule {$IFNDEF DOS_CAPAS}, IdmServerReportes {$ENDIF})
    cdsTemporal: TServerDataSet;
    procedure dmServerReportesCreate(Sender: TObject);
    procedure dmServerReportesDestroy(Sender: TObject);
  private
    { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
    {$IFNDEF DOS_CAPAS}
    oZetaCreator: TZetaCreator;
    {$ENDIF}
    oSuperSQL: TSuperSQL;
    FReporte : Integer;
    FAlta    : Boolean;
    FPosicion: Integer;
    function GetMaxReporte: Integer;
    procedure SetTablaInfoCampoRep;
    procedure SetTablaInfoReporte;
    procedure BeforeUpdatePoliza(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure UpdateDataPoliza(Sender: TObject; DataSet: TzProviderClientDataSet);
    procedure BeforeUpdateReporte(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateCampoRep(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeReporteUpdatePoliza(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
  protected
    { Protected declarations }
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    {$IFDEF DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
    {$ENDIF}
    function GetReportes(Empresa: OleVariant; iClasifActivo, iFavoritos: Integer; iSuscripciones: Integer;
      const sClasificaciones: WideString; lVerConfidencial: WordBool): OleVariant; {$IFNDEF DOS_CAPAS} safecall;
    {$ENDIF}
    function GetEditReportes(Empresa: OleVariant; iReporte: Integer; var CampoRep: OleVariant): OleVariant;
    {$IFNDEF DOS_CAPAS} safecall; {$ENDIF}
    function GetLookUpReportes(Empresa: OleVariant; lVerConfidencial: WordBool): OleVariant;
    {$IFNDEF DOS_CAPAS} safecall; {$ENDIF}
    function GrabaReporte(Empresa, oDelta, oCampoRep: OleVariant; out ErrorCount: Integer; var iReporte: Integer)
      : OleVariant; {$IFNDEF DOS_CAPAS} safecall; {$ENDIF}
    function GetEscogeReporte(Empresa: OleVariant; Entidad, Tipo: Integer; lVerConfidencial: WordBool): OleVariant;
    {$IFNDEF DOS_CAPAS} safecall; {$ENDIF}
    function GetEscogeReportes(Empresa: OleVariant; const Entidades, Tipos: WideString; lVerConfidencial: WordBool)
      : OleVariant; {$IFNDEF DOS_CAPAS} safecall; {$ENDIF}
    function CampoRepFiltros(Empresa: OleVariant; Reporte: Integer): OleVariant;
    {$IFNDEF DOS_CAPAS} safecall; {$ENDIF}
    function BorraReporte(Empresa: OleVariant; iReporte: Integer): OleVariant;
    {$IFNDEF DOS_CAPAS} safecall; {$ENDIF}
    function GrabaPoliza(Empresa, Delta, CampoRep: OleVariant; out ErrorCount: Integer; var iReporte: Integer)
      : OleVariant; {$IFNDEF DOS_CAPAS} safecall; {$ENDIF}
    function GetSuscripcion(Empresa: OleVariant; iFrecuencia: Integer; const sUsuarios, sReportes: WideString;
      var Usuarios: OleVariant): OleVariant;
    {$IFNDEF DOS_CAPAS} safecall; {$ENDIF}
    function BorraFavoritos(Empresa: OleVariant; iReporte: Integer): OleVariant;
    {$IFNDEF DOS_CAPAS} safecall; {$ENDIF}
    function AgregaFavoritos(Empresa: OleVariant; iReporte: Integer): Integer;
    {$IFNDEF DOS_CAPAS} safecall; {$ENDIF}
    function GetPlantilla(const Nombre: WideString): OleVariant; {$IFNDEF DOS_CAPAS} safecall;
    {$ENDIF}
  end;

var
  dmServerReportes: TdmServerReportes;

implementation

uses
  ZetaSQL, ZetaCommonLists, ZetaCommonTools, ZetaCommonClasses, ZetaServerTools, ZReportConst, ZFuncsReporte,
  ZFuncsGenerales, ZGlobalTress, DEntidadesTress;

{$R *.DFM}

const
  K_DELETE_FAVORITOS = 'delete FROM MISREPOR WHERE ( RE_CODIGO = %d ) and ( US_CODIGO = %d )';

  { ---------------------------------------------------------------- }

class procedure TdmServerReportes.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;

end;

{ ***************** PARTE PRIVADA ******************************* }

procedure TdmServerReportes.SetTablaInfoReporte;
begin
  with oZetaProvider do begin
    TablaInfo.SetInfo('REPORTE', 'RE_CODIGO,RE_NOMBRE,RE_TIPO,RE_TITULO,RE_ENTIDAD,' +
        'RE_SOLOT,RE_FECHA,RE_GENERAL,RE_REPORTE,RE_CFECHA,' + 'RE_HOJA,RE_ALTO,RE_ANCHO,RE_PRINTER,RE_COPIAS,' +
        'RE_PFILE,RE_ARCHIVO,RE_VERTICA,US_CODIGO,RE_FILTRO,' + 'RE_COLNUM,RE_COLESPA,RE_RENESPA,RE_MAR_SUP,RE_MAR_IZQ,'
        + 'RE_MAR_DER,RE_MAR_INF,RE_NIVEL,RE_FONTNAM,RE_FONTSIZ,' + 'QU_CODIGO,RE_CLASIFI,RE_CANDADO,RE_IFECHA',
      'RE_CODIGO');
  end;
end;

procedure TdmServerReportes.SetTablaInfoCampoRep;
begin
  with oZetaProvider do begin
    TablaInfo.SetInfo('CAMPOREP',
      'RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA',
      'RE_CODIGO,CR_TIPO,CR_POSICIO,CR_SUBPOS');
  end;
end;

{ ---------------------------------------------------------------- }

procedure TdmServerReportes.dmServerReportesCreate(Sender: TObject);
begin
  oZetaProvider := DZetaServerProvider.GetZetaProvider(Self);
end;

procedure TdmServerReportes.dmServerReportesDestroy(Sender: TObject);
begin
  if Assigned(oSuperSQL) then
    FreeAndNil(oSuperSQL);
  ZetaSQLBroker.FreeZetaCreator(oZetaCreator);
  DZetaServerProvider.FreeZetaProvider(oZetaProvider);
end;

{$IFDEF DOS_CAPAS}

procedure TdmServerReportes.CierraEmpresa;
begin
end;
{$ENDIF}

function TdmServerReportes.GetReportes(Empresa: OleVariant; iClasifActivo, iFavoritos: Integer; iSuscripciones: Integer;
  const sClasificaciones: WideString; lVerConfidencial: WordBool): OleVariant;
const
  Q_GET_REPORTE = 'select ' +
    'SUSCRIP.US_CODIGO US_SUSCRITO, MISREPOR.US_CODIGO US_FAVORITO, REPORTE.RE_CODIGO,RE_NOMBRE,RE_TIPO,RE_FECHA, ' +
    'REPORTE.US_CODIGO,RE_ENTIDAD,RE_REPORTE,RE_CANDADO,RE_CLASIFI ' + 'from REPORTE ' +
    'left outer join DICCION on DICCION.DI_CLASIFI = -1 AND DICCION.DI_CALC = REPORTE.RE_ENTIDAD ' +
    'LEFT OUTER join SUSCRIP on SUSCRIP.RE_CODIGO = REPORTE.RE_CODIGO and SUSCRIP.US_CODIGO = %d' +
    'LEFT OUTER join MISREPOR on MISREPOR.RE_CODIGO = REPORTE.RE_CODIGO and MISREPOR.US_CODIGO = %d' + 'where %s %s ' +
    'order by RE_NOMBRE ';

  function GetFiltro: string;
  const
    K_FILTRO_FAVORITOS =
      'REPORTE.RE_CODIGO in( select MISREPOR.RE_CODIGO from MISREPOR where MISREPOR.US_CODIGO = %d ) %s';
    K_FILTRO_SUSCRIP = 'REPORTE.RE_CODIGO in( select SUSCRIP.RE_CODIGO from SUSCRIP where SUSCRIP.US_CODIGO = %d ) %s';
    K_CLASIFICACIONES = 'and RE_CLASIFI in ( %s )';
  var
    sFiltroClasificaciones: string;
  begin
    sFiltroClasificaciones := VACIO;
    if strLleno(sClasificaciones) then
      sFiltroClasificaciones := Format(K_CLASIFICACIONES, [sClasificaciones]);

    if (iClasifActivo = iFavoritos) then begin
      Result := Format(K_FILTRO_FAVORITOS, [oZetaProvider.UsuarioActivo, sFiltroClasificaciones]);
    end else if (iClasifActivo = iSuscripciones) then begin
      Result := Format(K_FILTRO_SUSCRIP, [oZetaProvider.UsuarioActivo, sFiltroClasificaciones]);
    end else begin
      Result := Format('RE_CLASIFI = %d ', [iClasifActivo]);
    end;
  end;

begin
  oZetaProvider.EmpresaActiva := Empresa;
  if lVerConfidencial then
    Result := oZetaProvider.OpenSQL(Empresa, Format(Q_GET_REPORTE, [oZetaProvider.UsuarioActivo,
          oZetaProvider.UsuarioActivo, GetFiltro, '']), TRUE)
  else
    Result := oZetaProvider.OpenSQL(Empresa, Format(Q_GET_REPORTE, [oZetaProvider.UsuarioActivo,
          oZetaProvider.UsuarioActivo, GetFiltro, K_CONFIDENCIAL]), TRUE);
  SetComplete;
end;

function TdmServerReportes.GetEditReportes(Empresa: OleVariant; iReporte: Integer; var CampoRep: OleVariant): OleVariant;

  function GetTablaInfo: OleVariant;
  begin
    with oZetaProvider.TablaInfo do
      Filtro := Format('RE_CODIGO=%d', [iReporte]);
    Result := oZetaProvider.GetTabla(Empresa);
  end;

begin
  SetTablaInfoReporte;
  Result := GetTablaInfo;

  SetTablaInfoCampoRep;
  CampoRep := GetTablaInfo;
  SetComplete;
end;

function TdmServerReportes.GetLookUpReportes(Empresa: OleVariant; lVerConfidencial: WordBool): OleVariant;
const
  Q_GET_REPORTE = 'select RE_CODIGO,RE_NOMBRE,RE_TIPO, ' + 'RE_FECHA,REPORTE.US_CODIGO,RE_ENTIDAD, ' +
    'RE_FILTRO, QU_FILTRO, DI_CONFI ' + 'from REPORTE ' +
    'left outer join DICCION on DICCION.DI_CLASIFI = -1 AND DICCION.DI_CALC = REPORTE.RE_ENTIDAD ' +
    'left outer join QUERYS on QUERYS.QU_CODIGO = REPORTE.QU_CODIGO ' + '%s ' + 'order by RE_NOMBRE ';
begin
  if lVerConfidencial then
    Result := oZetaProvider.OpenSQL(Empresa, Format(Q_GET_REPORTE, ['']), TRUE)
  else
    Result := oZetaProvider.OpenSQL(Empresa, Format(Q_GET_REPORTE, ['where ' + 'DICCION.DI_CONFI =''N'' ']), TRUE);
  SetComplete;
end;

function TdmServerReportes.BorraReporte(Empresa: OleVariant; iReporte: Integer): OleVariant;
const
  K_DELETE = 'DELETE FROM REPORTE WHERE RE_CODIGO = %d';
begin
  with oZetaProvider do begin
    EmpresaActiva := Empresa;
    try
      EmpiezaTransaccion;
      ExecSQL(Empresa, Format(K_DELETE, [iReporte])); // PENDIENTE
      TerminaTransaccion(TRUE);
    except
      on Error: Exception do begin
        TerminaTransaccion(FALSE);
      end;
    end;
  end;
  SetComplete;
end;

function TdmServerReportes.GetEscogeReportes(Empresa: OleVariant; const Entidades, Tipos: WideString;
  lVerConfidencial: WordBool): OleVariant;
const
  K_ESCOGE_REPORTE = 'select RE_CODIGO,RE_NOMBRE,RE_ENTIDAD, RE_TIPO,RE_FECHA,US_CODIGO ' + 'from REPORTE ' +
    'left outer join DICCION on DICCION.DI_CLASIFI = -1 AND DICCION.DI_CALC = REPORTE.RE_ENTIDAD ' +
    'where RE_ENTIDAD in (%s) ' + 'and RE_TIPO in (%s)' + 'order by RE_NOMBRE';
begin

  if lVerConfidencial then
    Result := oZetaProvider.OpenSQL(Empresa, Format(K_ESCOGE_REPORTE, [Entidades, Tipos, '']), TRUE)
  else
    Result := oZetaProvider.OpenSQL(Empresa, Format(K_ESCOGE_REPORTE,
        [Entidades, Tipos, 'and DICCION.DI_CONFI =''N'' ']), TRUE);
  SetComplete;

end;

function TdmServerReportes.GetEscogeReporte(Empresa: OleVariant; Entidad, Tipo: Integer; lVerConfidencial: WordBool): OleVariant;
const
  K_ESCOGE_REPORTE = 'select RE_CODIGO,RE_NOMBRE,RE_ENTIDAD, RE_TIPO,RE_FECHA,US_CODIGO ' + 'from REPORTE ' +
    'left outer join DICCION on DICCION.DI_CLASIFI = -1 AND DICCION.DI_CALC = REPORTE.RE_ENTIDAD ' +
    'where RE_ENTIDAD = %d ' + 'and RE_TIPO = %d %s' + 'order by RE_NOMBRE';
begin
  if lVerConfidencial then
    Result := oZetaProvider.OpenSQL(Empresa, Format(K_ESCOGE_REPORTE, [Entidad, Tipo, '']), TRUE)
  else
    Result := oZetaProvider.OpenSQL(Empresa, Format(K_ESCOGE_REPORTE,
        [Entidad, Tipo, 'and DICCION.DI_CONFI =''N'' ']), TRUE);
  SetComplete;
end;

function TdmServerReportes.CampoRepFiltros(Empresa: OleVariant; Reporte: Integer): OleVariant;
const
  Q_FILTROS_DETALLE = 'select * FROM CAMPOREP ' + 'WHERE RE_CODIGO = %d AND CR_TIPO =5 ';
begin
  Result := oZetaProvider.OpenSQL(Empresa, Format(Q_FILTROS_DETALLE, [Reporte]), TRUE);
  SetComplete;
end;

function TdmServerReportes.GrabaReporte(Empresa, oDelta, oCampoRep: OleVariant; out ErrorCount: Integer;
  var iReporte: Integer): OleVariant;
begin
  FReporte := iReporte;
  with oZetaProvider do begin
    SetTablaInfoReporte;
    EmpresaActiva := Empresa;
    TablaInfo.BeforeUpdateRecord := BeforeUpdateReporte;
    Result := GrabaTabla(Empresa, oDelta, ErrorCount);
    if (ErrorCount = 0) then begin
      SetTablaInfoCampoRep;
      if FAlta then
        TablaInfo.BeforeUpdateRecord := BeforeUpdateCampoRep
      else
        TablaInfo.BeforeUpdateRecord := NIL;
      Result := GrabaTabla(Empresa, oCampoRep, ErrorCount);
      iReporte := FReporte;
    end;
  end;
  SetComplete;
end;

function TdmServerReportes.GrabaPoliza(Empresa, Delta, CampoRep: OleVariant; out ErrorCount: Integer;
  var iReporte: Integer): OleVariant;
const
  K_DELETE = 'DELETE FROM CAMPOREP WHERE RE_CODIGO = %d AND CR_TIPO <> 0';
begin
  FReporte := iReporte;
  with oZetaProvider do begin
    EmpresaActiva := Empresa;
    try
      EmpiezaTransaccion;
      if Not VarIsNull(CampoRep) then
        ExecSQL(Empresa, Format(K_DELETE, [iReporte])); // PENDIENTE
      TerminaTransaccion(TRUE);
    except
      on Error: Exception do
        TerminaTransaccion(FALSE);
    end;
    SetTablaInfoReporte;
    TablaInfo.BeforeUpdateRecord := BeforeReporteUpdatePoliza;

    Result := GrabaTabla(Empresa, Delta, ErrorCount);
    if (ErrorCount = 0) and Not VarIsNull(CampoRep) then begin
      SetTablaInfoCampoRep;
      with TablaInfo do begin
        OnUpdateData := UpdateDataPoliza;
        BeforeUpdateRecord := BeforeUpdatePoliza;
      end;
      Result := GrabaTabla(Empresa, CampoRep, ErrorCount);
    end;
  end;
  iReporte := FReporte;
  SetComplete;
end;

procedure TdmServerReportes.UpdateDataPoliza(Sender: TObject; DataSet: TzProviderClientDataSet);
const
  Q_POSICION = 'SELECT MAX( CR_POSICIO ) MAXIMO FROM CAMPOREP WHERE RE_CODIGO =%d AND CR_TIPO = 0';
var
  FDataSet: TZetaCursor;
begin
  with oZetaProvider do begin
    // CV: CreateQuery--ok
    // Variable local
    FDataSet := CreateQuery(Format(Q_POSICION, [FReporte]));

    with FDataSet do begin
      Open;
      FPosicion := Fields[0].AsInteger + 1;
      Close;
    end;

  end;
  FreeAndNil(FDataSet);
end;

function TdmServerReportes.GetMaxReporte: Integer;
const
  K_MAX_REPORTE = 'SELECT MAX(RE_CODIGO) MAXIMO FROM REPORTE';
var
  FDataSet: TZetaCursor;
begin
  with oZetaProvider do begin
    // CV: CreateQuery--ok
    FDataSet := CreateQuery(K_MAX_REPORTE);
    FDataSet.Open;
    Result := FDataSet.Fields[0].AsInteger + 1;
    FDataSet.Close;
  end;
  FreeAndNil(FDataSet);
end;

procedure TdmServerReportes.BeforeReporteUpdatePoliza(Sender: TObject; SourceDS: TDataSet;
  DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  // Si es Alta, tengo que obtener el m�ximo Folio
  // Si es Edici�n, tengo que borrar todos los CampoRep anteriores
  if (UpdateKind = ukInsert) then begin
    FReporte := GetMaxReporte;
    DeltaDS.Edit;
    DeltaDS.FieldByName('RE_CODIGO').AsInteger := FReporte;
    DeltaDS.Post;
  end;
end;

procedure TdmServerReportes.BeforeUpdatePoliza(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
  UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  with DeltaDS do
    if (UpdateKind = ukInsert) then begin
      Edit;
      FieldByName('RE_CODIGO').AsInteger := FReporte;
      if (FieldByName('CR_TIPO').AsInteger = 0) and (FieldByName('CR_POSICIO').AsInteger = -1) then begin
        FieldByName('CR_POSICIO').AsInteger := FPosicion;
        Inc(FPosicion);
      end;
      Post;
    end;
end;

procedure TdmServerReportes.BeforeUpdateCampoRep(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
  UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  // En la Alta de Reportes, el RE_CODIGO viene VACIO del Cliente
  // Se asigna del FReporte que se obtuvo en la Alta del registro
  // padre : REPORTE, en su BeforeUpdateRecord
  with DeltaDS do begin
    Edit;
    FieldByName('RE_CODIGO').AsInteger := FReporte;
    Post;
  end;
end;

procedure TdmServerReportes.BeforeUpdateReporte(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
  UpdateKind: TUpdateKind; var Applied: Boolean);
const
  K_DELETE = 'DELETE FROM CAMPOREP WHERE RE_CODIGO = %d';
  // K_MAX_REPORTE = 'SELECT MAX(RE_CODIGO) FROM REPORTE';
  // var FDataSet : TZetaCursor;
begin
  // Si es Alta, tengo que obtener el m�ximo Folio
  // Si es Edici�n, tengo que borrar todos los CampoRep anteriores
  with oZetaProvider do begin
    if (UpdateKind = ukInsert) then begin
      FReporte := GetMaxReporte;
      DeltaDS.Edit;
      DeltaDS.FieldByName('RE_CODIGO').AsInteger := FReporte;
      DeltaDS.Post;
      FAlta := TRUE;
    end else begin
      FAlta := FALSE;
      ExecSQL(EmpresaActiva, Format(K_DELETE, [FReporte]));
    end;
  end;
end;

function TdmServerReportes.GetSuscripcion(Empresa: OleVariant; iFrecuencia: Integer;
  const sUsuarios, sReportes: WideString; var Usuarios: OleVariant): OleVariant;
const
  Q_GET_SUSCRIP = 'select RE_CODIGO,US_CODIGO,SU_VACIO,SU_ENVIAR ' + 'from SUSCRIP %s ' +
    'order by RE_CODIGO, US_CODIGO ';
  Q_GET_USUARIO = 'select US_CODIGO, US_EMAIL, US_FORMATO, US_ACTIVO, US_NOMBRE, US_CORTO  ' + 'from USUARIO ' +
    'where US_EMAIL <> '''' %s';
var
  sFiltro: string;
begin
  if iFrecuencia >= 0 then
    sFiltro := Format('SU_FRECUEN=%d', [iFrecuencia]);

  if strLleno(sUsuarios) then
    sFiltro := ConcatFiltros(sFiltro, '( US_CODIGO in ( ' + sUsuarios + ' ) )');
  if strLleno(sReportes) then
    sFiltro := ConcatFiltros(sFiltro, '( RE_CODIGO in ( ' + sReportes + ' ) )');
  if strLleno(sFiltro) then
    sFiltro := 'WHERE ' + sFiltro;

  Result := oZetaProvider.OpenSQL(Empresa, Format(Q_GET_SUSCRIP, [sFiltro]), TRUE);
  Usuarios := oZetaProvider.OpenSQL(oZetaProvider.Comparte,
    Format(Q_GET_USUARIO, [StrLlenoDef(sUsuarios, ' AND ( US_CODIGO in ( ' + sUsuarios + ' ) )')]), TRUE);
  SetComplete;
end;

function TdmServerReportes.BorraFavoritos(Empresa: OleVariant; iReporte: Integer): OleVariant;
begin
  with oZetaProvider do begin
    EmpresaActiva := Empresa;
    try
      EmpiezaTransaccion;
      ExecSQL(Empresa, Format(K_DELETE_FAVORITOS, [iReporte, UsuarioActivo]));
      TerminaTransaccion(TRUE);
    except
      on Error: Exception do begin
        TerminaTransaccion(FALSE);
      end;
    end;
  end;
  SetComplete;
end;

function TdmServerReportes.AgregaFavoritos(Empresa: OleVariant; iReporte: Integer): Integer;
const
  K_INSERT_FAVORITOS = 'insert into MISREPOR (US_CODIGO,RE_CODIGO) values(:US_CODIGO,:RE_CODIGO)';
var
  FCursor: TZetaCursor;
begin
  Result := 0;
  with oZetaProvider do begin
    EmpresaActiva := Empresa;
    try
      EmpiezaTransaccion;
      ExecSQL(Empresa, Format(K_DELETE_FAVORITOS, [iReporte, UsuarioActivo]));
      FCursor := CreateQuery(K_INSERT_FAVORITOS);
      try
        try
          ParamAsInteger(FCursor, 'US_CODIGO', UsuarioActivo);
          ParamAsInteger(FCursor, 'RE_CODIGO', iReporte);
          Result := Ejecuta(FCursor);
          TerminaTransaccion(TRUE);
        except
          on Error: Exception do begin
            TerminaTransaccion(FALSE);
          end;
        end;
      finally
        FreeAndNil(FCursor);
      end;
    except
      on Error: Exception do begin
        TerminaTransaccion(FALSE);
      end;
    end;
  end;
  SetComplete;
end;

function TdmServerReportes.GetPlantilla(const Nombre: WideString): OleVariant;
var
  FStream: TMemoryStream;
  FBlob  : TBlobField;
begin
  with cdsTemporal do begin
    InitTempDataset;
    AddBlobField('CampoBlob', 0);
    CreateTempDataset;
    FBlob := TBlobField(FieldByName('CampoBlob'));
  end;

  FStream := TMemoryStream.Create;
  try
    if FileExists(Nombre) then begin
      FStream.LoadFromFile(Nombre);
      FBlob.DataSet.Edit;
      FBlob.LoadFromStream(FStream);
      FBlob.DataSet.Post;

      Result := TClientDataset(FBlob.DataSet).Data;
    end else
      Result := Null;
  finally
    FreeAndNil(FStream);
  end;
end;

{$IFNDEF DOS_CAPAS}

initialization

TComponentFactory.Create(ComServer, TdmServerReportes, Class_dmServerReportes, ciMultiInstance,
  ZetaServerTools.GetThreadingModel);
{$ENDIF}

end.
