unit DSelloDigital;

interface

uses
  SysUtils, Classes,DB,DZetaServerProvider,
  WODSSHKeyLib_TLB, OleServer, WODCRYPTCOMLib_TLB;

type
  TdmSelloDigital = class(TDataModule)
    Crypto: TwodCryptCom;
    PrivateKey: TKeys;
    PublicKey: TKeys;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FDataset: TDataset;
    FRazonSocial: TzetaCursorLocate;
    oZetaProvider: TdmZetaServerProvider;
    FCadenaOriginal: UTF8String;
    FDigestion: UTF8String;
    FSello: string;
    FErrorMessage: string;
    FYear: integer;
    OriginalFile, ResultBlob,SignatureBlob: TMemBlob;
    procedure CrearCadenaOriginal;
    procedure CrearDigestion;
    procedure CrearSello;
    procedure VerificarSello;
    procedure LogError(const sMensaje: string);
  public
    { Public declarations }
    function Calcular( oDataset : TDataset; oProvider:TdmZetaServerProvider ): Boolean;
    property CadenaOriginal: UTF8String read FCadenaOriginal;
    property Digestion: UTF8String read FDigestion;
    property Sello: string read FSello;
    property Year: integer read FYear write FYear;
    property ErrorMessage: string read FErrorMessage;

  end;

var
  dmSelloDigital: TdmSelloDigital;

implementation
uses

    ZetaCommonClasses,
    ZetaCommonTools;


 const
      Q_RAZONSOCIAL = 'select  RS_CODIGO , RS_NOMBRE, RS_RFC, RS_CURP, RS_RLEGAL, RS_RL_RFC, RS_RL_CURP, RS_KEY_PR, RS_KEY_PU FROM RSOCIAL ORDER BY RS_CODIGO ';

{$R *.dfm}

{ TdmSelloDigital }


procedure TdmSelloDigital.CrearCadenaOriginal;
 function xTrim( const sTexto: string ): string;
 begin
      Result := Trim( sTexto );
      Result := StrTransAll( Result, CR_LF, ' ' ); //Se sustituye cualquier enter, por un espacio
      Result := StrTransAll( Result, Chr( 9 ), ' ' ); //Se sustituye cualquier tab, por un espacio
      Result := StrTransAll( Result, '  ', ' ' );
 end;

 function xRFC( const sTexto: string ): string;
 begin
      Result := Trim( sTexto );
      Result := StrTransAll( Result, '-' , ''); //el rfc debe de contener solo las letras y numeros que lo forman
 end;

 function xConcat( const sTexto: String ): String;
 begin
      Result := FCadenaOriginal + sTexto + K_PIPE;
 end;

begin
     FCadenaOriginal := K_PIPE;
     with FDataset do
     begin
          
          FCadenaOriginal := xConcat( FieldByName('TD_MES_INI').AsString );
          FCadenaOriginal := xConcat( FieldByName('TD_MES_FIN').AsString );
          FCadenaOriginal := xConcat( IntToStr( FYear )  );

          FCadenaOriginal := xConcat( FieldByName('RFC_VALIDADO').AsString );
          FCadenaOriginal := xConcat( FieldByName('TD_CB_CURP').AsString );
          FCadenaOriginal := xConcat( xTrim( FieldByName('TD_CB_APE_PAT').AsString ) );
          FCadenaOriginal := xConcat( xTrim( FieldByName('TD_CB_APE_MAT').AsString ) );
          FCadenaOriginal := xConcat( xTrim( FieldByName('TD_CB_NOMBRES').AsString ) );


          //o	3. Informaci�n del nodo "Impuesto Sobre la Renta por Sueldos y Salarios"
          FCadenaOriginal := xConcat( FieldByName('TD_DATA_62').AsString );
          FCadenaOriginal := xConcat( FieldByName('TD_DATA_63').AsString );
          FCadenaOriginal := xConcat( FloatToStr( FieldByName('TD_DATA_62').AsFloat +
                                                  FieldByName('TD_DATA_63').AsFloat) );
          FCadenaOriginal := xConcat( '0');
          FCadenaOriginal := xConcat( FieldByName('TD_DATA_74').AsString );
          FCadenaOriginal := xConcat( FieldByName('TD_DATA_75').AsString );
          FCadenaOriginal := xConcat( FieldByName('TD_DATA_76').AsString );
          FCadenaOriginal := xConcat( FieldByName('TD_DATA_77').AsString );
          FCadenaOriginal := xConcat( '0');
          FCadenaOriginal := xConcat( '0');
          FCadenaOriginal := xConcat( FloatToStr( FieldByName('TD_DATA_64').AsFloat+
                                                  FieldByName('TD_DATA_65').AsFloat ) );
          FCadenaOriginal := xConcat( FieldByName('TD_DATA_65').AsString );


          //o	4. Informaci�n del nodo Datos del retenedor
          FCadenaOriginal := xConcat( xRFC( FRazonSocial.FieldByName('RS_RFC').AsString ) );
          FCadenaOriginal := xConcat( xRFC( FRazonSocial.FieldByName('RS_CURP').AsString ) );
          FCadenaOriginal := xConcat( xTrim( FRazonSocial.FieldByName('RS_NOMBRE').AsString ) );
          FCadenaOriginal := xConcat( xRFC( FRazonSocial.FieldByName('RS_RL_RFC').AsString ) );
          FCadenaOriginal := xConcat( xRFC( FRazonSocial.FieldByName('RS_RL_CURP').AsString ) );
          FCadenaOriginal := xConcat( xTrim( FRazonSocial.FieldByName('RS_RLEGAL').AsString )  );
          //La cadena original debe de comenzar con doble
          FCadenaOriginal := K_PIPE + FCadenaOriginal +K_PIPE ;
     end;
end;

procedure TdmSelloDigital.LogError( const sMensaje: string );
begin
     if StrLleno( sMensaje ) then
     begin
          FErrorMessage := sMensaje;
          FErrorMessage := FErrorMessage + CR_LF +
                           'Cadena Original: ' + WrapText( FCadenaOriginal,100 ) + CR_LF +
                           'Digesti�n: ' + FDigestion + CR_LF+
                           'Sello Digital: '+ FSello ;
     end;
end;

procedure TdmSelloDigital.CrearDigestion;
 var
    inb, outb : IBlob;
begin
     try
        OriginalFile.Text := AnsiToUtf8(FCadenaOriginal);

        OriginalFile.DefaultInterface.QueryInterface(WODCRYPTCOMLib_TLB.IID_IBlob, inb);
        ResultBlob.DefaultInterface.QueryInterface(WODCRYPTCOMLib_TLB.IID_IBlob, outb);

        Crypto.LicenseKey := 'AQ2H-TRA4-FXRS-F5HL';
        Crypto.type_ := MD5;
        Crypto.Digest( inb, outb );
        FDigestion := ResultBlob.ToHex;

     except
           on Error:Exception do
           begin
                LogError( 'La digestion de la cadena original no se pudo generar' + CR_LF + Error.Message );
           end;
     end;

end;

procedure TdmSelloDigital.CrearSello;
 var inb, outb : IBlob;

begin
        {El segundo parametro es el password de la llave privada}
        try
           //PrivateKey.Load( 'C:\Proyectos\PE - Kenworth\Sello Digital\1. Analisis\Ejemplos de XML\CadenaOriginalesTest\Declaracion Anual\llavePrivada.rsa.pem.key'  );
           PrivateKey.FromXmlString( FRazonSocial.FieldByName('RS_KEY_PR').AsString );

           Crypto.Type_ := rsa;
           Crypto.Padding := PadPKCS7;
           Crypto.SecretKey := PrivateKey.PrivateKey[0];
           Crypto.Optimized := True;

           OriginalFile.Text := FDigestion;
           OriginalFile.DefaultInterface.QueryInterface(WODCRYPTCOMLib_TLB.IID_IBlob, inb);
           SignatureBlob.DefaultInterface.QueryInterface(WODCRYPTCOMLib_TLB.IID_IBlob, outb);

           Crypto.Sign(inb, outb);

        except
              on Error:Exception do
              begin
                   LogError( 'El sello digital no se pudo generar' + CR_LF + Error.Message );
              end;
        end;

        FSello := SignatureBlob.ToBase64;
end;

procedure TdmSelloDigital.VerificarSello;
var
   inb, outb : IBlob;
begin
     try
        OriginalFile.DefaultInterface.QueryInterface(WODCRYPTCOMLib_TLB.IID_IBlob, inb);
        SignatureBlob.DefaultInterface.QueryInterface(WODCRYPTCOMLib_TLB.IID_IBlob, outb);

        //PublicKey.PublicKeyLoad( 'C:\Proyectos\PE - Kenworth\Sello Digital\1. Analisis\Ejemplos de XML\CadenaOriginalesTest\Declaracion Anual\Certificado.pem.cer' );
        PublicKey.FromXmlString( FRazonSocial.FieldByName('RS_KEY_PR').AsString );

        Crypto.Type_ := RSA;
        Crypto.Padding := PadPKCS7;
        Crypto.SecretKey := PublicKey.PublicKey[0];
        Crypto.Optimized := true;

        if not Crypto.Verify(inb, outb) Then
        begin
             LogError( 'El sello digital no se pudo verificar' );
        end
      except
            on Error:Exception do
            begin
                 LogError( 'El sello digital no se pudo verificar' + CR_LF + Error.Message );
            end;
      end;
end;

function TdmSelloDigital.Calcular(oDataset: TDataset; oProvider:TdmZetaServerProvider ): boolean;
begin
     if zStrToBool( oDataset.FieldByName('RS_SELLO').AsString ) then
     begin
          oZetaProvider := oProvider;
          if ( FRazonSocial = NIL ) then
          begin
               FRazonSocial:= oZetaProvider.CreateQueryLocate;
               oZetaProvider.PreparaQuery(FRazonSocial,Q_RAZONSOCIAL );
               FRazonSocial.Active := TRUE;
          end;


          FCadenaOriginal := VACIO;
          FDigestion := VACIO;
          FSello := VACIO;
          FErrorMessage := VACIO;

          FDataset:= oDataset;

          if FRazonSocial.Locate ('RS_CODIGO', oDataset.FieldByName('RS_CODIGO').AsString, [] ) then
          begin
               CrearCadenaOriginal;
               CrearDigestion;
               CrearSello;
               VerificarSello;
          end
          else
          begin
               FErrorMessage := Format( 'El sello digital no fu� generado debido a que la Raz�n Social (%s) a la que pertenece el empleado no existe', [oDataset.FieldByName('RS_CODIGO').AsString] );
          end;

          Result := StrVacio(FErrorMessage);
     end
     else
         Result := TRUE;
end;



procedure TdmSelloDigital.DataModuleCreate(Sender: TObject);
begin
     OriginalFile := WODCRYPTCOMLib_TLB.TMemBlob.Create(self);
     ResultBlob := WODCRYPTCOMLib_TLB.TMemBlob.Create(self);
     SignatureBlob := WODCRYPTCOMLib_TLB.TMemBlob.Create(self);

end;

procedure TdmSelloDigital.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FRazonSocial );
     FreeAndNil( OriginalFile );
     FreeAndNil( ResultBlob );
     FreeAndNil( SignatureBlob );
end;

end.
