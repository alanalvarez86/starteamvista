object dmSelloDigital: TdmSelloDigital
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 240
  Height = 570
  Width = 650
  object Crypto: TwodCryptCom
    AutoConnect = False
    ConnectKind = ckRunningOrNew
    Left = 24
    Top = 24
  end
  object PrivateKey: TKeys
    AutoConnect = False
    ConnectKind = ckRunningOrNew
    Left = 24
    Top = 80
  end
  object PublicKey: TKeys
    AutoConnect = False
    ConnectKind = ckRunningOrNew
    Left = 16
    Top = 136
  end
end
