library Sistema;

uses
  ComServ,
  Sistema_TLB in 'Sistema_TLB.pas',
  DServerSistema in 'DServerSistema.pas' {dmServerSistema: TMtsDataModule} {dmServerSistema: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
