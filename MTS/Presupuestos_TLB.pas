unit Presupuestos_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 21/01/2011 01:36:47 p.m. from Type Library described below.

// ************************************************************************  //
// Type Lib: d:\3win_20_Vsn_2011\MTS\Presupuestos.tlb (1)
// LIBID: {79ADFFEC-6F13-489C-8B56-42ACD9245CBD}
// LCID: 0
// Helpfile: 
// HelpString: Presupuestos Library
// DepndLst: 
//   (1) v1.0 Midas, (C:\WINDOWS\system32\midas.dll)
//   (2) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, Midas, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  PresupuestosMajorVersion = 1;
  PresupuestosMinorVersion = 0;

  LIBID_Presupuestos: TGUID = '{79ADFFEC-6F13-489C-8B56-42ACD9245CBD}';

  IID_IdmServerPresupuestos: TGUID = '{2604A806-F673-449E-A35F-2F8A510377DF}';
  CLASS_dmServerPresupuestos: TGUID = '{2416AB8B-40E0-4D3B-925C-9209CD3F0717}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerPresupuestos = interface;
  IdmServerPresupuestosDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerPresupuestos = IdmServerPresupuestos;


// *********************************************************************//
// Interface: IdmServerPresupuestos
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {2604A806-F673-449E-A35F-2F8A510377DF}
// *********************************************************************//
  IdmServerPresupuestos = interface(IAppServer)
    ['{2604A806-F673-449E-A35F-2F8A510377DF}']
    function DepuraSupuestosRH(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CalculaCubo(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetEventosAltas(Empresa: OleVariant): OleVariant; safecall;
    function GrabaEventosAltas(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetSupuestosRH(Empresa: OleVariant): OleVariant; safecall;
    function GrabaSupuestosRH(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function DepuraNominas(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetPeriodosBaja(Empresa: OleVariant; FechaIni: TDateTime; FechaFin: TDateTime): OleVariant; safecall;
    function GetPeriodosSimulacion(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetEscenarios(Empresa: OleVariant): OleVariant; safecall;
    function GrabaEscenarios(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerPresupuestosDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {2604A806-F673-449E-A35F-2F8A510377DF}
// *********************************************************************//
  IdmServerPresupuestosDisp = dispinterface
    ['{2604A806-F673-449E-A35F-2F8A510377DF}']
    function DepuraSupuestosRH(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 301;
    function CalculaCubo(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 307;
    function GetEventosAltas(Empresa: OleVariant): OleVariant; dispid 310;
    function GrabaEventosAltas(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 311;
    function GetSupuestosRH(Empresa: OleVariant): OleVariant; dispid 312;
    function GrabaSupuestosRH(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 313;
    function DepuraNominas(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 304;
    function GetPeriodosBaja(Empresa: OleVariant; FechaIni: TDateTime; FechaFin: TDateTime): OleVariant; dispid 302;
    function GetPeriodosSimulacion(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 303;
    function GetEscenarios(Empresa: OleVariant): OleVariant; dispid 305;
    function GrabaEscenarios(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 306;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer; 
                             out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                           Options: Integer; const CommandText: WideString; var Params: OleVariant; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerPresupuestos provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerPresupuestos exposed by              
// the CoClass dmServerPresupuestos. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerPresupuestos = class
    class function Create: IdmServerPresupuestos;
    class function CreateRemote(const MachineName: string): IdmServerPresupuestos;
  end;

implementation

uses ComObj;

class function CodmServerPresupuestos.Create: IdmServerPresupuestos;
begin
  Result := CreateComObject(CLASS_dmServerPresupuestos) as IdmServerPresupuestos;
end;

class function CodmServerPresupuestos.CreateRemote(const MachineName: string): IdmServerPresupuestos;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerPresupuestos) as IdmServerPresupuestos;
end;

end.
