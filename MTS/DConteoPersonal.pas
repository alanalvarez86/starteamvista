unit DConteoPersonal;

interface
Uses Windows, Messages, SysUtils, Classes,
     DZetaServerProvider;

type
    TConteoPersonal = Class(TObject)
    private
        oZetaProvider: TdmZetaServerProvider;
        FConteoNiveles : Integer;
        FConteoNivel : array[ 1..5 ] of String;
        function NumConteo: Integer;
    public
      constructor Create( oProvider : TdmZetaServerProvider );
      procedure ActualizaConteo;
    end;

implementation

uses ZetaCommonTools,
     ZetaCommonLists,
     ZetaCommonClasses;

{ TConteoPersonal }

constructor TConteoPersonal.Create(oProvider: TdmZetaServerProvider);
begin
     oZetaProvider := oProvider;
end;

function TConteoPersonal.NumConteo: Integer;
const
     //K_GLOBAL_CONTEO_NIVEL1            = 161;
     //K_GLOBAL_CONTEO_NIVEL5            = 165;
     Q_GET_NIVELES = 'select GL_FORMULA from GLOBAL where GL_CODIGO >= 161 and GL_CODIGO <= 165 order by GL_CODIGO';
var
    oQueryNiveles : TZetaCursor;
    iNivelConteo : Integer;
begin
    if ( FConteoNiveles = 0 ) then
    begin
         oQueryNiveles := oZetaProvider.CreateQuery;
         try
            oZetaProvider.AbreQueryScript( oQueryNiveles, Q_GET_NIVELES );
            with oQueryNiveles do
                 while not Eof do
                 begin
                     iNivelConteo := StrToIntDef( Fields[ 0 ].AsString, 0 );
                     if iNivelConteo > 0 then
                     begin
                         Inc( FConteoNiveles );
                         FConteoNivel[ FConteoNiveles ] := ZetaCommonLists.ObtieneElemento( lfCamposConteo, iNivelConteo );
                     end
                     else
                         break;
                     Next;
                 end;
         finally
            FreeAndNil( oQueryNiveles );
         end;
    end;
    Result := FConteoNiveles;
end;

procedure TConteoPersonal.ActualizaConteo;
const
     Q_CONTEO_COLABORA = 'select %s, COUNT(*) as ValorReal ' +
                         'from COLABORA where CB_ACTIVO = ''S'' ' +
                         'group by %0:s';
     Q_UPDATE_CONTEO_CERO = 'update CONTEO set CT_REAL = 0 ';
     Q_UPDATE_CONTEO = 'update CONTEO set CT_REAL = :Real ' +
                       'where CT_NIVEL_1 = :Nivel1 and CT_NIVEL_2 = :Nivel2 and '+
                       'CT_NIVEL_3 = :Nivel3 and CT_NIVEL_4 = :Nivel4 and CT_NIVEL_5 = :Nivel5';
     Q_INSERT_CONTEO = 'insert into CONTEO ( CT_NIVEL_1, CT_NIVEL_2, CT_NIVEL_3, CT_NIVEL_4, CT_NIVEL_5, CT_REAL ) '+
		       'VALUES (:Nivel1, :Nivel2, :Nivel3, :Nivel4, :Nivel5, :Real )';
var
   FQryConteo, FQryUpdate, FQryInsert: TZetaCursor;

   function GetCamposConteo: String;
   var
      i: Integer;
   begin
        Result := VACIO;
        for i := 1 to NumConteo do
            Result := ConcatString( Result,  FConteoNivel[i], ',' );
   end;

   function GetCampoConteo( iValor: Integer ): String;
   begin
        if ( iValor < NumConteo ) then
           Result := FQryConteo.Fields[iValor].AsString
        else
           Result := VACIO;
   end;

   procedure SetValorParametros( oDataSet: TZetaCursor );
   begin
        with oZetaProvider do
        begin
             ParamAsFloat( oDataSet, 'Real', FQryConteo.Fields[ NumConteo ].AsFloat );
             ParamAsChar( oDataSet, 'Nivel1', GetCampoConteo(0), 6 );
             ParamAsChar( oDataSet, 'Nivel2', GetCampoConteo(1), 6 );
             ParamAsChar( oDataSet, 'Nivel3', GetCampoConteo(2), 6 );
             ParamAsChar( oDataSet, 'Nivel4', GetCampoConteo(3), 6 );
             ParamAsChar( oDataSet, 'Nivel5', GetCampoConteo(4), 6 );
        end;
   end;

begin
     FConteoNiveles := 0;
     FQryInsert := NIL;
     if NumConteo > 0 then
        with oZetaProvider do
        begin
             FQryConteo := CreateQuery;
             FQryUpdate := CreateQuery( Q_UPDATE_CONTEO_CERO );

             try
                EmpiezaTransaccion;
                Ejecuta(FQryUpdate);
             finally
                    TerminaTransaccion( TRUE );
             end;

             PreparaQuery( FQryUpdate,Q_UPDATE_CONTEO );

             try
                AbreQueryScript( FQryConteo, Format( Q_CONTEO_COLABORA, [ GetCamposConteo ] ) );
                EmpiezaTransaccion;
                with FQryConteo do
                     while not EOF do
                     begin
                          SetValorParametros( FQryUpdate );
                          if ( Ejecuta( FQryUpdate ) = 0 ) then
                          begin
                               if ( FQryInsert = nil ) then
                                  FQryInsert := CreateQuery( Q_INSERT_CONTEO );
                               SetValorParametros( FQryInsert );
                               try
                                  Ejecuta( FQryInsert );
                               except
                                  on Error: Exception do
                                  begin
                                       if PK_Violation( Error ) then
                                          Error.Message := 'Registro Duplicado de Presupuesto ';
                                       raise;
                                  end;
                               end;
                          end;
                          Next;
                     end;
             finally
                TerminaTransaccion( TRUE );
                FreeAndNil( FQryConteo );
                FreeAndNil( FQryUpdate );
                if ( FQryInsert <> nil ) then
                   FreeAndNil( FQryInsert );
             end;
        end;
end;

end.
