unit DServerImss;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      DServerIMSS.pas                            ::
  :: Descripci�n: Programa principal de Imss.dll             ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComServ, ComObj, VCLCom, StdVcl,
     DataBkr, DB, DBClient, MtsRdm, Mtx,
{$ifndef DOS_CAPAS}
     Imss_TLB,
{$endif}
     DIMSSClass,
     DZetaServerProvider,
     ZCreator,
     ZetaSQLBroker,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaServerDataSet;

type
  eTipoIMSS = ( eIMSSDatosBim,
                eIMSSMovDatosBim,
                eIMSSDatosMen,
                eIMSSMovDatosMen,
                eIMSSDatosTot,
                eIMSSHisBim,
                eIMSSMovHisBim,
                eIMSSHisMen,
                eIMSSMovHisMen,
                eImssConciliaInf );

  TdmServerIMSS = class(TMtsDataModule {$ifndef DOS_CAPAS}, IdmServerImss {$endif} )
    cdsDatos: TServerDataSet;
    cdsLista: TServerDataSet;
    cdsIncapacidades: TServerDataSet;
    cdsInfonavit: TServerDataSet;
    procedure dmServerIMSSCreate(Sender: TObject);
    procedure dmServerIMSSDestroy(Sender: TObject);
  private
    { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
{$ifndef DOS_CAPAS}
    oZetaCreator: TZetaCreator;
    oZetaSQLBroker: TSQLBroker;
{$endif}
    FListaParametros: string;
    FIMSS: TIMSS;
    FIMSSTemp: TIMSS;
    FQryPrestamo, FInsCargo, FInsPrestamo, FUpdPrestamo, FDelCargo : TZetaCursor;
    function CalcularPagosIMSSDataset( Dataset: TDataset ): OleVariant;
    function CalcularTasaINFONAVITDataset(Dataset: TDataSet): OleVariant;
    function GetScript(const eTipo: ETipoIMSS): String;
    function GetSQLBroker: TSQLBroker;
    function ExportarSUADataset(Dataset: TDataSet): OleVariant;
    function RevisarSUADataset( Dataset: TDataSet ): OleVariant;
    procedure BuildDatasetPagosIMSS( const sPatron: string; const iYear, iMes: Integer);
    procedure CalcularTasaINFONAVITBuildDataset;
    procedure RevisarSUABuildDataset;
    procedure SetTablaInfo(const eTipo: ETipoIMSS);
    procedure ClearBroker;
    procedure ClearIMSS;
    procedure InitBroker;
    procedure InitCreator;
    procedure InitIMSS;
    procedure CalcularPagosIMSSParametros;
    procedure CalcularRecargosIMSSParametros;
    procedure CalcularTasaINFONAVITParametros;
    procedure ExportarSUAParametros;
    procedure CalcularPrimaRiesgoParametros;
    procedure RevisaSeguroSocialRepetido;
    procedure AjusteRetInfonavitParametros;
    procedure AjusteRetInfonavitBuildDataSet;
    function  AjusteRetInfonavitDataset(Dataset: TDataset): OleVariant;
    procedure PreparaPrestamosInfonavit;
    procedure ValidarMovimientosIDSEParametros;
    function  ValidarMovimientosIDSEDataset(Dataset: TDataset): OleVariant;
  protected
    { Protected declarations }
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    property SQLBroker: TSQLBroker read GetSQLBroker;


{$ifdef DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
{$endif}
    function GetIMSSDatosBim(Empresa: OleVariant; Year, Mes, Tipo, EmpleadoNumero: Integer; const Patron: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetMovIMSSDatosBim(Empresa: OleVariant; Year, Mes, Tipo, EmpleadoNumero: Integer; const Patron: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetIMSSDatosMen(Empresa: OleVariant; Year, Mes, Tipo, EmpleadoNumero: Integer; const Patron: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetMovIMSSDatosMen(Empresa: OleVariant; Year, Mes, Tipo, EmpleadoNumero: Integer; const Patron: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetIMSSDatosTot(Empresa: OleVariant; Year, Mes, Tipo: Integer; const Patron: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetIMSSHisMen(Empresa: OleVariant; Year, Mes, Tipo: Integer; const Patron: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetMovIMSSHisMen(Empresa: OleVariant; Year, Mes, Tipo: Integer; const Patron: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetIMSSHisBim(Empresa: OleVariant; Year, Mes, Tipo: Integer; const Patron: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetMovIMSSHisBim(Empresa: OleVariant; Year, Mes, Tipo: Integer; const Patron: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaIMSSDatosTot(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function CalcularPagosIMSS(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function CalcularRecargosIMSS(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function CalcularTasaINFONAVIT(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function RevisarSUA(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function ExportarSUA(Empresa, Parametros: OleVariant; out Empleados,Movimientos, Infonavit, Incapacidades: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall; {$endif}
    function GetPrimaRiesgo(Empresa: OleVariant; Fecha: TDateTime; const Patron: WideString): Double; {$ifndef DOS_CAPAS} safecall; {$endif}
    function CalcularPrimaRiesgo(Empresa, Parametros: OleVariant; out Datos: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetDatosConciliaInfo(Empresa: OleVariant; ImssYear, ImssMes, Empleado: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function AjusteRetInfonavit(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function AjusteRetInfonavitGetLista(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function AjusteRetInfonavitLista(Empresa, Lista, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS}  safecall; {$endif}
    function ValidarMovimientosIDSE(Empresa, Lista,  Parametros: OleVariant): OleVariant;  {$ifndef DOS_CAPAS}  safecall; {$endif}
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

uses ZGlobalTress,
     ZetaTipoEntidad,
     ZetaCommonTools,
     DQueries,
     ZetaServerTools;

const
     K_IMSS_PATRON = 'RegistroPatronal';
     K_IMSS_YEAR = 'IMSSYear';
     K_IMSS_MES = 'IMSSMes';
     K_IMSS_TIPO = 'IMSSTipo';
     K_FILTRO = '( LS_YEAR = %d ) and ( LS_MONTH = %d ) and ( LS_TIPO = %d ) and ( LS_PATRON = ''%s'' )';
     K_FILTRO_EMPLEADO = '( LS_YEAR = %d ) and ( LS_MONTH = %d ) and ( LS_TIPO = %d ) and ( LS_PATRON = ''%s'' ) and ( CB_CODIGO = %d )';
     K_FILTRO_REVISION_SUA = '( ( CB_ACTIVO = ''S'' ) and ( CB_FEC_ING <= ''%s'' ) ) or ( ( CB_ACTIVO = ''N'' ) and ( CB_FEC_BSS >= ''%s'' ) and ( CB_FEC_BSS <= ''%s'' ) )';
     K_FILTRO_VARIOS_PATRONES = '( LS_YEAR = %d ) and ( LS_MONTH = %d ) and ( CB_CODIGO = %d )';
     K_REFEREN_PRESTA_AJUSTE = 'INFO';

     Q_ACTUALIZA_TASA = 0;
     Q_CALCULA_RECARGOS = 1;
     Q_INCAPACIDADES = 2;
     Q_DIAS_COTIZA = 3;
     Q_PRIMA_BUSCA = 4;
     Q_PRIMA_AGREGA = 5;
     Q_PRIMA_MODIFICA = 6;
     Q_IMSS_REPETIDO = 7;
     Q_IMSS_REPETIDO_EMPLEADOS = 8;
     Q_ACUM_INFONAVIT = 9;
     Q_MONTOS_OTROS_AJUSTES = 10;
     Q_SALDO_ACTUAL_AJUSTE = 11;
     Q_MONTO_AJUSTE_ULTIMO_MES = 12;
     Q_REEMPLAZAR_AJUSTE = 13;
     Q_ACTUALIZAR_MOVIMIENTO_IDSE = 14;

function GetSQLScript( const iScript: Integer ): String;
begin
     case iScript of
{$ifdef INTERBASE}
          Q_ACTUALIZA_TASA: Result := 'select TASANUEVA from ACTUALIZA_TASA_INFONAVIT( :Empleado, :Fecha, :FechaCaptura, :Usuario )';
{$else}
          Q_ACTUALIZA_TASA: Result := 'EXECUTE PROCEDURE ACTUALIZA_TASA_INFONAVIT( :Empleado, :Fecha, :FechaCaptura, :Usuario, NULL )';
{$endif}
          Q_CALCULA_RECARGOS: Result := 'execute procedure IMSS_CALCULA_RECARGOS( :Patron, :Year, :Mes, :Tipo, :Factor, :Tasa )';
          Q_INCAPACIDADES: Result := 'select INCAPACI.CB_CODIGO, INCAPACI.IN_SUA_INI, INCAPACI.IN_SUA_FIN, INCAPACI.IN_DIAS, '+
                                     'INCAPACI.IN_FIN, INCAPACI.IN_TASA_IP, INCAPACI.IN_MOTIVO, INCAPACI.IN_NUMERO, '+
                                     K_PRETTYNAME + ' AS PrettyName, COLABORA.CB_SEGSOC '+
                                     'from INCAPACI '+
                                     'left outer join COLABORA on COLABORA.CB_CODIGO = INCAPACI.CB_CODIGO '+
                                     'left outer join INCIDEN on INCIDEN.TB_CODIGO = INCAPACI.IN_TIPO '+
                                     'where ( COLABORA.CB_PATRON = ''%s'' ) and ( INCIDEN.TB_INCAPA in ( 3, 5 ) ) '+
                                     'order by INCAPACI.CB_CODIGO, INCAPACI.IN_SUA_INI ';
          Q_DIAS_COTIZA: Result := 'select LS_DIAS_CO from LIQ_IMSS where '+
                                   '( LS_PATRON = ''%s'' ) and ( LS_YEAR = %d ) and ( LS_MONTH = :Mes ) '+
                                   'order by LS_TIPO desc';
          Q_PRIMA_BUSCA: Result := 'select COUNT(*) CUANTOS from PRIESGO where ( TB_CODIGO = ''%s'' ) and ( RT_FECHA = %s )';
          Q_PRIMA_AGREGA: Result := 'insert into PRIESGO( TB_CODIGO, RT_FECHA, RT_PRIMA, RT_PTANT, RT_S, RT_I, RT_D, RT_N, RT_F, RT_M ) ' +
                                    ' values ( ''%s'', %s, :RT_PRIMA, :RT_PTANT, :RT_S, :RT_I, :RT_D, :RT_N, :RT_F, :RT_M  )';
          Q_PRIMA_MODIFICA: Result := 'update PRIESGO set RT_PRIMA = :RT_PRIMA, RT_PTANT = :RT_PTANT, ' +
                                      'RT_S = :RT_S, RT_I = :RT_I, RT_D = :RT_D, RT_N = :RT_N, RT_F = :RT_F, RT_M = :RT_M '+
                                      'where ( TB_CODIGO = ''%s'' ) and ( RT_FECHA = %s )';
          Q_IMSS_REPETIDO: Result := 'select COUNT(*) CUANTOS, CB_SEGSOC ' +
                                     'from COLABORA ' +
                                     'where ( CB_SEGSOC <> %s ) and ( ' + K_FILTRO_REVISION_SUA + ' ) ' +
                                     'group by CB_SEGSOC ' +
                                     'having COUNT(*) > 1 ' +
                                     'order by CB_SEGSOC';
          Q_IMSS_REPETIDO_EMPLEADOS: Result := 'select CB_CODIGO from COLABORA ' +
                                               'where ( CB_SEGSOC = :SEGSOC ) and ( ' + K_FILTRO_REVISION_SUA + ' ) ' +
                                               'order by CB_CODIGO';

          {$ifdef INTERBASE}
          Q_ACUM_INFONAVIT: Result := 'select sum((select RESULTADO from SP_AS( CONCEPTO.CO_NUMERO, %d, %d, %d, :Empleado ))) RESULTADO from CONCEPTO where CO_NUMERO in (%s)';
          {$endif}
          {$ifdef MSSQL}
          Q_ACUM_INFONAVIT: Result := 'select COALESCE( sum( %s ), 0.0 ) as RESULTADO from ACUMULA where ( ACUMULA.CB_CODIGO = :Empleado ) and ( ACUMULA.AC_YEAR = %d ) and  ( ACUMULA.CO_NUMERO in (%s) )';
          {$endif}

          Q_MONTOS_OTROS_AJUSTES:  Result:= '( select SUM( CR_CARGO - CR_ABONO )'+
                                  ' from PCAR_ABO'+
                                  ' where ( CB_CODIGO = %s ) and ' +
                                  '( PR_TIPO = %s ) and ' +
                                  '( PR_REFEREN = %s ) and '+
                                  '( CR_FECHA between %s and %s ) )';
                                  
          Q_SALDO_ACTUAL_AJUSTE: Result:= '( select PRESTAMOAJUS.PR_SALDO'+
                                            ' from PRESTAMO PRESTAMOAJUS'+
                                            ' where ( PRESTAMOAJUS.CB_CODIGO = %s ) and ' +
                                            '( PRESTAMOAJUS.PR_TIPO = %s ) and ' +
                                            '( PRESTAMOAJUS.PR_REFEREN = %s ) )';
          Q_MONTO_AJUSTE_ULTIMO_MES: Result:= '( select CR_CARGO - CR_ABONO '+
                                              ' from PCAR_ABO'+
                                              ' where ( CB_CODIGO = %s ) and '+
                                              '( PR_TIPO = %s ) and ' +
                                              '( PR_REFEREN = %s ) and ' +
                                              '( CR_FECHA = %s ) )';
          Q_REEMPLAZAR_AJUSTE: Result:= '( select COUNT(*)' +
                                        ' from PCAR_ABO' +
                                        ' where ( CB_CODIGO = %s ) and ' +
                                        '( PR_TIPO = %s ) and ' +
                                        '( PR_REFEREN = %s ) and ' +
                                        '( CR_FECHA = %s ) ) = 0';

{$ifdef INTERBASE}
          Q_ACTUALIZAR_MOVIMIENTO_IDSE : Result :=  'select CB_CODIGO,RESPUESTA from SP_ACTUALIZAR_MOVIMIENTO_IDSE( :TIPO_MOV, :NSS_MOV, :FECHA_MOV, :LOTE_IDSE, :FECHA_IDSE, :US_CODIGO,:REG_PATRON )';
{$else}
          Q_ACTUALIZAR_MOVIMIENTO_IDSE : Result :=  'execute procedure SP_ACTUALIZAR_MOVIMIENTO_IDSE( :TIPO_MOV, :NSS_MOV, :FECHA_MOV, :LOTE_IDSE, :FECHA_IDSE, :US_CODIGO, :REG_PATRON ,:CB_CODIGO, :RESPUESTA )';
{$endif}
     else
         Result := '';
     end;
end;

{ *********** TdmServerIMSS ********** }

class procedure TdmServerIMSS.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
     if Register then
     begin
          inherited UpdateRegistry(Register, ClassID, ProgID);
          EnableSocketTransport(ClassID);
          EnableWebTransport(ClassID);
     end
     else
     begin
          DisableSocketTransport(ClassID);
          DisableWebTransport(ClassID);
          inherited UpdateRegistry(Register, ClassID, ProgID);
     end;
end;

procedure TdmServerIMSS.dmServerIMSSCreate(Sender: TObject);
begin
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
end;

procedure TdmServerIMSS.dmServerIMSSDestroy(Sender: TObject);
begin
     ZetaSQLBroker.FreeZetaCreator( oZetaCreator );
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
end;

{ ************** M�todos Privados ********************************** }

function TdmServerIMSS.GetSQLBroker: TSQLBroker;
begin
{$ifdef DOS_CAPAS}
     Result := ZetaSQLBroker.oZetaSQLBroker;
{$else}
     Result := Self.oZetaSQLBroker;
{$endif}
end;

{$ifdef DOS_CAPAS}
procedure TdmServerIMSS.CierraEmpresa;
begin
end;
{$endif}

procedure TdmServerIMSS.InitCreator;
begin
     if not Assigned( oZetaCreator ) then
        oZetaCreator := ZetaSQLBroker.GetZetaCreator( oZetaProvider );
end;

procedure TdmServerIMSS.InitBroker;
begin
     InitCreator;
     oZetaCreator.RegistraFunciones([efComunes]);
     oZetaSQLBroker := ZetaSQLBroker.GetSQLBroker( oZetaCreator );
end;

procedure TdmServerIMSS.ClearBroker;
begin
     ZetaSQLBroker.FreeSQLBroker( oZetaSQLBroker );
end;

procedure TdmServerIMSS.InitIMSS;
begin
     InitCreator;
     if not Assigned( FIMSS ) then
        FIMSS := TIMSS.Create( oZetaCreator );
end;

procedure TdmServerIMSS.ClearIMSS;
begin
     FreeAndNil( FIMSS );
end;

function TdmServerIMSS.GetScript( const eTipo: eTipoIMSS ): String;
begin
     case eTipo of
          eIMSSMovHisBim: Result := 'select A.CB_CODIGO, A.LE_TOT_RET, A.LE_TOT_INF, A.LE_DIAS_BM, '+
                                    K_PRETTYNAME + ' as PrettyName, B.CB_SEGSOC '+
                                    'from LIQ_EMP A '+
                                    'left outer join COLABORA B on ( A.CB_CODIGO = B.CB_CODIGO ) '+
                                    'where ( LS_YEAR = %d ) and ( LS_MONTH = %d ) and ( LS_TIPO = %d ) and ( LS_PATRON = ''%s'' ) %s';
          eIMSSMovHisMen: Result := 'select A.CB_CODIGO, A.LE_TOT_IMS, A.LE_DIAS_CO, '+
                                    K_PRETTYNAME + ' as PrettyName, B.CB_SEGSOC '+
                                    'from LIQ_EMP A '+
                                    'left outer join COLABORA B on ( A.CB_CODIGO = B.CB_CODIGO ) '+
                                    'where ( LS_YEAR = %d ) and ( LS_MONTH = %d ) and ( LS_TIPO = %d ) and ( LS_PATRON = ''%s'' ) %s';
          eIMSSMovDatosMen: Result := 'select LM_FECHA, LM_CLAVE, LM_DIAS, LM_INCAPAC, LM_AUSENCI, LM_BASE, '+
                                      'LM_EYM_FIJ, LM_EYM_EXC, LM_EYM_DIN, LM_EYM_ESP, LM_RIESGOS, LM_INV_VID, LM_GUARDER '+
                                      'from LIQ_MOV where ( LS_YEAR = %d ) and ( LS_MONTH = %d ) and ( LS_TIPO = %d ) and ( CB_CODIGO = %d ) and ( LS_PATRON = ''%s'' ) and ( LM_TIPO = ''M'' ) '+
                                      'order by LM_FECHA, LM_CLAVE';
          eIMSSMovDatosBim: Result := 'select LM_FECHA, LM_CLAVE, LM_DIAS, LM_INCAPAC, LM_AUSENCI, LM_BASE, '+
                                      'LM_RETIRO, LM_CES_VEJ, LM_INF_PAT, LM_INF_AMO '+
                                      'from LIQ_MOV where ( LS_YEAR = %d ) and ( LS_MONTH = %d ) and ( LS_TIPO = %d ) and ( CB_CODIGO = %d ) and ( LS_PATRON = ''%s'' ) and ( LM_TIPO = ''B'' ) '+
                                      'order by LM_FECHA, LM_CLAVE';
     else
         Result := '';
     end;
end;

procedure TdmServerIMSS.SetTablaInfo( const eTipo: ETipoIMSS );
begin
     with oZetaProvider.TablaInfo do
     begin
          case eTipo of
               eIMSSDatosBim, eIMSSDatosMen, eIMSSMovHisBim: SetInfo( 'LIQ_EMP', 'LE_APO_VOL,LE_CES_VEJ,LE_DIAS_BM,LE_DIAS_CO,LE_EYM_DIN,CB_CODIGO,LE_EYM_ESP,LE_EYM_EXC,LE_EYM_FIJ,LE_GUARDER, '+
                                        'LE_INF_AMO,LE_INF_PAT,LE_INV_VID,LE_RETIRO,LE_RIESGOS,LE_STATUS,LS_YEAR,LS_MONTH,LS_PATRON,LS_TIPO,LE_TOT_IMS,LE_TOT_RET, '+
                                        'LE_TOT_INF,LE_IMSS_OB,LE_IMSS_PA,LE_ACUMULA,LE_BIM_ANT,LE_BIM_SIG,LE_VA_PAGO,LE_VA_GOZO', 'LS_PATRON,LS_YEAR,LS_MONTH,LS_TIPO,CB_CODIGO' );
               eIMSSDatosTot, eIMSSHisBim, eIMSSHisMen: SetInfo( 'LIQ_IMSS', 'LS_ACT_AMO,LS_ACT_APO,LS_ACT_IMS,LS_ACT_INF,LS_ACT_RET,LS_APO_VOL,LS_CES_VEJ,LS_DIAS_BM,LS_DIAS_CO, '+
                                        'LS_EYM_DIN,LS_EYM_ESP,LS_EYM_EXC,LS_EYM_FIJ,LS_FAC_ACT,LS_FAC_REC,LS_FEC_REC,LS_GUARDER,LS_INF_ACR,LS_INF_AMO, '+
                                        'LS_INF_NAC,LS_INF_NUM,LS_INV_VID,LS_NUM_BIM,LS_NUM_TRA,LS_PATRON,LS_REC_AMO,LS_REC_APO,LS_REC_IMS,LS_REC_INF, '+
                                        'LS_REC_RET,LS_RETIRO,LS_RIESGOS,LS_STATUS,LS_YEAR,LS_MONTH,LS_TIPO,LS_SUB_IMS,LS_TOT_IMS,LS_TOT_MES,LS_SUB_RET, '+
                                        'LS_TOT_RET,LS_SUB_INF,LS_TOT_INF,US_CODIGO,LS_IMSS_OB,LS_IMSS_PA,LS_PARCIAL,LS_FINAN,LS_DES_20', 'LS_PATRON,LS_YEAR,LS_MONTH,LS_TIPO' );

               eImssConciliaInf: SetInfo( 'V_LIQ_EMP','LS_YEAR,LS_MONTH,LE_ACUMULA,LE_PROV,LE_VA_GOZO,LE_VA_PAGO,LE_INF_PAT,LE_INF_AMO,CB_CODIGO,LE_TOT_INF,LE_DIAS_BM,LE_BIM_SIG,LE_BIM_ANT','LS_YEAR,LS_MONTH,CB_CODIGO');
          end;
     end;
end;

{ ******** Interfases ( Paletitas ) ********** }

function TdmServerIMSS.GetPrimaRiesgo(Empresa: OleVariant; Fecha: TDateTime; const Patron: WideString): Double;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
     end;
     InitCreator;
     with oZetaCreator do
     begin
          PreparaPrimasIMSS;
          Result := dmPrimasIMSS.GetPrimaRiesgo( Fecha, Patron );
     end;
     SetComplete;
end;

function TdmServerIMSS.GetIMSSDatosBim(Empresa: OleVariant; Year, Mes, Tipo, EmpleadoNumero: Integer; const Patron: WideString): OleVariant;
begin
     SetTablaInfo( eIMSSDatosBim );
     with oZetaProvider do
     begin
          if ( StrLleno( Patron ) ) then
               TablaInfo.Filtro := Format( K_FILTRO_EMPLEADO, [ Year, Mes, Tipo, Patron, EmpleadoNumero ] )
          else
              TablaInfo.Filtro := Format( K_FILTRO_VARIOS_PATRONES, [ Year, Mes, EmpleadoNumero ] );

          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerIMSS.GetMovIMSSDatosBim(Empresa: OleVariant; Year, Mes, Tipo, EmpleadoNumero: Integer; const Patron: WideString): OleVariant;
var
   sScript: String;
begin
     sScript := Format( GetScript( eIMSSMovDatosBim ), [ Year, Mes, Tipo, EmpleadoNumero, Patron ] );
     Result := oZetaProvider.OpenSQL( Empresa, sScript, True );
     SetComplete;
end;

function TdmServerIMSS.GetIMSSDatosMen(Empresa: OleVariant; Year, Mes, Tipo, EmpleadoNumero: Integer; const Patron: WideString): OleVariant;
begin
     SetTablaInfo( eIMSSDatosMen );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( K_FILTRO_EMPLEADO, [ Year, Mes, Tipo, Patron, EmpleadoNumero ] );
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerIMSS.GetMovIMSSDatosMen(Empresa: OleVariant; Year, Mes, Tipo, EmpleadoNumero: Integer; const Patron: WideString): OleVariant;
var
   sScript: String;
begin
     sScript := Format( GetScript( eIMSSMovDatosMen ), [ Year, Mes, Tipo, EmpleadoNumero, Patron ] );
     Result := oZetaProvider.OpenSQL( Empresa, sScript, True );
     SetComplete;
end;

function TdmServerIMSS.GetIMSSDatosTot(Empresa: OleVariant; Year, Mes, Tipo: Integer; const Patron: WideString): OleVariant;
begin
     SetTablaInfo( eIMSSDatosTot );
     with oZetaProvider do
     begin
          with TablaInfo do
          begin
               Filtro := Format( K_FILTRO, [ Year, Mes, Tipo, Patron ] );
          end;
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerIMSS.GetIMSSHisMen(Empresa: OleVariant; Year, Mes, Tipo: Integer; const Patron: WideString): OleVariant;
begin
     SetTablaInfo( eIMSSHisMen );
     with oZetaProvider do
     begin
           TablaInfo.Filtro := Format( K_FILTRO, [ Year, Mes, Tipo, Patron ] );
           Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerIMSS.GetMovIMSSHisMen(Empresa: OleVariant; Year, Mes, Tipo: Integer; const Patron: WideString): OleVariant;
var
   sScript: String;
begin
     sScript := Format( GetScript( eIMSSMovHisMen ), [ Year, Mes, Tipo, Patron, Nivel0( Empresa ) ] );
     Result := oZetaProvider.OpenSQL( Empresa, sScript, True );
     SetComplete;
end;

function TdmServerIMSS.GetIMSSHisBim(Empresa: OleVariant; Year, Mes, Tipo: Integer; const Patron: WideString): OleVariant;
begin
     SetTablaInfo( eIMSSHisBim );
     with oZetaProvider do
     begin
           TablaInfo.Filtro := Format( K_FILTRO, [ Year, Mes, Tipo, Patron ] );
           Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerIMSS.GetMovIMSSHisBim(Empresa: OleVariant; Year, Mes, Tipo: Integer; const Patron: WideString): OleVariant;
var
   sScript: String;
begin
     sScript := Format( GetScript( eIMSSMovHisBim ), [ Year, Mes, Tipo, Patron, Nivel0( Empresa ) ] );
     Result := oZetaProvider.OpenSQL( Empresa, sScript, True );
     SetComplete;
end;

function TdmServerIMSS.GrabaIMSSDatosTot(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( eIMSSDatosTot );
     Result := oZetaProvider.GrabaTabla( Empresa, oDelta, ErrorCount );
     SetComplete;
end;

function TdmServerIMSS.GetDatosConciliaInfo(Empresa: OleVariant; ImssYear, ImssMes, Empleado: Integer): OleVariant;
begin
     SetTablaInfo( eImssConciliaInf );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( K_FILTRO_VARIOS_PATRONES, [ ImssYear, ImssMes, Empleado ] );
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

{ ********** Calcular Pagos IMSS ************ }

procedure TdmServerIMSS.BuildDatasetPagosIMSS( const sPatron: string; const iYear, iMes: Integer );
const
     {$ifdef INTERBASE}
     K_SQL_PATRON_INI = '( select CB_PATRON from SP_FECHA_KARDEX( %s, COLABORA.CB_CODIGO ) )';
     K_SQL_NOMINA_EMP = '( select CB_NOMINA from SP_FECHA_KARDEX( %s, COLABORA.CB_CODIGO ) )';
     {$endif}
     {$ifdef MSSQL}
     K_SQL_PATRON_INI = '( DBO.SP_KARDEX_CB_PATRON( %s, COLABORA.CB_CODIGO ) )';
     K_SQL_NOMINA_EMP = '( DBO.SP_KARDEX_CB_NOMINA( %s, COLABORA.CB_CODIGO ) )';
     {$endif}
     K_SQL_PATRON_MOV = '( select COUNT(*) from KARDEX K where ( K.CB_CODIGO = COLABORA.CB_CODIGO ) and ( K.CB_FECHA between %s and %s ) and ( K.CB_PATRON = %s ) )';

     K_FILTRO_COL_NOMINA =  '(  select SUM( %s ) from NOMINA where NOMINA.CB_CODIGO = COLABORA.CB_CODIGO and PE_YEAR = %d and ( %s ) ) ';

     K_FILTRO_PERIODO = 'select PE_NUMERO, PE_TIPO from periodo where ( PE_YEAR = %0:d ) and ( PE_MES in ( %1:s ) ) and ( PE_STATUS = %2:d ) order by PE_TIPO, PE_NUMERO ';

     K_ANCHO_LNAC = 25;
var
   dInicial, dFinal: TDate;
   sInicial, sFinal, sFiltroPeriodo, sMeses: string;

function GetColumnaNomina( const sColumna: String ): String;
begin
     Result:= Format( K_FILTRO_COL_NOMINA, [ sColumna, iYear, sFiltroPeriodo ] );
end;

function GetFiltroPeriodo( const sFiltro: String ): String;
const
     K_NINGUNO = '-1';
     QRY_CONCAT_FILTRO = '( ( PE_TIPO = %d ) AND ( PE_NUMERO IN ( %s ) ) )';
var
   FQryFiltroMonto: TZetaCursor;
   iTipoNomina: Integer;

     function GetListaPeriodos: String;
     begin
          Result:= VACIO;
          with oZetaProvider do
          begin
               with FQryFiltroMonto do
               begin
                    Repeat
                          Result:= ConcatString( Result,FieldByName('PE_NUMERO').AsString,',');
                          iTipoNomina:= FieldByName('PE_TIPO').AsInteger;
                          Next;
                    Until ( Eof or ( iTipoNomina <> FieldByName('PE_TIPO').AsInteger ) );
               end;
          end;
     end;

begin
     with oZetaProvider do
     begin
          FQryFiltroMonto:= CreateQuery ( sFiltro );
          try
             with FQryFiltroMonto do
             begin
                  Active:= TRUE;
                  if not ( IsEmpty ) then
                  begin
                       Repeat
                             iTipoNomina:= FieldByName('PE_TIPO').AsInteger;  //Se asigna al principio de concatenar
                             Result:= ConcatString( Result, Format( QRY_CONCAT_FILTRO, [ iTipoNomina, GetListaPeriodos ] ), ' OR ' );
                       Until ( Eof );
                  end;
                  Active:= FALSE;
                  //Si el query sobre peridos no da ningun periodo se concatenar� ( PE_NUMERO = -1 )
                  if ( StrVacio ( Result ) ) then
                     Result:= '( PE_NUMERO = -1 )';
             end;
          finally
                FreeAndNil( FQryFiltroMonto );
          end;
     end;
end;


begin
     DIMSSClass.FechasBimestre( iYear, iMes, dInicial, dFinal );
     sInicial := ZetaCommonTools.DateToStrSQLC( dInicial );
     sFinal := ZetaCommonTools.DateToStrSQLC( dFinal );

     if ( EsBimestre ( iMes ) ) then
        sMeses:= ConcatString ( IntToStr(iMes) , IntToStr( iMes - 1 ), ',' )
     else
         sMeses:= IntToStr( iMes );

     sFiltroPeriodo:= GetFiltroPeriodo( Format( K_FILTRO_PERIODO, [ iYear, sMeses, Ord( spAfectadaTotal ) ] ) );  // Dispara este evento solamente una vez para traerse el filtro sobre los periodos
     with oZetaProvider do
     begin
          with SQLBroker do
          begin
                Init( enEmpleado );
               with Agente do
               begin

                    AgregaColumna( 'CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
                    AgregaColumna( 'CB_RFC', True, Entidad, tgTexto, 30, 'CB_RFC' );
                    AgregaColumna( 'CB_CURP', True, Entidad, tgTexto, 30, 'CB_CURP' );
                    AgregaColumna( 'CB_APE_PAT', True, Entidad, tgTexto, 30, 'CB_APE_PAT' );
                    AgregaColumna( 'CB_APE_MAT', True, Entidad, tgTexto, 30, 'CB_APE_MAT' );
                    AgregaColumna( 'CB_NOMBRES', True, Entidad, tgTexto, 30, 'CB_NOMBRES' );
                    AgregaColumna( 'CB_NIVEL1', True, Entidad, tgTexto, 6, 'CB_NIVEL1' );
                    AgregaColumna( 'CB_INFCRED', True, Entidad, tgTexto, 30, 'CB_INFCRED' );
                    AgregaColumna( 'CB_SEGSOC', True, Entidad, tgTexto, 30, 'CB_SEGSOC' );
                    AgregaColumna( 'CB_SAL_INT', True, Entidad, tgFloat, 0, 'CB_SAL_INT' );
                    AgregaColumna( 'CB_FEC_INT', True, Entidad, tgFecha, 0, 'CB_FEC_INT' );
                    AgregaColumna( 'CB_INFTIPO', True, Entidad, tgNumero, 0, 'CB_INFTIPO' );
                    AgregaColumna( 'CB_INFTASA', True, Entidad, tgFloat, 0, 'CB_INFTASA' );
                    AgregaColumna( 'CB_INF_INI', True, Entidad, tgFecha, 0, 'CB_INF_INI' );
                    //AgregaColumna( 'CB_PATRON', True, Entidad, tgTexto, 1, 'CB_PATRON' );
                    AgregaColumna( Format( '%s', [ EntreComillas( sPatron ) ] ), True, Entidad, tgTexto, K_ANCHO_PATRON, 'CB_PATRON' );
                    AgregaColumna( 'CB_TURNO', True, Entidad, tgTexto, 6, 'CB_TURNO' );
                    AgregaColumna( 'CB_INF_OLD', True, Entidad, tgFloat, 0, 'CB_INF_OLD' );
                    AgregaColumna( Format( K_SQL_PATRON_INI, [ sInicial ] ), True, enNinguno, tgTexto, K_ANCHO_PATRON, 'PATRON_INI' );
                    AgregaColumna( Format( K_SQL_PATRON_MOV, [ sInicial, sFinal, EntreComillas( sPatron ) ] ), True, Entidad, tgNumero, 0, 'PATRON_MOV' );
                    AgregaColumna( 'CB_FEC_BSS', True, Entidad, tgFecha, 0, 'CB_FEC_BSS' );
                    //Datos Afilitaorios
                    AgregaColumna( 'CB_CODPOST', True, Entidad, tgTexto, K_ANCHO_REFERENCIA, 'CB_CODPOST' );
                    AgregaColumna( 'CB_FEC_NAC', True, Entidad, tgFecha, 0, 'CB_FEC_NAC' );
                    AgregaColumna( 'ENT_NAC.TB_ELEMENT', True, enEntidadNac, tgTexto, K_ANCHO_LNAC, 'ENT_NAC' );
                    AgregaColumna( 'CB_CLINICA', True, Entidad, tgTexto, K_ANCHO_CODIGO3, 'CB_CLINICA' );
                    AgregaColumna( 'CB_SEXO', True, Entidad, tgTexto, K_ANCHO_CODIGO1, 'CB_SEXO' );

                    AgregaColumna(  GetColumnaNomina('NO_DIAS_VA'), TRUE, Entidad, tgFloat, K_ANCHO_PESOS, 'K_NO_DIAS_VA' );
                    AgregaColumna(  GetColumnaNomina('NO_DIAS_VJ'), TRUE, Entidad, tgFloat, K_ANCHO_PESOS, 'K_NO_DIAS_VAGOZO' );
                    AgregaColumna( Format( K_SQL_NOMINA_EMP, [ sInicial ] ), True, enNinguno, tgNumero, 0, 'CB_NOMINA' );


                    //AgregaFiltro( '( CB_CODIGO = 292282 )', True, Entidad );

                    AgregaFiltro( Format( 'not ( ( CB_FEC_BSS > CB_FEC_ING ) and ( CB_FEC_BSS < ''%s'' ) )', [ ZetaCommonTools.DateToStrSQL( dInicial ) ] ), True, Entidad );
                    AgregaFiltro( Format( 'not ( ( CB_FEC_BSS is NULL ) and ( CB_FEC_ING > ''%s'' ) )', [ ZetaCommonTools.DateToStrSQL( dFinal ) ] ), True, Entidad );

                    AgregaOrden( 'CB_CODIGO', True, Entidad );

               end;
               AgregaRangoCondicion( ParamList );
               BuildDataset( EmpresaActiva );
          end;
     end;
end;

function TdmServerIMSS.CalcularPagosIMSSDataset( Dataset: TDataSet ): OleVariant;
var
   iEmpleado: TNumEmp;
   iYear, iMes: Integer;
   sPatron: TCodigo;
   eTipo: eTipoLiqIMSS;
   lAusentismos, lCuotaFija: Boolean;
   rSeguroVivienda, r_bim_ant, r_bim_sig, r_acumula,  r_vac_gozo, r_vac_pago, r_provision: TPesos;
   oQueryPer, oAcumuladoInfonavit, oAcumuladoProvision: TZetaCursor;
   dIniPerBim, dFinPerBim, dInicial, dFinal: TDate;
   eTipoNom: eTipoPeriodo;
const
     K_QRY_PERIODO_INI = 'select MIN( PE_FEC_INI ) PE_FEC_INI , MAX( PE_FEC_FIN ) PE_FEC_FIN from PERIODO where ( PE_YEAR = %0:d ) and ( PE_TIPO = :Tipo ) and ( PE_MES = :Mes )  ' +
                         'and ( PE_NUMERO < %1:d )';

procedure SetAcumuladoAmortizacion;
begin
     with oZetaProvider do
     begin
          ParamAsInteger( oAcumuladoInfonavit, 'Empleado', iEmpleado );
          with oAcumuladoInfonavit do
          begin
               Active := TRUE;
               r_acumula := ZetaCommonTools.Redondea( FieldByName( 'RESULTADO' ).AsFloat );
               Active := FALSE;
          end;
     end;
end;

procedure SetProvisionVacaciones;
begin
     with oZetaProvider do
     begin
          if StrLleno( GetGlobalString( K_GLOBAL_PROVISION_INFONAVIT ) ) then
          begin
               ParamAsInteger( oAcumuladoProvision, 'Empleado', iEmpleado );
               with oAcumuladoProvision do
               begin
                    Active := TRUE;
                    r_provision := ZetaCommonTools.Redondea( FieldByName( 'RESULTADO' ).AsFloat );
                    Active := FALSE;
               end;
          end
          else
              r_provision:= 0;
     end;
end;

procedure InitImssTemp;
begin
     FIMSSTemp := TIMSS.Create( oZetaCreator );
end;

procedure PreparaQuerysTemp;
var
   iMesIni, iMesFin: Integer;
   sScript,sConceptos,sScriptProvision: String;

   {$ifdef MSSQL}
   function  GetCamposAcumula: String;
   const
     K_CAMPO_ACUMULA = 'ACUMULA.AC_MES_%s';
   var
      i: Integer;
   begin
        Result := VACIO;
        for i := iMesIni to iMesFin do
            Result := ConcatString( Result, Format( K_CAMPO_ACUMULA, [ StrZero( IntToStr( i ), 2 ) ] ), '+' );
   end;
   {$endif}

   function GetConcepto( const sTempConcepto: String ): String;
   begin
        if StrVacio( sTempConcepto ) then
           Result:= '-1'
        else
            Result:= sTempConcepto;
   end;

begin
     with oZetaProvider do
     begin
          //Este query no considera n�minas especiales. Cambiar el filtro cuando se haga el merge para considerar el Global.
          oQueryPer := CreateQuery( Format( K_QRY_PERIODO_INI, [ iYear, K_LIMITE_NOM_NORMAL ] ) );
          if EsBimestre( iMes ) then
          begin
               iMesIni := iMes - 1;
               iMesFin := iMes;
          end
          else
          begin
               iMesIni := iMes;
               iMesFin := iMes + 1;
          end;

          sConceptos:= ConcatString( GetGlobalString( K_GLOBAL_CONCEPTOS_INFONAVIT ), GetGlobalString( K_GLOBAL_PROVISION_INFONAVIT ), ',' );


          {$ifdef INTERBASE}
          sScript:=  Format( GetSQLScript( Q_ACUM_INFONAVIT ),[ iMesIni,
                                                                iMesFin,
                                                                iYear,
                                                                GetConcepto( sConceptos ) ] );


          sScriptProvision:= Format( GetSQLScript( Q_ACUM_INFONAVIT ),[ iMesIni,
                                                                iMesFin,
                                                                iYear,
                                                                GetConcepto( GetGlobalString( K_GLOBAL_PROVISION_INFONAVIT ) ) ] );
          {$endif}
          {$ifdef MSSQL}
          sScript:=  Format( GetSQLScript( Q_ACUM_INFONAVIT ),[ GetCamposAcumula,
                                                                iYear,
                                                                GetConcepto( sConceptos ) ] );


          sScriptProvision:=  Format( GetSQLScript( Q_ACUM_INFONAVIT ),[ GetCamposAcumula,
                                                                         iYear,
                                                                         GetConcepto( GetGlobalString( K_GLOBAL_PROVISION_INFONAVIT ) ) ] );
          {$endif}

          oAcumuladoInfonavit := CreateQuery( sScript );
          oAcumuladoProvision := CreateQuery( sScriptProvision );
     end;
end;

procedure ClearImssTemp;
begin
     FreeAndNil( FIMSSTemp );
end;

procedure DespreparaQuerysTemp;
begin
     FreeAndNil( oAcumuladoInfonavit );
     FreeAndNil( oAcumuladoProvision );
     FreeAndNil( oQueryPer );
end;

procedure SetFechasBimestrePeriodo( const iYear, iMes: Integer );

    function GetFecha( const lEsInicio: Boolean; const iMesBim: Integer ): TDate;
    begin
         with oZetaProvider do
         begin
              with oQueryPer do
              begin
                   Active:= False;
                   ParamAsInteger( oQueryPer, 'Mes', iMesBim );
                   Active:= True;
                   if ( lEsInicio ) then
                   begin
                        Result:=  FieldByName('PE_FEC_INI').AsDateTime;
                   end
                   else
                       Result:=  FieldByName('PE_FEC_FIN').AsDateTime;
              end;
         end;

    end;

begin
     with oZetaProvider do
     begin
          with oQueryPer do
          begin
               Active:= False;
               ParamAsInteger( oQueryPer, 'Tipo', Ord( eTipoNom ) );

               if EsBimestre ( iMes )  then
                  //dIniPerBim:= GetFecha( TRUE, iMes - 1 ) + 1
                  dIniPerBim:= GetFecha( TRUE, iMes - 1 )
               else
                  dIniPerBim:= GetFecha( TRUE, iMes );

               //dFinPerBim:= GetFecha( FALSE, iMes )+1;
               dFinPerBim:= GetFecha( FALSE, iMes ) ;
          end;
     end;
end;

function GetTPeriodo: eTipoPeriodo;
begin
     Result:= eTipoPeriodo( Dataset.FieldByName('CB_NOMINA').AsInteger);
end;

procedure PreparaFechasEsquinas;
begin
     { PreparaFechasEsquinas }
     DImssClass.FechasBimestre( iYear, iMes, dInicial, dFinal );
     SetFechasBimestrePeriodo( iYear, iMes );
end;

procedure CalculaEsquinasBimestre;
var
   dFecIniEsquina, dFecFinEsquina: TDate;

     function ConsideradoEnNominas( const dFecha: TDate): Boolean;
     begin
          Result:=  ( dFecha <= dFinPerBim ) and ( dFecha >= dIniPerBim );
     end;

     function GetMontoEsquina( const dFechaInicial, dFechaFinal: TDate ): TPesos;
     begin
          with FIMSSTemp do
          begin
               EvaluarIMSSPagosBegin( TRUE, FALSE, 0, TheYear( dFechaInicial ), TheMonth( dFechaInicial ) );
               EvaluarIMSSPagosSetFechas( dFechaInicial, dFechaFinal, True );
               SetGlobalEmpleado( Dataset );
               CalcularIMSSPagos;

               { 1. Si la fecha de bimestre SI esta considerada en las nominas. Se resta
                 2. Si la fecha de bimestre NO esta considerada en las nominas. Se suma }

               if ConsideradoEnNominas( dFechaFinal ) then
                  Result :=  - LiquidacionEmpleado.LE_INF_AMO
               else
                   Result :=  LiquidacionEmpleado.LE_INF_AMO;
               EvaluarIMSSPagosEnd;
          end;
     end;

     procedure SetFechasIniFin( const lEsquinaAnterior: Boolean );
     begin
          if ( lEsquinaAnterior ) then
          begin
               dFecFinEsquina:= dMax( dInicial, dIniPerBim );
               dFecIniEsquina:= dMin( dInicial, dIniPerBim );
          end
          else
          begin
               dFecFinEsquina:= dMax( dFinal, dFinPerBim );
               dFecIniEsquina:= dMin( dFinal, dFinPerBim );
          end;
     end;

begin
     { CalculaEsquinasBimestre}

      { Esquina Anterior:
       1. Si la fecha del periodo es menor a la fecha inicial, el dia primero ya esta considerado en la amortizaci�n x lo tanto no se calcula
       2. Si la fecha del periodo es mayor a la fecha inicial, el 1er dia de la 1er semana ya esta considerado en las n�minas por lo tanto no se cuenta}
     SetFechasIniFin( TRUE );
     r_bim_ant:= GetMontoEsquina( dFecIniEsquina, dFecFinEsquina - 1 ); {Esquina que corresponde a bimestre anterior }

     { Esquina Siguiente:
       1. Si la fecha final del periodo es mayor a la fecha final del bimestre, significa que el dia final ya fue considerado en la amortizacion
       2. Si la fecha final del periodo es menor a la fecha final del bimestre, significa que el dia final del periodo ya fue considerado en la amortizacion}

     SetFechasIniFin( FALSE );
     r_bim_sig:=  GetMontoEsquina( dFecIniEsquina + 1, dFecFinEsquina );  {Esquina que corresponde a bimestre siguiente }
end;

procedure SetVacacionesEmpleado;
begin
     r_vac_pago:= DataSet.FieldByName('K_NO_DIAS_VA').AsFloat;
     r_vac_gozo:= DataSet.FieldByName('K_NO_DIAS_VAGOZO').AsFloat;
end;

procedure GetVariablesConciliacion;
begin
     SetAcumuladoAmortizacion; { Trae el acumulado del concepto de Infonavit }
     SetProvisionVacaciones; { Trae el acumulado del concepto de provisi�n }
     SetVacacionesEmpleado;   { Vacaciones gozadas y pagadas del bimestre }
     CalculaEsquinasBimestre; { Calcula los montos correspondientes al bimestre anterior y siguiente }
end;

begin
     with oZetaProvider do
     begin
          InitGlobales;
          with ParamList do
          begin
               sPatron := ParamByName( K_IMSS_PATRON ).AsString;
               eTipo := eTipoLiqIMSS( ParamByName( K_IMSS_TIPO ).AsInteger );
               iYear := ParamByName( K_IMSS_YEAR ).AsInteger;
               iMes := ParamByName( K_IMSS_MES ).AsInteger;
               lAusentismos := ParamByName( 'Ausentismos' ).AsBoolean;
               lCuotaFija := ParamByName( 'EnviarCuotaFija' ).AsBoolean;
               rSeguroVivienda := ParamByName( 'SeguroVivienda' ).AsFloat;
               //Lo inicializa fuera de rango para que no lo haga en Diario
               eTipoNom:= eTipoPeriodo( -1 );
          end;
          if OpenProcess( prIMSSCalculoPagos, Dataset.RecordCount, FListaParametros ) then
          begin
               try
                  { M�todos para los montos de las esquinas de Bimestre }
                  InitImssTemp;
                  PreparaQuerysTemp;

                  InitIMSS;
                  try
                     { Antes de empezar }
                     with FIMSS do
                     begin
                          CalcularIMSSPagosBegin( sPatron, iYear, iMes, eTipo, lAusentismos, lCuotaFija, rSeguroVivienda );
                     end;
                     { Ejecuci�n }
                     with Dataset do
                     begin
                          while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                          begin
                               iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                               if ( FieldByName( 'CB_NOMINA' ).AsInteger <> Ord( eTipoNom ) ) then
                               begin
                                    eTipoNom:= GetTPeriodo;
                                    PreparaFechasEsquinas;
                               end;
                               EmpiezaTransaccion;
                               try
                                  with FIMSS do
                                  begin
                                       SetGlobalEmpleado( Dataset );

                                       GetVariablesConciliacion;
                                       SetVariablesConciliacion( r_acumula, r_bim_ant, r_bim_sig, r_vac_gozo, r_vac_pago, r_provision );

                                       CalcularIMSSPagos;
                                  end;
                                  TerminaTransaccion( True );
                               except
                                     on Error: Exception do
                                     begin
                                          //TerminaTransaccion( False );
                                          RollBackTransaccion;
                                          Log.Excepcion( iEmpleado, 'Error Al Calcular Pago IMSS', Error );
                                     end;
                               end;
                               Next;
                          end;
                     end;
                     { Al terminar }
                     EmpiezaTransaccion;
                     try
                        with FIMSS do
                        begin
                             CalcularIMSSPagosEnd;
                        end;
                        TerminaTransaccion( True );
                     except
                           on Error: Exception do
                           begin
                                TerminaTransaccion( False );
                                Log.Excepcion( 0, 'Error Al Grabar Totales Pago IMSS', Error );
                           end;
                     end;
                  except
                        on Error: Exception do
                        begin
                             Log.Excepcion( 0, 'Error Al Calcular Pagos IMSS', Error );
                        end;
                  end;
               finally
                      ClearIMSS;
                      DespreparaQuerysTemp;
                      ClearImssTemp;
               end;
          end;
          Result := CloseProcess;
     end;
end;

procedure TdmServerIMSS.CalcularPagosIMSSParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Patr�n: ' + ParamByName( K_IMSS_PATRON ).AsString +
                         K_PIPE + 'Tipo: ' + ZetaCommonLists.ObtieneElemento( lfTipoLiqIMSS, ParamByName( K_IMSS_TIPO ).AsInteger ) +
                         K_PIPE + 'A�o: ' + ParamByName( K_IMSS_YEAR ).AsString +
                         K_PIPE + 'Mes: ' + ZetaCommonTools.MesConLetraMes( ParamByName( K_IMSS_MES ).AsInteger ) +
                         K_PIPE + 'Ausentismo con Fecha de Baja: ' + ZetaCommonTools.zBoolToStr( ParamByName( 'Ausentismos' ).AsBoolean ) +
                         K_PIPE + 'Enviar Como Cuota Fija: ' + ZetaCommonTools.zBoolToStr( ParamByName( 'EnviarCuotaFija' ).AsBoolean );
          if ZetaCommonTools.EsBimestre( ParamByName( K_IMSS_MES ).AsInteger ) then
             FListaParametros := FListaParametros + K_PIPE + 'Seguro de da�os a vivienda: ' +
                                 FormatFloat( '#,0.00', ParamByName( 'SeguroVivienda' ).AsFloat );
     end;
end;

function TdmServerIMSS.CalcularPagosIMSS(Empresa,Parametros: OleVariant): OleVariant;
begin
     InitBroker;
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList( Parametros );
             with ParamList do
             begin
                  BuildDatasetPagosIMSS( ParamByName( K_IMSS_PATRON ).AsString, ParamByName( K_IMSS_YEAR ).AsInteger, ParamByName( K_IMSS_MES ).AsInteger );
             end;
        end;
        CalcularPagosIMSSParametros;
        Result := CalcularPagosIMSSDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
            ClearBroker;
     end;
     SetComplete;
end;

{ ********** Calcular Recargos IMSS ************ }

procedure TdmServerIMSS.CalcularRecargosIMSSParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Patr�n: ' + ParamByName( K_IMSS_PATRON ).AsString +
                         K_PIPE + 'A�o: ' + ParamByName( K_IMSS_YEAR ).AsString +
                         K_PIPE + 'Mes: ' + ZetaCommonTools.MesConLetraMes( ParamByName( K_IMSS_MES ).AsInteger ) +
                         K_PIPE + 'Tipo: ' + ZetaCommonLists.ObtieneElemento( lfTipoLiqIMSS, ParamByName( K_IMSS_TIPO ).AsInteger ) +
                         K_PIPE + Format( 'Factor: %12.5n', [ ParamByName( 'Factor' ).AsFloat ] ) +
                         K_PIPE + Format( 'Tasa: %3.5n %', [ ParamByName( 'Tasa' ).AsFloat ] );
     end;
end;

function TdmServerIMSS.CalcularRecargosIMSS(Empresa, Parametros: OleVariant): OleVariant;
var
   FRecargos: TZetaCursor;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          FRecargos := CreateQuery( GetSQLScript( Q_CALCULA_RECARGOS ) );
          try
             with ParamList do
             begin
                  ParamAsChar( FRecargos, 'Patron', ParamByName( K_IMSS_PATRON ).AsString, K_ANCHO_PATRON );
                  ParamAsInteger( FRecargos, 'Year', ParamByName( K_IMSS_YEAR ).AsInteger );
                  ParamAsInteger( FRecargos, 'Mes', ParamByName( K_IMSS_MES ).AsInteger );
                  ParamAsInteger( FRecargos, 'Tipo', ParamByName( K_IMSS_TIPO ).AsInteger );
                  ParamAsFloat( FRecargos, 'Factor', ParamByName( 'Factor' ).AsFloat );
                  ParamAsFloat( FRecargos, 'Tasa', ParamByName( 'Tasa' ).AsFloat );
             end;
             CalcularRecargosIMSSParametros;
             if OpenProcess( prIMSSCalculoRecargos, 0, FListaParametros ) then
             begin
                  EmpiezaTransaccion;
                  try
                     Ejecuta( FRecargos );
                     TerminaTransaccion( True );
                  except
                        on Error: Exception do
                        begin
                             //TerminaTransaccion( False );
                             RollBackTransaccion;
                             Log.Excepcion( 0, 'Error Al Calcular Recargos', Error );
                        end;
                  end;
             end;
             Result := CloseProcess;
          finally
                 FreeAndNil( FRecargos );
          end;
     end;
     SetComplete;
end;

{ ********** Calcular Tasa INFONAVIT ************ }

procedure TdmServerIMSS.CalcularTasaINFONAVITBuildDataset;
const
     K_FILTRO_FECHA = '( ( ( CB_FEC_ING <= ''%s'' ) and ( ( CB_FEC_BAJ < CB_FEC_ING ) or ( CB_FEC_BAJ >= ''%s'' ) ) ) or ( ( CB_FEC_BAJ < CB_FEC_ING ) and ( CB_FEC_BAJ >= ''%s'' ) ) )';
var
   dInicial, dFinal: TDate;
   sFecha: String;
begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               DIMSSClass.FechasBimestre( ParamByName( 'Year' ).AsInteger,
                                          ParamByName( 'Mes' ).AsInteger,
                                          dInicial,
                                          dFinal );
          end;
          sFecha := DateToStrSQL( dInicial );
          with SQLBroker do
          begin
               Init( enEmpleado );
               with Agente do
               begin
                    AgregaColumna( 'CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
                    AgregaFiltro( Format( K_FILTRO_FECHA, [ sFecha, sFecha, sFecha ] ), True, Entidad );
                    AgregaFiltro( Format( '( CB_INFTIPO = %d )', [ Ord( tiPorcentaje ) ] ), True, Entidad );
                    AgregaFiltro( '( CB_INF_OLD > 0 )', True, Entidad );
                    AgregaOrden( 'CB_CODIGO', True, Entidad );
               end;
               AgregaRangoCondicion( ParamList );
               FiltroConfidencial( oZetaProvider.EmpresaActiva );
               BuildDataset( EmpresaActiva );
          end;
     end;
end;

function TdmServerIMSS.CalcularTasaINFONAVITDataset( Dataset: TDataset ): OleVariant;
var
   iEmpleado: TNumEmp;
   FKardex: TZetaCursor;
   dInicial, dFinal: TDate;
begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               DIMSSClass.FechasBimestre( ParamByName( 'Year' ).AsInteger,
                                          ParamByName( 'Mes' ).AsInteger,
                                          dInicial,
                                          dFinal );
          end;
          if OpenProcess( prIMSSTasaINFONAVIT, Dataset.RecordCount, FListaParametros ) then
          begin
               try
                  { Antes de empezar }
                  FKardex := CreateQuery( GetSQLScript( Q_ACTUALIZA_TASA ) );
                  try
                     ParamAsDate( FKardex, 'Fecha', dInicial );
                     ParamAsDate( FKardex, 'FechaCaptura', Date );
                     ParamAsInteger( FKardex, 'Usuario', UsuarioActivo );
                     { Ejecuci�n }
                     with Dataset do
                     begin
                          while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                          begin
                               iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                               EmpiezaTransaccion;
                               try
                                  ParamAsInteger( FKardex, 'Empleado', iEmpleado );
{$ifdef INTERBASE}
                                  with FKardex do
                                  begin
                                       Active := True;
                                       while not Eof do
                                       begin
                                            Next;
                                       end;
                                       Active := False;
                                  end;
{$else}
                                  Ejecuta( FKardex );
{$endif}
                                  TerminaTransaccion( True );
                               except
                                     on Error: Exception do
                                     begin
                                          //TerminaTransaccion( False );
                                          RollBackTransaccion;
                                          Log.Excepcion( iEmpleado, 'Error Al Calcular Tasa INFONAVIT', Error );
                                     end;
                               end;
                               Next;
                          end;
                     end;
                     { Al terminar }
                  finally
                         FreeAndNil( FKardex );
                  end;
               except
                     on Error: Exception do
                     begin
                          Log.Excepcion( 0, 'Error Al Calcular Tasas INFONAVIT', Error );
                     end;
              end;
          end;
          Result := CloseProcess;
     end;
end;

procedure TdmServerIMSS.CalcularTasaINFONAVITParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Mes: ' + ZetaCommonLists.ObtieneElemento( lfMeses, ParamByName( 'Mes' ).AsInteger ) +
                              K_PIPE + 'A�o: ' + ParamByName( 'Year' ).AsString;
     end;
end;

function TdmServerIMSS.CalcularTasaINFONAVIT(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitBroker;
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList( Parametros );
        end;
        CalcularTasaINFONAVITParametros;
        CalcularTasaINFONAVITBuildDataset;
        Result := CalcularTasaINFONAVITDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
            ClearBroker;
     end;
     SetComplete;
end;

{ ********** Revisar SUA ************ }

procedure TdmServerIMSS.RevisarSUABuildDataset;
var
   dInicial, dFinal: TDate;
   sInicial, sFinal: String;
begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               DIMSSClass.FechasBimestre( ParamByName( K_IMSS_YEAR ).AsInteger,
                                          ParamByName( K_IMSS_MES ).AsInteger,
                                          dInicial,
                                          dFinal );
               sInicial := ZetaCommonTools.DateToStrSQL( dInicial );
               sFinal := ZetaCommonTools.DateToStrSQL( dFinal );
          end;
          with SQLBroker do
          begin
               Init( enEmpleado );
               with Agente do
               begin
                    AgregaColumna( 'CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
                    AgregaColumna( 'CB_SEGSOC', True, Entidad, tgTexto, 30, 'CB_SEGSOC' );
                    AgregaColumna( 'CB_RFC', True, Entidad, tgTexto, 30, 'CB_RFC' );
                    AgregaColumna( 'CB_CURP', True, Entidad, tgTexto, 30, 'CB_CURP' );
                    //AgregaColumna( 'CB_PATRON', True, Entidad, tgTexto, 1, 'CB_PATRON' );
                    AgregaColumna( 'CB_PATRON', True, Entidad, tgTexto, K_ANCHO_PATRON, 'CB_PATRON' );
                    AgregaColumna( 'CB_ZONA_GE', True, Entidad, tgTexto, 1, 'CB_ZONA_GE' );
                    AgregaColumna( 'CB_APE_PAT', True, Entidad, tgTexto, 30, 'CB_APE_PAT' );
                    AgregaColumna( 'CB_APE_MAT', True, Entidad, tgTexto, 30, 'CB_APE_MAT' );
                    AgregaColumna( 'CB_NOMBRES', True, Entidad, tgTexto, 30, 'CB_NOMBRES' );
                    AgregaColumna( 'CB_INFCRED', True, Entidad, tgTexto, 30, 'CB_INFCRED' );
                    AgregaColumna( 'CB_INFTIPO', True, Entidad, tgNumero, 0, 'CB_INFTIPO' );
                    AgregaColumna( 'CB_INFTASA', True, Entidad, tgFloat, 0, 'CB_INFTASA' );
                    AgregaColumna( 'CB_SAL_INT', True, Entidad, tgFloat, 0, 'CB_SAL_INT' );
                    AgregaColumna( 'CB_INF_OLD', True, Entidad, tgFloat, 0, 'CB_INF_OLD' );
                    AgregaColumna( 'CB_FEC_INT', True, Entidad, tgFecha, 0, 'CB_FEC_INT' );
                    AgregaColumna( 'CB_FEC_ING', True, Entidad, tgFecha, 0, 'CB_FEC_ING' );
                    AgregaColumna( 'CB_FEC_BSS', True, Entidad, tgFecha, 0, 'CB_FEC_BSS' );
                    AgregaColumna( 'CB_FEC_NAC', True, Entidad, tgFecha, 0, 'CB_FEC_NAC' );
                    AgregaColumna( 'CB_ACTIVO', True, Entidad, tgTexto, 1, 'CB_ACTIVO' );
                    AgregaFiltro( Format( K_FILTRO_REVISION_SUA, [ sFinal, sInicial, sFinal ] ), True, Entidad );
                    AgregaOrden( 'CB_CODIGO', True, Entidad );
               end;
               FiltroConfidencial( oZetaProvider.EmpresaActiva );
               BuildDataset( EmpresaActiva );
          end;
     end;
end;

procedure TdmServerIMSS.RevisaSeguroSocialRepetido;
var
   dInicial, dFinal: TDate;
   sInicial, sFinal: String;
   FDataset, FDataSetEmpleados: TZetaCursor;
   oListaEmp: TStrings;

   procedure ReportaErroresRepetidos( const sImss: String; const iCuantos: Integer );
   const
        K_MESSAGE_REPETIDOS = '# de Seguro Social [%s] repetido %d veces';
        K_MESSAGE_LISTAEMPS = 'Empleados repetidos con # de Seguro Social : %s%s%s';
   var
      i: Integer;
      sHeader, sDetail: String;
   begin
        oListaEmp.Clear;
        // Llena lista de empleados
        with FDataSetEmpleados do
        begin
             Active := FALSE;
             oZetaProvider.ParamAsVarChar( FDataSetEmpleados, 'SEGSOC', sImss, K_ANCHO_DESCRIPCION );
             Active := TRUE;
             while ( not EOF ) do
             begin
                  oListaEmp.Add( FieldByName( 'CB_CODIGO' ).AsString );
                  Next;
             end;
        end;
        // Reporta un error por cada empleado con la misma lista de empleados en observaciones
        sHeader := Format( K_MESSAGE_REPETIDOS, [ sImss, iCuantos ] );
        sDetail := Format( K_MESSAGE_LISTAEMPS, [ sImss, CR_LF, oListaEmp.CommaText ] );
        with oListaEmp do
        begin
             for i := 0 to ( Count - 1 ) do    // Recorre la lista
             begin
                  oZetaProvider.Log.Error( StrToIntDef( Strings[i], 0 ), sHeader, sDetail );
             end;
        end;
   end;

begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               DIMSSClass.FechasBimestre( ParamByName( K_IMSS_YEAR ).AsInteger,
                                          ParamByName( K_IMSS_MES ).AsInteger,
                                          dInicial,
                                          dFinal );
               sInicial := ZetaCommonTools.DateToStrSQL( dInicial );
               sFinal := ZetaCommonTools.DateToStrSQL( dFinal );
          end;
          FDataset := CreateQuery( Format( GetSQLScript( Q_IMSS_REPETIDO ), [ EntreComillas( VACIO ),
                                                                              sFinal, sInicial, sFinal ] ) );
          FDataSetEmpleados := CreateQuery( Format( GetSQLScript( Q_IMSS_REPETIDO_EMPLEADOS ), [ sFinal, sInicial, sFinal ] ) );
          oListaEmp := TStringList.Create;
          try
             with FDataSet do
             begin
                  Active := TRUE;
                  while ( not EOF ) do
                  begin
                       ReportaErroresRepetidos( FieldByName( 'CB_SEGSOC' ).AsString, FieldByName( 'CUANTOS' ).AsInteger );
                       Next;
                  end;
             end;
          finally
                 FreeAndNil( oListaEmp );
                 FreeAndNil( FDataSetEmpleados );
                 FreeAndNil( FDataSet );
          end;
     end;
end;

function TdmServerIMSS.RevisarSUADataset( Dataset: TDataSet ): OleVariant;
var
   iEmpleado: TNumEmp;
begin
     with oZetaProvider do
     begin
          if OpenProcess( prIMSSRevisarSUA, Dataset.RecordCount ) then
          begin
               try
                  InitIMSS;
                  try
                     { Antes de empezar }
                     with FIMSS do
                     begin
                          RevisionSUABegin;
                     end;
                     { Ejecuci�n: Barrer empleados }
                     with Dataset do
                     begin
                          while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                          begin
                               iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                               try
                                  with FIMSS do
                                  begin
                                       RevisionSUA( Dataset );
                                  end;
                               except
                                     on Error: Exception do
                                     begin
                                          Log.Excepcion( iEmpleado, 'Error Al Revisar SUA', Error );
                                     end;
                               end;
                               Next;
                          end;
                     end;
                     { Al terminar }
                     with FIMSS do
                     begin
                          RevisionSUAEnd;
                     end;
                     { Revisar #'s de seguro social repetidos }
                     RevisaSeguroSocialRepetido;
                  except
                        on Error: Exception do
                        begin
                             Log.Excepcion( 0, 'Error Al Revisar SUA', Error );
                        end;
                  end;
               finally
                      ClearIMSS;
               end;
          end;
          Result := CloseProcess;
     end;
end;

function TdmServerIMSS.RevisarSUA(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitBroker;
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList( Parametros );
        end;
        RevisarSUABuildDataset;
        Result := RevisarSUADataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
            ClearBroker;
     end;
     SetComplete;
end;

{ ********** Exportar SUA ************ }

function TdmServerIMSS.ExportarSUADataset( Dataset: TDataSet ): OleVariant;
var
   iEmpleado: TNumEmp;
   iYear, iMes: Integer;
   sPatron: TCodigo;
   lEmpleados, lMovimientos, lHomoClave, lCuotaFija, lEnviarFaltaBaja, lValidarCURPvsRFC, lBorrarEmpleados, lSuspenFinBim: Boolean;
   BorrarMovimientos: eBorrarMovimSUA;
begin
     with oZetaProvider do
     begin
          InitGlobales;
          with ParamList do
          begin
               sPatron := ParamByName( 'Patron' ).AsString;
               iYear := ParamByName( 'Year' ).AsInteger;
               iMes := ParamByName( 'Mes' ).AsInteger;
               lHomoClave := ParamByName( 'IncluirHomoclave' ).AsBoolean;
               lCuotaFija := ParamByName( 'EnviarCuotaFija' ).AsBoolean;
               lEnviarFaltaBaja := ParamByName( 'EnviarFaltaBaja' ).AsBoolean;
               lEmpleados := ParamByName( 'EnviarEmpleados' ).AsBoolean;
               lMovimientos := ParamByName( 'EnviarMovimientos' ).AsBoolean;
               lValidarCURPvsRFC := ParamByName( 'ValidarCURPvsRFC' ).AsBoolean;
               lBorrarEmpleados := ParamByName( 'BorrarEmpleados' ).AsBoolean;
               BorrarMovimientos := eBorrarMovimSUA( ParamByName( 'BorrarMovimientos' ).AsInteger );
               lSuspenFinBim:= ParamByName('SuspenFinBim').AsBoolean;
          end;
          if OpenProcess( prIMSSExportarSUA, Dataset.RecordCount, FListaParametros ) then
          begin
               try
                  InitIMSS;
                  try
                     { Antes de empezar }
                     with FIMSS do
                     begin
                          ExportarSUABegin( sPatron, iYear, iMes, lHomoClave, lEnviarFaltaBaja, lCuotaFija, lValidarCURPvsRFC, lBorrarEmpleados, lSuspenFinBim, BorrarMovimientos, cdsLista, cdsDatos,cdsIncapacidades,cdsInfonavit );
                     end;
                     { Ejecuci�n }
                     with Dataset do
                     begin
                          while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                          begin
                               iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                               try
                                  with FIMSS do
                                  begin
                                       SetGlobalEmpleado( Dataset );
                                       ExportarSUA( lEmpleados, lMovimientos );
                                  end;
                               except
                                     on Error: Exception do
                                     begin
                                          Log.Excepcion( iEmpleado, 'Error Al Calcular Pago IMSS', Error );
                                     end;
                               end;
                               Next;
                          end;
                     end;
                     { Al terminar }
                     with FIMSS do
                     begin
                          ExportarSUAEnd;
                     end;
                  except
                        on Error: Exception do
                        begin
                             Log.Excepcion( 0, 'Error Al Calcular Pagos IMSS', Error );
                        end;
                  end;
               finally
                      ClearIMSS;
               end;
          end;
          Result := CloseProcess;
     end;
end;

procedure TdmServerIMSS.ExportarSUAParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Patr�n: ' + ParamByName( 'Patron' ).AsString +
                         K_PIPE + 'A�o: ' + ParamByName( 'Year' ).AsString +
                         K_PIPE + 'Mes: ' + ZetaCommonTools.MesConLetraMes( ParamByName( 'Mes' ).AsInteger );
          if ParamByName( 'EnviarEmpleados' ).AsBoolean then
          begin
               FListaParametros := FListaParametros + K_PIPE + 'Enviar Empleados' +
                                   K_PIPE + 'Archivo: ' + ParamByName( 'ArchivoEmpleados' ).AsString +
                                   K_PIPE + 'Borrar: ' + ZetaCommonTools.zBoolToStr( ParamByName( 'BorrarEmpleados' ).AsBoolean ) +
                                   K_PIPE + 'Homoclave: ' + ZetaCommonTools.zBoolToStr( ParamByName( 'IncluirHomoclave' ).AsBoolean ) +
                                   K_PIPE + 'Cuota Fija: ' + ZetaCommonTools.zBoolToStr( ParamByName( 'EnviarCuotaFija' ).AsBoolean );
          end;
          if ParamByName( 'EnviarMovimientos' ).AsBoolean then
          begin
               FListaParametros := FListaParametros + K_PIPE + 'Enviar Movimientos' +
                                   K_PIPE + 'Archivo: ' + ParamByName( 'ArchivoMovimientos' ).AsString +
                                   K_PIPE + 'Borrar: ' + ZetaCommonLists.ObtieneElemento( lfBorrarMovimSUA, ParamByName( 'BorrarMovimientos' ).AsInteger ) +
                                   K_PIPE + 'Bajas: ' + ZetaCommonTools.zBoolToStr( ParamByName( 'EnviarFaltaBaja' ).AsBoolean );
          end;
     end;
end;

function TdmServerIMSS.ExportarSUA(Empresa, Parametros: OleVariant;out Empleados, Movimientos, Infonavit,Incapacidades: OleVariant): OleVariant;
begin
     InitBroker;
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList( Parametros );
             with ParamList do
             begin
                  BuildDatasetPagosIMSS( ParamByName( 'Patron' ).AsString, ParamByName( 'Year' ).AsInteger, ParamByName( 'Mes' ).AsInteger );
             end;
        end;
        ExportarSUAParametros;
        Result := ExportarSUADataset( SQLBroker.SuperReporte.DataSetReporte );
        Empleados := cdsLista.Data;
        Movimientos := cdsDatos.Data;
        Incapacidades := cdsIncapacidades.Data;
        Infonavit := cdsInfonavit.Data;
     finally
            ClearBroker;
            cdsLista.Active := False;
            cdsDatos.Active := False;
            cdsIncapacidades.Active := False;
            cdsInfonavit.Active := False;
     end;
     SetComplete;
end;

{ ******** Calcular Prima Riesgo ********* }

procedure TdmServerIMSS.CalcularPrimaRiesgoParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Patr�n: ' + ParamByName( 'Patron' ).AsString +
                         K_PIPE + 'Archivo: ' + ParamByName( 'Archivo' ).AsString +
                         K_PIPE + 'A�o: ' + ParamByName( 'Year' ).AsString +
                         K_PIPE + Format( 'Prima: %12.5n', [ ParamByName( 'Prima' ).AsFloat ] ) +
                         K_PIPE + 'Fecha: ' + ZetaCommonTools.FechaCorta( ParamByName( 'FechaPrima' ).AsDateTime );
     end;
end;

function TdmServerIMSS.CalcularPrimaRiesgo(Empresa, Parametros: OleVariant; out Datos: OleVariant): OleVariant;
type
  TPromediosMensuales = array[ 1..12 ] of Integer;
const
     K_PROMEDIO_VIDA_ACTIVA = 28;
     K_FACTOR_PRIMA = 2.9;
     K_PRIMA_MINIMA = 0.0025;
     K_DIAS_ANIO = 365;
     K_CIEN_MIL = 100000;
var
   aPromedio: TPromediosMensuales;
   FRastreo: TStrings;
   FIncapacidades, FDias, FDataset: TZetaCursor;
   sPatron, sFecha: String;
   iYear: Integer;
   rPromedio, rDiasSubsidio, rPermanentes, rDefunciones: Currency;
   rPrePrima, rCalcPrima, rPrima, rOldPrima: Double;
   dFecha: TDate;
   lExiste, lAcreditaSTPS: Boolean;

function GetPrimaMinima: Double;
begin
     if ( iYear < K_YEAR_REFORMA ) then
        Result := K_PRIMA_MINIMA
     else
     begin
          case iYear of
               K_YEAR_REFORMA: Result := 0.0031;
               2003: Result := 0.0038;
               2004: Result := 0.0044;
          else
              Result := 0.0050;
          end;
     end;
end;

function GetFactorPrima: Double;
begin
     if ( iYear < K_YEAR_REFORMA ) then
        Result := K_FACTOR_PRIMA
     else
     begin
          case iYear of
               K_YEAR_REFORMA: Result := 2.7;
               2003: Result := 2.5;
               2004: Result := 2.3;
          else
              begin
                   if lAcreditaSTPS then
                      Result := 2.2
                   else
                       Result := 2.3;
              end;
          end;
     end;
end;

procedure LineaRastreo( const sLinea: String );
begin
     FRastreo.Add( sLinea );
end;

procedure MontoRastreo( const sLetrero: String; const rMonto: Currency );
begin
     LineaRastreo( sLetrero + FormatFloat( '#,0.00', rMonto ) );
end;

procedure PrimaRastreo( const sLetrero: String; const rPrima: Double );
begin
     LineaRastreo( sLetrero + FormatFloat( '0.00000 %', rPrima ) );
end;

procedure CalcCasos;
var
   iEmpleado: TNumEmp;
   iIncapacidad, iSumaDias: Integer;
   dInicial, dFinal, dYearStart, dYearEnd: TDate;
   eFinInca: eFinIncapacidad;
   rTasaIP: Currency;
   sNombre, sDescripcion: String;
   MotivoMedico: eMotivoIncapacidad;
begin
     rDiasSubsidio := 0;
     rPermanentes := 0;
     rDefunciones := 0;
     dYearStart := EncodeDate( iYear, 1, 1 );
     dYearEnd := EncodeDate( iYear, 12, 31 );
     with FIncapacidades do
     begin
          Active := True;
          LineaRastreo( '***** Casos de Incapacidad para Riesgo de Trabajo *****' );
          if ( Eof ) then
             LineaRastreo( '  No Hay ' )
          else
          begin
               while not Eof do
               begin
                    iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                    sNombre := FieldByName( 'PrettyName' ).AsString;
                    dInicial := FieldByName( 'IN_SUA_INI' ).AsDateTime; 
                    MotivoMedico := eMotivoIncapacidad(FieldByName( 'IN_MOTIVO' ).AsInteger);
                    dFinal := dInicial;
                    iIncapacidad := 0;
                    iSumaDias := 0;
                    eFinInca := fiNoTermina;
                    rTasaIP := 0;
                    while ( not Eof ) and ( FieldByName( 'CB_CODIGO' ).AsInteger = iEmpleado ) and
                          ( ( FieldByName( 'IN_SUA_INI' ).AsDateTime = dFinal ) or ( eFinInca = fiRecaida ) ) do
                    begin
                         dFinal := FieldByName( 'IN_SUA_FIN' ).AsDateTime;
                         eFinInca := eFinIncapacidad( FieldByName( 'IN_FIN' ).AsInteger );
                         rTasaIP := FieldByName( 'IN_TASA_IP' ).AsFloat;  
                         MotivoMedico := eMotivoIncapacidad (FieldByName( 'IN_MOTIVO' ).AsInteger);
                         iIncapacidad := iIncapacidad + 1;
                         iSumaDias := iSumaDias + FieldByName( 'IN_DIAS' ).AsInteger;
                         Next;
                    end;

                    {$ifdef ANTES} 
                    if ( dFinal >= dYearStart ) and ( dFinal <= dYearEnd ) and ( eFinInca <> fiRecaida ) then
                    {$else}
                    if ( dFinal >= dYearStart ) and ( dFinal <= dYearEnd ) and ( ( eFinInca <> fiRecaida ) and ( MotivoMedico = miAltaMedica ) ) then
                    {$endif}
                    begin
                         {
                         Result := Result + 1;
                         }
                         rDiasSubsidio := rDiasSubsidio + iSumaDias;
                         sDescripcion := Format( 'D�as : %d', [ iSumaDias ] );
                         if ( eFinInca = fiPermanente ) then
                         begin
                              rPermanentes := rPermanentes + rTasaIP;
                              sDescripcion := sDescripcion + ' -> Permanente : ' + FormatFloat( '0.0000 %', rTasaIP );
                         end
                         else
                             if ( eFinInca = fiMuerte ) then
                             begin
                                  rDefunciones := rDefunciones + 1;
                                  sDescripcion := sDescripcion + ' -> Muerte';
                             end;
                         LineaRastreo( IntToStr( iEmpleado ) + ' = ' + sNombre +
                                       '; Del ' + FormatDateTime( 'dd/mmm/yyyy', dInicial ) + ' al ' + FormatDateTime( 'dd/mmm/yyyy', dFinal ) +
                                       '; Registros ' + IntToStr( iIncapacidad ) + '; ' + sDescripcion );
                    end;
               end;
          end;
          Active := False;
     end;
end;

function CalcExpuestos: Currency;
var
   rPromedio: Currency;
   i: Integer;
begin
     rPromedio := 0;
     for i := 1 to 12 do
     begin
          with FDias do
          begin
               Active := False;
               oZetaProvider.ParamAsInteger( FDias, 'Mes', i );
               Active := True;
               aPromedio[ i ] := FieldByName( 'LS_DIAS_CO' ).AsInteger;
               rPromedio := rPromedio + aPromedio[ i ];
               Active := False;
          end;
     end;
     Result := ZetaCommonTools.Redondea( rPromedio / K_DIAS_ANIO );
end;

function ShowMeses: String;
var
   i: Integer;
begin
     Result := '';
     for i := 1 to 12 do
     begin
          Result := Result + FormatFloat( '#0', aPromedio[ i ] );
          if ( i < 12 ) then
             Result := Result + ',';
     end;
end;

begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          CalcularPrimaRiesgoParametros;
          if OpenProcess( prIMSSCalculoPrima, 0, FListaParametros ) then
          begin
               try
                  FRastreo := TStringList.Create;
                  try
                     with ParamList do
                     begin
                          sPatron := ParamByName( 'Patron' ).AsString;
                          iYear := ParamByName( 'Year' ).AsInteger;
                          dFecha := ParamByName( 'FechaPrima' ).AsDate;
                          rOldPrima := ParamByName( 'Prima' ).AsFloat;
                          lAcreditaSTPS := ParamByName( 'AcreditaSTPS' ).AsBoolean;
                     end;
                     sFecha := ZetaCommonTools.DateToStrSQLC( dFecha );
                     FIncapacidades := CreateQuery( Format( GetSQLScript( Q_INCAPACIDADES ), [ sPatron ] ) );
                     try
                        FDias := CreateQuery( Format( GetSQLScript( Q_DIAS_COTIZA ), [ sPatron, iYear ] ) );
                        try
                           CalcCasos;
                           rPromedio := CalcExpuestos;
                        finally
                               FreeAndNil( FDias );
                        end;
                     finally
                            FreeAndNil( FIncapacidades );
                     end;
                     if ( rPromedio = 0 ) then
                        rPromedio := 1; { Evita divisi�n por cero }
                     rPermanentes := rPermanentes / 100;
                     rCalcPrima := ( ( rDiasSubsidio / K_DIAS_ANIO ) +
                                     K_PROMEDIO_VIDA_ACTIVA *
                                     ( rPermanentes + rDefunciones ) ) *
                                   ( GetFactorPrima / rPromedio ) +
                                   GetPrimaMinima;
                     rCalcPrima := rCalcPrima * 100;
                     rPrePrima    := ZetaCommonTools.rMin( rCalcPrima, 15 );
                     if ( rOldPrima > 0 ) and ( Abs( rPrePrima - rOldPrima ) > 1 ) then
                     begin
                          if ( rPrePrima > rOldPrima ) then
                             rPrima := rOldPrima + 1
                          else
                              rPrima := rOldPrima - 1;
                     end
                     else
                         rPrima := rPrePrima;
                     FDataset := CreateQuery( Format( GetSQLScript( Q_PRIMA_BUSCA ), [ sPatron, sFecha ] ) );
                     try
                        with FDataset do
                        begin
                             Active := True;
                             lExiste := ( Fields[ 0 ].AsInteger > 0 );
                             Active := False;
                        end;
                     finally
                            FreeAndNil( FDataset );
                     end;
                     if lExiste then
                        FDataset := CreateQuery( Format( GetSQLScript( Q_PRIMA_MODIFICA ), [ sPatron, sFecha ] ) )
                     else
                         FDataset := CreateQuery( Format( GetSQLScript( Q_PRIMA_AGREGA ), [ sPatron, sFecha ] ) );
                     try
                        EmpiezaTransaccion;
                        ParamAsFloat( FDataset, 'RT_PRIMA', ZetaCommonTools.MiRound( K_CIEN_MIL * rPrima, 0 ) / K_CIEN_MIL );
                        ParamAsFloat( FDataset, 'RT_PTANT', ZetaCommonTools.MiRound( K_CIEN_MIL * rOldPrima, 0 ) / K_CIEN_MIL );
                        ParamAsFloat( FDataset, 'RT_S', rDiasSubsidio );
                        ParamAsFloat( FDataset, 'RT_I', rPermanentes );
                        ParamAsFloat( FDataset, 'RT_D', rDefunciones );
                        ParamAsFloat( FDataset, 'RT_N', rPromedio );
                        ParamAsFloat( FDataset, 'RT_F', GetFactorPrima );
                        ParamAsFloat( FDataset, 'RT_M', GetPrimaMinima );
                        Ejecuta( FDataset );
                        TerminaTransaccion( True );
                     finally
                            TerminaTransaccion( FALSE );
                            FreeAndNil( FDataset );
                     end;
                     LineaRastreo( '==============================================================' );
                     LineaRastreo( Format( 'FORMULA:    P = (( S / %d ) + V * ( I + D ) * ( F / N ) + M', [ K_DIAS_ANIO ] ) );
                     LineaRastreo( '' );
                     LineaRastreo( 'DIAS cotizados ANUALES=' );
                     LineaRastreo( '     ' + ShowMeses );
                     LineaRastreo( '' );
                     MontoRastreo( 'S = DIAS subsidiados p/temporales   = ', rDiasSubsidio );
                     MontoRastreo( 'V = Duraci�n Promedio Vida Activa   = ', K_PROMEDIO_VIDA_ACTIVA );
                     MontoRastreo( 'I = %s Incapacidades PERMANENTES    = ', rPermanentes );
                     MontoRastreo( 'D = NUMERO de DEFUNCIONES           = ', rDefunciones );
                     MontoRastreo( 'F = Factor de Prima                 = ', GetFactorPrima );
                     MontoRastreo( 'N = Trabajadores PROMEDIO expuestos = ', rPromedio );
                     PrimaRastreo( 'M = Prima de Riesgo MINIMA          = ', GetPrimaMinima );
                     LineaRastreo( '' );
                     PrimaRastreo( 'P = Prima de Riesgo Calculada       = ', rCalcPrima );
                     PrimaRastreo( '    Aplicando Tope de 15%           = ', rPrePrima );
                     PrimaRastreo( '(-) Prima de Riesgo Anterior        = ', rOldPrima );
                     PrimaRastreo( '(=) Diferencia entre Primas         = ', Abs( rPrePrima - rOldPrima ) );
                     LineaRastreo( '' );
                     PrimaRastreo( '* NUEVA PRIMA DE RIESGO DE TRABAJO  = ', rPrima );
                     LineaRastreo( '' );
                     Datos := FRastreo.Text;
                  finally
                         FRastreo.Free;
                  end;
               except
                     on Error: Exception do
                     begin
                          Log.Excepcion( 0, 'Error Al Calcular Prima de Riesgo', Error );
                     end;
               end;
          end;
          Result := CloseProcess;
     end;
     SetComplete;
end;

function TdmServerIMSS.AjusteRetInfonavit(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          InitGlobales;
          AsignaParamList( Parametros );
     end;
     AjusteRetInfonavitParametros;
     InitBroker;
     try
        AjusteRetInfonavitBuildDataSet;
        Result := AjusteRetInfonavitDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
        ClearBroker;
     end;
     SetComplete;
end;

function TdmServerIMSS.AjusteRetInfonavitGetLista(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          InitGlobales;
          AsignaParamList( Parametros );
     end;
     AjusteRetInfonavitParametros;
     InitBroker;
     try
        AjusteRetInfonavitBuildDataSet;
        Result := SQLBroker.SuperReporte.GetReporte;
     finally
            ClearBroker;
     end;
     SetComplete;
end;

function TdmServerIMSS.AjusteRetInfonavitLista(Empresa, Lista, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          InitGlobales;
          AsignaParamList( Parametros );
     end;
     AjusteRetInfonavitParametros;
     cdsLista.Lista := Lista;
     Result := AjusteRetInfonavitDataset( cdsLista );
     SetComplete;
end;

procedure TdmServerIMSS.AjusteRetInfonavitBuildDataSet;
const
     aRetencion: array[FALSE..TRUE] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( '( LE_ACUMULA + ( LE_BIM_ANT ) + ( LE_BIM_SIG ) )', 'LE_ACUMULA' );
     K_FILTRO_MES_YEAR = '( LS_MONTH = %d  and LS_YEAR = %d )';
var
   iYear, iMes: Integer;
   rTolerancia: TPesos;
   sTPrestamoAjus,sCampoRetencion: String;
   dFechaInicialBim, dFechaFinalBimestre: TDate;
   lReemplazar: Boolean;
begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               iYear:= ParamByName('Year').AsInteger;
               iMes:= MesBimestre( ParamByName('Bimestre').AsInteger );
               rTolerancia:= ParamByName('Tolerancia').AsFloat;
               lReemplazar:= ParamByName('AjusAnt').AsBoolean;
          end;

          sTPrestamoAjus:= GetGlobalString(K_GLOBAL_AJUSINFO_TPRESTA);
          dFechaInicialBim:= FirstDayOfBimestre( EncodeDate( iYear, iMes, 1 ) );
          dFechaFinalBimestre:= LastDayOfBimestre( EncodeDate( iYear, iMes, 1 ) );
          sCampoRetencion:= aRetencion[ IntToBool(GetGlobalInteger(K_GLOBAL_AJUSINFO_TCOMPARA))];
     end;

     with SQLBroker do
     begin
          Init( enVLiqEmp );
          with Agente do
          begin

               AgregaColumna( 'CB_CODIGO', TRUE, Entidad, tgNumero, 0, 'CB_CODIGO' );
               AgregaColumna( K_PRETTYNAME, TRUE, enEmpleado, tgTexto, K_ANCHO_PRETTYNAME, 'PRETTYNAME' );
               AgregaColumna( 'CB_INFCRED', TRUE, enEmpleado, tgTexto, K_ANCHO_DESCINFO, 'CB_INFCRED' );
               AgregaColumna( 'LE_INF_AMO', TRUE, Entidad, tgFloat, K_ANCHO_PESOS, 'K_AMORTIZA');
               AgregaColumna( sCampoRetencion, TRUE, Entidad, tgFloat, K_ANCHO_PESOS, 'K_ACUMULA');
               AgregaColumna( 'LE_DIAS_BM', TRUE, Entidad, tgFloat, K_ANCHO_PESOS, 'LE_DIAS_BM');
               AgregaColumna( 'LE_PROV', TRUE, Entidad, tgFloat, K_ANCHO_PESOS, 'LE_PROV');
               AgregaColumna( Format( GetSQLScript( Q_MONTOS_OTROS_AJUSTES ), ['V_LIQ_EMP.CB_CODIGO',
                                                                                EntreComillas( sTPrestamoAjus ),
                                                                                EntreComillas(K_REFEREN_PRESTA_AJUSTE),
                                                                                DateToStrSQLC(dFechaInicialBim),
                                                                                DateToStrSQLC(dFechaFinalBimestre - 1)
                                                                                ] ) ,TRUE, enNinguno, tgFloat, K_ANCHO_PESOS, 'K_OTROS_AJUSTES' );

               AgregaColumna( Format( GetSQLScript( Q_SALDO_ACTUAL_AJUSTE ), [ 'V_LIQ_EMP.CB_CODIGO',
                                                                                EntreComillas( sTPrestamoAjus ),
                                                                                EntreComillas(K_REFEREN_PRESTA_AJUSTE)] ), TRUE, enNinguno, tgFloat, K_ANCHO_PESOS, 'K_SALDO_ACTUAL_AJUSTE');


               AgregaColumna( Format ( GetSQLScript( Q_MONTO_AJUSTE_ULTIMO_MES ), ['V_LIQ_EMP.CB_CODIGO',
                                                                                   EntreComillas( sTPrestamoAjus ),
                                                                                   EntreComillas(K_REFEREN_PRESTA_AJUSTE),
                                                                                   DateToStrSQLC(dFechaFinalBimestre )] ), TRUE, enNinguno, tgFloat, K_ANCHO_PESOS, 'K_AJUS_ULIMO_D_MES' );

               AgregaColumna( '0', TRUE, enNinguno, tgFloat, K_ANCHO_PESOS, 'K_MONTO_AJUSTE' );
               AgregaColumna( '0', TRUE, enNinguno, tgFloat, K_ANCHO_PESOS, 'K_SALDO' );

               if not( lReemplazar ) then
                  AgregaFiltro( Format( GetSQLScript( Q_REEMPLAZAR_AJUSTE ), [ 'V_LIQ_EMP.CB_CODIGO',
                                                                               EntreComillas( sTPrestamoAjus ),
                                                                               EntreComillas(K_REFEREN_PRESTA_AJUSTE),
                                                                               DateToStrSQLC( dFechaFinalBimestre )] ) , True, Entidad);

               AgregaFiltro( Format( K_FILTRO_MES_YEAR, [ iMes, iYear ] ),True, Entidad );
               AgregaOrden( 'CB_CODIGO', True, Entidad );
          end;
          AgregaRangoCondicion( oZetaProvider.ParamList );
          FiltroConfidencial( oZetaProvider.EmpresaActiva );
          BuildDataset( oZetaProvider.EmpresaActiva );
          with SuperReporte.DataSetReporte do
          begin
               while not EOF do
               begin
                    if ( PesosIgualesP( FieldByName('K_ACUMULA').AsFloat +
                                        FieldByName('K_OTROS_AJUSTES').AsFloat, FieldByName('K_AMORTIZA').AsFloat, rTolerancia ) ) then
                    begin
                         Delete;
                    end
                    else
                    begin
                         Edit;
                         FieldByName('K_MONTO_AJUSTE').AsFloat:= FieldByName('K_AMORTIZA').AsFloat -
                                                                      FieldByName('K_ACUMULA').AsFloat -
                                                                      FieldByName('K_OTROS_AJUSTES').AsFloat;

                         FieldByName('K_SALDO').AsFloat:= FieldByName('K_SALDO_ACTUAL_AJUSTE').AsFloat -
                                                          FieldByName('K_AJUS_ULIMO_D_MES').AsFloat +
                                                          FieldByName('K_MONTO_AJUSTE').AsFloat ;
                         Next;
                    end;
               end;
          end;
     end;
end;

function TdmServerIMSS.AjusteRetInfonavitDataset( Dataset: TDataset): OleVariant;
const
     aStatusPresInfonavit: array[ FALSE..TRUE ] of Integer = ( Ord(spSaldado), Ord(spActivo) );
var
   iEmpleado: Integer;
   dFechaFinalBimestre: TDate;
   sTPrestaAjus,sTextoBitacora,sCreditoInfonavit: String;
   rMontoAjuste, rCargo, rAbono, rMontoUltimoDiaMes, rSaldoAjuste, rSaldoPagar, rMonto: TPesos;
   lStatusActivo, lEsCargo: Boolean;

   function ExistePrestamo: Boolean;
   begin
        with FQryPrestamo do
        begin
             Active := FALSE;
             with oZetaProvider do
             begin
                  ParamAsInteger( FQryPrestamo, 'CB_CODIGO', iEmpleado );
                  ParamAsString( FQryPrestamo, 'PR_TIPO', sTPrestaAjus);
                  ParamAsString( FQryPrestamo, 'PR_REFEREN', K_REFEREN_PRESTA_AJUSTE );
             end;
             Active := TRUE;
             Result := ( not IsEmpty );
        end;
   end;

   procedure DeleteCargoAbono;
   begin
        with oZetaProvider do
        begin
             ParamAsInteger( FDelCargo, 'CB_CODIGO', iEmpleado );
             ParamAsString( FDelCargo, 'PR_TIPO', sTPrestaAjus );
             ParamAsString( FDelCargo, 'PR_REFEREN', K_REFEREN_PRESTA_AJUSTE );
             ParamAsDate( FDelCargo, 'CR_FECHA', dFechaFinalBimestre );
             Ejecuta( FDelCargo );
        end;
   end;

   procedure AgregaPrestamo;
   begin
        with oZetaProvider do
        begin
             ParamAsDate( FInsPrestamo, 'PR_FECHA', dFechaFinalBimestre );
             ParamAsString( FInsPrestamo, 'PR_REFEREN', K_REFEREN_PRESTA_AJUSTE );
             ParamAsString( FInsPrestamo, 'PR_TIPO', sTPrestaAjus );
             ParamAsInteger( FInsPrestamo, 'US_CODIGO', UsuarioActivo );
             ParamAsInteger( FInsPrestamo, 'CB_CODIGO', iEmpleado );
             rMonto := FQryPrestamo.FieldByName('PR_MONTO').AsFloat;
             if rMontoAjuste < 0 then //Bug 16022
             begin
                  rSaldoPagar := rMontoAjuste;
                  rMontoAjuste := rMontoAjuste;    //
                  lStatusActivo := True;
                  ParamAsFloat( FInsPrestamo, 'PR_MONTO', rMontoAjuste );
             end
             else
                 ParamAsFloat( FInsPrestamo, 'PR_MONTO', rMonto );

             ParamAsInteger( FInsPrestamo, 'PR_STATUS', aStatusPresInfonavit[lStatusActivo] );
             ParamAsFloat( FInsPrestamo, 'PR_SALDO', rMontoAjuste );
             ParamAsString( FInsPrestamo, 'PR_OBSERVA', '# Cr�dito: '+ sCreditoInfonavit );
             if ( lEsCargo ) then
             begin
                  ParamAsFloat( FInsPrestamo, 'PR_CARGOS', Abs( rMontoAjuste ) );
                  ParamAsFloat( FInsPrestamo, 'PR_ABONOS', 0 );
             end
             else
             begin
                  if rMontoAjuste > 0 then
                  begin
                       ParamAsFloat( FInsPrestamo, 'PR_ABONOS', Abs( rMontoAjuste ) );
                  end
                  else
                  begin
                       ParamAsFloat( FInsPrestamo, 'PR_ABONOS', 0 );
                  end;

                  ParamAsFloat( FInsPrestamo, 'PR_CARGOS', 0 );
             end;
             Ejecuta ( FInsPrestamo );
        end;
   end;

   procedure ActualizaPrestamo;
   var
      rSaldo: TPesos;
      lAntesEraCargo: Boolean;
   begin
        with oZetaProvider do
        begin
             ParamAsInteger( FUpdPrestamo, 'CB_CODIGO', iEmpleado );
             ParamAsString( FUpdPrestamo, 'PR_TIPO', sTPrestaAjus );
             ParamAsString( FUpdPrestamo, 'PR_REFEREN', K_REFEREN_PRESTA_AJUSTE );
             rCargo := FQryPrestamo.FieldByName( 'PR_CARGOS' ).AsFloat;
             rAbono := FQryPrestamo.FieldByName( 'PR_ABONOS' ).AsFloat;
             rSaldo := FQryPrestamo.FieldByName('PR_SALDO').AsFloat;
             rMonto := FQryPrestamo.FieldByName('PR_MONTO').AsFloat;
             lAntesEraCargo:= ( rMontoUltimoDiaMes >= 0 );

             if ( lAntesEraCargo )  then
                rCargo := ( rCargo - rMontoUltimoDiaMes )
             else
                 rAbono := ( rAbono - Abs( rMontoUltimoDiaMes ) );

             if lEsCargo then
                rCargo := rCargo + rMontoAjuste
             else
                 rAbono := rAbono + Abs( rMontoAjuste );

             ParamAsFloat( FUpdPrestamo, 'PR_CARGOS', rCargo );


             if rMontoAjuste < 0 then //Bug 16022
             begin
                  rSaldoPagar := rMontoAjuste;
                  rMontoAjuste := (rMontoAjuste);
                  lStatusActivo := True;
                  ParamAsFloat( FUpdPrestamo, 'PR_MONTO', rMontoAjuste );
                  ParamAsFloat( FUpdPrestamo, 'PR_ABONOS', 0 );
             end
             else
             begin
                  ParamAsFloat( FUpdPrestamo, 'PR_ABONOS', rAbono );
                  ParamAsFloat( FUpdPrestamo, 'PR_MONTO', rMonto );
             end;

             ParamAsFloat( FUpdPrestamo, 'PR_SALDO', rSaldo - rMontoUltimoDiaMes + rMontoAjuste );
             ParamAsInteger( FUpdPrestamo, 'PR_STATUS', aStatusPresInfonavit[lStatusActivo] );       // Activar Prestamo
             ParamAsInteger( FUpdPrestamo, 'US_CODIGO', UsuarioActivo );
             Ejecuta( FUpdPrestamo );
        end;
   end;

   procedure AgregaCargoAbono;
   const
        K_COMENTA = 'Ajuste Infonavit';
   begin
        rAbono:= 0;
        rCargo:= 0;
        with oZetaProvider do
        begin
             ParamAsInteger( FInsCargo, 'CB_CODIGO', iEmpleado );
             ParamAsString( FInsCargo, 'PR_TIPO', sTPrestaAjus );
             ParamAsString( FInsCargo, 'PR_REFEREN', K_REFEREN_PRESTA_AJUSTE );
             ParamAsDate( FInsCargo, 'CR_CAPTURA', Date );
             ParamAsString( FInsCargo, 'CR_OBSERVA', K_COMENTA );
             ParamAsDate( FInsCargo, 'CR_FECHA', dFechaFinalBimestre );
             if ( lEsCargo ) then
                rCargo:= Abs( rMontoAjuste )
             else
                 rAbono:= Abs( rMontoAjuste );
             ParamAsFloat( FInsCargo, 'CR_ABONO', rAbono );
             ParamAsFloat( FInsCargo, 'CR_CARGO', rCargo );
             sTextoBitacora:= Format('Cargo: %s'+ CR_LF + 'Abono: %s ',[ FormatFloat('#0.00', rCargo), FormatFloat('#0.00', rAbono) ]);
             ParamAsInteger( FInsCargo, 'US_CODIGO', UsuarioActivo );
             Ejecuta( FInsCargo );
             EscribeBitacora( tbNormal,
                                   clbHisPrestamos,
                                   iEmpleado,
                                   dFechaFinalBimestre,
                                   Format('Agreg� ajuste de cr�dito #%s',[sCreditoInfonavit]) ,
                                   sTextoBitacora ) ;
        end;
   end;


begin
     { Acci�n!!!}
     with oZetaProvider do
     begin
          if OpenProcess( prIMSSAjusteRetInfonavit, DataSet.RecordCount, FListaParametros ) then
          begin
               PreparaPrestamosInfonavit;
               try
                  with DataSet do
                  begin
                       with ParamList do
                       begin
                            dFechaFinalBimestre:= LastDayOfBimestre( EncodeDate( ParamByName( 'Year' ).AsInteger, MesBimestre( ParamByName('Bimestre').AsInteger ), 1 )  );
                            sTPrestaAjus:= GetGlobalString(K_GLOBAL_AJUSINFO_TPRESTA);
                       end;
                       First;
                       while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                       begin
                            iEmpleado:= FieldByName('CB_CODIGO').AsInteger;
                            sCreditoInfonavit:= FieldByName('CB_INFCRED').AsString;
                            rSaldoAjuste:= FieldByName('K_SALDO').AsFloat;
                            lStatusActivo:= ( rSaldoAjuste > 0 );
                            rMontoAjuste:= FieldByName('K_MONTO_AJUSTE').AsFloat;
                            lEsCargo:= ( rMontoAjuste > 0 );
                            rMontoUltimoDiaMes:= FieldByName('K_AJUS_ULIMO_D_MES').AsFloat ;
                            EmpiezaTransaccion;
                            try
                               if ( ExistePrestamo ) then
                               begin
                                    DeleteCargoAbono;
                                    AgregaCargoAbono;
                                    ActualizaPrestamo;
                               end
                               else
                               begin
                                    AgregaPrestamo;
                                    AgregaCargoAbono;
                               end;
                               TerminaTransaccion( True );
                            except
                               on Error: Exception do
                               begin
                                    RollBackTransaccion;
                                    Log.Excepcion( iEmpleado, 'Error Al Agregar Pr�stamo de Ajuste Infonavit', Error );
                               end;
                            end;
                            Next;
                    end;
               end;
               finally
                      FreeAndNil( FQryPrestamo );
                      FreeAndNil( FInsCargo );
                      FreeAndNil( FInsPrestamo );
                      FreeAndNil( FUpdPrestamo );
                      FreeAndNil( FDelCargo );
               end;
          end;
          Result := CloseProcess;
     end;
end;

procedure TdmServerIMSS.AjusteRetInfonavitParametros;
begin
    with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'A�o: ' + ParamByName( 'Year' ).AsString +
                              K_PIPE + 'Bimestre: ' + ObtieneElemento( lfBimestres, ParamByName( 'Bimestre' ).AsInteger ) +
                              K_PIPE + 'Ajuste Anterior: ' + BoolToSiNo( ParamByName('AjusAnt').AsBoolean ) +
                              K_PIPE + 'Tolerancia: ' + ParamByName('Tolerancia').AsString;
     end;
end;

procedure TdmServerIMSS.PreparaPrestamosInfonavit;
const
     INFO_PRESTAMO = 'select PR_ABONOS,PR_CARGOS,PR_FECHA,PR_FORMULA,PR_MONTO,PR_NUMERO,PR_REFEREN,PR_SALDO_I,PR_STATUS,PR_TIPO,PR_TOTAL,PR_SALDO,US_CODIGO,CB_CODIGO,PR_OBSERVA from PRESTAMO ' +
                     'where CB_CODIGO = :CB_CODIGO and PR_TIPO = :PR_TIPO and PR_REFEREN = :PR_REFEREN';
     AGREGA_CARGO_PRESTAMO = 'insert into PCAR_ABO ( CB_CODIGO,CR_CAPTURA,PR_TIPO,CR_FECHA,PR_REFEREN,CR_OBSERVA,CR_ABONO,CR_CARGO,US_CODIGO ) ' +
                             'values ( :CB_CODIGO,:CR_CAPTURA,:PR_TIPO,:CR_FECHA,:PR_REFEREN,:CR_OBSERVA,:CR_ABONO,:CR_CARGO,:US_CODIGO )';
     AGREGA_PRESTAMO = 'insert into PRESTAMO (PR_FECHA,PR_REFEREN,PR_STATUS,PR_TIPO,US_CODIGO,CB_CODIGO, PR_SALDO, PR_CARGOS, PR_ABONOS, PR_OBSERVA, PR_MONTO ) '+
                                                'values ( :PR_FECHA,:PR_REFEREN,:PR_STATUS,:PR_TIPO,:US_CODIGO,:CB_CODIGO, :PR_SALDO, :PR_CARGOS, :PR_ABONOS, :PR_OBSERVA, :PR_MONTO )';
     ACTUALIZA_PRESTAMO = 'update PRESTAMO set PR_STATUS = :PR_STATUS, PR_CARGOS = :PR_CARGOS, PR_ABONOS = :PR_ABONOS, PR_SALDO = :PR_SALDO, US_CODIGO = :US_CODIGO, PR_MONTO = :PR_MONTO ' +
                          'where CB_CODIGO = :CB_CODIGO and PR_TIPO = :PR_TIPO and PR_REFEREN = :PR_REFEREN';
     DELETE_CARGO_ABONO = 'delete from PCAR_ABO where ( PR_TIPO = :PR_TIPO and PR_REFEREN = :PR_REFEREN and CR_FECHA = :CR_FECHA and CB_CODIGO = :CB_CODIGO )';

begin
     with oZetaProvider do
     begin
         if FQryPrestamo = nil then
            FQryPrestamo := CreateQuery( INFO_PRESTAMO );
         if FInsCargo = nil then
            FInsCargo := CreateQuery( AGREGA_CARGO_PRESTAMO );
         if FInsPrestamo = nil then
            FInsPrestamo := CreateQuery( AGREGA_PRESTAMO );
         if FUpdPrestamo = nil then
            FUpdPrestamo := CreateQuery( ACTUALIZA_PRESTAMO );
         if FDelCargo = nil then
            FDelCargo := CreateQuery ( DELETE_CARGO_ABONO );
    end;
end;

procedure TdmServerIMSS.ValidarMovimientosIDSEParametros;
begin
    with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Archivo: ' + ParamByName( 'Archivo' ).AsString +
                              K_PIPE + 'Lote: ' + ParamByName( 'LOTE_IDSE' ).AsString +
                              K_PIPE + 'Fecha de Transmisi�n: ' + FechaCorta( ParamByName('FECHA_IDSE').AsDateTime )+
                              K_PIPE + 'Registro Patronal: ' + ParamByName('REG_PATRON').AsString;
     end;
end;

function TdmServerIMSS.ValidarMovimientosIDSEDataSet(Dataset: TDataset): OleVariant;
const
     K_ITIPO_SUA_BAJA = 2; // '02';
     K_ITIPO_SUA_CAMBIO =  7; //'07';
     K_ITIPO_SUA_REINGRESO = 8; //'08';

     K_NO_EXISTE_EMPLEADO = -10;
     K_NSS_DUPLICADO      = -20;
     K_TIPO_MOV_INVALIDO  = -30;
     K_EXITO              = 0;
     K_NO_EXISTE_KARDEX   = 20;

     K_NO_EXISTE_EMPLEADO_TITULO = 'Empleado no existe';
     K_NO_EXISTE_EMPLEADO_MSG  = 'No se pudo validar el movimiento de %s ya que no existe un empleado con el NSS %s registrado en esta empresa';

     K_NSS_DUPLICADO_TITULO = 'Numero de S.S. Duplicado';
     K_NSS_DUPLICADO_MSG  = 'No se pudo validar el movimiento de %s ya que existe m�s de un empleado con el  NSS %s registrado en esta empresa';

     K_TIPO_MOV_INVALIDO_TITULO = 'Movimiento Inv�lido';
     K_TIPO_MOV_INVALIDO_MSG  = 'El Movimiento (%d) a la fecha reportada %s del empleado con NSS %s no puede ser validado en Sistema TRESS';

     K_NO_EXISTE_KARDEX_TITULO = 'Movimiento no existe';
     K_NO_EXISTE_KARDEX_MSG  = 'El movimiento de %s del empleado a la fecha reportada %s no pudo ser validado en Sistema TRESS';

     K_EXITO_TITULO = 'Se asign� Lote y Fecha de Transacci�n';
     K_EXITO_MSG = 'Se asign� Lote y Fecha de Transacci�n por Movimiento IDSE de %s de la fecha reportada %s';
var
   FActualizar: TZetaCursor;
   iEmpleado, iRespuesta, iTipoMov : integer;
   sLoteIDSE, sTipoMov, sFechaMov, sNSS,sRegPatron : string;
   dFechaIDSE, dFechaMov : TDateTime;

   function GetDescripcionMovimiento( iTipo : integer ) : string;
   begin
        case iTipo of
           K_ITIPO_SUA_BAJA :  Result := 'Baja' ;
           K_ITIPO_SUA_CAMBIO : Result := 'Cambio';
           K_ITIPO_SUA_REINGRESO : Result := 'Alta';
        else
            Result := '';
        end;
   end;

begin
     with oZetaProvider do
     begin
          sLoteIDSE  := ParamList.ParamByName( 'LOTE_IDSE' ).AsString;
          sRegPatron := ParamList.ParamByName( 'REG_PATRON' ).AsString;
          dFechaIDSE := ParamList.ParamByName( 'FECHA_IDSE' ).AsDateTime;

          if OpenProcess( prIMSSValidacionMovimientosIDSE, DataSet.RecordCount, FListaParametros ) then
          begin
               try
                  with DataSet do
                  begin
                       FActualizar := CreateQuery( GetSQLScript( Q_ACTUALIZAR_MOVIMIENTO_IDSE)  );
                       First;
                       while not Eof and CanContinue do
                       begin
                            iEmpleado := 0;

                            if FieldByName('VALIDO').AsBoolean then
                            begin
                                {
                                :TIPO_MOV, :NSS_MOV, :FECHA_MOV, :LOTE_IDSE, :FECHA_IDSE, :US_CODIGO,:REG_PATRON
                                }

                                iTipoMov := FieldByName('TIPO').AsInteger;
                                sTipoMov := GetDescripcionMovimiento( iTipoMov );
                                dFechaMov := FieldByName('FECHA').AsDateTime;
                                sFechaMov := FechaAsStr( dFechaMov );
                                sNSS := FieldByName('NSS').AsString ;

                                ParamAsInteger ( FActualizar, 'TIPO_MOV', iTipoMov );
                                ParamAsString  ( FActualizar, 'NSS_MOV',  sNSS  );
                                ParamAsDateTime( FActualizar, 'FECHA_MOV', dFechaMov );
                                ParamAsString  ( FActualizar, 'LOTE_IDSE',  sLoteIDSE  );
                                ParamAsDateTime( FActualizar, 'FECHA_IDSE', dFechaIDSE  );
                                ParamAsInteger ( FActualizar, 'US_CODIGO', UsuarioActivo );
                                ParamAsString  ( FActualizar, 'REG_PATRON',  sRegPatron );

                                EmpiezaTransaccion;
                                try
                                   {$ifdef INTERBASE}
                                     FActualizar.Active:=TRUE;
                                     iEmpleado := FActualizar.FieldByName( 'CB_CODIGO' ).AsInteger;
                                     iRespuesta := FActualizar.FieldByName( 'RESPUESTA' ).AsInteger;
                                     FActualizar.Active:=FALSE;
                                    {$else}
                                     ParamSalida( FActualizar, 'CB_CODIGO' );
                                     ParamSalida( FActualizar, 'RESPUESTA' );
                                     Ejecuta( FActualizar );
                                     iEmpleado := GetParametro( FActualizar, 'CB_CODIGO' ).AsInteger;
                                     iRespuesta := GetParametro( FActualizar, 'RESPUESTA' ).AsInteger;
                                   {$endif}
                                   TerminaTransaccion( True );

                                   case iRespuesta of
                                       K_NO_EXISTE_EMPLEADO :
                                          Log.Error(iEmpleado, K_NO_EXISTE_EMPLEADO_TITULO,Format(K_NO_EXISTE_EMPLEADO_MSG,[sTipoMov, sNSS]));
                                       K_NSS_DUPLICADO :
                                          Log.Error(iEmpleado, K_NSS_DUPLICADO_TITULO, Format(K_NSS_DUPLICADO_MSG,[sTipoMov, sNSS]) );
                                       K_TIPO_MOV_INVALIDO :
                                          Log.Error(iEmpleado,K_TIPO_MOV_INVALIDO_TITULO, Format(K_TIPO_MOV_INVALIDO_MSG,[FieldByName('TIPO').AsInteger, sFechaMov, sNSS]));
                                       K_EXITO :
                                          Log.Evento(clbNinguno,iEmpleado,dFechaMov,K_EXITO_TITULO,Format(K_EXITO_MSG,[sTipoMov, sFechaMov]));
                                       K_NO_EXISTE_KARDEX :
                                          Log.Advertencia(clbNinguno,iEmpleado,dFechaMov, K_NO_EXISTE_KARDEX_TITULO,Format(K_NO_EXISTE_KARDEX_MSG,[sTipoMov, sFechaMov]));
                                   else
                                       Log.Error( iEmpleado, 'Error NO Identificado', Format('Error NO Identificado Al Validar Movimientos IDSE (RESPUESTA=%d)',[iRespuesta]) );
                                   end;
                                except
                                   on Error: Exception do
                                   begin
                                        RollBackTransaccion;
                                        Log.Excepcion( iEmpleado, 'Error Al Validar Movimiento IDSE', Error );
                                   end;
                                end;
                            end;
                            Next;
                    end;
               end;
               finally
                      FreeAndNil( FActualizar );
               end;
          end;
          Result := CloseProcess;
     end;
end;

function TdmServerIMSS.ValidarMovimientosIDSE(Empresa, Lista, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          InitGlobales;
          AsignaParamList( Parametros );
     end;
     ValidarMovimientosIDSEParametros;
     cdsLista.Lista := Lista;
     Result := ValidarMovimientosIDSEDataSet( cdsLista );
     SetComplete;
end;

{$ifndef DOS_CAPAS}
initialization
  TComponentFactory.Create(ComServer, TdmServerIMSS, Class_dmServerIMSS, ciMultiInstance, ZetaServerTools.GetThreadingModel);
{$endif}
end.