unit DPrestamosServer;

interface


uses Classes, DB, DBClient, Windows, Messages, SysUtils, Graphics, Controls, Forms, Dialogs,
     ComServ, ComObj, VCLCom, StdVcl, DataBkr, MtsRdm, Mtx, Mask,
     StrUtils,
     Variants,MaskUtils,
     DZetaServerProvider,
{$ifndef DOS_CAPAS}
     Recursos_TLB,
{$endif}
     ZetaServerDataSet,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZAgenteSQLClient,
{$ifdef RDD}
{$else}
     ZAgenteSQL,
{$endif}
     DSeguridad,
     ZCreator,
     ZetaSQLBroker;

type
    eScript = (
                 eReglasPrestamo,
                 eFechasTAhorro,
                 eCountPrestamo
              );



type
  TPrestamosServer = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
  { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
{$ifndef DOS_CAPAS}
    oZetaCreator: TZetaCreator;
    oZetaSQLBroker: TSQLBroker;
{$endif}
    cdsPrestamosEmp: TServerDataSet;
    FForzarGrabaPrestamo: Boolean;
    FMensajePrestamo :string;
    function GetSQLBroker: TSQLBroker;
    procedure ClearBroker;
    procedure InitBroker;
    procedure InitCreator;

  protected
    property SQLBroker: TSQLBroker read GetSQLBroker;
    function GetScript(const eTipoScript: eScript): String;
    function GetExpresionAntig( const iDiasAntig: Integer; const sFormula, sReferencia: String ): string;

  public
    property ZetaProvider: TdmZetaServerProvider read oZetaProvider;
    property ForzarGrabaPrestamo: Boolean read FForzarGrabaPrestamo write FForzarGrabaPrestamo;
    property MensajePrestamo :string read FMensajePrestamo;
    procedure ValidaReglasPrestamos( UpdateKind: TUpdateKind; DeltaDS: TzProviderClientDataSet ;const lLevantaExcepcion:Boolean = True );

  end;

var
  PrestamosServer: TPrestamosServer;

implementation

uses ZetaCommonTools,
     ZGlobalTress,
     ZetaTipoEntidad,
     ZetaTipoEntidadTools,
     ZetaServerTools,
     ZetaSQL,
     DSuperASCII,
     ZGlobalSeleccion,
     ZEvaluador;

{$R *.dfm}


function TPrestamosServer.GetSQLBroker: TSQLBroker;
begin
{$ifdef DOS_CAPAS}
     Result := ZetaSQLBroker.oZetaSQLBroker;
{$else}
     Result := Self.oZetaSQLBroker;
{$endif}
end;


procedure TPrestamosServer.InitCreator;
begin
     if not Assigned( oZetaCreator ) then
        oZetaCreator := ZetaSQLBroker.GetZetaCreator( oZetaProvider );
end;

procedure TPrestamosServer.InitBroker;
begin
     InitCreator;
     oZetaCreator.RegistraFunciones([efComunes,efReglasPrestamos]);
     oZetaSQLBroker := ZetaSQLBroker.GetSQLBroker( oZetaCreator );
end;


procedure TPrestamosServer.ClearBroker;
begin
     ZetaSQLBroker.FreeSQLBroker( oZetaSQLBroker );
end;


procedure TPrestamosServer.ValidaReglasPrestamos( UpdateKind: TUpdateKind; DeltaDS: TzProviderClientDataSet ;const lLevantaExcepcion:Boolean = True );
 procedure FiltraDataset( const sFiltro : string; const lDejaAnterior : Boolean= TRUE );
 begin
      with cdsPrestamosEmp do
      begin
           Filtered := FALSE;

           if NOT lDejaAnterior then
              Filter := VACIO;

           Filter := ConcatFiltros( Filter, sFiltro );
           Filtered := TRUE;
      end
 end;
 function CambioCamposReglas: Boolean;
 begin
      Result := ZetaServerTools.CambiaCampo( DeltaDS.FieldByName('PR_TIPO') ) or
                ZetaServerTools.CambiaCampo( DeltaDS.FieldByName('PR_FECHA') ) or
                ZetaServerTools.CambiaCampo( DeltaDS.FieldByName('PR_MONTO') ) or
                ZetaServerTools.CambiaCampo( DeltaDS.FieldByName('PR_STATUS') ) ;
 end;


var
   oReglas, oTAhorro, oPrestamo : TZetaCursor;
   dInicial, dFinal: TDate ;
   lEmpActivo, lHayPrestamos, lCambioTipoCodigo, lCambioStatus, lContinua : Boolean;
   oLista, oListaMensajes: TStrings;
   sMensaje, sFormula,sPreTopeName: string;
   rTope, rTopeRegla: TPesos;
   iYear: integer;

 procedure AgregaMensaje( const sMensaje: string );
 begin
      oLista.Add(sMensaje);
 end;

begin
     oZetaProvider.InitGlobales;

     if oZetaProvider.GetGlobalBooleano(K_GLOBAL_VALIDAR_PRESTAMOS) then
     begin
          //Si el prestamo esta siendo Editado o Agregado, se Valida contra las Reglas.
          //Cuando se borra un registro no se aplican las reglas
          if NOT FForzarGrabaPrestamo and                  // --> Si el usuario tiene derecho puede grabar el prestamo aunque no pase las reglas
             ( DeltaDs.DataSetField = NIL ) and            // --> Si lo que cambio es el Dataset de CarAbo, no se aplican las reglas.
             ( ( UpdateKind = ukInsert) or                 // --> Cuando se agrega un prestamo se valida siempre
               ( ( UpdateKind = ukModify ) and ( CambioCamposReglas ) )) then //--> Cuando se modifica el registro, Solamente se aplican reglas cuando cambian ciertos campos
          begin
               FMensajePrestamo := VACIO;
               with oZetaProvider.DatosPrestamo do
               begin
                    Empleado := ZetaServerTools.CampoAsVar( DeltaDS.FieldByName('CB_CODIGO') );
                    Tipo := ZetaServerTools.CampoAsVar( DeltaDS.FieldByName('PR_TIPO') );
                    OldTipo := ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName('PR_TIPO') );
                    Referencia := ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName('PR_REFEREN') );
                    Fecha := ZetaServerTools.CampoAsVar( DeltaDS.FieldByName('PR_FECHA') );
                    Monto := ZetaServerTools.CampoAsVar( DeltaDS.FieldByName('PR_MONTO') );
                    //lCambioTipoCodigo := (OldTipo <> Tipo);

                    if DeltaDS.FindField('PR_STATUS') <> NIL then
                    begin
                         Status := ZetaServerTools.CampoAsVar( DeltaDS.FieldByName('PR_STATUS') );
                         OldStatus := ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName('PR_STATUS') );
                         lCambioStatus := (OldStatus<> Status);
                    end
                    else
                    begin
                        Status := spActivo;
                        OldStatus := spActivo;
                        lCambioStatus := TRUE;
                    end;

                    //Cuenta los prestamos del empleado, porque a lo mejor no tiene ninguno
                    oZetaProvider.EmpiezaTransaccion;
                    try
                       oPrestamo := oZetaProvider.CreateQuery( Format( GetScript( eCountPrestamo ), [Empleado] ) );
                       oPrestamo.Active := TRUE;
                       lHayPrestamos := oPrestamo.Fields[0].AsInteger >0;
                       oPrestamo.Active := FALSE;
                    finally
                           oZetaProvider.TerminaTransaccion(FALSE);
                    end;
               end;
               lEmpActivo := FALSE;
               //Solo se trae la reglas activa, que aplican a todos los prestamos, o bien, que aplique a este tipo de prestamo.
               oReglas := oZetaProvider.CreateQuery( Format( GetScript( eReglasPrestamo ),
                                                             [ //FechaAsStr(( ZetaServerTools.CampoAsVar( DeltaDS.FieldByName('PR_FECHA')) ) ),
                                                               //oZetaProvider.DatosPrestamo.Tipo,
                                                               oZetaProvider.DatosPrestamo.Tipo ] ) );
               oTAhorro  := oZetaProvider.CreateQuery( Format( GetScript( eFechasTAhorro ), [ oZetaProvider.DatosPrestamo.Tipo ] ) );
               oLista := TStringList.Create;
               oListaMensajes := TStringList.Create;
               cdsPrestamosEmp:= TServerDataSet.Create(oZetaProvider);

               try
                  with oReglas do
                  begin
                       Open;
                       try
                          if NOT IsEmpty then
                          begin
                               InitBroker;
                               try
                                  //Se mete al evaluador de expresiones
                                  with SQLBroker do
                                  begin
                                       if lHayPrestamos then
                                          Init( enPrestamo )
                                       else
                                           Init( enEmpleado );

                                       with Agente do
                                       begin
                                            //Datos del Empleado
                                            AgregaColumna( 'COLABORA.CB_CODIGO', True, enEmpleado, tgNumero, 0, 'CB_CODIGO' );
                                            AgregaColumna( 'COLABORA.CB_ACTIVO', True, enEmpleado, tgTexto, 1, 'CB_ACTIVO' );


                                            //Datos del Prestamo
                                            if lHayPrestamos then
                                            begin
                                                 AgregaColumna( 'PRESTAMO.PR_REFEREN', True, Entidad, tgTexto, 20, 'PR_REFEREN' );
                                                 AgregaColumna( 'PRESTAMO.PR_TIPO', True, Entidad, tgTexto, 6, 'PR_TIPO' );
                                                 AgregaColumna( 'PRESTAMO.PR_STATUS', True, Entidad, tgNumero, 0, 'PR_STATUS' );
                                                 AgregaColumna( 'PRESTAMO.PR_FECHA', True, Entidad, tgFecha, 0, 'PR_FECHA' );
                                                 AgregaColumna( 'PRESTAMO.PR_MONTO', True, Entidad, tgFloat, 0, 'PR_MONTO' );
                                            end;

                                            //Validacion del Tope del prestamo, la antiguedad del empleado, filtros y condiciones
                                            First;
                                            while NOT EOF do
                                            begin
                                                 if ( FieldByName('RP_LISTA').AsString = 'N' ) or
                                                   ( ( FieldByName('RP_LISTA').AsString = 'S' ) AND
                                                     ( FieldByName('TB_CODIGO').AsString = oZetaProvider.DatosPrestamo.Tipo ) ) then
                                                 begin
                                                      if StrLleno( FieldByName('RP_TOPE').AsString ) then
                                                         sFormula := FieldByName('RP_TOPE').AsString
                                                      else
                                                          sFormula := '0';

                                                      AgregaColumna( sFormula, FALSE, Entidad, tgNumero, 0, 'PRE_TOPE_' + FieldByName('RP_CODIGO').AsString );

                                                      //Validacion de las Condiciones y Filtros
                                                      AgregaColumna( FieldByName('RP_FILTRO').AsString, FALSE, Entidad, tgBooleano, 2, 'PRE_FILTRO_'  + FieldByName('RP_CODIGO').AsString);
                                                      AgregaColumna( FieldByName('QU_FILTRO').AsString, FALSE, Entidad, tgBooleano, 2, 'PRE_CONDICION_'  + FieldByName('RP_CODIGO').AsString);

                                                      //Se valida la antiguedad del empleado.
                                                      AgregaColumna( ConcatFiltros( GetExpresionAntig( FieldByName( 'RP_ANT_INI' ).AsInteger, 'ANTIG( %s, FALSE ) >= %d', EntreComillas( FechaAsStr( Date ) ) ),
                                                                                    GetExpresionAntig( FieldByName( 'RP_ANT_FIN' ).AsInteger, 'ANTIG( %s, FALSE ) <= %d', EntreComillas( FechaAsStr( Date ) ) ) ), FALSE, Entidad, tgTexto, 2, 'PRE_ANTIG_'  + FieldByName('RP_CODIGO').AsString);


                                                 end;
                                                 Next;


                                            end;

                                            //Se Filtra por el Empleado
                                            if lHayPrestamos then
                                               AgregaFiltro( Format( 'PRESTAMO.CB_CODIGO= %d', [oZetaProvider.DatosPrestamo.Empleado] ), TRUE, Entidad )
                                            else
                                                AgregaFiltro( Format( 'COLABORA.CB_CODIGO= %d', [oZetaProvider.DatosPrestamo.Empleado] ), TRUE, Entidad );


                                            //Se ordena por Tipo y Referencia
                                            if lHayPrestamos then
                                            begin
                                                 AgregaOrden( 'PRESTAMO.PR_TIPO', True, Entidad );
                                                 AgregaOrden( 'PRESTAMO.PR_REFEREN', True, Entidad );
                                            end;
                                       end;
                                       //cv: ES NECESARIO EL FILTRO CONFIDENCIAL??
                                       //FiltroConfidencial( oZetaProvider.EmpresaActiva );
                                       try
                                          lContinua := BuildDataset( oZetaProvider.EmpresaActiva );
                                       except
                                             On Error: Exception do
                                             begin
                                                  lContinua := FALSE;
                                                  oListaMensajes.Add(Error.Message);
                                             end;
                                       end;

                                       if lContinua then
                                       begin
                                            cdsPrestamosEmp.Data := SuperReporte.DataSetReporte.Data;
                                            {$ifdef CAROLINA}
                                            cdsPrestamosEmp.SAVETOFILE('D:\TEMP\CDS.CDS');
                                            {$endif}

                                            lEmpActivo := zStrToBool( cdsPrestamosEmp.FieldByName('CB_ACTIVO').AsString ) ;
                                       end;
                                  end;
                               finally
                                      ClearBroker;
                               end;

                               if lContinua then
                               begin
                                    First;
                                    oTAhorro.Active := True;
                                    while NOT EOF do
                                    begin
                                         lCambioTipoCodigo := (oZetaProvider.DatosPrestamo.OldTipo <> oZetaProvider.DatosPrestamo.Tipo);

                                         //Se valida que el prestamo tenga el status que indica la regla.
                                         if ( eStatusReglaPrestamo( FieldByName('RP_PRE_STS').AsInteger) = rpTodos)  or
                                            ( oZetaProvider.DatosPrestamo.Status = spActivo ) then
                                         begin
                                              if lCambioStatus then
                                              begin
                                                   if ( eStatusReglaPrestamo( FieldByName('RP_PRE_STS').AsInteger) <> rpTodos ) then
                                                       lCambioTipoCodigo := lCambioTipoCodigo or (oZetaProvider.DatosPrestamo.Status = spActivo);
                                              end;

                                              //Se valida que la regla aplique a todos los prestamos o bien,
                                              //que el Tipo del prestamo este en la lista de la regla.
                                              if ( FieldByName('RP_LISTA').AsString = 'N' ) or
                                                 ( ( FieldByName('RP_LISTA').AsString = 'S' ) AND
                                                   ( FieldByName('TB_CODIGO').AsString = oZetaProvider.DatosPrestamo.Tipo ) ) then
                                              begin

                                                   {--------------------------------------}
                                                   //Por cada regla se Limpia otra vez los filtros.
                                                   FiltraDataset( VACIO, FALSE );


                                                   {Se aplican las condiciones de la Regla}
                                                   //eStatusPrestamo =(spActivo,spCancelado,spSaldado);
                                                   //eStatusReglaPrestamo = ( rpActivo, rpTodos );
                                                   sMensaje := FieldByName('RP_LETRERO').AsString;
                                                   sPreTopeName := 'PRE_TOPE_' + FieldByName('RP_CODIGO').AsString;
                                                   rTopeRegla := cdsPrestamosEmp.FieldByName(sPreTopeName).AsFloat;
                                                   {****************************************}
                                                   { ** ** ** Validaciones de Prestamos ** ** ** }

                                                   //A cuales prestamo aplica esta regla
                                                   if lHayPrestamos then
                                                   begin
                                                        if eStatusReglaPrestamo( FieldByName('RP_PRE_STS').AsInteger ) = rpActivo then
                                                           FiltraDataset( Format( '(PR_STATUS=%d)', [Ord( spActivo )] ) );

                                                        //Se revisa si la regla aplica a todos los prestamos o nada mas al tipo que se esta editando.
                                                        if zStrToBool( FieldByName('RP_LISTA').AsString ) then
                                                           FiltraDataset( Format( '(PR_TIPO=''%s'')', [oZetaProvider.DatosPrestamo.Tipo] ) );
                                                   end;

                                                   {
                                                    1) La regla no tiene marcado Validacion de Fechas:
                                                       Se aplica la regla, dependiendo del status y la lista de prestamos.

                                                    2) La regla tiene marcado Validacion de Fechas:
                                                       Caso1: 	el prestamo no tiene asociado un tipo de ahorro,
                                                               se comporta como si no estuviera marcada la Validacion de fechas

                                                       Caso2: 	el prestamo esta asociado un Tipo de Ahorro,
                                                               el tipo de ahorro no valida rango de fechas,
                                                               se comporta como si no estuviera marcada la Validacion de fechas

                                                       Caso3: 	el prestamo esta asociado a un Tipo de Ahorro,
                                                               el ahorro SI valida rango de fechas,
                                                               la regla se valida contra el prestamo seleccionado y los prestamos
                                                               activos dentro del rango de fechas.
                                                               El rango de fecha es sobre el a�o activo del servidor.
                                                     }
                                                     if zStrToBool( FieldByName('RP_VAL_FEC').AsString ) and
                                                        zStrToBool( oTAhorro.FieldByName( 'TB_VAL_RAN' ).AsString ) then
                                                     begin
                                                          //Se valida que la fecha del prestamo caiga dentro de las fechas que indica el Ahorro
                                                          //if zStrToBool( oTAhorro.FieldByName( 'TB_VAL_RAN' ).AsString ) then
                                                          begin
                                                               with oZetaProvider.DatosPrestamo do
                                                               begin
                                                                    iYear:=TheYear(Date);
                                                                    dInicial := DateTheYear( oTAhorro.FieldByName('TB_FEC_INI').AsDateTime, iYear );
                                                                    dFinal := DateTheYear( oTAhorro.FieldByName('TB_FEC_FIN').AsDateTime, iYear );
                                                                    if dInicial > dFinal then
                                                                       dInicial := DateTheYear( oTAhorro.FieldByName('TB_FEC_INI').AsDateTime, iYear-1 );

                                                                    if NOT(( Fecha >= dInicial ) and ( Fecha <= dFinal )) then
                                                                       AgregaMensaje( Format( 'Fecha de Inicio debe de estar entre %s y %s', [FechaCorta(dInicial),FechaCorta(dFinal)] ));
                                                               end;
                                                               //En caso de aplique, los prestamos se filtran a que se muestran solo los prestamos dentro del rango de fechas
                                                               if lHayPrestamos then
                                                                  FiltraDataset( Format( '(PR_FECHA>=''%s'') and (PR_FECHA <= ''%s'')', [FechaAsStr(dInicial),FechaAsStr(dFinal)] ) );
                                                          end;
                                                     end;

                                                     if lHayPrestamos then
                                                     begin
                                                          //Cantidad m�xima de prestamos
                                                          with FieldByName('RP_LIMITE') do
                                                          begin
                                                               if ( AsInteger > 0 ) then
                                                               begin
                                                                    if ( UpdateKind = ukInsert ) or lCambioTipoCodigo then
                                                                    begin
                                                                         if (cdsPrestamosEmp.RecordCount >= AsInteger ) then
                                                                            //oLista.Add( sMensaje + '::Excede el l�mite de pr�stamos');
                                                                            AgregaMensaje( Format( 'Excede el l�mite de %d pr�stamo(s)', [AsInteger] ) );
                                                                    end
                                                                    else
                                                                    begin
                                                                         if (cdsPrestamosEmp.RecordCount > AsInteger ) then
                                                                            //oLista.Add( sMensaje + '::Excede el l�mite de pr�stamos');
                                                                            AgregaMensaje( Format( 'Excede el l�mite de %d pr�stamo(s)', [AsInteger] ) );
                                                                    end;
                                                               end;
                                                          end;
                                                     end;


                                                     //Validar el Tope del monto
                                                     if rTopeRegla <> 0 then
                                                     begin
                                                          rTope := 0;
                                                          if lHayPrestamos then
                                                          begin
                                                               while NOT cdsPrestamosEmp.EOF do
                                                               begin
                                                                    if NOT ( ( cdsPrestamosEmp.FieldByName('PR_REFEREN').AsString = oZetaProvider.DatosPrestamo.Referencia ) and
                                                                             ( cdsPrestamosEmp.FieldByName('PR_TIPO').AsString = oZetaProvider.DatosPrestamo.OldTipo ) ) then
                                                                    begin
                                                                         rTope := rTope + cdsPrestamosEmp.FieldByName('PR_MONTO').AsFloat;
                                                                    end;
                                                                    cdsPrestamosEmp.Next;
                                                               end;
                                                               rTope := rTopeRegla - rTope;
                                                          end
                                                          else
                                                              rTope := rTopeRegla;

                                                             if ( oZetaProvider.DatosPrestamo.Monto  > rTope ) then
                                                                //oLista.Add( sMensaje + ' :: El Monto Prestado excede el L�mite Autorizado de $' + FloatToStr(rTopeRegla) );
                                                                AgregaMensaje( 'Monto m�ximo a prestar $' + FloatToStr(rMax( rTope, 0 )) );
                                                     end;

                                                     { ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** }

                                                     { ** ** ** Validaciones de Empleados ** ** ** }
                                                     //Status del Empleado, se valida que el empleado tenga el status solicitado.
                                                     //eFiltroStatusEmpleado=( fseActivos, fseInactivos, fseTodos);
                                                     with FieldByName('RP_EMP_STS') do
                                                     begin

                                                          if ( eFiltroStatusEmpleado( AsInteger ) = fseActivos ) AND NOT lEmpActivo then
                                                             //oLista.Add( sMensaje + '::El Empleado debe de estar activo');
                                                             AgregaMensaje( 'Empleado debe de estar activo');

                                                          if (eFiltroStatusEmpleado( AsInteger ) = fseInactivos ) AND lEmpActivo then
                                                              AgregaMensaje( 'Empleado debe de estar dado de baja');
                                                     end;
                                                     //Quitar el filtro
                                                     FiltraDataset( VACIO, FALSE );

                                                     //Se valida la antiguedad del empleado
                                                     if ( ( FieldByName('RP_ANT_INI').AsInteger + FieldByName('RP_ANT_FIN').AsInteger ) >0 ) then
                                                     begin

                                                          if cdsPrestamosEmp.FieldByName('PRE_ANTIG_'  + FieldByName('RP_CODIGO').AsString).AsString = K_BOOLEANO_NO then
                                                             AgregaMensaje( 'Empleado no tiene la antig�edad suficiente');
                                                     end;

                                                      //Validar la condicion
                                                     if StrLleno( FieldByName('QU_CODIGO').AsString ) then
                                                     begin
                                                          if cdsPrestamosEmp.FieldByName('PRE_CONDICION_'  + FieldByName('RP_CODIGO').AsString ).AsString = K_BOOLEANO_NO then
                                                             AgregaMensaje(  Format( 'Empleado no cumple con la Condici�n(%s)', [FieldByName('QU_CODIGO').AsString] ) );
                                                     end;

                                                     //Validar el filtro
                                                     if StrLleno( FieldByName('RP_FILTRO').AsString ) then
                                                     begin
                                                          if cdsPrestamosEmp.FieldByName('PRE_FILTRO_' + FieldByName('RP_CODIGO').AsString).AsString = K_BOOLEANO_NO then
                                                             AgregaMensaje( 'Empleado no cumple el Filtro' ) ;
                                                     end;

                                                     { ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** }


                                                     {--------------------------------------}
                                              end;
                                         end;
                                         if ( oLista.Count >0 ) then
                                         begin
                                              oListaMensajes.Add(CR_LF);
                                              oListaMensajes.Add('Regla : ' + sMensaje + '');
                                              oListaMensajes.AddStrings(oLista);
                                              oLista.Clear;
                                         end;
                                         Next;
                                    end;
                               end;
                          end;
                       finally
                              oReglas.Close;
                       end;
                  end;

                  if ( oListaMensajes.Count > 0  ) then
                  begin
                       FMensajePrestamo := oListaMensajes.Text;
                       if (lLevantaExcepcion )then
                       begin
                            raise Exception.Create( FMensajePrestamo );
                       end;
                  end;


               finally
                      FreeAndNil(cdsPrestamosEmp);
                      FreeAndNil(oPrestamo);
                      FreeAndNil(oReglas);
                      FreeAndNil(oLista);
                      FreeAndNil(oTAhorro);
               end;
          end;
     end;
end;

function TPrestamosServer.GetScript(const eTipoScript: eScript): String;
begin
     Result := '';
     case eTipoScript of
          eReglasPrestamo : Result := 'SELECT REGLAPREST.*, '+
                                      'PRESTAXREG.TB_CODIGO, '+
                                      'QUERYS.QU_FILTRO '+
                                      'FROM  REGLAPREST '+
                                      'LEFT OUTER JOIN PRESTAXREG ON PRESTAXREG.RP_CODIGO = REGLAPREST.RP_CODIGO '+
                                      'LEFT OUTER JOIN QUERYS ON QUERYS.QU_CODIGO = REGLAPREST.QU_CODIGO '+
                                      'WHERE RP_ACTIVO = ''S'' ' +
                                      'AND ( (RP_LISTA = ''N'') OR (RP_LISTA = ''S'' AND TB_CODIGO = ''%s'') ) ' +
                                      'ORDER BY RP_ORDEN ';
          eFechasTAhorro : Result := 'SELECT TB_VAL_RAN , TB_FEC_INI , TB_FEC_FIN FROM TAHORRO WHERE TB_PRESTA = ''%s'' ';
          eCountPrestamo : Result := 'SELECT COUNT(*) CUANTOS FROM PRESTAMO WHERE CB_CODIGO = %d';
     end;
end;

function TPrestamosServer.GetExpresionAntig( const iDiasAntig: Integer; const sFormula, sReferencia: String ): string;
begin
     if ( iDiasAntig > 0 ) then
     begin
          Result := Format( sFormula, [ sReferencia, iDiasAntig ] )
     end;
end;

procedure TPrestamosServer.DataModuleCreate(Sender: TObject);
begin
      oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
      {$ifndef DOS_CAPAS}
      oZetaProvider.SetreadUncommited;
      {$endif}
end;

procedure TPrestamosServer.DataModuleDestroy(Sender: TObject);
begin
     ZetaSQLBroker.FreeZetaCreator( oZetaCreator );
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
end;

end.

//filtrar el dataset de prestamos para que se validen las fechas contra la cantidad
