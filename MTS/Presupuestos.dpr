library Presupuestos;

uses
  ComServ,
  Presupuestos_TLB in 'Presupuestos_TLB.pas',
  DServerPresupuestos in 'DServerPresupuestos.pas' {dmServerPresupuestos: TMtsDataModule} {dmServerPresupuestos: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
