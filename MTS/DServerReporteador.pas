unit DServerReporteador;

{$ifdef TRESS}
   {$DEFINE MULTIPLES_ENTIDADES}
{$ENDIF}

{$INCLUDE DEFINES.INC}

interface

Uses
  Windows, Messages, SysUtils, Classes, Controls, DB, ADODB,
  Variants,
  DZetaServerProvider,
  ZCreator,
  ZSuperSQL,
  ZAgenteSQL,
  ZetaSQLBroker,
  DSuperReporte
  {$IFDEF VISITANTES}
    , ZAgenteSQLClient
  {$ENDIF}
    ;

type
  TdmServerReporteador = Class(TObject)
    private
      { Private declarations }
      oSuperSQL: TSuperSQL;
      {$IFDEF VISITANTES}
      FImagen: TZetaCursor;
      {$ENDIF}
    protected
      oZetaCreator: TZetaCreator;
      oZetaProvider: TdmZetaServerProvider;
      oSuperReporte: TdmSuperReporte;
      FSQLAgente: TSQLAgente;
      procedure ErrorGrave(const Error, Descripcion: string; Agente: TSQLAgente);
      function GeneraSQLEspecial(oSQLAgente: TSQLAgente; var Error: string): TSQLAgente; virtual;
      procedure DespuesDeConstruyeSuperSQL(FAgenteReporte, oSQLAgente: TSQLAgente); virtual;
      procedure RegistraFunciones; virtual;
      procedure AsignaParametros; virtual;
      procedure AlInicializarParametros; virtual;
      {$IFDEF VISITANTES}
      procedure EvaluaColumnas(DataSet: TZetaCursor; oColumna: TSQLColumna; oCampo: TField);
      {$ENDIF}
    public
      constructor Create(oProvider: TdmZetaServerProvider; oCreator: TZetaCreator);
      destructor Destroy; override;
      function GeneraSQL(Empresa: OleVariant; var AgenteSQL: OleVariant; var Parametros: OleVariant;
        out Error: WideString): OleVariant;
      function EvaluaParam(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant;
        out Error: WideString): WordBool;
      function PruebaFormula(Empresa, Parametros: OleVariant; var Formula: WideString;
        Entidad: Integer; ParamsRep: OleVariant): WordBool;
  end;

implementation

uses
  ZetaCommonLists,
  ZetaCommonClasses,
  ZetaCommonTools,
  ZetaTipoEntidad,
  ZetaTipoEntidadTools,
  ZEvaluador,
  ZBaseCreator;

constructor TdmServerReporteador.Create(oProvider: TdmZetaServerProvider; oCreator: TZetaCreator);
begin
  oZetaProvider := oProvider;
  oZetaCreator := oCreator;
  oZetaCreator.PreparaEntidades;

  oSuperSQL := TSuperSQL.Create(oZetaCreator);
  oSuperReporte := TdmSuperReporte.Create(oZetaCreator);
  oZetaCreator.SuperReporte := oSuperReporte;
  {$IFDEF VISITANTES}
  FImagen := oZetaProvider.CreateQuery;
  {$ENDIF}
end;

destructor TdmServerReporteador.Destroy;
begin
  inherited;
  {$IFDEF VISITANTES}
  FreeAndNil(FImagen);
  {$ENDIF}
  FreeAndNil(oSuperReporte);
  FreeAndNil(oSuperSQL);
end;

function TdmServerReporteador.EvaluaParam(Empresa: OleVariant; var AgenteSQL: OleVariant;
  Parametros: OleVariant; out Error: WideString): WordBool;
var
  oSQLAgente: TSQLAgente;
begin
  oSQLAgente := Nil;
  Result := False;

  try
    oSQLAgente := TSQLAgente.Create;
    with oZetaProvider do begin
      EmpresaActiva := Empresa;
      AsignaParamList(Parametros);
      AlInicializarParametros;
    end;

    try
      oSQLAgente.VariantToAgente(AgenteSQL);
      with oZetaCreator do begin
        RegistraFunciones([efComunes, efReporte]);
      end;
      Result := oSuperSQL.EvaluaParams(oSQLAgente);
      if Result then
        AgenteSQL := oSQLAgente.AgenteToVariant
      else
        Error := oSQLAgente.ListaErrores.Text;
    except
      on E: Exception do begin
        Error := 'Exception al Crear Param Agente: ' + E.Message;
      end;
    end;

  finally
    oSQLAgente.Free;
  end;
end;

{ .$define XMLREPORT_DEBUG }
function TdmServerReporteador.GeneraSQL(Empresa: OleVariant; var AgenteSQL: OleVariant; var Parametros: OleVariant;
         out Error: WideString): OleVariant;
var
   FAgenteReporte: TSQLAgente;
   sError: string;
   {$IFDEF XMLREPORT_DEBUG}
   i: Integer;
   FDatos: TStrings;
   {$ENDIF}
begin
     Result := NULL;
     try
        FSQLAgente := TSQLAgente.Create;

        with oZetaProvider do
        begin
             FSQLAgente.VariantToAgente(AgenteSQL);
             AsignaParametros;
             { CV:2/abr/2003. Se tiene que mandar llamar,
               porque en DosCapas,
               se estan quedando con los valores de la ultima corrida,
               sobre todo, cuando la función CAL(), se utiliza en
               el calculo de la Nomina }
             ZetaCommonTools.InitDatosCalendario(DatosCalendario);
        end;
        {$IFDEF XMLREPORT_DEBUG}
        FDatos := TStringList.Create;
        try
           with FDatos do
           begin
                Add('********* AGENTE **********');
                Add('');
                FSQLAgente.AgenteToTextFile(FDatos);
                Add('');
                Add('****** FIN DE AGENTE ******');
                Add('');
                Add('******* PARAMETROS ********');
                Add('');
                with oZetaProvider do
                begin
                     with ParamList do
                     begin
                          for i := 0 to (Count - 1) do
                          begin
                               with Items[i] do
                               begin
                                    case DataType of
                                      ftString:
                                        FDatos.Add(Format('%s = %s', [Name, AsString]));
                                      ftInteger, ftSmallInt:
                                        FDatos.Add(Format('%s = %d', [Name, AsInteger]));
                                      ftDate, ftDateTime:
                                        FDatos.Add(Format('%s = %s', [Name, FormatDateTime('dd/mmm/yyyy hh:nn AM/PM', AsDateTime)]));
                                      ftFloat:
                                        FDatos.Add(Format('%s = %n', [Name, AsFloat]));
                                      ftBoolean:
                                        FDatos.Add(Format('%s = %s', [Name, BoolToStr(AsBoolean)]));
                                    end;
                               end;
                          end;
                     end;
                end;
                Add('');
                Add('*** FIN DE PARAMETROS *****');
                Add('');
                Add('********* EMPRESA *********');
                Add('');
                Add(Format('Database = %s', [Empresa[P_DATABASE]]));
                Add(Format('UserName = %s', [Empresa[P_USER_NAME]]));
                Add(Format('Password = %s', [Empresa[P_PASSWORD]]));
                Add(Format('#Usuario = %s', [Empresa[P_USUARIO]]));
                Add(Format(' Nivel 0 = %s', [Empresa[P_NIVEL_0]]));
                Add(Format(' Emp Cod = %s', [Empresa[P_CODIGO]]));
                Add('');
                Add('***** FIN DE EMPRESA ******');
                SaveToFile('D:\Temp\AgenteSQL.txt');
           end;
        finally
               FreeAndNil(FDatos);
        end;
        {$ENDIF}
        if oZetaProvider.OpenProcess(prReporteador, 2) then
        begin
             if EntidadValida(FSQLAgente.Entidad) then
             begin
                  try
                     oSuperReporte.Agente := FSQLAgente;
                     FSQLAgente.RastreaColumnaCalculada := TRUE;
                     with oZetaProvider do
                          CanContinue(ParamList.ParamByName('Codigo').AsInteger);
                     oSuperReporte.Parametros := FSQLAgente.Parametros;

                     RegistraFunciones;
                     sError := VACIO;
                     {$IFNDEF TRESSCFG}
                     {$IFDEF MULTIPLES_ENTIDADES}
                     if Dentro(FSQLAgente.Entidad, ReporteEspecial) then
                     {$ELSE}
                     if FSQLAgente.Entidad in ReporteEspecial then
                     {$ENDIF}
                     begin
                          oZetaProvider.OptimizeCursorB8 := FALSE;
                          TZADOQuery( oSuperReporte.QueryReporte ).CursorLocation := clUseClient;
                          TZADOQuery( oSuperReporte.QueryReporte ).CursorType := ctStatic;
                          FAgenteReporte := GeneraSQLEspecial(FSQLAgente, sError);
                          Error := 'Reporte Especial :' + sError
                     end
                     else
                         FAgenteReporte := FSQLAgente;
                     if FAgenteReporte = NIL then
                        raise Exception.Create(Error);
                     {$ENDIF}
                     FAgenteReporte.Optimizar := TRUE;
                     if oSuperSQL.Construye(FAgenteReporte) then
                     begin
                          DespuesDeConstruyeSuperSQL(FAgenteReporte, FSQLAgente);
                          {$IFDEF CAROLINA}
                          FAgenteReporte.SQL.SaveToFile('d:\temp\REPORTES.SQL');
                          {$ENDIF}
                          with oZetaProvider do
                          begin
                               if ( not OptimizeCursorB8 ) then     // Hay funciones con Calculate
                               begin
                                    TZADOQuery( oSuperReporte.QueryReporte ).CursorLocation := clUseClient;
                                    TZADOQuery( oSuperReporte.QueryReporte ).CursorType := ctStatic;
                               end;
                               if ParamList.ParamByName('GeneraBitacora').AsBoolean then
                                  Log.Evento(clbNinguno, 0, Date, 'Reporte Calculado', 'SQL Generado: ' + CR_LF + FAgenteReporte.SQL.Text);
                          end;
                          try
                             if oSuperReporte.Construye(FAgenteReporte, oZetaProvider.ParamList.ParamByName('SoloTotales').AsBoolean,
                                oZetaProvider.ParamList.ParamByName('TotalesGrupos').AsBoolean) then
                             begin
                                  if FAgenteReporte.NumErrores = 0 then
                                  begin
                                       Result := oSuperReporte.GetReporte;
                                  end
                                  else
                                  begin
                                       Error := FAgenteReporte.ListaErrores.Text;
                                       ErrorGrave('Error en Construcción de Super Reporte.', 'SQL Generado' + CR_LF + FAgenteReporte.SQL.Text, FAgenteReporte);
                                  end;
                             end
                             else
                             begin
                                  Error := FAgenteReporte.ListaErrores.Text;
                                  if NOT(oSuperReporte.QueryReporte.Bof AND oSuperReporte.QueryReporte.Eof) then
                                     ErrorGrave('Error en Construcción de Super Reporte', 'SQL Generado' + CR_LF + FAgenteReporte.SQL.Text, FAgenteReporte);
                             end;
                          except
                                on E: Exception do
                                   Error := 'Error al Abrir Reporte: ' + CR_LF + E.Message;
                          end;
                          AgenteSQL := FAgenteReporte.AgenteToVariant;
                          {$IFDEF CAROLINA}
                          oSuperReporte.DataSetReporte.SaveToFile('d:\temp\reportes.cds');
                          {$ENDIF}
                     end
                     else
                     begin
                          Error := FAgenteReporte.ListaErrores.Text;
                          ErrorGrave('Error en Construcción de Super SQL', 'SQL Generado' + CR_LF + FAgenteReporte.SQL.Text, FAgenteReporte);
                     end;
                  except
                        on E: Exception do
                           Error := 'Error al Crear SQL Agente: ' + CR_LF + E.Message;
                  end;
             end
             else
             begin
                  Error := 'El Reporte Contiene Una Tabla Para Uso Futuro';
                  ErrorGrave(Error, VACIO, FSQLAgente);
             end;
        end;
        with oZetaProvider do
        begin
             CanContinue(ParamList.ParamByName('Codigo').AsInteger);
             CloseProcess;
        end;
     finally
            Parametros := oZetaProvider.ParamList.VarValues;
            oSuperReporte.Parametros := NULL;
            FreeAndNil(FAgenteReporte);
     end;
end;

function TdmServerReporteador.PruebaFormula(Empresa, Parametros: OleVariant;
  var Formula: WideString; Entidad: Integer; ParamsRep: OleVariant): WordBool;
var
  oEvaluador: TZetaEvaluador;
  oSuperReporte: TdmSuperReporte;
  TipoFormula: eTipoGlobal;
  sTransformada: String;
  lConstante: Boolean;
  eFunciones: TipoFunciones;
  eEvaluador: eTipoEvaluador;
begin
  with oZetaProvider do begin
    EmpresaActiva := Empresa;
    AsignaParamList(Parametros);
    InitGlobales;
    eEvaluador := eTipoEvaluador(ParamList.ParamByName('TipoEvaluador').AsInteger);
  end;
  oSuperReporte := NIL;

  if not(VarIsNull(ParamsRep) OR VarIsEmpty(ParamsRep)) then begin
    oSuperReporte := TdmSuperReporte.Create(oZetaCreator);
    oSuperReporte.Parametros := ParamsRep;
  end;

  with oZetaCreator do begin
    eFunciones := [efComunes, efAnuales, efNomina, efReporte, efPoliza];
    if (eEvaluador = evAguinaldo) then
      eFunciones := eFunciones + [efAguinaldo];
    PreparaEntidades;
    RegistraFunciones(eFunciones);

    SuperReporte := oSuperReporte;
  end;
  oEvaluador := TZetaEvaluador.Create(oZetaCreator);

  { CV: 28-Nov-2001.
    Lo que se esta logrando aqui, es que el Objecto CalcNomina tenga un valor DIFERENTE de
    NIL. Esto es para que cuando se evalue una formula que nada mas pertenezca al calculo de nomina,
    esta funcion tenga acceso a este objeto. }
  if (eEvaluador = evNomina) then
    oEvaluador.CalcNomina := self;

  { Para que funcione con PARAM():
    -Se tiene que crear un oSuperReporte, y este asignarselo a oZetaCreator
    -Al oSuperReporte, se le asignan los parametros
    -Destruir al final el oSuperReporte }
  try
    oEvaluador.Entidad := TipoEntidad(Entidad);
    sTransformada := Formula;
    Result := oEvaluador.GetRequiere(Formula, TipoFormula, sTransformada, lConstante);
    if not Result then
      Formula := sTransformada; // Regresa el mensaje de Error en 'Formula'
  finally
    oEvaluador.Free;
    FreeAndNil(oSuperReporte);
  end;
end;

procedure TdmServerReporteador.DespuesDeConstruyeSuperSQL(FAgenteReporte, oSQLAgente: TSQLAgente);
begin
end;

function TdmServerReporteador.GeneraSQLEspecial(oSQLAgente: TSQLAgente; var Error: string)
  : TSQLAgente;
begin
  Result := NIL;
end;

procedure TdmServerReporteador.AsignaParametros;
begin
  {$IFDEF VISITANTES}
  oSuperReporte.OnEvaluaColumna := EvaluaColumnas;
  {$ENDIF}
end;

procedure TdmServerReporteador.AlInicializarParametros;
begin
end;

procedure TdmServerReporteador.RegistraFunciones;
var
  eFunciones: TipoFunciones;
begin
  eFunciones := [efComunes, efReporte];
  {$IFDEF TRESS}
  if Dentro(FSQLAgente.Entidad, [enMovimienBalanzaMensual, enMovimienBalanza]) then
    eFunciones := eFunciones + [efReporteBalanza];
  {$ENDIF}
  with oZetaCreator do begin
    RegistraFunciones(eFunciones);
  end;
end;

procedure TdmServerReporteador.ErrorGrave(const Error, Descripcion: string; Agente: TSQLAgente);
begin
  with oZetaProvider do
    Log.Error(0, Error, Agente.ListaErrores.Text + CR_LF + Descripcion);
end;

{$IFDEF VISITANTES}
procedure TdmServerReporteador.EvaluaColumnas(DataSet: TZetaCursor; oColumna: TSQLColumna;
  oCampo: TField);
const
  K_SQL = 'SELECT VI_FOTO FROM VISITA WHERE VI_NUMERO = %d';
begin
  oZetaProvider.AbreQueryScript(FImagen,
    Format(K_SQL, [DataSet.FieldByName(K_VISITA_VI_NUMERO).AsInteger]));
  FImagen.Active := TRUE;
  oCampo.AsVariant := FImagen.Fields[0].AsVariant;
end;
{$ENDIF}

end.
