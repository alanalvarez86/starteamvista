unit DPrestaciones;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
     ZetaCommonClasses,
     ZetaCommonLists,
     DZetaServerProvider;

const
    K_MAX_YEARS = 99;

{$define IMSS_UMA}
{.$define FLEXIBLES}
{.$undefine FLEXIBLES}

type
  TTablaDiasVaca = record
    DiasVaca: TDiasHoras;
    DiasPrima: TDiasHoras;
  end;

  TdmPrestaciones = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    FQryPrestaciones: TZetaCursor;
    FQryRenglon: TZetaCursor;
    aDiasVaca: array [ 0..K_MAX_YEARS ] of TTablaDiasVaca;
    FCacheTabla: String;
    function oZetaProvider: TdmZetaServerProvider;
    function GetDiasVaca( const sTabla: String; iYear: Integer{$ifdef FLEXIBLES}; iEmpleado: integer{$endif} ): TTablaDiasVaca;
    procedure CachePrestaciones( const sTabla: String{$ifdef FLEXIBLES}; iEmpleado: integer{$endif} );
    procedure PreparaQuery;
    procedure PreparaRenglon;
  public
    { Public declarations }
    function GetRenglonPresta( const sTabla: String; const iYear: Integer{$ifdef FLEXIBLES}; iEmpleado: Integer{$endif} ): TZetaCursor;
    function VacaProporcional( const dAntig, dUltimoCierre, dProximoCierre: TDate; const sTabla: String; var rDiasTabla, rDiasBase, rDiasPrima: Currency{$ifdef FLEXIBLES}; const iEmpleado: Integer{$endif} ): Currency;
  end;
  TOtrasPercepciones = class( TObject )
  private
    { Private declarations }
    FMonto    : TPesos;
    FGravado  : TPesos;
    FISPT     : TPesos;
    FCritIMSS : eIMSSOtrasPer;
    FCritISPT : eISPTOtrasPer;
  public
    { Public declarations }
    property Monto    : TPesos read FMonto;
    property Gravado  : TPesos read FGravado;
    property ISPT     : TPesos read FISPT;
    property CritIMSS : eIMSSOtrasPer read FCritIMSS;
  end;
  TSalarioIntegrado = class(TObject)
  private
    { Private declarations }
    FCreator         : TDatamodule;
    FSalario         : TPesos;
    FSalarioBase     : TPesos;
    FFactor          : TTasa;
    FPreIntegrado    : TPesos;
    FGravadoVariable : TPesos;
    FGravadoFijo     : TPesos;
    FIntegrado	     : TPesos;
    FOtrasFijas      : TPesos;
    FISPT            : TPesos;
    FRangoSal        : TPesos;
    FListaPerFijas   : TStringList;
    FReferencia      : TDate;
    FAntiguedad      : TDate;
    FTablaPresta     : String;
    FQryFactor       : TZetaCursor;
    FQryOtrasPer     : TZetaCursor;
    function oZetaProvider: TdmZetaServerProvider;
    function GetFactorIntegracion{$ifdef FLEXIBLES}( const iEmpleado: integer ){$endif}: TTasa;
    function TopeIntegrado : TPesos;
    function CreaPerFija( const sCodigo: String ): TOtrasPercepciones;
    {$ifdef IMSS_UMA}
    function ValUMA: TPesos;
    {$else}
    function SMGDF: TPesos;
    {$endif}
    procedure LimpiaListaFijas;
    procedure CalculaFijas(const Value: String);
    function CalculaRangoSal(const sFijas: String): TPesos;
    function SumaFijas(const Value: String): TPesos;
  public
    { Public declarations }
    constructor Create( oCreator: TDataModule );
    destructor  Destroy; override;
    property Salario         : TPesos read FSalario;
    property Factor          : TTasa  read FFactor;
    property PreIntegrado    : TPesos read FPreIntegrado;
    property GravadoVariable : TPesos read FGravadoVariable;
    property GravadoFijo     : TPesos read FGravadoFijo;
    property GravadoISPT     : TPesos read FISPT;
    property Integrado       : TPesos read FIntegrado;
    property OtrasFijas      : TPesos read FOtrasFijas;
    property RangoSal        : TPesos read FRangoSal;
    property ListaPerFijas   : TStringList  read FListaPerFijas;
    function CalculaIntegrado( const nSalario, nPerVar : TPesos;
                               const dReferencia, dAntiguedad : TDate;
                               const sTablaPresta, sListaFijas : String;
                               const nRangoSal : TPesos
{$ifdef FLEXIBLES}
                               ; const iEmpleado : integer
{$endif}
                               ) : TPesos;
    function CalculaOtrasPer(const nSalario, nPerVar: TPesos;
                             const nFactor: TTasa; const dReferencia: TDate;
                             const sListaFijas: String): TPesos;
  end;


function YearsCompletos( const dInicial, dFinal: TDate; var lAniversario: Boolean; lDiasCalendario: boolean = False ): Currency;

implementation

uses ZetaCommonTools,
     ZGlobalTress,
     ZCreator;

{$R *.DFM}

function YearsCompletos( const dInicial, dFinal: TDate; var lAniversario: Boolean; lDiasCalendario: boolean = False ): Currency;
var
   iDayIni, iMonthIni, iYearIni: Word;
   iDayFin, iMonthFin, iYearFin: Word;
begin
     lAniversario := False;
     if ( dFinal > dInicial ) then
     begin
          DecodeDate( dInicial, iYearIni, iMonthIni, iDayIni );
          DecodeDate( dFinal, iYearFin, iMonthFin, iDayFin );
          // Si coincide con en el aniversario //
          lAniversario :=( iMonthIni = iMonthFin ) and ( iDayIni = iDayFin );
          if lAniversario then   // Resta de a�os enteros //
             Result := iYearFin - iYearIni
          else  // Hay una parte proporcional //
              Result := ( ( dFinal - dInicial ) + 1 ) / 365.25;
          {
          begin
               if lDiasCalendario then
                  Result := ( ( dFinal - dInicial ) + 1 ) / K_DIAS_ANNO_AVENT
               else
                   Result := ( ( dFinal - dInicial ) + 1 ) / 365.25;
          end;
          }
     end
     else
         Result := 0;
end;

{ ************ TdmPrestaciones ************ }

procedure TdmPrestaciones.DataModuleCreate(Sender: TObject);
begin
     FCacheTabla := '';
end;

function TdmPrestaciones.oZetaProvider: TdmZetaServerProvider;
begin
     Result := TdmZetaServerProvider( Owner );
end;

procedure TdmPrestaciones.PreparaQuery;
const
{$ifdef FLEXIBLES}
   {$ifdef INTERBASE}
      K_QRY_PRESTACIONES = 'select PT_YEAR, PT_DIAS_VA, PT_PRIMAVA ' +
                           'from SP_GET_CACHE( :Tabla, :Empleado )';
   {$endif}
   {$ifdef MSSQL}
      K_QRY_PRESTACIONES = 'select PT_YEAR, PT_DIAS_VA, PT_PRIMAVA ' +
                           'from   DBO.SP_GET_CACHE( :Tabla, :Empleado )';
   {$endif}
{$else}
      K_QRY_PRESTACIONES = 'select PT_YEAR, PT_DIAS_VA, PT_PRIMAVA ' +
                           'from   PRESTACI ' +
                           'where  TB_CODIGO = :Tabla ' +
                           'order by PT_YEAR';
{$endif}
begin
    if ( FQryPrestaciones = NIL ) then
        FQryPrestaciones := oZetaProvider.CreateQuery( K_QRY_PRESTACIONES );
end;

procedure TdmPrestaciones.PreparaRenglon;
const
{$ifdef FLEXIBLES}
   {$ifdef INTERBASE}
      K_QRY_RENGLON = 'select PT_FACTOR, PT_DIAS_VA, PT_DIAS_AG, PT_PRIMAVA, PT_PRIMA_7, PT_PAGO_7 '+
                      'from   SP_GET_RENGLON( :Tabla, :Year, :Empleado )';
   {$endif}
   {$ifdef MSSQL}
      K_QRY_RENGLON = 'select PT_FACTOR, PT_DIAS_VA, PT_DIAS_AG, PT_PRIMAVA, PT_PRIMA_7, PT_PAGO_7 '+
                      'from   DBO.SP_GET_RENGLON( :Tabla, :Year, :Empleado )';
   {$endif}
{$else}
      K_QRY_RENGLON = 'select PT_FACTOR, PT_DIAS_VA, PT_DIAS_AG, PT_PRIMAVA, PT_PRIMA_7, PT_PAGO_7 '+
                      'from   PRESTACI '+
                      'where  TB_CODIGO = :Tabla AND PT_YEAR >= :Year '+
                      'order by PT_YEAR';
{$endif}
begin
    if ( FQryRenglon = NIL ) then
        FQryRenglon := oZetaProvider.CreateQuery( K_QRY_RENGLON );
end;

procedure TdmPrestaciones.CachePrestaciones(const sTabla: String{$ifdef FLEXIBLES}; iEmpleado: integer{$endif} );
var
    iAnterior, iActual: Integer;
    nDiasVaca, nDiasPrima: Currency;

    procedure LlenaArreglo( const Inicial, Final : Integer; const rDiasVaca, rDiasPrima : Currency );
    var
       i : Integer;
    begin
         for i := Inicial to Final do
         begin
              aDiasVaca[ i ].DiasVaca := rDiasVaca;
              aDiasVaca[ i ].DiasPrima := rDiasPrima;
         end;
    end;

begin
    PreparaQuery;
    iAnterior := 0;
    nDiasVaca := 0;
    nDiasPrima := 0;
    with oZetaProvider do
    begin
         ParamAsChar( FQryPrestaciones, 'Tabla', sTabla, K_ANCHO_CODIGO1 );
{$ifdef FLEXIBLES}
         ParamAsInteger( FQryPrestaciones, 'Empleado', iEmpleado );
{$endif}
         with FQryPrestaciones do
         begin
              Open;
              while ( not Eof ) do
              begin
                   iActual   := FieldByName( 'PT_YEAR' ).AsInteger;
                   nDiasVaca := FieldByName( 'PT_DIAS_VA' ).AsFloat;
                   nDiasPrima := nDiasVaca * ( FieldByName( 'PT_PRIMAVA' ).AsFloat / 100 );

                   LlenaArreglo( iAnterior+1, iActual, nDiasVaca, nDiasPrima );
                   iAnterior := iActual;

                   Next;
              end;
              Close;
         end;
    end;
    { Llena el resto del arreglo }
    LlenaArreglo( iAnterior+1, K_MAX_YEARS, nDiasVaca, nDiasPrima );
    FCacheTabla := sTabla;
end;

function TdmPrestaciones.GetDiasVaca( const sTabla: String; iYear: Integer{$ifdef FLEXIBLES}; iEmpleado: integer{$endif} ): TTablaDiasVaca;
begin
     if ( sTabla <> FCacheTabla ) then
         CachePrestaciones( sTabla{$ifdef FLEXIBLES}, iEmpleado{$endif}  );

     if ( iYear < 1 ) then
        iYear := 1;
     if ( iYear > K_MAX_YEARS ) then
        iYear := K_MAX_YEARS;

     Result := aDiasVaca[ iYear ];
end;

function TdmPrestaciones.VacaProporcional(const dAntig, dUltimoCierre, dProximoCierre: TDate;
         const sTabla: String; var rDiasTabla, rDiasBase, rDiasPrima: Currency
         {$ifdef FLEXIBLES}; const iEmpleado: Integer {$endif} ): Currency;
var
   rAntigUltimo, rAntigProximo, rAuxPrima: Currency;
   lAnivUltimo, lAnivProximo: Boolean;
   iAntig: Integer;
begin
     Result     := 0;
     rDiasTabla := 0;
     rDiasBase  := 0;
     rAuxPrima  := 0;
     rDiasPrima := 0;

     { Tiene que ser una fecha posterior al Cierre }
     if ( dProximoCierre > dUltimoCierre ) then
     begin
          if ( dUltimoCierre > 0 ) then  // Si hay cierre
             rAntigUltimo := YearsCompletos( dAntig, dUltimoCierre, lAnivUltimo, oZetaProvider.GetGlobalBooleano( K_GLOBAL_AVENT ) )
          else
          begin
               rAntigUltimo := 0;
               lAnivUltimo  := TRUE;
          end;
          rAntigProximo := YearsCompletos( dAntig, dProximoCierre, lAnivProximo, oZetaProvider.GetGlobalBooleano( K_GLOBAL_AVENT ) );
          { Si hay una parte proporcional al principio, se resta }
          if not lAnivUltimo then
          begin
               // 22/Nov/99: Estaba sumando de m�s con 2 cierres seguidos.
               // Result := dmCatalogos.GetDiasVaca( sTabla, Trunc( rAntigUltimo ) ) * ( 1 - Frac( rAntigUltimo ) );
               // rAntigUltimo := Trunc( rAntigUltimo ) + 1;

               // Lo correcto es que RESTE la fracci�n proporcional a lo que ya fu� Cerrado
               // La suma del la fracci�n complementaria se sumar� en el 'for' de a�os completos
               // o bien, en la parte proporcional final.
               // Ej. Si rAntigUltimo es 4.3, entonces se tiene restar el 0.3 utilizando
               // la tabla del a�o 5. Suponiendo que a�o 5 es 14 d�as. ( -0.3 x 14 )
               // Caso 1:  rAntigProximo es < 5, ej. 4.8
               // entonces se suma el 0.8 del a�o 5 :  (.8x14 )+(-.3x14 )= 7
               // Caso 2: rAntigProximo es > 5, ej. 6.5
               // entonces se suma el a�o 5 y 6 completos + el 0.5 del a�o 7
               // En ese caso el a�o 5 queda como (1x14)+(-3.14)= 9.8
               with GetDiasVaca( sTabla, ( Trunc( rAntigUltimo )+1 ){$ifdef FLEXIBLES}, iEmpleado{$endif} )  do
               begin
                    Result := DiasVaca * ( 0 - Frac( rAntigUltimo ) );
                    rAuxPrima := DiasPrima * ( 0 - Frac( rAntigUltimo ) );
               end;
          end;
          { C�lculo de a�os completos }
          for iAntig := ( Trunc( rAntigUltimo ) + 1 ) to Trunc( rAntigProximo ) do
          begin
               with GetDiasVaca( sTabla, iAntig{$ifdef FLEXIBLES}, iEmpleado{$endif} ) do
               begin
                    Result := Result + DiasVaca;
                    rAuxPrima := rAuxPrima + DiasPrima;
               end;
          end;
          { Si hay una parte proporcional al final, se suma }
          if not lAnivProximo then
          begin
               with GetDiasVaca( sTabla, ( Trunc( rAntigProximo ) + 1 ){$ifdef FLEXIBLES}, iEmpleado{$endif} ) do
               begin
                    rDiasTabla := DiasVaca;
                    if oZetaProvider.GetGlobalBooleano( K_GLOBAL_AVENT ) then
                       rDiasBase := Round(Frac( rAntigProximo )*K_DIAS_ANNO_AVENT)
                    else
                        rDiasBase := Round(Frac( rAntigProximo )*365);
                    Result := Result + DiasVaca * Frac( rAntigProximo );
                    rAuxPrima := rAuxPrima + DiasPrima * Frac( rAntigProximo );
               end;
          end;
          Result := ZetaCommonTools.Redondea( Result );
          rDiasPrima := ZetaCommonTools.Redondea( rAuxPrima );
     end;
end;

function TdmPrestaciones.GetRenglonPresta(const sTabla: String; const iYear: Integer{$ifdef FLEXIBLES}; iEmpleado: Integer{$endif} ): TZetaCursor;
begin
     PreparaRenglon;
     with FQryRenglon do
     begin
         Active := FALSE;
         with oZetaProvider do
         begin
              ParamAsChar( FQryRenglon, 'Tabla', sTabla, 1 );
              ParamAsInteger( FQryRenglon, 'Year', iYear );
         end;
         Active := TRUE;
     end;
     Result := FQryRenglon;
end;

{$ifdef FALSE}
function TdmPrestaciones.GetDiasVaca( const sTabla: String; iYear, iEmpleado: Integer ): Currency;
begin
    if ( sTabla <> FCacheTabla ) then
        CachePrestaciones( sTabla, iEmpleado );

    if ( iYear < 1 ) then
       iYear := 1;
    if ( iYear > K_MAX_YEARS ) then
       iYear := K_MAX_YEARS;

    Result := aDiasVaca[ iYear ];
end;

procedure TdmPrestaciones.CachePrestaciones(const sTabla: String; iEmpleado: integer);
var
    iAnterior, iActual: Integer;
    nDiasVaca: Currency;

    procedure LlenaArreglo( const Inicial, Final : Integer; const DiasVaca : Currency );
    var
        i : Integer;
    begin
        for i := Inicial to Final do
            aDiasVaca[ i ] := DiasVaca;
    end;

begin
    PreparaQuery;
    iAnterior := 0;
    nDiasVaca := 0;
    with oZetaProvider do
    begin
         ParamAsChar( FQryPrestaciones, 'Tabla', sTabla, 1 );
         ParamAsInteger( FQryPrestaciones, 'Empleado', iEmpleado );
         with FQryPrestaciones do
         begin
              Open;
              while not Eof do
              begin
                  iActual   := Fields[ 0 ].AsInteger;   // PT_YEAR
                  nDiasVaca := Fields[ 1 ].AsFloat;     // PT_DIAS_VA

                  LlenaArreglo( iAnterior+1, iActual, nDiasVaca );
                  iAnterior := iActual;

                  Next;
              end;
              Close;
         end;
    end;
    { Llena el resto del arreglo }
    LlenaArreglo( iAnterior+1, K_MAX_YEARS, nDiasVaca );
    FCacheTabla := sTabla;
end;

function TdmPrestaciones.GetRenglonPresta(const sTabla: String; const iYear, iEmpleado: Integer): TZetaCursor;
begin
     PreparaRenglon;
     with FQryRenglon do
     begin
         Active := FALSE;
         with oZetaProvider do
         begin
              ParamAsChar( FQryRenglon, 'Tabla', sTabla, 1 );
              ParamAsInteger( FQryRenglon, 'Year', iYear );
              ParamAsinteger( FQryRenglon, 'Empleado', iEmpleado );
         end;
         Active := TRUE;
     end;
     Result := FQryRenglon;
end;

function TdmPrestaciones.VacaProporcional(const dAntig, dUltimoCierre, dProximoCierre: TDate; const sTabla: String; var rDiasTabla, rDiasBase: Currency; iEmpleado: integer): Currency;
var
   rAntigUltimo, rAntigProximo: Currency;
   lAnivUltimo, lAnivProximo: Boolean;
   iAntig: Integer;
begin
     Result     := 0;
     rDiasTabla := 0;
     rDiasBase  := 0;

     { Tiene que ser una fecha posterior al Cierre }
     if ( dProximoCierre > dUltimoCierre ) then
     begin
          if ( dUltimoCierre > 0 ) then  // Si hay cierre
             rAntigUltimo := YearsCompletos( dAntig, dUltimoCierre, lAnivUltimo, oZetaProvider.GetGlobalBooleano( K_GLOBAL_AVENT ) )
          else
          begin
               rAntigUltimo := 0;
               lAnivUltimo  := TRUE;
          end;
          rAntigProximo := YearsCompletos( dAntig, dProximoCierre, lAnivProximo, oZetaProvider.GetGlobalBooleano( K_GLOBAL_AVENT ) );
          { Si hay una parte proporcional al principio, se resta }
          if not lAnivUltimo then
          begin
               // 22/Nov/99: Estaba sumando de m�s con 2 cierres seguidos.
               // Result := dmCatalogos.GetDiasVaca( sTabla, Trunc( rAntigUltimo ) ) * ( 1 - Frac( rAntigUltimo ) );
               // rAntigUltimo := Trunc( rAntigUltimo ) + 1;

               // Lo correcto es que RESTE la fracci�n proporcional a lo que ya fu� Cerrado
               // La suma del la fracci�n complementaria se sumar� en el 'for' de a�os completos
               // o bien, en la parte proporcional final.
               // Ej. Si rAntigUltimo es 4.3, entonces se tiene restar el 0.3 utilizando
               // la tabla del a�o 5. Suponiendo que a�o 5 es 14 d�as. ( -0.3 x 14 )
               // Caso 1:  rAntigProximo es < 5, ej. 4.8
               // entonces se suma el 0.8 del a�o 5 :  (.8x14 )+(-.3x14 )= 7
               // Caso 2: rAntigProximo es > 5, ej. 6.5
               // entonces se suma el a�o 5 y 6 completos + el 0.5 del a�o 7
               // En ese caso el a�o 5 queda como (1x14)+(-3.14)= 9.8
               Result := GetDiasVaca( sTabla, Trunc( rAntigUltimo )+1, iEmpleado ) * ( 0 - Frac( rAntigUltimo ) );
          end;
          { C�lculo de a�os completos }
          for iAntig := ( Trunc( rAntigUltimo ) + 1 ) to Trunc( rAntigProximo ) do
          begin
               Result := Result + GetDiasVaca( sTabla, iAntig, iEmpleado );
          end;
          { Si hay una parte proporcional al final, se suma }
          if not lAnivProximo then
          begin
               rDiasTabla := GetDiasVaca( sTabla, ( Trunc( rAntigProximo ) + 1 ), iEmpleado );
               rDiasBase := Round(Frac( rAntigProximo )*365);
               Result := Result +  rDiasTabla * Frac( rAntigProximo );
          end;
          Result := ZetaCommonTools.Redondea( Result );
     end;
end;
{$endif}

{ ************* TSalarioIntegrado *********** }

constructor TSalarioIntegrado.Create( oCreator: TDataModule );
const
     {$ifdef FLEXIBLES}
     K_QRY_FACTOR = 'select PT_FACTOR '+
                     'from  DBO.SP_GET_RENGLON( :Tabla, :Year, :Empleado )';
     {$else}
     K_QRY_FACTOR   = 'select PT_FACTOR from PRESTACI ' +
                     'where TB_CODIGO = :Tabla AND PT_YEAR >= :Year ' +
                     'order by PT_YEAR';
     {$endif}
     K_QRY_OTRASPER = 'select TB_CODIGO, TB_TIPO, TB_IMSS, TB_MONTO, TB_TASA, TB_ISPT, TB_TOPE ' +
                     'from   OTRASPER ' +
                     'where  TB_CODIGO = :Codigo';
begin
     inherited Create;
     FCreator := oCreator;
     FListaPerFijas := TStringList.Create;
     with oZetaProvider do
     begin
          FQryFactor     := CreateQuery( K_QRY_FACTOR );
          FQryOtrasPer   := CreateQuery( K_QRY_OTRASPER );
     end;
end;

function TSalarioIntegrado.oZetaProvider: TdmZetaServerProvider;
begin
     Result := TZetaCreator( FCreator ).oZetaProvider;
end;

function TSalarioIntegrado.CalculaOtrasPer( const nSalario, nPerVar : TPesos; const nFactor : TTasa;
                                         const dReferencia : TDate; const sListaFijas : String ) : TPesos;
begin
    FSalarioBase     := Redondea( nSalario );
    FSalario         := Redondea( nSalario );
    FGravadoVariable := Redondea( nPerVar );
    FReferencia      := dReferencia;
    FAntiguedad      := dReferencia;
    FFactor          := nFactor;
    FPreIntegrado    := Redondea( Redondea( nSalario ) * FFactor );
    CalculaFijas( sListaFijas );
    FIntegrado       := Redondea( rMin( FIntegrado, TopeIntegrado ) );
    Result           := FIntegrado;
end;

function TSalarioIntegrado.CalculaIntegrado( const nSalario, nPerVar : TPesos;
                                             const dReferencia, dAntiguedad : TDate;
                                             const sTablaPresta, sListaFijas : String;
                                             const nRangoSal : TPesos
{$ifdef FLEXIBLES}
                                             ; const iEmpleado : integer
{$endif}
                                             ) : TPesos;
begin
    FSalarioBase     := Redondea( nSalario );
    FGravadoVariable := Redondea( nPerVar );
    FReferencia      := dReferencia;
    FAntiguedad      := dAntiguedad;
    FTablaPresta     := sTablaPresta;
    FFactor          := GetFactorIntegracion{$ifdef FLEXIBLES}( iEmpleado ){$endif};
    //Matsushita
    FRangoSal        := nRangoSal;
    if ( FRangoSal = 0 ) then                       //Para los que no manejan Rango Sal viene 100 o 0
       FRangoSal := 100;
    if ( nRangoSal <> 100 ) then
         FSalario := CalculaRangoSal( sListaFijas )
    else
         FSalario := FSalarioBase;

    FPreIntegrado    := Redondea( FSalario * FFactor );
    CalculaFijas( sListaFijas );  //FOtrasFijas
    FIntegrado	     := Redondea( rMin( FIntegrado, TopeIntegrado ) );
    Result           := FIntegrado;
end;

destructor TSalarioIntegrado.Destroy;
begin
     LimpiaListaFijas;
     FListaPerFijas.Free;
     inherited;
end;

function TSalarioIntegrado.GetFactorIntegracion{$ifdef FLEXIBLES}( const iEmpleado: integer ){$endif}: TTasa;
var
    iAntiguedad : Integer;
begin
    iAntiguedad := zYearsBetween( FAntiguedad, FReferencia ) + 1;
    with FQryFactor do
    begin
         Active := FALSE;
         with oZetaProvider do
         begin
              ParamAsChar( FQryFactor, 'Tabla', FTablaPresta, 1 );
              ParamAsInteger( FQryFactor, 'Year', iAntiguedad );
              {$ifdef FLEXIBLES}
              ParamAsInteger( FQryFactor, 'Empleado', iEmpleado );
              {$endif}
         end;
         Active := TRUE;
         if Eof then
             Result := 1
         else
             Result := Fields[ 0 ].AsFloat;
         Active := False;
    end;
end;

procedure TSalarioIntegrado.LimpiaListaFijas;
var
    i : Integer;
begin
    with FListaPerFijas do
    begin
        for i := 0 to Count-1 do
            TOtrasPercepciones( Objects[ i ] ).Free;
        Clear;
    end;
end;

procedure TSalarioIntegrado.CalculaFijas(const Value: String);
var
    i : Integer;
    sCodigo : String;
    oPerFija : TOtrasPercepciones;
begin
  LimpiaListaFijas;
  FListaPerFijas.CommaText := Value;

  FIntegrado    := FPreIntegrado + FGravadoVariable;
  FOtrasFijas   := 0;
  FGravadoFijo  := 0;
  FISPT         := 0;

  with FListaPerFijas do
    for i := 0 to Count-1 do
    begin
        sCodigo := Strings[ i ];
        oPerFija:= CreaPerFija( sCodigo );
        Objects[ i ] := oPerFija;

        // Acumula Totales
{$ifdef ANTES}
        with oPerFija do
        begin
            FOtrasFijas   := FOtrasFijas  + Monto;
            FGravadoFijo  := FGravadoFijo + Gravado;
            FISPT         := FISPT        + ISPT;
            FIntegrado    := FIntegrado   + Gravado;
        end;
{$else}
        FOtrasFijas   := FOtrasFijas  + oPerFija.Monto;
        FGravadoFijo  := FGravadoFijo + oPerFija.Gravado;
        FISPT         := FISPT        + oPerFija.ISPT;    // Chocaba con el ISPT de oPerFija
        FIntegrado    := FIntegrado   + oPerFija.Gravado;
{$endif}
    end;
end;

function TSalarioIntegrado.CreaPerFija( const sCodigo : String ) : TOtrasPercepciones;
var
    Tipo : eTipoOtrasPer;
    Tope, MontoFijo : TPesos;
    Tasa : TTasa;
begin
    Result := TOtrasPercepciones.Create;

    with FQryOtrasPer do
    begin
         Active := FALSE;
         oZetaProvider.ParamAsChar( FQryOtrasPer, 'Codigo', sCodigo, 2 );
         Active := TRUE;

         with Result do
         begin
              Tipo      := eTipoOtrasPer( Fields[ 1 ].AsInteger );
              FCritIMSS := eIMSSOtrasPer( Fields[ 2 ].AsInteger );
              MontoFijo := Fields[ 3 ].AsFloat;
              Tasa      := Fields[ 4 ].AsFloat;
              FCritISPT := eISPTOtrasPer( Fields[ 5 ].AsInteger );
              Tope      := Fields[ 6 ].AsFloat;
         end;
         Active := FALSE;
    end;

    with Result do
    begin
        if ( Tipo = toCantidad ) then
            FMonto := MontoFijo
        else
        begin
            FMonto := ( FSalarioBase * ( Tasa / 100 ) );
            if ( Tope > 0 ) then
                FMonto := rMin( FMonto, Tope );
        end;

        FMonto := Redondea( FMonto );                   // Convertir a Pesos - Dejar con dos decimales

        // Calcula Impuestos
        case FCritIMSS of
             ioExento     :	FGravado := 0;
             ioGravado    :	FGravado := FMonto;
             ioAsistencia :	FGravado := rMax( FMonto-FIntegrado*0.10, 0 );
             {$ifdef IMSS_UMA}
             ioDespensa   :	FGravado := rMax( FMonto-ValUMA*0.40, 0 );
             {$else}
             ioDespensa   :	FGravado := rMax( FMonto-SMGDF*0.40, 0 );
             {$endif}
        end;

        FGravado := Redondea( FGravado );               // Convertir a Pesos - Dejar con dos decimales

        if FCritISPT = soExento then
            FISPT := 0
        else
            FISPT := Monto;
    end;
end;

{$ifdef IMSS_UMA}
function TSalarioIntegrado.ValUMA: TPesos;
begin
     with TZetaCreator( FCreator ) do
     begin
          PreparaValUma;
          Result := dmValUma.GetVUDefaultDiario( FReferencia );
     end;
end;
{$else}
function TSalarioIntegrado.SMGDF: TPesos;
begin
     with TZetaCreator( FCreator ) do
     begin
          PreparaSalMin;
          Result := dmSalMin.GetSMGDF( FReferencia );
     end;
end;
{$endif}

function TSalarioIntegrado.TopeIntegrado: TPesos;
begin
     {$ifdef IMSS_UMA}
     Result := 25 * ValUMA;
     {$else}
     Result := 25 * SMGDF;
     {$endif}
end;

{ Matsushita }

function TsalarioIntegrado.CalculaRangoSal( const sFijas : String ): TPesos;
begin
     Result:= FSalarioBase;
     if ( FSalarioBase > 0 ) then
     begin
          FRangoSal:= FRangoSal / 100;  // Convertir a Porcentaje
          Result:= Redondea( ( FRangoSal + ( SumaFijas( sFijas ) / FSalarioBase ) * ( FRangoSal - 1 ) )* FSalarioBase );
          FRangoSal:= FRangoSal * 100;
     end;
end;

function TSalarioIntegrado.SumaFijas( const Value : String )  : TPesos;
var
    i : Integer;
    sCodigo : String;
    oPerFija : TOtrasPercepciones;
begin
  LimpiaListaFijas;
  FListaPerFijas.CommaText := Value;
  Result := 0;
  with FListaPerFijas do
    for i := 0 to Count-1 do
    begin
        sCodigo := Strings[ i ];
        oPerFija:= CreaPerFija( sCodigo );
        Objects[ i ] := oPerFija;
        Result := Result + oPerFija.Monto;
    end;
end;

{$ifdef FALSE}
function TSalarioIntegrado.CalculaIntegrado( const nSalario, nPerVar : TPesos;
                                             const dReferencia, dAntiguedad : TDate;
                                             const sTablaPresta, sListaFijas : String;
                                             const nRangoSal : TPesos;
                                             const iEmpleado : integer ) : TPesos;
begin
    FSalarioBase     := Redondea( nSalario );
    FGravadoVariable := Redondea( nPerVar );
    FReferencia      := dReferencia;
    FAntiguedad      := dAntiguedad;
    FTablaPresta     := sTablaPresta;
    FFactor          := GetFactorIntegracion( iEmpleado );
    //Matsushita
    FRangoSal        := nRangoSal;
    if ( FRangoSal = 0 ) then                       //Para los que no manejan Rango Sal viene 100 o 0
       FRangoSal := 100;
    if ( nRangoSal <> 100 ) then
         FSalario := CalculaRangoSal( sListaFijas )
    else
         FSalario := FSalarioBase;

    FPreIntegrado    := Redondea( FSalario * FFactor );
    CalculaFijas( sListaFijas );  //FOtrasFijas
    FIntegrado	     := Redondea( rMin( FIntegrado, TopeIntegrado ) );
    Result           := FIntegrado;
end;

function TSalarioIntegrado.GetFactorIntegracion( const iEmpleado: integer ): TTasa;
var
    iAntiguedad : Integer;
begin
    iAntiguedad := zYearsBetween( FAntiguedad, FReferencia ) + 1;
    with FQryFactor do
    begin
         Active := FALSE;
         with oZetaProvider do
         begin
              ParamAsChar( FQryFactor, 'Tabla', FTablaPresta, 1 );
              ParamAsInteger( FQryFactor, 'Year', iAntiguedad );
              ParamAsInteger( FQryFactor, 'Empleado', iEmpleado );
         end;
         Active := TRUE;
         if Eof then
             Result := 1
         else
             Result := Fields[ 0 ].AsFloat;
         Active := False;
    end;
end;
{$endif}

end.
