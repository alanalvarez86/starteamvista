unit DServerLabor;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.7 / XE3                           ::
  :: Unidad:      DServerLabor.pas                           ::
  :: Descripci�n: Programa principal de Labor.dll            ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComServ, ComObj, VCLCom, StdVcl, {BdeMts,} DataBkr, DB, DBClient, Provider,
     MtsRdm, Mtx, Mask,
     Variants,MaskUtils,
     {$ifndef DOS_CAPAS}
     Labor_TLB,
     {$endif}
     DZetaServerProvider,
     ZetaCommonClasses,
     ZetaCommonLists,                               
     ZCreator,
     ZetaSQLBroker,
     ZetaServerDataSet,
     DLaborCalculo;

type
  eTipoCatalogo = ( eNinguna,       {0}
                    eModula1,       {1}
                    eModula2,       {2}
                    eModula3,       {3}
                    eTiempoMuerto,  {4}
                    eTParte,        {5}
                    eTOpera,        {6}
                    ePartes,        {7}
                    eOpera,         {8}
                    eOrdenesFijas,  {9}
                    eWorks,         {10}
                    eLecturas,      {11}
                    eDefSteps,      {12}
                    eDatosEmpleado, {13}
                    eWOrder,        {14}
                    eArea,          {15}
                    eSteps,         {16}
                    eWOrderWorks,   {17}
                    eAusencia,      {18}
                    eCedulas,       {19}
                    eCedEmp,        {20}
                    eCedMulti,      {21}
                    eBreaks,        {22}
                    eSupArea,       {23}
                    eKardexArea,    {24}
                    eTDefecto,      {25}
                    eCedulasInsp,   {26}
                    eDefecto,       {27}
                    eMotivoScrap,   {28}
                    eComponentes,   {29}
                    eCedulasScrap,  {30}
                    eScrap,         {31}
                    eTMaquinas,     {32}
                    eMaquinas,      {33}
                    eMaqCert,       {34}
                    eMaqArea,       {35}
                    eLayoutProd,    {36}
                    eKarEmpMaq,     {37}
                    eLayMap,        {38}
                    eSillasLay,     {39}
                    eSillasMaq      {40}
                    );

  eTipoArea = ( taOtraArea,                             // Es Mi Empleado y No Est� en Mis Areas
                taMisAreas,                             // Es Mi Empleado y Est� en Mis Areas
                taNoArea,                               // Es Mi Empleado y No Tiene Area Asignada
                taOtroSuper );                          // No Es Mi Empleado y Est� en Mis Areas
  TdmServerLabor = class(TMtsDataModule {$ifndef DOS_CAPAS}, IdmServerLabor {$endif} )
    cdsLog: TServerDataSet;
    IntegerField1: TIntegerField;
    DateTimeField1: TDateTimeField;
    cdsLista: TServerDataSet;
    cdsAdicional: TServerDataSet;
    cdsAdicional2: TServerDataSet;
    procedure MtsDataModuleCreate(Sender: TObject);
    procedure MtsDataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
    {$ifndef DOS_CAPAS}
    oZetaSQLBroker: TSQLBroker;
    oZetaCreator: TZetaCreator;
    {$endif}
    FWoNumber: String;
    FLaborCalculo: TLCalculo;
    FFecha: TDate;
    FHora: String;
    FDuracion: Integer;
    FListaGridLabor: String;
    FDataEmp: TClientDataSet;
    FDataMulti: TClientDataSet;
    FDataSet, FDataSetAdic: TZetaCursor;
    FQryAreas: TZetaCursorLocate;
    FListaParametros: string;
    FCedulaInspeccion: integer;
    FCedulaScrap: integer;
    FOperacion: eOperacionConflicto;

    function CancelarBreaksDataset(Dataset: TDataset): OleVariant;
    function FiltroNivel0( const Empresa : OleVariant ): String;
    function GetFiltrosWOrder: String;
    function GetNombreTabla( const eCatalogo: eTipoCatalogo): String;
    function GetSQLBroker: TSQLBroker;
    function ValidaHoraCedula(const sArea: TCodigo; const dFecha: TDate; const sFinal: String; const iDuracion: Integer;var sMensaje: String ): Boolean;
    function GetFiltroCedula(const sFiltro, sLookUp, sFormatFiltro: String): String;
    function GetMaxCedula(const iScript: integer): Integer;
    function GetMaxCedulaScrap: Integer;
    function GetMaxCedulaInspeccion: Integer;
    function ExisteCodigoImportar( const sCodigo: String; Dataset: TZetaCursor ): Boolean;
    function ObtieneListaASCII( ListaASCII: OleVariant; var lError: Boolean; var sError: String ): Integer;
    procedure ValidaFechaLabor( DeltaDS: TzProviderClientDataSet; const UpdateKind: TUpdateKind; const sCampo: string );
    procedure AfterUpdateCedulas(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    procedure AfterUpdateBorrarCedulas(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    procedure AfterUpdateKarArea(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    procedure BeforeUpdateCedulas(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateBorrarCedulas(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateEmpBreaks(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateWorks(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateKarArea(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateMaquinas( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean );
    //procedure BeforeUpdateWorderSteps(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure InitCreator;
    procedure InitBroker;
    procedure ClearBroker;
    procedure InitImportLog;
    procedure EscribeBitacoraCedula(UpdateKind: TUpdateKind; DeltaDS: TzProviderClientDataSet);
    procedure BeforeUpdateAsignaAreas(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateCedInspeccion( Sender: TObject;SourceDS: TDataSet;DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind;var Applied: Boolean );
    procedure OnErrorGridLabor(Sender: TObject; DataSet: TzProviderClientDataSet; E: EUpdateError; UpdateKind: TUpdateKind; var Response: TResolverResponse);
    procedure BeforeUpdateCedulaScrap(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure ImportarOrdenesValidaASCII(DataSet: TDataset; var iProblemas: Integer; var sErrorMsg: String);
    procedure RecalculaKardexArea( const sListaEmp: String );
    procedure CancelarBreaksBuildDataset;
    procedure DepuracionParametros;
    procedure ImportarCatalogosParametros;
    procedure ImportarCedulasParametros;
    procedure CancelarBreaksParametros;
    procedure SetTablaInfo(const eCatalogo: eTipoCatalogo);
    procedure SetDetailInfo(const eCatalogo: eTipoCatalogo);
    procedure ImportarCedulasValidaASCII(DataSet: TDataset; var iProblemas: Integer; var sErrorMsg: String);
    procedure FillCatalogosCedula;
    procedure ReleaseCatalogosCedula;
  protected
    { Protected declarations }
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    property SQLBroker: TSQLBroker read GetSQLBroker;
    {$ifdef DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
    {$endif}
    function GetBreaks(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCatalogo(Empresa: OleVariant; Catalogo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaCatalogo(Empresa: OleVariant; Catalogo: Integer; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetOrdenesFijas(Empresa: OleVariant; Empleado: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetLecturasEmpleado(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetWorksEmpleado(Empresa: OleVariant; var Ausencia: OleVariant; Empleado: Integer; Fecha: TDateTime): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCatalogoDetail(Empresa: OleVariant; Catalogo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaCatalogoDetail(Empresa: OleVariant; Catalogo: Integer; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetDatosEmpleado(Empresa: OleVariant; Empleado: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetLecturas(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetWOrder(Empresa, Parametros: OleVariant; Tipo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetWOrderSteps(Empresa: OleVariant; var Steps: OleVariant; Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetKardexWorder(Empresa: OleVariant; var WorksHijo: OleVariant; Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaWorderSteps(Empresa, Delta, Steps: OleVariant; const WOrderNumber: WideString; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function BorraWOrder(Empresa: OleVariant; const WOrderNumber: WideString; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function BorraParte(Empresa: OleVariant; const Parte: WideString; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaPartes(Empresa, Delta, DefSteps: OleVariant; const Parte: WideString; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetPartesSteps(Empresa: OleVariant; var DefSteps: OleVariant; const Parte: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaGridLabor(Empresa: OleVariant; const Area: WideString; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaSalidaWorks(Empresa, OtrosDatos, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function Depuracion(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaWorks(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCedulas(Empresa: OleVariant; Fecha: TDateTime): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCedEmpleados(Empresa: OleVariant; Folio: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCedMultiLote(Empresa: OleVariant; Folio: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaCedula(Empresa, oDelta, oDataEmp, oDataMulti: OleVariant; Parametros: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function BorraCedula(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetHistorialCedulas(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaBreaks(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function ImportarOrdenes(Empresa, Parametros, ListaASCII: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function ImportarPartes(Empresa, Parametros, ListaASCII: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function  GetTiempos(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    procedure GrabaTiempos(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime; Datos: OleVariant); {$ifndef DOS_CAPAS} safecall; {$endif}
    function  GrabaAsignaAreas(Empresa: OleVariant; oDelta: OleVariant; Parametros: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function  GetSupArea(Empresa: OleVariant; const Supervisor: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function  GrabaSupArea(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer; const Supervisor: WideString ): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function  GetKardexArea(Empresa: OleVariant; Empleado: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function  GrabaKardexArea(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function  GetAreas(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function  GetEmpAreas(Empresa: OleVariant; Fecha: TDateTime; const Hora: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function  GetAreaKardex(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime; const Hora: WideString): WideString; {$ifndef DOS_CAPAS} safecall; {$endif}
    function  GetEmpBreaks(Empresa: OleVariant; const Area: WideString; Fecha: TDateTime; const Hora: WideString; lSoloAltas: WordBool): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function  GrabaEmpBreaks(Empresa: OleVariant; oDelta: OleVariant; Fecha: TDateTime; const Hora: WideString; Duracion: Integer; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function  CancelarBreaksGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function  CancelarBreaksLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function  CancelarBreaks(Empresa: OleVariant; Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function  GetHistorialCedInsp(Empresa: OleVariant; Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function  GetCedInspeccion(Empresa: OleVariant; var Defectos: Olevariant; Cedula: integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function  GrabaCedInspeccion(Empresa, Delta, Defectos: Olevariant; Cedula: integer; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function  BorraCedulasInsp(Empresa: OleVariant; Cedula: Integer; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function  GetHistorialCedScrap(Empresa: OleVariant; Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function  GetCedulaScrap(Empresa: OleVariant; var Scrap: Olevariant; Cedula: integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function  BorraCedulaScrap(Empresa: OleVariant; Cedula: Integer; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function  GrabaCedulaScrap(Empresa: OleVariant; Delta: OleVariant; Scrap: OleVariant; Cedula: Integer; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function  GetComponentes(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function  GetComponente(Empresa: OleVariant; const Componente: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function  GrabaComponente(Empresa: OleVariant; Delta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function  ImportarComponentes(Empresa: OleVariant; Parametros: OleVariant; ListaASCII: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function  ImportarCedulas(Empresa, Parametros, ListaASCII: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function  GetKardexDiario(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function  GetCertificaciones(Empresa: OleVariant; const Maquina: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function  GrabaCertificaciones(Empresa: OleVariant; Delta: OleVariant; const Maquina: WideString; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetMapa(Empresa: OleVariant; const Layout: WideString;out oSillas: OleVariant;out oMaqSilla: OleVariant;out oCertMaq: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetLayoutsSuper(Empresa: OleVariant; Usuario: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function AsignaEmpleado(Empresa: OleVariant; oZetaParams: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetMaquinaEmpleados(Empresa: OleVariant; const Layout: WideString;const Hora: WideString; Fecha: TDateTime): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCertificacionesEmpleados(Empresa: OleVariant; Usuario: Integer; Fecha: TDateTime): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetKardexEmpleadoMaquina(Empresa: OleVariant; Empleado: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetKardexMaquinaEmpleado(Empresa: OleVariant; const Maquina: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}

  end;

var
  dmServerLabor: TdmServerLabor;

implementation

{$R *.DFM}

uses ZetaCommonTools,
     ZetaServerTools,
     ZetaTipoEntidad,
     ZGlobalTress,
     DTarjeta,
     DQueries,
     DSuperASCII;

const
     K_ERROR_BREAKS = 'Error al Registrar Breaks ';
     K_ANCHO_IMP_DATA = 50;
     K_ANCHO_IMP_ERROR = 255;
     K_ANCHO_DESCRIPCION = 255;
     K_ANCHO_TEXTO = 40;
     K_ANCHO_ELEMENT = 30;
     K_ANCHO_TEXTITO = 20;
     K_ANCHO_INGLES = 40;
     K_ANCHO_TIPO_PARTE = 6;
     K_ANCHO_FLOAT = 15;
     {$ifdef HYUNDAI}
     K_ANCHO_AREA10 = 10;
     K_ANCHO_PARTE18 = 18;
     {$endif}
     K_LEE_CAMPOS_CEDULA = 'CE_FOLIO, WO_NUMBER,AR_CODIGO,OP_NUMBER, CE_AREA, CE_TIPO, CE_FECHA,' +
                           'CE_HORA, CE_STATUS, CE_MOD_1, CE_MOD_2, CE_MOD_3, CE_TMUERTO, CE_PIEZAS,' +
                           'CE_COMENTA, CE_MULTI, CE_TIEMPO, US_CODIGO, ''N'' SETCAMBIOS '{$ifdef LA_ADA}+',CE_CONTEO '{$endif};
     K_LEE_CAMPOS_CED_INSP = 'CI_FOLIO,CE_FOLIO, WO_NUMBER, AR_CODIGO, CI_AREA, CI_TIPO, CI_FECHA,' +
                             'CI_HORA, CI_RESULT, CI_COMENTA, CI_TIEMPO, CI_TAMLOTE, US_CODIGO';
     K_LEE_CAMPOS_CED_SCRAP = 'CS_FOLIO,CS_AREA,CS_FECHA,CS_HORA,WO_NUMBER,AR_CODIGO,OP_NUMBER,'+
                             'CS_FEC_FAB,CS_TAMLOTE,CS_COMENTA,CS_OBSERVA,CS_NUMERO1,CS_NUMERO2,CS_TEXTO1,CS_TEXTO2,US_CODIGO';
     K_SIN_REGRESO = 0;
     K_PRIMERA_HORA = 1;

     Q_ORDENES_FIJAS = 0;
     Q_EMPLEADO = 1;
     Q_EMPLEADO_INIT_AREA = 2;
     Q_EMPLEADO_UPDATE_AREA = 3;
     Q_EMPLEADO_ESCRIBE_AREA = 4;
     Q_WORDER_KARDEX = 5;
     Q_WORDER_BORRA = 6;
     Q_WORDER_IMPORT_BORRA = 7;
     Q_WORDER_IMPORT_AGREGA = 8;
     Q_STEPS_BORRA = 9;
     Q_STEPS_AGREGA = 10;
     Q_PARTE_BORRA = 11;
     Q_DEFSTEPS_BORRA = 12;
     Q_DEFSTEPS_AGREGA = 13;
     Q_LECTURAS_DEPURAR = 14;
     Q_WORKS_DEPURAR = 15;
     Q_WORKS_CEDULA_BORRA = 16;
     Q_WORKS_CEDULA_AGREGA = 17;
     {
     Q_WORKS_CEDULA_LEE = 18;
     }
     Q_CEDULA_ULTIMA = 19;
     Q_CEDULA_LEER = 20;
     Q_CEDULA_EMPLEADOS = 21;
     Q_CEDULA_EMPLEADOS_AGREGA = 22;
     Q_CEDULA_MULTILOTE = 23;
     Q_CEDULA_WORDER_AGREGA = 24;
     Q_CEDULA_HISTORIAL = 25;
     Q_CEDULA_DEPURAR = 26;
     Q_CEDULA_CUENTA = 27;
     Q_TABLA_BORRAR = 28;
     Q_PARTES_EXISTE = 29;
     Q_PARTES_IMPORT_MODIFICA = 30;
     Q_PARTES_IMPORT_AGREGA = 31;
     Q_TIPO_PARTES_EXISTE = 32;
     Q_TIPO_PARTES_AGREGA = 33;
     Q_TIEMPOS_LEE = 34;
     Q_TIEMPOS_BORRA = 35;
     Q_TIEMPOS_INSERTA = 36;
     Q_TIEMPOS_HAY_TARJETA = 37;
     Q_COLABORA_UPDATE_AREA = 38;
     Q_RECALCULA_KARDEX_AREA = 39;
     Q_CAT_AREAS_SUPERVISORES = 40;
     Q_EMPLEADOS_ASIGNA = 41;
     Q_EMPLEADOS_AREAS = 42;
     Q_EMPLEADO_NIVEL = 43;
     Q_EMPLEADO_AREA = 44;
     Q_EMPLEADO_AREA_KARDEX = 45;
     Q_INSERT_KARDEX_AREA = 46;
     Q_GET_AREA_PRIMERA_HORA = 47;
     Q_CED_EMP_CEDULA_LEE = 48;
     Q_EMPLEADOS_AREA = 49;
     Q_SUP_AREA_DELETE = 50;
     Q_TMPLABOR_BORRAR = 51;
     Q_TMPLABOR_AGREGAR = 52;
     Q_BORRA_BREAKS =  53;
     Q_DELETE_KARDEX_AREA = 54;
     Q_CEDULA_INSP_HISTORIAL = 55;
     Q_DEFECTOS_BORRA = 56;
     Q_MAX_CEDULAINSPECCION = 57;
     Q_BORRA_CED_INSP = 58;
     Q_DEFECTOS_INSERT = 59;
     Q_BREAKS_LEE = 60;
     Q_CEDULA_INSPECCION_DEPURAR = 61;
     Q_GET_COMPONENTES = 62;
     Q_BORRA_CED_SCRAP = 63;
     Q_CEDULA_SCRAP_HISTORIAL = 64;
     Q_SCRAP_BORRA = 65;
     Q_SCRAP_INSERT = 66;
     Q_MAX_CEDULASCRAP = 67;
     Q_CEDULA_SCRAP_DEPURAR = 68;
     Q_COMPONENTES_EXISTE = 69;
     Q_COMPONENTES_IMPORT_MODIFICA = 70;
     Q_COMPONENTES_IMPORT_AGREGA = 71;
     Q_CEDULA_IMPORT_AGREGA = 72;
     Q_GET_LAYOUTS_SUPER = 73;
     Q_ASIGNA_EMPLEADO = 74;
     Q_EMPLEADOS_LAYOUT = 75;
     Q_KAR_EMP_MAQ = 76;
     Q_GET_CERTIFIC_EMPLEADOS = 77;
     Q_MAQUINA_USO = 78;

function GetSQLScript( const iScript: Integer ): String;
begin
     case iScript of
          Q_ORDENES_FIJAS: Result := 'select WOFIJA.CB_CODIGO, WF_FEC_INI, WO_NUMBER, AR_CODIGO, OP_NUMBER, '+
                                     K_PRETTYNAME + ' AS PRETTYNAME '+
                                     'from WOFIJA left outer join COLABORA on ( COLABORA.CB_CODIGO = WOFIJA.CB_CODIGO )';
          Q_EMPLEADO: Result := 'select B.CB_CODIGO, B.CB_APE_PAT, B.CB_APE_MAT, B.CB_NOMBRES, B.CB_PUESTO, B.CB_TURNO, B.CB_CLASIFI, B.CB_AREA,'+
                                'B.CB_NIVEL1, B.CB_NIVEL2, B.CB_NIVEL3, B.CB_NIVEL4, B.CB_NIVEL5, B.CB_NIVEL6, B.CB_NIVEL7, B.CB_NIVEL8, B.CB_NIVEL9, ' +
                                {$ifdef ACS}'B.CB_NIVEL10, B.CB_NIVEL11, B.CB_NIVEL12, '+{$endif}                                
                                'B.CB_FEC_ING, C.TB_ELEMENT CONTRATO, IMAGEN.IM_BLOB as FOTOGRAFIA '+
                                'from COLABORA B ' +
                                'left outer join CONTRATO C on ( C.TB_CODIGO = B.CB_CONTRAT )  '+
                                'left outer join IMAGEN on ( IMAGEN.CB_CODIGO = B.CB_CODIGO ) and ( IMAGEN.IM_TIPO = ''FOTO'' ) '+
                                'where ( B.CB_CODIGO = %d ) ';
          Q_EMPLEADO_INIT_AREA: Result := 'update COLABORA set CB_AREA = '''' where ( CB_AREA = ''%s'' )';
          Q_EMPLEADO_UPDATE_AREA: Result := 'update COLABORA set CB_AREA = ''%s'' where ( CB_CODIGO in ( %s ) )';
          Q_EMPLEADO_ESCRIBE_AREA: Result := 'update COLABORA set CB_AREA = ''%s'' where ( CB_CODIGO = %s )';
          Q_WORDER_KARDEX: Result := 'select WORKS.CB_CODIGO, AU_FECHA, OP_NUMBER, WK_STATUS, WK_TIEMPO, '+
                                     K_PRETTYNAME + ' as PRETTYNAME '+
                                     'from WORKS '+
                                     'left outer join COLABORA on ( COLABORA.CB_CODIGO = WORKS.CB_CODIGO ) '+
                                     'where ( WO_NUMBER = ''%s'' )';
          Q_WORDER_BORRA: Result := 'delete from WORDER where ( WO_NUMBER = ''%s'' )';
          Q_WORDER_IMPORT_BORRA: Result := 'delete from WORDER where ( ( select COUNT(*) from WOFIJA where ( WOFIJA.WO_NUMBER = WORDER.WO_NUMBER ) ) = 0 )';
          Q_WORDER_IMPORT_AGREGA: Result := 'insert into WORDER( WO_NUMBER, ' +
                                                                'AR_CODIGO, ' +
                                                                'WO_DESCRIP, ' +
                                                                'WO_QTY, ' +
                                                                'WO_FINISHD, ' +
                                                                'WO_FEC_INI, ' +
                                                                'WO_FEC_FIN, ' +
                                                                'WO_FEC_CIE, ' +
                                                                'WO_STATUS, ' +
                                                                'WO_LST_STP, ' +
                                                                'WO_TEXTO, ' +
                                                                'WO_NUM_GEN ) values (' +
                                                                ':WO_NUMBER, ' +
                                                                ':AR_CODIGO, ' +
                                                                ':WO_DESCRIP, ' +
                                                                ':WO_QTY, ' +
                                                                ':WO_FINISHD, ' +
                                                                ':WO_FEC_INI, ' +
                                                                ':WO_FEC_FIN, ' +
                                                                ':WO_FEC_CIE, ' +
                                                                ':WO_STATUS, ' +
                                                                ':WO_LST_STP, ' +
                                                                ':WO_TEXTO, ' +
                                                                ':WO_NUM_GEN ) ';
          Q_STEPS_BORRA: Result := 'delete from STEPS where ( WO_NUMBER = ''%s'' )';
          Q_STEPS_AGREGA: Result := 'insert into STEPS( WO_NUMBER, ST_SEQUENC, OP_NUMBER, ST_STD_HR, ST_REAL_HR, ST_QTY ) '+
                                    'values ( ''%s'', %d, ''%s'', %s, %s, %s )';
          Q_PARTE_BORRA: Result := 'delete from PARTES where ( AR_CODIGO = ''%s'' )';
          Q_DEFSTEPS_BORRA: Result := 'delete from DEFSTEPS where ( AR_CODIGO = ''%s'' )';
          Q_DEFSTEPS_AGREGA: Result := 'insert into DEFSTEPS( AR_CODIGO, DF_SEQUENC, OP_NUMBER, DF_STD_HR ) '+
                                       'values ( ''%s'', %d, ''%s'', %s )';
          Q_LECTURAS_DEPURAR: Result := 'delete from LECTURAS where ( LX_FECHA < ''%s'' )';
          Q_WORKS_DEPURAR: Result := 'delete from WORKS where ( AU_FECHA < ''%s'' )';
          Q_WORKS_CEDULA_BORRA: Result := 'delete from WORKS where ( WK_CEDULA = %d )';
          Q_WORKS_CEDULA_AGREGA: Result := 'insert into WORKS( CB_CODIGO, AU_FECHA, OP_NUMBER, WO_NUMBER, AR_CODIGO,' +
                                           'WK_HORA_A, WK_HORA_R, WK_TIEMPO, WK_STATUS, WK_MOD_1, WK_MOD_2, WK_MOD_3, WK_TMUERTO,' +
                                           'WK_MANUAL, CB_AREA, CB_PUESTO, WK_FOLIO, WK_TIPO, WK_PIEZAS, WK_CEDULA ) ' +
                                           'values ( :CB_CODIGO,:AU_FECHA,:OP_NUMBER,:WO_NUMBER,:AR_CODIGO,' +
                                           ':WK_HORA_A, :WK_HORA_R, :WK_TIEMPO, :WK_STATUS, :WK_MOD_1, :WK_MOD_2, :WK_MOD_3, :WK_TMUERTO,' +
                                           ':WK_MANUAL, :CB_AREA, :CB_PUESTO, :WK_FOLIO, :WK_TIPO, :WK_PIEZAS, :WK_CEDULA )';
          Q_CEDULA_ULTIMA: Result := 'select MAX( CE_FOLIO ) RESULTADO from CEDULA';
          Q_CEDULA_LEER: Result := 'select '+ K_LEE_CAMPOS_CEDULA + ' from CEDULA where ( US_CODIGO = %d ) and ( CE_FECHA = ''%s'' ) order by CE_HORA';
          Q_CEDULA_EMPLEADOS: Result := 'select A.CE_FOLIO, A.CB_CODIGO, A.CE_POSICIO, A.CB_PUESTO,' +
                                        K_PRETTYNAME + ' as PRETTYNAME ' +
                                        'from CED_EMP A join COLABORA B on ( B.CB_CODIGO = A.CB_CODIGO ) ' +
                                        'where ( A.CE_FOLIO = %d ) order by A.CE_POSICIO ASC';
          Q_CEDULA_EMPLEADOS_AGREGA: Result := 'insert into CED_EMP( CE_FOLIO, CB_CODIGO, CE_POSICIO, CB_PUESTO ) ' +
                                               'values ( :CE_FOLIO, :CB_CODIGO, :CE_POSICIO, :CB_PUESTO )';
          Q_CEDULA_MULTILOTE: Result := 'select A.CE_FOLIO, A.WO_NUMBER, A.CW_POSICIO, A.CW_PIEZAS, B.WO_DESCRIP, B.AR_CODIGO ' +
                                        'from CED_WORD A left outer join WORDER B on ( B.WO_NUMBER = A.WO_NUMBER ) ' +
                                        'where ( A.CE_FOLIO = %d ) order by A.CW_POSICIO';
          Q_CEDULA_WORDER_AGREGA: Result := 'insert into CED_WORD( CE_FOLIO, WO_NUMBER, CW_POSICIO, CW_PIEZAS ) ' +
                                            'values ( :CE_FOLIO, :WO_NUMBER, :CW_POSICIO, :CW_PIEZAS )';
          {$ifdef MSSQL}
          Q_CEDULA_HISTORIAL: Result := 'select '+ K_LEE_CAMPOS_CEDULA + ' ,CW_PIEZAS,VAR_CODIGO from V_CED_WORD %s order by CE_FOLIO';
          {$else}
          Q_CEDULA_HISTORIAL: Result := 'select '+ K_LEE_CAMPOS_CEDULA + 'from CEDULA %s order by CE_FOLIO';
          {$endif}
          Q_CEDULA_DEPURAR: Result := 'delete from CEDULA where ( CE_FECHA < ''%s'' )';
          Q_CEDULA_CUENTA: Result := 'select COUNT( * ) CUANTOS from CEDULA where ( CE_FECHA >= ''%s'' ) and ( CE_FECHA <= ''%s'' )';
          Q_TABLA_BORRAR: Result := 'delete from %s where ( CE_FOLIO = %d )';
          Q_PARTES_EXISTE: Result := 'select COUNT(*) CUENTA from PARTES where ( AR_CODIGO = :Codigo )';
          Q_PARTES_IMPORT_MODIFICA: Result := 'update PARTES set ' +
                                              'AR_NOMBRE = :AR_NOMBRE, '+
                                              'AR_SHORT = :AR_SHORT, '+
                                              'AR_BARCODE = :AR_BARCODE, '+
                                              'AR_STD_HR = :AR_STD_HR, '+
                                              'AR_FACTOR = :AR_FACTOR, '+
                                              'TT_CODIGO = :TT_CODIGO, '+
                                              'AR_STD_CST = :AR_STD_CST, '+
                                              'AR_INGLES = :AR_INGLES '+
                                              'where ( AR_CODIGO = :AR_CODIGO )';
          Q_PARTES_IMPORT_AGREGA: Result := 'insert into PARTES( AR_CODIGO, '+
                                                                'AR_NOMBRE, '+
                                                                'AR_SHORT, '+
                                                                'AR_BARCODE, '+
                                                                'AR_STD_HR, '+
                                                                'AR_FACTOR, '+
                                                                'TT_CODIGO, '+
                                                                'AR_STD_CST, '+
                                                                'AR_INGLES ) values ( '+
                                                                ':AR_CODIGO, '+
                                                                ':AR_NOMBRE, '+
                                                                ':AR_SHORT, '+
                                                                ':AR_BARCODE, '+
                                                                ':AR_STD_HR, '+
                                                                ':AR_FACTOR, '+
                                                                ':TT_CODIGO, '+
                                                                ':AR_STD_CST, '+
                                                                ':AR_INGLES )';
          Q_TIPO_PARTES_EXISTE: Result := 'select COUNT(*) CUENTA from TPARTE where ( TT_CODIGO = :Codigo )';
          Q_TIPO_PARTES_AGREGA: Result := 'insert into TPARTE( TT_CODIGO, TT_DESCRIP ) values ( :TT_CODIGO, :TT_DESCRIP )';
          Q_TIEMPOS_LEE: Result := 'select CB_CODIGO, OP_NUMBER, WO_NUMBER, AR_CODIGO, '+
                                   'AU_FECHA, WK_TIPO, WK_STATUS, WK_TMUERTO, WK_TIEMPO, '+
                                   'WK_MOD_1, WK_MOD_2, WK_MOD_3 '+
                                   'from WORKS where '+
                                   '( CB_CODIGO = %d ) and '+
                                   '( AU_FECHA = ''%s'' ) and '+
                                   '( WK_TIPO = %d ) and '+
                                   '( WK_MANUAL = ' + ZetaCommonTools.EntreComillas( K_GLOBAL_SI ) + ' ) '+
                                   'order by WK_HORA_R, WK_FOLIO';
          Q_TIEMPOS_BORRA: Result := 'delete from WORKS where ( CB_CODIGO = %d ) and ( AU_FECHA = ''%s'' )';
          Q_TIEMPOS_INSERTA: Result := 'insert into WORKS( ' +
                                       'CB_CODIGO, '+
                                       'OP_NUMBER, '+
                                       'WO_NUMBER, '+
                                       'AR_CODIGO, '+
                                       'CB_AREA,'+
                                       'WK_FECHA_R, '+
                                       'WK_HORA_R, '+
                                       'AU_FECHA, '+
                                       'WK_HORA_A, '+
                                       'WK_LINX_ID, '+
                                       'WK_TIPO, '+
                                       'WK_MOD_1, '+
                                       'WK_MOD_2, '+
                                       'WK_MOD_3, '+
                                       'WK_TMUERTO, '+
                                       'WK_MANUAL, '+
                                       'WK_PRE_CAL '+
                                       ') values ( '+
                                       ':CB_CODIGO, '+
                                       ':OP_NUMBER, '+
                                       ':WO_NUMBER, '+
                                       ':AR_CODIGO, '+
                                       ':CB_AREA, '+
                                       ':WK_FECHA_R, '+
                                       ':WK_HORA_R, '+
                                       ':AU_FECHA, '+
                                       ':WK_HORA_A, '+
                                       ':WK_LINX_ID, '+
                                       ':WK_TIPO, '+
                                       ':WK_MOD_1, '+
                                       ':WK_MOD_2, '+
                                       ':WK_MOD_3, '+
                                       ':WK_TMUERTO, '+
                                       ':WK_MANUAL, '+
                                       ':WK_PRE_CAL )';
          Q_TIEMPOS_HAY_TARJETA: Result := 'select COUNT(*) CUANTOS from AUSENCIA where ( CB_CODIGO = %d ) and ( AU_FECHA = ''%s'' )';
          Q_COLABORA_UPDATE_AREA: Result := 'update COLABORA set CB_AREA = :Area where ( CB_CODIGO = :Empleado )';
          Q_RECALCULA_KARDEX_AREA: Result := 'execute procedure RECALCULA_AREA( :Empleado )';
          Q_CAT_AREAS_SUPERVISORES: Result := 'select B.TB_CODIGO MI_CODIGO,A.TB_CODIGO,A.TB_ELEMENT,A.TB_INGLES,' +
                                              'A.TB_NUMERO,A.TB_TEXTO,A.TB_OP_UNI,A.CB_NIVEL1,A.CB_NIVEL2,A.CB_NIVEL3,' +
                                              'A.CB_NIVEL4,A.CB_NIVEL5,A.CB_NIVEL6,A.CB_NIVEL7,A.CB_NIVEL8,A.CB_NIVEL9,' +
                                              {$ifdef ACS}'A.CB_NIVEL10,A.CB_NIVEL11,A.CB_NIVEL12,'+{$endif }                                              
                                              'A.TB_BREAK_1,A.TB_BREAK_2,A.TB_BREAK_3,A.TB_BREAK_4,A.TB_BREAK_5,' +
                                              'A.TB_BREAK_6,A.TB_BREAK_7,A.TB_OPERA,A.AR_SHIFT,A.AR_PRI_HOR ' +
                                              'from AREA A left outer join AREA B on B.TB_CODIGO = A.TB_CODIGO and ' +
                                              '( ( 0 = ( select COUNT(*) from SUP_AREA where US_CODIGO = %0:d ) ) or ' +
                                              '( B.TB_CODIGO in ( select CB_AREA from SUP_AREA where US_CODIGO = %0:d ) ) )';
          {$ifdef HYUNDAI}
          Q_EMPLEADOS_ASIGNA: Result := 'select LISTA.CB_CODIGO,LISTA.CB_NIVEL,LISTA.CB_TIPO,' + K_PRETTYNAME + ' as PRETTYNAME,''          '' as CB_AREA,0 as TIPOAREA ' +
          {$else}
          Q_EMPLEADOS_ASIGNA: Result := 'select LISTA.CB_CODIGO,LISTA.CB_NIVEL,LISTA.CB_TIPO,' + K_PRETTYNAME + ' as PRETTYNAME,''      '' as CB_AREA,0 as TIPOAREA ' +
          {$endif}
                                        'from SP_LISTA_EMPLEADOS( %s, %d ) LISTA ' +
                                        'left outer join COLABORA on ( COLABORA.CB_CODIGO = LISTA.CB_CODIGO ) %s';
          Q_EMPLEADOS_AREAS: Result := 'select LISTA.CB_CODIGO,LISTA.CB_AREA,' +  K_PRETTYNAME + ' as PRETTYNAME,' +
                                        'COLABORA.CB_FEC_NIV,COLABORA.CB_NIVEL%d ' +
                                        'from SP_LISTA_AREA( %s, %s, %d ) LISTA ' +
                                        'left outer join COLABORA on ( COLABORA.CB_CODIGO = LISTA.CB_CODIGO ) %s';
          {$ifdef INTERBASE}
          Q_EMPLEADO_NIVEL: Result := 'select RESULTADO from SP_FECHA_NIVEL( %s, :EMPLEADO )';
          Q_EMPLEADO_AREA: Result := 'select CB_AREA from SP_FECHA_AREA( :EMPLEADO, %s, %s )';
          {$endif}
          {$ifdef MSSQL}
          Q_EMPLEADO_NIVEL: Result := 'select DBO.SP_FECHA_NIVEL( %s, :EMPLEADO ) as RESULTADO';
          Q_EMPLEADO_AREA: Result := 'select DBO.SP_FECHA_AREA( :EMPLEADO, %s, %s ) as CB_AREA';
          {$endif}
          Q_EMPLEADO_AREA_KARDEX: Result := 'select CB_AREA from SP_FECHA_AREA_KARDEX( %d, %s, %s )';
          Q_INSERT_KARDEX_AREA: Result := 'insert into KAR_AREA ( CB_CODIGO,KA_FECHA,KA_HORA,CB_AREA,US_CODIGO,KA_FEC_MOV,KA_HOR_MOV ) ' +
                                          'values ( :CB_CODIGO,:KA_FECHA,:KA_HORA,:CB_AREA,:US_CODIGO,:KA_FEC_MOV,:KA_HOR_MOV )';
          Q_DELETE_KARDEX_AREA: Result := 'delete from KAR_AREA where CB_CODIGO = :CB_CODIGO and KA_FECHA = :KA_FECHA and ' +
                                          'KA_HORA = :KA_HORA';
          Q_GET_AREA_PRIMERA_HORA: Result := 'select TB_CODIGO,AR_SHIFT,AR_PRI_HOR from AREA order by TB_CODIGO';
          Q_CED_EMP_CEDULA_LEE: Result := 'select CB_CODIGO from CED_EMP where ( CE_FOLIO = %d )';
          Q_EMPLEADOS_AREA: Result := 'select LISTA.CB_CODIGO,' +  K_PRETTYNAME + ' as PRETTYNAME '+
                                      'from SP_LISTA_AREA_EMPLEADOS( %s, %s, %s ) LISTA '+
                                      'left outer join COLABORA on ( COLABORA.CB_CODIGO = LISTA.CB_CODIGO ) %s';
          Q_SUP_AREA_DELETE: Result := 'delete from SUP_AREA where ( CB_NIVEL = %s )';
          Q_TMPLABOR_BORRAR: Result := 'delete from TMPLABOR where ( US_CODIGO = %d )';
          Q_TMPLABOR_AGREGAR: Result := 'insert into TMPLABOR( US_CODIGO, CB_CODIGO, CB_CHECA ) values ( %d, :CB_CODIGO, :CB_CHECA )';
          Q_BORRA_BREAKS: Result := 'delete from WORKS where ( CB_CODIGO = :Empleado ) and ( AU_FECHA = :Fecha ) and ' +
                                    '( WK_HORA_A = :Hora ) and ( WK_FOLIO = :Folio )';
          Q_CEDULA_INSP_HISTORIAL: Result := 'select ' + K_LEE_CAMPOS_CED_INSP + ', (select SUM(DE_PIEZAS) from Defecto where Defecto.CI_FOLIO = CED_INSP.CI_FOLIO) AS NumDefectos from CED_INSP %s order by CI_FOLIO';
          Q_DEFECTOS_BORRA: Result := 'delete from DEFECTO where ( CI_FOLIO = %d )';
          Q_MAX_CEDULAINSPECCION: Result := 'select MAX(CI_FOLIO) MAXIMO from CED_INSP';
          Q_BORRA_CED_INSP: Result := 'delete from CED_INSP where ( CI_FOLIO = %d )';
          Q_DEFECTOS_INSERT: Result :=  'insert into DEFECTO '+
                                        '( CI_FOLIO, DE_FOLIO, DE_CODIGO, DE_PIEZAS, DE_COMENTA ) values '+
                                        '( :CI_FOLIO, :DE_FOLIO, :DE_CODIGO, :DE_PIEZAS, :DE_COMENTA ) ';
          Q_BREAKS_LEE: Result := 'select BH_INICIO, BH_TIEMPO from BRK_HORA where '+
                                  '( BR_CODIGO = ( select TB_BREAK_%d from AREA where ( AREA.TB_CODIGO = ''%s'' ) ) ) '+
                                  'order by BH_INICIO';
          Q_CEDULA_INSPECCION_DEPURAR: Result := 'delete from CED_INSP where ( CI_FECHA < ''%s'' )';
          Q_CEDULA_SCRAP_DEPURAR: Result := 'delete from CEDSCRAP where ( CS_FECHA < ''%s'' )';
          Q_GET_COMPONENTES: Result := 'SELECT CN_CODIGO, CN_NOMBRE, CN_COSTO, CN_UNIDAD from COMPONEN order by CN_CODIGO';
          Q_BORRA_CED_SCRAP: Result := 'delete from CEDSCRAP where ( CS_FOLIO = %d )';
          Q_CEDULA_SCRAP_HISTORIAL: Result := 'select ' + K_LEE_CAMPOS_CED_SCRAP + ', (select SUM(SC_PIEZAS) from SCRAP where SCRAP.CS_FOLIO = CEDSCRAP.CS_FOLIO) AS NumScrap from CEDSCRAP %s order by CS_FOLIO';
          Q_SCRAP_BORRA: Result:= 'delete from SCRAP where (CS_FOLIO = %d)';
          Q_SCRAP_INSERT: Result := 'insert into SCRAP '+
                                    '(  CS_FOLIO, SC_FOLIO, CN_CODIGO, SC_MOTIVO, SC_PIEZAS, SC_COMENTA ) values '+
                                    '( :CS_FOLIO,:SC_FOLIO,:CN_CODIGO,:SC_MOTIVO,:SC_PIEZAS,:SC_COMENTA ) ';
          Q_MAX_CEDULASCRAP: Result := 'select MAX(CS_FOLIO) MAXIMO from CEDSCRAP';
          Q_COMPONENTES_EXISTE: Result := 'select COUNT(*) CUENTA from COMPONEN where ( CN_CODIGO = :Codigo )';
          Q_COMPONENTES_IMPORT_MODIFICA: Result := 'update COMPONEN set ' +
                                                   'CN_NOMBRE = :CN_NOMBRE,' +
                                                   'CN_SHORT = :CN_SHORT,' +
                                                   'CN_BARCODE = :CN_BARCODE,' +
                                                   'CN_INGLES = :CN_INGLES,' +
                                                   'CN_COSTO = :CN_COSTO,' +
                                                   'CN_UNIDAD = :CN_UNIDAD,' +
                                                   'CN_DETALLE = :CN_DETALLE,' +
                                                   'CN_NUMERO1 = :CN_NUMERO1,' +
                                                   'CN_NUMERO2 = :CN_NUMERO2,' +
                                                   'CN_TEXTO1 = :CN_TEXTO1,' +
                                                   'CN_TEXTO2 = :CN_TEXTO2 ' +
                                                   'where ( CN_CODIGO = :CN_CODIGO )';
          Q_COMPONENTES_IMPORT_AGREGA: Result := 'insert into COMPONEN ( CN_CODIGO,' +
                                                                        'CN_NOMBRE,' +
                                                                        'CN_SHORT,' +
                                                                        'CN_BARCODE,' +
                                                                        'CN_INGLES,' +
                                                                        'CN_COSTO,' +
                                                                        'CN_UNIDAD,' +
                                                                        'CN_DETALLE,' +
                                                                        'CN_NUMERO1,' +
                                                                        'CN_NUMERO2,' +
                                                                        'CN_TEXTO1,' +
                                                                        'CN_TEXTO2 ) values ( ' +
                                                                        ':CN_CODIGO,' +
                                                                        ':CN_NOMBRE,' +
                                                                        ':CN_SHORT,' +
                                                                        ':CN_BARCODE,' +
                                                                        ':CN_INGLES,' +
                                                                        ':CN_COSTO,' +
                                                                        ':CN_UNIDAD,' +
                                                                        ':CN_DETALLE,' +
                                                                        ':CN_NUMERO1,' +
                                                                        ':CN_NUMERO2,' +
                                                                        ':CN_TEXTO1,' +
                                                                        ':CN_TEXTO2 )';
          Q_CEDULA_IMPORT_AGREGA: Result := 'insert into CEDULA ( CE_FOLIO,' +
                                                                  'WO_NUMBER,' +
                                                                  'AR_CODIGO,' +
                                                                  'OP_NUMBER,' +
                                                                  'CE_AREA,' +
                                                                  'CE_TIPO,' +
                                                                  'CE_FECHA,' +
                                                                  'CE_HORA,' +
                                                                  'CE_STATUS,' +
                                                                  'CE_MOD_1,' +
                                                                  'CE_MOD_2,' +
                                                                  'CE_MOD_3,' +
                                                                  'CE_TMUERTO,' +
                                                                  'CE_PIEZAS,' +
                                                                  'CE_COMENTA,' +
                                                                  'US_CODIGO,' +
                                                                  'CE_TIEMPO,' +
                                                                  'CE_MULTI'+
                                                                  {$ifdef LA_ADA}
                                                                  ',CE_CONTEO'+
                                                                  {$endif}
                                                                  ' ) values ( ' +
                                                                  ':CE_FOLIO,' +
                                                                  ':WO_NUMBER,' +
                                                                  ':AR_CODIGO,' +
                                                                  ':OP_NUMBER,' +
                                                                  ':CE_AREA,' +
                                                                  ':CE_TIPO,' +
                                                                  ':CE_FECHA,' +
                                                                  ':CE_HORA,' +
                                                                  ':CE_STATUS,' +
                                                                  ':CE_MOD_1,' +
                                                                  ':CE_MOD_2,' +
                                                                  ':CE_MOD_3,' +
                                                                  ':CE_TMUERTO,' +
                                                                  ':CE_PIEZAS,' +
                                                                  ':CE_COMENTA,' +
                                                                  ':US_CODIGO,' +
                                                                  ':CE_TIEMPO,' +
                                                                  ':CE_MULTI'+
                                                                  {$ifdef LA_ADA}
                                                                  ',:CE_CONTEO'+
                                                                  {$endif}
                                                                  ' )';
          Q_GET_LAYOUTS_SUPER: Result :='select   SA.TB_CODIGO CB_AREA, '+
                                                ' A.TB_ELEMENT,'+
                                                ' LY.LY_CODIGO,'+
                                                ' LY.LY_AREA,'+
                                                ' LY.LY_NOMBRE'+
                                                ' FROM AREA A LEFT OUTER JOIN AREA SA ON SA.TB_CODIGO = A.TB_CODIGO AND (( SA.TB_CODIGO in ( select CB_AREA from SUP_AREA where US_CODIGO = %0:d ) ) OR ( 0 = ( SELECT COUNT(*) FROM SUP_AREA WHERE US_CODIGO = %0:d ) )) '+
															                  '             LEFT OUTER JOIN LAY_PRO_CE LY ON LY.LY_AREA = A.TB_CODIGO AND (( SA.TB_CODIGO in ( select CB_AREA from SUP_AREA where US_CODIGO = %0:d ) ) OR ( 0 = ( SELECT COUNT(*) FROM SUP_AREA WHERE US_CODIGO = %0:d ) )) '+
                                                ' Where LY.LY_CODIGO != ''''';

          Q_ASIGNA_EMPLEADO: Result := 'insert into KAR_EMPSIL (  CB_CODIGO, '+
                                                                ' SL_CODIGO, '+
                                                                ' LY_CODIGO, '+
                                                                ' EM_FECHA, '+
                                                                ' EM_HORA,  '+
                                                                ' EM_HOR_MOV,'+
                                                                ' EM_FEC_MOV,'+
                                                                ' US_CODIGO  '+
                                                                ' ) values ( '+
                                                                ' :CB_CODIGO,'+
                                                                ' :SL_CODIGO,'+
                                                                ' :LY_CODIGO, '+
                                                                ' :EM_FECHA, '+
                                                                ' :EM_HORA,  '+
                                                                ' :EM_HOR_MOV,'+
                                                                ' :EM_FEC_MOV,'+
                                                                ' :US_CODIGO  )';
          {$IFDEF MSSQL}
          Q_EMPLEADOS_LAYOUT: Result := 'select DISTINCT(CB_CODIGO),SILLA=''------'',HORA=''----'' from KAR_EMPSIL where LY_CODIGO = ''%s''';

          Q_KAR_EMP_MAQ: Result := 'select SL_CODIGO,EM_FECHA,EM_HORA,US_CODIGO from dbo.SP_FECHA_SILLA_KARDEX (%d ,''%s'',''%s'',''%s'')';

          Q_GET_CERTIFIC_EMPLEADOS : Result := 'select CI_CODIGO,KI_FEC_CER,CB_CODIGO,KI_RENOVAR,KI_APROBO from dbo.sp_certific_empl (%d,''%s'')';

          {$else}
          Q_EMPLEADOS_LAYOUT: Result := 'select DISTINCT(CB_CODIGO),''------''AS SILLA,''----''AS HORA from KAR_EMPSIL where LY_CODIGO = ''%s''';

          Q_KAR_EMP_MAQ: Result := 'select SL_CODIGO,EM_FECHA,EM_HORA,US_CODIGO from SP_FECHA_SILLA_KARDEX (%d ,''%s'',''%s'',''%s'')';

          Q_GET_CERTIFIC_EMPLEADOS : Result := 'select CI_CODIGO,KI_FEC_CER,CB_CODIGO,KI_RENOVAR from KAR_CERT where KI_APROBO = ''S'' and'+
                                                ' ( KI_RENOVAR = 0 or ( KI_FEC_CER + KI_RENOVAR  >= ''%1:s'' ) and (KI_FEC_CER <= ''%1:s'' ) )and '+
                                                ' ( CB_CODIGO in ( select CB_CODIGO from SP_LISTA_EMPLEADOS( ''%1:s'',%0:d ) ) ) ';
          {$endif}

          Q_MAQUINA_USO: Result := 'select MQ_CODIGO,LY_CODIGO from LAY_MAQ where MQ_CODIGO = ''%s''';
     else
         Result := '';
     end;
end;

{ ******** TdmServerLabor ********** }

class procedure TdmServerLabor.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
     if Register then
     begin
          inherited UpdateRegistry(Register, ClassID, ProgID);
          EnableSocketTransport(ClassID);
          EnableWebTransport(ClassID);
     end
     else
     begin
          DisableSocketTransport(ClassID);
          DisableWebTransport(ClassID);
          inherited UpdateRegistry(Register, ClassID, ProgID);
     end;
end;

procedure TdmServerLabor.MtsDataModuleCreate(Sender: TObject);
begin
     oZetaCreator := Nil;
     FDataEmp     := Nil;
     FDataMulti   := Nil;
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
end;

procedure TdmServerLabor.MtsDataModuleDestroy(Sender: TObject);
begin
     if Assigned( FDataEmp ) then
        FreeAndNil( FDataEmp );
     if Assigned( FDataMulti ) then
        FreeAndNil( FDataMulti );
     ZetaSQLBroker.FreeZetaCreator( oZetaCreator );
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
end;

{$ifdef DOS_CAPAS}
procedure TdmServerLabor.CierraEmpresa;
begin
end;
{$endif}

function TdmServerLabor.GetSQLBroker: TSQLBroker;
begin
     {$ifdef DOS_CAPAS}
     Result := ZetaSQLBroker.oZetaSQLBroker;
     {$else}
     Result := Self.oZetaSQLBroker;
     {$endif}
end;

function  TdmServerLabor.GetNombreTabla( const eCatalogo: ETipoCatalogo ): String;
begin
    case eCatalogo of
        eModula1: Result := 'MODULA1';
        eModula2: Result := 'MODULA2';
        eModula3: Result := 'MODULA3';
        eTiempoMuerto: Result := 'TMUERTO';
        eTDefecto: Result := 'TDEFECTO';
        eMotivoScrap: Result := 'MOTSCRAP';
    end;
end;

procedure TdmServerLabor.ValidaFechaLabor( DeltaDS: TzProviderClientDataSet; const UpdateKind: TUpdateKind; const sCampo: string );
var
   sMensaje: string;
   lOK, lValidaFechaCorte: boolean;
   dFechaCorte: Tdate;
begin
     with oZetaProvider do
     begin
          lOK := True;
          lValidaFechaCorte := GetGlobalBooleano( K_GLOBAL_LABOR_VALIDA_FECHACORTE );
          dFechaCorte := GetGlobalDate( K_GLOBAL_LABOR_FECHACORTE );
          if ( UpdateKind = ukInsert ) then
             lOK := dLaborCalculo.ValidaCambioLabor( CampoAsVar( DeltaDS.FieldByName( sCampo ) ), dFechaCorte, lValidaFechaCorte, UpdateKind, sMensaje )
          else if ( UpdateKind in [ ukModify, ukDelete ] ) then
               begin
                    lOK := dLaborCalculo.ValidaCambioLabor( CampoOldAsVar( DeltaDS.FieldByName( sCampo ) ), dFechaCorte, lValidaFechaCorte, UpdateKind, sMensaje );
                    if lOK and CambiaCampo( DeltaDS.FieldByName( sCampo ) ) then
                       lOK := dLaborCalculo.ValidaCambioLabor( CampoAsVar( DeltaDS.FieldByName( sCampo ) ), dFechaCorte, lValidaFechaCorte, UpdateKind, sMensaje );
               end;
          if not lOK then
             DataBaseError( sMensaje );
     end;
end;

procedure TdmServerLabor.InitCreator;
begin
     if not Assigned( oZetaCreator ) then
        oZetaCreator := ZetaSQLBroker.GetZetaCreator( oZetaProvider );
end;

procedure TdmServerLabor.InitBroker;
begin
     InitCreator;
     oZetaCreator.RegistraFunciones( [efGenerales] );
     oZetaSQLBroker := ZetaSQLBroker.GetSQLBroker( oZetaCreator );
end;

procedure TdmServerLabor.ClearBroker;
begin
     ZetaSQLBroker.FreeSQLBroker( SQLBroker );
end;

procedure TdmServerLabor.InitImportLog;
begin
     with cdsLog do
     begin
          InitTempDataset;
          AddStringField( 'IMP_DATA', K_ANCHO_IMP_DATA ); { Datos }
          AddMemoField( 'IMP_ERROR', K_ANCHO_IMP_ERROR );  { Error }
          CreateTempDataset;
     end;
end;

procedure TdmServerLabor.SetDetailInfo( const eCatalogo : ETipoCatalogo );
begin
     with oZetaProvider.DetailInfo do
        case eCatalogo of
             ePartes: SetInfoDetail( 'DEFSTEPS',
                                     'OP_NUMBER,DF_STD_HR,DF_SEQUENC,AR_CODIGO',
                                     'AR_CODIGO,DF_SEQUENC',
                                     'AR_CODIGO');
             eWorder: SetInfoDetail( 'STEPS',
                                     'WO_NUMBER,ST_SEQUENC,OP_NUMBER,ST_STD_HR,ST_REAL_HR,ST_QTY',
                                     'WO_NUMBER,ST_SEQUENC',
                                     'WO_NUMBER');
             eWOrderWorks: SetInfoDetail( 'WORKS',
                                          'CB_CODIGO,AU_FECHA,OP_NUMBER,WO_NUMBER,AR_CODIGO,WK_HORA_A,WK_FECHA_R,WK_HORA_R,' +
                                          'WK_LINX_ID,WK_TIEMPO,WK_STATUS,WK_MOD_1,WK_MOD_2,WK_MOD_3,WK_TMUERTO,WK_HRS_ORD,WK_HRS_2EX,WK_HRS_3EX,WK_PRE_CAL,WK_MANUAL,CB_AREA,CB_PUESTO',
                                          'WO_NUMBER,CB_CODIGO,AU_FECHA,WK_HORA_A,WK_PIEZAS',
                                          'WO_NUMBER' );
             eBreaks: SetInfoDetail( 'BRK_HORA',
                                     'BR_CODIGO,BH_INICIO,BH_TIEMPO,BH_MOTIVO',
                                     'BR_CODIGO,BH_INICIO',
                                     'BR_CODIGO' );
             eCedulasInsp: SetInfoDetail( 'DEFECTO',
                                          'CI_FOLIO,DE_FOLIO,DE_CODIGO,DE_PIEZAS,DE_COMENTA',
                                          'CI_FOLIO,DE_FOLIO',
                                          'CI_FOLIO' );
        end;
end;

procedure TdmServerLabor.SetTablaInfo( const eCatalogo: ETipoCatalogo );
begin
     with oZetaProvider.TablaInfo do
     begin
          case eCatalogo of
              eModula1, eModula2, eModula3, eTiempoMuerto, eTDefecto, eMotivoScrap:
              begin
                   SetInfo( GetNombreTabla( eCatalogo ),
                            'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO',
                            'TB_CODIGO' );
              end;
              eTParte       : SetInfo( 'TPARTE',  'TT_CODIGO,TT_DESCRIP,TT_INGLES,TT_NUMERO,TT_TEXTO', 'TT_CODIGO' );
              eTOpera       : SetInfo( 'TOPERA',  'TO_CODIGO,TO_DESCRIP,TO_INGLES,TO_NUMERO,TO_TEXTO', 'TO_CODIGO' );
              ePartes       : SetInfo( 'PARTES',  'AR_CODIGO,AR_NOMBRE,AR_SHORT,AR_BARCODE,AR_STD_HR,AR_FACTOR,TT_CODIGO,AR_STD_CST', 'AR_CODIGO' );
              eOpera        : SetInfo( 'OPERA',   'OP_NUMBER,OP_NOMBRE,OP_DESCRIP,OP_STD_HR,OP_FACTOR,TO_CODIGO,OP_STD_CST,' +
                                                  'CB_NIVEL1,CB_NIVEL2,CB_NIVEL3,CB_NIVEL4,CB_NIVEL5,CB_NIVEL6,CB_NIVEL7,CB_NIVEL8,CB_NIVEL9'{$ifdef ACS}+',CB_NIVEL10,CB_NIVEL11,CB_NIVEL12'{$endif},
                                                  'OP_NUMBER' );
              eOrdenesFijas : SetInfo( 'WOFIJA',  'CB_CODIGO,WF_FEC_INI,WO_NUMBER,AR_CODIGO,OP_NUMBER', 'CB_CODIGO,WF_FEC_INI' );
              eWorks        : SetInfo( 'WORKS',   'CB_CODIGO,AU_FECHA,OP_NUMBER,WO_NUMBER,AR_CODIGO,WK_HORA_A,WK_FECHA_R,WK_HORA_R,'+
                                                  'WK_LINX_ID,WK_TIEMPO,WK_STATUS,WK_MOD_1,WK_MOD_2,WK_MOD_3,WK_TMUERTO,WK_HRS_ORD,'+
                                                  'WK_HRS_2EX,WK_HRS_3EX,WK_PRE_CAL,WK_MANUAL,CB_AREA,CB_PUESTO,WK_FOLIO,WK_TIPO,WK_PIEZAS,WK_CEDULA',
                                                  'CB_CODIGO,AU_FECHA,WK_HORA_A' );
              eLecturas     : SetInfo( 'LECTURAS','CB_CODIGO,LX_FECHA,LX_HORA,LX_WORDER,LX_OPERA,LX_PIEZAS,LX_STATUS,'+
                                                  'LX_MODULA1,LX_MODULA2,LX_MODULA3,LX_TMUERTO,LX_NUMERO,LX_LINX_ID,LX_FOLIO',
                                                  'CB_CODIGO,LX_FECHA,LX_HORA,LX_WORDER,LX_OPERA' );
              eDefSteps     : SetInfo( 'DEFSTEPS','OP_NUMBER,DF_STD_HR,DF_SEQUENC,AR_CODIGO','DF_SEQUENC,AR_CODIGO');
              eWOrder       : SetInfo( 'WORDER',  'WO_NUMBER,AR_CODIGO,WO_DESCRIP,WO_QTY,WO_FINISHD,WO_FEC_INI,WO_FEC_FIN,WO_FEC_CIE,WO_STATUS,WO_LST_STP,WO_TEXTO,WO_NUM_GEN','WO_NUMBER');
              eWOrderWorks  : SetInfo( 'WORDER',  'WO_NUMBER,AR_CODIGO,WO_DESCRIP,WO_QTY,WO_FINISHD,WO_FEC_INI,WO_FEC_FIN,WO_FEC_CIE,WO_STATUS,WO_LST_STP,WO_TEXTO,WO_NUM_GEN','WO_NUMBER');
              eArea         : SetInfo( 'AREA',    'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_OP_UNI,CB_NIVEL1,CB_NIVEL2,CB_NIVEL3,CB_NIVEL4,CB_NIVEL5,CB_NIVEL6,CB_NIVEL7,' +
                                                  'CB_NIVEL8,CB_NIVEL9,'+
                                                  {$ifdef ACS}'CB_NIVEL10,CB_NIVEL11,CB_NIVEL12,'+{$endif}
                                                  'TB_BREAK_1,TB_BREAK_2,TB_BREAK_3,TB_BREAK_4,TB_BREAK_5,TB_BREAK_6,TB_BREAK_7,TB_OPERA,AR_SHIFT,AR_PRI_HOR,AR_TIPO', 'TB_CODIGO' );
              eSteps        : SetInfo( 'STEPS',   'WO_NUMBER,ST_SEQUENC,OP_NUMBER,ST_STD_HR,ST_REAL_HR,ST_QTY','WO_NUMBER,ST_SEQUENC');
              eAusencia     : SetInfo( 'AUSENCIA','CB_CODIGO, AU_FECHA, AU_TIPO, AU_TIPODIA, HO_CODIGO, AU_STATUS',
                                                  'CB_CODIGO, AU_FECHA' );
              eCedulas      : SetInfo( 'CEDULA',  'CE_FOLIO,WO_NUMBER,AR_CODIGO,OP_NUMBER,CE_AREA,CE_TIPO,CE_FECHA,CE_HORA,CE_STATUS,CE_MOD_1,CE_MOD_2,CE_MOD_3,CE_TMUERTO,CE_PIEZAS,CE_COMENTA,US_CODIGO,CE_TIEMPO,CE_MULTI'{$ifdef LA_ADA}+',CE_CONTEO'{$endif},'CE_FOLIO' );
              eCedEmp       : SetInfo( 'CED_EMP', 'CE_FOLIO,CB_CODIGO,CE_POSICIO,CB_PUESTO','CE_FOLIO,CB_CODIGO' );
              eCedMulti     : SetInfo( 'CED_WORD','CE_FOLIO,WO_NUMBER,CW_POSICIO,CW_PIEZAS','CE_FOLIO,WO_NUMBER' );
              eBreaks       : SetInfo( 'BREAKS',  'BR_CODIGO,BR_NOMBRE,BR_INGLES,BR_NUMERO,BR_TEXTO', 'BR_CODIGO' );
              eSupArea      : SetInfo( 'SUP_AREA','CB_NIVEL,CB_AREA','CB_NIVEL,CB_AREA' );
              eKardexArea   : SetInfo( 'KAR_AREA','CB_CODIGO,KA_FECHA,KA_HORA,CB_AREA,US_CODIGO,KA_FEC_MOV,KA_HOR_MOV','CB_CODIGO,KA_FECHA,KA_HORA' );
              eCedulasInsp  : SetInfo( 'CED_INSP','CI_FOLIO,CI_AREA,CE_FOLIO,WO_NUMBER,AR_CODIGO,CI_FECHA,CI_HORA,US_CODIGO,CI_TIPO,CI_RESULT,CI_COMENTA,CI_OBSERVA,CI_TAMLOTE,CI_MUESTRA,CI_TIEMPO,CI_NUMERO1,CI_NUMERO2','CI_FOLIO' );
              eDefecto      : SetInfo( 'DEFECTO', 'CI_FOLIO,DE_FOLIO,DE_CODIGO,DE_PIEZAS,DE_COMENTA', 'CI_FOLIO,DE_FOLIO' );
              eComponentes  : SetInfo( 'COMPONEN','CN_CODIGO,CN_NOMBRE,CN_SHORT,CN_BARCODE,CN_INGLES,CN_COSTO,CN_UNIDAD,CN_DETALLE,CN_NUMERO1,CN_NUMERO2,CN_TEXTO1,CN_TEXTO2' ,'CN_CODIGO');
              eCedulasScrap : SetInfo( 'CEDSCRAP','CS_FOLIO,CS_AREA,CS_FECHA,CS_HORA,WO_NUMBER,AR_CODIGO,OP_NUMBER,CS_FEC_FAB,CS_TAMLOTE,CS_COMENTA,CS_OBSERVA,CS_NUMERO1,CS_NUMERO2,CS_TEXTO1,CS_TEXTO2,US_CODIGO', 'CS_FOLIO' );
              eScrap        : SetInfo( 'SCRAP','CS_FOLIO,SC_FOLIO,CN_CODIGO,SC_MOTIVO,SC_PIEZAS,SC_COMENTA', 'CS_FOLIO, SC_FOLIO');
              eTMaquinas    : SetInfo( 'TMAQUINA','TB_CODIGO,TB_ELEMENT,TB_NUMERO,TB_TEXTO,TB_INGLES', 'TB_CODIGO');
              eMaquinas     : SetInfo( 'MAQUINA','MQ_CODIGO,MQ_NOMBRE,MQ_INGLES,MQ_NUMERO,MQ_TEXTO,MQ_RESUMEN,MQ_IMAGEN,MQ_TMAQUIN,MQ_MULTIP', 'MQ_CODIGO');
              eMaqCert      : SetInfo( 'MAQ_CERT','MQ_CODIGO,CI_CODIGO,MC_FECHA,US_CODIGO', 'CI_CODIGO,MQ_CODIGO');
              eMaqArea      : SetInfo( 'MAQ_AREA','MQ_CODIGO,AR_CODIGO,MA_FECHA,US_CODIGO', 'AR_CODIGO,MQ_CODIGO,MA_FECHA');
              eLayoutProd   : SetInfo( 'LAY_PRO_CE','LY_CODIGO,LY_NOMBRE,LY_INGLES,LY_NUMERO,LY_TEXTO,LY_AREA,LY_FECHA,US_CODIGO','LY_CODIGO');
              eLayMap       : SetInfo( 'LAY_MAQ','LY_CODIGO,MQ_CODIGO,LM_POS_X,LM_POS_Y','LY_CODIGO,MQ_CODIGO' );
              eKarEmpMaq    : SetInfo( 'KAR_EMPSIL','CB_CODIGO,SL_CODIGO,LY_CODIGO,EM_FECHA,EM_HORA,EM_HOR_MOV,EM_FEC_MOV,US_CODIGO','CB_CODIGO,MQ_CODIGO,EM_FECHA,EM_HORA,LY_CODIGO');
              eSillasLay    : SetInfo( 'SILLA_LAY','SL_CODIGO,LY_CODIGO,SL_POS_X,SL_POS_Y','SL_CODIGO,LY_CODIGO');
              eSillasMaq    : SetInfo( 'SILLA_MAQ','SL_CODIGO,LY_CODIGO,MQ_CODIGO','SL_CODIGO,LY_CODIGO,MQ_CODIGO');
          end;
     end;
end;

function TdmServerLabor.FiltroNivel0( const Empresa : OleVariant ): String;
begin
     Result := ZetaServerTools.GetFiltroNivel0( Empresa );
     if strLleno( Result ) then
        Result := 'where ' + Result;
end;

function TdmServerLabor.GetCatalogo( Empresa: OleVariant; Catalogo: Integer): OleVariant;
begin
     SetTablaInfo( eTipoCatalogo( Catalogo ) );
     Result := oZetaProvider.GetTabla( Empresa );
     SetComplete;
end;

function TdmServerLabor.GrabaCatalogo( Empresa: OleVariant; Catalogo: Integer; oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( eTipoCatalogo( Catalogo ) );
     with oZetaProvider do
     begin
          if ( eTipoCatalogo(catalogo) = eMaquinas )then
          begin
               EmpresaActiva := Empresa;
               TablaInfo.BeforeUpdateRecord := BeforeUpdateMaquinas;
          end;
          Result := oZetaProvider.GrabaTabla( Empresa, oDelta, ErrorCount );
     end;
     SetComplete;
end;

function TdmServerLabor.ExisteCodigoImportar( const sCodigo: String; Dataset: TZetaCursor ): Boolean;
begin
     oZetaProvider.ParamAsString( Dataset, 'Codigo', sCodigo );
     with Dataset do
     begin
          Active := True;
          Result := ( FieldByName( 'CUENTA' ).AsInteger > 0 );
          Active := False;
     end;
end;

procedure TdmServerLabor.ImportarCatalogosParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := 'Archivo: ' + ParamByName( 'Archivo' ).AsString +
                              ZetaCommonClasses.K_PIPE + 'Formato Del Archivo: ' + ObtieneElemento( lfFormatoASCII, ParamByName( 'Formato' ).AsInteger );
     end;
end;

function TdmServerLabor.GetOrdenesFijas(Empresa: OleVariant; Empleado: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Result := OpenSQL( Empresa, GetSQLScript( Q_ORDENES_FIJAS ), True );
     end;
     SetComplete;
end;

function TdmServerLabor.GetLecturasEmpleado(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime): OleVariant;
begin
     SetTablaInfo( eLecturas );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( '( CB_CODIGO = %d ) and ( LX_FECHA = ''%s'' )', [ Empleado, DateToStrSQL( Fecha ) ] );
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerLabor.GetWorksEmpleado(Empresa: OleVariant; var Ausencia: OleVariant; Empleado: Integer; Fecha: TDateTime): OleVariant;
var
   sFiltro: String;
begin
     sFiltro := Format( '( CB_CODIGO = %d ) and ( AU_FECHA = ''%s'' )', [ Empleado, DateToStrSQL( Fecha ) ] );
     SetTablaInfo( eWorks );
     with oZetaProvider do
     begin
          TablaInfo.Filtro :=  sFiltro;
          Result := GetTabla( Empresa );
          SetTablaInfo( eAusencia );
          TablaInfo.Filtro := sFiltro;
          Ausencia := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerLabor.GetCatalogoDetail(Empresa: OleVariant; Catalogo: Integer): OleVariant;
var
   eTipo: eTipoCatalogo;
begin
     eTipo := eTipoCatalogo( Catalogo );
     SetTablaInfo( eTipo );
     SetDetailInfo( eTipo );
     Result := oZetaProvider.GetMasterDetail( Empresa );
     SetComplete;
end;

function TdmServerLabor.GrabaCatalogoDetail(Empresa: OleVariant; Catalogo: Integer; oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
var
   eTipo: eTipoCatalogo;
begin
     eTipo := eTipoCatalogo( Catalogo );
     oZetaProvider.EmpresaActiva := Empresa;
     SetTablaInfo( eTipo );
     SetDetailInfo( eTipo );
     Result := oZetaProvider.GrabaMasterDetail( Empresa, oDelta, ErrorCount );
     SetComplete;
end;

function TdmServerLabor.GetDatosEmpleado(Empresa: OleVariant; Empleado: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Result := OpenSQL( Empresa, Format( GetSQLScript( Q_EMPLEADO ), [ Empleado ] ), True );
     end;
     SetComplete;
end;

function TdmServerLabor.GetLecturas(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime): OleVariant;
var
   sFecha: String;
begin
     sFecha := ZetaCommonTools.DateToStrSQL( Fecha );
     SetTablaInfo( eLecturas );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( '( CB_CODIGO = %d ) and ( LX_FECHA = ''%s'' )', [ Empleado, sFecha ] );
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerLabor.GetWOrder(Empresa, Parametros: OleVariant; Tipo: Integer): OleVariant;
var
   sFiltro: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          SetTablaInfo( eWOrder );
     end;
     case eTipoWorder( Tipo ) of
          ewHistorial: sFiltro := GetFiltrosWOrder;
          ewLookup: sFiltro := GetFiltrosWOrder;
     else
         sFiltro := '';
     end;
     with oZetaProvider do
     begin
          TablaInfo.Filtro := sFiltro;
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerLabor.GetFiltrosWOrder: String;
var
   sInicial, sFinal,sTipos: String;
begin
     with oZetaProvider.ParamList do
     begin
          with ParamByName( 'Tipo' ) do
          begin
               if ( AsInteger = -1 ) then
                  sTipos := '0,1,2,3'
               else if AsInteger = 0 then
                       sTipos := '1,2,3'
               else if AsInteger = 1 then
                       sTipos := '0';
               Result := Format( '( WO_STATUS in ( %s ) )', [ sTipos ] );
          end;

          if ( ParamByName( 'FechaInicial' ).AsDate <> NullDateTime ) and ( ParamByName( 'FechaFinal' ).AsDate <> NullDateTime ) then
          begin
               Result := ConcatFiltros( Result, Format( '( WO_FEC_INI >= %s ) and ( WO_FEC_INI <= %s )',
                                                      [ DateToStrSQLC( ParamByName( 'FechaInicial' ).AsDate ),
                                                        DateToStrSQLC( ParamByName( 'FechaFinal' ).AsDate ) ] ) );
          end;
          with ParamByName( 'Parte' ) do
          begin
               if ( AsString > '' ) then
               begin
                    Result := ConcatFiltros( Result, Format( '( AR_CODIGO = ''%s'' )', [ AsString ] ) );
               end;
          end;
          { Def. 1488: La busqueda de ordenes no trabaja bien cuando se busca con espacios o saltos de linea}
          sInicial := ParamByName( 'OrderInicial' ).AsString ;
          if ( sInicial > '' ) then
             sInicial := ZetaCommonTools.EntreComillas( sInicial );
          sFinal := ParamByName( 'OrderFinal' ).AsString ;
          if ( sFinal > '' ) then
             sFinal := ZetaCommonTools.EntreComillas( sFinal );
          Result := ConcatFiltros( Result, ZetaCommonTools.GetFilterRango( 'WO_NUMBER', 'WO_NUMBER', sInicial, sFinal ) );
     end;
end;

function TdmServerLabor.GetWOrderSteps(Empresa: OleVariant; var Steps: OleVariant; Parametros: OleVariant): OleVariant;
var
   sFiltro: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          with ParamList do
          begin
               sFiltro := Format( '( WO_NUMBER = ''%s'' )', [ ParamByName( 'WO_NUMBER' ).AsString ] );
          end;
          SetTablaInfo( eWOrder );
          TablaInfo.Filtro := sFiltro;
          Result := GetTabla( Empresa );
          SetTablaInfo( eSteps );
          TablaInfo.Filtro := sFiltro;
          Steps := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerLabor.GetKardexWorder(Empresa: OleVariant; var WorksHijo: OleVariant; Parametros: OleVariant): OleVariant;
var
   sWoNumber: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          with ParamList do
          begin
               sWoNumber := ParamByName( 'WO_NUMBER' ).AsString;
          end;
          SetTablaInfo( eWOrderWorks );
          TablaInfo.Filtro := Format( '( WO_NUMBER = ''%s'' )', [ sWoNumber ] );
          Result := GetTabla( Empresa );
          WorksHijo := OpenSQL( Empresa, Format( GetSQLScript( Q_WORDER_KARDEX ), [ sWoNumber ] ), True );
     end;
     SetComplete;
end;

function TdmServerLabor.GrabaWorderSteps(Empresa, Delta, Steps: OleVariant; const WOrderNumber: WideString; out ErrorCount: Integer): OleVariant;
var
   sInsert: String;
   i, iLow, iHigh: Integer;
begin
     SetOLEVariantToNull( Result );
     FWoNumber := WOrderNumber;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          try
             EmpiezaTransaccion;
             ExecSQL( Empresa, Format( GetSQLScript( Q_STEPS_BORRA ), [ FWoNumber ] ) );
             TerminaTransaccion(True);
          except
                TerminaTransaccion(False);
          end;
          if not VarIsNull( Delta ) then
          begin
               SetTablaInfo( eWorder );
               Result := GrabaTabla( Empresa, Delta, ErrorCount );
          end;
          if ( ErrorCount = 0 ) and not VarIsNull( Steps ) then { GA:� XQ no se usa un solo Query Parametrizado ? }
          begin
               iLow := VarArrayLowBound( Steps,1 );
               iHigh := VarArrayHighBound( Steps, 1 );
               if ( iLow + iHigh > 0 ) then
               begin
                    sInsert := GetSQLScript( Q_STEPS_AGREGA );
                    try
                       EmpiezaTransaccion;
                       for i := iLow to iHigh do
                       begin
                            ExecSQL( Empresa, Format( sInsert, [ FWoNumber,
                                                                 i,
                                                                 Steps[ i ][ 1 ],
                                                                 FloatToStr( Steps[ i ][ 2 ] ),
                                                                 FloatToStr( Steps[ i ][ 3 ] ),
                                                                 IntToStr( Steps[ i ][ 4 ] ) ] ) );
                       end;
                       TerminaTransaccion(True);
                    except
                          TerminaTransaccion(False);
                    end;
               end;
          end;
     end;
     SetComplete;
end;

function TdmServerLabor.BorraWOrder(Empresa: OleVariant; const WOrderNumber: WideString; out ErrorCount: Integer): OleVariant;
begin
     Result := NULL;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          EmpiezaTransaccion;
          try
             ExecSQL( Empresa, Format( GetSQLScript( Q_WORDER_BORRA ), [ WOrderNumber ] ) );
             TerminaTransaccion(True);
          except
                TerminaTransaccion(False);
                raise;
          end;
     end;
     SetComplete;
end;

function TdmServerLabor.BorraParte(Empresa: OleVariant; const Parte: WideString; out ErrorCount: Integer): OleVariant;
begin
     Result := NULL;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          try
             EmpiezaTransaccion;
             ExecSQL( Empresa, Format( GetSQLScript( Q_PARTE_BORRA ), [ Parte ] ) );
             TerminaTransaccion(True);
          except
                TerminaTransaccion(False);
          end;
     end;
     SetComplete;
end;

function TdmServerLabor.GrabaPartes(Empresa, Delta, DefSteps: OleVariant; const Parte: WideString; out ErrorCount: Integer): OleVariant;
var
   sInsert: String;
   i, iLow, iHigh: Integer;
begin
     SetOLEVariantToNull( Result );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          try
             EmpiezaTransaccion;
             ExecSQL( Empresa, Format( GetSQLScript( Q_DEFSTEPS_BORRA ), [ Parte ] ) );
             TerminaTransaccion(True);
          except
                TerminaTransaccion(False);
          end;
          if not VarIsNull( Delta ) then
          begin
               SetTablaInfo(ePartes);
               Result := GrabaTabla( Empresa, Delta, ErrorCount );
          end;
          if ( ErrorCount = 0 ) and not VarIsNull( DefSteps ) then { GA:� XQ no se usa un solo Query Parametrizado ? }
          begin
               iLow := VarArrayLowBound( DefSteps, 1 );
               iHigh := VarArrayHighBound( DefSteps, 1 );
               if ( iLow + iHigh > 0 ) then
               begin
                    sInsert := GetSQLScript( Q_DEFSTEPS_AGREGA );
                    try
                       EmpiezaTransaccion;
                       for i := iLow to iHigh do
                       begin
                            ExecSQL( Empresa, Format( sInsert,
                                                      [ Parte,
                                                        i,
                                                        DefSteps[ i ][ 1 ],
                                                        FloatToStr( DefSteps[ i ][ 2 ] ) ] ) );
                       end;
                       TerminaTransaccion(True);
                    except
                          TerminaTransaccion(False);
                    end;
               end;
          end;
     end;
     SetComplete;
end;

function TdmServerLabor.GetPartesSteps(Empresa: OleVariant; var DefSteps: OleVariant; const Parte: WideString): OleVariant;
var
   sFiltro: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          with ParamList do
          begin
               sFiltro := Format( '( AR_CODIGO = ''%s'' )', [ Parte ] );
          end;
          SetTablaInfo( ePartes );
          TablaInfo.Filtro := sFiltro;
          Result := GetTabla( Empresa );

          SetTablaInfo( eDefSteps );
          TablaInfo.Filtro := sFiltro;
          DefSteps := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerLabor.GrabaGridLabor(Empresa: OleVariant; const Area: WideString; oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( eWorks );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          with TablaInfo do
          begin
               OnUpdateError := OnErrorGridLabor;
          end;
          Result := GrabaTablaGrid( Empresa, oDelta, ErrorCount );
     end;
     SetComplete;
end;

procedure TdmServerLabor.OnErrorGridLabor(Sender: TObject; DataSet: TzProviderClientDataSet; E: EUpdateError; UpdateKind: TUpdateKind; var Response: TResolverResponse);
const
     K_MENSAJE = '%sHora: %s, Tipo: %s%s';
var
   iEmpleado, iTipo: Integer;
   sError, sDuracion: String;
begin
     if ( UpdateKind = ukInsert ) and PK_Violation( E ) then
        sError:= 'Registro ya Existe! '
     else
        sError := VACIO;
     with DataSet do
     begin
          iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
          //FListaGridLabor := ConcatString( FListaGridLabor, IntToStr( iEmpleado ), ',' );

          sDuracion := VACIO;
          iTipo := FieldByName( 'WK_TIPO' ).AsInteger;
          if ( eTipoLectura( iTipo ) in [ wtLibre, wtPrecalculada ] ) then
             sDuracion := sDuracion + ', Duraci�n : ' + FormatFloat( '0.00', FieldByName( 'WK_TIEMPO' ).AsFloat );
          oZetaProvider.EscribeBitacora( tbError,
                                         clbLabor,
                                         iEmpleado,
                                         FieldByName( 'AU_FECHA' ).AsDateTime,
                                         Format( K_MENSAJE, [ sError, FormateaHora( FieldByName( 'WK_HORA_R' ).AsString ),
                                         ObtieneElemento( lfTipoLectura, iTipo ), sDuracion ] ),
                                         E.Message );
     end;
     Response := rrSkip;
end;

function TdmServerLabor.GrabaSalidaWorks(Empresa, OtrosDatos, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
end;

procedure TdmServerLabor.BeforeUpdateWorks(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
     ValidaFechaLabor( DeltaDS, UpdateKind, 'AU_FECHA' );
end;

function TdmServerLabor.GrabaWorks(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     try
        SetTablaInfo( eWorks );
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             InitGlobales;
             with TablaInfo do
             begin
                  BeforeUpdateRecord := BeforeUpdateWorks;
             end;
             Result := oZetaProvider.GrabaTabla( Empresa, oDelta, ErrorCount );
        end;
     except
           on Error: Exception do
           begin
                DataBaseError( 'Error al Grabar Tabla de Works:' + Error.Message );
           end;
     end;
     SetComplete;
end;

function TdmServerLabor.GetCedulas(Empresa: OleVariant; Fecha: TDateTime): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Result := OpenSQL( Empresa, Format( GetSQLScript( Q_CEDULA_LEER ), [ UsuarioActivo, ZetaCommonTools.DateToStrSQL( Fecha ) ] ), True );
     end;
     SetComplete;
end;

function TdmServerLabor.GetCedEmpleados(Empresa: OleVariant; Folio: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          Result := OpenSQL( Empresa, Format( GetSQLScript( Q_CEDULA_EMPLEADOS ), [ Folio ] ), True );
     end;
     SetComplete;
end;

function TdmServerLabor.GetCedMultiLote(Empresa: OleVariant; Folio: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          Result := OpenSQL( Empresa, Format( GetSQLScript( Q_CEDULA_MULTILOTE ), [ Folio ] ), True );
     end;
     SetComplete;
end;

function TdmServerLabor.ValidaHoraCedula(const sArea: TCodigo; const dFecha: TDate; const sFinal: String; const iDuracion: Integer;var sMensaje: String ): Boolean;
var
   FQuery: TZetaCursor;
   iEmpieza, iTermina, iInicial, iFinal: Integer;
begin
     Result := False;
     iTermina := ZetaCommonTools.aMinutos( sFinal );                { Hora Final de la C�dula }
     iEmpieza := ZetaCommonTools.iMax( 0, ( iTermina - iDuracion ) ); { Hora Inicial de la C�dula }
     with oZetaProvider do
     begin
          FQuery := CreateQuery( Format( GetSQLScript( Q_BREAKS_LEE ), [ SysUtils.DayOfWeek( dFecha ), sArea ] ) );
          try
             with FQuery do
             begin
                  Active := True;
                  while not Eof do
                  begin
                       iInicial := ZetaCommonTools.aMinutos( FieldByName( 'BH_INICIO' ).AsString ); { Hora Inicial del Break }
                       iFinal := iInicial + FieldByName( 'BH_TIEMPO' ).AsInteger;                   { Hora Final del Break }
                       if (( iInicial <= iEmpieza ) and ( iEmpieza <= iFinal )) or     { La C�dula Empieza durante el Break }
                          (( iInicial <= iTermina ) and ( iTermina <= iFinal )) or     { La C�dula Termina durante el Break }
                          (( iEmpieza <= iInicial ) and ( iFinal <= iTermina ))then { El Break Ocurre Durante La C�dula  }
                       begin
                            sMensaje := 'Esta C�dula Abarca Un Break';
                            Result := True;
                            Break;
                       end;
                       Next;
                  end;
                  Active := False;
             end;
          finally
                 FreeAndNil( FQuery );
          end;
     end;
end;

procedure TdmServerLabor.BeforeUpdateCedulas(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
var
   sMensaje: String;

   function GetLastCedula: Integer;
   var
      FGeneral: TZetaCursor;
   begin
        with oZetaProvider do
        begin
             FGeneral := CreateQuery( GetSQLScript( Q_CEDULA_ULTIMA ) );
             try
                with FGeneral do
                begin
                     Active := True;
                     if IsEmpty then
                        Result := 0
                     else
                         Result := FieldByName( 'RESULTADO' ).AsInteger;
                     Active := False;
                end;
             finally
                    FreeAndNil( FGeneral );
             end;
        end;
   end;

begin
     ValidaFechaLabor( DeltaDS, UpdateKind, 'CE_FECHA' );
     if ( UpdateKind = ukInsert ) then
     begin
          with DeltaDS do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'CE_FOLIO' ).AsInteger := GetLastCedula + 1;
          end;
     end;
     if ( UpdateKind = ukModify ) then
        Applied := zStrToBool( CampoAsVar( DeltaDS.FieldByName( 'SETCAMBIOS' ) ) )
     else
     begin
          Applied := False;
          // Validar si No Existe Conflicto con breaks
          with DeltaDs do
          begin
               if ( FOperacion <> ocIgnorar ) and ( UpdateKind in [ ukInsert, ukModify ] ) then
               begin
                    if ValidaHoraCedula( CampoAsVar( FieldByName( 'CE_AREA' ) ), CampoAsVar( FieldByName( 'CE_FECHA' ) ),
                                         CampoAsVar( FieldByName( 'CE_HORA' ) ), CampoAsVar( FieldByName( 'CE_TIEMPO' ) ), sMensaje  ) then
                       DataBaseError( sMensaje );
               end;
          end;
     end;
end;

procedure TdmServerLabor.EscribeBitacoraCedula( UpdateKind: TUpdateKind; DeltaDS: TzProviderClientDataSet );
const
     K_MENSAJE = '%s C�dula - Hora: %s, Tipo:%s%s';
var
   sAccion, sDuracion, sNota: String;
   iTipo: Integer;
begin
     sDuracion := VACIO;
     sNota := VACIO;
     case UpdateKind of
          ukModify: sAccion := 'Modific�';
          ukDelete: sAccion := 'Borr�';
     else
         sAccion := 'Agreg�';
     end;
     if Not ( FListaGridLabor <> VACIO ) then
        FListaGridLabor := 'Se Obtienen En Base Al Kardex De Areas';
     with DeltaDS do
     begin
          iTipo := CampoAsVar( FieldByName( 'CE_TIPO' ) );
          if ( eTipoCedula( iTipo ) in [ tcEspeciales, tcTMuerto ] ) then
             sNota := sNota +  'Duraci�n : ' + FormatFloat( '0', CampoAsVar( FieldByName( 'CE_TIEMPO' ) ) ) + ' minutos' + CR_LF;
          sNota := sNota + 'Area : ' + CampoAsVar( FieldByName( 'CE_AREA' ) ) + CR_LF + 'Empleados : ' + FListaGridLabor;
          oZetaProvider.EscribeBitacora( tbNormal,
                                         clbLabor,
                                         0,
                                         CampoAsVar( FieldByName( 'CE_FECHA' ) ),
                                         Format( K_MENSAJE, [ sAccion, FormateaHora( CampoAsVar( FieldByName( 'CE_HORA' ) ) ),
                                         ObtieneElemento( lfTipoCedula, iTipo ), sDuracion ] ),
                                         sNota );
     end;
end;

procedure TdmServerLabor.AfterUpdateCedulas(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
var
   iFolio, iEmpleado, iPosicion: Integer;
   sArea : String;
   FDataSet: TZetaCursor;

   procedure InsertaCedEmp;
   begin
        with oZetaProvider do
        begin
             ParamAsInteger( FDataSet, 'CE_FOLIO', iFolio );
             ParamAsInteger( FDataSet, 'CE_POSICIO', iPosicion );
             with FDataEmp do
             begin
                  ParamAsInteger( FDataSet, 'CB_CODIGO', FieldByName( 'CB_CODIGO' ).AsInteger );
                  ParamAsChar( FDataSet, 'CB_PUESTO', FieldByName( 'CB_PUESTO' ).AsString, K_ANCHO_PUESTO );
             end;
             Ejecuta( FDataSet );
        end;
   end;

   procedure InsertaCedMulti;
   begin
        with oZetaProvider do
        begin
             ParamAsInteger( FDataSet, 'CE_FOLIO', iFolio );
             ParamAsInteger( FDataSet, 'CW_POSICIO', iPosicion );
             with FDataMulti do
             begin
                  ParamAsVarChar( FDataSet, 'WO_NUMBER', FieldByName( 'WO_NUMBER' ).AsString, K_ANCHO_ORDEN );
                  ParamAsInteger( FDataSet, 'CW_PIEZAS', FieldByName( 'CW_PIEZAS' ).AsInteger );
             end;
             Ejecuta( FDataSet );
        end;
   end;

begin
     with DeltaDS do
     begin
          iFolio := CampoAsVar( FieldByName( 'CE_FOLIO' ) );
          sArea  := CampoAsVar( FieldByName( 'CE_AREA' ) );
     end;
     with oZetaProvider do
     begin
          EmpiezaTransaccion;
          try
             // Agrega Lista de Empleados
             EjecutaAndFree( Format( GetSQLScript( Q_TABLA_BORRAR ), [ 'CED_EMP', iFolio ] ) );
             FDataSet := CreateQuery( GetSQLScript( Q_CEDULA_EMPLEADOS_AGREGA ) );
             try
                iPosicion := 0;
                with FDataEmp do
                begin
                     First;
                     while not EOF do
                     begin
                          iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                          FListaGridLabor := ConcatString( FListaGridLabor, IntToStr( iEmpleado ), ',' );
                          Inc( iPosicion );
                          InsertaCedEmp;
                          Next;
                     end;
                end;
             finally
                 FreeAndNil( FDataset );
             end;
             // Agrega Lista de MultiLotes
             EjecutaAndFree( Format( GetSQLScript( Q_TABLA_BORRAR ), [ 'CED_WORD', iFolio ] ) );
             FDataSet := CreateQuery( GetSQLScript( Q_CEDULA_WORDER_AGREGA ) );
             try
                iPosicion := 0;
                with FDataMulti do
                begin
                     First;
                     while not EOF do
                     begin
                          Inc( iPosicion );
                          InsertaCedMulti;
                          Next;
                     end;
                end;
             finally
                FreeAndNil( FDataset );
             end;
             TerminaTransaccion( TRUE );
          except
                on Error: Exception do
                begin
                     RollBackTransaccion;
                     DataBaseError( 'Error al Grabar C�dula:' + Error.Message );
                end;
          end;
          
          EscribeBitacoraCedula( UpdateKind, DeltaDS );
     end;
end;

function TdmServerLabor.GrabaCedula(Empresa, oDelta, oDataEmp, oDataMulti: OleVariant; Parametros: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     if ( FDataEmp = nil ) then
     begin
          FDataEmp := TClientDataSet.Create( Self );
          FDataMulti := TClientDataSet.Create( Self );
     end;
     FListaGridLabor := VACIO;
     SetTablaInfo( eCedulas );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
          FDataEmp.Data := oDataEmp;
          FDataMulti.Data := oDataMulti;
          AsignaParamList( Parametros );          
          FOperacion := eOperacionConflicto( ParamList.ParamByName( 'Operacion' ).AsInteger );
          with TablaInfo do
          begin
               BeforeUpdateRecord := BeforeUpdateCedulas;
               AfterUpdateRecord := AfterUpdateCedulas;
          end;
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
     end;
     SetComplete;
end;

procedure TdmServerLabor.BeforeUpdateBorrarCedulas(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
var
   iFolio: Integer;
   FEmpleados: TZetaCursor;
begin
     ValidaFechaLabor( DeltaDS, UpdateKind, 'CE_FECHA' );
     iFolio := CampoAsVar( DeltaDS.FieldByName( 'CE_FOLIO' ) );
     with oZetaProvider do
     begin
          FEmpleados:= CreateQuery;
          try
             AbreQueryScript( FEmpleados, Format( GetSQLScript( Q_CED_EMP_CEDULA_LEE ), [ iFolio ] ) );
             with FEmpleados do
             begin
                  while not EOF do
                  begin
                       FListaGridLabor := ConcatString( FListaGridLabor, IntToStr( FieldByName( 'CB_CODIGO' ).AsInteger ), ',' );
                       Next;
                  end;
             end;
          finally
                 FreeAndNil( FEmpleados );
          end;
     end;
end;

procedure TdmServerLabor.AfterUpdateBorrarCedulas(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind );
begin
     try
        EscribeBitacoraCedula( UpdateKind, DeltaDS );
     except
           on Error: Exception do
           begin
                DataBaseError( 'Error al Escribir En Bit�cora:' + Error.Message );
           end;
     end;
end;

function TdmServerLabor.BorraCedula(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     FListaGridLabor := VACIO;
     SetTablaInfo( eCedulas );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
          with TablaInfo do
          begin
               AfterUpdateRecord := AfterUpdateBorrarCedulas;
               BeforeUpdateRecord := BeforeUpdateBorrarCedulas;
          end;
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
     end;
     SetComplete;
end;

function TdmServerLabor.GetFiltroCedula( const sFiltro, sLookUp, sFormatFiltro: String ): String;
begin
     Result := sFiltro;
     with oZetaProvider.ParamList do
     begin
          with ParamByName( sLookup ) do
          begin
               if ( AsString > '' ) then
                  Result := ConcatFiltros( Result, Format( sFormatFiltro, [ AsString ] ) );
          end;
     end;
end;

function TdmServerLabor.GetHistorialCedulas(Empresa, Parametros: OleVariant): OleVariant;
var
   sFiltro: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          {
          SetTablaInfo( eCedulas );
          }
     end;

     with oZetaProvider.ParamList do
     begin
          with ParamByName( 'Status' ) do
          begin
               if ( AsInteger >= 0 ) then
                  sFiltro := Format( '( CE_STATUS = %d )', [ AsInteger ] );
          end;
          with ParamByName( 'Tipo' ) do
          begin
               if ( AsInteger >= 0 ) then
                  sFiltro := ConcatFiltros( sFiltro, Format( '( CE_TIPO = %d )', [ AsInteger ] ) );
          end;
          with ParamByName( 'Usuario' ) do
          begin
               if ( AsInteger > 0 ) then
                  sFiltro := ConcatFiltros( sFiltro, Format( '( US_CODIGO = %d )', [ AsInteger ] ) );
          end;
          if ( ParamByName( 'FechaInicial' ).AsDate <> NullDateTime) and ( ParamByName( 'FechaFinal' ).AsDate <> NullDateTime) then
          begin
               sFiltro := ConcatFiltros( sFiltro, Format( '( CE_FECHA >= %s ) and ( CE_FECHA <= %s )',
                                                      [ DateToStrSQLC( ParamByName( 'FechaInicial' ).AsDate ),
                                                        DateToStrSQLC( ParamByName( 'FechaFinal' ).AsDate ) ] ) );
          end;
          sFiltro := GetFiltroCedula( sFiltro, 'Parte', '( AR_CODIGO = ''%s'' )' );
          sFiltro := GetFiltroCedula( sFiltro, 'Operacion', '( OP_NUMBER = ''%s'' )' );
          sFiltro := GetFiltroCedula( sFiltro, 'Orden', '( WO_NUMBER = ''%s'' )' );
          sFiltro := GetFiltroCedula( sFiltro, 'Area', '( CE_AREA = ''%s'' )' );
          sFiltro := GetFiltroCedula( sFiltro, 'Cedula', '( CE_FOLIO = %s )' );
     end;

     if strLleno( sFiltro ) then
        sFiltro := 'where ' + sFiltro;
     Result := oZetaProvider.OpenSQL( Empresa, Format( GetSQLScript( Q_CEDULA_HISTORIAL ), [ sFiltro ] ), TRUE );

     {
     with oZetaProvider do
     begin
          TablaInfo.Filtro := sFiltro;
          Result := GetTabla( Empresa );
     end;
     }
     SetComplete;
end;

function TdmServerLabor.GetBreaks(Empresa: OleVariant): OleVariant;
begin
     SetTablaInfo( eBreaks );
     SetDetailInfo( eBreaks );
     Result := oZetaProvider.GetMasterDetail( Empresa );
     SetComplete;
end;

function TdmServerLabor.GrabaBreaks(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( eBreaks );
     SetDetailInfo( eBreaks );
     Result := oZetaProvider.GrabaMasterDetail( Empresa, oDelta, ErrorCount );
     SetComplete;
end;

{ **************** PROCESOS *************************** }

procedure TdmServerLabor.DepuracionParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Borrar Datos Anteriores a: ' + FechaCorta( ParamByName( 'Fecha' ).AsDateTime ) +
                              ZetaCommonClasses.K_PIPE + 'Del Kardex De Operaciones: ' + ZetaCommonTools.zBoolToStr( ParamByName( 'BorrarKardex' ).AsBoolean ) +
                              ZetaCommonClasses.K_PIPE + 'De Las C�dulas de Producci�n: ' + ZetaCommonTools.zBoolToStr( ParamByName( 'BorrarCedulas' ).AsBoolean ) +
                              ZetaCommonClasses.K_PIPE + 'De Las C�dulas de Inspecci�n: ' + ZetaCommonTools.zBoolToStr( ParamByName( 'BorrarCedulasInspeccion' ).AsBoolean ) +
                              ZetaCommonClasses.K_PIPE + 'De Las C�dulas de Scrap: ' + ZetaCommonTools.zBoolToStr( ParamByName( 'BorrarCedulasScrap' ).AsBoolean );
     end;
end;

function TdmServerLabor.Depuracion( Empresa, Parametros: OleVariant ): OleVariant;
var
   sFecha: String;

 procedure BorraRegistro( const sParametro: string; const iScript: integer );
 begin
      with oZetaProvider do
           if ParamList.ParamByName( sParametro ).AsBoolean then
           begin
                EmpiezaTransaccion;
                try
                   ExecSQL( Empresa, Format( GetSQLScript( iScript ), [ sFecha ] ) );
                   TerminaTransaccion(TRUE);
                except
                      TerminaTransaccion(FALSE);
                end;
           end;
 end;

begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          DepuracionParametros;
          if OpenProcess( prLabDepuracion, 0, FListaParametros ) then
          begin
               sFecha := DateToStrSQL( ParamList.ParamByName( 'Fecha' ).AsDate );
               BorraRegistro( 'BorrarCedulas', Q_CEDULA_DEPURAR );
               BorraRegistro( 'BorrarKardex', Q_WORKS_DEPURAR );
               BorraRegistro( 'BorrarLecturas', Q_LECTURAS_DEPURAR );
               BorraRegistro( 'BorrarCedulasInspeccion', Q_CEDULA_INSPECCION_DEPURAR );
               BorraRegistro( 'BorrarCedulasScrap', Q_CEDULA_SCRAP_DEPURAR );

                    {if ParamByName( 'BorrarCedulas' ).AsBoolean then
                    begin
                         EmpiezaTransaccion;
                         try
                            ExecSQL( Empresa, Format( GetSQLScript( Q_CEDULA_DEPURAR ), [ sFecha ] ) );
                            TerminaTransaccion(TRUE);
                         except
                            TerminaTransaccion(FALSE);
                         end;
                    end;
                    if ParamByName( 'BorrarKardex' ).AsBoolean then
                    begin
                         EmpiezaTransaccion;
                         try
                            ExecSQL( Empresa, Format( GetSQLScript( Q_WORKS_DEPURAR ), [ sFecha ] ) );
                            TerminaTransaccion(TRUE);
                         except
                            TerminaTransaccion(FALSE);
                         end;
                    end;
                    if ParamByName( 'BorrarLecturas' ).AsBoolean then
                    begin
                         EmpiezaTransaccion;
                         try
                            ExecSQL( Empresa, Format( GetSQLScript( Q_LECTURAS_DEPURAR ), [ sFecha ] ) );
                            TerminaTransaccion(True);
                         except
                            TerminaTransaccion(False);
                         end;
                    end;
                    if ParamByName( 'BorrarCedulasInspeccion' ).AsBoolean then
                    begin
                         EmpiezaTransaccion;
                         try
                            ExecSQL( Empresa, Format( GetSQLScript( Q_CEDULA_INSPECCION_DEPURAR ), [ sFecha ] ) );
                            TerminaTransaccion(TRUE);
                         except
                            TerminaTransaccion(FALSE);
                         end;
                    end;}
               Result := CloseProcess;
          end;
     end;
     SetComplete;
end;

procedure TdmServerLabor.ImportarOrdenesValidaASCII(DataSet: TDataset; var iProblemas: Integer; var sErrorMsg: String );

    procedure Error( const sMensaje: String );
    begin
         Inc( iProblemas );
         sErrorMsg := sErrorMsg + '| ' + sMensaje + ' |';
    end;

begin
     with Dataset do
     begin
          if ( FieldByName( 'WO_QTY' ).AsInteger < 1 ) then
             Error( 'Cantidad Por Terminar Inv�lida' );
          if ( FieldByName( 'WO_FINISHD' ).AsFloat < 0 ) then
             Error( 'Cantidad Terminada Inv�lida' );
          if ( FieldByName( 'WO_FEC_FIN' ).AsDateTime < FieldByName( 'WO_FEC_INI' ).AsDateTime ) then
             Error( 'Fecha De Promesa Menor Que Fecha de Inicio' );
          with FieldByName( 'WO_STATUS' ) do
          begin
               if ( AsInteger < 0 ) or ( AsInteger > Ord( High( eWorkStatus ) ) ) then
                  Error( 'Status de Orden Inv�lido' );
          end;
     end;
end;

function TdmServerLabor.ImportarOrdenes(Empresa, Parametros, ListaASCII: OleVariant): OleVariant;
{
Nombre              Campo             Tipo     Ancho Formato              Requerido
================    ================= =====    ===== ==================== =====================
Orden de Trabajo    WORDER.WO_NUMBER  Texto    20    X(20)                Obligatorio
# de Parte          WORDER.AR_CODIGO  Texto    15    X(15)                Obligatorio
Cantidad            WORDER.WO_QTY     Flotante 15    9999999999.9999      Opcional, default 1
Cantidad Terminada  WORDER.WO_FINISHD Flotante 15    9999999999.9999      Opcional, default 0
Fecha Inicio        WORDER.WO_FEC_INI Fecha    8     DD/MM/YY             Opcional, default Hoy
Fecha Fin           WORDER.WO_FEC_FIN Fecha    8     DD/MM/YY             Opcional, default Hoy
Fecha Cierre        WORDER.WO_FEC_CIE Fecha    8     DD/MM/YY             Opcional, default Hoy
Status              WORDER.WO_STATUS  Entero   1     9                    Opcional, default 0
Ultimo Paso         WORDER.WO_LST_STP Entero   3     999                  Opcional, default 0
N�mero General      WORDER.WO_NUM_GEN Flotante 15    9999999999.9999      Opcional, default 0
Texto General       WORDER.WO_TEXTO   Texto    40    X(40)                Opcional, default ''
Descripci�n         WORDER.WO_DESCRIP Texto    255   X(255)               Opcional, default ''
}
const
     K_POST_ERROR = 'Orden %s';
     K_DATE_FORMAT = 'DD/MM/YY';
     K_ANCHO_FECHA = 8;
var
   sParte, sDatos, sOrden: String;
   lError: Boolean;
   iCuantos: Integer;
   eFormato: eFormatoASCII;
   FInserta, FExiste: TZetaCursor;
begin
     InitImportLog;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          with ParamList do
          begin
               eFormato := eFormatoASCII( ParamByName( 'Formato' ).AsInteger );
          end;
          ImportarCatalogosParametros;
          lError := False;
          iCuantos := 0;
          oSuperASCII := TdmSuperASCII.Create( Self );
          try
             try
                with oSuperASCII do
                begin
                     OnValidar := ImportarOrdenesValidaASCII;
                     Formato := eFormato;
                     AgregaColumna( 'WO_NUMBER', tgTexto, K_ANCHO_ORDEN, True );
                     {$ifdef HYUNDAI}
                     AgregaColumna( 'AR_CODIGO', tgTexto, K_ANCHO_PARTE18, True );
                     {$else}
                            {$ifdef INTERRUPTORES}
                            AgregaColumna( 'AR_CODIGO', tgTexto, K_ANCHO_PARTE20, True );
                            {$else}
                            AgregaColumna( 'AR_CODIGO', tgTexto, K_ANCHO_PARTE, True );
                            {$endif}
                     {$endif}
                     AgregaColumna( 'WO_QTY', tgFloat, K_ANCHO_FLOAT, False, 0, True, '1' );
                     AgregaColumna( 'WO_FINISHD', tgFloat, K_ANCHO_FLOAT, False, 0, True, '0' );
                     AgregaColumna( 'WO_FEC_INI', tgFecha, K_ANCHO_FECHA, False, 0, True, FormatDateTime( K_DATE_FORMAT, Date ) );
                     AgregaColumna( 'WO_FEC_FIN', tgFecha, K_ANCHO_FECHA, False, 0, True, FormatDateTime( K_DATE_FORMAT, Date ) );
                     AgregaColumna( 'WO_FEC_CIE', tgFecha, K_ANCHO_FECHA, False, 0, True, FormatDateTime( K_DATE_FORMAT, Date ) );
                     AgregaColumna( 'WO_STATUS', tgNumero, 1, False, 0, True, Format( '%d', [ Ord( wsEnProceso ) ] ) );
                     AgregaColumna( 'WO_LST_STP', tgNumero, 3, False, 0, True, '0' );
                     AgregaColumna( 'WO_NUM_GEN', tgFloat, K_ANCHO_FLOAT, False, 0, True, '0' );
                     AgregaColumna( 'WO_TEXTO', tgTexto, K_ANCHO_TEXTO, False );
                     AgregaColumna( 'WO_DESCRIP', tgTexto, K_ANCHO_DESCRIPCION, False );
                     with cdsLista do
                     begin
                          Lista := Procesa( ListaASCII );
                          iCuantos := RecordCount - Errores;
                     end;
                end;
             except
                   on Error: Exception do
                   begin
                        lError := True;
                        sDatos := Error.Message;
                   end;
             end;
          finally
                 FreeAndNil(oSuperASCII);
          end;
          if OpenProcess( prLabImportarOrdenes, iCuantos, FListaParametros ) then
          begin
               if lError then
                  Log.Error( 0, 'Error Al Leer Archivo De Importaci�n', sDatos )
               else
               begin
                    EmpiezaTransaccion;
                    try
                       try
                          { Borra Ordenes Existentes }
                          ExecSQL( Empresa, GetSQLScript( Q_WORDER_IMPORT_BORRA ) );
                          FExiste := CreateQuery( GetSQLScript( Q_PARTES_EXISTE ) );
                          try
                             FInserta := CreateQuery( GetSQLScript( Q_WORDER_IMPORT_AGREGA ) );
                             try
                                with cdsLista do
                                begin
                                     First;
                                     while not Eof do
                                     begin
                                          sDatos := FieldByName( 'RENGLON' ).AsString;
                                          if ( FieldByName( 'NUM_ERROR' ).AsInteger = 0 ) then
                                          begin
                                               { Se convierten a May�sculas xq la interfase de usuario no trabaja con min�sculas }
                                               sOrden := AnsiUpperCase( FieldByName( 'WO_NUMBER' ).AsString );
                                               { Se convierten a May�sculas xq la interfase de usuario no trabaja con min�sculas }
                                               sParte := AnsiUpperCase( FieldByName( 'AR_CODIGO' ).AsString );
{
                                               ParamAsString( FExiste, 'Codigo', sParte );
                                               with FExiste do
                                               begin
                                                    Active := True;
                                                    lExiste := ( Fields[ 0 ].AsInteger > 0 );
                                                    Active := False;
                                               end;
}
                                               if ExisteCodigoImportar( sParte, FExiste ) then
                                               begin
                                                    try
                                                       ParamAsVarChar( FInserta, 'WO_NUMBER', sOrden, K_ANCHO_ORDEN );
                                                       {$ifdef HYUNDAI}
                                                       ParamAsVarChar( FInserta, 'AR_CODIGO', sParte, K_ANCHO_PARTE18 );
                                                       {$else}
                                                              {$ifdef INTERRUPTORES}
                                                              ParamAsVarChar( FInserta, 'AR_CODIGO', sParte, K_ANCHO_PARTE20 );
                                                              {$else}
                                                              ParamAsVarChar( FInserta, 'AR_CODIGO', sParte, K_ANCHO_PARTE );
                                                              {$endif}
                                                       {$endif}
                                                       ParamAsFloat( FInserta, 'WO_QTY', FieldByName( 'WO_QTY' ).AsFloat );
                                                       ParamAsFloat( FInserta, 'WO_FINISHD', FieldByName( 'WO_FINISHD' ).AsFloat );
                                                       ParamAsDate( FInserta, 'WO_FEC_INI', FieldByName( 'WO_FEC_INI' ).AsDateTime );
                                                       ParamAsDate( FInserta, 'WO_FEC_FIN', FieldByName( 'WO_FEC_FIN' ).AsDateTime );
                                                       ParamAsDate( FInserta, 'WO_FEC_CIE', FieldByName( 'WO_FEC_CIE' ).AsDateTime );
                                                       ParamAsInteger( FInserta, 'WO_STATUS', FieldByName( 'WO_STATUS' ).AsInteger );
                                                       ParamAsInteger( FInserta, 'WO_LST_STP', FieldByName( 'WO_LST_STP' ).AsInteger );
                                                       ParamAsFloat( FInserta, 'WO_NUM_GEN', FieldByName( 'WO_NUM_GEN' ).AsFloat );
                                                       ParamAsVarChar( FInserta, 'WO_TEXTO', FieldByName( 'WO_TEXTO' ).AsString, K_ANCHO_TEXTO );
                                                       ParamAsVarChar( FInserta, 'WO_DESCRIP', FieldByName( 'WO_DESCRIP' ).AsString, K_ANCHO_DESCRIPCION );
                                                       Ejecuta( FInserta );
                                                    except
                                                          on Error: Exception do
                                                          begin
                                                               Log.Excepcion( 0, Format( K_POST_ERROR, [ sOrden ] ), Error );
                                                          end;
                                                    end;
                                               end
                                               else
                                               begin
                                                    Log.Error( 0, Format( 'Orden %s: Art�culo %s No Existe', [ sOrden, sParte ] ), Format( 'Art�culo %s No Existe', [ sParte ] ) + CR_LF + sDatos );
                                               end;
                                          end
                                          else
                                          begin
                                               Log.Error( 0, 'Registro Rechazado', FieldByName( 'DESC_ERROR' ).AsString + CR_LF + sDatos );
                                          end;
                                          Next;
                                     end;
                                end;
                             finally
                                    FreeAndNil( FInserta );
                             end;
                          finally
                                 FreeAndNil( FExiste );
                          end;
                       except
                             on Error: Exception do
                             begin
                                  Log.Excepcion( 0, 'Error Al Importar Datos', Error );
                             end;
                       end;
                    finally
                           TerminaTransaccion( True );
                    end;
               end;
          end;
          Result := CloseProcess;
     end;
     SetComplete;
end;

function TdmServerLabor.ImportarPartes(Empresa, Parametros, ListaASCII: OleVariant): OleVariant;
{
Nombre              Campo             Tipo     Ancho Formato              Requerido
================    ================= =====    ===== ==================== =====================
# de Parte          PARTES.AR_CODIGO  Texto    15    X(15)                Obligatorio
Nombre de la Parte  PARTES.AR_NOMBRE  Texto    40    X(40)                Obligatorio
Nombre en Ingl�s    PARTES.AR_INGLES  Texto    40    X(40)                Opcional, default ''
Nombre Corto        PARTES.AR_SHORT   Texto    20    X(20)                Opcional, default ''
C�digo de Barras    PARTES.AR_BARCODE Texto    20    X(20)                Opcional, default ''
Tipo de Parte       PARTES.TT_CODIGO  Texto    6     X(6)                 Opcional, default ''
Tiempo Est�ndard    PARTES.AR_STD_HR  Flotante 15    9999999999.9999      Opcional, default 0
Factor              PARTES.AR_FACTOR  Flotante 15    9999999999.9999      Opcional, default 0
Costo Est�ndard     PARTES.AR_STD_CST Flotante 15    9999999999.9999      Opcional, default 0
}
const
     K_POST_ERROR = 'Parte %s';
     K_DATE_FORMAT = 'DD/MM/YY';
var
   lError: Boolean;
   iCuantos: Integer;
   sParte, sTipo, sDatos: String;
   eFormato: eFormatoASCII;
   FModifica, FInserta, FExiste, FTipoExiste, FTipoAgrega, FDataset: TZetaCursor;

{
function Existe( const sCodigo: String; Dataset: TZetaCursor ): Boolean;
begin
     with oZetaProvider do
     begin
          ParamAsString( Dataset, 'Codigo', sCodigo );
     end;
     with Dataset do
     begin
          Active := True;
          Result := ( Fields[ 0 ].AsInteger > 0 );
          Active := False;
     end;
end;
}

begin
     InitImportLog;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          with ParamList do
          begin
               eFormato := eFormatoASCII( ParamByName( 'Formato' ).AsInteger );
          end;
          ImportarCatalogosParametros;
          lError := False;
          iCuantos := 0;
          oSuperASCII := TdmSuperASCII.Create( Self );
          try
             try
                with oSuperASCII do
                begin
                     Formato := eFormato;
                     {$ifdef HYUNDAI}
                     AgregaColumna( 'AR_CODIGO', tgTexto, K_ANCHO_PARTE18, True );
                     {$else}
                            {$ifdef INTERRUPTORES}
                            AgregaColumna( 'AR_CODIGO', tgTexto, K_ANCHO_PARTE20, True );
                            {$else}
                            AgregaColumna( 'AR_CODIGO', tgTexto, K_ANCHO_PARTE, True );
                            {$endif}
                     {$endif}
                     AgregaColumna( 'AR_NOMBRE', tgTexto, K_ANCHO_TEXTO, True );
                     AgregaColumna( 'AR_INGLES', tgTexto, K_ANCHO_INGLES, False, 0, True, '' );
                     AgregaColumna( 'AR_SHORT', tgTexto, K_ANCHO_TEXTITO, False, 0, True, '' );
                     AgregaColumna( 'AR_BARCODE', tgTexto, K_ANCHO_TEXTITO, False, 0, True, '' );
                     AgregaColumna( 'TT_CODIGO', tgTexto, K_ANCHO_TIPO_PARTE, False, 0, True, '' );
                     AgregaColumna( 'AR_STD_HR', tgFloat, K_ANCHO_FLOAT, False, 0, True, '0' );
                     AgregaColumna( 'AR_FACTOR', tgFloat, K_ANCHO_FLOAT, False, 0, True, '0' );
                     AgregaColumna( 'AR_STD_CST', tgFloat, K_ANCHO_FLOAT, False, 0, True, '0' );
                     with cdsLista do
                     begin
                          Lista := Procesa( ListaASCII );
                          iCuantos := RecordCount - Errores;
                     end;
                end;
             except
                   on Error: Exception do
                   begin
                        lError := True;
                        sDatos := Error.Message;
                   end;
             end;
          finally
                 FreeAndNil(oSuperASCII);
          end;
          if OpenProcess( prLabImportarPartes, iCuantos, FListaParametros ) then
          begin
               if lError then
                  Log.Error( 0, 'Error Al Leer Archivo De Importaci�n', sDatos )
               else
               begin
                    EmpiezaTransaccion;
                    try
                       try
                          FExiste     := CreateQuery( GetSQLScript( Q_PARTES_EXISTE ) );
                          FInserta    := CreateQuery( GetSQLScript( Q_PARTES_IMPORT_AGREGA ) );
                          FModifica   := CreateQuery( GetSQLScript( Q_PARTES_IMPORT_MODIFICA ) );
                          FTipoExiste := CreateQuery( GetSQLScript( Q_TIPO_PARTES_EXISTE ) );
                          FTipoAgrega := CreateQuery( GetSQLScript( Q_TIPO_PARTES_AGREGA ) );
                          try
                               with cdsLista do
                               begin
                                    First;
                                    while not Eof do
                                    begin
                                         sDatos := FieldByName( 'RENGLON' ).AsString;
                                         if ( FieldByName( 'NUM_ERROR' ).AsInteger = 0 ) then
                                         begin
                                              { Se convierten a May�sculas xq la interfase de usuario no trabaja con min�sculas }
                                              sParte := AnsiUpperCase( FieldByName( 'AR_CODIGO' ).AsString );
                                              { Se convierten a May�sculas xq la interfase de usuario no trabaja con min�sculas }
                                              sTipo := AnsiUpperCase( FieldByName( 'TT_CODIGO' ).AsString );
                                              try
                                                 if not ExisteCodigoImportar( sTipo, FTipoExiste ) then
                                                 begin
                                                      ParamAsChar( FTipoAgrega, 'TT_CODIGO', sTipo, K_ANCHO_TIPO_PARTE );
                                                      ParamAsVarChar( FTipoAgrega, 'TT_DESCRIP', Format( 'IMP: Tipo %s', [ sTipo ] ), K_ANCHO_ELEMENT );
                                                      Ejecuta( FTipoAgrega );
                                                 end;
                                                 if ExisteCodigoImportar( sParte, FExiste ) then
                                                    FDataset := FModifica
                                                 else
                                                     FDataset := FInserta;
                                                 {$ifdef HYUNDAI}
                                                 ParamAsVarChar( FDataset, 'AR_CODIGO', sParte, K_ANCHO_PARTE18 );
                                                 {$else}
                                                 		{$ifdef INTERRUPTORES}
                                                        ParamAsVarChar( FDataset, 'AR_CODIGO', sParte, K_ANCHO_PARTE20 );
                                                        {$else}
                                                        ParamAsVarChar( FDataset, 'AR_CODIGO', sParte, K_ANCHO_PARTE );
                                                        {$endif}
                                                 {$endif}
                                                 ParamAsVarChar( FDataset, 'AR_NOMBRE', FieldByName( 'AR_NOMBRE' ).AsString, K_ANCHO_TEXTO );
                                                 ParamAsVarChar( FDataset, 'AR_INGLES', FieldByName( 'AR_INGLES' ).AsString, K_ANCHO_INGLES );
                                                 ParamAsVarChar( FDataset, 'AR_SHORT', FieldByName( 'AR_SHORT' ).AsString, K_ANCHO_TEXTITO );
                                                 ParamAsVarChar( FDataset, 'AR_BARCODE', FieldByName( 'AR_BARCODE' ).AsString, K_ANCHO_TEXTITO );
                                                 ParamAsChar( FDataset, 'TT_CODIGO', sTipo, K_ANCHO_TIPO_PARTE );
                                                 ParamAsFloat( FDataset, 'AR_STD_HR', FieldByName( 'AR_STD_HR' ).AsFloat );
                                                 ParamAsFloat( FDataset, 'AR_STD_CST', FieldByName( 'AR_STD_CST' ).AsFloat );
                                                 ParamAsFloat( FDataset, 'AR_FACTOR', FieldByName( 'AR_FACTOR' ).AsFloat );
                                                 Ejecuta( FDataset );
                                              except
                                                    on Error: Exception do
                                                    begin
                                                         Log.Excepcion( 0, Format( K_POST_ERROR, [ sParte ] ), Error );
                                                    end;
                                              end;
                                         end
                                         else
                                         begin
                                              Log.Error( 0, 'Registro Rechazado', FieldByName( 'DESC_ERROR' ).AsString + CR_LF + sDatos );
                                         end;
                                         Next;
                                    end;
                               end;
                          finally
                                 FreeAndNil( FTipoAgrega );
                                 FreeAndNil( FTipoExiste );
                                 FreeAndNil( FModifica );
                                 FreeAndNil( FInserta );
                                 FreeAndNil( FExiste );
                          end;
                       except
                             on Error: Exception do
                             begin
                                  Log.Excepcion( 0, 'Error Al Importar Datos', Error );
                             end;
                       end;
                    finally
                           TerminaTransaccion( True );
                    end;
               end;
          end;
          Result := CloseProcess;
     end;
     SetComplete;
end;

function TdmServerLabor.GetTiempos(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Result := OpenSQL( Empresa, Format( GetSQLScript( Q_TIEMPOS_LEE ), [ Empleado, ZetaCommonTools.DateToStrSQL( Fecha ) , Ord( wtChecada ) ] ), True );
     end;
     SetComplete;
end;

procedure TdmServerLabor.GrabaTiempos(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime; Datos: OleVariant);
var
   oLabor: TLCalculo;
   FDatosHorario: TDatosHorarioDiario;
   FDataset: TZetaCursor;
   sFecha: String;
   sHora: THoraLabor;
   iHora, iTiempo: Integer;
   lSinTarjeta: Boolean;
   TipoChecada: eTipoLectura;
begin
     sFecha := ZetaCommonTools.DateToStrSQL( Fecha );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitCreator;
          InitGlobales;
          oLabor := TLCalculo.Create( oZetaCreator );
          try
             with oLabor do
             begin
                  CalcularTiemposBegin( Fecha, Fecha, cdsLista, FALSE );
                  try
                     FDatosHorario := GetDatosHorario( Empleado, Fecha );
                     with FDatosHorario do
                     begin
                          iHora := Entra24;
                     end;
                     FDataset := CreateQuery( Format( GetSQLScript( Q_TIEMPOS_HAY_TARJETA ), [ Empleado, sFecha ] ) );
                     try
                        with FDataset do
                        begin
                             Active := True;
                             lSinTarjeta := ( FieldByName( 'CUANTOS' ).AsInteger = 0 );
                             Active := False;
                        end;
                     finally
                            FreeAndNil( FDataset );
                     end;
                     if lSinTarjeta then { Hay que agregarle la tarjeta de Ausencia }
                     begin
                          InsertaTarjeta( Empleado, Fecha );
                     end;
                     FDataset := CreateQuery( GetSQLScript( Q_TIEMPOS_INSERTA ) );
                     try
                        EmpiezaTransaccion;
                        try
                           EjecutaAndFree( Format( GetSQLScript( Q_TIEMPOS_BORRA ), [ Empleado, sFecha ] ) );
                           with cdsLista do
                           begin
                                Lista := Datos;
                                while not Eof do
                                begin
                                     iTiempo := Trunc( FieldByName( 'WK_TIEMPO' ).AsInteger );
                                     iHora := iHora + iTiempo;
                                     sHora := DLaborCalculo.Minutes2Hour( iHora );
                                     if ( FindField( 'WK_TIPO' ) <> nil ) then
                                         TipoChecada := eTipoLectura( FieldByName( 'WK_TIPO' ).AsInteger )
                                     else
                                         TipoChecada := wtChecada;
                                     // Asignar Parametros del Query
                                     ParamAsInteger( FDataset, 'CB_CODIGO', Empleado );
                                     ParamAsDate( FDataset, 'AU_FECHA', Fecha );
                                     ParamAsChar( FDataset, 'WK_HORA_A', sHora, K_ANCHO_HH_MM );
                                     ParamAsDate( FDataset, 'WK_FECHA_R', Fecha );
                                     ParamAsChar( FDataset, 'WK_HORA_R', sHora, K_ANCHO_HH_MM );
                                     ParamAsVarChar( FDataset, 'OP_NUMBER', FieldByName( 'OP_NUMBER' ).AsString, K_ANCHO_OPERACION );
                                     ParamAsVarChar( FDataset, 'WO_NUMBER', FieldByName( 'WO_NUMBER' ).AsString, K_ANCHO_ORDEN );
                                     {$ifdef HYUNDAI}
                                     ParamAsVarChar( FDataset, 'AR_CODIGO', FieldByName( 'AR_CODIGO' ).AsString, K_ANCHO_PARTE18 );
                                     {$else}
                                            {$ifdef INTERRUPTORES}
                                            ParamAsVarChar( FDataset, 'AR_CODIGO', FieldByName( 'AR_CODIGO' ).AsString, K_ANCHO_PARTE20 );
                                            {$else}
                                            ParamAsVarChar( FDataset, 'AR_CODIGO', FieldByName( 'AR_CODIGO' ).AsString, K_ANCHO_PARTE );
                                            {$endif}
                                     {$endif}
                                     ParamAsVarChar( FDataset, 'WK_LINX_ID', 'OPERADOR', K_ANCHO_TERMINAL_ID );
                                     // Campo CB_AREA puede o no venir en cdsLista
                                     if ( FindField( 'CB_AREA' ) <> nil ) then
                                        {$ifdef HYUNDAI}
                                        ParamAsChar( FDataset, 'CB_AREA', FieldByName( 'CB_AREA' ).AsString, K_ANCHO_AREA10 )
                                        {$else}
                                        ParamAsChar( FDataset, 'CB_AREA', FieldByName( 'CB_AREA' ).AsString, K_ANCHO_AREA )
                                        {$endif}
                                     else
                                        {$ifdef HYUNDAI}
                                        ParamAsChar( FDataset, 'CB_AREA', ZetaCommonClasses.VACIO, K_ANCHO_AREA10 );
                                        {$else}
                                        ParamAsChar( FDataset, 'CB_AREA', ZetaCommonClasses.VACIO, K_ANCHO_AREA );
                                        {$endif}
                                     ParamAsInteger( FDataset, 'WK_TIPO', Ord( TipoChecada ) );
                                     ParamAsChar( FDataset, 'WK_MOD_1', FieldByName( 'WK_MOD_1' ).AsString, K_ANCHO_MODULADOR );
                                     ParamAsChar( FDataset, 'WK_MOD_2', FieldByName( 'WK_MOD_2' ).AsString, K_ANCHO_MODULADOR );
                                     ParamAsChar( FDataset, 'WK_MOD_3', FieldByName( 'WK_MOD_3' ).AsString, K_ANCHO_MODULADOR );
                                     ParamAsChar( FDataset, 'WK_TMUERTO', FieldByName( 'WK_TMUERTO' ).AsString, K_ANCHO_TMUERTO );
                                     ParamAsBoolean( FDataset, 'WK_MANUAL', True );
                                     if ( TipoChecada <> wtChecada ) then
                                        ParamAsInteger( FDataset, 'WK_PRE_CAL', iTiempo )
                                     else
                                        ParamAsInteger( FDataset, 'WK_PRE_CAL', 0 );
                                     Ejecuta( FDataset );
                                     Next;
                                end;
                           end;
                           CalcularTiemposEmpleado( Fecha, Empleado );
                           TerminaTransaccion( True );
                        except
                              on Error: Exception do
                              begin
                                   //TerminaTransaccion( False );
                                   RollBackTransaccion;
                                   raise;
                              end;
                        end;
                     finally
                            FreeAndNil( FDataset );
                     end;
                  finally
                         CalcularTiemposEnd;
                  end;
             end;
          finally
                 FreeAndNil( oLabor );
          end;
     end;
     SetComplete;
end;

function TdmServerLabor.GrabaAsignaAreas(Empresa: OleVariant; oDelta: OleVariant; Parametros: OleVariant; out ErrorCount: Integer): OleVariant;

   procedure CrearListaErrores;
   begin
        with cdsLista do
        begin
             InitTempDataSet;
             AddIntegerField( 'CB_CODIGO' );
             AddStringField( 'CB_AREA', 6 );
             AddStringField( 'ANTERIOR', 6 );
             AddStringField( 'ERROR', 100 );
             IndexFieldNames:= 'CB_CODIGO';
             CreateTempDataset;
        end;
   end;

begin
     try
        FListaGridLabor := VACIO;
        SetTablaInfo( eKardexArea );
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList( Parametros );
             CrearListaErrores;
             with TablaInfo do
             begin
                  BeforeUpdateRecord := BeforeUpdateAsignaAreas;
             end;
             FDataSet := CreateQuery( GetSQLScript( Q_INSERT_KARDEX_AREA ) );
             FDataSetAdic := CreateQuery( GetSQLScript( Q_DELETE_KARDEX_AREA ) );
             if ( ParamList.ParamByName( 'TipoRegreso' ).AsInteger = K_PRIMERA_HORA ) then    // Hay que Abrir query de Areas
             begin
                  FQryAreas := CreateQueryLocate;
                  AbreQueryScript( FQryAreas, GetSQLScript( Q_GET_AREA_PRIMERA_HORA ) );
             end;
             try
                ParamAsInteger( FDataSet, 'US_CODIGO', UsuarioActivo );
                ParamAsDate( FDataSet, 'KA_FEC_MOV', ParamList.ParamByName( 'FechaDefault' ).AsDate );
                GrabaTablaGrid( Empresa, oDelta, ErrorCount );
                Result := cdsLista.Data;
             finally
                FreeAndNil( FDataSetAdic );
                FreeAndNil( FDataSet );
             end;
        end;
        ErrorCount := ( cdsLista.RecordCount );
        RecalculaKardexArea( FListaGridLabor );
     except
        DataBaseError( 'Error al Grabar Kardex de Areas' );
     end;
     SetComplete;
end;

procedure TdmServerLabor.BeforeUpdateAsignaAreas(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
var
   iEmpleado, iTipoRegreso: Integer;
   sArea, sAnterior, sHoraRegreso: String;
   dFechaRegreso: TDate;
   lOk: Boolean;

   function AgregaKardexArea( const sCodigo: String; const dFecha: TDate; const sHora: String ): Boolean;
   var
      sError: String;
      dFechaTope: TDate; // Validar Tope 24 Horas
      sHoraTope: String; // Validar Tope 24 Horas
   begin
        dFechaTope := dFecha;
        sHoraTope  := sHora;
        ZetaServerTools.RevisaHorasTope24( dFechaTope, sHoraTope );
        with oZetaProvider do
        begin
             ParamAsInteger( FDataSetAdic, 'CB_CODIGO', iEmpleado );
             ParamAsDate( FDataSetAdic, 'KA_FECHA', dFechaTope );
             ParamAsChar( FDataSetAdic, 'KA_HORA', sHoraTope, K_ANCHO_HH_MM );
             try
                Ejecuta( FDataSetAdic );
                Result := TRUE;
             except
                on E: Exception do
                begin
                     Result := FALSE;
                     sError := Format( 'No se Pudo Reemplazar Registro Anterior de Kardex de Area el %s a las %s', [ FechaCorta( dFechaTope ), Copy( sHoraTope, 1, 2 ) + ':' + Copy( sHoraTope, 3, 4 ) ] );
                     cdsLista.AppendRecord( [ iEmpleado, sArea, sAnterior, sError ] );
                end;
             end;
             if Result then
             begin
                  ParamAsInteger( FDataSet, 'CB_CODIGO', iEmpleado );
                  {$ifdef HYUNDAI}
                  ParamAsChar( FDataSet, 'CB_AREA', sCodigo, K_ANCHO_AREA10 );
                  {$else}
                  ParamAsChar( FDataSet, 'CB_AREA', sCodigo, K_ANCHO_AREA );
                  {$endif}
                  ParamAsDate( FDataSet, 'KA_FECHA', dFechaTope );
                  ParamAsChar( FDataSet, 'KA_HORA', sHoraTope, K_ANCHO_HH_MM );
                  ParamAsChar( FDataSet, 'KA_HOR_MOV', FormatDateTime( 'hhmm', Now ), K_ANCHO_HH_MM );
                  try
                     Result := ( Ejecuta( FDataSet ) > 0 );
                  except
                     on E: Exception do
                     begin
                          Result := FALSE;
                          if PK_Violation( E ) then
                             sError := Format( 'Ya Existe un Registro de Kardex de Area el %s a las %s', [ FechaCorta( dFechaTope ), Copy( sHoraTope, 1, 2 ) + ':' + Copy( sHoraTope, 3, 4 ) ] )
                          else
                             sError := E.Message;
                          cdsLista.AppendRecord( [ iEmpleado, sArea, sAnterior, sError ] );
                     end;
                  end;
             end;
        end;
   end;

   function GetPrimeraHora( const sCodigo: String ): String;
   var
      sHora: string;
   begin
        Result := ZetaCommonClasses.K_CERO_HORAS;
        with FQryAreas do
        begin
             if Locate( 'TB_CODIGO', sCodigo, [] ) then
             begin
                  sHora := FieldByName( 'AR_PRI_HOR' ).AsString;
                  if strLleno( sHora ) then
                     Result := sHora;
             end;
        end;
   end;

begin
     with DeltaDS do
     begin
          iEmpleado := CampoAsVar( FieldByName( 'CB_CODIGO' ) );
          sArea := CampoAsVar( FieldByName( 'CB_AREA' ) );
          sAnterior := CampoAsVar( FieldByName( 'ANTERIOR' ) );
     end;
     with oZetaProvider do
     begin
          with ParamList do
          begin
               iTipoRegreso := ParamByName( 'TipoRegreso' ).AsInteger;
               lOk := AgregaKardexArea( sArea, ParamByName( 'Fecha' ).AsDate, ParamByName( 'Hora' ).AsString );
               if lOk and ( iTipoRegreso <> K_SIN_REGRESO ) then
               begin
                    if ( iTipoRegreso = K_PRIMERA_HORA ) then
                    begin
                         dFechaRegreso := ParamByName( 'Fecha' ).AsDate + 1;
                         sHoraRegreso := GetPrimeraHora( sAnterior );
                    end
                    else
                    begin
                         dFechaRegreso := ParamByName( 'FechaRegreso' ).AsDate;
                         sHoraRegreso := ParamByName( 'HoraRegreso' ).AsString;
                    end;
                    AgregaKardexArea( sAnterior, dFechaRegreso, sHoraRegreso );
               end;
          end;
     end;
     if lOk then
        FListaGridLabor := ConcatString( FListaGridLabor, IntToStr( iEmpleado ), ',' );
     Applied := TRUE;
end;

function TdmServerLabor.GetSupArea(Empresa: OleVariant; const Supervisor: WideString): OleVariant;
begin
     SetTablaInfo( eSupArea );
     with oZetaProvider do
     begin
          TablaInfo.Filtro :=  Format( '( CB_NIVEL = %s )', [ EntreComillas( Supervisor ) ] );
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerLabor.GrabaSupArea(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer; const Supervisor: WideString ): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          EmpiezaTransaccion;
          try
             ExecSQL( Empresa, Format( GetSQLScript( Q_SUP_AREA_DELETE ), [ EntreComillas( Supervisor ) ] ) );
             TerminaTransaccion( TRUE );
          except
                on Error: Exception do
                begin
                     TerminaTransaccion( FALSE );
                     raise;
                end;
          end;
          if VarIsNull( oDelta ) then
             Result := oDelta
          else
          begin
               SetTablaInfo( eSupArea );
               Result := GrabaTabla( Empresa, oDelta, ErrorCount );
          end;
     end;
     SetComplete;
end;

function TdmServerLabor.GetKardexArea(Empresa: OleVariant; Empleado: Integer): OleVariant;
begin
     SetTablaInfo( eKardexArea );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( '( CB_CODIGO = %d )', [ Empleado ] );
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerLabor.GrabaKardexArea(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     FListaGridLabor := VACIO;
     try
        SetTablaInfo( eKardexArea );
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             InitGlobales;
             with TablaInfo do
             begin
                  BeforeUpdaterecord := BeforeUpdateKarArea;
                  AfterUpdateRecord := AfterUpdateKarArea;
             end;
             Result := oZetaProvider.GrabaTabla( Empresa, oDelta, ErrorCount );
        end;
     except
           on Error: Exception do
           begin
                DataBaseError( 'Error al Grabar Kardex de Areas:' + Error.Message );
           end;
     end;
     RecalculaKardexArea( FListaGridLabor );
     SetComplete;
end;

procedure TdmServerLabor.AfterUpdateKarArea(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
var
   iEmpleado: Integer;
begin
     with DeltaDS do
     begin
          iEmpleado := CampoAsVar( FieldByName( 'CB_CODIGO' ) );
          FListaGridLabor := ConcatString( FListaGridLabor, IntToStr( iEmpleado ), ',' );
     end;
end;

procedure TdmServerLabor.BeforeUpdateKarArea(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
     ValidaFechaLabor( DeltaDS, UpdateKind, 'KA_FECHA' );
end;

procedure TdmServerLabor.RecalculaKardexArea( const sListaEmp: String );
var
   i : Integer;
   oLista: TStrings;
begin
     with oZetaProvider do
     begin
          EmpiezaTransaccion;
          try
             FDataSet := CreateQuery( GetSQLScript( Q_RECALCULA_KARDEX_AREA ) );
             oLista := TStringList.Create;
             try
                oLista.CommaText := sListaEmp;
                for i := 0 to oLista.Count - 1 do
                begin
                     ParamAsInteger( FDataSet, 'Empleado', StrToIntDef( oLista[i], 0 ) );
                     Ejecuta( FDataSet );
                end;
             finally
                FreeAndNil( oLista );
                FreeAndNil( FDataSet );
             end;
             TerminaTransaccion( TRUE );
          except
             TerminaTransaccion( FALSE );
          end;
     end;
end;

function TdmServerLabor.GetAreas(Empresa: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          Result:= OpenSQL( Empresa, Format( GetSQLScript( Q_CAT_AREAS_SUPERVISORES ), [ UsuarioActivo ] ), TRUE );
     end;
     SetComplete;
end;

function TdmServerLabor.GetEmpAreas(Empresa: OleVariant; Fecha: TDateTime; const Hora: WideString): OleVariant;
var
   oDataSet: TClientDataSet;
   FGeneral: TZetaCursor;
   iEmpleado, iSuper: Integer;
   sArea, sFecha: String;
   dFecha: TDate;        // Validar Tope 24 Horas
   sHora : String;       // Validar Tope 24 Horas

   function GetNivel: String;
   begin
        if ( FDataSet.FieldByName( 'CB_FEC_NIV' ).AsDateTime <= Fecha ) then
           Result := FDataSet.FieldByName( Format( 'CB_NIVEL%d', [ iSuper ] ) ).AsString
        else
        begin
             oZetaProvider.ParamAsInteger( FGeneral, 'EMPLEADO', iEmpleado );
             with FGeneral do
             begin
                  Active := TRUE;
                  Result := FieldByName( 'RESULTADO' ).AsString;
                  Active := FALSE;
             end;
        end;
   end;

   function GetArea( const iEmpleado: Integer ): String;
   begin
        oZetaProvider.ParamAsInteger( FGeneral, 'EMPLEADO', iEmpleado );
        with FGeneral do
        begin
             Active := TRUE;
             Result := FieldByName( 'CB_AREA' ).AsString;
             Active := FALSE;
        end;
   end;

begin
     oDataSet := TClientDataSet.Create( Self );
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             InitGlobales;
             iSuper := GetGlobalInteger( ZGlobalTress.K_GLOBAL_NIVEL_SUPERVISOR );
             if ( iSuper < 1 ) or ( iSuper > 9 ) then
                DataBaseError( 'No Tiene Nivel de Supervisores Definido' );
             // Asigna Fecha y Hora para Validar Tope 24 horas
             dFecha := Fecha;
             sHora  := Hora;
             ZetaServerTools.RevisaHorasTope24( dFecha, sHora );
             sFecha := DateToStrSQLC( dFecha );
             with oDataSet do
             begin
                  Data:= OpenSQL( Empresa, Format( GetSQLScript( Q_EMPLEADOS_ASIGNA ), [ sFecha, UsuarioActivo, FiltroNivel0( Empresa ) ] ), TRUE );
                  LogChanges := FALSE;
             end;
             FDataSet := CreateQuery( Format( GetSQLScript( Q_EMPLEADOS_AREAS ), [ iSuper, sFecha, EntreComillas( sHora ), UsuarioActivo, FiltroNivel0( Empresa ) ] ) );
             FGeneral := CreateQuery( Format( GetSQLScript( Q_EMPLEADO_NIVEL ), [ sFecha ] ) );
             try
                with FDataSet do
                begin
                     Active := TRUE;
                     while not EOF do
                     begin
                          iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                          sArea := FieldByName( 'CB_AREA' ).AsString;
                          if oDataSet.Locate( 'CB_CODIGO', iEmpleado, [] ) then
                          begin
                               oDataSet.Edit;
                               oDataSet.FieldByName( 'CB_AREA' ).AsString := sArea;
                               if ( sArea = VACIO ) then
                                  oDataSet.FieldByName( 'TIPOAREA' ).AsInteger := Ord( taNoArea )
                               else
                                  oDataSet.FieldByName( 'TIPOAREA' ).AsInteger := Ord( taMisAreas );
                               oDataSet.Post;
                          end
                          else if ( sArea <> VACIO ) then
                          begin
                               oDataSet.Append;
                               oDataSet.FieldByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
                               oDataSet.FieldByName( 'CB_TIPO' ).AsInteger := 2;   // C�digo de Prestado??
                               oDataSet.FieldByName( 'PRETTYNAME' ).AsString := FieldByName( 'PRETTYNAME' ).AsString;
                               oDataSet.FieldByName( 'CB_AREA' ).AsString := FieldByName( 'CB_AREA' ).AsString;
                               oDataSet.FieldByName( 'TIPOAREA' ).AsInteger := Ord( taOtroSuper );
                               oDataSet.FieldByName( 'CB_NIVEL' ).AsString := GetNivel;
                               oDataSet.Post;
                          end;
                          Next;
                     end;
                     Active := FALSE;
                end;
             finally
                FreeAndNil( FGeneral );
                FreeAndNil( FDataset );
             end;
             FGeneral := CreateQuery( Format( GetSQLScript( Q_EMPLEADO_AREA ), [ sFecha, EntreComillas( sHora ) ] ) );
             try
                with oDataSet do
                begin
                     First;
                     while not EOF do
                     begin
                          if ( eTipoArea( FieldByName( 'TIPOAREA' ).AsInteger ) = taOtraArea ) then     // Es Mi Empleado y est� en otra Area
                          begin
                               Edit;
                               FieldByName( 'CB_AREA' ).AsString := GetArea( FieldByName( 'CB_CODIGO' ).AsInteger );  // Investigar Area
                               Post;
                          end
                          else if ( FieldByName( 'CB_AREA' ).AsString = Space(6) ) then
                          begin
                               Edit;
                               FieldByName( 'CB_AREA' ).AsString := VACIO;
                               Post;
                          end;
                          Next;
                     end;
                end;
             finally
                FreeAndNil( FGeneral );
             end;
        end;
        Result := oDataSet.Data;
     finally
        FreeAndNil( oDataSet );
     end;
     SetComplete;
end;

function TdmServerLabor.GetAreaKardex(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime; const Hora: WideString): WideString;
var
   dFecha: TDate;   // Validar Tope de 24 horas
   sHora: String;   // Validar Tope de 24 horas
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          // Asigna Fecha y Hora para Validar Tope 24 horas
          dFecha := Fecha;
          sHora  := Hora;
          ZetaServerTools.RevisaHorasTope24( dFecha, sHora );
          FDataSet:= CreateQuery;
          try
             AbreQueryScript( FDataSet,Format( GetSQLScript( Q_EMPLEADO_AREA_KARDEX ), [ Empleado,
                                               DateToStrSQLC( dFecha ), EntreComillas( sHora ) ] ) );
             Result := FDataSet.FieldByName( 'CB_AREA' ).AsString;
          finally
             FreeAndNil( FDataSet );
          end;
     end;
     SetComplete;
end;

function TdmServerLabor.GetEmpBreaks(Empresa: OleVariant;const Area: WideString; Fecha: TDateTime; const Hora: WideString; lSoloAltas: WordBool): OleVariant;
begin
     with oZetaProvider do
     begin
          Result := OpenSQL( Empresa, Format( GetSqlScript( Q_EMPLEADOS_AREA ),[ DateToStrSQLC( Fecha ), EntreComillas( Hora ), EntreComillas( Area ), FiltroNivel0( Empresa ) ] ), not lSoloAltas );
     end;
     SetComplete;
end;

function TdmServerLabor.GrabaEmpBreaks(Empresa, oDelta: OleVariant; Fecha: TDateTime; const Hora: WideString; Duracion: Integer; out ErrorCount: Integer): OleVariant;
begin
     FFecha := Fecha;
     FHora := Hora;
     FDuracion := Duracion;
     InitCreator;
     FLaborCalculo := TLCalculo.Create( oZetaCreator );
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             SetTablaInfo( eWorks );
             with FLaborCalculo do
             begin
                  RegistrarBreakManualBegin( UsuarioActivo, FFecha, FFecha );
                  try
                     with TablaInfo do
                     begin
                          BeforeUpdateRecord := BeforeUpdateEmpBreaks;
                     end;
                     Result := GrabaTablaGrid( Empresa, oDelta, ErrorCount );
                  finally
                         RegistrarBreakManualEnd;
                  end;
             end;
        end;
     finally
            FLaborCalculo.Free;
     end;
     SetComplete;
end;

procedure TdmServerLabor.BeforeUpdateEmpBreaks(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
var
   sTexto, sError, sUsuario: String;
begin
     try
        with DeltaDS do
        begin
             sUsuario := CampoAsVar( FieldByName( 'CB_CODIGO' ) );
             if not FLaborCalculo.RegistrarBreak( CampoAsVar( FieldByName('CB_CODIGO') ), FDuracion, FFecha, FHora, sTexto ) then
                DatabaseError( sTexto );
        end;
        Applied := True;
     except
           on Error: Exception do
           begin
                sError := 'Registro Ya Existe, Para El Empleado: ' + sUsuario;
                if PK_Violation( Error ) then
                    Error.Message := K_ERROR_BREAKS + sError
                else
                    Error.Message := K_ERROR_BREAKS + Error.Message;
                raise;
           end;
     end;
end;

procedure TdmServerLabor.CancelarBreaksParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Fecha: ' + FechaCorta( ParamByName( 'Fecha' ).AsDateTime ) +
                              ZetaCommonClasses.K_PIPE + 'Hora: ' + FormatMaskText( '99:99;0', ParamByName( 'Hora' ).AsString ) +
                              ZetaCommonClasses.K_PIPE + 'Area: ' + ParamByName( 'Area' ).AsString;
     end;
end;

function TdmServerLabor.CancelarBreaksGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
     end;
     InitBroker;
     try
        CancelarBreaksBuildDataset;
        Result := SQLBroker.SuperReporte.GetReporte;
     finally
        ClearBroker;
     end;
     SetComplete;
end;

function TdmServerLabor.CancelarBreaksLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
     end;
     CancelarBreaksParametros;
     cdsLista.Lista := Lista;
     Result := CancelarBreaksDataset( cdsLista );
     SetComplete;
end;

function TdmServerLabor.CancelarBreaks(Empresa: OleVariant; Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
     end;
     CancelarBreaksParametros;
     InitBroker;
     try
        CancelarBreaksBuildDataset;
        Result := CancelarBreaksDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
        ClearBroker;
     end;
     SetComplete;
end;

procedure TdmServerLabor.CancelarBreaksBuildDataset;
const
     {$ifdef INTERBASE}
     K_FILTRO_AREA = '( %s = ( select CB_AREA from SP_FECHA_AREA( %s, %s, %s ) ) )';
     {$endif}
     {$ifdef MSSQL}
     K_FILTRO_AREA = '( %s = dbo.sp_fecha_area( %s, %s, %s ) )';
     {$endif}
var
   sFecha, sHora, sArea : String;
begin
     with oZetaProvider.ParamList do
     begin
          sFecha := DateToStrSQLC( ParamByName( 'Fecha' ).AsDateTime );
          sHora := EntreComillas( ParamByName( 'Hora' ).AsString );
          sArea := ParamByName( 'Area' ).AsString;
     end;
     with SQLBroker do
     begin
          Init( enWorks );
          with Agente do
          begin
               AgregaColumna( 'CB_CODIGO', TRUE, Entidad, tgNumero, 0, 'CB_CODIGO' );
               AgregaColumna( K_PRETTYNAME, TRUE, enEmpleado, tgTexto, 50, 'PRETTYNAME' );
               AgregaColumna( 'AU_FECHA', TRUE, Entidad, tgFecha, 0, 'AU_FECHA' );
               AgregaColumna( 'WK_PRE_CAL', TRUE, Entidad, tgNumero, 0, 'WK_PRE_CAL' );
               AgregaColumna( 'WK_HORA_A', TRUE, Entidad, tgTexto, K_ANCHO_HH_MM, 'WK_HORA_A' );
               AgregaColumna( 'WK_FOLIO', TRUE, Entidad, tgNumero, 0, 'WK_FOLIO' );
               AgregaFiltro( '( WORKS.WK_MANUAL = ''S'' )', True, Entidad );
               AgregaFiltro( Format( '( WORKS.WK_TIPO = %d )', [ Ord( wtBreak ) ] ), True, Entidad );
               AgregaFiltro( Format( '( WORKS.AU_FECHA = %s )', [ sFecha ] ), True, Entidad );
               AgregaFiltro( Format( '( WORKS.WK_HORA_A = %s )', [ sHora ] ), True, Entidad );
               if strLleno( sArea ) then
                  AgregaFiltro( Format( K_FILTRO_AREA, [ EntreComillas( sArea ), 'WORKS.CB_CODIGO', sFecha, sHora ] ), True, Entidad );
               AgregaOrden( 'CB_CODIGO', True, Entidad );
          end;
          with oZetaProvider do
          begin
               AgregaRangoCondicion( ParamList );
               FiltroConfidencial( EmpresaActiva );
               BuildDataset( EmpresaActiva );
          end;
     end;
end;

function TdmServerLabor.CancelarBreaksDataset(Dataset: TDataset): OleVariant;
var
   FBorraBreaks : TZetaCursor;
   iEmpleado: Integer;
   dFecha: TDate;
   sHora: String;
begin
     with oZetaProvider do
     begin
          if OpenProcess( prLabCancelarBreaks, Dataset.RecordCount, FListaParametros ) then
          begin
               with ParamList do
               begin
                    dFecha := ParamByName( 'Fecha' ).AsDateTime;
                    sHora := ParamByName( 'Hora' ).AsString
               end;
               try
                  FBorraBreaks:= CreateQuery( GetSQLScript( Q_BORRA_BREAKS ) );
                  try
                     try
                        ParamAsDate( FBorraBreaks, 'Fecha', dFecha );
                        ParamAsVarChar( FBorraBreaks, 'Hora', sHora, K_ANCHO_HH_MM );
                        with Dataset do
                        begin
                             First;
                             while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                             begin
                                  iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                                  ParamAsInteger( FBorraBreaks, 'Empleado', iEmpleado );
                                  ParamAsInteger( FBorraBreaks, 'Folio', FieldByName( 'WK_FOLIO' ).AsInteger );
                                  EmpiezaTransaccion;
                                  try
                                     Ejecuta( FBorraBreaks );
                                     TerminaTransaccion( True );
                                  except
                                     on Error: Exception do
                                     begin
                                          RollBackTransaccion;
                                          Log.Excepcion( iEmpleado, Format( 'Error Al Cancelar Break a las %s:%s - %s', [ Copy( sHora, 1, 2 ),
                                                                            Copy( sHora, 3, 2 ), FechaCorta( dFecha ) ] ), Error );
                                     end;
                                  end;
                                  Next;
                             end;
                        end;
                     except
                           on Error: Exception do
                           begin
                                Log.Excepcion( 0, 'Error Al Cancelar Breaks', Error );
                           end;
                     end;
                  finally
                         FreeAndNil( FBorraBreaks );
                  end;
               except
                     on Error: Exception do
                     begin
                          Log.Excepcion( 0, 'Error Al Preparar Cancelaci�n De Breaks', Error );
                     end;
               end;
          end;
          Result := CloseProcess;
     end;
end;

function TdmServerLabor.GetHistorialCedInsp(Empresa, Parametros: OleVariant): OleVariant;
var
   sFiltro: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
     end;

     with oZetaProvider.ParamList do
     begin
          with ParamByName( 'Tipo' ) do
          begin
               if ( AsInteger >= 0 ) then
                  sFiltro := Format( '( CI_TIPO = %d )', [ AsInteger ] );
          end;
          with ParamByName( 'Resultado' ) do
          begin
               if ( AsInteger >= 0 ) then
                  sFiltro := ConcatFiltros( sFiltro, Format( '( CI_RESULT = %d )', [ AsInteger ] ) );
          end;
          with ParamByName( 'Usuario' ) do
          begin
               if ( AsInteger > 0 ) then
                  sFiltro := ConcatFiltros( sFiltro, Format( '( US_CODIGO = %d )', [ AsInteger ] ) );
          end;
          if ( ParamByName( 'FechaInicial' ).AsDate <> NullDateTime) and ( ParamByName( 'FechaFinal' ).AsDate <> NullDateTime) then
          begin
               sFiltro := ConcatFiltros( sFiltro, Format( '( CI_FECHA >= %s ) and ( CI_FECHA <= %s )',
                                                      [ DateToStrSQLC( ParamByName( 'FechaInicial' ).AsDate ),
                                                        DateToStrSQLC( ParamByName( 'FechaFinal' ).AsDate ) ] ) );
          end;
          //sFiltro := GetFiltroCedula( 'Cedula', '( CE_FOLIO = ''%s'' )' );<-- no se usa el campo de cedula
          sFiltro := GetFiltroCedula( sFiltro, 'Parte', '( AR_CODIGO = ''%s'' )' );
          sFiltro := GetFiltroCedula( sFiltro, 'Orden', '( WO_NUMBER = ''%s'' )' );
          sFiltro := GetFiltroCedula( sFiltro, 'Area', '( CI_AREA = ''%s'' )' );
     end;

     if strLleno( sFiltro ) then
        sFiltro := 'where ' + sFiltro;
     Result := oZetaProvider.OpenSQL( Empresa, Format( GetSQLScript( Q_CEDULA_INSP_HISTORIAL ), [ sFiltro ] ), TRUE );

     SetComplete;
end;

function TdmServerLabor.GetCedInspeccion(Empresa: OleVariant; var Defectos: Olevariant; Cedula: integer ): OleVariant;
var
   sFiltro: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          with ParamList do
          begin
               sFiltro := Format( '( CI_FOLIO = %d )', [ Cedula ] );
          end;
          SetTablaInfo( eCedulasInsp );
          TablaInfo.Filtro := sFiltro;
          Result := GetTabla( Empresa );
          SetTablaInfo( eDefecto );
          TablaInfo.Filtro := sFiltro;
          Defectos := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerLabor.GrabaCedInspeccion( Empresa, Delta, Defectos: Olevariant; Cedula: integer;
                                            out ErrorCount: Integer): OleVariant;
 var
    FDataSet: TZetaCursor;
    i, iLow, iHigh : integer;
begin
     SetOLEVariantToNull( Result );
     FCedulaInspeccion := Cedula;

     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          if not VarIsNull( Delta ) then
          begin
               SetTablaInfo( eCedulasInsp );
               TablaInfo.BeforeUpdateRecord := BeforeUpdateCedInspeccion;
               Result := GrabaTabla( Empresa, Delta, ErrorCount );
          end;

          if ( ErrorCount = 0 ) and not VarIsNull( Defectos ) then
          begin
               EmpiezaTransaccion;
               try
                  ExecSQL( Empresa, Format( GetSQLScript( Q_DEFECTOS_BORRA ), [ FCedulaInspeccion ] ) );
                  TerminaTransaccion(True);
               except
                     TerminaTransaccion(False);
               end;

               FDataSet := CreateQuery( GetSQLScript( Q_DEFECTOS_INSERT ) );
               try
                  iLow := VarArrayLowBound( Defectos,1 );
                  iHigh := VarArrayHighBound( Defectos, 1 );

                  if ( iLow + iHigh > 0 ) then
                  begin
                       try
                          EmpiezaTransaccion;
                          ParamAsInteger( FDataSet, 'CI_FOLIO', FCedulaInspeccion );

                          for i := iLow to iHigh do
                          begin
                               with FDataset do
                               begin
                                    ParamAsInteger( FDataSet, 'DE_FOLIO', i );
                                    ParamAsChar( FDataSet, 'DE_CODIGO', Defectos[ i ][ 3 ], 6 );
                                    ParamAsFloat( FDataSet, 'DE_PIEZAS', Defectos[ i ][ 4 ] );
                                    ParamAsVarChar( FDataSet, 'DE_COMENTA', Defectos[ i ][ 5 ], 40 );
                                    Ejecuta( FDataset );
                               end;
                          end;
                          TerminaTransaccion(True);
                       except
                             TerminaTransaccion(False);
                       end;
                  end;
               finally
                      FreeAndNil( FDataSet );
               end;
          end;

     end;
     SetComplete;
end;

function TdmServerLabor.GetMaxCedula( const iScript: integer ): Integer;
var
   FDataSet : TZetaCursor;
begin
     FDataSet := oZetaProvider.CreateQuery( GetSqlScript( iScript ) );
     try
        FDataSet.Open;
        Result := FDataSet.Fields[ 0 ].AsInteger + 1;
        FDataSet.Close;
     finally
            FreeAndNil(FDataSet);
     end;
end;

function TdmServerLabor.GetMaxCedulaInspeccion: Integer;
var
   FDataSet : TZetaCursor;
begin
     FDataSet := oZetaProvider.CreateQuery( GetSqlScript( Q_MAX_CEDULAINSPECCION ) );
     try
        FDataSet.Open;
        Result := FDataSet.Fields[ 0 ].AsInteger + 1;
        FDataSet.Close;
     finally
            FreeAndNil(FDataSet);
     end;
end;

procedure TdmServerLabor.BeforeUpdateCedInspeccion( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean );
begin
     if ( UpdateKind = ukInsert ) then
     begin
          FCedulaInspeccion := GetMaxCedulaInspeccion;
          DeltaDS.Edit;
          DeltaDS.FieldByName( 'CI_FOLIO' ).AsInteger := FCedulaInspeccion;
          DeltaDS.Post;
     end;
end;

function TdmServerLabor.BorraCedulasInsp(Empresa: OleVariant; Cedula: Integer; out ErrorCount: Integer): OleVariant;
begin
     Result := NULL;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          try
             EmpiezaTransaccion;
             ExecSQL( Empresa, Format( GetSQLScript( Q_BORRA_CED_INSP ), [ Cedula ] ) );
             TerminaTransaccion(True);
          except
                TerminaTransaccion(False);
          end;
     end;
     SetComplete;
end;

function TdmServerLabor.BorraCedulaScrap(Empresa: OleVariant; Cedula: Integer; out ErrorCount: Integer): OleVariant;
begin
     Result := NULL;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          try
             EmpiezaTransaccion;
             ExecSQL( Empresa, Format( GetSQLScript( Q_BORRA_CED_SCRAP ), [ Cedula ] ) );
             TerminaTransaccion(True);
          except
                TerminaTransaccion(False);
          end;
     end;
     SetComplete;
end;

function TdmServerLabor.GetCedulaScrap(Empresa: OleVariant; var Scrap: Olevariant; Cedula: integer): OleVariant;
var
   sFiltro: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          with ParamList do
          begin
               sFiltro := Format( '( CS_FOLIO = %d )', [ Cedula ] );
          end;
          SetTablaInfo( eCedulasScrap );
          TablaInfo.Filtro := sFiltro;
          Result := GetTabla( Empresa );
          SetTablaInfo( eScrap );
          TablaInfo.Filtro := sFiltro;
          Scrap := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerLabor.GetHistorialCedScrap(Empresa, Parametros: OleVariant): OleVariant;
var
   sFiltro: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
     end;

     with oZetaProvider.ParamList do
     begin
          with ParamByName( 'Usuario' ) do
          begin
               if ( AsInteger > 0 ) then
                  sFiltro := ConcatFiltros( sFiltro, Format( '( US_CODIGO = %d )', [ AsInteger ] ) );
          end;
          if ( ParamByName( 'FechaInicial' ).AsDate <> NullDateTime) and ( ParamByName( 'FechaFinal' ).AsDate <> NullDateTime) then
          begin
               sFiltro := ConcatFiltros( sFiltro, Format( '( CS_FECHA >= %s ) and ( CS_FECHA <= %s )',
                                                      [ DateToStrSQLC( ParamByName( 'FechaInicial' ).AsDate ),
                                                        DateToStrSQLC( ParamByName( 'FechaFinal' ).AsDate ) ] ) );
          end;
          sFiltro := GetFiltroCedula( sFiltro, 'Parte', '( AR_CODIGO = ''%s'' )' );
          sFiltro := GetFiltroCedula( sFiltro, 'Orden', '( WO_NUMBER = ''%s'' )' );
          sFiltro := GetFiltroCedula( sFiltro, 'Area', '( CS_AREA = ''%s'' )' );
          sFiltro := GetFiltroCedula( sFiltro, 'Operacion', '( OP_NUMBER = ''%s'' )' );
     end;

     if strLleno( sFiltro ) then
        sFiltro := 'where ' + sFiltro;
     Result := oZetaProvider.OpenSQL( Empresa, Format( GetSQLScript( Q_CEDULA_SCRAP_HISTORIAL ), [ sFiltro ] ), TRUE );

     SetComplete;
end;

function  TdmServerLabor.GetMaxCedulaScrap: Integer;
begin
     Result := GetMaxCedula( Q_MAX_CEDULASCRAP )
end;

procedure TdmServerLabor.BeforeUpdateCedulaScrap( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean );
begin
     if ( UpdateKind = ukInsert ) then
     begin
          FCedulaScrap := GetMaxCedulaScrap;
          DeltaDS.Edit;
          DeltaDS.FieldByName( 'CS_FOLIO' ).AsInteger := FCedulaScrap;
          DeltaDS.Post;
     end;
end;

function TdmServerLabor.GrabaCedulaScrap(Empresa, Delta, Scrap: OleVariant; Cedula: Integer; out ErrorCount: Integer): OleVariant;
 var
    FDataSet: TZetaCursor;
    i, iLow, iHigh : integer;
begin
     SetOLEVariantToNull( Result );
     FCedulaScrap := Cedula;

     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          if not VarIsNull( Delta ) then
          begin
               SetTablaInfo( eCedulasScrap );
               TablaInfo.BeforeUpdateRecord := BeforeUpdateCedulaScrap;
               Result := GrabaTabla( Empresa, Delta, ErrorCount );
          end;

          if ( ErrorCount = 0 ) and not VarIsNull( SCrap ) then
          begin
               EmpiezaTransaccion;
               try
                  ExecSQL( Empresa, Format( GetSQLScript( Q_SCRAP_BORRA ), [ FCedulaScrap ] ) );
                  TerminaTransaccion(True);
               except
                     TerminaTransaccion(False);
               end;

               FDataSet := CreateQuery( GetSQLScript( Q_SCRAP_INSERT ) );
               try
                  iLow := VarArrayLowBound( Scrap,1 );
                  iHigh := VarArrayHighBound( Scrap, 1 );

                  if ( iLow + iHigh > 0 ) then
                  begin
                       try
                          EmpiezaTransaccion;
                          ParamAsInteger( FDataSet, 'CS_FOLIO', FCedulaScrap );

                          for i := iLow to iHigh do
                          begin
                               with FDataset do
                               begin
                                    ParamAsInteger( FDataSet, 'SC_FOLIO', i );
                                    ParamAsChar( FDataSet, 'CN_CODIGO', Scrap[ i ][ 3 ], 15 );
                                    ParamAsChar( FDataSet, 'SC_MOTIVO', Scrap[ i ][ 4 ], 6 );
                                    ParamAsFloat( FDataSet, 'SC_PIEZAS', Scrap[ i ][ 5 ] );
                                    ParamAsVarChar( FDataSet, 'SC_COMENTA', Scrap[ i ][ 6 ], 40 );
                                    Ejecuta( FDataset );
                               end;
                          end;
                          TerminaTransaccion(True);
                       except
                             TerminaTransaccion(False);
                       end;
                  end;
               finally
                      FreeAndNil( FDataSet );
               end;
          end;

     end;
     SetComplete;
end;

function TdmServerLabor.GetComponentes(Empresa: OleVariant): OleVariant;
begin
     Result := oZetaProvider.OpenSQL( Empresa , GetSQLScript( Q_GET_COMPONENTES ), TRUE );
     SetComplete;
end;

function TdmServerLabor.GetComponente(Empresa: OleVariant; const Componente: WideString): OleVariant;
begin
     SetTablaInfo( eComponentes );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( '( CN_CODIGO = %s )', [ EntreComillas( Componente ) ] );
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerLabor.GrabaComponente(Empresa: OleVariant; Delta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( eComponentes );
     with oZetaProvider do
     begin
          //TablaInfo.Filtro := Format( '( CN_CODIGO = %s )', [ Componente ] );
          Result := GrabaTabla( Empresa, Delta, ErrorCount );
     end;
     SetComplete;
end;

function TdmServerLabor.ImportarComponentes(Empresa, Parametros, ListaASCII: OleVariant): OleVariant;
{
Nombre              Campo               Tipo     Ancho Formato              Requerido
================    =================   =====    ===== ==================== =====================
C�digo              COMPONEN.CN_CODIGO  Texto    15    X(15)                Obligatorio
Nombre              COMPONEN.CN_NOMBRE  Texto    40    X(40)                Obligatorio
Nombre Corto        COMPONEN.CN_SHORT   Texto    20    X(20)                Opcional, default ''
C�digo de Barras    COMPONEN.CN_BARCODE Texto    20    X(20)                Opcional, default ''
Costo               COMPONEN.CN_COSTO   Flotante 15    9999999999.9999      Opcional, default 0
Unidad de Medida    COMPONEN.CN_UNIDAD  Texto    8     X(8)                 Opcional, default ''
Nombre en Ingl�s    COMPONEN.CN_INGLES  Texto    30    X(30)                Opcional, default ''
N�mero General 1    COMPONEN.CN_NUMERO1 Flotante 15    9999999999.9999      Opcional, default 0
N�mero General 2    COMPONEN.CN_NUMERO2 Flotante 15    9999999999.9999      Opcional, default 0
Texto General 1     COMPONEN.CN_TEXTO1  Texto    30    X(30)                Opcional, default ''
Texto General 2     COMPONEN.CN_TEXTO2  Texto    30    X(30)                Opcional, default ''
Descripci�n Larga   COMPONEN.CN_DETALLE Texto    255   X(255)               Opcional, default ''
}
const
     K_POST_ERROR = 'Componente %s';
var
   lError: Boolean;
   iCuantos: Integer;
   sCodigo, sDatos: String;
   eFormato: eFormatoASCII;
   FModifica, FInserta, FExiste, FDataset: TZetaCursor;
begin
     InitImportLog;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          eFormato := eFormatoASCII( ParamList.ParamByName( 'Formato' ).AsInteger );
          ImportarCatalogosParametros;
          lError := False;
          iCuantos := 0;
          oSuperASCII := TdmSuperASCII.Create( Self );
          try
             try
                with oSuperASCII do
                begin
                     Formato := eFormato;
                     AgregaColumna( 'CN_CODIGO', tgTexto, K_ANCHO_CODIGOPARTE, True );
                     AgregaColumna( 'CN_NOMBRE', tgTexto, K_ANCHO_DESCLARGA, True );
                     AgregaColumna( 'CN_SHORT', tgTexto, K_ANCHO_DESCCORTA );
                     AgregaColumna( 'CN_BARCODE', tgTexto, K_ANCHO_DESCCORTA );
                     AgregaColumna( 'CN_COSTO', tgFloat, K_ANCHO_PESOS, False, 0, True, '0' );
                     AgregaColumna( 'CN_UNIDAD', tgTexto, K_ANCHO_REFERENCIA );
                     AgregaColumna( 'CN_INGLES', tgTexto, K_ANCHO_DESCRIPCION );
                     AgregaColumna( 'CN_NUMERO1', tgFloat, K_ANCHO_PESOS, False, 0, True, '0' );
                     AgregaColumna( 'CN_NUMERO2', tgFloat, K_ANCHO_PESOS, False, 0, True, '0' );
                     AgregaColumna( 'CN_TEXTO1', tgTexto, K_ANCHO_DESCRIPCION );
                     AgregaColumna( 'CN_TEXTO2', tgTexto, K_ANCHO_DESCRIPCION );
                     AgregaColumna( 'CN_DETALLE', tgTexto, K_ANCHO_FORMULA );
                     with cdsLista do
                     begin
                          Lista := Procesa( ListaASCII );
                          iCuantos := RecordCount - Errores;
                     end;
                end;
             except
                   on Error: Exception do
                   begin
                        lError := True;
                        sDatos := Error.Message;
                   end;
             end;
          finally
                 FreeAndNil( oSuperASCII );
          end;
          if OpenProcess( prLabImportarComponentes, iCuantos, FListaParametros ) then
          begin
               if lError then
                  Log.Error( 0, 'Error Al Leer Archivo De Importaci�n', sDatos )
               else
               begin
                    try
                       FExiste := CreateQuery( GetSQLScript( Q_COMPONENTES_EXISTE ) );
                       FInserta := CreateQuery( GetSQLScript( Q_COMPONENTES_IMPORT_AGREGA ) );
                       FModifica := CreateQuery( GetSQLScript( Q_COMPONENTES_IMPORT_MODIFICA ) );
                       try
                          with cdsLista do
                          begin
                               First;
                               while not Eof do
                               begin
                                    sDatos := FieldByName( 'RENGLON' ).AsString;
                                    if ( FieldByName( 'NUM_ERROR' ).AsInteger = 0 ) then      // No marc� error el SuperASCII para este registro
                                    begin
                                         { Se convierten a May�sculas xq la interfase de usuario no trabaja con min�sculas }
                                         sCodigo := AnsiUpperCase( FieldByName( 'CN_CODIGO' ).AsString );
                                         EmpiezaTransaccion;
                                         try
                                            if ExisteCodigoImportar( sCodigo, FExiste ) then
                                               FDataset := FModifica
                                            else
                                                FDataset := FInserta;
                                            ParamAsVarChar( FDataset, 'CN_CODIGO', sCodigo, K_ANCHO_CODIGOPARTE );
                                            ParamAsVarChar( FDataset, 'CN_NOMBRE', FieldByName( 'CN_NOMBRE' ).AsString, K_ANCHO_DESCLARGA );
                                            ParamAsVarChar( FDataset, 'CN_INGLES', FieldByName( 'CN_INGLES' ).AsString, K_ANCHO_DESCRIPCION );
                                            ParamAsVarChar( FDataset, 'CN_SHORT', FieldByName( 'CN_SHORT' ).AsString, K_ANCHO_DESCCORTA );
                                            ParamAsVarChar( FDataset, 'CN_BARCODE', FieldByName( 'CN_BARCODE' ).AsString, K_ANCHO_DESCCORTA );
                                            ParamAsFloat( FDataset, 'CN_COSTO', FieldByName( 'CN_COSTO' ).AsFloat );
                                            ParamAsFloat( FDataset, 'CN_NUMERO1', FieldByName( 'CN_NUMERO1' ).AsFloat );
                                            ParamAsFloat( FDataset, 'CN_NUMERO2', FieldByName( 'CN_NUMERO2' ).AsFloat );
                                            ParamAsVarChar( FDataset, 'CN_UNIDAD', FieldByName( 'CN_UNIDAD' ).AsString, K_ANCHO_REFERENCIA );
                                            ParamAsVarChar( FDataset, 'CN_TEXTO1', FieldByName( 'CN_TEXTO1' ).AsString, K_ANCHO_DESCRIPCION );
                                            ParamAsVarChar( FDataset, 'CN_TEXTO2', FieldByName( 'CN_TEXTO2' ).AsString, K_ANCHO_DESCRIPCION );
                                            ParamAsVarChar( FDataset, 'CN_DETALLE', FieldByName( 'CN_DETALLE' ).AsString, K_ANCHO_FORMULA );
                                            Ejecuta( FDataset );
                                            TerminaTransaccion( True );
                                         except
                                               on Error: Exception do
                                               begin
                                                    RollBackTransaccion;
                                                    Log.Excepcion( 0, Format( K_POST_ERROR, [ sCodigo ] ), Error );
                                               end;
                                         end;
                                    end
                                    else
                                    begin
                                         Log.Error( 0, 'Registro Rechazado', FieldByName( 'DESC_ERROR' ).AsString + CR_LF + sDatos );
                                    end;
                                    Next;
                               end;
                          end;
                       finally
                              FreeAndNil( FModifica );
                              FreeAndNil( FInserta );
                              FreeAndNil( FExiste );
                       end;
                    except
                          on Error: Exception do
                          begin
                               Log.Excepcion( 0, 'Error Al Importar Datos', Error );
                          end;
                    end;
               end;
          end;
          Result := CloseProcess;
     end;
     SetComplete;
end;

function TdmServerLabor.ImportarCedulas(Empresa, Parametros, ListaASCII: OleVariant): OleVariant;
{
FORMATO GENERAL
Nombre              Campo             Tipo     Ancho Formato              Requerido
================    ================= =====    ===== ==================== =====================
Tipo de C�dula      CEDULA.CE_TIPO    Entero   1     9                    Obligatorio
Fecha               CEDULA.CE_FECHA   Fecha    10    Configurable         Obligatorio
Hora                CEDULA.CE_HORA    Texto    4     9999                 Obligatorio
Area                CEDULA.CE_AREA    Texto    6     X(6)                 Obligatorio
# de Parte          CEDULA.AR_CODIGO  Texto    15    X(15)                Depende de Configuraci�n de la Empresa
Orden de Trabajo    CEDULA.WO_NUMBER  Texto    20    X(20)                Depende de Configuraci�n de la Empresa
ADICIONALES         Ver Tabla X Tipo           27    X(27)                Depende de Configuraci�n de la Empresa
Modulador #1        CEDULA.CE_MOD_1   Texto    1     X                    Opcional, default ''
Modulador #2        CEDULA.CE_MOD_2   Texto    1     X                    Opcional, default ''
Modulador #3        CEDULA.CE_MOD_3   Texto    1     X                    Opcional, default ''
Comentarios         CEDULA.CE_COMENTA Texto    40    X(40)                Opcional, default ''
Folio               CEDULA.CE_FOLIO   Entero   9     999999999            Opcional, default 0
Usuario             CEDULA.US_CODIGO  Entero   5     99999                Opcional, default Usuario Activo

FORMATO ADICIONALES - Tipo = Producci�n
Nombre              Campo             Tipo     Ancho Formato              Requerido
================    ================= =====    ===== ==================== =====================
Piezas              CEDULA.CE_PIEZAS  Flotante 13    99999999.9999        Opcional, default 0
Operaci�n           CEDULA.OP_NUMBER  Texto    10    X(10)                Depende de Configuraci�n de la Empresa
Filler                                Texto    4     X(4)                 No se Requiere, default ''

FORMATO ADICIONALES - Tipo = Horas Especiales
Nombre              Campo             Tipo     Ancho Formato              Requerido
================    ================= =====    ===== ==================== =====================
Piezas              CEDULA.CE_PIEZAS  Flotante 13    99999999.9999        Opcional, default 0
Operaci�n           CEDULA.OP_NUMBER  Texto    10    X(10)                Depende de Configuraci�n de la Empresa
Hora Inicial                          Texto    4     9999                 Obligatorio

FORMATO ADICIONALES - Tipo = Tiempo Muerto
Nombre              Campo             Tipo     Ancho Formato              Requerido
================    ================= =====    ===== ==================== =====================
Filler                                Texto    13    X(13)                No se Requiere, default ''
Motivo              CEDULA.CE_TMUERTO Texto    10    X(10)                Obligatorio ( Solo se leen los primeros 3 caracteres )
Hora Inicial                          Texto    4     9999                 Obligatorio
}
var
   TipoCedula : eTipoCedula;
   iCuantos: Integer;
   lError : Boolean;
   sDatos, sTMuerto, sOpera, sHoraFinal : String;
   rPiezas, rTiempo{$ifdef LA_ADA},rConteo{$endif} : TPesos;
   FInserta, FLastCedula : TZetaCursor;

   procedure SetQryValoresComunes;
   begin
        with oZetaProvider do
        begin
             ParamAsInteger( FInserta, 'CE_STATUS', Ord( slCompleto ) );
             ParamAsBoolean( FInserta, 'CE_MULTI', FALSE );
        end;
   end;

   function GetFolioCedula( const iFolio: Integer ): Integer;
   begin
        if ( iFolio > 0 ) then
           Result := iFolio
        else
        begin
             with FLastCedula do
             begin
                  Active := TRUE;
                  if IsEmpty then
                     Result := 0
                  else
                      Result := FieldByName( 'RESULTADO' ).AsInteger;
                  Active := FALSE;
             end;
             Result := ( Result + 1 );    // Incrementa el Folio de la �ltima c�dula
        end;
   end;

   function GetUsuarioCedula( const iUsuario: Integer ): Integer;
   begin
        if ( iUsuario > 0 ) then
           Result := iUsuario
        else
            Result := oZetaProvider.UsuarioActivo;
   end;

begin
     InitImportLog;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
          AsignaParamList( Parametros );
          ImportarCedulasParametros;
          iCuantos := ObtieneListaASCII( ListaASCII, lError, sDatos );
          if OpenProcess( prLabImportarCedulas, iCuantos, FListaParametros ) then
          begin
               if lError then
                  Log.Error( 0, 'Error Al Leer Archivo De Importaci�n', sDatos )
               else
               begin
                    EmpiezaTransaccion;
                    try
                       try
                          FInserta := CreateQuery( GetSQLScript( Q_CEDULA_IMPORT_AGREGA ) );
                          FLastCedula := CreateQuery( GetSQLScript( Q_CEDULA_ULTIMA ) );
                          try
                             SetQryValoresComunes;
                             with cdsLista do
                             begin
                                  First;
                                  while ( not EOF ) do
                                  begin
                                       sDatos := FieldByName( 'RENGLON' ).AsString;
                                       if ( FieldByName( 'NUM_ERROR' ).AsInteger = 0 ) then
                                       begin
                                            try
                                               TipoCedula := eTipoCedula( FieldByName( 'CE_TIPO' ).AsInteger );
                                               sHoraFinal := FieldByName( 'CE_HORA' ).AsString;
                                               ParamAsInteger( FInserta, 'CE_FOLIO', GetFolioCedula( FieldByName( 'CE_FOLIO' ).AsInteger ) );
                                               ParamAsInteger( FInserta, 'CE_TIPO', Ord( TipoCedula ) );
                                               ParamAsDate( FInserta, 'CE_FECHA', FieldByName( 'CE_FECHA' ).AsDateTime );
                                               ParamAsChar( FInserta, 'CE_HORA', sHoraFinal, K_ANCHO_HORA );
                                               ParamAsChar( FInserta, 'CE_AREA', FieldByName( 'CE_AREA' ).AsString, K_ANCHO_CODIGO );
                                               {$ifdef INTERRUPTORES}
                                               ParamAsVarChar( FInserta, 'AR_CODIGO', FieldByName( 'AR_CODIGO' ).AsString, K_ANCHO_PARTE20 );
                                               {$else}
                                               ParamAsVarChar( FInserta, 'AR_CODIGO', FieldByName( 'AR_CODIGO' ).AsString, K_ANCHO_CODIGOPARTE );
                                               {$endif}
                                               ParamAsVarChar( FInserta, 'WO_NUMBER', FieldByName( 'WO_NUMBER' ).AsString, K_ANCHO_ORDEN );
                                               ParamAsChar( FInserta, 'CE_MOD_1', FieldByName( 'CE_MOD_1' ).AsString, K_ANCHO_CODIGO1 );
                                               ParamAsChar( FInserta, 'CE_MOD_2', FieldByName( 'CE_MOD_2' ).AsString, K_ANCHO_CODIGO1 );
                                               ParamAsChar( FInserta, 'CE_MOD_3', FieldByName( 'CE_MOD_3' ).AsString, K_ANCHO_CODIGO1 );
                                               ParamAsVarChar( FInserta, 'CE_COMENTA', FieldByName( 'CE_COMENTA' ).AsString, K_ANCHO_DESCLARGA );
                                               if ( TipoCedula in [ tcOperacion, tcEspeciales ] ) then
                                               begin
                                                    rPiezas := FieldByName( 'CE_PIEZAS' ).AsFloat;
                                                    sOpera := FieldByName( 'OP_NUMBER' ).AsString;
                                                    {$ifdef LA_ADA}
                                                    rConteo := FieldByName( 'CE_CONTEO' ).AsFloat;
                                                    {$endif}
                                                    sTMuerto := VACIO;
                                               end
                                               else
                                               begin
                                                    rPiezas := 0.0;
                                                    sTMuerto := FieldByName( 'OP_NUMBER' ).AsString;
                                                    {$ifdef LA_ADA}
                                                    rConteo := 0.0;
                                                    {$endif}
                                                    sOpera := VACIO;
                                               end;
                                               ParamAsFloat( FInserta, 'CE_PIEZAS', rPiezas );
                                               ParamAsChar( FInserta, 'CE_TMUERTO', sTMuerto, K_ANCHO_CODIGO3 );
                                               ParamAsVarChar( FInserta, 'OP_NUMBER', sOpera, K_ANCHO_OPERACION );
                                               {$ifdef LA_ADA}
                                               ParamAsFloat( FInserta, 'CE_CONTEO', rConteo );
                                               {$endif}
                                               if ( TipoCedula in [ tcEspeciales, tcTMuerto ] ) then
                                                  rTiempo := ( aMinutos( sHoraFinal ) - aMinutos( FieldByName( 'HORA_INIT' ).AsString ) )
                                               else
                                                   rTiempo := 0.0;
                                               ParamAsFloat( FInserta, 'CE_TIEMPO', rTiempo );
                                               ParamAsInteger( FInserta, 'US_CODIGO', GetUsuarioCedula( FieldByName( 'US_CODIGO' ).AsInteger ) );
                                               Ejecuta( FInserta );
                                            except
                                                  on Error: Exception do
                                                  begin
                                                       Log.Error( 0, 'Error al Registrar C�dula', ZetaServerTools.GetExceptionInfo( Error ) + CR_LF + sDatos );
                                                  end;
                                            end;
                                       end
                                       else
                                       begin
                                            Log.Error( 0, 'Registro Rechazado', FieldByName( 'DESC_ERROR' ).AsString + CR_LF + sDatos );
                                       end;
                                       Next;
                                  end;
                             end;
                          finally
                                 FreeAndNil( FLastCedula );
                                 FreeAndNil( FInserta );
                          end;
                       except
                             on Error: Exception do
                             begin
                                  Log.Excepcion( 0, 'Error Al Importar Datos', Error );
                             end;
                       end;
                    finally
                           TerminaTransaccion( True );
                    end;
               end;
               Result := CloseProcess;
          end;
     end;
     SetComplete;
end;

procedure TdmServerLabor.ImportarCedulasParametros;
begin
     ImportarCatalogosParametros;
     with oZetaProvider.ParamList do
     begin
          FListaParametros := FListaParametros +
                              K_PIPE + 'Formato Fechas: ' + ObtieneElemento( lfFormatoImpFecha, ParamByName( 'FormatoImpFecha' ).AsInteger );
     end;
end;

function TdmServerLabor.ObtieneListaASCII( ListaASCII: OleVariant; var lError: Boolean; var sError: String ): Integer;
const
     K_ANCHO_PIEZAS_CED = 13;
begin
     Result := 0;
     lError := FALSE;
     sError := VACIO;
     FillCatalogosCedula;
     try
        oSuperASCII := TdmSuperASCII.Create( Self );
        try
           try
              with oSuperASCII do
              begin
                   OnValidar := ImportarCedulasValidaASCII;
                   with oZetaProvider do
                   begin
                        with ParamList do
                        begin
                             Formato := eFormatoASCII( ParamByName( 'Formato' ).AsInteger );
                             FormatoImpFecha := eFormatoImpFecha ( ParamByName( 'FormatoImpFecha' ).AsInteger );
                        end;
                   end;
                   AgregaColumna( 'CE_TIPO', tgNumero, 1, TRUE );
                   AgregaColumna( 'CE_FECHA', tgFecha, K_ANCHO_FECHA, TRUE );
                   AgregaColumna( 'CE_HORA', tgTexto, K_ANCHO_HORA, TRUE );
                   AgregaColumna( 'CE_AREA', tgTexto, K_ANCHO_CODIGO, TRUE );
                   {$ifdef INTERRUPTORES}
                   AgregaColumna( 'AR_CODIGO', tgTexto, K_ANCHO_PARTE20, FALSE );
                   {$else}
                   AgregaColumna( 'AR_CODIGO', tgTexto, K_ANCHO_CODIGOPARTE, FALSE );
                   {$endif}
                   AgregaColumna( 'WO_NUMBER', tgTexto, K_ANCHO_ORDEN, FALSE );
                   AgregaColumna( 'CE_PIEZAS', tgFloat, K_ANCHO_PIEZAS_CED, FALSE, 0, TRUE, '0' ); // No se Requiere en T.Muerto
                   AgregaColumna( 'OP_NUMBER', tgTexto, K_ANCHO_OPERACION, FALSE );       // Si es T.Muerto se traspasar� a CE_TMUERTO
                   AgregaColumna( 'HORA_INIT', tgTexto, K_ANCHO_HORA, FALSE );                   // No se Requiere en Operaci�n
                   AgregaColumna( 'CE_MOD_1', tgTexto, K_ANCHO_CODIGO1, FALSE );
                   AgregaColumna( 'CE_MOD_2', tgTexto, K_ANCHO_CODIGO1, FALSE );
                   AgregaColumna( 'CE_MOD_3', tgTexto, K_ANCHO_CODIGO1, FALSE );
                   AgregaColumna( 'CE_COMENTA', tgTexto, K_ANCHO_DESCLARGA, FALSE );
                   AgregaColumna( 'CE_FOLIO', tgNumero, K_ANCHO_NUMEROEMPLEADO, FALSE );
                   AgregaColumna( 'US_CODIGO', tgNumero, K_ANCHO_USUARIO, FALSE );
                   with cdsLista do
                   begin
                        Lista := Procesa( ListaASCII );
                        Result := RecordCount - Errores;
                   end;
              end;
           except
                 on Error: Exception do
                 begin
                      lError := True;
                      sError := Error.Message;
                 end;
           end;
        finally
               FreeAndNil(oSuperASCII);
        end;
     finally
            ReleaseCatalogosCedula;
     end;
end;

procedure TdmServerLabor.ImportarCedulasValidaASCII(DataSet: TDataset; var iProblemas: Integer; var sErrorMsg: String );
var
   TipoCedula : eTipoCedula;
   sTemp : String;

   procedure Error( const sMensaje: String );
   begin
        Inc( iProblemas );
        sErrorMsg := sErrorMsg + '| ' + sMensaje + ' |';
   end;

   function EsHoraValida( var sHora: String ): Boolean;
   const
        K_TOPE_HORAS = '48';
        K_ERROR_HORA = 'Hora %s:%s Inv�lida';
   begin
        Result := ZetaCommonTools.ValidaHora( sHora, K_TOPE_HORAS );
        if ( not Result ) then
           Error( Format( K_ERROR_HORA, [ Copy( sHora, 1, 2 ), Copy( sHora, 3, 2 ) ] ) );
   end;

   function ValidaCatalogoCedula( oDataSet: TDataSet; const sCodigo: String; const iGlobal: Integer ): Boolean;
   const
        K_MESS_NO_EXISTE = '%s %s No Existe';
   begin
        Result := oDataSet.Locate( 'TB_CODIGO', sCodigo, [] );
        if ( not Result ) then
           Error ( Format( K_MESS_NO_EXISTE, [ oZetaProvider.GetGlobalString( iGlobal ), sCodigo ] ) );
   end;

   function ValidaCodigoVacio( const sCodigo: String; const iGlobal: Integer ): Boolean;
   const
        K_MESS_NO_CODIGO = 'Falta C�digo de %s';
   var
      sCatalogo : String;
   begin
        sCatalogo := oZetaProvider.GetGlobalString( iGlobal );
        Result := strLleno( sCodigo ) or StrVacio( sCatalogo );
        if ( not Result ) then     // Se requiere el C�digo y no viene
           Error( Format( K_MESS_NO_CODIGO, [ sCatalogo ] ) );
   end;

begin
     with Dataset do
     begin
          // Validar Tipo de C�dula
          TipoCedula := eTipoCedula( FieldByName( 'CE_TIPO' ).AsInteger );
          if ( not ( TipoCedula in [ Low( eTipoCedula )..High( eTipoCedula ) ] ) ) then
             Error( 'Tipo de C�dula Inv�lido' );

          // Validar Formato de Hora Final
          sTemp := FieldByName( 'CE_HORA' ).AsString;
          if EsHoraValida( sTemp ) then
             FieldByName( 'CE_HORA' ).AsString := sTemp;

          // Validar C�digo de Area
          sTemp := AnsiUpperCase( FieldByName( 'CE_AREA' ).AsString );
          if ValidaCatalogoCedula( cdsAdicional, sTemp, K_GLOBAL_LABOR_AREA ) then
             FieldByName( 'CE_AREA' ).AsString := sTemp;

          // Validaciones Exclusivas de Producci�n y Especiales
          if ( TipoCedula in [ tcOperacion, tcEspeciales ] ) then
          begin
               ValidaCodigoVacio( FieldByName( 'AR_CODIGO' ).AsString, K_GLOBAL_LABOR_PARTE );
               ValidaCodigoVacio( FieldByName( 'WO_NUMBER' ).AsString, K_GLOBAL_LABOR_ORDEN );
               ValidaCodigoVacio( FieldByName( 'OP_NUMBER' ).AsString, K_GLOBAL_LABOR_OPERACION );
          end;

          // Validaciones exclusivas de Especiales � Tiempo Muerto
          if ( TipoCedula in [ tcEspeciales, tcTMuerto ] ) then
          begin
               // Validar Formato de Hora Inicial
               sTemp := FieldByName( 'HORA_INIT' ).AsString;
               if EsHoraValida( sTemp ) then
                  FieldByName( 'HORA_INIT' ).AsString := sTemp;

               if ( TipoCedula = tcTMuerto ) then
               begin
                    // Validar Motivo de Tiempo Muerto
                    sTemp := AnsiUpperCase( Copy( FieldByName( 'OP_NUMBER' ).AsString, 1, K_ANCHO_CODIGO3 ) );
                    if ValidaCodigoVacio( sTemp, K_GLOBAL_LABOR_TMUERTO ) and
                       ValidaCatalogoCedula( cdsAdicional2, sTemp, K_GLOBAL_LABOR_TMUERTO ) then
                       FieldByName( 'OP_NUMBER' ).AsString := sTemp;
               end;
          end;

          // Convierte a Mayusculas el Resto de los C�digos que no se Validan vs. Catalogos de Labor
          FieldByName( 'AR_CODIGO' ).AsString := AnsiUpperCase( FieldByName( 'AR_CODIGO' ).AsString );
          FieldByName( 'WO_NUMBER' ).AsString := AnsiUpperCase( FieldByName( 'WO_NUMBER' ).AsString );
          FieldByName( 'OP_NUMBER' ).AsString := AnsiUpperCase( FieldByName( 'OP_NUMBER' ).AsString );  // No afecta que vuelva a invocarlo cuando sea T. Muerto
          FieldByName( 'CE_MOD_1' ).AsString := AnsiUpperCase( FieldByName( 'CE_MOD_1' ).AsString );
          FieldByName( 'CE_MOD_2' ).AsString := AnsiUpperCase( FieldByName( 'CE_MOD_2' ).AsString );
          FieldByName( 'CE_MOD_3' ).AsString := AnsiUpperCase( FieldByName( 'CE_MOD_3' ).AsString );
     end;
end;

procedure TdmServerLabor.FillCatalogosCedula;
begin
     with oZetaProvider do
     begin
          cdsAdicional.Lista := Self.GetCatalogo( EmpresaActiva, Ord( eArea ) );
          cdsAdicional2.Lista := Self.GetCatalogo( EmpresaActiva, Ord( eTiempoMuerto ) );
     end;
end;

procedure TdmServerLabor.ReleaseCatalogosCedula;
begin
     cdsAdicional.InitTempDataset;    // Libera los registros que tenga
     cdsAdicional2.InitTempDataset;    // Libera los registros que tenga
end;

function TdmServerLabor.GetKardexDiario(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime): OleVariant;
begin
     InitCreator;
     FLaborCalculo := TLCalculo.Create( oZetaCreator );
     try
        oZetaProvider.EmpresaActiva := Empresa;
        Result := FLaborCalculo.GetAreasDiariasDataset( Empleado, Fecha );
     finally
            FreeAndNil( FLaborCalculo );
     end;
     SetComplete;
end;

function TdmServerLabor.GetCertificaciones(Empresa: OleVariant;
  const Maquina: WideString): OleVariant;
begin
     SetTablaInfo( eMaqCert );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format ('MQ_CODIGO = ''%s''',[Maquina]);
          Result := GetTabla( Empresa );
          SetComplete;
     end;
end;

function TdmServerLabor.GrabaCertificaciones(Empresa, Delta: OleVariant;
  const Maquina: WideString ; out ErrorCount: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          if not VarIsNull( Delta ) then
          begin
               SetTablaInfo( eMaqCert );
               Result := GrabaTablaGrid( Empresa, Delta, ErrorCount );
          end;
     end;
end;

function TdmServerLabor.GetMapa(Empresa: OleVariant;const Layout: WideString;out oSillas: OleVariant;out oMaqSilla: OleVariant;out oCertMaq: OleVariant): OleVariant;
var
   FDataSet:TClientDataSet;
   Filtro:string;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          SetTablaInfo( eLayMap );
          TablaInfo.Filtro := Format ('LY_CODIGO = ''%s''',[Layout]);
          Result := GetTabla( Empresa );

          SetTablaInfo( eSillasLay );
          TablaInfo.Filtro := Format ('LY_CODIGO = ''%s''',[Layout]);
          oSillas := GetTabla( Empresa );

          SetTablaInfo( eSillasMaq );
          TablaInfo.Filtro := Format ('LY_CODIGO = ''%s''',[Layout]);
          oMaqSilla := GetTabla( Empresa );

          FDataSet := TClientDataSet.Create(Self);
          try
             with FDataSet do
             begin
                  Data := oMaqSilla;
                  First;
                  While not eof do
                  begin
                       Filtro := ConcatString(Filtro,''''+FieldByName('MQ_CODIGO').AsString+'''',',');
                       Next;
                  end;
             end;
          finally
                 FreeAndNil(FDataSet);
          end;

          SetTablaInfo( eMaqCert );
          if StrLleno(Filtro) then
             TablaInfo.Filtro := Format ('MQ_CODIGO in ( %s )',[Filtro]);
          oCertMaq := GetTabla( Empresa );

     end;
     SetComplete;
end;

function TdmServerLabor.GetLayoutsSuper(Empresa: OleVariant;Usuario: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Result := OpenSQL( Empresa, Format( GetSQLScript( Q_GET_LAYOUTS_SUPER ),[Usuario] ), True );
     end;
     SetComplete;
end;

function TdmServerLabor.AsignaEmpleado(Empresa,oZetaParams: OleVariant): OleVariant;
var
   FInserta:TZetaCursor;
begin
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             EmpiezaTransaccion;
             try
                 AsignaParamList( oZetaParams );
                 FInserta := CreateQuery( GetSQLScript( Q_ASIGNA_EMPLEADO ) );
                 ParamAsInteger( FInserta, 'CB_CODIGO',  ParamList.ParamByName( 'CB_CODIGO' ).AsInteger);
                 ParamAsString( FInserta, 'SL_CODIGO',  ParamList.ParamByName( 'SL_CODIGO' ).AsString);
                 ParamAsString( FInserta, 'LY_CODIGO', ParamList.ParamByName( 'LY_CODIGO' ).AsString );
                 ParamAsDate( FInserta, 'EM_FECHA', ParamList.ParamByName( 'EM_FECHA' ).AsDateTime );
                 ParamAsChar( FInserta, 'EM_HORA', ParamList.ParamByName( 'EM_HORA' ).AsString, K_ANCHO_HORA );
                 ParamAsChar( FInserta, 'EM_HOR_MOV', ParamList.ParamByName( 'EM_HOR_MOV' ).AsString, {$ifdef DOS_CAPAS}K_ANCHO_CODIGO{$else}K_ANCHO_HORA{$endif} );
                 ParamAsDate( FInserta, 'EM_FEC_MOV', ParamList.ParamByName( 'EM_FEC_MOV' ).AsDateTime );
                 ParamAsInteger( FInserta, 'US_CODIGO', ParamList.ParamByName( 'US_CODIGO' ).AsInteger );
                 Ejecuta(FInserta);
             finally
                    TerminaTransaccion( True );
             end;
        end;
     finally
            FreeAndNil(FInserta);
     end;
end;

function TdmServerLabor.GetMaquinaEmpleados(Empresa: OleVariant;const Layout: WideString;const Hora: WideString; Fecha: TDateTime): OleVariant;
var
   dFecha: TDate;   // Validar Tope de 24 horas
   sHora: String;   // Validar Tope de 24 horas
   oDataSet: TClientDataSet;
   FDataSet: TZetaCursor;
begin
     oDataSet := TClientDataSet.Create(nil);

     with oDataSet do
     begin
          Data:= oZetaProvider.OpenSQL( Empresa, Format( GetSQLScript( Q_EMPLEADOS_LAYOUT ), [ Layout ]  ), TRUE );
          LogChanges := FALSE;
     end;

     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          // Asigna Fecha y Hora para Validar Tope 24 horas
          dFecha := Fecha;
          sHora  := Hora;
          ZetaServerTools.RevisaHorasTope24( dFecha, sHora );

          with oDataSet do
          begin
               FDataSet := Nil;
               try
                   while not eof do
                   begin
                        FDataSet:= CreateQuery( Format( GetSQLScript( Q_KAR_EMP_MAQ  ), [FieldByName('CB_CODIGO').AsInteger,DateToStrSQL(Fecha),Layout,Hora] ) );
                        FDataSet.Active := True;
                        if FDataSet.RecordCount > 0 then
                        begin
                             Edit;
                             FieldByName('SILLA').AsString := FDataSet.FieldByName('SL_CODIGO').AsString;
                             FieldByName('HORA').AsString := FDataSet.FieldByName('EM_HORA').AsString;
                             //FieldByName('EM_FECHA').AsDateTime := FDataSet.FieldByName('EM_FECHA').AsDateTime;
                             //FieldByName('US_CODIGO').AsInteger := FDataSet.FieldByName('US_CODIGO').AsInteger;
                             Post;
                        end;
                        FDataSet.Active := False;
                        Next;
                   end;
               finally
                 FreeAndNil(FDataSet);
               end;
          end;
     end;
     Result := oDataSet.Data;
     FreeAndNil(oDataSet);
     SetComplete;
end;

function TdmServerLabor.GetCertificacionesEmpleados(Empresa: OleVariant; Usuario: Integer; Fecha: TDateTime): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Result := OpenSQL( Empresa, Format( GetSQLScript( Q_GET_CERTIFIC_EMPLEADOS ),[Usuario,DateToStrSQL( Fecha ) ] ), True );
     end;
     SetComplete;
end;

function TdmServerLabor.GetKardexEmpleadoMaquina(Empresa: OleVariant; Empleado: Integer): OleVariant;
begin
     SetTablaInfo( eKarEmpMaq );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format ('CB_CODIGO = %d',[Empleado]);
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerLabor.GetKardexMaquinaEmpleado(Empresa: OleVariant; const Maquina: WideString): OleVariant;
begin
     SetTablaInfo( eKarEmpMaq );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format ('SL_CODIGO = ''%s''',[Maquina]);
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

procedure TdmServerLabor.BeforeUpdateMaquinas(Sender: TObject;SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;UpdateKind: TUpdateKind; var Applied: Boolean);
var
   FDataSet: TZetaCursor;
   sMaquina:string;
begin
     with oZetaProvider do
     begin
          if ( UpdateKind = ukDelete ) then
          begin
               sMaquina := DeltaDS.FieldByName('MQ_CODIGO').AsString ;
               FDataSet:= CreateQuery( Format( GetSQLScript( Q_MAQUINA_USO ), [ sMaquina ] ) );
               try
                  with FDataSet do
                  begin
                       Active := True;
                       if not FDataSet.IsEmpty then
                          DatabaseError(Format('M�quina no puede ser borrada: %s esta en uso en el Layout : %s',[ CampoAsVar( DeltaDS.FieldByName('MQ_CODIGO') ),FieldByName('LY_CODIGO').AsString ] ) );
                       Active := False;
                  end;
               finally
                      FreeAndNil( FDataSet );
               end;
          end;
     end;
end;

{$ifndef DOS_CAPAS}

initialization
  TComponentFactory.Create(ComServer, TdmServerLabor, Class_dmServerLabor, ciMultiInstance, ZetaServerTools.GetThreadingModel);
{$endif}

end.

