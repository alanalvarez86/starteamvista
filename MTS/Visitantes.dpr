library Visitantes;

uses
  ComServ,
  Visitantes_TLB in 'Visitantes_TLB.pas',
  DServerVisitantes in 'DServerVisitantes.pas' {dmServerVisitantes: TMtsDataModule} {dmSeverVisitantes: CoClass},
  ZCreator in '..\VisitantesMGR\ZCreator.pas',
  EditorDResumen in '..\Medicos\EditorDResumen.pas';

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
