object dmServerEvaluacion: TdmServerEvaluacion
  OldCreateOrder = False
  OnCreate = MtsDataModuleCreate
  OnDestroy = MtsDataModuleDestroy
  Pooled = False
  Height = 38
  Width = 132
  object cdsAgregarCriterios: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 50
    Top = 24
  end
  object cdsAgregarEvaluador: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 50
    Top = 80
  end
  object cdsLista: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 162
    Top = 24
  end
  object cdsSujetos: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 252
    Top = 24
  end
  object cdsListaUsuarios: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 162
    Top = 80
  end
  object cdsDatos: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 162
    Top = 144
  end
  object cdsCorreos: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 50
    Top = 144
  end
end
