unit DServerReporting;

{$ifdef VER150}
        {$WARN SYMBOL_PLATFORM OFF}
{$ENDIF}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComServ, ComObj, VCLCom, StdVcl, bdemts, DataBkr, DBClient,
  MtsRdm, Mtx,
  ZetaServerTools,
  {$ifndef DOS_CAPAS}
  Reporteador_TLB,
  {$endif}
  DZetaServerProvider,
  ZCreator, DB, ZetaServerDataSet ;

type
  TdmServerReporting = class(TMtsDataModule {$ifndef DOS_CAPAS}, IdmServerReporting {$endif})
    cdsTemporal: TServerDataSet;
    procedure MtsDataModuleCreate(Sender: TObject);
    procedure MtsDataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
{$ifndef DOS_CAPAS}
    oZetaCreator: TZetaCreator;
{$endif}
    procedure InitCreator;
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
{$ifdef DOS_CAPAS}
  public
    procedure CierraEmpresa;
{$endif}
    { Public declarations }
    function GetFoto(Empresa: OleVariant; Empleado: Integer;const Tipo: WideString): OleVariant;  {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetDocumento(Empresa: OleVariant; Empleado: Integer; const Tipo: WideString; var Extension: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GeneraSQL(Empresa: OleVariant; var AgenteSQL,Parametros: OleVariant; out Error: WideString): OleVariant;  {$ifndef DOS_CAPAS} safecall; {$endif}
    function EvaluaParam(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant; out Error: WideString): WordBool; {$ifndef DOS_CAPAS} safecall; {$endif}
  end;

var
  dmServerReporting: TdmServerReporting;

implementation
uses
    ZetaSQLBroker,
    DServerReportesIBO;

{$R *.DFM}

class procedure TdmServerReporting.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

procedure TdmServerReporting.MtsDataModuleCreate(Sender: TObject);
begin
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
end;

procedure TdmServerReporting.MtsDataModuleDestroy(Sender: TObject);
begin
     ZetaSQLBroker.FreeZetaCreator( oZetaCreator );
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
end;

{$ifdef DOS_CAPAS}
procedure TdmServerReporting.CierraEmpresa;
begin
end;
{$endif}

procedure TdmServerReporting.InitCreator;
begin
     if not Assigned( oZetaCreator ) then
        oZetaCreator := ZetaSQLBroker.GetZetaCreator( oZetaProvider );
end;


function TdmServerReporting.GeneraSQL(Empresa: OleVariant; var AgenteSQL,
  Parametros: OleVariant; out Error: WideString): OleVariant;
var
   Reporte: TdmServerReportes;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
     end;
     InitCreator;
     Reporte := TdmServerReportes.Create( oZetaProvider, oZetaCreator );
     try
        Result := Reporte.GeneraSQL( Empresa,
                                     AgenteSQL,
                                     Parametros,
                                     Error );
     finally
            oZetaProvider.VerConfidencial := TRUE;
            Reporte.Free;
            {$ifdef DOS_CAPAS}
            FreeAll( FALSE );
            {$endif}
     end;
     SetComplete;
end;

function TdmServerReporting.EvaluaParam(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant;  out Error: WideString): WordBool;
var
   Reporte: TdmServerReportes;
begin
     InitCreator;
     Reporte := TdmServerReportes.Create( oZetaProvider, oZetaCreator );
     try
        Result := Reporte.EvaluaParam( Empresa,
                                       AgenteSQL,
                                       Parametros,
                                       Error );
     finally
            Reporte.Free;
     end;
     SetComplete;
end;

function TdmServerReporting.GetFoto(Empresa: OleVariant; Empleado: Integer; const Tipo: WideString): OleVariant;
  const K_SQL = 'SELECT IM_BLOB FROM IMAGEN WHERE CB_CODIGO = %d AND IM_TIPO = ''%s''';
 var
    FBlob : TBlobField;
    FImagen : TZetaCursor;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     FImagen := oZetaProvider.CreateQuery;

     oZetaProvider.AbreQueryScript( FImagen , Format( K_SQL, [ Empleado, Tipo ] ) );

     //Result := FImagen.Fields[0].AsVariant;

     with cdsTemporal do
     begin
          InitTempDataset;
          AddBlobField('CampoBlob', 0 );
          CreateTempDataset;
          FBlob := TBlobField( FieldByName('CampoBlob') );
     end;

     FBlob.DataSet.Edit;
     FBlob.Value := FImagen.Fields[0].Value;
     FBlob.DataSet.Post;

     Result :=  TClientDataset(Fblob.Dataset).Data;
end;

// (JB) Anexar al expediente del trabajador documentos CR1880 T1107
function TdmServerReporting.GetDocumento(Empresa: OleVariant; Empleado: Integer; const Tipo: WideString; var Extension: WideString): OleVariant;
const
     K_SQL = 'SELECT DO_BLOB, DO_EXT FROM DOCUMENTO WHERE CB_CODIGO = %d AND DO_TIPO = ''%s''';
var
     FBlob : TBlobField;
     FImagen : TZetaCursor;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     FImagen := oZetaProvider.CreateQuery;

     oZetaProvider.AbreQueryScript( FImagen , Format( K_SQL, [ Empleado, Tipo ] ) );

     with cdsTemporal do
     begin
          InitTempDataset;
          AddBlobField('DO_BLOB', 0 );
          CreateTempDataset;
          FBlob := TBlobField( FieldByName('DO_BLOB') );
     end;

     Extension := FImagen.Fields[1].AsString;

     FBlob.DataSet.Edit;
     FBlob.Value := FImagen.Fields[0].Value;
     FBlob.DataSet.Post;

     Result :=  TClientDataset(Fblob.Dataset).Data;
end;


{$ifndef DOS_CAPAS}
initialization
  TComponentFactory.Create(ComServer, TdmServerReporting, Class_dmServerReporting, ciMultiInstance, ZetaServerTools.GetThreadingModel);
{$endif}
end.