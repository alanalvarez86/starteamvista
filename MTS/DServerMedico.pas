unit DServerMedico;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ComServ, ComObj, VCLCom,
  StdVcl, {BdeMts,} DataBkr, DBClient, MtsRdm, Mtx, db,
  Variants,
  {$ifndef DOS_CAPAS}
  ServerMedico_TLB,
  {$endif}
  ZetaCommonLists,
  {$ifdef FALSE}
  ZetaSQLBroker,
  FAutoServer,
  FAutoClasses,
  {$endif}
  DZetaServerprovider, ZetaServerDataSet;

type
  // MA: Numerado
  eTipoTabla = (eTConsulta, eTEstudio, eTAcciden, eCausacci, eDiagnostico, eMedicina, eExpedien,
    eConsulta, eEmbarazo, eAccidente, eMedicinaEntr, eGruposEx, eCamposEx, eIncapacidades,
    ePermisos, eConsultaGlobal, eMotivoAcci);

  eAllScripts = (eGetExpediente, eExpedienteAnterior, eExpedienteSiguiente, eHacerBusqueda,
    eGetCodigos, eMaxExpediente, eRevisaDuplicado, eConsultas, eEmpleado, eHistorial, eDeleteGrupos,
    eDeleteCampos, eGetConsGlobal, eDiccionario, eGetStatusEmb, eGetExpedienteStatus,
    eValidaStatusEmb, eActualizaExpedien, eObtenCamposExpedien, eExisteExpediente,{ OP: 04/06/08 }
    eExpedienteSiguienteConf, eExpedienteAnteriorConf);

  TdmServerMedico = class(TMtsDataModule{$ifndef DOS_CAPAS}, IdmServerMedico{$endif})
    cdsTemporal: TServerDataSet;
    procedure MtsDataModuleCreate(Sender: TObject);
    procedure MtsDataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
{$ifdef FALSE}
    {$ifndef DOS_CAPAS}
    oAutoServer: TAutoServer;
    {$endif}
{$endif}
    FExpediente  : Integer;
    FFiltroNivel0: string;
    oZetaProvider: TdmZetaServerProvider;
    procedure SetTablaInfo(const eTabla: eTipoTabla);
    function GetNombreTabla(const eTabla: eTipoTabla): string;
    function GetScript(const eScript: eAllScripts): string;
    function LeeConsulta(Empresa: OleVariant; const iExpediente: Integer): OleVariant;
    function LeeAccidente(Empresa: OleVariant; const iExpediente: Integer): OleVariant;
    function LeeEmbarazo(Empresa: OleVariant; const iExpediente: Integer): OleVariant;
    function LeeMedicinaEntr(Empresa: OleVariant; const iExpediente: Integer): OleVariant;
    function GetMaxExpediente: Integer;
    function GeneraCastDate: string;
    procedure BeforeUpdateExpediente(Sender: TObject; SourceDS: TDataSet;  DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateConsultaGlobal(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateEmbarazo(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    function GetTodosCamposExpediente: string; { OP: 04/06/08 }
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    {$ifdef DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
    {$endif}
    function GetTabla(Empresa: OleVariant; iTabla: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaTabla(Empresa: OleVariant; iTabla: Integer; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetKardexConsulta(Empresa: OleVariant; iExpediente: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetKardexEmbarazo(Empresa: OleVariant; iExpediente: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetKardexAccidente(Empresa: OleVariant; iExpediente: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetMedicinaEntr(Empresa: OleVariant; iExpediente: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaGruposEx(Empresa, oDelta, oCamposEx: OleVariant; var oCamposExResult: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetGruposEx(Empresa: OleVariant; var oCamposEx: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetExpediente(Empresa: OleVariant; iExpediente: Integer; Parametros: OleVariant; var oConsulta: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetHistorial(Empresa: OleVariant; iEmpleado: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetSoloExpediente(Empresa: OleVariant; iExpediente: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaMedicinaEntr(Empresa, oDelta: OleVariant; var ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaKardexEmbarazo(Empresa, oDelta: OleVariant; var ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabakardexConsultas(Empresa, oDelta: OleVariant; var ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaKardexAccidente(Empresa, oDelta: OleVariant; var ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaExpediente(Empresa, oDelta: OleVariant; var ErrorCount, iExpediente: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetDatosEmpleado(Empresa: OleVariant; Empleado: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function ExpedienteActivo(Empresa: OleVariant; iExpediente: Integer; out oDelta: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function ExpedienteAnterior(Empresa: OleVariant; iExpediente: Integer; out oDelta: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function ExpedienteSiguiente(Empresa: OleVariant; iExpediente: Integer; out oDelta: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function BuscarEnTablas(Empresa, oParams: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCodigos(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetConsultaGlobal(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaConsultaGlobal(Empresa, oDelta, oParams: OleVariant; var ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
  end;

const
     {$ifdef INTERBASE}
     PRETTY_EXP = 'EX_APE_PAT||'' ''||EX_APE_MAT||'', ''||EX_NOMBRES';
     {$endif}
     {$ifdef MSSQL}
     PRETTY_EXP = 'EX_APE_PAT+'' ''+EX_APE_MAT+'', ''+EX_NOMBRES';
     {$endif}

var
   dmServerMedico: TdmServerMedico;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses,
     ZetaServerTools;

const
     PRETTYNAME_EXP = PRETTY_EXP + ' PRETTYNAME';

     K_CAMPOS_EXPEDIEN = 'select E.EX_CODIGO,E.EX_TIPO,E.EX_SEXO,E.CB_CODIGO, ' +
       'C.CB_ACTIVO, C.CB_FEC_ING, C.CB_FEC_BAJ, ' + PRETTYNAME_EXP;
     K_FILTRO_EMPLEADO = '( EX_CODIGO = %d )';

     K_TODOS_CAMPOS_EXPEDIEN = ' EX_CODIGO,EX_TIPO,EX_NOMBRES,EX_APE_PAT,EX_APE_MAT,EX_SEXO,EX_FEC_NAC,EX_CALLE,EX_COLONIA,' +
                               ' EX_CIUDAD,EX_CODPOST,EX_ESTADO,EX_TEL,CB_CODIGO,EX_FEC_INI,EX_OBSERVA,US_CODIGO, EX_NUM_EXT, EX_NUM_INT,' +
                               ' EX_TSANGRE,EX_ALERGIA';
     {
       ' EX_G_TEX01,EX_G_TEX02,EX_G_TEX03,EX_G_TEX04,EX_G_TEX05,EX_G_TEX06,EX_G_TEX07,EX_G_TEX08,EX_G_TEX09,EX_G_TEX10,'+
       ' EX_G_TEX11,EX_G_TEX12,EX_G_TEX13,EX_G_TEX14,EX_G_TEX15,EX_G_TEX16,EX_G_TEX17,EX_G_TEX18,EX_G_TEX19,EX_G_TEX20,'+
       ' EX_G_TEX21,EX_G_TEX22,EX_G_TEX23,EX_G_TEX24,EX_G_TEX25,EX_G_TEX26,EX_G_TEX27,EX_G_TEX28,EX_G_TEX29,EX_G_TEX30,'+

       ' EX_G_LOG01,EX_G_LOG02,EX_G_LOG03,EX_G_LOG04,EX_G_LOG05,EX_G_LOG06,EX_G_LOG07,EX_G_LOG08,EX_G_LOG09,EX_G_LOG10,'+
       ' EX_G_LOG11,EX_G_LOG12,EX_G_LOG13,EX_G_LOG14,EX_G_LOG15,EX_G_LOG16,EX_G_LOG17,EX_G_LOG18,EX_G_LOG19,EX_G_LOG20,'+
       ' EX_G_LOG21,EX_G_LOG22,EX_G_LOG23,EX_G_LOG24,EX_G_LOG25,EX_G_LOG26,EX_G_LOG27,EX_G_LOG28,EX_G_LOG29,EX_G_LOG30,'+
       ' EX_G_LOG31,EX_G_LOG32,EX_G_LOG33,EX_G_LOG34,EX_G_LOG35,EX_G_LOG36,EX_G_LOG37,EX_G_LOG38,EX_G_LOG39,EX_G_LOG40,'+
       ' EX_G_LOG41,EX_G_LOG42,EX_G_LOG43,EX_G_LOG44,EX_G_LOG45,EX_G_LOG46,EX_G_LOG47,EX_G_LOG48,EX_G_LOG49,EX_G_LOG50,'+
       ' EX_G_LOG51,EX_G_LOG52,EX_G_LOG53,EX_G_LOG54,EX_G_LOG55,EX_G_LOG56,EX_G_LOG57,EX_G_LOG58,EX_G_LOG59,EX_G_LOG60,'+

       ' EX_G_NUM01,EX_G_NUM02,EX_G_NUM03,EX_G_NUM04,EX_G_NUM05,EX_G_NUM06,EX_G_NUM07,EX_G_NUM08,EX_G_NUM09,EX_G_NUM10,'+
       ' EX_G_NUM11,EX_G_NUM12,EX_G_NUM13,EX_G_NUM14,EX_G_NUM15,EX_G_NUM16,EX_G_NUM17,EX_G_NUM18,EX_G_NUM19,EX_G_NUM20,'+
       ' EX_G_NUM21,EX_G_NUM22,EX_G_NUM23,EX_G_NUM24,EX_G_NUM25,EX_G_NUM26,EX_G_NUM27,EX_G_NUM28,EX_G_NUM29,EX_G_NUM30,'+

       ' EX_G_BOL01,EX_G_BOL02,EX_G_BOL03,EX_G_BOL04,EX_G_BOL05,EX_G_BOL06,EX_G_BOL07,EX_G_BOL08,EX_G_BOL09,EX_G_BOL10,'+
       ' EX_G_BOL11,EX_G_BOL12,EX_G_BOL13,EX_G_BOL14,EX_G_BOL15,EX_G_BOL16,EX_G_BOL17,EX_G_BOL18,EX_G_BOL19,EX_G_BOL20,'+
       ' EX_G_BOL21,EX_G_BOL22,EX_G_BOL23,EX_G_BOL24,EX_G_BOL25,EX_G_BOL26,EX_G_BOL27,EX_G_BOL28,EX_G_BOL29,EX_G_BOL30,'+
       ' EX_G_BOL31,EX_G_BOL32,EX_G_BOL33,EX_G_BOL34,EX_G_BOL35,EX_G_BOL36,EX_G_BOL37,EX_G_BOL38,EX_G_BOL39,EX_G_BOL40,'+
       ' EX_G_BOL41,EX_G_BOL42,EX_G_BOL43,EX_G_BOL44,EX_G_BOL45,EX_G_BOL46,EX_G_BOL47,EX_G_BOL48,EX_G_BOL49,EX_G_BOL50,'+
       ' EX_G_BOL51,EX_G_BOL52,EX_G_BOL53,EX_G_BOL54,EX_G_BOL55,EX_G_BOL56,EX_G_BOL57,EX_G_BOL58,EX_G_BOL59,EX_G_BOL60,'+

       ' EX_G_DES01,EX_G_DES02,EX_G_DES03,EX_G_DES04,EX_G_DES05,EX_G_DES06,EX_G_DES07,EX_G_DES08,EX_G_DES09,EX_G_DES10,'+
       ' EX_G_DES11,EX_G_DES12,EX_G_DES13,EX_G_DES14,EX_G_DES15,EX_G_DES16,EX_G_DES17,EX_G_DES18,EX_G_DES19,EX_G_DES20,'+
       ' EX_G_DES21,EX_G_DES22,EX_G_DES23,EX_G_DES24,EX_G_DES25,EX_G_DES26,EX_G_DES27,EX_G_DES28,EX_G_DES29,EX_G_DES30,'+
       ' EX_G_DES31,EX_G_DES32,EX_G_DES33,EX_G_DES34,EX_G_DES35,EX_G_DES36,EX_G_DES37,EX_G_DES38,EX_G_DES39,EX_G_DES40,'+
       ' EX_G_DES41,EX_G_DES42,EX_G_DES43,EX_G_DES44,EX_G_DES45,EX_G_DES46,EX_G_DES47,EX_G_DES48,EX_G_DES49,EX_G_DES50,'+
       ' EX_G_DES51,EX_G_DES52,EX_G_DES53,EX_G_DES54,EX_G_DES55,EX_G_DES56,EX_G_DES57,EX_G_DES58,EX_G_DES59,EX_G_DES60';
     }
     K_FILTRO_NIVEL0 = // Filtro para Expedientes de Empleados y Parientes
       'and ( ( E.EX_TIPO in (0,2) and %s ) ' +
     // Filtro para Expedientes de Candidatos y 'Otros'
       ' or NOT ( E.EX_TIPO in (0,2) ) ) ';
  {$R *.DFM}

  { OP: 04/06/08 }
function TdmServerMedico.GetTodosCamposExpediente: string;
var
   sAdicionales: string;
   i : Integer;
begin
     sAdicionales := VACIO;
     cdsTemporal.Data := oZetaProvider.OpenSQL(oZetaProvider.EmpresaActiva, GetScript(eObtenCamposExpedien), FALSE);
     for i := 0 to cdsTemporal.FieldCount - 1 do
         if Copy(cdsTemporal.Fields[i].FieldName, 1, 5) = 'EX_G_' then
            sAdicionales := ConcatString(sAdicionales, cdsTemporal.Fields[i].FieldName, ',');
     if StrLleno(sAdicionales) then
        sAdicionales := ',' + sAdicionales;
     Result := K_TODOS_CAMPOS_EXPEDIEN + sAdicionales;
end;

class procedure TdmServerMedico.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
     if Register then
     begin
          inherited UpdateRegistry(Register, ClassID, ProgID);
          EnableSocketTransport(ClassID);
          EnableWebTransport(ClassID);
     end
     else
     begin
          DisableSocketTransport(ClassID);
          DisableWebTransport(ClassID);
          inherited UpdateRegistry(Register, ClassID, ProgID);
     end;
end;

procedure TdmServerMedico.MtsDataModuleCreate(Sender: TObject);
begin
     oZetaProvider := DZetaServerprovider.GetZetaProvider(Self);
     {$ifdef FALSE}
     oAutoServer := ZetaSQLBroker.GetAutoServer;
     ZetaSQLBroker.ReadAutoServer(oAutoServer, nil, nil);
     {$endif}
     FFiltroNivel0 := // Filtro para Expedientes de Empleados y Parientes
                      Format('and ( ( %0:s.EX_TIPO in (%1:d,%2:d) and %3:s ) ' +
                      // Filtro para Expedientes de Candidatos y 'Otros'
                      ' or NOT ( %0:s.EX_TIPO in (%1:d,%2:d) ) ) ',
                      ['%0:s', ord(exEmpleado), ord(exPariente), '%1:s']);
end;

procedure TdmServerMedico.MtsDataModuleDestroy(Sender: TObject);
begin
     {$ifdef FALSE}
     ZetaSQLBroker.FreeAutoServer(oAutoServer);
     {$endif}
     DZetaServerprovider.FreeZetaProvider(oZetaProvider);
end;

procedure TdmServerMedico.SetTablaInfo(const eTabla: eTipoTabla);
begin
     with oZetaProvider.TablaInfo do
     begin
          case eTabla of
            eDiagnostico: SetInfo('DIAGNOST', 'DA_CODIGO,DA_NOMBRE,DA_TIPO,DA_INGLES,DA_NUMERO,DA_TEXTO', 'DA_CODIGO');
            eMedicina: SetInfo('MEDICINA', 'ME_CODIGO,ME_NOMBRE,ME_TIPO,ME_INGLES,ME_NUMERO,ME_TEXTO,ME_MEDIDA,ME_DESCRIP', 'ME_CODIGO');
            eExpedien: SetInfo('EXPEDIEN', GetTodosCamposExpediente, 'EX_CODIGO'); { OP: 04/06/08 }
            eConsulta: SetInfo('CONSULTA', 'EX_CODIGO,CN_FECHA,CN_HOR_INI,DA_CODIGO,CN_TIPO,CN_MOTIVO,CN_SINTOMA,CN_EXPLORA,CN_OBSERVA,CN_DIAGNOS,CN_PULSO,CN_RESPIRA,CN_PESO,' +
                                           'CN_TEMPERA,CN_ALTURA,CN_PRE_SIS,CN_PRE_DIA,CN_CUIDADO,CN_RECETA,CN_AV_IZQ,CN_AV_DER,CN_EST_TIP,CN_EST_DES,CN_EST_HAY,CN_EST_FEC,CN_EST_RES,CN_EST_OBS,CN_HOR_FIN,CN_IMSS,US_CODIGO,CN_FEC_MOD,CN_SUB_SEC,CN_D_EXT,CN_D_BLOB,CN_D_OBS', 'EX_CODIGO,CN_FECHA,CN_TIPO');
            eEmbarazo: SetInfo('EMBARAZO', 'EX_CODIGO,EM_FEC_UM,EM_FEC_PP,EM_FINAL,EM_FEC_FIN,EM_TERMINO,EM_NORMAL,EM_MORTAL,EM_COMENTA,US_CODIGO,EM_RIESGO,EM_INC_INI,EM_INC_FIN,EM_OBS_RIE,EM_PRENAT,EM_POSNAT', 'EX_CODIGO,EM_FEC_UM');
            eAccidente: SetInfo('ACCIDENT', 'EX_CODIGO,AX_FECHA,AX_FEC_REG,AX_CAUSA,AX_HORA,AX_TIP_ACC,AX_FEC_SUS,AX_HOR_SUS,AX_PER_REG,AX_TIP_LES,AX_DESCRIP,AX_ATENDIO,AX_MOTIVO,' +
                                            'AX_OBSERVA,AX_NUM_INC,US_CODIGO,AX_TIPO,AX_FEC_COM,AX_HOR_COM,AX_RIESGO,AX_INCAPA,AX_DAN_MAT,AX_INF_ACC,AX_INF_TES,AX_INF_SUP,AX_NUM_TES,AX_NUMERO', 'EX_CODIGO,AX_FECHA');
            eMedicinaEntr: SetInfo('MED_ENTR', 'ME_CODIGO,EX_CODIGO,MT_FECHA,MT_CANTIDA,MT_COMENTA,US_CODIGO', 'ME_CODIGO,MT_FECHA,EX_CODIGO');
            eGruposEx: SetInfo('GRUPO_EX', 'GX_CODIGO,GX_TITULO,GX_POSICIO', 'GX_CODIGO');
            eCamposEx: SetInfo('CAMPO_EX', 'CX_NOMBRE,GX_CODIGO,CX_POSICIO,CX_TITULO,CX_TIPO,CX_DEFAULT,CX_MOSTRAR', 'CX_NOMBRE');
            eConsultaGlobal: SetInfo('CONSULTA', 'EX_CODIGO,CN_FECHA,CN_HOR_INI,CN_TIPO,CN_MOTIVO,CN_OBSERVA,CN_HOR_FIN,US_CODIGO', 'EX_CODIGO,CN_FECHA');
          else
              SetInfo(GetNombreTabla(eTabla), 'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_TIPO', 'TB_CODIGO');
          end;
     end;
end;

function TdmServerMedico.GetScript(const eScript: eAllScripts): string;
begin
     case eScript of
       eGetExpediente:
         Result := K_CAMPOS_EXPEDIEN + ' from EXPEDIEN E left outer join COLABORA C On C.CB_CODIGO = E.CB_CODIGO ' +
                                       'where ( EX_CODIGO = %d  ) %s';
       eExpedienteAnterior:
         Result := K_CAMPOS_EXPEDIEN +
           ' from EXPEDIEN E left outer join COLABORA C On C.CB_CODIGO = E.CB_CODIGO ' +
           'where ( EX_CODIGO = ( select MAX( C2.EX_CODIGO ) from EXPEDIEN C2 where ( C2.EX_CODIGO < %d ) ) ) ';
       eExpedienteAnteriorConf:
         Result := K_CAMPOS_EXPEDIEN +
           ' from EXPEDIEN E left outer join COLABORA C On C.CB_CODIGO = E.CB_CODIGO ' +
           Format('where ( EX_CODIGO = ( select MAX( C2.EX_CODIGO ) from EXPEDIEN C2 ' +
             ' left outer join COLABORA C3 on C3.CB_CODIGO = C2.CB_CODIGO ' +
             ' where ( C2.EX_CODIGO < %s ) %s))', ['%d', Format(FFiltroNivel0, ['C2', '%s'])]);
       eExpedienteSiguiente:
         Result := K_CAMPOS_EXPEDIEN +
           ' from EXPEDIEN E left outer join COLABORA C On C.CB_CODIGO = E.CB_CODIGO ' +
           'where ( EX_CODIGO = ( select MIN( C2.EX_CODIGO ) from EXPEDIEN C2 where ( C2.EX_CODIGO > %d ) ) )';

       eExpedienteSiguienteConf:
         Result := K_CAMPOS_EXPEDIEN +
           ' from EXPEDIEN E left outer join COLABORA C On C.CB_CODIGO = E.CB_CODIGO ' +
           Format('where ( EX_CODIGO = ( select MIN( C2.EX_CODIGO ) from EXPEDIEN C2 ' +
             ' left outer join COLABORA C3 on C3.CB_CODIGO = C2.CB_CODIGO ' +
             ' where ( C2.EX_CODIGO > %s ) %s )) ', ['%d', Format(FFiltroNivel0, ['C2', '%s'])]);
       eHacerBusqueda:
         Result := 'select %s from %s where %s ';
       eGetCodigos:
         Result := 'select c.cb_codigo Cb_CodigoC, e.cb_codigo, e.ex_codigo, e.ex_tipo from COLABORA c '
           + 'left outer join Expedien e On (c.cb_codigo = e.cb_codigo) ' + '%s order by 1';
       eMaxExpediente:
         Result := 'select MAX(EX_CODIGO) MAXIMO from EXPEDIEN';
       eRevisaDuplicado:
         Result := ' select COUNT(*) CUANTOS from EXPEDIEN ' + ' where ( EX_TIPO = :EX_TIPO ) ' +
           ' and ( CB_CODIGO = :CB_CODIGO ) ';
       // ' and ( EX_CODIGO <> :EX_CODIGO ) ';
       eConsultas:
         Result := 'select EX_CODIGO, CN_FECHA, CN_HOR_INI, DA_CODIGO, CN_TIPO, CN_MOTIVO, ' +
           'CN_SINTOMA, CN_EXPLORA, CN_OBSERVA, CN_DIAGNOS, CN_PULSO, CN_RESPIRA, ' +
           'CN_PESO, ( 2.2 * CN_PESO ) as CN_LIBRAS, ' +
           'CN_TEMPERA, ( ( 1.8 * CN_TEMPERA ) + 32 ) as CN_FAHREN, ' +
           'CN_ALTURA, 0 as CN_PIES, 0 as CN_PULGADAS, ' +
           'CN_PRE_SIS, CN_PRE_DIA, CN_CUIDADO, CN_RECETA, CN_AV_IZQ, CN_AV_DER, ' +
           'CN_EST_TIP, CN_EST_DES, CN_EST_HAY, CN_EST_FEC, CN_EST_RES, CN_EST_OBS, ' +
           'CN_HOR_FIN, CN_IMSS, US_CODIGO, CN_FEC_MOD, CN_SUB_SEC,  ' + 'CN_D_EXT,CN_D_BLOB,CN_D_OBS '
           + 'from CONSULTA where ( EX_CODIGO = %d ) order by CN_FECHA DESC';
       eEmpleado:
         Result := 'select B.CB_CODIGO, B.CB_APE_PAT, B.CB_APE_MAT, B.CB_NOMBRES, ' +
           'B.CB_PUESTO, B.CB_CONTRAT, B.CB_TURNO, B.CB_CLASIFI, B.CB_EDO_CIV, ' +
           'B.CB_LUG_NAC, B.CB_FEC_NAC, ' + 'B.CB_CALLE, B.CB_COLONIA, B.CB_CIUDAD, ' +
           'B.CB_FEC_ING, IMAGEN.IM_BLOB as FOTOGRAFIA ' + 'from COLABORA B ' +
           'left outer join IMAGEN on ( IMAGEN.CB_CODIGO = B.CB_CODIGO ) and ( IMAGEN.IM_TIPO = ''FOTO'' ) '
           + 'where ( B.CB_CODIGO = %d ) %s ';
       eHistorial:
         Result := 'select CB_CODIGO,IN_FEC_INI, IN_FEC_FIN, ' + 'cast(IN_DIAS AS INTEGER) IN_DIAS, ' +
           'cast(0 AS INTEGER )IN_CLASIFI, ' + 'cast(IN_TIPO AS CHAR(3)) IN_TIPO, ' +
           'cast(IN_NUMERO AS VARCHAR(30)) IN_NUMERO, ' + 'cast(IN_COMENTA AS CHAR(30)) IN_COMENTA, ' +
           'cast(''INC'' AS CHAR(3)) TIPO ' + 'from INCAPACI where CB_CODIGO =%d ' + 'union ' +
           'select cast( CB_CODIGO AS INTEGER), ' +
         {$ifdef INTERBASE}
           'cast( PM_FEC_INI AS DATE), ' + 'cast(PM_FEC_FIN AS DATE), ' +
         {$endif}
         {$ifdef MSSQL}
           'cast( PM_FEC_INI AS DATETIME), ' + 'cast(PM_FEC_FIN AS DATETIME), ' +
         {$endif}
           'cast(PM_DIAS AS INTEGER), ' + 'cast(PM_CLASIFI AS INTEGER ), ' +
           'cast(PM_TIPO AS CHAR(3)), ' + 'cast(PM_NUMERO AS VARCHAR(30)), ' +
           'cast(PM_COMENTA AS CHAR(30)), ' + 'cast(''PER'' AS CHAR(3)) TIPO ' +
           'from PERMISO where CB_CODIGO =%d ' + 'ORDER BY 2 DESC';
       eDeleteGrupos:
         Result := 'delete from GRUPO_EX';
       eDeleteCampos:
         Result := 'delete from CAMPO_EX';
       eGetConsGlobal:
         Result := ' select C.EX_CODIGO,E.CB_CODIGO,C.CN_FECHA,C.CN_HOR_INI,C.CN_HOR_FIN, ' +
           ' C.CN_TIPO,C.CN_MOTIVO,C.CN_OBSERVA,C.US_CODIGO,C.CN_FEC_MOD,E.EX_TIPO, ' + PRETTYNAME_EXP
           + ' from CONSULTA C left outer join Expedien E On E.EX_CODIGO = C.EX_CODIGO ';
       eDiccionario:
         Result := 'EXECUTE PROCEDURE SP_REFRESH_DICCION';
       eGetStatusEmb:
         Result := ' select E1.EM_FEC_UM, E1.EM_FEC_FIN, E1.EM_FEC_PP, E1.EM_PRENAT, E1.EM_POSNAT, ' +
           ' E1.EM_RIESGO, E1.EM_INC_INI, E1.EM_INC_FIN ' + ' from EMBARAZO E1 ' +
           ' where E1.EX_CODIGO = %0:d ' + ' and ( %1:s between EM_FEC_UM and EM_FEC_PP )';

       { No se dejo un solo query con MAX(AX_FEC_SUS) para algun cambio futuro en el rango de fechas
         del empleado accidentado, es parecido al embarazo } { eGetStatusAcc: Result := ' select A1.AX_FECHA from ACCIDENT A1 ' +
         ' where A1.EX_CODIGO = %d ' +
         ' and A1.AX_FECHA = ( select MAX(A2.AX_FECHA) '+
         ' from ACCIDENT A2 '+
         ' where A2.EX_CODIGO = A1.EX_CODIGO )'; } { eGetExpedienteStatus: Result := Format( ' select %s, 0 EX_STATUS_ACC, 0 EX_STATUS_EMB, CAST( ''01/01/2000'' as Date ) EX_FEC_UM, CAST( ''01/01/2000'' as Date ) EX_FEC_PP, ' +
         ' CAST( ''01/01/2000'' as Date ) EX_PRENAT, CAST( ''01/01/2000'' as Date ) EX_POSNAT, '' '' EX_RIESGO, '+
         ' CAST( ''01/01/2000'' as Date ) EX_INC_INI, CAST( ''01/01/2000'' as Date ) EX_INC_FIN, CAST( ''01/01/2000'' as Date ) EX_EMB_FIN ' +
         ' from EXPEDIEN where %s ', [ K_TODOS_CAMPOS_EXPEDIEN,
         K_FILTRO_EMPLEADO ] ); }
       {$ifdef INTERBASE}
       eGetExpedienteStatus:
         Result := Format
           (' select %1:s,( select RESULTADO from SP_STATUS_ACC( EXPEDIEN.EX_CODIGO, %3:s, %3:s )) EX_STATUS_ACC, '
             + ' ( select RESULTADO from SP_STATUS_EMB( EXPEDIEN.EX_CODIGO, %3:s, %3:s )) EX_STATUS_EMB, '
             + '%0:s' + 'EX_FEC_UM, ' + '%0:s' + 'EX_FEC_PP, ' + '%0:s' + 'EX_PRENAT, ' + '%0:s' +
             'EX_POSNAT, '' '' EX_RIESGO, ' + '%0:s' + 'EX_INC_INI, ' + '%0:s' + 'EX_INC_FIN, ' +
             '%0:s' + 'EX_EMB_FIN ' + ' from EXPEDIEN where %2:s ',
           [GeneraCastDate, GetTodosCamposExpediente, K_FILTRO_EMPLEADO,
             DatetoStrSQLC(oZetaProvider.FechaDefault)]); { OP: 04/06/08 }
       {$endif}
       {$ifdef MSSQL}
       eGetExpedienteStatus:
         Result := Format
           (' select %1:s,( select dbo.SP_STATUS_ACC( EXPEDIEN.EX_CODIGO, %3:s, %3:s )) EX_STATUS_ACC, '
             + ' ( select dbo.SP_STATUS_EMB( EXPEDIEN.EX_CODIGO, %3:s, %3:s )) EX_STATUS_EMB, ' +
             '%0:s' + 'EX_FEC_UM, ' + '%0:s' + 'EX_FEC_PP, ' + '%0:s' + 'EX_PRENAT, ' + '%0:s' +
             'EX_POSNAT, '' '' EX_RIESGO, ' + '%0:s' + 'EX_INC_INI, ' + '%0:s' + 'EX_INC_FIN, ' +
             '%0:s' + 'EX_EMB_FIN ' + ' from EXPEDIEN where %2:s ',
           [GeneraCastDate, GetTodosCamposExpediente, K_FILTRO_EMPLEADO,
             DatetoStrSQLC(oZetaProvider.FechaDefault)]); { OP: 04/06/08 }
       {$endif}
       eValidaStatusEmb:
         Result := ' select count( * ) CUANTOS from EMBARAZO ' + ' where ( EX_CODIGO = %d ) ' +
           ' and ( EM_FEC_UM <> %s )' + ' and ( ( %s between EM_FEC_UM and EM_FEC_PP ) ' +
           ' or ( %s between EM_FEC_UM and EM_FEC_PP ) ) ';
       eActualizaExpedien:
         Result := ' update EXPEDIEN ' + ' set EX_EMB_INI = %s, ' + '     EX_EMB_FIN = %s  ' +
           ' where EX_CODIGO = %d';
       eObtenCamposExpedien:
         Result := ' select * from EXPEDIEN where CB_CODIGO = -1'; { OP: 04/06/08 }
       eExisteExpediente:
         Result := ' select EX_CODIGO from EXPEDIEN where EX_CODIGO = %d';
     else
         Result := VACIO;
     end
end;

function TdmServerMedico.GetNombreTabla(const eTabla: eTipoTabla): string;
begin
     case eTabla of
       eTConsulta: Result := 'TCONSLTA';
       eTEstudio:  Result := 'TESTUDIO';
       eTAcciden:  Result := 'TACCIDEN';
       eCausacci:  Result := 'CAUSACCI';
       eMotivoAcci: Result := 'MOTACCI';
     end;
end;

{$ifdef DOS_CAPAS}

procedure TdmServerMedico.CierraEmpresa;
begin
end;
{$endif}

function TdmServerMedico.GetTabla(Empresa: OleVariant; iTabla: Integer): OleVariant;
begin
     try
        SetTablaInfo(eTipoTabla(iTabla));
        Result := oZetaProvider.GetTabla(Empresa);
     except
           SetOLEVariantToNull(Result);
     end;
     SetComplete;
end;

function TdmServerMedico.GrabaTabla(Empresa: OleVariant; iTabla: Integer; oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo(eTipoTabla(iTabla));
     Result := oZetaProvider.GrabaTabla(Empresa, oDelta, ErrorCount);
     SetComplete;
end;

function TdmServerMedico.GetKardexConsulta(Empresa: OleVariant; iExpediente: Integer): OleVariant;
begin
     Result := LeeConsulta(Empresa, iExpediente);
end;

function TdmServerMedico.GetKardexEmbarazo(Empresa: OleVariant; iExpediente: Integer): OleVariant;
begin
     Result := LeeEmbarazo(Empresa, iExpediente);
end;

function TdmServerMedico.GetKardexAccidente(Empresa: OleVariant; iExpediente: Integer): OleVariant;
begin
     Result := LeeAccidente(Empresa, iExpediente);
end;

function TdmServerMedico.GetMedicinaEntr(Empresa: OleVariant; iExpediente: Integer): OleVariant;
begin
     Result := LeeMedicinaEntr(Empresa, iExpediente);
end;

function TdmServerMedico.GetGruposEx(Empresa: OleVariant; var oCamposEx: OleVariant): OleVariant;
begin
     SetTablaInfo(eGruposEx);
     with oZetaProvider do
     begin
          TablaInfo.SetOrder('GX_POSICIO');
          Result := GetTabla(Empresa);
     end;
     SetTablaInfo(eCamposEx);
     with oZetaProvider do
     begin
          TablaInfo.SetOrder('GX_CODIGO,CX_POSICIO');
          oCamposEx := GetTabla(Empresa);
     end;
end;

function TdmServerMedico.GrabaGruposEx(Empresa, oDelta, oCamposEx: OleVariant; var oCamposExResult: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;

          ExecSQL(Empresa, GetScript(eDeleteGrupos));
          SetTablaInfo(eGruposEx);

          if (oDelta = Null) then
            Result := Null
          else
            Result := oZetaProvider.GrabaTabla(Empresa, oDelta, ErrorCount);

          ExecSQL(Empresa, GetScript(eDeleteCampos));
          SetTablaInfo(eCamposEx);
          oCamposExResult := oZetaProvider.GrabaTabla(Empresa, oCamposEx, ErrorCount);

          EmpiezaTransaccion;
          try
             EjecutaAndFree(GetScript(eDiccionario));
             TerminaTransaccion(TRUE);
          except
                on Error: Exception do
                begin
                     TerminaTransaccion(FALSE);
                     raise;
                end;
          end;
     end;
end;

function TdmServerMedico.LeeConsulta(Empresa: OleVariant; const iExpediente: Integer): OleVariant;
begin
     Result := oZetaProvider.OpenSQL(Empresa, Format(GetScript(eConsultas), [iExpediente]), TRUE);
end;

function TdmServerMedico.LeeAccidente(Empresa: OleVariant; const iExpediente: Integer): OleVariant;
begin
     SetTablaInfo(eAccidente);
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format(K_FILTRO_EMPLEADO, [iExpediente]);
          Result := GetTabla(Empresa);
     end;
end;

function TdmServerMedico.LeeEmbarazo(Empresa: OleVariant; const iExpediente: Integer): OleVariant;
begin
     SetTablaInfo(eEmbarazo);
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format(K_FILTRO_EMPLEADO, [iExpediente]);
          Result := GetTabla(Empresa);
     end;
end;

function TdmServerMedico.LeeMedicinaEntr(Empresa: OleVariant; const iExpediente: Integer): OleVariant;
begin
     SetTablaInfo(eMedicinaEntr);
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format(K_FILTRO_EMPLEADO, [iExpediente]);
          Result := GetTabla(Empresa);
     end;
end;

function TdmServerMedico.GetExpediente(Empresa: OleVariant; iExpediente: Integer; Parametros: OleVariant; var oConsulta: OleVariant): OleVariant;
var
   oDataSet     : TClientDataSet;
   oDataSetEmb  : TZetaCursor;
   dFUM, dFinEmb: TDate;
begin
     oZetaProvider.AsignaParamList(Parametros);
     oZetaProvider.GetDatosActivos;
     oDataSet := TClientDataSet.Create(Self);
     oDataSetEmb := nil;
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             oDataSet.Data := OpenSQL(Empresa, Format(GetScript(eGetExpedienteStatus), [iExpediente]), TRUE);
        end;
        with oDataSet do
        begin
             LogChanges := FALSE;
             if zStrToBool(FieldByName('EX_STATUS_EMB').AsString) then
             begin
                  { Obtiene los campos calculados de embarazo para mostrarlos en el RTF   }
                  oDataSetEmb := oZetaProvider.CreateQuery(Format(GetScript(eGetStatusEmb), [iExpediente, DatetoStrSQLC(oZetaProvider.FechaDefault)]));
                  oDataSetEmb.Open;

                  dFUM := oDataSetEmb.FieldByName('EM_FEC_UM').AsDateTime;
                  dFinEmb := oDataSetEmb.FieldByName('EM_FEC_FIN').AsDateTime;
                  Edit;
                  FieldByName('EX_FEC_UM').AsDateTime := dFUM;
                  FieldByName('EX_EMB_FIN').AsDateTime := dFinEmb;
                  FieldByName('EX_FEC_PP').AsDateTime := oDataSetEmb.FieldByName('EM_FEC_PP').AsDateTime;
                  FieldByName('EX_RIESGO').AsString := oDataSetEmb.FieldByName('EM_RIESGO').AsString;
                  FieldByName('EX_PRENAT').AsDateTime := oDataSetEmb.FieldByName('EM_PRENAT').AsDateTime;
                  FieldByName('EX_POSNAT').AsDateTime := oDataSetEmb.FieldByName('EM_POSNAT').AsDateTime;
                  FieldByName('EX_INC_INI').AsDateTime := oDataSetEmb.FieldByName('EM_INC_INI').AsDateTime;
                  FieldByName('EX_INC_FIN').AsDateTime := oDataSetEmb.FieldByName('EM_INC_FIN').AsDateTime;
                  oDataSetEmb.Close;
                  Post;
             end;
             Result := Data;
        end;
     finally
            //FreeAndNil(oDataSetEmb);
            FreeAndNil(oDataSet);
     end;
     oConsulta := LeeConsulta(Empresa, iExpediente);
end;

function TdmServerMedico.GetSoloExpediente(Empresa: OleVariant; iExpediente: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa; { OP: 04/06/08 }
          //SetTablaInfo(eExpedien);
          Result := oZetaProvider.OpenSQL(oZetaProvider.EmpresaActiva, Format(GetScript(eExisteExpediente), [iExpediente]), TRUE);
          //TablaInfo.Filtro := Format(K_FILTRO_EMPLEADO, [iExpediente]);
          //Result := GetTabla(Empresa);
     end;
end;

function TdmServerMedico.GetHistorial(Empresa: OleVariant; iEmpleado: Integer): OleVariant;
begin
     Result := oZetaProvider.OpenSQL(Empresa, Format(GetScript(eHistorial), [iEmpleado, iEmpleado]), TRUE);
end;

function TdmServerMedico.GrabaMedicinaEntr(Empresa, oDelta: OleVariant; var ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo(eMedicinaEntr);
     with oZetaProvider do
     begin
          Result := GrabaTabla(Empresa, oDelta, ErrorCount);
     end;
end;

{procedure TdmServerMedico.AfterUpdateEmbarazo(Sender: TObject; SourceDS: TDataSet;
  DeltaDS: TClientDataSet; UpdateKind: TUpdateKind);
var
  FDataSet        : TZetaCursor;
  iNumExp         : Integer;
  dInicial, dFinal: TDate;
begin
  iNumExp := DeltaDS.FieldByName('EX_CODIGO').AsInteger;
  if (UpdateKind = ukInsert) then begin
    with DeltaDS do begin
      dInicial := FieldByName('EM_FEC_UM').AsDateTime;
      dFinal := FieldByName('EM_FEC_PP').AsDateTime;
    end;
    with oZetaProvider do begin
      ExecSQL(EmpresaActiva, Format(GetScript(Q_ACTUALIZA_EXPEDIEN), [DatetoStrSQLC(dInicial), DatetoStrSQLC(dFinal), iNumExp]));
    end;
  end;
  if (UpdateKind = ukDelete) then begin
    FDataSet := oZetaProvider.CreateQuery(Format(GetScript(Q_TODOS_EMBARAZOS), [iNumExp]));
    try
      with FDataSet do begin
        Active := TRUE;
        if IsEmpty then begin
          dInicial := NULLDATETIME;
          dFinal := NULLDATETIME;
        end else begin
          dInicial := FieldByName('EM_FEC_UM').AsDateTime;
          dFinal := FieldByName('EM_FEC_PP').AsDateTime;
        end;
        with oZetaProvider do begin
          ExecSQL(EmpresaActiva, Format(GetScript(Q_ACTUALIZA_EXPEDIEN), [DatetoStrSQLC(dInicial),
                DatetoStrSQLC(dFinal), iNumExp]));
        end;
      end;
    finally
      FreeAndNil(FDataSet);
    end;
  end;
end;
}

function TdmServerMedico.GrabaKardexEmbarazo(Empresa, oDelta: OleVariant; var ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo(eEmbarazo);
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          with TablaInfo do
          begin
               BeforeUpdateRecord := BeforeUpdateEmbarazo;
               // AfterUpdateRecord := AfterUpdateEmbarazo;
          end;
          Result := GrabaTabla(Empresa, oDelta, ErrorCount);
     end;
end;

procedure TdmServerMedico.BeforeUpdateEmbarazo(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
          UpdateKind: TUpdateKind; var Applied: Boolean);
var
   FDataSet : TZetaCursor;
   iNumExp : Integer;
   dInicial, dFinal: TDate;
begin
     with oZetaProvider do
     begin
          if (UpdateKind in [ukInsert, ukModify]) then
          begin
               with DeltaDS do
               begin
                    iNumExp := ZetaServerTools.CampoAsVar(FieldByName('EX_CODIGO'));
                    dInicial := ZetaServerTools.CampoAsVar(FieldByName('EM_FEC_UM'));
                    dFinal := ZetaServerTools.CampoAsVar(FieldByName('EM_FEC_PP'));
               end;
               if (ZetaServerTools.CampoAsVar(DeltaDS.FieldByName('EM_FEC_UM')) >= ZetaServerTools.CampoAsVar(DeltaDS.FieldByName('EM_FEC_PP'))) then
                 DatabaseError('La Fecha Probable de Parto Debe Ser Menor Que La Fecha De Ultima Menstruaci�n')
               else
               begin
                    if (UpdateKind = ukInsert) OR CambiaCampo(DeltaDS.FieldByName('EM_FEC_UM')) then
                    begin
                         FDataSet := CreateQuery(Format(GetScript(eValidaStatusEmb), [iNumExp, DatetoStrSQLC(CampoOldAsVar(DeltaDS.FieldByName('EM_FEC_UM'))),
                                                        DatetoStrSQLC(dInicial), DatetoStrSQLC(dFinal)]));
                         try
                            with FDataSet do
                            begin
                                 Open;
                                 if (FieldByName('CUANTOS').AsInteger > 0) then
                                    DatabaseError('No Se Puede Tener Un Embarazo Dentro De las Fechas Del Embarazo Anterior');
                                 Close;
                            end;
                         finally
                                FreeAndNil(FDataSet);
                         end;
                    end;
               end;
          end;
     end;
end;

function TdmServerMedico.GrabakardexConsultas(Empresa, oDelta: OleVariant; var ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo(eConsulta);
     with oZetaProvider do
     begin
          Result := GrabaTabla(Empresa, oDelta, ErrorCount);
     end;
end;

function TdmServerMedico.GrabaKardexAccidente(Empresa, oDelta: OleVariant; var ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo(eAccidente);
     with oZetaProvider do
     begin
          Result := GrabaTabla(Empresa, oDelta, ErrorCount);
     end;
end;

function TdmServerMedico.GrabaExpediente(Empresa, oDelta: OleVariant; var ErrorCount, iExpediente: Integer): OleVariant;
begin
     FExpediente := iExpediente;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          SetTablaInfo(eExpedien); { OP: 04/06/08 }
          TablaInfo.BeforeUpdateRecord := BeforeUpdateExpediente;
          Result := GrabaTabla(Empresa, oDelta, ErrorCount);
     end;
     iExpediente := FExpediente;
end;

function TdmServerMedico.GetMaxExpediente: Integer;
var
   FDataSet: TZetaCursor;
begin
     FDataSet := oZetaProvider.CreateQuery(GetScript(eMaxExpediente));
     try
        FDataSet.Open;
        Result := FDataSet.FieldByName('MAXIMO').AsInteger + 1; // se le suma 1 al numero maximo de exp.
        FDataSet.Close;
     finally
            FreeAndNil(FDataSet);
     end;
end;

procedure TdmServerMedico.BeforeUpdateExpediente(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
          UpdateKind: TUpdateKind; var Applied: Boolean);
var
   oRevisaDuplicado   : TZetaCursor;
   iCuantos, iEmpleado: Integer;
   eTipo              : eExTipo;

   procedure ChecaDuplicados;
   begin
        with oZetaProvider do
        begin
             oRevisaDuplicado := CreateQuery(GetScript(eRevisaDuplicado));
             try
                ParamAsInteger(oRevisaDuplicado, 'EX_TIPO', ord(exEmpleado));
                ParamAsInteger(oRevisaDuplicado, 'CB_CODIGO', iEmpleado);
                // ParamAsInteger( oRevisaDuplicado, 'EX_CODIGO', ZetaServerTools.CampoAsVar( DeltaDS.FieldByName('EX_CODIGO') ) );
                oRevisaDuplicado.Active := TRUE;
                iCuantos := oRevisaDuplicado.Fields[0].AsInteger;
                oRevisaDuplicado.Active := FALSE;
                if (iCuantos > 0) and (UpdateKind <> ukDelete) then
                begin
                     DatabaseError(Format('El Empleado # %d ya Tiene un Expediente', [iEmpleado]));
                end;
             finally
                    FreeAndNil(oRevisaDuplicado);
             end;
        end;
   end;

begin
     with oZetaProvider do
     begin
          // Se tiene que revisar que no existan 2 pacientes
          // tipo empleado con el mismo CB_CODIGO
          eTipo := eExTipo(ZetaServerTools.CampoAsVar(DeltaDS.FieldByName('EX_TIPO')));
          if eTipo = exEmpleado then
          begin
               iEmpleado := ZetaServerTools.CampoAsVar(DeltaDS.FieldByName('CB_CODIGO'));
               if (UpdateKind = ukInsert) then
               begin
                    ChecaDuplicados;
               end
               else
               begin
                    if (UpdateKind = ukModify) then
                    begin
                         if ((CampoAsVar(DeltaDS.FieldByName('CB_CODIGO')) <> CampoOldAsVar(DeltaDS.FieldByName('CB_CODIGO'))) OR
                             (CampoAsVar(DeltaDS.FieldByName('EX_TIPO')) <>  CampoOldAsVar(DeltaDS.FieldByName('EX_TIPO')))) then
                            ChecaDuplicados;
                    end;
               end;
          end;
          // Si es Alta, tengo que obtener el m�ximo Folio
          if (UpdateKind = ukInsert) then
          begin
               FExpediente := GetMaxExpediente;
               DeltaDS.Edit;
               DeltaDS.FieldByName('EX_CODIGO').AsInteger := FExpediente;
               DeltaDS.Post;
          end;
     end;
end;

function TdmServerMedico.GetDatosEmpleado(Empresa: OleVariant; Empleado: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Result := OpenSQL(Empresa, Format(GetScript(eEmpleado), [Empleado, NiVel0(Empresa)]), TRUE);
     end;
end;

function TdmServerMedico.ExpedienteActivo(Empresa: OleVariant; iExpediente: Integer; out oDelta: OleVariant): OleVariant;
var
   sSQL : string;
   sFiltroConf: string;
begin
     sFiltroConf := GetFiltroNivel0(Empresa, 'C');
     if StrLleno(sFiltroConf) then
        sFiltroConf := Format(FFiltroNivel0, ['E', sFiltroConf]);

     sSQL := Format(GetScript(eGetExpediente), [iExpediente, sFiltroConf]);
     oDelta := oZetaProvider.OpenSQL(Empresa, sSQL, TRUE);
     Result := (oZetaProvider.RecsOut > 0);
     SetComplete;
end;

function TdmServerMedico.ExpedienteAnterior(Empresa: OleVariant; iExpediente: Integer; out oDelta: OleVariant): OleVariant;
var
   sSQL: string;
   sFiltroConf: string;
begin
     sFiltroConf := GetFiltroNivel0(Empresa, 'C3');
     if StrVacio(sFiltroConf) then
        sSQL := Format(GetScript(eExpedienteAnterior), [iExpediente])
     else
         sSQL := Format(GetScript(eExpedienteAnteriorConf), [iExpediente, sFiltroConf]);

     oDelta := oZetaProvider.OpenSQL(Empresa, sSQL, TRUE);
     Result := (oZetaProvider.RecsOut > 0);
     SetComplete;
end;

function TdmServerMedico.ExpedienteSiguiente(Empresa: OleVariant; iExpediente: Integer; out oDelta: OleVariant): OleVariant;
var
   sSQL       : string;
   sFiltroConf: string;
begin
     sFiltroConf := GetFiltroNivel0(Empresa, 'C3');
     if StrVacio(sFiltroConf) then
        sSQL := Format(GetScript(eExpedienteSiguiente), [iExpediente])
     else
         sSQL := Format(GetScript(eExpedienteSiguienteConf), [iExpediente, sFiltroConf]);

     oDelta := oZetaProvider.OpenSQL(Empresa, sSQL, TRUE);
     Result := (oZetaProvider.RecsOut > 0);
     SetComplete;
end;

function TdmServerMedico.BuscarEnTablas(Empresa, oParams: OleVariant): OleVariant;
const
     K_WHERE = ' ( UPPER( %0:s_APE_PAT ) like ''%1:s%2:s%1:s'' ) and ' +
               ' ( UPPER( %0:s_APE_MAT ) like ''%1:s%3:s%1:s'' ) and ' +
               ' ( UPPER( %0:s_NOMBRES ) like ''%1:s%4:s%1:s'' ) %5:s ';
     K_EXPEDIEN_JOIN = ' EXPEDIEN left outer join COLABORA on COLABORA.CB_CODIGO = EXPEDIEN.CB_CODIGO ';
var
   sSelect, sFrom, sWhere, sFiltro: string;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList(oParams);
          sFiltro := NiVel0(Empresa);
          with ParamList do
          begin
               if ParamByName('TIPO').AsBoolean then // Es busqueda sobre empleados
               begin
                    sWhere := 'CB';
                    sFiltro := NiVel0(Empresa);
                    if ParamByName('SoloExp').AsBoolean then
                    begin
                         sSelect := 'EXPEDIEN.CB_CODIGO, EX_CODIGO, ' + ZetaCommonClasses.K_PRETTYNAME + ' PRETTYNAME ';
                         sFrom := K_EXPEDIEN_JOIN;
                         sFiltro := sFiltro + ' and ( EX_TIPO = 0 )';
                    end
                    else
                    begin
                         sSelect := '0 EX_CODIGO, CB_CODIGO, ' + ZetaCommonClasses.K_PRETTYNAME + ' PRETTYNAME ';
                         sFrom := 'COLABORA';
                    end;
               end
               else
               begin
                    sSelect := 'EX_CODIGO, EXPEDIEN.CB_CODIGO, EX_TIPO, ' + PRETTYNAME_EXP;
                    if StrVacio(sFiltro) then
                    begin
                         sFrom := 'EXPEDIEN';
                         sFiltro := Format(' and (EX_TIPO <> %d) ', [ord(exEmpleado)]);
                    end
                    else
                    begin
                         sFrom := K_EXPEDIEN_JOIN;
                         sFiltro := ConcatFiltros(sFiltro, Format('(EX_TIPO = %d)', [ord(exPariente)])) + Format(' or ( EX_TIPO in (%d,%d) ) ', [ord(exCandidato), ord(exOtro)]);
                    end;
                    sWhere := 'EX';
               end;
               sWhere := Format(K_WHERE, [sWhere, '%', ParamByName('APE_PAT').AsString, ParamByName('APE_MAT').AsString, ParamByName('NOMBRES').AsString, sFiltro]);
               Result := OpenSQL(Empresa, Format(GetScript(eHacerBusqueda), [sSelect, sFrom, sWhere]), TRUE);
          end;
     end;
end;

function TdmServerMedico.GetCodigos(Empresa: OleVariant): OleVariant;
var
   sNivel0: string;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          sNivel0 := GetFiltroNivel0(Empresa);
          if StrLleno(sNivel0) then
             sNivel0 := 'where ' + sNivel0;
          Result := OpenSQL(Empresa, Format(GetScript(eGetCodigos), [sNivel0]), TRUE);
     end;
end;

function TdmServerMedico.GetConsultaGlobal(Empresa: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Result := OpenSQL(Empresa, GetScript(eGetConsGlobal), FALSE);
     end;
end;

function TdmServerMedico.GrabaConsultaGlobal(Empresa, oDelta, oParams: OleVariant; var ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo(eConsultaGlobal);
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList(oParams);
          TablaInfo.BeforeUpdateRecord := BeforeUpdateConsultaGlobal;
          // Result := GrabaTabla( Empresa, oDelta, ErrorCount );
          Result := GrabaTablaGrid(Empresa, oDelta, ErrorCount);
     end;
end;

procedure TdmServerMedico.BeforeUpdateConsultaGlobal(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
          UpdateKind: TUpdateKind; var Applied: Boolean);
begin
     if (UpdateKind = ukInsert) then
     begin
          with oZetaProvider do
          begin
               with ParamList do
               begin
                    DeltaDS.Edit;
                    DeltaDS.FieldByName('CN_FECHA').AsDateTime := ParamByName('CN_FECHA').AsDateTime;
                    DeltaDS.FieldByName('CN_HOR_INI').AsString := ParamByName('CN_HOR_INI').AsString;
                    DeltaDS.FieldByName('CN_HOR_FIN').AsString := ParamByName('CN_HOR_FIN').AsString;
                    DeltaDS.FieldByName('CN_TIPO').AsString := ParamByName('CN_TIPO').AsString;
                    DeltaDS.FieldByName('CN_MOTIVO').AsString := ParamByName('CN_MOTIVO').AsString;
                    DeltaDS.FieldByName('CN_OBSERVA').AsString := ParamByName('CN_OBSERVA').AsString;
                    DeltaDS.FieldByName('US_CODIGO').AsInteger := ParamByName('US_CODIGO').AsInteger;
                    DeltaDS.Post;
               end;
          end;
     end;
end;

function TdmServerMedico.GeneraCastDate: string;
begin
     {$ifdef INTERBASE}
     Result := ' CAST( ' + DatetoStrSQLC(NULLDATETIME) + ' as Date ) ';
     {$endif}
     {$ifdef MSSQL}
     Result := ' CAST( ' + DatetoStrSQLC(NULLDATETIME) + ' as DateTime ) ';
     {$endif}
end;

{$ifndef DOS_CAPAS}
initialization
  TComponentFactory.Create(ComServer, TdmServerMedico, Class_dmServerMedico, ciMultiInstance,
    ZetaServerTools.GetThreadingModel);
{$endif}

end.
