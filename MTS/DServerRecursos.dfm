object dmServerRecursos: TdmServerRecursos
  OldCreateOrder = False
  OnCreate = dmServerRecursosCreate
  OnDestroy = dmServerRecursosDestroy
  Pooled = False
  Height = 39
  Width = 140
  object cdsLista: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 24
    Top = 8
  end
  object cdsASCII: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 96
    Top = 8
  end
  object cdsKardex: TServerDataSet
    Aggregates = <>
    Params = <>
    BeforePost = cdsKardexBeforePost
    Left = 24
    Top = 72
    object cdsKardexCB_CODIGO: TIntegerField
      FieldName = 'CB_CODIGO'
    end
    object cdsKardexCB_FECHA: TDateTimeField
      FieldName = 'CB_FECHA'
    end
    object cdsKardexCB_TIPO: TStringField
      FieldName = 'CB_TIPO'
      Size = 6
    end
    object cdsKardexCB_AUTOSAL: TStringField
      FieldName = 'CB_AUTOSAL'
      Size = 1
    end
    object cdsKardexCB_CLASIFI: TStringField
      FieldName = 'CB_CLASIFI'
      Size = 6
    end
    object cdsKardexCB_COMENTA: TStringField
      FieldName = 'CB_COMENTA'
      Size = 50
    end
    object cdsKardexCB_CONTRAT: TStringField
      FieldName = 'CB_CONTRAT'
      Size = 1
    end
    object cdsKardexCB_FAC_INT: TFloatField
      FieldName = 'CB_FAC_INT'
    end
    object cdsKardexCB_FEC_CAP: TDateTimeField
      FieldName = 'CB_FEC_CAP'
    end
    object cdsKardexCB_FEC_CON: TDateTimeField
      FieldName = 'CB_FEC_CON'
    end
    object cdsKardexCB_FEC_INT: TDateTimeField
      FieldName = 'CB_FEC_INT'
    end
    object cdsKardexCB_FEC_REV: TDateTimeField
      FieldName = 'CB_FEC_REV'
    end
    object cdsKardexCB_FECHA_2: TDateTimeField
      FieldName = 'CB_FECHA_2'
    end
    object cdsKardexCB_GLOBAL: TStringField
      FieldName = 'CB_GLOBAL'
      Size = 1
    end
    object cdsKardexCB_TURNO: TStringField
      FieldName = 'CB_TURNO'
      Size = 6
    end
    object cdsKardexCB_MONTO: TFloatField
      FieldName = 'CB_MONTO'
    end
    object cdsKardexCB_MOT_BAJ: TStringField
      FieldName = 'CB_MOT_BAJ'
      Size = 3
    end
    object cdsKardexCB_NIVEL: TSmallintField
      FieldName = 'CB_NIVEL'
    end
    object cdsKardexCB_OLD_INT: TFloatField
      FieldName = 'CB_OLD_INT'
    end
    object cdsKardexCB_OLD_SAL: TFloatField
      FieldName = 'CB_OLD_SAL'
    end
    object cdsKardexCB_OTRAS_P: TFloatField
      FieldName = 'CB_OTRAS_P'
    end
    object cdsKardexCB_PATRON: TStringField
      FieldName = 'CB_PATRON'
      Size = 1
    end
    object cdsKardexCB_PER_VAR: TFloatField
      FieldName = 'CB_PER_VAR'
    end
    object cdsKardexCB_PRE_INT: TFloatField
      FieldName = 'CB_PRE_INT'
    end
    object cdsKardexCB_PUESTO: TStringField
      FieldName = 'CB_PUESTO'
      Size = 6
    end
    object cdsKardexCB_RANGO_S: TFloatField
      FieldName = 'CB_RANGO_S'
    end
    object cdsKardexCB_SAL_INT: TFloatField
      FieldName = 'CB_SAL_INT'
    end
    object cdsKardexCB_SALARIO: TFloatField
      FieldName = 'CB_SALARIO'
    end
    object cdsKardexCB_SAL_TOT: TFloatField
      FieldName = 'CB_SAL_TOT'
    end
    object cdsKardexCB_STATUS: TSmallintField
      FieldName = 'CB_STATUS'
    end
    object cdsKardexCB_TABLASS: TStringField
      FieldName = 'CB_TABLASS'
      Size = 1
    end
    object cdsKardexCB_TOT_GRA: TFloatField
      FieldName = 'CB_TOT_GRA'
    end
    object cdsKardexUS_CODIGO: TSmallintField
      FieldName = 'US_CODIGO'
    end
    object cdsKardexCB_ZONA_GE: TStringField
      FieldName = 'CB_ZONA_GE'
      Size = 1
    end
    object cdsKardexCB_NIVEL1: TStringField
      DisplayWidth = 12
      FieldName = 'CB_NIVEL1'
      Size = 12
    end
    object cdsKardexCB_NIVEL2: TStringField
      DisplayWidth = 12
      FieldName = 'CB_NIVEL2'
      Size = 12
    end
    object cdsKardexCB_NIVEL3: TStringField
      DisplayWidth = 12
      FieldName = 'CB_NIVEL3'
      Size = 12
    end
    object cdsKardexCB_NIVEL4: TStringField
      DisplayWidth = 12
      FieldName = 'CB_NIVEL4'
      Size = 12
    end
    object cdsKardexCB_NIVEL5: TStringField
      DisplayWidth = 12
      FieldName = 'CB_NIVEL5'
      Size = 12
    end
    object cdsKardexCB_NIVEL6: TStringField
      DisplayWidth = 12
      FieldName = 'CB_NIVEL6'
      Size = 12
    end
    object cdsKardexCB_NIVEL7: TStringField
      DisplayWidth = 12
      FieldName = 'CB_NIVEL7'
      Size = 12
    end
    object cdsKardexCB_NIVEL8: TStringField
      DisplayWidth = 12
      FieldName = 'CB_NIVEL8'
      Size = 12
    end
    object cdsKardexCB_NIVEL9: TStringField
      DisplayWidth = 12
      FieldName = 'CB_NIVEL9'
      Size = 12
    end
    object cdsKardexCB_NIVEL10: TStringField
      DisplayWidth = 12
      FieldName = 'CB_NIVEL10'
      Size = 12
    end
    object cdsKardexCB_NIVEL11: TStringField
      DisplayWidth = 12
      FieldName = 'CB_NIVEL11'
      Size = 12
    end
    object cdsKardexCB_NIVEL12: TStringField
      DisplayWidth = 12
      FieldName = 'CB_NIVEL12'
      Size = 12
    end
    object cdsKardexCB_NOMTIPO: TSmallintField
      FieldName = 'CB_NOMTIPO'
    end
    object cdsKardexCB_NOMYEAR: TSmallintField
      FieldName = 'CB_NOMYEAR'
    end
    object cdsKardexCB_NOMNUME: TSmallintField
      FieldName = 'CB_NOMNUME'
    end
    object cdsKardexCB_REINGRE: TStringField
      FieldName = 'CB_REINGRE'
      Size = 1
    end
    object cdsKardexCB_NOTA: TMemoField
      FieldName = 'CB_NOTA'
      BlobType = ftMemo
      Size = 1
    end
    object cdsKardexCB_FEC_ING: TDateTimeField
      FieldName = 'CB_FEC_ING'
    end
    object cdsKardexCB_FEC_ANT: TDateTimeField
      FieldName = 'CB_FEC_ANT'
    end
    object cdsKardexCB_PLAZA: TIntegerField
      FieldName = 'CB_PLAZA'
    end
    object cdsKardexCB_NOMINA: TIntegerField
      DisplayWidth = 10
      FieldName = 'CB_NOMINA'
    end
    object cdsKardexCB_RECONTR: TStringField
      DisplayWidth = 1
      FieldName = 'CB_RECONTR'
      Size = 1
    end
    object cdsKardexCB_FEC_COV: TDateField
      FieldName = 'CB_FEC_COV'
    end
    object cdsKardexCB_LOT_IDS: TStringField
      FieldName = 'CB_LOT_IDS'
      Size = 40
    end
    object cdsKardexCB_FEC_IDS: TDateField
      FieldName = 'CB_FEC_IDS'
    end
  end
  object cdsKarFija: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 104
    Top = 72
    object cdsKarFijaCB_CODIGO: TIntegerField
      FieldName = 'CB_CODIGO'
    end
    object cdsKarFijaCB_FECHA: TDateTimeField
      FieldName = 'CB_FECHA'
    end
    object cdsKarFijaCB_TIPO: TStringField
      FieldName = 'CB_TIPO'
      Size = 6
    end
    object cdsKarFijaKF_FOLIO: TSmallintField
      FieldName = 'KF_FOLIO'
    end
    object cdsKarFijaKF_CODIGO: TStringField
      FieldName = 'KF_CODIGO'
      Size = 2
    end
    object cdsKarFijaKF_MONTO: TFloatField
      FieldName = 'KF_MONTO'
    end
    object cdsKarFijaKF_GRAVADO: TFloatField
      FieldName = 'KF_GRAVADO'
    end
    object cdsKarFijaKF_IMSS: TSmallintField
      FieldName = 'KF_IMSS'
    end
  end
  object cdsPrestamosEmp: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 184
    Top = 72
  end
  object cdsTemp: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 160
    Top = 8
  end
end
