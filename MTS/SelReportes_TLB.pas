unit SelReportes_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision: 5$
// File generated on 04/07/2006 04:20:04 p.m. from Type Library described below.

// ************************************************************************ //
// Type Lib: D:\3win_20\MTS\SelReportes.tlb (1)
// IID\LCID: {991F7BC6-B293-11D5-9249-0050DA04EAA0}\0
// Helpfile: 
// DepndLst: 
//   (1) v1.0 Midas, (C:\WINDOWS\system32\midas.dll)
//   (2) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
//   (3) v4.0 StdVCL, (C:\WINDOWS\system32\stdvcl40.dll)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, OleCtrls, StdVCL, 
  MIDAS;

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  SelReportesMajorVersion = 1;
  SelReportesMinorVersion = 0;

  LIBID_SelReportes: TGUID = '{991F7BC6-B293-11D5-9249-0050DA04EAA0}';

  IID_IdmServerSelReportes: TGUID = '{991F7BC7-B293-11D5-9249-0050DA04EAA0}';
  CLASS_dmServerSelReportes: TGUID = '{991F7BC9-B293-11D5-9249-0050DA04EAA0}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerSelReportes = interface;
  IdmServerSelReportesDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerSelReportes = IdmServerSelReportes;


// *********************************************************************//
// Interface: IdmServerSelReportes
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {991F7BC7-B293-11D5-9249-0050DA04EAA0}
// *********************************************************************//
  IdmServerSelReportes = interface(IAppServer)
    ['{991F7BC7-B293-11D5-9249-0050DA04EAA0}']
    function  GeneraSQL(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant; 
                        out Error: WideString): OleVariant; safecall;
    function  EvaluaParam(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant; 
                          out Error: WideString): WordBool; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerSelReportesDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {991F7BC7-B293-11D5-9249-0050DA04EAA0}
// *********************************************************************//
  IdmServerSelReportesDisp = dispinterface
    ['{991F7BC7-B293-11D5-9249-0050DA04EAA0}']
    function  GeneraSQL(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant; 
                        out Error: WideString): OleVariant; dispid 1;
    function  EvaluaParam(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant; 
                          out Error: WideString): WordBool; dispid 2;
    function  AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; 
                              MaxErrors: Integer; out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function  AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                            Options: Integer; const CommandText: WideString; 
                            var Params: OleVariant; var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function  AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function  AS_GetProviderNames: OleVariant; dispid 20000003;
    function  AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function  AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                            var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerSelReportes provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerSelReportes exposed by              
// the CoClass dmServerSelReportes. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerSelReportes = class
    class function Create: IdmServerSelReportes;
    class function CreateRemote(const MachineName: string): IdmServerSelReportes;
  end;

implementation

uses ComObj;

class function CodmServerSelReportes.Create: IdmServerSelReportes;
begin
  Result := CreateComObject(CLASS_dmServerSelReportes) as IdmServerSelReportes;
end;

class function CodmServerSelReportes.CreateRemote(const MachineName: string): IdmServerSelReportes;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerSelReportes) as IdmServerSelReportes;
end;

end.
