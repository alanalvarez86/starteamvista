library TimbradoSistema;

uses
  ComServ,
  TimbradoSistema_TLB in 'TimbradoSistema_TLB.pas',
  DServerSistemaTimbrado in 'DServerSistemaTimbrado.pas' {dmServerSistema: TMtsDataModule} {dmServerSistemaTimbrado: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
