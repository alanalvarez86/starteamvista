unit Diccionario_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 3/27/2009 2:37:12 PM from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3win_20\MTS\Diccionario.tlb (1)
// LIBID: {B81B3ED7-88CE-11D3-8DEC-0050DA04EAA0}
// LCID: 0
// Helpfile: 
// HelpString: Tress Diccionario
// DepndLst: 
//   (1) v1.0 Midas, (C:\WINDOWS\system32\midas.dll)
//   (2) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, Midas, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  DiccionarioMajorVersion = 1;
  DiccionarioMinorVersion = 0;

  LIBID_Diccionario: TGUID = '{B81B3ED7-88CE-11D3-8DEC-0050DA04EAA0}';

  IID_IdmServerDiccionario: TGUID = '{B81B3ED8-88CE-11D3-8DEC-0050DA04EAA0}';
  CLASS_dmServerDiccionario: TGUID = '{B81B3EDA-88CE-11D3-8DEC-0050DA04EAA0}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerDiccionario = interface;
  IdmServerDiccionarioDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerDiccionario = IdmServerDiccionario;


// *********************************************************************//
// Interface: IdmServerDiccionario
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {B81B3ED8-88CE-11D3-8DEC-0050DA04EAA0}
// *********************************************************************//
  IdmServerDiccionario = interface(IAppServer)
    ['{B81B3ED8-88CE-11D3-8DEC-0050DA04EAA0}']
    function GrabaDiccion(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetListaFunciones(Empresa: OleVariant): OleVariant; safecall;
    function GetListaGlobal(Empresa: OleVariant): OleVariant; safecall;
    function PruebaFormula(Empresa: OleVariant; Parametros: OleVariant; var Formula: WideString; 
                           Entidad: Integer; ParamsRep: OleVariant): WordBool; safecall;
    function GetDiccionario(Empresa: OleVariant; lverConfidencial: WordBool; 
                            const Filtro: WideString): OleVariant; safecall;
    function SQLValido(Empresa: OleVariant; const Formula: WideString; Entidad: Integer; 
                       var MsgError: WideString): OleVariant; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerDiccionarioDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {B81B3ED8-88CE-11D3-8DEC-0050DA04EAA0}
// *********************************************************************//
  IdmServerDiccionarioDisp = dispinterface
    ['{B81B3ED8-88CE-11D3-8DEC-0050DA04EAA0}']
    function GrabaDiccion(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 8;
    function GetListaFunciones(Empresa: OleVariant): OleVariant; dispid 9;
    function GetListaGlobal(Empresa: OleVariant): OleVariant; dispid 10;
    function PruebaFormula(Empresa: OleVariant; Parametros: OleVariant; var Formula: WideString; 
                           Entidad: Integer; ParamsRep: OleVariant): WordBool; dispid 11;
    function GetDiccionario(Empresa: OleVariant; lverConfidencial: WordBool; 
                            const Filtro: WideString): OleVariant; dispid 12;
    function SQLValido(Empresa: OleVariant; const Formula: WideString; Entidad: Integer; 
                       var MsgError: WideString): OleVariant; dispid 301;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer; 
                             out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                           Options: Integer; const CommandText: WideString; var Params: OleVariant; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerDiccionario provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerDiccionario exposed by              
// the CoClass dmServerDiccionario. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerDiccionario = class
    class function Create: IdmServerDiccionario;
    class function CreateRemote(const MachineName: string): IdmServerDiccionario;
  end;

implementation

uses ComObj;

class function CodmServerDiccionario.Create: IdmServerDiccionario;
begin
  Result := CreateComObject(CLASS_dmServerDiccionario) as IdmServerDiccionario;
end;

class function CodmServerDiccionario.CreateRemote(const MachineName: string): IdmServerDiccionario;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerDiccionario) as IdmServerDiccionario;
end;

end.
