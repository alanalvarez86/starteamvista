unit DServerVisReportes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComServ, ComObj, VCLCom, StdVcl, {BdeMts,} DataBkr, DBClient,
  MtsRdm, Mtx,
  {$ifndef DOS_CAPAS}
  VisReportes_TLB,
  {$endif}
  ZCreator,
  DZetaServerProvider;

type
  TdmServerVisReportes = class(TMtsDataModule {$ifndef DOS_CAPAS}, IdmServerVisReportes {$endif})
    procedure MtsDataModuleCreate(Sender: TObject);
    procedure MtsDataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    oZetaProvider : TdmZetaServerProvider;
    oZetaCreator : TZetaCreator;
    procedure InitCreator;
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
  {$ifdef DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
    {$endif}
    function GeneraSQL(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant; out Error: WideString): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function EvaluaParam(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant; out Error: WideString): WordBool; {$ifndef DOS_CAPAS}safecall;{$endif}
  end;

var
  dmServerVisReportes: TdmServerVisReportes;

implementation
uses DServerReporteador,
     ZetaServerTools,
     ZetaSQLBroker;

{$R *.DFM}

class procedure TdmServerVisReportes.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

procedure TdmServerVisReportes.MtsDataModuleCreate(Sender: TObject);
begin
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
end;

procedure TdmServerVisReportes.MtsDataModuleDestroy(Sender: TObject);
begin
     ZetaSQLBroker.FreeZetaCreator( oZetaCreator );
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
end;

procedure TdmServerVisReportes.InitCreator;
begin
     if not Assigned( oZetaCreator ) then
        oZetaCreator := ZetaSQLBroker.GetZetaCreator( oZetaProvider );
end;

function TdmServerVisReportes.GeneraSQL(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant; out Error: WideString): OleVariant;
var
   Reporte: TdmServerReporteador;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
     end;
     InitCreator;
     Reporte := TdmServerReporteador.Create( oZetaProvider, oZetaCreator );
     try
        Result := Reporte.GeneraSQL( Empresa,
                                     AgenteSQL,
                                     Parametros,
                                     Error );
     finally
            FreeAndNil(Reporte);
     end;
     SetComplete;
end;

function TdmServerVisReportes.EvaluaParam(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant; out Error: WideString): WordBool;
var
   Reporte: TdmServerReporteador;
begin
     InitCreator;
     Reporte := TdmServerReporteador.Create( oZetaProvider, oZetaCreator );
     try
        Result := Reporte.EvaluaParam( Empresa,
                                       AgenteSQL,
                                       Parametros,
                                       Error );
     finally
            FreeAndNil(Reporte);
     end;
     SetComplete;
end;

{$ifdef DOS_CAPAS}
procedure TdmServerVisReportes.CierraEmpresa;
begin

end;
{$endif}

{$ifndef DOS_CAPAS}
initialization
  TComponentFactory.Create(ComServer, TdmServerVisReportes, Class_dmServerVisReportes, ciMultiInstance, ZetaServerTools.GetThreadingModel);
{$endif}
end.
