unit DServerCalcNomina;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      DServerCalcNomina.pas                      ::
  :: Descripci�n: C�lculo de N�mina (DCalcNomina.dll )       ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface
{$define DEBUGSENTINEL}
{$define CUENTA_EMPLEADOS}
{$define QUINCENALES}
{$define CAMBIO_TNOM}
{.$undefine QUINCENALES}
{.$define MODULO_PRESTADO_ELECTROCOMPONENTES_TRESSENLINEA}
{.$define MODULO_PRESTADO_EATON_TRESSENLINEA}
{.$define MODULO_PRESTADO_FRISAFORJADOS_TRESSENLINEA}
{$define MODULO_PRESTADO_SEBN_TRESSENLINEA}


uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComServ, ComObj, VCLCom, StdVcl, DataBkr, DBClient, DB, MtsRdm, Mtx,Mask,
     Variants,MaskUtils,
{$ifndef DOS_CAPAS}
{$ifndef CALCNOMINA_COMO_OBJETO}
     DCalcNomina_TLB,
{$endif}
{$endif}
     DCalculoNomina,
     DTarjeta,
     DNominaClass,
     DZetaServerProvider,
     ZCreator,
     ZetaSQLBroker,
     ZetaCommonLists,
     ZetaServerDataSet,
     FAutoServer,
     FAutoClasses;

type
  TdmServerCalcNomina = class(TMtsDataModule {$ifndef DOS_CAPAS}{$ifndef CALCNOMINA_COMO_OBJETO}, IdmServerCalcNomina {$endif}{$endif})
    cdsLista: TServerDataSet;
    cdsLog: TServerDataSet;
    procedure MtsDataModuleCreate(Sender: TObject);
    procedure MtsDataModuleDestroy(Sender: TObject);

  private
    { Private declarations }
    {$ifdef BITACORA_DLLS}
    FListaLog : TAsciiLog;
    FPaletita : string;
    FArchivoLog: string;
    FEmpresa:string;
    {$endif}
    oZetaProvider: TdmZetaServerProvider;
{$ifndef DOS_CAPAS}
    oZetaCreator: TZetaCreator;
    oZetaSQLBroker: TSQLBroker;
    oAutoServer: TAutoServer;
{$endif}
    FCalcNomina: TCalcNomina;
    {.$IFDEF RETROACTIVO}
    FRetroactivo : TRetroactivo;
    {.$endif}
    FNomina: TNomina;
    FTarjeta: TTarjeta;
    FTransformar: Boolean;
    FEmployee: TZetaCursor;
    FListaParametros: string;
    FBorraChecadas: TZetaCursor;
    property CalcNomina: TCalcNomina read FCalcNomina;
    function BuildEmpresa(DataSet: TZetaCursor): OleVariant;
    function CalcularPrenominaDataset(Dataset: TDataset; const eCalculo: eTipoCalculo): OleVariant;
    function ConfiguraBorrarAuto(var sLista: String): Boolean;
    {$ifndef CUENTA_EMPLEADOS}
    function MaxEmpleados: Integer;
    {$endif}
    function PollWrite( Datos: OleVariant; const sEmpresa, sCampo: String ): Integer;
    {$IFDEF INTERFAZ_ELECTROLUX_KRONOS}
    function PollWriteKronos( Datos: OleVariant; const sEmpresa, sCampo: String; InicioPeriodo, FinPeriodo: TDate ): Integer;
    {$ENDIF}
    function RecalcularTarjetasDataset(Dataset: TDataset): OleVariant;
    function RegistrosAutomaticosDataset(Dataset: TDataset): OleVariant;
    procedure ClearCalcNomina;
    procedure ClearTarjeta;
    procedure ClearNomina;
    procedure InitBroker;
    procedure InitCalcNomina;
    procedure InitCreator;
    procedure InitNomina;
    procedure InitQueries;
    procedure InitRitmos;
    procedure InitTarjeta;
    {.$IFDEF RETROACTIVO}
    procedure InitRetroactivo;
    procedure ClearRetroactivo;
    procedure CalculaRetroactivoParametros;
    function CalculaRetroactivoDataset( Empresa: oleVariant; DataSet : TDataSet ): Olevariant;
    {.$endif}

    procedure CalcularPrenominaBuildDataset( const eCalculo: eTipoCalculo );
    procedure ModuloAutorizado(const eModulo: TModulos);
    procedure PollError(const sDatos, sError: String);
    procedure PollExaminaASCII( var sDatos: String );
    procedure PollException(const sDatos: String; Error: Exception);
    procedure PollValidaASCII(DataSet: TDataset; var iProblemas: Integer; var sErrorMsg: String);
    procedure RecalcularTarjetasBuildDataset;
    procedure RegistrosAutomaticosBuildDataset;
    procedure CalculaPreNominaParametros;
    procedure PollParametros;
    procedure RecalcularTarjetasParametros;
    procedure RegistrosAutomaticosParametros;
    procedure TransferenciaParametros( const sEmpresaDestino: string );
    function GetDigitoEmpresa( const sCodigoEmpresa: String ): String;
    procedure TransfiereUsuario( Empresa, EmpresaDestino: OleVariant; const iEmpleado, iNewEmpleado : integer );
    function ProcesaTarjetas(Empresa,Parametros: OleVariant): OleVariant;
    function CalculaNominaSimulacion(Empresa,Parametros: OleVariant): OleVariant;

    procedure InitLog(Empresa: Olevariant; const sPaletita: string);
    procedure EndLog;
    {$ifndef DOS_CAPAS}
    procedure ActualizaEmpresaEmpId( sEmpresa: String; iNumeroBiometrico, iNewEmpleado: Integer );
    {$endif}
    
  protected
    { Protected declarations }
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;

{$ifdef DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
{$endif}
{$ifdef CALCNOMINA_COMO_OBJETO}
    public
{$endif}
    function CalculaNetoBruto(Empresa: OleVariant; var Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function CalculaNomina(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function CalculaPreNomina(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GeneraSQL(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant; out Error: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function EvaluaParam(Empresa: OleVariant; var AgenteSQL: OleVariant;Parametros: OleVariant; out Error: WideString): WordBool; {$ifndef DOS_CAPAS} safecall;{$endif}
    function Poll(Empresa, Parametros, ListaASCII: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function PollTotal(Parametros: OleVariant; var Lista: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function ProcesarTarjetasPendientes(Empresa,Parametros: OleVariant): OleVariant;  {$ifndef DOS_CAPAS} safecall; {$endif}
    function RastreaConcepto(Empresa, Parametros: OleVariant; out Rastreo: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function RecalcularTarjetas(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function RegistrosAutomaticos(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function Transferencia(Empresa, EmpresaDestino, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function CalculaRetroactivo(Empresa, Parametros: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall; {$endif}
    function CalculaRetroactivoGetLista(Empresa,Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function CalculaRetroactivoLista(Empresa, Lista,Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function RecalcularTarjetasSimple(Empresa,Parametros: OleVariant): WideString; {$ifndef DOS_CAPAS} safecall;{$endif}
    function ProcesarTarjetasSimple(Empresa, Parametros: OleVariant): WideString; {$ifndef CALCNOMINA_COMO_OBJETO}{$ifndef DOS_CAPAS} safecall;{$endif}{$endif}
  end;

implementation

uses ZetaCommonClasses,
     ZetaCommonTools,
     ZetaServerTools,
     ZetaTipoEntidad,
{$ifdef DEBUGSENTINEL}
     ZetaLicenseMgr,
{$endif}
     ZGlobalTress,
     DSeguridad,
     DQueries,
     DSuperASCII,
     DServerReportesIBO;

{$R *.DFM}

const
     K_ANCHO_EMPRESA = 1;
     K_ANCHO_LINX = 11;
     K_ANCHO_LETRA = 1;
     K_ANCHO_POLL_DATA = 50;
     K_ANCHO_POLL_ERROR = 255;
     K_ANCHO_G_TEX = 30;
     Q_POLL_PENDIENTES = 1;
     Q_POLL_BORRA = 2;
     Q_POLL_RANGO_FECHAS = 3;
     Q_POLL_ESCRIBE = 4;
     Q_COMPANYS_FIND = 5;
     Q_COMPANYS_LIST = 6;
     Q_COMPANYS_COUNT = 7;
     Q_RECALCULAR_TARJETAS_BORRA_AUTO = 8;
     Q_RECALCULAR_TARJETAS_REVISA_HORARIO = 9;
     Q_RECALCULAR_TARJETAS_RECLASIFICAR = 10;
     Q_HAY_CHECADAS = 11;
     Q_TRANSFERENCIA_EXISTE_EMPLEADO = 12;
     Q_TRANSFERENCIA_LEE = 13;
     Q_TRANSFERENCIA_BORRA = 14;
     Q_TURNO_LEE = 15;
     Q_EMPLEADO_FIND = 16;
     Q_POLL_GET_DIGITO = 17;
     Q_EMP_ID_FIND = 18;
     Q_AGREGA_KARDEX = 20;{OP: 28/05/08}
     Q_RECALCULA_KARDEX = 21;{OP: 28/05/08}
{$ifndef ANTES}
     Q_EMPLEADO_TIPO_NOMINA = 19;
{$endif}

     Q_CHECADAS_SIN_MODIFICAR = 22;

     Q_BORRA_CHECADAS_SIN_MODIFICAR  = 23;

     Q_TRANSFERENCIA_MEDICOS_LEE = 24;
     Q_TRANSFERENCIA_MEDICOS_BORRA = 25;

     Q_DISPOSITIVOS_X_COMPANIA = 26; {OP: 11/02/09}
     Q_POLL_RANGO_FECHAS_DISP = 27; {OP: 11/02/09}
     Q_POLL_PENDIENTES_DISP = 28; {OP: 11/02/09}
     Q_POLL_BORRA_DISP = 29; {OP: 11/02/09}

     {$ifndef DOS_CAPAS}
     Q_ACTUALIZA_CM_CODIGO_EMP_BIO = 30;
     {$endif}

     K_QTY_TABLAS_MEDICO = 4;

     {
     Con este query se averiguan las tablas que tienen CB_CODIGO:

     select DISTINCT( RDB$RELATION_NAME) from RDB$RELATION_FIELDS where
     RDB$FIELD_NAME = 'CB_CODIGO' group by RDB$RELATION_NAME

     Es necesario incluirlas en el arreglo A_TABLAS_TRANSFERENCIA
     y en TressCFG en el proceso de Transferencia de Empleados
     D:\3Win_20\Configuracion\DCompara.pas

     OJO con las dependencias: A_TABLAS_TRANSFERENCIA debe ir ordenado
     primero con las tablas padre y luego las hijas

     Las tablas ACCIDENT, CONSULTA, EMBARAZO, MED_ENTR utilizan el Query
     Q_TRANSFERENCIA_MEDICOS_LEE, porque no tienen campo colabora

     }

     A_TABLAS_TRANSFERENCIA: array[ 1..{$ifndef DOS_CAPAS}42{$else}38{$endif} ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'COLABORA',
                                                         'ACUMULA',
                                                         'ACUMULA_RS',
                                                         'AUSENCIA',
                                                         'CHECADAS',
                                                         'ANTESCUR',
                                                         'ANTESPTO',
                                                         'IMAGEN',
                                                         'PARIENTE',
                                                         'INCAPACI',
                                                         'VACACION',
                                                         'PERMISO',
                                                         'AHORRO',
                                                         'ACAR_ABO',
                                                         'PRESTAMO',
                                                         'PCAR_ABO',
                                                         'KARDEX',
                                                         'KAR_FIJA',
                                                         'KARCURSO',
                                                         'AGUINAL',
                                                         'REP_AHO',
                                                         'REP_PTU',
                                                         'COMPARA',
                                                         'DECLARA',
                                                         'CAF_COME',
                                                         'ASIGNA',
                                                         'KAR_TOOL',
                                                         'CED_EMP',
                                                         'KAR_AREA',
                                                         'WOFIJA',
                                                         'WORKS',
                                                         'EXPEDIEN',
                                                         'ACCIDENT',
                                                         'CONSULTA',
                                                         'EMBARAZO',
                                                         'MED_ENTR',
                                                         'KARINF',
                                                         'KAR_CERT'
                                                         {$ifndef DOS_CAPAS}
                                                         , 'HUELLA'
                                                         {$endif}
                                                         , 'DOCUMENTO'
                                                         ,'PENSION'
                                                         ,'PEN_PORCEN'
                                                         );

     A_TABLAS_MEDICO: array[ 1..K_QTY_TABLAS_MEDICO ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'ACCIDENT',
                                                                   'CONSULTA',
                                                                   'EMBARAZO',
                                                                   'MED_ENTR' );

     Q_TRANSFERENCIA_RESPALDA = 31;

function GetSQLScript( const iScript: Integer ): String;
begin
     case iScript of
          Q_POLL_PENDIENTES: Result := 'select PO_LINX, PO_NUMERO, PO_EMPRESA, PO_FECHA, PO_HORA, PO_LETRA from POLL where ( PO_EMPRESA = ''%s'' ) order by PO_NUMERO, PO_FECHA, PO_HORA';  { El Orden Es CRUCIAL para el Poll }
          Q_POLL_BORRA: Result := 'delete from POLL where ( PO_EMPRESA = ''%s'' )';
          Q_POLL_RANGO_FECHAS: Result := 'select MIN( PO_FECHA ) FECHA_MIN, MAX( PO_FECHA ) FECHA_MAX, COUNT(*) TOTAL from POLL where ( PO_EMPRESA = ''%s'' ) ';
          Q_POLL_ESCRIBE: Result := 'insert into POLL( PO_LINX, '+
                                                      'PO_EMPRESA, '+
                                                      'PO_NUMERO, '+
                                                      'PO_LETRA, '+
                                                      'PO_FECHA, '+
                                                      'PO_HORA ) values ( '+
                                                      ':PO_LINX, '+
                                                      ':PO_EMPRESA, '+
                                                      ':PO_NUMERO, '+
                                                      ':PO_LETRA, '+
                                                      ':PO_FECHA, '+
                                                      ':PO_HORA )';
          Q_COMPANYS_FIND: Result := 'select CM_NOMBRE, CM_ALIAS, CM_USRNAME, CM_PASSWRD, CM_NIVEL0, CM_DATOS, CM_CODIGO from COMPANY where ( CM_CODIGO = ''%s'' )';
          Q_COMPANYS_LIST: Result := 'select C.CM_NOMBRE, C.CM_ALIAS, C.CM_USRNAME, C.CM_PASSWRD, C.CM_NIVEL0, C.CM_DATOS, C.CM_CODIGO from COMPANY C where ( %s ) and '+
                                     '( C.CM_DIGITO is NOT NULL ) and '+
                                     '( C.CM_DIGITO <> '' '' ) and '+
                                     '( ( select COUNT(*) from POLL P where ( P.PO_EMPRESA = C.CM_DIGITO ) ) > 0 ) '+
                                     'order by C.CM_CODIGO';
          Q_COMPANYS_COUNT: Result := 'select COUNT(*) CUANTOS from COMPANY C where ( %s ) and '+
                                      '( C.CM_DIGITO is NOT NULL ) and '+
                                      '( C.CM_DIGITO <> '' '' ) and '+
                                      '( ( select COUNT(*) from POLL P where ( P.PO_EMPRESA = C.CM_DIGITO ) ) > 0 )';
          Q_RECALCULAR_TARJETAS_BORRA_AUTO: Result := 'delete from CHECADAS where ( CB_CODIGO = :Empleado ) and ( AU_FECHA = :Fecha ) and ( CH_TIPO in ( %s ) ) %s';
          Q_RECALCULAR_TARJETAS_REVISA_HORARIO: Result := 'update AUSENCIA set '+
                                                          'AU_HOR_MAN = :EsManual, '+
                                                          'AU_STATUS = :Status, '+
                                                          'HO_CODIGO = :Horario '+
                                                          'where ( AU_FECHA = :Fecha ) and '+
                                                          '( CB_CODIGO = :Empleado )';
          Q_RECALCULAR_TARJETAS_RECLASIFICAR: Result := 'execute procedure SP_CLAS_AUSENCIA( :Empleado, :Fecha )';
          Q_HAY_CHECADAS: Result := 'select COUNT(*) CUANTOS, MAX( CH_H_AJUS ) MAXIMO from CHECADAS where '+
                                    '( CB_CODIGO = :Empleado ) and '+
                                    '( AU_FECHA = :Fecha ) and '+
                                    Format( '( CH_TIPO in ( %d, %d ) ) and ', [  Ord( chEntrada ), Ord( chSalida ) ] ) +
                                    '( CH_SISTEMA <> ' + ZetaCommonTools.EntreComillas( K_GLOBAL_SI ) + ' )';
          Q_TRANSFERENCIA_EXISTE_EMPLEADO: Result := 'select CB_CODIGO from COLABORA where ( CB_CODIGO = %d )';
          Q_TRANSFERENCIA_LEE: Result := 'select * from %s where ( CB_CODIGO = %d )';
          Q_TRANSFERENCIA_MEDICOS_LEE: Result := 'select * from %s where ( EX_CODIGO = ( select EX_CODIGO from EXPEDIEN where CB_CODIGO = %d ) )';
          Q_TRANSFERENCIA_BORRA: Result := 'delete from %s where ( CB_CODIGO = %d )';


          { US #5617 Adecuar transferencia individual de empleados para respaldar a la nueva tabla EMP_TRANSF}
          { Inicio }
          Q_TRANSFERENCIA_RESPALDA: Result :=
          'INSERT INTO EMP_TRANSF ' +
          '([CB_CODIGO],[CB_ACTIVO],[CB_APE_MAT]      ,[CB_APE_PAT]      ,[CB_AUTOSAL]      ,[CB_AR_FEC]      ,[CB_AR_HOR]      ,[CB_BAN_ELE]      ,[CB_CARRERA]      ,[CB_CHECA]      ,[CB_CIUDAD]      ,[CB_CLASIFI] ' +
          '      ,[CB_CODPOST]      ,[CB_CONTRAT]      ,[CB_CREDENC]      ,[CB_CURP]      ,[CB_CALLE]      ,[CB_COLONIA]      ,[CB_EDO_CIV]      ,[CB_FEC_RES]      ,[CB_EST_HOR]      ,[CB_EST_HOY]      ,[CB_ESTADO]      ,[CB_ESTUDIO]      ,[CB_EVALUA] ' +
          '      ,[CB_EXPERIE]      ,[CB_FEC_ANT]      ,[CB_FEC_BAJ]      ,[CB_FEC_BSS]      ,[CB_FEC_CON]      ,[CB_FEC_ING]      ,[CB_FEC_INT]      ,[CB_FEC_NAC]      ,[CB_FEC_REV]      ,[CB_FEC_VAC] ' +
          '      ,[CB_G_FEC_1]      ,[CB_G_FEC_2]      ,[CB_G_FEC_3]      ,[CB_G_FEC_4]      ,[CB_G_FEC_5]      ,[CB_G_FEC_6] ' +
          '      ,[CB_G_FEC_7]      ,[CB_G_FEC_8]      ,[CB_G_LOG_1]      ,[CB_G_LOG_2]      ,[CB_G_LOG_3]      ,[CB_G_LOG_4]      ,[CB_G_LOG_5] ' +
          '      ,[CB_G_LOG_6]      ,[CB_G_LOG_7]      ,[CB_G_LOG_8]      ,[CB_G_NUM_1]      ,[CB_G_NUM_2]      ,[CB_G_NUM_3]      ,[CB_G_NUM_4]      ,[CB_G_NUM_5] ' +
          '      ,[CB_G_NUM_6]      ,[CB_G_NUM_7]      ,[CB_G_NUM_8]      ,[CB_G_NUM_9]      ,[CB_G_NUM10]      ,[CB_G_NUM11]      ,[CB_G_NUM12] ' +
          '      ,[CB_G_NUM13]      ,[CB_G_NUM14]      ,[CB_G_NUM15]      ,[CB_G_NUM16]      ,[CB_G_NUM17]      ,[CB_G_NUM18]      ,[CB_G_TAB_1] ' +
          '      ,[CB_G_TAB_2]      ,[CB_G_TAB_3]      ,[CB_G_TAB_4]      ,[CB_G_TAB_5]      ,[CB_G_TAB_6]      ,[CB_G_TAB_7]      ,[CB_G_TAB_8]      ,[CB_G_TAB_9] ' +
          '      ,[CB_G_TAB10]      ,[CB_G_TAB11]      ,[CB_G_TAB12]      ,[CB_G_TAB13]      ,[CB_G_TAB14]      ,[CB_G_TEX_1]      ,[CB_G_TEX_2] ' +
          '      ,[CB_G_TEX_3]      ,[CB_G_TEX_4]      ,[CB_G_TEX_5]      ,[CB_G_TEX_6]      ,[CB_G_TEX_7]      ,[CB_G_TEX_8]      ,[CB_G_TEX_9] ' +
          '      ,[CB_G_TEX10]      ,[CB_G_TEX11]      ,[CB_G_TEX12]      ,[CB_G_TEX13]      ,[CB_G_TEX14]      ,[CB_G_TEX15]      ,[CB_G_TEX16] ' +
          '      ,[CB_G_TEX17]      ,[CB_G_TEX18]      ,[CB_G_TEX19]      ,[CB_HABLA]      ,[CB_TURNO]      ,[CB_IDIOMA]      ,[CB_INFCRED]      ,[CB_INFMANT] ' +
          '      ,[CB_INFTASA]      ,[CB_LA_MAT]      ,[CB_LAST_EV]      ,[CB_LUG_NAC]      ,[CB_MAQUINA]      ,[CB_MED_TRA]      ,[CB_PASAPOR] ' +
          '      ,[CB_FEC_INC]      ,[CB_FEC_PER]      ,[CB_NOMYEAR]      ,[CB_NACION]      ,[CB_NOMTIPO]      ,[CB_NEXT_EV]      ,[CB_NOMNUME] ' +
          '      ,[CB_NOMBRES]      ,[CB_PATRON]      ,[CB_PUESTO]      ,[CB_RFC]      ,[CB_SAL_INT]      ,[CB_SALARIO]      ,[CB_SEGSOC] ' +
          '      ,[CB_SEXO]      ,[CB_TABLASS]      ,[CB_TEL]      ,[CB_VIVECON]      ,[CB_VIVEEN]      ,[CB_ZONA]      ,[CB_ZONA_GE]      ,[CB_NIVEL1] ' +
          '      ,[CB_NIVEL2]      ,[CB_NIVEL3]      ,[CB_NIVEL4]      ,[CB_NIVEL5]      ,[CB_NIVEL6]      ,[CB_NIVEL7]      ,[CB_INFTIPO] ' +
          '      ,[CB_NIVEL8]      ,[CB_NIVEL9]      ,[CB_DER_FEC]      ,[CB_DER_PAG]      ,[CB_V_PAGO]      ,[CB_DER_GOZ]      ,[CB_V_GOZO] ' +
          '      ,[CB_TIP_REV]      ,[CB_MOT_BAJ]      ,[CB_FEC_SAL]      ,[CB_INF_INI]      ,[CB_INF_OLD]      ,[CB_OLD_SAL]      ,[CB_OLD_INT] ' +
          '      ,[CB_PRE_INT]      ,[CB_PER_VAR]      ,[CB_SAL_TOT]      ,[CB_TOT_GRA]      ,[CB_FAC_INT]      ,[CB_RANGO_S]      ,[CB_CLINICA] ' +
          '      ,[CB_NIVEL0]      ,[CB_FEC_NIV]      ,[CB_FEC_PTO]      ,[CB_AREA]      ,[CB_FEC_TUR]      ,[CB_FEC_KAR] ' +
          '      ,[CB_PENSION]      ,[CB_CANDIDA]      ,[CB_ID_NUM]      ,[CB_ENT_NAC]      ,[CB_COD_COL]      ,[CB_PASSWRD]      ,[CB_PLAZA] ' +
          '      ,[CB_FEC_PLA]      ,[CB_DER_PV]      ,[CB_V_PRIMA]      ,[CB_SUB_CTA]      ,[CB_NETO]      ,[CB_E_MAIL]      ,[CB_PORTAL] ' +
          '      ,[CB_DNN_OK]      ,[CB_USRNAME]      ,[CB_NOMINA]      ,[CB_FEC_NOM]      ,[CB_RECONTR]      ,[CB_DISCAPA]      ,[CB_INDIGE] ' +
          '      ,[CB_FONACOT]      ,[CB_EMPLEO]      ,[US_CODIGO]      ,[CB_FEC_COV]      ,[CB_G_TEX20]      ,[CB_G_TEX21]      ,[CB_G_TEX22]      ,[CB_G_TEX23] ' +
          '      ,[CB_G_TEX24]      ,[CB_INFDISM]      ,[CB_INFACT]      ,[CB_NUM_EXT]      ,[CB_NUM_INT]      ,[CB_INF_ANT] ' +
          '     ,[CB_TDISCAP]      ,[CB_ESCUELA]      ,[CB_TESCUEL]      ,[CB_TITULO]      ,[CB_YTITULO]      ,[CB_CTA_GAS]      ,[CB_CTA_VAL] ' +
          '      ,[CB_MUNICIP]      ,[CB_ID_BIO]      ,[CB_GP_COD]      ,[CB_TIE_PEN]      ,[CB_TSANGRE]      ,[CB_ALERGIA]      ,[CB_BRG_ACT] ' +
          '      ,[CB_BRG_TIP]      ,[CB_BRG_ROL]      ,[CB_BRG_JEF]      ,[CB_BRG_CON]      ,[CB_BRG_PRA] ' +
          '      ,[CB_BRG_NOP]      ,[CB_BANCO]      ,[CB_REGIMEN]) ' +
          ' SELECT [CB_CODIGO],[CB_ACTIVO],[CB_APE_MAT]      ,[CB_APE_PAT]      ,[CB_AUTOSAL]      ,[CB_AR_FEC]      ,[CB_AR_HOR]      ,[CB_BAN_ELE]      ,[CB_CARRERA]      ,[CB_CHECA]      ,[CB_CIUDAD]      ,[CB_CLASIFI] ' +
          '      ,[CB_CODPOST]      ,[CB_CONTRAT]      ,[CB_CREDENC]      ,[CB_CURP]      ,[CB_CALLE]      ,[CB_COLONIA]      ,[CB_EDO_CIV]      ,[CB_FEC_RES]      ,[CB_EST_HOR]      ,[CB_EST_HOY]      ,[CB_ESTADO]      ,[CB_ESTUDIO]      ,[CB_EVALUA] ' +
          '      ,[CB_EXPERIE]      ,[CB_FEC_ANT]      ,[CB_FEC_BAJ]      ,[CB_FEC_BSS]      ,[CB_FEC_CON]      ,[CB_FEC_ING]      ,[CB_FEC_INT]      ,[CB_FEC_NAC]      ,[CB_FEC_REV]      ,[CB_FEC_VAC] ' +
          '      ,[CB_G_FEC_1]      ,[CB_G_FEC_2]      ,[CB_G_FEC_3]      ,[CB_G_FEC_4]      ,[CB_G_FEC_5]      ,[CB_G_FEC_6] ' +
          '      ,[CB_G_FEC_7]      ,[CB_G_FEC_8]      ,[CB_G_LOG_1]      ,[CB_G_LOG_2]      ,[CB_G_LOG_3]      ,[CB_G_LOG_4]      ,[CB_G_LOG_5] ' +
          '      ,[CB_G_LOG_6]      ,[CB_G_LOG_7]      ,[CB_G_LOG_8]      ,[CB_G_NUM_1]      ,[CB_G_NUM_2]      ,[CB_G_NUM_3]      ,[CB_G_NUM_4]      ,[CB_G_NUM_5] ' +
          '      ,[CB_G_NUM_6]      ,[CB_G_NUM_7]      ,[CB_G_NUM_8]      ,[CB_G_NUM_9]      ,[CB_G_NUM10]      ,[CB_G_NUM11]      ,[CB_G_NUM12] ' +
          '      ,[CB_G_NUM13]      ,[CB_G_NUM14]      ,[CB_G_NUM15]      ,[CB_G_NUM16]      ,[CB_G_NUM17]      ,[CB_G_NUM18]      ,[CB_G_TAB_1] ' +
          '      ,[CB_G_TAB_2]      ,[CB_G_TAB_3]      ,[CB_G_TAB_4]      ,[CB_G_TAB_5]      ,[CB_G_TAB_6]      ,[CB_G_TAB_7]      ,[CB_G_TAB_8]      ,[CB_G_TAB_9] ' +
          '      ,[CB_G_TAB10]      ,[CB_G_TAB11]      ,[CB_G_TAB12]      ,[CB_G_TAB13]      ,[CB_G_TAB14]      ,[CB_G_TEX_1]      ,[CB_G_TEX_2] ' +
          '      ,[CB_G_TEX_3]      ,[CB_G_TEX_4]      ,[CB_G_TEX_5]      ,[CB_G_TEX_6]      ,[CB_G_TEX_7]      ,[CB_G_TEX_8]      ,[CB_G_TEX_9] ' +
          '      ,[CB_G_TEX10]      ,[CB_G_TEX11]      ,[CB_G_TEX12]      ,[CB_G_TEX13]      ,[CB_G_TEX14]      ,[CB_G_TEX15]      ,[CB_G_TEX16] ' +
          '      ,[CB_G_TEX17]      ,[CB_G_TEX18]      ,[CB_G_TEX19]      ,[CB_HABLA]      ,[CB_TURNO]      ,[CB_IDIOMA]      ,[CB_INFCRED]      ,[CB_INFMANT] ' +
          '      ,[CB_INFTASA]      ,[CB_LA_MAT]      ,[CB_LAST_EV]      ,[CB_LUG_NAC]      ,[CB_MAQUINA]      ,[CB_MED_TRA]      ,[CB_PASAPOR] ' +
          '      ,[CB_FEC_INC]      ,[CB_FEC_PER]      ,[CB_NOMYEAR]      ,[CB_NACION]      ,[CB_NOMTIPO]      ,[CB_NEXT_EV]      ,[CB_NOMNUME] ' +
          '      ,[CB_NOMBRES]      ,[CB_PATRON]      ,[CB_PUESTO]      ,[CB_RFC]      ,[CB_SAL_INT]      ,[CB_SALARIO]      ,[CB_SEGSOC] ' +
          '      ,[CB_SEXO]      ,[CB_TABLASS]      ,[CB_TEL]      ,[CB_VIVECON]      ,[CB_VIVEEN]      ,[CB_ZONA]      ,[CB_ZONA_GE]      ,[CB_NIVEL1] ' +
          '      ,[CB_NIVEL2]      ,[CB_NIVEL3]      ,[CB_NIVEL4]      ,[CB_NIVEL5]      ,[CB_NIVEL6]      ,[CB_NIVEL7]      ,[CB_INFTIPO] ' +
          '      ,[CB_NIVEL8]      ,[CB_NIVEL9]      ,[CB_DER_FEC]      ,[CB_DER_PAG]      ,[CB_V_PAGO]      ,[CB_DER_GOZ]      ,[CB_V_GOZO] ' +
          '      ,[CB_TIP_REV]      ,[CB_MOT_BAJ]      ,[CB_FEC_SAL]      ,[CB_INF_INI]      ,[CB_INF_OLD]      ,[CB_OLD_SAL]      ,[CB_OLD_INT] ' +
          '      ,[CB_PRE_INT]      ,[CB_PER_VAR]      ,[CB_SAL_TOT]      ,[CB_TOT_GRA]      ,[CB_FAC_INT]      ,[CB_RANGO_S]      ,[CB_CLINICA] ' +
          '      ,[CB_NIVEL0]      ,[CB_FEC_NIV]      ,[CB_FEC_PTO]      ,[CB_AREA]      ,[CB_FEC_TUR]      ,[CB_FEC_KAR] ' +
          '      ,[CB_PENSION]      ,[CB_CANDIDA]      ,[CB_ID_NUM]      ,[CB_ENT_NAC]      ,[CB_COD_COL]      ,[CB_PASSWRD]      ,[CB_PLAZA] ' +
          '      ,[CB_FEC_PLA]      ,[CB_DER_PV]      ,[CB_V_PRIMA]      ,[CB_SUB_CTA]      ,[CB_NETO]      ,[CB_E_MAIL]      ,[CB_PORTAL] ' +
          '      ,[CB_DNN_OK]      ,[CB_USRNAME]      ,[CB_NOMINA]      ,[CB_FEC_NOM]      ,[CB_RECONTR]      ,[CB_DISCAPA]      ,[CB_INDIGE] ' +
          '      ,[CB_FONACOT]      ,[CB_EMPLEO]      ,[US_CODIGO]      ,[CB_FEC_COV]      ,[CB_G_TEX20]      ,[CB_G_TEX21]      ,[CB_G_TEX22]      ,[CB_G_TEX23] ' +
          '      ,[CB_G_TEX24]      ,[CB_INFDISM]      ,[CB_INFACT]      ,[CB_NUM_EXT]      ,[CB_NUM_INT]      ,[CB_INF_ANT] ' +
          '      ,[CB_TDISCAP]      ,[CB_ESCUELA]      ,[CB_TESCUEL]      ,[CB_TITULO]      ,[CB_YTITULO]      ,[CB_CTA_GAS]      ,[CB_CTA_VAL] ' +
          '      ,[CB_MUNICIP]      ,[CB_ID_BIO]      ,[CB_GP_COD]      ,[CB_TIE_PEN]      ,[CB_TSANGRE]      ,[CB_ALERGIA]      ,[CB_BRG_ACT] ' +
          '      ,[CB_BRG_TIP]      ,[CB_BRG_ROL]      ,[CB_BRG_JEF]      ,[CB_BRG_CON]      ,[CB_BRG_PRA] ' +
          '      ,[CB_BRG_NOP]      ,[CB_BANCO]      ,[CB_REGIMEN] ' +
          '  FROM COLABORA WHERE CB_CODIGO = %d';
          { Fin }
          { US #5617 Adecuar transferencia individual de empleados para respaldar a la nueva tabla EMP_TRANSF}


          Q_TRANSFERENCIA_MEDICOS_BORRA: Result := 'delete from %s where ( EX_CODIGO = ( select EX_CODIGO from EXPEDIEN where CB_CODIGO = %d ) )';
          Q_AGREGA_KARDEX: Result := 'INSERT INTO KARDEX ( CB_CODIGO, CB_FECHA, CB_TIPO, CB_COMENTA, CB_NIVEL, CB_NOTA ) '+
                            'VALUES ( :CB_CODIGO, :CB_FECHA, :CB_TIPO, :CB_COMENTA, :CB_NIVEL, :CB_NOTA )';
          Q_RECALCULA_KARDEX: Result := 'EXECUTE PROCEDURE RECALCULA_KARDEX( :EMPLEADO )';{OP: 27/05/08}

          Q_DISPOSITIVOS_X_COMPANIA: Result := 'select CM_CODIGO from DISXCOM where CM_CODIGO = ''%s'' and DI_TIPO = 0'; {OP: 11/02/09}
{$ifdef MSSQL}
          Q_POLL_RANGO_FECHAS_DISP: Result := 'select MIN( PO_FECHA ) FECHA_MIN, MAX( PO_FECHA ) FECHA_MAX, COUNT(*) TOTAL from POLL where ( PO_EMPRESA = ''%s'' ) and substring(PO_LINX, 1, 4) in ( select DI_NOMBRE from disxcom where CM_CODIGO = ''%s'' and DI_TIPO = 0 ) ';{OP: 11/02/09}
          Q_POLL_PENDIENTES_DISP:  Result := 'select PO_LINX, PO_NUMERO, PO_EMPRESA, PO_FECHA, PO_HORA, PO_LETRA from POLL where ( PO_EMPRESA = ''%s'' )  and substring(PO_LINX, 1, 4) in ( select DI_NOMBRE from disxcom where CM_CODIGO = ''%s'' and DI_TIPO = 0 ) order by PO_NUMERO, PO_FECHA, PO_HORA';  { El Orden Es CRUCIAL para el Poll }{OP: 11/02/09}
          Q_POLL_BORRA_DISP:  Result := 'delete from POLL where ( PO_EMPRESA = ''%s'' ) and substring(PO_LINX, 1, 4) in ( select DI_NOMBRE from disxcom where CM_CODIGO = ''%s'' and DI_TIPO = 0 )';{OP: 11/02/09}
{$endif}
{$ifdef INTERBASE}
          Q_POLL_RANGO_FECHAS_DISP: Result := 'select MIN( PO_FECHA ) FECHA_MIN, MAX( PO_FECHA ) FECHA_MAX, COUNT(*) TOTAL from POLL where ( PO_EMPRESA = ''%s'' ) and substring(PO_LINX FROM 1 FOR 4) in ( select DI_NOMBRE from disxcom where CM_CODIGO = ''%s'' and DI_TIPO = 0 ) ';{OP: 11/02/09}
          Q_POLL_PENDIENTES_DISP:  Result := 'select PO_LINX, PO_NUMERO, PO_EMPRESA, PO_FECHA, PO_HORA, PO_LETRA from POLL where ( PO_EMPRESA = ''%s'' )  and substring(PO_LINX FROM 1 FOR 4) in ( select DI_NOMBRE from disxcom where CM_CODIGO = ''%s'' and DI_TIPO = 0 ) order by PO_NUMERO, PO_FECHA, PO_HORA';  { El Orden Es CRUCIAL para el Poll }{OP: 11/02/09}
          Q_POLL_BORRA_DISP:  Result := 'delete from POLL where ( PO_EMPRESA = ''%s'' ) and substring(PO_LINX FROM 1 FOR 4) in ( select DI_NOMBRE from disxcom where CM_CODIGO = ''%s'' and DI_TIPO = 0 )';{OP: 11/02/09}
{$endif}

{$ifdef INTERBASE}
          Q_TURNO_LEE: Result := 'select CB_TURNO from SP_FECHA_KARDEX( :Fecha, :Empleado )';
{$endif}
{$ifdef MSSQL}
          Q_TURNO_LEE: Result := 'select CB_TURNO = DBO.SP_KARDEX_CB_TURNO( :Fecha, :Empleado )';
{$endif}
          Q_EMPLEADO_FIND: Result := 'select CB_CODIGO from COLABORA where ( %s = :Valor ) order by CB_CODIGO';
          Q_POLL_GET_DIGITO: Result := 'select CM_DIGITO from COMPANY where ( CM_CODIGO = %s )';
          Q_EMP_ID_FIND: Result := 'select E.CB_CODIGO, C.CM_CODIGO, C.CM_DIGITO from EMP_ID E '+
                                   'left outer join COMPANY C on ( C.CM_CODIGO = E.CM_CODIGO ) '+
                                   'where ( E.ID_NUMERO = :TARJETA )';

{$ifndef ANTES}
{$ifdef INTERBASE}
          Q_EMPLEADO_TIPO_NOMINA: Result := 'select ' +
                                            '( select TIPNOM from SP_GET_NOMTIPO( %0:s, COLABORA.CB_CODIGO ) ) NOMINA_INI, ' +
                                            '( select TIPNOM from SP_GET_NOMTIPO( ' +
                                            {$ifdef CAMBIO_TNOM}
                                            '%1:s, ' +
                                            {$else}
                                            'COLABORA.CB_FEC_ING, ' +
                                            {$endif}
                                            'COLABORA.CB_CODIGO )  ) TU_NOMINA '+
                                            'from COLABORA where ( CB_CODIGO = :Empleado )';
{$endif}
{$ifdef MSSQL}
          Q_EMPLEADO_TIPO_NOMINA: Result := 'select ' +
                                            '( select DBO.SP_GET_NOMTIPO(  %0:s, COLABORA.CB_CODIGO ) ) NOMINA_INI, ' +
                                            '( select DBO.SP_GET_NOMTIPO( '+
                                             {$ifdef CAMBIO_TNOM}
                                            '%1:s, ' +
                                            {$else}
                                            'COLABORA.CB_FEC_ING, ' +
                                            {$endif}
                                            ' COLABORA.CB_CODIGO ) ) TU_NOMINA '+
                                            'from COLABORA where ( CB_CODIGO = :Empleado )';
{$endif}
{$endif}
        Q_CHECADAS_SIN_MODIFICAR: Result := 'select CHECADAS.CB_CODIGO PO_NUMERO, CHECADAS.AU_FECHA PO_FECHA,  ' +
                                    'CHECADAS.CH_H_REAL PO_HORA, CHECADAS.CH_RELOJ PO_LINX, COLABORA.CB_CREDENC PO_LETRA, AUSENCIA.HO_CODIGO HO_CODIGO' +
                                    ' from CHECADAS ' +
                                    'LEFT OUTER JOIN AUSENCIA ON ( AUSENCIA.CB_CODIGO = CHECADAS.CB_CODIGO AND AUSENCIA.AU_FECHA = CHECADAS.AU_FECHA ) ' +
                                    'LEFT OUTER JOIN COLABORA ON ( COLABORA.CB_CODIGO = CHECADAS.CB_CODIGO ) ' +
                                    'where ( AUSENCIA.US_CODIGO = 0 ) AND ( COLABORA.CB_CODIGO = %0:d ) and ' +
                                    '( CHECADAS.AU_FECHA = %1:s ) and ( CHECADAS.US_CODIGO = 0 ) and ( CH_SISTEMA = ''N'') order by AUSENCIA.AU_FECHA, CHECADAS.CH_H_REAL';



        Q_BORRA_CHECADAS_SIN_MODIFICAR: Result:= 'delete from CHECADAS where ( CHECADAS.US_CODIGO = 0 ) and ( CHECADAS.CH_SISTEMA = ''N'' ) and ' +
                                                 '( CHECADAS.AU_FECHA = :Fecha ) and CHECADAS.AU_FECHA in ( select AUSENCIA.AU_FECHA from AUSENCIA ' +
                                                 ' where AUSENCIA.US_CODIGO = 0 AND AUSENCIA.CB_CODIGO = CHECADAS.CB_CODIGO ) '+
                                                 ' and CB_CODIGO = :Empleado ';
        {$ifndef DOS_CAPAS}
        Q_ACTUALIZA_CM_CODIGO_EMP_BIO: Result := 'update EMP_BIO set CM_CODIGO=''%s'', CB_CODIGO=%d where ID_NUMERO=%d';
        {$endif}
     end;
end;


class procedure TdmServerCalcNomina.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
     if Register then
     begin
          inherited UpdateRegistry(Register, ClassID, ProgID);
          EnableSocketTransport(ClassID);
          EnableWebTransport(ClassID);
     end
     else
     begin
          DisableSocketTransport(ClassID);
          DisableWebTransport(ClassID);
          inherited UpdateRegistry(Register, ClassID, ProgID);
     end;
end;

{$ifdef DEBUGSENTINEL}
procedure TdmServerCalcNomina.MtsDataModuleCreate(Sender: TObject);
{$ifndef DOS_CAPAS}
var
   oLicenseMgr : TLicenseMgr;
{$endif}
begin
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
     oAutoServer := ZetaSQLBroker.GetAutoServer;
     {$ifdef DOS_CAPAS}
     ZetaSQLBroker.ReadAutoServer( oAutoServer, NIL, NIL );
     {$else}
     oLicenseMgr := TLicenseMgr.Create( oZetaProvider );
     try
        ZetaSQLBroker.ReadAutoServer( oAutoServer, oLicenseMgr.AutoGetData, oLicenseMgr.AutoSetData);
     finally
            FreeAndNil( oLicenseMgr );
     end;
     {$endif}
end;
{$else}
procedure TdmServerCalcNomina.MtsDataModuleCreate(Sender: TObject);
begin
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
     oAutoServer := ZetaSQLBroker.GetAutoServer;
     ZetaSQLBroker.ReadAutoServer( oAutoServer, nil, nil );
end;
{$endif}
procedure TdmServerCalcNomina.MtsDataModuleDestroy(Sender: TObject);
begin
     ZetaSQLBroker.FreeAutoServer( oAutoServer );
     ZetaSQLBroker.FreeZetaCreator( oZetaCreator );
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
end;

procedure TdmServerCalcNomina.ModuloAutorizado( const eModulo : TModulos );
begin
     if not oAutoServer.OkModulo( eModulo, False ) then
        DataBaseError( 'M�dulo NO Autorizado' );
end;

{$ifndef CUENTA_EMPLEADOS}
function TdmServerCalcNomina.MaxEmpleados: Integer;
begin
     {$ifdef CAROLINA}
     Result := 150000;
     {$else}
     Result := oAutoServer.Empleados;
     {$endif}
end;
{$endif}

{$ifdef DOS_CAPAS}
procedure TdmServerCalcNomina.CierraEmpresa;
begin
end;
{$endif}

procedure TdmServerCalcNomina.InitCreator;
begin
     if not Assigned( oZetaCreator ) then
        oZetaCreator := ZetaSQLBroker.GetZetaCreator( oZetaProvider );
end;

procedure TdmServerCalcNomina.InitCalcNomina;
begin
     InitCreator;
     if FCalcNomina = NIL then
     begin
          FCalcNomina := TCalcNomina.Create( oZetaCreator );
          with FCalcNomina do
          begin
               {$ifdef CUENTA_EMPLEADOS}
               AutoServer := oAutoServer;
               {$else}
               LimiteEmpleados := MaxEmpleados;
               {$endif}
          end;
     end;
end;

procedure TdmServerCalcNomina.InitLog( Empresa: Olevariant; const sPaletita: string );
begin
     {$ifdef BITACORA_DLLS}
     FPaletita := sPaletita;
     FArchivoLog := 'c:\BitacoraTress '+ FormatDateTime('dd_mmm_yy', Date()) +'.txt';

     if FListaLog = NIL then
     begin
          try
             FListaLog :=  TAsciiLog.Create;
             FListaLog.Init(FArchivoLog);
             try
                if NOT varisNull( Empresa ) then
                   try
                      FEmpresa :=  ' -- Alias: ' + Empresa[P_CODIGO] +
                                   ' Usuario: ' + InTToStr(Empresa[P_USUARIO])
                   except
                         FEmpresa := ' Error al leer Empresa[]';
                   end
                else
                    FEmpresa := '';
             except
                   FEmpresa := ' Error al leer Empresa[]';
             end;

             FListaLog.WriteTexto( FormatDateTime('dd/mmm/yy hh:nn:ss',Now) +
                                   ' DServerCalcNomina :: INICIA :: '+  FPaletita +
                                   FEmpresa );
             FListaLog.Close;
          except
          end;
     end;
     {$endif}
end;

procedure TdmServerCalcNomina.EndLog;
begin
      {$ifdef BITACORA_DLLS}
      try
        if FArchivoLog= '' then
           FArchivoLog := 'c:\BitacoraTress '+ FormatDateTime('dd_mmm_yy', Date()) +'.txt';
        if FListaLog = NIL then
        begin
             FListaLog :=  TAsciiLog.Create;
             FListaLog.Init(FArchivoLog);
             FListaLog.WriteTexto( FormatDateTime('dd/mmm/yy hh:nn:ss',Now) + ' DServerCalcNomina :: TERMINA :: '+ FPaletita +' '+ FEmpresa);
        end
        else
        begin
             //FListaLog.Open(FArchivoLog);
             FListaLog.Init(FArchivoLog);
             FListaLog.WriteTexto( FormatDateTime('dd/mmm/yy hh:nn:ss',NOw) + ' DServerCalcNomina :: TERMINA :: '+ FPaletita +' '+ FEmpresa  );
             //FListaLog.Close;
        end;
        FListaLog.Close;
        FreeAndNil(FListaLog);
     except

     end;
     {$endif}
end;


procedure TdmServerCalcNomina.ClearCalcNomina;
begin
     FreeAndNil( FCalcNomina );
end;

procedure TdmServerCalcNomina.InitBroker;
begin
     InitCreator;
     oZetaCreator.RegistraFunciones([efComunes]);
     oZetaSQLBroker := ZetaSQLBroker.GetSQLBroker( oZetaCreator );
end;

procedure TdmServerCalcNomina.InitNomina;
begin
     InitCreator;
     FNomina := TNomina.Create( oZetaCreator );
end;

procedure TdmServerCalcNomina.ClearNomina;
begin
     FreeAndNil(FNomina);
end;

procedure TdmServerCalcNomina.InitQueries;
begin
     InitCreator;
     oZetaCreator.PreparaQueries;
end;

procedure TdmServerCalcNomina.InitRitmos;
begin
     InitQueries; { Llama a InitCreator }
     oZetaCreator.PreparaRitmos;
end;

procedure TdmServerCalcNomina.InitTarjeta;
begin
     InitQueries; { Llama a InitCreator }
     FTarjeta := TTarjeta.Create( oZetaCreator );
end;


procedure TdmServerCalcNomina.ClearTarjeta;
begin
     FreeAndNil( FTarjeta );
end;


function TdmServerCalcNomina.BuildEmpresa(DataSet: TZetaCursor): OleVariant;
     { Es una copia de DBaseCliente.BuildEmpresa }
     {
     Las posiciones de este arreglo se encuentran en ZetaCommonClasses:

     P_ALIAS = 0;
     P_DATABASE = 0;
     P_USER_NAME = 1;
     P_PASSWORD = 2;
     P_USUARIO = 3;
     P_NIVEL_0 = 4;
     P_CODIGO = 5;
     }
begin
     with DataSet do
     begin
          Result := VarArrayOf( [ FieldByName( 'CM_DATOS' ).AsString,
                                  FieldByName( 'CM_USRNAME' ).AsString,
                                  FieldByName( 'CM_PASSWRD' ).AsString,
                                  0,
                                  FieldByName( 'CM_NIVEL0' ).AsString,
                                  FieldByName( 'CM_CODIGO' ).AsString ] );
     end;
end;
{ *********** Interfases ( Paletitas ) ************ }

function TdmServerCalcNomina.CalculaNominaSimulacion(Empresa,Parametros: OleVariant): OleVariant;
 var
    i: eTipoPeriodo;
    oProcess: OleVariant;
begin
     SetOLEVariantToNull( Result );
     Result := GetEmptyProcessResultFecha( prNoCalcular );
     {*** US 15500: Los c�lculos de n�mina desde Simulaci�n de finiquitos tarda mucho tiempo ***}
     for i:= 0 to FArregloPeriodo.Count - 1 do
     begin
          with oZetaProvider,ParamList do
          begin
               InicializaValoresActivos;
               ParamByName('TIPO').AsInteger := Ord(i);
               //ParamList.ParamByName('NUMERO').AsInteger := GetGlobalInteger( K_GLOBAL_SIMULACION_FINIQUITOS );
               AddString('Filtro', Format( '@COLABORA.CB_CODIGO IN (SELECT N.CB_CODIGO FROM ' +
                                                     ' NOMINA N WHERE N.PE_YEAR = %d AND N.PE_TIPO = %d AND N.PE_NUMERO = %d and N.NO_GLOBAL =''S'')',
                                                     [ ParamByName('Year').AsInteger, Ord(i), ParamByName('Numero').AsInteger] ));
               try
                  oProcess := CalcNomina.CalculaNomina(Empresa,ParamList.VarValues);
                  if NOT VarIsNull( oProcess ) then
                     SumaProcessResult( Result, Result, oProcess );
               except
               end;
          end;
     end;
end;

function TdmServerCalcNomina.CalculaNomina(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'CalculaNomina');
     ModuloAutorizado( okNomina );
     InitCalcNomina;
     try
        with oZetaProvider do
        begin
             AsignaParamList( Parametros );
             if ( ParamList.FindParam('SimulacionCiclo') <> NIL ) and
                ( ParamList.ParamByName('SimulacionCiclo').AsString = K_GLOBAL_SI ) then
             begin
                  {*** US 15500: Los c�lculos de n�mina desde Simulaci�n de finiquitos tarda mucho tiempo ***}
                  EmpresaActiva := Empresa;
                  InitArregloTPeriodo;
                  {*** FIN ***}
                  Result := CalculaNominaSimulacion(Empresa,Parametros);
             end
             else
                 Result := CalcNomina.CalculaNomina(Empresa,Parametros);
        end;
     finally
            ClearCalcNomina;
     end;
     EndLog;SetComplete;
end;

function TdmServerCalcNomina.RastreaConcepto(Empresa, Parametros: OleVariant; out Rastreo: OleVariant): OleVariant;
begin
     InitLog(Empresa,'RastreaConcepto');
     InitCalcNomina;
     try
        Result := CalcNomina.RastreaConcepto(Empresa,Parametros,Rastreo);
     finally
            ClearCalcNomina;
     end;
     EndLog;SetComplete;
end;

function TdmServerCalcNomina.CalculaNetoBruto(Empresa: OleVariant; var Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'CalculaNetoBruto');
     InitCalcNomina;
     try
         Result := CalcNomina.CalculaNetoBruto(Empresa, Parametros );
     finally
            ClearCalcNomina;
     end;
     EndLog;SetComplete;
end;
{$ifdef NOTREPORTING}
function TdmServerCalcNomina.GeneraSQL(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant; out Error: WideString): OleVariant;
var
   Reporte: TdmServerReportes;
begin
     InitLog(Empresa,'GeneraSQL');
     InitCreator;
     Reporte := TdmServerReportes.Create( oZetaProvider, oZetaCreator );
     try
        Result := Reporte.GeneraSQL( Empresa,
                                     AgenteSQL,
                                     Parametros,
                                     Error );
     finally
            Reporte.Free;
     end;
     EndLog;SetComplete;
end;
{$else}
function TdmServerCalcNomina.GeneraSQL(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant; out Error: WideString): OleVariant;
begin
     raise Exception.Create( 'La llamada a ServerCalcNomina.Reporteador (GeneraSQL) ha sido descontinuada. ' + CR_LF +
                             'Contactar al Departamento de Desarrollo (CV/ER)' );
end;
{$ENDIF}

function TdmServerCalcNomina.EvaluaParam(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant;  out Error: WideString): WordBool;
var
   Reporte: TdmServerReportes;
begin
     InitLog(Empresa,'EvaluaParam');
     InitCreator;
     Reporte := TdmServerReportes.Create( oZetaProvider, oZetaCreator );
     try
        Result := Reporte.EvaluaParam( Empresa,
                                       AgenteSQL,
                                       Parametros,
                                       Error );
     finally
            Reporte.Free;
     end;
     EndLog;SetComplete;
end;


{ ******* Calcular Prenomina ********* }

procedure TdmServerCalcNomina.CalcularPrenominaBuildDataset( const eCalculo: eTipoCalculo );
const
{$ifndef ANTES}
    {$ifdef INTERBASE}
    K_TURNO_TIPO_NOMINA = '( select TIPNOM from SP_GET_NOMTIPO( %s, COLABORA.CB_CODIGO )  )';
    {$endif}
    {$ifdef MSSQL}
    K_TURNO_TIPO_NOMINA = '( select DBO. SP_GET_NOMTIPO( %s, COLABORA.CB_CODIGO ) )';
    {$endif}
{$endif}
begin
     with oZetaProvider do
     begin
          with oZetaSQLBroker do
          begin
               if ( eCalculo = tcNueva ) then
               begin
                    Init( enEmpleado );
                    with Agente do
                    begin
                         AgregaColumna( 'CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
                         AgregaColumna( 'CB_FEC_ING', True, Entidad, tgFecha, 0, 'CB_FEC_ING' );
                         AgregaColumna( 'CB_FEC_ANT', True, Entidad, tgFecha, 0, 'CB_FEC_ANT' );
                         AgregaColumna( 'CB_FEC_BAJ', True, Entidad, tgFecha, 0, 'CB_FEC_BAJ' );
                         AgregaColumna( 'CB_ACTIVO', True, Entidad, tgBooleano, 0, 'CB_ACTIVO' );
                         AgregaColumna( 'CB_CHECA', True, Entidad, tgBooleano, 0, 'CB_CHECA' );
                         AgregaColumna( 'CB_NOMYEAR', True, Entidad, tgNumero, 0, 'CB_NOMYEAR' );
                         AgregaColumna( 'CB_NOMTIPO', True, Entidad, tgNumero, 0, 'CB_NOMTIPO' );
                         AgregaColumna( 'CB_NOMNUME', True, Entidad, tgNumero, 0, 'CB_NOMNUME' );
                         {$ifdef ANTES}
                         AgregaColumna( 'TU_NOMINA', True, enTurno, tgNumero, 0, 'TU_NOMINA' );
                         {$else}
                         AgregaColumna( Format( K_TURNO_TIPO_NOMINA, [ DateToStrSQLC( DatosPeriodo.Inicio ) ] ), True, enTurno, tgNumero, 0, 'TU_NOMINA' );
                         {$endif}
                         {$ifdef CAMBIO_TNOM}
                         AgregaColumna( 'CB_FEC_NOM', True, Entidad, tgFecha, 0, 'CB_FEC_NOM' );
                         {$endif}
                         AgregaOrden( 'CB_CODIGO', True, Entidad );
                    end;
                    { Se deben crear todas las prenominas independientemente del Nivel0 ( Problemas de Status de Periodo ) }
               end
               else
               begin
                    Init( enNomina );
                    with Agente do
                    begin
                         AgregaColumna( 'CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
{$ifdef QUINCENALES}
                         AgregaColumna( 'COLABORA.CB_FEC_ING', True, enEmpleado, tgFecha, 0, 'CB_FEC_ING' );
                         AgregaColumna( 'COLABORA.CB_FEC_BAJ', True, enEmpleado, tgFecha, 0, 'CB_FEC_BAJ' );
{$endif}
                         AgregaColumna( 'COLABORA.CB_CHECA', True, enEmpleado, tgBooleano, 0, 'CB_CHECA' );
                         AgregaOrden( 'CB_CODIGO', True, Entidad );
                         with DatosPeriodo do
                         begin
                              AgregaFiltro( Format( 'PE_YEAR = %d', [ Year] ), True, Entidad );
                              AgregaFiltro( Format( 'PE_TIPO = %d', [ Ord( Tipo ) ] ), True, Entidad );
                              AgregaFiltro( Format( 'PE_NUMERO = %d', [ Numero ] ), True, Entidad );
                         end;
                    end;
                    { Para Calcular TODAS, si se aplica NIVEL0 }
                    FiltroConfidencial( EmpresaActiva );
               end;
               AgregaRangoCondicion( ParamList );
               BuildDataset( EmpresaActiva );
          end;
     end;
end;

function TdmServerCalcNomina.CalcularPrenominaDataset(Dataset: TDataset; const eCalculo: eTipoCalculo): OleVariant;
var
   iEmpleado: TNumEmp;
   lOk, lCheca: Boolean;
   PeriodoBaja: TDatosPeriodo;
{$ifndef ANTES}
   dIngreso,dBaja,dCambioTNom: TDate;
   TipoNomina
{$ifndef CAMBIO_TNOM}
   ,NominaIni
{$endif}: eTipoPeriodo;
   FEmpleadoTipoNomina: TZetaCursor;
{$endif}

   procedure ValidaCambioTNomPost;
   begin
        with oZetaProvider do
        begin
             if ( dCambioTNom > DatosPeriodo.Fin ) then
             begin
                  with oZetaCreator.Queries do
                  begin
                       GetFechaCambioTNomBegin;
                       try
                          dCambioTNom:= GetFechaCambioTNom( iEmpleado, DatosPeriodo.Fin );
                       finally
                             GetFechaCambioTNomEnd;
                       end;
                  end;
             end;
        end;
   end;
begin
     with oZetaProvider do
     begin
          //InitGlobales;    Se inicializa en M�todo CalculaPreNomina
          if OpenProcess( prASISCalculoPreNomina, Dataset.RecordCount, FListaParametros ) then
          begin
               InitQueries;
               InitRitmos;
               try
                  InitNomina;
                  try
                     InitTarjeta;
                     {$ifndef ANTES}
                     FEmpleadoTipoNomina := CreateQuery( Format( GetSQLScript( Q_EMPLEADO_TIPO_NOMINA ), [ DateToStrSQLC( DatosPeriodo.Inicio )
                     {$ifdef CAMBIO_TNOM}
                                                                                                           ,DateToStrSQLC( DatosPeriodo.Fin )
                                                                                                            ] ) );
                     {$endif}
                     {$endif}
                     try
                        { Antes de empezar }
                        with FNomina do
                        begin
                             CalculaPrenominaBegin;
                        end;
                        with FTarjeta do
                        begin
{$ifdef QUINCENALES}
                             CalculaPrenominaBegin;
{$else}
                             with DatosPeriodo do
                             begin
                                  CalculaPrenominaBegin( Inicio, Fin );
                             end;
{$endif}
                        end;
                        { Ejecuci�n }
                        with Dataset do
                        begin

                             while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                             begin
                                  iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                                  lCheca := zStrToBool( FieldByName( 'CB_CHECA' ).AsString );
                                  dIngreso := FieldByName( 'CB_FEC_ING' ).AsDateTime;
                                  EmpiezaTransaccion;
                                  try
                                     with FNomina do
                                     begin
                                          case eCalculo of
                                               tcNueva:
                                               begin
                                                    with PeriodoBaja do
                                                    begin
                                                         Year := FieldByName( 'CB_NOMYEAR' ).AsInteger;
                                                         Tipo := eTipoPeriodo( FieldByName( 'CB_NOMTIPO' ).AsInteger );
                                                         Numero := FieldByName( 'CB_NOMNUME' ).AsInteger;
                                                    end;
                                                    {$ifndef ANTES}

                                                    dBaja:= FieldByName('CB_FEC_BAJ').AsDateTime;
                                                    TipoNomina := eTipoPeriodo( FieldByName( 'TU_NOMINA' ).AsInteger );
                                                    dCambioTNom:= FieldByName('CB_FEC_NOM').AsDateTime;

                                                    { Si ha tenido una baja y la fecha de ingreso a la fecha de asistencia del periodo investiga fecha
                                                      en base a Kardex }
                                                    if  ( dBaja > DatosPeriodo.Inicio ) and ( dIngreso > DatosPeriodo.FinAsis ) then
                                                    begin
                                                         with Queries do
                                                         begin
                                                              GetFechaAltaBegin;
                                                              try
                                                                 dIngreso:= GetFechaAlta( DatosPeriodo.FinAsis, iEmpleado );
                                                              finally
                                                                     GetFechaAltaEnd;
                                                              end;
                                                         end;
                                                    end;

                                                    with oZetaProvider do
                                                    begin

                                                         if ( dIngreso > DatosPeriodo.Inicio ) and ( dIngreso <= DatosPeriodo.Fin )
                                                            {$ifdef CAMBIO_TNOM}
                                                            or ( EsCambioTNomPeriodo( dCambioTNom, DatosPeriodo ) or ( dCambioTNom > DatosPeriodo.Fin ) )
                                                            {$endif}
                                                            then
                                                         begin
                                                              ValidaCambioTNomPost;

                                                              ParamAsInteger( FEmpleadoTipoNomina, 'Empleado', iEmpleado );
                                                              with FEmpleadoTipoNomina do
                                                              begin
                                                                   Active := True;
                                                                   { En un reingreso a media n�mina o un cambio de Tipo de n�mina,
                                                                   al menos que en esa n�mina este la baja se incluye y que la n�mina al principio
                                                                   del periodo sea igual a la del c�lculo }
                                                                   {$ifdef CAMBIO_TNOM}
                                                                   if ( ( ( dBaja < DatosPeriodo.Inicio ) and ( dCambioTNom = dIngreso ) )
                                                                      or ( DatosPeriodo.Tipo <> TipoNomina ) ) then
                                                                   begin
                                                                        TipoNomina := eTipoPeriodo( FEmpleadoTipoNomina.FieldByName( 'TU_NOMINA' ).AsInteger );
                                                                   end;
                                                                   {$else}
                                                                   NominaIni:= eTipoPeriodo( FEmpleadoTipoNomina.FieldByName( 'NOMINA_INI' ).AsInteger );
                                                                   TipoNomina := eTipoPeriodo( FEmpleadoTipoNomina.FieldByName( 'TU_NOMINA' ).AsInteger );
                                                                   if ( TipoNomina <> NominaIni) then
                                                                   begin
                                                                        Log.Advertencia( iEmpleado, Format( 'Tipo de N�mina diferentes antes ( %d ) y despu�s ( %d ) del ingreso', [ Ord(NominaIni), Ord(TipoNOmina) ] ) );
                                                                   end;
                                                                   {$endif}
                                                                   {if ( sTurno <> sTurnoNuevo ) then
                                                                   begin
                                                                        Log.Advertencia( iEmpleado, Format( 'Turnos diferentes antes ( %s ) y despu�s ( %s ) del ingreso', [ sTurno, sTurnoNuevo ] ) );
                                                                   end;}
                                                                   Active := False;
                                                              end;
                                                         end;
                                                    end;
                                                    {$endif}
                                                    lOk := IncluyeNomina( iEmpleado,
                                                                          {$ifdef ANTES}
                                                                          FieldByName( 'CB_FEC_ING' ).AsDateTime,
                                                                          {$else}
                                                                          dIngreso,
                                                                          {$endif}
                                                                          FieldByName( 'CB_FEC_ANT' ).AsDateTime,
                                                                          FieldByName( 'CB_FEC_BAJ' ).AsDateTime,
                                                                          {$ifdef ANTES}
                                                                          eTipoPeriodo( FieldByName( 'TU_NOMINA' ).AsInteger ),
                                                                          {$else}
                                                                          TipoNomina,
                                                                          {$endif}
                                                                          ZetaCommonTools.zStrToBool( FieldByName( 'CB_ACTIVO' ).AsString ),
                                                                          PeriodoBaja
                                                                          {$ifdef CAMBIO_TNOM}
                                                                          ,dCambioTNom
                                                                          {$else}
                                                                          ,NullDateTime
                                                                          {$endif}
                                                                           );
                                               end;
                                          else
                                              lOk := InspeccionaNomina( iEmpleado, True );
                                          end;
                                          if lOk then
                                          begin
{$ifdef QUINCENALES}
{$ifdef CAMBIO_TNOM}
                                               FTarjeta.CalculaPreNomina( iEmpleado, lCheca, DiasHoras.Turno, dIngreso,
                                                                                                              FieldByName( 'CB_FEC_BAJ' ).AsDateTime,
                                                                                                              dCambioTNom );
{$else}
                                               FTarjeta.CalculaPreNomina( iEmpleado, lCheca, DiasHoras.Turno, FieldByName( 'CB_FEC_ING' ).AsDateTime, FieldByName( 'CB_FEC_BAJ' ).AsDateTime );
{$endif}
                                               CalculaPreNomina( iEmpleado, lCheca, FieldByName( 'CB_FEC_ING' ).AsDateTime, FieldByName( 'CB_FEC_BAJ' ).AsDateTime );
{$else}
                                               FTarjeta.CalculaPreNomina( iEmpleado, lCheca, DiasHoras.Turno );
                                               CalculaPreNomina( iEmpleado, lCheca );
{$endif}
                                          end;
                                     end;
                                     TerminaTransaccion( True );
                                  except
                                        on Error: Exception do
                                        begin
                                             //TerminaTransaccion( False );
                                             RollBackTransaccion;
                                             Log.Excepcion( iEmpleado, 'Error Al Calcular Pren�mina', Error );
                                        end;
                                  end;
                                  Next;
                             end;
                        end;
                        { Al terminar }
                        with FTarjeta do
                        begin
                             CalculaPrenominaEnd;
                        end;
                        with FNomina do
                        begin
                             CalculaPrenominaEnd;
                        end;
                     except
                           on Error: Exception do
                           begin
                                Log.Excepcion( 0, 'Error En C�lculo de Pren�mina', Error );
                           end;
                     end;
                  finally
{$ifndef ANTES}
                         FreeAndNil( FEmpleadoTipoNomina );
{$endif}
                         ClearTarjeta;
                  end;
               finally
                      ClearNomina;
               end;
          end;
          Result := CloseProcess;
     end;
end;

procedure TdmServerCalcNomina.CalculaPreNominaParametros;
begin
     with oZetaProvider, ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Peri�do: ' + ZetacommonTools.ShowNomina( DatosPeriodo.Year, Ord( DatosPeriodo.Tipo ), DatosPeriodo.Numero ) +
                                        K_PIPE + 'Tipo de C�lculo: ' + ZetaCommonLists.ObtieneElemento( lfTipoCalculo, ParamByname( 'TipoCalculo' ).AsInteger );
     end;
end;

function TdmServerCalcNomina.CalculaPreNomina(Empresa, Parametros: OleVariant): OleVariant;
var
   eCalculo: eTipoCalculo;
begin
     InitLog(Empresa,'CalculaPreNomina');
     ModuloAutorizado( okAsistencia );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitBroker;
          AsignaParamList( Parametros );
          InitArregloTPeriodo;//acl
          eCalculo := eTipoCalculo( ParamList.ParamByName( 'TipoCalculo' ).AsInteger );
          GetDatosPeriodo;
          CalculaPreNominaParametros;
          InitGlobales;
          { Validacion de N�mina Afectada }
          if ( DatosPeriodo.Status in [ spAfectadaParcial,spAfectadaTotal ] ) then
             DatabaseError( 'La N�mina ya fu� Afectada.' + CR_LF + 'No se puede volver a Calcular' );

          { Validacion de Global de Ajuste Asistencia }
{$ifdef ANTES}
          if NOT GetGlobalBooleano( K_GLOBAL_BLOQUEO_X_STATUS ) AND
{$else}
          if GetGlobalBooleano( K_GLOBAL_BLOQUEO_X_STATUS ) AND
{$endif}
             NOT ParamList.ParamByName('PuedeCambiarTarjeta').AsBoolean then
          begin
               if ( DatosPeriodo.Status > eStatusPeriodo( GetGlobalInteger( K_LIMITE_MODIFICAR_ASISTENCIA ) ) ) then
                  DatabaseError( 'La N�mina excede el Status L�mite para ser Modificada' );
          end;
     end;
     CalcularPrenominaBuildDataset( eCalculo );
     Result := CalcularPrenominaDataset( oZetaSQLBroker.SuperReporte.DataSetReporte, eCalculo );
     EndLog;SetComplete;
end;

{ ********** Poll ********* }

{$ifdef FALSE}
La checada de Asistencia es algo as�:
 
CODIGO DE BARRA:
================
 
123456XXYY@E123456789CMMDDHHNNI
 
donde
 
Concepto   Ancho  Descripci�n
========   =====  =======================
123456     6      Informaci�n del reloj donde se hizo la checada
XX         2      # de terminal Maestra a la que pertenece el reloj donde se hizo la checada (01, 02, etc. )
YY         2      # de terminal del reloj donde se hizo la checada (01, 02, etc. )
@          1      Constante que indica que la checada es de Asistencia
E          1      Caracter que identifica a la empresa (ver globales de  empresa)
123456789  9      N�mero de empleado rellenado con ceros a la izquierda
C          1      Letra de credencial del empleado
MM         2      Mes de la checada (00 al 12)
DD         2      D�a de la checada (01 al 31, dependiendo del mes )
HH         2      Hora de la checada ( 00 a 23)
NN         2      Minutos de la checada ( 00 a 59 )
I          1      0 si es Entrada, 1 si es Salida

Ejemplo:
 
1234560102@000672A112218451
 
PROXIMIDAD:
================
 
123456XXYY@E123456789CMMDDHHNNI
 
donde
 
Concepto    Ancho   Descripci�n
========    =====   =======================
123456      6       Informaci�n del reloj donde se hizo la checada
XX          2       # de terminal Maestra a la que pertenece el reloj donde se hizo la checada (01, 02, etc. )
YY          2       # de terminal del reloj donde se hizo la checada (01, 02, etc. )
@           1       Constante que indica que la checada es de Asistencia
~           1       Constante que indica que la checada proviene de un gafete de Proximidad
0000123456 10       6 d�gitos hexadecimales del n�mero leido del gafete de Proximidad justificado con 4 ceros a  la izquierda
MM          2       Mes de la checada (00 al 12)
DD          2       D�a de la checada (01 al 31, dependiendo del mes )
HH          2       Hora de la checada ( 00 a 23)
NN          2       Minutos de la checada ( 00 a 59 )
I           1       0 si es Entrada, 1 si es Salida
 
Ejemplo:
 
1234560102@~000019672A112218451
 
{$endif}

procedure TdmServerCalcNomina.PollError( const sDatos, sError: String );
begin
     with cdsLog do
     begin
          try
             Append;
             FieldByName( 'POLL_DATA' ).AsString := Copy( sDatos, 1, K_ANCHO_POLL_DATA );
             FieldByName( 'POLL_ERR' ).AsString := Copy( sError, 1, K_ANCHO_POLL_ERROR );
             Post;
          except
                on Error: Exception do
                begin
                     Cancel;
                end;
          end;
     end;
end;

procedure TdmServerCalcNomina.PollException( const sDatos: String; Error: Exception );
begin
     PollError( sDatos, Error.Message );
end;




function TdmServerCalcNomina.PollWrite( Datos: OleVariant; const sEmpresa, sCampo: String ): Integer;
const
     K_POST_ERROR = 'Cia %s Emp %d Cred %s Fecha %s Hora %s';
var
   sDatos: String;
   FDataset: TZetaCursor;
   Empresa: OleVariant;
begin
     with cdsLog do
     begin
          InitTempDataset;
          AddStringField( 'POLL_DATA', K_ANCHO_POLL_DATA ); { Datos }
          AddMemoField( 'POLL_ERR', K_ANCHO_POLL_ERROR );  { Error }
          CreateTempDataset;
     end;
     Result := 0;
     if FTransformar then
     begin
          with oZetaProvider do
          begin
               EmpresaActiva := Comparte;
               FDataset := CreateQuery( Format( GetSQLScript( Q_COMPANYS_FIND ), [ sEmpresa ] ) );
               try
                  with FDataset do
                  begin
                       Active := True;
                       if IsEmpty then
                       begin
                            Result := -1;
                            PollError( 'Error Al Buscar Empresa Transformadora', sEmpresa );
                       end
                       else
                           Empresa := BuildEmpresa( FDataSet );
                       Active := False;
                  end;
               finally
                      FreeAndNil( FDataset );
               end;
          end;
     end;
     if ( Result >= 0 ) then
     begin
          oSuperASCII := TdmSuperASCII.Create( Self );
          try
             if FTransformar then
             begin
                  with oZetaProvider do
                  begin
                       EmpresaActiva := Empresa;
                       FEmployee := CreateQuery( Format( GetSQLScript( Q_EMPLEADO_FIND ), [ sCampo ] ) );
                  end;
             end
             else
             begin
                  with oZetaProvider do
                  begin
                       EmpresaActiva := Comparte;
                       FEmployee := CreateQuery( GetSQLScript( Q_EMP_ID_FIND ) );
                  end;
             end;
             try
                try
                   with oSuperASCII do
                   begin
                        OnExaminar := PollExaminaASCII;
                        OnValidar := PollValidaASCII;
                        Formato := faASCIIFijo;
                        AgregaColumna( 'PO_LINX', tgTexto, 11, True );
                        AgregaColumna( 'PO_EMPRESA', tgTexto, 1, True );
                        AgregaColumna( 'PO_NUMERO', tgNumero, 9, True );
                        AgregaColumna( 'PO_LETRA', tgTexto, 1, True );
                        AgregaColumna( 'PO_MES', tgNumero, 2, True );
                        AgregaColumna( 'PO_DIA', tgNumero, 2, True );
                        AgregaColumna( 'PO_HORA', tgTexto, 4, True );
                        AgregaColumna( 'PO_IN_OUT', tgTexto, 1, True );
                        AgregaColumna( 'PO_FECHA', tgFecha, 8, False );
                        with cdsLista do
                        begin
                             Lista := Procesa( Datos );
                             Result := RecordCount - Errores;
                        end;
                   end;
                except
                      on Error: Exception do
                      begin
                           PollException( 'Error Al Leer Archivo POLL', Error );
                           Result := 0;
                      end;
                end;
             finally
                    if FTransformar then
                    begin
                         FreeAndNil( FEmployee );
                    end
                    else
                    begin
                         FreeAndNil( FEmployee );
                    end;
             end;
          finally
                 oSuperASCII.Free;
          end;
          with oZetaProvider do
          begin
               EmpresaActiva := Comparte;
               EmpiezaTransaccion;
               try
                  try
                     FDataset := CreateQuery( GetSQLScript( Q_POLL_ESCRIBE ) );
                     with cdsLista do
                     begin
                          while not Eof do

                          begin
                               sDatos := FieldByName( 'RENGLON' ).AsString;
                               if ( FieldByName( 'NUM_ERROR' ).AsInteger = 0 ) then
                               begin
                                    try
                                       ParamAsVarChar( FDataset, 'PO_LINX', FieldByName( 'PO_LINX' ).AsString, K_ANCHO_LINX );
                                       ParamAsChar( FDataset, 'PO_EMPRESA', FieldByName( 'PO_EMPRESA' ).AsString, K_ANCHO_EMPRESA );
                                       ParamAsInteger( FDataset, 'PO_NUMERO', FieldByName( 'PO_NUMERO' ).AsInteger );
                                       ParamAsChar( FDataset, 'PO_LETRA', FieldByName( 'PO_LETRA' ).AsString, K_ANCHO_LETRA );
                                       ParamAsDate( FDataset, 'PO_FECHA', FieldByName( 'PO_FECHA' ).AsDateTime );
                                       ParamAsChar( FDataset, 'PO_HORA', FieldByName( 'PO_HORA' ).AsString, K_ANCHO_HORA );
                                       Ejecuta( FDataset );
                                    except
                                          on Error: Exception do
                                          begin
                                               sDatos := Format( K_POST_ERROR, [ FieldByName( 'PO_EMPRESA' ).AsString,
                                                                                 FieldByName( 'PO_NUMERO' ).AsInteger,
                                                                                 FieldByName( 'PO_LETRA' ).AsString,
                                                                                 FormatDateTime( 'dd/mmm/yyyy', FieldByName( 'PO_FECHA' ).AsDateTime ),
                                                                                 FieldByName( 'PO_HORA' ).AsString ] );
                                               if DZetaServerProvider.PK_Violation( Error ) then
                                                  PollError( sDatos, 'Checada Ya Existe' )
                                               else
                                                   PollException( sDatos, Error );
                                          end;
                                    end;
                               end
                               else
                               begin
                                    PollError( sDatos, FieldByName( 'DESC_ERROR' ).AsString );
                               end;
                               Next;
                          end;
                     end;
                  except
                        on Error: Exception do
                        begin
                             PollException( 'Error Al Escribir POLL', Error );
                        end;
                  end;
               finally
                      TerminaTransaccion( True );
                      FreeAndNil(FDataset);
               end;
          end;
     end;
end;


procedure TdmServerCalcNomina.PollParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          {$IFDEF INTERFAZ_ELECTROLUX_KRONOS}
          if Count = 7 then
              FListaParametros := FListaParametros + K_PIPE + 'Proceso ejecutado desde Interfaz Kronos.';
          {$ENDIF}
          if strLleno( ParamByName( 'Programa' ).AsString ) then
             FListaParametros :=  ConcatString( FListaParametros, 'Programa: ' + ParamByName( 'Programa' ).AsString, K_PIPE );
          if strLleno( ParamByName( 'Comandos' ).AsString ) then
             FListaParametros :=  ConcatString( FListaParametros, 'Par�metros: ' + ParamByName( 'Comandos' ).AsString, K_PIPE );
          FListaParametros := FListaParametros + K_PIPE + 'Archivo POLL: ' + ParamByName( 'Archivo' ).AsString;
          if ( ParamByName( 'ProcesarTarjetas' ).AsBoolean ) then
              FListaParametros := FListaParametros + K_PIPE + 'Procesar Tarjetas: ' + K_GLOBAL_SI
          else
              FListaParametros := FListaParametros + K_PIPE + 'Procesar Tarjetas: ' + K_GLOBAL_NO;

          {$IFDEF INTERFAZ_ELECTROLUX_KRONOS}
          if Count = 7 then
          begin
            FListaParametros := FListaParametros + K_PIPE + 'Fecha inicio del periodo: ' + FechaCorta(ParamByName ('InicioPeriodo').AsDate);
            FListaParametros := FListaParametros + K_PIPE + 'Fecha fin del periodo: ' + FechaCorta(ParamByName ('FinPeriodo').AsDate);
          end;
          {$ENDIF}
     end;
end;

function TdmServerCalcNomina.Poll(Empresa, Parametros, ListaASCII: OleVariant): OleVariant;
var
   iRecords: Integer;
   lProcesar: Boolean;
begin
     InitLog(Empresa,'Poll');
     ModuloAutorizado( okAsistencia );
     FTransformar := False;
     {$IFDEF INTERFAZ_ELECTROLUX_KRONOS}
     oZetaProvider.AsignaParamList( Parametros );
     iCantidadParametros := oZetaProvider.ParamList.Count;
     if iCantidadParametros = 7 then
     begin
       iRecords := PollWriteKronos( ListaASCII, '', '',
          oZetaProvider.ParamList.ParamByName('InicioPeriodo').AsDate, oZetaProvider.ParamList.ParamByName('FinPeriodo').AsDate );
     end
     else
        iRecords := PollWrite( ListaASCII, '', '');
     {$ELSE}
     iRecords := PollWrite( ListaASCII, '', '');
     {$ENDIF}
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          {$IFNDEF INTERFAZ_ELECTROLUX_KRONOS}
          AsignaParamList( Parametros );
          {$ENDIF}
          lProcesar := ParamList.ParamByName( 'ProcesarTarjetas' ).AsBoolean;
          PollParametros;
          if OpenProcess( prASISPollRelojes, iRecords, FListaParametros ) then
          begin
               with cdsLog do
               begin
                    First;
                    while not Eof do
                    begin
                         Log.Error( 0, 'Error Al Escribir Checada', 'Datos: ' + CR_LF + FieldByName( 'POLL_DATA' ).AsString + CR_LF + 'Error: ' + CR_LF + FieldByName( 'POLL_ERR' ).AsString );
                         Next;
                    end;
               end;
          end;
          Result := CloseProcess;
     end;
     if lProcesar then
        ProcesaTarjetas( Empresa, Parametros );
     EndLog;
     SetComplete;
end;

{$ifdef FALSE}
CHECADAS de TurboTec:

030300602C@0$13113071107131
030300602C@0$24410071107221
010200602C@0$24289071107231
010200602C@0$20204071107231
010200602C@0$21782071107331
010200602C@0$13340071108021
010200602C@0$20410071108351
010200602C@0$20059071111531

Empresa, constante = 0
No hay credencial, se debe suponer A
El 'n�mero de empleado' tiene 5 d�gitos, sin contar el $
{$endif}

procedure TdmServerCalcNomina.PollExaminaASCII( var sDatos: String );
const
     K_LEN_CHECADA = 27;
     K_LEN_CHECADOTA = K_LEN_CHECADA + 4;
var
   iLength, iPos, iEmpleado: Integer;
   sTemp, sEmpleado, sCodEmpresa: String;
begin
     iLength := Length( sDatos );
     if FTransformar then
     begin
          iPos := Pos( TOKEN_ASISTENCIA, sDatos );
          if ( iPos > 0 ) then
          begin
               sTemp := sDatos;
               sEmpleado := Copy( sTemp, iPos + 3, 5 );
               with FEmployee do
               begin
                    Active := False;
                    with oZetaProvider do
                    begin
                         ParamAsVarChar( FEmployee, 'Valor', sEmpleado, K_ANCHO_G_TEX );
                    end;
                    Active := True;
                    if IsEmpty then
                       iEmpleado := 0
                    else
                        iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                    Active := False;
               end;
               sDatos := Copy( sTemp, 1, iPos + 1 ) + Format( '%9.9dA', [ iEmpleado ] ) + Copy( sTemp, ( iPos + 8 ), ( iLength - ( iPos + 7 ) ) );
          end;
     end
     else
     begin
          iPos := Pos( TOKEN_ASISTENCIA + TOKEN_PROXIMIDAD, sDatos );
          if ( iPos >0 ) then
          begin
               sTemp := sDatos;
               sEmpleado := Copy( sTemp, iPos + 2, 10 );
               sEmpleado := ZetaCommonTools.PreparaGafeteProximidad( sEmpleado );
               //sEmpleado := IntToStr( StrToIntDef( sEmpleado, 0 ) );
               with FEmployee do
               begin
                    Active := False;
                    with oZetaProvider do
                    begin
                         ParamAsVarChar( FEmployee, 'Tarjeta', sEmpleado, K_ANCHO_G_TEX );
                    end;
                    Active := True;
                    if IsEmpty then
                    begin
                         iEmpleado   := 0;
                         sCodEmpresa := VACIO;
                    end
                    else
                    begin
                         iEmpleado   := FieldByName( 'CB_CODIGO' ).AsInteger;
                         sCodEmpresa := FieldByName( 'CM_DIGITO' ).AsString;
                    end;
                    Active := False;
               end;
               sTemp := Format( '%1.1s%9.9dA', [ sCodEmpresa, iEmpleado ] );
               sDatos := Copy( sDatos, 1, iPos) + sTemp + Copy( sDatos, ( iPos + 12 ), 9 );
          end
          else
          begin
               if ( iLength < K_LEN_CHECADOTA ) then { Agrega los ceros restantes para que el empleado sea de 9 digitos }
               begin
                    iPos := Pos( TOKEN_ASISTENCIA, sDatos );
                    if ( iPos > 0 ) then
                    begin
                         sTemp := sDatos;
                         iPos := iPos + 1;
                         sTemp := Copy( sTemp, 1, iPos ) + StringOfChar( '0', (  K_LEN_CHECADOTA - iLength ) ) + Copy( sTemp, ( iPos + 1 ), ( iLength - iPos ) );
                         sDatos := sTemp;
                    end;
               end;
          end;
     end;
end;

procedure TdmServerCalcNomina.PollValidaASCII(DataSet: TDataset; var iProblemas: Integer; var sErrorMsg: String );
const
     K_HORA_MAXIMA = 24;
var
   iMes, iDia, iYear: Word;
   sHora: String;

function EsEntero( const sValor: String ): Boolean;
begin
     try
        StrToInt( Trim( sValor ) );
        Result := True;
     except
           Result := False;
     end;
end;

procedure Error( const sMensaje: String );
begin
     Inc( iProblemas );
     sErrorMsg := sErrorMsg + '| ' + sMensaje + ' |';
end;

begin
     with Dataset do
     begin
          if ( FieldByName( 'PO_NUMERO' ).AsInteger <= 0 ) then
             Error( 'Empleado Inv�lido' );
          iMes := FieldByName( 'PO_MES' ).AsInteger;
          if not ( iMes in [ 1..12 ] ) then
             Error( 'Mes ' + IntToStr( iMes ) +  ' Inv�lido' );
          iYear := ZetaCommonTools.TheYear( Date );
          if ( ZetaCommonTools.TheMonth( Date ) < iMes ) then
             iYear := ( iYear - 1 );
          iDia := FieldByName( 'PO_DIA' ).AsInteger;
          if not ZetaCommonTools.DayIsValid( iYear, iMes, iDia ) then
             Error( 'D�a ' + IntToStr( iDia ) +  ' Inv�lido' );
          FieldByName( 'PO_FECHA' ).AsDateTime := EncodeDate( iYear, iMes, iDia );
          with FieldByName( 'PO_HORA' ) do
          begin
               sHora := AsString;
               if EsEntero( sHora ) and ZetaCommonTools.TimeIsValid( K_HORA_MAXIMA, sHora ) then
                  AsString := sHora
               else
                   Error( 'Hora ' + AsString +  ' Inv�lida' );
          end;
     end;
end;

function TdmServerCalcNomina.PollTotal(Parametros: OleVariant; var Lista: OleVariant): OleVariant;
var
   iEmpresas: Integer;
   FDataSet: TZetaCursor;
   sEmpresa, sCampo: String;
begin
     InitLog(NULL,'PollTotal');
     ModuloAutorizado( okAsistencia );
     with oZetaProvider do
     begin
          AsignaParamList( Parametros );
          with ParamList do
          begin
               FTransformar := ParamByName( 'TransformarNumero' ).AsBoolean;
               sEmpresa := ParamByName( 'Empresa' ).AsString;
               sCampo := ParamByName( 'Campo' ).AsString;
          end;
     end;
     PollWrite( Lista, sEmpresa, sCampo );
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          FDataSet := CreateQuery( Format( GetSQLScript( Q_COMPANYS_COUNT ), [ GetTipoCompany( Ord( tc3Datos ) ) ] ) );
          try
             with FDataSet do
             begin
                  Active := True;
                  iEmpresas := Fields[ 0 ].AsInteger;
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataset );
          end;
          if ( iEmpresas = 0 ) then
             ZetaCommonTools.SetOLEVariantToNull( Lista )
          else
          begin
               Lista := VarArrayCreate( [ 1, iEmpresas, 1, 2 ], varVariant );
               VarArrayLock( Lista );
               try
                  iEmpresas := 1;
                  FDataset := CreateQuery( Format( GetSQLScript( Q_COMPANYS_LIST ), [ GetTipoCompany( Ord( tc3Datos ) ) ] ) );
                  try
                     with FDataSet do
                     begin
                          Active := True;
                          while not Eof do
                          begin
                               Lista[ iEmpresas, 1 ] := FieldByName( 'CM_NOMBRE' ).AsString;
                               Lista[ iEmpresas, 2 ] := BuildEmpresa( FDataset );
                               Inc( iEmpresas );
                               Next;
                          end;
                          Active := False;
                     end;
                  finally
                         FreeAndNil( FDataSet );
                  end;
               finally
                      VarArrayUnlock( Lista );
               end;
          end;
     end;
     Result := cdsLog.Data;
     EndLog;SetComplete;
end;

function TdmServerCalcNomina.GetDigitoEmpresa( const sCodigoEmpresa: String ): String;
var
   FCursor: TZetaCursor;
   oEmpresa: OleVariant;
begin
     Result := VACIO;
     with oZetaProvider do
     begin
          oEmpresa:= EmpresaActiva;
          EmpresaActiva:= Comparte;
          FCursor := CreateQuery;
          try
             if AbreQueryScript( FCursor, Format( GetSQLScript( Q_POLL_GET_DIGITO ), [ EntreComillas( sCodigoEmpresa ) ] ) ) then
             begin
                  if ( not FCursor.IsEmpty ) then
                     Result := FCursor.FieldByName( 'CM_DIGITO' ).AsString;
             end;
          finally
             FreeAndNil( FCursor );
             EmpresaActiva:= oEmpresa;
          end;
     end;
end;

{ ********** Procesar Tarjetas Pendientes *********** }

function TdmServerCalcNomina.ProcesarTarjetasPendientes(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'ProcesarTarjetasPendientes');
     Result := ProcesaTarjetas( Empresa, Parametros);
     Endlog;SetComplete;
end;

{ ********** Procesa Tarjetas *********** }

function TdmServerCalcNomina.ProcesaTarjetas(Empresa, Parametros: OleVariant): OleVariant;
var
   sEmpresa, sHora, sSQL, sReloj, sCredencial, sMensaje: String;
   dInicial, dFinal, dFecha, dLimite: TDate;
   iEmpleado: TNumEmp;
   iTotal: Integer;
   lOk, lPuedeModificar: Boolean;
   FPoll, FListaDisp: TZetaCursor;
   lListaDispositivos: Boolean; {OP: 11/02/09}

   {AP(03/Jun/2008: Ahora es un m�todo compartido
   function GetDigitoEmpresa: String;
   begin
        Result := VACIO;
        with oZetaProvider do
        begin
             FPoll := CreateQuery;
             try
                if AbreQueryScript( FPoll, Format( GetSQLScript( Q_POLL_GET_DIGITO ), [ EntreComillas( Empresa[ P_CODIGO ] ) ] ) ) then
                begin
                     if ( not FPoll.IsEmpty ) then
                        Result := FPoll.FieldByName( 'CM_DIGITO' ).AsString;
                end;
             finally
                FreeAndNil( FPoll );
             end;
        end;
   end;}

   { OP: 11/02/09 - Verifica si existe lista de dispositivos }
   procedure VerifiaListaDispositivos;
   begin
        lListaDispositivos := False;
        with oZetaProvider do
        begin
             FListaDisp := CreateQuery( Format( GetSQLScript( Q_DISPOSITIVOS_X_COMPANIA ), [ Empresa[ P_CODIGO ] ] ) );
             try
                with FListaDisp do
                begin
                     Active := True;
                     if Not Eof then
                        lListaDispositivos := True;
                     Active := False;
                end;
             finally
                    FreeAndNil( FListaDisp );
             end;
        end;                        
   end;
begin
     ModuloAutorizado( okAsistencia );
     Result := GetEmptyProcessResult( prASISProcesarTarjetas ); { Todo bien }
     with oZetaProvider do
     begin
{
          2.4.97 : Ya no se lee el Digito de Gafete de Global se obtiene de COMPARTE\COMPANY.CM_CODIGO
          EmpresaActiva := Empresa;
          InitGlobales;
          sEmpresa := GetGlobalString( K_GLOBAL_DIGITO_EMPRESA );
}
          EmpresaActiva := Empresa;
          InitGlobales;
          dLimite := GetGlobalDate( K_GLOBAL_FECHA_LIMITE );

          EmpresaActiva := Comparte;
          AsignaParamList( Parametros );
          sEmpresa := GetDigitoEmpresa( Empresa[P_CODIGO]);
          if ZetaCommonTools.StrLleno( sEmpresa ) then
          begin
               //EmpresaActiva := Comparte;
               {OP: 11/02/09}
               VerifiaListaDispositivos;
               if lListaDispositivos then
                  FPoll := CreateQuery( Format( GetSQLScript( Q_POLL_RANGO_FECHAS_DISP ), [ sEmpresa, Empresa[ P_CODIGO ] ] ) )
               else
                   FPoll := CreateQuery( Format( GetSQLScript( Q_POLL_RANGO_FECHAS ), [ sEmpresa ] ) );
               try
                  with FPoll do
                  begin
                       Active := True;
                       if Eof then
                       begin
                            dInicial := NullDateTime;
                            dFinal := dInicial;
                            iTotal := 0;
                            lOk := False;
                       end
                       else
                       begin
                            dInicial := FieldByName( 'FECHA_MIN' ).AsDateTime;
                            dFinal := FieldByName( 'FECHA_MAX' ).AsDateTime;
                            iTotal := FieldByName( 'TOTAL' ).AsInteger;
                            lOk := True;
                       end;
                       Active := False;
                  end;
               finally
                      FreeAndNil( FPoll );
               end;

               if lOk then
               begin
                    lPuedeModificar := GetGlobalBooleano(K_GLOBAL_BLOQUEO_X_STATUS) or
                                       ParamList.ParamByName( 'PuedeCambiarTarjeta' ).AsBoolean;

                    EmpresaActiva := Comparte;
                    { GA: Crea un buffer para guardar las checadas que }
                    { fallan, dado que no se pueden estar borrando }
                    { en l�nea xq al hacerlo el query que las lee }
                    { se cuatrapea, sobre todo cuando hay muchas checadas por procesar ( > 1,000 ) }
                    {OP: 11/02/09}
                    if lListaDispositivos then
                       sSQL := Format( GetSQLScript( Q_POLL_PENDIENTES_DISP ), [ sEmpresa, Empresa[ P_CODIGO ] ] )
                    else
                        sSQL := Format( GetSQLScript( Q_POLL_PENDIENTES ), [ sEmpresa ] );
                    cdsLista.Lista := OpenSQL( Comparte, sSQL, False );
                    FPoll := CreateQuery( sSQL );
                    try
                       EmpresaActiva := Empresa;
                       InitGlobales;
                       if OpenProcess( prASISProcesarTarjetas, iTotal ) then
                       begin
                            InitRitmos;
                            InitTarjeta;
                            try
                               with FTarjeta do
                               begin
                                    DoPollBegin( dInicial, dFinal );
                               end;
                               with FPoll do
                               begin
                                    Active := True;
                                    while not Eof and CanContinue( FieldByName( 'PO_NUMERO' ).AsInteger ) do
                                    begin
                                         iEmpleado := FieldByName( 'PO_NUMERO' ).AsInteger;
                                         dFecha := FieldByName( 'PO_FECHA' ).AsDateTime;
                                         sHora := FieldByName( 'PO_HORA' ).AsString;
                                         sReloj := FieldByName( 'PO_LINX' ).AsString;
                                         sCredencial := FieldByName( 'PO_LETRA' ).AsString;

                                         if NOT PuedeCambiarTarjeta( dFecha, dFecha, dLimite, lPuedeModificar, sMensaje ) then
                                         begin
                                              lOk := FALSE;
                                              Log.Advertencia( iEmpleado,
                                                               'Checada Anterior/Igual a Fecha L�mite: ' + FechaCorta(dLimite) ,
                                                               Format( 'Empleado: %d' + CR_LF +
                                                                       'D�a: %s'  + CR_LF +
                                                                       'Hora: %s ', [ iEmpleado, FechaCorta(dFecha), FormatMaskText( '00:00;0', sHora ) ] ) + CR_LF +
                                                               sMensaje );
                                         end
                                         else
                                             lOK := FTarjeta.DoPoll( iEmpleado,
                                                                    dFecha,
                                                                    sReloj,
                                                                    sCredencial,
                                                                    sHora );
                                         if NOT lOk then
                                         begin
                                              with cdsLista do
                                              begin
                                                   Append;
                                                   try
                                                      FieldByName( 'PO_LINX' ).AsString := sReloj;
                                                      FieldByName( 'PO_EMPRESA' ).AsString := sEmpresa;
                                                      FieldByName( 'PO_NUMERO' ).AsInteger := iEmpleado;
                                                      FieldByName( 'PO_LETRA' ).AsString := sCredencial;
                                                      FieldByName( 'PO_FECHA' ).AsDateTime := dFecha;
                                                      FieldByName( 'PO_HORA' ).AsString := sHora;
                                                      Post;
                                                   except
                                                         on Error: Exception do
                                                         begin
                                                              Cancel;
                                                         end;
                                                   end;
                                              end;
                                         end;
                                         Next;
                                    end;
                                    Active := False;
                               end;
                               FTarjeta.DoPollEnd;
                            except
                                  on Error: Exception do
                                  begin
                                       Log.Excepcion( 0, 'Error Al Procesar POLL', Error );
                                  end;
                            end;
                       end;
                       ClearTarjeta;
                    finally
                           FreeAndNil( FPoll );
                    end;
                    EmpresaActiva := Comparte;
                    { Borra todas los POLLS de esta empresa }
                    EmpiezaTransaccion;
                    try
                       {OP: 11/02/09}
                       if lListaDispositivos then
                          ExecSQL( Comparte, Format( GetSQLScript( Q_POLL_BORRA_DISP ), [ sEmpresa, Empresa[ P_CODIGO ] ] ) )
                       else
                           ExecSQL( Comparte, Format( GetSQLScript( Q_POLL_BORRA ), [ sEmpresa ] ) );
                       TerminaTransaccion( True );
                    except
                          on Error: Exception do
                          begin
                               TerminaTransaccion( False );
                               EmpresaActiva := Empresa;
                               Log.Excepcion( 0, 'Error Al Borrar POLL', Error );
                          end;
                    end;
                    { Agregar los que tuvieron problemas }
                    with cdsLista do
                    begin
                         if not IsEmpty then
                         begin
                              FPoll := CreateQuery( GetSQLScript( Q_POLL_ESCRIBE ) );
                              try
                                 First;
                                 while not Eof do
                                 begin
                                      ParamAsVarChar( FPoll, 'PO_LINX', FieldByName( 'PO_LINX' ).AsString, K_ANCHO_LINX );
                                      ParamAsChar( FPoll, 'PO_EMPRESA', FieldByName( 'PO_EMPRESA' ).AsString, K_ANCHO_EMPRESA );
                                      ParamAsInteger( FPoll, 'PO_NUMERO', FieldByName( 'PO_NUMERO' ).AsInteger );
                                      ParamAsChar( FPoll, 'PO_LETRA', FieldByName( 'PO_LETRA' ).AsString, K_ANCHO_LETRA );
                                      ParamAsDate( FPoll, 'PO_FECHA', FieldByName( 'PO_FECHA' ).AsDateTime );
                                      ParamAsChar( FPoll, 'PO_HORA', FieldByName( 'PO_HORA' ).AsString, K_ANCHO_HORA );
                                      EmpiezaTransaccion;
                                      try
                                         Ejecuta( FPoll );
                                         TerminaTransaccion( True );
                                      except
                                            on Error: Exception do
                                            begin
                                                 TerminaTransaccion( False );
                                                 EmpresaActiva := Empresa;
                                                 Log.Excepcion( 0, 'Error Al Devolver POLL', Error );
                                                 EmpresaActiva := Comparte;
                                            end;
                                      end;
                                      Next;
                                 end;
                              finally
                                     FreeAndNil( FPoll );
                              end;
                         end;
                         EmptyDataset;
                    end;
                    EmpresaActiva := Empresa;
                    Result := CloseProcess;
               end
               else
               begin
                    raise Exception.Create( 'No Hay Tarjetas Pendientes Por Procesar' );
               end;
          end
          else
          begin
               raise Exception.Create( 'La Letra De La Empresa No Ha Sido Asignada' );
          end;
     end;
     { Se cambio a un m�todo privado para que no hiciera 2 setcomplete
     SetComplete;}
end;

{ ********* Recalcular Tarjetas ************* }

procedure TdmServerCalcNomina.RecalcularTarjetasParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Fecha Inicial: ' + FechaCorta(ParamByName( 'FechaInicial' ).AsDateTime) +
                              ', Fecha Final: ' + FechaCorta(ParamByName( 'FechaFinal' ).AsDateTime);
          FListaParametros := FListaParametros + K_PIPE + 'Revisar Horarios Actuales: ';
          if ( ParamByName( 'RevisarHorarios' ).AsBoolean ) then
               FListaParametros := FListaParametros + K_GLOBAL_SI
          else
               FListaParametros := FListaParametros + K_GLOBAL_NO;
          FListaParametros := FListaParametros + K_PIPE + 'Cancelar Horarios Temporales: ';
          if ( ParamByName( 'CancelarTemporales' ).AsBoolean ) then
               FListaParametros := FListaParametros + K_GLOBAL_SI
          else
               FListaParametros := FListaParametros + K_GLOBAL_NO;
          //FListaParametros := FListaParametros + K_PIPE + 'Autorizaciones: ';
          if ( ParamByName( 'CancelarExtras' ).AsBoolean ) then
               FListaParametros := FListaParametros + K_PIPE + 'Hrs. Extras: '+ K_GLOBAL_SI
          else
               FListaParametros := FListaParametros + K_PIPE + 'Hrs. Extras: '+ K_GLOBAL_NO;
          if ( ParamByName( 'CancelarDescansos' ).AsBoolean ) then
               FListaParametros := FListaParametros + K_PIPE + 'Descanso Trabajado: '+ K_GLOBAL_SI
          else
               FListaParametros := FListaParametros + K_PIPE + 'Descanso Trabajado: '+ K_GLOBAL_NO;
          if ( ParamByName( 'CancelarPermisosConGoce' ).AsBoolean ) then
               FListaParametros := FListaParametros + K_PIPE + 'Permisos C/Goce: '+ K_GLOBAL_SI
          else
               FListaParametros := FListaParametros + K_PIPE + 'Permisos C/Goce: '+ K_GLOBAL_NO;
          if ( ParamByName( 'CancelarPermisosSinGoce' ).AsBoolean ) then
               FListaParametros := FListaParametros + K_PIPE + 'Permisos S/Goce: '+ K_GLOBAL_SI
          else
               FListaParametros := FListaParametros + K_PIPE + 'Permisos S/Goce: '+ K_GLOBAL_NO;
          if ( ParamByName( 'CancelarHorasPrepagadas' ).AsBoolean ) then
               FListaParametros := FListaParametros + K_PIPE + 'Horas Prepagadas: '+ K_GLOBAL_SI
          else
               FListaParametros := FListaParametros + K_PIPE + 'Horas Prepagadas: '+ K_GLOBAL_NO;
          FListaParametros := FListaParametros + K_PIPE + 'Revisar Puesto y Area: ';
          if ( ParamByName( 'Reclasificar' ).AsBoolean ) then
               FListaParametros := FListaParametros + K_GLOBAL_SI
          else
               FListaParametros := FListaParametros + K_GLOBAL_NO;

          FListaParametros:= FListaParametros + K_PIPE + 'Reprocesar: ' + zBoolToStr( ParamByName('ReprocesarTarjetas').AsBoolean );
     end;
end;

function TdmServerCalcNomina.RecalcularTarjetas(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'RecalcularTarjetas');
     ModuloAutorizado( okAsistencia );
     InitBroker;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          RecalcularTarjetasParametros;
          RecalcularTarjetasBuildDataset;
          Result := RecalcularTarjetasDataset( oZetaSQLBroker.SuperReporte.DataSetReporte );
     end;
     EndLog;SetComplete;
end;

procedure TdmServerCalcNomina.RecalcularTarjetasBuildDataset;
var
   dInicial, dFinal: TDate;
begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               dInicial := ParamByName( 'FechaInicial' ).AsDate;
               dFinal := ParamByName( 'FechaFinal' ).AsDate;
          end;
          with oZetaSQLBroker do
          begin
               Init( enAusencia );
               with Agente do
               begin
                    AgregaColumna( 'CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
                    AgregaColumna( 'AU_FECHA', True, Entidad, tgFecha, 0, 'AU_FECHA' );
                    AgregaColumna( 'US_CODIGO', True, Entidad, tgNumero, 0, 'US_CODIGO' );
                    AgregaColumna( 'AU_HOR_MAN', True, Entidad, tgBooleano, 0, 'AU_HOR_MAN' );
                    AgregaColumna( 'HO_CODIGO', True, Entidad, tgTexto, 6, 'HO_CODIGO' );
                    AgregaColumna( 'AU_STATUS', True, Entidad, tgNumero, 0, 'AU_STATUS' );
                    // DES US #13995: Rec�lculo de tarjetas no determina el horario que le corresponde - Wizard de Recalcular tarjetas
                    AgregaColumna( 'AU_STA_FES', True, Entidad, tgNumero, 0, 'AU_STA_FES' );

                    AgregaFiltro( Format( '( AU_FECHA >= ''%s'' )', [ DateToStrSQL( dInicial ) ] ), True, Entidad );
                    AgregaFiltro( Format( '( AU_FECHA <= ''%s'' )', [ DateToStrSQL( dFinal ) ] ), True, Entidad );
                    AgregaOrden( 'CB_CODIGO', True, Entidad );
                    AgregaOrden( 'AU_FECHA', True, Entidad );
               end;
               AgregaRangoCondicion( ParamList );
               FiltroConfidencial( EmpresaActiva );
               BuildDataset( EmpresaActiva );
          end;
     end;
end;

function TdmServerCalcNomina.RecalcularTarjetasDataset( Dataset: TDataset): OleVariant;
var
   FLeeTurno, FBorraAuto, FRevisaHorario, FReclasificar: TZetaCursor;
   FStatusHorario: TStatusHorario;
   iEmpleado: TNumEmp;
   iUsuario: Integer;
   dInicial, dFinal, dFecha: TDate;
   sTurno, sLista,sTitulo,sDetalle: String;
   lHayLista, lHayCambio, lBorraAuto, lRevisaHorario, lEsManual, lTemporales, lReclasificar,lPuedeCambiarTarjetas: Boolean;
   eTipoDia: eStatusAusencia;
   sHorario: TCodigo;
   lAprobarAutorizaciones, lGlobalAutorizaciones, lAplicaCambios, lReprocesarTarjetas: boolean;
   oDatosHorario: TDatosHorario;
   // DES US #13995: Rec�lculo de tarjetas no determina el horario que le corresponde - Wizard de Recalcular tarjetas
   efFestivo: eStatusFestivo;
   sStatusHorarioCodigo: String;
   sHorarioFestivo: String;

   function GetFiltroAutorizacion: string;
   const
        K_FILTRO_AUTORIZACIONES_EXISTENTES = 'and ( US_COD_OK = 0 )';
   begin
        Result := VACIO;
        if not lAplicaCambios then
           Result := K_FILTRO_AUTORIZACIONES_EXISTENTES;
   end;


   procedure ReprocesarTarjetasDataset;
   var
      sEmpresa,sScript, sHora, sReloj, sCredencial: String;
      iEmpleado: Integer;
      dFecha, dInicio, dFin: TDate;
      lEsEmpleadoDif, lOk: Boolean;

      procedure BorraChecadasReloj;
      begin
           with oZetaProvider do
           begin
                ParamAsInteger( FBorraChecadas, 'Empleado', iEmpleado );
                ParamAsDate( FBorraChecadas, 'Fecha', dInicio );
                Ejecuta( FBorraChecadas );
           end;
      end;

   begin
        { ******************* Reprocesar Tarjetas ********************* }

        with oZetaProvider do
        begin
             if OpenProcess( prASISReprocesaTarjetas, Dataset.RecordCount, FListaParametros ) then
             begin
                  sEmpresa:= GetDigitoEmpresa( EmpresaActiva[P_CODIGO] );
                  InitQueries;
                  InitTarjeta;
                  InitRitmos;
                  FTarjeta.DoPollBegin( dInicial - 1, dFinal + 1);
                  FBorraChecadas := CreateQuery( GetSQLScript( Q_BORRA_CHECADAS_SIN_MODIFICAR ) );
                  FTarjeta.Queries.GetDatosHorarioBegin;
                  FTarjeta.Queries.RevisaStatusBloqueoBegin;
                  try
                     with DataSet do
                     begin
                          First;
                          iEmpleado:= 0;

                          while not EOF and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                          begin
                               lEsEmpleadoDif:= ( iEmpleado <> FieldByName('CB_CODIGO').AsInteger );
                               if ( lEsEmpleadoDif ) then
                               begin
                                    iEmpleado:= FieldByName('CB_CODIGO').AsInteger;
                                    dInicio := dInicial - 1;  // Se revisa un d�a antes por horarios de madrugada
                                    dFin := dFinal + 1;       // Se revisa un d�a despues por horarios de madrugada
                                    while ( dInicio <= dFin ) do
                                    begin
                                         if ( lPuedeCambiarTarjetas or FTarjeta.Queries.RevisaStatusBloqueo( dInicio, iEmpleado, sTitulo, sDetalle ) ) then
                                         begin
                                              EmpiezaTransaccion;
                                              try
                                                 sScript := Format( GetSQLScript( Q_CHECADAS_SIN_MODIFICAR ), [ iEmpleado, DateToStrSQLC( dInicio ) ] );
                                                 cdsLista.Lista:= OpenSQL( EmpresaActiva, sScript, TRUE );
                                                 if not ( cdsLista.IsEmpty ) then
                                                 begin
                                                      BorraChecadasReloj;
                                                      while not cdsLista.EOF do
                                                      begin
                                                           dFecha:= cdsLista.FieldByName( 'PO_FECHA' ).AsDateTime;
                                                           sHora:= cdsLista.FieldByName( 'PO_HORA' ).AsString;
                                                           sReloj := cdsLista.FieldByName( 'PO_LINX' ).AsString;
                                                           sCredencial := cdsLista.FieldByName( 'PO_LETRA' ).AsString;
                                                           oDatosHorario:= oZetaCreator.Queries.GetDatosHorario( cdsLista.FieldByName( 'HO_CODIGO' ).AsString );
                                                           SetHora24( sHora, dFecha, oDatosHorario.Tipo, aMinutos( oDatosHorario.UltimaSalidaMadrugada ) );

                                                           lOk:= FTarjeta.DoPoll( iEmpleado, dFecha, sReloj, sCredencial, sHora, FALSE );

                                                           if not lOk then
                                                              raise Exception.Create( Format( 'Error al Grabar Tarjeta: Fecha: %s', [ FechaAsStr( dFecha ) ] )  );

                                                           cdsLista.Next;
                                                      end;
                                                 end;
                                                 TerminaTransaccion( TRUE );
                                              except
                                                    on Error: Exception do
                                                    begin
                                                         RollBackTransaccion;
                                                         Log.Excepcion( iEmpleado, Format( 'Error Al Reprocesar Tarjetas de Empleado: %d', [iEmpleado] ), Error );
                                                    end;
                                              end;
                                         end
                                         else
                                         begin
                                              if strLleno( sTitulo ) then
                                                 Log.Advertencia( clbNinguno, iEmpleado, dInicio, sTitulo, sDetalle );
                                         end;
                                         dInicio := ( dInicio + 1 );
                                    end;
                               end;
                               Next;
                          end;
                     end;
                  finally
                         FTarjeta.Queries.RevisaStatusBloqueoEnd;
                         FTarjeta.Queries.GetDatosHorarioEnd;
                         FreeAndNil( FBorraChecadas );
                         FTarjeta.DoPollEnd;
                         ClearTarjeta;
                  end;
             end;
             Result := CloseProcess;
        end;
   end;


begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               dInicial := ParamByName( 'FechaInicial' ).AsDate;
               dFinal := ParamByName( 'FechaFinal' ).AsDate;
               lRevisaHorario := ParamByName( 'RevisarHorarios' ).AsBoolean;
               lTemporales := ParamByName( 'CancelarTemporales' ).AsBoolean and lRevisaHorario;
               lBorraAuto := ConfiguraBorrarAuto( sLista );
               lHayLista := ZetaCommonTools.StrLleno( sLista );
               lReclasificar := ParamByName( 'Reclasificar' ).AsBoolean;
               //Derecho de Aprobar Autorizaciones
               lAprobarAutorizaciones := ParamByName( 'AprobarAutorizaciones' ).AsBoolean;
               lReprocesarTarjetas:= ParamByName('ReprocesarTarjetas').AsBoolean;
               lPuedeCambiarTarjetas := ParamByName( 'PuedeCambiarTarjeta' ).AsBoolean;
          end;
          InitGlobales;
          //Derecho de Aprobar Autorizaciones
          lGlobalAutorizaciones := oZetaProvider.GetGlobalBooleano( K_GLOBAL_APROBAR_AUTORIZACIONES );

          //Se determina si se Aplicaran los cambios con Respecto al Global Aprobar Autorizaciones
          lAplicaCambios := lAprobarAutorizaciones;
          if not lGlobalAutorizaciones then
             lAplicaCambios := TRUE;

          if OpenProcess( prASISRecalculoTarjetas, Dataset.RecordCount, FListaParametros ) then
          begin
              InitRitmos;
              try
                 InitTarjeta;
                 // Antes de empezar
                 with FTarjeta do
                 begin
                      GetTurnoHorarioBegin( dInicial, dFinal );
                      CalculaTarjetaBegin;
                      Queries.RevisaStatusBloqueoBegin;
                 end;
                 //CV: CreateQuery--ok
                 //Variable local
                 if lHayLista then
                    FBorraAuto := CreateQuery( Format( GetSQLScript( Q_RECALCULAR_TARJETAS_BORRA_AUTO ), [ ZetaCommonTools.CortaUltimo( sLista ), GetFiltroAutorizacion ] ) )
                 else
                     FBorraAuto := nil;
                 //CV: CreateQuery--ok
                 //Variable local
                 if lReclasificar then
                    FReclasificar := CreateQuery( GetSQLScript( Q_RECALCULAR_TARJETAS_RECLASIFICAR ) )
                 else
                     FReclasificar := nil;
                 FRevisaHorario := CreateQuery( GetSQLScript( Q_RECALCULAR_TARJETAS_REVISA_HORARIO ) );
                 ParamAsBoolean( FRevisaHorario, 'EsManual', False );
                 FLeeTurno := CreateQuery( GetSQLScript( Q_TURNO_LEE ) );
                 // Ejecuci�n
                 with Dataset do
                 begin
                      while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                      begin
                           iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                           iUsuario := FieldByName( 'US_CODIGO' ).AsInteger;
                           dFecha := FieldByName( 'AU_FECHA' ).AsDateTime;
                           sHorario := FieldByName( 'HO_CODIGO' ).AsString;
                           eTipoDia := eStatusAusencia( FieldByName( 'AU_STATUS' ).AsInteger );
                           lEsManual := ZetaCommonTools.zStrToBool( FieldByName( 'AU_HOR_MAN' ).AsString );
                           // DES US #13995: Rec�lculo de tarjetas no determina el horario que le corresponde - Wizard de Recalcular tarjetas
                           efFestivo := eStatusFestivo( FieldByName( 'AU_STA_FES' ).AsInteger );

                           if ( lPuedeCambiarTarjetas or FTarjeta.Queries.RevisaStatusBloqueo( dFecha, iEmpleado, sTitulo,sDetalle ) )then
                           begin
                                 EmpiezaTransaccion;
                                 try
                                    if ( lEsManual and lTemporales ) or ( not lEsManual and lRevisaHorario ) then
                                    begin
                                         with FLeeTurno do
                                         begin
                                              Active := False;
                                              ParamAsInteger( FLeeTurno, 'Empleado', iEmpleado );
                                              ParamAsDate( FLeeTurno, 'Fecha', dFecha );
                                              Active := True;
                                              if IsEmpty then
                                                 sTurno := ''
                                              else
                                                  sTurno := FieldByName( 'CB_TURNO' ).AsString;
                                              Active := False;
                                         end;
                                         if ZetaCommonTools.StrVacio( sTurno ) then
                                            raise Exception.Create( 'Turno Indeterminado Para Esta Fecha' );
                                         with FTarjeta do
                                         begin
                                              FStatusHorario := Ritmos.GetStatusHorario( sTurno, dFecha );
                                              // DES US #13995: Rec�lculo de tarjetas no determina el horario que le corresponde - Wizard de Recalcular tarjetas
                                              sStatusHorarioCodigo := Ritmos.GetEmpleadoDatosTurno(iEmpleado, dFecha).StatusHorario.Horario;
                                              sHorarioFestivo := Ritmos.GetEmpleadoDatosTurno(iEmpleado, dFecha).HorarioFestivo;
                                              if sHorarioFestivo = VACIO then
                                                 sHorarioFestivo := sStatusHorarioCodigo;
                                         end;
                                         with FStatusHorario do
                                         begin
                                              // DES US #13995: Rec�lculo de tarjetas no determina el horario que le corresponde - Wizard de Recalcular tarjetas.
                                              if (efFestivo = esfFestivo) and (Horario <> sHorarioFestivo) then
                                                 Horario := sHorarioFestivo;
                                              if (efFestivo = esfFestivoTransferido) and (Horario = sHorarioFestivo)then
                                                 Horario := sStatusHorarioCodigo;

                                              lHayCambio := ( sHorario <> Horario ) or ( eTipoDia <> Status );
                                         end;
                                         if ( lEsManual and lTemporales ) or ( not lEsManual and lRevisaHorario and lHayCambio ) then
                                         begin
                                              ParamAsDate( FRevisaHorario, 'FECHA', dFecha );
                                              ParamAsInteger( FRevisaHorario, 'EMPLEADO', iEmpleado );

                                              with FStatusHorario do
                                              begin
                                                   ParamAsChar( FRevisaHorario, 'Horario', Horario, K_ANCHO_HORARIO );
                                                   ParamAsInteger( FRevisaHorario, 'Status', Ord( Status ) );
                                                   sHorario := Horario;
                                                   eTipoDia := Status;
                                              end;
                                              Ejecuta( FRevisaHorario );
                                         end;
                                    end;
                                    if lBorraAuto then
                                    begin
                                         ParamAsInteger( FBorraAuto, 'Empleado', iEmpleado );
                                         ParamAsDate( FBorraAuto, 'Fecha', dFecha );
                                         Ejecuta( FBorraAuto );
                                    end;
                                    if lReclasificar then
                                    begin
                                         ParamAsInteger( FReclasificar, 'Empleado', iEmpleado );
                                         ParamAsDate( FReclasificar, 'Fecha', dFecha );
                                         Ejecuta( FReclasificar );
                                    end;
                                    FTarjeta.CalculaTarjeta( iEmpleado, dFecha, sHorario, eTipoDia, iUsuario );
                                    TerminaTransaccion( True );
                                 except
                                       on Error: Exception do
                                       begin
                                            //TerminaTransaccion( False );
                                            RollBackTransaccion;
                                            Log.Excepcion( iEmpleado, 'Error Al Recalcular Tarjeta ' + FechaCorta( dFecha ), Error );
                                       end;
                                 end;
                           end
                           else
                           begin
                                if strLleno(sTitulo)then
                                begin
                                      Log.Error(iEmpleado,sTitulo,sDetalle);
                                end;
                           end;
                           Next;
                      end;
                      {
                      Checa que el Global este Prendido, que se tenga derecho de Acceso y
                      que se haya seleccionado alguno de los checkboxes del Wizard para
                      Borrar Autorizaciones Existentes
                      }
                      if lGlobalAutorizaciones and not lAprobarAutorizaciones and lBorraAuto then
                         Log.Advertencia( 0, 'No se Borraron Autorizaciones Aprobadas Existentes' );
                 end;
                 with FTarjeta do
                 begin
                      Queries.RevisaStatusBloqueoEnd;
                      CalculaTarjetaEnd;
                      GetTurnoHorarioEnd;
                 end;

              except
                    on Error: Exception do
                    begin
                         Log.Excepcion( 0, 'Error Al Recalcular Tarjetas', Error );
                    end;
              end;
              FreeAndNil( FReclasificar );
              FreeAndNil( FBorraAuto );
              FreeAndNil( FLeeTurno );
              FreeAndNil( FRevisaHorario );

          end;
          ClearTarjeta;
          Result := CloseProcess;

                    
          { El procesamiento de tarjetas se hace despues de que termina,
            porque hay un openprocess y closeprocess dentro de procesartarjetasdataset}

          if ( lReprocesarTarjetas ) then
             ReprocesarTarjetasDataset;

          SetComplete;
     end;
end;

function TdmServerCalcNomina.ConfiguraBorrarAuto(var sLista: String): Boolean;
begin
     with oZetaProvider.ParamList do
     begin
          Result := ParamByName( 'CancelarExtras' ).AsBoolean or
                    ParamByName( 'CancelarDescansos' ).AsBoolean or
                    ParamByName( 'CancelarPermisosConGoce' ).AsBoolean or
                    ParamByName( 'CancelarPermisosSinGoce' ).AsBoolean or
                    ParamByName( 'CancelarHorasPrepagadas' ).AsBoolean;
          sLista := '';
          if ParamByName( 'CancelarExtras' ).AsBoolean then
          begin
               sLista := sLista + IntToStr( Ord( chExtras ) ) + ',';
          end;
          if ParamByName( 'CancelarDescansos' ).AsBoolean then
          begin
               sLista := sLista + IntToStr( Ord( chDescanso ) ) + ',';
          end;
          if ParamByName( 'CancelarPermisosConGoce' ).AsBoolean then
          begin
               sLista := sLista + IntToStr( Ord( chConGoceEntrada ) ) + ',' + IntToStr( Ord( chConGoce ) ) + ',';
          end;
          if ParamByName( 'CancelarPermisosSinGoce' ).AsBoolean then
          begin
               sLista := sLista + IntToStr( Ord( chSinGoceEntrada ) ) + ',' + IntToStr( Ord( chSinGoce ) ) + ',';
          end;
          if ParamByName( 'CancelarHorasPrepagadas' ).AsBoolean then
          begin
               sLista := sLista + IntToStr( Ord( chPrepagFueraJornada ) ) + ',' + IntToStr( Ord( chPrepagDentroJornada ) ) + ',';
          end;
     end;
end;

{ ******** Registros Automaticos ********** }

procedure TdmServerCalcNomina.RegistrosAutomaticosParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Fecha Inicial: ' + FechaCorta(ParamByName( 'FechaInicial' ).AsDateTime) +
                         K_PIPE + 'Fecha Final: ' + FechaCorta(ParamByName( 'FechaFinal' ).AsDateTime);
          FListaParametros := FListaParametros + K_PIPE + 'Agregar Checada de Entrada :';
          if ParamByName('AgregarEntrada').AsBoolean then
              FListaParametros := FListaParametros + 'Si'
          else
              FListaParametros := FListaParametros + 'No';
          FListaParametros := FListaParametros + K_PIPE + 'Agregar Checada de Salida :';
          if ParamByName('AgregarSalida').AsBoolean then
              FListaParametros := FListaParametros + 'Si'
          else
              FListaParametros := FListaParametros + 'No';
          if StrLleno( ParamByName('Motivo').AsString ) then
              FListaParametros := FListaParametros + K_PIPE + 'Motivo :' + ParamByName('Motivo').AsString;
     end;
end;

function TdmServerCalcNomina.RegistrosAutomaticos(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'RegistrosAutomaticos');
     ModuloAutorizado( okAsistencia );
     InitBroker;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          RegistrosAutomaticosParametros;
          RegistrosAutomaticosBuildDataset;
          Result := RegistrosAutomaticosDataset( oZetaSQLBroker.SuperReporte.DataSetReporte );
     end;
     EndLog;SetComplete;
end;

procedure TdmServerCalcNomina.RegistrosAutomaticosBuildDataset;
const
     K_FILTRO = '( ( ( CB_ACTIVO = ''S'' ) and ( CB_FEC_ING <= ''%s'' ) ) or ( ( CB_ACTIVO <> ''S'' ) and ( CB_FEC_BAJ >= ''%s'' ) ) )';
var
   dInicial, dFinal: TDate;
begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               dInicial := ParamByName( 'FechaInicial' ).AsDate;
               dFinal := ParamByName( 'FechaFinal' ).AsDate;
          end;
          with oZetaSQLBroker do
          begin
               Init( enEmpleado );
               with Agente do
               begin
                    AgregaColumna( 'CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
                    AgregaFiltro( '( CB_CHECA = ''S'' )', True, Entidad );
                    AgregaFiltro( Format( K_FILTRO, [ ZetaCommonTools.DateToStrSQL( dFinal ), ZetaCommonTools.DateToStrSQL( dInicial ) ] ), True, Entidad );
                    AgregaOrden( 'CB_CODIGO', True, Entidad );
               end;
               AgregaRangoCondicion( ParamList );
               FiltroConfidencial( EmpresaActiva );
               BuildDataset( EmpresaActiva );
          end;
     end;
end;

function TdmServerCalcNomina.RegistrosAutomaticosDataset(Dataset: TDataset): OleVariant;
type
    eTipoCambioFecha = ( tcfTarjeta, tcfEntrada, tcfSalida );
var
   dFecha, dInicial, dFinal: TDate;
   iEmpleado: TNumEmp;
   sMotivo,sTitulo,sDetalle : String;
   iUsuario, iChecadas, iHora, iMedioDia: Integer;
   lAddEntrada, lAddSalida, lHayEntrada, lHaySalida, lSabados, lDescanso, lExiste,lPuedeCambiarTarjetas: Boolean;
   eStatus: eStatusEmpleado;
   eTipoDia: eStatusAusencia;
   sHorario, sEntrada, sSalida: TCodigo;
   FDatosHorario: TDatosHorario;
   FExisten: TZetaCursor;
   FDatosTarjeta: TDatosTarjeta;
begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               dInicial := ParamByName( 'FechaInicial' ).AsDate;
               dFinal := ParamByName( 'FechaFinal' ).AsDate;
               lAddEntrada := ParamByName( 'AgregarEntrada' ).AsBoolean;
               lAddSalida := ParamByName( 'AgregarSalida' ).AsBoolean;
               lSabados := ParamByName( 'AgregarSabados' ).AsBoolean;
               lDescanso := ParamByName( 'AgregarDescanso' ).AsBoolean;
               sMotivo := ParamByName( 'Motivo' ).AsString;
               lPuedeCambiarTarjetas := ParamByName( 'PuedeCambiarTarjeta' ).AsBoolean;
          end;
          InitGlobales;
          if OpenProcess( prASISRegistrosAut, Dataset.RecordCount, FListaParametros ) then
          begin
               InitRitmos;
               try
                  InitTarjeta;
                  try
                     // Antes de empezar
                     with FTarjeta do
                     begin
                          AgregaTarjetaDiariaBegin( dInicial, dFinal );
                          AgregaChecadasBegin;
                          Queries.RevisaStatusBloqueoBegin;
                     end;
                     with oZetaCreator.Queries do
                     begin
                          GetStatusEmpleadoBegin;
                          GetDatosHorarioBegin;
                     end;
                     //CV: CreateQuery--ok
                     //Variable local
                     FExisten := CreateQuery( GetSQLScript( Q_HAY_CHECADAS ) );
                     // Ejecuci�n
                     with Dataset do
                     begin
                          while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                          begin
                               iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                               dFecha := dInicial;
                               while ( dFecha <= dFinal ) do
                               begin
                                    eStatus := oZetaCreator.Queries.GetStatusEmpleado( iEmpleado, dFecha );
                                    if ( eStatus in [ steEmpleado ] ) then
                                    begin
                                         if ( lPuedeCambiarTarjetas or FTarjeta.Queries.RevisaStatusBloqueo(dFecha,iEmpleado,sTitulo,sDetalle) )then
                                         begin
                                               lExiste :=  FTarjeta.AgregaTarjetaDiariaExiste( iEmpleado, dFecha, FDatosTarjeta );
                                               if (FDatosTarjeta.Status = auHabil) or
                                                  (lSabados and (FDatosTarjeta.Status = auSabado) ) or
                                                  (lDescanso and (FDatosTarjeta.Status = auDescanso) )then
                                               begin
                                                    EmpiezaTransaccion;
                                                    try
                                                       if not lExiste then
                                                       begin
                                                            FTarjeta.AgregaTarjetaDiariaEscribe( iEmpleado, dFecha, FDatosTarjeta );
                                                       end;
                                                       with FDatosTarjeta do
                                                       begin
                                                            sHorario := Horario;
                                                            eTipoDia := Status;
                                                            iUsuario := Usuario;
                                                       end;
                                                       with oZetaCreator.Queries do
                                                       begin
                                                            with GetDatosHorario( sHorario ) do
                                                            begin
                                                                 sEntrada := ZetaCommonTools.GetEntrada24( Tipo, Entrada );
                                                                 sSalida := ZetaCommonTools.GetSalida24( Tipo, Entrada, Salida );
                                                                 iMedioDia := ZetaCommonTools.aMinutos( sEntrada );
                                                                 iMedioDia := iMedioDia + Trunc( ( ZetaCommonTools.aMinutos( sSalida ) - iMedioDia ) / 2 );
                                                            end;
                                                       end;
                                                       with FExisten do
                                                       begin
                                                            Active := False;
                                                            ParamAsInteger( FExisten, 'Empleado', iEmpleado );
                                                            ParamAsDate( FExisten, 'Fecha', dFecha );
                                                            Active := True;
                                                            iChecadas := Fields[ 0 ].AsInteger;
                                                            iHora := ZetaCommonTools.aMinutos( Fields[ 1 ].AsString );
                                                            Active := False;
                                                       end;
                                                       case iChecadas of
                                                            { Cuando no hay checadas, SIEMPRE se pueden }
                                                            { agregar registros autom�ticos }
                                                            0:
                                                            begin
                                                                 lHayEntrada := False;
                                                                 lHaySalida := False;
                                                            end;
                                                            { Cuando hay una checada, se debe de tomar como referencia }
                                                            { la mitad entre la hora de entrada y la hora de salida  }
                                                            { del horario de la tarjeta para saber si la checada se }
                                                            { parece a una entrada � a una salida }
                                                            1:
                                                            begin
                                                                 lHayEntrada := ( iHora < iMedioDia );
                                                                 lHaySalida := not lHayEntrada;
                                                            end;
                                                       else
                                                           { En todos los dem�s casos, no se pueden }
                                                           { agregar checadas autom�ticas porque }
                                                           { ya existen muchas checadas (2+) en la tarjeta }
                                                           begin
                                                                lHayEntrada := True;
                                                                lHaySalida := True;
                                                           end;
                                                       end;
                                                       if lAddEntrada and not lHayEntrada then
                                                       begin
                                                            with FDatosHorario do
                                                            begin
                                                                 FTarjeta.AgregaChecada( iEmpleado,
                                                                                         dFecha,
                                                                                         sEntrada,
                                                                                         '',
                                                                                         sMotivo,
                                                                                         chEntrada,
                                                                                         dcOrdinaria,
                                                                                         0,
                                                                                         UsuarioActivo,
                                                                                         True );
                                                            end;
                                                            {$ifdef INTERMEX}
                                                            Log.Evento( clbNinguno, iEmpleado, dFecha, Format( 'Checada de entrada agregada con hora %s.', [ sEntrada ] ) );
                                                            {$endif}
                                                       end;
                                                       if lAddSalida and not lHaySalida then
                                                       begin
                                                            with FDatosHorario do
                                                            begin
                                                                 FTarjeta.AgregaChecada( iEmpleado,
                                                                                         dFecha,
                                                                                         sSalida,
                                                                                         '',
                                                                                         sMotivo,
                                                                                         chSalida,
                                                                                         dcOrdinaria,
                                                                                         0,
                                                                                         UsuarioActivo,
                                                                                         True );
                                                            end;
                                                            {$ifdef INTERMEX}
                                                            Log.Evento( clbNinguno, iEmpleado, dFecha, Format( 'Checada de salida agregada con hora %s.', [ sSalida ] ) );
                                                            {$endif}
                                                       end;
                                                       FTarjeta.CalculaTarjeta( iEmpleado, dFecha, sHorario, eTipoDia, iUsuario );
                                                       TerminaTransaccion( True );
                                                    except
                                                          on Error: Exception do
                                                          begin
                                                               //TerminaTransaccion( False );
                                                               RollBackTransaccion;
                                                               Log.Excepcion( iEmpleado, 'Error En Tarjeta Autom�tica ' + FechaCorta( dFecha ), Error );
                                                          end;
                                                    end;
                                               end // if lOk
                                               else
                                                   Log.Evento( clbNinguno, iEmpleado, dFecha, 'Empleado No Procesado ( Dia = ' + ObtieneElemento( lfStatusAusencia, Ord(FDatosTarjeta.Status )) + '  )' );
                                         end
                                         else
                                         begin
                                              if strLleno(sTitulo)then
                                              begin
                                                    Log.Error(iEmpleado,sTitulo,sDetalle);
                                              end;
                                         end;
                                    end
                                    else
                                        Log.Evento( clbNinguno, iEmpleado, dFecha, 'Empleado No Procesado ( Status = ' + ZetaCommonLists.ObtieneElemento( lfStatusEmpleado, Ord( eStatus ) ) + ' )' );
                                    dFecha := dFecha + 1;
                               end;
                               Next;
                          end;
                     end;
                     with oZetaCreator.Queries do
                     begin
                          GetDatosHorarioEnd;
                          GetStatusEmpleadoEnd;
                     end;
                     { Al terminar }
                     with FTarjeta do
                     begin
                          AgregaChecadasEnd;
                          AgregaTarjetaDiariaEnd;
                          Queries.RevisaStatusBloqueoEnd;
                     end;
                  except
                        on Error: Exception do
                        begin
                             Log.Excepcion( 0, 'Error Al Corregir Fecha de Tarjetas', Error );
                        end;
                  end;
               finally
                      ClearTarjeta;
                      FreeAndNil(FExisten);
               end;
          end;
          Result := CloseProcess;
     end;
end;


{ ********* Transferencia de Empleados ************* }

procedure TdmServerCalcNomina.TransferenciaParametros( const sEmpresaDestino: string );
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := 'Empleado Origen: ' + ParamByName( 'EmpleadoSource' ).AsString +
                              ZetacommonClasses.K_PIPE + 'Empresa Destino: ' + sEmpresaDestino +
                              ZetacommonClasses.K_PIPE + 'Empleado Destino: ' + ParamByName( 'EmpleadoTarget' ).AsString;{OP: 24/06/08}
     end;
end;

procedure TdmServerCalcNomina.TransfiereUsuario( Empresa, EmpresaDestino: OleVariant; const iEmpleado, iNewEmpleado : integer );
 var
    FUsuario: TUsuarioData;
begin
     FUsuario:= TUsuarioData.Create(oZetaProvider);
     try
        FUsuario.TransfiereEmpleado( Empresa, EmpresaDestino, iEmpleado, iNewEmpleado );
     finally
          FreeAndNil( FUsuario );
     end;
end;

function TdmServerCalcNomina.Transferencia(Empresa, EmpresaDestino, Parametros: OleVariant): OleVariant;
const
     K_NINGUNA = 0;
     K_TRANSFERIR = 1;
     K_EMP_TRANSF = 2;
     K_BORRAR = 3;
     // SOP 4968 [Profesional] Error al querer transferir empleado (Empleados>Procesos>Transferencias de Empleado)
     // K_EXCLUIDOS = 'PRETTYNAME;CB_TIP_REV;CB_FEC_SAL;LLAVE;PRETTY_EXP';
     K_EXCLUIDOS = 'PRETTYNAME;CB_TIP_REV;CB_FEC_SAL;LLAVE;PRETTY_EXP;AU_JORNADA;AU_STATEMP,AC_ID';

     aQryTransfiere: array[FALSE..TRUE] of Integer = ( Q_TRANSFERENCIA_LEE, Q_TRANSFERENCIA_MEDICOS_LEE );
     aQryTransfiereBorra: array[FALSE..TRUE] of Integer = ( Q_TRANSFERENCIA_BORRA, Q_TRANSFERENCIA_MEDICOS_BORRA );
var
   i, iActividad: Byte;
   iEmpleado, iNewEmpleado, iConcepto, iKardexNivel, iMaxFolioExp {$ifndef DOS_CAPAS}, iNumeroBiometrico{$endif}: Integer;{OP: 27/05/08}
   sTabla, sMsg, sKardexTipo, sKardexDescrip, sEmpresaCodigo, sEmpresaDescrip, sRS_CODIGO: String;{OP: 27/05/08}
   lCambiaNumero, lEmpleadoActivo:Boolean;
   oZetaProviderDestino: TdmZetaServerProvider;
   FOrigen, FDestino: TZetaCursor;
   dEmpleadoFecBaj: TDate;{OP: 28/05/08}

   function EsDestinoPruebas: boolean;
   var
      oLicMgr  :   TLicenseMgr;
   begin
        oLicMgr := TLicenseMgr.Create( oZetaProviderDestino );
        try
                Result := oLicMGr.GetTipoValidoCompany = tc3Prueba ;
        finally
               FreeAndNil( oLicMgr );
        end;
   end;

   {OP: 27/05/08}
   procedure GrabaMovimientoKardex;
   var
      FDataSet : TZetaCursor;
   begin
        with oZetaProviderDestino do
        begin
             FDataSet := CreateQuery( GetSQLScript( Q_AGREGA_KARDEX ) );
             try
                ParamAsInteger( FDataSet, 'CB_CODIGO', iNewEmpleado );
                if( lEmpleadoActivo ) then
                     ParamAsDate( FDataSet, 'CB_FECHA', Date )
                else
                begin
                     if ( dEmpleadoFecBaj < Date ) then
                        ParamAsDate( FDataSet, 'CB_FECHA', dEmpleadoFecBaj )
                     else
                        ParamAsDate( FDataSet, 'CB_FECHA', Date );
                end;
                ParamAsString( FDataSet, 'CB_TIPO', sKardexTipo );
                ParamAsString( FDataSet, 'CB_COMENTA', sKardexDescrip );
                ParamAsInteger( FDataSet, 'CB_NIVEL', iKardexNivel );
                ParamAsString( FDataSet, 'CB_NOTA', Format( 'Fecha de Movimiento: %s '+ CR_LF +'Empresa: %s - %s '+ CR_LF +'N�mero de empleado: %d', [ FechaAsStr( Date ), sEmpresaCodigo, sEmpresaDescrip, iEmpleado ] ) );
                Ejecuta( FDataSet );
             finally
                    FreeAndNil( FDataSet );
             end;
             FDataSet := CreateQuery( GetSQLScript( Q_RECALCULA_KARDEX ) );
             try
                ParamAsInteger( FDataSet, 'EMPLEADO', iNewEmpleado );
                Ejecuta( FDataSet );
             finally
                    FreeAndNil( FDataSet );
             end;
        end;
   end;

   function ExisteEmpleadoDestino: Boolean;
   var
      FExiste: TZetaCursor;
   begin
        with oZetaProviderDestino do
        begin
             //CV: CreateQuery--ok
             //Variable local
             FExiste := CreateQuery( Format( GetSQLScript( Q_TRANSFERENCIA_EXISTE_EMPLEADO ), [ iNewEmpleado ] ) );
             try
                with FExiste do
                begin
                     Active := True;
                     Result := not Eof;
                     Active := False;
                end;
             finally
                    FreeAndNil( FExiste );
             end;
        end;
   end;

   function GetMaxFolioExp:Integer;
   const
     Q_GET_MAX_FOLIO = 'select MAX(EX_CODIGO) MAXIMO from EXPEDIEN';
   var
       FDataSet : TZetaCursor;
   begin
        FDataset := oZetaProviderDestino.CreateQuery( Q_GET_MAX_FOLIO ) ;
        if ( FDataSet <> nil )then
        begin
             try
                FDataSet.Open;
                Result := FDataSet.FieldByName('MAXIMO').AsInteger + 1;
                FDataSet.Close;
             finally
                    FreeAndNil(FDataSet);
             end;
        end
        else
        begin
             Result := 1; //Se inicializa el orden en la tabla
        end;

   end;

   function EsTablaMedicos: Boolean;
   var
      c: Integer;
   begin
        Result:= FALSE;
        for c:= 1 to K_QTY_TABLAS_MEDICO do
         if ( A_TABLAS_MEDICO[c] = sTabla ) then
         begin
              Result:= TRUE;
              Break;
         end;
   end;


   procedure TraspasaTabla;
   begin
        with FOrigen do
        begin
             First;
             while not Eof do
             begin
                  oZetaProviderDestino.AsignaParamsDataSet( FOrigen, FDestino, K_EXCLUIDOS );
                  if ( sTabla = 'ACUMULA' ) then
                     iConcepto := FieldByName( 'CO_NUMERO' ).AsInteger
                  else
                      if ( sTabla = 'ACUMULA_RS' ) then
                      begin
                           iConcepto := FieldByName( 'CO_NUMERO' ).AsInteger;
                           sRS_CODIGO := FieldByName( 'RS_CODIGO' ).AsString;
                      end
                  else
                      if ( sTabla = 'AHORRO' ) then
                      begin
                           with oZetaProviderDestino do
                           begin
                                ParamAsFloat( FDestino, 'AH_SALDO_I', FieldByName( 'AH_SALDO_I' ).AsFloat + FieldByName( 'AH_TOTAL' ).AsFloat );
                                ParamAsFloat( FDestino, 'AH_TOTAL', 0 );
                                ParamAsInteger( FDestino, 'AH_NUMERO', 0 );
                           end;
                      end
                      else
                          if ( sTabla = 'PRESTAMO' ) then
                          begin
                               with oZetaProviderDestino do
                               begin
                                    ParamAsFloat( FDestino, 'PR_SALDO_I', FieldByName( 'PR_SALDO_I' ).AsFloat + FieldByName( 'PR_TOTAL' ).AsFloat );
                                    ParamAsFloat( FDestino, 'PR_TOTAL', 0 );
                                    ParamAsInteger( FDestino, 'PR_NUMERO', 0 );
                               end;
                          end
                          else
                              if ( ( sTabla = 'EXPEDIEN' ) or ( EsTablaMedicos ) ) then
                              begin
                                   with oZetaProviderDestino do
                                   begin
                                        ParamAsInteger( FDestino, 'EX_CODIGO', iMaxFolioExp );
                                   end;
                              end;
                  with oZetaProviderDestino do
                  begin
                       if lCambiaNumero then
                          ParamAsInteger( FDestino, 'CB_CODIGO', iNewEmpleado );
                       Ejecuta( FDestino );
                  end;
                  Next;
             end;
        end;
   end;


begin
     InitLog(Empresa,'Transferencia');
     oZetaProviderDestino := TdmZetaServerProvider.Create( Self );
     try
        with oZetaProviderDestino do
        begin
             EmpresaDestino[ P_PASSWORD ]:= ZetaServerTools.Encrypt( EmpresaDestino[ P_PASSWORD ] );  // Se Debe Encriptar por que as� lo lee dZetaServerProvider
             EmpresaActiva := EmpresaDestino;
        end;
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList( Parametros );
             with ParamList do
             begin
                  iEmpleado := ParamByName( 'EmpleadoSource' ).AsInteger;
                  iNewEmpleado := ParamByName( 'EmpleadoTarget' ).AsInteger;
                  lCambiaNumero := ( iEmpleado <> iNewEmpleado );
                  sKardexTipo := ParamByName( 'KardexTipo' ).AsString;{OP: 27/05/08}
                  sKardexDescrip := ParamByName( 'KardexDescrip' ).AsString;{OP: 27/05/08}
                  iKardexNivel := ParamByName( 'KardexNivel' ).AsInteger; {OP: 27/05/08}
                  sEmpresaCodigo := ParamByName( 'EmpresaCodigo' ).AsString;{OP: 28/05/08}
                  sEmpresaDescrip := ParamByName( 'EmpresaDescrip' ).AsString;{OP: 28/05/08}
                  dEmpleadoFecBaj := ParamByName( 'EmpleadoFecBaj' ).AsDateTime;{OP: 28/05/08}
                  lEmpleadoActivo := ParamByName( 'EmpleadoActivo' ).AsBoolean;{OP: 16/06/08}
                  {$ifndef DOS_CAPAS}
                  iNumeroBiometrico := ParamByName( 'NumeroBiometrico' ).AsInteger;
                  {$endif}
             end;
        end;
        TransferenciaParametros( oZetaProviderDestino.EmpresaActiva[ P_DATABASE ] );
        iConcepto := 0;
        iMaxFolioExp:= 0;
        sRS_CODIGO := VACIO;
        { Siempre se Procesar� un solo Empleado }
        if oZetaProvider.OpenProcess( prRHTranferencia, 1 + High( A_TABLAS_TRANSFERENCIA ), FListaParametros ) then
        begin
             if EsDestinoPruebas then
                oZetaProvider.Log.Error( iEmpleado, 'Empresa Destino es de Prueba', 'La Empresa Destino es de Prueba, No es posible Agregar Empleados, Es necesario revisar esto en el Configurador TRESSCFG'  )
             else
             if ExisteEmpleadoDestino then
                oZetaProvider.Log.Error( iEmpleado, 'Empleado Ya Existe', Format( 'N�mero De Empleado Nuevo ( %d ) Ya Existe en Empresa Destino', [ iNewEmpleado ] ) )
             else
             begin
                  iActividad := K_TRANSFERIR;
                  try
                     { Transferir Tablas a EmpresaDestino }
                     oZetaProviderDestino.EmpiezaTransaccion;
                     i := 1;
                     //Nada mas nos interesa que tome el folio del expediente una vez.
                     iMaxFolioExp := GetMaxFolioExp;
                     while ( i <= High( A_TABLAS_TRANSFERENCIA ) ) and oZetaProvider.CanContinue( iEmpleado ) do
                     begin
                          sTabla := A_TABLAS_TRANSFERENCIA[ i ];
                          try
                             with oZetaProvider do
                             begin
                                  { AP(09/01/2008): Se cambi� para poder transferir tambi�n el expediente m�dico del empleado
                                  FOrigen := CreateQuery( Format( GetSQLScript( Q_TRANSFERENCIA_LEE ), [ sTabla, iEmpleado ] ) );}
                                  FOrigen := CreateQuery( Format( GetSQLScript( aQryTransfiere[ EsTablaMedicos ] ), [ sTabla, iEmpleado ] ) );
                                  with FOrigen do
                                  begin
                                       Active := True;
                                  end;
                             end;
                             try
                                with oZetaProviderDestino do
                                begin
                                     FDestino := CreateQuery( GetInsertScript( sTabla, K_EXCLUIDOS, FOrigen ) );
                                end;
                                TraspasaTabla;
                             finally
                                    FreeAndNil( FDestino );
                             end;
                          finally
                                  with FOrigen do
                                  begin
                                       Active := False;
                                  end;
                                 FreeAndNil( FOrigen );
                          end;
                          Inc( i );
                     end;
                     { Borrar Tablas de EmpresaOrigen }
                     if oZetaProvider.CanContinue( iEmpleado ) then  //Debe validarse antes de Borrar, ya que se iniciar� una Transacci�n por todas las Tablas
                     begin
                          { US #5617 Adecuar transferencia individual de empleados para respaldar a la nueva tabla EMP_TRANSF }
                          { Inicio }
                          // Respaldar empleado en tabla EMP_TRANSF
                          iActividad := K_EMP_TRANSF;

                          // Se inicia una transacci�n que abarca insertar el empleado en EMP_TRANSF y el proceso de borrar el empleado de A_TABLAS_TRANSFERENCIA
                          oZetaProvider.EmpiezaTransaccion;
                          oZetaProvider.ExecSQL( oZetaProvider.EmpresaActiva, Format( GetSQLScript (Q_TRANSFERENCIA_RESPALDA), [ iEmpleado ] ) );
                          { Fin }
                          { US #5617 Adecuar transferencia individual de empleados para respaldar a la nueva tabla EMP_TRANSF }

                          iActividad := K_BORRAR;
                          // oZetaProvider.EmpiezaTransaccion; // Se inicia transacci�n antes de insertar el empleado en EMP_TRANSF
                          i := High( A_TABLAS_TRANSFERENCIA );
                          while ( i > 0 ) do                //No se agregar� el CanContinue, ya que este inicia una Transaccion
                          begin
                               sTabla := A_TABLAS_TRANSFERENCIA[ i ];
                               { Las tablas hijas de m�dicos tienen un DELETE en cascada, sin embargo al
                                 igual que con las otras tablas de tress ejecutamos el query por seguridad
                               oZetaProvider.ExecSQL( oZetaProvider.EmpresaActiva, Format( GetSQLScript( Q_TRANSFERENCIA_BORRA ), [ sTabla, iEmpleado ] ) );}
                               oZetaProvider.ExecSQL( oZetaProvider.EmpresaActiva, Format( GetSQLScript( aQryTransfiereBorra[ EsTablaMedicos ]  ), [ sTabla, iEmpleado ] ) );
                               Dec( i );
                          end;
                          with oZetaProviderDestino do
                          begin
                               if StrLleno( sKardexTipo ) then
                                  GrabaMovimientoKardex;{OP: 27/05/08}
                               TerminaTransaccion( True );
                          end;
                          with oZetaProvider do
                          begin
                               TerminaTransaccion( True );
                               Log.Evento( clbNinguno,
                                           iEmpleado,
                                           Now,
                                           Format( 'Transferido Como # %d', [ iNewEmpleado ] ),
                                           Format( 'Transferido a %s' + CR_LF + 'Como Empleado # %d', [ EmpresaDestino[ P_DATABASE ], iNewEmpleado ] ) );
                          end;

                          try
                             TransfiereUsuario( oZetaProvider.EmpresaActiva,oZetaProviderDestino.EmpresaActiva,iEmpleado, iNewEmpleado );
                          except
                                on Error: Exception do
                                begin
                                     oZetaProvider.Log.Excepcion( iNewEmpleado, 'Error al Transferir Usuario del Empleado', Error );
                                end;
                          end;

                          {$ifndef DOS_CAPAS}
                          // Actualiza EMP_BIO del empleado
                          if( iNumeroBiometrico > 0 )then
                              Self.ActualizaEmpresaEmpId( oZetaProviderDestino.CodigoEmpresaActiva, iNumeroBiometrico, iNewEmpleado );
                          {$endif}
                     end
                     else
                         //oZetaProviderDestino.TerminaTransaccion( False );
                         oZetaProviderDestino.RollBackTransaccion;
                  except
                        on Error: Exception do
                        begin
                             if ( iActividad > K_NINGUNA ) then
                                //oZetaProviderDestino.TerminaTransaccion( False );
                                oZetaProviderDestino.RollBackTransaccion;
                             if ( iActividad = K_BORRAR )  or ( iActividad = K_EMP_TRANSF )then
                                //oZetaProvider.TerminaTransaccion( False );
                                oZetaProvider.RollBackTransaccion;

                             if ( iActividad = K_EMP_TRANSF ) then
                             begin
                               sMsg := 'Error Al Transferir Empleado';
                             end
                             else
                             begin
                               sMsg := 'Error Al Transferir ' + sTabla;
                               if ( sTabla = 'ACUMULA' ) then
                                  sMsg := sMsg + ', Concepto: ' + IntToStr( iConcepto );
                               if ( sTabla = 'ACUMULA_RS' ) then
                                  sMsg := sMsg + ', Concepto: ' + IntToStr( iConcepto ) + ', Raz�n Social: ' + sRS_CODIGO;
                             end;

                             oZetaProvider.Log.Excepcion( iEmpleado, sMsg, Error );
                        end;
                  end;
             end;
             Result := oZetaProvider.CloseProcess;
        end;
     finally
            oZetaProviderDestino.Free;
     end;
     EndLog;SetComplete;
end;

{.$IFDEF RETROACTIVO}
procedure TdmServerCalcNomina.InitRetroactivo;
begin
     if NOT Assigned( FRetroactivo ) then
     begin
          FRetroactivo := TRetroactivo.Create( oZetaCreator );
     end;
     oZetaProvider.EvaluacionTipo := teRetroactiva
end;

procedure TdmServerCalcNomina.ClearRetroactivo;
begin
     oZetaProvider.EvaluacionTipo := teNormal;
     FreeAndNil( FRetroactivo );
end;

procedure TdmServerCalcNomina.CalculaRetroactivoParametros;
begin
     with oZetaProvider do
     begin
          FListaParametros := VACIO;
          with DatosPeriodo do
          begin
               FListaParametros := 'Peri�do: ' + ZetacommonTools.ShowNomina( Year, Ord( Tipo ), Numero ) +
                                   K_PIPE + 'Status Inicial: ' + ObtieneElemento( lfStatusPeriodo, Ord( Status ) );
          end;
          with ParamList do
          begin
               FListaParametros := FListaParametros  +
                                   K_PIPE + 'Cambio de Salario: ' + FechaCorta( ParamByName('CambioSalario').AsDate ) +
                                   K_PIPE + 'N�minas Calculadas: ' +
                                   K_PIPE + 'A�o: ' + ParamByName('NominaYear').AsString +
                                   ' Desde: ' + ParamByName('NominaDesde').AsString +
                                   ' Hasta: ' + ParamByName('NominaHasta').AsString +
                                   K_PIPE + 'Conceptos: ' + ParamByName('ListaConceptos').AsString +
                                   K_PIPE + 'Pagar en: ' + ParamByName('ConceptoRetroactivo').AsString +
                                   K_PIPE + 'Si Existen: ' + ObtieneElemento( lfOperacionMontos, Ord( ParamByName('ExcepcionOp' ).AsInteger ) );
               if ParamByName('IncluyeAnio').AsBoolean then
                  FListaParametros := FListaParametros  +
                                   K_PIPE + 'A�o Adicional: ' + ParamByName('NominaYearAd').AsString +
                                   ' Desde: ' + ParamByName('NominaDesdeAd').AsString +
                                   ' Hasta: ' + ParamByName('NominaHastaAd').AsString;


          end;
     end;
end;

function TdmServerCalcNomina.CalculaRetroactivoDataset( Empresa: oleVariant; DataSet : TDataSet ): Olevariant;
const
     K_YEAR_AD_NO = 0;
     K_YEAR_AD_SI = 1;
var
   iNomina, iDesde, iHasta,iDesdeAd, iHastaAd, i, iIncluyeAnio : integer;
begin
     iDesdeAd := 0;
     iHastaAd := 0;
     CalculaRetroactivoParametros;
     with oZetaProvider do
     begin
          InitArregloTPeriodo;//acl
          with ParamList do
          begin
               iIncluyeAnio := BoolToInt( ParamByName('IncluyeAnio').AsBoolean );
               iDesde := ParamByName('NominaDesde').AsInteger;
               iHasta := ParamByName('NominaHasta').AsInteger;
               if iIncluyeAnio = K_YEAR_AD_SI then
               begin
                    iDesdeAd := ParamByName('NominaDesdeAd').AsInteger;
                    iHastaAd := ParamByName('NominaHastaAd').AsInteger;
               end;

          end;

          if OpenProcess( prNOCalculaRetroactivos, ( cdsLista.RecordCount * ( (iHasta-iDesde)+1 ) ), FListaParametros ) then
          begin
               for i := 0 to iIncluyeAnio do
               begin
                    if ( i = K_YEAR_AD_SI ) then
                    begin
                         iDesde := iDesdeAd;
                         iHasta := iHastaAd;
                         InitCalcNomina;
                         with CalcNomina do
                         begin
                              Retroactivo := FRetroactivo;
                              Retroactivo.YearAd := K_YEAR_AD_SI;
                         end;
                    end;

                    for iNomina := iDesde to iHasta do
                    begin
                         InitCalcNomina;
                         try
                            with CalcNomina do
                            begin
                                 Retroactivo := FRetroactivo;
                                 Retroactivo.NominaActiva := iNomina;
                                 InitParametros(Empresa, ParamList.VarValues);
                                 Result := CalculoRetroactivoDataset ( cdsLista );
                            end;
                         finally
                                ClearCalcNomina;
                         end;
                    end;
                    FRetroactivo.GrabaExcepciones;
               end;
          end;
          Result := CloseProcess;
     end;
end;
{.$endif}

function TdmServerCalcNomina.CalculaRetroactivo(Empresa, Parametros: OleVariant): OleVariant;
{.$IFDEF RETROACTIVO}
{.$endif}
begin
     InitLog(Empresa,'CalculaRetroactivo');
     {.$IFDEF RETROACTIVO}
     ModuloAutorizado( okNomina );

     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          GetDatosPeriodo;

          InitCalcNomina;
          with CalcNomina do
          begin
               InitParametros(Empresa, ParamList.VarValues);
               cdsLista.Lista := CalculoRetroactivoBuildDataset;
          end;
          InitRetroactivo;
          try
             Result := CalculaRetroactivoDataset(Empresa, cdsLista);
          finally
                 ClearRetroactivo;
          end;
     end;
     {.$endif}
     EndLog;SetComplete;

end;

function TdmServerCalcNomina.CalculaRetroactivoGetLista(Empresa, Parametros: OleVariant): OleVariant;
{.$IFDEF RETROACTIVO}
{.$endif}
begin
     InitLog(Empresa,'CalculaRetroactivoGetLista');
     {.$IFDEF RETROACTIVO}
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          GetDatosPeriodo;
     end;
     InitCalcNomina;
     try
        with CalcNomina do
        begin
             InitParametros(Empresa, oZetaProvider.ParamList.VarValues);
             Result := CalculoRetroactivoBuildDataset;
        end;
     finally
            ClearCalcNomina;
     end;
     {.$endif}
     EndLog;SetComplete;
end;

function TdmServerCalcNomina.CalculaRetroactivoLista(Empresa, Lista, Parametros: OleVariant): OleVariant;
{.$IFDEF RETROACTIVO}
{.$endif}
begin
     InitLog(Empresa,'CalculaRetroactivoLista');
     {.$IFDEF RETROACTIVO}
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          GetDatosPeriodo;
          InitCalcNomina;
          with CalcNomina do
          begin
               InitParametros(Empresa, ParamList.VarValues);
               cdsLista.Lista := Lista;
          end;
          InitRetroactivo;
          try
             Result := CalculaRetroactivoDataset( Empresa, cdsLista );
          finally
                 ClearRetroactivo;
          end;
     end;
     {.$endif}
     EndLog;SetComplete;
end;


function TdmServerCalcNomina.RecalcularTarjetasSimple(Empresa,Parametros: OleVariant): WideString;
var
   FRevisaHorario: TZetaCursor;
   FStatusHorario: TStatusHorario;
   iEmpleado: TNumEmp;
   iUsuario: Integer;
   dFecha: TDate;
   sTurno:string;
   lHayCambio: Boolean;
   eTipoDia: eStatusAusencia;
   sHorario: TCodigo;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          with ParamList do
          begin
               dFecha := ParamByName( 'Fecha' ).AsDate;
               iEmpleado := ParamByName( 'Empleado' ).AsInteger;
               sHorario := ParamByName( 'Horario' ).AsString;
               eTipoDia := eStatusAusencia( ParamByName( 'TipoDia' ).AsInteger);
               sTurno := ParamByName( 'Turno' ).AsString;
               iUsuario := 0;//Para Recalcular la tarjeta siempre
          end;
          //InitGlobales;
          InitRitmos;
          InitTarjeta;
          try //Finally
             try  //Exception
                // Antes de empezar
                with FTarjeta do
                begin
                     GetTurnoHorarioBegin( dFecha, dFecha );
                     CalculaTarjetaBegin;
                end;
                FRevisaHorario := CreateQuery( GetSQLScript( Q_RECALCULAR_TARJETAS_REVISA_HORARIO ) );
                ParamAsBoolean( FRevisaHorario, 'EsManual', False );
                // Ejecuci�n

                EmpiezaTransaccion;
                try
                   with FTarjeta do
                   begin
                        FStatusHorario := Ritmos.GetStatusHorario( sTurno, dFecha );
                   end;
                   with FStatusHorario do
                   begin
                        lHayCambio := ( sHorario <> Horario ) or ( eTipoDia <> Status );
                   end;
                   if ( lHayCambio ) then
                   begin
                        ParamAsDate( FRevisaHorario, 'FECHA', dFecha );
                        ParamAsInteger( FRevisaHorario, 'EMPLEADO', iEmpleado );
                        with FStatusHorario do
                        begin
                             ParamAsChar( FRevisaHorario, 'Horario', Horario, K_ANCHO_HORARIO );
                             ParamAsInteger( FRevisaHorario, 'Status', Ord( Status ) );
                             sHorario := Horario;
                             eTipoDia := Status;
                        end;
                        Ejecuta( FRevisaHorario );
                   end;

                   FTarjeta.CalculaTarjeta( iEmpleado, dFecha, sHorario, eTipoDia, iUsuario );
                   TerminaTransaccion( True );
                except
                      on Error: Exception do
                      begin
                           //TerminaTransaccion( False );
                           RollBackTransaccion;
                           Log.Excepcion( iEmpleado, 'Error Al Recalcular Tarjeta ' + FechaCorta( dFecha ), Error );
                      end;
                end;

                with FTarjeta do
                begin
                     CalculaTarjetaEnd;
                     GetTurnoHorarioEnd;
                end;

             except
                   on Error: Exception do
                   begin
                        Log.Excepcion( 0, 'Error Al Recalcular Tarjetas', Error );
                   end;
             end;
          finally
                 ClearTarjeta;
                 FreeAndNil( FRevisaHorario );
          end
     end;
     Result := 'Termin� el Rec�lculo de Tarjeta Simple';
     SetComplete;
end;

function TdmServerCalcNomina.ProcesarTarjetasSimple(Empresa, Parametros: OleVariant): WideString;
var
   sEmpresa, sHora, sReloj, sCredencial, sMensaje, sSQL: String;
    dFecha, dLimite: TDate;
   iEmpleado: TNumEmp;
   lOk, lPuedeModificar: Boolean;
   FPoll: TZetaCursor;


{$ifdef MODULO_PRESTADO_ELECTROCOMPONENTES_TRESSENLINEA}
   function TieneModuloPrestado( eModulo : TModulos) : boolean;
   begin
        if ( eModulo = okTressEnLinea ) and  (oAutoServer.NumeroSerie = 1798)  then
           Result := (Date < CodificaFecha(2012,12,10) )
        else
            Result := FALSE;
   end;
{$endif}

{$ifdef MODULO_PRESTADO_EATON_TRESSENLINEA}
   function TieneModuloPrestado( eModulo : TModulos) : boolean;
   begin
        if ( eModulo = okTressEnLinea ) and  (oAutoServer.NumeroSerie = 1981)  then
           Result := (Date < CodificaFecha(2013,01,12) )
        else
            Result := FALSE;
   end;
{$endif}

{$ifdef MODULO_PRESTADO_FRISAFORJADOS_TRESSENLINEA}
   function TieneModuloPrestado( eModulo : TModulos) : boolean;
   begin
        if ( eModulo = okTressEnLinea ) and  (oAutoServer.NumeroSerie = 1551)  then
           Result := (Date < CodificaFecha(2013,01,11) )
        else
            Result := FALSE;
   end;
{$endif}

{$ifdef MODULO_PRESTADO_SEBN_TRESSENLINEA}
   function TieneModuloPrestado( eModulo : TModulos) : boolean;
   begin
        if ( eModulo = okTressEnLinea ) and  (oAutoServer.NumeroSerie = 1970)  then
           Result := (Date < CodificaFecha(2013,01,30) )
        else
            Result := FALSE;
   end;
{$endif}

begin
{$ifdef MODULO_PRESTADO_ELECTROCOMPONENTES_TRESSENLINEA}
     if not TieneModuloPrestado( okTressEnLinea ) then
{$endif}
{$ifdef MODULO_PRESTADO_EATON_TRESSENLINEA}
     if not TieneModuloPrestado( okTressEnLinea ) then
{$endif}
{$ifdef MODULO_PRESTADO_FRISAFORJADOS_TRESSENLINEA}
     if not TieneModuloPrestado( okTressEnLinea ) then
{$endif}
{$ifdef MODULO_PRESTADO_SEBN_TRESSENLINEA}
     if not TieneModuloPrestado( okTressEnLinea ) then
{$endif}
          ModuloAutorizado( okTressEnLinea );

     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
          dLimite := GetGlobalDate( K_GLOBAL_FECHA_LIMITE );
          lPuedeModificar := GetGlobalBooleano(K_GLOBAL_BLOQUEO_X_STATUS);

          sEmpresa := GetDigitoEmpresa( Empresa[P_CODIGO] );
          AsignaParamList( Parametros );
          with ParamList do
          begin
               dFecha := ParamByName( 'Fecha' ).AsDate;
               iEmpleado := ParamByName( 'Empleado' ).AsInteger;
               sHora := ParamByName( 'Hora' ).AsString;
               sCredencial := ParamByName( 'Credencial' ).AsString;
               sReloj := ParamByName( 'Reloj' ).AsString;
          end;

          // Crear la estructura en el cdsLista
          sSQL := Format( GetSQLScript( Q_POLL_PENDIENTES ), [ sEmpresa ] );
          cdsLista.Lista := OpenSQL( Comparte, sSQL, False );

          InitRitmos;
          InitTarjeta;
          try
             try
                FTarjeta.DoPollBegin( dFecha, dFecha );

                if NOT PuedeCambiarTarjeta( dFecha, dFecha, dLimite, lPuedeModificar, sMensaje ) then
                begin
                     lOk := FALSE;
                     oZetaProvider.EscribeBitacora( tbAdvertencia, clbTarjeta, iEmpleado, dFecha,
                                                    'Checada Anterior/Igual a Fecha L�mite: ' + FechaCorta(dLimite) ,
                                                    Format( 'Empleado: %d' + CR_LF +
                                                            'D�a: %s'  + CR_LF +
                                                            'Hora: %s ', [ iEmpleado, FechaCorta(dFecha), FormatMaskText( '00:00;0', sHora ) ] ) + CR_LF +
                                                            sMensaje );
                end
                else
                    lOK := FTarjeta.DoPoll( iEmpleado,
                                           dFecha,
                                           sReloj,
                                           sCredencial,
                                           sHora );
                if NOT lOk then
                begin
                     with cdsLista do
                     begin
                          Append;
                          try
                             FieldByName( 'PO_LINX' ).AsString := sReloj;
                             FieldByName( 'PO_EMPRESA' ).AsString := sEmpresa;
                             FieldByName( 'PO_NUMERO' ).AsInteger := iEmpleado;
                             FieldByName( 'PO_LETRA' ).AsString := sCredencial;
                             FieldByName( 'PO_FECHA' ).AsDateTime := dFecha;
                             FieldByName( 'PO_HORA' ).AsString := sHora;
                             Post;
                          except
                                on Error: Exception do
                                begin
                                     oZetaProvider.EscribeBitacora( tbErrorGrave, clbTarjeta, iEmpleado, dFecha, 'Error Al Guardar Errores Procesar Tarjeta Simple', Error.Message );
                                end;
                          end;
                     end;
                end;
                FTarjeta.DoPollEnd;
             except
                   on Error: Exception do
                   begin
                        oZetaProvider.EscribeBitacora( tbErrorGrave, clbTarjeta, iEmpleado, dFecha, 'Error Al Procesar Tarjeta Simple', Error.Message );
                   end;
             end;
          finally
                 ClearTarjeta;
          end;

          { Agregar los que tuvieron problemas }
          EmpresaActiva := Comparte;
          with cdsLista do
          begin
               if not IsEmpty then
               begin
                    FPoll := CreateQuery( GetSQLScript( Q_POLL_ESCRIBE ) );
                    try
                       First;
                       while not Eof do
                       begin
                            ParamAsVarChar( FPoll, 'PO_LINX', FieldByName( 'PO_LINX' ).AsString, K_ANCHO_LINX );
                            ParamAsChar( FPoll, 'PO_EMPRESA', FieldByName( 'PO_EMPRESA' ).AsString, K_ANCHO_EMPRESA );
                            ParamAsInteger( FPoll, 'PO_NUMERO', FieldByName( 'PO_NUMERO' ).AsInteger );
                            ParamAsChar( FPoll, 'PO_LETRA', FieldByName( 'PO_LETRA' ).AsString, K_ANCHO_LETRA );
                            ParamAsDate( FPoll, 'PO_FECHA', FieldByName( 'PO_FECHA' ).AsDateTime );
                            ParamAsChar( FPoll, 'PO_HORA', FieldByName( 'PO_HORA' ).AsString, K_ANCHO_HORA );
                            EmpiezaTransaccion;
                            try
                               Ejecuta( FPoll );
                               TerminaTransaccion( True );
                            except
                                  on Error: Exception do
                                  begin
                                       TerminaTransaccion( False );
                                       EmpresaActiva := Empresa;
                                       oZetaProvider.EscribeBitacora( tbErrorGrave, clbTarjeta, iEmpleado, dFecha, 'Error Al Guardar Registros Pendientes de Push', Error.Message );
                                       EmpresaActiva := Comparte;
                                  end;
                            end;
                            Next;
                       end;
                    finally
                           FreeAndNil( FPoll );
                    end;
               end;
               EmptyDataset;
          end;
     end;
     if not lOk then
        raise Exception.Create( 'Error al guardar checada del empleado.' )
     else
         Result := 'Termin� el Proceso de Tarjeta Simple';
     SetComplete;
end;

{$ifndef DOS_CAPAS}
procedure TdmServerCalcNomina.ActualizaEmpresaEmpId( sEmpresa: String; iNumeroBiometrico, iNewEmpleado: Integer );
var
   oZetaProviderLocal: TdmZetaServerProvider;
begin
     if( iNumeroBiometrico > 0 )then
     begin
          oZetaProviderLocal := TdmZetaServerProvider.Create( Self );
          try
             with oZetaProviderLocal do
             begin
                  try
                     // Consulta informaci�n del cliente
                     EmpresaActiva := Comparte;
                     EmpiezaTransaccion;

                     ExecSQL( EmpresaActiva, Format( GetSQLScript( Q_ACTUALIZA_CM_CODIGO_EMP_BIO ), [ sEmpresa, iNewEmpleado, iNumeroBiometrico ] ) );

                     TerminaTransaccion( true );
                  except
                        on Error: Exception do
                           TerminaTransaccion( false );
                  end;
             end;
          finally
                 DZetaServerProvider.FreeZetaProvider( oZetaProviderLocal );
          end;
     end;
end;
{$endif}

{$ifndef DOS_CAPAS}
{$ifndef CALCNOMINA_COMO_OBJETO}
initialization
  TComponentFactory.Create( ComServer, TdmServerCalcNomina, Class_dmServerCalcNomina,
                            ciMultiInstance,{$ifdef INTERBASE}tmApartment{$endif}
                            {$ifdef MSSQL}ZetaServerTools.GetThreadingModel{$endif});
{$endif}
{$endif}
end.