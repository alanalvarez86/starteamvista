unit DServerSistema;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi 5/ XE3                              ::
  :: Unidad:      DServerSistema.pas                         ::
  :: Descripci�n: Programa principal de Sistema.dll          ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

{$DEFINE SISNOM_CLAVES}
{$DEFINE DEBUGSENTINEL}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComServ, ComObj, VCLCom, StdVcl, DataBkr, DBClient, MtsRdm, Mtx, Db,
     Variants,
{$ifndef DOS_CAPAS}
     Sistema_TLB,
{$endif}
     DZetaServerProvider,
     ZetaCommonClasses,
     ZetaServerDataSet,
     ZCreator,
     ZetaSQLBroker,
     ZetaCommonLists,
     DSeguridad,
     FAutoServer,
     FAutoClasses,
     Registry,
     ADODB,
     System.MaskUtils;

type
  eTipoSistema = ( eArbol,
                   eEmpresas,
                   eGruposUsuarios,
                   eUsuarios,
                   ePoll,
                   eImpresoras,
                   eAccesos,
                   eUserSuper,
                   eNivel0,
                   eSuscrip,
                   eUsuarioAreas,
                   eGruposAdic,
                   eCamposAdic,
                   eRoles,
                   eUserRol,
                   eAccesosAdicionales,
                   eSistLstDispositivos,
                   eDisxCom,
                   eAccArbol,
                   eUsuarioCCosto,
                   eBaseDatos,
                   eSistListaGrupos, // SYNERGY
                   eTermPorGrupo, // SYNERGY
                   eMensajes, // SYNERGY
                   eMensajesTerm, // SYNERGY
                   eTerminales, // SYNERGY
                   eConfigCafeteria,
                   eCalendCafeteria,
                   {$ifdef SISNOM_CLAVES}
                   eUsuariosSeguridad,
                   eSistTareaCalendario,
                   eSistTareaUsuario,
                   eSistTareaRoles

					{$endif}
                    );

  TdmServerSistema = class(TMtsDataModule {$ifndef DOS_CAPAS}, IdmServerSistema {$endif} )
    cdsLista: TServerDataSet;
    Connector: TADOConnection;
    procedure dmServerSistemaCreate(Sender: TObject);
    procedure dmServerSistemaDestroy(Sender: TObject);
  private
    { Private declarations }
    oZetaProviderComparte: TdmZetaServerProvider;
    oZetaProviderDiccionario: TdmZetaServerProvider;
    oZetaProvider: TdmZetaServerProvider;
    FListaParametros: string;
    FListaFormulas: string;
    FTipoSistema: eTipoSistema;
    FUsuario: Integer;
    FUsuarioData : TUsuarioData;
    {$ifndef DOS_CAPAS}
    FSinBiometrico: Boolean;
    {$endif}
    {$ifndef DOS_CAPAS}
    oZetaCreator: TZetaCreator;
    oZetaSQLBroker: TSQLBroker;
    oAutoServer: TAutoServer;
    {$endif}
    FEmpleado : TZetaCursor;
    FAccionAlEnrolar : eAccionRegistros;
    FModoGrupos:TUpdateKind;
    FCodigoAnterior:Integer;
    FCodigoNuevo:Integer;
    FCM_CODIGOAnt:string;
    FCM_CODIGONuevo:string;
    FDataSetEmpresa:TZetaCursor;

    function GetUsuarioData : TUsuarioData;
    procedure GrabaCambiosBitacoraDiccion(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
      UpdateKind: TUpdateKind);
    property UsuarioData : TUsuarioData read GetUsuarioData;
    function ActivaDesActivaUsuarios(const Usuario, Grupo:Integer;const lActiva: Boolean): OleVariant;
    function DecodificaClaves( Datos: OleVariant; const sCampo: String ): OleVariant;
    function GetScript( const iScript: Integer ): String;
    function LeerClaves(const iNumero: Integer): String;
    function BuildEmpresa(DataSet: TZetaCursor): OleVariant;
    procedure Codifica( const sCampo: String; DeltaDS: TzProviderClientDataSet );
    procedure CodificaClaveEmpresa(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure AfterUpdateGrupos(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    procedure AfterUpdateCalendarioReportes(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    procedure SetTablaInfo( const eTabla: eTipoSistema );
    procedure BorrarBitacoraParametros;
    procedure AfterUpdateSuscripciones( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind );
    {$ifndef DOS_CAPAS}
    procedure ServerActivate(Sender: TObject);
    procedure ServerDeactivate(Sender: TObject);
    {$endif}
    procedure SetFiltroGrupos( const iGrupo: integer );
    procedure GrabaCambiosBitacora( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind );
    function GetSuscripciones(const sCampo: string; const iCodigo: integer): Olevariant;
    function GetSuscripcionesCalendarioUsuario(const sCampo: string; const iCodigo: integer): Olevariant;
    procedure EnrolamientoMasivoBuildDataSet;
    function EnrolamientoMasivoDataset(Dataset: TDataset;Proceso: Procesos): OleVariant;
    procedure EnrolamientoMasivoParametros;
    function GetSQLBroker: TSQLBroker;
    procedure ClearBroker;
    procedure InitBroker;
    procedure InitCreator;
    {$ifndef DOS_CAPAS}
    procedure DescargarUsuarioRoles(const iUsuario: Integer;const sRoles: string);
    {$endif}
    procedure ImportarEnrolamientoMasivoValidaASCII(DataSet: TDataset;var nProblemas: Integer; var sErrorMsg: String);
    procedure ImportarEnrolamientoMasivoParametros;
    function  GetTipoLogin:Integer;
    {$ifdef TRESS}
    function ModuloAutorizado( const eModulo : TModulos ):Boolean;
    {$endif}
    procedure ActualizarEmpresasGrupo(const iUsuario:Integer);
    procedure ActualizaCodigoEmpresa(const CodigoAnterior,NuevoCodigo:string);
    procedure ActualizaDerechos(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    {$ifndef DOS_CAPAS}
    procedure AsignaNumBiometricosBuildDataset; // SYNERGY
    function AsignaNumBiometricosDataset( Dataset: TDataset ): OleVariant; // SYNERGY
    function AsignaGrupoTerminalesDataset( Dataset: TDataset ): OleVariant; // SYNERGY
    procedure SetAfterUpdateGruposDisp( Sender: TObject; SourceDS: TDataSet; DeltaDS: TZProviderClientDataSet; UpdateKind: TUpdateKind );
    procedure SetAfterUpdateMensajesPorTerminal( Sender: TObject; SourceDS: TDataSet; DeltaDS: TZProviderClientDataSet; UpdateKind: TUpdateKind );
    procedure SetAfterUpdateTerminales( Sender: TObject; SourceDS: TDataSet; DeltaDS: TZProviderClientDataSet; UpdateKind: TUpdateKind );
    {$endif}
    procedure BeforeUpdateDB(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure InitLog(Empresa:Olevariant; const sPaletita: string);
    procedure EndLog;
    procedure ImportarTablaCSVListaParametros;
    function  ImportarTablaCSVDataset: OleVariant;
    function camposLlaveAsCommaText (sTabla: String): String;
    procedure ExtraerChecadasBitTermParametros;
    function LeeUnaConfiguracion(const Identificador: WideString; var Calendario: OleVariant):OleVariant;
    function AgregaTareaAlCalendario(Empresa: OleVariant; out Datos: OleVariant): OleVariant;
  protected
    { Protected declarations }
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    property SQLBroker: TSQLBroker read GetSQLBroker;
    procedure LeeScriptEstructura(const oZetaProviderBD: TdmZetaServerProvider; const sRutaArchivosConfiguracion: string; TipoBD: Integer);
    procedure LeeScriptEspeciales(const oZetaProviderBD: TdmZetaServerProvider; const sRutaArchivosConfiguracion: string);
    procedure procesaScript (oZetaProviderDB:TdmZetaServerProvider; sNombreScript: String);
    procedure LeeXMLSugeridos(const sRutaArchivosConfiguracion, BaseDatos, sBaseDatosBaseUbica: string; ArregloXML: Array of string);
    procedure LeeIniciales( const sRutaArchivosConfiguracion, sBaseDatosBaseUbica: String; const TipoBD: eTipoCompany );
    procedure LeeDiccionario(const sRutaArchivosConfiguracion, BaseDatos, sBaseDatosBaseUbica: string; TipoBD: Integer);
    function GetArrayEmpresa(sCM_DATOS, sCM_USRNAME, sCM_PASSWRD, sCM_NIVEL0, sCM_CODIGO: string): OleVariant;
    procedure procesaXML (cdsXML : TServerDataSet; rutaXML, stabla, sBaseDatosBaseUbica: String);
    procedure procesaTablaDiccionario (tabla, sBaseDatosBaseUbica: String);
    function LeerTablaDesdeBD (const rutaXML, tabla, sBaseDatosBaseUbica: String): OleVariant;
    function GetFieldsFromXML (rutaXML, tabla: String):TStringList;
    function TransformaScriptMSSQL(sScript: string ; const sDatabaseComparte: string ): String;
    function GetConnStr (BaseDatos: String; Server: String = ''; Usuario: String = '';  Password: String = ''): widestring;
    procedure setProviderDiccionario (BaseDatos: String; bTraeUbicacion: Boolean = FALSE);
    function GetNuevoProvider (sUbicacion: WideString; iQuery: Integer; sUsuario: WideString; sPassword: WideString): TdmZetaServerProvider;
    function GetComparte: String;
    procedure Method1; safecall;


{$ifdef DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
{$endif}
    function GetAccesos(iGrupo: Integer; const sCompany: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    //function GetBitacora(Empresa: OleVariant; FechaInicial, FechaFinal: TDateTime; TipoBitacora, Empleado, Usuario: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetEmpresas(Tipo, GrupoActivo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetEmpresasAccesos(Tipo, Grupo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetGrupos(Grupo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetImpresoras: OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetPoll: OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetPollVacio: OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetUsuarios(out Longitud: Integer; Grupo: Integer; out BlockSistema: WordBool): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetUsuarioArbol(iUsuario: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetUsuarioArbolApp(iUsuario, iExe: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetUsuarioSupervisores(Empresa: OleVariant; iUsuario: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaAccesos(oDelta, oParams: OleVariant;out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;
    function GrabaArbol(iUsuario: Integer; oDelta: OleVariant;out ErrorCount: Integer): OleVariant; safecall;
    function GrabaArbolApp(iUsuario, iExe: Integer; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;{$endif}
    function GrabaEmpresas(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaGrupos(oDelta: OleVariant; iUsuario: Integer; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaImpresoras(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaPoll(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaUsuarios(oDelta: OleVariant; iUsuario: Integer; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaUsuariosSupervisores(Empresa, oDelta: OleVariant;out ErrorCount: Integer; iUsuario: Integer; const sTexto: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function ActivaUsuarios(Usuario, Grupo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function SuspendeUsuarios(Usuario, Grupo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function BorrarBitacora(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetNivel0: OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaNivel0(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetUsuarioSuscrip(Empresa: OleVariant; Usuario: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaUsuarioSuscrip(Empresa, oDelta: OleVariant; out ErrorCount: Integer; Usuario: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetGruposAccesos(iGrupo: Integer; const sCompany: WideString; iTipo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetUsuarioAreas(Empresa: OleVariant; Usuario: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaUsuarioAreas(Empresa: OleVariant; Usuario: Integer;oDelta: OleVariant; out ErrorCount: Integer;const sTexto: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetNuevoUsuario: Integer; {$ifndef DOS_CAPAS} safecall;{$endif}
    function ActualizaNumeroTarjeta(out Bitacora: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function GetGruposAdic(Empresa: OleVariant; out oCamposAdic: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaGruposAdic(Empresa, oDelta, oDeltaCampos: OleVariant; out ErrorCount, ErrorCampos: Integer; out CamposResult: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetBitacora(Empresa, Parametros: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function GetReporteSuscrip(Empresa: OleVariant; iReporte: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetRoles: OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function GetDatosEmpleadoUsuario(Empresa, Datos: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaRoles(const Rol: WideString; Delta, Usuarios, Modelos: OleVariant; var ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetUsuariosRol(const Rol: WideString; out Modelos: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetUserRoles(Empresa: OleVariant; iUsuario: Integer): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaUsuarioRoles( iUsuario: Integer; iUsuarioLog: Integer;Delta: OleVariant; var ErrorCount: Integer):OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaEmpleadoUsuario(oEmpresa: OleVariant;iUsuario, iEmpleado: Integer): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function EnrolamientoMasivo(Empresa, Parametros: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function EnrolamientoMasivoGetLista(oEmpresa,oParametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function ImportarEnrolamientoMasivoGetASCII(oEmpresa, Parametros, ListaASCII: OleVariant; out ErrorCount: Integer; LoginAD: WordBool): OleVariant; {$ifndef DOS_CAPAS}  safecall;{$endif}
    function ImportarEnrolamientoMasivo(Empresa, Parametros,Datos: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetClasifiEmpresa(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetDerechosAdicionales(Empresa: OleVariant; Grupo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaAccesosAdicionales(Empresa: OleVariant; Grupo: Integer; Delta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetSistLstDispositivos(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaSistLstDispositivos(Empresa, oDelta: OleVariant;out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetGlobalLstDispositivos(Empresa: OleVariant; var Disponibles, Asignados: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaGlobalLstDispositivos(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetAccArbol(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    procedure ActualizaUsuariosEmpresa(Empresa, Delta: OleVariant); {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetUsuarioCCosto(Empresa: OleVariant; iUsuario: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaUsuarioCCosto(Empresa, oDelta: OleVariant; ErrorCount, iUsuario: Integer; const sTexto, sNombreCosteo: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall;
    function GrabaConfigCafeteria(Empresa, oDelta, oCalend: OleVariant;
      var ErrorCount, ErrorCalendCount: Integer;
      out Valor: OleVariant): OleVariant; safecall; {$endif}
    function GetConfigCafeteria(const Identificador: WideString; out Calendario: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetSistListEstaciones(Filtro: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;
    function GrabaTareaCalendario(Empresa, Parametros: OleVariant;
      out Datos: OleVariant): OleVariant; safecall; {$endif}
    function GetTareaCalendario(Empresa: OleVariant; Grupo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaCalendario(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetTareaUsuario(Empresa: OleVariant; Folio: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetTareaRoles(Empresa: OleVariant; Folio: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaTareaRoles(oDelta: OleVariant; out ErrorCount: Integer; Folio: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaTareaUsuario(oDelta: OleVariant; out ErrorCount: Integer; Folio: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}

    {$ifndef DOS_CAPAS}
    function ConsultaBitacoraBiometrica(Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function DepuraBitacoraBiometrica(Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function ObtenIdBioMaximo: Integer;{$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function ActualizaIdBio(const CodigoEmpresaLocal: WideString; Empleado, Id: Integer; const Grupo: WideString; Invitador: Integer; const Confidencialidad: WideString): WordBool; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function ConsultaListaGrupos(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function GrabaSistListaGrupos(oDelta: OleVariant; UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function ConsultaTermPorGrupo(const Grupo: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function GrabaTermPorGrupo(oDelta: OleVariant; UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    procedure GrabaListaTermPorGrupo(const Terminales: WideString; UsuarioTerminal: Integer; const Grupo, SinTerminales: WideString); {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function AsignaNumBiometricos(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    procedure ReiniciaSincTerminales(const Lista: WideString); {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function AsignaGrupoTerminales(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function EmpleadosBiometricosGetLista(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function EmpleadosSinBiometricosGetLista(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function AsignaGrupoTerminalesLista(Empresa, Lista, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function AsignaNumBiometricosLista(Empresa, Lista, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    procedure ActualizaTerminalesGTI(const Lista: WideString); {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function ConsultaMensaje(const Filtro: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function ConsultaMensajeTerminal(const Filtro: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function ConsultaTerminal(const Filtro: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function GrabaMensaje(oDelta: OleVariant; UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function GrabaMensajeTerminal(oDelta: OleVariant; UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function GrabaTerminal(oDelta: OleVariant; UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function ImportaTerminales(Parametros: OleVariant; UsuarioTerminal: Integer; out Bitacora: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function ExisteHuellaRegistrada(Empleado: Integer; const Empresa: WideString): WordBool; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    procedure ReiniciaTerminal(Terminal: Integer; const Empresa: WideString); {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function ExisteEmpleadoEnGrupo(const Grupo: WideString): WordBool; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function ObtieneTemplates(Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    procedure EliminaHuellas(const Empresa: WideString; Empleado, Invitador, Usuario, Tipo: Integer); {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    procedure InsertaHuella(Parametros: OleVariant); {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function ReportaHuella(Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif} // SYNERGY
    function ExisteHuellaInvitador(const Empresa: WideString; Invitador: Integer): WordBool; {$ifndef DOS_CAPAS} safecall;
    function CopiarConfiguracionCafeteria(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetUsuariosSeguridad(out Longitud: Integer; Grupo: Integer; out BlockSistema: WordBool): OleVariant;
         {$ifndef DOS_CAPAS} safecall; {$endif}
    function ConsultaConfBD(Tipo: Integer): OleVariant; safecall; {$endif}
    function GrabaBD(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetEmpresasSistema(GrupoActivo: Integer): OleVariant; safecall;
    function GetBasesDatos(const CM_DATOS, CM_USRNAME, CM_PASSWRD: WideString): OleVariant; safecall;
    function GetServer: WideString; safecall;
    function GetSQLUserName: WideString; safecall;
    function GetSQLUserPassword: WideString;
    function EsBDEmpleados(const BaseDatos: WideString): WordBool; safecall;
    function EsBDSeleccion(const BaseDatos: WideString): WordBool; safecall;
    function EsBDAgregada(const Ubicacion: WideString): WordBool; safecall;
    function CrearEstructuraBD(Parametros: OleVariant): WordBool; safecall;
    function EmpGrabarDBSistema(Parametros: OleVariant): WordBool; safecall;
    function CreaInicialesTabla(Parametros: OleVariant): WordBool; safecall;
    function CreaSugeridosTabla(Parametros: OleVariant): WordBool; safecall;
    function CreaDiccionarioTabla(Parametros: OleVariant): WordBool; safecall;
    function GetLogicalName(const Conexion, Nombre: WideString; Tipo: Integer): WideString; safecall;
    function BackupDB(const Conexion, Origen, Destino, Ruta: WideString): OleVariant; safecall;
    function RestoreDB(const Conexion, Origen, Destino, DataName, PathDataName, LogName, PathLogName: WideString): OleVariant; safecall;
    function DepurarTabla(const Conexion, Tabla, Database: WideString): OleVariant; safecall;
    procedure AddField(DataSet: TDataSet; FieldName: string; Size: Integer);
    function ConectarseServidor(const Conexion: WideString; out Mensaje: WideString): WordBool; safecall;
    function GetDBSize(const Conn, Database: WideString; bUsarConexion: WordBool): OleVariant; safecall;
    function GetDataLogSize(const Conn, Database: WideString; bUsarConexion: WordBool): OleVariant; safecall;
    function ShrinkDB(const Conexion, Database: WideString): OleVariant; safecall;
    function GetConexion(const Conn, Database, Datos, UserName, Password: WideString): WordBool; safecall;
    function GrabaDB_INFO(Parametros: OleVariant): OleVariant; safecall;
    function GetPathSQLServer: OleVariant; safecall;
    function NoEsBDSeleccionVisitantes(const BaseDatos: WideString): WordBool; safecall;
    function NoEsBDTressVisitantes(const BaseDatos: WideString): WordBool; safecall;
    function ValidarBDEmpleados(const Codigo, Ubicacion: WideString; out Mensaje: WideString): OleVariant; safecall;
    function CrearBDEmpleados(Parametros: OleVariant): WordBool; safecall;
    function PrepararPresupuestos(Parametros: OleVariant): WordBool; safecall;
    function CatalogosPresupuestos(Parametros: OleVariant): WordBool; safecall;
    function EmpleadosPresupuestos(Parametros: OleVariant): WordBool; safecall;
    function OptimizarPresupuestos(Parametros: OleVariant): WordBool; safecall;
    function GetBDFromDB_INFO(const DB_CODIGO: WideString): WideString; safecall;
    function ConsultaActualizarBDs(Tipo: Integer): OleVariant; safecall;
    function EjecutaMotorPatch(var Parametros: OleVariant): WordBool; safecall;
    function CreaEspeciales(Parametros: OleVariant): WordBool; safecall;
    function ExistenEspeciales: WordBool; safecall;
    function ExisteBD_DBINFO(const Codigo: WideString): WordBool; safecall;
    function ImportarTablas(Empresa, Parametros: OleVariant): OleVariant; safecall;
    function GetTablasBD(const BaseDatosUbicacion, Usuario, Password: WideString): OleVariant; safecall;

    function ImportarTablaCSVGetASCII(Empresa, Parametros, ListaASCII: OleVariant; var ErrorCount: Integer): OleVariant; safecall;
    function ImportarTablaCSVLista(Empresa, Lista, Parametros: OleVariant): OleVariant; safecall;
    function GetCamposTabla(const BaseDatosUbicacion, Usuario, Password, Tabla: WideString): OleVariant; safecall;
    function GetCamposLlaveTabla(const BaseDatosUbicacion, Usuario, Password, Tabla: WideString): OleVariant; safecall;

    function GetTablaInfo(Empresa: OleVariant; const Tabla: WideString): OleVariant; safecall;
    function GetMotorPatchAvance(Parametros: OleVariant): OleVariant; safecall;
    function PrendeDerechosEntidades(const Empresas: WideString;
      Destino: OleVariant; Bitacora, SoloEntidadesNuevas: WordBool;
      out ErrorCount: Integer): OleVariant; safecall;
    function CantidadHuellaRegistrada(Empresa: OleVariant; Empleado, Biometrico, Tipo: Integer): Integer; safecall;
    function ExtraerChecadasBitTerm(Empresa, Parametros: OleVariant; out Datos: OleVariant): OleVariant; safecall;
    function GeneraReloj(Empresa, Parametros: OleVariant; out Datos: OleVariant): OleVariant; safecall;//@DC
    function GrabaBaseAdicionales(Empresa: OleVariant; GrupoUsuarios: Integer): OleVariant;safecall;


    function GetSistEnvioEmail(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetSuscripcionesCalendarioReportes(Empresa: OleVariant; Reporte: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    procedure BorrarUsuarioRolCalendario(Empresa: OleVariant; Folio,iUsCodigo: Integer; const sRoCodigo: WideString;iTipoOperacion: Integer; out iErrorCount: Integer); {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetUsuarioSuscripCalendarioReporte(Empresa: OleVariant;Usuario: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetBitacoraReportes(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetBitacoraCorreos(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function EnviarCorreo(const sUsuario, sBody, sRemitente, sDescripRemitente, sSubject, Encriptacion: WideString; oListaCorreos: OleVariant; const sDirecErrores: WideString; iSubtype: Integer): OleVariant;{$ifndef DOS_CAPAS} safecall; {$endif}
    function GetTareaCalendarioEmpresa(Empresa: OleVariant; Grupo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function ImportarSuscripcionesTressEmailPorTarea(Empresa: OleVariant; FolioTarea: Integer; const Frecuencia, Usuarios: WideString; Reporte: Integer; out Error: WideString): OleVariant; safecall;
    function GetSolicitudEnvio(Empresa, Parametros: OleVariant): OleVariant; safecall;
    procedure AgregarSolicitudEnvioProgramado(Folio, Reporte: Integer; const Nombre: WideString; out Mensaje: WideString); safecall;
    procedure CancelarEnvio(Folio: Integer; var Mensaje: WideString); safecall;
    procedure SetEnviosProgramados(out sError: WideString); safecall;
    function GetEmpresasAccesosGruposAcceso(Parametros: OleVariant): OleVariant; safecall;
    function GetUserConfidencialidad(UserId: Integer; const Empresa: WideString): OleVariant; safecall; //Davol #Bug: 21992 //#Bug: 15995

    {$endif}
 end;

 var
     // Ejecuci�n de los siguientes scripts para definir una base de datos de Empleados.
     // Ejecutar en el mismo orden.
     {1. 3DatosDefaults.sql
      2. 3DatosDomains.sql
      3. 3DatosTablas.sql
      4. 3DatosLabor.sql
      5. 3DatosMedico.sql
      6. 3DatosViews.sql
      7. 3DatosFKeys.sql
      8. 3DatosTriggers.sql
      9. 3DatosFunciones.sql
      10. 3DatosFuncionesKardex.sql
      11. 3DatosProcedures.sql
      12. 3DatosLaborSP.sql
      13. 3DatosVersion.sql}
    aArchivosSQLEstructura: array[ 0..11 ] of String =
       ('3DatosDefaults', '3DatosDomains', '3DatosTablas', '3DatosLabor',
        '3DatosMedico', '3DatosViews', '3DatosFKeys', '3DatosTriggers',
        '3DatosFunciones', '3DatosFuncionesKardex', '3DatosProcedures', '3DatosLaborSP');

    aArchivosXMLInicial: array[ 0..7 ] of String =
       ('GLOBAL', 'R_CLASIFI', 'REPORTE', 'CAMPOREP', 'NOMPARAM', 'QUERYS', 'TPERIODO', 'R_MODULO');

	aArchivosXMLSugeridos: array[ 0..39 ] of String =
       ('TAHORRO', 'CONCEPTO', 'NUMERICA', 'T_ART_80', 'ART_80',
        'AR_TEM_CUR', 'CLASIFI', 'CONTRATO', 'EDOCIVIL', 'ENTIDAD',
        'ESTUDIOS', 'TURNO', 'FESTIVO', 'HORARIO', 'INCIDEN',
        'TKARDEX', 'LEY_IMSS', 'MONEDA', 'MOT_AUTO', 'MOT_BAJA',
        'MOT_TOOL', 'OCUPA_NAC', 'TPRESTA', 'SSOCIAL', 'PRESTACI',
        // Correcci�n de bug #10380: Crear nueva BD basada en otra.
        'RIESGO', 'SAL_MIN', 'TCAMBIO',
        'TCTAMOVS', 'TRANSPOR', 'VIVE_CON', 'VIVE_EN', 'C_T_NACOMP', 'TIPO_SAT', 'BANCO',
		'TimListas', 'TSATCONTRA', 'TSATJORNAD', 'TSATRIESGO', 'VALOR_UMA');

    aArchivosXMLSugeridosCopia : array[ 0..50 ] of String =
       ('TAHORRO', 'CONCEPTO', 'NUMERICA', 'T_ART_80', 'ART_80',
        'AR_TEM_CUR', 'CLASIFI', 'CONTRATO', 'EDOCIVIL', 'ENTIDAD',
        'ESTUDIOS', 'TURNO', 'FESTIVO', 'HORARIO', 'INCIDEN',
        'TKARDEX', 'LEY_IMSS', 'MONEDA', 'MOT_AUTO', 'MOT_BAJA',
        'MOT_TOOL', 'OCUPA_NAC', 'TPRESTA', 'SSOCIAL', 'PRESTACI',
        // Correcci�n de bug #10380: Crear nueva BD basada en otra.
        'RIESGO', 'SAL_MIN', 'TCAMBIO',
        'TCTAMOVS', 'TRANSPOR', 'VIVE_CON', 'VIVE_EN', 'C_T_NACOMP', 'TIPO_SAT', 'BANCO','PUESTO','NIVEL1','NIVEL2','NIVEL3','NIVEL4'
        ,'NIVEL5','NIVEL6','NIVEL7','NIVEL8','NIVEL9','CURSO', 'TimListas', 'TSATCONTRA', 'TSATJORNAD', 'TSATRIESGO', 'VALOR_UMA' );

    aTablasDiccionario: array[ 0..9 ] of String =
       ('R_ENTIDAD', 'R_CLAS_ENT', 'R_DEFAULT',
        'R_ATRIBUTO', 'R_FILTRO', 'R_LISTAVAL', 'R_MOD_ENT', 'R_ORDEN',
        'R_RELACION', 'R_VALOR');
    //Selecci�n
    aArchivosSeleccionEstructura: array[ 0..6 ] of String =
       ('3SeleccionDefaults', '3SeleccionDomains', '3SeleccionTablas','3SeleccionFKeys', '3SeleccionTriggers',
       '3SeleccionProcedures', '3SeleccionVersion');

    aArchivosSeleccionInicial: array[ 0..2 ] of String =
       ('GLOBAL', 'REPORTE', 'CAMPOREP');

    aArchivosSeleccionDiccionario: array[ 0..0 ] of String = ('DICCION');

    //Visitantes
    aArchivosVisitantesEstructura: array[ 0..6 ] of String =
       ('3VisitasDefaults', '3VisitasDomains', '3VisitasTablas','3VisitasFKeys', '3VisitasTriggers',
       '3VisitasProcedures', '3VisitasVersion');

    aArchivosVisitantesInicial: array[ 0..2 ] of String =
       ('GLOBAL', 'REPORTE', 'CAMPOREP');

    aArchivosVisitantesDiccionario: array[ 0..0 ] of String = ('DICCION');

implementation

uses
     ZetaCommonTools,
     ZetaServerTools,
     ZEvaluador,
     ZetaQrExpr,
     ZGlobalTress,
     DSuperASCII,
     ZetaTipoEntidad,
    {$ifdef DEBUGSENTINEL}
     {$ifndef DOS_CAPAS}
     ZetaLicenseMgr,
     {$endif}
     {$endif}
     ZetaRegistryServer,
     ZReportConst,
     DBConfigServer,
     DPreparaPresupuestos,
     DMotorPatch,
     MotorPatchUtils,
     ZetaSQLScriptParser,
     DImportarDiccionario;

const
     {$ifdef ANTES }
     Q_SIST_BITACORA = 1;
     {$endif }
     Q_SIST_EMPRESAS = 2;
     Q_SIST_GRUPOS = 3;
     Q_SIST_POLL = 4;
     Q_SIST_USUARIOS = 5;
     Q_SIST_IMPRESORAS = 6;
     Q_SIST_USER_SUPER = 7;
     Q_SIST_ACCESOS = 8;
     Q_SIST_USER_ARBOL = 9;
     Q_ACTIVA_APAGA_USUARIOS_SIN_RESTRICCION = 10;
     Q_BORRA_PROCESOS = 11;
     Q_BORRA_BITACORA = 12;
     Q_GET_USUARIO_SUSCRIP = 13;
     Q_CLAVES_LEER = 14;
     Q_GET_GRUPOS_ACCESOS = 15;
     Q_GET_USUARIO_AREAS = 16;
     Q_DEL_USUARIO_AREAS = 17;
     Q_BORRA_BITACORA_PROCESOS = 18;
     Q_GET_EMPRESAS = 19;
     Q_GET_EMPRESAS_ACCESOS = 20;
     Q_ACTIVA_APAGA_USUARIOS = 21;
     Q_CHECAGRUPOS = 22;
     Q_NUEVO_USUARIO = 23;
     Q_GET_EMPRESAS_SIN_RESTRICCION = 24;
     Q_COMPANYS_COUNT = 25;
     Q_COMPANYS_LIST = 26;
     Q_EMPLEADOS_COMPANY = 27;
     Q_BORRA_EMPLEADOS_COMPARTE = 28;
     Q_INSERTA_EMP_ID = 29;
     Q_INVITADORES_COMPANY = 30;
     Q_INSERTA_INV_EMP_ID = 31;
     Q_CODIGO_REPETIDO = 32;
     Q_BORRA_TABLA = 33;
     Q_ACTUALIZA_DICCION = 34;
     Q_NIVEL0 = 35;
     Q_BITACORA = 36;
     Q_UPDATE_USER_COLABORA = 37;
     Q_USER_ROL_INSERT = 38;
     Q_ROL_USUARIOS_DELETE = 39;
     Q_ROL_USUARIOS = 40;
     Q_GET_USER_ROLES = 41;
     Q_DEL_USUARIO_ROLES = 42;
     Q_CLASIFI_EMPRESA = 43;
     Q_ACCESOS_ADICIONALES = 44;
     Q_BORRA_GRUPO_AD = 45;
     Q_GRUPO_ACCESOS = 46;
     Q_BORRA_BITACORA_KIOSCO = 47; {OP: 06/06/08}
     Q_BORRA_BITACORA_CAFETERIA = 48; {OP: 06/06/08}
     Q_ACTUALIZA_CODIGO_EMPRESA = 49;
     Q_COMPANYS_LIST_DISTINCT = 50;
     Q_BORRA_CODIGO_EMPRESA = 51;
     Q_SIST_LST_DISPOSITIVOS = 52;
     Q_DISP_DISPONIBLES = 53;
     Q_DISP_ASIGNADOS = 54;
     Q_BORRA_DISP = 55;
     Q_ACCARBOL = 56;
     Q_ACTUALIZA_EMPRESA_DER = 57;
     Q_BORRA_EMPRESA_DER = 58;
     Q_COMPANY = 59;
     Q_SIST_OBTEN_MAX_BIO = 60; // SYNERGY
     Q_SIST_BIT_BIO = 61; // SYNERGY
     Q_SIST_DEPURA_BIT_BIO = 62; // SYNERGY
     Q_SIST_ACTUALIZA_BIO_BORRA = 63; // SYNERGY
     Q_SIST_ACTUALIZA_BIO_AGREGA = 64; // SYNERGY
     Q_CONSULTA_LISTA_GRUPOS = 65; // SYNERGY
     Q_CONSULTA_TERM_POR_GRUPO = 66; // SYNERGY
     Q_AGREGA_LISTA_TERM_POR_GRUPO = 67; // SYNERGY
     Q_BORRA_LISTA_TERM_POR_GRUPO = 68; // SYNERGY
     Q_AGREGA_NUM_BIOMETRICO = 69; // SYNERGY
     Q_EXISTE_HUELLA_EMP = 70; // SYNERGY
     Q_AGREGA_TERMINALES = 71; // SYNERGY
     Q_REINICIA_TERMINAL = 72; // SYNERGY
     Q_REINICIA_TERM_MSG_FECHA = 73; // SYNERGY
     Q_EXISTE_EMPLEADO_EN_GRUPO = 74; // SYNERGY
     Q_BORRA_HUELLAGTI = 75; // SYNERGY
     Q_HUELLA_OBTENER_TODOS = 76; // BIOMETRICO
     Q_HUELLA_INSERTA = 77; // BIOMETRICO
     Q_HUELLA_BORRA = 78; // BIOMETRICO
     Q_SIST_ACTUALIZA_EMPLEADO = 79;
     Q_HUELLA_OBTENER_MAQUINA = 80;
     Q_HUELLA_REPORTA_INSERTA = 81;
     Q_HUELLA_REPORTA_ELIMINA = 82;
     Q_EXISTE_HUELLA_INV = 83;
     Q_CONSULTA_CONFIG_DBINFO                = 84;
     Q_CONSULTA_DB_SIZE                      = 85;
     Q_CONSULTA_VERSION                      = 86;
     Q_COMPANYS_LIST_DB                      = 87;
     Q_CONSULTA_DB_EMPLEADOS                 = 88;

     Q_GET_EMPRESAS_SISTEMA                  = 89;
     Q_GET_BASES_DATOS                       = 90;
     Q_INSERT_DB                             = 91;
     Q_UPDATE_DB                             = 92;
     Q_EMPRESAS_CON_DB                       = 93;
     Q_GET_BASES_DATOS_SERVER                = 94;
     Q_GET_BASES_DATOS_EMPLEADOS             = 95;
     Q_ES_BD_EMPLEADOS                       = 96;
     Q_CONSULTA_ACTUALIZAR_BDS               = 97;
     Q_GET_BASES_DATOS_SERVER2 			         = 98;

     Q_CONSULTA_LOGICAL_NAME 				         = 99;
     Q_BACKUP                                = 100;
     Q_RESTORE                               = 101;
     Q_DEPURAR                               = 102;
     Q_DB_SIZE                               = 103;
     Q_LOG_SIZE                              = 104;
     Q_SHRINK                                = 105;
     Q_CONEXION_EMPRESA                      = 106;

     Q_NO_ES_SELECCION_VISITANTES            = 107;
     K_QRY_CREA_BD_TRESS                     = 108;

     Q_ES_BD_SELECCION                       = 109;
     Q_NO_ES_TRESS_VISITANTES                = 110;

     Q_GET_TABLAS_BD                         = 111;
     Q_GET_CAMPOS_LLAVE                      = 112;
     Q_GET_CAMPOS_TABLA                      = 113;
     Q_GET_TABLA_EXPORTAR                    = 114;
     Q_UPDATE_TERM_MSG                       = 115; // SYNERGY
     Q_VERIFICA_TERMINAL                     = 116; // SYNERGY
     Q_OBTIENE_EMPLEADO_EMP_BIO              = 117; //SYNERGY
     Q_ACTUALIZA_ID_BIO_CONFIDENCIALIDAD     = 118; //SYNERGY
     Q_GET_EMPRESA_SIN_CONFIDENCIALIDAD      = 119;  //SYNERGY
     Q_CANTIDAD_HUELLA_EMP                   = 120; // SYNERGY
     Q_EXPORTAR_CHECADAS_BIT_TERM_GTI        = 121; // SYNERGY
     Q_SIST_GET_INFO_ID_HUELLA               = 122; // SYNERGY
     Q_SIST_GET_INFO_ID_PROX                 = 123; // SYNERGY
     Q_BORRA_TERM_HUELLA_TODAS               = 124; // SYNERGY
     Q_REINICIA_TERM_MSG_SYNC                = 125; // SYNERGY
     Q_SIST_GET_CM_DIGITO                    = 126; // SYNERGY
     Q_SIST_ENVIO_EMAIL                      = 127;
     Q_INSERT_TAREA_CALENDARIO               = 128;
     Q_SIST_TAREA_CALENDARIO                 = 129;
     Q_CONFIGURACION_CAF                     = 130;
     Q_CALENDARIO_CAF                        = 131;
     Q_LISTA_ESTACIONES                      = 132;
     Q_GET_DISP                              = 133;
     Q_ACTUALIZAR_CONFIG                     = 134;
     Q_INSERTAR_CONFIG                       = 135;
     Q_INSERTAR_CALEND                       = 136;
     Q_BORRAR_CALEND                         = 137;
     Q_SIST_TAREA_USUARIO                    = 138;
     Q_SIST_TAREA_ROLES                      = 139;
     Q_BORRA_SIST_TAREA_ROLES                = 140;
     Q_BORRA_SIST_TAREA_USUARIO              = 141;
     Q_SIST_UPD_TAREA_CALENDARIO             = 142;
     Q_SIST_GET_SUSCRIPCIONES_CALENDARIO_REP = 143;
     Q_SIST_UPDATE_USUARIO_CALENDARIO        = 144;
     Q_SIST_UPDATE_ROL_CALENDARIO            = 145;
     Q_GET_USUARIO_SUSCRIP_CALENDARIO        = 146;
     Q_BITACORA_REPORTES                     = 147;
     Q_SP_AGREGA_CALENDARIO                  = 148;
     Q_BITACORA_CORREOS                      = 149;

     {
      Reportes Email v 2017.
      Herramienta de migraci�n de tareas de TressEmail
      a nuevo esquema de suscripciones en Comparte.
      Consultas.
     }
     Q_LEER_SUSCRIP                                = 150;
     Q_SIST_TAREA_CALENDARIO_EMPRESA               = 151;
     Q_INSERT_ROLUSUARIO                           = 152;
     Q_UPDATE_CALENDARIO_DESDE_SUSC                = 153;
     Q_INSERT_CALENDARIO_DESDE_SUSC                = 154;
     Q_LEE_CALENDARIO                              = 155;
     Q_SOLICITUD_ENVIOS                            = 156;
     Q_BITACORA_REPORTES_FOLIO                     = 157;
     Q_INSERT_CALENSOLI                            = 158;
     Q_LEER_CALENSOLI                              = 159;
     Q_UPDATE_CALENSOLI                            = 160;
     Q_GET_STATUS_EMPLEADO                   	   = 161; //SYNERGY
     Q_INICIAR_KARDEX_CONCEPTOS                    = 162; //US 17266: Almacenar la clave SAT cuando se timbra la n�mina para obtener reportes y auditar timbrado
     Q_TERMINALES_STATUS                            = 163; //Consultar Status de terminales
     Q_REINICIA_TERM_MSG_DIAGNOSTICO               = 164; //Diagn�stico de todas las terminales
     Q_GET_EMPRESAS_ACCESOS_GRUPO_USUARIOS         = 165;

  K_NOMBRE_EMPRESA  = '#EMPRESA';
  K_NOMBRE_COMPARTE = '#COMPARTE';
  K_DIGITO_EMPRESA  = '#DIGITO';

  {$R *.DFM}

{ ************ TdmServerSistema ************ }

class procedure TdmServerSistema.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
     if Register then
     begin
          inherited UpdateRegistry(Register, ClassID, ProgID);
          EnableSocketTransport(ClassID);
          EnableWebTransport(ClassID);
     end
     else
     begin
          DisableSocketTransport(ClassID);
          DisableWebTransport(ClassID);
          inherited UpdateRegistry(Register, ClassID, ProgID);
     end;
end;

{$ifdef DEBUGSENTINEL}
procedure TdmServerSistema.dmServerSistemaCreate(Sender: TObject);
{$ifndef DOS_CAPAS}
var
   oLicenseMgr : TLicenseMgr;
{$endif}
begin
     FUsuarioData := Nil;
     FDataSetEmpresa := Nil;
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
     oAutoServer := ZetaSQLBroker.GetAutoServer;
     {$ifndef DOS_CAPAS}
     OnActivate := ServerActivate;
     OnDeactivate := ServerDeactivate;
     {$endif}
     {$ifdef DOS_CAPAS}
     ZetaSQLBroker.ReadAutoServer( oAutoServer, NIL, NIL );
     {$else}
     oLicenseMgr := TLicenseMgr.Create( oZetaProvider );
     try
        ZetaSQLBroker.ReadAutoServer( oAutoServer, oLicenseMgr.AutoGetData, oLicenseMgr.AutoSetData );
     finally
            FreeAndNil( oLicenseMgr );
     end;
     {$endif}
     oZetaProviderComparte := DZetaServerProvider.GetZetaProvider( Self );
     oZetaProviderDiccionario := DZetaServerProvider.GetZetaProvider( Self );

end;
{$else}
procedure TdmServerSistema.dmServerSistemaCreate(Sender: TObject);
begin
     FUsuarioData := Nil;
     FDataSetEmpresa := Nil;
     {$ifndef DOS_CAPAS}
     OnActivate := ServerActivate;
     OnDeactivate := ServerDeactivate;
     {$endif}
     oZetaProviderComparte := DZetaServerProvider.GetZetaProvider( Self );
     oZetaProviderDiccionario := DZetaServerProvider.GetZetaProvider( Self );
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
     oAutoServer := ZetaSQLBroker.GetAutoServer;
     ZetaSQLBroker.ReadAutoServer( oAutoServer, nil, nil );
end;
{$endif}

procedure TdmServerSistema.dmServerSistemaDestroy(Sender: TObject);
begin
     FreeAndNil( FUsuarioData );
     ZetaSQLBroker.FreeZetaCreator( oZetaCreator );
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
     DZetaServerProvider.FreeZetaProvider( oZetaProviderComparte );
     DZetaServerProvider.FreeZetaProvider( oZetaProviderDiccionario );
     ZetaSQLBroker.FreeAutoServer( oAutoServer );
end;

{$ifdef DOS_CAPAS}
procedure TdmServerSistema.CierraEmpresa;
begin
end;
{$else}
procedure TdmServerSistema.ServerActivate(Sender: TObject);
begin
     oZetaProviderComparte.Activate;
     oZetaProviderDiccionario.Activate;
     oZetaProvider.Activate;
end;

procedure TdmServerSistema.ServerDeactivate(Sender: TObject);
begin
     oZetaProviderComparte.Deactivate;
     oZetaProviderDiccionario.Deactivate;
     oZetaProvider.Deactivate;
end;
{$endif}

{ ***************** PARTE PRIVADA *******************************}

function TdmServerSistema.GetScript( const iScript: Integer ): String;
begin
     case iScript of
     {$ifdef ANTES}
          Q_SIST_BITACORA: Result := 'select BI_FECHA, BI_HORA, BI_TIPO, BI_NUMERO, '+
                                     'BI_TEXTO, BI_PROC_ID, US_CODIGO, '+
                                     'CB_CODIGO, BI_DATA, BI_CLASE, BI_FEC_MOV ' +
                                     'from BITACORA where ' +
                                     '( BI_FECHA BETWEEN ''%s'' and ''%s'' ) '+
                                     '@BITACORA @EMPLEADO @USUARIO '+
                                     'order by BI_FECHA DESC, BI_HORA desc';
     {$endif}
          Q_SIST_EMPRESAS: Result := 'select CM_CODIGO, CM_NOMBRE, CM_ALIAS, CM_USRNAME, '+
                                     'CM_PASSWRD, CM_ACUMULA, CM_CONTROL, CM_EMPATE, CM_USACAFE, CM_USACASE, DB_CODIGO '+
                                     'from COMPANY order by CM_CODIGO';
          Q_SIST_GRUPOS: Result := 'select GR_CODIGO, GR_DESCRIP from GRUPO order by GR_CODIGO';
          Q_SIST_POLL: Result := 'select PO_EMPRESA,PO_NUMERO,PO_LETRA,PO_FECHA,PO_HORA, PO_LINX '+
                                 'from POLL order by PO_EMPRESA, PO_NUMERO, PO_FECHA, PO_HORA';
          Q_SIST_USUARIOS: Result := 'select US_CODIGO, US_CORTO, US_NOMBRE, US_PASSWRD, US_DENTRO, '+
                                     'GRUPO.GR_CODIGO, GRUPO.GR_DESCRIP, US_NIVEL, US_BLOQUEA, US_CAMBIA, '+
                                     'US_FEC_IN, US_FEC_OUT, US_ARBOL, US_FORMA, US_BIT_LST, '+
                                     'US_DOMAIN, US_ACTIVO, US_PORTAL, CB_CODIGO '+
                                     'from USUARIO '+
                                     'left outer join GRUPO on ( GRUPO.GR_CODIGO = USUARIO.GR_CODIGO ) '+
                                     'order by US_CODIGO';
          Q_SIST_IMPRESORAS: Result := 'select PI_NOMBRE, PI_EJECT, '+
                                       'PI_CHAR_10, PI_CHAR_12, PI_CHAR_17, '+
                                       'PI_EXTRA_1, PI_EXTRA_2, PI_RESET, '+
                                       'PI_UNDE_ON, PI_UNDE_OF, '+
                                       'PI_BOLD_ON, PI_BOLD_OF, '+
                                       'PI_6_LINES, PI_8_LINES, '+
                                       'PI_ITAL_ON, PI_ITAL_OF, '+
                                       'PI_LANDSCA from PRINTER order by PI_NOMBRE';
          Q_SIST_USER_SUPER: Result := 'select US_CODIGO, CB_NIVEL from SUPER where ( US_CODIGO = :Usuario ) order by US_CODIGO, CB_NIVEL';
          Q_SIST_ACCESOS: Result := 'select GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO from ACCESO where '+
                                    '( GR_CODIGO = :Grupo ) and ( CM_CODIGO = :Empresa )';
          Q_SIST_USER_ARBOL: Result := 'select US_CODIGO, AB_ORDEN, AB_NIVEL, AB_NODO, AB_DESCRIP from ARBOL where ( US_CODIGO = :Usuario ) order by AB_ORDEN';
          Q_ACTIVA_APAGA_USUARIOS_SIN_RESTRICCION: Result := 'update USUARIO set US_BLOQUEA = ''%s'' '+
                                                             'where ( US_CODIGO <> %d )';
          Q_BORRA_PROCESOS: Result := 'delete from PROCESO where ( PC_FEC_INI < ''%s'' ) and PC_PROC_ID in( %s )';
          Q_BORRA_BITACORA: Result:= 'delete from BITACORA where ( BI_FECHA < ''%s'' ) %s';
          Q_GET_USUARIO_SUSCRIP : Result := 'select SUSCRIP.US_CODIGO, SUSCRIP.RE_CODIGO, REPORTE.RE_NOMBRE, SUSCRIP.SU_FRECUEN, SUSCRIP.SU_VACIO, SUSCRIP.SU_ENVIAR ' +
                                            'from SUSCRIP ' +
                                            'left outer join REPORTE ON REPORTE.RE_CODIGO = SUSCRIP.RE_CODIGO '+
                                            'where SUSCRIP.%s = %d   ' +
                                            'order BY REPORTE.RE_NOMBRE';
          Q_CLAVES_LEER: Result := 'select CL_TEXTO from CLAVES where ( CL_NUMERO = %d )';
          Q_GET_GRUPOS_ACCESOS: Result := 'select A.GR_CODIGO, A.CM_CODIGO, G.GR_DESCRIP, C.CM_NOMBRE from ACCESO A '+
                                          'left outer join GRUPO G on ( G.GR_CODIGO = A.GR_CODIGO ) '+
                                          'left outer join COMPANY C on ( C.CM_CODIGO = A.CM_CODIGO ) '+
                                          'where ( ( A.GR_CODIGO <> %d ) or ( A.CM_CODIGO <> ''%s'' ) ) and ( %s ) '+
                                          'group by A.GR_CODIGO, G.GR_DESCRIP, A.CM_CODIGO, C.CM_NOMBRE ' +
                                          'ORDER by A.GR_CODIGO, G.GR_DESCRIP, A.CM_CODIGO, C.CM_NOMBRE ';
          Q_GET_USUARIO_AREAS: Result := 'select TB_CODIGO CB_AREA, TB_ELEMENT, '+
                                          '( select US_CODIGO from SUP_AREA S where ( S.US_CODIGO = %d ) and ( S.CB_AREA = A.TB_CODIGO ) ) US_CODIGO '+
                                          'from AREA A Order by A.TB_ELEMENT';
          Q_DEL_USUARIO_AREAS: Result := 'delete from SUP_AREA where ( US_CODIGO = %d )';
          Q_BORRA_BITACORA_PROCESOS: Result := 'delete from BITACORA where BI_NUMERO in( select PC_NUMERO from PROCESO '+
                                               'where ( PC_FEC_INI < ''%s'' ) and ( PC_PROC_ID in( %s ) ) )';
          Q_BORRA_BITACORA_KIOSCO: Result := 'delete from BITKIOSCO where BI_FECHA < ''%s'' ';{OP: 07/06/08}
          Q_BORRA_BITACORA_CAFETERIA: Result := 'delete from BITCAFE where BC_FECHA < ''%s'' ';{OP: 07/06/08}
          Q_GET_EMPRESAS: Result := 'select C.CM_CODIGO, C.CM_NOMBRE, C.CM_ACUMULA, C.CM_CONTROL, C.CM_EMPATE, '+
                                    'C.CM_ALIAS, C.CM_PASSWRD, C.CM_USRNAME, C.CM_USACAFE, C.CM_USACASE, C.CM_NIVEL0, C.CM_DATOS, C.CM_DIGITO, C.CM_KCLASIFI, ' +
                                    'C.CM_KCONFI, C.CM_KUSERS, C.CM_CTRL_RL '+
                                    ', DB_CODIGO ' +
                                    'from COMPANY C '+
                                    'where ( %0:s ) and ( ( %1:d = %2:d ) or ( C.CM_CODIGO in( select DISTINCT( CM_CODIGO ) '+
                                    'from ACCESO where ( AX_DERECHO > 0 ) and '+
{$ifdef INTERBASE}
                                    '( GR_CODIGO in ( select GR_HIJO from GET_DESCENDENCIA( %1:d ) ) ) '+
{$endif}
{$ifdef MSSQL}
                                    '( GR_CODIGO in ( select GR_HIJO from dbo.GET_DESCENDENCIA( %1:d ) ) ) '+
{$endif}
                                    ') ) ) order by C.CM_CODIGO';
          Q_GET_EMPRESAS_ACCESOS: Result := 'select C.CM_CODIGO, C.CM_NOMBRE, C.CM_ALIAS, C.CM_USRNAME, '+
                                                   'C.CM_PASSWRD, C.CM_ACUMULA, C.CM_CONTROL, C.CM_EMPATE, C.CM_USACAFE, C.CM_USACASE, '+
                                                   'C.CM_NIVEL0, C.CM_DATOS, C.CM_DIGITO, '+
                                                    '( select count(*) from ACCESO A where ( A.GR_CODIGO = %d ) and ( A.CM_CODIGO = C.CM_CODIGO ) '+
                                                    'and ( A.AX_DERECHO > 0 ) ) ACCESOS '+
                                                    ', DB_CODIGO ' +
                                                    'from company C '+
                                                    'where ( %s ) order by C.CM_CODIGO';
          Q_ACTIVA_APAGA_USUARIOS: Result := 'update USUARIO set US_BLOQUEA = ''%s'' '+
                                             'where ( US_CODIGO <> %d ) and ' +
{$ifdef INTERBASE}
                                             'GR_CODIGO in ( select GR_HIJO from GET_DESCENDENCIA( %d ) )';
{$endif}
{$ifdef MSSQL}
                                             'GR_CODIGO in ( select GR_HIJO from dbo.GET_DESCENDENCIA( %d ) )';
{$endif}
{$ifdef INTERBASE}
          Q_CHECAGRUPOS: Result := 'execute procedure CHECK_ASCENDENCIA( %d )';
{$endif}
{$ifdef MSSQL}
          Q_CHECAGRUPOS: Result := 'exec dbo.CHECK_ASCENDENCIA %d';
{$endif}
          Q_NUEVO_USUARIO: Result := 'Select MAX(US_CODIGO) US_CODIGO from USUARIO';
          Q_GET_EMPRESAS_SIN_RESTRICCION: Result := 'select CM_CODIGO, CM_NOMBRE, CM_ACUMULA, CM_CONTROL, CM_EMPATE, '+
                                                    'CM_ALIAS, CM_PASSWRD, CM_USRNAME, CM_USACAFE, CM_USACASE, CM_NIVEL0, CM_DATOS '+
                                                    ', DB_CODIGO ' +
                                                    'from COMPANY '+
                                                    'where ( %s ) order by CM_CODIGO';
          Q_COMPANYS_COUNT: Result := 'select COUNT(*) CUANTOS '+
                                      'from COMPANY '+
                                      'where ( %s ) and '+
                                      '( CM_DIGITO is NOT NULL ) and '+
                                      '( CM_DIGITO <> '' '' )';
          Q_COMPANYS_LIST: Result := 'select CM_NOMBRE, CM_ALIAS, CM_USRNAME, CM_PASSWRD, CM_NIVEL0, CM_DATOS, CM_CODIGO '+
                                      ', DB_CODIGO ' +
                                     'from COMPANY '+
                                     'where ( %s ) and '+
                                     '( CM_DIGITO is NOT NULL ) and '+
                                     '( CM_DIGITO <> '' '' ) '+
                                     'order by CM_CODIGO';
          Q_EMPLEADOS_COMPANY: Result := 'select CB_CODIGO, CB_ID_NUM, CB_GP_COD from COLABORA '+
                                        'where ( CB_ID_NUM is NOT NULL ) and '+
                                        '( CB_ID_NUM <> '' '' ) '+
                                        'order by CB_CODIGO';
          Q_BORRA_EMPLEADOS_COMPARTE: Result := 'delete from EMP_ID '+
                                               'where ( CM_CODIGO = :CM_CODIGO )';
          Q_INSERTA_EMP_ID: Result := 'insert into EMP_ID( ID_NUMERO, CM_CODIGO, CB_CODIGO, IV_CODIGO, GP_CODIGO ) '+
                                      'values( :ID_NUMERO, :CM_CODIGO, :CB_CODIGO, 0, :CB_GP_COD )';
          Q_INVITADORES_COMPANY: Result := 'select IV_CODIGO, IV_ID_NUM from INVITA '+
                                         'where ( IV_ID_NUM is NOT NULL ) and '+
                                         '( IV_ID_NUM <> '' '' ) '+
                                         'order by IV_CODIGO';
          Q_INSERTA_INV_EMP_ID: Result := 'insert into EMP_ID( ID_NUMERO, CM_CODIGO, CB_CODIGO, IV_CODIGO ) '+
                                          'values( :ID_NUMERO, :CM_CODIGO, 0, :IV_CODIGO )';
          Q_CODIGO_REPETIDO: Result := 'select CM_CODIGO, CB_CODIGO, IV_CODIGO from EMP_ID '+
                                        'where ( ID_NUMERO = %s )';
          Q_BORRA_TABLA: Result := 'delete from %s';
          Q_ACTUALIZA_DICCION: Result := 'execute procedure SP_REFRESH_DICCION';
          Q_NIVEL0: Result := 'SELECT TB_CODIGO, TB_ELEMENT,%s TB_SUB_CTA FROM NIVEL0';
          Q_BITACORA: Result := 'select BI_FECHA, BI_HORA, BI_TIPO, BI_NUMERO, '+
                                'BI_TEXTO, BI_PROC_ID, US_CODIGO, CB_CODIGO, '+
                                'BI_DATA, BI_CLASE, BI_FEC_MOV ' +
                                'from BITACORA where ' +
                                '( %0:s >= %1:s and %0:s < %2:s ) and '+
                                '( ( %3:d = -1 ) or ( BI_TIPO = %3:d ) ) and '+
                                '( ( %4:d <= 0 ) or ( BI_CLASE = %4:d  ) ) and '+
                                '( ( %5:d = 0 ) or ( US_CODIGO = %5:d ) ) and '+
                                '( ( %6:d = -1 ) or ( BI_NUMERO = %6:d ) ) '+
                                'order by BI_FECHA DESC, BI_HORA DESC';

          Q_USER_ROL_INSERT: Result := 'insert into USER_ROL( RO_CODIGO, US_CODIGO ) values ( :RO_CODIGO, :US_CODIGO )';
          Q_ROL_USUARIOS_DELETE: Result := 'delete from USER_ROL where ( USER_ROL.RO_CODIGO = ''%s'' )';
          Q_ROL_USUARIOS: Result := 'select USER_ROL.RO_CODIGO, USER_ROL.US_CODIGO, ROL.RO_NOMBRE, USUARIO.US_NOMBRE from USER_ROL '+
                                    'left outer join ROL on ( ROL.RO_CODIGO = USER_ROL.RO_CODIGO ) '+
                                    'left outer join USUARIO on ( USUARIO.US_CODIGO = USER_ROL.US_CODIGO ) '+
                                    'where ( USER_ROL.RO_CODIGO = ''%s'' ) order by USER_ROL.US_CODIGO';
          Q_GET_USER_ROLES: Result := 'select UR.US_CODIGO,UR.RO_CODIGO,UR.UR_PPAL,R.RO_NOMBRE from USER_ROL UR '+
                                      'left outer join ROL R on ( R.RO_CODIGO = UR.RO_CODIGO ) '+
                                      'where UR.US_CODIGO = %d';
          Q_DEL_USUARIO_ROLES: Result := 'delete from USER_ROL where US_CODIGO = %d';
          Q_CLASIFI_EMPRESA: Result := 'select RC_ORDEN, RC_CODIGO, RC_NOMBRE from R_CLASIFI order by RC_ORDEN';
          Q_ACCESOS_ADICIONALES: Result := 'select * from GR_AD_ACC where CM_CODIGO = ''%s'' and GR_CODIGO= %d';
          Q_BORRA_GRUPO_AD: Result := 'delete from gr_ad_acc where GX_CODIGO NOT IN (select GX_CODIGO from grupo_ad)';
          Q_GRUPO_ACCESOS:  Result := 'select GRUPO_AD.*, GR_AD_ACC.GX_DERECHO from GRUPO_AD '+
                                      'left outer join GR_AD_ACC ON GR_AD_ACC.CM_CODIGO = %0:s '+
                                            'and GR_AD_ACC.GR_CODIGO = %1:d '+
                                            'and GR_AD_ACC.GX_CODIGO = GRUPO_AD.GX_CODIGO '+
                                            'where GRUPO_AD.GX_CODIGO IN ( select GR_AD_ACC.GX_CODIGO from GR_AD_ACC '+
                                      'where CM_CODIGO = %0:s and GR_CODIGO = %1:d and ( GX_DERECHO > 0 %2:s ) ) ';
          Q_ACTUALIZA_CODIGO_EMPRESA: Result :=
                                               {$IFDEF MSSQL}
                                               'if ( DBO.SP_EXISTE_TABLA(''%0:s'') = 1 ) '+
                                               'BEGIN '+
                                               {$endif}
                                               ' update %0:s set GR_CODIGO = %1:d where GR_CODIGO = %2:d '{$IFDEF MSSQL}+{$ELSE};{$ENDIF}
                                               {$IFDEF MSSQL}
                                               'END ';
                                               {$ENDIF}
          Q_COMPANYS_LIST_DISTINCT :  Result := 'select DISTINCT(CM_DATOS),CM_NOMBRE, CM_ALIAS, CM_USRNAME, CM_PASSWRD, CM_NIVEL0,  CM_CODIGO '+
                                                ', DB_CODIGO ' +
                                                'from COMPANY '+
                                                'where ( %s ) and '+
                                                '( CM_DIGITO is NOT NULL ) and '+
                                                '( CM_DIGITO <> '' '' ) '+
                                                'order by CM_CODIGO';
          Q_BORRA_CODIGO_EMPRESA : Result :=   {$IFDEF MSSQL}
                                               'if ( DBO.SP_EXISTE_TABLA(''%0:s'') = 1 ) '+
                                               'BEGIN '+
                                               {$endif}
                                               ' delete from %0:s where GR_CODIGO = %1:d '{$IFDEF MSSQL}+{$ELSE};{$ENDIF}
                                               {$IFDEF MSSQL}
                                               'END ';
                                               {$ENDIF}

          Q_SIST_LST_DISPOSITIVOS : Result :='SELECT DI_NOMBRE, D.DI_TIPO, DI_DESCRIP, DI_NOTA, DI_IP,'+
                                             'CASE ' +
                                             'WHEN D.DI_TIPO <> 1 THEN 0 ' +
                                             'WHEN D.DI_TIPO = 1 AND CF_NOMBRE IS NOT NULL THEN 1 ' +
                                             'ELSE 2 ' +
                                             'END AS STATUS '+
                                             'FROM DISPOSIT D '+
                                             'LEFT JOIN CAF_CONFIG CF '+
                                             'ON DI_NOMBRE = CF_NOMBRE AND D.DI_TIPO = CF.DI_TIPO '+
                                             'order by DI_TIPO, DI_NOMBRE asc';
          Q_DISP_DISPONIBLES : Result := 'select DI_NOMBRE, DI_TIPO, DI_DESCRIP, DI_NOTA, DI_IP from DISPOSIT where DI_NOMBRE not in( select DI_NOMBRE from DISXCOM where CM_CODIGO = ''%s''  )';
          Q_DISP_ASIGNADOS : Result := 'select CM_CODIGO, DI_NOMBRE, DI_TIPO from DISXCOM where CM_CODIGO = ''%s'' ';
          Q_BORRA_DISP : Result := 'delete from DISXCOM where CM_CODIGO = ''%s'' ';
          Q_ACCARBOL : Result := 'select dbo.TF(288,V_ACCARBOL.AA_SOURCE) AA_SOURCE, AX_NUMERO,AA_DESCRIP,dbo.QuitaAcentos(AA_DESCRIP) FILTRO_DES, '+
                                 'MO_NOMBRE from V_ACCARBOL                  '+
                                 'left outer join R_MODULO on R_MODULO.MO_CODIGO = AA_MODULO ';
          Q_ACTUALIZA_EMPRESA_DER : Result :=  'update %s set CM_CODIGO = ''%s'' where CM_CODIGO = ''%s'' ';
          Q_BORRA_EMPRESA_DER : Result :=  'delete from %s where CM_CODIGO = ''%s'' ';
          Q_COMPANY : Result := 'Select CM_DATOS,CM_NOMBRE, CM_ALIAS, CM_USRNAME, CM_PASSWRD, CM_NIVEL0, CM_CODIGO from COMPANY WHERE CM_CODIGO = ''%s''';
          Q_SIST_OBTEN_MAX_BIO : Result := 'select max(ID_NUMERO) as MAXIMO from EMP_BIO'; // SYNERGY
          Q_SIST_BIT_BIO : Result := 'select WS_FECHA, CH_RELOJ, WS_CHECADA, CB_CODIGO, WS_MENSAJE, CM_CODIGO, WS_TIPO from WS_BITAC %s order by WS_FECHA desc, CM_CODIGO'; // SYNERGY
          Q_SIST_DEPURA_BIT_BIO : Result := 'delete from WS_BITAC where ( WS_FECHA >= ''%s'' ) and ( WS_FECHA < ''%s'' ) %s'; // SYNERGY
          Q_SIST_ACTUALIZA_BIO_BORRA : Result := 'delete from EMP_BIO where CM_CODIGO=''%s'' '; // SYNERGY
          Q_SIST_ACTUALIZA_BIO_AGREGA : Result := 'insert into EMP_BIO (ID_NUMERO, CB_CODIGO, CM_CODIGO, GP_CODIGO, IV_CODIGO, CB_ACTIVO, CB_FEC_BAJ) values (%d, %d, ''%s'', ''%s'', %d, ''%s'', ''%s'')'; // SYNERGY
          Q_CONSULTA_LISTA_GRUPOS : Result := 'select GP_CODIGO, GP_DESCRIP,CM_CODIGO from GRUPOTERM order by GP_CODIGO'; // SYNERGY [where CM_CODIGO in(select CM_CODIGO from COMPANY where CM_DATOS in(select CM_DATOS from COMPANY where CM_CODIGO =''%s'')) ]
          Q_CONSULTA_TERM_POR_GRUPO : Result := 'select TE_CODIGO, GP_CODIGO from TERM_GPO %s order by TE_CODIGO'; // SYNERGY
          Q_AGREGA_LISTA_TERM_POR_GRUPO : Result := 'insert into TERM_GPO (TE_CODIGO, GP_CODIGO) values (''%s'', ''%s'')'; // SYNERGY
          Q_BORRA_LISTA_TERM_POR_GRUPO : Result := 'delete TERM_GPO where GP_CODIGO = ''%s'''; // SYNERGY
          Q_AGREGA_NUM_BIOMETRICO: Result := 'update COLABORA set CB_ID_BIO=:CB_ID_BIO, CB_GP_COD=:CB_GP_COD where CB_CODIGO=:CB_CODIGO'; // SYNERGY
          Q_EXISTE_HUELLA_EMP: Result := 'select   h.cb_codigo from huellagti h , company c,emp_bio e  where h.cm_codigo=c.cm_codigo and c.cm_codigo = e.cm_codigo and h.cb_codigo = e.cb_codigo and h.cb_codigo = %d and e.id_numero = %s and h.iv_codigo = 0 and hu_indice = 11'; // SYNERGY
          Q_AGREGA_TERMINALES: Result := 'exec dbo.WS_ALTA_TERMINAL ''%s'''; // SYNERGY
          Q_REINICIA_TERMINAL: Result := 'update TERM_HUELLA set borrar = ''S'' %s ' ; // SYNERGY

          Q_REINICIA_TERM_MSG_FECHA: Result:=  'update TERM_MSG set TM_NEXT=''18991230'' where DM_CODIGO=6 %s'; // SYNERGY

          Q_EXISTE_EMPLEADO_EN_GRUPO: Result := 'select top 1 GP_CODIGO from EMP_BIO where GP_CODIGO=''%s'''; // SYNERGY
          //BIOMETRICO
          // Q_BORRA_HUELLAGTI: Result := 'delete from HUELLAGTI where CB_CODIGO=%d '; // SYNERGY
          Q_BORRA_HUELLAGTI: Result := 'delete from HUELLAGTI where CB_CODIGO=%d and IV_CODIGO = 0 and CM_CODIGO =''%s'''; // SYNERGY
          Q_HUELLA_OBTENER_TODOS: Result := 'SELECT HU_ID, ID_NUMERO, CM_CODIGO, CB_CODIGO, HU_INDICE, HU_HUELLA, IV_CODIGO, HU_TIMEST, TIPO FROM V_HUELLAS'; //BIOMETRICO
          Q_HUELLA_INSERTA: Result := 'exec SP_HUELLAGTI_INSERTA ''%s'',%d,%d,''%s'',%d, ''%s'''; //BIOMETRICO
          Q_HUELLA_BORRA: Result := 'delete from HUELLAGTI where CM_CODIGO=''%s'' '; //BIOMETRICO
          Q_SIST_ACTUALIZA_EMPLEADO: Result := 'update HUELLAGTI set CB_CODIGO = %d where CM_CODIGO = ''%s'' and IV_CODIGO = %d'; //BIOMETRICO
          Q_HUELLA_OBTENER_MAQUINA: Result := 'SELECT V.HU_ID, V.ID_NUMERO, V.CM_CODIGO, V.CB_CODIGO, V.HU_INDICE, V.HU_HUELLA, V.IV_CODIGO, V.HU_TIMEST, V.TIPO, C.CH_CODIGO FROM V_HUELLAS AS V LEFT OUTER JOIN COM_HUELLA AS C ON V.HU_ID = C.HU_ID AND C.CH_CODIGO = ''%s'' WHERE (C.CH_CODIGO IS NULL) '; //Mejoras de Biometrico
          Q_HUELLA_REPORTA_INSERTA: Result := 'exec SP_REPORTAHUELLA ''%s'', %s';
          Q_HUELLA_REPORTA_ELIMINA: Result := 'delete from COM_HUELLA where CH_CODIGO = ''%s''';
          Q_EXISTE_HUELLA_INV: Result := 'select top 1 IV_CODIGO from HUELLAGTI where upper(CM_CODIGO)=upper(''%s'') and IV_CODIGO=%d'; // BIOMETRICO
          Q_UPDATE_TERM_MSG: Result := 'update TERM_MSG set TM_NEXT = ''18991230'' where ( DM_CODIGO = ''4'' ) and ( TE_CODIGO in ( %s ) )';
          Q_VERIFICA_TERMINAL: Result := 'select COUNT(*) CUANTOS from TERM_GPO where ( TE_CODIGO = %s ) and ( GP_CODIGO = ''%s'' )';
          Q_OBTIENE_EMPLEADO_EMP_BIO: Result := 'select count(*) CUANTOS from EMP_BIO where ( CM_CODIGO = %s ) and ( CB_CODIGO = %d )';
          Q_ACTUALIZA_ID_BIO_CONFIDENCIALIDAD: Result := 'delete from EMP_BIO where ( CM_CODIGO = %s ) ';
          Q_GET_EMPRESA_SIN_CONFIDENCIALIDAD: Result := 'select CM_CODIGO from COMPANY where ( CM_NIVEL0 = '''' ) and UPPER( CM_DATOS ) = ( select UPPER( CM_DATOS ) from COMPANY where CM_CODIGO = %s )';
          Q_CONSULTA_CONFIG_DBINFO: Result := 'select DB_CODIGO, DB_DESCRIP, DB_DATOS, DB_CONTROL, DB_TIPO, DB_USRNAME, DB_PASSWRD, 0.0 as DB_VERSION, 0 as DB_EMPLEADOS, 0.0 as DB_SIZE, cast ('''' as VARCHAR(250) ) as DB_ERROR, ''  '' as DB_OK, ''N'' AS DB_USRDFLT, DB_CTRL_RL, DB_CHKSUM, DB_ESPC' + ' from DB_INFO ';
          Q_CONSULTA_DB_SIZE: Result := 'SELECT DB_SIZE = CAST(SUM(size) * 8. / 1024 AS DECIMAL(8,2)) FROM sys.master_files WITH(NOWAIT) WHERE database_id = DB_ID() GROUP BY database_id';
          Q_CONSULTA_VERSION: Result := 'select GL_FORMULA as DB_VERSION from GLOBAL where GL_CODIGO = %d';


          {Q_COMPANYS_LIST_DB :  Result := 'select DISTINCT(CM_DATOS), CM_NOMBRE, CM_ALIAS, CM_USRNAME, CM_PASSWRD, CM_NIVEL0, CM_CODIGO '+
                                                ', DB_CODIGO ' +
                                                'from COMPANY '+
                                                'where ( %s ) and '+
                                                '( CM_DIGITO is NOT NULL ) ';}
          Q_COMPANYS_LIST_DB :  Result := 'select DISTINCT(DB_DATOS) AS CM_DATOS, DB_USRNAME AS CM_USRNAME, DB_PASSWRD AS CM_PASSWRD, 0 AS CM_NIVEL0, DB_CODIGO AS CM_CODIGO '+
                                                ' from DB_INFO '+
                                                'where ( %s ) ';
          Q_CONSULTA_DB_EMPLEADOS: Result := 'SELECT COUNT(*) AS EMPLEADOS FROM COLABORA WHERE CB_ACTIVO =''S'' ';

          Q_GET_EMPRESAS_SISTEMA: Result := 'select C.CM_CODIGO, C.CM_NOMBRE, C.CM_ACUMULA, C.CM_CONTROL, C.CM_EMPATE, '+
                                    'C.CM_ALIAS, C.CM_PASSWRD, C.CM_USRNAME, C.CM_USACAFE, C.CM_USACASE, C.CM_NIVEL0, C.CM_DATOS, C.CM_DIGITO, C.CM_KCLASIFI, ' +
                                    'C.CM_KCONFI, C.CM_KUSERS, C.CM_CTRL_RL '+
                                    ', C.DB_CODIGO ' +
                                    'from COMPANY C '+
                                    'where  CM_CODIGO <> %0:s  AND ( %1:s ) and ( ( %2:d = %3:d ) or ( C.CM_CODIGO in( select DISTINCT( CM_CODIGO ) '+
                                    'from ACCESO where ( AX_DERECHO > 0 ) and '+
                                    '( GR_CODIGO in ( select GR_HIJO from dbo.GET_DESCENDENCIA( %2:d ) ) ) '+
                                    ') ) ) order by C.CM_CODIGO';

          Q_GET_BASES_DATOS: Result:= 'SELECT NAME FROM SYS.DATABASES ' +
                          ' 	WHERE ' +
                          ' 		(name NOT IN (''master'', ''model'', ''msdb'', ''tempdb'', ''resource'', ''pubs'', ''Northwind'') ' +
                          ' 		and is_distributor = 0) ' +
                          // ' ORDER BY DATABASE_ID ';
                          ' ORDER BY NAME ';

          Q_GET_BASES_DATOS_SERVER: Result := 'select ''%s'' AS CM_DATOS, '' '' AS CM_NOMBRE, '' '' AS CM_ALIAS, ''%s'' AS CM_USRNAME, ''%s'' AS CM_PASSWRD, '' '' AS CM_NIVEL0,  ''DATOS'' AS CM_CODIGO '+
                                                ' from COMPANY ';

          Q_CONEXION_EMPRESA: Result := 'select top 1 ''%s'' AS CM_DATOS, '' '' AS CM_NOMBRE, '' '' AS CM_ALIAS, ''%s'' AS CM_USRNAME, ''%s'' AS CM_PASSWRD, '' '' AS CM_NIVEL0,  ''DATOS'' AS CM_CODIGO '+
                                                ' from COMPANY';

          Q_INSERT_DB : Result := 'insert into DB_INFO (DB_CODIGO, DB_DESCRIP, DB_CONTROL, DB_TIPO, DB_DATOS, DB_USRNAME, DB_PASSWRD, DB_CTRL_RL, DB_CHKSUM, DB_ESPC) values (''%s'', ''%s'', ''%s'', %d, ''%s'', ''%s'', ''%s'', ''%s'', ''%s'', ''%s'')';

          Q_UPDATE_DB: Result := 'update DB_INFO set DB_CODIGO = ''%s'', DB_DESCRIP = ''%s'', DB_CONTROL = ''%s'', DB_TIPO = ''%s'', DB_DATOS= ''%s'', DB_USRNAME=''%s'', DB_PASSWRD = ''%s'', DB_CTRL_RL = ''%s'', DB_CHKSUM = ''%s'', DB_ESPC = ''%s'' where DB_CODIGO = ''%s'' ';

          Q_EMPRESAS_CON_DB: Result := 'SELECT COUNT(CM_CODIGO) AS CANTIDAD FROM COMPANY WHERE DB_CODIGO = ''%s'' ';

          Q_GET_BASES_DATOS_EMPLEADOS: Result:= 'EXEC SP_BASES_DATOS_EMPLEADOS';

          Q_ES_BD_EMPLEADOS: Result := // 'SELECT COUNT(*) AS CANTIDAD FROM %s.INFORMATION_SCHEMA.TABLES ' +
                                          'SELECT * FROM %s.INFORMATION_SCHEMA.TABLES ' +
                                          ' WHERE TABLE_NAME = ''Colabora'' ';

          Q_ES_BD_SELECCION: Result :=    'SELECT * FROM %s.INFORMATION_SCHEMA.TABLES ' +
                                          ' WHERE TABLE_NAME = ''SOLICITA'' ';

          Q_GET_BASES_DATOS_SERVER2: Result := 'select DB_DATOS AS CM_DATOS, '' '' AS CM_NOMBRE, '' '' AS CM_ALIAS, DB_USRNAME AS CM_USRNAME, DB_PASSWRD AS CM_PASSWRD, '' '' AS CM_NIVEL0,  ''DATOS'' AS CM_CODIGO '+
                                                ' from DB_INFO WHERE DB_CODIGO = ''%s'' ';

          Q_CONSULTA_LOGICAL_NAME: Result := 'use [%s] SELECT name FROM sys.master_files WHERE database_id = db_id() AND type = %d';
          Q_BACKUP: Result := 'BACKUP DATABASE [%s] TO DISK = %s';
          Q_RESTORE: Result := 'RESTORE DATABASE [%s] FROM DISK = %s with MOVE %s to %s,  MOVE %s to %s ';
          Q_DEPURAR: Result := 'use [%s] DELETE FROM [%s]';
          Q_DB_SIZE: Result := 'use [%s] SELECT  CAST(SUM(size) AS DECIMAL) * 8.0 / 1024 as DBSize from sys.database_files';
          Q_LOG_SIZE: Result := 'SELECT (CAST(mflog.LogSize AS FLOAT)*8)/1024 LogSizeMB FROM sys.databases db ' +
          'LEFT JOIN (SELECT database_id, SUM(size) LogSize FROM sys.master_files WHERE type = 1 GROUP BY database_id, type) mflog ON mflog.database_id = db.database_id where DB_NAME(db.database_id) = ''%s'' ';
          Q_SHRINK : Result := 'dbcc shrinkdatabase ([%s])';


          Q_NO_ES_SELECCION_VISITANTES: Result := 'SELECT * FROM %s.INFORMATION_SCHEMA.TABLES ' +
                                          ' WHERE ' +
                                              ' TABLE_NAME = ''SOLICITA'' ' +
                                              ' OR ' +
                                              ' TABLE_NAME = ''LIBRO'' ' +
                                              ' OR ' +
                                              ' TABLE_NAME = ''COMPANY'' ' ;

          Q_NO_ES_TRESS_VISITANTES: Result := 'SELECT * FROM %s.INFORMATION_SCHEMA.TABLES ' +
                                          ' WHERE ' +
                                              ' TABLE_NAME = ''COLABORA'' ' +
                                              ' OR ' +
                                              ' TABLE_NAME = ''LIBRO'' ' +
                                              ' OR ' +
                                              ' TABLE_NAME = ''COMPANY'' ' ;

           K_QRY_CREA_BD_TRESS: Result := 'CREATE DATABASE [%0:s] ' +
                      ' ON ' +
                      ' ( NAME = ''%0:s_dat'', ' +
                      '   FILENAME = ''%1:s%0:s_dat.mdf'',' +
                      '   SIZE = %2:d, ' +
                      '   FILEGROWTH = %4:d ) ' +
                      ' LOG ON ' +
                      ' ( NAME = ''%0:s_log'',' +
                      '  FILENAME = ''%1:s%0:s_log.ldf'',' +
                      '  SIZE = %5:d, ' +
                      '  FILEGROWTH = %4:d )' +
                      ' COLLATE SQL_Latin1_General_CP1_CI_AS';

          Q_GET_TABLAS_BD: Result := 'select TABLE_NAME ' +
                                      ' from INFORMATION_SCHEMA.TABLES '+
                                      ' where TABLE_TYPE = ''BASE TABLE'' order by TABLE_NAME' ;

          Q_GET_CAMPOS_LLAVE: Result := 'SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE ' +
                                        ' WHERE ' +
                                          ' OBJECTPROPERTY(OBJECT_ID(constraint_name), ''IsPrimaryKey'') = 1 ' +
                                          ' AND table_name = ''%s'' ';

          Q_GET_CAMPOS_TABLA: Result := 'select NAME from syscolumns WHERE OBJECT_NAME(id) = ''%s'' and ' +
                                        'ISCOMPUTED = 0 and COLUMNPROPERTY(id, name, ''IsIdentity'') = 0';
          Q_GET_TABLA_EXPORTAR: Result := 'select %s from %s';

          Q_CONSULTA_ACTUALIZAR_BDS: Result := 'select ' +
            'cast (0 as Bit) as DB_APLICAPATCH, ' +
            'DB_CODIGO, ' +
            'DB_DESCRIP, ' +
            'DB_DATOS, ' +
            'DB_TIPO, ' +
            'DB_USRNAME, ' +
            'DB_PASSWRD, ' +
            '0 as DB_VERSION, ' +
            'DB_ESPC, ' +
            '0 as DB_APLICAVERSION, ' +
            'cast (0 as Bit) as DB_APLICADICCIONARIO, ' +
            'cast (0 as Bit) as DB_APLICAREPORTES, ' +
            'cast (0 as Bit) as DB_APLICAESPECIALES, ' +
            'cast ('''' as VARCHAR(250) ) as DB_STATUS, ' +
            '0 as DB_PROCESO, ' +
            'cast ('''' as TEXT) as DB_VERSIONESPATCH, ' +
            '0 as DB_AVANCE ' +
            'from DB_INFO WHERE DB_CODIGO <> ''%s'' ';

          Q_CANTIDAD_HUELLA_EMP: Result := 'SELECT COALESCE (COUNT(HU_ID), 0) AS CANTIDAD FROM VHUELLAGTI WHERE CB_CODIGO = %d AND IV_CODIGO = %d AND HU_ID != 0 AND CM_CODIGO = ''%s'' AND HU_INDICE <> 11'; // SYNERGY
          Q_EXPORTAR_CHECADAS_BIT_TERM_GTI: Result := 'SELECT WS_MIENT FROM WS_BITAC ' +
                                            ' WHERE WS_MIENT <> '''' ' +
                                            ' AND CONVERT(DATE, SUBSTRING (WS_MENSAJE, CHARINDEX (''EN'', UPPER(WS_MENSAJE)) + 3, 10), 103) ' +
                                            ' BETWEEN '' %s'' AND ''%s'' ';
          {$ifdef ANTES}
          Q_SIST_GET_INFO_ID_HUELLA: Result := 'select CM_DIGITO,CB_CODIGO from COMPANY,EMP_BIO where ( ID_NUMERO = %s )' ; // By (@DC)
          Q_SIST_GET_INFO_ID_PROX  : Result := 'select CM_DIGITO,CB_CODIGO from COMPANY,EMP_ID where ( ID_NUMERO = %s )' ; // By (@DC)
          {$else}
          Q_SIST_GET_INFO_ID_HUELLA: Result := 'select CM_CODIGO, CB_CODIGO from EMP_BIO where ( ID_NUMERO = %s )' ; // By (@DC)
          Q_SIST_GET_INFO_ID_PROX  : Result := 'select CM_CODIGO, CB_CODIGO from EMP_ID where ( ID_NUMERO =  %s )' ; // By (@DC)
          Q_SIST_GET_CM_DIGITO     : Result := 'select CM_DIGITO from COMPANY where ( CM_CODIGO = %s )';
          {$endif}
          Q_BORRA_TERM_HUELLA_TODAS: Result := 'delete from TERM_HUELLA %s' ; // SYNERGY
          Q_REINICIA_TERM_MSG_SYNC: Result:=  'update TERM_MSG set TM_NEXT=''18991230'' where ( DM_CODIGO = 8 ) %s'; // SYNERGY
          Q_SIST_ENVIO_EMAIL : Result := 'SELECT EE_FOLIO, EE_TO, EE_CC, EE_SUBJECT, CASE WHEN EE_SUBTYPE = 0 THEN EE_BODY ELSE '' '' end AS EE_BODY, EE_FROM_NA, EE_FROM_AD, EE_ERR_AD, EE_SUBTYPE, EE_STATUS, ' +
            'EE_ENVIADO, EE_MOD_ADJ, EE_FEC_IN, EE_FEC_OUT, EE_ATTACH, EE_BORRAR, EE_LOG FROM ENVIOEMAIL order by EE_FOLIO asc'; //US14322
          Q_SIST_TAREA_CALENDARIO : Result := 'SELECT CA_FOLIO, CA_NOMBRE, CA_DESCRIP, CA_ACTIVO, CM_CODIGO, CA_REPORT, CA_REPEMP, CA_FECHA, CA_HORA, ' +
                                  'CA_FREC, CA_RECUR, CA_DOWS, CA_MESES, CA_MESDIAS, CA_MES_ON, CA_MESWEEK, CA_FEC_ESP, CA_TSALIDA, CA_FSALIDA, CA_ULT_FEC, ' +
                                  'CA_NX_FEC, CA_NX_EVA, CA_US_CHG, CA_CAPTURA FROM CALENDARIO %s';
          Q_CALENDARIO_CAF: Result := 'SELECT CF_NOMBRE, DI_TIPO, CC_REG_ID, HORA, ACCION, CONTADOR, SEMANA, SYNC ' +
                                      'from CAF_CALEND WHERE CF_NOMBRE = ''%s'' AND DI_TIPO = %d order by DI_TIPO, CF_NOMBRE, CC_REG_ID asc';
          Q_CONFIGURACION_CAF: Result := 'SELECT CF_NOMBRE, DI_TIPO, CF_TIP_COM, CF_FOTO, CF_COM, CF_SIG_COM, CF_TECLADO, ' +
                                      'CF_DEF_COM, CF_DEF_CRE, CF_PRE_TIP, CF_PRE_QTY, CF_BANNER, ' +
                                      'CF_VELOCI, CF_T_ESP, CF_CALEND, CF_GAFETE, CF_CLAVE, CF_A_PRINT, CF_A_CANC, ' +
                                      'CF_TCOM_1, CF_TCOM_2, CF_TCOM_3, CF_TCOM_4, CF_TCOM_5, CF_TCOM_6, CF_TCOM_7, CF_TCOM_8, CF_TCOM_9, ' +
                                      'CF_REINICI, CF_CHECK_S, CF_SINC, CF_CAMBIOS ' +
                                      'FROM CAF_CONFIG WHERE DI_TIPO = 1 order by DI_TIPO, CF_NOMBRE asc';
          Q_LISTA_ESTACIONES : Result := 'SELECT DI_NOMBRE FROM DISPOSIT WHERE DI_TIPO = %d %s order by DI_NOMBRE';

          Q_SIST_TAREA_USUARIO : Result := 'SELECT US_CODIGO, CA_FOLIO, RU_ACTIVO, RU_VACIO, RU_US_CHG FROM ROLUSUARIO WHERE CA_FOLIO = %d';
          Q_GET_DISP: Result := 'SELECT *, CASE WHEN D.DI_TIPO = 1 AND CF_NOMBRE IS NOT NULL THEN 1 ELSE 2 END AS STATUS ' +
                             ' FROM DISPOSIT D LEFT JOIN CAF_CONFIG CF ON DI_NOMBRE = CF_NOMBRE AND D.DI_TIPO = CF.DI_TIPO WHERE %s ORDER BY D.DI_TIPO, DI_NOMBRE asc';

          Q_SIST_TAREA_ROLES : Result := 'SELECT RO_CODIGO, CA_FOLIO, RS_ACTIVO, RS_VACIO, RS_US_CHG FROM ROLSUSCRIP WHERE CA_FOLIO = %d';

          Q_BORRA_SIST_TAREA_ROLES : Result := 'DELETE FROM ROLSUSCRIP WHERE CA_FOLIO = %d';
          Q_INSERTAR_CONFIG: Result := 'INSERT INTO CAF_CONFIG '  +
             '(CF_NOMBRE, DI_TIPO, CF_TIP_COM, ' +
             'CF_A_PRINT, CF_A_CANC, CF_FOTO, CF_COM, CF_SIG_COM, CF_TECLADO, CF_DEF_COM, CF_DEF_CRE, ' +
             'CF_PRE_TIP, CF_PRE_QTY, CF_BANNER, CF_VELOCI, CF_T_ESP, CF_CHECK_S, ' +
             'CF_TCOM_1, CF_TCOM_2, CF_TCOM_3, CF_TCOM_4, CF_TCOM_5, CF_TCOM_6, CF_TCOM_7, CF_TCOM_8, CF_TCOM_9, ' +
             'CF_GAFETE, CF_CLAVE, CF_REINICI) VALUES ' +
             '(:CF_NOMBRE, :DI_TIPO, :CF_TIP_COM, ' +
             ':CF_A_PRINT, :CF_A_CANC, :CF_FOTO, :CF_COM, :CF_SIG_COM, :CF_TECLADO, :CF_DEF_COM, :CF_DEF_CRE, ' +
             ':CF_PRE_TIP, :CF_PRE_QTY, :CF_BANNER, :CF_VELOCI, :CF_T_ESP, :CF_CHECK_S, ' +
             ':CF_TCOM_1, :CF_TCOM_2, :CF_TCOM_3, :CF_TCOM_4, :CF_TCOM_5, :CF_TCOM_6, :CF_TCOM_7, :CF_TCOM_8, :CF_TCOM_9, ' +
             ':CF_GAFETE, :CF_CLAVE, :CF_REINICI)';

          Q_BORRA_SIST_TAREA_USUARIO : Result := 'DELETE FROM ROLUSUARIO WHERE CA_FOLIO = %d';
          Q_ACTUALIZAR_CONFIG: Result := 'UPDATE CAF_CONFIG '  +
             'SET CF_TIP_COM = :CF_TIP_COM, CF_A_PRINT = :CF_A_PRINT, CF_A_CANC = :CF_A_CANC,' +
             'CF_FOTO = :CF_FOTO, CF_COM = :CF_COM, CF_SIG_COM = :CF_SIG_COM, CF_TECLADO = :CF_TECLADO, CF_DEF_COM = :CF_DEF_COM, CF_DEF_CRE = :CF_DEF_CRE, ' +
             'CF_PRE_TIP = :CF_PRE_TIP, CF_PRE_QTY = :CF_PRE_QTY, CF_BANNER = :CF_BANNER, CF_VELOCI = :CF_VELOCI, CF_T_ESP = :CF_T_ESP, CF_CHECK_S = :CF_CHECK_S, ' +
             'CF_TCOM_1 = :CF_TCOM_1, CF_TCOM_2 = :CF_TCOM_2, CF_TCOM_3 = :CF_TCOM_3, CF_TCOM_4 = :CF_TCOM_4, CF_TCOM_5 = :CF_TCOM_5, CF_TCOM_6 = :CF_TCOM_6, ' +
             'CF_TCOM_7 = :CF_TCOM_7, CF_TCOM_8 = :CF_TCOM_8, CF_TCOM_9 = :CF_TCOM_9, CF_GAFETE = :CF_GAFETE, CF_CLAVE = :CF_CLAVE, CF_REINICI = :CF_REINICI ' +
             'WHERE CF_NOMBRE = :CF_NOMBRE AND DI_TIPO = :DI_TIPO';

          Q_INSERT_TAREA_CALENDARIO : Result := 'INSERT INTO CALENDARIO (CA_NOMBRE, CA_DESCRIP, CA_FREC, CA_RECUR, CM_CODIGO, CA_HORA, CA_FECHA, ' +
                                       'CA_DOWS, CA_MESES, CA_MESDIAS, CA_MESWEEK, CA_MES_ON, CA_US_CHG, CA_REPORT, CA_ACTIVO, ' +
                                       'CA_NX_FEC, CA_FSALIDA, CA_REPEMP, CA_TSALIDA, CA_FEC_ESP, CA_NX_EVA, CA_CAPTURA) ' +
                                       ' VALUES ( ' +
                                       ':CA_NOMBRE, :CA_DESCRIP, :CA_FREC, :CA_RECUR, :CM_CODIGO, :CA_HORA, :CA_FECHA, :CA_DOWS, :CA_MESES, :CA_MESDIAS, :CA_MESWEEK, ' +
                                       ':CA_MES_ON, :CA_US_CHG, :CA_REPORT, :CA_ACTIVO, :CA_NX_FEC, :CA_FSALIDA, :CA_REPEMP, :CA_TSALIDA, :CA_FEC_ESP, :CA_NX_EVA, :CA_CAPTURA )';
          Q_INSERTAR_CALEND: Result := 'INSERT INTO CAF_CALEND (CF_NOMBRE, DI_TIPO, HORA, ACCION, CONTADOR, SEMANA, SYNC) VALUES (:CF_NOMBRE, :DI_TIPO, :HORA, :ACCION, ' +
             ':CONTADOR, :SEMANA, :SYNC)';

          Q_SIST_UPD_TAREA_CALENDARIO : Result := 'UPDATE CALENDARIO SET CA_NOMBRE = :CA_NOMBRE, CA_DESCRIP = :CA_DESCRIP, CA_FREC = :CA_FREC, ' +
                                      'CA_RECUR = :CA_RECUR, CM_CODIGO = :CM_CODIGO, CA_HORA = :CA_HORA, CA_FECHA = :CA_FECHA, CA_DOWS = :CA_DOWS, ' +
                                      'CA_MESES = :CA_MESES, CA_MESDIAS = :CA_MESDIAS, CA_MESWEEK = :CA_MESWEEK, CA_MES_ON = :CA_MES_ON, ' +
                                      'CA_US_CHG = :CA_US_CHG, CA_REPORT = :CA_REPORT, CA_ACTIVO = :CA_ACTIVO, ' +
                                      'CA_NX_FEC = :CA_NX_FEC, CA_FSALIDA = :CA_FSALIDA, CA_REPEMP = :CA_REPEMP, CA_TSALIDA = :CA_TSALIDA, CA_FEC_ESP = :CA_FEC_ESP, CA_NX_EVA = :CA_NX_EVA, ' +
                                      'CA_CAPTURA = :CA_CAPTURA WHERE CA_FOLIO = :CA_FOLIO';
          Q_BORRAR_CALEND: Result := 'DELETE FROM CAF_CALEND WHERE CF_NOMBRE = ''%s'' AND DI_TIPO = %d';

          Q_SIST_GET_SUSCRIPCIONES_CALENDARIO_REP : Result := ' select row_number() over ( order by CA_FOLIO, RO_CODIGO ) ORDEN,  CA_FOLIO, CA_NOMBRE, CA_DESCRIP, CA_ACTIVO, CM_CODIGO,  CA_FREC, CA_HORA, CA_ULT_FEC, ' +
                                                              'CA_REPORT, VS_TIPO, RO_CODIGO, RO_NOMBRE, US_CODIGO, US_NOMBRE, US_EMAIL from V_SUSCRIP '+
                                                              'where CM_CODIGO = ''%s'' and CA_REPORT = %d';

          Q_SIST_UPDATE_USUARIO_CALENDARIO: Result := 'update ROLUSUARIO SET RU_ACTIVO = ''N'' where US_CODIGO = %d AND CA_FOLIO = %d';

          Q_SIST_UPDATE_ROL_CALENDARIO: Result := 'update ROLSUSCRIP SET RS_ACTIVO = ''N'' where RO_CODIGO = ''%s'' AND CA_FOLIO = %d';

          Q_GET_USUARIO_SUSCRIP_CALENDARIO : Result := 'select V_SUSC_CAL.CA_FOLIO, V_SUSC_CAL.CA_NOMBRE, REPORTE.RE_NOMBRE, V_SUSC_CAL.CA_DESCRIP, V_SUSC_CAL.CA_ACTIVO, V_SUSC_CAL.CM_CODIGO ' +
                                            ', V_SUSC_CAL.CA_FREC, V_SUSC_CAL.CA_HORA, V_SUSC_CAL.CA_ULT_FEC, V_SUSC_CAL.CA_REPORT  '+
                                            ', V_SUSC_CAL.RO_CODIGO, V_SUSC_CAL.RO_NOMBRE, V_SUSC_CAL.US_CODIGO, V_SUSC_CAL.US_NOMBRE, V_SUSC_CAL.US_EMAIL, 0 AS SU_VACIO  '+
                                            'from V_SUSC_CAL ' +
                                            'left outer join REPORTE ON REPORTE.RE_CODIGO = V_SUSC_CAL.CA_REPORT '+
                                            'where V_SUSC_CAL.%s = %d AND  V_SUSC_CAL.CM_CODIGO = ''%s''  ' +
                                            'order BY REPORTE.RE_NOMBRE';

          Q_BITACORA_REPORTES: Result := 'select B.BI_FOLIO, B.CA_FOLIO, COALESCE (C.CA_NOMBRE, ''[No definido]'') CA_NOMBRE, C.CM_CODIGO, C.CA_REPORT, C.CA_FREC, C.CA_ULT_FEC, B.BI_FECHA, B.BI_HORA, B.BI_TIPO, B.BI_TEXTO, B.BI_DATA, B.CAL_FOLIO ' +
                                ' from BITSERVREP B LEFT OUTER JOIN CALENDARIO C ON C.CA_FOLIO = B.CA_FOLIO WHERE ' +
                                ' ( %0:s >= %1:s and %0:s < %2:s ) and '+
                                ' ( ( %3:d = -1 ) or ( BI_TIPO = %3:d ) ) '+
                                ' order by BI_FECHA DESC, BI_HORA DESC';

          Q_BITACORA_REPORTES_FOLIO: Result := 'select B.BI_FOLIO, B.CA_FOLIO, COALESCE (C.CA_NOMBRE, ''[No definido]'') CA_NOMBRE, C.CM_CODIGO, C.CA_REPORT, C.CA_FREC, C.CA_ULT_FEC, B.BI_FECHA, B.BI_HORA, B.BI_TIPO, B.BI_TEXTO, B.BI_DATA, B.CAL_FOLIO ' +
                                ' from BITSERVREP B LEFT OUTER JOIN CALENDARIO C ON C.CA_FOLIO = B.CA_FOLIO WHERE ' +
                                ' ( B.CAL_FOLIO = %d) '+
                                ' order by BI_FECHA DESC, BI_HORA DESC';

          Q_BITACORA_CORREOS: Result := 'SELECT EE_FOLIO, EE_TO, EE_CC, EE_SUBJECT, CASE WHEN EE_SUBTYPE = 0 THEN EE_BODY ELSE SUBSTRING(EE_BODY,0,255) end AS EE_BODY, ' +
                                'EE_FROM_NA, EE_FROM_AD, EE_ERR_AD, EE_SUBTYPE, EE_STATUS, ' +
                                'EE_ENVIADO, EE_MOD_ADJ, EE_FEC_IN, EE_FEC_OUT, EE_ATTACH, EE_BORRAR, EE_LOG ' +
                                ' from ENVIOEMAIL WHERE ' +
                                ' ( %0:s >= %1:s and %0:s < %2:s ) and '+
                                ' ( ( %3:d = -1 ) or ( EE_STATUS = %3:d ) ) '+
                                ' order by EE_FEC_IN DESC';


          Q_SP_AGREGA_CALENDARIO : Result := '{CALL SP_AGREGAR_CALENDARIO (:CA_NOMBRE, :CM_CODIGO, :CA_DESCRIP, :CA_ACTIVO' +
                                 ', :CA_FREC, :CA_RECUR, :CA_HORA, :CA_FECHA, :CA_DOWS, :CA_MESES, :CA_MESDIAS, :CA_MESWEEK ' +
                                 ', :CA_MES_ON, :CA_US_CHG, :CA_REPORT, :CA_NX_FEC, :CA_FSALIDA, :CA_REPEMP ' +
                                 ', :CA_TSALIDA, :CA_FEC_ESP, :CA_NX_EVA, :CA_FOLIO, :SuscribirUsuario, :CA_CAPTURA ) }';

          Q_LEER_SUSCRIP: Result := 'SELECT RE_CODIGO, US_CODIGO, SU_FRECUEN, SU_ENVIAR, SU_VACIO FROM SUSCRIP ';

          Q_SIST_TAREA_CALENDARIO_EMPRESA : Result := 'SELECT CA_FOLIO, CA_NOMBRE, CA_DESCRIP, CA_ACTIVO, CM_CODIGO, CA_REPORT, RE_NOMBRE, CA_REPEMP, CA_FECHA, CA_HORA, ' +
                                  'CA_FREC, CA_RECUR, CA_DOWS, CA_MESES, CA_MESDIAS, CA_MES_ON, CA_MESWEEK, CA_FEC_ESP, CA_TSALIDA, CA_FSALIDA, CA_ULT_FEC, ' +
                                  'CA_NX_FEC, CA_NX_EVA, CA_US_CHG, CA_CAPTURA FROM V_CALENDAR ' +
                                  ', REPORTE WHERE CA_REPORT = RE_CODIGO ' +
                                  ' %s ';

          Q_INSERT_ROLUSUARIO: Result := 'INSERT INTO ROLUSUARIO ' +
                                         ' (US_CODIGO, CA_FOLIO, RU_ACTIVO, RU_VACIO, RU_US_CHG) ' +
                                         ' VALUES (:US_CODIGO, :CA_FOLIO, :RU_ACTIVO, :RU_VACIO, :RU_US_CHG) ';
          Q_UPDATE_CALENDARIO_DESDE_SUSC: Result := 'UPDATE CALENDARIO SET CA_NOMBRE = CA_NOMBRE +  '' [Reporte: %d]'', CA_REPORT = %d WHERE CA_REPORT = 0 ';
          Q_INSERT_CALENDARIO_DESDE_SUSC: Result := 'INSERT INTO CALENDARIO ' +
                    ' (CA_NOMBRE,CA_DESCRIP,CA_ACTIVO,CM_CODIGO,CA_REPORT,CA_REPEMP,CA_FECHA,CA_HORA ' +
                    ' ,CA_FREC,CA_RECUR,CA_DOWS,CA_MESES,CA_MESDIAS,CA_MES_ON,CA_MESWEEK,CA_FEC_ESP,CA_TSALIDA ' +
                    ' ,CA_FSALIDA,CA_ULT_FEC,CA_NX_FEC,CA_NX_EVA,CA_US_CHG,CA_CAPTURA) ' +
                    ' SELECT %s,CA_DESCRIP,CA_ACTIVO,CM_CODIGO,0,CA_REPEMP,CA_FECHA,CA_HORA ' +
                    ' ,CA_FREC,CA_RECUR,CA_DOWS,CA_MESES,CA_MESDIAS,CA_MES_ON,CA_MESWEEK,CA_FEC_ESP,CA_TSALIDA ' +
                    ' ,CA_FSALIDA,CA_ULT_FEC,CA_NX_FEC,CA_NX_EVA,CA_US_CHG,CA_CAPTURA FROM CALENDARIO WHERE CA_FOLIO = %d';
          Q_LEE_CALENDARIO: Result := 'SELECT CA_NOMBRE FROM CALENDARIO WHERE CA_FOLIO = %d';

          Q_SOLICITUD_ENVIOS: Result := 'SELECT CAL_FOLIO, CA_FOLIO, RE_CODIGO, CAL_FECHA, CAL_INICIO, CAL_FIN, CAL_ESTATUS, CAL_MENSAJ' +
                                ' from CALENSOLI WHERE ' +
                                ' ( %0:s >= %1:s and %0:s < %2:s ) and '+
                                ' ( ( %3:d = -1 ) or ( CAL_ESTATUS = %3:d ) ) '+
                                ' order by CAL_FECHA DESC';

          Q_INSERT_CALENSOLI : Result := 'INSERT INTO CALENSOLI (CA_FOLIO, RE_CODIGO, CAL_FECHA, CAL_ESTATUS) VALUES ' +
                             '(:CA_FOLIO, :RE_CODIGO, :CAL_FECHA, :CAL_ESTATUS)';

          Q_LEER_CALENSOLI : Result := 'SELECT CA_FOLIO FROM CALENSOLI WHERE CA_FOLIO = %d AND CAL_ESTATUS = %d';

          Q_UPDATE_CALENSOLI : Result := 'UPDATE CALENSOLI SET CAL_ESTATUS = :CAL_ESTATUS WHERE CAL_FOLIO = :CAL_FOLIO';
          Q_GET_STATUS_EMPLEADO: Result := 'select CB_ACTIVO, CB_FEC_BAJ from VEMPL_BAJA where ( CM_CODIGO = %s ) and ( CB_CODIGO = %d )';
          Q_INICIAR_KARDEX_CONCEPTOS: Result := 'execute procedure SP_INICIAR_KARDEX_CONCEPTOS';
          Q_TERMINALES_STATUS: Result := 'select * from FN_TERMINAL_STATUS(%s)';
          Q_REINICIA_TERM_MSG_DIAGNOSTICO: Result := 'update TERM_MSG set TM_NEXT=''18991230'' where ( DM_CODIGO = 13 )';
          Q_GET_EMPRESAS_ACCESOS_GRUPO_USUARIOS: Result := 'exec dbo.SP_GET_EMPRESAS_ACCESOS %d, %s, %d, %s, %s';

     else
         Result := '';
     end;
end;

procedure TdmServerSistema.SetTablaInfo( const eTabla: ETipoSistema );
begin
     with oZetaProvider.TablaInfo do
     begin
          case eTabla of
              eArbol: SetInfo( 'ARBOL',
                               'US_CODIGO, AB_ORDEN, AB_NIVEL, AB_NODO, AB_DESCRIP, AB_MODULO',
                               'US_CODIGO, AB_ORDEN, AB_MODULO' );
              eEmpresas: SetInfo( 'COMPANY',
                                  'CM_CODIGO, CM_NOMBRE, CM_ACUMULA, CM_CONTROL, CM_EMPATE, CM_ALIAS, CM_PASSWRD, CM_USRNAME, CM_USACAFE, CM_USACASE, CM_NIVEL0, CM_DATOS, CM_DIGITO, CM_KCLASIFI, CM_KCONFI, CM_KUSERS, DB_CODIGO',
                                  'CM_CODIGO' );
              eGruposUsuarios: SetInfo( 'GRUPO',
                                        'GR_CODIGO, GR_DESCRIP, GR_PADRE',
                                        'GR_CODIGO' );
              eUsuarios: SetInfo( 'USUARIO',
                                  'US_CODIGO, US_CORTO, GR_CODIGO, US_NIVEL, US_NOMBRE, US_PASSWRD, US_BLOQUEA, US_CAMBIA, US_FEC_IN, US_FEC_OUT, US_FEC_SUS, US_DENTRO, US_ARBOL, US_FORMA, US_BIT_LST, US_FEC_PWD, US_EMAIL, US_FORMATO, US_LUGAR, US_MAQUINA, CM_CODIGO, CB_CODIGO, ' +
                                  'US_DOMAIN, US_PORTAL, US_ACTIVO, US_JEFE ,US_CLASICA',
                                  'US_CODIGO' );
              ePoll: Setinfo( 'POLL',
                              'PO_LINX, PO_EMPRESA, PO_NUMERO, PO_FECHA, PO_HORA, PO_LETRA',
                              'PO_EMPRESA, PO_NUMERO, PO_FECHA, PO_HORA' );
              eImpresoras: SetInfo( 'PRINTER',
                                    'PI_NOMBRE, PI_EJECT, PI_CHAR_10, PI_CHAR_12, PI_CHAR_17, PI_EXTRA_1, PI_EXTRA_2, PI_RESET, PI_UNDE_ON, PI_UNDE_OF, PI_BOLD_ON, PI_BOLD_OF, PI_6_LINES, PI_8_LINES, PI_ITAL_ON, PI_ITAL_OF, PI_LANDSCA',
                                    'PI_NOMBRE' );
              eAccesos: SetInfo( 'ACCESO',
                                 'GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO',
                                 'GR_CODIGO, CM_CODIGO, AX_NUMERO' );
              eUserSuper: SetInfo( 'SUPER',
                                   'US_CODIGO, CB_NIVEL',
                                   'US_CODIGO, CB_NIVEL' );
              eNivel0 : SetInfo( 'NIVEL0',
                                 'TB_CODIGO, TB_ELEMENT',
                                 'TB_CODIGO' );
              eSuscrip: SetInfo( 'SUSCRIP',
                                 'US_CODIGO, RE_CODIGO, SU_FRECUEN, SU_ENVIAR, SU_VACIO',
                                 'US_CODIGO, RE_CODIGO' );
              eUsuarioAreas: SetInfo( 'SUP_AREA',
                                      'US_CODIGO, CB_AREA',
                                      'US_CODIGO, CB_AREA' );
              eGruposAdic: SetInfo( 'GRUPO_AD',
                                    'GX_CODIGO, GX_TITULO, GX_POSICIO',
                                    'GX_CODIGO' );
              eCamposAdic: SetInfo( 'CAMPO_AD',
                                    'CX_NOMBRE,GX_CODIGO,CX_POSICIO,CX_TITULO,CX_TIPO,CX_DEFAULT,CX_MOSTRAR,CX_ENTIDAD,CX_OBLIGA',
                                    'CX_NOMBRE' );
              eRoles: SetInfo(    'ROL',
                                  'RO_CODIGO, RO_NOMBRE, RO_DESCRIP, RO_NIVEL ',
                                  'RO_CODIGO' );
              eUserRol: SetInfo(  'USER_ROL',
                                  'RO_CODIGO, US_CODIGO, UR_PPAL',
                                  'RO_CODIGO, US_CODIGO' );
              eAccesosAdicionales: SetInfo(  'GR_AD_ACC',
                                             'CM_CODIGO, GR_CODIGO, GX_CODIGO, GX_DERECHO',
                                             'CM_CODIGO, GR_CODIGO, GX_CODIGO' );
              eSistLstDispositivos: SetInfo( 'DISPOSIT',
                                             'DI_NOMBRE,DI_TIPO,DI_DESCRIP,DI_NOTA,DI_IP',
                                             'DI_NOMBRE,DI_TIPO');

              eConfigCafeteria: SetInfo( 'CAF_CONFIG',
                                         'CF_NOMBRE, DI_TIPO, CF_TIP_COM, CF_FOTO, CF_COM, CF_SIG_COM, CF_TECLADO, ' +
                                         'CF_DEF_COM, CF_DEF_CRE, CF_PRE_TIP, CF_PRE_QTY, CF_BANNER, CF_VELOCI, ' +
                                         'CF_T_ESP, CF_CALEND, ' +
                                         'CF_GAFETE, CF_CLAVE, CF_A_PRINT, CF_A_CANC, ' +
                                         'CF_TCOM_1, CF_TCOM_2, CF_TCOM_3, CF_TCOM_4, CF_TCOM_5, ' +
                                         'CF_TCOM_6, CF_TCOM_7, CF_TCOM_8, CF_TCOM_9, CF_REINICI, CF_CHECK_S, ' +
                                         'CF_SINC, CF_CAMBIOS',
                                         'CF_NOMBRE, DI_TIPO');

              eCalendCafeteria: SetInfo( 'CAF_CALEND',
                                         'CF_NOMBRE, DI_TIPO, CC_REG_ID, HORA, ACCION, CONTADOR, SEMANA, SYNC',
                                         'CF_NOMBRE, DI_TIPO, CC_REG_ID');

              eDisxCom: SetInfo( 'DISXCOM',
                                 'CM_CODIGO,DI_NOMBRE,DI_TIPO',
                                 'CM_CODIGO,DI_NOMBRE,DI_TIPO' );
              //eAccArbol: SetInfo('ACC_ARBOL', 'AA_SOURCE,AX_NUMERO,AA_DESCRI,AA_INGLES,AA_VERSIO,AA_POSICI,AA_MODULO,AA_SCREEN', 'AA_SOURCE,AX_NUMERO' );
              eUsuarioCCosto: SetInfo( 'SUP_COSTEO',
                                       'CB_NIVEL, US_CODIGO',
                                       'CB_NIVEL, US_CODIGO' );

              eBaseDatos:
              begin
                  Setinfo( 'DB_INFO', 'DB_CODIGO,DB_DESCRIP,DB_CONTROL,DB_TIPO,DB_DATOS,DB_USRNAME,DB_PASSWRD,DB_CTRL_RL,DB_CHKSUM,DB_ESPC', 'DB_CODIGO');
                  // SetOrder( 'DB_CODIGO' );
              end;

              {$ifndef DOS_CAPAS}
              eSistListaGrupos:
              begin
                   SetInfo( 'GRUPOTERM',
                                         'GP_CODIGO, GP_DESCRIP, CM_CODIGO',
                                        'GP_CODIGO' ); // SYNERGY
                   AfterUpdateRecord := SetAfterUpdateGruposDisp;
              end;
              eTermPorGrupo:
              begin
                   SetInfo( 'TERM_GRUPO',
                                      'TE_NOMBRE, GP_CODIGO',
                                      'TE_NOMBRE, GP_CODIGO' ); // SYNERGY
              end;
              eMensajes: // SYNERGY
              begin
                   SetInfo( 'MENSAJE',
                            'DM_CODIGO, DM_MENSAJE, DM_ORDEN, DM_SLEEP, DM_ACTIVO',
                            'DM_CODIGO' );
                   SetOrder( 'DM_CODIGO' );
              end;
              eMensajesTerm: // SYNERGY
              begin
                   SetInfo( 'TERM_MSG',
                            'DM_CODIGO, TE_CODIGO, TM_SLEEP, TM_NEXT,TM_ULTIMA,TM_NEXTXHR,TM_NEXTHOR',
                            'DM_CODIGO, TE_CODIGO');
                   SetOrder( 'TE_CODIGO, DM_CODIGO' );
                   AfterUpdateRecord := SetAfterUpdateMensajesPorTerminal;
              end;
              eTerminales: // SYNERGY
              begin
                   SetInfo( 'TERMINAL',
                            'TE_CODIGO, TE_NOMBRE, TE_NUMERO, TE_TEXTO, TE_MAC, TE_APP, TE_BEAT, TE_ZONA,TE_ZONA_ES, TE_ACTIVO, TE_XML, TE_VERSION, TE_IP, TE_TER_HOR, TE_HUELLAS, TE_ID',
                            'TE_CODIGO' );
                   SetOrder( 'TE_CODIGO' );
                   AfterUpdateRecord := SetAfterUpdateTerminales;
              end;
				{$ifdef SISNOM_CLAVES}
               eUsuariosSeguridad: SetInfo( 'USUARIO',
                                            'US_CODIGO, US_CORTO, GR_CODIGO, US_NIVEL, US_NOMBRE, US_PASSWRD, US_BLOQUEA, US_CAMBIA, US_FEC_IN, US_FEC_OUT, US_FEC_SUS, US_DENTRO, US_ARBOL, US_FORMA, US_BIT_LST, US_FEC_PWD, US_FORMATO, US_LUGAR, CM_CODIGO, CB_CODIGO, ' +
                                            'US_DOMAIN, US_PORTAL, US_ACTIVO, US_JEFE ','US_CODIGO' );
				{$endif}
              {$endif}

              eSistTareaCalendario: SetInfo( 'CALENDARIO',
                                    'CA_FOLIO, CA_NOMBRE, CA_DESCRIP, CA_ACTIVO, CM_CODIGO, CA_REPORT, CA_REPEMP, CA_FECHA, CA_HORA, CA_FREC, CA_RECUR, CA_DOWS, CA_MESES, CA_MESDIAS, CA_MES_ON, CA_FEC_ESP, CA_TSALIDA, CA_FSALIDA, CA_NX_FEC, CA_NX_EVA, CA_US_CHG',
                                    'CA_FOLIO, CA_CAPTURA');

              eSistTareaUsuario: SetInfo( 'ROLUSUARIO',
                                    'US_CODIGO, CA_FOLIO, RU_ACTIVO, RU_VACIO, RU_US_CHG',
                                    'US_CODIGO, CA_FOLIO');

              eSistTareaRoles: SetInfo( 'ROLSUSCRIP',
                                    'RO_CODIGO, CA_FOLIO, RS_ACTIVO, RS_VACIO, RS_US_CHG',
                                    'RO_CODIGO, CA_FOLIO');

          end;
     end;
end;

function TdmServerSistema.DecodificaClaves( Datos: OleVariant; const sCampo: String ): OleVariant;
begin
     with cdsLista do
     begin
          Active := False;
          Data := Datos;
          while not Eof do
          begin
               try
                  Edit;
                  with FieldByName( sCampo ) do
                  begin
                       AsString := ZetaServerTools.Decrypt( AsString );
                  end;
                  Post;
               except
                     on Error: Exception do
                     begin
                          Cancel;
                     end;
               end;
               Next;
          end;
          MergeChangeLog;
          Result := Data;
          Active := False;
     end;
end;

procedure TdmServerSistema.Codifica( const sCampo: String; DeltaDS: TzProviderClientDataSet );
begin
     with DeltaDS do
     begin
          Edit;
          FieldByName( sCampo ).AsString := ZetaServerTools.Encrypt( ZetaServerTools.CampoAsVar( FieldByName( sCampo ) ) );
          Post;
     end;
end;

function TdmServerSistema.LeerClaves( const iNumero: Integer ): String;
var
   FDataset: TZetaCursor;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          FDataset := CreateQuery( Format( GetScript( Q_CLAVES_LEER ), [ iNumero ] ) );
          try
             with FDataset do
             begin
                  Active := True;
                  if EOF then
                     Result := ''
                  else
                      Result := FieldByName( 'CL_TEXTO' ).AsString;
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataset );
          end;
     end;
end;

procedure TdmServerSistema.SetFiltroGrupos( const iGrupo: integer );
var
   sFiltro: string;
begin
     if ( iGrupo <> ZetaCommonClasses.D_GRUPO_SIN_RESTRICCION ) then
     begin
          {$ifdef INTERBASE}
          sFiltro := Format( '( GR_CODIGO in ( select GR_HIJO from GET_DESCENDENCIA( %d ) ) )', [ iGrupo ] );
          {$endif}
          {$ifdef MSSQL}
          sFiltro := Format( '( GR_CODIGO in ( select GR_HIJO from dbo.GET_DESCENDENCIA( %d ) ) )', [ iGrupo ] );
          {$endif}
          oZetaProvider.TablaInfo.Filtro := sFiltro;
     end;
end;
{
function TdmServerSistema.ChecaUsuarios( const sNombreCorto: string; const iCodigo: integer; var sMensaje: string ): boolean;
const
     K_MENSAJE = '� Nombre de usuario ya existe !';
var
   sNombre: string;
   FUsers: TZetaCursor;
   iUsuario: integer;

begin
     Result := True;
     with oZetaProvider do
     begin
          FUsers := CreateQuery( GetScript( Q_CHECAUSUARIOS ) );
          try
             with FUsers do
             begin
                  Active := True;
                  while not EOF do
                  begin
                       iUsuario := FieldByName( 'US_CODIGO' ).AsInteger;
                       sNombre := UpperCase( FieldByName( 'US_CORTO' ).AsString );
                       if iCodigo <> iUsuario then
                       begin
                            if sNombreCorto = sNombre then
                            begin
                                 Result := False;
                                 sMensaje := K_MENSAJE;
                                 Break;
                            end;
                       end;
                       Next;
                  end;
                  Active := False;
             end;
          finally
                 FreeAndNil( FUsers );
          end;
     end;
end;
}

function TdmServerSistema.BuildEmpresa(DataSet: TZetaCursor): OleVariant;
     { Es una copia de DBaseCliente.BuildEmpresa }
     {
     Las posiciones de este arreglo se encuentran en ZetaCommonClasses:

     P_ALIAS = 0;
     P_DATABASE = 0;
     P_USER_NAME = 1;
     P_PASSWORD = 2;
     P_USUARIO = 3;
     P_NIVEL_0 = 4;
     P_CODIGO = 5;
     }
begin
     with DataSet do
     begin
          Result := VarArrayOf( [ FieldByName( 'CM_DATOS' ).AsString,
                                  FieldByName( 'CM_USRNAME' ).AsString,
                                  FieldByName( 'CM_PASSWRD' ).AsString,
                                  0,
                                  FieldByName( 'CM_NIVEL0' ).AsString,
                                  FieldByName( 'CM_CODIGO' ).AsString ] );
     end;
end;

{ ************ Interfases ( Paletitas ) ****************** }

{$IFDEF ANTES}
// AP,CV: 30/Mar/2007 Este c�digo no es utilizado en otro lado
function TdmServerSistema.GetBitacora( Empresa: OleVariant; FechaInicial, FechaFinal: TDateTime; TipoBitacora, Empleado, Usuario: Integer): OleVariant;
var
   sScript, sEmpleado, sBitacora, sUsuario: String;
begin
     if ( TipoBitacora < 0 ) then
        sBitacora := ''
     else
         sBitacora := ' and ( BI_TIPO = :TipoBitacora ) ';
     if ( Empleado = 0 ) then
        sEmpleado := ''
     else
         sEmpleado := ' and ( CB_CODIGO = :EmpleadoNumero ) ';
     if ( Usuario = 0 ) then
        sUsuario := ''
     else
         sUsuario := ' AND ( US_CODIGO = :Usuario ) ';
     sScript := StrTransform( StrTransform( StrTransform(
                      Format( GetScript( Q_SIST_BITACORA ), [DateToStrSQL( FechaInicial ), DateToStrSQL( FechaFinal )]),
                      '@BITACORA', sBitacora ),
                      '@EMPLEADO', sEmpleado ),
                      '@USUARIO', sUsuario );
     with oZetaProvider do
     begin
          Result:= OpenSQL( Comparte, sScript, True );
     end;
     SetComplete;
end;

{$ENDIF}
function TdmServerSistema.GetEmpresas( Tipo, GrupoActivo: Integer ): OleVariant;
begin
     with oZetaProvider do
     begin
          Result:= DecodificaClaves( OpenSQL( Comparte, Format( GetScript( Q_GET_EMPRESAS ), [ GetTipoCompany( Tipo ), GrupoActivo, ZetaCommonClasses.D_GRUPO_SIN_RESTRICCION ] ), True ), K_CLAVE_EMPRESA )
     end;
     SetComplete;
end;

function TdmServerSistema.GetEmpresasAccesos(Tipo, Grupo: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          Result:= OpenSQL( Comparte, Format( GetScript( Q_GET_EMPRESAS_ACCESOS ), [ Grupo, GetTipoCompanyDerechos( Tipo ) ] ), True );
     end;
     SetComplete;
end;

function TdmServerSistema.GetGrupos(Grupo: Integer): OleVariant;
begin
     SetTablaInfo( eGruposUsuarios );
     SetFiltroGrupos( Grupo );
     with oZetaProvider do
     begin
          Result := GetTabla( Comparte );
     end;
     SetComplete;
end;

function TdmServerSistema.GrabaEmpresas(oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     //FCampo := K_CLAVE_EMPRESA;
     SetTablaInfo( eEmpresas );
     with oZetaProvider do
     begin

          with TablaInfo do
          begin
               BeforeUpdateRecord := CodificaClaveEmpresa;
               AfterUpdateRecord := ActualizaDerechos;
          end;
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );

          try
            ActualizaCodigoEmpresa(FCM_CODIGOAnt,FCM_CODIGONuevo );
          except

          end;
     end;
     SetComplete;
end;

procedure TdmServerSistema.ActualizaCodigoEmpresa(const CodigoAnterior,NuevoCodigo:string);
var
   FDataset: TZetaCursor;
   oZetaProviderCompany: TdmZetaServerProvider;
   FEmpresasLog:string;

    procedure CambiarCodigoGrupoEmpresa(const sTabla:string);
    begin
        try
           with oZetaProviderCompany do
           begin
                 EmpiezaTransaccion;
                 if FModoGrupos = ukModify then
                    FDataSet := CreateQuery( Format( GetScript( Q_ACTUALIZA_EMPRESA_DER ),[sTabla,NuevoCodigo,CodigoAnterior ]))
                 else if FModoGrupos = ukDelete then
                      FDataSet := CreateQuery( Format( GetScript( Q_BORRA_EMPRESA_DER ),[sTabla,CodigoAnterior ]))
                 else
                   FDataSet := Nil;
                 try
                    Ejecuta( FDataSet );
                 finally
                        TerminaTransaccion(True);
                        FreeAndNil( FDataSet );
                 end;
           end;
        except
              FEmpresasLog := ConcatString(FEmpresasLog,FDataSetEmpresa.FieldByName('CM_CODIGO').AsString,',');
        end;
    end;

   {Actualizar empresa}
   procedure ActualizarEmpresa;
   var
      bConexion:Boolean;
   begin
        oZetaProviderCompany := TdmZetaServerProvider.Create( nil );
        try
           with oZetaProviderCompany do
           begin
                 //Conexion a la empresa
                 try
                    EmpresaActiva := BuildEmpresa(FDataSetEmpresa);
                    bConexion := True;
                 except
                       bConexion := False;
                 end;
                 if( bConexion )then
                 begin
                      {$ifdef MSSQL}
                      //Checar las tablas creadas
                      CambiarCodigoGrupoEmpresa('R_ENT_ACC');
                      CambiarCodigoGrupoEmpresa('R_CLAS_ACC');
                      {$endif}
                      CambiarCodigoGrupoEmpresa('GR_AD_ACC');
                 end;
           end;
        Finally
               FreeAndNil( oZetaProviderCompany );
        end;
   end;

   procedure SetEmpresa;
   begin
        with oZetaProviderComparte do
        begin
             FDataSetEmpresa := CreateQuery( Format( GetScript( Q_COMPANY ), [ FCM_CODIGONuevo ] ) );
             FDataSetEmpresa.Active := True;
        end;
   end;

begin
      try
         FEmpresasLog := VACIO;
         with FDataSetEmpresa do
         begin
              SetEmpresa;
              ActualizarEmpresa;
              Active := False;
         end;
      finally
             FreeAndNil( FDataSetEmpresa );
      end;
end;

procedure TdmServerSistema.CodificaClaveEmpresa(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
// RCM 20/11/2013 Al parecer se movi� la funcionalidad a ActualizaCodigoEmpresa, la creacion
//  del FDataSetEmpresa no tiene sentido aqui, comentado el bloque with

//     with oZetaProviderComparte do
//     begin
//          EmpresaActiva := Comparte;
//          FDataSetEmpresa := CreateQuery( Format( GetScript( Q_COMPANY ), [ CampoAsVar(DeltaDS.FieldByName( 'CM_CODIGO' )) ] ) );
//          FDataSetEmpresa.Active := True;
//     end;
     if ( UpdateKind in [ ukModify, ukInsert ] ) then
        Codifica( K_CLAVE_EMPRESA, DeltaDS );
     Applied := False;

end;

procedure TdmServerSistema.ActualizaDerechos(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind );
begin
     FModoGrupos := UpdateKind;
     with DeltaDS.FieldByName( 'CM_CODIGO' ) do
     begin
          FCM_CODIGOAnt := OldValue;
          FCM_CODIGONuevo := NewValue;
     end;
end;

function TdmServerSistema.GrabaGrupos(oDelta: OleVariant; iUsuario: Integer; out ErrorCount: Integer): OleVariant;
begin
     FUsuario:= iUsuario;
     FTipoSistema:= eGruposUsuarios;
     SetTablaInfo( eGruposUsuarios );
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          TablaInfo.AfterUpdateRecord := AfterUpdateGrupos;
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
          ActualizarEmpresasGrupo(iUsuario);
     end;
     SetComplete;
end;

procedure TdmServerSistema.ActualizarEmpresasGrupo(const iUsuario:Integer);
var
   FDataset: TZetaCursor;
   FDataSetComparte:TZetaCursor;
   oZetaProviderCompany: TdmZetaServerProvider;
   FEmpresasLog:string;

   {Actualizar el codigo de empresa en cada tabla de accesos }
   procedure CambiarCodigoGrupoEmpresa(const CodigoAnterior,NuevoCodigo:Integer;const sTabla:string);
   begin
        try
           with oZetaProviderCompany do
           begin
                 EmpiezaTransaccion;
                 if FModoGrupos = ukModify then
                    FDataSet := CreateQuery( Format( GetScript( Q_ACTUALIZA_CODIGO_EMPRESA ),[sTabla,NuevoCodigo,CodigoAnterior ]))
                 else if FModoGrupos = ukDelete then
                      FDataSet := CreateQuery( Format( GetScript( Q_BORRA_CODIGO_EMPRESA ),[sTabla,CodigoAnterior ]))
                 else
                     FDataSet := Nil;
                 try
                    Ejecuta( FDataSet );
                 finally
                        TerminaTransaccion(True);
                        FreeAndNil( FDataSet );
                 end;
           end;
        except
              FEmpresasLog := ConcatString(FEmpresasLog,FDataSetComparte.FieldByName('CM_CODIGO').AsString,',');
        end;
   end;

   {Actualizar empresa}
   procedure ActualizarEmpresa(const CodigoAnterior,NuevoCodigo:Integer);
   var
      bConexion:Boolean;
   begin
        oZetaProviderCompany := TdmZetaServerProvider.Create( nil );
        try
           with oZetaProviderCompany do
           begin
                 //Conexion ala empresa
                 try
                    EmpresaActiva := BuildEmpresa(FDataSetComparte);
                    bConexion := True;
                 except
                       bConexion := False;
                 end;
                 if( bConexion )then
                 begin
                      {$ifdef MSSQL}
                      //Checar las tablas creadas
                      CambiarCodigoGrupoEmpresa(CodigoAnterior,NuevoCodigo,'R_ENT_ACC');
                      CambiarCodigoGrupoEmpresa(CodigoAnterior,NuevoCodigo,'R_CLAS_ACC');
                      {$endif}
                      CambiarCodigoGrupoEmpresa(CodigoAnterior,NuevoCodigo,'GR_AD_ACC');
                 end;
           end;
        Finally
               FreeAndNil( oZetaProviderCompany );
        end;
   end;

   {Obtener la lista de empresas y Actualizar el codigo para cada Tabla}
   procedure ActualizarCodigoGrupo(const NuevoCodigo,CodigoAnterior:Integer );
   begin
        with oZetaProviderComparte do
        begin
             EmpresaActiva := Comparte;
             FDataSetComparte := CreateQuery( Format( GetScript( Q_COMPANYS_LIST_DISTINCT ), [ GetTipoCompany( Ord( tc3Datos ) )] ) );
              try
                 FEmpresasLog := VACIO;
                 with FDataSetComparte do
                 begin
                      Active := True;
                      First;
                      while not Eof do
                      begin
                           ActualizarEmpresa( CodigoAnterior,NuevoCodigo );
                           Next;
                      end;
                      Active := False;
                 end;
              finally
                     FreeAndNil( FDataSetComparte );
              end;
        end;
   end;

begin
     if ( FModoGrupos = ukModify ) then
     begin
          ActualizarCodigoGrupo( FCodigoNuevo ,FCodigoAnterior );
     end;
     if ( FModoGrupos = ukDelete ) then
     begin
          ActualizarCodigoGrupo( 0,FCodigoAnterior );
     end;
     //Grabar a bitacora aquellas empresas en las que no se actualizaron los grupos � ocurri� una excepci�n
     if StrLleno(FEmpresasLog)then
     begin
          oZetaProviderComparte.EscribeBitacoraComparte(tbAdvertencia,clbDerechosAcceso,Now,'Excepci�n al Modificar Grupo de Acceso ','Ocurri� una excepci�n al grabar Accesos en las siguientes empresas: '+FEmpresasLog ,iUsuario);
     end;
end;


procedure TdmServerSistema.AfterUpdateGrupos(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind );
var
   FGrupos: TZetaCursor;
begin
     FModoGrupos := UpdateKind;
     if ( UpdateKind in [ ukInsert, ukModify ] ) then
     begin
          if CambiaCampo( DeltaDS.FieldByName( 'GR_PADRE' ) ) then
          begin
               with oZetaProvider do
               begin
                    FGrupos := CreateQuery( Format( GetScript( Q_CHECAGRUPOS ), [ DeltaDS.FieldByName( 'GR_PADRE' ).AsInteger ] ) );
                    try
                       Ejecuta( FGrupos );
                    finally
                           FreeAndNil( FGrupos );
                    end;
               end;
          end;
          {Actualizar el codigo de grupos en las dem�s bases de datos si se cambia el codigo del grupo }
          if ( UpdateKind in [ ukModify ] ) then
          begin
               if CambiaCampo( DeltaDS.FieldByName( 'GR_CODIGO' ) ) then
               begin
                    FCodigoAnterior := DeltaDS.FieldByName( 'GR_CODIGO' ).OldValue;
                    FCodigoNuevo := DeltaDS.FieldByName( 'GR_CODIGO' ).NewValue;
               end;
          end;
     end;
     if ( UpdateKind in [ ukDelete ] ) then
        FCodigoAnterior:= DeltaDS.FieldByName( 'GR_CODIGO' ).AsInteger;

     GrabaCambiosBitacora( Sender, SourceDS, DeltaDS, UpdateKind );
end;

procedure TdmServerSistema.AfterUpdateCalendarioReportes(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind );
var
   sTitulobitacora : string;
begin
     sTitulobitacora := VACIO;
     if ( UpdateKind in [ ukDelete ] ) then
     begin
           sTitulobitacora := 'Borr� env�o: '+ DeltaDS.FieldByName( 'CA_NOMBRE' ).AsString;
           oZetaProvider.EscribeBitacoraComparte( tbNormal, clbCalendarioReportesSist, Now, sTitulobitacora, 'Nombre: '+DeltaDS.FieldByName( 'CA_NOMBRE' ).AsString + CR_LF +
           'Descripci�n: '+DeltaDS.FieldByName( 'CA_DESCRIP' ).AsString + CR_LF +
           'Empresa: '+DeltaDS.FieldByName( 'CM_CODIGO' ).AsString + CR_LF +
           'Reporte: '+DeltaDS.FieldByName( 'CA_REPORT' ).AsString + CR_LF +
           'Frecuencia: '+ObtieneElemento(lfEmailFrecuencia, DeltaDS.FieldByName( 'CA_FREC' ).AsInteger) + CR_LF +
           'Fecha de inicio del env�o: '+ DeltaDS.FieldByName( 'CA_FECHA' ).AsString
           , DeltaDS.FieldByName( 'CA_US_CHG' ).AsInteger);
     end;
end;

function TdmServerSistema.GetImpresoras: OleVariant;
begin
     SetTablaInfo( eImpresoras );
     with oZetaProvider do
     begin
          Result := GetTabla( Comparte );
     end;
     SetComplete;
end;

function TdmServerSistema.GetAccesos(iGrupo: Integer; const sCompany: WideString): OleVariant;
const
     K_FILTRO = '( GR_CODIGO = %d ) and ( CM_CODIGO = ''%s'' )';
begin
     SetTablaInfo( eAccesos );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( K_FILTRO, [ iGrupo, sCompany ] );
          Result := GetTabla( Comparte );
     end;
     SetComplete;
end;

function TdmServerSistema.GetUsuarios(out Longitud: Integer;Grupo: Integer; out BlockSistema: WordBool): OleVariant;
begin
     SetTablaInfo( eUsuarios );
     SetFiltroGrupos( Grupo );
     with oZetaProvider do
     begin
	{$ifdef SISNOM_CLAVES}
          Result := GetTabla( Comparte );      // Se manda encriptado al cliente.
	{$else}
          Result := DecodificaClaves( GetTabla( Comparte ), K_CLAVE_USUARIO );
	{$endif}
     end;
     Longitud := StrToIntDef( LeerClaves( CLAVE_LIMITE_PASSWORD ), 0 );
     BlockSistema := zstrToBool( LeerClaves( CLAVE_SISTEMA_BLOQUEADO ) );
     SetComplete;
end;

function TdmServerSistema.GetUsuarioSupervisores(Empresa: OleVariant; iUsuario: Integer): OleVariant;
const
     K_FILTRO = 'US_CODIGO = %d';
begin
     SetTablaInfo( eUserSuper );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( K_FILTRO, [ iUsuario ] );
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerSistema.GrabaAccesos(oDelta, oParams: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( eAccesos );
     with oZetaProvider do
     begin
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
          if ErrorCount = 0 then
          begin
               EmpresaActiva:= Comparte;
               AsignaParamList( oParams );
               with ParamList do
               begin
                    //EscribeBitacoraComparte( tbNormal, clbDerechosAcceso, NullDateTime,ParamByName('Descripcion').AsString, ParamByName('TextoBitacora').AsBlob, ParamByName('Usuario').AsInteger  );
                    EscribeBitacoraComparte( tbNormal, clbDerechosAcceso, NullDateTime,ParamByName('Descripcion').AsString, ParamByName('TextoBitacora').AsString, ParamByName('Usuario').AsInteger  );
               end;
          end;
     end;
     SetComplete;
end;

function TdmServerSistema.GrabaImpresoras(oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( eImpresoras );
     with oZetaProvider do
     begin
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
     end;
     SetComplete;
end;

function TdmServerSistema.GrabaUsuarios(oDelta: OleVariant; iUsuario: Integer; out ErrorCount: Integer): OleVariant;
begin
{$ifdef SISNOM_CLAVES}
     UsuarioData.EncriptaPass := FALSE;   // Ya viene encriptado del cliente - no se requiere encriptar
     try
{$endif}
     Result := UsuarioData.GrabaUsuarios(oDelta, iUsuario, ErrorCount);
{$ifdef SISNOM_CLAVES}
     finally
            UsuarioData.EncriptaPass := TRUE;   // Lo regresa a TRUE para que el resto de los procesos si lo encripten - Impacto principal en Profesional
     end;
{$endif}
     SetComplete;
end;

procedure TdmServerSistema.GrabaCambiosBitacora(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;UpdateKind: TUpdateKind);
var
   eClase: eClaseSistBitacora;
   sTitulo: String;

 procedure DatosBitacora( eClaseCatComparte: eClaseSistBitacora; sDescripcion, sKeyField : string );
  var
     oField : TField;
 begin
      eClase  := eClaseCatComparte;
      sTitulo := sDescripcion;

      if StrLleno(sKeyField) then
      begin
           oField := DeltaDS.FieldByName( sKeyField );

           if oField is TNumericField then
              sTitulo := sTitulo + ' ' + IntToStr( ZetaServerTools.CampoOldAsVar( oField ) )
           else
               sTitulo := sTitulo + ' ' + ZetaServerTools.CampoOldAsVar( oField );
      end;
 end;

begin
     if UpdateKind in [ ukModify, ukDelete ] then
     begin
          eClase := clbNingunoSist;

          case  FTipoSistema of
                    eGruposUsuarios              : DatosBitacora( clbCatalogoGrupoUsuarios, 'Grupo :', 'GR_CODIGO' );


                    else raise Exception.Create( 'No ha sido Integrado el Cat�logo al Case de <DServerSistema.GrabaCambiosBitacora>' )
          end;

          with oZetaProvider do
            if UpdateKind = ukModify then
                CambioCatalogoComparte( sTitulo, eClase, DeltaDS, FUsuario )
            else
                BorraCatalogoComparte( sTitulo,  eClase, DeltaDS, FUsuario );
     end;
end;


procedure TdmServerSistema.GrabaCambiosBitacoraDiccion(Sender: TObject; SourceDS: TDataSet;
  DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
var
  eClase  : eClaseBitacora;
  sTitulo : string;
  sDetalle: string;
  eClaseDiccion:eTipoDiccionario;

  procedure DatosBitacora(eClaseCatalogo: eClaseBitacora; sDescripcion, sKeyField: string);
  var
    oField: TField;
  begin
    eClase := eClaseCatalogo;
    sTitulo := sDescripcion;

    if strLleno(sKeyField) then begin
      oField := DeltaDS.FieldByName(sKeyField);

      if oField is TNumericField then
        sTitulo := sTitulo + ' ' + InTToStr(ZetaServerTools.CampoOldAsVar(oField))
      else
        sTitulo := sTitulo + ' ' + ZetaServerTools.CampoOldAsVar(oField);
    end;
  end;

  function GetEntidad: string;
  var
    Nombre: string;
    Codigo: Integer;
  begin
    InitCreator;
    oZetaCreator.PreparaEntidades;
    Codigo := ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('EN_CODIGO'));
    Nombre := oZetaCreator.dmEntidadesTress.NombreEntidad(Codigo);
    Result := InTToStr(Codigo) + '=' + Nombre;
  end;

  function GetClaseDiccion(TablaInfo:TTablaInfo):eTipoDiccionario;
  begin
       if TablaInfo.Tabla = 'R_CLASIFI'  then
          Result := eClasifi
       else if TablaInfo.Tabla = 'R_ENTIDAD'  then
           Result := eEntidades
       else if TablaInfo.Tabla = 'R_LISTAVAL'  then
           Result := eListasFijas
       else if TablaInfo.Tabla = 'R_VALOR'  then
           Result := eListasFijasValor
       else if TablaInfo.Tabla = 'R_MODULO'  then
           Result := eModulos
       else if TablaInfo.Tabla = 'R_CLAS_ACC'  then
           Result := eAccesosClasifi
       else if TablaInfo.Tabla = 'R_ENT_ACC'  then
           Result := eAccesosEntidades
       else if TablaInfo.Tabla = 'R_MOD_ENT'  then
           Result := eModulosPorEntidad
       else if TablaInfo.Tabla = 'R_ATRIBUTO'  then
           Result := eCamposPorEntidad
       else if TablaInfo.Tabla = 'R_RELACION'  then
           Result := eRelacionesPorEntidad
       else if TablaInfo.Tabla = 'R_CLAS_ENT'  then
           Result := eClasifiPorEntidad
       else if TablaInfo.Tabla = 'R_DEFAULT'  then
           Result := eCamposDefault
       else if TablaInfo.Tabla = 'R_ORDEN'  then
           Result := eOrdenDefault
       else if TablaInfo.Tabla = 'R_FILTRO'  then
           Result := eFiltrosDefault;

  end;


begin
  if UpdateKind in [ukModify, ukDelete] then begin

    eClase := clbNinguno;
    eClaseDiccion := GetClaseDiccion(oZetaProvider.TablaInfo);
    case eClaseDiccion  of
      eClasifi:
        DatosBitacora(clbRepCfgClasificaciones, 'Clasificaci�n:', 'RC_CODIGO');

      eListasFijas:
        DatosBitacora(clbRepCfgListaValores, 'Lista Valor:', 'LV_CODIGO');

      eListasFijasValor:
        DatosBitacora(clbRepCfgValores,
          'Lista Fija:' + InTToStr(ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('LV_CODIGO'))) + ' Valor: ' +
            InTToStr(ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('VL_CODIGO'))), VACIO);

      eModulos:
        DatosBitacora(clbRepCfgModulos, 'M�dulo:', 'MO_CODIGO');

      eEntidades:
        DatosBitacora(clbRepCfgEntidades, 'Entidad:' + GetEntidad, VACIO);

      eCamposPorEntidad: begin
          DatosBitacora(clbRepCfgCampos, 'Entidad:' + GetEntidad, VACIO);
          sDetalle := ' Campo: ' + ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('AT_CAMPO')) + CR_LF;
        end;
      eCamposDefault: begin
          DatosBitacora(clbRepCfgCamposDefault, 'Entidad:' + GetEntidad, VACIO);
          sDetalle := ' Campo:' + ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('AT_CAMPO')) + CR_LF;
        end;
      eOrdenDefault: begin
          DatosBitacora(clbRepCfgOrden, 'Entidad:' + GetEntidad, VACIO);
          sDetalle := ' Campo:' + ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('AT_CAMPO')) + CR_LF;
        end;
      eFiltrosDefault: begin
          DatosBitacora(clbRepCfgFiltros, 'Entidad:' + GetEntidad, VACIO);
          sDetalle := ' Campo:' + ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('AT_CAMPO')) + CR_LF;
        end;
      eRelacionesPorEntidad: begin
          DatosBitacora(clbRepCfgRelaciones, 'Entidad: ' + GetEntidad, VACIO);
          sDetalle := ' Tabla:' + InTToStr(ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('RL_ENTIDAD'))) + CR_LF;
          sDetalle := sDetalle + ' Campos de la relaci�n:' + ZetaServerTools.CampoOldAsVar
            (DeltaDS.FieldByName('RL_CAMPOS')) + CR_LF;
        end;
      eClasifiPorEntidad: begin
          DatosBitacora(clbRepCfgEntidadClasificacioes,
            'Clasificaci�n: ' + InTToStr(ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('RC_CODIGO'))), VACIO);
          sDetalle := ' Entidad:' + GetEntidad + CR_LF;
        end;
      eModulosPorEntidad: begin
          DatosBitacora(clbRepCfgEntidadModulos,
            'M�dulo: ' + InTToStr(ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('MO_CODIGO'))), VACIO);
          sDetalle := ' Entidad:' + GetEntidad;
        end;

      else
        raise Exception.Create
          ('No ha sido Integrado el Cat�logo al Case de <DServerReporteadorDD.GrabaCambiosBitacora>')
    end;

    with oZetaProvider do
      if UpdateKind = ukModify then
        CambioCatalogo(sTitulo, eClase, DeltaDS, 0, sDetalle)
      else
        BorraCatalogo(sTitulo, eClase, DeltaDS, 0, sDetalle);
  end;
end;

function TdmServerSistema.GrabaUsuariosSupervisores(Empresa,oDelta: OleVariant; out ErrorCount: Integer; iUsuario: Integer; const sTexto: WideString): OleVariant;
{$ifdef TRESS}
var
   sDescripcion: String;
const
     K_DELETE = 'delete from SUPER where ( US_CODIGO = %d )';
{$else}
{$endif}
begin
     {$ifdef TRESS}
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          EmpiezaTransaccion;
          try
             ExecSQL( Empresa, Format( K_DELETE, [ iUsuario ] ) );
             TerminaTransaccion( TRUE );
          except
                on Error: Exception do
                begin
                     TerminaTransaccion( FALSE );
                     raise;
                end;
          end;
          if VarIsNull( oDelta ) then
             Result := oDelta
          else
          begin
               SetTablaInfo( eUserSuper );
               Result := GrabaTabla( Empresa, oDelta, ErrorCount );
          end;
          sDescripcion:= 'Se cambiaron Supervisores de Usuario: '+ IntToStr( iUsuario );
          if ( ErrorCount = 0 ) then
          begin
               EscribeBitacora( tbNormal, clbSupervisores, 0, NullDateTime, sDescripcion, sTexto );
          end;
     end;
     {$else}
     Result := NULL;
     {$endif}

     SetComplete;
end;

function TdmServerSistema.GetUsuarioArbol(iUsuario: Integer): OleVariant;
const
     K_TRESS_EXE = 0; // 0 = Tress
begin
     Result := GetUsuarioArbolApp( iUsuario, K_TRESS_EXE );
     SetComplete;
end;
{const
     //K_FILTRO = 'US_CODIGO = %d';
     K_FILTRO = ' ( US_CODIGO = %d ) and ( AB_MODULO = %d )';
begin
     SetTablaInfo( eArbol );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( K_FILTRO, [ iUsuario, iExe ] );
          Result := GetTabla( Comparte );
     end;
     SetComplete;
end;}
{GrabaArbolApp(iUsuario, iExe: Integer;
  oDelta: OleVariant; out ErrorCount: Integer): OleVariant;}
function TdmServerSistema.GrabaArbol(iUsuario: Integer; oDelta: OleVariant;out ErrorCount: Integer): OleVariant;
const
     K_TRESS_EXE = 0; // 0 = Tress
begin
     Result := GrabaArbolApp( iUsuario, K_TRESS_EXE, oDelta, ErrorCount );
     SetComplete;
end;
{const
     //K_DELETE = 'delete from ARBOL where ( US_CODIGO = %d ) ';
     K_DELETE = 'delete from ARBOL where ( US_CODIGO = %d ) and ( AB_MODULO = %d ) ';
begin
     // Complemente el 'EmptyDataSet'
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          EmpiezaTransaccion;
          try
             ExecSQL( Comparte, Format( K_DELETE, [ iUsuario, iExe ] ) );
             TerminaTransaccion( TRUE );
          except
                on Error: Exception do
                begin
                     TerminaTransaccion( FALSE );
                     raise;
                end;
          end;
          SetTablaInfo( eArbol );
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
     end;
     SetComplete;
end;}

function TdmServerSistema.GetPoll: OleVariant;
begin
     SetTablaInfo( ePoll );
     with oZetaProvider do
     begin
          Result := GetTabla( Comparte );
     end;
     SetComplete;
end;

function TdmServerSistema.GrabaPoll(oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( ePoll );
     with oZetaProvider do
     begin
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
     end;
     SetComplete;
end;

function TdmServerSistema.GetPollVacio: OleVariant;
const
     K_FILTRO = 'PO_NUMERO = 0';
begin
     SetTablaInfo( ePoll );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := K_FILTRO; { Forzar un query vacio para obtener Metadata }
          Result := GetTabla( Comparte );
     end;
     SetComplete;
end;

function TdmServerSistema.ActivaDesActivaUsuarios( const Usuario, Grupo: integer; const lActiva : Boolean ): OleVariant;
var
   FDataset: TZetaCursor;
   iLongitud: Integer;
   lBloqueoSistema: WordBool;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          EmpiezaTransaccion;
          if ( Grupo = ZetaCommonClasses.D_GRUPO_SIN_RESTRICCION ) then
             FDataSet := CreateQuery( Format( GetScript( Q_ACTIVA_APAGA_USUARIOS_SIN_RESTRICCION ), [ zBoolToStr( lActiva ), Usuario ] ) )
          else
              FDataSet := CreateQuery( Format( GetScript( Q_ACTIVA_APAGA_USUARIOS ), [ zBoolToStr( lActiva ), Usuario, Grupo ] ) );
          try
             try
                Ejecuta( FDataSet );
                TerminaTransaccion( TRUE );
             except
                   on Error: Exception do
                   begin
                        RollBackTransaccion;
                        raise;
                   end;
             end;
          finally
                 FreeAndNil( FDataset );
          end;
     end;
     Result := GetUsuarios( iLongitud, Grupo, lBloqueoSistema );
end;

function TdmServerSistema.ActivaUsuarios(Usuario, Grupo: Integer): OleVariant;
begin
     Result := ActivaDesActivaUsuarios( Usuario, Grupo, False );
     SetComplete;
end;

function TdmServerSistema.SuspendeUsuarios(Usuario, Grupo: Integer): OleVariant;
begin
     Result := ActivaDesActivaUsuarios( Usuario, Grupo, True );
     SetComplete;
end;

procedure TdmServerSistema.BorrarBitacoraParametros;
const
     K_PROCESOS = 'Procesos';
     K_INDIVIDUALES = 'Individuales';
var
   sTipo: String;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          if ParamByName( 'Procesos' ).AsBoolean then
             sTipo := K_PROCESOS
          else
             sTipo := K_INDIVIDUALES;
          FListaParametros := 'Borrar Registros de Bit�cora: ' + sTipo;
          FListaParametros := FListaParametros + K_PIPE + 'Anteriores a: ' + FechaCorta( ParamByName( 'Fecha' ).AsDateTime );
          FListaFormulas   := VACIO;
          FListaFormulas   := FListaFormulas + K_PIPE + Format( 'Lista de %s: ', [ sTipo ] ) + ParamByName( 'Lista' ).AsString;
     end;
end;

function TdmServerSistema.BorrarBitacora(Empresa, Parametros: OleVariant): OleVariant;
var
   lProcesos, lIndividuales, lEmpresa, lSistema, lKiosco, lCafeteria, lBorraBitacoraOk : Boolean;{OP: 06/06/08}
   dFecha : TDate;
   sFecha, sBitacora, sLista, sMessageError: String;{OP: 06/06/08}

   function GetFiltroNumero: String;
   begin
        if ( lIndividuales or lSistema )then
        begin
             Result := Format( 'and BI_CLASE in ( %s )', [ sLista ] ); //and BI_NUMERO = 0
             sBitacora := 'Individual ';
        end;
   end;

begin
     {$ifndef ADUANAS}
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          lBorraBitacoraOk := False;{OP: 06/06/08}
          sMessageError := VACIO;{OP: 06/06/08}
          AsignaParamList( Parametros );
          with ParamList do
          begin
               lProcesos := ParamByName( 'Procesos' ).AsBoolean;
               lIndividuales := ParamByName( 'Individuales' ).AsBoolean;
               lEmpresa := ParamByName( 'Empresa' ).AsBoolean;{OP: 06/06/08}
               lSistema := ParamByName( 'Sistema' ).AsBoolean;{OP: 06/06/08}
               lKiosco := ParamByName( 'Kiosco' ).AsBoolean;{OP: 06/06/08}
               lCafeteria := ParamByName( 'Cafeteria' ).AsBoolean;{OP: 06/06/08}
               dFecha := ParamByName( 'Fecha' ).AsDateTime;
               sFecha := DateToStrSQL( dFecha );
               sLista := ParamByName( 'Lista' ).AsString;
          end;
          BorrarBitacoraParametros;
          if OpenProcess( prSISTDepurar, 1, FListaParametros, FListaFormulas ) then
          begin
               Result := DZetaServerProvider.GetEmptyProcessResult( prSISTDepurar );
               {OP: 06/06/08}
               if lEmpresa then
               begin
                    EmpiezaTransaccion;
                    try
                       if lProcesos then
                       begin
                            sBitacora := 'de Procesos ';
                            ExecSQL( Empresa, Format( GetScript( Q_BORRA_BITACORA_PROCESOS ), [ sFecha, sLista ] ) );
                            ExecSQL( Empresa, Format( GetScript( Q_BORRA_PROCESOS ), [ sFecha, sLista ] ) );
                       end
                       else if ( lEmpresa and lIndividuales ) then
                       begin
                            ExecSQL( Empresa, Format( GetScript( Q_BORRA_BITACORA ), [ sFecha, GetFiltroNumero ] ) )
                       end;
                       TerminaTransaccion( True );
                       lBorraBitacoraOk := True;
                    except
                          on Error: Exception do
                          begin
                               TerminaTransaccion( False );
                               Log.Excepcion( 0, 'Error Al Depurar Bit�cora %s', Error );
                          end;
                    end;
               end
               else
               begin
                    EmpresaActiva := Comparte;
                    try
                       EmpiezaTransaccion;
                       try
                          if lSistema then
                          begin
                               sBitacora := 'de Sistemas ';
                               ExecSQL( Comparte, Format( GetScript( Q_BORRA_BITACORA ), [ sFecha, GetFiltroNumero ] ) );
                          end
                          else if( lKiosco ) then
                          begin
                               sBitacora := 'de Kiosko ';
                               ExecSQL( Comparte, Format( GetScript( Q_BORRA_BITACORA_KIOSCO ), [ sFecha ] ) );
                          end
                          else if( lCafeteria ) then
                          begin
                               sBitacora := 'de Cafeteria ';
                               ExecSQL( Comparte, Format( GetScript( Q_BORRA_BITACORA_CAFETERIA ), [ sFecha ] ) );
                          end;
                          TerminaTransaccion( True );
                          lBorraBitacoraOk := True;
                       except
                             on Error: Exception do
                             begin
                                  TerminaTransaccion( False );
                                  sMessageError := Format( 'Error Al Depurar Bit�cora %s', [ Error.Message ] );
                             end;
                       end;
                    finally
                           EmpresaActiva := Empresa;
                    end;
               end;

               { Bit�cora }
               if lBorraBitacoraOk then
                  Log.Evento( clbNinguno, 0, Now, Format( 'Depur� Bit�cora %s Anterior al %s', [ sBitacora, FechaCorta( dFecha ) ] ) )
               else
                   Log.Error( 0, 'Error Al Depurar Bit�cora', sMessageError );
          end;
          Result := CloseProcess;
     end;
     {$endif}
     SetComplete;
end;

function TdmServerSistema.GetNivel0: OleVariant;
begin
     {$ifdef ANTES}
     SetTablaInfo( eNivel0 );
     with oZetaProvider do
     begin
          Result := GetTabla( Comparte );
     end;
     {$endif}
     with oZetaProvider do
     begin
          Result := OpenSQL( Comparte, Format( GetScript( Q_NIVEL0 ), [ EntreComillas(' ') ] ), True );
     end;

     SetComplete;
end;

function TdmServerSistema.GrabaNivel0(oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( eNivel0 );
     with oZetaProvider do
     begin
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
     end;
     SetComplete;
end;

function TdmServerSistema.GetSuscripciones(const sCampo: string; const iCodigo:integer ): Olevariant;
var
   sScript: String;
begin
     with oZetaProvider do
     begin
          sScript := Format( GetScript( Q_GET_USUARIO_SUSCRIP ), [ sCampo, iCodigo ] );
          Result:= OpenSQL( EmpresaActiva, sScript, True );
     end;
end;

function TdmServerSistema.GetUsuarioSuscrip(Empresa: OleVariant; Usuario: Integer): OleVariant;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     Result:= GetSuscripciones( 'US_CODIGO', Usuario );
     SetComplete;
end;

function TdmServerSistema.GetReporteSuscrip(Empresa: OleVariant; iReporte: Integer): OleVariant;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     Result:= GetSuscripciones( 'RE_CODIGO', iReporte );
     SetComplete;
end;

function TdmServerSistema.GrabaUsuarioSuscrip(Empresa, oDelta: OleVariant; out ErrorCount: Integer; Usuario: Integer): OleVariant;
const
     K_DELETE = 'delete from SUSCRIP where ( US_CODIGO = %d )';
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          SetTablaInfo( eSuscrip );
          TablaInfo.AfterUpdateRecord:= AfterUpdateSuscripciones;
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
     end;
     SetComplete;

end;

procedure TdmServerSistema.AfterUpdateSuscripciones(Sender: TObject;SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;UpdateKind: TUpdateKind);
{$ifdef TRESS}
var
   sTitulo: String;
   iUsuario: Integer;
   iReporte: Integer;
{$else}
{$endif}
begin
     {$ifdef TRESS}
     with oZetaProvider do
     begin
           with DeltaDs do
           begin
                if ( UpdateKind in [ ukModify, ukDelete ]) then
                begin
                     iUsuario := CampoOldAsVar( FieldByName('US_CODIGO') );
                     iReporte := CampoOldAsVar( FieldByName('RE_CODIGO') );
                end
                else
                begin
                     iUsuario := CampoAsVar( FieldByName('US_CODIGO') );
                     iReporte := CampoAsVar( FieldByName('RE_CODIGO') );
                end;
           end;
           sTitulo:= Format('Suscrip. Reporte %d Usuario %d', [iReporte, iUsuario] );

           case UpdateKind of
                ukModify:  CambioCatalogo( sTitulo, clbSuscripcionReportes, DeltaDS );
                ukDelete:  BorraCatalogo( sTitulo, clbSuscripcionReportes, DeltaDS );
                ukInsert:  EscribeBitacora( tbNormal, clbSuscripcionReportes, 0, NullDateTime, 'Agreg� '+ sTitulo, '' );
           end;
     end;
     {$else}
     {$endif}

end;

function TdmServerSistema.GetGruposAccesos(iGrupo: Integer; const sCompany: WideString; iTipo: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          Result:= OpenSQL( Comparte, Format( GetScript( Q_GET_GRUPOS_ACCESOS ), [ iGrupo, sCompany, ZetaServerTools.GetTipoCompany( iTipo ) ] ), True );
     end;
     SetComplete;
end;

function TdmServerSistema.GetUsuarioAreas(Empresa: OleVariant; Usuario: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          Result:= OpenSQL( Empresa, Format( GetScript( Q_GET_USUARIO_AREAS ), [ Usuario ] ), True );
     end;
     SetComplete;
end;

function TdmServerSistema.GrabaUsuarioAreas(Empresa: OleVariant; Usuario: Integer; oDelta: OleVariant; out ErrorCount: Integer; const sTexto: WideString): OleVariant;
{$ifdef TRESS}
var
   sDescripcion: String;
{$endif}
begin
     {$ifdef TRESS}
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          EmpiezaTransaccion;
          try
             ExecSQL( Empresa, Format( GetScript( Q_DEL_USUARIO_AREAS ), [ Usuario ] ) );
             TerminaTransaccion( TRUE );
          except
                on Error: Exception do
                begin
                     TerminaTransaccion( FALSE );
                     raise;
                end;
          end;
          if VarIsNull( oDelta ) then
             Result := oDelta
          else
          begin
               SetTablaInfo( eUsuarioAreas );
               Result := GrabaTabla( Empresa, oDelta, ErrorCount );
          end;
          InitGlobales;
          sDescripcion:= 'Se cambiaron '+ GetGlobalString( K_GLOBAL_LABOR_AREAS )+ ' de usuario: '+ IntToStr( Usuario );
          if ( ErrorCount = 0 ) then
          begin
               EscribeBitacora( tbNormal, clbAreasLabor, 0, NullDateTime, sDescripcion, sTexto );
          end;
     end;
     {$ELSE}
     Result := NULL;
     {$ENDIF}
     SetComplete;
end;

function TdmServerSistema.GetNuevoUsuario: Integer;
var
   FCodigo: TZetaCursor;
   oEmpresaTemp: OleVariant;
begin
     with oZetaProvider do
     begin
          oEmpresaTemp:= EmpresaActiva;
          try
             EmpresaActiva := Comparte;
             FCodigo := CreateQuery( GetScript( Q_NUEVO_USUARIO ) );
             try
                with FCodigo do
                begin
                     Active := True;
                     Result := FieldByName( 'US_CODIGO' ).AsInteger + 1;
                     Active := False;
                end;
             finally
                    FreeAndNil( FCodigo );
             end;
          finally
                 if NOT VarIsNull(oEmpresaTemp) then
                    EmpresaActiva := oEmpresaTemp;

          end;
     end;
     SetComplete;
end;

function TdmServerSistema.ActualizaNumeroTarjeta(out Bitacora: OleVariant): OleVariant;
const
     K_ANCHO_EMPRESA = 10;
     K_ANCHO_PROBLEMA = 80;
var
   iEmpleado, iInvitador, i, iLow, iHigh, iEmpresas: Integer;
   oLista, oEmpresa: OleVariant;
   sTarjeta, sEmpresa, sGrupoTerminal: string;
   FDataSet, FEmpleados, FInvitadores, FEmpID, FRepetido: TZetaCursor;
   oProvider: TdmZetaServerProvider;
   FLog: TStrings;

   procedure InitLog;
   begin
        {
        with cdsLista do
        begin
             InitTempDataSet;
             AddStringField( 'PROBLEMA', K_ANCHO_PROBLEMA );
             CreateTempDataset;
             Active := True;
        end;
        }
        FLog := TStringList.Create;
   end;

   procedure CloseLog;
   begin
        {
        with cdsLista do
        begin
             Active := False;
        end;
        }
        FreeAndNil( FLog );
   end;

   procedure WriteLog( const iEmpleado: Integer; const sMensaje, sError: String );
   begin
        {
        with cdsLista do
        begin
             Insert;
             try
                FieldByName( 'PROBLEMA' ).AsString := Format( '%s: %s', [ sMensaje, sError ] );
                Post;
             except
                   Cancel;
             end;
        end;
        }
        FLog.Add( Format( '%s: %s', [ sMensaje, sError ] ) );
        {$ifndef ADUANAS}
        if Assigned( oProvider ) then
        begin
             oProvider.EscribeBitacora( tbError,
                                        clbNumTarjeta,
                                        iEmpleado,
                                        Date,
                                        sMensaje,
                                        sError );
        end;
        {$endif}
   end;

   procedure WriteException( const iEmpleado: Integer; const sMensaje: String; Error: Exception );
   begin
        WriteLog( iEmpleado, sMensaje, Error.Message );
   end;

   procedure ObtieneListaEmpresas;
   begin
        with oZetaProviderComparte do
        begin
             EmpresaActiva := Comparte;
             FDataSet := CreateQuery( Format( GetScript( Q_COMPANYS_COUNT ), [ GetTipoCompany( Ord( tc3Datos ) ) ] ) );
             try
                with FDataSet do
                begin
                     Active := True;
                     iEmpresas := Fields[ 0 ].AsInteger;
                     Active := False;
                end;
             finally
                    FreeAndNil( FDataset );
             end;
             if ( iEmpresas = 0 ) then
                ZetaCommonTools.SetOLEVariantToNull( oLista )
             else
             begin
                  oLista := VarArrayCreate( [ 1, iEmpresas, 1, 2 ], varVariant );
                  VarArrayLock( oLista );
                  try
                     iEmpresas := 1;
                     FDataset := CreateQuery( Format( GetScript( Q_COMPANYS_LIST ), [ GetTipoCompany( Ord( tc3Datos ) ) ] ) );
                     try
                        with FDataSet do
                        begin
                             Active := True;
                             while not Eof do
                             begin
                                  oLista[ iEmpresas, 1 ] := FieldByName( 'CM_NOMBRE' ).AsString;
                                  oLista[ iEmpresas, 2 ] := BuildEmpresa( FDataset );
                                  Inc( iEmpresas );
                                  Next;
                             end;
                             Active := False;
                        end;
                     finally
                            FreeAndNil( FDataSet );
                     end;
                  finally
                         VarArrayUnlock( oLista );
                  end;
             end;
        end;
   end;

   procedure BorrarRegistrosTabla( const sEmpresa: String );
   begin
        { Se Borran los Registro de la Tabla }
        with oZetaProviderComparte do
        begin
             EmpiezaTransaccion;
             try
                ParamAsString( FEmpId, 'CM_CODIGO', sEmpresa );
                Ejecuta( FEmpId );
                TerminaTransaccion( TRUE );
             except
                   on Error: Exception do
                   begin
                        TerminaTransaccion( FALSE );
                        WriteException( iEmpleado, 'Error al Borrar Informaci�n de Comparte', Error );
                   end;
             end;
        end;
   end;

   procedure EscribeRepetidos( const iCodigo: integer; const lEmpleado: boolean = TRUE );
   const
        aCampo: array[ FALSE..TRUE ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'IV_CODIGO', 'CB_CODIGO' );
        aMensaje: array[ FALSE..TRUE ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'Invitador %d e Invitador %d de Empresa %s', 'Empleado  %d y Empleado  %d de Empresa %s' );
   var
      sCampo: string;
   begin
        sCampo := aCampo[ lEmpleado ];
        { MV(21/Mar/2014): Se valida antes de hacer el query que la tarjeta que envio no es igual a CERO.
                           Defecto de agregar al a bit�cora mensajes de error por cada empleado que no tiene tarjeta de proximidad }
        if ( sTarjeta <> '0' ) then
        begin
             FRepetido := oZetaProviderComparte.CreateQuery( Format( GetScript( Q_CODIGO_REPETIDO ), [ EntreComillas( sTarjeta ) ] ) );
             try
                with FRepetido do
                begin
                     Active := True;
                     try
                        WriteLog( iCodigo, Format( 'Tarjeta %s Repetida', [ sTarjeta ] ), Format( aMensaje[ lEmpleado ], [ iCodigo, FieldByName( sCampo ).AsInteger, FieldByName( 'CM_CODIGO' ).AsString ] ) );
                     finally
                            Active := False;
                     end;
                end;
             finally
                    FreeAndNil( FRepetido );
             end;
        end;
   end;

   procedure ActualizaEmpleados;
   begin
        { Se buscan los Empleados de la Empresa }
        FDataSet := oProvider.CreateQuery( GetScript( Q_EMPLEADOS_COMPANY ) );
        try
           with FDataSet do
           begin
                try
                   try
                      Active := True;
                      while not EOF do
                      begin
                           iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                           sTarjeta  := FieldByName( 'CB_ID_NUM' ).Asstring;
                           sGrupoTerminal := FieldByName( 'CB_GP_COD' ).AsString;
                           sEmpresa  := oProvider.CodigoEmpresaActiva;
                           with oZetaProviderComparte do
                           begin
                                EmpiezaTransaccion;
                                try
                                   ParamAsString( FEmpleados, 'ID_NUMERO', sTarjeta );
                                   ParamAsString( FEmpleados, 'CM_CODIGO', sEmpresa );
                                   ParamAsInteger( FEmpleados, 'CB_CODIGO', iEmpleado );
                                   ParamAsString( FEmpleados, 'CB_GP_COD', sGrupoTerminal );
                                   Ejecuta( FEmpleados );
                                   TerminaTransaccion( TRUE );
                                except
                                      on Error: Exception do
                                      begin
                                           TerminaTransaccion( FALSE );
                                           if PK_VIOLATION( Error )then
                                              EscribeRepetidos( iEmpleado )
                                           else
                                               WriteException( iEmpleado, 'Error Al Actualizar Empleados En Tabla EMP_ID', Error );
                                      end;
                                end;
                           end;
                           Next;
                      end;
                   except
                         on Error: Exception do
                         begin
                              WriteException( iEmpleado, 'Error Al Actualizar # Tarjeta de Empleados', Error );
                              SetProcessError( Result );
                         end;
                   end;
                finally
                       Active := False;
                end;
           end;
        finally
               FreeAndNil( FDataset );
        end;
   end;

   procedure ActualizaInvitadores;
   begin
        { Se buscan los invitadores de la Empresa }
        FDataSet := oProvider.CreateQuery( GetScript( Q_INVITADORES_COMPANY ) );
        try
           with FDataSet do
           begin
                try
                   try
                      Active := True;
                      while not EOF do
                      begin
                           iInvitador := FieldByName( 'IV_CODIGO' ).AsInteger;
                           sTarjeta  := FieldByName( 'IV_ID_NUM' ).Asstring;
                           sEmpresa  := oProvider.CodigoEmpresaActiva;
                           with oZetaProviderComparte do
                           begin
                                EmpiezaTransaccion;
                                try
                                   ParamAsString( FInvitadores, 'ID_NUMERO', sTarjeta );
                                   ParamAsString( FInvitadores, 'CM_CODIGO', sEmpresa );
                                   ParamAsInteger( FInvitadores, 'IV_CODIGO', iInvitador );
                                   Ejecuta( FInvitadores );
                                   TerminaTransaccion( TRUE );
                                except
                                      on Error: Exception do
                                      begin
                                           TerminaTransaccion( FALSE );
                                           if PK_VIOLATION( Error )then
                                              EscribeRepetidos( iInvitador, FALSE )
                                           else
                                               WriteException( iInvitador, 'Error al Actualizar Invitadores en Tabla EMP_ID', Error );
                                      end;
                                end;
                           end;
                           Next;
                      end;
                   except
                         on Error: Exception do
                         begin
                              WriteException( iInvitador, 'Error Al Actualizar # Tarjeta Invitadores', Error );
                              SetProcessError( Result );
                         end;
                   end;
                finally
                       Active := False;
                end;
           end;
        finally
               FreeAndNil( FDataset );
        end;
   end;

   procedure InitQuerys;
   begin
        with oZetaProviderComparte do
        begin
             FEmpId := CreateQuery( GetScript( Q_BORRA_EMPLEADOS_COMPARTE ) );
             FEmpleados := CreateQuery( GetScript( Q_INSERTA_EMP_ID ) );
             FInvitadores := CreateQuery( GetScript( Q_INSERTA_INV_EMP_ID ) );
        end;
   end;

   procedure FreeQuerys;
   begin
        FreeAndNil( FInvitadores );
        FreeAndNil( FEmpleados );
        FreeAndNil( FEmpId );
   end;

begin
     {$ifndef ADUANAS}
     InitLog;
     with FLog do
     begin
          Add( Format( '**** Inicio de Actualizaci�n de Tarjetas: %s ****', [ FormatDateTime( 'dd/mmm/yyyy hh:nn:ss AM/PM', Now ) ] ) );
          Add( '' );
          Bitacora := Text;
     end;
     Result := GetEmptyProcessResult( prSISTNumeroTarjeta );
     try
          { Se llena el OleVariant oLista }
          try
               ObtieneListaEmpresas;
          except
               on Error: Exception do
               begin
                    WriteException( 0, 'Error Al Leer Lista De Empresas', Error );
                    ZetaCommonTools.SetOLEVariantToNull( oLista )
               end;
          end;

          { Se Verifica que se tengan empresas con Digito }
          if not VarIsNull( oLista ) then
          begin
               iLow := VarArrayLowBound( oLista, 1 );
               iHigh := VarArrayHighBound( oLista, 1 );
               InitQuerys;
               try
                    for i := iLow to iHigh do
                    begin
                         iEmpleado := 0;
                         iInvitador := 0;
                         oEmpresa := oLista[ i, 2 ];
                         oProvider := TdmZetaServerProvider.Create( Self );
                         try
                              try
                                   oProvider.EmpresaActiva := oEmpresa;
                                   with FLog do
                                   begin
                                        Add( Format( '--- Actualizaci�n Tarjetas De La Empresa: %s ---', [ oProvider.CodigoEmpresaActiva ] ) );
                                        Add('');
                                        Bitacora := Text;
                                   end;
                                   BorrarRegistrosTabla( oProvider.CodigoEmpresaActiva ); { Borrar los Registro de la Tabla EMP_ID por Empresa }
                                   ActualizaEmpleados;                                    { M�todo para Actualizar los Empleados de Cada Empresa }
                                   ActualizaInvitadores;                                  { M�todo para Actualizar los Invitadores de Cada Empresa }

                              finally
                                     FreeAndNil( oProvider );
                              end;
                         except                                                           //Si falla el EmpresaActiva escribe error en log pero contin�a
                              on Error: Exception do                                      //Escribe error en log
                                   WriteException( 0, '--- Error Al Actualizar Tarjetas --- ', Error );
                         end;
                    end;
                    SetProcessOK( Result );                                               //Termina thread en OK
               finally
                      FreeQuerys;                                                              //Termina thread en OK
               end;
          end;
     except
           on Error: Exception do
           begin
                SetProcessError( Result );
                FreeAndNil( oProvider );
                WriteException( 0, 'Error Al Actualizar Tarjetas', Error );
           end;
     end;
     with FLog do
     begin
          Add( '' );
          Add( Format( '**** Fin de Actualizaci�n de Tarjetas:    %s ****', [ FormatDateTime( 'dd/mmm/yyyy hh:nn:ss AM/PM', Now ) ] ) );
          Bitacora := Text;
     end;
     CloseLog;
     {$endif}
     SetComplete;
end;

function TdmServerSistema.GetGruposAdic(Empresa: OleVariant; out oCamposAdic: OleVariant): OleVariant;
var
   sFiltroDerecho :string;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          if ( oZetaProvider.CodigoGrupo = D_GRUPO_SIN_RESTRICCION ) then
          begin
               SetTablaInfo( eGruposAdic );
               TablaInfo.SetOrder( 'GX_POSICIO' );
               Result := GetTabla( Empresa );
          end
          else
          begin
               sFiltroDerecho := 'and ( ( GX_DERECHO % 2 ) = 1 )';
               {$ifdef DOS_CAPAS}
               sFiltroDerecho := 'and ( GX_DERECHO in ( 1, 9, 17, 25 ) )';
               {$endif}
               Result := OpenSQL(Empresa, Format(GetScript(Q_GRUPO_ACCESOS),[ EntreComillas( CodigoEmpresaActiva ) ,CodigoGrupo,sFiltroDerecho  ] ),True );
          end;
          SetTablaInfo( eCamposAdic );
          TablaInfo.SetOrder( 'GX_CODIGO,CX_POSICIO' );
          oCamposAdic := GetTabla( Empresa );
     end;
end;


function TdmServerSistema.GrabaGruposAdic(Empresa, oDelta, oDeltaCampos: OleVariant;
         out ErrorCount, ErrorCampos: Integer; out CamposResult: OleVariant): OleVariant;

   function BorraTabla( const sTabla: String ): Boolean;
   begin
        with oZetaProvider do
        begin
             EmpiezaTransaccion;
             try
                ExecSQL( Empresa, Format( GetScript( Q_BORRA_TABLA ), [ sTabla ] ) );
                TerminaTransaccion( TRUE );
                Result := TRUE;
             except
                   on Error: Exception do
                   begin
                        TerminaTransaccion( FALSE );
                        raise;
                   end;
             end;
        end;
   end;

begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;

          if BorraTabla( 'GRUPO_AD' ) then
          begin
               if VarIsNull( oDelta ) then
                  Result := oDelta
               else
               begin
                    SetTablaInfo( eGruposAdic );
                    Result := GrabaTabla( Empresa, oDelta, ErrorCount );
               end;

               if ( ErrorCount = 0 ) and BorraTabla( 'CAMPO_AD' ) then
               begin
                    //Borra los grupos adicionales que ya no existen
                    ExecSQL( Empresa, GetScript( Q_BORRA_GRUPO_AD ) );

                    if VarIsNull( oDeltaCampos ) then
                       Result := oDeltaCampos
                    else
                    begin
                         SetTablaInfo( eCamposAdic );
                         CamposResult := GrabaTabla( Empresa, oDeltaCampos, ErrorCampos );
                    end;

                    if ( ErrorCampos = 0 ) then
                       EjecutaAndFree( GetScript( Q_ACTUALIZA_DICCION ) );
               end;
          end;
     end;
end;

function TdmServerSistema.GetBitacora(Empresa, Parametros: OleVariant): OleVariant;
var
   sSQL, CampoFecha: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          AsignaParamList( Parametros );
          with ParamList do
          begin
               if ParamByName('FechaCaptura').AsBoolean then
                  CampoFecha := 'BI_FECHA'
               else
                  CampoFecha := 'BI_FEC_MOV';

               sSQL := Format( GetScript( Q_BITACORA ),
                             [ CampoFecha,
                               DateToStrSQLC( ParamByName( 'FechaInicial' ).AsDate ),
                               DateToStrSQLC( ParamByName( 'FechaFinal' ).AsDate + 1 ),
                               ParamByName( 'Tipo' ).AsInteger,
                               ParamByName( 'Clase' ).AsInteger,
                               ParamByName( 'Usuario' ).AsInteger,
                               ParamByName( 'NumProceso' ).AsInteger ] );
          end;
          Result := OpenSQL( EmpresaActiva, sSQL, True );
     end;
     SetComplete;
end;

function TdmServerSistema.GetClasifiEmpresa( Empresa: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Result := OpenSQL( EmpresaActiva, GetScript( Q_CLASIFI_EMPRESA ), True );
     end;
     SetComplete;
end;

function TdmServerSistema.GetRoles: OleVariant;
begin
     SetTablaInfo( eRoles );
     with oZetaProvider do
     begin
          Result := GetTabla( Comparte );
     end;
     SetComplete;
end;

function TdmServerSistema.GrabaRoles(const Rol: WideString; Delta, Usuarios, Modelos: OleVariant; var ErrorCount: Integer): OleVariant;
var
   FAgregar: TZetaCursor;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          SetTablaInfo( eRoles );
          if Delta <> NULL then
             Result := GrabaTabla( Comparte, Delta, ErrorCount )
          else
              Result := NULL;

          if ( ErrorCount <= 0 ) then
          begin
               { Grabar Usuarios del Rol }
               FAgregar := CreateQuery( GetScript( Q_USER_ROL_INSERT ) );
               try
                  EmpiezaTransaccion;
                  try
                     ExecSQL( Comparte, Format( GetScript( Q_ROL_USUARIOS_DELETE ), [ Rol ] ) );
                     with cdsLista do
                     begin
                          Lista := Usuarios;
                          First;
                          while not Eof do
                          begin
                               ParamAsString( FAgregar, 'RO_CODIGO', Rol );
                               ParamAsInteger( FAgregar, 'US_CODIGO', FieldByName( 'US_CODIGO' ).AsInteger );
                               Ejecuta( FAgregar );
                               Next;
                          end;
                     end;
                     TerminaTransaccion( TRUE );
                  except
                        on Error: Exception do
                        begin
                             TerminaTransaccion( FALSE );
                             raise;
                        end;
                  end;
               finally
                      FreeAndNil( FAgregar );
               end;
               { Grabar Modelos del Rol }
               {Revisar GrabaRoles del DServerComparte}
          end;
     end;
     SetComplete;
end;

function TdmServerSistema.GetDatosEmpleadoUsuario(Empresa, Datos: OleVariant): OleVariant;
{$ifndef SELECCION}
 {$ifndef VISITANTES}
var
    oParametros: TZetaParams;
    oEvaluador: TZetaEvaluador;
    oZetaCreator : TZetaCreator;
    sError: string;
    {$ifdef DOS_CAPAS}
    oDataSet: TZetaCursor;
    {$endif}

  function CalculaFormula( const iGlobal: integer; const sTitulo: string ): string;
   var
      EVResultado : TQREvResult;
      ErrorNumero: integer;
      ErrorDescrip, ErrorMsg, ErrorExp: string;
  begin
       Result := VACIO;
       EVResultado := oEvaluador.Calculate( oZetaProvider.GetGlobalString(iGlobal) );
       if HuboErrorEvaluador( EVResultado, ErrorNumero, ErrorDescrip, ErrorMsg, ErrorExp ) then
       begin
            sError := 'Error al calcular la f�rmula de ' + sTitulo + ':' + CR_LF +
                      'Error N�mero: ' + IntToStr( ErrorNumero ) + CR_LF +
                      ErrorDescrip + CR_LF + ErrorMsg + CR_LF + ErrorExp + CR_LF + CR_LF +
                      'Revisar las f�rmulas en la forma de Globales\Acceso a Tress';
       end
       else
       begin
            with EvResultado do
            begin
                 case Kind of
                      resInt: Result := IntToStr( intResult );
                      resDouble : Result := FloatToStr( dblResult );
                      resBool: Result := zBoolToStr(booResult);
                      resString: Result := strResult;
                 end;
            end;
       end;
  end;
   {$endif}
 {$endif}

begin
     {$ifndef SELECCION}
        {$ifndef VISITANTES}
     sError := VACIO;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
     end;
     cdsLista.Data := Datos;
     {$ifdef DOS_CAPAS}
     oDataSet:= oZetaProvider.CreateQuery(Format( 'select * from COLABORA where CB_CODIGO = %d', [cdsLista.FieldByName('CB_CODIGO').AsInteger] ));
     oDataSet.Open;
     {$endif}

     oZetaCreator := TZetaCreator.Create( oZetaProvider );
     with oZetaCreator do
     begin
          //PreparaEntidades;
          RegistraFunciones( [ efComunes ] );
     end;

     oEvaluador := TZetaEvaluador.Create( oZetaCreator, self );

     with oEvaluador do
     begin
          {$ifdef DOS_CAPAS}
          AgregaDataSet( TZetaCursor( oDataSet ) );
          {$else}
          AgregaDataSet( TZetaCursor( cdsLista ) );
          {$endif}
          Entidad := enEmpleado;
     end;

     //Resultado
     oParametros:= TZetaParams.Create;
     oParametros.AddString('US_CORTO', CalculaFormula(ZGlobalTress.K_GLOBAL_ENROLL_FORMULA_USER, 'Usuario' ));
     oParametros.AddString('US_PASSWRD',{$ifdef SISNOM_CLAVES}ZetaServerTools.Encrypt({$endif}CalculaFormula(ZGlobalTress.K_GLOBAL_ENROLL_FORMULA_PWD, 'Contrase�a' ){$ifdef SISNOM_CLAVES}){$endif});
     oParametros.AddString('US_DOMAIN', CalculaFormula(ZGlobalTress.K_GLOBAL_ENROLL_FORMULA_AD, 'UsuarioDominio' ));
     oParametros.AddString('US_EMAIL',CalculaFormula(ZGlobalTress.K_GLOBAL_ENROLL_FORMULA_MAIL, 'Correo Electr�nico' ));
     oParametros.AddString('Error',sError);

     Result := oParametros.VarValues;

     FreeAndNil( oParametros );
     FreeAndNil(oEvaluador);
     FreeAndNil(oZetaCreator);
     {$ifdef DOS_CAPAS}
     FreeAndNil( oDataset );
     {$endif}
      {$endif}
     {$endif}

end;

function TdmServerSistema.GetUsuariosRol(const Rol: WideString;  out Modelos: OleVariant): OleVariant;
begin
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Comparte;
             Result := OpenSQL( Comparte, Format( GetScript( Q_ROL_USUARIOS ), [ Rol ] ), True );
             //Modelos := OpenSQL( Comparte, Format( GetScript( Q_ROL_MODELOS ), [ Rol ] ), True );
        end;
        SetComplete;
     except
           SetAbort;
           raise;
     end;
end;

function TdmServerSistema.GetUserRoles(Empresa: OleVariant;iUsuario: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result:= OpenSQL( EmpresaActiva, Format( GetScript( Q_GET_USER_ROLES ), [ iUsuario ] ), True );
     end;
     SetComplete;
end;

function TdmServerSistema.GrabaUsuarioRoles( iUsuario: Integer; iUsuarioLog: Integer; Delta: OleVariant;var ErrorCount: Integer): OleVariant;
{$ifdef TRESS}
  var
   sDescripcion: String;
{$endif}
begin
     {$ifdef TRESS}
     with oZetaProvider do
     begin
          if FUsuario = 0 then
          begin
               FUsuario := iUsuario; //Implementacion para alta de usuario en modo sistema
              {
              EmpresaActiva := Empresa;
              FUsuario := UsuarioActivo;
              }
          end;

           //Implementacion para alta de usuario en modo sistema

          EmpresaActiva := Comparte;
          EmpiezaTransaccion;
          try
             ExecSQL( EmpresaActiva, Format( GetScript( Q_DEL_USUARIO_ROLES ), [ iUsuario ] ) );
             TerminaTransaccion( TRUE );
          except
                on Error: Exception do
                begin
                     TerminaTransaccion( FALSE );
                     raise;
                end;
          end;
          if VarIsNull( Delta ) then
             Result := Delta
          else
          begin
               SetTablaInfo( eUserRol );
               Result := GrabaTabla( EmpresaActiva, Delta, ErrorCount );
          end;
          InitGlobales;
          sDescripcion:= 'Se cambiaron '+ 'los Roles' + ' al usuario: '+ IntToStr( iUsuario );
          if ( ErrorCount = 0 ) then
          begin
               EscribeBitacoraComparte ( tbNormal,clbAsignacionRoles ,NullDateTime, sDescripcion,VACIO,FUsuario);
               //EscribeBitacoraComparte ( tbNormal,clbAsignacionRoles ,NullDateTime, sDescripcion,VACIO, iUsuarioLog );  //grabar el codigo del usuario que realizo el cambio
          end;
     end;
     {$ELSE}
     Result := NULL;
     {$ENDIF}
     SetComplete;
end;

function TdmServerSistema.GrabaEmpleadoUsuario(oEmpresa: OleVariant; iUsuario, iEmpleado: Integer): OleVariant;
begin
     oZetaProvider.EmpresaActiva := oEmpresa;
     UsuarioData.GrabaEmpleadoTress(iEmpleado,iUsuario, oEmpresa[P_CODIGO]);
     SetComplete;
end;

function TdmServerSistema.GetUsuarioData: TUsuarioData;
begin
     if FUsuarioData = NIL then
        FUsuarioData := TUsuarioData.Create(oZetaProvider);
     Result := FUsuarioData;
end;

procedure TdmServerSistema.ClearBroker;
begin
     ZetaSQLBroker.FreeSQLBroker( oZetaSQLBroker );
end;

procedure TdmServerSistema.InitCreator;
begin
     if not Assigned( oZetaCreator ) then
        oZetaCreator := ZetaSQLBroker.GetZetaCreator( oZetaProvider );
end;

procedure TdmServerSistema.InitBroker;
begin
     InitCreator;
     oZetaCreator.RegistraFunciones([efComunes]);
     oZetaSQLBroker := ZetaSQLBroker.GetSQLBroker( oZetaCreator );
end;

function TdmServerSistema.EnrolamientoMasivo(Empresa,Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
          AsignaParamList( Parametros );
     end;
     EnrolamientoMasivoParametros;
     InitBroker;
     try
        EnrolamientoMasivoBuildDataSet;
        Result := EnrolamientoMasivoDataSet( SQLBroker.SuperReporte.DataSetReporte,prSistEnrolarUsuario );
     finally
        ClearBroker;
     end;
     SetComplete;
end;

procedure TdmServerSistema.EnrolamientoMasivoBuildDataSet ;
begin
     {$ifndef SELECCION}
      {$ifndef VISITANTES}
     with SQLBroker do
     begin
          Init( enEmpleado );
          with Agente do
          begin
               AgregaColumna( 'COLABORA.CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
               AgregaColumna( 'COLABORA.US_CODIGO', True, Entidad, tgNumero, 0, 'US_CODIGO' );
               AgregaColumna( K_PRETTYNAME, True, Entidad, tgTexto,100 , 'US_NOMBRE' );
               AgregaColumna( oZetaProvider.ParamList.ParamByName('Usuario').AsString, False, Entidad, tgTexto, 15, 'US_CORTO' );
               AgregaColumna( oZetaProvider.ParamList.ParamByName('Clave').AsString, False, Entidad, tgTexto, 30, 'US_PASSWRD' );
               AgregaColumna( oZetaProvider.ParamList.ParamByName('CorreoElectr�nico').AsString, False, Entidad, tgTexto, 50, 'US_EMAIL' );
               AgregaColumna( oZetaProvider.ParamList.ParamByName('FormulaAD').AsString, False, Entidad, tgTexto, 50, 'US_DOMAIN' );
               AgregaFiltro( 'COLABORA.CB_ACTIVO = ''S'' ' , FALSE, Entidad );
               AgregaOrden( 'COLABORA.CB_CODIGO', True, Entidad );
          end;
          AgregaRangoCondicion( oZetaProvider.ParamList );
          FiltroConfidencial( oZetaProvider.EmpresaActiva );
          BuildDataset( oZetaProvider.EmpresaActiva );
     end;
      {$endif}
     {$endif}
end;

function TdmServerSistema.EnrolamientoMasivoDataset(Dataset: TDataset;Proceso: Procesos): OleVariant;
var
   iEmpleado: TNumEmp;
   iUsuario :Integer;

  function AgregarUsuario(const iGrupo,iReportaA:Integer;const lUsaPortal:Boolean):Boolean;
  begin
       with oZetaProvider do
       begin
            Result:= True;
            with Dataset do
            begin
                 if( not UsuarioData.AgregarUsuario( FieldByName('US_CORTO').AsString,
                                             FieldByName('US_PASSWRD').AsString,
                                             FieldByName('US_DOMAIN').AsString,
                                             FieldByName('US_EMAIL').AsString,
                                             FieldByName('US_NOMBRE').AsString,
                                             iEmpleado,
                                             iGrupo,
                                             iReportaA,
                                             iUsuario,
                                             lUsaPortal
                                             ) )then
                 begin
                      Log.Advertencia(iEmpleado,'No Se Le Di� Acceso a Sistema TRESS',Format('Usuario repetido. Verificar que el usuario: %s � el usuario del dominio: %s no existan',[ UpperCase( FieldByName('US_CORTO').AsString),UpperCase( FieldByName('US_DOMAIN').AsString ) ]) );
                      Result:= False;
                 end;
            end;
       end;
  end;

  procedure RegistrarBitacora(const sRoles:string);
  begin
       with oZetaProvider do
       begin
            EmpiezaTransaccion;
            with Dataset do
            begin
                 try
                    Log.Evento( clbNinguno, iEmpleado, NullDateTime, 'Se Le Di� Acceso a Sistema TRESS ' );
                    if( StrLleno( sRoles ) )then
                    begin
                         Log.Evento( clbNinguno, iEmpleado, NullDateTime, Format( 'Se Asignaron los roles: %s al Usuario: %s ',[sRoles,FieldByName('US_CORTO').AsString ] ) );
                    end;
                    TerminaTransaccion( TRUE );
                 except
                       on Error: Exception do
                       begin
                            RollBackTransaccion;
                       end;
                 end;
            end;
       end;
  end;

  {$ifndef DOS_CAPAS}
  function  VerificarSupervisorEnrolado(const iCB_CODIGO : integer ) : Boolean;
  const
      Q_GET_SUPERVISOR_ENROLADO_POR_EMPLEADO = 'SELECT CB_NIVEL_COD, US_JEFE FROM dbo.SP_GET_SUPERVISOR_ENROLADO_POR_EMPLEADO( %d ) ';
  var
   FSupEnrolado: TZetaCursor;
   sAreaSup, sNombreNivelSupervisor : string;
   iUS_JEFE, iNivelSupervisor : integer;
  begin
       iUS_JEFE := 0;
       sAreaSup := VACIO;
       with oZetaProvider do
       begin
            if not GetGlobalBooleano( K_GLOBAL_ENROLAMIENTO_SUPERVISORES ) then
            begin
                 Result := True;
            end
            else
            begin
               FSupEnrolado := CreateQuery( Format(Q_GET_SUPERVISOR_ENROLADO_POR_EMPLEADO, [iCB_CODIGO])  );
               try
                  with FSupEnrolado do
                  begin
                       Active := True;
                       if ( not IsEmpty )then
                       begin
                            sAreaSup  := FieldByName( 'CB_NIVEL_COD' ).AsString;
                            iUS_JEFE  := FieldByName( 'US_JEFE' ).AsInteger;
                       end;
                       Active := False;
                  end;
               finally
                      FreeAndNil( FSupEnrolado );
               end;

               if ( iUS_JEFE <= 0 ) then
               begin
                  try
                     EmpiezaTransaccion;
                     sNombreNivelSupervisor := VACIO;
                     iNivelSupervisor := GetGlobalInteger( K_GLOBAL_NIVEL_SUPERVISOR );
                     if ( iNivelSupervisor > 0 ) and ( iNivelSupervisor <= 9) then
                        sNombreNivelSupervisor := GetGlobalString( K_GLOBAL_NIVEL1 + iNivelSupervisor -1 );

                     {$ifdef ACS}
                     if ( iNivelSupervisor >=10 ) and ( iNivelSupervisor <= 12) then
                        sNombreNivelSupervisor := GetGlobalString( K_GLOBAL_NIVEL10 + iNivelSupervisor -1 );
                     {$endif}

                     Log.Advertencia(iEmpleado,'No se le asign� un Reporta A',Format('El empleado no tiene asignado un Reporta A: es necesario enrolar al %s %s ',[ sNombreNivelSupervisor, UpperCase( sAreaSup ) ]) );
                     TerminaTransaccion( TRUE );
                  except
                          on Error: Exception do
                          begin
                               RollBackTransaccion;
                          end;
                  end;

                  Result := False;
               end
               else
                   Result := True;
            end;
       end;
  end;
  {$endif}

begin
     with oZetaProvider do
     begin
          if OpenProcess(prSistEnrolarUsuario, Dataset.RecordCount, FListaParametros ) or  OpenProcess(prSistImportarEnrolamientoMasivo , Dataset.RecordCount, FListaParametros )then
          begin
               try
                  try
                     with Dataset do
                     begin
                          First;
                          while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                          begin
                               iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                               try
                                  //Wizard de enrolar empleados
                                  iUsuario := GetNuevoUsuario;
                                  if ( Proceso = prSistEnrolarUsuario ) then
                                  begin
                                       if (FieldByName( 'US_CODIGO' ).AsInteger > 0)then
                                          Log.Evento( clbNinguno, iEmpleado, NullDateTime, Format ('Empleado Ya Tiene Acceso a Sistema TRESS por el Usuario: %s',[ IntToStr( FieldByName( 'US_CODIGO' ).AsInteger) ]) )
                                       else
                                       begin
                                            if ( AgregarUsuario( ParamList.ParamByName('GrupoUsuarios').AsInteger,ParamList.ParamByName('ReportaA').AsInteger,ParamList.ParamByName('UsaPortal').AsBoolean ) )then
                                            begin
                                                 {$ifndef DOS_CAPAS}
                                                 DescargarUsuarioRoles(iUsuario,ParamList.ParamByName('RolesAsignados').AsString);
                                                 VerificarSupervisorEnrolado(iEmpleado);
                                                 {$endif}
                                                 RegistrarBitacora( ParamList.ParamByName('RolesAsignados').AsString );

                                            end;
                                       end;
                                  end   ///Wizard de Importacion de usuarios
                                  else if (Proceso = prSistImportarEnrolamientoMasivo) then
                                  begin
                                       if (zStrToBool(FieldByName('REPETIDO').AsString ) )then
                                       begin
                                            if FAccionAlEnrolar = arIgnorar then
                                               Log.Evento( clbNinguno, iEmpleado, NullDateTime, Format('Usuario: %s ya existe',[FieldByName('US_CORTO').AsString ] ) )
                                            else if( FAccionAlEnrolar = arSobreescribir )then
                                            begin
                                                 UsuarioData.EditarAgregarUsuario( Dataset,False,iUsuario );
                                                 {$ifndef DOS_CAPAS}
                                                 DescargarUsuarioRoles( iUsuario,FieldByName('US_ROLES').AsString );
                                                 VerificarSupervisorEnrolado(iEmpleado);
                                                 {$endif}
                                                 RegistrarBitacora( FieldByName('US_ROLES').AsString );
                                            end;
                                       end
                                       else
                                       begin
                                           UsuarioData.EditarAgregarUsuario(Dataset,True,iUsuario);
                                           {$ifndef DOS_CAPAS}
                                           DescargarUsuarioRoles( iUsuario,FieldByName('US_ROLES').AsString );
                                           VerificarSupervisorEnrolado(iEmpleado);
                                           {$endif}
                                           RegistrarBitacora( FieldByName('US_ROLES').AsString );
                                       end;
                                  end;

                               except
                                     on Error: Exception do
                                     begin
                                          EmpiezaTransaccion;
                                          try
                                             Log.Excepcion( iEmpleado, 'Error Al Dar Acceso a Sistema TRESS ', Error );
                                             TerminaTransaccion( TRUE );
                                          except
                                                on Error: Exception do
                                                begin
                                                     RollBackTransaccion;
                                                end;
                                          end;
                                     end;
                               end;
                               Next;
                          end;
                     end;
                  finally
                      //FreeAndNil( FDataSet );
                  end;
               except
                     on Error: Exception do
                     begin
                          Log.Excepcion( 0, 'Error Al Enrolar Masivamente', Error );
                     end;
               end;
          end;
          Result := CloseProcess;
     end;
end;

{$ifndef DOS_CAPAS}
procedure TdmServerSistema.DescargarUsuarioRoles (const iUsuario:Integer;const sRoles:string);
var
   oEmpresaTemp : OleVariant;
   ErrorCount : Integer;
   Lista :TStrings;
   i:Integer;
   UserRoles : TzProviderClientDataSet;
begin
     with oZetaProvider do
     begin
          if( StrLleno( sRoles ) )then
          begin
                oEmpresaTemp := EmpresaActiva;
                Lista := TStringList.Create;
                UserRoles := TzProviderClientDataSet.Create(Self);
                try
                   EmpresaActiva := Comparte;
                   SetTablaInfo(eUserRol);
                   UserRoles.Data := GetTabla( Comparte );
                   Lista.CommaText := sRoles;
                   with Lista do
                   begin
                        for i := 0 to ( Count - 1 ) do
                        begin
                             with UserRoles do
                             begin
                                   if( Lista[i] <> '' )then
                                   begin
                                        Append;
                                        FieldByName( 'US_CODIGO' ).AsInteger := iUsuario;
                                        FieldByName( 'RO_CODIGO' ).AsString := Lista[i];
                                        Post;
                                   end;
                             end;
                        end;
                   end;
                   //GrabaUsuarioRoles(EmpresaActiva,iUsuario,UserRoles.Delta, ErrorCount);
                   //GrabaUsuarioRoles(iUsuario,UserRoles.Delta, ErrorCount);
                   GrabaUsuarioRoles(iUsuario, iUsuario, UserRoles.Delta, ErrorCount); 
                finally
                       EmpresaActiva := oEmpresaTemp;
                       FreeAndNil(Lista);
                       FreeAndNil(UserRoles);
                end;
          end;
     end;
end;
{$endif}

procedure TdmServerSistema.EnrolamientoMasivoParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'F�rmula Usuario: ' +  ParamByName( 'Usuario' ).AsString + K_PIPE +
                              'F�rmula Clave: ' + ParamByName('Clave').AsString  + K_PIPE +
                              'F�rmula Correo Electr�nico: ' +  ParamByName( 'CorreoElectr�nico' ).AsString + K_PIPE +
                              'Grupo de Usuarios: ' + IntToStr( ParamByName( 'GrupoUsuarios' ).AsInteger) + K_PIPE +
                              'Usa Portal: ' + ZetaCommonTools.zBoolToStr( ParamByName( 'UsaPortal' ).AsBoolean ) + K_PIPE +
                              'Roles Asignados: ' + ParamByName( 'RolesAsignados' ).AsString ;

          {$ifdef TRESS}
          if  oZetaProvider.GetGlobalBooleano( K_GLOBAL_ENROLAMIENTO_SUPERVISORES ) then
          begin
                FListaParametros := FListaParametros + K_PIPE +
                              'Reporta a: seg�n Enrolamiento de Supervisores';
          end
          else
          begin
                    FListaParametros := FListaParametros + K_PIPE +
                              'Reporta a: ' + IntToStr( ParamByName( 'ReportaA' ).AsInteger );
          end;
          {$endif}
     FUsuario := oZetaProvider.UsuarioActivo;
     end;
end;

function TdmServerSistema.EnrolamientoMasivoGetLista(oEmpresa,oParametros: OleVariant): OleVariant;
begin
      with oZetaProvider do
     begin
          EmpresaActiva := oEmpresa;
          AsignaParamList( oParametros );
     end;
     InitBroker;
     try
        EnrolamientoMasivoBuildDataSet;
        Result := SQLBroker.SuperReporte.GetReporte;
     finally
        ClearBroker;
     end;
     SetComplete;
end;

function TdmServerSistema.GetSQLBroker: TSQLBroker;
begin
{$ifdef DOS_CAPAS}
     Result := ZetaSQLBroker.oZetaSQLBroker;
{$else}
     Result := Self.oZetaSQLBroker;
{$endif}
end;

function TdmServerSistema.ImportarEnrolamientoMasivo(Empresa, Parametros,Datos: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
          AsignaParamList( Parametros );
          FAccionAlEnrolar := arIgnorar;
          FAccionAlEnrolar := eAccionRegistros(ParamList.ParamByName('Accion').AsInteger);
          ImportarEnrolamientoMasivoParametros;
          with cdsLista do
          begin
               Lista := Datos;
               Result:= EnrolamientoMasivoDataset( cdsLista ,prSistImportarEnrolamientoMasivo);
          end;
     end;
     SetComplete;
end;

procedure TdmServerSistema.ImportarEnrolamientoMasivoParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := FListaParametros + K_PIPE + 'Archivo: ' + ParamByName( 'Archivo' ).AsString +
                                                 K_PIPE + 'Formato: ' + ObtieneElemento( lfFormatoASCII, ParamByName( 'Formato' ).AsInteger )+
                                                 K_PIPE + 'Acci�n a usuarios existentes: ' + ObtieneElemento( lfAccionRegistros, ParamByName( 'Accion' ).AsInteger);
     end;
end;

function TdmServerSistema.GetTipoLogin:Integer;
var
   oRegistry : TZetaRegistryServer;
begin
     oRegistry := TZetaRegistryServer.Create();
     try
        Result := oRegistry.TipoLogin;
     finally
            FreeAndNil(oRegistry);
     end;
end;

function TdmServerSistema.ImportarEnrolamientoMasivoGetASCII(oEmpresa,Parametros, ListaASCII: OleVariant; out ErrorCount: Integer;LoginAD: WordBool): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := oEmpresa;
          InitGlobales;
          AsignaParamList( Parametros );
          oSuperASCII := TdmSuperASCII.Create( Self );
          try
             with oSuperASCII do
             begin
                  OnValidar := ImportarEnrolamientoMasivoValidaASCII;
                  FEmpleado:= oZetaProvider.CreateQuery( Format('select CB_CODIGO,CB_ACTIVO,%s from COLABORA where CB_CODIGO = :Empleado' ,[K_PRETTYNAME ]) );
                  Formato := eFormatoASCII( ParamList.ParamByName( 'Formato' ).AsInteger );

                  AgregaColumna( 'US_CORTO', tgTexto, K_ANCHO_CODIGOPARTE, True);
                  AgregaColumna( 'US_NOMBRE', tgTexto, K_ANCHO_OBSERVACIONES);
                  if ( ( eTipoLogin(GetTipoLogin) = tlAD ) and ( eAccionRegistros(ParamList.ParamByName('Accion').AsInteger) = arIgnorar ) )then
                     AgregaColumna( 'US_DOMAIN', tgTexto, K_ANCHO_OBSERVACIONES, True)
                  else
                      AgregaColumna( 'US_DOMAIN', tgTexto, K_ANCHO_OBSERVACIONES);

                  AgregaColumna( 'US_PASSWRD', tgTexto, K_ANCHO_PASSWD, True);
                  AgregaColumna( 'GR_CODIGO', tgNumero, K_ANCHO_PESOS,True);
                  AgregaColumna( 'CM_CODIGO', tgTexto, K_ANCHO_PASSWD);
                  AgregaColumna( 'CB_CODIGO', tgNumero, K_ANCHO_NUMEROEMPLEADO);
                  AgregaColumna( 'US_EMAIL', tgTexto, K_ANCHO_OBSERVACIONES);
                  AgregaColumna( 'US_CAMBIA', tgTexto,K_ANCHO_BOOLEANO,False,0,True,'S');
                  AgregaColumna( 'US_ROLES', tgTexto, K_ANCHO_FORMULA );
                  AgregaColumna( 'US_JEFE', tgNumero, K_ANCHO_PESOS );
                  AgregaColumna( 'US_FORMATO', tgNumero,K_ANCHO_PESOS);
                  AgregaColumna( 'US_ACTIVO', tgTexto,K_ANCHO_BOOLEANO,False,0,True,'S');
                  AgregaColumna( 'US_PORTAL', tgTexto,K_ANCHO_BOOLEANO,False,0,True,'S');
                  AgregaColumna( 'US_BLOQUEA', tgTexto,K_ANCHO_BOOLEANO,False,0,True,'N');
                  AgregaColumna( 'REPETIDO', tgTexto, K_ANCHO_BOOLEANO,False,0,True,'N');

                  Result := Procesa( ListaASCII );
                  ErrorCount := Errores;
             end;
          Finally
                 FreeAndNil(oSuperASCII);
                 FreeAndNil(FEmpleado);
          end;
     end;
     SetComplete;
end;

procedure TdmServerSistema.ImportarEnrolamientoMasivoValidaASCII(DataSet: TDataset; var nProblemas: Integer; var sErrorMsg: String );
var
   iEmpleado : Integer;
   cdsGrupos,cdsUsuarios,cdsRoles,cdsCompanys : TServerDataSet;

   function ExisteEmpleado( const iEmpleado: Integer ): Boolean;
   begin
        with FEmpleado do
        begin
             Close;
             oZetaProvider.ParamAsInteger( FEmpleado, 'Empleado', iEmpleado );
             Open;
             Result := ( not IsEmpty );
        end;
   end;
   {
   function ValidaExiste(  ) : Boolean;
   begin
        with FExisteRegistro do
        begin
             Active := TRUE;
             Result := ( not IsEmpty);
             Active := FALSE;
        end;
   end;
    }
   procedure CargarData;
   begin
        cdsGrupos := TServerDataSet.Create(Self);
        cdsUsuarios := TServerDataSet.Create(Self);
        cdsRoles := TServerDataSet.Create(Self);
        cdsCompanys := TServerDataSet.Create(Self);

        with oZetaProvider do
        begin
             cdsGrupos.Data := OpenSQL(Comparte,'select GR_CODIGO from GRUPO',True);
             cdsUsuarios.Data := OpenSQL(Comparte,'select US_CORTO,US_CODIGO,US_DOMAIN,CB_CODIGO from USUARIO',True);
             {$ifndef DOS_CAPAS}
             cdsRoles.Data := OpenSQL(Comparte,'select RO_CODIGO from ROL',True);
             {$endif}
             cdsCompanys.Data := OpenSQL(Comparte,'select CM_CODIGO from COMPANY',True);
        end;

   end;

   function ExisteGrupo( const iGrupo: Integer ) : Boolean;
   begin
         Result := cdsGrupos.Locate('GR_CODIGO',iGrupo,[]);
   end;

   function ExisteUsuario( const sCampo,sValor: string ) : Boolean;
   begin
         if ( StrLleno( sValor ) )then
         begin
              Result := cdsUsuarios.Locate(sCampo,sValor,[])
         end
         else
             Result := False;
   end;

   function ExisteCodigoUsuario( const iCodigo: Integer ) : Boolean;
   begin
        Result := cdsUsuarios.Locate('US_CODIGO',iCodigo,[]);
   end;

   function ExisteRol (const sCodigo:string):Boolean;
   begin
        Result := cdsRoles.Locate('RO_CODIGO',sCodigo,[]);
   end;

   procedure ChecarRoles ( sRoles :string );
   var
      Lista:TStrings;
      i:Integer;
   begin
        Lista := TStringList.Create;
        Lista.CommaText := sRoles;
        with Lista do
        begin
             for i := 0 to ( Count - 1 ) do
             begin
                  if not ExisteRol(Lista[i])then
                  begin
                       Inc( nProblemas );
                       sErrorMsg := sErrorMsg + K_PIPE + Format(' El Rol con el c�digo: %s no existe',[Lista[i]]) + K_PIPE;
                  end;
             end;
        end;
        FreeAndNil(Lista);
   end;

   function ExisteEmpresa(const sCodigo:string) :Boolean;
   begin
        Result := cdsCompanys.Locate('CM_CODIGO',sCodigo,[]);
   end;

   function ExisteEmpleadoEnrolado(Const Empleado:Integer ):Boolean;
   begin
        Result := cdsUsuarios.Locate('CB_CODIGO',Empleado,[]);
   end;

begin
     with DataSet do
     try
          CargarData;
          //Nombres de usuarios repetidos
          if ( ExisteUsuario('US_CORTO',UpperCase( FieldByName('US_CORTO').AsString ) ) ) then
          begin
               FieldByName('REPETIDO').AsString := K_GLOBAL_SI;
          end;
          //Nombres de usuarios del dominio repetidos
          if ( FAccionAlEnrolar = arSobreescribir )then
          begin
               if ( ExisteUsuario('US_DOMAIN',UpperCase( FieldByName('US_DOMAIN').AsString ) ) ) then
               begin
                    if ( not ( FieldByName('US_CORTO').AsString = cdsUsuarios.FieldByName('US_CORTO').AsString ) )then
                    begin
                         Inc( nProblemas );
                         sErrorMsg := sErrorMsg + K_PIPE + Format(' El nombre del Usuario del dominio: %s ya existe',[FieldByName('US_DOMAIN').AsString]) + K_PIPE;
                    end;
               end;
          end
          else if ( FAccionAlEnrolar = arIgnorar )then
          begin
               if ( ExisteUsuario('US_DOMAIN',FieldByName('US_DOMAIN').AsString ) ) then
               begin
                    Inc( nProblemas );
                    sErrorMsg := sErrorMsg + K_PIPE + Format(' El nombre del Usuario del dominio: %s ya existe',[FieldByName('US_DOMAIN').AsString]) + K_PIPE;
               end;
          end;
          //Que exista el Grupo
          if ( not ExisteGrupo(FieldByName('GR_CODIGO').AsInteger ) ) then
          begin
               Inc( nProblemas );
               sErrorMsg := sErrorMsg + K_PIPE + Format(' Grupo con el c�digo: %d no existe',[ FieldByName('GR_CODIGO').AsInteger ] ) + K_PIPE;
          end;

          //Si NO se tiene el global de Enrolamiento de Supervisores Se revisa US_JEFE
          {$ifdef TRESS}
          if not oZetaProvider.GetGlobalBooleano( K_GLOBAL_ENROLAMIENTO_SUPERVISORES ) then
          begin
             //Que exista el US_JEFE
             if ( not ExisteCodigoUsuario(FieldByName('US_JEFE').AsInteger ) and ( ModuloAutorizado( okWorkflow ) or ModuloAutorizado( okEvaluacion ) ) )then
             begin
                  Inc( nProblemas );
                  sErrorMsg := sErrorMsg + K_PIPE + Format(' El Usuario con el c�digo: %d al cual va reportar no existe',[ FieldByName('US_JEFE').AsInteger ] ) + K_PIPE;
             end;
          end;
          {$endif}
          //si quieren usar portal validar que tengan el modulo

          if ( not ExisteEmpresa(FieldByName('CM_CODIGO').AsString ) )then
          begin
               Inc( nProblemas );
               sErrorMsg := sErrorMsg + K_PIPE + Format('La empresa con el c�digo: %s no existe',[ FieldByName('CM_CODIGO').AsString ] ) + K_PIPE;
          end;

          iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
          if  ( ( not ExisteEmpleado (iEmpleado) ) and ( iEmpleado > 0 ) ) then
          begin
               Inc( nProblemas );
               sErrorMsg := sErrorMsg + K_PIPE + ' Empleado no existe ' + K_PIPE;
          end;

          //Checar si el empleado ya esta asignado a otro Usuario
          if ExisteEmpleadoEnrolado(iEmpleado)then
          begin
               Inc( nProblemas );
               sErrorMsg := sErrorMsg + K_PIPE + ' El empleado ya esta asignado al usuario #'+IntToStr( cdsUsuarios.FieldByName('US_CODIGO').AsInteger ) + K_PIPE;
          end;

          {$ifndef DOS_CAPAS}
          //Checar los roles
          ChecarRoles( FieldByName('US_ROLES').AsString);
          {$endif}
     finally
            FreeAndNil(cdsGrupos);
            FreeAndNil(cdsUsuarios);
            FreeAndNil(cdsRoles);
            FreeAndNil(cdsCompanys);
     end;
end;

{$ifdef TRESS}
function TdmServerSistema.ModuloAutorizado(const eModulo: TModulos):Boolean;
begin
     {$ifndef DOS_CAPAS}
     Result := oAutoServer.OkModulo( eModulo, False);
     {$else}
     Result := False;
     {$endif}
end;
{$endif}

function TdmServerSistema.GetDerechosAdicionales(Empresa: OleVariant; Grupo: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Result:= OpenSQL( Empresa, Format( GetScript( Q_ACCESOS_ADICIONALES ),[CodigoEmpresaActiva,Grupo] ),  True );
     end;
     SetComplete;
end;

function TdmServerSistema.GrabaAccesosAdicionales(Empresa: OleVariant; Grupo: Integer; Delta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     if VarIsNull( Delta ) then
     begin
          ErrorCount := 0;
          Result := Null;
     end
     else
         with oZetaProvider do
         begin
              EmpresaActiva := Empresa;
              SetTablaInfo( eAccesosAdicionales );
              Result := GrabaTabla( EmpresaActiva, Delta, ErrorCount );
         end;
end;

//acl
function TdmServerSistema.GetSistLstDispositivos(Empresa: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := OpenSQL( EmpresaActiva, GetScript( Q_SIST_LST_DISPOSITIVOS ), True );
     end;
     SetComplete;
end;

function TdmServerSistema.GrabaSistLstDispositivos(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( eSistLstDispositivos );
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
     end;
     SetComplete;
end;

function TdmServerSistema.GrabaConfigCafeteria(Empresa, oDelta,
  oCalend: OleVariant; var ErrorCount, ErrorCalendCount: Integer;
  out Valor: OleVariant): OleVariant;
var
   iChangeCConfig, iChangeCCalend : Integer;
begin
     iChangeCConfig := ErrorCount;
     iChangeCCalend := ErrorCalendCount;
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          if ( iChangeCConfig = 0 ) then
             Result := oDelta
          else
          begin
               SetTablaInfo( eConfigCafeteria );
               Result := GrabaTabla( Comparte, oDelta, ErrorCount );
          end;

          if ( ( iChangeCConfig = 0 ) or ( ErrorCount = 0 ) ) and ( iChangeCCalend > 0 ) then
          begin
               SetTablaInfo( eCalendCafeteria );
               Valor := GrabaTabla( Comparte, oCalend, ErrorCalendCount );
          end;
     end;
     SetComplete;
end;

function TdmServerSistema.LeeUnaConfiguracion(const Identificador: WideString; var Calendario: OleVariant):OleVariant;
const
     K_TIPO_CAFETERIA = 1;
begin
     SetTablaInfo( eConfigCafeteria );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( '( CF_NOMBRE = ''%s'' ) and ( DI_TIPO = %d )', [ Identificador, K_TIPO_CAFETERIA ] );
          Result := GetTabla( Comparte );
          Calendario := oZetaProvider.OpenSQL( Comparte, Format( GetScript( Q_CALENDARIO_CAF ), [ Identificador, K_TIPO_CAFETERIA ] ), True );
     end;
end;

function TdmServerSistema.GetConfigCafeteria(const Identificador: WideString; out Calendario: OleVariant): OleVariant;
begin
     Result := LeeUnaConfiguracion( Identificador, Calendario);
     SetComplete;
end;

function TdmServerSistema.GetSistListEstaciones(Filtro: OleVariant): OleVariant;
const
     K_DISP_TIPO = 1;
var
   FILTRO_CONFIG : String;
begin
     FILTRO_CONFIG := VACIO;
     with oZetaProvider do
     begin
          if Filtro then
             FILTRO_CONFIG := ' AND DI_NOMBRE NOT IN (SELECT CF_NOMBRE FROM CAF_CONFIG)';

          Result := OpenSQL( Comparte, Format(GetScript( Q_LISTA_ESTACIONES ),[ K_DISP_TIPO, FILTRO_CONFIG ] ), TRUE );
     end;
     SetComplete;
end;

function TdmServerSistema.CopiarConfiguracionCafeteria(Empresa, Parametros: OleVariant): OleVariant;
const
     K_TIPO = 1;
     K_FUENTE = ' ( DI_NOMBRE = ''%s'' ) AND ( D.DI_TIPO = %d ) ';
     K_TODAS = ' ( DI_NOMBRE NOT IN (%s) AND D.DI_TIPO = %d ) ';
     K_NUEVAS = ' ( CF_NOMBRE IS NULL  AND D.DI_TIPO = %d ) ';
     K_ESPECIFICAS = ' ( DI_NOMBRE IN (%s) )';
var
   sNombre, sQuery, sTCom1, sTCom2, sTCom3, sTCom4, sTCom5, sTCom6, sTCom7, sTCom8, sTCom9, sDefCom, sDefCre, sBanner, sGafete, sClave,
   lFoto, lCom, lSigCom, lTeclado, lPreTip, lPreQty, lReinici, lAPrint, lACanc: string;
   iTipo, iTipoComida, iVeloci, iTEsp, iCheckS: Integer;
   FConfig, FDisp, FCopia, FCalend, FCalendCopia : TZetaCursor;
   lTodas, lGeneral, lTipo, lCalendario, lSeguridad: Boolean;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );

         with ParamList do
          begin
               FListaParametros := VACIO;
               FListaParametros := 'Copiar configuraci�n de: ' + ParamByName( 'De' ).AsString + K_PIPE +
                              'Copiar configuraci�n hacia: ' + ParamByName('Hacia').AsString  + K_PIPE +
                              'Copiar datos en: ' + ParamByName( 'Categorias' ).AsString ;

               sNombre := ParamByName( 'De' ).AsString;
               iTipo := K_TIPO;
               lTodas := Pos('Todas', ParamList.ParamByName('Categorias').AsString ) > 0;
               lGeneral := Pos('General', ParamList.ParamByName('Categorias').AsString ) > 0;
               lTipo := Pos('Tipos', ParamList.ParamByName('Categorias').AsString ) > 0;
               lCalendario := Pos('Calendario', ParamList.ParamByName('Categorias').AsString ) > 0;
               lSeguridad := Pos('Seguridad', ParamList.ParamByName('Categorias').AsString ) > 0;
          end;

          if OpenProcess( prSISTImportarCafeteria, 0, FListaParametros ) then
          begin
               try
                  EmpresaActiva := Comparte;
                  sQuery := Format(K_FUENTE, [sNombre, iTipo ]);
                  sQuery := Format(GetScript(Q_GET_DISP), [sQuery]);
                  FDisp := CreateQuery(sQuery);
                  FDisp.Active := True;

                  if Pos('Todas', ParamList.ParamByName('Hacia').AsString ) > 0  then
                     sQuery :=  Format(K_TODAS, [ UnaComilla + sNombre + UnaComilla, K_TIPO ])
                  else if Pos('Nuevas', ParamList.ParamByName('Hacia').AsString ) > 0  then
                       sQuery := Format(K_NUEVAS, [ K_TIPO ] )
                  else
                      sQuery := Format(K_ESPECIFICAS, [UnaComilla + StringReplace(ParamList.ParamByName('Hacia').AsString, ', ', UnaCOMILLA + ',' + UnaCOMILLA, [rfReplaceAll]) + UnaCOMILLA ]);

                  sQuery := Format(GetScript(Q_GET_DISP), [sQuery]);
                  FCopia := CreateQuery(sQuery);
                  with FCopia do
                  begin
                       Active := True;
                       First;
                       while not Eof do
                       begin
                            try
                               if FieldByName( 'STATUS' ).AsInteger = ord(tNoConfigurada) then
                               begin
                                    FConfig := CreateQuery(GetScript(Q_INSERTAR_CONFIG));
                                    iTipoComida := 1;
                                    lFoto := K_GLOBAL_SI;
                                    lCom := K_GLOBAL_SI;
                                    lSigCom := K_GLOBAL_SI;
                                    lTeclado := K_GLOBAL_NO;
                                    sDefCom := VACIO;
                                    sDefCre := VACIO;
                                    lPreTip := K_GLOBAL_NO;
                                    lPreQty := K_GLOBAL_NO;
                                    sBanner := 'Grupo Tress Internacional, S.A. de C.V.';
                                    iVeloci := 5;
                                    iTEsp := 60;
                                    lReinici := K_GLOBAL_SI;
                                    iCheckS := 0;
                                    sGafete := 'CAFE';
                                    sClave := VACIO;
                                    lAPrint := K_GLOBAL_SI;
                                    lACanc := K_GLOBAL_SI;
                                    sTCom1 := 'Tipo de Comida #1';
                                    sTCom2 := 'Tipo de Comida #2';
                                    sTCom3 := 'Tipo de Comida #3';
                                    sTCom4 := 'Tipo de Comida #4';
                                    sTCom5 := 'Tipo de Comida #5';
                                    sTCom6 := 'Tipo de Comida #6';
                                    sTCom7 := 'Tipo de Comida #7';
                                    sTCom8 := 'Tipo de Comida #8';
                                    sTCom9 := 'Tipo de Comida #9';
                               end
                               else
                               begin
                                    //Defaults
                                    FConfig := CreateQuery(GetScript(Q_ACTUALIZAR_CONFIG));
                                    iTipoComida := FCopia.FieldByName('CF_TIP_COM').AsInteger;
                                    lFoto := zBoolToStr ( FCopia.FieldByName('CF_FOTO').AsBoolean );
                                    lCom := zBoolToStr( FCopia.FieldByName('CF_COM').AsBoolean );
                                    lSigCom := zBoolToStr( FCopia.FieldByName('CF_SIG_COM').AsBoolean );
                                    lTeclado := zBoolToStr(  FCopia.FieldByName('CF_TECLADO').AsBoolean );
                                    sDefCom := FCopia.FieldByName('CF_DEF_COM').AsString;
                                    sDefCre := FCopia.FieldByName('CF_DEF_CRE').AsString;
                                    lPreTip := zBoolToStr( FCopia.FieldByName('CF_PRE_TIP').AsBoolean );
                                    lPreQty := zBoolToStr( FCopia.FieldByName('CF_PRE_QTY').AsBoolean );
                                    sBanner := FCopia.FieldByName('CF_BANNER').AsString;
                                    iVeloci := FCopia.FieldByName('CF_VELOCI').AsInteger;
                                    iTEsp := FCopia.FieldByName('CF_T_ESP').AsInteger;
                                    lReinici := FCopia.FieldByName('CF_REINICI').AsString;
                                    iCheckS := FDisp.FieldByName('CF_CHECK_S').AsInteger;
                                    sGafete := FCopia.FieldByName('CF_GAFETE').AsString;
                                    sClave := FCopia.FieldByName('CF_CLAVE').AsString;
                                    lAPrint := zBoolToStr( FCopia.FieldByName('CF_A_PRINT').AsBoolean );
                                    lACanc := zBoolToStr( FCopia.FieldByName('CF_A_CANC').AsBoolean );
                                    sTCom1 := FCopia.FieldByName('CF_TCOM_1').AsString;
                                    sTCom2 := FCopia.FieldByName('CF_TCOM_2').AsString;
                                    sTCom3 := FCopia.FieldByName('CF_TCOM_3').AsString;
                                    sTCom4 := FCopia.FieldByName('CF_TCOM_4').AsString;
                                    sTCom5 := FCopia.FieldByName('CF_TCOM_5').AsString;
                                    sTCom6 := FCopia.FieldByName('CF_TCOM_6').AsString;
                                    sTCom7 := FCopia.FieldByName('CF_TCOM_7').AsString;
                                    sTCom8 := FCopia.FieldByName('CF_TCOM_8').AsString;
                                    sTCom9 := FCopia.FieldByName('CF_TCOM_9').AsString;

                               end;

                               ParamAsString(FConfig, 'CF_NOMBRE', FCopia.FieldByName('DI_NOMBRE').AsString);
                               ParamAsInteger(FConfig, 'DI_TIPO', iTipo);

                               if ( lTodas ) or ( lGeneral ) then
                               begin
                                    iTipoComida := FDisp.FieldByName('CF_TIP_COM').AsInteger;
                                    lFoto := FDisp.FieldByName('CF_FOTO').AsString;
                                    lCom := FDisp.FieldByName('CF_COM').AsString;
                                    lSigCom :=  FDisp.FieldByName('CF_SIG_COM').AsString;
                                    lTeclado := (  FDisp.FieldByName('CF_TECLADO').AsString );
                                    sDefCom := FDisp.FieldByName('CF_DEF_COM').AsString;
                                    sDefCre := FDisp.FieldByName('CF_DEF_CRE').AsString;
                                    lPreTip := ( FDisp.FieldByName('CF_PRE_TIP').AsString );
                                    lPreQty := ( FDisp.FieldByName('CF_PRE_QTY').AsString );
                                    sBanner := FDisp.FieldByName('CF_BANNER').AsString;
                                    iVeloci := FDisp.FieldByName('CF_VELOCI').AsInteger;
                                    iTEsp := FDisp.FieldByName('CF_T_ESP').AsInteger;
                                    lReinici := ( FDisp.FieldByName('CF_REINICI').AsString );
                                    iCheckS := FDisp.FieldByName('CF_CHECK_S').AsInteger;
                               end;

                               if ( lTodas ) or ( lTipo ) then
                               begin
                                    sTCom1 := FDisp.FieldByName('CF_TCOM_1').AsString;
                                    sTCom2 := FDisp.FieldByName('CF_TCOM_2').AsString;
                                    sTCom3 := FDisp.FieldByName('CF_TCOM_3').AsString;
                                    sTCom4 := FDisp.FieldByName('CF_TCOM_4').AsString;
                                    sTCom5 := FDisp.FieldByName('CF_TCOM_5').AsString;
                                    sTCom6 := FDisp.FieldByName('CF_TCOM_6').AsString;
                                    sTCom7 := FDisp.FieldByName('CF_TCOM_7').AsString;
                                    sTCom8 := FDisp.FieldByName('CF_TCOM_8').AsString;
                                    sTCom9 := FDisp.FieldByName('CF_TCOM_9').AsString;
                               end;

                               if ( lTodas ) or ( lSeguridad ) then
                               begin
                                    sGafete := FDisp.FieldByName('CF_GAFETE').AsString;
                                    sClave := FDisp.FieldByName('CF_CLAVE').AsString;
                                    lAPrint := ( FDisp.FieldByName('CF_A_PRINT').AsString );
                                    lACanc := ( FDisp.FieldByName('CF_A_CANC').AsString );
                               end;

                               //General
                               ParamAsInteger(FConfig, 'CF_TIP_COM', iTipoComida);
                               ParamAsString(FConfig, 'CF_FOTO', lFoto );
                               ParamAsString(FConfig, 'CF_COM', lCom );
                               ParamAsString(FConfig, 'CF_SIG_COM', lSigCom );
                               ParamAsString(FConfig, 'CF_TECLADO', lTeclado );
                               ParamAsString(FConfig, 'CF_DEF_COM', sDefCom );
                               ParamAsString(FConfig, 'CF_DEF_CRE', sDefCre );
                               ParamAsString(FConfig, 'CF_PRE_TIP', lPreTip );
                               ParamAsString(FConfig, 'CF_PRE_QTY', lPreQty );
                               ParamAsString(FConfig, 'CF_BANNER', sBanner );
                               ParamAsInteger(FConfig, 'CF_VELOCI', iVeloci );
                               ParamAsInteger(FConfig, 'CF_T_ESP', iTEsp );
                               ParamAsString(FConfig, 'CF_REINICI', lReinici );
                               ParamAsInteger(FConfig, 'CF_CHECK_S', iCheckS );

                               //Seguridad
                               ParamAsString(FConfig, 'CF_GAFETE', sGafete);
                               ParamAsString(FConfig, 'CF_CLAVE', sClave );
                               ParamAsString(FConfig, 'CF_A_PRINT', lAPrint );
                               ParamAsString(FConfig, 'CF_A_CANC', lACanc );

                               //TipoComida
                               ParamAsString(FConfig, 'CF_TCOM_1', sTCom1 );
                               ParamAsString(FConfig, 'CF_TCOM_2', sTCom2 );
                               ParamAsString(FConfig, 'CF_TCOM_3', sTCom3 );
                               ParamAsString(FConfig, 'CF_TCOM_4', sTCom4 );
                               ParamAsString(FConfig, 'CF_TCOM_5', sTCom5 );
                               ParamAsString(FConfig, 'CF_TCOM_6', sTCom6 );
                               ParamAsString(FConfig, 'CF_TCOM_7', sTCom7 );
                               ParamAsString(FConfig, 'CF_TCOM_8', sTCom8 );
                               ParamAsString(FConfig, 'CF_TCOM_9', sTCom9 );

                               Ejecuta(FConfig);

                               if ( lTodas ) or ( lCalendario ) then
                               begin
                                    try
                                       //Borra calendario Actual
                                       try
                                          sQuery := Format(GetScript(Q_BORRAR_CALEND), [ FCopia.FieldByName('DI_NOMBRE').AsString, iTipo ]);
                                          FCalendCopia := CreateQuery(sQuery);
                                          Ejecuta(FCalendCopia);
                                       finally
                                              FreeAndNil(FCalendCopia);
                                       end;

                                       sQuery := Format(GetScript(Q_CALENDARIO_CAF), [ sNombre, iTipo ]);
                                       FCalend := CreateQuery(sQuery);
                                       FCalend.Active := True;
                                       FCalend.First;
                                       while not FCalend.Eof do
                                       begin
                                            try
                                               FCalendCopia := CreateQuery(GetScript(Q_INSERTAR_CALEND));
                                               ParamAsString(FCalendCopia, 'CF_NOMBRE', FCopia.FieldByName('DI_NOMBRE').AsString);
                                               ParamAsInteger(FCalendCopia, 'DI_TIPO', iTipo);
                                               ParamAsString(FCalendCopia, 'HORA', FCalend.FieldByName('HORA').AsString );
                                               ParamAsInteger(FCalendCopia, 'ACCION', FCalend.FieldByName('ACCION').AsInteger );
                                               ParamAsString(FCalendCopia, 'CONTADOR', FCalend.FieldByName('CONTADOR').AsString );
                                               ParamAsInteger(FCalendCopia, 'SEMANA', FCalend.FieldByName('SEMANA').AsInteger );
                                               ParamAsString(FCalendCopia, 'SYNC', FCalend.FieldByName('SYNC').AsString );
                                               Ejecuta(FCalendCopia);
                                            finally
                                                   FreeAndNil(FCalendCopia);
                                            end;

                                            FCalend.Next;
                                       end;
                                    finally
                                           FreeAndNil(FCalend);
                                    end;
                               end;
                            finally
                                   FreeAndNil(FConfig);
                            end;
                            Next;
                       end
                  end;
               finally
                      FreeAndNil(FCopia);
                      FreeAndNil(FDisp);
               end;
          end;
          EmpresaActiva := Empresa;
          Result := CloseProcess;
     end;
     SetComplete;
end;

function TdmServerSistema.GrabaTareaCalendario(Empresa,
  Parametros: OleVariant; out Datos: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );

          with ParamList do
          begin
               FListaParametros := VACIO;
               FListaParametros := 'Nombre del env�o: ' + ParamByName( 'Nombre' ).AsString +
                           CR_LF + 'Descripci�n: ' + ParamByName( 'Descripcion' ).AsString +
                           CR_LF + 'Empresa: ' + ParamByName( 'Empresa' ).AsString +
                           CR_LF + 'Reporte (s): ' + ParamByName( 'Reportes' ).AsString +
                           CR_LF + 'Frecuencia: ' + ObtieneElemento(lfEmailFrecuencia, ParamByName( 'Frecuencia' ).AsInteger) +
                           CR_LF + 'Fecha de inicio del env�o: ' + ParamByName( 'FechaInicio' ).AsString +
                           CR_LF + 'Hora: ' + FormatMaskText('99:99;0', ParamByName( 'Hora' ).AsString) ;
          end;
     end;

     InitBroker;
     try
        Result := AgregaTareaAlCalendario (Empresa, Datos);
     finally
        ClearBroker;
     end;
     SetComplete;

end;

function TdmServerSistema.AgregaTareaAlCalendario(Empresa: OleVariant; out Datos: OleVariant) : OleVariant;
const K_UPDATE_CALENDARIO_ULTIMO = 'UPDATE CALENDARIO SET CA_ULT_FEC = %s WHERE CA_FOLIO = %d';
var
   FDataset: TZetaCursor;
   iFolio, iCaFolio : Integer;
   sTituloBitacora : string;
begin
     InitLog(Empresa,'Agregar');
     with oZetaProvider do
     begin
          if OpenProcess( prSISTAgregarTareaCalendarioReportes, 0, FListaParametros ) then
          begin
               EmpiezaTransaccion;
               try
                  begin
                       sTituloBitacora := VACIO;
                       EmpresaActiva := Comparte;
                       iFolio := ParamList.ParamByName( 'Folio' ).AsInteger;
                       if ( iFolio > 0 ) then
                       begin
                            FDataset := CreateQuery( GetScript( Q_SIST_UPD_TAREA_CALENDARIO ) );
                            ParamAsInteger( FDataset, 'CA_FOLIO', iFolio );
                       end
                       else
                       begin
                            FDataset := CreateQuery( GetScript( Q_SP_AGREGA_CALENDARIO ) );
                       end;

                       ParamAsString( FDataset, 'CA_NOMBRE', ParamList.ParamByName( 'Nombre' ).AsString );
                       ParamAsString( FDataset, 'CM_CODIGO', ParamList.ParamByName( 'Empresa' ).AsString );
                       ParamAsString( FDataset, 'CA_DESCRIP', ParamList.ParamByName( 'Descripcion' ).AsString );
                       ParamAsString( FDataset, 'CA_ACTIVO', ParamList.ParamByName( 'Activo' ).AsString );
                       ParamAsInteger( FDataset, 'CA_FREC', ParamList.ParamByName( 'Frecuencia' ).AsInteger );
                       ParamAsInteger( FDataset, 'CA_RECUR', ParamList.ParamByName( 'Recurrencia' ).AsInteger );
                       ParamAsString( FDataset, 'CA_HORA', ParamList.ParamByName( 'Hora' ).AsString );
                       ParamAsDate( FDataset, 'CA_FECHA', ParamList.ParamByName( 'FechaInicio' ).AsDate );
                       ParamAsString( FDataset, 'CA_DOWS', ParamList.ParamByName( 'Dias' ).AsString );
                       ParamAsString( FDataset, 'CA_MESES', ParamList.ParamByName( 'Meses' ).AsString );
                       ParamAsString( FDataset, 'CA_MESDIAS', ParamList.ParamByName( 'DiasMes' ).AsString );
                       ParamAsString( FDataset, 'CA_MESWEEK', ParamList.ParamByName( 'SemanaMes' ).AsString );
                       ParamAsInteger( FDataset, 'CA_MES_ON', ParamList.ParamByName( 'TipoMes' ).AsInteger );
                       ParamAsInteger( FDataset, 'CA_US_CHG', ParamList.ParamByName( 'Usuario' ).AsInteger );
                       ParamAsString( FDataset, 'CA_REPORT', ParamList.ParamByName( 'Reportes' ).AsString );

                       if ParamList.FindParam( 'FechaSiguiente' ) <> nil then
                          ParamAsDate( FDataset, 'CA_NX_FEC', ParamList.ParamByName( 'FechaSiguiente' ).AsDate )
                       else
                           ParamAsDate( FDataset, 'CA_NX_FEC', ParamList.ParamByName( 'FechaInicio' ).AsDate );

                       ParamAsString( FDataset, 'CA_FSALIDA', ParamList.ParamByName( 'SalidaArchivo' ).AsString );
                       ParamAsInteger( FDataset, 'CA_REPEMP', ParamList.ParamByName( 'ReporteEmpleados' ).AsInteger );
                       ParamAsInteger( FDataset, 'CA_TSALIDA', ParamList.ParamByName( 'Salida' ).AsInteger );
                       ParamAsDate( FDataset, 'CA_FEC_ESP', ParamList.ParamByName( 'FechaEspecial' ).AsDate );
                       ParamAsString( FDataset, 'CA_NX_EVA', ParamList.ParamByName( 'Evaluar' ).AsString );
                       ParamAsDate( FDataset, 'CA_CAPTURA', ParamList.ParamByName( 'FechaModifico' ).AsDate );

                       if iFolio = 0 then
                       begin
                            ParamSalida( FDataset, 'CA_FOLIO' );
                            ParamAsInteger( FDataset, 'SuscribirUsuario', ParamList.ParamByName( 'SuscribirUsuario').AsInteger );
                       end;

                       Ejecuta (FDataSet);
                       if ( iFolio > 0 ) then
                       begin
                            sTitulobitacora := 'Modific� env�o: '+ParamList.ParamByName( 'Nombre' ).AsString ;
                       end
                       else
                       begin
                            Datos := GetParametro( FDataset, 'CA_FOLIO' ).AsInteger;
                            sTitulobitacora := 'Agreg� env�o: '+ ParamList.ParamByName( 'Nombre' ).AsString;
                            try
                               if ParamList.FindParam( 'FechaUltima' ) <> nil then
                               begin
                                    ExecSQL( Comparte, Format( K_UPDATE_CALENDARIO_ULTIMO, [ EntreComillas( DateToStrSQL( ParamList.ParamByName( 'FechaUltima' ).AsDate )), GetParametro( FDataset, 'CA_FOLIO' ).AsInteger  ] ));
                               end
                            except on E: Exception do
                                   //sError := E.Message;
                            end;
                       end;

                       oZetaProvider.EscribeBitacoraComparte( tbNormal, clbCalendarioReportesSist, Now, sTitulobitacora, 'Nombre: '+ParamList.ParamByName( 'Nombre' ).AsString + CR_LF +
                       'Descripci�n: '+ ParamList.ParamByName( 'Descripcion' ).AsString + CR_LF +
                       'Empresa: '+ ParamList.ParamByName( 'Empresa' ).AsString  + CR_LF +
                       'Reporte: '+ ParamList.ParamByName( 'Reportes' ).AsString  + CR_LF +
                       'Frecuencia: '+ ObtieneElemento(lfEmailFrecuencia, ParamList.ParamByName( 'Frecuencia' ).AsInteger)  + CR_LF +
                       'Fecha de inicio del env�o: '+ ParamList.ParamByName( 'FechaInicio' ).AsString
                       , ParamList.ParamByName( 'Usuario' ).AsInteger);
                       EmpresaActiva := Empresa;
                  end;
               finally
                      FreeAndNil( FDataset );
               end;
               TerminaTransaccion( TRUE );
          end;
          Result := CloseProcess;
     end;
     EndLog;
     SetComplete;
end;

function TdmServerSistema.GetGlobalLstDispositivos(Empresa: OleVariant; var Disponibles, Asignados: OleVariant): OleVariant;
begin
     SetOLEVariantToNull( Disponibles );
     SetOLEVariantToNull( Asignados );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;

          Disponibles:= OpenSQL( Comparte, Format(GetScript( Q_DISP_DISPONIBLES ),[ CodigoEmpresaActiva ] ), TRUE );
          Asignados:= OpenSQL( Comparte, Format(GetScript( Q_DISP_ASIGNADOS ),[ CodigoEmpresaActiva ] ), TRUE );
     end;
     SetComplete;
end;

function TdmServerSistema.GrabaGlobalLstDispositivos(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          ExecSQL( Comparte, Format( GetScript( Q_BORRA_DISP ), [ CodigoEmpresaActiva ] ) );

          if NOT varisNull(oDelta) then
          begin
               SetTablaInfo( eDisxCom );
               Result := GrabaTabla( Comparte, oDelta, ErrorCount );
          end
          else
              Result := NULL;
     end;
     SetComplete;
end;

function TdmServerSistema.GetAccArbol(Empresa: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Result := OpenSQL( Empresa, GetScript(Q_ACCARBOL), TRUE  )
     end;
     SetComplete;
end;

procedure TdmServerSistema.ActualizaUsuariosEmpresa(Empresa, Delta: OleVariant);
begin
  {$ifndef DOS_CAPAS}
  UsuarioData.ActualizaUsuariosEmpresa(Empresa, Delta);
  SetComplete;
  {$endif}
end;

function TdmServerSistema.GetUsuarioCCosto(Empresa: OleVariant;  iUsuario: Integer): OleVariant;
const
     K_FILTRO = 'US_CODIGO = %d';
begin
     SetTablaInfo( eUsuarioCCosto );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( K_FILTRO, [ iUsuario ] );
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerSistema.GrabaUsuarioCCosto(Empresa, oDelta: OleVariant; ErrorCount, iUsuario: Integer; const sTexto, sNombreCosteo: WideString): OleVariant;
{$ifdef TRESS}
var
   sDescripcion: String;
const
     K_DELETE = 'delete from SUP_COSTEO where ( US_CODIGO = %d )';
{$else}
{$endif}
begin
     {$ifdef TRESS}
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          EmpiezaTransaccion;
          try
             ExecSQL( Empresa, Format( K_DELETE, [ iUsuario ] ) );
             TerminaTransaccion( TRUE );
          except
                on Error: Exception do
                begin
                     TerminaTransaccion( FALSE );
                     raise;
                end;
          end;

          if VarIsNull( oDelta ) then
             Result := oDelta
          else
          begin
               SetTablaInfo( eUsuarioCCosto );
               Result := GrabaTabla( Empresa, oDelta, ErrorCount );
          end;
          sDescripcion:= Format( 'Se cambiaron %s de Usuario: ', [sNombreCosteo] )+ IntToStr( iUsuario );
          if ( ErrorCount = 0 ) then
          begin
               EscribeBitacora( tbNormal, clbSupervisores, 0, NullDateTime, sDescripcion, sTexto );
          end;
     end;
     {$else}
     Result := NULL;
     {$endif}

     SetComplete;

end;

{$ifndef DOS_CAPAS}
// by @DC
function TdmServerSistema.GeneraReloj(Empresa, Parametros: OleVariant; out Datos: OleVariant): OleVariant;
const
   K_NUM_REGISTROS = 5;
   K_EMPRESA = '0';
   K_CREDENCIAL = 'A';
   K_RELLENO = '000000';
   K_TIPO ='1';
var
   FFormatoFecha: eFormatoImpFecha;
   oArchivo, oRenglon: TStringList;
   iCont,iTipoChecada,iLinea, iMes,iDia,iHora,iMin : Integer;
   sRutaArchivo,sSalida,sIdHuella,sMin,sIdProx,sIdCB,sTerminal,sReloj,sRenglon,sEmpleado,sMes,sDia,sHora,sCM_DIGITO,sCB_CODIGO, sCM_CODIGO:String;
   FDataset: TZetaCursor;
   lContinuar :Boolean;
   dFecha: TDate;

    procedure Agregar (const sRenglon_Reloj: String);
    begin
         with cdsLista do
         begin
              Append;
              FieldByName( 'CHECADA_RELOJ' ).AsString := sRenglon_Reloj;
              Post;
         end;
    end;

    function esNumeroEntero (valor : string) : boolean;
    var
       numero : integer;
    begin
         try
            numero := strtoint(valor);
            result := true;
         except
               result := false;
         end;
    end;

begin
     InitLog(Empresa,'Agregar');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          with ParamList do
          begin
               sRutaArchivo := ParamByName ('RutaArchivoFuente').AsString;
               sSalida := ParamByName ('RutaSalidaArchivo').AsString;
          end;
          FDataSet := Nil;
          if OpenProcess( prSISTRecuperarChecadas, 0, FListaParametros ) then
          begin
               with cdsLista do
               begin
                    InitTempDataset;
                    AddStringField( 'CHECADA_RELOJ', 31 );
                    CreateTempDataset;
               end;
               EmpresaActiva := Comparte;
               oArchivo := TStringList.Create;
               try
                  oRenglon := TStringList.Create;
                  try
                     FFormatoFecha := ifYYYYMMDDs;
                     oRenglon.Delimiter := '|';
                     iLinea:=0;
                     try
                        oArchivo.LoadFromFile( Trim( sRutaArchivo ) );
                        for iCont := 0 to oArchivo.Count-1 do
                        begin
                             Inc( iLinea );
                             lContinuar := True;
                             oRenglon.Clear;
                             //asignamos el renglon al archivo
                             oRenglon.DelimitedText := oArchivo[iCont];
                             if ( oRenglon.Count >= K_NUM_REGISTROS ) then
                             begin
                                  sRenglon := VACIO;
                                  sCM_DIGITO := VACIO;
                                  sCB_CODIGO := VACIO;
                                  sCM_CODIGO := VACIO;
                                  //validamos que los campos no vengan vacios y que las fechas esten en formato valido
                                  if ( StrVacio ( oRenglon [ 0 ] ) ) then
                                  begin
                                       lContinuar := False;
                                       Log.Advertencia( 0,'L�nea #' + IntToStr( iLinea ) + ',Error en el campo id de checada.', ' El identificador de checada en el archivo est� vac�o.' );
                                  end;
                                  dFecha := GetFechaImportada( Trim( oRenglon[ 1 ] ) , FFormatoFecha );
                                  if ( dFecha = NullDateTime ) then
                                  begin
                                       lContinuar := False;
                                       Log.Advertencia(0,'L�nea #'+IntToStr( iLinea )+',Error en el campo fecha de checada.','L�nea #'+IntToStr( iLinea )+',El campo fecha de checada en el archivo es inv�lida.');
                                  end;
                                  if ( StrVacio ( oRenglon [ 2 ] ) ) then
                                  begin
                                       lContinuar := False;
                                       Log.Advertencia(0,'L�nea #'+IntToStr( iLinea )+',Error en el campo hora de checada.','L�nea #'+IntToStr( iLinea )+',El campo hora de checada en el archivo es inv�lida.');
                                  end
                                  else
                                  begin
                                       sHora :=  Copy ( oRenglon[ 2 ], 1, 2 );
                                       sMin  :=  Copy ( oRenglon[ 2 ], 4, 2 );
                                       iHora := StrToInt( sHora );
                                       iMin  := StrToint( sMin );
                                       if( ( iHora > 24) or ( iMin > 59 ) ) then
                                       begin
                                            Log.Advertencia(0,'L�nea #'+IntToStr( iLinea )+',Error en el campo hora de checada.','L�nea #'+IntToStr( iLinea )+',El campo hora de checada en el archivo es inv�lida.');
                                            lContinuar:=False;
                                       end;
                                  end;
                                  if( strVacio ( oRenglon[ 3 ] ) ) then
                                  begin
                                       lContinuar:=False;
                                       Log.Advertencia(0,'L�nea #'+IntToStr( iLinea )+',Error en el campo id de terminal.','L�nea #'+IntToStr( iLinea )+',El identificador de la terminal en el archivo est� vac�o.');
                                  end;
                                  if  ( not EsNumeroEntero( oRenglon[ 4 ] ) ) then
                                  begin
                                       lContinuar:=False;
                                       Log.Advertencia(0,'L�nea #'+IntToStr( iLinea )+',Error en el campo tipo de checada.','L�nea #'+IntToStr( iLinea )+',El campo tipo de checada en el archivo es inv�lido.');
                                  end;
                                  if ( lContinuar ) then
                                  begin
                                       iTipoChecada := StrToInt( oRenglon[ 4 ] );
                                       sReloj:= ZetaCommontools.PadLCar( trim ( oRenglon [ 3 ] ), 4, '0' );  //identificador de terminal
                                       sMes :=  Copy( oRenglon[ 1 ], 6, 2 );
                                       sDia :=  Copy( oRenglon[ 1 ], 9, 2 );
                                       if( iTipoChecada = 0 ) then   //C�digo de barra
                                       begin
                                            sIdCB:= trim ( oRenglon[ 0 ] );
                                            sRenglon := sReloj + K_RELLENO + TOKEN_ASISTENCIA + oRenglon[ 0 ]+ sMes + sDia + sHora + sMin + K_TIPO;
                                       end
                                       else if ( iTipoChecada = 1 ) then //Es Huella
                                            begin
                                                 sIdHuella := Trim ( oRenglon[ 0 ] );
                                                 FDataSet := CreateQuery( Format( GetScript( Q_SIST_GET_INFO_ID_HUELLA ),  [ sIdHuella ] )  );
                                                 try
                                                    with FDataSet do
                                                    begin
                                                         try
                                                            Active := True;
                                                            if( FDataSet.RecordCount > 0 ) then
                                                            begin
                                                                  sCM_CODIGO := FieldByName( 'CM_CODIGO' ).AsString;
                                                                  sCB_CODIGO := FieldByName( 'CB_CODIGO' ).AsString;
                                                                  sEmpleado := ZetaCommontools.PadLCar( sCB_CODIGO, 9, '0' );
                                                            end
                                                            else
                                                            begin
                                                                 Log.Advertencia(0,Format( 'L�nea #'+IntToStr( iLinea )+',No es posible identificar al empleado con n�mero de biom�trico[%s].',[ sIdHuella ] ) );
                                                            end;
                                                         finally
                                                                Active := False;
                                                         end;
                                                    end;
                                                 finally
                                                        FreeAndNil( FDataset );
                                                 end;
                                                 if strLleno( sCM_CODIGO ) then
                                                 begin
                                                      FDataSet := CreateQuery( Format( GetScript( Q_SIST_GET_CM_DIGITO ),  [ EntreComillas( sCM_CODIGO ) ] )  );
                                                      try
                                                         with FDataSet do
                                                         begin
                                                              try
                                                                 Active := True;
                                                                 if ( FDataSet.RecordCount > 0 ) then
                                                                    sCM_DIGITO := FieldByName( 'CM_DIGITO' ).AsString
                                                                 else
                                                                     Log.Advertencia(0,Format( 'L�nea #'+IntToStr( iLinea )+',No es posible identificar al empleado con n�mero de biom�trico[%s].',[ sIdHuella ] ) );
                                                              finally
                                                                     Active := False;
                                                              end;
                                                         end;
                                                      finally
                                                             FreeAndNil( FDataset );
                                                      end;
                                                      if strLleno( sEmpleado ) and
                                                         strLleno( sCM_DIGITO ) then
                                                      begin
                                                           sRenglon := sReloj + K_RELLENO + TOKEN_ASISTENCIA + sCM_DIGITO + sEmpleado + K_CREDENCIAL + sMes + sDia + sHora + sMin + K_TIPO;
                                                      end;
                                                 end;
                                            end
                                            else if ( iTipoChecada = 3 ) then   //Es proximidad
                                                 begin
                                                      sIdProx := IntToHex( strToIntDef( Trim( oRenglon[ 0 ] ), 0 ), 0 );
                                                      FDataSet := CreateQuery( Format( GetScript( Q_SIST_GET_INFO_ID_PROX ),  [ EntreComillas( sIdProx ) ] )  );
                                                      try
                                                         with FDataSet do
                                                         begin
                                                              try
                                                                 Active := True;
                                                                 if ( FDataSet.RecordCount > 0 ) then
                                                                 begin
                                                                      sCM_CODIGO := FieldByName( 'CM_CODIGO' ).AsString;
                                                                      sCB_CODIGO := FieldByName( 'CB_CODIGO' ).AsString;
                                                                      sEmpleado := ZetaCommontools.PadLCar( sCB_CODIGO , 9 ,'0' );
                                                                 end
                                                                 else
                                                                 begin
                                                                      Log.Advertencia(0,Format( 'L�nea #'+IntToStr( iLinea )+',No es posible identificar al empleado con n�mero de tarjeta[%s].',[sIdProx] ) );
                                                                 end;
                                                              finally
                                                                     Active := False;
                                                              end;
                                                         end;
                                                      finally
                                                             FreeAndNil( FDataset );
                                                      end;

                                                      if strLleno( sCM_CODIGO ) then
                                                      begin
                                                           FDataSet := CreateQuery( Format( GetScript( Q_SIST_GET_CM_DIGITO ),  [ EntreComillas( sCM_CODIGO ) ] )  );
                                                           try
                                                              with FDataSet do
                                                              begin
                                                                   try
                                                                      Active := True;
                                                                      if ( FDataSet.RecordCount > 0 ) then
                                                                         sCM_DIGITO := FieldByName( 'CM_DIGITO' ).AsString
                                                                      else
                                                                          Log.Advertencia(0,Format( 'L�nea #'+IntToStr( iLinea )+',No es posible identificar al empleado con n�mero de biom�trico[%s].',[ sIdHuella ] ) );
                                                                   finally
                                                                          Active := False;
                                                                   end;
                                                              end;
                                                           finally
                                                                  FreeAndNil( FDataset );
                                                           end;
                                                           if strLleno( sEmpleado ) and
                                                              strLleno( sCM_DIGITO ) then
                                                           begin
                                                                sRenglon := sReloj + K_RELLENO + TOKEN_ASISTENCIA + sCM_DIGITO + sEmpleado + K_CREDENCIAL + sMes + sDia + sHora + sMin + K_TIPO;
                                                           end;
                                                      end;
                                                 end;
                                       with cdsLista do   //proceso que agrega los registros al dataset por enviar VERIFICARMETODO
                                       begin
                                            Active := True;
                                            Agregar ( sRenglon );
                                            if ( iCont  <> oArchivo.Count-1 ) then
                                            begin
                                                 Next;
                                            end;
                                       end;
                                  end; //finlContinuar
                             end //fin del ciclo de numRegistros
                             else
                             begin
                                  Log.Advertencia(0,'L�nea #'+ IntToStr( iLinea )+ 'Error en la cantidad de campos esperados' ,'L�nea #'+IntToStr( iLinea )+ Format( ',La cantidad de campos del rengl�n es menor al m�nimo esperado [%d].', [ K_NUM_REGISTROS ] ) );
                             end;
                        end; // Fin del For
                     except
                           on Error: Exception do
                           begin
                                Log.Excepcion( 0, 'Error al exportar checadas del archivo.', Error );
                           end;
                     end;
                  finally
                         FreeAndNil( oRenglon );
                  end;
               finally
                      FreeAndNil( oArchivo );
               end;
               if( cdsLista.RecordCount > 0 ) then
                   Datos := cdsLista.Data
               else
                   log.Error(0,'No se puede procesar el archivo.','Verifique el formato del archivo.');
          end;// Open Process
          EmpresaActiva := Empresa;
          Result := CloseProcess;
          FreeAndNil(FDataSet);
          FreeAndNil( cdsLista );
     end;
     EndLog;
     SetComplete;
end;

// SYNERGY
function TdmServerSistema.ConsultaBitacoraBiometrica(Parametros: OleVariant): OleVariant;
var
   dFechaInicio, dFechaFin: TDateTime;
   sFiltro, sEmpresa, sMensaje, sTerminal: String;
begin
     with oZetaProvider do
     begin
          sFiltro := VACIO;
          EmpresaActiva := Comparte;
          AsignaParamList( Parametros );

          with ParamList do
          begin
               dFechaInicio := ParamByName( 'FechaInicio' ).AsDateTime;
               dFechaFin := ParamByName( 'FechaFin' ).AsDateTime;
               sEmpresa := ParamByName( 'Empresa' ).AsString;
               sTerminal := ParamByName( 'Terminal' ).AsString;
               sMensaje := ParamByName( 'Mensaje' ).AsString;
          end;

          if( dFechaInicio <> NullDateTime )then
              sFiltro := Format( '(WS_FECHA >= ''%s'')', [ DateToStrSQL( dFechaInicio ) ] );

          if( dFechaFin <> NullDateTime )then
              sFiltro := ConcatFiltros( sFiltro, Format( '(WS_FECHA < ''%s'')', [ DateToStrSQL( dFechaFin + 1 ) ] ) ); // Por el time stamp

          if StrLleno( sEmpresa )then
             sFiltro := ConcatFiltros( sFiltro, Format( '(CM_CODIGO = ''%s'')', [ sEmpresa ] ) );

          if StrLleno( sTerminal )then
             sFiltro := ConcatFiltros( sFiltro, Format( '(CH_RELOJ = ''%s'')', [ sTerminal ] ) );

          if StrLleno( sMensaje )then
             sFiltro := ConcatFiltros( sFiltro, Format( '( Upper( WS_MENSAJE ) like ''%s%s%s'')', [ '%', ansiUpperCase( sMensaje ), '%' ] ) );

          if StrLleno( sFiltro )then
             sFiltro := 'where ' + sFiltro;

          Result := OpenSQL( EmpresaActiva, Format( GetScript( Q_SIST_BIT_BIO ), [ sFiltro ] ), true );
     end;
     SetComplete;
end;

// SYNERGY
function TdmServerSistema.DepuraBitacoraBiometrica(Parametros: OleVariant): OleVariant;
var
   dInicio, dFin: TDateTime;
   sMensajeError, sEmpresa: string;
const
     K_FILTRO_EMPRESA = 'and CM_CODIGO=''%s''';
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          sMensajeError := VACIO;

          AsignaParamList( Parametros );
          with ParamList do
          begin
               dInicio := ParamByName( 'FechaInicio' ).AsDateTime;
               dFin := ParamByName( 'FechaFin' ).AsDateTime;
               sEmpresa := ParamByName( 'Empresa' ).AsString;
          end;

          Result := DZetaServerProvider.GetEmptyProcessResult( prSISTDepuraBitBiometrico );

          EmpiezaTransaccion;
          try
             if StrLleno( sEmpresa )then
                ExecSQL( EmpresaActiva, Format( GetScript( Q_SIST_DEPURA_BIT_BIO ), [ DateToStrSQL( dInicio ), DateToStrSQL( dFin + 1 ), Format( K_FILTRO_EMPRESA, [ sEmpresa ] ) ] ) )
             else
                 ExecSQL( EmpresaActiva, Format( GetScript( Q_SIST_DEPURA_BIT_BIO ), [ DateToStrSQL( dInicio ), DateToStrSQL( dFin + 1 ), VACIO ] ) );
             TerminaTransaccion( true );
             DZetaServerProvider.SetProcessOK( Result );
          except
                on Error: Exception do
                begin
                     TerminaTransaccion( false );
                     DZetaServerProvider.SetProcessError( Result );
                end;
          end;
     end;
     SetComplete;
end;

// SYNERGY
function TdmServerSistema.ObtenIdBioMaximo: Integer;
var
   oDs: TZetaCursor;
begin
     Result := 0;
     with oZetaProviderComparte do
     begin
          EmpresaActiva := Comparte;
          oDs := CreateQuery( GetScript( Q_SIST_OBTEN_MAX_BIO ) );
          try
             with oDs do
             begin
                  Active := true;
                  if not Eof then
                     Result := FieldByName( 'MAXIMO' ).AsInteger + 1
                  else
                      Result := 1;
                  Active := false;
             end;
          finally
                 FreeAndNil( oDs );
          end;
     end;
end;

// SYNERGY
function TdmServerSistema.ActualizaIdBio(const CodigoEmpresaLocal: WideString; Empleado, Id: Integer; const Grupo: WideString; Invitador: Integer; const Confidencialidad: WideString): WordBool;
var
   sEmpresa: string;
   FDataset: TZetaCursor;
   sStatus: string;
   dFechaBaja: TDate;

   function EmpresaDeEmpleado: string;
   begin
        FDataSet := oZetaProviderComparte.CreateQuery( Format( GetScript( Q_GET_EMPRESA_SIN_CONFIDENCIALIDAD ), [ EntreComillas( CodigoEmpresaLocal ) ] )  );
        try
           with FDataSet do
           begin
                try
                   Active := True;
                   result := FieldByName( 'CM_CODIGO' ).AsString;
                finally
                       Active := False;
                end;
           end;
        finally
               FreeAndNil( FDataset );
        end;
   end;

   function ExisteEmpleado: boolean;
   begin
        FDataSet := oZetaProviderComparte.CreateQuery( Format( GetScript( Q_OBTIENE_EMPLEADO_EMP_BIO ), [ EntreComillas( CodigoEmpresaLocal ), Empleado ] )  );
        try
           with FDataSet do
           begin
                try
                   Active := True;
                   result := ( FieldByName( 'CUANTOS' ).AsInteger > 0 );
                finally
                       Active := False;
                end;
           end;
        finally
               FreeAndNil( FDataset );
        end;
   end;

   procedure GetStatusEmpleado;
   begin
        FDataSet := oZetaProviderComparte.CreateQuery( Format( GetScript( Q_GET_STATUS_EMPLEADO ), [ EntreComillas( CodigoEmpresaLocal ), Empleado ] )  );
        try
           with FDataSet do
           begin
                try
                   Active := True;
                   sStatus := FieldByName( 'CB_ACTIVO' ).AsString;
                   dFechaBaja := FieldByName( 'CB_FEC_BAJ' ).AsDateTime;
                   if strVacio( sStatus ) then
                      sStatus := 'S';
                finally
                       Active := False;
                end;
           end;
        finally
               FreeAndNil( FDataset );
        end;
   end;

begin
     Result := True;
     with oZetaProviderComparte do
     begin
          EmpresaActiva := Comparte;
          EmpiezaTransaccion;
          try
             GetStatusEmpleado;
             // Borrando registro del empleado
             if ( Invitador = 0 ) then
             begin
                  if ExisteEmpleado then
                     ExecSQL( EmpresaActiva, Format( GetScript( Q_SIST_ACTUALIZA_BIO_BORRA ), [ CodigoEmpresaLocal ] ) + ' and CB_CODIGO = ' + IntToStr( Empleado ) + 'and IV_CODIGO = 0' )
                  else
                  begin
                       if strLleno( Confidencialidad ) then
                       begin
                            sEmpresa := EmpresaDeEmpleado;
                            if strLleno( sEmpresa ) then
                               ExecSQL( EmpresaActiva, Format( GetScript( Q_ACTUALIZA_ID_BIO_CONFIDENCIALIDAD ), [ EntreComillas( sEmpresa ) ] ) + ' and CB_CODIGO = ' + IntToStr( Empleado ) + 'and IV_CODIGO = 0' );
                       end
                       else
                           sEmpresa := VACIO;
                  end;
                  if( Id > 0 )then
                  begin
                       if strLleno( sEmpresa ) then
                       begin
                            ExecSQL( EmpresaActiva, Format( GetScript( Q_SIST_ACTUALIZA_BIO_AGREGA ), [ Id, Empleado, sEmpresa, Grupo, 0, sStatus, FechaToStr( dFechaBaja ) ] ) )  ;
                            ExecSQL( EmpresaActiva, Format( GetScript( Q_REINICIA_TERMINAL ), [ Format( 'where CB_CODIGO=%d and CM_CODIGO=''%s''',[ Empleado, sEmpresa ] ) ] ) );
                       end
                       else
                       begin
                            ExecSQL( EmpresaActiva, Format( GetScript( Q_SIST_ACTUALIZA_BIO_AGREGA ), [ Id, Empleado, CodigoEmpresaLocal, Grupo, 0, sStatus, FechaToStr( dFechaBaja ) ] ) )  ;
                            ExecSQL( EmpresaActiva, Format( GetScript( Q_REINICIA_TERMINAL ), [ Format( 'where CB_CODIGO=%d and CM_CODIGO=''%s''',[ Empleado, CodigoEmpresaLocal ] ) ] ) );
                       end;
                       EjecutaAndFree( Format( GetScript( Q_REINICIA_TERM_MSG_FECHA ), [ VACIO,VACIO ] ) );
                  end
                  else if( Id = 0 )then
                  begin
                       if strLleno( sEmpresa ) then
                       begin
                            ExecSQL( EmpresaActiva, Format( GetScript( Q_REINICIA_TERMINAL ), [ Format( 'where CB_CODIGO=%d and CM_CODIGO=''%s''',[ Empleado, sEmpresa ] ) ] ) );
                       end
                       else
                       begin
                            ExecSQL( EmpresaActiva, Format( GetScript( Q_REINICIA_TERMINAL ), [ Format( 'where CB_CODIGO=%d and CM_CODIGO=''%s''',[ Empleado, CodigoEmpresaLocal ] ) ] ) );
                       end;
                       sEmpresa := EmpresaDeEmpleado;
                       if strLleno( sEmpresa ) then
                          ExecSQL( EmpresaActiva, Format( GetScript( Q_BORRA_HUELLAGTI ), [ Empleado, sEmpresa ] ) );
                  end;
             end
             else if ( Invitador > 0 ) then
             begin
                  if ExisteEmpleado then
                     ExecSQL( EmpresaActiva, Format( GetScript( Q_SIST_ACTUALIZA_BIO_BORRA ), [ CodigoEmpresaLocal ] ) + ' and IV_CODIGO = ' + IntToStr( Invitador ) )
                  else
                  begin
                       if strLleno( Confidencialidad ) then
                       begin
                            sEmpresa := EmpresaDeEmpleado;
                            if strLleno( sEmpresa ) then
                            begin
                                 ExecSQL( EmpresaActiva, Format( GetScript( Q_ACTUALIZA_ID_BIO_CONFIDENCIALIDAD ), [ EntreComillas( sEmpresa ) ] ) + ' and IV_CODIGO = ' + IntToStr( Invitador ) );
                            end;
                       end;
                  end;
                  if( Id > 0 )then
                  begin
                       if strLleno( sEmpresa ) then
                       begin
                            ExecSQL( EmpresaActiva, Format( GetScript( Q_SIST_ACTUALIZA_BIO_AGREGA ), [ Id, Empleado, sEmpresa, Grupo, Invitador, sStatus, FechaToStr( dFechaBaja ) ] ) );
                            ExecSQL( EmpresaActiva, Format( GetScript( Q_SIST_ACTUALIZA_EMPLEADO ), [ Empleado, sEmpresa, Invitador ] ) );
                            ExecSQL( EmpresaActiva, Format( GetScript( Q_REINICIA_TERMINAL ), [ Format( 'where CB_CODIGO=%d and CM_CODIGO=''%s''',[ Empleado, sEmpresa ] ) ] ) );
                       end
                       else
                       begin
                            ExecSQL( EmpresaActiva, Format( GetScript( Q_SIST_ACTUALIZA_BIO_AGREGA ), [ Id, Empleado, CodigoEmpresaLocal, Grupo, Invitador, sStatus, FechaToStr( dFechaBaja ) ] ) );
                            ExecSQL( EmpresaActiva, Format( GetScript( Q_SIST_ACTUALIZA_EMPLEADO ), [ Empleado, CodigoEmpresaLocal, Invitador ] ) );
                            ExecSQL( EmpresaActiva, Format( GetScript( Q_REINICIA_TERMINAL ), [ Format( 'where CB_CODIGO=%d and CM_CODIGO=''%s''',[ Empleado, CodigoEmpresaLocal ] ) ] ) );
                       end;
                  end;
             end;

             TerminaTransaccion( true );
          except
                on Error : Exception do
                begin
                     RollBackTransaccion;
                     Result := False;
                end
          end;
     end;
     SetComplete;
end;

// SYNERGY
function TdmServerSistema.ConsultaListaGrupos(
  Empresa: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := OpenSQL( EmpresaActiva, Format(GetScript( Q_CONSULTA_LISTA_GRUPOS ),[Empresa]), true );
     end;
     SetComplete;
end;

// SYNERGY
function TdmServerSistema.GrabaSistListaGrupos(oDelta: OleVariant;
  UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant;
begin
     FUsuario := UsuarioTerminal;
     SetTablaInfo( eSistListaGrupos );
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
     end;
     SetComplete;
end;

// SYNERGY
function TdmServerSistema.ConsultaTermPorGrupo(const Grupo: WideString): OleVariant;
var
   sSQL: string;
const
     K_FILTRO = 'where GP_CODIGO = ''%s''';
begin
     with oZetaProvider do
     begin
          if StrLleno( Grupo )then
             sSQL := Format( GetScript( Q_CONSULTA_TERM_POR_GRUPO ), [ Format( K_FILTRO, [ Grupo ] )] )
          else
              sSQL := Format( GetScript( Q_CONSULTA_TERM_POR_GRUPO ), [ VACIO ] );

          EmpresaActiva := Comparte;
          Result := OpenSQL( EmpresaActiva, sSQL, true );
     end;
     SetComplete;
end;

// SYNERGY
function TdmServerSistema.GrabaTermPorGrupo(oDelta: OleVariant; UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant;
begin
     FUsuario := UsuarioTerminal;
     SetTablaInfo( eTermPorGrupo );
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
     end;
     SetComplete;
end;

// SYNERGY
procedure TdmServerSistema.GrabaListaTermPorGrupo( const Terminales: WideString; UsuarioTerminal: Integer; const Grupo, SinTerminales: WideString);
var
   sLista, sSinLista: TStringList;
   sBorrarTerminales: string;
   i: integer;
   FDataset: TZetaCursor;

   Procedure VerificaTerminal( sTerminal: string );
   begin
        with oZetaProvider do
        begin
             EmpresaActiva := Comparte;
             FDataset := CreateQuery( Format( GetScript( Q_VERIFICA_TERMINAL ), [ sTerminal, Grupo ] ) );
             try
                with FDataset do
                begin
                     Active := True;
                     if ( FieldByName( 'CUANTOS' ).AsInteger <> 0 ) then
                        sBorrarTerminales := ConcatString( sBorrarTerminales, sTerminal, ',' );
                     Active := False;
                end;
             finally
                    FreeAndNil( FDataset );
             end;
        end;
   end;

begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          EmpiezaTransaccion;
          try
             // Agrega nuevos registros
             sBorrarTerminales := VACIO;
             sLista := TStringList.Create;
             try
                sSinLista := TStringList.Create;
                try
                   sLista.CommaText := Terminales;
                   sSinLista.CommaText := SinTerminales;

                   { conocer si ya estaba y fue desmarcada la terminal }
                   for i := 0 to ( sSinLista.Count - 1 ) do
                   begin
                        VerificaTerminal( sSinLista[ i ] );
                   end;

                   { Borra los registros de la tabla de relaci�n de grupos }
                   ExecSQL( EmpresaActiva, Format( GetScript( Q_BORRA_LISTA_TERM_POR_GRUPO ), [ Grupo ] ) );

                   { Agrega los registros de la tabla de relaci�n de grupos }
                   for i := 0 to ( sLista.Count - 1 ) do
                   begin
                        ExecSQL( EmpresaActiva, Format( GetScript( Q_AGREGA_LISTA_TERM_POR_GRUPO ), [ sLista[ i ], Grupo ] ) );
                   end;

                   if strLleno( sBorrarTerminales ) then
                   begin
                        ExecSQL( EmpresaActiva, Format( GetScript( Q_BORRA_TERM_HUELLA_TODAS ), [ Format( 'where ( TE_CODIGO in ( %s ) )', [ sBorrarTerminales ] ) ] ) );
                        ExecSQL( EmpresaActiva, Format( GetScript( Q_REINICIA_TERM_MSG_SYNC ), [ Format( 'and ( TE_CODIGO in ( %s ) )', [ sBorrarTerminales ] ) ] ) );
                   end;
                finally
                        FreeAndNil( sSinLista );
                end;
             finally
                    FreeAndNil( sLista );
             end;
             TerminaTransaccion( True );
             FreeAndNil(sLista);
          except
                on Error : Exception do
                   RollBackTransaccion;
          end;
     end;
     SetComplete;
end;

// SYNERGY
function TdmServerSistema.AsignaNumBiometricos(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );

          FListaParametros := VACIO;
          FListaParametros := 'Asignaci�n de N�meros Biom�tricos y Grupo de Terminales [' + Paramlist.ParamByName( 'Grupo' ).AsString + ']';
     end;

     InitBroker;
     try
        FSinBiometrico := true;
        AsignaNumBiometricosBuildDataset;
        Result := AsignaNumBiometricosDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
        ClearBroker;
     end;
     SetComplete;
end;

// SYNERGY
function TdmServerSistema.AsignaNumBiometricosLista(Empresa, Lista, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );

          FListaParametros := VACIO;
          FListaParametros := 'Asignaci�n de N�meros Biom�tricos y Grupo de Terminales [' + Paramlist.ParamByName( 'Grupo' ).AsString + ']';
     end;

     cdsLista.Lista := Lista;
     Result := AsignaNumBiometricosDataset( cdsLista );
     SetComplete;
end;

// SYNERGY
function TdmServerSistema.EmpleadosSinBiometricosGetLista(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
     end;
     InitBroker;
     try
        FSinBiometrico := true;
        AsignaNumBiometricosBuildDataset;
        Result := SQLBroker.SuperReporte.GetReporte;
     finally
        ClearBroker;
     end;
     SetComplete;
end;

// SYNERGY
procedure TdmServerSistema.AsignaNumBiometricosBuildDataset;
begin
     with SQLBroker do
     begin
          Init( enEmpleado );
          with Agente do
          begin
               AgregaColumna( 'COLABORA.CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
               AgregaColumna( 'COLABORA.PRETTYNAME', True, Entidad, tgTexto, 93, 'PRETTYNAME' );
               AgregaColumna( 'COLABORA.CB_ACTIVO', True, Entidad, tgTexto, 1, 'CB_ACTIVO' );
               AgregaColumna( 'COLABORA.CB_ID_BIO', True, Entidad, tgNumero, 0, 'CB_ID_BIO' );
               AgregaColumna( 'COLABORA.CB_GP_COD', True, Entidad, tgTexto, 6, 'CB_GP_COD' );
               AgregaColumna( 'COLABORA.CB_NIVEL0', True, Entidad, tgTexto, 6, 'CB_NIVEL0' );
               AgregaFiltro( '( COLABORA.CB_ACTIVO = ''S'' )', True, Entidad );
               if FSinBiometrico then
                  AgregaFiltro( '( COLABORA.CB_ID_BIO = 0 )', True, Entidad )
               else
                   AgregaFiltro( '( COLABORA.CB_ID_BIO > 0 )', True, Entidad );
               AgregaOrden( 'COLABORA.CB_CODIGO', True, Entidad );
          end;
          AgregaRangoCondicion( oZetaProvider.ParamList );
          FiltroConfidencial( oZetaProvider.EmpresaActiva );
          BuildDataset( oZetaProvider.EmpresaActiva );
     end;
end;

// SYNERGY
function TdmServerSistema.AsignaNumBiometricosDataset(Dataset: TDataset): OleVariant;
var
   iEmpleado: TNumEmp;
   FDataset: TZetaCursor;
   iNumBio: Integer;
   sGrupo, sEmpresa, sConfidencialidad: string;
begin
     with oZetaProvider do
     begin
          if OpenProcess( prSISTAsignaNumBiometricos, Dataset.RecordCount, FListaParametros ) then
          begin
               try
                  FDataset := CreateQuery( GetScript( Q_AGREGA_NUM_BIOMETRICO ) );
                  with ParamList do
                  begin
                       sGrupo := ParamByName( 'Grupo' ).AsString;
                       sEmpresa := ParamByName( 'Empresa' ).AsString;
                  end;
                  try
                     with Dataset do
                     begin
                          First;
                          while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                          begin
                               iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                               sConfidencialidad := FieldByName( 'CB_NIVEL0' ).AsString;
                               iNumBio := ObtenIdBioMaximo;
                               if( iNumBio <> 0 )then
                               begin
                                    EmpiezaTransaccion;
                                    try
                                       ParamAsInteger( FDataset, 'CB_CODIGO', iEmpleado );
                                       ParamAsInteger( FDataset, 'CB_ID_BIO', iNumBio );
                                       ParamAsString( FDataset, 'CB_GP_COD', sGrupo );
                                       Ejecuta( FDataset );

                                       if ( ActualizaIdBio( sEmpresa, iEmpleado, iNumBio, sGrupo, 0, sConfidencialidad ) ) then
                                       begin
                                            TerminaTransaccion( TRUE );
                                            Log.Evento( clbSisBorrarBajas, iEmpleado, NullDateTime, Format( 'Se asign� el N�mero Biom�trico [%d] y Grupo de Terminales [%s] al Empleado', [ iNumBio, sGrupo ] ) );
                                       end
                                       else
                                       begin
                                            RollBackTransaccion;
                                            Log.ErrorGrave( iEmpleado, 'Error Al Asignar el N�mero Biom�trico del Empleado' );
                                       end;
                                    except
                                          on Error: Exception do
                                          begin
                                               RollBackTransaccion;
                                               Log.Excepcion( iEmpleado, 'Error Al Asignar el N�mero Biom�trico del Empleado', Error );
                                          end;
                                    end;
                               end;
                               Next;
                          end;
                     end;
                  finally
                      FreeAndNil( FDataSet );
                  end;
               except
                     on Error: Exception do
                     begin
                          Log.Excepcion( 0, 'Error Al Asignar el N�mero Biom�trico del Empleado', Error );
                     end;
               end;
          end;
          Result := CloseProcess;
     end;
     SetComplete;
end;

// SYNERGY
procedure TdmServerSistema.ReiniciaSincTerminales(const Lista: WideString);
begin
     {with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          EmpiezaTransaccion;
          try
             ExecSQL( EmpresaActiva, Format( GetScript( Q_REINICIA_SINC_TERMINALES ), [ Lista ] ) );
             TerminaTransaccion( True );
          except
                on Error : Exception do
                begin
                     TerminaTransaccion( False );
                     raise;
                end;
          end;
     end;
     SetComplete;}

     //OBSOLETO
end;

// SYNERGY
procedure TdmServerSistema.ActualizaTerminalesGTI(const Lista: WideString);
begin
     {with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          EmpiezaTransaccion;
          try
             ExecSQL( EmpresaActiva, Format( GetScript( Q_ACTUALIZA_TERMINALES_GTI ), [ Lista ] ) );
             TerminaTransaccion( True );
          except
                on Error : Exception do
                begin
                     TerminaTransaccion( False );
                     raise;
                end;
          end;
     end;
     SetComplete;}

     //OBSOLETO
end;

// SYNERGY
function TdmServerSistema.AsignaGrupoTerminales(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );

          FListaParametros := VACIO;
          FListaParametros := 'Asignaci�n de Grupo de Terminales [' + Paramlist.ParamByName( 'Grupo' ).AsString + ']';
     end;

     InitBroker;
     try
        FSinBiometrico := false;
        AsignaNumBiometricosBuildDataset;
        Result := AsignaGrupoTerminalesDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
        ClearBroker;
     end;
     SetComplete;
end;

// SYNERGY
function TdmServerSistema.AsignaGrupoTerminalesLista(Empresa, Lista, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );

          FListaParametros := VACIO;
          FListaParametros := 'Asignaci�n de Grupo de Terminales [' + Paramlist.ParamByName( 'Grupo' ).AsString + ']';
     end;

     cdsLista.Lista := Lista;
     Result := AsignaGrupoTerminalesDataset( cdsLista );
     SetComplete;
end;

// SYNERGY
function TdmServerSistema.EmpleadosBiometricosGetLista(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
     end;
     InitBroker;
     try
        FSinBiometrico := false;
        AsignaNumBiometricosBuildDataset;
        Result := SQLBroker.SuperReporte.GetReporte;
     finally
        ClearBroker;
     end;
     SetComplete;
end;

// SYNERGY
function TdmServerSistema.AsignaGrupoTerminalesDataset( Dataset: TDataset ): OleVariant;
var
   iEmpleado: TNumEmp;
   FDataset: TZetaCursor;
   iNumBio: Integer;
   sGrupo, sEmpresa, sConfidencialidad: string;
begin
     with oZetaProvider do
     begin
          if OpenProcess( prSISTAsignaGrupoTerminales, Dataset.RecordCount, FListaParametros ) then
          begin
               try
                  FDataset := CreateQuery( GetScript( Q_AGREGA_NUM_BIOMETRICO ) );
                  with ParamList do
                  begin
                       sGrupo := ParamByName( 'Grupo' ).AsString;
                       sEmpresa := ParamByName( 'Empresa' ).AsString;
                  end;

                  try
                     with Dataset do
                     begin
                          First;
                          while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                          begin
                               // En lugar de obtener el n�mero, se actualiza con el existente (solo se actualiza el grupo)
                               iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                               iNumBio := FieldByName( 'CB_ID_BIO' ).AsInteger;
                               sConfidencialidad := FieldByName( 'CB_NIVEL0' ).AsString;
                               EmpiezaTransaccion;
                               try
                                  ParamAsInteger( FDataset, 'CB_CODIGO', iEmpleado );
                                  ParamAsInteger( FDataset, 'CB_ID_BIO', iNumBio );
                                  ParamAsString( FDataset, 'CB_GP_COD', sGrupo );
                                  Ejecuta( FDataset );

                                  if ( ActualizaIdBio( sEmpresa, iEmpleado, iNumBio, sGrupo, 0, sConfidencialidad ) ) then
                                  begin
                                       TerminaTransaccion( TRUE );
                                       Log.Evento( clbSisBorrarBajas, iEmpleado, NullDateTime, Format( 'Se asign� el Grupo de Terminales [%s] al Empleado con N�mero Biom�trico [%d]', [ sGrupo, iNumBio ] ) );
                                  end
                                  else
                                  begin
                                       RollBackTransaccion;
                                       Log.ErrorGrave( iEmpleado, 'Error Al Asignar el Grupo de Terminales' );
                                  end;
                               except
                                     on Error: Exception do
                                     begin
                                          RollBackTransaccion;
                                          Log.Excepcion( iEmpleado, 'Error Al Asignar el Grupo de Terminales', Error );
                                     end;
                               end;
                               Next;
                          end;
                     end;
                  finally
                      FreeAndNil( FDataSet );
                  end;
               except
                     on Error: Exception do
                     begin
                          Log.Excepcion( 0, 'Error Al Asignar el Grupo de Terminales', Error );
                     end;
               end;
          end;
          Result := CloseProcess;
     end;
     SetComplete;
     oZetaProvider.EscribeBitacoraComparte( tbNormal, clbTerminalesGTI, NullDateTime, 'Relaci�n de Grupo-Terminales', 'Se actualiz� las relaciones del cat�logo de Grupo-Terminales', FUsuario);
end;

// SYNERGY
function TdmServerSistema.ConsultaMensaje(const Filtro: WideString): OleVariant;
begin
     SetTablaInfo( eMensajes );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Filtro;
          Result := GetTabla( Comparte );
     end;
     SetComplete;
end;

// SYNERGY
function TdmServerSistema.ConsultaMensajeTerminal(const Filtro: WideString): OleVariant;
begin
     SetTablaInfo( eMensajesTerm );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Filtro;
          Result := GetTabla( Comparte );
     end;
     SetComplete;
end;

// SYNERGY
function TdmServerSistema.ConsultaTerminal(const Filtro: WideString): OleVariant;
var
FDataset: TZetaCursor;
begin

     with oZetaProvider do
     begin
          TablaInfo.Filtro := Filtro;

          Result := OpenSQL( COMPARTE, Format(GetScript( Q_TERMINALES_STATUS), [EntreComillas(Filtro)]), TRUE );
     end;
     SetComplete;




end;

// SYNERGY
function TdmServerSistema.GrabaMensaje(oDelta: OleVariant; UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant;
begin
     FUsuario := UsuarioTerminal;
     SetTablaInfo( eMensajes );
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
     end;
     SetComplete;
end;

// SYNERGY
function TdmServerSistema.GrabaMensajeTerminal(oDelta: OleVariant; UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant;
begin
     FUsuario := UsuarioTerminal;
     SetTablaInfo( eMensajesTerm );
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
     end;
     SetComplete;
end;

// SYNERGY
function TdmServerSistema.GrabaTerminal(oDelta: OleVariant; UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant;
begin
     FUsuario := UsuarioTerminal;
     SetTablaInfo( eTerminales );
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
     end;
     SetComplete;
end;

// SYNERGY
function TdmServerSistema.ImportaTerminales(Parametros: OleVariant; UsuarioTerminal: Integer;
          out Bitacora: OleVariant): OleVariant;

   procedure Split (const Delimiter: Char; Input: string; const Strings: TStrings) ;
   begin
        Assert(Assigned(Strings)) ;
        Strings.Clear;
        Strings.Delimiter := Delimiter;
        Strings.DelimitedText := Input;
   end;

const K_SIN_TIPO_TERMINAL = 'insert into TERMINAL (TE_MAC, TE_ZONA, TE_APP, TE_ACTIVO, TE_ZONA_ES, TE_HUELLAS) values (''%s'', ''Pacific Standard Time'', 1, ''S'', ''Pacific Standard Time'', 0)';
      K_CON_TIPO_TERMINAL = 'insert into TERMINAL (TE_MAC, TE_ZONA, TE_APP, TE_ACTIVO, TE_ZONA_ES, TE_HUELLAS, TE_ID) values (''%s'', ''Pacific Standard Time'', 1, ''S'', ''Pacific Standard Time'', 0, %d)';
      K_UPD_TIPO_TERMINAL = 'UPDATE TERMINAL set TE_ID = %d WHERE TE_MAC = ''%s''';

var
   FDataSet, FExists: TZetaCursor;
   sListaTerminales: String;
   oListaTerminalesNuevas, oListaTerminalesAgregadas, oListaTerminalesId, oListaTerminalesAgregadasB: TStringList;
   i, iTotal, iTipoTerminal, iIndex: Integer;
   sTerminalesImportadas, sTerminal, sEncryptado: String;
   sMensajeBitacora: String;
   FLog: TStrings;
   lError, lContinuar : Boolean;
begin
     iTotal := 0;

     FLog := TStringList.Create;
     with FLog do
     begin
          Add( Format( '**** Inicio de Importaci�n de Terminales GTI: %s ****', [ FormatDateTime( 'dd/mmm/yyyy hh:nn:ss AM/PM', Now ) ] ) );
          Bitacora := Text;
     end;

     with oZetaProvider do
     begin
          EmpresaActiva:= Comparte;
          AsignaParamList( Parametros );
          with ParamList do
          begin
               sListaTerminales := ParamByName( 'ListaTerminales' ).AsString;
               FListaParametros := 'Importando terminales';
          end;

          {Result := GetEmptyProcessResult( prSISTImportaTerminales );

          if OpenProcess( prSISTImportaTerminales, 1, FListaParametros )then
          begin}

               try
                  oListaTerminalesAgregadas := TStringList.Create;
                  oListaTerminalesAgregadasB := TStringList.Create;
                  try
                     oListaTerminalesNuevas := TStringList.Create;
                     FDataSet := CreateQuery( 'select TE_MAC from TERMINAL order by TE_CODIGO' );
                     try
                        with FDataSet do
                        begin
                             Open;
                             while not EOF do
                             begin
                                  oListaTerminalesAgregadas.Append( Decrypt( Trim( FieldByName( 'TE_MAC' ).AsString ) ) );
                                  oListaTerminalesAgregadasB.Append( Trim( FieldByName( 'TE_MAC' ).AsString ) );
                                  Next;
                             end;
                             Close;
                        end;
                     finally
                            FreeAndNil( FDataSet );
                     end;

                     oListaTerminalesNuevas.CommaText := sListaTerminales;
                     for i:=0 to (oListaTerminalesNuevas.Count - 1) do
                     begin
                          iIndex := oListaTerminalesAgregadas.IndexOf( Copy(oListaTerminalesNuevas[i],0,ansipos('|', oListaTerminalesNuevas[i] )-1));
                          lContinuar := ( iIndex < 0 );

                          oListaTerminalesId := TStringList.Create;
                          try
                             Split('|', oListaTerminalesNuevas[i], oListaTerminalesId);
                             lError := False;
                             if oListaTerminalesId.Count > 1 then
                             begin
                                  if StrLleno( Trim (oListaTerminalesId[ 1 ]) ) then
                                  begin
                                       iTipoTerminal := StrToInt ( Trim (oListaTerminalesId[ 1 ] ) );
                                       if ( iTipoTerminal > 0 ) and ( iTipoTerminal < 6 ) then
                                       begin
                                            sTerminal := Trim ( oListaTerminalesId[ 0 ] );
                                            sEncryptado := Encrypt( sTerminal );

                                            if lContinuar then
                                               FDataSet := CreateQuery( Format( K_CON_TIPO_TERMINAL, [ sEncryptado, iTipoTerminal ]))
                                            else
                                                FDataSet := CreateQuery( Format( K_UPD_TIPO_TERMINAL, [ iTipoTerminal, oListaTerminalesAgregadasB[iIndex] ]));
                                       end
                                       else
                                           lError := True;
                                       end
                                  else
                                      lError := True;
                             end
                             else
                             begin
                                  if lContinuar then
                                  begin
                                       sTerminal := Trim( oListaTerminalesNuevas[ i ] );
                                       sEncryptado := Encrypt( sTerminal );
                                       FDataSet := CreateQuery( Format( K_SIN_TIPO_TERMINAL, [ sEncryptado ] ));
                                  end
                                  else
                                      lError := True;
                             end;

                             if Not lError then
                             begin
                                  try
                                     Ejecuta( FDataSet );
                                     oListaTerminalesAgregadas.Append( sTerminal );
                                     oListaTerminalesAgregadasB.Append ( sEncryptado );
                                     Inc( iTotal );

                                     sTerminalesImportadas := sTerminalesImportadas + CR_LF + sTerminal;
                                  finally
                                         FreeAndNil( FDataSet )
                                  end;
                             end;
                          finally
                                 FreeAndNil( oListaTerminalesId );
                          end;
                     end;
                     TerminaTransaccion( True );
                     Result := VarArrayOf([0, Ord(epsEjecutando), Ord(prSISTImportaTerminales), iTotal, 0, 0, Now, Now, 0, 0, 0]);
                     SetProcessOK( Result );
                  finally
                         FreeAndNil( oListaTerminalesNuevas );
                  end;
               finally
                      FreeAndNil( oListaTerminalesAgregadas );
                      FreeAndNil ( oListaTerminalesAgregadasB );
               end;

          {end;
          Result := CloseProcess;}
     end;

     if iTotal < 1 then
        sMensajeBitacora := Format( 'Se importaron %d terminales a los cat�logos del Sistema.', [ iTotal ] )
     else if iTotal = 1 then
          sMensajeBitacora := Format( 'Se import� 1 terminal a los cat�logos del Sistema.' + CR_LF + CR_LF + 'Terminal: %s', [ sTerminalesImportadas ] )
     else
         sMensajeBitacora := Format( 'Se importaron %d terminales a los cat�logos del Sistema.' + CR_LF + CR_LF + 'Terminales: %s', [ iTotal, sTerminalesImportadas ] );

     with FLog do
     begin
          Add (CR_LF + sMensajeBitacora);
          Add( Format(CR_LF + '**** Fin de Importaci�n de Terminales GTI:    %s ****', [ FormatDateTime( 'dd/mmm/yyyy hh:nn:ss AM/PM', Now ) ] ) );
          Bitacora := Text;
     end;
     FreeAndNil( FLog );

     SetComplete;
     oZetaProvider.EscribeBitacoraComparte( tbNormal, clbTerminalesGTI, NullDateTime, 'Importaci�n de terminales', sMensajeBitacora, UsuarioTerminal);     
end;

// SYNERGY
function TdmServerSistema.ExisteHuellaRegistrada(Empleado: Integer; const Empresa: WideString): WordBool;
var
   FDataSet: TZetaCursor;
begin
     Result := False;
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          if( Empleado > 0 )then
          begin
               FDataSet := CreateQuery( Format( GetScript( Q_EXISTE_HUELLA_EMP ), [ Empleado,Empresa ] ) );
               try
                  with FDataset do
                  begin
                       Active := True;
                       if not EOF then
                          Result := True;
                       Active := False;
                  end;
               finally
                      FreeAndNil( FDataSet );
               end;
          end;
     end;
     SetComplete;
end;

// SYNERGY
procedure TdmServerSistema.ReiniciaTerminal(Terminal: Integer; const Empresa: WideString);
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          if( Terminal > 0 )then
          begin
               // Limpia la relacion de la terminal con las huellas en TERM_HUELLA
               EjecutaAndFree( Format( GetScript( Q_BORRA_TERM_HUELLA_TODAS ), [ Format( 'where ( TE_CODIGO = %d )', [ Terminal ] ) ] ) );
               // Reinicia la fecha (nula) de la tabla de TERM_MSG
               EjecutaAndFree( Format( GetScript( Q_REINICIA_TERM_MSG_SYNC ), [ Format( 'and ( TE_CODIGO = %d )', [ Terminal ] ) ] ) );

               oZetaProvider.EscribeBitacoraComparte( tbNormal, clbTerminalesGTI, Now, 'Reinicio de Terminales', Format( 'Se reinicio la terminal %d', [ Terminal ] ), FUsuario);
          end;
          if ( Terminal = -1 )then // -1 representa reiniciar TODAS las terminales
          begin
               // Limpia la relacion de la terminal con las huellas en TERM_HUELLA
               EjecutaAndFree( Format( GetScript( Q_BORRA_TERM_HUELLA_TODAS ), [ VACIO ] ) );
               // Reinicia la fecha (nula) de la tabla de TERM_MSG
               EjecutaAndFree( Format( GetScript( Q_REINICIA_TERM_MSG_SYNC ), [ VACIO ] ) );

               oZetaProvider.EscribeBitacoraComparte( tbNormal, clbTerminalesGTI, Now, 'Reinicio de Terminales', 'Se reiniciaron todas las terminales', FUsuario);
          end
          else if (Terminal = -2)then
          begin
           EjecutaAndFree( GetScript( Q_REINICIA_TERM_MSG_DIAGNOSTICO ) );
               oZetaProvider.EscribeBitacoraComparte( tbNormal, clbTerminalesGTI, Now, 'Diagn�stico de Terminales', 'Se activ� el mensaje para diagn�stico de terminales.', FUsuario);
          end;

     end;
     SetComplete;
end;

// SYNERGY
procedure TdmServerSistema.SetAfterUpdateGruposDisp( Sender: TObject; SourceDS: TDataSet; DeltaDS: TZProviderClientDataSet; UpdateKind: TUpdateKind );
var
   sTitulo, sMensaje: string;
   lCambiosBit: Boolean;
begin
     lCambiosBit := False;
     if( UpdateKind = ukDelete )then
     begin
          sTitulo := 'Borrado de cat�logo de Grupos de Terminales';
          sMensaje := Format( 'Se elimin� el registro %s del cat�logo', [ ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'GP_CODIGO' ) ) ] );
          lCambiosBit := True;
     end
     else if( UpdateKind = ukModify )then
     begin
          sTitulo := 'Edici�n de cat�logo de Grupos de Terminales';
          sMensaje := Format( 'Se modific� el registro %s del cat�logo', [ ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'GP_CODIGO' ) ) ] );
          if( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'GP_CODIGO' ) ) <> ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'GP_CODIGO' ) ) )then
          begin
               sMensaje := sMensaje + CR_LF + 'C�digo' +
                           CR_LF + 'De: ' + ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'GP_CODIGO' ) ) +
                           CR_LF + 'A: ' + ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'GP_CODIGO' ) ) + CR_LF;
               lCambiosBit := True;
          end;
          if( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'GP_DESCRIP' ) ) <> ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'GP_DESCRIP' ) ) )then
          begin
               sMensaje := sMensaje + 'Descripci�n' +
                           CR_LF + 'De: ' + ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'GP_DESCRIP' ) ) +
                           CR_LF + 'A: ' + ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'GP_DESCRIP' ) );
               lCambiosBit := True;
          end;
     end
     else // UpdateKind = ukInsert
     begin
          sTitulo := 'Nuevo registro en cat�logo de Grupos de Terminales';
          sMensaje := 'Se agreg� registro al cat�logo' +
                      CR_LF + 'C�digo: ' + ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'GP_CODIGO' ) ) +
                      CR_LF + 'Descripci�n: ' + ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'GP_DESCRIP' ) );
          lCambiosBit := True;
     end;
     if lCambiosBit then
        oZetaProvider.EscribeBitacoraComparte( tbNormal, clbTerminalesGTI, Now, sTitulo, sMensaje, FUsuario);
end;

// SYNERGY
procedure TdmServerSistema.SetAfterUpdateMensajesPorTerminal( Sender: TObject; SourceDS: TDataSet; DeltaDS: TZProviderClientDataSet; UpdateKind: TUpdateKind );
var
   sTitulo, sMensaje: string;
   lCambiosBit: Boolean;
begin
     lCambiosBit := False;
     if( UpdateKind = ukDelete )then
     begin
          sTitulo := 'Borrado de cat�logo de Mensajes por Terminal';
          sMensaje := 'Se elimin� el mensaje ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'DM_CODIGO' ) ) ) +
                      ' de la terminal ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TE_CODIGO' ) ) );
          lCambiosBit := True;
     end
     else if( UpdateKind = ukModify )then
     begin
          sTitulo := 'Edici�n de cat�logo de Mensajes por Terminal';
          sMensaje := 'Se modific� el registro del mensaje ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'DM_CODIGO' ) ) ) +
                      ' de la terminal ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TE_CODIGO' ) ) );
          if( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TE_CODIGO' ) ) <> ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TE_CODIGO' ) ) )then
          begin
               sMensaje := sMensaje + CR_LF + 'Terminal' +
                           CR_LF + 'De: ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TE_CODIGO' ) ) ) +
                           CR_LF + 'A: ' + IntToStr( ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TE_CODIGO' ) ) ) + CR_LF;
               lCambiosBit := True;
          end;
          if( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'DM_CODIGO' ) ) <> ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'DM_CODIGO' ) ) )then
          begin
               sMensaje := sMensaje + CR_LF + 'Mensaje' +
                           CR_LF + 'De: ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'DM_CODIGO' ) ) ) +
                           CR_LF + 'A: ' + IntToStr( ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'DM_CODIGO' ) ) ) + CR_LF;
               lCambiosBit := True;
          end;
          if( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TM_SLEEP' ) ) <> ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TM_SLEEP' ) ) )then
          begin
               sMensaje := sMensaje + CR_LF + 'Espera' +
                           CR_LF + 'De: ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TM_SLEEP' ) ) ) +
                           CR_LF + 'A: ' + IntToStr( ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TM_SLEEP' ) ) ) + CR_LF;
               lCambiosBit := True;
          end;
          if( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TM_NEXT' ) ) <> ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TM_NEXT' ) ) )then
          begin
               sMensaje := sMensaje + CR_LF + 'Siguiente mensaje' +
                           CR_LF + 'De: ' + FechaCorta( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TM_NEXT' ) ) ) +
                           CR_LF + 'A: ' + FechaCorta( ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TM_NEXT' ) ) );
               lCambiosBit := True;
          end;
     end
     else // UpdateKind = ukInsert
     begin
          sTitulo := 'Nuevo registro en cat�logo de Mensajes por Terminal';
          sMensaje := 'Se agreg� registro al cat�logo' +
                      CR_LF + 'Terminal: ' + IntToStr( ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TE_CODIGO' ) ) ) +
                      CR_LF + 'Mensaje: ' + IntToStr( ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'DM_CODIGO' ) ) );
          lCambiosBit := True;
     end;
     if lCambiosBit then
        oZetaProvider.EscribeBitacoraComparte( tbNormal, clbTerminalesGTI, Now, sTitulo, sMensaje, FUsuario);
end;

// SYNERGY
procedure TdmServerSistema.SetAfterUpdateTerminales( Sender: TObject; SourceDS: TDataSet; DeltaDS: TZProviderClientDataSet; UpdateKind: TUpdateKind );
var
   sTitulo, sMensaje: string;
   lCambiosBit: Boolean;
begin
     lCambiosBit := False;
     if( UpdateKind = ukDelete )then
     begin
          sTitulo := 'Borrado de cat�logo de Terminales';
          sMensaje := 'Se elimin� la terminal ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TE_CODIGO' ) ) );
          lCambiosBit := True;
     end
     else if( UpdateKind = ukModify )then
     begin
          sTitulo := 'Edici�n de cat�logo de Terminales';
          sMensaje := 'Se modific� el registro de la terminal ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TE_CODIGO' ) ) );
          if( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TE_CODIGO' ) ) <> ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TE_CODIGO' ) ) )then
          begin
               sMensaje := sMensaje + CR_LF + 'C�digo' +
                           CR_LF + 'De: ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TE_CODIGO' ) ) ) +
                           CR_LF + 'A: ' + IntToStr( ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TE_CODIGO' ) ) ) + CR_LF;
               lCambiosBit := True;
          end;
          if( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TE_NOMBRE' ) ) <> ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TE_NOMBRE' ) ) )then
          begin
               sMensaje := sMensaje + CR_LF + 'Descripci�n' +
                           CR_LF + 'De: ' + ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TE_NOMBRE' ) ) +
                           CR_LF + 'A: ' + ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TE_NOMBRE' ) ) + CR_LF;
               lCambiosBit := True;
          end;
          if( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TE_APP' ) ) <> ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TE_APP' ) ) )then
          begin
               sMensaje := sMensaje + CR_LF + 'Aplicaci�n' +
                           CR_LF + 'De: ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TE_APP' ) ) ) +
                           CR_LF + 'A: ' + IntToStr( ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TE_APP' ) ) ) + CR_LF;
               lCambiosBit := True;
          end;
          if( ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TE_ZONA' ) ) <> ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TE_ZONA' ) ) )then
          begin
               sMensaje := sMensaje + CR_LF + 'Zona' +
                           CR_LF + 'De: ' + ZetaServerTools.CampoOldAsVar( DeltaDs.FieldByName( 'TE_ZONA' ) ) +
                           CR_LF + 'A: ' + ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TE_ZONA' ) );
               lCambiosBit := True;
          end;
     end
     else // UpdateKind = ukInsert
     begin
          sTitulo := 'Nuevo registro en cat�logo de Terminales';
          sMensaje := 'Se agreg� registro al cat�logo' +
                      CR_LF + 'Terminal: ' + IntToStr( ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TE_CODIGO' ) ) ) +
                      CR_LF + 'Descripci�n: ' + IntToStr( ZetaServerTools.CampoAsVar( DeltaDs.FieldByName( 'TE_NOMBRE' ) ) );
          lCambiosBit := True;
     end;
     if lCambiosBit then
        oZetaProvider.EscribeBitacoraComparte( tbNormal, clbTerminalesGTI, Now, sTitulo, sMensaje, FUsuario);
end;

// SYNERGY
function TdmServerSistema.ExisteEmpleadoEnGrupo(const Grupo: WideString): WordBool;
var
   FDataSet: TZetaCursor;
begin
     Result := True;
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          if StrLleno( Grupo )then
          begin
               FDataSet := CreateQuery( Format( GetScript( Q_EXISTE_EMPLEADO_EN_GRUPO ), [ Grupo ] ) );
               try
                  with FDataset do
                  begin
                       Active := True;
                       if not EOF then
                          Result := True
                       else
                           Result := False;
                       Active := False;
                  end;
               finally
                      FreeAndNil( FDataSet );
               end;
          end;
     end;
     SetComplete;
end;

// BIOMETRICO
function TdmServerSistema.ObtieneTemplates(Parametros: OleVariant): OleVariant;
var
   sScript, sMensaje, sTitle : String;
begin
     sMensaje := VACIO;
     sTitle := VACIO;

     with oZetaProvider do
     begin
          AsignaParamList( Parametros );
          if (ParamList.IndexOf('ComputerName') > 0) then
               sScript := Format( GetScript( Q_HUELLA_OBTENER_MAQUINA ) , [ ParamList.ParamByName('ComputerName').AsString ] )
          else
          begin
              sScript := GetScript( Q_HUELLA_OBTENER_TODOS );
              {if (ParamList.IndexOf('FechaPivote') > 0) then
                   sScript := sScript + ' WHERE HU_TIMEST >= ' + DateToStrSQLC( ParamList.ParamByName('FechaPivote').AsDateTime ) ;}
          end;

          EmpresaActiva := Comparte;
          Result := OpenSQL( EmpresaActiva, sScript, true );
     end;
     SetComplete;
end;

// BIOMETRICO
procedure TdmServerSistema.EliminaHuellas(const Empresa: WideString; Empleado, Invitador, Usuario, Tipo: Integer);
const K_ELIMINAR_HUELLAS = ' AND HU_INDICE < 11';
      K_ELIMINAR_FACIAL = ' AND HU_INDICE = 11';
var
   sQuery, sTitle, sMensaje, sQueryTerminal: String;
   FDataset: TZetaCursor;

   function EmpresaDeEmpleado: string;
   begin
        with oZetaProviderComparte do
        begin
             EmpresaActiva := Comparte;
             FDataSet := CreateQuery( Format( GetScript( Q_GET_EMPRESA_SIN_CONFIDENCIALIDAD ), [ EntreComillas( Empresa ) ] )  );
             try
                with FDataSet do
                begin
                     try
                        Active := True;
                        result := FieldByName( 'CM_CODIGO' ).AsString;
                        if strVacio( Result ) then
                           Result := Empresa;
                     finally
                            Active := False;
                     end;
                end;
             finally
                    FreeAndNil( FDataset );
             end;
        end;
   end;

begin
     if Tipo = K_TERMINAL_HUELLA then
     begin
          sTitle := 'Eliminaci�n de Huellas';
          sQueryTerminal := K_ELIMINAR_HUELLAS;
     end
     else
     begin
          sTitle := 'Eliminaci�n Facial';
          sQueryTerminal := K_ELIMINAR_FACIAL;
     end;

     sMensaje := ' Empleado: ' + IntToStr( Empleado );
     sQuery := Format( GetScript( Q_HUELLA_BORRA ),[ EmpresaDeEmpleado ]);

     if ( Invitador <> 0 ) then
     begin
          sMensaje := sMensaje + CR_LF + ' Invitador: ' + IntToStr( Invitador );
          sQuery := sQuery + ' and IV_CODIGO = ' + IntToStr( Invitador ) + ' ' + sQueryTerminal;
     end
     else
          sQuery := sQuery + ' and CB_CODIGO = ' + IntToStr( Empleado ) + ' and IV_CODIGO = 0' + ' ' + sQueryTerminal;

     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          EjecutaAndFree( sQuery );
          EscribeBitacoraComparte(tbNormal, clbBiometrico, Now, sTitle, sTitle + CR_LF + sMensaje, Usuario);
     end;
     SetComplete;
end;

// BIOMETRICO
procedure TdmServerSistema.InsertaHuella(Parametros: OleVariant);
var
   sQuery, sTitle, sMensaje, sEmpresa, sHuella, sComputadora : String;
   iEmpleado, iDedo, iInvitador, iUsuario : Integer;
   FDataset: TZetaCursor;

   function EmpresaDeEmpleado( sCodigoEmpresaEmpleado: string ): string;
   begin
        with oZetaProviderComparte do
        begin
             EmpresaActiva := Comparte;
        end;
        FDataSet := oZetaProviderComparte.CreateQuery( Format( GetScript( Q_GET_EMPRESA_SIN_CONFIDENCIALIDAD ), [ EntreComillas( sCodigoEmpresaEmpleado ) ] )  );
        try
           with FDataSet do
           begin
                try
                   Active := True;
                   result := FieldByName( 'CM_CODIGO' ).AsString;
                finally
                       Active := False;
                end;
           end;
        finally
               FreeAndNil( FDataset );
        end;
   end;

begin
     sTitle := 'Enrolamiento de Huella';
     with oZetaProvider do
     begin
          AsignaParamList( Parametros );

          sEmpresa     := EmpresaDeEmpleado( ParamList.ParamByName('Empresa').AsString );
          iEmpleado    := ParamList.ParamByName('Empleado').AsInteger;
          iDedo        := ParamList.ParamByName('Dedo').AsInteger;
          sHuella    := ParamList.ParamByName('Huella').AsString;
          iInvitador   := ParamList.ParamByName('Invitador').AsInteger;
          sComputadora := ParamList.ParamByName('Computadora').AsString;
          iUsuario     := ParamList.ParamByName('Usuario').AsInteger;

          sMensaje := ' Empleado: ' + IntToStr( iEmpleado );
          sQuery := Format( GetScript( Q_HUELLA_INSERTA ), [ sEmpresa, iEmpleado, iDedo, sHuella, iInvitador, sComputadora ] );

          if ( iInvitador <> 0 ) then
             sMensaje := sMensaje + CR_LF + ' Invitador: ' + IntToStr( iInvitador );

          sMensaje := sMensaje + CR_LF + ' �ndice: ' + IntToStr( iDedo );

          with oZetaProvider do
          begin
               EmpresaActiva := Comparte;
               EjecutaAndFree( sQuery );
               EscribeBitacoraComparte(tbNormal, clbBiometrico, Now, sTitle, sTitle + CR_LF + sMensaje, iUsuario);
          end;
     end;
     SetComplete;
end;

function TdmServerSistema.ReportaHuella( Parametros: OleVariant ): OleVariant;
var
   sQuery : String;
   sRenglones : TStringList;
   i : Integer;

   function ExisteParametro( sNombreParametro : String ) : Boolean;
   begin
        Result := ( oZetaProvider.ParamList.IndexOf(sNombreParametro) >= 0 );
   end;

   procedure Split (const Delimiter: Char; Input: string; const Strings: TStrings) ;
   begin
        Assert(Assigned(Strings)) ;
        Strings.Clear;
        Strings.Delimiter := Delimiter;
        Strings.DelimitedText := Input;
   end;

begin
     with oZetaProvider do
     begin
          AsignaParamList( Parametros );

          if ( ExisteParametro( 'ComputerName' ) ) then
          begin
               EmpresaActiva := Comparte;
               if ( ExisteParametro( 'HuellaId' ) ) then
               begin
                    sRenglones := TStringList.Create;
                    try
                       Split(',', ParamList.ParamByName('HuellaId').AsString, sRenglones) ;
                       if ( sRenglones.Count > 0 ) then
                            for i := 0 to sRenglones.Count - 1 do
                            begin
                                 sQuery := Format( GetScript( Q_HUELLA_REPORTA_INSERTA ), [ ParamList.ParamByName('ComputerName').AsString, sRenglones[i] ]);
                                 EjecutaAndFree( sQuery );
                            end;
                    finally
                           FreeAndNil(sRenglones);
                    end;
               end
               else
               begin
                    sQuery := Format( GetScript( Q_HUELLA_REPORTA_ELIMINA ), [ ParamList.ParamByName('ComputerName').AsString ]);
                    EjecutaAndFree( sQuery );
               end;
          end
     end;
     SetComplete;
end;

function TdmServerSistema.ExisteHuellaInvitador(const Empresa: WideString; Invitador: Integer): WordBool;
var
   FDataSet: TZetaCursor;
   sEmpresaSinConfidencialidad: string;

   function EmpresaDeEmpleado: string;
   begin
        FDataSet := oZetaProvider.CreateQuery( Format( GetScript( Q_GET_EMPRESA_SIN_CONFIDENCIALIDAD ), [ EntreComillas( Empresa ) ] )  );
        try
           with FDataSet do
           begin
                try
                   Active := True;
                   result := FieldByName( 'CM_CODIGO' ).AsString;
                finally
                       Active := False;
                end;
           end;
        finally
               FreeAndNil( FDataset );
        end;
   end;

begin
     Result := False;
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          if( Invitador > 0 )then
          begin
               sEmpresaSinConfidencialidad := EmpresaDeEmpleado;
               if strLleno( sEmpresaSinConfidencialidad ) then
               begin
                    FDataSet := CreateQuery( Format( GetScript( Q_EXISTE_HUELLA_INV ), [ sEmpresaSinConfidencialidad, Invitador ] ) );
                    try
                       with FDataset do
                       begin
                            Active := True;
                            if not EOF then
                               Result := True;
                            Active := False;
                       end;
                    finally
                           FreeAndNil( FDataSet );
                    end;
               end;
          end;
     end;
     SetComplete;
end;

{$ENDIF}

procedure TdmServerSistema.AddField(DataSet: TDataSet; FieldName: string; Size: Integer);
var
  AddedField: TStringField;
begin
  if Assigned(DataSet.FindField(FieldName)) then
    Exit;
  try
    AddedField := TStringField.Create(DataSet);
    AddedField.FieldKind := fkData;
    AddedField.FieldName := FieldName;
    AddedField.Size := Size;
    AddedField.DataSet := DataSet;
  Except
    on e: Exception do
    // Pon aqui tu mensaje de errr
  end;
end;


function TdmServerSistema.GetBasesDatos(const CM_DATOS, CM_USRNAME, CM_PASSWRD: WideString): OleVariant;
var
   oZetaProviderCompany: TdmZetaServerProvider;
   sUserName, sPassword: String;
begin
     oZetaProviderCompany := TdmZetaServerProvider.Create(Nil);
     with oZetaProvider do
     begin
       EmpresaActiva := Comparte;
      try
        // Si no se env�a usuario, obtener el usuario de Base de Datos default del sistema
        if (Trim (CM_USRNAME) = VACIO) then
        begin
            sUserName :=  GetSQLUserName;
            sPassword :=  GetSQLUserPassword;
        end
        else
        begin
            sUserName := CM_USRNAME;
            sPassword := CM_PASSWRD;
        end;

        try
          oZetaProviderCompany.EmpresaActiva := VarArrayOf([CM_DATOS,
          sUserName,  sPassword, oZetaProvider.UsuarioActivo]);

          with oZetaProviderCompany do
          begin
            Result:= oZetaProviderCompany.OpenSQL( oZetaProviderCompany.EmpresaActiva, GetScript( Q_GET_BASES_DATOS ), True );
          end;

        except
          raise Exception.Create( Format ('No es posible conectarse a: %s. Por favor, verifique su informaci�n.', [CM_DATOS]) );
        end;

      finally
           FreeAndNil(oZetaProviderCompany);
      end;
      end;
     SetComplete;
end;

function TdmServerSistema.ConsultaConfBD(Tipo: Integer): OleVariant;
var
   cdsBaseDatos, cdsDBSize, cdsVersion, cdsEmpleados : TClientDataset;
   oZetaProviderCompany: TdmZetaServerProvider;
   dsBaseDatos: TZetaCursor;
   sMensajeError : String;
   sWhere: String;
   VersionDatos:Integer;

   procedure SetTipoBD(DB_TIPO:Integer);
   var
     fDBTipo : eTipoCompany;
   begin
        fDBTipo := eTipoCompany(DB_TIPO);
        case fDBTipo of
            tc3Datos, tc3Prueba, tcPresupuesto:
              begin
                  VersionDatos := K_GLOBAL_VERSION_DATOS;
              end;
            tcRecluta:
            begin
                 VersionDatos := 78;
            end;
            tcVisitas:
            begin
                 VersionDatos := 12;
            end;
            tcOtro:
            begin
                 VersionDatos := K_GLOBAL_VERSION_DATOS;
            end;
        end;
   end;


begin
     sWhere := '';
     cdsBaseDatos := TClientDataset.Create(oZetaProvider);
     cdsDBSize := TClientDataset.Create(oZetaProvider);
     cdsVersion   := TClientDataset.Create(oZetaProvider);
     cdsEmpleados := TClientDataset.Create(oZetaProvider);

     oZetaProviderCompany := TdmZetaServerProvider.Create(Nil);
     sMensajeError        := '';

     with oZetaProvider do begin
       EmpresaActiva := Comparte;

       with cdsBaseDatos do
         try
            if (tipo <> 0 ) then
            begin
                sWhere := Format (' WHERE DB_TIPO = %d ',  [tipo]);
                // sWhere := Format (' AND DB_TIPO = %d ',  [tipo]);
            end;

            Data := OpenSQL(EmpresaActiva, GetScript(Q_CONSULTA_CONFIG_DBINFO)+ sWhere, True);
            while not cdsBaseDatos.Eof do
            begin
             try
               //dsBaseDatos := CreateQuery(Format(GetScript(Q_COMPANYS_LIST_DB), ['DB_DATOS = ' + EntreComillas(cdsBaseDatos.FieldByName('DB_DATOS').AsString) + '']));
               //dsBaseDatos.Active := True;
               //Si no tiene usuario y contrase�a evita conectarse , para bd recien migradas
               if StrLLeno(cdsBaseDatos.FieldByName( 'DB_USRNAME' ).AsString) and StrLleno(FieldByName( 'DB_PASSWRD' ).AsString) then
               begin
                    oZetaProviderCompany := TdmZetaServerProvider.Create(Nil);
                    with cdsBaseDatos do
                    begin
                         oZetaProviderCompany.EmpresaActiva := VarArrayOf( [FieldByName('DB_DATOS').AsString,
                                                                                 FieldByName('DB_USRNAME').AsString,
                                                                                 FieldByName('DB_PASSWRD').AsString,
                                                                                 0   ] );
                    end;
                    if not (cdsBaseDatos.state in [dsInsert, dsEdit ]) then
                        cdsBaseDatos.Edit;

                     with cdsDBSize do
                     begin
                       try
                         Data := oZetaProviderCompany.OpenSQL(oZetaProviderCompany.EmpresaActiva,
                           GetScript(Q_CONSULTA_DB_SIZE), True);
                           cdsBaseDatos.FieldByName('DB_SIZE').AsString := FieldByName('DB_SIZE').AsString;
                       except
                         cdsBaseDatos.FieldByName('DB_SIZE').AsString := '';
                         cdsBaseDatos.FieldByName('DB_ERROR').AsString := 'No es posible obtener el tama�o de la base de datos';
                       end;
                     end;

                     with cdsVersion do
                     begin
                       try
                          SetTipoBD(cdsBaseDatos.FieldByName('DB_TIPO').AsInteger);
                         Data := oZetaProviderCompany.OpenSQL(oZetaProviderCompany.EmpresaActiva,
                           Format(GetScript(Q_CONSULTA_VERSION), [VersionDatos]), True);
                           cdsBaseDatos.FieldByName('DB_VERSION').AsString := FieldByName('DB_VERSION').AsString;
                       except
                           cdsBaseDatos.FieldByName('DB_VERSION').AsString := '';
                         cdsBaseDatos.FieldByName('DB_ERROR').AsString := 'No es posible obtener la versi�n de la base de datos';
                       end;
                     end;

                     with cdsEmpleados do
                     begin
                       try
                         Data := oZetaProviderCompany.OpenSQL(oZetaProviderCompany.EmpresaActiva,
                           GetScript(Q_CONSULTA_DB_EMPLEADOS), True);
                         cdsBaseDatos.FieldByName('DB_EMPLEADOS').AsInteger := FieldByName('EMPLEADOS').AsInteger;
                       except
                         cdsBaseDatos.FieldByName('DB_EMPLEADOS').AsInteger := 0;
                       end;
                     end;
                     cdsBaseDatos.FieldByName('DB_OK').AsString := K_GLOBAL_SI;
               end
               else
               begin
                    with cdsBaseDatos do
                    begin
                         if not (state in [dsInsert, dsEdit ]) then
                            Edit;
                         Edit;
                         FieldByName('DB_OK').AsString := K_GLOBAL_NO;
                         FieldByName('DB_ERROR').AsString := 'No es posible conectarse a Base de Datos, falta definir usuario y/o clave';
                    end;
               end;
             except
               on E: Exception do begin
                 if not (cdsBaseDatos.state in [dsInsert, dsEdit ]) then
                    cdsBaseDatos.Edit;
                 cdsBaseDatos.FieldByName('DB_OK').AsString := K_GLOBAL_NO;
                 cdsBaseDatos.FieldByName('DB_ERROR').AsString := E.Message;
               end;
             end;
             //
             cdsBaseDatos.Post;
             // ----- -----

             Next;
           end;

           MergeChangeLog;
           // ----- -----
           Result := Data;
           //
           Active := False;
           // ----- -----

         finally
           FreeAndNil(oZetaProviderCompany);
           FreeAndNil(cdsDBSize);
           FreeAndNil(cdsVersion);
           FreeAndNil(cdsEmpleados);
           // FreeAndNil(dsBaseDatos);
         end;

     end;
     SetComplete;
end;

function TdmServerSistema.GrabaBD(oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( eBaseDatos );
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          with TablaInfo do
          begin
               BeforeUpdateRecord := BeforeUpdateDB;
          end;
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
     end;
     SetComplete;
end;

procedure TdmServerSistema.BeforeUpdateDB(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
var
  FDataSet: TZetaCursor;
  sUserName, sPassword: String;

  // Validaci�n de Base de Datos de Empleados
  oConfigServer : TDBConfigServer;
  oZetaProviderValidacion: TdmZetaServerProvider;
  oEmpresa : TEmpresaConfig;
  sAdvertencia : string;

begin
     with oZetaProvider do
     begin
            EmpresaActiva := Comparte;
            Applied := TRUE;

            sUserName := CampoAsVar (DeltaDS.FieldByName('DB_USRNAME'));
            sPassword := CampoAsVar (DeltaDS.FieldByName('DB_PASSWRD'));

            if CampoAsVar (DeltaDS.FieldByName('DB_USRDFLT')) = K_GLOBAL_SI then
            begin
                sUserName :=  GetSQLUserName;
                sPassword :=  GetSQLUserPassword;
            end;

            // ===================================================================================================
            //  VALIDACI�N DE BASE DE DATOS DE EMPLEADOS
            if UpdateKind in [ ukInsert, ukModify ]  then
            begin
              try
                sAdvertencia := '';
                oZetaProviderValidacion := TdmZetaServerProvider.Create (nil);
                oConfigServer := TDBConfigServer.Create(oZetaProviderValidacion, oAutoServer);

                oEmpresa :=  TEmpresaConfig.Create;
                oEmpresa.Codigo := CampoAsVar(DeltaDS.FieldByName('DB_CODIGO'));
                oEmpresa.Datos  := CampoAsVar(DeltaDS.FieldByName('DB_DATOS'));
                oEmpresa.ControlReal := ObtieneElemento(lfTipoCompany, CampoAsVar(DeltaDS.FieldByName('DB_TIPO')));
                oEmpresa.Control := ObtieneElemento(lfTipoCompany, CampoAsVar(DeltaDS.FieldByName('DB_TIPO')));
                if oEmpresa.Control = ObtieneElemento(lfTipoCompany, ord(tc3Prueba)) then
                  oEmpresa.Control :=  ObtieneElemento(lfTipoCompany, ord(tc3Datos));
                oEmpresa.Digito := '';
                oEmpresa.OldCodigo := DeltaDS.FieldByName('DB_CODIGO').OldValue;
                oEmpresa.OldDatos := DeltaDS.FieldByName('DB_DATOS').OldValue;
                if DeltaDS.FieldByName('DB_CTRL_RL').OldValue = null then
                  oEmpresa.OldControlReal := ''
                else
                  oEmpresa.OldControlReal := DeltaDS.FieldByName('DB_CTRL_RL').OldValue;
                if DeltaDS.FieldByName('DB_CONTROL').OldValue = null then
                  oEmpresa.OldControl := ''
                else
                  oEmpresa.OldControl := DeltaDS.FieldByName('DB_CONTROL').OldValue;
                if DeltaDS.FieldByName('DB_CHKSUM').OldValue = null then
                  oEmpresa.OldCheckSum := ''
                else
                  oEmpresa.OldCheckSum := DeltaDS.FieldByName('DB_CHKSUM').OldValue;

                oConfigServer.BeforeEmpresaObjPost( oEmpresa, UpdateKind,  sAdvertencia );

                //
                // Actualizar Delta:
                DeltaDS.FieldByName('DB_CONTROL').NewValue := oEmpresa.Control;
                if oEmpresa.Control = ObtieneElemento(lfTipoCompany, ord(tc3Prueba)) then
                begin
                  DeltaDS.FieldByName('DB_CONTROL').NewValue :=  ObtieneElemento(lfTipoCompany, ord(tc3Datos));
                  oEmpresa.Control := ObtieneElemento(lfTipoCompany, ord(tc3Datos));
                end;
                DeltaDS.FieldByName('DB_CTRL_RL').NewValue := oEmpresa.ControlReal;
                DeltaDS.FieldByName('DB_CHKSUM').NewValue := oEmpresa.CheckSum;

              finally
                // FreeAndNil (oZetaProviderValidacion);
                // FreeAndNil (oConfigServer);
                // FreeAndNil (oEmpresa);
              end;
            end;
            // ===================================================================================================

            if ( UpdateKind in [ ukInsert ] ) then
            begin
                  ExecSQL( EmpresaActiva, Format( GetScript( Q_INSERT_DB ), [ CampoAsVar(DeltaDS.FieldByName('DB_CODIGO')),
                        CampoAsVar (DeltaDS.FieldByName('DB_DESCRIP')), CampoAsVar (DeltaDS.FieldByName('DB_CONTROL')),
                        DeltaDS.FieldByName('DB_TIPO').AsInteger, CampoAsVar (DeltaDS.FieldByName('DB_DATOS')),
                        sUserName, sPassword,
                        CampoAsVar (DeltaDS.FieldByName('DB_CTRL_RL')),
                        CampoAsVar (DeltaDS.FieldByName('DB_CHKSUM')),
                        CampoAsVar (DeltaDS.FieldByName('DB_ESPC')) ] ) );
                  // Hacer aqu� el object Post
                  oConfigServer.AfterEmpresaObjPost( oEmpresa );
            end
            else if ( UpdateKind in [ ukModify ] ) then
            begin
                  ExecSQL( EmpresaActiva, Format( GetScript( Q_UPDATE_DB ), [CampoAsVar (DeltaDS.FieldByName('DB_CODIGO')),
                        CampoAsVar (DeltaDS.FieldByName('DB_DESCRIP')), CampoAsVar (DeltaDS.FieldByName('DB_CONTROL')),
                        CampoAsVar (DeltaDS.FieldByName('DB_TIPO')), CampoAsVar (DeltaDS.FieldByName('DB_DATOS')),
                        sUserName, sPassword, CampoAsVar (DeltaDS.FieldByName('DB_CTRL_RL')),
                        CampoAsVar (DeltaDS.FieldByName('DB_CHKSUM')),
                        CampoAsVar (DeltaDS.FieldByName('DB_ESPC')),
                        DeltaDS.FieldByName('DB_CODIGO').OldValue ] ));
                  // Hacer aqu� el object Post
                  oConfigServer.AfterEmpresaObjPost( oEmpresa );
            end
            else
            begin
                FDataSet := CreateQuery( Format( GetScript( Q_EMPRESAS_CON_DB ), [ CampoAsVar (DeltaDS.FieldByName('DB_CODIGO')) ] ) );
                try
                  with FDataSet do
                  begin
                       Active := True;
                       if (Fields[ 0 ].AsInteger > 0) then
                       begin
                          Active := False;
                          raise Exception.Create(
                            Format ('No se puede eliminar la base de datos ''%s''. Se encuentra asignada a una o m�s empresas.',
                             [CampoAsVar (DeltaDS.FieldByName('DB_DESCRIP'))]));
                       end
                       else
                       begin
                           try
                              EmpiezaTransaccion;
                              ExecSQL( EmpresaActiva, Format('DELETE FROM DB_INFO WHERE DB_CODIGO = ''%s'' ', [DeltaDS.FieldByName('DB_CODIGO').AsString ] ) );
                              TerminaTransaccion( TRUE );
                           except
                             on Error: Exception do
                             begin
                                 TerminaTransaccion( FALSE );
                                 raise;
                             end;
                           end;
                       end;
                  end;
                finally
                    FreeAndNil(FDataset);
                end;
            end;
     end;
     SetComplete;
end;

function TdmServerSistema.GetEmpresasSistema(GrupoActivo: Integer): OleVariant;
const
     K_COMPARTE_CODIGO = 'XCOMPARTEY';
begin
     with oZetaProvider do
     begin
          Result:= DecodificaClaves( OpenSQL( Comparte, Format( GetScript( Q_GET_EMPRESAS_SISTEMA ),
                     [EntreComillas (K_COMPARTE_CODIGO),
                    ' CM_CONTROL = ' + EntreComillas( ObtieneElemento( lfTipoCompany, ord (tc3Datos) ) )
                  + ' OR CM_CONTROL = ' + EntreComillas( ObtieneElemento( lfTipoCompany, ord (tc3Prueba) ) )
                  + ' OR CM_CONTROL = ' + EntreComillas( ObtieneElemento( lfTipoCompany, ord (tcRecluta) ) )
                  + ' OR CM_CONTROL = ' + EntreComillas( ObtieneElemento( lfTipoCompany, ord (tcVisitas) ) )
                  + ' OR CM_CONTROL = ' + EntreComillas( ObtieneElemento( lfTipoCompany, ord (tcPresupuesto) ) )
                  + ' OR CM_CONTROL = ' + EntreComillas( ObtieneElemento( lfTipoCompany, ord (tcOtro) ) )
                  , GrupoActivo, ZetaCommonClasses.D_GRUPO_SIN_RESTRICCION ] ), True ), K_CLAVE_EMPRESA )
     end;
     SetComplete;

end;

function TdmServerSistema.GetServer: WideString;
var
    FRegistry: TZetaRegistryServer;
begin
      try
        FRegistry := TZetaRegistryServer.Create ();
        Result :=  FRegistry.ServerDB;
      finally
        FreeAndNil (FRegistry);
      end;
end;

function TdmServerSistema.GetSQLUserName: WideString;
var
   FRegistry: TZetaRegistryServer;
begin
      try
        FRegistry := TZetaRegistryServer.Create(FALSE);
        Result :=  FRegistry.UserName;
      finally
        FreeAndNil (FRegistry);
      end;
end;

function TdmServerSistema.GetSQLUserPassword: WideString;
var
   FRegistry: TZetaRegistryServer;
begin
      try
        FRegistry := TZetaRegistryServer.Create(FALSE);
        Result :=  FRegistry.PasswordRaw;
      finally
        FreeAndNil (FRegistry);
      end;
end;

function TdmServerSistema.EsBDEmpleados(const BaseDatos: WideString): WordBool;
var
   FDataSet: TZetaCursor;
   oZetaProviderCompany: TdmZetaServerProvider;
   sUserName, sPassword: String;
begin
     Result := False;
     sUserName :=  GetSQLUserName;
     sPassword :=  GetSQLUserPassword;

     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          try
              try
                oZetaProviderCompany := TdmZetaServerProvider.Create(Nil);

                oZetaProviderCompany.EmpresaActiva := VarArrayOf([GetServer + '.' + BaseDatos,
                        sUserName,  sPassword, oZetaProvider.UsuarioActivo]);

                FDataSet := oZetaProviderCompany.CreateQuery( Format( GetScript( Q_ES_BD_EMPLEADOS ), [BaseDatos ] ) );
                try
                    with FDataset do
                    begin
                         Active := True;
                         if not EOF then
                            Result := True;
                         Active := False;
                    end;
                finally
                    FreeAndNil( FDataSet );
                end;

              except
                raise Exception.Create( Format ('No es posible conectarse a: %s. Por favor, verifique su informaci�n.', [ GetServer + '.' + BaseDatos]) );
              end;

          finally
              FreeAndNil(oZetaProviderCompany);
          end;

     end;
     SetComplete;
end;

function TdmServerSistema.EsBDSeleccion(const BaseDatos: WideString): WordBool;
var
   FDataSet: TZetaCursor;
   oZetaProviderCompany: TdmZetaServerProvider;
   sUserName, sPassword: String;
begin
     Result := False;
     sUserName :=  GetSQLUserName;
     sPassword :=  GetSQLUserPassword;

     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          try
              try
                oZetaProviderCompany := TdmZetaServerProvider.Create(Nil);

                oZetaProviderCompany.EmpresaActiva := VarArrayOf([GetServer + '.' + BaseDatos,
                sUserName,  sPassword, oZetaProvider.UsuarioActivo]);

                FDataSet := oZetaProviderCompany.CreateQuery( Format( GetScript( Q_ES_BD_SELECCION ), [BaseDatos ] ) );
                try
                    with FDataset do
                    begin
                         Active := True;
                         if not EOF then
                            Result := True;
                         Active := False;
                    end;
                finally
                    FreeAndNil( FDataSet );
                end;

              except
                raise Exception.Create( Format ('No es posible conectarse a: %s. Por favor, verifique su informaci�n.', [ GetServer + '.' + BaseDatos]) );
              end;

          finally
              FreeAndNil(oZetaProviderCompany);
          end;

     end;
     SetComplete;
end;

function TdmServerSistema.EsBDAgregada(const Ubicacion: WideString): WordBool;
var
   FDataSet: TZetaCursor;
begin
     Result := False;
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          try
              try
                  FDataSet := CreateQuery( Format( 'SELECT DB_CODIGO FROM DB_INFO WHERE DB_DATOS = ''%s'' ', [Ubicacion ] ) );
                  try
                      with FDataset do
                      begin
                           Active := True;
                           if not EOF then
                              Result := True;
                           Active := False;
                      end;
                  finally
                      FreeAndNil( FDataSet );
                  end;
              except
                raise Exception.Create( Format ('No es posible conectarse a: %s. Por favor, verifique su informaci�n.', [Ubicacion]) );
              end;

          finally
              FreeAndNil(FDataSet);
          end;
     end;
     SetComplete;
end;

function TdmServerSistema.GetConnStr (BaseDatos: String; Server: String = ''; Usuario: String = '';  Password: String = ''): widestring;
begin
      Result := 'Provider=SQLOLEDB.1;';
      if Server = VACIO then
          Result := Result + 'Data Source=' + GetServer + ';'
      else
          Result := Result + 'Data Source=' + Server + ';';

      if BaseDatos <> '' then
         Result := Result + 'Initial Catalog=' + BaseDatos + ';';
      if Usuario <> VACIO then
      begin
         Result := Result + 'uid=' + Usuario + ';';
         Result := Result + 'pwd=' + ZetaServerTools.Decrypt(Password) + ';';
      end
      else
      begin
         Result := Result + 'uid=' + GetSQLUserName + ';';
         Result := Result + 'pwd=' + ZetaServerTools.Decrypt(GetSQLUserPassword) + ';';
      end;
end;

procedure TdmServerSistema.setProviderDiccionario (BaseDatos: String; bTraeUbicacion: Boolean = FALSE);
begin
    if not bTraeUbicacion then
      oZetaProviderDiccionario.EmpresaActiva := VarArrayOf([GetServer + '.' + BaseDatos,
          GetSQLUserName,  GetSQLUserPassword, oZetaProvider.UsuarioActivo])
    else
      oZetaProviderDiccionario.EmpresaActiva := VarArrayOf([BaseDatos,
          GetSQLUserName,  GetSQLUserPassword, 0]);
end;

function TdmServerSistema.GetNuevoProvider (sUbicacion: WideString; iQuery: Integer; sUsuario: WideString;
                                            sPassword: WideString): TdmZetaServerProvider;
var
  dsBaseDatos: TZetaCursor;
  oZetaProviderCompany: TdmZetaServerProvider;
  empresaActivaTMP: Variant;
begin
     Result := nil;
      with oZetaProvider do
      begin
          try
            empresaActivaTMP := EmpresaActiva;
            EmpresaActiva := Comparte;
            dsBaseDatos := CreateQuery(Format(GetScript(iQuery), [sUbicacion, sUsuario, sPassword]));
            dsBaseDatos.Active := True;

            if not dsBaseDatos.Eof then
            begin
                  oZetaProviderCompany := TdmZetaServerProvider.Create(Nil);
                  oZetaProviderCompany.EmpresaActiva := BuildEmpresa(dsBaseDatos);
                  Result := oZetaProviderCompany;
            end;

            dsBaseDatos.Active := False;        
            EmpresaActiva := empresaActivaTMP;
          finally
               FreeAndNil(dsBaseDatos);
          end;
      end;
end;

function TdmServerSistema.GetComparte: String;
var
   FRegistry : TZetaRegistryServer;
begin
      try
        FRegistry := TZetaRegistryServer.Create(FALSE);
        Result := FRegistry.DataBaseADO;
      finally
        FreeAndNil (FRegistry);
      end;
end;

function TdmServerSistema.TransformaScriptMSSQL(sScript: string ; const sDatabaseComparte: string ): String;
begin
   if Pos( K_NOMBRE_COMPARTE, UpperCase(sScript) ) > 0 then
      sScript := StrTransAll( sScript, K_NOMBRE_COMPARTE, sDatabaseComparte );

   if Pos( K_DIGITO_EMPRESA, UpperCase (sScript) ) > 0 then
        // sScript := StrTransAll( sScript,K_DIGITO_EMPRESA, EntreComillas(dmDBConfig.GetDigito) );
        sScript := StrTransAll( sScript, K_DIGITO_EMPRESA, EntreComillas('0') );

   Result := sScript;
end;

procedure TdmServerSistema.LeeScriptEstructura(const oZetaProviderBD: TdmZetaServerProvider; const sRutaArchivosConfiguracion: string; TipoBD: Integer);
const K_ESTRUCTURA = '\Estructura\';
      K_EXT_SQL = '.sql';
var
  i: Integer;
begin
      if (TipoBD = ord (tc3Datos)) or (TipoBD = ord (tcPresupuesto)) then
      begin
          for i := ( Low( aArchivosSQLEstructura ) ) to High( aArchivosSQLEstructura ) do
          begin
              procesaScript(oZetaProviderBD, sRutaArchivosConfiguracion + K_ESTRUCTURA + aArchivosSQLEstructura[i] + K_EXT_SQL);
          end
      end
      else if TipoBD = ord (tcRecluta) then
      begin
          for i := ( Low( aArchivosSeleccionEstructura ) ) to High( aArchivosSeleccionEstructura ) do
          begin
              procesaScript(oZetaProviderBD, sRutaArchivosConfiguracion + K_ESTRUCTURA + aArchivosSeleccionEstructura[i] + K_EXT_SQL);
          end
      end
      else if TipoBD = ord (tcVisitas) then
      begin
          for i := ( Low( aArchivosVisitantesEstructura ) ) to High( aArchivosVisitantesEstructura ) do
          begin
              procesaScript(oZetaProviderBD, sRutaArchivosConfiguracion + K_ESTRUCTURA + aArchivosVisitantesEstructura[i] + K_EXT_SQL);
          end
      end;
end;

procedure TdmServerSistema.LeeScriptEspeciales(const oZetaProviderBD: TdmZetaServerProvider; const sRutaArchivosConfiguracion: string);
const K_ESPECIALES= '\Especiales\';
      K_EXT_SQL = '.sql';
var
  srArchivo: TSearchRec;
begin
    if (FindFirst(sRutaArchivosConfiguracion + K_ESPECIALES + '*' + K_EXT_SQL, faArchive, srArchivo)) = 0 then
    begin
      repeat
        procesaScript(oZetaProviderBD, sRutaArchivosConfiguracion + K_ESPECIALES + srArchivo.Name);
      until FindNext(srArchivo) <> 0;
      FindClose(srArchivo);
    end;
end;

procedure TdmServerSistema.procesaScript (oZetaProviderDB: TdmZetaServerProvider; sNombreScript: String);
var sScript: WideString;
    oScriptParser   : TZetaSQLScriptParser;
    i: Integer;
begin
     oScriptParser := TZetaSQLScriptParser.Create;
     oScriptParser.EliminarComentarios := FALSE;
     oScriptParser.LoadFromFile(sNombreScript);
     try
         for i := 0 to oScriptParser.Sentences.Count - 1 do
         begin
             sScript := TransformaScriptMSSQL(oScriptParser.Sentences[i], GetComparte);
             TRY
              oZetaProviderDB.ExecSQL(oZetaProviderDB.EmpresaActiva, sScript);
             except
                on Error: Exception do
                begin
                    // Se muestra en pantalla el nombre del archivo de scripts que contiene el error
                    // adem�s del mensaje de error.
                    raise Exception.Create('Error al ejecutar script: ' + sNombreScript +'.' +
                      CR_LF+ 'Error:' + Error.Message);
                end;
             END;
         end;
     finally
        FreeAndNil (oScriptParser);
     end;
end;

procedure TdmServerSistema.LeeIniciales( const sRutaArchivosConfiguracion, sBaseDatosBaseUbica: String; const TipoBD: eTipoCompany );
var
   i: Integer;
begin
     // Valores Default - Tablas iniciales y sugeridas
     // Iniciales
     // Recorrer array de Iniciales y Procesar XML
      cdsLista.InitTempDataSet;
      case TipoBD of
           tc3Datos:
           begin
                for i := ( Low( aArchivosXMLInicial ) ) to High( aArchivosXMLInicial ) do
                begin
                    procesaXML(cdsLista, sRutaArchivosConfiguracion, aArchivosXMLInicial[i], sBaseDatosBaseUbica);
                end;
           end;
           tcRecluta:
           begin
                for i := ( Low(  aArchivosSeleccionInicial ) ) to High( aArchivosSeleccionInicial ) do
                begin
                    procesaXML(cdsLista, sRutaArchivosConfiguracion, aArchivosSeleccionInicial[i], sBaseDatosBaseUbica);
                end;
           end;
           tcVisitas:
           begin
                for i := ( Low( aArchivosVisitantesInicial ) ) to High( aArchivosVisitantesInicial ) do
                begin
                    procesaXML(cdsLista, sRutaArchivosConfiguracion, aArchivosVisitantesInicial[i], sBaseDatosBaseUbica);
                end;
           end;
      end;
end;

procedure TdmServerSistema.LeeXMLSugeridos(const sRutaArchivosConfiguracion, BaseDatos, sBaseDatosBaseUbica: string; ArregloXML: Array of string);
var
   i: Integer;
begin
     // Valores Default - Tablas iniciales y sugeridas
     // Sugeridos
     // Recorrer array de Sugeridos y Procesar XML
     for i := ( Low( ArregloXML ) ) to High( ArregloXML ) do
     begin
         cdsLista.InitTempDataset;
         procesaXML (cdsLista, sRutaArchivosConfiguracion + K_SUGERIDOS, ArregloXML[i], sBaseDatosBaseUbica);
     end;
end;

procedure TdmServerSistema.LeeDiccionario(const sRutaArchivosConfiguracion, BaseDatos, sBaseDatosBaseUbica: string; TipoBD: Integer);
var
   i: Integer;
begin
     with cdsLista do
     begin
          InitTempDataSet;

          // DICCIONARIO DE DATOS
          if TipoBD = ord (tc3Datos) then
          begin
                try
                    // Si sBaseDatosBaseUbica es diferente de VACIO, entonces crear diccionario
                    // a partir de �sa base de datos.
                    // Si no, ejecutar proceso de ImportarDiccionarioXML, el cual lee el archivo DICCION.xml
                    if sBaseDatosBaseUbica <> VACIO then
                    begin
                        // Recorrer array de Diccionario y Procesar
                        for i := ( Low( aTablasDiccionario ) ) to High( aTablasDiccionario ) do
                        begin
                            procesaTablaDiccionario (aTablasDiccionario[i], sBaseDatosBaseUbica);
                        end;
                    end
                    else
                    begin
                        // Crear Provider Diccionario.
                        setProviderDiccionario(BaseDatos);
                        oZetaProviderDiccionario.OpenProcess(prSISTAgregarBaseDatosEmpleados, 0);

                        // 2 y 3er parametros diferentes: Trabaja sobre EMPRESA y deja la bit�cora en COMPARTE,
                        // 2 y 3er parametros iguales: Trabaja y deja la bit�cora en EMPRESA
                        ImportarDiccionarioXML (sRutaArchivosConfiguracion, oZetaProviderDiccionario, oZetaProviderDiccionario, GrabaCambiosBitacora, VACIO);
                        oZetaProviderDiccionario.CloseProcess;
                    end;
                except
                  on Error: Exception do
                  begin
                      raise Exception.Create('Error en Diccionario: ' + Error.Message);
                  end;
                
                end;
                
          end
          else if TipoBD = ord (tcRecluta) then
          begin
              for i := ( Low(  aArchivosSeleccionDiccionario ) ) to High( aArchivosSeleccionDiccionario ) do
              begin
                  procesaXML (cdsLista, sRutaArchivosConfiguracion + K_DICCIONARIO, 'DICCION', sBaseDatosBaseUbica);
              end;
          end
          else  if TipoBD = ord (tcVisitas) then
          begin
              for i := ( Low(  aArchivosVisitantesDiccionario ) ) to High( aArchivosVisitantesDiccionario ) do
              begin
                  procesaXML (cdsLista, sRutaArchivosConfiguracion + K_DICCIONARIO, aArchivosVisitantesDiccionario[i], sBaseDatosBaseUbica);
              end;
          end;
          // ===================================
     end;
end;

procedure TdmServerSistema.procesaXML (cdsXML : TServerDataSet; rutaXML, stabla, sBaseDatosBaseUbica: String);
var
   iAdvertencias, iErrores: Integer;
begin
    // Si se recibe una Base de Datos de origen (sBaseDatosBaseUbica), llenar cdsXML desde �sa ubicaci�n,
    // si no, obtener del XML
    iAdvertencias := 0;
    iErrores:= 0;
    if strLLeno( sBaseDatosBaseUbica ) then
    begin
         cdsXML.Data := LeerTablaDesdeBD(rutaXML, stabla, sBaseDatosBaseUbica);
    end
    else
        cdsXML.LoadFromFile( SetFileNamePath( rutaXML, stabla + EXT_XML ) );

    GrabaDataSet( cdsXML, stabla, VACIO, oZetaProvider, iAdvertencias, iErrores );
end;

procedure TdmServerSistema.procesaTablaDiccionario (tabla, sBaseDatosBaseUbica: String);
var
   iAdvertencias, iErrores: Integer;
begin
     iAdvertencias := 0;
     iErrores := 0;
     cdsLista.Data := LeerTablaDesdeBD('', tabla, sBaseDatosBaseUbica);
     GrabaDataSet(cdsLista, tabla, VACIO, oZetaProvider, iAdvertencias, iErrores );
end;

function TdmServerSistema.LeerTablaDesdeBD ( const rutaXML, tabla, sBaseDatosBaseUbica: String): OleVariant;
var
  oZetaServerProviderCompany: TdmZetaServerProvider;
begin
  try
      oZetaServerProviderCompany := GetNuevoProvider(sBaseDatosBaseUbica, Q_GET_BASES_DATOS_SERVER2, '', '');
      with oZetaServerProviderCompany do
      begin
          Result := OpenSQL(EmpresaActiva, Format ('SELECT * FROM %s', [tabla]), TRUE);
      end;
  finally
      FreeAndNil (oZetaServerProviderCompany);
  end;
end;

function TdmServerSistema.GetFieldsFromXML (rutaXML, tabla: String):TStringList;
const
    EXT_XML = '.xml';
//    K_LLAVE = 'LLAVE';
var
  Campos: TStringList;
  cdsXML: TClientDataSet;
  I: Integer;
begin
  Campos := TStringList.Create;
  cdsXML := TClientDataSet.Create(Self);
  cdsXML.LoadFromFile(rutaXML +  tabla + EXT_XML);

  with cdsXML do
  begin
    try
      // Obtener los nombres de Campo
      for I := 0 to Fields.Count - 1 do
      begin
        if ( Fields[I].FieldName <> K_CAMPO_LLAVE_PORTAL ) or
           ( Fields[I].FieldName <> K_CAMPO_SINC_UPDATED ) or
           ( Fields[I].FieldName <> K_CAMPO_SINC_CREATED ) then
          Campos.Add(Fields[I].FieldName);
      end;
    finally
        FreeAndNil (cdsXML);
    end;
  end;

  Result := Campos;

end;

function TdmServerSistema.GetArrayEmpresa(sCM_DATOS, sCM_USRNAME, sCM_PASSWRD, sCM_NIVEL0, sCM_CODIGO: string): OleVariant;
begin
  Result := VarArrayOf([sCM_DATOS, sCM_USRNAME, sCM_PASSWRD, 0, sCM_NIVEL0, sCM_CODIGO,
      D_GRUPO_SIN_RESTRICCION, 0]);
end;

function TdmServerSistema.GetLogicalName(const Conexion, Nombre: WideString; Tipo: Integer): WideString;
var
  FLogicalName: TZetaCursor;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          FLogicalName := CreateQuery( Format( GetScript( Q_CONSULTA_LOGICAL_NAME ), [ Nombre, Tipo ] ) );
          try
          with FLogicalName do
          begin
            Active := True;
            Result := FieldByName('Name').AsString;
            Active := False;
          end;
        finally
          FreeAndNil(FLogicalName);
        end;
     end;
     SetComplete;
end;

{$ifdef SISNOM_CLAVES}
function TdmServerSistema.GetUsuariosSeguridad(out Longitud: Integer; Grupo: Integer;
          out BlockSistema: WordBool): OleVariant;
begin
     SetTablaInfo( eUsuariosSeguridad );
     SetFiltroGrupos( Grupo );
     with oZetaProvider do
     begin
	{$ifdef SISNOM_CLAVES}
          Result := GetTabla( Comparte );      // Se manda encriptado al cliente.
	{$else}
          Result := DecodificaClaves( GetTabla( Comparte ), K_CLAVE_USUARIO );
	{$endif}
     end;
     Longitud := StrToIntDef( LeerClaves( CLAVE_LIMITE_PASSWORD ), 0 );
     BlockSistema := zstrToBool( LeerClaves( CLAVE_SISTEMA_BLOQUEADO ) );
     SetComplete;
end;
{$endif}

function TdmServerSistema.BackupDB(const Conexion, Origen, Destino, Ruta: WideString): OleVariant; safecall;
 var
   FDataset: TZetaCursor;
   ok : Integer;
begin
     with oZetaProvider do
     begin
           EmpresaActiva := Comparte;
           begin
                FDataSet := CreateQuery( Format( GetScript( Q_BACKUP ),[Origen, Destino]));
           try
              ok := Ejecuta( FDataSet );
              if ok = -1 then
              begin
                   Result :=  TRUE;
              end
              else
              begin
                   Result := FALSE;
              end;
           finally
                  FreeAndNil( FDataSet );
           end;
           end;
     end;
end;

function TdmServerSistema.RestoreDB(const Conexion, Origen, Destino, DataName, PathDataName, LogName, PathLogName: WideString): OleVariant;
var
   FDataset: TZetaCursor;
   ok : Integer;
begin
     with oZetaProvider do
     begin
           EmpresaActiva := Comparte;
           begin
                FDataSet := CreateQuery( Format( GetScript( Q_RESTORE ),[Origen, Destino, DataName, PathDataName, LogName, PathLogName]));
           try
              ok := Ejecuta( FDataSet );
              if ok = -1 then
              begin
                   Result :=  TRUE;
              end
              else
              begin
                   Result := FALSE;
              end;
           finally
                  FreeAndNil( FDataSet );
           end;
           end;
     end;
end;

function TdmServerSistema.DepurarTabla(const Conexion, Tabla, Database: WideString): OleVariant;
var
   FDataset: TZetaCursor;
   ok : Integer;
begin
     with oZetaProvider do
     begin
           EmpresaActiva := Comparte;
           begin
                FDataSet := CreateQuery( Format( GetScript( Q_DEPURAR ),[Database, Tabla]));
           try
              ok := Ejecuta( FDataSet );
              if ok = -1 then
              begin
                   Result :=  TRUE;
              end
              else
              begin
                   Result := FALSE;
              end;
           finally
                  FreeAndNil( FDataSet );
           end;
           end;
     end;
end;

function TdmServerSistema.ConectarseServidor(const Conexion: WideString; out Mensaje: WideString): WordBool;
begin
     with Connector do
     begin
          Close;
          ConnectionString := Conexion;
          LoginPrompt := False;
          try
             Open;
             Result := True;
          Except
            on e:exception do
            begin
                 Mensaje := 'Se ingres� Usuario o Contrase�a incorrectos. ' + #13#10 + 'Verifique los datos.';
                 Result := False;
            end;
          end;
     end;
end;

function TdmServerSistema.GetDBSize(const Conn, Database: WideString; bUsarConexion: WordBool): OleVariant;
var
   FDataset: TZetaCursor;
   oZetaProviderCompany: TdmZetaServerProvider;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;

          // Caso en que se tiene que usar la Conexi�n (se recibe nombre de servidor en lugar de un
          // conexion string.
          if bUsarConexion then
          begin
            oZetaProviderCompany := GetNuevoProvider(Conn, Q_GET_BASES_DATOS_SERVER2, '', '');
            FDataset :=  oZetaProviderCompany.CreateQuery( Format( GetScript( Q_DB_SIZE ), [ Database ] ) );
          end
          else
            FDataset := CreateQuery( Format( GetScript( Q_DB_SIZE ), [ Database ] ) );
          try
             with FDataset do
             begin
                  Active := True;
                  if EOF then
                     Result := ''
                  else
                      Result := FieldByName( 'DBSize' ).AsString;
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataset );
                 FreeAndNil( oZetaProviderCompany );
          end;
     end;
end;

function TdmServerSistema.GetDataLogSize(const Conn, Database: WideString; bUsarConexion: WordBool): OleVariant;
var
   FDataset: TZetaCursor;
   oZetaProviderCompany: TdmZetaServerProvider;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;

          // Caso en que se tiene que usar la Conexi�n (se recibe nombre de servidor en lugar de un
          // conexion string.
          if bUsarConexion then
          begin
            oZetaProviderCompany := GetNuevoProvider(Conn, Q_GET_BASES_DATOS_SERVER2, '', '');
            FDataset :=  oZetaProviderCompany.CreateQuery( Format( GetScript( Q_LOG_SIZE ), [ Database ] ) );
          end
          else
          	FDataset := CreateQuery( Format( GetScript( Q_LOG_SIZE ), [ Database ] ) );
          try
             with FDataset do
             begin
                  Active := True;
                  if EOF then
                     Result := ''
                  else
                      Result := FieldByName( 'LogSizeMB' ).AsString;
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataset );
                 FreeAndNil( oZetaProviderCompany );
          end;
     end;
end;

function TdmServerSistema.ShrinkDB(const Conexion, Database: WideString): OleVariant;
var
   FDataset: TZetaCursor;
   ok : Integer;
begin
     //GetConnStr(Database);
     with oZetaProvider do
     begin
           EmpresaActiva := Comparte;
           begin
                FDataSet := CreateQuery( Format( GetScript( Q_SHRINK ),[Database]));
           try
              ok := Ejecuta( FDataSet );
              if ok = -1 then
              begin
                   Result :=  TRUE;
              end
              else
              begin
                   Result := FALSE;
                   raise Exception.Create( Format ('No es posible compactar la base de datos:  %s. Por favor, compactar manualmente.', [Database]) );
              end;
           finally
                  FreeAndNil( FDataSet );
           end;
           end;
     end;
end;

function TdmServerSistema.GetConexion(const Conn, Database, Datos, UserName, Password: WideString): WordBool;
var
   dsBaseDatos: TZetaCursor;
   oZetaProviderCompany: TdmZetaServerProvider;
   sUserName, sPassword: String;
begin
     with oZetaProvider do
     begin
       EmpresaActiva := Comparte;
      try
        // Si no se env�a usuario, obtener el usuario de Base de Datos default del sistema
        if (Trim (UserName) = VACIO) then
        begin
            sUserName :=  GetSQLUserName;
            sPassword :=  GetSQLUserPassword;
        end
        else
        begin
            sUserName := UserName;
            sPassword := Password;
        end;;

        try

        dsBaseDatos := CreateQuery(Format(GetScript(Q_CONEXION_EMPRESA), [Datos, sUserName, sPassword]));
        dsBaseDatos.Active := True;
        if not dsBaseDatos.Eof then
        begin
              oZetaProviderCompany := TdmZetaServerProvider.Create(Nil);
              oZetaProviderCompany.EmpresaActiva := BuildEmpresa(dsBaseDatos);
        end
        else
        begin
              raise Exception.Create( 'Error al conectarse a Servidor: ' + Datos );
        end;

            dsBaseDatos.Active := False;

        except
          raise Exception.Create( Format ('No es posible conectarse a: %s. Por favor, verifique su informaci�n.', [Datos]) );
        end;


      finally
           FreeAndNil(oZetaProviderCompany);
           FreeAndNil(dsBaseDatos);
      end;
      end;
     SetComplete;
end;

function TdmServerSistema.GrabaDB_INFO(Parametros: OleVariant): OleVariant;
var
  sUserName, sPassword: String;
  sCodigo, sDescripcion, sUbicacion:String;
  // sControl, sControlReal: String;
  iTipo : Integer;
  // Validaci�n de Base de Datos de Empleados
  oConfigServer : TDBConfigServer;
  oZetaProviderValidacion: TdmZetaServerProvider;
  oEmpresa : TEmpresaConfig;
  sAdvertencia: String;
  // ----- -----
begin
     with oZetaProvider do
     begin
        try
            EmpresaActiva := Comparte;
            AsignaParamList( Parametros );
            with ParamList do
            begin
                 iTipo := ParamByName( 'Tipo' ).AsInteger;
                 sCodigo := ParamByName( 'Codigo' ).AsString;
                 sDescripcion := ParamByName( 'Descripcion' ).AsString;
                 sUbicacion := GetServer + '.' + ParamByName( 'BaseDatos' ).AsString;
                 sUserName := ParamByName( 'Usuario' ).AsString;
                 sPassword := ParamByName( 'Clave' ).AsString;
            end;

            if sUserName = VACIO then
            begin
              sUserName :=  GetSQLUserName;
              sPassword :=  GetSQLUserPassword;
            end;

            // ===================================================================================================
            //  Incrementar Contador de Base de Datos de Producci�n (tipo Tress)
            oZetaProviderValidacion := TdmZetaServerProvider.Create (nil);
            oConfigServer := TDBConfigServer.Create(oZetaProviderValidacion, oAutoServer);
            oEmpresa :=  TEmpresaConfig.Create;
            oEmpresa.Codigo := sCodigo;
            oEmpresa.Datos  := sUbicacion;
            oEmpresa.Control := ObtieneElemento(lfTipoCompany, iTipo);
            oEmpresa.ControlReal := ObtieneElemento(lfTipoCompany, iTipo);
            oEmpresa.Digito := '';
            oEmpresa.OldCodigo := '';
            oEmpresa.OldDatos := '';
            oEmpresa.OldControlReal := '';
            oEmpresa.OldControl := '';
            oEmpresa.OldCheckSum := '';

            oConfigServer.BeforeEmpresaObjPost(oEmpresa, ukInsert, sAdvertencia);

            if oEmpresa.Control = ObtieneElemento(lfTipoCompany, ord(tc3Prueba)) then
                  oEmpresa.Control :=  ObtieneElemento(lfTipoCompany, ord(tc3Datos));

            // ===================================================================================================

            try
                 ExecSQL( EmpresaActiva, Format( GetScript( Q_INSERT_DB ), [ sCodigo,
                    sDescripcion, oEmpresa.Control, iTipo, sUbicacion, sUserName, sPassword,
                    oEmpresa.ControlReal, oEmpresa.CheckSum, 'N'] ) );
                 TerminaTransaccion( TRUE );
            except
                  on Error: Exception do
                  begin
                       TerminaTransaccion( FALSE );
                       raise;
                  end;
            end;
            // =======================================================
            // Incrementar contador de bases de datos de tipo Tress.
            oConfigServer.AfterEmpresaObjPost(oEmpresa);
            // =======================================================
        finally
              FreeAndNil (oConfigServer);
              FreeAndNil (oZetaProviderValidacion);
              FreeAndNil (oEmpresa);
        end;
     end;
     Result := true;
     SetComplete;
end;

function TdmServerSistema.GetPathSQLServer: OleVariant;
var
    FRegistry: TZetaRegistryServer;
begin
      try
        FRegistry := TZetaRegistryServer.Create ();
        Result :=  VerificaDir (FRegistry.PathMSSQL);
      finally
        FreeAndNil (FRegistry);
      end;
end;

function TdmServerSistema.CrearEstructuraBD(Parametros: OleVariant): WordBool;
const K_ARCHIVOS_DEFINICION_EMPLEADOS = '\Empleados\NuevaBD';
const K_ARCHIVOS_DEFINICION_SELECCION = '\Seleccion\NuevaBD';
const K_ARCHIVOS_DEFINICION_VISITANTES = '\Visitantes\NuevaBD';
var
  FRegistry : TZetaRegistryServer;
  sDescripcion, sUbicacion, sRutaArchivosConfiguracion, sUsuario, sPassword: String;
  Tipo : Integer;
  oZetaProviderBD: TdmZetaServerProvider;
begin
     Result:= FALSE;
     FRegistry := TZetaRegistryServer.Create;
     oZetaProviderBD := TdmZetaServerProvider.Create(Nil);

     with oZetaProvider do
     begin
        try
            EmpresaActiva := Comparte;
            AsignaParamList( Parametros );
            with ParamList do
            begin
                 sUsuario := StrDef( ParamByName( 'Usuario' ).AsString, FRegistry.UserName );
                 sPassword := StrDef( ParamByName( 'Clave' ).AsString, FRegistry.PasswordRaw );
                 sDescripcion := ParamByName( 'Descripcion' ).AsString;
                 sUbicacion := FRegistry.ServerDB + '.' + ParamByName( 'BaseDatos' ).AsString;
                 Tipo := ParamByName( 'Tipo' ).AsInteger;
            end;

            if (Tipo = ord (tc3Datos)) or (Tipo = ord (tcPresupuesto)) then
            begin
                sRutaArchivosConfiguracion := FRegistry.PathConfig + K_ARCHIVOS_DEFINICION_EMPLEADOS;
            end
            else if Tipo = ord (tcRecluta) then
            begin
                 sRutaArchivosConfiguracion := FRegistry.PathConfig + K_ARCHIVOS_DEFINICION_SELECCION;
            end
            else if Tipo = ord (tcVisitas) then
            begin
                sRutaArchivosConfiguracion := FRegistry.PathConfig + K_ARCHIVOS_DEFINICION_VISITANTES;
            end;

            oZetaProviderBD.EmpresaActiva := VarArrayOf([sUbicacion,
              sUsuario,  sPassword, oZetaProvider.UsuarioActivo]);

            LeeScriptEstructura(oZetaProviderBD, sRutaArchivosConfiguracion, Tipo);
        finally
              FreeAndNil(FRegistry);
              FreeAndNil(oZetaProviderBD);
        end;
     end;
     SetComplete;
     Result:= TRUE;
end;

function TdmServerSistema.CreaInicialesTabla(Parametros: OleVariant): WordBool;
var
  FRegistry : TZetaRegistryServer;
  sUserName, sPassword: String;
  sBaseDatosBaseUbica: String;
  sDescripcion, sUbicacion, sRutaArchivosConfiguracion: String;
  Tipo : eTipoCompany;
begin
     Result:= FALSE;
     FRegistry := TZetaRegistryServer.Create(FALSE);
     sBaseDatosBaseUbica := VACIO;

     with oZetaProvider do
     begin
         try
            AsignaParamList( Parametros );
            with ParamList do
            begin
                 sUserName := StrDef( ParamByName( 'Usuario' ).AsString, FRegistry.UserName );
                 sPassword := StrDef( ParamByName( 'Clave' ).AsString, FRegistry.PasswordRaw );
                 sDescripcion := ParamByName( 'Descripcion' ).AsString;
                 sUbicacion := FRegistry.ServerDB + '.' + ParamByName( 'BaseDatos' ).AsString;
                 Tipo := eTipoCompany( ParamByName( 'Tipo' ).AsInteger );
                 if not ParamByName( 'ValoresDefault' ).AsBoolean then
                    sBaseDatosBaseUbica := ParamByName( 'BaseDatosBase' ).AsString;
            end;

            EmpresaActiva := VarArrayOf( [ sUbicacion, sUserName, sPassword] );

            if Tipo = tc3Datos then
            begin
                sRutaArchivosConfiguracion := SetFileNamePath( FRegistry.PathConfig, K_ARCHIVOS_DEFINICION_EMPLEADOS );
            end
            else if Tipo = tcRecluta then
            begin
                 sRutaArchivosConfiguracion := SetFileNamePath( FRegistry.PathConfig, K_ARCHIVOS_DEFINICION_SELECCION );
            end
            else if Tipo = tcVisitas then
            begin
                sRutaArchivosConfiguracion := SetFileNamePath( FRegistry.PathConfig, K_ARCHIVOS_DEFINICION_VISITANTES );
            end;
            // Se leen XML para cargar Iniciales
            LeeIniciales( SetFileNamePath( sRutaArchivosConfiguracion, K_INICIALES ), sBaseDatosBaseUbica, Tipo );
        finally
               FreeAndNil(FRegistry);
        end;
     end;
     Result:= TRUE;
     SetComplete;
 end;

function TdmServerSistema.CreaSugeridosTabla(Parametros: OleVariant): WordBool;
const K_ARCHIVOS_DEFINICION_EMPLEADOS = '\Empleados\NuevaBD';
var
  FRegistry : TZetaRegistryServer;
  sUserName, sPassword: String;
  sDescripcion, sUbicacion, sRutaArchivosConfiguracion, sBaseDatosBaseUbica: String;
begin
     Result:= FALSE;
     FRegistry := TZetaRegistryServer.Create(FALSE);
     sBaseDatosBaseUbica := VACIO;

     with oZetaProvider do
     begin
        try
            EmpresaActiva := Comparte;
            AsignaParamList( Parametros );
            with ParamList do
            begin
                 sUserName := StrDef( ParamByName( 'Usuario' ).AsString, FRegistry.UserName );
                 sPassword := StrDef( ParamByName( 'Clave' ).AsString, FRegistry.PasswordRaw );
                 sDescripcion := ParamByName( 'Descripcion' ).AsString;
                 sUbicacion := GetServer + '.' + ParamByName( 'BaseDatos' ).AsString;
                 if not ParamByName( 'ValoresDefault' ).AsBoolean then
                    sBaseDatosBaseUbica := ParamByName( 'BaseDatosBase' ).AsString;
            end;

            EmpresaActiva := VarArrayOf( [ sUbicacion, sUserName, sPassword ] );
            sRutaArchivosConfiguracion := FRegistry.PathConfig + K_ARCHIVOS_DEFINICION_EMPLEADOS;
            // Se leen XML para cargar Sugeridos
            if StrLleno(sBaseDatosBaseUbica) then
                LeeXMLSugeridos(sRutaArchivosConfiguracion, ParamList.ParamByName( 'BaseDatos' ).AsString, sBaseDatosBaseUbica, aArchivosXMLSugeridosCopia)
            else
               LeeXMLSugeridos(sRutaArchivosConfiguracion, ParamList.ParamByName( 'BaseDatos' ).AsString, sBaseDatosBaseUbica, aArchivosXMLSugeridos);
            //US 17266: Almacenar la clave SAT cuando se timbra la n�mina para obtener reportes y auditar timbrado
            EjecutaAndFree( GetScript( Q_INICIAR_KARDEX_CONCEPTOS ) );
        finally
              FreeAndNil(FRegistry);
        end;
     end;
     SetComplete;
     Result:= TRUE;;
end;

function TdmServerSistema.CreaDiccionarioTabla(Parametros: OleVariant): WordBool;
const K_ARCHIVOS_DEFINICION_EMPLEADOS = '\Empleados\NuevaBD';
const K_ARCHIVOS_DEFINICION_SELECCION = '\Seleccion\NuevaBD';
const K_ARCHIVOS_DEFINICION_VISITANTES = '\Visitantes\NuevaBD';
var
  FRegistry : TZetaRegistryServer;
  sDescripcion, sUbicacion, sRutaArchivosConfiguracion, sBaseDatosBaseUbica: String;
  bDefinirEstructura: Boolean;
  sUserName, sPassword: String;
  TipoBD: Integer;
begin
     Result:= FALSE;
     FRegistry := TZetaRegistryServer.Create(FALSE);
     sBaseDatosBaseUbica := VACIO;

     with oZetaProvider do
     begin
        try
            EmpresaActiva := Comparte;
            AsignaParamList( Parametros );
            with ParamList do
            begin
                 sDescripcion := ParamByName( 'Descripcion' ).AsString;
                 sUbicacion := FRegistry.ServerDB + '.' + ParamByName( 'BaseDatos' ).AsString;
                 sUserName := StrDef( ParamByName( 'Usuario' ).AsString, FRegistry.UserName );
                 sPassword := StrDef( ParamByName( 'Clave' ).AsString, FRegistry.PasswordRaw );
                 bDefinirEstructura := ParamByName( 'DefinirEstructura' ).AsBoolean;
                 TipoBD := ParamByName( 'Tipo' ).AsInteger;
                 if not ParamByName( 'ValoresDefault' ).AsBoolean then
                    sBaseDatosBaseUbica := ParamByName( 'BaseDatosBase' ).AsString;
            end;

            EmpresaActiva := VarArrayOf( [ sUbicacion, sUserName, sPassword, 0] );

            if bDefinirEstructura then
            begin
              if TipoBD = ord(tc3Datos) then
              begin
                  sRutaArchivosConfiguracion := FRegistry.PathConfig + K_ARCHIVOS_DEFINICION_EMPLEADOS;
              end
              else if TipoBD = ord (tcRecluta) then
              begin
                   sRutaArchivosConfiguracion := FRegistry.PathConfig + K_ARCHIVOS_DEFINICION_SELECCION;
              end
              else if TipoBD = ord (tcVisitas) then
              begin
                  sRutaArchivosConfiguracion := FRegistry.PathConfig + K_ARCHIVOS_DEFINICION_VISITANTES;
              end;

              // Se leen XML para cargar Diccionario
              LeeDiccionario(sRutaArchivosConfiguracion, ParamList.ParamByName( 'BaseDatos' ).AsString, sBaseDatosBaseUbica, TipoBD);
            end;
        finally
              FreeAndNil(FRegistry);
        end;
     end;
     SetComplete;
     Result:= TRUE;;
end;

function TdmServerSistema.EmpGrabarDBSistema(Parametros: OleVariant): WordBool;
const K_ARCHIVOS_DEFINICION_EMPLEADOS = '\Empleados\NuevaBD';
var
  sConnStr, sCodigo, sDescripcion, sUbicacion, sAdvertencia, sUsuario, sPassword: String;
  // Validaci�n de Base de Datos de Empleados
  oConfigServer : TDBConfigServer;
  oZetaProviderValidacion: TdmZetaServerProvider;
  oEmpresa : TEmpresaConfig;
  bEspeciales: Boolean;
  // ----- -----
begin
     Result := FALSE;
     with oZetaProvider do
     begin
        try
            EmpresaActiva := Comparte;
            AsignaParamList( Parametros );
            with ParamList do
            begin
                 sConnStr := GetConnStr(ParamByName( 'BaseDatos' ).AsString);
                 sCodigo := ParamByName( 'Codigo' ).AsString;
                 sDescripcion := ParamByName( 'Descripcion' ).AsString;
                 sUbicacion := GetServer + '.' + ParamByName( 'BaseDatos' ).AsString;

                 if (ParamByName( 'NuevaBD' ).AsBoolean) and (ParamByName( 'Usuario' ).AsString <> VACIO) then
                 begin
                    sUsuario := ParamByName( 'Usuario' ).AsString;
                    sPassword := ParamByName( 'Clave' ).AsString;
                 end
                 else
                 begin
                    sUsuario := GetSQLUserName;
                    sPassword := GetSQLUserPassword;
                 end;
                 bEspeciales := ParamByName( 'Especiales' ).AsBoolean;
            end;

            // ===================================================================================================
            //  Incrementar Contador de Base de Datos de Producci�n (tipo Tress)
            oZetaProviderValidacion := TdmZetaServerProvider.Create (nil);
            oConfigServer := TDBConfigServer.Create(oZetaProviderValidacion, oAutoServer);
            oEmpresa :=  TEmpresaConfig.Create;
            oEmpresa.Codigo := sCodigo;
            oEmpresa.Datos  := sUbicacion;
            oEmpresa.ControlReal := ObtieneElemento(lfTipoCompany, ord(tc3Datos));
            oEmpresa.Control := ObtieneElemento(lfTipoCompany, ord(tc3Datos));
            oEmpresa.Digito := '';
            oEmpresa.OldCodigo := '';
            oEmpresa.OldDatos := '';
            oEmpresa.OldControlReal := '';
            oEmpresa.OldControl := '';
            oEmpresa.OldCheckSum := '';

            oConfigServer.BeforeEmpresaObjPost(oEmpresa, ukInsert, sAdvertencia);

            if oEmpresa.Control = ObtieneElemento(lfTipoCompany, ord(tc3Prueba)) then
                  oEmpresa.Control :=  ObtieneElemento(lfTipoCompany, ord(tc3Datos));

            // Grabar registro de base de datos en DB_INFO
            ExecSQL( EmpresaActiva, Format( GetScript( Q_INSERT_DB ), [ sCodigo,
            sDescripcion, oEmpresa.Control, 0, sUbicacion, sUsuario, sPassword,
            oEmpresa.ControlReal, oEmpresa.CheckSum, zBoolToStr(bEspeciales)] ) );

            // Incrementar contador de bases de datos de tipo Tress.
            oConfigServer.AfterEmpresaObjPost(oEmpresa);
            // ===================================================================================================

        finally
            FreeAndNil (oConfigServer);
            FreeAndNil (oZetaProviderValidacion);
            FreeAndNil (oEmpresa);
        end;
     end;
     SetComplete;
     Result := TRUE;
end;

{ Aqui estaba el c�digo para Importar Diccionario, movido a la unidad DImportaDiccionario.pas}

function TdmServerSistema.NoEsBDSeleccionVisitantes(const BaseDatos: WideString): WordBool;
var
   FDataSet: TZetaCursor;
   oZetaProviderCompany: TdmZetaServerProvider;
   sUserName, sPassword: String;
begin
     Result := TRUE;
     sUserName :=  GetSQLUserName;
     sPassword :=  GetSQLUserPassword;
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          try
              try
                oZetaProviderCompany := TdmZetaServerProvider.Create(Nil);
                oZetaProviderCompany.EmpresaActiva := VarArrayOf([GetServer + '.' + BaseDatos,
                        sUserName,  sPassword, oZetaProvider.UsuarioActivo]);
                FDataSet := oZetaProviderCompany.CreateQuery( Format( GetScript( Q_NO_ES_SELECCION_VISITANTES ), [BaseDatos ] ) );
                try
                    with FDataset do
                    begin
                         Active := True;
                         if not EOF then
                            Result := FALSE;
                         Active := False;
                    end;
                finally
                    FreeAndNil( FDataSet );
                end;
              except
                raise Exception.Create( Format ('No es posible conectarse a: %s. Por favor, verifique su informaci�n.', [GetServer + '.' + BaseDatos]) );
              end;
          finally
              FreeAndNil(oZetaProviderCompany);
          end;
     end;
     SetComplete;
end;

function TdmServerSistema.NoEsBDTressVisitantes(const BaseDatos: WideString): WordBool;
var
   FDataSet: TZetaCursor;
   oZetaProviderCompany: TdmZetaServerProvider;
   sUserName, sPassword: String;
begin
     Result := TRUE;
     sUserName :=  GetSQLUserName;
     sPassword :=  GetSQLUserPassword;

     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          try
              try
                oZetaProviderCompany := TdmZetaServerProvider.Create(Nil);
                oZetaProviderCompany.EmpresaActiva := VarArrayOf([GetServer + '.' + BaseDatos,
                        sUserName,  sPassword, oZetaProvider.UsuarioActivo]);

                FDataSet := oZetaProviderCompany.CreateQuery( Format( GetScript( Q_NO_ES_TRESS_VISITANTES ), [BaseDatos ] ) );
                try
                    with FDataset do
                    begin
                         Active := True;
                         if not EOF then
                            Result := FALSE;
                         Active := False;
                    end;
                finally
                    FreeAndNil( FDataSet );
                end;
              except
                raise Exception.Create( Format ('No es posible conectarse a: %s. Por favor, verifique su informaci�n.', [GetServer + '.' + BaseDatos]) );
              end;

          finally
              FreeAndNil(oZetaProviderCompany);
          end;
     end;
     SetComplete;
end;

function TdmServerSistema.ValidarBDEmpleados(const Codigo, Ubicacion: WideString; out Mensaje: WideString): OleVariant;
var
  // Validaci�n de Base de Datos de Empleados
  oConfigServer : TDBConfigServer;
  oZetaProviderValidacion: TdmZetaServerProvider;
  oEmpresa : TEmpresaConfig;
  sAdvertencia : string;
begin
     with oZetaProvider do
     begin
        EmpresaActiva := Comparte;

        // ===================================================================================================
        //  VALIDACI�N DE BASE DE DATOS DE EMPLEADOS
        sAdvertencia := '';
        oZetaProviderValidacion := TdmZetaServerProvider.Create (nil);
        oConfigServer := TDBConfigServer.Create(oZetaProviderValidacion, oAutoServer);
        oEmpresa :=  TEmpresaConfig.Create;
        // oEmpresa.Codigo := ParamList.ParamByName('Codigo').AsString;
        oEmpresa.Codigo := Codigo;
        // oEmpresa.Datos  := GetServer + '.' + ParamList.ParamByName( 'BaseDatos' ).AsString;
        oEmpresa.Datos  := Ubicacion;
        oEmpresa.ControlReal := ObtieneElemento(lfTipoCompany, ord(tc3Datos));
        oEmpresa.Control := ObtieneElemento(lfTipoCompany, ord(tc3Datos));
        oEmpresa.Digito := '';
        oEmpresa.OldCodigo := '';
        oEmpresa.OldDatos := '';
        oEmpresa.OldControlReal := '';
        oEmpresa.OldControl := '';
        oEmpresa.OldCheckSum := '';
        oConfigServer.BeforeEmpresaObjPost( oEmpresa, ukInsert,  sAdvertencia );
        // oConfigServer.ValidarAltaBDProduccion(sAdvertencia);
        Mensaje := sAdvertencia;
        // ===================================================================================================
     end;
     SetComplete;
end;

{$IFNDEF DOS_CAPAS}

function TdmServerSistema.CrearBDEmpleados(Parametros: OleVariant): WordBool;
Const K_TAM_INICIAL = 50;
      K_PORCENTAJE_INC = 0.20;
      K_PORCENTAJE_LOG = 0.25;
var
  sNombreBDSQL, sRuta: String;
  iMaxSize: Integer;
  dIncrementos: Double;
begin
     Result := FALSE;
     with oZetaProvider do
     begin
        try
            EmpresaActiva := Comparte;
            AsignaParamList( Parametros );
            with ParamList do
            begin
                 sNombreBDSQL := ParamByName( 'NombreBDSQL' ).AsString;
                 iMaxSize := ParamByName( 'MaxSize' ).AsInteger;
                 dIncrementos := iMaxSize*K_PORCENTAJE_INC;
                 sRuta := GetPathSQLServer;
            end;

              ExecSQL (EmpresaActiva, Format( GetScript(K_QRY_CREA_BD_TRESS),
                [sNombreBDSQL, sRuta, iMaxSize, iMaxSize, Round (dIncrementos),
                Round (iMaxSize*K_PORCENTAJE_LOG), Round (iMaxSize*K_PORCENTAJE_LOG)]));            

            Result := TRUE;

        finally
            Result := TRUE;
        end;
     end;
     SetComplete;
     Result := TRUE;
end;

function TdmServerSistema.PrepararPresupuestos(Parametros: OleVariant): WordBool;
var odmPreparaPresupuestos: TdmPreparaPresupuestos;
    oZetaProviderCompany: TdmZetaServerProvider;
    // Variables Par�metros exclusivos para preparar presupuestos
    iYear: Integer;
    dFechaEmpAlta, dFechaEmpBaja: TDateTime;
    lNominaMensual, lConservarConfNomina: Boolean;
    // ----- -----
    sUsuario, sPassword, sBaseDatosDestino, sBaseDatosDestinoUbica: String;
    sBaseDatosBaseCodigo: String; 
    FRegistry : TZetaRegistryServer;
begin
    try
        Result := FALSE;
        oZetaProvider.AsignaParamList( Parametros ); 
        FRegistry := TZetaRegistryServer.Create(FALSE);
        with oZetaProvider.ParamList do
        begin
            iYear := ParamByName('anioPresupuestar').AsInteger;
            dFechaEmpAlta := ParamByName('FechaAlta').AsDate;
            dFechaEmpBaja := ParamByName('FechaBaja').AsDate;
            lConservarConfNomina := ParamByName('ConservarConfNomina').AsBoolean;
            lNominaMensual := ParamByName('NominaMensual').AsBoolean;
            sBaseDatosDestinoUbica := FRegistry.ServerDB + '.' + ParamByName('BaseDatos').AsString;
            sBaseDatosDestino := ParamByName('BaseDatos').AsString;
            sBaseDatosBaseCodigo := ParamByName('BaseDatosBase').AsString;
            sUsuario := StrDef( ParamByName( 'Usuario' ).AsString, FRegistry.UserName );
            sPassword := StrDef( ParamByName( 'Clave' ).AsString, FRegistry.PasswordRaw );
        end;

        oZetaProviderCompany := TdmZetaServerProvider.Create(nil);        
        oZetaProviderCompany.EmpresaActiva := VarArrayOf([sBaseDatosDestinoUbica, sUsuario,  sPassword]);
        
        odmPreparaPresupuestos := TdmPreparaPresupuestos.Create(nil);
        odmPreparaPresupuestos.PreparacionPresupuestos
          (iYear, dFechaEmpAlta, dFechaEmpBaja, lNominaMensual, lConservarConfNomina, oZetaProviderCompany,
          sBaseDatosBaseCodigo, sBaseDatosDestino);
    finally
        FreeAndNil (oZetaProviderCompany); 
        FreeAndNil(FRegistry);
        Result := TRUE;
    end;
    SetComplete;
end;

function TdmServerSistema.CatalogosPresupuestos(Parametros: OleVariant): WordBool;
var odmPreparaPresupuestos: TdmPreparaPresupuestos;   
    FRegistry : TZetaRegistryServer;
    oZetaProviderCompany: TdmZetaServerProvider;
    sUsuario, sPassword, sBaseDatosDestinoUbica: String;
begin
    try
        Result := FALSE;  
        FRegistry := TZetaRegistryServer.Create(FALSE);
        oZetaProvider.AsignaParamList( Parametros );
        with oZetaProvider.ParamList do
        begin
            sBaseDatosDestinoUbica := FRegistry.ServerDB + '.' + ParamByName('BaseDatos').AsString;
            sUsuario := StrDef( ParamByName( 'Usuario' ).AsString, FRegistry.UserName );
            sPassword := StrDef( ParamByName( 'Clave' ).AsString, FRegistry.PasswordRaw );
        end;
       
        oZetaProviderCompany := TdmZetaServerProvider.Create(nil);        
        oZetaProviderCompany.EmpresaActiva := VarArrayOf([sBaseDatosDestinoUbica, sUsuario,  sPassword]);

        odmPreparaPresupuestos := TdmPreparaPresupuestos.Create(nil);
        Result := odmPreparaPresupuestos.PresupuestosCatalogos(Parametros, oZetaProviderCompany);
    finally
        FreeAndNil (oZetaProviderCompany);
        FreeAndNil (FRegistry);
    end;
    SetComplete;
end;

function TdmServerSistema.EmpleadosPresupuestos(Parametros: OleVariant): WordBool;
var odmPreparaPresupuestos: TdmPreparaPresupuestos;  
    FRegistry : TZetaRegistryServer;
    oZetaProviderCompany: TdmZetaServerProvider;
    sUsuario, sPassword, sBaseDatosDestinoUbica, sBaseDatosBaseCodigo, sBaseDatosBaseNombre, sServer: String;
    dsBaseDatos2: TZetaCursor;
begin
    Result := FALSE;
    FRegistry := TZetaRegistryServer.Create(FALSE);
    dsBaseDatos2 := TZetaCursor.Create(nil);
    try
        oZetaProvider.AsignaParamList( Parametros );
        with oZetaProvider.ParamList do
        begin
            sBaseDatosDestinoUbica := FRegistry.ServerDB + '.' + ParamByName('BaseDatos').AsString;
            sBaseDatosBaseCodigo :=  ParamByName('BaseDatosBase').AsString;
            sUsuario := StrDef( ParamByName( 'Usuario' ).AsString, FRegistry.UserName );
            sPassword := StrDef( ParamByName( 'Clave' ).AsString, FRegistry.PasswordRaw );
        end;
        
        oZetaProviderCompany := TdmZetaServerProvider.Create(nil);        
        oZetaProviderCompany.EmpresaActiva := VarArrayOf([sBaseDatosDestinoUbica, sUsuario,  sPassword]);

        // ===============================================================
        oZetaProvider.EmpresaActiva := oZetaProvider.Comparte;

        dsBaseDatos2 := oZetaProvider.CreateQuery(Format(GetScript(Q_GET_BASES_DATOS_SERVER2), [sBaseDatosBaseCodigo, '', '']));
        dsBaseDatos2.Active := True;

        if not dsBaseDatos2.Eof then
        begin
              sBaseDatosBaseNombre := dsBaseDatos2.FieldByName('CM_DATOS').AsString;
              GetServerDatabase(sBaseDatosBaseNombre, sServer, sBaseDatosBaseNombre);
        end;
        // ===============================================================

        odmPreparaPresupuestos := TdmPreparaPresupuestos.Create(nil);
        Result := odmPreparaPresupuestos.PresupuestosEmpleados (Parametros, oZetaProviderCompany, sBaseDatosBaseNombre);
    finally
        FreeAndNil (dsBaseDatos2);
        FreeAndNil (oZetaProviderCompany);
        FreeAndNil (FRegistry);
    end;
    SetComplete;
end;

function TdmServerSistema.OptimizarPresupuestos(Parametros: OleVariant): WordBool;
const K_INIT_SQL = '{CALL INIT_PROCESS_LOG( %d, %d, %d, %d, %d, %s, %d )}';
var odmPreparaPresupuestos: TdmPreparaPresupuestos;
    oZetaProviderCompany: TdmZetaServerProvider;
    sUsuario, sPassword, sBaseDatosDestinoUbica: String; 
    FRegistry : TZetaRegistryServer;
begin
    Result := FALSE;
    FRegistry := TZetaRegistryServer.Create(FALSE);
    try
        oZetaProvider.AsignaParamList( Parametros );
        with oZetaProvider.ParamList do
        begin
            sBaseDatosDestinoUbica := FRegistry.ServerDB + '.' + ParamByName('BaseDatos').AsString;
            sUsuario := StrDef( ParamByName( 'Usuario' ).AsString, FRegistry.UserName );
            sPassword := StrDef( ParamByName( 'Clave' ).AsString, FRegistry.PasswordRaw );
        end;

        oZetaProviderCompany := TdmZetaServerProvider.Create(nil);        
        oZetaProviderCompany.EmpresaActiva := VarArrayOf([sBaseDatosDestinoUbica, sUsuario,  sPassword]);

        odmPreparaPresupuestos := TdmPreparaPresupuestos.Create(nil);
        odmPreparaPresupuestos.OptimizacionPresupuestos (Parametros, oZetaProviderCompany); 
        Result := TRUE;
    finally
        FreeAndNil (oZetaProviderCompany);
        FreeAndNil (FRegistry);
    end;
    SetComplete;
end;

function TdmServerSistema.GetBDFromDB_INFO(const DB_CODIGO: WideString): WideString;
var
  FGetUbicacion: TZetaCursor;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          FGetUbicacion := CreateQuery( Format (GetScript(Q_GET_BASES_DATOS_SERVER2), [DB_CODIGO]));
          try
            Ejecuta(FGetUbicacion);
            Result := FGetUbicacion.FieldByName('CM_DATOS').AsString;
          finally
            FreeAndNil(FGetUbicacion);
          end;
     end;
end;

{* Actualiza la Base de Datos Empleados desde una version inicial de Patch hasta una versi�n destino, aplicando las versiones
   intermedias.
   @author Ricardo Carrillo Morales 03/Mar/2014
   @Param Parametros Contiene los valores necesarios para aplicar el patch. Es de tipo TZetaParams.VarValues.
   @return TRUE si pudo aplicar los parches sin errores pero con advertencias, FALSE en caso contrario.

   Par�metros REQUERIDOS:
   @Param Parametros.DB_APLICAPATCH       Boolean Bandera para aplicar el Patch de Versi�n
   @Param Parametros.DB_DATOS             string  Nombre del servidor SQL Server, Instancia Y Cat�logo a aplica parche
   @Param Parametros.DB_USRNAME           string  Usuario de SQL
   @Param Parametros.DB_PASSWRD           string  Contrase�a encriptada del usuario
   @Param Parametros.DB_VERSION           integer Versi�n inicial del parche a aplicar
   @Param Parametros.DB_NUEVAVERSION      integer Versi�n final del parche a aplicar
   @Param Parametros.DB_APLICADICCIONARIO Boolean Importar Diccionario?
   @Param Parametros.DB_APLICAREPORTES    Boolean Importar Reportes?
   @Param Parametros.DB_APLICAESPECIALES  Boolean Importar Scripts especiales?
}
function TdmServerSistema.EjecutaMotorPatch(var Parametros: OleVariant): WordBool;
const
  K_SCRIPT = 'Empleados\Patch\Estructura\SQL Server %d.sql';
var
  ZParams: TZetaParams;
begin
  Result  := False;
  ZParams := TZetaParams.Create(Self);
  try
    ZParams.VarValues := Parametros;
    with ZParams do begin
      oZetaProvider.EmpresaActiva := GetArrayEmpresa(ParamValues['DB_DATOS'],
                                                     ParamValues['DB_USRNAME'],
                                                     ParamValues['DB_PASSWRD'],
                                                     '',
                                                     '');
    end;

    oZetaProvider.InitGlobales;
    ZParams.AddInteger('USER', oZetaProvider.EmpresaActiva[P_USUARIO]);
    ZParams.AddInteger('PRO_EXITOS', 0);
    ZParams.AddInteger('PRO_ERRORES', 0);
    ZParams.AddInteger('PRO_ADVERTENCIAS', 0);

    // 2 y 3er parametros diferentes: Trabaja sobre EMPRESA y deja la bit�cora en COMPARTE,
    // Ejemplo: Result := MotorPatch(ZParams, oZetaProvider, oZetaProviderComparte, GrabaCambiosBitacoraDiccion);

    // 2 y 3er parametros iguales: Trabaja y deja la bit�cora en EMPRESA
    Result := MotorPatch(ZParams, oZetaProvider, oZetaProvider, GrabaCambiosBitacoraDiccion);

    Parametros := ZParams.VarValues;
  except
    on e: Exception do
       raise Exception.Create( 'Ocurri� un error al aplicar el Patch' + CR_LF + e.Message );
  end;
  FreeAndNil(ZParams);
  SetComplete;
end;

function TdmServerSistema.CreaEspeciales(Parametros: OleVariant): WordBool;
const K_ARCHIVOS_DEFINICION = '\Empleados\NuevaBD';
var
  FRegistry : TZetaRegistryServer;
  sConnStr, sDescripcion, sUbicacion, sRutaArchivosConfiguracion, sUsuario, sPassword: String;
  oZetaProviderBD: TdmZetaServerProvider;

begin
     Result:= FALSE;
     FRegistry := TZetaRegistryServer.Create;
     oZetaProviderBD := TdmZetaServerProvider.Create(Nil);

     with oZetaProvider do
     begin
        try
            EmpresaActiva := Comparte;
            AsignaParamList( Parametros );
            with ParamList do
            begin
                 sConnStr := GetConnStr(ParamByName( 'BaseDatos' ).AsString);
                 sUsuario := StrDef( ParamByName( 'Usuario' ).AsString, FRegistry.UserName );
                 sPassword := StrDef( ParamByName( 'Clave' ).AsString, FRegistry.PasswordRaw );
                 sDescripcion := ParamByName( 'Descripcion' ).AsString;
                 sUbicacion := GetServer + '.' + ParamByName( 'BaseDatos' ).AsString;
            end;

            sRutaArchivosConfiguracion := FRegistry.PathConfig + K_ARCHIVOS_DEFINICION;

            oZetaProviderBD.EmpresaActiva := VarArrayOf([sUbicacion,
              sUsuario,  sPassword, oZetaProvider.UsuarioActivo]);

            LeeScriptEspeciales(oZetaProviderBD, sRutaArchivosConfiguracion);
        finally
              FreeAndNil(FRegistry);
              FreeAndNil(oZetaProviderBD);
        end;
     end;
     SetComplete;
     Result:= TRUE;
end;

function TdmServerSistema.ExistenEspeciales: WordBool;
const K_ARCHIVOS_DEFINICION = '\Empleados\NuevaBD';
      K_ESPECIALES= '\Especiales\';
      K_EXT_SQL = '.sql';
var sRutaArchivosConfiguracion: String;
    FRegistry : TZetaRegistryServer;
    srArchivo: TSearchRec;
begin
  Result:= FALSE;
  FRegistry := TZetaRegistryServer.Create;
  try
    sRutaArchivosConfiguracion := FRegistry.PathConfig + K_ARCHIVOS_DEFINICION;
    if (FindFirst(sRutaArchivosConfiguracion + K_ESPECIALES + '*' + K_EXT_SQL, faArchive, srArchivo)) = 0 then
      Result := TRUE;
  finally
    FreeAndNil (FRegistry);
  end;
end;

function TdmServerSistema.ExisteBD_DBINFO(const Codigo: WideString): WordBool;
var
   FDataSet: TZetaCursor;
begin
     Result := False;
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          try
              try
                  FDataSet := CreateQuery( Format( 'SELECT DB_CODIGO FROM DB_INFO WHERE DB_CODIGO = ''%s'' ', [Codigo ] ) );
                  try
                      with FDataset do
                      begin
                           Active := True;
                           if not EOF then
                              Result := True;
                           Active := False;
                      end;
                  finally
                      FreeAndNil( FDataSet );
                  end;
              except
                raise Exception.Create( Format ('No es posible conectarse a: %s. Por favor, verifique su informaci�n.', [Codigo]) );
              end;

          finally
              FreeAndNil(FDataSet);
          end;
     end;
     SetComplete;

end;

{* Recuperar la lista de Base de Datos para aplicar Patchs @author Ricardo Carrillo 14/Mar/2004}
function TdmServerSistema.ConsultaActualizarBDs(Tipo: Integer): OleVariant;
const
  K_COMPARTE_CODIGO = 'XCOMPARTEY';
var
  cdsBaseDatos        : TServerDataSet;
  oZetaProviderCompany: TdmZetaServerProvider;
  oDetallesDB         : TDetallesBD;
  sMensajeError       : string;
  sWhere              : string;
begin
  sWhere       := '';
  cdsBaseDatos := TServerDataSet.Create(oZetaProvider);

  oZetaProviderCompany := TdmZetaServerProvider.Create(Nil);
  sMensajeError        := '';

  with oZetaProvider do begin
    EmpresaActiva := Comparte;
    with cdsBaseDatos do
      try
        if (Tipo <> 0) then
          sWhere := Format(' AND DB_TIPO = %d ', [Tipo]);

        Data := OpenSQL(EmpresaActiva, Format(GetScript(Q_CONSULTA_ACTUALIZAR_BDS), [K_COMPARTE_CODIGO]) + sWhere, True);
        while not Eof do begin
          try
            Edit;
            oDetallesDB := TDetallesBD.Create(oZetaProvider, FieldByName('DB_DATOS').AsString);
            FieldByName('DB_VERSION').AsInteger       := oDetallesDB.DBVersion;
            FieldByName('DB_APLICAVERSION').AsInteger := oDetallesDB.DBMaxVersion;
            FieldByName('DB_VERSIONESPATCH').AsString := oDetallesDB.DBVersiones.Text;
            if oDetallesDB.DBVersion >= oDetallesDB.DBMaxVersion then
              FieldByName('DB_STATUS').AsString := 'Actualizada';
          except
            on e: Exception do
              FieldByName('DB_STATUS').AsString := e.Message;
          end;
          Post;
          FreeAndNil(oDetallesDB);
          Next;
        end;
        MergeChangeLog;
        Result := Data;
        Active := False;
      finally
        FreeAndNil(oZetaProviderCompany);
        FreeAndNil(oDetallesDB);
      end;
  end;
  SetComplete;
end;

function TdmServerSistema.ImportarTablas(Empresa, Parametros: OleVariant): OleVariant;
var
   sBaseDatosUbicacion, sUsuario, sPassword, sTabla: String;
   bEncimar: Boolean;
   iAdvertencias, iErrores: Integer;
begin
     iAdvertencias := 0;
     iErrores := 0;
     cdsLista.InitTempDataset;

     with oZetaProvider do
     begin
        EmpresaActiva := Comparte;
        AsignaParamList( Parametros );
        try
          with ParamList do
          begin
              sTabla := ParamByName('Tabla').AsString;
              bEncimar := ParamByName('Encimar').AsBoolean;
              sBaseDatosUbicacion := ParamByName('BaseDatosUbicacion').AsString;
              sUsuario := ParamByName('Usuario').AsString;
              sPassword := ParamByName('Password').AsString;
              cdsLista.Data := ParamByName ('DatosTabla').Value;

              FListaParametros := VACIO;
              FListaParametros := 'Base de datos: ' + ParamByName('BaseDatosUbicacion').AsString + K_PIPE
                              + 'Tabla: ' + ParamByName('Tabla').AsString + K_PIPE
                              + 'Archivo: ' + ParamByName('Archivo').AsString + K_PIPE
                              + 'Modo: ' + ParamByName('Modo').AsString;
          end;

          try
            EmpresaActiva := VarArrayOf([sBaseDatosUbicacion,
                sUsuario,  sPassword, oZetaProvider.UsuarioActivo]);
          except
              raise Exception.Create( Format ('No es posible conectarse a: %s. Por favor, verifique su informaci�n.', [sBaseDatosUbicacion]) );
          end;

          if OpenProcess(prSISTImportarTablas, cdsLista.RecordCount, FListaParametros) then
          begin
              GrabaDataSet(cdsLista, sTabla, camposLlaveAsCommaText (sTabla), oZetaProvider, iAdvertencias, iErrores, bEncimar, TRUE);
          end;
          Result := CloseProcess;
        finally

        end;
      end;
     SetComplete;
end;

function TdmServerSistema.camposLlaveAsCommaText (sTabla: String): String;
var cdsCamposLlave: TDataSet;
begin
    Result := '';
    with oZetaProvider do
    begin
        cdsCamposLlave := CreateQuery(Format (GetScript(Q_GET_CAMPOS_LLAVE), [sTabla]));
        cdsCamposLlave.Active := TRUE;
        cdsCamposLlave.First;
        while not cdsCamposLlave.Eof do
        begin
            if not StrLleno (Result) then
              Result := cdsCamposLlave.FieldByName('COLUMN_NAME').AsString
            else
              Result := Result + ',' + cdsCamposLlave.FieldByName('COLUMN_NAME').AsString;
            cdsCamposLlave.Next;
        end;
    end;
end;

function TdmServerSistema.GetTablasBD(const BaseDatosUbicacion, Usuario, Password: WideString): OleVariant;
begin
    with oZetaProvider do
    begin
        EmpresaActiva := Comparte;
        try
          EmpresaActiva := VarArrayOf([BaseDatosUbicacion,
          Usuario,  Password, 0]);

          Result:= OpenSQL( EmpresaActiva, GetScript( Q_GET_TABLAS_BD ), True );

        except
          raise Exception.Create( Format ('No es posible conectarse a: %s. Por favor, verifique su informaci�n.', [BaseDatosUbicacion]) );
        end;
    end;
    SetComplete;
end;

function TdmServerSistema.GetTablaInfo(Empresa: OleVariant; const Tabla: WideString): OleVariant;

   function GetCampos: String;
   var
      FDataset: TZetaCursor;
   begin
        Result := ZetaCommonClasses.VACIO;
        FDataset := oZetaProvider.CreateQuery( Format( GetScript( Q_GET_CAMPOS_TABLA ), [ Tabla ] ) );
        try
           with FDataset do
           begin
                Active := True;
                while ( not EOF ) do
                begin
                     Result := ConcatString( Result, FieldByName( 'NAME' ).AsString, ',' );
                     Next;
                end;
                Active := False;
           end;
        finally
               FreeAndNil( FDataset );
        end;
   end;

begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Result:= OpenSQL( Empresa, Format( GetScript( Q_GET_TABLA_EXPORTAR ), [ GetCampos, Tabla ] ), True );
     end;
     SetComplete;
end;

function TdmServerSistema.ImportarTablaCSVGetASCII(Empresa, Parametros, ListaASCII: OleVariant; var ErrorCount: Integer): OleVariant;
var sTabla, sListaCampos: String;
    i: Integer;
    dsCampos: TDataSet;
begin
     InitLog(Empresa,'ImportarTablaCSVGetASCII');
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          InitGlobales;
     end;
     oSuperASCII := TdmSuperASCII.Create( Self );
     try
        with oSuperASCII do
        begin
             Formato := eFormatoASCII(faASCIIDel);
             FormatoImpFecha := eFormatoImpFecha( oZetaProvider.ParamList.ParamByName( 'FormatoImpFecha' ).AsInteger );
             sListaCampos := oZetaProvider.ParamList.ParamByName( 'Campos' ).AsString;
             sTabla := oZetaProvider.ParamList.ParamByName( 'Tabla' ).AsString;

             // Ejecutar query con los campos que se enviaron y hacia la tabla que se importa
             // para saber el tipo de dato al agregar columna
             dsCampos := oZetaProvider.CreateQuery(Format('SELECT top 1 %s from %s', [sListaCampos, sTabla]));
             dsCampos.Active := TRUE;

             // Iterar y agregar columnas
             for i := 0 to dsCampos.Fields.Count-1 do
             begin
                with  dsCampos.FieldByName(dsCampos.Fields[i].FieldName) do
                begin
                      if (DataType = ftString) or (DataType = ftWord)  or (DataType = ftFixedChar)
                         or (DataType = ftWideString)  or (DataType = ftLongWord)  or (DataType = ftStream)
                         or (DataType = ftFixedWideChar)  or (DataType = ftWideMemo) then
                            AgregaColumna( dsCampos.Fields[i].FieldName, tgTexto, Size, TRUE )
                      else if (DataType = ftDate) or (DataType = ftTime)
                      or (DataType = ftDateTime) or (DataType = ftTimeStamp)  then
                            AgregaColumna( dsCampos.Fields[i].FieldName, tgFecha, Size, TRUE )
                      else if (DataType = ftSmallint) or (DataType = ftInteger)
                      or (DataType = ftLargeint) or (DataType = ftShortint) then
                            AgregaColumna( dsCampos.Fields[i].FieldName, tgNumero, Size, TRUE )
                      else if (DataType = ftFloat)or (DataType = ftCurrency) then
                            AgregaColumna( dsCampos.Fields[i].FieldName, tgFloat, Size, TRUE )
                      else if (DataType = ftBoolean) then
                            AgregaColumna( dsCampos.Fields[i].FieldName, tgBooleano, Size, TRUE )
                      else if (DataType = ftMemo) then
                            AgregaColumna( dsCampos.Fields[i].FieldName, tgMemo, Size, TRUE )
                      else if (DataType = ftBlob) then
                            AgregaColumna( dsCampos.Fields[i].FieldName, tgBlob, Size, TRUE )
                      else
                            AgregaColumna( dsCampos.Fields[i].FieldName, tgAutomatico, Size, TRUE );                      
                end;

             end;

             Result := Procesa( ListaASCII );
             dsCampos.Active := FALSE;
             ErrorCount := Errores;
        end;
     finally
            FreeAndNil (dsCampos);
            oSuperASCII.Free;
     end;
     EndLog; SetComplete;
end;

function TdmServerSistema.ImportarTablaCSVLista(Empresa, Lista, Parametros: OleVariant): OleVariant;
begin
     InitLog(Empresa,'ImportarTablaCSVLista');
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
     end;
     ImportarTablaCSVListaParametros;
     cdsLista.Lista := Lista;
     // Result := ImportarTablaCSVDataset( cdsLista );
     Result := ImportarTablaCSVDataset;
     EndLog; SetComplete;
end;

procedure TdmServerSistema.ImportarTablaCSVListaParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Base de datos: ' + ParamByName('BaseDatosUbicacion').AsString + K_PIPE
                              + 'Tabla: ' + ParamByName('Tabla').AsString + K_PIPE
                              + 'Archivo: ' + ParamByName('Archivo').AsString + K_PIPE
                              + 'Formato Fechas: ' + ObtieneElemento(lfFormatoImpFecha, ParamByName('FormatoImpFecha').AsInteger);
     end;
end;

function TdmServerSistema.ImportarTablaCSVDataset: OleVariant;
const K_CAMPOS_EXCLUIR = 'RENGLON, NUM_ERROR, DESC_ERROR';
var
   sTabla: String;
   iAdvertencias, iErrores: Integer;
begin
     iAdvertencias := 0;
     iErrores := 0;
     with oZetaProvider do
     begin
        with ParamList do
        begin
            sTabla := ParamByName('Tabla').AsString;
        end;

        with cdsLista do
        begin
           if OpenProcess(prSISTImportarTablasCSV, RecordCount, FListaParametros) then
              GrabaDataSet(cdsLista, sTabla, VACIO, oZetaProvider, iAdvertencias, iErrores, FALSE, TRUE, K_CAMPOS_EXCLUIR);

           Result := CloseProcess;
        end;
     end;
end;

procedure TdmServerSistema.InitLog( Empresa:Olevariant; const sPaletita: string );
begin
     {$ifdef BITACORA_DLLS}
     FPaletita := sPaletita;
     FArchivoLog := 'c:\BitacoraTress '+ FormatDateTime('dd_mmm_yy', Date()) +'.txt';

     if FListaLog = NIL then
     begin
          try
             FListaLog :=  TAsciiLog.Create;
             FListaLog.Init(FArchivoLog);
             try
                if NOT varisNull( Empresa ) then
                   try
                      FEmpresa :=  ' -- Alias: ' + Empresa[P_CODIGO] +
                                   ' Usuario: ' + InTToStr(Empresa[P_USUARIO])
                   except
                         FEmpresa := ' Error al leer Empresa[]';
                   end
                else
                    FEmpresa := '';
             except
                   FEmpresa := ' Error al leer Empresa[]';
             end;

             FListaLog.WriteTexto( FormatDateTime('dd/mmm/yy hh:nn:ss',Now) +
                                   ' DServerSistema :: INICIA :: '+  FPaletita +
                                   FEmpresa );
             FListaLog.Close;
          except
          end;
     end;
     {$endif}
end;

procedure TdmServerSistema.EndLog;
begin
     {$ifdef BITACORA_DLLS}
     try
        if FArchivoLog= '' then
           FArchivoLog := 'c:\BitacoraTress '+ FormatDateTime('dd_mmm_yy', Date()) +'.txt';
        if FListaLog = NIL then
        begin
             FListaLog :=  TAsciiLog.Create;
             FListaLog.Init(FArchivoLog);
             FListaLog.WriteTexto( FormatDateTime('dd/mmm/yy hh:nn:ss',Now) + ' DServerSistema :: TERMINA :: '+ FPaletita +' '+ FEmpresa);
        end
        else
        begin
             //FListaLog.Open(FArchivoLog);
             FListaLog.Init(FArchivoLog);
             FListaLog.WriteTexto( FormatDateTime('dd/mmm/yy hh:nn:ss',NOw) + ' DServerSistema :: TERMINA :: '+ FPaletita +' '+ FEmpresa  );
             //FListaLog.Close;
        end;
        FListaLog.Close;
        FreeAndNil(FListaLog);
     except

     end;
     {$endif}
end;

function TdmServerSistema.GetCamposTabla(const BaseDatosUbicacion, Usuario, Password, Tabla: WideString): OleVariant;
var
   oZetaProviderCompany: TdmZetaServerProvider;
begin
     oZetaProviderCompany := TdmZetaServerProvider.Create(Nil);
     with oZetaProvider do
     begin
        EmpresaActiva := Comparte;
        try
          try
            oZetaProviderCompany.EmpresaActiva := VarArrayOf([BaseDatosUbicacion,
            Usuario, Password, oZetaProvider.UsuarioActivo]);

            with oZetaProviderCompany do
            begin
              Result:= OpenSQL( EmpresaActiva, Format (GetScript( Q_GET_CAMPOS_TABLA ), [Tabla]), True );
            end;

          except
              try
                with oZetaProviderCompany do
                begin
                    Result:= OpenSQL( Comparte, Format (GetScript( Q_GET_CAMPOS_TABLA ), [Tabla]), True );
                end;
              except
                  raise Exception.Create( Format ('No es posible conectarse a: %s. Por favor, verifique su informaci�n.', [BaseDatosUbicacion]) );
              end;
          end;

        finally
             FreeAndNil(oZetaProviderCompany);
        end;
      end;
     SetComplete;
end;

function TdmServerSistema.GetCamposLlaveTabla(const BaseDatosUbicacion, Usuario, Password, Tabla: WideString): OleVariant;
var
   oZetaProviderCompany: TdmZetaServerProvider;
begin
     oZetaProviderCompany := TdmZetaServerProvider.Create(Nil);
     with oZetaProvider do
     begin
        EmpresaActiva := Comparte;
        try
          try
            oZetaProviderCompany.EmpresaActiva := VarArrayOf([BaseDatosUbicacion,
            Usuario,  Password, oZetaProvider.UsuarioActivo]);

            with oZetaProviderCompany do
            begin
              Result:= OpenSQL( EmpresaActiva, Format (GetScript( Q_GET_CAMPOS_LLAVE ), [Tabla]), True );
            end;

          except
              try
                with oZetaProviderCompany do
                begin
                    Result:= OpenSQL( Comparte, Format (GetScript( Q_GET_CAMPOS_LLAVE ), [Tabla]), True );
                end;
              except
                  raise Exception.Create( Format ('No es posible conectarse a: %s. Por favor, verifique su informaci�n.', [BaseDatosUbicacion]) );
              end;
          end;

        finally
             FreeAndNil(oZetaProviderCompany);
        end;
      end;
     SetComplete;
end;

function TdmServerSistema.GetUsuarioArbolApp(iUsuario, iExe: Integer): OleVariant;
const
     K_FILTRO = ' ( US_CODIGO = %d ) and ( AB_MODULO = %d )';
begin
     SetTablaInfo( eArbol );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( K_FILTRO, [ iUsuario, iExe ] );
          Result := GetTabla( Comparte );
     end;
     SetComplete;
end;

function TdmServerSistema.GrabaArbolApp(iUsuario, iExe: Integer; oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
const
     K_DELETE = 'delete from ARBOL where ( US_CODIGO = %d ) and ( AB_MODULO = %d ) ';
begin
     { Complemente el 'EmptyDataSet' }
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          EmpiezaTransaccion;
          try
             ExecSQL( Comparte, Format( K_DELETE, [ iUsuario, iExe ] ) );
             TerminaTransaccion( TRUE );
          except
                on Error: Exception do
                begin
                     TerminaTransaccion( FALSE );
                     raise;
                end;
          end;
          SetTablaInfo( eArbol );
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
     end;
     SetComplete;
end;


{* Regresar una tabla con datos de un proceso de actualizaci�n. @autor Ricardo Carrillo}
function TdmServerSistema.GetMotorPatchAvance(Parametros: OleVariant): OleVariant;
var
   oQuery    : TZetaCursor;
   Params    : TZetaParams;
   sAlias    : string;
   sServer   : string;
   sDatabase : string;
   sTop      : string;
   sWhere    : string;
   sSentencia: string;
begin
     Result := 0;
     oQuery := nil;
     Params := TZetaParams.Create(Self);
     Params.VarValues := Parametros;
     with oZetaProvider, Params do 
     try
        sAlias := ParamByName('DB_DATOS').AsString;
        EmpresaActiva := VarArrayOf([sAlias,
                                 ParamByName('DB_USRNAME').AsString,
                                 ParamByName('DB_PASSWRD').AsString,
                                 0]);
        try
           ZetaServerTools.GetServerDatabase(sAlias, sServer, sDatabase);
           sSentencia := 'select %s PC_NUMERO, PC_MAXIMO, PC_PASO, PC_ERROR from PROCESO where %s';

           if ParamByName('DB_PROCESO').AsInteger = 0 then 
           begin
                sTop := 'top 1';
                sWhere := 'PC_PARAMS = ' + Format(QuotedStr(MSG_CAMBIO_VERSION), [sDatabase, ParamByName('DB_VERSION').AsInteger, ParamByName('DB_APLICAVERSION').AsInteger]) + ' order by PC_FEC_INI + PC_HOR_INI desc';
           end 
           else 
           begin
                sTop := '';
                sWhere := 'PC_NUMERO = ' + ParamByName('DB_PROCESO').AsString;
           end;
           sSentencia := Format(sSentencia, [sTop, sWhere]);
           Result := OpenSQL(EmpresaActiva, sSentencia, True);
        except
              on e: Exception do
                raise Exception.Create( Format ('No es posible conectarse a: %s. Por favor, verifique su informaci�n. %s', [ParamByName('DB_DATOS').AsString, sLineBreak + e.Message]) );
        end;
     finally
            FreeAndNil(oQuery);
            SetComplete;
     end;
end;

function TdmServerSistema.PrendeDerechosEntidades(
  const Empresas: WideString; Destino: OleVariant; Bitacora,
  SoloEntidadesNuevas: WordBool; out ErrorCount: Integer): OleVariant;
begin
    try
        oZetaProviderDiccionario.EmpresaActiva := Destino;
        // 2 y 3er parametros diferentes: Trabaja sobre EMPRESA y deja la bit�cora en COMPARTE,
        // 2 y 3er parametros iguales: Trabaja y deja la bit�cora en EMPRESA
        DImportarDiccionario.PrendeDerechosEntidades (Empresas, oZetaProviderDiccionario, oZetaProviderDiccionario,
            GrabaCambiosBitacora, Bitacora, SoloEntidadesNuevas);
    except
      on Error: Exception do
      begin
        raise Exception.Create('Error al prender derechos de acceso a entidades.' + CR_LF + Error.Message);
      end;
    end;
end;

function TdmServerSistema.CantidadHuellaRegistrada(Empresa: OleVariant; Empleado,
          Biometrico, Tipo: Integer): Integer;
var
   FDataSet: TZetaCursor;
   sEmpresaEmpleado: string;
begin
     Result := 0;
     sEmpresaEmpleado := VACIO;
     oZetaProvider.EmpresaActiva := Empresa;

     with oZetaProviderComparte do
     begin
          EmpresaActiva := Comparte;
          FDataSet := CreateQuery( Format( GetScript( Q_GET_EMPRESA_SIN_CONFIDENCIALIDAD ), [ EntreComillas( oZetaProvider.CodigoEmpresaActiva ) ] )  );
          try
             with FDataSet do
             begin
                  try
                     Active := True;
                     sEmpresaEmpleado := FieldByName( 'CM_CODIGO' ).AsString;
                  finally
                         Active := False;
                  end;
             end;
          finally
                 FreeAndNil( FDataset );
          end;
     end;

     if strLleno( sEmpresaEmpleado ) then
     begin
          with oZetaProvider do
          begin
               EmpresaActiva := Empresa;
               FDataSet := CreateQuery( Format( GetScript( Q_CANTIDAD_HUELLA_EMP ), [ Empleado, Tipo, sEmpresaEmpleado ] ) );
               try
                  with FDataset do
                  begin
                       Active := True;
                       if not EOF then
                          Result := FieldByName ('CANTIDAD').AsInteger;
                       Active := False;
                  end;
               finally
                      FreeAndNil( FDataSet );
               end;
          end;
     end;
     SetComplete;
end;

procedure TdmServerSistema.ExtraerChecadasBitTermParametros;
var sTerminal: String;
begin
     with oZetaProvider, ParamList do
     begin
          if ParamByName ('Terminal').AsInteger = 0 then
            sTerminal := 'Todas'
          else
            sTerminal := IntToStr (ParamByName ('Terminal').AsInteger);
          FListaParametros := VACIO;
          FListaParametros := 'Archivo: ' + ParamByName( 'Archivo' ).AsString + K_PIPE + 'Terminal: ' + sTerminal + K_PIPE + 'Fecha inicio: ' + DateToStr (ParamByName ('Inicio').AsDate) + K_PIPE + 'Fecha fin: ' + DateToStr (ParamByName ('Fin').AsDate);
     end;
end;

function TdmServerSistema.ExtraerChecadasBitTerm(Empresa, Parametros: OleVariant; out Datos: OleVariant): OleVariant;
var
   FechaInicial, FechaFinal: TDate;
   FDataSet: TZetaCursor;
   iTerminal: Integer;
   sFiltroTerminal: String;

   procedure Agregar (const sWS_MIENT: String);
   begin
        with cdsLista do
        begin
              Append;
              FieldByName( 'WS_MIENT' ).AsString := sWS_MIENT;
              Post;
        end;
   end;

begin
     sFiltroTerminal := VACIO;
     InitLog(Empresa,'Agregar');
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          with ParamList do
          begin
               FechaInicial := ParamByName ('Inicio').AsDate;
               FechaFinal := ParamByName ('Fin').AsDate;
               iTerminal := ParamByName ('Terminal').AsInteger;
          end;
          ExtraerChecadasBitTermParametros;
          FDataSet := Nil;
          if OpenProcess( prSISTExtraerChecadas, 0, FListaParametros ) then
          begin
               try
                  with cdsLista do
                  begin
                       InitTempDataset;
                       AddStringField( 'WS_MIENT', 31 );
                       CreateTempDataset;
                  end;
                       EmpresaActiva := Comparte;
                       if iTerminal > 0 then
                          sFiltroTerminal := Format (' AND CH_RELOJ = %d', [iTerminal]);
                       FDataSet := CreateQuery( Format (GetScript( Q_EXPORTAR_CHECADAS_BIT_TERM_GTI ),
                          [FechaToStr( FechaInicial ), FechaToStr( FechaFinal )])
                          + sFiltroTerminal );
                       with FDataSet do
                       begin
                            Active := True;
                            while not Eof do
                            begin
                                 Agregar ( FieldByName( 'WS_MIENT' ).AsString);
                                 Next;
                            end;
                            Active := False;
                            EmpresaActiva := Empresa;
                       end;
                  Datos := cdsLista.Data;
               except
                     on Error: Exception do
                     begin
                          Log.Excepcion( 0, 'Error al exportar checadas.', Error );
                     end;
               end;
          end;
          Result := CloseProcess;
          FreeAndNil(FDataSet);
     end;
     EndLog;SetComplete;
end;

function TdmServerSistema.GrabaBaseAdicionales(Empresa: OleVariant; GrupoUsuarios: Integer): OleVariant;
const K_INSERTA_BASE_ADIC = 'INSERT INTO GR_AD_ACC ' +
                            ' (CM_CODIGO, GR_CODIGO, GX_CODIGO, GX_DERECHO) ' +
                            ' SELECT %s AS EMPRESA, %d AS GRUPO, GX_CODIGO, 0 AS DERECHO FROM GRUPO_AD' +
                            ' WHERE GX_CODIGO NOT IN (SELECT GX_CODIGO FROM GR_AD_ACC WHERE CM_CODIGO = %s AND GR_CODIGO = %d)';
var
   FAgregar: TZetaCursor;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          EmpiezaTransaccion;
          try
             ExecSQL( EmpresaActiva, Format( K_INSERTA_BASE_ADIC, [ EntreComillas (Empresa[P_CODIGO]), GrupoUsuarios,
                                                                    EntreComillas (Empresa[P_CODIGO]), GrupoUsuarios ] ) );
             TerminaTransaccion( TRUE );
          except
                on Error: Exception do
                begin
                     TerminaTransaccion( FALSE );
                     raise;
                end;
          end;
     end;
     SetComplete;
end;

function TdmServerSistema.GetSistEnvioEmail(Empresa: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := OpenSQL( EmpresaActiva, GetScript( Q_SIST_ENVIO_EMAIL ), True );
     end;
     SetComplete;
end;

function TdmServerSistema.GetTareaCalendario(Empresa: OleVariant; Grupo: Integer): OleVariant;
const
     K_CON_DERECHO = 'WHERE CM_CODIGO IN (SELECT distinct cm_codigo FROM ACCESO where GR_CODIGO = %d and AX_DERECHO > 0)';
var
   sQuery : String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          if Grupo = 1 then
             sQuery := Format (GetScript( Q_SIST_TAREA_CALENDARIO ), [VACIO])
          else
          begin
               sQuery := Format ( K_CON_DERECHO, [Grupo] );
               sQuery := Format ( GetScript( Q_SIST_TAREA_CALENDARIO ), [ sQuery ]);
          end;

          Result := OpenSQL( EmpresaActiva, sQuery, True );
     end;
     SetComplete;
end;

function TdmServerSistema.GrabaCalendario(oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( eSistTareaCalendario );
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          FUsuario := oZetaProvider.UsuarioActivo;
          TablaInfo.AfterUpdateRecord := AfterUpdateCalendarioReportes;
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
     end;
     SetComplete;
end;

function TdmServerSistema.GetTareaUsuario(Empresa: OleVariant; Folio: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := OpenSQL( EmpresaActiva, Format( GetScript( Q_SIST_TAREA_USUARIO ), [ Folio ] ), True );
     end;
     SetComplete;
end;

function TdmServerSistema.GetTareaRoles(Empresa: OleVariant; Folio: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := OpenSQL( EmpresaActiva, Format( GetScript( Q_SIST_TAREA_ROLES ), [ Folio ] ), True );
     end;
     SetComplete;
end;

function TdmServerSistema.GrabaTareaRoles(oDelta: OleVariant; out ErrorCount: Integer; Folio: Integer): OleVariant;
begin
     SetTablaInfo( eSistTareaRoles );
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          ExecSQL( Comparte, Format( GetScript( Q_BORRA_SIST_TAREA_ROLES ), [ Folio ] ) );
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
     end;
     SetComplete;
end;

function TdmServerSistema.GrabaTareaUsuario(oDelta: OleVariant; out ErrorCount: Integer; Folio: Integer): OleVariant;
begin
     SetTablaInfo( eSistTareaUsuario );
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          ExecSQL( Comparte, Format( GetScript( Q_BORRA_SIST_TAREA_USUARIO ), [ Folio ] ) );
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
     end;
     SetComplete;
end;

function TdmServerSistema.GetSuscripcionesCalendarioReportes(Empresa: OleVariant; Reporte: Integer): OleVariant;
begin
     SetOLEVariantToNull( Result );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Result:= OpenSQL( Comparte, Format(GetScript( Q_SIST_GET_SUSCRIPCIONES_CALENDARIO_REP ),[ CodigoEmpresaActiva, Reporte ] ), TRUE );
     end;
     SetComplete;

end;

procedure TdmServerSistema.BorrarUsuarioRolCalendario(Empresa: OleVariant; Folio, iUsCodigo: Integer; const sRoCodigo: WideString; iTipoOperacion: Integer; out iErrorCount: Integer);
const
	  DELETE_USUARIO = 1;
    DELETE_ROL = 2;
    COUNT_CALENDARIO_REGISTROS = 3;
    PUNTERO_EMPRESA = 5;
    QUERY_COUNT_REGISTROS = 'select count (CA_FOLIO) from V_SUSCRIP where ((RO_CODIGO <> '''') or (US_CODIGO <> 0) or (RO_CODIGO <> null) or (US_CODIGO <> null)) '+
                            'and CM_CODIGO = ''%s'' and CA_FOLIO = %d';

var
    FDataSet: TZetaCursor;
begin
    try
         FDataSet := Nil;
         iErrorCount := 0;
         if iTipoOperacion > 0 then
         begin
               with oZetaProvider do
               begin
                     case iTipoOperacion of
                         DELETE_USUARIO:
                                        begin
                                              ExecSQL( Comparte, Format( GetScript( Q_SIST_UPDATE_USUARIO_CALENDARIO ), [ iUsCodigo, Folio ] ) );
                                        end;
                         DELETE_ROL:
                                     begin
                                          ExecSQL( Comparte, Format( GetScript( Q_SIST_UPDATE_ROL_CALENDARIO ), [ sRoCodigo, Folio ] ) );
                                     end;

                         COUNT_CALENDARIO_REGISTROS:
                                     begin
                                          EmpresaActiva := Comparte;
                                          FDataSet := CreateQuery( Format( QUERY_COUNT_REGISTROS, [ Empresa[PUNTERO_EMPRESA], Folio ] ) );
                                          with FDataSet do
                                          begin
                                               Active := True;
                                               iErrorCount := Fields[ 0 ].AsInteger;
                                               Active := False;
                                          end;
                                     end;
                     end;
               end;
               SetComplete;
         end;
    Except
           on Error: Exception do
           begin
                FreeAndNil( FDataset );
           end;
    end;

end;

function TdmServerSistema.GetUsuarioSuscripCalendarioReporte(
  Empresa: OleVariant; Usuario: Integer): OleVariant;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     Result:= GetSuscripcionesCalendarioUsuario( 'US_CODIGO', Usuario );
     SetComplete;
end;

function TdmServerSistema.GetSuscripcionesCalendarioUsuario(const sCampo: string; const iCodigo:integer ): Olevariant;
var
   sScript: String;
begin
     with oZetaProvider do
     begin
          sScript := Format( GetScript( Q_GET_USUARIO_SUSCRIP_CALENDARIO ), [ sCampo, iCodigo, CodigoEmpresaActiva ] );
          Result:= OpenSQL( EmpresaActiva, sScript, True );
     end;
end;

function TdmServerSistema.GetBitacoraReportes(Empresa, Parametros: OleVariant): OleVariant;
var
   sSQL: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          AsignaParamList( Parametros );
          with ParamList do
          begin
               if FindParam( 'FOLIO' ) <> nil then
                  sSQL := Format( GetScript( Q_BITACORA_REPORTES_FOLIO ),
                             [ ParamByName( 'FOLIO' ).AsInteger ] )
               else
                   sSQL := Format( GetScript( Q_BITACORA_REPORTES ),
                             [ 'BI_FECHA',
                               DateToStrSQLC( ParamByName( 'FechaInicial' ).AsDate ),
                               DateToStrSQLC( ParamByName( 'FechaFinal' ).AsDate + 1 ),
                               ParamByName( 'Tipo' ).AsInteger
                                ] );
          end;
          Result := OpenSQL( EmpresaActiva, sSQL, True );
     end;
     SetComplete;
end;

function TdmServerSistema.GetBitacoraCorreos(Empresa, Parametros: OleVariant): OleVariant;
var
   sSQL: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          AsignaParamList( Parametros );
          with ParamList do
          begin

               sSQL := Format( GetScript( Q_BITACORA_CORREOS ),
                             [ 'EE_FEC_IN',
                               DateToStrSQLC( ParamByName( 'FechaInicial' ).AsDate ),
                               DateToStrSQLC( ParamByName( 'FechaFinal' ).AsDate + 1 ),
                               ParamByName( 'Tipo' ).AsInteger
                                ] );
          end;
          Result := OpenSQL( EmpresaActiva, sSQL, True );
     end;
     SetComplete;
end;

function TdmServerSistema.EnviarCorreo(const sUsuario, sBody, sRemitente,
  sDescripRemitente, sSubject, Encriptacion: WideString;
  oListaCorreos: OleVariant; const sDirecErrores: WideString;
  iSubtype: Integer): OleVariant;
const
    EXISTE_CORREO = 'select US_NOMBRE, US_EMAIL, US_CODIGO FROM USUARIO WHERE ( US_CODIGO = %d )';//'select US_NOMBRE, US_EMAIL, US_CODIGO FROM USUARIO WHERE ( US_CORTO = %s )';
    CORREO_FORMATO = 'INSERT INTO ENVIOEMAIL (EE_TO, EE_CC, EE_SUBJECT, EE_BODY, EE_FROM_NA, EE_FROM_AD, EE_ERR_AD, EE_SUBTYPE, EE_STATUS, EE_ENVIADO,'+
    ' EE_MOD_ADJ, EE_FEC_IN, EE_FEC_OUT, EE_ATTACH, EE_BORRAR, EE_LOG, EE_ENCRIPT)'+
    'VALUES (''%s'', '''', ''%s'', %s, '+
    '''%s'', ''%s'',  ''%s'', %d, 0, ''N'', ''N'', GETDATE(), '''', '''', ''N'', '''', ''%s'')';
var
   FDataset: TZetaCursor;
   sCorreo, sUsuarioNombre : string;
   iCodigoUsuario : integer;
   oUsuariosMails: TStrings;
   sUsCorto : string;
   sSQL: string;
   sBodyEncriptado : string;
   iIndice : integer;
        procedure VariantToStream(const Data: OleVariant; Stream: TStream);
        var
          p: Pointer;
        begin
          p := VarArrayLock(Data);
          try
            Stream.Write(p^, VarArrayHighBound(Data,1) + 1);  //assuming low bound = 0
          finally
            VarArrayUnlock(Data);
          end;
        end;

        procedure VariantToStringlist(const Data: OleVariant; aStrlist: TStrings);
        var
          hStream: TStream;
        begin
          hStream := TMemoryStream.Create;
          try
            VariantToStream(Data,hStream);
            hStream.Seek(0,soFromBeginning);
            aStrList.LoadFromStream(hStream);
          finally
            hStream.Free;
          end;
        end;

        procedure InsertarRegistroCorreo;
        begin
            try
                 FDataSet := Nil;
                 with oZetaProvider do
                 begin
                       if Encriptacion = 'S' then
                       begin
                             sBodyEncriptado := VACIO;
                             if sCorreo <> VACIO then
                             begin
                                  sBodyEncriptado := ZetaServerTools.Encrypt(sBody);
                                  sSQL := Format(  CORREO_FORMATO , [ sCorreo, sSubject, entrecomillas(sBodyEncriptado), sDescripRemitente, sRemitente, sDirecErrores, iSubtype, Encriptacion]);
                                  ExecSQL( Comparte, sSQL );
                             end;
                       end
                       else if Encriptacion = 'N' then
                       begin
                             if sCorreo <> VACIO then
                             begin
                                  sSQL := Format(  CORREO_FORMATO , [ sCorreo, sSubject, entrecomillas(sBody), sDescripRemitente, sRemitente, sDirecErrores, iSubtype, Encriptacion]);
                                  ExecSQL( Comparte, sSQL );
                             end;
                       end;
                 end;
                 SetComplete;
            Except
            end;
        end;

begin
     try
         oUsuariosMails := TStringList.Create;
         if oListaCorreos <> NULL then
         begin
              VariantToStringlist(oListaCorreos,oUsuariosMails);
         end;


         if sUsuario <> VACIO then
         begin
             with oZetaProvider do
             begin
                  iCodigoUsuario := StrToInt(sUsuario);
                  sCorreo := VACIO;
                  EmpresaActiva := Comparte;
                  FDataset := CreateQuery( Format(  EXISTE_CORREO , [ iCodigoUsuario ] ) );
                  try
                     with FDataset do
                     begin
                          Active := True;
                          if EOF then
                             sCorreo := ''
                          else
                              sCorreo := FieldByName( 'US_EMAIL' ).AsString;
                              sUsuarioNombre := FieldByName( 'US_NOMBRE' ).AsString;
                              sUsCorto := FieldByName( 'US_CORTO' ).AsString;
                          Active := False;
                     end;
                  finally
                         FreeAndNil( FDataset );
                  end;
             end;
         end;


         if ( oUsuariosMails.Count > 0 ) then
         begin
              sCorreo := VACIO;
              for iIndice := 0 to ( oUsuariosMails.Count - 1 ) do
              begin
                   sCorreo := oUsuariosMails.Strings[ iIndice ];
                   InsertarRegistroCorreo;
              end;
         end
         else
         begin
              InsertarRegistroCorreo;
         end;
     finally
            FreeAndNil( oUsuariosMails);
     end;

end;

function TdmServerSistema.ImportarSuscripcionesTressEmailPorTarea(Empresa: OleVariant; FolioTarea: Integer; const Frecuencia, Usuarios: WideString; Reporte: Integer; out Error: WideString): OleVariant;
const
     K_FILTRO_REPORTE = ' RE_CODIGO = %d ';
     K_FILTRO_USUARO = ' US_CODIGO IN (%s) ';
     K_FILTRO_FRECUENCIA = ' SU_FRECUEN = %d ';
     K_FREC_DIARIA = 'D';
     K_FREC_SEMANAL = 'S';
     K_FREC_MENSUAL = 'M';
     K_FREC_PORHORA = 'P';
     K_FREC_ESPECIAL = 'E';

     K_FREC_DIARIA_T = 0;
     K_FREC_SEMANAL_T = 1;
     K_FREC_MENSUAL_T = 2;
     K_FREC_PORHORA_T = 3;
     K_FREC_ESPECIAL_T = 4;
var
   FSuscrip: TZetaCursor;
   FInsertaRolUsuario: TZetaCursor;
   FInsertaTareaPorSuscripcion: TZetaCursor;
   FUpdateTareaPorSuscripcion: TZetaCursor;
   FLeeCalendario: TZetaCursor;
   sConsulta: String;
   sFiltro: String;
   iReporteActual: Integer;
   sTareaActual: String;
   sCA_NOMBRE: String;

   procedure agregaFiltro (nuevoFiltro: String);
   begin
        if sFiltro = VACIO then
        begin
             sFiltro := ' WHERE ' + nuevoFiltro;
        end
        else
        begin
             sFiltro := sFiltro + ' AND ' + nuevoFiltro;
        end;
   end;

   procedure actualizaCalendario ();
   begin
        FUpdateTareaPorSuscripcion := oZetaProviderComparte.CreateQuery
                    (Format (GetScript (Q_UPDATE_CALENDARIO_DESDE_SUSC), [iReporteActual, iReporteActual]));
        try
           with oZetaProviderComparte do
           begin
                EmpiezaTransaccion;
                try
                   Ejecuta (FUpdateTareaPorSuscripcion );
                   TerminaTransaccion( TRUE );
                except
                      on ErrorTareaSuscrip: Exception do
                      begin
                           TerminaTransaccion( FALSE );
                           Error := 'Error al generar el env�o desde suscripci�n de reportes (actualizaci�n en Solicitudes de env�os programados): '
                                 + ErrorTareaSuscrip.Message;
                      end;
                end;
           end;
        finally
               FreeAndNil(FUpdateTareaPorSuscripcion);
        end;
   end;

   procedure insertaCalendario ();
   begin
        FInsertaTareaPorSuscripcion := oZetaProviderComparte.CreateQuery
                                    (Format (GetScript (Q_INSERT_CALENDARIO_DESDE_SUSC), [EntreComillas (sCA_NOMBRE), FolioTarea]));
        try
           with oZetaProviderComparte do
           begin
                EmpiezaTransaccion;
                try
                   Ejecuta (FInsertaTareaPorSuscripcion );
                   TerminaTransaccion( TRUE );
                except
                      on ErrorTareaSuscrip: Exception do
                      begin
                           TerminaTransaccion( FALSE );
                           Error := 'Error al generar el env�o desde suscripci�n de reportes (inserci�n en Solicitudes de env�os programados): '
                                 + ErrorTareaSuscrip.Message;
                      end;
                end;
           end;
        finally
               FreeAndNil(FInsertaTareaPorSuscripcion);
        end;
        // ----- -----

        iReporteActual := FSuscrip.FieldByName ('RE_CODIGO').AsInteger;
        // Actualizar Folio.
        FolioTarea := FolioTarea + 1;
        // ----- -----
   end;

begin
     try
        sFiltro := VACIO;
        iReporteActual := 0;

        // Armar consulta.
        sConsulta := Format (GetScript (Q_LEER_SUSCRIP), [Reporte]);

        if Reporte > 0 then
        begin
             // sConsulta := sConsulta + Format (K_FILTRO_USUARO, [Usuarios]);
             agregaFiltro (Format (K_FILTRO_REPORTE, [Reporte]));
        end;

        // Si lista de usuarios es diferente de VACIO,
        // agregar filtro de usuarios.
        if Usuarios <> VACIO then
        begin
             // sConsulta := sConsulta + Format (K_FILTRO_USUARO, [Usuarios]);
             agregaFiltro (Format (K_FILTRO_USUARO, [Usuarios]));
        end;

        if Frecuencia <> VACIO then
        begin
             if Frecuencia = K_FREC_DIARIA then
                // sConsulta := sConsulta + Format (K_FILTRO_FRECUENCIA, [K_FREC_DIARIA_T])
                agregaFiltro(Format (K_FILTRO_FRECUENCIA, [K_FREC_DIARIA_T]))
             else if Frecuencia = K_FREC_SEMANAL then
                // sConsulta := sConsulta + Format (K_FILTRO_FRECUENCIA, [K_FREC_SEMANAL_T])
                agregaFiltro (Format (K_FILTRO_FRECUENCIA, [K_FREC_SEMANAL_T]))
             else if Frecuencia = K_FREC_MENSUAL then
                // sConsulta := sConsulta + Format (K_FILTRO_FRECUENCIA, [K_FREC_MENSUAL_T])
                agregaFiltro(Format (K_FILTRO_FRECUENCIA, [K_FREC_MENSUAL_T]))
             else if Frecuencia = K_FREC_PORHORA then
                // sConsulta := sConsulta + Format (K_FILTRO_FRECUENCIA, [K_FREC_PORHORA_T])
                agregaFiltro (Format (K_FILTRO_FRECUENCIA, [K_FREC_PORHORA_T]))
             else if Frecuencia = K_FREC_ESPECIAL then
                // sConsulta := sConsulta + Format (K_FILTRO_FRECUENCIA, [K_FREC_ESPECIAL_T])
                agregaFiltro(sConsulta + Format (K_FILTRO_FRECUENCIA, [K_FREC_ESPECIAL_T]))
        end;
        sConsulta := sConsulta + sFiltro + ' ORDER BY RE_CODIGO, US_CODIGO';

        // Si Reporte = 0, obtener CA_NOMBRE.
        // Se necesitar� para generar las tareas base (con Reporte = 0).
        if Reporte = 0 then
        begin
             sCA_NOMBRE := VACIO;
             with oZetaProviderComparte do
             begin
                  EmpresaActiva := Comparte;
                  FLeeCalendario := CreateQuery (Format (GetScript (Q_LEE_CALENDARIO), [FolioTarea]));
                  try
                     try
                        FLeeCalendario.Active := TRUE;
                        sCA_NOMBRE := FLeeCalendario.FieldByName('CA_NOMBRE').AsString;
                     finally
                             FLeeCalendario.Active := FALSE;
                     end;
                  finally
                         FreeAndNil(FLeeCalendario);
                  end;
             end;
        end;

        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             FSuscrip := CreateQuery (sConsulta);

             oZetaProviderComparte.EmpresaActiva := oZetaProviderComparte.Comparte;
             FInsertaRolUsuario := oZetaProviderComparte.CreateQuery (GetScript (Q_INSERT_ROLUSUARIO));

             try
                try
                   try
                      FSuscrip.Active := True;

                      while not FSuscrip.EOF do
                      begin
                           if Reporte = 0 then
                           begin
                                if (iReporteActual <> 0) and
                                   (iReporteActual <> FSuscrip.FieldByName ('RE_CODIGO').AsInteger) then
                                begin
                                     // actualizar registro en Calendario.
                                     actualizaCalendario ();
                                     // Hacer inserci�n de registro base en Calendario.
                                     insertaCalendario ();
                                end;
                           end;

                           iReporteActual := FSuscrip.FieldByName ('RE_CODIGO').AsInteger;
                           with oZetaProviderComparte do
                           begin
                                // oZetaProviderComparte.EmpresaActiva := Comparte;
                                EmpiezaTransaccion;
                                try
                                   ParamAsString (FInsertaRolUsuario, 'US_CODIGO', IntToStr ( FSuscrip.FieldByName ('US_CODIGO').AsInteger));
                                   ParamAsInteger (FInsertaRolUsuario, 'CA_FOLIO', FolioTarea );
                                   ParamAsString (FInsertaRolUsuario, 'RU_ACTIVO', 'S' );
                                   ParamAsInteger (FInsertaRolUsuario, 'RU_VACIO', FSuscrip.FieldByName ('SU_VACIO').AsInteger);
                                   ParamAsInteger (FInsertaRolUsuario, 'RU_US_CHG', 0 );
                                   Ejecuta (FInsertaRolUsuario );
                                   TerminaTransaccion( TRUE );
                                except
                                      on ErrorEx1: Exception do
                                      begin
                                           TerminaTransaccion( FALSE );
                                           Error := 'Error al importar suscripciones. Mensaje: ' + ErrorEx1.Message;
                                      end;
                                end;
                           end;
                           FSuscrip.Next;
                      end;

                      if Reporte = 0 then
                         actualizaCalendario();


                   except
                         on ErrorEx2: Exception do
                         begin
                              Error := 'Error al importar suscripciones. Mensaje: ' + ErrorEx2.Message;
                              // SetProcessError( Result );
                         end;
                   end;
                finally
                       FSuscrip.Active := False;
                end;
           finally
                  FreeAndNil (FInsertaRolUsuario);
                  FreeAndNil (FInsertaTareaPorSuscripcion);
                  FreeAndNil (FSuscrip);
           end;
        end;
     except
           on ErrorBase: Exception do
           begin
                Error := 'Error al importar suscripciones. Mensaje: ' + ErrorBase.Message;
           end;
     end;
end;

function TdmServerSistema.GetTareaCalendarioEmpresa(Empresa: OleVariant; Grupo: Integer): OleVariant;
const
     X_EMPRESA = ' AND CM_CODIGO = %s';
     K_CON_DERECHO = ' AND CM_CODIGO IN (SELECT distinct cm_codigo FROM V_ACCESO where GR_CODIGO = %d and AX_DERECHO > 0) and CM_CODIGO = %s';
var
   sQuery : String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          if Grupo = 1 then
          begin
             sQuery := Format ( X_EMPRESA, [EntreComillas (Empresa[P_CODIGO])] );
             sQuery := Format (GetScript( Q_SIST_TAREA_CALENDARIO_EMPRESA ), [ sQuery ])
          end
          else
          begin
               sQuery := Format ( K_CON_DERECHO, [Grupo, EntreComillas (Empresa[P_CODIGO])] );
               sQuery := Format ( GetScript( Q_SIST_TAREA_CALENDARIO_EMPRESA ), [ sQuery ]);
          end;

          Result := OpenSQL( EmpresaActiva, sQuery, True );
     end;
     SetComplete;
end;

function TdmServerSistema.GetSolicitudEnvio(Empresa, Parametros: OleVariant): OleVariant;
var
   sSQL: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          AsignaParamList( Parametros );
          with ParamList do
          begin

               sSQL := Format( GetScript( Q_SOLICITUD_ENVIOS ),
                             [ 'CAL_FECHA',
                               DateToStrSQLC( ParamByName( 'FechaInicial' ).AsDate ),
                               DateToStrSQLC( ParamByName( 'FechaFinal' ).AsDate + 1 ),
                               ParamByName( 'Tipo' ).AsInteger
                                ] );
          end;
          Result := OpenSQL( EmpresaActiva, sSQL, True );
     end;
     SetComplete;
end;

procedure TdmServerSistema.AgregarSolicitudEnvioProgramado(Folio, Reporte: Integer; const Nombre: WideString; out Mensaje: WideString);
const K_ERROR = 'Este env�o programado se encuentra pendiente de ejecuci�n.' + CR_LF + 'No es posible ejecutar.';
var
   FInsertaSolicitudEnvio, FSeleccionaCalensoli: TZetaCursor;
begin
     oZetaProviderComparte.EmpresaActiva := oZetaProviderComparte.Comparte;
     FSeleccionaCalensoli := oZetaProviderComparte.CreateQuery(Format(GetScript(Q_LEER_CALENSOLI), [Folio, ord(tcSEPendiente)]));
     try
        FSeleccionaCalensoli.Active := True;
        if FSeleccionaCalensoli.RecordCount = 0 then
        begin
             FInsertaSolicitudEnvio := oZetaProviderComparte.CreateQuery (GetScript (Q_INSERT_CALENSOLI));
             try
                with oZetaProviderComparte do
                begin
                     EmpiezaTransaccion;
                     try
                        ParamAsInteger (FInsertaSolicitudEnvio, 'CA_FOLIO', Folio );
                        ParamAsInteger (FInsertaSolicitudEnvio, 'RE_CODIGO', Reporte );
                        ParamAsInteger (FInsertaSolicitudEnvio, 'CAL_ESTATUS', ord(tcSEPendiente) );
                        ParamAsDateTime( FInsertaSolicitudEnvio, 'CAL_FECHA', Now );
                        Ejecuta (FInsertaSolicitudEnvio );
                        TerminaTransaccion( TRUE );
                     except
                           on Error: Exception do
                           begin
                                TerminaTransaccion( FALSE );
                                Mensaje := 'Error al ejecutar el env�o: ' + Error.Message;
                           end;
                     end;
                end;
             finally
                    FreeAndNil(FInsertaSolicitudEnvio);
             end;
        end
        else
            Mensaje := K_ERROR;
     finally
             FreeAndNil(FSeleccionaCalensoli);
     end;
end;

procedure TdmServerSistema.CancelarEnvio(Folio: Integer; var Mensaje: WideString);
var
   FSolicitudEnvio: TZetaCursor;
begin
     oZetaProviderComparte.EmpresaActiva := oZetaProviderComparte.Comparte;
     FSolicitudEnvio := oZetaProviderComparte.CreateQuery (GetScript (Q_UPDATE_CALENSOLI));
     try
        with oZetaProviderComparte do
        begin
             EmpiezaTransaccion;
             try
                ParamAsInteger (FSolicitudEnvio, 'CAL_FOLIO', Folio );
                ParamAsInteger (FSolicitudEnvio, 'CAL_ESTATUS', ord(tcSECancelar) );
                Ejecuta (FSolicitudEnvio );
                TerminaTransaccion( TRUE );
             except
                   on Error: Exception do
                   begin
                        TerminaTransaccion( FALSE );
                        Mensaje := 'Error al ejecutar el env�o: ' + Error.Message;
                   end;
             end;
        end;
     finally
            FreeAndNil(FSolicitudEnvio);
     end;
end;

procedure TdmServerSistema.Method1;
begin

end;

procedure TdmServerSistema.SetEnviosProgramados(out sError: WideString);
const K_EXECUTE_SP_CALENDARIO = 'EXECUTE SP_CALENDARIO';
begin
     oZetaProviderComparte.EmpresaActiva := oZetaProviderComparte.Comparte;
     try
        with oZetaProviderComparte do
        begin
             ExecSQL( Comparte, K_EXECUTE_SP_CALENDARIO );
        end;
     except on E: Exception do
            sError := E.Message;
     end;
end;

function TdmServerSistema.GetEmpresasAccesosGruposAcceso(Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          AsignaParamList( Parametros );
          Result:= OpenSQL( Comparte, Format( GetScript( Q_GET_EMPRESAS_ACCESOS_GRUPO_USUARIOS ), [ ParamList.ParamByName('GR_CODIGO').AsInteger,
                                      GetTipoCompanyDerechosGrupoUsuarios( ParamList.ParamByName('TIPO_COMPANY').AsInteger ),
                                      ParamList.ParamByName('GR_CODIGO_ACTIVO').AsInteger,
                                      EntreComillas( ParamList.ParamByName('ASIGNA_DERECHOS_OTROS_GRUPOS').AsString ),
                                      EntreComillas( ParamList.ParamByName('ASIGNA_DERECHOS_GRUPO_PROPIO').AsString ) ] ),
                                      True );
     end;
     SetComplete;
end;

//Davol #Bug: 21992 //#Bug: 15995
function TdmServerSistema.GetUserConfidencialidad(UserId: Integer; const Empresa: WideString): OleVariant;
const K_QUERY_CONFIDENCIALIDAD = 'SELECT US.CM_CODIGO from  USUARIO US, COMPANY C join COMPANY CBaja on C.DB_CODIGO = CBaja.DB_CODIGO ' +
                                 'where Cbaja.CM_CODIGO = %s and US.CM_CODIGO = C.CM_CODIGO and US.CB_CODIGO = %d';
var FDataSet: TZetaCursor;
begin
     oZetaProviderComparte.EmpresaActiva := oZetaProviderComparte.Comparte;
     with oZetaProviderComparte do
     begin
          FDataSet:= CreateQuery( Format( K_QUERY_CONFIDENCIALIDAD, [ EntreComillas (Empresa), UserId] ));
          try
             Result := VACIO;
             FDataSet.Active := True;
             if FDataSet.RecordCount > 0 then
                Result := FDataSet.Fields[0].AsString;
          finally
                 FreeAndNil(FDataSet);
          end;

     end;
     SetComplete;
end;

initialization

 TComponentFactory.Create(ComServer, TdmServerSistema, Class_dmServerSistema, ciMultiInstance, ZetaServerTools.GetThreadingModel);
 {$ENDIF}

 end.
