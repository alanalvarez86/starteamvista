unit FEvaluadorPrenomina;

interface

uses Windows, Messages, SysUtils, Classes,
     Variants,
     DZetaServerProvider,
     ZetaTipoEntidad,
     ZCreator,
     ZetaSQLBroker,
     ZEvaluador,
     ZetaQRExpr,
     ZetaCommonClasses,
     ZetaCommonLists;

type
    TResultadosEvaluador = record
      DiasOrdinarios : TPesos;
      TotalEvaluado1  : TPesos;
      TotalEvaluado2  : TPesos;
      TotalEvaluado3  : TPesos;
      TotalEvaluado4  : TPesos;
      TotalEvaluado5  : TPesos;
    end;
    TNominaEvaluator = class( TObject )
    private
      { Private declarations }
      FEvaluador: TZetaEvaluador;
      FResultadosEval: TResultadosEvaluador;
      FSQLBroker: TSQLBroker;
      FQuery: TZetaCursor;
      FCreator: TZetaCreator;
      function GetZetaProvider: TdmZetaServerProvider;
    protected
      { Protected declarations }
      property oZetaProvider: TdmZetaServerProvider read GetZetaProvider;
      property oZetaCreator: TZetaCreator read FCreator;
    public
      { Public declarations }
      property ResultadosEval: TResultadosEvaluador read FResultadosEval;
      constructor Create( Creator: TZetaCreator );
      destructor Destroy; override;
      procedure CalcularBegin( oPeriodo: TDatosPeriodo );
      procedure Calcular( const iEmpleado: TNumEmp );
      procedure CalcularEnd;
    end;

implementation

uses ZetaCommonTools;

const
     K_CAMPO_DIAS = 'DIAS_EVAL';
     K_CAMPO_TOTAL1 = 'TOTAL_EV1';
     K_CAMPO_TOTAL2 = 'TOTAL_EV2';
     K_CAMPO_TOTAL3 = 'TOTAL_EV3';
     K_CAMPO_TOTAL4 = 'TOTAL_EV4';
     K_CAMPO_TOTAL5 = 'TOTAL_EV5';

constructor TNominaEvaluator.Create( Creator: TZetaCreator );
begin
     FCreator := Creator;
end;

destructor TNominaEvaluator.Destroy;
begin
     if Assigned( FEvaluador ) then
        FreeAndNil( FEvaluador );
     if Assigned( FQuery ) then
        FreeAndNil( FQuery );
     if Assigned( FSQLBroker ) then
        FreeAndNil( FSQLBroker );
     inherited Destroy;
end;

function TNominaEvaluator.GetZetaProvider: TdmZetaServerProvider;
begin
     Result := FCreator.oZetaProvider;
end;

procedure TNominaEvaluator.CalcularBegin( oPeriodo: TDatosPeriodo );
begin
     FEvaluador := TZetaEvaluador.Create( oZetaCreator );
     FEvaluador.Entidad := enNomina;
     FSQLBroker := TSQLBroker.Create( oZetaCreator );
     FSQLBroker.Init( enNomina );
     oZetaCreator.RegistraFunciones( [ efComunes, efTress ] );
     with FSQLBroker do
     begin
          with Agente, oPeriodo do
          begin
               // Formula de D�as ordinarios
               if strLleno( FormulaDias ) then
                  AgregaColumna( LimpiaFormula( FormulaDias ), FALSE, enNomina, tgFloat, 0, K_CAMPO_DIAS )
               else
                  AgregaColumna( '0', TRUE, enNinguno, tgFloat, 0, K_CAMPO_DIAS );
               // Formula Total #1
               if strLleno( FormulaTotal1 ) then
                  AgregaColumna( LimpiaFormula( FormulaTotal1 ), FALSE, enNomina, tgFloat, 0, K_CAMPO_TOTAL1 )
               else
                  AgregaColumna( '0', TRUE, enNinguno, tgFloat, 0, K_CAMPO_TOTAL1 );
               // Formula Total #2
               if strLleno( FormulaTotal2 ) then
                  AgregaColumna( LimpiaFormula( FormulaTotal2 ), FALSE, enNomina, tgFloat, 0, K_CAMPO_TOTAL2 )
               else
                  AgregaColumna( '0', TRUE, enNinguno, tgFloat, 0, K_CAMPO_TOTAL2 );
               // Formula Total #3
               if strLleno( FormulaTotal3 ) then
                  AgregaColumna( LimpiaFormula( FormulaTotal3 ), FALSE, enNomina, tgFloat, 0, K_CAMPO_TOTAL3 )
               else
                  AgregaColumna( '0', TRUE, enNinguno, tgFloat, 0, K_CAMPO_TOTAL3 );
               // Formula Total #4
               if strLleno( FormulaTotal4 ) then
                  AgregaColumna( LimpiaFormula( FormulaTotal4 ), FALSE, enNomina, tgFloat, 0, K_CAMPO_TOTAL4 )
               else
                  AgregaColumna( '0', TRUE, enNinguno, tgFloat, 0, K_CAMPO_TOTAL4 );
               // Formula Total #5
               if strLleno( FormulaTotal5 ) then
                  AgregaColumna( LimpiaFormula( FormulaTotal5 ), FALSE, enNomina, tgFloat, 0, K_CAMPO_TOTAL5 )
               else
                  AgregaColumna( '0', TRUE, enNinguno, tgFloat, 0, K_CAMPO_TOTAL5 );

               AgregaFiltro( 'NOMINA.CB_CODIGO = :Empleado', TRUE, enNomina );
               AgregaFiltro( Format( 'NOMINA.PE_YEAR = %d', [ Year ] ), TRUE, Entidad );
               AgregaFiltro( Format( 'NOMINA.PE_TIPO = %d', [ Ord( Tipo ) ] ), TRUE, Entidad );
               AgregaFiltro( Format( 'NOMINA.PE_NUMERO = %d', [ Numero ] ), TRUE, Entidad );
          end;
          if SuperSQL.Construye( Agente ) then
          begin
               if SuperReporte.Prepara( Agente ) then
               begin
                    FQuery := SuperReporte.QueryReporte;
               end
               else
               begin
                    raise Exception.Create( Agente.ListaErrores.Text );
               end;
          end
          else
          begin
               raise Exception.Create( Agente.ListaErrores.Text );
          end;
     end;
end;

procedure TNominaEvaluator.Calcular( const iEmpleado: TNumEmp );
begin
     with FQuery do
     begin
          Active := False;
          with oZetaCreator.oZetaProvider do
          begin
               ParamAsInteger( FQuery, 'Empleado', iEmpleado );
          end;
     end;
     with FSQLBroker.SuperReporte do
     begin
          AbreDataSet;
          with DatasetReporte, FResultadosEval do
          begin
               DiasOrdinarios := FieldByName( K_CAMPO_DIAS ).AsFloat;
               TotalEvaluado1 := FieldByName( K_CAMPO_TOTAL1 ).AsFloat;
               TotalEvaluado2 := FieldByName( K_CAMPO_TOTAL2 ).AsFloat;
               TotalEvaluado3 := FieldByName( K_CAMPO_TOTAL3 ).AsFloat;
               TotalEvaluado4 := FieldByName( K_CAMPO_TOTAL4 ).AsFloat;
               TotalEvaluado5 := FieldByName( K_CAMPO_TOTAL5 ).AsFloat;
          end;
     end;
     FQuery.Active := False;
end;

procedure TNominaEvaluator.CalcularEnd;
begin
     { ... }
     { ... }
end;

end.
