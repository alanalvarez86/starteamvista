library PortalTress;

uses
  ComServ,
  PortalTress_TLB in 'PortalTress_TLB.pas',
  DServerPortalTress in 'DServerPortalTress.pas' {dmPortalTress: TMtsDataModule} {dmPortalTress: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
