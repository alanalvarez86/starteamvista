library GlobalVisitantes;

uses
  ComServ,
  GlobalVisitantes_TLB in 'GlobalVisitantes_TLB.pas',
  DServerGlobalVisitantes in 'DServerGlobalVisitantes.pas' {dmServerGlobalVisitantes: TMtsDataModule} {dmServerGlobalVisitantes: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
