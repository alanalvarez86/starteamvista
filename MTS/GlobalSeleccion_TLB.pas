unit GlobalSeleccion_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision: 4$
// File generated on 04/07/2006 05:12:33 p.m. from Type Library described below.

// ************************************************************************ //
// Type Lib: D:\3win_20\MTS\GlobalSeleccion.tlb (1)
// IID\LCID: {09D3B631-B809-11D5-A56C-0050DA04EC66}\0
// Helpfile: 
// DepndLst: 
//   (1) v1.0 Midas, (C:\WINDOWS\system32\midas.dll)
//   (2) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
//   (3) v4.0 StdVCL, (C:\WINDOWS\system32\stdvcl40.dll)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, OleCtrls, StdVCL, 
  MIDAS;

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  GlobalSeleccionMajorVersion = 1;
  GlobalSeleccionMinorVersion = 0;

  LIBID_GlobalSeleccion: TGUID = '{09D3B631-B809-11D5-A56C-0050DA04EC66}';

  IID_IdmServerGlobalSeleccion: TGUID = '{09D3B632-B809-11D5-A56C-0050DA04EC66}';
  CLASS_dmServerGlobalSeleccion: TGUID = '{09D3B634-B809-11D5-A56C-0050DA04EC66}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerGlobalSeleccion = interface;
  IdmServerGlobalSeleccionDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerGlobalSeleccion = IdmServerGlobalSeleccion;


// *********************************************************************//
// Interface: IdmServerGlobalSeleccion
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {09D3B632-B809-11D5-A56C-0050DA04EC66}
// *********************************************************************//
  IdmServerGlobalSeleccion = interface(IAppServer)
    ['{09D3B632-B809-11D5-A56C-0050DA04EC66}']
    function  GetGlobales(Empresa: OleVariant): OleVariant; safecall;
    function  GrabaGlobales(Empresa: OleVariant; oDelta: OleVariant; lActualizaDiccion: WordBool; 
                            out ErrorCount: Integer): OleVariant; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerGlobalSeleccionDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {09D3B632-B809-11D5-A56C-0050DA04EC66}
// *********************************************************************//
  IdmServerGlobalSeleccionDisp = dispinterface
    ['{09D3B632-B809-11D5-A56C-0050DA04EC66}']
    function  GetGlobales(Empresa: OleVariant): OleVariant; dispid 1;
    function  GrabaGlobales(Empresa: OleVariant; oDelta: OleVariant; lActualizaDiccion: WordBool; 
                            out ErrorCount: Integer): OleVariant; dispid 2;
    function  AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; 
                              MaxErrors: Integer; out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function  AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                            Options: Integer; const CommandText: WideString; 
                            var Params: OleVariant; var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function  AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function  AS_GetProviderNames: OleVariant; dispid 20000003;
    function  AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function  AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                            var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerGlobalSeleccion provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerGlobalSeleccion exposed by              
// the CoClass dmServerGlobalSeleccion. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerGlobalSeleccion = class
    class function Create: IdmServerGlobalSeleccion;
    class function CreateRemote(const MachineName: string): IdmServerGlobalSeleccion;
  end;

implementation

uses ComObj;

class function CodmServerGlobalSeleccion.Create: IdmServerGlobalSeleccion;
begin
  Result := CreateComObject(CLASS_dmServerGlobalSeleccion) as IdmServerGlobalSeleccion;
end;

class function CodmServerGlobalSeleccion.CreateRemote(const MachineName: string): IdmServerGlobalSeleccion;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerGlobalSeleccion) as IdmServerGlobalSeleccion;
end;

end.
