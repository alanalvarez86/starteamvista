unit DServerGlobal;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.7 / XE3                           ::
  :: Unidad:      DServerGlobal.pas                          ::
  :: Descripci�n: Programa principal de Global.dll           ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ComServ, ComObj, VCLCom,
  StdVcl, DataBkr, DBClient, Db, MtsRdm, Mtx,
  Variants,
  {$IFNDEF DOS_CAPAS}
  Global_TLB,
  {$ENDIF}
  DQueries, DSuperGlobal, DZetaServerProvider;

type
  TdmServerGlobal = class(TMtsDataModule {$IFNDEF DOS_CAPAS}, IdmServerGlobal {$ENDIF} )
    procedure dmServerGlobalCreate(Sender: TObject);
    procedure dmServerGlobalDestroy(Sender: TObject);
  private
    { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
    oSuperGlobal: TdmSuperGlobal;
    FDepuraConteo: Boolean;
    procedure OtrosCambiosGlobal(iCodigo: Integer; sDelta: String);
    procedure CambiaSPCosteo(const iNivel: Integer);
    procedure CambiaSPSupervisores(const iNivel: Integer);
    procedure DepuraTablaConteo;
    // procedure ActualizaDiccionConteo;
    {$IFNDEF DOS_CAPAS}
    procedure ServerActivate(Sender: TObject);
    procedure ServerDeactivate(Sender: TObject);
    {$ENDIF}
  protected
    { Protected declarations }
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    {$IFDEF DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
    {$ENDIF}
    function GetGlobales(Empresa: OleVariant): OleVariant; {$IFNDEF DOS_CAPAS} safecall; {$ENDIF}
    function GrabaGlobales(Empresa, oDelta: OleVariant; lActualizaDiccion: WordBool;
      out ErrorCount: Integer): OleVariant; {$IFNDEF DOS_CAPAS} safecall; {$ENDIF}
    function GetFechaLimite(Empresa: OleVariant): OleVariant; {$IFNDEF DOS_CAPAS} safecall; {$ENDIF}
    function SetFechaLimite(Empresa: OleVariant; Fecha: TDateTime): WordBool;
    {$IFNDEF DOS_CAPAS} safecall; {$ENDIF}
    function GrabaDescripcionesGV(Empresa, oDelta: OleVariant; lActualizaDiccion: WordBool; 
	out ErrorCount: Integer): OleVariant; {$IFNDEF DOS_CAPAS} safecall; {$ENDIF}
    function GetDescripcionesGV(Empresa: OleVariant): OleVariant; {$IFNDEF DOS_CAPAS} safecall; {$ENDIF}
  end;

implementation

{$R *.DFM}

uses
  ZGlobalTress, ZetaTipoEntidad, ZetaServerTools, ZetaCommonLists, ZetaCommonTools,
  ZetaCommonClasses;

type
  eQuerys = (Q_SELECT_FECHA_LIMITE);

function GetSQLScript(const eScript: eQuerys): String;
begin
  case eScript of
    Q_SELECT_FECHA_LIMITE:
      Result := 'select GL_DESCRIP, GL_FORMULA, US_CODIGO, GL_CAPTURA ' +
        'from GLOBAL where ( GL_CODIGO = %d )';
  end;
end;

{ ************ TdmServerGlobal ************ }

class procedure TdmServerGlobal.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end
  else begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

procedure TdmServerGlobal.dmServerGlobalCreate(Sender: TObject);
begin
  {$IFNDEF DOS_CAPAS}
  OnActivate := ServerActivate;
  OnDeactivate := ServerDeactivate;
  {$ENDIF}
  oZetaProvider := DZetaServerProvider.GetZetaProvider(Self);
  oSuperGlobal := TdmSuperGlobal.Create;
  with oSuperGlobal do begin
    Provider := oZetaProvider;
  end;
end;

procedure TdmServerGlobal.dmServerGlobalDestroy(Sender: TObject);
begin
  FreeAndNil(oSuperGlobal);
  DZetaServerProvider.FreeZetaProvider(oZetaProvider);
end;

{$IFDEF DOS_CAPAS}

procedure TdmServerGlobal.CierraEmpresa;
begin
end;
{$ELSE}

procedure TdmServerGlobal.ServerActivate(Sender: TObject);
begin
  oZetaProvider.Activate;
end;

procedure TdmServerGlobal.ServerDeactivate(Sender: TObject);
begin
  oZetaProvider.Deactivate;
end;
{$ENDIF}
{ *********** Procedimientos Privados ************* }

procedure TdmServerGlobal.CambiaSPCosteo(const iNivel: Integer);
{$IFDEF MSSQL}
const
  K_SP_VIEW_COSTEO = 'ALTER VIEW CCOSTO AS  ' + 'SELECT	TB_CODIGO	CC_CODIGO,  ' + CR_LF +
    '		TB_ELEMENT	CC_NOMBRE,  ' + CR_LF + '		TB_INGLES	CC_INGLES,  ' + CR_LF +
    '		TB_NUMERO	CC_NUMERO,  ' + CR_LF + '		TB_TEXTO	CC_TEXTO,   ' + CR_LF +
    '		TB_SUB_CTA	CC_SUB_CTA, ' + CR_LF + '		TB_ACTIVO	CC_ACTIVO,  ' + CR_LF +
    '		LLAVE                       ' + CR_LF + 'FROM	NIVEL%d                     ';

  {$ENDIF}
begin
  {$IFDEF MSSQL}
  if (iNivel > 0) then begin
    with oZetaProvider do begin
      EmpiezaTransaccion;
      try
        ExecSQL(EmpresaActiva, Format(K_SP_VIEW_COSTEO, [iNivel]));
        TerminaTransaccion(TRUE);
      except
        on Error: Exception do begin
          TerminaTransaccion(FALSE);
          raise;
        end;
      end;
    end;
  end;
  {$ENDIF}
end;

procedure TdmServerGlobal.CambiaSPSupervisores(const iNivel: Integer);
const
  {$IFDEF INTERBASE}
  K_SP_SUPERVISORES = 'ALTER PROCEDURE SP_FECHA_NIVEL ( ' + '  FECHA DATE, ' + '  EMPLEADO INTEGER '
    + ') RETURNS ( ' + '  RESULTADO CHAR(6) ' + ') AS ' + CR_LF + 'BEGIN ' + CR_LF + '    FOR ' +
    CR_LF + '    select CB_NIVEL%d from KARDEX ' + CR_LF +
    '    where  ( CB_CODIGO = :Empleado ) and ' + CR_LF + '           ( CB_FECHA <= :Fecha ) ' +
    CR_LF + '    order by CB_FECHA desc, CB_NIVEL desc ' + CR_LF + '    into RESULTADO ' + CR_LF +
    '    DO ' + CR_LF + '      BEGIN ' + CR_LF + '           SUSPEND; ' + CR_LF +
    '           EXIT; ' + CR_LF + '      END ' + CR_LF + 'END';
  {$ENDIF}
  {$IFDEF MSSQL}
  K_SP_SUPERVISORES = 'ALTER  FUNCTION SP_FECHA_NIVEL (' + CR_LF + '  @FECHA DATETIME,' + CR_LF +
    '  @EMPLEADO INTEGER' + CR_LF + ') RETURNS CHAR(6)' + CR_LF + 'AS' + CR_LF + 'BEGIN' + CR_LF +
    '	DECLARE @Resultado CHAR(6)' + CR_LF + '	DECLARE Temporal CURSOR FOR' + CR_LF +
    '	select CB_NIVEL%d from KARDEX' + CR_LF + '    	where  ( CB_CODIGO = @Empleado ) and' + CR_LF
    + '		( CB_FECHA <= @Fecha )' + CR_LF + '	order by CB_FECHA DESC, CB_NIVEL DESC' + CR_LF +
    '	open Temporal' + CR_LF + '	fetch NEXT FROM Temporal' + CR_LF + '	INTO @Resultado' + CR_LF +
    '	close Temporal' + CR_LF + '	deallocate Temporal' + CR_LF + '	RETURN @Resultado' +
    CR_LF + 'END';
  {$ENDIF}
begin
  if (iNivel > 0) then begin
    with oZetaProvider do begin
      EmpiezaTransaccion;
      try
        ExecSQL(EmpresaActiva, Format(K_SP_SUPERVISORES, [iNivel]));
        TerminaTransaccion(TRUE);
      except
        on Error: Exception do begin
          TerminaTransaccion(FALSE);
          raise;
        end;
      end;
    end;
  end;
end;

procedure TdmServerGlobal.DepuraTablaConteo;
const
  K_DELETE_CONTEO = 'delete from CONTEO';
begin
  with oZetaProvider do begin
    EmpiezaTransaccion;
    try
      ExecSQL(EmpresaActiva, K_DELETE_CONTEO);
      TerminaTransaccion(TRUE);
    except
      on Error: Exception do begin
        TerminaTransaccion(FALSE);
        raise;
      end;
    end;
  end;
end;

{ *********** Interfases ( Paletitas ) *********** }

function TdmServerGlobal.GetGlobales(Empresa: OleVariant): OleVariant;
begin
  try
    oZetaProvider.EmpresaActiva := Empresa;
    with oSuperGlobal do begin
      Result := ProcesaLectura;
    end;
    SetComplete;
  except
    SetAbort;
    raise;
  end;
end;

function TdmServerGlobal.GetDescripcionesGV(Empresa: OleVariant): OleVariant;
begin
  try
    oZetaProvider.EmpresaActiva := Empresa;
    with oSuperGlobal do begin
      Result := ProcesaLecturaGV
    end;
    SetComplete;
  except
    SetAbort;
    raise;
  end;
end;

procedure TdmServerGlobal.OtrosCambiosGlobal(iCodigo: Integer; sDelta: String);
begin
  if (iCodigo = K_GLOBAL_NIVEL_SUPERVISOR) then
    CambiaSPSupervisores(StrToIntDef(sDelta, 0))
  else if (iCodigo = K_GLOBAL_NIVEL_COSTEO) then
    CambiaSPCosteo(StrToIntDef(sDelta, 0))
  else if (iCodigo in [K_GLOBAL_CONTEO_NIVEL1 .. K_GLOBAL_CONTEO_NIVEL5]) then
    FDepuraConteo := TRUE;
end;

function TdmServerGlobal.GrabaGlobales(Empresa, oDelta: OleVariant; lActualizaDiccion: WordBool; out ErrorCount: Integer): OleVariant;
begin
  try
    oZetaProvider.EmpresaActiva := Empresa;
    FDepuraConteo := FALSE;
    with oSuperGlobal do begin
      OnGrabarGlobal := OtrosCambiosGlobal;
      ActualizaDiccion := lActualizaDiccion;
      Result := ProcesaEscritura(oDelta);
      ErrorCount := Errores;
    end;
    if FDepuraConteo then begin
      DepuraTablaConteo;
    end;
    SetComplete;
  except
    on e: Exception do begin
      SetAbort;
      raise;
    end;
  end;
end;

function TdmServerGlobal.GrabaDescripcionesGV(Empresa, oDelta: OleVariant; lActualizaDiccion: WordBool; out ErrorCount: Integer): OleVariant;
begin
  try
    oZetaProvider.EmpresaActiva := Empresa;
    FDepuraConteo := FALSE;
    with oSuperGlobal do begin
      ActualizaDiccion := lActualizaDiccion;
      Result := ProcesaEscrituraDescripcionesGV(oDelta);
      ErrorCount := Errores;
    end;
    if FDepuraConteo then begin
      DepuraTablaConteo;
    end;
    SetComplete;
  except
    on e: Exception do begin
      SetAbort;
      raise;
    end;
  end;
end;

function TdmServerGlobal.GetFechaLimite(Empresa: OleVariant): OleVariant;
var
  FDataset: TZetaCursor;
begin
  Result := VarArrayCreate([K_FECHA_LIMITE_DESCRIPCION, K_FECHA_LIMITE_CAPTURA], varVariant);
  with oZetaProvider do begin
    EmpresaActiva := Empresa;
    FDataset := CreateQuery(Format(GetSQLScript(Q_SELECT_FECHA_LIMITE), [ZGlobalTress.K_GLOBAL_FECHA_LIMITE]));
    try
      with FDataset do begin
        Active := TRUE;
        if EOF then begin
          Result[K_FECHA_LIMITE_DESCRIPCION] := VACIO;
          Result[K_FECHA_LIMITE_FORMULA] := VACIO;
          Result[K_FECHA_LIMITE_USUARIO] := 0;
          Result[K_FECHA_LIMITE_CAPTURA] := NullDateTime;
        end else begin
          Result[K_FECHA_LIMITE_DESCRIPCION] := FieldByName('GL_DESCRIP').AsString;
          Result[K_FECHA_LIMITE_FORMULA] := FieldByName('GL_FORMULA').AsString;
          Result[K_FECHA_LIMITE_USUARIO] := FieldByName('US_CODIGO').AsInteger;
          Result[K_FECHA_LIMITE_CAPTURA] := FieldByName('GL_CAPTURA').AsDateTime;
        end;
        Active := FALSE;
      end;
    finally
      FreeAndNil(FDataset);
    end;
  end;
  SetComplete;
end;

function TdmServerGlobal.SetFechaLimite(Empresa: OleVariant; Fecha: TDateTime): WordBool;
const
  K_DESCRIPCION_MANUAL = 'Ajustada Manualmente';
begin
  oZetaProvider.EmpresaActiva := Empresa;
  Result := DQueries.EscribeFechaLimite(K_DESCRIPCION_MANUAL, Fecha, Date, oZetaProvider.UsuarioActivo, oZetaProvider);
  SetComplete;
end;

{$IFNDEF DOS_CAPAS}
initialization
  TComponentFactory.Create(ComServer, TdmServerGlobal, Class_dmServerGlobal, ciMultiInstance, ZetaServerTools.GetThreadingModel);
{$ENDIF}

end.
