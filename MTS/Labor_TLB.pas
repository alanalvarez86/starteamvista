unit Labor_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 4/6/2010 4:01:19 PM from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3win_20_Emerson\MTS\Labor.tlb (1)
// LIBID: {27817FA0-1C90-11D4-8C26-0050DA04EAC5}
// LCID: 0
// Helpfile: 
// HelpString: Project2 Library
// DepndLst: 
//   (1) v1.0 Midas, (C:\WINDOWS\system32\midas.dll)
//   (2) v2.0 stdole, (C:\WINDOWS\System32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, Midas, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  LaborMajorVersion = 1;
  LaborMinorVersion = 0;

  LIBID_Labor: TGUID = '{27817FA0-1C90-11D4-8C26-0050DA04EAC5}';

  IID_IdmServerLabor: TGUID = '{27817FA1-1C90-11D4-8C26-0050DA04EAC5}';
  CLASS_dmServerLabor: TGUID = '{27817FA3-1C90-11D4-8C26-0050DA04EAC5}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerLabor = interface;
  IdmServerLaborDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerLabor = IdmServerLabor;


// *********************************************************************//
// Interface: IdmServerLabor
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {27817FA1-1C90-11D4-8C26-0050DA04EAC5}
// *********************************************************************//
  IdmServerLabor = interface(IAppServer)
    ['{27817FA1-1C90-11D4-8C26-0050DA04EAC5}']
    function GetCatalogo(Empresa: OleVariant; Catalogo: Integer): OleVariant; safecall;
    function GrabaCatalogo(Empresa: OleVariant; Catalogo: Integer; oDelta: OleVariant; 
                           out ErrorCount: Integer): OleVariant; safecall;
    function GetOrdenesFijas(Empresa: OleVariant; Empleado: Integer): OleVariant; safecall;
    function GetWorksEmpleado(Empresa: OleVariant; var Ausencia: OleVariant; Empleado: Integer; 
                              Fecha: TDateTime): OleVariant; safecall;
    function GetLecturasEmpleado(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime): OleVariant; safecall;
    function GetCatalogoDetail(Empresa: OleVariant; Catalogo: Integer): OleVariant; safecall;
    function GrabaCatalogoDetail(Empresa: OleVariant; Catalogo: Integer; oDelta: OleVariant; 
                                 out ErrorCount: Integer): OleVariant; safecall;
    function GetDatosEmpleado(Empresa: OleVariant; Empleado: Integer): OleVariant; safecall;
    function GetLecturas(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime): OleVariant; safecall;
    function GetWOrder(Empresa: OleVariant; Parametros: OleVariant; Tipo: Integer): OleVariant; safecall;
    function GetWOrderSteps(Empresa: OleVariant; var Steps: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetKardexWorder(Empresa: OleVariant; var WorksHijo: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GrabaWorderSteps(Empresa: OleVariant; Delta: OleVariant; Steps: OleVariant; 
                              const WOrderNumber: WideString; out ErrorCount: Integer): OleVariant; safecall;
    function BorraWOrder(Empresa: OleVariant; const WOrderNumber: WideString; 
                         out ErrorCount: Integer): OleVariant; safecall;
    function GrabaPartes(Empresa: OleVariant; Delta: OleVariant; DefSteps: OleVariant; 
                         const Parte: WideString; out ErrorCount: Integer): OleVariant; safecall;
    function BorraParte(Empresa: OleVariant; const Parte: WideString; out ErrorCount: Integer): OleVariant; safecall;
    function GetPartesSteps(Empresa: OleVariant; var DefSteps: OleVariant; const Parte: WideString): OleVariant; safecall;
    function GrabaGridLabor(Empresa: OleVariant; const Area: WideString; oDelta: OleVariant; 
                            out ErrorCount: Integer): OleVariant; safecall;
    function GrabaSalidaWorks(Empresa: OleVariant; OtrosDatos: OleVariant; oDelta: OleVariant; 
                              out ErrorCount: Integer): OleVariant; safecall;
    function Depuracion(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GrabaWorks(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetCedulas(Empresa: OleVariant; Fecha: TDateTime): OleVariant; safecall;
    function GetCedEmpleados(Empresa: OleVariant; Folio: Integer): OleVariant; safecall;
    function GetCedMultiLote(Empresa: OleVariant; Folio: Integer): OleVariant; safecall;
    function GrabaCedula(Empresa: OleVariant; oDelta: OleVariant; oDataEmp: OleVariant; 
                         oDataMulti: OleVariant; Parametros: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function BorraCedula(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetHistorialCedulas(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetBreaks(Empresa: OleVariant): OleVariant; safecall;
    function GrabaBreaks(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function ImportarOrdenes(Empresa: OleVariant; Parametros: OleVariant; ListaASCII: OleVariant): OleVariant; safecall;
    function ImportarPartes(Empresa: OleVariant; Parametros: OleVariant; ListaASCII: OleVariant): OleVariant; safecall;
    function GetTiempos(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime): OleVariant; safecall;
    procedure GrabaTiempos(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime; 
                           Datos: OleVariant); safecall;
    function GrabaAsignaAreas(Empresa: OleVariant; oDelta: OleVariant; Parametros: OleVariant; 
                              out ErrorCount: Integer): OleVariant; safecall;
    function GetSupArea(Empresa: OleVariant; const Supervisor: WideString): OleVariant; safecall;
    function GrabaSupArea(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer; 
                          const Supervisor: WideString): OleVariant; safecall;
    function GetKardexArea(Empresa: OleVariant; Empleado: Integer): OleVariant; safecall;
    function GrabaKardexArea(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetAreas(Empresa: OleVariant): OleVariant; safecall;
    function GetEmpAreas(Empresa: OleVariant; Fecha: TDateTime; const Hora: WideString): OleVariant; safecall;
    function GetAreaKardex(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime; 
                           const Hora: WideString): WideString; safecall;
    function GetEmpBreaks(Empresa: OleVariant; const Area: WideString; Fecha: TDateTime; 
                          const Hora: WideString; lSoloAltas: WordBool): OleVariant; safecall;
    function GrabaEmpBreaks(Empresa: OleVariant; oDelta: OleVariant; Fecha: TDateTime; 
                            const Hora: WideString; Duracion: Integer; out ErrorCount: Integer): OleVariant; safecall;
    function CancelarBreaksGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CancelarBreaksLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CancelarBreaks(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetHistorialCedInsp(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetCedInspeccion(Empresa: OleVariant; var Defectos: OleVariant; Cedula: Integer): OleVariant; safecall;
    function GrabaCedInspeccion(Empresa: OleVariant; Delta: OleVariant; Defectos: OleVariant; 
                                Cedula: Integer; out ErrorCount: Integer): OleVariant; safecall;
    function BorraCedulasInsp(Empresa: OleVariant; Cedula: Integer; out ErrorCount: Integer): OleVariant; safecall;
    function BorraCedulaScrap(Empresa: OleVariant; Cedula: Integer; out ErrorCount: Integer): OleVariant; safecall;
    function GetHistorialCedScrap(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetCedulaScrap(Empresa: OleVariant; var Scrap: OleVariant; Cedula: Integer): OleVariant; safecall;
    function GrabaCedulaScrap(Empresa: OleVariant; Delta: OleVariant; Scrap: OleVariant; 
                              Cedula: Integer; out ErrorCount: Integer): OleVariant; safecall;
    function GetComponentes(Empresa: OleVariant): OleVariant; safecall;
    function GrabaComponente(Empresa: OleVariant; Delta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function ImportarComponentes(Empresa: OleVariant; Parametros: OleVariant; ListaASCII: OleVariant): OleVariant; safecall;
    function GetComponente(Empresa: OleVariant; const Componente: WideString): OleVariant; safecall;
    function ImportarCedulas(Empresa: OleVariant; Parametros: OleVariant; ListaASCII: OleVariant): OleVariant; safecall;
    function GetKardexDiario(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime): OleVariant; safecall;
    function GetCertificaciones(Empresa: OleVariant; const Maquina: WideString): OleVariant; safecall;
    function GrabaCertificaciones(Empresa: OleVariant; Delta: OleVariant; 
                                  const Maquina: WideString; out ErrorCount: Integer): OleVariant; safecall;
    function GetMapa(Empresa: OleVariant; const Layout: WideString; out oSillas: OleVariant; 
                     out oMaqSilla: OleVariant; out oCertMaq: OleVariant): OleVariant; safecall;
    function GetLayoutsSuper(Empresa: OleVariant; Usuario: Integer): OleVariant; safecall;
    function AsignaEmpleado(Empresa: OleVariant; oZetaParams: OleVariant): OleVariant; safecall;
    function GetMaquinaEmpleados(Empresa: OleVariant; const Layout: WideString; 
                                 const Hora: WideString; Fecha: TDateTime): OleVariant; safecall;
    function GetCertificacionesEmpleados(Empresa: OleVariant; Usuario: Integer; Fecha: TDateTime): OleVariant; safecall;
    function GetKardexEmpleadoMaquina(Empresa: OleVariant; Empleado: Integer): OleVariant; safecall;
    function GetKardexMaquinaEmpleado(Empresa: OleVariant; const Maquina: WideString): OleVariant; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerLaborDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {27817FA1-1C90-11D4-8C26-0050DA04EAC5}
// *********************************************************************//
  IdmServerLaborDisp = dispinterface
    ['{27817FA1-1C90-11D4-8C26-0050DA04EAC5}']
    function GetCatalogo(Empresa: OleVariant; Catalogo: Integer): OleVariant; dispid 1;
    function GrabaCatalogo(Empresa: OleVariant; Catalogo: Integer; oDelta: OleVariant; 
                           out ErrorCount: Integer): OleVariant; dispid 2;
    function GetOrdenesFijas(Empresa: OleVariant; Empleado: Integer): OleVariant; dispid 3;
    function GetWorksEmpleado(Empresa: OleVariant; var Ausencia: OleVariant; Empleado: Integer; 
                              Fecha: TDateTime): OleVariant; dispid 4;
    function GetLecturasEmpleado(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime): OleVariant; dispid 5;
    function GetCatalogoDetail(Empresa: OleVariant; Catalogo: Integer): OleVariant; dispid 6;
    function GrabaCatalogoDetail(Empresa: OleVariant; Catalogo: Integer; oDelta: OleVariant; 
                                 out ErrorCount: Integer): OleVariant; dispid 8;
    function GetDatosEmpleado(Empresa: OleVariant; Empleado: Integer): OleVariant; dispid 10;
    function GetLecturas(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime): OleVariant; dispid 11;
    function GetWOrder(Empresa: OleVariant; Parametros: OleVariant; Tipo: Integer): OleVariant; dispid 7;
    function GetWOrderSteps(Empresa: OleVariant; var Steps: OleVariant; Parametros: OleVariant): OleVariant; dispid 9;
    function GetKardexWorder(Empresa: OleVariant; var WorksHijo: OleVariant; Parametros: OleVariant): OleVariant; dispid 12;
    function GrabaWorderSteps(Empresa: OleVariant; Delta: OleVariant; Steps: OleVariant; 
                              const WOrderNumber: WideString; out ErrorCount: Integer): OleVariant; dispid 14;
    function BorraWOrder(Empresa: OleVariant; const WOrderNumber: WideString; 
                         out ErrorCount: Integer): OleVariant; dispid 13;
    function GrabaPartes(Empresa: OleVariant; Delta: OleVariant; DefSteps: OleVariant; 
                         const Parte: WideString; out ErrorCount: Integer): OleVariant; dispid 15;
    function BorraParte(Empresa: OleVariant; const Parte: WideString; out ErrorCount: Integer): OleVariant; dispid 16;
    function GetPartesSteps(Empresa: OleVariant; var DefSteps: OleVariant; const Parte: WideString): OleVariant; dispid 17;
    function GrabaGridLabor(Empresa: OleVariant; const Area: WideString; oDelta: OleVariant; 
                            out ErrorCount: Integer): OleVariant; dispid 18;
    function GrabaSalidaWorks(Empresa: OleVariant; OtrosDatos: OleVariant; oDelta: OleVariant; 
                              out ErrorCount: Integer): OleVariant; dispid 19;
    function Depuracion(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 21;
    function GrabaWorks(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 23;
    function GetCedulas(Empresa: OleVariant; Fecha: TDateTime): OleVariant; dispid 24;
    function GetCedEmpleados(Empresa: OleVariant; Folio: Integer): OleVariant; dispid 25;
    function GetCedMultiLote(Empresa: OleVariant; Folio: Integer): OleVariant; dispid 27;
    function GrabaCedula(Empresa: OleVariant; oDelta: OleVariant; oDataEmp: OleVariant; 
                         oDataMulti: OleVariant; Parametros: OleVariant; out ErrorCount: Integer): OleVariant; dispid 26;
    function BorraCedula(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 28;
    function GetHistorialCedulas(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 29;
    function GetBreaks(Empresa: OleVariant): OleVariant; dispid 30;
    function GrabaBreaks(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 31;
    function ImportarOrdenes(Empresa: OleVariant; Parametros: OleVariant; ListaASCII: OleVariant): OleVariant; dispid 32;
    function ImportarPartes(Empresa: OleVariant; Parametros: OleVariant; ListaASCII: OleVariant): OleVariant; dispid 33;
    function GetTiempos(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime): OleVariant; dispid 34;
    procedure GrabaTiempos(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime; 
                           Datos: OleVariant); dispid 35;
    function GrabaAsignaAreas(Empresa: OleVariant; oDelta: OleVariant; Parametros: OleVariant; 
                              out ErrorCount: Integer): OleVariant; dispid 36;
    function GetSupArea(Empresa: OleVariant; const Supervisor: WideString): OleVariant; dispid 37;
    function GrabaSupArea(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer; 
                          const Supervisor: WideString): OleVariant; dispid 38;
    function GetKardexArea(Empresa: OleVariant; Empleado: Integer): OleVariant; dispid 39;
    function GrabaKardexArea(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 40;
    function GetAreas(Empresa: OleVariant): OleVariant; dispid 41;
    function GetEmpAreas(Empresa: OleVariant; Fecha: TDateTime; const Hora: WideString): OleVariant; dispid 42;
    function GetAreaKardex(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime; 
                           const Hora: WideString): WideString; dispid 43;
    function GetEmpBreaks(Empresa: OleVariant; const Area: WideString; Fecha: TDateTime; 
                          const Hora: WideString; lSoloAltas: WordBool): OleVariant; dispid 44;
    function GrabaEmpBreaks(Empresa: OleVariant; oDelta: OleVariant; Fecha: TDateTime; 
                            const Hora: WideString; Duracion: Integer; out ErrorCount: Integer): OleVariant; dispid 45;
    function CancelarBreaksGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 46;
    function CancelarBreaksLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 47;
    function CancelarBreaks(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 48;
    function GetHistorialCedInsp(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 50;
    function GetCedInspeccion(Empresa: OleVariant; var Defectos: OleVariant; Cedula: Integer): OleVariant; dispid 20;
    function GrabaCedInspeccion(Empresa: OleVariant; Delta: OleVariant; Defectos: OleVariant; 
                                Cedula: Integer; out ErrorCount: Integer): OleVariant; dispid 22;
    function BorraCedulasInsp(Empresa: OleVariant; Cedula: Integer; out ErrorCount: Integer): OleVariant; dispid 49;
    function BorraCedulaScrap(Empresa: OleVariant; Cedula: Integer; out ErrorCount: Integer): OleVariant; dispid 52;
    function GetHistorialCedScrap(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 53;
    function GetCedulaScrap(Empresa: OleVariant; var Scrap: OleVariant; Cedula: Integer): OleVariant; dispid 54;
    function GrabaCedulaScrap(Empresa: OleVariant; Delta: OleVariant; Scrap: OleVariant; 
                              Cedula: Integer; out ErrorCount: Integer): OleVariant; dispid 55;
    function GetComponentes(Empresa: OleVariant): OleVariant; dispid 51;
    function GrabaComponente(Empresa: OleVariant; Delta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 56;
    function ImportarComponentes(Empresa: OleVariant; Parametros: OleVariant; ListaASCII: OleVariant): OleVariant; dispid 57;
    function GetComponente(Empresa: OleVariant; const Componente: WideString): OleVariant; dispid 58;
    function ImportarCedulas(Empresa: OleVariant; Parametros: OleVariant; ListaASCII: OleVariant): OleVariant; dispid 59;
    function GetKardexDiario(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime): OleVariant; dispid 60;
    function GetCertificaciones(Empresa: OleVariant; const Maquina: WideString): OleVariant; dispid 301;
    function GrabaCertificaciones(Empresa: OleVariant; Delta: OleVariant; 
                                  const Maquina: WideString; out ErrorCount: Integer): OleVariant; dispid 302;
    function GetMapa(Empresa: OleVariant; const Layout: WideString; out oSillas: OleVariant; 
                     out oMaqSilla: OleVariant; out oCertMaq: OleVariant): OleVariant; dispid 304;
    function GetLayoutsSuper(Empresa: OleVariant; Usuario: Integer): OleVariant; dispid 305;
    function AsignaEmpleado(Empresa: OleVariant; oZetaParams: OleVariant): OleVariant; dispid 303;
    function GetMaquinaEmpleados(Empresa: OleVariant; const Layout: WideString; 
                                 const Hora: WideString; Fecha: TDateTime): OleVariant; dispid 306;
    function GetCertificacionesEmpleados(Empresa: OleVariant; Usuario: Integer; Fecha: TDateTime): OleVariant; dispid 307;
    function GetKardexEmpleadoMaquina(Empresa: OleVariant; Empleado: Integer): OleVariant; dispid 308;
    function GetKardexMaquinaEmpleado(Empresa: OleVariant; const Maquina: WideString): OleVariant; dispid 309;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer; 
                             out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                           Options: Integer; const CommandText: WideString; var Params: OleVariant; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerLabor provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerLabor exposed by              
// the CoClass dmServerLabor. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerLabor = class
    class function Create: IdmServerLabor;
    class function CreateRemote(const MachineName: string): IdmServerLabor;
  end;

implementation

uses ComObj;

class function CodmServerLabor.Create: IdmServerLabor;
begin
  Result := CreateComObject(CLASS_dmServerLabor) as IdmServerLabor;
end;

class function CodmServerLabor.CreateRemote(const MachineName: string): IdmServerLabor;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerLabor) as IdmServerLabor;
end;

end.
