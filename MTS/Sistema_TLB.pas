unit Sistema_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 11/2/2018 3:24:11 PM from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3win_30_Vsn_2018\MTS\Sistema.tlb (1)
// LIBID: {9F7939B1-88BA-11D3-8DEB-0050DA04EAA0}
// LCID: 0
// Helpfile: 
// HelpString: Tress Sistema
// DepndLst: 
//   (1) v1.0 Midas, (C:\Program Files (x86)\Embarcadero\RAD Studio\12.0\bin64\midas.dll)
//   (2) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, Midas, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  SistemaMajorVersion = 1;
  SistemaMinorVersion = 0;

  LIBID_Sistema: TGUID = '{9F7939B1-88BA-11D3-8DEB-0050DA04EAA0}';

  IID_IdmServerSistema: TGUID = '{9F7939B2-88BA-11D3-8DEB-0050DA04EAA0}';
  CLASS_dmServerSistema: TGUID = '{9F7939B4-88BA-11D3-8DEB-0050DA04EAA0}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerSistema = interface;
  IdmServerSistemaDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerSistema = IdmServerSistema;


// *********************************************************************//
// Interface: IdmServerSistema
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9F7939B2-88BA-11D3-8DEB-0050DA04EAA0}
// *********************************************************************//
  IdmServerSistema = interface(IAppServer)
    ['{9F7939B2-88BA-11D3-8DEB-0050DA04EAA0}']
    function GetEmpresas(Tipo: Integer; GrupoActivo: Integer): OleVariant; safecall;
    function GrabaEmpresas(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetGrupos(Grupo: Integer): OleVariant; safecall;
    function GrabaGrupos(oDelta: OleVariant; iUsuario: Integer; out ErrorCount: Integer): OleVariant; safecall;
    function GetImpresoras: OleVariant; safecall;
    function GrabaImpresoras(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetUsuarios(out Longitud: Integer; Grupo: Integer; out BlockSistema: WordBool): OleVariant; safecall;
    function GrabaUsuarios(oDelta: OleVariant; iUsuario: Integer; out ErrorCount: Integer): OleVariant; safecall;
    function GetUsuarioSupervisores(Empresa: OleVariant; iUsuario: Integer): OleVariant; safecall;
    function GrabaUsuariosSupervisores(Empresa: OleVariant; oDelta: OleVariant; 
                                       out ErrorCount: Integer; iUsuario: Integer; 
                                       const sTexto: WideString): OleVariant; safecall;
    function GetAccesos(iGrupo: Integer; const sCompany: WideString): OleVariant; safecall;
    function GrabaAccesos(oDelta: OleVariant; oParams: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetUsuarioArbol(iUsuario: Integer): OleVariant; safecall;
    function GrabaArbol(iUsuario: Integer; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetPoll: OleVariant; safecall;
    function GrabaPoll(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetPollVacio: OleVariant; safecall;
    function SuspendeUsuarios(Usuario: Integer; Grupo: Integer): OleVariant; safecall;
    function ActivaUsuarios(Usuario: Integer; Grupo: Integer): OleVariant; safecall;
    function BorrarBitacora(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetNivel0: OleVariant; safecall;
    function GrabaNivel0(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetUsuarioSuscrip(Empresa: OleVariant; Usuario: Integer): OleVariant; safecall;
    function GrabaUsuarioSuscrip(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer; 
                                 Usuario: Integer): OleVariant; safecall;
    function GetGruposAccesos(iGrupo: Integer; const sCompany: WideString; iTipo: Integer): OleVariant; safecall;
    function GetUsuarioAreas(Empresa: OleVariant; Usuario: Integer): OleVariant; safecall;
    function GrabaUsuarioAreas(Empresa: OleVariant; Usuario: Integer; oDelta: OleVariant; 
                               out ErrorCount: Integer; const sTexto: WideString): OleVariant; safecall;
    function GetNuevoUsuario: Integer; safecall;
    function GetEmpresasAccesos(Tipo: Integer; Grupo: Integer): OleVariant; safecall;
    function ActualizaNumeroTarjeta(out Bitacora: OleVariant): OleVariant; safecall;
    function GetGruposAdic(Empresa: OleVariant; out oCamposAdic: OleVariant): OleVariant; safecall;
    function GrabaGruposAdic(Empresa: OleVariant; oDelta: OleVariant; oDeltaCampos: OleVariant; 
                             out ErrorCount: Integer; out ErrorCampos: Integer; 
                             out CamposResult: OleVariant): OleVariant; safecall;
    function GetBitacora(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetReporteSuscrip(Empresa: OleVariant; iReporte: Integer): OleVariant; safecall;
    function GetRoles: OleVariant; safecall;
    function GetDatosEmpleadoUsuario(Empresa: OleVariant; Datos: OleVariant): OleVariant; safecall;
    function GrabaRoles(const Rol: WideString; Delta: OleVariant; Usuarios: OleVariant; 
                        Modelos: OleVariant; var ErrorCount: Integer): OleVariant; safecall;
    function GetUsuariosRol(const Rol: WideString; out Modelos: OleVariant): OleVariant; safecall;
    function GetUserRoles(Empresa: OleVariant; iUsuario: Integer): OleVariant; safecall;
    function GrabaUsuarioRoles(iUsuario: Integer; iUsuarioLog: Integer; Delta: OleVariant; 
                               var ErrorCount: Integer): OleVariant; safecall;
    function GrabaEmpleadoUsuario(oEmpresa: OleVariant; iUsuario: Integer; iEmpleado: Integer): OleVariant; safecall;
    function EnrolamientoMasivo(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function EnrolamientoMasivoGetLista(oEmpresa: OleVariant; oParametros: OleVariant): OleVariant; safecall;
    function ImportarEnrolamientoMasivoGetASCII(oEmpresa: OleVariant; Parametros: OleVariant; 
                                                ListaASCII: OleVariant; out ErrorCount: Integer; 
                                                LoginAD: WordBool): OleVariant; safecall;
    function ImportarEnrolamientoMasivo(Empresa: OleVariant; Parametros: OleVariant; 
                                        Datos: OleVariant): OleVariant; safecall;
    function GetClasifiEmpresa(Empresa: OleVariant): OleVariant; safecall;
    function GetDerechosAdicionales(Empresa: OleVariant; Grupo: Integer): OleVariant; safecall;
    function GrabaAccesosAdicionales(Empresa: OleVariant; Grupo: Integer; Delta: OleVariant; 
                                     out ErrorCount: Integer): OleVariant; safecall;
    function GetSistLstDispositivos(Empresa: OleVariant): OleVariant; safecall;
    function GrabaSistLstDispositivos(Empresa: OleVariant; oDelta: OleVariant; 
                                      out ErrorCount: Integer): OleVariant; safecall;
    function GetGlobalLstDispositivos(Empresa: OleVariant; var Disponibles: OleVariant; 
                                      var Asignados: OleVariant): OleVariant; safecall;
    function GrabaGlobalLstDispositivos(Empresa: OleVariant; oDelta: OleVariant; 
                                        out ErrorCount: Integer): OleVariant; safecall;
    function GetAccArbol(Empresa: OleVariant): OleVariant; safecall;
    procedure ActualizaUsuariosEmpresa(Empresa: OleVariant; Delta: OleVariant); safecall;
    function ConsultaBitacoraBiometrica(Parametros: OleVariant): OleVariant; safecall;
    function DepuraBitacoraBiometrica(Parametros: OleVariant): OleVariant; safecall;
    function ObtenIdBioMaximo: Integer; safecall;
    function ActualizaIdBio(const CodigoEmpresaLocal: WideString; Empleado: Integer; Id: Integer; 
                            const Grupo: WideString; Invitador: Integer; 
                            const Confidencialidad: WideString): WordBool; safecall;
    function GetUsuarioCCosto(Empresa: OleVariant; iUsuario: Integer): OleVariant; safecall;
    function GrabaUsuarioCCosto(Empresa: OleVariant; oDelta: OleVariant; ErrorCount: Integer; 
                                iUsuario: Integer; const sTexto: WideString; 
                                const sNombreCosteo: WideString): OleVariant; safecall;
    function ConsultaListaGrupos(Empresa: OleVariant): OleVariant; safecall;
    function GrabaSistListaGrupos(oDelta: OleVariant; UsuarioTerminal: Integer; 
                                  out ErrorCount: Integer): OleVariant; safecall;
    function ConsultaTermPorGrupo(const Grupo: WideString): OleVariant; safecall;
    function GrabaTermPorGrupo(oDelta: OleVariant; UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant; safecall;
    procedure GrabaListaTermPorGrupo(const Terminales: WideString; UsuarioTerminal: Integer; 
                                     const Grupo: WideString; const SinTerminales: WideString); safecall;
    function AsignaNumBiometricos(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    procedure ReiniciaSincTerminales(const Lista: WideString); safecall;
    function AsignaGrupoTerminales(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function EmpleadosBiometricosGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function EmpleadosSinBiometricosGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function AsignaGrupoTerminalesLista(Empresa: OleVariant; Lista: OleVariant; 
                                        Parametros: OleVariant): OleVariant; safecall;
    function AsignaNumBiometricosLista(Empresa: OleVariant; Lista: OleVariant; 
                                       Parametros: OleVariant): OleVariant; safecall;
    procedure ActualizaTerminalesGTI(const Lista: WideString); safecall;
    function ConsultaMensaje(const Filtro: WideString): OleVariant; safecall;
    function GrabaMensaje(oDelta: OleVariant; UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant; safecall;
    function ConsultaMensajeTerminal(const Filtro: WideString): OleVariant; safecall;
    function GrabaMensajeTerminal(oDelta: OleVariant; UsuarioTerminal: Integer; 
                                  out ErrorCount: Integer): OleVariant; safecall;
    function ConsultaTerminal(const Filtro: WideString): OleVariant; safecall;
    function GrabaTerminal(oDelta: OleVariant; UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant; safecall;
    function ImportaTerminales(Parametros: OleVariant; UsuarioTerminal: Integer; 
                               out Bitacora: OleVariant): OleVariant; safecall;
    function ExisteHuellaRegistrada(Empleado: Integer; const Empresa: WideString): WordBool; safecall;
    procedure ReiniciaTerminal(Terminal: Integer; const Empresa: WideString); safecall;
    function ExisteEmpleadoEnGrupo(const Grupo: WideString): WordBool; safecall;
    function ObtieneTemplates(Parametros: OleVariant): OleVariant; safecall;
    procedure InsertaHuella(Parametros: OleVariant); safecall;
    procedure EliminaHuellas(const Empresa: WideString; Empleado: Integer; Invitador: Integer; 
                             Usuario: Integer; Tipo: Integer); safecall;
    function ReportaHuella(Parametros: OleVariant): OleVariant; safecall;
    function ExisteHuellaInvitador(const Empresa: WideString; Invitador: Integer): WordBool; safecall;
    function ConsultaConfBD(Tipo: Integer): OleVariant; safecall;
    function GrabaBD(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetEmpresasSistema(GrupoActivo: Integer): OleVariant; safecall;
    function GetBasesDatos(const CM_DATOS: WideString; const CM_USRNAME: WideString; 
                           const CM_PASSWRD: WideString): OleVariant; safecall;
    function GetServer: WideString; safecall;
    function GetSQLUserName: WideString; safecall;
    function EsBDEmpleados(const BaseDatos: WideString): WordBool; safecall;
    function EsBDAgregada(const Ubicacion: WideString): WordBool; safecall;
    function EmpGrabarDBSistema(Parametros: OleVariant): WordBool; safecall;
    function GetLogicalName(const Conexion: WideString; const Nombre: WideString; Tipo: Integer): WideString; safecall;
    function BackupDB(const Conexion: WideString; const Origen: WideString; 
                      const Destino: WideString; const Ruta: WideString): OleVariant; safecall;
    function RestoreDB(const Conexion: WideString; const Origen: WideString; 
                       const Destino: WideString; const DataName: WideString; 
                       const PathDataName: WideString; const LogName: WideString; 
                       const PathLogName: WideString): OleVariant; safecall;
    function DepurarTabla(const Conexion: WideString; const Tabla: WideString; 
                          const Database: WideString): OleVariant; safecall;
    function ConectarseServidor(const Conexion: WideString; out Mensaje: WideString): WordBool; safecall;
    function GetDBSize(const Conn: WideString; const Database: WideString; bUsarConexion: WordBool): OleVariant; safecall;
    function GetDataLogSize(const Conn: WideString; const Database: WideString; 
                            bUsarConexion: WordBool): OleVariant; safecall;
    function ShrinkDB(const Conexion: WideString; const Database: WideString): OleVariant; safecall;
    function GetConexion(const Conn: WideString; const Database: WideString; 
                         const Datos: WideString; const UserName: WideString; 
                         const Password: WideString): WordBool; safecall;
    function GrabaDB_INFO(Parametros: OleVariant): OleVariant; safecall;
    function GetPathSQLServer: OleVariant; safecall;
    function NoEsBDSeleccionVisitantes(const BaseDatos: WideString): WordBool; safecall;
    function ValidarBDEmpleados(const Codigo: WideString; const Ubicacion: WideString; 
                                out Mensaje: WideString): OleVariant; safecall;
    function CrearBDEmpleados(Parametros: OleVariant): WordBool; safecall;
    function PrepararPresupuestos(Parametros: OleVariant): WordBool; safecall;
    function CatalogosPresupuestos(Parametros: OleVariant): WordBool; safecall;
    function EmpleadosPresupuestos(Parametros: OleVariant): WordBool; safecall;
    function OptimizarPresupuestos(Parametros: OleVariant): WordBool; safecall;
    function GetBDFromDB_INFO(const DB_CODIGO: WideString): WideString; safecall;
    function EjecutaMotorPatch(var Parametros: OleVariant): WordBool; safecall;
    function EsBDSeleccion(const BaseDatos: WideString): WordBool; safecall;
    function NoEsBDTressVisitantes(const BaseDatos: WideString): WordBool; safecall;
    function CrearEstructuraBD(Parametros: OleVariant): WordBool; safecall;
    function CreaInicialesTabla(Parametros: OleVariant): WordBool; safecall;
    function CreaSugeridosTabla(Parametros: OleVariant): WordBool; safecall;
    function CreaDiccionarioTabla(Parametros: OleVariant): WordBool; safecall;
    function CreaEspeciales(Parametros: OleVariant): WordBool; safecall;
    function ExistenEspeciales: WordBool; safecall;
    function ExisteBD_DBINFO(const Codigo: WideString): WordBool; safecall;
    function ImportarTablas(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetTablasBD(const BaseDatosUbicacion: WideString; const Usuario: WideString; 
                         const Password: WideString): OleVariant; safecall;
    function ConsultaActualizarBDs(Tipo: Integer): OleVariant; safecall;
    function ImportarTablaCSVGetASCII(Empresa: OleVariant; Parametros: OleVariant; 
                                      ListaASCII: OleVariant; var ErrorCount: Integer): OleVariant; safecall;
    function ImportarTablaCSVLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetCamposTabla(const BaseDatosUbicacion: WideString; const Usuario: WideString; 
                            const Password: WideString; const Tabla: WideString): OleVariant; safecall;
    function GetCamposLlaveTabla(const BaseDatosUbicacion: WideString; const Usuario: WideString; 
                                 const Password: WideString; const Tabla: WideString): OleVariant; safecall;
    function GetTablaInfo(Empresa: OleVariant; const Tabla: WideString): OleVariant; safecall;
    function GetMotorPatchAvance(Parametros: OleVariant): OleVariant; safecall;
    function GetUsuarioArbolApp(iUsuario: Integer; iExe: Integer): OleVariant; safecall;
    function GrabaArbolApp(iUsuario: Integer; iExe: Integer; oDelta: OleVariant; 
                           out ErrorCount: Integer): OleVariant; safecall;
    function PrendeDerechosEntidades(const Empresas: WideString; Destino: OleVariant; 
                                     Bitacora: WordBool; SoloEntidadesNuevas: WordBool; 
                                     out ErrorCount: Integer): OleVariant; safecall;
    function CantidadHuellaRegistrada(Empresa: OleVariant; Empleado: Integer; Biometrico: Integer; 
                                      Tipo: Integer): Integer; safecall;
    function ExtraerChecadasBitTerm(Empresa: OleVariant; Parametros: OleVariant; 
                                    out Datos: OleVariant): OleVariant; safecall;
    function GeneraReloj(Empresa: OleVariant; Parametros: OleVariant; out Datos: OleVariant): OleVariant; safecall;
    function GrabaBaseAdicionales(Empresa: OleVariant; GrupoUsuarios: Integer): OleVariant; safecall;
    function GetSistEnvioEmail(Empresa: OleVariant): OleVariant; safecall;
    function GrabaTareaCalendario(Empresa: OleVariant; Parametros: OleVariant; out Datos: OleVariant): OleVariant; safecall;
    function GetTareaCalendario(Empresa: OleVariant; Grupo: Integer): OleVariant; safecall;
    function GrabaCalendario(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetTareaUsuario(Empresa: OleVariant; Folio: Integer): OleVariant; safecall;
    function GetTareaRoles(Empresa: OleVariant; Folio: Integer): OleVariant; safecall;
    function GrabaTareaUsuario(oDelta: OleVariant; out ErrorCount: Integer; Folio: Integer): OleVariant; safecall;
    function GrabaTareaRoles(oDelta: OleVariant; out ErrorCount: Integer; Folio: Integer): OleVariant; safecall;
    function GetSuscripcionesCalendarioReportes(Empresa: OleVariant; Reporte: Integer): OleVariant; safecall;
    procedure BorrarUsuarioRolCalendario(Empresa: OleVariant; Folio: Integer; iUsCodigo: Integer; 
                                         const sRoCodigo: WideString; iTipoOperacion: Integer; 
                                         out iErrorCount: Integer); safecall;
    function GetUsuarioSuscripCalendarioReporte(Empresa: OleVariant; Usuario: Integer): OleVariant; safecall;
    function GetBitacoraReportes(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetBitacoraCorreos(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function EnviarCorreo(const sUsuario: WideString; const sBody: WideString; 
                          const sRemitente: WideString; const sDescripRemitente: WideString; 
                          const sSubject: WideString; const Encriptacion: WideString; 
                          oListaCorreos: OleVariant; const sDirecErrores: WideString; 
                          iSubtype: Integer): OleVariant; safecall;
    function ImportarSuscripcionesTressEmailPorTarea(Empresa: OleVariant; FolioTarea: Integer; 
                                                     const Frecuencia: WideString; 
                                                     const Usuarios: WideString; Reporte: Integer; 
                                                     out Error: WideString): OleVariant; safecall;
    function GetTareaCalendarioEmpresa(Empresa: OleVariant; Grupo: Integer): OleVariant; safecall;
    function GetSolicitudEnvio(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    procedure AgregarSolicitudEnvioProgramado(Folio: Integer; Reporte: Integer; 
                                              const Nombre: WideString; out Mensaje: WideString); safecall;
    procedure CancelarEnvio(Folio: Integer; var Mensaje: WideString); safecall;
    function GrabaConfigCafeteria(Empresa: OleVariant; oDelta: OleVariant; oCalend: OleVariant; 
                                  var ErrorCount: Integer; var ErrorCalendCount: Integer; 
                                  out Valor: OleVariant): OleVariant; safecall;
    function GetConfigCafeteria(const Identificador: WideString; out Calendario: OleVariant): OleVariant; safecall;
    function GetSistListEstaciones(Filtro: OleVariant): OleVariant; safecall;
    function CopiarConfiguracionCafeteria(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetUsuariosSeguridad(out Longitud: Integer; Grupo: Integer; out BlockSistema: WordBool): OleVariant; safecall;
    procedure SetEnviosProgramados(out sError: WideString); safecall;
    function GetEmpresasAccesosGruposAcceso(Parametros: OleVariant): OleVariant; safecall;
    function GetUserConfidencialidad(UserId: Integer; const Empresa: WideString): OleVariant; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerSistemaDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9F7939B2-88BA-11D3-8DEB-0050DA04EAA0}
// *********************************************************************//
  IdmServerSistemaDisp = dispinterface
    ['{9F7939B2-88BA-11D3-8DEB-0050DA04EAA0}']
    function GetEmpresas(Tipo: Integer; GrupoActivo: Integer): OleVariant; dispid 2;
    function GrabaEmpresas(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 3;
    function GetGrupos(Grupo: Integer): OleVariant; dispid 4;
    function GrabaGrupos(oDelta: OleVariant; iUsuario: Integer; out ErrorCount: Integer): OleVariant; dispid 5;
    function GetImpresoras: OleVariant; dispid 6;
    function GrabaImpresoras(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 7;
    function GetUsuarios(out Longitud: Integer; Grupo: Integer; out BlockSistema: WordBool): OleVariant; dispid 8;
    function GrabaUsuarios(oDelta: OleVariant; iUsuario: Integer; out ErrorCount: Integer): OleVariant; dispid 9;
    function GetUsuarioSupervisores(Empresa: OleVariant; iUsuario: Integer): OleVariant; dispid 10;
    function GrabaUsuariosSupervisores(Empresa: OleVariant; oDelta: OleVariant; 
                                       out ErrorCount: Integer; iUsuario: Integer; 
                                       const sTexto: WideString): OleVariant; dispid 11;
    function GetAccesos(iGrupo: Integer; const sCompany: WideString): OleVariant; dispid 12;
    function GrabaAccesos(oDelta: OleVariant; oParams: OleVariant; out ErrorCount: Integer): OleVariant; dispid 13;
    function GetUsuarioArbol(iUsuario: Integer): OleVariant; dispid 14;
    function GrabaArbol(iUsuario: Integer; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 15;
    function GetPoll: OleVariant; dispid 16;
    function GrabaPoll(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 17;
    function GetPollVacio: OleVariant; dispid 18;
    function SuspendeUsuarios(Usuario: Integer; Grupo: Integer): OleVariant; dispid 20;
    function ActivaUsuarios(Usuario: Integer; Grupo: Integer): OleVariant; dispid 21;
    function BorrarBitacora(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 22;
    function GetNivel0: OleVariant; dispid 19;
    function GrabaNivel0(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 23;
    function GetUsuarioSuscrip(Empresa: OleVariant; Usuario: Integer): OleVariant; dispid 24;
    function GrabaUsuarioSuscrip(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer; 
                                 Usuario: Integer): OleVariant; dispid 25;
    function GetGruposAccesos(iGrupo: Integer; const sCompany: WideString; iTipo: Integer): OleVariant; dispid 26;
    function GetUsuarioAreas(Empresa: OleVariant; Usuario: Integer): OleVariant; dispid 27;
    function GrabaUsuarioAreas(Empresa: OleVariant; Usuario: Integer; oDelta: OleVariant; 
                               out ErrorCount: Integer; const sTexto: WideString): OleVariant; dispid 28;
    function GetNuevoUsuario: Integer; dispid 29;
    function GetEmpresasAccesos(Tipo: Integer; Grupo: Integer): OleVariant; dispid 30;
    function ActualizaNumeroTarjeta(out Bitacora: OleVariant): OleVariant; dispid 31;
    function GetGruposAdic(Empresa: OleVariant; out oCamposAdic: OleVariant): OleVariant; dispid 32;
    function GrabaGruposAdic(Empresa: OleVariant; oDelta: OleVariant; oDeltaCampos: OleVariant; 
                             out ErrorCount: Integer; out ErrorCampos: Integer; 
                             out CamposResult: OleVariant): OleVariant; dispid 33;
    function GetBitacora(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 301;
    function GetReporteSuscrip(Empresa: OleVariant; iReporte: Integer): OleVariant; dispid 302;
    function GetRoles: OleVariant; dispid 303;
    function GetDatosEmpleadoUsuario(Empresa: OleVariant; Datos: OleVariant): OleVariant; dispid 304;
    function GrabaRoles(const Rol: WideString; Delta: OleVariant; Usuarios: OleVariant; 
                        Modelos: OleVariant; var ErrorCount: Integer): OleVariant; dispid 305;
    function GetUsuariosRol(const Rol: WideString; out Modelos: OleVariant): OleVariant; dispid 307;
    function GetUserRoles(Empresa: OleVariant; iUsuario: Integer): OleVariant; dispid 308;
    function GrabaUsuarioRoles(iUsuario: Integer; iUsuarioLog: Integer; Delta: OleVariant; 
                               var ErrorCount: Integer): OleVariant; dispid 309;
    function GrabaEmpleadoUsuario(oEmpresa: OleVariant; iUsuario: Integer; iEmpleado: Integer): OleVariant; dispid 306;
    function EnrolamientoMasivo(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 310;
    function EnrolamientoMasivoGetLista(oEmpresa: OleVariant; oParametros: OleVariant): OleVariant; dispid 311;
    function ImportarEnrolamientoMasivoGetASCII(oEmpresa: OleVariant; Parametros: OleVariant; 
                                                ListaASCII: OleVariant; out ErrorCount: Integer; 
                                                LoginAD: WordBool): OleVariant; dispid 312;
    function ImportarEnrolamientoMasivo(Empresa: OleVariant; Parametros: OleVariant; 
                                        Datos: OleVariant): OleVariant; dispid 313;
    function GetClasifiEmpresa(Empresa: OleVariant): OleVariant; dispid 314;
    function GetDerechosAdicionales(Empresa: OleVariant; Grupo: Integer): OleVariant; dispid 315;
    function GrabaAccesosAdicionales(Empresa: OleVariant; Grupo: Integer; Delta: OleVariant; 
                                     out ErrorCount: Integer): OleVariant; dispid 316;
    function GetSistLstDispositivos(Empresa: OleVariant): OleVariant; dispid 317;
    function GrabaSistLstDispositivos(Empresa: OleVariant; oDelta: OleVariant; 
                                      out ErrorCount: Integer): OleVariant; dispid 318;
    function GetGlobalLstDispositivos(Empresa: OleVariant; var Disponibles: OleVariant; 
                                      var Asignados: OleVariant): OleVariant; dispid 319;
    function GrabaGlobalLstDispositivos(Empresa: OleVariant; oDelta: OleVariant; 
                                        out ErrorCount: Integer): OleVariant; dispid 320;
    function GetAccArbol(Empresa: OleVariant): OleVariant; dispid 321;
    procedure ActualizaUsuariosEmpresa(Empresa: OleVariant; Delta: OleVariant); dispid 322;
    function ConsultaBitacoraBiometrica(Parametros: OleVariant): OleVariant; dispid 324;
    function DepuraBitacoraBiometrica(Parametros: OleVariant): OleVariant; dispid 325;
    function ObtenIdBioMaximo: Integer; dispid 323;
    function ActualizaIdBio(const CodigoEmpresaLocal: WideString; Empleado: Integer; Id: Integer; 
                            const Grupo: WideString; Invitador: Integer; 
                            const Confidencialidad: WideString): WordBool; dispid 326;
    function GetUsuarioCCosto(Empresa: OleVariant; iUsuario: Integer): OleVariant; dispid 327;
    function GrabaUsuarioCCosto(Empresa: OleVariant; oDelta: OleVariant; ErrorCount: Integer; 
                                iUsuario: Integer; const sTexto: WideString; 
                                const sNombreCosteo: WideString): OleVariant; dispid 328;
    function ConsultaListaGrupos(Empresa: OleVariant): OleVariant; dispid 329;
    function GrabaSistListaGrupos(oDelta: OleVariant; UsuarioTerminal: Integer; 
                                  out ErrorCount: Integer): OleVariant; dispid 330;
    function ConsultaTermPorGrupo(const Grupo: WideString): OleVariant; dispid 331;
    function GrabaTermPorGrupo(oDelta: OleVariant; UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant; dispid 332;
    procedure GrabaListaTermPorGrupo(const Terminales: WideString; UsuarioTerminal: Integer; 
                                     const Grupo: WideString; const SinTerminales: WideString); dispid 333;
    function AsignaNumBiometricos(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 334;
    procedure ReiniciaSincTerminales(const Lista: WideString); dispid 335;
    function AsignaGrupoTerminales(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 336;
    function EmpleadosBiometricosGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 337;
    function EmpleadosSinBiometricosGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 338;
    function AsignaGrupoTerminalesLista(Empresa: OleVariant; Lista: OleVariant; 
                                        Parametros: OleVariant): OleVariant; dispid 339;
    function AsignaNumBiometricosLista(Empresa: OleVariant; Lista: OleVariant; 
                                       Parametros: OleVariant): OleVariant; dispid 340;
    procedure ActualizaTerminalesGTI(const Lista: WideString); dispid 341;
    function ConsultaMensaje(const Filtro: WideString): OleVariant; dispid 342;
    function GrabaMensaje(oDelta: OleVariant; UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant; dispid 343;
    function ConsultaMensajeTerminal(const Filtro: WideString): OleVariant; dispid 344;
    function GrabaMensajeTerminal(oDelta: OleVariant; UsuarioTerminal: Integer; 
                                  out ErrorCount: Integer): OleVariant; dispid 345;
    function ConsultaTerminal(const Filtro: WideString): OleVariant; dispid 346;
    function GrabaTerminal(oDelta: OleVariant; UsuarioTerminal: Integer; out ErrorCount: Integer): OleVariant; dispid 347;
    function ImportaTerminales(Parametros: OleVariant; UsuarioTerminal: Integer; 
                               out Bitacora: OleVariant): OleVariant; dispid 348;
    function ExisteHuellaRegistrada(Empleado: Integer; const Empresa: WideString): WordBool; dispid 349;
    procedure ReiniciaTerminal(Terminal: Integer; const Empresa: WideString); dispid 350;
    function ExisteEmpleadoEnGrupo(const Grupo: WideString): WordBool; dispid 351;
    function ObtieneTemplates(Parametros: OleVariant): OleVariant; dispid 352;
    procedure InsertaHuella(Parametros: OleVariant); dispid 353;
    procedure EliminaHuellas(const Empresa: WideString; Empleado: Integer; Invitador: Integer; 
                             Usuario: Integer; Tipo: Integer); dispid 354;
    function ReportaHuella(Parametros: OleVariant): OleVariant; dispid 355;
    function ExisteHuellaInvitador(const Empresa: WideString; Invitador: Integer): WordBool; dispid 356;
    function ConsultaConfBD(Tipo: Integer): OleVariant; dispid 357;
    function GrabaBD(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 358;
    function GetEmpresasSistema(GrupoActivo: Integer): OleVariant; dispid 359;
    function GetBasesDatos(const CM_DATOS: WideString; const CM_USRNAME: WideString; 
                           const CM_PASSWRD: WideString): OleVariant; dispid 360;
    function GetServer: WideString; dispid 361;
    function GetSQLUserName: WideString; dispid 362;
    function EsBDEmpleados(const BaseDatos: WideString): WordBool; dispid 364;
    function EsBDAgregada(const Ubicacion: WideString): WordBool; dispid 366;
    function EmpGrabarDBSistema(Parametros: OleVariant): WordBool; dispid 371;
    function GetLogicalName(const Conexion: WideString; const Nombre: WideString; Tipo: Integer): WideString; dispid 372;
    function BackupDB(const Conexion: WideString; const Origen: WideString; 
                      const Destino: WideString; const Ruta: WideString): OleVariant; dispid 373;
    function RestoreDB(const Conexion: WideString; const Origen: WideString; 
                       const Destino: WideString; const DataName: WideString; 
                       const PathDataName: WideString; const LogName: WideString; 
                       const PathLogName: WideString): OleVariant; dispid 374;
    function DepurarTabla(const Conexion: WideString; const Tabla: WideString; 
                          const Database: WideString): OleVariant; dispid 375;
    function ConectarseServidor(const Conexion: WideString; out Mensaje: WideString): WordBool; dispid 376;
    function GetDBSize(const Conn: WideString; const Database: WideString; bUsarConexion: WordBool): OleVariant; dispid 377;
    function GetDataLogSize(const Conn: WideString; const Database: WideString; 
                            bUsarConexion: WordBool): OleVariant; dispid 378;
    function ShrinkDB(const Conexion: WideString; const Database: WideString): OleVariant; dispid 379;
    function GetConexion(const Conn: WideString; const Database: WideString; 
                         const Datos: WideString; const UserName: WideString; 
                         const Password: WideString): WordBool; dispid 380;
    function GrabaDB_INFO(Parametros: OleVariant): OleVariant; dispid 381;
    function GetPathSQLServer: OleVariant; dispid 382;
    function NoEsBDSeleccionVisitantes(const BaseDatos: WideString): WordBool; dispid 383;
    function ValidarBDEmpleados(const Codigo: WideString; const Ubicacion: WideString; 
                                out Mensaje: WideString): OleVariant; dispid 384;
    function CrearBDEmpleados(Parametros: OleVariant): WordBool; dispid 385;
    function PrepararPresupuestos(Parametros: OleVariant): WordBool; dispid 386;
    function CatalogosPresupuestos(Parametros: OleVariant): WordBool; dispid 387;
    function EmpleadosPresupuestos(Parametros: OleVariant): WordBool; dispid 388;
    function OptimizarPresupuestos(Parametros: OleVariant): WordBool; dispid 389;
    function GetBDFromDB_INFO(const DB_CODIGO: WideString): WideString; dispid 390;
    function EjecutaMotorPatch(var Parametros: OleVariant): WordBool; dispid 391;
    function EsBDSeleccion(const BaseDatos: WideString): WordBool; dispid 392;
    function NoEsBDTressVisitantes(const BaseDatos: WideString): WordBool; dispid 393;
    function CrearEstructuraBD(Parametros: OleVariant): WordBool; dispid 394;
    function CreaInicialesTabla(Parametros: OleVariant): WordBool; dispid 395;
    function CreaSugeridosTabla(Parametros: OleVariant): WordBool; dispid 396;
    function CreaDiccionarioTabla(Parametros: OleVariant): WordBool; dispid 397;
    function CreaEspeciales(Parametros: OleVariant): WordBool; dispid 399;
    function ExistenEspeciales: WordBool; dispid 400;
    function ExisteBD_DBINFO(const Codigo: WideString): WordBool; dispid 1610809470;
    function ImportarTablas(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 363;
    function GetTablasBD(const BaseDatosUbicacion: WideString; const Usuario: WideString; 
                         const Password: WideString): OleVariant; dispid 365;
    function ConsultaActualizarBDs(Tipo: Integer): OleVariant; dispid 1610809472;
    function ImportarTablaCSVGetASCII(Empresa: OleVariant; Parametros: OleVariant; 
                                      ListaASCII: OleVariant; var ErrorCount: Integer): OleVariant; dispid 369;
    function ImportarTablaCSVLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 370;
    function GetCamposTabla(const BaseDatosUbicacion: WideString; const Usuario: WideString; 
                            const Password: WideString; const Tabla: WideString): OleVariant; dispid 368;
    function GetCamposLlaveTabla(const BaseDatosUbicacion: WideString; const Usuario: WideString; 
                                 const Password: WideString; const Tabla: WideString): OleVariant; dispid 398;
    function GetTablaInfo(Empresa: OleVariant; const Tabla: WideString): OleVariant; dispid 1610809478;
    function GetMotorPatchAvance(Parametros: OleVariant): OleVariant; dispid 367;
    function GetUsuarioArbolApp(iUsuario: Integer; iExe: Integer): OleVariant; dispid 401;
    function GrabaArbolApp(iUsuario: Integer; iExe: Integer; oDelta: OleVariant; 
                           out ErrorCount: Integer): OleVariant; dispid 402;
    function PrendeDerechosEntidades(const Empresas: WideString; Destino: OleVariant; 
                                     Bitacora: WordBool; SoloEntidadesNuevas: WordBool; 
                                     out ErrorCount: Integer): OleVariant; dispid 403;
    function CantidadHuellaRegistrada(Empresa: OleVariant; Empleado: Integer; Biometrico: Integer; 
                                      Tipo: Integer): Integer; dispid 404;
    function ExtraerChecadasBitTerm(Empresa: OleVariant; Parametros: OleVariant; 
                                    out Datos: OleVariant): OleVariant; dispid 405;
    function GeneraReloj(Empresa: OleVariant; Parametros: OleVariant; out Datos: OleVariant): OleVariant; dispid 1610809484;
    function GrabaBaseAdicionales(Empresa: OleVariant; GrupoUsuarios: Integer): OleVariant; dispid 406;
    function GetSistEnvioEmail(Empresa: OleVariant): OleVariant; dispid 412;
    function GrabaTareaCalendario(Empresa: OleVariant; Parametros: OleVariant; out Datos: OleVariant): OleVariant; dispid 450;
    function GetTareaCalendario(Empresa: OleVariant; Grupo: Integer): OleVariant; dispid 451;
    function GrabaCalendario(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 452;
    function GetTareaUsuario(Empresa: OleVariant; Folio: Integer): OleVariant; dispid 453;
    function GetTareaRoles(Empresa: OleVariant; Folio: Integer): OleVariant; dispid 454;
    function GrabaTareaUsuario(oDelta: OleVariant; out ErrorCount: Integer; Folio: Integer): OleVariant; dispid 455;
    function GrabaTareaRoles(oDelta: OleVariant; out ErrorCount: Integer; Folio: Integer): OleVariant; dispid 456;
    function GetSuscripcionesCalendarioReportes(Empresa: OleVariant; Reporte: Integer): OleVariant; dispid 457;
    procedure BorrarUsuarioRolCalendario(Empresa: OleVariant; Folio: Integer; iUsCodigo: Integer; 
                                         const sRoCodigo: WideString; iTipoOperacion: Integer; 
                                         out iErrorCount: Integer); dispid 458;
    function GetUsuarioSuscripCalendarioReporte(Empresa: OleVariant; Usuario: Integer): OleVariant; dispid 459;
    function GetBitacoraReportes(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 460;
    function GetBitacoraCorreos(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 461;
    function EnviarCorreo(const sUsuario: WideString; const sBody: WideString; 
                          const sRemitente: WideString; const sDescripRemitente: WideString; 
                          const sSubject: WideString; const Encriptacion: WideString; 
                          oListaCorreos: OleVariant; const sDirecErrores: WideString; 
                          iSubtype: Integer): OleVariant; dispid 465;
    function ImportarSuscripcionesTressEmailPorTarea(Empresa: OleVariant; FolioTarea: Integer; 
                                                     const Frecuencia: WideString; 
                                                     const Usuarios: WideString; Reporte: Integer; 
                                                     out Error: WideString): OleVariant; dispid 407;
    function GetTareaCalendarioEmpresa(Empresa: OleVariant; Grupo: Integer): OleVariant; dispid 463;
    function GetSolicitudEnvio(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 464;
    procedure AgregarSolicitudEnvioProgramado(Folio: Integer; Reporte: Integer; 
                                              const Nombre: WideString; out Mensaje: WideString); dispid 466;
    procedure CancelarEnvio(Folio: Integer; var Mensaje: WideString); dispid 467;
    function GrabaConfigCafeteria(Empresa: OleVariant; oDelta: OleVariant; oCalend: OleVariant; 
                                  var ErrorCount: Integer; var ErrorCalendCount: Integer; 
                                  out Valor: OleVariant): OleVariant; dispid 408;
    function GetConfigCafeteria(const Identificador: WideString; out Calendario: OleVariant): OleVariant; dispid 409;
    function GetSistListEstaciones(Filtro: OleVariant): OleVariant; dispid 410;
    function CopiarConfiguracionCafeteria(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 411;
    function GetUsuariosSeguridad(out Longitud: Integer; Grupo: Integer; out BlockSistema: WordBool): OleVariant; dispid 413;
    procedure SetEnviosProgramados(out sError: WideString); dispid 414;
    function GetEmpresasAccesosGruposAcceso(Parametros: OleVariant): OleVariant; dispid 415;
    function GetUserConfidencialidad(UserId: Integer; const Empresa: WideString): OleVariant; dispid 416;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer; 
                             out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                           Options: Integer; const CommandText: WideString; var Params: OleVariant; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerSistema provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerSistema exposed by              
// the CoClass dmServerSistema. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerSistema = class
    class function Create: IdmServerSistema;
    class function CreateRemote(const MachineName: string): IdmServerSistema;
  end;

implementation

uses ComObj;

class function CodmServerSistema.Create: IdmServerSistema;
begin
  Result := CreateComObject(CLASS_dmServerSistema) as IdmServerSistema;
end;

class function CodmServerSistema.CreateRemote(const MachineName: string): IdmServerSistema;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerSistema) as IdmServerSistema;
end;

end.
