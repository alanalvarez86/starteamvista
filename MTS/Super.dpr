library Super;

uses
  ComServ,
  Super_TLB in 'Super_TLB.pas',
  DServerSuper in 'DServerSuper.pas' {dmServerSuper: TMtsDataModule} {dmServerSuper: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
