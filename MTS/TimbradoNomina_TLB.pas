unit TimbradoNomina_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 5/24/2016 9:53:14 AM from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3win_30_Vsn_2016\MTS\TimbradoNomina.tlb (1)
// LIBID: {33103F32-8A3F-4C62-9139-D2FCB20614D7}
// LCID: 0
// Helpfile: 
// HelpString: Tress Nomina
// DepndLst: 
//   (1) v1.0 Midas, (C:\Program Files (x86)\Embarcadero\RAD Studio\12.0\bin64\midas.dll)
//   (2) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, Midas, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  TimbradoNominaMajorVersion = 1;
  TimbradoNominaMinorVersion = 0;

  LIBID_TimbradoNomina: TGUID = '{33103F32-8A3F-4C62-9139-D2FCB20614D7}';

  IID_IdmServerNominaTimbrado: TGUID = '{499DA6EF-CE43-4910-8E24-1E5C148B69DF}';
  CLASS_dmServerNominaTimbrado: TGUID = '{0ED8F668-7E6E-4E9A-BF24-3FCBF7A25432}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerNominaTimbrado = interface;
  IdmServerNominaTimbradoDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerNominaTimbrado = IdmServerNominaTimbrado;


// *********************************************************************//
// Interface: IdmServerNominaTimbrado
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {499DA6EF-CE43-4910-8E24-1E5C148B69DF}
// *********************************************************************//
  IdmServerNominaTimbrado = interface(IAppServer)
    ['{499DA6EF-CE43-4910-8E24-1E5C148B69DF}']
    function GetEmpleadoClasifi(Empresa: OleVariant; iEmpleado: Integer; iYear: Integer; 
                                iTipo: Integer; iPeriodo: Integer): OleVariant; safecall;
    function GetDatosTotales(Empresa: OleVariant; iYear: Integer; iTipo: Integer; 
                             iPeriodo: Integer; const sConfiden: WideString): OleVariant; safecall;
    function GetDatosExcepGlobales(Empresa: OleVariant; iYear: Integer; iTipo: Integer; 
                                   iPeriodo: Integer): OleVariant; safecall;
    function GetDatosExcepciones(Empresa: OleVariant; iYear: Integer; iTipo: Integer; 
                                 iPeriodo: Integer; lSoloAltas: WordBool): OleVariant; safecall;
    function GetDatosExcepMontos(Empresa: OleVariant; iYear: Integer; iTipo: Integer; 
                                 iPeriodo: Integer; lSoloAltas: WordBool): OleVariant; safecall;
    function GetDatosNomina(Empresa: OleVariant; iEmpleado: Integer; iYear: Integer; 
                            iTipo: Integer; iPeriodo: Integer): OleVariant; safecall;
    function GetNomDatosAsist(Empresa: OleVariant; Parametros: OleVariant; out Tarjetas: OleVariant): OleVariant; safecall;
    function GetMontos(Empresa: OleVariant; iEmpleado: Integer; iYear: Integer; iTipo: Integer; 
                       iPeriodo: Integer): OleVariant; safecall;
    function GetMovMontos(Empresa: OleVariant; iEmpleado: Integer; iYear: Integer; iTipo: Integer; 
                          iPeriodo: Integer; out oTotales: OleVariant): OleVariant; safecall;
    function GetDiasHoras(Empresa: OleVariant; iEmpleado: Integer; iYear: Integer; iTipo: Integer; 
                          iPeriodo: Integer): OleVariant; safecall;
    function GetFaltas(Empresa: OleVariant; iEmpleado: Integer; iYear: Integer; iTipo: Integer; 
                       iPeriodo: Integer; out oTotales: OleVariant): OleVariant; safecall;
    function GetPagoRecibos(Empresa: OleVariant; iYear: Integer; iTipo: Integer; iPeriodo: Integer; 
                            lSoloAltas: WordBool): OleVariant; safecall;
    function GrabaEmpleadoClasifi(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GrabaExcepGlobales(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GrabaMovMontos(Empresa: OleVariant; Parametros: OleVariant; oDelta: OleVariant; 
                            out ErrorCount: Integer; out ErrorData: OleVariant): OleVariant; safecall;
    function GrabaFaltas(Empresa: OleVariant; Parametros: OleVariant; oDelta: OleVariant; 
                         out ErrorCount: Integer; out ErrorData: OleVariant): OleVariant; safecall;
    function GrabaNomina(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GrabaPrenomina(Empresa: OleVariant; Parametros: OleVariant; oDelta: OleVariant; 
                            out ErrorCount: Integer): OleVariant; safecall;
    function GrabaPagoRecibos(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function ValidaNominaEmpleado(Empresa: OleVariant; iEmpleado: Integer; iYear: Integer; 
                                  iTipo: Integer; iPeriodo: Integer; out dFecha: TDateTime; 
                                  out iUsuario: Integer): WordBool; safecall;
    function GetLiquidacion(Empresa: OleVariant; Empleado: Integer; Parametros: OleVariant; 
                            out Ahorros: OleVariant; out Prestamos: OleVariant; Ingreso: TDateTime): OleVariant; safecall;
    procedure GrabaLiquidacion(Empresa: OleVariant; Parametros: OleVariant; Datos: OleVariant; 
                               Ahorros: OleVariant; Prestamos: OleVariant); safecall;
    function BorraNomina(Empresa: OleVariant; iUsuario: Integer; dFecha: TDateTime; iYear: Integer; 
                         iTipo: Integer; iPeriodo: Integer; BorraRegistros: WordBool; 
                         const Filtro: WideString): OleVariant; safecall;
    function BorrarPeriodos(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function AfectarNomina(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function DesafectarNomina(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function DefinirPeriodos(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function RecalculoAcumulado(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function LimpiarAcumulado(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function RecalculoDias(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CalcularDiferencias(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CopiarNomina(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function FoliarRecibos(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function ReFoliarRecibos(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function PagarPorFuera(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function PagarPorFueraLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function PagarPorFueraGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function Poliza(Empresa: OleVariant; Parametros: OleVariant; Filtros: OleVariant): OleVariant; safecall;
    function LiquidacionGlobal(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function LiquidacionGlobalLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function LiquidacionGlobalGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetDatosJornada(Empresa: OleVariant; const Turno: WideString; FechaInicial: TDateTime; 
                             FechaFinal: TDateTime): OleVariant; safecall;
    function ImportarMovimientos(Empresa: OleVariant; Parametros: OleVariant; Datos: OleVariant): OleVariant; safecall;
    function ImportarMovimientosGetASCII(Empresa: OleVariant; Parametros: OleVariant; 
                                         ListaASCII: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function ExportarMovimientos(Empresa: OleVariant; Parametros: OleVariant; out Datos: OleVariant): OleVariant; safecall;
    function ImportarPagoRecibosGetASCII(Empresa: OleVariant; Parametros: OleVariant; 
                                         ListaASCII: OleVariant; var ErrorCount: Integer): OleVariant; safecall;
    function ImportarPagoRecibosLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function ImportarPagoRecibosGetLista(Empresa: OleVariant; Parametros: OleVariant; 
                                         Lista: OleVariant): OleVariant; safecall;
    function GetDatosPoliza(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GrabaDatosPoliza(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function AplicaFiniquito(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function AjusteRetFonacot(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function AjusteRetFonacotGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function AjusteRetFonacotLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CalcularPagoFonacot(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetTotalesFonacot(Empresa: OleVariant; iYear: Integer; iMes: Integer): OleVariant; safecall;
    function GrabaTotalesFonacot(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetDatosFonacot(Empresa: OleVariant; iYear: Integer; iMes: Integer; 
                             iEmpleado: Integer; iTabla: Integer): OleVariant; safecall;
    function GetTNomEmpleado(Empresa: OleVariant; iEmpleado: Integer; dFecha: TDateTime): Integer; safecall;
    function GetFecCambioTNom(Empresa: OleVariant; iEmpleado: Integer; dFecha: TDateTime): TDateTime; safecall;
    function GetSimulacionesGlobales(Empresa: OleVariant; Params: OleVariant): OleVariant; safecall;
    function GetTotalesConceptos(Empresa: OleVariant; Parametros: OleVariant; 
                                 out Movimientos: OleVariant): OleVariant; safecall;
    function GetSimulacionesAprobar(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function AplicacionGlobalFiniquitos(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function AplicacionGlobalFiniquitosGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function AplicacionGlobalFiniquitosLista(Empresa: OleVariant; Lista: OleVariant; 
                                             Parametros: OleVariant): OleVariant; safecall;
    function GetDeclaracion(Empresa: OleVariant): OleVariant; safecall;
    function TimbrarNominasLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function TimbrarNominasGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CancelarTimbrado(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function MarcarTimbrado(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetLookupEmpleado(Empresa: OleVariant; Empleado: Integer; out Datos: OleVariant; 
                               TipoLookup: Integer): WordBool; safecall;
    function GetEmpleadosBuscados_DevExTimbrado(Empresa: OleVariant; const sPista: WideString): OleVariant; safecall;
    function ActualizaFoliosFiscales(Empresa: OleVariant; Parameters: OleVariant): OleVariant; safecall;
    function GetFoliosPendientes(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetEmpleadosBuscados_DevExTimbradoAvanzada(Empresa: OleVariant; 
                                                        const sPaterno: WideString; 
                                                        const sMaterno: WideString; 
                                                        const sNombre: WideString; 
                                                        const sRFC: WideString; 
                                                        const sNSS: WideString; 
                                                        const sBanca: WideString): OleVariant; safecall;
    function GetEmpleadoTransferidoParaActivo(Empresa: OleVariant; Empleado: Integer; 
                                              out Datos: OleVariant): WordBool; safecall;
    function GetEmpleadoTransferidoSiguiente(Empresa: OleVariant; Empleado: Integer; 
                                             const sNavega: WideString; out Datos: OleVariant): WordBool; safecall;
    function GetEmpleadoTransferidoAnterior(Empresa: OleVariant; Empleado: Integer; 
                                            const sNavega: WideString; out Datos: OleVariant): WordBool; safecall;
    function GetValidacionEmpleadoTransferido(Empresa: OleVariant; Empleado: Integer; 
                                              out Datos: OleVariant): WordBool; safecall;
    function GetPeriodoTimbrado(Empresa: OleVariant; Year: Integer; Tipo: Integer; Numero: Integer; 
                                out Datos: OleVariant): WordBool; safecall;
    function GetPeriodoAnteriorTimbrado(Empresa: OleVariant; Year: Integer; Tipo: Integer; 
                                        Numero: Integer; out Datos: OleVariant): WordBool; safecall;
    function GetPeriodoSiguienteTimbrado(Empresa: OleVariant; Year: Integer; Tipo: Integer; 
                                         Numero: Integer; out Datos: OleVariant): WordBool; safecall;
    function IniciarConciliacionTimbradoPeriodos(var Parametros: OleVariant): WordBool; safecall;
    function AplicarConciliacionTimbradoPeriodos(var Parametros: OleVariant): WordBool; safecall;
    function ObtenerPeriodosAConciliarTimbrado(Empresa: OleVariant; iYear: Integer; iMes: Integer; 
                                               const sRsCodigo: WideString): OleVariant; safecall;
    function GetMotorAvanceProceso(var Parametros: OleVariant): OleVariant; safecall;
    function ObtenerXMLEmpleadosAConciliarTimbrado(Empresa: OleVariant; var Params: OleVariant; 
                                                   var XML: WideString; var Error: WideString): OleVariant; safecall;
    function ObtenerPeriodosConciliados(Empresa: OleVariant; var Params: OleVariant): OleVariant; safecall;
    function GetEmpleadosConciliados(Empresa: OleVariant; var Params: OleVariant): OleVariant; safecall;
    function SetEmpleadosPendienteTimbrado(Empresa: OleVariant; Lista: OleVariant; 
                                           Parametros: OleVariant): OleVariant; safecall;
    function GetXMLAutidoriaConceptoTimbrado(Empresa: OleVariant; var Params: OleVariant): OleVariant; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerNominaTimbradoDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {499DA6EF-CE43-4910-8E24-1E5C148B69DF}
// *********************************************************************//
  IdmServerNominaTimbradoDisp = dispinterface
    ['{499DA6EF-CE43-4910-8E24-1E5C148B69DF}']
    function GetEmpleadoClasifi(Empresa: OleVariant; iEmpleado: Integer; iYear: Integer; 
                                iTipo: Integer; iPeriodo: Integer): OleVariant; dispid 1;
    function GetDatosTotales(Empresa: OleVariant; iYear: Integer; iTipo: Integer; 
                             iPeriodo: Integer; const sConfiden: WideString): OleVariant; dispid 2;
    function GetDatosExcepGlobales(Empresa: OleVariant; iYear: Integer; iTipo: Integer; 
                                   iPeriodo: Integer): OleVariant; dispid 3;
    function GetDatosExcepciones(Empresa: OleVariant; iYear: Integer; iTipo: Integer; 
                                 iPeriodo: Integer; lSoloAltas: WordBool): OleVariant; dispid 4;
    function GetDatosExcepMontos(Empresa: OleVariant; iYear: Integer; iTipo: Integer; 
                                 iPeriodo: Integer; lSoloAltas: WordBool): OleVariant; dispid 5;
    function GetDatosNomina(Empresa: OleVariant; iEmpleado: Integer; iYear: Integer; 
                            iTipo: Integer; iPeriodo: Integer): OleVariant; dispid 6;
    function GetNomDatosAsist(Empresa: OleVariant; Parametros: OleVariant; out Tarjetas: OleVariant): OleVariant; dispid 7;
    function GetMontos(Empresa: OleVariant; iEmpleado: Integer; iYear: Integer; iTipo: Integer; 
                       iPeriodo: Integer): OleVariant; dispid 8;
    function GetMovMontos(Empresa: OleVariant; iEmpleado: Integer; iYear: Integer; iTipo: Integer; 
                          iPeriodo: Integer; out oTotales: OleVariant): OleVariant; dispid 9;
    function GetDiasHoras(Empresa: OleVariant; iEmpleado: Integer; iYear: Integer; iTipo: Integer; 
                          iPeriodo: Integer): OleVariant; dispid 10;
    function GetFaltas(Empresa: OleVariant; iEmpleado: Integer; iYear: Integer; iTipo: Integer; 
                       iPeriodo: Integer; out oTotales: OleVariant): OleVariant; dispid 11;
    function GetPagoRecibos(Empresa: OleVariant; iYear: Integer; iTipo: Integer; iPeriodo: Integer; 
                            lSoloAltas: WordBool): OleVariant; dispid 12;
    function GrabaEmpleadoClasifi(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 13;
    function GrabaExcepGlobales(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 14;
    function GrabaMovMontos(Empresa: OleVariant; Parametros: OleVariant; oDelta: OleVariant; 
                            out ErrorCount: Integer; out ErrorData: OleVariant): OleVariant; dispid 17;
    function GrabaFaltas(Empresa: OleVariant; Parametros: OleVariant; oDelta: OleVariant; 
                         out ErrorCount: Integer; out ErrorData: OleVariant): OleVariant; dispid 18;
    function GrabaNomina(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 19;
    function GrabaPrenomina(Empresa: OleVariant; Parametros: OleVariant; oDelta: OleVariant; 
                            out ErrorCount: Integer): OleVariant; dispid 20;
    function GrabaPagoRecibos(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 23;
    function ValidaNominaEmpleado(Empresa: OleVariant; iEmpleado: Integer; iYear: Integer; 
                                  iTipo: Integer; iPeriodo: Integer; out dFecha: TDateTime; 
                                  out iUsuario: Integer): WordBool; dispid 24;
    function GetLiquidacion(Empresa: OleVariant; Empleado: Integer; Parametros: OleVariant; 
                            out Ahorros: OleVariant; out Prestamos: OleVariant; Ingreso: TDateTime): OleVariant; dispid 25;
    procedure GrabaLiquidacion(Empresa: OleVariant; Parametros: OleVariant; Datos: OleVariant; 
                               Ahorros: OleVariant; Prestamos: OleVariant); dispid 26;
    function BorraNomina(Empresa: OleVariant; iUsuario: Integer; dFecha: TDateTime; iYear: Integer; 
                         iTipo: Integer; iPeriodo: Integer; BorraRegistros: WordBool; 
                         const Filtro: WideString): OleVariant; dispid 27;
    function BorrarPeriodos(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 28;
    function AfectarNomina(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 30;
    function DesafectarNomina(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 31;
    function DefinirPeriodos(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 32;
    function RecalculoAcumulado(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 33;
    function LimpiarAcumulado(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 34;
    function RecalculoDias(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 36;
    function CalcularDiferencias(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 35;
    function CopiarNomina(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 37;
    function FoliarRecibos(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 38;
    function ReFoliarRecibos(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 39;
    function PagarPorFuera(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 40;
    function PagarPorFueraLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 41;
    function PagarPorFueraGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 42;
    function Poliza(Empresa: OleVariant; Parametros: OleVariant; Filtros: OleVariant): OleVariant; dispid 43;
    function LiquidacionGlobal(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 44;
    function LiquidacionGlobalLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 45;
    function LiquidacionGlobalGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 46;
    function GetDatosJornada(Empresa: OleVariant; const Turno: WideString; FechaInicial: TDateTime; 
                             FechaFinal: TDateTime): OleVariant; dispid 29;
    function ImportarMovimientos(Empresa: OleVariant; Parametros: OleVariant; Datos: OleVariant): OleVariant; dispid 15;
    function ImportarMovimientosGetASCII(Empresa: OleVariant; Parametros: OleVariant; 
                                         ListaASCII: OleVariant; out ErrorCount: Integer): OleVariant; dispid 16;
    function ExportarMovimientos(Empresa: OleVariant; Parametros: OleVariant; out Datos: OleVariant): OleVariant; dispid 21;
    function ImportarPagoRecibosGetASCII(Empresa: OleVariant; Parametros: OleVariant; 
                                         ListaASCII: OleVariant; var ErrorCount: Integer): OleVariant; dispid 22;
    function ImportarPagoRecibosLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 47;
    function ImportarPagoRecibosGetLista(Empresa: OleVariant; Parametros: OleVariant; 
                                         Lista: OleVariant): OleVariant; dispid 48;
    function GetDatosPoliza(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 301;
    function GrabaDatosPoliza(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 302;
    function AplicaFiniquito(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 303;
    function AjusteRetFonacot(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 304;
    function AjusteRetFonacotGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 305;
    function AjusteRetFonacotLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 306;
    function CalcularPagoFonacot(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 307;
    function GetTotalesFonacot(Empresa: OleVariant; iYear: Integer; iMes: Integer): OleVariant; dispid 308;
    function GrabaTotalesFonacot(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 309;
    function GetDatosFonacot(Empresa: OleVariant; iYear: Integer; iMes: Integer; 
                             iEmpleado: Integer; iTabla: Integer): OleVariant; dispid 310;
    function GetTNomEmpleado(Empresa: OleVariant; iEmpleado: Integer; dFecha: TDateTime): Integer; dispid 311;
    function GetFecCambioTNom(Empresa: OleVariant; iEmpleado: Integer; dFecha: TDateTime): TDateTime; dispid 312;
    function GetSimulacionesGlobales(Empresa: OleVariant; Params: OleVariant): OleVariant; dispid 313;
    function GetTotalesConceptos(Empresa: OleVariant; Parametros: OleVariant; 
                                 out Movimientos: OleVariant): OleVariant; dispid 314;
    function GetSimulacionesAprobar(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 315;
    function AplicacionGlobalFiniquitos(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 317;
    function AplicacionGlobalFiniquitosGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 316;
    function AplicacionGlobalFiniquitosLista(Empresa: OleVariant; Lista: OleVariant; 
                                             Parametros: OleVariant): OleVariant; dispid 318;
    function GetDeclaracion(Empresa: OleVariant): OleVariant; dispid 319;
    function TimbrarNominasLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 320;
    function TimbrarNominasGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 321;
    function CancelarTimbrado(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 322;
    function MarcarTimbrado(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 323;
    function GetLookupEmpleado(Empresa: OleVariant; Empleado: Integer; out Datos: OleVariant; 
                               TipoLookup: Integer): WordBool; dispid 324;
    function GetEmpleadosBuscados_DevExTimbrado(Empresa: OleVariant; const sPista: WideString): OleVariant; dispid 325;
    function ActualizaFoliosFiscales(Empresa: OleVariant; Parameters: OleVariant): OleVariant; dispid 326;
    function GetFoliosPendientes(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 327;
    function GetEmpleadosBuscados_DevExTimbradoAvanzada(Empresa: OleVariant; 
                                                        const sPaterno: WideString; 
                                                        const sMaterno: WideString; 
                                                        const sNombre: WideString; 
                                                        const sRFC: WideString; 
                                                        const sNSS: WideString; 
                                                        const sBanca: WideString): OleVariant; dispid 328;
    function GetEmpleadoTransferidoParaActivo(Empresa: OleVariant; Empleado: Integer; 
                                              out Datos: OleVariant): WordBool; dispid 329;
    function GetEmpleadoTransferidoSiguiente(Empresa: OleVariant; Empleado: Integer; 
                                             const sNavega: WideString; out Datos: OleVariant): WordBool; dispid 330;
    function GetEmpleadoTransferidoAnterior(Empresa: OleVariant; Empleado: Integer; 
                                            const sNavega: WideString; out Datos: OleVariant): WordBool; dispid 331;
    function GetValidacionEmpleadoTransferido(Empresa: OleVariant; Empleado: Integer; 
                                              out Datos: OleVariant): WordBool; dispid 332;
    function GetPeriodoTimbrado(Empresa: OleVariant; Year: Integer; Tipo: Integer; Numero: Integer; 
                                out Datos: OleVariant): WordBool; dispid 333;
    function GetPeriodoAnteriorTimbrado(Empresa: OleVariant; Year: Integer; Tipo: Integer; 
                                        Numero: Integer; out Datos: OleVariant): WordBool; dispid 334;
    function GetPeriodoSiguienteTimbrado(Empresa: OleVariant; Year: Integer; Tipo: Integer; 
                                         Numero: Integer; out Datos: OleVariant): WordBool; dispid 335;
    function IniciarConciliacionTimbradoPeriodos(var Parametros: OleVariant): WordBool; dispid 336;
    function AplicarConciliacionTimbradoPeriodos(var Parametros: OleVariant): WordBool; dispid 337;
    function ObtenerPeriodosAConciliarTimbrado(Empresa: OleVariant; iYear: Integer; iMes: Integer; 
                                               const sRsCodigo: WideString): OleVariant; dispid 338;
    function GetMotorAvanceProceso(var Parametros: OleVariant): OleVariant; dispid 339;
    function ObtenerXMLEmpleadosAConciliarTimbrado(Empresa: OleVariant; var Params: OleVariant; 
                                                   var XML: WideString; var Error: WideString): OleVariant; dispid 340;
    function ObtenerPeriodosConciliados(Empresa: OleVariant; var Params: OleVariant): OleVariant; dispid 341;
    function GetEmpleadosConciliados(Empresa: OleVariant; var Params: OleVariant): OleVariant; dispid 342;
    function SetEmpleadosPendienteTimbrado(Empresa: OleVariant; Lista: OleVariant; 
                                           Parametros: OleVariant): OleVariant; dispid 343;
    function GetXMLAutidoriaConceptoTimbrado(Empresa: OleVariant; var Params: OleVariant): OleVariant; dispid 344;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer; 
                             out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                           Options: Integer; const CommandText: WideString; var Params: OleVariant; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerNominaTimbrado provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerNominaTimbrado exposed by              
// the CoClass dmServerNominaTimbrado. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerNominaTimbrado = class
    class function Create: IdmServerNominaTimbrado;
    class function CreateRemote(const MachineName: string): IdmServerNominaTimbrado;
  end;

implementation

uses ComObj;

class function CodmServerNominaTimbrado.Create: IdmServerNominaTimbrado;
begin
  Result := CreateComObject(CLASS_dmServerNominaTimbrado) as IdmServerNominaTimbrado;
end;

class function CodmServerNominaTimbrado.CreateRemote(const MachineName: string): IdmServerNominaTimbrado;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerNominaTimbrado) as IdmServerNominaTimbrado;
end;

end.
