unit DServerSelReportes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComServ, ComObj, VCLCom, StdVcl, {BdeMts,} DataBkr, DBClient,
  MtsRdm, Mtx,
  {$ifndef DOS_CAPAS}
  SelReportes_TLB,
  {$endif}
  ZCreator,
  DZetaServerProvider;

type
  TdmServerSelReportes = class(TMtsDataModule{$ifndef DOS_CAPAS}, IdmServerSelReportes {$endif})
    procedure MtsDataModuleCreate(Sender: TObject);
    procedure MtsDataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    oZetaProvider : TdmZetaServerProvider;
    oZetaCreator : TZetaCreator;
    procedure InitCreator;
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
  {$ifdef DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
  {$endif}
    { Public declarations }
    function GeneraSQL(Empresa: OleVariant; var AgenteSQL: OleVariant;Parametros: OleVariant; out Error: WideString): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function EvaluaParam(Empresa: OleVariant; var AgenteSQL: OleVariant;Parametros: OleVariant; out Error: WideString): WordBool; {$ifndef DOS_CAPAS}safecall;{$endif}
  end;

var
  dmServerSelReportes: TdmServerSelReportes;

implementation

uses DServerReporteador,
     ZetaServerTools,
     ZetaSQLBroker;

{$R *.DFM}

class procedure TdmServerSelReportes.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

procedure TdmServerSelReportes.MtsDataModuleCreate(Sender: TObject);
begin
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
end;

procedure TdmServerSelReportes.MtsDataModuleDestroy(Sender: TObject);
begin
     ZetaSQLBroker.FreeZetaCreator( oZetaCreator );
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
end;

procedure TdmServerSelReportes.InitCreator;
begin
     if not Assigned( oZetaCreator ) then
        oZetaCreator := ZetaSQLBroker.GetZetaCreator( oZetaProvider );
end;

function TdmServerSelReportes.GeneraSQL( Empresa: OleVariant;var AgenteSQL: OleVariant; Parametros: OleVariant;
                                         out Error: WideString): OleVariant;
var
   Reporte: TdmServerReporteador;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
     end;
     InitCreator;
     Reporte := TdmServerReporteador.Create( oZetaProvider, oZetaCreator );
     try
        Result := Reporte.GeneraSQL( Empresa,
                                     AgenteSQL,
                                     Parametros,
                                     Error );
     finally
            FreeAndNil(Reporte);
     end;
     SetComplete;
end;

function TdmServerSelReportes.EvaluaParam(Empresa: OleVariant;var AgenteSQL: OleVariant; Parametros: OleVariant; out Error: WideString): WordBool;
var
   Reporte: TdmServerReporteador;
begin
     InitCreator;
     Reporte := TdmServerReporteador.Create( oZetaProvider, oZetaCreator );
     try
        Result := Reporte.EvaluaParam( Empresa,
                                       AgenteSQL,
                                       Parametros,
                                       Error );
     finally
            FreeAndNil(Reporte);
     end;
     SetComplete;
end;

{$ifdef DOS_CAPAS}
procedure TdmServerSelReportes.CierraEmpresa;
begin

end;
{$endif}

{$ifndef DOS_CAPAS}
initialization
  TComponentFactory.Create(ComServer, TdmServerSelReportes,
    Class_dmServerSelReportes, ciMultiInstance, ZetaServerTools.GetThreadingModel);
{$endif}
end.