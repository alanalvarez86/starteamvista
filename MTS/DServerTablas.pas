unit DServerTablas;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.7 / XE3                           ::
  :: Unidad:      DServerTablas.pas                          ::
  :: Descripci�n: Programa principal de Tablas.dll           ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface 

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComServ, ComObj, VCLCom, StdVcl, DataBkr, DBClient, MtsRdm, Mtx,
     Variants,
{$ifndef DOS_CAPAS}
     Tablas_TLB,
{$endif}
     DZetaServerProvider, Db, ZetaServerDataSet;

type
  eTipoTabla = ( eGeneral {0},
                 eTipoCambio,
                 eSalMin,
                 eLeyIMSS,
                 eNumericas,
                 eGradosRiesgo {5},
                 eHabitacion,
                 eEstadoCivil,
                 eViveCon,
                 eTransporte,
                 eExtra1 {10},
                 eExtra2,
                 eExtra3,
                 eExtra4,
                 eEstado,
                 eTipoCursos {15},
                 eEstudios,
                 eMovKardex,
                 eNivel1,
                 eNivel2,         
                 eNivel3 {20},
                 eNivel4,
                 eNivel5,
                 eNivel6,
                 eNivel7,
                 eNivel8 {25},
                 eNivel9,
                 eIncidencias,
                 eMotivoBaja,
                 eTAhorro,
                 eTPresta {30},
                 eMotAuto,
                 eMonedas,
                 eTallas,
                 eMotivoTools,
                 eClasifiCurso {35},
                 eTArt80,
                 eColonia,
                 eExtra5,
                 eExtra6,
                 eExtra7 {40},
                 eExtra8,
                 eExtra9,
                 eExtra10,
                 eExtra11,
                 eExtra12 {45},
                 eExtra13,
                 eExtra14
                 ,eMotCheca
                 ,eNivel10
                 ,eNivel11 {50}
                 ,eNivel12
                 ,eOcupaNac
                 ,eAreaTemCur
                 ,eMunicipios {54}
                 ,eMotTransfer
                 ,eTCompetencias  {56}
                 ,eTPerfiles      {57}
                 ,eTNacComp     {58}
                 ,eBancos {59}
                 ,eTipoContrato {60}
                 ,eTipoJornada {61}
                 ,eRiesgoPuesto {62}
                 ,eUma {63}
                 ,eRegimenContraTrabaj {64}
                 );



type
  TdmServerTablas = class(TMtsDataModule{$ifndef DOS_CAPAS}, IdmServerTablas {$endif})
    cdsLista: TServerDataSet;
    procedure dmServerTablasCreate(Sender: TObject);
    procedure dmServerTablasDestroy(Sender: TObject);
  private
    { Private declarations }
    FTipoTabla: eTipoTabla;
    oZetaProvider: TdmZetaServerProvider;
    function GetNombreTabla(const eTabla: eTipoTabla): string;
    procedure SetTablaInfo(const eTabla: eTipoTabla);
    procedure SetDetailInfo(eDetail: eTipoTabla);
    procedure GrabaCambiosBitacora( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind );
    {$ifndef DOS_CAPAS}
    procedure ServerActivate(Sender: TObject);
    procedure ServerDeactivate(Sender: TObject);
    {$endif}
  protected
    { Protected declarations }
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
{$ifdef DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
{$endif}
    function GetTabla(Empresa: OleVariant; iTabla: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaTabla(Empresa: OleVariant; iTabla: Integer; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetNumerica(Empresa: OleVariant; Codigo: Integer; out oArt80: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaNumerica(Empresa, oDelta, oArt80: OleVariant; out ErrorCount, ErrorArt80: Integer; out oArt80Result: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetPrioridadDescuento(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaPrioridadDescuento(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetUMA(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
  end;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses,
     ZetaServerTools,
     ZGlobalTress,
     ZetaCommonLists;

type
    eScripts = ( qGetPrioridad,
                 qGrabaPrioridadAhorro,
                 qGrabaPrioridadPresta,
                 qGetNombreAdicional,
                 qGetValUma); // (JB) Se grega Query en funcion GetScript

{$R *.DFM}

function GetScript( const eQuery: eScripts ): String;
begin
     case eQuery of
          qGetPrioridad: Result := 'select A.TB_CODIGO, CAST( ''A'' AS CHAR( 1 ) ) TB_CLASE, CAST( %s as VARCHAR( 10 ) ) TB_TIPO, '+
                                   'A.TB_ELEMENT, A.TB_ALTA, A.TB_PRIORID from TAHORRO A '+
                                   'union '+
                                   'select P.TB_CODIGO, CAST( ''P'' AS CHAR( 1 ) ) TB_CLASE, CAST( %s as VARCHAR( 10 ) ) TB_TIPO, '+
                                   'P.TB_ELEMENT, P.TB_ALTA, P.TB_PRIORID from TPRESTA P '+
                                   'order by 6 desc, 2, 1';
          qGrabaPrioridadAhorro: Result := 'update TAHORRO set TB_PRIORID = :TB_PRIORID where ( TB_CODIGO = :TB_CODIGO )';
          qGrabaPrioridadPresta: Result := 'update TPRESTA set TB_PRIORID = :TB_PRIORID where ( TB_CODIGO = :TB_CODIGO )';
          qGetNombreAdicional : Result := 'SELECT CX_TITULO FROM CAMPO_AD WHERE CX_NOMBRE = %s';
          qGetValUma : Result :=  'select UM_FEC_INI, UM_VALOR, UM_FDESC from VALOR_UMA order by UM_FEC_INI desc';

     else
         Result := VACIO;
     end;
end;

class procedure TdmServerTablas.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
     if Register then
     begin
          inherited UpdateRegistry(Register, ClassID, ProgID);
          EnableSocketTransport(ClassID);
          EnableWebTransport(ClassID);
     end
     else
     begin
          DisableSocketTransport(ClassID);
          DisableWebTransport(ClassID);
          inherited UpdateRegistry(Register, ClassID, ProgID);
     end;
end;

procedure TdmServerTablas.dmServerTablasCreate(Sender: TObject);
begin
     {$ifndef DOS_CAPAS}
     OnActivate := ServerActivate;
     OnDeactivate := ServerDeactivate;
     {$endif}
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
end;

procedure TdmServerTablas.dmServerTablasDestroy(Sender: TObject);
begin
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
end;

{$ifdef DOS_CAPAS}
procedure TdmServerTablas.CierraEmpresa;
begin
end;
{$else}
procedure TdmServerTablas.ServerActivate(Sender: TObject);
begin
     oZetaProvider.Activate;
end;

procedure TdmServerTablas.ServerDeactivate(Sender: TObject);
begin
     oZetaProvider.Deactivate;
end;
{$endif}

{ ********** M�todos Privados *********************** }

procedure TdmServerTablas.SetTablaInfo( const eTabla: eTipoTabla );
begin
     with oZetaProvider.TablaInfo do
     begin
          case eTabla of
              eTipoCambio : SetInfo( 'TCAMBIO','TC_FEC_INI,TC_MONTO,TC_NUMERO,TC_TEXTO', 'TC_FEC_INI' );
              eSalMin : SetInfo( 'SAL_MIN', 'SM_FEC_INI,SM_ZONA_A,SM_ZONA_B,SM_ZONA_C', 'SM_FEC_INI' );
              eLeyIMSS : SetInfo( 'LEY_IMSS', 'SS_INICIAL,SS_L_A1061,SS_L_A1062,SS_L_A107,'+
                                              'SS_L_A25,SS_L_CV,SS_L_GUARD,SS_L_INFO,SS_L_IV,SS_L_RT,SS_L_SAR,SS_MAXIMO,SS_O_A1061,SS_O_A1062,SS_O_A107,SS_O_A25,SS_O_CV,SS_O_GUARD,SS_O_INFO,SS_O_IV,SS_O_SAR,SS_P_A1061,SS_P_A1062,SS_P_A107,SS_P_A25,SS_P_CV,SS_P_GUARD,SS_P_INFO,SS_P_IV,SS_P_SAR', 'SS_INICIAL' );
              eNumericas : SetInfo( 'NUMERICA', 'NU_CODIGO,NU_DESCRIP,NU_INGLES,NU_NUMERO,NU_TEXTO', 'NU_CODIGO' );
              eGradosRiesgo : SetInfo( 'RIESGO', 'RI_CLASE,RI_GRADO,RI_INDICE,RI_PRIMA', 'RI_CLASE,RI_GRADO' );
              eMovKardex : SetInfo('TKARDEX', 'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_NIVEL,TB_SISTEMA', 'TB_CODIGO' );
              eIncidencias : SetInfo('INCIDEN', 'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_INCIDEN,TB_INCAPA,TB_SISTEMA,TB_PERMISO', 'TB_CODIGO' );
              eMotivoBaja : SetInfo('MOT_BAJA', 'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_IMSS,TB_NUMERO,TB_TEXTO', 'TB_CODIGO' );
              eTAhorro : SetInfo('TAHORRO', 'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_CONCEPT,TB_RELATIV,TB_LIQUIDA,TB_ALTA,TB_PRESTA,TB_TASA1,TB_TASA2,TB_TASA3,TB_VAL_RAN,TB_FEC_FIN,TB_FEC_INI,TB_ACTIVO,TB_NIVEL0', 'TB_CODIGO' );
              eTPresta : SetInfo('TPRESTA', 'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_CONCEPT,TB_RELATIV,TB_LIQUIDA,TB_ALTA,TB_TASA1,TB_TASA2,TB_TASA3,TB_ACTIVO,TB_NIVEL0', 'TB_CODIGO' );
              eMonedas : SetInfo( 'MONEDA', 'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_VALOR', 'TB_CODIGO' );
              eMotivoTools : SetInfo( 'MOT_TOOL', 'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_TIPO,TB_PRESTA', 'TB_CODIGO' );
              eEstudios : SetInfo( 'ESTUDIOS', 'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_TIPO', 'TB_CODIGO' );
              eTArt80 : SetInfo( 'T_ART_80', 'NU_CODIGO,TI_INICIO,TI_DESCRIP', 'NU_CODIGO,TI_INICIO' );
              eEstado : SetInfo( 'ENTIDAD', 'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO, TB_CURP,TB_STPS, TB_NIVEL0', 'TB_CODIGO' );
              eColonia: SetInfo( 'COLONIA', 'TB_CODIGO, TB_ELEMENT, TB_INGLES, TB_NUMERO, TB_TEXTO, TB_CODPOST, TB_CLINICA, TB_ZONA, TB_NIVEL0', 'TB_CODIGO' );
              eNivel1..eNivel9 {$ifdef ACS},eNivel10,eNivel11,eNivel12{$endif} : SetInfo( GetNombreTabla(eTabla), 'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_SUB_CTA,TB_ACTIVO'{$ifdef MSSQL}+',US_CODIGO'{$endif}{$ifdef ACS}+',TB_RELACIO'{$endif}+',TB_NIVEL0','TB_CODIGO');
              eMotAuto: SetInfo( 'MOT_AUTO', 'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_TIPO', 'TB_CODIGO' );
              eMotCheca: SetInfo('MOT_CHECA', 'TB_CODIGO,TB_ELEMENT, TB_INGLES, TB_NUMERO, TB_TEXTO', 'TB_CODIGO');
              eOcupaNac: SetInfo('OCUPA_NAC', 'TB_CODIGO,TB_ELEMENT, TB_INGLES, TB_NUMERO, TB_TEXTO', 'TB_CODIGO');
              eAreaTemCur: SetInfo('AR_TEM_CUR', 'TB_CODIGO,TB_ELEMENT, TB_INGLES, TB_NUMERO, TB_TEXTO', 'TB_CODIGO');
              eMunicipios: SetInfo('MUNICIPIO', 'TB_CODIGO,TB_ELEMENT, TB_INGLES, TB_NUMERO, TB_TEXTO,TB_ENTIDAD,TB_STPS', 'TB_CODIGO');
              eMotTransfer: SetINfo( 'MOT_TRANS',  'TB_CODIGO,TB_ELEMENT, TB_INGLES, TB_NUMERO, TB_TEXTO', 'TB_CODIGO');
              eTCompetencias :SetINfo( 'T_COMPETEN', 'TB_CODIGO,TB_ELEMENT, TB_INGLES, TB_NUMERO, TB_TEXTO', 'TB_CODIGO');
              eTPerfiles :SetINfo( 'T_PERFIL', 'TB_CODIGO,TB_ELEMENT, TB_INGLES, TB_NUMERO, TB_TEXTO', 'TB_CODIGO');
              eTNacComp : SetINfo( 'C_T_NACOMP', 'TN_CODIGO,TN_TITULO, TN_COMITE, TN_PROPOSI,TN_FEC_APR,TN_NIVEL,TN_ACT_NIV,TN_PERFIL,TN_CR_EVA,TN_AC_COMP', 'TN_CODIGO');
              eBancos :SetINfo( 'BANCO', 'TB_CODIGO,TB_ELEMENT,TB_NOMBRE,TB_SAT_BAN,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_ACTIVO', 'TB_CODIGO');
              eTipoContrato :SetINfo( 'TSATCONTRA','TB_CODIGO,TB_ELEMENT,TB_COD_SAT,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_ACTIVO', 'TB_CODIGO');
              eTipoJornada :SetINfo( 'TSATJORNAD','TB_CODIGO,TB_ELEMENT,TB_COD_SAT,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_ACTIVO', 'TB_CODIGO');
              eRiesgoPuesto :SetINfo( 'TSATRIESGO','TB_CODIGO,TB_ELEMENT,TB_COD_SAT,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_ACTIVO', 'TB_CODIGO');
			        eUma : SetInfo( 'VALOR_UMA', 'UM_FEC_INI,UM_VALOR,UM_FDESC', 'UM_FEC_INI' );
              eRegimenContraTrabaj : SetInfo( 'V_TIM_REG', 'TL_VALOR,TL_DESCRIP,TL_TEXTO,TL_INGLES,TL_NUMERO,TL_ACTIVO', 'TL_VALOR' );
          else
              SetInfo( GetNombreTabla(eTabla), 'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO', 'TB_CODIGO' );
          end;
     end;
end;

procedure TdmServerTablas.SetDetailInfo(eDetail: eTipoTabla);
begin
     with oZetaProvider.DetailInfo do
     begin
          case eDetail of
               eTArt80: SetInfoDetail( 'ART_80', 'NU_CODIGO,TI_INICIO,A80_LI,A80_CUOTA,A80_TASA', 'NU_CODIGO,TI_INICIO,A80_LI', 'NU_CODIGO,TI_INICIO' );
          end;
     end;
end;

function TdmServerTablas.GetNombreTabla( const eTabla : eTipoTabla ) : string;
begin
     case eTabla of
          eTipoCambio : Result := 'TCAMBIO';
          eSalMin : Result := 'SAL_MIN';
          eLeyIMSS : Result := 'LEY_IMSS';
          eNumericas : Result := 'NUMERICA';
          eGradosRiesgo : Result := 'RIESGP';
          eHabitacion : Result := 'VIVE_EN';
          eEstadoCivil : Result := 'EDOCIVIL';
          eViveCon : Result := 'VIVE_CON';
          eTransporte : Result := 'TRANSPOR';
          eExtra1 : Result := 'EXTRA1';
          eExtra2 : Result := 'EXTRA2';
          eExtra3 : Result := 'EXTRA3';
          eExtra4 : Result := 'EXTRA4';
          eEstado : Result := 'ENTIDAD';
          eTipoCursos : Result := 'TCURSO';
          //eEstudios : Result := 'ESTUDIOS';
          eMovKardex : Result := 'TKARDEX';
          eNivel1 : Result := 'NIVEL1';
          eNivel2 : Result := 'NIVEL2';
          eNivel3 : Result := 'NIVEL3';
          eNivel4 : Result := 'NIVEL4';
          eNivel5 : Result := 'NIVEL5';
          eNivel6 : Result := 'NIVEL6';
          eNivel7 : Result := 'NIVEL7';
          eNivel8 : Result := 'NIVEL8';
          eNivel9 : Result := 'NIVEL9';
          eIncidencias : Result := 'INCIDEN';
          eMotivoBaja : Result := 'MOT_BAJA';
          eTAhorro : Result := 'TAHORRO';
          eTPresta : Result := 'TPRESTA';
         // eMotAuto : Result := 'MOT_AUTO';
          eMonedas : Result := 'MONEDAS';
          eTallas : Result := 'TALLA';
          eClasifiCurso: Result := 'CCURSO';
          eColonia: Result := 'COLONIA';
          eExtra5 : Result := 'EXTRA5';
          eExtra6 : Result := 'EXTRA6';
          eExtra7 : Result := 'EXTRA7';
          eExtra8 : Result := 'EXTRA8';
          eExtra9 : Result := 'EXTRA9';
          eExtra10 : Result := 'EXTRA10';
          eExtra11 : Result := 'EXTRA11';
          eExtra12 : Result := 'EXTRA12';
          eExtra13 : Result := 'EXTRA13';
          eExtra14 : Result := 'EXTRA14';
          {$ifdef ACS}
          eNivel10 : Result := 'NIVEL10';
          eNivel11 : Result := 'NIVEL11';
          eNivel12 : Result := 'NIVEL12';
          {$endif}
          eOcupaNac: Result := 'OCUPA_NAC';
          eAreaTemCur : Result := 'AR_TEM_CUR';
          eMunicipios: Result := 'MUNICIPIO';
          eUma : Result := 'VALOR_UMA';
          eRegimenContraTrabaj:  Result := 'V_TIM_REG';
     end;
end;

{ ************ Interfases ( Paletitas ) ************* }

function TdmServerTablas.GetTabla(Empresa: OleVariant; iTabla: Integer): OleVariant;
begin
     try
        SetTablaInfo( eTipoTabla( iTabla ));
        Result := oZetaProvider.GetTabla( Empresa );
     except
        SetOLEVariantToNull( Result );
     end;
     SetComplete;
end;

// (JB) Se realiza la modificacion de esta funcion para poder almacenar en bitacora los cambios y eliminaciones
//      realizadas en las tablas de sistema. Esta modificacion sera a partir de 2011. Antes no se tenia esta funcionalidad.
//      Para mayor informacion reivsar tambien archivos de ZetaCommonLists.
function TdmServerTablas.GrabaTabla(Empresa: OleVariant; iTabla: Integer; oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Initglobales;
          FTipoTabla := eTipoTabla( iTabla );
          SetTablaInfo( FTipoTabla );
          TablaInfo.AfterUpdateRecord := GrabaCambiosBitacora;
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
     end;
     SetComplete;
end;

function TdmServerTablas.GetNumerica(Empresa: OleVariant; Codigo: Integer;
         out oArt80: OleVariant): OleVariant;
const
     K_FILTRO_NUMERICA = '( NU_CODIGO = %d )';
var
   sFiltroNumerica : String;
begin
     sFiltroNumerica := Format( K_FILTRO_NUMERICA, [ Codigo ] );
     // Tabla NUMERICA
     SetTablaInfo( eNumericas );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := sFiltroNumerica;
          Result := GetTabla( Empresa );
     end;
     // Master-Detail ART_80
     SetTablaInfo( eTArt80 );
     SetDetailInfo( eTArt80 );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := sFiltroNumerica;
          oArt80 := GetMasterDetail( Empresa );
     end;
     SetComplete;
end;

function TdmServerTablas.GrabaNumerica(Empresa, oDelta, oArt80: OleVariant;
         out ErrorCount, ErrorArt80: Integer; out oArt80Result: OleVariant): OleVariant;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     if ( not VarIsNull( oDelta ) ) then
     begin
          {*SetTablaInfo( eNumericas );*}
          // (JB)
          FTipoTabla := eNumericas;
          SetTablaInfo( FTipoTabla );

          oZetaProvider.TablaInfo.AfterUpdateRecord := GrabaCambiosBitacora;

          Result := oZetaProvider.GrabaTabla( Empresa, oDelta, ErrorCount );
     end
     else
         Result := oDelta;
     if ( ErrorCount = 0 ) and ( not VarIsNull( oArt80 ) ) then
     begin
          SetTablaInfo( eTArt80 );
          SetDetailInfo( eTArt80 );
          FTipoTabla := eTArt80;
          oZetaProvider.TablaInfo.AfterUpdateRecord := GrabaCambiosBitacora;
          oArt80Result := oZetaProvider.GrabaMasterDetail( Empresa, oArt80, ErrorArt80 );
     end
     else
         oArt80Result := oArt80;
     SetComplete;
end;

function TdmServerTablas.GetPrioridadDescuento( Empresa: OleVariant ): OleVariant;
const
     K_PRESTAMO = 'Pr�stamo';
     K_AHORRO  = 'Ahorro';
begin
     oZetaProvider.EmpresaActiva := Empresa;
     Result := oZetaProvider.OpenSQL( Empresa,
                                      Format( GetScript( qGetPrioridad ), [ EntreComillas( K_AHORRO ),
                                                                            EntreComillas( K_PRESTAMO ) ] ),
                                                                            TRUE );
     SetComplete;
end;

function TdmServerTablas.GrabaPrioridadDescuento(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
const
     K_PRESTAMO = 'P';
var
   Ahorro, Presta, oCursor: TZetaCursor;
   sCodigo: string;
   iPrioridad: integer;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Ahorro := CreateQuery( GetScript( qGrabaPrioridadAhorro ) );
          Presta := CreateQuery( GetScript( qGrabaPrioridadPresta ) );
          try
             EmpiezaTransaccion;
             try
                with cdsLista do
                begin
                     Lista := oDelta;
                     while not Eof do
                     begin
                          if ( FieldByName( 'TB_CLASE' ).AsString = K_PRESTAMO ) then
                             oCursor := Presta
                          else
                              oCursor := Ahorro;

                          iPrioridad := FieldByName( 'TB_PRIORID' ).AsInteger;
                          sCodigo := FieldByName( 'TB_CODIGO' ).AsString;

                          ParamAsInteger( oCursor, 'TB_PRIORID', iPrioridad );
                          ParamAsString( oCursor, 'TB_CODIGO', sCodigo );

                          Ejecuta( oCursor );
                          Next;
                     end;
                end;
                TerminaTransaccion( TRUE );
             except
                   on Error: Exception do
                   begin
                        TerminaTransaccion( FALSE );
                        ErrorCount := 1;
                   end;
             end;
          finally
                 FreeAndNil( Presta );
                 FreeAndNil( Ahorro );
          end;
     end;
     ZetaCommonTools.SetOLEVariantToNull( Result );
     SetComplete;
end;

// (JB) GrabaCambiosBitacora - Evento Realizado para la version 2011. Realizado despues del GrabaTabla para poder almacenar el bitacora los
//      cambios de las tablas importantes de sistema que anteriormente no se tenia registrado. Esto es �nicamente para las actualizaciones
//      y eliminaciones de los registros de dichas tablas.
procedure TdmServerTablas.GrabaCambiosBitacora(Sender: TObject;  SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;  UpdateKind: TUpdateKind);
var
     eClase: eClaseBitacora; // Clase de bitacora que se registra. Ver ZetaCommonLists
     sTitulo: String;        // Descripcion que se mostrara en Bitacora
     sKeyFieldName: String;  // Campo llave que indica el registro que se modific�
     sAdicional: String;     // Campo adicional para enviar a la bit�cora.

     // (JB) DatosBitacora - Procedimiento que permite configurar el Titulo que sera enviado a la bitacora.
     procedure DatosBitacora( eClaseCatalogo:eClaseBitacora; sDescripcion, sKeyField : string );
     var
        oField : TField;
     begin
          eClase  := eClaseCatalogo;
          sTitulo := sDescripcion;
          if StrLleno(sKeyField) then
          begin
               oField := DeltaDS.FieldByName( sKeyField );
               if eClaseCatalogo = clbNumericasTabla then
                    sTitulo := sTitulo + ' ' + FloatToStr( ZetaServerTools.CampoOldAsVar( oField ) )
               else if oField is TNumericField then
                    sTitulo := sTitulo + ' ' + IntToStr( ZetaServerTools.CampoOldAsVar( oField ) )
               else if oField is tDateTimeField then
                    sTitulo := sTitulo + ' ' + ZetaCommonTools.FechaCorta( ZetaServerTools.CampoOldAsVar( oField ) )
               else
                    sTitulo := sTitulo + ' ' + ZetaServerTools.CampoOldAsVar( oField );
          end;
     end;

     // (JB) Funcion que permite consultar la tabla CAMPO_AD para poder traer el nombre configurado de las tablas adicionales.
     function GetNombreAdicionales( NombreTabla : String ) : String;
     var
          FDatos: TZetaCursor;
     begin
          with oZetaProvider do
          begin
               FDatos := CreateQuery( Format ( GetScript( qGetNombreAdicional ) , [ EntreComillas( NombreTabla ) ] ) );
               try
                    //if AbreQueryScript(FDatos, Format ( GetScript( qGrabaPrioridadPresta ) , [ EntreComillas( NombreTabla ) ] ) ) then
                    FDatos.Active := True;
                    Result := FDatos.FieldByName( 'CX_TITULO' ).AsString;
                    FDatos.Active := False
               finally
                      FreeAndNil( FDatos );
               end;
          end;
     end;

begin
     if UpdateKind in [ ukModify, ukDelete ] then
     begin
          sAdicional := VACIO;
          eClase := clbNinguno;
          sKeyFieldName := 'TB_CODIGO';
          case FTipoTabla of
               //(JB) En el caso de las tablas de Salario Minimo, LeyIMSS, Numericas, Detalle de Numericas(Art80), y grados de riesgo
               //     se especifica el campo llave para poder enviarlo a la bitacora.
               eSalMin         :
                               begin
                                    sKeyFieldName := 'SM_FEC_INI';
                                    DatosBitacora( clbOficialesSalMin ,          'Salario M�nimo :', sKeyFieldName );
                               end;
               eLeyIMSS        :
                               begin
                                    sKeyFieldName := 'SS_INICIAL';
                                    DatosBitacora( clbOficialesCuotasIMSS ,      'Cuotas del IMSS :', sKeyFieldName );
                               end;
               eNumericas      :
                               begin
                                    sKeyFieldName := 'NU_CODIGO';
                                    DatosBitacora( clbNumericasGeneral ,         'Num�ricas Generales :', sKeyFieldName );
                               end;
               eTArt80         :
                               begin
                                    // (JB) Cambi� ya que el limite inferior quedara en el detalle de Bit�cora.
                                    sKeyFieldName := 'NU_CODIGO';

                                    // (JB) Datos Adicionales en Bitacora
                                    sAdicional := sAdicional + ' Vigencia : ' +        ZetaCommonTools.FechaCorta( ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'TI_INICIO' ) ) ) + CR_LF;
                                    // (JB) Fix Issue 3734 
                                    if DeltaDS.FindField('A80_LI') <> NIL then
                                         sAdicional := sAdicional + ' L�mite Inferior : ' + FloatToStr( ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'A80_LI' ) ) ) + CR_LF;

                                    DatosBitacora( clbNumericasTabla ,           'Num�ricas Tablas :', sKeyFieldName);
                               end;
               eGradosRiesgo   :
                               begin
                                    sKeyFieldName := 'RI_CLASE';
                                    DatosBitacora( clbOficialesGradosRiesgo ,    'Grado de Riesgo :', sKeyFieldName );
                               end;
               eEstado         :DatosBitacora( clbPersEstados ,              'Estado :', sKeyFieldName );
               eMovKardex      :DatosBitacora( clbHistTipoKardex ,           'Tipo de Kardex :', sKeyFieldName );
               //(JB) Para las tablas de nivel (area) es necesario mostrar en la descripcion de la bitacora el nombre que se tiene configurado.
               //     Para ello lo hacemos por medio de los stringsglobales y la funcion GetGlobalString de Tools/DZetaServerProvider.pas
               eNivel1         :DatosBitacora( clbPersArea1 ,                oZetaProvider.GetGlobalString( K_GLOBAL_NIVEL1 ) + ' :', sKeyFieldName );
               eNivel2         :DatosBitacora( clbPersArea2 ,                oZetaProvider.GetGlobalString( K_GLOBAL_NIVEL2 ) + ' :', sKeyFieldName );
               eNivel3         :DatosBitacora( clbPersArea3 ,                oZetaProvider.GetGlobalString( K_GLOBAL_NIVEL3 ) + ' :', sKeyFieldName );
               eNivel4         :DatosBitacora( clbPersArea4 ,                oZetaProvider.GetGlobalString( K_GLOBAL_NIVEL4 ) + ' :', sKeyFieldName );
               eNivel5         :DatosBitacora( clbPersArea5 ,                oZetaProvider.GetGlobalString( K_GLOBAL_NIVEL5 ) + ' :', sKeyFieldName );
               eNivel6         :DatosBitacora( clbPersArea6 ,                oZetaProvider.GetGlobalString( K_GLOBAL_NIVEL6 ) + ' :', sKeyFieldName );
               eNivel7         :DatosBitacora( clbPersArea7 ,                oZetaProvider.GetGlobalString( K_GLOBAL_NIVEL7 ) + ' :', sKeyFieldName );
               eNivel8         :DatosBitacora( clbPersArea8 ,                oZetaProvider.GetGlobalString( K_GLOBAL_NIVEL8 ) + ' :', sKeyFieldName );
               eNivel9         :DatosBitacora( clbPersArea9 ,                oZetaProvider.GetGlobalString( K_GLOBAL_NIVEL9 ) + ' :', sKeyFieldName );
               // (JB) Se acortan las descripciones de la bitacora ya que se cuenta unicamente con ciertos caracteres
               //      y muchas veces no se muestra el indice de la modificacion. De esta forma sigue siendo entendible
               //      y rastreable el cambio que queda en bitacora.
               eIncidencias    :DatosBitacora( clbHistIncidencias ,          'Incidencia :', sKeyFieldName );
               eMotivoBaja     :DatosBitacora( clbHistMotivoBaja ,           'Motivo de Baja :', sKeyFieldName );
               eTAhorro        :DatosBitacora( clbNomTipoAhorro ,            'Tipo de Ahorro :', sKeyFieldName );
               eTPresta        :DatosBitacora( clbNomTipoPrestamo ,          'Tipo de Prestamo :', sKeyFieldName );
               eMotAuto        :DatosBitacora( clbHistMotivosAut ,           'Motivo de Aut. :', sKeyFieldName );
               eMotCheca       :DatosBitacora( clbHistMotivosManCheck ,      'Motivo de Checada Man. :', sKeyFieldName );
               eOcupaNac       :DatosBitacora( clbOficialesCatNacOcup ,      'C�t. Nac. de Ocup. :', sKeyFieldName );
               eAreaTemCur     :DatosBitacora( clbOficialesAreasTemat ,      '�rea Tem�tica Cursos :', sKeyFieldName );
               eMunicipios     :DatosBitacora( clbPersMunicipios ,           'Municipio :', sKeyFieldName );
               //(JB) Para las tablas adicionales es necesario consultar el nombre que el usuario tiene configurado para poder
               //     almacenarlo en al descripcion de la bitacora. Esto se realiza con la funcion GetNombreAdicionales.
               eExtra1         :DatosBitacora(	clbPersAdicionales1 ,        GetNombreAdicionales( 'CB_G_TAB_1' ) + ' :', sKeyFieldName );
               eExtra2         :DatosBitacora(	clbPersAdicionales2 ,        GetNombreAdicionales( 'CB_G_TAB_2' ) + ' :', sKeyFieldName );
               eExtra3         :DatosBitacora(	clbPersAdicionales3 ,        GetNombreAdicionales( 'CB_G_TAB_3' ) + ' :', sKeyFieldName );
               eExtra4         :DatosBitacora(	clbPersAdicionales4 ,        GetNombreAdicionales( 'CB_G_TAB_4' ) + ' :', sKeyFieldName );
               eExtra5         :DatosBitacora(	clbPersAdicionales5 ,        GetNombreAdicionales( 'CB_G_TAB_5' ) + ' :', sKeyFieldName );
               eExtra6         :DatosBitacora(	clbPersAdicionales6 ,        GetNombreAdicionales( 'CB_G_TAB_6' ) + ' :', sKeyFieldName );
               eExtra7         :DatosBitacora(	clbPersAdicionales7 ,        GetNombreAdicionales( 'CB_G_TAB_7' ) + ' :', sKeyFieldName );
               eExtra8         :DatosBitacora(	clbPersAdicionales8 ,        GetNombreAdicionales( 'CB_G_TAB_8' ) + ' :', sKeyFieldName );
               eExtra9         :DatosBitacora(	clbPersAdicionales9 ,        GetNombreAdicionales( 'CB_G_TAB_9' ) + ' :', sKeyFieldName );
               eExtra10        :DatosBitacora(	clbPersAdicionales10 ,       GetNombreAdicionales( 'CB_G_TAB_10' ) + ' :', sKeyFieldName );
               eExtra11        :DatosBitacora(	clbPersAdicionales11 ,       GetNombreAdicionales( 'CB_G_TAB_11' ) + ' :', sKeyFieldName );
               eExtra12        :DatosBitacora(	clbPersAdicionales12 ,       GetNombreAdicionales( 'CB_G_TAB_12' ) + ' :', sKeyFieldName );
               eExtra13        :DatosBitacora(	clbPersAdicionales13 ,       GetNombreAdicionales( 'CB_G_TAB_13' ) + ' :', sKeyFieldName );
               eExtra14        :DatosBitacora(	clbPersAdicionales14 ,       GetNombreAdicionales( 'CB_G_TAB_14' ) + ' :', sKeyFieldName );
               eTCompetencias  :DatosBitacora(  clbTCompetencias ,           'Tipo de Competencia:', sKeyFieldName );
               eTPerfiles      :DatosBitacora(  clbTPerfiles ,               'Tipo de Perfiles:', sKeyFieldName );
               eTNacComp       :DatosBitacora(  clbTCatNacComp ,             'C�t. Nac.Competencias:', 'TN_CODIGO' );
               eBancos         :DatosBitacora(  clbBancos ,               'Banco:', sKeyFieldName );
               eUma            :
                                begin
                                     sKeyFieldName := 'UM_FEC_INI';
                                     DatosBitacora( clbUma ,          'Unidad de Medida y Actualizaci�n:', sKeyFieldName );
                                end;
               eTipoContrato   :DatosBitacora(  clbTipoContratoSAT ,               'Tipo Contrato SAT:', sKeyFieldName );
               eTipoJornada    :DatosBitacora(  clbTipoJornadaSAT ,               'Tipo Jornada SAT:', sKeyFieldName );
               eRiesgoPuesto    :DatosBitacora(  clbTipoJornadaSAT ,               'Riesgo Puesto SAT:', sKeyFieldName );
          end;

          with oZetaProvider do
            if UpdateKind = ukModify then
                 CambioCatalogo( sTitulo, eClase, DeltaDS, 0, sAdicional )
            else
                 BorraCatalogo( sTitulo, eClase, DeltaDS, 0, sAdicional );
     end;
end;
function TdmServerTablas.GetUMA( Empresa: OleVariant ): OleVariant;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     Result := oZetaProvider.OpenSQL( Empresa, GetScript( qGetValUma ),TRUE );
     SetComplete;
end;
{$ifndef DOS_CAPAS}


initialization
  TComponentFactory.Create(ComServer, TdmServerTablas, Class_dmServerTablas, ciMultiInstance, ZetaServerTools.GetThreadingModel);
{$endif}
end.
