library Asistencia;

uses
  ComServ,
  Asistencia_TLB in 'Asistencia_TLB.pas',
  DServerAsistencia in 'DServerAsistencia.pas' {dmServerAsistencia: TMtsDataModule} {dmServerAsistencia: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
