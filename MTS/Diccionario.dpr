library Diccionario;

uses
  ComServ,
  Diccionario_TLB in 'Diccionario_TLB.pas',
  DServerDiccionario in 'DServerDiccionario.pas' {dmServerDiccionario: TMtsDataModule} {dmServerDiccionario: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
