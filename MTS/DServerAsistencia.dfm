object dmServerAsistencia: TdmServerAsistencia
  OldCreateOrder = False
  OnCreate = dmServerAsistenciaCreate
  OnDestroy = dmServerAsistenciaDestroy
  OnActivate = dmServerAsistenciaCreate
  Pooled = False
  Height = 472
  Width = 741
  object cdsLista: TServerDataSet
    Aggregates = <>
    FetchOnDemand = False
    Params = <>
    Left = 40
    Top = 24
  end
  object cdsErroresAuto: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 112
    Top = 24
    object cdsErroresAutoCB_CODIGO: TIntegerField
      FieldName = 'CB_CODIGO'
    end
    object cdsErroresAutoAU_FECHA: TDateTimeField
      FieldName = 'AU_FECHA'
    end
    object cdsErroresAutoCH_TIPO: TSmallintField
      FieldName = 'CH_TIPO'
    end
    object cdsErroresAutoHORAS: TFloatField
      FieldName = 'HORAS'
    end
    object cdsErroresAutoCH_HOR_DES: TFloatField
      FieldName = 'CH_HOR_DES'
    end
    object cdsErroresAutoUS_COD_OK: TIntegerField
      FieldName = 'US_COD_OK'
    end
  end
  object cdsASCII: TServerDataSet
    Aggregates = <>
    FetchOnDemand = False
    Params = <>
    Left = 64
    Top = 104
  end
end
