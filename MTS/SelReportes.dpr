library SelReportes;

uses
  ComServ,
  SelReportes_TLB in 'SelReportes_TLB.pas',
  DServerSelReportes in 'DServerSelReportes.pas' {dmServerSelReportes: TMtsDataModule} {dmServerSelReportes: CoClass},
  DEntidadesTress in '..\Seleccion\DEntidadesTress.pas' {dmEntidadesTress: TDataModule};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
