library Imss;

uses
  ComServ,
  Imss_TLB in 'Imss_TLB.pas',
  DServerImss in 'DServerImss.pas' {dmServerIMSS: TMtsDataModule} {dmServerImss: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
