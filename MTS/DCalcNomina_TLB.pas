unit DCalcNomina_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 2/1/2012 2:21:43 PM from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3win_20_Vsn_2012_Mejoras\MTS\DCalcNomina.tlb (1)
// LIBID: {54C4A326-A9AF-11D3-8E51-0050DA04EAA0}
// LCID: 0
// Helpfile: 
// HelpString: DCalcNomina Library
// DepndLst: 
//   (1) v1.0 Midas, (C:\Windows\System32\midas.dll)
//   (2) v2.0 stdole, (C:\Windows\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, Midas, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  DCalcNominaMajorVersion = 1;
  DCalcNominaMinorVersion = 0;

  LIBID_DCalcNomina: TGUID = '{54C4A326-A9AF-11D3-8E51-0050DA04EAA0}';

  IID_IdmServerCalcNomina: TGUID = '{54C4A327-A9AF-11D3-8E51-0050DA04EAA0}';
  CLASS_dmServerCalcNomina: TGUID = '{54C4A329-A9AF-11D3-8E51-0050DA04EAA0}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerCalcNomina = interface;
  IdmServerCalcNominaDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerCalcNomina = IdmServerCalcNomina;


// *********************************************************************//
// Interface: IdmServerCalcNomina
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {54C4A327-A9AF-11D3-8E51-0050DA04EAA0}
// *********************************************************************//
  IdmServerCalcNomina = interface(IAppServer)
    ['{54C4A327-A9AF-11D3-8E51-0050DA04EAA0}']
    function CalculaNomina(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function RastreaConcepto(Empresa: OleVariant; Parametros: OleVariant; out Rastreo: OleVariant): OleVariant; safecall;
    function CalculaNetoBruto(Empresa: OleVariant; var Parametros: OleVariant): OleVariant; safecall;
    function CalculaPreNomina(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function Poll(Empresa: OleVariant; Parametros: OleVariant; ListaASCII: OleVariant): OleVariant; safecall;
    function ProcesarTarjetasPendientes(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function RecalcularTarjetas(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function RegistrosAutomaticos(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GeneraSQL(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant; 
                       out Error: WideString): OleVariant; safecall;
    function Transferencia(Empresa: OleVariant; EmpresaDestino: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function PollTotal(Parametros: OleVariant; var Lista: OleVariant): OleVariant; safecall;
    function EvaluaParam(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant; 
                         out Error: WideString): WordBool; safecall;
    function CalculaRetroactivo(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CalculaRetroactivoGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CalculaRetroactivoLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function RecalcularTarjetasSimple(Empresa: OleVariant; Parametros: OleVariant): WideString; safecall;
    function ProcesarTarjetasSimple(Empresa: OleVariant; Parametros: OleVariant): WideString; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerCalcNominaDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {54C4A327-A9AF-11D3-8E51-0050DA04EAA0}
// *********************************************************************//
  IdmServerCalcNominaDisp = dispinterface
    ['{54C4A327-A9AF-11D3-8E51-0050DA04EAA0}']
    function CalculaNomina(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 1;
    function RastreaConcepto(Empresa: OleVariant; Parametros: OleVariant; out Rastreo: OleVariant): OleVariant; dispid 2;
    function CalculaNetoBruto(Empresa: OleVariant; var Parametros: OleVariant): OleVariant; dispid 3;
    function CalculaPreNomina(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 4;
    function Poll(Empresa: OleVariant; Parametros: OleVariant; ListaASCII: OleVariant): OleVariant; dispid 5;
    function ProcesarTarjetasPendientes(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 6;
    function RecalcularTarjetas(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 7;
    function RegistrosAutomaticos(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 8;
    function GeneraSQL(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant; 
                       out Error: WideString): OleVariant; dispid 9;
    function Transferencia(Empresa: OleVariant; EmpresaDestino: OleVariant; Parametros: OleVariant): OleVariant; dispid 10;
    function PollTotal(Parametros: OleVariant; var Lista: OleVariant): OleVariant; dispid 11;
    function EvaluaParam(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant; 
                         out Error: WideString): WordBool; dispid 12;
    function CalculaRetroactivo(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 13;
    function CalculaRetroactivoGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 14;
    function CalculaRetroactivoLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 15;
    function RecalcularTarjetasSimple(Empresa: OleVariant; Parametros: OleVariant): WideString; dispid 301;
    function ProcesarTarjetasSimple(Empresa: OleVariant; Parametros: OleVariant): WideString; dispid 302;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer; 
                             out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                           Options: Integer; const CommandText: WideString; var Params: OleVariant; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerCalcNomina provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerCalcNomina exposed by              
// the CoClass dmServerCalcNomina. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerCalcNomina = class
    class function Create: IdmServerCalcNomina;
    class function CreateRemote(const MachineName: string): IdmServerCalcNomina;
  end;

implementation

uses ComObj;

class function CodmServerCalcNomina.Create: IdmServerCalcNomina;
begin
  Result := CreateComObject(CLASS_dmServerCalcNomina) as IdmServerCalcNomina;
end;

class function CodmServerCalcNomina.CreateRemote(const MachineName: string): IdmServerCalcNomina;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerCalcNomina) as IdmServerCalcNomina;
end;

end.
