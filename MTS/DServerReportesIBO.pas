unit DServerReportesIBO;


{$INCLUDE DEFINES.INC}
interface
Uses Windows, Messages, SysUtils, Classes,Controls,
     DB,
     Variants,
     DZetaServerProvider,
     ZetaCommonClasses,
     ZAgenteSQLClient,
     ZAgenteSQL,
     ZCreator,
     ZetaSQLBroker,
     ZetaServerDataSet,
     DServerReporteador;


type
    TdmServerReportes = Class(TdmServerReporteador)
    private
      { Private declarations }
       FEstadoCuenta : TZetaCursor;
       {Variables para el Reporte de  Listado de Totales}
       GRUPO1,GRUPO2,GRUPO3,GRUPO4,GRUPO5 : string;
       PE_YEAR,PE_TIPO,PE_NUMERO : integer;
       FNominaTotales : TDataset;
       {Terminan Variables para el Reporte de  Listado de Totales}

       oSQLBroker : TSQLBroker;
       FImagen : TZetaCursor;

       { **** CR:1777, se tuvieron que subir las variables, para que los 2 reportes de Balanza los utilicen ****** }
       Conceptos : TStrings;
       FConceptosAgregado: TStrings;
       MovimienBalanzaMensual : TSQLBroker;
       FPeriodoBalanza: TZetaCursor;
       FDatasetNOMM, FSP_Balanza, FBalanza: TZetaCursor;
       FUltimoPeriodo: TDatosPeriodo;



       property SQLBroker: TSQLBroker read oSQLBroker;
       procedure InitBroker;
       procedure ClearBroker;

       procedure PreparaAgenteTotalesLista(oSQLAgente: TSQLAgente);
       procedure AgregaRegistros( const iCuantos, CB_CODIGO,US_CODIGO: integer; const TE_CARGO, TE_ABONO, TE_SALDO: Double;
                                  const TE_DESCRIP, TE_TIPO, TE_REFEREN: string;
                                  const TE_FECHA: TDate);
       function CalculaBalanza(oSQLAgente: TSQLAgente;var Error: string): TSQLAgente;
       function CalculaBalanzaMensualReporte(oSQLAgente: TSQLAgente;var Error: string ): TSQLAgente;
       function CalculaCalendario(oSQlAgente: TSQLAgente;var Error: string): TSQLAgente;
       function CalculaCalendarioHoras(oSQlAgente: TSQLAgente;var Error: string): TSQLAgente;
       function CalculaConciliacion(oSQLAgente: TSQLAgente;var Error: string): TSQLAgente;
       function CalculaCursoProg(oSQLAgente: TSQLAgente;var Error: string): TSQLAgente;
       function CalculaDemografica(oSQLAgente: TSQLAgente;var Error: string): TSQLAgente;
       function CalculaEstadoAhorro(oSQLAgente: TSQLAgente;var Error: string): TSQLAgente;
       function CalculaEstadoPrestamo(oSQLAgente: TSQLAgente;var Error: string): TSQLAgente;
       function CalculaListadoTotales(oSQLAgente: TSQLAgente; var Error: string): TSQLAgente;
       function CalculaListadoNomina(oSQLAgente: TSQLAgente; var Error: string): TSQLAgente;
       function CalculaRotacion(oSQLAgente: TSQLAgente;var Error: string): TSQLAgente;
       function CalculaConteoPersonal(oSQLAgente: TSQLAgente;var Error: string): TSQLAgente;
       {$ifdef INTERBASE}
       function Unionizer(const sSQL, sTabla1, sTabla2,sTabla3: String): String;
       {$endif}

       procedure AgregandoRelacion(Sender:TObject);
       procedure EvaluaColumnas(DataSet: TZetaCursor; oColumna: TSQLColumna;oCampo: TField);
       procedure BorraTabla(const sTabla, sUserCampo: string);

       {***** CR 1777 : Recibo Mensual. Alias Reporte de Balanza de Movimientos Mensual ****}
       procedure CalculaBalanzaMensual( oSQLAgente: TSQLAgente; oDatosPeriodo:TDatosPeriodo; var iCuantos: integer; var Error: string );
       function InitBalanzaMensual(oSQLAgente: TSQLAgente; var Error: string): Boolean;
       procedure AgregaConceptos( MovimienBroker: TSQLBroker; const sFiltro: string );
       procedure AgregaRegistroBalanza( DataSet : TServerDataset; const iColumna,iEmpleado : integer; const iNumeroPeriodo: integer = 0 );
       procedure AgregaRegistroBalanzaPeriodo( DataSet : TServerDataset; const iColumna,iEmpleado : integer; const iNumeroPeriodo: integer = 0 );
       procedure AgregaRegistroBalanzaMensual( DataSet : TServerDataset; const iColumna,iEmpleado : integer; const iNumeroPeriodo: integer = 0 );

       //procedure DebugDialog(const sMensaje: string);
       function GetFiltroTipoConceptosBalanza: string;

    protected
       function GeneraSQLEspecial(oSQLAgente: TSQLAgente;var Error: string): TSQLAgente;override;
       procedure DespuesDeConstruyeSuperSQL(FAgenteReporte,oSQLAgente: TSQLAgente);override;
       procedure AsignaParametros;override;
       procedure AlInicializarParametros;override;
    public
          constructor Create(oProvider : TdmZetaServerProvider ; oCreator : TZetaCreator);
          destructor Destroy;override;
    end;
implementation
uses Mask,
     ZetaServerTools,
     ZetaEntidad,
     ZGlobalTress,
     ZetaTipoEntidad,
     ZetaCommonTools,
     ZetaCommonLists,
     ZReportConst,
     DReportesEspeciales,
     DConteoPersonal;

const

     Q_DEMO_INSERT = 'INSERT INTO TMPDEMO ( TD_USER, '+
                      'TD_GRUPO,TD_DESCRIP,TD_EMPL,TD_EMPMIN,TD_EMPMAX,                         '+
                      'TD_HOMBRES,TD_MUJERES,TD_FAMILIA,TD_MICA,TD_ESTUDIO,TD_IDIOMA,            '+
                      'TD_TABULA,TD_EDADMIN,TD_EDADMAX,TD_EDADAVG,TD_ANTMIN,TD_ANTMAX,           '+
                      'TD_ANTAVG,TD_RESMIN,TD_RESMAX,TD_RESAVG,TD_HIJOMIN,TD_HIJOMAX,            '+
                      'TD_HIJOAVG,TD_SALMIN,TD_SALMAX,TD_SALAVG,TD_DIASMIN,TD_DIASMAX,TD_DIASAVG )'+
                      'VALUES( :TD_USER, :TD_GRUPO,:TD_DESCRIP,:TD_EMPL,:TD_EMPMIN,:TD_EMPMAX,:TD_HOMBRES, '+
                      ':TD_MUJERES,:TD_FAMILIA,:TD_MICA,:TD_ESTUDIO,:TD_IDIOMA,:TD_TABULA,'+
                      ':TD_EDADMIN,:TD_EDADMAX,:TD_EDADAVG,:TD_ANTMIN,:TD_ANTMAX,:TD_ANTAVG,'+
                      ':TD_RESMIN,:TD_RESMAX,:TD_RESAVG,:TD_HIJOMIN,:TD_HIJOMAX,:TD_HIJOAVG,'+
                      ':TD_SALMIN,:TD_SALMAX,:TD_SALAVG,:TD_DIASMIN,:TD_DIASMAX,:TD_DIASAVG )';
      Q_ROTA_INSERT = 'INSERT INTO TMPROTAI ( TR_USER, TR_GRUPO, TR_DESCRIP, CB_CODIGO, CB_FEC_ING, CB_FEC_BAJ ) ' +
                      'VALUES( :TR_USER, :TR_GRUPO, :TR_DESCRIP, :CB_CODIGO, :CB_FEC_ING, :CB_FEC_BAJ ) ';
      Q_ROTA_SP     = 'EXECUTE PROCEDURE SP_TOTALES_ROTACION ( %d, %s, %s )';
      Q_BALANZA_INSERT = 'INSERT INTO TMPBALAN ( TZ_USER, TZ_CODIGO, TZ_NUM_PER, TZ_DES_PER, TZ_MON_PER, TZ_REF_PER, TZ_NUM_DED, TZ_DES_DED, TZ_MON_DED, TZ_REF_DED, CB_CODIGO, PE_YEAR, PE_TIPO, PE_NUMERO ) ' +
                         'VALUES ( :TZ_USER, :TZ_CODIGO, :TZ_NUM_PER, :TZ_DES_PER, :TZ_MON_PER, :TZ_REF_PER, :TZ_NUM_DED, :TZ_DES_DED, :TZ_MON_DED, :TZ_REF_DED, :CB_CODIGO, :PE_YEAR, :PE_TIPO, :PE_NUMERO )';
      {$ifdef FALSE}
      Q_BALANZA_MOVIMIEN = 'SELECT MOVIMIEN.CB_CODIGO, ' +
                           '       MOVIMIEN.PE_TIPO, ' +
                           '       MOVIMIEN.PE_YEAR, ' +
                           '       MOVIMIEN.PE_NUMERO, ' +
                           '       MOVIMIEN.MO_PERCEPC + MOVIMIEN.MO_DEDUCCI AS MO_MONTO, ' +
                           '       MOVIMIEN.MO_REFEREN, ' +
                           '       CONCEPTO.CO_NUMERO, ' +
                           '       CONCEPTO.CO_DESCRIP, ' +
                           '       CONCEPTO.CO_TIPO ' +
                           'FROM MOVIMIEN ' +
                           'LEFT OUTER JOIN CONCEPTO ON CONCEPTO.CO_NUMERO = MOVIMIEN.CO_NUMERO ' +
                           'WHERE ( MOVIMIEN.CB_CODIGO = :EMPLEADO ) AND ' +
                           '      ( MOVIMIEN.PE_YEAR = :YEAR ) AND ' +
                           '      ( MOVIMIEN.PE_TIPO = :TIPO ) AND ' +
                           '      ( MOVIMIEN.PE_NUMERO = :NUMERO ) AND ' +
                           '      ( MOVIMIEN.MO_ACTIVO = ''S'' ) AND ' +
                           '      ( CONCEPTO.CO_TIPO IN (1,2,3,4) ) %s ' +
                           ' ORDER BY MOVIMIEN.CB_CODIGO,' +
                           '          MOVIMIEN.CO_NUMERO ';
      {$endif}
      Q_SP_BALANZA = 'EXECUTE PROCEDURE SP_BALANZA( '+
                     ':USUARIO, '+
                     ':YEAR, '+
                     ':TIPO, '+
                     ':NUMERO, '+
                     ':EMPLEADO, '+
                     ':CONCEPTO, '+
                     ':COLUMNA, '+
                     ':DESCRIPCION, '+
                     ':MONTO, '+
                     ':HORAS, '+
                     ':REFERENCIA) ';

      //** CR 1777: Recibo Mensual
      //** **************************************************
      //** Scripts para el reporte de Balanza Mensual

      Q_SP_BALANZA_MENSUAL  = 'EXECUTE PROCEDURE SP_BALANZAMENSUAL( '+
                     ':USUARIO, '+
                     ':YEAR, '+
                     ':TIPO, '+
                     ':NUMERO, '+
                     ':EMPLEADO, '+
                     ':CONCEPTO, '+
                     ':COLUMNA, '+
                     ':DESCRIPCION, '+
                     ':MONTO, '+
                     ':HORAS, '+
                     ':REFERENCIA ) ';

      Q_BALANZA_MENSUAL_INSERT = 'INSERT INTO TMPBALAMES ( TM_USER, TM_CODIGO, TM_NUM_PER, TM_DES_PER, TM_MON_PER, TM_REF_PER, TM_NUM_DED, TM_DES_DED, TM_MON_DED, TM_REF_DED, CB_CODIGO, PE_YEAR, PE_TIPO, PE_NUMERO ) ' +
                                 'VALUES ( :TM_USER, :TM_CODIGO, :TM_NUM_PER, :TM_DES_PER, :TM_MON_PER, :TM_REF_PER, :TM_NUM_DED, :TM_DES_DED, :TM_MON_DED, :TM_REF_DED, :CB_CODIGO, :PE_YEAR, :PE_TIPO, :PE_NUMERO )';


      {$ifdef INTERBASE}
      Q_ESTADO_AHORRO_INSERT = 'execute procedure INSERT_ESTADO_AHORRO( ' +
                               ':TE_CODIGO, :CB_CODIGO, :TE_TIPO, ' +
                               ':TE_FECHA, :TE_DESCRIP, :TE_CARGO, :TE_ABONO, :TE_SALDO, :US_CODIGO, :TE_USER, :TE_REFEREN)';
      {$endif}

      {$ifdef MSSQL}
      Q_ESTADO_AHORRO_INSERT = 'insert into TMPESTAD '+
                               '(TE_CODIGO, CB_CODIGO, TE_TIPO, '+
                               'TE_FECHA, TE_DESCRIP, TE_CARGO, TE_ABONO, '+
                               'TE_SALDO, US_CODIGO, TE_USER, TE_REFEREN ) '+
                               'values (:TE_CODIGO, :CB_CODIGO, :TE_TIPO, '+
                               ':TE_FECHA, :TE_DESCRIP, :TE_CARGO, :TE_ABONO, '+
                               ':TE_SALDO, :US_CODIGO, :TE_USER, :TE_REFEREN) ';
      {$endif}

      Q_MOVIMIENTOS = 'SELECT '+
                      'TMPESTAD.CB_CODIGO, '+
                      'TMPESTAD.TE_TIPO, '+
                      'TMPESTAD.TE_REFEREN, '+
                      'PERIODO.PE_FEC_INI PE_FEC_INI, '+
                      'MOVIMIEN.MO_PERCEPC MO_PERCEPC, '+
                      'MOVIMIEN.MO_DEDUCCI MO_DEDUCCI, '+
                      'MOVIMIEN.US_CODIGO, '+
                      'NOMINA.PE_YEAR, '+
                      'NOMINA.PE_TIPO, '+
                      'NOMINA.PE_NUMERO, '+
                      'MOVIMIEN.CO_NUMERO, '+
                      '''M'' AS TIPO '+
                      'FROM TMPESTAD '+
                      'LEFT OUTER JOIN NOMINA ON NOMINA.CB_CODIGO = TMPESTAD.CB_CODIGO '+
                      'LEFT OUTER JOIN PERIODO ON PERIODO.PE_YEAR = NOMINA.PE_YEAR AND PERIODO.PE_TIPO = 	NOMINA.PE_TIPO AND PERIODO.PE_NUMERO = NOMINA.PE_NUMERO  '+
                      'LEFT OUTER JOIN MOVIMIEN ON NOMINA.PE_YEAR = MOVIMIEN.PE_YEAR AND NOMINA.PE_TIPO = 	MOVIMIEN.PE_TIPO AND NOMINA.PE_NUMERO = MOVIMIEN.PE_NUMERO AND NOMINA.CB_CODIGO = MOVIMIEN.CB_CODIGO '+
                      'WHERE ( TMPESTAD.TE_USER = %d) '+
                      'AND ( TMPESTAD.US_CODIGO = -1 ) '+
                      'AND  ( MOVIMIEN.CO_NUMERO = TMPESTAD.TE_CARGO ) '+
                      'AND  ( NOMINA.NO_STATUS=6 ) '+
                      'AND  ( MOVIMIEN.MO_REFEREN= TMPESTAD.TE_REFEREN ) '+
                      //'AND ( MOVIMIEN.MO_PERCEPC+MOVIMIEN.MO_DEDUCCI)>0 ' +
                      'AND ( MOVIMIEN.MO_ACTIVO = ''S'') ' +
                      'AND  ( PERIODO.PE_FEC_FIN >= TMPESTAD.TE_FECHA ) ';
      Q_PCAR_ABO= ' UNION ALL '+
                  'SELECT '+
                  'TMPESTAD.CB_CODIGO, '+
                  'TMPESTAD.TE_TIPO, '+
                  'TMPESTAD.TE_REFEREN, '+
                  'PCAR_ABO.CR_FECHA PE_FEC_INI, '+
                  'PCAR_ABO.CR_CARGO MO_PERCEPC, '+
                  'PCAR_ABO.CR_ABONO MO_DEDUCCI, '+
                  'PCAR_ABO.US_CODIGO, '+
                  'CAST(0 AS SMALLINT) PE_YEAR, '+
                  'CAST(0 AS SMALLINT) PE_TIPO, '+
                  'CAST(0 AS SMALLINT) PE_NUMERO, '+
                  'CAST(0 AS SMALLINT) CO_NUMERO, '+
                  '''P'' AS TIPO '+
                  'FROM TMPESTAD '+
                  'LEFT OUTER JOIN PCAR_ABO ON PCAR_ABO.CB_CODIGO = TMPESTAD.CB_CODIGO '+
                  'AND PCAR_ABO.PR_TIPO = TMPESTAD.TE_TIPO '+
                  'AND PCAR_ABO.PR_REFEREN = TMPESTAD.TE_REFEREN '+
                  'WHERE TMPESTAD.TE_USER = %d '+
                  'AND TMPESTAD.US_CODIGO = -1 '+
                  'AND (PCAR_ABO.CR_CARGO +PCAR_ABO.CR_ABONO)>0 '+
                  'AND TMPESTAD.TE_FECHA <= ''%s'' '+
                  'ORDER BY 1,2,3,4 ';

      Q_ACAR_ABO= ' UNION ALL '+
                  'SELECT '+
                  'TMPESTAD.CB_CODIGO, '+
                  'TMPESTAD.TE_TIPO, '+
                  'TMPESTAD.TE_REFEREN, '+
                  'ACAR_ABO.CR_FECHA PE_FEC_INI, '+
                  'ACAR_ABO.CR_CARGO MO_PERCEPC, '+
                  'ACAR_ABO.CR_ABONO MO_DEDUCCI, '+
                  'ACAR_ABO.US_CODIGO, '+
                  'CAST(0 AS SMALLINT) PE_YEAR, '+
                  'CAST(0 AS SMALLINT) PE_TIPO, '+
                  'CAST(0 AS SMALLINT) PE_NUMERO, '+
                  'CAST(0 AS SMALLINT) CO_NUMERO, '+
                  '''A'' AS TIPO '+
                  'FROM TMPESTAD '+
                  'LEFT OUTER JOIN ACAR_ABO ON ACAR_ABO.CB_CODIGO = TMPESTAD.CB_CODIGO '+
                  'AND ACAR_ABO.AH_TIPO = TMPESTAD.TE_TIPO '+
                  'WHERE TMPESTAD.TE_USER = %d '+
                  'AND TMPESTAD.US_CODIGO = -1 '+
                  'AND (ACAR_ABO.CR_CARGO +ACAR_ABO.CR_ABONO)>0 '+
                  'AND TMPESTAD.TE_FECHA <= ''%s'' '+
                  'ORDER BY 1,2,3,4 ';

      Q_MAX_PERIODO = 'SELECT MAX(PERIODO.PE_FEC_FIN) MAXIMO '+
                      'FROM PERIODO '+
                      'WHERE PE_STATUS = 6 ';
      {$ifdef INTERBASE}
      Q_SP_CALENDARIO = 'SELECT CUANTOS FROM  PREPARA_CALENDARIO( '+
                        ':USUARIO, '+
                        ':EMPLEADO, '+
                        ':YEAR, '+
                        ':MONTH, '+
                        ':FECHA_MES, '+
                        ':FECHA_INI, '+
                        ':FECHA_FIN ) ';
      {$endif}
      {$ifdef MSSQL}
      Q_SP_CALENDARIO = 'execute procedure PREPARA_CALENDARIO( '+
                        ':USUARIO, '+
                        ':EMPLEADO, '+
                        ':YEAR, '+
                        ':MONTH, '+
                        ':FECHA_MES, '+
                        ':FECHA_INI, '+
                        ':FECHA_FIN ) ';
      {$endif}

      {$ifdef INTERBASE}
      Q_SP_CALENDARIO_HORAS = 'SELECT CUANTOS FROM  PREPARA_CALENDARIO_HORAS( '+
                              ':USUARIO, '+
                              ':EMPLEADO, '+
                              ':YEAR, '+
                              ':MONTH, '+
                              ':FECHA_MES, '+
                              ':FECHA_INI, '+
                              ':FECHA_FIN, '+
                              ':CAMPO, '+
                              ':PRIMERO )';
      {$endif}
      {$ifdef MSSQL}
      Q_SP_CALENDARIO_HORAS = 'execute procedure PREPARA_CALENDARIO_HORAS( '+
                              ':USUARIO, '+
                              ':EMPLEADO, '+
                              ':YEAR, '+
                              ':MONTH, '+
                              ':FECHA_MES, '+
                              ':FECHA_INI, '+
                              ':FECHA_FIN, '+
                              ':CAMPO, '+
                              ':PRIMERO )';
      {$endif}
{------------------------------------------------------------------}
{------------------------------------------------------------------}
      //SQL PARA CONCILIACION DE FOLIOS
      Q_CONCILIA_EXTREMOS = ' SELECT MIN(NO_FOLIO_%d) MINIMO, MAX(NO_FOLIO_%d) MAXIMO, '+
                   ' COUNT(*) CUANTOS '+
                   ' FROM NOMINA '+
                   ' WHERE PE_YEAR = %d '+
                   ' AND PE_TIPO = %d '+
                   ' AND PE_NUMERO = %d '+
                   ' AND NO_FOLIO_%d >0 ';
      Q_CONCILIA_INSERT = 'INSERT INTO TMPFOLIO '+
                     '(TO_USER,TO_FOLIO,CB_CODIGO,PE_YEAR,PE_TIPO,PE_NUMERO) '+
                     'VALUES (:TO_USER,:TO_FOLIO,:CB_CODIGO,:PE_YEAR,:PE_TIPO,:PE_NUMERO) ';
      Q_CONCILIA_NO_EXISTEN = 'INSERT INTO TMPFOLIO '+
                     '(TO_USER,TO_FOLIO,CB_CODIGO,PE_YEAR,PE_TIPO,PE_NUMERO) ' +
                     'SELECT %d, RESULTADO, '+
                     '0, '+
                     '%d,%d,%d '+
                     'FROM LISTA_ENTEROS(%d,%d) '+
                     'WHERE RESULTADO NOT IN ( '+
                     'SELECT NO_FOLIO_%d FROM NOMINA '+
                     'WHERE PE_YEAR = %d AND '+
                     'PE_TIPO = %d AND '+
                     'PE_NUMERO = %d )';

       Q_CONCILIA_SELECT = 'SELECT '+
                  'TMPFOLIO.TO_FOLIO, %s '+
                  'FROM TMPFOLIO %s '+
                  'WHERE (TMPFOLIO.TO_USER = %d) %s %s';
{------------------------------------------------------------------}
{------------------------------------------------------------------}
       //SQL PARA LISTADO DE TOTALES DE NOMINA
       TA_NIVEL = 'TA_NIVEL';

       {Es la lista de campos de Nomina que estan en TmpNom}
       Q_TOTALES_NOMCAMPOS =
       ' CB_SAL_INT,CB_SALARIO,NO_DIAS,NO_ADICION,NO_DEDUCCI, '+
       'NO_NETO,NO_DES_TRA,NO_DIAS_AG,NO_DIAS_VA,NO_DOBLES, '+
       'NO_EXTRAS,NO_FES_PAG,NO_FES_TRA,NO_HORA_CG,NO_HORA_SG, '+
       'NO_HORAS,NO_IMP_CAL,NO_JORNADA,NO_PER_CAL,NO_PER_MEN, '+
       'NO_PERCEPC,NO_TARDES,NO_TRIPLES ,NO_TOT_PRE ,NO_VAC_TRA , '+
       'NO_X_CAL,NO_X_ISPT,NO_X_MENS,NO_D_TURNO,NO_DIAS_AJ, '+
       'NO_DIAS_AS,NO_DIAS_CG,NO_DIAS_EM,NO_DIAS_FI,NO_DIAS_FJ, '+
       'NO_DIAS_FV,NO_DIAS_IN,NO_DIAS_NT,NO_DIAS_OT,NO_DIAS_RE, '+
       'NO_DIAS_SG,NO_DIAS_SS,NO_DIAS_SU,NO_HORA_PD,NO_EXENTAS, '+
       'NO_DIAS_BA,NO_DIAS_SI,NO_HORAPDT,NO_HORASNT,NO_PREV_GR,'+
       'NO_DIAS_PV,NO_DIAS_VJ,NO_PRE_EXT,NO_PER_ISN,NO_DIAS_IH,'+
       'NO_DIAS_ID,NO_DIAS_IT';

       Q_TOTALES_NOMPARAMS =
       ' :CB_SAL_INT,:CB_SALARIO,:NO_DIAS,:NO_ADICION,:NO_DEDUCCI, '+
       ':NO_NETO,:NO_DES_TRA,:NO_DIAS_AG,:NO_DIAS_VA,:NO_DOBLES, '+
       ':NO_EXTRAS,:NO_FES_PAG,:NO_FES_TRA,:NO_HORA_CG,:NO_HORA_SG, '+
       ':NO_HORAS,:NO_IMP_CAL,:NO_JORNADA,:NO_PER_CAL,:NO_PER_MEN, '+
       ':NO_PERCEPC,:NO_TARDES,:NO_TRIPLES ,:NO_TOT_PRE ,:NO_VAC_TRA , '+
       ':NO_X_CAL,:NO_X_ISPT,:NO_X_MENS,:NO_D_TURNO,:NO_DIAS_AJ, '+
       ':NO_DIAS_AS,:NO_DIAS_CG,:NO_DIAS_EM,:NO_DIAS_FI,:NO_DIAS_FJ, '+
       ':NO_DIAS_FV,:NO_DIAS_IN,:NO_DIAS_NT,:NO_DIAS_OT,:NO_DIAS_RE, '+
       ':NO_DIAS_SG,:NO_DIAS_SS,:NO_DIAS_SU,:NO_HORA_PD,:NO_EXENTAS, '+
       ':NO_DIAS_BA,:NO_DIAS_SI,:NO_HORAPDT,:NO_HORASNT,:NO_PREV_GR, '+
       ':NO_DIAS_PV,:NO_DIAS_VJ,:NO_PRE_EXT,:NO_PER_ISN,:NO_DIAS_IH,'+
       ':NO_DIAS_ID,:NO_DIAS_IT';

       Q_LTOTALES_DETALLE_NOMINA =
       'INSERT INTO TMPNOM '+
       '(TA_USER,TA_NIVEL1,TA_NIVEL2,TA_NIVEL3,  '+
       'TA_NIVEL4,TA_NIVEL5,CB_CODIGO, '+
       Q_TOTALES_NOMCAMPOS + ' ) '+
       ' VALUES ( :TA_USER,:TA_NIVEL1,:TA_NIVEL2,:TA_NIVEL3,  '+
       ':TA_NIVEL4,:TA_NIVEL5,:CB_CODIGO, '+
       Q_TOTALES_NOMPARAMS + ' )';

       Q_TOTALES_INSERT_NOMINA =
       'INSERT INTO TMPNOM '+
       '(TA_USER,TA_NIVEL1,TA_NIVEL2,TA_NIVEL3,  '+
       'TA_NIVEL4,TA_NIVEL5,CB_CODIGO, '+
       Q_TOTALES_NOMCAMPOS + ' ) '+
       ' VALUES ( :TA_USER,:TA_NIVEL1,:TA_NIVEL2,:TA_NIVEL3,  '+
       ':TA_NIVEL4,:TA_NIVEL5,:CB_CODIGO, '+
       Q_TOTALES_NOMPARAMS + ' )';

       Q_TOTALES_BORRA_LISTA = 'DELETE FROM TMPLISTA WHERE TA_USER = %d AND CB_CODIGO <'+ Q_MAX_EMPLEADO;
       Q_TOTALES_BORRA_NOMINA = 'DELETE FROM TMPNOM WHERE TA_USER = %d AND CB_CODIGO <'+ Q_MAX_EMPLEADO;
       Q_TOTALES_COUNT =
       'SELECT COUNT(CB_CODIGO) CUANTOS '+
       'FROM TMPNOM '+
       'WHERE TA_USER=%d AND CB_CODIGO < ' + Q_MAX_EMPLEADO;
       Q_TOTALES_UPDATE =
       'UPDATE TMPLISTA SET TA_TOTAL = %d ' +
       'WHERE TA_USER = %d AND CB_CODIGO = ' + Q_MAX_EMPLEADO +
       ' %s ';
       Q_TOTALES_BORRA_CEROS = 'DELETE FROM TMPLISTA WHERE TA_USER = %d AND TA_TOTAL = 1';
       Q_TOTALES_BORRA_CEROS_GRUPO = 'DELETE FROM TMPLISTA WHERE TA_USER = %d AND TA_NIVEL%d='''+Q_GRUPOS+''' AND TA_TOTAL = 1';

{------------------------------------------------------------------}
{------------------------------------------------------------------}

       {----------------------------------------}
       Q_GET_GLOBALES_CONTEO = 'SELECT GL_FORMULA FROM GLOBAL '+
                               'WHERE GL_CODIGO BETWEEN %d AND %d '+
                               'ORDER BY GL_CODIGO';

       Q_BALANZA_PERIODO = 'SELECT * FROM PERIODO WHERE %s order by PE_FEC_FIN DESC';
       {$ifdef FALSE}
       Q_BALANZA_NOMM    = 'SELECT '+
                           'min(case PE_USO WHEN 0 THEN PE_FEC_INI ELSE NULL END) Primer_dia_ordinario, ' +
                           'max(case PE_USO WHEN 0 THEN PE_FEC_FIN ELSE NULL END) Ultimo_dia_ordinario, ' +
                           {$ifdef DOS_CAPAS}
                           //En dosCapas no soporta el mismo comando que en Corporativa
                           '( select MAX(PE_NUMERO) from NOMINA N2 '+
                           'LEFT OUTER JOIN PERIODO P3	ON N2.PE_YEAR = P3.PE_YEAR  '+
                                                        'AND N2.PE_TIPO = P3.PE_TIPO  '+
                                                        'AND N2.PE_NUMERO = P3.PE_NUMERO  '+
                           'where (N2.NO_FUERA = ''N'') AND (P3.PE_USO=0) and %s '+
                           'and  N2.PE_TIPO = ( select CB_NOMINA from SP_FECHA_KARDEX( %s, N2.CB_CODIGO ) ) ) Ultimo_Tipo_periodo_ordinario, '+
                           {$else}
                           '( dbo.SP_KARDEX_CB_NOMINA( max(case PE_USO WHEN 0 THEN PE_FEC_FIN ELSE NULL END), NOMINA.CB_CODIGO ) ) Ultimo_Tipo_periodo_ordinario, '+
                           '( dbo.SP_KARDEX_CB_NOMINA( min(case PE_USO WHEN 0 THEN PE_FEC_INI ELSE NULL END), NOMINA.CB_CODIGO ) ) Primer_Tipo_periodo_ordinario, '+
                           {$endif}
                           'CB_CODIGO, '+
                           {$ifdef DOS_CAPAS}
                            '( select CB_NOMINA from SP_FECHA_KARDEX( %s, NOMINA.CB_CODIGO ) ) PE_TIPO, '+
                           {$else}
                           '( dbo.SP_KARDEX_CB_NOMINA( %s, NOMINA.CB_CODIGO ) )  PE_TIPO, '+
                           {$endif}
                           'count(*) Total_Periodos, ' +
                           'sum(case PE_USO WHEN 0 THEN 1 ELSE 0 END) Total_Ordinarios, ' +
                           'sum(case PE_USO WHEN 1 THEN 1 ELSE 0 END) Total_Especiales, ' +
                           'min(case PE_USO WHEN 0 THEN NOMINA.PE_NUMERO ELSE NULL END) Primer_periodo_ordinario, ' +
                           'max(case PE_USO WHEN 0 THEN NOMINA.PE_NUMERO ELSE NULL END) Ultimo_periodo_ordinario, ' +
                           'min(PE_MES) Primer_mes, '+
                           'max(PE_MES) Ultimo_mes  '+
                           'FROM NOMINA '+
                           'LEFT OUTER JOIN PERIODO	ON NOMINA.PE_YEAR = PERIODO.PE_YEAR '+
			                                              'AND NOMINA.PE_TIPO = PERIODO.PE_TIPO '+
                                                    'AND NOMINA.PE_NUMERO = PERIODO.PE_NUMERO '+
                           'WHERE NOMINA.NO_FUERA = ''N'' AND %s '+
                           'GROUP BY CB_CODIGO, NOMINA.PE_TIPO ' +
                           'order by 1';
       {$ELSE}
       Q_BALANZA_NOMM    = 'SELECT '+
                           'CB_CODIGO, '+
                           'count(*) Total_Periodos, ' +
                           'sum(case PE_USO WHEN 0 THEN 1 ELSE 0 END) Total_Ordinarios, ' +
                           'sum(case PE_USO WHEN 1 THEN 1 ELSE 0 END) Total_Especiales, ' +
                           'min(PE_MES) Primer_mes, '+
                           'max(PE_MES) Ultimo_mes  '+
                           'FROM NOMINA '+
                           'LEFT OUTER JOIN PERIODO	ON NOMINA.PE_YEAR = PERIODO.PE_YEAR '+
			                                              'AND NOMINA.PE_TIPO = PERIODO.PE_TIPO '+
                                                    'AND NOMINA.PE_NUMERO = PERIODO.PE_NUMERO '+
                           'WHERE NOMINA.NO_FUERA = ''N'' AND %s '+
                           //'GROUP BY CB_CODIGO, NOMINA.PE_TIPO ' +
                           'GROUP BY CB_CODIGO ' +
                           'order by 1';

       Q_BALANZA_NOMM_MAX_NUM = 'SELECT '+
                                'max(NOMINA.PE_NUMERO) PE_NUMERO ' +
                                'FROM NOMINA '+
                                'LEFT OUTER JOIN PERIODO	ON NOMINA.PE_YEAR = PERIODO.PE_YEAR '+
                                                         'AND NOMINA.PE_TIPO = PERIODO.PE_TIPO '+
                                                         'AND NOMINA.PE_NUMERO = PERIODO.PE_NUMERO '+
                                'WHERE (PERIODO.PE_USO = 0) AND (NOMINA.NO_FUERA = ''N'') AND %s '+
                                'GROUP BY CB_CODIGO ' +
                                'order by 1';

       {$ENDIF}




{procedure TdmServerReportes.DebugDialog(const sMensaje: string);
begin
     if oZetaProvider.Log <> nil then
        oZetaProvider.Log.Evento(clbNinguno,0,NulldateTime, Copy( FormatDateTime( 'hh:nn:ss:zzz',Now ) +'-::-'+ sMensaje, 1,50), sMensaje );
end;}

constructor TdmServerReportes.Create( oProvider : TdmZetaServerProvider ; oCreator : TZetaCreator);
 var oEntidad: TEntidad;
begin
     inherited Create( oProvider, oCreator );

     {CV: Ahorita la unica Entidad que tiene un evento AgregaRelacion es
     enConteo, en un futuro, si alguna otra cae en ese caso, se haria una
     lista de entidades, y el siguiente codigo estaria dentro de un FOR}
     oEntidad := oZetaCreator.dmEntidadesTress.GetEntidad(enConteo) ;
     if ( oEntidad <> NIL ) then
        with oEntidad do
             AlAgregarRelacion := AgregandoRelacion;

     FImagen := oZetaProvider.CreateQuery;


end;

destructor TdmServerReportes.Destroy;
begin
     FreeAndNil(FImagen);
     inherited;
end;

procedure TdmServerReportes.InitBroker;
begin
     if NOT Assigned(oSQLBroker) then
        oSQLBroker := ZetaSQLBroker.GetSQLBroker( oZetaCreator );
end;

procedure TdmServerReportes.ClearBroker;
begin
     ZetaSQLBroker.FreeSQLBroker( oSQLBroker );
end;

procedure TdmServerReportes.DespuesDeConstruyeSuperSQL(FAgenteReporte,oSQLAgente: TSQLAgente);
begin
     case oSQLAgente.Entidad of
          {$ifdef INTERBASE}
          enCurProg :FAgenteReporte.SQL.Text := Unionizer( FAgenteReporte.SQL.Text, 'CUR_PROG', 'CUR_PROG_E','CUR_PROG_R' );
          {$endif}
          enEstadoPrestamo : FAgenteReporte.SQL.Text := StrTransAll( FAgenteReporte.SQL.Text, 'TMPESTAB', 'TMPESTAD' );
          enMovimienLista : PreparaAgenteTotalesLista(FAgenteReporte);
          enListadoNomina : oZetaProvider.ParamList.ParamByName('SoloTotales').AsBoolean := FALSE;
     end;
end;

procedure TdmServerReportes.BorraTabla( const sTabla, sUserCampo : string );
 var FBorra : TZetaCursor;
begin
     with oZetaProvider do
     begin
          FBorra := CreateQuery( Format(Q_DELETE_TABLA, [sTabla,sUserCampo,UsuarioActivo]) );
          try
             EmpiezaTransaccion;
             try
                Ejecuta(FBorra);
                TerminaTransaccion(True);
             except
                   TerminaTransaccion(False);
             end;
          finally
                 FreeAndNil(FBorra);
          end;
     end;
end;


procedure TdmServerReportes.EvaluaColumnas(DataSet: TZetaCursor; oColumna: TSQLColumna; oCampo: TField);
 const K_SQL = 'SELECT IM_BLOB FROM IMAGEN WHERE CB_CODIGO = %d AND IM_TIPO = ''%s''';
       K_SQL_IMAGEN_MAESTRO = 'SELECT MA_IMAGEN FROM MAESTRO WHERE MA_CODIGO = ''%s''';
       {$ifdef BOSEWF}
       K_SQL_IMAGEN_WEVALUA = 'SELECT EV_FIRMA_3 FROM WEVALUA WHERE CB_CODIGO = %d AND EV_FOLIO = %d';
       {$endif}


var  sSQL : string;
 lAplica: boolean;
begin
     lAplica := FALSE;
     if ( Pos(K_IMAGEN_EMPLEADO_X, oColumna.Alias ) > 0 ) then
     begin
          lAplica := TRUE;
          {$ifdef BOSEWF}
           if ( oColumna.ValorConstante = 'FIRMAWF' ) then
             sSQL:= Format( K_SQL_IMAGEN_WEVALUA, [ Dataset.FieldByName(Q_EMPLEADO_FOTO).AsInteger, dataset.FieldByName( 'FOLIOWF' ).AsInteger ] ) //dataset.Fields[29].AsInteger ] )
          else
              sSQL:= Format( K_SQL, [ Dataset.FieldByName(Q_EMPLEADO_FOTO).AsInteger, oColumna.ValorConstante ] );
          {$else}
          sSQL:= Format( K_SQL, [ Dataset.FieldByName(Q_EMPLEADO_FOTO).AsInteger, oColumna.ValorConstante ] );
          {$endif}
     end
     else if Pos(K_IMAGEN_MAESTRO_X, oColumna.Alias ) > 0  then
     begin
               lAplica := TRUE;
          sSQL:= Format( K_SQL_IMAGEN_MAESTRO, [ Dataset.FieldByName(K_MAESTRO_MA_CODIGO).AsString ] );
     end;
     if lAplica  then
     begin
       oZetaProvider.AbreQueryScript( FImagen , sSQL );
       FImagen.Active := TRUE;
       oCampo.AsVariant := FImagen.Fields[0].AsVariant;
     end
     else
     begin
       oCampo.AsVariant := Dataset.FieldByName(oColumna.Formula).AsString;
     end;
end;

procedure TdmServerReportes.AlInicializarParametros;
begin
     with oZetaProvider do
     begin
          if NOT ParamList.ParamByName( 'RDD_APP' ).AsBoolean then
          begin
               GetDatosPeriodo;
               GetDatosImss;
               GetDatosActivos;
          end;
     end;
end;

procedure TdmServerReportes.AsignaParametros;
begin
     inherited;
     with oZetaProvider do
     begin
          with ParamList do
          begin
               VerConfidencial := ParamByName('VerConfidencial').AsBoolean;
               if ParamByName('ContieneImagenes').AsBoolean then
               begin
                    {$ifdef BOSEWF}
                    if FSQLAgente.Entidad >= 10000 then
                       FSQLAgente.AgregaColumna( 'WEVALUA.EV_FOLIO', TRUE, enEmpleado, tgNumero, 10, 'FOLIOWF' );
                    {$endif}
                    //FSQLAgente.AgregaColumna( 'COLABORA.CB_CODIGO', TRUE, enEmpleado, tgNumero, 10, Q_EMPLEADO_FOTO );
                    oSuperReporte.OnEvaluaColumna:= EvaluaColumnas
               end
               else oSuperReporte.OnEvaluaColumna:= NIL; {<-- En Dos_Capas, quedaria Asignado el Evento}
          end;

          AlInicializarParametros;
     end;
end;

procedure TdmServerReportes.AgregandoRelacion(Sender : TObject);
 var FGlobal : TZetaCursor;
     oEntidad : TipoEntidad;
     i: integer;
begin
     i := 1;
     FGlobal := oZetaProvider.CreateQuery( Format(Q_GET_GLOBALES_CONTEO, [K_GLOBAL_CONTEO_NIVEL1,K_GLOBAL_CONTEO_NIVEL5]) );

     try
        with FGlobal do
        begin
             Active := TRUE;
             while NOT EOF do
             begin
                  oEntidad := GetConteoEntidad(eCamposConteo(Fields[0].AsInteger));
                  if oEntidad = enNinguno then
                     Exit
                  else
                  begin
                       {CV: Ahorita el Sender siempre es la Entidad enCONTEO}
                       if oEntidad <> enNivel0 then
                          with TEntidad(Sender), oZetaCreator.dmEntidadesTress do
                               AgregaRelacion(GetEntidad(oEntidad),'CT_NIVEL_'+IntToStr(i));
                       inc(i);
                  end;
                  Next;
             end;
        end;
     finally
            FreeAndNil(FGlobal);
     end;

end;

function TdmServerReportes.GeneraSQLEspecial( oSQLAgente : TSQLAgente;
                                              var Error : string ): TSQLAgente;
begin
     Result := NIL;
     case oSQLAgente.Entidad of
          enMovimienBalanza: Result := CalculaBalanza( oSQLAgente, Error );
          enMovimienBalanzaMensual: Result := CalculaBalanzaMensualReporte( oSQLAgente, Error );
          enCurProg: Result := CalculaCursoProg( oSQlAgente, Error );
          enCalendario: Result := CalculaCalendario( oSQlAgente, Error );
          enCalendarioHoras: Result := CalculaCalendarioHoras( oSQLAgente, Error );
          enMovimienLista: Result := CalculaListadoTotales( oSQlAgente, Error );
          enEstadoAhorro: Result := CalculaEstadoAhorro( oSQlAgente, Error );
          enEstadoPrestamo: Result := CalculaEstadoPrestamo( oSQLAgente, Error );
          enDemografica : Result := CalculaDemografica( oSQlAgente, Error );
          enRotacion: Result := CalculaRotacion( oSQlAgente, Error );
          enConcilia: Result := CalculaConciliacion(oSQLAgente, Error );
          enListadoNomina :
          begin
               Result := CalculaListadoNomina(oSQLAgente, Error );
          end;
          enConteo : Result := CalculaConteoPersonal(oSQLAgente, Error )
     end;
end;

function TdmServerReportes.CalculaListadoNomina(oSQLAgente : TSQLAgente; var Error : string ) : TSQLAgente;
begin
     with TListadoNomina.Create(oZetaProvider,oZetaCreator) do
     begin
          try
             Result := Calcula( oSQLAgente, Error );
          finally
                 Free;
          end;
     end;
end;

function TdmServerReportes.CalculaCursoProg( oSQLAgente : TSQLAgente; var Error : string ) : TSQLAgente;
 var sNivelProg : string;
begin
     Result := oSQLAgente;
     with oZetaProvider do
     begin
          InitGlobales;
          sNivelProg := GetCampoNivelProg(GetGlobalInteger( ZGlobalTress.K_NIVEL_PROGRAMACION_CURSOS ));
     end;

     if StrLleno( sNivelProg )  then
     begin
          Result.AgregaColumna( 'COLABORA.CB_CODIGO', True, enEmpleado, tgNumero, 10, 'CB_CODIGO' ); //PARA FORZAR EL JOIN HACIA COLABORA
          Result.AgregaFiltro( Format( Q_CURSO_PROG, [sNivelProg] ),
                               TRUE,
                               enCurProg );
     end;
end;

{--------------------------------------------------------------}
{--------------- ESTADOS DE CUENTA -----------------------------}
procedure TdmServerReportes.AgregaRegistros( const iCuantos,CB_CODIGO, US_CODIGO : integer;
                                             const TE_CARGO,TE_ABONO, TE_SALDO : Double;
                                             const TE_DESCRIP, TE_TIPO, TE_REFEREN : string;
                                             const TE_FECHA : TDate );
begin
     //Agrega los registros a la Tabla Temporal TMPESTAD
     with oZetaProvider do
     begin
          ParamAsInteger( FEstadoCuenta, 'TE_CODIGO', iCuantos );
          ParamAsInteger( FEstadoCuenta, 'CB_CODIGO', CB_CODIGO );
          ParamAsChar( FEstadoCuenta, 'TE_TIPO', TE_TIPO, 1 );
          ParamAsDate( FEstadoCuenta, 'TE_FECHA', TE_FECHA );
          ParamAsFloat( FEstadoCuenta, 'TE_CARGO', TE_CARGO );
          ParamAsFloat( FEstadoCuenta, 'TE_ABONO', TE_ABONO );
          ParamAsFloat( FEstadoCuenta, 'TE_SALDO', TE_SALDO );
          ParamAsVarChar( FEstadoCuenta, 'TE_DESCRIP', TE_DESCRIP, 30 );
          ParamAsVarChar( FEstadoCuenta, 'TE_REFEREN', TE_REFEREN, 8 );
          ParamAsInteger( FEstadoCuenta, 'US_CODIGO', US_CODIGO );
          Ejecuta( FEstadoCuenta );
     end;
end;

function TdmServerReportes.CalculaEstadoAhorro( oSQLAgente : TSQLAgente; var Error : string ) : TSQLAgente;
 var FMovimien : TZetaCursor;
     FAhorro : TDataset;
     rSaldo : Double;
     i, iCuantos, iColumna, iEmpleado : integer;
     sFecha, sTipo : string;

 procedure PreparaAhorro;
  var i : integer;
 begin
      InitBroker;
      SQLBroker.Init(enAhorro);
      oZetaProvider.InitArregloTPeriodo;//acl
      with SQLBroker.Agente do
      begin
           AgregaColumna( 'AHORRO.CB_CODIGO', True, Entidad, tgNumero, 10, 'CB_CODIGO' );
           AgregaColumna( 'AHORRO.AH_TIPO', True, Entidad, tgTexto, 10, 'AH_TIPO' );
           AgregaColumna( 'AHORRO.AH_FECHA', True, Entidad, tgFecha, 10, 'AH_FECHA' );
           AgregaColumna( 'TAHORRO.TB_CONCEPT', True, enTAhorro, tgNumero, 10, 'TB_CONCEPT' );
           AgregaColumna( 'AHORRO.AH_SALDO_I', True, Entidad, tgFloat, 10, 'AH_SALDO_I' );
           AgregaColumna( 'AHORRO.US_CODIGO', True, Entidad, tgNumero, 10, 'US_CODIGO' );
           AgregaFiltro( Format('AHORRO.AH_FECHA<''%s''',[sFecha]) , TRUE, Entidad );
           AgregaFiltro( '((AHORRO.AH_ABONOS+AH_CARGOS<>0) OR '+
                         '(AHORRO.AH_NUMERO<>0 )) ', True, Entidad );
           AgregaOrden( 'AHORRO.CB_CODIGO' , TRUE, Entidad );
           AgregaOrden( 'AHORRO.AH_TIPO' , TRUE, Entidad );
      end;
      for i:=0 to oSQLAgente.NumFiltros-1 do
          with oSQLAgente.GetFiltro(i) do
          begin
               Formula := StrTransAll( oSQLAgente.GetFiltro(i).Formula, 'TMPESTAD','AHORRO');
               if ( Entidad = enEstadoAhorro ) then
                  Entidad := enAhorro;
          end;

      SQLBroker.Agente.AsignaFiltros( oSQLAgente );
      SQLBroker.BuildDataSet(oZetaProvider.EmpresaActiva);
 end;

 function GetSaldo : Double;
 begin
      if FMovimien.Active then
         with FMovimien do
         begin
              if FieldByName('MO_DEDUCCI').AsFloat<>0 then
                 rSaldo := rSaldo + FieldByName('MO_DEDUCCI').AsFloat
              else rSaldo := rSaldo - FieldByName('MO_PERCEPC').AsFloat;
         end;
      Result := rSaldo;
 end;

 function GetReferencia : string;
 begin
      with FMovimien do
      begin
           if FieldByName('TIPO').AsString = 'M' then
           begin
                Result := ObtieneElemento( lfTipoNomina, FieldByName('PE_TIPO').AsInteger ) +
                       ' # ' + FieldByName('PE_NUMERO').AsString +
                       ' A�o: ' + FieldByName('PE_YEAR').AsString;
           end
           else
           begin
                if ( FieldByName('MO_DEDUCCI').IsNull ) or
                   ( FieldByName('MO_DEDUCCI').AsFloat = 0 ) then
                   Result := 'CARGO'
                else Result := 'ABONO'
           end;
      end;
 end;

 procedure PreparaMovimientos;
 begin
     with oZetaProvider do
          FMovimien := oZetaProvider.CreateQuery( Format( Q_MOVIMIENTOS + Q_ACAR_ABO,
                                                        [UsuarioActivo,UsuarioActivo,sFecha]));
 end;

begin
     iCuantos :=0;
     try
        BorraTabla('TMPESTAD','TE_USER');
        with oZetaProvider do
        begin
             FEstadoCuenta := CreateQuery( Q_MAX_PERIODO );
             FEstadoCuenta.Active:= TRUE;
             sFecha := FormatDateTime( 'mm/dd/yyyy', FEstadoCuenta.Fields[0].AsDateTime);
             PreparaQuery( FEstadoCuenta, Q_ESTADO_AHORRO_INSERT );
             ParamAsInteger( FEstadoCuenta, 'TE_USER', UsuarioActivo );
             ParamAsVarChar( FEstadoCuenta, 'TE_REFEREN', '', 8 );
        end;
        PreparaAhorro;
        PreparaMovimientos;
        FAhorro := SQLBroker.SuperReporte.DataSetReporte;

        try
           oZetaProvider.EmpiezaTransaccion;
           with FAhorro do//este es el de AHORRO
           begin
                while NOT EOF do
                begin
                     Inc(iCuantos);
                     AgregaRegistros( iCuantos,
                                      FieldByName('CB_CODIGO').AsInteger,
                                      -1,
                                      FieldByName('TB_CONCEPT').AsInteger,
                                      0,
                                      FieldByName('AH_SALDO_I').AsFloat,
                                      'SALDO INICIAL',
                                      FieldByName('AH_TIPO').AsString,
                                      '',
                                      FieldByName('AH_FECHA').AsDateTime);
                     Next;
                end;
           end;
           oZetaProvider.TerminaTransaccion(TRUE);
        except
              oZetaProvider.TerminaTransaccion(FALSE);
        end;

        with FMovimien do
        begin
             Active := TRUE;
             iEmpleado := 0;
             sTipo := '';
             try
                oZetaProvider.EmpiezaTransaccion;
                while NOT EOF do
                begin
                     if (sTipo <> FieldByName('TE_TIPO').AsString) OR
                        (iEmpleado <> FieldByName('CB_CODIGO').AsInteger) then
                     begin
                          iEmpleado := FieldByName('CB_CODIGO').AsInteger;
                          sTipo := FieldByName('TE_TIPO').AsString;
                          if FAhorro.Locate('CB_CODIGO;AH_TIPO',VarArrayOf([iEmpleado,sTipo]),[]) then
                          begin
                               Inc(iCuantos);
                               rSaldo := FAhorro.FieldByName('AH_SALDO_I').AsFloat;
                               AgregaRegistros( iCuantos,
                                                FAhorro.FieldByName('CB_CODIGO').AsInteger,
                                                FAhorro.FieldByName('US_CODIGO').AsInteger,
                                                0,
                                                0,
                                                rSaldo,
                                                'SALDO INICIAL',
                                                FAhorro.FieldByName('AH_TIPO').AsString,
                                                '',
                                                0);
                          end;
                     end;
                     Inc(iCuantos);
                     AgregaRegistros( iCuantos,
                                      iEmpleado,
                                      FieldByName('US_CODIGO').AsInteger,
                                      FieldByName('MO_PERCEPC').AsFloat,
                                      FieldByName('MO_DEDUCCI').AsFloat,
                                      GetSaldo,
                                      GetReferencia,
                                      sTipo,
                                      '',
                                      FieldByName('PE_FEC_INI').AsDateTime );
                     Next;
                end;
                rSaldo := 0;
                oZetaProvider.TerminaTransaccion(TRUE);
             except
                   oZetaProvider.TerminaTransaccion(FALSE);
             end;
        end;

        Result := TSQLAgente.Create;
        Result.Entidad := enEstadoAhorro;
        with oSQLAgente do
             for i := 0 to NumColumnas - 1 do
                 with GetColumna(i) do
                 begin
                      iColumna := Result.AgregaColumna( Formula,
                                                        DeDiccionario,
                                                        Entidad,
                                                        TipoFormula,
                                                        Ancho,
                                                        Alias,
                                                        Totalizacion,
                                                        GroupExp );
                      Result.GetColumna(iColumna).ValorConstante := ValorConstante;

                 end;
        Result.AgregaFiltro( Format('TMPESTAD.TE_USER=%d',[oZetaProvider.UsuarioActivo]),TRUE,enEstadoAhorro );
        Result.AgregaFiltro( Format('TMPESTAD.US_CODIGO<>-1',[oZetaProvider.UsuarioActivo]),TRUE,enEstadoAhorro);
        Result.AgregaOrden( 'TMPESTAD.CB_CODIGO',TRUE,enEstadoAhorro );
        Result.AgregaOrden( 'TMPESTAD.TE_TIPO',TRUE,enEstadoAhorro );
        Result.AgregaOrden( 'TMPESTAD.TE_CODIGO',TRUE,enEstadoAhorro );
     finally
            FreeAndNil( FEstadoCuenta );
            FreeAndNil( FMovimien );
            ClearBroker;
     end;

end;
function TdmServerReportes.CalculaEstadoPrestamo( oSQLAgente : TSQLAgente;
                                                  var Error : string ) : TSQLAgente;
 var FMovimien : TZetaCursor;
     FPrestamo : TDataset;
     rSaldo : Double;
     i, iCuantos, iColumna, iEmpleado : integer;
     sFecha, sTipo, sReferencia : string;
     
 procedure PreparaPrestamo;
  var i : integer;
 begin
      InitBroker;
      SQLBroker.Init(enPrestamo);
      oZetaProvider.InitArregloTPeriodo;//acl
      with SQLBroker.Agente do
      begin
           AgregaColumna( 'PRESTAMO.CB_CODIGO', True, Entidad, tgNumero, 10, 'CB_CODIGO' );
           AgregaColumna( 'PRESTAMO.PR_TIPO', True, Entidad, tgTexto, 10, 'PR_TIPO' );
           AgregaColumna( 'PRESTAMO.PR_FECHA', True, Entidad, tgFecha, 10, 'PR_FECHA' );
           AgregaColumna( 'TPRESTA.TB_CONCEPT', True, enTPresta, tgNumero, 10, 'TB_CONCEPT' );
           AgregaColumna( 'PRESTAMO.PR_SALDO_I', True, Entidad, tgFloat, 10, 'PR_SALDO_I' );
           AgregaColumna( 'PRESTAMO.PR_SALDO', True, Entidad, tgFloat, 10, 'PR_SALDO' );
           AgregaColumna( 'PRESTAMO.PR_MONTO', True, Entidad, tgFloat, 10, 'PR_MONTO' );
           AgregaColumna( 'PRESTAMO.PR_REFEREN', True, Entidad, tgTexto, 8, 'PR_REFEREN' );
           AgregaColumna( 'PRESTAMO.US_CODIGO', True, Entidad, tgNumero, 10, 'US_CODIGO' );
           AgregaFiltro( Format('PRESTAMO.PR_FECHA<''%s''',[sFecha]) , TRUE, Entidad );
           AgregaFiltro( '((PRESTAMO.PR_ABONOS+PR_CARGOS<>0) OR '+
                         '(PRESTAMO.PR_NUMERO<>0 )) ', True, Entidad );
           AgregaOrden( 'PRESTAMO.CB_CODIGO' , TRUE, Entidad );
           AgregaOrden( 'PRESTAMO.PR_TIPO' , TRUE, Entidad );
      end;
      for i:=0 to oSQLAgente.NumFiltros-1 do
          with oSQLAgente.GetFiltro(i) do
          begin
               Formula := StrTransAll( oSQLAgente.GetFiltro(i).Formula, 'TMPESTAB','PRESTAMO');
               if ( Entidad = enEstadoPrestamo ) then
                  Entidad := enPrestamo;
          end;

      SQLBroker.Agente.AsignaFiltros( oSQLAgente );
      SQLBroker.BuildDataSet(oZetaProvider.EmpresaActiva);
 end;
 function GetSaldo : Double;
 begin
      if FMovimien.Active then
         with FMovimien do
           if FieldByName('MO_DEDUCCI').AsFloat<>0 then
              rSaldo := rSaldo - FieldByName('MO_DEDUCCI').AsFloat
           else rSaldo := rSaldo + FieldByName('MO_PERCEPC').AsFloat;
      Result := rSaldo;
 end;

 function GetReferencia : string;
 begin
       with FMovimien do
       begin
            if FieldByName('TIPO').AsString = 'M' then
            begin
                 Result := ObtieneElemento( lfTipoNomina, FieldByName('PE_TIPO').AsInteger ) +
                           ' # ' + FieldByName('PE_NUMERO').AsString +
                           ' A�o: ' + FieldByName('PE_YEAR').AsString;
            end
            else
            begin
                if ( FieldByName('MO_DEDUCCI').IsNull ) or
                   ( FieldByName('MO_DEDUCCI').AsFloat = 0 ) then
                   Result := 'CARGO'
                else Result := 'ABONO'
            end;
       end;
 end;

 procedure PreparaMovimientos;
 begin
     with oZetaProvider do
          FMovimien := oZetaProvider.CreateQuery( Format( Q_MOVIMIENTOS + Q_PCAR_ABO,
                                                        [UsuarioActivo,UsuarioActivo,sFecha]));
 end;

begin
     iCuantos :=0;
     try
        BorraTabla('TMPESTAD','TE_USER');
        with oZetaProvider do
        begin
             FEstadoCuenta := CreateQuery( Q_MAX_PERIODO );
             FEstadoCuenta.Active:= TRUE;
             sFecha := FormatDateTime( 'mm/dd/yyyy', FEstadoCuenta.Fields[0].AsDateTime);
             PreparaQuery( FEstadoCuenta, Q_ESTADO_AHORRO_INSERT );
             ParamAsInteger( FEstadoCuenta, 'TE_USER', UsuarioActivo );
             ParamAsInteger( FEstadoCuenta, 'US_CODIGO', -1 );
        end;
        PreparaPrestamo;
        PreparaMovimientos;
        FPrestamo := SQLBroker.SuperReporte.DataSetReporte;

        with FPrestamo do//este es el de PRESTAMO
        begin
             try
                oZetaProvider.EmpiezaTransaccion;
                while NOT EOF do
                begin
                     Inc(iCuantos);
                     AgregaRegistros( iCuantos,
                                      FieldByName('CB_CODIGO').AsInteger,
                                      -1,
                                      FieldByName('TB_CONCEPT').AsInteger,
                                      0,
                                      FieldByName('PR_MONTO').AsFloat - FieldByName('PR_SALDO_I').AsFloat,
                                      'SALDO INICIAL',
                                      FieldByName('PR_TIPO').AsString,
                                      FieldByName('PR_REFEREN').AsString,
                                      FieldByName('PR_FECHA').AsDateTime);
                     Next;
                end;
                oZetaProvider.TerminaTransaccion(TRUE);
             except
                   oZetaProvider.TerminaTransaccion(False);
             end;
        end;

        with FMovimien do
        begin
             Active := TRUE;
             iEmpleado := 0;
             sTipo := '';
             sReferencia := '';

             try
                oZetaProvider.EmpiezaTransaccion;
                while NOT EOF do
                begin
                     if (sReferencia <> FieldByName('TE_REFEREN').AsString) OR
                        (sTipo <> FieldByName('TE_TIPO').AsString) OR
                        (iEmpleado <> FieldByName('CB_CODIGO').AsInteger) then
                     begin
                          iEmpleado := FieldByName('CB_CODIGO').AsInteger;
                          sTipo := FieldByName('TE_TIPO').AsString;
                          sReferencia := FieldByName('TE_REFEREN').AsString;
                          if FPrestamo.Locate('CB_CODIGO;PR_TIPO;PR_REFEREN',VarArrayOf([iEmpleado,sTipo,sReferencia]),[]) then
                          begin
                               Inc(iCuantos);
                               rSaldo := FPrestamo.FieldByName('PR_MONTO').AsFloat - FPrestamo.FieldByName('PR_SALDO_I').AsFloat;
                               AgregaRegistros( iCuantos,
                                                FPrestamo.FieldByName('CB_CODIGO').AsInteger,
                                                FPrestamo.FieldByName('US_CODIGO').AsInteger,
                                                0,
                                                0,
                                                rSaldo,
                                                'SALDO INICIAL',
                                                FPrestamo.FieldByName('PR_TIPO').AsString,
                                                FPrestamo.FieldByName('PR_REFEREN').AsString,
                                                0);
                          end;
                     end;
                     Inc(iCuantos);
                     AgregaRegistros( iCuantos,
                                      iEmpleado,
                                      FieldByName('US_CODIGO').AsInteger,
                                      FieldByName('MO_PERCEPC').AsFloat,
                                      FieldByName('MO_DEDUCCI').AsFloat,
                                      GetSaldo,
                                      GetReferencia,
                                      sTipo,
                                      sReferencia,
                                      FieldByName('PE_FEC_INI').AsDateTime );
                     Next;
                end;
                oZetaProvider.TerminaTransaccion(TRUE);
             except
                   oZetaProvider.TerminaTransaccion(False);
             end;
             rSaldo := 0;
        end;

        Result := TSQLAgente.Create;
        Result.Entidad := enEstadoPrestamo;
        with oSQLAgente do
             for i := 0 to NumColumnas - 1 do
                 with GetColumna(i) do
                 begin
                      iColumna := Result.AgregaColumna( //StrTransAll( Formula, 'TMPESTAB', 'TMPESTAD' ),
                                                        Formula,
                                                        DeDiccionario,
                                                        Entidad,
                                                        TipoFormula,
                                                        Ancho,
                                                        Alias,
                                                        Totalizacion,
                                                        GroupExp );
                      Result.GetColumna(iColumna).ValorConstante := ValorConstante;

                 end;
        Result.AgregaFiltro( Format('TMPESTAD.TE_USER=%d',[oZetaProvider.UsuarioActivo]),TRUE,enEstadoPrestamo );
        Result.AgregaFiltro( Format('TMPESTAD.US_CODIGO<>-1',[oZetaProvider.UsuarioActivo]),TRUE,enEstadoPrestamo );
        Result.AgregaOrden( 'TMPESTAD.CB_CODIGO',TRUE,enEstadoPrestamo );
        Result.AgregaOrden( 'TMPESTAD.TE_TIPO',TRUE,enEstadoPrestamo );
        Result.AgregaOrden( 'TMPESTAD.TE_REFEREN',TRUE,enEstadoPrestamo );
        Result.AgregaOrden( 'TMPESTAD.TE_CODIGO',TRUE,enEstadoPrestamo );
     finally
            FreeAndNil( FEstadoCuenta );
            FreeAndNil( FMovimien );
            ClearBroker;
     end;

end;

procedure TdmServerReportes.AgregaConceptos( MovimienBroker: TSQLBroker; const sFiltro: string );
 var FConcepto : TZetaCursor;
     sConcepto, sRecibo : string;
     i:integer;
begin
     FConcepto := oZetaProvider.CreateQuery( Format( Q_CONCEPTO_SELECT, [' AND (' + sFiltro + ')' ] ));
     with FConcepto do
     begin
          if Conceptos = NIL then
             Conceptos := TStringList.Create;
          Active := TRUE;

          if NOT EOF then
          begin
               while NOT Eof do
               begin
                    sRecibo := FieldByName( 'CO_RECIBO' ).AsString;
                    if StrVacio( sRecibo ) then sRecibo := '0';
                    sConcepto := 'CONCEPTO'+FieldByName( 'CO_NUMERO' ).AsString;
                    //with SQLBroker.Agente do
                    with MovimienBroker.Agente do
                    begin
                         i:=AgregaColumna( sRecibo, FALSE, enNomina, tgFloat, 10, sConcepto );
                         with GetColumna(i) do
                         begin
                              MsgError := Format( 'Error en F�rmula de Recibo en el Concepto #%d',
                                                  [FieldByName( 'CO_NUMERO' ).AsInteger] );
                         end;
                    end;

                    Conceptos.AddObject(sConcepto, TObject( BoolToInt( zStrToBool( FieldByName( 'CO_SUMRECI' ).AsString ) ) ) );

                    Next;
               end;
          end;
     end;
end;

procedure TdmServerReportes.AgregaRegistroBalanza( DataSet : TServerDataset; const iColumna,iEmpleado : integer; const iNumeroPeriodo: integer = 0 );
 var sConcepto : string;
begin
     AgregaRegistroBalanzaPeriodo( DataSet, iColumna, iEmpleado, iNumeroPeriodo );
     with oZetaProvider, DataSet do
     begin
          sConcepto := 'CONCEPTO'+FieldByName('CO_NUMERO').AsString;
          if Conceptos.IndexOf( sConcepto ) < 0 then
             ParamAsFloat( FSp_Balanza, 'HORAS', 0 )
          else
              ParamAsFloat( FSp_Balanza, 'HORAS', FieldByName(sConcepto).AsFloat );

          Ejecuta( FSp_Balanza );
     end;

end;

procedure TdmServerReportes.AgregaRegistroBalanzaPeriodo( DataSet : TServerDataset; const iColumna,iEmpleado : integer; const iNumeroPeriodo: integer = 0 );
begin
     with oZetaProvider, DataSet do
     begin
          ParamAsInteger( FSp_Balanza, 'USUARIO', UsuarioActivo );
          ParamAsInteger( FSp_Balanza, 'YEAR', FieldByName('PE_YEAR').AsInteger );
          if iNumeroPeriodo = 0 then
          begin
               ParamAsInteger( FSp_Balanza, 'NUMERO', FieldByName('PE_NUMERO').AsInteger );
               ParamAsInteger( FSp_Balanza, 'TIPO', FieldByName('PE_TIPO').AsInteger );
          end
          else
          begin //Para la balanza mensual
               ParamAsInteger( FSp_Balanza, 'NUMERO', FUltimoPeriodo.Numero );
               ParamAsInteger( FSp_Balanza, 'TIPO', Ord( FUltimoPeriodo.Tipo ) );
          end;
          
          ParamAsInteger( FSp_Balanza, 'EMPLEADO', iEmpleado );
          ParamAsInteger( FSp_Balanza, 'CONCEPTO', FieldByName('CO_NUMERO').AsInteger );
          ParamAsInteger( FSp_Balanza, 'COLUMNA', iColumna );
          ParamAsChar( FSp_Balanza, 'DESCRIPCION', FieldByName('CO_DESCRIP').AsString, 30 );
          ParamAsVarChar( FSp_Balanza, 'REFERENCIA', FieldByName('MO_REFEREN').AsString, 30 );
          ParamAsFloat( FSp_Balanza, 'MONTO', FieldByName('MO_MONTO').AsFloat );
     end;
end;

procedure TdmServerReportes.AgregaRegistroBalanzaMensual( DataSet : TServerDataset; const iColumna, iEmpleado : integer; const iNumeroPeriodo: integer = 0 );
 var sConcepto, sConceptoReferencia : string;
     iPosicion : integer;
begin
     AgregaRegistroBalanzaPeriodo( Dataset, iColumna, iEmpleado, iNumeroPeriodo );
     with oZetaProvider, DataSet do
     begin
          sConcepto := 'CONCEPTO'+FieldByName('CO_NUMERO').AsString;
          sConceptoReferencia := sConcepto +';' + FieldByName('MO_REFEREN').AsString;
          
          iPosicion := Conceptos.IndexOf( sConcepto );

          if iPosicion < 0 then
             ParamAsFloat( FSp_Balanza, 'HORAS', 0 )
          else
          begin
               //Se revisa si el concepto se suma en todos los periodos.
               //Si no se suma en todos los periodos, solo se pone el valor en el ultimo periodo de nomina
               if ( Integer( Conceptos.Objects[iPosicion] ) = 1 ) then
                  ParamAsFloat( FSp_Balanza, 'HORAS', FieldByName(sConcepto).AsFloat )
               else if ( FConceptosAgregado.IndexOf( sConceptoReferencia ) < 0 ) then
               begin
                    ParamAsFloat( FSp_Balanza, 'HORAS', FieldByName(sConcepto).AsFloat );
                    FConceptosAgregado.Add(sConceptoReferencia);
               end
               else
                   ParamAsFloat( FSp_Balanza, 'HORAS', 0 );
          end;

          Ejecuta( FSp_Balanza );
     end;  
end;

function TdmServerReportes.GetFiltroTipoConceptosBalanza: string;
begin
     if oZetaProvider.GetGlobalBooleano( K_GLOBAL_IMPRIMIR_CONCEPTOS_APOYO ) then
        Result := 'CONCEPTO.CO_TIPO IN (1,2,3,4,6,7)'
     else
         Result := 'CONCEPTO.CO_TIPO IN (1,2,3,4)';
end;

function TdmServerReportes.CalculaBalanza( oSQLAgente : TSQLAgente;
                                           var Error : string) : TSQLAgente;
 var
    FMovimien : TZetaCursor;
    FNomina : TDataset;
    MovimienBroker : TSQLBroker;
    i, iCuantos, iNomEmpleado, iEmpleado, iColumna : integer;
    iLado: integer;
    sFiltro, sFiltroPeriodo : string;

  procedure AgregaRegistroNomina( iEmpleado : integer );
  begin
       with oZetaProvider do
       begin
            ParamAsInteger( FBalanza, 'TZ_USER', UsuarioActivo );
            ParamAsInteger( FBalanza, 'TZ_CODIGO', iCuantos );
            ParamAsInteger( FBalanza, 'TZ_NUM_PER', 0);
            ParamAsVarChar( FBalanza, 'TZ_DES_PER', '', 30 );
            ParamAsFloat( FBalanza, 'TZ_MON_PER', 0 );
            ParamAsInteger( FBalanza, 'TZ_NUM_DED', 0 );
            ParamAsVarChar( FBalanza, 'TZ_DES_DED','', 30 );
            ParamAsFloat( FBalanza, 'TZ_MON_DED', 0 );
            ParamAsVarChar( FBalanza, 'TZ_REF_PER' ,'', 8 );
            ParamAsVarChar( FBalanza, 'TZ_REF_DED' ,'', 8 );
            ParamAsInteger( FBalanza, 'CB_CODIGO', iEmpleado);
            ParamAsInteger( FBalanza, 'PE_YEAR', DatosPeriodo.Year ) ;
            ParamAsInteger( FBalanza, 'PE_TIPO', Ord( DatosPeriodo.Tipo ) );
            ParamAsInteger( FBalanza, 'PE_NUMERO', DatosPeriodo.Numero );
            Ejecuta( FBalanza );
       end;
       Inc( iCuantos );
  end;


begin //CalculaBalanza
     Result := NIL;
     sFiltroPeriodo := VACIO;
     try
        BorraTabla('TMPBALAN','TZ_USER');
        with oZetaProvider do
        begin
             FSP_Balanza := CreateQuery( Q_SP_BALANZA );

             FBalanza := CreateQuery( Q_BALANZA_INSERT );

             if eTipoReporte(ParamList.ParamByName('TipoReporte').AsInteger) = trListado then
                sFiltro := Q_FILTRO_LISTADO
             else sFiltro := 'CONCEPTO.CO_IMPRIME = ''S''';
        end;
        InitBroker;
        SQLBroker.Init(enNomina);
        with SQLBroker.Agente do
        begin
             AgregaColumna( 'NOMINA.CB_CODIGO', True, Entidad, tgNumero, 10, 'CB_CODIGO' );

             with oZetaProvider.DatosPeriodo do
             begin
                  AgregaFiltro( Format( 'NOMINA.PE_YEAR = %d', [Year] ) , TRUE, enNomina );
                  AgregaFiltro( Format( 'NOMINA.PE_TIPO = %d', [Ord(Tipo)] ) , TRUE, enNomina );
                  AgregaFiltro( Format( 'NOMINA.PE_NUMERO = %d', [Numero] ) , TRUE, enNomina );

                  sFiltroPeriodo := ConcatFiltros( sFiltroPeriodo, Format( 'NOMINA.PE_YEAR = %d', [Year] ) );
                  sFiltroPeriodo := ConcatFiltros( sFiltroPeriodo, Format( 'NOMINA.PE_TIPO = %d', [Ord(Tipo)] ) );
                  sFiltroPeriodo := ConcatFiltros( sFiltroPeriodo, Format( 'NOMINA.PE_NUMERO = %d', [Numero] ) );

             end;
             AgregaFiltro( 'NOMINA.NO_FUERA = ''N''' , TRUE, enNomina );
        end;

        for i:=0 to oSQLAgente.NumFiltros-1 do
            with oSQLAgente.GetFiltro(i) do
            begin
                 Formula := StrTransAll( oSQLAgente.GetFiltro(i).Formula, 'TMPBALAN','NOMINA');
                 {cv: 18-JULIO-2002 2.1.93W}
                 {Se requiere el if porque cuando se agregan campos de tablas que no son
                 ni de Nomina ni de MovimienBalanza, marcaba error, de que no se podia relacionar
                 el campo}
                 {No tiene un ELSE, porque la entidad se queda tal y como se agreg� inicialmente.
                 Ejemplo, si el campo es PUESTO.PU_CLASIFI, su entidad es enPUESTO, entonces, la
                 entidad del Filtro queda como enPUESTO. }
                 if Entidad = enMovimienBalanza then
                    Entidad := enNomina;
            end;

        SQLBroker.Agente.AsignaFiltros( oSQLAgente );
        SQLBroker.BuildDataSet(oZetaProvider.EmpresaActiva);

        oZetaProvider.UltimaFechaBalanza := oZetaProvider.DatosPeriodo.Fin;
        with oZetaProvider.DatosNOmm do
             ScriptNOMM := VACIO;

        {$ifdef FALSE}
        {$ifdef DOS_CAPAS}
        sFiltroPeriodo := ConcatFiltros( sFiltroPeriodo, 'NOMINA.CB_CODIGO = :Empleado');
        oZetaProvider.DatosNomm.ScriptNOMM := Format( Q_BALANZA_NOMM, [ sFiltroPeriodo, DateToStrSQLC(oZetaProvider.UltimaFechaBalanza), DateToStrSQLC(oZetaProvider.UltimaFechaBalanza), sFiltroPeriodo  ] );
        {$else}
        with oZetaProvider.DatosNomm do
             ScriptNOMM := Format( Q_BALANZA_NOMM, [ DateToStrSQLC(oZetaProvider.UltimaFechaBalanza), ConcatFiltros( sFiltroPeriodo, 'NOMINA.CB_CODIGO = :Empleado' ) ] );

        {$endif}
        {$ENDIF}

        MovimienBroker := TSQLBroker.Create(oZetaCreator);
        MovimienBroker.Init(enMovimien);

        with MovimienBroker.Agente do
        begin
             AgregaColumna( 'MOVIMIEN.CB_CODIGO', True, enMovimien, tgNumero, 10, 'CB_CODIGO' );
             AgregaColumna( 'MOVIMIEN.PE_TIPO', True, enMovimien, tgNumero, 10, 'PE_TIPO' );
             AgregaColumna( 'MOVIMIEN.PE_YEAR', True, enMovimien, tgNumero, 10, 'PE_YEAR' );
             AgregaColumna( 'MOVIMIEN.PE_NUMERO', True, enMovimien, tgNumero, 10, 'PE_NUMERO' );
             AgregaColumna( 'MOVIMIEN.MO_PERCEPC + MOVIMIEN.MO_DEDUCCI ', True, enMovimien, tgFloat, 10, 'MO_MONTO');
             AgregaColumna( 'MOVIMIEN.MO_REFEREN', True, enMovimien, tgTexto, 8, 'MO_REFEREN' );
             AgregaColumna( 'CONCEPTO.CO_NUMERO', True, enConcepto, tgNumero, 10, 'CO_NUMERO' );
             AgregaColumna( 'CONCEPTO.CO_DESCRIP', True, enConcepto, tgTexto, 30, 'CO_DESCRIP' );
             AgregaColumna( 'CONCEPTO.CO_TIPO', True, enConcepto, tgNumero, 10, 'CO_TIPO' );
             AgregaColumna( 'CONCEPTO.CO_CAMBIA', True, enConcepto, tgTexto, 1, 'CO_CAMBIA' );
             //Temporal - por que no se genera el join hacia nomina
             AgregaColumna( 'NOMINA.CB_CODIGO', True, enNomina, tgNumero, 10  );
             AgregaConceptos( MovimienBroker, sFiltro );

             with oZetaProvider.DatosPeriodo do
             begin
                  AgregaFiltro( Format( 'MOVIMIEN.PE_YEAR = %d', [Year] ) , TRUE, enMovimien );
                  AgregaFiltro( Format( 'MOVIMIEN.PE_TIPO = %d', [Ord(Tipo)] ) , TRUE, enMovimien );
                  AgregaFiltro( Format( 'MOVIMIEN.PE_NUMERO = %d', [Numero] ) , TRUE, enMovimien );
             end;
             AgregaFiltro( 'MOVIMIEN.CB_CODIGO = :EMPLEADO', TRUE, enMovimien );
             AgregaFiltro( 'MOVIMIEN.MO_ACTIVO = ''S''', TRUE, enMovimien );
             AgregaFiltro( 'MOVIMIEN.MO_PERCEPC+MOVIMIEN.MO_DEDUCCI <> 0', TRUE, enMovimien );
             AgregaFiltro( GetFiltroTipoConceptosBalanza , TRUE, enMovimien );
             AgregaFiltro( sFiltro , TRUE, enMovimien );

             AgregaOrden( 'MOVIMIEN.CB_CODIGO', TRUE, enMovimien );
             AgregaOrden( 'MOVIMIEN.CO_NUMERO', TRUE, enMovimien );
        end;

        //pendiente la asignacion de parametros
        if MovimienBroker.SuperSQL.Construye( MovimienBroker.Agente ) then
           if MovimienBroker.SuperReporte.Prepara( MovimienBroker.Agente ) then
           begin
                iCuantos :=0;
                FNomina := SQLBroker.SuperReporte.DataSetReporte;

                try
                   oZetaProvider.EmpiezaTransaccion;

                   while NOT FNomina.EOF do
                   begin
                        iNomEmpleado := FNomina.FieldByName('CB_CODIGO').AsInteger;
                        iEmpleado := iNomEmpleado;

                        {FMovimien, se utiliza nada mas para parametrizar el query,
                        el query que se barre es el DatasetReporte}
                        FMovimien := MovimienBroker.SuperReporte.QueryReporte;
                        FMovimien.Close;
                        begin
                             oZetaProvider.ParamAsInteger( FMovimien, 'Empleado', iNomEmpleado );
                             if MovimienBroker.SuperReporte.AbreDataSet then
                             begin
                                  while ( NOT MovimienBroker.SuperReporte.DatasetReporte.EOF ) and
                                        ( iNomEmpleado = iEmpleado ) do
                                  begin
                                       if ( eTipoConcepto(MovimienBroker.SuperReporte.DatasetReporte.FieldByName('CO_TIPO').AsInteger) in [coPercepcion,coPrestacion,coCaptura] ) then
                                          iLado := 1
                                       else
                                           iLado := 2;

                                       if  zStrToBool( MovimienBroker.SuperReporte.DatasetReporte.FieldByName('CO_CAMBIA').AsString ) and
                                           ( MovimienBroker.SuperReporte.DatasetReporte.FieldByName('MO_MONTO').AsFloat < 0 ) then
                                       begin
                                            if ( iLado = 1 ) then iLado := 2
                                            else iLado := 1;
                                            MovimienBroker.SuperReporte.DatasetReporte.Edit;
                                            MovimienBroker.SuperReporte.DatasetReporte.FieldByName('MO_MONTO').AsFloat := MovimienBroker.SuperReporte.DatasetReporte.FieldByName('MO_MONTO').AsFloat * -1;
                                            MovimienBroker.SuperReporte.DatasetReporte.Post;
                                       end;

                                       AgregaRegistroBalanza( MovimienBroker.SuperReporte.DatasetReporte, iLado, iEmpleado );

                                       MovimienBroker.SuperReporte.DatasetReporte.Next;
                                       iEmpleado := MovimienBroker.SuperReporte.DatasetReporte.FieldByName('CB_CODIGO').AsInteger;
                                  end;//while
                             end//if
                             else
                             begin
                                  AgregaRegistroNomina( iEmpleado );
                             end;//else
                             FNomina.Next;
                        end;
                   end;//while
                   oZetaProvider.TerminaTransaccion(True);
                except
                      On Error : Exception do
                      begin
                           oZetaProvider.TerminaTransaccion(False);
                           raise ;
                      end;
                end;

                Result := TSQLAgente.Create;
                Result.Entidad := enMovimienBalanza;
                   with oSQLAgente do
                        for i := 0 to NumColumnas - 1 do
                            with GetColumna(i) do
                            begin
                                 iColumna := Result.AgregaColumna( Formula,
                                                                   DeDiccionario,
                                                                   Entidad,
                                                                   TipoFormula,
                                                                   Ancho,
                                                                   Alias,
                                                                   Totalizacion,
                                                                   GroupExp );
                                 Result.GetColumna(iColumna).ValorConstante := ValorConstante;

                            end;
                   Result.AgregaFiltro( Format('TMPBALAN.TZ_USER=%d',[oZetaProvider.UsuarioActivo]),TRUE,enMovimienBalanza );
                   Result.AsignaOrdenes( oSQLAgente );
                   Result.AgregaOrden( 'TMPBALAN.CB_CODIGO',TRUE,enMovimienBalanza );
                   Result.AgregaOrden( 'TMPBALAN.TZ_CODIGO',TRUE,enMovimienBalanza );
           end
           else raise Exception.Create(MovimienBroker.Agente.ListaErrores.Text)
        else raise Exception.Create(MovimienBroker.Agente.ListaErrores.Text);
     finally
            ClearBroker;
            FreeAndNil(MovimienBroker);
            FreeAndNil(Conceptos);
     end;
end;


{Metodo para inicializar el Broker de Nomina, se requiere hacer solo una vez}
function TdmServerReportes.InitBalanzaMensual( oSQLAgente: TSQLAgente; var Error: string ) : Boolean;
 var sFiltroMes, sFiltroFormula, sFiltroConcepto, sFiltroPeriodo, sFiltroTipoPeriodo: string;
     sColumnaUltimoTipo, sColumnaUltimoYear: string;
     i, j, iMes: integer;
     lHayMes, lHayYear, lHayTipo, lAgregaFiltro: Boolean;
     oListaMeses: TStrings;
     oEntidad: TipoEntidad;


  function CambiaEntidad( oFiltro: TSQLFiltro; const EntidadAnterior, EntidadNueva : TipoEntidad ): TipoEntidad;
  begin
       if ( oFiltro.Entidad = EntidadAnterior ) then
          Result := EntidadNueva
       else
           Result := oFiltro.Entidad;
  end;

begin
     //Result := FALSE;
     lHayMes:= FALSE;
     lHayYear:= FALSE;
     lHayTipo:= FALSE;
     sFiltroPeriodo := VACIO;
     sFiltroTipoPeriodo := VACIO;
     Error:= VACIO;
     //iFiltroTipo := -1;
     oZetaProvider.UltimaFechaBalanza:= NullDateTime;


     BorraTabla('TMPBALAMES','TM_USER');

     with oZetaProvider do
     begin
          FSP_Balanza := CreateQuery( Q_SP_BALANZA_MENSUAL );
          FBalanza := CreateQuery( Q_BALANZA_MENSUAL_INSERT );
          FPeriodoBalanza := CreateQuery;
          FDatasetNOMM := CreateQuery;
          
          if eTipoReporte(ParamList.ParamByName('TipoReporte').AsInteger) = trListado then
             sFiltroConcepto := Q_FILTRO_LISTADO
          else sFiltroConcepto := 'CONCEPTO.CO_IMPRIME = ''S''';
     end;

     FConceptosAgregado := TStringList.Create;

     InitBroker;
     SQLBroker.Init(enNomina);
     with SQLBroker.Agente do
     begin
          AgregaColumna( 'NOMINA.CB_CODIGO', True, Entidad, tgNumero, 10, 'CB_CODIGO' );
          AgregaColumna( 'NOMINA.PE_TIPO', True, Entidad, tgNumero, 10, 'PE_TIPO' );
          AgregaColumna( 'NOMINA.PE_NUMERO', True, Entidad, tgNumero, 10, 'PE_NUMERO' );
          AgregaColumna( 'NOMINA.PE_YEAR', True, Entidad, tgNumero, 10, 'PE_YEAR' );
          AgregaColumna( 'PERIODO.PE_FEC_INI', True, Entidad, tgNumero, 10, 'PE_FEC_INI' );
          AgregaColumna( 'PERIODO.PE_FEC_FIN', True, Entidad, tgNumero, 10, 'PE_FEC_FIN' );
          //AgregaColumna( '@dbo.SP_KARDEX_CB_NOMINA( PERIODO.PE_FEC_INI, NOMINA.CB_CODIGO )' , False, Entidad, tgNumero, 10, 'PRIMER_TIPO' );
          AgregaFiltro( 'NOMINA.NO_FUERA = ''N''' , TRUE, enNomina );
          AgregaOrden( 'NOMINA.CB_CODIGO', TRUE, enNomina );
          AgregaOrden( 'PERIODO.PE_FEC_INI', TRUE, enPeriodo );

     end;

     for i:=0 to oSQLAgente.NumFiltros-1 do
         with oSQLAgente.GetFiltro(i) do
         begin
              sFiltroFormula := oSQLAgente.GetFiltro(i).Formula;

              //Si el usuario agrego el Filtro de Numero de Periodo, este es ignorado.
              lAgregaFiltro := ( Pos( 'PE_NUMERO', oSQLAgente.GetFiltro(i).Formula ) = 0 );

              //Si el usuario indica una lista o un rango de a�os, se ignora y se especifica el a�o default
              case StrCount( sFiltroFormula, 'PE_YEAR' ) of //Cuantas veces aparece PE_YEAR en el filtro
                   { Si aparece una vez en el filtro, significa que:
                        1) se filtra al a�o activo o se indico un a�o en la lista. En este caso, se mantiene el filtro.
                        2) se filtra de un a�o en adelante o de un a�o hacia atras. En este caso, se ignora el filtro }
                   1 : begin
                            if ( Pos('>=', sFiltroFormula)<>0  ) or
                               ( Pos('<=', sFiltroFormula)<>0  ) then
                               lHayYear := FALSE
                            else
                            begin
                                 lHayYear := Pos( ',', oZetaProvider.ParamList.ParamByName( 'ListaYears' ).AsString ) =0;
                            end;

                            lAgregaFiltro := lHayYear;
                            if lHayYear then
                            begin
                                 sFiltroPeriodo := ConcatFiltros( sFiltroPeriodo, Parentesis( sFiltroFormula ) );

                                 if POS( 'IN', UPPERCASE(sFiltroFormula) ) >0 then
                                    sColumnaUltimoYear := Copy( sFiltroFormula, POS( 'IN', UPPERCASE(sFiltroFormula) )+2, MaxInt)
                                 else
                                     sColumnaUltimoYear := Copy( sFiltroFormula, POS( '=', UPPERCASE(sFiltroFormula) )+2, MaxInt)
                            end;
                       end;
                   { Si aparece mas de una vez en el filtro, significa que:
                        1) se usa en un rango
                        2) se indico una lista de a�os
                    en cualquier caso, se ignora el a�o }
                   2:  begin
                            lHayYear := FALSE;
                            lAgregaFiltro := lHayYear;
                       end;
              end;

              //Si el usuario indica una lista de tipos de periodo, se ignora y se especifica el tipo de periodo activo
              if ( Pos( 'PE_TIPO', sFiltroFormula ) <> 0 ) then
              begin
                   case StrCount( sFiltroFormula, 'PE_TIPO' ) of
                         1 : begin
                                  lHayTipo := FALSE;
                                  lAgregaFiltro := lHayTipo;
                                  sColumnaUltimoTipo := Copy( sFiltroFormula, Pos('=', sFiltroFormula)+1, maxint );
                                  {$IFDEF DOS_CAPAS}
                                  sFiltroTipoPeriodo := Format( '(select CB_NOMINA from SP_FECHA_KARDEX( %s, NOMINA.CB_CODIGO )) = %d', ['%s', StrToInt(sColumnaUltimoTipo) ]);
                                  {$ELSE}
                                  sFiltroTipoPeriodo := Format( 'dbo.SP_KARDEX_CB_NOMINA( %s, NOMINA.CB_CODIGO ) = %d', ['%s', StrToInt(sColumnaUltimoTipo) ]);
                                  {$ENDIF}

                             end
                         else
                         begin
                              lHayTipo := FALSE;
                              lAgregaFiltro := lHayTipo;
                         end;
                   end;
              end;

              //Si el usuario especifica una lista de meses salteados, se ignora y se especifica el mes del periodo activo
              if ( Pos( 'PE_MES',   sFiltroFormula ) <> 0 ) then
              begin
                   lHayMes := TRUE;

                   if ( StrCount( sFiltroFormula ,'PE_MES') > 1 ) then
                   begin
                        oListaMeses := TStringList.Create;
                        try
                           oListaMeses.CommaText := oZetaProvider.ParamList.ParamByName( 'ListaMeses' ).AsString;
                           iMes:= StrToInt( oListaMeses[0] );
                           for j:=1 to oListaMeses.Count - 1 do
                           begin
                                if ( iMes + 1 <> StrToInt( oListaMeses[j] ) ) then
                                begin
                                     lHayMes := FALSE;
                                     Break;
                                end;
                                inc( iMes );
                           end;
                        finally
                               FreeAndNil( oListaMeses );
                        end;
                   end;
                   lAgregaFiltro := FALSE;
                   if lHayMes then
                   begin
                        sFiltroPeriodo := ConcatFiltros( sFiltroPeriodo, Parentesis( sFiltroFormula ) );
                        SQLBroker.Agente.AgregaFiltro( StrTransAll( StrTransAll( sFiltroFormula, 'TMPBALAMES','PERIODO'), 'V_NOMMES', 'NOMINA' ),
                                              oSQLAgente.GetFiltro(i).DeDiccionario,
                                              enPeriodo  );

                   end;
              end;

              if lAgregaFiltro then
              begin
                   oEntidad := CambiaEntidad( oSQLAgente.GetFiltro(i), enMovimienBalanzaMensual, enNomina );
                   if oEntidad = enNomMes then
                      oEntidad := CambiaEntidad( oSQLAgente.GetFiltro(i), enNomMes, enNomina );

                   SQLBroker.Agente.AgregaFiltro( StrTransAll( StrTransAll( sFiltroFormula, 'TMPBALAMES','NOMINA'), 'V_NOMMES', 'NOMINA' ),
                                                               oSQLAgente.GetFiltro(i).DeDiccionario,
                                                               oEntidad  );
              end;
         end;

     with oZetaProvider,DatosPeriodo do
     begin
          //Se agregan el filtro para acumular solo Periodos con Status=Afectada Total
          SQLBroker.Agente.AgregaFiltro( Format( 'NOMINA.NO_STATUS = %d', [ord(spAfectadaTotal)] ) , TRUE, enNomina );
          sFiltroPeriodo := ConcatFiltros( sFiltroPeriodo, Format( 'PERIODO.PE_STATUS = %d', [ord(spAfectadaTotal)] ) );

          //Si el usuario no agrego los filtros minimos requeridos, se agregan los filtros default:
          if NOT lHayYear then
          begin
               SQLBroker.Agente.AgregaFiltro( Format( 'NOMINA.PE_YEAR = %d', [Year] ) , TRUE, enNomina );
               sFiltroPeriodo := ConcatFiltros( sFiltroPeriodo, Format( 'PERIODO.PE_YEAR = %d', [Year] ) );
               sColumnaUltimoYear := IntToStr( Year );
          end;
          if NOT lHayTipo and StrVacio(sFiltroTipoPeriodo) then
          begin
               sColumnaUltimoTipo := IntToStr( Ord( Tipo ) );

               {$IFDEF DOS_CAPAS}
               sFiltroTipoPeriodo := Format( '(select CB_NOMINA from SP_FECHA_KARDEX( %s, NOMINA.CB_CODIGO )) = %d', ['%s', Ord(Tipo)]);
               {$ELSE}
               sFiltroTipoPeriodo := Format( 'dbo.SP_KARDEX_CB_NOMINA( %s, NOMINA.CB_CODIGO ) = %d', ['%s', Ord(Tipo)]);
               {$ENDIF}
          end;



          if not lHayMes then
          begin
               if oZetaProvider.ParamList.ParamByName( 'ListaMesesTodos' ).AsBoolean then
                  sFiltroMes := 'PERIODO.PE_MES >=1 and PERIODO.PE_MES <=12'
               else
                   sFiltroMes := Format( 'PERIODO.PE_MES = %d', [Mes] ) ;

               sFiltroPeriodo := ConcatFiltros( sFiltroPeriodo, sFiltroMes );
               SQLBroker.Agente.AgregaFiltro( StrTransAll( Parentesis( sFiltroMes ), 'TMPBALAMES','PERIODO' ), TRUE, enPeriodo );

          end;

          sFiltroPeriodo := StrTransAll( sFiltroPeriodo, 'TMPBALAMES', 'PERIODO');

          PreparaQuery( FPeriodoBalanza, Format( Q_BALANZA_PERIODO, [ sFiltroPeriodo ] ) );
          FPeriodoBalanza.Open;
          with FPeriodoBalanza.fieldbyname('PE_YEAR') DO
          begin
               Result := ( NOT IsNull )and (AsInteger <> 0) ;
               if not Result then
               begin
                    Error := K_NoHayRegistros;
               end
          end;

          if Result then
          begin
               //Se agregan el tipo de nomina con el que se esta obteniendo el reporte
               with SQLBroker.Agente do
               begin
                    FUltimoPeriodo.Tipo := eTipoPeriodo( StrToInt( sColumnaUltimoTipo ));
                    AgregaColumna( Format( '@%s', [Trim(sColumnaUltimoTipo)] ), FALSE, Entidad, tgNumero, 10, 'ULTIMO_TIPO' );
               end;

               oZetaProvider.UltimaFechaBalanza := FPeriodoBalanza.FieldByName('PE_FEC_FIN').AsDateTime;

               if not lHayTipo then
               begin
                    //sFiltroTipoPeriodo := Format( '@dbo.SP_KARDEX_CB_NOMINA( %s, NOMINA.CB_CODIGO ) = %d', ['%s', Ord(Tipo)]);
                    SQLBroker.Agente.AgregaFiltro( '@'+Format( sFiltroTipoPeriodo, [Comillas( FechaAsStr( oZetaProvider.UltimaFechaBalanza )) ]), FALSE, enNomina  ); //Tipo de Nomina Final del Empleado
                    sFiltroPeriodo := ConcatFiltros( sFiltroPeriodo, Format( sFiltroTipoPeriodo, [DateToStrSQLC( oZetaProvider.UltimaFechaBalanza ) ]) );
               end;

               {$ifdef DOS_CAPAS}
               //sFiltroPeriodo := ConcatFiltros( sFiltroPeriodo, 'NOMINA.CB_CODIGO = :Empleado');
               //ScriptNOMM := Format( Q_BALANZA_NOMM, [ sFiltroPeriodo, DateToStrSQLC(oZetaProvider.UltimaFechaBalanza), DateToStrSQLC(oZetaProvider.UltimaFechaBalanza), sFiltroPeriodo  ] );
               {$else}
               //ScriptNOMM := Format( Q_BALANZA_NOMM, [ DateToStrSQLC(oZetaProvider.UltimaFechaBalanza), ConcatFiltros( sFiltroPeriodo, 'NOMINA.CB_CODIGO = :Empleado' ) ] );
               //ScriptNOMM :=
               {$endif}
          end;
     end;
     if Result then
     begin
          {Se inicializa el Broker de la Nomina
           este Broker me sirve para saber cuales empleados se van a imprimir }
          if SQLBroker.SuperSQL.Construye( SQLBroker.Agente ) then
             Result := SQLBroker.SuperReporte.Prepara( SQLBroker.Agente );

          if NOT Result then
              Error:= SQLBroker.Agente.ListaErrores.Text;


          {$ifdef CAROLINA}
          SQLBroker.Agente.SQL.SaveToFile( 'd:\temp\NominaSQL.sql' );
          {$endif}

          { Si el Broker de la Nomina se prepara correctamente,
            ya puedo preparar el Broker de Movimien }
          if Result then
          begin

               with oZetaProvider,DatosNOmm do
               begin
                    TotalesNOMM := Format( Q_BALANZA_NOMM, [ ConcatFiltros( sFiltroPeriodo, 'NOMINA.CB_CODIGO = :Empleado' ) ] );

                    ScriptNOMM := StrTransform( SQLBroker.Agente.SQL.Text,
                                                         'WHERE',
                                                         //' WHERE (PERIODO.PE_USO=0) AND (NOMINA.CB_CODIGO = :Empleado) and ' ) + ', PERIODO.PE_FEC_INI' ;
                                                         ' WHERE (PERIODO.PE_USO=0) AND (NOMINA.CB_CODIGO = :Empleado) and ' ) ;
                    ScriptNOMMTipo := StrTransform( SQLBroker.Agente.SQL.Text, 'WHERE',
                                                         ' WHERE (PERIODO.PE_USO=0) AND (NOMINA.CB_CODIGO = :Empleado) and '+
                                                         //'(NOMINA.PE_TIPO = :Tipo) and ' )+ ', PERIODO.PE_FEC_INI';
                                                         '(NOMINA.PE_TIPO = :Tipo) and ' );
                    PreparaQuery( FDataSetNOMM, Format( Q_BALANZA_NOMM_MAX_NUM, [ ConcatFiltros( sFiltroPeriodo, '(NOMINA.CB_CODIGO = :Empleado) AND (NOMINA.PE_TIPO= :Tipo)' ) ] ) );
               end;


               {$ifdef CAROLINA}
               TZetaComando(FDataSetNOMM).SQL.SaveToFile( 'd:\temp\nOMMSQL.sql' );
               {$endif}

               MovimienBalanzaMensual := TSQLBroker.Create(oZetaCreator);
               MovimienBalanzaMensual.Init(enMovimien);

               with MovimienBalanzaMensual.Agente do
               begin
                    AgregaColumna( 'MOVIMIEN.CB_CODIGO', True, enMovimien, tgNumero, 10, 'CB_CODIGO' );
                    AgregaColumna( 'MOVIMIEN.PE_TIPO', True, enMovimien, tgNumero, 10, 'PE_TIPO' );
                    AgregaColumna( 'MOVIMIEN.PE_YEAR', True, enMovimien, tgNumero, 10, 'PE_YEAR' );
                    AgregaColumna( 'MOVIMIEN.PE_NUMERO', True, enMovimien, tgNumero, 10, 'PE_NUMERO' );
                    AgregaColumna( 'MOVIMIEN.MO_PERCEPC + MOVIMIEN.MO_DEDUCCI ', True, enMovimien, tgFloat, 10, 'MO_MONTO');
                    AgregaColumna( 'MOVIMIEN.MO_REFEREN', True, enMovimien, tgTexto, 8, 'MO_REFEREN' );
                    AgregaColumna( 'CONCEPTO.CO_NUMERO', True, enConcepto, tgNumero, 10, 'CO_NUMERO' );
                    AgregaColumna( 'CONCEPTO.CO_DESCRIP', True, enConcepto, tgTexto, 30, 'CO_DESCRIP' );
                    AgregaColumna( 'CONCEPTO.CO_TIPO', True, enConcepto, tgNumero, 10, 'CO_TIPO' );
                    AgregaColumna( 'CONCEPTO.CO_CAMBIA', True, enConcepto, tgTexto, 1, 'CO_CAMBIA' );
                    //Temporal - por que no se genera el join hacia nomina
                    AgregaColumna( 'NOMINA.CB_CODIGO', True, enNomina, tgNumero, 10  );
                    AgregaConceptos( MovimienBalanzaMensual, sFiltroConcepto );

                    with oZetaProvider.DatosPeriodo do
                    begin
                         AgregaFiltro( 'MOVIMIEN.PE_YEAR = :YEAR' , TRUE, enMovimien );
                         AgregaFiltro( 'MOVIMIEN.PE_TIPO = :TIPO' , TRUE, enMovimien );
                         AgregaFiltro( 'MOVIMIEN.PE_NUMERO = :NUMERO' , TRUE, enMovimien );
                    end;
                    AgregaFiltro( 'MOVIMIEN.CB_CODIGO = :EMPLEADO', TRUE, enMovimien );
                    AgregaFiltro( 'MOVIMIEN.MO_ACTIVO = ''S''', TRUE, enMovimien );
                    AgregaFiltro( 'MOVIMIEN.MO_PERCEPC+MOVIMIEN.MO_DEDUCCI <> 0', TRUE, enMovimien );
                    AgregaFiltro( GetFiltroTipoConceptosBalanza , TRUE, enMovimien );
                    AgregaFiltro( sFiltroConcepto , TRUE, enMovimien );

                    AgregaOrden( 'MOVIMIEN.CB_CODIGO', TRUE, enMovimien );
                    AgregaOrden( 'MOVIMIEN.CO_NUMERO', TRUE, enMovimien );
               end;

               Result := MovimienBalanzaMensual.SuperSQL.Construye( MovimienBalanzaMensual.Agente );
               if Result then
                  Result := MovimienBalanzaMensual.SuperReporte.Prepara( MovimienBalanzaMensual.Agente );
               {$IFDEF CAROLINA}
               MovimienBalanzaMensual.Agente.SQL.SaveToFile( 'd:\temp\MovimienSQL.sql' );
               {$ENDIF}
               if NOT Result then
                  Error:= MovimienBalanzaMensual.Agente.ListaErrores.Text;
          end;
     end;
end;

procedure TdmServerReportes.CalculaBalanzaMensual( oSQLAgente: TSQLAgente; oDatosPeriodo: TDatosPeriodo; var iCuantos: integer; var Error: string );
 var
    FMovimien : TZetaCursor;
    FNomina : TDataset;

    iEmpleado, iEmpleadoTemp: integer;
    iLado: integer;
    {$ifdef CAROLINA}
    oBitacora: TStrings;
    {$endif}

begin //CalculaBalanzaMensual
      //Para el dataset de NOMINA hay que indicar el periodo de NOMINA que se quiere calcular
      with SQLBroker.SuperReporte do
      begin
           QueryReporte.Close;

           with TZetaComando( SQLBroker.SuperReporte.queryreporte ) do
                Sql.Text := SQL.Text + ' DESC '; 


           if AbreDataSet then
              FNomina := DataSetReporte
           else
               FNomina := NIL;
      end;

      if ( FNomina <> NIL ) then
      begin
           try
              {$ifdef CAROLINA}
              oBitacora:= TStringList.Create;
              {$endif}

              {$ifndef DOS_CAPAS}
              oZetaProvider.EmpiezaTransaccion;
              {$endif}

              iEmpleadoTemp := 0;
              
              while NOT FNomina.EOF do
              begin
                   iEmpleado := FNomina.FieldByName('CB_CODIGO').AsInteger;
                   if iEmpleado <> iEmpleadoTemp then
                      FConceptosAgregado.Clear;

                   with FDataSetNOMM do
                   begin
                        Close;
                        oZetaProvider.ParamAsInteger( FDatasetNOMM, 'Empleado', iEmpleado );
                        oZetaProvider.ParamAsInteger( FDatasetNOMM, 'Tipo', Ord( FUltimoPeriodo.Tipo)  );
                        Open;
                        FUltimoPeriodo.Numero := FieldByName('PE_NUMERO').AsInteger;
                        Close;
                   end;


                   {FMovimien, se utiliza nada mas para parametrizar el query,
                   el query que se barre es el DatasetReporte}
                   FMovimien := MovimienBalanzaMensual.SuperReporte.QueryReporte;
                   FMovimien.Close;
                   begin
                        oZetaProvider.ParamAsInteger( FMovimien, 'Empleado', iEmpleado );
                        oZetaProvider.ParamAsInteger( FMovimien, 'Year',   FNomina.FieldByName('PE_YEAR').AsInteger );
                        oZetaProvider.ParamAsInteger( FMovimien, 'Tipo',   FNomina.FieldByName('PE_TIPO').AsInteger );
                        oZetaProvider.ParamAsInteger( FMovimien, 'Numero', FNomina.FieldByName('PE_NUMERO').AsInteger );
                        if MovimienBalanzaMensual.SuperReporte.AbreDataSet then
                        begin
                             while ( NOT MovimienBalanzaMensual.SuperReporte.DatasetReporte.EOF ) do
                             begin
                                  if ( MovimienBalanzaMensual.SuperReporte.DatasetReporte.FieldByName('CO_TIPO').AsInteger in [Ord(coPercepcion),ord(coPrestacion),ord(coCaptura)] ) then
                                     iLado := 1
                                  else
                                      iLado := 2;

                                  if  zStrToBool( MovimienBalanzaMensual.SuperReporte.DatasetReporte.FieldByName('CO_CAMBIA').AsString ) and
                                      ( MovimienBalanzaMensual.SuperReporte.DatasetReporte.FieldByName('MO_MONTO').AsFloat < 0 ) then
                                  begin
                                       if ( iLado = 1 ) then iLado := 2
                                       else iLado := 1;
                                       MovimienBalanzaMensual.SuperReporte.DatasetReporte.Edit;
                                       MovimienBalanzaMensual.SuperReporte.DatasetReporte.FieldByName('MO_MONTO').AsFloat := MovimienBalanzaMensual.SuperReporte.DatasetReporte.FieldByName('MO_MONTO').AsFloat * -1;
                                       MovimienBalanzaMensual.SuperReporte.DatasetReporte.Post;
                                  end;

                                  AgregaRegistroBalanzaMensual( MovimienBalanzaMensual.SuperReporte.DatasetReporte, iLado, iEmpleado, FUltimoPeriodo.Numero );

                                  MovimienBalanzaMensual.SuperReporte.DatasetReporte.Next;
                             end;//while
                        end;//if
                        {$ifdef CAROLINA}
                        //oBitacora.LoadFromFile( 'd:\temp\BitacoraTiempos.txt' );
                        //oBitacora.Add( Format( '%s - Empleado %d', [ FormatDateTime('dd/mmm/2010 hh:nn:ss', Now), iEmpleado ]  ) );
                        //oBitacora.SaveToFile( 'd:\temp\BitacoraTiempos.txt' );
                        {$endif}
                        iEmpleadoTemp := iEmpleado;
                        FNomina.Next;
                   end;
              end;//while
              {$ifndef DOS_CAPAS}
              oZetaProvider.TerminaTransaccion(True);
              {$endif}
           except
                 On Error : Exception do
                 begin
                      {$ifndef DOS_CAPAS}
                      oZetaProvider.TerminaTransaccion(False);
                      {$endif}
                      raise ;
                 end;
           end;
      end
end;

function TdmServerReportes.CalculaBalanzaMensualReporte( oSQLAgente: TSQLAgente; var Error: string ): TSQLAgente;
 var i, iColumna, iCuantos: integer;
     //lContinua : Boolean;
     oDatosPeriodo: TDatosPeriodo;
begin
     Result := NIL;
     iCuantos:=0 ;
     //lContinua := TRUE;
     try
        
        { ** ******************************************************* **}
        { ** ***** CREACION DE LA BALANZA QUE SE ACUMULA *********** **}
        if InitBalanzaMensual( oSQLAgente, Error ) then
        begin
             {$ifdef FALSE}
             FPeriodoBalanza.Open;
             oZetaProvider.UltimaFechaBalanza := FPeriodoBalanza.FieldByName('PE_FEC_FIN').AsDateTime;
             while NOT FPeriodoBalanza.EOF and lContinua do
             begin
                  with oDatosPeriodo do
                  begin
                       Year := FPeriodoBalanza.FieldByName('PE_YEAR').AsInteger;
                       Tipo := eTipoPeriodo( FPeriodoBalanza.FieldByName('PE_TIPO').AsInteger );
                       Numero := FPeriodoBalanza.FieldByName('PE_NUMERO').AsInteger;
                  end;

                  lContinua := CalculaBalanzaMensual( oSQLAgente, oDatosPeriodo, iCuantos, Error );
                  FPeriodoBalanza.Next;
             end;
             {$ELSE}
             CalculaBalanzaMensual( oSQLAgente, oDatosPeriodo, iCuantos, Error );
             {$ENDIF}
             //OrdenaBalanza:
             oZetaProvider.EjecutaAndFree( Format( 'execute procedure SP_ORDENA_BALAMES( %d )', [oZetaProvider.UsuarioActivo])  );

              //Para hacer pruebas a un periodo en especifico:
             {CalculaBalanzaMensual( oSQLAgente, 2, iCuantos, Error );
             CalculaBalanzaMensual( oSQLAgente, 2, iCuantos, Error );
             CalculaBalanzaMensual( oSQLAgente, 3, iCuantos, Error );
             CalculaBalanzaMensual( oSQLAgente, 4, iCuantos, Error ); }

             { ** ******************************************************* **}
             { ** * SE CREA EL AGENTE CON EL QUE SE MUESTRA EL REPORTE ** **}

             if StrVacio(Error) then
             begin
                  Result := TSQLAgente.Create;
                  Result.Entidad := enMovimienBalanzaMensual;
                     with oSQLAgente do
                          for i := 0 to NumColumnas - 1 do
                              with GetColumna(i) do
                              begin
                                   iColumna := Result.AgregaColumna( Formula,
                                                                     DeDiccionario,
                                                                     Entidad,
                                                                     TipoFormula,
                                                                     Ancho,
                                                                     Alias,
                                                                     Totalizacion,
                                                                     GroupExp );
                                   Result.GetColumna(iColumna).ValorConstante := ValorConstante;

                              end;
                  Result.AgregaFiltro( Format('TMPBALAMES.TM_USER=%d',[oZetaProvider.UsuarioActivo]),TRUE,enMovimienBalanzaMensual );
                  Result.AsignaOrdenes( oSQLAgente );
                  Result.AgregaOrden( 'TMPBALAMES.CB_CODIGO',TRUE,enMovimienBalanzaMensual );
                  Result.AgregaOrden( 'TMPBALAMES.TM_CODIGO',TRUE,enMovimienBalanzaMensual )
             end
             else
                 raise Exception.Create( Error );
        end
        else
            raise Exception.Create( Error );
     finally
            ClearBroker;
            oSQLBroker := NIL; //Queda en estado dudoso si solamente se usa el ClearBroker.
            FreeAndNil(MovimienBalanzaMensual);
            FreeAndNil(Conceptos); //Se crea en el metodo AgregaConceptos
            FreeAndNil(FPeriodoBalanza);
            FreeAndNil(FDatasetNOMM);
            FreeAndNil( FConceptosAgregado );
     end;

end;


function TdmServerReportes.CalculaCalendario(oSQlAgente: TSQLAgente;var Error: string): TSQLAgente;
 var spCalendario : TZetaCursor;
     dFechaI, dFechaF, dInicial, dFinal : TDate;
     iYear, YearInicial, YearFinal, iMes, MesInicial, MesFinal : integer;
     i, iColumna: integer;
 procedure CreaDataset;
 begin
      BorraTabla( 'TMPCALEN','US_CODIGO' );
      with oZetaProvider do
           spCalendario := CreateQuery( Q_SP_CALENDARIO );
 end;
begin
     Result := NIL;
     try
        CreaDataset;
        InitBroker;
        SQLBroker.Init(enEmpleado);
        with SQLBroker.Agente do
        begin
             AgregaColumna( 'COLABORA.CB_CODIGO', True, Entidad, tgNumero, 10, 'CB_CODIGO' );
             AgregaOrden( 'COLABORA.CB_CODIGO', True, Entidad );

             with oZetaProvider.ParamList do
             begin
                  dFinal:= ParamByName('dCalenFinal').AsDate;
                  dInicial:= ParamByName('dCalenInicial').AsDate;
                  if dInicial > dFinal then
                     raise Exception.Create( 'La Fecha Inicial:'+FechaAsStr(dInicial)+' debe Ser Menor que la Fecha Final '+FechaAsStr(dFinal));
             end;
             for i:=0 to oSQLAgente.NumFiltros - 1 do
             begin
                  with oSQLAgente.GetFiltro(i) do
                       if (DeDiccionario) then
                       begin
                            if NOT (Pos( 'TMPCALEN.TL_FEC_MIN',Formula )>0) then
                            begin
                                 if Entidad = enCalendario then
                                    AgregaFiltro( StrTransAll(Formula,'TMPCALEN.CB_CODIGO','COLABORA.CB_CODIGO'),
                                                  DeDiccionario,
                                                  enEmpleado )
                                 else
                                     AgregaFiltro( Formula,
                                                   DeDiccionario,
                                                   Entidad );
                            end
                       end
             end;
        end;
        with SQLBroker,oZetaProvider do
             if BuildDataset(EmpresaActiva) then
             begin
               ParamAsInteger( spCalendario,'USUARIO', UsuarioActivo );
               try
                  oZetaProvider.EmpiezaTransaccion;
                  while NOT SuperReporte.DataSetReporte.EOF do
                  begin
                       YearInicial := TheYear(dInicial);
                       YearFinal := TheYear(dFinal);

                       for iYear := YearInicial to YearFinal do
                       begin
                            if iYear = YearInicial then
                               MesInicial := TheMonth(dInicial)
                            else MesInicial := 1;
                            if iYear = YearFinal then
                               MesFinal := TheMonth(dFinal)
                            else MesFinal := 12;

                            for iMes := MesInicial to MesFinal do
                            begin
                                 if (iYear=YearInicial) AND (iMes = MesInicial) then
                                    dFechaI := dInicial
                                 else dFechaI := EncodeDate( iYear, iMes, 1 );
                                 if (iYear=YearFinal) AND (iMes = MesFinal) then
                                    dFechaF := dFinal
                                 else dFechaF := EncodeDate( iYear, iMes, MonthDays[IsLeapYear(iYear),iMes] );

                                 ParamAsInteger( spCalendario,'EMPLEADO',SuperReporte.DataSetReporte.Fields[0].AsInteger );
                                 ParamAsInteger( spCalendario,'YEAR',iYear );
                                 ParamAsInteger( spCalendario,'MONTH',iMes );
                                 ParamAsDate( spCalendario,'FECHA_MES', FirstDayOfMonth(dFechaI) );
                                 ParamAsDate( spCalendario,'FECHA_INI', dFechaI );
                                 ParamAsDate( spCalendario,'FECHA_FIN', dFechaF );
                                 {$ifdef INTERBASE}
                                 spCalendario.Active:= TRUE;
                                 spCalendario.Active:= FALSE;
                                 {$endif}

                                 {$ifdef MSSQL}
                                 Ejecuta( spCalendario );
                                 {$endif}

                            end;//Mes
                       end;//Year
                       SuperReporte.DataSetReporte.Next;
                  end;
                  oZetaProvider.TerminaTransaccion(True);
               except
                     oZetaProvider.TerminaTransaccion(False);
               end;
             end;

        Result := TSQLAgente.Create;
        Result.Entidad := enCalendario;
        with oSQLAgente do
        begin
             for i := 0 to NumColumnas - 1 do
                 with GetColumna(i) do
                 begin
                      iColumna := Result.AgregaColumna( Formula,
                                                        DeDiccionario,
                                                        Entidad,
                                                        TipoFormula,
                                                        Ancho,
                                                        Alias,
                                                        Totalizacion,
                                                        GroupExp );
                      Result.GetColumna(iColumna).ValorConstante := ValorConstante;

                 end;
             for i:=0 to NumFiltros - 1 do
             begin
                  with GetFiltro(i) do
                       if NOT (DeDiccionario) then
                          Result.AgregaFiltro( Formula,
                                               DeDiccionario,
                                               Entidad );
             end;
        end;
        Result.AgregaFiltro( Format('TMPCALEN.US_CODIGO=%d',[oZetaProvider.UsuarioActivo]),TRUE,enCalendario);
        Result.AsignaOrdenes(oSQLAgente);
     finally
            FreeAndNil(spCalendario);
            ClearBroker;
     end;
end;

function TdmServerReportes.CalculaCalendarioHoras(oSQlAgente: TSQLAgente;var Error: string): TSQLAgente;
 var spCalendario : TZetaCursor;
     dFechaI, dFechaF, dInicial, dFinal : TDate;
     iYear, YearInicial, YearFinal, iMes, MesInicial, MesFinal : integer;
     i,{iInicial,}iFinal, iColumna: integer;

     Campos : Variant;
 procedure CreaDataset;
 begin
      BorraTabla( 'TMPCALHR','US_CODIGO' );
      with oZetaProvider do
           spCalendario := CreateQuery( Q_SP_CALENDARIO_HORAS );
 end;

 function GetListaCampos( sCampos : String ) : Variant;

  function NextToken( var sTarget: String ): String;
  var
     i: Integer;
  begin
       i := Pos( ',', sTarget );
       if ( i <> 0 ) then
       begin
            Result := System.Copy( sTarget, 1, ( i - 1 ) );
            System.Delete( sTarget, 1, i );
       end
       else
       begin
            Result := sTarget;
            sTarget := '';
       end;
  end;
 begin
      Result := VarArrayCreate( [1,Ord(High(eCampoCalendario))+1], varInteger);
      iFinal := 0;
      if StrLleno(sCampos) then
      begin
           while ( Length( sCampos ) > 0 )  do
           begin
                iFinal:= iFinal+1;
                Result[iFinal] := NextToken( sCampos );
           end
      end
 end;
begin
     Result := NIL;
     try
        CreaDataset;
        InitBroker;
        SQLBroker.Init(enEmpleado);
        with SQLBroker.Agente do
        begin
             AgregaColumna( 'COLABORA.CB_CODIGO', True, Entidad, tgNumero, 10, 'CB_CODIGO' );
             AgregaOrden( 'COLABORA.CB_CODIGO', True, Entidad );

             with oZetaProvider.ParamList do
             begin
                  dFinal:= ParamByName('dCalenFinal').AsDate;
                  dInicial:= ParamByName('dCalenInicial').AsDate;
                  Campos := GetListaCampos(ParamByName('ListaCampos').AsString);
                  if dInicial > dFinal then
                     raise Exception.Create( 'La Fecha Inicial:'+FechaAsStr(dInicial)+' debe Ser Menor que la Fecha Final '+FechaAsStr(dFinal));
             end;
             for i:=0 to oSQLAgente.NumFiltros - 1 do
             begin
                  with oSQLAgente.GetFiltro(i) do
                       if (DeDiccionario) then
                       begin
                            if NOT (Pos( 'TMPCALHR.TL_FEC_MIN',Formula )>0) AND
                               NOT (Pos( 'TMPCALHR.TL_CAMPO',Formula )>0) then
                            begin
                                 if Entidad = enCalendarioHoras then
                                    AgregaFiltro( StrTransAll(Formula,'TMPCALHR.CB_CODIGO','COLABORA.CB_CODIGO'),
                                                  DeDiccionario,
                                                  enEmpleado )
                                 else
                                     AgregaFiltro( Formula,
                                                   DeDiccionario,
                                                   Entidad );
                            end
                       end
             end;
        end;
        with SQLBroker,oZetaProvider do
             if BuildDataset(EmpresaActiva) then
             begin
               ParamAsInteger( spCalendario,'USUARIO', UsuarioActivo );
               try
                  oZetaProvider.EmpiezaTransaccion;

                  while NOT SuperReporte.DataSetReporte.EOF do
                  begin
                       YearInicial := TheYear(dInicial);
                       YearFinal := TheYear(dFinal);

                       for iYear := YearInicial to YearFinal do
                       begin
                            if iYear = YearInicial then
                               MesInicial := TheMonth(dInicial)
                            else MesInicial := 1;
                            if iYear = YearFinal then
                               MesFinal := TheMonth(dFinal)
                            else MesFinal := 12;

                            for iMes := MesInicial to MesFinal do
                            begin
                                 if (iYear=YearInicial) AND (iMes = MesInicial) then
                                    dFechaI := dInicial
                                 else dFechaI := EncodeDate( iYear, iMes, 1 );
                                 if (iYear=YearFinal) AND (iMes = MesFinal) then
                                    dFechaF := dFinal
                                 else dFechaF := EncodeDate( iYear, iMes, MonthDays[IsLeapYear(iYear),iMes] );

                                 ParamAsInteger( spCalendario,'EMPLEADO',SuperReporte.DataSetReporte.Fields[0].AsInteger );
                                 ParamAsInteger( spCalendario,'YEAR',iYear );
                                 ParamAsInteger( spCalendario,'MONTH',iMes );
                                 ParamAsDate( spCalendario,'FECHA_MES', FirstDayOfMonth(dFechaI) );
                                 ParamAsDate( spCalendario,'FECHA_INI', dFechaI );
                                 ParamAsDate( spCalendario,'FECHA_FIN', dFechaF );
                                 for i := 1 to iFinal do
                                 begin
                                      ParamAsInteger( spCalendario,'PRIMERO',i);
                                      ParamAsInteger( spCalendario,'CAMPO',Campos[i]);
                                      {$ifdef INTERBASE}
                                      spCalendario.Active:= TRUE;
                                      spCalendario.Active:= FALSE;
                                      {$endif}
                                      {$ifdef MSSQL}
                                      Ejecuta(spCalendario);
                                      {$endif}
                                 end;
                            end;//Mes
                       end;//Year
                       SuperReporte.DataSetReporte.Next;
                  end;
                  oZetaProvider.TerminaTransaccion(True);
               except
                     oZetaProvider.TerminaTransaccion(False);
               end;
             end;

        Result := TSQLAgente.Create;
        Result.Entidad := enCalendarioHoras;
        with oSQLAgente do
        begin
             for i := 0 to NumColumnas - 1 do
                 with GetColumna(i) do
                 begin
                      iColumna := Result.AgregaColumna( Formula,
                                                        DeDiccionario,
                                                        Entidad,
                                                        TipoFormula,
                                                        Ancho,
                                                        Alias,
                                                        Totalizacion,
                                                        GroupExp );

                      Result.GetColumna(iColumna).ValorConstante := ValorConstante;

                 end;

             for i:=0 to NumFiltros - 1 do
             begin
                  with GetFiltro(i) do
                       if NOT (DeDiccionario) then
                          Result.AgregaFiltro( Formula,
                                               DeDiccionario,
                                               Entidad );
             end;
        end;
        Result.AgregaFiltro( Format('TMPCALHR.US_CODIGO=%d',[oZetaProvider.UsuarioActivo]),TRUE,enCalendarioHoras);
        Result.AsignaOrdenes(oSQLAgente);
     finally
            FreeAndNil(spCalendario);
            ClearBroker;
     end;
end;

function TdmServerReportes.CalculaConteoPersonal(oSQLAgente: TSQLAgente;var Error: string): TSQLAgente;
begin
     with TConteoPersonal.Create(oZetaProvider) do
     begin
          ActualizaConteo;
          Free;
     end;

     Result := oSQLAgente;
end;

function TdmServerReportes.CalculaRotacion( oSQLAgente : TSQLAgente;
                                            var Error : string) : TSQLAgente;
 const
      K_COLABORA = 'COLABORA.';
      K_KARDEX = 'KARDEX.';
 var DataSetRota, SP_ROTACION : TZetaCursor;
     sInicial, sFinal, sFormula : string;
     i, iColumna: integer;

 procedure CreaDataset;
 begin
      BorraTabla('TMPROTA','TR_USER');
      BorraTabla('TMPROTAI','TR_USER');
      with oZetaProvider do
           DataSetRota := CreateQuery( Q_ROTA_INSERT );
 end;
 {$hints off}
 function CampoEsValido( const sFormula : string ) : Boolean;
  const Arreglo: array[1..18] of string = ('CB_CLASIFI','CB_CONTRAT',
                                    'CB_MOT_BAJ','CB_NIVEL1',
                                    'CB_NIVEL2','CB_NIVEL3',
                                    'CB_NIVEL4','CB_NIVEL5',
                                    'CB_NIVEL6','CB_NIVEL7',
                                    'CB_NIVEL8','CB_NIVEL9',
                                    'CB_NOMINA','CB_PATRON',
                                    'CB_PLAZA', 'CB_PUESTO',
                                    'CB_TABLASS','CB_TURNO');
  var i: integer;
 begin
      Result:= FALSE;
      for i:= 1 to High(Arreglo) do
      begin
           Result := Pos(Arreglo[i],sFormula)>0;
           if Result then
              Break;
      end;

 end;
 {$hints on}
begin
     Result := NIL;
     try
        CreaDataset;
        InitBroker;
        SQLBroker.Init(enEmpleado);
        with SQLBroker.Agente do
        begin
             if oSQLAgente.NumOrdenes > 0 then
             begin
                  {$ifdef FALSE}
                  AgregaColumna( oSQLAgente.GetOrden(0).Formula,
                                 FALSE, Entidad, tgAutomatico, 10, 'TD_GRUPO_ROTA' );

                  AgregaColumna( Format('LOOKUP(''%s'')',[oSQLAgente.GetOrden(0).Formula]),
                                 FALSE,enNinguno,tgTexto,30,'TD_GRUPO_DESC' );
                  AgregaOrden( oSQLAgente.GetOrden(0).Formula,
                               oSQLAgente.GetOrden(0).Ascendente,
                               oSQLAgente.GetOrden(0).Entidad );
                  {$else}
                  sFormula := UpperCase( Trim( oSQLAgente.GetOrden(0).Formula ) );

                  if ( Copy( sFormula, 1, 9 ) = K_COLABORA ) then
                  begin
                       AgregaColumna( sFormula,
                                      FALSE, Entidad, tgAutomatico, 10, 'TD_GRUPO_ROTA' );

                       AgregaColumna( Format('LOOKUP(''%s'')',[sFormula]),
                                      FALSE,enNinguno,tgTexto,30,'TD_GRUPO_DESC' );
                       AgregaOrden( sFormula,
                                    oSQLAgente.GetOrden(0).Ascendente,
                                    oSQLAgente.GetOrden(0).Entidad );
                  end
                  else if ( Copy( sFormula, 1, 7 ) = K_KARDEX ) and CampoEsValido(sFormula) then
                  begin
                       AgregaColumna( Format( 'FECHA_KARDEX(%s, %s)', [ Comillas( sFormula ), Comillas(FormatDateTime( 'dd/mm/yyyy', oZetaProvider.ParamList.ParamByName('dRotaFinal').AsDate))] ),
                                 FALSE, Entidad, tgAutomatico, 10, 'TD_GRUPO_ROTA' );

                       if sFormula = 'KARDEX.CB_NOMINA' then
                          AgregaColumna( Format( 'TF(14,FECHA_KARDEX(%s, %s))', [Comillas( sFormula ), Comillas(FormatDateTime( 'dd/mm/yyyy', oZetaProvider.ParamList.ParamByName('dRotaFinal').AsDate))] ),
                                         FALSE,enNinguno,tgTexto,30,'TD_GRUPO_DESC' )
                       else
                           AgregaColumna( Format( 'T( 21,FECHA_KARDEX(%s, %s), 1 )', [Comillas( sFormula ), Comillas(FormatDateTime( 'dd/mm/yyyy', oZetaProvider.ParamList.ParamByName('dRotaFinal').AsDate))] ),
                                          FALSE,enNinguno,tgTexto,30,'TD_GRUPO_DESC' );
                       AgregaOrden( Format( 'FECHA_KARDEX(%s, %s)', [Comillas( sFormula ), Comillas(FormatDateTime( 'dd/mm/yyyy', oZetaProvider.ParamList.ParamByName('dRotaFinal').AsDate))] ),
                                    oSQLAgente.GetOrden(0).Ascendente,
                                    oSQLAgente.GetOrden(0).Entidad,
                                    FALSE );
                  end
                  else
                  begin
                       raise Exception.Create('La f�rmula del orden/grupo de Rotaci�n no es v�lido');
                  end;

                  {$endif}
             end
             else
             begin
                  AgregaColumna( '''#EMPR#''', TRUE, Entidad,
                                 tgTexto, 10, 'TD_GRUPO_ROTA' );
                  AgregaColumna( '''Totales de Empresa''',
                                 TRUE,enNinguno,tgTexto,30,'TD_GRUPO_DESC');
             end;

             AgregaColumna( 'COLABORA.CB_CODIGO', True, Entidad, tgNumero, 10, 'CB_CODIGO' );
             AgregaColumna( 'COLABORA.CB_FEC_ING', True, Entidad, tgFecha, 10, 'CB_FEC_ING' );
             AgregaColumna( 'COLABORA.CB_FEC_BAJ', True, Entidad, tgFecha, 10, 'CB_FEC_BAJ' );

             with oZetaProvider.ParamList do
             begin
                  sFinal:= Comillas(FormatDateTime( 'mm/dd/yyyy', ParamByName('dRotaFinal').AsDate));
                  sInicial:= Comillas(FormatDateTime( 'mm/dd/yyyy', ParamByName('dRotaInicial').AsDate));
             end;

             AgregaFiltro( Format( '(((COLABORA.CB_FEC_ING <= %s) AND '+
                                   '(COLABORA.CB_FEC_BAJ < COLABORA.CB_FEC_ING OR COLABORA.CB_FEC_BAJ>=%s)) OR '+
                                   '(COLABORA.CB_FEC_BAJ < COLABORA.CB_FEC_ING AND COLABORA.CB_FEC_BAJ>=%s))',
                           [sFinal,sInicial,sInicial]),
                           TRUE,
                           enEmpleado );
             SQLBroker.FiltroConfidencial( oZetaProvider.EmpresaActiva );

             for i:=0 to oSQLAgente.NumFiltros - 1 do
             begin
                  with oSQLAgente.GetFiltro(i) do
                       if NOT ((DeDiccionario) AND
                              (Pos( 'TMPROTA.TR_FECHA',Formula )>0)) then
                              AgregaFiltro( Formula,
                                            DeDiccionario,
                                            Entidad );
             end;
        end;
        with SQLBroker do
        begin
             if BuildDataset(oZetaProvider.EmpresaActiva) then
             begin
                  try
                     oZetaProvider.EmpiezaTransaccion;

                      with SuperReporte.DataSetReporte do
                          while NOT EOF do
                          begin
                               with oZetaProvider do
                               begin
                                    ParamAsInteger( DataSetRota, 'TR_USER', UsuarioActivo );
                                    ParamAsChar( DataSetRota, 'TR_GRUPO', FieldByName( 'TD_GRUPO_ROTA' ).AsString, 6 );
                                    ParamAsVarChar( DataSetRota, 'TR_DESCRIP', FieldByName( 'TD_GRUPO_DESC').AsString, 30 );
                                    ParamAsInteger( DataSetRota, 'CB_CODIGO', FieldByName('CB_CODIGO').AsInteger );
                                    ParamAsDate( DataSetRota, 'CB_FEC_ING', FieldByName('CB_FEC_ING').AsDateTime );
                                    ParamAsDate( DataSetRota, 'CB_FEC_BAJ', FieldByName('CB_FEC_BAJ').AsDateTime );
                                    Ejecuta( DataSetRota );
                               end;
                               Next;
                          end;
                     oZetaProvider.TerminaTransaccion(True);
                  except
                        oZetaProvider.TerminaTransaccion(False);
                  end;
             end;
        end;

        with oZetaProvider do
        begin
             SP_ROTACION := CreateQuery( Format( Q_ROTA_SP, [UsuarioActivo,sInicial,sFinal] ) );
             try
                oZetaProvider.EmpiezaTransaccion;
                Ejecuta( SP_Rotacion );
                oZetaProvider.TerminaTransaccion(True);
             except
                   oZetaProvider.TerminaTransaccion(False);
             end;
        end;

        Result := TSQLAgente.Create;
        Result.Entidad := enRotacion;
        with oSQLAgente do
             for i := 0 to NumColumnas - 1 do
                 with GetColumna(i) do
                      if (Dentro( Entidad ,[enRotacion,enNinguno])) AND NOT GroupExp then
                      begin
                           iColumna := Result.AgregaColumna( Formula,
                                                             DeDiccionario,
                                                             Entidad,
                                                             TipoFormula,
                                                             Ancho,
                                                             Alias,
                                                             Totalizacion,
                                                             GroupExp );
                           Result.GetColumna(iColumna).ValorConstante := ValorConstante;
                      end;
        Result.AgregaFiltro( Format('TMPROTA.TR_USER=%d',[oZetaProvider.UsuarioActivo]),TRUE,enRotacion );
        Result.AgregaOrden( 'TMPROTA.TR_GRUPO',TRUE,enRotacion );

     finally
            FreeAndNil(DataSetRota);
            FreeAndNil(SP_Rotacion);
            ClearBroker;
     end;

end;
type TLimites = class (TObject)
private
    FCuantos: Double;
    FSuma: Double;
    FMinimo: Double;
    FMaximo: Double;
    procedure SetValor( rValor : Double );
    function GetPromedio: Double;
public
     constructor Create;
     procedure Clear;
     property Maximo : Double read FMaximo;
     property Minimo : Double read FMinimo;
     property Suma : Double read FSuma;
     property Promedio : Double read GetPromedio;
     property Cuantos : Double read FCuantos;
     property Valor : Double write SetValor;
end;

procedure TLimites.Clear;
begin
     FCuantos:= 0;
     FSuma:= 0;
     FMinimo:= 99999999;
     FMaximo:= 0;
end;

constructor TLimites.Create;
begin
     Clear;
end;

function TLimites.GetPromedio: Double;
begin
     Result := FSuma/FCuantos;
end;

procedure TLimites.SetValor(rValor: Double);
begin
     FCuantos := FCuantos + 1;
     FMaximo := ZetaCommonTools.rMax(FMaximo,rValor);
     FMinimo := ZetaCommonTools.rMin(FMinimo,rValor);
     FSuma := FSuma + rValor;
end;

function TdmServerReportes.CalculaDemografica( oSQLAgente : TSQLAgente;
                                               var Error : string) : TSQLAgente;

 const Q_YEAR = 365.4;
 var oEmpleado, oRevision, oEdad,
     oAntig, oEvalua, oSalario, oHijos : TLimites;
     i, TD_HOMBRE, TD_MUJER, CB_PASAPOR, CB_EST_HOY,
     CB_IDIOMA, CB_AUTOSAL, CB_PARIENTE : Integer;
     sGrupo, sDescripcion : string;
     dFecha : TDate;
     DatasetDemo : TZetaCursor;
     iColumna : integer;

 procedure CreaDataset;
 begin
      BorraTabla( 'TMPDEMO','TD_USER' );
      with oZetaProvider do
           DataSetDemo := CreateQuery( Q_DEMO_INSERT );
 end;

 procedure AgregaRegistro;
 begin
      with oZetaProvider do
      begin
           ParamAsInteger( DataSetDemo, 'TD_USER', UsuarioActivo );
           ParamAsChar( DataSetDemo, 'TD_GRUPO', sGrupo, 6 );
           ParamAsVarChar( DataSetDemo, 'TD_DESCRIP', sDescripcion, 30 );
           ParamAsFloat( DataSetDemo, 'TD_EMPL', oEmpleado.Cuantos);
           ParamAsFloat( DataSetDemo, 'TD_EMPMIN', oEmpleado.Minimo);
           ParamAsFloat( DataSetDemo, 'TD_EMPMAX', oEmpleado.Maximo);
           ParamAsInteger( DataSetDemo, 'TD_HOMBRES', TD_HOMBRE);
           ParamAsInteger( DataSetDemo, 'TD_MUJERES', TD_MUJER);
           ParamAsInteger( DataSetDemo, 'TD_FAMILIA', CB_PARIENTE);
           ParamAsInteger( DataSetDemo, 'TD_MICA', CB_PASAPOR);
           ParamAsInteger( DataSetDemo, 'TD_ESTUDIO', CB_EST_HOY);
           ParamAsInteger( DataSetDemo, 'TD_IDIOMA', CB_IDIOMA);
           ParamAsInteger( DataSetDemo, 'TD_TABULA', CB_AUTOSAL);

           ParamAsFloat( DataSetDemo, 'TD_EDADMIN', oEdad.Minimo/Q_YEAR);
           ParamAsFloat( DataSetDemo, 'TD_EDADMAX', oEdad.Maximo/Q_YEAR);
           ParamAsFloat( DataSetDemo, 'TD_EDADAVG', oEdad.Promedio/Q_YEAR);
           ParamAsFloat( DataSetDemo, 'TD_ANTMIN', oAntig.Minimo/Q_YEAR  );
           ParamAsFloat( DataSetDemo, 'TD_ANTMAX', oAntig.Maximo/Q_YEAR  );
           ParamAsFloat( DataSetDemo, 'TD_ANTAVG', oAntig.Promedio/Q_YEAR);
           ParamAsFloat( DataSetDemo, 'TD_RESMIN', oRevision.Minimo/Q_YEAR  );
           ParamAsFloat( DataSetDemo, 'TD_RESMAX', oRevision.Maximo/Q_YEAR  );
           ParamAsFloat( DataSetDemo, 'TD_RESAVG', oRevision.Promedio/Q_YEAR);
           ParamAsFloat( DataSetDemo, 'TD_HIJOMIN', oHijos.Minimo );
           ParamAsFloat( DataSetDemo, 'TD_HIJOMAX', oHijos.Maximo );
           ParamAsFloat( DataSetDemo, 'TD_HIJOAVG', oHijos.Promedio);
           ParamAsFloat( DataSetDemo, 'TD_SALMIN', oSalario.Minimo);
           ParamAsFloat( DataSetDemo, 'TD_SALMAX', oSalario.Maximo);
           ParamAsFloat( DataSetDemo, 'TD_SALAVG', oSalario.Promedio);
           ParamAsFloat( DataSetDemo, 'TD_DIASMIN', oEvalua.Minimo/Q_YEAR);
           ParamAsFloat( DataSetDemo, 'TD_DIASMAX', oEvalua.Maximo/Q_YEAR);
           ParamAsFloat( DataSetDemo, 'TD_DIASAVG', oEvalua.Promedio/Q_YEAR);
           Ejecuta( DataSetDemo );
      end;
 end;
 procedure ClearValores;
 begin
      oEmpleado.Clear;
      oRevision.Clear;
      oEdad.Clear;
      oAntig.Clear;
      oEvalua.Clear;
      oSalario.Clear;
      oHijos.Clear;

      TD_HOMBRE := 0;
      TD_MUJER := 0;
      CB_PASAPOR := 0;
      CB_EST_HOY := 0;
      CB_IDIOMA := 0;
      CB_AUTOSAL := 0;
      CB_PARIENTE := 0;
 end;
begin
     try
        CreaDataset;
        InitBroker;
        SQLBroker.Init(enEmpleado);
        with SQLBroker.Agente do
        begin
             if oSQLAgente.NumOrdenes > 0 then
             begin
                  AgregaColumna( oSQLAgente.GetOrden(0).Formula,
                                 FALSE, Entidad, tgAutomatico, 10, 'TD_GRUPO_DEMO' );

                  AgregaColumna( Format('LOOKUP(''%s'')',[oSQLAgente.GetOrden(0).Formula]),
                                 FALSE,enNinguno,tgTexto,30,'TD_GRUPO_DESC' );
                  AgregaOrden( oSQLAgente.GetOrden(0).Formula,
                               oSQLAgente.GetOrden(0).Ascendente,
                               oSQLAgente.GetOrden(0).Entidad );
             end
             else
             begin
                  AgregaColumna( '''#EMPR#''', TRUE, Entidad,
                                 tgTexto, 10, 'TD_GRUPO_DEMO' );
                  AgregaColumna( '''Totales de Empresa''',
                                 TRUE,enNinguno,tgTexto,30,'TD_GRUPO_DESC');
             end;

             AgregaColumna( 'COLABORA.CB_CODIGO', True, Entidad, tgNumero, 10, 'CB_CODIGO' );
             AgregaColumna( 'COLABORA.CB_FEC_RES', True, Entidad, tgFecha, 10, 'CB_FEC_RES' );
             AgregaColumna( 'COLABORA.CB_FEC_NAC', True, Entidad, tgFecha, 10, 'CB_FEC_NAC' );
             AgregaColumna( 'COLABORA.CB_FEC_ANT', True, Entidad, tgFecha, 10, 'CB_FEC_ANT' );
             AgregaColumna( 'COLABORA.CB_LAST_EV', True, Entidad, tgFecha, 10, 'CB_LAST_EV' );
             AgregaColumna( 'COLABORA.CB_SALARIO', True, Entidad, tgFloat, 10, 'CB_SALARIO' );
             AgregaColumna( 'IF(COLABORA.CB_SEXO = ''M'', 1, 0 )', False, Entidad, tgNumero, 10,'TD_HOMBRE' );
             AgregaColumna( 'IF(COLABORA.CB_SEXO = ''F'', 1, 0 )', False, Entidad, tgNumero, 10,'TD_MUJER' );
             AgregaColumna( 'IF(COLABORA.CB_PASAPOR = ''S'', 1, 0 )',False, Entidad, tgNumero, 10,'TD_MICA');
             AgregaColumna( 'IF(COLABORA.CB_EST_HOY = ''S'', 1, 0 )',False, Entidad, tgNumero, 10,'TD_ESTUDIO');
             AgregaColumna( 'IF(COLABORA.CB_IDIOMA > '' '', 1, 0 )',False, Entidad, tgNumero, 10,'TD_IDIOMA');
             AgregaColumna( 'IF(COLABORA.CB_AUTOSAL = ''S'', 1, 0 )',False, Entidad, tgNumero, 10,'TD_TABULA');
             AgregaColumna( '@SELECT COUNT(*) FROM PARIENTE WHERE PARIENTE.CB_CODIGO = COLABORA.CB_CODIGO AND PA_TRABAJA<>0',
                            FALSE, enNinguno, tgNumero, 10, 'TD_PARIENTE' );
             AgregaColumna( '@SELECT COUNT(*) FROM PARIENTE WHERE PARIENTE.CB_CODIGO = COLABORA.CB_CODIGO AND PA_RELACIO = 3',
                            FALSE, enNinguno, tgNumero, 10, 'TD_HIJOS' );
             for i:=0 to oSQLAgente.NumFiltros - 1 do
             begin
                 AgregaFiltro( oSQLAgente.GetFiltro(i).Formula,
                               oSQLAgente.GetFiltro(i).DeDiccionario,
                               oSQLAgente.GetFiltro(i).Entidad );
             end;
             SQLBroker.FiltroConfidencial( oZetaProvider.EmpresaActiva );
        end;
        with SQLBroker do
        begin
             if BuildDataset(oZetaProvider.EmpresaActiva) then
             begin
                  oEmpleado := TLimites.Create;
                  oRevision := TLimites.Create;
                  oEdad := TLimites.Create;
                  oAntig := TLimites.Create;
                  oEvalua := TLimites.Create;
                  oSalario := TLimites.Create;
                  oHijos := TLimites.Create;

                  ClearValores;

                  dFecha := oZetaProvider.FechaDefault;

                  with SuperReporte.DataSetReporte do
                  begin
                       sGrupo := Fields[0].AsString;
                       try
                          oZetaProvider.EmpiezaTransaccion;

                          while NOT EOF do
                          begin

                               oEmpleado.Valor := FieldByName('CB_CODIGO').AsInteger;
                               oRevision.Valor := dFecha - FieldByName('CB_FEC_RES').AsDateTime + 1;
                               oEdad.Valor := dFecha - FieldByName('CB_FEC_NAC').AsDateTime + 1;
                               oAntig.Valor := dFecha - FieldByName('CB_FEC_ANT').AsDateTime + 1;
                               oEvalua.Valor := dFecha - FieldByName('CB_LAST_EV').AsDateTime + 1;
                               oSalario.Valor := FieldByName('CB_SALARIO').AsFloat;
                               oHijos.Valor := FieldByName('TD_HIJOS').AsFloat;
                               TD_HOMBRE := TD_HOMBRE + FieldByName('TD_HOMBRE').AsInteger;
                               TD_MUJER := TD_MUJER + FieldByName('TD_MUJER').AsInteger;
                               CB_PASAPOR := CB_PASAPOR + FieldByName('TD_MICA').AsInteger;
                               CB_EST_HOY := CB_EST_HOY + FieldByName('TD_ESTUDIO').AsInteger;
                               CB_IDIOMA := CB_IDIOMA + FieldByName('TD_IDIOMA').AsInteger;
                               CB_AUTOSAL := CB_AUTOSAL + FieldByName('TD_TABULA').AsInteger;
                               CB_PARIENTE := CB_PARIENTE + FieldByName('TD_PARIENTE').AsInteger;

                               sDescripcion := FieldByName('TD_GRUPO_DESC').AsString;

                               Next;
                               if sGrupo <> Fields[0].AsString then
                               begin
                                    AgregaRegistro;
                                    ClearValores;
                               end;
                               sGrupo := Fields[0].AsString;
                          end;
                          AgregaRegistro;
                          oZetaProvider.TerminaTransaccion(True);
                       except
                             oZetaProvider.TerminaTransaccion(False);
                       end;

                  end;
             end;
        end;
        Result := TSQLAgente.Create;
        Result.Entidad := enDemografica;
        with oSQLAgente do
             for i := 0 to NumColumnas - 1 do
                 with GetColumna(i) do
                 begin
                      iColumna := Result.AgregaColumna( Formula,
                                                        DeDiccionario,
                                                        Entidad,
                                                        TipoFormula,
                                                        Ancho,
                                                        Alias,
                                                        Totalizacion,
                                                        GroupExp );
                      Result.GetColumna(iColumna).ValorConstante := ValorConstante;

                 end;
        Result.AgregaFiltro( Format('TMPDEMO.TD_USER=%d',[oZetaProvider.UsuarioActivo]),TRUE,enDemografica );
        Result.AgregaOrden( 'TMPDEMO.TD_GRUPO',TRUE,enDemografica );
     finally
            ClearBroker;
     end;
end;


function TdmServerReportes.CalculaConciliacion(oSQLAgente: TSQLAgente; var Error: string): TSQLAgente;
 var iFolio, iInicial, iFinal, iCuantos, i, iColumna : integer;
     FConcilia, FQuery : TZetaCursor;
begin
     BorraTabla('TMPFOLIO','TO_USER');
     with oZetaProvider do
     begin
          iFolio := ParamList.ParamByName('iFolio').AsInteger;

          FQuery := CreateQuery( Format( Q_CONCILIA_EXTREMOS,
                                       [iFolio,iFolio,
                                        DatosPeriodo.Year,
                                        Ord(DatosPeriodo.Tipo),
                                        DatosPeriodo.Numero,
                                        iFolio] ));

          with FQuery do
          begin
               FQuery.Active := TRUE;
               iInicial := Fields[0].AsInteger;
               iFinal := Fields[1].AsInteger;
               iCuantos := Fields[2].AsInteger;
          end;

          InitBroker;
          SQLBroker.Init(enNomina);

          with SQLBroker.Agente do
          begin
               AgregaColumna( Format('NOMINA.NO_FOLIO_%d',[iFolio]), True, Entidad, tgNumero, 10, 'NO_FOLIO' );
               AgregaColumna( 'NOMINA.CB_CODIGO', True, Entidad, tgNumero, 10, 'CB_CODIGO' );
               AgregaFiltro(Format('NOMINA.PE_YEAR = %d',[DatosPeriodo.Year]), TRUE,Entidad);
               AgregaFiltro(Format('NOMINA.PE_TIPO = %d',[Ord(DatosPeriodo.Tipo)]), TRUE,Entidad);
               AgregaFiltro(Format('NOMINA.PE_NUMERO = %d',[DatosPeriodo.Numero]), TRUE,Entidad);
               AgregaFiltro(Format('NOMINA.NO_FOLIO_%d >0',[iFolio]), TRUE,Entidad);
          end;
          with SQLBroker do
          begin
               BuildDataSet(EmpresaActiva);
               with SuperReporte.DataSetReporte do
               begin
                    PreparaQuery(FQuery, Q_CONCILIA_INSERT);
                    try
                       oZetaProvider.EmpiezaTransaccion;
                       while ( NOT EOF ) do
                       begin
                            ParamAsInteger( FQuery,'TO_USER', UsuarioActivo );
                            ParamAsInteger( FQuery,'TO_FOLIO',FieldByName('NO_FOLIO').AsInteger );
                            ParamAsInteger( FQuery,'CB_CODIGO',FieldByName('CB_CODIGO').AsInteger );
                            ParamAsInteger( FQuery,'PE_YEAR',DatosPeriodo.Year );
                            ParamAsInteger( FQuery,'PE_TIPO',Ord(DatosPeriodo.Tipo) );
                            ParamAsInteger( FQuery,'PE_NUMERO',DatosPeriodo.Numero );
                            Ejecuta(FQuery);
                            Next;
                       end;
                       oZetaProvider.TerminaTransaccion(True);
                    except
                          oZetaProvider.TerminaTransaccion(False);

                    end;
               end;
          end;
          if iCuantos < (iFinal-iInicial+1) then
          begin
               FConcilia := CreateQuery( Format( Q_CONCILIA_NO_EXISTEN,
                                     [UsuarioActivo,
                                      DatosPeriodo.Year,
                                      Ord(DatosPeriodo.Tipo),
                                      DatosPeriodo.Numero,
                                      iInicial,iFinal,
                                      iFolio,
                                      DatosPeriodo.Year,
                                      Ord(DatosPeriodo.Tipo),
                                      DatosPeriodo.Numero]) );
               try
                  oZetaProvider.EmpiezaTransaccion;
                  Ejecuta(FConcilia);
                  oZetaProvider.TerminaTransaccion(True);
               except
                     oZetaProvider.TerminaTransaccion(False);
               end;
          end;
          Result := TSQLAgente.Create;
          Result.Entidad := enConcilia;
          with oSQLAgente do
          begin
               for i := 0 to NumColumnas - 1 do
                   with GetColumna(i) do
                   begin
                        iColumna := Result.AgregaColumna( Formula,
                                                          DeDiccionario,
                                                          Entidad,
                                                          TipoFormula,
                                                          Ancho,
                                                          Alias,
                                                          Totalizacion,
                                                          GroupExp );
                        Result.GetColumna(iColumna).ValorConstante := ValorConstante;

                   end;
               for i := 0 to NumOrdenes - 1 do
                   with GetOrden(i) do
                        Result.AgregaOrden( Formula,
                                            TRUE,
                                            Entidad );
          end;

          Result.AgregaFiltro( Format('TMPFOLIO.TO_USER=%d',[oZetaProvider.UsuarioActivo]),TRUE,enConcilia );
     end;
end;

procedure TdmServerReportes.PreparaAgenteTotalesLista(oSQLAgente: TSQLAgente);
 var SQL : string;
begin
     SQL := oSQLAgente.SQL.Text;
     SQL := StrTransAll(SQL,Grupo1,'TMPLISTA.TA_NIVEL1');
     SQL := StrTransAll(SQL,Grupo2,'TMPLISTA.TA_NIVEL2');
     SQL := StrTransAll(SQL,Grupo3,'TMPLISTA.TA_NIVEL3');
     SQL := StrTransAll(SQL,Grupo4,'TMPLISTA.TA_NIVEL4');
     SQL := StrTransAll(SQL,Grupo5,'TMPLISTA.TA_NIVEL5');
     oSQLAgente.SQL.Text := SQL;
end;

function TdmServerReportes.CalculaListadoTotales(oSQLAgente: TSQLAgente; var Error: string): TSQLAgente;
 var nGruposListado, iColumna : integer;
     TA_NIVEL1,TA_NIVEL2,TA_NIVEL3,TA_NIVEL4,TA_NIVEL5 : string;
     FTablaPrincipal : string;

 function GetNombreNivel(const Posicion : integer ) : string;
 begin
      case Posicion of
           1: Result := StrDef( GRUPO1, Comillas(Q_CHR_RELLENO));
           2: Result := StrDef( GRUPO2, Comillas(Q_CHR_RELLENO));
           3: Result := StrDef( GRUPO3, Comillas(Q_CHR_RELLENO));
           4: Result := StrDef( GRUPO4, Comillas(Q_CHR_RELLENO));
           5: Result := StrDef( GRUPO5, Comillas(Q_CHR_RELLENO));
      end;
 end;

 function GetCodigoGrupo(const Posicion: integer; var sGrupo : string ) : Boolean;
 begin
      Result := GetNombreNivel(Posicion) <> Comillas(Q_CHR_RELLENO);
      if Result then
         sGrupo := 'TA_NIVEL'+IntToStr(Posicion);
 end;

 function CambiaTabla( const sTexto : string ) : string;
 begin
      Result := sTexto;
      Result := StrTransAll(sTexto,FTablaPrincipal,'MOVIMIEN');
 end;

 var i, FUsuario : integer;
     CB_CODIGO : integer;
     lSoloTotales : Boolean;
     oEntidad : TipoEntidad;
     FMovimien : TServerDataSet;
     FTmpNom, FInsertTmpLista, FInsertTmpNom : TZetaCursor;
     oBroker : TSQLBroker;

  function GetFiltroTotales(const Posicion : integer ) : string;
   var i : integer;
       sNivel : string;
  begin
       Result := '';
       for i := 1 to Posicion do
       begin
            sNivel := TA_NIVEL+IntToStr(i);
            Result := Result +
                      sNivel+' = ' +
                      EntreComillas(Trim(FMovimien.FieldByName(sNivel).AsString)) + ' AND ';
       end;
       Result := Copy(Result,1,Length(Result)-5);
  end;

  procedure InicializaListadoTotales;
  begin
       oBroker := TSQLBroker.Create(oZetaCreator);

       with oZetaProvider do
       begin
            with ParamList do
            begin
                 lSoloTotales := ParamByName('SoloTotales').AsBoolean;
                 GRUPO1 := ParamByName('TA_NIVEL1').AsString;
                 GRUPO2 := ParamByName('TA_NIVEL2').AsString;
                 GRUPO3 := ParamByName('TA_NIVEL3').AsString;
                 GRUPO4 := ParamByName('TA_NIVEL4').AsString;
                 GRUPO5 := ParamByName('TA_NIVEL5').AsString;
            end;

            FUsuario := UsuarioActivo;

            FInsertTmpLista := CreateQuery;
            PreparaQuery(FInsertTmpLista,Q_TOTALES_INSERT_LISTA);
            ParamAsInteger(FInsertTmpLista,'TA_USER',FUsuario);

            FInsertTmpNom := CreateQuery;
            PreparaQuery(FInsertTmpNom,Q_TOTALES_INSERT_NOMINA);
            ParamAsInteger(FInsertTmpNom,'TA_USER',FUsuario);

       end;
  end;

  procedure GetBuildDataset;
  begin
       with oBroker do
       begin
            BuildDataSet(oZetaProvider.EmpresaActiva);
            //FMovimien := SuperReporte.QueryReporte;
            FMovimien := SuperReporte.DataSetReporte;
            //FMovimien.First;
       end;
  end;
  procedure AgregaColumnasNomina( oAgente: TSQLAgente;
                                  const oEntidad : TipoEntidad = enNomina;
                                  const sTabla : string = 'NOMINA';
                                  const lSumar : Boolean = FALSE);
   function GetName( const sName : string ) : string;
   begin
        Result := sTabla+'.'+sName;
        if lSumar then
           Result := 'SUM('+sName+')'
   end;
  begin
       with oAgente do
       begin
            AgregaColumna( GetName('CB_SAL_INT'),True, oEntidad, tgFloat, 10, 'CB_SAL_INT' );
            AgregaColumna( GetName('CB_SALARIO'),True, oEntidad, tgFloat, 10, 'CB_SALARIO' );
            AgregaColumna( GetName('NO_DIAS'),True, oEntidad, tgFloat, 10, 'NO_DIAS' );
            AgregaColumna( GetName('NO_ADICION'),True, oEntidad, tgFloat, 10, 'NO_ADICION' );
            AgregaColumna( GetName('NO_DEDUCCI'),True, oEntidad, tgFloat, 10, 'NO_DEDUCCI' );
            AgregaColumna( GetName('NO_NETO'),True, oEntidad, tgFloat, 10, 'NO_NETO' );
            AgregaColumna( GetName('NO_DES_TRA'),True, oEntidad, tgFloat, 10, 'NO_DES_TRA' );
            AgregaColumna( GetName('NO_DIAS_AG'),True, oEntidad, tgFloat, 10, 'NO_DIAS_AG' );
            AgregaColumna( GetName('NO_DIAS_VA'),True, oEntidad, tgFloat, 10, 'NO_DIAS_VA' );
            AgregaColumna( GetName('NO_DOBLES'),True, oEntidad, tgFloat, 10, 'NO_DOBLES' );
            AgregaColumna( GetName('NO_EXTRAS'),True, oEntidad, tgFloat, 10, 'NO_EXTRAS' );
            AgregaColumna( GetName('NO_FES_PAG'),True, oEntidad, tgFloat, 10, 'NO_FES_PAG' );
            AgregaColumna( GetName('NO_FES_TRA'),True, oEntidad, tgFloat, 10, 'NO_FES_TRA' );
            AgregaColumna( GetName('NO_HORA_CG'),True, oEntidad, tgFloat, 10, 'NO_HORA_CG' );
            AgregaColumna( GetName('NO_HORA_SG'),True, oEntidad, tgFloat, 10, 'NO_HORA_SG' );
            AgregaColumna( GetName('NO_HORAS'),True, oEntidad, tgFloat, 10, 'NO_HORAS' );
            AgregaColumna( GetName('NO_IMP_CAL'),True, oEntidad, tgFloat, 10, 'NO_IMP_CAL' );
            AgregaColumna( GetName('NO_JORNADA'),True, oEntidad, tgFloat, 10, 'NO_JORNADA' );
            AgregaColumna( GetName('NO_PER_CAL'),True, oEntidad, tgFloat, 10, 'NO_PER_CAL' );
            AgregaColumna( GetName('NO_PER_MEN'),True, oEntidad, tgFloat, 10, 'NO_PER_MEN' );
            AgregaColumna( GetName('NO_PERCEPC'),True, oEntidad, tgFloat, 10, 'NO_PERCEPC' );
            AgregaColumna( GetName('NO_TARDES'),True, oEntidad, tgFloat, 10, 'NO_TARDES' );
            AgregaColumna( GetName('NO_TRIPLES'),True, oEntidad, tgFloat, 10, 'NO_TRIPLES' );
            AgregaColumna( GetName('NO_TOT_PRE'),True, oEntidad, tgFloat, 10, 'NO_TOT_PRE' );
            AgregaColumna( GetName('NO_VAC_TRA'),True, oEntidad, tgFloat, 10, 'NO_VAC_TRA' );
            AgregaColumna( GetName('NO_X_CAL'),True, oEntidad, tgFloat, 10, 'NO_X_CAL' );
            AgregaColumna( GetName('NO_X_ISPT'),True, oEntidad, tgFloat, 10, 'NO_X_ISPT' );
            AgregaColumna( GetName('NO_X_MENS'),True, oEntidad, tgFloat, 10, 'NO_X_MENS' );
            AgregaColumna( GetName('NO_D_TURNO'),True, oEntidad, tgFloat, 10, 'NO_D_TURNO' );
            AgregaColumna( GetName('NO_DIAS_AJ'),True, oEntidad, tgFloat, 10, 'NO_DIAS_AJ' );
            AgregaColumna( GetName('NO_DIAS_AS'),True, oEntidad, tgFloat, 10, 'NO_DIAS_AS' );
            AgregaColumna( GetName('NO_DIAS_CG'),True, oEntidad, tgFloat, 10, 'NO_DIAS_CG' );
            AgregaColumna( GetName('NO_DIAS_EM'),True, oEntidad, tgFloat, 10, 'NO_DIAS_EM' );
            AgregaColumna( GetName('NO_DIAS_FI'),True, oEntidad, tgFloat, 10, 'NO_DIAS_FI' );
            AgregaColumna( GetName('NO_DIAS_FJ'),True, oEntidad, tgFloat, 10, 'NO_DIAS_FJ' );
            AgregaColumna( GetName('NO_DIAS_FV'),True, oEntidad, tgFloat, 10, 'NO_DIAS_FV' );
            AgregaColumna( GetName('NO_DIAS_IN'),True, oEntidad, tgFloat, 10, 'NO_DIAS_IN' );
            AgregaColumna( GetName('NO_DIAS_NT'),True, oEntidad, tgFloat, 10, 'NO_DIAS_NT' );
            AgregaColumna( GetName('NO_DIAS_OT'),True, oEntidad, tgFloat, 10, 'NO_DIAS_OT' );
            AgregaColumna( GetName('NO_DIAS_RE'),True, oEntidad, tgFloat, 10, 'NO_DIAS_RE' );
            AgregaColumna( GetName('NO_DIAS_SG'),True, oEntidad, tgFloat, 10, 'NO_DIAS_SG' );
            AgregaColumna( GetName('NO_DIAS_SS'),True, oEntidad, tgFloat, 10, 'NO_DIAS_SS' );
            AgregaColumna( GetName('NO_DIAS_SU'),True, oEntidad, tgFloat, 10, 'NO_DIAS_SU' );
            AgregaColumna( GetName('NO_HORA_PD'),True, oEntidad, tgFloat, 10, 'NO_HORA_PD' );
            AgregaColumna( GetName('NO_EXENTAS'),True, oEntidad, tgFloat, 10, 'NO_EXENTAS' );
            AgregaColumna( GetName('NO_DIAS_BA'),True, oEntidad, tgFloat, 10, 'NO_DIAS_BA' );
            AgregaColumna( GetName('NO_DIAS_SI'),True, oEntidad, tgFloat, 10, 'NO_DIAS_SI' );
            AgregaColumna( GetName('NO_HORAPDT'),True, oEntidad, tgFloat, 10, 'NO_HORAPDT' );
            AgregaColumna( GetName('NO_HORASNT'),True, oEntidad, tgFloat, 10, 'NO_HORASNT' );
            AgregaColumna( GetName('NO_PREV_GR'),True, oEntidad, tgFloat, 10, 'NO_PREV_GR' );
            AgregaColumna( GetName('NO_DIAS_PV'),True, oEntidad, tgFloat, 10, 'NO_DIAS_PV' );
            AgregaColumna( GetName('NO_DIAS_VJ'),True, oEntidad, tgFloat, 10, 'NO_DIAS_VJ' );
            AgregaColumna( GetName('NO_PRE_EXT'),True, oEntidad, tgFloat, 10, 'NO_PRE_EXT' );
            AgregaColumna( GetName('NO_PER_ISN'),True, oEntidad, tgFloat, 10, 'NO_PER_ISN' );
            AgregaColumna( GetName('NO_DIAS_IH'),True, oEntidad, tgFloat, 10, 'NO_DIAS_IH' );
            AgregaColumna( GetName('NO_DIAS_ID'),True, oEntidad, tgFloat, 10, 'NO_DIAS_ID' );
            AgregaColumna( GetName('NO_DIAS_IT'),True, oEntidad, tgFloat, 10, 'NO_DIAS_IT' );
       end;
  end;

  procedure AgregaFiltros(oAgente : TSQLAgente; const oEntidad : TipoEntidad );
  begin
       with oAgente do
       begin
            AgregaFiltro( Format('PE_YEAR = %d',[PE_YEAR]),TRUE,oEntidad);
            AgregaFiltro( Format('PE_TIPO = %d',[PE_TIPO]),TRUE,oEntidad);
            AgregaFiltro( Format('PE_NUMERO = %d',[PE_NUMERO]),TRUE,oEntidad);
            AgregaFiltro( Format('CB_CODIGO = %d',[CB_CODIGO]),TRUE,oEntidad);
       end;
  end;

  procedure CreaDataSetTmpNom;
  begin
       with oBroker do
       begin
            Init(enNomina);
            AgregaColumnasNomina(Agente);
            AgregaFiltros(Agente,enNomina);
            GetBuildDataset;
       end;
  end;

  procedure DataSetTmpLista;
   var i : integer;
  begin
       with oBroker do
       begin
            with Agente do
            begin
                 Init(enMovimien);
                 AgregaColumna( 'MOVIMIEN.CO_NUMERO',True, Entidad, tgNumero, 10, 'CO_NUMERO' );
                 AgregaColumna( 'MOVIMIEN.MO_REFEREN',True, Entidad, tgTexto, 10, 'MO_REFEREN' );
                 AgregaColumna( 'MOVIMIEN.MO_PERCEPC',True, Entidad, tgFloat, 10, 'MO_PERCEPC' );
                 AgregaColumna( 'MOVIMIEN.MO_DEDUCCI',True, Entidad, tgFloat, 10, 'MO_DEDUCCI' );
                 AgregaFiltros(Agente,enMovimien);
                 AgregaFiltro( 'MO_ACTIVO = ''S''',TRUE,enMovimien );
                 for i:=0 to oSQLAgente.NumFiltros - 1 do
                     with oSQLAgente.GetFiltro(i) do
                          if Entidad = enConcepto then
                             AgregaFiltro( Formula, DeDiccionario, Entidad );
            end;
            GetBuildDataset;
       end;
  end;

  function CreaDataSetNomina : Boolean;
   var i: integer;
  begin
       with SQLBroker do
       begin
            Init(enMovimien);
            with Agente do
            begin
                 AgregaColumna( 'DISTINCT(MOVIMIEN.CB_CODIGO)',True, Entidad, tgNumero, 10, 'CB_CODIGO' );
                 AgregaColumnasNomina(Agente);
                 AgregaColumna( 'MOVIMIEN.PE_YEAR',True, Entidad, tgNumero, 10, 'PE_YEAR' );
                 AgregaColumna( 'MOVIMIEN.PE_TIPO',True, Entidad, tgNumero, 10, 'PE_TIPO' );
                 AgregaColumna( 'MOVIMIEN.PE_NUMERO',True, Entidad, tgNumero, 10, 'PE_NUMERO' );
                       //PENDIENTE:EL TIPO DE DATO
                 AgregaColumna( GetNombreNivel( 1 ),FALSE, Entidad, tgTexto, 10, 'TA_NIVEL1' );
                 AgregaColumna( GetNombreNivel( 2 ),FALSE, Entidad, tgTexto, 10, 'TA_NIVEL2' );
                 AgregaColumna( GetNombreNivel( 3 ),FALSE, Entidad, tgTexto, 10, 'TA_NIVEL3' );
                 AgregaColumna( GetNombreNivel( 4 ),FALSE, Entidad, tgTexto, 10, 'TA_NIVEL4' );
                 AgregaColumna( GetNombreNivel( 5 ),FALSE, Entidad, tgTexto, 10, 'TA_NIVEL5' );

                 for i:=0 to oSQLAgente.NumFiltros - 1 do
                 begin
                      with oSQLAgente.GetFiltro(i) do
                           if Entidad <> enConcepto then
                           begin
                                oEntidad := Entidad;
                                if oEntidad in [enMovimienLista] then
                                   oEntidad := enMovimien;
                                AgregaFiltro( CambiaTabla(Formula), DeDiccionario, oEntidad );
                           end;
                 end;
            end;
            Result := BuildDataSet(oZetaProvider.EmpresaActiva);
            if Result then
               FNominaTotales := SuperReporte.DataSetReporte
            else Error := 'No Existen Nominas Calculadas para Este Periodo';
       end;
  end;

  function GetValoresNomina : string;
   var i : integer;
  begin
       Result := '';
       with FMovimien do
            for i:=0 to FieldCount - 1 do
                Result := ConcatString( Result, Fields[i].AsString, ',' );
  end;

  procedure DataSetTotalesTmpLista;
  begin {Totales de Empresa por Concepto}
       with oBroker do
       begin
            Init(enMovimienLista);
            with Agente do
            begin
                 AgregaColumna( 'CO_NUMERO',True, Entidad, tgNumero, 10, 'CO_NUMERO' );
                 AgregaColumna( 'SUM(TA_PERCEPC)',True, Entidad, tgFloat, 10, 'TA_PERCEPC' );
                 AgregaColumna( 'SUM(TA_DEDUCCI)',True, Entidad, tgFloat, 10, 'TA_DEDUCCI' );
                 AgregaFiltro( Format('TA_USER = %d', [FUsuario]), True, Entidad ) ;
                 AgregaFiltro( Format('CB_CODIGO < %d',[Q_IMAX_EMPLEADO]), True, Entidad );
                 AgregaGrupo( 'CO_NUMERO', TRUE, Entidad );
            end;
            GetBuildDataset;
       end;
  end;
  procedure DataSetGrupos( const Posicion : integer;
                           const oEntidad : TipoEntidad );
   var sGrupo : string;
       i : integer;
  begin
       with oBroker do
       begin
            Init(oEntidad);
            with Agente do
            begin
                 AgregaFiltro( Format('TA_USER = %d', [FUsuario]), True, Entidad ) ;
                 AgregaFiltro( Format('CB_CODIGO < %d',[Q_IMAX_EMPLEADO]), True, Entidad );
                 for i := 1 to Posicion do
                     if GetCodigoGrupo(i,sGrupo) then
                     begin
                          AgregaGrupo( sGrupo,TRUE , Entidad );
                          AgregaColumna( sGrupo,TRUE , Entidad, tgTexto, 10, sGrupo );
                     end;
                 for i:= Posicion+1 to K_MAXGRUPOS do
                 begin
                      AgregaColumna( Comillas(Q_CHR_RELLENO),TRUE , Entidad,tgTexto, 10,'TA_NIVEL'+IntToStr(i) );
                 end;

                 if oEntidad = enMovimienLista then
                 begin
                      AgregaColumna( 'CO_NUMERO',True, Entidad, tgNumero, 10, 'CO_NUMERO' );
                      AgregaColumna( 'SUM(TA_PERCEPC)',True, Entidad, tgFloat, 10, 'TA_PERCEPC' );
                      AgregaColumna( 'SUM(TA_DEDUCCI)',True, Entidad, tgFloat, 10, 'TA_DEDUCCI' );
                      AgregaGrupo( 'CO_NUMERO', TRUE, Entidad )
                 end
                 else
                     AgregaColumnasNomina(Agente,oEntidad,'TMPNOM',TRUE);

            end;
            GetBuildDataset;
       end;
  end;

  procedure DataSetGruposNomina(const i : integer);
   var sGrupo : string;
  begin
       with oBroker do
       begin
            Init(enTemporalNomina);
            with Agente do
            begin
                 AgregaColumnasNomina(Agente,enTemporalNomina,'TMPNOM',TRUE);
                 AgregaFiltro(Format('TA_USER = %d',[FUsuario]),TRUE, Entidad);
                 AgregaFiltro(Format('CB_CODIGO < %d',[Q_iMAX_EMPLEADO]),TRUE,Entidad);
                 if GetCodigoGrupo(i,sGrupo) then
                    AgregaGrupo( sGrupo,TRUE , Entidad );
                 if GetCodigoGrupo(i,sGrupo) then
                    AgregaGrupo( sGrupo,TRUE , Entidad );
                 if GetCodigoGrupo(i,sGrupo) then
                    AgregaGrupo( sGrupo,TRUE , Entidad );
                 if GetCodigoGrupo(i,sGrupo) then
                    AgregaGrupo( sGrupo,TRUE , Entidad );
                 if GetCodigoGrupo(i,sGrupo) then
                    AgregaGrupo( sGrupo,TRUE , Entidad );
            end;
            GetBuildDataset;
       end;
  end;

  procedure DataSetCountGrupos(const Posicion : integer);
   var sGrupo : string;
       i : integer;
  begin
       with oBroker do
       begin
            Init(enTemporalNomina);
            with Agente do
            begin
                 AgregaColumna( 'COUNT(CB_CODIGO)',True, Entidad, tgNumero, 10, 'CUANTOS' );
                 for i := 1 to Posicion do
                     if GetCodigoGrupo(i,sGrupo) then
                     begin
                          AgregaGrupo( sGrupo,TRUE , Entidad );
                          AgregaColumna( sGrupo,TRUE , Entidad, tgTexto, 10, sGrupo );
                     end;
                 AgregaFiltro(Format('TA_USER = %d',[FUsuario]),TRUE, Entidad);
                 AgregaFiltro(Format('CB_CODIGO < %d',[Q_iMAX_EMPLEADO]),TRUE,Entidad);
            end;
            GetBuildDataset;
       end;
  end;
  procedure DatasetTotalNomina;
  begin
       with oBroker do
       begin
            Init(enTemporalNomina);
            with Agente do
            begin
                 AgregaColumnasNomina(Agente,enTemporalNomina,'TMPNOM',TRUE);
                 AgregaFiltro(Format('TA_USER = %d',[FUsuario]),TRUE, Entidad);
                 AgregaFiltro(Format('CB_CODIGO < %d',[Q_iMAX_EMPLEADO]),TRUE,Entidad);
            end;
            GetBuildDataset;
       end;
  end;

  procedure InsertTmpLista( const iConcepto : integer;
                            const rPercepc, rDeducci: Currency;
                            const sReferen : string = '';
                            const rTotal : Currency = 0 );
  begin
       with oZetaProvider do
       begin
            ParamAsInteger(FInsertTmpLista,'PE_YEAR',PE_YEAR);
            ParamAsInteger(FInsertTmpLista,'PE_TIPO',PE_TIPO);
            ParamAsInteger(FInsertTmpLista,'PE_NUMERO',PE_NUMERO);
            ParamAsInteger(FInsertTmpLista,'CB_CODIGO',CB_CODIGO);
            ParamAsInteger(FInsertTmpLista,'CO_NUMERO',iConcepto);
            ParamAsChar(FInsertTmpLista,'TA_NIVEL1', TA_NIVEL1,6);
            ParamAsChar(FInsertTmpLista,'TA_NIVEL2', TA_NIVEL2,6);
            ParamAsChar(FInsertTmpLista,'TA_NIVEL3', TA_NIVEL3,6);
            ParamAsChar(FInsertTmpLista,'TA_NIVEL4', TA_NIVEL4,6);
            ParamAsChar(FInsertTmpLista,'TA_NIVEL5', TA_NIVEL5,6);
            ParamAsVarChar(FInsertTmpLista,'TA_REFEREN',sReferen,8);
            ParamAsFloat(FInsertTmpLista,'TA_TOTAL',rTotal);
            ParamAsFloat(FInsertTmpLista,'TA_PERCEPC',rPercepc);
            ParamAsFloat(FInsertTmpLista,'TA_DEDUCCI',rDeducci);
            Ejecuta(FInsertTmpLista);
       end;
  end;

  procedure ParamsNomina(oParam : TZetaCursor;oFuente : TServerDataSet);
  begin
       with oZetaProvider do
       begin
            ParamAsInteger(oParam,'CB_CODIGO',CB_CODIGO);
            ParamAsInteger(oParam,'TA_USER',FUsuario);
            ParamAsChar(oParam,'TA_NIVEL1', TA_NIVEL1,6);
            ParamAsChar(oParam,'TA_NIVEL2', TA_NIVEL2,6);
            ParamAsChar(oParam,'TA_NIVEL3', TA_NIVEL3,6);
            ParamAsChar(oParam,'TA_NIVEL4', TA_NIVEL4,6);
            ParamAsChar(oParam,'TA_NIVEL5', TA_NIVEL5,6);

            with oFuente do
            begin
                 ParamAsFloat(oParam,'CB_SAL_INT',FieldByName('CB_SAL_INT').AsFloat);
                 ParamAsFloat(oParam,'CB_SALARIO',FieldByName('CB_SALARIO').AsFloat);
                 ParamAsFloat(oParam,'NO_DIAS',FieldByName('NO_DIAS').AsFloat);
                 ParamAsFloat(oParam,'NO_ADICION',FieldByName('NO_ADICION').AsFloat);
                 ParamAsFloat(oParam,'NO_DEDUCCI',FieldByName('NO_DEDUCCI').AsFloat);
                 ParamAsFloat(oParam,'NO_NETO',FieldByName('NO_NETO').AsFloat);
                 ParamAsFloat(oParam,'NO_DES_TRA',FieldByName('NO_DES_TRA').AsFloat);
                 ParamAsFloat(oParam,'NO_DIAS_AG',FieldByName('NO_DIAS_AG').AsFloat);
                 ParamAsFloat(oParam,'NO_DIAS_VA',FieldByName('NO_DIAS_VA').AsFloat);
                 ParamAsFloat(oParam,'NO_DOBLES',FieldByName('NO_DOBLES').AsFloat);
                 ParamAsFloat(oParam,'NO_EXTRAS',FieldByName('NO_EXTRAS').AsFloat);
                 ParamAsFloat(oParam,'NO_FES_PAG',FieldByName('NO_FES_PAG').AsFloat);
                 ParamAsFloat(oParam,'NO_FES_TRA',FieldByName('NO_FES_TRA').AsFloat);
                 ParamAsFloat(oParam,'NO_HORA_CG',FieldByName('NO_HORA_CG').AsFloat);
                 ParamAsFloat(oParam,'NO_HORA_SG',FieldByName('NO_HORA_SG').AsFloat);
                 ParamAsFloat(oParam,'NO_HORAS',FieldByName('NO_HORAS').AsFloat);
                 ParamAsFloat(oParam,'NO_IMP_CAL',FieldByName('NO_IMP_CAL').AsFloat);
                 ParamAsFloat(oParam,'NO_JORNADA',FieldByName('NO_JORNADA').AsFloat);
                 ParamAsFloat(oParam,'NO_PER_CAL',FieldByName('NO_PER_CAL').AsFloat);
                 ParamAsFloat(oParam,'NO_PER_MEN',FieldByName('NO_PER_MEN').AsFloat);
                 ParamAsFloat(oParam,'NO_PERCEPC',FieldByName('NO_PERCEPC').AsFloat);
                 ParamAsFloat(oParam,'NO_TARDES',FieldByName('NO_TARDES').AsFloat);
                 ParamAsFloat(oParam,'NO_TRIPLES',FieldByName('NO_TRIPLES').AsFloat);
                 ParamAsFloat(oParam,'NO_TOT_PRE',FieldByName('NO_TOT_PRE').AsFloat);
                 ParamAsFloat(oParam,'NO_VAC_TRA',FieldByName('NO_VAC_TRA').AsFloat);
                 ParamAsFloat(oParam,'NO_X_CAL',FieldByName('NO_X_CAL').AsFloat);
                 ParamAsFloat(oParam,'NO_X_ISPT',FieldByName('NO_X_ISPT').AsFloat);
                 ParamAsFloat(oParam,'NO_X_MENS',FieldByName('NO_X_MENS').AsFloat);
                 ParamAsInteger(oParam,'NO_D_TURNO',FieldByName('NO_D_TURNO').AsInteger);
                 ParamAsInteger(oParam,'NO_DIAS_AJ',FieldByName('NO_DIAS_AJ').AsInteger);
                 ParamAsInteger(oParam,'NO_DIAS_AS',FieldByName('NO_DIAS_AS').AsInteger);
                 ParamAsInteger(oParam,'NO_DIAS_CG',FieldByName('NO_DIAS_CG').AsInteger);
                 ParamAsInteger(oParam,'NO_DIAS_EM',FieldByName('NO_DIAS_EM').AsInteger);
                 ParamAsInteger(oParam,'NO_DIAS_FI',FieldByName('NO_DIAS_FI').AsInteger);
                 ParamAsInteger(oParam,'NO_DIAS_FJ',FieldByName('NO_DIAS_FJ').AsInteger);
                 ParamAsInteger(oParam,'NO_DIAS_FV',FieldByName('NO_DIAS_FV').AsInteger);
                 ParamAsInteger(oParam,'NO_DIAS_IN',FieldByName('NO_DIAS_IN').AsInteger);
                 ParamAsInteger(oParam,'NO_DIAS_NT',FieldByName('NO_DIAS_NT').AsInteger);
                 ParamAsInteger(oParam,'NO_DIAS_OT',FieldByName('NO_DIAS_OT').AsInteger);
                 ParamAsInteger(oParam,'NO_DIAS_RE',FieldByName('NO_DIAS_RE').AsInteger);
                 ParamAsInteger(oParam,'NO_DIAS_SG',FieldByName('NO_DIAS_SG').AsInteger);
                 ParamAsInteger(oParam,'NO_DIAS_SS',FieldByName('NO_DIAS_SS').AsInteger);
                 ParamAsInteger(oParam,'NO_DIAS_SU',FieldByName('NO_DIAS_SU').AsInteger);
                 ParamAsFloat(oParam,'NO_HORA_PD',FieldByName('NO_HORA_PD').AsFloat);
                 ParamAsFloat(oParam,'NO_EXENTAS',FieldByName('NO_EXENTAS').AsFloat);
                 ParamAsFloat(oParam,'NO_DIAS_BA',FieldByName('NO_DIAS_BA').AsFloat);
                 ParamAsFloat(oParam,'NO_DIAS_SI',FieldByName('NO_DIAS_SI').AsFloat);
                 ParamAsFloat(oParam,'NO_HORAPDT',FieldByName('NO_HORAPDT').AsFloat);
                 ParamAsFloat(oParam,'NO_HORASNT',FieldByName('NO_HORASNT').AsFloat);
                 ParamAsFloat(oParam,'NO_PREV_GR',FieldByName('NO_PREV_GR').AsFloat);
                 ParamAsFloat(oParam,'NO_DIAS_PV',FieldByName('NO_DIAS_PV').AsFloat);
                 ParamAsFloat(oParam,'NO_DIAS_VJ',FieldByName('NO_DIAS_VJ').AsFloat);
                 ParamAsFloat(oParam, 'NO_PRE_EXT', FieldByName('NO_PRE_EXT').AsFloat);
                 ParamAsFloat(oParam,'NO_PER_ISN',FieldByName('NO_PER_ISN').AsFloat);
                 ParamAsFloat(oParam,'NO_DIAS_IH',FieldByName('NO_DIAS_IH').AsFloat);
                 ParamAsFloat(oParam,'NO_DIAS_ID',FieldByName('NO_DIAS_ID').AsFloat);
                 ParamAsFloat(oParam,'NO_DIAS_IT',FieldByName('NO_DIAS_IT').AsFloat);

            end;
            Ejecuta(oParam);
       end;
  end;
  function ValidaCodigo( const sCodigo: string; const iPos: integer ): string;
  begin
       if Length( sCodigo )> 6 then
          raise Exception.Create( Format( 'Los campos de agrupacion no deben de exceder los 6 caracteres' + CR_LF +
                                          'Es necesario cambiar la agrupaci�n #%d, para que el reporte salga correctamente', [iPos] ) );
       Result := sCodigo;
  end;

begin //.CalculaListadoTotales
     Result := NIL;
     oZetaProvider.EmpiezaTransaccion;
     try
        InicializaListadoTotales;
        if oSQLAgente.Entidad = enMovimienLista then
           FTablaPrincipal := 'TMPLISTA'
        else FTablaPrincipal := 'TMPNOMTOT';

        BorraTabla('TMPLISTA','TA_USER');
        BorraTabla('TMPNOM','TA_USER');


        nGruposListado :=oZetaProvider.ParamList.ParamByName('CountGrupos').AsInteger;
        //lEsGrupoValido := nGruposListado = 0;

        InitBroker;
        if CreaDataSetNomina then
        begin
             with oZetaProvider do
             begin
                  FTmpNom := CreateQuery;
                  PreparaQuery( FTmpNom, Q_LTOTALES_DETALLE_NOMINA );

                  PE_YEAR := FNominaTotales.FieldByName('PE_YEAR').AsInteger;
                  PE_TIPO := FNominaTotales.FieldByName('PE_TIPO').AsInteger;
                  PE_NUMERO := FNominaTotales.FieldByName('PE_NUMERO').AsInteger;

                  try
                     oZetaProvider.EmpiezaTransaccion;

                     while NOT FNominaTotales.EOF do
                     begin
                          CB_CODIGO := FNominaTotales.FieldByName('CB_CODIGO').AsInteger;
                          TA_NIVEL1 := ValidaCodigo( FNominaTotales.FieldByName('TA_NIVEL1').Asstring, 1 );
                          TA_NIVEL2 := ValidaCodigo( FNominaTotales.FieldByName('TA_NIVEL2').Asstring, 2 );
                          TA_NIVEL3 := ValidaCodigo( FNominaTotales.FieldByName('TA_NIVEL3').Asstring, 3 );
                          TA_NIVEL4 := ValidaCodigo( FNominaTotales.FieldByName('TA_NIVEL4').Asstring, 4 );
                          TA_NIVEL5 := ValidaCodigo( FNominaTotales.FieldByName('TA_NIVEL5').Asstring, 5 );

                          DataSetTmpLista;
                          while NOT FMovimien.EOF do
                          begin
                                InsertTmpLista( FMovimien.FieldByName('CO_NUMERO').AsInteger,
                                                FMovimien.FieldByName('MO_PERCEPC').AsFloat,
                                                FMovimien.FieldByName('MO_DEDUCCI').AsFloat,
                                                FMovimien.FieldByName('MO_REFEREN').AsString);
                                FMovimien.Next;
                          end;
                          CreaDatasetTmpNom;
                             while NOT FMovimien.EOF do
                             begin
                                  ParamsNomina(FTmpNom,FMovimien);
                                  FMovimien.Next;
                             end;
                             FNominaTotales.Next;
                     end;
                     oZetaProvider.TerminaTransaccion(True);
                  except
                        on E:Exception do
                        begin
                             oZetaProvider.TerminaTransaccion(False);
                             raise;
                        end;
                  end;
                  //Calcula los Totales de Empresa Para TMPLISTA
                  TA_NIVEL1:=Q_EMPRESA;
                  TA_NIVEL2:=Q_EMPRESA;
                  TA_NIVEL3:=Q_EMPRESA;
                  TA_NIVEL4:=Q_EMPRESA;
                  TA_NIVEL5:=Q_EMPRESA;
                  CB_CODIGO:=Q_IMAX_EMPLEADO;

                  DataSetTotalesTmpLista;
                  with FMovimien do
                  begin
                       try
                          oZetaProvider.EmpiezaTransaccion;
                          while NOT EOF do
                          begin
                               InsertTmpLista( FieldByName('CO_NUMERO').AsInteger,
                                               FieldByName('TA_PERCEPC').AsFloat,
                                               FieldByName('TA_DEDUCCI').AsFloat);
                               Next;
                          end;
                          oZetaProvider.TerminaTransaccion(True);
                       except
                             oZetaProvider.TerminaTransaccion(False);
                       end;
                  end;
                  //Calcula los Totales de Empresa Para TMPNOM
                  DatasetTotalNomina;
                  with FMovimien do
                       while NOT EOF do
                       begin
                            ParamsNomina(FInsertTmpNom,FMovimien);
                            Next;
                       end;

                  //Calcula el Total de Empleados de Empresa , lo hace sobre TmpNom
                  AbreQueryScript( FTmpNom,Format( Q_TOTALES_COUNT,[FUsuario]));
                  PreparaQuery( FTmpNom,
                                Format( Q_TOTALES_UPDATE,[
                                FTmpNom.Fields[0].AsInteger,
                                FUsuario,
                                'AND TA_NIVEL1 = ' + Comillas(Q_EMPRESA)]));
                  try
                     oZetaProvider.EmpiezaTransaccion;
                     Ejecuta(FTmpNom);
                     oZetaProvider.TerminaTransaccion(True);
                  except
                        oZetaProvider.TerminaTransaccion(False);
                  end;

                  //Calcula los Totales de Grupos
                  if nGruposListado > 0 then
                  begin
                       for i := 1 to nGruposListado do
                       begin
                            //sGrupos := '';
                            CB_CODIGO := Q_IMAX_EMPLEADO;

                            DataSetGrupos(i,enMovimienLista);
                            with FMovimien do
                                 while NOT EOF do
                                 begin
                                      TA_NIVEL1 := FieldByName('TA_NIVEL1').AsString;
                                      TA_NIVEL2 := FieldByName('TA_NIVEL2').AsString;
                                      TA_NIVEL3 := FieldByName('TA_NIVEL3').AsString;
                                      TA_NIVEL4 := FieldByName('TA_NIVEL4').AsString;
                                      TA_NIVEL5 := FieldByName('TA_NIVEL5').AsString;
                                      InsertTmpLista( FieldByName('CO_NUMERO').AsInteger,
                                                      FieldByName('TA_PERCEPC').AsFloat,
                                                      FieldByName('TA_DEDUCCI').AsFloat );
                                      Next;
                                 end;

                            DataSetGrupos(i,enTemporalNomina);
                            with FMovimien do
                                 while NOT EOF do
                                 begin
                                      TA_NIVEL1 := FieldByName('TA_NIVEL1').AsString;
                                      TA_NIVEL2 := FieldByName('TA_NIVEL2').AsString;
                                      TA_NIVEL3 := FieldByName('TA_NIVEL3').AsString;
                                      TA_NIVEL4 := FieldByName('TA_NIVEL4').AsString;
                                      TA_NIVEL5 := FieldByName('TA_NIVEL5').AsString;
                                      ParamsNomina(FInsertTmpNom,FMovimien);
                                      Next;
                                 end;

                                 DataSetCountGrupos(i);
                                 with FMovimien do
                                 begin
                                      try
                                         oZetaProvider.EmpiezaTransaccion;
                                         while NOT EOF do
                                         begin
                                              PreparaQuery( FTmpNom,
                                                            Format( Q_TOTALES_UPDATE,
                                                            [FieldByName('CUANTOS').AsInteger,
                                                            FUsuario,
                                                            'AND '+ GetFiltroTotales(i)]));
                                              Ejecuta(FTmpNom);
                                              Next;
                                         end;
                                         oZetaProvider.TerminaTransaccion(True);
                                      except
                                            oZetaProvider.TerminaTransaccion(False);
                                      end;
                                 end;
                       end;
                  end;

                  if lSoloTotales then
                  begin
                       PreparaQuery( FTmpNom,Format( Q_TOTALES_BORRA_LISTA,
                                                      [FUsuario]));
                       try
                          oZetaProvider.EmpiezaTransaccion;
                          Ejecuta(FTmpNom);
                          oZetaProvider.TerminaTransaccion(True);
                       except
                             oZetaProvider.TerminaTransaccion(False);
                       end;
                       PreparaQuery( FTmpNom,Format( Q_TOTALES_BORRA_NOMINA,
                                             [FUsuario]));
                       try
                          oZetaProvider.EmpiezaTransaccion;
                          Ejecuta(FTmpNom);
                          oZetaProvider.TerminaTransaccion(True);
                       except
                             oZetaProvider.TerminaTransaccion(False);
                       end;

                       if nGruposListado > 1 then
                       begin
                            try
                               oZetaProvider.EmpiezaTransaccion;
                               for i := 2 to nGruposListado do
                               begin
                                    PreparaQuery( FTmpNom,Format( Q_TOTALES_BORRA_CEROS_GRUPO,
                                                  [FUsuario,i] ));
                                    Ejecuta(FTmpNom);
                               end;
                               oZetaProvider.TerminaTransaccion(True);
                            except
                                  oZetaProvider.TerminaTransaccion(False);
                            end;
                       end;
                  end
                  else //Borra los Totales que nada mas tienen un empleado
                  begin
                       PreparaQuery( FTmpNom,Format( Q_TOTALES_BORRA_CEROS,
                                                      [FUsuario] ));
                       try
                          oZetaProvider.EmpiezaTransaccion;
                          Ejecuta(FTmpNom);
                          oZetaProvider.TerminaTransaccion(True);
                       except
                             oZetaProvider.TerminaTransaccion(False);
                       end;
                  end;
             end;
             if oSQLAgente.Entidad = enMovimienLista then
             begin
                  Result := TSQLAgente.Create;
                  Result.Entidad := enMovimienLista;
                  with oSQLAgente do
                  begin
                       for i := 0 to NumColumnas - 1 do
                           with GetColumna(i) do
                           begin
                                iColumna := Result.AgregaColumna( Formula,
                                                                  DeDiccionario,
                                                                  Entidad,
                                                                  TipoFormula,
                                                                  Ancho,
                                                                  Alias,
                                                                  Totalizacion,
                                                                  GroupExp );
                                Result.GetColumna(iColumna).ValorConstante := ValorConstante;

                           end;
                  end;
                  with Result do
                  begin
                       AgregaColumna( 'TMPLISTA.TA_NIVEL1',TRUE,Entidad,tgTexto,10,'TA_NIVEL1' );
                       AgregaColumna( 'TMPLISTA.TA_NIVEL2',TRUE,Entidad,tgTexto,10,'TA_NIVEL2' );
                       AgregaColumna( 'TMPLISTA.TA_NIVEL3',TRUE,Entidad,tgTexto,10,'TA_NIVEL3' );
                       AgregaColumna( 'TMPLISTA.TA_NIVEL4',TRUE,Entidad,tgTexto,10,'TA_NIVEL4' );
                       AgregaColumna( 'TMPLISTA.TA_NIVEL5',TRUE,Entidad,tgTexto,10,'TA_NIVEL5' );
                       AgregaColumna( 'TMPLISTA.CB_CODIGO',TRUE,Entidad,tgNumero,10,'CB_CODIGO' );
                       AgregaColumna( 'TMPLISTA.TA_TOTAL',TRUE,Entidad,tgNumero,10,'TA_TOTAL' );
                       AgregaColumna( 'COLABORA.CB_NOMBRES',TRUE,enEmpleado,tgTexto,30,'CB_NOMBRES' );
                       AgregaColumna( 'COLABORA.CB_APE_PAT',TRUE,enEmpleado,tgTexto,30,'CB_APE_PAT' );
                       AgregaColumna( 'COLABORA.CB_APE_MAT',TRUE,enEmpleado,tgTexto,30,'CB_APE_MAT' );
                       AgregaOrden( 'TMPLISTA.TA_NIVEL1',TRUE,Entidad );
                       AgregaOrden( 'TMPLISTA.TA_NIVEL2',TRUE,Entidad );
                       AgregaOrden( 'TMPLISTA.TA_NIVEL3',TRUE,Entidad );
                       AgregaOrden( 'TMPLISTA.TA_NIVEL4',TRUE,Entidad );
                       AgregaOrden( 'TMPLISTA.TA_NIVEL5',TRUE,Entidad );
                       AgregaOrden( 'TMPLISTA.CB_CODIGO',TRUE,Entidad );
                       AgregaOrden( 'TMPLISTA.CO_NUMERO',TRUE,Entidad );
                       AgregaOrden( 'TMPLISTA.TA_REFEREN',TRUE,Entidad );
                       AgregaFiltro( Format('TMPLISTA.TA_USER=%d',[FUsuario]),TRUE,Entidad );
                  end;
             end;
             with oZetaProvider do
             begin
                  ParamList.AddBoolean('SoloTotales',FALSE);
                  TerminaTransaccion(TRUE);
             end;
        end;
     except
           on E:Exception do
           begin
                oZetaProvider.TerminaTransaccion(FALSE);
                raise;
           end;
     end;

end;

{$ifdef INTERBASE}
function TdmServerReportes.Unionizer(const sSQL, sTabla1, sTabla2,sTabla3: String): String;

const
     K_ORDER_BY = 'ORDER BY';
     K_COLUMNA = '%s COLUMNA'; { El espacio inicial es importante ! }
     K_COMA = ',';
     K_DESCENDING = 'DESCENDING';
     K_DESC = 'DESC';

var
   sOrderBy, sSQL1, sSQL2,sSQL3, sCampo, sNumero: String;
   cDigito: Char;
   i, j, iPos, iCampo: Integer;
   FOrders: TStrings;
   lDescending: Boolean;

begin

     sSQL1 := Trim( sSQL );
     iPos := Pos( K_ORDER_BY, AnsiUpperCase( sSQL1 ) );

     if ( iPos > 0 ) then
     begin

          sOrderBy := Copy( sSQL1, iPos, ( Length( sSQL1 ) - iPos + 1 ) );
          sSQL1 := Copy( sSQL1, 1, ( iPos - 1 ) );

          FOrders := TStringList.Create;
          try
             with FOrders do
             begin
                  iPos := Length( K_ORDER_BY ) + 1;
                  sOrderBy := Copy( sOrderBy, iPos, ( Length( sOrderBy ) - iPos + 1 ) );

                  iPos := 1;
                  sCampo := VACIO;

                  while ( iPos <= Length( sOrderBy ) ) do
                  begin
                       cDigito := sOrderBy[ iPos ];

                       if ( cDigito = K_COMA ) then
                       begin
                            Add( Trim( sCampo ) );
                            sCampo := VACIO;
                       end
                       else
                           if  ( iPos = Length( sOrderBy ) ) then
                           begin
                                Add( Trim( sCampo + cDigito ) );
                           end
                           else
                               sCampo := sCampo + cDigito;
                       Inc( iPos );
                  end;
                  sOrderBy := ZetaCommonClasses.VACIO;

                  for i := 0 to ( Count - 1 ) do
                  begin
                       sCampo := AnsiUpperCase( Strings[ i ] );

                       if ( Pos( K_DESCENDING, sCampo ) > 0 ) then
                       begin
                            sCampo := Trim( ZetaCommonTools.StrTransform( sCampo, K_DESCENDING, VACIO ) );
                            lDescending := True;
                       end
                       else
                           if ( Pos( K_DESC, sCampo ) > 0 ) then
                           begin
                                sCampo := Trim( ZetaCommonTools.StrTransform( sCampo, K_DESC, VACIO ) );
                                lDescending := True;
                           end
                           else
                               lDescending := False;

                       sCampo := Format( K_COLUMNA, [ sCampo ] );
                       iPos := Pos( sCampo, AnsiUpperCase( sSQL1 ) );

                       Assert( ( iPos > 0 ), Format( 'No Se Encontraron Todos Los Order By ( %s )', [ sCampo ] )  );

                       if ( iPos > 0 ) then
                       begin
                            { Posicion del 1er Caracter despues de 'TABLA.CAMPO COLUMNA' }

                            iPos := iPos + Length( sCampo );
                            sSQL2 := Copy( sSQL1, iPos, Length( sSQL1 ) - iPos + 1 );
                            { Buscar los d�gitos }
                            j := 1;
                            sNumero := ZetaCommonClasses.VACIO;

                            while ( j <= Length( sSQL2 ) ) do
                            begin
                                 cDigito := sSQL2[ j ];

                                 if ( cDigito in [ '0'..'9' ] ) then
                                    sNumero := sNumero + cDigito
                                 else
                                       Break;
                                 Inc( j );
                            end;

                            iCampo := StrToIntDef( sNumero, 0 ) + 1;
                            Assert( ( iCampo > 0 ), 'Valor Del Campo Mal Calculado' );
                            if lDescending then
                               sOrderBy := sOrderBy + Format( '%d %s,', [ iCampo, K_DESC ] )
                            else
                                sOrderBy := sOrderBy + Format( '%d,', [ iCampo ] );
                       end;
                  end;
             end;
          finally
                 FreeAndNil( FOrders );
          end;

          if ZetaCommonTools.StrLleno( sOrderBy ) then
          begin
               sOrderBy := Format( '%s %s', [ K_ORDER_BY, ZetaCommonTools.CortaUltimo( sOrderBy ) ] );
          end;
     end
     else
         sOrderBy := ZetaCommonClasses.VACIO;

     sSQL2 := ZetaCommonTools.StrTransAll( sSQL1, sTabla1, sTabla2 );
     if StrLleno(sTabla3 )then
     begin
          sSQL3 := ZetaCommonTools.StrTransAll( sSQL1, sTabla1, sTabla3 );
          Result := Format( '%s union %s union %s %s', [ sSQL1, sSQL2, sSQL3, sOrderBy ] );
     end
     else
         Result := Format( '%s union %s %s', [ sSQL1, sSQL2, sOrderBy ] );
end;
{$endif}
end.
