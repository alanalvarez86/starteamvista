unit DServerAnualNomina;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      DServerAnualNomina.pas                     ::
  :: Descripci�n: Programa principal de DAnualNomina.dll     ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface
uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComServ, ComObj, VCLCom, StdVcl, DataBkr, DBClient, DB, MtsRdm, Mtx,
{$ifndef DOS_CAPAS}
     DAnualNomina_TLB,
{$endif}
     DNominaClass,
     DLaborCalculo,
     DZetaServerProvider,
     ZCreator,
     ZAgenteSQLClient,
     ZetaCommonLists,
     ZetaSQLBroker,
     ZetaServerDataSet,
     ZetaCommonClasses;

type
  TdmServerAnualNomina = class(TMtsDataModule {$ifndef DOS_CAPAS}, IdmServerAnualNomina {$endif})
    cdsLista: TServerDataSet;
    procedure MtsDataModuleCreate(Sender: TObject);
    procedure MtsDataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
{$ifndef DOS_CAPAS}
    oZetaCreator: TZetaCreator;
    oZetaSQLBroker: TSQLBroker;
{$endif}
    FListaParametros: string;
    FListaFormulas: string;
    FFormulasPendientesTD: TList;
    FOffsetCampos: integer;
    FNomina: TNomina;
    property Nomina: TNomina read FNomina;
    function CalcularAguinaldoDataset(DataSet: TDataSet): OleVariant;
    function CalcularPTUDataset(DataSet: TDataSet): OleVariant;
    function CalcularRepartoAhorroDataset(DataSet: TDataSet): OleVariant;
    function DeclaraCreditoDataset( DataSet: TDataset ): OleVariant;
    function EsPercepcion( const iConcepto: Integer ): Boolean;
    function GetSQLBroker: TSQLBroker;
    function ISPTAnualDataset(Dataset: TDataset): OleVariant;
    function ISPTPagoDataset(Dataset: TDataset): OleVariant;
    function PagarAguinaldoDataset(Dataset: TDataset): OleVariant;
    function PagarAhorroDataset(Dataset: TDataset): OleVariant;
    function PTUPagoDataset(Dataset: TDataset): OleVariant;
    function CreditoAplicadoDataset(DataSet: TDataSet): OleVariant;
    function DeclaracionAnualDataset(DataSet: TDataSet): OleVariant;
    function Rastreador: TRastreador;
    procedure ClearBroker;
    procedure ClearNomina;
    procedure InitBroker( const lRegistraFuncionesAnuales: Boolean );
    procedure InitCreator;
    procedure InitNomina;
    procedure CalcularAguinaldoBuildDataset;
    procedure CalcularPTUBuildDataset;
    procedure CalcularRepartoAhorroBuildDataset;
    procedure DeclaraCreditoBuildDataset;
    procedure ISPTAnualBuildDataset;
    procedure ISPTPagoBuildDataset;
    procedure ISPTPagoInitProvider;
    procedure PagarAhorroBuildDataset;
    procedure PagarAguinaldoBuildDataset;
    procedure PTUPagoBuildDataset;
    procedure CreditoAplicadoBuildDataset;
    procedure GetDatosPeriodoCredito;
    procedure SetRastreo(DataSet: TZetaCursor; oColumna: TSQLColumna; oCampo: TField);
    procedure PagoAguinaldoParametros;
    procedure PTUPagoParametros;
    procedure ISPTPagoParametros;
    procedure PagarAhorroParametros;
    procedure CalcularAguinaldoParametros;
    procedure CalcularPTUParametros;
    procedure CalcularRepartoAhorroParametros;
    procedure ISPTAnualParametros;
    procedure DeclaraCreditoParametros;
    procedure CerrarAhorrosParametros;
    procedure CerrarPrestamosParametros;
    procedure CreditoAplicadoParametros;
    procedure DeclaracionAnualParametros;
    procedure DeclaracionAnualCierreParametros;
    procedure AfectarLaborParametros;
    function CerrarAhorrosDataset( DataSet: TDataSet ): OleVariant;
    function CerrarPrestamosDataset( DataSet: TDataSet ): OleVariant;
    procedure CerrarAhorrosBuildDataset;
    procedure CerrarPrestamosBuildDataset;
    function CalcularTiemposDataSet(Dataset: TDataSet): OleVariant;
    procedure CalcularTiemposParametros;
    procedure CalcularTiemposBuildDataset;
    {M�todos para la Declaraci�n Anual}
    procedure DeclaracionAnualBuildDatasetInitLista;
    procedure DeclaracionAnualBuildDatasetLista;
    procedure DeclaracionAnualCargaFormulas;
    procedure ProcesoAnualValidaRepetidos(oDataSet: TServerDataSet; const eRepetidos : eEmpleadoRepetido; aCampos: array of TPesos; const dFecValidaBajas: TDate; const lEsCalculoISPT:Boolean = FALSE );
    function DeclaracionAnualFormulas(Dataset: TDataSet): TServerDataset;
    function DeclaracionAnualNombreCampo(const iPos: integer): string;
{$ifdef ANTES}
    function GetFiltroDeclaracionAnual(DataSet: TDataset): String;
{$endif}
    function RecalculaTotPerGetDataTotales: OleVariant;
    procedure FiltrosAnualesInit;
    procedure FiltrosAnualesColumnas;
    procedure CalculaSelloDigital(oDataset: TDataset);
  protected
    { Protected declarations }
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    property SQLBroker: TSQLBroker read GetSQLBroker;

    procedure PrevioISRBuildDataSet;
    procedure PrevioISRParametros;
    function PrevioISRDataset( Dataset: TDataset): OleVariant;

{$ifdef DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
{$endif}
    function CalcularAguinaldo(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function CalcularPTU(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function CalcularRepartoAhorro(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function ISPTAnual(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function DeclaraCredito(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function PagarAguinaldoGetLista(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function PagarAguinaldoLista(Empresa, Lista, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function PagarAguinaldo(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function PTUPago(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function PTUPagoGetLista(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function PTUPagoLista(Empresa, Lista, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function ISPTPago(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function ISPTPagoGetLista(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function ISPTPagoLista(Empresa, Lista, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function PagarAhorro(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function PagarAhorroGetLista(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function PagarAhorroLista(Empresa, Lista, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function CerrarAhorros(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function CerrarPrestamos(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function CalcularTiempos(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function CalcularTiemposGetLista(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function CreditoAplicado(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function CreditoAplicadoLista(Empresa, Lista, Parametros: OleVariant): OleVariant;  {$ifndef DOS_CAPAS} safecall; {$endif}
    function CreditoAplicadoGetLista(Empresa,Parametros: OleVariant): OleVariant;  {$ifndef DOS_CAPAS} safecall; {$endif}
    function AfectarLabor(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function DeclaracionAnual(Empresa, Parametros: OleVariant): OleVariant;  {$ifndef DOS_CAPAS} safecall; {$endif}
    function DeclaracionAnualGetLista(Empresa, Parametros: OleVariant): OleVariant;  {$ifndef DOS_CAPAS} safecall; {$endif}
    function DeclaracionAnualLista(Empresa, Lista,Parametros: OleVariant): OleVariant;  {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaDeclaracionGlobales(Empresa, oDelta: OleVariant;out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetDeclaracionGlobales(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function ActualizaTotPerLista(Empresa, Lista, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function RecalculaTotPerGetLista(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function DeclaracionAnualCierre(Empresa, Parametros: OleVariant): OleVariant;  {$ifndef DOS_CAPAS} safecall; {$endif}
    function PrevioISR(Empresa, Parametros: OleVariant): OleVariant;  {$ifndef DOS_CAPAS} safecall; {$endif}
  end;

implementation

uses DQueries,
     {$IFDEF DOS_CAPAS}
     IB_Components,
     {$ENDIF}
     ZEvaluador,
     ZGlobalTress,
     DSuperReporte,
     ZetaTipoEntidad,
     ZetaCommonTools,
     ZetaServerTools,
     ZetaDataSetTools,
     DSelloDigital;

{$R *.DFM}
type
    eCierrePrestamo = ( ecBorrarSaldados, ecCierreAnualPrestamo, ecCancelarRegistros );
    eCierreAhorro = ( ecPonerCeros, ecCierreAnualAhorro, ecBorrarRegistros );

const
     aCreditoSalarioLower: array[ FALSE..TRUE ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'Cr�dito', '   SUBE' );

     K_ANCHO_TIPO = 1;
     K_ANCHO_REFERENCIA = 8;
     K_ANCHO_FORMULA = 255;
     Q_AGUINALDO_BORRAR = 1;
     Q_AGUINALDO_AGREGAR = 2;
     Q_PTU_BORRAR = 3;
     Q_PTU_INSERTAR = 4;
     Q_PTU_TOTALIZAR =5;
     Q_AHORRO_BORRAR = 6;
     Q_AHORRO_INSERT = 7;
     Q_AHORRO_LIST = 8;
     Q_AHORRO_UPDATE = 9;
     Q_ISPT_ANUAL_BORRAR = 10;
     Q_ISPT_ANUAL_INSERT = 11;
     Q_CREDITO_BORRAR = 12;
     Q_CREDITO_INSERT = 13;
     Q_PRESTAMO_EXISTE = 14;
     Q_PRESTAMO_AGREGA = 15;
     Q_PRESTAMO_MODIFICA = 16;
     Q_CIERRE_AHORRO_DEL_CANCELADOS = 17;
     Q_CIERRE_AHORRO_DEL_CAR_ABO = 18;
     Q_CIERRE_AHORRO_PONER_EN_CEROS = 19;
     Q_CIERRE_AHORRO_FECHA_CIERRE = 20;
     Q_CIERRE_AHORRO_ACUMULA_SALDO = 21;
     Q_CIERRE_AHORRO_INIT_CEROS = 22;
     Q_CIERRE_AHORRO_DEL_TODO = 23;
     Q_CIERRE_PRESTAMO_DEL_SALDADOS = 24;
     Q_CIERRE_PRESTAMO_DEL_CANCELADOS = 25;
     Q_CIERRE_PRESTAMO_DEL_CAR_ABO = 26;
     Q_CIERRE_PRESTAMO_INIT_SALDO = 27;
     Q_CIERRE_PRESTAMO_INIT_CEROS = 28;
     Q_CIERRE_PRESTAMO_FECHA_CIERRE = 29;
     Q_CONCEPTO_LEE = 30;
     Q_TMPLABOR_BORRAR = 31;
     Q_TMPLABOR_AGREGAR = 32;
     Q_CRED_APLICADO_FECHAS_PERIODO = 33;
     Q_CRED_APLICADO_FILTRO = 34;
     Q_CRED_APLICADO_AFECTA_CONCEPTO = 35;
     Q_CIERRE_PRESTAMO_CANCELA_REG = 36;
     Q_UPDATE_GLOBAL_FECHACORTE = 37;
     Q_DECLARACION_ANUAL_BORRAR = 38;
     Q_DECLARACION_ANUAL_INSERT = 39;
     Q_GET_DIAS_AGUINALDO       = 40;
     Q_DECLARACION_CIERRE       = 41;
     Q_DECLARACION_CIERRE_BORRA = 42;
     Q_DECLARACION_CIERRE_INS   = 43;
     Q_PREVIO_ISR_BORRAR        = 44;
     Q_PREVIO_ISR_INSERT        = 45;


     //Constante para la Declaracion Anual:
     K_TD_PRIMER_GRAVADO_RESULT = 4 ;
     K_TD_ULTIMO_GRAVADO_RESULT = 61 ;
     K_TD_PRIMER_GRAVADO = K_GLOBAL_DECANUAL_PRIMER_CAMPO + 4 -1;
     K_TD_ULTIMO_GRAVADO = K_GLOBAL_DECANUAL_PRIMER_CAMPO + 66 ;

function GetSQLScript( const iScript: Integer ): String;
begin
     case iScript of
          Q_AGUINALDO_BORRAR: Result:= 'delete from AGUINAL where ( AG_YEAR = %d )';
          Q_AGUINALDO_AGREGAR: Result:= 'insert into AGUINAL ( '+
                                        'AG_YEAR, '+
                                        'CB_CODIGO, '+
                                        'AG_FEC_ING, '+
                                        'AG_BASE, '+
                                        'AG_FALTAS, '+
                                        'AG_INCAPA, '+
                                        'AG_NETOS, '+
                                        'AG_DIAS_AG, '+
                                        'AG_SALARIO, '+
                                        'AG_AGUINAL ) values ( '+
                                        ':AG_YEAR, '+
                                        ':CB_CODIGO, '+
                                        ':AG_FEC_ING, '+
                                        ':AG_BASE, '+
                                        ':AG_FALTAS, '+
                                        ':AG_INCAPA, '+
                                        ':AG_NETOS, '+
                                        ':AG_DIAS_AG, '+
                                        ':AG_SALARIO, '+
                                        ':AG_AGUINAL )';
          Q_PTU_BORRAR: Result := 'delete from REP_PTU where ( RU_YEAR = :RU_YEAR )';
          Q_PTU_INSERTAR: Result := 'insert into REP_PTU '+
                                    '( RU_YEAR, CB_CODIGO, RU_DIAS, RU_MONTO, US_CODIGO ) values '+
                                    '(:RU_YEAR,:CB_CODIGO,:RU_DIAS,:RU_MONTO,:US_CODIGO )';
{$ifdef INTERBASE}
          Q_PTU_TOTALIZAR: Result := 'select EMPLEADO from SP_TOPE_PTU( :NYEAR, :NUTILIDADES, :NTOPESALARIO )';
{$else}
          Q_PTU_TOTALIZAR: Result := 'EXECUTE PROCEDURE SP_TOPE_PTU( :NYEAR, :NUTILIDADES, :NTOPESALARIO )';
{$endif}
          Q_AHORRO_BORRAR: Result := 'delete from REP_AHO where ( RA_YEAR = :RA_YEAR ) and ( TB_CODIGO = :TB_CODIGO )';
          Q_AHORRO_INSERT: Result := 'insert into REP_AHO ( '+
                                     'RA_YEAR, '+
                                     'TB_CODIGO, '+
                                     'CB_CODIGO, '+
                                     'RA_AHORRO ) values ( '+
                                     ':RA_YEAR, '+
                                     ':TB_CODIGO, '+
                                     ':CB_CODIGO, '+
                                     ':RA_AHORRO )';
          Q_AHORRO_LIST: Result := 'select CB_CODIGO, RA_AHORRO from REP_AHO where '+
                                   '( RA_YEAR = %d ) and ( TB_CODIGO = ''%s'' )';
          Q_AHORRO_UPDATE: Result := 'update REP_AHO set RA_REPARTO = :RA_REPARTO where '+
                                     '( RA_YEAR = %d ) and ( TB_CODIGO = ''%s'' ) and ( CB_CODIGO = :CB_CODIGO )';
          Q_ISPT_ANUAL_BORRAR: Result := 'delete from COMPARA where ( CP_YEAR = :CP_YEAR )';
          Q_ISPT_ANUAL_INSERT: Result := 'insert into COMPARA ( '+
                                         'CP_YEAR, '+
                                         'CB_CODIGO, '+
                                         'CB_SALARIO, '+
                                         'CP_TOT_PER, '+
                                         'CP_GRAVADO, '+
                                         'CP_PAGADO, '+
                                         'CP_RETENID, '+
                                         'CP_ART_141, '+
                                         'CP_FAVOR, '+
                                         'CP_CONTRA, '+
                                         'CP_A_PAGAR, '+
                                         'CP_CALCULO, '+
                                         'US_CODIGO ) values ( '+
                                         ':CP_YEAR, '+
                                         ':CB_CODIGO, '+
                                         ':CB_SALARIO, '+
                                         ':CP_TOT_PER, '+
                                         ':CP_GRAVADO, '+
                                         ':CP_PAGADO, '+
                                         ':CP_RETENID, '+
                                         ':CP_ART_141, '+
                                         ':CP_FAVOR, '+
                                         ':CP_CONTRA, '+
                                         ':CP_A_PAGAR, '+
                                         ':CP_CALCULO, '+
                                         ':US_CODIGO )';
          Q_CREDITO_BORRAR: Result := 'delete from DECLARA where ( DC_YEAR = :DC_YEAR )';
          Q_CREDITO_INSERT: Result := 'insert into DECLARA ( '+
                                      'CB_CODIGO, '+
                                      'DC_YEAR, '+
                                      'DC_TOT_PER, '+
                                      'DC_GRAVADO, '+
                                      'DC_CREDITO ) values ( '+
                                      ':CB_CODIGO, '+
                                      ':DC_YEAR, '+
                                      ':DC_TOT_PER, '+
                                      ':DC_GRAVADO, '+
                                      ':DC_CREDITO )';
          Q_PRESTAMO_EXISTE: Result := 'select COUNT(*) CUANTOS from PRESTAMO where '+
                                       '( CB_CODIGO = :Empleado ) and '+
                                       '( PR_TIPO = :Tipo ) and '+
                                       '( PR_REFEREN = :Referencia )';
          Q_PRESTAMO_AGREGA: Result := 'insert into PRESTAMO'+
                                       '( CB_CODIGO, PR_TIPO, PR_REFEREN, PR_FECHA, PR_MONTO, PR_FORMULA, PR_STATUS, PR_SALDO ) values'+
                                       '(:CB_CODIGO,:PR_TIPO,:PR_REFEREN,:PR_FECHA,:PR_MONTO,:PR_FORMULA,:PR_STATUS, :PR_MONTO2 )';
          Q_PRESTAMO_MODIFICA: Result := 'update PRESTAMO set '+
                                         'PR_FECHA = :PR_FECHA, '+
                                         'PR_MONTO = :PR_MONTO, '+
                                         'PR_FORMULA = :PR_FORMULA, '+
                                         'PR_STATUS = :PR_STATUS, '+
                                         'PR_SALDO = PR_MONTO- PR_SALDO_I - PR_TOTAL - PR_ABONOS + PR_CARGOS '+
                                         'where '+
                                         '( CB_CODIGO = :CB_CODIGO ) and '+
                                         '( PR_TIPO = :PR_TIPO ) and'+
                                         '( PR_REFEREN = :PR_REFEREN )';
          Q_CIERRE_AHORRO_DEL_CANCELADOS: Result := Format( 'delete from AHORRO where ( AH_TIPO = :Tipo ) and ( CB_CODIGO = :Empleado ) and ( AH_STATUS = %d )', [ Ord( saCancelado ) ] );
          Q_CIERRE_AHORRO_DEL_CAR_ABO: Result := 'delete from ACAR_ABO where ( AH_TIPO = :Tipo ) and ( CB_CODIGO = :Empleado )';
          Q_CIERRE_AHORRO_PONER_EN_CEROS: Result := 'update AHORRO set '+
                                                    'AH_TOTAL = 0, '+
                                                    'AH_NUMERO = 0, '+
                                                    'AH_SALDO_I = 0, '+
                                                    'AH_CARGOS = 0, '+
                                                    'AH_ABONOS = 0, '+
                                                    'AH_SALDO = 0 '+
                                                    'where ( AH_TIPO = :Tipo ) and ( CB_CODIGO = :Empleado )';
          Q_CIERRE_AHORRO_FECHA_CIERRE: Result := 'update AHORRO set AH_FECHA = :Fecha where ( AH_TIPO = :Tipo ) and ( CB_CODIGO = :Empleado )';
          Q_CIERRE_AHORRO_ACUMULA_SALDO: Result := 'update AHORRO set '+
                                                   'AH_SALDO_I = ( AH_SALDO_I + AH_TOTAL + AH_ABONOS - AH_CARGOS ) '+
                                                   'where ( AH_TIPO = :Tipo ) and ( CB_CODIGO = :Empleado )';
          Q_CIERRE_AHORRO_INIT_CEROS: Result := 'update AHORRO set '+
                                                'AH_CARGOS = 0, '+
                                                'AH_ABONOS = 0, '+
                                                'AH_TOTAL = 0 '+
                                                'where ( AH_TIPO = :Tipo ) and ( CB_CODIGO = :Empleado )';
          Q_CIERRE_AHORRO_DEL_TODO: Result := 'delete from AHORRO where ( AH_TIPO = :Tipo ) and ( CB_CODIGO = :Empleado )';
          Q_CIERRE_PRESTAMO_DEL_SALDADOS: Result := 'delete from PRESTAMO where ( PR_TIPO = :Tipo ) and ( CB_CODIGO = :Empleado ) and ( PR_REFEREN = :Referencia )';
          Q_CIERRE_PRESTAMO_DEL_CANCELADOS: Result := Format( 'delete from PRESTAMO where ( PR_TIPO = :Tipo ) and ( CB_CODIGO = :Empleado ) and ( PR_REFEREN = :Referencia ) and ( PR_STATUS = %d )', [ Ord( spCancelado ) ] );
          Q_CIERRE_PRESTAMO_DEL_CAR_ABO: Result := 'delete from PCAR_ABO where ( PR_TIPO = :Tipo ) and ( CB_CODIGO = :Empleado ) and ( PR_REFEREN = :Referencia ) ';
          Q_CIERRE_PRESTAMO_INIT_SALDO: Result := 'update PRESTAMO set '+
                                                  'PR_SALDO_I = ( PR_SALDO_I + PR_TOTAL + PR_ABONOS - PR_CARGOS ) '+
                                                  'where ( PR_TIPO = :Tipo ) and ( CB_CODIGO = :Empleado ) and ( PR_REFEREN = :Referencia )';
          Q_CIERRE_PRESTAMO_INIT_CEROS: Result := 'update PRESTAMO set '+
                                                  'PR_CARGOS = 0, '+
                                                  'PR_ABONOS = 0, '+
                                                  'PR_TOTAL = 0 '+
                                                  'where ( PR_TIPO = :Tipo ) and ( CB_CODIGO = :Empleado ) and ( PR_REFEREN = :Referencia )';
          Q_CIERRE_PRESTAMO_FECHA_CIERRE: Result := 'update PRESTAMO set PR_FECHA = :Fecha where ( PR_TIPO = :Tipo ) and ( CB_CODIGO = :Empleado ) and ( PR_REFEREN = :Referencia )';
          Q_CONCEPTO_LEE: Result := 'select CO_TIPO from CONCEPTO where ( CO_NUMERO = %d )';
          Q_TMPLABOR_BORRAR: Result := 'delete from TMPLABOR where ( US_CODIGO = %d )';
          Q_TMPLABOR_AGREGAR: Result := 'insert into TMPLABOR( US_CODIGO, CB_CODIGO, CB_CHECA ) values ( %d, :CB_CODIGO, :CB_CHECA )';
          Q_CRED_APLICADO_FECHAS_PERIODO: Result := 'select MIN(PE_FEC_INI) FechaMin, MAX(PE_FEC_FIN) FechaMax ' +
                                                    'from PERIODO ' +
                                                    'where PE_YEAR = %d '+
                                                    'and PE_MES = %d' ;
          {$ifdef INTERBASE}
          Q_CRED_APLICADO_FILTRO: Result := '(select DIAS_ACT '+
                                            'from SP_RANGO_STATUS_ACT (COLABORA.CB_CODIGO, %s, %s ) ) >0 ';
          {$else}
          Q_CRED_APLICADO_FILTRO: Result := '( select dbo.SP_RANGO_STATUS_ACT( COLABORA.CB_CODIGO, %s, %s, 0 ) ) > 0';
          {$endif}
          Q_CRED_APLICADO_AFECTA_CONCEPTO: Result := 'EXECUTE PROCEDURE AFECTA_UN_CONCEPTO(:YEAR,:EMPLEADO,:CONCEPTO,:MES,:FACTOR,:MONTO )';
          Q_CIERRE_PRESTAMO_CANCELA_REG: Result := 'update PRESTAMO '+
                                                   'set PR_STATUS = :Status '+
                                                   'where ( CB_CODIGO = :Empleado ) and (PR_TIPO = :Tipo ) '+
                                                   'and ( PR_REFEREN = :Referencia )';
          Q_UPDATE_GLOBAL_FECHACORTE: Result := 'update GLOBAL set GL_FORMULA = ''%s'' where GL_CODIGO = %d';
          Q_DECLARACION_ANUAL_BORRAR: Result := 'DELETE FROM TMPDIMM WHERE TD_YEAR = %d'; //'DELETE FROM TMPDIMM WHERE TD_YEAR = %d AND US_CODIGO = %d';
          Q_DECLARACION_ANUAL_INSERT: Result := 'INSERT INTO TMPDIMM (TD_YEAR,US_CODIGO,CB_CODIGO,TD_CUANTOS,TD_REPETID, ' +
                                                'TD_DATA_01,TD_DATA_02,TD_DATA_03,TD_DATA_04,TD_DATA_05,TD_DATA_06,TD_DATA_07,TD_DATA_08,TD_DATA_09,TD_DATA_10, ' +
                                                'TD_DATA_11,TD_DATA_12,TD_DATA_13,TD_DATA_14,TD_DATA_15,TD_DATA_16,TD_DATA_17,TD_DATA_18,TD_DATA_19,TD_DATA_20, ' +
                                                'TD_DATA_21,TD_DATA_22,TD_DATA_23,TD_DATA_24,TD_DATA_25,TD_DATA_26,TD_DATA_27,TD_DATA_28,TD_DATA_29,TD_DATA_30, ' +
                                                'TD_DATA_31,TD_DATA_32,TD_DATA_33,TD_DATA_34,TD_DATA_35,TD_DATA_36,TD_DATA_37,TD_DATA_38,TD_DATA_39,TD_DATA_40, ' +
                                                'TD_DATA_41,TD_DATA_42,TD_DATA_43,TD_DATA_44,TD_DATA_45,TD_DATA_46,TD_DATA_47,TD_DATA_48,TD_DATA_49,TD_DATA_50, ' +
                                                'TD_DATA_51,TD_DATA_52,TD_DATA_53,TD_DATA_54,TD_DATA_55,TD_DATA_56,TD_DATA_57,TD_DATA_58,TD_DATA_59,TD_DATA_60, ' +
                                                'TD_DATA_61,TD_DATA_62,TD_DATA_63,TD_DATA_64,TD_DATA_65,TD_DATA_66,TD_DATA_67,TD_DATA_68,TD_DATA_69,TD_DATA_70, ' +
                                                'TD_DATA_71,TD_DATA_72,TD_DATA_73,TD_DATA_74,TD_DATA_75,TD_DATA_76,TD_DATA_77,TD_DATA_78,TD_DATA_79,TD_DATA_80, ' +
                                                'TD_DATA_81,TD_DATA_82,TD_DATA_83,TD_DATA_84,TD_DATA_85,TD_DATA_86,TD_DATA_87,TD_DATA_88,TD_DATA_89,TD_DATA_90, ' +
                                                'TD_DATA_91,TD_DATA_92,TD_DATA_93,TD_DATA_94,TD_DATA_95,TD_DATA_96,TD_DATA_97,TD_DATA_98,TD_DATA_99,TD_DATA_A0, ' +
                                                'TD_MES_INI,TD_MES_FIN, '+
                                                'TD_SELLO,TD_ORIG,TD_DIGES,TD_FEC_SEL,TD_HOR_SEL )'+

                                                'VALUES(:TD_YEAR,:US_CODIGO,:CB_CODIGO,:TD_CUANTOS,:TD_REPETID, '+
                                                ':TD_DATA_01,:TD_DATA_02,:TD_DATA_03,:TD_DATA_04,:TD_DATA_05,:TD_DATA_06,:TD_DATA_07,:TD_DATA_08,:TD_DATA_09,:TD_DATA_10, ' +
                                                ':TD_DATA_11,:TD_DATA_12,:TD_DATA_13,:TD_DATA_14,:TD_DATA_15,:TD_DATA_16,:TD_DATA_17,:TD_DATA_18,:TD_DATA_19,:TD_DATA_20, ' +
                                                ':TD_DATA_21,:TD_DATA_22,:TD_DATA_23,:TD_DATA_24,:TD_DATA_25,:TD_DATA_26,:TD_DATA_27,:TD_DATA_28,:TD_DATA_29,:TD_DATA_30, ' +
                                                ':TD_DATA_31,:TD_DATA_32,:TD_DATA_33,:TD_DATA_34,:TD_DATA_35,:TD_DATA_36,:TD_DATA_37,:TD_DATA_38,:TD_DATA_39,:TD_DATA_40, ' +
                                                ':TD_DATA_41,:TD_DATA_42,:TD_DATA_43,:TD_DATA_44,:TD_DATA_45,:TD_DATA_46,:TD_DATA_47,:TD_DATA_48,:TD_DATA_49,:TD_DATA_50, ' +
                                                ':TD_DATA_51,:TD_DATA_52,:TD_DATA_53,:TD_DATA_54,:TD_DATA_55,:TD_DATA_56,:TD_DATA_57,:TD_DATA_58,:TD_DATA_59,:TD_DATA_60, ' +
                                                ':TD_DATA_61,:TD_DATA_62,:TD_DATA_63,:TD_DATA_64,:TD_DATA_65,:TD_DATA_66,:TD_DATA_67,:TD_DATA_68,:TD_DATA_69,:TD_DATA_70, ' +
                                                ':TD_DATA_71,:TD_DATA_72,:TD_DATA_73,:TD_DATA_74,:TD_DATA_75,:TD_DATA_76,:TD_DATA_77,:TD_DATA_78,:TD_DATA_79,:TD_DATA_80, ' +
                                                ':TD_DATA_81,:TD_DATA_82,:TD_DATA_83,:TD_DATA_84,:TD_DATA_85,:TD_DATA_86,:TD_DATA_87,:TD_DATA_88,:TD_DATA_89,:TD_DATA_90, ' +
                                                ':TD_DATA_91,:TD_DATA_92,:TD_DATA_93,:TD_DATA_94,:TD_DATA_95,:TD_DATA_96,:TD_DATA_97,:TD_DATA_98,:TD_DATA_99,:TD_DATA_A0, '+
                                                ':TD_MES_INI,:TD_MES_FIN, '+
                                                ':TD_SELLO,:TD_ORIG,:TD_DIGES,:TD_FEC_SEL,:TD_HOR_SEL )';

          {$ifdef INTERBASE}
          Q_GET_DIAS_AGUINALDO: Result := ' select DIAS_AG from SP_IMSS_YEAR( %s, %d )';
          {$else}
          Q_GET_DIAS_AGUINALDO: Result := ' select DIAS_AG = DBO.SP_IMSS_YEAR( %s, %d, %d )';
          {$endif}
          Q_DECLARACION_CIERRE: Result := 'update TMPDIMMTOT set TD_STATUS = %d, US_CODIGO = %d where TD_YEAR = %d';
          Q_DECLARACION_CIERRE_BORRA: Result := 'delete from TMPDIMMTOT where TD_YEAR = %d';
          Q_DECLARACION_CIERRE_INS: Result := 'insert into TMPDIMMTOT (TD_YEAR, TD_STATUS, US_CODIGO) values( %d, %d, %d )';
          Q_PREVIO_ISR_BORRAR: Result := 'delete from ISR_AJUSTE where IS_YEAR = :IS_YEAR and IS_MES = :IS_MES ';
          Q_PREVIO_ISR_INSERT: Result := ' INSERT INTO ISR_AJUSTE    ' +
                                         '            (IS_YEAR,      ' +
                                         '             IS_MES,       ' +
                                         '             CB_CODIGO,    ' +
                                         '             CB_SALARIO,   ' +
                                         '             IS_ING_BR,  ' +
                                         '             IS_GRAVADO,   ' +
                                         '             IS_RETENID,   ' +
                                         '             IS_SUBE,      ' +
                                         '             IS_IMPCALC,   ' +
                                         '             IS_FAVOR,     ' +
                                         '             IS_CONTRA,    ' +
                                         '             US_CODIGO )   ' +
                                         '      VALUES               ' +
                                         '            (:IS_YEAR,     ' +
                                         '             :IS_MES,      ' +
                                         '             :CB_CODIGO,   ' +
                                         '             :CB_SALARIO,  ' +
                                         '             :IS_ING_BR, ' +
                                         '             :IS_GRAVADO,  ' +
                                         '             :IS_RETENID,  ' +
                                         '             :IS_SUBE,     ' +
                                         '             :IS_IMPCALC,  ' +
                                         '             :IS_FAVOR,    ' +
                                         '             :IS_CONTRA,   ' +
                                         '             :US_CODIGO )  ';
          end;
end;

function SetVacioACero( const sValor: String ): String;
begin
     if ZetaCommonTools.StrVacio( sValor ) then
        Result := '0'
     else
         Result := sValor;
end;

function SetVacioABooleano( const sValor: String; const lValor: Boolean ): String;
begin
     if ZetaCommonTools.StrVacio( sValor ) then
     begin
          if lValor then
             Result := '.T.'
          else
              Result := '.F.';
     end
     else
         Result := sValor;
end;

{ ******* TdmServerAnualNomina ******** }

class procedure TdmServerAnualNomina.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
     if Register then
     begin
          inherited UpdateRegistry(Register, ClassID, ProgID);
          EnableSocketTransport(ClassID);
          EnableWebTransport(ClassID);
     end
     else
     begin
          DisableSocketTransport(ClassID);
          DisableWebTransport(ClassID);
          inherited UpdateRegistry(Register, ClassID, ProgID);
     end;
end;

procedure TdmServerAnualNomina.MtsDataModuleCreate(Sender: TObject);
begin
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
     FOffsetCampos :=0;
end;

procedure TdmServerAnualNomina.MtsDataModuleDestroy(Sender: TObject);
begin
     //Variable de la Declaracion Anual
     FreeAndNil( dmSelloDigital );
     FreeAndNil( FFormulasPendientesTD );

     ZetaSQLBroker.FreeZetaCreator( oZetaCreator );
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
end;

{ *********** Clases Auxiliares ************ }

{$ifdef DOS_CAPAS}
procedure TdmServerAnualNomina.CierraEmpresa;
begin
end;
{$endif}

function TdmServerAnualNomina.GetSQLBroker: TSQLBroker;
begin
{$ifdef DOS_CAPAS}
     Result := ZetaSQLBroker.oZetaSQLBroker;
{$else}
     Result := Self.oZetaSQLBroker;
{$endif}
end;

procedure TdmServerAnualNomina.InitCreator;
begin
     if not Assigned( oZetaCreator ) then
        oZetaCreator := ZetaSQLBroker.GetZetaCreator( oZetaProvider );
end;

procedure TdmServerAnualNomina.InitBroker( const lRegistraFuncionesAnuales: Boolean );
begin
     InitCreator;
     with oZetaCreator do
     begin
          if lRegistraFuncionesAnuales then
             RegistraFunciones( [ efComunes,efAnuales ] )
          else
              RegistraFunciones([efComunes,efAguinaldo]);
     end;
     oZetaSQLBroker := ZetaSQLBroker.GetSQLBroker( oZetaCreator );
end;

procedure TdmServerAnualNomina.ClearBroker;
begin
     ZetaSQLBroker.FreeSQLBroker( oZetaSQLBroker );
end;

procedure TdmServerAnualNomina.InitNomina;
begin
     InitCreator;
     FNomina := TNomina.Create( oZetaCreator );
end;

procedure TdmServerAnualNomina.ClearNomina;
begin
     FreeAndNil( FNomina );
end;

function TdmServerAnualNomina.EsPercepcion( const iConcepto: Integer ): Boolean;
var
   FDataset: TZetaCursor;
begin
     with oZetaProvider do
     begin
          FDataSet := CreateQuery( Format( GetSQLScript( Q_CONCEPTO_LEE ), [ iConcepto ] ) );
          try
             with FDataset do
             begin
                  Active := True;
                  if IsEmpty then
                     Result := False
                  else
                      Result := not ( eTipoConcepto( FieldByName( 'CO_TIPO' ).AsInteger ) in [ coDeduccion, coObligacion ] );
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataSet );
          end;
     end;
end;

function TdmServerAnualNomina.Rastreador: TRastreador;
begin
     Result := oZetaCreator.Rastreador;
end;

procedure TdmServerAnualNomina.SetRastreo(DataSet: TZetaCursor; oColumna: TSQLColumna; oCampo: TField);
begin
     oCampo.AsString:= oZetaCreator.Rastreador.Rastro;
     { Defecto 1214: Lo limpia porque se queda con lo del empleado anterior }
     oZetaCreator.Rastreador.RastreoBegin;
end;

{ ******* Interfases ( Paletitas ) ******* }

{ ******** Calcular Aguinaldos *********** }

procedure TdmServerAnualNomina.CalcularAguinaldoBuildDataset;
var
   sFecha, sFaltas, sAnticipos, sIncapacidades: String;
   dFechaFinal, dFechaInicial: TDate;

begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               if ( ParamByName( 'Proporcional' ).AsInteger = 1 ) then
                  sFecha := 'CB_FEC_ANT'
               else
                   sFecha := 'CB_FEC_ING';
               dFechaInicial := ParamByName( 'dInicial' ).AsDateTime;
               dFechaFinal := ParamByName( 'dFinal' ).AsDateTime;
               sFaltas := SetVacioACero( ParamByName( 'Faltas' ).AsString );
               sIncapacidades := SetVacioACero( ParamByName( 'Incapacidades' ).AsString );
               sAnticipos := SetVacioACero( ParamByName( 'Anticipos' ).AsString );
          end;
     end;
     with SQLBroker do
     begin
          Init( enEmpleado );
          with Agente do
          begin
               AgregaColumna( 'COLABORA.CB_CODIGO', True, Entidad, tgNumero, 10, 'CB_CODIGO' );
               AgregaColumna( 'COLABORA.CB_SALARIO', True, Entidad, tgFloat, 10, 'CB_SALARIO' );
               AgregaColumna( Format( 'IMSS( 3, ''%s'', COLABORA.%s, COLABORA.CB_TABLASS )',
                              [ ZetaCommonTools.FechaAsStr( dFechaFinal ), sFecha ] ),
                              False, Entidad, tgAutomatico, 10, 'DIAS_AG' );
               AgregaColumna( 'COLABORA.CB_TABLASS', True, Entidad, tgTexto, 2, 'CB_TABLASS' );

               AgregaColumna( sFecha, True, Entidad, tgFecha, 10, 'CB_INGRESO' );

               AgregaColumna( sFaltas, False, Entidad, tgFloat, 10, 'FALTA_AGUINAL' );
               AgregaColumna( sIncapacidades, False, Entidad, tgFloat, 10, 'INCAPACI_AGUINAL' );
               AgregaColumna( sAnticipos, False, Entidad, tgFloat, 10, 'ANTICIPO_AGUINAL' );

               AgregaColumna( Comillas( FechaAsStr( dFechaInicial ) ), True, Entidad, tgFecha, 10, 'FechaInicial');
               AgregaColumna( Comillas( FechaAsStr( dFechaFinal ) ), True, Entidad, tgFecha, 10, 'FechaFinal');

               AgregaFiltro ( 'COLABORA.CB_ACTIVO = ''S''', True, Entidad );
               AgregaFiltro( Format( '%s<=''%s''', [ sFecha, ZetaCommonTools.DateToStrSQL( dFechaFinal ) ] ), True, Entidad );
               AgregaOrden( 'COLABORA.CB_CODIGO', True, Entidad );

          end;
          with oZetaProvider do
          begin
               AgregaRangoCondicion( ParamList );
               FiltroConfidencial( EmpresaActiva );
               BuildDataset( EmpresaActiva );
          end;
     end;
end;

function TdmServerAnualNomina.CalcularAguinaldoDataset( DataSet: TDataSet ): OleVariant;
var
   iEmpleado: TNumEmp;
   iYear: Integer;
   rBase, rFaltas, rIncapacidades, rNetos, rAnticipos, rSalario, rDiasYear, rDias, rTotal: TPesos;
   sRequeridos: String;
   dIngreso, dInicial, dFinal: TDate;
   FAguinaldo,FAguinaldoYear: TZetaCursor;

const
     K_DIAS_AGUINALDO = 3;

function DiasEnCurso( const dIngreso, dReferencia: TDate ): Integer;
var
   dPrimero: TDate;
begin
     dPrimero := ZetaCommonTools.FirstDayOfYear( ZetaCommonTools.TheYear( dReferencia ) );
     if ( dPrimero < dIngreso ) then
        dPrimero := dIngreso;
     Result := ( Trunc( dReferencia ) - Trunc( dPrimero ) + 1 );
end;

function DiasAguinaldo( iAnios:Integer ): TDiasHoras;
begin
     with oZetaProvider do
     begin
          FAguinaldoYear := CreateQuery( Format( GetSQLScript( Q_GET_DIAS_AGUINALDO ), [ EntreComillas( DataSet.FieldByName('CB_TABLASS').AsString ),iAnios ,K_DIAS_AGUINALDO ] ) );
          try
             with FAguinaldoYear do
             begin
                  Active := True;
                  if IsEmpty then
                     Result := 0
                  else
                      Result := FieldByName('DIAS_AG').AsFloat;
                  Active := False;
             end;
          finally
                 FreeAndNil( FAguinaldoYear );
          end;
     end;
end;

begin
     {
     iCuantos := 0;
     }
     rTotal := 0;
     sRequeridos := '';
     with oZetaProvider do
     begin
          with ParamList do
          begin
               iYear := ParamByName( 'Year' ).AsInteger;
               dInicial := ParamByName( 'dInicial' ).AsDateTime;
               dFinal := ParamByName( 'dFinal' ).AsDateTime;

               { PENDIENTE: � COMO REGRESAN LOS VALORES ? }
               {
               ParamByName( 'iCuantos' ).AsInteger := 0;
               ParamByName( 'rTotal' ).AsFloat := 0;
               }
          end;
          YearDefault := iYear;
          if IsLeapYear( iYear ) then
             rDiasYear := 366
          else
             rDiasYear := 365;
          if OpenProcess( prNOCalculoAguinaldo, Dataset.RecordCount, FListaParametros, FListaFormulas ) then
          begin
               try
                  EmpiezaTransaccion;
                  try
                     { Borra Aguinaldos Anteriores }
                     //CV: CreateQuery--ok
                     //Variable local
                     FAguinaldo := CreateQuery( Format( GetSQLScript( Q_AGUINALDO_BORRAR ), [ iYear ] ) );
                     Ejecuta( FAguinaldo );
                     TerminaTransaccion( True );
                  except
                        on Error: Exception do
                        begin
                             TerminaTransaccion( False );
                             raise;
                        end;
                  end;
                  { Prepara Inserci�n de Aguinaldos }
                  PreparaQuery( FAguinaldo, GetSQLScript( Q_AGUINALDO_AGREGAR ) );
                  ParamAsInteger( FAguinaldo, 'AG_YEAR', iYear );
                  with Dataset do
                  begin
                       while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                       begin
                            iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                            EmpiezaTransaccion;
                            try
                               dIngreso := FieldByName( 'CB_INGRESO' ).AsDateTime;
                               //rDias := FieldByName( 'DIAS_AG' ).AsFloat;
                               rDias := DiasAguinaldo( ( zYearsBetween(dIngreso,dFinal) + 1 ) );
                               rSalario := FieldByName( 'CB_SALARIO' ).AsFloat;
                               rBase := ZetaCommonTools.rMin( rDiasYear, DiasEnCurso( ZetaServerTools.dMax( dIngreso, dInicial ), dFinal ) );
                               rFaltas := ZetaCommonTools.Redondea( FieldByName( 'FALTA_AGUINAL' ).AsFloat );
                               rIncapacidades := ZetaCommonTools.Redondea( FieldByName( 'INCAPACI_AGUINAL' ).AsFloat );
                               rAnticipos := FieldByName( 'ANTICIPO_AGUINAL' ).AsFloat;
                               rFaltas := ZetaCommonTools.rMin( rFaltas, rBase );
                               rNetos := rBase - rFaltas;
                               rIncapacidades := ZetaCommonTools.rMin( rIncapacidades, rNetos );
                               rNetos := rNetos - rIncapacidades;
                               rDias := ZetaCommonTools.Redondea( rNetos / rDiasYear * rDias );
                               rDias := ZetaCommonTools.Redondea( ZetaCommonTools.rMax( ( rDias - rAnticipos ), 0 ) );
                               ParamAsInteger( FAguinaldo, 'CB_CODIGO', iEmpleado );
                               ParamAsDate( FAguinaldo, 'AG_FEC_ING', dIngreso );
                               ParamAsInteger( FAguinaldo, 'AG_BASE', ZetaServerTools.Round2Integer( rBase ) );
                               ParamAsInteger( FAguinaldo, 'AG_FALTAS', ZetaServerTools.Round2Integer( rFaltas ) );
                               ParamAsInteger( FAguinaldo, 'AG_INCAPA', ZetaServerTools.Round2Integer( rIncapacidades ) );
                               ParamAsInteger( FAguinaldo, 'AG_NETOS', ZetaServerTools.Round2Integer( rNetos ) );
                               ParamAsFloat( FAguinaldo, 'AG_DIAS_AG', rDias );
                               ParamAsFloat( FAguinaldo, 'AG_SALARIO', ZetaCommonTools.Redondea( rSalario ) );
                               ParamAsFloat( FAguinaldo, 'AG_AGUINAL', ZetaCommonTools.Redondea( rSalario * rDias ) );
                               Ejecuta( FAguinaldo );
                               {
                               iCuantos := iCuantos + 1;
                               }
                               rTotal := rTotal + rDias * rSalario;
                               TerminaTransaccion( True );
                            except
                                  on Error: Exception do
                                  begin
                                       TerminaTransaccion( False );
                                       Log.Excepcion( iEmpleado, 'Error Al Calcular Aguinaldos', Error, DescripcionParams );
                                  end;
                            end;
                            Next;
                       end;
                  end;
               except
                     on Error: Exception do
                     begin
                          Log.Excepcion( 0, 'Error Al Calcular Aguinaldos', Error );
                     end;
               end;
               FreeAndNil(FAguinaldo);
          end;
          Result := CloseProcess;
     end;

end;

procedure TdmServerAnualNomina.CalcularAguinaldoParametros;
const
     aProporcional: array[ 0..1 ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'Fecha de Ingreso', 'Fecha de Antig�edad' );

begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'A�o: ' + ParamByName( 'Year' ).AsString +
                         K_PIPE + 'Inicio: ' + FechaCorta( ParamByName( 'dInicial' ).AsDateTime ) +
                         K_PIPE + 'Fin: ' + FechaCorta( ParamByName( 'dFinal' ).AsDateTime ) +
                         K_PIPE + 'Proporcional: ' + aProporcional[ ParamByName( 'Proporcional' ).AsInteger ];
          FListaFormulas :=    VACIO;
          FListaFormulas :=    K_PIPE + 'F�rmulas ' +
                     K_PIPE + 'Faltas: ' + ParamByName( 'Faltas' ).AsString +
                     K_PIPE + 'Incapacidades: ' + ParamByName( 'Incapacidades' ).AsString +
                     K_PIPE + 'Anticipos: ' + ParamByName( 'Anticipos' ).AsString;
     end;
end;

function TdmServerAnualNomina.CalcularAguinaldo(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitBroker( False );
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList( Parametros );
             GetDatosPeriodo;
             GetDatosImss;
             GetDatosActivos;
             with ParamList do
             begin
                  YearDefault := ParamByName( 'Year' ).AsInteger;  // Para Evaluar Funci�n A()
             end;
        end;
        CalcularAguinaldoParametros;
        CalcularAguinaldoBuildDataset;
        Result := CalcularAguinaldoDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
            ClearBroker;
     end;
     SetComplete;
end;

{ ********** Calcular PTU ************ }

procedure TdmServerAnualNomina.CalcularPTUBuildDataset;
var
   sSumaDias, sPercepciones, sFiltroDirector, sFiltroPlanta, sFiltroEventual: String;
begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               sSumaDias := SetVacioACero( ParamByName( 'SumaDias' ).AsString );
               sPercepciones := SetVacioACero( ParamByName( 'Percepciones' ).AsString );
               sFiltroDirector := SetVacioABooleano( ParamByName( 'FiltroDirector' ).AsString, False );
               sFiltroPlanta := SetVacioABooleano( ParamByName( 'FiltroPlanta' ).AsString, True );
               sFiltroEventual := SetVacioABooleano( ParamByName( 'FiltroEventual' ).AsString, True );
          end;
          with SQLBroker do
          begin
               Init( enEmpleado );
               with Agente do
               begin
                    AgregaColumna( 'COLABORA.CB_CODIGO', True, Entidad, tgNumero, 10, 'CB_CODIGO' );
                    AgregaColumna( sSumaDias, False, Entidad, tgFloat, 10, 'PTU_SUMA_DIAS' );
                    AgregaColumna( sPercepciones, False, Entidad, tgFloat, 10, 'PERCEPCIONES' );

                    AgregaColumna( sFiltroPlanta, False, Entidad, tgBooleano, 10, 'FILTRO_PLANTA' );
                    AgregaColumna( sFiltroEventual, False, Entidad, tgBooleano, 10, 'FILTRO_EVENTUAL' );
                    AgregaColumna( sFiltroDirector, False, Entidad, tgBooleano, 10, 'FILTRO_DIRECTOR' );

                    AgregaOrden( 'COLABORA.CB_CODIGO', True, Entidad );
               end;
               AgregaRangoCondicion( ParamList );
               FiltroConfidencial( EmpresaActiva );
               BuildDataset( EmpresaActiva );
          end;
     end;
end;

function TdmServerAnualNomina.CalcularPTUDataset( DataSet: TDataSet ): OleVariant;
const
     K_BOOLEANO_SI = 'SI';
     K_BOOLEANO_NO = 'NO';
var
   iEmpleado: TNumEmp;
   iYear: Integer;
   rTopeDado, rUtilidad, rSumaDias, rPercepciones, rDiasYear, rTopeSalario: TPesos;
   lIncluye: Boolean;
   FUtilidad: TZetaCursor;
   nRegistros : Integer;
begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               iYear := ParamByName( 'Year' ).AsInteger;
               rUtilidad := ParamByName( 'Utilidad' ).AsFloat;
               rTopeDado := ParamByName( 'SalarioTope' ).AsFloat;
          end;
          YearDefault := iYear;
          if IsLeapYear( iYear ) then
             rDiasYear := 366
          else
             rDiasYear := 365;
          rTopeSalario := 0;
          { El 3 viene por el Stored Procedure SP_TOPA_PTU }
          {$ifdef INTERBASE}
          nRegistros := 3 * Dataset.RecordCount;
          {$else}
          nRegistros := DataSet.RecordCount;
          {$endif}
          if OpenProcess(prNOCalculoPTU, nRegistros, FListaParametros, FListaFormulas ) then
          begin
               try
                  EmpiezaTransaccion;
                  try
                     { Borra PTU's Anteriores }
                     //CV: CreateQuery--ok
                     //Variable local
                     FUtilidad := CreateQuery( GetSQLScript( Q_PTU_BORRAR ) );
                     ParamAsInteger( FUtilidad, 'RU_YEAR', iYear );
                     Ejecuta( FUtilidad );
                     TerminaTransaccion( True );
                  except
                        on Error: Exception do
                        begin
                             TerminaTransaccion( False );
                             raise;
                        end;
                  end;
                  PreparaQuery( FUtilidad, GetSQLScript( Q_PTU_INSERTAR ) );
                  ParamAsInteger( FUtilidad, 'RU_YEAR', iYear );
                  with Dataset do
                  begin
                       while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                       begin
                            iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                            if ( FieldByName( 'FILTRO_DIRECTOR' ).AsString = K_BOOLEANO_NO ) then
                            begin
                                 { Calcular Acumulados }
                                 rSumaDias := ZetaCommonTools.rMin( rDiasYear, ZetaCommonTools.rMax( 0, FieldByName( 'PTU_SUMA_DIAS' ).AsCurrency ) );
                                 rPercepciones := ZetaCommonTools.rMax( 0, ZetaCommonTools.Redondea( FieldByName( 'PERCEPCIONES' ).AsFloat ) );
                                 if ( rSumaDias >= 60 ) then { Deteminar si se incluye al empleado en el reparto }
                                    lIncluye := True
                                 else
                                    if ( ( rSumaDias > 0 ) or ( rPercepciones > 0 ) ) then
                                       lIncluye := ( FieldByName( 'FILTRO_EVENTUAL' ).AsString = K_BOOLEANO_NO )
                                    else
                                       lIncluye := False;
                                 if lIncluye then
                                 begin
                                      EmpiezaTransaccion;
                                      try
                                         ParamAsInteger( FUtilidad, 'CB_CODIGO', iEmpleado );
                                         ParamAsInteger( FUtilidad, 'RU_DIAS', ZetaServerTools.Round2Integer( rSumaDias ) );
                                         ParamAsFloat( FUtilidad, 'RU_MONTO', rPercepciones );
                                         ParamAsInteger( FUtilidad, 'US_CODIGO', UsuarioActivo );
                                         Ejecuta( FUtilidad );
                                         TerminaTransaccion( True );
                                      except
                                            on Error: Exception do
                                            begin
                                                 TerminaTransaccion( False );
                                                 Log.Excepcion( iEmpleado, 'Error Al Calcular PTU ', Error, DescripcionParams );
                                            end;
                                      end;
                                 end;
                                 if ( rTopeDado <= 0 ) and ( FieldByName( 'FILTRO_PLANTA' ).AsString = K_BOOLEANO_SI ) then
                                 begin
                                      if ( rPercepciones > rTopeSalario ) then
                                         rTopeSalario := rPercepciones;
                                 end;
                            end;
                            Next;
                       end;
                  end;
                  { Aplicar Tope de Salario: Si no se especific�, tomar el calculado }
                  if ( rTopeDado <= 0 ) then
                     rTopeSalario := 1.2 * rTopeSalario  // 120% de Empleado de Planta m�s alto
                  else
                      rTopeSalario := rTopeDado;
                  with FUtilidad do
                  begin
                       Active := False;
                       EmpiezaTransaccion;
                       try
                          { Totaliza PTU's Calculados }
                          PreparaQuery( FUtilidad, GetSQLScript( Q_PTU_TOTALIZAR ) );
                          ParamAsInteger( FUtilidad, 'nYear', iYear );
                          ParamAsFloat( FUtilidad, 'nUtilidades', rUtilidad );
                          ParamAsFloat( FUtilidad, 'nTopeSalario', rTopeSalario );
                          {$ifdef INTERBASE}
                          Active := True;
                          while not Eof and CanContinue( FieldByName( 'Empleado' ).AsInteger ) do
                          begin
                               Next;
                          end;
                          {$else}
                          Ejecuta( FUtilidad );
                          {$endif}
                          TerminaTransaccion( True );
                       except
                             on Error: Exception do
                             begin
                                  //TerminaTransaccion( False );
                                  RollBackTransaccion;
                                  raise;
                             end;
                       end;
                       Active := False;
                  end;
               except
                     on Error: Exception do
                     begin
                          Log.Excepcion( 0, 'Error Al Calcular PTU ', Error, DescripcionParams );
                     end;
               end;
               FreeAndNil(FUtilidad);
          end;
          Result := CloseProcess;
     end;
end;

procedure TdmServerAnualNomina.CalcularPTUParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'A�o: ' + ParamByName( 'Year' ).AsString +
                         K_PIPE + Format( 'Utilidad: $ %10.2n', [ ParamByName( 'Utilidad' ).AsFloat ] ) +
                         K_PIPE + Format( 'Salario Tope: $ %10.2n', [ ParamByName( 'SalarioTope' ).AsFloat ] );
          FListaFormulas :=    VACIO;
          FListaFormulas :=    K_PIPE + 'F�rmulas' +
                     K_PIPE + 'Percepciones: ' + ParamByName( 'Percepciones' ).AsString +
                     K_PIPE + 'Suma D�as: ' + ParamByName( 'SumaDias' ).AsString +
                     K_PIPE + 'Filtro Director: ' + ParamByName( 'FiltroDirector' ).AsString +
                     K_PIPE + 'Filtro Planta: ' + ParamByName( 'FiltroPlanta' ).AsString +
                     K_PIPE + 'Filtro Eventual: ' + ParamByName( 'FiltroEventual' ).AsString;
     end;
end;

function TdmServerAnualNomina.CalcularPTU(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitBroker( False );
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList( Parametros );
             GetDatosPeriodo;
             GetDatosImss;
             GetDatosActivos;
             YearDefault := ParamList.ParamByName( 'Year' ).AsInteger;  // Para Evaluar Funci�n A()
        end;
        CalcularPTUParametros;
        CalcularPTUBuildDataset;
        Result := CalcularPTUDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
            ClearBroker;
     end;
     SetComplete;
end;

{ ********** Calcular Reparto de Ahorros ************ }

procedure TdmServerAnualNomina.CalcularRepartoAhorroBuildDataset;
var
   sProporcional, sAhorro: String;
begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               sProporcional := SetVacioACero( ParamByName( 'Proporcional' ).AsString );
               sAhorro := ParamByName( 'TipoAhorro' ).AsString;
          end;
          with SQLBroker do
          begin
               Init( enAhorro );
               with Agente do
               begin
                    AgregaColumna( 'AHORRO.CB_CODIGO', True, Entidad, tgNumero, 10, 'CB_CODIGO' );
                    AgregaColumna( sProporcional, False, Entidad, tgFloat, 10, 'AHORRO_PROPORCIONAL' );
                    AgregaFiltro( 'COLABORA.CB_ACTIVO = ''S''', True, enEmpleado );
                    AgregaFiltro( 'AHORRO.AH_STATUS = 0', True, enAhorro );
                    AgregaFiltro( Format( 'AHORRO.AH_TIPO = ''%s''', [ sAhorro ] ), True, enAhorro );
                    AgregaOrden( 'AHORRO.CB_CODIGO', True, Entidad );
               end;
               AgregaRangoCondicion( ParamList );
               FiltroConfidencial( EmpresaActiva );
               BuildDataset( EmpresaActiva );
          end;
     end;
end;

function TdmServerAnualNomina.CalcularRepartoAhorroDataset( DataSet: TDataSet ): OleVariant;
var
   iEmpleado: TNumEmp;
   iYear: Integer;
   rMonto, rAhorrado, rTotal: TPesos;
   rReparto: Double;
   sAhorro: String;
   FAhorro, FUpdate: TZetaCursor;
begin
     rTotal := 0;
     { Set de Year Activo }
     with oZetaProvider do
     begin
          with ParamList do
          begin
               sAhorro := ParamByName( 'TipoAhorro' ).AsString;
               rMonto := ParamByName( 'Monto' ).AsFloat;
               iYear := ParamByName( 'Year' ).AsInteger;
          end;
          YearDefault := iYear;
          if OpenProcess( prNORepartoAhorro, Dataset.RecordCount, FListaParametros, FListaFormulas ) then
          begin
               try
                  FAhorro := CreateQuery( GetSQLScript( Q_AHORRO_BORRAR ) );
                  try
                     EmpiezaTransaccion;
                     try
                        //CV: CreateQuery--ok
                        //Variable local
                        ParamAsInteger( FAhorro, 'RA_YEAR', iYear );
                        ParamAsChar( FAhorro, 'TB_CODIGO', sAhorro, K_ANCHO_CODIGO1 );
                        Ejecuta( FAhorro );
                        TerminaTransaccion( True );
                     except
                           on Error: Exception do
                           begin
                                TerminaTransaccion( False );
                                raise;
                           end;
                     end;
                     PreparaQuery( FAhorro, GetSQLScript( Q_AHORRO_INSERT ) );
                     ParamAsInteger( FAhorro, 'RA_YEAR', iYear );
                     ParamAsChar( FAhorro, 'TB_CODIGO', sAhorro, K_ANCHO_CODIGO1 );
                     with Dataset do
                     begin
                          while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                          begin
                               iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                               EmpiezaTransaccion;
                               try
                                  rAhorrado := Redondea( FieldByName('AHORRO_PROPORCIONAL').AsFloat );
                                  if ( rAhorrado > 0 ) then
                                  begin
                                       ParamAsInteger( FAhorro, 'CB_CODIGO', iEmpleado );
                                       ParamAsFloat( FAhorro, 'RA_AHORRO', rAhorrado );
                                       Ejecuta( FAhorro );
                                  end;
                                  rTotal := rTotal + rAhorrado;
                                  {
                                  iCuantos := iCuantos + 1;
                                  }
                                  TerminaTransaccion( True );
                               except
                                     on Error: Exception do
                                     begin
                                          TerminaTransaccion( False );
                                          Log.Excepcion( iEmpleado, 'Error Al Calcular Reparto de Ahorros ', Error, DescripcionParams );
                                     end;
                               end;
                               Next;
                          end;
                     end;
                     if ( rTotal > 0 ) then
                     begin
                          rReparto := ( rMonto / rTotal );
                          PreparaQuery( FAhorro,Format( GetSQLScript( Q_AHORRO_LIST ), [ iYear, sAhorro ] ) );
                          FUpdate := CreateQuery( Format( GetSQLScript( Q_AHORRO_UPDATE ), [ iYear, sAhorro ] ) );
                          try
                             EmpiezaTransaccion;
                             try
                                { Totaliza Ahorros Calculados }
                                //CV: CreateQuery--ok
                                //Variable local
                                with FAhorro do
                                begin
                                     Active := True;
                                     while not Eof do
                                     begin
                                          ParamAsInteger( FUpdate, 'CB_CODIGO', FieldByName( 'CB_CODIGO' ).AsInteger );
                                          ParamAsFloat( FUpdate, 'RA_REPARTO', ZetaCommonTools.Redondea( rReparto * FieldByName( 'RA_AHORRO' ).AsFloat ) );
                                          Ejecuta( FUpdate );
                                          Next;
                                     end;
                                     Active := False;
                                end;
                                TerminaTransaccion( True );
                             except
                                   on Error: Exception do
                                   begin
                                        TerminaTransaccion( False );
                                        raise;
                                   end;
                             end;
                          finally
                                 FreeAndNil( FUpdate );
                          end;
                     end;
                  finally
                         FreeAndNil( FAhorro );
                  end;
               except
                     on Error: Exception do
                     begin
                          Log.Excepcion( 0, 'Error Al Calcular Reparto de Ahorros', Error, DescripcionParams );
                     end;
               end;
          end;
          Result := CloseProcess;
     end;
end;

procedure TdmServerAnualNomina.CalcularRepartoAhorroParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'A�o: ' + ParamByName( 'Year' ).AsString +
                              K_PIPE + 'Tipo de Ahorro: ' + ParamByName( 'TipoAhorro' ).AsString +
                              K_PIPE + Format( 'Monto: %10.2n', [ ParamByName( 'Monto' ).AsFloat ] );
          FListaFormulas :=    VACIO;
          FListaFormulas :=    K_PIPE + 'Porporcional A: ' + ParamByName( 'Proporcional' ).AsString;
     end;
end;

function TdmServerAnualNomina.CalcularRepartoAhorro(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitBroker( False );
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList( Parametros );
             GetDatosPeriodo;
             GetDatosImss;
             GetDatosActivos;
             YearDefault := ParamList.ParamByName( 'Year' ).AsInteger;  // Para Evaluar Funci�n A()
        end;
        CalcularRepartoAhorroParametros;
        CalcularRepartoAhorroBuildDataset;
        Result := CalcularRepartoAhorroDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
            ClearBroker;
     end;
     SetComplete;
end;

{ ********* Calcular Diferencias en ISPT ********** }

procedure TdmServerAnualNomina.ISPTAnualBuildDataset;
var
   iYear: Integer;
   sIngresoBruto, sIngresoExento, sImpuestoRetenido, sCreditoPagado, sCalculoImpuesto, sEmpleadosACalcular: String;
   lRastrear: Boolean;
   eRepetidos: eEmpleadoRepetido;

   {$ifdef ANTESVERSION2010}
   function GetFechaLimiteBaja: String;
   begin
        { Al validar empleados repetidos tambien debe incluir las bajas en el a�o como parte de su evaluaci�n }
        if ( eRepetidos in [ erIncluirTodos ] ) then
        begin
             Result:= ZetaCommonTools.DateToStrSQL( EncodeDate( iYear, 12, 1 ) );
        end
        else
        begin
             Result:= ZetaCommonTools.DateToStrSQL( FirstDayOfYear( iYear ) );
        end;
   end;

   {$else}
   function GetFechaLimiteBaja: String;
   begin
        { Al validar empleados repetidos tambien debe incluir las bajas en el a�o como parte de su evaluaci�n }
        if ( eRepetidos in [ erIncluirTodos ] ) then
        begin
             Result:= ZetaCommonTools.FechaAsStr( EncodeDate( iYear, 12, 1 ) );
        end
        else
        begin
             Result:= ZetaCommonTools.FechaAsStr( FirstDayOfYear( iYear ) );
        end;
   end;
   {$endif}

begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               iYear := ParamByName( 'Year' ).AsInteger;
               sIngresoBruto := SetVacioACero( ParamByName( 'IngresoBruto' ).AsString );
               sIngresoExento := SetVacioACero( ParamByName( 'IngresoExento' ).AsString );
               sImpuestoRetenido := SetVacioACero( ParamByName( 'ImpuestoRetenido' ).AsString );
               sCreditoPagado := SetVacioACero( ParamByName( 'CreditoPagado' ).AsString );
               sCalculoImpuesto := SetVacioACero( ParamByName( 'CalculoImpuesto' ).AsString );
               sEmpleadosACalcular := SetVacioABooleano( ParamByName( 'EmpleadosACalcular' ).AsString, TRUE );
               lRastrear := ParamByName( 'Rastrear' ).AsBoolean;
               eRepetidos := eEmpleadoRepetido( ParamByName( 'Repetidos' ).AsInteger );
          end;
          YearDefault := iYear;


          with SQLBroker do
          begin
               if lRastrear then
                  oZetaCreator.PreparaRastreo;
               try
                  if lRastrear then
                     SuperReporte.OnEvaluaColumna := SetRastreo;
                  try
                     with Agente do
                     begin
                          {$ifdef ANTESVERSION2010}
                          Init( enEmpleado );
                          {Las dos primeras columnas: INGRESO_BRUTO e INGRESO_EXENTO, se utilizan
                          en las funciones de TZetaISPTAnualMonto y TZetaISPTAnualEXENTO de ZFuncsAnuales,
                          por lo que NO SE PUEDEN mover de POSICION, por que se hace referencia a ellas por
                          POSICION}
                          AgregaColumna( sIngresoBruto, False, Entidad, tgFloat, 10, 'INGRESO_BRUTO' );
                          AgregaColumna( sIngresoExento, False, Entidad, tgFloat, 10, 'INGRESO_EXENTO' );

                          AgregaColumna( sImpuestoRetenido, False, Entidad, tgFloat, 10, 'IMPUESTO_RETENIDO' );
                          AgregaColumna( sCreditoPagado, False, Entidad, tgFloat, 10, 'CREDITO_PAGADO' );
                          AgregaColumna( sCalculoImpuesto, False, Entidad, tgFloat, 10, 'CALCULO_IMPUESTO' );
                          AgregaColumna( 'COLABORA.CB_CODIGO', True, Entidad, tgNumero, 10, 'CB_CODIGO' );
                          AgregaColumna( K_PRETTYNAME, TRUE, enEmpleado, tgTexto, 50, 'PrettyName' );
                          AgregaColumna( 'COLABORA.CB_FEC_ING', True, Entidad, tgFecha, 10, 'CB_FEC_ING' );
                          AgregaColumna( Format( 'FECHA_KARDEX("KARDEX.CB_SALARIO","%s")', [ ZetaCommonTools.FechaAsStr( EncodeDate( iYear, 12, 31 ) ) ] ), False, Entidad, tgFloat, 10, 'CB_SALARIO' );
                          if lRastrear then
                             AgregaColumna( '@0', FALSE, Entidad, tgMemo, 0, 'RASTREO' );
                          AgregaFiltro( Format( '( CB_FEC_BAJ < CB_FEC_ING ) or ( CB_FEC_BAJ >= ''%s'' )', [ GetFechaLimiteBaja ] ), True, enEmpleado );
                          AgregaColumna( '0', True, enEmpleado, tgNumero, 10, 'EMP_REPETIDO' );
                          AgregaColumna( '0', True, enEmpleado, tgNumero, 10, 'FECHA_REPETIDA' );
                          AgregaColumna( 'COLABORA.CB_FEC_BAJ', True, Entidad, tgFecha, 10, 'CB_FEC_BAJ' );
                          AgregaColumna( 'VALIDA_RFC', FALSE, enNinguno, tgTexto, K_ANCHO_DESCCORTA, 'RFC_VALIDADO' );
                          AgregaOrden( 'COLABORA.CB_CODIGO', True, Entidad );
                          {$else}
                          {Las dos primeras columnas: INGRESO_BRUTO e INGRESO_EXENTO, se utilizan
                          en las funciones de TZetaISPTAnualMonto y TZetaISPTAnualEXENTO de ZFuncsAnuales,
                          por lo que NO SE PUEDEN mover de POSICION, por que se hace referencia a ellas por
                          POSICION}

                          FiltrosAnualesInit;
                          FiltrosAnualesColumnas;
                          {Las dos primeras columnas: INGRESO_BRUTO e INGRESO_EXENTO, se utilizan
                          en las funciones de TZetaISPTAnualMonto y TZetaISPTAnualEXENTO de ZFuncsAnuales,
                          por lo que NO SE PUEDEN mover de POSICION, por que se hace referencia a ellas por
                          POSICION}

                          AgregaColumna( sIngresoBruto, False, Entidad, tgFloat, 10, 'INGRESO_BRUTO' );
                          AgregaColumna( sIngresoExento, False, Entidad, tgFloat, 10, 'INGRESO_EXENTO' );

                          AgregaColumna( sImpuestoRetenido, False, Entidad, tgFloat, 10, 'IMPUESTO_RETENIDO' );
                          AgregaColumna( sCreditoPagado, False, Entidad, tgFloat, 10, 'CREDITO_PAGADO' );
                          AgregaColumna( sCalculoImpuesto, False, Entidad, tgFloat, 10, 'CALCULO_IMPUESTO' );

                          {CV: Se manda llamar aqui para no desfasar los datos}
                          //FiltrosAnualesColumnas;
                          AgregaColumna( K_PRETTYNAME, TRUE, enEmpleado, tgTexto, 50, 'PrettyName' );


                          AgregaColumna( 'COLABORA.CB_FEC_ING', True, enEmpleado, tgFecha, 10, 'CB_FEC_ING' );
                          AgregaColumna( Format( 'FECHA_KARDEX("KARDEX.CB_SALARIO","%s")', [ ZetaCommonTools.FechaAsStr( EncodeDate( iYear, 12, 31 ) ) ] ), False, Entidad, tgFloat, 10, 'CB_SALARIO' );
                          if lRastrear then
                             AgregaColumna( '@0', FALSE, Entidad, tgMemo, 0, 'RASTREO' );

                          AgregaColumna( '0', True, enEmpleado, tgNumero, 10, 'EMP_REPETIDO' );
                          AgregaColumna( '0', True, enEmpleado, tgNumero, 10, 'FECHA_REPETIDA' );
                          AgregaColumna( 'COLABORA.CB_FEC_BAJ', True, enEmpleado, tgFecha, 10, 'CB_FEC_BAJ' );
                          AgregaColumna( 'VALIDA_RFC', FALSE, enNinguno, tgTexto, K_ANCHO_DESCCORTA, 'RFC_VALIDADO' );
                          AgregaColumna( sEmpleadosACalcular, False, Entidad, tgBooleano, 10, 'EMPLEADO_CALCULADO' );
                          {Para version 2010, se quita el filtro de antiguedad. Al empleado si se le agrega su registro, pero queda en ceros si no estuvo activo}
                          //AgregaFiltro( Format( '( CB_FEC_BAJ < CB_FEC_ING ) or ( CB_FEC_BAJ >= ''%s'' )', [ GetFechaLimiteBaja ] ), True, enEmpleado );
                          //AgregaColumna( Format( '( CB_FEC_BAJ < CB_FEC_ING ) or ( CB_FEC_BAJ >= ''%s'' )', [ GetFechaLimiteBaja ] ), True, enEmpleado, tgBooleano,10, 'EMPLEADO_ACTIVO' );
                          AgregaColumna( Format( '@CASE WHEN ( COLABORA.CB_FEC_BAJ < COLABORA.CB_FEC_ING ) or ( CB_FEC_BAJ >= ''%s'' ) THEN ''S'' ELSE ''N'' END', [ GetFechaLimiteBaja ] ), FALSE, enEmpleado, tgTexto, 1, 'EMPLEADO_ACTIVO' );
                          AgregaFiltro( 'COLABORA.CB_CODIGO is NOT NULL', TRUE, Entidad );
                          {$endif}
                     end;
                     AgregaRangoCondicion( ParamList );
                     FiltroConfidencial( EmpresaActiva );
                     oZetaCreator.SuperReporte := SQLBroker.SuperReporte;
                     BuildDataset( EmpresaActiva );
                     {$Ifdef CAROLINA}
                     Agente.SQL.SaveTofile('d:\temp\ispt.sql');
                     {$endif}
                  finally
                         if lRastrear then
                            SuperReporte.OnEvaluaColumna := nil;
                  end;
               finally
                      if lRastrear then
                         oZetaCreator.DesPreparaRastreo;
               end;
          end;
     end;
end;

function TdmServerAnualNomina.ISPTAnualDataset( Dataset: TDataset ): OleVariant;
const
     K_2003 = 2003;
     K_POS_PRIMER_CAMPO_A_SUMAR = 1;
     K_POS_ULTIMO_CAMPO_A_SUMAR = 5;
var
   dInicial, dIngreso: TDate;
   iEmpleado: TNumEmp;
   iYear: Integer;
   rSalario, rIngBruto, rIngExento, rGravado, rImpRetenido, rCredPagado, rCalcImpto, rAFavor, rAPagar, rEnContra, rCredAnual, rISRCargo: TPesos;
   FISPTAnual: TZetaCursor;
   lRastrear: Boolean;
   sRastro, sMensaje: String;
   lReformaFiscal2008, lCalcularISPT, lEmpleadoActivo: Boolean;
   eRepetidos: eEmpleadoRepetido;
   aCampos:  array[ K_POS_PRIMER_CAMPO_A_SUMAR..K_POS_ULTIMO_CAMPO_A_SUMAR ] of TPesos;
   ImpuestoEval : TZetaEvaluador;
   FRes : TQREvResult;


 procedure CalculaImpuestoEmpleadosRepetidos;
 begin
      ImpuestoEval := TZetaEvaluador.Create( oZetaCreator );
      ImpuestoEval.AgregaDataset( TdmSuperReporte( oZetaCreator.SuperReporte ).QueryReporte );
 end;

 function PosicionaDatasetEval : Boolean;
 begin
      with TdmSuperReporte( oZetaCreator.SuperReporte ).QueryReporte do
      begin
           {$ifdef DOS_CAPAS}
           First; //Se hace el First, porque no hay seguridad de que el apuntador se queda en donde lo deje
           while not EOF and ( FieldByName('CB_CODIGO').AsInteger <> iEmpleado ) do
                 Next;
           Result := FieldByName('CB_CODIGO').AsInteger = iEmpleado

           {$else}
           Result := Locate('CB_CODIGO', iEmpleado, [] ) ;
           {$endif}
      end;
 end;

begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               iYear := ParamByName( 'Year' ).AsInteger;
               dInicial := ZetaCommonTools.FirstDayOfYear( iYear );
               lRastrear := ParamByName( 'Rastrear' ).AsBoolean;
               eRepetidos := eEmpleadoRepetido( ParamByName( 'Repetidos' ).AsInteger );
          end;
          YearDefault := iYear;
          lReformaFiscal2008 := ( iYear >= ZetaCommonClasses.K_REFORMA_FISCAL_2008 );
          if OpenProcess( prNOISPTAnual, Dataset.RecordCount, FListaParametros, FListaFormulas ) then
          begin
               FOffsetCampos := 1; //Es el offset, para que no tome en cuenta el primer campo

               ProcesoAnualValidaRepetidos( TServerDataSet( Dataset ), eRepetidos, aCampos, EncodeDate( iYear, 12, 1 ), TRUE );
               try
                  FISPTAnual := CreateQuery( GetSQLScript( Q_ISPT_ANUAL_BORRAR ) );
                  ImpuestoEval := Nil;
                  try
                     EmpiezaTransaccion;
                     try
                        ParamAsInteger( FISPTAnual, 'CP_YEAR', iYear );
                        Ejecuta( FISPTAnual );
                        TerminaTransaccion( True );
                     except
                           on Error: Exception do
                           begin
                                TerminaTransaccion( False );
                                raise;
                           end;
                     end;
                     PreparaQuery( FISPTAnual, GetSQLScript( Q_ISPT_ANUAL_INSERT ) );  { Agrega El Comparativo De Un Empleado }
                     ParamAsInteger( FISPTAnual, 'CP_YEAR', iYear );
                     ParamAsInteger( FISPTAnual, 'US_CODIGO', UsuarioActivo );
                     if lRastrear then
                        oZetaCreator.PreparaRastreo;
                     if (eRepetidos in [erSumarUltimo, erSumarPrimero] ) then
                        CalculaImpuestoEmpleadosRepetidos;
                     try
                        with Dataset do
                        begin
                             First;
                             while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                             begin
                                  iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                                  rSalario := FieldByName( 'CB_SALARIO' ).AsFloat;
                                  dIngreso := FieldByName( 'CB_FEC_ING' ).AsDateTime;
                                  {$ifdef ANTESVERSION2010}
                                  lCalcularISPT := TRUE;
                                  lEmpleadoActivo := TRUE;
                                  {$else}
                                  lCalcularISPT := FieldByName( 'EMPLEADO_CALCULADO' ).AsString = K_BOOLEANO_SI;
                                  lEmpleadoActivo := FieldByName( 'EMPLEADO_ACTIVO' ).AsString = K_GLOBAL_SI;
                                  {$endif}
                                  EmpiezaTransaccion;
                                  try
                                     rIngBruto := ZetaCommonTools.Redondea( FieldByName( 'INGRESO_BRUTO' ).AsFloat );
                                     rIngExento := ZetaCommonTools.Redondea( FieldByName( 'INGRESO_EXENTO' ).AsFloat );
                                     rGravado := rIngBruto - rIngExento;
                                     rImpRetenido := ZetaCommonTools.Redondea( FieldByName( 'IMPUESTO_RETENIDO' ).AsFloat );
                                     rCredPagado := ( -1 * ZetaCommonTools.Redondea( FieldByName( 'CREDITO_PAGADO' ).AsFloat ) ); { Debe ser Negativo }

                                     //Aqui se Calcula el Impuesto sobre el Monto SUMADO, cuando se repiten los empleados.
                                     if (eRepetidos in [erSumarUltimo, erSumarPrimero] ) and
                                        ( FieldByName('EMP_REPETIDO').AsInteger <> 0 ) then
                                     begin
                                          rCalcImpto := 0;
                                          if PosicionaDataSetEval then
                                          begin
                                               if ImpuestoEval.Rastreando then
                                                  ImpuestoEval.Rastreador.RastreoBegin; //CV: Si no se limpia se queda con el del empleado anterior y lo va acumulando.
                                               FRes := ImpuestoEval.Calculate( SetVacioACero( ParamList.ParamByName( 'CalculoImpuesto' ).AsString ) );

                                               with FRes do
                                               begin
                                                    if ( Kind = resDouble ) then
                                                         rCalcImpto := ZetaCommonTools.Redondea( dblResult )
                                                    else if ( Kind = resInt ) then
                                                         rCalcImpto := intResult
                                                    else if ( Kind = resError ) then
                                                         Log.Error( iEmpleado, 'Error en la f�rmula del Impuesto' ,FRes.StrResult );
                                               end;
                                               if ImpuestoEval.Rastreando then
                                                  sRastro := ImpuestoEval.Rastreador.Rastro;
                                          end
                                          else
                                          begin
                                               sRastro := VACIO;
                                               Log.Error( iEmpleado, 'Error al Calcular el Impuesto' , 'El Empleado no se encontr� en la Lista Original' );
                                          end;
                                     end
                                     else
                                     begin
                                          rCalcImpto := ZetaCommonTools.Redondea( FieldByName( 'CALCULO_IMPUESTO' ).AsFloat );
                                          if lRastrear then
                                             sRastro := FieldByName( 'RASTREO' ).AsString;
                                     end;

                                     if lRastrear then
                                     begin
                                          with Rastreador do
                                          begin
                                               RastreoBegin;
                                               RastreoHeader( 'RASTREO DEL COMPARATIVO DE ISPT' );
                                               RastreoTextoLibre( PadC( Format( 'Empleado : %d = %s', [ iEmpleado, FieldByName( 'PrettyName' ).AsString ] ), 70 ) );
                                               if ZetaCommonTools.StrLleno( sRastro ) then
                                               begin
                                                    RastreoHeader( 'FORMULAS:' );
                                                    AgregaTexto( sRastro );
                                                    RastreoEspacio;
                                               end;
                                               RastreoHeader('MONTOS CONSIDERADOS' );
                                               RastreoFecha( '                      Primer D�a del A�o: ', dInicial );
                                               RastreoFecha( '                        Fecha de Ingreso: ', dIngreso );
                                               RastreoPesos( '                          Salario Diario: ', rSalario );
                                               RastreoEspacio;
                                               RastreoPesos( '                   Total de Percepciones: ', rIngBruto );
                                               RastreoPesos( '                          Ingreso Exento: ', rIngExento );
                                               RastreoPesos( '                   Ingreso Anual Gravado: ', rGravado );
                                               RastreoPesos( '                 Impuesto Anual Retenido: ', rImpRetenido );
                                               RastreoPesos( Format( '                  %s Anual Aplicado: ', [ aCreditoSalarioLower[lReformaFiscal2008] ] ), rCredPagado );
                                               if ( iYear >= K_2003 ) then
                                               begin
                                                    RastreoPesos( '                     Excedente ISR Anual: ', rCalcImpto );
                                               end;
                                               RastreoEspacio;
                                               if ( iYear < K_2003 ) then
                                                  RastreoHeader( 'APLICACION DE ARTICULO 81 LISR' )
                                               else
                                                   RastreoHeader( 'DE ACUERDO A FRACCIONES II y III DEL ARTICULO 116' );
                                          end;
                                     end;
                                     rAFavor := 0;
                                     rAPagar := 0;
                                     rEnContra := 0;
                                     if ( iYear < K_2003 ) then
                                     begin
                                          if ( rCalcImpto < 0 ) then
                                          begin
                                               rCredAnual := Abs( rCalcImpto );
                                               if lRastrear then
                                               begin
                                                    with Rastreador do
                                                    begin
                                                         RastreoMsg( 'FRACCION II' );
                                                         RastreoPesos( '        Cr�dito Anual Mayor Que Impuesto: ', rCredAnual );
                                                    end;
                                               end;
                                               if ( rCredAnual > rCredPagado ) then  { INCISO A }
                                               begin
                                                    rAPagar := rCredAnual - rCredPagado;
                                                    if lRastrear then
                                                    begin
                                                         with Rastreador do
                                                         begin
                                                              RastreoMsg( 'INCISO A:' );
                                                              RastreoPesos( 'Cr�dito Anual Mayor Que Cr�dito Recibido: ', rCredPagado );
                                                              RastreoPesos( '                  Entregar La Diferencia: ', rAPagar );
                                                         end;
                                                    end;
                                               end
                                               else                                  { INCISO B }
                                               begin
                                                    rEnContra := rCredPagado - rCredAnual;
                                                    if lRastrear then
                                                    begin
                                                         with Rastreador do
                                                         begin
                                                              RastreoMsg( 'INCISO B:' );
                                                              RastreoPesos( 'Cr�dito Anual Menor Que Cr�dito Recibido: ', rCredPagado );
                                                              RastreoPesos( '                   A Cargo La Diferencia: ', rEnContra );
                                                         end;
                                                    end;
                                               end;
                                          end
                                          else  { FRACCION III y IV }
                                          begin
                                               rEnContra := rCalcImpto + rCredPagado;
                                               if lRastrear then
                                               begin
                                                    with Rastreador do
                                                    begin
                                                         RastreoMsg( 'FRACCIONES III y IV:' );
                                                         RastreoPesos( '  Impuesto Mayor O Igual A Cr�dito Anual: ', rCalcImpto );
                                                         RastreoPesos( '(+)                     Cr�dito Recibido: ', rCredPagado );
                                                         RastreoPesos( '(=)                     Impuesto a Cargo: ', rEnContra );
                                                    end;
                                               end;
                                          end;
                                          if ( rEnContra >= 0 ) then { FRACCION V }
                                          begin
                                               if lRastrear then
                                               begin
                                                    with Rastreador do
                                                    begin
                                                         RastreoMsg( 'FRACCION V:' );
                                                         RastreoPesos( '                        Impuesto A Cargo: ', rEnContra );
                                                         RastreoPesos( '(-)                       Retenido Anual: ', rImpRetenido );
                                                    end;
                                               end;
                                               rAFavor := ZetaCommonTools.rMax( ( rImpRetenido - rEnContra ), 0 );
                                               rEnContra := ZetaCommonTools.rMax( ( rEnContra - rImpRetenido ), 0 );
                                               if lRastrear then
                                               begin
                                                    with Rastreador do
                                                    begin
                                                         if ( rAFavor > 0 ) then
                                                            RastreoPesos( '(=)                     Impuesto A Favor: ', rAFavor )
                                                         else
                                                             RastreoPesos( '(=)                     Impuesto A Cargo: ', rEnContra );
                                                    end;
                                               end;
                                          end;
                                          if ( dIngreso > dInicial ) then { FRACCION VI }
                                          begin
                                               rAPagar := 0;
                                               if lRastrear then
                                               begin
                                                    with Rastreador do
                                                    begin
                                                         RastreoMsg( 'FRACCION VI: Servicio Menor Que 12 Meses' );
                                                         RastreoPesos( Format( '           Cr�dito Al Salario A Entregar: ', [] ), 0 );
                                                    end;
                                               end;
                                          end;
                                     end
                                     else {iYear >= K_2003 }
                                     begin
                                          { Cambios a las reglas de subsidio en el 2003 }
                                          { Nunca se va a tener rAPagar > 0 ( Art�culo 116, Fraccion III }
                                          { Topar ISR a Cargo a 0 � a la diferencia entre Impuesto Calculado y CAS}
                                          { NOTA: Se suma dado que rCredPagado es negativo (ver c�digo arriba) }
                                          rISRCargo := ZetaCommonTools.rMax( 0, ( rCalcImpto - rCredPagado ) );
                                          if lRastrear then
                                          begin
                                               with Rastreador do
                                               begin
                                                    RastreoPesos( '                     Excedente ISR Anual: ', rCalcImpto );
                                                    RastreoPesos( Format( '(-)                     %s Aplicado: ', [ aCreditoSalarioLower[lReformaFiscal2008] ] ), rCredPagado );
                                                    if ( rCalcImpto < rCredPagado ) then
                                                    begin
                                                         RastreoMsg( 'ARTICULO 116, FRACCION III:' );
                                                    end;
                                                    RastreoPesos( '(=)                     Impuesto a Cargo: ', rISRCargo );
                                                    RastreoPesos( '(-)        Retenciones ISR Provisionales: ', rImpRetenido );
                                               end;
                                          end;
                                          if ( rISRCargo > rImpRetenido ) then
                                          begin
                                               rEnContra := ZetaCommonTools.Redondea( rISRCargo - rImpRetenido );
                                               if lRastrear then
                                               begin
                                                    with Rastreador do
                                                    begin
                                                         RastreoPesos( '(=)                          ISR A Cargo: ', rEnContra );
                                                    end;
                                               end;
                                          end
                                          else
                                          begin
                                               rAFavor := ZetaCommonTools.Redondea( rImpRetenido - rISRCargo );
                                               if lRastrear then
                                               begin
                                                    with Rastreador do
                                                    begin
                                                         RastreoPesos( '(=)                          ISR A Favor: ', rAFavor );
                                                    end;
                                               end;
                                          end;
                                     end;
                                     //cv: En este momento ya se calcularon todos los montos.
                                     //A los empleados que no les corresponde calculo, se hace ceros los siguientes montos
                                     if NOT( lCalcularISPT and lEmpleadoActivo ) then
                                     begin
                                          //rIngBruto :=0;
                                          //rGravado :=0;
                                          //rCredPagado :=0;
                                          //rImpRetenido :=0;
                                          rCalcImpto :=0;
                                          rAFavor :=0;
                                          rEnContra :=0;
                                          //rAPagar :=0;
                                     end;

                                     ParamAsInteger( FISPTAnual, 'CB_CODIGO', iEmpleado );
                                     ParamAsFloat( FISPTAnual, 'CB_SALARIO', rSalario );
                                     ParamAsFloat( FISPTAnual, 'CP_TOT_PER', rIngBruto );
                                     ParamAsFloat( FISPTAnual, 'CP_GRAVADO', rGravado );
                                     ParamAsFloat( FISPTAnual, 'CP_PAGADO', rCredPagado );
                                     ParamAsFloat( FISPTAnual, 'CP_RETENID', rImpRetenido );
                                     ParamAsFloat( FISPTAnual, 'CP_ART_141', rCalcImpto );
                                     ParamAsFloat( FISPTAnual, 'CP_FAVOR', rAFavor  );
                                     ParamAsFloat( FISPTAnual, 'CP_CONTRA', rEnContra );
                                     ParamAsFloat( FISPTAnual, 'CP_A_PAGAR', rAPagar );
                                     ParamAsString( FISPTAnual, 'CP_CALCULO', zBoolToStr( lCalcularISPT and lEmpleadoActivo) );
                                     Ejecuta( FISPTAnual );
                                     TerminaTransaccion( True );
                                  except
                                        on Error: Exception do
                                        begin
                                             TerminaTransaccion( False );
                                             Log.Excepcion( iEmpleado, 'Error Al Calcular ISPT Anual ', Error, DescripcionParams );
                                        end;
                                  end;

                                  if lCalcularISPT and lEmpleadoActivo then
                                  begin
                                       if lRastrear then
                                       begin
                                            sMensaje := 'Rastreo del Comparativo de ISPT Para %d';
                                            sRastro := Rastreador.Rastro;

                                            if ZetaCommonTools.StrLleno( sRastro ) then
                                               Log.Evento( clbNinguno, iEmpleado, Date, Format( sMensaje, [ iYear ] ), sRastro );
                                       end
                                  end
                                  else
                                  begin
                                       sMensaje := Format( 'Comparativo de ISPT Para %d', [ iYear ] );
                                       if NOT lCalcularISPT then
                                          Log.Advertencia( clbNinguno, iEmpleado, Date, sMensaje,
                                          'El empleado no tiene c�lculo debido a que no cumple con el filtro de "Empleados a Calcular"' )
                                       else
                                          Log.Advertencia( clbNinguno, iEmpleado, Date, sMensaje,
                                          'El empleado no tiene c�lculo debido a que no tiene la antig�edad suficiente' );
                                  end;

                                  Next;
                             end;
                        end;
                     finally
                            if lRastrear then
                               oZetaCreator.DesPreparaRastreo;
                     end;
                  finally
                         FreeAndNil( ImpuestoEval );
                         FreeAndNil( FISPTAnual );
                  end;
               except
                     on Error: Exception do
                     begin
                          Log.Excepcion( 0, 'Error Al Calcular ISPT Anual', Error, DescripcionParams );
                     end;
               end;
          end;
          Result := CloseProcess;
     end;
end;

procedure TdmServerAnualNomina.ISPTAnualParametros;
{
const
     K_PORCENTAJE = '%';
}
begin
     with oZetaProvider, ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'A�o: ' + ParamByName( 'Year' ).AsString +
{'Subsidio: %3.5n %s'}   K_PIPE + Format( 'Proporci�n Del Subsidio: %1.4n', [ ParamByName( 'Subsidio' ).AsFloat ] ) +//[ ParamByName( 'Subsidio' ).AsFloat, K_PORCENTAJE ] ) +
                         K_PIPE + 'Rastrear: ' + ZetacommonTools.zBoolToStr( ParamByName( 'Rastrear' ).AsBoolean ) +
                         K_PIPE + 'Empleados Repetidos: ' + ZetaCommonLists.ObtieneElemento( lfEmpleadoRepetido, ParamByName( 'Repetidos' ).AsInteger );
          FListaFormulas :=    VACIO;
          FListaFormulas :=    K_PIPE + 'F�rmulas' +
                     K_PIPE + 'Ingreso Bruto: ' + ParamByName( 'IngresoBruto' ).AsString +
                     K_PIPE + 'Ingreso Exento: ' + ParamByName( 'IngresoExento' ).AsString +
                     K_PIPE + Format( '%s Pagado: ', [ aCreditoSalarioLower[ParamByName( 'Year' ).AsInteger >= K_REFORMA_FISCAL_2008] ] ) + ParamByName( 'CreditoPagado' ).AsString +
                     K_PIPE + 'Impuesto Retenido: ' + ParamByName( 'ImpuestoRetenido' ).AsString +
                     K_PIPE + 'C�lculo Impuesto: ' + ParamByName( 'CalculoImpuesto' ).AsString+
                     K_PIPE + 'Empleado a Calcular: ' + ParamByName( 'EmpleadosACalcular' ).AsString;
     end;
end;

function TdmServerAnualNomina.ISPTAnual(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitBroker( True );
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList( Parametros );
             GetDatosPeriodo;
             GetDatosImss;
             GetDatosActivos;
        end;
        ISPTAnualParametros;
        ISPTAnualBuildDataset;
        Result := ISPTAnualDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
            ClearBroker;
     end;
     SetComplete;
end;

{ ********* Declaraci�n de Cr�dito Al Salario ********* }

procedure TdmServerAnualNomina.DeclaraCreditoBuildDataset;
var
   sIngresoBruto, sIngresoExento, sCreditoPagado: String;
begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               sIngresoBruto := SetVacioACero( ParamByName( 'IngresoBruto' ).AsString );
               sIngresoExento := SetVacioACero( ParamByName( 'IngresoExento' ).AsString );
               sCreditoPagado := SetVacioACero( ParamByName( 'CreditoPagado' ).AsString );
          end;
          with SQLBroker do
          begin
               Init( enEmpleado );
               with Agente do
               begin
                    AgregaColumna( 'COLABORA.CB_CODIGO', True, Entidad, tgNumero, 10, 'CB_CODIGO' );
                    AgregaColumna( sIngresoBruto, False, Entidad, tgFloat, 10, 'INGRESO_BRUTO' );
                    AgregaColumna( sIngresoExento, False, Entidad, tgFloat, 10, 'INGRESO_EXENTO' );
                    AgregaColumna( sCreditoPagado, False, Entidad, tgFloat, 10, 'CREDITO_PAGADO' );

                    AgregaOrden( 'COLABORA.CB_CODIGO', True, Entidad );
               end;
               AgregaRangoCondicion( ParamList );
               FiltroConfidencial( EmpresaActiva );
               BuildDataset( EmpresaActiva );
          end;
     end;
end;

function TdmServerAnualNomina.DeclaraCreditoDataset( DataSet: TDataset ): OleVariant;
var
   iEmpleado, iYear: Integer;
   rIngBruto, rIngExento, rCredPagado: TPesos;
   FCredito: TZetaCursor;
   lReformaFiscal2008: Boolean;
begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               iYear := ParamByName( 'Year' ).AsInteger;
          end;
          YearDefault := iYear;
          lReformaFiscal2008 := ( iYear >= ZetaCommonClasses.K_REFORMA_FISCAL_2008 );
          if OpenProcess( prNOCreditoSalario, Dataset.RecordCount, FListaParametros, FListaFormulas ) then
          begin
               try
                  EmpiezaTransaccion;
                  try
                     //CV: CreateQuery--ok
                     //Variable local
                     FCredito := CreateQuery( GetSQLScript( Q_CREDITO_BORRAR ) );
                     ParamAsInteger( FCredito, 'DC_YEAR', iYear );
                     Ejecuta( FCredito );
                     TerminaTransaccion( True );
                  except
                        on Error: Exception do
                        begin
                             TerminaTransaccion( False );
                             raise;
                        end;
                  end;
                  PreparaQuery( FCredito, GetSQLScript( Q_CREDITO_INSERT ) );  { Agrega El Comparativo De Un Empleado }
                  ParamAsInteger( FCredito, 'DC_YEAR', iYear );
                  with Dataset do
                  begin
                       while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                       begin
                            iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                            EmpiezaTransaccion;
                            try
                               rIngBruto := ZetaCommonTools.Redondea( FieldByName( 'INGRESO_BRUTO' ).AsFloat );
                               rIngExento := ZetaCommonTools.Redondea( FieldByName( 'INGRESO_EXENTO' ).AsFloat );
                               rCredPagado := ( -1 * ZetaCommonTools.Redondea( FieldByName( 'CREDITO_PAGADO' ).AsFloat ) );
                               ParamAsInteger( FCredito, 'CB_CODIGO', iEmpleado );
                               ParamAsFloat( FCredito, 'DC_TOT_PER', rIngBruto );
                               ParamAsFloat( FCredito, 'DC_GRAVADO', ( rIngBruto - rIngExento ) );
                               ParamAsFloat( FCredito, 'DC_CREDITO', rCredPagado );
                               Ejecuta( FCredito );
                               TerminaTransaccion( True );
                            except
                                  on Error: Exception do
                                  begin
                                       TerminaTransaccion( False );
                                       if lReformaFiscal2008 then
                                          Log.Excepcion( iEmpleado, 'Error Al Calcular Subsidio Al Empleo', Error, DescripcionParams )
                                       else
                                           Log.Excepcion( iEmpleado, 'Error Al Calcular Cr�dito Al Salario', Error, DescripcionParams );
                                  end;
                            end;
                            Next;
                       end;
                  end;
               except
                     on Error: Exception do
                     begin
                          if lReformaFiscal2008 then
                             Log.Excepcion( 0, 'Error Al Calcular Subsidio Al Empleo', Error, DescripcionParams )
                          else
                              Log.Excepcion( 0, 'Error Al Calcular Cr�dito Al Salario', Error, DescripcionParams );
                     end;
               end;
               FreeAndNil(FCredito);
          end;
          Result := CloseProcess;
     end;
end;

procedure TdmServerAnualNomina.DeclaraCreditoParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'A�o: ' + ParamByName( 'Year' ).AsString;
          FListaFormulas :=    VACIO;
          FListaFormulas :=    K_PIPE + 'F�rmulas' +
                     K_PIPE + 'Ingreso Bruto: ' + ParamByName( 'IngresoBruto' ).AsString +
                     K_PIPE + 'Ingreso Exento: ' + ParamByName( 'IngresoExento' ).AsString +
                     K_PIPE + Format( 'Cr�dito Pagado: ', [ aCreditoSalarioLower[ParamByName( 'Year' ).AsInteger >= K_REFORMA_FISCAL_2008] ] ) + ParamByName( 'CreditoPagado' ).AsString;
     end;
end;

function TdmServerAnualNomina.DeclaraCredito(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitBroker( True );
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList(Parametros);
             GetDatosPeriodo;
             GetDatosImss;
             GetDatosActivos;
             YearDefault := ParamList.ParamByName( 'Year' ).AsInteger;  // Para Evaluar Funci�n A()
        end;
        DeclaraCreditoParametros;
        DeclaraCreditoBuildDataset;
        Result := DeclaraCreditoDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
            ClearBroker;
     end;
     SetComplete;
end;

{ *********** Pagar Aguinaldos ********** }

procedure TdmServerAnualNomina.PagarAguinaldoBuildDataset;
begin
     with oZetaProvider do
     begin
          with SQLBroker do
          begin
               Init( enAguinaldo );
               with Agente do
               begin
                    AgregaColumna( 'CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
                    AgregaColumna( 'AG_DIAS_AG', True, Entidad, tgFloat, 0, 'AG_DIAS_AG' );
                    AgregaColumna( K_PRETTYNAME, TRUE, enEmpleado, tgTexto, 50, 'PrettyName' );

                    AgregaFiltro( Format( 'AG_YEAR = %d', [ ParamList.ParamByName( 'YearAguinaldo' ).AsInteger ] ), True, Entidad );
                    {Cambios sug 460: Multiples Tipos de N�mina}
                    AgregaFiltro( Format( 'CB_NOMINA = %d', [ Ord( DatosPeriodo.Tipo ) ] ), True, enEmpleado );

                    AgregaOrden( 'CB_CODIGO', True, Entidad );
               end;
               AgregaRangoCondicion( ParamList );
               FiltroConfidencial( EmpresaActiva );
               BuildDataset( EmpresaActiva );
          end;
     end;
end;

function TdmServerAnualNomina.PagarAguinaldoDataset( Dataset: TDataset ): OleVariant;
var
   iEmpleado: TNumEmp;
   rDias: TDiasHoras;
begin
     with oZetaProvider do
     begin
          InitGlobales;
          if OpenProcess( prNOPagoAguinaldo, Dataset.RecordCount, FListaParametros ) then
          begin
               try
                  try
                     InitNomina;
                     with Nomina do
                     begin
                          try
                             VerificaNominaBegin;
                             PreparaDiasHorasBegin;
                             ExcepcionDiaHoraBegin;
                             with Dataset do
                             begin
                                  while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                                  begin
                                       iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                                       rDias := FieldByName( 'AG_DIAS_AG' ).AsFloat;
                                       EmpiezaTransaccion;
                                       try
                                          { Verificar si Nomina Existe }
                                          VerificaRegistro( iEmpleado );
                                          ExcepcionDiaHora( iEmpleado, NullDateTime, K_TIPO_DIA, Ord( mfdAguinaldo ), rDias, omSustituir );
                                          PreparaDiasHoras( iEmpleado );
                                          TerminaTransaccion( True );
                                       except
                                             on Error: Exception do
                                             begin
                                                  //TerminaTransaccion( False );
                                                  RollBackTransaccion;
                                                  Log.Excepcion( iEmpleado, 'Error Al Pagar Aguinaldo ', Error, DescripcionParams );
                                             end;
                                       end;
                                       Next;
                                  end;
                             end;
                          finally
                                 ExcepcionDiaHoraEnd;
                                 PreparaDiasHorasEnd;
                                 VerificaNominaEnd;
                          end;
                     end;
                  finally
                         ClearNomina;
                  end;
               except
                     on Error: Exception do
                     begin
                          Log.Excepcion( 0, 'Error Al Pagar Aguinaldo', Error, DescripcionParams );
                     end;
               end;
          end;
          Result := CloseProcess;
     end;
end;

procedure TdmServerAnualNomina.PagoAguinaldoParametros;
begin
     with oZetaProvider, DatosPeriodo do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'A�o: ' + ParamList.ParamByName( 'YearAguinaldo' ).AsString +
                         K_PIPE + 'Periodo: ' + ZetacommonTools.ShowNomina( Year, Ord( Tipo ), Numero );
     end;
end;

function TdmServerAnualNomina.PagarAguinaldoGetLista(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          GetDatosPeriodo;
     end;
     InitBroker( False );
     try
        PagarAguinaldoBuildDataset;
        Result := SQLBroker.SuperReporte.GetReporte;
     finally
            ClearBroker;
     end;
     SetComplete;
end;

function TdmServerAnualNomina.PagarAguinaldoLista(Empresa, Lista, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          GetDatosPeriodo;
          InitArregloTPeriodo;
     end;

     PagoAguinaldoParametros;
     cdsLista.Lista := Lista;
     Result := PagarAguinaldoDataset( cdsLista );
     SetComplete;
end;

function TdmServerAnualNomina.PagarAguinaldo(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          GetDatosPeriodo;
          InitArregloTPeriodo;
     end;
     PagoAguinaldoParametros;
     InitBroker( False );
     try
        PagarAguinaldoBuildDataset;
        Result := PagarAguinaldoDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
            ClearBroker;
     end;
     SetComplete;
end;

{ ***** Pagar PTU ************* }

procedure TdmServerAnualNomina.PTUPagoBuildDataset;
begin
     with oZetaProvider do
     begin
          with SQLBroker do
          begin
               Init( enRepartoPTU );
               with Agente do
               begin
                    AgregaColumna( 'CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
                    AgregaColumna( 'RU_M_DIAS', True, Entidad, tgFloat, 0, 'RU_M_DIAS' );
                    AgregaColumna( 'RU_M_MONTO', True, Entidad, tgFloat, 0, 'RU_M_MONTO' );
                    AgregaColumna( K_PRETTYNAME, TRUE, enEmpleado, tgTexto, 50, 'PrettyName' );

                    AgregaFiltro( Format( 'RU_YEAR = %d', [ ParamList.ParamByName( 'YearPTU' ).AsInteger ] ), True, Entidad );
                    AgregaFiltro( '( ( RU_M_DIAS > 0 ) or ( RU_M_MONTO > 0 ) )', True, Entidad );
                    {Cambios sug 460: Multiples Tipos de N�mina}
                    AgregaFiltro( Format( 'CB_NOMINA = %d', [ Ord( DatosPeriodo.Tipo ) ] ), True, enEmpleado );

                    AgregaOrden( 'CB_CODIGO', True, Entidad );
               end;
               AgregaRangoCondicion( ParamList );
               FiltroConfidencial( EmpresaActiva );
               BuildDataset( EmpresaActiva );
          end;
     end;
end;

function TdmServerAnualNomina.PTUPagoDataset(Dataset: TDataset): OleVariant;
var
   iEmpleado: TNumEmp;
   iConcepto: Integer;
   rMonto: TPesos;
   lPercepcion: Boolean;
begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               iConcepto := ParamByName( 'Concepto' ).AsInteger;
          end;
          InitGlobales;
          if OpenProcess( prNOPagoPTU, Dataset.RecordCount, FListaParametros ) then
          begin
               try
                  lPercepcion := EsPercepcion( iConcepto );
                  try
                     InitNomina;
                     with Nomina do
                     begin
                          try
                             VerificaNominaBegin;
                             ExcepcionMontoBegin;
                             with Dataset do
                             begin
                                  while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                                  begin
                                       iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                                       rMonto := ZetaCommonTools.Redondea( FieldByName( 'RU_M_DIAS' ).AsFloat ) +
                                                 ZetaCommonTools.Redondea( FieldByName( 'RU_M_MONTO' ).AsFloat );
                                       EmpiezaTransaccion;
                                       try
                                          VerificaRegistro( iEmpleado );
                                          //ExcepcionMonto( iEmpleado, iConcepto, '', rMonto, omSustituir, True );
                                          ExcepcionMonto( iEmpleado, iConcepto, '', rMonto, omSustituir, lPercepcion );
                                          TerminaTransaccion( True );
                                       except
                                             on Error: Exception do
                                             begin
                                                  //TerminaTransaccion( False );
                                                  RollBackTransaccion;
                                                  Log.Excepcion( iEmpleado, 'Error Al Pagar PTU ', Error, DescripcionParams );
                                             end;
                                       end;
                                       Next;
                                  end;
                             end;
                          finally
                                 ExcepcionMontoEnd;
                                 VerificaNominaEnd;
                          end;
                          CalculaStatusPeriodo;     { 2.6 - Faltaba Calcular Status del Periodo }
                     end;
                  finally
                         ClearNomina;
                  end;
               except
                     on Error: Exception do
                     begin
                          Log.Excepcion( 0, 'Error Al Pagar PTU ', Error, DescripcionParams );
                     end;
               end;
          end;
          Result := CloseProcess;
     end;
end;

procedure TdmServerAnualNomina.PTUPagoParametros;
begin
     with oZetaProvider, DatosPeriodo do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'A�o: ' + ParamList.ParamByName( 'YearPTU' ).AsString +
                         K_PIPE + 'Concepto: ' + ParamList.ParamByName( 'Concepto' ).AsString +
                         K_PIPE + 'Periodo: ' + ZetacommonTools.ShowNomina( Year, Ord( Tipo ), Numero );
     end;
end;

function TdmServerAnualNomina.PTUPagoGetLista(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          GetDatosPeriodo;
     end;
     InitBroker( False );
     try
        PTUPagoBuildDataset;
        Result := SQLBroker.SuperReporte.GetReporte;
     finally
            ClearBroker;
     end;
     SetComplete;
end;

function TdmServerAnualNomina.PTUPagoLista(Empresa, Lista, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          GetDatosPeriodo;
          InitArregloTPeriodo;
     end;
     PTUPagoParametros;
     cdsLista.Lista := Lista;
     Result := PTUPagoDataset( cdsLista );
     SetComplete;
end;

function TdmServerAnualNomina.PTUPago(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          GetDatosPeriodo;
          InitArregloTPeriodo;
     end;
     PTUPagoParametros;
     InitBroker( False );
     try
        PTUPagoBuildDataset;
        Result := PTUPagoDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
            ClearBroker;
     end;
     SetComplete;
end;

{ ***** Pagar Diferencias de ISPT ************* }

procedure TdmServerAnualNomina.ISPTPagoInitProvider;
begin
     with oZetaProvider do
     begin
          GetDatosPeriodo;
          YearDefault := ParamList.ParamByName( 'YearActivo' ).AsInteger;
     end;
end;

procedure TdmServerAnualNomina.ISPTPagoBuildDataset;
begin
     with oZetaProvider do
     begin
          with SQLBroker do
          begin
               Init( enCompara );
               with Agente do
               begin
                    AgregaColumna( 'CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
                    AgregaColumna( 'CP_FAVOR', True, Entidad, tgFloat, 0, 'CP_FAVOR' );
                    AgregaColumna( 'CP_CONTRA', True, Entidad, tgFloat, 0, 'CP_CONTRA' );
                    AgregaColumna( 'CP_A_PAGAR', True, Entidad, tgFloat, 0, 'CP_A_PAGAR' );
                    AgregaColumna( K_PRETTYNAME, TRUE, enEmpleado, tgTexto, 50, 'PrettyName' );

                    AgregaFiltro( Format( 'CP_YEAR = %d', [ YearDefault ] ), True, Entidad );
                    AgregaFiltro( '( ( CP_FAVOR <> 0 ) or ( CP_CONTRA <> 0 ) or ( CP_A_PAGAR <> 0 ) )', True, Entidad );
                    {Cambios sug 460: Multiples Tipos de N�mina}
                    AgregaFiltro( Format( 'CB_NOMINA = %d', [ Ord( DatosPeriodo.Tipo ) ] ), True, enEmpleado );

                    AgregaOrden( 'CB_CODIGO', True, Entidad );
               end;
               AgregaRangoCondicion( ParamList );
               FiltroConfidencial( EmpresaActiva );
               BuildDataset( EmpresaActiva );
          end;
     end;
end;

function TdmServerAnualNomina.ISPTPagoDataset(Dataset: TDataset): OleVariant;
var
   iEmpleado: TNumEmp;
   lEnviaNomina: Boolean;
   iYear: Integer;
   rFavor, rContra, rPagar: TPesos;

procedure PagarANomina;
const
     K_ERROR = 'Error Al Pasar Dif ISPT %d A N�mina';
var
   iAFavor, iACargo, iISPTNeto, iCredito: Integer;
   lAFavor, lACargo, lISPTNeto, lCredito: Boolean;
begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               iAFavor := ParamByName( 'ExcepcionAFavor' ).AsInteger;
               iACargo := ParamByName( 'ExcepcionACargo' ).AsInteger;
               iISPTNeto := ParamByName( 'ExcepcionISPTNeto' ).AsInteger;
               iCredito := ParamByName( 'ExcepcionCredito' ).AsInteger;
          end;
          try
             InitNomina;
             with Nomina do
             begin
                  try
                     lAFavor := EsPercepcion( iAFavor );
                     lACargo := EsPercepcion( iACargo );
                     lISPTNeto := EsPercepcion( iISPTNeto );
                     lCredito := EsPercepcion( iCredito );
                     VerificaNominaBegin;
                     ExcepcionMontoBegin;
                     with Dataset do
                     begin
                          while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                          begin
                               iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                               rFavor := FieldByName( 'CP_FAVOR' ).AsFloat;
                               rContra := FieldByName( 'CP_CONTRA' ).AsFloat;
                               rPagar := FieldByName( 'CP_A_PAGAR' ).AsFloat;
                               EmpiezaTransaccion;
                               try
                                  VerificaRegistro( iEmpleado );
                                  if ( iAFavor > 0 ) and ( rFavor > 0 ) then
                                     ExcepcionMonto( iEmpleado, iAFavor, '', rFavor, omSustituir, lAFavor );
                                     //ExcepcionMonto( iEmpleado, iAFavor, '', rFavor, omSustituir, True );
                                  if ( iACargo > 0 ) and ( rContra > 0 ) then
                                     ExcepcionMonto( iEmpleado, iACargo, '', rContra, omSustituir, lACargo );
                                     //ExcepcionMonto( iEmpleado, iACargo, '', rContra, omSustituir, True );
                                  if ( iCredito > 0 ) and ( rPagar > 0 ) then
                                     ExcepcionMonto( iEmpleado, iCredito, '', rPagar, omSustituir, lCredito );
                                     //ExcepcionMonto( iEmpleado, iCredito, '', rPagar, omSustituir, True );
                                  if ( iISPTNeto > 0 ) and ( ( rFavor > 0 ) or ( rContra > 0 ) ) then
                                  begin
                                       if ( rContra > 0 ) then
                                          ExcepcionMonto( iEmpleado, iISPTNeto, '', rContra, omSustituir, lISPTNeto )
                                          //ExcepcionMonto( iEmpleado, iISPTNeto, '', rContra, omSustituir, True )
                                       else
                                           ExcepcionMonto( iEmpleado, iISPTNeto, '', -1 * rFavor, omSustituir, lISPTNeto );
                                           //ExcepcionMonto( iEmpleado, iISPTNeto, '', -1 * rFavor, omSustituir, True );
                                  end;
                                  TerminaTransaccion( True );
                               except
                                     on Error: Exception do
                                     begin
                                          //TerminaTransaccion( False );
                                          RollBackTransaccion;
                                          Log.Excepcion( iEmpleado, Format( K_ERROR, [ iYear ] ), Error );
                                     end;
                               end;
                               Next;
                          end;//While
                     end;
                  finally
                         ExcepcionMontoEnd;
                         VerificaNominaEnd;
                  end;
                  CalculaStatusPeriodo;     { 2.6 - Faltaba Calcular Status del Periodo }
             end;
          finally
                 ClearNomina;
          end;
     end;
end;

procedure PagarAPrestamo;
const
     K_ERROR = 'Error Al Pasar Dif ISPT %d A Pr�stamo';
var
   sPAFavor, sPACargo, sPISPTNeto, sPCreditoSalario, sPReferencia, sPFormula: String;
   dPInicio: TDate;
   FExiste, FAgrega, FCambia: TZetaCursor;

procedure AgregaPrestamo( const sTipo, sReferencia, sFormula: String; const dInicio: TDate; const rMonto: TPesos );
var
   lExiste: Boolean;
begin
     with oZetaProvider do
     begin
          ParamAsInteger( FExiste, 'Empleado', iEmpleado );
          ParamAsChar( FExiste, 'Tipo', sTipo, K_ANCHO_TIPO );
          ParamAsVarChar( FExiste, 'Referencia', sReferencia, K_ANCHO_REFERENCIA );
          with FExiste do
          begin
               Active := True;
               lExiste := ( Fields[ 0 ].AsInteger > 0 );
               Active := False;
          end;
          if lExiste then
          begin
               ParamAsInteger( FCambia, 'CB_CODIGO', iEmpleado );
               ParamAsChar( FCambia, 'PR_TIPO', sTipo, K_ANCHO_TIPO );
               ParamAsVarChar( FCambia, 'PR_REFEREN', sReferencia, K_ANCHO_REFERENCIA );
               ParamAsVarChar( FCambia, 'PR_FORMULA', sFormula, K_ANCHO_FORMULA );
               ParamAsDate( FCambia, 'PR_FECHA', dInicio );
               ParamAsFloat( FCambia, 'PR_MONTO', rMonto );
               Ejecuta( FCambia );
          end
          else
          begin
               ParamAsInteger( FAgrega, 'CB_CODIGO', iEmpleado );
               ParamAsChar( FAgrega, 'PR_TIPO', sTipo, K_ANCHO_TIPO );
               ParamAsVarChar( FAgrega, 'PR_REFEREN', sReferencia, K_ANCHO_REFERENCIA );
               ParamAsVarChar( FAgrega, 'PR_FORMULA', sFormula, K_ANCHO_FORMULA );
               ParamAsDate( FAgrega, 'PR_FECHA', dInicio );
               ParamAsFloat( FAgrega, 'PR_MONTO', rMonto );
               ParamAsFloat( FAgrega, 'PR_MONTO2', rMonto );
               Ejecuta( FAgrega );
          end;
     end;
end;

begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               sPAFavor := ParamByName( 'PrestamoAFavor' ).AsString;
               sPACargo := ParamByName( 'PrestamoACargo' ).AsString;
               sPISPTNeto := ParamByName( 'PrestamoISPTNeto' ).AsString;
               sPCreditoSalario := ParamByName( 'PrestamoCredito' ).AsString;
               sPReferencia := ParamByName( 'PrestamoReferencia' ).AsString;
               sPFormula := ParamByName( 'PrestamoFormula' ).AsString;
               dPInicio := ParamByName( 'PrestamoInicio' ).AsDate;
          end;
          //CV: CreateQuery--ok
          //Variable local
          FExiste := CreateQuery( GetSQLScript( Q_PRESTAMO_EXISTE ) );
          FAgrega := CreateQuery( GetSQLScript( Q_PRESTAMO_AGREGA ) );
          ParamAsInteger( FAgrega, 'PR_STATUS', Ord( spActivo ) );
          FCambia := CreateQuery( GetSQLScript( Q_PRESTAMO_MODIFICA ) );
          ParamAsInteger( FCambia, 'PR_STATUS', Ord( spActivo ) );
          with Dataset do
          begin
               while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
               begin
                    iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                    rFavor := FieldByName( 'CP_FAVOR' ).AsFloat;
                    rContra := FieldByName( 'CP_CONTRA' ).AsFloat;
                    rPagar := FieldByName( 'CP_A_PAGAR' ).AsFloat;
                    EmpiezaTransaccion;
                    try
                       if StrLleno( sPAFavor ) and ( rFavor > 0 ) then
                          AgregaPrestamo( sPAFavor, sPReferencia, sPFormula, dPInicio, -1 * rFavor );
                       if StrLleno( sPACargo ) and ( rContra > 0 ) then
                          AgregaPrestamo( sPACargo, sPReferencia, sPFormula, dPInicio, rContra );
                       if StrLleno( sPCreditoSalario ) and ( rPagar > 0 ) then
                          AgregaPrestamo( sPCreditoSalario, sPReferencia, sPFormula, dPInicio, -1 * rPagar );
                       if StrLleno( sPISPTNeto ) and ( ( rFavor > 0 ) or ( rContra > 0 ) ) then
                       begin
                            if ( rContra > 0 ) then
                               AgregaPrestamo( sPISPTNeto, sPReferencia, sPFormula, dPInicio, rContra )
                            else
                                AgregaPrestamo( sPISPTNeto, sPReferencia, sPFormula, dPInicio, -1 * rFavor );
                       end;
                       TerminaTransaccion( True );
                    except
                          on Error: Exception do
                          begin
                               //TerminaTransaccion( False );
                               RollBackTransaccion;
                               Log.Excepcion( iEmpleado, Format( K_ERROR, [ iYear ] ), Error );
                          end;
                    end;
                    Next;
               end;//while
          end;
          FreeAndNil(FCambia);
          FreeAndNil(FAgrega);
          FreeAndNil(FExiste);
     end;
end;

begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               iYear := YearDefault;
               lEnviaNomina := ParamByName( 'EnviarNomina' ).AsBoolean;
          end;
          InitGlobales;
          if OpenProcess( prNOISPTAnualPago, Dataset.RecordCount, FListaParametros, FListaFormulas ) then
          begin
               try
                  if lEnviaNomina then
                     PagarANomina
                  else
                      PagarAPrestamo;
               except
                     on Error: Exception do
                     begin
                          Log.Excepcion( 0, 'Error Al Pagar Diferencias ISPT ', Error, DescripcionParams );
                     end;
               end;
          end;
          Result := CloseProcess;
     end;
end;

function TdmServerAnualNomina.ISPTPagoGetLista(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
     end;
     ISPTPagoInitProvider;
     InitBroker( False );
     try
        ISPTPagoBuildDataset;
        Result := SQLBroker.SuperReporte.GetReporte;
     finally
            ClearBroker;
     end;
     SetComplete;
end;

procedure TdmServerAnualNomina.ISPTPagoParametros;
const
     aDiferencias: array[ FALSE..TRUE ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'Pr�stamos', 'N�mina' );
     aCreditoSalarioLbl: array[ FALSE..TRUE ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'Cr�dito al Salario: ', 'Subsidio al Empleo: ' );
var
   lReformaFiscal2008: Boolean;
begin
     with oZetaProvider, ParamList do
     begin
          FListaParametros := VACIO;
          FListaFormulas   := VACIO;
          FListaParametros := 'A�o: ' + ParamByName( 'YearActivo' ).AsString +
                         K_PIPE + 'Diferencias A: ' + aDiferencias[ ParamByName( 'EnviarNomina' ).AsBoolean ];
          lReformaFiscal2008 := ( ParamByName( 'YearActivo' ).AsInteger >= ZetaCommonClasses.K_REFORMA_FISCAL_2008 );
          if ParamByName( 'EnviarNomina' ).AsBoolean then
             FListaParametros := FListaParametros +
                         K_PIPE + 'Conceptos' +
                         K_PIPE + 'Favor: ' + ParamByName( 'ExcepcionAFavor' ).AsString +
                         K_PIPE + 'Cargo: ' + ParamByName( 'ExcepcionACargo' ).AsString +
                         K_PIPE + 'ISPT: ' + ParamByName( 'ExcepcionISPTNeto' ).AsString +
                         K_PIPE + aCreditoSalarioLbl[lReformaFiscal2008] + ParamByName( 'ExcepcionCredito' ).AsString +
                         K_PIPE + 'Periodo: ' + ShowNomina( DatosPeriodo.Year, Ord( DatosPeriodo.Tipo ), DatosPeriodo.Numero )
          else
          begin
               FListaParametros := FListaParametros +
                          K_PIPE + 'Pr�stamos' +
                          K_PIPE + 'Favor: ' + ParamByName( 'PrestamoAFavor' ).AsString +
                          K_PIPE + 'Cargo: ' + ParamByName( 'PrestamoACargo' ).AsString +
                          K_PIPE + 'ISPT: ' + ParamByName( 'PrestamoISPTNeto' ).AsString +
                          K_PIPE + aCreditoSalarioLbl[lReformaFiscal2008] + ParamByName( 'PrestamoCredito' ).AsString +
                          K_PIPE + 'Compensar Desde: ' + FechaCorta( ParamByName( 'PrestamoInicio' ).AsDateTime ) +
                          K_PIPE + 'Referencia: ' + ParamByName( 'PrestamoReferencia' ).AsString;
               FListaFormulas :=    K_PIPE + 'F�rmula: ' + ParamByName( 'PrestamoFormula' ).AsString;
          end;
     end;
end;

function TdmServerAnualNomina.ISPTPagoLista(Empresa, Lista, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          GetDatosPeriodo;
          InitArregloTPeriodo;
     end;
     ISPTPagoParametros;
     ISPTPagoInitProvider;
     cdsLista.Lista := Lista;
     Result := ISPTPagoDataset( cdsLista );
     SetComplete;
end;

function TdmServerAnualNomina.ISPTPago(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          GetDatosPeriodo;
          InitArregloTPeriodo;
     end;
     ISPTPagoParametros;
     ISPTPagoInitProvider;
     InitBroker( False );
     try
        ISPTPagoBuildDataset;
        Result := ISPTPagoDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
            ClearBroker;
     end;
     SetComplete;
end;

{ ***** Pagar Reparto de Ahorros ************* }

procedure TdmServerAnualNomina.PagarAhorroBuildDataset;
begin
     with oZetaProvider do
     begin
          with SQLBroker do
          begin
               Init( enRepAhorro );
               with Agente do
               begin
                    AgregaColumna( 'CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
                    AgregaColumna( 'RA_REPARTO', True, Entidad, tgFloat, 0, 'RA_REPARTO' );
                    AgregaColumna( K_PRETTYNAME, TRUE, enEmpleado, tgTexto, 50, 'PrettyName' );
                    with ParamList do
                    begin
                         AgregaFiltro( Format( 'RA_YEAR = %d', [ ParamByName( 'YearAhorro' ).AsInteger ] ), True, Entidad );
                         AgregaFiltro( Format( 'TB_CODIGO = ''%s''', [ ParamByName( 'TipoAhorro' ).AsString ] ), True, Entidad );
                    end;
                    AgregaFiltro( 'RA_REPARTO <> 0', True, Entidad );
                    {Cambios sug 460: Multiples Tipos de N�mina}
                    AgregaFiltro( Format( 'CB_NOMINA = %d', [ Ord( DatosPeriodo.Tipo ) ] ), True, enEmpleado );

                    AgregaOrden( 'CB_CODIGO', True, Entidad );
               end;
               AgregaRangoCondicion( ParamList );
               FiltroConfidencial( EmpresaActiva );
               BuildDataset( EmpresaActiva );
          end;
     end;
end;

function TdmServerAnualNomina.PagarAhorroDataset(Dataset: TDataset): OleVariant;
var
   iEmpleado: TNumEmp;
   iConcepto: Integer;
   rMonto: Extended;
   lPercepcion: Boolean;
begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               iConcepto := ParamByName( 'Concepto' ).AsInteger;
          end;
          InitGlobales;
          if OpenProcess( prNOPagoRepartoAhorro, Dataset.RecordCount, FListaParametros ) then
          begin
               try
                  try
                     lPercepcion := EsPercepcion( iConcepto );
                     InitNomina;
                     with Nomina do
                     begin
                          try
                             VerificaNominaBegin;
                             ExcepcionMontoBegin;
                             with Dataset do
                             begin
                                  while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                                  begin
                                       iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                                       rMonto := ZetaCommonTools.Redondea( FieldByName( 'RA_REPARTO' ).AsFloat );
                                       if not lPercepcion then { Si Es Deduccion, Debe Ser Un Monto Negativo }
                                          rMonto := rMonto * -1;
                                       EmpiezaTransaccion;
                                       try
                                          VerificaRegistro( iEmpleado );
                                          ExcepcionMonto( iEmpleado, iConcepto, '', rMonto, omSustituir, lPercepcion );
                                          TerminaTransaccion( True );
                                       except
                                             on Error: Exception do
                                             begin
                                                  //TerminaTransaccion( False );
                                                  RollBackTransaccion;
                                                  Log.Excepcion( iEmpleado, 'Error Al Pagar Ahorros ', Error, DescripcionParams );
                                             end;
                                       end;
                                       Next;
                                  end;
                             end;
                          finally
                                 ExcepcionMontoEnd;
                                 VerificaNominaEnd;
                          end;
                          CalculaStatusPeriodo;     { 2.6 - Faltaba Calcular Status del Periodo }
                     end;
                  finally
                         ClearNomina;
                  end;
               except
                     on Error: Exception do
                     begin
                          Log.Excepcion( 0, 'Error Al Pagar Ahorros ', Error, DescripcionParams );
                     end;
               end;
          end;
          Result := CloseProcess;
     end;
end;

function TdmServerAnualNomina.PagarAhorroGetLista(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          GetDatosPeriodo;
     end;
     InitBroker( False );
     try
        PagarAhorroBuildDataset;
        Result := SQLBroker.SuperReporte.GetReporte;
     finally
            ClearBroker;
     end;
     SetComplete;
end;

procedure TdmServerAnualNomina.PagarAhorroParametros;
begin
     with oZetaProvider, DatosPeriodo do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'A�o: ' + ParamList.ParamByName( 'YearAhorro' ).AsString +
                         K_PIPE + 'Tipo de Ahorro: ' + ParamList.ParamByName( 'TipoAhorro' ).AsString +
                         K_PIPE + 'Concepto: ' + ParamList.ParamByName( 'Concepto' ).AsString +
                         K_PIPE + 'Periodo: ' + ZetacommonTools.ShowNomina( Year, Ord( Tipo ), Numero );
     end;
end;

function TdmServerAnualNomina.PagarAhorroLista(Empresa, Lista, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          GetDatosPeriodo;
          InitArregloTPeriodo;
     end;
     PagarAhorroParametros;
     cdsLista.Lista := Lista;
     Result := PagarAhorroDataset( cdsLista );
     SetComplete;
end;

function TdmServerAnualNomina.PagarAhorro(Empresa,Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          GetDatosPeriodo;
          InitArregloTPeriodo;
     end;
     PagarAhorroParametros;
     InitBroker( False );
     try
        PagarAhorroBuildDataset;
        Result := PagarAhorroDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
            ClearBroker;
     end;
     SetComplete;
end;

{ ******** Cerrar Ahorros ******* }

procedure TdmServerAnualNomina.CerrarAhorrosParametros;
const
     aOperacion: array[ 0..2 ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'Poner en Ceros', 'Cierre Anual', 'Borrar Registros' );
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Tipo de Ahorro: ' + ParamByName( 'TipoAhorro' ).AsString +
                          K_PIPE + 'Operaci�n: ' + aOperacion[ ParamByName( 'Operacion' ).AsInteger ];
          if  ParamByName( 'PonerFechaCierre' ).AsBoolean then
              FListaParametros := FListaParametros + K_PIPE + 'Fecha de Cierre: ' + FechaCorta( ParamByName( 'FechaCierre' ).AsDateTime );
     end;
end;

procedure TdmServerAnualNomina.CerrarAhorrosBuildDataset;
var
   sAhorro: string;
begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               sAhorro := ParamByName( 'TipoAhorro' ).AsString;
          end;
          with SQLBroker do
          begin
               Init( enAhorro );
               with Agente do
               begin
                    AgregaColumna( 'AHORRO.CB_CODIGO', True, Entidad, tgNumero, 10, 'CB_CODIGO' );
                    AgregaColumna( 'AHORRO.AH_TIPO', True, Entidad, tgTexto, 1, 'AH_TIPO' );
                    AgregaColumna( 'TAHORRO.TB_ELEMENT', True, enTAhorro, tgTexto, 30, 'TB_ELEMENT' );
                    AgregaColumna( 'AHORRO.AH_ABONOS', True, Entidad, tgFloat, 10, 'AH_ABONOS' );
                    AgregaColumna( 'AHORRO.AH_CARGOS', True, Entidad, tgFloat, 10, 'AH_CARGOS' );
                    AgregaColumna( 'AHORRO.AH_FECHA', True, Entidad, tgFecha, 10, 'AH_FECHA' );
                    AgregaColumna( 'AHORRO.AH_FORMULA', True, Entidad, tgTexto, 255, 'AH_FORMULA' );
                    AgregaColumna( 'AHORRO.AH_NUMERO', True, Entidad, tgNumero, 10, 'AH_NUMERO' );
                    AgregaColumna( 'AHORRO.AH_SALDO_I', True, Entidad, tgFloat, 10, 'AH_SALDO_I' );
                    AgregaColumna( 'AHORRO.AH_STATUS', True, Entidad, tgNumero, 10, 'AH_STATUS' );
                    AgregaColumna( 'AHORRO.AH_SALDO', True, Entidad, tgFloat, 10, 'AH_SALDO' );
                    AgregaColumna( 'AHORRO.AH_TOTAL', True, Entidad, tgFloat, 10, 'AH_TOTAL' );

                    AgregaFiltro( Format( 'AHORRO.AH_TIPO = ''%s''', [ sAhorro ] ), True, enAhorro );
                    AgregaOrden( 'AHORRO.CB_CODIGO', True, Entidad );
               end;
               AgregaRangoCondicion( ParamList );
               FiltroConfidencial( EmpresaActiva );
               BuildDataset( EmpresaActiva );
          end;
     end;
end;

function TdmServerAnualNomina.CerrarAhorrosDataset( DataSet: TDataSet ): OleVariant;
var
   sTipo : string;
   eOperacion: eCierreAhorro;
   lFechaCierre: Boolean;
   dCierre: TDate;
   FDataset: TZetaCursor;
   iEmpleado: integer;

   procedure EjecutaQuery( const iScript: Integer );
   begin
        with oZetaProvider do
        begin
             PreparaQuery( FDataset, GetSQLScript( iScript ) );
             ParamAsChar( FDataset, 'Tipo', sTipo, K_ANCHO_TIPO );
             ParamAsInteger( FDataSet, 'Empleado', iEmpleado );
             Ejecuta( FDataset );
        end;
   end;

   procedure PonerFechaCierre;
   begin
        if lFechaCierre then
        begin
             with oZetaProvider do
             begin
                  PreparaQuery( FDataset, GetSQLScript( Q_CIERRE_AHORRO_FECHA_CIERRE ) );
                  ParamAsDate( FDataset, 'Fecha', dCierre );
                  ParamAsChar( FDataset, 'Tipo', sTipo, K_ANCHO_TIPO );
                  ParamAsInteger( FDataSet, 'Empleado', iEmpleado );
                  Ejecuta( FDataset );
             end;
        end;
   end;

   function GetRegistroAnterior: string;

      function GetMonto( const sTexto, sField: string ): string;
      begin
           Result := sTexto + ': ' + FormatFloat('#,0.00;-#,0.00', DataSet.FieldByName(sField).AsFloat);
      end;

   begin
        with Dataset do
        begin
             Result := '***** Registro Anterior ***** ' + CR_LF +
                       Format( 'Ahorro Tipo %s = %s ', [ FieldByName('AH_TIPO').AsString, FieldByName('TB_ELEMENT').AsString ] ) + CR_LF +
                       'Fecha de Inicio: ' + FechaCorta( FieldByName('AH_FECHA').AsDateTime ) + CR_LF +
                       'Status: ' + ObtieneElemento( lfStatusAhorro, FieldByName('AH_STATUS').AsInteger ) + CR_LF +
                       'F�rmula: ' + FieldByName('AH_FORMULA').AsString + CR_LF +
                       CR_LF +
                       '-- Montos Totales --' + CR_LF +
                       GetMonto('Saldo Inicial','AH_SALDO_I') + CR_LF +
                       GetMonto('Ahorrado','AH_TOTAL') + CR_LF +
                       GetMonto('Otros Abonos','AH_ABONOS') + CR_LF +
                       GetMonto('Otros Cargos','AH_CARGOS') + CR_LF +
                       GetMonto('Saldo Actual','AH_SALDO') + CR_LF +
                       '# de Aportaciones: ' + FieldByName('AH_NUMERO').AsString;
        end;
   end;

   var sRegistroAnterior : string;

begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               sTipo := ParamByName( 'TipoAhorro' ).AsString;
               eOperacion := eCierreAhorro( ParamByName( 'Operacion' ).AsInteger );
               lFechaCierre := ParamByName( 'PonerFechaCierre' ).AsBoolean;
               dCierre := ParamByName( 'FechaCierre' ).AsDate;
          end;
          if OpenProcess( prSISTCierreAhorros, Dataset.RecordCount, FListaParametros ) then
          begin
               FDataset := CreateQuery;
               try
                  with Dataset do
                  begin
                       while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                       begin
                            iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                            sRegistroAnterior := GetRegistroAnterior;
                            EmpiezaTransaccion;
                            try
                               case eOperacion of
                                    ecPonerCeros:
                                    begin
                                         EjecutaQuery( Q_CIERRE_AHORRO_DEL_CANCELADOS );
                                         EjecutaQuery( Q_CIERRE_AHORRO_DEL_CAR_ABO );
                                         EjecutaQuery( Q_CIERRE_AHORRO_PONER_EN_CEROS );
                                         PonerFechaCierre;
                                         Log.Evento( clbHisAhorros, iEmpleado, Date, 'Ahorro Actualizado en Ceros Tipo: ' + sTipo, sRegistroAnterior ) ;

                                    end;
                                    ecCierreAnualAhorro:
                                    begin
                                         EjecutaQuery( Q_CIERRE_AHORRO_DEL_CANCELADOS );
                                         EjecutaQuery( Q_CIERRE_AHORRO_DEL_CAR_ABO );
                                         EjecutaQuery( Q_CIERRE_AHORRO_ACUMULA_SALDO );
                                         EjecutaQuery( Q_CIERRE_AHORRO_INIT_CEROS );
                                         PonerFechaCierre;
                                         Log.Evento( clbHisAhorros, iEmpleado, Date, 'Cierre de Ahorro Tipo: ' + sTipo, sRegistroAnterior ) ;
                                    end;
                                    ecBorrarRegistros:
                                    begin
                                         EjecutaQuery( Q_CIERRE_AHORRO_DEL_TODO );
                                         Log.Evento( clbHisAhorros, iEmpleado, Date, 'Ahorro Borrado Tipo: ' + sTipo, sRegistroAnterior );
                                    end;
                               end;
                               TerminaTransaccion( True );
                            except
                                  on Error: Exception do
                                  begin
                                       RollBackTransaccion;
                                       Log.Excepcion( iEmpleado, 'Error Al Cerrar Ahorros', Error, DescripcionParams );
                                  end;
                            end;
                            Next;
                       end;
                  end;
               finally
                      FreeAndNIl( FDataSet );
               end;
               Result := CloseProcess;
          end;
     end;
end;

function TdmServerAnualNomina.CerrarAhorros(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitBroker( False );
     try
        with oZetaProvider do
        begin
             EmpresaActiva:= Empresa;
             AsignaParamList( Parametros );
        end;
        CerrarAhorrosParametros;
        CerrarAhorrosBuildDataset;
        Result := CerrarAhorrosDataset( SQLBroker.SuperReporte.DataSetReporte );
    finally
           ClearBroker;
     end;
     SetComplete;
end;

{ ******* Cerrar Prestamos ******* }

procedure TdmServerAnualNomina.CerrarPrestamosParametros;
const
     aOperacion: array[ eCierrePrestamo ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'Borrar Saldados', 'Cierre Anual', 'Cancelar Pr�stamo' );

begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Tipo de Pr�stamo: ' + ParamByName( 'TipoPrestamo' ).AsString +
                          K_PIPE + 'Operaci�n: ' + aOperacion[ eCierrePrestamo( ParamByName( 'Operacion' ).AsInteger ) ];
          if ParamByName( 'PonerFechaCierre' ).AsBoolean then
             FListaParametros := FListaParametros + K_PIPE + 'Fecha Cierre: ' + FechaCorta( ParamByName( 'FechaCierre' ).AsDateTime );
          if ParamByName( 'UtilizaReferencia' ).AsBoolean then
             FListaParametros := FListaParametros + K_PIPE + 'Referencia del Pr�stamo: ' + ParamByName('RefPrestamo').AsString;
     end;
end;

procedure TdmServerAnualNomina.CerrarPrestamosBuildDataset;
var
   sPrestamo: string;
   ePrestamo: eCierrePrestamo;
   lPrestamoReferencia: Boolean;
   sReferencia: String;
begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               sPrestamo := ParamByName( 'TipoPrestamo' ).AsString;
               ePrestamo := eCierrePrestamo( ParamByName( 'Operacion' ).AsInteger );
               lPrestamoReferencia := ParamByName('UtilizaReferencia').AsBoolean;
	       sReferencia := ParamByName('RefPrestamo').AsString;
          end;
          with SQLBroker do
          begin
               Init( enPrestamo );
               with Agente do
               begin
                    AgregaColumna( 'PRESTAMO.CB_CODIGO', True, Entidad, tgNumero, 10, 'CB_CODIGO' );
                    AgregaColumna( 'PRESTAMO.PR_TIPO', True, Entidad, tgTexto, 1, 'PR_TIPO' );
                    AgregaColumna( 'PRESTAMO.PR_REFEREN', True, Entidad, tgTexto, 8, 'PR_REFEREN' );
                    AgregaColumna( 'TPRESTA.TB_ELEMENT', True, enTPresta, tgTexto, 30, 'TB_ELEMENT' );
                    AgregaColumna( 'PRESTAMO.PR_ABONOS', True, Entidad, tgFloat, 10, 'PR_ABONOS' );
                    AgregaColumna( 'PRESTAMO.PR_CARGOS', True, Entidad, tgFloat, 10, 'PR_CARGOS' );
                    AgregaColumna( 'PRESTAMO.PR_FECHA', True, Entidad, tgFecha, 10, 'PR_FECHA' );
                    AgregaColumna( 'PRESTAMO.PR_FORMULA', True, Entidad, tgTexto, 255, 'PR_FORMULA' );
                    AgregaColumna( 'PRESTAMO.PR_MONTO', True, Entidad, tgFloat, 10, 'PR_MONTO' );
                    AgregaColumna( 'PRESTAMO.PR_NUMERO', True, Entidad, tgNumero, 10, 'PR_NUMERO' );
                    AgregaColumna( 'PRESTAMO.PR_SALDO_I', True, Entidad, tgFloat, 10, 'PR_SALDO_I' );
                    AgregaColumna( 'PRESTAMO.PR_STATUS', True, Entidad, tgNumero, 10, 'PR_STATUS' );
                    AgregaColumna( 'PRESTAMO.PR_TOTAL', True, Entidad, tgFloat, 10, 'PR_TOTAL' );
                    AgregaColumna( 'PRESTAMO.PR_SALDO', True, Entidad, tgFloat, 10, 'PR_SALDO' );
                    AgregaFiltro( Format( 'PRESTAMO.PR_TIPO = ''%s''', [ sPrestamo ] ), True, enPrestamo );
                    case ePrestamo of
                         ecBorrarSaldados: AgregaFiltro( Format( 'PRESTAMO.PR_STATUS <> %d', [ Ord( spActivo ) ] ), True, enPrestamo );
                         ecCierreAnualPrestamo: AgregaFiltro( Format( 'PRESTAMO.PR_STATUS <> %d', [ Ord( spSaldado ) ] ), True, enPrestamo );
                         ecCancelarRegistros: AgregaFiltro( Format( 'PRESTAMO.PR_STATUS = %d', [ Ord( spActivo ) ] ), True, enPrestamo );
                    end;
                    if( lPrestamoReferencia )then
                         AgregaFiltro( Format( 'PRESTAMO.PR_REFEREN = %s',  [ EntreComillas( sReferencia )  ] ), True, enPrestamo );
                    AgregaOrden( 'PRESTAMO.CB_CODIGO', True, Entidad );
               end;
               AgregaRangoCondicion( ParamList );
               FiltroConfidencial( EmpresaActiva );
               BuildDataset( EmpresaActiva );
          end;
     end;
end;

function TdmServerAnualNomina.CerrarPrestamosDataset( DataSet: TDataSet ): OleVariant;
var
   sTipo, sReferencia: string;
   eOperacion: eCierrePrestamo;
   FDataset: TZetaCursor;
   lPonerFecha: Boolean;
   dCierre: TDate;
   iEmpleado: integer;

   procedure EjecutaQuery( const iScript: Integer );
   begin
        with oZetaProvider do
        begin
             PreparaQuery( FDataset, GetSQLScript( iScript ) );
             ParamAsChar( FDataset, 'Tipo', sTipo, K_ANCHO_TIPO );
             ParamAsInteger( FDataSet, 'Empleado', iEmpleado );
             ParamAsChar( FDataSet, 'Referencia', sReferencia, K_ANCHO_REFERENCIA );
             Ejecuta( FDataset );
        end;
   end;

   function GetRegistroAnterior: string;

      function GetMonto( const sTexto, sField: string ): string;
      begin
           Result := sTexto + ': ' + FormatFloat('#,0.00;-#,0.00', DataSet.FieldByName(sField).AsFloat);
      end;

   begin
        with Dataset do
        begin
             Result := '***** Registro Anterior ***** ' + CR_LF +
                       Format( 'Pr�stamo Tipo %s = %s ', [ FieldByName('PR_TIPO').AsString, FieldByName('TB_ELEMENT').AsString ] ) + CR_LF +
                       'Referencia: ' + FieldByName('PR_REFEREN').AsString + CR_LF +
                       'Fecha de Inicio: ' + FechaCorta( FieldByName('PR_FECHA').AsDateTime ) + CR_LF +
                       'Status: ' + ObtieneElemento( lfStatusPrestamo, FieldByName('PR_STATUS').AsInteger ) + CR_LF +
                       'F�rmula: ' + FieldByName('PR_FORMULA').AsString + CR_LF +
                       CR_LF +
                       '-- Montos Totales --' + CR_LF +
                       GetMonto('Monto Prestado','PR_MONTO') + CR_LF +
                       GetMonto('Abonos a�os anteriores','PR_SALDO_I') + CR_LF +
                       GetMonto('Abonado Este a�o','PR_TOTAL') + CR_LF +
                       GetMonto('Otros Abonos','PR_ABONOS') + CR_LF +
                       GetMonto('Otros Cargos','PR_CARGOS') + CR_LF +
                       GetMonto('Saldo Actual','PR_SALDO') + CR_LF +
                       '# de Aportaciones: ' + FieldByName('PR_NUMERO').AsString;
        end;
   end;

   var sRegistroAnterior, sMsgBitacora: string;
begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               sTipo := ParamByName( 'TipoPrestamo' ).AsString;
               eOperacion := eCierrePrestamo( ParamByName( 'Operacion' ).AsInteger );
               lPonerFecha := ParamByName( 'PonerFechaCierre' ).AsBoolean;
               dCierre := ParamByName( 'FechaCierre' ).AsDate;
          end;
          if OpenProcess( prSISTCierrePrestamos, Dataset.RecordCount, FListaParametros ) then
          begin
               FDataset := CreateQuery;
               try
                  with Dataset do
                  begin
                       while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                       begin
                            iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                            sReferencia := FieldByName( 'PR_REFEREN' ).AsString;
                            sMsgBitacora := Format('Tipo: %s, Referencia: %s', [ sTipo, sReferencia ]);
                            sRegistroAnterior := GetRegistroAnterior;
                            EmpiezaTransaccion;
                            try
                               case eOperacion of
                                    ecBorrarSaldados:
                                    begin
                                         EjecutaQuery( Q_CIERRE_PRESTAMO_DEL_SALDADOS );
                                         Log.Evento( clbHisPrestamos, iEmpleado, Date, 'Pr�stamo Borrado ' + sMsgBitacora, sRegistroAnterior );
                                    end;
                                    ecCierreAnualPrestamo:
                                    begin
                                         EjecutaQuery( Q_CIERRE_PRESTAMO_DEL_CANCELADOS );
                                         EjecutaQuery( Q_CIERRE_PRESTAMO_DEL_CAR_ABO );
                                         EjecutaQuery( Q_CIERRE_PRESTAMO_INIT_SALDO );
                                         EjecutaQuery( Q_CIERRE_PRESTAMO_INIT_CEROS );
                                         if lPonerFecha then
                                         begin
                                              PreparaQuery( FDataset, GetSQLScript( Q_CIERRE_PRESTAMO_FECHA_CIERRE ) );
                                              ParamAsDate( FDataset, 'Fecha', dCierre );
                                              ParamAsChar( FDataset, 'Tipo', sTipo, K_ANCHO_TIPO );
                                              ParamAsInteger( FDataSet, 'Empleado', iEmpleado );
                                              ParamAsChar( FDataSet, 'Referencia', sReferencia, K_ANCHO_REFERENCIA );
                                              Ejecuta( FDataset );
                                         end;
                                         Log.Evento( clbHisPrestamos, iEmpleado, Date, 'Cierre de Pr�stamo ' + sMsgBitacora, sRegistroAnterior ) ;
                                    end;
                                    ecCancelarRegistros:
                                    begin
                                         PreparaQuery( FDataset, GetSQLScript( Q_CIERRE_PRESTAMO_CANCELA_REG ) );
                                         ParamAsInteger( FDataset, 'Status', Ord( spCancelado ) );
                                         ParamAsChar( FDataset, 'Tipo', sTipo, K_ANCHO_TIPO );
                                         ParamAsInteger( FDataSet, 'Empleado', iEmpleado );
                                         ParamAsChar( FDataSet, 'Referencia', sReferencia, K_ANCHO_REFERENCIA );
                                         Ejecuta( FDataset );
                                         Log.Evento( clbHisPrestamos, iEmpleado, Date, 'Pr�stamo Cancelado ' + sMsgBitacora, sRegistroAnterior  );
                                    end;
                               end;
                               TerminaTransaccion( True );
                            except
                                  on Error: Exception do
                                  begin
                                       RollBackTransaccion;
                                       Log.Excepcion( iEmpleado, 'Error Al Cerrar Pr�stamos', Error, DescripcionParams );
                                  end;
                            end;
                            Next;
                       end;
                  end;
               finally
                      FreeAndNil( FDataSet );
               end;
               Result := CloseProcess;
          end;
     end;
end;

function TdmServerAnualNomina.CerrarPrestamos(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitBroker( False );
     try
        with oZetaProvider do
        begin
             EmpresaActiva:= Empresa;
             AsignaParamList( Parametros );
        end;
        CerrarPrestamosParametros;
        CerrarPrestamosBuildDataset;
        Result := CerrarPrestamosDataset( SQLBroker.SuperReporte.DataSetReporte );
    finally
           ClearBroker;
     end;
     SetComplete;
end;

{ ********** C�lculo de Tiempos de Labor ************* }

procedure TdmServerAnualNomina.CalcularTiemposParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'Fecha Inicial: ' + FechaCorta( ParamByName( 'FechaInicial' ).AsDateTime ) +
                              ZetaCommonClasses.K_PIPE + 'Fecha Final: ' + FechaCorta( ParamByName( 'FechaFinal' ).AsDateTime );
     end;
end;

procedure TdmServerAnualNomina.CalcularTiemposBuildDataset;
const
     K_FILTRO = '( ( CB_ACTIVO = ''S'' ) and ( CB_FEC_ING <= ''%s'' ) ) or ( ( CB_ACTIVO <> ''S'' ) and ( CB_FEC_BAJ >= ''%s'' ) )';
var
   dInicial, dFinal: TDate;
begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               dInicial := ParamByName( 'FechaInicial' ).AsDate;
               dFinal := ParamByName( 'FechaFinal' ).AsDate;
          end;
          with SQLBroker do
          begin
               Init( enEmpleado );
               with Agente do
               begin
                    AgregaColumna( 'COLABORA.CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
                    AgregaColumna( 'COLABORA.CB_CHECA', True, Entidad, tgBooleano, 0, 'CB_CHECA' );
                    AgregaColumna( K_PRETTYNAME, TRUE, enEmpleado, tgTexto, 35, 'PRETTYNAME' );
                    AgregaFiltro( Format( K_FILTRO, [ ZetaCommonTools.DateToStrSQL( dFinal ), ZetaCommonTools.DateToStrSQL( dInicial ) ] ), True, Entidad );
                    AgregaOrden( 'CB_CODIGO', True, Entidad );
               end;
               AgregaRangoCondicion( ParamList );
               BuildDataset( EmpresaActiva );
          end;
     end;
end;

function TdmServerAnualNomina.CalcularTiemposDataSet(Dataset: TDataSet): OleVariant;
var
   dFecha, dInicial, dFinal, dFechaCorte: TDate;
   lLog, lOk, lLecturas, lValidaFechaCorte: Boolean;
   oLabor: TLCalculo;
   FQuery: TZetaCursor;
   iCedulas, iDias: Integer;
   sMensaje: string;
begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               dInicial := ParamByName( 'FechaInicial' ).AsDate;
               dFinal := ParamByName( 'FechaFinal' ).AsDate;
               lLecturas := ParamByName( 'CalcularLecturas' ).AsBoolean;
               lLog := ParamByName( 'RegistrarBitacora' ).AsBoolean;
          end;
          InitGlobales;
          lValidaFechaCorte := GetGlobalBooleano( K_GLOBAL_LABOR_VALIDA_FECHACORTE );
          dFechaCorte := GetGlobalDate( K_GLOBAL_LABOR_FECHACORTE );
          if ( ( not dLaborCalculo.ValidaCambioLabor( dInicial, dFechaCorte, lValidaFechaCorte, ukModify, sMensaje ) ) or
             ( not dLaborCalculo.ValidaCambioLabor( dFinal, dFechaCorte, lValidaFechaCorte, ukModify, sMensaje ) ) ) then
             DataBaseError( sMensaje );
          oLabor := TLCalculo.Create( oZetaCreator );
          try
             { Ejecuci�n }
             {PROCESAR LAS LECTURAS Y PASARLAS A WORKS}
             if lLecturas then
                oLabor.ProcesarPendientes;
             { Contar cuantas c�dulas hay que procesar }
             iCedulas := oLabor.CuantasCedulas( dInicial, dFinal );
             iDias := ( Trunc( dFinal ) - Trunc( dInicial ) ) + 1;
             if OpenProcess( prLabCalcularTiempos, ( ( 2 * iCedulas ) + ( ( ( 2 * iDias ) + 1 ) * Dataset.RecordCount ) ), FListaParametros ) then
             begin
                  { Antes de empezar, hay que pasar del DATASET hacia TMPLABOR ( Cuenta por 1 ciclo completo ) }
                  lOk := False;
                  FQuery := CreateQuery( Format( GetSQLScript( Q_TMPLABOR_AGREGAR ), [ UsuarioActivo ] ) );
                  try
                     EmpiezaTransaccion;
                     try
                        ExecSQL( EmpresaActiva, Format( GetSQLScript( Q_TMPLABOR_BORRAR ), [ UsuarioActivo ] ) );
                        with Dataset do
                        begin
                             while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                             begin
                                  ParamAsInteger( FQuery, 'CB_CODIGO', FieldByName( 'CB_CODIGO' ).AsInteger );
                                  ParamAsBoolean( FQuery, 'CB_CHECA', ZetaCommonTools.zStrToBool( FieldByName( 'CB_CHECA' ).AsString ) );
                                  Ejecuta( FQuery );
                                  Next;
                             end;
                        end;
                        TerminaTransaccion( True );
                        lOk := True;
                     except
                           on Error: Exception do
                           begin
                                TerminaTransaccion( False );
                                Log.Excepcion( 0, 'Error Al Crear Lista De Empleados', Error );
                           end;
                     end;
                  finally
                         FreeAndNil( FQuery );
                  end;
                  if lOk then
                  begin
                       with oLabor do
                       begin
                            try
                               CalcularTiemposBegin( dInicial, dFinal, cdsLista, lLog );
                               dFecha := dInicial;
                               while ( dFecha  <= dFinal ) do
                               begin
                                    CalcularTiempos( dFecha );
                                    dFecha := dFecha + 1;
                               end;
                            finally
                                   CalcularTiemposEnd;
                            end;
                       end;
                  end;
                  Result := CloseProcess;
             end;
          finally
                 FreeAndNil( oLabor );
          end;
     end;
end;

function TdmServerAnualNomina.CalcularTiemposGetLista(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
     end;
     CalcularTiemposParametros;
     InitBroker( False );
     try
        CalcularTiemposBuildDataset;
        Result := SQLBroker.SuperReporte.GetReporte;
     finally
            ClearBroker;
     end;
     SetComplete;
end;

function TdmServerAnualNomina.CalcularTiempos(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
     end;
     CalcularTiemposParametros;
     InitBroker( False );
     try
        CalcularTiemposBuildDataset;
        Result := CalcularTiemposDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
            ClearBroker;
     end;
     SetComplete;
end;

procedure TdmServerAnualNomina.CreditoAplicadoParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'A�o: ' + ParamByName( 'Year' ).AsString +
                              K_PIPE + 'Mes: ' + ObtieneElemento( lfMeses, ParamByName( 'Mes' ).AsInteger - GetOffSet( lfMeses ) ) +
                              K_PIPE + 'Concepto a Acumular: ' + ParamByName( 'Concepto' ).AsString +'= ' +  ParamByName( 'DescConcepto' ).AsString +
                              K_PIPE + 'Factor Mensual: ' + FormatFloat( '0.00', ParamByName( 'Factor' ).AsFloat ) +
                              K_PIPE + 'Tabla: ' + ParamByName( 'Tabla' ).AsString + '= ' + ParamByName( 'DescTabla' ).AsString +
                              K_PIPE + 'Rastreo: ' + BoolToSiNo( ParamByName( 'Rastreo' ).AsBoolean );
          FListaFormulas := VACIO;
          FListaFormulas := K_PIPE + 'F�rmulas ' +
                            K_PIPE + 'Ingreso Bruto: ' + ParamByName( 'IngresoBruto' ).AsString +
                            K_PIPE + 'Ingreso Exento: ' + ParamByName( 'IngresoExento' ).AsString +
                            K_PIPE + 'D�as Trabajados: ' + ParamByName( 'Dias' ).AsString;
     end;
end;

function TdmServerAnualNomina.CreditoAplicadoDataset(DataSet: TDataSet): OleVariant;
var
   FAfectaConcepto : TZetaCursor;
   rFactor : TTasa;
   iEmpleado, iTabla,iYear,iMes,iConcepto : integer;
   rDias,rPercepciones, rAcumulado, rCuota, rIngresoBruto, rIngresoExento, rCuotaTabla : TPesos;
   lRastrear : Boolean;
   sRastro : string;
   dFecha : TDate;
   lReformaFiscal2008: Boolean;
begin
     with oZetaProvider do
     begin
          if OpenProcess( prNOCreditoAplicado, Dataset.RecordCount, FListaParametros, FListaFormulas ) then
          begin
               with ParamList do
               begin
                    rFactor := ParamByName('Factor').AsFloat;
                    iTabla := ParamByName('Tabla').AsInteger;
                    iYear := ParamByName('Year').AsInteger;
                    iMes := ParamByName('Mes').AsInteger;
                    iConcepto := ParamByName('Concepto').AsInteger;
                    lRastrear := ParamByName( 'Rastreo' ).AsBoolean ;
               end;

               dFecha := ZetaCommonTools.CodificaFecha( iYear, iMes, 1 );  // Primer D�a del Mes a Evaluar
               lReformaFiscal2008 := ( iYear >= ZetaCommonClasses.K_REFORMA_FISCAL_2008 );

               with oZetaCreator do
               begin
                    PreparaTablasISPT;
                    if lRastrear then
                       PreparaRastreo;
               end;

               try
                  try
                     FAfectaConcepto := CreateQuery( GetSQLScript( Q_CRED_APLICADO_AFECTA_CONCEPTO ) );
                     ParamAsInteger( FAfectaConcepto, 'Year', iYear );
                     ParamAsInteger( FAfectaConcepto, 'Mes', iMes );
                     ParamAsInteger( FAfectaConcepto, 'Concepto',iConcepto );
                     ParamAsInteger( FAfectaConcepto, 'Factor', 1 ); //Es uno porque quiero afectar el concepto

                     with Dataset do
                     begin
                          while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                          begin
                               iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                               rDias := FieldByName( 'Dias' ).AsFloat;
                               rIngresoBruto := FieldByName( 'IngresoBruto' ).AsFloat;
                               rIngresoExento := FieldByName( 'IngresoExento' ).AsFloat;
                               rPercepciones := rIngresoBruto - rIngresoExento;

                               if ( rDias <> 0 ) then
                                  rPercepciones := ( rPercepciones / rDias ) * rFactor
                               else
                                   rPercepciones := 0;

                               rAcumulado := FieldByName('Acumulado').AsFloat;

                               if ( rFactor <> 0 ) then
                               begin
                                    rCuotaTabla := oZetaCreator.dmTablasISPT.ConsultaTablaNumerica( iTabla, rPercepciones, dFecha ).Cuota;
                                    rCuota := ( rCuotaTabla / rFactor ) * rDias
                               end
                               else
                               begin
                                    rCuotaTabla := 0;;
                                    rCuota := 0;
                               end;


                               if NOT PesosIguales( rAcumulado, rCuota ) then
                               begin
                                    ParamAsInteger( FAfectaConcepto, 'Empleado', iEmpleado );
                                    ParamAsFloat( FAfectaConcepto, 'Monto', rCuota - rAcumulado );

                                    EmpiezaTransaccion;
                                    try
                                       Ejecuta( FAfectaConcepto );
                                       TerminaTransaccion( True );
                                    except
                                          on Error: Exception do
                                          begin
                                               TerminaTransaccion( False );
                                               raise;
                                          end;
                                    end;
                               end;

                               if lRastrear then
                               begin
                                    with Rastreador do
                                    begin
                                         RastreoBegin;
                                         if lReformaFiscal2008 then
                                            RastreoHeader( 'RASTREO DEL CALCULO DE SUBE APLICADO MENSUAL' )
                                         else
                                             RastreoHeader( 'RASTREO DEL CALCULO DEL CREDITO APLICADO MENSUAL' );
                                         RastreoTexto( ' Concepto: ', IntToStr(iConcepto) + ' = ' + ParamList.ParamByName('DescConcepto').AsString  );
                                         RastreoTexto( ' A�o: ', IntToStr( iYear ) );
                                         RastreoTexto( ' Mes: ', ObtieneElemento( lfMeses, iMes - GetOffSet( lfMeses ) ) );
                                         RastreoTexto( ' Empleado: ', IntToStr( iEmpleado ) + ' = ' + FieldByName('PrettyName').AsString );
                                         RastreoEspacio;
                                         RastreoHeader( 'FORMULAS' );
                                         RastreoTexto( ' Ingreso Bruto: ', ParamList.ParamByName('IngresoBruto').AsString );
                                         RastreoTexto( ' Ingreso Exento: ', ParamList.ParamByName('IngresoExento').AsString );
                                         RastreoTexto( ' D�as Trabajados: ', ParamList.ParamByName('Dias').AsString );
                                         RastreoEspacio;
                                         RastreoHeader('MONTOS CONSIDERADOS' );
                                         RastreoPesos( ' (+)           Percepciones Brutas: ', rIngresoBruto );
                                         RastreoPesos( ' (-)          Percepciones Exentas: ', rIngresoExento );
                                         RastreoPesos( ' (=)         Percepciones Gravadas: ', rIngresoBruto - rIngresoExento );
                                         RastreoPesos( ' (/)               D�as Trabajados: ', rDias );
                                         RastreoFloat( ' (X)                Factor Mensual: ', rFactor, 5 );
                                         RastreoPesos( ' (=)   Percepci�n Promedio Mensual: ', rPercepciones );
                                         RastreoEspacio;
                                         RastreoHeader('APLICACION DE LA TABLA' );
                                         RastreoTexto( '                    Tabla Aplicada: ', IntToStr( iTabla ) + ' = ' + ParamList.ParamByName('DescTabla').AsString  );
                                         RastreoPesos( '       Percepci�n Promedio Mensual: ', rPercepciones );
                                         RastreoPesos( '                             Cuota: ', rCuotaTabla );
                                         RastreoFloat( ' (/)                Factor Mensual: ', rFactor, 5 );
                                         RastreoPesos( ' (X)               D�as Trabajados: ', rDias );
                                         RastreoPesos( Format( ' (=)              %s Aplicado: ', [ aCreditoSalarioLower[lReformaFiscal2008] ] ), rCuota );

                                         sRastro := Rastro;
                                    end;

                                    if ZetaCommonTools.StrLleno( sRastro ) then
                                       Log.Evento( clbNinguno, iEmpleado, Date, Format( 'Rastreo %s Aplicado A�o %d, Mes %s', [ aCreditoSalarioLower[lReformaFiscal2008], iYear, ObtieneElemento( lfMeses, iMes - GetOffSet( lfMeses ) ) ] ), sRastro );
                               end;

                               Next;
                          end;
                     end;//with
                  finally
                         FreeAndNil( FAfectaConcepto );
                  end;
               finally
                      if lRastrear then
                         oZetaCreator.DesPreparaRastreo;
               end;
          end; // If OpenProcess
          Result := CloseProcess;
     end; //oZetaProvider
end;

procedure TdmServerAnualNomina.CreditoAplicadoBuildDataset;
var
    FPeriodo : TZetaCursor;
    iYear, iMes, iConcepto : integer;
    sIngresoBruto, sIngresoExento, sDias : string;
    dInicial, dFinal : TDateTime;
begin
     with oZetaProvider do
     begin

          with ParamList do
          begin
               iYear := ParamByName( 'Year' ).AsInteger;
               iMes := ParamByName( 'Mes' ).AsInteger;
               iConcepto := ParamByName( 'Concepto' ).AsInteger;
               {iMes := ParamByName( 'Tabla' ).AsInteger;
               }
               sIngresoBruto := ParamByName( 'IngresoBruto' ).AsString;
               sIngresoExento := ParamByName( 'IngresoExento' ).AsString;
               sDias := ParamByName( 'Dias' ).AsString;
               {iMes := ParamByName( 'Factor' ).AsString;
               iMes := ParamByName( 'Rastreo' ).AsBoolean;}
          end;

          FPeriodo := CreateQuery;
          try
             AbreQueryScript( FPeriodo,  Format( GetSQLScript(Q_CRED_APLICADO_FECHAS_PERIODO), [iYear, iMes]) );

             dInicial := FPeriodo.FieldByName( 'FechaMin' ).AsDateTime;
             dFinal := FPeriodo.FieldByName( 'FechaMax' ).AsDateTime;
          finally
                 FreeAndNil( FPeriodo );
          end;

          with SQLBroker do
          begin
               Init( enEmpleado );
               with Agente do
               begin
                    AgregaColumna( 'COLABORA.CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
                    AgregaColumna( K_PRETTYNAME, TRUE, enEmpleado, tgTexto, 35, 'PRETTYNAME' );
                    AgregaColumna( sIngresoBruto, False, Entidad, tgFloat, 0, 'INGRESOBRUTO' );
                    AgregaColumna( sIngresoExento, False, Entidad, tgFloat, 0, 'INGRESOEXENTO' );
                    AgregaColumna( sDias, False, Entidad, tgFloat, 0, 'DIAS' );
                    AgregaColumna( Format('A(%d,1)', [iConcepto]), False, Entidad, tgFloat, 0, 'ACUMULADO' );
                    AgregaFiltro( Format( GetSQLScript( Q_CRED_APLICADO_FILTRO ), [ ZetaCommonTools.DateToStrSQLC( dInicial ),
                                                                                    ZetaCommonTools.DateToStrSQLC( dFinal ) ] ), True, Entidad );
                    AgregaOrden( 'CB_CODIGO', True, Entidad );
               end;
               AgregaRangoCondicion( ParamList );
               BuildDataset( EmpresaActiva );
          end;
     end;
end;

function TdmServerAnualNomina.CreditoAplicado(Empresa,Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          GetDatosPeriodoCredito;
     end;
     CreditoAplicadoParametros;
     InitBroker( False );
     try
        CreditoAplicadoBuildDataset;
        Result := CreditoAplicadoDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
            ClearBroker;
     end;
     SetComplete;
end;

function TdmServerAnualNomina.CreditoAplicadoGetLista(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          GetDatosPeriodoCredito;
     end;
     CreditoAplicadoParametros;
     InitBroker( False );
     try
        CreditoAplicadoBuildDataset;
        Result := SQLBroker.SuperReporte.GetReporte;
     finally
            ClearBroker;
     end;
     SetComplete;
end;

procedure TdmServerAnualNomina.GetDatosPeriodoCredito;
begin
     with oZetaProvider do
     begin
          GetDatosPeriodo;
          with ParamList do
          begin
               YearDefault := ParamByName('Year').AsInteger;
               MesDefault := ParamByName('Mes').AsInteger;
          end;
     end;
end;

function TdmServerAnualNomina.CreditoAplicadoLista(Empresa, Lista,Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          GetDatosPeriodoCredito;
     end;
     CreditoAplicadoParametros;
     InitBroker( False );
     cdsLista.Lista := Lista;
     Result := CreditoAplicadoDataset( cdsLista );
     SetComplete;
end;

procedure TdmServerAnualNomina.AfectarLaborParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'FechaCorte: ' + FechaCorta( ParamByName( 'FechaCorte' ).AsDateTime ) +
                              K_PIPE + 'A�o: ' + ParamByName( 'Year' ).AsString +
                              K_PIPE + 'Tipo: ' + ZetaCommonLists.ObtieneElemento( lfTipoPeriodo, ParamByName( 'Tipo' ).AsInteger ) +
                              K_PIPE + 'N�mero: ' + ParamByName( 'Numero' ).AsString;
     end;
end;

function TdmServerAnualNomina.AfectarLabor(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitArregloTPeriodo;
          AsignaParamList( Parametros );
          AfectarLaborParametros;
          OpenProcess( prLabAfectarLabor, 0, FListaParametros );
          //Solo se esta actualizando el Global 130, Fecha de Corte en Labor
          //Posteriomente se realizaran varios procesos
          Result := CloseProcess;
     end;
     Setcomplete;
end;

procedure TdmServerAnualNomina.DeclaracionAnualCierreParametros;
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'A�o: ' + ParamByName( 'Year' ).AsString +
                              K_PIPE + 'Status Inicial: ' + ZetaCommonLists.ObtieneElemento( lfStatusDeclaracionAnual, ParamByName('StatusInicial' ).AsInteger ) +
                              K_PIPE + 'Status Final: ' + ZetaCommonLists.ObtieneElemento( lfStatusDeclaracionAnual, ParamByName('StatusFinal' ).AsInteger );
     end;
end;

function TdmServerAnualNomina.DeclaracionAnualCierre(Empresa, Parametros: OleVariant): OleVariant;  {$ifndef DOS_CAPAS} safecall; {$endif}
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );

          DeclaracionAnualCierreParametros;
          if OpenProcess( prNODeclaracionAnualCierre, 0, FListaParametros ) then
          begin
               try
                  EjecutaAndFree( Format( GetSQLScript( Q_DECLARACION_CIERRE ),
                                          [ ParamList.ParamByName('StatusFinal').AsInteger,
                                            UsuarioActivo,
                                            ParamList.ParamByName('Year').AsInteger] ) );
               except
                     on Error:Exception do
                        Log.Excepcion( 0 , 'Error al ejecutar Cierre de la Declaraci�n Anual' , Error );
               end;
          end;
          Result := CloseProcess;
     end;
     SetComplete;
end;


//DECLARACION ANUAL
function TdmServerAnualNomina.DeclaracionAnual(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          GetDatosPeriodo;
{$ifndef ANTES}
          GetDatosActivos;
          YearDefault := oZetaProvider.ParamList.ParamByName( 'Year' ).AsInteger;   // Para que las formulas funcionen con el a�o del proceso
          InitGlobales;
{$endif}
     end;
     DeclaracionAnualParametros;
     InitBroker( False ); //Revisar si se requieren funciones anuales
     try
        DeclaracionAnualBuildDatasetLista;
{$ifdef ANTES}
        Result := DeclaracionAnualDataset( SQLBroker.SuperReporte.DataSetReporte );
{$else}
        cdsLista.Data := SQLBroker.SuperReporte.GetReporte;
        {$ifdef CAROLINA}
        cdsLista.SaveToFile( 'd:\temp\subt.cds' );
        SQLBroker.Agente.SQL.SaveToFile( 'd:\temp\DeclaracionAnual.sql' );
        {$ENDIF}
        Result := DeclaracionAnualDataset( cdsLista );
{$endif}
     finally
            ClearBroker;
     end;
     SetComplete;
end;

function TdmServerAnualNomina.DeclaracionAnualGetLista(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          GetDatosPeriodo;
{$ifndef ANTES}
          GetDatosActivos;
          YearDefault := oZetaProvider.ParamList.ParamByName( 'Year' ).AsInteger;   // Para que las formulas funcionen con el a�o del proceso
          InitGlobales;
{$endif}
     end;
     DeclaracionAnualParametros;
     InitBroker( False );
     try
        DeclaracionAnualBuildDatasetLista;
        Result := SQLBroker.SuperReporte.GetReporte;
     finally
            ClearBroker;
     end;
     SetComplete;
end;

function TdmServerAnualNomina.DeclaracionAnualLista(Empresa, Lista, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
{$ifndef ANTES}
          GetDatosActivos;
          YearDefault := oZetaProvider.ParamList.ParamByName( 'Year' ).AsInteger;   // Para que las formulas funcionen con el a�o del proceso
          InitGlobales;
{$endif}
     end;
     DeclaracionAnualParametros;
     cdsLista.Lista := Lista;
     Result := DeclaracionAnualDataset( cdsLista );
     SetComplete;
end;

procedure TdmServerAnualNomina.FiltrosAnualesInit;
var
    oEntidad: TipoEntidad;
begin
     with SQLBroker do
     begin
          if HayFiltroCondicion then oEntidad := enEmpleado
          else oEntidad := enNomina;

          Init( oEntidad );
     end;

end;

procedure TdmServerAnualNomina.FiltrosAnualesColumnas;
var
    sTabla, CB_CODIGO: string;
begin
     with SQLBroker do
     begin
          if Agente.Entidad = enEmpleado then
          begin
               sTabla := 'COLABORA';
               CB_CODIGO := 'COLABORA.CB_CODIGO';
          end
          else
          begin
               sTabla := 'NOMINA';
               CB_CODIGO := 'DISTINCT(NOMINA.CB_CODIGO)';
          end;


          with Agente do
          begin
               AgregaColumna( CB_CODIGO, True, Entidad, tgNumero, 0, 'CB_CODIGO' );
               //AgregaColumna( K_PRETTYNAME, TRUE, enEmpleado, tgTexto, K_ANCHO_PRETTYNAME, 'PRETTYNAME' );
               AgregaOrden( 'CB_CODIGO', True, Entidad );
               if Entidad = enNomina then
               begin
{$ifdef ANTES}
                    AgregaFiltro( Format('NOMINA.PE_YEAR = %d',[oZetaProvider.DatosPeriodo.Year]), TRUE, Entidad );
{$else}
                    AgregaFiltro( Format('NOMINA.PE_YEAR = %d',[oZetaProvider.YearDefault]), TRUE, Entidad );
{$endif}
               end;
          end;
     end;

end;

procedure TdmServerAnualNomina.DeclaracionAnualBuildDatasetInitLista;
begin
     FFormulasPendientesTD:= TList.Create;
     FiltrosAnualesInit;
     FiltrosAnualesColumnas;
     SQLBroker.Agente.AgregaColumna( K_PRETTYNAME, TRUE, enEmpleado, tgTexto, 50, 'PrettyName' );

end;

procedure TdmServerAnualNomina.DeclaracionAnualBuildDatasetLista;
begin
     DeclaracionAnualBuildDatasetInitLista;
     with SQLBroker do
     begin
          with oZetaProvider do
          begin
               AgregaRangoCondicion( ParamList );
               BuildDataset( EmpresaActiva );
          end;
     end;
end;

{$ifdef ANTES}
function TdmServerAnualNomina.GetFiltroDeclaracionAnual( DataSet: TDataset ): String;
begin
     Result := VACIO;
     with DataSet do
     begin
          First;
          while NOT EOF do
          begin
               Result := ConcatString( Result, FieldByName('CB_CODIGO').AsString, ',' );
               Next;
          end;
     end;

     if StrLleno( Result ) then
        Result := Format( 'CB_CODIGO IN (%s)', [ Result ] );
end;
{$endif}

function TdmServerAnualNomina.DeclaracionAnualNombreCampo( const iPos: integer ): string;
begin
     //Result := 'TD_DATA_' + StrZero( IntToStr( iPos - K_GLOBAL_DECANUAL_PRIMER_CAMPO + 1), 2 );
     Result := ZetaCommonTools.DeclaracionAnualNombreCampo( iPos, K_GLOBAL_DECANUAL_PRIMER_CAMPO );
end;

procedure TdmServerAnualNomina.DeclaracionAnualCargaFormulas;
  const K_TD_PARAM = '#PARAM';
        K_TD_PARAM1 = '#PARAM1';
        K_TD_PARAM2 = '#PARAM2';
        K_TD_PARAM3 = '#PARAM3';
        K_TD_EXENTO = '#EXENTO';
  const
        aConstantes: array[ 0..1 ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( K_TD_PARAM, K_TD_EXENTO );

 function RequiereSustituir(const sValor : string ): Boolean;
  var j: integer;
 begin
      Result := FALSE;
      if StrLleno(sValor) then
         for j:=Low(aConstantes) to High(aConstantes) do
         begin
              Result := AnsiPos( aConstantes[j], Uppercase(sValor) ) > 0;
              if Result then Break;
         end;
 end;

 function TransForma(const sValor, sBusca, sSustituye: string ): string;
 begin
      Result := StrTransAll( sValor, sBusca, StrDef( sSustituye, '0' ) );
 end;

 function SetMax( const sValor: string ) : string;
 begin
      Result := sValor;
      if ( Result <>  '0' ) then
      begin
           Result := Format( 'MAX(%s,0)' , [sValor] );
      end;
 end;

  function SetMXR( const sValor: string ) : string;
 begin
      Result := sValor;
      if ( Result <>  '0' ) then
      begin
           if( oZetaprovider.ParamList.ParamByName( 'Redondear' ).AsBoolean = True ) then
               Result := Format( 'MXR(%s)' , [sValor] )
           else
               Result := Format( 'MAX(%s,0)' , [sValor] );
      end;
 end;

 function SustituyeValores( const sValor : string; const iPosicion: integer; var lPendientes: Boolean ): string;
 begin
      Result := sValor;
      if RequiereSustituir( Result ) then
      begin
           //Parametros
           if ( AnsiPos(K_TD_PARAM, UpperCase(Result))>0 ) then
           begin
                Result := TransForma( Result, K_TD_PARAM1, Parentesis( oZetaProvider.GetGlobalString(K_GLOBAL_DECANUAL_PRIMER_CAMPO) ) );
                Result := TransForma( Result, K_TD_PARAM2, Parentesis( oZetaProvider.GetGlobalString(K_GLOBAL_DECANUAL_PRIMER_CAMPO+1) ) );
                Result := TransForma( Result, K_TD_PARAM3, Parentesis( oZetaProvider.GetGlobalString(K_GLOBAL_DECANUAL_PRIMER_CAMPO+2) ) );
           end;

           //Se sustituyen los exentos
           if ( AnsiPos(K_TD_EXENTO, UpperCase(Result))>0 ) and
              ( iPosicion >= K_TD_PRIMER_GRAVADO ) and ( iPosicion <= K_TD_ULTIMO_GRAVADO ) then
           begin
                if ( (iPOsicion mod 2)=0 ) then
                   Result := TransForma( Result, K_TD_EXENTO,
                                         SetMXR( StrDef( oZetaProvider.GetGlobalString(iPosicion+1), '0') ) )
                else
                    oZetaProvider.Log.ErrorGrave(0, 'No es V�lido el uso de #EXENTO en la F�rmula #' + IntToStr(iPosicion) );
           end;

           //Se valida si hay Sustituciones que quedaron pendientes.
           lPendientes := RequiereSustituir(Result);
      end;
 end;

 const K_MAX_VUELTAS = 10;
 var
    i, iColumna, iVueltas: integer;
    oColumna : TSQLColumna;
    lPendientes: Boolean;
    sError: string;
begin
{$ifdef ANTES}
     oZetaProvider.InitGlobales;
{$endif}
     lPendientes := FALSE;

     {Agregar las columnas numericas}
     for i:=K_GLOBAL_DECANUAL_PRIMER_CAMPO to K_GLOBAL_DECANUAL_ULTIMO_CAMPO do
     begin
          with SQLBroker.Agente do
          begin
               if ( i <=  K_GLOBAL_DECANUAL_PRIMER_CAMPO+2 ) then
                  iColumna := AgregaColumna( SustituyeValores( SetMAX( StrDef( oZetaProvider.GetGlobalString(i), '0') ), i, lPendientes ) , FALSE, enNinguno, tgFloat, 0, DeclaracionAnualNombreCampo(i) )
               else
                   iColumna := AgregaColumna( SustituyeValores( SetMXR( StrDef( oZetaProvider.GetGlobalString(i), '0') ), i, lPendientes ) , FALSE, enNinguno, tgFloat, 0, DeclaracionAnualNombreCampo(i) );
               GetColumna(iColumna).PosCampo := i;
               if lPendientes then
                  FFormulasPendientesTD.Add( Pointer( iColumna ) );
          end;
     end;
     //Si hubo sustituciones pendientes de hacer, se dan las pasadas necesarias.
     //Existe un limite de 10 vueltas.
     if ( FFormulasPendientesTD.Count > 0 ) then
     begin
          iVueltas := 0;
          repeat
                i :=0;
                with SQLBroker.Agente do
                begin
                     while i < FFormulasPendientesTD.Count do
                     begin
                          oColumna := GetColumna( Integer( FFormulasPendientesTD[i] ) );
                          oColumna.Formula := SustituyeValores( oColumna.Formula, oColumna.PosCampo, lPendientes );
                          if NOT lPendientes then
                             FFormulasPendientesTD.Delete( i )
                          else
                              Inc(i);
                     end;
                end;
                Inc(iVueltas);
          until (FFormulasPendientesTD.Count = 0) or (iVueltas = K_MAX_VUELTAS );

          if FFormulasPendientesTD.Count > 0 then
          begin
               for i:= 0 to FFormulasPendientesTD.Count - 1 do
                   sError := ConcatString( sError, '#' + IntToStr( Integer( FFormulasPendientesTD[i] ) + 1 ), ', ' );
               oZetaProvider.Log.ErrorGrave(0, 'Existe Referencia Circular entre las F�rmulas ' + sError );
          end;
     end;


     //Se agrega a la Bitacora como quedaron las formulas sustituidas.
     with SQLBroker.Agente do
     begin
          sError := VACIO;
          for i := 0  to NumColumnas - 1 do
          begin
               oColumna := GetColumna( i );
               if ( oColumna.PosCampo >= K_GLOBAL_DECANUAL_PRIMER_CAMPO ) and
                  ( oColumna.PosCampo <= K_GLOBAL_DECANUAL_ULTIMO_CAMPO)
                  then
               begin
                    sError := ConCatString( sError, oColumna.Alias + ': '+ oColumna.Formula, CR_LF );
                    if ( Length( oColumna.Formula ) > 255 ) then
                       oZetaProvider.Log.ErrorGrave(0, 'La f�rmula excede 255 caracteres: ' + oColumna.Alias + ': '+ oColumna.Formula );
               end;
          end;
          oZetaProvider.Log.Evento( clbNinguno, 0, Date, 'F�rmulas Sustituidas',  sError );
     end;

end;

procedure TdmServerAnualNomina.CalculaSelloDigital( oDataset : TDataset );
begin
     if zStrToBool( oDataset.FieldByName( 'RS_SELLO' ).AsString ) then
     begin
          if dmSelloDigital = NIL then
             dmSelloDigital := TdmSelloDigital.Create( NIL );

          with dmSelloDigital do
          begin
               Year := oZetaProvider.ParamList.ParamByName('Year').AsInteger;
               if Calcular( oDataset, oZetaProvider ) then
               begin
                    with oDataset do
                    begin
                         if State = dsBrowse then Edit;

                         FieldByName( 'TD_SELLO' ).AsString := Sello;
                         FieldByName( 'TD_ORIG' ).AsString :=  CadenaOriginal;
                         FieldByName( 'TD_DIGES' ).AsString := Digestion;
                    end;
               end
               else
               begin
                    {$ifdef CAROLINA}
                    with oDataset do
                    begin
                         if State = dsBrowse then Edit;

                         FieldByName( 'TD_SELLO' ).AsString := Sello;
                         FieldByName( 'TD_ORIG' ).AsString :=  CadenaOriginal;
                         FieldByName( 'TD_DIGES' ).AsString := Digestion;
                    end;

                    {$endif}
                    oZetaProvider.Log.Advertencia( oDataset.FieldByName('CB_CODIGO').AsInteger ,
                                                   'Error en la generaci�n del Sello Digital',
                                                   ErrorMessage );


               end;
          end;
     end;
end;

function TdmServerAnualNomina.DeclaracionAnualFormulas( Dataset: TDataSet ): TServerDataset;
var
   i: integer;
   sNombreCampo: string;
{$ifdef ANTES}
   sFiltro: string;
{$else}
   sOldIndice : String;
{$endif}
begin
{$ifdef ANTES}
     // Primera Opci�n - Falla por L�mite de BD Interbase y Firebird
     sFiltro := GetFiltroDeclaracionAnual(DataSet);
     // Segunda Opci�n - Falla por L�mite de tama�o del Script
     sFiltro := ZetaDataSetTools.ConstruyeFiltroEmpleados( DataSet, 'CB_CODIGO' );
{$endif}

     InitBroker( TRUE );
     oZetaCreator.RegistraFunciones( [efComunes,efAnuales,efReporte]  );

     with SQLBroker do
     begin
          Init( enEmpleado );
          oZetaCreator.SuperReporte := SuperReporte;

          {Van primero las f�rmulas, para que concuerden con el Result(x)}
          DeclaracionAnualCargaFormulas;

          with Agente do
          begin
               AgregaColumna( 'COLABORA.CB_CODIGO', True, Entidad, tgNumero, 0, 'CB_CODIGO' );
               AgregaColumna( K_PRETTYNAME, TRUE, enEmpleado, tgTexto, K_ANCHO_PRETTYNAME, 'PRETTYNAME' );
               AgregaColumna( 'VALIDA_RFC', FALSE, enNinguno, tgTexto, K_ANCHO_DESCCORTA, 'RFC_VALIDADO' );
               AgregaColumna( 'COLABORA.CB_CURP', TRUE, enEmpleado, tgTexto, K_ANCHO_PRETTYNAME, 'TD_CB_CURP' );
               AgregaColumna( 'COLABORA.CB_APE_PAT', TRUE, enEmpleado, tgTexto, K_ANCHO_PRETTYNAME, 'TD_CB_APE_PAT' );
               AgregaColumna( 'COLABORA.CB_APE_MAT', TRUE, enEmpleado, tgTexto, K_ANCHO_PRETTYNAME, 'TD_CB_APE_MAT' );
               AgregaColumna( 'COLABORA.CB_NOMBRES', TRUE, enEmpleado, tgTexto, K_ANCHO_PRETTYNAME, 'TD_CB_NOMBRES' );
               AgregaColumna( 'COLABORA.CB_FEC_ING', True, enEmpleado, tgFecha, 10, 'CB_FEC_ING' );
               AgregaColumna( '0', True, enEmpleado, tgNumero, 10, 'EMP_REPETIDO' );
               AgregaColumna( '0', True, enEmpleado, tgNumero, 10, 'FECHA_REPETIDA' );
               //AgregaOrden( 'VALIDA_RFC', False, enEmpleado );

               { Columnas necesarias para poder generar el sello digital}
               AgregaColumna( 'RSOCIAL.RS_SELLO', True, enRSocial, tgTexto, 1, 'RS_SELLO' );
               AgregaColumna( 'RSOCIAL.RS_CODIGO', True, enRSocial, tgTexto, 6, 'RS_CODIGO' );
               {
               AgregaColumna( 'RSOCIAL.RS_NOMBRE', True, enRSocial, tgTexto, 255, 'RET_RAZONSOCIAL' );
               AgregaColumna( 'RSOCIAL.RS_RFC', True, enRSocial, tgTexto, 255, 'RET_RFC' );
               AgregaColumna( 'RSOCIAL.RS_CURP', True, enRSocial, tgTexto, 255, 'RET_CURP' );

               AgregaColumna( 'RSOCIAL.RS_RLEGAL', True, enRSocial, tgTexto, 255, 'RL_NOMBRE' );
               AgregaColumna( 'RSOCIAL.RS_RL_RFC', True, enRSocial, tgTexto, 255, 'RL_RFC' );
               AgregaColumna( 'RSOCIAL.RS_RL_CURP', True, enRSocial, tgTexto, 255, 'RL_CURP' );
               AgregaColumna( 'RSOCIAL.RS_KEY_PR', True, enRSocial, tgMemo, 255, 'RL_KEY_PR' );
               AgregaColumna( 'RSOCIAL.RS_KEY_PU', True, enRSocial, tgMemo, 255, 'RL_KEY_PU' );
               }

               AgregaColumna( '''', FALSE, Entidad, tgTexto, 255, 'TD_SELLO' );
               AgregaColumna( '@'+''' ''', FALSE, Entidad, tgMemo, 0, 'TD_ORIG' );
               AgregaColumna( '''', FALSE, Entidad, tgTexto, 50, 'TD_DIGES' );
               {AgregaColumna( 'DATE', FALSE, Entidad, tgFecha, 0, 'TD_FEC_SEL' );
               AgregaColumna( Comillas(FormatDateTime( 'hhmm', now )), FALSE, Entidad, tgTexto, 0, 'TD_HOR_SEL' );}


               AgregaOrden( 'COLABORA.CB_RFC', True, enEmpleado );
{$ifdef ANTES}
               AgregaFiltro( sFiltro, True, Entidad );
{$endif}
          end;
          with oZetaProvider do
          begin
               //AgregaRangoCondicion( ParamList );
               BuildDataset( EmpresaActiva );
               {$ifdef CAROLINA}
               SQLBroker.Agente.SQL.SaveToFile( 'd:\temp\DECLARA.SQL' );
               {$ENDIF}

{$ifndef ANTES}
               sOldIndice := TServerDataSet( DataSet ).IndexFieldNames;
               TServerDataSet( DataSet ).IndexFieldNames := 'CB_CODIGO';
               try
                  with SuperReporte.DataSetReporte do
                  begin
                       First;
                       while ( not EOF ) do
                       begin
                            if DataSet.Locate( 'CB_CODIGO', FieldByName( 'CB_CODIGO' ).AsInteger, [] ) then
                            begin
                                 for i:=K_GLOBAL_DECANUAL_PRIMER_CAMPO to K_GLOBAL_DECANUAL_ULTIMO_CAMPO do
                                 begin
                                      sNombreCampo := DeclaracionAnualNombreCampo(i);
                                      if (i >= K_TD_PRIMER_GRAVADO) and (i<= K_TD_ULTIMO_GRAVADO) then
                                      begin
                                           if (FieldByName( sNombreCampo ).AsFloat < 0) then
                                           begin
                                                if not ( State in [ dsEdit, dsInsert ] ) then
                                                   Edit;
                                                FieldByName( sNombreCampo ).AsFloat := 0;
                                                oZetaProvider.Log.Advertencia(FieldByName( 'CB_CODIGO' ).AsInteger, 'Valor Negativo Campo # ' + IntToStr(i-K_GLOBAL_DECANUAL_PRIMER_CAMPO+1) , 'El Monto del Campo #'+ IntToStr(i-K_GLOBAL_DECANUAL_PRIMER_CAMPO+1) + ' se Actualiz� a CERO' );
                                           end;
                                      end;

                                      {if( ParamList.ParamByName( 'Redondear' ).AsBoolean = True ) then
                                      begin
                                           if not ( State in [ dsEdit, dsInsert ] ) then
                                              Edit;
                                           FieldByName( sNombreCampo ).AsInteger := Round( FieldByName( sNombreCampo ).AsFloat ) ;
                                      end;}
                                 end;
                                 {Calculo del Sello Digital}
                                 //CV: PENDIENTE falta que esta parte sea opcional.
                                 CalculaSelloDigital(SuperReporte.DataSetReporte);

                                 Next;
                            end
                            else
                                Delete;
                       end;
                       MergeChangeLog;
                  end;
               finally
                      TServerDataSet( DataSet ).IndexFieldNames := sOldIndice;
               end;
{$endif}
               Result := SuperReporte.DataSetReporte;
          end;
     end;
end;


procedure TdmServerAnualNomina.ProcesoAnualValidaRepetidos(oDataSet: TServerDataSet; const eRepetidos : eEmpleadoRepetido; aCampos: array of TPesos; const dFecValidaBajas: TDate; const lEsCalculoISPT:Boolean = FALSE );
var
   iRepetido, iEmpleado, iTotalRep : integer;
   sRFC: string;
   dTope : TDate;
   lHayRepetido: Boolean;
   sRFCValidado, sRFCAnterior, sFiltro: string;
   iTotalEmp, iUltimoEmpleado : integer;
   lFiltrado: Boolean;

   procedure AsignaRepetido( const iEmpleadoRepetido: Integer );
   var
      dFecha : TDate;
   begin
        with oDataSet do
        begin
             dFecha := FieldByName('CB_FEC_ING').AsDateTime;
             if ( eRepetidos in [ erUltimo, erSumarUltimo ] ) then
             begin
                  dFecha := dTope - dFecha;
             end;
             if not( State in [dsEdit] ) then
                Edit;
             FieldByName('EMP_REPETIDO').AsInteger := iEmpleadoRepetido;
             FieldByName('FECHA_REPETIDA' ).AsInteger := Trunc( dFecha );

             Inc(iTotalRep);
             oZetaProvider.Log.Evento( clbNinguno, FieldByName('CB_CODIGO').AsInteger, Date, Format( 'El Empleado %d Repetido con el Empleado %d', [FieldByName('CB_CODIGO').AsInteger, iEmpleadoRepetido ] ) ) ;
        end;
   end;

   procedure InitValores;
   var
      i: integer;
   begin
        for i := FOffsetCampos to High(aCampos) do
        begin
             aCampos[i] := 0;
        end;
   end;

   procedure AcumulaValores;
   var
      i: integer;
   begin
        for i := FOffsetCampos to High(aCampos) do
        begin
             aCampos[i] := aCampos[i] + oDataSet.Fields[i].AsFloat;
        end;
   end;

   procedure SumaValores;
   var
      i: integer;
   begin
        for i := FOffsetCampos to High(aCampos) do
        begin
             with oDataSet do
             begin
                  if Not( State in [dsEdit] ) then
                     Edit;
                  with Fields[i] do
                       AsFloat := AsFloat + aCampos[i];
                  Post;
             end;
        end;
   end;

   procedure RevisaRFCValido;
   var
      sFiltro: string;
      lFiltered : Boolean;
   begin
        with oDataset do
        begin
             First;
             sFiltro := Filter;
             lFiltered := Filtered;

             Filtered := FALSE;
             try
                Filter := 'RFC_VALIDADO = ''''';
                Filtered := TRUE;
                while not EOF do
                begin
                     if StrVacio( FieldByName('RFC_VALIDADO').AsString ) then
                     begin
                          oZetaProvider.Log.Advertencia( clbNinguno, FieldByName('CB_CODIGO').AsInteger, Date, 'No se pudo calcular RFC del empleado',
                                                         'El RFC del empleado no se puede calcular.' +CR_LF+
                                                         'Revise que el empleado tenga capturados los datos de Nombre, Apellidos y Fecha de Nacimiento.' +CR_LF+
                                                         'El c�lculo no se realiz� para este empleado.' );
                          Delete;
                     end
                     else
                         Next;
                end;
             finally
                    Filter := sFiltro;
                    Filtered := lFiltered;
             end;
        end;
   end;

begin
     dTope := EncodeDate( 2100, 1, 1 );
     with oDataset do
     begin
          RevisaRFCValido;

          iTotalEmp := RecordCount;

          IndexFieldNames := 'RFC_VALIDADO;CB_CODIGO';
          First;

          lHayRepetido := FALSE;
          iEmpleado := FieldByName('CB_CODIGO').AsInteger;
          iRepetido := iEmpleado;
          sRFCAnterior := Trim( FieldByName('RFC_VALIDADO').AsString );
          Next;
          while not Eof and oZetaProvider.CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
          begin
               sRFCValidado := Trim( FieldByName('RFC_VALIDADO').AsString );

               if ( sRFCAnterior = sRFCValidado ) then
               begin
                    lHayRepetido := TRUE;
                    AsignaRepetido(iEmpleado);
                    iRepetido:= FieldByName('CB_CODIGO').AsInteger
               end
               else
               begin
                    if lHayRepetido then
                    begin
                         iUltimoEmpleado := FieldByName('CB_CODIGO').AsInteger;
                         if Locate( 'CB_CODIGO', iEmpleado, [] ) then
                         begin
                              AsignaRepetido( iRepetido );
                         end;
                         Locate( 'CB_CODIGO', iUltimoEmpleado, [] );
                         Assert( (FieldByName('CB_CODIGO').AsInteger = iUltimoEmpleado), 'No se posicion� en el lugar correcto' );
                    end;

                    lHayRepetido := FALSE;

                    iEmpleado := FieldByName('CB_CODIGO').AsInteger;
                    sRFCAnterior := sRFCValidado;
               end;

               Next;
          end;

          //Si los dos ultimos empleados del oDataset son empleados repetidos entre si, ignoraba al ultimo.
          //Por eso hay que preguntar si quedo algun empleado repetido y asignarlo
          if lHayRepetido then
          begin
               if Locate( 'CB_CODIGO', iEmpleado, [] ) then
               begin
                    AsignaRepetido( iRepetido );
               end;
          end;

          IndexFieldNames := 'RFC_VALIDADO;FECHA_REPETIDA;CB_CODIGO';
          First;

          //iEmpleado := FieldByName('CB_CODIGO').AsInteger;
          sRFC := Trim( FieldByName('RFC_VALIDADO').AsString );
          if ( eRepetidos = erNoIncluir ) then
          begin
               while not Eof and oZetaProvider.CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
               begin
                    {$ifdef ANTESVERSION2010}
                    if ( FieldByName( 'EMP_REPETIDO' ).AsInteger <> 0 ) or
                       ( ( dFecValidaBajas <> NullDateTime ) and ( FieldByName('CB_FEC_BAJ').AsDateTime >= FieldByName('CB_FEC_ING').AsDateTime ) and
                                                                 ( FieldByName('CB_FEC_BAJ').AsDateTime < dFecValidaBajas ) ) then
                    begin
                         Delete;
                    end
                    else
                        Next;
                    {$else}
                    if ( FieldByName( 'EMP_REPETIDO' ).AsInteger <> 0 ) or
                            ( ( dFecValidaBajas <> NullDateTime ) and ( FieldByName('CB_FEC_BAJ').AsDateTime >= FieldByName('CB_FEC_ING').AsDateTime ) and
                                                                      ( FieldByName('CB_FEC_BAJ').AsDateTime < dFecValidaBajas ) ) then
                    begin
                         if lEsCalculoISPT then
                         begin
                              //Si el empleado esta repetido, necesito borrarlo.
                              if ( FieldByName( 'EMP_REPETIDO' ).AsInteger <> 0 ) then
                                 Delete
                              //Si no es repetido, nada mas marcarlo como que no esta activo, para que no lo calcule.
                              else
                              begin
                                   if Not( State in [dsEdit] ) then
                                      Edit;
                                   FieldByName( 'EMPLEADO_ACTIVO' ).AsString := K_GLOBAL_NO;
                                   Next;
                              end;
                         end
                         else
                             Delete
                    end
                    else
                         Next;
                    {$endif}
               end;
          end
          else
          begin
               if ( eRepetidos <> erIncluirTodos ) then
               begin
                    while not Eof and oZetaProvider.CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                    begin
                         iRepetido := FieldByName( 'EMP_REPETIDO' ).AsInteger;
                         iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                         //Next;

                         //( COLABORA.CB_FEC_BAJ < COLABORA.CB_FEC_ING ) or ( CB_FEC_BAJ >= ''%s'' )
                         if ( iRepetido = 0 ) then
                         begin
                              {$ifdef ANTESVERSION2010}
                              if ( dFecValidaBajas <> NullDateTime ) and
                                 ( FieldByName('CB_FEC_BAJ').AsDateTime >= FieldByName('CB_FEC_ING').AsDateTime ) and
                                 ( FieldByName('CB_FEC_BAJ').AsDateTime < dFecValidaBajas ) then
                                 Delete
                              else
                                  Next;
                              {$else}
                              if ( lEsCalculoISPT ) and
                                 ( FieldByName('CB_FEC_BAJ').AsDateTime >= FieldByName('CB_FEC_ING').AsDateTime ) and
                                 ( FieldByName('CB_FEC_BAJ').AsDateTime < dFecValidaBajas ) then
                              begin
                                   if Not( State in [dsEdit] ) then
                                      Edit;
                                   FieldByName( 'EMPLEADO_ACTIVO' ).AsString := K_GLOBAL_NO;
                              end;
                              //CV: Siempre se hace el next, porque nunca se borra el registro
                              Next;
                              {$endif}
                         end
                         else //iRepetido<>0
                         {$ifdef ANTES_VERSION2010}
                         begin
                              Next;
                              InitValores;
                              while not Eof and oZetaProvider.CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) and
                                    (iRepetido = FieldByName( 'EMP_REPETIDO' ).AsInteger) do
                              begin
                                   if ( eRepetidos in [erSumarPrimero, erSumarUltimo] ) then
                                   begin
                                        AcumulaValores;
                                   end;
                                   Delete;
                              end;
                              if ( eRepetidos in [erSumarPrimero, erSumarUltimo] ) then
                              begin
                                   if NOT EOF  then
                                      Prior;
                                   Assert( iEmpleado = FieldByName( 'CB_CODIGO' ).AsInteger, 'El empleado no es el mismo' );
                                   SumaValores;
                                   Next;
                              end;
                         end
                         {$else}
                         begin
                              lFiltrado := Filtered;
                              sFiltro := Filter;
                              Filter := ConcatFiltros( sFiltro , Format( 'RFC_VALIDADO = %s', [EntreComillas(FieldByName('RFC_VALIDADO').AsString)] ) );
                              try
                                 try
                                    Filtered := TRUE;
                                    First;
                                    Next;
                                    InitValores;
                                    while (RecordCount>1) and oZetaProvider.CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                                    begin
                                         if ( eRepetidos in [erSumarPrimero, erSumarUltimo] ) then
                                         begin
                                              AcumulaValores;
                                         end;

                                         Delete;
                                    end;
                                    if ( eRepetidos in [erSumarPrimero, erSumarUltimo] ) then
                                    begin
                                         Assert( iEmpleado = FieldByName( 'CB_CODIGO' ).AsInteger, 'El empleado no es el mismo' );
                                         SumaValores;
                                    end;
                                 except
                                       on E: Exception do
                                          oZetaProvider.Log.Error( FieldByName( 'CB_CODIGO' ).AsInteger, 'Error al buscar al empleado', E.Message )  ;

                                 end;

                              finally
                                     Filter := sFiltro;
                                     Filtered := lFiltrado;
                                     //Hago el Next, porque al empleado posicionado lo acabo de revisar, y necesito que se actualice el EOF del Dataset
                                     if Locate( 'CB_CODIGO', iEmpleado, []) then
                                        Next
                                     else
                                         Assert( TRUE, 'El empleado no es el mismo' );
                              end;
                         end
                         {$endif}
                    end;
               end;
          end;
          { Se puede borrar en otras situaciones, el incremento se da ahora en
            asigna repetido. iTotalRep := iTotalEmp - RecordCount;}
          oZetaProvider.Log.Evento( clbNinguno, 0, Date,
                                    Format( 'Empleados Procesados: %d', [iTotalEmp] ),
                                    Format( 'Se Procesaron %d Empleados De Los Cuales Se Encontraron %d Repetidos', [ iTotalEmp, iTotalRep ] ) ) ;
     end;
end;

function TdmServerAnualNomina.DeclaracionAnualDataset(DataSet: TDataSet): OleVariant;
var
   i, iYear : integer;
   eRepetidos : eEmpleadoRepetido;
   FQuery : TZetaCursor;

   oDataset: TServerDataset;
   aCampos:  array[ 0..K_GLOBAL_DECANUAL_ULTIMO_CAMPO - K_GLOBAL_DECANUAL_PRIMER_CAMPO ] of TPesos;
begin
     with oZetaProvider do
     begin
          if OpenProcess( prNODeclaracionAnual, Dataset.RecordCount * 3, FListaParametros, FListaFormulas ) then
          begin
               FOffsetCampos := 3; //Es el offset, para que no tome en cuenta los primeros 3 parametros de la declaracion.
               with ParamList do
               begin
                    iYear := ParamByName('Year').AsInteger;
                    eRepetidos := eEmpleadoRepetido( ParamByName( 'Repetidos' ).AsInteger );
               end;

               EmpiezaTransaccion;
               try
                  FQuery := CreateQuery( Format( GetSQLScript( Q_DECLARACION_ANUAL_BORRAR ), [ iYear ] ) );
                  Ejecuta( FQuery );
                  TerminaTransaccion( True );
               except
                     on Error: Exception do
                     begin
                          TerminaTransaccion( False );
                          raise;
                     end;
               end;


               oDataset := DeclaracionAnualFormulas( Dataset );

               ProcesoAnualValidaRepetidos( oDataset, eRepetidos, aCampos, NullDateTime, FALSE );

               {
               ****************************************************************
               Ya est� analizado el Dataset, ya se eliminaron y se seleccionaron
               los empleados repetidos.
               Ya esta listo para agregarse a la tabla Temporal TMPDIMM
               ****************************************************************
               }
               PreparaQuery( FQuery, GetSQLScript( Q_DECLARACION_ANUAL_INSERT ) );
               ParamAsInteger( FQuery, 'TD_YEAR',  iYear );
               ParamAsInteger( FQuery, 'US_CODIGO', UsuarioActivo );

               with oDataset do
               begin
                    First;
                    while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                    begin
                         ParamAsInteger( FQuery, 'CB_CODIGO', FieldByName( 'CB_CODIGO' ).AsInteger );
                         ParamAsInteger( FQuery, 'TD_REPETID', FieldByName( 'EMP_REPETIDO' ).AsInteger );
                         ParamAsInteger( FQuery, 'TD_CUANTOS', 0 );

                         if zStrToBool( oDataset.FieldByName( 'RS_SELLO' ).AsString ) then
                         begin
                              ParamAsString( FQuery, 'TD_SELLO', FieldByName('TD_SELLO').AsString );
                              ParamAsString( FQuery, 'TD_ORIG', FieldByName('TD_ORIG').AsString );
                              ParamAsString( FQuery, 'TD_DIGES', FieldByName('TD_DIGES').AsString );
                              ParamAsDate( FQuery, 'TD_FEC_SEL',  Date );
                              ParamAsString( FQuery, 'TD_HOR_SEL',  FormatDateTime( '0000', Time )  );
                         end
                         else
                         begin
                              ParamAsString( FQuery, 'TD_SELLO', VACIO );
                              ParamAsString( FQuery, 'TD_ORIG', VACIO );
                              ParamAsString( FQuery, 'TD_DIGES', VACIO );
                              ParamAsDate( FQuery, 'TD_FEC_SEL',  NullDateTime );
                              ParamAsString( FQuery, 'TD_HOR_SEL',  FormatDateTime( '0000', 0 )  );
                         end;

                          
                         for i:=K_GLOBAL_DECANUAL_PRIMER_CAMPO to K_GLOBAL_DECANUAL_ULTIMO_CAMPO do
                             ParamAsFloat( FQuery,
                                           DeclaracionAnualNombreCampo(i),
                                           FieldByName(DeclaracionAnualNombreCampo(i)).AsFloat );
                         EmpiezaTransaccion;
                         try
                            Ejecuta( FQuery );
                            TerminaTransaccion( True );
                         except
                               on Error: Exception do
                               begin
                                    TerminaTransaccion( False );
                                    raise;
                               end;
                         end;
                         Next;
                    end;

                    //Despues de Calcular la declaracion debe de quedar marcada como calculada
                    EmpiezaTransaccion;
                    try
                       PreparaQuery( FQuery, Format( GetSQLScript( Q_DECLARACION_CIERRE_BORRA ),
                                                      [ ParamList.ParamByName('Year').AsInteger] ) );
                       Ejecuta( FQuery );

                       PreparaQuery( FQuery, Format( GetSQLScript( Q_DECLARACION_CIERRE_INS ),
                                                      [ ParamList.ParamByName('Year').AsInteger,
                                                        Ord(  esdAbierta ),
                                                        UsuarioActivo ] ) );
                       Ejecuta( FQuery );
                       TerminaTransaccion( True );
                    except
                          on Error: Exception do
                          begin
                               TerminaTransaccion( False );
                               raise;
                          end;
                    end;

               end;
               FreeAndNil(FQuery);
          end; // If OpenProcess
          Result := CloseProcess;
     end; //oZetaProvider
end;

procedure TdmServerAnualNomina.DeclaracionAnualParametros;
{$ifndef ANTES}
var
   i : Integer;
{$endif}
begin
     with oZetaProvider.ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'A�o: ' + ParamByName( 'Year' ).AsString +
                              K_PIPE + 'Empleados Repetidos: ' + ZetaCommonLists.ObtieneElemento( lfEmpleadoRepetido, ParamByName( 'Repetidos' ).AsInteger ) +
                              K_PIPE + 'Subsidio Acreditable: ' + ParamByName( 'Subsidio' ).AsString;
{$ifndef ANTES}
          FListaFormulas := VACIO;
          FListaFormulas := K_PIPE + 'F�rmulas ';
          for i := K_GLOBAL_DECANUAL_PRIMER_CAMPO to K_GLOBAL_DECANUAL_ULTIMO_CAMPO do
          begin
               FListaFormulas := FListaFormulas + K_PIPE + DeclaracionAnualNombreCampo( i ) + ': ' +
                                 oZetaProvider.GetGlobalString(i);
          end;
{$endif}
     end;
end;

function TdmServerAnualNomina.GrabaDeclaracionGlobales(Empresa,oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
 const
      K_DELETE = 'DELETE FROM GLOBAL WHERE GL_CODIGO >= %d AND GL_CODIGO <= %d';
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          try
             EmpiezaTransaccion;
             ExecSQL( Empresa, Format( K_DELETE, [K_GLOBAL_DECANUAL_PRIMER_CAMPO, K_GLOBAL_DECANUAL_ULTIMO_CAMPO] ) ); //PENDIENTE
             TerminaTransaccion( TRUE );
          except
                on Error: Exception do
                   TerminaTransaccion( FALSE );
          end;

          TablaInfo.SetInfo( 'GLOBAL', 'GL_CODIGO, GL_DESCRIP,GL_FORMULA', 'GL_CODIGO' );
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
     end;
     SetComplete;
end;

function TdmServerAnualNomina.GetDeclaracionGlobales(Empresa: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          TablaInfo.SetInfo( 'GLOBAL', 'GL_CODIGO, GL_DESCRIP,GL_FORMULA', 'GL_CODIGO' );
          TablaInfo.Filtro := Format( 'GL_CODIGO >= %d AND GL_CODIGO <= %d', [K_GLOBAL_DECANUAL_PRIMER_CAMPO, K_GLOBAL_DECANUAL_ULTIMO_CAMPO] );
          Result := GetTabla( Empresa );
     end;
end;

function TdmServerAnualNomina.ActualizaTotPerLista(Empresa, Lista, Parametros: OleVariant): OleVariant;
const
     K_UPDATE_ANUAL = 'update ACUMULA set AC_MES_13 = AC_MES_13 + :AC_MES_13 where AC_YEAR = :AC_YEAR AND CB_CODIGO = :CB_CODIGO AND CO_NUMERO = :CO_NUMERO ';

     K_UPDATE_TOPE = 'update ACUMULA set AC_MES_13 = AC_MES_13 - ( AC_MES_01 + AC_MES_02 + AC_MES_03 + AC_MES_04 + AC_MES_05 + AC_MES_06 + ' +
                     'AC_MES_07 + AC_MES_08 + AC_MES_09 + AC_MES_10 + AC_MES_11 + AC_MES_12 + AC_MES_13 ) where AC_YEAR = %d AND ' +
                     '( ( AC_MES_01 + AC_MES_02 + AC_MES_03 + AC_MES_04 + AC_MES_05 + AC_MES_06 + ' +
                     'AC_MES_07 + AC_MES_08 + AC_MES_09 + AC_MES_10 + AC_MES_11 + AC_MES_12 + AC_MES_13 ) < 0 ) %s %s';

     K_LIMITE_CONCEPO = '%s ( CO_NUMERO IN (%s) )';

     K_INSERT_ANUAL = 'insert into ACUMULA ( AC_YEAR, CB_CODIGO, CO_NUMERO, AC_MES_13 ) VALUES ( :AC_YEAR, :CB_CODIGO, :CO_NUMERO, :AC_MES_13 ) ';

var
   oUpdAcAnual, oUpdTopeAnual, oInsAcAnual: TZetaCursor;
   sExentos: String;
   iEmpleado, iCambios: Integer;

   procedure ActualizaAnual( const iConcepto: Integer; const rActualAnual, rTotalAnual: Double; sListado: String );
   begin
        with oZetaProvider do
        begin
             if ( StrLleno ( sListado ) and not PesosIgualesP( rTotalAnual, rActualAnual, ParamList.ParamByName('Tolerancia').AsFloat ) ) then
             begin
                  {ParamAsFloat( oUpdAcAnual, 'AC_ANUAL', rTotalAnual );}
                  { Mete un ajuste en mes 13 para que la funci�n A regrese el resultado esperado }
                  ParamAsFloat( oUpdAcAnual, 'AC_MES_13', rTotalAnual - rActualAnual );
                  ParamAsInteger( oUpdAcAnual, 'CO_NUMERO', iConcepto );
                  iCambios:= Ejecuta( oUpdAcAnual );

                  if ( iCambios = 0 ) and ( rActualAnual = 0 ) then
                  begin
                       //ParamAsFloat( oInsAcAnual, 'AC_ANUAL', rTotalAnual );
                       ParamAsFloat( oInsAcAnual, 'AC_MES_13', rTotalAnual - rActualAnual );
                       ParamAsInteger( oInsAcAnual, 'CO_NUMERO', iConcepto );
                       ParamAsInteger( oInsAcAnual, 'CB_CODIGO', iEmpleado );
                       iCambios:= Ejecuta( oInsAcAnual );
                  end;
             end;
        end;
   end;

   procedure PreparaActAnuales;
   begin
        with oZetaProvider do
        begin
             oUpdAcAnual := CreateQuery ( K_UPDATE_ANUAL );
             ParamAsInteger( oUpdAcAnual, 'AC_YEAR', ParamList.ParamByName('Year').AsInteger );

             oInsAcAnual := CreateQuery ( K_INSERT_ANUAL );
             ParamAsInteger( oInsAcAnual, 'AC_YEAR', ParamList.ParamByName('Year').AsInteger );
        end;
   end;

   procedure DespreparaActAnuales;
   begin
        FreeAndNil( oUpdAcAnual );
   end;

   function GetTopePorConcepto: String;
   begin
        with oZetaProvider.ParamList do
        begin
             if StrLleno ( ParamByName('CoTope').AsString ) then
                Result:= Format( K_LIMITE_CONCEPO, [ 'AND NOT', ParamByName('CoTope').AsString ] );
        end;
   end;

   procedure ActualizaTopes;
   begin
        with oZetaProvider do
        begin
             with ParamList do
             begin
                  oUpdTopeAnual:= CreateQuery ( Format( K_UPDATE_TOPE, [ParamByName('Year').AsInteger,
                                                                        Format( K_LIMITE_CONCEPO, ['AND', ParamByName('CoPercepciones').AsString ] ),
                                                                         GetTopePorConcepto  ] ) );

                  Ejecuta( oUpdTopeAnual );
                  FreeAndNil(oUpdTopeAnual);
             end;


        end;
   end;


begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
          cdsLista.Lista:= Lista;
     end;

     PreparaActAnuales;
     try
        { Percepciones }
        with oZetaProvider do
        begin
             EmpiezaTransaccion;
             try
                with cdsLista do
                begin
                     First;
                     while not Eof do
                     begin
                          iEmpleado:= FieldByName('EMPLEADO').AsInteger;

                          with ParamList do
                          begin
                               sExentos:= ParamByName('CoPTU').AsString + ParamByName('CoAguinaldo').AsString + ParamByName('CoPrimaVaca').AsString;

                               ParamAsInteger( oUpdAcAnual, 'CB_CODIGO', iEmpleado );

                               ActualizaAnual( 1000, FieldByName('TOT_PER').AsFloat, FieldByName('C_TOT_PER').AsFloat, ParamByName('CoPercepciones').AsString );
                               {ActualizaAnual( 1001, FieldByName('TOT_DED').AsFloat, FieldByName('C_TOT_DED').AsFloat, ParamByName('CoDeducciones').AsString );
                               ActualizaAnual( 1005, FieldByName('TOT_MENS').AsFloat, FieldByName('C_TOT_MENS').AsFloat, ParamByName('CoPerMens').AsString ); }
                               ActualizaAnual( 1006, FieldByName('EXENTO_MENSUAL').AsFloat, FieldByName('C_EXENTO_MENSUAL').AsFloat, sExentos );
                               ActualizaAnual( 1003, FieldByName('EXENTO_ISPT').AsFloat, FieldByName('C_EXENTO_ISPT').AsFloat, sExentos );
                               ActualizaAnual( 1009, FieldByName('EXENTO_INDIVIDUAL').AsFloat, FieldByName('C_EXENTO_INDIVIDUAL').AsFloat, sExentos );
                               ActualizaAnual( StrAsInteger( ParamByName('GuardaPagoSepara').AsString ), FieldByName('PAGOS_X_SEPARA').AsFloat, FieldByName('C_PAGOS_X_SEPARA').AsFloat, ParamByName('GuardaPagoSepara').AsString );


                          end;
                          Next;
                     end;

                     if ( ParamList.ParamByName('ToparNegativos').AsBoolean ) then
                        ActualizaTopes;
                end;
                TerminaTransaccion(TRUE);
             except
                   on Error: Exception do
                   begin
                        RollBackTransaccion;
                        Raise;
                   end;
             end;
        end;

     finally
            DespreparaActAnuales;
     end;

     SetComplete;
end;

function TdmServerAnualNomina.RecalculaTotPerGetLista(Empresa, Parametros: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Empresa;
          AsignaParamList( Parametros );
     end;
     Result:= RecalculaTotPerGetDataTotales;
     SetComplete;
end;

function TdmServerAnualNomina.RecalculaTotPerGetDataTotales: OleVariant;
var
   sSQL, sExentos: String;
const
    {K_COLUMNA_ACUMULA = '( SELECT SUM( AC_ANUAL ) FROM ACUMULA WHERE ( AC_YEAR = %d ) AND ( CO_NUMERO IN ( %s ) ) %s' +
                         'AND ( COLABORA.CB_CODIGO = CB_CODIGO ) )';}

     K_COLUMNA_ACUMULA = '( SELECT SUM( AC_MES_01 + AC_MES_02 + AC_MES_03 + AC_MES_04 + ' +
                         ' AC_MES_05 + AC_MES_06 + AC_MES_07 + AC_MES_08 + AC_MES_09 + '+
                         ' AC_MES_10 + AC_MES_11 + AC_MES_12 + AC_MES_13 '+
                         ' ) FROM ACUMULA WHERE ( AC_YEAR = %d ) AND ( CO_NUMERO IN ( %s ) ) %s' +
                         'AND ( COLABORA.CB_CODIGO = CB_CODIGO ) )';

     K_COLUMNA_MOVIMIEN = '( SELECT SUM (MO_IMP_CAL) FROM MOVIMIEN ' +
                          'LEFT OUTER JOIN PERIODO on ( PERIODO.PE_YEAR = MOVIMIEN.PE_YEAR ) and ( PERIODO.PE_TIPO = MOVIMIEN.PE_TIPO ) and ' +
                          '( PERIODO.PE_NUMERO = MOVIMIEN.PE_NUMERO  ) ' +
                          'WHERE ( MOVIMIEN.PE_YEAR = %0:d AND MOVIMIEN.CB_CODIGO = COLABORA.CB_CODIGO  AND ' +
                          'MOVIMIEN.CO_NUMERO = %1:d and PERIODO.PE_STATUS = 6 ) )';

     K_FILTRO_TOPE = 'AND ( ( ( AC_MES_01 + AC_MES_02 + AC_MES_03 + AC_MES_04 + AC_MES_05 + AC_MES_06 + AC_MES_07 + AC_MES_08 + AC_MES_09 + AC_MES_10 + ' +
                     'AC_MES_11 + AC_MES_12 + AC_MES_13 ) > 0 ) %s ) ';

     K_FILTRO_TOPO_X_CONCEPTO = 'OR ( CO_NUMERO IN (%s) )';

     K_COLUMNA_EXENTOS = '( select SUM(MO_X_ISPT) from MOVIMIEN ' +
                         ' LEFT OUTER JOIN PERIODO on ( PERIODO.PE_YEAR = MOVIMIEN.PE_YEAR ) and ( PERIODO.PE_TIPO = MOVIMIEN.PE_TIPO ) and ' +
                         '( PERIODO.PE_NUMERO = MOVIMIEN.PE_NUMERO  ) ' +
                         ' WHERE ( MOVIMIEN.PE_YEAR = %0:d ) AND ( COLABORA.CB_CODIGO = CB_CODIGO ) ' +
                         ' AND ( CO_NUMERO IN ( %2:s ) ) AND NOT ( CO_NUMERO IN ( %3:s ) )  ' +
                         ' AND ( MO_PER_CAL = %1:s ) AND ( MO_X_ISPT <> 0 ) AND PERIODO.PE_STATUS = 6 )';

     K_COLUMNA_EXENTO_ACTUAL = '( select SUM(MO_X_ISPT) from MOVIMIEN ' +
                               ' LEFT OUTER JOIN PERIODO on ( PERIODO.PE_YEAR = MOVIMIEN.PE_YEAR ) and ( PERIODO.PE_TIPO = MOVIMIEN.PE_TIPO ) and ' +
                               '( PERIODO.PE_NUMERO = MOVIMIEN.PE_NUMERO  ) ' +
                               'where CO_NUMERO in (%0:s) and MOVIMIEN.PE_YEAR = %1:d AND CB_CODIGO = COLABORA.CB_CODIGO AND PERIODO.PE_STATUS = 6 )';


     K_CO_TOT_PER = '1000';
     K_CO_TOT_DED = '1001';
     //K_CO_TOT_MENS = '1005';
     K_CO_EXENTO_MENSUAL = '1006';
     K_CO_EXENTO_INDIVIDUAL = '1009';
     K_CO_EXENTO_ISPT = '1003';

   function GetColumnaAcumula( const sConceptos: String; const sFiltroTope: String = VACIO ): String;
   begin
        if StrVacio ( sConceptos ) then
           Result:= '0'
        else
            Result:= Format( K_COLUMNA_ACUMULA, [ oZetaProvider.ParamList.ParamByName('Year').AsInteger, sConceptos, sFiltroTope ]);
   end;

   function GetColumnaExentoActual( const sConceptos: String ): String;
   begin
        if StrVacio ( sConceptos ) then
           Result:= '0'
        else
            Result:= Format( K_COLUMNA_EXENTO_ACTUAL, [ sConceptos, oZetaProvider.ParamList.ParamByName('Year').AsInteger ]);
   end;

   procedure AgregaColumnaColabora( sCampo, sAlias: String );
   begin
        if ( StrVacio ( sCampo ) ) then
           sCampo:= '0';

        sSQL:= sSQL + ConcatString( sCampo,sAlias,' ') + ',';
   end;

   procedure AgregaOrdenColabora( sOrden: String );
   begin
        sSQL:= sSQL + 'ORDER BY ' + sOrden;
   end;

   procedure AgregaFiltroColabora( sFiltro: String );
   begin
        {if( Pos('WHERE', sSQL ) > 0 ) then
           sSQL:= sSQL+ ' AND' + sFiltro
        else }
        sSQL:= sSQL + 'WHERE '+ sFiltro;
   end;

   function GetFiltroTope: String;
   var
      sTopexConcepto: String;
   begin
        with oZetaProvider.ParamList do
        begin
             if ( ParamByName('ToparNegativos').AsBoolean ) then
             begin
                  if StrLleno(ParamByName('CoTope').AsString ) then
                     sTopexConcepto:= Format( K_FILTRO_TOPO_X_CONCEPTO, [ParamByName('CoTope').AsString] );
                  Result:= Format( K_FILTRO_TOPE, [sTopexConcepto] );
             end;
        end;
   end;

   function GetColumnaExentos( const sPercepciones, sExentoExcluidos: String; lIndividual: Boolean ): String;
   begin
        with oZetaProvider.ParamList do
        begin
             if StrLleno ( sPercepciones ) and StrLleno ( sExentoExcluidos )  then
             begin
                  Result:= Format( K_COLUMNA_EXENTOS ,[ ParamByName('Year').AsInteger, EntreComillas( zBoolToStr( lIndividual ) ),
                                                        sPercepciones, sExentoExcluidos ]);
             end
             else
                 Result:= '0';
        end;
   end;


begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               sExentos:= ConcatString( ParamByName('CoPrimaVaca').AsString,
                                        ConcatString( ParamByName('CoPTU').AsString, ParamByName('CoAguinaldo').AsString, ',' ), ',') ;


               sSQL:= 'SELECT ';
               AgregaColumnaColabora( 'CB_CODIGO', 'CB_CODIGO' );
               AgregaColumnaColabora( K_PRETTYNAME, 'PrettyName' );
               AgregaColumnaColabora( GetColumnaAcumula( ParamByName('CoPercepciones').AsString, GetFiltroTope ), 'C_TOT_PER');
               AgregaColumnaColabora( GetColumnaAcumula( K_CO_TOT_PER ), 'TOT_PER');
               {AgregaColumnaColabora( GetColumnaAcumula( ParamByName('CoDeducciones').AsString ), 'C_TOT_DED');
               AgregaColumnaColabora( GetColumnaAcumula( K_CO_TOT_DED ), 'TOT_DED');
               AgregaColumnaColabora( GetColumnaAcumula( ParamByName('CoPerMens').AsString, GetFiltroTope ), 'C_TOT_MENS');
               AgregaColumnaColabora( GetColumnaAcumula( K_CO_TOT_MENS ), 'TOT_MENS');  }

               AgregaColumnaColabora( GetColumnaExentoActual( ParamByName('CoPTU').AsString ),  'C_PTU');
               AgregaColumnaColabora( GetColumnaExentoActual( ParamByName('CoAguinaldo').AsString ),  'C_AGUINALDO');
               AgregaColumnaColabora( GetColumnaExentoActual( ParamByName('CoPrimaVaca').AsString ),  'C_PRIMA_VACA');

               if ( StrLleno ( ParamByName('TomaPagoSepara').AsString ) ) then
               begin
                    AgregaColumnaColabora( GetColumnaAcumula( ParamByName('GuardaPagoSepara').AsString ), 'PAGO_X_SEPARA');
                    AgregaColumnaColabora( Format( K_COLUMNA_MOVIMIEN, [ParamByName('Year').AsInteger,
                                                                        ParamByName('TomaPagoSepara').AsInteger ]), 'C_PAGO_X_SEPARA');
               end
               else
               begin
                    AgregaColumnaColabora( VACIO, 'PAGO_X_SEPARA');
                    AgregaColumnaColabora( VACIO, 'C_PAGO_X_SEPARA');
               end;

               { Exento ISPT Individual }
               AgregaColumnaColabora( GetColumnaAcumula( K_CO_EXENTO_INDIVIDUAL ), 'EXENTO_INDIVIDUAL');
               AgregaColumnaColabora( GetColumnaExentos( ParamByName('CoPercepciones').AsString, sExentos, TRUE ), 'C_EXENTO_INDIVIDUAL');
               { Exento Mensual }
               AgregaColumnaColabora( GetColumnaAcumula( K_CO_EXENTO_MENSUAL ), 'EXENTO_MENSUAL');
               AgregaColumnaColabora( GetColumnaExentos( ParamByName('CoPerMens').AsString, sExentos, FALSE ), 'C_EXENTO_MENSUAL');
               {Exento ISPT}
               AgregaColumnaColabora( GetColumnaAcumula( K_CO_EXENTO_ISPT ), 'EXENTO_ISPT');

               AgregaColumnaColabora( GetColumnaExentos( ParamByName('CoPercepciones').AsString, ConcatString( sExentos, ParamByName('CoPerMens').AsString,''), FALSE ), 'C_EXENTO_ISPT');

               sSQL:= CortaUltimo( sSQL ) + ' FROM COLABORA ';

               AgregaFiltroColabora( Format( '( CB_CODIGO = %d )', [ ParamByName('Empleado').AsInteger ] )  );

               Result:= oZetaProvider.OpenSQL( EmpresaActiva, sSQL, True );
          end;
     end;
end;

{ *********  *********  *********  *********  *********  *********  *********  ********* }
{ *********  *********  *********  Proceso de Calculo Previo de ISR *********  ********* }

procedure TdmServerAnualNomina.PrevioISRParametros;
begin
     with oZetaProvider, ParamList do
     begin
          FListaParametros := VACIO;
          FListaParametros := 'A�o: ' + ParamByName( 'Year' ).AsString +
                           K_PIPE + 'Mes: ' + ParamByName( 'Mes' ).AsString +
                           K_PIPE + 'Rastrear: ' + ZetacommonTools.zBoolToStr( ParamByName( 'Rastrear' ).AsBoolean ) +
                           K_PIPE + 'Empleados Repetidos: ' + ZetaCommonLists.ObtieneElemento( lfEmpleadoRepetido, ParamByName( 'Repetidos' ).AsInteger );
          FListaFormulas :=    VACIO;
          FListaFormulas :=    K_PIPE + 'F�rmulas' +
                     K_PIPE + 'Ingreso Bruto: ' + ParamByName( 'IngresoBruto' ).AsString +
                     K_PIPE + 'Ingreso Exento: ' + ParamByName( 'IngresoExento' ).AsString +
                     K_PIPE + 'Impuesto Retenido: ' + ParamByName( 'ImpuestoRetenido' ).AsString +
                     K_PIPE + 'SUBE Aplicado: ' + ParamByName( 'SUBEAplicado' ).AsString +
                     K_PIPE + 'C�lculo Impuesto: ' + ParamByName( 'CalculoImpuesto' ).AsString+
                     K_PIPE + 'Empleado a Calcular: ' + ParamByName( 'EmpleadosACalcular' ).AsString;
     end;
end;

function TdmServerAnualNomina.PrevioISR(Empresa, Parametros: OleVariant): OleVariant;
begin
     InitBroker(True);
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaParamList( Parametros );
             GetDatosPeriodo;
             GetDatosImss;
             GetDatosActivos;
             InitGlobales;
        end;
        PrevioISRParametros;
        PrevioISRBuildDataset;
        Result := PrevioISRDataset( SQLBroker.SuperReporte.DataSetReporte );
     finally
            ClearBroker;
     end;
     SetComplete;
end;

procedure TdmServerAnualNomina.PrevioISRBuildDataSet;
var
   iYear, iMonth : integer;
   sIngresoBruto, sIngresoExento, sImpuestoRetenido, sSUBEAplicado, sCalculoImpuesto, sEmpleadosACalcular: string;
   lRastrear: Boolean;
   eRepetidos : eEmpleadoRepetido;

   function GetFechaLimiteBaja: String;
   begin
        { Al validar empleados repetidos tambien debe incluir las bajas en el a�o como parte de su evaluaci�n }
        if ( eRepetidos in [ erIncluirTodos ] ) then
        begin
             Result:= ZetaCommonTools.FechaAsStr( EncodeDate( iYear, 12, 1 ) );
        end
        else
        begin
             Result:= ZetaCommonTools.FechaAsStr( FirstDayOfYear( iYear ) );
        end;
   end;

begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               iYear := ParamByName( 'Year' ).AsInteger;
               iMonth := ParamByName( 'Mes' ).AsInteger;
               sIngresoBruto := SetVacioACero( ParamByName( 'IngresoBruto' ).AsString );
               sIngresoExento := SetVacioACero( ParamByName( 'IngresoExento' ).AsString );
               sImpuestoRetenido := SetVacioACero( ParamByName( 'ImpuestoRetenido' ).AsString );
               sSUBEAplicado := SetVacioACero( ParamByName( 'SUBEAplicado' ).AsString );
               sCalculoImpuesto := SetVacioACero( ParamByName( 'CalculoImpuesto' ).AsString );
               sEmpleadosACalcular := SetVacioABooleano( ParamByName( 'EmpleadosACalcular' ).AsString, TRUE );
               lRastrear := ParamByName( 'Rastrear' ).AsBoolean;
               eRepetidos := eEmpleadoRepetido( ParamByName( 'Repetidos' ).AsInteger );
          end;

          YearDefault := iYear;
          MesDefault := iMonth;

          with SQLBroker do
          begin
               if lRastrear then
                  oZetaCreator.PreparaRastreo;
               try
                  if lRastrear then
                     SuperReporte.OnEvaluaColumna := SetRastreo;
                  try
                     with Agente do
                     begin
                          //Init( enEmpleado );
                          //AgregaColumna( 'COLABORA.CB_CODIGO', True, Entidad, tgNumero, 10, 'CB_CODIGO' );
                          FiltrosAnualesInit;
                          FiltrosAnualesColumnas;
                          {Las dos primeras columnas: INGRESO_BRUTO e INGRESO_EXENTO, se utilizan
                          en las funciones de TZetaISPTAnualMonto y TZetaISPTAnualEXENTO de ZFuncsAnuales,
                          por lo que NO SE PUEDEN mover de POSICION, por que se hace referencia a ellas por
                          POSICION}
                          AgregaColumna( sIngresoBruto, False, Entidad, tgFloat, 10, 'INGRESO_BRUTO' );
                          AgregaColumna( sIngresoExento, False, Entidad, tgFloat, 10, 'INGRESO_EXENTO' );

                          AgregaColumna( sImpuestoRetenido, False, Entidad, tgFloat, 10, 'IMPUESTO_RETENIDO' );
                          AgregaColumna( sSUBEAplicado, False, Entidad, tgFloat, 10, 'SUBE_APLICADO' );
                          AgregaColumna( sCalculoImpuesto, False, Entidad, tgFloat, 10, 'CALCULO_IMPUESTO' );
                          AgregaColumna( K_PRETTYNAME, TRUE, enEmpleado, tgTexto, 50, 'PrettyName' );
                          AgregaColumna( 'COLABORA.CB_FEC_ING', True, Entidad, tgFecha, 10, 'CB_FEC_ING' );
                          AgregaColumna( Format( 'FECHA_KARDEX("KARDEX.CB_SALARIO","%s")', [ ZetaCommonTools.FechaAsStr( EncodeDate( iYear, 12, 31 ) ) ] ), False, Entidad, tgFloat, 10, 'CB_SALARIO' );
                          if lRastrear then
                             AgregaColumna( '@0', FALSE, Entidad, tgMemo, 0, 'RASTREO' );
                          AgregaColumna( '0', True, enEmpleado, tgNumero, 10, 'EMP_REPETIDO' );
                          AgregaColumna( '0', True, enEmpleado, tgNumero, 10, 'FECHA_REPETIDA' );
                          AgregaColumna( 'COLABORA.CB_FEC_BAJ', True, Entidad, tgFecha, 10, 'CB_FEC_BAJ' );
                          AgregaColumna( 'VALIDA_RFC', FALSE, enNinguno, tgTexto, K_ANCHO_DESCCORTA, 'RFC_VALIDADO' );
                          AgregaColumna( sEmpleadosACalcular, False, Entidad, tgBooleano, 10, 'EMPLEADO_CALCULADO' );
                          AgregaColumna( Format( '@CASE WHEN ( COLABORA.CB_FEC_BAJ < COLABORA.CB_FEC_ING ) or ( CB_FEC_BAJ >= ''%s'' ) THEN ''S'' ELSE ''N'' END', [ GetFechaLimiteBaja ] ), FALSE, enEmpleado, tgTexto, 1, 'EMPLEADO_ACTIVO' );

                          //AgregaFiltro( sEmpleadosACalcular, FALSE, Entidad );
                          AgregaFiltro( 'COLABORA.CB_CODIGO is NOT NULL', TRUE, Entidad );
                          //AgregaOrden( 'COLABORA.CB_CODIGO', TRUE, Entidad );
                     end;
                     AgregaRangoCondicion( ParamList );
                     FiltroConfidencial( EmpresaActiva );
                     oZetaCreator.SuperReporte := SQLBroker.SuperReporte;
                     BuildDataset( EmpresaActiva );
                     {$Ifdef CAROLINA}
                     Agente.SQL.SaveTofile('d:\temp\isr.sql');
                     {$endif}
                  finally
                         if lRastrear then
                            SuperReporte.OnEvaluaColumna := nil;
                  end;
               finally
                      if lRastrear then
                         oZetaCreator.DesPreparaRastreo;
               end;
          end;
     end;
end;

function TdmServerAnualNomina.PrevioISRDataset(Dataset: TDataset): OleVariant;
const
     K_POS_PRIMER_CAMPO_A_SUMAR = 1;
     K_POS_ULTIMO_CAMPO_A_SUMAR = 5;
var
   dInicial, dIngreso: TDate;
   iEmpleado: TNumEmp;
   iYear, iMes: Integer;
   rSalario, rIngBruto, rIngExento, rGravado, rImpRetenido, rSUBEAplicado, rCalcImpto, rAFavor, rEnContra, rISRCargo: TPesos;
   FPrevioISR: TZetaCursor;
   lRastrear: Boolean;
   sRastro, sMensaje: String;
   lCalcularISPT, lEmpleadoActivo: Boolean;
   eRepetidos: eEmpleadoRepetido;
   ImpuestoEval : TZetaEvaluador;
   FRes : TQREvResult;
   aCampos:  array[ K_POS_PRIMER_CAMPO_A_SUMAR..K_POS_ULTIMO_CAMPO_A_SUMAR ] of TPesos;

   function Rastreador: TRastreador;
   begin
        Result := oZetaCreator.Rastreador;
   end;

   procedure CalculaImpuestoEmpleadosRepetidos;
   begin
        ImpuestoEval := TZetaEvaluador.Create( oZetaCreator );
        ImpuestoEval.AgregaDataset( TdmSuperReporte( oZetaCreator.SuperReporte ).QueryReporte );
   end;

   function PosicionaDatasetEval : Boolean;
   begin
        with TdmSuperReporte( oZetaCreator.SuperReporte ).QueryReporte do
        begin
             {$ifdef DOS_CAPAS}
             First; //Se hace el First, porque no hay seguridad de que el apuntador se queda en donde lo deje
             while not EOF and ( FieldByName('CB_CODIGO').AsInteger <> iEmpleado ) do
                   Next;
             Result := FieldByName('CB_CODIGO').AsInteger = iEmpleado
             {$else}
             Result := Locate('CB_CODIGO', iEmpleado, [] ) ;
             {$endif}
        end;
   end;

begin
     with oZetaProvider do
     begin
          with ParamList do
          begin
               iYear := ParamByName( 'Year' ).AsInteger;
               iMes := ParamByName( 'Mes' ).AsInteger;
               dInicial := ZetaCommonTools.FirstDayOfYear( iYear );
               lRastrear := ParamByName( 'Rastrear' ).AsBoolean;
               eRepetidos := eEmpleadoRepetido( ParamByName( 'Repetidos' ).AsInteger );
          end;
          FOffsetCampos := 1; //Es el offset, para que no tome en cuenta el primer campo
          YearDefault := iYear;
          MesDefault := iMes;

          if OpenProcess( prNOPrevioISR, Dataset.RecordCount, FListaParametros, FListaFormulas ) then
          begin
               ProcesoAnualValidaRepetidos( TServerDataSet( Dataset ), eRepetidos, aCampos, EncodeDate( iYear, 12, 1 ) );
               try
                  FPrevioISR := CreateQuery( GetSQLScript( Q_PREVIO_ISR_BORRAR ) );
                  try
                     EmpiezaTransaccion;
                     try
                        ParamAsInteger( FPrevioISR, 'IS_YEAR', iYear );
                        ParamAsInteger( FPrevioISR, 'IS_MES', iMes );
                        Ejecuta( FPrevioISR );
                        TerminaTransaccion( True );
                     except
                           on Error: Exception do
                           begin
                                TerminaTransaccion( False );
                                raise;
                           end;
                     end;
                     PreparaQuery( FPrevioISR, GetSQLScript( Q_PREVIO_ISR_INSERT ) );  { Agrega El resultado de Un Empleado para un a�o y para un mes}
                     ParamAsInteger( FPrevioISR, 'IS_YEAR', iYear );
                     ParamAsInteger( FPrevioISR, 'IS_MES', iMes );
                     ParamAsInteger( FPrevioISR, 'US_CODIGO', UsuarioActivo );
                     if lRastrear then
                        oZetaCreator.PreparaRastreo;
                     if (eRepetidos in [erSumarUltimo, erSumarPrimero] ) then
                        CalculaImpuestoEmpleadosRepetidos
                     else
                       ImpuestoEval := Nil;
                     try
                        with Dataset do
                        begin
                             First;
                             while not Eof and CanContinue( FieldByName( 'CB_CODIGO' ).AsInteger ) do
                             begin
                                  iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                                  rSalario := FieldByName( 'CB_SALARIO' ).AsFloat;
                                  dIngreso := FieldByName( 'CB_FEC_ING' ).AsDateTime;
                                  lCalcularISPT := ( FieldByName( 'EMPLEADO_CALCULADO' ).AsString = K_BOOLEANO_SI );
                                  lEmpleadoActivo := zStrToBool( FieldByName( 'EMPLEADO_ACTIVO' ).AsString );

                                  rIngBruto := ZetaCommonTools.Redondea( FieldByName( 'INGRESO_BRUTO' ).AsFloat );
                                  rIngExento := ZetaCommonTools.Redondea( FieldByName( 'INGRESO_EXENTO' ).AsFloat );
                                  rGravado := rIngBruto - rIngExento;
                                  rImpRetenido := ZetaCommonTools.Redondea( FieldByName( 'IMPUESTO_RETENIDO' ).AsFloat );
                                  rSUBEAplicado := ( -1 * ZetaCommonTools.Redondea( FieldByName( 'SUBE_APLICADO' ).AsFloat ) ); { Debe ser Negativo }

                                  //Aqui se Calcula el Impuesto sobre el Monto SUMADO, cuando se repiten los empleados.
                                  if (eRepetidos in [erSumarUltimo, erSumarPrimero] ) and
                                     ( FieldByName('EMP_REPETIDO').AsInteger <> 0 ) then
                                  begin
                                       rCalcImpto := 0;
                                       if PosicionaDataSetEval then
                                       begin
                                            if ImpuestoEval.Rastreando then
                                               ImpuestoEval.Rastreador.RastreoBegin; //CV: Si no se limpia se queda con el del empleado anterior y lo va acumulando.
                                            FRes := ImpuestoEval.Calculate( SetVacioACero( ParamList.ParamByName( 'CalculoImpuesto' ).AsString ) );

                                            with FRes do
                                            begin
                                                 if ( Kind = resDouble ) then
                                                      rCalcImpto := ZetaCommonTools.Redondea( dblResult )
                                                 else if ( Kind = resInt ) then
                                                      rCalcImpto := intResult
                                                 else if ( Kind = resError ) then
                                                      Log.Error( iEmpleado, 'Error en la f�rmula del Impuesto' ,FRes.StrResult );
                                            end;
                                            if ImpuestoEval.Rastreando then
                                               sRastro := ImpuestoEval.Rastreador.Rastro;
                                       end
                                       else
                                       begin
                                            sRastro := VACIO;
                                            Log.Error( iEmpleado, 'Error al Calcular el Impuesto' , 'El Empleado no se encontr� en la Lista Original' );
                                       end;
                                  end
                                  else
                                  begin
                                       rCalcImpto := ZetaCommonTools.Redondea( FieldByName( 'CALCULO_IMPUESTO' ).AsFloat );
                                       if lRastrear then
                                          sRastro := FieldByName( 'RASTREO' ).AsString;
                                  end;

                                  if lRastrear then
                                  begin
                                       with Rastreador do
                                       begin
                                            RastreoBegin;
                                            RastreoHeader( 'RASTREO DEL CALCULO PREVIO DE ISR' );
                                            RastreoTextoLibre( PadC( Format( 'Empleado : %d = %s', [ iEmpleado, FieldByName( 'PrettyName' ).AsString ] ), 70 ) );
                                            if ZetaCommonTools.StrLleno( sRastro ) then
                                            begin
                                                 RastreoHeader( 'FORMULAS:' );
                                                 AgregaTexto( sRastro );
                                                 RastreoEspacio;
                                            end;
                                            RastreoHeader('MONTOS CONSIDERADOS' );
                                            RastreoFecha( '                      Primer D�a del A�o: ', dInicial );
                                            RastreoFecha( '                        Fecha de Ingreso: ', dIngreso );
                                            RastreoPesos( '                          Salario Diario: ', rSalario );
                                            RastreoEspacio;
                                            RastreoPesos( '                   Total de Percepciones: ', rIngBruto );
                                            RastreoPesos( '                          Ingreso Exento: ', rIngExento );
                                            RastreoPesos( '                   Ingreso Anual Gravado: ', rGravado );
                                            RastreoPesos( '                 Impuesto Anual Retenido: ', rImpRetenido );
                                            RastreoPesos( '                     SUBE Anual Aplicado: ', rSUBEAplicado );
                                            RastreoPesos( '                     Excedente ISR Anual: ', rCalcImpto );

                                            RastreoEspacio;
                                            RastreoHeader( 'DE ACUERDO A FRACCIONES II y III DEL ARTICULO 116' );
                                       end;
                                  end;
                                  rAFavor := 0;
                                  rEnContra := 0;
                                  begin
                                       { Cambios a las reglas de subsidio en el 2003 }
                                       { Nunca se va a tener rAPagar > 0 ( Art�culo 116, Fraccion III }
                                       { Topar ISR a Cargo a 0 � a la diferencia entre Impuesto Calculado y CAS}
                                       { NOTA: Se suma dado que rCredPagado es negativo (ver c�digo arriba) }
                                       rISRCargo := ZetaCommonTools.rMax( 0, ( rCalcImpto - rSUBEAplicado ) );
                                       if lRastrear then
                                       begin
                                            with Rastreador do
                                            begin
                                                 RastreoPesos( '                     Excedente ISR Anual: ', rCalcImpto );
                                                 RastreoPesos( '(-)                  SUBE Anual Aplicado: ', rSUBEAplicado );
                                                 if ( rCalcImpto < rSUBEAplicado ) then
                                                 begin
                                                      RastreoMsg( 'ARTICULO 116, FRACCION III:' );
                                                 end;
                                                 RastreoPesos( '(=)                     Impuesto a Cargo: ', rISRCargo );
                                                 RastreoPesos( '(-)        Retenciones ISR Provisionales: ', rImpRetenido );
                                            end;
                                       end;
                                       if ( rISRCargo > rImpRetenido ) then
                                       begin
                                            rEnContra := ZetaCommonTools.Redondea( rISRCargo - rImpRetenido );
                                            if lRastrear then
                                            begin
                                                 with Rastreador do
                                                 begin
                                                      RastreoPesos( '(=)                          ISR A Cargo: ', rEnContra );
                                                 end;
                                            end;
                                       end
                                       else
                                       begin
                                            rAFavor := ZetaCommonTools.Redondea( rImpRetenido - rISRCargo );
                                            if lRastrear then
                                            begin
                                                 with Rastreador do
                                                 begin
                                                      RastreoPesos( '(=)                          ISR A Favor: ', rAFavor );
                                                 end;
                                            end;
                                       end;
                                  end;
                                  if ( lCalcularISPT and lEmpleadoActivo ) then
                                  begin
                                       EmpiezaTransaccion;
                                       try
                                          ParamAsInteger( FPrevioISR, 'CB_CODIGO', iEmpleado );
                                          ParamAsFloat( FPrevioISR, 'CB_SALARIO', rSalario );
                                          ParamAsFloat( FPrevioISR, 'IS_ING_BR', rIngBruto );
                                          ParamAsFloat( FPrevioISR, 'IS_GRAVADO', rGravado );
                                          ParamAsFloat( FPrevioISR, 'IS_SUBE', rSUBEAplicado );
                                          ParamAsFloat( FPrevioISR, 'IS_RETENID', rImpRetenido );
                                          ParamAsFloat( FPrevioISR, 'IS_IMPCALC', rCalcImpto );
                                          ParamAsFloat( FPrevioISR, 'IS_FAVOR', rAFavor  );
                                          ParamAsFloat( FPrevioISR, 'IS_CONTRA', rEnContra );
                                          Ejecuta( FPrevioISR );
                                          TerminaTransaccion( True );
                                       except
                                              on Error: Exception do
                                              begin
                                                   TerminaTransaccion( False );
                                                   Log.Excepcion( iEmpleado, 'Error Al Realizar C�lculo Previo de ISR ', Error, DescripcionParams );
                                              end;
                                        end;
                                  end;

                                  if lCalcularISPT and lEmpleadoActivo then
                                  begin
                                       if lRastrear then
                                       begin
                                            sMensaje := 'Rastreo de C�lculo Previo de ISR: Mes %d de %d';
                                            sRastro := Rastreador.Rastro;
                                            if ZetaCommonTools.StrLleno( sRastro ) then
                                               Log.Evento( clbNinguno, iEmpleado, Date, Format( sMensaje, [ iMes, iYear ] ), sRastro );
                                       end;
                                  end
                                  else
                                  begin
                                       sMensaje := Format( 'No procede C�lculo Previo de ISR: Mes %d de %d', [ iMes, iYear ] );
                                       if NOT lCalcularISPT then
                                          Log.Advertencia( clbNinguno, iEmpleado, Date, sMensaje,
                                          'El empleado no tiene c�lculo debido a que no cumple con el filtro de "Empleados a Calcular"' )
                                       else
                                          Log.Advertencia( clbNinguno, iEmpleado, Date, sMensaje,
                                          'El empleado no tiene c�lculo debido a que no tiene la antig�edad suficiente' );
                                  end;

                                  Next;
                             end;
                        end;
                     finally
                            FreeAndNil( ImpuestoEval );
                            if lRastrear then
                               oZetaCreator.DesPreparaRastreo;
                     end;
                  finally
                         FreeAndNil( FPrevioISR );
                  end;
               except
                     on Error: Exception do
                     begin
                          Log.Excepcion( 0, 'Error al Realizar C�lculo Previo de ISR', Error, DescripcionParams );
                     end;
               end;
          end;
          Result := CloseProcess;
     end;
end;

{$ifndef DOS_CAPAS}
initialization
  TComponentFactory.Create( ComServer, TdmServerAnualNomina, Class_dmServerAnualNomina,
                            ciMultiInstance, {$ifdef INTERBASE}tmApartment{$endif}
                            {$ifdef MSSQL}ZetaServerTools.GetThreadingModel{$endif});
{$endif}
end.


