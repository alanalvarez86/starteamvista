unit Asistencia_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 9/4/2017 10:53:54 AM from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3win_30_Vsn_2017\MTS\Asistencia.tlb (1)
// LIBID: {A89D2232-88C6-11D3-8BBD-0050DA04EAC5}
// LCID: 0
// Helpfile: 
// HelpString: Tress Asistencia
// DepndLst: 
//   (1) v1.0 Midas, (C:\Program Files (x86)\Embarcadero\RAD Studio\12.0\bin64\midas.dll)
//   (2) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, Midas, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  AsistenciaMajorVersion = 1;
  AsistenciaMinorVersion = 0;

  LIBID_Asistencia: TGUID = '{A89D2232-88C6-11D3-8BBD-0050DA04EAC5}';

  IID_IdmServerAsistencia: TGUID = '{A89D2233-88C6-11D3-8BBD-0050DA04EAC5}';
  CLASS_dmServerAsistencia: TGUID = '{A89D2235-88C6-11D3-8BBD-0050DA04EAC5}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerAsistencia = interface;
  IdmServerAsistenciaDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerAsistencia = IdmServerAsistencia;


// *********************************************************************//
// Interface: IdmServerAsistencia
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A89D2233-88C6-11D3-8BBD-0050DA04EAC5}
// *********************************************************************//
  IdmServerAsistencia = interface(IAppServer)
    ['{A89D2233-88C6-11D3-8BBD-0050DA04EAC5}']
    function GetTarjeta(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime; 
                        out Checadas: OleVariant): OleVariant; safecall;
    function GetAutorizaciones(Empresa: OleVariant; Fecha: TDateTime; lSoloAltas: WordBool): OleVariant; safecall;
    function GetHorarioTemp(Empresa: OleVariant; Empleado: Integer; FechaIni: TDateTime; 
                            FechaFin: TDateTime): OleVariant; safecall;
    function GrabaAutorizaciones(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer; 
                                 out ErrorData: OleVariant; Operacion: Integer): OleVariant; safecall;
    function GrabaHorarioTemp(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GrabaTarjeta(Empresa: OleVariant; oDelta: OleVariant; oChecadas: OleVariant; 
                          var ErrorCount: Integer; var ErrorChCount: Integer; 
                          out Valor: OleVariant; out oNewTarjeta: OleVariant; 
                          out oNewChecadas: OleVariant): OleVariant; safecall;
    function GetCalendario(Empresa: OleVariant; Empleado: Integer; FechaIni: TDateTime; 
                           FechaFin: TDateTime): OleVariant; safecall;
    function GrabaCalendario(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function BorrarPoll(Usuario: Integer; Parametros: OleVariant): OleVariant; safecall;
    function BorrarTarjetas(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CorregirFechas(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function AutorizarHoras(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function AutorizarHorasLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function AutorizarHorasGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetHorarioStatus(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetVacaDiasGozar(Empresa: OleVariant; Empleado: Integer; FechaIni: TDateTime; 
                              FechaFin: TDateTime): Double; safecall;
    function GetVacaFechaRegreso(Empresa: OleVariant; Empleado: Integer; FechaIni: TDateTime; 
                                 DiasGozo: Double; out DiasRango: Double): TDateTime; safecall;
    function GetAutoXAprobar(Empresa: OleVariant; Fecha: TDateTime; Todos: WordBool): OleVariant; safecall;
    function GrabaAutoXAprobar(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetClasificacionTemp(Empresa: OleVariant; Fecha: TDateTime; 
                                  const Empleados: WideString; SoloAltas: WordBool): OleVariant; safecall;
    function GrabaClasificacionTemp(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetCalendarioEmpleado(Empresa: OleVariant; Empleado: Integer; FechaIni: TDateTime; 
                                   FechaFin: TDateTime): OleVariant; safecall;
    function PuedeCambiarTarjetaStatus(Empresa: OleVariant; Fecha: TDateTime; Empleado: Integer; 
                                       StatusLimite: Integer): WordBool; safecall;
    function IntercambioFestivo(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function IntercambioFestivoLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function IntercambioFestivoGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CancelarExcepcionesFestivos(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CancelarExcepcionesFestivosLista(Empresa: OleVariant; Lista: OleVariant; 
                                              Parametros: OleVariant): OleVariant; safecall;
    function CancelarExcepcionesFestivosGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function AjustarIncapaCal(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function AjustarIncapaCalLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function AjustarIncapaCalGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function AutorizacionPreNomina(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function AutorizacionPreNominaGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function AutorizacionPreNominaLista(Empresa: OleVariant; Lista: OleVariant; 
                                        Parametros: OleVariant): OleVariant; safecall;
    function GetCosteoTransferencias(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GrabaCosteoTransferencias(Empresa: OleVariant; oDelta: OleVariant; 
                                       Parametros: OleVariant; out ResultData: OleVariant; 
                                       out ErrorCount: Integer): OleVariant; safecall;
    function RegistrarExcepcionesFestivo(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function RegistrarExcepcionesFestivoLista(Empresa: OleVariant; Parametros: OleVariant; 
                                              Lista: OleVariant): OleVariant; safecall;
    function RegistrarExcepcionesFestivoGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function TransferenciasCosteo(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function TransferenciasCosteoGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function TransferenciasCosteoLista(Empresa: OleVariant; Lista: OleVariant; 
                                       Parametros: OleVariant): OleVariant; safecall;
    function CalculaCosteo(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetSuperXCentroCosto(Empresa: OleVariant): OleVariant; safecall;
    function CancelaTransferenciasCosteo(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CancelaTransferenciasCosteoGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CancelaTransferenciasCosteoLista(Empresa: OleVariant; Lista: OleVariant; 
                                              Parametros: OleVariant): OleVariant; safecall;
    function GetAlarmasSynel(Empresa: OleVariant; const Filtro: WideString): OleVariant; safecall;
    function GrabaAlarmaSynel(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetRutaReporteSynel(Emrpesa: OleVariant; Reporte: Integer): WideString; safecall;
    function GetListaReloj(Emrpesa: OleVariant): OleVariant; safecall;
    function GetClasificacionTempLista(Empresa: OleVariant; Parametros: OleVariant; 
                                       Lista: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetClasificacionTempGetLista(Empresa: OleVariant; Parametros: OleVariant; 
                                          Lista: OleVariant): OleVariant; safecall;
    function ImportarClasificacionesTemp(Empresa: OleVariant; Parametros: OleVariant; 
                                         Lista: OleVariant): OleVariant; safecall;
    function GetPermisoDiasHabiles(Empresa: OleVariant; Empleado: Integer; FechaIni: TDateTime; 
                                   FechaFin: TDateTime): Double; safecall;
    function GetPermisoFechaRegreso(Empresa: OleVariant; Empleado: Integer; FechaIni: TDateTime; 
                                    Dias: Double; out DiasRango: Double): TDateTime; safecall;
    function GetHorarioTurno(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetEstatusDia(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime): Currency; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerAsistenciaDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A89D2233-88C6-11D3-8BBD-0050DA04EAC5}
// *********************************************************************//
  IdmServerAsistenciaDisp = dispinterface
    ['{A89D2233-88C6-11D3-8BBD-0050DA04EAC5}']
    function GetTarjeta(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime; 
                        out Checadas: OleVariant): OleVariant; dispid 1;
    function GetAutorizaciones(Empresa: OleVariant; Fecha: TDateTime; lSoloAltas: WordBool): OleVariant; dispid 2;
    function GetHorarioTemp(Empresa: OleVariant; Empleado: Integer; FechaIni: TDateTime; 
                            FechaFin: TDateTime): OleVariant; dispid 3;
    function GrabaAutorizaciones(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer; 
                                 out ErrorData: OleVariant; Operacion: Integer): OleVariant; dispid 4;
    function GrabaHorarioTemp(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 5;
    function GrabaTarjeta(Empresa: OleVariant; oDelta: OleVariant; oChecadas: OleVariant; 
                          var ErrorCount: Integer; var ErrorChCount: Integer; 
                          out Valor: OleVariant; out oNewTarjeta: OleVariant; 
                          out oNewChecadas: OleVariant): OleVariant; dispid 6;
    function GetCalendario(Empresa: OleVariant; Empleado: Integer; FechaIni: TDateTime; 
                           FechaFin: TDateTime): OleVariant; dispid 7;
    function GrabaCalendario(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 8;
    function BorrarPoll(Usuario: Integer; Parametros: OleVariant): OleVariant; dispid 9;
    function BorrarTarjetas(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 10;
    function CorregirFechas(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 13;
    function AutorizarHoras(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 15;
    function AutorizarHorasLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 16;
    function AutorizarHorasGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 17;
    function GetHorarioStatus(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 19;
    function GetVacaDiasGozar(Empresa: OleVariant; Empleado: Integer; FechaIni: TDateTime; 
                              FechaFin: TDateTime): Double; dispid 11;
    function GetVacaFechaRegreso(Empresa: OleVariant; Empleado: Integer; FechaIni: TDateTime; 
                                 DiasGozo: Double; out DiasRango: Double): TDateTime; dispid 12;
    function GetAutoXAprobar(Empresa: OleVariant; Fecha: TDateTime; Todos: WordBool): OleVariant; dispid 14;
    function GrabaAutoXAprobar(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 18;
    function GetClasificacionTemp(Empresa: OleVariant; Fecha: TDateTime; 
                                  const Empleados: WideString; SoloAltas: WordBool): OleVariant; dispid 20;
    function GrabaClasificacionTemp(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 21;
    function GetCalendarioEmpleado(Empresa: OleVariant; Empleado: Integer; FechaIni: TDateTime; 
                                   FechaFin: TDateTime): OleVariant; dispid 22;
    function PuedeCambiarTarjetaStatus(Empresa: OleVariant; Fecha: TDateTime; Empleado: Integer; 
                                       StatusLimite: Integer): WordBool; dispid 23;
    function IntercambioFestivo(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 301;
    function IntercambioFestivoLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 302;
    function IntercambioFestivoGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 303;
    function CancelarExcepcionesFestivos(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 304;
    function CancelarExcepcionesFestivosLista(Empresa: OleVariant; Lista: OleVariant; 
                                              Parametros: OleVariant): OleVariant; dispid 305;
    function CancelarExcepcionesFestivosGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 306;
    function AjustarIncapaCal(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 307;
    function AjustarIncapaCalLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 308;
    function AjustarIncapaCalGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 309;
    function AutorizacionPreNomina(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 310;
    function AutorizacionPreNominaGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 311;
    function AutorizacionPreNominaLista(Empresa: OleVariant; Lista: OleVariant; 
                                        Parametros: OleVariant): OleVariant; dispid 312;
    function GetCosteoTransferencias(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 313;
    function GrabaCosteoTransferencias(Empresa: OleVariant; oDelta: OleVariant; 
                                       Parametros: OleVariant; out ResultData: OleVariant; 
                                       out ErrorCount: Integer): OleVariant; dispid 314;
    function RegistrarExcepcionesFestivo(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 315;
    function RegistrarExcepcionesFestivoLista(Empresa: OleVariant; Parametros: OleVariant; 
                                              Lista: OleVariant): OleVariant; dispid 316;
    function RegistrarExcepcionesFestivoGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 317;
    function TransferenciasCosteo(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 318;
    function TransferenciasCosteoGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 319;
    function TransferenciasCosteoLista(Empresa: OleVariant; Lista: OleVariant; 
                                       Parametros: OleVariant): OleVariant; dispid 320;
    function CalculaCosteo(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 321;
    function GetSuperXCentroCosto(Empresa: OleVariant): OleVariant; dispid 322;
    function CancelaTransferenciasCosteo(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 323;
    function CancelaTransferenciasCosteoGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 324;
    function CancelaTransferenciasCosteoLista(Empresa: OleVariant; Lista: OleVariant; 
                                              Parametros: OleVariant): OleVariant; dispid 325;
    function GetAlarmasSynel(Empresa: OleVariant; const Filtro: WideString): OleVariant; dispid 326;
    function GrabaAlarmaSynel(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 327;
    function GetRutaReporteSynel(Emrpesa: OleVariant; Reporte: Integer): WideString; dispid 328;
    function GetListaReloj(Emrpesa: OleVariant): OleVariant; dispid 329;
    function GetClasificacionTempLista(Empresa: OleVariant; Parametros: OleVariant; 
                                       Lista: OleVariant; out ErrorCount: Integer): OleVariant; dispid 330;
    function GetClasificacionTempGetLista(Empresa: OleVariant; Parametros: OleVariant; 
                                          Lista: OleVariant): OleVariant; dispid 331;
    function ImportarClasificacionesTemp(Empresa: OleVariant; Parametros: OleVariant; 
                                         Lista: OleVariant): OleVariant; dispid 332;
    function GetPermisoDiasHabiles(Empresa: OleVariant; Empleado: Integer; FechaIni: TDateTime; 
                                   FechaFin: TDateTime): Double; dispid 333;
    function GetPermisoFechaRegreso(Empresa: OleVariant; Empleado: Integer; FechaIni: TDateTime; 
                                    Dias: Double; out DiasRango: Double): TDateTime; dispid 334;
    function GetHorarioTurno(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 335;
    function GetEstatusDia(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime): Currency; dispid 336;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer; 
                             out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                           Options: Integer; const CommandText: WideString; var Params: OleVariant; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerAsistencia provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerAsistencia exposed by              
// the CoClass dmServerAsistencia. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerAsistencia = class
    class function Create: IdmServerAsistencia;
    class function CreateRemote(const MachineName: string): IdmServerAsistencia;
  end;

implementation

uses ComObj;

class function CodmServerAsistencia.Create: IdmServerAsistencia;
begin
  Result := CreateComObject(CLASS_dmServerAsistencia) as IdmServerAsistencia;
end;

class function CodmServerAsistencia.CreateRemote(const MachineName: string): IdmServerAsistencia;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerAsistencia) as IdmServerAsistencia;
end;

end.
