unit SeleccionWeb_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision: 3$
// File generated on 11/11/2002 7:46:29 AM from Type Library described below.

// *************************************************************************//
// NOTE:                                                                      
// Items guarded by $IFDEF_LIVE_SERVER_AT_DESIGN_TIME are used by properties  
// which return objects that may need to be explicitly created via a function 
// call prior to any access via the property. These items have been disabled  
// in order to prevent accidental use from within the object inspector. You   
// may enable them by defining LIVE_SERVER_AT_DESIGN_TIME or by selectively   
// removing them from the $IFDEF blocks. However, such items must still be    
// programmatically created via a method of the appropriate CoClass before    
// they can be used.                                                          
// ************************************************************************ //
// Type Lib: D:\3win_20\MTS\SeleccionWeb.tlb (1)
// IID\LCID: {A90EE799-BA0F-437E-949A-1B92F37B250B}\0
// Helpfile: 
// DepndLst: 
//   (1) v1.0 Midas, (C:\WINDOWS\System32\midas.dll)
//   (2) v2.0 stdole, (C:\WINDOWS\System32\stdole2.tlb)
//   (3) v4.0 StdVCL, (C:\WINDOWS\System32\STDVCL40.DLL)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, OleCtrls, StdVCL, 
  MIDAS;

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  SeleccionWebMajorVersion = 1;
  SeleccionWebMinorVersion = 0;

  LIBID_SeleccionWeb: TGUID = '{A90EE799-BA0F-437E-949A-1B92F37B250B}';

  IID_IdmServerSeleccionWeb: TGUID = '{3CC44947-689F-4715-BA41-B642EC330356}';
  CLASS_dmServerSeleccionWeb: TGUID = '{430129A5-155B-43B7-A4FB-935B51839BE4}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerSeleccionWeb = interface;
  IdmServerSeleccionWebDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerSeleccionWeb = IdmServerSeleccionWeb;


// *********************************************************************//
// Interface: IdmServerSeleccionWeb
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {3CC44947-689F-4715-BA41-B642EC330356}
// *********************************************************************//
  IdmServerSeleccionWeb = interface(IAppServer)
    ['{3CC44947-689F-4715-BA41-B642EC330356}']
    function  GetCompanys(const Parametros: WideString): WideString; safecall;
    function  GetPuestosDisponibles(const Parametros: WideString): WideString; safecall;
    function  GetCFGSolicitud(const Parametros: WideString): WideString; safecall;
    function  GetOneCompany(const Parametros: WideString): WideString; safecall;
    function  AgregaSolicitud(const Parametros: WideString): WideString; safecall;
    function  GetRequisicion(const Parametros: WideString): WideString; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerSeleccionWebDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {3CC44947-689F-4715-BA41-B642EC330356}
// *********************************************************************//
  IdmServerSeleccionWebDisp = dispinterface
    ['{3CC44947-689F-4715-BA41-B642EC330356}']
    function  GetCompanys(const Parametros: WideString): WideString; dispid 1;
    function  GetPuestosDisponibles(const Parametros: WideString): WideString; dispid 2;
    function  GetCFGSolicitud(const Parametros: WideString): WideString; dispid 3;
    function  GetOneCompany(const Parametros: WideString): WideString; dispid 4;
    function  AgregaSolicitud(const Parametros: WideString): WideString; dispid 5;
    function  GetRequisicion(const Parametros: WideString): WideString; dispid 6;
    function  AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; 
                              MaxErrors: Integer; out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function  AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                            Options: Integer; const CommandText: WideString; 
                            var Params: OleVariant; var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function  AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function  AS_GetProviderNames: OleVariant; dispid 20000003;
    function  AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function  AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                            var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerSeleccionWeb provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerSeleccionWeb exposed by              
// the CoClass dmServerSeleccionWeb. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerSeleccionWeb = class
    class function Create: IdmServerSeleccionWeb;
    class function CreateRemote(const MachineName: string): IdmServerSeleccionWeb;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TdmServerSeleccionWeb
// Help String      : dmServerSeleccionWeb Object
// Default Interface: IdmServerSeleccionWeb
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TdmServerSeleccionWebProperties= class;
{$ENDIF}
  TdmServerSeleccionWeb = class(TOleServer)
  private
    FIntf:        IdmServerSeleccionWeb;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps:       TdmServerSeleccionWebProperties;
    function      GetServerProperties: TdmServerSeleccionWebProperties;
{$ENDIF}
    function      GetDefaultInterface: IdmServerSeleccionWeb;
  protected
    procedure InitServerData; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: IdmServerSeleccionWeb);
    procedure Disconnect; override;
    function  AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; 
                              MaxErrors: Integer; out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant;
    function  AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                            Options: Integer; const CommandText: WideString; 
                            var Params: OleVariant; var OwnerData: OleVariant): OleVariant;
    function  AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant;
    function  AS_GetProviderNames: OleVariant;
    function  AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant;
    function  AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                            var OwnerData: OleVariant): OleVariant;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant);
    function  GetCompanys(const Parametros: WideString): WideString;
    function  GetPuestosDisponibles(const Parametros: WideString): WideString;
    function  GetCFGSolicitud(const Parametros: WideString): WideString;
    function  GetOneCompany(const Parametros: WideString): WideString;
    function  AgregaSolicitud(const Parametros: WideString): WideString;
    function  GetRequisicion(const Parametros: WideString): WideString;
    property  DefaultInterface: IdmServerSeleccionWeb read GetDefaultInterface;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TdmServerSeleccionWebProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TdmServerSeleccionWeb
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TdmServerSeleccionWebProperties = class(TPersistent)
  private
    FServer:    TdmServerSeleccionWeb;
    function    GetDefaultInterface: IdmServerSeleccionWeb;
    constructor Create(AServer: TdmServerSeleccionWeb);
  protected
  public
    property DefaultInterface: IdmServerSeleccionWeb read GetDefaultInterface;
  published
  end;
{$ENDIF}


procedure Register;

implementation

uses ComObj;

class function CodmServerSeleccionWeb.Create: IdmServerSeleccionWeb;
begin
  Result := CreateComObject(CLASS_dmServerSeleccionWeb) as IdmServerSeleccionWeb;
end;

class function CodmServerSeleccionWeb.CreateRemote(const MachineName: string): IdmServerSeleccionWeb;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerSeleccionWeb) as IdmServerSeleccionWeb;
end;

procedure TdmServerSeleccionWeb.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{430129A5-155B-43B7-A4FB-935B51839BE4}';
    IntfIID:   '{3CC44947-689F-4715-BA41-B642EC330356}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TdmServerSeleccionWeb.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as IdmServerSeleccionWeb;
  end;
end;

procedure TdmServerSeleccionWeb.ConnectTo(svrIntf: IdmServerSeleccionWeb);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TdmServerSeleccionWeb.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TdmServerSeleccionWeb.GetDefaultInterface: IdmServerSeleccionWeb;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call ''Connect'' or ''ConnectTo'' before this operation');
  Result := FIntf;
end;

constructor TdmServerSeleccionWeb.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TdmServerSeleccionWebProperties.Create(Self);
{$ENDIF}
end;

destructor TdmServerSeleccionWeb.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TdmServerSeleccionWeb.GetServerProperties: TdmServerSeleccionWebProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function  TdmServerSeleccionWeb.AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; 
                                                MaxErrors: Integer; out ErrorCount: Integer; 
                                                var OwnerData: OleVariant): OleVariant;
begin
  Result := DefaultInterface.AS_ApplyUpdates(ProviderName, Delta, MaxErrors, ErrorCount, OwnerData);
end;

function  TdmServerSeleccionWeb.AS_GetRecords(const ProviderName: WideString; Count: Integer; 
                                              out RecsOut: Integer; Options: Integer; 
                                              const CommandText: WideString; 
                                              var Params: OleVariant; var OwnerData: OleVariant): OleVariant;
begin
  Result := DefaultInterface.AS_GetRecords(ProviderName, Count, RecsOut, Options, CommandText, 
                                           Params, OwnerData);
end;

function  TdmServerSeleccionWeb.AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant;
begin
  Result := DefaultInterface.AS_DataRequest(ProviderName, Data);
end;

function  TdmServerSeleccionWeb.AS_GetProviderNames: OleVariant;
begin
  Result := DefaultInterface.AS_GetProviderNames;
end;

function  TdmServerSeleccionWeb.AS_GetParams(const ProviderName: WideString; 
                                             var OwnerData: OleVariant): OleVariant;
begin
  Result := DefaultInterface.AS_GetParams(ProviderName, OwnerData);
end;

function  TdmServerSeleccionWeb.AS_RowRequest(const ProviderName: WideString; Row: OleVariant; 
                                              RequestType: Integer; var OwnerData: OleVariant): OleVariant;
begin
  Result := DefaultInterface.AS_RowRequest(ProviderName, Row, RequestType, OwnerData);
end;

procedure TdmServerSeleccionWeb.AS_Execute(const ProviderName: WideString; 
                                           const CommandText: WideString; var Params: OleVariant; 
                                           var OwnerData: OleVariant);
begin
  DefaultInterface.AS_Execute(ProviderName, CommandText, Params, OwnerData);
end;

function  TdmServerSeleccionWeb.GetCompanys(const Parametros: WideString): WideString;
begin
  Result := DefaultInterface.GetCompanys(Parametros);
end;

function  TdmServerSeleccionWeb.GetPuestosDisponibles(const Parametros: WideString): WideString;
begin
  Result := DefaultInterface.GetPuestosDisponibles(Parametros);
end;

function  TdmServerSeleccionWeb.GetCFGSolicitud(const Parametros: WideString): WideString;
begin
  Result := DefaultInterface.GetCFGSolicitud(Parametros);
end;

function  TdmServerSeleccionWeb.GetOneCompany(const Parametros: WideString): WideString;
begin
  Result := DefaultInterface.GetOneCompany(Parametros);
end;

function  TdmServerSeleccionWeb.AgregaSolicitud(const Parametros: WideString): WideString;
begin
  Result := DefaultInterface.AgregaSolicitud(Parametros);
end;

function  TdmServerSeleccionWeb.GetRequisicion(const Parametros: WideString): WideString;
begin
  Result := DefaultInterface.GetRequisicion(Parametros);
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TdmServerSeleccionWebProperties.Create(AServer: TdmServerSeleccionWeb);
begin
  inherited Create;
  FServer := AServer;
end;

function TdmServerSeleccionWebProperties.GetDefaultInterface: IdmServerSeleccionWeb;
begin
  Result := FServer.DefaultInterface;
end;

{$ENDIF}

procedure Register;
begin
  RegisterComponents('Servers',[TdmServerSeleccionWeb]);
end;

end.
