unit Evaluacion_TLB;

// ************************************************************************ //
// WARNING
// -------
// The types declared in this file were generated from data read from a
// Type Library. If this type library is explicitly or indirectly (via
// another type library referring to this type library) re-imported, or the
// 'Refresh' command of the Type Library Editor activated while editing the
// Type Library, the contents of this file will be regenerated and all
// manual modifications will be lost.
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 7/18/2018 3:16:23 PM from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3win_30_Vsn_2018\MTS\Evaluacion (1)
// LIBID: {4260F6E1-36E4-4628-88A9-098848FA4ADF}
// LCID: 0
// Helpfile:
// HelpString: Tress Evaluacion
// DepndLst:
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
//   (2) v1.0 Midas, (midas.dll)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers.
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface

uses Winapi.Windows, Midas, System.Classes, System.Variants, System.Win.StdVCL, Vcl.Graphics, Vcl.OleServer, Winapi.ActiveX;



// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:
//   Type Libraries     : LIBID_xxxx
//   CoClasses          : CLASS_xxxx
//   DISPInterfaces     : DIID_xxxx
//   Non-DISP interfaces: IID_xxxx
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  EvaluacionMajorVersion = 1;
  EvaluacionMinorVersion = 0;

  LIBID_Evaluacion: TGUID = '{4260F6E1-36E4-4628-88A9-098848FA4ADF}';

  IID_IdmServerEvaluacion: TGUID = '{48F2C37B-A23C-44D6-B1FC-8811379DC707}';
  CLASS_dmServerEvaluacion: TGUID = '{D4C36C59-A76B-49A0-82F8-51FF5227D3F9}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary
// *********************************************************************//
  IdmServerEvaluacion = interface;
  IdmServerEvaluacionDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library
// (NOTE: Here we map each CoClass to its Default Interface)
// *********************************************************************//
  dmServerEvaluacion = IdmServerEvaluacion;


// *********************************************************************//
// Interface: IdmServerEvaluacion
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {48F2C37B-A23C-44D6-B1FC-8811379DC707}
// *********************************************************************//
  IdmServerEvaluacion = interface(IAppServer)
    ['{48F2C37B-A23C-44D6-B1FC-8811379DC707}']
    function EncuestaSiguiente(Empresa: OleVariant; iEncuesta: Integer; out oDelta: OleVariant): WordBool; safecall;
    function EncuestaAnterior(Empresa: OleVariant; iEncuesta: Integer; out oDelta: OleVariant): WordBool; safecall;
    function GetCatCompetencias(Empresa: OleVariant): OleVariant; safecall;
    function GetCatHabilidades(Empresa: OleVariant): OleVariant; safecall;
    function GetCatPreguntas(Empresa: OleVariant; const sCompetencia: WideString): OleVariant; safecall;
    function GetCatNivelesEscala(Empresa: OleVariant; const sEscala: WideString;
                                 out oEscNiv: OleVariant): OleVariant; safecall;
    procedure CambiaPregunta(Empresa: OleVariant; const CM_CODIGO: WideString; iActual: Smallint;
                             iNuevo: Smallint); safecall;
    function GrabaCompetencia(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    procedure BorraPregunta(Empresa: OleVariant; const CM_CODIGO: WideString; iActual: Smallint); safecall;
    function GrabaTCompetencia(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GrabaCatEscalas(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function SiguientePregunta(Empresa: OleVariant): Smallint; safecall;
    function GrabaPregunta(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetEscalasNiveles(Empresa: OleVariant; const sEscala: WideString): OleVariant; safecall;
    function GrabaEscalaNiveles(Empresa: OleVariant; oDelta: OleVariant; oEscNiv: OleVariant;
                                out ErrorCount: Integer; out oResultEscNiv: OleVariant): OleVariant; safecall;
    function GetCatEscalas(Empresa: OleVariant): OleVariant; safecall;
    function BorraEscala(Empresa: OleVariant; const sEscala: WideString; out ErrorCount: Integer): OleVariant; safecall;
    function GetCriteriosEvaluar(Empresa: OleVariant; iEncuesta: Integer): OleVariant; safecall;
    procedure CambiaCriterios(Empresa: OleVariant; iEncuesta: Integer; iActual: Smallint;
                              iNueva: Smallint); safecall;
    procedure BorraCriterio(Empresa: OleVariant; iEncuesta: Integer; iPosicion: Smallint); safecall;
    function GrabaCriteriosEval(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetPreguntasCriterio(Empresa: OleVariant; iEncuesta: Integer; iCriterio: Smallint): OleVariant; safecall;
    procedure CambiaPreguntaCriterio(Empresa: OleVariant; iEncuesta: Integer; iCriterio: Smallint;
                                     iActual: Smallint; iNueva: Smallint); safecall;
    procedure BorraPreguntaCriterio(Empresa: OleVariant; iEncuesta: Integer; iCriterio: Smallint;
                                    iPregunta: Smallint); safecall;
    function GetEscalasEncuesta(Empresa: OleVariant; iEncuesta: Integer): OleVariant; safecall;
    function GetPerfilEvaluador(Empresa: OleVariant; iEncuesta: Integer): OleVariant; safecall;
    function GetEmpEvaluar(Empresa: OleVariant; iEncuesta: Integer; iEmpleado: Integer;
                           iStatus: Smallint): OleVariant; safecall;
    function GetEncuestas(Empresa: OleVariant): OleVariant; safecall;
    function GrabaEncuesta(Empresa: OleVariant; oDelta: OleVariant; oDataCriterio: OleVariant;
                           oDataEval: OleVariant; lGrabaCriterios: WordBool; out ErrorCount: Integer): OleVariant; safecall;
    procedure CopiaEncuesta(Empresa: OleVariant; iEncuesta: Integer; const sNombre: WideString;
                            lSujetos: WordBool; lEvaluadores: WordBool); safecall;
    procedure AgregaCriterios(Empresa: OleVariant; iEncuesta: Integer; iCriterios: Smallint;
                              oData: OleVariant); safecall;
    function GetEncuesta(Empresa: OleVariant; iEncuesta: Integer): OleVariant; safecall;
    function GetCatNivelesEnc(Empresa: OleVariant; const sFiltro: WideString): OleVariant; safecall;
    function GrabaPreguntasCriterio(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetEscalaEncuestaNiv(Empresa: OleVariant; iEncuesta: Integer; const sEscala: WideString): OleVariant; safecall;
    function GrabaEscalaNivelesEnc(Empresa: OleVariant; oDelta: OleVariant; oEscNivEnc: OleVariant;
                                   out ErrorCount: Integer; out oResultEscNivEnc: OleVariant): OleVariant; safecall;
    procedure ContadoresDEncuesta(Empresa: OleVariant; iEncuesta: Integer); safecall;
    function GrabaPerfilEvaluador(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetMaxTipoEvaluador(Empresa: OleVariant; iEncuesta: Integer): Smallint; safecall;
    function GetEvalua(Empresa: OleVariant; iEncuesta: Integer; iEmpleado: Integer): OleVariant; safecall;
    procedure AgregaEvalua(Empresa: OleVariant; iEncuesta: Integer; iSujeto: Integer;
                           iRelacion: Integer; iEmpleado: Integer; iStatus: Smallint;
                           lInserta: WordBool); safecall;
    procedure BorraEvalua(Empresa: OleVariant; iEncuesta: Integer; iEmpleado: Integer;
                          iSujeto: Integer); safecall;
    function GetDatosEncuesta(Empresa: OleVariant; iEncuesta: Integer;
                              out ParamEvaluacion: OleVariant; out ParamEvaluador: OleVariant;
                              out ParamEvaluado: OleVariant): OleVariant; safecall;
    function GetEvaluadores(Empresa: OleVariant; iEncuesta: Integer; const sFiltro: WideString): OleVariant; safecall;
    function GetEvaluaciones(Empresa: OleVariant; iEncuesta: Integer; const sFiltro: WideString): OleVariant; safecall;
    function GetAnalisisCriterio(Empresa: OleVariant; iEncuesta: Integer; iMostrar: Smallint;
                                 iRelacion: Smallint; iOrden: Smallint): OleVariant; safecall;
    function GetAnalisisPreguntas(Empresa: OleVariant; iEncuesta: Integer; iMostrar: Smallint;
                                  iRelacion: Smallint; iCriterio: Smallint; iOrden: Smallint): OleVariant; safecall;
    function GetPreguntasEnc(Empresa: OleVariant; iEncuesta: Integer): OleVariant; safecall;
    function GetFrecuenciaResp(Empresa: OleVariant; iEncuesta: Integer; iPregunta: Smallint;
                               iRelacion: Smallint): OleVariant; safecall;
    function GetResultadoCriterio(Empresa: OleVariant; iEncuesta: Integer; iEmpleado: Integer;
                                  iMostrar: Smallint; iOrden: Smallint): OleVariant; safecall;
    function GetResultadoPregunta(Empresa: OleVariant; iEncuesta: Integer; iEmpleado: Integer;
                                  iMostrar: Smallint; iOrden: Smallint): OleVariant; safecall;
    function GetSujetos(Empresa: OleVariant): OleVariant; safecall;
    function AgregaEmpEvaluar(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    procedure BorraEmpEvaluar(Empresa: OleVariant; iEncuesta: Integer; iEmpleado: Integer); safecall;
    procedure CambiaStatusSujeto(Empresa: OleVariant; iEncuesta: Integer; iEmpleado: Integer;
                                 lPonerListo: WordBool); safecall;
    function AgregaEmpEvaluarGlobal(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function AgregaEmpEvaluarGlobalGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function AgregaEmpEvaluarGlobalLista(Empresa: OleVariant; Lista: OleVariant;
                                         Parametros: OleVariant): OleVariant; safecall;
    function PrepararEvaluaciones(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CalcularResultados(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function RecalcularPromedios(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetPregContestadas(Empresa: OleVariant; iUsuario: Integer; iEmpleado: Integer;
                                iEncuesta: Integer): Integer; safecall;
    function CierraEvaluaciones(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CierraEvaluacionesGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CierraEvaluacionesLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CopiaEscalasEncuesta(Empresa: OleVariant; iEncuesta: Smallint;
                                  const sEscala: WideString): Smallint; safecall;
    procedure InicializaEncuesta(Empresa: OleVariant; iEncuesta: Integer); safecall;
    function GetDatosXEvaluado(Empresa: OleVariant; iEncuesta: Integer; iSujeto: Integer): OleVariant; safecall;
    function CorreosInvitacion(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CorreosInvitacionGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CorreosInvitacionLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CorreosNotificacion(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetEmpleado(Empresa: OleVariant; Empleado: Integer; Encuesta: Integer): Integer; safecall;
    function GetEmpleadoAnterior(Empresa: OleVariant; Empleado: Integer; Encuesta: Integer): Integer; safecall;
    function GetEmpleadoSiguiente(Empresa: OleVariant; Empleado: Integer; Encuesta: Integer): Integer; safecall;
    function GetEmpleadosBuscados(Empresa: OleVariant; const sPaterno: WideString;
                                  const sMaterno: WideString; const sNombre: WideString;
                                  const sRFC: WideString; const sNSS: WideString;
                                  const sBanca: WideString; iEncuesta: Integer): OleVariant; safecall;
    function GetCorreos(Parametros: OleVariant): OleVariant; safecall;
    function GetGlobales(Empresa: OleVariant): OleVariant; safecall;
    procedure GrabaGlobales(Empresa: OleVariant; Valores: OleVariant); safecall;
    function GetRolesUsuario(Usuario: Integer): OleVariant; safecall;
    function GrabaUsuarios(Usuario: Integer; Delta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetUsuarios(Grupo: Integer): OleVariant; safecall;
    function AsignaRelaciones(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function AsignaRelacionesGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function AsignaRelacionesLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function ExportarEncuesta(Empresa: OleVariant; iEncuesta: Integer; iUsuario: Integer): WideString; safecall;
    procedure ImportarEncuesta(Empresa: OleVariant; const sArchivo: WideString); safecall;
    procedure GetParametrosURL(Empresa: OleVariant; const sEmpresa: WideString; Usuario: Integer;
                               out RutaVirtual: WideString; out Empleado: Integer;
                               out Nombre: WideString); safecall;
    function CreaEncuestaSimple(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CreaEncuestaSimpleGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function CreaEncuestaSimpleLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function AgregarEncuestaParametros(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerEvaluacionDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {48F2C37B-A23C-44D6-B1FC-8811379DC707}
// *********************************************************************//
  IdmServerEvaluacionDisp = dispinterface
    ['{48F2C37B-A23C-44D6-B1FC-8811379DC707}']
    function EncuestaSiguiente(Empresa: OleVariant; iEncuesta: Integer; out oDelta: OleVariant): WordBool; dispid 1;
    function EncuestaAnterior(Empresa: OleVariant; iEncuesta: Integer; out oDelta: OleVariant): WordBool; dispid 2;
    function GetCatCompetencias(Empresa: OleVariant): OleVariant; dispid 3;
    function GetCatHabilidades(Empresa: OleVariant): OleVariant; dispid 4;
    function GetCatPreguntas(Empresa: OleVariant; const sCompetencia: WideString): OleVariant; dispid 5;
    function GetCatNivelesEscala(Empresa: OleVariant; const sEscala: WideString;
                                 out oEscNiv: OleVariant): OleVariant; dispid 6;
    procedure CambiaPregunta(Empresa: OleVariant; const CM_CODIGO: WideString; iActual: Smallint;
                             iNuevo: Smallint); dispid 7;
    function GrabaCompetencia(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 8;
    procedure BorraPregunta(Empresa: OleVariant; const CM_CODIGO: WideString; iActual: Smallint); dispid 9;
    function GrabaTCompetencia(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 10;
    function GrabaCatEscalas(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 11;
    function SiguientePregunta(Empresa: OleVariant): Smallint; dispid 12;
    function GrabaPregunta(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 13;
    function GetEscalasNiveles(Empresa: OleVariant; const sEscala: WideString): OleVariant; dispid 14;
    function GrabaEscalaNiveles(Empresa: OleVariant; oDelta: OleVariant; oEscNiv: OleVariant;
                                out ErrorCount: Integer; out oResultEscNiv: OleVariant): OleVariant; dispid 15;
    function GetCatEscalas(Empresa: OleVariant): OleVariant; dispid 16;
    function BorraEscala(Empresa: OleVariant; const sEscala: WideString; out ErrorCount: Integer): OleVariant; dispid 17;
    function GetCriteriosEvaluar(Empresa: OleVariant; iEncuesta: Integer): OleVariant; dispid 18;
    procedure CambiaCriterios(Empresa: OleVariant; iEncuesta: Integer; iActual: Smallint;
                              iNueva: Smallint); dispid 19;
    procedure BorraCriterio(Empresa: OleVariant; iEncuesta: Integer; iPosicion: Smallint); dispid 20;
    function GrabaCriteriosEval(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 21;
    function GetPreguntasCriterio(Empresa: OleVariant; iEncuesta: Integer; iCriterio: Smallint): OleVariant; dispid 22;
    procedure CambiaPreguntaCriterio(Empresa: OleVariant; iEncuesta: Integer; iCriterio: Smallint;
                                     iActual: Smallint; iNueva: Smallint); dispid 23;
    procedure BorraPreguntaCriterio(Empresa: OleVariant; iEncuesta: Integer; iCriterio: Smallint;
                                    iPregunta: Smallint); dispid 24;
    function GetEscalasEncuesta(Empresa: OleVariant; iEncuesta: Integer): OleVariant; dispid 25;
    function GetPerfilEvaluador(Empresa: OleVariant; iEncuesta: Integer): OleVariant; dispid 26;
    function GetEmpEvaluar(Empresa: OleVariant; iEncuesta: Integer; iEmpleado: Integer;
                           iStatus: Smallint): OleVariant; dispid 27;
    function GetEncuestas(Empresa: OleVariant): OleVariant; dispid 28;
    function GrabaEncuesta(Empresa: OleVariant; oDelta: OleVariant; oDataCriterio: OleVariant;
                           oDataEval: OleVariant; lGrabaCriterios: WordBool; out ErrorCount: Integer): OleVariant; dispid 29;
    procedure CopiaEncuesta(Empresa: OleVariant; iEncuesta: Integer; const sNombre: WideString;
                            lSujetos: WordBool; lEvaluadores: WordBool); dispid 30;
    procedure AgregaCriterios(Empresa: OleVariant; iEncuesta: Integer; iCriterios: Smallint;
                              oData: OleVariant); dispid 31;
    function GetEncuesta(Empresa: OleVariant; iEncuesta: Integer): OleVariant; dispid 32;
    function GetCatNivelesEnc(Empresa: OleVariant; const sFiltro: WideString): OleVariant; dispid 33;
    function GrabaPreguntasCriterio(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 34;
    function GetEscalaEncuestaNiv(Empresa: OleVariant; iEncuesta: Integer; const sEscala: WideString): OleVariant; dispid 35;
    function GrabaEscalaNivelesEnc(Empresa: OleVariant; oDelta: OleVariant; oEscNivEnc: OleVariant;
                                   out ErrorCount: Integer; out oResultEscNivEnc: OleVariant): OleVariant; dispid 36;
    procedure ContadoresDEncuesta(Empresa: OleVariant; iEncuesta: Integer); dispid 37;
    function GrabaPerfilEvaluador(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 38;
    function GetMaxTipoEvaluador(Empresa: OleVariant; iEncuesta: Integer): Smallint; dispid 39;
    function GetEvalua(Empresa: OleVariant; iEncuesta: Integer; iEmpleado: Integer): OleVariant; dispid 41;
    procedure AgregaEvalua(Empresa: OleVariant; iEncuesta: Integer; iSujeto: Integer;
                           iRelacion: Integer; iEmpleado: Integer; iStatus: Smallint;
                           lInserta: WordBool); dispid 40;
    procedure BorraEvalua(Empresa: OleVariant; iEncuesta: Integer; iEmpleado: Integer;
                          iSujeto: Integer); dispid 42;
    function GetDatosEncuesta(Empresa: OleVariant; iEncuesta: Integer;
                              out ParamEvaluacion: OleVariant; out ParamEvaluador: OleVariant;
                              out ParamEvaluado: OleVariant): OleVariant; dispid 43;
    function GetEvaluadores(Empresa: OleVariant; iEncuesta: Integer; const sFiltro: WideString): OleVariant; dispid 44;
    function GetEvaluaciones(Empresa: OleVariant; iEncuesta: Integer; const sFiltro: WideString): OleVariant; dispid 45;
    function GetAnalisisCriterio(Empresa: OleVariant; iEncuesta: Integer; iMostrar: Smallint;
                                 iRelacion: Smallint; iOrden: Smallint): OleVariant; dispid 46;
    function GetAnalisisPreguntas(Empresa: OleVariant; iEncuesta: Integer; iMostrar: Smallint;
                                  iRelacion: Smallint; iCriterio: Smallint; iOrden: Smallint): OleVariant; dispid 47;
    function GetPreguntasEnc(Empresa: OleVariant; iEncuesta: Integer): OleVariant; dispid 48;
    function GetFrecuenciaResp(Empresa: OleVariant; iEncuesta: Integer; iPregunta: Smallint;
                               iRelacion: Smallint): OleVariant; dispid 49;
    function GetResultadoCriterio(Empresa: OleVariant; iEncuesta: Integer; iEmpleado: Integer;
                                  iMostrar: Smallint; iOrden: Smallint): OleVariant; dispid 50;
    function GetResultadoPregunta(Empresa: OleVariant; iEncuesta: Integer; iEmpleado: Integer;
                                  iMostrar: Smallint; iOrden: Smallint): OleVariant; dispid 51;
    function GetSujetos(Empresa: OleVariant): OleVariant; dispid 53;
    function AgregaEmpEvaluar(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 54;
    procedure BorraEmpEvaluar(Empresa: OleVariant; iEncuesta: Integer; iEmpleado: Integer); dispid 56;
    procedure CambiaStatusSujeto(Empresa: OleVariant; iEncuesta: Integer; iEmpleado: Integer;
                                 lPonerListo: WordBool); dispid 52;
    function AgregaEmpEvaluarGlobal(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 55;
    function AgregaEmpEvaluarGlobalGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 57;
    function AgregaEmpEvaluarGlobalLista(Empresa: OleVariant; Lista: OleVariant;
                                         Parametros: OleVariant): OleVariant; dispid 58;
    function PrepararEvaluaciones(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 59;
    function CalcularResultados(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 60;
    function RecalcularPromedios(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 61;
    function GetPregContestadas(Empresa: OleVariant; iUsuario: Integer; iEmpleado: Integer;
                                iEncuesta: Integer): Integer; dispid 62;
    function CierraEvaluaciones(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 63;
    function CierraEvaluacionesGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 64;
    function CierraEvaluacionesLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 65;
    function CopiaEscalasEncuesta(Empresa: OleVariant; iEncuesta: Smallint;
                                  const sEscala: WideString): Smallint; dispid 66;
    procedure InicializaEncuesta(Empresa: OleVariant; iEncuesta: Integer); dispid 67;
    function GetDatosXEvaluado(Empresa: OleVariant; iEncuesta: Integer; iSujeto: Integer): OleVariant; dispid 68;
    function CorreosInvitacion(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 69;
    function CorreosInvitacionGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 70;
    function CorreosInvitacionLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 71;
    function CorreosNotificacion(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 72;
    function GetEmpleado(Empresa: OleVariant; Empleado: Integer; Encuesta: Integer): Integer; dispid 73;
    function GetEmpleadoAnterior(Empresa: OleVariant; Empleado: Integer; Encuesta: Integer): Integer; dispid 74;
    function GetEmpleadoSiguiente(Empresa: OleVariant; Empleado: Integer; Encuesta: Integer): Integer; dispid 75;
    function GetEmpleadosBuscados(Empresa: OleVariant; const sPaterno: WideString;
                                  const sMaterno: WideString; const sNombre: WideString;
                                  const sRFC: WideString; const sNSS: WideString;
                                  const sBanca: WideString; iEncuesta: Integer): OleVariant; dispid 76;
    function GetCorreos(Parametros: OleVariant): OleVariant; dispid 77;
    function GetGlobales(Empresa: OleVariant): OleVariant; dispid 78;
    procedure GrabaGlobales(Empresa: OleVariant; Valores: OleVariant); dispid 79;
    function GetRolesUsuario(Usuario: Integer): OleVariant; dispid 80;
    function GrabaUsuarios(Usuario: Integer; Delta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 81;
    function GetUsuarios(Grupo: Integer): OleVariant; dispid 82;
    function AsignaRelaciones(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 83;
    function AsignaRelacionesGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 84;
    function AsignaRelacionesLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 85;
    function ExportarEncuesta(Empresa: OleVariant; iEncuesta: Integer; iUsuario: Integer): WideString; dispid 86;
    procedure ImportarEncuesta(Empresa: OleVariant; const sArchivo: WideString); dispid 87;
    procedure GetParametrosURL(Empresa: OleVariant; const sEmpresa: WideString; Usuario: Integer;
                               out RutaVirtual: WideString; out Empleado: Integer;
                               out Nombre: WideString); dispid 301;
    function CreaEncuestaSimple(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 302;
    function CreaEncuestaSimpleGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 303;
    function CreaEncuestaSimpleLista(Empresa: OleVariant; Lista: OleVariant; Parametros: OleVariant): OleVariant; dispid 304;
    function AgregarEncuestaParametros(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 305;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer;
                             out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer;
                           Options: Integer; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerEvaluacion provides a Create and CreateRemote method to
// create instances of the default interface IdmServerEvaluacion exposed by
// the CoClass dmServerEvaluacion. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CodmServerEvaluacion = class
    class function Create: IdmServerEvaluacion;
    class function CreateRemote(const MachineName: string): IdmServerEvaluacion;
  end;

implementation

uses System.Win.ComObj;

class function CodmServerEvaluacion.Create: IdmServerEvaluacion;
begin
  Result := CreateComObject(CLASS_dmServerEvaluacion) as IdmServerEvaluacion;
end;

class function CodmServerEvaluacion.CreateRemote(const MachineName: string): IdmServerEvaluacion;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerEvaluacion) as IdmServerEvaluacion;
end;

end.

