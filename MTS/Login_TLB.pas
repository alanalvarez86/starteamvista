unit Login_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 6/21/2018 8:09:44 AM from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3win_30_Vsn_2018\MTS\Login.tlb (1)
// LIBID: {5B24F017-8891-11D3-8BBD-0050DA04EAC5}
// LCID: 0
// Helpfile: 
// HelpString: Tress Login
// DepndLst: 
//   (1) v1.0 Midas, (C:\Program Files (x86)\Embarcadero\RAD Studio\12.0\bin64\midas.dll)
//   (2) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, Midas, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  LoginMajorVersion = 1;
  LoginMinorVersion = 0;

  LIBID_Login: TGUID = '{5B24F017-8891-11D3-8BBD-0050DA04EAC5}';

  IID_IdmServerLogin: TGUID = '{5B24F018-8891-11D3-8BBD-0050DA04EAC5}';
  CLASS_dmServerLogin: TGUID = '{5B24F01A-8891-11D3-8BBD-0050DA04EAC5}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerLogin = interface;
  IdmServerLoginDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerLogin = IdmServerLogin;


// *********************************************************************//
// Interface: IdmServerLogin
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {5B24F018-8891-11D3-8BBD-0050DA04EAC5}
// *********************************************************************//
  IdmServerLogin = interface(IAppServer)
    ['{5B24F018-8891-11D3-8BBD-0050DA04EAC5}']
    function GetCompanys(Usuario: Integer; Tipo: Integer): OleVariant; safecall;
    function GetPatrones(Empresa: OleVariant): OleVariant; safecall;
    function GetAccesos(Grupo: Integer; const Company: WideString): OleVariant; safecall;
    function GetEmpleado(Empresa: OleVariant; Empleado: Integer; out Datos: OleVariant): WordBool; safecall;
    function GetEmpleadoAnterior(Empresa: OleVariant; Empleado: Integer; const sNavega: WideString; 
                                 out Datos: OleVariant): WordBool; safecall;
    function GetEmpleadoSiguiente(Empresa: OleVariant; Empleado: Integer; 
                                  const sNavega: WideString; out Datos: OleVariant): WordBool; safecall;
    function GetEmpleadosBuscados(Empresa: OleVariant; const sPaterno: WideString; 
                                  const sMaterno: WideString; const sNombre: WideString; 
                                  const sRFC: WideString; const sNSS: WideString; 
                                  const sBanca: WideString): OleVariant; safecall;
    function GetPeriodoInicial(Empresa: OleVariant; Year: Integer; Tipo: Integer): OleVariant; safecall;
    function GetPeriodo(Empresa: OleVariant; Year: Integer; Tipo: Integer; Numero: Integer; 
                        out Datos: OleVariant): WordBool; safecall;
    function GetPeriodoAnterior(Empresa: OleVariant; Year: Integer; Tipo: Integer; Numero: Integer; 
                                out Datos: OleVariant): WordBool; safecall;
    function GetPeriodoSiguiente(Empresa: OleVariant; Year: Integer; Tipo: Integer; 
                                 Numero: Integer; out Datos: OleVariant): WordBool; safecall;
    function UsuarioLogin(const NombreCorto: WideString; const Clave: WideString; 
                          const ComputerName: WideString; AllowReentry: WordBool; 
                          out Datos: OleVariant; out Autorizacion: OleVariant; out Intentos: Integer): Integer; safecall;
    function UsuarioLogout(Usuario: Integer): Integer; safecall;
    procedure UsuarioCambiaPswd(Usuario: Integer; const ClaveActual: WideString; 
                                const ClaveNueva: WideString); safecall;
    function GetLookupEmpleado(Empresa: OleVariant; Empleado: Integer; out Datos: OleVariant; 
                               TipoLookup: Integer): WordBool; safecall;
    function GetNominaSiguiente(Empresa: OleVariant; Parametros: OleVariant; out Datos: OleVariant): WordBool; safecall;
    function GetNominaAnterior(Empresa: OleVariant; Parametros: OleVariant; out Datos: OleVariant): WordBool; safecall;
    function GetAuto: OleVariant; safecall;
    function GetSeguridad: OleVariant; safecall;
    procedure SetSeguridad(Empresa: OleVariant; Valores: OleVariant); safecall;
    procedure UsuarioBloquea(const NombreCorto: WideString; const ComputerName: WideString; 
                             Intentos: Integer); safecall;
    procedure GrabaBloqueoSistema(lValorBloqueo: WordBool); safecall;
    function GetClave(Clave: Integer): WideString; safecall;
    function GetTipoLogin: Integer; safecall;
    procedure GrabaTipoLogin(TipoLogin: Integer); safecall;
    function LoginAD(const ComputerName: WideString; AllowReentry: WordBool; out Tipo: Integer; 
                     out Datos: OleVariant; out Autorizacion: OleVariant; 
                     const sDominioUsuario: WideString): Integer; safecall;
    function GetTipoPeriodo(Empresa: OleVariant): OleVariant; safecall;
    function GetSeguridadEmpresa(Empresa: OleVariant): OleVariant; safecall;
    function UsuarioLoginConsola(const Usuario: WideString; const Clave: WideString; 
                                 ActiveDir: WordBool; Intentos: Integer; 
                                 const EquipoLocal: WideString): WideString; safecall;
    function CheckVersionDatos(Empresa: OleVariant): OleVariant; safecall;
    function GetEmpleadosBuscados_DevEx(Empresa: OleVariant; const sPista: WideString): OleVariant; safecall;
    function CheckDBServer(Empresa: OleVariant): Integer; safecall;
    function GetCompanysList(Usuario: Integer; Lista: OleVariant): OleVariant; safecall;
    function GetNotificacionAdvertencia: OleVariant; safecall;
    procedure SetNotificacionAdvertencia(Empresa: OleVariant; Valores: OleVariant); safecall;
    function GetServicioReportesCorreos: OleVariant; safecall;
    procedure SetServicioReportesCorreos(Empresa: OleVariant; Valores: OleVariant); safecall;
    function EnviaClaveTemporal(const sUsuario: WideString; const sClaveCifrada: WideString; 
                                const sCorreoRemitente: WideString; 
                                const sDescripRemitente: WideString; 
                                const sCorreoValidar: WideString): WordBool; safecall;
    function GetStatusTimbrado(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    procedure AnaliticaContabilizarUso(Empresa: OleVariant; const sModulo: WideString; 
                                       HelpContext: Integer; Conteo: Integer); safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerLoginDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {5B24F018-8891-11D3-8BBD-0050DA04EAC5}
// *********************************************************************//
  IdmServerLoginDisp = dispinterface
    ['{5B24F018-8891-11D3-8BBD-0050DA04EAC5}']
    function GetCompanys(Usuario: Integer; Tipo: Integer): OleVariant; dispid 1;
    function GetPatrones(Empresa: OleVariant): OleVariant; dispid 2;
    function GetAccesos(Grupo: Integer; const Company: WideString): OleVariant; dispid 3;
    function GetEmpleado(Empresa: OleVariant; Empleado: Integer; out Datos: OleVariant): WordBool; dispid 4;
    function GetEmpleadoAnterior(Empresa: OleVariant; Empleado: Integer; const sNavega: WideString; 
                                 out Datos: OleVariant): WordBool; dispid 5;
    function GetEmpleadoSiguiente(Empresa: OleVariant; Empleado: Integer; 
                                  const sNavega: WideString; out Datos: OleVariant): WordBool; dispid 6;
    function GetEmpleadosBuscados(Empresa: OleVariant; const sPaterno: WideString; 
                                  const sMaterno: WideString; const sNombre: WideString; 
                                  const sRFC: WideString; const sNSS: WideString; 
                                  const sBanca: WideString): OleVariant; dispid 7;
    function GetPeriodoInicial(Empresa: OleVariant; Year: Integer; Tipo: Integer): OleVariant; dispid 8;
    function GetPeriodo(Empresa: OleVariant; Year: Integer; Tipo: Integer; Numero: Integer; 
                        out Datos: OleVariant): WordBool; dispid 9;
    function GetPeriodoAnterior(Empresa: OleVariant; Year: Integer; Tipo: Integer; Numero: Integer; 
                                out Datos: OleVariant): WordBool; dispid 10;
    function GetPeriodoSiguiente(Empresa: OleVariant; Year: Integer; Tipo: Integer; 
                                 Numero: Integer; out Datos: OleVariant): WordBool; dispid 11;
    function UsuarioLogin(const NombreCorto: WideString; const Clave: WideString; 
                          const ComputerName: WideString; AllowReentry: WordBool; 
                          out Datos: OleVariant; out Autorizacion: OleVariant; out Intentos: Integer): Integer; dispid 12;
    function UsuarioLogout(Usuario: Integer): Integer; dispid 13;
    procedure UsuarioCambiaPswd(Usuario: Integer; const ClaveActual: WideString; 
                                const ClaveNueva: WideString); dispid 14;
    function GetLookupEmpleado(Empresa: OleVariant; Empleado: Integer; out Datos: OleVariant; 
                               TipoLookup: Integer): WordBool; dispid 17;
    function GetNominaSiguiente(Empresa: OleVariant; Parametros: OleVariant; out Datos: OleVariant): WordBool; dispid 15;
    function GetNominaAnterior(Empresa: OleVariant; Parametros: OleVariant; out Datos: OleVariant): WordBool; dispid 16;
    function GetAuto: OleVariant; dispid 20;
    function GetSeguridad: OleVariant; dispid 21;
    procedure SetSeguridad(Empresa: OleVariant; Valores: OleVariant); dispid 22;
    procedure UsuarioBloquea(const NombreCorto: WideString; const ComputerName: WideString; 
                             Intentos: Integer); dispid 23;
    procedure GrabaBloqueoSistema(lValorBloqueo: WordBool); dispid 19;
    function GetClave(Clave: Integer): WideString; dispid 18;
    function GetTipoLogin: Integer; dispid 301;
    procedure GrabaTipoLogin(TipoLogin: Integer); dispid 302;
    function LoginAD(const ComputerName: WideString; AllowReentry: WordBool; out Tipo: Integer; 
                     out Datos: OleVariant; out Autorizacion: OleVariant; 
                     const sDominioUsuario: WideString): Integer; dispid 303;
    function GetTipoPeriodo(Empresa: OleVariant): OleVariant; dispid 304;
    function GetSeguridadEmpresa(Empresa: OleVariant): OleVariant; dispid 305;
    function UsuarioLoginConsola(const Usuario: WideString; const Clave: WideString; 
                                 ActiveDir: WordBool; Intentos: Integer; 
                                 const EquipoLocal: WideString): WideString; dispid 306;
    function CheckVersionDatos(Empresa: OleVariant): OleVariant; dispid 307;
    function GetEmpleadosBuscados_DevEx(Empresa: OleVariant; const sPista: WideString): OleVariant; dispid 308;
    function CheckDBServer(Empresa: OleVariant): Integer; dispid 309;
    function GetCompanysList(Usuario: Integer; Lista: OleVariant): OleVariant; dispid 310;
    function GetNotificacionAdvertencia: OleVariant; dispid 311;
    procedure SetNotificacionAdvertencia(Empresa: OleVariant; Valores: OleVariant); dispid 312;
    function GetServicioReportesCorreos: OleVariant; dispid 313;
    procedure SetServicioReportesCorreos(Empresa: OleVariant; Valores: OleVariant); dispid 314;
    function EnviaClaveTemporal(const sUsuario: WideString; const sClaveCifrada: WideString; 
                                const sCorreoRemitente: WideString; 
                                const sDescripRemitente: WideString; 
                                const sCorreoValidar: WideString): WordBool; dispid 315;
    function GetStatusTimbrado(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 316;
    procedure AnaliticaContabilizarUso(Empresa: OleVariant; const sModulo: WideString; 
                                       HelpContext: Integer; Conteo: Integer); dispid 317;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer; 
                             out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                           Options: Integer; const CommandText: WideString; var Params: OleVariant; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerLogin provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerLogin exposed by              
// the CoClass dmServerLogin. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerLogin = class
    class function Create: IdmServerLogin;
    class function CreateRemote(const MachineName: string): IdmServerLogin;
  end;

implementation

uses ComObj;

class function CodmServerLogin.Create: IdmServerLogin;
begin
  Result := CreateComObject(CLASS_dmServerLogin) as IdmServerLogin;
end;

class function CodmServerLogin.CreateRemote(const MachineName: string): IdmServerLogin;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerLogin) as IdmServerLogin;
end;

end.
