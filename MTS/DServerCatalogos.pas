unit DServerCatalogos;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      DServerCatalogos.pas                       ::
  :: Descripci�n: Programa principal de Catalogos.dll        ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComServ, ComObj, VCLCom, StdVcl, DataBkr, DBClient, DB,
  MtsRdm, Mtx,
  Variants,
{$ifndef DOS_CAPAS}
  Catalogos_TLB,
{$endif}                    
  ZCreator,
  ZetaSQLBroker,
  ZetaCommonLists,
  DZetaServerProvider, ZetaServerDataSet;

{$define QUINCENALES}
{.$undefine QUINCENALES}

type
  eTipoCatalogo = ( ePuestos,     {0}
                    eTurnos,      {1}
                    eCursos,      {2}
                    eCalendario,  {3}
                    eClasifi,     {4}
                    eConceptos,   {5}
                    eCondiciones, {6}
                    eContratos,   {7}
                    eEventos,     {8}
                    eFestTurno,   {9}
                    eFolios,      {10}
                    eHorarios,    {11}
                    eInvitadores, {12}
                    eMaestros,    {13}
                    eMatrizCurso, {14}
                    eMatrizPuesto,{15}
                    eOtrasPer,    {16}
                    ePeriodos,    {17}
                    ePrestaciones,{18}
                    eReglas,      {19}
                    eRPatron,     {20}
                    eNomParam,    {21}
                    eOrdFolios,   {22}
                    eTools,       {23}
                    ePtoFijas,    {24}
                    ePtoTools,    {25}
                    eSesiones,    {26}
                    eKarCurso,    {27}
                    eAccReglas,   {28}
                    eAulas, 	  {29}
                    ePrerequisitosCurso, {30}
                    eCertificaciones,    {31}
                    eTiposPoliza,        {32}
{$ifdef QUINCENALES}
                    eTPeriodos,          {33}
{$endif}
                    ePerfilPuestos,      {34}
                    eSeccionesPerfil,    {35}
                    eCamposPerfil,       {36}
                    ePerfiles,           {37}
                    eDescPerfil,         {38}
                    eProvCap             {39}
                    ,eVistaValPlant      {40}
                    ,eValPlant           {41}
                    ,eVistaValFact       {42}
                    ,eValFact            {43}
                    ,eVistaValSubfact    {44}
                    ,eValSubFact         {45}
                    ,eValNiveles         {46}
                    ,eValuacion          {47}
                    ,eValPuntos          {48}
                    ,eRSocial            {49}
                    ,eMatrizCertificPuesto {50}
                    ,eMatrizPuestoCertific {51}
                    ,eReglasPrestamos      {52}
                    ,ePrestaXRegla         {53}
                    ,eHistRev              {54}
                    ,eEstablecimientos     {55}
                    ,eTipoPension          {56}
                    ,eSegurosGastosMed     {57}
                    ,eVigenciasSeguro      {58}
                    ,eViewVigenciasSeguro  {59}
                    ,eCosteoGrupos         {60: Grupos de Costeo}
                    ,eCosteoCriterios      {61: Criterios de Costeo}
                    ,eCriteriosPorConcepto {62: Criterios por Concepto}
                    ,eCatTablaAmortizacion {63}
                    ,eCatCostosTablaAmort  {64}
                    ,eCompetencias         {65}
                    ,eNivelesCompetencias  {66}
                    ,eRevisionCompetencias {67}
                    ,eCursosCompetencias   {68}
                    ,eCPerfiles            {69}
                    ,eRevisionPerfiles     {70}
                    ,ePerfilesCompetencias {71}
                    ,ePuestoGpoCompeten    {72}
                    ,eTipoSAT              {73}
                    ,eTPeriodoClasifi      {74}
                    ,eConceptosSeguridad   {75}
                     );

  TdmServerCatalogos = class(TMtsDataModule{$ifndef DOS_CAPAS}, IdmServerCatalogos {$endif} )
    cdsTemporal: TServerDataSet;
    procedure dmServerCatalogosCreate(Sender: TObject);
    procedure dmServerCatalogosDestroy(Sender: TObject);
  private
    { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
    {$ifndef DOS_CAPAS}
             oZetaCreator: TZetaCreator;
    {$endif}
    FTipoCatalogo: eTipoCatalogo;
    FMaxFolio: Integer;
    FMaxOrden: Integer;
    FValidaGrabado: WordBool;
    FValidaCurso, FValidaCampos: TZetaCursor;
    function GetCatalogo( const eCatalogo: eTipoCatalogo; const Empresa: OleVariant ): OLEVariant;
    function GetMatrizCurso(const sMatrizCurso: string; const Empresa: OleVariant ) : OleVariant;
    function GetMatrizPuesto(const sMatrizPuesto: string; const Empresa: OleVariant ) : OleVariant;
    function GetMaxFolioSesion: integer;
    function GetMaxFolio (sCampo:string; sTabla:string):Integer;
    function CheckEmpleadoBaja( const iEmpleado: Integer; const dFecha: TDate ): eStatusEmpleado;
    procedure InitCreator;
    procedure InitQueries;
    procedure QueriesStatusActBegin;
    procedure QueriesStatusActEnd;
    procedure SetTablaInfo( eCatalogo : eTipoCatalogo );
    procedure SetDetailInfo( eDetail : eTipoCatalogo );
    procedure GrabaCambiosBitacora( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind );
    procedure GrabaCambiosPuesto( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind );
    procedure BeforeUpdateSesion(Sender: TObject; SourceDS: TDataSet;DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind;var Applied: Boolean);
    procedure AfterUpdateSesion(Sender: TObject; SourceDS: TDataSet;DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    procedure BeforeUpdateHisCursos(Sender: TObject; SourceDS: TDataSet;DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind;var Applied: Boolean);
    procedure BeforeUpdateInvitadores(Sender: TObject; SourceDS: TDataSet;DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind;var Applied: Boolean);
    procedure BeforeUpdateDESCTIPO( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateDESC_FLD( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateDESC_PTO( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateCamposVal( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateCursos(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateMaestros(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure AfterUpdateMaestros(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind );
    procedure GrabaFechasRevisiones (Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind );
    procedure AfterUpdateConceptos(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind );
    {$ifndef DOS_CAPAS}
    procedure ServerActivate(Sender: TObject);
    procedure ServerDeactivate(Sender: TObject);
    {$endif}
    function EsCursoInValido( oDelta: TzProviderClientDataSet ; var sMensaje: String ): Integer;
    function GetCertificacionesPuesto(const Empresa: OleVariant; const sPuesto: WideString): OleVariant;
    function GetPuestosCertificacion(const Empresa: OleVariant; const sCertificacion: WideString): OleVariant;
    procedure BeforeUpdateReglasPrestamo(Sender: TObject; SourceDS: TDataSet;DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind;var Applied: Boolean);

  protected
    { Protected declarations }
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;

{$ifdef DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
{$endif}
    function GetPuestos(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetTurnos(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCursos(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCalendario(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetClasifi(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetConceptos(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCondiciones(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetContratos(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetEventos(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetFestTurno(Empresa: OleVariant; const sTurnoFestivo: WideString;iVigencia: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetFolios(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetHorarios(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetNomParam(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetRPatron(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetInvitadores(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetReglas(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetPrestaciones(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetPeriodos(Empresa: OleVariant; iYear, iTipo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetOtrasPer(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetMaestros(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetMatriz(Empresa: OleVariant; lCurso: WordBool; const sCodigo: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaMatriz(Empresa: OleVariant; lCurso: WordBool; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaCatalogo(Empresa: OleVariant; iCatalogo: Integer; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetTools(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetPercepFijas(Empresa: OleVariant; const sCodigo: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaPercepFijas(Empresa, oDelta: OleVariant; const sCodigo: WideString; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetHerramientas(Empresa: OleVariant; const sCodigo: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaHerramientas(Empresa, oDelta: OleVariant; const sCodigo: WideString; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetSesiones(Empresa: OleVariant; const sCodigo: WideString; dFechaIni, dFechaFin: TDateTime; iFolio: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaSesiones(Empresa, oDelta, oKCDelta: OleVariant; out iFolio: Integer; iOperacion: Integer; var ErrorCount, ErrorKCCount: Integer; out Valor: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetHisCursos(Empresa: OleVariant; iSesion: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetAccReglas(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetAulas(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetPrerequisitosCurso(Empresa: OleVariant; const sPrerequisitosCurso: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCertificaciones(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetTiposPoliza(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetTPeriodos(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetSeccionesPerfil(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCamposPerfil(Empresa: OleVariant; const sSeccion: WideString): OleVariant; {$ifndef DOS_CAPAS}safecall; {$endif}
    function GrabaSeccion(Empresa, oDelta: OleVariant; var ErrorCount: Integer;out iFolio: Integer): OleVariant; {$ifndef DOS_CAPAS}safecall; {$endif}
    function GrabaCamposPerfil(Empresa, oDelta: OleVariant;var ErrorCount: Integer; out iFolio: Integer): OleVariant; {$ifndef DOS_CAPAS}safecall; {$endif}
    function CambiaOrden(Empresa: OleVariant; iCatalogo, iActual,iNuevo: Integer; const sCodigo, sClasifi: WideString): OleVariant; {$ifndef DOS_CAPAS}safecall; {$endif}
    function GetPerfilesPuesto(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetPerfiles(Empresa: OleVariant;const sPuesto: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function CopiarPerfil(Empresa: OleVariant; const sFuente,sDestino: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetDescPerfil(Empresa: OleVariant; const sPuesto: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaDescPerfil(Empresa, oDelta: OleVariant;const sCampos: WideString; out ErrorCount,iOrden: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetProvCap(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetValPlantilla(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetValFactores(Empresa: OleVariant; iPlantilla: Integer): OleVariant;{$ifndef DOS_CAPAS} safecall; {$endif}
    function GetValNiveles(Empresa: OleVariant; iPlantilla,iOrden: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetValSubFactores(Empresa: OleVariant; iPlantilla: Integer): OleVariant;{$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaCamposVal(Empresa: OleVariant; iCatalogo: Integer; oDelta: OleVariant; var ErrorCount: Integer; out iFolio: Integer): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function GetValuaciones(Empresa, Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function CopiaValuacion(Empresa, Parametros: OleVariant; var ErrorCount: Integer): Integer; {$ifndef DOS_CAPAS} safecall; {$endif}
    function AgregaValuacion(Empresa, oDelta: OleVariant; var ErrorCount: Integer; out iFolio: Integer; iPlantilla: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaValuacion(Empresa, oDelta, oPDelta: OleVariant; out iFolio: Integer; var ErrorCount, PErrorCount: Integer; out Valor: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetValPuntos(Empresa: OleVariant; iValuacion: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaMaestros(Empresa, oDelta: OleVariant; var ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetRSocial(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetVistaCursos(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaMatrizCetifica(Empresa: OleVariant; lCertificacion: WordBool;Delta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetMatrizCertifica(Empresa: OleVariant; const sCodigo: WideString;lMatriz: WordBool): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetReglasPrestamo(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetPrestamoPorRegla(Empresa: OleVariant;Codigo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetHistRev(Empresa: OleVariant; const sCodigo: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaCursos(Empresa, oDelta, oDeltaH: OleVariant; out ErrorCount, ErrorCountH: Integer; out oHResult: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetGruposConceptos(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetEstablecimientos(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaEstablecimientos(Empresa, Delta: OleVariant;out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetTiposPension(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetSeguroGastosMedicos(Empresa: OleVariant;out Vigencias: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaVigencias(Empresa, Delta: OleVariant;out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCosteoCriterios(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCosteoGrupos(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCosteoCriteriosGrupos(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCosteoConceptosDisponibles(Empresa: OleVariant; const Grupo: WideString; Criterio: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetTablasAmortizacion(Empresa: OleVariant;out Costos: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaCostosTablaAmort(Empresa, Delta: OleVariant;out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCompetencias(Empresa: OleVariant; out oRevisiones, oNiveles,oCursos: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCatPerfiles(Empresa, Tipo: OleVariant; out oRevisiones,oPtoGpoComp: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaCPerfiles(Empresa, Delta, oRevisiones: OleVariant;out oRevisionesR: OleVariant; out ErrorCount,ErrorCountR: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaCompetencias(Empresa, Delta, Niveles, Cursos,Revisiones: OleVariant; out ErrorCount: Integer; out NivelesRes,CursosRes, RevisionesRes: OleVariant; out ErrorNiv, ErrorCur,ErrorRev: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetMatrizPerfilComps(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaMatrizPerfComp(Empresa: OleVariant; Delta: OleVariant; out ErrorCount: Integer): OleVariant;{$ifndef DOS_CAPAS} safecall;{$endif}
    function GetTiposSAT(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetConceptosSeguridad(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function SetPrioridadConceptoNomina(Empresa: OleVariant; Tipo,  Numero: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    end;

implementation

uses ZetaServerTools,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZGlobalTress,
     ZAccesosTress;

{$R *.DFM}

// directiva temporal se va a mostrar el dato pero no se va a hacer nada con el
{$define TJORNADA_FESTIVOS}

const
     CatalogosConDetail = [ ePrestaciones,
                            eRPatron,
                            eFolios,
                            eMatrizPuesto,
                            eMatrizCurso ,
                            eMatrizCertificPuesto,
                            eMatrizPuestoCertific];
     CatalogosConBitacora = [ eTurnos, eConceptos, eHorarios, eCondiciones, eReglas, eConceptosSeguridad ];

     QRY_DELETE = 'delete from %s where ( PU_CODIGO = %s )';

     {$IFDEF INTERBASE}
      SP_VALIDA_CAMPOS = 'SELECT MensajeError, Status FROM SP_VALIDA_CAMPOS_SESION(:MA_CODIGO,:SE_LUGAR,:SE_FEC_INI,:SE_HOR_INI,'+
                        ':SE_FEC_FIN,:SE_HOR_FIN,:SE_HORAS,:SE_CUPO,:SE_STATUS,:SE_COSTO1,:SE_COSTO2,:SE_COSTO3,:SE_COMENTA,:SE_REVISIO ) ';

      SP_VALIDA_CURSO = 'SELECT MENSAJEERROR, STATUS FROM SP_VALIDA_CURSO_ASIG( :CB_CODIGO,:CU_CODIGO,:CB_PUESTO,:CB_CLASIFI,:CB_TURNO,:KC_EVALUA,' +
                       ':KC_FEC_TOM,:KC_HORAS,:CB_NIVEL1,:CB_NIVEL2,:CB_NIVEL3,:CB_NIVEL4,:CB_NIVEL5,:CB_NIVEL6,'+
                       ':CB_NIVEL7,:CB_NIVEL8,:CB_NIVEL9,'+
                       {$ifdef ACS}':CB_NIVEL10,:CB_NIVEL11,:CB_NIVEL12,'+{$endif}
                       ':MA_CODIGO,:KC_FEC_FIN,:KC_REVISIO,:SE_FOLIO) ';
      {$ENDIF}
     {$IFDEF MSSQL}
     SP_VALIDA_CAMPOS = '{CALL SP_VALIDA_CAMPOS_SESION( :MA_CODIGO,:SE_LUGAR,:SE_FEC_INI,:SE_HOR_INI,'+
                        ':SE_FEC_FIN,:SE_HOR_FIN,:SE_HORAS,:SE_CUPO,:SE_STATUS,:SE_COSTO1,:SE_COSTO2,:SE_COSTO3,:SE_COMENTA,:SE_REVISIO, :MensajeError, :Status )}';

     SP_VALIDA_CURSO = '{CALL SP_VALIDA_CURSO_ASIG( :CB_CODIGO,:CU_CODIGO,:CB_PUESTO,:CB_CLASIFI,:CB_TURNO,:KC_EVALUA,' +
                       ':KC_FEC_TOM,:KC_HORAS,:CB_NIVEL1,:CB_NIVEL2,:CB_NIVEL3,:CB_NIVEL4,:CB_NIVEL5,:CB_NIVEL6,'+
                       ':CB_NIVEL7,:CB_NIVEL8,:CB_NIVEL9,'+
                       {$ifdef ACS}':CB_NIVEL10,:CB_NIVEL11,:CB_NIVEL12,'+{$endif}                       
                       ':MA_CODIGO,:KC_FEC_FIN,:KC_REVISIO,:SE_FOLIO,:MensajeError,:Status )}';
     {$ENDIF}




{ ********** TdmServerCatalogos *********** }

class procedure TdmServerCatalogos.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
     if Register then
     begin
          inherited UpdateRegistry(Register, ClassID, ProgID);
          EnableSocketTransport(ClassID);
          EnableWebTransport(ClassID);
     end
     else
     begin
          DisableSocketTransport(ClassID);
          DisableWebTransport(ClassID);
          inherited UpdateRegistry(Register, ClassID, ProgID);
     end;
end;

procedure TdmServerCatalogos.dmServerCatalogosCreate(Sender: TObject);
begin
     {$ifndef DOS_CAPAS}
     OnActivate := ServerActivate;
     OnDeactivate := ServerDeactivate;
     {$endif}
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
end;

procedure TdmServerCatalogos.dmServerCatalogosDestroy(Sender: TObject);
begin
     ZetaSQLBroker.FreeZetaCreator( oZetaCreator );
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
end;

{$ifdef DOS_CAPAS}
procedure TdmServerCatalogos.CierraEmpresa;
begin
end;
{$else}
procedure TdmServerCatalogos.ServerActivate(Sender: TObject);
begin
     oZetaProvider.Activate;
end;

procedure TdmServerCatalogos.ServerDeactivate(Sender: TObject);
begin
     oZetaProvider.Deactivate;
end;
{$endif}

{ ****************** Funciones y Procedimientos Privados ********************* }

procedure TdmServerCatalogos.InitCreator;
begin
     if not Assigned( oZetaCreator ) then
        oZetaCreator := ZetaSQLBroker.GetZetaCreator( oZetaProvider );
end;

procedure TdmServerCatalogos.InitQueries;
begin
     InitCreator;
     oZetaCreator.PreparaQueries;
end;

procedure TdmServerCatalogos.QueriesStatusActBegin;
begin
     InitQueries;
     oZetaCreator.Queries.GetStatusActEmpleadoBegin;
end;

procedure TdmServerCatalogos.QueriesStatusActEnd;
begin
     oZetaCreator.Queries.GetStatusActEmpleadoEnd;
end;

procedure TdmServerCatalogos.SetTablaInfo(eCatalogo: ETipoCatalogo);
begin
  with oZetaProvider.TablaInfo do
     case eCatalogo of
          eCalendario  : SetInfo( 'CALCURSO', 'CU_CODIGO,CC_FECHA', 'CU_CODIGO,CC_FECHA' );
          eInvitadores : SetInfo( 'INVITA', 'IV_CODIGO,IV_NOMBRE,CB_CODIGO,IV_ACTIVO,IV_FEC_INI,IV_FEC_FIN,IV_TOPE_1,IV_TOPE_2,IV_TOPE_3,IV_TOPE_4,IV_TOPE_5,IV_TOPE_6,IV_TOPE_7,IV_TOPE_8,IV_TOPE_9,IV_ID_NUM,IV_MIFARE'{$ifndef DOS_CAPAS} + ', IV_ID_BIO, IV_ID_GPO'{$endif}, 'IV_CODIGO' );
          eReglas      : SetInfo( 'CAFREGLA', 'CL_CODIGO,CL_LETRERO,CL_ACTIVO,CL_QUERY,CL_FILTRO,CL_TIPOS,CL_TOTAL,CL_LIMITE,CL_EXTRAS', 'CL_CODIGO' );
          eClasifi     : SetInfo( 'CLASIFI', 'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_SALARIO,TB_OP1,TB_OP2,TB_OP3,TB_OP4,TB_OP5,TB_SUB_CTA,TB_ACTIVO,TB_NIVEL0', 'TB_CODIGO' );
          ePuestos     : SetInfo( 'PUESTO', 'PU_CODIGO,PU_DESCRIP,PU_INGLES,PU_CLASIFI,PU_NUMERO,PU_TEXTO,PU_DETALLE, '+
                                            'PU_REPORTA, PU_PLAZAS, PU_NIVEL0, PU_NIVEL1, PU_NIVEL2, PU_NIVEL3, PU_NIVEL4, '+
                                            'PU_NIVEL5, PU_NIVEL6, PU_NIVEL7, PU_NIVEL8, PU_NIVEL9, '+
                                            {$ifdef ACS}'PU_NIVEL10, PU_NIVEL11, PU_NIVEL12, '+{$endif}
                                            'PU_TURNO, PU_AUTOSAL, '+
                                            'PU_CONTRAT, PU_TABLASS, PU_PATRON, PU_AREA, PU_ZONA_GE, PU_CHECA, PU_SALARIO, '+
                                            'PU_PER_VAR, PU_TIPO, PU_ACTIVO, PU_COSTO1, PU_COSTO2, PU_COSTO3, PU_LEVEL, '+
                                            'PU_SAL_MIN, PU_SAL_MAX, PU_SAL_MED, PU_SAL_EN1, PU_SAL_EN2,PU_SUB_CTA,PU_CLAVE,PU_SAT_RSG', 'PU_CODIGO' );
          eConceptos   : SetInfo( 'CONCEPTO', 'CO_A_PTU,CO_ACTIVO,CO_CALCULA,CO_DESCRIP,CO_FORMULA,CO_G_IMSS,CO_G_ISPT,CO_IMP_CAL,CO_IMPRIME,CO_LISTADO,CO_MENSUAL,CO_NUMERO,CO_QUERY,CO_RECIBO,CO_TIPO,CO_X_ISPT,CO_SUB_CTA,CO_ISN,CO_FRM_ALT,CO_USO_NOM '
{$ifdef QUINCENALES}
                                  + ',CO_CAMBIA'
{$endif}
         + ',CO_D_EXT,CO_D_NOM,CO_D_BLOB,CO_NOTA,CO_VER_INF,CO_VER_SUP,CO_LIM_INF,CO_LIM_SUP,CO_VER_ACC,CO_GPO_ACC, CO_SUMRECI'
         + ',CO_SAT_CLP, CO_SAT_TPP, CO_SAT_CLN, CO_SAT_TPN, CO_SAT_EXE, CO_SAT_CON, CO_PS_TIPO, CO_TIMBRA'


         , 'CO_NUMERO' );
        eCondiciones : SetInfo( 'QUERYS', 'QU_CODIGO,QU_DESCRIP,QU_FILTRO,QU_NIVEL,US_CODIGO,QU_NAVEGA,QU_ORDEN, QU_CANDADO', 'QU_CODIGO' );
          eTurnos      : SetInfo( 'TURNO', 'TU_CODIGO,TU_DESCRIP,TU_DIAS,TU_DOBLES,TU_DOMINGO,TU_FESTIVO, ' +
                                           'TU_HOR_1,TU_HOR_2,TU_HOR_3,TU_HOR_4,TU_HOR_5,TU_HOR_6,TU_HOR_7, '+
                                           'TU_HORARIO,TU_JORNADA,TU_RIT_INI,TU_RIT_PAT,TU_TIP_1, '+
                                           'TU_TIP_2,TU_TIP_3,TU_TIP_4,TU_TIP_5,TU_TIP_6,TU_TIP_7,TU_TIP_JOR, '+
{$ifdef QUINCENALES}
                                           'TU_DIAS_BA, ' +
{$endif}
{$ifdef TJORNADA_FESTIVOS}
                                           'TU_TIP_JT, '  +
{$endif}
                                           'TU_INGLES,TU_TEXTO,TU_NUMERO,TU_HOR_FES,TU_VACA_HA,TU_VACA_SA,TU_VACA_DE,TU_SUB_CTA,TU_ACTIVO,TU_NIVEL0,TU_SAT_JOR','TU_CODIGO');

          ePeriodos    : SetInfo( 'PERIODO', 'PE_YEAR,PE_TIPO,PE_NUMERO,PE_DESCRIP,PE_USO,PE_STATUS,PE_INC_BAJ, '+
                                             'PE_SOLO_EX,PE_FEC_INI,PE_FEC_FIN,PE_FEC_PAG,PE_MES,PE_DIAS,PE_DIAS_AC,PE_CAL, '+
{$ifdef QUINCENALES}
                                             'PE_ASI_INI, PE_ASI_FIN, '+
{$endif}
                                             'PE_DIA_MES,PE_POS_MES,PE_PER_MES,PE_PER_TOT,PE_FEC_MOD,PE_AHORRO,PE_PRESTAM, '+
                                             'PE_NUM_EMP,PE_TOT_PER,PE_TOT_NET,PE_TOT_DED,US_CODIGO,PE_TIMBRO,PE_MES_FON', 'PE_YEAR,PE_TIPO,PE_NUMERO' );
          eRPatron     : SetInfo( 'RPATRON', 'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_NUMREG,TB_MODULO,RS_CODIGO, TB_CALLE, TB_NUMEXT, '+
                                             'TB_NUMINT, TB_COLONIA, TB_CIUDAD, TB_ENTIDAD, TB_CODPOST, TB_TEL, TB_FAX, TB_EMAIL, TB_WEB, TB_CLASE, TB_FRACC, TB_STYPS, TB_SIEM, TB_PLANPRE, TB_PLANPER, TB_PLANFOL,TB_ACTIVO,TB_NIVEL0', 'TB_CODIGO' );{OP: 10/06/08}
          ePrestaciones: SetInfo( 'SSOCIAL', 'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_PRIMAVA,TB_DIAS_AG,TB_PAGO_7,TB_PRIMA_7,TB_PRIMADO,TB_DIAS_AD,TB_ACTIVO,TB_NIVEL0', 'TB_CODIGO' );
          eOtrasPer    : SetInfo( 'OTRASPER', 'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_TIPO,TB_MONTO,TB_TASA,TB_IMSS,TB_ISPT,TB_TOPE', 'TB_CODIGO' );
          eMatrizPuesto: SetInfo( 'ENTRENA', 'CU_CODIGO,PU_CODIGO,EN_DIAS,EN_OPCIONA,EN_LISTA,EN_REPROG,EN_RE_DIAS','PU_CODIGO,CU_CODIGO' );
          eMatrizCurso : SetInfo( 'ENTRENA', 'CU_CODIGO,PU_CODIGO,EN_DIAS,EN_OPCIONA,EN_LISTA,EN_REPROG,EN_RE_DIAS','PU_CODIGO,CU_CODIGO' );
          eMaestros    : SetInfo( 'MAESTRO', 'MA_CODIGO,MA_NOMBRE,MA_CEDULA,MA_RFC,MA_NUMERO,MA_TEXTO,MA_EMPRESA,MA_DIRECCI,MA_CIUDAD,MA_TEL,MA_ACTIVO,PC_CODIGO,MA_IMAGEN,MA_TAGENTE,MA_NAGENTE', 'MA_CODIGO' );
          eCursos      : SetInfo( 'CURSO', 'CU_CODIGO,CU_ACTIVO,CU_CLASIFI,CU_CLASE,CU_COSTO1,CU_COSTO2,CU_COSTO3,CU_HORAS,CU_INGLES,CU_NOMBRE,CU_TEXTO1,CU_TEXTO2,MA_CODIGO,CU_NUMERO,CU_TEXTO,CU_REVISIO,CU_FEC_REV,CU_STPS,CU_FOLIO,CU_DOCUM,AT_CODIGO,CU_OBJETIV,CU_MODALID', 'CU_CODIGO' );
          eEventos     : SetInfo( 'EVENTO', 'EV_ACTIVO,EV_ALTA,EV_ANT_FIN,EV_ANT_INI,EV_AUTOSAL,EV_BAJA, '+
                                            'EV_CLASIFI,EV_CODIGO,EV_CONTRAT,EV_DESCRIP,EV_FEC_BSS, '+
                                            'EV_FILTRO,EV_KARDEX,EV_MOT_BAJ,EV_OTRAS_1,EV_OTRAS_2, '+
                                            'EV_OTRAS_3,EV_OTRAS_4,EV_OTRAS_5,EV_PATRON,EV_NOMNUME,EV_PRIORID, '+
                                            'EV_PUESTO,EV_QUERY,EV_SALARIO,EV_TABLASS,EV_NIVEL1,EV_NIVEL2,EV_NIVEL3, '+
                                            'EV_NIVEL4,EV_NIVEL5,EV_NIVEL6,EV_NIVEL7,EV_NIVEL8,EV_NIVEL9, '+
                                            {$ifdef ACS}'EV_NIVEL10,EV_NIVEL11,EV_NIVEL12, '+{$endif}
                                            'EV_TURNO,EV_INCLUYE, '+
                                            'EV_NOMTIPO,EV_NOMYEAR,EV_TABULA,EV_M_ANTIG,EV_FEC_CON,EV_CAMPO,EV_FORMULA,EV_M_SVAC,EV_M_TVAC,EV_TIPNOM,EV_CAMBNOM', 'EV_CODIGO' );
          eFestTurno  : SetInfo( 'FESTIVO', 'TU_CODIGO,FE_MES,FE_DIA,FE_DESCRIP,FE_TIPO,FE_CAMBIO, FE_YEAR', 'TU_CODIGO,FE_MES,FE_DIA, FE_YEAR' );
          eFolios     : SetInfo( 'FOLIO', 'FL_CODIGO,FL_DESCRIP,FL_REPORTE,FL_MONTO,FL_REPITE,FL_MONEDA,FL_CEROS,FL_INICIAL,FL_FINAL,FL_FILTRO,QU_CODIGO', 'FL_CODIGO' );
          eHorarios   : SetInfo( 'HORARIO', 'HO_CODIGO,HO_ADD_EAT,HO_CHK_EAT,HO_COMER,HO_DESCRIP,HO_DOBLES,HO_EXT_FIJ,HO_IGN_EAT, '+
                                            'HO_IN_EAT,HO_IN_TARD,HO_IN_TEMP,HO_INTIME,HO_LASTOUT,HO_MIN_EAT,HO_JORNADA,HO_OU_TARD, '+
                                            'HO_OU_TEMP,HO_OUT_EAT,HO_OUTTIME,HO_TIPO,HO_EXT_COM,HO_EXT_MIN,HO_OUT_BRK,HO_IN_BRK,HO_ACTIVO,HO_PARES,HO_INGLES,HO_TEXTO,HO_NUMERO, HO_NIVEL0, HO_TIP_JT', 'HO_CODIGO' );
          eContratos  : SetInfo( 'CONTRATO', 'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_DIAS,TB_ACTIVO,TB_NIVEL0,TB_SAT_CON', 'TB_CODIGO' );
          eNomParam   : SetInfo( 'NOMPARAM', 'NP_FOLIO,NP_NOMBRE,NP_FORMULA,NP_TIPO,NP_DESCRIP,NP_ACTIVO', 'NP_FOLIO' );
          eTools      : SetInfo( 'TOOL', 'TO_CODIGO,TO_DESCRIP,TO_INGLES,TO_NUMERO,TO_TEXTO,TO_COSTO,TO_VAL_REP,TO_VAL_BAJ,TO_VIDA,TO_DESCTO', 'TO_CODIGO' );
          ePtoFijas   : SetInfo( 'PTOFIJAS', 'PU_CODIGO,PF_FOLIO,PF_CODIGO', 'PU_CODIGO,PF_FOLIO' );
          ePtoTools   : SetInfo( 'PTOTOOLS', 'PU_CODIGO,TO_CODIGO,KT_REFEREN', 'PU_CODIGO,TO_CODIGO' );
          eSesiones   : SetInfo( 'SESION', 'SE_FOLIO,CU_CODIGO,MA_CODIGO,SE_LUGAR,SE_FEC_INI,SE_FEC_FIN,SE_HOR_INI,SE_HOR_FIN,SE_HORAS,SE_CUPO,SE_COMENTA,SE_REVISIO,SE_COSTO1,SE_COSTO2,SE_COSTO3,US_CODIGO,SE_EST'{$ifdef ICUMEDICAL_CURSOS} + ',SE_D_RUTA,SE_D_EXT,SE_D_NOM' {$endif},'SE_FOLIO');
          eKarCurso   : SetInfo( 'KARCURSO', 'SE_FOLIO,CB_CODIGO,KC_EVALUA,KC_FEC_TOM,CU_CODIGO,KC_HORAS,KC_FEC_FIN,CB_CLASIFI,CB_TURNO,CB_PUESTO, MA_CODIGO, '+
                                             'CB_NIVEL1,CB_NIVEL2,CB_NIVEL3,CB_NIVEL4,CB_NIVEL5,CB_NIVEL6,CB_NIVEL7,CB_NIVEL8,CB_NIVEL9, '+
                                             {$ifdef ACS}'CB_NIVEL10,CB_NIVEL11,CB_NIVEL12, '+{$endif}
                                             'KC_REVISIO,KC_EST', 'CB_CODIGO,KC_FEC_TOM,CU_CODIGO' );
          eAccReglas  : SetInfo( 'ACCREGLA', 'AE_CODIGO,AE_TIPO,AE_LETRERO,AE_FORMULA,QU_CODIGO,AE_FILTRO,AE_SEVERI,AE_ACTIVO,US_CODIGO', 'AE_CODIGO' );
          eAulas      : SetInfo( 'AULA', 'AL_CODIGO, AL_NOMBRE, AL_INGLES, AL_NUMERO, AL_TEXTO, AL_CUPO, AL_DESCRIP, AL_ACTIVA', 'AL_CODIGO' );
          ePrerequisitosCurso : SetInfo( 'CURSOPRE', 'CU_CODIGO,CP_CURSO,CP_COMENTA,CP_OPCIONA', 'CU_CODIGO,CP_CURSO' );
          eCertificaciones    : SetInfo( 'CERTIFIC', 'CI_CODIGO,CI_NOMBRE,CI_RESUMEN,CI_DETALLE,CI_INGLES,CI_NUMERO,CI_TEXTO,CI_RENOVAR,CI_ACTIVO', 'CI_CODIGO' );
          eTiposPoliza        : SetInfo( 'POL_TIPO', 'PT_CODIGO,PT_NOMBRE,PT_INGLES,PT_NUMERO,PT_TEXTO,PT_REPORTE', 'PT_CODIGO' );
{$ifdef QUINCENALES}
          eTPeriodos  : SetInfo( 'TPERIODO', 'TP_TIPO,TP_NOMBRE,TP_DESCRIP,TP_DIAS,TP_HORAS,TP_DIAS_BT,TP_DIAS_EV,TP_HORASJO,TP_DIAS_7,TP_NIVEL0,TP_TOT_EV1,TP_TOT_EV2,TP_TOT_EV3,TP_TOT_EV4,TP_TOT_EV5,TP_CLAS', 'TP_TIPO' );
{$endif}
          eSeccionesPerfil    : SetInfo( 'DESCTIPO', 'DT_CODIGO,DT_NOMBRE,DT_INGLES,DT_NUMERO,DT_TEXTO,DT_ORDEN', 'DT_CODIGO' );
          eCamposPerfil       : SetInfo( 'DESC_FLD', 'DT_CODIGO,DF_ORDEN,DF_TITULO,DF_CAMPO,DF_CONTROL,DF_LIMITE,DF_VALORES', 'DT_CODIGO,DF_ORDEN' );
          ePerfiles           : SetInfo( 'PERFIL',   'PU_CODIGO,PF_ESTUDIO,PF_DESCRIP,PF_OBJETIV,PF_FECHA,PF_EXP_PTO,PF_POSTING,PF_CONTRL1,PF_CONTRL2,PF_NUMERO1,PF_NUMERO2,PF_TEXTO1,PF_TEXTO2,PF_EDADMIN,PF_EDADMAX,PF_SEXO','PU_CODIGO');
          eProvCap            : SetInfo( 'PROV_CAP', 'PC_CODIGO,PC_NOMBRE,PC_DIRECCI,PC_CIUDAD,PC_ESTADO,PC_PAIS,PC_CODPOST,PC_RFC,PC_CONTACT,PC_TEL,PC_FAX,PC_EMAIL,PC_WEB,PC_NUMERO,PC_TEXTO,PC_ACTIVO','PC_CODIGO');
          eVistaValPlant      : SetInfo( 'V_VALPLANT', 'VL_CODIGO,VL_NOMBRE,VL_INGLES,VL_NUMERO,VL_TEXTO,VL_FECHA,VL_NUM_PTO,VL_NUM_FAC,VL_NUM_SUB,VL_MAX_PTS,VL_STATUS,VL_MIN_PTS,VL_COMENTA,VL_TAB_PTS','VL_CODIGO');
          eValPlant           : SetInfo( 'VALPLANT', 'VL_CODIGO,VL_NOMBRE,VL_INGLES,VL_NUMERO,VL_TEXTO,VL_FECHA,VL_COMENTA,VL_TAB_PTS,VL_STATUS','VL_CODIGO');
          eVistaValFact       : SetInfo( 'V_VFACTOR', 'VL_CODIGO,VF_ORDEN,VF_CODIGO,VF_NOMBRE,VF_DESCRIP,VF_INGLES,VF_DES_ING,VF_NUMERO,VF_TEXTO,VF_NUM_SUB,VF_MIN_PTS,VF_MAX_PTS','VL_CODIGO,VF_ORDEN');
          eValFact            : SetInfo( 'VFACTOR', 'VL_CODIGO,VF_ORDEN,VF_CODIGO,VF_NOMBRE,VF_DESCRIP,VF_INGLES,VF_DES_ING,VF_NUMERO,VF_TEXTO','VF_ORDEN,VL_CODIGO');
          eVistaValSubfact    : SetInfo( 'V_VSUBFACT','VL_CODIGO,VS_ORDEN,VF_ORDEN,VS_CODIGO,VS_NOMBRE,VS_DESCRIP,VS_INGLES,VS_DES_ING,VS_NUMERO,VS_TEXTO,VS_NUM_NIV,VS_MIN_PTS,VS_MAX_PTS','VL_CODIGO,VS_ORDEN');
          eValSubfact         : SetInfo( 'VSUBFACT','VF_ORDEN,VL_CODIGO,VS_ORDEN,VS_CODIGO,VS_NOMBRE,VS_DESCRIP,VS_INGLES,VS_DES_ING,VS_NUMERO,VS_TEXTO','VS_ORDEN,VL_CODIGO');
          eValNiveles         : SetInfo( 'VALNIVEL','VL_CODIGO,VS_ORDEN,VN_ORDEN,VN_CODIGO,VN_NOMBRE,VN_DESCRIP,VN_INGLES,VN_DES_ING,VN_NUMERO,VN_TEXTO,VN_PUNTOS,VN_COLOR','VN_ORDEN,VS_ORDEN,VL_CODIGO');
          eValuacion          : SetInfo( 'VAL_PTO','VP_FOLIO,VL_CODIGO,PU_CODIGO,VP_FECHA,VP_COMENTA,VP_GRADO,VP_PT_CODE,VP_PT_NOM,VP_PT_GRAD,VP_FEC_INI,VP_FEC_FIN,US_CODIGO,VP_STATUS','VP_FOLIO');
          eValPuntos          : SetInfo( 'VPUNTOS','VP_FOLIO,VT_ORDEN,VT_CUAL,VT_COMENTA','VP_FOLIO,VT_ORDEN');
          eRSocial            : SetInfo( 'RSOCIAL','RS_CODIGO,RS_NOMBRE,RS_CALLE,RS_NUMEXT,RS_NUMINT,RS_COLONIA,RS_CIUDAD,RS_ENTIDAD,RS_CODPOST,RS_TEL,RS_FAX,RS_EMAIL,RS_WEB,RS_RFC,' +
                                         'RS_INFO,RS_GUIA,RS_RLEGAL,RS_SUBSID,RS_GIRO,RS_STPS,RS_STPS_R1,RS_STPS_R2,RS_NUMERO,RS_TEXTO,'+
                                         'RS_KEY_PR,RS_KEY_PU,RS_CURP,RS_RL_RFC,RS_RL_CURP,RS_CERT,RS_VAL_INI,'+
                                         'RS_VAL_FIN,RS_SERIAL,RS_ISSUETO,RS_ISSUEBY,RS_SELLO,RS_CONTID,CT_CODIGO', 'RS_CODIGO' );
          eMatrizCertificPuesto: SetInfo( 'PTO_CERT','CI_CODIGO,PU_CODIGO,PC_DIAS,PC_OPCIONA,PC_LISTA','CI_CODIGO,PU_CODIGO');
          eMatrizPuestoCertific: SetInfo( 'PTO_CERT','CI_CODIGO,PU_CODIGO,PC_DIAS,PC_OPCIONA,PC_LISTA','CI_CODIGO,PU_CODIGO');
          eHistRev            : SetInfo ('CUR_REV', 'CU_CODIGO,CH_REVISIO,CH_FECHA,CH_OBSERVA,US_CODIGO','CU_CODIGO, CH_REVISIO, CH_FECHA');

          eReglasPrestamos    :
                              begin
                                     SetInfo( 'REGLAPREST', 'RP_CODIGO,RP_LETRERO,RP_ORDEN,RP_ACTIVO,RP_PRE_STS,RP_LIMITE,RP_VAL_FEC,RP_TOPE,RP_EMP_STS,RP_ANT_INI,RP_ANT_FIN,QU_CODIGO,RP_FILTRO,RP_LISTA','RP_CODIGO' );
                                     BeforeUpdateRecord := BeforeUpdateReglasPrestamo;
                              end;
          ePrestaXRegla       : SetInfo( 'PRESTAXREG','RP_CODIGO,TB_CODIGO','RP_CODIGO,TB_CODIGO' );
          eEstablecimientos   : SetInfo ('ESTABLEC', 'ES_CODIGO,ES_ELEMENT,ES_NUMERO,ES_INGLES,ES_TEXTO,ES_CALLE,ES_NUMEXT,ES_NUMINT,ES_COLONIA,ES_CIUDAD,ES_ENTIDAD,ES_CODPOST,ES_ACTIVO','ES_CODIGO');
          eTipoPension        : SetInfo ('TPENSION','TP_CODIGO,TP_NOMBRE ,TP_INGLES ,TP_NUMERO ,TP_TEXTO,TP_PERCEP ,TP_PRESTA ,TP_CO_PENS ,TP_CO_DESC ,TP_APL_NOR ,TP_APL_LIQ ,TP_APL_IND,US_CODIGO,TP_DESC_N','TP_CODIGO');
          eSegurosGastosMed   : SetInfo ('POLIZA_MED','PM_CODIGO,PM_NUMERO,PM_TIPO,PM_DESCRIP,PM_ASEGURA,PM_BROKER,PM_TEL_BRK,PM_NOM_CT1,PM_COR_CT1,PM_TEL1CT1,PM_TEL2CT1,PM_NOM_CT2,PM_COR_CT2 ,PM_TEL1CT2 ,PM_TEL2CT2,PM_OBSERVA','PM_CODIGO');
          eVigenciasSeguro    : SetInfo ('POL_VIGENC','PM_CODIGO,PV_REFEREN,PV_FEC_INI,PV_FEC_FIN,PV_CONDIC,PV_TEXTO,PV_NUMERO,PV_CFIJO','PM_CODIGO,PV_REFEREN' );
          eViewVigenciasSeguro: SetInfo ('V_POL_VIG', 'PM_CODIGO,PV_REFEREN,PV_FEC_INI,PV_FEC_FIN,PV_CONDIC,PV_TEXTO,PV_NUMERO,PV_CFIJO,PV_STITULA,PV_SDEPEND', 'PM_CODIGO,PV_REFEREN' );
          eCosteoGrupos         : SetInfo( 'T_GruposCosto','GpoCostoCodigo, GpoCostoNombre, GpoCostoActivo, GpoCostoID','GpoCostoID' );
          eCosteoCriterios      : SetInfo( 'T_CriteriosCosto','CritCostoID, CritCostoNombre, CritCostoActivo','CritCostoID' );
          eCriteriosPorConcepto : SetInfo( 'T_ConceptosCosto','ConcepCostoID, CO_NUMERO, GpoCostoID, CritCostoID','ConcepCostoID' );
          eCompetencias : SetInfo( 'C_COMPETEN','CC_CODIGO,CC_ELEMENT,CC_NUMERO,CC_INGLES,CC_TEXTO,CC_DETALLE,TB_CODIGO,CC_ACTIVO,TN_CODIGO','CC_CODIGO' );
          eNivelesCompetencias : SetInfo( 'C_NIV_COMP','NC_NIVEL,NC_DESCRIP,CC_CODIGO','NC_NIVEL,CC_CODIGO' );
          eRevisionCompetencias : SetInfo( 'C_REV_COMP','RC_REVISIO,RC_OBSERVA,RC_FEC_INI,RC_FEC_FIN,US_CODIGO,CC_CODIGO','RC_FEC_INI,CC_CODIGO' );
          eCursosCompetencias :SetInfo( 'C_COMP_CUR','CC_ORDEN,CC_CODIGO,CU_CODIGO','CU_CODIGO,CC_CODIGO' );
          eCPerfiles  : SetInfo( 'C_PERFIL','CP_CODIGO,CP_ELEMENT,CP_NUMERO,CP_INGLES,CP_TEXTO,CP_DETALLE,TB_CODIGO,CP_ACTIVO','CP_CODIGO' );
          eRevisionPerfiles : SetInfo( 'C_REV_PERF','RP_REVISIO,RP_OBSERVA,RP_FEC_INI,RP_FEC_FIN,US_CODIGO,CP_CODIGO','RP_FEC_INI,CP_CODIGO' );
          ePerfilesCompetencias: SetInfo( 'C_PER_COMP','CP_CODIGO,RP_FEC_INI,CC_CODIGO,NC_NIVEL,PC_PESO','CP_CODIGO,CC_CODIGO,RP_FEC_INI' );
          ePuestoGpoCompeten : SetInfo( 'C_PERF_PTO','CP_CODIGO,PU_CODIGO,PP_ACTIVO,US_CODIGO,PP_PESO,PP_FEC_REG','CP_CODIGO,PU_CODIGO' );
          eCatTablaAmortizacion : SetInfo( 'AMORTIZ_TB','AT_CODIGO,AT_DESCRIP,AT_INGLES,AT_NUMERO,AT_TEXTO,AT_ACTIVO','AT_CODIGO' );
          eCatCostosTablaAmort  : SetInfo( 'AMORTIZ_CS','AT_CODIGO,AC_EDADINI,AC_EDADFIN,AC_CHOMBRE,AC_CMUJER','AT_CODIGO,AC_EDADINI,AC_EDADFIN' );
          eTipoSAT       : SetInfo ('TIPO_SAT','TB_CODIGO,TB_ELEMENT,TB_SAT_CLA,TB_SAT_NUM,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_ACTIVO','TB_CODIGO');
          eTPeriodoClasifi: SetInfo( 'TPERIODO_CLASIFI', 'TP_CLAS,TP_NOMBRE', 'TP_CLAS' );
          eConceptosSeguridad   : SetInfo( 'CONCEPTO', 'CO_A_PTU,CO_ACTIVO,CO_CALCULA,CO_DESCRIP,CO_G_IMSS,CO_G_ISPT,CO_IMPRIME,CO_LISTADO,CO_MENSUAL,CO_NUMERO,CO_QUERY,CO_TIPO,CO_SUB_CTA,CO_ISN,CO_USO_NOM '
            {$ifdef QUINCENALES}
                                  + ',CO_CAMBIA'
            {$endif}
         + ',CO_D_EXT,CO_D_NOM,CO_D_BLOB,CO_NOTA,CO_VER_INF,CO_VER_SUP,CO_LIM_INF,CO_LIM_SUP,CO_VER_ACC,CO_GPO_ACC, CO_SUMRECI,CO_PS_TIPO', 'CO_NUMERO' );
     end;
end;

procedure TdmServerCatalogos.SetDetailInfo(eDetail: eTipoCatalogo);
begin
     with oZetaProvider.DetailInfo do
     begin
          case eDetail of
               ePrestaciones: SetInfoDetail( 'PRESTACI', 'TB_CODIGO,PT_YEAR,PT_DIAS_VA,PT_PRIMAVA,PT_DIAS_AG,PT_DIAS_AD,PT_PRIMADO,PT_PAGO_7,PT_PRIMA_7,PT_FACTOR',
                                                   'TB_CODIGO,PT_YEAR', 'TB_CODIGO' );
               eRPatron : SetInfoDetail( 'PRIESGO', 'TB_CODIGO,RT_FECHA,RT_PRIMA,RT_PTANT,RT_S,RT_I,RT_D,RT_N,RT_M,RT_F',
                                                    'TB_CODIGO,RT_FECHA', 'TB_CODIGO' );
               eFolios  : SetInfoDetail( 'ORDFOLIO', 'FL_CODIGO,OF_POSICIO,OF_CAMPO,OF_TITULO,OF_DESCEND',
                                                     'FL_CODIGO,OF_POSICIO', 'FL_CODIGO' );
               eMatrizCurso, eMatrizPuesto :
                             SetInfoDetail( 'ENTNIVEL', 'PU_CODIGO,CU_CODIGO,ET_CODIGO',
                                                        'PU_CODIGO,CU_CODIGO,ET_CODIGO', 'PU_CODIGO,CU_CODIGO' );
               eMatrizCertificPuesto,eMatrizPuestoCertific :
                             SetInfoDetail( 'CERNIVEL', 'PU_CODIGO,CI_CODIGO,CN_CODIGO',
                                                        'PU_CODIGO,CI_CODIGO,CN_CODIGO', 'PU_CODIGO,CI_CODIGO' );
//               eHistRev :  SetInfoDetail( 'CUR_REV', 'CU_CODIGO,CH_REVISIO,CH_FECHA,CH_OBSERVA,US_CODIGO', 'CU_CODIGO, CH_REVISIO, CH_FECHA', 'CU_CODIGO' );
          end;
     end;
end;

function TdmServerCatalogos.GetCatalogo( const eCatalogo: eTipoCatalogo; const Empresa: OleVariant ): OLEVariant;
begin
     SetTablaInfo( eCatalogo );
     if ( eCatalogo in CatalogosConDetail ) then
     begin
          SetDetailInfo( eCatalogo );
          Result := oZetaProvider.GetMasterDetail( Empresa );
     end
     else
         Result := oZetaProvider.GetTabla( Empresa );
end;

function TdmServerCatalogos.GetMatrizCurso(const sMatrizCurso: string; const Empresa: OleVariant): OleVariant;
begin
     SetTablaInfo( eMatrizCurso );
     with oZetaProvider do
     begin
          TablaInfo.SetOrder( 'PU_CODIGO' );
          TablaInfo.Filtro := Format( '( CU_CODIGO = ''%s'' )', [ sMatrizCurso ] );
          SetDetailInfo( eMatrizCurso );
          Result := GetMasterDetail( Empresa );
     end;
end;

function TdmServerCatalogos.GetMatrizPuesto(const sMatrizPuesto: string; const Empresa: OleVariant): OleVariant;
const
     Q_GET_ENTRENA_JOIN_CURSO = 'select CURSO.CU_REVISIO, CU_CODIGO,PU_CODIGO,EN_DIAS,EN_OPCIONA,EN_LISTA,EN_RE_DIAS,EN_REPROG,CURSO.CU_TIPO '+
                                'from ENTRENA '+
                                'left outer join CURSO on ( CURSO.CU_CODIGO = ENTRENA.CU_CODIGO ) '+
                                'where PU_CODIGO = %s '+
                                'order by CU_CODIGO ';

begin
     SetTablaInfo( eMatrizPuesto );
     with oZetaProvider do
     begin
          TablaInfo.SetOrder( 'CU_CODIGO' );
          TablaInfo.Filtro := Format( '( PU_CODIGO = ''%s'' )', [ sMatrizPuesto ] );
          //Result := OpenSQL( Empresa, Format( Q_GET_ENTRENA_JOIN_CURSO, [ EntreComillas(sMatrizPuesto) ] ), True );
          SetDetailInfo( eMatrizPuesto );
          Result := GetMasterDetail( Empresa );
     end;
end;

procedure TdmServerCatalogos.GrabaCambiosPuesto( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind );
const
     {$ifdef INTERBASE}
          Q_CHECAPUESTOS = 'execute procedure CHECK_REPORT_CHAIN( %s )';
     {$endif}
     {$ifdef MSSQL}
          Q_CHECAPUESTOS = 'exec dbo.CHECK_REPORT_CHAIN %s';
     {$endif}
var
   FPuestos: TZetaCursor;
begin
     if ( UpdateKind in [ ukInsert, ukModify ] ) then
     begin
          if CambiaCampo( DeltaDS.FieldByName( 'PU_REPORTA' ) ) then
          begin
               with oZetaProvider do
               begin
                    FPuestos := CreateQuery( Format( Q_CHECAPUESTOS, [ EntreComillas( DeltaDS.FieldByName( 'PU_REPORTA' ).AsString ) ] ) );
                    try
                       Ejecuta( FPuestos );
                    finally
                           FreeAndNil( FPuestos );
                    end;
               end;
          end;
     end;
     GrabaCambiosBitacora( Sender, SourceDS, DeltaDS, UpdateKind );
end;

procedure TdmServerCatalogos.GrabaCambiosBitacora(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
var
   eClase: eClaseBitacora;
   sTitulo: String;

 procedure DatosBitacora( eClaseCatalogo:eClaseBitacora; sDescripcion, sKeyField : string );
  var
     oField : TField;
 begin
      eClase  := eClaseCatalogo;
      sTitulo := sDescripcion;

      if StrLleno(sKeyField) then
      begin
           oField := DeltaDS.FieldByName( sKeyField );

           if oField is TNumericField then
              sTitulo := sTitulo + ' ' + IntToStr( ZetaServerTools.CampoOldAsVar( oField ) )
           else
               sTitulo := sTitulo + ' ' + ZetaServerTools.CampoOldAsVar( oField );
      end;
 end;

begin
     if UpdateKind in [ ukModify, ukDelete ] then
     begin
          eClase := clbNinguno;

          case FTipoCatalogo of
                    ePuestos     : DatosBitacora( clbPuesto, 'Puesto :', 'PU_CODIGO' );
                    eTurnos      : DatosBitacora( clbTurno, 'Turno :', 'TU_CODIGO' );
                    eCursos      : DatosBitacora( clbCursos, 'Curso :', 'CU_CODIGO' );
                    eClasifi     : DatosBitacora( clbClasifi, 'Clasificaci�n :', 'TB_CODIGO' );
                    eConceptos   : DatosBitacora( clbConcepto, 'Concepto #', 'CO_NUMERO' );
                    eCondiciones : DatosBitacora( clbCondicion, 'Condici�n :', 'QU_CODIGO' );
                    eContratos   : DatosBitacora( clbContratos, 'Contrato :', 'TB_CODIGO' );
                    eEventos     : DatosBitacora( clbEventos, 'Evento :', 'EV_CODIGO' );
                    eFolios      : DatosBitacora( clbFolios, 'Folio :', 'FL_CODIGO' );
                    eHorarios    : DatosBitacora( clbHorario, 'Horario :', 'HO_CODIGO' );
                    eInvitadores : DatosBitacora( clbInvitadores, 'Invitador :', 'IV_CODIGO' );
                    eMaestros    : DatosBitacora( clbMaestros, 'Maestro :', 'MA_CODIGO' );
                    eOtrasPer    : DatosBitacora( clbOtrasPer, 'Percepci�n Fija :', 'TB_CODIGO' );
                    ePrestaciones: DatosBitacora( clbPrestaciones, 'Prestaci�n :', 'TB_CODIGO' );
                    eReglas      : DatosBitacora( clbReglas, 'Regla de Cafeter�a :', 'CL_CODIGO' );
                    eAccReglas   : DatosBitacora( clbAccReglas, 'Regla de Acceso :', 'AE_CODIGO' );
                    eRPatron     : DatosBitacora( clbRPatron, 'Registro Patronal :', 'TB_CODIGO' );
                    eNomParam    : DatosBitacora( clbNomParam, 'Par�metro de N�mina :', 'NP_FOLIO' );
                    eTools       : DatosBitacora( clbTools, 'Herramienta :', 'TO_CODIGO' );
                    eCalendario  : DatosBitacora( clbCalendario, 'Curso: ' + ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CU_CODIGO' ) ) +
                                                                 ' Fecha: ' + FormatDateTime('dd/mmm/yyyy', ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CC_FECHA' ) ) ) , VACIO );

                    eFestTurno   : DatosBitacora( clbFestTurno, 'Turno: '+ ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'TU_CODIGO' ) ) +
                                                                ' Mes: ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'FE_MES' ) ) ) +
                                                                ' D�a: ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'FE_DIA' ) ) ), VACIO );
                    ePeriodos    :
                    begin
                         oZetaProvider.InitArregloTPeriodo;
                         DatosBitacora( clbPeriodos, 'Periodo ' + GetPeriodoInfo( CampoOldAsVar( DeltaDS.FieldByName( 'PE_YEAR' ) ),
                                                                                            CampoOldAsVar( DeltaDS.FieldByName( 'PE_NUMERO' ) ),
                                                                                            eTipoPeriodo( CampoOldAsVar( DeltaDS.FieldByName( 'PE_TIPO' ) ) ) ) , VACIO );
                    end;
                    eMatrizCurso : DatosBitacora( clbMatrizCurso, 'Curso: ' +  ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CU_CODIGO' ) ) +
                                                                  ' Puesto: ' + ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'PU_CODIGO' ) ), VACIO );
                    eMatrizPuesto: DatosBitacora( clbMatrizPuesto, 'Puesto: ' + ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'PU_CODIGO' ) ) +
                                                                   ' Curso: ' +  ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CU_CODIGO' ) ), VACIO );
                    eSesiones    : DatosBitacora( clbSesiones, 'Puesto: ' + ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CB_PUESTO' ) ) +
                                                               ' Curso: ' +  ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CU_CODIGO' ) ),
                                                                VACIO );
                    eAulas      : DatosBitacora( clbAulas, 'Aula :', 'AL_CODIGO' );

                    ePrerequisitosCurso : DatosBitacora( clbPrerequisitosCurso, 'Curso: ' + ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CU_CODIGO' ) ) +
                                                                                 'Curso Requerido: ' + ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CP_CURSO' ) ), VACIO );

                    eCertificaciones    : DatosBitacora( clbCertificaciones, 'Certificaci�n: ' +
                                                          ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CI_CODIGO' )  ), VACIO );
                    eTiposPoliza        : DatosBitacora( clbTiposPoliza, 'Tipos de P�liza: ' + ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'PT_CODIGO' )  ), VACIO );
{$ifdef QUINCENALES}
                    eTPeriodos  : DatosBitacora( clbPeriodos, 'Tipo de periodo: ', 'TP_TIPO' );
{$endif}
                    eSeccionesPerfil     : DatosBitacora( clbSeccionesPerfil, 'Secci�n de Perfil: ' + ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'DT_CODIGO' )  ), VACIO );

                    eCamposPerfil        : DatosBitacora( clbCamposPerfil, 'Campo de Perfil: ' + ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'DT_CODIGO' )  ), VACIO );

                    ePerfiles            : DatosBitacora( clbPerfiles, 'Perfil: ' + ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'PU_CODIGO' )  ), VACIO );

                    eProvCap             : DatosBitacora( clbProvCap, 'Proveedor de Capacitaci�n: ' + ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'PC_CODIGO' )  ), VACIO );
{$ifndef DOS_CAPAS}
                    eValPlant            : DatosBitacora( clbValPlantillas, 'Plantilla: ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'VL_CODIGO' ) ) ), VACIO );

                    eValFact             : DatosBitacora( clbFactores, 'Plantilla: ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'VL_CODIGO' ) ) )
                                                                      + ' Factor:'+ ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'VF_CODIGO' ) ), VACIO );

                    eValSubfact          : DatosBitacora( clbSubfactores, 'Plantilla: ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'VL_CODIGO' ) ) )
                                                                         + ' Subfactor:'+ ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'VS_CODIGO' ) ), VACIO );

                    eValNiveles          : DatosBitacora( clbValNiveles, 'Nivel: ' + ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'VN_CODIGO' )  )
                                                                       + ' Subfactor:' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'VS_ORDEN' ) ) ), VACIO );
{$endif}
                   eRSocial              : DatosBitacora( clbRSocial, 'Raz�n Social: ' + ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'RS_CODIGO' ) ), VACIO );

                   eMatrizCertificPuesto : DatosBitacora( clbMatrizCertificaPuesto, 'Certificaci�n: ' +  ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CI_CODIGO' ) ) +
                                                                  ' Puesto: ' + ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'PU_CODIGO' ) ), VACIO );
                   eMatrizPuestoCertific : DatosBitacora( clbMatrizPuestoCertifica, 'Puesto: ' + ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'PU_CODIGO' ) ) +
                                                                   ' Certificaci�n: ' +  ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CI_CODIGO' ) ), VACIO );
                   eReglasPrestamos      : DatosBitacora( clbReglaPrestamo, 'C�digo: ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'RP_CODIGO' ) ) ) +
                                                                   ' Mensaje: ' +  ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'RP_LETRERO' ) ), VACIO );
                   ePrestaXRegla         : DatosBitacora( clbPrestaXRegla, 'C�digo: ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'RP_CODIGO' ) ) ) +
                                                                   ' Tipo de Pr�stamo: ' +  ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'TB_CODIGO' ) ), VACIO );
                   eTipoPension         : DatosBitacora( clbCatNomTPension , 'Tipo de Pensi�n: ' + ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'TP_CODIGO' ) ),VACIO );

                   eSegurosGastosMed    : DatosBitacora( clbCatGralSGM , 'Seguro de Gastos M�dicos: ' + ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'PM_CODIGO' ) ),VACIO );

                   eVigenciasSeguro     : DatosBitacora( clbCatGralSGM , 'Vigencias de SGM: ' + ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'PM_CODIGO' ) )+ ' Vigencia: '+ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'PV_REFEREN' ) ),VACIO );

                   eCosteoGrupos         : DatosBitacora( clbCosteoGrupos, 'Grupo de Costeo:', 'GpoCostoCodigo' );
                   eCosteoCriterios      : DatosBitacora( clbCosteoCriterios, 'Criterio de Costeo:', 'CritCostoID' );
                   eCriteriosPorConcepto : DatosBitacora( clbCosteoCriteriosPorConcepto, 'Concepto Criterio:', 'CO_NUMERO' );

                   eCatTablaAmortizacion : DatosBitacora( clbCatGralSGM , 'Tabla de Cotizaci�n SGM: ' + ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'AT_CODIGO' ) ),VACIO );
                   eCatCostosTablaAmort  : DatosBitacora( clbCatGralSGM , 'Costos de Cotizaci�n SGM: ' + ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'AT_CODIGO' ) ),VACIO );

                   eCompetencias         : DatosBitacora( clbCatCompetencias, 'Competencia:', 'CC_CODIGO' );
                   eRevisionCompetencias : DatosBitacora( clbCatCompetencias, 'Competencia: ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CC_CODIGO' ) ) ) +
                                                                   ' Revision: ' +  ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'RC_REVISIO' ) ), VACIO );
                   eNivelesCompetencias  : DatosBitacora( clbCatCompetencias, 'Competencia: ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CC_CODIGO' ) ) ) +
                                                                   ' Nivel: ' +  ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'NC_NIVEL' ) ), VACIO );
                   eCursosCompetencias   : DatosBitacora( clbCatCompetencias, 'Competencia: ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CC_CODIGO' ) ) ) +
                                                                   ' Curso: ' +  ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CU_CODIGO' ) ), VACIO );
                   eCPerfiles            : DatosBitacora( clbCatPerfiles, 'Perfil:', 'CP_CODIGO' );
                   eRevisionPerfiles     : DatosBitacora( clbCatPerfiles, 'Perfil: ' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CP_CODIGO' ) ) ) +
                                                                   ' Revisi�n: ' +  ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'RP_REVISIO' ) ), VACIO );
                   ePerfilesCompetencias : DatosBitacora( clbPerfilesCompetencias, 'Perfil: ' + ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CP_CODIGO' ) )  +
                                                                   ' Competencia: ' +  ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CC_CODIGO' ) ), VACIO );
                   ePuestoGpoCompeten    : DatosBitacora( clbPuesto, 'Grupo Compentencia: ' + ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CP_CODIGO' )) +
                                                                   ' Puesto: ' +  ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'PU_CODIGO' ) ), VACIO );
                   eTipoSat              : DatosBitacora( clbTipoSat, 'Tipo de concepto SAT :', 'TB_CODIGO' );
                   
                   eConceptosSeguridad   : DatosBitacora( clbConcepto, 'Concepto #', 'CO_NUMERO' );

                    else raise Exception.Create( 'No ha sido Integrado el Cat�logo al Case de <DServerCatalogos.GrabaCambiosBitacora>' )
          end;

          with oZetaProvider do
            if UpdateKind = ukModify then
                CambioCatalogo( sTitulo, eClase, DeltaDS )
            else
                BorraCatalogo( sTitulo, eClase, DeltaDS );
     end;
end;

{ ********* Interfases ( Paletitas ) ******** }

function TdmServerCatalogos.GetPuestos(Empresa: OleVariant): OleVariant;
begin
     Result := GetCatalogo( ePuestos, Empresa );
     SetComplete;
end;

function TdmServerCatalogos.GetTurnos(Empresa: OleVariant): OleVariant;
begin
     SetTablaInfo( eTurnos );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( '(TU_CODIGO <> ''%s'' )', [ TURNO_VACIO ] );
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerCatalogos.GetCursos(Empresa: OleVariant): OleVariant;
const
     Q_GET_CURSOS = 'select CU_CODIGO,CU_ACTIVO,CU_CLASIFI,CU_CLASE,CU_COSTO1,CU_COSTO2,CU_COSTO3,CU_HORAS,CU_INGLES,CU_NOMBRE,CU_TEXTO1,CU_TEXTO2,MA_CODIGO,CU_NUMERO,CU_TEXTO,CU_REVISIO,CU_FEC_REV,CU_STPS,CU_FOLIO,CU_DOCUM,AT_CODIGO,CU_OBJETIV,CU_MODALID, ' +
                    'CU_NOMBRE as CU_DESCRIP '+
                    'from CURSO '+
                    'order by CU_CODIGO';
begin
//   Result := GetCatalogo( eCursos, Empresa );
     with oZetaProvider do
     begin
          with cdsTemporal do
          begin
               Lista := OpenSQL( Empresa, Q_GET_CURSOS, TRUE );
               while ( not EOF ) do
               begin
                    if strLleno( FieldByName( 'CU_FOLIO' ).AsString ) then
                    begin
                         Edit;
                         FieldByName( 'CU_DESCRIP' ).AsString := FieldByName( 'CU_FOLIO' ).AsString + ' : ' +
                                                                 FieldByName( 'CU_NOMBRE' ).AsString;
                         Post;
                    end;
                    Next;
               end;
               MergeChangeLog;
               Result := Lista;
          end;
     end;
     SetComplete;
end;

function TdmServerCatalogos.GrabaCursos(Empresa, oDelta, oDeltaH: OleVariant; out ErrorCount, ErrorCountH: Integer; out oHResult: OleVariant): OleVariant;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     if ( not VarIsNull( oDelta ) ) then
     begin
          SetTablaInfo( eCursos );
          Result := oZetaProvider.GrabaTabla( Empresa, oDelta, ErrorCount );
     end
     else
         Result := oDelta;
         if ( ErrorCount = 0 ) and ( not VarIsNull ( oDeltaH ) ) then
         begin
              SetTablaInfo( eHistRev );
              oHResult := oZetaProvider.GrabaTablaGrid( Empresa, oDeltaH, ErrorCountH );
         end
         else
              oHResult := oDeltaH;
     SetComplete;
end;

function TdmServerCatalogos.GetHistRev(Empresa: OleVariant; const sCodigo: WideString): OleVariant;
begin
     SetTablaInfo( eHistRev );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( '(CU_CODIGO = ''%s'' )', [ sCodigo ] );
          Result := GetTabla( Empresa );
     end;
     SetComplete;   
end;

function TdmServerCatalogos.GetCalendario(Empresa: OleVariant): OleVariant;
begin
     Result := GetCatalogo( eCalendario, Empresa );
     SetComplete;
end;

function TdmServerCatalogos.GetClasifi(Empresa: OleVariant): OleVariant;
begin
     Result := GetCatalogo( eClasifi, Empresa );
     SetComplete;
end;

function TdmServerCatalogos.GetConceptos(Empresa: OleVariant): OleVariant;
begin
     Result := GetCatalogo( eConceptos, Empresa );
     SetComplete;
end;

function TdmServerCatalogos.GetCondiciones(Empresa: OleVariant): OleVariant;
begin
     Result := GetCatalogo( eCondiciones, Empresa );
     SetComplete;
end;

function TdmServerCatalogos.GetContratos(Empresa: OleVariant): OleVariant;
begin
     Result := GetCatalogo( eContratos, Empresa );
     SetComplete;
end;

function TdmServerCatalogos.GetEventos(Empresa: OleVariant): OleVariant;
begin
     Result := GetCatalogo( eEventos, Empresa );
     SetComplete;
end;

function TdmServerCatalogos.GetFestTurno(Empresa: OleVariant; const sTurnoFestivo: WideString; iVigencia: Integer): OleVariant;
begin
     SetTablaInfo( eFestTurno );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( '(TU_CODIGO = ''%s'' ) AND (( FE_YEAR = %d) OR ( FE_YEAR = 0 ))', [ sTurnoFestivo, iVigencia ] );
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerCatalogos.GetFolios(Empresa: OleVariant): OleVariant;
begin
     Result:= GetCatalogo( eFolios, Empresa );
     SetComplete;
end;

function TdmServerCatalogos.GetHorarios(Empresa: OleVariant): OleVariant;
begin
     Result := GetCatalogo( eHorarios, Empresa );
     SetComplete;
end;

function TdmServerCatalogos.GetNomParam(Empresa: OleVariant): OleVariant;
begin
     Result := GetCatalogo( eNomParam, Empresa );
     SetComplete;
end;

function TdmServerCatalogos.GetRPatron(Empresa: OleVariant): OleVariant;
begin
     Result := GetCatalogo( eRPatron, Empresa );
     SetComplete;
end;

function TdmServerCatalogos.GetInvitadores(Empresa: OleVariant): OleVariant;
var
   sFacilityCode: string;
begin
     with cdsTemporal, oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
          InitTempDataset;
          Data := GetCatalogo( eInvitadores, Empresa );

          while not EOF do
          begin
               if not ( State in [dsEdit, dsInsert] ) then
                  Edit;
               sFacilityCode := GetGlobalString( K_GLOBAL_PROXIMITY_FACILITY_CODE );
               if ( FieldByName( 'IV_MIFARE' ).AsString = K_GLOBAL_SI ) then
                  sFacilityCode := VACIO;
               FieldByName( 'IV_ID_NUM' ).AsString := ZetaServerTools.ProximidadHexToDec( FieldByName( 'IV_ID_NUM' ).AsString,
                                                                                     sFacilityCode,
                                                                                     GetGlobalInteger( K_GLOBAL_PROXIMITY_TOTAL_BYTES ) );
               Post;
               Next;
          end;
          MergeChangeLog;
          Result := Data;
     end;
     SetComplete;
end;

function TdmServerCatalogos.GetReglas(Empresa: OleVariant): OleVariant;
begin
     Result := GetCatalogo( eReglas, Empresa );
     SetComplete;
end;

function TdmServerCatalogos.GetPrestaciones(Empresa: OleVariant): OleVariant;
begin
     Result := GetCatalogo( ePrestaciones, Empresa );
     SetComplete;
end;

function TdmServerCatalogos.GetPeriodos(Empresa: OleVariant; iYear, iTipo: Integer): OleVariant;
begin
     SetTablaInfo( ePeriodos );
     with oZetaProvider do
     begin
          if ( iTipo = - 1 ) then
          begin
               TablaInfo.Filtro := Format( '( PE_YEAR = %d )', [ iYear ] );
          end
          else
              TablaInfo.Filtro := Format( '( PE_YEAR = %d ) and ( PE_TIPO = %d )', [ iYear, iTipo ] );
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerCatalogos.GetOtrasPer(Empresa: OleVariant): OleVariant;
begin
     Result := GetCatalogo( eOtrasPer, Empresa );
     SetComplete;
end;

function TdmServerCatalogos.GetMaestros(Empresa: OleVariant): OleVariant;
begin
     Result := GetCatalogo( eMaestros, Empresa );
     SetComplete;
end;

function TdmServerCatalogos.GetProvCap(Empresa: OleVariant): OleVariant;
begin
     Result := GetCatalogo( eProvCap, Empresa );
     SetComplete;
end;

function TdmServerCatalogos.GetMatriz(Empresa: OleVariant; lCurso: WordBool; const sCodigo: WideString): OleVariant;
begin
     if ( lCurso ) then
        Result := GetMatrizCurso( sCodigo, Empresa )
     else
        Result := GetMatrizPuesto( sCodigo, Empresa );
     SetComplete;
end;

function TdmServerCatalogos.GrabaCatalogo(Empresa: OleVariant; iCatalogo: Integer; oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     FTipoCatalogo := eTipoCatalogo( iCatalogo );

     SetTablaInfo( FTipoCatalogo );
     // Cat�logos que graban en bit�cora las modificaciones
     {CV: Este C�digo estaba hasta la version 2.3.96
     if ( FTipoCatalogo in CatalogosConBitacora ) then
     begin
        oZetaProvider.EmpresaActiva := Empresa;    // Lo requiere Bit�cora
        oZetaProvider.TablaInfo.AfterUpdateRecord := GrabaCambiosBitacora;
     end;}


     {CV: A Partir de la version 2.4.97, TODOS los catalogos
     grabar�n a bitacoras sus cambios, por lo que hay que integrar el Cat�logo al CASE
     del Evento GrabaCambiosBitacora. }
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;    // Lo requiere Bit�cora
          InitGlobales;
          if ( FTipoCatalogo = eSesiones ) then
             TablaInfo.BeforeUpdateRecord := BeforeUpdateSesion
          else if ( FTipoCatalogo = eInvitadores ) then
                  TablaInfo.BeforeUpdateRecord := BeforeUpdateInvitadores
          else if ( FTipoCatalogo = eCursos ) then
                  TablaInfo.BeforeUpdateRecord := BeforeUpdateCursos;
          if ( FTipoCatalogo = ePuestos ) then
             TablaInfo.AfterUpdateRecord := GrabaCambiosPuesto
          //US 17266: Almacenar la clave SAT cuando se timbra la n�mina para obtener reportes y auditar timbrado
          else if ( FTipoCatalogo = eConceptos ) then
                  TablaInfo.AfterUpdateRecord := AfterUpdateConceptos
          else
             TablaInfo.AfterUpdateRecord := GrabaCambiosBitacora;
          if ( FTipoCatalogo in CatalogosConDetail ) then
          begin
               SetDetailInfo( FTipoCatalogo );
               Result := GrabaMasterDetail( Empresa, oDelta, ErrorCount );
          end
          else
              Result := GrabaTabla( Empresa, oDelta, ErrorCount );
     end;
     SetComplete;
end;

function TdmServerCatalogos.GrabaMatriz(Empresa: OleVariant; lCurso: WordBool; oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     if ( lCurso ) then
        Result := GrabaCatalogo( Empresa, Ord( eMatrizCurso ), oDelta, ErrorCount )
     else
        Result := GrabaCatalogo( Empresa, Ord( eMatrizPuesto ), oDelta, ErrorCount );
     SetComplete;
end;

function TdmServerCatalogos.GetTools(Empresa: OleVariant): OleVariant;
begin
     Result := GetCatalogo( eTools, Empresa );
     SetComplete;
end;

function TdmServerCatalogos.GetPercepFijas(Empresa: OleVariant; const sCodigo: WideString): OleVariant;
begin
     SetTablaInfo( ePtoFijas );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( '(PU_CODIGO = %s )', [ EntreComillas( sCodigo ) ] );
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerCatalogos.GrabaPercepFijas(Empresa, oDelta: OleVariant; const sCodigo: WideString; out ErrorCount: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          EmpiezaTransaccion;
          try
             ExecSQL( Empresa, Format( QRY_DELETE, [ 'PTOFIJAS', EntreComillas( sCodigo ) ] ) );
             TerminaTransaccion( TRUE );
          except
                on Error: Exception do
                begin
                     TerminaTransaccion( FALSE );
                     raise;
                end;
          end;
          if VarIsNull( oDelta ) then
             Result := oDelta
          else
          begin
               SetTablaInfo( ePtoFijas );
               Result := GrabaTabla( Empresa, oDelta, ErrorCount );
          end;
     end;
     SetComplete;
end;

function TdmServerCatalogos.GetHerramientas(Empresa: OleVariant; const sCodigo: WideString): OleVariant;
const
     K_FILTRO = 'PU_CODIGO = %s';
begin
     SetTablaInfo( ePtoTools );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( K_FILTRO, [ EntreComillas( sCodigo ) ] );
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerCatalogos.GrabaHerramientas(Empresa, oDelta: OleVariant; const sCodigo: WideString; out ErrorCount: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          EmpiezaTransaccion;
          try
             ExecSQL( Empresa, Format( QRY_DELETE, [ 'PTOTOOLS', EntreComillas( sCodigo ) ] ) );
             TerminaTransaccion( TRUE );
          except
                on Error: Exception do
                begin
                     TerminaTransaccion( FALSE );
                     raise;
                end;
          end;
          if VarIsNull( oDelta ) then
             Result := oDelta
          else
          begin
               SetTablaInfo( ePtoTools );
               Result := GrabaTabla( Empresa, oDelta, ErrorCount );
          end;
     end;
     SetComplete;
end;

function TdmServerCatalogos.GetSesiones(Empresa: OleVariant; const sCodigo: WideString; dFechaIni, dFechaFin: TDateTime;
  iFolio: Integer): OleVariant;
var
   sCurso: String;
begin
     SetTablaInfo( eSesiones );
     with oZetaProvider do
     begin
          if( iFolio = 0 )then
          begin
               if( strLleno( sCodigo ) )then
                   sCurso :=  Format('( CU_CODIGO = %s ) and ', [ EntreComillas( sCodigo ) ])
               else
                   sCurso := VACIO;
               TablaInfo.Filtro := Format( '%s ( SE_FEC_INI >= %s ) and ( SE_FEC_FIN <= %s )', [ sCurso, DatetoStrSQLC( dFechaIni ), DatetoStrSQLC( dFechaFin ) ] )
          end
          else
              TablaInfo.Filtro := Format( '( SE_FOLIO = %d )', [ iFolio ] );
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerCatalogos.GetMaxFolio (sCampo:string; sTabla:string):Integer;
const
     Q_GET_MAX_FOLIO = 'select MAX(%s) MAXIMO from %s ';
var
    FDataSet : TZetaCursor;
begin
     FDataset := oZetaProvider.CreateQuery( Format(Q_GET_MAX_FOLIO,[sCampo,sTabla]) );
     if ( FDataSet <> nil )then
     begin
          try
             FDataSet.Open;
             Result := FDataSet.FieldByName('MAXIMO').AsInteger + 1;
             FDataSet.Close;
          finally
                 FreeAndNil(FDataSet);
          end;
     end
     else
     begin
          Result := 1; //Se inicializa el orden en la tabla
     end;

end;

function TdmServerCatalogos.GetMaxFolioSesion: integer;
begin
     Result := GetMaxFolio('SE_FOLIO','SESION');
end;

function TdmServerCatalogos.GrabaSesiones(Empresa, oDelta, oKCDelta: OleVariant; out iFolio: Integer; iOperacion: Integer; var ErrorCount, ErrorKCCount: Integer;
  out Valor: OleVariant): OleVariant;
begin
     ErrorCount := 0;
     ErrorKCCount := 0;
     FValidaGrabado:= ( eOperacionConflicto( iOperacion ) = ocReportar ); //Indica si se se quiere validar que este dentro de sus cursos programados y si comentarios viene vac�o
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          if ( oDelta = null ) then
             Result := oDelta
          else
          begin
               FValidaCampos:= CreateQuery( SP_VALIDA_CAMPOS );
               try
                  SetTablaInfo( eSesiones );
                  with TablaInfo do
                  begin
                       BeforeUpdateRecord := BeforeUpdateSesion;
                       AfterUpdateRecord := AfterUpdateSesion;
                  end;
                  Result := GrabaTabla( Empresa, oDelta, ErrorCount );
                  iFolio := FMaxFolio;
               finally
                      FreeAndNil(FValidaCampos);
               end;
          end;
          if ( ErrorCount = 0 ) then
          begin
               if ( oKCDelta = null ) then
                  Valor := oKCDelta
               else
               begin
                    QueriesStatusActBegin;
                    FValidaCurso:= CreateQuery( SP_VALIDA_CURSO );
                    try
                       SetTablaInfo( eKarCurso );
                       FMaxFolio:= iFolio;
                       TablaInfo.BeforeUpdateRecord := BeforeUpdateHisCursos;
                       Valor := GrabaTablaGrid( Empresa, oKCDelta, ErrorKCCount );
                    finally
                           FreeAndNil(FValidaCurso);
                           QueriesStatusActEnd;
                    end;
               end;
          end;
     end;
     SetComplete;
end;

function TdmServerCatalogos.CheckEmpleadoBaja( const iEmpleado: Integer; const dFecha: TDate ): eStatusEmpleado;
begin
     Result := oZetaCreator.Queries.GetStatusActEmpleado( iEmpleado, dFecha );
end;

function TdmServerCatalogos.GetHisCursos(Empresa: OleVariant; iSesion: Integer): OleVariant;
const
     Q_GET_HIS_CURSO = 'select KARCURSO.CB_CODIGO, KC_EVALUA, KC_FEC_TOM, CU_CODIGO, SE_FOLIO, KC_HORAS, '+
                       'KC_FEC_FIN, KC_REVISIO, MA_CODIGO,' + K_PRETTYNAME + ' AS PrettyName, '+
                       'KARCURSO.CB_NIVEL1, KARCURSO.CB_NIVEL2, KARCURSO.CB_NIVEL3, KARCURSO.CB_NIVEL4, '+
                       'KARCURSO.CB_NIVEL5, KARCURSO.CB_NIVEL6, KARCURSO.CB_NIVEL7, KARCURSO.CB_NIVEL8, KARCURSO.CB_NIVEL9, '+
                       {$ifdef ACS}'KARCURSO.CB_NIVEL10, KARCURSO.CB_NIVEL11, KARCURSO.CB_NIVEL12, '+{$endif}
                       'KARCURSO.CB_PUESTO, KARCURSO.CB_CLASIFI, KARCURSO.CB_TURNO,KARCURSO.KC_EST '+
                       'from KARCURSO '+
                       'left outer join COLABORA on ( KARCURSO.CB_CODIGO = COLABORA.CB_CODIGO ) '+
                       'where SE_FOLIO = %d '+
                       'order by KARCURSO.CB_CODIGO';
begin
     with oZetaProvider do
     begin
          Result := OpenSQL( Empresa, Format(Q_GET_HIS_CURSO, [iSesion] ), ( iSesion<>0 ) );
     end;
end;

procedure TdmServerCatalogos.BeforeUpdateSesion( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
var
   sProblema: String;

   function ValidaCampos( sMaestro, sLugar, sComentario, sRevision, sHoraIni, sHoraFin : String;  dFechaIni, dFechaFin: TDate;
                          iCupo, iStatus: Integer; rCosto1, rCosto2, rCosto3: TPesos ): Boolean;
   begin
        with oZetaProvider do
        begin
             ParamAsChar( FValidaCampos, 'SE_COMENTA', sComentario, K_ANCHO_OBSERVACIONES );

             ParamAsChar( FValidaCampos, 'MA_CODIGO', sMaestro, K_ANCHO_CODIGO );
             ParamAsChar( FValidaCampos, 'SE_LUGAR', sLugar, K_ANCHO_OBSERVACIONES );
             ParamAsDate( FValidaCampos, 'SE_FEC_INI', dFechaIni );
             ParamAsChar( FValidaCampos, 'SE_HOR_INI', sHoraIni, K_ANCHO_HORA );
             ParamAsChar( FValidaCampos, 'SE_HOR_FIN', sHoraFin, K_ANCHO_HORA );
             ParamAsDate( FValidaCampos, 'SE_FEC_FIN', dFechaFin );
             ParamAsInteger(FValidaCampos, 'SE_CUPO', iCupo );
             ParamAsInteger(FValidaCampos, 'SE_STATUS', iStatus );
             ParamAsFloat(FValidaCampos, 'SE_COSTO1', rCosto1  );
             ParamAsFloat(FValidaCampos, 'SE_COSTO2', rCosto2  );
             ParamAsFloat(FValidaCampos, 'SE_COSTO3', rCosto3  );
             ParamAsChar( FValidaCampos, 'SE_REVISIO', sRevision, K_ANCHO_OPERACION );

             {$IFDEF MSSQL}
             ParamSalida( FValidaCampos, 'MensajeError');
             ParamSalida( FValidaCampos, 'Status' );
             Ejecuta( FValidaCampos );
             {$ENDIF}


             {$IFDEF INTERBASE}
             with FValidaCampos do
             begin
                  Active:= True;
                  sProblema:= FieldByName('MensajeError').AsString;
                  Result:= IntToBool(FieldByName('Status').AsInteger);
                  Active:= False;
             end;
             {$ENDIF}

             {$IFDEF MSSQL}
             sProblema:= GetParametro( FValidaCampos, 'MensajeError').AsString;
             Result := IntToBool( GetParametro( FValidaCampos, 'Status').AsInteger );
             {$ENDIF}
        end;
   end;

begin
     if ( FValidaGrabado ) and ( UpdateKind in [ ukInsert, ukModify ] ) then
     begin
          with DeltaDS do
          begin
               if ( ValidaCampos( CampoAsVar(FieldByName('MA_CODIGO') ), CampoAsVar( FieldByName('SE_LUGAR') ), CampoAsVar(FieldByName('SE_COMENTA') ),
                                  CampoAsVar(FieldByName('SE_REVISIO') ), CampoAsVar(FieldByName('SE_HOR_INI') ), CampoAsVar(FieldByName('SE_HOR_FIN') ),
                                  CampoAsVar(FieldByName('SE_FEC_INI') ), CampoAsVar(FieldByName('SE_FEC_FIN') ), CampoAsVar(FieldByName('SE_CUPO') ),
                                  CampoAsVar(FieldByName('SE_STATUS')), CampoAsVar(FieldByName('SE_COSTO1') ),
                                  CampoAsVar(FieldByName('SE_COSTO2') ), CampoAsVar(FieldByName('SE_COSTO3') )
                                  ) ) then
               begin
                    DataBaseError( sProblema );
               end;
          end;
     end;
     if( UpdateKind = ukDelete ) then
         FMaxFolio := 0;
     if( UpdateKind = ukInsert ) then
     begin
          FMaxFolio := GetMaxFolioSesion;
          DeltaDS.Edit;
          DeltaDS.FieldByName( 'SE_FOLIO' ).AsInteger := FMaxFolio;
          DeltaDS.Post;
     end;
end;

procedure TdmServerCatalogos.AfterUpdateSesion( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
const
     aTipoRegistro: array[ TUpdateKind ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'Modific�','Agreg�','Borr�');
var
   iGrupo: Integer;
   sCurso,sTitulo,sTextoBitacora: String;
   dFecha: TDate;
begin
     with DeltaDS do
     begin
          if( UpdateKind in [ ukModify, ukDelete ]) then
          begin
               iGrupo:= CampoOldAsVar(FieldByName('SE_FOLIO'));
               sCurso:= CampoOldAsVar(FieldByName( 'CU_CODIGO' ));
               dFecha:= CampoOldAsVar(FieldByName('SE_FEC_INI'));
               sTextoBitacora:= oZetaProvider.GetDatasetInfo( DeltaDS, ( UpdateKind = ukDelete ) ) ;
          end
          else
          begin
              iGrupo:= CampoAsVar(FieldByName('SE_FOLIO'));
              sCurso:= CampoAsVar(FieldByName( 'CU_CODIGO' ));
              dFecha:= CampoAsVar(FieldByName('SE_FEC_INI'));
          end;

         sTitulo := Format( '%s Grupo: %d Curso: %s', [ aTipoRegistro[ UpdateKind ], iGrupo, sCurso] );
         oZetaProvider.EscribeBitacora( tbNormal, clbSesiones,0,dFecha, sTitulo, sTextoBitacora);
     end;
end;

function TdmServerCatalogos.EsCursoInValido( oDelta: TzProviderClientDataSet ; var sMensaje: String ): Integer;
begin
     with oZetaProvider do
     begin
          with oDelta do
          begin
               ParamAsInteger( FValidaCurso, 'CB_CODIGO', CampoAsVar( FieldByName('CB_CODIGO') ) );
               ParamAsChar( FValidaCurso, 'CU_CODIGO', CampoAsVar( FieldByName('CU_CODIGO') ), K_ANCHO_CODIGO );
               ParamAsChar( FValidaCurso, 'CB_PUESTO', CampoAsVar( FieldByName('CB_PUESTO') ), K_ANCHO_CODIGO );
               ParamAsChar( FValidaCurso, 'CB_NIVEL5', CampoAsVar( FieldByName('CB_NIVEL5') ), K_ANCHO_CODIGO_NIVELES);

               ParamAsChar( FValidaCurso, 'CB_CLASIFI', CampoAsVar( FieldByName('CB_CLASIFI') ), K_ANCHO_CODIGO );
               ParamAsChar( FValidaCurso, 'CB_TURNO', CampoAsVar( FieldByName('CB_TURNO') ), K_ANCHO_CODIGO );
               ParamAsFloat( FValidaCurso, 'KC_EVALUA',CampoAsVar( FieldByName('KC_EVALUA') ) );
               ParamAsDate( FValidaCurso, 'KC_FEC_TOM', CampoAsVar( FieldByName('KC_FEC_TOM') ) );
               ParamAsFloat( FValidaCurso, 'KC_HORAS', CampoAsVar( FieldByName('KC_HORAS') ));

               ParamAsChar( FValidaCurso, 'CB_NIVEL1', CampoAsVar( FieldByName('CB_NIVEL1') ), K_ANCHO_CODIGO_NIVELES);
               ParamAsChar( FValidaCurso, 'CB_NIVEL2', CampoAsVar( FieldByName('CB_NIVEL2') ), K_ANCHO_CODIGO_NIVELES );
               ParamAsChar( FValidaCurso, 'CB_NIVEL3', CampoAsVar( FieldByName('CB_NIVEL3') ), K_ANCHO_CODIGO_NIVELES );
               ParamAsChar( FValidaCurso, 'CB_NIVEL4', CampoAsVar( FieldByName('CB_NIVEL4') ), K_ANCHO_CODIGO_NIVELES);

               ParamAsChar( FValidaCurso, 'CB_NIVEL6', CampoAsVar( FieldByName('CB_NIVEL6') ), K_ANCHO_CODIGO_NIVELES );
               ParamAsChar( FValidaCurso, 'CB_NIVEL7', CampoAsVar( FieldByName('CB_NIVEL7') ), K_ANCHO_CODIGO_NIVELES );
               ParamAsChar( FValidaCurso, 'CB_NIVEL8', CampoAsVar( FieldByName('CB_NIVEL8') ), K_ANCHO_CODIGO_NIVELES);
               ParamAsChar( FValidaCurso, 'CB_NIVEL9', CampoAsVar( FieldByName('CB_NIVEL9') ), K_ANCHO_CODIGO_NIVELES );

               {$ifdef ACS}
               ParamAsChar( FValidaCurso, 'CB_NIVEL10', CampoAsVar( FieldByName('CB_NIVEL10') ), K_ANCHO_CODIGO_NIVELES );
               ParamAsChar( FValidaCurso, 'CB_NIVEL11', CampoAsVar( FieldByName('CB_NIVEL11') ), K_ANCHO_CODIGO_NIVELES );
               ParamAsChar( FValidaCurso, 'CB_NIVEL12', CampoAsVar( FieldByName('CB_NIVEL12') ), K_ANCHO_CODIGO_NIVELES );
               {$endif}

               ParamAsChar( FValidaCurso, 'MA_CODIGO', CampoAsVar( FieldByName('MA_CODIGO') ), K_ANCHO_CODIGO );
               ParamAsDate( FValidaCurso, 'KC_FEC_FIN', CampoAsVar( FieldByName('KC_FEC_FIN') ) );
               ParamAsChar( FValidaCurso, 'KC_REVISIO', CampoAsVar( FieldByName('KC_REVISIO') ), K_ANCHO_FECHA );
               ParamAsChar( FValidaCurso, 'SE_FOLIO', CampoAsVar( FieldByName('SE_FOLIO') ), K_ANCHO_CODIGO );
          end;
          {$IFDEF MSSQL}
          ParamSalida( FValidaCurso, 'MensajeError');
          ParamSalida( FValidaCurso, 'Status' );
          {$ENDIF}

          {$IFDEF MSSQL}
          Ejecuta( FValidaCurso );
          sMensaje:= GetParametro( FValidaCurso, 'MensajeError').AsString;
          Result := GetParametro( FValidaCurso, 'Status').AsInteger;
          {$ENDIF}

          {$IFDEF INTERBASE}
          with FValidaCurso do
          begin
               Active := True;
               sMensaje:= FieldByName('MensajeError').AsString;
               Result:= FieldByName('Status').AsInteger;
               Active := False;
          end;
          {$ENDIF}
      end;
end;

procedure TdmServerCatalogos.BeforeUpdateHisCursos(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
var
   sProblema: String;

   function EsFechaValidaCurso( const iEmpleado: Integer; const dFecha: TDate; var sProblema : String ) : Boolean;
   begin
        Result := ( CheckEmpleadoBaja( iEmpleado, dFecha ) = steEmpleado );
        if not Result then
           sProblema:= 'Empleado # ' + IntToStr( iEmpleado ) + ' - No est� Activo en La Fecha del Curso ';
   end;

begin
     if UpdateKind in [ ukModify, ukInsert ] then
     begin
          with DeltaDS do
          begin
               if ( FValidaGrabado ) then
               begin
                    if ( ( CampoAsVar( FieldByName( 'OPERACION' ) ) = Ord( ocReportar ) )  ) and
                       ( IntToBool( EsCursoInvalido( DeltaDS , sProblema )  ) ) then
                    begin
                         DataBaseError( sProblema );
                    end;
               end;
               if ( not EsFechaValidaCurso( CampoAsVar( FieldByName( 'CB_CODIGO' ) ),
                                        CampoAsVar( FieldByName( 'KC_FEC_TOM' ) ),
                                        sProblema ) )  then
               begin
                    DataBaseError( sProblema );

               end
               else if( UpdateKind = ukInsert ) then
               begin
                    if( CampoAsVar( FieldByName('SE_FOLIO') ) = 0 )then
                    begin
                         Edit;
                         FieldByName('SE_FOLIO').AsInteger := FMaxFolio;
                         Post;
                    end;
               end;
          end;
     end;
end;

procedure TdmServerCatalogos.BeforeUpdateInvitadores( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
var
   sFacilityCode: string;
begin
     if( UpdateKind in [ ukInsert, ukModify ] )  then
     begin
          with DeltaDS, oZetaProvider do
          begin
               {Defecto 1431: Todo viene convertido a decimal desde el Get lo convirti�. Sino cambio no es necesario volverlo a convertir no lo va a grabar}
               if ( CambiaCampo( FieldByName( 'IV_ID_NUM' ) ) or ( UpdateKind = ukInsert ) ) then
               begin
                    if not ( State in [ dsEdit, dsInsert ] ) then
                       Edit;
                    sFacilityCode := GetGlobalString( K_GLOBAL_PROXIMITY_FACILITY_CODE );
                    if ( FieldByName( 'IV_MIFARE' ).AsString = K_GLOBAL_SI ) then
                       sFacilityCode := VACIO;
                    FieldByName( 'IV_ID_NUM' ).AsString := ZetaServerTools.ProximidadDecToHex( FieldByName( 'IV_ID_NUM' ).AsString,
                                                                                               sFacilityCode,
                                                                                               GetGlobalInteger( K_GLOBAL_PROXIMITY_TOTAL_BYTES ) );
               end;
          end;
     end;
end;

function TdmServerCatalogos.GetAccReglas(Empresa: OleVariant): OleVariant;
begin
     Result := GetCatalogo( eAccReglas, Empresa );
     SetComplete;
end;

function TdmServerCatalogos.GetAulas(Empresa: OleVariant): OleVariant;
begin
     Result := GetCatalogo( eAulas, Empresa );
     SetComplete;
end;

function TdmServerCatalogos.GetPrerequisitosCurso(Empresa: OleVariant; const sPrerequisitosCurso: WideString): OleVariant;
begin
     SetTablaInfo( ePrerequisitosCurso );
     with oZetaProvider do
     begin
          TablaInfo.SetOrder( 'CP_CURSO' );
          TablaInfo.Filtro := Format( '( CU_CODIGO = %s )', [ EntreComillas( sPrerequisitosCurso ) ] );
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerCatalogos.GetCertificaciones(Empresa: OleVariant): OleVariant;
begin
     Result := GetCatalogo( eCertificaciones, Empresa );
     SetComplete;
end;

function TdmServerCatalogos.GetTiposPoliza(Empresa: OleVariant): OleVariant;
begin
     Result := GetCatalogo( eTiposPoliza, Empresa );
     SetComplete;
end;

function TdmServerCatalogos.GetTPeriodos(Empresa: OleVariant): OleVariant;
begin
{$ifdef QUINCENALES}
     Result := GetCatalogo( eTPeriodos, Empresa );
{$endif}
end;

function TdmServerCatalogos.GetSeccionesPerfil( Empresa: OleVariant ): OleVariant;
begin
     SetTablaInfo( eSeccionesPerfil );
     with oZetaProvider do
     begin
          TablaInfo.SetOrder( 'DT_ORDEN' );
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

procedure TdmServerCatalogos.BeforeUpdateDESCTIPO( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
const
     {$ifdef INTERBASE}
          Q_SP_BORRA_DESCTIPO = 'execute procedure SP_BORRA_DESCTIPO( %d )';
     {$endif}
     {$ifdef MSSQL}
          Q_SP_BORRA_DESCTIPO = 'exec dbo.SP_BORRA_DESCTIPO %d ';
     {$endif}
begin
     FMaxOrden := 0;
     if( UpdateKind = ukInsert ) then
     begin
          FMaxOrden := GetMaxFolio('DT_ORDEN','DESCTIPO');
          DeltaDS.Edit;
          DeltaDS.FieldByName( 'DT_ORDEN' ).AsInteger := FMaxOrden;
          DeltaDS.Post;
     End
     else if (  UpdateKind = ukDelete ) then
     begin
          oZetaProvider.EjecutaAndFree(  Format( Q_SP_BORRA_DESCTIPO, [ DeltaDs.FieldByName ( 'DT_ORDEN' ).AsInteger ]  ) );
          Applied := True;
     End;
     FTipoCatalogo := eSeccionesPerfil;
     GrabaCambiosBitacora( Sender, SourceDS, DeltaDS, UpdateKind );
end;

function TdmServerCatalogos.GrabaSeccion(Empresa, oDelta: OleVariant; var ErrorCount: Integer; out iFolio: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          SetTablaInfo( eSeccionesPerfil );
          TablaInfo.BeforeUpdateRecord := BeforeUpdateDESCTIPO;
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
          iFolio := FMaxOrden;
     end;
end;

function TdmServerCatalogos.GetCamposPerfil(Empresa: OleVariant; const sSeccion: WideString): OleVariant;
const
     K_FILTRO = 'DT_CODIGO = %s';
begin
     SetTablaInfo( eCamposPerfil );
     with oZetaProvider do
     begin
          TablaInfo.SetOrder( 'DF_ORDEN' );
          TablaInfo.Filtro := Format( K_FILTRO, [ EntreComillas( sSeccion ) ] );
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

procedure TdmServerCatalogos.BeforeUpdateDESC_FLD( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
var
   sSeccion:String;
   FDataSet : TZetaCursor;
const
     Q_MAX_DF_ORDEN = 'select max(DF_ORDEN) MAXIMO from DESC_FLD where DT_CODIGO = %s';

     {$ifdef INTERBASE}
          Q_SP_BORRA_DESC_FLD = 'execute procedure SP_BORRA_DESC_FLD ( %s , %d )';
     {$endif}
     {$ifdef MSSQL}
          Q_SP_BORRA_DESC_FLD = 'exec dbo.SP_BORRA_DESC_FLD  %s , %d';
     {$endif}
begin
     FMaxOrden := 0;
     if( UpdateKind = ukInsert ) then
     begin
          sSeccion := CampoAsVar( DeltaDS.FieldByName( 'DT_CODIGO' ) );
          FDataset := oZetaProvider.CreateQuery;
          try
             if oZetaProvider.AbreQueryScript( FDataset, Format( Q_MAX_DF_ORDEN ,[ EntreComillas(sSeccion) ] ) ) then
                FMaxOrden := FDataSet.FieldByName( 'MAXIMO' ).AsInteger + 1   // Si est� vacio, regresa 0
             else
                 FMaxOrden := 1;
          finally
                 FreeAndNil( FDataSet );
          end;
          with DeltaDS do
          begin
               Edit;
               FieldByName( 'DF_ORDEN' ).AsInteger := FMaxOrden;
               Post;
          end;
     end
     else if ( UpdateKind = ukDelete ) then
     begin
          oZetaProvider.EjecutaAndFree(  Format( Q_SP_BORRA_DESC_FLD, [ EntreComillas( DeltaDS.FieldByName( 'DT_CODIGO' ).AsString ),DeltaDS.FieldByName( 'DF_ORDEN' ).AsInteger ] ) );
          Applied := True;
     end;
     FTipoCatalogo := eCamposPerfil;
     GrabaCambiosBitacora( Sender, SourceDS, DeltaDS, UpdateKind );
end;

function TdmServerCatalogos.GrabaCamposPerfil(Empresa, oDelta: OleVariant; var ErrorCount: Integer; out iFolio: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          SetTablaInfo( eCamposPerfil );
          TablaInfo.BeforeUpdateRecord := BeforeUpdateDESC_FLD;
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
          iFolio := FMaxOrden;
     end;
     SetComplete;
end;

function TdmServerCatalogos.CambiaOrden(Empresa: OleVariant; iCatalogo, iActual, iNuevo: Integer;  const sCodigo, sClasifi: WideString): OleVariant;
const
     {$ifdef INTERBASE}
          Q_SP_CAMBIA_DESCTIPO = 'execute procedure SP_CAMBIA_DESCTIPO( %d , %d )';
          Q_SP_CAMBIA_DESC_FLD = 'execute procedure SP_CAMBIA_DESC_FLD( %s, %d , %d )';
          Q_SP_CAMBIA_DESC_PTO = 'execute procedure SP_CAMBIA_DESC_PTO( %s, %s, %d , %d )';
          Q_SP_CAMBIA_REGLAPRESTA = 'execute procedure SP_CAMBIA_REGLAPRESTA( %d, %d , %d )';
     {$endif}
     {$ifdef MSSQL}
          Q_SP_CAMBIA_DESCTIPO = 'exec dbo.SP_CAMBIA_DESCTIPO %d,%d ';
          Q_SP_CAMBIA_DESC_FLD = 'exec dbo.SP_CAMBIA_DESC_FLD %s,%d,%d ';
          Q_SP_CAMBIA_DESC_PTO = 'exec dbo.SP_CAMBIA_DESC_PTO %s, %s, %d, %d ';
          Q_SP_CAMBIA_REGLAPRESTA = 'exec dbo.SP_CAMBIA_REGLAPRESTA %d, %d , %d ';

          Q_VAL_CAMBIA_FACTOR = 'execute dbo.VAL_CAMBIA_FACTOR %d, %d, %d ';
          Q_VAL_CAMBIA_SUBFACTOR = 'execute dbo.VAL_CAMBIA_SUBFACTOR %d, %d, %d ';
          Q_VAL_CAMBIA_NIVEL = 'execute dbo.VAL_CAMBIA_NIVEL %d, %d, %d, %d ';
     {$endif}
var
   FCambio: TZetaCursor;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          case eTipoCatalogo( iCatalogo ) of
               {$IFNDEF INTERBASE}
               eValNiveles: FCambio := CreateQuery( Format ( Q_VAL_CAMBIA_NIVEL, [ StrToInt(sCodigo), StrToInt(sClasifi), iActual, iNuevo ] ) );
               eValSubfact: FCambio := CreateQuery( Format ( Q_VAL_CAMBIA_SUBFACTOR, [ StrToInt(sCodigo), iActual, iNuevo ] ) );
               eValfact: FCambio:= CreateQuery( Format ( Q_VAL_CAMBIA_FACTOR, [ StrToInt(sCodigo), iActual, iNuevo ] ) );
               {$ENDIF}
               eSeccionesPerfil: FCambio := CreateQuery( Format ( Q_SP_CAMBIA_DESCTIPO , [ iActual ,iNuevo  ]  ) );
               eCamposPerfil: FCambio := CreateQuery( Format ( Q_SP_CAMBIA_DESC_FLD , [ EntreComillas(sCodigo)  , iActual , iNuevo  ]  ) );
               eDescPerfil: FCambio := CreateQuery( Format ( Q_SP_CAMBIA_DESC_PTO , [ EntreComillas(sClasifi), EntreComillas(sCodigo), iActual , iNuevo  ]  ) );
               eReglasPrestamos : FCambio := CreateQuery( Format ( Q_SP_CAMBIA_REGLAPRESTA , [ StrToInt(sCodigo)  , iActual , iNuevo  ]  ) );
          end;

          try
             EmpiezaTransaccion;
             try
                Ejecuta( FCambio );
                TerminaTransaccion( True );
             except
                   on Error: Exception do
                   begin
                        RollBackTransaccion;
                        raise;
                   end;
             end;
          finally
                 FreeAndNil( FCambio );
          end;
     end;
     SetComplete;
end;

function TdmServerCatalogos.GetPerfilesPuesto( Empresa: OleVariant): OleVariant;
const
     Q_GET_PERFILES_POR_PUESTO = ' Select  PERFIL.PU_CODIGO, PUESTO.PU_DESCRIP, PF_CONTRL1, PF_CONTRL2 '+
                                 ' From PERFIL '+
                                 ' Left outer join PUESTO on PERFIL.PU_CODIGO = PUESTO.PU_CODIGO'+
                                 ' Order by PERFIL.PU_CODIGO ';
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Result := OpenSQL( Empresa,Q_GET_PERFILES_POR_PUESTO, True );
     end;
end;

function TdmServerCatalogos.GetPerfiles( Empresa: OleVariant;const sPuesto: WideString): OleVariant;
begin
     with oZetaProvider do
     begin
          SetTablaInfo( ePerfiles );
          TablaInfo.Filtro := Format( 'PU_CODIGO = %s', [ EntreComillas(sPuesto) ] );
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerCatalogos.CopiarPerfil(Empresa: OleVariant; const sFuente, sDestino: WideString): OleVariant;
const
     {$ifdef INTERBASE}
          Q_SP_COPIA_PERFIL = 'execute procedure SP_COPIA_PERFIL ( %s , %s )';
     {$endif}
     {$ifdef MSSQL}
          Q_SP_COPIA_PERFIL = 'exec dbo.SP_COPIA_PERFIL  %s, %s ' ;
     {$endif}
var
   FCopiar: TZetaCursor;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FCopiar := CreateQuery( Format( Q_SP_COPIA_PERFIL, [ EntreComillas( sFuente ),EntreComillas( sDestino )  ] ) );
          try
             EmpiezaTransaccion;
             try
                Ejecuta( FCopiar );
                TerminaTransaccion( True );
             except
                on Error: Exception do
                begin
                     RollBackTransaccion;
                     raise;
                end;
             end;
          finally
                 FreeAndNil( FCopiar );
          end;
          Result := self.GetPerfiles( Empresa, sDestino );
     end;
     SetComplete;
end;

function TdmServerCatalogos.GetDescPerfil(Empresa: OleVariant; const sPuesto: WideString): OleVariant;
const
     Q_GET_DESC_PTO_POR_PUESTO = ' Select * From DESC_PTO ' +
                                 ' Where PU_CODIGO = %s ' +
                                 ' Order by DP_ORDEN ';
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Result := OpenSQL( Empresa, Format ( Q_GET_DESC_PTO_POR_PUESTO, [EntreComillas( sPuesto ) ] ) , True );
    end;
end;

function TdmServerCatalogos.GrabaDescPerfil(Empresa, oDelta: OleVariant;const sCampos: WideString; out ErrorCount, iOrden: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          TablaInfo.SetInfo ('DESC_PTO' ,sCampos ,'PU_CODIGO,DT_CODIGO,DP_ORDEN' );
          TablaInfo.BeforeUpdateRecord := BeforeUpdateDESC_PTO;
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
          iOrden := FMaxOrden;
     end;
     SetComplete;
end;

procedure TdmServerCatalogos.BeforeUpdateDESC_PTO(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
                                                  UpdateKind: TUpdateKind; var Applied: Boolean);
const K_QRY = 'select  max (DP_ORDEN) MAXIMO from DESC_PTO where DT_CODIGO = %s and PU_CODIGO = %s ';

      {$ifdef INTERBASE}
          Q_SP_BORRA_DESC_PTO = 'execute procedure SP_BORRA_DESC_PTO ( %s, %s, %d )';
      {$endif}
      {$ifdef MSSQL}
          Q_SP_BORRA_DESC_PTO = 'exec dbo.SP_BORRA_DESC_PTO %s, %s ,%d';
      {$endif}

var sSeccion,sPerfil:String;
    FDataSet : TZetaCursor;
begin
     FMaxOrden := 0;
     if( UpdateKind = ukInsert ) then
     begin
          sSeccion := CampoAsVar( DeltaDS.FieldByName( 'DT_CODIGO' ) );
          sPerfil  := CampoAsVar( DeltaDS.FieldByName( 'PU_CODIGO' ) );
          FDataset := oZetaProvider.CreateQuery;
          try
             if oZetaProvider.AbreQueryScript( FDataset, Format( K_QRY,[ EntreComillas( sSeccion ),EntreComillas( sPerfil ) ] ) ) then
                 FMaxOrden := FDataSet.FieldByName( 'MAXIMO' ).AsInteger + 1   // Si est� vacio, regresa 0
             else
                 FMaxOrden := 1;
          finally
                 FreeAndNil( FDataSet );
          end;

          with DeltaDs do
          begin
               Edit;
               FieldByName( 'DP_ORDEN' ).AsInteger := FMaxOrden;
               Post;
          end;
     end
     else if( UpdateKind = ukDelete  ) then
     begin
          oZetaProvider.EjecutaAndFree(  Format( Q_SP_BORRA_DESC_PTO , [ EntreComillas( DeltaDS.FieldByName( 'PU_CODIGO' ).AsString ),
                                                                         EntreComillas( DeltaDS.FieldByName( 'DT_CODIGO' ).AsString ),
                                                                         DeltaDS.FieldByName( 'DP_ORDEN' ).AsInteger ] ) );
          Applied := True;
     end;
end;

procedure TdmServerCatalogos.BeforeUpdateCursos(Sender: TObject;SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;UpdateKind: TUpdateKind; var Applied: Boolean);
const
     Q_FOLIO_CURSO = 'select CU_CODIGO,CU_NOMBRE from CURSO where ( CU_FOLIO = %s )%s';
     Q_CODIGO = ' and ( CU_CODIGO <> %s )';
var
   sFolio : string;
   sCodigo: string;
   sMensaje: WideString;
   oCambio:TZetaCursor;

    function ExisteFolioCurso : Boolean;
    var
       sCodigo : string;
    begin
         with oZetaProvider do
         begin
              sCodigo := VACIO;
              if ( UpdateKind = ukModify )then
              begin
                   sCodigo := Format ( Q_CODIGO,[ EntreComillas( sCodigo ) ] );
              end;
              oCambio := CreateQuery( Format( Q_FOLIO_CURSO,[ EntreComillas( sFolio),sCodigo ] ) );
              try
                 with oCambio do
                 begin
                      Active := TRUE;
                      Result := ( not IsEmpty );
                      if Result then
                      begin
                           sMensaje := Format( 'No se puede asignar el folio: %s ', [ sFolio] ) + CR_LF +
                                       Format( 'Esta asignado al curso: %s - %s ', [ FieldByName('CU_CODIGO').AsString,FieldByName('CU_NOMBRE').AsString  ] );
                      end
                      else
                          sMensaje := VACIO;
                 end;
              finally
                     FreeAndNil( oCambio );
              end;
         end;
    end;

begin
     if ( UpdateKind in [ ukModify, ukInsert ] ) then
     begin
          with DeltaDS do
          begin
               sFolio := CampoAsVar( FieldByName('CU_FOLIO') );
               sCodigo := CampoOldAsVar( FieldByName('CU_CODIGO') );
               if strLleno( sFolio ) and
               ( ( UpdateKind = ukInsert ) or ( CambiaCampo( FieldByName( 'CU_FOLIO' ) ) ) ) then
               begin
                    if ExisteFolioCurso then
                       db.DatabaseError( sMensaje );
               end;
          end;
     end;
end;

//US 17266: Almacenar la clave SAT cuando se timbra la n�mina para obtener reportes y auditar timbrado
procedure TdmServerCatalogos.AfterUpdateConceptos(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind );
const
     Q_SP_KAR_CONCEP_UPDATE = 'exec dbo.KardexConceptos_NotifyUpdate  %d, %d, %s' ;
var
   FKarConceptoUpdate: TZetaCursor;
   iConcepto: Integer;

   function CambiaCampoMultiples: Boolean;
   var
      oLista: TStrings;
      i: integer;
   begin
        Result := False;
        try
           oLista := TStringList.Create;
           oLista.CommaText := 'CO_A_PTU,CO_ACTIVO,CO_CALCULA,CO_DESCRIP,CO_FORMULA,CO_G_IMSS,CO_G_ISPT,CO_IMP_CAL,CO_IMPRIME,CO_LISTADO,CO_MENSUAL,' +
                               'CO_QUERY,CO_RECIBO,CO_TIPO,CO_X_ISPT,CO_SUB_CTA,CO_CAMBIA,CO_D_EXT,CO_D_NOM,CO_LIM_SUP,CO_LIM_INF,CO_VER_INF,CO_VER_SUP,' +
                               'CO_VER_ACC,CO_GPO_ACC,CO_ISN,CO_SUMRECI,CO_FRM_ALT,CO_USO_NOM,CO_SAT_CLP,CO_SAT_TPP,CO_SAT_CLN,CO_SAT_TPN,CO_SAT_EXE,CO_SAT_CON,CO_PS_TIPO';
           if (  UpdateKind = ukInsert ) then
           begin
                Result := True;
           end
           else
           begin
                for i:= 0 to oLista.Count - 1 do
                begin
                     try
                        with DeltaDS do
                        begin
                             if CambiaCampo( FieldByName( oLista[i] ) ) then
                             begin
                                  Result := True;
                                  Break;
                             end;
                        end;
                     except
                           Result := FALSE;
                           Break;
                     end;
                end;
           end;
        finally
               FreeAndNil( oLista );
        end;
   end;
begin
     iConcepto := 0;
     if ( ( UpdateKind = ukModify ) or ( UpdateKind = ukInsert ) ) then
     begin
          with DeltaDS do
          begin
               if( CambiaCampoMultiples ) then
               begin
                    with oZetaProvider do
                    begin
                         iConcepto := CampoOldAsVar( DeltaDS.FieldByName( 'CO_NUMERO' ) );
                         FKarConceptoUpdate := CreateQuery( Format( Q_SP_KAR_CONCEP_UPDATE, [ iConcepto, UsuarioActivo, ZetaCommonTools.DateToStrSQLC( Now ) ] ) );
                         try
                            EmpiezaTransaccion;
                            try
                               Ejecuta( FKarConceptoUpdate );
                               TerminaTransaccion( True );
                            except
                               on Error: Exception do
                               begin
                                    RollBackTransaccion;
                                    raise;
                               end;
                            end;
                         finally
                                FreeAndNil( FKarConceptoUpdate );
                         end;
                    end;
                    SetComplete;
               end;
          end;
     end;
     GrabaCambiosBitacora( Sender, SourceDS, DeltaDS, UpdateKind );
end;

function TdmServerCatalogos.GetValPlantilla( Empresa: OleVariant): OleVariant;
begin
     Result := GetCatalogo( eVistaValPlant, Empresa );
     SetComplete;
end;

function TdmServerCatalogos.GetValFactores(Empresa: OleVariant; iPlantilla: Integer): OleVariant;
begin
     SetTablaInfo( eVistaValFact );
     with oZetaProvider do
     begin
          TablaInfo.SetOrder( 'VF_ORDEN' );
          TablaInfo.Filtro := Format( '( VL_CODIGO = %d )', [ iPlantilla ] );
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerCatalogos.GetValNiveles(Empresa: OleVariant; iPlantilla,iOrden: Integer): OleVariant;
var
   sFiltro : String;
begin
     SetTablaInfo( eValNiveles );
     with oZetaProvider do
     begin
          TablaInfo.SetOrder( 'VN_ORDEN' );
          sFiltro:= Format('VL_CODIGO = %d ',[ iPlantilla ]);
          if ( iOrden <> 0 ) then
             sFiltro:= sFiltro + Format('AND VS_ORDEN = %d',[ iOrden ]);

          TablaInfo.Filtro:= sFiltro;
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerCatalogos.GetValSubFactores(Empresa: OleVariant; iPlantilla: Integer): OleVariant;
begin
     SetTablaInfo( eVistaValSubfact );
     with oZetaProvider do
     begin
          TablaInfo.SetOrder( 'VS_ORDEN' );
          TablaInfo.Filtro := Format( '( VL_CODIGO = %d )', [ iPlantilla  ] );
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerCatalogos.GrabaCamposVal(Empresa: OleVariant;iCatalogo: Integer; oDelta: OleVariant; var ErrorCount: Integer; out iFolio: Integer): OleVariant;
begin
     FTipoCatalogo := eTipoCatalogo( iCatalogo );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          SetTablaInfo( FTipoCatalogo );
          TablaInfo.BeforeUpdateRecord := BeforeUpdateCamposVal;
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
          iFolio := FMaxOrden;
     end;
     SetComplete;
end;

procedure TdmServerCatalogos.BeforeUpdateCamposVal( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
var sCampo, sSQL, sProcedure:String;
    FDataSet : TZetaCursor;
    iPlantilla, iOrden: Integer;
const
     Q_MAX_VF_ORDEN = 'select max(VF_ORDEN) MAXIMO from VFACTOR where VL_CODIGO = %d';
     Q_MAX_VS_ORDEN = 'select max(VS_ORDEN) MAXIMO from VSUBFACT where VL_CODIGO = %d';
     Q_MAX_VN_ORDEN = 'select max(VN_ORDEN) MAXIMO from VALNIVEL where VL_CODIGO = %d and VS_ORDEN = %d';

     Q_VAL_BORRA_FACTOR = 'exec dbo.VAL_BORRA_FACTOR  %d , %d';
     Q_VAL_BORRA_SUBFACTOR = 'exec dbo.VAL_BORRA_SUBFACTOR %d, %d';
     Q_VAL_BORRA_NIVEL = 'exec dbo.VAL_BORRA_NIVEL %d, %d, %d';

begin
     FMaxOrden := 0;
     iOrden:= 0;
     iPlantilla := StrToInt(CampoAsVar( DeltaDS.FieldByName('VL_CODIGO') ));

     case FTipoCatalogo of
          eValFact: sCampo:= 'VF_ORDEN';
          eValSubfact: sCampo:= 'VS_ORDEN';
          eValNiveles:
          begin
               sCampo:= 'VN_ORDEN';
               iOrden:= StrToInt(CampoAsVar( DeltaDS.FieldByName('VS_ORDEN') ) );
          end;
     end;

     if( UpdateKind = ukInsert ) then
     begin
          case FTipoCatalogo of
               eValfact: sSQL:= Format(Q_MAX_VF_ORDEN,[iPlantilla]);
               eValSubfact: sSQL:= Format(Q_MAX_VS_ORDEN,[iPlantilla]);
               eValNiveles: sSQL:= Format(Q_MAX_VN_ORDEN,[iPlantilla,iOrden]);
          end;
          FDataset := oZetaProvider.CreateQuery;
          try
             if oZetaProvider.AbreQueryScript( FDataset, sSQL ) then
                FMaxOrden := FDataSet.FieldByName( 'MAXIMO' ).AsInteger + 1   // Si est� vacio, regresa 0
             else
                 FMaxOrden := 1;
          finally
                 FreeAndNil( FDataSet );
          end;
          with DeltaDS do
          begin
               Edit;
               FieldByName( sCampo ).AsInteger := FMaxOrden;
               Post;
          end;
     end
     else if ( UpdateKind = ukDelete ) then
     begin
          case FTipoCatalogo of
               eValFact: sProcedure:= Format( Q_VAL_BORRA_FACTOR, [  iPlantilla ,DeltaDS.FieldByName( sCampo).AsInteger ] );
               eValSubfact: sProcedure:= Format( Q_VAL_BORRA_SUBFACTOR, [  iPlantilla ,DeltaDS.FieldByName( sCampo).AsInteger ] );
               eValNiveles: sProcedure:= Format( Q_VAL_BORRA_NIVEL, [ iPlantilla ,iOrden,DeltaDS.FieldByName( sCampo).AsInteger ] );
          end;
          oZetaProvider.EjecutaAndFree( sProcedure );
          Applied := True;
     end;
     GrabaCambiosBitacora( Sender, SourceDS, DeltaDS, UpdateKind );
end;

function TdmServerCatalogos.GetValuaciones(Empresa, Parametros: OleVariant): OleVariant;
const
     Q_VALUACIONES = 'select VP_FOLIO, PU_CODIGO, VL_CODIGO, VP_FECHA, VP_COMENTA,' +
                   'VP_GRADO, VP_PT_CODE, VP_PT_NOM, VP_PT_GRAD, VP_FEC_INI,' +
                   'VP_FEC_FIN, US_CODIGO, VP_STATUS, VP_NUM_FIN, VP_PUNTOS, VP_ULTIMA ' +
                   'from V_VAL_PTO where ' +
                   '( ( VP_FECHA between %0:s and %2:s ) or ( %0:s = %1:s and %2:s = %1:s ) )and '+
                   '( ( %3:s = ''T''  ) or ( VP_ULTIMA = %3:s )  ) and ' +
                   '( ( %4:d = -1 ) or ( VP_STATUS = %4:d ) ) and '+
                   '( ( %5:s =  ''''  ) or ( PU_CODIGO = %5:s ) ) and '+
                   '( ( %6:d = 0 ) or ( VL_CODIGO = %6:d ) ) and '+
                   '( ( %7:d = 0 ) or ( VP_GRADO = %7:d ) ) '+
                   'order by VP_FOLIO';

var
   sSQL: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          with ParamList do
          begin
               sSQL := Format( Q_VALUACIONES,
                               [ EntreComillas( DateToStrSQL( ParamByName( 'FechaInicial' ).AsDate ) ),
                               EntreComillas( DateToStrSQL( NullDateTime ) ),
                               EntreComillas( DateToStrSQL( ParamByName( 'FechaFinal' ).AsDate ) ),
                               EntreComillas( ParamByName( 'Historial' ).AsString ),
                               ParamByName( 'Status' ).AsInteger,
                               EntreComillas( ParamByName( 'Puesto' ).AsString ),
                               ParamByName( 'Plantilla').AsInteger,
                               ParamByName( 'Grado' ).AsInteger ]
                                );
          end;
          Result := OpenSQL( EmpresaActiva, sSQL, True );
     end;
     SetComplete;
end;

function TdmServerCatalogos.CopiaValuacion(Empresa, Parametros: OleVariant; var ErrorCount: Integer): Integer;
{$IFNDEF DOS_CAPAS}
const
     Q_VAL_COPIA_PUESTO = '{CALL VAL_COPIA_PUESTO( :VP_FOLIO, :PU_CODIGO, :VP_FECHA, :US_CODIGO, :NewFolio)}';
var
   FCopiar: TZetaCursor;
{$endif}
begin
     {$IFNDEF DOS_CAPAS}
      with oZetaProvider do
      begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          FCopiar:= CreateQuery( Q_VAL_COPIA_PUESTO );
          try
             EmpiezaTransaccion;
             try
                ParamAsInteger( FCopiar, 'VP_FOLIO', ParamList.ParamByName('Folio').AsInteger );
                ParamAsChar( FCopiar, 'PU_CODIGO', ParamList.ParamByName('Puesto').AsString, ZetaCommonClasses.K_ANCHO_CODIGO );
                ParamAsDate( FCopiar, 'VP_FECHA', ParamList.ParamByName('Fecha').AsDate ) ;
                ParamAsInteger( FCopiar, 'US_CODIGO', ParamList.ParamByName('Usuario').AsInteger );
                ParamSalida( FCopiar, 'NewFolio');
                Ejecuta( FCopiar );
                TerminaTransaccion( True );
             except
                   on Error: Exception do
                   begin
                        RollBackTransaccion;
                        if ( Pos( 'UNIQUE INDEX', UpperCase( Error.Message ) ) > 0 ) then
                        begin
                             DatabaseError( Format('� Valuaci�n ya Registrada con el Puesto: %s y Fecha: %s !',[  ParamList.ParamByName('Puesto').AsString ,
                                                                                                                  FechaCorta( ParamList.ParamByName('Fecha').AsDate )  ] ) );
                        end;
                        raise;
                   end;
             end;
             Result := GetParametro( FCopiar, 'NewFolio').AsInteger;

          finally
                 FreeAndNil( FCopiar );
          end;
      end;
     {$ELSE}
     Result:= 0;
     {$ENDIF}
     SetComplete;
end;

function TdmServerCatalogos.AgregaValuacion(Empresa, oDelta: OleVariant; var ErrorCount: Integer; out iFolio: Integer; iPlantilla: Integer): OleVariant;
const
     Q_VAL_PREPARA_PUNTOS = 'exec dbo.VAL_PREPARA_PUNTOS  %d , %d';
var
   sSQL: String;

   function GetMaxFolioValuacion(sCampo:string; sTabla:string): Integer;
   const
        Q_GET_MAX_FOLIO = 'select MAX(%s) MAXIMO from %s ';
   var
      FDataSet : TZetaCursor;
   begin
        FDataset := oZetaProvider.CreateQuery( Format(Q_GET_MAX_FOLIO,[sCampo,sTabla]) );
        if ( FDataSet <> nil )then
        begin
             try
                FDataSet.Open;
                Result := FDataSet.FieldByName('MAXIMO').AsInteger;
                FDataSet.Close;
             finally
                    FreeAndNil(FDataSet);
             end;
        end
        else
        begin
             Result := 1; //Se inicializa el orden en la tabla
        end;

   end;

begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          if ( iFolio = 0 ) then
          begin
               SetTablaInfo( eValuacion );
               Result := GrabaTabla( Empresa, oDelta, ErrorCount );
               if ( ErrorCount = 0 ) then
               begin
                    iFolio := GetMaxFolioValuacion('VP_FOLIO','VAL_PTO');
                    sSQL:= Format( Q_VAL_PREPARA_PUNTOS, [ iFolio, iPlantilla ] );
                    EjecutaAndFree( sSQL );
               end;
          end;
     end;
     SetComplete;
end;

function TdmServerCatalogos.GrabaValuacion(Empresa, oDelta, oPDelta: OleVariant; out iFolio: Integer; var ErrorCount,
  PErrorCount: Integer; out Valor: OleVariant): OleVariant;
begin
     ErrorCount := 0;
     PErrorCount := 0;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          if ( oDelta = null ) then
             Result := oDelta
          else
          begin
               SetTablaInfo( eValuacion );
               Result := GrabaTabla( Empresa, oDelta, ErrorCount );
          end;
          if ( ErrorCount = 0 ) then
          begin
               if ( oPDelta = null ) then
                  Valor := oPDelta
               else
               begin
                    SetTablaInfo( eValPuntos );
                    Valor := GrabaTabla( Empresa, oPDelta, PErrorCount );
               end;
          end;
     end;
     SetComplete;
end;

function TdmServerCatalogos.GetValPuntos(Empresa: OleVariant; iValuacion: Integer): OleVariant;
const
     Q_GET_VAL_PUNTOS = 'select VP_FOLIO, VT_ORDEN, VT_CUAL, VT_COMENTA, VT_PUNTOS '+
                       'from V_VPUNTOS '+
                       'where VP_FOLIO = %d '+
                       'order by VT_ORDEN';

begin
     with oZetaProvider do
     begin
          Result := OpenSQL( Empresa, Format( Q_GET_VAL_PUNTOS, [ iValuacion ] ), TRUE ) ;
     end;
end;

function TdmServerCatalogos.GrabaMaestros(Empresa, oDelta: OleVariant; var ErrorCount: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          FTipoCatalogo:= eMaestros;
          EmpresaActiva := Empresa;
          SetTablaInfo( eMaestros );
          TablaInfo.AfterUpdateRecord:= AfterUpdateMaestros;
          TablaInfo.BeforeUpdateRecord := BeforeUpdateMaestros;
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
     end;
     SetComplete;
end;

procedure TdmServerCatalogos.BeforeUpdateMaestros(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
var
   i : Integer;
   lHuboCambios: Boolean;
begin
     if ( UpdateKind = ukModify ) then
     begin
          lHuboCambios := FALSE;
          with DeltaDS do
               for i := 0 to FieldCount-1 do
                   if CambiaCampo( Fields[i] ) then
                   begin
                        lHuboCambios := TRUE;
                        Break;
                   end;
          Applied := ( not lHuboCambios );
     end;
end;

procedure TdmServerCatalogos.AfterUpdateMaestros(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind );
const
     K_UPDATE_FOTO = 'UPDATE MAESTRO SET MA_IMAGEN = NULL WHERE MA_CODIGO = %s';
     K_DELETE = 'D';
begin
     if ( UpdateKind = ukModify ) then
     begin
        with DeltaDS.FieldByName('MA_IMAGEN') do
        begin
             { AL 2014-1017
             Cambio como parte del Bug #7797: Despu�s de borrar la imagen, los controles siguen aparentando que tienen una imagen almacenada.}
             // if VarIsNull( NewValue )  and (OldValue <> NewValue) then
             // if VarToStr (NewValue) = VACIO then
             if VarToStr (NewValue) = K_DELETE then
                with oZetaProvider do
                     ExecSQL( EmpresaActiva, Format( K_UPDATE_FOTO, [ EntreComillas( CampoAsVar( DeltaDS.FieldByName('MA_CODIGO') ) )  ] ) );
        end;
     end;
     GrabaCambiosBitacora( Sender, SourceDS, DeltaDS, UpdateKind );
end;

function TdmServerCatalogos.GetRSocial(Empresa: OleVariant): OleVariant;
begin
     Result := GetCatalogo( eRSocial, Empresa );
     SetComplete;
end;

function TdmServerCatalogos.GetVistaCursos( Empresa: OleVariant): OleVariant;
const
     Q_GET_VISTA_CURSOS = 'select CU_CODIGO,CU_FOLIO,CU_NOMBRE,CU_ACTIVO,MA_CODIGO,CU_HORAS,CU_REVISIO,CU_COSTO1,CU_COSTO2, CU_COSTO3, ' +
                          'CU_NOMBRE as CU_DESCRIP '+
                          'from V_CURSO '+
                          'order by CU_CODIGO';
begin
     with oZetaProvider do
     begin
          with cdsTemporal do
          begin
               Lista := OpenSQL( Empresa, Q_GET_VISTA_CURSOS, TRUE );
               while ( not EOF ) do
               begin
                    if strLleno( FieldByName( 'CU_FOLIO' ).AsString ) then
                    begin
                         Edit;
                         FieldByName( 'CU_DESCRIP' ).AsString := FieldByName( 'CU_FOLIO' ).AsString + ' : ' +
                                                                 FieldByName( 'CU_NOMBRE' ).AsString;
                         Post;
                    end;
                    Next;
               end;
               MergeChangeLog;
               Result := Lista;
          end;
     end;
     SetComplete;
end;

function TdmServerCatalogos.GetCertificacionesPuesto(const Empresa: OleVariant;const sPuesto: WideString ):OleVariant;
begin
     SetTablaInfo( eMatrizCertificPuesto );
     with oZetaProvider do
     begin
          TablaInfo.SetOrder( 'CI_CODIGO' );
          TablaInfo.Filtro := Format( '( PU_CODIGO = ''%s'' )', [ sPuesto ] );
          SetDetailInfo( eMatrizCertificPuesto );
          Result := GetMasterDetail( Empresa );
     end;

end;

function TdmServerCatalogos.GetPuestosCertificacion(const Empresa: OleVariant;const sCertificacion: WideString ): OleVariant;
begin
      SetTablaInfo( eMatrizPuestoCertific );
     with oZetaProvider do
     begin
          TablaInfo.SetOrder( 'PU_CODIGO' );
          TablaInfo.Filtro := Format( '( CI_CODIGO = ''%s'' )', [ sCertificacion ] );
          SetDetailInfo( eMatrizPuestoCertific );
          Result := GetMasterDetail( Empresa );
     end;
end;

function TdmServerCatalogos.GetMatrizCertifica(Empresa: OleVariant;const sCodigo: WideString; lMatriz: WordBool): OleVariant;
begin
     if ( lMatriz ) then
        Result := GetPuestosCertificacion( Empresa, sCodigo )
     else
        Result := GetCertificacionesPuesto( Empresa ,sCodigo );
     SetComplete;
end;

function TdmServerCatalogos.GrabaMatrizCetifica(Empresa: OleVariant;lCertificacion: WordBool; Delta: OleVariant;out ErrorCount: Integer): OleVariant;
begin
     if ( lCertificacion ) then
        Result := GrabaCatalogo( Empresa, Ord(eMatrizPuestoCertific   ), Delta, ErrorCount )
     else
        Result := GrabaCatalogo( Empresa, Ord( eMatrizCertificPuesto  ), Delta, ErrorCount );
     SetComplete;
end;

function TdmServerCatalogos.GetReglasPrestamo(Empresa: OleVariant): OleVariant;
begin
     SetTablaInfo( eReglasPrestamos );
     with oZetaProvider do
     begin
          TablaInfo.SetOrder( 'RP_ORDEN' );
          Result := GetTabla( Empresa );
     end;
end;

function TdmServerCatalogos.GetPrestamoPorRegla(Empresa: OleVariant; Codigo: Integer): OleVariant;
begin
     SetTablaInfo( ePrestaXRegla );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( '( RP_CODIGO = %d )', [ Codigo ] );
          Result := GetTabla( Empresa );
     end;
end;

procedure TdmServerCatalogos.BeforeUpdateReglasPrestamo(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
  UpdateKind: TUpdateKind; var Applied: Boolean);
const
     {$ifdef INTERBASE}
          Q_SP_BORRA_REGLAPREST = 'execute procedure SP_BORRA_REGLAPREST ( %s , %d )';
     {$endif}
     {$ifdef MSSQL}
          Q_SP_BORRA_REGLAPREST = 'exec dbo.SP_BORRA_REGLAPREST  %s , %d';
     {$endif}
begin

      if ( UpdateKind = ukInsert )then
      begin
           with DeltaDS do
           begin
                Edit;
                FieldByName( 'RP_ORDEN' ).AsInteger := GetMaxFolio('RP_ORDEN','REGLAPREST');
                Post;
           end;
      end
      else if ( UpdateKind = ukDelete  )then
      begin
           oZetaProvider.EjecutaAndFree(  Format( Q_SP_BORRA_REGLAPREST, [ EntreComillas( DeltaDS.FieldByName( 'RP_CODIGO' ).AsString ),DeltaDS.FieldByName( 'RP_ORDEN' ).AsInteger ] ) );
           Applied := True;
      end;
end;

function TdmServerCatalogos.GetGruposConceptos(Empresa: OleVariant): OleVariant;
const
     K_GET_GRUPOS_CONCEPTOS = 'select Distinct(G.GR_CODIGO),G.GR_DESCRIP from GRUPO G LEFT OUTER JOIN ACCESO A on G.GR_CODIGO = A.GR_CODIGO'+
                              ' where (A.AX_NUMERO in (%s) and A.AX_DERECHO >= %d ) '+
                              ' or(A.AX_NUMERO in (%s) and A.AX_DERECHO >= %d ) '+
                              ' and A.CM_CODIGO = %s';
var
   sDerechosEdicion, sDerechosConsulta :string;
begin
     sDerechosEdicion := IntToStr(D_NOM_EXCEP_MONTOS) +','+
                         IntToStr(D_NOM_DATOS_MONTOS ) +','+
                         IntToStr(D_EMP_SIM_FINIQUITOS_EMPLEADO );

     sDerechosConsulta := IntToStr(D_NOM_REG_EXCEP_MONTO ) +','+
                          IntToStr(D_NOM_REG_EXCEP_MONTO_GLOBAL  ) +','+
                          IntToStr(D_NOM_EXCEP_GLOBALES  );

     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Result := OpenSQL( Comparte, Format( K_GET_GRUPOS_CONCEPTOS,[sDerechosEdicion,K_DERECHO_CAMBIO,sDerechosConsulta,K_DERECHO_CONSULTA, EntreComillas(CodigoEmpresaActiva) ] ), True );
          SetComplete;          
     end;
end;

function TdmServerCatalogos.GetEstablecimientos(Empresa: OleVariant): OleVariant;
begin
     Result := GetCatalogo(eEstablecimientos,Empresa);
end;

function TdmServerCatalogos.GrabaEstablecimientos(Empresa,Delta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo(eEstablecimientos);
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Result := GrabaTabla(Empresa,Delta,ErrorCount);
     end;
end;

function TdmServerCatalogos.GetTiposPension(Empresa: OleVariant): OleVariant;
begin
     Result := GetCatalogo(eTipoPension,Empresa);
     SetComplete;
end;

function TdmServerCatalogos.GetSeguroGastosMedicos(Empresa: OleVariant; out Vigencias: OleVariant): OleVariant;
begin
     Vigencias := GetCatalogo(eViewVigenciasSeguro,Empresa);
     Result := GetCatalogo(eSegurosGastosMed,Empresa);
     SetComplete;
end;

function TdmServerCatalogos.GrabaVigencias(Empresa, Delta: OleVariant;out ErrorCount: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          if not VarIsNull( Delta ) then
          begin
               SetTablaInfo( eVigenciasSeguro  );
               FTipoCatalogo := eVigenciasSeguro ;
               TablaInfo.AfterUpdateRecord := GrabaCambiosBitacora;
               Result := GrabaTablaGrid( Empresa, Delta, ErrorCount );
          end;
     end;
     SetComplete;
end;

{JB Costeo}
function TdmServerCatalogos.GetCosteoCriterios(Empresa: OleVariant): OleVariant;
begin
     Result := GetCatalogo( eCosteoCriterios, Empresa);
     SetComplete;
end;

function TdmServerCatalogos.GetCosteoGrupos(Empresa: OleVariant): OleVariant;
begin
     Result := GetCatalogo( eCosteoGrupos, Empresa);
     SetComplete;
end;

function TdmServerCatalogos.GetCosteoCriteriosGrupos(Empresa: OleVariant): OleVariant;
const
     Q_GET_COSTEO_CRITERIOS_GRUPOS = 'SELECT GC.GpoCostoCodigo, CC.CritCostoID, CC.GpoCostoID, CC.CO_NUMERO, CC.ConcepCostoID, GC.GpoCostoNombre ' +
                                     'FROM T_ConceptosCosto AS CC INNER JOIN T_GruposCosto AS GC ON CC.GpoCostoID = GC.GpoCostoID ' +
                                     'ORDER BY GC.GpoCostoCodigo, CC.CritCostoID, CC.ConcepCostoID ';
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          Result := OpenSQL( Empresa, Q_GET_COSTEO_CRITERIOS_GRUPOS, TRUE );
     end;
     SetComplete;
end;

function TdmServerCatalogos.GetCosteoConceptosDisponibles(Empresa: OleVariant; const Grupo: WideString; Criterio: Integer): OleVariant;
const
     Q_GET_COSTEO_CONCEPTOS_DISPONIBLES = 'SELECT CO_NUMERO, CO_DESCRIP, CO_TIPO, ' +
                                          ' ( select COUNT(*) from T_ConceptosCosto where GpoCostoID = (select GpoCostoID from T_GruposCosto where GpoCostoCodigo = ''%0:s'') and CritCostoID = %1:d and CO_NUMERO = CONCEPTO.CO_NUMERO ) as Encendido, CO_ACTIVO ' +
                                          ' FROM CONCEPTO where CO_NUMERO not in ' +
                                          ' (select CO_NUMERO from T_ConceptosCosto where GpoCostoID = (select GpoCostoID from T_GruposCosto where GpoCostoCodigo = ''%0:s'') and CritCostoID <> %1:d) ' +
                                          ' and CO_NUMERO < 1000 and CO_ACTIVO = ''S''';
begin
     with oZetaProvider do
     begin
          Result := OpenSQL( Empresa, Format( Q_GET_COSTEO_CONCEPTOS_DISPONIBLES, [ Grupo, Criterio ] ), TRUE );
     end;
     SetComplete;
end;

function TdmServerCatalogos.GetTablasAmortizacion(Empresa: OleVariant; out Costos: OleVariant): OleVariant;
begin
     Costos := GetCatalogo(eCatCostosTablaAmort,Empresa);
     Result := GetCatalogo(eCatTablaAmortizacion,Empresa);
     SetComplete;
end;

function TdmServerCatalogos.GrabaCostosTablaAmort(Empresa, Delta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          if not VarIsNull( Delta ) then
          begin
               SetTablaInfo( eCatCostosTablaAmort );
               FTipoCatalogo := eCatCostosTablaAmort;
               TablaInfo.AfterUpdateRecord := GrabaCambiosBitacora;
               Result := GrabaTablaGrid( Empresa, Delta, ErrorCount );
          end;
     end;
     SetComplete;
end;

function TdmServerCatalogos.GetCompetencias(Empresa: OleVariant; out oRevisiones, oNiveles, oCursos: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          SetTablaInfo( eCompetencias );
          TablaInfo.SetOrder( 'CC_CODIGO' );
          Result := GetTabla(Empresa);

          SetTablaInfo(eNivelesCompetencias );
          TablaInfo.SetOrder( 'NC_NIVEL' );
          oNiveles := GetTabla(Empresa);

          SetTablaInfo(eRevisionCompetencias );
          TablaInfo.SetOrder( 'RC_FEC_INI' );
          oRevisiones := GetTabla(Empresa);

          SetTablaInfo(eCursosCompetencias );
          TablaInfo.SetOrder( 'CC_ORDEN' );
          oCursos := GetTabla(Empresa);
          SetComplete;
     end;
end;

function TdmServerCatalogos.GetCatPerfiles(Empresa, Tipo: OleVariant; out oRevisiones, oPtoGpoComp: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          SetTablaInfo( eCPerfiles );
          TablaInfo.SetOrder( 'CP_CODIGO' );
          Result := GetTabla(Empresa);

          SetTablaInfo(eRevisionPerfiles );
          oRevisiones := GetTabla(Empresa);

          SetTablaInfo(ePuestoGpoCompeten );
          oPtoGpoComp := GetTabla(Empresa);
          SetComplete;
     end;
end;

function TdmServerCatalogos.GrabaCompetencias(Empresa, Delta, Niveles, Cursos, Revisiones: OleVariant; out ErrorCount: Integer; out NivelesRes,
  CursosRes, RevisionesRes: OleVariant; out ErrorNiv, ErrorCur, ErrorRev: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          if ( not VarIsNull( Delta ) ) then
          begin
               setTablaInfo( eCompetencias );
               FTipoCatalogo := eCompetencias;
               TablaInfo.AfterUpdateRecord := GrabaCambiosBitacora;
               Result:= GrabaTabla(Empresa,Delta,ErrorCount );
          end
          else
              Result := Delta;
          if ErrorCount = 0 then
          begin
               if not VarIsNull( Niveles ) then
               begin
                    SetTablaInfo( eNivelesCompetencias  );
                    FTipoCatalogo := eNivelesCompetencias ;
                    TablaInfo.AfterUpdateRecord := GrabaCambiosBitacora;
                    NivelesRes := GrabaTablaGrid( Empresa, Niveles, ErrorNiv );
               end;
               if not VarIsNull( Cursos ) then
               begin
                    SetTablaInfo( eCursosCompetencias   );
                    FTipoCatalogo := eCursosCompetencias;
                    TablaInfo.AfterUpdateRecord := GrabaCambiosBitacora;
                    CursosRes := GrabaTablaGrid( Empresa, Cursos, ErrorCur );
               end;
               if not VarIsNull( Revisiones ) then
               begin
                    SetTablaInfo( eRevisionCompetencias );
                    FTipoCatalogo := eRevisionCompetencias;
                    
                    TablaInfo.AfterUpdateRecord := GrabaFechasRevisiones;
                    RevisionesRes := GrabaTablaGrid( Empresa, Revisiones, ErrorRev );
               end;
          end;
     end;
     SetComplete;
end;

function TdmServerCatalogos.GrabaCPerfiles(Empresa, Delta,oRevisiones: OleVariant; out oRevisionesR: OleVariant; out ErrorCount,ErrorCountR: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          if ( not VarIsNull( Delta ) ) then
          begin
               setTablaInfo( eCPerfiles  );
               FTipoCatalogo := eCPerfiles;
               TablaInfo.AfterUpdateRecord := GrabaCambiosBitacora;
               Result:= GrabaTabla(Empresa,Delta,ErrorCount );
          end
          else
              Result := Delta;
          if ErrorCount = 0 then
          begin
               if not VarIsNull( oRevisiones ) then
               begin
                    SetTablaInfo( eRevisionPerfiles );
                    FTipoCatalogo := eRevisionPerfiles;
                    TablaInfo.AfterUpdateRecord := GrabaFechasRevisiones;
                    oRevisionesR := GrabaTablaGrid( Empresa, oRevisiones, ErrorCountR );
               end
               else
                   oRevisionesR :=oRevisiones
          end;
     end;
     SetComplete;
end;

function TdmServerCatalogos.GetMatrizPerfilComps(Empresa: OleVariant): OleVariant;
const
     Q_GET_MATRIZ_FUNC = 'select CPF.RP_REVISIO,CN.NC_DESCRIP,CP.CP_CODIGO,CP.RP_FEC_INI,CP.CC_CODIGO,CP.NC_NIVEL,CP.PC_PESO from C_PER_COMP CP '+
                         ' left outer join C_REV_PERF CPF on CPF.RP_FEC_INI = CP.RP_FEC_INI and CPF.CP_CODIGO = CP.CP_CODIGO'+
                         ' left outer join C_NIV_COMP CN on CN.CC_CODIGO = CP.CC_CODIGO and CP.NC_NIVEL = CN.NC_NIVEL';
begin
     with oZetaProvider do
     begin
          Result := OpenSQL( Empresa, Q_GET_MATRIZ_FUNC, TRUE );
     end;
     SetComplete;
end;

function TdmServerCatalogos.GrabaMatrizPerfComp(Empresa, Delta: OleVariant;out ErrorCount: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          SetTablaInfo(ePerfilesCompetencias);
          EmpresaActiva := Empresa;
          FTipoCatalogo := ePerfilesCompetencias;
          TablaInfo.AfterUpdateRecord := GrabaCambiosBitacora;
          Result := GrabaTabla( Empresa, Delta, ErrorCount );
     end;
     SetComplete;
end;

procedure TdmServerCatalogos.GrabaFechasRevisiones(Sender: TObject;SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;UpdateKind: TUpdateKind);
var
   NuevoValor,ValorAnterior:TDate;
   Competencia,sTabla,sFecIni,sFecFin,sCodigo:string;

const Q_UPDATE_REV = 'Update %0:s set %1:s = %2:s '+
                          ' where %3:s = %4:s and %1:s = %5:s';

      Q_INS_REV = 'Update %0:s set %1:s = %2:s '+
                          ' where %3:s = %4:s and %5:s < %2:s AND %1:s = %6:s';

begin
     if FTipoCatalogo = eRevisionCompetencias then
     begin
          sTabla := 'C_REV_COMP';
          sFecIni := 'RC_FEC_INI';
          sFecFin := 'RC_FEC_FIN';
          sCodigo := 'CC_CODIGO';
     end
     else
     begin
          sTabla := 'C_REV_PERF';
          sFecIni := 'RP_FEC_INI';
          sFecFin := 'RP_FEC_FIN';
          sCodigo := 'CP_CODIGO';
     end;
     with DeltaDS do
     begin
          if( UpdateKind in [ ukInsert ]) then
          begin
               oZetaProvider.EjecutaAndFree(  Format( Q_INS_REV, [ sTabla,
                                                                        sFecFin,
                                                                        DateToStrSQLC(FieldByName ( sFecIni ).AsDateTime),
                                                                        sCodigo,
                                                                        EntreComillas(FieldByName ( sCodigo ).AsString),
                                                                        sFecIni,
                                                                        DateToStrSQLC(NullDateTime) ]  ) );
          end
          else if( UpdateKind in [ ukModify ]) then
          begin
               if ( CampoAsVar( FieldByName( sFecIni ) ) <> FieldByName( sFecIni ).OldValue )then
               begin
                    NuevoValor := FieldByName ( sFecIni ).AsDateTime;
                    ValorAnterior :=  VarAsType( FieldByName( sFecIni ).OldValue, varDate );
                    Competencia := FieldByName(sCodigo).OldValue;
                    oZetaProvider.EjecutaAndFree(  Format( Q_UPDATE_REV, [ sTabla,
                                                                           sFecFin,
                                                                           DateToStrSQLC(NuevoValor),
                                                                           sCodigo,
                                                                           EntreComillas(Competencia),
                                                                           DateToStrSQLC(ValorAnterior) ]  ) );
               end;

          end
          else if( UpdateKind in [ ukDelete ]) then
          begin
               oZetaProvider.EjecutaAndFree(  Format( Q_UPDATE_REV, [ sTabla,
                                                                           sFecFin,
                                                                           DateToStrSQLC(NullDateTime),
                                                                           sCodigo,
                                                                           EntreComillas(FieldByName ( sCodigo ).AsString),
                                                                           DateToStrSQLC(FieldByName ( sFecIni ).AsDateTime) ]  ) );
          end;
     end;
end;

function TdmServerCatalogos.GetTiposSAT(
  Empresa: OleVariant): OleVariant;
begin
     Result := GetCatalogo( eTipoSAT, Empresa );
     SetComplete;
end;

function TdmServerCatalogos.GetConceptosSeguridad(Empresa: OleVariant): OleVariant;
begin
     Result := GetCatalogo( eConceptosSeguridad, Empresa );
     SetComplete;
end;

function TdmServerCatalogos.SetPrioridadConceptoNomina(Empresa: OleVariant; Tipo, Numero: Integer): OleVariant;
const K_EXECUTE_BAJA_PRIORIDAD = 'EXECUTE Conceptos_PrevisionSocial_BajaPrioridad %d';
      K_EXECUTE_SUBE_PRIORIDAD = 'EXECUTE Conceptos_PrevisionSocial_SubePrioridad %d';
      K_EXECUTE_CALCULA_PRIORIDAD = 'EXECUTE Conceptos_PrevisionSocial_CalcularPrioridad %d';
      K_CONST_BAJAPRIORIDAD = 2;
      K_CONST_SUBEPRIORIDAD = 1;
var
   sQuery, sError: String;
begin
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             if Tipo = K_CONST_SUBEPRIORIDAD then
                sQuery := Format ( K_EXECUTE_SUBE_PRIORIDAD, [Numero])
             else if Tipo = K_CONST_BAJAPRIORIDAD then
                  sQuery := Format ( K_EXECUTE_BAJA_PRIORIDAD, [Numero])
             else
                 sQuery := Format ( K_EXECUTE_CALCULA_PRIORIDAD, [Numero]);

             ExecSQL( EmpresaActiva, sQuery );
        end;
     except on E: Exception do
            sError := E.Message;
     end;
end;

{$ifndef DOS_CAPAS}
initialization
  TComponentFactory.Create(ComServer, TdmServerCatalogos, Class_dmServerCatalogos, ciMultiInstance, ZetaServerTools.GetThreadingModel);
{$endif}
end.



