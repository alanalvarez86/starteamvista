unit TimbradoCatalogos_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 22/03/2014 11:29:22 a.m. from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3win_30_Timbrado_2014\MTS\TimbradoCatalogos.tlb (1)
// LIBID: {2104511B-5DDF-4B09-A9F2-F53DB6046FE4}
// LCID: 0
// Helpfile: 
// HelpString: Tress Catalogos
// DepndLst: 
//   (1) v1.0 Midas, (C:\Windows\SysWOW64\midas.dll)
//   (2) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, Midas, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  TimbradoCatalogosMajorVersion = 1;
  TimbradoCatalogosMinorVersion = 0;

  LIBID_TimbradoCatalogos: TGUID = '{2104511B-5DDF-4B09-A9F2-F53DB6046FE4}';

  IID_IdmServerCatalogosTimbrado: TGUID = '{534433A8-72BB-42F6-AD6D-DFBD29388C62}';
  CLASS_dmServerCatalogosTimbrado: TGUID = '{3A10B2C6-DC89-40CF-A722-89DD17A68E28}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerCatalogosTimbrado = interface;
  IdmServerCatalogosTimbradoDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerCatalogosTimbrado = IdmServerCatalogosTimbrado;


// *********************************************************************//
// Interface: IdmServerCatalogosTimbrado
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {534433A8-72BB-42F6-AD6D-DFBD29388C62}
// *********************************************************************//
  IdmServerCatalogosTimbrado = interface(IAppServer)
    ['{534433A8-72BB-42F6-AD6D-DFBD29388C62}']
    function GetPuestos(Empresa: OleVariant): OleVariant; safecall;
    function GetTurnos(Empresa: OleVariant): OleVariant; safecall;
    function GetCursos(Empresa: OleVariant): OleVariant; safecall;
    function GetCalendario(Empresa: OleVariant): OleVariant; safecall;
    function GetClasifi(Empresa: OleVariant): OleVariant; safecall;
    function GetConceptos(Empresa: OleVariant): OleVariant; safecall;
    function GetCondiciones(Empresa: OleVariant): OleVariant; safecall;
    function GetContratos(Empresa: OleVariant): OleVariant; safecall;
    function GetEventos(Empresa: OleVariant): OleVariant; safecall;
    function GetFestTurno(Empresa: OleVariant; const sTurnoFestivo: WideString; iVigencia: Integer): OleVariant; safecall;
    function GetFolios(Empresa: OleVariant): OleVariant; safecall;
    function GetHorarios(Empresa: OleVariant): OleVariant; safecall;
    function GetNomParam(Empresa: OleVariant): OleVariant; safecall;
    function GetRPatron(Empresa: OleVariant): OleVariant; safecall;
    function GetInvitadores(Empresa: OleVariant): OleVariant; safecall;
    function GetReglas(Empresa: OleVariant): OleVariant; safecall;
    function GetPrestaciones(Empresa: OleVariant): OleVariant; safecall;
    function GetPeriodos(Empresa: OleVariant; iYear: Integer; iTipo: Integer): OleVariant; safecall;
    function GetOtrasPer(Empresa: OleVariant): OleVariant; safecall;
    function GetMaestros(Empresa: OleVariant): OleVariant; safecall;
    function GetMatriz(Empresa: OleVariant; lCurso: WordBool; const sCodigo: WideString): OleVariant; safecall;
    function GrabaMatriz(Empresa: OleVariant; lCurso: WordBool; oDelta: OleVariant; 
                         out ErrorCount: Integer): OleVariant; safecall;
    function GrabaCatalogo(Empresa: OleVariant; iCatalogo: Integer; oDelta: OleVariant; 
                           out ErrorCount: Integer): OleVariant; safecall;
    function GetTools(Empresa: OleVariant): OleVariant; safecall;
    function GetPercepFijas(Empresa: OleVariant; const sCodigo: WideString): OleVariant; safecall;
    function GrabaPercepFijas(Empresa: OleVariant; oDelta: OleVariant; const sCodigo: WideString; 
                              out ErrorCount: Integer): OleVariant; safecall;
    function GetHerramientas(Empresa: OleVariant; const sCodigo: WideString): OleVariant; safecall;
    function GrabaHerramientas(Empresa: OleVariant; oDelta: OleVariant; const sCodigo: WideString; 
                               out ErrorCount: Integer): OleVariant; safecall;
    function GetSesiones(Empresa: OleVariant; const sCodigo: WideString; dFechaIni: TDateTime; 
                         dFechaFin: TDateTime; iFolio: Integer): OleVariant; safecall;
    function GrabaSesiones(Empresa: OleVariant; oDelta: OleVariant; oKCDelta: OleVariant; 
                           out iFolio: Integer; iOperacion: Integer; var ErrorCount: Integer; 
                           var ErrorKCCount: Integer; out Valor: OleVariant): OleVariant; safecall;
    function GetHisCursos(Empresa: OleVariant; iSesion: Integer): OleVariant; safecall;
    function GetAccReglas(Empresa: OleVariant): OleVariant; safecall;
    function GetAulas(Empresa: OleVariant): OleVariant; safecall;
    function GetPrerequisitosCurso(Empresa: OleVariant; const sPrerequisitosCurso: WideString): OleVariant; safecall;
    function GetCertificaciones(Empresa: OleVariant): OleVariant; safecall;
    function GetTiposPoliza(Empresa: OleVariant): OleVariant; safecall;
    function GetSeccionesPerfil(Empresa: OleVariant): OleVariant; safecall;
    function GetCamposPerfil(Empresa: OleVariant; const sSeccion: WideString): OleVariant; safecall;
    function GrabaSeccion(Empresa: OleVariant; oDelta: OleVariant; var ErrorCount: Integer; 
                          out iFolio: Integer): OleVariant; safecall;
    function GrabaCamposPerfil(Empresa: OleVariant; oDelta: OleVariant; var ErrorCount: Integer; 
                               out iFolio: Integer): OleVariant; safecall;
    function CambiaOrden(Empresa: OleVariant; iCatalogo: Integer; iActual: Integer; 
                         iNuevo: Integer; const sCodigo: WideString; const sClasifi: WideString): OleVariant; safecall;
    function GetPerfilesPuesto(Empresa: OleVariant): OleVariant; safecall;
    function GetPerfiles(Empresa: OleVariant; const sPuesto: WideString): OleVariant; safecall;
    function CopiarPerfil(Empresa: OleVariant; const sFuente: WideString; const sDestino: WideString): OleVariant; safecall;
    function GetDescPerfil(Empresa: OleVariant; const sPuesto: WideString): OleVariant; safecall;
    function GrabaDescPerfil(Empresa: OleVariant; oDelta: OleVariant; const sCampos: WideString; 
                             out ErrorCount: Integer; out iOrden: Integer): OleVariant; safecall;
    function GetTPeriodos(Empresa: OleVariant): OleVariant; safecall;
    function GetProvCap(Empresa: OleVariant): OleVariant; safecall;
    function GetValPlantilla(Empresa: OleVariant): OleVariant; safecall;
    function GetValFactores(Empresa: OleVariant; iPlantilla: Integer): OleVariant; safecall;
    function GetValSubFactores(Empresa: OleVariant; iPlantilla: Integer): OleVariant; safecall;
    function GetValNiveles(Empresa: OleVariant; iPlantilla: Integer; iOrden: Integer): OleVariant; safecall;
    function GrabaCamposVal(Empresa: OleVariant; iCatalogo: Integer; oDelta: OleVariant; 
                            var ErrorCount: Integer; out iFolio: Integer): OleVariant; safecall;
    function GetValuaciones(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function AgregaValuacion(Empresa: OleVariant; oDelta: OleVariant; var ErrorCount: Integer; 
                             out iFolio: Integer; iPlantilla: Integer): OleVariant; safecall;
    function GrabaValuacion(Empresa: OleVariant; oDelta: OleVariant; oPDelta: OleVariant; 
                            out iFolio: Integer; var ErrorCount: Integer; var PErrorCount: Integer; 
                            out Valor: OleVariant): OleVariant; safecall;
    function GetValPuntos(Empresa: OleVariant; iValuacion: Integer): OleVariant; safecall;
    function CopiaValuacion(Empresa: OleVariant; Parametros: OleVariant; var ErrorCount: Integer): Integer; safecall;
    function GrabaMaestros(Empresa: OleVariant; oDelta: OleVariant; var ErrorCount: Integer): OleVariant; safecall;
    function GetRSocial(Empresa: OleVariant): OleVariant; safecall;
    function GetVistaCursos(Empresa: OleVariant): OleVariant; safecall;
    function GrabaMatrizCetifica(Empresa: OleVariant; lCertificacion: WordBool; Delta: OleVariant; 
                                 out ErrorCount: Integer): OleVariant; safecall;
    function GetMatrizCertifica(Empresa: OleVariant; const sCodigo: WideString; lMatriz: WordBool): OleVariant; safecall;
    function GetHistRev(Empresa: OleVariant; const sCodigo: WideString): OleVariant; safecall;
    function GrabaCursos(Empresa: OleVariant; oDelta: OleVariant; oDeltaH: OleVariant; 
                         out ErrorCount: Integer; out ErrorCountH: Integer; out oHResult: OleVariant): OleVariant; safecall;
    function GetReglasPrestamo(Empresa: OleVariant): OleVariant; safecall;
    function GetPrestamoPorRegla(Empresa: OleVariant; Codigo: Integer): OleVariant; safecall;
    function GetGruposConceptos(Empresa: OleVariant): OleVariant; safecall;
    function GetEstablecimientos(Empresa: OleVariant): OleVariant; safecall;
    function GrabaEstablecimientos(Empresa: OleVariant; Delta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetTiposPension(Empresa: OleVariant): OleVariant; safecall;
    function GetSeguroGastosMedicos(Empresa: OleVariant; out Vigencias: OleVariant): OleVariant; safecall;
    function GrabaVigencias(Empresa: OleVariant; Delta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetCosteoCriterios(Empresa: OleVariant): OleVariant; safecall;
    function GetCosteoGrupos(Empresa: OleVariant): OleVariant; safecall;
    function GetCosteoCriteriosGrupos(Empresa: OleVariant): OleVariant; safecall;
    function GetCosteoConceptosDisponibles(Empresa: OleVariant; const Grupo: WideString; 
                                           Criterio: Integer): OleVariant; safecall;
    function GetTablasAmortizacion(Empresa: OleVariant; out Costos: OleVariant): OleVariant; safecall;
    function GrabaCostosTablaAmort(Empresa: OleVariant; Delta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetCompetencias(Empresa: OleVariant; out oRevisiones: OleVariant; 
                             out oNiveles: OleVariant; out oCursos: OleVariant): OleVariant; safecall;
    function GetCatPerfiles(Empresa: OleVariant; Tipo: OleVariant; out oRevisiones: OleVariant; 
                            out oPtoGpoComp: OleVariant): OleVariant; safecall;
    function GrabaCompetencias(Empresa: OleVariant; Delta: OleVariant; Niveles: OleVariant; 
                               Cursos: OleVariant; Revisiones: OleVariant; out ErrorCount: Integer; 
                               out NivelesRes: OleVariant; out CursosRes: OleVariant; 
                               out RevisionesRes: OleVariant; out ErrorNiv: Integer; 
                               out ErrorCur: Integer; out ErrorRev: Integer): OleVariant; safecall;
    function GrabaCPerfiles(Empresa: OleVariant; Delta: OleVariant; oRevisiones: OleVariant; 
                            out oRevisionesR: OleVariant; out ErrorCount: Integer; 
                            out ErrorCountR: Integer): OleVariant; safecall;
    function GetMatrizPerfilComps(Empresa: OleVariant): OleVariant; safecall;
    function GrabaMatrizPerfComp(Empresa: OleVariant; Delta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetTiposSAT(Empresa: OleVariant): OleVariant; safecall;
    function GrabaCatalogoRazonesSociales(Empresa: OleVariant; iCatalogo: Integer; 
                                          oDelta: OleVariant; out ErrorCount: Integer; 
                                          Parametros: OleVariant): OleVariant; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerCatalogosTimbradoDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {534433A8-72BB-42F6-AD6D-DFBD29388C62}
// *********************************************************************//
  IdmServerCatalogosTimbradoDisp = dispinterface
    ['{534433A8-72BB-42F6-AD6D-DFBD29388C62}']
    function GetPuestos(Empresa: OleVariant): OleVariant; dispid 1;
    function GetTurnos(Empresa: OleVariant): OleVariant; dispid 2;
    function GetCursos(Empresa: OleVariant): OleVariant; dispid 3;
    function GetCalendario(Empresa: OleVariant): OleVariant; dispid 4;
    function GetClasifi(Empresa: OleVariant): OleVariant; dispid 5;
    function GetConceptos(Empresa: OleVariant): OleVariant; dispid 6;
    function GetCondiciones(Empresa: OleVariant): OleVariant; dispid 7;
    function GetContratos(Empresa: OleVariant): OleVariant; dispid 8;
    function GetEventos(Empresa: OleVariant): OleVariant; dispid 9;
    function GetFestTurno(Empresa: OleVariant; const sTurnoFestivo: WideString; iVigencia: Integer): OleVariant; dispid 10;
    function GetFolios(Empresa: OleVariant): OleVariant; dispid 11;
    function GetHorarios(Empresa: OleVariant): OleVariant; dispid 12;
    function GetNomParam(Empresa: OleVariant): OleVariant; dispid 13;
    function GetRPatron(Empresa: OleVariant): OleVariant; dispid 14;
    function GetInvitadores(Empresa: OleVariant): OleVariant; dispid 15;
    function GetReglas(Empresa: OleVariant): OleVariant; dispid 16;
    function GetPrestaciones(Empresa: OleVariant): OleVariant; dispid 17;
    function GetPeriodos(Empresa: OleVariant; iYear: Integer; iTipo: Integer): OleVariant; dispid 18;
    function GetOtrasPer(Empresa: OleVariant): OleVariant; dispid 19;
    function GetMaestros(Empresa: OleVariant): OleVariant; dispid 20;
    function GetMatriz(Empresa: OleVariant; lCurso: WordBool; const sCodigo: WideString): OleVariant; dispid 21;
    function GrabaMatriz(Empresa: OleVariant; lCurso: WordBool; oDelta: OleVariant; 
                         out ErrorCount: Integer): OleVariant; dispid 22;
    function GrabaCatalogo(Empresa: OleVariant; iCatalogo: Integer; oDelta: OleVariant; 
                           out ErrorCount: Integer): OleVariant; dispid 23;
    function GetTools(Empresa: OleVariant): OleVariant; dispid 24;
    function GetPercepFijas(Empresa: OleVariant; const sCodigo: WideString): OleVariant; dispid 25;
    function GrabaPercepFijas(Empresa: OleVariant; oDelta: OleVariant; const sCodigo: WideString; 
                              out ErrorCount: Integer): OleVariant; dispid 26;
    function GetHerramientas(Empresa: OleVariant; const sCodigo: WideString): OleVariant; dispid 27;
    function GrabaHerramientas(Empresa: OleVariant; oDelta: OleVariant; const sCodigo: WideString; 
                               out ErrorCount: Integer): OleVariant; dispid 28;
    function GetSesiones(Empresa: OleVariant; const sCodigo: WideString; dFechaIni: TDateTime; 
                         dFechaFin: TDateTime; iFolio: Integer): OleVariant; dispid 29;
    function GrabaSesiones(Empresa: OleVariant; oDelta: OleVariant; oKCDelta: OleVariant; 
                           out iFolio: Integer; iOperacion: Integer; var ErrorCount: Integer; 
                           var ErrorKCCount: Integer; out Valor: OleVariant): OleVariant; dispid 30;
    function GetHisCursos(Empresa: OleVariant; iSesion: Integer): OleVariant; dispid 31;
    function GetAccReglas(Empresa: OleVariant): OleVariant; dispid 32;
    function GetAulas(Empresa: OleVariant): OleVariant; dispid 33;
    function GetPrerequisitosCurso(Empresa: OleVariant; const sPrerequisitosCurso: WideString): OleVariant; dispid 34;
    function GetCertificaciones(Empresa: OleVariant): OleVariant; dispid 35;
    function GetTiposPoliza(Empresa: OleVariant): OleVariant; dispid 301;
    function GetSeccionesPerfil(Empresa: OleVariant): OleVariant; dispid 302;
    function GetCamposPerfil(Empresa: OleVariant; const sSeccion: WideString): OleVariant; dispid 303;
    function GrabaSeccion(Empresa: OleVariant; oDelta: OleVariant; var ErrorCount: Integer; 
                          out iFolio: Integer): OleVariant; dispid 304;
    function GrabaCamposPerfil(Empresa: OleVariant; oDelta: OleVariant; var ErrorCount: Integer; 
                               out iFolio: Integer): OleVariant; dispid 305;
    function CambiaOrden(Empresa: OleVariant; iCatalogo: Integer; iActual: Integer; 
                         iNuevo: Integer; const sCodigo: WideString; const sClasifi: WideString): OleVariant; dispid 306;
    function GetPerfilesPuesto(Empresa: OleVariant): OleVariant; dispid 307;
    function GetPerfiles(Empresa: OleVariant; const sPuesto: WideString): OleVariant; dispid 308;
    function CopiarPerfil(Empresa: OleVariant; const sFuente: WideString; const sDestino: WideString): OleVariant; dispid 309;
    function GetDescPerfil(Empresa: OleVariant; const sPuesto: WideString): OleVariant; dispid 310;
    function GrabaDescPerfil(Empresa: OleVariant; oDelta: OleVariant; const sCampos: WideString; 
                             out ErrorCount: Integer; out iOrden: Integer): OleVariant; dispid 311;
    function GetTPeriodos(Empresa: OleVariant): OleVariant; dispid 313;
    function GetProvCap(Empresa: OleVariant): OleVariant; dispid 312;
    function GetValPlantilla(Empresa: OleVariant): OleVariant; dispid 314;
    function GetValFactores(Empresa: OleVariant; iPlantilla: Integer): OleVariant; dispid 315;
    function GetValSubFactores(Empresa: OleVariant; iPlantilla: Integer): OleVariant; dispid 316;
    function GetValNiveles(Empresa: OleVariant; iPlantilla: Integer; iOrden: Integer): OleVariant; dispid 317;
    function GrabaCamposVal(Empresa: OleVariant; iCatalogo: Integer; oDelta: OleVariant; 
                            var ErrorCount: Integer; out iFolio: Integer): OleVariant; dispid 318;
    function GetValuaciones(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 319;
    function AgregaValuacion(Empresa: OleVariant; oDelta: OleVariant; var ErrorCount: Integer; 
                             out iFolio: Integer; iPlantilla: Integer): OleVariant; dispid 320;
    function GrabaValuacion(Empresa: OleVariant; oDelta: OleVariant; oPDelta: OleVariant; 
                            out iFolio: Integer; var ErrorCount: Integer; var PErrorCount: Integer; 
                            out Valor: OleVariant): OleVariant; dispid 321;
    function GetValPuntos(Empresa: OleVariant; iValuacion: Integer): OleVariant; dispid 322;
    function CopiaValuacion(Empresa: OleVariant; Parametros: OleVariant; var ErrorCount: Integer): Integer; dispid 323;
    function GrabaMaestros(Empresa: OleVariant; oDelta: OleVariant; var ErrorCount: Integer): OleVariant; dispid 324;
    function GetRSocial(Empresa: OleVariant): OleVariant; dispid 325;
    function GetVistaCursos(Empresa: OleVariant): OleVariant; dispid 326;
    function GrabaMatrizCetifica(Empresa: OleVariant; lCertificacion: WordBool; Delta: OleVariant; 
                                 out ErrorCount: Integer): OleVariant; dispid 328;
    function GetMatrizCertifica(Empresa: OleVariant; const sCodigo: WideString; lMatriz: WordBool): OleVariant; dispid 327;
    function GetHistRev(Empresa: OleVariant; const sCodigo: WideString): OleVariant; dispid 329;
    function GrabaCursos(Empresa: OleVariant; oDelta: OleVariant; oDeltaH: OleVariant; 
                         out ErrorCount: Integer; out ErrorCountH: Integer; out oHResult: OleVariant): OleVariant; dispid 330;
    function GetReglasPrestamo(Empresa: OleVariant): OleVariant; dispid 331;
    function GetPrestamoPorRegla(Empresa: OleVariant; Codigo: Integer): OleVariant; dispid 332;
    function GetGruposConceptos(Empresa: OleVariant): OleVariant; dispid 333;
    function GetEstablecimientos(Empresa: OleVariant): OleVariant; dispid 334;
    function GrabaEstablecimientos(Empresa: OleVariant; Delta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 335;
    function GetTiposPension(Empresa: OleVariant): OleVariant; dispid 336;
    function GetSeguroGastosMedicos(Empresa: OleVariant; out Vigencias: OleVariant): OleVariant; dispid 337;
    function GrabaVigencias(Empresa: OleVariant; Delta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 338;
    function GetCosteoCriterios(Empresa: OleVariant): OleVariant; dispid 339;
    function GetCosteoGrupos(Empresa: OleVariant): OleVariant; dispid 340;
    function GetCosteoCriteriosGrupos(Empresa: OleVariant): OleVariant; dispid 341;
    function GetCosteoConceptosDisponibles(Empresa: OleVariant; const Grupo: WideString; 
                                           Criterio: Integer): OleVariant; dispid 342;
    function GetTablasAmortizacion(Empresa: OleVariant; out Costos: OleVariant): OleVariant; dispid 343;
    function GrabaCostosTablaAmort(Empresa: OleVariant; Delta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 344;
    function GetCompetencias(Empresa: OleVariant; out oRevisiones: OleVariant; 
                             out oNiveles: OleVariant; out oCursos: OleVariant): OleVariant; dispid 345;
    function GetCatPerfiles(Empresa: OleVariant; Tipo: OleVariant; out oRevisiones: OleVariant; 
                            out oPtoGpoComp: OleVariant): OleVariant; dispid 346;
    function GrabaCompetencias(Empresa: OleVariant; Delta: OleVariant; Niveles: OleVariant; 
                               Cursos: OleVariant; Revisiones: OleVariant; out ErrorCount: Integer; 
                               out NivelesRes: OleVariant; out CursosRes: OleVariant; 
                               out RevisionesRes: OleVariant; out ErrorNiv: Integer; 
                               out ErrorCur: Integer; out ErrorRev: Integer): OleVariant; dispid 347;
    function GrabaCPerfiles(Empresa: OleVariant; Delta: OleVariant; oRevisiones: OleVariant; 
                            out oRevisionesR: OleVariant; out ErrorCount: Integer; 
                            out ErrorCountR: Integer): OleVariant; dispid 348;
    function GetMatrizPerfilComps(Empresa: OleVariant): OleVariant; dispid 349;
    function GrabaMatrizPerfComp(Empresa: OleVariant; Delta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 350;
    function GetTiposSAT(Empresa: OleVariant): OleVariant; dispid 351;
    function GrabaCatalogoRazonesSociales(Empresa: OleVariant; iCatalogo: Integer; 
                                          oDelta: OleVariant; out ErrorCount: Integer; 
                                          Parametros: OleVariant): OleVariant; dispid 352;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer; 
                             out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                           Options: Integer; const CommandText: WideString; var Params: OleVariant; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerCatalogosTimbrado provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerCatalogosTimbrado exposed by              
// the CoClass dmServerCatalogosTimbrado. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerCatalogosTimbrado = class
    class function Create: IdmServerCatalogosTimbrado;
    class function CreateRemote(const MachineName: string): IdmServerCatalogosTimbrado;
  end;

implementation

uses ComObj;

class function CodmServerCatalogosTimbrado.Create: IdmServerCatalogosTimbrado;
begin
  Result := CreateComObject(CLASS_dmServerCatalogosTimbrado) as IdmServerCatalogosTimbrado;
end;

class function CodmServerCatalogosTimbrado.CreateRemote(const MachineName: string): IdmServerCatalogosTimbrado;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerCatalogosTimbrado) as IdmServerCatalogosTimbrado;
end;

end.
