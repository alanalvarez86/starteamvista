unit FPuestos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls;

type
  TPuestos = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    {procedure Agregar; override;
    procedure Borrar; override;}
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  Puestos: TPuestos;

implementation

{$R *.DFM}

uses
    dPlanCarrera,
    ZetaBuscador,
    ZetaCommonClasses;

{ TPuestos }
procedure TPuestos.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     //HelpContext := H00005_Catalogo_de_Medicinas;
end;

procedure TPuestos.Connect;
begin
     with dmPlanCarrera do
     begin
          cdsFamilias.Conectar;
          cdsNiveles.Conectar;
          cdsPuestos.Conectar;
          DataSource.DataSet := cdsPuestos;
     end;
end;

procedure TPuestos.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Puestos', 'PUESTO', 'PU_CODIGO', dmPlanCarrera.cdsPuestos );
end;

procedure TPuestos.ImprimirForma;
begin
  inherited;

end;

procedure TPuestos.Modificar;
begin
     dmPlanCarrera.cdsPuestos.Modificar;
end;

function TPuestos.PuedeAgregar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se Pueden Agregar Registros a este Cat�logo';
     Result := False;
end;

function TPuestos.PuedeBorrar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se Pueden Borrar Registros a este Cat�logo';
     Result := False;
end;

procedure TPuestos.Refresh;
begin
     dmPlanCarrera.cdsPuestos.Refrescar;
end;

end.
