inherited EditCompetencias: TEditCompetencias
  Left = 250
  Top = 203
  Caption = 'Competencias '
  ClientHeight = 400
  ClientWidth = 563
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 364
    Width = 563
    inherited OK: TBitBtn
      Left = 405
    end
    inherited Cancelar: TBitBtn
      Left = 484
    end
  end
  inherited PanelSuperior: TPanel
    Width = 563
    inherited DBNavigator: TDBNavigator
      Visible = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 563
    inherited ValorActivo2: TPanel
      Width = 237
    end
  end
  inherited Panel1: TPanel
    Width = 563
    object Label1: TLabel
      Left = 111
      Top = 8
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'digo:'
    end
    object Label2: TLabel
      Left = 88
      Top = 30
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descripci'#243'n:'
    end
    object CM_CODIGO: TZetaDBEdit
      Left = 151
      Top = 4
      Width = 90
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
      ConfirmEdit = True
      DataField = 'CM_CODIGO'
      DataSource = DataSource
    end
    object CM_DESCRIP: TDBEdit
      Left = 151
      Top = 26
      Width = 386
      Height = 21
      DataField = 'CM_DESCRIP'
      DataSource = DataSource
      TabOrder = 1
    end
  end
  inherited PageControl: TPageControl
    Width = 563
    Height = 263
    inherited Datos: TTabSheet
      object Label3: TLabel
        Left = 110
        Top = 28
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ingl'#233's:'
        FocusControl = CM_INGLES
      end
      object Label6: TLabel
        Left = 101
        Top = 50
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'N'#250'mero:'
      end
      object Label7: TLabel
        Left = 15
        Top = 91
        Width = 126
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descripci'#243'n de Funciones:'
      end
      object Label4: TLabel
        Left = 117
        Top = 5
        Width = 24
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo:'
      end
      object Label5: TLabel
        Left = 111
        Top = 72
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Texto:'
      end
      object CM_INGLES: TDBEdit
        Left = 146
        Top = 24
        Width = 275
        Height = 21
        DataField = 'CM_INGLES'
        DataSource = DataSource
        TabOrder = 1
      end
      object CM_DETALLE: TDBMemo
        Left = 146
        Top = 90
        Width = 399
        Height = 142
        DataField = 'CM_DETALLE'
        DataSource = DataSource
        ScrollBars = ssVertical
        TabOrder = 4
      end
      object TC_CODIGO: TZetaDBKeyLookup
        Left = 146
        Top = 2
        Width = 275
        Height = 21
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        DataField = 'TC_CODIGO'
        DataSource = DataSource
      end
      object CM_TEXTO: TDBEdit
        Left = 146
        Top = 68
        Width = 275
        Height = 21
        DataField = 'CM_TEXTO'
        DataSource = DataSource
        TabOrder = 3
      end
      object CM_NUMERO: TZetaDBNumero
        Left = 146
        Top = 46
        Width = 110
        Height = 21
        Mascara = mnNumeroGlobal
        TabOrder = 2
        Text = '0.00'
        UseEnterKey = True
        DataField = 'CM_NUMERO'
        DataSource = DataSource
      end
    end
    inherited Tabla: TTabSheet
      Caption = 'Acciones Recomendadas'
      object SplitterAcciones: TSplitter [0]
        Left = 323
        Top = 29
        Height = 206
      end
      inherited Panel2: TPanel [1]
        Width = 555
      end
      inherited GridRenglones: TZetaDBGrid [2]
        Left = 30
        Width = 293
        Height = 206
        Align = alLeft
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect]
        Columns = <
          item
            Expanded = False
            FieldName = 'CM_ORDEN'
            Title.Caption = '#'
            Width = -1
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'AN_CODIGO'
            ReadOnly = True
            Title.Caption = 'C'#243'digo'
            Width = 73
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AN_NOMBRE'
            ReadOnly = True
            Title.Caption = 'Nombre'
            Width = 180
            Visible = True
          end>
      end
      object Panel4: TPanel
        Left = 0
        Top = 29
        Width = 30
        Height = 206
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 2
        object Subir: TZetaSmartListsButton
          Left = 3
          Top = 72
          Width = 26
          Height = 25
          Hint = 'Incrementar Prioridad de la Acci'#243'n'
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000333
            3333333333090333333333333309033333333333330903333333333333090333
            3333333333090333333333300009000033333330999999903333333309999903
            3333333309999903333333333099903333333333309990333333333333090333
            3333333333090333333333333330333333333333333033333333}
          ParentShowHint = False
          ShowHint = True
          OnClick = SubirClick
          Tipo = bsSubir
        end
        object Bajar: TZetaSmartListsButton
          Left = 3
          Top = 96
          Width = 26
          Height = 25
          Hint = 'Decrementar Prioridad de la Acci'#243'n'
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333303333
            3333333333303333333333333309033333333333330903333333333330999033
            3333333330999033333333330999990333333333099999033333333099999990
            3333333000090000333333333309033333333333330903333333333333090333
            3333333333090333333333333309033333333333330003333333}
          ParentShowHint = False
          ShowHint = True
          OnClick = BajarClick
          Tipo = bsBajar
        end
      end
      object GroupBox1: TGroupBox
        Left = 326
        Top = 29
        Width = 229
        Height = 206
        Align = alClient
        Caption = ' Descripci'#243'n '
        TabOrder = 3
        object CM_OBSERVA: TDBMemo
          Left = 2
          Top = 15
          Width = 225
          Height = 189
          Align = alClient
          DataField = 'CM_OBSERVA'
          DataSource = dsRenglon
          ScrollBars = ssVertical
          TabOrder = 0
        end
      end
    end
    object Criterios: TTabSheet
      Caption = 'Criterios de Evaluaci'#243'n'
      ImageIndex = 2
      object SplitterCriterios: TSplitter
        Left = 305
        Top = 29
        Height = 206
      end
      object GridCriterios: TZetaDBGrid
        Left = 0
        Top = 29
        Width = 305
        Height = 206
        Align = alLeft
        DataSource = dsCalifica
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnTitleClick = GridRenglonesTitleClick
        Columns = <
          item
            Expanded = False
            FieldName = 'CA_CODIGO'
            ReadOnly = True
            Title.Caption = 'C'#243'digo'
            Width = 82
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CA_DESCRIP'
            ReadOnly = True
            Title.Caption = 'Nombre'
            Width = 180
            Visible = True
          end>
      end
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 555
        Height = 29
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object bbAgregarCriterios: TBitBtn
          Left = 8
          Top = 1
          Width = 129
          Height = 25
          Caption = 'Agregar Rengl'#243'n'
          TabOrder = 0
          OnClick = bbAgregarCriteriosClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
            333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
            0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
            07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
            07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
            0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
            33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
            B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
            3BB33773333773333773B333333B3333333B7333333733333337}
          NumGlyphs = 2
        end
        object bbBorrarCriterios: TBitBtn
          Left = 148
          Top = 1
          Width = 129
          Height = 25
          Caption = 'Borrar Rengl'#243'n'
          TabOrder = 1
          OnClick = bbBorrarCriteriosClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
            305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
            005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
            B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
            B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
            B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
            B0557777FF577777F7F500000E055550805577777F7555575755500000555555
            05555777775555557F5555000555555505555577755555557555}
          NumGlyphs = 2
        end
        object bbModificarCriterios: TBitBtn
          Left = 288
          Top = 1
          Width = 129
          Height = 25
          Caption = 'Modificar Rengl'#243'n'
          TabOrder = 2
          OnClick = bbModificarCriteriosClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
            000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
            00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
            F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
            0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
            FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
            FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
            0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
            00333377737FFFFF773333303300000003333337337777777333}
          NumGlyphs = 2
        end
      end
      object GroupBox2: TGroupBox
        Left = 308
        Top = 29
        Width = 247
        Height = 206
        Align = alClient
        Caption = ' Descripci'#243'n '
        TabOrder = 2
        object MC_DESCRIP: TDBMemo
          Left = 2
          Top = 15
          Width = 243
          Height = 189
          Align = alClient
          DataField = 'MC_DESCRIP'
          DataSource = dsCalifica
          MaxLength = 255
          ScrollBars = ssVertical
          TabOrder = 0
        end
      end
    end
  end
  inherited dsRenglon: TDataSource
    OnDataChange = dsRenglonDataChange
  end
  object dsCalifica: TDataSource
    OnDataChange = dsCalificaDataChange
    Left = 431
    Top = 1
  end
end
