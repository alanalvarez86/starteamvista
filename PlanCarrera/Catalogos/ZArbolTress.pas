unit ZArbolTress;

interface

uses ComCtrls, Dialogs, SysUtils, DB, Controls,
     ZArbolTools, ZNavBarTools, ZetaCommonLists, cxTreeView;

function  GetNodoInfo( const iNodo: Integer ): TNodoInfo;
procedure CreaArbolDefault( oArbolMgr: TArbolMgr );
procedure CreaNavBarDefault ( oNavBarMgr: TNavBarMgr ; Arbolitos: array of TcxTreeView );
function GetGrupoInfo( const iNodo: Integer ): TGrupoInfo;
procedure CreaArbolitoDefault( oArbolMgr: TArbolMgr ; iAccesoGlobal: Integer );

implementation

uses ZetaDespConsulta,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZAccesosMgr,
     ZAccesosTress,
     ZGlobalTress,
     FCatTCompetencias,
     DGlobal,
     DSistema,
     DCliente;

const
     K_PLAN_CARRERA         = 800;
     K_CAT_TCOMPETEN        = 801;
     K_CAT_CALIFICA         = 802;
     K_CAT_COMPETEN         = 803;
     K_CAT_ACCION           = 804;
     K_CAT_PUESTO           = 805;
     K_CAT_FAMILIA          = 806;
     K_CAT_NIVEL            = 807;
     K_CAT_DIMENSIO         = 808;
     K_CONSULTAS            = 809;
     K_REPORTES             = 810;
     K_SISTEMA              = 811;
     K_GLOBAL_CARRERA       = 812;
     K_PLAN_CARRERA_ADMIN   = 813;


function Forma( const sCaption: String; const eTipo: eFormaConsulta ): TNodoInfo;
begin
     Result.EsForma   := TRUE;
     Result.Caption   := sCaption;
     Result.TipoForma := eTipo;
end;

function Folder( const sCaption: String; const iDerechos: Integer ): TNodoInfo;
begin
     Result.EsForma   := FALSE;
     Result.Caption   := sCaption;
     Result.IndexDerechos  := iDerechos;
end;

function Grupo( const sCaption: String; const iDerechos: Integer ): TGrupoInfo;
begin
     Result.Caption := sCaption;
     Result.IndexDerechos := iDerechos;
end;

function GetNodoInfo( const iNodo: Integer ): TNodoInfo;
begin
          case iNodo of
               //K_PLAN_CARRERA_ADMIN : Result := Folder( 'Plan Carrera Admin.', D_CARRERA );
               K_PLAN_CARRERA    : Result := Folder( 'Catálogos', D_CARRERA_CATALOGOS );
                 K_CAT_COMPETEN  : Result := Forma( 'Competencias', efcCatCompeten );
                 K_CAT_ACCION    : Result := Forma( 'Acciones', efcCatAccion );
                 K_CAT_PUESTO    : Result := Forma( 'Puestos', efcCatPuesto );
                 K_CAT_CALIFICA  : Result := Forma( 'Tabla de Calificaciones', efcCatCalifica );
                 K_CAT_TCOMPETEN : Result := Forma( 'Tipos de Competencia', efcCatTCompeten );
                 K_CAT_FAMILIA   : Result := Forma( 'Familias', efcCatFamilia );
                 K_CAT_NIVEL     : Result := Forma( 'Niveles', efcCatNivel );
                 K_CAT_DIMENSIO  : Result := Forma( 'Dimensiones', efcCatDimensio );
               K_CONSULTAS     : Result := Folder( 'Consultas', D_CARRERA_CONSULTAS );
                 K_REPORTES      : Result := Forma( 'Reportes', efcReportes );
               K_SISTEMA       : Result := Folder( 'Sistema', D_CARRERA_SISTEMA );
                 K_GLOBAL_CARRERA: Result := Forma( 'Globales de Empresa', efcSistGlobales );
            else
                Result := Folder( VACIO, 0 );
            end;
end;

function GetGrupoInfo( const iNodo: Integer ): TGrupoInfo;
begin
     case iNodo of
          //D_CARRERA             : Result := Grupo( 'Plan Carrera Admin.', iNodo );
          D_CARRERA_CATALOGOS   : Result := Grupo( 'Catálogos', iNodo );
          D_CARRERA_CONSULTAS   : Result := Grupo( 'Consultas', iNodo );
          D_CARRERA_SISTEMA     : Result := Grupo( 'Sistema', iNodo );
     else
          Result := Grupo( '', 0 );
     end;
end;

procedure CreaArbolDefault( oArbolMgr: TArbolMgr );
   procedure NodoNivel( const iNivel, iNodo: Integer );
   begin
        oArbolMgr.NodoNivel( iNivel, GetNodoInfo( iNodo ), iNodo );
   end;

begin
///     NodoNivel( 0, K_PLAN_CARRERA_ADMIN );
       NodoNivel( 0, K_PLAN_CARRERA );
          NodoNivel( 1, K_CAT_COMPETEN );
          NodoNivel( 1, K_CAT_ACCION );
          NodoNivel( 1, K_CAT_PUESTO );
          NodoNivel( 1, K_CAT_CALIFICA );
          NodoNivel( 1, K_CAT_TCOMPETEN );
          NodoNivel( 1, K_CAT_FAMILIA );
          NodoNivel( 1, K_CAT_NIVEL );
          NodoNivel( 1, K_CAT_DIMENSIO );
       NodoNivel( 0, K_CONSULTAS );
          NodoNivel( 1, K_REPORTES );
       NodoNivel( 0, K_SISTEMA );
          NodoNivel( 1, K_GLOBAL_CARRERA );
end;

{procedure CreaArbolUsuario( oArbolMgr: TArbolMgr; const sFileName: String );
begin
     with oArbolMgr do
     begin
          Configuracion := TRUE;
          oArbolMgr.NumDefault := K_PLAN_CARRERA;
          CreaArbolDefault( oArbolMgr );
          Arbol.FullExpand;
     end;
end;  }

procedure CreaArbolitoDefault( oArbolMgr : TArbolMgr; iAccesoGlobal: Integer );
function NodoNivel( const iNivel, iNodo: Integer ): Boolean;
begin
     Result := oArbolMgr.NodoNivel( iNivel, GetNodoInfo( iNodo ), iNodo); //Siempre mandar 0 en iNodo
end;
begin
      case iAccesoGlobal of
            K_PLAN_CARRERA		:
            begin
                 NodoNivel( 0, K_CAT_COMPETEN );
                 NodoNivel( 0, K_CAT_ACCION );
                 NodoNivel( 0, K_CAT_PUESTO );
                 NodoNivel( 0, K_CAT_CALIFICA );
                 NodoNivel( 0, K_CAT_TCOMPETEN );
                 NodoNivel( 0, K_CAT_FAMILIA );
                 NodoNivel( 0, K_CAT_NIVEL );
                 NodoNivel( 0, K_CAT_DIMENSIO );
            end;
           K_CONSULTAS        :
           begin
                 NodoNivel( 0, K_REPORTES  );
            end;
            K_SISTEMA               :
            begin
                 NodoNivel( 0, K_GLOBAL_CARRERA );
            end;
      end;
end;

procedure CreaNavBarDefault ( oNavBarMgr: TNavBarMgr; Arbolitos: array of TcxTreeView);
var
   NodoRaiz: TGrupoInfo;
function NodoNivel( iNodo, iGroupIndex: Integer): Boolean;
begin
     Result := oNavBarMgr.NodoNivel( GetGrupoInfo( iNodo ), iGroupIndex, iNodo );
end;
procedure CreaArbolito( const K_GLOBAL:Integer; Arbolito: TcxTreeView);
var
   oArbolMgr: TArbolMgr;
begin
     with Arbolito do
     begin
          Parent := oNavBarMgr.NavBar.Groups[oNavBarMgr.UltimoGrupo.Index].Control;
          Align := alClient;
     end;
     oArbolMgr := TArbolMgr.Create( Arbolito );
     with oArbolMgr do
     begin
          try
             with Arbolito.Items do
             begin
                  BeginUpdate;
                  Clear;
                  try
                     with oArbolMgr do
                     begin
                           CreaArbolitoDefault( oArbolMgr, K_GLOBAL  );
                           Arbol_DevEx.FullCollapse;
                     end;
                  finally
                     EndUpdate;
                  end;
             end;
          finally
             Free;
          end;
     end;
end;
begin

     if( NodoNivel( D_CARRERA_CATALOGOS, 0 )) then
                CreaArbolito ( K_PLAN_CARRERA, Arbolitos[0]);
     // ******** Consultas ***********
     if( NodoNivel( D_CARRERA_CONSULTAS, 1 )) then
                CreaArbolito ( K_CONSULTAS, Arbolitos[1]);
     // ******* Catálogos *************
     if( NodoNivel( D_CARRERA_SISTEMA, 2 ) ) then
                CreaArbolito ( K_SISTEMA, Arbolitos[2]);

     if ( oNavBarMgr.NavBar.Groups.Count = 0 ) then
     begin
        with NodoRaiz do
          begin
            Caption := 'No tiene derechos';
            IndexDerechos := K_SIN_RESTRICCION;
            oNavBarMgr.NodoNivel(NodoRaiz,0, K_ARBOL_NODO_RAIZ)
        end;
     end
end;
end.
