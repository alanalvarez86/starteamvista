unit FSistEditEmpresas_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FSistBaseEditEmpresas, Db, StdCtrls, ZetaEdit, Mask, DBCtrls, ExtCtrls,
  Buttons, ZetaSmartLists;

type
  TSistEditEmpresas_DevEx = class(TSistBaseEditEmpresas)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SistEditEmpresas_DevEx: TSistEditEmpresas_DevEx;

implementation

{$R *.DFM}

end.
