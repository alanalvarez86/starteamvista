unit FFamilias_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls,
  ZBaseConsulta, Vcl.StdCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList,
  Vcl.ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TFamilias_DevEx = class(TBaseGridLectura_DevEx)
    ZetaDBGrid1: TZetaDBGrid;
    FP_CODIGO: TcxGridDBColumn;
    FP_DESCRIP: TcxGridDBColumn;
    FP_INGLES: TcxGridDBColumn;
    FP_NUMERO: TcxGridDBColumn;
    FP_TEXTO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  Familias_DevEx: TFamilias_DevEx;

implementation

{$R *.DFM}

uses
    dPlanCarrera,
    ZetaBuscador_DevEx,
    ZetaCommonClasses;

{ TFamilias }
procedure TFamilias_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H_Catalogo_Familias;
end;

procedure TFamilias_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  CreaColumaSumatoria(ZetaDbGridDBTableView.Columns[0], 0 ,'' , SkCount );
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := False;
  ApplyMinWidth;
  ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TFamilias_DevEx.Agregar;
begin
     dmPlanCarrera.cdsFamilias.Agregar;
     DoBestFit;
end;

procedure TFamilias_DevEx.Borrar;
begin
     dmPlanCarrera.cdsFamilias.Borrar;
     DoBestFit;
end;

procedure TFamilias_DevEx.Connect;
begin
     with dmPlanCarrera do
     begin
          cdsFamilias.Conectar;
          DataSource.DataSet := cdsFamilias;
     end;
end;

procedure TFamilias_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Familias', 'FAM_PTO', 'FP_CODIGO', dmPlanCarrera.cdsFamilias );
end;

procedure TFamilias_DevEx.ImprimirForma;
begin
  inherited;

end;

procedure TFamilias_DevEx.Modificar;
begin
     dmPlanCarrera.cdsFamilias.Modificar;
     DoBestFit;
end;

function TFamilias_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := True;
end;

function TFamilias_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := True;
end;

procedure TFamilias_DevEx.Refresh;
begin
     dmPlanCarrera.cdsFamilias.Refrescar;
end;

end.
