unit FTressShell;

interface

{$INCLUDE DEFINES.INC}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, ImgList, ActnList, ExtCtrls, ComCtrls, StdCtrls,
  Buttons, ZetaSmartLists,
  ZetaClientDataSet,
  ZetaCommonLists,
  ZetaTipoEntidad, ZBaseShell,
  cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxStatusBar, dxRibbonStatusBar, dxRibbonSkins,
  dxRibbonCustomizationForm, System.Actions, cxStyles, cxClasses, dxSkinsForm,
  dxRibbon, dxBarBuiltInMenu, cxLocalization, dxBar, dxNavBar, cxButtons, cxPC,
  ZBasicoNavBarShell;


type
  TTressShell = class(TBasicoNavBarShell)
    TabArchivo_btnConfigurarEmpresa: TdxBarLargeButton;
    TabArchivo_btnCatalogoUsuarios: TdxBarLargeButton;
    AConsultas: TdxBar;
    TabArchivo_btnExplorardorReportes: TdxBarLargeButton;
    TabArchivo_btnBitacora: TdxBarLargeButton;
    TabArchivo_btnConsultaSQL: TdxBarLargeButton;
    _A_ConfigurarEmpresa: TAction;
    _A_ExploradorReportes: TAction;
    _A_SQL_DevEx: TAction;
    _A_Bitacora_DevEx: TAction;
    _A_ListaProcesos: TAction;
    TabArchivo_btnListaProcesos: TdxBarLargeButton;
    _A_CatalogoUsuarios: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ClientAreaPG_DevExTcxPageControlPropertiesTcxPCCustomButtonsButtons0Click(
      Sender: TObject);
    procedure _A_ConfigurarEmpresaExecute(Sender: TObject);
    procedure _A_ExploradorReportesExecute(Sender: TObject);
    procedure _H_GlosarioExecute(Sender: TObject);
  protected
    FHelpGlosario: String;
    procedure DoOpenAll; override;
    procedure DoCloseAll; override;
    Procedure HabilitaControles; override;
    procedure CargaTraducciones; override;
    procedure NotifyDataChange(const Entidades: Array of TipoEntidad; const Estado: TipoEstado);override;
    function ProcesaDialogo(oLookUp: TZetaLookupDataset; const eEntidad: TipoEntidad; const sFilter: String; var sKey,
      sDescription: String; var lEncontrado: Boolean): Boolean;override;
  private
    { Private declarations }
    procedure RevisaDerechos;
     procedure AsignacionTabOrder_DevEx;
  public
    { Public declarations }
    function GetLookUpDataSet(const eEntidad: TipoEntidad): TZetaLookUpDataSet;override;
    procedure SetDataChange(const Entidades: Array of TipoEntidad); override;
    procedure CargaVista; override;
    procedure LLenaOpcionesGrupos;
  end;

    const
     K_PANEL_PLANCARRERA = 1;

var
  TressShell: TTressShell;

implementation

uses DCliente, DGlobal, DSistema, DCatalogos, DReportes, DDiccionario,
     DPlanCarrera, ZetaDialogo, ZAccesosMgr, ZAccesosTress,  
     ZetacommonClasses, ZetaClientTools,
     ZetaBuscaEmpleado, ZGlobalTress, ZetaDespConsulta, ZetaCommonTools;

{$R *.DFM}

procedure TTressShell.DoCloseAll;
begin
     {CierraFormaTodas;
     inherited DoCloseAll;}
     CierraFormaTodas;
     if EmpresaAbierta then
     begin
          dmCliente.CierraEmpresa;
          CierraDatasets( dmDiccionario );
          CierraDatasets( dmReportes );
          CierraDatasets( dmPlanCarrera );
          CierraDatasets( dmSistema );
          CierraDatasets( dmCatalogos );
     end;
     inherited DoCloseAll;
end;

procedure TTressShell.DoOpenAll;
const
     K_TAMANO_ARBOLITOS = 3;
begin
     try
        inherited DoOpenAll;
        Global.Conectar;
        {***US 11757: Proteccion HTTP SkinController = True
        Evita que el backgound de algunos componentes de la aplicacion
        sea de color blanco
        ***}
        //DevEx_SkinController.NativeStyle := TRUE; //@gbeltran: se comenta el skin controller ya que no contrasta con nueva imagen
        SetArbolitosLength( K_TAMANO_ARBOLITOS );
        CreaNavBar;
     except
        on Error : Exception do
        begin
             ZetaDialogo.zError( 'Error al Conectar', 'Empresa: ' + dmCliente.Compania + CR_LF + Error.Message, 0 );
             DoCloseAll;
        end;
     end;
     LLenaOpcionesGrupos;
end;

Procedure TTressShell.LLenaOpcionesGrupos;
var
I:integer;
begin
  if(dmCliente.EmpresaAbierta) then
  begin
        for I:=0 to DevEx_NavBar.Groups.Count-1 do
        begin
              if (DevEx_NavBar.Groups[I].Caption = 'Cat�logos')  then
              begin
                    DevEx_NavBar.Groups[I].LargeImageIndex:=0;
                    DevEx_NavBar.Groups[I].SmallImageIndex:=0;
              end;
              if (DevEx_NavBar.Groups[I].Caption = 'Consultas')  then
              begin
                    DevEx_NavBar.Groups[I].LargeImageIndex:=1;
                    DevEx_NavBar.Groups[I].SmallImageIndex:=1;
              end;
              if (DevEx_NavBar.Groups[I].Caption = 'Sistema')  then
              begin
                    DevEx_NavBar.Groups[I].LargeImageIndex:=2;
                    DevEx_NavBar.Groups[I].SmallImageIndex:=2;
              end;
              if (I = 0) then
              begin
                    if(DevEx_NavBar.Groups.Count = 1) then
                    begin
                          // @DChavez : Cuando no tiene derecho se asigna los iconos que corresponden al navbar por el index fijo en la lista
                          if( DevEx_NavBar.Groups[0].Caption = 'No tiene derechos' )   then
                          begin
                                 DevEx_NavBar.Groups[0].LargeImageIndex := 3;
                                 DevEx_NavBar.Groups[0].SmallImageIndex := 3;
                          end
                          else

                          DevEx_NavBar.Groups[0].LargeImageIndex:=0;
                          DevEx_NavBar.Groups[0].SmallImageIndex:=0;
                    end
                    else
                    begin
                          if (DevEx_NavBar.Groups[0].Caption = 'Cat�logos')  then
                          begin
                                DevEx_NavBar.Groups[0].LargeImageIndex:=0;
                                DevEx_NavBar.Groups[0].SmallImageIndex:=0;
                          end;
                    end;
              end;
              DevEx_NavBar.Groups[I].UseSmallImages:=false;
        end;
  end
  else
  begin
        if(DevEx_NavBar.Groups.Count >0) then
        begin
              DevEx_NavBar.Groups[0].UseSmallImages:=false;
              DevEx_NavBar.Groups[0].LargeImageIndex:=0;
              DevEx_NavBar.Groups[0].SmallImageIndex:=0;
        end;
  end;
end;

procedure TTressShell.FormCreate(Sender: TObject);
begin
     Global := TdmGlobal.Create;
     dmCliente := TdmCliente.Create( Self );
     dmSistema := TdmSistema.Create( Self );
     dmCatalogos := TdmCatalogos.Create( Self );
     dmReportes := TdmReportes.Create( Self );
     dmDiccionario := TdmDiccionario.Create( Self );
     dmPlanCarrera := TdmPlanCarrera.Create( Self );
     FHelpGlosario := 'Tress.chm';
     inherited;
     ZAccesosMgr.SetValidacion( TRUE );
     HelpContext := H00001_Pantalla_principal;
end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
     inherited;
     dmPlanCarrera.Free;
     dmDiccionario.Free;
     dmReportes.Free;
     dmCatalogos.Free;
     dmSistema.Free;
     dmCliente.Free;
     Global.Free;
end;

procedure TTressShell.FormShow(Sender: TObject);
begin
  inherited;
  CargaVista;
  CargaTraducciones;
end;

procedure TTressShell.AsignacionTabOrder_DevEx;
begin
     DevEx_ShellRibbon.TabOrder:= 1;
     PanelNavBar.TabOrder := 2;
     DevEx_BandaValActivos.TabOrder :=3;
     PanelConsulta.TabOrder :=4;
end;

procedure TTressShell.CargaTraducciones;
begin
    DevEx_cxLocalizer.Active := True;
    DevEx_cxLocalizer.Locale := 2058;  // Carinal para Espanol (Mexico)
end;


procedure TTressShell.CargaVista;
begin
     inherited;
     //Mostar Banda Valores activos
     DevEx_BandaValActivos.Visible := False;
     //Asignacion de TabOrders para el shell
     AsignacionTabOrder_DevEx;
     //Para avisar a los controles que hubo movimientos
     Application.ProcessMessages;
end;

procedure TTressShell.ClientAreaPG_DevExTcxPageControlPropertiesTcxPCCustomButtonsButtons0Click(
  Sender: TObject);
begin
  inherited;
  _V_Cerrar.Execute;
end;

procedure TTressShell.HabilitaControles;
begin
  inherited HabilitaControles;
     if not EmpresaAbierta then
     begin
          StatusBarMsg( '', K_PANEL_PLANCARRERA );
     end;

     //aqui van las datos del ribbon

end;

procedure TTressShell.SetDataChange(const Entidades: Array of TipoEntidad);
begin
     { Cambios a otros datasets }
     inherited SetDataChange( Entidades );
     //NotifyDataChange( Entidades, stNinguno );
end;

procedure TTressShell._A_ConfigurarEmpresaExecute(Sender: TObject);
begin
  inherited;
  AbreFormaConsulta( efcSistGlobales );
end;

procedure TTressShell._A_ExploradorReportesExecute(Sender: TObject);
begin
  inherited;
  AbreFormaConsulta( efcReportes );
end;

procedure TTressShell._H_GlosarioExecute(Sender: TObject);
var
   sHelpFile: String;
begin
     if strLleno( FHelpGlosario ) then  // En el create poner FHelpGlosario indicando el Archivo de Ayuda y Secci�n al cual apuntar
        with Application do
        begin
             sHelpFile := HelpFile;
             try
                HelpFile := FHelpGlosario; //'Tress.hlp>Main';
                HelpContext ( HContenido_Glosario.HelpContext );
             finally
                HelpFile := sHelpFile;
             end;
        end;

end;

procedure TTressShell.NotifyDataChange(const Entidades: Array of TipoEntidad; const Estado: TipoEstado);
begin
     dmPlanCarrera.NotifyDataChange( Entidades, Estado );
     inherited NotifyDataChange( Entidades, Estado );
end;

function TTressShell.ProcesaDialogo(oLookUp: TZetaLookupDataset; const eEntidad: TipoEntidad;
         const sFilter: String; var sKey, sDescription: String; var lEncontrado: Boolean ): Boolean;
begin
     Result := ( eEntidad = enEmpleado );
     if Result then
        lEncontrado := ZetaBuscaEmpleado.BuscaEmpleadoDialogo( VACIO, sKey, sDescription )
end;

function TTressShell.GetLookUpDataSet( const eEntidad: TipoEntidad ): TZetaLookUpDataSet;
begin
     case eEntidad of
          enEmpleado: Result := dmCliente.cdsEmpleadoLookUp;
          enTCompetencia: Result := dmPlanCarrera.cdsTCompetencia;
          enCalifica: Result := dmPlanCarrera.cdsCalificaciones;
          enDimensiones: Result := dmPlanCarrera.cdsDimensiones;
          enAcciones: Result := dmPlanCarrera.cdsAcciones;
          enFamiliasPuesto: Result := dmPlanCarrera.cdsFamilias;
          enNivelesPuesto : Result := dmPlanCarrera.cdsNiveles;
          enCompetencias: Result := dmPlanCarrera.cdsCompetencia;
          enPuesto: Result := dmPlanCarrera.cdsPuestos;
          enUsuarios: Result := dmSistema.cdsUsuarios;
          enCurso: Result := dmCatalogos.cdsCursos
          else
              Result := NIL;
	end {case}
end;

procedure TTressShell.RevisaDerechos;
begin
     _A_ExploradorReportes.Enabled := CheckDerecho(D_CONS_REPORTES , K_DERECHO_CONSULTA);
end;


end.
