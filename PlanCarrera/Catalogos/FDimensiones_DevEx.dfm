inherited Dimensiones_DevEx: TDimensiones_DevEx
  Left = 246
  Top = 276
  Caption = 'Dimensiones'
  ClientHeight = 212
  ClientWidth = 626
  ExplicitWidth = 626
  ExplicitHeight = 212
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 626
    ExplicitWidth = 626
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 367
      ExplicitWidth = 367
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 361
        ExplicitLeft = 3
      end
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 626
    Height = 193
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'DM_CODIGO'
        Title.Caption = 'C'#243'digo'
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DM_DESCRIP'
        Title.Caption = 'Descripci'#243'n'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DM_INGLES'
        Title.Caption = 'Ingl'#233's'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DM_NUMERO'
        Title.Caption = 'N'#250'mero'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DM_TEXTO'
        Title.Caption = 'Texto'
        Width = 200
        Visible = True
      end>
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 626
    Height = 193
    ExplicitWidth = 626
    ExplicitHeight = 193
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object DM_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'DM_CODIGO'
      end
      object DM_DESCRIP: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'DM_DESCRIP'
      end
      object DM_INGLES: TcxGridDBColumn
        Caption = 'Ingl'#233's'
        DataBinding.FieldName = 'DM_INGLES'
      end
      object DM_NUMERO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'DM_NUMERO'
      end
      object DM_TEXTO: TcxGridDBColumn
        Caption = 'Texto'
        DataBinding.FieldName = 'DM_TEXTO'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
