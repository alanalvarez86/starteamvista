unit FEditFamilias_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, Grids, DBGrids, ComCtrls, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask,
     ZBaseEdicionRenglon_DevEx,
     ZetaDBGrid,
     ZetaEdit,
     ZetaNumero,
     ZetaSmartLists, ZBaseEdicionRenglon, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, cxControls, dxBarBuiltInMenu, dxBarExtItems,
  dxBar, cxClasses, Vcl.ImgList, cxPC, cxNavigator, cxDBNavigator, cxButtons;

type
  TEditFamilias_DevEx = class(TBaseEdicionRenglon_DevEx)
    Label1: TLabel;
    Label2: TLabel;
    FP_DESCRIP: TDBEdit;
    FP_CODIGO: TZetaDBEdit;
    Label3: TLabel;
    Label5: TLabel;
    Label4: TLabel;
    FP_TEXTO: TDBEdit;
    FP_NUMERO: TZetaDBNumero;
    FP_INGLES: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure BBBorrarClick(Sender: TObject);
    procedure BBModificarClick(Sender: TObject);
    procedure dsRenglonDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect;override;
    procedure DoLookup; override;
    procedure HabilitaControles; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure DoCancelChanges; override;
  public
    { Public declarations }
  end;

var
  EditFamilias_DevEx: TEditFamilias_DevEx;

implementation

{$R *.DFM}

uses FSelCompetenciasFamilia_DevEx,
     DPlanCarrera,
     ZetaBuscador,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaDialogo,
     ZBaseDlgModal_DevEx,
     ZAccesosTress;

{ TEditFamilias }

const
     K_COLUMNA_COMPETENCIA_CODIGO = 0;
     K_COLUMNA_COMPETENCIA_DESCRIPCION = 1;
     K_COLUMNA_COMPETENCIA_OBSERVACIONES = 2;

procedure TEditFamilias_DevEx.FormCreate(Sender: TObject);
var
   i: Integer;
begin
     inherited;
     IndexDerechos := D_CARRERA_CAT_FAMILIA;
     FirstControl := FP_CODIGO;
     with GridRenglones do
     begin
          for i := K_COLUMNA_COMPETENCIA_CODIGO to K_COLUMNA_COMPETENCIA_DESCRIPCION do
          begin
               with Columns[ i ] do
               begin
                    Color := clInfoBk;
                    Font.Color := clInfoText;
                    ReadOnly := True;
               end;
          end;
     end;
     HelpContext:= H_Catalogo_Familias;
end;

procedure TEditFamilias_DevEx.Connect;
begin
     with dmPlanCarrera do
     begin
          {
          cdsEditFamilia.Refrescar;
          }
          DataSource.DataSet := cdsEditFamilia;
          dsRenglon.DataSet := cdsCompetenciaFamilia;
     end;
end;

procedure TEditFamilias_DevEx.dsRenglonDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     with dsRenglon.Dataset do
     begin
          Self.GridRenglones.Columns[ K_COLUMNA_COMPETENCIA_OBSERVACIONES ].ReadOnly := IsEmpty;
          Self.BBBorrar_DevEx.Enabled := not IsEmpty;
          Self.BBModificar_DevEx.Enabled := not IsEmpty;
     end;
end;

procedure TEditFamilias_DevEx.BBAgregarClick(Sender: TObject);
begin
     inherited;
     ShowDlgModal( SelCompetenciasFamilia_DevEx, TSelCompetenciasFamilia_DevEx );
end;

procedure TEditFamilias_DevEx.BBBorrarClick(Sender: TObject);
begin
     if not dmPlanCarrera.cdsCompetenciaFamilia.IsEmpty then
     begin
          if ZetaDialogo.ZWarningConfirm( '� Atenci�n !', '� Desea borrar la Competencia ?', 0, mbCancel ) then
             inherited;
     end;
end;

procedure TEditFamilias_DevEx.BBModificarClick(Sender: TObject);
begin
     inherited;
     ActiveControl := GridRenglones;
     with GridRenglones do
     begin
          SelectedField := Columns[ K_COLUMNA_COMPETENCIA_OBSERVACIONES ].Field;
     end;
end;

procedure TEditFamilias_DevEx.DoLookup;
begin
     inherited DoLookup;
end;

procedure TEditFamilias_DevEx.HabilitaControles;
begin
     inherited HabilitaControles;
end;

procedure TEditFamilias_DevEx.Agregar;
begin
     inherited Agregar;
end;

procedure TEditFamilias_DevEx.Borrar;
begin
     inherited Borrar;
end;

procedure TEditFamilias_DevEx.Modificar;
begin
     inherited Modificar;
end;

procedure TEditFamilias_DevEx.DoCancelChanges;
begin
     dmPlanCarrera.cdsCompetenciaFamilia.CancelUpdates;
     inherited DoCancelChanges;
end;

end.
