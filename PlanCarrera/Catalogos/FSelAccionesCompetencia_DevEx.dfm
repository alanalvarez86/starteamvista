inherited SelAccionesCompetencia_DevEx: TSelAccionesCompetencia_DevEx
  Left = 240
  Top = 198
  Caption = 'Seleccionar Acciones'
  ClientHeight = 373
  ClientWidth = 442
  OnShow = FormShow
  ExplicitWidth = 448
  ExplicitHeight = 402
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 337
    Width = 442
    ExplicitTop = 337
    ExplicitWidth = 442
    inherited OK_DevEx: TcxButton
      Left = 275
      OnClick = OK_DevExClick
      ExplicitLeft = 275
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 355
      ExplicitLeft = 355
    end
  end
  object Lista: TCheckListBox [1]
    Left = 0
    Top = 0
    Width = 442
    Height = 337
    Align = alClient
    ItemHeight = 13
    TabOrder = 1
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
