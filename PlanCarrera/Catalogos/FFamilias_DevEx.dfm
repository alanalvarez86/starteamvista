inherited Familias_DevEx: TFamilias_DevEx
  Caption = 'Familias'
  ClientHeight = 277
  ClientWidth = 605
  ExplicitWidth = 605
  ExplicitHeight = 277
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 605
    ExplicitWidth = 605
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 346
      ExplicitWidth = 346
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 340
        ExplicitLeft = 3
      end
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 605
    Height = 258
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'FP_CODIGO'
        Title.Caption = 'C'#243'digo'
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FP_DESCRIP'
        Title.Caption = 'Descripci'#243'n'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FP_INGLES'
        Title.Caption = 'Ingl'#233's'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FP_NUMERO'
        Title.Caption = 'N'#250'mero'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FP_TEXTO'
        Title.Caption = 'Texto'
        Width = 200
        Visible = True
      end>
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 605
    Height = 258
    ExplicitWidth = 605
    ExplicitHeight = 258
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object FP_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'FP_CODIGO'
      end
      object FP_DESCRIP: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'FP_DESCRIP'
      end
      object FP_INGLES: TcxGridDBColumn
        Caption = 'Ingl'#233's'
        DataBinding.FieldName = 'FP_INGLES'
      end
      object FP_NUMERO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'FP_NUMERO'
      end
      object FP_TEXTO: TcxGridDBColumn
        Caption = 'Texto'
        DataBinding.FieldName = 'FP_TEXTO'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
