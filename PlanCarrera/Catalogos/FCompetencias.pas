unit FCompetencias;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls;

type
  TCompetencias = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  Competencias: TCompetencias;

implementation

{$R *.DFM}

uses
    dPlanCarrera,
    ZetaBuscador,
    ZetaCommonClasses;

{ TCompetencias }
procedure TCompetencias.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     //HelpContext := H00005_Catalogo_de_Medicinas;
end;

procedure TCompetencias.Agregar;
begin
     dmPlanCarrera.cdsCompetencia.Agregar;
end;

procedure TCompetencias.Borrar;
begin
     dmPlanCarrera.cdsCompetencia.Borrar;
end;

procedure TCompetencias.Connect;
begin
     with dmPlanCarrera do
     begin
          cdsTCompetencia.Conectar;
          cdsCompetencia.Conectar;
          DataSource.DataSet := cdsCompetencia;
     end;
end;

procedure TCompetencias.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Competencia', 'COMPETEN', 'CM_CODIGO', dmPlanCarrera.cdsCompetencia );
end;

procedure TCompetencias.ImprimirForma;
begin
  inherited;
end;

procedure TCompetencias.Modificar;
begin
     dmPlanCarrera.cdsCompetencia.Modificar;
end;

function TCompetencias.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := True;
end;

function TCompetencias.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := True;
end;

procedure TCompetencias.Refresh;
begin
     dmPlanCarrera.cdsCompetencia.Refrescar;
end;

end.
