unit FEditCompetencias_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComCtrls, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask, Db, Grids, DBGrids,
     ZBaseEdicionRenglon_DevEx,
     ZetaDBGrid,
     ZetaKeyLookup,
     ZetaEdit,
     ZetaNumero,
     ZetaSmartLists, ZBaseEdicionRenglon, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, cxControls, dxBarBuiltInMenu, dxBarExtItems,
  dxBar, cxClasses, Vcl.ImgList, cxPC, cxNavigator, cxDBNavigator, cxButtons,
  ZetaKeyLookup_DevEx, cxContainer, cxEdit, cxSplitter, cxGroupBox, cxTextEdit,
  cxMemo, cxDBEdit;

type
  TEditCompetencias_DevEx = class(TBaseEdicionRenglon_DevEx)
    Label1: TLabel;
    CM_CODIGO: TZetaDBEdit;
    Label2: TLabel;
    CM_DESCRIP: TDBEdit;
    dsCalifica: TDataSource;
    Label3: TLabel;
    CM_INGLES: TDBEdit;
    Label6: TLabel;
    Label7: TLabel;
    Label4: TLabel;
    TC_CODIGO: TZetaDBKeyLookup_DevEx;
    Label5: TLabel;
    CM_TEXTO: TDBEdit;
    CM_NUMERO: TZetaDBNumero;
    Criterios: TcxTabSheet;
    GridCriterios: TZetaDBGrid;
    Panel5: TPanel;
    bbAgregarCriterios: TcxButton;
    bbBorrarCriterios: TcxButton;
    bbModificarCriterios: TcxButton;
    Panel4: TPanel;
    Subir: TcxButton;
    Bajar: TcxButton;
    gbdescripcion: TcxGroupBox;
    cxGroupBox2: TcxGroupBox;
    Panel3: TPanel;
    Panel6: TPanel;
    MC_DESCRIP: TcxDBMemo;
    CM_OBSERVA: TcxDBMemo;
    CM_DETALLE: TcxDBMemo;
    procedure FormCreate(Sender: TObject);
    procedure bbAgregarCriteriosClick(Sender: TObject);
    procedure bbBorrarCriteriosClick(Sender: TObject);
    procedure bbModificarCriteriosClick(Sender: TObject);
    procedure SubirClick(Sender: TObject);
    procedure BajarClick(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure BBBorrarClick(Sender: TObject);
    procedure BBModificarClick(Sender: TObject);
    procedure dsRenglonDataChange(Sender: TObject; Field: TField);
    procedure dsCalificaDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
    function SubeBajaAcciones( const lSubir: Boolean ): Boolean;
  protected
    { Protected declarations }
    procedure Connect;override;
    procedure DoLookup; override;
    procedure HabilitaControles; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure DoCancelChanges; override;
  public
    { Public declarations }
  end;

var
  EditCompetencias_DevEx: TEditCompetencias_DevEx;

implementation

{$R *.DFM}

uses
    FSelCalificacionesCompetencia_DevEx,
    FSelAccionesCompetencia_DevEx,
    dPlanCarrera,
    ZetaBuscador,
    ZetaCommonClasses,
    ZetaCommonLists,
    ZetaDialogo,
    ZBaseDlgModal_DevEx,
    ZAccesosTress;

const
     K_COLUMNA_ACCION_ORDEN = 0;
     K_COLUMNA_ACCION_CODIGO = 1;
     K_COLUMNA_ACCION_NOMBRE = 2;
     K_COLUMNA_CRITERIO_CODIGO = 0;
     K_COLUMNA_CRITERIO_NOMBRE = 1;

{ TEditHabilidades }

procedure TEditCompetencias_DevEx.FormCreate(Sender: TObject);
var
   i: Integer;
begin
     inherited;
     IndexDerechos := D_CARRERA_CAT_COMPETEN;
     FirstControl := CM_CODIGO;
     HelpContext:= H_Catalogo_Competencias;
     TC_CODIGO.LookupDataset := dmPlanCarrera.cdsTCompetencia;
     with GridRenglones do
     begin
          for i := K_COLUMNA_ACCION_ORDEN to K_COLUMNA_ACCION_NOMBRE do
          begin
               with Columns[ i ] do
               begin
                    Color := clInfoBk;
                    Font.Color := clInfoText;
                    ReadOnly := True;
               end;
          end;
          Self.GridCriterios.Options := Options;
     end;
     with GridCriterios do
     begin
          for i := K_COLUMNA_CRITERIO_CODIGO to K_COLUMNA_CRITERIO_NOMBRE do
          begin
               with Columns[ i ] do
               begin
                    Color := clInfoBk;
                    Font.Color := clInfoText;
                    ReadOnly := True;
               end;
          end;
     end;
end;

procedure TEditCompetencias_DevEx.Connect;
begin
     with dmPlanCarrera do
     begin
          {
          cdsEditCompetencia.Refrescar;
          }
          DataSource.DataSet := cdsEditCompetencia;
          dsRenglon.DataSet := cdsCompetenciaMapa;
          dsCalifica.DataSet := cdsCompetenciaCalifica;
     end;
end;

procedure TEditCompetencias_DevEx.DoLookup;
begin
     inherited DoLookup;
end;

procedure TEditCompetencias_DevEx.HabilitaControles;
begin
     inherited HabilitaControles;
end;

procedure TEditCompetencias_DevEx.Modificar;
begin
     inherited Modificar;
end;

procedure TEditCompetencias_DevEx.Agregar;
begin
     inherited Agregar;
end;

procedure TEditCompetencias_DevEx.Borrar;
begin
     inherited Borrar;
end;

procedure TEditCompetencias_DevEx.DoCancelChanges;
begin
     with dmPlanCarrera do
     begin
          cdsCompetenciaMapa.CancelUpdates;
          cdsCompetenciaCalifica.CancelUpdates;
     end;
     inherited DoCancelChanges;
end;

{ GridRenglones - Acciones por Competencia }

procedure TEditCompetencias_DevEx.dsRenglonDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     with dsRenglon.Dataset do
     begin
          Self.Subir.Enabled := not IsEmpty;
          Self.Bajar.Enabled := not IsEmpty;
          CM_OBSERVA.Enabled := not IsEmpty;
          Self.BBBorrar_DevEx.Enabled := not IsEmpty;
          Self.BBModificar_DevEx.Enabled := not IsEmpty;
     end;
end;

procedure TEditCompetencias_DevEx.SubirClick(Sender: TObject);
begin
     if not SubeBajaAcciones( True ) then
        ZetaDialogo.ZError( 'Competencias', '� Es El Inicio de la Lista de Acciones !', 0 );
end;

procedure TEditCompetencias_DevEx.BajarClick(Sender: TObject);
begin
     if not SubeBajaAcciones( False ) then
        ZetaDialogo.ZError( 'Competencias', '� Es El Final de la Lista de Acciones !', 0 );
end;

function TEditCompetencias_DevEx.SubeBajaAcciones( const lSubir: Boolean ): Boolean;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        Result := dmPlanCarrera.SubeBajaAcciones( lSubir );
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TEditCompetencias_DevEx.BBAgregarClick(Sender: TObject);
begin
     inherited;
     ShowDlgModal( SelAccionesCompetencia_DevEx, TSelAccionesCompetencia_DevEx );
end;

procedure TEditCompetencias_DevEx.BBBorrarClick(Sender: TObject);
begin
     if ( Zetadialogo.ZWarningConfirm( 'Competencias', '� Seguro de Borrar el Registro ?', 0, mbCancel ) ) then
        inherited;
end;

procedure TEditCompetencias_DevEx.BBModificarClick(Sender: TObject);
begin
     inherited;
     with dmPlanCarrera.cdsCompetenciaMapa do
     begin
          if not IsEmpty then
             Edit;
     end;
     ActiveControl := CM_OBSERVA;
end;

{ GridCriterios - Criterios por Competencia }

procedure TEditCompetencias_DevEx.dsCalificaDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     with dsCalifica.Dataset do
     begin
          MC_DESCRIP.Enabled := not IsEmpty;
          Self.bbBorrarCriterios.Enabled := not IsEmpty;
          Self.bbModificarCriterios.Enabled := not IsEmpty;
     end;
end;

procedure TEditCompetencias_DevEx.bbAgregarCriteriosClick(Sender: TObject);
begin
     inherited;
     ShowDlgModal( SelCalificacionesCompetencia_DevEx, TSelCalificacionesCompetencia_DevEx );
end;

procedure TEditCompetencias_DevEx.bbBorrarCriteriosClick(Sender: TObject);
begin
     if ZetaDialogo.ZWarningConfirm( '� Atenci�n !', '� Desea borrar este Criterio ?', 0, mbCancel ) then
     begin
          inherited;
          with dmPlanCarrera.cdsCompetenciaCalifica  do
          begin
               if not IsEmpty then
                  Delete;
          end;
     end;
end;
procedure TEditCompetencias_DevEx.bbModificarCriteriosClick(Sender: TObject);
begin
     inherited;
     with dmPlanCarrera.cdsCompetenciaCalifica do
     begin
          if not IsEmpty then
             Edit;
     end;
     ActiveControl := MC_DESCRIP;
end;

end.
