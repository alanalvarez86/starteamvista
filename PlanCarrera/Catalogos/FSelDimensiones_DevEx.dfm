inherited SelDimensiones_DevEx: TSelDimensiones_DevEx
  Left = 395
  Top = 230
  Caption = 'Seleccionar Dimensiones'
  ClientHeight = 323
  ClientWidth = 342
  OldCreateOrder = True
  OnShow = FormShow
  ExplicitWidth = 348
  ExplicitHeight = 352
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 287
    Width = 342
    ExplicitTop = 287
    ExplicitWidth = 342
    inherited OK_DevEx: TcxButton
      Left = 175
      OnClick = OK_DevExClick
      ExplicitLeft = 175
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 255
      ExplicitLeft = 255
    end
  end
  object Lista: TCheckListBox [1]
    Left = 0
    Top = 0
    Width = 342
    Height = 287
    Align = alClient
    ItemHeight = 13
    TabOrder = 1
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
