unit FCatTCompetencias_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Db, ExtCtrls, StdCtrls, Grids, DBGrids, ZetaDBGrid,
  ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TTipoCompetencias_DevEx = class(TBaseGridLectura_DevEx)
    ZetaDBGrid1: TZetaDBGrid;
    TC_CODIGO: TcxGridDBColumn;
    TC_DESCRIP: TcxGridDBColumn;
    TC_TIPO: TcxGridDBColumn;
    TC_INGLES: TcxGridDBColumn;
    TC_NUMERO: TcxGridDBColumn;
    TC_TEXTO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  TipoCompetencias_DevEx: TTipoCompetencias_DevEx;

implementation

{$R *.DFM}

uses
    dPlanCarrera,
    ZetaBuscador_DevEx,
    ZetaCommonClasses;


{ TTipoCompetencias }
procedure TTipoCompetencias_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H_Tipos_Competencia;
end;

procedure TTipoCompetencias_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  CreaColumaSumatoria(ZetaDbGridDBTableView.Columns[0], 0 ,'' , SkCount );
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := False;
  ApplyMinWidth;
  ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TTipoCompetencias_DevEx.Agregar;
begin
     dmPlanCarrera.cdsTCompetencia.Agregar;
end;

procedure TTipoCompetencias_DevEx.Borrar;
begin
     dmPlanCarrera.cdsTCompetencia.Borrar;
end;

procedure TTipoCompetencias_DevEx.Connect;
begin
     with dmPlanCarrera do
     begin
          cdsTCompetencia.Conectar;
          DataSource.DataSet:= cdsTCompetencia;
     end;
end;

procedure TTipoCompetencias_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Competencia', 'TCOMPETE', 'TC_CODIGO', dmPlanCarrera.cdsTCompetencia );
end;

procedure TTipoCompetencias_DevEx.ImprimirForma;
begin
  inherited;

end;

procedure TTipoCompetencias_DevEx.Modificar;
begin
     dmPlanCarrera.cdsTCompetencia.Modificar;
end;

function TTipoCompetencias_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := true;
end;

function TTipoCompetencias_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := true;
end;

procedure TTipoCompetencias_DevEx.Refresh;
begin
     dmPlanCarrera.cdsTCompetencia.Refrescar;
end;

end.
