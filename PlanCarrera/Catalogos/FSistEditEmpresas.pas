unit FSistEditEmpresas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FSistBaseEditEmpresas, Db, StdCtrls, ZetaEdit, Mask, DBCtrls, ExtCtrls,
  Buttons;

type
  TSistEditEmpresas = class(TSistBaseEditEmpresas)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SistEditEmpresas: TSistEditEmpresas;

implementation

{$R *.DFM}

end.
