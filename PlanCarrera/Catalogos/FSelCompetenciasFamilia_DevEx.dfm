inherited SelCompetenciasFamilia_DevEx: TSelCompetenciasFamilia_DevEx
  Caption = 'Seleccionar Competencias'
  ClientHeight = 323
  ClientWidth = 342
  OldCreateOrder = True
  OnShow = FormShow
  ExplicitWidth = 348
  ExplicitHeight = 352
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 287
    Width = 342
    ExplicitTop = 287
    ExplicitWidth = 342
    inherited OK_DevEx: TcxButton
      Left = 168
      OnClick = OK_DevExClick
      ExplicitLeft = 168
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 248
      ExplicitLeft = 248
    end
  end
  object Lista: TCheckListBox [1]
    Left = 0
    Top = 0
    Width = 342
    Height = 287
    Align = alClient
    ItemHeight = 13
    TabOrder = 1
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
