unit ZetaDespConsulta;

interface

uses Forms, Controls, Sysutils, Dialogs,
     ZBaseConsulta;

type
  eFormaConsulta = ( efcSistGlobales,
                     efcReportes,
                     efcSistProcesos,
                     efcCatTCompeten,
                     efcCatCalifica,
                     efcCatCompeten,
                     efcCatAccion,
                     efcCatPuesto,
                     efcCatFamilia,
                     efcCatNivel,
                     efcCatDimensio );

  TPropConsulta = record
    IndexDerechos: Integer;
    ClaseForma: TBaseConsultaClass;
  end;

function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;

implementation

uses
    FCatTCompetencias_DevEx,
    FCalifica_DevEx,
    FDimensiones_DevEx,
    FAcciones_DevEx,
    FNiveles_DevEx,
    FFamilias_DevEx,
    FPuestos_DevEx,
    FCompetencias_DevEx,
    FReportes_DevEx,
    FGlobalesCarrera_DevEx,
    ZAccesosMgr,
    ZAccesosTress;

function Consulta( const iDerechos: Integer; const Forma: TBaseConsultaClass ): TPropConsulta;
begin
     with Result do
     begin
          IndexDerechos := iDerechos;
          ClaseForma := Forma;
     end;
end;

function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;
begin
     //Result := Consulta( 0, nil );
     case Forma of
          efcCatTCompeten             : Result := Consulta( D_CARRERA_CAT_TCOMPETEN, TTipoCompetencias_DevEx );
          efcCatCalifica              : Result := Consulta( D_CARRERA_CAT_CALIFICA, TCalifica_DevEx );
          efcCatCompeten              : Result := Consulta( D_CARRERA_CAT_COMPETEN, TCompetencias_DevEx );
          efcCatAccion                : Result := Consulta( D_CARRERA_CAT_ACCION, TAcciones_DevEx );
          efcCatPuesto                : Result := Consulta( D_CARRERA_CAT_PUESTO, TPuestos_DevEx );
          efcCatFamilia               : Result := Consulta( D_CARRERA_CAT_FAMILIA, TFamilias_DevEx );
          efcCatNivel                 : Result := Consulta( D_CARRERA_CAT_NIVEL, TNiveles_DevEx );
          efcCatDimensio              : Result := Consulta( D_CARRERA_CAT_DIMENSIO, TDimensiones_DevEx );
          efcReportes                 : Result := Consulta( D_REPORTES_CARRERA, TReportes_DevEx );
          efcSistGlobales             : Result := Consulta( D_CARRERA_GLOBALES, TGlobalesCarrera_DevEx );
     else
         Result := Consulta( 0, nil );
     end;
end;

end.
