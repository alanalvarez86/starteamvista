inherited EditNiveles: TEditNiveles
  Left = 196
  Top = 163
  Caption = 'Niveles'
  ClientHeight = 434
  ClientWidth = 624
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 398
    Width = 624
    inherited OK: TBitBtn
      Left = 466
    end
    inherited Cancelar: TBitBtn
      Left = 545
    end
  end
  inherited PanelSuperior: TPanel
    Width = 624
    inherited DBNavigator: TDBNavigator
      DataSource = nil
      Enabled = False
      Visible = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 624
    inherited ValorActivo2: TPanel
      Width = 298
    end
  end
  inherited Panel1: TPanel
    Width = 624
    Height = 54
    object Label1: TLabel
      Left = 57
      Top = 8
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'digo:'
    end
    object Label2: TLabel
      Left = 34
      Top = 30
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descripci'#243'n:'
    end
    object NP_DESCRIP: TDBEdit
      Left = 97
      Top = 26
      Width = 413
      Height = 21
      DataField = 'NP_DESCRIP'
      DataSource = DataSource
      TabOrder = 1
    end
    object NP_CODIGO: TZetaDBEdit
      Left = 97
      Top = 4
      Width = 90
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
      ConfirmEdit = True
      DataField = 'NP_CODIGO'
      DataSource = DataSource
    end
  end
  inherited PageControl: TPageControl
    Top = 105
    Width = 624
    Height = 293
    inherited Datos: TTabSheet
      object Label3: TLabel
        Left = 57
        Top = 7
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ingl'#233's:'
        FocusControl = NP_INGLES
      end
      object Label4: TLabel
        Left = 58
        Top = 51
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Texto:'
      end
      object Label5: TLabel
        Left = 48
        Top = 29
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'N'#250'mero:'
      end
      object Label6: TLabel
        Left = 52
        Top = 73
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Actitud:'
      end
      object Label7: TLabel
        Left = 52
        Top = 94
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Detalle:'
      end
      object NP_INGLES: TDBEdit
        Left = 93
        Top = 3
        Width = 275
        Height = 21
        DataField = 'NP_INGLES'
        DataSource = DataSource
        TabOrder = 0
      end
      object NP_TEXTO: TDBEdit
        Left = 93
        Top = 47
        Width = 275
        Height = 21
        DataField = 'NP_TEXTO'
        DataSource = DataSource
        TabOrder = 2
      end
      object NP_NUMERO: TZetaDBNumero
        Left = 93
        Top = 25
        Width = 121
        Height = 21
        Mascara = mnPesos
        TabOrder = 1
        Text = '0.00'
        UseEnterKey = True
        DataField = 'NP_NUMERO'
        DataSource = DataSource
      end
      object NP_ACTITUD: TDBEdit
        Left = 93
        Top = 69
        Width = 275
        Height = 21
        DataField = 'NP_ACTITUD'
        DataSource = DataSource
        TabOrder = 3
      end
      object NP_DETALLE: TDBMemo
        Left = 93
        Top = 91
        Width = 516
        Height = 166
        DataField = 'NP_DETALLE'
        DataSource = DataSource
        ScrollBars = ssVertical
        TabOrder = 4
      end
    end
    inherited Tabla: TTabSheet
      Caption = 'Dimensiones'
      object SplitterDimensiones: TSplitter [0]
        Left = 345
        Top = 29
        Height = 236
      end
      inherited GridRenglones: TZetaDBGrid
        Width = 345
        Height = 236
        Align = alLeft
        Columns = <
          item
            Expanded = False
            FieldName = 'DM_CODIGO'
            ReadOnly = True
            Title.Caption = 'Dimensi'#243'n'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DM_DESCRIP'
            ReadOnly = True
            Title.Caption = 'Descripci'#243'n'
            Width = 180
            Visible = True
          end>
      end
      inherited Panel2: TPanel
        Width = 616
      end
      object Panel3: TPanel
        Left = 348
        Top = 29
        Width = 268
        Height = 236
        Align = alClient
        TabOrder = 2
        object gbResumen: TGroupBox
          Left = 1
          Top = 1
          Width = 266
          Height = 41
          Align = alTop
          Caption = ' Resumen '
          TabOrder = 0
          object ND_RESUMEN: TDBEdit
            Left = 3
            Top = 12
            Width = 261
            Height = 21
            DataField = 'ND_RESUMEN'
            DataSource = dsRenglon
            TabOrder = 0
          end
        end
        object GroupBox1: TGroupBox
          Left = 1
          Top = 42
          Width = 266
          Height = 193
          Align = alClient
          Caption = ' Descripci'#243'n '
          TabOrder = 1
          object ND_DESCRIP: TDBMemo
            Left = 2
            Top = 15
            Width = 262
            Height = 176
            Align = alClient
            DataField = 'ND_DESCRIP'
            DataSource = dsRenglon
            ScrollBars = ssVertical
            TabOrder = 0
          end
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 364
  end
  inherited dsRenglon: TDataSource
    OnDataChange = dsRenglonDataChange
  end
end
