unit FEditCalifica_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaNumero,
  ZetaKeyCombo, Mask, ZetaEdit, ZetaSmartLists, ZBaseEdicion, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, cxControls, dxBarExtItems,
  dxBar, cxClasses, Vcl.ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TEditCalifica_DevEx = class(TBaseEdicion_DevEx)
    Label1: TLabel;
    CA_CODIGO: TZetaDBEdit;
    Label2: TLabel;
    CA_DESCRIP: TDBEdit;
    Label3: TLabel;
    CA_INGLES: TDBEdit;
    Label5: TLabel;
    CA_NUMERO: TZetaDBNumero;
    Label6: TLabel;
    CA_TEXTO: TDBEdit;
    Label7: TLabel;
    CA_GRUPO: TDBEdit;
    Label8: TLabel;
    CA_ORDEN: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  EditCalifica_DevEx: TEditCalifica_DevEx;

implementation

{$R *.DFM}

uses
    dPlanCarrera,
    ZetaBuscador,
    ZetaCommonClasses,
    ZAccesosTress;


{ TEditCalifica }

procedure TEditCalifica_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CARRERA_CAT_CALIFICA;
     FirstControl := CA_CODIGO;
     HelpContext:= H_Tabla_Calificaciones;
end;

procedure TEditCalifica_DevEx.Connect;
begin
     DataSource.DataSet := dmPlanCarrera.cdsCalificaciones
end;

procedure TEditCalifica_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Calificaciones', 'CALIFICA', 'CA_CODIGO', dmPlanCarrera.cdsCalificaciones );
end;

end.
