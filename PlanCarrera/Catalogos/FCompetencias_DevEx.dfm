inherited Competencias_DevEx: TCompetencias_DevEx
  Caption = 'Competencias'
  ClientHeight = 257
  ClientWidth = 614
  ExplicitWidth = 614
  ExplicitHeight = 257
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 614
    ExplicitWidth = 614
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 355
      ExplicitWidth = 355
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 349
        ExplicitLeft = 3
      end
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 614
    Height = 238
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'CM_CODIGO'
        Title.Caption = 'C'#243'digo'
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CM_DESCRIP'
        Title.Caption = 'Descripci'#243'n'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TC_DESCRIP'
        Title.Caption = 'Tipo'
        Width = 180
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CM_INGLES'
        Title.Caption = 'Ingl'#233's'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CM_NUMERO'
        Title.Caption = 'N'#250'mero'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CM_TEXTO'
        Title.Caption = 'Texto'
        Width = 200
        Visible = True
      end>
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 614
    Height = 238
    ExplicitWidth = 614
    ExplicitHeight = 238
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CM_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CM_CODIGO'
      end
      object CM_DESCRIP: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'CM_DESCRIP'
      end
      object TC_DESCRIP: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'TC_DESCRIP'
      end
      object CM_INGLES: TcxGridDBColumn
        Caption = 'Ingl'#233's'
        DataBinding.FieldName = 'CM_INGLES'
      end
      object CM_NUMERO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'CM_NUMERO'
      end
      object CM_TEXTO: TcxGridDBColumn
        Caption = 'Texto'
        DataBinding.FieldName = 'CM_TEXTO'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
