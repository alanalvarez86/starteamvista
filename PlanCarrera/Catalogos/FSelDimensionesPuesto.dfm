inherited SelDimensionesPuesto: TSelDimensionesPuesto
  Caption = 'Seleccionar Dimensiones'
  ClientHeight = 323
  ClientWidth = 342
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 287
    Width = 342
    inherited OK: TBitBtn
      Left = 174
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 259
    end
  end
  object Lista: TCheckListBox
    Left = 0
    Top = 0
    Width = 342
    Height = 287
    Align = alClient
    ItemHeight = 13
    TabOrder = 1
  end
end
