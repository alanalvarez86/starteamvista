inherited EditFamilias: TEditFamilias
  Left = 259
  Top = 211
  Caption = 'Familias'
  ClientHeight = 436
  ClientWidth = 597
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 400
    Width = 597
    inherited OK: TBitBtn
      Left = 439
    end
    inherited Cancelar: TBitBtn
      Left = 518
    end
  end
  inherited PanelSuperior: TPanel
    Width = 597
    inherited DBNavigator: TDBNavigator
      Visible = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 597
    inherited ValorActivo2: TPanel
      Width = 271
    end
  end
  inherited Panel1: TPanel
    Width = 597
    object Label1: TLabel
      Left = 114
      Top = 8
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'digo:'
    end
    object Label2: TLabel
      Left = 91
      Top = 30
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descripci'#243'n:'
    end
    object FP_DESCRIP: TDBEdit
      Left = 154
      Top = 26
      Width = 335
      Height = 21
      DataField = 'FP_DESCRIP'
      DataSource = DataSource
      TabOrder = 1
    end
    object FP_CODIGO: TZetaDBEdit
      Left = 154
      Top = 4
      Width = 90
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
      ConfirmEdit = True
      DataField = 'FP_CODIGO'
      DataSource = DataSource
    end
  end
  inherited PageControl: TPageControl
    Width = 597
    Height = 299
    inherited Datos: TTabSheet
      object Label3: TLabel
        Left = 110
        Top = 10
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ingl'#233's:'
        FocusControl = FP_INGLES
      end
      object Label5: TLabel
        Left = 101
        Top = 32
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'N'#250'mero:'
      end
      object Label4: TLabel
        Left = 111
        Top = 54
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Texto:'
      end
      object FP_TEXTO: TDBEdit
        Left = 146
        Top = 50
        Width = 275
        Height = 21
        DataField = 'FP_TEXTO'
        DataSource = DataSource
        TabOrder = 2
      end
      object FP_NUMERO: TZetaDBNumero
        Left = 146
        Top = 28
        Width = 121
        Height = 21
        Mascara = mnPesos
        TabOrder = 1
        Text = '0.00'
        UseEnterKey = True
        DataField = 'FP_NUMERO'
        DataSource = DataSource
      end
      object FP_INGLES: TDBEdit
        Left = 146
        Top = 6
        Width = 275
        Height = 21
        DataField = 'FP_INGLES'
        DataSource = DataSource
        TabOrder = 0
      end
    end
    inherited Tabla: TTabSheet
      Caption = 'Competencias'
      inherited GridRenglones: TZetaDBGrid
        Width = 589
        Height = 242
        Columns = <
          item
            Expanded = False
            FieldName = 'CM_CODIGO'
            ReadOnly = True
            Title.Caption = 'C'#243'digo'
            Width = 70
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CM_DESCRIP'
            ReadOnly = True
            Title.Caption = 'Descripci'#243'n'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CF_OBSERVA'
            Title.Caption = 'Observaciones'
            Width = 265
            Visible = True
          end>
      end
      inherited Panel2: TPanel
        Width = 589
      end
    end
  end
  inherited dsRenglon: TDataSource
    OnDataChange = dsRenglonDataChange
  end
end
