inherited EditPuestos: TEditPuestos
  Left = 250
  Top = 184
  Caption = 'Puestos'
  ClientHeight = 392
  ClientWidth = 526
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 356
    Width = 526
    inherited OK: TBitBtn
      Left = 368
    end
    inherited Cancelar: TBitBtn
      Left = 447
    end
  end
  inherited PanelSuperior: TPanel
    Width = 526
    inherited DBNavigator: TDBNavigator
      Visible = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 526
    inherited ValorActivo2: TPanel
      Width = 200
    end
  end
  inherited Panel1: TPanel
    Width = 526
    Height = 62
    object Label1: TLabel
      Left = 111
      Top = 8
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'digo:'
    end
    object Label2: TLabel
      Left = 88
      Top = 27
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descripci'#243'n:'
    end
    object PU_CODIGO: TZetaDBTextBox
      Left = 151
      Top = 6
      Width = 90
      Height = 17
      AutoSize = False
      Caption = 'PU_CODIGO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'PU_CODIGO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object PU_DESCRIP: TZetaDBTextBox
      Left = 151
      Top = 26
      Width = 354
      Height = 17
      AutoSize = False
      Caption = 'PU_DESCRIP'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'PU_DESCRIP'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
  end
  inherited PageControl: TPageControl
    Top = 113
    Width = 526
    Height = 243
    ActivePage = Dimensiones
    inherited Datos: TTabSheet
      object Label3: TLabel
        Left = 106
        Top = 10
        Width = 35
        Height = 13
        Alignment = taRightJustify
        Caption = 'Familia:'
      end
      object Label6: TLabel
        Left = 114
        Top = 32
        Width = 27
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel:'
      end
      object Label7: TLabel
        Left = 15
        Top = 51
        Width = 126
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descripci'#243'n de Funciones:'
      end
      object PU_DETALLE: TDBMemo
        Left = 146
        Top = 50
        Width = 369
        Height = 163
        DataField = 'PU_DETALLE'
        DataSource = DataSource
        ScrollBars = ssVertical
        TabOrder = 2
      end
      object FP_CODIGO: TZetaDBKeyLookup
        Left = 146
        Top = 6
        Width = 300
        Height = 21
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        DataField = 'FP_CODIGO'
        DataSource = DataSource
      end
      object NP_CODIGO: TZetaDBKeyLookup
        Left = 146
        Top = 28
        Width = 300
        Height = 21
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        DataField = 'NP_CODIGO'
        DataSource = DataSource
      end
    end
    inherited Tabla: TTabSheet
      Caption = 'Competencias'
      inherited GridRenglones: TZetaDBGrid
        Width = 518
        Height = 186
        OnColEnter = GridRenglonesColEnter
        OnColExit = GridRenglonesColExit
        OnEnter = GridRenglonesEnter
        Columns = <
          item
            Expanded = False
            FieldName = 'CM_CODIGO'
            ReadOnly = True
            Title.Caption = 'C'#243'digo'
            Width = 82
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CM_DESCRIP'
            ReadOnly = True
            Title.Caption = 'Nombre'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CA_CODIGO'
            Title.Caption = 'Requisito'
            Width = 82
            Visible = True
          end>
      end
      inherited Panel2: TPanel
        Width = 518
      end
    end
    object Dimensiones: TTabSheet
      Caption = 'Dimensiones'
      ImageIndex = 2
      object SplitterDimension: TSplitter
        Left = 289
        Top = 29
        Height = 186
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 518
        Height = 29
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object bbAgregarDimensiones: TBitBtn
          Left = 8
          Top = 1
          Width = 129
          Height = 25
          Caption = 'Agregar Rengl'#243'n'
          TabOrder = 0
          OnClick = bbAgregarDimensionesClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
            333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
            0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
            07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
            07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
            0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
            33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
            B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
            3BB33773333773333773B333333B3333333B7333333733333337}
          NumGlyphs = 2
        end
        object bbBorrarDimensiones: TBitBtn
          Left = 148
          Top = 1
          Width = 129
          Height = 25
          Caption = 'Borrar Rengl'#243'n'
          TabOrder = 1
          OnClick = bbBorrarDimensionesClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
            305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
            005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
            B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
            B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
            B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
            B0557777FF577777F7F500000E055550805577777F7555575755500000555555
            05555777775555557F5555000555555505555577755555557555}
          NumGlyphs = 2
        end
        object bbModificarDimensiones: TBitBtn
          Left = 288
          Top = 1
          Width = 129
          Height = 25
          Caption = 'Modificar Rengl'#243'n'
          TabOrder = 2
          OnClick = bbModificarDimensionesClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
            000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
            00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
            F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
            0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
            FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
            FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
            0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
            00333377737FFFFF773333303300000003333337337777777333}
          NumGlyphs = 2
        end
      end
      object GridDimensiones: TZetaDBGrid
        Left = 0
        Top = 29
        Width = 289
        Height = 186
        Align = alLeft
        DataSource = dsDimensiones
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnTitleClick = GridRenglonesTitleClick
        Columns = <
          item
            Expanded = False
            FieldName = 'DM_CODIGO'
            ReadOnly = True
            Title.Caption = 'C'#243'digo'
            Width = 82
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DM_DESCRIP'
            ReadOnly = True
            Title.Caption = 'Nombre'
            Width = 170
            Visible = True
          end>
      end
      object GroupBox1: TGroupBox
        Left = 292
        Top = 29
        Width = 226
        Height = 186
        Align = alClient
        Caption = ' Descripci'#243'n '
        TabOrder = 2
        object PD_DESCRIP: TDBMemo
          Left = 2
          Top = 15
          Width = 222
          Height = 169
          Align = alClient
          DataField = 'PD_DESCRIP'
          DataSource = dsDimensiones
          MaxLength = 255
          TabOrder = 0
        end
      end
    end
  end
  inherited dsRenglon: TDataSource
    OnDataChange = dsRenglonDataChange
  end
  object dsDimensiones: TDataSource
    OnDataChange = dsDimensionesDataChange
    Left = 423
    Top = 1
  end
end
