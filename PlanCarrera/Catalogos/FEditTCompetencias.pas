unit FEditTCompetencias;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaKeyCombo,
  ZetaNumero, Mask, ZetaEdit, ZetaSmartLists;

type
  TEditTCompetencias = class(TBaseEdicion)
    TC_CODIGO: TZetaDBEdit;
    Label1: TLabel;
    Label2: TLabel;
    TC_DESCRIP: TDBEdit;
    TC_INGLES: TDBEdit;
    Label3: TLabel;
    Label5: TLabel;
    TC_NUMERO: TZetaDBNumero;
    TC_TEXTO: TDBEdit;
    Label6: TLabel;
    TC_TIPO: TZetaDBKeyCombo;
    Label4: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  EditTCompetencias: TEditTCompetencias;

implementation

{$R *.DFM}

uses
    dPlanCarrera,
    ZetaBuscador,
    ZetaCommonClasses,
    ZAccesosTress;

{ TEditTCompetencias }

procedure TEditTCompetencias.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CARRERA_CAT_TCOMPETEN;
     FirstControl := TC_CODIGO;
     //HelpContext:= H60617_Tipo_contrato;
end;

procedure TEditTCompetencias.Connect;
begin
     DataSource.DataSet := dmPlanCarrera.cdsTCompetencia;
end;

procedure TEditTCompetencias.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Competencia', 'TCOMPETE', 'TC_CODIGO', dmPlanCarrera.cdsTCompetencia );
end;

end.
