unit FEditNiveles;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Db, Grids, DBGrids, ComCtrls, ExtCtrls, DBCtrls, Buttons, Mask,
     ZBaseEdicionRenglon,
     ZetaEdit,
     ZetaDBGrid,
     ZetaNumero,
     ZetaSmartLists;

type
  TEditNiveles = class(TBaseEdicionRenglon)
    Label1: TLabel;
    Label2: TLabel;
    NP_DESCRIP: TDBEdit;
    NP_CODIGO: TZetaDBEdit;
    Label3: TLabel;
    NP_INGLES: TDBEdit;
    NP_TEXTO: TDBEdit;
    Label4: TLabel;
    Label5: TLabel;
    NP_NUMERO: TZetaDBNumero;
    Label6: TLabel;
    NP_ACTITUD: TDBEdit;
    NP_DETALLE: TDBMemo;
    Label7: TLabel;
    Panel3: TPanel;
    gbResumen: TGroupBox;
    ND_RESUMEN: TDBEdit;
    GroupBox1: TGroupBox;
    ND_DESCRIP: TDBMemo;
    SplitterDimensiones: TSplitter;
    procedure FormCreate(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure BBBorrarClick(Sender: TObject);
    procedure dsRenglonDataChange(Sender: TObject; Field: TField);
    procedure BBModificarClick(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect;override;
    procedure DoLookup; override;
    procedure HabilitaControles; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure DoCancelChanges; override;
  public
    { Public declarations }
  end;

var
  EditNiveles: TEditNiveles;

implementation

{$R *.DFM}

uses
    FSelDimensiones,
    DPlanCarrera,
    ZetaBuscador,
    ZetaCommonClasses,
    ZetaCommonLists,
    ZetaDialogo,
    ZBaseDlgModal,
    ZAccesosTress;

const
     K_COLUMNA_DIMENSION_CODIGO = 0;
     K_COLUMNA_DIMENSION_NOMBRE = 1;

{ TEditNiveles }

procedure TEditNiveles.FormCreate(Sender: TObject);
var
   i: Integer;
begin
     inherited;
     IndexDerechos := D_CARRERA_CAT_NIVEL;
     FirstControl := NP_CODIGO;
     with GridRenglones do
     begin
          for i := K_COLUMNA_DIMENSION_CODIGO to K_COLUMNA_DIMENSION_NOMBRE do
          begin
               with Columns[ i ] do
               begin
                    Color := clInfoBk;
                    Font.Color := clInfoText;
                    ReadOnly := True;
               end;
          end;
     end;
     //HelpContext:= H60617_Tipo_contrato;
end;

procedure TEditNiveles.Connect;
begin
     with dmPlanCarrera do
     begin
          {
          cdsDimensiones.Conectar;
          cdsNiveles.Conectar;
          cdsEditNiveles.Refrescar;
          }
          DataSource.DataSet := cdsEditNiveles;
          dsRenglon.DataSet := cdsNivelDimension;
     end;
end;

procedure TEditNiveles.DoLookup;
begin
     inherited DoLookup;
end;

procedure TEditNiveles.HabilitaControles;
begin
     inherited HabilitaControles;
end;

procedure TEditNiveles.Agregar;
begin
     inherited Agregar;
end;

procedure TEditNiveles.Borrar;
begin
     inherited Borrar;
end;

procedure TEditNiveles.Modificar;
begin
     inherited Modificar;
end;

procedure TEditNiveles.DoCancelChanges;
begin
     dmPlanCarrera.cdsNivelDimension.CancelUpdates;
     inherited DoCancelChanges;
end;

procedure TEditNiveles.dsRenglonDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     with dsRenglon.Dataset do
     begin
          ND_RESUMEN.Enabled := not IsEmpty;
          ND_DESCRIP.Enabled := not IsEmpty;
          Self.BBBorrar.Enabled := not IsEmpty;
          Self.BBModificar.Enabled := not IsEmpty;
     end;
end;

procedure TEditNiveles.BBAgregarClick(Sender: TObject);
begin
     inherited;
     ShowDlgModal( SelDimensiones, TSelDimensiones );
end;

procedure TEditNiveles.BBBorrarClick(Sender: TObject);
begin
     if ZetaDialogo.ZWarningConfirm( '� Atenci�n !', '� Desea borrar esta Dimensi�n ?', 0, mbCancel ) then
        inherited;
end;

procedure TEditNiveles.BBModificarClick(Sender: TObject);
begin
     inherited;
     with dmPlanCarrera.cdsPuestoDimension do
     begin
          if not IsEmpty then
             Edit;
     end;
     ActiveControl := ND_RESUMEN;
end;

end.
