unit DCliente;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBaseCliente, Db, DBClient,
{$ifdef DOS_CAPAS}
     DServerCatalogos,
     DServerGlobal,
     DServerSistema,
     DServerTablas,
     DServerReportes,
     DServerCalcNomina,
{$endif}
     ZetaCommonClasses,
     ZetaClientDataSet, DBasicoCliente;

type
  TdmCliente = class(TBaseCliente)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
{$ifdef DOS_CAPAS}
    FServerCatalogos: TdmServerCatalogos;
    FServerGlobal: TdmServerGlobal;
    FServerSistema: TdmServerSistema;
    FServerTablas: TdmServerTablas;
    FServerReportes : TdmServerReportes;
    FServerCalcNomina: TdmServerCalcNomina;
{$endif}
  public
    { Public declarations }
    procedure GetEmpleadosBuscados(const sPaterno, sMaterno, sNombre, sRFC, sNSS, sBanca: String; const oEmpresa: OleVariant; const oDataSet: TZetaClientDataSet);
    function GetEmpresas: OleVariant;
{$ifdef DOS_CAPAS}
    property ServerCatalogos: TdmServerCatalogos read FServerCatalogos;
    property ServerGlobal: TdmServerGlobal read FServerGlobal;
    property ServerSistema: TdmServerSistema read FServerSistema;
    property ServerTablas: TdmServerTablas read FServerTablas;
    property ServerReportes: TdmServerReportes read FServerReportes;
    property ServerCalcNomina: TdmServerCalcNomina read FServerCalcNomina;
{$endif}
    function GetDatosPeriodoActivo: TDatosPeriodo;

    procedure CargaActivosTodos(Parametros: TZetaParams); override;
    procedure CargaActivosIMSS( Parametros: TZetaParams );
    procedure CargaActivosPeriodo(Parametros: TZetaParams);
    procedure CargaActivosSistema(Parametros: TZetaParams);
    procedure InitActivosSistema;

  end;

var
  dmCliente: TdmCliente;

implementation

uses ZetaCommonLists,
     ZetaCommonTools;

{$R *.DFM}



procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
     inherited;
     ModoPlanCarrera := TRUE;
     TipoCompany := tc3Datos;

{$ifdef DOS_CAPAS}
     FServerCatalogos := TdmServerCatalogos.Create( Self );
     FServerGlobal := TdmServerGlobal.Create( Self );
     FServerSistema := TdmServerSistema.Create( Self );
     FServerTablas := TdmServerTablas.Create( Self );
     FServerReportes := TdmServerReportes.Create( Self );
     FServerCalcNomina := TdmServerCalcNomina.Create( Self );
{$endif}
end;

procedure TdmCliente.DataModuleDestroy(Sender: TObject);
begin
{$ifdef DOS_CAPAS}
     FreeAndNil( FServerCalcNomina );
     FreeAndNil( FServerReportes );
     FreeAndNil( FServerTablas );
     FreeAndNil( FServerSistema );
     FreeAndNil( FServerGlobal );
     FreeAndNil( FServerCatalogos );
{$endif}
  inherited;
end;

procedure TdmCliente.GetEmpleadosBuscados(const sPaterno, sMaterno, sNombre, sRFC, sNSS, sBanca: String; const oEmpresa: OleVariant; const oDataSet: TZetaClientDataSet);
begin
     with oDataSet do
     begin
          if ( FServidor = nil ) then
             Data := Servidor.GetEmpleadosBuscados( oEmpresa, sPaterno, sMaterno, sNombre, sRFC, sNSS, sBanca )
          else
             Data := FServidor.GetEmpleadosBuscados( oEmpresa, sPaterno, sMaterno, sNombre, sRFC, sNSS, sBanca );
     end;
end;

function TdmCliente.GetEmpresas: OleVariant;
begin
     Result := Servidor.GetCompanys( 0, Ord( tc3Datos ) );
end;

function TdmCliente.GetDatosPeriodoActivo: TDatosPeriodo;
begin
end;


procedure TdmCliente.CargaActivosTodos(Parametros: TZetaParams);
begin
     CargaActivosIMSS( Parametros );
     CargaActivosPeriodo( Parametros );
     CargaActivosSistema( Parametros );
end;

procedure TdmCliente.CargaActivosIMSS( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddString( 'RegistroPatronal', VACIO );
          AddInteger( 'IMSSYear', 0 );
          AddInteger( 'IMSSMes', 0 );
          AddInteger( 'IMSSTipo', 0 );
     end;
end;

procedure TdmCliente.CargaActivosPeriodo( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddInteger( 'Year', TheYear( FechaDefault ) );
          AddInteger( 'Tipo', 0 );
          AddInteger( 'Numero', 0 );
     end;
end;

procedure TdmCliente.CargaActivosSistema( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddDate( 'FechaAsistencia', FechaDefault );
          AddDate( 'FechaDefault', FechaDefault );
          AddInteger( 'YearDefault', TheYear( FechaDefault ) );
          AddInteger( 'EmpleadoActivo', 0 );
          AddString( 'NombreUsuario', GetDatosUsuarioActivo.Nombre );
          AddString( 'CodigoEmpresa', GetDatosEmpresaActiva.Codigo );
     end;
end;

procedure TdmCliente.InitActivosSistema;
begin
     FechaDefault := Now;
end;

end.
