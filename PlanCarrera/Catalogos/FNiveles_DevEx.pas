unit FNiveles_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, ZBaseGridLectura_DevEx, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls,
  Vcl.StdCtrls, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ZetaCXGrid;

type
  TNiveles_DevEx = class(TBaseGridLectura_DevEx)
    ZetaDBGrid1: TZetaDBGrid;
    NP_CODIGO: TcxGridDBColumn;
    NP_DESCRIP: TcxGridDBColumn;
    NP_ACTITUD: TcxGridDBColumn;
    NP_INGLES: TcxGridDBColumn;
    NP_NUMERO: TcxGridDBColumn;
    NP_TEXTO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  Niveles_DevEx: TNiveles_DevEx;

implementation

{$R *.DFM}

uses
    dPlanCarrera,
    ZetaBuscador_DevEx,
    ZetaCommonClasses;

{ TNiveles }
procedure TNiveles_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H_Catalog_Niveles;
end;

procedure TNiveles_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  CreaColumaSumatoria(ZetaDbGridDBTableView.Columns[0], 0 ,'' , SkCount );
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := False;
  ApplyMinWidth;
  ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TNiveles_DevEx.Agregar;
begin
     dmPlanCarrera.cdsNiveles.Agregar;
     DoBestFit;
end;

procedure TNiveles_DevEx.Borrar;
begin
     dmPlanCarrera.cdsNiveles.Borrar;
     DoBestFit;
end;

procedure TNiveles_DevEx.Connect;
begin
     with dmPlanCarrera do
     begin
          cdsNiveles.Conectar;
          DataSource.DataSet := cdsNiveles;
     end;
end;

procedure TNiveles_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Niveles', 'NIV_PTO', 'NP_CODIGO', dmPlanCarrera.cdsNiveles );
end;

procedure TNiveles_DevEx.ImprimirForma;
begin
  inherited;
end;

procedure TNiveles_DevEx.Modificar;
begin
     dmPlanCarrera.cdsNiveles.Modificar;
     DoBestFit;
end;

function TNiveles_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := True;
end;

function TNiveles_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := True;
end;

procedure TNiveles_DevEx.Refresh;
begin
     dmPlanCarrera.cdsNiveles.Refrescar;
end;

end.
