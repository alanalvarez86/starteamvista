unit FEditCompetencias;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComCtrls, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask, Db, Grids, DBGrids,
     ZBaseEdicionRenglon,
     ZetaDBGrid,
     ZetaKeyLookup,
     ZetaEdit,
     ZetaNumero,
     ZetaSmartLists;

type
  TEditCompetencias = class(TBaseEdicionRenglon)
    Label1: TLabel;
    CM_CODIGO: TZetaDBEdit;
    Label2: TLabel;
    CM_DESCRIP: TDBEdit;
    dsCalifica: TDataSource;
    Label3: TLabel;
    CM_INGLES: TDBEdit;
    Label6: TLabel;
    Label7: TLabel;
    CM_DETALLE: TDBMemo;
    Label4: TLabel;
    TC_CODIGO: TZetaDBKeyLookup;
    Label5: TLabel;
    CM_TEXTO: TDBEdit;
    CM_NUMERO: TZetaDBNumero;
    Criterios: TTabSheet;
    GridCriterios: TZetaDBGrid;
    Panel5: TPanel;
    bbAgregarCriterios: TBitBtn;
    bbBorrarCriterios: TBitBtn;
    bbModificarCriterios: TBitBtn;
    Panel4: TPanel;
    Subir: TZetaSmartListsButton;
    Bajar: TZetaSmartListsButton;
    GroupBox2: TGroupBox;
    MC_DESCRIP: TDBMemo;
    GroupBox1: TGroupBox;
    CM_OBSERVA: TDBMemo;
    SplitterAcciones: TSplitter;
    SplitterCriterios: TSplitter;
    procedure FormCreate(Sender: TObject);
    procedure bbAgregarCriteriosClick(Sender: TObject);
    procedure bbBorrarCriteriosClick(Sender: TObject);
    procedure bbModificarCriteriosClick(Sender: TObject);
    procedure SubirClick(Sender: TObject);
    procedure BajarClick(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure BBBorrarClick(Sender: TObject);
    procedure BBModificarClick(Sender: TObject);
    procedure dsRenglonDataChange(Sender: TObject; Field: TField);
    procedure dsCalificaDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
    function SubeBajaAcciones( const lSubir: Boolean ): Boolean;
  protected
    { Protected declarations }
    procedure Connect;override;
    procedure DoLookup; override;
    procedure HabilitaControles; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure DoCancelChanges; override;
  public
    { Public declarations }
  end;

var
  EditCompetencias: TEditCompetencias;

implementation

{$R *.DFM}

uses
    FSelCalificacionesCompetencia,
    FSelAccionesCompetencia,    
    dPlanCarrera,
    ZetaBuscador,
    ZetaCommonClasses,
    ZetaCommonLists,
    ZetaDialogo,
    ZBaseDlgModal,
    ZAccesosTress;

const
     K_COLUMNA_ACCION_ORDEN = 0;
     K_COLUMNA_ACCION_CODIGO = 1;
     K_COLUMNA_ACCION_NOMBRE = 2;
     K_COLUMNA_CRITERIO_CODIGO = 0;
     K_COLUMNA_CRITERIO_NOMBRE = 1;

{ TEditHabilidades }

procedure TEditCompetencias.FormCreate(Sender: TObject);
var
   i: Integer;
begin
     inherited;
     IndexDerechos := D_CARRERA_CAT_COMPETEN;
     FirstControl := CM_CODIGO;
     //HelpContext:= H60617_Tipo_contrato;
     TC_CODIGO.LookupDataset := dmPlanCarrera.cdsTCompetencia;
     with GridRenglones do
     begin
          for i := K_COLUMNA_ACCION_ORDEN to K_COLUMNA_ACCION_NOMBRE do
          begin
               with Columns[ i ] do
               begin
                    Color := clInfoBk;
                    Font.Color := clInfoText;
                    ReadOnly := True;
               end;
          end;
          Self.GridCriterios.Options := Options;
     end;
     with GridCriterios do
     begin
          for i := K_COLUMNA_CRITERIO_CODIGO to K_COLUMNA_CRITERIO_NOMBRE do
          begin
               with Columns[ i ] do
               begin
                    Color := clInfoBk;
                    Font.Color := clInfoText;
                    ReadOnly := True;
               end;
          end;
     end;
end;

procedure TEditCompetencias.Connect;
begin
     with dmPlanCarrera do
     begin
          {
          cdsEditCompetencia.Refrescar;
          }
          DataSource.DataSet := cdsEditCompetencia;
          dsRenglon.DataSet := cdsCompetenciaMapa;
          dsCalifica.DataSet := cdsCompetenciaCalifica;
     end;
end;

procedure TEditCompetencias.DoLookup;
begin
     inherited DoLookup;
end;

procedure TEditCompetencias.HabilitaControles;
begin
     inherited HabilitaControles;
end;

procedure TEditCompetencias.Modificar;
begin
     inherited Modificar;
end;

procedure TEditCompetencias.Agregar;
begin
     inherited Agregar;
end;

procedure TEditCompetencias.Borrar;
begin
     inherited Borrar;
end;

procedure TEditCompetencias.DoCancelChanges;
begin
     with dmPlanCarrera do
     begin
          cdsCompetenciaMapa.CancelUpdates;
          cdsCompetenciaCalifica.CancelUpdates;
     end;
     inherited DoCancelChanges;
end;

{ GridRenglones - Acciones por Competencia }

procedure TEditCompetencias.dsRenglonDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     with dsRenglon.Dataset do
     begin
          Self.Subir.Enabled := not IsEmpty;
          Self.Bajar.Enabled := not IsEmpty;
          CM_OBSERVA.Enabled := not IsEmpty;
          Self.BBBorrar.Enabled := not IsEmpty;
          Self.BBModificar.Enabled := not IsEmpty;
     end;
end;

procedure TEditCompetencias.SubirClick(Sender: TObject);
begin
     if not SubeBajaAcciones( True ) then
        ZetaDialogo.ZError( 'Competencias', '� Es El Inicio de la Lista de Acciones !', 0 );
end;

procedure TEditCompetencias.BajarClick(Sender: TObject);
begin
     if not SubeBajaAcciones( False ) then
        ZetaDialogo.ZError( 'Competencias', '� Es El Final de la Lista de Acciones !', 0 );
end;

function TEditCompetencias.SubeBajaAcciones( const lSubir: Boolean ): Boolean;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        Result := dmPlanCarrera.SubeBajaAcciones( lSubir );
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TEditCompetencias.BBAgregarClick(Sender: TObject);
begin
     inherited;
     ShowDlgModal( SelAccionesCompetencia, TSelAccionesCompetencia );
end;

procedure TEditCompetencias.BBBorrarClick(Sender: TObject);
begin
     if ( Zetadialogo.ZWarningConfirm( 'Competencias', '� Seguro de Borrar el Registro ?', 0, mbCancel ) ) then
        inherited;
end;

procedure TEditCompetencias.BBModificarClick(Sender: TObject);
begin
     inherited;
     with dmPlanCarrera.cdsCompetenciaMapa do
     begin
          if not IsEmpty then
             Edit;
     end;
     ActiveControl := CM_OBSERVA;
end;

{ GridCriterios - Criterios por Competencia }

procedure TEditCompetencias.dsCalificaDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     with dsCalifica.Dataset do
     begin
          MC_DESCRIP.Enabled := not IsEmpty;
          Self.bbBorrarCriterios.Enabled := not IsEmpty;
          Self.bbModificarCriterios.Enabled := not IsEmpty;
     end;
end;

procedure TEditCompetencias.bbAgregarCriteriosClick(Sender: TObject);
begin
     inherited;
     ShowDlgModal( SelCalificacionesCompetencia, TSelCalificacionesCompetencia );
end;

procedure TEditCompetencias.bbBorrarCriteriosClick(Sender: TObject);
begin
     if ZetaDialogo.ZWarningConfirm( '� Atenci�n !', '� Desea borrar este Criterio ?', 0, mbCancel ) then
     begin
          inherited;
          with dmPlanCarrera.cdsCompetenciaCalifica  do
          begin
               if not IsEmpty then
                  Delete;
          end;
     end;
end;
procedure TEditCompetencias.bbModificarCriteriosClick(Sender: TObject);
begin
     inherited;
     with dmPlanCarrera.cdsCompetenciaCalifica do
     begin
          if not IsEmpty then
             Edit;
     end;
     ActiveControl := MC_DESCRIP;
end;

end.
