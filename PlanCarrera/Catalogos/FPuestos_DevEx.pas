unit FPuestos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, ZBaseGridLectura_DevEx, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls,
  Vcl.StdCtrls, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ZetaCXGrid;

type
  TPuestos_DevEx = class(TBaseGridLectura_DevEx)
    ZetaDBGrid1: TZetaDBGrid;
    PU_CODIGO: TcxGridDBColumn;
    PU_DESCRIP: TcxGridDBColumn;
    FP_DESCRIP: TcxGridDBColumn;
    NP_DESCRIP: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    {procedure Agregar; override;
    procedure Borrar; override;}
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  Puestos_DevEx: TPuestos_DevEx;

implementation

{$R *.DFM}

uses
    dPlanCarrera,
    ZetaBuscador_DevEx,
    ZetaCommonClasses;

{ TPuestos }
procedure TPuestos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H_Catalogo_Puestos;
end;

procedure TPuestos_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  CreaColumaSumatoria(ZetaDbGridDBTableView.Columns[0], 0 ,'' , SkCount );
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := False;
  ApplyMinWidth;
  ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TPuestos_DevEx.Connect;
begin
     with dmPlanCarrera do
     begin
          cdsFamilias.Conectar;
          cdsNiveles.Conectar;
          cdsPuestos.Conectar;
          DataSource.DataSet := cdsPuestos;
     end;
end;

procedure TPuestos_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Puestos', 'PUESTO', 'PU_CODIGO', dmPlanCarrera.cdsPuestos );
end;

procedure TPuestos_DevEx.ImprimirForma;
begin
  inherited;

end;

procedure TPuestos_DevEx.Modificar;
begin
     dmPlanCarrera.cdsPuestos.Modificar;
     DoBestFit;
end;

function TPuestos_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se Pueden Agregar Registros a este Cat�logo';
     Result := False;
end;

function TPuestos_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se Pueden Borrar Registros a este Cat�logo';
     Result := False;
end;

procedure TPuestos_DevEx.Refresh;
begin
     dmPlanCarrera.cdsPuestos.Refrescar;
end;

end.
