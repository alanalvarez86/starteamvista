unit FFamilias;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls;

type
  TFamilias = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  Familias: TFamilias;

implementation

{$R *.DFM}

uses
    dPlanCarrera,
    ZetaBuscador,
    ZetaCommonClasses;

{ TFamilias }
procedure TFamilias.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     //HelpContext := H00005_Catalogo_de_Medicinas;
end;

procedure TFamilias.Agregar;
begin
     dmPlanCarrera.cdsFamilias.Agregar;
end;

procedure TFamilias.Borrar;
begin
     dmPlanCarrera.cdsFamilias.Borrar;
end;

procedure TFamilias.Connect;
begin
     with dmPlanCarrera do
     begin
          cdsFamilias.Conectar;
          DataSource.DataSet := cdsFamilias;
     end;
end;

procedure TFamilias.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Familias', 'FAM_PTO', 'FP_CODIGO', dmPlanCarrera.cdsFamilias );
end;

procedure TFamilias.ImprimirForma;
begin
  inherited;

end;

procedure TFamilias.Modificar;
begin
     dmPlanCarrera.cdsFamilias.Modificar;
end;

function TFamilias.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := True;
end;

function TFamilias.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := True;
end;

procedure TFamilias.Refresh;
begin
     dmPlanCarrera.cdsFamilias.Refrescar;
end;

end.
