/*Store Procedure para obtener el porcentaje de cumplimiento de un candidato*/

Create PROCEDURE SP_CUMPLE_PUESTO(
  EMPLEADO INTEGER,
  PUESTO CHAR(6)) 
RETURNS (
  CUMPLE INTEGER
) AS 
  declare variable Total NUMERIC( 15, 2 );
  declare variable ORDEN1 INTEGER;
  declare variable ORDEN2 INTEGER;
  declare variable CUANTOS INTEGER;
  declare variable CONTADOR INTEGER;
begin     
     CONTADOR = 0;
     CUANTOS = 0;
     for
     	select C1.CA_ORDEN, C2.CA_ORDEN
	from COMP_PTO
	LEFT OUTER JOIN COMPETEN ON COMPETEN.CM_CODIGO = COMP_PTO.CM_CODIGO
	LEFT OUTER JOIN EMP_COMP ON EMP_COMP.CM_CODIGO = COMP_PTO.CM_CODIGO AND EMP_COMP.CB_CODIGO = :EMPLEADO
	LEFT OUTER JOIN CALIFICA C2 ON C2.CA_CODIGO  = COMP_PTO.CA_CODIGO
	LEFT OUTER JOIN CALIFICA C1 ON C1.CA_CODIGO = EMP_COMP.CA_CODIGO
	where COMP_PTO.PU_CODIGO = :PUESTO
	into ORDEN1, ORDEN2 do
     begin
        if ( ORDEN2 IS NOT NULL ) then
        begin
		if ( ORDEN1 >= ORDEN2 ) then
		begin
			CONTADOR = CONTADOR + 1;
		end
        	CUANTOS = CUANTOS + 1;
	end
     end
     CUMPLE = CAST( ( ( CONTADOR / CUANTOS ) * 100 ) as INTEGER ) ; 
     suspend;
end
GO
CREATE FUNCTION SP_STR_TO_BOOL( @Valor INTEGER )
RETURNS CHAR(1)
AS
BEGIN
	if @Valor < 0
		Return 'N'
	Return 'S'
END

