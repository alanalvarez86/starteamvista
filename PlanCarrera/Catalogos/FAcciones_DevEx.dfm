inherited Acciones_DevEx: TAcciones_DevEx
  Left = 193
  Top = 232
  Caption = 'Acciones'
  ClientHeight = 220
  ClientWidth = 765
  ExplicitWidth = 765
  ExplicitHeight = 220
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 765
    ExplicitWidth = 765
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 506
      ExplicitWidth = 506
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 500
        ExplicitLeft = 3
      end
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 765
    Height = 201
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'AN_CODIGO'
        Title.Caption = 'C'#243'digo'
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AN_NOMBRE'
        Title.Caption = 'Nombre'
        Width = 250
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AN_CLASE'
        Title.Caption = 'Tipo'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AN_TIP_MAT'
        Title.Caption = 'Material Did'#225'ctico'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AN_DIAS'
        Title.Caption = 'Duraci'#243'n en D'#237'as'
        Width = 100
        Visible = True
      end>
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 765
    Height = 201
    ExplicitWidth = 765
    ExplicitHeight = 201
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object AN_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'AN_CODIGO'
      end
      object AN_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'AN_NOMBRE'
      end
      object AN_CLASE: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'AN_CLASE'
      end
      object AN_TIP_MAT: TcxGridDBColumn
        Caption = 'Material Did'#225'ctico'
        DataBinding.FieldName = 'AN_TIP_MAT'
      end
      object AN_DIAS: TcxGridDBColumn
        Caption = 'Duraci'#243'n en D'#237'as'
        DataBinding.FieldName = 'AN_DIAS'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
