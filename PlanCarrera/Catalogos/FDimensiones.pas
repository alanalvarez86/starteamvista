unit FDimensiones;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls;

type
  TDimensiones = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  Dimensiones: TDimensiones;

implementation

{$R *.DFM}

uses
    dPlanCarrera,
    ZetaBuscador,
    ZetaCommonClasses;

{ TDimensiones }
procedure TDimensiones.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     //HelpContext := H00005_Catalogo_de_Medicinas;
end;

procedure TDimensiones.Agregar;
begin
     dmPlanCarrera.cdsDimensiones.Agregar;
end;

procedure TDimensiones.Borrar;
begin
     dmPlanCarrera.cdsDimensiones.Borrar;
end;

procedure TDimensiones.Connect;
begin
     with dmPlanCarrera do
     begin
          //cdsTCompetencia.Conectar;
          cdsDimensiones.conectar;
          DataSource.DataSet := cdsDimensiones;
     end;
end;

procedure TDimensiones.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Dimensiones', 'DIMENSIO', 'DM_CODIGO', dmPlanCarrera.cdsDimensiones );
end;

procedure TDimensiones.ImprimirForma;
begin
  inherited;

end;

procedure TDimensiones.Modificar;
begin
     dmPlanCarrera.cdsDimensiones.Modificar;
end;

function TDimensiones.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := True;
end;

function TDimensiones.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := True;
end;

procedure TDimensiones.Refresh;
begin
     dmPlanCarrera.cdsDimensiones.Refrescar;
end;

end.
