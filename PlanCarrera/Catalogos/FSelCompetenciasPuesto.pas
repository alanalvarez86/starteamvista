unit FSelCompetenciasPuesto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal, StdCtrls, CheckLst, Db, Buttons, ExtCtrls;

type
  TSelCompetenciasPuesto = class(TZetaDlgModal)
    Lista: TCheckListBox;
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
    procedure Inicializa;
    procedure GrabaSeleccionados;
    function  YaExistia( const sCodigo : String ) : Boolean;
  public
    { Public declarations }
  end;

var
  SelCompetenciasPuesto: TSelCompetenciasPuesto;

implementation

{$R *.DFM}

uses
    dPlanCarrera;

procedure TSelCompetenciasPuesto.FormShow(Sender: TObject);
begin
     inherited;
     Inicializa;
end;

procedure TSelCompetenciasPuesto.OKClick(Sender: TObject);
begin
     inherited;
     GrabaSeleccionados;
end;

procedure TSelCompetenciasPuesto.GrabaSeleccionados;
var
   i: Integer;
   sCodigo, sDescripcion: String;
begin
     with dmPlanCarrera.cdsCompetenciaPuesto do
     begin
          DisableControls;
          try
             with Lista do
             begin
                  for i := 0 to Items.Count - 1 do
                  begin
                       sCodigo := Items.Names[ i ];
                       sDescripcion := Trim( Items.Values[ sCodigo ] );
                       sCodigo := Trim( sCodigo );
                       if Checked[ i ] then
                       begin
                            if not YaExistia( sCodigo ) then
                            begin
                                 Append;
                                 FieldByName( 'CM_CODIGO' ).AsString := sCodigo;
                                 FieldByName( 'CM_DESCRIP' ).AsString := sDescripcion;
                                 Post;
                            end;
                       end
                       else
                           if YaExistia( sCodigo ) then
                              Delete;
                  end;
             end;
          finally
                 EnableControls;
          end;
     end;
end;

procedure TSelCompetenciasPuesto.Inicializa;
var
   iPosicion : integer;
   sCodigo: string;
begin
     iPosicion := 0;
     with dmPlanCarrera.cdsCompetencia do
     begin
          Conectar;
          First;
          Lista.Clear;
          while not Eof do
          begin
               sCodigo := FieldByName( 'CM_CODIGO' ).AsString;
               Lista.Items.Add( sCodigo + ' = ' + FieldByName( 'CM_DESCRIP' ).AsString );
               if ( YaExistia( sCodigo ) ) then
                  Lista.Checked[ iPosicion ] := TRUE;
               iPosicion := iPosicion + 1;
               Next;
          end;
     end;
end;

function TSelCompetenciasPuesto.YaExistia(const sCodigo: String): Boolean;
begin
     with dmPlanCarrera do
     begin
          Result := YaExistia( cdsCompetenciaPuesto, 'CM_CODIGO', sCodigo );
     end;
end;

end.
