unit FAcciones_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls,
  ZBaseConsulta, Vcl.StdCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList,
  Vcl.ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TAcciones_DevEx = class(TBaseGridLectura_DevEx)
    ZetaDBGrid1: TZetaDBGrid;
    AN_CODIGO: TcxGridDBColumn;
    AN_NOMBRE: TcxGridDBColumn;
    AN_CLASE: TcxGridDBColumn;
    AN_TIP_MAT: TcxGridDBColumn;
    AN_DIAS: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  Acciones_DevEx: TAcciones_DevEx;

implementation

{$R *.DFM}

uses
    dPlanCarrera,
    ZetaBuscador_DevEx,
    ZetaCommonClasses;

{ TAcciones }
procedure TAcciones_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H_Catalogo_Acciones;
end;

procedure TAcciones_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  CreaColumaSumatoria(ZetaDbGridDBTableView.Columns[0], 0 ,'' , SkCount );
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := False;
  ApplyMinWidth;
  ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TAcciones_DevEx.Agregar;
begin
     dmPlanCarrera.cdsAcciones.Agregar;
      DoBestFit;
end;

procedure TAcciones_DevEx.Borrar;
begin
     dmPlanCarrera.cdsAcciones.Borrar;
      DoBestFit;
end;

procedure TAcciones_DevEx.Connect;
begin
     with dmPlanCarrera do
     begin
          cdsAcciones.Conectar;
          DataSource.DataSet := cdsAcciones;
     end;
end;

procedure TAcciones_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Acciones', 'ACCION', 'AN_CODIGO', dmPlanCarrera.cdsAcciones );
end;

procedure TAcciones_DevEx.ImprimirForma;
begin
  inherited;

end;

procedure TAcciones_DevEx.Modificar;
begin
     dmPlanCarrera.cdsAcciones.Modificar;
      DoBestFit;
end;

function TAcciones_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := True;
end;

function TAcciones_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := True;
end;

procedure TAcciones_DevEx.Refresh;
begin
     dmPlanCarrera.cdsAcciones.Refrescar;
end;

end.
