unit FGlobalEvaluacion_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGlobal, ZBaseGlobal_DevEx, StdCtrls, Buttons, ExtCtrls, Mask, ZetaFecha,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, Vcl.ImgList,
  cxButtons;

type
  TGlobalEvaluacion_DevEx = class(TBaseGlobal_DevEx)
    GroupBox1: TGroupBox;
    lblDe: TLabel;
    lblHasta: TLabel;
    zfFin: TZetaFecha;
    zfInicio: TZetaFecha;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  GlobalEvaluacion_DevEx: TGlobalEvaluacion_DevEx;

implementation

uses ZGlobalTress,
     ZAccesosTress,
     ZetaCommonTools,
     ZetaDialogo,
     ZetaCommonClasses;
{$R *.DFM}

procedure TGlobalEvaluacion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos          := D_CARRERA_GLOBALES;
     zfInicio.Tag           := K_GLOBAL_CARRERA_FECHA_EVAL_INICIO;
     zfFin.Tag              := K_GLOBAL_CARRERA_FECHA_EVAL_FINAL;
     HelpContext            := H_Fechas_Carrera;
end;

procedure TGlobalEvaluacion_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     if( strVacio( zfInicio.Texto ) )then
     begin
          zfInicio.Valor := Date;
          zfFin.Valor := Date;
     end;
end;

procedure TGlobalEvaluacion_DevEx.OKClick(Sender: TObject);
begin
     if( zfFin.Valor >= zfInicio.Valor )then
         inherited
     else
         ZetaDialogo.ZInformation(self.Caption,'El Valor de la Fecha Final Debe de Ser Mayor � Igual a la Fecha De Inicio', 0 ); 
end;

end.
