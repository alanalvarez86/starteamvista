inherited TipoCompetencias: TTipoCompetencias
  Left = 227
  Top = 193
  Caption = 'Tipo de Competencia'
  ClientHeight = 287
  ClientWidth = 617
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 617
    inherited ValorActivo2: TPanel
      Width = 358
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 617
    Height = 268
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'TC_CODIGO'
        Title.Caption = 'C�digo'
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TC_DESCRIP'
        Title.Caption = 'Descripci�n'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TC_TIPO'
        Title.Caption = 'Tipo'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TC_INGLES'
        Title.Caption = 'Ingl�s'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TC_NUMERO'
        Title.Caption = 'N�mero'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TC_TEXTO'
        Title.Caption = 'Texto'
        Width = 200
        Visible = True
      end>
  end
end
