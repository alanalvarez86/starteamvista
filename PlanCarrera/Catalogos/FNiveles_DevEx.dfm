inherited Niveles_DevEx: TNiveles_DevEx
  Left = 220
  Top = 163
  Caption = 'Niveles'
  ClientHeight = 271
  ClientWidth = 647
  ExplicitWidth = 647
  ExplicitHeight = 271
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 647
    ExplicitWidth = 647
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 388
      ExplicitWidth = 388
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 382
        ExplicitLeft = 3
      end
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 647
    Height = 252
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'NP_CODIGO'
        Title.Caption = 'C'#243'digo'
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NP_DESCRIP'
        Title.Caption = 'Descripci'#243'n'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NP_ACTITUD'
        Title.Caption = 'Actitud'
        Width = 250
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NP_INGLES'
        Title.Caption = 'Ingl'#233's'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NP_NUMERO'
        Title.Caption = 'N'#250'mero'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NP_TEXTO'
        Title.Caption = 'Texto'
        Width = 200
        Visible = True
      end>
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 647
    Height = 252
    ExplicitWidth = 647
    ExplicitHeight = 252
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object NP_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'NP_CODIGO'
      end
      object NP_DESCRIP: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'NP_DESCRIP'
      end
      object NP_ACTITUD: TcxGridDBColumn
        Caption = 'Actitud'
        DataBinding.FieldName = 'NP_ACTITUD'
      end
      object NP_INGLES: TcxGridDBColumn
        Caption = 'Ingl'#233's'
        DataBinding.FieldName = 'NP_INGLES'
      end
      object NP_NUMERO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'NP_NUMERO'
      end
      object NP_TEXTO: TcxGridDBColumn
        Caption = 'Texto'
        DataBinding.FieldName = 'NP_TEXTO'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
