unit FEditDimensiones;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, ZetaNumero, Mask, DBCtrls, StdCtrls, ZetaEdit, Db,
  ExtCtrls, Buttons, ZetaSmartLists;

type
  TEditDimensiones = class(TBaseEdicion)
    Label1: TLabel;
    DM_CODIGO: TZetaDBEdit;
    Label2: TLabel;
    DM_DESCRIP: TDBEdit;
    Label3: TLabel;
    DM_INGLES: TDBEdit;
    Label5: TLabel;
    DM_NUMERO: TZetaDBNumero;
    Label6: TLabel;
    DM_TEXTO: TDBEdit;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  EditDimensiones: TEditDimensiones;

implementation

{$R *.DFM}

uses
    dPlanCarrera,
    ZetaBuscador,
    ZetaCommonClasses,
    ZAccesosTress;

{ TEditDimensiones }

procedure TEditDimensiones.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CARRERA_CAT_DIMENSIO;
     FirstControl := DM_CODIGO;
     //HelpContext:= H60617_Tipo_contrato;
end;

procedure TEditDimensiones.Connect;
begin
     DataSource.DataSet := dmPlanCarrera.cdsDimensiones;
end;

procedure TEditDimensiones.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Dimensiones', 'DIMENSIO', 'DM_CODIGO', dmPlanCarrera.cdsDimensiones );
end;

end.
