object dmCatalogos: TdmCatalogos
  OldCreateOrder = False
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  object cdsCursos: TZetaLookupDataSet
    Tag = 2
    Aggregates = <>
    IndexFieldNames = 'CU_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsCursosAlAdquirirDatos
    UsaCache = True
    LookupName = 'Cat'#225'logo de Cursos'
    LookupDescriptionField = 'CU_NOMBRE'
    LookupKeyField = 'CU_CODIGO'
    OnGetRights = ReadOnlyGetRights
    Left = 72
    Top = 8
  end
  object cdsConceptosLookup: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    OnGetRights = ReadOnlyGetRights
    Left = 188
    Top = 9
  end
  object cdsCondiciones: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'QU_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsCondicionesAlAdquirirDatos
    LookupName = 'Cat'#225'logo de Condiciones'
    LookupDescriptionField = 'QU_DESCRIP'
    LookupKeyField = 'QU_CODIGO'
    OnGetRights = ReadOnlyGetRights
    Left = 188
    Top = 74
  end
  object cdsNomParamLookUp2: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    OnGetRights = ReadOnlyGetRights
    Left = 72
    Top = 72
  end
end
