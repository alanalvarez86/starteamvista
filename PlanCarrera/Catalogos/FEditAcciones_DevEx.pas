unit FEditAcciones_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, Grids, DBGrids, ComCtrls, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask,
     ZBaseEdicionRenglon_DevEx,
     ZetaDBGrid,
     ZetaEdit,
     ZetaKeyLookup,
     ZetaKeyCombo,
     ZetaNumero,
     ZetaSmartLists, ZBaseEdicionRenglon, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, cxControls, dxBarBuiltInMenu, dxBarExtItems,
  dxBar, cxClasses, Vcl.ImgList, cxPC, cxNavigator, cxDBNavigator, cxButtons,
  ZetaKeyLookup_DevEx, cxContainer, cxEdit, cxTextEdit, cxMemo, cxDBEdit;

type
  TEditAcciones_DevEx = class(TBaseEdicionRenglon_DevEx)
    Label1: TLabel;
    AN_CODIGO: TZetaDBEdit;
    AN_NOMBRE: TDBEdit;
    Label2: TLabel;
    Label3: TLabel;
    AN_INGLES: TDBEdit;
    AN_NUMERO: TZetaDBNumero;
    Label5: TLabel;
    Label6: TLabel;
    AN_TEXTO: TDBEdit;
    AN_CLASE: TZetaDBKeyCombo;
    Label4: TLabel;
    lblCurso: TLabel;
    CU_CODIGO: TZetaDBKeyLookup_DevEx;
    lblMat: TLabel;
    Label10: TLabel;
    AN_DIAS: TZetaDBNumero;
    AN_URL: TDBEdit;
    Label11: TLabel;
    Label8: TLabel;
    ArchivoSeek: TSpeedButton;
    AN_TIP_MAT: TDBComboBox;
    AN_DETALLE: TcxDBMemo;
    procedure FormCreate(Sender: TObject);
    procedure BBBorrarClick(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure AN_CLASEChange(Sender: TObject);
    procedure dsRenglonDataChange(Sender: TObject; Field: TField);
    procedure BBModificarClick(Sender: TObject);
  private
    { Private declarations }
    procedure ActivaControles;
  protected
    { Protected declarations }
    procedure Connect;override;
    procedure DoLookup; override;
    procedure HabilitaControles; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure DoCancelChanges; override;
  public
    { Public declarations }
  end;

var
  EditAcciones_DevEx: TEditAcciones_DevEx;

implementation

uses DPlanCarrera,
     DCatalogos,
     FSelCompetenciasAccion_DevEx,
     ZetaBuscador,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaDialogo,
     ZBaseDlgModal_DevEx,
     ZAccesosTress;

{$R *.DFM}

const
     K_COLUMNA_COMPETENCIA_CODIGO = 0;
     K_COLUMNA_COMPETENCIA_DESCRIPCION = 1;
     K_COLUMNA_COMPETENCIA_OBSERVACIONES = 2;

{ TEditAcciones  }

procedure TEditAcciones_DevEx.FormCreate(Sender: TObject);
var
   i: Integer;
begin
     inherited;
     IndexDerechos := D_CARRERA_CAT_ACCION;
     FirstControl := AN_CODIGO;
     CU_CODIGO.LookupDataset := dmCatalogos.cdsCursos;
     with GridRenglones do
     begin
          for i := K_COLUMNA_COMPETENCIA_CODIGO to K_COLUMNA_COMPETENCIA_OBSERVACIONES do
          begin
               with Columns[ i ] do
               begin
                    Color := clInfoBk;
                    Font.Color := clInfoText;
                    ReadOnly := True;
               end;
          end;
     end;
     HelpContext := H_Catalogo_Acciones;
end;

procedure TEditAcciones_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     ActivaControles;
end;

procedure TEditAcciones_DevEx.Connect;
begin
     dmCatalogos.cdsCursos.Conectar;
     with dmPlanCarrera do
     begin
          DataSource.DataSet := cdsEditAcciones;
          dsRenglon.DataSet := cdsCompetenciaAccion;
     end;
end;

procedure TEditAcciones_DevEx.Agregar;
begin
     inherited Agregar;
end;

procedure TEditAcciones_DevEx.Borrar;
begin
     inherited Borrar;
end;

procedure TEditAcciones_DevEx.Modificar;
begin
     inherited Modificar;
end;

procedure TEditAcciones_DevEx.DoCancelChanges;
begin
     dmPlanCarrera.cdsCompetenciaAccion.CancelUpdates;
     inherited DoCancelChanges;
end;

procedure TEditAcciones_DevEx.DoLookup;
begin
     inherited DoLookup;
end;

procedure TEditAcciones_DevEx.HabilitaControles;
begin
     inherited HabilitaControles;
end;

procedure TEditAcciones_DevEx.ActivaControles;
var
   iTipo: integer;
begin
     with dmPlanCarrera.cdsAcciones do
     begin
          iTipo := AN_CLASE.ItemIndex; // FieldByName( 'AN_CLASE' ).AsInteger;
          case eClaseAcciones( iTipo ) of
               eaCurso:
               begin
                    CU_CODIGO.Enabled := True;
                    lblCurso.Enabled := CU_CODIGO.Enabled;
                    AN_TIP_MAT.Enabled := False;
                    lblMat.Enabled := AN_TIP_MAT.Enabled;
               end;
               eaDidactico:
               begin
                    AN_TIP_MAT.Enabled := True;
                    lblMat.Enabled := AN_TIP_MAT.Enabled;
                    CU_CODIGO.Enabled := False;
                    lblCurso.Enabled := CU_CODIGO.Enabled;
               end;
               eaActividad, eaOtra:
               begin
                    AN_TIP_MAT.Enabled := False;
                    lblMat.Enabled := AN_TIP_MAT.Enabled;
                    CU_CODIGO.Enabled := False;
                    lblCurso.Enabled := CU_CODIGO.Enabled;
               end;
          end;
     end;
end;

procedure TEditAcciones_DevEx.DataSourceDataChange(Sender: TObject;  Field: TField);
begin
     inherited;
     if ( Field = nil ) or ( Field.FieldName = 'AN_CLASE' ) then
        ActivaControles;
end;

procedure TEditAcciones_DevEx.AN_CLASEChange(Sender: TObject);
begin
     inherited;
     ActivaControles;
end;

procedure TEditAcciones_DevEx.dsRenglonDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     with dsRenglon.Dataset do
     begin
          //Self.GridRenglones.Columns[ K_COLUMNA_COMPETENCIA_OBSERVACIONES ].ReadOnly := IsEmpty;
          Self.BBBorrar_DevEx.Enabled := not IsEmpty;
          Self.BBModificar_DevEx.Enabled := not IsEmpty;
     end;
end;

procedure TEditAcciones_DevEx.BBAgregarClick(Sender: TObject);
begin
     inherited;
     ShowDlgModal( SelCompetenciaAccion_DevEx, TSelCompetenciaAccion_DevEx );
end;

procedure TEditAcciones_DevEx.BBBorrarClick(Sender: TObject);
begin
     if ZetaDialogo.ZWarningConfirm( '� Atenci�n !', '� Desea borrar la Competencia ?', 0, mbCancel ) then
        inherited;
end;

procedure TEditAcciones_DevEx.BBModificarClick(Sender: TObject);
begin
     inherited;
     ActiveControl := GridRenglones;
     with GridRenglones do
     begin
          SelectedField := Columns[ K_COLUMNA_COMPETENCIA_OBSERVACIONES ].Field;
     end;
end;

end.
