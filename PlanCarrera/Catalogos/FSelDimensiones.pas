unit FSelDimensiones;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal, StdCtrls, CheckLst, Db, Buttons, ExtCtrls;

type
  TSelDimensiones = class(TZetaDlgModal)
    Lista: TCheckListBox;
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
    procedure Inicializa;
    procedure GrabaSeleccionados;
    function  YaExistia( const sCodigo : String ) : Boolean;
  public
    { Public declarations }
  end;

var
  SelDimensiones: TSelDimensiones;

implementation

{$R *.DFM}

uses
    dPlanCarrera;

{ TSelDimensiones }

procedure TSelDimensiones.FormShow(Sender: TObject);
begin
     inherited;
     Inicializa;
end;

procedure TSelDimensiones.OKClick(Sender: TObject);
begin
     inherited;
     GrabaSeleccionados;
end;

procedure TSelDimensiones.GrabaSeleccionados;
var
   i : Integer;
   sCodigo : String;
begin
     with dmPlanCarrera.cdsNivelDimension do
     begin
          DisableControls;
          try
             with Lista do
             begin
                  for i := 0 to Items.Count - 1 do
                  begin
                       sCodigo := Items.Names[ i ];
                       sCodigo := Trim( sCodigo );
                       if Checked[ i ] then
                       begin
                            if not YaExistia( sCodigo ) then
                            begin
                                 Append;
                                 FieldByName( 'DM_CODIGO' ).AsString := sCodigo;
                                 Post;
                            end;
                       end
                       else
                           if YaExistia( sCodigo ) then
                              Delete;
                  end;
             end;
          finally
                 EnableControls;
          end;
     end;
end;

procedure TSelDimensiones.Inicializa;
var
   iPosicion : integer;
   sCodigo: string;
begin
     iPosicion := 0;
     with dmPlanCarrera.cdsDimensiones do
     begin
          Conectar;
          First;
          Lista.Clear;
          while not Eof do
          begin
               sCodigo := FieldByName( 'DM_CODIGO' ).AsString;
               Lista.Items.Add( sCodigo + ' = ' + FieldByName( 'DM_DESCRIP' ).AsString );
               if ( YaExistia( sCodigo ) ) then
                  Lista.Checked[ iPosicion ] := TRUE;
               iPosicion := iPosicion + 1;
               Next;
          end;
     end;
end;

function TSelDimensiones.YaExistia(const sCodigo: String): Boolean;
begin
     with dmPlanCarrera do
     begin
          Result := YaExistia( cdsNivelDimension, 'DM_CODIGO', sCodigo );
     end;
end;

end.
