unit FGlobalesCarrera;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, ImgList, ComCtrls, ToolWin, ZetaDBTextBox,
  ZBaseGlobal, StdCtrls;

type
  TGlobalesCarrera = class(TBaseConsulta)
    PanelTitulos: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    RazonLbl: TZetaDBTextBox;
    RfcLbl: TZetaDBTextBox;
    InfonavitLbl: TZetaDBTextBox;
    ToolBar: TToolBar;
    EvaluacionBtn: TToolButton;
    ImageList: TImageList;
    procedure FormCreate(Sender: TObject);
    procedure EvaluacionBtnClick(Sender: TObject);
  private
    { Private declarations }
    function AbrirGlobal( GlobalClass: TBaseGlobalClass ): Integer;
  protected
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  GlobalesCarrera: TGlobalesCarrera;

implementation

uses DGlobal,
     //DMedico,
     ZAccesosMgr,
     ZAccesosTress,
     ZetaDialogo,
     ZetaCommonClasses,
     FGlobalEvaluacion,
     //FAyudaContexto,
     ZGlobalTress;

{$R *.DFM}

procedure TGlobalesCarrera.FormCreate(Sender: TObject);
begin
     inherited;
     EsConsulta := False;
     //HelpContext := H00101_Configurar_Expediente;
end;

procedure TGlobalesCarrera.Connect;
begin
     with Global do
     begin
          RFCLbl.Caption := GetGlobalString( ZGlobalTress.K_GLOBAL_RFC_EMPRESA );
          InfonavitLbl.Caption := GetGlobalString( ZGlobalTress.K_GLOBAL_INFONAVIT_EMPRESA );
          RazonLbl.Caption := GetGlobalString( ZGlobalTress.K_GLOBAL_RAZON_EMPRESA );
     end;
end;

procedure TGlobalesCarrera.EvaluacionBtnClick(Sender: TObject);
begin
     inherited;
     if ZAccesosMgr.CheckDerecho(  D_CARRERA_GLOBALES, K_DERECHO_CAMBIO ) then
         AbrirGlobal( TGlobalEvaluacion )
     else
         ZetaDialogo.ZInformation('Operaci�n No V�lida', 'No Se Tiene Permiso Para Modificar Globales', 0 );
end;

function TGlobalesCarrera.AbrirGlobal( GlobalClass: TBaseGlobalClass ): Integer;
var
   Forma: TBaseGlobal;
begin
     Forma := GlobalClass.Create( Application );
     with Forma do
     begin
          try
             ShowModal;
             Result := LastAction;
          finally
                 Free;
          end;
     end;
end;

end.
