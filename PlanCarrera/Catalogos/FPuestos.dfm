inherited Puestos: TPuestos
  Caption = 'Puestos'
  ClientHeight = 276
  ClientWidth = 608
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 608
    inherited ValorActivo2: TPanel
      Width = 349
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 608
    Height = 257
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'PU_CODIGO'
        Title.Caption = 'C�digo'
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PU_DESCRIP'
        Title.Caption = 'Descripci�n'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FP_DESCRIP'
        Title.Caption = 'Familia'
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NP_DESCRIP'
        Title.Caption = 'Nivel'
        Width = 82
        Visible = True
      end>
  end
end
