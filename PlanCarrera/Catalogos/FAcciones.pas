unit FAcciones;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls;

type
  TAcciones = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  Acciones: TAcciones;

implementation

{$R *.DFM}

uses
    dPlanCarrera,
    ZetaBuscador,
    ZetaCommonClasses;

{ TAcciones }
procedure TAcciones.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     //HelpContext := H00005_Catalogo_de_Medicinas;
end;

procedure TAcciones.Agregar;
begin
     dmPlanCarrera.cdsAcciones.Agregar;
end;

procedure TAcciones.Borrar;
begin
     dmPlanCarrera.cdsAcciones.Borrar;
end;

procedure TAcciones.Connect;
begin
     with dmPlanCarrera do
     begin
          cdsAcciones.Conectar;
          DataSource.DataSet := cdsAcciones;
     end;
end;

procedure TAcciones.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Acciones', 'ACCION', 'AN_CODIGO', dmPlanCarrera.cdsAcciones );
end;

procedure TAcciones.ImprimirForma;
begin
  inherited;

end;

procedure TAcciones.Modificar;
begin
     dmPlanCarrera.cdsAcciones.Modificar;
end;

function TAcciones.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := True;
end;

function TAcciones.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := True;
end;

procedure TAcciones.Refresh;
begin
     dmPlanCarrera.cdsAcciones.Refrescar;
end;

end.
