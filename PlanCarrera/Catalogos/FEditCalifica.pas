unit FEditCalifica;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaNumero,
  ZetaKeyCombo, Mask, ZetaEdit, ZetaSmartLists;

type
  TEditCalifica = class(TBaseEdicion)
    Label1: TLabel;
    CA_CODIGO: TZetaDBEdit;
    Label2: TLabel;
    CA_DESCRIP: TDBEdit;
    Label3: TLabel;
    CA_INGLES: TDBEdit;
    Label5: TLabel;
    CA_NUMERO: TZetaDBNumero;
    Label6: TLabel;
    CA_TEXTO: TDBEdit;
    Label7: TLabel;
    CA_GRUPO: TDBEdit;
    Label8: TLabel;
    CA_ORDEN: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  EditCalifica: TEditCalifica;

implementation

{$R *.DFM}

uses
    dPlanCarrera,
    ZetaBuscador,
    ZetaCommonClasses,
    ZAccesosTress;


{ TEditCalifica }

procedure TEditCalifica.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CARRERA_CAT_CALIFICA;
     FirstControl := CA_CODIGO;
     //HelpContext:= H60617_Tipo_contrato;
end;

procedure TEditCalifica.Connect;
begin
     DataSource.DataSet := dmPlanCarrera.cdsCalificaciones
end;

procedure TEditCalifica.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Calificaciones', 'CALIFICA', 'CA_CODIGO', dmPlanCarrera.cdsCalificaciones );
end;

end.
