unit FCalifica;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls;

type
  TCalifica = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
    procedure DoLookup; override;       
  end;

var
  Califica: TCalifica;

implementation

{$R *.DFM}

uses
    dPlanCarrera,
    ZetaBuscador,
    ZetaCommonClasses;

{ TCalifica }
procedure TCalifica.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     //HelpContext := H00005_Catalogo_de_Medicinas;
end;

procedure TCalifica.Agregar;
begin
     dmPlanCarrera.cdsCalificaciones.Agregar;
end;

procedure TCalifica.Borrar;
begin
     dmPlanCarrera.cdsCalificaciones.Borrar;
end;

procedure TCalifica.Connect;
begin
     with dmPlanCarrera do
     begin
          cdsCalificaciones.Conectar;
          DataSource.DataSet := cdsCalificaciones;
     end;
end;

procedure TCalifica.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Calificaciones', 'CALIFICA', 'CA_CODIGO', dmPlanCarrera.cdsCalificaciones );
end;

procedure TCalifica.ImprimirForma;
begin
  inherited;

end;

procedure TCalifica.Modificar;
begin
     dmPlanCarrera.cdsCalificaciones.Modificar;
end;

function TCalifica.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := true;
end;

function TCalifica.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := true;
end;

procedure TCalifica.Refresh;
begin
     dmPlanCarrera.cdsCalificaciones.Refrescar;
end;

end.
