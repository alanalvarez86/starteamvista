unit FSelDimensionesPuesto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Db, Controls, Forms, Dialogs,
  ZBaseDlgModal, StdCtrls, CheckLst, Buttons, ExtCtrls;

type
  TSelDimensionesPuesto = class(TZetaDlgModal)
    Lista: TCheckListBox;
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
    procedure Inicializa;
    procedure GrabaSeleccionados;
    function  YaExistia( const sCodigo : String ) : Boolean;
  public
    { Public declarations }
  end;

var
  SelDimensionesPuesto: TSelDimensionesPuesto;

implementation

{$R *.DFM}

uses
    dPlanCarrera;

{ TSelDimensionesPuesto }

procedure TSelDimensionesPuesto.FormShow(Sender: TObject);
begin
     inherited;
     Inicializa;
end;

procedure TSelDimensionesPuesto.OKClick(Sender: TObject);
begin
     inherited;
     GrabaSeleccionados;
end;

procedure TSelDimensionesPuesto.GrabaSeleccionados;
var
   i: Integer;
   sCodigo, sDescripcion: String;
begin
     with dmPlanCarrera.cdsPuestoDimension do
     begin
          DisableControls;
          try
             with Lista do
             begin
                  for i := 0 to Items.Count - 1 do
                  begin
                       sCodigo := Items.Names[ i ];
                       sDescripcion := Trim( Items.Values[ sCodigo ] );
                       sCodigo := Trim( sCodigo );
                       if Checked[ i ] then
                       begin
                            if not YaExistia( sCodigo ) then
                            begin
                                 Append;
                                 FieldByName( 'DM_CODIGO' ).AsString := sCodigo;
                                 FieldByName( 'DM_DESCRIP' ).AsString := sDescripcion;
                                 Post;
                            end;
                       end
                       else
                           if YaExistia( sCodigo ) then
                              Delete;
                  end;
             end;
          finally
                 EnableControls;
          end;
     end;
end;

procedure TSelDimensionesPuesto.Inicializa;
var
   iPosicion : integer;
   sCodigo: string;
begin
     iPosicion := 0;
     with dmPlanCarrera.cdsDimensiones do
     begin
          Conectar;
          First;
          Lista.Clear;
          while not Eof do
          begin
               sCodigo := FieldByName( 'DM_CODIGO' ).AsString;
               Lista.Items.Add( sCodigo + ' = ' + FieldByName( 'DM_DESCRIP' ).AsString );
               if ( YaExistia( sCodigo ) ) then
                  Lista.Checked[ iPosicion ] := TRUE;
               iPosicion := iPosicion + 1;
               Next;
          end;
     end;end;

function TSelDimensionesPuesto.YaExistia(const sCodigo: String): Boolean;
begin
     with dmPlanCarrera do
     begin
          Result := YaExistia( cdsPuestoDimension, 'DM_CODIGO', sCodigo );
     end;
end;

end.
