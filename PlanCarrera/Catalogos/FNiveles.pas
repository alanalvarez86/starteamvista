unit FNiveles;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls;

type
  TNiveles = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  Niveles: TNiveles;

implementation

{$R *.DFM}

uses
    dPlanCarrera,
    ZetaBuscador,
    ZetaCommonClasses;

{ TNiveles }
procedure TNiveles.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     //HelpContext := H00005_Catalogo_de_Medicinas;
end;

procedure TNiveles.Agregar;
begin
     dmPlanCarrera.cdsNiveles.Agregar;
end;

procedure TNiveles.Borrar;
begin
     dmPlanCarrera.cdsNiveles.Borrar;
end;

procedure TNiveles.Connect;
begin
     with dmPlanCarrera do
     begin
          cdsNiveles.Conectar;
          DataSource.DataSet := cdsNiveles;
     end;
end;

procedure TNiveles.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Niveles', 'NIV_PTO', 'NP_CODIGO', dmPlanCarrera.cdsNiveles );
end;

procedure TNiveles.ImprimirForma;
begin
  inherited;
end;

procedure TNiveles.Modificar;
begin
     dmPlanCarrera.cdsNiveles.Modificar;
end;

function TNiveles.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := True;
end;

function TNiveles.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := True;
end;

procedure TNiveles.Refresh;
begin
     dmPlanCarrera.cdsNiveles.Refrescar;
end;

end.
