inherited Familias: TFamilias
  Caption = 'Familias'
  ClientHeight = 277
  ClientWidth = 605
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 605
    inherited ValorActivo2: TPanel
      Width = 346
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 605
    Height = 258
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'FP_CODIGO'
        Title.Caption = 'C�digo'
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FP_DESCRIP'
        Title.Caption = 'Descripci�n'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FP_INGLES'
        Title.Caption = 'Ingl�s'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FP_NUMERO'
        Title.Caption = 'N�mero'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FP_TEXTO'
        Title.Caption = 'Texto'
        Width = 200
        Visible = True
      end>
  end
end
