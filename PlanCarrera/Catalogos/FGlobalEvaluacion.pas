unit FGlobalEvaluacion;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGlobal, StdCtrls, Buttons, ExtCtrls, Mask, ZetaFecha;

type
  TGlobalEvaluacion = class(TBaseGlobal)
    GroupBox1: TGroupBox;
    lblDe: TLabel;
    lblHasta: TLabel;
    zfFin: TZetaFecha;
    zfInicio: TZetaFecha;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  GlobalEvaluacion: TGlobalEvaluacion;

implementation

uses ZGlobalTress,
     ZAccesosTress,
     ZetaCommonTools,
     ZetaDialogo,
     ZetaCommonClasses;
{$R *.DFM}

procedure TGlobalEvaluacion.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos          := D_CARRERA_GLOBALES;
     zfInicio.Tag           := K_GLOBAL_CARRERA_FECHA_EVAL_INICIO;
     zfFin.Tag              := K_GLOBAL_CARRERA_FECHA_EVAL_FINAL;
     //HelpContext            := H00050_Globales_de_empresa;
end;

procedure TGlobalEvaluacion.FormShow(Sender: TObject);
begin
     inherited;
     if( strVacio( zfInicio.Texto ) )then
     begin
          zfInicio.Valor := Date;
          zfFin.Valor := Date;
     end;
end;

procedure TGlobalEvaluacion.OKClick(Sender: TObject);
begin
     if( zfFin.Valor >= zfInicio.Valor )then
         inherited
     else
         ZetaDialogo.ZInformation(self.Caption,'El Valor de la Fecha Final Debe de Ser Mayor � Igual a la Fecha De Inicio', 0 ); 
end;

end.
