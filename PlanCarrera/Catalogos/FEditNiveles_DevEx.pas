unit FEditNiveles_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Db, Grids, DBGrids, ComCtrls, ExtCtrls, DBCtrls, Buttons, Mask,
     ZBaseEdicionRenglon_DevEx,
     ZetaEdit,
     ZetaDBGrid,
     ZetaNumero,
     ZetaSmartLists, ZBaseEdicionRenglon, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, cxControls, dxBarBuiltInMenu, dxBarExtItems,
  dxBar, cxClasses, Vcl.ImgList, cxPC, cxNavigator, cxDBNavigator, cxButtons,
  cxSplitter, cxContainer, cxEdit, cxGroupBox, cxTextEdit, cxMemo, cxDBEdit;

type
  TEditNiveles_DevEx = class(TBaseEdicionRenglon_DevEx)
    Label1: TLabel;
    Label2: TLabel;
    NP_DESCRIP: TDBEdit;
    NP_CODIGO: TZetaDBEdit;
    Label3: TLabel;
    NP_INGLES: TDBEdit;
    NP_TEXTO: TDBEdit;
    Label4: TLabel;
    Label5: TLabel;
    NP_NUMERO: TZetaDBNumero;
    Label6: TLabel;
    NP_ACTITUD: TDBEdit;
    Label7: TLabel;
    ND_RESUMEN: TDBEdit;
    cxGroupBox1: TcxGroupBox;
    cxGroupBox2: TcxGroupBox;
    Panel3: TPanel;
    Panel4: TPanel;
    ND_DESCRIP: TcxDBMemo;
    NP_DETALLE: TcxDBMemo;
    procedure FormCreate(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure BBBorrarClick(Sender: TObject);
    procedure dsRenglonDataChange(Sender: TObject; Field: TField);
    procedure BBModificarClick(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect;override;
    procedure DoLookup; override;
    procedure HabilitaControles; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure DoCancelChanges; override;
  public
    { Public declarations }
  end;

var
  EditNiveles_DevEx: TEditNiveles_DevEx;

implementation

{$R *.DFM}

uses
    FSelDimensiones_DevEx,
    DPlanCarrera,
    ZetaBuscador,
    ZetaCommonClasses,
    ZetaCommonLists,
    ZetaDialogo,
    ZBaseDlgModal_DevEx,
    ZAccesosTress;

const
     K_COLUMNA_DIMENSION_CODIGO = 0;
     K_COLUMNA_DIMENSION_NOMBRE = 1;

{ TEditNiveles }

procedure TEditNiveles_DevEx.FormCreate(Sender: TObject);
var
   i: Integer;
begin
     inherited;
     IndexDerechos := D_CARRERA_CAT_NIVEL;
     FirstControl := NP_CODIGO;
     with GridRenglones do
     begin
          for i := K_COLUMNA_DIMENSION_CODIGO to K_COLUMNA_DIMENSION_NOMBRE do
          begin
               with Columns[ i ] do
               begin
                    Color := clInfoBk;
                    Font.Color := clInfoText;
                    ReadOnly := True;
               end;
          end;
     end;
     HelpContext:= H_Catalog_Niveles;
end;

procedure TEditNiveles_DevEx.Connect;
begin
     with dmPlanCarrera do
     begin
          {
          cdsDimensiones.Conectar;
          cdsNiveles.Conectar;
          cdsEditNiveles.Refrescar;
          }
          DataSource.DataSet := cdsEditNiveles;
          dsRenglon.DataSet := cdsNivelDimension;
     end;
end;

procedure TEditNiveles_DevEx.DoLookup;
begin
     inherited DoLookup;
end;

procedure TEditNiveles_DevEx.HabilitaControles;
begin
     inherited HabilitaControles;
end;

procedure TEditNiveles_DevEx.Agregar;
begin
     inherited Agregar;
end;

procedure TEditNiveles_DevEx.Borrar;
begin
     inherited Borrar;
end;

procedure TEditNiveles_DevEx.Modificar;
begin
     inherited Modificar;
end;

procedure TEditNiveles_DevEx.DoCancelChanges;
begin
     dmPlanCarrera.cdsNivelDimension.CancelUpdates;
     inherited DoCancelChanges;
end;

procedure TEditNiveles_DevEx.dsRenglonDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     with dsRenglon.Dataset do
     begin
          ND_RESUMEN.Enabled := not IsEmpty;
          ND_DESCRIP.Enabled := not IsEmpty;
          Self.BBBorrar_DevEx.Enabled := not IsEmpty;
          Self.BBModificar_DevEx.Enabled := not IsEmpty;
     end;
end;

procedure TEditNiveles_DevEx.BBAgregarClick(Sender: TObject);
begin
     inherited;
     ShowDlgModal( SelDimensiones_DevEx, TSelDimensiones_DevEx );
end;

procedure TEditNiveles_DevEx.BBBorrarClick(Sender: TObject);
begin
     if ZetaDialogo.ZWarningConfirm( '� Atenci�n !', '� Desea borrar esta Dimensi�n ?', 0, mbCancel ) then
        inherited;
end;

procedure TEditNiveles_DevEx.BBModificarClick(Sender: TObject);
begin
     inherited;
     with dmPlanCarrera.cdsPuestoDimension do
     begin
          if not IsEmpty then
             Edit;
     end;
     ActiveControl := ND_RESUMEN;
end;

end.
