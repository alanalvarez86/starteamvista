inherited TipoCompetencias_DevEx: TTipoCompetencias_DevEx
  Left = 227
  Top = 193
  Caption = 'Tipo de Competencia'
  ClientHeight = 287
  ClientWidth = 617
  ExplicitWidth = 617
  ExplicitHeight = 287
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 617
    ExplicitWidth = 617
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 358
      ExplicitWidth = 358
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 352
        ExplicitLeft = 3
      end
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 617
    Height = 268
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'TC_CODIGO'
        Title.Caption = 'C'#243'digo'
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TC_DESCRIP'
        Title.Caption = 'Descripci'#243'n'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TC_TIPO'
        Title.Caption = 'Tipo'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TC_INGLES'
        Title.Caption = 'Ingl'#233's'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TC_NUMERO'
        Title.Caption = 'N'#250'mero'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TC_TEXTO'
        Title.Caption = 'Texto'
        Width = 200
        Visible = True
      end>
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 617
    Height = 268
    ExplicitWidth = 617
    ExplicitHeight = 268
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object TC_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'TC_CODIGO'
      end
      object TC_DESCRIP: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'TC_DESCRIP'
      end
      object TC_TIPO: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'TC_TIPO'
      end
      object TC_INGLES: TcxGridDBColumn
        Caption = 'Ingl'#233's'
        DataBinding.FieldName = 'TC_INGLES'
      end
      object TC_NUMERO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'TC_NUMERO'
      end
      object TC_TEXTO: TcxGridDBColumn
        Caption = 'Texto'
        DataBinding.FieldName = 'TC_TEXTO'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
