unit FCatTCompetencias;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, StdCtrls, Grids, DBGrids, ZetaDBGrid;

type
  TTipoCompetencias = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  TipoCompetencias: TTipoCompetencias;

implementation

{$R *.DFM}

uses
    dPlanCarrera,
    ZetaBuscador,
    ZetaCommonClasses;


{ TTipoCompetencias }
procedure TTipoCompetencias.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     //HelpContext := H00005_Catalogo_de_Medicinas;
end;

procedure TTipoCompetencias.Agregar;
begin
     dmPlanCarrera.cdsTCompetencia.Agregar;
end;

procedure TTipoCompetencias.Borrar;
begin
     dmPlanCarrera.cdsTCompetencia.Borrar;
end;

procedure TTipoCompetencias.Connect;
begin
     with dmPlanCarrera do
     begin
          cdsTCompetencia.Conectar;
          DataSource.DataSet:= cdsTCompetencia;
     end;
end;

procedure TTipoCompetencias.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Competencia', 'TCOMPETE', 'TC_CODIGO', dmPlanCarrera.cdsTCompetencia );
end;

procedure TTipoCompetencias.ImprimirForma;
begin
  inherited;

end;

procedure TTipoCompetencias.Modificar;
begin
     dmPlanCarrera.cdsTCompetencia.Modificar;
end;

function TTipoCompetencias.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := true;
end;

function TTipoCompetencias.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := true;
end;

procedure TTipoCompetencias.Refresh;
begin
     dmPlanCarrera.cdsTCompetencia.Refrescar;
end;

end.
