inherited EditDimensiones: TEditDimensiones
  Left = 334
  Top = 265
  Caption = 'Dimensiones'
  ClientHeight = 193
  ClientWidth = 394
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 47
    Top = 41
    Width = 36
    Height = 13
    Caption = 'C'#243'digo:'
  end
  object Label2: TLabel [1]
    Left = 24
    Top = 63
    Width = 59
    Height = 13
    Caption = 'Descripci'#243'n:'
  end
  object Label3: TLabel [2]
    Left = 52
    Top = 85
    Width = 31
    Height = 13
    Caption = 'Ingl'#233's:'
  end
  object Label5: TLabel [3]
    Left = 43
    Top = 107
    Width = 40
    Height = 13
    Caption = 'N'#250'mero:'
  end
  object Label6: TLabel [4]
    Left = 53
    Top = 129
    Width = 30
    Height = 13
    Caption = 'Texto:'
  end
  inherited PanelBotones: TPanel
    Top = 157
    Width = 394
    inherited OK: TBitBtn
      Left = 226
    end
    inherited Cancelar: TBitBtn
      Left = 311
    end
  end
  inherited PanelSuperior: TPanel
    Width = 394
  end
  inherited PanelIdentifica: TPanel
    Width = 394
    inherited ValorActivo2: TPanel
      Width = 68
    end
  end
  object DM_CODIGO: TZetaDBEdit [8]
    Left = 88
    Top = 37
    Width = 90
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 3
    ConfirmEdit = True
    DataField = 'DM_CODIGO'
    DataSource = DataSource
  end
  object DM_DESCRIP: TDBEdit [9]
    Left = 88
    Top = 59
    Width = 230
    Height = 21
    DataField = 'DM_DESCRIP'
    DataSource = DataSource
    TabOrder = 4
  end
  object DM_INGLES: TDBEdit [10]
    Left = 88
    Top = 81
    Width = 230
    Height = 21
    DataField = 'DM_INGLES'
    DataSource = DataSource
    TabOrder = 5
  end
  object DM_NUMERO: TZetaDBNumero [11]
    Left = 88
    Top = 103
    Width = 110
    Height = 21
    Mascara = mnNumeroGlobal
    TabOrder = 6
    Text = '0.00'
    UseEnterKey = True
    DataField = 'DM_NUMERO'
    DataSource = DataSource
  end
  object DM_TEXTO: TDBEdit [12]
    Left = 88
    Top = 125
    Width = 230
    Height = 21
    DataField = 'DM_TEXTO'
    DataSource = DataSource
    TabOrder = 7
  end
end
