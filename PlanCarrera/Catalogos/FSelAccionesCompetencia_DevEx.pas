unit FSelAccionesCompetencia_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, CheckLst, DB, Buttons, ExtCtrls,
     ZBaseDlgModal_DevEx, ZBaseDlgModal, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, Vcl.ImgList, cxButtons;

type
  TSelAccionesCompetencia_DevEx = class(TZetaDlgModal_DevEx)
    Lista: TCheckListBox;
    procedure FormShow(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    function YaExistia( const sCodigo: String ): Boolean;
    procedure Inicializa;
    procedure GrabaSeleccionados;
  public
    { Public declarations }
  end;

var
  SelAccionesCompetencia_DevEx: TSelAccionesCompetencia_DevEx;

implementation

{$R *.DFM}

uses DPlanCarrera,
     ZetaCommonTools;

procedure TSelAccionesCompetencia_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     Inicializa;
end;

procedure TSelAccionesCompetencia_DevEx.OK_DevExClick(Sender: TObject);
begin
  inherited;
  GrabaSeleccionados;
end;

procedure TSelAccionesCompetencia_DevEx.GrabaSeleccionados;
var
   i, iOrden: Integer;
   sCodigo, sDescripcion: String;
begin
     with dmPlanCarrera.cdsCompetenciaMapa do
     begin
          DisableControls;
          try
             iOrden := 0;
             First;
             while not Eof do
             begin
                  iOrden := ZetaCommonTools.iMax( iOrden, FieldByName( 'CM_ORDEN' ).AsInteger );
                  Next;
             end;
             with Lista do
             begin
                  for i := 0 to Items.Count - 1 do
                  begin
                       sCodigo := Items.Names[ i ];
                       sDescripcion := Trim( Items.Values[ sCodigo ] );
                       sCodigo := Trim( sCodigo );
                       if Checked[ i ] then
                       begin
                            if not YaExistia( sCodigo ) then
                            begin
                                 Inc( iOrden );
                                 Append;
                                 FieldByName( 'CM_ORDEN' ).AsInteger := iOrden;
                                 FieldByName( 'AN_CODIGO' ).AsString := sCodigo;
                                 FieldByName( 'AN_NOMBRE' ).AsString := sDescripcion;
                                 Post;
                            end;
                       end
                       else
                           if YaExistia( sCodigo ) then
                              Delete;
                  end;
             end;
          finally
                 EnableControls;
          end;
     end;
end;

procedure TSelAccionesCompetencia_DevEx.Inicializa;
var
   iPosicion : integer;
   sCodigo: string;
begin
     iPosicion := 0;
     with dmPlanCarrera.cdsAcciones do
     begin
          Conectar;
          First;
          Lista.Clear;
          while not Eof do
          begin
               sCodigo := FieldByName( 'AN_CODIGO' ).AsString;
               Lista.Items.Add( sCodigo + ' = ' + FieldByName( 'AN_NOMBRE' ).AsString );
               if YaExistia( sCodigo ) then
                  Lista.Checked[ iPosicion ] := TRUE;
               iPosicion := iPosicion + 1;
               Next;
          end;
     end;
end;

function TSelAccionesCompetencia_DevEx.YaExistia( const sCodigo: String ): Boolean;
begin
     with dmPlanCarrera do
     begin
          Result := YaExistia( cdsCompetenciaMapa, 'AN_CODIGO', sCodigo );
     end;
end;

end.
