inherited Competencias: TCompetencias
  Caption = 'Competencias'
  ClientHeight = 257
  ClientWidth = 614
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 614
    inherited ValorActivo2: TPanel
      Width = 355
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 614
    Height = 238
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'CM_CODIGO'
        Title.Caption = 'C�digo'
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CM_DESCRIP'
        Title.Caption = 'Descripci�n'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TC_DESCRIP'
        Title.Caption = 'Tipo'
        Width = 180
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CM_INGLES'
        Title.Caption = 'Ingl�s'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CM_NUMERO'
        Title.Caption = 'N�mero'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CM_TEXTO'
        Title.Caption = 'Texto'
        Width = 200
        Visible = True
      end>
  end
end
