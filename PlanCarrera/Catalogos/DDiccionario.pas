unit DDiccionario;

interface
{$INCLUDE DEFINES.INC}
uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,
     DBaseTressDiccionario,
     DBaseDiccionario,
     ZetaTipoEntidad,
     ZetaClientDataSet,
     ZetaCommonLists;

type
  TdmDiccionario = class(TdmBaseTressDiccionario)
    procedure cdsDiccionAlModificar(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
    procedure GetListaClasifiModulo(oLista: TStrings; const lMigracion: Boolean);override;
    procedure SetLookupNames;override;
  end;

var
  dmDiccionario: TdmDiccionario;

implementation
uses FEditDiccion,
     ZBaseEdicion;
{$R *.DFM}

procedure TdmDiccionario.SetLookupNames;
begin
     inherited;
     //dmSeleccion.SetLookupNames;
end;

procedure TdmDiccionario.cdsDiccionAlModificar(Sender: TObject);
begin
     inherited;
     ZBaseEdicion.ShowFormaEdicion( EditDiccion, TEditDiccion );

end;

procedure TdmDiccionario.GetListaClasifiModulo(oLista: TStrings; const lMigracion: Boolean);
begin
     inherited GetListaClasifiModulo( oLista, lMigracion );
     {$ifdef RDD}
     {$else}
     AgregaClasifi( oLista, crCarrera );
     {$endif}
end;

end.
