<%Function ChecaRespuesta( strResultado, xslOk, xslError )
	
	dim xmlResultado	
	dim docXSL
	dim Nodos
			
	set xmlResultado = CreateObject("Msxml2.DOMDocument.4.0")
	xmlResultado.loadXML(strResultado)

	set Nodos = xmlResultado.documentElement.selectNodes( "//ERRORES" )
	set docXSL = Server.CreateObject("Msxml2.DOMDocument.4.0")
	docXSL.async = false
	
	if ( Nodos.Length = 0 ) then
		docXSL.load(Server.MapPath(xslOk))	
		'Response.Write( " NO HAY " )
	else
		docXSL.load(Server.MapPath(xslError))		
		'Response.Write( Nodos.Item( 0 ).Text )
	end if
	
	'Response.Write(strResultado)
	Response.Write(xmlResultado.transformNode(docXSL))
	
End Function%>