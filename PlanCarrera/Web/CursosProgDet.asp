<%@ Language=VBScript %>
<%option explicit%>
<!-- #include file="Tools.asp" -->
<%
	Response.Expires=0
	Response.Buffer=false

	'Declaramos Variables
	dim PlanCarreraXML
	dim strCursosProgDet
	dim docXSL
	dim docXML

	'Declaramos Variables para Iniciar el Componente y recibir el XML generado
	set PlanCarreraXML = Server.CreateObject("PlanCarrera.dmServerPlanCarrera")
	strCursosProgDet= PlanCarreraXML.GetCursosProgDetalle(GetXML("<TIPO>"+Request("TIPO")+"</TIPO><CURSO>"+Request("CURSO")+"</CURSO><STATUS>"+Request("STATUS")+"</STATUS>"))
        'Response.write(strCursosProgDet)

	set docXML = Server.CreateObject("Msxml2.DOMDocument.4.0")
	docXML.async = false
        docXML.loadXML(strCursosProgDet)

	set docXSL = Server.CreateObject("Msxml2.DOMDocument.4.0")
	docXSL.async = false
	docXSL.load(Server.MapPath("PlanCarrera.xsl"))

        Response.Write(docXML.transformNode(docXSL))
%>

