<?xml version='1.0' encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>
<xsl:template match="/">

<xsl:element name="link">
	<xsl:attribute name="rel">stylesheet</xsl:attribute>
	<xsl:attribute name="href">PlanCarrera.css</xsl:attribute>
	<xsl:attribute name="type">text/css</xsl:attribute>
</xsl:element>

<xsl:element name="title">Grupo Tress - Matriz de Entrenamiento por Curso</xsl:element>

<center>
<font class="TITULO">
	<b><TITULO>Matriz de Entrenamiento por Curso</TITULO></b>
</font>
<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>
<table>
	<tr>
		<td align="right">	
				<font class="LETREROS">
					<b><xsl:value-of select="//CURSO/LABELS/@CU_CODIGO"/>:</b>
				</font>	
		</td>	
		<td>
			<xsl:variable name="Codigo"><xsl:value-of select="//CURSO/ROWS/ROW/@CU_CODIGO"/></xsl:variable>
			<xsl:variable name="LenCodigo"><xsl:value-of select="string-length($Codigo)"/></xsl:variable>
			<xsl:choose>
				<xsl:when test="$LenCodigo!=0">
					<xsl:element name="a">
						<xsl:attribute name="href">Curso.asp?ItemNum=<xsl:value-of select="//CURSO/ROWS/ROW/@CU_CODIGO"/></xsl:attribute>
						<xsl:value-of select="//CURSO/ROWS/ROW/@CU_CODIGO"/><xsl:text> = </xsl:text><xsl:value-of select="//CURSO/ROWS/ROW/@CU_NOMBRE"/>
					</xsl:element>					
				</xsl:when>
				<xsl:otherwise>
					* No se tiene registrado *			
				</xsl:otherwise>				
			</xsl:choose>
		</td>	
	</tr>
	<tr>
		<td align="right">	
				<font class="LETREROS">
					<b><xsl:value-of select="//CURSO/LABELS/@TB_ELEMENT"/>:</b>
				</font>	
		</td>	
		<td>
			<xsl:variable name="Codigo2"><xsl:value-of select="//CURSO/ROWS/ROW/@CU_CLASIFI"/></xsl:variable>
			<xsl:variable name="LenCodigo2"><xsl:value-of select="string-length($Codigo2)"/></xsl:variable>
			<xsl:choose>
				<xsl:when test="$LenCodigo2!=0">
					<xsl:element name="a">
						<xsl:attribute name="href">Cursos.asp?ItemNum=<xsl:value-of select="//CURSO/ROWS/ROW/@CU_CLASIFI"/></xsl:attribute>
						<xsl:value-of select="//CURSO/ROWS/ROW/@CU_CLASIFI"/><xsl:text> = </xsl:text><xsl:value-of select="//CURSO/ROWS/ROW/@TB_ELEMENT"/>
					</xsl:element>					
				</xsl:when>
				<xsl:otherwise>
					* No se tiene registrado *			
				</xsl:otherwise>				
			</xsl:choose>
		</td>	
	</tr>
	<tr>
		<td align="right">	
				<font class="LETREROS">
					<b><xsl:value-of select="//CURSO/LABELS/@MA_CODIGO"/>:</b>
				</font>	
		</td>	
		<td>
			<xsl:variable name="Codigo3"><xsl:value-of select="//CURSO/ROWS/ROW/@MA_CODIGO"/></xsl:variable>
			<xsl:variable name="LenCodigo3"><xsl:value-of select="string-length($Codigo3)"/></xsl:variable>
			<xsl:choose>
				<xsl:when test="$LenCodigo3!=0">
					<xsl:element name="a">
						<xsl:attribute name="href">Maestro.asp?ItemNum=<xsl:value-of select="//CURSO/ROWS/ROW/@MA_CODIGO"/></xsl:attribute>
						<xsl:value-of select="//CURSO/ROWS/ROW/@MA_CODIGO"/><xsl:text> = </xsl:text><xsl:value-of select="//CURSO/ROWS/ROW/@MA_NOMBRE"/>
					</xsl:element>					
				</xsl:when>
				<xsl:otherwise>
					* No se tiene registrado *			
				</xsl:otherwise>				
			</xsl:choose>
		</td>	
	</tr>
</table>
	
<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>

<xsl:element name="br"/>
<!--********************* PUESTOS ************************-->
<table width="95%">
	<tr align="right">
		<font class="SUBTITULO">
			<b>Puestos</b>
		</font> 
	</tr>
</table>	

<xsl:variable name="NumRegPue"><xsl:value-of select="//ENTRENA/ROWS/@cuantos"/></xsl:variable>
<xsl:choose>
	<xsl:when test="$NumRegPue!=0">
		<table class="TH_TEAL" width="95%"><!-- cellSpacing="1" cellPadding="10" width="95%" border="0" align="center">-->
			<tr>
				<th class="TH_TEAL"><xsl:value-of select="//ENTRENA/LABELS/@PU_CODIGO"/></th>
				<th class="TH_TEAL"><xsl:value-of select="//ENTRENA/LABELS/@PU_DESCRIP"/></th>
				<th class="TH_TEAL"><xsl:value-of select="//ENTRENA/LABELS/@PU_CLASIFI"/></th>
			</tr>
			<xsl:for-each select="//ENTRENA/ROWS/ROW">
			<tr>
				<td class="TD_COLOR"><font class="xsmallblack">
					<xsl:element name="a">
						<xsl:attribute name="href">Puesto.asp?ItemNum=<xsl:value-of select="@PU_CODIGO"/></xsl:attribute>
						<xsl:value-of select="@PU_CODIGO"/>
					</xsl:element></font></td>
				<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@PU_DESCRIP"/></font></td>
				<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@PU_CLASIFI"/></font></td>
			</tr>
			</xsl:for-each>
		</table>
	</xsl:when>
	<xsl:otherwise>		
		<font class="REG_ZERO">
			<b>* No hay datos *</b>
		</font>		
	</xsl:otherwise>
</xsl:choose>

<xsl:element name="br"/>

<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
 		<tr>
 			<td align="left">
 				<xsl:element name="a">
 					<xsl:attribute name="href">javascript:window.history.back()</xsl:attribute>
 					<xsl:element name="img">
 						<xsl:attribute name="src">back.gif</xsl:attribute> 
 						<xsl:attribute name="border">0</xsl:attribute>
 						<xsl:attribute name="alt">Pagina Anterior</xsl:attribute>
 					</xsl:element>
 				</xsl:element>	
 			</td>
     		<td align="right">
 				<xsl:element name="a">
 					<xsl:attribute name="href">PlanCarrera.asp</xsl:attribute>
 					<xsl:element name="img">
 						<xsl:attribute name="src">inicio.gif</xsl:attribute> 
 						<xsl:attribute name="border">0</xsl:attribute>
 						<xsl:attribute name="alt">Incio</xsl:attribute>
 					</xsl:element>
 				</xsl:element>	
 			</td>
		</tr>
</table>

<table width="95%">
	<tr align="center">
		<td align="center">
   			<xsl:element name="a">
				<xsl:attribute name="href">PlanCarrera.asp</xsl:attribute>
			<xsl:element name="img">
				<xsl:attribute name="src">tress_small.gif</xsl:attribute> 
					<xsl:attribute name="border">0</xsl:attribute>
				</xsl:element> 					
			</xsl:element>	
			<xsl:element name="br"/>
			Plan de Carrera
		</td>
	</tr>	
</table>
	<p align="center">
		<font size="2" color="#333333" face="Verdana, Arial, Helvetica, sans-serif"> Copyright � 1998-2002 
			<a href="http://www.tress.com.mx">Grupo Tress</a> Todos los derechos reservados 
		</font>
	</p>
</center>	

</xsl:template>
</xsl:stylesheet>