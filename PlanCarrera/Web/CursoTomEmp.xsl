<?xml version='1.0' encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>
<xsl:template match="/">

<xsl:element name="link">
	<xsl:attribute name="rel">stylesheet</xsl:attribute>
	<xsl:attribute name="href">PlanCarrera.css</xsl:attribute>
	<xsl:attribute name="type">text/css</xsl:attribute>
</xsl:element>

<xsl:element name="title">Grupo Tress - Cursos Tomados por Empleado</xsl:element>

<center>
<font class="TITULO">
	<b>Cursos por Empleado</b>
</font>
<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>

<table>
	<tr>
		<td align="right">	
			<font class="LETREROS">
				<b><xsl:value-of select="//COLABORA/LABELS/@CB_CODIGO"/>:</b>			
			</font>	
		</td>	
		<td>
			<xsl:element name="a">
				<xsl:attribute name="href">Empleado.asp?ItemNum=<xsl:value-of select="//COLABORA/ROWS/ROW/@CB_CODIGO"/></xsl:attribute>
				<xsl:value-of select="//COLABORA/ROWS/ROW/@CB_CODIGO"/>
			</xsl:element>				
		</td>
	</tr>
	<tr>
		<td align="right">	
			<font class="LETREROS">
				<b><xsl:value-of select="//COLABORA/LABELS/@PRETTYNAME"/>:</b>			
			</font>	
		</td>	
		<td>
			<xsl:value-of select="//COLABORA/ROWS/ROW/@PRETTYNAME"/>
		</td>
	</tr>
	<tr>
		<td align="right">	
			<font class="LETREROS">
				<b><xsl:value-of select="//COLABORA/LABELS/@CB_PUESTO"/>:</b>			
			</font>	
		</td>	
		<td>
			<xsl:variable name="strPuesto"><xsl:value-of select="//COLABORA/ROWS/ROW/@CB_PUESTO"/></xsl:variable>
			<xsl:variable name="LenstrPuesto"><xsl:value-of select="string-length($strPuesto)"/></xsl:variable>								
			<xsl:choose>
				<xsl:when test="$LenstrPuesto!=0">
					<xsl:element name="a">
						<xsl:attribute name="href">Puesto.asp?ItemNum=<xsl:value-of select="//COLABORA/ROWS/ROW/@CB_PUESTO"/></xsl:attribute>
						<xsl:value-of select="//COLABORA/ROWS/ROW/@CB_PUESTO"/><xsl:text> = </xsl:text><xsl:value-of select="//COLABORA/ROWS/ROW/@PU_DESCRIP"/>
					</xsl:element>	
				</xsl:when>
				<xsl:otherwise>
					* No se tiene registrado *				
				</xsl:otherwise>
			</xsl:choose>
		</td>
	</tr>
	<tr>
		<td align="right">	
			<font class="LETREROS">
				<b><xsl:value-of select="//COLABORA/LABELS/@FP_CODIGO"/>:</b>			
			</font>	
		</td>	
		<td>
			<xsl:variable name="strCodigo"><xsl:value-of select="//COLABORA/ROWS/ROW/@FP_CODIGO"/></xsl:variable>
			<xsl:variable name="LenstrCodigo"><xsl:value-of select="string-length($strCodigo)"/></xsl:variable>								
			<xsl:choose>
				<xsl:when test="$LenstrCodigo!=0">
					<xsl:element name="a">
						<xsl:attribute name="href">PtosxFam.asp?ItemNum=<xsl:value-of select="//COLABORA/ROWS/ROW/@FP_CODIGO"/></xsl:attribute>
						<xsl:value-of select="//COLABORA/ROWS/ROW/@FP_CODIGO"/><xsl:text> = </xsl:text><xsl:value-of select="//COLABORA/ROWS/ROW/@FP_DESCRIP"/>
					</xsl:element>	
				</xsl:when>
				<xsl:otherwise>
					* No se tiene registrado *				
				</xsl:otherwise>
			</xsl:choose>
		</td>
	</tr>
	<tr>
		<td align="right">	
			<font class="LETREROS">
				<b><xsl:value-of select="//COLABORA/LABELS/@NP_CODIGO"/>:</b>			
			</font>	
		</td>	
		<td>
			<xsl:variable name="strCodigo"><xsl:value-of select="//COLABORA/ROWS/ROW/@NP_CODIGO"/></xsl:variable>
			<xsl:variable name="LenstrCodigo"><xsl:value-of select="string-length($strCodigo)"/></xsl:variable>								
			<xsl:choose>
				<xsl:when test="$LenstrCodigo!=0">
					<xsl:element name="a">
						<xsl:attribute name="href">PtosxNiv.asp?ItemNum=<xsl:value-of select="//COLABORA/ROWS/ROW/@NP_CODIGO"/></xsl:attribute>
						<xsl:value-of select="//COLABORA/ROWS/ROW/@NP_CODIGO"/><xsl:text> = </xsl:text><xsl:value-of select="//COLABORA/ROWS/ROW/@NP_DESCRIP"/>
					</xsl:element>	
				</xsl:when>
				<xsl:otherwise>
					* No se tiene registrado *				
				</xsl:otherwise>
			</xsl:choose>
		</td>
	</tr>
</table>

<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>
<xsl:element name="br"/>

<table width="95%" >
	<tr>
		<td>
			<xsl:element name="a">
				<xsl:attribute name="href">ComparaCompPto.asp?EMPLEADO=<xsl:value-of select="//COLABORA/ROWS/ROW/@CB_CODIGO"/><xsl:text>&#38;</xsl:text>PRETTYNAME=<xsl:value-of select="//COLABORA/ROWS/ROW/@PRETTYNAME"/><xsl:text>&#38;</xsl:text>PUESTOACTUAL=<xsl:value-of select="//COLABORA/ROWS/ROW/@CB_PUESTO"/><xsl:text>&#38;</xsl:text>DESCRIPCION=<xsl:value-of select="//COLABORA/ROWS/ROW/@PU_DESCRIP"/><xsl:text>&#38;</xsl:text>FAMILIA=<xsl:value-of select="//COLABORA/ROWS/ROW/@FP_CODIGO"/><xsl:text>&#38;</xsl:text>PUESTO=<xsl:value-of select="//COLABORA/ROWS/ROW/@CB_PUESTO"/></xsl:attribute>
				Competencias vs. Puesto
			</xsl:element>
		</td>
	</tr> 
	<tr>
		<td>
			<xsl:element name="a">
				<xsl:attribute name="href">CompxEmpleado.asp?ItemNum=<xsl:value-of select="//COLABORA/ROWS/ROW/@CB_CODIGO"/></xsl:attribute>
				Competencias por Empleado
			</xsl:element>
		</td>
	</tr>
	<tr>
		<td>
			<xsl:element name="a">
				<xsl:attribute name="href">PlanAccxEmp.asp?ItemNum=<xsl:value-of select="//COLABORA/ROWS/ROW/@CB_CODIGO"/></xsl:attribute>
				Plan de Acci�n
			</xsl:element>
		</td>
	</tr>	
</table>


<xsl:element name="br"/>

<!-- ********** Cursos Programados ************* -->

<table width="95%">
	<tr align="right">
		<font class="SUBTITULO">
			<b>Cursos programados</b>
		</font> 
	</tr>
</table>	

<xsl:variable name="NumReg2"><xsl:value-of select="//CUR_PROG/ROWS/@cuantos"/></xsl:variable>
<xsl:choose>
	<xsl:when test="$NumReg2!=0">
		<table class="TH_TEAL" width="95%">     		
			<tr>
				<th class="TH_TEAL"><xsl:value-of select="//CUR_PROG/LABELS/@CU_CODIGO"/></th>
				<th class="TH_TEAL"><xsl:value-of select="//CUR_PROG/LABELS/@CU_NOMBRE"/></th>
				<th class="TH_TEAL"><xsl:value-of select="//CUR_PROG/LABELS/@MA_CODIGO"/></th>
				<th class="TH_TEAL"><xsl:value-of select="//CUR_PROG/LABELS/@KC_FEC_PRO"/></th>			
			</tr>
			<xsl:for-each select="//CUR_PROG/ROWS/ROW">
			<tr>
				<td class="TD_COLOR"><font class="xsmallblack">
					<xsl:element name="a">
						<xsl:attribute name="href">Curso.asp?ItemNum=<xsl:value-of select="@CU_CODIGO"/></xsl:attribute>
						<xsl:value-of select="@CU_CODIGO"/>
					</xsl:element></font></td>
				<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@CU_NOMBRE"/></font></td>
				<xsl:variable name="strCodigo"><xsl:value-of select="@MA_CODIGO"/></xsl:variable>
				<xsl:variable name="LenstrCodigo"><xsl:value-of select="string-length($strCodigo)"/></xsl:variable>								
				<xsl:choose>
					<xsl:when test="$LenstrCodigo!=0">				
						<td class="TD_COLOR"><font class="xsmallblack">
							<xsl:element name="a">
								<xsl:attribute name="href">Maestro.asp?ItemNum=<xsl:value-of select="@MA_CODIGO"/></xsl:attribute>
								<xsl:value-of select="@MA_NOMBRE"/>
							</xsl:element></font></td>
					</xsl:when>
					<xsl:otherwise>						
						<td class="TD_COLOR"><font class="xsmallblack">	</font></td>				
					</xsl:otherwise>
				</xsl:choose>							
				<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@KC_FEC_PRO"/></font></td>				
			</tr>
			</xsl:for-each>
		</table>

	</xsl:when>
	<xsl:otherwise>
		<font class="REG_ZERO">
			<b>* No hay datos *</b>
		</font>		
	</xsl:otherwise>
</xsl:choose>
<xsl:element name="br"/>

<!-- ********** Cursos Tomados ************* -->
<table width="95%">
	<tr align="right">
		<font class="SUBTITULO">
			<b>Cursos Tomados</b>
		</font> 
	</tr>
</table>

<xsl:variable name="NumReg"><xsl:value-of select="//KARCURSO/ROWS/@cuantos"/></xsl:variable>
<xsl:choose>
	<xsl:when test="$NumReg!=0">
		<table class="TH_TEAL" width="95%">     		
			<tr>
				<th class="TH_TEAL"><xsl:value-of select="//KARCURSO/LABELS/@CU_CODIGO"/></th>
				<th class="TH_TEAL"><xsl:value-of select="//KARCURSO/LABELS/@CU_NOMBRE"/></th>
				<th class="TH_TEAL"><xsl:value-of select="//KARCURSO/LABELS/@MA_CODIGO"/></th>
				<th class="TH_TEAL"><xsl:value-of select="//KARCURSO/LABELS/@KC_FEC_TOM"/></th>			
				<th class="TH_TEAL"><xsl:value-of select="//KARCURSO/LABELS/@KC_EVALUA"/></th>				
			</tr>
			<xsl:for-each select="//KARCURSO/ROWS/ROW">
			<tr>
				<td class="TD_COLOR"><font class="xsmallblack">
					<xsl:element name="a">
						<xsl:attribute name="href">Curso.asp?ItemNum=<xsl:value-of select="@CU_CODIGO"/></xsl:attribute>
						<xsl:value-of select="@CU_CODIGO"/>
					</xsl:element></font></td>
				<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@CU_NOMBRE"/></font></td>
				<xsl:variable name="strCodigo"><xsl:value-of select="@MA_CODIGO"/></xsl:variable>
				<xsl:variable name="LenstrCodigo"><xsl:value-of select="string-length($strCodigo)"/></xsl:variable>								
				<xsl:choose>
					<xsl:when test="$LenstrCodigo!=0">				
						<td class="TD_COLOR"><font class="xsmallblack">
							<xsl:element name="a">
								<xsl:attribute name="href">Maestro.asp?ItemNum=<xsl:value-of select="@MA_CODIGO"/></xsl:attribute>
								<xsl:value-of select="@MA_NOMBRE"/>
							</xsl:element></font></td>
					</xsl:when>
					<xsl:otherwise>
						<td class="TD_COLOR"><font class="xsmallblack">
					    </font></td>
					</xsl:otherwise>
				</xsl:choose>							
				<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@KC_FEC_TOM"/></font></td>
				<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@KC_EVALUA"/></font></td>				
			</tr>
			</xsl:for-each>
		</table>
	</xsl:when>
	<xsl:otherwise>
		<font class="REG_ZERO">
			<b>* No hay datos *</b>
		</font>		
	</xsl:otherwise>
</xsl:choose>
<xsl:element name="br"/>

<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>

<table width="95%" border="0" cellspacing="0" cellpadding="0">
 		<tr>
 			<td align="left">
 				<xsl:element name="a">
 					<xsl:attribute name="href">javascript:window.history.back()</xsl:attribute>
 					<xsl:element name="img">
 						<xsl:attribute name="src">back.gif</xsl:attribute> 
 						<xsl:attribute name="border">0</xsl:attribute>
 						<xsl:attribute name="alt">Pagina Anterior</xsl:attribute>
 					</xsl:element>
 				</xsl:element>	
 			</td> 			
     		<td align="right">
 				<xsl:element name="a">
 					<xsl:attribute name="href">PlanCarrera.asp</xsl:attribute>
 					<xsl:element name="img">
 						<xsl:attribute name="src">inicio.gif</xsl:attribute> 
 						<xsl:attribute name="border">0</xsl:attribute>
 						<xsl:attribute name="alt">Incio</xsl:attribute>
 					</xsl:element>
 				</xsl:element>	
 			</td>
		</tr>
</table>

<table width="95%">
	<tr align="center">
		<td align="center">
   			<xsl:element name="a">
				<xsl:attribute name="href">PlanCarrera.asp</xsl:attribute>
			<xsl:element name="img">
				<xsl:attribute name="src">tress_small.gif</xsl:attribute> 
					<xsl:attribute name="border">0</xsl:attribute>
				</xsl:element> 					
			</xsl:element>	
			<xsl:element name="br"/>
			Plan de Carrera
		</td>
	</tr>	
</table>
	<p align="center">
		<font size="2" color="#333333" face="Verdana, Arial, Helvetica, sans-serif"> Copyright � 1998-2002 
			<a href="http://www.tress.com.mx">Grupo Tress</a> Todos los derechos reservados 
		</font>
	</p>
</center>
</xsl:template>
</xsl:stylesheet>