<?xml version='1.0' encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>
<xsl:template match="/">

<xsl:element name="link">
	<xsl:attribute name="rel">stylesheet</xsl:attribute>
	<xsl:attribute name="href">PlanCarrera.css</xsl:attribute>
	<xsl:attribute name="type">text/css</xsl:attribute>
</xsl:element>

<xsl:element name="title">Grupo Tress - Puestos por Nivel</xsl:element>

<script language="javascript">
	function MuestraPtoXNiv( oCombo )
	{
	var sNew
		if ( oCombo.value != T1.value )
		{
			sNew = oCombo.value;
			oCombo.value = T1.value;			
 	 		location.href="PtosxNiv.asp?ItemNum="+sNew;	 	 		
		}
	}
</script>

<center>
<font class="TITULO">
	<b><TITULO>Puestos por Nivel</TITULO></b>
</font>
<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>
<xsl:element name="input">
	<xsl:attribute name="Type">hidden</xsl:attribute>
	<xsl:attribute name="name">T1</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of select="//NIV_PTO/COD_DEFAULT"/></xsl:attribute>
</xsl:element>
<table>
	<tr>
		<td align="right">	
				<font class="LETREROS">
					<b><xsl:value-of select="//NIV_PTO/LABELS/@NP_CODIGO"/>:</b>
				</font>	
		</td>	
		<td>
			<xsl:element name="select">
				<xsl:attribute name="name">cboPtoXNiv</xsl:attribute>
				<xsl:attribute name="ID">cboPtoXNiv</xsl:attribute>
				<xsl:attribute name="OnChange">MuestraPtoXNiv(this)</xsl:attribute>
				<xsl:variable name="valorDef"><xsl:value-of select="//NIV_PTO/COD_DEFAULT"/></xsl:variable>
					<xsl:for-each select="//NIV_PTO/ROWS/ROW">
						<xsl:variable name="Codigo"><xsl:value-of select="//NIV_PTO/ROWS/ROW/@NP_CODIGO"/></xsl:variable>
						<xsl:variable name="LenCodigo"><xsl:value-of select="string-length($Codigo)"/></xsl:variable>
						<xsl:choose>
							<xsl:when test="$LenCodigo!=0">
								<xsl:element name="option">
									<xsl:attribute name="value">							
										<xsl:value-of select="@NP_CODIGO"/>						
									</xsl:attribute>					
									<xsl:if test="@NP_CODIGO=$valorDef">
										<xsl:attribute name="selected">yes</xsl:attribute>
									</xsl:if>					
									<xsl:value-of select="@NP_DESCRIP"/>
								</xsl:element>
							</xsl:when>
						</xsl:choose>
					</xsl:for-each>				
			</xsl:element>
		</td>	
	</tr>
	<tr>
		<td align="right">	
			<font class="LETREROS">
				<b><xsl:value-of select="//NIV_PTO/LABELS/@NP_ACTITUD"/>:</b>
			</font>	
		</td>	
		<td>
			<xsl:value-of select="//NIV_PTO/ROWS/ROW/@NP_ACTITUD"/>		
		</td>
	</tr>
	<tr>
		<td align="right">	
			<font class="LETREROS">
				<b><xsl:value-of select="//NIV_PTO/LABELS/@NP_DETALLE"/>:</b>
			</font>	
		</td>	
		<td>
			<xsl:value-of select="//NIV_PTO/ROWS/ROW/@NP_DETALLE"/>		
		</td>
	</tr>	
</table>
	
<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>

<xsl:element name="br"/>
<!--********************* DIMENSIONES ************************-->
<table width="95%">
	<tr align="right">
		<font class="SUBTITULO">
			<b>Dimensiones</b>
		</font> 
	</tr>
</table>	

<xsl:variable name="NumRegDim"><xsl:value-of select="//NIV_DIME/ROWS/@cuantos"/></xsl:variable>
<xsl:choose>
	<xsl:when test="$NumRegDim!=0">
		<table class="TH_TEAL" width="95%"><!-- cellSpacing="1" cellPadding="10" width="95%" border="0" align="center">-->
			<tr>
				<th class="TH_TEAL"><xsl:value-of select="//NIV_DIME/LABELS/@DM_CODIGO"/></th>
				<th class="TH_TEAL"><xsl:value-of select="//NIV_DIME/LABELS/@DM_DESCRIP"/></th>
				<th class="TH_TEAL"><xsl:value-of select="//NIV_DIME/LABELS/@ND_RESUMEN"/></th>
				<th class="TH_TEAL"><xsl:value-of select="//NIV_DIME/LABELS/@ND_DESCRIP"/></th>			
			</tr>
			<xsl:for-each select="//NIV_DIME/ROWS/ROW">
			<tr>
				<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@DM_CODIGO"/></font></td>
				<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@DM_DESCRIP"/></font></td>
				<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@ND_RESUMEN"/></font></td>
				<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@ND_DESCRIP"/></font></td>			
			</tr>
			</xsl:for-each>
		</table>
	</xsl:when>
	<xsl:otherwise>		
		<font class="REG_ZERO">
			<b>* No hay datos *</b>
		</font>		
	</xsl:otherwise>
</xsl:choose>

<xsl:element name="br"/>

<!-- *********************** PUESTOS ********************-->
<table width="95%">
	<tr align="right">
		<font class="SUBTITULO">
			<b>Puestos</b>
		</font> 
	</tr>
</table>	

<xsl:variable name="NumRegPto"><xsl:value-of select="//PUESTO/ROWS/@cuantos"/></xsl:variable>
<xsl:choose>
	<xsl:when test="$NumRegPto!=0">
		<table class="TH_TEAL" width="95%"><!-- cellSpacing="1" cellPadding="10" width="95%" border="0" align="center">-->
			<tr>
				<th class="TH_TEAL"><xsl:value-of select="//PUESTO/LABELS/@PU_CODIGO"/></th>
				<th class="TH_TEAL"><xsl:value-of select="//PUESTO/LABELS/@PU_DESCRIP"/></th>
			</tr>
			<xsl:for-each select="//PUESTO/ROWS/ROW">
			<tr>
				<td class="TD_COLOR"><font class="xsmallblack">
					<xsl:element name="a">
						<xsl:attribute name="href">Puesto.asp?ItemNum=<xsl:value-of select="@PU_CODIGO"/></xsl:attribute>
						<xsl:value-of select="@PU_CODIGO"/>
					</xsl:element></font></td>
				<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@PU_DESCRIP"/></font></td>
			</tr>
			</xsl:for-each>
		</table>
	</xsl:when>
	<xsl:otherwise>	
		<font class="REG_ZERO">
			<b>* No hay datos *</b>
		</font>			
	</xsl:otherwise>	
</xsl:choose>

<xsl:element name="br"/>
<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
 		<tr>
 			<td align="left">
 				<xsl:element name="a">
 					<xsl:attribute name="href">javascript:window.history.back()</xsl:attribute>
 					<xsl:element name="img">
 						<xsl:attribute name="src">back.gif</xsl:attribute> 
 						<xsl:attribute name="border">0</xsl:attribute>
 						<xsl:attribute name="alt">Pagina Anterior</xsl:attribute>
 					</xsl:element>
 				</xsl:element>	
 			</td>
     		<td align="right">
 				<xsl:element name="a">
 					<xsl:attribute name="href">PlanCarrera.asp</xsl:attribute>
 					<xsl:element name="img">
 						<xsl:attribute name="src">inicio.gif</xsl:attribute> 
 						<xsl:attribute name="border">0</xsl:attribute>
 						<xsl:attribute name="alt">Incio</xsl:attribute>
 					</xsl:element>
 				</xsl:element>	
 			</td>
		</tr>
</table>

<table width="95%">
	<tr align="center">
		<td align="center">
   			<xsl:element name="a">
				<xsl:attribute name="href">PlanCarrera.asp</xsl:attribute>
			<xsl:element name="img">
				<xsl:attribute name="src">tress_small.gif</xsl:attribute> 
					<xsl:attribute name="border">0</xsl:attribute>
				</xsl:element> 					
			</xsl:element>	
			<xsl:element name="br"/>
			Plan de Carrera
		</td>
	</tr>	
</table>
	<p align="center">
		<font size="2" color="#333333" face="Verdana, Arial, Helvetica, sans-serif"> Copyright � 1998-2002 
			<a href="http://www.tress.com.mx">Grupo Tress</a> Todos los derechos reservados 
		</font>
	</p>
</center>	

</xsl:template>
</xsl:stylesheet>