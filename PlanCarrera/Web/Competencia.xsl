<?xml version='1.0' encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>
<xsl:template match="/">

<xsl:element name="link">
	<xsl:attribute name="rel">stylesheet</xsl:attribute>
	<xsl:attribute name="href">PlanCarrera.css</xsl:attribute>
	<xsl:attribute name="type">text/css</xsl:attribute>
</xsl:element>

<xsl:element name="title">Grupo Tress - Competencia</xsl:element>

<center>
<font class="TITULO">
	<b>Competencia</b>
</font>
<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>
<table>
	<tr>
		<td align="right">
			<font class="LETREROS">
				<b><xsl:value-of select="//COMPETEN/LABELS/@CM_CODIGO"/>:</b>
			</font>
		</td>
		<td>
			<xsl:value-of select="//COMPETEN/ROWS/ROW/@CM_CODIGO"/>
		</td>
	</tr>
	<tr>
		<td align="right">
			<font class="LETREROS">				
				<b><xsl:value-of select="//COMPETEN/LABELS/@TC_DESCRIP"/>:</b>
			</font>
		</td>		
		<td>
			<xsl:variable name="strTipo"><xsl:value-of select="//COMPETEN/ROWS/ROW/@TC_DESCRIP"/></xsl:variable>
			<xsl:if test="string-length($strTipo)!=0">
				<xsl:element name="a">
						<xsl:attribute name="href">Competencias.asp?ItemNum=<xsl:value-of select="//COMPETEN/ROWS/ROW/@TC_CODIGO"/></xsl:attribute>
						<xsl:value-of select="$strTipo"/>
				</xsl:element>		
			</xsl:if>
			<xsl:if test="string-length($strTipo)=0">
				* No se tiene registrado *
			</xsl:if>
		</td>	
	</tr>
	<tr>
		<td align="right">
			<font class="LETREROS">
				<b><xsl:value-of select="//COMPETEN/LABELS/@TC_TIPO"/>:</b>
			</font>
		</td>
		<td>
			<xsl:value-of select="//COMPETEN/ROWS/ROW/@TC_TIPO"/>
		</td>	
	</tr>
	<tr>
		<td align="right">
			<font class="LETREROS">
				<b><xsl:value-of select="//COMPETEN/LABELS/@CM_DESCRIP"/>:</b>
			</font>
		</td>
		<td>
			<xsl:value-of select="//COMPETEN/ROWS/ROW/@CM_DESCRIP"/>
		</td>	
	</tr>
	<tr>
		<td align="right">
			<font class="LETREROS">
				<b><xsl:value-of select="//COMPETEN/LABELS/@CM_DETALLE"/>:</b>
			</font>
		</td>
		<td>
			<xsl:value-of select="//COMPETEN/ROWS/ROW/@CM_DETALLE"/>
		</td>	
	</tr>
</table>
<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>

<!--************** NIVEL DE DESEMPENO *******************-->
<xsl:variable name="NumRegDes"><xsl:value-of select="//COMP_CAL/ROWS/@cuantos"/></xsl:variable>
<table width="95%" >
	<tr>
		<font class="SUBTITULO">
			<b>Nivel de desempe&#241;o</b>
		</font>		
	</tr>
</table>

<xsl:if test="$NumRegDes!=0">
	<table class="TH_TEAL" width="95%">
		<tr>
			<th class="TH_TEAL"><xsl:value-of select="//COMP_CAL/LABELS/@CA_CODIGO"/></th>
			<th class="TH_TEAL"><xsl:value-of select="//COMP_CAL/LABELS/@CA_DESCRIP"/></th>	
			<th class="TH_TEAL"><xsl:value-of select="//COMP_CAL/LABELS/@MC_DESCRIP"/></th>	
		</tr>
		<xsl:for-each select="//COMP_CAL/ROWS/ROW">
		<tr>
			<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@CA_CODIGO"/></font></td>
			<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@CA_DESCRIP"/></font></td>
			<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@MC_DESCRIP"/></font></td>
		</tr>
		</xsl:for-each>
	</table>
</xsl:if>

<xsl:if test="$NumRegDes=0">	
	<font class="REG_ZERO">
		<b>* No hay datos *</b>
	</font>
</xsl:if>

<xsl:element name="br"/>
<xsl:element name="br"/>

<!--************* ACCIONES RECOMENDADAS ******************-->
<xsl:variable name="NumRegAcc"><xsl:value-of select="//COMP_MAP/ROWS/@cuantos"/></xsl:variable>
<table width="95%" >
	<tr>
		<font class="SUBTITULO">
			<b>Acciones recomendadas</b>
		</font>		
	</tr>
</table>

<xsl:if test="$NumRegAcc!=0">
	<table class="TH_TEAL" width="95%">
		<tr>
			<th class="TH_TEAL"><xsl:value-of select="//COMP_MAP/LABELS/@CM_ORDEN"/></th>
			<th class="TH_TEAL"><xsl:value-of select="//COMP_MAP/LABELS/@AN_CODIGO"/></th>	
			<th class="TH_TEAL"><xsl:value-of select="//COMP_MAP/LABELS/@AN_NOMBRE"/></th>	
			<th class="TH_TEAL"><xsl:value-of select="//COMP_MAP/LABELS/@AN_CLASE"/></th>	
		</tr>
		<xsl:for-each select="//COMP_MAP/ROWS/ROW">
		<tr>
			<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@CM_ORDEN"/></font></td>
			<td class="TD_COLOR"><font class="xsmallblack">
				<xsl:element name="a">
					<xsl:attribute name="href">Accion.asp?ItemNum=<xsl:value-of select="@AN_CODIGO"/></xsl:attribute>
					<xsl:value-of select="@AN_CODIGO"/>
				</xsl:element></font></td>
			<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@AN_NOMBRE"/></font></td>
			<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@AN_CLASE"/></font></td>
		</tr>
		</xsl:for-each>
	</table>
</xsl:if>

<xsl:if test="$NumRegAcc=0">
	<font class="REG_ZERO">
		<b>* No hay datos *</b>
	</font>
</xsl:if>
<xsl:element name="br"/>
<xsl:element name="br"/>

<!--*************** PUESTOS **************************-->
<xsl:variable name="NumRegPto"><xsl:value-of select="//COMP_PTO/ROWS/@cuantos"/></xsl:variable>
<table width="95%" >
	<tr>
		<font class="SUBTITULO">
			<b>Puestos</b>
		</font>		
	</tr>
</table>

<xsl:if test="$NumRegPto!=0">
	<table class="TH_TEAL" width="95%">
		<tr>
			<th class="TH_TEAL"><xsl:value-of select="//COMP_PTO/LABELS/@PU_CODIGO"/></th>
			<th class="TH_TEAL"><xsl:value-of select="//COMP_PTO/LABELS/@PU_DESCRIP"/></th>	
			<th class="TH_TEAL"><xsl:value-of select="//COMP_PTO/LABELS/@CA_CODIGO"/></th>	
			<th class="TH_TEAL"><xsl:value-of select="//COMP_PTO/LABELS/@MP_DESCRIP"/></th>	
		</tr>
		<xsl:for-each select="//COMP_PTO/ROWS/ROW">
		<tr>
			<td class="TD_COLOR"><font class="xsmallblack">
				<xsl:element name="a">
					<xsl:attribute name="href">Puesto.asp?ItemNum=<xsl:value-of select="@PU_CODIGO"/></xsl:attribute>
					<xsl:value-of select="@PU_CODIGO"/>
				</xsl:element></font></td>
			<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@PU_DESCRIP"/></font></td>		
			<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@CA_CODIGO"/></font></td>
			<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@MP_DESCRIP"/></font></td>
		</tr>
		</xsl:for-each>
	</table>
</xsl:if>

<xsl:if test="$NumRegPto=0">
	<font class="REG_ZERO">
		<b>* No hay datos  *</b>
	</font>
</xsl:if>
<xsl:element name="br"/>

<table width="95%"> 
	<tr>
		<td>
			<xsl:element name="a">
				<xsl:attribute name="href">DesemCompTot.asp?TIPO=<xsl:value-of select="//COMPETEN/ROWS/ROW/@TC_CODIGO"/><xsl:text>&#38;</xsl:text>COMPETENCIA=<xsl:value-of select="//COMPETEN/ROWS/ROW/@CM_CODIGO"/></xsl:attribute>
				Desempe�o por competencia
			</xsl:element>
		</td>
	</tr>
</table>
<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
 		<tr>
 			<td align="left">
 				<xsl:element name="a">
 					<xsl:attribute name="href">javascript:window.history.back()</xsl:attribute>
 					<xsl:element name="img">
 						<xsl:attribute name="src">back.gif</xsl:attribute> 
 						<xsl:attribute name="border">0</xsl:attribute>
 						<xsl:attribute name="alt">Pagina Anterior</xsl:attribute>
 					</xsl:element>
 				</xsl:element>	
 			</td>
     		<td align="right">
 				<xsl:element name="a">
 					<xsl:attribute name="href">PlanCarrera.asp</xsl:attribute>
 					<xsl:element name="img">
 						<xsl:attribute name="src">inicio.gif</xsl:attribute> 
 						<xsl:attribute name="border">0</xsl:attribute>
 						<xsl:attribute name="alt">Incio</xsl:attribute>
 					</xsl:element>
 				</xsl:element>	
 			</td>
		</tr>
</table>

<table width="95%">
	<tr align="center">
		<td align="center">
   			<xsl:element name="a">
				<xsl:attribute name="href">PlanCarrera.asp</xsl:attribute>
			<xsl:element name="img">
				<xsl:attribute name="src">tress_small.gif</xsl:attribute> 
					<xsl:attribute name="border">0</xsl:attribute>
				</xsl:element> 					
			</xsl:element>	
			<xsl:element name="br"/>
			Plan de Carrera
		</td>
	</tr>	
</table>
	<p align="center">
		<font size="2" color="#333333" face="Verdana, Arial, Helvetica, sans-serif"> Copyright � 1998-2002 
			<a href="http://www.tress.com.mx">Grupo Tress</a> Todos los derechos reservados 
		</font>
	</p>
</center>	

</xsl:template>
</xsl:stylesheet>