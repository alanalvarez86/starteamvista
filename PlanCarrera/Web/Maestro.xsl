<?xml version='1.0' encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>
<xsl:template match="/">

<xsl:element name="link">
	<xsl:attribute name="rel">stylesheet</xsl:attribute>
	<xsl:attribute name="href">PlanCarrera.css</xsl:attribute>
	<xsl:attribute name="type">text/css</xsl:attribute>
</xsl:element>

<xsl:element name="title">Grupo Tress - Maestro</xsl:element>

<center>
<font class="TITULO">
	<b>Maestro</b>
</font>
<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>

<table>
	<tr>
		<td align="right">	
			<font class="LETREROS">
				<b><xsl:value-of select="//MAESTRO/LABELS/@MA_CODIGO"/>:</b>			
			</font>	
		</td>	
		<td>
			<xsl:value-of select="//MAESTRO/ROWS/ROW/@MA_CODIGO"/>
		</td>
	</tr>
	<tr>
		<td align="right">	
			<font class="LETREROS">
				<b><xsl:value-of select="//MAESTRO/LABELS/@MA_NOMBRE"/>:</b>			
			</font>	
		</td>	
		<td>
			<xsl:value-of select="//MAESTRO/ROWS/ROW/@MA_NOMBRE"/>
		</td>
	</tr>
	<tr>
		<td align="right">	
			<font class="LETREROS">
				<b><xsl:value-of select="//MAESTRO/LABELS/@MA_CEDULA"/>:</b>			
			</font>	
		</td>	
		<td>
			<xsl:value-of select="//MAESTRO/ROWS/ROW/@MA_CEDULA"/>		
		</td>
	</tr>
	<tr>
		<td align="right">	
			<font class="LETREROS">
				<b><xsl:value-of select="//MAESTRO/LABELS/@MA_RFC"/>:</b>			
			</font>	
		</td>	
		<td>
			<xsl:value-of select="//MAESTRO/ROWS/ROW/@MA_RFC"/>
		</td>
	</tr>
	<tr>
		<td align="right">	
			<font class="LETREROS">
				<b><xsl:value-of select="//MAESTRO/LABELS/@MA_EMPRESA"/>:</b>			
			</font>	
		</td>	
		<td>
			<xsl:value-of select="//MAESTRO/ROWS/ROW/@MA_EMPRESA"/>
		</td>
	</tr>
	<tr>
		<td align="right">	
			<font class="LETREROS">
				<b><xsl:value-of select="//MAESTRO/LABELS/@MA_DIRECCI"/>:</b>
			</font>	
		</td>	
		<td>
			<xsl:value-of select="//MAESTRO/ROWS/ROW/@MA_DIRECCI"/>		
		</td>
	</tr>
	<tr>
		<td align="right">	
			<font class="LETREROS">
				<b><xsl:value-of select="//MAESTRO/LABELS/@MA_CIUDAD"/>:</b>
			</font>	
		</td>	
		<td>
			<xsl:value-of select="//MAESTRO/ROWS/ROW/@MA_CIUDAD"/>		
		</td>
	</tr>
	<tr>
		<td align="right">	
			<font class="LETREROS">
				<b><xsl:value-of select="//MAESTRO/LABELS/@MA_TEL"/>:</b>
			</font>	
		</td>	
		<td>
			<xsl:value-of select="//MAESTRO/ROWS/ROW/@MA_TEL"/>		
		</td>
	</tr>
	
</table>

<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>
<xsl:element name="br"/>

<table width="95%" border="0" cellspacing="0" cellpadding="0">
 		<tr>
 			<td align="left">
 				<xsl:element name="a">
 					<xsl:attribute name="href">javascript:window.history.back()</xsl:attribute>
 					<xsl:element name="img">
 						<xsl:attribute name="src">back.gif</xsl:attribute> 
 						<xsl:attribute name="border">0</xsl:attribute>
 						<xsl:attribute name="alt">Pagina Anterior</xsl:attribute>
 					</xsl:element>
 				</xsl:element>	
 			</td> 			
     		<td align="right">
 				<xsl:element name="a">
 					<xsl:attribute name="href">PlanCarrera.asp</xsl:attribute>
 					<xsl:element name="img">
 						<xsl:attribute name="src">inicio.gif</xsl:attribute> 
 						<xsl:attribute name="border">0</xsl:attribute>
 						<xsl:attribute name="alt">Incio</xsl:attribute>
 					</xsl:element>
 				</xsl:element>	
 			</td>
		</tr>
</table>

<table width="95%">
	<tr align="center">
		<td align="center">
   			<xsl:element name="a">
				<xsl:attribute name="href">PlanCarrera.asp</xsl:attribute>
			<xsl:element name="img">
				<xsl:attribute name="src">tress_small.gif</xsl:attribute> 
					<xsl:attribute name="border">0</xsl:attribute>
				</xsl:element> 					
			</xsl:element>	
			<xsl:element name="br"/>
			Plan de Carrera
		</td>
	</tr>	
</table>
	<p align="center">
		<font size="2" color="#333333" face="Verdana, Arial, Helvetica, sans-serif"> Copyright � 1998-2002 
			<a href="http://www.tress.com.mx">Grupo Tress</a> Todos los derechos reservados 
		</font>
	</p>
</center>	

</xsl:template>
</xsl:stylesheet>