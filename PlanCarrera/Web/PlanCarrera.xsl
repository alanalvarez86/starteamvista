<?xml version='1.0' encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>

<!-- TEMPLATE RAIZ -->

<xsl:template match="/">
<html> 
  <head>
    <link rel="stylesheet" href="PlanCarrera.css" type="text/css"/>
    <script language="JavaScript" SRC="CommonTools.js"/>
        <title> <xsl:apply-templates select="RAIZ/DESCRIPCIONES/MODULO"/> - 
	        <xsl:apply-templates select="RAIZ/DESCRIPCIONES/PANTALLA"/> 
	</title>
  </head>
 <body>
      <font class="TITULO">
         <center>   
            <TITULO><xsl:value-of select="RAIZ/DESCRIPCIONES/PANTALLA"/></TITULO>
         </center>   
      </font>
        <xsl:apply-templates/>
  </body>
</html>
</xsl:template>

<!-- TEMPLATE DE ELEMENTO RAIZ -->


<xsl:template match="RAIZ">
     <xsl:apply-templates/>
</xsl:template> 

<!-- TEMPLATE DE ELEMENTO ERRORES -->

<xsl:template match="ERRORES">
     <center>
       <font color="FF0000" face="Verdana"><h1>��� Se encontraron errores !!!</h1></font>
	  <hr width="95%"/>
	  <br/>
	  <h2><font face="Verdana">Mensajes de Error</font></h2>
	  <table width="95%" bgcolor="#E6E4E4" border="1" bordercolor="#acacac" bordercolorlight="#C5C3C3" bordercolordark="#ffffff">
		<tr align="center">
			<td>
    			    <xsl:apply-templates/>
    		     </td>
    	     </tr>
       </table>
     </center>
</xsl:template>

<!-- TEMPLATE DE ELEMENTO ERROR -->

<xsl:template match="ERROR">
     <xsl:value-of select="."/>
</xsl:template>

<!-- TEMPLATE DE ELEMENTO DESCRIPCIONES -->

<xsl:template match="DESCRIPCIONES">
     <xsl:apply-templates select="modulo"/>
</xsl:template> 


<!-- TEMPLATE DESPLIEGUE MODULO -->

<xsl:template match="MODULO">
    <xsl:value-of select="."/>
</xsl:template>

<!-- TEMPLATE DESCRIPCION ASP
<xsl:template match="ASP">
     <xsl:apply-templates/>
</xsl:template>
-->

<!-- TEMPLATE DESPLIEGUE PANTALLA -->

<xsl:template match="PANTALLA">
     <xsl:value-of select="."/>
</xsl:template> 

<!-- TEMPLATE DESPLIEGUE ESPACIO -->

<xsl:template match="ESPACIO">
     <br/>
</xsl:template> 

<!-- TEMPLATE PARA DESPLIEGUE MASTER (MENU-TITULOS) -->

<xsl:template match="MASTER">
     <hr width="95%"/>
     <table align="center" width="95%">
       	<xsl:apply-templates/>
     </table>
</xsl:template>


<!-- TEMPLATE PARA TABLA MASTER -->
<xsl:template match="TABLAMASTER">
     <tr>
     	<td>	
         	<table border="0" align="{@alineacion}">
             	<xsl:apply-templates/>
         	</table>
         </td>
     </tr>
	 <tr>
       <td>
       		<hr/>
       </td>
     </tr>
</xsl:template>


<!-- TEMPLATES PARA COLUMNAS DE TABLAS CONTENIDO MASTER -->
 <xsl:template match="CONTENIDO">
    <xsl:choose>
       <xsl:when test="string-length(@alineacion)!=0">
          <td align="{@alineacion}"><xsl:apply-templates/></td>
       </xsl:when>
       <xsl:otherwise>
          <td><xsl:apply-templates/></td>
       </xsl:otherwise>
    </xsl:choose>
 </xsl:template>


<xsl:template match="LABEL">
   <font class="LETREROS">
      <xsl:value-of select="."/>
   </font>
</xsl:template>

<xsl:template match="FORMA">
   <form name="{@nombre}" method="{@metodo}" action="{@accion}">
       <xsl:apply-templates/>
   </form>
</xsl:template>

<!-- TEMPLATE PARA MENU DE DESPLIEGUE MASTER -->

<xsl:template match="MENU">
    <input type="hidden" name="{@nombre}" value="{OPCIONSELECTED/@value}"/>
    <select onchange="{@alcambiar}" onfocus="MenuTemp(this,{@nombre})">
      <xsl:apply-templates select="OPCION|OPCIONSELECTED"/>
    </select>
</xsl:template>

<xsl:template match="COMBO">
    <select name="{@nombre}" onchange="{@alcambiar}">
      <xsl:apply-templates select="OPCION|OPCIONSELECTED"/>
    </select>
</xsl:template>

<!-- TEMPLATE PARA RADIO OPTIONS HABILITADO -->
<xsl:template match="RADIOABLE">
    <input type="radio" name="{@nombre}" value="{@valor}" onchange="{@alcambiar}" > <xsl:value-of select="."/> </input>
</xsl:template>

<!-- TEMPLATE PARA RADIO SELECTED HABILITADO--> 
<xsl:template match="RADIOCHECKABLE">
    <input type="radio" name="{@nombre}" value="{@valor}" checked="yes" onchange="{@alcambiar}"> <xsl:value-of select="."/> </input>
</xsl:template>

<!-- TEMPLATE PARA RADIO OPTIONS DESHABILITADO -->
<xsl:template match="RADIO">
    <input type="radio" name="{@nombre}" value="{@valor}" disabled="yes" onchange="{@alcambiar}"> <xsl:value-of select="."/> </input>
</xsl:template>

<!-- TEMPLATE PARA RADIO SELECTED DESHABILITADO --> 
<xsl:template match="RADIOCHECK">
    <input type="radio" name="{@nombre}" value="{@valor}" checked="yes" disabled="yes" onchange="{@alcambiar}"> <xsl:value-of select="."/> </input>
</xsl:template>

<!-- TEMPLATE PARA BOTONES DE SUBMIT -->
<xsl:template match="BTNSUBMIT">
    <input type="submit" name="{@nombre}" value="{.}"/>
</xsl:template>

<!-- TEMPLATE PARA BOTONES DE USO GENERAL -->
<xsl:template match="BOTON">
    <input type="button" name="{@nombre}" value="{.}" onclick="{@onclick}"/>
</xsl:template>


<!-- TEMPLATE PARA EDITS -->
<xsl:template match="EDIT">
    <input type="Edit" size="{@tamano}" name="{@nombre}" value="{.}"/>
</xsl:template>

<!-- TEMPLATE PARA TEXTAREAS HABILITADA-->
<xsl:template match="TEXTAREAABLE">
    <textarea rows="5" name="{@nombre}" cols="50" onchange="{@alcambiar}"> <xsl:value-of select="."/> </textarea>
</xsl:template>

<!-- TEMPLATE PARA TEXTAREAS DESHABILITADA-->
<xsl:template match="TEXTAREA">
    <textarea rows="5" name="{@nombre}" cols="50" disabled="yes" onchange="{@alcambiar}"> <xsl:value-of select="."/> </textarea>
</xsl:template>

<!-- TEMPLATE PARA EDITS HIDDEN -->
<xsl:template match="HIDDEN">
    <input type="hidden" name="{@nombre}" value="{.}"/>
</xsl:template>

<!-- TEMPLATE PARA OPCION DE MENU -->
<xsl:template match="OPCION">
     <option value="{@value}"> <xsl:value-of select="@descripcion"/> </option>
</xsl:template>

<!-- TEMPLATE PARA OPCION DE MENU SELECTED--> 
<xsl:template match="OPCIONSELECTED">
      <option value="{@value}" selected="yes"> <xsl:value-of select="@descripcion"/> </option> 
</xsl:template>

<!-- TEMPLATE PARA LIGA HTML -->  

<xsl:template match="LIGA">
  	<a href="{@href}"><xsl:value-of select="."/></a>  
</xsl:template> 

<!--
<xsl:template match="LIGAIMG">
	<a href="{@href}"><img src="{@fuente}" border="0"/></a>
</xsl:template>
-->

<xsl:template match="LIGAIMG">
	<a href="{@href}"><img src="{@fuente}" border="0" alt="{@hint}"/></a>
</xsl:template>

<!-- TEMPLATE PARA LISTA HTML -->
<xsl:template match="LISTA">
<ul>  
  <xsl:apply-templates/>
</ul>
</xsl:template>

<!-- TEMPLATE PARA LAS LINEAS EN BLANCO Y LOS ENTERS -->
<xsl:template match="LINE">
  	<xsl:value-of select="."/><br></br>
</xsl:template>

<!-- TEMPLATE PARA INCISO -->

<xsl:template match="INCISO">
  <li><xsl:apply-templates/></li>
</xsl:template> 


<!-- TEMPLATE PARA DESPLIEGUE TABLA DETAIL -->

<xsl:template match="TABLA">
    <xsl:choose>
       <xsl:when test="@registros!=0">
           <table class="TH_TEAL" width="95%" align="center">
                  <font class="SUBTITULO"><xsl:value-of select="@titulo"/></font>
                  <xsl:apply-templates/>
           </table>
       </xsl:when>
       <xsl:otherwise>
           <center>
           <font class="REG_ZERO">
           		<table width="95%" align="center">
                  <font class="SUBTITULO"><xsl:value-of select="@titulo"/></font>
           		</table>
              	* No hay datos *
           </font>
           </center>
       </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template match="ENCABEZADO|RENGLON">
    <tr>
      <xsl:apply-templates/>
    </tr>
</xsl:template>

<xsl:template match="HEADER">    
    	<xsl:if test="string-length(@clasefuente)=0">	
    		<th class="TH_TEAL" font-class="xsmallblack">    		
    		  <xsl:apply-templates/></th>    		  
    	</xsl:if>
    	<xsl:if test="string-length(@clasefuente)!=0">	
    		<th class="TH_TEAL" font-class="{@clasefuente}">    			
    			<xsl:apply-templates/></th>
    	</xsl:if>
    	<!--<xsl:apply-templates/></th>	-->
</xsl:template>

<xsl:template match="COLUMNA">
	<!--<td class="TD_COLOR">
        <xsl:apply-templates/></td>-->
	<xsl:choose>
       <xsl:when test="string-length(@alineacion)!=0">
           <xsl:if test="string-length(@clasefuente)!=0">
          	<td class="TD_COLOR" align="{@alineacion}" font-class="{@clasefuente}">
          		<xsl:apply-templates/></td>
    		</xsl:if>
    		<xsl:if test="string-length(@clasefuente)=0">	
    			<td class="TD_COLOR" align="{@alineacion}" font-class="xsmallblack">          	          
          			<xsl:apply-templates/></td>    			
    		</xsl:if>
          	<!--<td class="TD_COLOR" align="{@alineacion}">
          <xsl:apply-templates/></td>-->
       </xsl:when>
       <xsl:otherwise>                 		
          	<xsl:if test="string-length(@clasefuente)!=0">	
          		<td class="TD_COLOR" font-class="{@clasefuente}">          	          
          			<xsl:apply-templates/></td>    			
    		</xsl:if>
    		<xsl:if test="string-length(@clasefuente)=0">	
    			<td class="TD_COLOR" font-class="xsmallblack">          	          
          			<xsl:apply-templates/></td>    			
    		</xsl:if>
          <!--<xsl:apply-templates/></td>-->
       </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template match="TEXTO">
    <font class="xsmallblack">
      <xsl:value-of select="."/>
    </font>
</xsl:template>

<xsl:template match="CHECK">
	<IMG src="Check.gif"/>
</xsl:template>

<xsl:template match="NOTCHECK">
	<IMG src="NotCheck.gif"/>
</xsl:template>

<!-- 
<xsl:template match="IMAGEN">
	<p align="center">
		<IMG src="{@fuente}"/></p>
</xsl:template>
-->

<xsl:template match="IMAGEN">
    <IMG src="{@fuente}" alt="{@hint}"/>
</xsl:template>

<!-- TEMPLATE DE LINEAS  -->
<xsl:template match="LINEA">
	<hr width="95%"/>
</xsl:template>


<!-- TEMPLATE DE FOOTER(FECHAS/MENUS RETORNOS)  -->

<xsl:template match="FOOTER">
   <table align="center" width="95%" border="0" cellspacing="0" cellpadding="0">
   <tr>
   <td align="left">
      <a href="javascript:window.history.back()">
          <img src="back.gif" border="0" alt="Pagina Anterior"/>
      </a>
   </td>
   <td align="right">
      <a href="PlanCarrera.asp">
          <img src="inicio.gif" border="0" alt="Inicio"/>
      </a>
   </td>
   </tr>
   </table>
   <table align="center" width="95%">
   <tr align="center">
   <td align="center">
       <a href="PlanCarrera.asp">
           <img src="tress_small.gif" border="0"/>
       </a>
    <br/>
	Plan de Carrera
    </td>
    </tr>
    </table>
    <center>
    <font size="2" color="#333333" face="Verdana, Arial, Helvetica, sans-serif">
      Copyright � 1998-2002
    <a href="http://www.tress.com.mx">Grupo Tress</a>
      Todos los derechos reservados
    </font>
    </center>
</xsl:template>

</xsl:stylesheet>







               



