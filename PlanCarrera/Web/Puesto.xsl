<?xml version='1.0' encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>
<xsl:template match="/">

<xsl:element name="link">
	<xsl:attribute name="rel">stylesheet</xsl:attribute>
	<xsl:attribute name="href">PlanCarrera.css</xsl:attribute>
	<xsl:attribute name="type">text/css</xsl:attribute>
</xsl:element>

<xsl:element name="title">Grupo Tress - Puesto</xsl:element>

<center>
<font class="TITULO">
	<b>Puesto</b>
</font>
<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>
<table>
	<tr>
		<td align="right">
			<font class="LETREROS">
				<b><xsl:value-of select="//PUESTO/LABELS/@PU_CODIGO"/>:</b>
			</font>
		</td>
		<td>
			<xsl:value-of select="//PUESTO/ROWS/ROW/@PU_CODIGO"/>
		</td>
	</tr>
	<tr>
		<td align="right">
			<font class="LETREROS">				
				<b><xsl:value-of select="//PUESTO/LABELS/@PU_DESCRIP"/>:</b>
			</font>
		</td>		
		<td>
			<xsl:value-of select="//PUESTO/ROWS/ROW/@PU_DESCRIP"/>
		</td>	
	</tr>
	<tr>
		<td align="right">
			<font class="LETREROS">
				<b><xsl:value-of select="//PUESTO/LABELS/@PU_DETALLE"/>:</b>
			</font>
		</td>
		<td>
			<xsl:value-of select="//PUESTO/ROWS/ROW/@PU_DETALLE"/>
		</td>	
	</tr>
	<tr>
		<xsl:variable name="Codigo"><xsl:value-of select="//PUESTO/ROWS/ROW/@FP_CODIGO"/></xsl:variable>
		<xsl:variable name="LenCodigo"><xsl:value-of select="string-length($Codigo)"/></xsl:variable>			
		<td align="right">
			<font class="LETREROS">			
				<b><xsl:value-of select="//PUESTO/LABELS/@FP_CODIGO"/>:</b>
			</font>
		</td>
		<td>
			<xsl:choose>
				<xsl:when test="$LenCodigo!=0">
					<xsl:element name="a">
						<xsl:attribute name="href">PtosxFam.asp?ItemNum=<xsl:value-of select="//PUESTO/ROWS/ROW/@FP_CODIGO"/></xsl:attribute>
						<xsl:value-of select="//PUESTO/ROWS/ROW/@FP_CODIGO"/><xsl:text> = </xsl:text><xsl:value-of select="//PUESTO/ROWS/ROW/@FP_DESCRIP"/>
					</xsl:element>									
				</xsl:when>
				<xsl:otherwise>
					* No se tiene registrado *
				</xsl:otherwise>
			</xsl:choose>				
		</td>	
	</tr>
	<tr>
		<xsl:variable name="Codigo"><xsl:value-of select="//PUESTO/ROWS/ROW/@NP_CODIGO"/></xsl:variable>
		<xsl:variable name="LenCodigo"><xsl:value-of select="string-length($Codigo)"/></xsl:variable>		
		<td align="right">
			<font class="LETREROS">
				<b><xsl:value-of select="//PUESTO/LABELS/@NP_CODIGO"/>:</b>
			</font>
		</td>
		<td>
			<xsl:choose>
				<xsl:when test="$LenCodigo!=0">
					<xsl:element name="a">
						<xsl:attribute name="href">PtosxNiv.asp?ItemNum=<xsl:value-of select="//PUESTO/ROWS/ROW/@NP_CODIGO"/></xsl:attribute>
						<xsl:value-of select="//PUESTO/ROWS/ROW/@NP_CODIGO"/><xsl:text> = </xsl:text><xsl:value-of select="//PUESTO/ROWS/ROW/@NP_DESCRIP"/>
					</xsl:element>													
				</xsl:when>
				<xsl:otherwise>
					* No se tiene registrado *
				</xsl:otherwise>
			</xsl:choose>
		</td>	
	</tr>	
</table>
<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>

<!--************** Competencias *******************-->
<table width="95%" >
<tr>
	<font class="SUBTITULO">
		<b>Competencias</b>
	</font>		
</tr>
</table>
<xsl:variable name="Cuantos"><xsl:value-of select="//COMP_PTO/ROWS/@cuantos"/></xsl:variable>
<xsl:choose>
	<xsl:when test="$Cuantos=0">
		<center>
			<font class="REG_ZERO">
				<b>* No hay datos *</b>
			</font>
		</center>
	</xsl:when>
	<xsl:otherwise>	
		<table class="TH_TEAL" width="95%">
			<tr>
				<th class="TH_TEAL"><xsl:value-of select="//COMP_PTO/LABELS/@CM_CODIGO"/></th>
				<th class="TH_TEAL"><xsl:value-of select="//COMP_PTO/LABELS/@CM_DESCRIP"/></th>
				<th class="TH_TEAL"><xsl:value-of select="//COMP_PTO/LABELS/@CA_CODIGO"/></th>
				<th class="TH_TEAL"><xsl:value-of select="//COMP_PTO/LABELS/@MP_DESCRIP"/></th>				
			</tr>
			<xsl:for-each select="//COMP_PTO/ROWS/ROW">
			<tr>
				<xsl:variable name="Codigo"><xsl:value-of select="//COMP_PTO/ROWS/ROW/@CA_CODIGO"/></xsl:variable>
				<xsl:variable name="LenCodigo"><xsl:value-of select="string-length($Codigo)"/></xsl:variable>
				<td class="TD_COLOR"><font class="xsmallblack">
				<xsl:element name="a">
					<xsl:attribute name="href">Competencia.asp?ItemNum=<xsl:value-of select="@CM_CODIGO"/></xsl:attribute>
					<xsl:value-of select="@CM_CODIGO"/>
				</xsl:element></font></td>
				<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@CM_DESCRIP"/></font></td>
				<xsl:choose>
					<xsl:when test="$LenCodigo!=0">
						<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@CA_DESCRIP"/></font></td>
					</xsl:when>
					<xsl:otherwise>
						<td class="TD_COLOR"><font class="xsmallblack"> </font></td>
					</xsl:otherwise>
				</xsl:choose>
				<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@MP_DESCRIP"/></font></td>				
			</tr>
			</xsl:for-each>
		</table>
	</xsl:otherwise>
</xsl:choose>

<xsl:element name="br"/>

<!--************* Dimensiones ******************-->

<table width="95%" >
<tr>
	<font class="SUBTITULO">
		<b>Dimensiones</b>
	</font>		
</tr>
</table>
<xsl:variable name="Cuantos2"><xsl:value-of select="//PTO_DIME/ROWS/@cuantos"/></xsl:variable>
<xsl:choose>
	<xsl:when test="$Cuantos2=0">
		<center>
			<font class="REG_ZERO">
				<b>* No hay datos *</b>
			</font>
		</center>
	</xsl:when>
	<xsl:otherwise>	
		<table class="TH_TEAL" width="95%">
			<tr>
				<th class="TH_TEAL"><xsl:value-of select="//PUESTO/LABELS/@DM_CODIGO"/></th>
				<th class="TH_TEAL"><xsl:value-of select="//PUESTO/LABELS/@DM_DESCRIP"/></th>	
				<th class="TH_TEAL"><xsl:value-of select="//PUESTO/LABELS/@PD_DESCRIP"/></th>				
			</tr>
			<xsl:for-each select="//COMP_MAP/ROWS/ROW">
			<tr>
				<td class="TD_COLOR"><font class="xsmallblack">	
				<xsl:element name="a">
					<xsl:attribute name="href">http://www.tress.com.mx<!--Puesto.asp?ItemNum=<xsl:value-of select="@DM_CODIGO"/>--></xsl:attribute>
					<xsl:value-of select="@DM_CODIGO"/>
				</xsl:element></font></td>	
				<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@DM_DESCRIP"/></font></td>
				<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@PD_DESCRIP"/></font></td>				
			</tr>
			</xsl:for-each>
		</table>
	</xsl:otherwise>
</xsl:choose>
<xsl:element name="br"/>
<table width="95%" >
<tr>
	<font class="xsmallblack">	
	<xsl:element name="a">
		<xsl:attribute name="href">MatrizxPuesto.asp?ItemNum=<xsl:value-of select="//PUESTO/ROWS/ROW/@PU_CODIGO"/></xsl:attribute>
		<xsl:text>Matriz de Entrenamiento</xsl:text>
	</xsl:element></font>		
</tr>
<tr>
	<font class="xsmallblack">	
	<xsl:element name="a">
		<xsl:attribute name="href">BusquedaCandidatos.asp?FAMILIA=<xsl:value-of select="//PUESTO/ROWS/ROW/@FP_CODIGO"/><xsl:text>&#38;</xsl:text>PUESTO=<xsl:value-of select="//PUESTO/ROWS/ROW/@PU_CODIGO"/><xsl:text>&#38;</xsl:text>MOSTRAR=10</xsl:attribute>
		<xsl:text>Buscar Candidatos</xsl:text>
	</xsl:element></font>		
</tr>
</table>

<xsl:element name="br"/>
<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
 		<tr>
 			<td align="left">
 				<xsl:element name="a">
 					<xsl:attribute name="href">javascript:window.history.back()</xsl:attribute>
 					<xsl:element name="img">
 						<xsl:attribute name="src">back.gif</xsl:attribute> 
 						<xsl:attribute name="border">0</xsl:attribute>
 						<xsl:attribute name="alt">Pagina Anterior</xsl:attribute>
 					</xsl:element>
 				</xsl:element>	
 			</td>
     		<td align="right">
 				<xsl:element name="a">
 					<xsl:attribute name="href">PlanCarrera.asp</xsl:attribute>
 					<xsl:element name="img">
 						<xsl:attribute name="src">inicio.gif</xsl:attribute> 
 						<xsl:attribute name="border">0</xsl:attribute>
 						<xsl:attribute name="alt">Incio</xsl:attribute>
 					</xsl:element>
 				</xsl:element>	
 			</td>
		</tr>
</table>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
	<tr>
     	<td align="center">	
  		<xsl:element name="a">
 			<xsl:attribute name="href">PlanCarrera.asp</xsl:attribute>
				<xsl:element name="img">
 					<xsl:attribute name="src">tress_small.gif</xsl:attribute> 
 					<xsl:attribute name="border">0</xsl:attribute>
 				</xsl:element> 					
 			</xsl:element>	
 			<xsl:element name="br"/>
			Plan de Carrera
		</td>
	</tr>
</table>
	<p align="center">
		<font size="2" color="#333333" face="Verdana, Arial, Helvetica, sans-serif"> Copyright � 1998-2002 
			<a href="http://www.tress.com.mx">Grupo Tress</a> Todos los derechos reservados 
		</font>
	</p>
</center>	
</xsl:template>
</xsl:stylesheet>