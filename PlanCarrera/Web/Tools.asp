<%
	Function GetXML( Parametros )
		Response.Expires=0
		Response.Buffer=false

		dim xmlcadena	
		dim ComponenteXML	
		dim strResultado
		dim xmlSalida
		dim docXSL

		'Para Transformar el XML con el XSL tenemos que pasarl el string recibido  a documento XML
		set xmlSalida =  server.CreateObject("Msxml2.DOMDocument.4.0")
		xmlSalida.loadXML(Application("Empresa"))
	
		'Response.Write(xmlSalida.xml)
		
		'Load the XSL para mostrar en pantalla
		set docXSL = Server.CreateObject("Msxml2.DOMDocument.4.0")
		docXSL.async = false
		docXSL.load(Server.MapPath("empgen.xsl"))	
	
		xmlcadena=""
		xmlcadena = "<?xml version='1.0' encoding='ISO-8859-1'?><RAIZ>" + xmlSalida.transformNode(docXSL) 
		if Parametros = "" then
			xmlcadena = xmlcadena + "</RAIZ>"
		else	
			xmlcadena = xmlcadena + "<PARAMETROS>"+ Parametros +"</PARAMETROS></RAIZ>"
		end if	

		GetXML = xmlCadena
	End Function
	
	Function GetAtributo( sCampo, sValue )
	    GetAtributo = sCampo + + "=" + chr(34) + sValue +  chr(34) + " "
	End Function

	
	Function ChecaRespuesta( strResultado, xslError )	
		dim xmlResultado	
		dim docXSL
		dim Nodos
			
		set xmlResultado = CreateObject("Msxml2.DOMDocument.4.0")
		xmlResultado.loadXML(strResultado)

		set Nodos = xmlResultado.documentElement.selectNodes( "//ERRORES" )
		set docXSL = Server.CreateObject("Msxml2.DOMDocument.4.0")
		docXSL.async = false
	
		if ( Nodos.Length = 0 ) then
			ChecaRespuesta = 1
			'docXSL.load(Server.MapPath(xslOk))	
			'Response.Write( " NO HAY " )
		else			
			docXSL.load(Server.MapPath(xslError))		
			Response.Write(xmlResultado.transformNode(docXSL))	
			'Response.Write( Nodos.Item( 0 ).Text )
		end if
		'Response.Write(strResultado)
		
	End Function
	
	Function ChecaRespuesta2( strResultado, xslOk, xslError )	
		dim xmlResultado	
		dim docXSL
		dim Nodos
			
		set xmlResultado = CreateObject("Msxml2.DOMDocument.4.0")
		xmlResultado.loadXML(strResultado)

		set Nodos = xmlResultado.documentElement.selectNodes( "//ERRORES" )
		set docXSL = Server.CreateObject("Msxml2.DOMDocument.4.0")
		docXSL.async = false
	
		if ( Nodos.Length = 0 ) then
			docXSL.load(Server.MapPath(xslOk))	
			'Response.Write( " NO HAY " )
		else			
			docXSL.load(Server.MapPath(xslError))					
			'Response.Write( Nodos.Item( 0 ).Text )
		end if
		'Response.Write(strResultado)
       Response.Write(xmlResultado.transformNode(docXSL))	
	End Function

	Function GetNodeEmpresa( strEmpresa )
		dim xmlResultado	
		dim docXSL
		dim Nodos
			
		set xmlResultado = CreateObject("Msxml2.DOMDocument.4.0")
		xmlResultado.loadXML(strEmpresa)

		set Nodos = xmlResultado.documentElement.selectNodes( "//EMPRESA/CODIGO" )
		set docXSL = Server.CreateObject("Msxml2.DOMDocument.4.0")
		docXSL.async = false
			
		if ( Nodos.Length <> 0 ) then
			GetNodeEmpresa = Nodos.Item( 0 ).Text 
		else
			GetNodeEmpresa = 0	
		end if
		
	End Function

%>
