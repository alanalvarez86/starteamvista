<?xml version='1.0' encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>
<xsl:template match="/">

<xsl:element name="link">
	<xsl:attribute name="rel">stylesheet</xsl:attribute>
	<xsl:attribute name="href">PlanCarrera.css</xsl:attribute>
	<xsl:attribute name="type">text/css</xsl:attribute>
</xsl:element>

<xsl:element name="title">Grupo Tress - Acci�n</xsl:element>

<center>
<font class="TITULO">
	<b>Acci�n</b>
</font>
<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>
<xsl:variable name="clase"><xsl:value-of select="//ACCION/ROWS/ROW/@AN_CLASE"/></xsl:variable>
<table>
	<tr>
		<td align="right">
			<font class="LETREROS">
				<b><xsl:value-of select="//ACCION/LABELS/@AN_CODIGO"/>:</b>
			</font>
		</td>
		<td>
			<xsl:value-of select="//ACCION/ROWS/ROW/@AN_CODIGO"/>
		</td>
	</tr>
	<tr>
		<td align="right">
			<font class="LETREROS">				
				<b><xsl:value-of select="//ACCION/LABELS/@CLASE"/>:</b>
			</font>
		</td>		
		<td>
			<xsl:value-of select="//ACCION/ROWS/ROW/@CLASE"/>
		</td>	
	</tr>
	<tr>
		<td align="right">
			<font class="LETREROS">
				<b><xsl:value-of select="//ACCION/LABELS/@AN_NOMBRE"/>:</b>
			</font>
		</td>
		<td>
			<xsl:value-of select="//ACCION/ROWS/ROW/@AN_NOMBRE"/>
		</td>	
	</tr>
	<tr>
		<td align="right">
			<font class="LETREROS">
				<b><xsl:value-of select="//ACCION/LABELS/@AN_DETALLE"/>:</b>
			</font>
		</td>
		<td>
			<xsl:value-of select="//ACCION/ROWS/ROW/@AN_DETALLE"/>
		</td>	
	</tr>
	<tr>
		<td align="right">
			<font class="LETREROS">
				<b><xsl:value-of select="//ACCION/LABELS/@AN_DIAS"/>:</b>
			</font>
		</td>
		<td>
			<xsl:value-of select="//ACCION/ROWS/ROW/@AN_DIAS"/>
		</td>	
	</tr>
	<xsl:variable name="Codigo"><xsl:value-of select="//ACCION/ROWS/ROW/@CU_CODIGO"/></xsl:variable>
	<xsl:variable name="LenCodigo"><xsl:value-of select="string-length($Codigo)"/></xsl:variable>	
	<xsl:choose>
		<xsl:when test="$clase=0">
			<tr>
				<td align="right">
					<font class="LETREROS">
						<b><xsl:value-of select="//ACCION/LABELS/@CU_CODIGO"/>:</b>
					</font>
				</td>
				<td>
					<xsl:choose>
						<xsl:when test="$LenCodigo!=0">
							<xsl:element name="a">
								<xsl:attribute name="href">Curso.asp?ItemNum=<xsl:value-of select="//ACCION/ROWS/ROW/@CU_CODIGO"/></xsl:attribute>
								<xsl:value-of select="//ACCION/ROWS/ROW/@CU_CODIGO"/><xsl:text> = </xsl:text><xsl:value-of select="//ACCION/ROWS/ROW/@CU_NOMBRE"/>
							</xsl:element>
						</xsl:when>
						<xsl:otherwise>
							* No se tiene registrado *
						</xsl:otherwise>
					</xsl:choose>
				</td>	
			</tr>
		</xsl:when>
		<xsl:when test="$clase=1">
			<xsl:variable name="URL"><xsl:value-of select="//ACCION/ROWS/ROW/@AN_URL"/></xsl:variable>
			<xsl:variable name="LenURL"><xsl:value-of select="string-length($URL)"/></xsl:variable>			
			<tr>
				<td align="right">
					<font class="LETREROS">
						<b><xsl:value-of select="//ACCION/LABELS/@AN_TIP_MAT"/>:</b>
					</font>
				</td>
				<td>
					<xsl:value-of select="//ACCION/ROWS/ROW/@AN_TIP_MAT"/>
				</td>	
			</tr>
			<tr>
				<td align="right">
					<font class="LETREROS">
						<b><xsl:value-of select="//ACCION/LABELS/@AN_URL"/>:</b>
					</font>
				</td>
				<td>
					<xsl:choose>
						<xsl:when test="LenURL!=0">
							<xsl:element name="a">
								<xsl:attribute name="href"><xsl:value-of select="//ACCION/ROWS/ROW/@AN_URL"/></xsl:attribute>
								<xsl:value-of select="//ACCION/ROWS/ROW/@AN_URL"/>
							</xsl:element>
						</xsl:when>
						<xsl:otherwise>
							* No se tiene registrado *						
						</xsl:otherwise>
					</xsl:choose>
				</td>	
			</tr>			
		</xsl:when>
	</xsl:choose>
</table>
<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>

<!--************** Competencias que Recomiendan esta Acci�n *******************-->
<table width="95%" >
<tr>
	<font class="SUBTITULO">
		<b>Competencias que recomiendan esta acci�n</b>
	</font>		
</tr>
</table>
<xsl:variable name="Cuantos"><xsl:value-of select="//COMP_MAP/ROWS/@cuantos"/></xsl:variable>
<xsl:choose>
	<xsl:when test="$Cuantos=0">
		<center>
			<font class="REG_ZERO">
				<b>* No hay datos *</b>
			</font>
		</center>
	</xsl:when>
	<xsl:otherwise>	
		<table class="TH_TEAL" width="95%">
			<tr>
				<th class="TH_TEAL"><xsl:value-of select="//COMP_MAP/LABELS/@CM_CODIGO"/></th>
				<th class="TH_TEAL"><xsl:value-of select="//COMP_MAP/LABELS/@CM_DESCRIP"/></th>	
			</tr>
			<xsl:for-each select="//COMP_MAP/ROWS/ROW">
			<tr>
				<td class="TD_COLOR"><font class="xsmallblack">
				<xsl:element name="a">
					<xsl:attribute name="href">Competencia.asp?ItemNum=<xsl:value-of select="@CM_CODIGO"/></xsl:attribute>
					<xsl:value-of select="@CM_CODIGO"/>
				</xsl:element></font></td>
				<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@CM_DESCRIP"/></font></td>
			</tr>
			</xsl:for-each>
		</table>
	</xsl:otherwise>
</xsl:choose>

<xsl:element name="br"/>

<!--************* Puestos que comtempan esta Acci�n ******************-->
<table width="95%" >
<tr>
	<font class="SUBTITULO">
		<b>Puestos que contemplan esta acci�n</b>
	</font>		
</tr>
</table>
<xsl:variable name="Cuantos2"><xsl:value-of select="//PUESTO/ROWS/@cuantos"/></xsl:variable>
<xsl:choose>
	<xsl:when test="$Cuantos2=0">
		<center>
			<font class="REG_ZERO">
				<b>* No hay datos *</b>
			</font>
		</center>
	</xsl:when>
	<xsl:otherwise>	
		<table class="TH_TEAL" width="95%">
			<tr>
				<th class="TH_TEAL"><xsl:value-of select="//PUESTO/LABELS/@PU_CODIGO"/></th>
				<th class="TH_TEAL"><xsl:value-of select="//PUESTO/LABELS/@PU_DESCRIP"/></th>	
			</tr>
			<xsl:for-each select="//PUESTO/ROWS/ROW">
			<tr>
				<td class="TD_COLOR"><font class="xsmallblack">	
				<xsl:element name="a">
					<xsl:attribute name="href">Puesto.asp?ItemNum=<xsl:value-of select="@PU_CODIGO"/></xsl:attribute>
					<xsl:value-of select="@PU_CODIGO"/>
				</xsl:element></font></td>	
				<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@PU_DESCRIP"/></font></td>
			</tr>
			</xsl:for-each>
		</table>
	</xsl:otherwise>
</xsl:choose>
<xsl:element name="br"/>

<table width="95%"> 
	<tr>
		<td>
			<xsl:element name="a">
				<xsl:attribute name="href">CumpliCompTot.asp?CLASE=<xsl:value-of select="//ACCION/ROWS/ROW/@AN_CLASE"/><xsl:text>&#38;</xsl:text>ACCION=<xsl:value-of select="//ACCION/ROWS/ROW/@AN_CODIGO"/></xsl:attribute>
				Cumplimiento de Compromisos
			</xsl:element>
		</td>
	</tr>
</table>

<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
 		<tr>
 			<td align="left">
 				<xsl:element name="a">
 					<xsl:attribute name="href">javascript:window.history.back()</xsl:attribute>
 					<xsl:element name="img">
 						<xsl:attribute name="src">back.gif</xsl:attribute> 
 						<xsl:attribute name="border">0</xsl:attribute>
 						<xsl:attribute name="alt">Pagina Anterior</xsl:attribute>
 					</xsl:element>
 				</xsl:element>	
 			</td>
     		<td align="right">
 				<xsl:element name="a">
 					<xsl:attribute name="href">PlanCarrera.asp</xsl:attribute>
 					<xsl:element name="img">
 						<xsl:attribute name="src">inicio.gif</xsl:attribute> 
 						<xsl:attribute name="border">0</xsl:attribute>
 						<xsl:attribute name="alt">Incio</xsl:attribute>
 					</xsl:element>
 				</xsl:element>	
 			</td>
		</tr>
</table>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
	<tr>
     	<td align="center">	
  		<xsl:element name="a">
 			<xsl:attribute name="href">PlanCarrera.asp</xsl:attribute>
				<xsl:element name="img">
 					<xsl:attribute name="src">tress_small.gif</xsl:attribute> 
 					<xsl:attribute name="border">0</xsl:attribute>
 				</xsl:element> 					
 			</xsl:element>	
 			<xsl:element name="br"/>
			Plan de Carrera
		</td>
	</tr>
</table>
	<p align="center">
		<font size="2" color="#333333" face="Verdana, Arial, Helvetica, sans-serif"> Copyright � 1998-2002 
			<a href="http://www.tress.com.mx">Grupo Tress</a> Todos los derechos reservados 
		</font>
	</p>
</center>	

</xsl:template>
</xsl:stylesheet>