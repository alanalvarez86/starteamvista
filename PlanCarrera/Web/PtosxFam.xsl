<?xml version='1.0' encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>
<xsl:template match="/">

<xsl:element name="link">
	<xsl:attribute name="rel">stylesheet</xsl:attribute>
	<xsl:attribute name="href">PlanCarrera.css</xsl:attribute>
	<xsl:attribute name="type">text/css</xsl:attribute>
</xsl:element>

<xsl:element name="title">Grupo Tress - Puestos por Familia</xsl:element>

<script language="javascript">
	function MuestraPtoxFam( oCombo )
	{
	var sNew
		if ( oCombo.value != T1.value )
		{
			sNew = oCombo.value;
			oCombo.value = T1.value;			
 	 		location.href="PtosxFam.asp?ItemNum="+sNew;	 	 		
		}			
	}
</script>

<center>
<font class="TITULO">
	<b><TITULO>Puestos por Familia</TITULO></b>
</font>
<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>
<xsl:element name="input">
	<xsl:attribute name="Type">hidden</xsl:attribute>
	<xsl:attribute name="name">T1</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of select="//FAM_PTO/COD_DEFAULT"/></xsl:attribute>
</xsl:element>
<table>
	<tr>
		<td align="right">	
				<font class="LETREROS">
					<b><xsl:value-of select="//FAM_PTO/LABELS/@FP_CODIGO"/>:</b>			
				</font>	
		</td>	
		<td>
			<xsl:element name="select">
				<xsl:attribute name="name">cboPtoxFam</xsl:attribute>
				<xsl:attribute name="ID">cboPtoxFam</xsl:attribute>
				<xsl:attribute name="OnChange">MuestraPtoxFam(this)</xsl:attribute>
				<xsl:variable name="valorDef"><xsl:value-of select="//FAM_PTO/COD_DEFAULT"/></xsl:variable>
					<xsl:for-each select="//FAM_PTO/ROWS/ROW">		   
					<xsl:element name="option">
						<xsl:attribute name="value">							
								<xsl:value-of select="@FP_CODIGO"/>						
						</xsl:attribute>					
						<xsl:if test="@FP_CODIGO=$valorDef">
							<xsl:attribute name="selected">yes</xsl:attribute>
						</xsl:if>					
						<xsl:value-of select="@FP_DESCRIP"/>
				</xsl:element>
				</xsl:for-each>				
			</xsl:element>
		</td>	
	</tr>
</table>
	
<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>

<xsl:element name="br"/>
<!--********************* PUESTOS ************************-->
<table width="95%">
	<tr align="right">
		<font class="SUBTITULO">
			<b>Puestos</b>
		</font> 
	</tr>
</table>	

<xsl:variable name="NumRegPto"><xsl:value-of select="//PUESTO/ROWS/@cuantos"/></xsl:variable>
<xsl:if test="$NumRegPto!=0">	
	<table class="TH_TEAL" width="95%"><!-- cellSpacing="1" cellPadding="10" width="95%" border="0" align="center">-->
		<tr>
			<th class="TH_TEAL"><xsl:value-of select="//PUESTO/LABELS/@PU_CODIGO"/></th>
			<th class="TH_TEAL"><xsl:value-of select="//PUESTO/LABELS/@PU_DESCRIP"/></th>
		</tr>
		<xsl:for-each select="//PUESTO/ROWS/ROW">
		<tr>
			<td class="TD_COLOR"><font class="xsmallblack">
				<xsl:element name="a">
					<xsl:attribute name="href">Puesto.asp?ItemNum=<xsl:value-of select="@PU_CODIGO"/></xsl:attribute>
					<xsl:value-of select="@PU_CODIGO"/>
				</xsl:element></font></td>
			<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@PU_DESCRIP"/></font></td>
		</tr>
		</xsl:for-each>
	</table>
</xsl:if>
<xsl:if test="$NumRegPto=0">	
	<xsl:element name="br"/>
	<font class="REG_ZERO">
		<b>* No hay datos *</b>
	</font>	
</xsl:if>

<xsl:element name="br"/>

<!-- *********************** COMPETENCIAS ********************-->
<table width="95%">
	<tr align="right">
		<font class="SUBTITULO">
			<b>Competencias</b>
		</font> 
	</tr>
</table>	

<xsl:variable name="NumRegComp"><xsl:value-of select="//COMP_FAM/ROWS/@cuantos"/></xsl:variable>
<xsl:if test="$NumRegComp!=0">	
	<table class="TH_TEAL" width="95%"><!-- cellSpacing="1" cellPadding="10" width="95%" border="0" align="center">-->
		<tr>
			<th class="TH_TEAL"><xsl:value-of select="//COMP_FAM/LABELS/@CM_CODIGO"/></th>
			<th class="TH_TEAL"><xsl:value-of select="//COMP_FAM/LABELS/@CM_DESCRIP"/></th>
			<th class="TH_TEAL"><xsl:value-of select="//COMP_FAM/LABELS/@CF_OBSERVA"/></th>
		</tr>
		<xsl:for-each select="//COMP_FAM/ROWS/ROW">
		<tr>
			<td class="TD_COLOR"><font class="xsmallblack">
				<xsl:element name="a">
					<xsl:attribute name="href">Competencia.asp?ItemNum=<xsl:value-of select="@CM_CODIGO"/></xsl:attribute>
					<xsl:value-of select="@CM_CODIGO"/>
				</xsl:element></font></td>
			<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@CM_DESCRIP"/></font></td>
			<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@CF_OBSERVA"/></font></td>
		</tr>
		</xsl:for-each>
	</table>
</xsl:if>
<xsl:if test="$NumRegComp=0">	
	<xsl:element name="br"/>
	<font class="REG_ZERO">
		<b>* No hay datos *</b>
	</font>	
</xsl:if>


<xsl:element name="br"/>

<table width="95%"> 
	<tr>
		<td>
			<xsl:element name="a">
				<xsl:attribute name="href">ComparaPtos.asp?FAMILIA=<xsl:value-of select="//FAM_PTO/COD_DEFAULT"/></xsl:attribute>
				Comparativo de puestos
			</xsl:element>
		</td>
	</tr>
</table>

<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
 		<tr>
 			<td align="left">
 				<xsl:element name="a">
 					<xsl:attribute name="href">javascript:window.history.back()</xsl:attribute>
 					<xsl:element name="img">
 						<xsl:attribute name="src">back.gif</xsl:attribute> 
 						<xsl:attribute name="border">0</xsl:attribute>
 						<xsl:attribute name="alt">Pagina Anterior</xsl:attribute>
 					</xsl:element>
 				</xsl:element>	
 			</td>
     		<td align="right">
 				<xsl:element name="a">
 					<xsl:attribute name="href">PlanCarrera.asp</xsl:attribute>
 					<xsl:element name="img">
 						<xsl:attribute name="src">inicio.gif</xsl:attribute> 
 						<xsl:attribute name="border">0</xsl:attribute>
 						<xsl:attribute name="alt">Incio</xsl:attribute>
 					</xsl:element>
 				</xsl:element>	
 			</td>
		</tr>
</table>

<table width="95%">
	<tr align="center">
		<td align="center">
   			<xsl:element name="a">
				<xsl:attribute name="href">PlanCarrera.asp</xsl:attribute>
			<xsl:element name="img">
				<xsl:attribute name="src">tress_small.gif</xsl:attribute> 
					<xsl:attribute name="border">0</xsl:attribute>
				</xsl:element> 					
			</xsl:element>	
			<xsl:element name="br"/>
			Plan de Carrera
		</td>
	</tr>	
</table>
	<p align="center">
		<font size="2" color="#333333" face="Verdana, Arial, Helvetica, sans-serif"> Copyright � 1998-2002 
			<a href="http://www.tress.com.mx">Grupo Tress</a> Todos los derechos reservados 
		</font>
	</p>
</center>	

</xsl:template>
</xsl:stylesheet>