<%@ Language=VBScript %>
<%option explicit%>
<!-- #include file="Tools.asp" -->
<%
	Response.Expires=0
	Response.Buffer=false
	
	'Declaramos Variables para Iniciar el COmponente y recibir el XML generado
	dim CursoXML	
	dim strCurso
	dim xmlCurso
	dim docXSL
        dim docXML

	'Declaramos Variables para Iniciar el COmponente y recibir el XML generado
	set CursoXML = Server.CreateObject("PlanCarrera.dmServerPlanCarrera")
	strCurso = CursoXML.GetCurso(GetXML("<CURSO>"+Request("CURSO")+"</CURSO>"))

        set docXML = Server.CreateObject("Msxml2.DOMDocument.4.0")
	docXML.async = false
        docXML.loadXML( strCurso )

	set docXSL = Server.CreateObject("Msxml2.DOMDocument.4.0")
	docXSL.async = false
	docXSL.load(Server.MapPath("PlanCarrera.xsl"))

        Response.Write(docXML.transformNode(docXSL))
%>
