<%@ Language=VBScript %>
<%option explicit%>
<!-- #include file="Tools.asp" -->
<%
	Response.Expires=0
	Response.Buffer=false
	
	'Declaramos Variables para Iniciar el COmponente y recibir el XML generado
	dim CursosXML	
	dim strCursos
	dim xmlCursos
	dim docXSL
	dim docXML

	'Declaramos Variables para Iniciar el COmponente y recibir el XML generado
	set CursosXML = Server.CreateObject("PlanCarrera.dmServerPlanCarrera")

	strCursos = CursosXML.GetCursos(GetXML("<TIPO>"+Request("TIPO")+"</TIPO>"))

	set docXML = Server.CreateObject("Msxml2.DOMDocument.4.0")
	docXML.async = false
        docXML.loadXML( strCursos )

	set docXSL = Server.CreateObject("Msxml2.DOMDocument.4.0")
	docXSL.async = false
	docXSL.load(Server.MapPath("PlanCarrera.xsl"))

        Response.Write(docXML.transformNode(docXSL))
%>
