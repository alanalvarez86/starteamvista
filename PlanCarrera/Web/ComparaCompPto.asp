<%@ Language=VBScript %>
<%option explicit%>
<!-- #include file="Tools.asp" -->
<%
	Response.Expires=0
	Response.Buffer=false

	'Declaramos Variables
	dim PlanCarreraXML
	dim strCompPuestos
	dim docXSL
	dim docXML
	dim strCodigo

	'Declaramos Variables para Iniciar el Componente y recibir el XML generado
	set PlanCarreraXML = Server.CreateObject("PlanCarrera.dmServerPlanCarrera")
	strCompPuestos = PlanCarreraXML.GetCompetenciasPuesto(GetXML("<EMPLEADO>"+Request("EMPLEADO")+"</EMPLEADO><PRETTYNAME>"+Request("PRETTYNAME")+"</PRETTYNAME><PUESTOACTUAL>"+Request("PUESTOACTUAL")+"</PUESTOACTUAL><DESCRIPCION>"+Request("DESCRIPCION")+"</DESCRIPCION><FAMILIA>"+Request("FAMILIA")+"</FAMILIA><PUESTO>"+Request("PUESTO")+"</PUESTO>"))
        'Response.write(strCompPuestos)

	set docXML = Server.CreateObject("Msxml2.DOMDocument.4.0")
	docXML.async = false
        docXML.loadXML(strCompPuestos)

	set docXSL = Server.CreateObject("Msxml2.DOMDocument.4.0")
	docXSL.async = false
	docXSL.load(Server.MapPath("PlanCarrera.xsl"))

        Response.Write(docXML.transformNode(docXSL))
%>

