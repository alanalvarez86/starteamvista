<%@ Language=VBScript %>
<%option explicit%>
<!-- #include file="Tools.asp" -->
<%
	Response.Expires=0
	Response.Buffer=false

	'Declaramos Variables
	dim PlanCarreraXML
	dim strCumpComproTot
	dim docXSL
	dim docXML

	'Declaramos Variables para Iniciar el Componente y recibir el XML generado
	set PlanCarreraXML = Server.CreateObject("PlanCarrera.dmServerPlanCarrera")
	strCumpComproTot= PlanCarreraXML.GetCumplimientoTotales(GetXML("<CLASE>"+Request("CLASE")+"</CLASE><ACCION>"+Request("ACCION")+"</ACCION>"))
        'Response.write(strCumpComproTot)

	set docXML = Server.CreateObject("Msxml2.DOMDocument.4.0")
	docXML.async = false
        docXML.loadXML(strCumpComproTot)

	set docXSL = Server.CreateObject("Msxml2.DOMDocument.4.0")
	docXSL.async = false
	docXSL.load(Server.MapPath("PlanCarrera.xsl"))

        Response.Write(docXML.transformNode(docXSL))
%>

