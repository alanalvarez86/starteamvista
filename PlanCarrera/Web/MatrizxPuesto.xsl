<?xml version='1.0' encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>
<xsl:template match="/">

<xsl:element name="link">
	<xsl:attribute name="rel">stylesheet</xsl:attribute>
	<xsl:attribute name="href">PlanCarrera.css</xsl:attribute>
	<xsl:attribute name="type">text/css</xsl:attribute>
</xsl:element>

<xsl:element name="title">Grupo Tress - Matriz de Entrenamiento por Puesto</xsl:element>

<center>
<font class="TITULO">
	<b>Matriz de Entrenamiento por Puesto</b>
</font>
<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>
<table>
	<tr>
		<td align="right">
			<font class="LETREROS">
				<b><xsl:value-of select="//PUESTO/LABELS/@PU_CODIGO"/>:</b>
			</font>
		</td>
		<td>
			<xsl:variable name="codPto"><xsl:value-of select="//PUESTO/ROWS/ROW/@PU_CODIGO"/></xsl:variable>
			<xsl:if test="string-length($codPto)!=0">
				<xsl:element name="a">
					<xsl:attribute name="href">Puesto.asp?ItemNum=<xsl:value-of select="$codPto"/></xsl:attribute>
					<xsl:value-of select="$codPto"/><xsl:text> = </xsl:text><xsl:value-of select="//PUESTO/ROWS/ROW/@PU_DESCRIP"/>
				</xsl:element>	
			</xsl:if>
			<xsl:if test="string-length($codPto)=0">	
				* No se tiene registrado *
			</xsl:if>
		</td>
	</tr>
	<tr>
		<td align="right">
			<font class="LETREROS">
				<b><xsl:value-of select="//PUESTO/LABELS/@FP_CODIGO"/>:</b>
			</font>
		</td>
		<td>
			<xsl:variable name="codFam"><xsl:value-of select="//PUESTO/ROWS/ROW/@FP_CODIGO"/></xsl:variable>
			<xsl:if test="string-length($codFam)!=0">
				<xsl:element name="a">
					<xsl:attribute name="href">PtosxFam.asp?ItemNum=<xsl:value-of select="$codFam"/></xsl:attribute>
					<xsl:value-of select="$codFam"/><xsl:text> = </xsl:text><xsl:value-of select="//PUESTO/ROWS/ROW/@FP_DESCRIP"/>
				</xsl:element>	
			</xsl:if>
			<xsl:if test="string-length($codFam)=0">	
				* No se tiene registrado *
			</xsl:if>
		</td>
	</tr>
	<tr>
		<td align="right">
			<font class="LETREROS">
				<b><xsl:value-of select="//PUESTO/LABELS/@NP_CODIGO"/>:</b>
			</font>
		</td>
		<td>
			<xsl:variable name="codNiv"><xsl:value-of select="//PUESTO/ROWS/ROW/@NP_CODIGO"/></xsl:variable>
			<xsl:if test="string-length($codNiv)!=0">
				<xsl:element name="a">
					<xsl:attribute name="href">PtosxNiv.asp?ItemNum=<xsl:value-of select="$codNiv"/></xsl:attribute>
					<xsl:value-of select="$codNiv"/><xsl:text> = </xsl:text><xsl:value-of select="//PUESTO/ROWS/ROW/@NP_DESCRIP"/>
				</xsl:element>	
			</xsl:if>
			<xsl:if test="string-length($codNiv)=0">	
				* No se tiene registrado *
			</xsl:if>
		</td>
	</tr>
</table>

<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>

<table width="95%">
	<tr>
		<font class="SUBTITULO">
			Curso
		</font>
	</tr>
</table>

<xsl:variable name="NumReg"><xsl:value-of select="//ENTRENA/ROWS/@cuantos"/></xsl:variable>
<xsl:if test="$NumReg!=0">
	<table class="TH_TEAL" width="95%">     		
		<tr>
			<th class="TH_TEAL"><xsl:value-of select="//ENTRENA/LABELS/@CU_CODIGO"/></th>
			<th class="TH_TEAL"><xsl:value-of select="//ENTRENA/LABELS/@CU_NOMBRE"/></th>
			<th class="TH_TEAL"><xsl:value-of select="//ENTRENA/LABELS/@MA_CODIGO"/></th>
			<th class="TH_TEAL"><xsl:value-of select="//ENTRENA/LABELS/@EN_DIAS"/></th>
		</tr>
		<xsl:for-each select="//ENTRENA/ROWS/ROW">
		<tr>
			<td class="TD_COLOR"><font class="xsmallblack">
				<xsl:element name="a">
					<xsl:attribute name="href">Curso.asp?ItemNum=<xsl:value-of select="@CU_CODIGO"/></xsl:attribute>
					<xsl:value-of select="@CU_CODIGO"/>
				</xsl:element></font></td>
			<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@CU_NOMBRE"/></font></td>	
			<td class="TD_COLOR"><font class="xsmallblack">
				<xsl:variable name="codMaestro"><xsl:value-of select="@MA_CODIGO"/></xsl:variable>
				<xsl:choose>
					<xsl:when test="string-length($codMaestro)!=0">
						<xsl:element name="a">
							<xsl:attribute name="href">Maestro.asp?ItemNum=<xsl:value-of select="@MA_CODIGO"/></xsl:attribute>
							<xsl:value-of select="@MA_CODIGO"/> = <xsl:value-of select="@MA_NOMBRE"/>
						</xsl:element>
					</xsl:when>
					<xsl:otherwise>
					</xsl:otherwise>
				</xsl:choose>
			</font></td>					
			<td class="TD_COLOR"><font class="xsmallblack"><xsl:value-of select="@EN_DIAS"/></font></td>
		</tr>
		</xsl:for-each>
	</table>
</xsl:if>
<xsl:if test="$NumReg=0">	
	<xsl:element name="br"/>
	<font class="REG_ZERO">
		<b>* No hay datos *</b>
	</font>	
</xsl:if>
<xsl:element name="br"/>

<xsl:element name="br"/>
<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
 		<tr>
 			<td align="left">
 				<xsl:element name="a">
 					<xsl:attribute name="href">javascript:window.history.back()</xsl:attribute>
 					<xsl:element name="img">
 						<xsl:attribute name="src">back.gif</xsl:attribute> 
 						<xsl:attribute name="border">0</xsl:attribute>
 						<xsl:attribute name="alt">Pagina Anterior</xsl:attribute>
 					</xsl:element>
 				</xsl:element>	
 			</td>
     		<td align="right">
 				<xsl:element name="a">
 					<xsl:attribute name="href">PlanCarrera.asp</xsl:attribute>
 					<xsl:element name="img">
 						<xsl:attribute name="src">inicio.gif</xsl:attribute> 
 						<xsl:attribute name="border">0</xsl:attribute>
 						<xsl:attribute name="alt">Incio</xsl:attribute>
 					</xsl:element>
 				</xsl:element>	
 			</td>
		</tr>
</table>

<table width="95%">
	<tr align="center">
		<td align="center">
   			<xsl:element name="a">
				<xsl:attribute name="href">PlanCarrera.asp</xsl:attribute>
			<xsl:element name="img">
				<xsl:attribute name="src">tress_small.gif</xsl:attribute> 
					<xsl:attribute name="border">0</xsl:attribute>
				</xsl:element> 					
			</xsl:element>	
			<xsl:element name="br"/>
			Plan de Carrera
		</td>
	</tr>	
</table>
	<p align="center">
		<font size="2" color="#333333" face="Verdana, Arial, Helvetica, sans-serif"> Copyright � 1998-2002 
			<a href="http://www.tress.com.mx">Grupo Tress</a> Todos los derechos reservados 
		</font>
	</p>
</center>	

</xsl:template>
</xsl:stylesheet>