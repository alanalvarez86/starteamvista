<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta NAME="GENERATOR" Content="Microsoft FrontPage 4.0">
<base target="main">
<link rel="stylesheet" href="PlanCarrera.css" type="text/css">
<title></title>
</head>

<body>

<center>
<p>
  <a href="http://www.tress.com.mx" title="P�gina Principal de Grupo Tress"><img border="0" src="tress_small.gif"></a>
  <br>
  Usuario: <%=Session("Nombre")%>
</p>
	<table class="TH_TEAL" border="0" width="95%">
  		<tr>
			<th class="TH_TEAL">Mi Plan de Carrera</th>
	  	</tr>
  		<tr>
    		<td class="TD_COLOR" width="200" align="center"><A href="/PlanCarrera/ComparaCompPto.asp?EMPLEADO=<%=Session("cb_codigo")%>"><font class="xsmallblack">Competencias vs. Mi Puesto</font></a></td>
	  	</tr>	  	
  		<tr>
    		<td class="TD_COLOR" width="200" align="center"><A href="/PlanCarrera/CompxEmpleado.asp?EMPLEADO=<%=Session("cb_codigo")%>"><font class="xsmallblack">Mis Competencias</font></a></td>
	  	</tr>
  		<tr>
    		<td class="TD_COLOR" width="200" align="center"><A href="/PlanCarrera/PlanAccxEmp.asp?EMPLEADO=<%=Session("cb_codigo")%>"><font class="xsmallblack">Mi Plan de Acci�n</font></a></td>
	  	</tr>
  		<tr>
    		<td class="TD_COLOR" width="200" align="center"><A href="/PlanCarrera/CursoTomEmp.asp?EMPLEADO=<%=Session("cb_codigo")%>"><font class="xsmallblack">Mis Cursos</font></a></td>
	  	</tr>
  		<tr>
    		<td class="TD_COLOR" width="200" align="center"><A href="/PlanCarrera/Empleado.asp?EMPLEADO=<%=Session("cb_codigo")%>"><font class="xsmallblack">Mis Datos(Empleado)</font></a></td>
	  	</tr>
	</table>
<br>
	<table class="TH_TEAL" border="0" width="95%">
  		<tr>
			<th class="TH_TEAL">Puestos</th>
	  	</tr>
	  	<tr>
   		 	<td class="TD_COLOR" width="200" align="center"><A href="/PlanCarrera/PtosxFam.asp"><font class="xsmallblack">Por Familia</font></a></td>
	  	</tr>
  		<tr>
    		<td class="TD_COLOR" width="200" align="center"><A href="/PlanCarrera/PtosxNiv.asp"><font class="xsmallblack">Por Nivel</font></a></td>
	  	</tr>
  		<tr>
    		<td class="TD_COLOR" width="200" align="center"><A href="/PlanCarrera/PtosxDim.asp"><font class="xsmallblack">Por Dimensi�n</font></a></td>
	  	</tr>
	</table>
<br>
	<table class="TH_TEAL" border="0" width="95%">
  		<tr>
			<th class="TH_TEAL">Cat�logos</th>
		</tr>
  		<tr>
    		<td class="TD_COLOR" width="200" align="center"><A href="/PlanCarrera/Competencias.asp"><font class="xsmallblack">Competencias</font></a></td>
  		</tr>
  		<tr>
    		<td class="TD_COLOR" width="200" align="center"><A href="/PlanCarrera/Acciones.asp"><font class="xsmallblack">Acciones</font></a></td>
  		</tr>
  		<tr>
    		<td class="TD_COLOR" width="200" align="center"><A href="/PlanCarrera/Cursos.asp"><font class="xsmallblack">Cursos</font></a></td>
  		</tr>
	</table>
<br>
	<table class="TH_TEAL" border="0" width="95%">
  		<tr>
			<th class="TH_TEAL">Anal�ticas</th>
		</tr>
  		<tr>
    		<td class="TD_COLOR" width="200" align="center"><A href="/PlanCarrera/ComparaPtos.asp"><font class="xsmallblack">Comparativo de Puestos</font></a></td>
  		</tr>
  		<tr>
    		<td class="TD_COLOR" width="200" align="center"><A href="/PlanCarrera/BusquedaCandidatos.asp?MOSTRAR=10"><font class="xsmallblack">B�squeda de Candidatos</font></a></td>
  		</tr>  		
  		<tr>
    		<td class="TD_COLOR" width="200" align="center"><A href="/PlanCarrera/DesemCompTot.asp"><font class="xsmallblack">Desempe�o por Competencia</font></a></td>
  		</tr>
  		<tr>
    		<td class="TD_COLOR" width="200" align="center"><A href="/PlanCarrera/CumpliCompTot.asp"><font class="xsmallblack">Cumplimiento de Compromisos</font></a></td>
  		</tr>
  		<tr>
    		<td class="TD_COLOR" width="200" align="center"><A href="/PlanCarrera/CursosProgTot.asp"><font class="xsmallblack">Cursos Programados</font></a></td>
  		</tr>
  		
	</table>
<br>
	<table width="95%" border="0" cellspacing="0" cellpadding="0">
 		<tr>
 			<td align="center"><a href="PlanCarrera.asp"><img src="/PlanCarrera/inicio.gif" border="0" alt="Inicio"/></a></td>
 		</tr>
	</table>
</center>
</body>

</html>
