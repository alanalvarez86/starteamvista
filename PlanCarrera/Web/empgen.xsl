<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" />
<xsl:template match="/">
	<EMPRESA>
		<CODIGO><xsl:value-of select="RAIZ/EMPRESA/CODIGO" /></CODIGO>
		<NOMBRE><xsl:value-of select="RAIZ/EMPRESA/NOMBRE" /></NOMBRE>
		<USERNAME><xsl:value-of select="RAIZ/EMPRESA/USERNAME" /></USERNAME>
		<PASSWORD><xsl:value-of select="RAIZ/EMPRESA/PASSWORD" /></PASSWORD>
		<NIVEL0><xsl:value-of select="RAIZ/EMPRESA/NIVEL0" /></NIVEL0>
		<DATOS><xsl:value-of select="RAIZ/EMPRESA/DATOS" /></DATOS>
	</EMPRESA>
</xsl:template>
</xsl:stylesheet>