<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:template match="/">
	<center>
		<table cellSpacing="0" cellPadding="0" width="95%" bgColor="#666666" border="0">
			<tr>
				<td>
					<div align="center"> 
						<table cellSpacing="1" cellPadding="10" width="100%" bgColor="#666666" border="0" align="center">     		
							<tr>
								<td bgColor="#ffffff" colSpan="3"><font face="Verdana, Arial, Helvetica, sans-serif" color="#333333"><b>Seleccionar Empresa</b></font>
								</td>
							</tr>
							<tr>
								<th  align="right">Codigo</th>
								<th  align="left">Nombre</th>
								<th> </th>
							</tr>
							<xsl:for-each select="raiz/EMPRESAS/EMPRESA">
								<tr>
									<td align="right" bgColor="#eeeeee"><font face="Verdana, Arial, Helvetica, sans-serif" color="#333333" size="2"><b><a href="javascript:MuestraPuesto()"><xsl:value-of select="Codigo"/></a></b></font>
									</td>
									<td bgColor="#dddddd"><font class="xsmallblack"><xsl:value-of select="Nombre"/></font>
									</td>
									<td bgColor="#dddddd"><INPUT id="BUTTON1" name="BUTTON1" onclick="MuestraPuestos()" type="button" value="Seleccionar"/></td>
								</tr>
							</xsl:for-each>
							<tr>
							  <td vAlign="top" bgColor="#ffffff" colSpan="3"><font face="Verdana, Arial, Helvetica, sans-serif" color="#0000ff" size="2"><b>Aqui se debe mostrar alguna leyenda.</b></font> </td></tr>
							<tr>
								<td bgColor="#cccccc" colSpan="3"><font face="Verdana, Arial, Helvetica, sans-serif" color="#333333" size="2"><b>Seleccionar Codigo de Empresa</b></font></td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
		</table>
	</center>
</xsl:template>
</xsl:stylesheet>