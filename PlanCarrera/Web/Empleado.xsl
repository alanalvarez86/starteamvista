<?xml version='1.0' encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>
<xsl:template match="/">

<xsl:element name="link">
	<xsl:attribute name="rel">stylesheet</xsl:attribute>
	<xsl:attribute name="href">PlanCarrera.css</xsl:attribute>
	<xsl:attribute name="type">text/css</xsl:attribute>
</xsl:element>

<xsl:element name="title">Grupo Tress - Empleado</xsl:element>

<center>
<font class="TITULO">
	<b>Empleado</b>
</font>
<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>
<table>
	<tr>
		<td align="right">
			<font class="LETREROS">
				<b><xsl:value-of select="//COLABORA/LABELS/@CB_CODIGO"/>:</b>
			</font>
		</td>
		<td>
			<xsl:value-of select="//COLABORA/ROWS/ROW/@CB_CODIGO"/>
		</td>
	</tr>
	<tr>
		<td align="right">
			<font class="LETREROS">
				<b><xsl:value-of select="//COLABORA/LABELS/@PRETTYNAME"/>:</b>
			</font>
		</td>
		<td>
			<xsl:value-of select="//COLABORA/ROWS/ROW/@PRETTYNAME"/>
		</td>
	</tr>
	<tr>
		<td align="right">
			<font class="LETREROS">
				<b><xsl:value-of select="//COLABORA/LABELS/@CB_PUESTO"/>:</b>
			</font>
		</td>
		<td>
			<xsl:variable name="codPto"><xsl:value-of select="//COLABORA/ROWS/ROW/@CB_PUESTO"/></xsl:variable>
			<xsl:if test="string-length($codPto)!=0">
				<xsl:element name="a">
					<xsl:attribute name="href">Puesto.asp?ItemNum=<xsl:value-of select="$codPto"/></xsl:attribute>
					<xsl:value-of select="$codPto"/><xsl:text> = </xsl:text><xsl:value-of select="//COLABORA/ROWS/ROW/@PU_DESCRIP"/>
				</xsl:element>	
			</xsl:if>
			<xsl:if test="string-length($codPto)=0">	
				* No se tiene registrado *
			</xsl:if>
		</td>
	</tr>
	<tr>
		<td align="right">
			<font class="LETREROS">
				<b><xsl:value-of select="//COLABORA/LABELS/@FP_CODIGO"/>:</b>
			</font>
		</td>
		<td>
			<xsl:variable name="codFam"><xsl:value-of select="//COLABORA/ROWS/ROW/@FP_CODIGO"/></xsl:variable>
			<xsl:if test="string-length($codFam)!=0">
				<xsl:element name="a">
					<xsl:attribute name="href">PtosxFam.asp?ItemNum=<xsl:value-of select="$codFam"/></xsl:attribute>
					<xsl:value-of select="$codFam"/><xsl:text> = </xsl:text><xsl:value-of select="//COLABORA/ROWS/ROW/@FP_DESCRIP"/>
				</xsl:element>	
			</xsl:if>
			<xsl:if test="string-length($codFam)=0">	
				* No se tiene registrado *
			</xsl:if>
		</td>
	</tr>
	<tr>
		<td align="right">
			<font class="LETREROS">
				<b><xsl:value-of select="//COLABORA/LABELS/@NP_CODIGO"/>:</b>
			</font>
		</td>
		<td>
			<xsl:variable name="codNiv"><xsl:value-of select="//COLABORA/ROWS/ROW/@NP_CODIGO"/></xsl:variable>
			<xsl:if test="string-length($codNiv)!=0">
				<xsl:element name="a">
					<xsl:attribute name="href">PtosxNiv.asp?ItemNum=<xsl:value-of select="$codNiv"/></xsl:attribute>
					<xsl:value-of select="$codNiv"/><xsl:text> = </xsl:text><xsl:value-of select="//COLABORA/ROWS/ROW/@NP_DESCRIP"/>
				</xsl:element>	
			</xsl:if>
			<xsl:if test="string-length($codNiv)=0">	
				* No se tiene registrado *
			</xsl:if>
		</td>
	</tr>
	<tr>
		<td align="right">
			<font class="LETREROS">
				<b><xsl:value-of select="//COLABORA/LABELS/@CB_FEC_NAC"/>:</b>
			</font>
		</td>
		<td>
			<xsl:value-of select="//COLABORA/ROWS/ROW/@CB_FEC_NAC"/>
		</td>
	</tr>
	<tr>
		<td align="right">
			<font class="LETREROS">
				<b><xsl:value-of select="//COLABORA/LABELS/@EDAD"/>:</b>
			</font>
		</td>
		<td>
			<xsl:value-of select="//COLABORA/ROWS/ROW/@EDAD"/>
		</td>
	</tr>
	<tr>
		<td align="right">
			<font class="LETREROS">
				<b><xsl:value-of select="//COLABORA/LABELS/@CB_FEC_ING"/>:</b>
			</font>
		</td>
		<td>
			<xsl:value-of select="//COLABORA/ROWS/ROW/@CB_FEC_ING"/>
		</td>
	</tr>
	<tr>
		<td align="right">
			<font class="LETREROS">
				<b><xsl:value-of select="//COLABORA/LABELS/@CB_NIVEL4"/>:</b>
			</font>
		</td>
		<td>
			<xsl:value-of select="//COLABORA/ROWS/ROW/@CB_NIVEL4"/>
		</td>
	</tr>
	<tr>
		<td align="right">
			<font class="LETREROS">
				<b><xsl:value-of select="//COLABORA/LABELS/@ANTIGUEDAD"/>:</b>
			</font>
		</td>
		<td>
			<xsl:value-of select="//COLABORA/ROWS/ROW/@ANTIGUEDAD"/>
		</td>
	</tr>	
</table>

<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>

<xsl:element name="br"/>

<table width="95%"> 
	<tr>
		<td>
			<xsl:element name="a">
				<xsl:attribute name="href">ComparaCompPto.asp?EMPLEADO=<xsl:value-of select="//COLABORA/ROWS/ROW/@CB_CODIGO"/><xsl:text>&#38;</xsl:text>PRETTYNAME=<xsl:value-of select="//COLABORA/ROWS/ROW/@PRETTYNAME"/><xsl:text>&#38;</xsl:text>PUESTOACTUAL=<xsl:value-of select="//COLABORA/ROWS/ROW/@CB_PUESTO"/><xsl:text>&#38;</xsl:text>DESCRIPCION=<xsl:value-of select="//COLABORA/ROWS/ROW/@PU_DESCRIP"/><xsl:text>&#38;</xsl:text>FAMILIA=<xsl:value-of select="//COLABORA/ROWS/ROW/@FP_CODIGO"/><xsl:text>&#38;</xsl:text>PUESTO=<xsl:value-of select="//COLABORA/ROWS/ROW/@CB_PUESTO"/></xsl:attribute>
				Competencias vs. Puesto
			</xsl:element>
		</td>
	</tr>
	<tr>
		<td>
			<xsl:element name="a">
				<xsl:attribute name="href">CompxEmpleado.asp?ItemNum=<xsl:value-of select="//COLABORA/ROWS/ROW/@CB_CODIGO"/></xsl:attribute>
				Competencias por Empleado
			</xsl:element>
		</td>
	</tr>
	<tr>
		<td>
			<xsl:element name="a">
				<xsl:attribute name="href">PlanAccxEmp.asp?ItemNum=<xsl:value-of select="//COLABORA/ROWS/ROW/@CB_CODIGO"/></xsl:attribute>
				Plan de Acci�n
			</xsl:element>
		</td>
	</tr>
	<tr>
		<td>
			<xsl:element name="a">
				<xsl:attribute name="href">CursoTomEmp.asp?ItemNum=<xsl:value-of select="//COLABORA/ROWS/ROW/@CB_CODIGO"/></xsl:attribute>
				Cursos Tomados
			</xsl:element>
		</td>
	</tr>
	<xsl:variable name="usCodigo"><xsl:value-of select="//COLABORA/ROWS/ROW/@US_CODIGO"/></xsl:variable>
	<xsl:if test="$usCodigo!=0">
	<tr>
		<td>
			<xsl:element name="a">
				<xsl:attribute name="href">Supervisor.asp?ItemNum=<xsl:value-of select="//COLABORA/ROWS/ROW/@CB_CODIGO"/></xsl:attribute><!--$usCodigo-->
				Mis Colaboradores
			</xsl:element>
		</td>
	</tr>
	</xsl:if>
</table>

<xsl:element name="br"/>

<xsl:element name="hr">
	<xsl:attribute name="width">95%</xsl:attribute>
</xsl:element>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
 		<tr>
 			<td align="left">
 				<xsl:element name="a">
 					<xsl:attribute name="href">javascript:window.history.back()</xsl:attribute>
 					<xsl:element name="img">
 						<xsl:attribute name="src">back.gif</xsl:attribute> 
 						<xsl:attribute name="border">0</xsl:attribute>
 						<xsl:attribute name="alt">Pagina Anterior</xsl:attribute>
 					</xsl:element>
 				</xsl:element>	
 			</td>
     		<td align="right">
 				<xsl:element name="a">
 					<xsl:attribute name="href">PlanCarrera.asp</xsl:attribute>
 					<xsl:element name="img">
 						<xsl:attribute name="src">inicio.gif</xsl:attribute> 
 						<xsl:attribute name="border">0</xsl:attribute>
 						<xsl:attribute name="alt">Incio</xsl:attribute>
 					</xsl:element>
 				</xsl:element>	
 			</td>
		</tr>
</table>

<table width="95%">
	<tr align="center">
		<td align="center">
   			<xsl:element name="a">
				<xsl:attribute name="href">PlanCarrera.asp</xsl:attribute>
			<xsl:element name="img">
				<xsl:attribute name="src">tress_small.gif</xsl:attribute> 
					<xsl:attribute name="border">0</xsl:attribute>
				</xsl:element> 					
			</xsl:element>	
			<xsl:element name="br"/>
			Plan de Carrera
		</td>
	</tr>	
</table>
	<p align="center">
		<font size="2" color="#333333" face="Verdana, Arial, Helvetica, sans-serif"> Copyright � 1998-2002 
			<a href="http://www.tress.com.mx">Grupo Tress</a> Todos los derechos reservados 
		</font>
	</p>
</center>	

</xsl:template>
</xsl:stylesheet>