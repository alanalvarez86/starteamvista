unit FClientTest;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ComObj, Mask, ComCtrls,
     Login_TLB,
     ZetaMessages,
     ZetaDBTextBox,
     ZetaNumero;

type
  TLoginThread = class(TThread)
  private
    { Private declarations }
    FHandle: HWND;
    FComputerName: String;
    procedure Failure;
    procedure Success;
    function CreateServer(const ClassID: TGUID): IDispatch;
  protected
    { Protected declarations }
    function GetServidorLogin: IdmServerLoginDisp;
    procedure Execute; override;
  public
    { Public declarations }
    constructor Create( MainForm: HWND; const sServer: String );
  end;
  TClientTester = class(TForm)
    ServidorLBL: TLabel;
    RemoteServerSeek: TSpeedButton;
    RemoteServer: TZetaTextBox;
    Probar: TBitBtn;
    Salir: TBitBtn;
    Veces: TZetaNumero;
    VecesLBL: TLabel;
    Repetir: TSpeedButton;
    Resultado: TStaticText;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure RemoteServerSeekClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ProbarClick(Sender: TObject);
    procedure RepetirClick(Sender: TObject);
  private
    { Private declarations }
    FOk: Integer;
    FAttempts: Integer;
    function CreateServer(const ClassID: TGUID): IDispatch;
    function GetServidorLogin: IdmServerLoginDisp;
    procedure WMWizardEnd(var Message: TMessage); message WM_WIZARD_END;
  public
    { Public declarations }
    procedure Start;
  end;

var
  ClientTester: TClientTester;

implementation


uses ZetaClientTools,
     ZetaCommonTools,
     ZetaRegistryCliente,
     ZetaDialogo;

{$R *.DFM}

{ ******* TLoginThread ****** }

constructor TLoginThread.Create( MainForm: HWND; const sServer: String );
begin
     FreeOnTerminate := True;
     FHandle := MainForm;
     FComputerName := sServer;
     inherited Create( True );
end;

procedure TLoginThread.Success;
begin
     if ( FHandle > 0 ) then
        Windows.PostMessage( FHandle, WM_WIZARD_END, 1, 0 );
end;

procedure TLoginThread.Failure;
begin
     if ( FHandle > 0 ) then
        Windows.PostMessage( FHandle, WM_WIZARD_END, 0, 0 );
end;

function TLoginThread.CreateServer( const ClassID: TGUID ): IDispatch;
begin
     ZetaClientTools.InitDCOM( False );
     Result := ComObj.CreateRemoteComObject( FComputerName, ClassID ) as IDispatch;
end;

function TLoginThread.GetServidorLogin: IdmServerLoginDisp;
begin
     Result := IdmServerLoginDisp( CreateServer( CLASS_dmServerLogin ) );
end;

procedure TLoginThread.Execute;
var
   Autorizacion: OleVariant;
begin
     try
        Autorizacion := GetServidorLogin.GetAuto;
        Synchronize( Success );
     except
           on Error: Exception do
           begin
                Synchronize( Failure );
           end;
     end;
end;

{ ********** TClientTester ********** }

procedure TClientTester.FormCreate(Sender: TObject);
begin
     ZetaRegistryCliente.InitClientRegistry;
     Veces.Valor := 100;
end;

procedure TClientTester.FormShow(Sender: TObject);
begin
     with ClientRegistry do
     begin
          RemoteServer.Caption := ClientRegistry.ComputerName;
     end;
end;

procedure TClientTester.FormDestroy(Sender: TObject);
begin
     ZetaRegistryCliente.ClearClientRegistry;
end;

{ ************* M�todos para DCOM ***************** }

function TClientTester.CreateServer( const ClassID: TGUID ): IDispatch;
begin
     Result := CreateRemoteComObject( ClientRegistry.ComputerName, ClassID ) as IDispatch;
end;

function TClientTester.GetServidorLogin: IdmServerLoginDisp;
begin
     Result := IdmServerLoginDisp( CreateServer( CLASS_dmServerLogin ) );
end;

{ ************* Eventos de botones ****************** }

procedure TClientTester.RemoteServerSeekClick(Sender: TObject);
begin
     with ClientRegistry do
     begin
          if BuscaServidor then
             RemoteServer.Caption := ComputerName;
     end;
end;

procedure TClientTester.ProbarClick(Sender: TObject);
var
    FServidor: IdmServerLoginDisp;
    Autorizacion: OleVariant;
    oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        FServidor := GetServidorLogin;
        try
           if Assigned( FServidor ) then
           begin
                try
                   Autorizacion := FServidor.GetAuto;
                   ZetaDialogo.zInformation( '� Exito !', 'El Servidor Remoto Existe y Est� Disponible', 0 );
                except
                      on Error: Exception do
                      begin
                           ZetaDialogo.zError( '� Error Al Invocar Servidor Remoto !', Error.Message, 0 );
                      end;
                end;
           end
           else
               ZetaDialogo.zError( '� Error !', 'Servidor No Fu� Encontrado', 0 );
        finally
               FServidor := nil;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TClientTester.RepetirClick(Sender: TObject);
var
   i, iTop: Integer;
   oCursor: TCursor;
   Proceso: TLoginThread;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        iTop := Veces.ValorEntero;
        Start;
        for i := 1 to iTop do
        begin
             Proceso := TLoginThread.Create( Self.Handle, ClientRegistry.ComputerName );
             with Proceso do
             begin
                  Resume;
             end;
        end;
        Resultado.Caption := Format( '%d Llamadas', [ iTop ] );
     finally
          Screen.Cursor := oCursor;
     end;
end;

procedure TClientTester.Start;
begin
     FOk := 0;
     FAttempts := 0;
     Resultado.Caption := '';
end;

procedure TClientTester.WMWizardEnd(var Message: TMessage);
begin
     with Message do
     begin
          Inc( FAttempts );
          case WParam of
               1: Inc( FOk );
          end;
          Resultado.Caption := Format( '%d / %d', [ FOk, FAttempts ] );
     end;
     Application.ProcessMessages;
end;


end.
