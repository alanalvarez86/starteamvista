program EmailTester;

uses
  Forms,
  FEmailTester in 'FEmailTester.pas' {EmailChecker},
  ZetaWinAPITools in '..\Tools\ZetaWinAPITools.pas',
  ZetaCommonTools in '..\Componentes\ZetaCommonTools.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.HelpFile := 'Tress.hlp>CONTEXT';
  Application.CreateForm(TEmailChecker, EmailChecker);
  Application.Run;
end.
