unit ZArbolTress;

interface

uses SysUtils, ComCtrls, Dialogs, DB, Controls,
     ZArbolTools, ZNavBarTools, ZetaCommonLists,cxTreeView;

function  GetNodoInfo( const iNodo: Integer ): TNodoInfo;
procedure CreaArbolDefault( oArbolMgr: TArbolMgr );
procedure CreaNavBarDefault ( oNavBarMgr: TNavBarMgr ; Arbolitos: array of TcxTreeView );
function GetGrupoInfo( const iNodo: Integer ): TGrupoInfo;
procedure CreaArbolitoDefault( oArbolMgr: TArbolMgr ; iAccesoGlobal: Integer );

implementation

uses ZetaDespConsulta,
     ZetaCommonTools,
     ZAccesosMgr,
     ZAccesosTress,
     ZGlobalTress,
     DGlobal,
     DSistema,
     DCliente;

const
     { Reporteador }
     K_REPORTEADOR                      = 1; { MENU }
     K_REPORTES                         = 3;
     K_CONS_SQL                         = 10;
     K_CONS_PROCESOS                    = 9;
     K_CONS_BITACORA                    = 15;

     { Configuraciones }
     K_CONFIGURACION                    = 2; { "MENU" }
     K_CLASIFICACIONES                  = 4;
     K_DICCIONARIO                      = 6;
     K_LISTAS                           = 5;
     K_MODULOS                          = 8;
     K_CONF_PROCESO                     = 16;
     //K_CONDICIONES                      = 7; //@gbeltran: "Catalogo de Condiciones" Se quito de la solucion.

    { Sistema }
     K_SISTEMA                          = 11; { MENU }
     K_SIST_DATOS                       = 12;
     K_SIST_DATOS_USUARIOS              = 13;
     K_SIST_DATOS_GRUPOS                = 14;


function Forma( const sCaption: String; const eTipo: eFormaConsulta ): TNodoInfo;
begin
     Result.EsForma   := TRUE;
     Result.Caption   := sCaption;
     Result.TipoForma := eTipo;
end;

function Folder( const sCaption: String; const iDerechos: Integer ): TNodoInfo;
begin
     Result.EsForma   := FALSE;
     Result.Caption   := sCaption;
     Result.IndexDerechos  := iDerechos;
end;


function Grupo( const sCaption: String; const iDerechos: Integer ): TGrupoInfo;
begin
     Result.Caption := sCaption;
     Result.IndexDerechos := iDerechos;
end;


function GetNodoInfo( const iNodo: Integer ): TNodoInfo;
begin
     //with GLobal do
     //begin
          case iNodo of
               { ** REPORTES ** }
               K_REPORTEADOR            : Result := Folder( 'Consultas', D_CR_REPORTEADOR );
               K_REPORTES               : Result := Forma( 'Reportes', efcReportes );
               K_CONS_SQL               : Result := Forma( 'SQL', efcQueryGral );
               K_CONS_BITACORA          : Result := Forma( 'Bit�cora', efcSistBitacora );
               K_CONS_PROCESOS          : Result := Forma( 'Bit�cora de Procesos', efcSistProcesos );

               { ** Catalogos ** }
               K_CONFIGURACION          : Result := Folder( 'Configuraci�n de Cat�logos', D_CR_CONFIGURACION );
               K_CLASIFICACIONES        : Result := Forma( 'Cat�logo de Clasificaciones', efcClasificaciones );
               K_DICCIONARIO            : Result := Forma( 'Diccionario de Datos', efcDiccion );
               K_LISTAS                 : Result := Forma( 'Cat�logo de Listas de Valores', efcListas );
               //K_CONDICIONES            : Result := Forma( 'Cat�logo de Condiciones', efcCatCondiciones ); //@gbeltran: Se quito de la solucion
               K_MODULOS                : Result := Forma( 'Cat�logo de M�dulos', efcModulos );
               //K_CONF_PROCESO           : Result := Forma( 'Procesos', efcProceso ); //@gbeltran: Se paso al ribbon

               { ** SISTEMA ** }
               K_SISTEMA                : Result := Folder( 'Sistema', D_CR_SISTEMA );
               K_SIST_DATOS_GRUPOS      : Result := Forma( 'Grupos de Usuarios', efcSistGrupos );
               K_SIST_DATOS             : Result := Folder( 'Datos', iNodo );
               K_SIST_DATOS_USUARIOS    : Result := Forma( 'Usuarios', efcSistUsuarios );


            else
                Result := Folder( '', 0 );
            end;
     //end;
end;


function GetGrupoInfo( const iNodo: Integer ): TGrupoInfo;
begin
     case iNodo of
         // Reporteador
          D_CR_REPORTEADOR     : Result := Grupo( 'Consultas', iNodo );
         // Configuraci�n de Cat�logos
          D_CR_CONFIGURACION  : Result := Grupo( 'Configuraci�n de Cat�logos', iNodo );
          // Sistema
          D_CR_SISTEMA         : Result := Grupo( 'Sistema', iNodo );
     else
          Result := Grupo( '', 0 );
     end;
end;


procedure CreaArbolDefault( oArbolMgr : TArbolMgr );

function NodoNivel( const iNivel, iNodo: Integer ): Boolean;
begin
     Result := oArbolMgr.NodoNivel( iNivel, GetNodoInfo( iNodo ), iNodo );
end;
begin
     NodoNivel( 0, K_REPORTEADOR );
       NodoNivel( 1, K_REPORTES );
       NodoNivel( 1, K_CONS_SQL );
       NodoNivel( 1, K_CONS_BITACORA );
       NodoNivel( 1, K_CONS_PROCESOS );
     NodoNivel( 0, K_CONFIGURACION );
       NodoNivel( 1, K_CLASIFICACIONES );
       NodoNivel( 1, K_DICCIONARIO );
       NodoNivel( 1, K_LISTAS );
       { NodoNivel( 1, K_CONDICIONES ); //@gbeltran: Se quito de la solucion }
       NodoNivel( 1, K_MODULOS );
        { NodoNivel( 0, K_CONF_PROCESO ); //@gbeltran: Se agrego al ribbon }
     NodoNivel(0,K_SISTEMA);
       //NodoNivel(1,K_SIST_DATOS);
         NodoNivel(1,K_SIST_DATOS_GRUPOS);
         NodoNivel(1,K_SIST_DATOS_USUARIOS);

end;

procedure CreaArbolitoDefault( oArbolMgr : TArbolMgr; iAccesoGlobal: Integer );
function NodoNivel( const iNivel, iNodo: Integer ): Boolean;
begin
     Result := oArbolMgr.NodoNivel( iNivel, GetNodoInfo( iNodo ), iNodo); //Siempre mandar 0 en iNodo
end;
begin
      case iAccesoGlobal of
            K_REPORTEADOR		:
            begin
                 NodoNivel( 0, K_REPORTES );
                 NodoNivel( 0, K_CONS_SQL );
                 NodoNivel( 0, K_CONS_BITACORA );
                 NodoNivel( 0, K_CONS_PROCESOS );
            end;
           K_CONFIGURACION      :
           begin
                 NodoNivel( 0, K_CLASIFICACIONES  );
                 NodoNivel( 0, K_DICCIONARIO );
                 NodoNivel( 0, K_LISTAS );
                 { NodoNivel( 0, K_CONDICIONES ); } //@gbeltran: Se quito de la solucion
                 NodoNivel( 0, K_MODULOS );
            end;
            K_SISTEMA               :
            begin
                 NodoNivel( 0, K_SIST_DATOS_GRUPOS );
                 //NodoNivel( 0, K_SIST_DATOS_USUARIOS );
            end;
      end;
end;

procedure CreaNavBarDefault ( oNavBarMgr: TNavBarMgr; Arbolitos: array of TcxTreeView);
var
   NodoRaiz: TGrupoInfo;
function NodoNivel( iNodo, iGroupIndex: Integer): Boolean;
begin
     Result := oNavBarMgr.NodoNivel( GetGrupoInfo( iNodo ), iGroupIndex, iNodo );
end;
procedure CreaArbolito( const K_GLOBAL:Integer; Arbolito: TcxTreeView);
var
   oArbolMgr: TArbolMgr;
begin
     with Arbolito do
     begin
          Parent := oNavBarMgr.NavBar.Groups[oNavBarMgr.UltimoGrupo.Index].Control;
          Align := alClient;
     end;
     oArbolMgr := TArbolMgr.Create( Arbolito );
     with oArbolMgr do
     begin
          try
             with Arbolito.Items do
             begin
                  BeginUpdate;
                  Clear;
                  try
                     // Aqu� se agrega el arbol definido por el usuario
                     //CreaArbolitoUsuario( oArbolMgr, '',K_GLOBAL );
                     //Antes metodo: CreaArbolUsuario
                     with oArbolMgr do
                     begin
                           //Configuracion := TRUE;
                           //oArbolMgr.NumDefault := K_GLOBAL ;
                           CreaArbolitoDefault( oArbolMgr, K_GLOBAL  );
                           Arbol_DevEx.FullCollapse;//edit by mp: el arbol dentro del navbar es el arbol_DevEx
                     end;
                  finally
                     EndUpdate;
                  end;
             end;
          finally
             Free;
          end;
     end;
end;
begin
     if( NodoNivel( D_CR_REPORTEADOR, 0 )) then
                CreaArbolito ( K_REPORTEADOR, Arbolitos[0]);
     // ******** Consultas ***********
     if( NodoNivel( D_CR_CONFIGURACION, 1 )) then
                CreaArbolito ( K_CONFIGURACION, Arbolitos[1]);
     // ******* Cat�logos *************
     if( NodoNivel( D_CR_SISTEMA, 2 ) ) then
                CreaArbolito ( K_SISTEMA, Arbolitos[2]);

     if ( oNavBarMgr.NavBar.Groups.Count = 0 ) then
     begin
        with NodoRaiz do
          begin
            Caption := 'No tiene derechos';
            IndexDerechos := K_SIN_RESTRICCION;
            oNavBarMgr.NodoNivel(NodoRaiz,0, K_ARBOL_NODO_RAIZ)
        end;
     end
end;

end.



