inherited DialogoLista_DevEx: TDialogoLista_DevEx
  Left = 580
  Top = 291
  Caption = 'DialogoLista_DevEx'
  ClientHeight = 291
  ClientWidth = 248
  OldCreateOrder = True
  OnShow = FormShow
  ExplicitWidth = 254
  ExplicitHeight = 320
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 255
    Width = 248
    ExplicitTop = 255
    ExplicitWidth = 248
    DesignSize = (
      248
      36)
    inherited OK_DevEx: TcxButton
      Left = 79
      OnClick = OK_DevExClick
      ExplicitLeft = 79
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 160
      ParentFont = False
      ExplicitLeft = 160
    end
  end
  object cbLista: TCheckListBox [1]
    Left = 0
    Top = 0
    Width = 248
    Height = 255
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 13
    ParentFont = False
    TabOrder = 1
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
