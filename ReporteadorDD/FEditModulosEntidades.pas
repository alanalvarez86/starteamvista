unit FEditModulosEntidades;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FDialogoLista, StdCtrls, CheckLst, Buttons, ExtCtrls;

type
  TEditModulosEntidades = class(TDialogoLista)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EditModulosEntidades: TEditModulosEntidades;

implementation
uses
    DDiccionario;
{$R *.dfm}

procedure TEditModulosEntidades.FormCreate(Sender: TObject);
begin
     inherited;
     with dmDiccionario do
     begin
          DataSetChecked:= cdsEntidadesPorModulo;
          DataSetInicial:= cdsModulos;
     end;
end;

end.
