unit FProcesos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, ToolWin, Db, ExtCtrls, ImgList,
     ZBaseConsulta,
     ZBaseConsultaBotones;

type
  TConfProcesos = class(TBaseBotones)
    btnImportar: TToolButton;
    btnDepurar: TToolButton;
    btnValidar: TToolButton;
    btnExportar: TToolButton;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ConfProcesos: TConfProcesos;

implementation

uses FTressShell,
     ZetaCommonClasses;

{$R *.DFM}

procedure TConfProcesos.FormCreate(Sender: TObject);
begin
     inherited;
     //HelpContext:= 
end;

end.
