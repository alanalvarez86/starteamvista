unit FSistEditAccesos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEditAccesos_DevEx, ImgList, Buttons, ZetaDBTextBox, StdCtrls, ComCtrls,
  ExtCtrls, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus,
  dxBarBuiltInMenu, cxControls, cxContainer, cxEdit, cxTreeView, cxPC, cxButtons;

type
  TSistEditAccesos_DevEx = class(TZetaEditAccesos_DevEx)
    Entidades: TcxTabSheet;
    ArbolEntidades: TcxTreeView;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure PageControlChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    procedure ApagaPrendePadres;
  protected
    function ArbolClasifi: TcxTreeview;
    function Clasificaciones: TcxTabSheet;
    function GetTipoDerecho(Nodo: TTreeNode): eDerecho; override;
    function TextoEspecial(const sText: String): Boolean; override;
    function GetArbol:TcxTreeView; override;
    procedure ArbolDefinir; override;
    procedure Cargar;override;
    procedure CopiaDerechos;override;
    function GetImageIndex: integer;override;
  public
    { Public declarations }
  end;

var
  SistEditAccesos_DevEx: TSistEditAccesos_DevEx;

implementation

uses ZAccesosTress, DBaseSistema,DDiccionario, DSistema, FEscogeGrupoEmpresa_DevEx;

{$R *.DFM}

function TSistEditAccesos_DevEx.GetArbol: TcxTreeView;
begin
     case ArbolSeleccionado of
          adEntidades: Result := ArbolEntidades
          else Result := ArbolBase;
     end;
end;

function TSistEditAccesos_DevEx.GetTipoDerecho(Nodo: TTreeNode): eDerecho;
begin
    with Nodo do
    begin
      if ( Text = 'Entidades' ) then
        Result := edConsulta
      else
        Result := inherited GetTipoDerecho( Nodo );
    end;
    { if ( Nodo.TreeView = ArbolEntidades ) then
        Result := edConsulta
     else
         Result := inherited GetTipoDerecho( Nodo );}
end;


function TSistEditAccesos_DevEx.TextoEspecial(const sText: String): Boolean;
begin
     if dmSistema.ArbolSeleccionado = adClasifi then
     begin
          Result := inherited TextoEspecial( sText );
     end
     else
     begin
          Result := (sText = '< Sin Módulo >') or
                    dmDiccionario.cdsModulos.Locate( 'MO_NOMBRE',sText,[] ) or
                    inherited TextoEspecial( sText );
     end;
end;

procedure TSistEditAccesos_DevEx.FormShow(Sender: TObject);
begin
     ArbolSeleccionado:= adClasifi;
     dmDiccionario.cdsModulos.Conectar;
     inherited;
     PageControl.ActivePage := Clasificaciones;
end;

procedure TSistEditAccesos_DevEx.PageControlChange(Sender: TObject);
begin
     inherited;
     if PageControl.ActivePage = tsTress then
        ArbolSeleccionado := adClasifi
     else
         ArbolSeleccionado := adEntidades;
end;

procedure TSistEditAccesos_DevEx.ArbolDefinir;
var
   iLastModulo : Integer;
begin
     ArbolSeleccionado:= adClasifi;
     ArbolClasifi.Items.Clear;

     with dmDiccionario.cdsClasificaciones do
     begin
          dmDiccionario.ConectaClasificacionesGrupoAdmin( dmSistema.GetEmpresaAccesos );
          First;
          while (not EOF) do
          begin
               AddElemento(0, FieldByName('RC_CODIGO').AsInteger, FieldByName('RC_NOMBRE').AsString);
                  AddDerechoConsulta;
                  AddDerechoAlta;
                  AddDerechoBaja;
                  AddDerechoEdicion;
                  AddDerechoImpresion;
               Next;
          end;
     end;

     iLastModulo := -1;
     ArbolSeleccionado:= adEntidades;
     ArbolEntidades.Items.Clear;
     with dmDiccionario.cdsEntidadesPorModulo do
     begin
          //Conectar;
          dmDiccionario.ConectaEntidadesPorModulo( dmSistema.GetEmpresaAccesos );
          First;
          while (not EOF) do
          begin
               if ( (iLastModulo <> FieldByName('MO_CODIGO').AsInteger)) then
               begin
                    iLastModulo := FieldByName('MO_CODIGO').AsInteger;
                    AddElemento(0, GetImageIndex, FieldByName('MO_NOMBRE').AsString);
               end;

               AddElemento(1, FieldByName('EN_CODIGO').AsInteger, FieldByName('EN_TITULO').AsString + '  ( ' + FieldByName('EN_TABLA').AsString + ' )' );
                  {AddDerechoConsulta;
                  AddDerechoAlta;
                  AddDerechoBaja;
                  AddDerechoEdicion;
                  AddDerechoImpresion;}
               Next;
          end;
     end;
     ArbolSeleccionado:= adClasifi;
end;

procedure TSistEditAccesos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ArbolConstruir;
end;

procedure TSistEditAccesos_DevEx.OKClick(Sender: TObject);
begin
     ArbolSeleccionado:= adClasifi;
     Descargar;
     ArbolSeleccionado:= adEntidades;
     Descargar;
end;


procedure TSistEditAccesos_DevEx.Cargar;
var
   oCursor: TCursor;
   sGroupName, sCompanyName: String;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        dmSistema.ConectaAccesos( sGroupName, sCompanyName );
        ArbolSeleccionado:= adClasifi;
        RefrescaArbol( dmSistema.BuscaDerechoClasificaciones );
        ArbolSeleccionado:= adEntidades;
        try
           RefrescaArbol( dmSistema.BuscaDerechoEntidades );
           ApagaPrendePadres;
        finally
               PageControl.ActivePage := Clasificaciones;
               ArbolSeleccionado:= adClasifi;
        end;
        Grupo.Caption := sGroupName;
        Company.Caption := sCompanyName;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TSistEditAccesos_DevEx.ApagaPrendePadres;
var
   oPadre: TTreeNode;

   procedure CambiaImagenParent;
   var
      oHijo: TTreeNode;
   begin
         oPadre.StateIndex:= K_UN_CHECK;
         oHijo:= oPadre.GetNext;
         repeat
               if oHijo.StateIndex = K_CHECK then
                  oPadre.StateIndex:= K_CHECK;
               oHijo:= oHijo.GetNext;
         until( oPadre.StateIndex = K_CHECK ) or ( oHijo = NIL ) or ( oHijo.HasChildren );
   end;

begin
      with ArbolEntidades do
      begin
           Items.BeginUpdate;
           oPadre := ArbolEntidades.Items[ 0 ];
           repeat
                 if ( ( oPadre.HasChildren ) ) then
                      CambiaImagenParent;
                 oPadre:= oPadre.GetNext;
           until ( oPadre = nil );
           Items.EndUpdate;
      end;

end;

function TSistEditAccesos_DevEx.ArbolClasifi: TcxTreeview;
begin
     Result := ArbolBase;
end;

function TSistEditAccesos_DevEx.Clasificaciones: TcxTabSheet;
begin
     Result := tsTress;
end;

function TSistEditAccesos_DevEx.GetImageIndex: integer;
begin
     Result := K_IMAGENINDEXRDD;
end;


procedure TSistEditAccesos_DevEx.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     inherited;
     dmDiccionario.cdsClasificaciones.Close;
     dmDiccionario.cdsEntidadesPorModulo.Close;
end;

{AV: Inicio de integración para solucionar el defecto 1896}
procedure TSistEditAccesos_DevEx.CopiaDerechos;
var
   oCursor: TCursor;
   iGrupo: Integer;
   sEmpresa: String;
begin
     case ArbolSeleccionado of
          adClasifi:
          begin
               if FEscogeGrupoEmpresa_DevEx.SeleccionaGrupoEmpresa( iGrupo, sEmpresa ) then
               begin
                    oCursor := Screen.Cursor;
                    Screen.Cursor := crHourglass;
                    try
                       dmSistema.CopyAccesos( iGrupo, sEmpresa );
                       RefrescaArbol( dmSistema.BuscaDerechoCopiadoClasificaciones );
                       SetEditState;
                    finally
                           Screen.Cursor := oCursor;
                    end;
               end;
          end;
          adEntidades:
          begin
               if FEscogeGrupoEmpresa_DevEx.SeleccionaGrupoEmpresa( iGrupo, sEmpresa ) then
               begin
                    oCursor := Screen.Cursor;
                    Screen.Cursor := crHourglass;
                    try
                       dmSistema.CopyAccesos( iGrupo, sEmpresa );
                       RefrescaArbol( dmSistema.BuscaDerechoCopiadoEntidades );
                       SetEditState;
                       {$ifdef RDD}
                       ApagaPrendePadres;
                       {$endif}
                    finally
                           Screen.Cursor := oCursor;
                    end;
               end;
          end;
     end;
end;
{AV: Fin de integración para solucionar el defecto 1896}

end.

