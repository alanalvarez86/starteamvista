inherited EditCampos: TEditCampos
  Left = 335
  Top = 225
  Anchors = [akTop, akRight]
  Caption = 'Editar Campos'
  ClientHeight = 464
  ClientWidth = 465
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lbCampo: TLabel [0]
    Left = 78
    Top = 65
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Campo:'
  end
  object Label1: TLabel [1]
    Left = 83
    Top = 90
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'T'#237'tulo:'
  end
  object Label3: TLabel [2]
    Left = 40
    Top = 189
    Width = 74
    Height = 13
    Alignment = taRightJustify
    Caption = 'Palabras Clave:'
  end
  object Label2: TLabel [3]
    Left = 46
    Top = 164
    Width = 68
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nombre Corto:'
  end
  object Label4: TLabel [4]
    Left = 39
    Top = 41
    Width = 75
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo de Campo:'
  end
  object Label6: TLabel [5]
    Left = 70
    Top = 214
    Width = 44
    Height = 13
    Alignment = taRightJustify
    Caption = 'Mascara:'
  end
  object Label5: TLabel [6]
    Left = 80
    Top = 238
    Width = 34
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ancho:'
  end
  object Label7: TLabel [7]
    Left = 55
    Top = 128
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descripci'#243'n:'
  end
  object Label8: TLabel [8]
    Left = 8
    Top = 263
    Width = 106
    Height = 13
    Alignment = taRightJustify
    Caption = 'Criterio de totalizaci'#243'n:'
  end
  object Label9: TLabel [9]
    Left = 55
    Top = 362
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor activo:'
  end
  object lbTexto: TLabel [10]
    Left = 91
    Top = 338
    Width = 23
    Height = 13
    Alignment = taRightJustify
    Caption = 'texto'
  end
  object Label11: TLabel [11]
    Left = 53
    Top = 288
    Width = 61
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo de filtro:'
  end
  object Label10: TLabel [12]
    Left = 10
    Top = 313
    Width = 104
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor default del Filtro:'
  end
  object Label12: TLabel [13]
    Left = 74
    Top = 385
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'Versi'#243'n:'
    Enabled = False
  end
  object US_CODIGO: TZetaDBTextBox [14]
    Left = 116
    Top = 404
    Width = 200
    Height = 17
    AutoSize = False
    Caption = 'US_CODIGO'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'US_CODIGO'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object Label13: TLabel [15]
    Left = 69
    Top = 405
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Modific'#243':'
    Enabled = False
  end
  inherited PanelBotones: TPanel
    Top = 428
    Width = 465
    TabOrder = 19
    inherited Cancelar: TBitBtn
      Left = 384
    end
  end
  inherited PanelSuperior: TPanel
    Width = 465
    TabOrder = 0
  end
  object AT_TIPO: TZetaDBKeyCombo [18]
    Left = 116
    Top = 37
    Width = 145
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 1
    ListaFija = lfTipoGlobal
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    DataField = 'AT_TIPO'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object AT_TCORTO: TDBEdit [19]
    Left = 116
    Top = 161
    Width = 300
    Height = 21
    DataField = 'AT_TCORTO'
    DataSource = DataSource
    TabOrder = 8
  end
  object AT_CLAVES: TDBEdit [20]
    Left = 116
    Top = 186
    Width = 300
    Height = 21
    CharCase = ecUpperCase
    DataField = 'AT_CLAVES'
    DataSource = DataSource
    TabOrder = 9
  end
  object AT_CAMPO: TZetaDBEdit [21]
    Left = 116
    Top = 61
    Width = 145
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 10
    TabOrder = 4
    DataField = 'AT_CAMPO'
    DataSource = DataSource
  end
  object AT_MASCARA: TDBEdit [22]
    Left = 116
    Top = 211
    Width = 145
    Height = 21
    DataField = 'AT_MASCARA'
    DataSource = DataSource
    TabOrder = 10
  end
  object AT_ANCHO: TZetaDBNumero [23]
    Left = 116
    Top = 234
    Width = 121
    Height = 21
    Mascara = mnDias
    TabOrder = 11
    Text = '0'
    DataField = 'AT_ANCHO'
    DataSource = DataSource
  end
  object AT_SISTEMA: TDBCheckBox [24]
    Left = 295
    Top = 54
    Width = 121
    Height = 17
    Alignment = taLeftJustify
    Caption = #191'Campo de Sistema?:'
    DataField = 'AT_SISTEMA'
    DataSource = DataSource
    TabOrder = 3
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  object AT_TOTAL: TZetaDBKeyCombo [25]
    Left = 116
    Top = 259
    Width = 145
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 12
    ListaFija = lfTipoOperacionCampo
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    DataField = 'AT_TOTAL'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object AT_VALORAC: TZetaDBKeyCombo [26]
    Left = 116
    Top = 358
    Width = 145
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 17
    ListaFija = lfNinguna
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    DataField = 'AT_VALORAC'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object AT_ENTIDAD: TZetaDBKeyLookup [27]
    Left = 116
    Top = 334
    Width = 300
    Height = 21
    TabOrder = 15
    TabStop = True
    WidthLlave = 60
    DataField = 'AT_ENTIDAD'
    DataSource = DataSource
  end
  object LV_CODIGO: TZetaDBKeyLookup [28]
    Left = 148
    Top = 334
    Width = 300
    Height = 21
    TabOrder = 16
    TabStop = True
    WidthLlave = 60
    DataField = 'LV_CODIGO'
    DataSource = DataSource
  end
  object AT_TRANGO: TZetaDBKeyCombo [29]
    Left = 116
    Top = 284
    Width = 145
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 13
    OnChange = AT_TRANGOChange
    ListaFija = lfTipoRango
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    DataField = 'AT_TRANGO'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object AT_DESCRIP: TDBMemo [30]
    Left = 116
    Top = 112
    Width = 300
    Height = 47
    DataField = 'AT_DESCRIP'
    DataSource = DataSource
    TabOrder = 7
  end
  object AT_CONFI: TDBCheckBox [31]
    Left = 290
    Top = 70
    Width = 126
    Height = 17
    Alignment = taLeftJustify
    Caption = #191'Campo Confidencial?:'
    DataField = 'AT_CONFI'
    DataSource = DataSource
    TabOrder = 5
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  inherited PanelIdentifica: TPanel
    Width = 465
    TabOrder = 18
    inherited ValorActivo2: TPanel
      Width = 139
    end
  end
  object AT_TITULO: TDBEdit [33]
    Left = 116
    Top = 87
    Width = 300
    Height = 21
    DataField = 'AT_TITULO'
    DataSource = DataSource
    TabOrder = 6
  end
  object AT_FILTRO: TZetaDBKeyCombo [34]
    Left = 116
    Top = 309
    Width = 145
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 14
    ListaFija = lfNinguna
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    DataField = 'AT_FILTRO'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object AT_ACTIVO: TDBCheckBox [35]
    Left = 366
    Top = 39
    Width = 50
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Activo:'
    DataField = 'AT_ACTIVO'
    DataSource = DataSource
    TabOrder = 2
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  object AT_VERSION: TDBEdit [36]
    Left = 116
    Top = 381
    Width = 123
    Height = 21
    DataField = 'AT_VERSION'
    DataSource = DataSource
    MaxLength = 30
    ReadOnly = True
    TabOrder = 20
  end
end
