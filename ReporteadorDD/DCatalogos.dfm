object dmCatalogos: TdmCatalogos
  OldCreateOrder = False
  Left = 479
  Top = 315
  Height = 268
  Width = 462
  object cdsCondiciones: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'QU_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsCondicionesAlAdquirirDatos
    LookupName = 'Cat'#225'logo de Condiciones'
    LookupDescriptionField = 'QU_DESCRIP'
    LookupKeyField = 'QU_CODIGO'
    Left = 300
    Top = 12
  end
  object cdsPuestos: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'PU_CODIGO'
    Params = <>
    ReadOnly = True
    AlAdquirirDatos = cdsPuestosAlAdquirirDatos
    UsaCache = True
    LookupName = 'Cat'#225'logo de Puestos'
    LookupDescriptionField = 'PU_DESCRIP'
    LookupKeyField = 'PU_CODIGO'
    Left = 16
    Top = 16
  end
  object cdsTurnos: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'TU_CODIGO'
    Params = <>
    ReadOnly = True
    AlAdquirirDatos = cdsTurnosAlAdquirirDatos
    UsaCache = True
    LookupName = 'Cat'#225'logo de Turnos'
    LookupDescriptionField = 'TU_DESCRIP'
    LookupKeyField = 'TU_CODIGO'
    Left = 88
    Top = 16
  end
  object cdsClasifi: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    ReadOnly = True
    AlAdquirirDatos = cdsClasifiAlAdquirirDatos
    LookupName = 'Cat'#225'logo de Clasificaciones'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    Left = 152
    Top = 16
  end
  object cdsHorarios: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'HO_CODIGO'
    Params = <>
    ReadOnly = True
    AlAdquirirDatos = cdsHorariosAlAdquirirDatos
    UsaCache = True
    LookupName = 'Cat'#225'logo de Horarios'
    LookupDescriptionField = 'HO_DESCRIP'
    LookupKeyField = 'HO_CODIGO'
    Left = 224
    Top = 16
  end
  object cdsConceptos: TZetaLookupDataSet
    Tag = 5
    Aggregates = <>
    IndexFieldNames = 'CO_NUMERO'
    Params = <>
    AlAdquirirDatos = cdsConceptosAlAdquirirDatos
    UsaCache = True
    LookupName = 'Cat'#225'logo de Conceptos'
    LookupDescriptionField = 'CO_DESCRIP'
    LookupKeyField = 'CO_NUMERO'
    Left = 16
    Top = 72
  end
  object cdsNomParamLookUp: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    Left = 20
    Top = 144
  end
  object cdsConceptosLookup: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    Left = 144
    Top = 144
  end
end
