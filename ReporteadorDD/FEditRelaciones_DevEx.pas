unit FEditRelaciones_DevEx;
{$INCLUDE DEFINES.INC}
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Buttons,
  DBCtrls, StdCtrls, ExtCtrls, Mask, ComCtrls,
  ZetaNumero, ZetaKeyLookup, ZetaEdit, ZetaSmartLists, ZetaDBTextBox,
  ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, cxControls, dxBarExtItems, dxBar, cxClasses, Vcl.ImgList,
  cxNavigator, cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx;


type
  TEditRelaciones_DevEx = class(TBaseEdicion_DevEx)
    Label3: TLabel;
    Label1: TLabel;
    RL_ENTIDAD: TZetaDBKeyLookup_DevEx;
    RL_CAMPOS: TDBEdit;
    RL_INNER: TDBCheckBox;
    RL_VERSION: TDBEdit;
    Label12: TLabel;
    RL_ACTIVO: TDBCheckBox;
    US_CODIGO: TZetaDBTextBox;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
  protected
    procedure Connect; override;
    function PuedeAgregar(var sMensaje: String): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    function PuedeModificar(var sMensaje: String): Boolean; override;
    function PuedeImprimir(var sMensaje: String): Boolean; override;

  public
  end;

var
  EditRelaciones_DevEx: TEditRelaciones_DevEx;

implementation

uses dDiccionario,
     ZetaCommonTools,
     ZetaCommonLists;

{$R *.DFM}

procedure TEditRelaciones_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     //IndexDerechos := D_CONS_CLASIFICACIONES;
     HelpContext:= 0; //Pendiente
     FirstControl := RL_ENTIDAD;
     RL_ENTIDAD.LookupDataset := dmDiccionario.cdsEntidadesLookup;
     {$ifdef  RDD_Desarrollo }
     RL_VERSION.ReadOnly := False;
     {$endif}
end;

procedure TEditRelaciones_DevEx.Connect;
begin
     with dmDiccionario do
     begin
          cdsEntidadesLookup.Conectar;
          cdsRelaciones.Conectar;
          Datasource.Dataset := cdsRelaciones;

     end;
end;


procedure TEditRelaciones_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     //Esta pantalla no tiene derecho de acceso por si sola.
     Datasource.AutoEdit := TRUE;
end;

function TEditRelaciones_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := TRUE;
end;

function TEditRelaciones_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := TRUE;
end;

function TEditRelaciones_DevEx.PuedeImprimir(var sMensaje: String): Boolean;
begin
     Result := TRUE;
end;

function TEditRelaciones_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := TRUE;
end;

end.
