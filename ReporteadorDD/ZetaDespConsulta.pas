unit ZetaDespConsulta;

interface

uses Forms, Controls, Sysutils, Dialogs,
     ZBaseConsulta;

type
  eFormaConsulta = ( efcNinguna,
                     efcReportes,
                     efcClasificaciones,
                     efcListas,
                     efcModulos,
                     efcDiccion,
                     efcCatCondiciones,
                     efcQueryGral,
                     efcSistBitacora,
                     efcSistProcesos,
                     efcSistGlobales,
                     efcSistGrupos,
                     efcSistUsuarios,
                     efcProceso  );

  TPropConsulta = record
    IndexDerechos: Integer;
    ClaseForma: TBaseConsultaClass;
  end;

function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;

implementation

uses ZAccesosMgr,
     ZAccesosTress,
     FReportes_DevEx,
     FCatCondiciones_DevEx,
     FQueryGral_DevEx,
     FSistBitacora_DevEx,
     FSistProcesos_DevEx,
     FClasifiReporte_DevEx,
     FListasFijas_DevEx,
     FModulos_DevEx,
     FEntidades_DevEx,
     FSistGrupos_DevEx
     //FProcesos //@gbeltran: Se quito del NavBar, el llamado se hace desde el Ribbon.
     ;


function Consulta( const iDerechos: Integer; const Forma: TBaseConsultaClass ): TPropConsulta;
begin
     with Result do
     begin
          IndexDerechos := iDerechos;
          ClaseForma := Forma;
     end;
end;

function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;
begin
     case Forma of
          efcReportes        : Result := Consulta( D_CRR_REPORTES, TReportes_DevEx );
          efcClasificaciones : Result := Consulta( D_CRC_CATALOGOS_CLASIFICACIONES, TClasifiReporte_DevEx );
          efcListas          : Result := Consulta( D_CRC_CATALOGO_VALORES, TListasFijas_DevEx );
          efcModulos         : Result := Consulta( D_CRC_CATALOGOS_MODULOS, TModulos_DevEx );
          //efcCatCondiciones  : Result := Consulta( D_CRC_CATALOGOS_CONDICIONES, TCatCondiciones_DevEx ); //@gbeltran: Se quito de la Solucion.
          efcQueryGral       : Result := Consulta( D_CRR_SQL, TQueryGral_DevEx );
          efcSistProcesos    : Result := Consulta( D_CRR_BITACORA_PROCESOS, TSistProcesos_DevEx );
          efcSistGrupos      : Result := Consulta( D_CRS_GRUPO_USUARIOS, TSistGrupos_DevEx );
          efcDiccion         : Result := Consulta( D_CRC_DICCIONARIO_DATOS, TEntidades_DevEx );
          efcSistBitacora    : Result := Consulta( D_CRR_BITACORA, TSistBitacora_DevEx );
          //efcProceso         : Result := Consulta( D_CRC_PROCESO, TConfProcesos ); //@gbeltran: Se quito del NavBar, el llamado se hace desde el ribbon.
     else
         Result := Consulta( 0, nil );
     end;
end;

end.
