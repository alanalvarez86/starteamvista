unit FModulos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, DB, ExtCtrls, Grids, DBGrids, ZetaDBGrid, Buttons,
  Vcl.StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList,
  Vcl.ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid,
  dxSkinsCore, dxSkinsDefaultPainters, cxButtons;

type
  TModulos_DevEx = class(TBaseGridLectura_DevEx)
    ZetaDBGrid1: TZetaDBGrid;
    MO_ORDEN: TcxGridDBColumn;
    MO_NOMBRE: TcxGridDBColumn;
    MO_CODIGO: TcxGridDBColumn;
    PanelMover: TPanel;
    Subir: TcxButton;
    Bajar: TcxButton;
    procedure BajarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
           procedure Connect; override;
           procedure Refresh; override;
           procedure Agregar;override;
           procedure Borrar; override;
           procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  Modulos_DevEx: TModulos_DevEx;

implementation

uses
    ZetaCommonClasses,
    DCliente,
    dDiccionario;
{$R *.dfm}

{ TClasifiReporte }

procedure TModulos_DevEx.Agregar;
begin
     inherited;
     dmDiccionario.cdsModulos.Agregar;
end;

procedure TModulos_DevEx.Borrar;
begin
     inherited;
     dmDiccionario.cdsModulos.Borrar;
end;

procedure TModulos_DevEx.Connect;
begin
     inherited;
     with dmDiccionario do
     begin
          cdsModulos.Conectar;
          DataSource.DataSet:= cdsModulos;
     end;
end;

procedure TModulos_DevEx.Modificar;
begin
     inherited;
     dmDiccionario.cdsModulos.Modificar;

end;

procedure TModulos_DevEx.Refresh;
begin
     inherited;
     dmDiccionario.cdsModulos.Refrescar;
end;

procedure TModulos_DevEx.BajarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        dmDiccionario.ModuloMover(TControl(Sender).Tag);
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TModulos_DevEx.FormCreate(Sender: TObject);
begin
  inherited;
  HelpContext:= H_Catalogo_Modulos;
  ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= TRUE;
end;

procedure TModulos_DevEx.FormShow(Sender: TObject);
begin
ApplyMinWidth;
  CreaColumaSumatoria(ZetaDbGridDBTableView.Columns[2], 0 ,'' , SkCount );
  ZetaDBGridDBTableView.ApplyBestFit();
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
  inherited;

end;

end.
