inherited EditCampos: TEditCampos
  Left = 380
  Top = 231
  Anchors = [akTop, akRight]
  Caption = 'Editar Campos'
  ClientHeight = 368
  ClientWidth = 364
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lbCampo: TLabel [0]
    Left = 62
    Top = 89
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Campo:'
  end
  object Label1: TLabel [1]
    Left = 39
    Top = 112
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descripci'#243'n:'
  end
  object Label3: TLabel [2]
    Left = 24
    Top = 158
    Width = 74
    Height = 13
    Alignment = taRightJustify
    Caption = 'Palabras Clave:'
  end
  object Label2: TLabel [3]
    Left = 30
    Top = 135
    Width = 68
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nombre Corto:'
  end
  object Label4: TLabel [4]
    Left = 23
    Top = 66
    Width = 75
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo de Campo:'
  end
  object Label6: TLabel [5]
    Left = 54
    Top = 180
    Width = 44
    Height = 13
    Alignment = taRightJustify
    Caption = 'Mascara:'
  end
  object lbTexto: TLabel [6]
    Left = 63
    Top = 249
    Width = 35
    Height = 13
    Alignment = taRightJustify
    Caption = 'lbTexto'
  end
  object Label5: TLabel [7]
    Left = 64
    Top = 203
    Width = 34
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ancho:'
  end
  object Label10: TLabel [8]
    Left = 34
    Top = 226
    Width = 64
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo de Filtro:'
  end
  inherited PanelBotones: TPanel
    Top = 332
    Width = 364
    TabOrder = 13
    inherited OK: TBitBtn
      Left = 190
    end
    inherited Cancelar: TBitBtn
      Left = 275
    end
  end
  inherited PanelSuperior: TPanel
    Width = 364
    TabOrder = 14
  end
  inherited PanelIdentifica: TPanel
    Width = 364
    TabOrder = 15
    inherited ValorActivo2: TPanel
      Width = 38
    end
  end
  object cbConfidencial: TCheckBox [12]
    Left = 34
    Top = 272
    Width = 79
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Confidencial:'
    TabOrder = 10
  end
  object eAncho: TZetaNumero [13]
    Left = 100
    Top = 199
    Width = 36
    Height = 21
    Mascara = mnDias
    TabOrder = 6
    Text = '0'
  end
  object cbEntidades: TComboBox [14]
    Left = 100
    Top = 244
    Width = 245
    Height = 21
    ItemHeight = 13
    TabOrder = 9
  end
  object cbListaFija: TZetaKeyCombo [15]
    Left = 100
    Top = 245
    Width = 145
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 8
    ListaFija = lfTipoRango
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
  end
  object eMascara: TEdit [16]
    Left = 100
    Top = 176
    Width = 145
    Height = 21
    TabOrder = 5
  end
  object cbTipoCampo: TZetaKeyCombo [17]
    Left = 100
    Top = 62
    Width = 145
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
    OnChange = cbTipoCampoChange
    OnExit = cbTipoCampoChange
    ListaFija = lfTipoGlobal
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
  end
  object eTCorto: TEdit [18]
    Left = 100
    Top = 131
    Width = 225
    Height = 21
    MaxLength = 30
    TabOrder = 3
  end
  object eClaves: TEdit [19]
    Left = 100
    Top = 154
    Width = 225
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 30
    TabOrder = 4
  end
  object eTitulo: TComboBox [20]
    Left = 100
    Top = 109
    Width = 225
    Height = 21
    DropDownCount = 50
    ItemHeight = 13
    MaxLength = 30
    Sorted = True
    TabOrder = 2
    OnExit = eTituloChange
    Items.Strings = (
      'C'#243'digo de Tabla'
      'Descripci'#243'n'
      'Descripcion #'
      'Descripci'#243'n en Ingl'#233's'
      'N'#250'mero #'
      'N'#250'mero de Empleado'
      'N'#250'mero de Expediente'
      'N'#250'mero de Uso General'
      'Observaciones'
      'Si/No #'
      'Si/No/Texto #'
      'Texto #'
      'Texto de Uso General'
      'Usuario que Modific'#243)
  end
  object eNombre: TEdit [21]
    Left = 100
    Top = 85
    Width = 145
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 10
    TabOrder = 1
    OnChange = eNombreChange
  end
  object cbTipoFiltro: TComboBox [22]
    Left = 100
    Top = 222
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 7
    OnChange = cbTipoFiltroChange
    Items.Strings = (
      'Lookup a una Tabla'
      'Lista Fija'
      'Ninguna de las dos')
  end
  object Button1: TButton [23]
    Left = 256
    Top = 296
    Width = 75
    Height = 25
    Caption = 'Nombre Corto'
    TabOrder = 11
    OnClick = Button1Click
  end
  object Button2: TButton [24]
    Left = 328
    Top = 135
    Width = 19
    Height = 17
    TabOrder = 12
    OnClick = Button2Click
  end
  inherited DataSource: TDataSource
    Left = 276
    Top = 1
  end
end
