unit FEditListasFijas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, Db, Buttons,
  DBCtrls, StdCtrls, ExtCtrls, Mask, ComCtrls,
  ZetaNumero, ZetaKeyLookup, ZetaEdit, ZetaSmartLists, ZetaDBTextBox;


type
  TEditListasFijas = class(TBaseEdicion)
    Label2: TLabel;
    LV_NOMBRE: TDBEdit;
    Label1: TLabel;
    LV_CODIGO: TZetaDBNumero;
    LV_VALORES: TMemo;
    Label3: TLabel;
    cbSistema: TCheckBox;
    Label4: TLabel;
    LV_VERSION: TDBEdit;
    US_CODIGO: TZetaDBTextBox;
    Label6: TLabel;
    LV_ACTIVO: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure LV_VALORESChange(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
    FMemoCambio: Boolean;
    procedure EnabledControls(const lEnabled: Boolean);

  protected
    procedure Connect; override;
    procedure EscribirCambios; override;
  public
  end;

var
  EditListasFijas: TEditListasFijas;

implementation

uses dDiccionario,
     ZetaCommonTools,
     ZAccesosTress,
     ZetaCommonLists;

{$R *.DFM}

procedure TEditListasFijas.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CRC_CATALOGO_VALORES;
     HelpContext:= 0; //Pendiente
     FirstControl := LV_CODIGO;
end;

procedure TEditListasFijas.Connect;
begin
     with dmDiccionario do
     begin
          cdsListasFijas.Conectar;
          Datasource.Dataset := cdsListasFijas;
          LV_VALORES.Lines.Clear;
          LlenaLista( cdsListasFijas.FieldByName('LV_CODIGO').AsInteger, LV_VALORES.Lines, FALSE );
     end;
     FMemoCambio := FALSE;
     Modo := dsBrowse;
end;

procedure TEditListasFijas.EscribirCambios;
 var sValores: string;
begin
     with dmDiccionario do
     begin
          with cdsListasFijas do
          begin
               if ( State = dsBrowse ) then
               begin
                    Edit;
                    FieldByName('LV_DUMMY').AsInteger := FieldByName('LV_DUMMY').AsInteger + 1;
               end;
          end;
          sValores := '';

          if FMemoCambio then
             sValores := LV_VALORES.Lines.CommaText;

          GrabaListasFijas( sValores );
     end;
     FMemoCambio := FALSE;
end;

procedure TEditListasFijas.LV_VALORESChange(Sender: TObject);
begin
     inherited;
     Modo := dsEdit;
     FMemoCambio:= TRUE;
end;

procedure TEditListasFijas.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
     inherited;
     with dmDiccionario do
     begin
          EnabledControls( not RecordSistema( cdsListasFijas ) );
     end;

end;

procedure TEditListasFijas.EnabledControls(const lEnabled: Boolean);
begin
     cbSistema.Checked := not lEnabled;
end;

end.
