inherited EditModulos: TEditModulos
  Left = 352
  Top = 155
  Width = 391
  Height = 476
  BorderStyle = bsSizeable
  Caption = 'M'#243'dulo'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 406
    Width = 383
    TabOrder = 1
    inherited OK: TBitBtn
      Left = 208
    end
    inherited Cancelar: TBitBtn
      Left = 293
    end
  end
  inherited PanelSuperior: TPanel
    Width = 383
    TabOrder = 2
    inherited BuscarBtn: TSpeedButton
      Visible = True
    end
    inherited DBNavigator: TDBNavigator
      Visible = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 383
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 57
    end
  end
  object Panel2: TPanel [3]
    Left = 0
    Top = 177
    Width = 383
    Height = 229
    Align = alClient
    TabOrder = 3
    object PanelCamposDefault: TPanel
      Left = 1
      Top = 1
      Width = 381
      Height = 28
      Align = alTop
      Alignment = taLeftJustify
      Caption = '                           Tablas que contiene el m'#243'dulo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      object BitBtn12: TBitBtn
        Left = 8
        Top = 1
        Width = 25
        Height = 25
        TabOrder = 0
        Visible = False
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
          333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
          0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
          07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
          07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
          0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
          33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
          B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
          3BB33773333773333773B333333B3333333B7333333733333337}
        NumGlyphs = 2
      end
      object BitBtn13: TBitBtn
        Left = 36
        Top = 1
        Width = 25
        Height = 25
        TabOrder = 1
        Visible = False
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
          55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
          305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
          005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
          B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
          B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
          B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
          B0557777FF577777F7F500000E055550805577777F7555575755500000555555
          05555777775555557F5555000555555505555577755555557555}
        NumGlyphs = 2
      end
    end
    object Panel8: TPanel
      Left = 347
      Top = 29
      Width = 35
      Height = 199
      Align = alRight
      Alignment = taLeftJustify
      BevelOuter = bvNone
      TabOrder = 1
      object TablaSubir: TSpeedButton
        Tag = -1
        Left = 5
        Top = 52
        Width = 28
        Height = 28
        Hint = 'Mover Tabla Hacia Arriba'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000333
          3333333333777F33333333333309033333333333337F7F333333333333090333
          33333333337F7F33333333333309033333333333337F7F333333333333090333
          33333333337F7F33333333333309033333333333FF7F7FFFF333333000090000
          3333333777737777F333333099999990333333373F3333373333333309999903
          333333337F33337F33333333099999033333333373F333733333333330999033
          3333333337F337F3333333333099903333333333373F37333333333333090333
          33333333337F7F33333333333309033333333333337373333333333333303333
          333333333337F333333333333330333333333333333733333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = TablaSubirClick
      end
      object TablaBajar: TSpeedButton
        Tag = 1
        Left = 5
        Top = 84
        Width = 28
        Height = 28
        Hint = 'Mover Tabla Hacia Abajo'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333303333
          333333333337F33333333333333033333333333333373F333333333333090333
          33333333337F7F33333333333309033333333333337373F33333333330999033
          3333333337F337F33333333330999033333333333733373F3333333309999903
          333333337F33337F33333333099999033333333373333373F333333099999990
          33333337FFFF3FF7F33333300009000033333337777F77773333333333090333
          33333333337F7F33333333333309033333333333337F7F333333333333090333
          33333333337F7F33333333333309033333333333337F7F333333333333090333
          33333333337F7F33333333333300033333333333337773333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = TablaSubirClick
      end
    end
    object DBGrid: TDBGrid
      Left = 1
      Top = 29
      Width = 346
      Height = 199
      Align = alClient
      DataSource = dsEntidadesPorModulo
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'EN_TITULO'
          Title.Caption = 'Descripci'#243'n de la Tabla'
          Width = 193
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EN_TABLA'
          Title.Caption = 'Nombre'
          Width = 93
          Visible = True
        end>
    end
  end
  object Panel1: TPanel [4]
    Left = 0
    Top = 51
    Width = 383
    Height = 126
    Align = alTop
    TabOrder = 4
    object lblRevisio: TLabel
      Left = 37
      Top = 58
      Width = 32
      Height = 13
      Alignment = taRightJustify
      Caption = 'Orden:'
      Enabled = False
    end
    object Label2: TLabel
      Left = 10
      Top = 35
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descripci'#243'n:'
    end
    object lbCodigo: TLabel
      Left = 33
      Top = 13
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'digo:'
      Enabled = False
    end
    object Label3: TLabel
      Left = 31
      Top = 81
      Width = 38
      Height = 13
      Alignment = taRightJustify
      Caption = 'Versi'#243'n:'
      Enabled = False
    end
    object Label1: TLabel
      Left = 26
      Top = 100
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'Modific'#243':'
      Enabled = False
    end
    object US_CODIGO: TZetaDBTextBox
      Left = 73
      Top = 99
      Width = 200
      Height = 17
      AutoSize = False
      Caption = 'US_CODIGO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'US_CODIGO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object MO_NOMBRE: TDBEdit
      Left = 73
      Top = 32
      Width = 300
      Height = 21
      DataField = 'MO_NOMBRE'
      DataSource = DataSource
      MaxLength = 30
      TabOrder = 0
    end
    object MO_CODIGO: TZetaDBNumero
      Left = 73
      Top = 10
      Width = 121
      Height = 21
      Enabled = False
      Mascara = mnDias
      TabOrder = 3
      Text = '0'
      DataField = 'MO_CODIGO'
      DataSource = DataSource
    end
    object MO_ORDEN: TZetaDBNumero
      Left = 73
      Top = 54
      Width = 123
      Height = 21
      Enabled = False
      Mascara = mnEmpleado
      TabOrder = 4
      DataField = 'MO_ORDEN'
      DataSource = DataSource
    end
    object cbSistema: TCheckBox
      Left = 263
      Top = 15
      Width = 110
      Height = 17
      Alignment = taLeftJustify
      Caption = 'M'#243'dulo de sistema'
      Enabled = False
      TabOrder = 2
    end
    object RC_ACTIVO: TDBCheckBox
      Left = 323
      Top = 0
      Width = 50
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Activo'
      DataField = 'MO_ACTIVO'
      DataSource = DataSource
      TabOrder = 1
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
    object MO_VERSION: TDBEdit
      Left = 73
      Top = 77
      Width = 123
      Height = 21
      DataField = 'MO_VERSION'
      DataSource = DataSource
      MaxLength = 30
      ReadOnly = True
      TabOrder = 5
    end
  end
  inherited DataSource: TDataSource
    Left = 292
    Top = 1
  end
  object dsEntidadesPorModulo: TDataSource
    OnStateChange = DataSourceStateChange
    OnDataChange = DataSourceDataChange
    Left = 404
    Top = 1
  end
end
