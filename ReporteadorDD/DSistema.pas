unit DSistema;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,ComCtrls,
  {$ifndef VER130}
  Variants,
  {$endif}
  {$ifdef DOS_CAPAS}
  DServerReporteadorDD,
  {$else}
         {$ifdef RDD}
         ReporteadorDD_TLB,
         {$else}
         DiccionarioGuardias_TLB,
         {$endif}

  {$endif}
  DBaseSistema, Db, DBClient, ZetaClientDataSet, ZetaServerDataSet, ZAccesosMgr;

type

 {$ifdef RDD}
    {$ifdef DOS_CAPAS}
       TdmServerDiccionario = TdmServerReporteadorDD;
    {$else}
       IdmServerDiccionarioDisp = IdmServerReporteadorDDDisp;
    {$endif}
  {$endif}

  TdmSistema = class(TdmBaseSistema)
    cdsClasificacionesAccesos: TZetaClientDataSet;
    cdsEntidadesAccesos: TZetaClientDataSet;
    procedure cdsEntidadesAccesosAlEnviarDatos(Sender: TObject);
    procedure cdsClasificacionesAccesosAlEnviarDatos(Sender: TObject);
  private
    { Private declarations }
  protected
{$ifdef DOS_CAPAS}
    function GetServerDiccionario: TdmServerDiccionario;
    property ServerDiccionario: TdmServerDiccionario read GetServerDiccionario;
{$else}
    FServidor: IdmServerDiccionarioDisp;
    function GetServerDiccionario: IdmServerDIccionarioDisp;
    property ServerDiccionario: IdmServerDiccionarioDisp read GetServerDiccionario;
{$endif}
    function BuscaAccesosClasificaciones(const iPosition: Integer): Boolean;
    function BuscaAccesosEntidades(const iPosition: Integer): Boolean;
    function GetcdsAccesos : TZetaClientDataSet; override;
  public
    { Public declarations }
     function BuscaDerechoEntidades(const iPosition: Integer): TDerechosAcceso;override;
     function BuscaDerechoClasificaciones(const iPosition: Integer): TDerechosAcceso;override;
     procedure GrabaDerecho( const iPosition: Integer; const iDerecho: TDerechosAcceso; oNodo: TTreeNode; oTextoBitacora: TGetTextoBitacora);override;
     procedure ConectaAccesos( var sGroupName, sCompanyName: String ); override;
{$ifdef RDD}
     function BuscaDerechoCopiadoEntidades(const iPosition: Integer): TDerechosAcceso;override;
     function BuscaDerechoCopiadoClasificaciones(const iPosition: Integer): TDerechosAcceso;override;
     procedure CopyAccesos(iGrupo: Integer; sCompany: String);override;
{$endif}
  end;

var
  dmSistema: TdmSistema;

implementation

uses
    ZetaCommonClasses,
    dCliente;

{$R *.DFM}

{ TdmSistema }

{$ifndef DOS_CAPAS}
function TdmSistema.GetServerDiccionario: IdmServerDiccionarioDisp;
begin
     Result := IdmServerDiccionarioDisp(dmCliente.CreaServidor( {$ifdef RDD}CLASS_dmServerReporteadorDD{$else}CLASS_dmServerDiccionario{$endif}, FServidor ));
end;
{$endif}

procedure TdmSistema.ConectaAccesos( var sGroupName, sCompanyName: String );
var
   iGrupo: Integer;
   sCompany: String;
begin
     with cdsGrupos do
     begin
          iGrupo := FieldByName( 'GR_CODIGO' ).AsInteger;
          sGroupName := FieldByName( 'GR_DESCRIP' ).AsString;
     end;
     with cdsEmpresasAccesos do
     begin
          sCompany := FieldByName( 'CM_CODIGO' ).AsString;
          sCompanyName := FieldByName( 'CM_NOMBRE' ).AsString;
     end;
     cdsClasificacionesAccesos.Data := ServerDiccionario.GetDerechosClasifi( dmCliente.Empresa, iGrupo );
     cdsEntidadesAccesos.Data := ServerDiccionario.GetDerechosEntidades( dmCliente.Empresa, iGrupo );
end;

function TdmSistema.BuscaAccesosEntidades(const iPosition: Integer): Boolean;
begin
     Result := cdsEntidadesAccesos.Locate( 'CM_CODIGO;GR_CODIGO;EN_CODIGO',
                                           VarArrayOf( [ cdsEmpresasAccesos.FieldByName( 'CM_CODIGO' ).AsString,
                                                         cdsGrupos.FieldByName( 'GR_CODIGO' ).AsInteger,
                                                         iPosition ] ), [] );

end;

function TdmSistema.BuscaAccesosClasificaciones(const iPosition: Integer): Boolean;
begin
     Result := cdsClasificacionesAccesos.Locate( 'CM_CODIGO;GR_CODIGO;RC_CODIGO',
                                                 VarArrayOf( [ cdsEmpresasAccesos.FieldByName( 'CM_CODIGO' ).AsString,
                                                               cdsGrupos.FieldByName( 'GR_CODIGO' ).AsInteger,
                                                               iPosition ] ), [] )
end;


function TdmSistema.BuscaDerechoEntidades(const iPosition: Integer): TDerechosAcceso;
begin
      if BuscaAccesosEntidades( iPosition ) then
      begin
           Result := cdsEntidadesAccesos.FieldByName( 'RE_DERECHO' ).AsInteger
      end
      else
         Result := K_SIN_DERECHOS;
end;

function TdmSistema.BuscaDerechoClasificaciones(const iPosition: Integer): TDerechosAcceso;
begin
      if BuscaAccesosClasificaciones( iPosition ) then
      begin
           Result := cdsClasificacionesAccesos.FieldByName( 'RA_DERECHO' ).AsInteger
      end
      else
         Result := K_SIN_DERECHOS;
end;


procedure TdmSistema.cdsEntidadesAccesosAlEnviarDatos( Sender: TObject);
var
   ErrorCount: Integer;
   sCodigo: string;
begin
     ErrorCount := 0;
     with cdsEntidadesAccesos do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
             if Reconcile( ServerDiccionario.GrabaAccesosEntidad( dmCliente.Empresa, cdsGrupos.FieldByName('GR_CODIGO').AsInteger, Delta, ErrorCount ) ) then
             begin
                  sCodigo := cdsEmpresasAccesos.FieldByName( 'CM_CODIGO' ).AsString;
                  cdsEmpresasAccesos.Refrescar;
                  cdsEmpresasAccesos.Locate( 'CM_CODIGO', sCodigo, [ loCaseInsensitive ] );
             end;
    end;
end;

procedure TdmSistema.cdsClasificacionesAccesosAlEnviarDatos( Sender: TObject);
var
   ErrorCount: Integer;
   sCodigo: string;
begin
     ErrorCount := 0;
     with cdsClasificacionesAccesos do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
             if Reconcile( ServerDiccionario.GrabaAccesosClasifi( dmCliente.Empresa, cdsGrupos.FieldByName('GR_CODIGO').AsInteger, Delta, ErrorCount ) ) then
             begin
                  sCodigo := cdsEmpresasAccesos.FieldByName( 'CM_CODIGO' ).AsString;
                  cdsEmpresasAccesos.Refrescar;
                  cdsEmpresasAccesos.Locate( 'CM_CODIGO', sCodigo, [ loCaseInsensitive ] );
             end;
    end;
end;

procedure TdmSistema.GrabaDerecho( const iPosition: Integer; const iDerecho: TDerechosAcceso; oNodo: TTreeNode; oTextoBitacora: TGetTextoBitacora);
begin
     with cdsAccesos do
     begin
          if ArbolSeleccionado = adClasifi then
          begin
               if BuscaAccesosClasificaciones( iPosition ) then
               begin
                    if ( FieldByName( 'RA_DERECHO' ).AsInteger <> iDerecho ) then
                    begin
                         Edit;
                         FieldByName( 'RA_DERECHO' ).AsInteger := iDerecho;
                         Post;
                    end;
               end
               else
               begin
                   Append;
                   FieldByName( 'GR_CODIGO' ).AsInteger := cdsGrupos.FieldByName( 'GR_CODIGO' ).AsInteger;
                   FieldByName( 'CM_CODIGO' ).AsString := cdsEmpresasAccesos.FieldByName( 'CM_CODIGO' ).AsString;
                   FieldByName( 'RC_CODIGO' ).AsInteger := iPosition;
                   FieldByName( 'RA_DERECHO' ).AsInteger := iDerecho;
                   Post;
               end;
          end
          else
          begin
               if BuscaAccesosEntidades( iPosition ) then
               begin
                    if ( FieldByName( 'RE_DERECHO' ).AsInteger <> iDerecho ) then
                    begin
                         Edit;
                         FieldByName( 'RE_DERECHO' ).AsInteger := iDerecho;
                         Post;
                    end;
               end
               else
               begin
                    Append;
                    FieldByName( 'GR_CODIGO' ).AsInteger := cdsGrupos.FieldByName( 'GR_CODIGO' ).AsInteger;
                    FieldByName( 'CM_CODIGO' ).AsString := cdsEmpresasAccesos.FieldByName( 'CM_CODIGO' ).AsString;
                    FieldByName( 'EN_CODIGO' ).AsInteger := iPosition;
                    FieldByName( 'RE_DERECHO' ).AsInteger := iDerecho;
                    Post;
               end;
          end;
     end;
end;

function TdmSistema.GetcdsAccesos : TZetaClientDataSet;
begin
      if ArbolSeleccionado = adClasifi then
         Result := cdsClasificacionesAccesos
      else
          Result:= cdsEntidadesAccesos;
end;

{$ifdef RDD}
{AV: Inicio de integración para solucionar el defecto 1896}
function TdmSistema.BuscaDerechoCopiadoEntidades(const iPosition: Integer): TDerechosAcceso;
begin
     {CV: Correcion defecto 1731}
     if  cdsCopiarAccesos.Locate( 'EN_CODIGO',
                                   VarArrayOf( [ iPosition ] ), [] ) then
     begin
          Result := cdsCopiarAccesos.FieldByName( 'RE_DERECHO' ).AsInteger
     end
     else
        Result := K_SIN_DERECHOS;
end;

function TdmSistema.BuscaDerechoCopiadoClasificaciones(const iPosition: Integer): TDerechosAcceso;
begin
      {CV: Correcion defecto 1731}
      if  cdsCopiarAccesos.Locate( 'RC_CODIGO',
                                   VarArrayOf( [ iPosition ] ), [] ) then
      begin
           Result := cdsCopiarAccesos.FieldByName( 'RA_DERECHO' ).AsInteger
      end
      else
         Result := K_SIN_DERECHOS;
end;


procedure TdmSistema.CopyAccesos( iGrupo: Integer; sCompany: String );
 var
    oEmpresa: Olevariant;
begin
     case ArbolSeleccionado of
          adClasifi:
          begin
               {CV: Correcion defecto 1731}
               oEmpresa := GetEmpresaAccesos( sCompany );
               cdsCopiarAccesos.Data := ServerDiccionario.GetDerechosClasifi( oEmpresa, iGrupo );
               cdsCopiarAccesos.IndexFieldNames := 'RC_CODIGO';
          end;
          adEntidades:
          begin
               {CV: Correcion defecto 1731}
               oEmpresa := GetEmpresaAccesos(sCompany);
               cdsCopiarAccesos.Data := ServerDiccionario.GetDerechosEntidades( oEmpresa, iGrupo );
               cdsCopiarAccesos.IndexFieldNames := 'EN_CODIGO';
          end;
      end;
end;
{AV: Fin de integración para solucionar el defecto 1896}
{$endif}
end.
