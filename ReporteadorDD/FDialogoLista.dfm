inherited DialogoLista: TDialogoLista
  Left = 580
  Top = 291
  Caption = 'DialogoLista'
  ClientHeight = 291
  ClientWidth = 263
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 255
    Width = 263
    DesignSize = (
      263
      36)
    inherited OK: TBitBtn
      Left = 95
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 180
    end
  end
  object cbLista: TCheckListBox
    Left = 0
    Top = 0
    Width = 263
    Height = 255
    Align = alClient
    ItemHeight = 13
    TabOrder = 1
  end
end
