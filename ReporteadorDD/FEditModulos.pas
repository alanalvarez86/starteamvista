unit FEditModulos;
{$INCLUDE DEFINES.INC}
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, Db, Buttons,
  DBCtrls, StdCtrls, ExtCtrls, Mask, ComCtrls,
  ZetaNumero, ZetaKeyLookup, ZetaEdit, ZetaSmartLists, Grids, DBGrids,
  ZetaDBTextBox;


type
  TEditModulos = class(TBaseEdicion)
    dsEntidadesPorModulo: TDataSource;
    Panel2: TPanel;
    PanelCamposDefault: TPanel;
    BitBtn12: TBitBtn;
    BitBtn13: TBitBtn;
    Panel8: TPanel;
    TablaSubir: TSpeedButton;
    TablaBajar: TSpeedButton;
    DBGrid: TDBGrid;
    Panel1: TPanel;
    lblRevisio: TLabel;
    Label2: TLabel;
    lbCodigo: TLabel;
    Label3: TLabel;
    Label1: TLabel;
    US_CODIGO: TZetaDBTextBox;
    MO_NOMBRE: TDBEdit;
    MO_CODIGO: TZetaDBNumero;
    MO_ORDEN: TZetaDBNumero;
    cbSistema: TCheckBox;
    RC_ACTIVO: TDBCheckBox;
    MO_VERSION: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
    procedure TablaSubirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure EnabledControls(const lEnabled: Boolean);
    procedure ConectaEntidadesPorModulo;
  protected
    procedure Connect; override;
    procedure DisConnect; override;
  public
  end;

var
  EditModulos: TEditModulos;

implementation

uses DCliente, dDiccionario, ZetaCommonClasses, ZAccesosTress, Math, DBClient;

{$R *.DFM}

procedure TEditModulos.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CRC_CATALOGOS_MODULOS;
     HelpContext:= 0; //Pendiente
     TablaSubir.Visible := dmCliente.GetGrupoActivo = D_GRUPO_SIN_RESTRICCION;
     TablaBajar.Visible := dmCliente.GetGrupoActivo = D_GRUPO_SIN_RESTRICCION;
      {$ifdef  RDD_Desarrollo }
     MO_VERSION.ReadOnly := False;
     {$endif}
end;

procedure TEditModulos.Connect;
begin
     with dmDiccionario do
     begin
          //cdsModulos.Conectar;
          Datasource.Dataset := cdsModulos;
          dsEntidadesPorModulo.DataSet := cdsEntidadesPorModulo;
          Self.ConectaEntidadesPorModulo;
     end;
end;

procedure TEditModulos.ConectaEntidadesPorModulo;
begin
     with dmDiccionario do
     begin
          if ( cdsModulos.State <> dsInsert )then
          begin
               cdsEntidadesPorModulo.Refrescar;
               cdsEntidadesPorModulo.Filter := Format( 'MO_CODIGO = %d', [cdsModulos.FieldByName('MO_CODIGO').AsInteger] );
               cdsEntidadesPorModulo.Filtered := TRUE;
          end
          else
               cdsEntidadesPorModulo.Active  := False;
     end;
end;

procedure TEditModulos.DisConnect;
begin
     with dmDiccionario.cdsEntidadesPorModulo do
     begin
          Filter := VACIO;
          Filtered := False;
     end;
     
     inherited;

end;

procedure TEditModulos.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
     inherited;
     with dmDiccionario do
          EnabledControls( not RecordSistema( cdsModulos ) );
end;

procedure TEditModulos.EnabledControls( const lEnabled: Boolean );
begin
     cbSistema.Checked := NOT lEnabled;
     if lEnabled then
     begin
          lbCodigo.Enabled := lEnabled;
          MO_CODIGO.Enabled := lEnabled;
     end;
end;

procedure TEditModulos.FormShow(Sender: TObject);
begin
     inherited;
     if MO_CODIGO.Enabled then
        FirstControl := MO_CODIGO
     else
         FirstControl := MO_NOMBRE;

end;


procedure TEditModulos.TablaSubirClick(Sender: TObject);
var
   oCursor: TCursor;
   iPosicion: integer;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;

     try
        with dmDiccionario do
        begin
             cdsEntidadesPorModulo.DisableControls;
             try
                iPosicion := cdsEntidadesPorModulo.FieldByName('EN_CODIGO').AsInteger;
                TablasPorModuloMover(TControl(Sender).Tag);
                Self.ConectaEntidadesPorModulo;
                cdsEntidadesPorModulo.Locate('EN_CODIGO',iPosicion,[]);
             finally
                    dmDiccionario.cdsEntidadesPorModulo.EnableControls;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TEditModulos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     inherited;
     dmDiccionario.cdsEntidadesPorModulo.Filtered := FALSE;
end;

end.
