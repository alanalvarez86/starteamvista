unit FSistEditUsuarios;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FSistBaseEditUsuarios, Db, ZetaDBTextBox, StdCtrls, ZetaKeyCombo,
  ZetaKeyLookup, DBCtrls, Mask, ZetaNumero, ComCtrls, ExtCtrls, Buttons;

type
  TSistEditUsuarios = class(TSistBaseEditUsuarios)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SistEditUsuarios: TSistEditUsuarios;

implementation
{$R *.DFM}

end.
