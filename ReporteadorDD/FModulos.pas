unit FModulos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, DB, ExtCtrls, Grids, DBGrids, ZetaDBGrid, Buttons;

type
  TModulos = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    PanelMover: TPanel;
    Subir: TSpeedButton;
    Bajar: TSpeedButton;
    procedure BajarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
           procedure Connect; override;
           procedure Refresh; override;
           procedure Agregar;override;
           procedure Borrar; override;
           procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  Modulos: TModulos;

implementation

uses
    ZetaCommonClasses,
    DCliente,
    dDiccionario;
{$R *.dfm}

{ TClasifiReporte }

procedure TModulos.Agregar;
begin
     inherited;
     dmDiccionario.cdsModulos.Agregar;
end;

procedure TModulos.Borrar;
begin
     inherited;
     dmDiccionario.cdsModulos.Borrar;
end;

procedure TModulos.Connect;
begin
     inherited;
     with dmDiccionario do
     begin
          cdsModulos.Conectar;
          DataSource.DataSet:= cdsModulos;
     end;
end;

procedure TModulos.Modificar;
begin
     inherited;
     dmDiccionario.cdsModulos.Modificar;

end;

procedure TModulos.Refresh;
begin
     inherited;
     dmDiccionario.cdsModulos.Refrescar;
end;

procedure TModulos.BajarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        dmDiccionario.ModuloMover(TControl(Sender).Tag);
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TModulos.FormCreate(Sender: TObject);
begin
     inherited;
     Subir.Visible := dmCliente.GetGrupoActivo = D_GRUPO_SIN_RESTRICCION;
     Bajar.Visible := dmCliente.GetGrupoActivo = D_GRUPO_SIN_RESTRICCION;
end;

end.
