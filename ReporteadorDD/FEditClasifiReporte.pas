unit FEditClasifiReporte;

interface

{$INCLUDE DEFINES.INC}


uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, Db, Buttons,
  DBCtrls, StdCtrls, ExtCtrls, Mask, ComCtrls,
  ZetaNumero, ZetaKeyLookup, ZetaEdit, ZetaSmartLists, Grids, DBGrids,
  ZetaDBTextBox;


type
  TEditClasifiReporte = class(TBaseEdicion)
    Splitter1: TSplitter;
    Panel1: TPanel;
    lblRevisio: TLabel;
    Label2: TLabel;
    lblCodigo: TLabel;
    RC_NOMBRE: TDBEdit;
    RC_CODIGO: TZetaDBNumero;
    RC_ORDEN: TZetaDBNumero;
    Panel2: TPanel;
    PanelCamposDefault: TPanel;
    BitBtn12: TBitBtn;
    BitBtn13: TBitBtn;
    Panel8: TPanel;
    TablaSubir: TSpeedButton;
    TablaBajar: TSpeedButton;
    DBGrid: TDBGrid;
    dsTablasPorClasificacion: TDataSource;
    RC_ACTIVO: TDBCheckBox;
    Label3: TLabel;
    Label4: TLabel;
    RC_VERSION: TDBEdit;
    cbSistema: TCheckBox;
    US_CODIGO: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure TablaSubirClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure ConectaTablasPorClasificacion;
    procedure EnabledControls(const lEnabled: Boolean);
  protected
    procedure Connect; override;
  public
  end;

var
  EditClasifiReporte: TEditClasifiReporte;

implementation

uses DCliente,dDiccionario, ZetaCommonClasses, ZAccesosTress, DBaseDiccionario;

{$R *.DFM}

procedure TEditClasifiReporte.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CRC_CATALOGOS_CLASIFICACIONES;
     HelpContext:= 0; //Pendiente
     FirstControl := RC_NOMBRE;
     TablaSubir.Visible := dmCliente.GetGrupoActivo = D_GRUPO_SIN_RESTRICCION;
     TablaBajar.Visible := dmCliente.GetGrupoActivo = D_GRUPO_SIN_RESTRICCION;
     {$ifdef  RDD_Desarrollo }
     RC_VERSION.ReadOnly := False;
     {$endif}
end;

procedure TEditClasifiReporte.Connect;
begin
     with dmDiccionario do
     begin
          //cdsClasificaciones.Conectar;
          dsTablasPorClasificacion.DataSet := cdsTablasPorClasificacion;
          Datasource.Dataset := cdsClasificaciones;
          ConectaTablasPorClasificacion;
     end;
end;

procedure TEditClasifiReporte.EnabledControls( const lEnabled: Boolean );
begin
     cbSistema.Checked := NOT lEnabled;
     if lEnabled then
     begin
          lblCodigo.Enabled := lEnabled;
          RC_CODIGO.Enabled := lEnabled;
     end;
end;


procedure TEditClasifiReporte.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
     inherited;
     with dmDiccionario do
     begin
          EnabledControls( not  ClasificacionSistema(cdsClasificaciones.FieldByName('RC_CODIGO').AsInteger));
     end;
end;


procedure TEditClasifiReporte.ConectaTablasPorClasificacion;
begin
     with dmDiccionario do
     begin
          if ( cdsClasificaciones.State <> dsInsert )then
          begin
               cdsTablasPorClasificacion.Refrescar;
               cdsTablasPorClasificacion.Filter := Format( 'RC_CODIGO = %d', [cdsClasificaciones.FieldByName('RC_CODIGO').AsInteger] );
               cdsTablasPorClasificacion.Filtered := TRUE;
          end
          else
               cdsTablasPorClasificacion.Active := FALSE;
     end;
end;

procedure TEditClasifiReporte.TablaSubirClick(Sender: TObject);
var
   oCursor: TCursor;
   iPosicion: integer;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;

     try
        with dmDiccionario do
        begin
             cdsTablasPorClasificacion.DisableControls;
             try
                iPosicion := cdsTablasPorClasificacion.FieldByName('EN_CODIGO').AsInteger;
                TablasPorClasificacionMover(TControl(Sender).Tag);
                ConectaTablasPorClasificacion;
                cdsTablasPorClasificacion.Locate('EN_CODIGO',iPosicion,[]);
             finally
                    dmDiccionario.cdsTablasPorClasificacion.EnableControls;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TEditClasifiReporte.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     inherited;
     dmDiccionario.cdsTablasPorClasificacion.Filtered := FALSE;
end;

end.
