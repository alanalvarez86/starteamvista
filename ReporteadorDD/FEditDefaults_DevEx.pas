unit FEditDefaults_DevEx;
{$INCLUDE DEFINES.INC}
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Buttons,
  DBCtrls, StdCtrls, ExtCtrls, Mask, ComCtrls, ZetaClientDataset,
  ZetaNumero, ZetaKeyLookup, ZetaEdit, ZetaSmartLists, ZetaDBTextBox,
  ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, cxControls, dxBarExtItems, dxBar, cxClasses, Vcl.ImgList,
  cxNavigator, cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx;


type
  TEditDefaults_DevEx = class(TBaseEdicion_DevEx)
    Label3: TLabel;
    Label1: TLabel;
    RD_ENTIDAD: TZetaDBKeyLookup_DevEx;
    AT_CAMPO: TDBEdit;
    RD_VERSION: TDBEdit;
    Label12: TLabel;
    Label4: TLabel;
    US_CODIGO: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FCampoAtributo: string;
    FCampoEntidad: string;
    FCampoVersion :string;
    FDataSet: TZetaClientDataset;
  protected
    procedure Connect; override;
    procedure DisConnect;override;
    function PuedeAgregar(var sMensaje: String): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    function PuedeModificar(var sMensaje: String): Boolean; override;
    function PuedeImprimir(var sMensaje: String): Boolean; override;

  public
   property Dataset: TZetaClientDataset read FDataSet write FDataset;
   property CampoEntidad: string read FCampoEntidad write FCampoEntidad;
   property CampoAtributo: string read FCampoAtributo write FCampoAtributo;
   property CampoVersion: string read FCampoVersion write FCampoVersion;

  end;

var
  EditDefaults_DevEx: TEditDefaults_DevEx;

implementation

uses dDiccionario;

{$R *.DFM}

procedure TEditDefaults_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     //IndexDerechos := D_CONS_CLASIFICACIONES;
     HelpContext:= 0; //Pendiente
     FirstControl := RD_ENTIDAD;
     FCampoAtributo := 'AT_CAMPO';
     FCampoVersion := 'AT_VERSION';

     RD_ENTIDAD.LookupDataset := dmDiccionario.cdsEntidadesLookup;
     {$ifdef  RDD_Desarrollo}
     RD_VERSION.ReadOnly := False;
     {$endif}
end;

procedure TEditDefaults_DevEx.Connect;
begin
     RD_ENTIDAD.DataField := CampoEntidad;
     AT_CAMPO.DataField := CampoAtributo;
     RD_VERSION.DataField := CampoVersion;
     with dmDiccionario do
     begin
          cdsEntidadesLookup.Conectar;
          Dataset.Conectar;
          Datasource.Dataset := Dataset;
     end;
end;

procedure TEditDefaults_DevEx.DisConnect;
begin
     DataSource.Dataset := NIL;
end;
function TEditDefaults_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := TRUE;
end;

function TEditDefaults_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := TRUE;
end;

function TEditDefaults_DevEx.PuedeImprimir(var sMensaje: String): Boolean;
begin
     Result := TRUE;
end;

function TEditDefaults_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := TRUE;
end;

procedure TEditDefaults_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     //Esta pantalla no tiene derecho de acceso por si sola.
     Datasource.AutoEdit := TRUE;
end;

end.
