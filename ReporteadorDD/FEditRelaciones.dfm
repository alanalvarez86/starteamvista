inherited EditRelaciones: TEditRelaciones
  Left = 393
  Top = 281
  Caption = 'Relaci'#243'n'
  ClientHeight = 211
  ClientWidth = 400
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel [0]
    Left = 43
    Top = 68
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Campos:'
    WordWrap = True
  end
  object Label1: TLabel [1]
    Left = 30
    Top = 44
    Width = 54
    Height = 13
    Alignment = taRightJustify
    Caption = 'Relaci'#243'n a:'
  end
  object Label12: TLabel [2]
    Left = 46
    Top = 132
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'Versi'#243'n:'
    Enabled = False
  end
  object US_CODIGO: TZetaDBTextBox [3]
    Left = 91
    Top = 151
    Width = 200
    Height = 17
    AutoSize = False
    Caption = 'US_CODIGO'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'US_CODIGO'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object Label2: TLabel [4]
    Left = 43
    Top = 152
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Modific'#243':'
    Enabled = False
  end
  inherited PanelBotones: TPanel
    Top = 175
    Width = 400
    TabOrder = 1
    inherited OK: TBitBtn
      Left = 225
    end
    inherited Cancelar: TBitBtn
      Left = 310
    end
  end
  inherited PanelSuperior: TPanel
    Width = 400
    TabOrder = 2
    inherited BuscarBtn: TSpeedButton
      Visible = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 400
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 74
    end
  end
  object RL_ENTIDAD: TZetaDBKeyLookup [8]
    Left = 91
    Top = 40
    Width = 300
    Height = 21
    TabOrder = 3
    TabStop = True
    WidthLlave = 60
    DataField = 'RL_ENTIDAD'
    DataSource = DataSource
  end
  object RL_CAMPOS: TDBEdit [9]
    Left = 91
    Top = 64
    Width = 297
    Height = 21
    CharCase = ecUpperCase
    DataField = 'RL_CAMPOS'
    DataSource = DataSource
    TabOrder = 4
  end
  object RL_INNER: TDBCheckBox [10]
    Left = 6
    Top = 88
    Width = 97
    Height = 17
    Alignment = taLeftJustify
    Caption = #191' Es Inner Join ?'
    DataField = 'RL_INNER'
    DataSource = DataSource
    TabOrder = 5
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  object RL_VERSION: TDBEdit [11]
    Left = 91
    Top = 128
    Width = 123
    Height = 21
    DataField = 'RL_VERSION'
    DataSource = DataSource
    MaxLength = 30
    ReadOnly = True
    TabOrder = 7
  end
  object RL_ACTIVO: TDBCheckBox [12]
    Left = 48
    Top = 107
    Width = 55
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Activo'
    DataField = 'RL_ACTIVO'
    DataSource = DataSource
    TabOrder = 6
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  inherited DataSource: TDataSource
    Left = 332
    Top = 1
  end
end
