inherited dmSistema: TdmSistema
  OldCreateOrder = True
  Left = 228
  Top = 251
  inherited cdsGrupos: TZetaLookupDataSet
    Aggregates = <
      item
        Active = True
        Expression = 'Max( GR_CODIGO ) + 0'
        Visible = False
      end>
  end
  inherited cdsImpresoras: TZetaClientDataSet
    Left = 112
  end
  inherited cdsAccesosBase: TZetaClientDataSet
    Left = 112
    Top = 136
  end
  inherited cdsGruposTodos: TZetaLookupDataSet
    Aggregates = <
      item
        Active = True
        Expression = 'Max( GR_CODIGO ) + 0'
        Visible = False
      end>
  end
  object cdsClasificacionesAccesos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = GenericReconcileError
    AlEnviarDatos = cdsClasificacionesAccesosAlEnviarDatos
    Left = 272
    Top = 288
  end
  object cdsEntidadesAccesos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = GenericReconcileError
    AlEnviarDatos = cdsEntidadesAccesosAlEnviarDatos
    Left = 400
    Top = 288
  end
end
