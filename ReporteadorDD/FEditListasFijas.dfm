inherited EditListasFijas: TEditListasFijas
  Left = 304
  Top = 254
  Caption = 'Lista de valores'
  ClientHeight = 274
  ClientWidth = 394
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel [0]
    Left = 7
    Top = 78
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descripci'#243'n:'
  end
  object Label1: TLabel [1]
    Left = 30
    Top = 56
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo:'
  end
  object Label3: TLabel [2]
    Left = 13
    Top = 98
    Width = 53
    Height = 26
    Alignment = taRightJustify
    Caption = 'Valores de la lista:'
    WordWrap = True
  end
  object Label4: TLabel [3]
    Left = 28
    Top = 194
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'Versi'#243'n:'
    Enabled = False
  end
  object US_CODIGO: TZetaDBTextBox [4]
    Left = 70
    Top = 214
    Width = 200
    Height = 17
    AutoSize = False
    Caption = 'US_CODIGO'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'US_CODIGO'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object Label6: TLabel [5]
    Left = 23
    Top = 215
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Modific'#243':'
    Enabled = False
  end
  inherited PanelBotones: TPanel
    Top = 238
    Width = 394
    TabOrder = 1
    inherited OK: TBitBtn
      Left = 219
    end
    inherited Cancelar: TBitBtn
      Left = 304
    end
  end
  inherited PanelSuperior: TPanel
    Width = 394
    TabOrder = 2
    inherited BuscarBtn: TSpeedButton
      Visible = True
    end
    inherited DBNavigator: TDBNavigator
      Visible = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 394
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 68
    end
  end
  object LV_NOMBRE: TDBEdit [9]
    Left = 70
    Top = 75
    Width = 300
    Height = 21
    DataField = 'LV_NOMBRE'
    DataSource = DataSource
    MaxLength = 30
    TabOrder = 6
  end
  object LV_CODIGO: TZetaDBNumero [10]
    Left = 70
    Top = 53
    Width = 121
    Height = 21
    Mascara = mnDias
    TabOrder = 3
    Text = '0'
    DataField = 'LV_CODIGO'
    DataSource = DataSource
  end
  object LV_VALORES: TMemo [11]
    Left = 70
    Top = 98
    Width = 300
    Height = 89
    ScrollBars = ssVertical
    TabOrder = 7
    WordWrap = False
    OnChange = LV_VALORESChange
  end
  object cbSistema: TCheckBox [12]
    Left = 260
    Top = 55
    Width = 110
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Lista fija de sistema'
    Enabled = False
    TabOrder = 5
  end
  object LV_VERSION: TDBEdit [13]
    Left = 70
    Top = 190
    Width = 123
    Height = 21
    DataField = 'LV_VERSION'
    DataSource = DataSource
    MaxLength = 30
    ReadOnly = True
    TabOrder = 8
  end
  object LV_ACTIVO: TDBCheckBox [14]
    Left = 321
    Top = 39
    Width = 49
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Activo'
    DataField = 'LV_ACTIVO'
    DataSource = DataSource
    TabOrder = 4
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  inherited DataSource: TDataSource
    Left = 332
    Top = 1
  end
end
