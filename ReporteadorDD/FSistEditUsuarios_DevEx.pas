unit FSistEditUsuarios_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FSistBaseEditUsuarios_DevEx, Db, ZetaDBTextBox, StdCtrls, ZetaKeyCombo,
  ZetaKeyLookup, DBCtrls, Mask, ZetaNumero, ComCtrls, ExtCtrls, Buttons,
  ZetaSmartLists, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, cxControls, dxBarBuiltInMenu, dxBarExtItems,
  dxBar, cxClasses, Vcl.ImgList, ZetaKeyLookup_DevEx, cxPC, cxNavigator,
  cxDBNavigator, cxButtons;

type
  TSistEditUsuarios_DevEx = class(TSistBaseEditUsuarios_DevEx)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SistEditUsuarios_DevEx: TSistEditUsuarios_DevEx;

implementation
{$R *.DFM}

end.
