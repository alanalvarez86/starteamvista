inherited EditRelaciones_DevEx: TEditRelaciones_DevEx
  Left = 393
  Top = 281
  Caption = 'Relaci'#243'n'
  ClientHeight = 211
  ClientWidth = 400
  ExplicitWidth = 406
  ExplicitHeight = 240
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel [0]
    Left = 43
    Top = 68
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Campos:'
    WordWrap = True
  end
  object Label1: TLabel [1]
    Left = 30
    Top = 44
    Width = 54
    Height = 13
    Alignment = taRightJustify
    Caption = 'Relaci'#243'n a:'
  end
  object Label12: TLabel [2]
    Left = 47
    Top = 129
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'Versi'#243'n:'
    Enabled = False
  end
  object US_CODIGO: TZetaDBTextBox [3]
    Left = 91
    Top = 151
    Width = 200
    Height = 17
    AutoSize = False
    Caption = 'US_CODIGO'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'US_CODIGO'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object Label2: TLabel [4]
    Left = 43
    Top = 152
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Modific'#243':'
    Enabled = False
  end
  inherited PanelBotones: TPanel
    Top = 175
    Width = 400
    TabOrder = 1
    ExplicitTop = 175
    ExplicitWidth = 400
    inherited OK_DevEx: TcxButton
      Left = 226
      ParentFont = False
      ExplicitLeft = 226
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 307
      ParentFont = False
      ExplicitLeft = 307
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 400
    TabOrder = 0
    ExplicitTop = 26
    ExplicitWidth = 400
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 317
      end
    end
    inherited ValorActivo2: TPanel
      Width = 74
      ExplicitWidth = 74
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 68
        ExplicitLeft = -12
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    Left = 0
    Top = 171
    Width = 72
    TabOrder = 11
    ExplicitLeft = 0
    ExplicitTop = 171
    ExplicitWidth = 72
  end
  object RL_ENTIDAD: TZetaDBKeyLookup_DevEx [8]
    Left = 91
    Top = 40
    Width = 300
    Height = 21
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 3
    TabStop = True
    WidthLlave = 60
    DataField = 'RL_ENTIDAD'
    DataSource = DataSource
  end
  object RL_CAMPOS: TDBEdit [9]
    Left = 91
    Top = 64
    Width = 298
    Height = 21
    CharCase = ecUpperCase
    DataField = 'RL_CAMPOS'
    DataSource = DataSource
    TabOrder = 4
  end
  object RL_INNER: TDBCheckBox [10]
    Left = 8
    Top = 87
    Width = 97
    Height = 17
    Alignment = taLeftJustify
    Caption = ' '#191'Es Inner Join?'
    DataField = 'RL_INNER'
    DataSource = DataSource
    TabOrder = 5
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  object RL_VERSION: TDBEdit [11]
    Left = 91
    Top = 125
    Width = 123
    Height = 21
    DataField = 'RL_VERSION'
    DataSource = DataSource
    MaxLength = 30
    ReadOnly = True
    TabOrder = 7
  end
  object RL_ACTIVO: TDBCheckBox [12]
    Left = 47
    Top = 106
    Width = 58
    Height = 17
    Alignment = taLeftJustify
    Caption = '  Activo'
    DataField = 'RL_ACTIVO'
    DataSource = DataSource
    TabOrder = 6
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  inherited DataSource: TDataSource
    Left = 332
    Top = 1
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 4194696
    ImageInfo = <
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF98A1E2FFF6F7FDFFB7BEEBFF4A5ACDFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFB1B8
          E9FFF6F7FDFFACB3E8FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFFE2E5F7FFFFFFFFFFFFFFFFFFC3C8EEFF4A5ACDFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFF
          FFFFFFFFFFFFF6F7FDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF9BA4E3FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4A5A
          CDFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFA6AEE6FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4A5ACDFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4A5ACDFF4A5ACDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFC3C8EEFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6
          E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8
          EEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4A5ACDFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4A5ACDFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFAFB6E9FFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4A5A
          CDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4A5ACDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4A5ACDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFFA9B0E7FFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFB7BEEBFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFFE2E5F7FFFFFFFFFFFFFFFFFFAFB6E9FF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFF6F7FDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF8792DEFFDFE2F6FFA1A9E5FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF9BA4
          E3FFDFE2F6FF98A1E2FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF65D8
          A1FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF8CE2B8FFE9F9
          F1FF54D396FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF65D8A1FFF4FCF8FFFFFF
          FFFFADEACCFF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF52D395FFD6F4E6FFFFFFFFFFFFFF
          FFFFFCFEFDFF70DAA7FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FFA5E8C8FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFD9F5E7FF52D395FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF78DDACFFFCFEFDFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF9AE5C1FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF5AD59AFFE9F9F1FFFFFFFFFFFFFFFFFFC3EFDAFFBDEE
          D6FFFFFFFFFFFFFFFFFFF4FCF8FF62D79FFF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FFC3EFDAFFFFFFFFFFF7FDFAFF8CE2B8FF4FD293FF54D3
          96FFE1F7ECFFFFFFFFFFFFFFFFFFC8F1DDFF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF8AE1B7FFE6F9F0FF6AD9A4FF4FD293FF4FD293FF4FD2
          93FF5FD69DFFE4F8EEFFFFFFFFFFFFFFFFFF8FE2BAFF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF52D395FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF65D8A1FFEFFBF5FFFFFFFFFFF7FDFAFF68D8A2FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF6AD9A4FFEFFBF5FFFFFFFFFFDEF7EBFF52D395FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF78DDACFFF7FDFAFFFFFFFFFFB8EDD3FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF70DAA7FFEFFBF5FFFFFFFFFFA0E6
          C4FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF65D8A1FFE1F7ECFFFFFF
          FFFF8FE2BAFF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF57D498FFBDEE
          D6FFFCFEFDFF8AE1B7FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF81DFB1FF8FE2BAFF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF5261CFFF6C79D6FF8D97DFFFACB3E8FFC8CD
          F0FFE8EAF9FFBAC0ECFF6F7CD7FF7582D9FF6471D4FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF848FDDFF8D97DFFFACB3E8FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFF969FE2FF6471D4FF969FE2FF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFCFC
          FEFFD4D8F3FFBAC0ECFF4757CCFF4757CCFF969FE2FFF9FAFDFFFFFFFFFFFFFF
          FFFFA4ACE6FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE5E7F8FFBAC0ECFF4757CCFF4757CCFF6F7CD7FFD1D5F2FFE5E7F8FFBAC0
          ECFF818CDCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF6976D6FF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFFACB3E8FFBAC0ECFFB1B8E9FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF5564D0FF7582D9FF969FE2FFB1B8
          E9FFD1D5F2FFACB3E8FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end>
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    Left = 384
    Top = 96
    DockControlHeights = (
      0
      0
      26
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    DesignInfo = 8913288
  end
end
