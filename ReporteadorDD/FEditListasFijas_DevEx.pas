unit FEditListasFijas_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Buttons,
  DBCtrls, StdCtrls, ExtCtrls, Mask, ComCtrls,
  ZetaNumero, ZetaKeyLookup, ZetaEdit, ZetaSmartLists, ZetaDBTextBox,
  ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, cxControls, dxBarExtItems, dxBar, cxClasses, Vcl.ImgList,
  cxNavigator, cxDBNavigator, cxButtons;


type
  TEditListasFijas_DevEx = class(TBaseEdicion_DevEx)
    Label2: TLabel;
    LV_NOMBRE: TDBEdit;
    Label1: TLabel;
    LV_CODIGO: TZetaDBNumero;
    LV_VALORES: TMemo;
    Label3: TLabel;
    cbSistema: TCheckBox;
    Label4: TLabel;
    LV_VERSION: TDBEdit;
    US_CODIGO: TZetaDBTextBox;
    Label6: TLabel;
    LV_ACTIVO: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure LV_VALORESChange(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
    FMemoCambio: Boolean;
    procedure EnabledControls(const lEnabled: Boolean);

  protected
    procedure Connect; override;
    procedure EscribirCambios; override;
  public
  end;

var
  EditListasFijas_DevEx: TEditListasFijas_DevEx;

implementation

uses dDiccionario,
     ZetaCommonTools,
     ZAccesosTress,
     ZetaCommonLists,
     ZetaCommonClasses;

{$R *.DFM}

procedure TEditListasFijas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CRC_CATALOGO_VALORES;
     HelpContext:= H_Lista_Valores; //Pendiente
     FirstControl := LV_CODIGO;
end;

procedure TEditListasFijas_DevEx.Connect;
begin
     with dmDiccionario do
     begin
          cdsListasFijas.Conectar;
          Datasource.Dataset := cdsListasFijas;
          LV_VALORES.Lines.Clear;
          LlenaLista( cdsListasFijas.FieldByName('LV_CODIGO').AsInteger, LV_VALORES.Lines, FALSE );
     end;
     FMemoCambio := FALSE;
     Modo := dsBrowse;
end;

procedure TEditListasFijas_DevEx.EscribirCambios;
 var sValores: string;
begin
     with dmDiccionario do
     begin
          with cdsListasFijas do
          begin
               if ( State = dsBrowse ) then
               begin
                    Edit;
                    FieldByName('LV_DUMMY').AsInteger := FieldByName('LV_DUMMY').AsInteger + 1;
               end;
          end;
          sValores := '';

          if FMemoCambio then
             sValores := LV_VALORES.Lines.CommaText;

          GrabaListasFijas( sValores );
     end;
     FMemoCambio := FALSE;
end;

procedure TEditListasFijas_DevEx.LV_VALORESChange(Sender: TObject);
begin
     inherited;
     Modo := dsEdit;
     FMemoCambio:= TRUE;
end;

procedure TEditListasFijas_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
     inherited;
     with dmDiccionario do
     begin
          EnabledControls( not RecordSistema( cdsListasFijas ) );
     end;

end;

procedure TEditListasFijas_DevEx.EnabledControls(const lEnabled: Boolean);
begin
     cbSistema.Checked := not lEnabled;
end;

end.
