unit FListasFijas_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, DB, ExtCtrls, Grids, DBGrids, ZetaDBGrid, Buttons,
  Vcl.StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList,
  Vcl.ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint,
  dxSkinsDefaultPainters, dxSkinscxPCPainter;

type
  TListasFijas_DevEx = class(TBaseGridLectura_DevEx)
    ZetaDBGrid1: TZetaDBGrid;
    LV_CODIGO: TcxGridDBColumn;
    LV_NOMBRE: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
           procedure Connect; override;
           procedure Refresh; override;
           procedure Agregar;override;
           procedure Borrar; override;
           procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  ListasFijas_DevEx: TListasFijas_DevEx;

implementation

uses
    dDiccionario, ZetaCommonClasses;
{$R *.dfm}

{ TClasifiReporte }

procedure TListasFijas_DevEx.Agregar;
begin
     inherited;
     dmDiccionario.cdsListasFijas.Agregar;
end;

procedure TListasFijas_DevEx.Borrar;
begin
     inherited;
     dmDiccionario.cdsListasFijas.Borrar;
end;

procedure TListasFijas_DevEx.Connect;
begin
     inherited;
     with dmDiccionario do
     begin
          cdsListasFijas.Conectar;
          DataSource.DataSet:= cdsListasFijas;
     end;
end;

procedure TListasFijas_DevEx.FormCreate(Sender: TObject);
begin
  inherited;
  HelpContext:= H_Lista_Valores;
  ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= TRUE;
end;

procedure TListasFijas_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  CreaColumaSumatoria(ZetaDbGridDBTableView.Columns[0], 0 ,'' , SkCount );
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := False;
  ApplyMinWidth;
  ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TListasFijas_DevEx.Modificar;
begin
     inherited;
     dmDiccionario.cdsListasFijas.Modificar;

end;

procedure TListasFijas_DevEx.Refresh;
begin
     inherited;
     dmDiccionario.cdsListasFijas.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

end.
