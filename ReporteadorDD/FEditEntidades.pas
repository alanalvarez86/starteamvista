unit FEditEntidades;

{$INCLUDE DEFINES.INC}
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, Db, Buttons,
  DBCtrls, StdCtrls, ExtCtrls, Mask, ComCtrls,
  ZetaNumero, ZetaKeyLookup, ZetaEdit, ZetaSmartLists, Grids, DBGrids,
  Menus,ZetaClientDataSet,
  ZetaCommonLists, ZetaDBGrid, ActnList, ZetaDBTextBox;


type
  TEditEntidades = class(TBaseEdicion)
    Panel1: TPanel;
    PageControl: TPageControl;
    Label2: TLabel;
    lbTabla: TLabel;
    EN_TITULO: TDBEdit;
    EN_CODIGO: TZetaDBNumero;
    EN_TABLA: TDBEdit;
    lbNombre: TLabel;
    EN_DESCRIP: TDBMemo;
    Label4: TLabel;
    cbSistema: TCheckBox;
    Label5: TLabel;
    EN_PRIMARY: TDBEdit;
    Label1: TLabel;
    EN_ATDESC: TDBEdit;
    tsCampos: TTabSheet;
    tsClasificaciones: TTabSheet;
    tsModulos: TTabSheet;
    tsRelaciones: TTabSheet;
    PanelCampos: TPanel;
    DBGridCampos: TZetaDBGrid;
    BBModificar: TBitBtn;
    DBGrid1: TDBGrid;
    Panel4: TPanel;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    PanelRelaciones: TPanel;
    DBGrid3: TDBGrid;
    PanelMover: TPanel;
    RelacionesSubir: TSpeedButton;
    Bajar: TSpeedButton;
    PanelClasifi: TPanel;
    dsClasificaciones: TDataSource;
    dsModulos: TDataSource;
    dsRelaciones: TDataSource;
    PanelModulos: TPanel;
    DBGrid2: TDBGrid;
    BitBtn6: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn10: TBitBtn;
    BitBtn11: TBitBtn;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn7: TBitBtn;
    BitBtn8: TBitBtn;
    BitBtn9: TBitBtn;
    dsCampos: TDataSource;
    Label3: TLabel;
    EN_ALIAS: TZetaDBKeyLookup;
    SpeedButton1: TSpeedButton;
    tsCamposDefault: TTabSheet;
    tsFiltrosDefault: TTabSheet;
    tsOrdenDefault: TTabSheet;
    PanelCamposDefault: TPanel;
    BitBtn12: TBitBtn;
    BitBtn13: TBitBtn;
    BitBtn14: TBitBtn;
    Panel3: TPanel;
    CamposDefaultSubir: TSpeedButton;
    CamposDefaultBajar: TSpeedButton;
    PanelOrdenDefault: TPanel;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    BitBtn17: TBitBtn;
    Panel6: TPanel;
    OrdenDefaultSubir: TSpeedButton;
    OrdenDefaultBajar: TSpeedButton;
    PanelFiltrosDefault: TPanel;
    BitBtn18: TBitBtn;
    BitBtn19: TBitBtn;
    BitBtn20: TBitBtn;
    Panel8: TPanel;
    FiltroDefaultSubir: TSpeedButton;
    FiltroDefaultBajar: TSpeedButton;
    dsCamposDefault: TDataSource;
    dsFiltrosDefault: TDataSource;
    dsOrdenDefault: TDataSource;
    DBGridOrdenDefault: TDBGrid;
    DBGridFiltrosDefault: TDBGrid;
    PopupMenuCampo: TPopupMenu;
    Agregarcomocampodefault1: TMenuItem;
    Agregarcomoordendefault1: TMenuItem;
    Agregarcomofiltrodefault1: TMenuItem;
    Agregarunarelacinsobreestecampo1: TMenuItem;
    Agregarsudescripcincomocampodefaul1: TMenuItem;
    dbGridCamposDefault: TDBGrid;
    SpeedButton2: TSpeedButton;
    ActionList1: TActionList;
    actAgregaCampos: TAction;
    actAgregaRelaciones: TAction;
    EN_ACTIVO: TDBCheckBox;
    EN_VERSION: TDBEdit;
    Label6: TLabel;
    Label7: TLabel;
    US_CODIGO: TZetaDBTextBox;
    EN_NIVEL0: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BBModificarClick(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure BitBtn11Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BitBtn12Click(Sender: TObject);
    procedure BitBtn13Click(Sender: TObject);
    procedure BitBtn14Click(Sender: TObject);
    procedure BitBtn15Click(Sender: TObject);
    procedure BitBtn16Click(Sender: TObject);
    procedure BitBtn17Click(Sender: TObject);
    procedure BitBtn18Click(Sender: TObject);
    procedure BitBtn19Click(Sender: TObject);
    procedure BitBtn20Click(Sender: TObject);
    procedure DataSourceStateChange(Sender: TObject);
    procedure RelacionesSubirClick(Sender: TObject);
    procedure CamposDefaultBajarClick(Sender: TObject);
    procedure OrdenDefaultSubirClick(Sender: TObject);
    procedure FiltroDefaultSubirClick(Sender: TObject);
    procedure EN_TITULOExit(Sender: TObject);
    procedure Agregarunarelacinsobreestecampo1Click(Sender: TObject);
    procedure Agregarsudescripcincomocampodefaul1Click(Sender: TObject);
    procedure dsCamposDataChange(Sender: TObject; Field: TField);
    procedure SpeedButton2Click(Sender: TObject);
    procedure actAgregaCamposExecute(Sender: TObject);
  private
    procedure EnabledControls(const lEnabled: Boolean);
    procedure BorraRegistro(Dataset: TZetaClientDataset);
    procedure ModificaRegistro(Dataset: TZetaClientDataset);

  protected
    procedure Connect; override;
    procedure Disconnect;override;
    procedure Agregar;override;
  public
  end;

var
  EditEntidades: TEditEntidades;

implementation

uses dDiccionario,
     dCliente,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZAccesosTress,
     ZAccesosMgr,
     DBaseDiccionario, DateUtils;

{$R *.DFM}

procedure TEditEntidades.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CRC_DICCIONARIO_DATOS;
     HelpContext:= 0; //Pendiente
     FirstControl := EN_CODIGO;
     PageControl.ActivePage := tsCampos;
     Agregarcomocampodefault1.Tag := ord( eCamposDefault );
     Agregarcomoordendefault1.Tag := ord( eOrdenDefault );
     Agregarcomofiltrodefault1.Tag := ord( eFiltrosDefault );
     Agregarunarelacinsobreestecampo1.Tag := ord( eRelacionesPorEntidad );
     {$ifdef RDD_Desarrollo}
     EN_CODIGO.Enabled := TRUE;
     EN_VERSION.ReadOnly := False;
     {$endif}
end;

procedure TEditEntidades.Connect;
 var
    iEntidad: integer;
begin
     with dmDiccionario do
     begin
          cdsEntidadesLookup.Conectar;
          if ( cdsEntidades.State = dsInsert ) then
             iEntidad := -1
          else
              iEntidad := cdsEntidades.FieldByName('EN_CODIGO').AsInteger;

          ConectarDatasetEntidades( iEntidad );
          EN_ALIAS.LookupDataset := cdsEntidadesLookup;

          Datasource.Dataset := cdsEntidades;
          dsClasificaciones.Dataset := cdsTablasPorClasificacion;
          dsModulos.Dataset := cdsEntidadesPorModulo;
          dsRelaciones.Dataset := cdsRelaciones;
          dsCampos.Dataset := cdsCamposPorTabla;
          dsCamposDefault.DataSet := cdsCamposDefault;
          dsFiltrosDefault.DataSet := cdsFiltrosDefault;
          dsOrdenDefault.DataSet := cdsOrdenDefault;
     end;

                          {CV: Si la empresa activa NO es de Tress, el control se deshabilita}
     EN_NIVEL0.Enabled := (dmCliente.TipoCompany = tc3Datos) and
                          {CV: Si el usuario no tiene derechos para modificar la Confidencialidad(NIVEL0) se deshabilita el control}
                          ZAccesosMgr.CheckDerecho( D_CRC_DICCIONARIO_DATOS, K_DERECHO_SIST_KARDEX ) ;
                          



end;

procedure TEditEntidades.Disconnect;
begin
     with dmDiccionario do
     begin
          cdsTablasPorClasificacion.Close;
          cdsEntidadesPorModulo.Close;
          cdsRelaciones.Close;
          cdsCamposPorTabla.Close;
    end;
end;


procedure TEditEntidades.DataSourceDataChange(Sender: TObject; Field: TField);
 var
    sFiller: string;
begin
     inherited;
     with dmDiccionario do
     begin
          EnabledControls( not RecordSistema( cdsEntidades ) );
          with cdsEntidades do
          begin
               sFiller := Replicate(' ',35);
               PanelCampos.Caption := Format(sFiller+'Campos que contiene la tabla %s:',[FieldByName('EN_TABLA').AsString]);
               PanelRelaciones.Caption := Format(sFiller+'Tablas con las que se relaciona la tabla %s:',[FieldByName('EN_TABLA').AsString]);
               PanelModulos.Caption := Format(sFiller+'M�dulos en los que se muestra la tabla %s:',[FieldByName('EN_TABLA').AsString]);
               PanelClasifi.Caption := Format(sFiller+'Clasificaciones en las que se muestra la tabla %s:',[FieldByName('EN_TABLA').AsString]);
               PanelCamposDefault.Caption := Format(sFiller+'Campos default que se muestran al crear un reporte de la tabla %s:',[FieldByName('EN_TABLA').AsString]);
               PanelFiltrosDefault.Caption := Format(sFiller+'Filtros default que se muestran al crear un reporte de la tabla %s:',[FieldByName('EN_TABLA').AsString]);
               PanelOrdenDefault.Caption := Format(sFiller+'Criterios de orden default que se muestran al crear un reporte de la tabla %s:',[FieldByName('EN_TABLA').AsString]);
          end;
     end;

end;

procedure TEditEntidades.EnabledControls( const lEnabled: Boolean );
begin
     cbSistema.Checked := NOT lEnabled;
     {$ifndef RDD_Desarrollo}
     EN_CODIGO.Enabled  := lEnabled;
     {$endif}
     lbTabla.Enabled := lEnabled;
     //EN_TABLA.Enabled := lEnabled;
     //lbNombre.Enabled := lEnabled;
end;

procedure TEditEntidades.BitBtn1Click(Sender: TObject);
begin
     inherited;
     dmDiccionario.cdsCamposPorTabla.Agregar;
end;

procedure TEditEntidades.BorraRegistro(Dataset: TZetaClientDataset);
begin
     with Dataset do
     begin
          if RecordCount > 0 then
             Borrar;
     end;
end;

procedure TEditEntidades.ModificaRegistro(Dataset: TZetaClientDataset);
begin
     with Dataset do
     begin
          if RecordCount > 0 then
             Modificar;
     end;
end;


procedure TEditEntidades.BitBtn2Click(Sender: TObject);
begin
     inherited;
     BorraRegistro(dmDiccionario.cdsCamposPorTabla);

end;

procedure TEditEntidades.BBModificarClick(Sender: TObject);
begin
     inherited;
     ModificaRegistro( dmDiccionario.cdsCamposPorTabla )

end;

procedure TEditEntidades.BitBtn10Click(Sender: TObject);
begin
     inherited;
     dmDiccionario.cdsTablasPorClasificacion.Agregar;
end;

procedure TEditEntidades.BitBtn11Click(Sender: TObject);
begin
     inherited;
     BorraRegistro(dmDiccionario.cdsTablasPorClasificacion);
end;

procedure TEditEntidades.BitBtn3Click(Sender: TObject);
begin
     inherited;
     dmDiccionario.cdsEntidadesPorModulo.Agregar;
end;

procedure TEditEntidades.BitBtn6Click(Sender: TObject);
begin
     inherited;
     BorraRegistro(dmDiccionario.cdsEntidadesPorModulo);
end;

procedure TEditEntidades.BitBtn7Click(Sender: TObject);
begin
     inherited;
     dmDiccionario.cdsRelaciones.Agregar;
end;

procedure TEditEntidades.BitBtn8Click(Sender: TObject);
begin
     inherited;
     BorraRegistro(dmDiccionario.cdsRelaciones);
end;

procedure TEditEntidades.BitBtn9Click(Sender: TObject);
begin
     inherited;
     ModificaRegistro( dmDiccionario.cdsRelaciones );
end;

procedure TEditEntidades.SpeedButton1Click(Sender: TObject);
begin
     inherited;
     dmDiccionario.AgregarCamposFaltantes;
end;

procedure TEditEntidades.Agregar;
begin
     ClientDataset.Append;
     Connect;
end;

procedure TEditEntidades.BitBtn12Click(Sender: TObject);
begin
     inherited;
     dmDiccionario.cdsCamposDefault.Agregar;
     DBGridCamposDefault.SetFocus;
end;

procedure TEditEntidades.BitBtn13Click(Sender: TObject);
begin
     inherited;
     BorraRegistro(dmDiccionario.cdsCamposDefault );
end;

procedure TEditEntidades.BitBtn14Click(Sender: TObject);
begin
     inherited;
     ModificaRegistro( dmDiccionario.cdsCamposDefault );

end;

procedure TEditEntidades.BitBtn15Click(Sender: TObject);
begin
     inherited;
     dmDiccionario.cdsOrdenDefault.Agregar;
     DBGridOrdenDefault.SetFocus;
end;

procedure TEditEntidades.BitBtn16Click(Sender: TObject);
begin
     inherited;
     BorraRegistro(dmDiccionario.cdsOrdenDefault);
end;

procedure TEditEntidades.BitBtn17Click(Sender: TObject);
begin
     inherited;
     ModificaRegistro( dmDiccionario.cdsOrdenDefault );

end;

procedure TEditEntidades.BitBtn18Click(Sender: TObject);
begin
     inherited;
     dmDiccionario.cdsFiltrosDefault.Agregar;
     DBGridFiltrosDefault.SetFocus;
end;

procedure TEditEntidades.BitBtn19Click(Sender: TObject);
begin
     inherited;
     BorraRegistro(dmDiccionario.cdsFiltrosDefault);
end;

procedure TEditEntidades.BitBtn20Click(Sender: TObject);
begin
     inherited;
     ModificaRegistro( dmDiccionario.cdsFiltrosDefault );
end;

procedure TEditEntidades.DataSourceStateChange(Sender: TObject);
begin
     inherited;
     PageControl.Enabled := dmDiccionario.cdsEntidades.State <> dsInsert;
end;

procedure TEditEntidades.RelacionesSubirClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        dmDiccionario.RelacionesMover(TControl(Sender).Tag);
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TEditEntidades.CamposDefaultBajarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        dmDiccionario.CamposDefaultMover(TControl(Sender).Tag);
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TEditEntidades.OrdenDefaultSubirClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        dmDiccionario.OrdenDefaultMover(TControl(Sender).Tag);
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TEditEntidades.FiltroDefaultSubirClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        dmDiccionario.FiltrosDefaultMover(TControl(Sender).Tag);
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TEditEntidades.EN_TITULOExit(Sender: TObject);
begin
     inherited;
     with dmDiccionario.cdsEntidades do
          if StrVacio( FieldByName('EN_DESCRIP').AsString ) and (State in [dsInsert,dsEdit]) then
          begin

               FieldByName('EN_DESCRIP').AsString := FieldByName('EN_TITULO').AsString
          end;
end;

procedure TEditEntidades.Agregarunarelacinsobreestecampo1Click(
  Sender: TObject);
begin
     inherited;
     with dmDiccionario do
          AgregarCampoDefault(  cdsCamposPorTabla.FieldByName('AT_CAMPO').AsString, eTipoDiccionario(TComponent(Sender).Tag) );
end;

procedure TEditEntidades.Agregarsudescripcincomocampodefaul1Click(
  Sender: TObject);
begin
     inherited;
     with dmDiccionario do
          AgregarDescripcionDefault(cdsCamposPorTabla.FieldByName('AT_CAMPO').AsString );
end;

procedure TEditEntidades.dsCamposDataChange(Sender: TObject;
  Field: TField);
begin
     inherited;
     with dmDiccionario.cdsCamposPorTabla do
          Agregarsudescripcincomocampodefaul1.Enabled := ( eTipoRango( FieldByName('AT_TRANGO').AsInteger ) = rRangoEntidad ) and
                                                         ( FieldByName('AT_ENTIDAD').AsInteger>= 0 )

end;

procedure TEditEntidades.SpeedButton2Click(Sender: TObject);
 var sPrimary: string;
begin
     inherited;
     with dmDiccionario,cdsEntidades do
     begin
          sPrimary := GetPrimaryKey;
          if (sPrimary <> FieldByName('EN_PRIMARY').AsString) then
          begin
               if ( State = dsBrowse ) then
                  Edit;
               FieldByName('EN_PRIMARY').AsString := sPrimary;
          end;
     end;
end;

procedure TEditEntidades.actAgregaCamposExecute(Sender: TObject);
begin
     inherited;
     {$ifdef CAROLINA}
     dmDiccionario.AgregarCamposFaltantes;
     dmDiccionario.AgregarRelacionesDefault;
     {$else}
     {$endif}
end;

end.




