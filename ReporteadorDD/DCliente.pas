unit DCliente;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBasicoCliente, Db, DBClient,
  ZetaCommonClasses,
  ZetaClientDataSet;

type
  TdmCliente = class(TBasicoCliente)
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
    procedure CargaActivosTodos(Parametros: TZetaParams);override;
    function GetListaEmpleados( const sFiltro : string ): string;
  end;

var
  dmCliente: TdmCliente;

implementation
uses
    ZetaCommonTools,
    ZetaCommonLists;

{$R *.DFM}



procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
     inherited;
     TipoCompany := tc3Datos;
end;

procedure TdmCliente.CargaActivosTodos(Parametros: TZetaParams);
begin
     with Parametros do
     begin
          AddDate( 'FechaAsistencia', FechaDefault );
          AddDate( 'FechaDefault', FechaDefault );
          AddInteger( 'YearDefault', TheYear( FechaDefault ) );
          AddInteger( 'Year', TheYear( FechaDefault ) );
          AddInteger( 'EmpleadoActivo', 0 );
          AddString( 'NombreUsuario', GetDatosUsuarioActivo.Nombre );
          AddString( 'CodigoEmpresa', GetDatosEmpresaActiva.Codigo );
     end;
end;


function TdmCliente.GetListaEmpleados(const sFiltro: string): string;
begin
     Result := '';
end;

end.
