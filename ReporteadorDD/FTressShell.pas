unit FTressShell;

interface

{$INCLUDE DEFINES.INC}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, ImgList, ActnList, ExtCtrls, ComCtrls, StdCtrls,
  Buttons, ZetaSmartLists, ZBaseShell, ZetaMessages, XPMan, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxRibbonCustomizationForm,
  System.Actions, cxStyles, cxClasses, dxSkinsForm, dxRibbon, dxStatusBar,
  dxRibbonStatusBar, ZBasicoNavBarShell, dxBarBuiltInMenu, cxLocalization, dxBar,
  dxNavBar, cxButtons, cxPC, ZetaClientDataset, dxBevel, cxBlobEdit,
  cxBarEditItem;


type
  TTressShell = class(TBasicoNavBarShell)
    XPManifest1: TXPManifest;
    _P_Importar: TAction;
    _P_Exportar: TAction;
    _P_Depurar: TAction;
    _P_Validar: TAction;
    TabProcesos: TdxRibbonTab;
    PProcesos: TdxBar;
    PProceso_btnImportar: TdxBarLargeButton;
    PProceso_btnExportar: TdxBarLargeButton;
    PProceso_btnDepurar: TdxBarLargeButton;
    PProceso_btnValidar: TdxBarLargeButton;
    ReporteadorCFGRibbonSmall: TcxImageList;
    ReporteadorCFGRibbonLarge: TcxImageList;
    dxBevel1: TdxBevel;
    TabArchivo_btnConfigurarEmpresa: TdxBarLargeButton;
    _A_ConfigurarEmpresa: TAction;
    AConsultas: TdxBar;
    TabArchivo_btnExplorardorReportes: TdxBarLargeButton;
    TabArchivo_btnSQL: TdxBarLargeButton;
    TabArchivo_btnBitacora: TdxBarLargeButton;
    TabArchivo_btnListaProcesos: TdxBarLargeButton;
    _A_ExploradorReportes: TAction;
    _A_SQL_DevEx: TAction;
    _A_Bitacora_DevEx: TAction;
    _A_ListaProcesos: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ClientAreaPG_DevExTcxPageControlPropertiesTcxPCCustomButtonsButtons0Click(
      Sender: TObject);
    procedure _P_ImportarExecute(Sender: TObject);
    procedure _P_ExportarExecute(Sender: TObject);
    procedure _A_ConfigurarEmpresaExecute(Sender: TObject);
    procedure _A_ExploradorReportesExecute(Sender: TObject);
    procedure _A_SQL_DevExExecute(Sender: TObject);
    procedure _A_Bitacora_DevExExecute(Sender: TObject);
    procedure _A_ListaProcesosExecute(Sender: TObject);
    procedure _H_GlosarioExecute(Sender: TObject);
  private
    { Private declarations }
    procedure WMWizardEnd(var Message: TMessage); message WM_WIZARD_END;
    procedure RevisaDerechos;
    procedure AsignacionTabOrder_DevEx;

  protected
    { Protected declarations }
    FHelpGlosario: String;
    procedure DoCloseAll;override;
    procedure DoOpenAll;override;
    procedure HabilitaControles; override;
    procedure CargaTraducciones; override;
  public
    { Public declarations }
    procedure CargaVista; override;
    procedure LLenaOpcionesGrupos;
  end;

  const
     K_PANEL_REPORTEADOR = 1;

var
  TressShell: TTressShell;

implementation

uses DCliente,
     DGlobal,
     DSistema,
     DCatalogos,
     DReportes,
     DDiccionario,
     DConsultas,
     ZetaClientTools,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaDialogo,
     ZAccesosMgr,
     ZAccesosTress,
     DProcesos,
     ZGlobalTress, ZetaDespConsulta;

{$R *.DFM}

procedure TTressShell.FormCreate(Sender: TObject);
begin
     Global := TdmGlobal.Create;
     dmCliente := TdmCliente.Create( Self );
     dmSistema := TdmSistema.Create( Self );
     dmCatalogos := TdmCatalogos.Create( Self );
     dmReportes := TdmReportes.Create( Self );
     dmDiccionario := TdmDiccionario.Create( Self );
     dmConsultas := TdmConsultas.Create( Self );
     dmProcesos := TdmProcesos.Create (Self);
     FHelpGlosario := 'Tress.chm';
     inherited;
     ZAccesosMgr.SetValidacion( TRUE );
     HelpContext := H00001_Pantalla_principal;
end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
     inherited;
     dmConsultas.Free;
     dmDiccionario.Free;
     dmReportes.Free;
     dmCatalogos.Free;
     dmSistema.Free;
     dmCliente.Free;
     dmProcesos.Free;
     Global.Free;
end;

procedure TTressShell.FormShow(Sender: TObject);
begin
  inherited;
  CargaVista;
  CargaTraducciones;
end;

procedure TTressShell.DoOpenAll;
const
     K_TAMANO_ARBOLITOS = 3;
begin
     try
        inherited DoOpenAll;
        Global.Conectar;
        SetArbolitosLength( K_TAMANO_ARBOLITOS );
        CreaNavBar;
        //CreaArbol( True );
        RevisaDerechos;
     except
           on Error : Exception do
           begin
                ZetaDialogo.zError( 'Error al Conectar', 'Empresa: ' + dmCliente.Compania + CR_LF + Error.Message, 0 );
                DoCloseAll;
           end;
     end;
     llenaopcionesGrupos;
end;

procedure TTressShell.CargaTraducciones;
begin
    DevEx_cxLocalizer.Active := True;
    DevEx_cxLocalizer.Locale := 2058;  // Carinal para Espanol (Mexico)
end;

//Procedures
procedure TTressShell.AsignacionTabOrder_DevEx;
begin
     DevEx_ShellRibbon.TabOrder:= 1;
     PanelNavBar.TabOrder := 2;
     DevEx_BandaValActivos.TabOrder :=3;
     PanelConsulta.TabOrder :=4;
end;

procedure TTressShell.CargaVista;
begin
     inherited;
     //Mostar Banda Valores activos
     DevEx_BandaValActivos.Visible := False;
     //Asignacion de TabOrders para el shell
     AsignacionTabOrder_DevEx;
     //Para avisar a los controles que hubo movimientos
     Application.ProcessMessages;
end;

procedure TTressShell.HabilitaControles;
begin
     inherited HabilitaControles;
     if not EmpresaAbierta then
     begin
          StatusBarMsg( '', K_PANEL_REPORTEADOR );
     end;
     DevEx_BarManager.GetItemByName('PProceso_btnImportar').Enabled := EmpresaAbierta;
     DevEx_BarManager.GetItemByName('PProceso_btnExportar').Enabled := EmpresaAbierta;

     DevEx_BarManager.GetItemByName('TabArchivo_btnConfigurarEmpresa').Enabled := EmpresaAbierta;
     DevEx_BarManager.GetItemByName('TabArchivo_btnExplorardorReportes').Enabled := EmpresaAbierta;
     DevEx_BarManager.GetItemByName('TabArchivo_btnSQL').Enabled := EmpresaAbierta;
     DevEx_BarManager.GetItemByName('TabArchivo_btnBitacora').Enabled := EmpresaAbierta;
     DevEx_BarManager.GetItemByName('TabArchivo_btnBitacora').Enabled := EmpresaAbierta;
end;

procedure TTressShell.DoCloseAll;
begin
     CierraFormaTodas;
     if EmpresaAbierta then
     begin
          dmDiccionario.CierraEmpresa;
          dmCliente.CierraEmpresa;
          CierraDatasets( dmConsultas );
          CierraDatasets( dmDiccionario );
          CierraDatasets( dmReportes );
          CierraDatasets( dmCatalogos );
          CierraDatasets( dmSistema );
     end;
     inherited DoCloseAll;
{$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll;
{$endif}
end;

Procedure TTressShell.LLenaOpcionesGrupos;
var
I:integer;
begin
  if(dmCliente.EmpresaAbierta) then
  begin
        for I:=0 to DevEx_NavBar.Groups.Count-1 do
        begin
              if (DevEx_NavBar.Groups[I].Caption = 'Consultas')  then
              begin
                    DevEx_NavBar.Groups[I].LargeImageIndex:=0;
                    DevEx_NavBar.Groups[I].SmallImageIndex:=0;
              end;
              if (DevEx_NavBar.Groups[I].Caption = 'Configuración de Catálogos')  then
              begin
                    DevEx_NavBar.Groups[I].LargeImageIndex:=1;
                    DevEx_NavBar.Groups[I].SmallImageIndex:=1;
              end;
              if (DevEx_NavBar.Groups[I].Caption = 'Sistema')  then
              begin
                    DevEx_NavBar.Groups[I].LargeImageIndex:=2;
                    DevEx_NavBar.Groups[I].SmallImageIndex:=2;
              end;
              if (I = 0) then
              begin
                    if(DevEx_NavBar.Groups.Count = 1) then
                    begin
                          // @DChavez : Cuando no tiene derecho se asigna los iconos que corresponden al navbar por el index fijo en la lista
                          if( DevEx_NavBar.Groups[0].Caption = 'No tiene derechos' )   then
                          begin
                                 DevEx_NavBar.Groups[0].LargeImageIndex := 3;
                                 DevEx_NavBar.Groups[0].SmallImageIndex := 3;
                          end
                          else

                          DevEx_NavBar.Groups[0].LargeImageIndex:=0;
                          DevEx_NavBar.Groups[0].SmallImageIndex:=0;
                    end
                    else
                    begin
                          if (DevEx_NavBar.Groups[0].Caption = 'Consultas')  then
                          begin
                                DevEx_NavBar.Groups[0].LargeImageIndex:=0;
                                DevEx_NavBar.Groups[0].SmallImageIndex:=0;
                          end;
                    end;
              end;
              DevEx_NavBar.Groups[I].UseSmallImages:=false;
        end;
  end
  else
  begin
        if(DevEx_NavBar.Groups.Count >0) then
        begin
              DevEx_NavBar.Groups[0].UseSmallImages:=false;
              DevEx_NavBar.Groups[0].LargeImageIndex:=0;
              DevEx_NavBar.Groups[0].SmallImageIndex:=0;
        end;
  end;
end;

procedure TTressShell.ClientAreaPG_DevExTcxPageControlPropertiesTcxPCCustomButtonsButtons0Click(
  Sender: TObject);
begin
  inherited;
  _V_Cerrar.Execute;
end;

procedure TTressShell._A_Bitacora_DevExExecute(Sender: TObject);
begin
  inherited;
  AbreFormaConsulta( efcSistBitacora );
end;

procedure TTressShell._A_ConfigurarEmpresaExecute(Sender: TObject);
begin
  inherited;
  AbreFormaConsulta( efcSistGlobales );
end;

procedure TTressShell._A_ExploradorReportesExecute(Sender: TObject);
begin
  inherited;
  AbreFormaConsulta( efcReportes );
end;

procedure TTressShell._A_ListaProcesosExecute(Sender: TObject);
begin
  inherited;
  AbreFormaConsulta( efcSistProcesos );
end;

procedure TTressShell._A_SQL_DevExExecute(Sender: TObject);
begin
  inherited;
  AbreFormaConsulta( efcQueryGral );
end;

procedure TTressShell._H_GlosarioExecute(Sender: TObject);
var
   sHelpFile: String;
begin
     if strLleno( FHelpGlosario ) then  // En el create poner FHelpGlosario indicando el Archivo de Ayuda y Sección al cual apuntar
        with Application do
        begin
             sHelpFile := HelpFile;
             try
                HelpFile := FHelpGlosario; //'Tress.hlp>Main';
                HelpContext ( HContenido_Glosario.HelpContext );
             finally
                HelpFile := sHelpFile;
             end;
        end;

end;

procedure TTressShell._P_ExportarExecute(Sender: TObject);
begin
  inherited;
  dmProcesos.ShowWizard(prRDDExportarDiccionario);
end;

procedure TTressShell._P_ImportarExecute(Sender: TObject);
begin
  inherited;
   dmProcesos.ShowWizard(prRDDImportarDiccionario);
end;

procedure TTressShell.WMWizardEnd(var Message: TMessage);
begin
     with Message do
     begin
          dmProcesos.HandleProcessEnd( WParam, LParam );
     end;
end;

procedure TTressShell.RevisaDerechos;
begin
     _P_Importar.Enabled := Revisa ( D_CRC_PROC_IMPORTAR );
     _P_Exportar.Enabled := Revisa ( D_CRC_PROC_EXPORTAR );

     _A_ExploradorReportes.Enabled := CheckDerecho(D_CONS_REPORTES , K_DERECHO_CONSULTA);
     _A_ListaProcesos.Enabled := CheckDerecho(GetPropConsulta(efcSistProcesos).IndexDerechos,K_DERECHO_CONSULTA);
     _A_SQL_DevEx.enabled := CheckDerecho(GetPropConsulta(efcQueryGral).IndexDerechos,K_DERECHO_CONSULTA);
     _A_Bitacora_DevEx.enabled := CheckDerecho(GetPropConsulta(efcSistBitacora).IndexDerechos,K_DERECHO_CONSULTA);
end;

end.
