unit FEditCampos_DevEx;

{$INCLUDE DEFINES.INC}
interface

uses
  Windows, Messages,  TypInfo, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, ComCtrls, ToolWin, ZetaKeyCombo,
  ZetaDBTextBox, Mask, ZetaNumero,
  ZetaCommonLists, DB, DBCtrls, ZetaSmartLists, ZetaEdit, ZetaKeyLookup,
  ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, cxControls, dxBarExtItems, dxBar, cxClasses, Vcl.ImgList,
  cxNavigator, cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx;

type
  eTipoFiltro = (tfLookup,tfListaFija);                 
  TEditCampos_DevEx = class(TBaseEdicion_DevEx)
    AT_TIPO: TZetaDBKeyCombo;
    AT_TCORTO: TDBEdit;
    AT_CLAVES: TDBEdit;
    lbCampo: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label5: TLabel;
    AT_CAMPO: TZetaDBEdit;
    Label7: TLabel;
    AT_MASCARA: TDBEdit;
    AT_ANCHO: TZetaDBNumero;
    AT_SISTEMA: TDBCheckBox;
    Label8: TLabel;
    AT_TOTAL: TZetaDBKeyCombo;
    Label9: TLabel;
    AT_VALORAC: TZetaDBKeyCombo;
    lbTexto: TLabel;
    AT_ENTIDAD: TZetaDBKeyLookup_DevEx;
    LV_CODIGO: TZetaDBKeyLookup_DevEx;
    Label11: TLabel;
    AT_TRANGO: TZetaDBKeyCombo;
    AT_DESCRIP: TDBMemo;
    AT_CONFI: TDBCheckBox;
    AT_TITULO: TDBEdit;
    Label10: TLabel;
    AT_FILTRO: TZetaDBKeyCombo;
    AT_ACTIVO: TDBCheckBox;
    AT_VERSION: TDBEdit;
    Label12: TLabel;
    US_CODIGO: TZetaDBTextBox;
    Label13: TLabel;
    procedure AT_TRANGOChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure ControlesTipoFiltro;
    function PuedeAgregar(var sMensaje: String): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    function PuedeModificar(var sMensaje: String): Boolean; override;
    function PuedeImprimir(var sMensaje: String): Boolean; override;

  public
    { Public declarations }
    procedure Connect;override;
  end;

var
  EditCampos_DevEx: TEditCampos_DevEx;

implementation

uses
    DDiccionario,
    ZetaCommonTools,
    ZetaTipoEntidad,
    ZetaTipoEntidadTools,
    ZGlobalTress,
    ZAccesosTress,
    ZetaCommonClasses;

{$R *.DFM}
procedure TEditCampos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= 0; //Pendiente
     FirstControl := AT_TITULO    ;
     LV_CODIGO.Left := AT_ENTIDAD.Left;
     AT_VALORAC.ListaFija := lfRDDValorActivo;
     {$ifdef  RDD_Desarrollo}
     AT_VERSION.ReadOnly := False;
     {$endif}
end;

procedure TEditCampos_DevEx.Connect;
begin
     inherited;
     with dmDiccionario do
     begin
          cdsEntidadesLookup.Conectar;
          cdsListasFijas.Conectar;
          Datasource.Dataset := cdsCamposPorTabla;
          AT_ENTIDAD.lookupDataset := cdsEntidadesLookup;
          LV_CODIGO.LookupDataset := cdsListasFijas;
     end;

     ControlesTipoFiltro;
end;

procedure TEditCampos_DevEx.AT_TRANGOChange(Sender: TObject);
begin
     ControlesTipoFiltro;
end;

procedure TEditCampos_DevEx.ControlesTipoFiltro;
begin
     AT_FILTRO.ListaFija := lfNinguna;
     case eTipoRango( AT_TRANGO.ItemIndex ) of
          rNinguno:
          begin
               if ( eTipoGlobal( AT_TIPO.ItemIndex ) = tgTexto ) then
                  AT_FILTRO.ListaFija := lfRDDDefaultsTexto
               else if ( eTipoGlobal( AT_TIPO.ItemIndex ) in [tgNumero,tgFloat] ) then
                    AT_FILTRO.ListaFija := lfRDDDefaultsFloat
               else
          end;
          rRangoEntidad, rRangoListas: AT_FILTRO.ListaFija := lfTipoRangoActivo;
          rFechas: AT_FILTRO.ListaFija := lfRangoFechas;
          rBool: AT_FILTRO.ListaFija := lfRDDDefaultsLogico;
     end;

     AT_ENTIDAD.Enabled := FALSE; //Pendienteee se quitan por alguna razon
     LV_CODIGO.Enabled := FALSE;    //Pendienteee se quitan por alguna razon

     lbTexto.Caption := '';

     case eTipoRango( AT_TRANGO.ItemIndex ) of
          rRangoEntidad:
          begin
               AT_ENTIDAD.Enabled := TRUE;
               lbTexto.Caption := 'Lookup a Tabla:'
          end;
          rRangoListas:
          begin
               LV_CODIGO.Enabled := TRUE;
               lbTexto.Caption := 'Lista Fija:'
          end;
     end;

     LV_CODIGO.Visible := LV_CODIGO.Enabled;     //Pendienteee se quitan por alguna razon
     AT_ENTIDAD.Visible := AT_ENTIDAD.Enabled;    //Pendienteee se quitan por alguna razon
end;


procedure TEditCampos_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( Field <> NIL ) then
        ControlesTipoFiltro;
end;

procedure TEditCampos_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     //Esta pantalla no tiene derecho de acceso por si sola.
     Datasource.AutoEdit := TRUE;

     if ( AT_TITULO.Visible and AT_TITULO.Enabled )then
        AT_TITULO.SelectAll;
end;

function TEditCampos_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := TRUE;
end;

function TEditCampos_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := TRUE;
end;

function TEditCampos_DevEx.PuedeImprimir(var sMensaje: String): Boolean;
begin
     Result := TRUE;
end;

function TEditCampos_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := TRUE;
end;

end.
