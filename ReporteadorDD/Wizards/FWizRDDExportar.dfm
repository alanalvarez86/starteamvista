inherited WizRDDExportar: TWizRDDExportar
  Left = 422
  Top = 279
  Caption = 'Exportar diccionario'
  ClientHeight = 401
  PixelsPerInch = 96
  TextHeight = 13
  inherited Wizard: TZetaWizard
    Top = 365
    BeforeMove = WizardBeforeMove
    inherited Anterior: TZetaWizardButton
      Enabled = False
    end
  end
  inherited PageControl: TPageControl
    Height = 365
    ActivePage = Parametros
    inherited Parametros: TTabSheet
      object ArchivoSeek: TSpeedButton
        Left = 364
        Top = 7
        Width = 25
        Height = 25
        Hint = 'Buscar Archivo de Importaci'#243'n'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333333333333333333FFF333333333333000333333333
          3333777FFF3FFFFF33330B000300000333337F777F777773F333000E00BFBFB0
          3333777F773333F7F333000E0BFBF0003333777F7F3337773F33000E0FBFBFBF
          0333777F7F3333FF7FFF000E0BFBF0000003777F7F3337777773000E0FBFBFBF
          BFB0777F7F33FFFFFFF7000E0BF000000003777F7FF777777773000000BFB033
          33337777773FF733333333333300033333333333337773333333333333333333
          3333333333333333333333333333333333333333333333333333333333333333
          3333333333333333333333333333333333333333333333333333}
        NumGlyphs = 2
        OnClick = ArchivoSeekClick
      end
      object Label5: TLabel
        Left = 28
        Top = 13
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Arc&hivo:'
        FocusControl = Archivo
      end
      object Archivo: TEdit
        Left = 72
        Top = 9
        Width = 289
        Height = 21
        TabOrder = 0
      end
      object GroupBox2: TGroupBox
        Left = 73
        Top = 36
        Width = 289
        Height = 137
        Caption = ' N'#250'mero de Versi'#243'n '
        TabOrder = 1
        object Label1: TLabel
          Left = 40
          Top = 58
          Width = 30
          Height = 13
          Caption = 'Inicial:'
        end
        object Label2: TLabel
          Left = 155
          Top = 58
          Width = 25
          Height = 13
          Caption = 'Final:'
        end
        object edtVersionFinal: TZetaEdit
          Left = 181
          Top = 54
          Width = 80
          Height = 21
          Enabled = False
          MaxLength = 9
          TabOrder = 3
        end
        object zedtListaVersion: TZetaEdit
          Left = 54
          Top = 95
          Width = 207
          Height = 21
          TabOrder = 5
        end
        object edtVersionInicial: TZetaEdit
          Left = 72
          Top = 54
          Width = 80
          Height = 21
          Enabled = False
          MaxLength = 9
          TabOrder = 2
        end
        object rdVersionLista: TRadioButton
          Left = 34
          Top = 77
          Width = 73
          Height = 17
          Caption = '&Lista'
          Checked = True
          TabOrder = 4
          TabStop = True
          OnClick = VersionsClick
        end
        object rdVersionRango: TRadioButton
          Left = 34
          Top = 37
          Width = 57
          Height = 17
          Caption = '&Rango'
          TabOrder = 1
          OnClick = VersionsClick
        end
        object rdVersionTodas: TRadioButton
          Left = 34
          Top = 21
          Width = 113
          Height = 17
          Caption = '&Todas'
          TabOrder = 0
          OnClick = VersionsClick
        end
      end
      object rdInformacionExportar: TRadioGroup
        Left = 72
        Top = 184
        Width = 289
        Height = 105
        Hint = 
          '"Los cambios oficiales son US_CODIGO=0.Los cambios del cliente s' +
          'on US_CODIGO<>0.Aplica para todas las tablas a exportar. '
        Caption = 'Informaci'#243'n a Exportar'
        ItemIndex = 0
        Items.Strings = (
          'Diccionario completo'
          'S'#243'lo cambios oficiales'
          'S'#243'lo cambios del cliente')
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
      end
    end
    inherited FiltrosCondiciones: TTabSheet
      inherited FiltrosGB: TGroupBox
        Height = 355
        inherited sFiltro: TMemo
          TabOrder = 2
        end
        inherited GBRango: TGroupBox
          TabOrder = 1
        end
        inherited BAgregaCampo: TBitBtn
          TabOrder = 3
        end
        inherited Seleccionar: TBitBtn
          TabOrder = 4
          Visible = True
        end
        inherited GroupBox1: TGroupBox
          TabOrder = 0
        end
      end
    end
    inherited Ejecucion: TTabSheet
      inherited Advertencia: TMemo
        Height = 185
        Lines.Strings = (
          ''
          
            'El proceso exportar'#225' el contenido del diccionario  creando un ar' +
            'chivo '
          'con formato XML , el contenido del '
          'archivo depende de los par'#225'metros seleccionados a lo largo del '
          'proceso.'
          ''
          'Presione el bot'#243'n '#39'Ejecutar'#39' para iniciar el proceso.'
          '')
      end
      inherited ProgressPanel: TPanel
        Top = 284
        Height = 71
      end
    end
  end
  object dlgExportar: TOpenDialog
    DefaultExt = 'XML'
    FileName = 'Diccion.xml'
    Filter = '(Archivos XML)|*.xml|Todos los archivos|*.*'
    Left = 380
    Top = 166
  end
end
