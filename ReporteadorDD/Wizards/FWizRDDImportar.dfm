inherited WizRDDImportar: TWizRDDImportar
  Left = 391
  Top = 221
  Caption = 'Importaci'#243'n de diccionario'
  ClientHeight = 353
  ClientWidth = 429
  PixelsPerInch = 96
  TextHeight = 13
  inherited Wizard: TZetaWizard
    Top = 317
    Width = 429
    BeforeMove = WizardBeforeMove
    DesignSize = (
      429
      36)
    inherited Salir: TZetaWizardButton
      Left = 338
    end
    inherited Ejecutar: TZetaWizardButton
      Left = 255
    end
  end
  inherited PageControl: TPageControl
    Width = 429
    Height = 317
    inherited Parametros: TTabSheet
      object ArchivoSeek: TSpeedButton
        Left = 368
        Top = 76
        Width = 25
        Height = 25
        Hint = 'Buscar Archivo de Importaci'#243'n'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333333333333333333FFF333333333333000333333333
          3333777FFF3FFFFF33330B000300000333337F777F777773F333000E00BFBFB0
          3333777F773333F7F333000E0BFBF0003333777F7F3337773F33000E0FBFBFBF
          0333777F7F3333FF7FFF000E0BFBF0000003777F7F3337777773000E0FBFBFBF
          BFB0777F7F33FFFFFFF7000E0BF000000003777F7FF777777773000000BFB033
          33337777773FF733333333333300033333333333337773333333333333333333
          3333333333333333333333333333333333333333333333333333333333333333
          3333333333333333333333333333333333333333333333333333}
        NumGlyphs = 2
        OnClick = ArchivoSeekClick
      end
      object Label5: TLabel
        Left = 33
        Top = 82
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Arc&hivo:'
        FocusControl = Archivo
      end
      object Archivo: TEdit
        Left = 76
        Top = 78
        Width = 289
        Height = 21
        TabOrder = 0
      end
      object rdgConflicto: TRadioGroup
        Left = 76
        Top = 126
        Width = 302
        Height = 105
        Caption = ' En caso de conflicto o duplicado, respetar '
        ItemIndex = 0
        Items.Strings = (
          'El diccionario actual ( Cambios realizados por el cliente )'
          'El archivo a Importar')
        TabOrder = 1
      end
      object lPrenderDerechos: TCheckBox
        Left = 78
        Top = 240
        Width = 193
        Height = 17
        Caption = 'Prender derechos de tablas nuevas'
        Checked = True
        State = cbChecked
        TabOrder = 2
      end
    end
    inherited Ejecucion: TTabSheet
      inherited Advertencia: TMemo
        Width = 421
        Height = 169
        Lines.Strings = (
          'Advertencia'
          
            'La informaci'#243'n de este archivo se agregar'#225' /encimar'#225'/borrar'#225' sob' +
            're '
          'la informaci'#243'n del dicccionario de datos, seg'#250'n sea el caso. '
          
            'El proceso al finalizar actualizar'#225' la versi'#243'n de diccionario de' +
            ' datos. '
          ''
          'Presione '#39'Ejecutar'#39' para iniciar el proceso')
      end
      inherited ProgressPanel: TPanel
        Top = 240
        Width = 421
      end
    end
  end
  object dlgImportar: TOpenDialog
    DefaultExt = 'XML'
    FileName = 'Diccion.xml'
    Filter = '(Archivos XML)|*.xml|Todos los archivos|*.*'
    Left = 380
    Top = 182
  end
end
