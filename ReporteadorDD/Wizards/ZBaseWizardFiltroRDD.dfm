inherited BaseWizardFiltroRDD: TBaseWizardFiltroRDD
  Left = 391
  Top = 221
  Caption = 'BaseWizardFiltroRDD'
  ClientHeight = 399
  ClientWidth = 429
  PixelsPerInch = 96
  TextHeight = 13
  inherited Wizard: TZetaWizard
    Top = 363
    Width = 429
    DesignSize = (
      429
      36)
    inherited Anterior: TZetaWizardButton
      Enabled = True
    end
    inherited Salir: TZetaWizardButton
      Left = 338
    end
    inherited Ejecutar: TZetaWizardButton
      Left = 255
    end
  end
  inherited PageControl: TPageControl
    Width = 429
    Height = 363
    ActivePage = FiltrosCondiciones
    object FiltrosCondiciones: TTabSheet [1]
      Caption = 'FiltrosCondiciones'
      TabVisible = False
      object FiltrosGB: TGroupBox
        Left = 0
        Top = 0
        Width = 421
        Height = 353
        Align = alClient
        Caption = ' Para Generar la lista de tablas '
        TabOrder = 0
        object sFiltroLBL: TLabel
          Left = 21
          Top = 230
          Width = 25
          Height = 13
          Alignment = taRightJustify
          Caption = '&Filtro:'
          FocusControl = sFiltro
        end
        object sFiltro: TMemo
          Left = 49
          Top = 230
          Width = 312
          Height = 83
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 1
        end
        object GBRango: TGroupBox
          Left = 49
          Top = 104
          Width = 312
          Height = 118
          Caption = ' Lista '
          TabOrder = 0
          object lbInicial: TLabel
            Left = 24
            Top = 52
            Width = 30
            Height = 13
            Alignment = taRightJustify
            Caption = 'Inicial:'
            Enabled = False
          end
          object lbFinal: TLabel
            Left = 171
            Top = 52
            Width = 25
            Height = 13
            Alignment = taRightJustify
            Caption = 'Final:'
            Enabled = False
          end
          object BFinal: TSpeedButton
            Left = 280
            Top = 46
            Width = 25
            Height = 25
            Hint = 'Invocar B'#250'squeda de Tablas'
            Enabled = False
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000010000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              33033333333333333F7F3333333333333000333333333333F777333333333333
              000333333333333F777333333333333000333333333333F77733333333333300
              033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
              33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
              3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
              33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
              333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
              333333773FF77333333333370007333333333333777333333333}
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            OnClick = BFinalClick
          end
          object BInicial: TSpeedButton
            Left = 138
            Top = 46
            Width = 25
            Height = 25
            Hint = 'Invocar B'#250'squeda de Tablas'
            Enabled = False
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000010000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              33033333333333333F7F3333333333333000333333333333F777333333333333
              000333333333333F777333333333333000333333333333F77733333333333300
              033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
              33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
              3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
              33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
              333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
              333333773FF77333333333370007333333333333777333333333}
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            OnClick = BInicialClick
          end
          object BLista: TSpeedButton
            Left = 280
            Top = 85
            Width = 25
            Height = 25
            Hint = 'Invocar B'#250'squeda de Tablas'
            Enabled = False
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000010000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              33033333333333333F7F3333333333333000333333333333F777333333333333
              000333333333333F777333333333333000333333333333F77733333333333300
              033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
              33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
              3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
              33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
              333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
              333333773FF77333333333370007333333333333777333333333}
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            OnClick = BListaClick
          end
          object RBTodos: TRadioButton
            Left = 18
            Top = 13
            Width = 113
            Height = 17
            Caption = '&Todas'
            Checked = True
            TabOrder = 0
            TabStop = True
            OnClick = RBTodosClick
          end
          object RBRango: TRadioButton
            Left = 18
            Top = 29
            Width = 57
            Height = 17
            Caption = '&Rango'
            TabOrder = 1
            OnClick = RBRangoClick
          end
          object RBLista: TRadioButton
            Left = 18
            Top = 69
            Width = 73
            Height = 17
            Caption = '&Lista'
            TabOrder = 4
            OnClick = RBListaClick
          end
          object EInicial: TZetaEdit
            Left = 56
            Top = 47
            Width = 80
            Height = 21
            Enabled = False
            LookUpBtn = BInicial
            MaxLength = 9
            TabOrder = 2
          end
          object EFinal: TZetaEdit
            Left = 198
            Top = 48
            Width = 80
            Height = 21
            Enabled = False
            LookUpBtn = BFinal
            MaxLength = 9
            TabOrder = 3
          end
          object ELista: TZetaEdit
            Left = 37
            Top = 87
            Width = 239
            Height = 21
            Enabled = False
            LookUpBtn = BLista
            TabOrder = 5
          end
        end
        object BAgregaCampo: TBitBtn
          Left = 364
          Top = 230
          Width = 25
          Height = 25
          Hint = 'Invocar al Constructor de F'#243'rmulas'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BAgregaCampoClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
            333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
            0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
            07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
            07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
            0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
            33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
            B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
            3BB33773333773333773B333333B3333333B7333333733333337}
          NumGlyphs = 2
        end
        object Seleccionar: TBitBtn
          Left = 129
          Top = 320
          Width = 169
          Height = 25
          Hint = 'Invocar Pantalla de Selecci'#243'n de Tablas'
          Caption = '&Verificar Lista de Tablas'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          Visible = False
          OnClick = SeleccionarClick
          Glyph.Data = {
            42010000424D4201000000000000760000002800000011000000110000000100
            040000000000CC00000000000000000000001000000010000000000000000000
            BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            77777000000070000000000007777000000070FFFFFFFFFF07777000000070FC
            CFCCCCCF07777000000070FFFFFFFFFF07777000000070FCCFCCCCCF07777000
            000070FFFFFFFFFF07777000000070FFFFFFF0FF07777000000070F00FFF0B0F
            07770000000070F0F0F0B0F000700000000070FF0B0B0F0FBF00000000007000
            00F0F0FBFBF0000000007777770B0FBFBFB00000000077777770FBFBFB000000
            0000777777770000007000000000777777777777777770000000777777777777
            777770000000}
        end
        object GroupBox1: TGroupBox
          Left = 48
          Top = 24
          Width = 313
          Height = 81
          Caption = ' Tipo '
          TabOrder = 4
          object chkTipoSistema: TCheckBox
            Left = 24
            Top = 16
            Width = 185
            Height = 17
            Caption = 'De sistema (#Tabla < 9000 )'
            TabOrder = 0
          end
          object chkTipoReporteador: TCheckBox
            Left = 24
            Top = 32
            Width = 201
            Height = 17
            Caption = 'De reporteador (#Tabla = 9000'#39's )'
            TabOrder = 1
          end
          object chkTipoCliente: TCheckBox
            Left = 24
            Top = 48
            Width = 193
            Height = 17
            Caption = 'Del cliente ( #Tabla > 10000 )'
            TabOrder = 2
          end
        end
      end
    end
    inherited Ejecucion: TTabSheet
      inherited Advertencia: TMemo
        Width = 421
        Height = 169
      end
      inherited ProgressPanel: TPanel
        Top = 286
        Width = 421
      end
    end
  end
end
