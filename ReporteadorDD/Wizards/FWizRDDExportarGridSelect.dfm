inherited WizRDDExportarGridSelect: TWizRDDExportarGridSelect
  Left = 199
  Top = 289
  Width = 714
  Caption = 'Tablas'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 706
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Top = 35
    Width = 706
    Height = 129
    Columns = <
      item
        Expanded = False
        FieldName = 'EN_CODIGO'
        Title.Caption = 'C'#243'digo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EN_TITULO'
        Title.Caption = 'T'#237'tulo'
        Width = 129
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EN_TABLA'
        Title.Caption = 'Tabla'
        Width = 111
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EN_VERSION'
        Title.Caption = 'Versi'#243'n'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EN_ACTIVO'
        Title.Caption = 'Activa'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EN_DESCRIP'
        Title.Caption = 'Descripci'#243'n'
        Width = 238
        Visible = True
      end>
  end
  object PanelSuperior: TPanel [2]
    Left = 0
    Top = 0
    Width = 706
    Height = 35
    Align = alTop
    TabOrder = 2
    object PistaLBL: TLabel
      Left = 26
      Top = 11
      Width = 63
      Height = 13
      Alignment = taRightJustify
      Caption = '&Busca Tabla:'
      FocusControl = Pista
    end
    object Pista: TEdit
      Left = 92
      Top = 7
      Width = 173
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
      OnChange = PistaChange
      OnKeyDown = PistaKeyDown
    end
    object BtnFiltrar: TBitBtn
      Left = 274
      Top = 5
      Width = 75
      Height = 25
      Hint = 'Filtrar Registros Por Esta Descripci'#243'n'
      Caption = '&Filtrar'
      Enabled = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtnFiltrarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33033333333333333F7F3333333333333000333333333333F777333333333333
        000333333333333F777333333333333000333333333333F77733333333333300
        033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
        33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
        3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
        33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
        333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
        333333773FF77333333333370007333333333333777333333333}
      NumGlyphs = 2
    end
  end
  inherited DataSource: TDataSource
    Left = 152
    Top = 64
  end
end
