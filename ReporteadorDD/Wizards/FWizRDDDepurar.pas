unit FWizRDDDepurar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseWizardFiltroRDD, ComCtrls, ZetaEdit, StdCtrls, Buttons,
  ZetaWizard, ExtCtrls, ZetaKeyCombo, jpeg;

type
  TWizRDDDepurar = class(TBaseWizardFiltroRDD)
    GroupBox1: TGroupBox;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    TabSheet1: TTabSheet;
    GroupBox2: TGroupBox;
    CheckBox4: TCheckBox;
    CheckBox5: TCheckBox;
    CheckBox6: TCheckBox;
    CheckBox7: TCheckBox;
    CheckBox8: TCheckBox;
    TabSheet2: TTabSheet;
    GroupBox4: TGroupBox;
    CheckBox10: TCheckBox;
    CheckBox11: TCheckBox;
    CheckBox12: TCheckBox;
    CheckBox13: TCheckBox;
    CheckBox14: TCheckBox;
    RadioGroup1: TRadioGroup;
    TabSheet3: TTabSheet;
    Image1: TImage;
    Image2: TImage;
    GroupBox5: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    ZetaEdit1: TZetaEdit;
    ZetaEdit2: TZetaEdit;
    ZetaEdit3: TZetaEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  WizRDDDepurar: TWizRDDDepurar;

implementation

{$R *.dfm}

end.
