unit FWizRDDExportarGridSelect_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBasicoSelectGrid_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, cxControls, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, Vcl.Buttons,
  Vcl.StdCtrls, Vcl.ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid,
  cxButtons, Vcl.ExtCtrls, Grids, DBGrids, ZetaDBGrid,
  ZetaClientDataSet;

type
  TWizRDDExportarGridSelect_DevEx = class(TBasicoGridSelect_DevEx)
    PanelSuperior: TPanel;
    PistaLBL: TLabel;
    Pista: TEdit;
    BtnFiltrar: TcxButton;
    EN_CODIGO: TcxGridDBColumn;
    EN_TITULO: TcxGridDBColumn;
    EN_TABLA: TcxGridDBColumn;
    EN_VERSION: TcxGridDBColumn;
    EN_ACTIVO: TcxGridDBColumn;
    EN_DESCRIP: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
    procedure PistaChange(Sender: TObject);
    procedure PistaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure BtnFiltrarClick(Sender: TObject);
  private
    { Private declarations }
    function Llave: String;
    function GetFiltroDataset: String;
    function GetFiltroPista: String;
    procedure SetFilter;
  protected
    procedure KeyPress( var Key: Char ); override; { TWinControl }
  public
    { Public declarations }
  end;

var
  WizRDDExportarGridSelect_DevEx: TWizRDDExportarGridSelect_DevEx;

function GridSelect( ZetaDataset: TZetaClientDataset; GridSelectClass: TBasicoGridSelectClass;
                     ValidarCambios: Boolean = TRUE ): Boolean;

implementation

uses ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo;

{$R *.DFM}

function GridSelect( ZetaDataset: TZetaClientDataset; GridSelectClass: TBasicoGridSelectClass;
                     ValidarCambios: Boolean = TRUE ): Boolean;
begin
     Result := ZBasicoSelectGrid_DevEx.GridSelectBasico( ZetaDataset, GridSelectClass, ValidarCambios );
end;

procedure TWizRDDExportarGridSelect_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     Pista.Clear;
     SetFilter;
end;

function TWizRDDExportarGridSelect_DevEx.Llave: String;
begin
     Result := Pista.Text;
end;

function TWizRDDExportarGridSelect_DevEx.GetFiltroDataset: String;
begin
     if ZetaCommonTools.strVacio( Filtro ) then
        Result := ZetaCommonClasses.VACIO
     else
         Result := ZetaCommonTools.Parentesis( Filtro );
end;

function TWizRDDExportarGridSelect_DevEx.GetFiltroPista: String;
var
   iTabla : Integer;
begin
     Result := ZetaCommonClasses.VACIO;
     if ZetaCommonTools.strLleno( Llave ) then
     begin
          iTabla := StrToIntDef( Llave, 0 );
          if ( iTabla > 0 ) then
          begin
               if Assigned( DataSet.FindField( 'EN_CODIGO' ) ) then
                  Result := Format( '( EN_CODIGO = %d )', [ iTabla ] )
               else
                   ZetaDialogo.ZError( self.Caption, 'No Se Puede Filtrar Por C�digo de Tabla - DataSet Inv�lido', 0 );
          end
          else
          begin
               if Assigned( DataSet.FindField( 'EN_TABLA' ) ) then
                  Result := Format( '( Upper( EN_TABLA ) like %s )', [ EntreComillas( '%' + Llave + '%' ) ] )
               else
                   ZetaDialogo.ZError( self.Caption, 'No Se Puede Filtrar Por Nombre de Tabla - DataSet Inv�lido', 0 );
          end;
     end;
end;

procedure TWizRDDExportarGridSelect_DevEx.SetFilter;
var
   Pos : TBookMark;
begin
     with Dataset do
     begin
          DisableControls;
          try
             if strLleno( Filtro ) or strLleno( Llave ) then
             begin
                  Pos:= GetBookMark;
                  ZetaDBGridDBTableview.Controller.ClearSelection;  // Se hacen invalidos los bookmarks cuando se filtra el dataset
                  Filtered := False;
                  Filter := ZetaCommonTools.ConcatFiltros( GetFiltroDataset, GetFiltroPista );
                  Filtered := True;
                  if ( Pos <> nil ) then
                  begin
                       if BookMarkValid( Pos ) then
                          GotoBookMark( Pos );
                       FreeBookMark( Pos );
                  end;
             end
             else if Filtered then
             begin
                  Pos:= GetBookMark;
                  Filtered := False;
                  Filter := ZetaCommonClasses.VACIO;
                  if ( Pos <> nil ) then
                  begin
                       if BookMarkValid( Pos ) then
                          GotoBookMark( Pos );
                       FreeBookMark( Pos );
                  end;
             end;
          finally
                 EnableControls;
          end;
     end;
end;

procedure TWizRDDExportarGridSelect_DevEx.BtnFiltrarClick(Sender: TObject);
begin
     inherited;
     SetFilter;
end;

procedure TWizRDDExportarGridSelect_DevEx.PistaChange(Sender: TObject);
begin
     inherited;
     if ZetaCommonTools.StrVacio( Llave ) then
     begin
          SetFilter;
          BtnFiltrar.Enabled := False;
     end
     else
          BtnFiltrar.Enabled := True;
end;

procedure TWizRDDExportarGridSelect_DevEx.PistaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     inherited;
     with ZetaDBGrid do
     begin
          case Key of
               VK_PRIOR: Perform( WM_KEYDOWN, VK_PRIOR, 0 );   // PgUp //
               VK_NEXT: Perform( WM_KEYDOWN, VK_NEXT, 0 );   // PgDn //
               VK_UP: Perform( WM_KEYDOWN, VK_UP, 0 );   //Up Arrow //
               VK_DOWN: Perform( WM_KEYDOWN, VK_DOWN, 0 );   //Down Arrow //
          end;
     end;
end;

procedure TWizRDDExportarGridSelect_DevEx.KeyPress(var Key: Char);
begin
     if ( ActiveControl = Pista ) then
     begin
          if ( Key = Chr( VK_RETURN ) ) then
          begin
               Key := #0;
               if self.BtnFiltrar.Enabled then
                  self.BtnFiltrar.Click
               else
                   ZetaDBGrid.SetFocus;
          end;
     end
     else
         inherited KeyPress( Key );
end;

end.
