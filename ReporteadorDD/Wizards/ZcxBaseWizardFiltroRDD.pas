unit ZcxBaseWizardFiltroRDD;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ComCtrls, ExtCtrls, DbGrids,
     ZetaCommonLists, ZcxBaseWizard,
     ZetaWizard,
     ZetaKeyLookup,
     ZetaEdit, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, ZetaCXWizard, dxGDIPlusClasses, cxImage, cxLabel,
  cxGroupBox, dxCustomWizardControl, dxWizardControl, Vcl.Menus, cxButtons;

type
  TBaseWizardFiltroRDD = class(TcxBaseWizard)
    FiltrosCondiciones: TdxWizardControlPage;
    GroupBox1: TGroupBox;
    chkTipoSistema: TCheckBox;
    chkTipoReporteador: TCheckBox;
    chkTipoCliente: TCheckBox;
    GBRango: TGroupBox;
    lbInicial: TLabel;
    lbFinal: TLabel;
    BFinal: TcxButton;
    BInicial: TcxButton;
    BLista: TcxButton;
    RBTodos: TRadioButton;
    RBRango: TRadioButton;
    RBLista: TRadioButton;
    EInicial: TZetaEdit;
    EFinal: TZetaEdit;
    ELista: TZetaEdit;
    sFiltroLBL: TLabel;
    sFiltro: TMemo;
    Seleccionar: TcxButton;
    BAgregaCampo: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RBTodosClick(Sender: TObject);
    procedure RBRangoClick(Sender: TObject);
    procedure RBListaClick(Sender: TObject);
    procedure BInicialClick(Sender: TObject);
    procedure BFinalClick(Sender: TObject);
    procedure BListaClick(Sender: TObject);
    procedure BAgregaCampoClick(Sender: TObject);
    procedure SeleccionarClick(Sender: TObject);
    procedure chkTipoSistemaClick(Sender: TObject);
  private
    { Private declarations }
    FTipoRango: eTipoRangoActivo;
    FTablaCodigo: String;
    FTablaFiltro: String;
    FVerificacion: Boolean;
    procedure SetVerificacion( const Value: Boolean );
  protected
    { Protected declarations }
    property TablaCodigo: String read FTablaCodigo write FTablaCodigo;
    property TablaFiltro: String read FTablaFiltro write FTablaFiltro;
    property Verificacion: Boolean read FVerificacion write FVerificacion;
    function Verificar: Boolean; virtual; abstract;
    function GetFiltro: String;
    function GetRango: String;
    procedure CargaListaVerificacion; virtual; abstract;
    procedure CargaParametros; override;
    procedure EnabledBotones( const eTipo: eTipoRangoActivo );
  public
    { Public declarations }
    property TipoRango: eTipoRangoActivo read FTipoRango;
  end;

var
  BaseWizardFiltroRDD: TBaseWizardFiltroRDD;

implementation

uses ZetaClientTools,
     ZetaTipoEntidad,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZConstruyeFormula,
     ZConfirmaVerificacion_DevEx,
     DCliente,
     DCatalogos,
     DDiccionario,
     FTressShell;

{$R *.DFM}

{ ************ TBaseWizardFiltro ************ }

procedure TBaseWizardFiltroRDD.FormCreate(Sender: TObject);
begin
     inherited;
     FTablaCodigo := ARROBA_TABLA + '.EN_CODIGO';
     FTablaFiltro := '';
end;

procedure TBaseWizardFiltroRDD.FormShow(Sender: TObject);
begin
     inherited;
     FVerificacion := False;
     sFiltro.Text := VACIO;
     RBTodos.Checked := True;
     chkTipoSistema.Checked := True;
     chkTipoReporteador.Checked := True;
     chkTipoCliente.Checked := True;
end;

procedure TBaseWizardFiltroRDD.SetVerificacion( const Value: Boolean );
begin
     FVerificacion := FVerificacion or Value;
end;

procedure TBaseWizardFiltroRDD.EnabledBotones( const eTipo: eTipoRangoActivo );
var
   lEnabled: Boolean;
begin
     FTipoRango := eTipo;
     lEnabled := ( eTipo = raRango );
     lbInicial.Enabled := lEnabled;
     lbFinal.Enabled := lEnabled;
     EInicial.Enabled := lEnabled;
     EFinal.Enabled := lEnabled;
     bInicial.Enabled := lEnabled;
     bFinal.Enabled := lEnabled;
     lEnabled := ( eTipo = raLista );
     ELista.Enabled := lEnabled;
     bLista.Enabled := lEnabled;
end;

function TBaseWizardFiltroRDD.GetRango: String;
begin
     case TipoRango of
          raRango: Result := GetFiltroRango( FTablaCodigo, Trim( EInicial.Text ), Trim( EFinal.Text ) );
          raLista: Result := GetFiltroLista( FTablaCodigo, Trim( ELista.Text ) );
     else
         Result := '';
     end;
end;



function TBaseWizardFiltroRDD.GetFiltro: String;
begin
     Result := Trim( sFiltro.Text );
     { Los Paréntesis no son necesarios
     if StrLleno( Result ) then
        Result := Parentesis( Result );
     }
end;

procedure TBaseWizardFiltroRDD.CargaParametros;
begin
     with ParameterList do
     begin
          AddString( 'RangoLista', GetRango );
          AddString( 'Condicion' ,VACIO);
          AddString( 'Filtro', GetFiltro );
          AddBoolean( 'TipoSistema', chkTipoSistema.Checked );
          AddBoolean( 'TipoReporteador' ,chkTipoReporteador.Checked );
          AddBoolean( 'TipoCliente' ,chkTipoCliente.Checked );
     end;

     with Descripciones do
     begin
          {AddString( 'Lista', GetRango );
          AddString( 'Filtro', GetFiltro );
          AddBoolean( 'TipoSistema',chkTipoSistema.Checked );
          AddBoolean( 'TipoReporteador',chkTipoReporteador.Checked );
          AddBoolean( 'TipoCliente',chkTipoCliente.Checked );   }
     end;

     with dmCliente do
     begin
          //CargaActivosSistema( ParameterList );
     end;
end;



procedure TBaseWizardFiltroRDD.chkTipoSistemaClick(Sender: TObject);
begin
end;

{ ********** Eventos ******** }

procedure TBaseWizardFiltroRDD.RBTodosClick(Sender: TObject);
begin
     EnabledBotones( raTodos );
end;

procedure TBaseWizardFiltroRDD.RBRangoClick(Sender: TObject);
begin
     EnabledBotones( raRango );
end;

procedure TBaseWizardFiltroRDD.RBListaClick(Sender: TObject);
begin
     EnabledBotones( raLista );
end;

procedure TBaseWizardFiltroRDD.BInicialClick(Sender: TObject);
var
   sLlave,sDescripcion :string;
begin
     sLlave:= VACIO;
     TressShell.BuscaDialogo( TipoEntidad( enRDDEntidad ), '', sLLave, sDescripcion );

     with EInicial do
     begin
          Text := sLlave;
     end;
end;

procedure TBaseWizardFiltroRDD.BFinalClick(Sender: TObject);
var
   sLlave,sDescripcion :string;
begin
     sLlave:= VACIO;
     TressShell.BuscaDialogo( TipoEntidad(enRDDEntidad  ), '', sLLave, sDescripcion );

     with EFinal do
     begin
          Text := sLlave;
     end;
end;

procedure TBaseWizardFiltroRDD.BListaClick(Sender: TObject);
var
   sLlave,sDescripcion :string;
begin
     sLlave:= VACIO;
     TressShell.BuscaDialogo( TipoEntidad( enRDDEntidad ), '', sLLave, sDescripcion );

     with ELista do
     begin
          if StrLleno(Text)then
             Text := Text +','+ sLlave
          else
              Text := sLlave;
     end;
end;

procedure TBaseWizardFiltroRDD.BAgregaCampoClick(Sender: TObject);
begin
     with sFiltro do
     begin
          Text:= ZConstruyeFormula.GetFormulaConst( enRDDEntidad, Text, SelStart, evBase );
     end;
end;

procedure TBaseWizardFiltroRDD.SeleccionarClick(Sender: TObject);
var
   lCarga: Boolean;
   lOk: Boolean;
   oCursor: TCursor;
begin
     inherited;
     if Verificacion then
     begin
          case ZConfirmaVerificacion_DevEx.ConfirmVerification of
               mrYes:
               begin
                    lCarga := True;
                    lOk := True;
               end;
               mrNo:
               begin
                    lCarga := False;
                    lOk := True;
               end;
          else
              begin
                   lCarga := False;
                   lOk := False;
              end;
          end;
     end
     else
     begin
          lCarga := True;
          lOk := True;
     end;
     if lOk then
     begin
          if lCarga then
          begin
               oCursor := Screen.Cursor;
               Screen.Cursor := crHourglass;
               try
                  CargaParametros;
                  CargaListaVerificacion;
               finally
                      Screen.Cursor := oCursor;
               end;
          end;
          SetVerificacion( Verificar );
     end;
end;

end.



