unit FSistEditEmpresas_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FSistBaseEditEmpresas_DevEx, DB, StdCtrls,
  ZetaEdit, Mask, DBCtrls, ExtCtrls, ZetaSmartLists, Buttons, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters, cxControls, cxContainer, cxEdit,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, ImgList,
  cxGroupBox, cxNavigator, cxDBNavigator, cxButtons;

type
  TSistEditEmpresas_DevEx = class(TSistBaseEditEmpresas_DevEx)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SistEditEmpresas_DevEx: TSistEditEmpresas_DevEx;

implementation

{$R *.DFM}

end.
