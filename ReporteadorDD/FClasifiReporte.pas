unit FClasifiReporte;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, DB, ExtCtrls, Grids, DBGrids, ZetaDBGrid, Buttons;

type
  TClasifiReporte = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    PanelMover: TPanel;
    Subir: TSpeedButton;
    Bajar: TSpeedButton;
    procedure SubirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
           procedure Connect; override;
           procedure Refresh; override;
           procedure Agregar;override;
           procedure Borrar; override;
           procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  ClasifiReporte: TClasifiReporte;

implementation

uses
    ZetaCommonClasses,
    DCliente,
    dDiccionario;
{$R *.dfm}

{ TClasifiReporte }

procedure TClasifiReporte.Agregar;
begin
     inherited;
     dmDiccionario.cdsClasificaciones.Agregar;
end;

procedure TClasifiReporte.Borrar;
begin
     inherited;
     dmDiccionario.cdsClasificaciones.Borrar;
end;

procedure TClasifiReporte.Connect;
begin
     inherited;
     with dmDiccionario do
     begin
          cdsClasificaciones.Conectar;
          DataSource.DataSet:= cdsClasificaciones;
     end;
end;

procedure TClasifiReporte.Modificar;
begin
     inherited;
     dmDiccionario.cdsClasificaciones.Modificar;

end;

procedure TClasifiReporte.Refresh;
begin
     inherited;
     dmDiccionario.cdsClasificaciones.Refrescar;
end;

procedure TClasifiReporte.SubirClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        dmDiccionario.ClasifiMover(TControl(Sender).Tag);
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TClasifiReporte.FormCreate(Sender: TObject);
begin
     inherited;
     Subir.Visible := dmCliente.GetGrupoActivo = D_GRUPO_SIN_RESTRICCION;
     Bajar.Visible := dmCliente.GetGrupoActivo = D_GRUPO_SIN_RESTRICCION;
end;

end.
