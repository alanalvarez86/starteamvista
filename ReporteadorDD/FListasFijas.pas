unit FListasFijas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, DB, ExtCtrls, Grids, DBGrids, ZetaDBGrid, Buttons;

type
  TListasFijas = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
  private
    { Private declarations }
  protected
           procedure Connect; override;
           procedure Refresh; override;
           procedure Agregar;override;
           procedure Borrar; override;
           procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  ListasFijas: TListasFijas;

implementation

uses
    dDiccionario;
{$R *.dfm}

{ TClasifiReporte }

procedure TListasFijas.Agregar;
begin
     inherited;
     dmDiccionario.cdsListasFijas.Agregar;
end;

procedure TListasFijas.Borrar;
begin
     inherited;
     dmDiccionario.cdsListasFijas.Borrar;
end;

procedure TListasFijas.Connect;
begin
     inherited;
     with dmDiccionario do
     begin
          cdsListasFijas.Conectar;
          DataSource.DataSet:= cdsListasFijas;
     end;
end;

procedure TListasFijas.Modificar;
begin
     inherited;
     dmDiccionario.cdsListasFijas.Modificar;

end;

procedure TListasFijas.Refresh;
begin
     inherited;
     dmDiccionario.cdsListasFijas.Refrescar;
end;

end.
