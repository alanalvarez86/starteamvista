unit DProcesos;

interface
{$INCLUDE DEFINES.INC}
uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,Variants,
     ZetaClientDataSet,
     ZBaseThreads,
     ZetaTipoEntidad,
     {$ifdef RDD}
     ReporteadorDD_TLB,
     {$else}
     Diccionario_TLB,
     {$endif}
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaCommonClasses;

type

{$ifdef RDD}
    IdmServerDiccionarioDisp = IdmServerReporteadorDDDisp;
  {$endif}

  TReporteadorThread = class( TBaseThread )
  private
    { Private declarations }
    function GetServidor: IdmServerDiccionarioDisp;
    property Servidor: IdmServerDiccionarioDisp read GetServidor;

  protected
    { Protected declarations }
    function Procesar: OleVariant; override;
  end;

  TdmProcesos = class(TDataModule)
    cdsDataSet: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FParametros: TZetaParams;
    FListaProcesos: TProcessList;
    FServidor: IdmServerDiccionarioDisp;
    function GetServerDiccionario: IdmServerDiccionarioDisp;
    function CorreThreadReporteador(const eProceso: Procesos; const oLista: OleVariant; oParametros: TZetaParams): Boolean;
    function GetLista(ZetaDataset: TZetaClientDataset): OleVariant;
  public
    { Public declarations }
     procedure HandleProcessEnd(const iIndice, iValor: Integer);
     procedure ShowWizard(const eProceso: Procesos);
     {Exporta Diccionario }
     procedure TablasGetLista (Parametros: TZetaParams);
     function ExportarDiccionario(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;

     {Importar Diccionario}
     function ImportarDiccionario(Parametros: TZetaParams): Boolean;

  protected
    property ServerDiccionario: IdmServerDiccionarioDisp read GetServerDiccionario;
  end;

var
  dmProcesos: TdmProcesos;

implementation
uses
     ZetaWizardFeedBack_DevEx,
     ZGlobalTress,
     ZcxWizardBasico,
     ZetaDialogo,
     FWizRDDExportar_DevEx,
     FWizRDDimportar_DevEx,
     FTressShell,
     DCliente,
     DConsultas,
     DXMLTools;




{$R *.DFM}


procedure TdmProcesos.ShowWizard( const eProceso: Procesos );
var
   WizardCXClass: TcxWizardBasicoClass;
begin
     cdsDataSet.Init;
     WizardCXClass := nil;
     case eProceso of
          prRDDExportarDiccionario : WizardCXClass := TWizRDDExportar_DevEx;
          prRDDImportarDiccionario : WizardCXClass := TWizRDDimportar_DevEx;

     end;
     if ( WizardCXClass = nil ) then { Este IF debe ser Temporal }
        ZetaDialogo.zInformation( 'Estamos Trabajando...', 'Esta Opci�n No Ha Sido Implementada', 0 )
     else
         ZcxWizardBasico.ShowWizard( WizardCXClass );
     TressShell.ReconectaMenu;
     Application.ProcessMessages;
end;

procedure SetDataChange( const Proceso: Procesos );
begin
     case Proceso of
          prRDDExportarDiccionario : TressShell.SetDataChange( [  ] );
          prRDDImportarDiccionario : TressShell.SetDataChange( [ enRDDEntidad,enRDDClasificaciones,enRDDModulos,enRDDListasFijas ] );

     end;
end;

{TReporteadorThread}

function TReporteadorThread.GetServidor: IdmServerDiccionarioDisp;
begin
     Result := IdmServerDiccionarioDisp(CreateServer( {$ifdef RDD}CLASS_dmServerReporteadorDD{$else}CLASS_dmServerDiccionario{$endif} ) );
end;


function TReporteadorThread.Procesar: OleVariant;
 var XMLFile: WideString;
begin
     case Proceso of
          prRDDExportarDiccionario :
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.ExportarDiccionario( Empresa, Parametros.VarValues, XMLFile )
               else
                   Result := Servidor.ExportarDiccionarioLista( Empresa, Employees, Parametros.VarValues, XMLFile );

               if StrLleno( XMLFile) then
               begin
                    with TdmXMLTools.Create(NIL) do
                    begin
                         XMLAsText := XMLFile;
                         XMLDocument.SaveToFile( Parametros.ParamByName('Archivo').AsString );
                         Free;
                    end;
               end;
          end;

          prRDDImportarDiccionario : Result := Servidor.ImportarDiccionario( Empresa, Parametros.VarValues );
     end;
end;

{TdmProcesos}
function TdmProcesos.CorreThreadReporteador(const eProceso: Procesos;  const oLista: OleVariant; oParametros: TZetaParams): Boolean;
begin
      with TReporteadorThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          Employees := oLista;
          CargaParametros( oParametros );
          Resume;
     end;
     Result := True;
end;

function TdmProcesos.GetServerDiccionario: IdmServerDiccionarioDisp;
begin
     Result := IdmServerDiccionarioDisp(dmCliente.CreaServidor( {$ifdef RDD}CLASS_dmServerReporteadorDD{$else}CLASS_dmServerDiccionario{$endif}, FServidor ));
end;

procedure TdmProcesos.HandleProcessEnd(const iIndice, iValor: Integer);
begin
     with TWizardFeedback_DevEx.Create( Application ) do
     begin
          try
             with FListaProcesos do
             begin
                  try
                     with LockList do
                     begin
                          if ( iIndice >= 0 ) and ( iIndice < Count ) then
                             ProcessData.Assign( TProcessInfo( Items[ iIndice ] ) )
                          else
                              raise Exception.Create( 'Indice De Lista De Procesos Fuera De Rango ( ' + IntToStr( iIndice ) + ' )' );
                     end;
                  finally
                         UnlockList;
                  end;
             end;
             if ProcessOK then
                SetMensajes( '' );
             dmConsultas.LeerUnProceso( ShowProcessInfo( True ) );
             if ProcessOK then
                SetDataChange( ProcessData.Proceso );
          finally
                 Free;
          end;
     end;
     TressShell.ReconectaMenu;
     Application.ProcessMessages;
end;

function TdmProcesos.GetLista(ZetaDataset: TZetaClientDataset): OleVariant;
begin
     with ZetaDataset do
     begin
          MergeChangeLog;  // Elimina Registros Borrados //
          Result := Data;
     end;
end;

procedure TdmProcesos.TablasGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerDiccionario.DiccionarioListaTablasGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;


function TdmProcesos.ExportarDiccionario(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadReporteador( prRDDExportarDiccionario, GetLista( cdsDataset ), Parametros )
     else
        Result := CorreThreadReporteador( prRDDExportarDiccionario, NULL, Parametros );
end;


function TdmProcesos.ImportarDiccionario(Parametros: TZetaParams): Boolean;
begin
      Result := CorreThreadReporteador( prRDDImportarDiccionario, NULL, Parametros );
end;

procedure TdmProcesos.DataModuleCreate(Sender: TObject);
begin
     FListaProcesos := TProcessList.Create;
     cdsDataSet := TZetaClientDataSet.Create(Self);
end;

procedure TdmProcesos.DataModuleDestroy(Sender: TObject);
begin
     FParametros.Free;
     FListaProcesos.Free;
end;

end.
