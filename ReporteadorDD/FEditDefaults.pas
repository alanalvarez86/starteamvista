unit FEditDefaults;
{$INCLUDE DEFINES.INC}
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, Db, Buttons,
  DBCtrls, StdCtrls, ExtCtrls, Mask, ComCtrls, ZetaClientDataset,
  ZetaNumero, ZetaKeyLookup, ZetaEdit, ZetaSmartLists, ZetaDBTextBox;


type
  TEditDefaults = class(TBaseEdicion)
    Label3: TLabel;
    Label1: TLabel;
    RD_ENTIDAD: TZetaDBKeyLookup;
    AT_CAMPO: TDBEdit;
    RD_VERSION: TDBEdit;
    Label12: TLabel;
    Label4: TLabel;
    US_CODIGO: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FCampoAtributo: string;
    FCampoEntidad: string;
    FCampoVersion :string;
    FDataSet: TZetaClientDataset;
  protected
    procedure Connect; override;
    procedure DisConnect;override;
    function PuedeAgregar(var sMensaje: String): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    function PuedeModificar(var sMensaje: String): Boolean; override;
    function PuedeImprimir(var sMensaje: String): Boolean; override;

  public
   property Dataset: TZetaClientDataset read FDataSet write FDataset;
   property CampoEntidad: string read FCampoEntidad write FCampoEntidad;
   property CampoAtributo: string read FCampoAtributo write FCampoAtributo;
   property CampoVersion: string read FCampoVersion write FCampoVersion;

  end;

var
  EditDefaults: TEditDefaults;

implementation

uses dDiccionario;

{$R *.DFM}

procedure TEditDefaults.FormCreate(Sender: TObject);
begin
     inherited;
     //IndexDerechos := D_CONS_CLASIFICACIONES;
     HelpContext:= 0; //Pendiente
     FirstControl := RD_ENTIDAD;
     FCampoAtributo := 'AT_CAMPO';
     FCampoVersion := 'AT_VERSION';

     RD_ENTIDAD.LookupDataset := dmDiccionario.cdsEntidadesLookup;
     {$ifdef  RDD_Desarrollo}
     RD_VERSION.ReadOnly := False;
     {$endif}
end;

procedure TEditDefaults.Connect;
begin
     RD_ENTIDAD.DataField := CampoEntidad;
     AT_CAMPO.DataField := CampoAtributo;
     RD_VERSION.DataField := CampoVersion;
     with dmDiccionario do
     begin
          cdsEntidadesLookup.Conectar;
          Dataset.Conectar;
          Datasource.Dataset := Dataset;
     end;
end;

procedure TEditDefaults.DisConnect;
begin
     DataSource.Dataset := NIL;
end;
function TEditDefaults.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := TRUE;
end;

function TEditDefaults.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := TRUE;
end;

function TEditDefaults.PuedeImprimir(var sMensaje: String): Boolean;
begin
     Result := TRUE;
end;

function TEditDefaults.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := TRUE;
end;

procedure TEditDefaults.FormShow(Sender: TObject);
begin
     inherited;
     //Esta pantalla no tiene derecho de acceso por si sola.
     Datasource.AutoEdit := TRUE;
end;

end.
