inherited ClasifiReporte: TClasifiReporte
  Left = 399
  Top = 212
  Caption = 'Clasificaci'#243'n'
  ClientWidth = 405
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 405
    inherited ValorActivo2: TPanel
      Width = 146
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 368
    Height = 254
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'RC_ORDEN'
        Title.Caption = 'Orden'
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RC_NOMBRE'
        Title.Caption = 'Clasificaci'#243'n'
        Width = 259
        Visible = True
      end>
  end
  object PanelMover: TPanel [2]
    Left = 368
    Top = 19
    Width = 37
    Height = 254
    Align = alRight
    Alignment = taLeftJustify
    BevelOuter = bvNone
    TabOrder = 2
    object Subir: TSpeedButton
      Tag = -1
      Left = 5
      Top = 84
      Width = 28
      Height = 28
      Hint = 'Mover Clasificaci'#243'n Hacia Arriba'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000333
        3333333333777F33333333333309033333333333337F7F333333333333090333
        33333333337F7F33333333333309033333333333337F7F333333333333090333
        33333333337F7F33333333333309033333333333FF7F7FFFF333333000090000
        3333333777737777F333333099999990333333373F3333373333333309999903
        333333337F33337F33333333099999033333333373F333733333333330999033
        3333333337F337F3333333333099903333333333373F37333333333333090333
        33333333337F7F33333333333309033333333333337373333333333333303333
        333333333337F333333333333330333333333333333733333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = SubirClick
    end
    object Bajar: TSpeedButton
      Tag = 1
      Left = 5
      Top = 116
      Width = 28
      Height = 28
      Hint = 'Mover Clasificaci'#243'n Hacia Abajo'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333303333
        333333333337F33333333333333033333333333333373F333333333333090333
        33333333337F7F33333333333309033333333333337373F33333333330999033
        3333333337F337F33333333330999033333333333733373F3333333309999903
        333333337F33337F33333333099999033333333373333373F333333099999990
        33333337FFFF3FF7F33333300009000033333337777F77773333333333090333
        33333333337F7F33333333333309033333333333337F7F333333333333090333
        33333333337F7F33333333333309033333333333337F7F333333333333090333
        33333333337F7F33333333333300033333333333337773333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = SubirClick
    end
  end
  inherited DataSource: TDataSource
    Left = 56
    Top = 176
  end
end
