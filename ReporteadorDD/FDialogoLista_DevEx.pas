unit FDialogoLista_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseDlgModal, StdCtrls, CheckLst, Buttons, ExtCtrls, ZetaClientDataset,
  ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, Vcl.ImgList, cxButtons;

type
  TDialogoLista_DevEx = class(TZetaDlgModal_DevEx)
    cbLista: TCheckListBox;
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    FDataSetChecked: TZetaClientDataset;
    FDataSetInicial: TZetaLookupDataset;
  protected
    property DataSetChecked: TZetaClientDataset read FDataSetChecked write  FDataSetChecked;
    property DataSetInicial: TZetaLookupDataset read FDataSetInicial write  FDataSetInicial;
  public
    { Public declarations }
  end;

var
  DialogoLista_DevEx: TDialogoLista_DevEx;

implementation
uses
    ZetaDialogo,
    DDiccionario;

{$R *.dfm}

procedure TDialogoLista_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     cbLista.Items.Clear;
     with DataSetInicial do
     begin
          First;
          while NOT EOF do
          begin
               cbLista.Items.AddObject(FieldByName(LookupDescriptionField).AsString, TObject(FieldByName(LookupKeyField).AsInteger));
               cbLista.Checked[cbLista.Items.Count-1] := FDataSetChecked.Locate(LookupKeyField,FieldByName(LookupKeyField).AsInteger,[]);
               Next;
          end;
     end;

end;

procedure TDialogoLista_DevEx.OKClick(Sender: TObject); //@gbeltran: Evento para el antiguo boton de ok
 var
    i: integer;
begin
     inherited;
     FDataSetChecked.EmptyDataSet;
     FDataSetChecked.MergeChangeLog;

     for i:=0 to cbLista.Items.Count - 1 do
     begin
          with FDataSetChecked do
          begin
               if cbLista.Checked[i] then
               begin
                    Append;
                    FieldByName(FDataSetInicial.LookupKeyField).AsInteger := Integer( cbLista.Items.Objects[i] );
                    Post;
               end;
          end;
     end;
     FDataSetChecked.Enviar;
end;

procedure TDialogoLista_DevEx.OK_DevExClick(Sender: TObject);
var
    i: integer;
begin
     inherited;
     FDataSetChecked.EmptyDataSet;
     FDataSetChecked.MergeChangeLog;

     for i:=0 to cbLista.Items.Count - 1 do
     begin
          with FDataSetChecked do
          begin
               if cbLista.Checked[i] then
               begin
                    Append;
                    FieldByName(FDataSetInicial.LookupKeyField).AsInteger := Integer( cbLista.Items.Objects[i] );
                    Post;
               end;
          end;
     end;
     FDataSetChecked.Enviar;

end;

end.
