inherited SistEditAccesos: TSistEditAccesos
  Left = 453
  Top = 247
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl: TPageControl
    OnChange = PageControlChange
    inherited tsTress: TTabSheet
      Caption = 'Clasificaciones'
    end
    object Entidades: TTabSheet
      Caption = 'Entidades'
      ImageIndex = 1
      object ArbolEntidades: TTreeView
        Left = 0
        Top = 0
        Width = 392
        Height = 265
        Align = alClient
        HideSelection = False
        Indent = 19
        ReadOnly = True
        RightClickSelect = True
        StateImages = ArbolImages
        TabOrder = 0
        OnKeyPress = ArbolBaseKeyPress
        OnMouseDown = ArbolBaseMouseDown
      end
    end
  end
end
