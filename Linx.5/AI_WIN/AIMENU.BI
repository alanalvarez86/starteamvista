' AiMenu.BI - Basic Include file
'
DEFINT A-Z

'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@                          Constants and Types                            @@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'
' ---------- AI Menu Structure ----------
TYPE AiMenuT
  Mname         AS STRING * 32  ' Name to display
  MhotC         AS INTEGER      ' Hot character
  MhotP         AS INTEGER      ' Hot character position
  Msetup        AS INTEGER      ' Setup operation
  Mop           AS INTEGER      ' Operation to perform when selected
  Mdone         AS INTEGER      ' Completion operation
  Mparam        AS STRING * 30  ' String parameter
END TYPE ' AiMenuT

'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@                               Common Section                            @@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'
COMMON SHARED /Menu/ AiMenuSetup           ' Setup operation
COMMON SHARED /Menu/ AiMenuOp              ' Operation to perform
COMMON SHARED /Menu/ AiMenuDone            ' Completion operation
COMMON SHARED /Menu/ AiMenuLen             ' Number of menu entries
COMMON SHARED /Menu/ AiMenuName$           ' Text name of the menu
COMMON SHARED /Menu/ AiMenuParam$          ' String menu parameter

'$DYNAMIC
COMMON SHARED /Menu/ AiMenus() AS AiMenuT
'$STATIC

'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@                     Sub and Function Declarations                       @@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'
DECLARE SUB      AiMenu         ( n$ )
DECLARE SUB      AiM            ( s$, bo, op, ao, p$ )
DECLARE SUB      AiMshow        ()
DECLARE FUNCTION AiMDo%         ( ky )

'End AiMenu.BI



