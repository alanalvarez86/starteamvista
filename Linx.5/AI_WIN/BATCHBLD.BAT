@Echo off
Echo Build it and he will come. > ~1-~1-.~1-
call BATCHASK.EXE
if errorlevel 3 goto SVBDOS
if errorlevel 2 goto SQB7x
if errorlevel 1 goto SQB45
goto EndItAll
:SQB45
Echo Creating link files for QuickBASIC V4.5
Blurp AIMAI_.LNK AIMAIN.LNK Q /Q
IF ERRORLEVEL 1 GOTO EndItAll
Echo Created AIMAIN.LNK
Blurp AIGE_.LNK AIGEN.LNK Q /Q
IF ERRORLEVEL 1 GOTO EndItAll
Echo Created AIGEN.LNK
goto EndItAll
:SQB7x
Echo Creating link files for QuickBASIC V7.x (PDS)
Blurp AIMAI_.LNK AIMAIN.LNK B /Q
IF ERRORLEVEL 1 GOTO EndItAll
Echo Created AIMAIN.LNK
Blurp AIGE_.LNK AIGEN.LNK B /Q
IF ERRORLEVEL 1 GOTO EndItAll
Echo Created AIGEN.LNK
goto EndItAll
:SVBDOS
Echo Creating link files for VisualBASIC for DOS V1.0
Blurp AIMAI_.LNK AIMAIN.LNK V /Q
IF ERRORLEVEL 1 GOTO EndItAll
Echo Created AIMAIN.LNK
Blurp AIGE_.LNK AIGEN.LNK V /Q
IF ERRORLEVEL 1 GOTO EndItAll
Echo Created AIGEN.LNK
:EndItAll
Del ~1-~1-.~1-
