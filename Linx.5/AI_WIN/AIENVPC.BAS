' AiEnvPC.bas
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'%%%%%%%%%%%                                                       %%%%%%%%%%
'%%%%%%%%%%%                     A i E n v P C                     %%%%%%%%%%
'%%%%%%%%%%%                                                       %%%%%%%%%%
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'
'   Project  : Applications Interface                   Version : 1.0
'   Usage    : Low level LINX environment               OS : PCDOS V3.3
'   Language : Microsoft Quick Basic V4.5               CS : IBM PC/AT
'
'   Purpose : LINX dependent device IO emulation for IBM PC compatibles
'
'-------------------------------- History -----------------------------------
'
'      Version     Date      Who     Short description of change
'        1.0     5 May 90    JHF     Created from ENVPC.BAS
'
'----------------------- Function and Procedure Index -----------------------
'
' OpenLINX   : Opens the LINX devices for IO
' TestEvent% : Tests for an event
' SaveEvent  : Saves the previous event to allow it to happen again
' ReadEvent% : Waits for an event
'
' Send       : Sends a record directly to to the host
' SendQue    : Sends a record indirectly to the host through the queue
' SetLINE    : Sets the state the control line
' SetLED     : Turns a LINX keyboard LED on or off
'
' SendCom    : Sends a record to the comm line
' SetDTR     : Sets the state of Data Terminal Ready
' SetRTS     : Sets the state of Request To Send for a comm port
' GetDSR%    : Gets Data Set Ready status for a comm port
'
' PENDING%   : TRUE if Send will result in a pause
' MEMFULL%   : TRUE if less than 1024 bytes remain in RAM
' ONLINE%    : TRUE when ONLINE to the network
' GetDATE$   : Translates DATE$ into standard form (MM/DD/YYYY)
' GetTIME$   : Translates TIME$ into standard form (HH:MM:SS)
'
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'%%%%%%%%%%        CopyRight 1990, Linx Data Terminals, Inc.       %%%%%%%%%%
'%%%%%%%%%%                  All Rights Reserved.                  %%%%%%%%%%
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
DEFINT A-Z
'$INCLUDE:'AiVar.BI'
'$INCLUDE:'AiUtil.BI'
'$INCLUDE:'AiEnvPc.BI'

'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@                         Low Level IO Functions                          @@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'
'   OpenLINX      : Opens all devices to be used by IO functions
'   TestEvent%    : Tests for a low level event
'   SaveEvent     : Saves the previous event to allow it to happen again
'   ReadEvent%    : Waits for a low level event
'

'****************************************************************************
'
'   OpenLINX : Opens all devices to be used by low level IO routines
'
'   This MUST be called before any of the Event or Read routines.
'   This performs the actual BASIC OPEN statements for all of the
'   devices which are accessed by the low level IO.  Note, the global
'   constant PcMode will be TRUE if this program is running using
'   EnvPC.BAS instead of Env.BAS.  You may use this for any operations
'   which are not fully emulated by the EnvPC environment.
'
'   This also establishes the global environment variables :
'      DSPWIDTH% = 40 or 16 depending on the type of LINX
'      ONLINE%   = TRUE when online (updated by TestEvent%)
'
'   OpenLINX
'
'****************************************************************************
SUB OpenLINX
  COMOPEN%  = TRUE
  TERMTYPE$ = "NORML"
  TERMNUM%  = 1
END SUB 'OpenLINX

'****************************************************************************
'
'   TestEvent% : Tests for a low level event
'
'   Returns 0 when no event has occurred.  Events are either keys read
'   from the LINX keyboard, or special codes.  Special codes are
'   always negative, although some special keys also return negative
'   codes.  See OpenEvent for initialization information.
'
'   The most important events are :
'
'       TimeoutEvent : The input timer has expired
'       BadgeEvent   : Read from card readers; See INTYPE%
'       NetEvent     : Read from network has occurred
'       ComEvent     : Read from the main comm port
'       AuxEvent     : Read from the aux comm port
'       SensorEvent  : Sensor state change
'
'   When BadgeEvent has occurred, InBuf$ contains the bar code or mag
'   stripe which was read.
'
'   When ComEvent or AuxEvent has occurred, InCom$ contains the
'   LINE INPUT format line which was read from the comm line.
'
'   When NetEvent has occurred, InNet$ contains the record from the host.
'
'   The events which you want to test for are indicated by the "mask"
'   parameter.  Each device which you want tested MUST have a bit set
'   in the mask.  Bits are set by building a mask using the constants :
'
'       KbdMask, TimeoutMask, BadgeMask, NetMask, ComMask,
'       AuxMask, and SensorMask.
'
'   To simplify matters, some special mask values are also allowed,
'   These are :
'
'       0 = Read from keyboard only
'       1 = Read from keyboard with timeout (see ReadEvent%)
'       2 = Read from keyboard and Badge, etc with timeout
'
'   e = TestEvent% ( mask );
'
'   Note that timeouts only apply to reads which wait for an event.
'   TestEvent% never waits.  See ReadEvent% for more info.
'
'****************************************************************************
FUNCTION TestEvent% ( mask% )
  DIM cop AS OpCode
  cop = Ops(Pc)
Reenter:
  x=SAVEDEVENT%
  IF x=0 THEN
    ' ----- Keyboard Character Input with mapping -----
    IF mask%<3 OR (mask% AND KbdMask) THEN
      kk$ = INKEY$
      IF LEN(kk$) > 0 THEN
        IF LEN(kk$) = 1 THEN
          x = ASC(kk$)
          IF x=HT THEN AiEnter HT : GOTO Reenter
        ELSE
          x = -ASC(MID$(kk$, 2))
          IF x = -75 THEN x = 8   'Remapped to BS to for LINX
          IF x = -83 THEN x = 127 'Remapped from DEL for LINX
          IF x = -79 THEN x = 159 'EndKey mapped to LINX exit
          IF x>=AltM AND x<=AltQ THEN
            AiEnter x : GOTO Reenter
          END IF
          IF x<=F1 AND x>=F10 THEN
            x=ABS(x-F1)+LF1
          END IF
        END IF
        'UvWas SvInKbd : Uv(SvInKbd) = kk$ : Uve(SvInKbd)=x : UvIs SvInKbd
      END IF
    ELSE
      kk$ = INKEY$
      IF LEN(kk$)=1 THEN
        IF ASC(kk$)=HT THEN AiEnter HT : GOTO Reenter
        IF ASC(kk$)=ESC THEN x=ESC
      ELSE
        IF LEN(kk$)=2 THEN
          k = -ASC(MID$(kk$, 2))
          IF k>=AltM AND k<=AltQ THEN
            AiEnter k : GOTO Reenter
          END IF
        END IF
      END IF
    END IF
    IF x = 0 THEN
      ' ----- Bar code and mag stripe input -----
      IF mask%=2 OR (mask% AND BadgeMask) THEN
        IF AiReadDevice% ( AiBadge, s$ ) THEN
          AiLog "<--BDGE", s$
          UvWas SvInBuf  : Uv(SvInBuf)=s$
          x = BadgeEvent : Uve(SvInBuf) = x
          SOUND 200, 1   : UvIs SvInBuf
          BadgeSource$ = "SLOT" ' SLOT, WAND, or LASER
          BadgeType$   = "C39"  ' C39, I25, UPC, CBAR, C128, or MAG
        END IF
      END IF
      ' ----- Network line input -----
      IF x = 0 AND (mask% AND NetMask) THEN
        IF AiReadDevice% ( AiNet, s$ ) THEN
          AiLog "<--HOST", s$
          UvWas SvInNet  : Uv(SvInNet)=s$
          x = NetEvent   : Uve(SvInNet) = x
          SOUND 300, 1   : UvIs SvInNet
        END IF
      END IF
      ' ----- Main Comm port line input -----
      IF x=0 AND (mask% AND ComMask) THEN
        IF AiReadDevice% ( AiCom, s$ ) THEN
          AiLog "<--COM ", s$
          UvWas SvInCom  : Uv(SvInCom)=s$
          x = ComEvent   : Uve(SvInCom)=x
          SOUND 400, 1   : UvIs SvInCom
        END IF
      END IF
      ' ----- Aux Comm port line input -----
      IF x=0 AND (mask% AND AuxMask) THEN
        IF AiReadDevice% ( AiAux, s$ ) THEN
          AiLog "<--AUX ", s$
          UvWas SvInCom  : Uv(SvInCom)=s$
          x = AuxEvent   : Uve(SvInCom)=x
          SOUND 500, 1   : UvIs SvInCom
        END IF
      END IF
      ' ----- Sensor port state change input -----
      IF x=0 AND (mask% AND SensorMask) THEN
        xs = 1
        FOR txs = 2 TO cop.Si: xs = xs + xs: NEXT txs
        IF AiOldSense% <> Uvs(SvSENSE) THEN
          v1 = NOT (AiOldSense% AND xs) AND (Uvs(SvSENSE) AND xs)
          v2 = (AiOldSense% AND xs) AND NOT (Uvs(SvSENSE) AND xs)
          IF v1 OR v2 THEN
            IF Uvs(SvSENSE) AND xs THEN m$="ON" : ELSE m$="OFF"
            AiLog "<--SENS", m$
            UvWas SvSENSE : x = SensorEvent : Uve(SvSENSE)=x
            UvIs  SvSENSE : AiOldSense% = Uvs(SvSENSE)
            SOUND 600, 1
          END IF
        END IF
      END IF
    END IF
  END IF
  SAVEDEVENT% = 0
  TestEvent% = x
END FUNCTION 'TestEvent%

'****************************************************************************
'
'   SaveEvent : Saves the previous event to allow it to happen again
'
'   Saves the event which has just occurred from TestEvent%.  The very
'   next TestEvent% will return this event code.
'
'   SaveEvent eventcode%
'
'****************************************************************************
SUB SaveEvent ( e% )
  SAVEDEVENT% = e%
END SUB 'SaveEvent

'****************************************************************************
'
'   ReadEvent% : Waits for a low level event
'
'   Waits for an event to occur.  The event(s) to wait for are specified
'   in the same manner as TestEvent%.
'
'   For timeouts set the global variable TIMEOUT% to the number of
'   1/100 second intervals you want to wait before a TimeoutEvent is
'   returned.  The value of TIMEOUT% is not modified by ReadEvent%.
'
'   e = ReadEvent% ( mask );
'
'****************************************************************************
FUNCTION ReadEvent% ( mask% )
  AiInputStat
  t = mask%=1 OR mask%=2 OR (mask% AND TimeoutMask)
  v&  = TIMEOUT%
  st& = TIMER
  DO
    x = TestEvent ( mask% )
    IF x=0 THEN
      ot& = et& : et& = TIMER : SaveXY
      hs& = (et&-st&)*100
      IF ot& <> et& AND NOT Ai40% THEN
        COLOR LightGreen, Black
        IF hs& >= v& THEN
          t$ = "Timeout of"+STR$(v&)+" has expired.           "
        ELSE
          t$ = "Timeout of"+STR$(v&)+" has"+STR$(v&-hs&)+" left.            "
        END IF
        LOCATE 1,44 : PRINT LEFT$(t$,36);
        LOCATE 2,44 : PRINT "Running : Reading ...              "
      END IF
      IF t AND hs& >= v& THEN x = TimeoutEvent
      RestoreXY
    END IF
  LOOP WHILE x=0
  EXITEVENT% = x
  ReadEvent% = x
  AiStat  "Running..."
END FUNCTION 'ReadEvent%

'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@                      Low Level Comm Port Access                         @@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'
'   SetDTR  : Sets the state of Data Terminal Ready
'   SetRTS  : Sets the state of Request To Send for a comm port
'   GetDSR% : Gets Data Set Ready status for a comm port
'   SendCom : Sends a record to the comm line
'

'****************************************************************************
'
'   SetDTR  : Sets the state of Data Terminal Ready
'   SetRTS  : Sets the state of Request To Send for a comm port
'
'   Set state=TRUE to turn DTR or RTS on.  You may examine the global
'   variables RTS1%, DTR1%, RTS2%, and DTR2% to get the current state.
'   n% is 1 for COM and 2 for AUX
'
'   SetDTR n%, state%
'   SetRTS n%, state%
'
'****************************************************************************
SUB SetDTR ( n%, s% )
  IF n% = 1 THEN UvWas SvDTR1 : Uvs(SvDTR1) = s%
  IF n% = 2 THEN UvWas SvDTR2 : Uvs(SvDTR2) = s%
  AiUpdateLines
END SUB 'SetDTR

SUB SetRTS ( n%, s% )
  IF n% = 1 THEN UvWas SvRTS1 : Uvs(SvRTS1) = s%
  IF n% = 2 THEN UvWas SvRTS2 : Uvs(SvRTS2) = s%
  AiUpdateLines
END SUB 'SetRTS

'****************************************************************************
'
'   GetDSR% : Gets Data Set Ready status for a comm port
'
'   A TRUE is returned if DSR is ON.  n% is 1 for COM and 2 for AUX
'
'   state = GetDSR%(n%)
'
'****************************************************************************
FUNCTION GetDSR% ( n% )
  IF n% = 1 THEN GetDSR% = Uvs(SvDSR1) ELSE GetDSR% = FALSE
END FUNCTION 'GetDSR%

'****************************************************************************
'
'   SendCom : Sends a record to a comm line
'
'   A string is sent to the comm line.  The string is sent exactly as is,
'   so append CR or CR and LF if you are sending a line.
'   n% is 1 for COM and 2 for AUX.
'
'   SendCom n%, s$
'
'****************************************************************************
SUB SendCom ( n%, s$ )
  IF n%=2 THEN i=AiAux : d$="-->AUX " ELSE i=AiCom : d$="-->COM "
  AiLog d$, s$
  UvWas AiD(i).tx : Uv(AiD(i).tx) = s$
  AiDevUpdate i
  SOUND 60, 4
END SUB 'SendCom
'
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@                      Low Level Write/Control Functions                  @@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'
'   PENDING% : TRUE if Send will result in a pause
'   MEMFULL% : TRUE if less than 1024 bytes remain in RAM
'   ONLINE%  : TRUE when ONLINE to the network
'   SetLINE  : Sets the state of the control line
'   SetLED   : Turns a LINX keyboard LED on or off
'   Send     : Sends a record directly to to the host
'   SendQue  : Sends a record indirectly to the host through the que
'   GetDATE$ : Translates DATE$ into standard form (MM/DD/YYYY)
'   GetTIME$ : Translates TIME$ into standard form (HH:MM:SS)
'

'****************************************************************************
'
'   SetLINE : Sets the state of the control lines.  To set a specific
'             control line then the corresponding bit in the parameter
'             variable LineMask% needs to bet set.
'             Example to set control line 1, 3, 5, 6.
'
'             LineMask% = 0     ' Set all bits to 0
'             LineMask% OR 1    ' To set bit 0 to a 1
'             LineMask% OR 4    ' To set bit 3 to a 1
'             LineMask% OR 16   ' To set bit 5 to a 1
'             LineMask% OR 32   ' To set bit 6 to a 1
'
'             or you could just write LineMask% = 53
'
'   The control lines (1-6) may be turned ON or OFF.  You may check the
'   global variable LINESTATE% for current state of any given control
'   line by performing the following:
'
'    IF LINESTATE% AND 1  THEN LineOneOn   = TRUE     'bit 0
'    IF LINESTATE% AND 2  THEN LineTwoOn   = TRUE     'bit 1
'    IF LINESTATE% AND 4  THEN LineThreeOn = TRUE     'bit 2
'    IF LINESTATE% AND 8  THEN LineFourOn  = TRUE     'bit 3
'    IF LINESTATE% AND 16 THEN LineFiveOn  = TRUE     'bit 4
'    IF LINESTATE% AND 32 THEN LineSixOn   = TRUE     'bit 5
'
'
'   Call the routine to set the control lines...
'   SetLINE LineMask%
'
'****************************************************************************
SUB SetLINE ( s% )
  STATIC LastMask%
  cln% = 1
  LINESTATE% = s%
  UvWas SvLINESTATE : Uvs(SvLINESTATE) = LINESTATE%
  FOR x = 1 TO 6
    IF LINESTATE% AND cln% THEN m$=" ON" : ELSE m$=" OFF"
    IF NOT (LastMask AND cln%) THEN AiLog "-->LINE" + STR$(x), m$
    cln% = cln% + cln%
  NEXT x
  LastMask% = LINESTATE%
  AiUpdateLines
END SUB 'SetLINE

'****************************************************************************
'
'   Send : Sends a record directly to to the host
'
'   A string is sent to the host from this terminal.  The global variable
'   ONLINE% should be TRUE before calling Send.  If the terminal is not
'   ONLINE% then your application will be paused until it is.
'
'   Send s$
'
'****************************************************************************
SUB Send ( s$ )
  UvWas AiD(AiNet).tx : Uv(AiD(AiNet).tx) = s$
  AiLog "-->NET ", s$
  AiDevUpdate AiNet
  SOUND 80, 8
END SUB 'Send

'****************************************************************************
'
'   SendQue : Sends a record indirectly to the host through the que
'
'   A string is placed into the network queue.  The record will in turn
'   be sent to the host as soon as possible.  SendQue has a lower priority
'   than Send, and will buffer records even when NOT ONLINE.
'   ERR is 0 if the queue was not full.
'
'   SendQue s$
'
'****************************************************************************
SUB SendQue ( s$ )
  UvWas AiD(AiNet).tx : Uv(AiD(AiNet).tx) = s$
  AiLog "-->QUE ", s$
  AiDevUpdate AiNet
  SOUND 90, 2
END SUB 'SendQue

'****************************************************************************
'
'   SetLED : Turns a LINX keyboard LED on or off
'
'   A LINX may have up to 25 LEDs (Most LINX IIIs have only 10).
'   The LEDs are numbered 1 to 25.  SetLED -1,FALSE clears all of the
'   LEDs.  state% should be TRUE for ON and FALSE for OFF.
'
'   SetLED n%, state%
'
'****************************************************************************
SUB SetLED ( n%, state% )
  UvWas SvLED
  IF state% THEN
    IF n%>0 AND n%<11 THEN MID$(Uv(SvLED),n%*3,1)=CHR$(4)
  ELSE
    IF n%<0 THEN
      Uv(SvLED) = "  �  �  �  �  �  �  �  �  �  � "
    ELSE
      IF n%>0 AND n%<11 THEN MID$(Uv(SvLED),n%*3,1)="�"
    END IF
  END IF
  AiUpdateLeds
END SUB 'SetLED

'****************************************************************************
'
'   GetTIME$ : Translates TIME$ into standard form (HH:MM:SS)
'
'   In LINX Basic TIME$ is in the form hhmmsshh.  This routine drops the
'   hundreth seconds and puts colons between hours-minutes and minute-
'   seconds.
'
'   GetTIME$
'
'****************************************************************************
FUNCTION GetTIME$
  GetTIME$ = TIME$
END FUNCTION 'GetTIME$

'****************************************************************************
'
'   GetDATE$ : Translates DATE$ into standard form (MM/DD/YYYY)
'
'   In LINX Basic DATE$ is in the form yymmddx.  This routine ignores the
'   day (the x)
'
'   GetDATE$
'
'****************************************************************************
FUNCTION GetDATE$
  GetDATE$ = DATE$
END FUNCTION 'GetDATE$

' END OF MODULE AiEnvPC.bas
