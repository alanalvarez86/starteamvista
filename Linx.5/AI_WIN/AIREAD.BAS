' AiRead.bas
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'%%%%%%%%%%%                                                       %%%%%%%%%%
'%%%%%%%%%%%                       A i R e a d                     %%%%%%%%%%
'%%%%%%%%%%%                                                       %%%%%%%%%%
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'
'   Project  : LINX Support Environment                 Version : 2.0
'   Usage    : Formatted reading                        OS : PCDOS V3.3
'   Language : LinxBasic V1.0                           CS : IBM PC/AT
'
'   Purpose : Formatted read keyboard functions
'   Requirements : Must be preceded by Env.bas, or EnvPC.bas
'
'-------------------------------- History -----------------------------------
'
'      Version     Date      Who     Short description of change
'        1.0    20 Mar 89    JHF     Original creation
'
'----------------------- Function and Procedure Index -----------------------
'
'   CL1           : clears the top line of the display and homes cursor
'   CL2           : clears line 2 and left justifies cursor
'   PC1           : prints a string centered on line 1
'   PC2           : prints a string centered on line 2
'   ReadFlush     : flushes the keyboard buffer
'   ReadStrField% : edit a string field on the display
'   ReadStr%      : simple edit a string on the screen
'   ReadNumStr%   : simple edit of string of numeric characters
'   ReadPassword% : inputs a password from the keyboard
'   ReadYN%       : wait for a YES/NO response from user
'   ReadInt%      : input an integer value from the keyboard
'   ReadReal%     : input a real value from the keyboard
'   ReadLong%     : input a long value from the keyboard
'
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'%%%%%%%%%%        CopyRight 1989, Linx Data Terminals, Inc.       %%%%%%%%%%
'%%%%%%%%%%                  All Rights Reserved.                  %%%%%%%%%%
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
DEFINT A-Z
''##C @
'$INCLUDE:'AiVar.BI'
'$INCLUDE:'AiEnvPc.BI'
''##E

' --------------------  Read default attributes --------------------
CONST NUMERIC   = &H0001 ' Treat as numeric
CONST ZEROFILL  = &H0002 ' Treat as numeric and pad with zeros
CONST AUTOEXIT  = &H0010 ' Causes the field to be auto exited
CONST BLANKPAD  = &H0020 ' Selects blank padding instead of underscores
CONST SECURE    = &H0040 ' The field is displayed as *****s
CONST NOEDIT    = &H0080 ' Displayed but cannot be edited
CONST NOCLEAR   = &H0100 ' Inhibits auto clear on input
CONST NOCURSOR  = &H0200 ' Inhibits display of cursor during edit
CONST NODEFAULT = &H0400 ' Do not use the default value in the string
CONST FORMEXIT  = &H0800 ' Allow arrows to exit the field
CONST USETABLE  = &H1000 ' Use CHARTABLE$ to restrict legal characters

'****************************************************************************
'
'   CL1 : clears the top line of the display and homes cursor
'   CL2 : clears line 2 and left justifies cursor
'
'****************************************************************************
SUB CL1
  LOCATE 1,1 : PRINT SPACE$(DSPWIDTH%); : LOCATE 1,1
END SUB 'CL1

SUB CL2
  LOCATE 2,1 : PRINT SPACE$(DSPWIDTH%); : LOCATE 2,1
END SUB 'CL2

'****************************************************************************
'
'   PC1 : prints a string centered on line 1
'   PC2 : prints a string centered on line 2
'
'   PC1 s$
'
'****************************************************************************
''##C E
SUB PC1 ( s$ )
  LOCATE 1,1 : i = LEN(s$) : c = (DSPWIDTH%-i) \ 2
  IF c<0 THEN c=0
  d$=SPACE$(c)+s$+SPACE$(DSPWIDTH%)
  PRINT LEFT$(d$,40);
END SUB 'PC1
''##E

''##C E
SUB PC2 ( s$ )
  LOCATE 2,1 : i = LEN(s$) : c = (DSPWIDTH%-i) \ 2
  IF c<0 THEN c=0
  d$=SPACE$(c)+s$+SPACE$(DSPWIDTH%)
  PRINT LEFT$(d$,40);
END SUB 'PC2
''##E

'****************************************************************************
'
'     ReadFlush : flushes the keyboard buffer
'
'     This clears the keyboard buffer of any pending keystrokes.
'
'     ReadFlush
'
'****************************************************************************
SUB ReadFlush
  DO
    x = TestEvent%(0)
  LOOP WHILE x <> 0
END SUB 'ReadFlush

'****************************************************************************
'
'      ReadStrField% : Edit a string on the display
'
'      The string$ contents is placed on the screen and may be edited by
'      the user.  This default may be inhibited using NODEFAULT
'
'      The attr% is made by adding the following :
'
'         NUMERIC    : Assume field is numeric
'         ZEROFILL   : Pad front of value with zeros
'         AUTOEXIT   : Causes the field to be auto exited
'         BLANKPAD   : Selects blank padding instead of underscores
'         SECURE     : The field is displayed as *****s
'         NOEDIT     : Displayed but cannot be edited
'         NOCLEAR    : Inhibits auto clear on input
'         NOCURSOR   : Inhibits display of cursor during edit
'         FORMEXIT   : Allow arrows to exit the field entry
'         NODEFAULT  : Do not use the default value in the string
'         USETABLE   : Use CHARTABLE$ to restrict legal characters
'
'      The fieldlength% is in characters.  The startingchar% specifies
'      the offset from the beginning of the field at which editing is
'      to start, this begins at 0.  Note that the global EXITOFFSET%
'      is set to the cursor position at exit.  These may be used to
'      restart a read which was interrupted by some event.  The global
'      MODIFIED% is TRUE when the string was changed in any way.
'
'      The strlength is the maximum number of characters to allow in the
'      string (not including the terminal null).  This must be at least
'      fieldlength% characters long.
'
'      The return value "e" is an event code.  This is 0 on a normal
'      return (operator pressed CR).  The devices which are active are
'      defined by the global variable GLOBALMASK% which is in the same
'      format as the mask% used by ReadEvent% and TestEvent%.
'
'      The field will start at the current cursor location.  The
'      cursor is left at its starting location when ReadStrField exits.
'
'      e = ReadStrField% ( attr%, fieldlength%, startingchar%,
'                          string$, stringlen% )
'
'***************************************************************************
FUNCTION ReadStrField% ( attr%, flength%, strpos%, s$, slen% )
  length = flength%
  spos   = strpos%
  MODIFIED% = FALSE
  x = POS(0)
  y = CSRLIN
  
  ' ----- Get the flags -----
  IF (attr% AND NODEFAULT) THEN s$=""
  autoEx    = (attr% AND AUTOEXIT) <> 0
  sec       = (attr% AND SECURE  ) <> 0
  noEditing = (attr% AND NOEDIT  ) <> 0
  noC       = (attr% AND NOCURSOR) <> 0
  formE     = (attr% AND FORMEXIT) <> 0
  utable    = (attr% AND USETABLE) <> 0
  zeroed    = (attr% AND ZEROFILL) <> 0
  isnumber  = (attr% AND NUMERIC ) <> 0 OR zeroed
  IF isnumber AND NOT utable THEN                  '!JF91
    utable = TRUE : CHARTABLE$ = "0123456789.-"    '!JF91
  END IF                                           '!JF91
  IF (attr% AND BLANKPAD) <> 0 THEN pad$=" " ELSE pad$="_"
  clearNow  = (attr% AND NOCLEAR) = 0
  IF sec THEN clearNow = TRUE
  bsDel     = FALSE ' Initially BS does not delete characters
  IF length > slen% THEN length = slen%
  IF spos   > slen% THEN spos   = slen%
  IF noC THEN
    LOCATE y, x, 0 ' Disable cursor
  ELSE
    LOCATE y, x, 1 ' Enable cursor
  END IF
  
  ' ---------- Main keyboard read and edit loop ----------
  DO WHILE LEN(s$)<slen%
    s$ = s$+pad$ ' Pad the end of the string with underscores or spaces
  LOOP
  p = spos
  IF p > length THEN p = length : n = spos-length
  n    = 0
  ends = slen% - 1
  
  DO '***** Process a keystroke loop *****
    IF p < 0 THEN p = 0 : n=n-1
    IF n < 0 THEN n = 0
    IF p > length THEN p = length : n = n+1
    IF p+n > slen% THEN n = slen%-p
    LOCATE y, x
    IF sec THEN
      PRINT STRING$(length,"*");
    ELSE
      PRINT MID$(s$,n+1,length);
    END IF
    IF NOT noC AND x+p<DSPWIDTH% THEN LOCATE y, x+p
    
    ' Wait for a keystroke
    k = ReadEvent% ( GLOBALMASK% )
    IF noEditing THEN
      exitc = k
    ELSE ' Do a single keystroke
      exitc = 0
      SELECT CASE k ' Key function selection
        CASE BS        ' ----- Delete char preceeding cursor -----
          IF n+p>0 AND bsDel THEN
            p = p-1 : i = p+n
            DO WHILE i<slen%
              IF i=ends THEN
                MID$(s$,i+1,1) = pad$
              ELSE
                MID$(s$,i+1,1) = MID$(s$,i+2,1)
              END IF
              i=i+1
            LOOP
            MODIFIED% = TRUE
          ELSE
            IF p<>0 THEN
              p = p - 1
            ELSE
              IF n<>0 THEN
                n=n-1
              ELSE
                IF formE THEN exitc=Left
              END IF
            END IF
          END IF
        CASE Right     ' ----- Move right in the current field -----
          bsDel = FALSE ' BS is now safe
          IF p<length THEN
            p=p+1
          ELSE
            IF n+p<slen% THEN
              n=n+1
            ELSE
              IF formE THEN exitc=Right
            END IF
          END IF
        CASE ClearKey  ' ----- Delete the entire field -----
          IF formE AND n+p=0 THEN
            exitc = ClearKey
          ELSE
            n = 0 : p = 0
            FOR i=n+p TO slen%-1
              MID$(s$,i+1,1) = pad$
            NEXT i
            MODIFIED% = TRUE
          END IF
        CASE ELSE      ' ----- Put normal character in buffer -----
          IF k>=32 AND k<127 THEN
            bsDel = TRUE ' BS is now destructive
            IF clearNow THEN
              FOR i=n+p TO slen%-1
                MID$(s$,i+1,1) = pad$
              NEXT i
            END IF
            IF utable THEN
              IF INSTR(CHARTABLE$,CHR$(k))=0 THEN k=0
            END IF
            IF n+p <> slen% THEN
              IF k > 0 THEN
                ' Insert mode has              IF INSERTMODE% THEN
                ' been removed to                i = ends
                ' improve overall                DO WHILE i > n+p
                ' keypad entry                     MID$(s$,i+1,1) = MID$(s$,i,1)
                ' performance.                     i = i-1
                '                                LOOP
                '                              END IF
                MID$(s$,n+p+1,1) = CHR$(k)
                p = p+1
              END IF
            END IF
            MODIFIED% = TRUE
            IF autoEx AND n+p=slen% THEN exitc=EnterKey
          ELSE
            exitc = k
          END IF
      END SELECT ' Key function selection
    END IF ' Do a single keystroke
    clearNow = FALSE
  LOOP WHILE exitc = 0 '***** Process a keystroke loop *****
  
  '---------- Return the data ----------
  i = ends
  DO WHILE i>0 AND MID$(s$,i+1,1)=pad$
    i = i-1 ' Lop all trailing pads off of the input field
  LOOP
  i = i + 1 : IF i=1 AND MID$(s$,1,1)=pad$ THEN i=0
  s$ = LEFT$(s$,i)
  i = 1
  DO
    i = INSTR(i,s$,pad$)
    IF i<>0 THEN MID$(s$,i,1) = " " ' Set all pads to blanks
  LOOP WHILE i<>0
  IF isnumber AND length<10 THEN s$ = LTRIM$(STR$(VAL(s$))) '!JF91
  i = length-LEN(s$) : IF zeroed AND i>0 THEN s$=STRING$(i,&H30)+s$
  IF NOT sec THEN
    LOCATE y, x
    PRINT MID$(s$,1,length);
  END IF
  EXITOFFSET% = n+p
  EXITEVENT%  = exitc
  IF exitc = EnterKey THEN exitc = 0
  LOCATE y, x
  ReadStrField% = exitc
END FUNCTION ' ReadStrField%

'****************************************************************************
'
'       ReadStr% : Edit a string on the screen
'
'       This is a simplified version of ReadStrField% that does not
'       have as many parameters.  Any attributes should be in
'       the GLOBALATTR% variable.  Don't forget to set GLOBALMASK%
'       to the devices which can terminate the read.  Make sure
'       you include KbdMask.  See TestEvent% for more info.
'
'       e = ReadStr ( length%, s$ )
'
'*****************************************************************************
''##C ?
FUNCTION ReadStr% ( n%, s$ )
  ReadStr% = ReadStrField% ( GLOBALATTR%, n%, 0, s$, n% )
END FUNCTION 'ReadStr%
''##E

'****************************************************************************
'
'       ReadNumStr% : Edit a numeric string on the screen
'
'       The string which is edited may contain only 0 through 9.
'       Any attributes should be in the GLOBALATTR% variable.
'
'       e = ReadNumStr ( length%, s$ )
'
'*****************************************************************************
''##C ?
FUNCTION ReadNumStr% ( n%, s$ )
  CHARTABLE$ = "0123456789"
  ReadNumStr% = ReadStrField% ( GLOBALATTR% OR USETABLE, n%, 0, s$, n% )
END FUNCTION 'ReadNumStr%
''##E

'****************************************************************************
'
'       ReadYN% : Wait for a YES/NO response from user
'
'       Returns TRUE or FALSE depending on the user's response.
'       EXITKEY% is set if you really need the event value.
'       Nothing is displayed, this just waits for Enter, Clear,
'       or a Y or N.
'
'       IF ReadYN% ( default% ) THEN ...
'
'****************************************************************************
''##C ?
FUNCTION ReadYN% ( defv% )
  e = ReadEvent% ( GLOBALATTR% )
  IF e=89 OR e=121 OR e=EnterKey THEN ReadYN% = TRUE  : EXIT FUNCTION
  IF e=78 OR e=110 OR e=ClearKey THEN ReadYN% = FALSE : EXIT FUNCTION
  IF e=0 THEN ReadYN% = defv% ELSE ReadYN% = NOT defv%
END FUNCTION 'ReadYN%
''##E

'****************************************************************************
'
'   ReadPassword% : Inputs a password from the keyboard
'
'   e = ReadPassword% ( length%, string$ );
'
'****************************************************************************
''##C ?
FUNCTION ReadPassword% ( n%, s$ )
  ReadPassword% = ReadStrField% ( GLOBALATTR% OR SECURE, n%, 0, s$, n% )
END FUNCTION 'ReadPassword
''##E

'****************************************************************************
'
'   ReadInt% : Input an integer value from the keyboard
'
'   To suppress display of the default value, use a negative length.
'
'   e = ReadInt% ( length%, int% )
'
'****************************************************************************
''##C ?
FUNCTION ReadInt% ( n%, v% )
  IF n% > 0 THEN s$ = STR$(v%) ELSE s$ = ""
  IF LEFT$(s$,1)=" " THEN s$=MID$(s$,2)
  i = ABS(n%) : CHARTABLE$ = "0123456789-"
  ReadInt% = ReadStrField% ( GLOBALATTR% OR USETABLE, i, 0, s$, i )
  v% = VAL(s$)
END FUNCTION 'ReadInt%
''##E

'****************************************************************************
'
'   ReadLong% : Input a long value from the keyboard
'
'   To suppress display of the default value, use a negative length.
'
'   e = ReadLong% ( length%, long& )
'
'****************************************************************************
''##C ?
FUNCTION ReadLong% ( n%, v& )
  IF n% > 0 THEN s$ = STR$(v&) ELSE s$ = ""
  IF LEFT$(s$,1)=" " THEN s$=MID$(s$,2)
  i = ABS(n%) : CHARTABLE$ = "0123456789-"
  ReadLong% = ReadStrField% ( GLOBALATTR% OR USETABLE, i, 0, s$, i )
  v& = VAL(s$)
END FUNCTION 'ReadLong%
''##E

'****************************************************************************
'
'   ReadReal% : Input a real value from the keyboard
'
'   The precision specifies how many decimal points to display.  It
'   does not affect the precision of the value input by the user.
'   Use a negative length to suppress the default value.
'   This only handles floating point values, NOT scientific forms.
'
'   e = ReadReal% ( length%, precision%, real! );
'
'****************************************************************************
''##C ?
FUNCTION ReadReal% ( n%, p%, v! )
  IF n% > 0 THEN
    s$ = STR$(v!)
    IF LEFT$(s$,1)=" " THEN s$=MID$(s$,2)
    i  = INSTR(s$,".")
    IF i>0 THEN s$ = LEFT$(s$,i+p%)
  ELSE
    s$ = ""
  END IF
  i = ABS(n%) : CHARTABLE$ = "0123456789.-"
  ReadReal% = ReadStrField% ( GLOBALATTR% OR USETABLE, i, 0, s$, i )
  v! = VAL(s$)
END FUNCTION 'ReadReal%
''##E

' End of Module AiRead.bas





