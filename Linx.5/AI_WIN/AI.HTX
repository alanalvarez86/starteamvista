' AI.HTX
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'%%%%%%%%%%%                                                       %%%%%%%%%%
'%%%%%%%%%%%                A I   H e l p   T e x t                %%%%%%%%%%
'%%%%%%%%%%%                                                       %%%%%%%%%%
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'
'   Project  : Applications Interface                   Version : 1.0
'   Usage    : Help text                                OS : PCDOS V3.3
'   Language : AI Help Text V1.0                        CS : IBM PC/AT
'
'   Purpose : Provides the help info for the applications interface
'
'-------------------------------- History -----------------------------------
'
'      Version     Date      Who     Short description of change
'        1.0    11 Jul 90    JHF     Original Creation
'
'------------------------------ Description ---------------------------------
'
'       The help file uses control records to determine the contents
'       of subsequent text.  The control records are :
'
'       ~C              : Start of table of contents (main index)
'       ~X <name> | <index> : Name and index for table of contents
'       ~I <index>      : Matching index for a help subject (1st line)
'       ~T <title>      : Title to put at top of the screen (2nd line)
'       ~Z              : End of the help file (must follow a ^L)
'
'       The first non comment record of the file must be ~C and
'       it must follow a Ctrl L.  All lines until the Ctrl L are
'       comments.
'
'       The table of contents must consist of two pages separated
'       with Ctrl Ls.  These will form the two columns of the
'       help index.
'
'       Comment lines are lines which start with ' (single quote.
'       Subjects and subject pages must be separated with Ctrl Ls (FFs).
'       A subject may not have more than 5 pages.
'
'       Build the AI.HLP file using :
'
'               HELPBLD AI
'
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'%%%%%%%%%%        CopyRight 1990, Linx Data Terminals, Inc.       %%%%%%%%%%
'%%%%%%%%%%                  All Rights Reserved.                  %%%%%%%%%%
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

~C
~T Help Index
' 
~X Keystroke quick reference            |Keys
~X Adding LinxBASIC Functions           |LinxBASIC
~X Advanced variable operations         |Advanced
~X Branching (LOOPs and GOTOs)          |GOTO
~X Chaining multiple programs           |CHAIN
~X Conditional execution (IF)           |IF
~X Control Lines/LEDs                   |Control
'''''' ~X Special file operations                 |File I/O
~X Displaying on the screen             |Display
~X Downloading to LINX terminals        |Downloading
~X Field Attributes                     |Fields
~X Generating LinxBASIC programs        |Generating
~X Lookups to LINX files                |Lookups
~X Lookups to the host computer         |Host Lookups
~X Miscellaneous Operations             |OtherM
~X Numerical operations                 |Numerical
~X
~X
~X
~X
                                    
~X Other variable operations            |Other
~X ON event operations                  |On
~X Pausing execution (delay)            |Pause
~X Reading from the KEYBOARD            |Read KBD
~X Reading from the SENSOR port         |Read Devices
~X Saving/Loading personalities         |File
~X Searching files                      |Search
~X Sending to host or devices           |Sending
~X Setting Range values                 |Ranges
~X Sounds and noises                    |Sound
~X String (text) operations             |Transfer
~X Subroutines (gosub/return)           |Gosub
~X User LinxBASIC Functions             |User
~X Using timeouts                       |Timeouts
~X Variables                            |Vars
~X
~X
~X
~X

'----------------------------------------------------------------------------
~I General Help
~T Applications Interface General Help
'
The Application Interface (AI) is an interactive program
development tool for LINX products.

An AI developer trains a LINX terminal by interactively modifying
its current behavior until the desired functions are achieved.

Because the developer is working with the actual operation of
the program, instead of trying to relate the program's behavior
to a listing of program source code, development is both faster
and more intuitive than using traditional programming techniques.


Use F1 for the HELP index, Press PgDn for the next page


Getting Started

An AI Personality is an interactive computer program which you
build by selecting operations from the menu shown on the right 
side of your screen.  (Press ESC to see the menu, F1,PgDn to get
back here).  The operations are a list of instructions which
make the LINX perform various functions.  You must select which
operations to perform, and in what order.

The AI environment is a LINX SIMULATOR.  It allows you to try
out your program in an environment where you can have total
control over what happens and when.  After you have made your
program behave properly, you then generate a LinxBASIC program
which you load into your LINX network using the LIO utility
program.  



The AI screen makes it simple to see the results of your program
as it executes.  The screen is organized as follows :

SIMULATED LINX DISPLAY       �  PROGRAM STATUS
                             �
��������������������������������������������������������������
SIMULATED LINX INPUT         �        OPERATIONS MENU
AND OUTPUT DEVICES           �                         
 (access using Alt B, Alt H  � (use function keys or the
  Alt C, Alt A and Alt S)    �  highlighted letters to select)
��������������������������������������������������������������
CONTENTS OF VARIABLES YOUR PROGRAM IS USING 
��������������������������������������������������������������
THE PROGRAM OPERATIONS YOU HAVE ENTERED (use up and down)
��������������������������������������������������������������
STATUS AND HELP LINES


When you do a DISPLAY operation (main menu F2 or D) you will
be affecting the simulated display in the upper left corner of
your screen.  This shows exactly what will appear on the LINX
display when you run your program.

The SIMULATED LINX I/O DEVICES are used to see what your
program will do when it is executed.  

The VARIABLES are named storage locations in the LINX memory.
As your program runs, you can see the contents of the last few
variables that have been used.  

'----------------------------------------------------------------------------
~I Keys
~T Keystroke quick reference
'
Commonly used keys for controlling the Applications Interface    

Up Arrow  : Undo the previous line (if stepping) or up one line
Down Arrow: Execute the next line (if stepping) or down one line
INS       : Toggles between single step and stopped modes
ESC       : Stop running or abandon entry of the current operation
DEL       : Delete an operation (press it twice)
ENTER     : Edit the current operation

Alt R     : RUN the program starting at line 1 (ESC to stop)
Alt G     : Go (RUN) starting at this line
Alt U     : Undo back to this line
Alt E     : Full screen edit of the current program
Alt V     : List the variables in the program

The next page describes the simulated device input keystrokes

'
Simulated Device Input Keys

Alt B     : Simulate input from the badge reader (key entry)
Alt H     : Simulate input from the host computer (key entry)
Alt C     : Simulate input from the COM1 RS232 line (key entry)
Alt A     : Simulate input from the COM2 RS232 line (key entry)
Alt S     : Simulate toggling of the sense line

Alt F     : Select a DOS file to use for simulated inputs
Alt N     : Simulate the next input from the response file
Alt L     : Log device I/O to a disk file

The next page describes some other specialized keys

Special Purpose Keys

-         : Save a line to the copy buffer and delete it
+         : Save a line to the copy buffer
*         : Restore a line from the copy buffer
END       : Move to the end of the program
HOME      : Undo or Move to the start of the program

Alt I     : Set or clear the interrupt (breakpoint)
Alt D     : Toggles between 40 and 80 column display mode
����������������������������������������������������������������ͻ
�      These keys will only work when in full screen edit mode   �
�      or program is stopped.                                    �
�                                                                �
�  -         : Save a line to the copy buffer and delete it      �
�  +         : Save a line to the copy buffer                    �
�  *         : Restore a line from the copy buffer               �
�                                                                �
����������������������������������������������������������������ͼ


'----------------------------------------------------------------------------
~I LinxBASIC
~T Adding LinxBASIC Functions
'
LinxBASIC Functions are added to the Other Operations Menu by
modifying the file AiUser.BAS.  You may add up to 9 named
statements which will appear in a sub-menu named Additional
Operations.

The file AiUser.BAS contains commentary which describes what
must be done for your LinxBASIC source code to work properly
with the Applications Interface.

Note that you should also modify the "User" section of AI.HTX
and rerun HELPBLD so that you have on-line HELP for your added
functions.  See the file AI.HTX for more info.

'----------------------------------------------------------------------------
~I User
~T User LinxBASIC Functions
'
These functions have been added by users of AI. 

No user defined routines have been added at this time.

User must modify AI.HTX then run HELPBLD AI.HTX in order
to obtain on-line help for their added routines.

'**********************************************************
'*********      AiUSER.BAS modifiers should      **********
'*********    change this section!!!!!!!!!!      **********
'**********************************************************

'----------------------------------------------------------------------------
~I Pause
~T Pause Operation
'
You can pause your program for any number of seconds.  Use
this to leave a prompt on the display for long enough for
the user to see it.

Note that while you are testing your program on the PC, even
a pause as short as .1 seconds will take a full second to
execute.  When you generate your program, the downloaded
version will pause for the selected interval.

'----------------------------------------------------------------------------
~I GOTO
~T Branching (LOOPs and GOTOs)
'
A GOTO is a way of changing the order in which operations are
performed in your program.  A program which contains no GOTOs
will do one step after another until it reaches the last 
step.  Then it will stop.  Since programs for the LINX must
usually operate 24 hours a day and allow lots of interactions,
your program will include GOTOs.

Each GOTO specifies the LINE NUMBER/LABEL that the program
should go to when the GOTO operation is reached.  You can
either key in a line number or use the up and down arrow keys
to find where you would like to go to.  Press ENTER when you
have made your selection.

See the next page for a discussion of LABELS

USING LABELS

Line numbers work well for writing simple programs that you
will not have to change often.  For better structured programs
you should use LABELS.  A label is a name for a line in your
program.  The advantage is that the GOTO or GOSUB will have
a name that makes sense instead of a meaningless line number.

You can place a LABEL anywhere in your program using the
LABEL option in the OTHER OPERATIONS menu.  When you key
in a GOTO, you can use the label name instead of a line number.

When you are prompted for the line/label name, if you key in a
label name that does not currently exist, you will be asked if
you would like to put the label somewhere in your program.  This
is a nice shortcut for entering the labels.

'----------------------------------------------------------------------------
~I GOSUB
~T Subroutines (gosub/return)
'
A GOSUB is a way of calling a subroutine.  The subroutine can do
its job and then return to the line immediately after the GOSUB
statement.  This makes it easier to write programs that have the
kind of structure found in high level languages such as QuickBASIC.

When you write the subroutine that is called by the GOSUB statement,
you must be sure to put a RETURN operation at the last line of the
GOSUB.  If you execute a GOSUB without a RETURN you will get an
error message stating: "GOSUB encountered without a RETURN".

GOSUB and RETURN are found in the OTHER OPERATIONS menu.

'----------------------------------------------------------------------------
~I IF
~T Conditional execution (IF)
'
An IF statement is another way of changing the order or program
execution.  It allows you to GOTO a statement IF a variable
is in some state.  

The conditions you can check are :

       IF equals ...              IF not equal ...
       IF less than ...           IF greater than ...
       IF in a range ...          IF out of range ...
       IF starts with ...         IF ends with ...


The starts with and ends with are used to branch (GOTO) if the
variable starts or ends with some piece of text.  You will
usually type a quote (") in front of the text being checked.
If you have a variable name 


Example : Suppose that a badge is read that contains a "S",
is this is a supervisors badge?  You want your program to
do something special when such as badge is entered so you 
use :    
        IF IN starts with "S" then GOTO xxx

'----------------------------------------------------------------------------
~I ON
~T ON Event Operations
'
The ON event operation (in the OTHER MENU) is a way of branching 
to more than one place depending on the status of a previous 
READ operation.  The previous operation will be a READ SINGLE
KEYSTROKE to IN, READ KEY BADGE to IN, or other read.

The conditions you can test for are :

   ON Function xx  GOTO ...    ON key    xxxxx GOTO ...
   ON BADGE  INPUT GOTO ...    ON HOST   INPUT GOTO ...
   ON COM    INPUT GOTO ...    ON AUX    INPUT GOTO ...
   ON SENSOR INPUT GOTO ...

     ����������������������������������������������������Ŀ
     �                  *** Note ***                      �
     �  The destination variable must be the IN variable. �
     ������������������������������������������������������

This is a very useful operation.  If you're still confused
about it, put a few in your program immediately after a READ SINGLE
KEYSTROKE operation and see what it does.

'----------------------------------------------------------------------------
~I Pause
~T Pausing execution (delay)
'
Pausing program execution is accomplished by selecting one of the
following:

  F2: Pause for 1/4 second  - delays program for .25 of a second
  F3: Pause for 1/2 second  - delays program for .50 of a second
  F4: Pause for  1  second  - delays program for  1  second
  F5: Pause for  2  seconds - delays program for  2  seconds
  F6: Pause for  4  seconds - delays program for  4  seconds
  F7: Key in pause period   - Allow up to 32767 seconds, over 9 hours.

Delaying program execution is helpful when displaying a message to
the user by allowing them time to read the text before displaying
another message or clearing the display.

'----------------------------------------------------------------------------
~I Control
~T Control Lines/LEDs
'
CONTROL LINES:


If you have LinxBASIC V5.0 or higher your terminals can
be equipped with up to five (5) extra control lines. In previous
versions of AI the user could only access one (1) control line,
but with V1.3 you can now access all six (6) control lines.

When choosing an option dealing with the control line you will
be prompted as to which control line you'd like to effect.  There is
no way to determine if a control line is currently ON/OFF unless you
use variables to keep track of the state of any given control line.

LEDs:


When choosing an option dealing with the LEDs you will
be prompted as to which LED you'd like to effect.  There is
no way to determine if an LED is currently ON/OFF unless you
use variables to keep track of the state of any given LED.

'----------------------------------------------------------------------------
~I File I/O
~T Special file operations
'



  These operations have not been defined or implemented at this time!



'----------------------------------------------------------------------------
~I Display
~T Displaying on the screen
'
The Display Menu (F2) is used to display text, variables, date
or time on the screen.  It is also used to CLEAR the screen.

The size and format of each field is controlled by pressing + 
and -.  The arrow keys are used to position the field on the
screen.

Note that if you select a variable to display, make sure to use
+ to make the field as big as the data you want to display.  If
the field is too small then characters will be lopped off.

'----------------------------------------------------------------------------
~I Downloading
~T Downloading to LINX terminals
'
You use the program LIO to download your generated personality 
to your LINX terminals.  You can start LIO from the DOS command
line by typing LIO.  If your personality is named MYPROG then type:

        LIO
        P              - after terminals have come on-line.
        \AI\MYPROG

to download MYPROG to your network.

'----------------------------------------------------------------------------
~I Chain
~T Chaining multiple programs
'
If your program becomes too large to run as a single program 
then you will need to use CHAINING (in SPECIAL FILE OPERATIONS
in the OTHER MENU).  This is a method of having one program start
the execution of another.

In order to use chaining, you will need to put CHAIN operations
in the main program to start up your subprogram.  Then in the
subprogram, you will need a CHAIN that restarts the main program
when the operation is completed.

The chain operation does not save the contents of all user
variables so you will need to make use of GLOBAL variables
which are described on page 3,(press PgDn twice).

CHAINING REQUIREMENTS

Chaining requires LinxBASIC 1.4 or later.  If you do not have
this version then you will not be able to load the files. 

Note that to download the files, you must use the FILE DOWNLOAD
feature of LIO.EXE.  This is in the FILE SERVICES menu of LIO
and is NOT the same as PROGRAM DOWNLOAD.  Both your main program
and your subprograms must be loaded using FILE DOWNLOAD.

You can start the main program by making a small program containing
only the statement : CHAIN "your prog. name" and using the
Program Download option.

Alternatively you can start the main program using the Begin 
Program feature of LIO.  This feature is accessed from the
Supervisor menu and can be used to begin a chain file program. 

GLOBAL VARIABLES

Chained programs may make use of GLOBAL variables.  These are
variables whose value is accessible by all of the chained 
programs.  The names of the global variables must be placed
in a text file named "GLOBALS.DEF" in your program directory
using a text editor.  Put one variable name on each line. 

WARNING! Once you have created GLOBALS.DEF you may only ADD variable
names to the end of the list.  If you insert or delete old names
then you will damage your existing AI Personalities that use globals.

A maximum of 99 global variables may be defined.  Using a lot of
globals will slow down chaining in your programs.

'----------------------------------------------------------------------------
~I Generating
~T Generating LinxBASIC program
'
Once your AI personality behaves as you desire, you will need
to generate a LinxBASIC program that can be downloaded to 
the LINX network.  This program will behave almost exactly like
the personality you have been interacting with in the AI
environment.

'----------------------------------------------------------------------------
~I Lookups
~T Lookups to LINX files
'
A lookup is a method of verifying data against a file or
obtaining records from a file.   A text file must first be
created using a text editor or another program.

Lookups can be selected from the range menu of the Keyboard
Read menu or in the other sub-menu from the Move/Transfer/Add
menu.  You must specify both the file name to look in and
the offset of the key in the file records.  

����������������������������������������������������������Ŀ
� When using READ lookups, the record read from the file is�
� ALWAYS placed in the OUT variable.                       �
������������������������������������������������������������

See next page for more info.

The file that you perform lookups in must be downloaded to your
LINX before you download your AI personality.  This can be
done using the File/Chain download feature of LIO.  Once
the file is downloaded, you no longer need to have LIO
running for the lookups to operate.

See next page for local Lookup options.

�������������������  LOOKUP FILE SEARCHES ��������������������

Lookups being performed on files residing in the terminal,
"Lookup in LINX", can perform the lookup on the file
in one of two ways.  The options are as follows:
Sequential: The records are different lengths.
Binary    : The records are fixed length.

The sequential search is used for files where each line in the
file has a varying length, such as the file you're reading.  To
locate data in a sequential file you must read line after line
from the file until you locate what you're looking for.

Searching large files using this method requires too much time
to be beneficial, therefore we included a "Binary search"
option that can be used on large, fixed length files.

Next page for binary option.

The binary search option is used on files where each line contains
the same number of characters, regardless of what line in the file
you're on.  Such files are often referred to as "Random Files",
meaning you can randomly select a record from anywhere in the file.
This file structure allows the use of a "Binary search" algorithm
which greatly reduces search times on large files.

Random files, fixed length records, MUST have a carriage-return
or a carriage-return-line-feed sequence as the last one or two
characters.  This is required to facilitate loading the file to
the terminal!  Random files must also be sorted, by the field that
you will be performing your lookups on, in ascending order, which
means smallest to largest.  This is the order that the search
algorithm expects to find the data in.  So if your not getting any
matches be sure that your file is sorted correctly.  Also be sure
you provide the correct information to the lookup operation, such
as field and record length.
Don't forget the CR/CRLF as part of the overall record length.

'----------------------------------------------------------------------------
~I Search
~T Searching files
'
�������������������  LOOKUP FILE SEARCHES ��������������������

Lookups being performed on files residing in the terminal,
"Lookup in LINX", can perform the lookup on the file
in one of two ways.  The options are as follows:
Sequential: The records are different lengths.
Binary    : The records are fixed length.

The sequential search is used for files where each line in the
file has a varying length, such as the file you're reading.  To
locate data in a sequential file you must read line after line
from the file until you locate what you're looking for.

Searching large files using this method requires too much time
to be beneficial, therefore we included a "Binary search"
option that can be used on large, fixed length files.


Next page for binary option.

The binary search option is used on files where each line contains
the same number of characters, regardless of what line in the file
you're on.  Such files are often referred to as "Random Files",
meaning you can randomly select a record from anywhere in the file.
This file structure allows the use of a "Binary search" algorithm
which greatly reduces search times on large files.

Random files, fixed length records, MUST have a carriage-return
or a carriage-return-line-feed sequence as the last one or two
characters.  This is required to facilitate loading the file to
the terminal!  Random files must also be sorted, by the field that
you will be performing your lookups on, in ascending order, which
means smallest to largest.  This is the order that the search
algorithm expects to find the data in.  So if your not getting any
matches be sure that your file is sorted correctly.  Also be sure
you provide the correct information to the lookup operation, such
as field and record length.
Don't forget the CR/CRLF as part of the overall record length.

'----------------------------------------------------------------------------
~I Host Lookups
~T Lookups to the host computer
'
Host lookups are lookups that are performed by your host
computer.  LIO must be installed and RUNNING on your
host computer in order for host lookups to work.

This allows a LINX program to always have up to the minute
information about a file which is being maintained by the
host computer. 

����������������������������������������������������������Ŀ
� When using READ lookups, the record read from the file is�
� ALWAYS placed in the OUT variable.                       �
������������������������������������������������������������


'----------------------------------------------------------------------------
~I OtherM
~T Miscellaneous (Other Operations)
'
This menu allows access to the Beep, Pause and On Event menus
as well as the following operations:

F4: Chain to personality - Lets current personality run another one

F5: Comment line         - Place a comment in your personality

F6: Gosub                - Goto line #/lable.  Use with F7: Return

F7: Return               - Returns to line following Gosub

F8: Label                - Place a label for use with Goto / Gosub


'----------------------------------------------------------------------------
~I Numerical
~T Numerical operations
'
The Move/Transfer/Add menu gives you access to the AI functions
for changing the contents of variables.

You are first asked for a destination variable which is the
one which is modified by the operation.  You are then prompted
for additional values where needed.

See the next page for a description of these functions


MOVE    This is used to copy the contents of one variable to
        another, or to set a variable to a value or text string.
        To clear a variable set it to "".
      
APPEND  Appends a variable or text string onto the end of
        another.  This is often used in building a record
        for sending to the host.

ADD and SUBTRACT

        These will convert the variable into a number and
        add or subtract a value.  

TRANSFER    See  String (text) operations

OTHER       See Other variable operations

'----------------------------------------------------------------------------
~I Transfer
~T String (text) operations
'
Transfers (See the Move/... menu) are the most powerful way of
manipulating variables.  These allow you to edit the contents
of a variable in the same way you can edit the screen using
the display operations.  

Variables, text, date and time can be positioned within the 
variable (this is shown on the second to the last line of
the display).  + and - will change the field size and format
while left and right arrows are used for positioning.

It is best to be in single step mode while entering transfers
since that makes it easiest to see the relationship between 
fields that are being formatted.

Transfers are fixed length operations that always pad with spaces.
If this is not what you want, you may want to use Append or the
advanced operations.

Make sure that when you position a variable using a transfer,
make the field big enough (using +).

'----------------------------------------------------------------------------
~I Other
~T Other Variable Operations
'
These operations are accessed from the Move/... menu. 

MULTIPLY and DIVIDE will operate on any variable.  The
DIVIDE can result in a floating point number.  

NEGATE is use to alter the sign of a value.

MAKE NUMERIC will just get rid of any non-numeric parts
to a text value.  For example lets say the variable SUBTOT
is equal to a text string containing "123.45ABC" this
operation would make a string containing "123.45".

MAKE INTEGER will remove the fractional part of a number.

LOOKUP is described under Lookups

'----------------------------------------------------------------------------
~I Advanced 
~T Advanced Variable Operations
'
These operations are accessed from the Move/... menu. 

LENGTH returns the number of characters in a variable

LEFT and RIGHT isolate the left or right n characters from
a variable.  

MIDDLE is used to grab a chunk from the middle.  Warning!
If you use middle on a variable that is too short and try
to get characters that are not present, AI may abort.

TRIM removes spaces from the front and back of a variable

UPPER CASE converts a variable into upper case.

CHAR and ASCII converts the exchange character values into
ASCII and back to characters.

MODULUS is the positive remainder from a division operation.

'----------------------------------------------------------------------------
~I Ranges
~T Setting Range values
'

The Range selection menu facilitates the setting of:


  Smallest valid number - Smallest number allowed for numeric field

  Biggest  valid number - Largest  number allowed for numeric field 

  Valid characters      - Allowable characters in an alpha field

  Lookup in file        - SEE LOOKUPS


'----------------------------------------------------------------------------
~I Read KBD
~T Reading from the KEYBOARD
'
The Keyboard Read menu is used to read fields and characters from
the keyboard.  Optionally you can also read from the badge reader.

Since there are a lot of options that can be selected for
reading, you will have to press ENTER until the main menu is
shown in order to store a READ operation.  Keep an eye on the
help line at the bottom of the screen.

The READ NUMBER and READ ALPHA allow you to position an input
field on the display.  A numeric field is one which is always
converted into a number.

If you use READ A SINGLE KEYSTROKE then the key number will be
placed into the input variable.  Some of the more important
numbers are :

      F1=225     F2=226    F3=227    F4=228    F5 =229
      F6=230     F7=231    F8=232    F9=233    F10=234

      ENTER=13    IN=43    OUT=45     CLEAR=127   SPACE=32
      EXIT=159    UP=24    LEFT=8     DOWN=25     RIGHT=26

You can use IF EQUALS to select where to GO TO based on the
input key.   

Special options which can be selected for reading fields
are :

   AUTOEXIT : When the operator has entered as many
              characters as are in the field, then the
              read is completed.

   DEFAULT  : The previous value of the variable is shown
              instead of displaying a blank field.

   DONT CLEAR : Prevents the field from being cleared on
                the first character typed (see default).
 
   ZERO FILL  : Numeric field is right justified with zeros.

   PASSWORD   : ***'s are shown instead of characters typed


       ��������������������������������������������������Ŀ
       �  REMEBER THESE ARE ONLY VALID ON FIELDED READS!  �
       ���Ŀ ���������������������������������������Ŀ ����        
������������ ����������������������������������������� �����������Ŀ
�  On fielded reads you can select upper and lower numeric ranges  �
�  for the input value.  If the value is out of range then         �
�  * OUT OF RANGE * is displayed and the user is prompted again.   �
�                                                                  �
�  VALID CHARACTERS lets you specify a set of characters that can  �
�  be used during entry.  Anything not in the set you specify will �
�  not be displayed.                                               �
��������������������������������������������������������������������


The LOOKUPs allow you to restrict input to values which match
those in a file.  See Lookups.

'----------------------------------------------------------------------------
~I Read Devices
~T Reading from the SENSOR port

If you have LinxBASIC V5.0 or higher your terminals can
be equipped with up to five (5) extra sense lines. In previous
versions of AI the user could only access the one default
sense line, but now AI allows access to all six sense lines.

When choosing any operation dealing with a read from the SENSE
port, the user will be prompted for the sense line to read and
what variable to store the state of the sense line in.  After the
user has completed the operation the program will then prompt
as to whether or not they want the duration and transition counts for
the specified sense line.  If the user responds "Y" the program
will prompt for the variable names to store the values in.

The state of the sense line is either zero (0) for OFF or one (1)
for ON.  The duration is the amount of time (in 100ths of a second)
that the sense line was in the ON state since the last time this
sense line was read.  Transitions are the number of ON/OFF transitions
since the last time this sense line was read.

'----------------------------------------------------------------------------
~I Sending
~T Sending to host or devices

   ������������������������������������������������������������Ŀ
   � NOTICE: ** Variable contents are deleted after performing  �
   �            any send operation!!!                           �
   ��������������������������������������������������������������

Send var. indirectly to HOST is used to place data into the QUE
device which stores data for transmission to the HOST.  If the
network is not online to the HOST then this data is placed in a
FIFO (First In First Out) que for future transmission to the HOST
when the network does come online.

Send var. directly to HOST (NET) is used to bypass the QUE device
and send the data directly to the HOST using the NET device.  If
the network is not online to the HOST then this operation could
cause a pause in your program's execution until the HOST can take
the data record from the NET device.  This may not happen if you
send only one data record and the HOST is not online, but if you
send two or more records your program will pause until the NET
device can be cleared.



   ������������������������������������������������������������Ŀ
   � NOTICE: ** Variable contents are deleted after performing  �
   �            any send operation!!!                           �
   ��������������������������������������������������������������

Options F4 and F5 are used to send data out the two communications
ports available, COM1 and COM2 (AUX).  The data is sent out the
selected com port and is followed by a carriage return.  If you do
not want a carriage return to be sent out after the data then you
will have to modify AI, re-compile and re-link it in order to
remove the carriage return.


NOTICE: When using option F4 Send variable to COM port, please be
aware that a terminal cannot be acting as a MASTER and communicating
with the HOST out the COM1 port for this operation to work correctly.
You may even cause the loss of a record being transmitted to the
HOST, if you send data out the COM1 port while the terminal is also
trying to communicate with the HOST.

~I Fields
~T Field Attributes
'
Special options which can be selected for reading fields
are :

   AUTOEXIT : When the operator has entered as many
              characters as are in the field, then the
              read is completed.

   DEFAULT  : The previous value of the variable is shown
              instead of displaying a blank field.

   DONT CLEAR : Prevents the field from being cleared on
                the first character typed (see default).
 
   ZERO FILL  : Numeric field is right justified with zeros.

   PASSWORD   : ***'s are shown instead of characters typed


On fielded reads you can select upper and lower numeric ranges
for the input value.  If the value is out of range then
* OUT OF RANGE * is displayed and the user is prompted again.

     ��������������������������������������������������Ŀ
     �  REMEBER THESE ARE ONLY VALID ON FIELDED READS!  �
     ����������������������������������������������������        
        
VALID CHARACTERS lets you specify a set of characters that can
be used during entry.  Anything not in the set you specify will
not be displayed.


The LOOKUPs allow you to restrict input to values which match
those in a file.  See Lookups.

'----------------------------------------------------------------------------
~I Sound
~T Sounds and Noises 
'
The LINX can produce sounds in the range of 100 to 3000 hz.
A low sounding tone is around 200 hz.  A high pitched sound is
above 2000 hz.

Note that the loudness of the sound will vary depending on the
frequency.  Your PC will only produce a crude approximation of
the actual sound produced by the LINX.

Use the Other Operations menu to select a sound.  

'----------------------------------------------------------------------------
~I File
~T Saving/Loading personalities
'
The F10 menu is used for saving or loading your personalities.

A personality name is an 8 character name that contains only
the letters A to Z and 0 to 9.  The name is used to create
DOS files which contain the personality.

Select personality names that will make them easy to remember.
You can list all of your personalities from the DOS prompt by
typing :   DIR *.AIP   while in the \AI directory.

See also Downloading and Generating.

'----------------------------------------------------------------------------
~I Timeouts
~T Using timeouts
'
Timeouts are a way of stopping a read operation before the 
operation has finished.  If your program is waiting for the
user to enter his password and the user walks off an leaves
the LINX, you might want your program to restart after a
few seconds or minutes.

When you select a timeout you are also asked to specify where
you want to go to if the timeout occurs.  Use the arrow keys to
select where the timeout should go to.

'----------------------------------------------------------------------------
~I Vars
~T Variables
'
Variables are named storage locations.  They can each hold up to
80 characters of information.  When you start AI, you are given
the variables "IN", "OUT", "ERR", "NET", "COM", and "AUX".  Any
time you are asked for a variable name, you can key in one of the
built in names, or make up one of your own.  Use Alt V to see
all of your variables.

Even though the above text states that each variable can only hold
80 characters, this is refering to entering data into the variable
at design time from AI.  You can move multiple variable contents
to a single variable for up to 250 characters.

By using variables, you can move data from place to place such
as from the badge reader to the display.  

The names of the default variables give you a hint to what you
can use them for, but only a hint.  The name of a variable does
not in any way affect what you can use it for.  A name is just
a way of referring to the same group of characters.

Variable names must not start with " or a digit.  It is good
practice to use just one word.  Upper and lower case does
not matter.

In many cases where you are asked for a variable name, you can
key in a number or a string.  A string is characters enclosed
in quotes, and are considered string constants.

~Z
