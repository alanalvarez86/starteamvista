' AiEnvPc.BI - Basic Include file
'
DEFINT A-Z

'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@                     Sub and Function Declarations                       @@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

DECLARE SUB      OpenLINX       ()
DECLARE FUNCTION TestEvent%     ( mask% )
DECLARE SUB      SaveEvent      ( e% )
DECLARE FUNCTION ReadEvent%     ( mask% )
DECLARE SUB      SetDTR         ( n%, s% )
DECLARE SUB      SetRTS         ( n%, s% )
DECLARE FUNCTION GetDSR%        ( n% )
DECLARE SUB      SendCom        ( n%, s$ )
DECLARE SUB      SetLINE        ( s% )
DECLARE SUB      Send           ( s$ )
DECLARE SUB      SendQue        ( s$ )
DECLARE SUB      SetLED         ( n%, state% )
DECLARE FUNCTION GetTIME$       ()
DECLARE FUNCTION GetDATE$       ()

'End AiEnvPc.BI



