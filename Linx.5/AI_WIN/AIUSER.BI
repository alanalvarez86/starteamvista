' AiUser.BI - Basic Include file
'
DEFINT A-Z

'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@                               Common Section                            @@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'
' ----------  Environment Status Variables ----------
COMMON SHARED /AiUser/ UserName1  AS STRING ' Menu names
COMMON SHARED /AiUser/ UserName2  AS STRING
COMMON SHARED /AiUser/ UserName3  AS STRING
COMMON SHARED /AiUser/ UserName4  AS STRING
COMMON SHARED /AiUser/ UserName5  AS STRING
COMMON SHARED /AiUser/ UserName6  AS STRING
COMMON SHARED /AiUser/ UserName7  AS STRING
COMMON SHARED /AiUser/ UserName8  AS STRING
COMMON SHARED /AiUser/ UserName9  AS STRING

'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@                     Sub and Function Declarations                       @@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'
DECLARE SUB      AiUserInit     ()
DECLARE SUB      User1          ()
DECLARE SUB      User2          ()
DECLARE SUB      User3          ()
DECLARE SUB      User4          ()
DECLARE SUB      User5          ()
DECLARE SUB      User6          ()
DECLARE SUB      User7          ()
DECLARE SUB      User8          ()
DECLARE SUB      User9          ()

'End AiUser.BI

