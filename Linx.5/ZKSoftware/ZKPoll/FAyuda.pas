unit FAyuda;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ExtCtrls,
     ZBaseDlgModal;

type
  TShowAyuda = class(TZetaDlgModal)
    Exportar: TBitBtn;
    SaveDialog: TSaveDialog;
    Ayuda: TMemo;
    procedure ExportarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ShowAyuda: TShowAyuda;

implementation

uses ZetaDialogo,
     ZetaCommonClasses,
     ZetaClientTools;

{$R *.DFM}

procedure TShowAyuda.ExportarClick(Sender: TObject);
var
   sFileName: String;
   oCursor: TCursor;
begin
     inherited;
     sFileName := Format( '%sTransformadorAyuda.txt', [ ExtractFilePath( Application.ExeName ) ] );
     with SaveDialog do
     begin
          FileName := sFileName;
          InitialDir := ExtractFilePath( sFileName );
          if Execute then
          begin
               sFileName := FileName;
               oCursor := Screen.Cursor;
               Screen.Cursor := crHourglass;
               try
                  Ayuda.Lines.SaveToFile( sFileName );
               finally
                      Screen.Cursor := oCursor;
               end;
               if ZetaDialogo.zConfirm( Caption, 'El Archivo ' + sFileName + ' Fu� Creado' + CR_LF + '� Desea Verlo ?', 0, mbYes ) then
               begin
                    ZetaClientTools.ExecuteFile( 'NOTEPAD.EXE', sFileName, ExtractFileDir( sFileName ), SW_SHOWDEFAULT );
               end;
          end;
     end;
end;

end.
