unit DInterfase;

interface

uses
    Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
    Db, DBClient, ZetaClientDataSet, Variants, DZetaServerProvider, ZetaTipoEntidad,
    ZetaTipoEntidadTools, ZetaCommonLists, ZetaCommonClasses,
    {$ifdef DOS_CAPAS}DServerRecursos{$else}Recursos_TLB{$endif}, zkemkeeper_TLB, OleServer;

type
  TArrayListaIP = Array[0..23] of String;
  eTipoMovimiento = ( tmEnvia, tmAlmacena ); // Tipos de procesos.
  TdmInterfase = class(TDataModule)
    cdsBitacora: TZetaClientDataSet;
    cdsListaEmpleados: TZetaClientDataSet;
    cdsLista: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FHuboCambios, FProcesandoThread, FHuboErrores: Boolean;
    FDatos: TStrings;
    oZetaProviderCliente: TdmZetaServerProvider;
    FTipoMovimiento: eTipoMovimiento;
    FListaIP: TArrayListaIP;
    FDigitoEmpresa: String;
    FZK: TCZKEM;
{$ifdef DOS_CAPAS}
    function GetServerRecursos : TdmServerRecursos;
    property ServerRecursos :TdmServerRecursos read GetServerRecursos;
{$else}
    FServidor : IdmServerRecursosDisp;
    function GetServerRecursos : IdmServerRecursosDisp ;
    property ServerRecursos : IdmServerRecursosDisp read GetServerRecursos;
{$endif}
    function GetHeaderTexto(const iEmpleado: Integer; const iLinea: Integer): string;
    procedure ReportaProcessDetail(const iFolio: Integer);
    function GetSQLScript( iConsulta: Integer ): String;
    function TieneDigito: Boolean;
    procedure ZKProcesa( sTerminal: string );
    procedure AlmacenaHuellas;
    procedure LeeDatosEmpleados;
  protected
    { Protected declarations }
  public
    { Public declarations }
    procedure Procesa;
    procedure HandleProcessEnd(const iIndice, iValor: Integer);
    procedure EscribeError(const sMensaje: string; const iEmpleado: Integer = 0; const iLinea: Integer = 0; lBandera: Boolean = True );
    procedure EscribeBitacora(const sMensaje: string; const iEmpleado: Integer = 0; const iLinea: Integer = 0 );
    property TipoMovimiento: eTipoMovimiento read FTipoMovimiento write FTipoMovimiento;
    property ListaIP: TArrayListaIP read FListaIP write FListaIP;
  end;

var
  dmInterfase: TdmInterfase;

implementation

uses dProcesos,
     dCliente,
     dRecursos,
     dTablas,
     dCatalogos,
     dConsultas,
     dGlobal,
     dSistema,
     ZGlobalTress,
     FTressShell,
     ZAsciiTools,
     ZetaAsciiFile,
     ZBaseThreads,
     ZBaseSelectGrid,
     ZetaCommonTools,
     ZetaServerTools,
     dSuperAscii,
     StrUtils,
     ZReconcile,
     FAutoClasses,
     ZetaDialogo, OleCtrls;

const
    // Scripts de SQL.
    K_CONSULTA_DIGITO = 1;

{$R *.DFM}

{ TdmInterfase }
procedure TdmInterfase.DataModuleCreate(Sender: TObject);
begin
     oZetaProviderCliente := DZetaServerProvider.GetZetaProvider( Self );
     FDatos := TstringList.Create;
end;

procedure TdmInterfase.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FDatos );
     DZetaServerProvider.FreeZetaProvider( oZetaProviderCliente );
end;

{ Manejo de Fin de Proceso - Bit�cora }
procedure TdmInterfase.HandleProcessEnd( const iIndice, iValor: Integer );
var
   oProcessData: TProcessInfo;
   function GetResultadoProceso: string;
   begin
        case oProcessData.Status of
             epsOK: Result := '** Proceso terminado **';
             epsCancelado: Result := '** Proceso cancelado **';
             epsError, epsCatastrofico: Result := '** Proceso con errores **';
        else
             Result := ZetaCommonClasses.VACIO;
        end;
   end;
begin
     oProcessData := TProcessInfo.Create( nil );
     try
        with dmProcesos do
        begin
             with FListaProcesos do
             begin
                  try
                     with LockList do
                     begin
                          if ( iIndice >= 0 ) and ( iIndice < Count ) then
                             oProcessData.Assign( TProcessInfo( Items[ iIndice ] ) )
                          else
                              raise Exception.Create( '�ndice de lista de procesos fuera de rango ( ' + IntToStr( iIndice ) + ' )' );
                     end;
                  finally
                         UnlockList;
                  end;
             end;
             ReportaProcessDetail( oProcessData.Folio );
        end;
        EscribeBitacora( GetResultadoProceso );
     finally
            FreeAndNil( oProcessData );
            FProcesandoThread := FALSE;      // Mientras se encuentra la forma de manejar los threads
     end;
end;

procedure TdmInterfase.ReportaProcessDetail( const iFolio: Integer );
const
     K_MENSAJE_BIT = 'Fecha movimiento: %s - %s - %s';
var
   sMensaje: string;
begin
     with dmConsultas do
     begin
          GetProcessLog( iFolio );
          with cdsLogDetail do
          begin
               if ( not IsEmpty ) then
               begin
                    First;
                    while ( not EOF ) do
                    begin
                         sMensaje := Format( K_MENSAJE_BIT, [ ZetaCommonTools.FechaCorta( FieldByName( 'BI_FEC_MOV' ).AsDateTime ),
                                                              FieldByName( 'BI_TEXTO' ).AsString, FieldByName( 'BI_DATA' ).AsString ] );
                         case eTipoBitacora( FieldByName( 'BI_TIPO' ).AsInteger ) of
                              tbNormal, tbAdvertencia : EscribeBitacora( sMensaje, FieldByName( 'CB_CODIGO' ).AsInteger );
                              tbError, tbErrorGrave: EscribeError( sMensaje, FieldByName( 'CB_CODIGO' ).AsInteger );
                         end;
                         Next;
                    end;
               end;
          end;
     end;
end;

{ Utilerias de Bit�cora }
function TdmInterfase.GetHeaderTexto( const iEmpleado: Integer; const iLinea: Integer ): string;
const
     K_HEADER_EMPLEADO = '%sempleado #%d: ';
var
   sFechaHora: String;
begin
     Result := VACIO;
     sFechaHora := '(' + FechaCorta( date ) + '-' + FormatDateTime( 'hh:mm', Now ) + '): ';
     if( iLinea > 0 )then
         sFechaHora := sFechaHora + Format( 'l�nea #%d, ', [ iLinea ] );

     if ( iEmpleado > 0 ) then
        Result := Format( K_HEADER_EMPLEADO, [ sFechaHora, iEmpleado ] )
     else
         Result := sFechaHora;
end;

procedure TdmInterfase.EscribeBitacora(const sMensaje: string; const iEmpleado: Integer = 0; const iLinea: Integer = 0 );
begin
     { Se cambia la bandera FHuboCambios para indicar que existen cambios en la informaci�n de los empleados }
     if ( not FHuboCambios ) and ( iEmpleado > 0 ) then
        FHuboCambios := TRUE;
     TressShell.EscribeBitacora( GetHeaderTexto( iEmpleado, iLinea ) + sMensaje );
     Application.ProcessMessages;
end;

procedure TdmInterfase.EscribeError(const sMensaje: string; const iEmpleado: Integer = 0; const iLinea: Integer = 0; lBandera: Boolean = True );
begin
     if lBandera then
     begin
          { Se cambia la bandera FHuboErrores para indicar existen errores en el proceso }
          if ( not FHuboErrores ) then
             FHuboErrores := TRUE;
     end;
     TressShell.EscribeError( GetHeaderTexto( iEmpleado, iLinea ) + sMensaje );
     Application.ProcessMessages
end;

{ Procesa archivo }
procedure TdmInterfase.Procesa;
var
  i: Integer;
begin
     i := 0;
     // Intenta conectar a equipo
     EscribeBitacora( '>> Inicia proceso <<' );
     if TieneDigito then
     begin
          while( i < Length( FListaIP ) )do
          begin
               if StrLleno( FListaIP[ i ] )then
               begin
                    with TressShell.ParameterList do
                    begin
                         AddString( 'IP', FListaIP[ i ] );
                         AddString( 'DigitoEmpresa', FDigitoEmpresa );
                         AddInteger( 'Operacion', Ord( FTipoMovimiento ) );
                         AddDate( 'Fecha', dmCliente.FechaDefault );
                    end;

                    ZKProcesa( FListaIP[ i ] ); // Se env�a la terminal a procesar.

               end
               else
                   Break; // Se sale del ciclo, ya que ya no debe haber registros v�lidos en el arreglo.
               Inc( i );
          end;
     end
     else
         EscribeBitacora( '>>> No se cuenta con d�gito de empresa (num�rico) en la empresa seleccionada. No se continua con el proceso <<<' );

     if( FHuboErrores )then
         EscribeBitacora( '>> Finaliza proceso con errores <<' )
     else
         EscribeBitacora( '>> Finaliza proceso <<' );
end;

// Regresa el texto de la consulta indicada
function TdmInterfase.GetSQLScript(iConsulta: Integer): String;
begin
     case iConsulta of
          {$ifdef DOS_CAPAS}
          K_CONSULTA_DIGITO: Result := 'select CM_DIGITO from COMPANY where CM_CODIGO=''%s''';
          {$else}
          K_CONSULTA_DIGITO: Result := 'select CM_DIGITO from V_COMPANY where ( CM_CODIGO = %s )';
          {$endif}
          else
              Result := '';
     end;
end;

// Se verifica si existe d�gito de empresa.
function TdmInterfase.TieneDigito: Boolean;
begin
     Result := False;
     FDigitoEmpresa := '0';
     try
        {$ifdef DOS_CAPAS}
        with dmConsultas.cdsQueryGeneral do
        begin
             Data := oZetaProviderCliente.OpenSQL( oZetaProviderCliente.Comparte, Format( GetSQLScript( K_CONSULTA_DIGITO ), [ dmCliente.Compania ] ), True );
             Conectar;
             if Not IsEmpty then
             begin
                  Result := ( StrToIntDef( FieldByName( 'CM_DIGITO' ).AsString, -1 ) >= 0 );
                  if Result then
                     FDigitoEmpresa := FieldByName( 'CM_DIGITO' ).AsString;
             end;
        end;
        {$else}
        with dmConsultas do
        begin
             SQLText := Format( GetSQLScript( K_CONSULTA_DIGITO ), [ EntreComillas( dmCliente.Compania ) ] );
             cdsQueryGeneral.Refrescar;
             if Not cdsQueryGeneral.IsEmpty then
             begin
                  Result := ( StrToIntDef( cdsQueryGeneral.FieldByName( 'CM_DIGITO' ).AsString, -1 ) >= 0 );
                  if Result then
                     FDigitoEmpresa := cdsQueryGeneral.FieldByName( 'CM_DIGITO' ).AsString;
             end;
        end;
        {$endif}
     except
           on Error: Exception do
              EscribeError( Format( 'Se gener� un error al consultar el d�gito de empresa: %s', [ Error.Message ] ) );
     end;
end;

procedure TdmInterfase.ZKProcesa( sTerminal: string );
const
     K_PUERTO_DEFAULT = 4370;
begin
     try
        FZK := TCZKEM.Create( Self );
        try
           EscribeBitacora( Format( '>>> Preparando conexi�n con Terminal ZK(%s) <<<', [ sTerminal ] ) );
           if FZK.Connect_net( sTerminal, K_PUERTO_DEFAULT ) then
           begin
                try
                   EscribeBitacora( Format( '>>> Conexi�n con Terminal ZK(%s) exitosa <<<', [ sTerminal ] ) );
                   if( FTipoMovimiento = tmEnvia )then
                   begin
                        EscribeBitacora( '>>> Inicia proceso de env�o de datos a Terminal ZK <<<' );
                        // Obtiene lista
                        try
                           cdsListaEmpleados.Data := ServerRecursos.GetListaEmpleadosZK( dmCliente.Empresa, TressShell.ParameterList.VarValues );

                           // Envia datos a Terminal.
                           LeeDatosEmpleados;

                           EscribeBitacora( '>>> Finaliza proceso de env�o de datos a Terminal ZK' );
                        except
                              on Error: Exception do
                                 EscribeError( Format( 'Se gener� un error al obtener la lista de empleados: %s', [ Error.Message ] ) );
                        end;
                   end
                   else //tmAlmacena
                   begin
                        // Almacena datos a estructura Tress.
                        EscribeBitacora( '>>> Inicia proceso de almacenado de huellas <<<' );
                        AlmacenaHuellas;
                        EscribeBitacora( '>>> Finaliza proceso de almacenado de huellas <<<' );
                   end;
                finally
                       FZK.Disconnect;
                       EscribeBitacora( Format( '>>> Desconexi�n con Terminal ZK(%s) exitosa <<<', [ sTerminal ] ) );
                end;
           end
           else
               EscribeError( Format( 'Conexi�n con Terminal ZK(%s) no exitosa', [ sTerminal ] ) );
        finally
               FreeAndNil( FZK );
        end;
     except
           on Error : Exception do
              EscribeError( Format( 'Se gener� un error al preparar la conexi�n: %s', [ Error.Message ] ) );
     end;
end;

procedure TdmInterfase.LeeDatosEmpleados;
var
   sEmpDig, sNom, sPwd: WideString;
   sFiltroEmp: String;
   iPri, iFlag, iContador: integer;
   lEna: WordBool;
begin
     iFlag := 0;
     iContador := 0;
     try
        if not cdsListaEmpleados.IsEmpty then
        begin
             with cdsListaEmpleados do
             begin
                  First;
                  sFiltroEmp := VACIO;
                  // Recorre los registros de los empleados.
                  while Not EoF do
                  begin
                       sEmpDig := FDigitoEmpresa + FieldByName( 'CB_CODIGO' ).AsString;
                       if( ( FieldByName( 'CB_ACTIVO' ).AsString = K_GLOBAL_SI ) or ( FieldByName( 'CB_FEC_BAJ' ).AsDateTime > Now ) )then
                       begin
                            if( FZK.SSR_SetUserInfo( 1, sEmpDig, FieldByName( 'PRETTYNAME' ).AsString, VACIO, 0, True ) )then
                                EscribeBitacora( Format( '>>> Se envi� ID(%d) del empleado a Terminal ZK', [ StrToInt( sEmpDig ) ] ), StrToInt( FieldByName( 'CB_CODIGO' ).AsString ) );
                       end
                       else
                       begin
                            if( FZK.SSR_GetUserInfo( 1, sEmpDig, sNom, sPwd, iPri, lEna ) )then
                            begin
                                 // En caso de existir huellas del usuario, las borra.
                                 FZK.SSR_DeleteEnrollData( 1, sEmpDig, 11 );

                                 // Borra el usuario y su contrase�a.
                                 if( FZK.SSR_DeleteEnrollData( 1, sEmpDig, 12 ) )then
                                     EscribeBitacora( Format( '>>> Se elimin� ID(%d) del empleado en Terminal ZK <<<', [ StrToInt( sEmpDig ) ] ), StrToInt( FieldByName( 'CB_CODIGO' ).AsString ) );
                            end;
                       end;
                       // Maximo de empleado por Clausula IN seran 1000 Empleados
                       if ( iContador = 1000 ) then
                       begin
                            sFiltroEmp := ConcatString( sFiltroEmp, FieldByName( 'CB_CODIGO' ).AsString, ',' );
                            with cdsLista do
                            begin
                                 EscribeBitacora( '>>> Buscando huellas a enviar a Terminal ZK <<<' );
                                 if StrLleno( sFiltroEmp )then
                                 begin
                                      try
                                         Data := ServerRecursos.ConsultaZK( dmCliente.Empresa, dmCliente.FechaDefault, sFiltroEmp );//oZetaProvider.OpenSQL( Empresa, Format( GetScript( eConsultaZK ), [ DateToStrSQLC( dFecha ), sFiltroEmp ] ), TRUE );
                                         First;
                                         while Not EoF do
                                         begin
                                              sEmpDig := FDigitoEmpresa + FieldByName( 'CB_CODIGO' ).AsString;
                                              // Esta consulta ya contiene el d�gito de empresa antepuesto.
                                              //if( FZK.SSR_SetUserTmpStr( 1, sEmpDig, FieldByName( 'NO_HUELLA' ).AsInteger, FieldByName( 'HUELLA' ).AsString ) )then  // Version 9
                                              if( FZK.SetUserTmpExStr( 1, sEmpDig, FieldByName( 'NO_HUELLA' ).AsInteger, iFlag, FieldByName( 'HUELLA' ).AsString ) )then
                                                  EscribeBitacora( Format( '>>> Se envi� huella (%d) del ID(%d) del empleado a Terminal ZK <<<', [ FieldByName( 'NO_HUELLA' ).AsInteger, StrToInt( sEmpDig ) ] ), StrToInt( FieldByName( 'CB_CODIGO' ).AsString ) );
                                              Next;
                                         end;
                                      except
                                            on Error : Exception do
                                               EscribeError( Format( 'Se gener� un error al consultar los empleados de la empresa: %s', [ Error.Message ] ) );
                                      end;
                                 end;
                            end;
                            iContador := 0;
                            sFiltroEmp := VACIO;
                       end
                       else
                       begin
                            sFiltroEmp := ConcatString( sFiltroEmp, FieldByName( 'CB_CODIGO' ).AsString, ',' );
                            Inc( iContador );
                       end;
                       Next;
                  end;
             end;

             // Si queda informacion por enviar al filtro de empleado
             if StrLleno( sFiltroEmp )then
             begin
                  with cdsLista do
                  begin
                       EscribeBitacora( '>>> Buscando huellas a enviar a Terminal ZK <<<' );
                       try
                          Data := ServerRecursos.ConsultaZK( dmCliente.Empresa, dmCliente.FechaDefault, sFiltroEmp );//oZetaProvider.OpenSQL( Empresa, Format( GetScript( eConsultaZK ), [ DateToStrSQLC( dFecha ), sFiltroEmp ] ), TRUE );
                          First;
                          while Not EoF do
                          begin
                               sEmpDig := FDigitoEmpresa + FieldByName( 'CB_CODIGO' ).AsString;
                               // Esta consulta ya contiene el d�gito de empresa antepuesto.
                               //if( FZK.SSR_SetUserTmpStr( 1, sEmpDig, FieldByName( 'NO_HUELLA' ).AsInteger, FieldByName( 'HUELLA' ).AsString ) )then  // Version 9
                               if( FZK.SetUserTmpExStr( 1, sEmpDig, FieldByName( 'NO_HUELLA' ).AsInteger, iFlag, FieldByName( 'HUELLA' ).AsString ) )then
                                   EscribeBitacora( Format( '>>> Se envi� huella (%d) del ID(%d) del empleado a Terminal ZK <<<', [ FieldByName( 'NO_HUELLA' ).AsInteger, StrToInt( sEmpDig ) ] ), StrToInt( FieldByName( 'CB_CODIGO' ).AsString ) );
                               Next;
                          end;
                       except
                             on Error : Exception do
                                EscribeError( Format( 'Se gener� un error al consultar los empleados de la empresa: %s', [ Error.Message ] ) );
                       end;
                  end;
             end;
        end;
     except
           on Error: Exception do
              EscribeError( Format( 'Se gener� un error al consultar los empleados de la empresa: %s', [ Error.Message ] ) );
     end;
end;

procedure TdmInterfase.AlmacenaHuellas;
var
  i, iTmpLen, iEmpleadoLocal, iPri, iFlag: Integer;
  sEmp, sTmp, sNom, sPwd: WideString;
  sEmpresaLocal : String;
  lEna: WordBool;
begin
     try
        while FZK.SSR_GetAllUserInfo( 1, sEmp, sNom, sPwd, iPri, lEna )do
        begin
             if( Length( sEmp ) > 1 )then
             begin
                  i := 0;
                  iEmpleadoLocal := StrToIntDef( Copy( sEmp, 2, Length( sEmp )-1 ), 0 );
                  sEmpresaLocal := Copy( sEmp, 1, 1 );
                  with cdsLista do
                  begin
                       if( sEmpresaLocal = FDigitoEmpresa )then
                       begin
                            try
                               Data := ServerRecursos.ConsultaUnEmpleadoZK( dmCliente.Empresa, iEmpleadoLocal ); //oZetaProvider.OpenSQL( Empresa, Format( GetScript( eConsultaUnEmpleadoZK ), [ iEmpleadoLocal ] ), TRUE );
                               First;
                               // Si el empleado pertenece a la empresa;
                               if Not IsEmpty then
                               begin
                                    // Borra una sola vez, en caso de encontrar alg�n registro. Tomando en cuenta que se debe almacenar primero el 0.
                                    //if( FZK.SSR_GetUserTmpStr( 1, sEmp, 0, sTmp, iTmpLen ) )then // Version 9
                                    if( FZK.GetUserTmpExStr( 1, sEmp, 0, iFlag, sTmp, iTmpLen ) )then
                                    begin
                                         try
                                            ServerRecursos.BorraDatoZK( dmCliente.Empresa, iEmpleadoLocal );//oZetaProvider.EjecutaAndFree( Format( GetScript( eBorraDatoZK ), [ iEmpleadoLocal ] ) );
                                         except
                                               On Error: Exception do
                                                  EscribeError( Format( 'Se gener� un error al borrar la huellas almacenadas del empleado: %s', [Error.Message] ), iEmpleadoLocal );
                                         end;
                                         // Borra registro
                                    end;

                                    while( i < 10 )do
                                    begin
                                         //if( FZK.SSR_GetUserTmpStr( 1, sEmp, i, sTmp, iTmpLen  ) )then // Version 9
                                         if( FZK.GetUserTmpExStr( 1, sEmp, i, iFlag, sTmp, iTmpLen ) )then
                                         begin
                                              //agrega registro
                                              try
                                                 ServerRecursos.AgregaDatoZK( dmCliente.Empresa, iEmpleadoLocal, i, sTmp );//oZetaProvider.EjecutaAndFree( Format( GetScript( eAgregaDatoZK ) , [ iEmpleadoLocal, i, sTmp ] ) );
                                                 EscribeBitacora( Format( '>>> Se almacena huella (%d) del ID(%d) del empleado', [ (i), StrToInt( sEmp ) ] ), iEmpleadoLocal );
                                              except
                                                    On Error: Exception do
                                                       EscribeError( Format( 'Se gener� un error al almacenar la huella', [Error.Message] ), iEmpleadoLocal );
                                              end;
                                         end
                                         else
                                             break;
                                         Inc( i );
                                    end;
                               end;
                            except
                                  on Error : Exception do
                                     EscribeError( Format( 'Se gener� un error al consultar los empleados de la empresa: %s', [ Error.Message ] ) );
                            end;
                       end;
                  end; // with ...
             end; // if length ...
        end; // while ...
     except
           on Error: Exception do
              EscribeError( Format( 'Se gener� un error al consultar las huellas de los empleados: %s', [ Error.Message ] ) );
     end;
end;

{$ifdef DOS_CAPAS}
function TdmInterfase.GetServerRecursos: TdmServerRecursos;
begin
     Result:= DCliente.dmCliente.ServerRecursos;
end;
{$else}
function TdmInterfase.GetServerRecursos: IdmServerRecursosDisp;
begin
     Result:= IdmServerRecursosDisp( dmCliente.CreaServidor( CLASS_dmServerRecursos, FServidor ) );
end;
{$endif}

end.
