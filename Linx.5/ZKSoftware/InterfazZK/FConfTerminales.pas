unit FConfTerminales;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, IniFiles, Mask, ZetaNumero, Buttons;

type
  TTArchivosIni = class( TObject )
  private
    { Private declarations }
    FiniFile: TiniFile;
    function GetListaTerminales: String;
    procedure SetListaTerminales( const Value: String );
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property ListaTerminales: String read GetListaTerminales write SetListaTerminales;
  end;

  TConfTerminales = class(TForm)
    Panel2: TPanel;
    btnBorrar: TBitBtn;
    btnModificar: TBitBtn;
    btnNuevo: TBitBtn;
    lbxTerminales: TListBox;
    Label3: TLabel;
    Panel3: TPanel;
    Panel4: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    txtNombre: TEdit;
    btnGuardar: TBitBtn;
    btnSalir: TBitBtn;
    txtIP1: TZetaNumero;
    txtIP2: TZetaNumero;
    txtIP3: TZetaNumero;
    txtIP4: TZetaNumero;
    btnEnviar: TBitBtn;
    lblTerminales: TLabel;
    procedure btnSalirClick(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnEnviarClick(Sender: TObject);
    procedure txtIPChange(Sender: TObject);
    procedure txtNombreChange(Sender: TObject);
    procedure btnNuevoClick(Sender: TObject);
    procedure btnModificarClick(Sender: TObject);
    procedure btnBorrarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure txtIPKeyPress(Sender: TObject; var Key: Char);
    procedure txtNombreKeyPress(Sender: TObject; var Key: Char);
    procedure txtIP1KeyPress(Sender: TObject; var Key: Char);
    procedure txtIP4KeyPress(Sender: TObject; var Key: Char);
    procedure txtIP3KeyPress(Sender: TObject; var Key: Char);
    procedure txtIP2KeyPress(Sender: TObject; var Key: Char);
  private
    lHuboCambio: Boolean;
    FIniValues: TTArchivosIni;
    lAgregando: Boolean;
    iSeleccionadoMod: Integer;
    procedure ActivaBtnEnviar;
    procedure ActivaTextos( lActivar: Boolean );
    procedure ActivaBotonesDerecha( lActivar: Boolean );
    procedure ActivaBtnGuardar;
    procedure ObtenLista( sLista: String );
    function CreaLista: String;
    { Private declarations }
  public
    { Public declarations }
    property IniValues: TTArchivosIni read FIniValues;
  end;

var
  ConfTerminales: TConfTerminales;

const
  K_LISTA_TERMINALES = 'LISTA_TERM';

implementation

uses
  ZetaCommonTools,
  ZetaCommonClasses,
  ZetaDialogo, FTressShell;

{$R *.dfm}

// Activa/Desactiva el bot�n de enviar, dependiendo si los campos se llenaron de
// forma exitosa.
procedure TConfTerminales.ActivaBtnEnviar;
begin
     if( StrLleno( Trim( txtIP1.ValorAsText ) ) and StrLleno( Trim( txtIP2.ValorAsText ) ) and
         StrLleno( Trim( txtIP3.ValorAsText ) ) and StrLleno( Trim( txtIP4.ValorAsText ) ) and StrLleno( txtNombre.Text ) )then
         btnEnviar.Enabled := True
     else
         btnEnviar.Enabled := False;
end;

// Activa/Desactiva los texto de IP y Nombre.
procedure TConfTerminales.ActivaTextos(lActivar: Boolean);
begin
     txtIP1.Enabled := lActivar;
     txtIP2.Enabled := lActivar;
     txtIP3.Enabled := lActivar;
     txtIP4.Enabled := lActivar;
     txtNombre.Enabled := lActivar;
end;

// Activa/Desactiva botones del lado derecho de la forma
procedure TConfTerminales.ActivaBotonesDerecha(lActivar: Boolean);
begin
     btnNuevo.Enabled := lActivar;
     btnModificar.Enabled := lActivar;
     btnBorrar.Enabled := lActivar;
     //btnCancelar.Enabled := Not lActivar;
end;

// Activa/Desactiva bot�n de Guardar, si existen registros en la lista se activa
// , caso contrario no.
procedure TConfTerminales.ActivaBtnGuardar;
begin
     btnGuardar.Enabled := lHuboCambio;
end;

// Crea la lista en base al control lbxTerminales y regresa la cadena.
function TConfTerminales.CreaLista: String;
var
   sLista: String;
   i: Integer;
begin
     sLista := VACIO;
     i := 0;
     with lbxTerminales do
     begin
          while( i < Items.Count )do
          begin
               sLista := sLista + Items.Strings[ i ] + ',';
               Inc( i );
          end;

          if( Length( sLista ) > 0 )then
          begin
               sLista := '"' + Copy( sLista, 1, Length( sLista ) - 1 ) + '"';
          end;
     end;
     Result := sLista;
end;

// En base a una cadena proporcionada, genera la lista del control lbxTerminales.
procedure TConfTerminales.ObtenLista(sLista: String);
var
  strTmp: String;
const
  K_COMMA = ',';
begin
     if strLleno( sLista ) then
     begin
          strTmp := VACIO;
          while( pos( K_COMMA , sLista ) <> 0 ) do { Mientras siga existiendo ',' }
          begin
               if( Copy( sLista, 1, 1 ) <> Chr( 9 ) ) and ( Copy( sLista, 1, 1 ) <> ' ' ) then { Verifica que no haya tabs/espacios antes de un valor }
               begin
                    if( Copy( sLista , 1 , 1 ) = '"' )then { En el caso que el valor vaya delimitado por '"' }
                    begin
                         Delete( sLista, 1, 1 ); { Borra la primera '"' }
                         strTmp := Copy( sLista, 1, pos( '"' , sLista ) - 1 ); { Copia a strTmp el valor hasta la siguiente '"' }
                         Delete( sLista, 1, pos( '"', sLista ) ); { Borra todo hasta la segunda '"' }
                         Delete( sLista, 1, pos( K_COMMA, sLista ) ); { Borra el valor hasta la coma siguiente }
                    end
                    else
                    begin
                         strTmp := Copy( sLista, 1, pos( K_COMMA, sLista ) - 1 ); { Copia a strTmp el valor hasta la siguiente ',' }
                         Delete( sLista, 1, pos( K_COMMA, sLista ) ); { Borrar el valor hasta la coma siguiente }
                    end;
                    lbxTerminales.Items.Add( strTmp );
               end
               else
                   Delete( sLista, 1, 1 ); { En el caso de ser tab/espacio lo borra y continua con el siguiente caracter }
          end; //while

          { Para el �ltimo valor }
          if( Length( sLista ) > 0 )then
          begin
               sLista := Trim( sLista );
               if ( Copy( sLista , 1 , 1 ) = '"' ) then
               begin
                    Delete( sLista, 1, 1 ); { Borra la primera '"' }
                    strTmp := Copy( sLista, 1, pos( '"' , sLista ) - 1 ) { Copia a strTmp el valor depues de la primera '"' hasta la segunda '"' }
               end
               else
                   strTmp := Copy( sLista, 1, Length( sLista ) ); { Copia el valor restante a strTmp }
               lbxTerminales.Items.Add( strTmp ); { Agrega a oLinea el valor tomado }
          end;
     end;
end;

procedure TConfTerminales.btnSalirClick(Sender: TObject);
begin
     if( lHuboCambio and btnNuevo.Enabled )then
        Close
     else
     begin
          // Limpia controles.
          txtIP1.Valor := 0;
          txtIP2.Valor := 0;
          txtIP3.Valor := 0;
          txtIP4.Valor := 0;
          txtNombre.Text := VACIO;
          ActivaTextos( False );
          ActivaBotonesDerecha( True );
          lAgregando := False;

          if Not lHuboCambio then
          begin
               btnSalir.Kind := bkClose;
               btnSalir.ModalResult := mrNone;
               btnSalir.Caption := '&Salir';
          end;
     end
end;

procedure TConfTerminales.btnGuardarClick(Sender: TObject);
begin
     // Genera la nueva lista de terminales y la env�a al archivo.
     try
        FIniValues.SetListaTerminales( CreaLista );
        lHuboCambio := False;
        ActivaBtnGuardar;
        Close;
     except
           on Error: Exception do
              zError( Self.Caption, 'No se guard� la lista exit�samente', 0 );
     end;
end;

procedure TConfTerminales.FormShow(Sender: TObject);
begin
     // Limpia lista
     lbxTerminales.Clear;
     // Obtiene la lista de terminales y la asigna al control lbxTerminales.
     ObtenLista( FIniValues.GetListaTerminales );

     // Si existen elementos en la lista, permite borrar/editar.
     if( lbxTerminales.Items.Count > 0 )then
     begin
          btnModificar.Enabled := True;
          btnBorrar.Enabled := True;
     end;

     // Mostrar la cantidad actual de terminales
     lblTerminales.Caption := Format( 'Terminales: %d', [ lbxTerminales.Items.Count ] );
     
     // En el caso que tenga ya 24 elementos, no se permitir� agregar m�s.
     if( lbxTerminales.Items.Count >= 24 )then
     begin
          btnNuevo.Enabled := False;
          btnModificar.SetFocus;
     end
     else
         btnNuevo.SetFocus;
end;

procedure TConfTerminales.btnEnviarClick(Sender: TObject);
begin
     if( StrLleno( Trim( txtIP1.ValorAsText ) ) and StrLleno( Trim( txtIP2.ValorAsText ) ) and
         StrLleno( Trim( txtIP3.ValorAsText ) ) and StrLleno( Trim( txtIP4.ValorAsText ) ) )then
     begin
          if lAgregando then
          begin
               // Envia registro a la lista.
               lbxTerminales.Items.Add( txtNombre.Text + ' (' + Trim( txtIP1.ValorAsText ) + '.' + Trim( txtIP2.ValorAsText ) + '.' + Trim( txtIP3.ValorAsText ) + '.' + Trim( txtIP4.ValorAsText ) + ')' );
               lAgregando := False;
          end
          else
          begin
               lbxTerminales.Items[ iSeleccionadoMod ] := txtNombre.Text + ' (' + Trim( txtIP1.ValorAsText ) + '.' + Trim( txtIP2.ValorAsText ) + '.' + Trim( txtIP3.ValorAsText ) + '.' + Trim( txtIP4.ValorAsText ) + ')';
          end;
          // Indica que hubo alg�n cambio
          lHuboCambio := True;
          ActivaBtnGuardar;

          // Finalmente limpia controles.
          txtIP1.Valor := 0;
          txtIP2.Valor := 0;
          txtIP3.Valor := 0;
          txtIP4.Valor := 0;
          txtNombre.Text := VACIO;
          ActivaTextos( False );
          ActivaBotonesDerecha( True );

          btnSalir.Kind := bkCancel;
          btnSalir.ModalResult := mrNone;
          btnSalir.Caption := '&Cancelar';

          // Mostrar la cantidad actual de terminales
          lblTerminales.Caption := Format( 'Terminales: %d', [ lbxTerminales.Items.Count ] );

          // En el caso que tenga ya 24 elementos, no se permitir� agregar m�s.
          if( lbxTerminales.Items.Count >= 24 )then
          begin
               btnNuevo.Enabled := False;
               btnModificar.SetFocus;
          end
          else
              btnNuevo.SetFocus;
     end
     else
     begin
          zError( Self.Caption, 'Es necesario ingresar los cuatro octetos', 0 );
          if StrVacio( Trim( txtIP1.ValorAsText ) )then
              txtIP1.SetFocus
          else if StrVacio( Trim( txtIP2.ValorAsText ) )then
               txtIP2.SetFocus
          else if StrVacio( Trim( txtIP3.ValorAsText ) )then
               txtIP3.SetFocus
          else if StrVacio( Trim( txtIP4.ValorAsText ) )then
               txtIP4.SetFocus;
     end;
end;

procedure TConfTerminales.txtIPChange(Sender: TObject);
begin
     ActivaBtnEnviar;
end;

procedure TConfTerminales.txtNombreChange(Sender: TObject);
begin
     ActivaBtnEnviar;
end;

procedure TConfTerminales.btnNuevoClick(Sender: TObject);
begin
//     lbxTerminales.Items.Add( 'TERMINAL (0.0.0.0)' );
     lAgregando := True;
     ActivaTextos( True );
     ActivaBotonesDerecha( False );
     txtIP1.Valor := 0;
     txtIP2.Valor := 0;
     txtIP3.Valor := 0;
     txtIP4.Valor := 0;
     txtNombre.Text := 'TERMINAL';
     txtNombre.SetFocus;

     btnSalir.Kind := bkCancel;
     btnSalir.ModalResult := mrNone;
     btnSalir.Caption := '&Cancelar';
end;

procedure TConfTerminales.btnModificarClick(Sender: TObject);
var
  sTmp: String;
begin
     if( lbxTerminales.ItemIndex >= 0 )then
     begin
          txtNombre.Text := Copy( lbxTerminales.Items[ lbxTerminales.ItemIndex ], 1, Pos( '(', lbxTerminales.Items[ lbxTerminales.ItemIndex ] )-2 );

          sTmp := Copy( lbxTerminales.Items[ lbxTerminales.ItemIndex ],
                        Pos( '(', lbxTerminales.Items[ lbxTerminales.ItemIndex ] )+1,
                        ( Length( lbxTerminales.Items[ lbxTerminales.ItemIndex ] ) - ( Pos( '(', lbxTerminales.Items[ lbxTerminales.ItemIndex ] )+1 ) ) );

          txtIP1.Valor := StrToIntDef( Copy( sTmp, 1, Pos( '.', sTmp )-1 ), 0 );
          Delete( sTmp, 1, Pos( '.', sTmp ) );

          txtIP2.Valor := StrToIntDef( Copy( sTmp, 1, Pos( '.', sTmp )-1 ), 0 );
          Delete( sTmp, 1, Pos( '.', sTmp ) );

          txtIP3.Valor := StrToIntDef( Copy( sTmp, 1, Pos( '.', sTmp )-1 ), 0 );
          Delete( sTmp, 1, Pos( '.', sTmp ) );

          txtIP4.Valor := StrToIntDef( sTmp, 0 );

          iSeleccionadoMod := lbxTerminales.ItemIndex;
          ActivaTextos( True );
          ActivaBotonesDerecha( False );
          txtNombre.SetFocus;
          btnSalir.Kind := bkCancel;
          btnSalir.ModalResult := mrNone;
          btnSalir.Caption := '&Cancelar';
     end
     else
         zInformation( Self.Caption, 'Favor de seleccionar una terminal', 0 );          
end;

procedure TConfTerminales.btnBorrarClick(Sender: TObject);
begin
     // Borra registro de lista
     if( lbxTerminales.ItemIndex >= 0 )then
     begin
          if( zConfirm( Self.Caption, '�Borrar Registro?', 0, mbOK ) )then
          begin
               lbxTerminales.Items.Delete( lbxTerminales.ItemIndex );
               lHuboCambio := True;
               ActivaBtnGuardar;

               btnSalir.Kind := bkCancel;
               btnSalir.ModalResult := mrNone;
               btnSalir.Caption := '&Cancelar';
          end;
     end
     else
         zInformation( Self.Caption, 'Favor de seleccionar una terminal', 0 );

     // Mostrar la cantidad actual de terminales
     lblTerminales.Caption := Format( 'Terminales: %d', [ lbxTerminales.Items.Count ] );
         
     // En el caso que tenga ya 24 elementos, no se permitir� agregar m�s.
     if( lbxTerminales.Items.Count < 24 )then
     begin
          btnNuevo.Enabled := True;
          btnNuevo.SetFocus;
     end
     else
         btnModificar.SetFocus;
end;

procedure TConfTerminales.FormCreate(Sender: TObject);
begin
     FIniValues := TTArchivosIni.Create;
     lHuboCambio := False;
     lAgregando := False;
     iSeleccionadoMod := 0;
end;

procedure TConfTerminales.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FIniValues );
end;

procedure TConfTerminales.btnCancelarClick(Sender: TObject);
begin
     // Limpia controles.
     txtIP1.Valor := 0;
     txtIP2.Valor := 0;
     txtIP3.Valor := 0;
     txtIP4.Valor := 0;
     txtNombre.Text := VACIO;
     ActivaTextos( False );
     ActivaBotonesDerecha( True );
     lAgregando := False;
end;

procedure TConfTerminales.txtIPKeyPress(Sender: TObject; var Key: Char);
begin
     if ( Key <> Chr( VK_RETURN ) ) then { ENTER as TAB }
     begin
          case Key of
               #48..#57:{ Nada que hacer };
               '.': {ok};
               Chr( VK_BACK ):{ Nada que hacer };
          else
              begin
                   //MessageBeep( MB_ICONEXCLAMATION );
                   Key := Chr( 0 );
              end;
          end;
     end;
     inherited;
end;

procedure TConfTerminales.txtNombreKeyPress(Sender: TObject;
  var Key: Char);
begin
     if( Key <> Chr( VK_RETURN ) )then
     begin
          case Key of
               '''': Key := Chr( 0 );
               '"': Key := Chr( 0 );
               '(': Key := Chr( 0 );
               ')': Key := Chr( 0 );
               ',': Key := Chr( 0 );
          end;
     end;
end;

{ TTArchivosIni }

constructor TTArchivosIni.Create;
begin
     FIniFile := TIniFile.Create( ChangeFileExt( Application.ExeName, '.INI' ) );
end;

destructor TTArchivosIni.Destroy;
begin
     FreeAndNil( FIniFile );
     inherited;
end;

function TTArchivosIni.GetListaTerminales: String;
begin
     Result := FiniFile.ReadString( TressShell.CodigoEmpresa, K_LISTA_TERMINALES, VACIO );
end;

procedure TTArchivosIni.SetListaTerminales(const Value: String);
begin
     FIniFile.WriteString( TressShell.CodigoEmpresa, K_LISTA_TERMINALES, Value );
end;

procedure TConfTerminales.txtIP1KeyPress(Sender: TObject; var Key: Char);
begin
     if( Length( Trim( txtIP1.Text ) ) >= 3 )then
         Key := Chr( 0 );
end;

procedure TConfTerminales.txtIP4KeyPress(Sender: TObject; var Key: Char);
begin
     if( Length( Trim( txtIP4.Text ) ) >= 3 )then
         Key := Chr( 0 );
end;

procedure TConfTerminales.txtIP3KeyPress(Sender: TObject; var Key: Char);
begin
     if( Length( Trim( txtIP3.Text ) ) >= 3 )then
         Key := Chr( 0 );
end;

procedure TConfTerminales.txtIP2KeyPress(Sender: TObject; var Key: Char);
begin
     if( Length( Trim( txtIP2.Text ) ) >= 3 )then
         Key := Chr( 0 );
end;

end.
