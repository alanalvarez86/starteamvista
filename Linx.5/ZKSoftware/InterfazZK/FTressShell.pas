unit FTressShell;

interface

{$DEFINE MULTIPLES_ENTIDADES}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseImportaShell, Psock, NMsmtp, ImgList, Menus, ActnList, ExtCtrls,
  StdCtrls, Buttons, ComCtrls, Mask, ZetaFecha, ZetaKeyCombo, ZetaTipoEntidad,
  ZetaMessages, ZetaNumero, FileCTRL, ZBaseConsulta, FConfTerminales, XPMan,
  ZetaKeyLookup, ZetaEdit, ZetaCommonLists, ZetaCommonClasses;

type
  TTressShell = class(TBaseImportaShell)
    Panel3: TPanel;
    rgTipoMovimiento: TRadioGroup;
    Configuracin: TMenuItem;
    _ConfiguracionTerminales: TAction;
    ListadeTerminales1: TMenuItem;
    Panel2: TPanel;
    XPManifest1: TXPManifest;
    PageControl1: TPageControl;
    Terminales: TTabSheet;
    Filtro: TTabSheet;
    GroupBox1: TGroupBox;
    CheckBox1: TCheckBox;
    CheckBox9: TCheckBox;
    CheckBox17: TCheckBox;
    CheckBox18: TCheckBox;
    CheckBox19: TCheckBox;
    CheckBox20: TCheckBox;
    CheckBox21: TCheckBox;
    CheckBox22: TCheckBox;
    CheckBox23: TCheckBox;
    CheckBox24: TCheckBox;
    CheckBox16: TCheckBox;
    CheckBox15: TCheckBox;
    CheckBox14: TCheckBox;
    CheckBox13: TCheckBox;
    CheckBox12: TCheckBox;
    CheckBox11: TCheckBox;
    CheckBox10: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    CheckBox5: TCheckBox;
    CheckBox6: TCheckBox;
    CheckBox7: TCheckBox;
    CheckBox8: TCheckBox;
    cbxTodos: TCheckBox;
    GBRango: TGroupBox;
    lbInicial: TLabel;
    BInicial: TSpeedButton;
    lbFinal: TLabel;
    BFinal: TSpeedButton;
    BLista: TSpeedButton;
    RBTodos: TRadioButton;
    RBRango: TRadioButton;
    EInicial: TZetaEdit;
    EFinal: TZetaEdit;
    ELista: TZetaEdit;
    RBLista: TRadioButton;
    sCondicionLBl: TLabel;
    ECondicion: TZetaKeyLookup;
    BAgregaCampo: TBitBtn;
    sFiltro: TMemo;
    sFiltroLBL: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure zFechaValidDate(Sender: TObject);
    procedure _ConfiguracionTerminalesExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cbxTodosClick(Sender: TObject);
    procedure ActualizaCheckTodos( Sender: TObject );
    procedure BInicialClick(Sender: TObject);
    procedure BFinalClick(Sender: TObject);
    procedure BListaClick(Sender: TObject);
    procedure BAgregaCampoClick(Sender: TObject);
    procedure RBTodosClick(Sender: TObject);
    procedure RBRangoClick(Sender: TObject);
    procedure RBListaClick(Sender: TObject);
    procedure rgTipoMovimientoClick(Sender: TObject);
  private
    { Private declarations }
    lModoBatch: Boolean;
    FCodigoEmpresa: string;
    FListaTermBatch: String;
    FConfTerminales: TConfTerminales;
    FListaIP: Array[0..23] of String;
    FListaCheck: TList;
    FSeleccionarTodos: boolean;
    FEmpleadoFiltro: String;
    FEmpleadoCodigo: String;
    FTipoRango: eTipoRangoActivo;
    FParameterList: TZetaParams;
    procedure SetNombreLogs;
    procedure WMWizardEnd(var Message: TMessage); message WM_WIZARD_END;
    procedure CargaSistemaActivos;
    procedure RefrescaSistemaActivos;
    function CreaListaIP: Boolean;
    function BuscaEmpleado(const lConcatena: Boolean; const sLlave: String): String;
    procedure CargaParametros;
    function GetFiltro: String;
    function GetRango: String;
    function GetCondicion: String;
    procedure ActivaFiltro( lActivo : Boolean );
    {procedure ManejaExcepcionLog(Sender: TObject; Error: Exception);}
  protected
    { Protected declarations }
    procedure ActivaControles( const lProcesando: Boolean ); override;
    procedure DoOpenAll; override;
    procedure DoCloseAll; override;
    procedure DoProcesar; override;
    procedure SetControlesBatch; override;
    procedure ProcesarArchivo; override;
    property EmpleadoFiltro: String read FEmpleadoFiltro write FEmpleadoFiltro;
    procedure EnabledBotones( const eTipo: eTipoRangoActivo );
  public
    { Public declarations }
    { Procedimientos y Funciones Publicos del Shell de Tress }
    function FormaActivaNomina: Boolean;
    function FormaActiva: TBaseConsulta;
    procedure SetDataChange(const Entidades: array of TipoEntidad);
    procedure RefrescaEmpleado;
    procedure CambiaEmpleadoActivos;
    procedure CambiaPeriodoActivos;
    procedure CambiaSistemaActivos;
    procedure SetAsistencia( const dValue: TDate );
    procedure BuscaNuevoPeriodo( const iTipo, iPeriodoBorrado: Integer );
    procedure RefrescaIMSS;
    procedure ReinitPeriodo;
    procedure CreaArbol( const lEjecutar: Boolean );
    procedure CargaEmpleadoActivos;
    procedure ReconectaFormaActiva;
    procedure ChangeTimerInfo;
    procedure GetFechaLimiteShell;
    procedure SetBloqueo;
    procedure AbreFormaSimulaciones(const TipoForma: Integer);
    procedure RefrescaNavegacionInfo;
    procedure MustraTerminales;
    property CodigoEmpresa: string read FCodigoEmpresa write FCodigoEmpresa;
    property TipoRango: eTipoRangoActivo read FTipoRango;
    property ParameterList: TZetaParams read FParameterList;
    property EmpleadoCodigo: String read FEmpleadoCodigo write FEmpleadoCodigo;
  end;

var
  TressShell: TTressShell;

const
     K_PARAM_TIPO = 5;
     K_PARAM_TERM = 4;
     K_PARAM_FILTRO = 6;

implementation

uses DCliente,
     DGlobal,
     DSistema,
     DCatalogos,
     DTablas,
     DRecursos,
     DConsultas,
     DProcesos,
     DInterfase,
     ZAsciiTools,
     ZetaCommonTools,
     ZetaClientTools,
     FCalendario,
     {$ifdef DOS_CAPAS}
     DZetaServerProvider,
     {$endif}
     ZetaDialogo,
     ZetaBuscaEmpleado,
     ZConstruyeFormula;

{$R *.dfm}

procedure TTressShell.FormCreate(Sender: TObject);
const
     K_BATCH_SINTAXIS = '%s EMPRESA USUARIO PASSWORD TERMINALES MOVIMIENTO FILTRO';
begin
{$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
{$endif}
     Global := TdmGlobal.Create;
     dmCliente := TdmCliente.Create( Self );
     dmSistema := TdmSistema.Create( Self );
     dmCatalogos := TdmCatalogos.Create( Self );
     dmTablas := TdmTablas.Create( Self );
     dmConsultas := TdmConsultas.Create( Self );
     dmRecursos := TdmRecursos.Create( Self );
     dmProcesos := TdmProcesos.Create( Self );
     dmInterfase := TdmInterfase.Create( Self );
     inherited;
     EnviarCorreo := FALSE;
     SintaxisBatch := Format( K_BATCH_SINTAXIS, [ ExtractFileName( Application.ExeName ) ] );
     lModoBatch := False;
     FCodigoEmpresa := VACIO;
     FConfTerminales := TConfTerminales.Create( Self );

     FSeleccionarTodos := true;
     FEmpleadoFiltro := VACIO;
     ECondicion.LookupDataset := dmCatalogos.cdsCondiciones;
     FParameterList := TZetaParams.Create;
     FEmpleadoCodigo := ARROBA_TABLA + '.CB_CODIGO';
end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
     FParameterList.Free;
     FreeAndNil( FConfTerminales );
     FreeAndNil( dmInterfase );
     FreeAndNil( dmProcesos );
     FreeAndNil( dmRecursos );
     FreeAndNil( dmConsultas );
     FreeAndNil( dmTablas );
     FreeAndNil( dmCatalogos );
     FreeAndNil( dmSistema );
     //FreeAndNil( dmCliente );
     FreeAndNil( Global );
{$ifdef DOS_CAPAS}
     DZetaServerProvider.FreeAll;
{$endif}
     inherited;
end;

procedure TTressShell.DoOpenAll;
begin
     try
        inherited DoOpenAll;
        Global.Conectar;
        {***US 11757: Proteccion HTTP SkinController = True
        Evita que el backgound de algunos componentes de la aplicacion
        sea de color blanco
        ***}
        DevEx_SkinController.NativeStyle := TRUE;
        with dmCliente do
        begin
             InitActivosSistema;
             InitActivosAsistencia;
             InitActivosEmpleado;
             InitActivosPeriodo;
             FCodigoEmpresa := Compania;
        end;
        InitBitacora;
        CargaSistemaActivos;
     except
        on Error : Exception do
        begin
             ReportaErrorDialogo( 'Error al conectar', 'Empresa: ' + dmCliente.Compania + CR_LF + Error.Message );
             DoCloseAll;
        end;
     end;
     ActivaControles( FALSE );

     if Not ModoBatch then
        MustraTerminales; // Verifica lista de terminales vs. checkbox's

     dmCatalogos.cdsCondiciones.Conectar;

end;

procedure TTressShell.DoCloseAll;
begin
     if EmpresaAbierta then
     begin
          dmCliente.CierraEmpresa;
          ZetaClientTools.CierraDatasets( dmProcesos );
          ZetaClientTools.CierraDatasets( dmRecursos );
          ZetaClientTools.CierraDatasets( dmConsultas );
          ZetaClientTools.CierraDatasets( dmTablas );
          ZetaClientTools.CierraDatasets( dmCatalogos );
     end;
     inherited DoCloseAll;
end;

procedure TTressShell.DoProcesar;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     SetNombreLogs;
     try
        with dmInterfase do
        begin
             if( CreaListaIP )then
             begin
                  ListaIP := TArrayListaIP( FListaIP );
                  TipoMovimiento := eTipoMovimiento( rgTipoMovimiento.ItemIndex );
                  Self.CargaParametros;
                  Procesa;
             end
             else
                 ReportaErrorDialogo( self.Caption, 'No se ha seleccionado alguna terminal' );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell.ActivaControles(const lProcesando: Boolean);
begin
     inherited;
     with dmCliente do
     begin
          if ( not ModoBatch ) then
          begin
               if lProcesando and ( BtnDetener.CanFocus ) then
                  BtnDetener.SetFocus
               else if EmpresaAbierta and ( ArchOrigen.CanFocus ) then
                       ArchOrigen.SetFocus
                    else if BtnSalir.CanFocus then
                            BtnSalir.SetFocus;
          end;
     end;
end;

procedure TTressShell.SetControlesBatch;
begin
     inherited;
     rgTipoMovimiento.ItemIndex := StrToIntDef( ParamStr( K_PARAM_TIPO ), 0 );
     FListaTermBatch := ParamStr( K_PARAM_TERM );
     ECondicion.Llave := Copy( ParamStr( K_PARAM_FILTRO ), 1, 8 );
     EnviarCorreo := FALSE;
     SetNombreLogs;
end;

procedure TTressShell.CargaSistemaActivos;
begin
     RefrescaSistemaActivos;
end;

procedure TTressShell.SetNombreLogs;
var
   sNombre, sArchBit, sArchErr: String;
begin
     with dmCliente do
     begin
          sNombre := FechaToStr( FechaDefault ) + FormatDateTime( 'hhmmss', Time ) + '.LOG';
          sArchBit := ZetaMessages.SetFileNameDefaultPath( 'BITACORA-' + sNombre );
          sArchErr := ZetaMessages.SetFileNameDefaultPath( 'ERRORES-' + sNombre );
     end;

     if StrVacio( ArchBitacora.Text )  then
          ArchBitacora.Text := sArchBit;

     if StrVacio( ArchErrores.Text ) then
          ArchErrores.Text := sArchErr;
end;

function TTressShell.FormaActivaNomina: Boolean;
begin
     Result := FALSE;
end;

function TTressShell.FormaActiva: TBaseConsulta;
begin
     Result := Nil;
end;

{ ******* Mensajes de Wizards ejecutados con Threads ****** }
procedure TTressShell.WMWizardEnd(var Message: TMessage);
begin
     with Message do
     begin
          dmInterfase.HandleProcessEnd( WParam, LParam );
     end;
end;

{ Procedimientos y Funciones Publicos del Shell de Tress }

procedure TTressShell.RefrescaEmpleado;
begin
     // No Implementar
end;

procedure TTressShell.SetDataChange(const Entidades: array of TipoEntidad);
begin
     // No Implementar
end;

procedure TTressShell.CambiaEmpleadoActivos;
begin
     // No Implementar
end;

procedure TTressShell.SetAsistencia(const dValue: TDate);
begin
     // No Implementar
end;

procedure TTressShell.BuscaNuevoPeriodo(const iTipo, iPeriodoBorrado: Integer);
begin
     // No Implementar
end;

procedure TTressShell.RefrescaIMSS;
begin
     // No Implementar
end;

procedure TTressShell.ReinitPeriodo;
begin
     // No Implementar
end;

procedure TTressShell.CreaArbol(const lEjecutar: Boolean);
begin
     // No Implementar
end;

procedure TTressShell.CargaEmpleadoActivos;
begin
     // No Implementar
end;

procedure TTressShell.ReconectaFormaActiva;
begin
     // No Implementar
end;

procedure TTressShell.ChangeTimerInfo;
begin
     // No Implementar
end;

procedure TTressShell.GetFechaLimiteShell;
begin
     // No Implementar
end;

procedure TTressShell.SetBloqueo;
begin
     // No Implementar
end;

procedure TTressShell.zFechaValidDate(Sender: TObject);
begin
     inherited;
     RefrescaSistemaActivos;
end;

procedure TTressShell.RefrescaSistemaActivos;
begin
     SetNombreLogs;
end;

procedure TTressShell.AbreFormaSimulaciones(const TipoForma: Integer);
begin
     // No Implementar
end;

procedure TTressShell.RefrescaNavegacionInfo;
begin
     // No Implementar
end;

procedure TTressShell.ProcesarArchivo;
begin
     ActivaControles( TRUE );
     try
        InitBitacora;
        DoProcesar;
     finally
        EscribeBitacoras;
        ActivaControles( FALSE );
     end;
end;

procedure TTressShell._ConfiguracionTerminalesExecute(Sender: TObject);
begin
     with TConfTerminales.Create( Self )do
     begin
          try
             ShowModal;
             MustraTerminales;
          finally
                 Free;
          end;
     end;
end;

// Lee el archivo de configuraci�n y muestra las terminales en base a este.
procedure TTressShell.MustraTerminales;
var
  sLista, strTmp: String;
  i: Integer;
const
  K_COMMA = ',';

  procedure AsigtaTextoCBX( sTerminal: String; iPos: Integer );
  begin
       TCheckBox( FListaCheck[ iPos - 1 ] ).Enabled := True;
       TCheckBox( FListaCheck[ iPos - 1 ] ).Caption := sTerminal;
       TCheckBox( FListaCheck[ iPos - 1 ] ).Visible := True;
  end;

  procedure DeshabilidaCBX;
  var
    ii: Integer;
  begin
       ii := 0;
       while( ii < FListaCheck.Count )do
       begin
            TCheckBox( FListaCheck[ ii ] ).Enabled := False;
            TCheckBox( FListaCheck[ ii ] ).Caption := VACIO;
            TCheckBox( FListaCheck[ ii ] ).Visible := False;
            Inc( ii );
       end;
  end;

begin
     i := 0;
     DeshabilidaCBX;
     sLista := FConfTerminales.IniValues.ListaTerminales;
     if strLleno( sLista ) then
     begin
          strTmp := VACIO;
          while( pos( K_COMMA , sLista ) <> 0 ) do { Mientras siga existiendo ',' }
          begin
               Inc( i );
               if( Copy( sLista, 1, 1 ) <> Chr( 9 ) ) and ( Copy( sLista, 1, 1 ) <> ' ' ) then { Verifica que no haya tabs/espacios antes de un valor }
               begin
                    if( Copy( sLista , 1 , 1 ) = '"' )then { En el caso que el valor vaya delimitado por '"' }
                    begin
                         Delete( sLista, 1, 1 ); { Borra la primera '"' }
                         strTmp := Copy( sLista, 1, pos( '"' , sLista ) - 1 ); { Copia a strTmp el valor hasta la siguiente '"' }
                         Delete( sLista, 1, pos( '"', sLista ) ); { Borra todo hasta la segunda '"' }
                         Delete( sLista, 1, pos( K_COMMA, sLista ) ); { Borra el valor hasta la coma siguiente }
                    end
                    else
                    begin
                         strTmp := Copy( sLista, 1, pos( K_COMMA, sLista ) - 1 ); { Copia a strTmp el valor hasta la siguiente ',' }
                         Delete( sLista, 1, pos( K_COMMA, sLista ) ); { Borrar el valor hasta la coma siguiente }
                    end;
                    AsigtaTextoCBX( strTmp, i );
                    //lbxTerminales.Items.Add( strTmp );
               end
               else
                   Delete( sLista, 1, 1 ); { En el caso de ser tab/espacio lo borra y continua con el siguiente caracter }
          end; //while

          { Para el �ltimo valor }
          if( Length( sLista ) > 0 )then
          begin
               Inc( i );
               sLista := Trim( sLista );
               if ( Copy( sLista , 1 , 1 ) = '"' ) then
               begin
                    Delete( sLista, 1, 1 ); { Borra la primera '"' }
                    strTmp := Copy( sLista, 1, pos( '"' , sLista ) - 1 ) { Copia a strTmp el valor depues de la primera '"' hasta la segunda '"' }
               end
               else
                   strTmp := Copy( sLista, 1, Length( sLista ) ); { Copia el valor restante a strTmp }
               AsigtaTextoCBX( strTmp, i );
               //lbxTerminales.Items.Add( strTmp ); { Agrega a oLinea el valor tomado }
          end;
          {
          if( i > 4 )then
              Panel2.Height := 145
          else
              Panel2.Height := 89;
          }
     end;
end;

// Crea una lista, en forma de cadena de caracteres, en base a las terminales seleccionadas.
function TTressShell.CreaListaIP: Boolean;
var
  i, j: Integer;
  lSalida: Boolean;
  sTmp: String;
const
  K_COMMA = ',';

  procedure AgregaIP( sTerminal: String; iPos: Integer );
  begin
       lSalida := True;
       FListaIP[ iPos ] := Copy( sTerminal,
                                 Pos( '(', sTerminal )+1,
                                 ( Length( sTerminal ) - ( Pos( '(', sTerminal )+1 ) ) );
       //Inc( iPos );
  end;
begin
     //Limpia arreglo.
     for i := 0 to 23 do
         FListaIP[i] := VACIO;

     i := 0;
     lSalida := False;

     if( ModoBatch )then
     begin
          if( StrLleno( FListaTermBatch ) and ( FListaTermBatch <> '0' ) )then
          begin
               sTmp := VACIO;
               while( pos( K_COMMA , FListaTermBatch ) <> 0 ) do { Mientras siga existiendo ',' }
               begin
                    if( Copy( FListaTermBatch, 1, 1 ) <> Chr( 9 ) ) and ( Copy( FListaTermBatch, 1, 1 ) <> ' ' ) then { Verifica que no haya tabs/espacios antes de un valor }
                    begin
                         if( Copy( FListaTermBatch , 1 , 1 ) = '"' )then { En el caso que el valor vaya delimitado por '"' }
                         begin
                              Delete( FListaTermBatch, 1, 1 ); { Borra la primera '"' }
                              sTmp := Copy( FListaTermBatch, 1, pos( '"' , FListaTermBatch ) - 1 ); { Copia a strTmp el valor hasta la siguiente '"' }
                              Delete( FListaTermBatch, 1, pos( '"', FListaTermBatch ) ); { Borra todo hasta la segunda '"' }
                              Delete( FListaTermBatch, 1, pos( K_COMMA, FListaTermBatch ) ); { Borra el valor hasta la coma siguiente }
                         end
                         else
                         begin
                              sTmp := Copy( FListaTermBatch, 1, pos( K_COMMA, FListaTermBatch ) - 1 ); { Copia a strTmp el valor hasta la siguiente ',' }
                              Delete( FListaTermBatch, 1, pos( K_COMMA, FListaTermBatch ) ); { Borrar el valor hasta la coma siguiente }
                         end;
                         FListaIP[ i ] := sTmp;
                    end
                    else
                        Delete( FListaTermBatch, 1, 1 ); { En el caso de ser tab/espacio lo borra y continua con el siguiente caracter }
                    Inc( i );
               end; //while

               { Para el �ltimo valor }
               if( Length( FListaTermBatch ) > 0 )then
               begin
                    FListaTermBatch := Trim( FListaTermBatch );
                    if ( Copy( FListaTermBatch , 1 , 1 ) = '"' ) then
                    begin
                         Delete( FListaTermBatch, 1, 1 ); { Borra la primera '"' }
                         sTmp := Copy( FListaTermBatch, 1, pos( '"' , FListaTermBatch ) - 1 ) { Copia a strTmp el valor depues de la primera '"' hasta la segunda '"' }
                    end
                    else
                        sTmp := Copy( FListaTermBatch, 1, Length( FListaTermBatch ) ); { Copia el valor restante a strTmp }
                    FListaIP[ i ] := sTmp;
               end;
               lSalida := True;
          end;
     end
     else
     begin
          j := 0;
          while( i < FListaCheck.Count )do
          begin
               if( TCheckBox( FListaCheck[ i ] ).Enabled and TCheckBox( FListaCheck[ i ] ).Checked )then
               begin
                    AgregaIP( TCheckBox( FListaCheck[ i ] ).Caption, j );
                    Inc( j );
               end;
               Inc( i );
          end;
     end;

     Result := lSalida;
end;

procedure TTressShell.FormShow(Sender: TObject);
begin
     inherited;
     FListaCheck := TList.Create;

     FListaCheck.Add( CheckBox1 );
     FListaCheck.Add( CheckBox2 );
     FListaCheck.Add( CheckBox3 );
     FListaCheck.Add( CheckBox4 );
     FListaCheck.Add( CheckBox5 );
     FListaCheck.Add( CheckBox6 );
     FListaCheck.Add( CheckBox7 );
     FListaCheck.Add( CheckBox8 );
     FListaCheck.Add( CheckBox9 );
     FListaCheck.Add( CheckBox10 );
     FListaCheck.Add( CheckBox11 );
     FListaCheck.Add( CheckBox12 );
     FListaCheck.Add( CheckBox13 );
     FListaCheck.Add( CheckBox14 );
     FListaCheck.Add( CheckBox15 );
     FListaCheck.Add( CheckBox16 );
     FListaCheck.Add( CheckBox17 );
     FListaCheck.Add( CheckBox18 );
     FListaCheck.Add( CheckBox19 );
     FListaCheck.Add( CheckBox20 );
     FListaCheck.Add( CheckBox21 );
     FListaCheck.Add( CheckBox22 );
     FListaCheck.Add( CheckBox23 );
     FListaCheck.Add( CheckBox24 );
     ECondicion.Llave := VACIO;
     PageControl1.ActivePage := Terminales;
end;

// Selecciona todos las terminales o quita selecci�n.
procedure TTressShell.cbxTodosClick(Sender: TObject);
var
  i: Integer;
begin
     inherited;
     i := 0;
     // Esta bandera nos sirve para saber si se seleccion� el check desde la interfaz
     // o el cambio fue invocado internamente. En el segundo caso, no queremos que
     // tenga el comportamiento normal.
     if FSeleccionarTodos then
     begin
          while( i < FListaCheck.Count )do
          begin
               FSeleccionarTodos := false;
               if( TCheckBox( FListaCheck[ i ] ).Enabled )then
                   TCheckBox( FListaCheck[ i ] ).Checked := cbxTodos.Checked;
               FSeleccionarTodos := true;
               Inc( i );
          end;
     end;
end;

// Si alguna terminal no est� checked, entonces quita el check del control de TODOS,
// en el caso que todos est�n seleccionados, agrega el check al control.
procedure TTressShell.ActualizaCheckTodos;
var
  i: integer;
  lTodos: boolean;
begin
     i := 0;
     lTodos := False;

     // Esta bandera nos sirve para saber si se seleccion� el check desde la interfaz
     // o el cambio fue invocado internamente. En el segundo caso, no queremos que
     // tenga el comportamiento normal.
     if FSeleccionarTodos then
     begin
          while( i < FListaCheck.Count )do
          begin
               if( TCheckBox( FListaCheck[ i ] ).Enabled )then
               begin
                    lTodos := TCheckBox( FListaCheck[ i ] ).Checked;
                    if Not lTodos then
                       break;
               end;
               inc( i );
          end;

          FSeleccionarTodos := false;
          cbxTodos.Checked := lTodos;
          FSeleccionarTodos := true;
     end;
end;

procedure TTressShell.BInicialClick(Sender: TObject);
begin
     with EInicial do
     begin
          Text := BuscaEmpleado( False, Text );
     end;
end;

procedure TTressShell.BFinalClick(Sender: TObject);
begin
     with EFinal do
     begin
          Text := BuscaEmpleado( False, Text );
     end;
end;

procedure TTressShell.BListaClick(Sender: TObject);
begin
     with ELista do
     begin
          Text := BuscaEmpleado( True, Text );
     end;
end;

procedure TTressShell.BAgregaCampoClick(Sender: TObject);
begin
     with sFiltro do
     begin
          Text:= ZConstruyeFormula.GetFormulaConst( enEmpleado, Text, SelStart, evBase );
     end;
end;

function TTressShell.BuscaEmpleado( const lConcatena: Boolean; const sLlave: String ): String;
var
   sKey, sDescripcion: String;
const
     K_COMA = ',';
begin
     if ZetaBuscaEmpleado.BuscaEmpleadoDialogo( FEmpleadoFiltro, sKey, sDescripcion ) then
     begin
          if lConcatena and ZetaCommonTools.StrLleno( Text ) then
             Result := ZetaCommonTools.ConcatString( sLlave, sKey, K_COMA )
          else
              Result := sKey;
     end;
end;

procedure TTressShell.RBTodosClick(Sender: TObject);
begin
     EnabledBotones( raTodos );
end;

procedure TTressShell.RBRangoClick(Sender: TObject);
begin
     EnabledBotones( raRango );
end;

procedure TTressShell.RBListaClick(Sender: TObject);
begin
     EnabledBotones( raLista );
end;

procedure TTressShell.EnabledBotones( const eTipo: eTipoRangoActivo );
var
   lEnabled: Boolean;
begin
     FTipoRango := eTipo;
     lEnabled := ( eTipo = raRango );
     lbInicial.Enabled := lEnabled;
     lbFinal.Enabled := lEnabled;
     EInicial.Enabled := lEnabled;
     EFinal.Enabled := lEnabled;
     bInicial.Enabled := lEnabled;
     bFinal.Enabled := lEnabled;
     lEnabled := ( eTipo = raLista );
     ELista.Enabled := lEnabled;
     bLista.Enabled := lEnabled;
end;

procedure TTressShell.CargaParametros;
begin
     if( ModoBatch and StrLleno( ECondicion.Llave )and( rgTipoMovimiento.ItemIndex = 1 ) )then
     begin
          ECondicion.Llave := VACIO;
          EscribeBitacora( 'Advertencia - En el movimiento de Almacenamiento de Huellas no se considera una condici�n' );
     end
     else
     begin
          if( ModoBatch and StrLleno( ECondicion.Llave ) )then
          begin
               if not dmCatalogos.cdsCondiciones.Locate( 'QU_CODIGO', Copy( ECondicion.Llave, 1, 8 ), [] )then
               begin
                    EscribeBitacora( Format( 'Advertencia - El C�digo de Condici�n no existe', [ ECondicion.Llave ] ) );
                    ECondicion.Llave := VACIO;
               end;
          end;

          with ParameterList do
          begin
               AddString( 'RangoLista', GetRango );
               AddString( 'Condicion', GetCondicion );
               AddString( 'Filtro', GetFiltro );
          end;
     end;
end;

function TTressShell.GetRango: String;
begin
     case TipoRango of
          raRango: Result := GetFiltroRango( FEmpleadoCodigo, Trim( EInicial.Text ), Trim( EFinal.Text ) );
          raLista: Result := GetFiltroLista( FEmpleadoCodigo, Trim( ELista.Text ) );
     else
         Result := '';
     end;
end;

function TTressShell.GetCondicion: String;
begin
     if StrLleno( ECondicion.Llave ) then
     begin
          Result := Trim( ECondicion.LookUpDataSet.FieldByName( 'QU_FILTRO' ).AsString );
          { Causa problemas porque le quita la '@'; los par�ntesis no son necesarios
          if EsFormula( Result ) then
             Result := Parentesis( Result );
          }
     end
     else
         Result := '';
end;

function TTressShell.GetFiltro: String;
begin
     Result := Trim( sFiltro.Text );
     { Los Par�ntesis no son necesarios
     if StrLleno( Result ) then
        Result := Parentesis( Result );
     }
end;

procedure TTressShell.ActivaFiltro( lActivo : Boolean );
begin
     if not ModoBatch then
     begin
          RBTodos.Enabled := lActivo;
          RBRango.Enabled := lActivo;
          EInicial.ReadOnly := not lActivo;
          EFinal.ReadOnly := not lActivo;
          RBLista.Enabled := lActivo;
          ELista.ReadOnly := not lActivo;
          ECondicion.Enabled := lActivo;
          ECondicion.ReadOnly := not lActivo;
          sFiltro.Enabled := lActivo;
          sFiltro.ReadOnly := not lActivo;
          BAgregaCampo.Enabled := lActivo;
     end;

     // Inicializa valores cada vez que se cambia el valor.
     RBTodos.Checked := true;
     RBRango.Checked := false;
     EInicial.Text := VACIO;
     EFinal.Text := VACIO;
     RBLista.Checked := false;
     ELista.Text := VACIO;
     ECondicion.Llave := VACIO;
     sFiltro.Text := VACIO;
end;

procedure TTressShell.rgTipoMovimientoClick(Sender: TObject);
begin
     inherited;
     ActivaFiltro( ( rgTipoMovimiento.ItemIndex = 0 ) );
end;


procedure TTressShell.CambiaPeriodoActivos;
begin
     // No Implementar
end;

procedure TTressShell.CambiaSistemaActivos;
begin
     // No Implementar
end;

end.
