unit FConfigurar;

interface

uses Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ExtCtrls, OleCtrls, SHDocVw,
     LinxIni;

type
  TConfigurarTerminal = class(TForm)
    PanelInferior: TPanel;
    Salir: TBitBtn;
    WebBrowser: TWebBrowser;
    Anterior: TBitBtn;
    Siguiente: TBitBtn;
    PaginaPrincipal: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure SalirClick(Sender: TObject);
    procedure AnteriorClick(Sender: TObject);
    procedure SiguienteClick(Sender: TObject);
    procedure PaginaPrincipalClick(Sender: TObject);
  private
    { Private declarations }
    FTerminal: TTerminal;
  public
    { Public declarations }
    property Terminal: TTerminal read FTerminal write FTerminal;
  end;

procedure ConfigurarTerminal( TerminalInfo: TTerminal );

implementation

uses ZetaCommonTools;

procedure ConfigurarTerminal( TerminalInfo: TTerminal );
var
   Pantalla: TConfigurarTerminal;
begin
     Pantalla := TConfigurarTerminal.Create( Application );
     try
        with Pantalla do
        begin
             Terminal := TerminalInfo;
             ShowModal;
        end;
     finally
            FreeAndNil( Pantalla );
     end;
end;

{$R *.dfm}

procedure TConfigurarTerminal.FormShow(Sender: TObject);
var
   sTexto: String;
begin
     with Self.Terminal do
     begin
          if ZetaCommonTools.StrLleno( Description ) then
             sTexto := Format( 'Configuración de %s', [ Description ] )
          else
              sTexto := Format( 'Configuración de Terminal %s', [ IPAddress ] );
     end;
     Caption := sTexto;
     WindowState := wsMaximized;
     Application.ProcessMessages;
     PaginaPrincipalClick( Self );
end;

procedure TConfigurarTerminal.SalirClick(Sender: TObject);
begin
     Close;
end;

procedure TConfigurarTerminal.AnteriorClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        WebBrowser.GoBack;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TConfigurarTerminal.SiguienteClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        WebBrowser.GoForward;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TConfigurarTerminal.PaginaPrincipalClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        WebBrowser.Navigate( Format( 'http://%s', [ Terminal.IPAddress ] ) );
     finally
            Screen.Cursor := oCursor;
     end;
end;

end.
