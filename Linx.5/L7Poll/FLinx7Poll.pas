unit FLinx7Poll;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      FLinx5Poll.pas                             ::
  :: Descripci�n: Programa principal de L5Poll.exe           ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Db, DBClient,
     Forms, Dialogs, ExtCtrls, Menus, StdCtrls, Buttons, ComCtrls, ComObj, ActiveX,
     LinxIni,
     LinxDll,
     FLinxBase, IdBaseComponent, IdAntiFreezeBase, IdAntiFreeze;

type
  TLinx7Poll = class(TLinxBase)
    N5: TMenuItem;
    ConfiguracionArchivos: TMenuItem;
    TerminalesDirectorio: TMenuItem;
    Empleados: TBitBtn;
    Directorio: TBitBtn;
    TerminalesFechaYHora: TMenuItem;
    IdAntiFreeze1: TIdAntiFreeze;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ConfiguracionClick(Sender: TObject);
    procedure ConfiguracionArchivosClick(Sender: TObject);
    procedure ConfiguracionListaClick(Sender: TObject);
    procedure TerminalesProgramaClick(Sender: TObject);
    procedure TerminalesAlarmaClick(Sender: TObject);
    procedure TerminalesEmpleadosClick(Sender: TObject);
    procedure ArchivoRecolectarClick(Sender: TObject);
    procedure TerminalesDirectorioClick(Sender: TObject);
    procedure TerminalesVersionClick(Sender: TObject);
    procedure TerminalesSincronizarClick(Sender: TObject);
    procedure TerminalesSincronizarTodasClick(Sender: TObject);
    procedure TerminalesResetearClick(Sender: TObject);
    procedure TerminalesRebootClick(Sender: TObject);
    procedure TerminalesFechaYHoraClick(Sender: TObject);
    procedure ArchivoTerminarClick(Sender: TObject);
  private
    { Private declarations }
    FLinxServer: TL7Server;
    function PuedeEjecutarEnDemo: Boolean;
    procedure SendFile( Command: FileSender; sFileName: String; lAskForFile: Boolean );
    procedure ExecuteCmd( Command: Executer );
{$ifdef FALSE}
    procedure PostFeedback(const sMessage: String; iTerminal: Integer);
{$endif}
  protected
    { Protected declarations }
    function DatosEnviados: String; override;
    function InitAllServers: Boolean; override;
    function Read: Boolean; override;
    function RegistroLeido: String; override;
    function RegistroNormal: Boolean; override;
    function Terminal: Integer; override;
    function VerifyAllServers: Boolean; override;
    function PuedeRecolectar: Boolean; override;
    procedure AutoSincronizarTodas( const dValue: TDate ); override;
    procedure IncrementaLecturas; override;
    procedure DownAllServers; override;
    procedure EnableControls( const lRecolectando: Boolean ); override;
    procedure MakeNodeList; override;
    procedure ProcesarLectura( var sData: String ); override;
    procedure SendAlarm( const sFileName: String ); override;
    procedure SendEmpleados( const sFileName: String ); override;
    procedure SendFlash( const sFileName: String ); override;
    procedure SendScript( const sFileName: String ); override;
    procedure SendRebootCmd; override;
    procedure EscribeLecturaLinea( const sData: String );
  public
    { Public declarations }
    property LinxServer: TL7Server read FLinxServer;
    function BatchModeEnabled: Boolean; override;
    procedure MakeUnitList; override;
    procedure ProcesaParametro( const sParametro: String ); override;
    procedure ProcesaParametro( const sParametro, sArchivo: String ); override;
  end;

var
  Linx7Poll: TLinx7Poll;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo,
     FFileMgr,
     FSincronizar,
     FArchivos,
     FTerminales;

{$R *.DFM}

const
     K_FEEDBACK_DELIMITER = '>>';
     K_FLASH_EXT1 = '.S19';
     K_FLASH_EXT2 = '.S39';
     K_REMOTE_COMMAND_FILE = 'RMTCNTRL.TXT';

{ ************* TLinx5Poll ************ }

procedure TLinx7Poll.FormCreate(Sender: TObject);
const
     {$ifdef DOS_CAPAS}
     K_TITULO = '%s Profesional';
     {$else}
     K_TITULO = '%s';
     {$endif}
begin
     inherited;
     {$ifdef DUMMY_SERVER}
     FLinxServer := TL7DummyServer.Create;
     {$else}
     FLinxServer := TL7Server.Create;
     {$endif}
     with LinxServer do
     begin
          OnMakeUnitList := MakeUnitList;
          OnFeedBack := PostMessage;
          OnGaugeSetSize := GaugeSetSize;
          OnGaugeAdvance := GaugeAdvance;
          OnGaugeFinish := GaugeFinish;
          OnDoLectura := EscribeLecturaLinea;
          ValidTokens := TOKEN_ASISTENCIA;
     end;
     Self.Caption := Format( K_TITULO, [ Application.Title ] );
end;

procedure TLinx7Poll.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FLinxServer );
     inherited;
end;

function TLinx7Poll.BatchModeEnabled: Boolean;
begin
     Result := True;
end;

procedure TLinx7Poll.EnableControls( const lRecolectando: Boolean );
begin
     inherited EnableControls( lRecolectando );
     Self.Empleados.Enabled := lRecolectando;
     Self.Directorio.Enabled := lRecolectando;
end;

{$ifdef FALSE}
procedure TLinx7Poll.PostFeedback( const sMessage: String; iTerminal: Integer );
var
   i: Integer;
   lSelected: Boolean;

function FormatUnitMessage( sOldMsg, sNewMsg: String ): String;
const
     K_WIDTH_OLD_MESSAGE = 16;
begin
     Result := Copy( Trim( Copy( sOldMsg, 1, K_WIDTH_OLD_MESSAGE ) ) + StringOfChar( ' ', K_WIDTH_OLD_MESSAGE ), 1, K_WIDTH_OLD_MESSAGE ) + sNewMsg;
end;

begin
     iTerminal := iTerminal - 1;
     with UnitList do
     begin
          Items.BeginUpdate;
          if ( iTerminal < 0 ) then
          begin
               for i := 0 to ( Items.Count - 1 ) do
               begin
                    lSelected := Selected[ i ];
                    Items.Strings[ i ] := FormatUnitMessage( Items.Strings[ i ], sMessage );
                    Selected[ i ] := lSelected;
               end;
          end
          else
          begin
               if ( iTerminal < Items.Count ) then
               begin
                    lSelected := Selected[ iTerminal ];
                    Items.Strings[ iTerminal ] := FormatUnitMessage( Items.Strings[ iTerminal ], sMessage );
                    Selected[ iTerminal ] := lSelected;
               end
               else
                   Items.Add( FormatUnitMessage( 'Terminal Desconocida', sMessage ) );
          end;
          Items.EndUpdate;
     end;
end;
{$endif}

{ ********** Abrir Servidores ************ }

function TLinx7Poll.InitAllServers: Boolean;
begin
     Result := LinxServer.Init;
     if Result then
        Result := inherited InitAllServers;
end;

function TLinx7Poll.VerifyAllServers: Boolean;
begin
     try
        LinxServer.Verify;
        Result := True;
     except
           on Error: Exception do
           begin
                ManejaError( 'Error Al Verificar Servidores', Error );
                Result := False;
           end;
     end;
end;

{ ********** Procesamiento ********** }

procedure TLinx7Poll.ProcesarLectura( var sData: String );
begin
     inherited ProcesarLectura( sData );
end;

{ ********** Cerrar Servidores ********** }

procedure TLinx7Poll.DownAllServers;
begin
     LinxServer.Close;
     if( PuedeRecolectar )then
         inherited DownAllServers;
end;

{ ********** M�todos de Manejo De Linx5Server ********** }

procedure TLinx7Poll.MakeUnitList;
var
   i: Integer;
   Terminal: TTerminal;
begin
     with UnitList.Items do
     begin
          BeginUpdate;
          Clear;
          for i := 0 to ( LinxServer.ListaTerminales.Count - 1 ) do
          begin
               Terminal := LinxServer.ListaTerminales.Terminal[ i ];
               if Terminal.Conectada then
               begin
                    Add( Format( '%-15.15s< %4.4s >: %s', [ Terminal.IPAddress, Terminal.Identificador, LinxServer.GetSerialNumber ( i ) ] ) );
               end
               else
                   Add( Format( '%-15.15s< %4.4s >: < No Conectada >', [ Terminal.IPAddress, Terminal.Identificador ] ) );
          end;
          EndUpdate;
     end;
end;

function TLinx7Poll.DatosEnviados: String;
begin
     Result := LinxServer.DatosEnviados;
end;

function TLinx7Poll.Read: Boolean;
begin
     Result := LinxServer.Read;
end;

function TLinx7Poll.RegistroLeido: String;
begin
     Result := LinxServer.RegistroLeido;
end;

function TLinx7Poll.RegistroNormal: Boolean;
begin
     Result := LinxServer.RegistroNormal;
end;

function TLinx7Poll.Terminal: Integer;
begin
     Result := LinxServer.Terminal + 1;
end;

procedure TLinx7Poll.MakeNodeList;
begin
     LinxServer.MakeNodeList;
end;

procedure TLinx7Poll.AutoSincronizarTodas( const dValue: TDate );
begin
     LinxServer.SetDateTimeAll( dValue );
end;

procedure TLinx7Poll.SendAlarm( const sFileName: String );
var
   i: Integer;
   sAlarmFile: String;
   TerminalData: TTerminal;
begin
     with LinxServer do
     begin
          for i := 0 to ( ListaTerminales.Count - 1 ) do
          begin
               sAlarmFile := VACIO;
               TerminalData := ListaTerminales.Terminal[ i ];  { Obtener datos de la terminal i }
               if TerminalData.Conectada then
               begin
                    if ZetaCommonTools.StrLleno( sFileName ) then
                       sAlarmFile := sFileName
                    else
                    begin
                         if Assigned( TerminalData ) then
                         begin
                              sAlarmFile := TerminalData.Alarma;
                         end;
                         if ZetaCommonTools.StrVacio( sAlarmFile ) then
                         begin
                              sAlarmFile := LinxIni.IniValues.Alarma;
                         end;
                    end;
                    if ZetaCommonTools.StrLleno( sAlarmFile ) and FileExists( sAlarmFile ) then
                    begin
                         SendParameterFile( i, sAlarmFile );
                         Bitacora.WriteTimeStamp( Format( 'Alarma %s Enviado A %s', [ sAlarmFile, TerminalData.IPAddress ] ) + ' ( %s )' );
                    end
                    else
                        if ZetaCommonTools.StrLleno( sAlarmFile ) and not FileExists( sAlarmFile) then
                        begin
                             Bitacora.WriteTimeStamp( Format( 'Terminal %s: Archivo De Alarmas %s No Existe', [ TerminalData.IPAddress, sAlarmFile ] ) + ' ( %s )' );
                        end;
               end;
          end;
     end;
end;

procedure TLinx7Poll.SendEmpleados( const sFileName: String );
var
   i: Integer;
   sEmployeeFile: String;
   TerminalData: TTerminal;
begin
     with LinxServer do
     begin
          for i := 0 to ( ListaTerminales.Count - 1 ) do
          begin
               sEmployeeFile := VACIO;
               TerminalData := ListaTerminales.Terminal[ i ];  { Obtener datos de la terminal i }
               if TerminalData.Conectada then
               begin
                    if ZetaCommonTools.StrLleno( sFileName ) then
                       sEmployeeFile := sFileName
                    else
                    begin
                         if Assigned( TerminalData ) then
                         begin
                              sEmployeeFile := TerminalData.Empleados;
                         end;
                         if ZetaCommonTools.StrVacio( sEmployeeFile ) then
                         begin
                              sEmployeeFile := LinxIni.IniValues.Empleados;
                         end;
                    end;
                    if ZetaCommonTools.StrLleno( sEmployeeFile ) and FileExists( sEmployeeFile ) then
                    begin
                         SendEmployeeListFile( i, sEmployeeFile );
                         Bitacora.WriteTimeStamp( Format( 'Lista De Empleados %s Enviada A %s', [ sEmployeeFile, TerminalData.IPAddress ] ) + ' ( %s )' );
                    end
                    else
                        if ZetaCommonTools.StrLleno( sEmployeeFile ) and not FileExists( sEmployeeFile) then
                        begin
                             Bitacora.WriteTimeStamp( Format( 'Terminal %s: Lista De Empleados %s No Existe', [ TerminalData.IPAddress, sEmployeeFile ] ) + ' ( %s )' );
                        end;
               end;
          end;
     end;
end;

procedure TLinx7Poll.SendFlash( const sFileName: String );
var
   i: Integer;
   sFileExt: String;
   TerminalData: TTerminal;
begin
     with LinxServer do
     begin
          for i := 0 to ( ListaTerminales.Count - 1 ) do
          begin
              sFileExt := UpperCase(ExtractFileExt(sFileName));
              TerminalData := ListaTerminales.Terminal[ i ];  { Obtener datos de la terminal i }
              if TerminalData.Conectada then
               begin
                if ZetaCommonTools.StrLleno( sFileName ) and FileExists( sFileName ) then
                 begin
                  if ((sFileExt = K_FLASH_EXT1) or (sFileExt = K_FLASH_EXT2)) or
                     ( UpperCase(ExtractFileName(sFileName)) = K_REMOTE_COMMAND_FILE ) then
                   begin
                    //El archivo existe y la extension es correcta
                    SendDataFile( i , sFileName , ExtractFileName(sFileName) );
                    if ((sFileExt = K_FLASH_EXT1) or (sFileExt = K_FLASH_EXT2)) then
                     begin
                      Bitacora.WriteTimeStamp( Format( 'Terminal %s: El archivo %s fue enviado a la memoria flash', [ TerminalData.IPAddress, sFileName ] ) + ' ( %s )' );
                     end//if
                    else
                     begin
                      Bitacora.WriteTimeStamp( Format( 'Terminal %s: El archivo de comando %s fue enviado', [ TerminalData.IPAddress, sFileName ] ) + ' ( %s )' );
                      Sleep(8000);
                      UnitList.Selected[ i ] := true;
                      ExecuteCmd( LinxServer.RebootUnit );
                      Bitacora.WriteTimeStamp( Format( 'Terminal %s: La terminal fue reiniciada', [ TerminalData.IPAddress] ) + ' ( %s )' );
                     end;//else
                   end//if
                  else
                   begin
                    //El archivo contiene una extension incorrecta
                    Bitacora.WriteTimeStamp( Format( 'Terminal %s: El archivo %s no se puede enviar a la memoria flash por que es invalido', [ TerminalData.IPAddress, sFileName ] ) + ' ( %s )' );
                   end;//else
                 end//if ZetaCommonTools.StrLleno( sFileName ) and FileExists( sFileName ) then
                else
                 begin
                  if ZetaCommonTools.StrLleno( sFileName ) and not FileExists( sFileName ) then
                   begin
                    Bitacora.WriteTimeStamp( Format( 'Terminal %s: El archivo %s no existe', [ TerminalData.IPAddress, sFileName ] ) + ' ( %s )' );
                   end;//if ZetaCommonTools.StrLleno( sEmployeeFile ) and not FileExists( sEmployeeFile) then
                 end;//else
               end;//if TerminalData.Conectada then
          end;
     end;
end;

procedure TLinx7Poll.SendScript( const sFileName: String );
var
   i: Integer;
   TerminalData: TTerminal;
begin
     with LinxServer do
     begin
          for i := 0 to ( ListaTerminales.Count - 1 ) do
          begin
              TerminalData := ListaTerminales.Terminal[ i ];  { Obtener datos de la terminal i }
              if TerminalData.Conectada then
               begin
                if ZetaCommonTools.StrLleno( sFileName ) and FileExists( sFileName ) then
                 begin
                   //El archivo existe
                   SendDataFile( i , sFileName , ExtractFileName(sFileName) );
                   Bitacora.WriteTimeStamp( Format( 'Terminal %s: El archivo %s fue enviado a la memoria no volatil', [ TerminalData.IPAddress, sFileName ] ) + ' ( %s )' );
                 end//if ZetaCommonTools.StrLleno( sFileName ) and FileExists( sFileName ) then
                else
                 begin
                  if ZetaCommonTools.StrLleno( sFileName ) and not FileExists( sFileName ) then
                   begin
                    Bitacora.WriteTimeStamp( Format( 'Terminal %s: El archivo %s no existe', [ TerminalData.IPAddress, sFileName ] ) + ' ( %s )' );
                   end;//if ZetaCommonTools.StrLleno( sEmployeeFile ) and not FileExists( sEmployeeFile) then
                 end;//else
               end;//if TerminalData.Conectada then
          end;
     end;
end;

procedure TLinx7Poll.SendRebootCmd;
var
   i: Integer;
   TerminalData: TTerminal;
begin
     with LinxServer do
     begin
          for i := 0 to ( ListaTerminales.Count - 1 ) do
          begin
              TerminalData := ListaTerminales.Terminal[ i ];  { Obtener datos de la terminal i }
              if TerminalData.Conectada then
               begin
                 Bitacora.WriteTimeStamp( Format( 'Terminal %s: Comando reboot ejecutado', [ TerminalData.IPAddress] ) + ' ( %s )' );
                 UnitList.Selected[ i ] := true;
                 ExecuteCmd( LinxServer.RebootUnit );
               end;//if TerminalData.Conectada then
          end;
     end;
end;

procedure TLinx7Poll.SendFile( Command: FileSender; sFileName: String; lAskForFile: Boolean );
var
   i: Integer;
begin
     if ( UnitList.SelCount > 0 ) then
     begin
          if not lAskForFile OR GetFileName( sFileName ) then
          begin
               if FileExists( sFileName ) then
               begin
                    SetCursorWait;
                    for i := 0 to ( UnitList.Items.Count - 1 ) do
                    begin
                         if UnitList.Selected[ i ] then
                            Command( i, sFileName );
                    end;
                    SetCursorNormal;
               end
               else
                   zWarning( '� Archivo No Existe !', sFileName, 0, mbOK );
          end;
     end
     else
         ZetaDialogo.zWarning( '� No Se Escogi� Ninguna Terminal !', 'No Hay Ninguna Terminal Seleccionada', 0, mbOK );
end;

procedure TLinx7Poll.ExecuteCmd( Command: Executer );
var
   i: Integer;
begin
     if ( UnitList.SelCount > 0 ) then
     begin
          SetCursorWait;
          for i := 0 to ( UnitList.Items.Count - 1 ) do
          begin
               if UnitList.Selected[ i ] then
               begin
                    Command( i );
               end;
          end;
          SetCursorNormal;
     end
     else
         ZetaDialogo.zWarning( '� No Se Escogi� Ninguna Terminal !', 'No Hay Ninguna Terminal Seleccionada', 0, mbOK );
end;

{ ************ Eventos del Men� *********** }

procedure TLinx7Poll.ConfiguracionClick(Sender: TObject);
begin
     inherited;
     ConfiguracionArchivos.Enabled := not Recolectando;
end;

procedure TLinx7Poll.ConfiguracionArchivosClick(Sender: TObject);
begin
     inherited;
     with TArchivos.Create( Self ) do
     begin
          try
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TLinx7Poll.ConfiguracionListaClick(Sender: TObject);
begin
     inherited;
     with TCapturarTerminales.Create( Self ) do
     begin
          try
             LinxServer := Self.LinxServer;
             ShowModal;
             if ( ModalResult = mrOk ) and Recolectando then
             begin
                  ZetaDialogo.zInformation( '� Atenci�n !',
                                            'Para Que La Nueva Lista de Terminales' +
                                            CR_LF +
                                            'Sea Aplicada, Es Necesario Reiniciar' +
                                            CR_LF +
                                            'La Recolecci�n de Datos',
                                            0 );
             end;
          finally
                 Free;
          end;
     end;
end;

procedure TLinx7Poll.TerminalesProgramaClick(Sender: TObject);
begin
     inherited;
     SendFile( LinxServer.SendApplicationFile, IniValues.Programa, True );
end;

procedure TLinx7Poll.TerminalesAlarmaClick(Sender: TObject);
begin
     //ZetaMessage('L7Poll','Funci�n no disponible.',mtInformation,mbOKCancel,0,mbOK);
     inherited;
     SendFile( LinxServer.SendParameterFile, IniValues.Alarma, True );
end;

procedure TLinx7Poll.TerminalesEmpleadosClick(Sender: TObject);
begin
     //ZetaMessage('L7Poll','Funci�n no disponible.',mtInformation,mbOKCancel,0,mbOK);
     inherited;
     SendFile( LinxServer.SendEmployeeListFile, IniValues.Empleados, True );
end;

procedure TLinx7Poll.ArchivoRecolectarClick(Sender: TObject);
const
     K_ARCHIVO_DEMO = '\DEMO.DAT'; //Modificar tambien en TL7Server.DataDownload
var
   strArchivoDEMO : string;
   sMensajeErrorHTTP: String;
begin
     if not ValidaConexionHTTP( sMensajeErrorHTTP ) then
     begin
          if PuedeRecolectar then
          begin
               if EsDemo then
               begin
                    //Asignar bandera DEMO y Batch
                    LinxServer.EsDemo := EsDemo;
                    LinxServer.BatchMode := False;
               end//if EsDemo then
               else
               begin
                    //Borrar Archivo DEMO Local
                    strArchivoDEMO := ZetaCommonTools.VerificaDir( ExtractFileDir( Application.ExeName ) ) + K_ARCHIVO_DEMO;
                    if FileExists( strArchivoDEMO ) then
                    begin
                         DeleteFile( strArchivoDEMO );
                    end;
               end;//else

               //Continuar con el proceso normal
               inherited;

          end//if PuedeRecolectar then
          else
          begin
               ZetaDialogo.zInformation( '� Atenci�n !',
                                         'Para realizar la Recolecci�n de checadas es' +
                                         CR_LF +
                                         'necesario tener una lista de Terminales',
                                         0 );
          end;//else
     end
     else
          ZetaDialogo.zInformation( Application.Title, sMensajeErrorHTTP, 0 );
end;

procedure TLinx7Poll.IncrementaLecturas;
begin
     inherited;
     LinxServer.ChecadasDemo := FLecturas;
end;

function TLinx7Poll.PuedeRecolectar: Boolean;
begin
     Result := not EsDemo or PuedeEjecutarEnDemo;
end;

function TLinx7Poll.PuedeEjecutarEnDemo: Boolean;
begin
     Result := EsDemo;
end;

procedure TLinx7Poll.TerminalesDirectorioClick(Sender: TObject);
var
   i, iSelected: Integer;
begin
     inherited;
     if ( UnitList.SelCount = 0 ) then
         ZetaDialogo.zWarning( '� No Se Escogi� Ninguna Terminal !', 'No Hay Ninguna Terminal Seleccionada', 0, mbOK )
     else
         if ( UnitList.SelCount > 1 ) then
            ZetaDialogo.zWarning( '� Demasiadas Terminales !', 'Se Debe Escoger Una Sola Terminal', 0, mbOK )
         else
         begin
              iSelected := -1;
              SetCursorWait;
              for i := 0 to ( UnitList.Items.Count - 1 ) do
              begin
                   if UnitList.Selected[ i ] then
                   begin
                        iSelected := i;
                        Break;
                   end;
              end;
              SetCursorNormal;
              FileManager := TFileManager.Create( Self );
              try
                 with FileManager do
                 begin
                      TerminalIndex := iSelected;
                      LinxServer := Self.LinxServer;
                      CargarArchivos;
                      ShowModal;
                 end;
              finally
                     FreeAndNil( FileManager );
              end;
         end;
end;

procedure TLinx7Poll.TerminalesFechaYHoraClick(Sender: TObject);
begin
     inherited;
     ExecuteCmd( LinxServer.ShowDateTime );
end;

procedure TLinx7Poll.TerminalesVersionClick(Sender: TObject);
var
   i: Integer;
begin
     inherited;
     if ( UnitList.SelCount = 0 ) then
         ZetaDialogo.zWarning( '� No Se Escogi� Ninguna Terminal !', 'No Hay Ninguna Terminal Seleccionada', 0, mbOK )
     else
         if ( UnitList.SelCount > 1 ) then
            ZetaDialogo.zWarning( '� Demasiadas Terminales !', 'Se Debe Escoger Una Sola Terminal', 0, mbOK )
         else
         begin
              SetCursorWait;
              for i := 0 to ( UnitList.Items.Count - 1 ) do
              begin
                   if UnitList.Selected[ i ] then
                   begin
                        LinxServer.ShowVersion( i );
                        Break;
                   end;
              end;
              SetCursorNormal;
         end;
end;

procedure TLinx7Poll.TerminalesSincronizarClick(Sender: TObject);
var
   i: Integer;
   dDateTime: TDateTime;
   lHoraSistema: Boolean;
begin
     inherited;
     if ( UnitList.SelCount > 0 ) then
     begin
          dDateTime := Now;
          if FSincronizar.GetDateTime( dDateTime, TRUE, lHoraSistema ) then
          begin
               SetCursorWait;
               for i := 0 to ( UnitList.Items.Count - 1 ) do
               begin
                    if UnitList.Selected[ i ] then
                    begin
                         if lHoraSistema then
                            dDateTime := Now;
                         LinxServer.SetDateTime( i, dDateTime );
                    end;
               end;
               SetCursorNormal;
          end;
     end
     else
         ZetaDialogo.zWarning( '� No Se Escogi� Ninguna Terminal !', 'No Hay Ninguna Terminal Seleccionada', 0, mbOK );
end;

procedure TLinx7Poll.TerminalesSincronizarTodasClick(Sender: TObject);
var
   dDateTime: TDateTime;
   lHoraSistema: Boolean;
begin
     inherited;
     dDateTime := Now;
     if FSincronizar.GetDateTime( dDateTime, FALSE, lHoraSistema ) then
     begin
          SetCursorWait;
          LinxServer.SetDateTimeAll( dDateTime );
          SetCursorNormal;
     end;
end;

procedure TLinx7Poll.TerminalesResetearClick(Sender: TObject);
begin
     inherited;
     if ZetaDialogo.zConfirm( '� Atencion !', '� Desea Resetear La Terminal Seleccionada?', 0, mbOK ) then
     begin
          ExecuteCmd( LinxServer.ResetUnit );
     end;
end;

procedure TLinx7Poll.TerminalesRebootClick(Sender: TObject);
begin
     inherited;
     if ZetaDialogo.zConfirm( '� Atencion !', '� Desea Rebootear La Terminal Seleccionada?', 0, mbOK ) then
     begin
          ExecuteCmd( LinxServer.RebootUnit );
     end;
end;

procedure TLinx7Poll.ProcesaParametro( const sParametro: String );
begin
     //Asignar bandera DEMO y Batch
     LinxServer.EsDemo := EsDemo;
     LinxServer.BatchMode := True;
     inherited;
end;

procedure TLinx7Poll.ProcesaParametro( const sParametro, sArchivo: String );
begin
     //Asignar bandera DEMO y Batch
     LinxServer.EsDemo := EsDemo;
     LinxServer.BatchMode := True;
     inherited;
end;

procedure TLinx7Poll.EscribeLecturaLinea(const sData: String);
begin
     Bitacora.WriteTimeStamp( sData );
end;

procedure TLinx7Poll.ArchivoTerminarClick(Sender: TObject);
begin
     inherited;
     Terminar.Enabled := False;
end;

end.
