inherited CapturarTerminales: TCapturarTerminales
  Caption = 'Capturar Lista de Terminales Linx VII'
  ClientHeight = 372
  ClientWidth = 562
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 336
    Width = 562
    TabOrder = 2
    inherited OK: TBitBtn
      Left = 394
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 479
    end
  end
  object PanelControles: TPanel
    Left = 241
    Top = 0
    Width = 321
    Height = 336
    Align = alClient
    TabOrder = 1
    object PanelDescripcion: TPanel
      Left = 1
      Top = 1
      Width = 319
      Height = 78
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object DescriptionLBL: TLabel
        Left = 9
        Top = 14
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descripci'#243'n:'
        FocusControl = Description
        ParentShowHint = False
        ShowHint = True
      end
      object IdentificadorLBL: TLabel
        Left = 7
        Top = 37
        Width = 61
        Height = 13
        Alignment = taRightJustify
        Caption = 'Identificador:'
        FocusControl = Identificador
        ParentShowHint = False
        ShowHint = True
      end
      object LetreroLBL: TLabel
        Left = 32
        Top = 60
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Letrero:'
        FocusControl = Letrero
        ParentShowHint = False
        ShowHint = True
      end
      object Identificador: TEdit
        Left = 71
        Top = 33
        Width = 52
        Height = 21
        Hint = 'Identificador de 4 caracteres para mostrar en checadas'
        CharCase = ecUpperCase
        MaxLength = 4
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnChange = HayCambios
        OnClick = IdentificadorEnter
        OnEnter = IdentificadorEnter
      end
      object Description: TEdit
        Left = 71
        Top = 10
        Width = 162
        Height = 21
        Hint = 'Descripci'#243'n de la terminal'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnChange = HayCambiosDescripcion
      end
      object Configurar: TBitBtn
        Left = 243
        Top = 9
        Width = 63
        Height = 55
        Hint = 'Invocar pantalla de configuraci'#243'n de Terminal Linx VII'
        Caption = 'Configurar'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = ConfigurarClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555550FF0559
          1950555FF75F7557F7F757000FF055591903557775F75557F77570FFFF055559
          1933575FF57F5557F7FF0F00FF05555919337F775F7F5557F7F700550F055559
          193577557F7F55F7577F07550F0555999995755575755F7FFF7F5570F0755011
          11155557F755F777777555000755033305555577755F75F77F55555555503335
          0555555FF5F75F757F5555005503335505555577FF75F7557F55505050333555
          05555757F75F75557F5505000333555505557F777FF755557F55000000355557
          07557777777F55557F5555000005555707555577777FF5557F55553000075557
          0755557F7777FFF5755555335000005555555577577777555555}
        Layout = blGlyphTop
        NumGlyphs = 2
      end
      object Letrero: TEdit
        Left = 71
        Top = 56
        Width = 162
        Height = 21
        Hint = 'Letrero a mostrar en pantalla de terminal'
        CharCase = ecUpperCase
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnChange = HayCambios
      end
    end
    object gbConexion: TGroupBox
      Left = 1
      Top = 79
      Width = 319
      Height = 146
      Align = alTop
      Caption = ' Conexi'#243'n '
      TabOrder = 1
      object DireccionLBL: TLabel
        Left = 7
        Top = 21
        Width = 61
        Height = 13
        Alignment = taRightJustify
        Caption = 'Direcci'#243'n IP:'
        FocusControl = DireccionIP
      end
      object UsuarioLBL: TLabel
        Left = 29
        Top = 67
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Usuario:'
        FocusControl = Usuario
        ParentShowHint = False
        ShowHint = True
      end
      object PasswordLBL: TLabel
        Left = 11
        Top = 90
        Width = 57
        Height = 13
        Alignment = taRightJustify
        Caption = 'Contrase'#241'a:'
        FocusControl = Password
        ParentShowHint = False
        ShowHint = True
      end
      object ConfirmacionLBL: TLabel
        Left = 4
        Top = 112
        Width = 64
        Height = 13
        Alignment = taRightJustify
        Caption = 'Confirmaci'#243'n:'
        FocusControl = Confirmacion
        ParentShowHint = False
        ShowHint = True
      end
      object PuertoLBL: TLabel
        Left = 35
        Top = 44
        Width = 34
        Height = 13
        Alignment = taRightJustify
        Caption = 'Puerto:'
        FocusControl = Puerto
        ParentShowHint = False
        ShowHint = True
      end
      object DireccionIP: TMaskEdit
        Left = 72
        Top = 17
        Width = 97
        Height = 21
        Hint = 'Direcci'#243'n de TCP/IP asignada a la terminal'
        EditMask = '000\.000\.000\.000;0'
        MaxLength = 15
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Text = '000000000000'
        OnChange = HayCambios
        OnKeyPress = DireccionIPKeyPress
      end
      object Usuario: TEdit
        Left = 72
        Top = 63
        Width = 209
        Height = 21
        Hint = 'Usuario con el cual se puede accesar FTP de terminal'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnChange = HayCambios
      end
      object Password: TEdit
        Left = 72
        Top = 86
        Width = 209
        Height = 21
        Hint = 'Contrase'#241'a del usuario de FTP de la terminal'
        ParentShowHint = False
        PasswordChar = '*'
        ShowHint = True
        TabOrder = 3
        OnChange = HayCambiosPassword
      end
      object Confirmacion: TEdit
        Left = 72
        Top = 108
        Width = 209
        Height = 21
        Hint = 'Confirmaci'#243'n de la constrase'#241'a'
        ParentShowHint = False
        PasswordChar = '*'
        ShowHint = True
        TabOrder = 4
        OnChange = HayCambios
      end
      object Puerto: TZetaNumero
        Left = 72
        Top = 40
        Width = 57
        Height = 21
        Hint = 'Puerto de TCP/IP a trav'#233's del cual se comunica la terminal'
        Mascara = mnEmpleado
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
    end
    object gbArchivos: TGroupBox
      Left = 1
      Top = 225
      Width = 319
      Height = 110
      Align = alClient
      Caption = ' Archivos '
      TabOrder = 2
      object ProgramaLBL: TLabel
        Left = 21
        Top = 21
        Width = 48
        Height = 13
        Alignment = taRightJustify
        Caption = 'Programa:'
        FocusControl = Programa
        ParentShowHint = False
        ShowHint = True
      end
      object BuscarPrograma: TSpeedButton
        Left = 286
        Top = 45
        Width = 23
        Height = 24
        Hint = 'Buscar Programa LXE'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
          300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
          330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
          333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
          339977FF777777773377000BFB03333333337773FF733333333F333000333333
          3300333777333333337733333333333333003333333333333377333333333333
          333333333333333333FF33333333333330003333333333333777333333333333
          3000333333333333377733333333333333333333333333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = BuscarAlarmaClick
      end
      object BuscarAlarma: TSpeedButton
        Left = 286
        Top = 74
        Width = 23
        Height = 24
        Hint = 'Buscar Archivo ALARMA.VAL'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
          300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
          330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
          333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
          339977FF777777773377000BFB03333333337773FF733333333F333000333333
          3300333777333333337733333333333333003333333333333377333333333333
          333333333333333333FF33333333333330003333333333333777333333333333
          3000333333333333377733333333333333333333333333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = BuscarEmpleadosClick
      end
      object BuscarEmpleados: TSpeedButton
        Left = 286
        Top = 17
        Width = 23
        Height = 24
        Hint = 'Buscar Archivo EMPLEADOS.VAL'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
          300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
          330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
          333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
          339977FF777777773377000BFB03333333337773FF733333333F333000333333
          3300333777333333337733333333333333003333333333333377333333333333
          333333333333333333FF33333333333330003333333333333777333333333333
          3000333333333333377733333333333333333333333333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = BuscarProgramaClick
      end
      object AlarmaLBL: TLabel
        Left = 34
        Top = 50
        Width = 35
        Height = 13
        Alignment = taRightJustify
        Caption = 'Alarma:'
        FocusControl = Alarma
        ParentShowHint = False
        ShowHint = True
      end
      object EmpleadosLBL: TLabel
        Left = 13
        Top = 77
        Width = 55
        Height = 13
        Alignment = taRightJustify
        Caption = 'Empleados:'
        FocusControl = Empleados
        ParentShowHint = False
        ShowHint = True
      end
      object Programa: TEdit
        Left = 72
        Top = 18
        Width = 213
        Height = 21
        Hint = 'Programa LXE asignado a la terminal'
        CharCase = ecUpperCase
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnChange = HayCambios
        OnClick = IdentificadorEnter
        OnEnter = IdentificadorEnter
      end
      object Empleados: TEdit
        Left = 72
        Top = 74
        Width = 213
        Height = 21
        Hint = 'Lista de empleados contra la cual validar las checadas '
        CharCase = ecUpperCase
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnChange = HayCambios
        OnClick = IdentificadorEnter
        OnEnter = IdentificadorEnter
      end
      object Alarma: TEdit
        Left = 72
        Top = 46
        Width = 213
        Height = 21
        Hint = 
          'Lista de horas para disparar '#39'relays'#39' (alarmas, chicharras, etc.' +
          ')'
        CharCase = ecUpperCase
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnChange = HayCambios
        OnClick = IdentificadorEnter
        OnEnter = IdentificadorEnter
      end
    end
  end
  object PanelLista: TPanel
    Left = 0
    Top = 0
    Width = 241
    Height = 336
    Align = alLeft
    TabOrder = 0
    object TerminalList: TListBox
      Left = 1
      Top = 1
      Width = 239
      Height = 299
      Align = alClient
      ItemHeight = 13
      TabOrder = 0
      OnClick = TerminalListClick
    end
    object PanelABC: TPanel
      Left = 1
      Top = 300
      Width = 239
      Height = 35
      Align = alBottom
      TabOrder = 1
      object Agregar: TBitBtn
        Left = 4
        Top = 4
        Width = 77
        Height = 27
        Hint = 'Agregar Rengl'#243'n'
        Caption = '&Agregar'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = AgregarClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          33333333FF33333333FF333993333333300033377F3333333777333993333333
          300033F77FFF3333377739999993333333333777777F3333333F399999933333
          33003777777333333377333993333333330033377F3333333377333993333333
          3333333773333333333F333333333333330033333333F33333773333333C3333
          330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
          993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
          333333333337733333FF3333333C333330003333333733333777333333333333
          3000333333333333377733333333333333333333333333333333}
        NumGlyphs = 2
      end
      object Borrar: TBitBtn
        Left = 83
        Top = 4
        Width = 77
        Height = 27
        Hint = 'Borrar Rengl'#243'n'
        Caption = '&Borrar'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BorrarClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
          55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
          305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
          005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
          B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
          B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
          B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
          B0557777FF577777F7F500000E055550805577777F7555575755500000555555
          05555777775555557F5555000555555505555577755555557555}
        NumGlyphs = 2
      end
      object Modificar: TBitBtn
        Left = 163
        Top = 4
        Width = 71
        Height = 27
        Caption = '&Modificar'
        TabOrder = 2
        OnClick = ModificarClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
          000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
          00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
          F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
          0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
          FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
          FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
          0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
          00333377737FFFFF773333303300000003333337337777777333}
        NumGlyphs = 2
      end
    end
  end
  object OpenDialog: TOpenDialog
    Left = 104
    Top = 16
  end
end
