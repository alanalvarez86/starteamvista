unit FArchivos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls,
     ZBaseDlgModal, LinxIni, FileCTRL, Mask, ZetaNumero;

type
  TArchivos = class(TZetaDlgModal)
    AlarmaLBL: TLabel;
    EmpleadosLBL: TLabel;
    AlarmaSeek: TSpeedButton;
    EmpleadosSeek: TSpeedButton;
    Empleados: TEdit;
    Alarma: TEdit;
    OpenDialog: TOpenDialog;
    ProgramaLBL: TLabel;
    Programa: TEdit;
    ProgramaSeek: TSpeedButton;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Pausa: TEdit;
    Reinicio: TEdit;
    PausaSeek: TSpeedButton;
    ReinicioSeek: TSpeedButton;
    Label3: TLabel;
    TimeOut: TZetaNumero;
    Label4: TLabel;
    procedure FormShow(Sender: TObject);
    procedure AlarmaSeekClick(Sender: TObject);
    procedure EmpleadosSeekClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure ProgramaSeekClick(Sender: TObject);
    procedure PausaSeekClick(Sender: TObject);
    procedure ReinicioSeekClick(Sender: TObject);
  private
    { Private declarations }
    function BuscaArchivo( const sFileName, sFilter, sTitle: String ): String;
    function BuscaDirectorio( const sDirectorio: String ): String;
  public
    { Public declarations }
  end;

var
  Archivos: TArchivos;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo;

{$R *.DFM}

procedure TArchivos.FormShow(Sender: TObject);
begin
     inherited;
     with LinxIni.IniValues do
     begin
          Self.Alarma.Text := Alarma;
          Self.Empleados.Text := Empleados;
          Self.Programa.Text := Programa;
          Self.Pausa.Text := DirPausa;
          Self.Reinicio.Text := DirReinicio;
          if( TimeOut > 0 )then
              Self.TimeOut.Valor := TimeOut
          else
              Self.TimeOut.Valor := 120;
     end;
end;

function TArchivos.BuscaArchivo( const sFileName, sFilter, sTitle: String ): String;
begin
     with OpenDialog do
     begin
          Title := sTitle;
          FileName := sFileName;
          Filter := sFilter;
          FilterIndex := 0;
          if Execute then
             Result := FileName
          else
              Result := sFileName;
     end;
end;

function TArchivos.BuscaDirectorio( const sDirectorio: String ): String;
var
   sDirLocal: string;
begin
     Result := VACIO;
     sDirLocal := sDirectorio;
     if SelectDirectory( sDirLocal, [ sdAllowCreate, sdPerformCreate, sdPrompt ], 0 )then
        Result := sDirLocal;
end;

procedure TArchivos.ProgramaSeekClick(Sender: TObject);
begin
     inherited;
     Programa.Text := BuscaArchivo( Programa.Text, 'Configuración Linx7|*.lxe|Todos los Archivos|*.*', 'Archivo de Configuración para Aplicaciones de Linx7' );
end;

procedure TArchivos.AlarmaSeekClick(Sender: TObject);
begin
     inherited;
     Alarma.Text := BuscaArchivo( Alarma.Text, 'Alarmas|*.val|Todos los Archivos|*.*', 'Archivo de Alarma para Aplicaciones de Linx7' );
end;

procedure TArchivos.EmpleadosSeekClick(Sender: TObject);
begin
     inherited;
     Empleados.Text := BuscaArchivo( Empleados.Text, 'Empleado|*.val|Todos los Archivos|*.*', 'Archivo de Empleados para Aplicaciones de Linx7' );
end;

procedure TArchivos.PausaSeekClick(Sender: TObject);
begin
     inherited;
     Pausa.Text := BuscaDirectorio( Pausa.Text );
end;

procedure TArchivos.ReinicioSeekClick(Sender: TObject);
begin
     inherited;
     Reinicio.Text := BuscaDirectorio( Reinicio.Text );
end;

procedure TArchivos.OKClick(Sender: TObject);
begin
     inherited;
     // Por default, negar el funcionamiento de OK
     ModalResult := mrNone;

     if StrLleno( Pausa.Text )then
     begin
          if StrLleno( Reinicio.Text )then
          begin
               if( Pausa.Text <> Reinicio.Text )then
               begin
                    if( TimeOut.ValorEntero > 0 )then
                    begin
                         if( TimeOut.ValorEntero <= 600 )then
                         begin
                              with LinxIni.IniValues do
                              begin
                                   Alarma := Self.Alarma.Text;
                                   Empleados := Self.Empleados.Text;
                                   Programa := Self.Programa.Text;
                                   DirPausa := Self.Pausa.Text;
                                   DirReinicio := Self.Reinicio.Text;
                                   TimeOut := Self.TimeOut.ValorEntero;
                                   // funcionamiento de OK
                                   ModalResult := mrOK;
                              end;
                         end
                         else
                         begin
                              TimeOut.SetFocus;
                              zError( Self.Caption, 'El valor de TimeOut no puede exceder 600 segundos', 0 );
                         end;
                    end
                    else
                    begin
                         TimeOut.SetFocus;
                         zError( Self.Caption, 'El valor de TimeOut debe ser mayor a 0', 0 );
                    end;
               end
               else
               begin
                    Reinicio.SetFocus;
                    zError( Self.Caption, 'Los directorios de Pausa y Reinicio deben ser distintos', 0 );
               end;
          end
          else
          begin
               Reinicio.SetFocus;
               zError( Self.Caption, 'Es necesario especificar el directorio de Reinicio', 0 );
          end;
     end
     else
     begin
          Pausa.SetFocus;
          zError( Self.Caption, 'Es necesario especificar el directorio de Pausa', 0 );
     end;
end;

end.
