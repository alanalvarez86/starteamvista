unit LinxDll;

interface

uses Classes, SysUtils, WinTypes, WinProcs, Forms,
     IdBaseComponent, IdComponent, IdTCPConnection,
     IdTCPClient, IdFTP, IdFTPCommon, IdGlobal, IdEcho, Dialogs,
     LinxIni;

type
    eDataStatus = ( edsOK, edsStatus, edsGarbage );
    TMakeUnitListEvent = procedure of object;
    TFeedBackEvent = procedure( const sMensaje: String; iTerminal: Integer ) of object;
    TGaugeEvent = procedure( const sMsg: String; iValue: Integer ) of object;
    TLecturaEvent = procedure( const sData: String ) of object;
    TL7Server = class( TObject )
    private
      { Private declarations }
      FTerminalActiva: Integer;
      FUnitListMade: TDateTime;
      FValidTokens: String;
      FOnFeedBack: TFeedBackEvent;
      FOnGaugeSetSize: TGaugeEvent;
      FOnGaugeAdvance: TGaugeEvent;
      FOnGaugeFinish: TGaugeEvent;
      FOnMakeUnitList: TMakeUnitListEvent;
      FOnDoLectura: TLecturaEvent;
      FIdFTP: TIdFTP;
      FIdTCP: TIdTCPClient;
      FTempFile: String;
      FBufferFile: String;
      FData: String;
      FEsDemo: Boolean;
      FBatchMode: Boolean;
      FChecadasDemo: integer;
      FDataBuffer: TStrings;
      FDataRaw: TStrings;
      FDataRawItems: TStrings;
      FDataStatus: eDataStatus;
      FTerminalPtr: Integer;
      FIntervalo: TDateTime;
      function GetDatosEnviados: String;
      function GetListaTerminales: TTerminals;
      function GetRegistroEsNormal: Boolean;
      function GetRegistroLeido: String;
      function GetTime( Terminal: TTerminal ): TDateTime;
      function PacketLength( const sPacket: String ): String;
      function SendCommand( Terminal: TTerminal; const sCommand: String ): Boolean;
      function SendQuery( Terminal: TTerminal; const sQuery: String; out sResponse: String ): Boolean;
      function SetTime( Terminal: TTerminal; const dValue: TDateTime ): Boolean;
      procedure SetValidTokens( const Value: String );
      procedure DoFeedBack( const iTerminal: Integer; sMsg: String );
      procedure EscribeBitacora( const sMensaje : string );
      procedure SiguienteTerminal;
    protected
      { Protected declarations }
      function ConectarFTP( Terminal: TTerminal ): Boolean;
      function ConectarTCP( Terminal: TTerminal ): Boolean;
      function DataDownload( Terminal: TTerminal ): Boolean;
      function DataTransform: Boolean;
      function DataTransformItem( var sData: String ): eDataStatus;
      function DesconectarFTP( Terminal: TTerminal ): Boolean;
      function DesconectarTCP( Terminal: TTerminal ): Boolean;
      function GetTerminalInfo( const iTerminal: Integer ): TTerminal;
      procedure DoGaugeSetSize( const sMsg: String; iSize: Integer );
      procedure DoGaugeAdvance( const sMsg: String; iStep: Integer );
      procedure DoGaugeFinish( const sMsg: String; iValue: Integer );
      procedure DelayFTP(const Tiempo : integer = 500);
      procedure DelayTCP(const Tiempo : integer = 500);
    public
      { Public declarations }
      constructor Create;
      destructor Destroy; override;
      property EsDemo: Boolean read FEsDemo write FEsDemo;
      property BatchMode: Boolean read FBatchMode write FBatchMode;
      property ChecadasDemo: integer read FChecadasDemo write FChecadasDemo;
      property RegistroNormal: Boolean read GetRegistroEsNormal;
      property RegistroLeido: String read GetRegistroLeido;
      property DatosEnviados: String read GetDatosEnviados;
      property Terminal: Integer read FTerminalActiva;
      property ValidTokens: String write SetValidTokens;
      property OnFeedBack: TFeedBackEvent read FOnFeedBack write FOnFeedBack;
      property OnGaugeSetSize: TGaugeEvent read FOnGaugeSetSize write FOnGaugeSetSize;
      property OnGaugeAdvance: TGaugeEvent read FOnGaugeAdvance write FOnGaugeAdvance;
      property OnGaugeFinish: TGaugeEvent read FOnGaugeFinish write FOnGaugeFinish;
      property OnMakeUnitList: TMakeUnitListEvent read FOnMakeUnitList write FOnMakeUnitList;
      property OnDoLectura: TLecturaEvent read FOnDoLectura write FOnDoLectura;
      property ListaTerminales: TTerminals read GetListaTerminales;
      function BorrarArchivo( const iTerminal: Integer; sFileName: String ): Boolean;
      function GetDataFile( const iTerminal: Integer; sFileName, sInternalName: String ): Boolean;
      function GetSerialNumber( const iTerminal: Integer ): String;
      function Init: Boolean; virtual;
      function Read: Boolean; virtual;
      procedure AbortApp( const iTerminal: Integer );
      procedure CargarDirectorio( const iTerminal: Integer; Lista: TStrings );
      procedure Close; virtual;
      procedure MakeNodeList;
      procedure SendApplicationFile( const iTerminal: Integer; sFileName: String );
      procedure SendDataFile( const iTerminal: Integer; sFileName, sInternalName: String );
      procedure SendEmployeeListFile( const iTerminal: Integer; sFileName: String );
      procedure SendParameterFile( const iTerminal: Integer; sFileName: String );
      procedure ResetUnit( const iTerminal: Integer );
      procedure RebootUnit( const iTerminal: Integer );
      procedure RestartUnit( const iTerminal: Integer );
      procedure SetDateTime( const iTerminal: Integer; const dValue: TDateTime );
      procedure SetDateTimeAll( const dValue: TDateTime );
      procedure ShowDateTime( const iTerminal: Integer );
      procedure ShowVersion( const iTerminal: Integer );
      procedure Verify;
    end;
    TL7DummyServer = class( TL7Server )
    private
      { Private declarations }
      FLista: TStrings;
      FPtr: Integer;
    public
      { Public declarations }
      constructor Create;
      destructor Destroy; override;
      function Init: Boolean; override;
      function Read: Boolean; override;
      procedure MakeNodeList;
      procedure Close; override;
    end;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo,
     LinxIniBase,
     FLinxTools, Windows;

const

     PARAMETER_FILE_NAME = 'BEL.CFG';
     EMPLOYEE_LIST_FILE_NAME = 'EMP.TXT';
     {$ifdef ANTES}
     APPLICATION_FILE_NAME = 'CLK.CFG';
     {$else}
     APPLICATION_FILE_NAME = 'Script.txt';
     {$endif}

     K_UNKNOWN = '???';
     K_TIMEOUT = -1; { Default timeout }
     K_PRIMERA_TERMINAL = 0;
     K_DIRECTORY = 'D:';
     K_FLASH_EXT1 = '.S19';
     K_FLASH_EXT2 = '.S39';
     K_REMOTE_COMMAND_FILE = 'RMTCNTRL.TXT';

     QUERY_TYPE_COMMAND = '>';
     QUERY_TYPE_MESSAGE = '<';
     QUERY_TYPE_QUERY = '?';
     QUERY_TYPE_RESPONSE = '!';

     QUERY_DEV_ID_GENERAL = 0;
     QUERY_DEV_ID_DISPLAY = 1;
     QUERY_DEV_ID_KEYBOARD = 2;
     QUERY_DEV_ID_BARCODE = 3;
     QUERY_DEV_ID_MAGSWIPE = 4;
     QUERY_DEV_ID_SENSE_LINES = 5;
     QUERY_DEV_ID_LEDS = 6;
     QUERY_DEV_ID_CONTROL_LINES = 7;
     QUERY_DEV_ID_SPEAKER = 8;
     QUERY_DEV_ID_COM1 = 9;
     QUERY_DEV_ID_COM2 = 10;
     QUERY_DEV_ID_PROXIMITY_READER = 11;
     QUERY_DEV_ID_APPLICATION = 12;
     QUERY_DEV_ID_FINGERPRINT_ID_UNIT = 13;
     QUERY_OPTION_ERROR = $FF;
     QUERY_OPTION_ENCRYPTION = $80;
     QUERY_OPTION_NORMAL = $7F;

     TRM_QRY_GET_VERSION = 2;
     TRM_QRY_GET_TIME = 3;
     TRM_QRY_GET_SERIAL = 4;
     TRM_QRY_GET_APP_INFO = 5;

     TRM_CMD_REBOOT = 2;
     TRM_CMD_RESET_DEFAULTS = 3;
     TRM_CMD_SET_TIME = 4;
     TRM_CMD_EXE_APP = 5;
     TRM_CMD_HALT_APP = 6;

     K_QUERY_GET_VERSION = Chr( QUERY_DEV_ID_GENERAL ) + Chr( TRM_QRY_GET_VERSION );
     K_QUERY_GET_TIME = Chr( QUERY_DEV_ID_GENERAL ) + Chr( TRM_QRY_GET_TIME );
     K_QUERY_GET_SERIAL = Chr( QUERY_DEV_ID_GENERAL ) + Chr( TRM_QRY_GET_SERIAL );
     K_QUERY_GET_APP_INFO = Chr( QUERY_DEV_ID_GENERAL ) + Chr( TRM_QRY_GET_APP_INFO );

     K_COMMAND_REBOOT = Chr( QUERY_DEV_ID_GENERAL ) + Chr( TRM_CMD_REBOOT );
     K_COMMAND_RESET_DEFAULTS = Chr( QUERY_DEV_ID_GENERAL ) + Chr( TRM_CMD_RESET_DEFAULTS );
     K_COMMAND_SET_TIME = Chr( QUERY_DEV_ID_GENERAL ) + Chr( TRM_CMD_SET_TIME );
     K_COMMAND_APP_START = Chr( QUERY_DEV_ID_GENERAL ) + Chr( TRM_CMD_EXE_APP );
     K_COMMAND_APP_STOP = Chr( QUERY_DEV_ID_GENERAL ) + Chr( TRM_CMD_HALT_APP );

     K_TIEMPO_DELAY = 500;

{ ******* TL7Server ********* }

constructor TL7Server.Create;
const
     FTP_DEFAULT_PORT = 21;
     ANCHO_MAXIMO = $4000; { $3FFF + 1 }
     BUFFER_SIZE = $8000; {7FFF + 1 }
     K_BUFFER_FILE = 'BUFFER.DAT';
     K_TEMP_FILE = 'TEMP.DAT';
begin
     inherited Create;
     FValidTokens := '';
     FUnitListMade := NullDateTime;
     FIdFTP := TIdFTP.Create( nil );
     with FIdFTP do
     begin
          {
          Asegurarnos que el componente de Indy
          se inicialize igual que en Dise�o
          }
          MaxLineAction := maException;
          ASCIIFilter := True;
          MaxLineLength := ANCHO_MAXIMO;
          Passive := true;
          Port := FTP_DEFAULT_PORT;
          RecvBufferSize := BUFFER_SIZE;
          SendBufferSize := BUFFER_SIZE;
          TransferType := ftBinary;
          Name := 'IdFTP';
     end;
     FIdTCP := TIdTCPClient.Create( nil );
     with FIdTCP do
     begin
          {
          Asegurarnos que el componente de Indy
          se inicialize igual que en Dise�o
          }
          ASCIIFilter := False;
          MaxLineAction := maException;
          MaxLineLength := ANCHO_MAXIMO;
          RecvBufferSize := BUFFER_SIZE;
          SendBufferSize := BUFFER_SIZE;
          Name := 'IdTCP';
     end;
     FDataBuffer := TStringList.Create;
     FDataRaw := TStringList.Create;
     FDataRawItems := TStringList.Create;
     FBufferFile := ZetaCommonTools.VerificaDir( ExtractFilePath( Application.ExeName ) ) + K_BUFFER_FILE;
     FTempFile := ZetaCommonTools.VerificaDir( ExtractFilePath( Application.ExeName ) ) + K_TEMP_FILE;
     FIntervalo := NullDateTime;
end;

destructor TL7Server.Destroy;
begin
     Close;
     FreeAndNil( FDataRawItems );
     FreeAndNil( FDataRaw );
     FreeAndNil( FDataBuffer );
     {Destruir componente de TCP}
     FreeAndNil( FIdTCP );
     {Destruir componente de FTP}
     FreeAndNil( FIdFTP );
     inherited Destroy;
end;

function TL7Server.GetListaTerminales: TTerminals;
begin
     Result := LinxIni.IniValues.ListaTerminales;
end;

procedure TL7Server.EscribeBitacora( const sMensaje : string );
begin
     if Assigned( OnDoLectura ) then
     begin
          Application.ProcessMessages;
          OnDoLectura( DateToStr( NOW ) + ' - ' +
                       TimeToStr( NOW ) + ' : ' +
                       sMensaje );
     end;
end;

procedure TL7Server.SiguienteTerminal;
begin
     inc( FTerminalPtr );
     if ( FTerminalPtr > ( ListaTerminales.Count - 1 ) ) then
     begin
          FTerminalPtr := K_PRIMERA_TERMINAL;
     end;
end;

{ ************ Funciones para FTP ****************** }

procedure TL7Server.DelayFTP(const Tiempo: integer);
begin
     Sleep( Tiempo );
end;

function TL7Server.ConectarFTP( Terminal: TTerminal ): Boolean;
begin
     Result := False;
     try
        with FIdFTP do
        begin
             {propiedades de FIdFTP basados  en los datos de Terminal  }
             Host := Terminal.IPAddress;
             Username := Terminal.UserName;
             Password := Terminal.Password;
             Connect( True );
             Result := Connected;
        end;
     except
           on Error: Exception do
           begin
                //Error.Message := Format( 'Terminal %s no pudo ser conectada por FTP ( %s )', [ Terminal.IPAddress, Error.Message ] );
                //raise;
                FIdFTP.Abort;
                EscribeBitacora( Format( 'Terminal %s no pudo ser conectada por FTP ( %s )', [ Terminal.IPAddress, Error.Message ] ) );
                SiguienteTerminal;
           end;
     end;
end;

function TL7Server.DesconectarFTP( Terminal: TTerminal ): Boolean;
begin
     Result := True;
     try
        with FIdFTP do
        begin
             {
             Importante: llamar QUIT antes de desconectar para evitar que
             se contin�e interrumpiendo al programa en la terminal
             }
             Quit;
             Disconnect;
             DisconnectSocket;
        end;
     except
           on Error: Exception do
           begin
                //Error.Message := Format( 'Terminal %s no pudo ser desconectada por FTP ( %s )', [ Terminal.IPAddress, Error.Message ] );
                //raise;
                FIdFTP.Abort;
                EscribeBitacora( Format( 'Terminal %s no pudo ser desconectada por FTP ( %s )', [ Terminal.IPAddress, Error.Message ] ) );
           end;
     end;
end;

{ ********** Funciones para TCP/IP ************* }

procedure TL7Server.DelayTCP(const Tiempo: integer);
begin
     Sleep( Tiempo );
end;

function TL7Server.ConectarTCP( Terminal: TTerminal ): Boolean;
begin
     Result := False;
     try
        with FIdTCP do
        begin
             Host := Terminal.IPAddress;
             Port := Terminal.Puerto;
             Connect;
             Result := Connected;
        end;
     except
           on Error: Exception do
           begin
                //Error.Message := Format( 'Terminal %s no pudo ser conectada por TCP ( %s )', [ Terminal.IPAddress, Error.Message ] );
                //raise;
                //FIdTCP.Disconnect;
                FIdTCP.DisconnectSocket;
                EscribeBitacora( Format( 'Terminal %s no pudo ser conectada por TCP ( %s )', [ Terminal.IPAddress, Error.Message ] ) );
                SiguienteTerminal;
           end;
     end;
end;

function TL7Server.DesconectarTCP( Terminal: TTerminal ): Boolean;
begin
     Result := False;
     try
        with FIdTCP do
        begin
             if Connected then
             begin
                  Disconnect;
                  DisconnectSocket;
             end;
             Result := Connected;
        end;
     except
           on Error: Exception do
           begin
                //Error.Message := Format( 'Terminal %s no pudo ser desconectada por TCP ( %s )', [ Terminal.IPAddress, Error.Message ] );
                //raise;
                EscribeBitacora( Format( 'Terminal %s no pudo ser desconectada por TCP ( %s )', [ Terminal.IPAddress, Error.Message ] ) );
           end;
     end;
     DelayTCP( K_TIEMPO_DELAY );
end;

function TL7Server.PacketLength( const sPacket: String ): String;
var
   iCount: Integer;
begin
     iCount := ( Length( sPacket ) + 2 );
     Result := Chr( Hi( iCount ) ) + Chr( Lo( iCount ) ) + sPacket;
end;

function TL7Server.SendQuery( Terminal: TTerminal; const sQuery: String; out sResponse: String ): Boolean;
var
   iCount: Integer;
   sPacket: String;
begin
     Result := False;
     sPacket := PacketLength( QUERY_TYPE_QUERY + sQuery );
     try
        try
           if ConectarTCP( Terminal ) then
           begin
                with FIdTCP do
                begin
                     Write( sPacket );
                     iCount := ReadFromStack();
                     if ( iCount > 0 ) then
                     begin
                          sResponse := InputBuffer.Extract( iCount );
                          sResponse := Copy( sResponse, Length( sPacket ) + 1, Length( sResponse ) - Length( sPacket ) );
                          Result := True;
                     end;
                end;
           end;
        finally
               DesconectarTCP( Terminal );
        end;
     except
           on Error: Exception do
           begin
                {
                GA: Lo mejor es que regrese el mensaje de error
                Application.HandleException( Error );
                }
                sResponse := Error.Message;
           end;
     end;
end;

function TL7Server.SendCommand( Terminal: TTerminal; const sCommand: String ): Boolean;
begin
     Result := False;
     try
        try
           if ConectarTCP( Terminal ) then
           begin
                with FIdTCP do
                begin
                     Write( PacketLength( QUERY_TYPE_COMMAND + sCommand ) );
                end;
                Result := True;
           end;
        finally
               DesconectarTCP( Terminal );
        end;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
end;

function TL7Server.GetTime( Terminal: TTerminal ): TDateTime;
var
   sDateTime: String;
begin
     Result := NullDateTime;
     if SendQuery( Terminal, K_QUERY_GET_TIME, sDateTime ) then
     begin
          Result := EncodeDate( ZetaCommonTools.CodificaYear( StrToIntDef( Copy( sDateTime, 9, 2 ), 0 ) ),
                                StrToIntDef( Copy( sDateTime, 11, 2 ), 0 ),
                                StrToIntDef( Copy( sDateTime, 13, 2 ), 0 ) ) +
                    EncodeTime( StrToIntDef( Copy( sDateTime, 1, 2 ), 0 ),
                                StrToIntDef( Copy( sDateTime, 3, 2 ), 0 ),
                                StrToIntDef( Copy( sDateTime, 5, 2 ), 0 ),
                                StrToIntDef( Copy( sDateTime, 7, 2 ), 0 ) );
     end;
end;

function TL7Server.SetTime( Terminal: TTerminal; const dValue: TDateTime ): Boolean;
var
   sDateTime: String;
begin
     sDateTime := Format( '%6.6s00%6.6s%1.1d', [ FormatDateTime( 'hhnnss', dValue ),  FormatDateTime( 'yymmdd', dValue ), DayOfWeek( dValue ) ] );
     Result := SendCommand( Terminal, K_COMMAND_SET_TIME + sDateTime );
end;

{ *************** Funcionalidad General ****************** }

{$define USE_ECHO}

function TL7Server.Init: Boolean;
const
     K_ECHO = 'GTI1234567890ECHO';
var
   i, iSegundos, iMinutos: Integer;
   {$ifdef USE_ECHO}
   FIdEcho: TIdEcho;
   {$endif}
begin
     {$ifdef USE_ECHO}
     FIdEcho := TIdEcho.Create( nil );
     try
     {$endif}
        with ListaTerminales do
        begin
             for i := 0 to ( Count - 1 ) do
             begin
                  {$ifdef USE_ECHO}
                  {
                  Usar componente de FTP para ver si la terminal est� viva.
                  }
                  with FIdEcho do
                  begin
                       Host := Terminal[ i ].IPAddress;
                       try
                          Connect( K_TIMEOUT );
                          if Connected then
                          begin
                               Terminal[ i ].Conectada := ( Echo( K_ECHO ) = K_ECHO );
                          end
                          else
                          begin
                               Terminal[ i ].Conectada := False;
                          end;
                       except
                             on Error: Exception do
                             begin
                                  Terminal[ i ].Conectada := False;
                             end;
                       end;
                       Disconnect;
                  end;
                  if ( Terminal[ i ].Conectada ) then
                  begin
                  {$endif}
                       { Probar acceso por FTP }
                       try
                          Terminal[ i ].Conectada := ConectarFTP( Terminal[ i ] );
                       finally
                              DesconectarFTP( Terminal[ i ] );
                       end;
                  {$ifdef USE_ECHO}
                  end;
                  {$endif}
             end;//for i := 0 to ( Count - 1 ) do
             Result := ( ActiveCount > 0 );
        end;//with ListaTerminales do
     {$ifdef USE_ECHO}
     finally
            FreeAndNil( FIdEcho );
     end;
     {$endif}
     if Result then
     begin
          MakeNodeList;
          FUnitListMade := NullDateTime;
          { Inicializar apuntador a terminales }
          FTerminalPtr := K_PRIMERA_TERMINAL;
          { Inicializar Buffer de checadas }
          FDataBuffer.Clear;
          { Inicializa el registro le�do }
          FData := '';
          { Inicializa el status del registro le�do }
          FDataStatus := edsOK;
          { Cantidad de segundos antes de volver a polear una terminal }

          if ( LinxIni.IniValues.TimeOut >= 60 ) then
          begin
               iSegundos := LinxIni.IniValues.TimeOut Mod 60;
               iMinutos := Trunc( LinxIni.IniValues.TimeOut/60 );
          end
          else
          begin
               iMinutos := 0;
               iSegundos := LinxIni.IniValues.TimeOut;
          end;
          FIntervalo := EncodeTime( 0, iMinutos, iSegundos, 0 );
     end
     else
     begin
          Close;
          if not FBatchMode then
             raise Exception.Create( 'Servidor De Linx 7 No Pudo Ser Inicializado' )
          else
              EscribeBitacora( 'Servidor De Linx 7 No Pudo Ser Inicializado' );
     end;
end;

procedure TL7Server.Verify;
var
   sMensaje: string;
   i: integer;
begin
     if ( ListaTerminales.ActiveCount = 0 ) then
     begin
          sMensaje := 'No Se Detectaron Terminales';
          if not FBatchMode then
             raise Exception.Create( sMensaje )
          else
              EscribeBitacora( sMensaje );
     end
     else if ( ListaTerminales.ActiveCount > 0 ) and ( ListaTerminales.ActiveCount <> ListaTerminales.Count ) then
          begin
               sMensaje := Format( 'Teminales Configuradas: %d Terminales Detectadas: %d', [ ListaTerminales.Count, ListaTerminales.ActiveCount ] );
               if not FBatchMode then
                  raise Exception.Create( sMensaje )
               else
               begin
                    EscribeBitacora( sMensaje );
                    EscribeBitacora( '*** Terminales No Detectadas ****' );
                    for i := 0 to ( ListaTerminales.Count - 1 ) do
                    begin
                         if not ListaTerminales.Terminal[ i ].Conectada then
                         begin
                              EscribeBitacora( Format( 'Terminal: %s Identificador: %s', [ ListaTerminales.Terminal[ i ].IPAddress, ListaTerminales.Terminal[ i ].Identificador ] ) );
                         end;
                    end;
               end;
          end
          else
          begin
               EscribeBitacora( Format( 'Teminales Configuradas: %d Terminales Detectadas: %d', [ ListaTerminales.Count, ListaTerminales.ActiveCount ] ) );
               EscribeBitacora( '*** Terminales Detectadas ****' );
               for i := 0 to ( ListaTerminales.Count - 1 ) do
               begin
                    EscribeBitacora( Format( 'Terminal: %s Identificador: %s', [ ListaTerminales.Terminal[ i ].IPAddress, ListaTerminales.Terminal[ i ].Identificador ] ) );
               end;
          end;
end;

procedure TL7Server.Close;
var
   i: Integer;
begin
     with ListaTerminales do
     begin
          for i := ( Count - 1 ) downto 0 do
          begin
               Terminal[ i ].Conectada := False;
          end;
     end;
     { Desconectar componente de TCP }
     with FIdTCP do
     begin
          if Connected then
          begin
               Disconnect;
               DisconnectSocket;
               DelayTCP( K_TIEMPO_DELAY );
          end;
     end;
     { Desconectar componente de FTP }
     with FIdFTP do
     begin
          if Connected then
          begin
               Quit;
               Disconnect;
               DisconnectSocket;
               DelayFTP( K_TIEMPO_DELAY );
          end;
     end;

end;

function TL7Server.DataDownload( Terminal: TTerminal ): Boolean;
const
     K_DAT_FILE = '\RELOJ.DAT';
     K_ARCHIVO_TEMP = '\CHK_TEMP.DAT';
     K_ARCHIVO_PAUSA_INICIO = 'RMTCNTRL.txt';
     K_TOPE_DEMO = 25;
var
   i: Integer;
   lFound: Boolean;
   iChecadasDisponibles : integer;
   strArchivoDEMO, sArcPausa, sArcReinicio : string;
   lstArchivoDEMO, lstArchivoTemp : TStringList;

   procedure PausaPrograma( const sArchivoPausa: string );
   begin
        if FileExists( sArchivoPausa ) then
        begin
             try
                if ConectarFTP( Terminal ) then
                begin
                     try
                        try
                           with FIdFTP do
                           begin
                                { MV(15/abr/2011): Cambio a la memoria Flash }
                                ChangeDir('C:');
                                DelayFTP( K_TIEMPO_DELAY );
                                { Enviar el archivo a la terminal }
                                Put( sArchivoPausa, K_REMOTE_COMMAND_FILE, False );
                                DelayFTP( K_TIEMPO_DELAY );
                           end;//with
                        except
                              on Error: Exception do
                              begin
                                   Application.HandleException( Error );
                              end;
                        end;
                     finally
                            DesconectarFTP( Terminal );
                     end;
                end;
             except
                   on Error: Exception do
                   begin
                        Application.HandleException( Error );
                   end;
             end;
        end;
   end;

begin
     Result := False;
     try
        //Verificar si esta en modo DEMO y si quedan checadas DEMO disponibles
        if ( FEsDemo and (FChecadasDEMO < K_TOPE_DEMO) ) or ( not FEsDemo ) then
        begin
             // Obtiene archivos de Pausa y Reinicio
             sArcPausa := LinxIni.IniValues.DirPausa;
             if StrVacio( sArcPausa ) then
                sArcPausa := ExtractFileDir( Application.ExeName );

             sArcReinicio := LinxIni.IniValues.DirReinicio;
             if StrVacio( sArcReinicio ) then
                sArcReinicio := ExtractFileDir( Application.ExeName );

             PausaPrograma( ZetaCommonTools.VerificaDir( sArcPausa ) + K_ARCHIVO_PAUSA_INICIO );
             try
                if ConectarFTP( Terminal ) then
                begin
                     if FBatchMode then
                        EscribeBitacora( Format( '>>> Conectado al FTP de Terminal: %s Identificador: %s <<<', [ Terminal.IPAddress, Terminal.Identificador ] ) );
                     try
                        try
                           with FIdFTP do
                           begin
                                { Buscar directorio ra�z }
                                ChangeDir( K_DIRECTORY );
                                DelayFTP( K_TIEMPO_DELAY );

                                { Obtener lista de archivos }
                                List( ListResult, '*.*', false );
                                DelayFTP( K_TIEMPO_DELAY );

                                { Buscar archivo de checadas }
                                lFound := False;
                                with ListResult do
                                begin
                                     for i := 0 to ( Count - 1 ) do
                                     begin
                                          if ( Pos( K_DAT_FILE, UpperCase( Strings[ i ] ) ) > 0 ) then
                                          begin
                                               lFound := True;
                                               Break;
                                          end;
                                     end;
                                end;//with ListResult do
                           end;//with FIdFTP do
                           {
                           Si hay checadas, cargarlas
                           siempre y cuando no exista
                           el archivo buffer de checadas
                           }
                           if lFound then
                           begin
                                {
                                Verificar que no exista un archivo buffer
                                de checadas previo para que este no sea
                                encimado por las checadas de la terminal
                                }
                                if not SysUtils.FileExists( FBufferFile ) then
                                begin
                                     { Bajar de terminal hacia archivo BUFFER }
                                     {
                                     NOTA: el Par�metro RESUME no puede ser True
                                     debido a que depende de que el sitio de FTP
                                     lo soporte
                                     }
                                     FIdFTP.Get( K_DAT_FILE, FBufferFile, True, False );
                                     DelayFTP( K_TIEMPO_DELAY );

                                     { Verificar que archivo BUFFER existe }
                                     if SysUtils.FileExists( FBufferFile ) then
                                     begin
                                          {
                                          Borrar archivo de checadas de la terminal
                                          para las nuevas checadas sean depositadas
                                          en la terminal en un archivo nuevo
                                          }
                                          FIdFTP.Delete( K_DAT_FILE );
                                          DelayFTP( K_TIEMPO_DELAY );

                                          Result := True;

                                          //Verificar si esta en modo DEMO
                                          if FEsDemo then
                                          begin

                                               //Crear archivos temporales del contador de checadas DEMO
                                               lstArchivoDEMO := TStringList.Create;
                                               lstArchivoTemp := TStringList.Create;
                                               lstArchivoDEMO.Clear;
                                               lstArchivoTemp.Clear;

                                               try

                                                  //Verificar si quedan checadas DEMO disponibles
                                                  if FChecadasDEMO < K_TOPE_DEMO then
                                                  begin
                                                       //Cargar registros que se bajaron del reloj
                                                       lstArchivoTemp.LoadFromFile( FBufferFile );

                                                       //Obtener cuantas checadas quedan disponibles
                                                       iChecadasDisponibles := K_TOPE_DEMO - FChecadasDEMO;

                                                       //Verificar si el contenido bajado del reloj es mayor al permitido
                                                       if lstArchivoTemp.Count > iChecadasDisponibles then
                                                       begin
                                                            // Dividir el archivo del buffer dejando solo las checadas permitidas
                                                            // y regresando las no permitidas al reloj
                                                            // lstArchivoDEMO <- Checadas Permitidas
                                                            // lstArchivoTemp <- Checadas NO Permitidas, regresar al reloj

                                                            for i := 0 to iChecadasDisponibles - 1 do
                                                            begin
                                                                 lstArchivoDEMO.Add( lstArchivoTemp.Strings[ 0 ] );
                                                                 lstArchivoTemp.Delete( 0 );
                                                            end;//for

                                                            //Volver a crear el archivo del buffer con el contenido valido
                                                            SysUtils.DeleteFile( FBufferFile );
                                                            lstArchivoDEMO.SaveToFile( FBufferFile ); //Checadas a procesar

                                                            //Generar un nuevo archivo con las checadas que no seran procesadas
                                                            strArchivoDEMO := ZetaCommonTools.VerificaDir( ExtractFileDir( Application.ExeName ) ) + K_ARCHIVO_TEMP;
                                                            lstArchivoTemp.SaveToFile( strArchivoDEMO ); //Checadas a regresar al reloj

                                                            //Subir Checadas NO Permitidas al Reloj
                                                            FIdFTP.Put( strArchivoDEMO, K_DAT_FILE );

                                                            //Borrar el archivo temporal
                                                            SysUtils.DeleteFile( strArchivoDEMO );

                                                            //Incrementar Contador de Checadas con el Nuevo Indice (acumular)
                                                            FChecadasDEMO := FChecadasDEMO + iChecadasDisponibles;
                                                       end//if lstArchivoTemp.Count > iChecadasDisponibles then
                                                       else
                                                       begin
                                                            // No hay necesidad de dividir el archivo del buffer
                                                            // debido a que las checadas restantes cubren su contenido

                                                            //Sumar al contador los registros del buffer
                                                            FChecadasDEMO := FChecadasDEMO + lstArchivoTemp.Count;
                                                       end;

                                                  end//if FChecadasDEMO < K_TOPE_DEMO then
                                                  else
                                                  begin
                                                       //Se terminaron las checadas DEMO

                                                       //Borrar el buffer ya que no sera procesado
                                                       SysUtils.DeleteFile( FBufferFile );
                                                       Result := False;
                                                  end;//else if FChecadasDEMO < K_TOPE_DEMO then

                                               finally
                                                      FreeAndNil( lstArchivoDEMO );
                                                      FreeAndNil( lstArchivoTemp );
                                               end;//try

                                          end;//if FEsDemo then

                                     end;//if SysUtils.FileExists( FBufferFile ) then
                                     {else
                                         EscribeBitacora( Format( 'Archivo: %s creado despu�s de obtener del FTP', [ FBufferFile ] ) );}
                                end;//if not SysUtils.FileExists( FBufferFile ) then
                                {else
                                    EscribeBitacora( Format( 'Existe archivo: %s en folder de L7Poll', [ FBufferFile ] ) );}
                           end//if lFound then
                           else
                           begin
                                {
                                OK debido a que si se realiz� el poleo
                                aunque el resultado es que no hay
                                checadas por bajar
                                }
                                Result := True;
                                if FBatchMode then
                                   EscribeBitacora( Format( 'Archivo: %s No encontrado (No hay Checadas)', [ 'RELOJ.DAT' ] ) );
                           end;
                        except
                              on Error: Exception do
                              begin
                                   Application.HandleException( Error );
                              end;
                        end;
                     finally
                            DesconectarFTP( Terminal );
                            if FBatchMode then
                               EscribeBitacora( Format( '>>> Desconectado del FTP de Terminal: %s Identificador: %s <<<', [ Terminal.IPAddress, Terminal.Identificador ] ) );
                     end;
                end;//if ConectarFTP( Terminal ) then
             finally
                    PausaPrograma( ZetaCommonTools.VerificaDir( sArcReinicio ) + K_ARCHIVO_PAUSA_INICIO );
             end;
        end;//if ( FEsDemo and (FChecadasDEMO < K_TOPE_DEMO) ) or ( not FEsDemo ) then
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;//try
end;

function TL7Server.DataTransformItem( var sData: String ): eDataStatus;
const
     K_DATA_LINE_ITEM_COUNT = 5;
     POS_IP_ADDRESS = 0;
     POS_EMPLOYEE_ID = 1;
     POS_DATE = 2;
     POS_TIME = 3;
     POS_KEY_ID = 4;
var
   sTerminalID, strHEX: String;
begin
     with FDataRawItems do
     begin
          Clear;
          CommaText := sData;
          if ( Count = K_DATA_LINE_ITEM_COUNT ) then
          begin
               sTerminalID := ListaTerminales.GetTerminalID( Strings[ POS_IP_ADDRESS ] );
               //Verificar si la lectura es hexadecimal
               if ( copy( Strings[ POS_EMPLOYEE_ID ], 1, 1 ) = '~' ) then
               begin
                    strHEX := Strings[ POS_EMPLOYEE_ID ];
                    System.Delete( strHEX, 1, 1 ); //Quitar la "~"
                    //Colocar tilde "~" y rellenar con ceros a la izquierda
                    strHEX := '~' + ZetaCommonTools.PadLCar( strHEX, 10, '0' );
                    sData := Format( '%6.6s0000@%s%4.4s%4.4s%1.1s', [ sTerminalID,
                                                                      strHEX,
                                                                      Strings[ POS_DATE ],
                                                                      Strings[ POS_TIME ],
                                                                      Strings[ POS_KEY_ID ] ] );
               end//if
               else
               begin
                    sData := Format( '%6.6s0000@%s%4.4s%4.4s%1.1s', [ sTerminalID,
                                                                      Strings[ POS_EMPLOYEE_ID ],
                                                                      Strings[ POS_DATE ],
                                                                      Strings[ POS_TIME ],
                                                                      Strings[ POS_KEY_ID ] ] );
               end;//else
               Result := edsOK;
          end
          else
          begin
               { Los datos son basura }
               Result := edsGarbage;
          end;
     end;
end;

function TL7Server.DataTransform: Boolean;
var
   i: Integer;
   sData: String;
   eStatus: eDataStatus;
begin
     Result := False;
     if FileExists( FBufferFile ) then
     begin
          Result := True;
          with FDataRaw do
          begin
               LoadFromFile( FBufferFile );
               if ( Count > 0 ) then
               begin
                    for i := 0 to ( Count - 1 ) do
                    begin
                         { Desmenuza el texto a una lista de 5 elementos }
                         sData := Strings[ i ];
                         eStatus := DataTransformItem( sData );
                         { Transformar a formato TRESS }
                         FDataBuffer.AddObject( sData, TObject( Ord( eStatus ) ) );
                    end;
                    {if ( iChecadas > 0 ) then
                       EscribeBitacora( Format( '[DataTransform] Termin� de transformar archivo %s. N�mero de checadas: %d', [ FBufferFile, iChecadas ] ) )
                    else
          		EscribeBitacora( Format( '[DataTransform] Termin� de transformar archivo %s.', [ FBufferFile ] ) )}
               end
               else
               begin
                    {
                    Borrar archivo buffer de checadas, dado que no
                    hay checadas por procesar (archivo vac�o)
                    }
                    SysUtils.DeleteFile( FBufferFile );
          	    //EscribeBitacora( Format( '[DataTransform] Borrando archivo %s (Archivo Vac�o)', [ FBufferFile ] ) );
               end;
          end;
     end;
     {else
         EscribeBitacora( Format( '[DataTransform] No existe archivo %s', [ FBufferFile ] ) );}
end;

procedure TL7Server.MakeNodeList;
begin
     if ( ( Now - FUnitListMade ) > FIntervalo ) then
     begin
          if Assigned( FOnMakeUnitList ) then
             FOnMakeUnitList;
          FUnitListMade := Now;
     end;
end;

procedure TL7Server.SetValidTokens( const Value: String );
begin
     FValidTokens := Value;
end;

function TL7Server.GetTerminalInfo( const iTerminal: Integer ): TTerminal;
begin
     Result := ListaTerminales.Terminal[ iTerminal ];
end;

function TL7Server.GetRegistroEsNormal: Boolean;
begin
     Result := ( FDataStatus = edsOK );
end;

function TL7Server.GetDatosEnviados: String;
begin
     Result := FData;
end;

function TL7Server.GetRegistroLeido: String;
begin
     Result := FData;
end;

function TL7Server.Read: Boolean;
const
     K_BUFFER_POSITION = 0;
var
   Terminal: TTerminal;
begin
     FData := '';
     Result := False;
     { Averiguar si hay checadas en el Buffer }
     with FDataBuffer do
     begin
          if ( Count > 0 ) then
          begin
               FData := Strings[ K_BUFFER_POSITION ];
               FDataStatus := eDataStatus( Integer( Objects[ K_BUFFER_POSITION ] ) );
               Delete( K_BUFFER_POSITION );
               { Ya no hay checadas en el Buffer }
               if ( Count = 0 ) then
               begin
                    {
                    Borrar archivo buffer de checadas anterior, para que
                    no se reprocesen
                    Es importante borrarlo hasta que FDataBuffer se vac�e
                    para asegurarse que una falla no vaya a perder checadas
                    dado que FDataBuffer se encuentra �nicamente en memoria
                    }
                    SysUtils.DeleteFile( FBufferFile );
               end;
               Result := True;
          end;
     end;
     { No hay buffer de datos }
     if not Result then
     begin
          Terminal := ListaTerminales.Terminal[ FTerminalPtr ];
          if Terminal.Conectada then
          begin
               { Averiguar si ya transcurri� el Intervalo de Recolecci�n }
               if ( ( Now - Terminal.UltimoPoleo ) > FIntervalo ) then
               begin
                    { Bajar checadas de la terminal FTerminalPtr }
                    if DataDownload( Terminal ) then
                    begin
                         { Resetear el Ultimo Poleo de la terminal }
                         Terminal.UltimoPoleo := Now;
                         //Establecer numero de terminal
                         FTerminalActiva := FTerminalPtr;
                         //EscribeBitacora( Format( '[Read] Bajo Checadas de Terminal: %s FTerminalPtr: %d', [ Terminal.IPAddress, FTerminalPtr ] ) );
                         { Incrementar apuntador de terminales }
                         Inc( FTerminalPtr );
                    end;
                    { Transformar las checadas al formato TRESS }
                    DataTransform;
               end;
          end
          else
          begin
               EscribeBitacora( Format( '[Read] Terminal No Conectada: %s FTerminalPtr: %d', [ Terminal.IPAddress, FTerminalPtr ] ) );
               { Incrementar apuntador de terminales }
               Inc( FTerminalPtr );
          end;
          if ( FTerminalPtr > ( ListaTerminales.Count - 1 ) ) then
          begin
               FTerminalPtr := K_PRIMERA_TERMINAL;
          end;
     end;
end;

procedure TL7Server.DoFeedBack( const iTerminal: Integer; sMsg: String );
begin
     //MODIFICACION
     if Assigned( FOnFeedBack ) then
        FOnFeedBack( sMsg, iTerminal + 1 );
end;

function TL7Server.GetSerialNumber( const iTerminal: Integer ): String;
begin
     if not SendQuery( ListaTerminales.Terminal[ iTerminal ], K_QUERY_GET_SERIAL, Result ) then
        Result := K_UNKNOWN;
     Result := 'S/N: ' + Result;
end;

procedure TL7Server.ShowDateTime( const iTerminal: Integer );
begin
     DoFeedBack( iTerminal, FormatDateTime( 'dd/MMM/yyyy HH:NN:SS AM/PM', GetTime( ListaTerminales.Terminal[ iTerminal ] ) ) );
end;

procedure TL7Server.ShowVersion( const iTerminal: Integer );
var
   sVersion, sAppInfo: String;
   Terminal: TTerminal;
begin
     Terminal := ListaTerminales.Terminal[ iTerminal ];
     if not SendQuery( Terminal, K_QUERY_GET_VERSION, sVersion ) then
        sVersion := K_UNKNOWN;
     if not SendQuery( Terminal, K_QUERY_GET_APP_INFO, sAppInfo ) then
        sAppInfo := K_UNKNOWN;
     DoFeedBack( iTerminal, Format( 'Versi�n: %s App: %s', [ sVersion, sAppInfo ] ) );
end;

procedure TL7Server.SetDateTimeAll( const dValue: TDateTime );
var
   i: Integer;
begin
     with ListaTerminales do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               if Terminal[ i ].Conectada then
               begin
                    SetDateTime( i, dValue );
               end;
          end;
     end;
end;

procedure TL7Server.SetDateTime( const iTerminal: Integer; const dValue: TDateTime );
begin
     SetTime( ListaTerminales.Terminal[ iTerminal ], dValue );
end;

procedure TL7Server.AbortApp( const iTerminal: Integer );
begin
     SendCommand( ListaTerminales.Terminal[ iTerminal ], K_COMMAND_APP_STOP );
     DoFeedBack( iTerminal, 'Deteniendo programa...' );
end;

procedure TL7Server.ResetUnit( const iTerminal: Integer );
var
   Terminal: TTerminal;
begin
     Terminal := ListaTerminales.Terminal[ iTerminal ];
     SendCommand( Terminal, K_COMMAND_RESET_DEFAULTS );
     { Para evitar que se intente polear mientras la terminal se est� resteando }
     Terminal.UltimoPoleo := Now + ( 3 * FIntervalo );
     DoFeedBack( iTerminal, 'Resetting...' );
end;

procedure TL7Server.RebootUnit( const iTerminal: Integer );
var
   Terminal: TTerminal;
begin
     Terminal := ListaTerminales.Terminal[ iTerminal ];
     SendCommand( Terminal, K_COMMAND_REBOOT );
     { Para evitar que se intente polear mientras la terminal se est� rebooteando }
     Terminal.UltimoPoleo := Now + ( 3 * FIntervalo );
     DoFeedBack( iTerminal, 'Rebooting...' );
end;

procedure TL7Server.RestartUnit( const iTerminal: Integer );
begin
     SendCommand( ListaTerminales.Terminal[ iTerminal ], K_COMMAND_APP_START );
     DoFeedBack( iTerminal, 'Reiniciando programa...' );
end;

procedure TL7Server.SendDataFile( const iTerminal: Integer; sFileName, sInternalName: String  );
var
   Terminal: TTerminal;
   sFileExt : string;
begin
     if FileExists( sFileName ) then
     begin
          sFileExt := UpperCase(ExtractFileExt(sFileName));
          Terminal := ListaTerminales.Terminal[ iTerminal ];
          try
             if ConectarFTP( Terminal ) then
             begin
                  try
                     try
                        with FIdFTP do
                        begin
                             { Buscar directorio ra�z }
                             if ((sFileExt = K_FLASH_EXT1) or (sFileExt = K_FLASH_EXT2)) or
                                ( UpperCase(sInternalName) = K_REMOTE_COMMAND_FILE ) then
                              begin
                               ChangeDir('C:');
                              end//if (sFileExt = '.S19') or (sFileExt = '.S39') or ( UpperCase(sInternalName) = K_REMOTE_COMMAND_FILE ) then
                             else
                              begin
                               ChangeDir( K_DIRECTORY );
                              end;//else

                             //Enviar Archivo
                             DelayFTP( K_TIEMPO_DELAY );
                             { Enviar el archivo a la terminal }
                             Put( sFileName, sInternalName, False );
                             DelayFTP( K_TIEMPO_DELAY );
                        end;//with
                     except
                           on Error: Exception do
                           begin
                                Application.HandleException( Error );
                           end;
                     end;
                  finally
                         DesconectarFTP( Terminal );
                  end;
             end;
          except
                on Error: Exception do
                begin
                     Application.HandleException( Error );
                end;
          end;
     end;
end;

function TL7Server.GetDataFile( const iTerminal: Integer; sFileName, sInternalName: String ): Boolean;
var
   Terminal: TTerminal;
begin
     Result := False;
     Terminal := ListaTerminales.Terminal[ iTerminal ];
     try
        if ConectarFTP( Terminal ) then
        begin
             try
                try
                   with FIdFTP do
                   begin
                        { Buscar directorio ra�z }
                        ChangeDir( K_DIRECTORY );
                        DelayFTP( K_TIEMPO_DELAY );
                        { Bajar de terminal hacia archivo deseado }
                        {
                        NOTA: el Par�metro RESUME no puede ser True
                        debido a que depende de que el sitio de FTP
                        lo soporte
                        }
                        Get( sInternalName, sFileName, True, False );
                        DelayFTP( K_TIEMPO_DELAY );
                        Result := SysUtils.FileExists( sFileName );
                   end;
                except
                      on Error: Exception do
                      begin
                           Application.HandleException( Error );
                      end;
                end;
             finally
                    DesconectarFTP( Terminal );
             end;
        end;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
end;

procedure TL7Server.SendParameterFile( const iTerminal: Integer; sFileName: String );
var
   FSource, FBells: TStrings;
   sHora: String;
   i, iPos, iPtr: Integer;
begin
     iPtr := 0;
     FSource := TStringList.Create;
     try
        FBells := TStringList.Create;
        try
           with FSource do
           begin
                LoadFromFile( sFileName );
                for i := 0 to ( Count - 1 ) do
                begin
                     sHora := Trim( Strings[ i ] );
                     iPos := Pos( ':', sHora );
                     if ( Length( sHora ) > 0 ) and
                        ( sHora[ 1 ] in [ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' ] ) and
                        ( iPos > 0 )
                     then
                     begin
                          sHora := Copy( sHora, 1, ( iPos + 2 ) );
                          FBells.Add( Format( 'B%2.2d=%s', [ iPtr, sHora ] ) );
                          Inc( iPtr );
                     end;
                end;
           end;
           with FBells do
           begin
                if ( Count > 0 ) then
                begin
                     SaveToFile( FTempFile );
                     SendDataFile( iTerminal, FTempFile, PARAMETER_FILE_NAME );
                     if not FBatchMode then
                     begin
                          //Reiniciar Terminal
                          if ZetaDialogo.zConfirm( 'Reiniciar Terminal', '� Desea reiniciar la terminal para que los cambios surtan efecto ?'
                                                   , 0, mbOK ) then
                           begin
                             RebootUnit( iTerminal );
                           end;//if
                     end;//if not FBatchMode then
                end
                else
                begin
                     if not FBatchMode then
                        raise Exception.Create( Format( 'Archivo de ALARMA %s inv�lido', [ sFileName ] ) )
                     else
                         EscribeBitacora( Format( 'Archivo de ALARMA %s inv�lido', [ sFileName ] ) );
                end;
           end;
        finally
               FreeAndNil( FBells );
        end;
     finally
            FreeAndNil( FSource );
     end;
end;

procedure TL7Server.SendEmployeeListFile( const iTerminal: Integer; sFileName: String );
const
     K_DELIMITADOR = '|';
var
   FSource, FDoors: TStrings;
   sGafete, sNombre, sApellido: String;
   i, iPos: Integer;
begin
     FSource := TStringList.Create;
     try
        FDoors := TStringList.Create;
        try
           with FSource do
           begin
                LoadFromFile( sFileName );
                for i := 0 to ( Count - 1 ) do
                begin
                     sGafete := Trim( Strings[ i ] );
                     if ( Length( sGafete ) > 0 ) then
                     begin
                          { Verificar que no es un comentario }
                          if ( sGafete[ 1 ] <> '#' ) then
                          begin
                               { Buscar Nombre }
                               iPos := Pos( K_DELIMITADOR, sGafete );
                               if ( iPos > 0 ) then
                               begin
                                    sNombre := Copy( sGafete, ( iPos + 1 ), Length( sGafete ) - iPos );
                                    sApellido := '';
                                    sGafete := Copy( sGafete, 1, ( iPos - 1 ) );
                                    { Buscar Apellido }
                                    iPos := Pos( K_DELIMITADOR, sNombre );
                                    if ( iPos > 0 ) then
                                    begin
                                         sApellido := Copy( sNombre, ( iPos + 1 ), Length( sNombre ) - iPos );
                                         sNombre := Copy( sNombre, 1, ( iPos - 1 ) );
                                    end;
                                    FDoors.Add( Trim( Format( '%s,%s %s', [ sGafete, sNombre, sApellido ] ) ) );
                               end
                               else
                               begin
                                    { No se incluye el nombre; solo el Gafete }
                                    FDoors.Add( sGafete );
                               end;
                          end;
                     end;
                end;
           end;
           with FDoors do
           begin
                if ( Count > 0 ) then
                begin
                     SaveToFile( FTempFile );
                     SendDataFile( iTerminal, FTempFile, EMPLOYEE_LIST_FILE_NAME );
                     if not FBatchMode then
                     begin
                          //Reiniciar Terminal
                          if ZetaDialogo.zConfirm( 'Reiniciar Terminal', '� Desea reiniciar la terminal para que los cambios surtan efecto ?'
                                                   , 0, mbOK ) then
                           begin
                             RebootUnit( iTerminal );
                           end;//if
                     end //if not FBatchMode then
                     else
                     begin
                          RebootUnit( iTerminal );
                     end;
                end
                else
                begin
                     if not FBatchMode then
                        raise Exception.Create( Format( 'Archivo de EMPLEADOS %s inv�lido', [ sFileName ] ) )
                     else
                         EscribeBitacora( Format( 'Archivo de EMPLEADOS %s inv�lido', [ sFileName ] ) );
                end;
           end;
        finally
               FreeAndNil( FDoors );
        end;
     finally
            FreeAndNil( FSource );
     end;
end;

procedure TL7Server.SendApplicationFile( const iTerminal: Integer; sFileName: String );
{$ifdef ANTES}
const
     TOKEN_COMPANY_NAME = '<%EMPRESA%>';
var
   FSource, FTarget: TStrings;
   sData: String;
   i: Integer;
{$endif}
begin
     {$ifdef ANTES}
     FSource := TStringList.Create;
     try
        FTarget := TStringList.Create;
        try
           with FSource do
           begin
                LoadFromFile( sFileName );
                for i := 0 to ( Count - 1 ) do
                begin
                     sData := Strings[ i ];
                     if ( Pos( TOKEN_COMPANY_NAME, sData ) > 0 ) then
                     begin
                          sData := SysUtils.StringReplace( sData,  TOKEN_COMPANY_NAME, ListaTerminales.Terminal[ iTerminal ].Letrero, [] );
                     end;
                     FTarget.Add( sData );
                end;
           end;
           with FTarget do
           begin
                if ( Count > 0 ) then
                begin
                     SaveToFile( FTempFile );
                     SendDataFile( iTerminal, FTempFile, APPLICATION_FILE_NAME );
                     if not FBatchMode then
                     begin
                          //Reiniciar Terminal
                          if ZetaDialogo.zConfirm( 'Reiniciar Terminal', '� Desea reiniciar la terminal para que los cambios surtan efecto ?'
                                                   , 0, mbOK ) then
                           begin
                             RebootUnit( iTerminal );
                           end;//if
                     end;//if not FBatchMode then
                end
                else
                begin
                     if not FBatchMode then
                        raise Exception.Create( Format( 'Programa %s inv�lido', [ sFileName ] ) )
                     else
                         EscribeBitacora( Format( 'Programa %s inv�lido', [ sFileName ] ) );
                end;
           end;
        finally
               FreeAndNil( FTarget );
        end;
     finally
            FreeAndNil( FSource );
     end;
     {$else}
     SendDataFile( iTerminal, sFileName, APPLICATION_FILE_NAME );
     if not FBatchMode then
     begin
          if ZetaDialogo.zConfirm( 'Reiniciar Terminal', '�Desea reiniciar la terminal para que los cambios tomen efecto?', 0, mbOK ) then
             RebootUnit( iTerminal );
     end
     else
         RebootUnit( iTerminal );

     {$endif}
end;

procedure TL7Server.CargarDirectorio( const iTerminal: Integer; Lista: TStrings );
var
   Terminal: TTerminal;
begin
     Terminal := ListaTerminales.Terminal[ iTerminal ];
     try
        if ConectarFTP( Terminal ) then
        begin
             try
                try
                   with FIdFTP do
                   begin
                        { Buscar directorio ra�z }
                        ChangeDir( K_DIRECTORY );
                        DelayFTP( K_TIEMPO_DELAY );
                        { Obtener lista de archivos }
                        List( Lista, '*.*', false );
                        DelayFTP( K_TIEMPO_DELAY );
                   end;
                except
                      on Error: Exception do
                      begin
                           Application.HandleException( Error );
                      end;
                end;
             finally
                    DesconectarFTP( Terminal );
             end;
        end;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
end;

function TL7Server.BorrarArchivo( const iTerminal: Integer; sFileName: String ): Boolean;
var
   Terminal: TTerminal;
begin
     Result := False;
     Terminal := ListaTerminales.Terminal[ iTerminal ];
     try
        if ConectarFTP( Terminal ) then
        begin
             try
                try
                   with FIdFTP do
                   begin
                        { Buscar directorio ra�z }
                        ChangeDir( K_DIRECTORY );
                        DelayFTP( K_TIEMPO_DELAY );
                        { Borrar archivo indicado }
                        Delete( sFileName );
                        DelayFTP( K_TIEMPO_DELAY );
                        Result := True;
                   end;
                except
                      on Error: Exception do
                      begin
                           Application.HandleException( Error );
                      end;
                end;
             finally
                    DesconectarFTP( Terminal );
             end;
        end;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
end;

procedure TL7Server.DoGaugeSetSize( const sMsg: String; iSize: Integer );
begin
     if Assigned( FOnGaugeSetSize ) then
        FOnGaugeSetSize( sMsg, iSize );
end;

procedure TL7Server.DoGaugeAdvance( const sMsg: String; iStep: Integer );
begin
     if Assigned( FOnGaugeAdvance ) then
        FOnGaugeAdvance( sMsg, iStep );
end;

procedure TL7Server.DoGaugeFinish( const sMsg: String; iValue: Integer );
begin
     if Assigned( FOnGaugeFinish ) then
        FOnGaugeFinish( sMsg, iValue );
end;

{ ******* TL7DummyServer ********* }

constructor TL7DummyServer.Create;
begin
     inherited Create;
     FLista := TStringList.Create;
end;

destructor TL7DummyServer.Destroy;
begin
     FLista.Free;
     inherited Destroy;
end;

function TL7DummyServer.Init: Boolean;
begin
     MakeNodeList;
     if ( ListaTerminales.ActiveCount > 0 ) then
     begin
          Result := True;
          Randomize;
     end
     else
     begin
          Result := False;
          Close;
     end;
     with FLista do
     begin
          Clear;
          LoadFromFile( 'D:\3Win_13\Pruebas\Reloj.dat' );
          FPtr := 0;
     end;
end;

procedure TL7DummyServer.MakeNodeList;
begin
     inherited MakeNodeList;
end;

procedure TL7DummyServer.Close;
begin
end;

function TL7DummyServer.Read: Boolean;
var
   iValue: Integer;

function RandomTerminal: Integer;
begin
     Result := Integer( Random( 10 ) + 1 );
end;

begin
     MakeNodeList;
     iValue := Integer( Random( 100 ) );
     Result := ( iValue >= 80 ) and ( iValue <= 99 );
     if Result then
     begin
     end;
end;



end.
