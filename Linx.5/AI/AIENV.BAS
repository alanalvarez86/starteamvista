' AiEnv.bas
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'%%%%%%%%%%%                                                       %%%%%%%%%%
'%%%%%%%%%%%                        A i E n v                      %%%%%%%%%%
'%%%%%%%%%%%                                                       %%%%%%%%%%
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'
'   Project  : Applications Interface                   Version : 1.0
'   Usage    : Low level LINX environment               OS : PCDOS V3.3
'   Language : LinxBASIC V1.0                           CS : IBM PC/AT
'
'   Purpose : LINX dependent device IO
'
'-------------------------------- History -----------------------------------
'
'      Version     Date      Who    Short description of change
'        1.0    20 Mar 89    JHF    Original creation
'----------------------- Function and Procedure Index -----------------------
'
' OpenLINX   : Opens all devices to be used by low level IO routines
' TestEvent% : Tests for an event
' ReadEvent% : Waits for an event
'
' Send       : Sends a record directly to to the host
' SendQue    : Sends a record indirectly to the host through the queue
' SetLINE    : Sets the state the control line
' SetLED     : Turns a LINX keyboard LED on or off
'
' SendCom    : Sends a record to the comm line
' SetDTR     : Sets the state of Data Terminal Ready
' SetRTS     : Sets the state of Request To Send for a comm port
' GetDSR%    : Gets Data Set Ready status for a comm port
'
' PENDING%   : TRUE if Send will result in a pause.
' MEMFULL%   : TRUE if less than 1024 bytes remain in RAM
' ONLINE%    : TRUE when ONLINE to the network
' GetDATE$   : Translates DATE$ into standard form (MM/DD/YYYY)
' GetTIME$   : Translates TIME$ into standard form (HH:MM:SS)
'
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'%%%%%%%%%%        CopyRight 1989, Linx Data Terminals, Inc.       %%%%%%%%%%
'%%%%%%%%%%                  All Rights Reserved.                  %%%%%%%%%%
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

DEFINT A-Z
CONST FALSE  = 0, TRUE = NOT FALSE
CONST PcMode = FALSE ' Constant which indicates program is under emulation

'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@                          Keyboard and Event Equates                     @@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

DIM SHARED ExitKey
ExitKey         = 159  ' ExitKey is remapped to F5 on 20 key keyboards.
CONST Up        =  24
CONST Left      =   8
CONST Right     =  26
CONST Down      =  25
CONST ClearKey  = 127
CONST EnterKey  =  13  ' The LINX ENTER key is the ASCII CR
CONST SpaceBar  =  32  ' Space key
CONST OutKey    =  45  ' ASCII -
CONST InKey     =  43  ' ASCII +

' ---------- Function keys ----------
CONST F1=225, F2=226, F3=227, F4=228, F5 =229
CONST F6=230, F7=231, F8=232, F9=233, F10=234

' ---------- Shifted function keys ----------
CONST ShiftF1 = 11, ShiftF2 = 12, ShiftF3 = 13, ShiftF4 = 14, ShiftF5 = 15
CONST ShiftF6 = 16, ShiftF7 = 17, ShiftF8 = 18, ShiftF9 = 19, ShiftF10 = 20

' ---------- Alt function keys ----------
CONST AltF1 = 21, AltF2 = 22, AltF3 = 23, AltF4 = 24, AltF5 = 25
CONST AltF6 = 26, AltF7 = 27, AltF8 = 28, AltF9 = 29, AltF10 = 30

' ---------- Alt alpha keys ----------
CONST AltQ = 17, AltW = 23, AltE = 5, AltR = 18, AltT = 20
CONST AltY = 25, AltU = 21, AltI = 9, AltO = 15, AltP = 16

CONST AltA = 1, AltS = 19, AltD = 4, AltF = 6, AltG = 7
CONST AltH = 8, AltJ = 10, AltK = 11, AltL = 12

CONST AltZ = 26, AltX = 24, AltC = 3, AltV = 22, AltB = 2, AltN = 14, AltM = 13

' ---------- Standard ASCII character equates ----------
CONST NUL = 0,   BS =  8,   DLE  = 16,   CAN = 24
CONST SOH = 1,   HT =  9,   DC1  = 17,   EM  = 25
CONST STX = 2,   LF = 10,   XON  = 18,   SB  = 26
CONST ETX = 3,   VT = 11,   DC3  = 19,   ESC = 27
CONST EOT = 4,   FF = 12,   XOFF = 20,   FS  = 28
CONST ENQ = 5,   CR = 13,   NAK  = 21,   GS  = 39
CONST ACK = 6,   SO = 14,   SYN  = 22,   RS  = 30
CONST BEL = 7,   SI = 15,   ETB  = 23,   VS  = 31,   DEL=127

' --------------------  Event Return Codes --------------------
CONST TimeoutEvent = -1000  ' The input timer has expired
CONST BadgeEvent   = -1100  ' Read from card readers
CONST NetEvent     = -1300  ' Read from network has occurred
CONST ComEvent     = -1400  ' Read from the main comm port
CONST AuxEvent     = -1500  ' Read from the aux comm port
CONST SensorEvent  = -1600  ' Sensor state change

CONST Sensor1Event = &H1    ' Sensor #1 state change  <--|
CONST Sensor2Event = &H2    ' Sensor #2 state change     |
CONST Sensor3Event = &H4    ' Sensor #3 state change     | Versions 5.0
CONST Sensor4Event = &H8    ' Sensor #4 state change     | and higher!
CONST Sensor5Event = &H10   ' Sensor #5 state change     |
CONST Sensor6Event = &H20   ' Sensor #6 state change  <--|

' --------------------  Event Bit Masks --------------------
CONST KbdMask      = &H0010 ' Allow keystrokes
CONST TimeoutMask  = &H0020 ' Allow timeout wakeup
CONST BadgeMask    = &H0040 ' Allow read from card readers
CONST NetMask      = &H0100 ' Allow reading from network
CONST ComMask      = &H0200 ' Allow read from the main comm port
CONST AuxMask      = &H0400 ' Allow read from the aux comm port
CONST SensorMask   = &H0800 ' Allow Sensor state change event

' --------------------  Device Numbers --------------------
CONST LookDev      = 14     ' Lookup file number
CONST TimeDev      = 15     ' Timer device number
CONST BadgeDev     = 16     ' Badge reader device number (MAG/BAR)
CONST NetDev       = 17     ' Network device number
CONST HostDev      = 18     ' Network device number
CONST ComDev       = 19     ' Comm port device number
CONST AuxDev       = 20     ' Aux comm port device number
CONST SensDev      = 21     ' Sensor device number
CONST LineDev      = 22     ' Control line device number
CONST LiteDev      = 23     ' Front panel/keyboard lights device
CONST DispDev      = 24     ' Display device

'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@                          Global SHARED Variables                        @@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'
' ----------  Buffers ----------
DIM SHARED InBuf$, InKbd$, InCom$, InNet$, LiteCtl$, CHARTABLE$
InBuf$ = "" ' Bar Code and Mag stripe input buffer
InKbd$ = "" ' Keyboard RAW INKEY$ input buffer
InCom$ = "" ' Com and Aux input buffer
InNet$ = "" ' Network input buffer
DIM SHARED NetFull%, RqNum%
'
' ----------  Environment Status Variables ----------
DIM SHARED DSPWIDTH%, COMOPEN%
DIM SHARED TERMNUM%, TERMTYPE$
DIM SHARED BadgeSource$, BadgeType$
COMOPEN%    = FALSE ' Set to TRUE if this is a normal terminal
'
' ----------  Scaler Variables ----------
DIM SHARED TIMEOUT%, SAVEDEVENT%, GLOBALMASK%, GLOBALATTR%
TIMEOUT%    = 1000  ' Number of seconds before timeout for reads
SAVEDEVENT% = 0     ' Event saved by SaveEvent
GLOBALMASK% = 0     ' Mask used for reads which don't specify a mask
GLOBALATTR% = 0     ' Global attributes used for formatted reads
'
DIM SHARED EXITEVENT%, EXITOFFSET, MODIFIED%, INSERTMODE%, EmSavedScreen$
EXITEVENT%  = 0     ' Actual event code value that terminated read
EXITOFFSET% = 0     ' Offset in a string at exit from formatted reads
MODIFIED%   = FALSE ' True when a read changes the default value
INSERTMODE% = TRUE  ' Set to true when insert should occur during reads
'
' ----------  Line State Scaler Variables ----------
DIM SHARED DSR1%, DTR1%, RTS1%, DS21%, DTR2%, RTS2%, LINESTATE%
DSR1%       = FALSE  ' Comm port 1 last GetDSR% status (COM)
DSR2%       = FALSE  ' Comm port 2 last GetDSR% status (AUX)
DTR1%       = FALSE  ' Comm port 1 Data Terminal Ready state (COM)
DTR2%       = FALSE  ' Comm port 2 Data Terminal Ready state (AUX)
RTS1%       = FALSE  ' Comm port 1 Request to Send state (COM)
RTS2%       = FALSE  ' Comm port 2 Request to Send state (AUX)
LINESTATE%  = FALSE  ' Current state of the control lines (AND to get bit set)
DIM SHARED LineMask% ' Mask used to set or disable control lines. See SetLINE.
LineMask%   = FALSE
'
''##C S
' ----------  Sensor State Variables ----------
DIM SHARED SensorEventMask AS INTEGER
TYPE SENSORTYPE
  Now1 AS INTEGER        ' Current sensor state.
  Transitions1 AS LONG   ' Changes since last read (max 65535).
  Duration1    AS LONG   ' Time in 1/100th seconds that sensor was on.
  Now2 AS INTEGER
  Transitions2 AS LONG
  Duration2    AS LONG
  Now3 AS INTEGER
  Transitions3 AS LONG
  Duration3    AS LONG
  Now4 AS INTEGER
  Transitions4 AS LONG
  Duration4    AS LONG
  Now5 AS INTEGER
  Transitions5 AS LONG
  Duration5    AS LONG
  Now6 AS INTEGER
  Transitions6 AS LONG
  Duration6    AS LONG
END TYPE 'SENSORTYPE
'
TYPE SenseBuf                'Current status of sense lines
  N AS INTEGER
  D AS LONG
  T AS LONG
END TYPE
SensorEventMask = 0
DIM SHARED SENSE AS SENSORTYPE
DIM SHARED SStat( 1 TO 6 ) AS SenseBuf
''##E
'
''##C C
' ----------  Comm Line Control ----------
DIM SHARED ComXBuf$, AuxXBuf$   ' Temporary line building buffers.
DIM SHARED XCR$, XLF$, XBS$     ' Special characters for comm line.
XCR$ = CHR$(13)                 ' End of line value.
XLF$ = CHR$(10)                 ' Line feed value.
XBS$ = CHR$(08)                 ' Backspace value.
''##E
'
FUNCTION ComBuildLine$( c$, i )
  r$ = LEFT$(c$,i-1)                    ' Isolate the record.
  c$ = MID$(c$,i+1)                     ' Keep whats left over.
  DO
    j = INSTR(r$,XLF$)                  ' Look for line feeds.
    IF j>0 THEN                         ' Strip line feed.
      r$ = MID$(r$,1,j-1)+MID$(r$,j+1)
    ELSE
      j = INSTR(r$,XBS$)                ' Look for back spaces.
      IF j>0 THEN
        IF j=1 THEN r$=MID$(r$,2) ELSE r$=MID$(r$,1,j-2)+MID$(r$,j+1)
      END IF
    END IF
  LOOP WHILE j>0
  ComBuildLine$ = r$
END FUNCTION ' ComBuildLine$
'
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@                      Low Level Read Functions                           @@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'
'   OpenLINX   : Opens all devices to be used by low level IO routines
'   TestEvent% : Tests for a low level event
'   ReadEvent% : Waits for a low level event
'
'
'****************************************************************************
'
'   OpenLINX : Opens all devices to be used by low level IO routines
'
'   This MUST be called before any of the Event or Read routines.
'   This performs the actual BASIC OPEN statements for all of the
'   devices which are accessed by the low level IO.  Note, the global
'   constant PcMode will be TRUE if this program is running using
'   EnvPC.BAS instead of Env.BAS.  You may use this for any operations
'   which are not fully emulated by the EnvPC environment.
'
'   This also establishes the global environment variables :
'      DSPWIDTH% = 40 or 16 depending on the type of LINX
'      TERMNUM%  = The current terminal number
'      TERMTYPE$ = "NORML", "MASTR, "ALTM ", "CONCN", "SUBM ", "ALTSM"
'
'   OpenLINX
'
'****************************************************************************
SUB OpenLINX
  GLOBALMASK% = 0
  GLOBALATTR% = 0
  OPEN "EGG"  FOR RANDOM AS #TimeDev
''##C B
  OPEN "BAR"  FOR INPUT  AS #BadgeDev
''##E
  OPEN "NET"  FOR OUTPUT AS #NetDev
  OPEN "HOST" FOR RANDOM AS #HostDev
''##C S
  OPEN "SEN"  FOR RANDOM AS #SensDev
''##E
''##C I
  OPEN "LINE" FOR RANDOM AS #LineDev
''##E
''##C A
  OPEN "AUX"  FOR RANDOM AS #AuxDev
''##E
''##C L
  OPEN "LITE" FOR OUTPUT AS #LiteDev
''##E
  OPEN "LCD"  FOR OUTPUT AS #DispDev
  DSPWIDTH% = LOF(DispDev) \ 2  ' Get width of the actual display
  InKbd$      = IOCTL$(HostDev)       ' Get the network status string
  TERMNUM%    = VAL(MID$(InKbd$,2,4)) ' Get actual terminal number
  TERMTYPE$   = MID$(InKbd$,6,5)      ' Get actual terminal type!
  IOCTL #HostDev, "02"                ' Set transaction number.
  IF DSPWIDTH%=16 THEN ExitKey = F5   ' ReMap EXIT key for 20 key keyboards.
''##C C
  IF TERMTYPE$="NORML" THEN
    OPEN "COM"  FOR RANDOM AS #ComDev
    COMOPEN% = TRUE
  END IF
''##E
END SUB 'OpenLINX

'
'****************************************************************************
'
'   LoadGlobals : Load the global variables table
'
'****************************************************************************
''##C G
SUB LoadGlobals
  nn = FREEFILE
  OPEN "DIR" FOR INPUT AS #nn
  a$ = "?": Ok = FALSE
  DO WHILE a$ <> ""
    LINE INPUT #nn, a$
    IF a$ = "GLOBALS" THEN Ok = TRUE: EXIT DO
  LOOP
  CLOSE #nn
  IF NOT Ok THEN EXIT SUB
  OPEN "GLOBALS" FOR INPUT AS #LookDev
  i = 0
  DO WHILE NOT EOF(LookDev) AND i<GlobalCount
    i = i+1 : LINE INPUT #LookDev, Gv(i)
  LOOP
  CLOSE LookDev
END SUB 'LoadGlobals
'
'****************************************************************************
'
'   SaveGlobals : Save the global variables table
'
'****************************************************************************
SUB SaveGlobals
  OPEN "GLOBALS" FOR OUTPUT AS #LookDev
  i = 0
  DO WHILE i<GlobalCount
    i = i+1 : PRINT #LookDev, Gv(i)
  LOOP
  CLOSE LookDev
END SUB 'SaveGlobals
''##E
'
'**************************************************************************
'
' MarkSStat:  Marks the sensor buffer to the current sense line status.
'
'**************************************************************************
''##C S
SUB MarkSStat
  mc& = 2000000000
  SStat(1).N = SENSE.Now1
  SStat(1).D = SENSE.Duration1    + SStat(1).D
  SStat(1).T = SENSE.Transitions1 + SStat(1).T
  SStat(2).N = SENSE.Now2
  SStat(2).D = SENSE.Duration2    + SStat(2).D
  SStat(2).T = SENSE.Transitions2 + SStat(2).T
  SStat(3).N = SENSE.Now3
  SStat(3).D = SENSE.Duration3    + SStat(3).D
  SStat(3).T = SENSE.Transitions3 + SStat(3).T
  SStat(4).N = SENSE.Now4
  SStat(4).D = SENSE.Duration4    + SStat(4).D
  SStat(4).T = SENSE.Transitions4 + SStat(4).T
  SStat(5).N = SENSE.Now5
  SStat(5).D = SENSE.Duration5    + SStat(5).D
  SStat(5).T = SENSE.Transitions5 + SStat(5).T
  SStat(6).N = SENSE.Now6
  SStat(6).D = SENSE.Duration6    + SStat(6).D
  SStat(6).T = SENSE.Transitions6 + SStat(6).T
  FOR cc = 1 to 6
    IF SStat(cc).T > mc& THEN SStat(cc).T = 0
    IF SStat(cc).D > mc& THEN SStat(cc).D = 0
  NEXT cc
END SUB
''##E
'****************************************************************************
'
'   TestEvent% : Tests for a low level event
'
'   Returns 0 when no event has occurred.  Events are either keys read
'   from the LINX keyboard, or special codes.  Special codes are
'   always negative, although some special keys also return negative
'   codes.  See OpenEvent for initialization information.
'
'   The most important events are :
'
'       TimeoutEvent : The input timer has expired
'       BadgeEvent   : Read from card readers; See INTYPE%
'       NetEvent     : Read from network has occurred
'       ComEvent     : Read from the main comm port
'       AuxEvent     : Read from the aux comm port
'       SensorEvent  : Sensor state change
'
'   When BadgeEvent has occurred, InBuf$ contains the bar code or mag
'   stripe which was read.
'
'   When ComEvent or AuxEvent has occurred, InCom$ contains the
'   LINE INPUT format line which was read from the comm line.
'
'   When NetEvent has occurred, InNet$ contains the record from the host.
'
'   The events which you want to test for are indicated by the "mask"
'   parameter.  Each device which you want tested MUST have a bit set
'   in the mask.  Bits are set by building a mask using the constants :
'
'       KbdMask, TimeoutMask, BadgeMask, NetMask, ComMask, AuxMask,
'       and SensorMask.
'
'   To simplify matters, some special mask values are also allowed,
'   These are :
'
'       0 = Read from keyboard only
'       1 = Read from keyboard with timeout (see ReadEvent%)
'
'   e = TestEvent% ( mask );
'
'   Note that timeouts only apply to reads which wait for an event.
'   TestEvent% never waits.  See ReadEvent% for more info.
'
'****************************************************************************
FUNCTION TestEvent% ( mask% ) STATIC
  x=SAVEDEVENT%
  IF x=0 THEN
    ' ----- Keyboard Character Input with mapping -----
    IF mask%<3 OR (mask% AND KbdMask) THEN
      InKbd$ = INKEY$
      IF LEN(InKbd$) > 0 THEN x = ASC(InKbd$)
    END IF
    IF x = 0 THEN
      '----- Always capture input from the host ----
      IF EOF(HostDev) THEN
        LINE INPUT #HostDev, InNet$
        NetFull% = TRUE
      END IF
      
''##C B
      ' ----- Badge input -----
      IF (mask% AND BadgeMask) THEN
        IF EOF(BadgeDev) THEN
          LINE INPUT #BadgeDev, InBuf$
          InKbd$ = IOCTL$(BadgeDev) ' Is in the form"type/source"
          x = INSTR(InKbd$,"/")
          BadgeSource$ = MID$(InKbd$,x+1)
          BadgeType$   =  LEFT$(InKbd$,x-1)
          x = 0
          IF BadgeType$<>"MAG" OR BadgeSource$="SLOT" THEN
            SOUND 1000,2        ' Beep nicely on a good badge read.
            x = BadgeEvent      ' Indicate a badge read event.
          END IF
          ' where type   = C39, I25, UPC, CBAR or C128 or MAG
          ' where source = WAND, SLOT, or LASER
          ' if type=MAG then source is SLOT, PARITY!, LRC!, TIMEOUT!
          ' if source for MAG is not SLOT then the badge is in error.
        END IF
      END IF
''##E
''##C H
      ' ----- Network line input -----
      IF x = 0 AND (mask% AND NetMask) THEN
        IF NetFull% THEN
          x = NetEvent
          NetFull% = FALSE
        END IF
      END IF
''##E
''##C C
      ' ----- Main Comm port line input -----
      IF x=0 AND (mask% AND ComMask) THEN
        IF COMOPEN% THEN
          IF LOC(ComDev)<>0 THEN
            ComXBuf$ = ComXBuf$+INPUT$(LOC(ComDev),ComDev)
          END IF
          i = INSTR(ComXBuf$,XCR$)
          IF i<>0 THEN
            InCom$ = ComBuildLine$( ComXBuf$, i )
            x = ComEvent
          END IF
        END IF
      END IF
''##E
''##C A
      ' ----- Aux Comm port line input -----
      IF x=0 AND (mask% AND AuxMask) THEN
        IF LOC(AuxDev)<>0 THEN
          AuxXBuf$ = AuxXBuf$+INPUT$(LOC(AuxDev),AuxDev)
        END IF
        i = INSTR(AuxXBuf$,XCR$)
        IF i<>0 THEN
          InCom$ = ComBuildLine$( AuxXBuf$, i )
          x = AuxEvent
        END IF
      END IF
''##E
''##C S
      ' ----- Sensor port state change input -----
      SensorEventMask = 0
      IF (mask% AND SensorMask) THEN
        t1 = SENSE.Now1
        t2 = SENSE.Now2
        t3 = SENSE.Now3
        t4 = SENSE.Now4
        t5 = SENSE.Now5
        t6 = SENSE.Now6
        GET #SensDev, 1, SENSE
        MarkSStat
        IF t1 <> SENSE.Now1 THEN
          SensorEventMask = SensorEventMask OR Sensor1Event
          x = SensorEvent
        END IF
        IF t2 <> SENSE.Now2 THEN
          SensorEventMask = SensorEventMask OR Sensor2Event
          x = SensorEvent
        END IF
        IF t3 <> SENSE.Now3 THEN
          SensorEventMask = SensorEventMask OR Sensor3Event
          x = SensorEvent
        END IF
        IF t4 <> SENSE.Now4 THEN
          SensorEventMask = SensorEventMask OR Sensor4Event
          x = SensorEvent
        END IF
        IF t5 <> SENSE.Now5 THEN
          SensorEventMask = SensorEventMask OR Sensor5Event
          x = SensorEvent
        END IF
        IF t6 <> SENSE.Now6 THEN
          SensorEventMask = SensorEventMask OR Sensor6Event
          x = SensorEvent
        END IF
      END IF
''##E
      ' ----- Get any changes in terminal status -----
      ' !!!!! Can a menu access be detected???
    END IF
  END IF
  SAVEDEVENT% = 0
  TestEvent% = x
END FUNCTION 'TestEvent%
'
'****************************************************************************
'
'   SaveEvent : Saves the previous event to allow it to happen again
'
'   Saves the event which has just occurred from TestEvent%.  The very
'   next TestEvent% will return this event code.
'
'   SaveEvent eventcode%
'
'****************************************************************************
SUB SaveEvent ( e% )
  SAVEDEVENT% = e%
END SUB 'SaveEvent
'
'****************************************************************************
'
'   ReadEvent% : Waits for a low level event
'
'   Waits for an event to occur.  The event(s) to wait for are specified
'   in the same manner as TestEvent%.
'
'   For timeouts set the global variable TIMEOUT% to the number of
'   1/100 second intervals you want to wait before a TimeoutEvent is
'   returned.  The value of TIMEOUT% is not modified by ReadEvent%.
'
'   e = ReadEvent% ( mask );
'
'****************************************************************************
FUNCTION ReadEvent% ( mask% )
  t = mask%=1 OR mask%=2 OR (mask% AND TimeoutMask)
  IF t THEN
    SEEK #TimeDev, TIMEOUT%
  END IF
  DO
    x = TestEvent ( mask% )
    IF x=0 AND t THEN
      IF EOF(TimeDev) THEN x = TimeoutEvent
    END IF
  LOOP WHILE x=0
  EXITEVENT% = x
  ReadEvent% = x
END FUNCTION 'ReadEvent%
'
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@                      Low Level Comm Port Access                         @@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'
'   SetDTR  : Sets the state of Data Terminal Ready
'   SetRTS  : Sets the state of Request To Send for a comm port
'   GetDSR% : Gets Data Set Ready status for a comm port
'   SendCom : Sends a record to the comm line
'
'
'****************************************************************************
'
'   SetDTR  : Sets the state of Data Terminal Ready
'   SetRTS  : Sets the state of Request To Send for a comm port
'
'   Set state=TRUE to turn DTR or RTS on.  You may examine the global
'   variables RTS1%, DTR1%, RTS2%, and DTR2% to get the current state.
'   n% is 1 for COM and 2 for AUX
'
'   SetDTR n%, state%
'   SetRTS n%, state%
'
'****************************************************************************
''##C R
SUB SetRS232Lines ( n% )
  IF n%=1 THEN
    IF NOT COMOPEN% THEN EXIT SUB
    c$ = CHR$(DTR1%+&H30) + CHR$(RTS1%+&H30) : d=ComDev
  ELSE
    c$ = CHR$(DTR2%+&H30) + CHR$(RTS2%+&H30) : d=AuxDev
  END IF
  IOCTL #d, c$
END SUB 'SetRS232Lines
'
SUB SetDTR ( n%, s% )
  IF n% = 1 THEN DTR1% = s% ELSE DTR2% = s%
  SetRS232Lines n%
END SUB 'SetDTR
'
SUB SetRTS ( n%, s% )
  IF n% = 1 THEN RTS1% = s% ELSE RTS2% = s%
  SetRS232Lines n%
END SUB 'SetRTS
''##E
'
'****************************************************************************
'
'   GetDSR% : Gets Data Set Ready status for a comm port
'
'   A TRUE is returned if DSR is ON.  n% is 1 for COM and 2 for AUX
'
'   state = GetDSR%(n%)
'
'****************************************************************************
''##C R
FUNCTION GetDSR% ( n% )
  IF n% = 1 AND COMOPEN% THEN
    '!!!!! No mechanism defined for access to DSR on COM
    GetDSR% = DSR1%
  END IF
  IF n% = 2 THEN
    '!!!!! No mechanism defined for access to DSR on AUX
    GetDSR% = DSR2%
  END IF
END FUNCTION 'GetDSR%
''##E
'
'****************************************************************************
'
'   SendCom : Sends a record to a comm line
'
'   A string is sent to the comm line.  The string is sent exactly as is,
'   so append CR or CR and LF if you are sending a line.  n% is 1 for COM
'   and 2 for AUX.  Note that COMOPEN% must be true for a send to COM to
'   work.  COMOPEN% is true only if the TERMTYPE$ is "NORML".
'
'   SendCom n%, s$
'
'****************************************************************************
''##C C
SUB SendCom ( n%, s$ )
  IF n%=2 THEN
    PRINT #AuxDev, s$;
  ELSE
    IF COMOPEN% THEN PRINT #ComDev, s$;
  END IF
END SUB 'SendCom
''##E
'
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@                      Low Level Write/Control Functions                  @@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'
'   PENDING% : TRUE if Send will result in a pause
'   MEMFULL% : TRUE if less than 1024 bytes remain in RAM
'   ONLINE%  : TRUE when ONLINE to the network
'   SetLINE  : Sets the state of the control line
'   Send     : Sends a record directly to to the host
'   SendQue  : Sends a record indirectly to the host through the que
'   SetLED   : Turns a LINX keyboard LED on or off
'   GetDATE$ : Translates DATE$ into standard form (MM/DD/YYYY)
'   GetTIME$ : Translates TIME$ into standard form (HH:MM:SS)
'   DspLinx  : Displays the current linx display string
'   ClrLinx  : Clears the linx display
'   Lookup%  : File Lookup operation
'   EnvTransportRq : Handle a transport request
'
''##E
'
'****************************************************************************
'
'       DspLinx : Displays the current linx display string
'
'****************************************************************************
SUB DspLinx STATIC
  LOCATE 1, 1, 0
  PRINT LEFT$(UvDSP + SPACE$(DSPWIDTH%),     DSPWIDTH%);
  LOCATE 2, 1, 0
  PRINT MID$ (UvDSP + SPACE$(DSPWIDTH%*4),41,DSPWIDTH%);
  LOCATE 1, 1, 0
END SUB 'DspLinx
'
'****************************************************************************
'
'       ClrLinx : Clears the linx display
'
'****************************************************************************
SUB ClrLinx STATIC
  LOCATE 1, 1, 0
  PRINT SPACE$(DSPWIDTH%) : PRINT SPACE$(DSPWIDTH%)
  UvDSP="" : LOCATE 1, 1
END SUB 'ClrLinx
'
'****************************************************************************
'
'   ONLINE% : TRUE if the unit is online
'
'   IF ONLINE% THEN ...
'
'****************************************************************************
FUNCTION ONLINE%
  ONLINE% = LEFT$(IOCTL$(HostDev),1)="1"
END FUNCTION 'ONLINE%
'
'****************************************************************************
'
'   MEMFULL% : TRUE if less than 1024 bytes remain in RAM
'
'   This should be checked before any SendQue or outputs to the
'   RAM files to prevent memory full.
'
'   IF MEMFULL% THEN ...
'
'****************************************************************************
FUNCTION MEMFULL%
  MEMFULL = FRE(1)<1024
END FUNCTION 'MEMFULL%
'
'****************************************************************************
'
'   PENDING% : TRUE if Send will result in a pause.
'
'   This should be checked before any Send to prevent pauses in execution.
'
'   WHILE PENDING% : WEND
'
'****************************************************************************
FUNCTION PENDING%
  PENDING% = LOF(NetDev)=0
END FUNCTION 'PENDING%
'
'****************************************************************************
'
'   Send : Sends a record directly to to the host
'
'   A string is sent to the host from this terminal.  You should
'   test ONLINE% and PENDING% if you don't want this to pause.
'
'   Send s$
'
'****************************************************************************
SUB Send ( s$ )
  IF ONLINE% THEN
    IF PENDING% THEN
      CLS
      PRINT "* Waiting for *"
      PRINT "*   Response  *";
      ' Wait for buffer to be free.
      DO WHILE PENDING%
        IF NOT ONLINE% THEN EXIT SUB
      LOOP
    END IF
    DspLinx
    PRINT #NetDev, s$
  END IF
END SUB 'Send
'
'****************************************************************************
'
'   SendQue : Sends a record indirectly to the host through the que
'
'   A string is placed into the network queue.  The record will in turn
'   be sent to the host as soon as possible.  SendQue has a lower priority
'   than Send, and will buffer records even when NOT ONLINE.
'   Pause will occur if the unit was not ONLINE% and MEMFULL%.
'
'   SendQue s$
'
'****************************************************************************
SUB SendQue ( s$ )
  IF MEMFULL% THEN
    CLS
    PRINT "* Memory full *"
    PRINT "* Please wait *";
    DO WHILE MEMFULL% : LOOP ' Wait for memory to be freed.
    DspLinx
  END IF
  PRINT #HostDev, s$
END SUB 'SendQue
'
'****************************************************************************
'
'   SetLED : Turns a LINX keyboard LED on or off
'
'   A LINX may have up to 10 LEDs (Most LINX IIIs have only 10).
'   The LEDs are numbered 1 to 10.
'   state% should be TRUE for ON and FALSE for OFF.
'
'   SetLED n%, state%
'
'****************************************************************************
''##C L
SUB SetLED ( n%, state% )
  IF state% THEN
    LiteCtl$ = "1" : PUT #LiteDev, n%, LiteCtl$
  ELSE
    LiteCtl$ = "0" : PUT #LiteDev, n%, LiteCtl$
  END IF
END SUB 'SetLED
''##E
'****************************************************************************
'
'   SetLINE : Sets the state of the control lines.  To set a specific
'             control line to ON the corresponding bit in the global
'             variable LineMask% needs to bet set to 1.
'             Example to turn on control lines 1, 3, 5, 6.
'
'             LineMask% = 0     ' Set all bits to 0
'             LineMask% OR 1    ' To set bit 0 to a 1
'             LineMask% OR 4    ' To set bit 3 to a 1
'             LineMask% OR 16   ' To set bit 5 to a 1
'             LineMask% OR 32   ' To set bit 6 to a 1
'
'             or you could just write LineMask% = 53
'
'   The control line (1-6) may be turned ON or OFF.  You may check the
'   global variable LINESTATE% for current state of any given control
'   line by performing the following:
'
'    IF LINESTATE% AND 1  THEN LineOneOn   = TRUE
'    IF LINESTATE% AND 2  THEN LineTwoOn   = TRUE
'    IF LINESTATE% AND 4  THEN LineThreeOn = TRUE
'    IF LINESTATE% AND 8  THEN LineFourOn  = TRUE
'    IF LINESTATE% AND 16 THEN LineFiveOn  = TRUE
'    IF LINESTATE% AND 32 THEN LineSixOn   = TRUE
'
'   SetLINE LineMask%
'
'****************************************************************************
''##C I
SUB SetLINE ( s% )                                   '!GMH92-5
  LINESTATE% = s%: ln% = 1                           '!GMH92-5
  IF LINESTATE% AND 1 THEN                           '!GMH92-5
    c$="1"                                           '!GMH92-5
  ELSE                                               '!GMH92-5
    c$ = "0"                                         '!GMH92-5
  END IF                                             '!GMH92-5
  PUT #LineDev, 1, c$                                '!GMH92-5
  FOR x = 2 TO 6                                     '!GMH92-5
    ln% = ln% + ln%                                  '!GMH92-5
    IF LINESTATE% AND ln% THEN                       '!GMH92-5
      SetLED x+9, 1                                  '!GMH92-5
    ELSE                                             '!GMH92-5
      SetLED x+9, 0                                  '!GMH92-5
    END IF                                           '!GMH92-5
  NEXT x                                             '!GMH92-5
END SUB 'SetLINE                                     '!GMH92-5
''##E
'
'****************************************************************************
'
'   GetTIME$ : Translates TIME$ into standard form (HH:MM:SS)
'
'   In LINX Basic TIME$ is in the form hhmmsshh.  This routine drops the
'   hundreth seconds and puts colons between hours-minutes and minute-
'   seconds.
'
'   GetTIME$
'
'****************************************************************************
FUNCTION GetTIME$
  s$ = TIME$
  GetTIME$ = LEFT$(s$,2) + ":" + MID$(s$,3,2) + ":" + MID$(s$,5,2)
END FUNCTION 'GetTIME$
'
'****************************************************************************
'
'   GetDATE$ : Translates DATE$ into standard form (MM-DD-YYYY)
'
'   In LINX Basic DATE$ is in the form yymmddx.  This routine ignores the
'   day (the x)
'
'   GetDATE$
'
'****************************************************************************
FUNCTION GetDATE$
  s$ = DATE$ ' Get LINX yymmddw date and reformat it to QuickBasic format.
  GetDATE$ = MID$(s$,3,2) + "-" + MID$(s$,5,2) + "-19" + LEFT$(s$,2)
END FUNCTION 'GetDATE$
'
'****************************************************************************
'
'       IsNum% : True if a string is numeric
'
'****************************************************************************
FUNCTION IsNum% ( s$ ) STATIC
  IsNum% = FALSE : k = LEN(s$) : m = 0
  IF k > 10 THEN EXIT FUNCTION
  FOR i=1 TO k
    j=ASC(MID$(s$,i,1))
    IF j<>&H20 AND j<>&H2E AND j<>&H2D AND (j<&H30 OR j>&H39) THEN
      EXIT FUNCTION
    END IF
    IF j=&H20 OR j=&H2D OR j=&H2E THEN m=m+1
  NEXT i
  isNum% = m<k
END FUNCTION 'IsNum%
'
'****************************************************************************
'
'       LMID : String concat with space padding
'
'       LMID  dst$, pos, len, src$
'
'****************************************************************************
SUB LMID ( d$, p%, i%, s$ ) STATIC
  k=LEN(d$) : IF k < p%+i%-1 THEN d$=d$+SPACE$(p%+i%-1-k)
  MID$(d$,p%,i%) = s$
END SUB 'LMID
'
''##C K
'****************************************************************************
'
'   EnvTransportRq : Handle a transport output request
'
'   "t" is the request type, "request" is the parameters of the request.
'   reply$ is filled with the host reply.  Retrycount is an arbitrary
'   numbe of retries before failure.  A good number is 5.
'
'   EnvTransportRq ( "t", "request", srcvar$, reply$, retrycount )
'
'****************************************************************************
SUB EnvTransportRq ( rq$, args$, srv$, reply$, rc% )
  w = rc%
  RqNum% = RqNum%+1
  IF RqNum%>9 THEN RqNum%=1
  DO
    s$ = "&"+rq$+CHR$(RqNum%+48)+args$  ' Put in rq #
    Send s$ : InNet$ = ""               ' Send it
    e = 0
    TIMEOUT% = 1000                     ' Ten seconds per rc%, 50 secs total.
    SEEK #TimeDev, TIMEOUT%
    DO
      IF EOF(HostDev) THEN
        LINE INPUT #HostDev, InNet$
        IF LEFT$(InNet$,2)="&S" THEN
          PRINT #NetDev, InNet$  ' SYNCRONIZE by sending it to the host
          RqNum% = 1             ' and resetting the RQ number.
          s$ = "&"+rq$+CHR$(RqNum%+48)+args$ 
          Send s$ : InNet$ = ""              
          NetFull% = FALSE
        ELSE
          NetFull% = TRUE
          e = 1
        END IF
      ELSEIF EOF(TimeDev) THEN
        e = 2
      END IF
    LOOP WHILE e=0
    r$ = InNet$
    IF LEFT$(r$,3)=LEFT$(s$,3) THEN     ' If the response is good
      reply$ = MID$(r$,4)               ' then return it.
      srv$ = "R"
      EXIT FUNCTION
    END IF                              ' Keep resending
    w = w-1                             ' until retry fails.
  LOOP WHILE w > 0
  srv$ = "T"
  reply$ = ""                           ' Error reply.
END SUB 'EnvTransportRq
''##E

''##C K
'****************************************************************************
'
'   Lookup% : File Lookup operation
'
'   The outvar will contain the results of the lookup.  The filevar
'   is the name of the file, preceded by "L-" or "R-".
'   "L-" indicates the file is local; "R-" indicates a remote file.
'
'   e = Lookup% ( outvar, keystring, filevar, offset, length, reclen )
'
'****************************************************************************
FUNCTION Lookup% ( ov$, ss$, fff$, pp%, n%, rl% )
  Lookup% = FALSE
  nn% = n% : IF nn% < 0 THEN nn% = LEN(ss$)
  nf$ = MID$(fff$,3)
  s$ = LEFT$(ss$+SPACE$(nn%),nn%)
  
  '----- Remote file lookup -----
  IF LEFT$(fff$,2)="R-" THEN
    s$ = nf$ + "|" + s$ + "|" + STR$(pp%)
    EnvTransportRq  "L", s$, sv$, ov$, 5
    IF sv$ = "T" THEN Lookup% = TimeOutEvent: EXIT FUNCTION
  ELSE
    f$  = ""
    cnt& = 0
    IF rl% <= 0 THEN    'Sequential Lookup
      OPEN nf$ FOR INPUT AS #LookDev
      DO WHILE f$="" AND NOT EOF(LookDev)
        LINE INPUT #LookDev, f$
        cnt& = cnt& + 1
        m$ = SPACE$(nn%)
        IF pp%<=LEN(f$) THEN m$ = LEFT$(MID$(f$,pp%,nn%)+m$,nn%)
        IF m$ <> s$ THEN f$=""
      LOOP
      CLOSE #LookDev
    ELSE               'Binary Lookup
      OPEN nf$ FOR BINARY AS #LookDev LEN=rl%
      NumRecs& = LOF(LookDev) / rl%
      IF NumRecs& > 0 THEN
        Top& = NumRecs&: Bottom& = 1
        DO UNTIL Top& < Bottom&
          mr& = (Top& + Bottom&) \ 2
          bp& = (((mr& * rl%)-rl%)+1)
          SEEK #LookDev, bp&
          f$ = INPUT$(rl%, LookDev)
          m$ = SPACE$(nn%)
          IF pp%<=LEN(f$) THEN m$ = LEFT$(MID$(f$,pp%,nn%)+m$,nn%)
          IF m$ = s$ THEN
            cnt& = mr&
            EXIT DO                                   
          ELSEIF m$ > s$ THEN                
            Top& = mr& - 1                             
          ELSE                                        
            Bottom& = mr& + 1                          
          END IF
          f$ = "": cnt& = 0
        LOOP
      END IF
      CLOSE #LookDev
    END IF
    IF f$="" THEN
      ov$="0000"
    ELSE
      ov$ = RIGHT$(STR$(cnt&+10000),4) + f$
    END IF
  END IF
  Lookup% = LEN(ov$)>4
END FUNCTION 'Lookup%
''##E
'
' END OF MODULE AiEnv.bas







