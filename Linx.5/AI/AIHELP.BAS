' AiHelp.bas
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'%%%%%%%%%%%                                                       %%%%%%%%%%
'%%%%%%%%%%%                     A i H e l p                       %%%%%%%%%%
'%%%%%%%%%%%                                                       %%%%%%%%%%
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'
'   Project  : Applications Interface                   Version : 1.1
'   Usage    : Help display support                     OS : PCDOS V3.3
'   Language : Microsoft Quick Basic V4.5               CS : IBM PC/AT
'
'   Purpose : Provides access to the help file
'
'-------------------------------- History -----------------------------------
'
'      Version     Date      Who     Short description of change
'        1.0    15 Jul 90    JHF     Original Creation
'        1.1    24 Sep 92    GMH     Changed location of "Page x of x" to 64
'
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'%%%%%%%%%%        CopyRight 1990, Linx Data Terminals, Inc.       %%%%%%%%%%
'%%%%%%%%%%                  All Rights Reserved.                  %%%%%%%%%%
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
DEFINT A-Z
'$INCLUDE:'AiVar.BI'
'$INCLUDE:'AiUtil.BI'
'$INCLUDE:'AiHelp.BI'

'****************************************************************************
'
'      HelpLookup% : Returns the table index of a help subject
'
'      i = HelpLookup% ( subject )
'
'****************************************************************************
FUNCTION HelpLookup% ( ss$ )
  s$ = UCASE$(ss$)
  FOR i = 1 TO HelpTotal
    t$ = RTRIM$(HelpTOC(i).LName)
    IF t$=s$ THEN HelpLookup% = i : EXIT FUNCTION
  NEXT i
  HelpLookup% = 0
END FUNCTION ' HelpLookup%

'****************************************************************************
'
'   HelpOpen : Opens and reads a help file into the TOC
'
'   HelpOpen ( filenumber, "filename", "application title" )
'
'****************************************************************************
SUB HelpOpen ( n, f$, t$ )
  HelpF      = n
  HelpFile$  = f$
  HelpTitle$ = t$
  IF OpenInput ( HelpF, f$ ) THEN
    
    '----- There are 3 lines of heading information -----
    LINE INPUT #HelpF, s$
    LINE INPUT #HelpF, s$
    LINE INPUT #HelpF, s$
    
    '----- LINE 4 : Help Index Title -----
    LINE INPUT #HelpF, HelpTOCTitle$
    HelpTOCTitle$ = LTRIM$(HelpTOCTitle$)
    
    '----- LINE 5 : HelpTOCcnt and HelpTotal -----
    INPUT #HelpF, HelpTOCcnt, HelpTotal
    
    '----- Table of contents (3 lines per entry) -----
    FOR i = 1 TO HelpTotal
      INPUT #HelpF, HelpTOC(i).DName
      INPUT #HelpF, HelpTOC(i).LName
      INPUT #HelpF, HelpTOC(i).SubPos, HelpTOC(i).SubPages
    NEXT i
  ELSE
    AiFerr IoErr$
  END IF
END SUB

'****************************************************************************
'
'   HelpBorder : Draws the help border and clears the insides
'
'****************************************************************************
SUB HelpBorder
  COLOR Cyan, Black
  CLS
  LOCATE  1,  1 : PRINT "�";STRING$(77,"�");"�";
  LOCATE  2,  1 : PRINT "� ";
  LOCATE  2, 79 : PRINT "� ";
  LOCATE  3,  1 : PRINT "�";STRING$(77,"�");"�";
  LOCATE 25,  1 : PRINT "�";STRING$(77,"�");"�";
  FOR i = 2 TO 24
    LOCATE i,  1 : PRINT "�";
    LOCATE i, 79 : PRINT "�";
  NEXT i
  COLOR LightGreen, Black : LOCATE  2,  3 : PRINT HelpTitle$;
  COLOR Cyan, Black : PRINT " -";
END SUB 'HelpBorder

'****************************************************************************
'
'   HelpShowPage : Displays a particular subject and page
'
'   HelpShowPage  subject#, page#
'
'****************************************************************************
SUB HelpShowPage ( sbj, pg )
  
  ' ----- Clear the screen -----
  IF sbj > 0 OR pg = 0 THEN
    s$ = SPACE$(77)
    toff = LEN(HelpTitle$)+6
    COLOR LightCyan, Black
    LOCATE 2, toff : PRINT SPACE$(78-toff);
    FOR i = 4 TO 24 : LOCATE i, 2 : PRINT s$; : NEXT i
  END IF
  
  ' ------- Display a normal subject -------
  IF sbj > 0 THEN
    
    ' ----- Display the title and page numbers -----
    p& = HelpTOC(sbj).SubPos
    n  = HelpTOC(sbj).SubPages
    IF p& = 0 OR n<pg THEN
      LOCATE 13, 20
      PRINT "No Help Is Available for Subject #";sbj;"page";pg
      EXIT SUB
    END IF
    SEEK #HelpF, p&
    LINE INPUT #HelpF, s$
    COLOR LightCyan, Black : LOCATE 2, toff : PRINT s$;
    COLOR LightGreen, Black : LOCATE 2, 64 : PRINT "Page";
    COLOR LightCyan  : PRINT pg;
    COLOR LightGreen : PRINT "of";
    COLOR LightCyan  : PRINT n;
    COLOR LightGreen
    
    ' ----- Skip to the correct page and display it-----
    j = 5
    FOR i = 1 TO pg
      DO
        LINE INPUT #HelpF, s$
        ok = LEFT$(s$,1)<>CHR$(FF)
        IF i=pg AND ok THEN LOCATE j, 8 : j=j+1 : PRINT s$;
      LOOP WHILE ok
    NEXT i
    
    ' ------- Display the index -------
  ELSE
    ln = 4
    IF pg = 0 THEN COLOR LightCyan : LOCATE 2,toff : PRINT HelpTOCTitle$;
    FOR i = 1 TO HelpTOCcnt
      IF i MOD 2 = 0 THEN j = 40 ELSE j = 8 : ln = ln+1
      s$ = RTRIM$(HelpTOC(i).DName)
      COLOR LightGreen, Black
      LOCATE ln, j
      IF pg = 0 THEN
        PRINT s$;
      ELSE
        IF ABS(pg) = i THEN
          IF pg > 0 THEN COLOR Black, LightGray
          PRINT s$;
        END IF
      END IF
    NEXT i
  END IF
  
END SUB 'HelpShowPage

'****************************************************************************
'
'   Help : Displays a particular subject by name and waits
'
'   Help "subject"
'
'****************************************************************************
SUB Help ( s$ )
  HelpBorder
  pn  = 1
  sbj = HelpLookup% ( s$ )
  DO
    ' ----- Scan the help index -----
    IF sbj = 0 THEN
      HelpShowPage 0, 0
      pn = 1 : opn = 1000 :  n = HelpTOCcnt
      rekey = FALSE
      DO
        HelpShowPage 0, pn
        opn = pn
        IF NOT rekey THEN
          DO : ky$ = INKEY$ : LOOP WHILE LEN(ky$)=0
          IF LEN(ky$)=1 THEN k=ASC(ky$) ELSE k=-ASC(MID$(ky$,2))
        END IF
        rekey = FALSE
        SELECT CASE k
          CASE Home   : pn = 1
          CASE EndKey : pn = n
          CASE Left   : IF pn > 1   THEN pn = pn - 1 ELSE pn = n
          CASE Right  : IF pn < n   THEN pn = pn + 1 ELSE pn = 1
          CASE Up     : IF pn > 2   THEN pn = pn - 2 ELSE pn = n
          CASE Down   : IF pn < n-1 THEN pn = pn + 2 ELSE pn = 1
          CASE CR     : sbj = pn
          CASE F1     : EXIT SUB
          CASE ELSE   : EXIT SUB
        END SELECT
        IF HelpTOC(pn).SubPages = 0 THEN rekey = TRUE
        HelpShowPage 0, -opn
      LOOP WHILE sbj = 0
    ELSE
      ' ----- Normal Page Display -----
      n = HelpTOC(sbj).SubPages
      pn = 1
      DO
        HelpShowPage sbj, pn
        DO : ky$ = INKEY$ : LOOP WHILE LEN(ky$)=0
        IF LEN(ky$)=1 THEN k=ASC(ky$) ELSE k=-ASC(MID$(ky$,2))
        SELECT CASE k
          CASE Home   : pn = 1
          CASE EndKey : pn = n
          CASE PgUp   : IF pn > 1 THEN pn = pn - 1 ELSE pn = n
          CASE PgDn   : IF pn < n THEN pn = pn + 1 ELSE pn = 1
          CASE F1     : sbj = 0
          CASE ELSE   : EXIT SUB
        END SELECT
      LOOP WHILE sbj <> 0
    END IF
  LOOP
END SUB 'Help

' End of MODULE AiHelp.BAS



