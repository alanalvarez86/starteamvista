' COMIN.bas
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'%%%%%%%%%%%                     C O M I N                         %%%%%%%%%%
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'
'   Project  : LinxBasic                                Version : 1.0
'   Usage    : Low level LINX IO Checkout               OS : PCDOS V3.3
'   Language : LinxBASIC V1.0                           CS : IBM PC/AT
'
'   Purpose  : Inputs data from the COM port and echos the results.
'   Requires : First test port with COMOUT.bas.  
'              Note! Do not run on the LINX MASTER!
'   Running on a MASTER will result in a NOT A NORMAL TERMINAL ERROR.
'
'   Version     Date      Who     Short description of change
'     1.0    11 Aug 89    JHF     Original creation 
'
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'%%%%%%%%%%        CopyRight 1989, Linx Data Terminals, Inc.       %%%%%%%%%%
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
DEFINT A-Z 
OPEN "COM" FOR RANDOM AS #1
PRINT #1,"- Enter data."
DO 
  DO WHILE LOC(#1)=0 : LOOP             ' Wait for data.
  t$ = INPUT$(LOC(#1),#1)               ' Get the data.
  PRINT #1,"<"+t$+">"                   ' Display the data.
LOOP



  

