' AUXOUT.bas
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'%%%%%%%%%%%                     A U X O U T                       %%%%%%%%%%
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'
'   Project  : LinxBasic                                Version : 1.0
'   Usage    : Low level LINX IO Checkout               OS : PCDOS V3.3
'   Language : LinxBASIC V1.0                           CS : IBM PC/AT
'
'   Purpose  : Outputs records to the AUX port
'
'   Version     Date      Who     Short description of change
'     1.0    11 Aug 89    JHF     Original creation 
'
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'%%%%%%%%%%        CopyRight 1989, Linx Data Terminals, Inc.       %%%%%%%%%%
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
DEFINT A-Z 
OPEN "AUX" FOR OUTPUT AS #1
t$ = " > ABCDEFGHIJKLMNOPQRSTUVWXYZ "
t$ = t$+"abcdefghijklmnopqrstuvwxyz 1234567890"
i& = i& + 1
DO 
  PRINT #1,i&;t$
  i& = i&+1
LOOP
