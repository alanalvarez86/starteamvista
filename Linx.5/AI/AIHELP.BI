' AiHelp.BI - Basic Include file
'
DEFINT A-Z

'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@                          Constants and Types                            @@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'
CONST HelpTOCmax = 100

TYPE HelpInx
  DName         AS STRING * 40  ' Name to display
  LName         AS STRING * 20  ' Name to match/look up
  SubPos        AS LONG         ' Subject position in the help file
  SubPages      AS INTEGER      ' Number of pages in the subject
END TYPE ' HelpInx

'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@                               Common Section                            @@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'
' ----------  Help Index Structure ----------
COMMON SHARED /Help/ HelpFile$        'Help file name
COMMON SHARED /Help/ HelpF            'Help file number
COMMON SHARED /Help/ HelpTOCcnt       ' Number of elements in Help TOC
COMMON SHARED /Help/ HelpTotal        ' Includes all other help indexes
COMMON SHARED /Help/ HelpTitle$       ' Title of the General Help Facility
COMMON SHARED /Help/ HelpTOCTitle$    ' Title of the Table of Contents

'$DYNAMIC
COMMON SHARED /Help/ HelpTOC()            AS HelpInx
'$STATIC

'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@                     Sub and Function Declarations                       @@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'
DECLARE FUNCTION HelpLookup%    ( ss$ )
DECLARE SUB      HelpOpen       ( n, f$, t$ )
DECLARE SUB      HelpBorder     ()
DECLARE SUB      HelpShowPage   ( sbj, pg )
DECLARE SUB      Help           ( s$ )

'End AiHelp.BI


