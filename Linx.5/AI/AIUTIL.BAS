' AiUtil.bas
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'%%%%%%%%%%%                                                       %%%%%%%%%%
'%%%%%%%%%%%                       A i U t i l                     %%%%%%%%%%
'%%%%%%%%%%%                                                       %%%%%%%%%%
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'
'   Project  : Applications Interface                   Version : 1.0
'   Usage    : Simulation I/O utilities                 OS : PCDOS V3.3
'   Language : Microsoft Quick Basic V4.5               CS : IBM PC/AT
'
'   Purpose : LINX dependent device IO emulation for IBM PC compatibles
'
'   These routines provide access to the AI menus and device emulation.
'   Most PC display control occurs here.
'
'-------------------------------- History -----------------------------------
'
'      Version     Date      Who     Short description of change
'        1.0     5 May 90    JHF     Created from ENVPC.BAS
'
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'%%%%%%%%%%        CopyRight 1990, Linx Data Terminals, Inc.       %%%%%%%%%%
'%%%%%%%%%%                  All Rights Reserved.                  %%%%%%%%%%
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
DEFINT A-Z
'$INCLUDE:'AiVar.BI'
'$INCLUDE:'AiUtil.BI'
'$INCLUDE:'AiMenu.BI'

TYPE RegType
  ax    AS INTEGER
  bx    AS INTEGER
  cx    AS INTEGER
  dx    AS INTEGER
  bp    AS INTEGER
  si    AS INTEGER
  di    AS INTEGER
  flags AS INTEGER
END TYPE

DECLARE SUB INTERRUPT (intnum AS INTEGER,inreg AS RegType,outreg AS RegType)


GOTO AiUtilEnd

SUB AiUtilInit
  AiDX%       = 1     ' Saved cursor location during full screen access
  AiDY%       = 1
  AiOldSense% = 0     ' Previous value of the sense line
  Ai40%       = FALSE ' 40 character screen mode is on
  AiWidth%    = 80    ' 80 characters in the screen
  AiLogging%  = FALSE ' Logging in progress
END SUB

'****************************************************************************
'
'   IsColor% : True if the screen uses color
'
'   IF IsColor% THEN ...
'
'****************************************************************************
FUNCTION IsColor%
  DIM inr  AS RegType
  DIM outr AS RegType
  inr.ax = &h0F00
  CALL INTERRUPT (&h10, inr, outr)
  IsColor% = (outr.ax AND &hFF) <> 7
END FUNCTION 'IsColor%

'****************************************************************************
'
'   OpenInput  : Returns TRUE if the file could be opened for input
'   OpenOutput : Returns TRUE if the file could be opened for output
'   OpenAppend : Returns TRUE if the file could be opened for append
'
'   IF OpenInput ( fn, filename$ ) THEN ...
'
'****************************************************************************
FUNCTION OpenInput ( n,  f$ )
  FileN$ = f$ : ReturnCode = TRUE : ON ERROR GOTO FileInputErr
  OPEN f$ FOR INPUT AS #n
  OpenInput = ReturnCode : ON ERROR GOTO 0
END FUNCTION 'OpenInput

FUNCTION OpenAppend ( n,  f$ )
  FileN$ = f$ : ReturnCode = TRUE : ON ERROR GOTO FileInputErr
  OPEN f$ FOR APPEND AS #n
  OpenAppend = ReturnCode : ON ERROR GOTO 0
END FUNCTION 'OpenAppend

FUNCTION OpenOutput ( n,  f$ )
  FileN$ = f$ : ReturnCode = TRUE : ON ERROR GOTO FileInputErr
  OPEN f$ FOR OUTPUT AS #n
  OpenOutput = ReturnCode : ON ERROR GOTO 0
END FUNCTION 'OpenOutput

FUNCTION OpenBinary ( n, f$, l )
  FileN$ = f$ : ReturnCode = TRUE : ON ERROR GOTO FileInputErr
  OPEN f$ FOR BINARY AS #n LEN=l
  OpenBinary = ReturnCode : ON ERROR GOTO 0
END FUNCTION 'OpenBinary

FileInputErr:                   ' Error handler in module level code.
ReturnCode = FALSE            ' Return a failure completion.
i = ERR
IoErr$ = "File "+FileN$+" was not found."
IF i=53 THEN RESUME NEXT
IoErr$ = FileN$+" is not a valid filename."
IF i=64 THEN RESUME NEXT
IoErr$ = "File error"+STR$(i)+" on file "+FileN$
RESUME NEXT

'****************************************************************************
'
'  addext$ : adds an extension to a filename string if it needs one
'
'  f$ = addext$ ( sourcefile, "TXT" );
'
'****************************************************************************
FUNCTION addext$ ( s$, e$ )
  i = INSTR(s$,"/") : IF i<>0 THEN s$ = LEFT$(s$,i)
  i = INSTR(s$,".") : IF i=0 THEN addext$ = s$+"."+e$ ELSE addext$ = s$
END FUNCTION 'addext$

'****************************************************************************
'
'  getext$ : returns the extension of a filename string
'
'  p$ = getext$ ( file$ );
'
'****************************************************************************
FUNCTION getext$ ( s$ )
  i = INSTR(s$,"/") : IF i<>0 THEN s$ = LEFT$(s$,i)
  i = INSTR(s$,".") : IF i=0 THEN getext$ = "" ELSE getext$ = MID$(s$,i+1)
END FUNCTION 'getext$

'****************************************************************************
'
'  setext$ : puts an extension on a filename, overwriting the old one
'
'  f$ = setext$ ( file$, "LST" );
'
'****************************************************************************
FUNCTION setext$ ( s$, e$ )
  i = INSTR(s$,"/") : IF i<>0 THEN s$ = LEFT$(s$,i)
  i = INSTR(s$,".")
  IF i = 0 THEN setext$ = s$+"."+e$ ELSE setext$ = LEFT$(s$,i)+e$
END FUNCTION 'setext$

'****************************************************************************
'
'   Switch : Returns TRUE if the specified switch is in the command line
'
'   IF Switch(command$,"s") THEN ... switch /s is in command$
'
'****************************************************************************
FUNCTION Switch ( c$, s$ )
  t$ = UCASE$(c$)                       ' Ignore case in command line.
  Switch = TRUE
  DO                                    ' Scan all the switches
    i = INSTR(t$,"/")                   ' Switches start with /.
    IF i>0 THEN
      t$ = MID$(t$,i+1)                 ' Isolate the switch.
      IF s$=LEFT$(t$,LEN(s$)) THEN EXIT FUNCTION
    END IF
  LOOP WHILE i>0                        ' Any more to try?
  Switch = FALSE                        ' Switch not found.
END FUNCTION 'Switch

'****************************************************************************
'
'   CommandArg$ : Returns the next argument from command line
'
'   The command$ is modified.  The next isolated argument is returned.
'
'   arg$ = CommandArg$ ( command$ )
'
'****************************************************************************
SUB ScanSpaces ( c$ )
  DO WHILE LEFT$(c$,1)=" "
    c$ = MID$(c$,2)
  LOOP
END SUB 'ScanSpaces

FUNCTION ScanStuff$ ( c$ ) STATIC
  r$ = ""
  DO
    t$ = LEFT$(c$,1)
    IF t$<>" " AND t$<>"/" THEN
      r$ = r$+t$
      c$ = MID$(c$,2)
    END IF
  LOOP UNTIL t$="" OR t$=" " OR t$="/"
  ScanStuff$ = r$
END FUNCTION 'ScanStuff$

FUNCTION CommandArg$ ( c$ )
  CommandArg$ = ""
  IF LEN(c$)>0 THEN
    DO WHILE LEFT$(c$,1) = "/"
      c$ = MID$(c$,2)                   ' Skip over the /
      x$ = ScanStuff$ ( c$ )            ' Skip the switch itself.
      ScanSpaces c$                     ' Ignore trailing spaces.
    LOOP
    ScanSpaces c$                       ' Ignore leading spaces
    CommandArg$ = ScanStuff$ ( c$ )
  END IF
END FUNCTION 'CommandArg$
'****************************************************************************
'
'   EnvDir$ : Returns a directory name from the environment variable
'
'   dir$ = EnvDir$ ( "envvar" )
'
'****************************************************************************
FUNCTION EnvDir$ ( c$ )
  t$ = ENVIRON$(c$)
  IF LEN(t$)>0 THEN
    IF MID$(t$,LEN(t$))<>"\" THEN t$=t$+"\"
  END IF
  EnvDir$ = t$
END FUNCTION 'EnvDir$
'
'****************************************************************************
'
'       ClrLinx : Clears the linx display
'
'****************************************************************************
SUB ClrLinx
  LOCATE 1, 1, 0
  PRINT SPACE$(40) : PRINT SPACE$(40) : Uv(SvDSP)=""
  LOCATE 1, 1
END SUB 'ClrLinx
'
'****************************************************************************
'
'       IsNum% : True if a string is numeric
'
'****************************************************************************
FUNCTION IsNum% ( s$ )
  IsNum% = FALSE : k = LEN(s$) : m = 0
  FOR i=1 TO k
    j=ASC(MID$(s$,i,1))
    IF j<>&H20 AND j<>&H2E AND j<>&H2D AND (j<&H30 OR j>&H39) THEN
      EXIT FUNCTION
    END IF
    IF j=&H20 OR j=&H2D OR j=&H2E THEN m=m+1
  NEXT i
  isNum% = m<k
END FUNCTION 'IsNum%
'
'****************************************************************************
'
'       DspLinx : Displays the current linx display string
'
'****************************************************************************
SUB DspLinx
  LOCATE 1, 1, 0 : PRINT LEFT$(Uv(SvDSP)+SPACE$(40),40)
  PRINT MID$(Uv(SvDSP)+SPACE$(80),41,40); : LOCATE 1, 1
END SUB 'ClrLinx
'
'****************************************************************************
'
'       LMID : String concat with space padding
'
'       LMID  dst$, pos, len, src$
'
'****************************************************************************
SUB LMID ( d$, p%, i%, s$ )
  k=LEN(d$) : IF k < p%+i%-1 THEN d$=d$+SPACE$(p%+i%-1-k)
  MID$(d$,p%,i%) = s$
END SUB 'LMID
'
'****************************************************************************
'
'       LNUM$ : real number to string
'
'****************************************************************************
FUNCTION LNUM$ ( v! )
  s$ = LTRIM$(STR$(v!))
  IF v!<1.0 AND v!>0.0 THEN s$="0"+s$
  LNUM$ = s$
END FUNCTION 'LNUM$

'****************************************************************************
'
'       VA& : safe string to LONG
'
'****************************************************************************
FUNCTION VA& ( ss$ )
  s$ = LTRIM$(RTRIM$(ss$))
  VA&     = 0
  VAERROR = TRUE
  IF LEN(s$)>9 THEN EXIT FUNCTION
  v&      = VAL(s$)
  IF s$ <> LTRIM$(STR$(v&)) THEN EXIT FUNCTION
  VAERROR = FALSE
  VA&     = v&
END FUNCTION 'VA&
'
'****************************************************************************
'
'       SaveXY    : Saves the current linx cursor
'       RestoreXY : Restores the current linx cursor
'
'****************************************************************************
SUB SaveXY
  AiDX% = POS(0) : AiDY% = CSRLIN : LOCATE ,,0
END SUB
SUB RestoreXY
  LOCATE AiDY%, AiDX%, 1
END SUB
'
'****************************************************************************
'
'       AiStat : Displays a status line
'
'       AiStat ( "status message" )
'
'****************************************************************************
SUB AiStat ( s$ )
  x = POS(0) : y = CSRLIN
  COLOR LightGreen, Black
  LOCATE AiStatY, 1 : PRINT SPACE$(AiWidth%-1);
  LOCATE AiStatY, 1 : PRINT LEFT$(" "+s$, AiWidth%-1);
  LOCATE y, x
END SUB 'AiStat

'****************************************************************************
'
'       AiHelp : Displays a help line
'
'       AiHelp ( "help message" )
'
'****************************************************************************
SUB AiHelp ( s$ )
  x = POS(0) : y = CSRLIN
  COLOR White, Red
  LOCATE AiHelpY, 1 : PRINT SPACE$(AiWidth%-1);
  LOCATE AiHelpY, 1 : PRINT LEFT$(" "+s$,AiWidth%-1);
  COLOR LightGreen, Black
  LOCATE y, x
END SUB 'AiHelp


'****************************************************************************
'
'       AiGenStat : Displays memory usage stats.
'
'       AiGenStat ( "message" )
'
'****************************************************************************
SUB AiGenStat ( s$ )
  STATIC nx
  iy = 12: ix = 18
  x = POS(0) : y = CSRLIN
  nx = nx + 1
  COLOR LightRed, Black
  LOCATE iy+nx,ix : PRINT SPACE$(AiWidth%-ix+2);
  LOCATE iy+nx,ix : PRINT LEFT$(s$,AiWidth%-ix+2);
  COLOR LightGreen, Black
  LOCATE y, x
END SUB 'AiHelp

'****************************************************************************
'
'       AiErr : Displays an error line
'
'       AiErr ( "error message" )
'
'****************************************************************************
SUB AiErr ( s$ )
  COLOR LightRed, Black
  LOCATE AiHelpY, 1 : PRINT SPACE$(AiWidth%-1);
  LOCATE AiHelpY, 1 : PRINT LEFT$(" "+s$, AiWidth%-1);
  SOUND 100, 3
  PState = xStopped
  SLEEP 2
END SUB 'AiErr

'****************************************************************************
'
'       AiFErr : Displays a fatal error line
'
'       AiFErr ( "error message" )
'
'****************************************************************************
SUB AiFErr ( s$ )
  AiErr "* FATAL ERROR: "+s$
  COLOR LightGray, Black
  SLEEP 5
  CLS
  PRINT "* FATAL ERROR: "+s$
  SYSTEM
END SUB 'AiFErr

'****************************************************************************
'
'       AiSaveDisplay : Saves the LINX display to a string
'
'****************************************************************************
SUB AiSaveDisplay
  AiSavedScreen$ = ""
  FOR i=1 TO 2 : FOR j=1 TO 40
    AiSavedScreen$ = AiSavedScreen$ + CHR$(SCREEN(i,j))
  NEXT j : NEXT i
END SUB 'AiSaveDisplay

'****************************************************************************
'
'       AiRestoreDisplay : Restored the saved LINX display to the screen
'
'****************************************************************************
SUB AiRestoreDisplay
  COLOR Yellow, Black
  LOCATE 1,1 : PRINT LEFT$ (AiSavedScreen$,40)
  LOCATE 2,1 : PRINT RIGHT$(AiSavedScreen$,40)
END SUB 'AiRestoreDisplay

'****************************************************************************
'
'       AiFix$ : Converts control chars to readable ones
'
'       AiFix$ ( string, count )
'
'****************************************************************************
FUNCTION AiFix$ ( ss$, length% )
  a$ = ""
  s$ = RTRIM$(ss$)
  j  = LEN(s$)
  IF length%>0 AND j>length% THEN j=length%
  i  = 1
  DO WHILE i<=j
    c  = ASC(MID$(s$,i,1))
    i = i+1
    IF c < 32 THEN
      IF c <=CR THEN
        IF c=0   THEN a$=a$+"<NUL>"
        IF c=SOH THEN a$=a$+"<SOH>"
        IF c=STX THEN a$=a$+"<STX>"
        IF c=ETX THEN a$=a$+"<ETX>"
        IF c=EOT THEN a$=a$+"<EOT>"
        IF c=ACK THEN a$=a$+"<ACK>"
        IF c=BEL THEN a$=a$+"<BEL>"
        IF c=BS  THEN a$=a$+"<BS>"
        IF c=HT  THEN a$=a$+"<HT>"
        IF c=LF  THEN a$=a$+"<LF>"
        IF c=VT  THEN a$=a$+"<VT>"
        IF c=CR  THEN a$=a$+"<CR>"
      ELSE
        IF c=ESC THEN a$=a$+"<ESC>" ELSE a$=a$+"<"+LTRIM$(RTRIM$(STR$(c)))+">"
      END IF
    ELSE
      IF c>126 THEN a$=a$+"<"+STR$(c)+">" ELSE a$=a$+CHR$(c)
    END IF
  LOOP
  IF length% > 0 THEN
    IF LEN(a$) < length% THEN
      a$ = a$ + SPACE$(length%-LEN(a$))
    ELSE
      a$ = LEFT$(a$,length%)
    END IF
  END IF
  AiFix$ = a$
END FUNCTION 'AiFix$

'****************************************************************************
'
'       AiReadKey% : Internal single key read
'       AiSnagKey% : Internal single key read without waiting
'
'       key = AiReadKey%
'
'****************************************************************************
FUNCTION AiSnagKey%
  AiSnagKey% = 0 : ky$ = INKEY$
  IF LEN(ky$)=0 THEN EXIT FUNCTION
  IF LEN(ky$)=1 THEN k=ASC(ky$) ELSE k=-ASC(MID$(ky$,2))
  AiSnagKey% = k
END FUNCTION 'AiSnagKey%

FUNCTION AiReadKey%
  DO : k=AiSnagKey% : LOOP WHILE k=0 : AiReadKey% = k
END FUNCTION 'AiReadKey%

'****************************************************************************
'
'       AiChr$ : Converts a ASCII value to an upper case letter
'
'       string = AiChr$( asciivalue )
'
'****************************************************************************
FUNCTION AiChr$( c% )
  IF c% > 0 THEN AiChr$ = UCASE$(CHR$(c%))
END FUNCTION 'AiChr$

'****************************************************************************
'
'       AiReadStr% : Internal Edit of a string on the display
'
'       keycode = AiReadStr ( length, string, stringlength, c1, c2, bk )
'
'****************************************************************************
FUNCTION AiReadStr% ( length%, s$, slen%, c1, c2, bk )
  imode = TRUE
  x = POS(0) : y = CSRLIN
  pad$=" " : clearNow = TRUE : LOCATE y, x, 1, 1, 9
  DO WHILE LEN(s$)<slen%
    s$ = s$+pad$
  LOOP
  p = 0 : n = 0 : ends = slen% - 1
  COLOR c1, bk
  DO '***** Process a keystroke loop *****
    IF p < 0       THEN p = 0 : n=n-1
    IF n < 0       THEN n = 0
    IF p > length% THEN p = length% : n = n+1
    IF p+n > slen% THEN n = slen%-p
    IF imode THEN LOCATE y, x, 1, 8, 9 ELSE LOCATE y, x, 1, 1, 9
    PRINT MID$(s$,n+1,length%);
    IF x+p <= AiWidth% THEN LOCATE y, x+p
    k = AiReadKey%
    exitc = 0
    SELECT CASE k ' Key function selection
      CASE BS        ' ----- Delete char preceding cursor -----
        IF n+p>0 THEN
          p = p-1 :i = p+n
          DO WHILE i<slen%
            IF i=ends THEN
              MID$(s$,i+1,1) = pad$
            ELSE
              MID$(s$,i+1,1) = MID$(s$,i+2,1)
            END IF
            i=i+1
          LOOP
        END IF
      CASE -77       ' ----- Move right in the current field -----
        IF p<length% THEN
          p=p+1
        ELSE
          IF n+p<slen% THEN n=n+1 ELSE exitc=Right
        END IF
      CASE -75      ' ----- Move left in the current field -----
        IF p<>0 THEN
          p = p - 1
        ELSE
          IF n<>0 THEN n=n-1 ELSE exitc=Left
        END IF
      CASE Home      ' ----- Home the the start of field -----
        IF n+p<>0 THEN n=0 : p=0 ELSE exitc=Home
      CASE EndKey    ' ----- Move cursor to the end of the field -----
        i = n+p
        DO WHILE i<slen% AND MID$(s$,i+1,1)=pad$
          i=i+1
        LOOP
        IF i=slen% THEN
          exitc = EndKey
        ELSE
          p = slen%-1
          DO WHILE p>=0 AND MID$(s$,p+1,1)=pad$
            p = p-1
          LOOP
          p=p+1 : n=0
          IF p > length% THEN n=p-length% : p=length%
        END IF
      CASE DelKey    ' ----- Deletes char cursor is on -----
        i = n+p
        DO WHILE i<slen%
          IF i=ends THEN
            MID$(s$,i+1,1) = pad$
          ELSE
            MID$(s$,i+1,1) = MID$(s$,i+2,1)
          END IF
          i=i+1
        LOOP
      CASE CtrlEnd   ' ----- Deletes to the end of the field -----
        FOR i=n+p TO slen%-1
          MID$(s$,i+1,1) = pad$
        NEXT i
      CASE InsKey    ' ----- Toggles insert/overwrite mode -----
        imode = NOT imode
      CASE ELSE      ' ----- Put normal character in buffer -----
        IF k>=32 AND k<127 THEN
          COLOR c2, bk
          IF clearNow THEN
            FOR i=n+p TO slen%-1
              MID$(s$,i+1,1) = pad$
            NEXT i
          END IF
          IF n+p <> slen% THEN
            IF imode THEN
              i = ends
              DO WHILE i > n+p
                MID$(s$,i+1,1) = MID$(s$,i,1)
                i = i-1
              LOOP
            END IF
            MID$(s$,n+p+1,1) = CHR$(k)
            p = p+1
          END IF
          IF autoEx AND n+p=slen% THEN exitc=EnterKey
        ELSE
          exitc = k
        END IF
    END SELECT ' Key function selection
    clearNow = FALSE
  LOOP WHILE exitc = 0 '***** Process a keystroke loop *****
  '---------- Return the data ----------
  i = ends
  DO WHILE i>0 AND MID$(s$,i+1,1)=pad$
    i = i-1 ' Lop all trailing pads off of the input field
  LOOP
  i = i + 1
  IF i=1 AND MID$(s$,1,1)=pad$ THEN i=0
  s$ = LEFT$(s$,i)
  LOCATE y, x
  PRINT MID$(s$,1,length%);
  IF exitc = EnterKey THEN exitc = 0
  AiReadStr% = exitc
END FUNCTION 'AiReadStr%

'****************************************************************************
'
'       AiAsk% : Ask for a string of text on the status line
'
'       keycode = AiAsk% ( "text", length, string, stringlength )
'
'****************************************************************************
FUNCTION AiAsk% ( t$, length%, s$, slen% )
  COLOR LightGray, Black
  LOCATE AiStatY, 1 : PRINT SPACE$(AiWidth%-1);
  LOCATE AiStatY, 1 : PRINT t$;" : ";
  i = LEN (t$)+4 : j = length%
  IF j+i > AiWidth% THEN j=AiWidth%-i
  AiAsk% = AiReadStr% ( j, s$, slen%, Black, Black, LightGray )
  AiStat ""
END FUNCTION 'AiAsk%

'****************************************************************************
'
'       AiYN% : Ask for a Yes or No answer
'
'       Returns TRUE if the user entered an answer and flag% has been
'       been set.  FALSE indicates an ESC was pressed.
'
'       IF AiYN% ( "Die?", flag%, default ) THEN ...
'
'****************************************************************************
FUNCTION AiYN% ( t$, flag%, defv% )
  LOCATE ,,0
  AiYN% = TRUE
  DO
    AiStat t$
    k = AiReadKey%
    IF k=ASC("n") OR k=ASC("N") THEN flag%=FALSE : EXIT FUNCTION
    IF k=ASC("y") OR k=ASC("Y") THEN flag%=TRUE  : EXIT FUNCTION
    IF k=EnterKey THEN flag% = defv% : EXIT FUNCTION
  LOOP UNTIL k = ESC
  AiYN% = FALSE
END FUNCTION 'AiYN%

'****************************************************************************
'
'   AiUpdateLeds : PC LINX Emulator LEDs status update
'
'****************************************************************************
SUB AiUpdateLeds
  COLOR Red, LightGray
  IF MONO% THEN COLOR Black
  LOCATE AiLedY,1 : PRINT Uv(SvLED)
END SUB 'AiUpdateLeds

'****************************************************************************
'
'   AiUpdateLines : PC LINX Emulator LINES status update
'
'****************************************************************************
SUB AiUpdateLines
  LOCATE AiLinesY,1
  n$="  RTS"  : v = Uvs(SvRTS1) : GOSUB DoState
  n$="DTR"    : v = Uvs(SvDTR1) : GOSUB DoState
  'n$="DSR1"   : v = Uvs(SvDSR1) : GOSUB DoState
  'PRINT "  ";
  n$="LINE"   : v = Uvs(SvLINESTATE) : GOSUB DoStateL
  n$="SENS"   : v = Uvs(SvSENSE)     : GOSUB DoStateL
  AiUpdateLeds
  EXIT SUB
  
DoState:
  COLOR Black, LightGray
  PRINT N$;"=";
  IF v <> 0 THEN COLOR Red: PRINT "1";  ELSE COLOR Blue: PRINT "0";
  COLOR Black : PRINT " ";
  RETURN
  
DoStateL:
  COLOR Black, LightGray
  PRINT N$;"=";
  cln% = 32
  FOR x = 1 TO 6
    IF v AND cln% THEN COLOR Red: PRINT "1";  ELSE COLOR Blue: PRINT "0";
    cln% = cln% \ 2
  NEXT x
  COLOR Black : PRINT " ";
  RETURN
END SUB 'AiUpdateLines

'****************************************************************************
'
'   AiInputStat : Update the input status indicators
'
'****************************************************************************
SUB AiInputStat
  IF NOT Ai40% THEN
    SaveXY
    COLOR LightGray, Black
    LOCATE  1,DSPWIDTH%+1 : PRINT CHR$(179);
    LOCATE  2,DSPWIDTH%+1 : PRINT CHR$(179);
    COLOR LightRed, Black
    LOCATE 1,44
    SELECT CASE PState
      CASE xStopped  : COLOR White,Red
        PRINT "         Program is Stopped         "
      CASE xRunning  : COLOR White+Blinking, Red
        PRINT "Running ..." ;
        COLOR Cyan, Black
        PRINT "                         "
      CASE xSS       : PRINT "Single stepping.  UP/DOWN to step.  "
      CASE xUndoing  : PRINT "Scanning backwards ...              "
      CASE xScanning : PRINT "Scanning forwards ...               "
    END SELECT
    COLOR Cyan, Black
    LOCATE 2,44 : PRINT SPACE$(36);
    RestoreXY
  END IF
END SUB 'AiInputStat

'****************************************************************************
'
'      AiLog : Logs a message to the log file
'
'****************************************************************************
SUB AiLog ( d$, m$ )
  IF AiLogging% THEN
    PRINT #AiLogF, TIME$+" "+d$+" : "+AiFix$(m$,0)
  END IF
END SUB 'AiLog

'****************************************************************************
'
'      AiBuildDev : Builds an emulation device entry
'
'      AiBuildDev number, "rx name", rxvar, "txname", txvar, dspline
'
'****************************************************************************
SUB AiBuildDev ( n%, rx$, rxn%, tx$, txn%, l% )
  AiD(n%).mode         = 0
  AiD(n%).rxname       = rx$
  AiD(n%).txname       = tx$
  AiD(n%).rx           = rxn%
  AiD(n%).tx           = txn%
  AiD(n%).dspline      = l%
  AiD(n%).nextRead     = 0
  AiD(n%).readInterval = 0
  Uv (AiD(n%).tx)      = ""
  Uv (AiD(n%).rx)      = ""
  Uvs(AiD(n%).rx)      = FALSE ' Mark as not pending
END SUB 'AiBuildDev

'****************************************************************************
'
'      AiDevInx% : Gets a device index from an ASCII code
'
'****************************************************************************
FUNCTION AiDevInx% ( n% )
  i = n% : IF i<0 THEN i=0
  IF i>=ASC("a") AND i<=ASC("z") THEN i = i-32
  ky$ = CHR$(i)
  AiDevInx% = 0
  IF ky$="B" OR n%=AltB THEN AiDevInx% = AiBadge
  IF ky$="W" OR n%=AltW THEN AiDevInx% = AiBadge
  IF ky$="H" OR n%=AltH THEN AiDevInx% = AiNet
  IF ky$="Q" OR n%=AltQ THEN AiDevInx% = AiNet
  IF ky$="C" OR n%=AltC THEN AiDevInx% = AiCom
  IF ky$="A" OR n%=AltA THEN AiDevInx% = AiAux
END FUNCTION 'AiDevInx%

'****************************************************************************
'
'      AiDevUpdate : Updates an emulation device entry
'
'****************************************************************************
SUB AiDevUpdate (N%)
  y = AiD(N%).dspline
  rxn$ = RTRIM$(AiD(N%).rxname)
  bk = LightGray
  IF rxn$ <> "" THEN
    LOCATE y, 1
    COLOR Red, bk
    PRINT AiD(N%).rxname;
    COLOR Black, bk
    PRINT ":";
    IF Uvs(AiD(N%).rx) THEN
      IF NOT MONO% THEN COLOR Red:
      PRINT "p";
    ELSE
      PRINT " ";
    END IF
    COLOR White, Blue
    PRINT LEFT$(Uv(AiD(N%).rx) + SPACE$(AiDwidth), AiDwidth);
    y = y + 1
  END IF
  txn$ = RTRIM$(AiD(N%).txname)
  IF txn$ <> "" THEN
    LOCATE y, 1
    COLOR Black, bk
    PRINT AiD(N%).txname;
    COLOR Black, bk
    PRINT ": ";
    COLOR Yellow, Blue
    PRINT LEFT$(AiFix$(Uv(AiD(N%).tx),AiDwidth)+SPACE$(AiDwidth),AiDwidth);
  END IF
END SUB 'AiDevUpdate

'****************************************************************************
'
'      AiReadDevice% : Does a device read if a record is pending
'
'****************************************************************************
FUNCTION AiReadDevice% ( n%, buffer$ )
  AiReadDevice% = FALSE
  i = AiD(n%).rx
  IF Uvs(i) THEN
    UvWas i : Uvs(i) = FALSE
    buffer$ = RTRIM$ ( Uv(i) )
    UvIs i : AiDevUpdate n%
    AiReadDevice% = TRUE
  END IF
END FUNCTION 'AiReadDevice%

'****************************************************************************
'
'      AiDevRxKbd : Receives an emulation device record from keyboard
'
'****************************************************************************
SUB AiDevRxKbd ( n% )
  dn$ = LEFT$(AiD(n%).rxname,5)
  AiStat "Key in a record to be read from "+dn$
  y = AiD(n%).dspline
  LOCATE y,11
  i = AiD(n%).rx
  rx$ = Uv(i)
  e%  = AiReadStr% ( AiDwidth, rx$, 255, LightGreen, Yellow, Blue )
  IF e%=0 THEN
    UvWas i : Uvs(i) = TRUE
    Uv (i) = rx$ : UvIs i
  END IF
  AiDevUpdate n%
  AiStat "Read "+dn$+" from PC keyboard"
END SUB 'AiDevRxKbd

'****************************************************************************
'
'      AiDevRxFile : Reads an emulation device record from a file
'
'****************************************************************************
SUB AiDevRxFile ( n% )
  dn$ = LEFT$(AiD(n%).rxname,4)
  rf$ = RTRIM$(AiD(n%).rxfile)
  IF AiD(n%).mode>0 THEN
    IF EOF(n%+AiRespFn) THEN
      AiD(n%).mode = 0
      AiStat "End of file on "+rf$+" for "+dn$
      CLOSE n%+AiRespFn
    ELSE
      LINE INPUT #n%+AiRespFn, rx$
      i = AiD(n%).rx
      UvWas i : Uvs(i) = TRUE : Uv (i) = rx$
      UvIs i : AiDevUpdate n%
      AiStat "Read record from "+rf$+" for "+dn$
    END IF
  ELSE
    AiErr "No file is open to read records for "+dn$
  END IF
END SUB 'AiDevRxKbd

'****************************************************************************
'
'      AiInitScreen : Display the startup screen
'
'****************************************************************************
SUB AiInitScreen
  COLOR LightRed, Black
  CLS
  IF OpenInput ( AiInitFn,  AiDir+"AI.SCR" ) THEN
    FOR i = 1 TO 24
      LINE INPUT #AiInitFn, line$
      PRINT line$;
      IF i<24 THEN PRINT
    NEXT i
    CLOSE AiInitFn
    SLEEP 2
    COLOR Yellow, Black
  ELSE
    PRINT "***** ";IoErr$;" *****" : PRINT
    PRINT "This directory does not contain the necessary files"
    PRINT "for AI to run.  You may need to reinstall before you"
    PRINT "can continue."
    PRINT
    PRINT "Please use the evironment variable SET AI=path to AI."
    SYSTEM
  END IF
END SUB 'AiInitScreen

'****************************************************************************
'
'      AiInit : Initializes the emulator
'
'****************************************************************************
SUB AiInit
  AiBuildDev AiBadge," BDG In ",SvEmInB,""        ,0,        4
  AiBuildDev AiNet,  " HST In ",SvEmInH," NET Out",SvEmOutN, 6
  AiBuildDev AiCom,  " COM In ",SvEmInC," COM Out",SvEmOutC, 9
  AiBuildDev AiAux,  " AUX In ",SvEmInA," AUX Out",SvEmOutA, 12
  COLOR Yellow, Black
  CLS
  COLOR Yellow, Black
  WIDTH 80
  LOCATE ,, 1, 8, 9
  LOCATE  1,1 : PRINT SPACE$(DSPWIDTH%);
  LOCATE  2,1 : PRINT SPACE$(DSPWIDTH%);
END SUB 'AiInit

'****************************************************************************
'
'      AiBuildScreen : Builds the emulator screen
'
'****************************************************************************
SUB AiBuildScreen
  COLOR Cyan, Black
  COLOR Black, LightGray
  AiUpdateLines
  FOR i = AiMenuY-1 TO AiVarY - 1
    COLOR Black, LightGray
    LOCATE i, 1
    PRINT SPACE$(40);
  NEXT i
  FOR i = 1 TO AiMax: AiDevUpdate i: NEXT i
  AiInputStat
  COLOR LightCyan, Black
END SUB 'AiBuildScreen

'****************************************************************************
'
'      AiScreen40 : Set screen mode
'
'      AiScreen40  true for 40 column screen
'
'****************************************************************************
SUB AiScreen40 ( m )
  IF m < 2 THEN
    Ai40% = m
    AiSaveDisplay
  END IF
  ON ERROR GOTO ScreenChangeErr
  IF Ai40% THEN WIDTH 40 ELSE WIDTH 80
  IF Ai40% THEN AiWidth% = 40 ELSE AiWidth% = 80
  ON ERROR GOTO 0
  AiBuildScreen
  AiRestoreDisplay
END SUB ' AiScreen40

ScreenChangeErr:                ' Error handler in module level code.
Ai40% = FALSE : RESUME NEXT

'****************************************************************************
'
'       AiEnter : Keyboard access to the emulator
'
'       AiEnter ( key )
'
'****************************************************************************
SUB AiEnter ( e% )
  k = e%
  IF k = HT OR k=ASC("/") THEN
    AiHelp "Badge, Host, Com, Aux, Sense" + _
    ", Next, Disp, Log, File  (press a letter)"
    k = AiReadKey%
  END IF
  i = AiDevInx% (k)
  IF i>0 THEN AiDevRxKbd i : EXIT SUB
  
  ' ----- Modify the value of the sensor port -----
  IF AiChr$(k)="S" OR k=AltS THEN
    s$ = "1"
    DO
      e = AiAsk("Sense Line to Toggle (1-6) ",2,s$,2)
      vv = VAL(s$)
      IF vv<=0 OR vv>6 THEN
        SOUND 100, 1
        AiHelp "** Value must be between 1-6"
      END IF
    LOOP UNTIL (vv>0 AND vv<7) OR e <> 0
    IF e = 0 THEN
      AiOldSense% = Uvs(SvSENSE)
      UvWas SvSENSE
      xs = 1
      FOR txs = 2 TO vv: xs = xs + xs: NEXT txs
      IF xs AND Uvs(SvSENSE) THEN
        Uvs(SvSENSE) = Uvs(SvSENSE) XOR xs
      ELSE
        Uvs(SvSENSE) = Uvs(SvSENSE) OR xs
      END IF
      UvWas SvDSR   : Uvs(SvDSR)   = NOT Uvs(SvDSR)
      IF Uvs(SvSENSE) AND xs THEN s$ = "ON" : ELSE s$ = "OFF"
      UvIs SvSENSE
      AiStat "Sensor"+STR$(vv)+" is now "+s$
      AiUpdateLines
      EXIT SUB
    END IF
  END IF

  ' ----- Put display in 40 column mode (COLOR Displays ONLY!) -----
  IF AiChr$(k)="D" OR k=AltD THEN
    AiScreen40  NOT Ai40%
    EXIT SUB
  END IF

  ' ----- Initiate or terminate logging -----
  IF AiChr$(k)="L" OR k=AltL THEN
    IF AiLogging% THEN
      CLOSE AiLogF
      AiStat "Log file closed."
      AiLogging% = FALSE
    ELSE
      e = AiAsk% ( "Device I/O log file name", 20, rfn$, 60 )
      IF e = 0 THEN
        IF OpenOutput ( AiLogF, rfn$ ) THEN
          AiStat "Opened file "+rfn$+" for device I/O logging"
          AiLogging% = TRUE
        ELSE
          AiErr IoErr$
        END IF
      END IF
    END IF
    EXIT SUB
  END IF
  
  ' ----- Open a file for input -----
  IF AiChr$(k)="F" OR k=AltF THEN
    AiStat "Open file for Badge, Host, Com, or Aux? (press a key)"
    k = AiReadKey%
    i = AiDevInx%(k)
    IF i > 0 THEN
      dn$ = LEFT$(AiD(i).rxname,4)
      IF AiD(i).mode>0 THEN CLOSE(i+AiRespFn)
      AiD(i).mode  = 0
      rfn$ = AiD(i).rxfile
      e = AiAsk% ( "Filename for "+dn$,  40, rfn$, 60 )
      IF e = 0 THEN
        IF OpenInput ( i+AiRespFn, rfn$ ) THEN
          AiD(i).rxfile = rfn$
          AiD(i).mode   = AiManualMode
          AiStat "Opened file "+rfn$+" for device "+dn$
        ELSE
          AiErr IoErr$
        END IF
      END IF
    ELSE
      AiErr("No such device")
    END IF
    EXIT SUB
  END IF
  
  ' ----- Get the next record from an opened text file -----
  IF AiChr$(k)="N" OR k=AltN THEN
    s$ = ""
    f  = 0
    FOR i=1 TO AiMax
      IF AiD(i).mode=AiManualMode THEN
        s$ = s$ + LEFT$(AiD(i).rxname,4)+" "
        f  = i
      END IF
    NEXT i
    IF f=0 THEN AiErr "No device files open in manual mode" : EXIT SUB
    IF LEN(s$)>6 THEN
      AiStat "Select next device read from ( "+s$+")  (press a key)"
      k = AiReadKey%
      f = AiDevInx%(k)
    END IF
    AiDevRxFile ( f )
  END IF
END SUB 'AiEnter

AiUtilEnd:
' End of MODULE AiUtil.BAS

