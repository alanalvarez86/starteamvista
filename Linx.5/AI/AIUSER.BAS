' AiUser.bas
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'%%%%%%%%%%%                                                       %%%%%%%%%%
'%%%%%%%%%%%                      A i U s e r                      %%%%%%%%%%
'%%%%%%%%%%%                                                       %%%%%%%%%%
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'
'   Project  : Applications Interface                   Version : 1.0
'   Usage    : User supplied AI operations              OS : PCDOS V3.3
'   Language : LinxBasic V1.0                           CS : IBM PC/AT
'
'   Purpose : This provides the interface between user written BASIC
'       subroutines, and AI.
'
'   Usage : Fill in all ???'s with appropriate information for your
'       application.  All user variables are present in a string
'       array named Uv.  Your subroutines should only get and return
'       values  by accessing and modifying :
'
'               Uv(VarIn)  : The "IN"  variable
'               Uv(VarOut) : The "OUT" variable
'               Uv(VarSrc) : The "SRC" variable (also error codes)
'               Uv(VarNet) : The "NET" variable
'               Uv(VarCom) : The "COM" variable
'               Uv(VarAux) : The "AUX" variable
'
'       As long as your routines don't modify other variables, and
'       have minimal side effects, then they will work properly in
'       both the Applications Interface environment and when
'       included in the final BASIC program.
'
'       After you have modified this file, you should recompile AI
'       using BCAI.BAT.  AI users will be able to access your
'       routines from the Additional Operations sub-menu in the
'       Other Operations menu.
'
'       Note that in some cases (mostly related to timing, sounds,
'       and special device IO) your user routines may not be able to
'       run identically under the A.I. Environment as they do in
'       the LINX terminal.  The PcMode constant will be TRUE when
'       you are under DOS and FALSE when running in the LINX.
'
'-------------------------------- History -----------------------------------
'
'      Version     Date      Who     Short description of change
'        1.0    dd mmm yy    ???     Original creation
'
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'%%%%%%%%%%        CopyRight 19??, ?????????????????????????       %%%%%%%%%%
'%%%%%%%%%%                  All Rights Reserved.                  %%%%%%%%%%
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

DEFINT A-Z
''##C !
'$INCLUDE:'AiVar.BI'
'$INCLUDE:'AiUser.BI'
'
'    * NOTE! * You MUST provide a name for your AI accessible routine
'    by modifying the values below.  This is the name that will
'    show up in the Additional Operations menu.  If you don't provide
'    any names below, then there will be no additional operations menu.
'
'    Also, you MUST use the names in order.  UserName1 MUST be used
'    before 2 and so on.
'
SUB AiUserInit
  UserName1  = "?"
  UserName2  = "?"
  UserName3  = "?"
  UserName4  = "?"
  UserName5  = "?"
  UserName6  = "?"
  UserName7  = "?"
  UserName8  = "?"
  UserName9  = "?"
END SUB  'AiUserInit
''##E
'
'    Place any subroutines, functions, constants or global variables
'    here.  The AI program can only directly access User1..User9 so
'    you will have to add calls to any such routines in your modified
'    versions of User1..User9.
'
'****************************************************************************

'****************************************************************************
SUB User1
END SUB 'User1

'****************************************************************************
SUB User2
END SUB 'User2

'****************************************************************************
SUB User3
END SUB 'User3

'****************************************************************************
SUB User4
END SUB 'User4

'****************************************************************************
SUB User5
END SUB 'User5

'****************************************************************************
SUB User6
END SUB 'User6

'****************************************************************************
SUB User7
END SUB 'User7

'****************************************************************************
SUB User8
END SUB 'User8

'****************************************************************************
SUB User9
END SUB 'User9

'****************************************************************************

' End of Module AiUser.bas






