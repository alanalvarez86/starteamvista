inherited EditAlarmas: TEditAlarmas
  Caption = 'Alarmas'
  ClientHeight = 333
  ClientWidth = 418
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 88
    Top = 39
    Width = 26
    Height = 13
    Caption = 'Hora:'
  end
  object Label4: TLabel [1]
    Left = 8
    Top = 183
    Width = 90
    Height = 13
    Caption = 'N'#250'mero de Relays:'
  end
  object Label9: TLabel [2]
    Left = 123
    Top = 184
    Width = 30
    Height = 13
    Caption = '1 '#243' 2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsItalic]
    ParentFont = False
  end
  object Label10: TLabel [3]
    Left = 8
    Top = 40
    Width = 27
    Height = 13
    Caption = 'Reloj:'
  end
  object Label11: TLabel [4]
    Left = 162
    Top = 40
    Width = 55
    Height = 13
    Caption = '( HH:MM )'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsItalic]
    ParentFont = False
  end
  inherited PanelBotones: TPanel
    Top = 297
    Width = 418
    TabOrder = 6
    inherited OK: TBitBtn
      Left = 250
    end
    inherited Cancelar: TBitBtn
      Left = 335
    end
  end
  inherited PanelSuperior: TPanel
    Width = 418
    TabOrder = 7
    inherited AgregarBtn: TSpeedButton
      Left = 5
    end
    inherited BorrarBtn: TSpeedButton
      Left = 29
    end
    inherited ModificarBtn: TSpeedButton
      Left = 53
    end
    inherited BuscarBtn: TSpeedButton
      Left = 77
    end
    inherited CortarBtn: TSpeedButton
      Visible = False
    end
    inherited CopiarBtn: TSpeedButton
      Visible = False
    end
    inherited PegarBtn: TSpeedButton
      Visible = False
    end
    inherited UndoBtn: TSpeedButton
      Visible = False
    end
    inherited DBNavigator: TDBNavigator
      Left = 113
      OnClick = DBNavigatorClick
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 418
    TabOrder = 8
    inherited ValorActivo2: TPanel
      Width = 92
    end
  end
  object GroupBox1: TGroupBox [8]
    Left = 8
    Top = 60
    Width = 401
    Height = 73
    Caption = ' D'#237'a '
    TabOrder = 2
    object LUNES: TCheckBox
      Left = 32
      Top = 16
      Width = 73
      Height = 17
      Caption = 'Lunes'
      TabOrder = 0
      OnClick = DIASClick
    end
    object MARTES: TCheckBox
      Left = 128
      Top = 16
      Width = 57
      Height = 17
      Caption = 'Martes'
      TabOrder = 1
      OnClick = DIASClick
    end
    object MIERCOLES: TCheckBox
      Left = 232
      Top = 16
      Width = 73
      Height = 17
      Caption = 'Mi'#233'rcoles'
      TabOrder = 2
      OnClick = DIASClick
    end
    object JUEVES: TCheckBox
      Left = 336
      Top = 16
      Width = 57
      Height = 17
      Caption = 'Jueves'
      TabOrder = 3
      OnClick = DIASClick
    end
    object SABADO: TCheckBox
      Left = 128
      Top = 32
      Width = 65
      Height = 17
      Caption = 'S'#225'bado'
      TabOrder = 5
      OnClick = DIASClick
    end
    object DOMINGO: TCheckBox
      Left = 232
      Top = 32
      Width = 97
      Height = 17
      Caption = 'Domingo'
      TabOrder = 6
      OnClick = DIASClick
    end
    object TODOS: TCheckBox
      Left = 32
      Top = 47
      Width = 105
      Height = 17
      Caption = 'Todos los d'#237'as'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      OnClick = DIASClick
    end
    object VIERNES: TCheckBox
      Left = 32
      Top = 32
      Width = 73
      Height = 17
      Caption = 'Viernes'
      TabOrder = 4
      OnClick = DIASClick
    end
  end
  object TPO_OPERA: TRadioGroup [9]
    Left = 8
    Top = 133
    Width = 401
    Height = 41
    Caption = ' Operaci'#243'n del Relay '
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      'Cerrado'
      'Abierto'
      'Pulso')
    TabOrder = 3
    OnClick = TPO_OPERAClick
  end
  object RELAY: TZetaDBNumero [10]
    Left = 102
    Top = 180
    Width = 19
    Height = 21
    Mascara = mnEmpleado
    MaxLength = 3
    TabOrder = 4
    DataField = 'RELAY'
    DataSource = DataSource
  end
  object GroupBox2: TGroupBox [11]
    Left = 8
    Top = 204
    Width = 401
    Height = 89
    Caption = ' Tiempo: '
    TabOrder = 5
    object Label2: TLabel
      Left = 23
      Top = 17
      Width = 33
      Height = 13
      Alignment = taRightJustify
      BiDiMode = bdRightToLeft
      Caption = 'Activo:'
      ParentBiDiMode = False
    end
    object Label3: TLabel
      Left = 15
      Top = 40
      Width = 41
      Height = 13
      Alignment = taRightJustify
      BiDiMode = bdRightToLeft
      Caption = 'Inactivo:'
      ParentBiDiMode = False
    end
    object Label5: TLabel
      Left = 29
      Top = 63
      Width = 27
      Height = 13
      Alignment = taRightJustify
      BiDiMode = bdRightToLeft
      Caption = 'Total:'
      ParentBiDiMode = False
    end
    object Label6: TLabel
      Left = 90
      Top = 18
      Width = 60
      Height = 13
      Caption = '(Segundos)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsItalic]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 90
      Top = 41
      Width = 60
      Height = 13
      Caption = '(Segundos)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsItalic]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 90
      Top = 64
      Width = 60
      Height = 13
      Caption = '(Segundos)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsItalic]
      ParentFont = False
    end
    object ACTIVO: TZetaDBNumero
      Left = 60
      Top = 14
      Width = 29
      Height = 21
      Hint = 'Hasta 3 d'#237'gitos'
      Enabled = False
      Mascara = mnEmpleado
      MaxLength = 3
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnChange = TIEMPOChange
      DataField = 'ACTIVO'
      DataSource = DataSource
    end
    object TOTAL: TZetaDBNumero
      Left = 60
      Top = 60
      Width = 29
      Height = 21
      Hint = 'Hasta 3 d'#237'gitos'
      Mascara = mnEmpleado
      MaxLength = 3
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      DataField = 'TOTAL'
      DataSource = DataSource
    end
    object INACTIVO: TZetaDBNumero
      Left = 60
      Top = 37
      Width = 29
      Height = 21
      Hint = 'Hasta 3 d'#237'gitos'
      Enabled = False
      Mascara = mnEmpleado
      MaxLength = 3
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnChange = TIEMPOChange
      DataField = 'INACTIVO'
      DataSource = DataSource
    end
  end
  object RELOJ: TZetaDBEdit [12]
    Left = 38
    Top = 36
    Width = 41
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 0
    Text = 'RELOJ'
    DataField = 'RELOJ'
    DataSource = DataSource
  end
  object HORA: TZetaDBHora [13]
    Left = 119
    Top = 36
    Width = 40
    Height = 21
    Hint = 'Formato de 24 horas'
    EditMask = '99:99;0'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    Text = '    '
    Tope = 24
    Valor = '    '
    DataField = 'HORA'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 284
    Top = 98
  end
end
