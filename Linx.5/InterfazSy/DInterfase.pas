unit DInterfase;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBClient, ZetaClientDataSet,
  {$ifdef DOS_CAPAS}
  DServerAsistencia,
  {$else}
  Asistencia_TLB,
  {$endif}
  {$ifdef VER150}Variants,{$endif}
  ZetaTipoEntidad, ZetaTipoEntidadTools, ZetaCommonLists, ZetaCommonClasses,
  ZetaServerDataSet, DZetaServerProvider, IniFiles;

type
  TTArchivosIni = class( TObject )
  private
    { Private declarations }
    FiniFile: TiniFile;
    function GetRutaEmpleado: string;
    function GetRutaAlarma: string;
    function GetReporte: string;
    procedure SetRutaEmpleado( const Value: string );
    procedure SetRutaAlarma( const Value: string );
    procedure SetReporte( const Value: string );
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property RutaEmpleado: string read GetRutaEmpleado write SetRutaEmpleado;
    property RutaAlarma: string read GetRutaAlarma write SetRutaAlarma;
    property Reporte: string read GetReporte write SetReporte;
  end;

  eTipoMovimiento = ( tmEmpleados,
                      tmAlarmas );
                      
  TdmInterfase = class(TDataModule)
    cdsAlarmas: TZetaClientDataSet;
    cdsReloj: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsAlarmasAlAdquirirDatos(Sender: TObject);
    procedure cdsAlarmasAlEnviarDatos(Sender: TObject);
    {$ifdef VER130}
    procedure cdsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    procedure cdsAlarmasAlModificar(Sender: TObject);
    procedure cdsAlarmasAlCrearCampos(Sender: TObject);
    procedure cdsAlarmasBeforePost(DataSet: TDataSet);
    procedure cdsAlarmasNewRecord(DataSet: TDataSet);
    procedure cdsRelojAlAdquirirDatos(Sender: TObject);
    procedure cdsAlarmasAlBorrar(Sender: TObject);
  private
    { Private declarations }
    FHuboCambios, FHuboErrores, FProcesandoThread: Boolean;
    FNumeroReporte: integer;
    FArchivoReporte, FFiltro, FReloj: string;
    FIniValues: TTArchivosIni;
    FTipoMovimiento: eTipoMovimiento;
    FArchivo, FRenglon, FArchivoFinal: TStrings;
    {$ifdef DOS_CAPAS}
    function GetServerAsistencia: TdmServerAsistencia;
    property ServerAsistencia: TdmServerAsistencia read GetServerAsistencia;
    {$else}
    FServidorAsistencia: IdmServerAsistenciaDisp;
    function GetServerAsistencia: IdmServerAsistenciaDisp;
    property ServerAsistencia: IdmServerAsistenciaDisp read GetServerAsistencia;
    {$endif}
    function GetHeaderTexto(const iEmpleado: Integer; const iLinea : Integer = 0): String;
    procedure SetNuevosEventos;
    procedure EscribeBitacora(const sMensaje: String; const iEmpleado: Integer = 0 );
    procedure EscribeError(const sMensaje: String; const iEmpleado: Integer = 0; const iLinea : Integer = 0 );
    procedure ReportaProcessDetail(const iFolio: Integer);
    //procedure ValidaEnviar ( DataSet : TZetaClientDataSet );
    procedure cdsEmpleadoBeforePost(DataSet: TDataSet);
    procedure cdsEmpleadoAlCrearCampos(Sender: TObject);
    procedure CB_SEGSOCValidate(Sender: TField);
    procedure cdsEmpleadoReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure TPO_OPERAGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure BOOLEANOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure GeneraArchivoEmpleados;
    procedure GeneraArchivoAlarmas;
  public
    { Public declarations }
    procedure HandleProcessEnd(const iIndice, iValor: Integer);
    procedure Procesa;
    function ObtenRutaReporte( iReporte: integer ): string;
    property NumeroReporte: integer read FNumeroReporte write FNumeroReporte;
    property IniValues: TTArchivosIni read FIniValues;
    property Filtro: string read FFiltro write FFiltro;
    property TipoMovimiento: eTipoMovimiento read FTipoMovimiento write FTipoMovimiento;
    property ArchivoReporte: string read FArchivoReporte write FArchivoReporte;
    property Reloj: string read FReloj write FReloj;
  end;

var
  dmInterfase: TdmInterfase;

implementation

uses ZetaMsgDlg,
     dProcesos,
     dCliente,
     dRecursos,
     dConsultas,
     dSistema,
     dReportes,
     FTressShell,
     ZAsciiTools,
     DSuperASCII,
     ZBaseThreads,
     ZetaCommonTools,
     dCatalogos,
     FAutoClasses,
     ZReportTools,
     ZReconcile,
     ZBaseEdicion,
     FEditAlarmas;

const
    // Constantes para archivo ini
    K_SYNEL    = 'Synel';
    K_RUTA_EMP = 'RutaEmpleado';
    K_RUTA_ALA = 'RutaAlarma';
    K_REPORTE  = 'Reporte';

{$R *.DFM}

{ TTArchivosIni }

constructor TTArchivosIni.Create;
begin
     FIniFile := TIniFile.Create( ChangeFileExt( Application.ExeName, '.INI' ) );
end;

destructor TTArchivosIni.Destroy;
begin
     FreeAndNil( FIniFile );
     inherited;
end;

function TTArchivosIni.GetRutaEmpleado: String;
begin
     Result := FiniFile.ReadString( K_SYNEL, K_RUTA_EMP, VACIO );
end;

procedure TTArchivosIni.SetRutaEmpleado(const Value: String);
begin
     FIniFile.WriteString( K_SYNEL, K_RUTA_EMP, Value );
end;

function TTArchivosIni.GetRutaAlarma: String;
begin
     Result := FiniFile.ReadString( K_SYNEL, K_RUTA_ALA, VACIO );
end;

procedure TTArchivosIni.SetRutaAlarma(const Value: String);
begin
     FIniFile.WriteString( K_SYNEL, K_RUTA_ALA, Value );
end;

function TTArchivosIni.GetReporte: String;
begin
     Result := FiniFile.ReadString( K_SYNEL, K_REPORTE, VACIO );
end;

procedure TTArchivosIni.SetReporte(const Value: String);
begin
     FIniFile.WriteString( K_SYNEL, K_REPORTE, Value );
end;

{ TdmInterfase }
{$ifdef DOS_CAPAS}
function TdmInterfase.GetServerAsistencia: TdmServerAsistencia;
begin
     Result := DCliente.dmCliente.ServerAsistencia;
end;
{$else}
function TdmInterfase.GetServerAsistencia: IdmServerAsistenciaDisp;
begin
     Result := IdmServerAsistenciaDisp( dmCliente.CreaServidor( CLASS_dmServerAsistencia, FServidorAsistencia ) );
end;
{$endif}

procedure TdmInterfase.DataModuleCreate(Sender: TObject);
begin
     SetNuevosEventos;
     FIniValues := TTArchivosIni.Create;
end;

// Otro tipo de m�todos
procedure TdmInterfase.SetNuevosEventos;
begin
     dmRecursos.cdsEditHisKardex.OnReconcileError := self.cdsReconcileError;

     with dmCliente.cdsEmpleado do
     begin
          BeforePost := self.cdsEmpleadoBeforePost;
          AlCrearCampos := self.cdsEmpleadoAlCrearCampos;
          OnReconcileError := self.cdsEmpleadoReconcileError;
     end;
end;

procedure TdmInterfase.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FIniValues );
end;

// Manejo de Fin de Proceso - Bit�cora
procedure TdmInterfase.HandleProcessEnd( const iIndice, iValor: Integer );
var
     oProcessData: TProcessInfo;
     sMensaje: string;

     function GetResultadoProceso: String;
     begin
          case oProcessData.Status of
               epsCancelado: Result := '******* Proceso Cancelado *******';
          else
               Result := ZetaCommonClasses.VACIO;
          end;
     end;
begin
     oProcessData := TProcessInfo.Create( nil );
     try
        with dmProcesos do
        begin
             with FListaProcesos do
             begin
                  try
                     with LockList do
                     begin
                          if ( iIndice >= 0 ) and ( iIndice < Count ) then
                             oProcessData.Assign( TProcessInfo( Items[ iIndice ] ) )
                          else
                              raise Exception.Create( 'Indice De Lista De Procesos Fuera De Rango ( ' + IntToStr( iIndice ) + ' )' );
                     end;
                  finally
                         UnlockList;
                  end;
             end;
             ReportaProcessDetail( oProcessData.Folio );
        end;
        sMensaje := GetResultadoProceso;
        if strLleno( sMensaje ) then
           EscribeBitacora( sMensaje );
     finally
            FreeAndNil( oProcessData );
            FProcesandoThread := FALSE;
     end;
end;

function TdmInterfase.GetHeaderTexto( const iEmpleado: Integer; const iLinea : Integer = 0 ): String;
const
     K_HEADER_EMPLEADO = '%sEmpleado No. %d: ';
var
     sFechaHora : String;
begin
     Result := VACIO;
     sFechaHora := '(' + FechaCorta ( date ) + '-' + FormatDateTime ( 'hh:mm', Now ) + '): ';
     if ( iLinea > 0 ) then
          sFechaHora := sFechaHora + Format ( 'L�nea #%d, ', [iLinea] );

     if ( iEmpleado > 0 ) then
          Result := Format ( K_HEADER_EMPLEADO, [sFechaHora, iEmpleado] )
     else
          Result := sFechaHora;
end;

procedure TdmInterfase.EscribeBitacora(const sMensaje: String; const iEmpleado: Integer = 0 );
begin
     if ( not FHuboCambios ) and ( iEmpleado > 0 ) then
        FHuboCambios := TRUE;

     TressShell.EscribeBitacora( GetHeaderTexto( iEmpleado ) + sMensaje );

     Application.ProcessMessages;
end;

procedure TdmInterfase.EscribeError(const sMensaje: String; const iEmpleado: Integer = 0; const iLinea : Integer = 0 );
begin
     TressShell.EscribeError( GetHeaderTexto( iEmpleado, iLinea ) + sMensaje );
     FHuboErrores := TRUE;
     Application.ProcessMessages;
end;

procedure TdmInterfase.ReportaProcessDetail( const iFolio: Integer );
const
     K_MENSAJE_BIT = 'Fecha Movimiento: %s - %s';
var
   sMensaje: String;
begin
     with dmConsultas do
     begin
          GetProcessLog( iFolio );
          with cdsLogDetail do
          begin
               if ( not IsEmpty ) then
               begin
                    First;
                    while ( not EOF ) do
                    begin
                         sMensaje := Format( K_MENSAJE_BIT, [ ZetaCommonTools.FechaCorta( FieldByName( 'BI_FEC_MOV' ).AsDateTime ),
                                                              FieldByName( 'BI_TEXTO' ).AsString ] );
                         case eTipoBitacora( FieldByName( 'BI_TIPO' ).AsInteger ) of
                              tbNormal, tbAdvertencia : EscribeBitacora( sMensaje, FieldByName( 'CB_CODIGO' ).AsInteger );
                              tbError, tbErrorGrave : EscribeError( sMensaje, FieldByName( 'CB_CODIGO' ).AsInteger );
                         end;
                         Next;
                    end;
               end;
          end;
     end;
end;

// M�todos del procesamiento del archivo de importaci�n
procedure TdmInterfase.Procesa;
begin
     FHuboErrores := FALSE;
     FHuboCambios := FALSE;
     FProcesandoThread := FALSE;

     case FTipoMovimiento of
          tmEmpleados: GeneraArchivoEmpleados;
          tmAlarmas: GeneraArchivoAlarmas;
     end;

     if FHuboErrores then
        EscribeBitacora( '**** Termin� proceso con errores ****' )
     else
         EscribeBitacora( '**** Termin� proceso ****' );
end;

procedure TdmInterfase.cdsEmpleadoBeforePost(DataSet: TDataSet);
begin
     dmCliente.cdsEmpleadoBeforePost( DataSet );
end;

procedure TdmInterfase.cdsEmpleadoAlCrearCampos(Sender: TObject);
begin
     dmCliente.cdsEmpleadoAlCrearCampos( Sender );
     dmCliente.cdsEmpleado.FieldByName( 'CB_SEGSOC' ).onValidate := self.CB_SEGSOCValidate;
end;

procedure TdmInterfase.CB_SEGSOCValidate(Sender: TField);
var
   sMensaje: String;
begin
     if not dmCliente.ValidaSEGSOC( Sender.AsString, sMensaje, FALSE ) or strLleno( sMensaje ) then
        EscribeError( sMensaje, 0, 0 );
end;

procedure TdmInterfase.cdsEmpleadoReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     if PK_Violation( E ) and
        ( UpdateKind = ukInsert ) then // Solo en la Alta del Empleado //
        EscribeError( 'No se pudo registrar el Alta, el n�mero de empleado ya existe', 0, 0 )
     else
        EscribeError( E.Message, 0 );
     Action := raCancel;
end;

{$ifdef VER130}
procedure TdmInterfase.cdsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$else}
procedure TdmInterfase.cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$endif}

{procedure TdmInterfase.ValidaEnviar ( DataSet : TZetaClientDataSet );
begin
     try
          DataSet.Enviar;
     except
          on Error : Exception do
          begin
               FHuboErrores := True;
               EscribeError ( Error.Message, 0, 0 );
               DataSet.CancelUpdates;
          end;
     end;
end;}

procedure TdmInterfase.BOOLEANOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if( Sender.AsString = K_GLOBAL_SI )then
         Text := K_PROPPERCASE_SI
     else if( Sender.AsString = K_GLOBAL_NO )then
          Text := K_PROPPERCASE_NO
     else
         Text := VACIO;
end;

procedure TdmInterfase.TPO_OPERAGetText(Sender: TField; var Text: String; DisplayText: Boolean);
const
     K_ABIERTO = 'R';
     K_CERRADO = 'S';
     K_PULSO   = 'P';
     K_TXT_ABIERTO = 'Abierto';
     K_TXT_CERRADO = 'Cerrado';
     K_TXT_PULSO   = 'Pulso';
     K_GUION       = ' - ';
begin
     if( Sender.AsString = K_ABIERTO )then
         Text := Sender.AsString + K_GUION + K_TXT_ABIERTO
     else if( Sender.AsString = K_CERRADO )then
          Text := Sender.AsString + K_GUION + K_TXT_CERRADO
     else if( Sender.AsString = K_PULSO )then
          Text := Sender.AsString + K_GUION + K_TXT_PULSO
     else
         Text := VACIO;
end;

procedure TdmInterfase.cdsAlarmasAlAdquirirDatos(Sender: TObject);
begin
     TZetaClientDataSet( Sender ).Data := ServerAsistencia.GetAlarmasSynel( dmCliente.Empresa, FFiltro );
end;

procedure TdmInterfase.cdsAlarmasAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with TZetaClientDataSet( Sender ) do
     begin
          //if ( ChangeCount > 0 ) then
          //begin
               Reconcile( ServerAsistencia.GrabaAlarmaSynel( dmCliente.Empresa, Delta, ErrorCount ) );
          //end;
     end;
end;

procedure TdmInterfase.cdsAlarmasAlModificar(Sender: TObject);
begin
     ZBaseEdicion.ShowFormaEdicion( EditAlarmas, TEditAlarmas );
end;

procedure TdmInterfase.cdsAlarmasAlCrearCampos(Sender: TObject);
begin
     with TZetaClientDataSet( Sender ) do
     begin
          MaskTime( 'HORA' );
          FieldByName( 'LUNES' ).OnGetText := BOOLEANOGetText;
          FieldByName( 'MARTES' ).OnGetText := BOOLEANOGetText;
          FieldByName( 'MIERCOLES' ).OnGetText := BOOLEANOGetText;
          FieldByName( 'JUEVES' ).OnGetText := BOOLEANOGetText;
          FieldByName( 'VIERNES' ).OnGetText := BOOLEANOGetText;
          FieldByName( 'SABADO' ).OnGetText := BOOLEANOGetText;
          FieldByName( 'DOMINGO' ).OnGetText := BOOLEANOGetText;
          FieldByName( 'TPO_OPERA' ).OnGetText := TPO_OPERAGetText;
     end;
end;

procedure TdmInterfase.cdsAlarmasBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          if StrVacio( FieldByName( 'RELOJ' ).AsString )then
          begin
               DataBaseError( 'Se debe especificar el Reloj' );
               FieldByName( 'RELOJ' ).FocusControl;
          end
          else if StrVacio( FieldByName( 'HORA' ).AsString )then
          begin
               DataBaseError( 'Se debe especificar la Hora' );
               FieldByName( 'HORA' ).FocusControl;
          end
          else if Not( FieldByName( 'RELAY' ).AsInteger in [ 1, 2 ] )then
          begin
               DataBaseError( 'El valor del Relay debe ser 1 � 2' );
               FieldByName( 'RELAY' ).FocusControl;
          end
          else if not( ( FieldByName( 'LUNES' ).AsString = K_GLOBAL_SI )or
                       ( FieldByName( 'MARTES' ).AsString = K_GLOBAL_SI )or
                       ( FieldByName( 'MIERCOLES' ).AsString = K_GLOBAL_SI )or
                       ( FieldByName( 'JUEVES' ).AsString = K_GLOBAL_SI )or
                       ( FieldByName( 'VIERNES' ).AsString = K_GLOBAL_SI )or
                       ( FieldByName( 'SABADO' ).AsString = K_GLOBAL_SI )or
                       ( FieldByName( 'DOMINGO' ).AsString = K_GLOBAL_SI ) )then
          begin
               DataBaseError( 'Es necesario especificar por lo menos un d�a de la semana' );
          end;
     end;
end;

procedure TdmInterfase.cdsAlarmasNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName( 'HORA' ).AsString := '0000';
          FieldByName( 'Relay' ).AsInteger := 1;
     end;
end;

procedure TdmInterfase.GeneraArchivoEmpleados;
var
   i, j, iLength, iContador, iTotalCaracteres: integer;
   sNombre, sNumero: string;
   cLetraPtr: PChar;
const
     K_CABECERA01 = 'Employee_______';
     K_CABECERA02 = 'V';
     K_CABECERA03 = '100';
     K_CABECERA04 = '�1';
     K_CABECERA05 = '00000'; // cantidad de caracteres sin contar rengl�n 1 y 4.
     K_CABECERA06 = 'A';
     K_CABECERA07 = '23';
     K_CABECERA08 = '27';
     K_CABECERA09 = '000'; // cantidad de registros
     K_CABECERA10 = '11';
     K_CABECERA11 = '00';
     K_CABECERA12 = '02';

     K_CERO       = '0';
     K_ESPACIO    = ' ';
     K_SYNEL_EMP  = 'vld100.rdy';
     K_DIAGONAL   = '\';
begin
     EscribeBitacora( 'Buscando archivo de empleados' );
     if DirectoryExists( FIniValues.GetRutaEmpleado )then
     begin
          if FileExists( FArchivoReporte )then
          begin
               EscribeBitacora( Format( 'Archivo de empleados encontrado [%s]', [ FArchivoReporte ] ) );
               try
                  FArchivo := TStringList.Create;
                  FRenglon := TStringList.Create;
                  FArchivo.LoadFromFile( FArchivoReporte );

                  // Leyendo
                  EscribeBitacora( 'Leyendo lista de empleados' );
                  if( FArchivo.Count > 0 )then
                  begin
                       EscribeBitacora( 'Preparando nueva lista de empleados' );
                       FArchivoFinal := TStringList.Create;
                       try
                          // Agrega valores constantes.
                          FArchivoFinal.Add( K_CABECERA01 );
                          FArchivoFinal.Add( K_CABECERA02 );
                          FArchivoFinal.Add( K_CABECERA03 );
                          FArchivoFinal.Add( K_CABECERA04 );
                          FArchivoFinal.Add( K_CABECERA05 ); // # caracteres
                          FArchivoFinal.Add( K_CABECERA06 );
                          FArchivoFinal.Add( K_CABECERA07 );
                          FArchivoFinal.Add( K_CABECERA08 );
                          FArchivoFinal.Add( K_CABECERA09 ); // # registros
                          FArchivoFinal.Add( K_CABECERA10 );
                          FArchivoFinal.Add( K_CABECERA11 );
                          FArchivoFinal.Add( K_CABECERA12 );

                          iContador := 0;
                          iTotalCaracteres := 0;

                          for i := 0 to ( FArchivo.Count - 1 )do
                          begin
                               FRenglon.CommaText := FArchivo[ i ];

                               if( FRenglon.Count = 2 )then
                               begin
                                    sNumero := Copy( Trim( FRenglon[ 0 ] ), 1, 11);
                                    sNombre := Copy( AnsiUpperCase( Trim( FRenglon[ 1 ] ) ), 1, 16 );

                                    // Quitar caracteres especiales del nombre (�, acentos, comas )
                                    sNombre := StringReplace( sNombre, '�', 'N', [ rfReplaceAll, rfIgnoreCase ] );
                                    sNombre := StringReplace( sNombre, '�', 'A', [ rfReplaceAll, rfIgnoreCase ] );
                                    sNombre := StringReplace( sNombre, '�', 'E', [ rfReplaceAll, rfIgnoreCase ] );
                                    sNombre := StringReplace( sNombre, '�', 'Y', [ rfReplaceAll, rfIgnoreCase ] );
                                    sNombre := StringReplace( sNombre, '�', 'O', [ rfReplaceAll, rfIgnoreCase ] );
                                    sNombre := StringReplace( sNombre, '�', 'U', [ rfReplaceAll, rfIgnoreCase ] );

                                    sNombre := StringReplace( sNombre, ',', ' ', [ rfReplaceAll, rfIgnoreCase ] );

                                    // Solo dejar de la Aa..Zz y 0..9, el resto sustituir por ' ';
                                    iLength := Length( Trim( sNombre ) );
                                    cLetraPtr := PChar( Trim( sNombre ) );
                                    for j := 0 to iLength - 1 do
                                    begin
                                         if ( cLetraPtr[ j ] in [ 'A'..'Z', 'a'..'z', '0'..'9' ] )then
                                            cLetraPtr[ j ] := cLetraPtr[ j ]
                                         else
                                             cLetraPtr[ j ] := K_ESPACIO;
                                    end;

                                    sNombre := cLetraPtr;
                                    //sNombre := Trim( sNombre );

                                    if( StrLleno( sNumero )and( StrLleno( sNombre ) ) )then
                                    begin
                                         FArchivoFinal.Add( PadLCar( Format( '%s', [ sNumero ] ), 11, K_ESPACIO ) );
                                         iTotalCaracteres := iTotalCaracteres + 11;
                                         FArchivoFinal.Add( PadRCar( sNombre, 16, K_ESPACIO ) );
                                         iTotalCaracteres := iTotalCaracteres + 16;

                                         Inc( iContador );
                                    end;
                               end;
                          end;

                          if( iContador > 0 )then
                          begin
                               iTotalCaracteres := iTotalCaracteres + 15 + Length( PadLCar( Format( '%d', [ iContador ] ), 3, K_CERO ) );
                               iTotalCaracteres := iTotalCaracteres + Length( PadLCar( Format( '%d', [ iTotalCaracteres ] ), 5, K_CERO ) );
                               FArchivoFinal[ 4 ] := PadLCar( Format( '%d', [ iTotalCaracteres  ] ), 5, K_CERO );

                               FArchivoFinal[ 8 ] := PadLCar( Format( '%d', [ iContador ] ), 3, K_CERO );

                               if FileExists( FIniValues.GetRutaEmpleado + K_DIAGONAL + K_SYNEL_EMP )then
                                  DeleteFile( FIniValues.GetRutaEmpleado + K_DIAGONAL + K_SYNEL_EMP );

                               EscribeBitacora( Format( 'Escribiendo nueva lista de empleados [%s]', [ FIniValues.GetRutaEmpleado + K_DIAGONAL+ K_SYNEL_EMP ] ) );
                               ZAsciiTools.GrabaBitacora( FArchivoFinal, ( FIniValues.GetRutaEmpleado + K_DIAGONAL+ K_SYNEL_EMP ) );
                          end;
                       finally
                              FreeAndNil( FArchivoFinal );
                       end;
                  end;
               finally
                      FreeAndNil( FRenglon );
                      FreeAndNil( FArchivo );
               end;
          end
          else
              EscribeError( Format( 'Archivo de empleados NO encontrado [%s]', [ FArchivoReporte ] ) );
     end
     else
         EscribeError( Format( 'No se encontr� la ruta destino [%s]', [ FIniValues.GetRutaEmpleado ] ) );
end;

procedure TdmInterfase.GeneraArchivoAlarmas;
var
   sHora, sTipoOperacion, sRelay, sActivo,
   sInactivo, sTotal : string;
   iContador, iTotalCaracteres, iRelay, iActivo,
   iInactivo, iTotal: integer;
   lLunes, lMartes, lMiercoles, lJueves,
   lViernes, lSabado, lDomingo, lCreadoOk: boolean;
const
     K_CABECERA01 = 'Keys___________';
     K_CABECERA02 = 'e';
     K_CABECERA03 = '001';
     K_CABECERA04 = '�1';
     K_CABECERA05 = '00000'; // cantidad de caracteres sin contar rengl�n 1 y 4.
     K_CABECERA06 = 'A';
     K_CABECERA07 = '23';
     K_CABECERA08 = '23';
     K_CABECERA09 = '000'; // cantidad de registros
     K_CABECERA10 = '04';
     K_CABECERA11 = '00';
     K_CABECERA12 = '02';
     K_REGISTRO   = '%s%s%s%s%s%s00000O%d';

     K_CERO       = '0';
     K_ESPACIO    = ' ';
     K_SYNEL_ALA  = 'fts001.rdy';
     K_DIAGONAL   = '\';
begin
     EscribeBitacora( 'Leyendo configuraci�n de alarmas' );

     if StrLleno( FReloj )then
     begin
          if DirectoryExists( FIniValues.GetRutaAlarma )then
          begin
               iContador := 0;
               iTotalCaracteres := 0;
               FArchivoFinal := TStringList.Create;
               try
                  FArchivoFinal.Add( K_CABECERA01 );
                  FArchivoFinal.Add( K_CABECERA02 );
                  FArchivoFinal.Add( K_CABECERA03 );
                  FArchivoFinal.Add( K_CABECERA04 );
                  FArchivoFinal.Add( K_CABECERA05 );
                  FArchivoFinal.Add( K_CABECERA06 );
                  FArchivoFinal.Add( K_CABECERA07 );
                  FArchivoFinal.Add( K_CABECERA08 );
                  FArchivoFinal.Add( K_CABECERA09 );
                  FArchivoFinal.Add( K_CABECERA10 );
                  FArchivoFinal.Add( K_CABECERA11 );
                  FArchivoFinal.Add( K_CABECERA12 );
                  with cdsAlarmas do
                  begin
                       Filter := Format( '( RELOJ=''%s'' )', [ FReloj ] );
                       Filtered := TRUE;
                       Conectar;
                       First;

                       EscribeBitacora( 'Preparando lista de alarmas' );
                       if not IsEmpty then
                       begin
                            while not EoF do
                            begin
                                 iRelay := ( FieldByName( 'RELAY' ).AsInteger ); // Conversi�n a 200 ms x unidad, o sea, 1 segundo equivale a 5 ms.
                                 iActivo := ( FieldByName( 'ACTIVO' ).AsInteger * 5 );
                                 iInactivo := ( FieldByName( 'INACTIVO' ).AsInteger * 5 );
                                 iTotal := ( FieldByName( 'TOTAL' ).AsInteger * 5 );

                                 sHora := FieldByName( 'HORA' ).AsString;
                                 sTipoOperacion := Copy( FieldByName( 'TPO_OPERA' ).AsString, 1, 1 );
                                 sRelay := PadLCar( Format( '%d', [ iRelay ] ), 2, K_CERO );
                                 sActivo := PadLCar( Format( '%d', [ iActivo ] ), 3, K_CERO );
                                 sInactivo := PadLCar( Format( '%d', [ iInactivo ] ), 3, K_CERO );
                                 sTotal := PadLCar( Format( '%d', [ iTotal ] ), 3, K_CERO );
                                 lLunes := zStrToBool( FieldByName( 'LUNES' ).AsString );
                                 lMartes := zStrToBool( FieldByName( 'MARTES' ).AsString );
                                 lMiercoles := zStrToBool( FieldByName( 'MIERCOLES' ).AsString );
                                 lJueves := zStrToBool( FieldByName( 'JUEVES' ).AsString );
                                 lViernes := zStrToBool( FieldByName( 'VIERNES' ).AsString );
                                 lSabado := zStrToBool( FieldByName( 'SABADO' ).AsString );
                                 lDomingo := zStrToBool( FieldByName( 'DOMINGO' ).AsString );

                                 if( lLunes and lMartes and lMiercoles and lJueves and lViernes and
                                     lSabado and lDomingo )then
                                 begin
                                      FArchivoFinal.Add( Format( K_REGISTRO, [ sHora,
                                                                               sTipoOperacion,
                                                                               sRelay,
                                                                               sActivo,
                                                                               sInactivo,
                                                                               sTotal,
                                                                               0 ] ) );
                                      iTotalCaracteres := iTotalCaracteres + 23;
                                      Inc( iContador );
                                 end
                                 else
                                 begin
                                      if lLunes then
                                      begin
                                           FArchivoFinal.Add( Format( K_REGISTRO, [ sHora,
                                                                                    sTipoOperacion,
                                                                                    sRelay,
                                                                                    sActivo,
                                                                                    sInactivo,
                                                                                    sTotal,
                                                                                    2 ] ) );
                                           iTotalCaracteres := iTotalCaracteres + 23;
                                           Inc( iContador );
                                      end;

                                      if lMartes then
                                      begin
                                           FArchivoFinal.Add( Format( K_REGISTRO, [ sHora,
                                                                                    sTipoOperacion,
                                                                                    sRelay,
                                                                                    sActivo,
                                                                                    sInactivo,
                                                                                    sTotal,
                                                                                    3 ] ) );
                                           iTotalCaracteres := iTotalCaracteres + 23;
                                           Inc( iContador );
                                      end;

                                      if lMiercoles then
                                      begin
                                           FArchivoFinal.Add( Format( K_REGISTRO, [ sHora,
                                                                                    sTipoOperacion,
                                                                                    sRelay,
                                                                                    sActivo,
                                                                                    sInactivo,
                                                                                    sTotal,
                                                                                    4 ] ) );
                                           iTotalCaracteres := iTotalCaracteres + 23;
                                           Inc( iContador );
                                      end;

                                      if lJueves then
                                      begin
                                           FArchivoFinal.Add( Format( K_REGISTRO, [ sHora,
                                                                                    sTipoOperacion,
                                                                                    sRelay,
                                                                                    sActivo,
                                                                                    sInactivo,
                                                                                    sTotal,
                                                                                    5 ] ) );
                                           iTotalCaracteres := iTotalCaracteres + 23;
                                           Inc( iContador );
                                      end;

                                      if lViernes then
                                      begin
                                           FArchivoFinal.Add( Format( K_REGISTRO, [ sHora,
                                                                                    sTipoOperacion,
                                                                                    sRelay,
                                                                                    sActivo,
                                                                                    sInactivo,
                                                                                    sTotal,
                                                                                    6 ] ) );
                                           iTotalCaracteres := iTotalCaracteres + 23;
                                           Inc( iContador );
                                      end;

                                      if lSabado then
                                      begin
                                           FArchivoFinal.Add( Format( K_REGISTRO, [ sHora,
                                                                                    sTipoOperacion,
                                                                                    sRelay,
                                                                                    sActivo,
                                                                                    sInactivo,
                                                                                    sTotal,
                                                                                    7 ] ) );
                                           iTotalCaracteres := iTotalCaracteres + 23;
                                           Inc( iContador );
                                      end;

                                      if lDomingo then
                                      begin
                                           FArchivoFinal.Add( Format( K_REGISTRO, [ sHora,
                                                                                    sTipoOperacion,
                                                                                    sRelay,
                                                                                    sActivo,
                                                                                    sInactivo,
                                                                                    sTotal,
                                                                                    1 ] ) );
                                           iTotalCaracteres := iTotalCaracteres + 23;
                                           Inc( iContador );
                                      end;
                                 end;

                                 Next;
                            end;
                       end
                       else
                           EscribeError( Format( 'El reloj [%s] no se encontr� en la lista de alarmas', [ FReloj ] ) );
                       Filter := VACIO;
                       Filtered := FALSE;
                  end;
                  if( iContador > 0 )then
                  begin
                       iTotalCaracteres := iTotalCaracteres + 15 + Length( PadLCar( Format( '%d', [ iContador ] ), 3, K_CERO ) );
                       iTotalCaracteres := iTotalCaracteres + Length( PadLCar( Format( '%d', [ iTotalCaracteres ] ), 5, K_CERO ) );
                       FArchivoFinal[ 4 ] := PadLCar( Format( '%d', [ iTotalCaracteres  ] ), 5, K_CERO );

                       FArchivoFinal[ 8 ] := PadLCar( Format( '%d', [ iContador ] ), 3, K_CERO );

                       if not DirectoryExists( FIniValues.GetRutaAlarma + K_DIAGONAL + FReloj + K_DIAGONAL )then
                       begin
                            EscribeBitacora( Format( 'No se encontr� el directorio [%s]', [ FReloj ] ) );
                            lCreadoOk := CreateDir( FIniValues.GetRutaAlarma + K_DIAGONAL + FReloj + K_DIAGONAL );
                            if lCreadoOK then
                               EscribeBitacora( Format( 'El directorio [%s] fue creado con �xito', [ FReloj ] ) )
                            else
                                EscribeBitacora( Format( 'El directorio [%s] NO fue creado con �xito', [ FReloj ] ) );
                       end
                       else
                       begin
                            lCreadoOk := TRUE;
                            EscribeBitacora( Format( 'Se encontr� el directorio [%s]', [ FReloj ] ) );
                       end;

                       if lCreadoOK then
                       begin
                            if FileExists( FIniValues.GetRutaAlarma + K_DIAGONAL + FReloj + K_DIAGONAL + K_SYNEL_ALA )then
                               DeleteFile( FIniValues.GetRutaAlarma + K_DIAGONAL + FReloj + K_DIAGONAL + K_SYNEL_ALA );

                            EscribeBitacora( Format( 'Escribiendo lista de alarmas [%s]', [ FIniValues.GetRutaAlarma + K_DIAGONAL + FReloj + K_DIAGONAL + K_SYNEL_ALA ] ) );
                            ZAsciiTools.GrabaBitacora( FArchivoFinal, ( FIniValues.GetRutaAlarma + K_DIAGONAL + FReloj + K_DIAGONAL + K_SYNEL_ALA ) );
                       end
                       else
                           EscribeError( Format( 'Error al crear lista de alarmas en directorio [%s]', [ FIniValues.GetRutaAlarma + K_DIAGONAL + FReloj + K_DIAGONAL + K_SYNEL_ALA ] ) );
                  end;
               finally
                      FreeAndNil( FArchivoFinal );
               end;
          end
          else
              EscribeError( Format( 'No se encontr� la ruta destino [%s]', [ FIniValues.GetRutaAlarma ] ) );
     end
     else
         EscribeError( 'Es necesario especificar que el reloj' );
end;

function TdmInterfase.ObtenRutaReporte( iReporte: integer): string;
begin
     Result := ServerAsistencia.GetRutaReporteSynel( dmCliente.Empresa, iReporte );
end;

procedure TdmInterfase.cdsRelojAlAdquirirDatos(Sender: TObject);
begin
     TZetaClientDataSet( Sender ).Data := ServerAsistencia.GetListaReloj( dmCliente.Empresa );
end;

procedure TdmInterfase.cdsAlarmasAlBorrar(Sender: TObject);
begin
     with cdsAlarmas do
     begin
          if ZetaMsgDlg.ConfirmaCambio( '�Desea borrar este registro?' ) then
          begin
               Delete;
               Enviar;
               Refrescar;
          end;
     end;
end;

end.
