object General: TGeneral
  Left = 541
  Top = 199
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Configuraci'#243'n General'
  ClientHeight = 153
  ClientWidth = 408
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 408
    Height = 73
    Align = alTop
    Caption = ' Lista de Empleados: '
    TabOrder = 0
    DesignSize = (
      408
      73)
    object Label1: TLabel
      Left = 52
      Top = 19
      Width = 41
      Height = 13
      Caption = 'Reporte:'
    end
    object Label2: TLabel
      Left = 28
      Top = 45
      Width = 65
      Height = 13
      Alignment = taRightJustify
      BiDiMode = bdRightToLeft
      Caption = 'Ruta Destino:'
      ParentBiDiMode = False
    end
    object ArchivoSeek: TSpeedButton
      Left = 373
      Top = 40
      Width = 25
      Height = 25
      Hint = 'Buscar ruta'
      Anchors = []
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333333333333333333FFF333333333333000333333333
        3333777FFF3FFFFF33330B000300000333337F777F777773F333000E00BFBFB0
        3333777F773333F7F333000E0BFBF0003333777F7F3337773F33000E0FBFBFBF
        0333777F7F3333FF7FFF000E0BFBF0000003777F7F3337777773000E0FBFBFBF
        BFB0777F7F33FFFFFFF7000E0BF000000003777F7FF777777773000000BFB033
        33337777773FF733333333333300033333333333337773333333333333333333
        3333333333333333333333333333333333333333333333333333333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = ArchivoSeekClick
    end
    object luReporte: TZetaKeyLookup
      Left = 97
      Top = 16
      Width = 300
      Height = 21
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
    end
    object txtRutaEmpleado: TZetaEdit
      Left = 97
      Top = 42
      Width = 274
      Height = 21
      Hint = 'Ruta donde se crear'#225' el archivo'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 73
    Width = 408
    Height = 47
    Align = alTop
    Caption = ' Lista de Alarmas: '
    TabOrder = 1
    DesignSize = (
      408
      47)
    object Label3: TLabel
      Left = 28
      Top = 19
      Width = 65
      Height = 13
      Alignment = taRightJustify
      Caption = 'Ruta Destino:'
    end
    object SpeedButton1: TSpeedButton
      Left = 373
      Top = 14
      Width = 25
      Height = 25
      Hint = 'Buscar ruta'
      Anchors = []
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333333333333333333FFF333333333333000333333333
        3333777FFF3FFFFF33330B000300000333337F777F777773F333000E00BFBFB0
        3333777F773333F7F333000E0BFBF0003333777F7F3337773F33000E0FBFBFBF
        0333777F7F3333FF7FFF000E0BFBF0000003777F7F3337777773000E0FBFBFBF
        BFB0777F7F33FFFFFFF7000E0BF000000003777F7FF777777773000000BFB033
        33337777773FF733333333333300033333333333337773333333333333333333
        3333333333333333333333333333333333333333333333333333333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = SpeedButton1Click
    end
    object txtRutaAlarma: TZetaEdit
      Left = 97
      Top = 16
      Width = 274
      Height = 21
      Hint = 'Ruta donde se crear'#225' el archivo'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 120
    Width = 408
    Height = 33
    Align = alClient
    TabOrder = 2
    DesignSize = (
      408
      33)
    object BitBtn1: TBitBtn
      Left = 250
      Top = 4
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Guardar'
      TabOrder = 0
      OnClick = BitBtn1Click
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 329
      Top = 4
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Salir'
      TabOrder = 1
      Kind = bkClose
    end
  end
end
