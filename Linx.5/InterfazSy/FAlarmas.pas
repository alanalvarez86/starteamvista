unit FAlarmas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicion, Grids, DBGrids, ZetaDBGrid, StdCtrls, DB,
  ExtCtrls, DBCtrls, ZetaSmartLists, Buttons;

type
  TAlarmas = class(TBaseEdicion)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    txtReloj: TEdit;
    BitBtn1: TBitBtn;
    grAlarmas: TZetaDBGrid;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AgregarBtnClick(Sender: TObject);
    procedure BorrarBtnClick(Sender: TObject);
    procedure ModificarBtnClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure grAlarmasDblClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
//    procedure Imprimir; override;
  public
    { Public declarations }
  end;

var
  Alarmas: TAlarmas;

implementation

uses DInterfase,
     ZetaCommonTools,
     ZetaCommonClasses,
     FTressShell;

{$R *.dfm}

{ TAlarmas }

procedure TAlarmas.Agregar;
begin
     dmInterfase.cdsAlarmas.Append;
     dmInterfase.cdsAlarmas.Agregar;
end;

procedure TAlarmas.Borrar;
begin
     dmInterfase.cdsAlarmas.Borrar;
end;

procedure TAlarmas.Connect;
begin
     with dmInterfase do
     begin
          cdsAlarmas.Filter := VACIO;
          cdsAlarmas.Filtered := FALSE;
          cdsAlarmas.Conectar;
          DataSource.DataSet := cdsAlarmas;
     end;
end;

procedure TAlarmas.Modificar;
begin
     dmInterfase.cdsAlarmas.Edit;
     dmInterfase.cdsAlarmas.Modificar;
end;

procedure TAlarmas.BitBtn1Click(Sender: TObject);
const
     K_FILTRO = '';
begin
     with dmInterfase.cdsAlarmas do
     begin
          if StrLleno( txtReloj.Text)then
          begin
               Filter := '( RELOJ like ''%' + format( '%s', [ Copy( txtReloj.Text, 1, 4 ) ] ) + '%'' )';
               Filtered := TRUE;
          end
          else
          begin
               Filter := VACIO;
               Filtered := FALSE;
          end;
          //cdsAlarmas.Refrescar;
          //Filtro := VACIO;
     end;
end;

procedure TAlarmas.FormShow(Sender: TObject);
begin
     inherited;
     DBNavigator.Visible := FALSE;
     UndoBtn.Visible := FALSE;
     PegarBtn.Visible := FALSE;
     CopiarBtn.Visible := FALSE;
     CortarBtn.Visible := FALSE;
     ExportarBtn.Visible := FALSE;
     ImprimirFormaBtn.Visible := FALSE;
     ImprimirBtn.Visible := FALSE;
     BuscarBtn.Visible := FALSE;

     with dmInterfase.cdsAlarmas do
     begin
          Filter := VACIO;
          Filtered := FALSE;
     end;
     txtReloj.Text := VACIO;
end;

procedure TAlarmas.AgregarBtnClick(Sender: TObject);
begin
     dmInterfase.cdsAlarmas.Agregar;
     
end;

procedure TAlarmas.BorrarBtnClick(Sender: TObject);
begin
     dmInterfase.cdsAlarmas.Borrar;
end;

procedure TAlarmas.ModificarBtnClick(Sender: TObject);
begin
     with dmInterfase.cdsAlarmas do
     begin
          Edit;
          Modificar;
     end;
end;

procedure TAlarmas.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     //inherited;
     //if NoHayDatos then
     //   Close;
end;

procedure TAlarmas.grAlarmasDblClick(Sender: TObject);
begin
     with dmInterfase.cdsAlarmas do
     begin
          Edit;
          Modificar;
     end;
end;

procedure TAlarmas.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     TressShell.AgregarListaReloj;
end;

end.
