inherited FileManager: TFileManager
  Caption = 'Administrador De Archivos'
  ClientHeight = 254
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 218
    BevelOuter = bvNone
    inherited OK: TBitBtn
      Left = 7
      Hint = 'Borrar El Archivo Seleccionado'
      Caption = '&Borrar'
      ModalResult = 0
      OnClick = OKClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333FF33333333333330003333333333333777333333333333
        300033FFFFFF3333377739999993333333333777777F3333333F399999933333
        3300377777733333337733333333333333003333333333333377333333333333
        3333333333333333333F333333333333330033333F33333333773333C3333333
        330033337F3333333377333CC3333333333333F77FFFFFFF3FF33CCCCCCCCCC3
        993337777777777F77F33CCCCCCCCCC399333777777777737733333CC3333333
        333333377F33333333FF3333C333333330003333733333333777333333333333
        3000333333333333377733333333333333333333333333333333}
    end
    inherited Cancelar: TBitBtn
      Hint = 'Salir De Esta Pantalla'
      Caption = '&Salir'
      Kind = bkClose
    end
  end
  object ArchivosGB: TGroupBox
    Left = 0
    Top = 0
    Width = 375
    Height = 218
    Align = alClient
    Caption = ' Archivos Cargados En La Terminal '
    TabOrder = 1
    object Archivos: TListBox
      Left = 2
      Top = 15
      Width = 371
      Height = 201
      Align = alClient
      BorderStyle = bsNone
      Color = clBtnFace
      ItemHeight = 13
      TabOrder = 0
    end
  end
end
