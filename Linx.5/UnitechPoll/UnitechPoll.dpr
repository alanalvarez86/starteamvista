program UnitechPoll;

uses
  Forms,
  SysUtils,
  ZetaClientTools,
  FLinxBase in '..\LinxBase\FLinxBase.pas' {LinxBase},
  FAcercaDe in '..\LinxBase\FAcercaDe.pas' {AcercaDe},
  ZBaseDlgModal in '..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  FUnitekPoll in 'FUnitekPoll.pas' {UnitechPoller};

{$R *.RES}
{$R WindowsXP.res}

begin
     ZetaClientTools.InitDCOM;
     Application.Initialize;
     Application.Title := 'UnitechPoll';
  Application.HelpFile := 'UnitechPoll.chm';
  Application.CreateForm(TUnitechPoller, UnitechPoller);
  case ParamCount of
          1:
          begin
               UnitechPoller.ProcesaParametro( UpperCase( Trim( ParamStr( 1 ) ) ) );
               FreeAndNil( UnitechPoller );
          end;
          2:
          begin
               UnitechPoller.ProcesaParametro( UpperCase( Trim( ParamStr( 1 ) ) ), ParamStr( 2 ) );
               FreeAndNil( UnitechPoller );
          end;
     end;
     Application.Run;
end.
