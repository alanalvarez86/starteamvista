unit FTerminales;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Mask, Buttons, ExtCtrls,
     LinxIni,
     ZBaseDlgModal,
     ZetaNumero;

type
  TCapturarTerminales = class(TZetaDlgModal)
    PanelControles: TPanel;
    PanelLista: TPanel;
    TerminalList: TListBox;
    Agregar: TBitBtn;
    Borrar: TBitBtn;
    Modificar: TBitBtn;
    TcpIpGB: TGroupBox;
    TcpIpLBL: TLabel;
    TCPIP1: TZetaNumero;
    Punto1: TLabel;
    TCPIP2: TZetaNumero;
    Label1: TLabel;
    TCPIP3: TZetaNumero;
    Label2: TLabel;
    TCPIP4: TZetaNumero;
    PuertoTCPLBL: TLabel;
    PuertoTCP: TZetaNumero;
    PanelGeneral: TPanel;
    DescriptionLBL: TLabel;
    Description: TEdit;
    Identificador: TEdit;
    IdentificadorLBL: TLabel;
    LetraLBL: TLabel;
    Letra: TComboBox;
    Tipo: TComboBox;
    TipoLBL: TLabel;
    SerialGB: TGroupBox;
    SerialPortLBL: TLabel;
    BaudsLBL: TLabel;
    Bauds: TComboBox;
    SerialPort: TComboBox;
    StopBitsLBL: TLabel;
    ParidadLBL: TLabel;
    Paridad: TComboBox;
    StopBits: TComboBox;
    TimeOutLBL: TLabel;
    Timeout: TZetaNumero;
    TimeOutUnitLBL: TLabel;
    OpenDialog: TOpenDialog;
    AlarmaLBL: TLabel;
    EmpleadosLBL: TLabel;
    Empleados: TEdit;
    Alarma: TEdit;
    BuscarAlarma: TSpeedButton;
    BuscarEmpleados: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure AgregarClick(Sender: TObject);
    procedure BorrarClick(Sender: TObject);
    procedure ModificarClick(Sender: TObject);
    procedure TerminalListClick(Sender: TObject);
    procedure HayCambios(Sender: TObject);
    procedure HayCambiosDescripcion(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure IdentificadorEnter(Sender: TObject);
    procedure TCPIP1Exit(Sender: TObject);
    procedure TCPIP2Exit(Sender: TObject);
    procedure TCPIP3Exit(Sender: TObject);
    procedure TCPIP4Exit(Sender: TObject);
    procedure PuertoTCPExit(Sender: TObject);
    procedure BuscarAlarmaClick(Sender: TObject);
    procedure BuscarEmpleadosClick(Sender: TObject);
  private
    { Private declarations }
    FRefreshing: Boolean;
    function BuscaArchivo(const sFileName, sFilter, sTitle: String): String;
    function VerificaDatos( Lista: TStrings ): Boolean;
    procedure EnableControls( const lTcpIP, lSerial: Boolean );
    procedure LimpiarLista;
    procedure SeleccionaPrimerTerminal;
    procedure SetControls;
    procedure ShowTerminalInfo;
    procedure ValidaTCPIP(Sender: TZetaNumero);
  public
    { Public declarations }
  end;

var
  CapturarTerminales: TCapturarTerminales;

implementation

uses ZetaDialogo;

{$R *.DFM}

procedure TCapturarTerminales.FormCreate(Sender: TObject);
begin
     inherited;
     FRefreshing := False;
end;

procedure TCapturarTerminales.FormShow(Sender: TObject);
begin
     inherited;
     with LinxIni.IniValues do
     begin
          ListaTerminales.Cargar( TerminalList.Items );
     end;
     SeleccionaPrimerTerminal;
     ShowTerminalInfo;
     SetControls;
end;

procedure TCapturarTerminales.FormDestroy(Sender: TObject);
begin
     inherited;
     LimpiarLista;
end;

procedure TCapturarTerminales.LimpiarLista;
begin
     with TerminalList do
     begin
          LinxIni.IniValues.ListaTerminales.Limpiar( Items );
     end;
end;

procedure TCapturarTerminales.SetControls;
begin
     Borrar.Enabled := TerminalList.Items.Count > 0;
     Modificar.Enabled := Borrar.Enabled;
end;

function TCapturarTerminales.BuscaArchivo( const sFileName, sFilter, sTitle: String ): String;
begin
     with OpenDialog do
     begin
          Title := sTitle;
          FileName := sFileName;
          InitialDir := ExtractFilePath( sFileName );
          Filter := sFilter;
          FilterIndex := 0;
          if Execute then
             Result := FileName
          else
              Result := sFileName;
     end;
end;

procedure TCapturarTerminales.SeleccionaPrimerTerminal;
begin
     with TerminalList do
     begin
          if ( Items.Count > 0 ) then
             ItemIndex := 0;
     end;
end;

procedure TCapturarTerminales.EnableControls( const lTcpIP, lSerial: Boolean );
begin
     Self.DescriptionLBL.Enabled := ( lTcpIP or lSerial );
     Self.Description.Enabled := ( lTcpIP or lSerial );
     Self.IdentificadorLBL.Enabled := ( lTcpIP or lSerial );
     Self.Identificador.Enabled := ( lTcpIP or lSerial );
     Self.LetraLBL.Enabled := ( lTcpIP or lSerial );
     Self.Letra.Enabled := ( lTcpIP or lSerial );
     Self.Alarma.Enabled := ( lTcpIP or lSerial );
     Self.Empleados.Enabled := ( lTcpIP or lSerial );
     Self.TipoLBL.Enabled := ( lTcpIP or lSerial );
     Self.Tipo.Enabled := ( lTcpIP or lSerial );
     Self.TcpIpLBL.Enabled := lTcpIp;
     Self.TCPIP1.Enabled := lTcpIp;
     Self.TCPIP2.Enabled := lTcpIp;
     Self.TCPIP3.Enabled := lTcpIp;
     Self.TCPIP4.Enabled := lTcpIp;
     Self.PuertoTCPLBL.Enabled := lTcpIp;
     Self.PuertoTCP.Enabled := lTcpIp;
     Self.TimeOutLBL.Enabled := lTcpIp;
     Self.TimeOutUnitLBL.Enabled := lTcpIp;
     Self.TimeOut.Enabled := lTcpIp;
     Self.TcpIpGB.Enabled := lTcpIp;
     Self.SerialPortLBL.Enabled := lSerial;
     Self.SerialPort.Enabled := lSerial;
     Self.BaudsLBL.Enabled := lSerial;
     Self.Bauds.Enabled := lSerial;
     Self.StopBitsLBL.Enabled := lSerial;
     Self.StopBits.Enabled := lSerial;
     Self.ParidadLBL.Enabled := lSerial;
     Self.Paridad.Enabled := lSerial;
     Self.SerialGB.Enabled := lSerial;
end;

procedure TCapturarTerminales.ShowTerminalInfo;
var
   i: Integer;
begin
     with TerminalList do
     begin
          i := ItemIndex;
          if ( i < 0 ) then
          begin
               EnableControls( False, False );
          end
          else
          begin
               FRefreshing := True;
               with TTerminal( Items.Objects[ i ] ) do
               begin
                    Self.Description.Text := Description;
                    Self.Identificador.Text := Identificador;
                    Self.Letra.ItemIndex := Self.Letra.Items.IndexOf( Letter );
                    Self.Tipo.ItemIndex := Ord( Tipo );
                    Self.Alarma.Text := Alarma;
                    Self.Empleados.Text := Empleados;
                    Self.TCPIP1.Valor := TCPAddress1;
                    Self.TCPIP2.Valor := TCPAddress2;
                    Self.TCPIP3.Valor := TCPAddress3;
                    Self.TCPIP4.Valor := TCPAddress4;
                    Self.PuertoTCP.Valor := TCPPort;
                    Self.TimeOut.Valor := TCPTimeOut;
                    Self.SerialPort.ItemIndex := SerialPort - 1;
                    Self.Bauds.ItemIndex := Ord( BaudRate );
                    Self.StopBits.ItemIndex := Ord( StopBits );
                    Self.Paridad.ItemIndex := Ord( Parity );
                    case Tipo of
                         ttTCPIP: EnableControls( True, False );
                         ttSerial: EnableControls( False, True );
                    end;
               end;
               FRefreshing := False;
          end;
     end;
end;

procedure TCapturarTerminales.HayCambios(Sender: TObject);
begin
     if not FRefreshing then
     begin
          with TerminalList do
          begin
               if ( ItemIndex >= 0 ) then
               begin
                    with TTerminal( Items.Objects[ ItemIndex ] ) do
                    begin
                         Description := Self.Description.Text;
                         Identificador := Format( '%4.4s', [ Trim( Self.Identificador.Text ) ] );
                         Letter := Self.Letra.Items[ Self.Letra.ItemIndex ];
                         Tipo := eTerminalType( Self.Tipo.ItemIndex );
                         Alarma := Self.Alarma.Text;
                         Empleados := Self.Empleados.Text;
                         TCPAddress1 := Self.TCPIP1.ValorEntero;
                         TCPAddress2 := Self.TCPIP2.ValorEntero;
                         TCPAddress3 := Self.TCPIP3.ValorEntero;
                         TCPAddress4 := Self.TCPIP4.ValorEntero;
                         TCPPort := Self.PuertoTCP.ValorEntero;
                         TCPTimeOut := Self.TimeOut.ValorEntero;
                         SerialPort := Self.SerialPort.ItemIndex + 1;
                         BaudRate := eBaudRate( Self.Bauds.ItemIndex );
                         StopBits := eStopBits( Self.StopBits.ItemIndex );
                         Parity := eParity( Self.Paridad.ItemIndex );
                         case Tipo of
                              ttTCPIP: EnableControls( True, False );
                              ttSerial: EnableControls( False, True );
                         end;
                    end;
               end;
          end;
     end;
end;

procedure TCapturarTerminales.HayCambiosDescripcion(Sender: TObject);
begin
     HayCambios( Sender );
     if not FRefreshing then
     begin
          with TerminalList do
          begin
               if ( ItemIndex >= 0 ) then
               begin
                    Items.Strings[ ItemIndex ] := Description.Text;
               end;
          end;
     end;
end;

procedure TCapturarTerminales.IdentificadorEnter(Sender: TObject);
begin
     inherited;
     Identificador.SelectAll;
end;

procedure TCapturarTerminales.ValidaTCPIP(Sender: TZetaNumero);
begin
     if ( Sender.ValorEntero < 0 ) or ( Sender.ValorEntero > 255 ) then
     begin
          ZetaDialogo.zError( '� Direcci�n Inv�lida !', 'Valor Debe Estar Entre 0 y 255', 0 );
          ActiveControl := Sender;
     end;
end;

procedure TCapturarTerminales.TCPIP1Exit(Sender: TObject);
begin
     inherited;
     ValidaTCPIP( TCPIP1 );
end;

procedure TCapturarTerminales.TCPIP2Exit(Sender: TObject);
begin
     inherited;
     ValidaTCPIP( TCPIP2 );
end;

procedure TCapturarTerminales.TCPIP3Exit(Sender: TObject);
begin
     inherited;
     ValidaTCPIP( TCPIP3 );
end;

procedure TCapturarTerminales.TCPIP4Exit(Sender: TObject);
begin
     inherited;
     ValidaTCPIP( TCPIP4 );
end;

procedure TCapturarTerminales.PuertoTCPExit(Sender: TObject);
begin
     inherited;
     if ( PuertoTCP.ValorEntero < 0 ) or ( PuertoTCP.ValorEntero > 65535 ) then
     begin
          ZetaDialogo.zError( '� Puerto Inv�lida !', 'Puerto TCP Debe Estar Entre 0 y 65,535', 0 );
          ActiveControl := PuertoTCP;
     end;
end;

procedure TCapturarTerminales.TerminalListClick(Sender: TObject);
begin
     inherited;
     ShowTerminalInfo;
end;

procedure TCapturarTerminales.AgregarClick(Sender: TObject);
begin
     inherited;
     with TerminalList do
     begin
          Items.AddObject( 'TerminalNueva', TTerminal.Create );
          ItemIndex := ( Items.Count - 1 );
     end;
     SetControls;
     Modificar.Click;
end;

procedure TCapturarTerminales.BorrarClick(Sender: TObject);
var
   i: Integer;
begin
     inherited;
     with TerminalList do
     begin
          i := ItemIndex;
          if ( i >= 0 ) then
          begin
               with Items do
               begin
                    TTerminal( Objects[ i ] ).Free;
                    Delete( i );
                    if ( i >= Count ) then
                       i := ( i - 1 );
               end;
               ItemIndex := i;
               ShowTerminalInfo;
               SetControls;
          end;
     end;
end;

procedure TCapturarTerminales.ModificarClick(Sender: TObject);
begin
     inherited;
     ShowTerminalInfo;
     with Description do
     begin
          if CanFocus and Visible then
             SetFocus;
     end;
end;

function TCapturarTerminales.VerificaDatos( Lista: TStrings ): Boolean;
var
   i, j: Integer;
   sMensaje: String;
   FTerminal: TTerminal;
begin
     Result := True;
     sMensaje := '';
     for i := 0 to ( Lista.Count - 1 ) do
     begin
          FTerminal := TTerminal( Lista.Objects[ i ] );
          if ( ( Lista.Count ) > 1 ) then
          begin
               for j := ( i + 1 ) to ( Lista.Count - 1 ) do
               begin
                    if not FTerminal.Verificar( TTerminal( Lista.Objects[ j ] ), sMensaje ) then
                    begin
                         ZetaDialogo.zError( 'Error Al Grabar', sMensaje, 0 );
                         Result := False;
                         Break;
                    end;
               end;
          end;
     end;
end;

procedure TCapturarTerminales.BuscarAlarmaClick(Sender: TObject);
begin
     inherited;
     Alarma.Text := BuscaArchivo( Alarma.Text, 'Alarmas|*.val|Todos los Archivos|*.*', 'Archivo de Alarma' );
end;

procedure TCapturarTerminales.BuscarEmpleadosClick(Sender: TObject);
begin
     inherited;
     Empleados.Text := BuscaArchivo( Empleados.Text, 'Empleado|*.val|Todos los Archivos|*.*', 'Archivo de Empleados' );
end;

procedure TCapturarTerminales.OKClick(Sender: TObject);
begin
     inherited;
     TCPIP1.Perform( CM_EXIT, 0, 0 );
     TCPIP2.Perform( CM_EXIT, 0, 0 );
     TCPIP3.Perform( CM_EXIT, 0, 0 );
     TCPIP4.Perform( CM_EXIT, 0, 0 );
     PuertoTCP.Perform( CM_EXIT, 0, 0 );
     TimeOut.Perform( CM_EXIT, 0, 0 );
     with LinxIni.IniValues do
     begin
          if VerificaDatos( TerminalList.Items ) then
          begin
               ListaTerminales.Descargar( TerminalList.Items );
          end
          else
          begin
               ModalResult := mrNone;
          end;
     end;
end;

end.
