unit FArchivos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls,
     ZBaseDlgModal,
     LinxIni;

type
  TArchivos = class(TZetaDlgModal)
    ConfigLBL: TLabel;
    ProgramaLBL: TLabel;
    AlarmaLBL: TLabel;
    EmpleadosLBL: TLabel;
    ConfiguracionSeek: TSpeedButton;
    ProgramaSeek: TSpeedButton;
    AlarmaSeek: TSpeedButton;
    EmpleadosSeek: TSpeedButton;
    Empleados: TEdit;
    Alarma: TEdit;
    Programa: TEdit;
    Configuracion: TEdit;
    OpenDialog: TOpenDialog;
    procedure FormShow(Sender: TObject);
    procedure ConfiguracionSeekClick(Sender: TObject);
    procedure ProgramaSeekClick(Sender: TObject);
    procedure AlarmaSeekClick(Sender: TObject);
    procedure EmpleadosSeekClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
    function BuscaArchivo( const sFileName, sFilter, sTitle: String ): String;
  public
    { Public declarations }
  end;

var
  Archivos: TArchivos;

implementation

{$R *.DFM}

procedure TArchivos.FormShow(Sender: TObject);
begin
     inherited;
     with LinxIni.IniValues do
     begin
          Self.Configuracion.Text := Configuracion;
          Self.Programa.Text := Programa;
          Self.Alarma.Text := Alarma;
          Self.Empleados.Text := Empleados;
     end;
end;

function TArchivos.BuscaArchivo( const sFileName, sFilter, sTitle: String ): String;
begin
     with OpenDialog do
     begin
          Title := sTitle;
          FileName := sFileName;
          Filter := sFilter;
          FilterIndex := 0;
          if Execute then
             Result := FileName
          else
              Result := sFileName;
     end;
end;

procedure TArchivos.ConfiguracionSeekClick(Sender: TObject);
begin
     inherited;
     Configuracion.Text := BuscaArchivo( Configuracion.Text, 'Configuración (*.INI)|*.INI|Todos los Archivos|*.*', 'Archivo Configuración' );
end;

procedure TArchivos.ProgramaSeekClick(Sender: TObject);
begin
     inherited;
     Programa.Text := BuscaArchivo( Programa.Text, 'Programas (*.EXE)|*.exe|Todos los Archivos|*.*', 'Aplicación Default' );
end;

procedure TArchivos.AlarmaSeekClick(Sender: TObject);
begin
     inherited;
     Alarma.Text := BuscaArchivo( Alarma.Text, 'Alarmas (*.VAL)|*.val|Todos los Archivos|*.*', 'Archivo de Alarma' );
end;

procedure TArchivos.EmpleadosSeekClick(Sender: TObject);
begin
     inherited;
     Empleados.Text := BuscaArchivo( Empleados.Text, 'Empleado (*.VAL)|*.val|Todos los Archivos|*.*', 'Archivo de Empleados' );
end;

procedure TArchivos.OKClick(Sender: TObject);
begin
     inherited;
     with LinxIni.IniValues do
     begin
          Configuracion := Self.Configuracion.Text;
          Programa := Self.Programa.Text;
          Alarma := Self.Alarma.Text;
          Empleados := Self.Empleados.Text;
     end;
end;

end.
