inherited CapturarTerminales: TCapturarTerminales
  Left = 221
  Top = 239
  Caption = 'Capturar Lista de Terminales'
  ClientHeight = 388
  ClientWidth = 609
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 352
    Width = 609
    TabOrder = 2
    inherited OK: TBitBtn
      Left = 441
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 526
    end
    object Agregar: TBitBtn
      Left = 4
      Top = 4
      Width = 77
      Height = 27
      Hint = 'Agregar Rengl�n'
      Caption = '&Agregar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = AgregarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333333FF33333333FF333993333333300033377F3333333777333993333333
        300033F77FFF3333377739999993333333333777777F3333333F399999933333
        33003777777333333377333993333333330033377F3333333377333993333333
        3333333773333333333F333333333333330033333333F33333773333333C3333
        330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
        993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
        333333333337733333FF3333333C333330003333333733333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
    end
    object Borrar: TBitBtn
      Left = 83
      Top = 4
      Width = 77
      Height = 27
      Hint = 'Borrar Rengl�n'
      Caption = '&Borrar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = BorrarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
        305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
        005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
        B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
        B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
        B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
        B0557777FF577777F7F500000E055550805577777F7555575755500000555555
        05555777775555557F5555000555555505555577755555557555}
      NumGlyphs = 2
    end
    object Modificar: TBitBtn
      Left = 163
      Top = 4
      Width = 71
      Height = 27
      Caption = '&Modificar'
      TabOrder = 4
      OnClick = ModificarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
        000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
        00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
        F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
        0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
        FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
        FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
        0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
        00333377737FFFFF773333303300000003333337337777777333}
      NumGlyphs = 2
    end
  end
  object PanelControles: TPanel
    Left = 240
    Top = 0
    Width = 369
    Height = 352
    Align = alClient
    TabOrder = 1
    object TcpIpGB: TGroupBox
      Left = 1
      Top = 152
      Width = 367
      Height = 88
      Align = alTop
      Caption = ' TCP / IP '
      TabOrder = 1
      object TcpIpLBL: TLabel
        Left = 16
        Top = 18
        Width = 48
        Height = 13
        Alignment = taRightJustify
        Caption = 'Direcci�n:'
      end
      object Punto1: TLabel
        Left = 103
        Top = 20
        Width = 3
        Height = 13
        Caption = '.'
      end
      object Label1: TLabel
        Left = 146
        Top = 20
        Width = 3
        Height = 13
        Caption = '.'
      end
      object Label2: TLabel
        Left = 189
        Top = 20
        Width = 3
        Height = 13
        Caption = '.'
      end
      object PuertoTCPLBL: TLabel
        Left = 30
        Top = 41
        Width = 34
        Height = 13
        Alignment = taRightJustify
        Caption = 'Puerto:'
      end
      object TimeOutLBL: TLabel
        Left = 21
        Top = 64
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'TimeOut:'
      end
      object TimeOutUnitLBL: TLabel
        Left = 119
        Top = 64
        Width = 48
        Height = 13
        Caption = 'Segundos'
      end
      object TCPIP1: TZetaNumero
        Left = 68
        Top = 14
        Width = 33
        Height = 21
        Mascara = mnDias
        MaxLength = 3
        TabOrder = 0
        Text = '0'
        OnChange = HayCambios
        OnExit = TCPIP1Exit
      end
      object TCPIP2: TZetaNumero
        Left = 109
        Top = 14
        Width = 33
        Height = 21
        Mascara = mnDias
        MaxLength = 3
        TabOrder = 1
        Text = '0'
        OnChange = HayCambios
        OnExit = TCPIP2Exit
      end
      object TCPIP3: TZetaNumero
        Left = 152
        Top = 14
        Width = 33
        Height = 21
        Mascara = mnDias
        MaxLength = 3
        TabOrder = 2
        Text = '0'
        OnChange = HayCambios
        OnExit = TCPIP3Exit
      end
      object TCPIP4: TZetaNumero
        Left = 195
        Top = 14
        Width = 33
        Height = 21
        Mascara = mnDias
        MaxLength = 3
        TabOrder = 3
        Text = '0'
        OnChange = HayCambios
        OnExit = TCPIP4Exit
      end
      object PuertoTCP: TZetaNumero
        Left = 68
        Top = 37
        Width = 47
        Height = 21
        Mascara = mnDias
        MaxLength = 5
        TabOrder = 4
        Text = '0'
        OnChange = HayCambios
        OnExit = PuertoTCPExit
      end
      object Timeout: TZetaNumero
        Left = 68
        Top = 60
        Width = 47
        Height = 21
        Mascara = mnDias
        MaxLength = 5
        TabOrder = 5
        Text = '0'
        OnChange = HayCambios
        OnExit = PuertoTCPExit
      end
    end
    object PanelGeneral: TPanel
      Left = 1
      Top = 1
      Width = 367
      Height = 151
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object DescriptionLBL: TLabel
        Left = 7
        Top = 12
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descripci�n:'
      end
      object IdentificadorLBL: TLabel
        Left = 5
        Top = 36
        Width = 61
        Height = 13
        Alignment = taRightJustify
        Caption = 'Identificador:'
      end
      object LetraLBL: TLabel
        Left = 39
        Top = 60
        Width = 27
        Height = 13
        Alignment = taRightJustify
        Caption = 'Letra:'
      end
      object TipoLBL: TLabel
        Left = 42
        Top = 84
        Width = 24
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo:'
      end
      object AlarmaLBL: TLabel
        Left = 30
        Top = 106
        Width = 35
        Height = 13
        Alignment = taRightJustify
        Caption = 'Alarma:'
      end
      object EmpleadosLBL: TLabel
        Left = 11
        Top = 129
        Width = 55
        Height = 13
        Alignment = taRightJustify
        Caption = 'Empleados:'
      end
      object BuscarAlarma: TSpeedButton
        Left = 340
        Top = 102
        Width = 23
        Height = 24
        Hint = 'Buscar Archivo ALARMA.VAL'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
          300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
          330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
          333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
          339977FF777777773377000BFB03333333337773FF733333333F333000333333
          3300333777333333337733333333333333003333333333333377333333333333
          333333333333333333FF33333333333330003333333333333777333333333333
          3000333333333333377733333333333333333333333333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = BuscarAlarmaClick
      end
      object BuscarEmpleados: TSpeedButton
        Left = 340
        Top = 126
        Width = 23
        Height = 24
        Hint = 'Buscar Archivo EMPLEADOS.VAL'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
          300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
          330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
          333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
          339977FF777777773377000BFB03333333337773FF733333333F333000333333
          3300333777333333337733333333333333003333333333333377333333333333
          333333333333333333FF33333333333330003333333333333777333333333333
          3000333333333333377733333333333333333333333333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = BuscarEmpleadosClick
      end
      object Description: TEdit
        Left = 68
        Top = 8
        Width = 169
        Height = 21
        MaxLength = 15
        TabOrder = 0
        OnChange = HayCambiosDescripcion
      end
      object Identificador: TEdit
        Left = 68
        Top = 32
        Width = 52
        Height = 21
        CharCase = ecUpperCase
        MaxLength = 4
        TabOrder = 1
        OnChange = HayCambios
        OnClick = IdentificadorEnter
        OnEnter = IdentificadorEnter
      end
      object Letra: TComboBox
        Left = 68
        Top = 56
        Width = 53
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 2
        OnClick = HayCambios
        Items.Strings = (
          'A'
          'B'
          'C'
          'D'
          'E'
          'F'
          'G'
          'H'
          'I'
          'J'
          'K'
          'L'
          'M'
          'N'
          'O'
          'P'
          'Q'
          'R'
          'S'
          'T'
          'U'
          'V'
          'W'
          'X'
          'Y'
          'Z')
      end
      object Tipo: TComboBox
        Left = 68
        Top = 80
        Width = 85
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 3
        OnClick = HayCambios
        Items.Strings = (
          'TCP/IP'
          'Serial')
      end
      object Empleados: TEdit
        Left = 68
        Top = 126
        Width = 269
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 5
        OnChange = HayCambios
      end
      object Alarma: TEdit
        Left = 68
        Top = 103
        Width = 269
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 4
        OnChange = HayCambios
      end
    end
    object SerialGB: TGroupBox
      Left = 1
      Top = 240
      Width = 367
      Height = 111
      Align = alClient
      Caption = ' Serial '
      TabOrder = 2
      object SerialPortLBL: TLabel
        Left = 32
        Top = 20
        Width = 34
        Height = 13
        Alignment = taRightJustify
        Caption = 'Puerto:'
      end
      object BaudsLBL: TLabel
        Left = 33
        Top = 44
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Bauds:'
      end
      object StopBitsLBL: TLabel
        Left = 21
        Top = 68
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Stop Bits:'
      end
      object ParidadLBL: TLabel
        Left = 27
        Top = 92
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Paridad:'
      end
      object Bauds: TComboBox
        Left = 68
        Top = 40
        Width = 109
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 1
        OnClick = HayCambios
        Items.Strings = (
          '300'
          '600'
          '2,400'
          '4,800'
          '9,600'
          '19,200'
          '38,400')
      end
      object SerialPort: TComboBox
        Left = 68
        Top = 16
        Width = 109
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 0
        OnClick = HayCambios
        Items.Strings = (
          'COM 1'
          'COM 2'
          'COM 3'
          'COM 4'
          'COM 5'
          'COM 6'
          'COM 7'
          'COM 8'
          'COM 9')
      end
      object Paridad: TComboBox
        Left = 68
        Top = 88
        Width = 109
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 3
        OnClick = HayCambios
        Items.Strings = (
          'Non'
          'Par'
          'Ninguna')
      end
      object StopBits: TComboBox
        Left = 68
        Top = 64
        Width = 109
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 2
        OnClick = HayCambios
        Items.Strings = (
          'Uno'
          'Dos')
      end
    end
  end
  object PanelLista: TPanel
    Left = 0
    Top = 0
    Width = 240
    Height = 352
    Align = alLeft
    TabOrder = 0
    object TerminalList: TListBox
      Left = 1
      Top = 1
      Width = 238
      Height = 350
      Align = alClient
      ItemHeight = 13
      TabOrder = 0
      OnClick = TerminalListClick
    end
  end
  object OpenDialog: TOpenDialog
    Left = 104
    Top = 16
  end
end
