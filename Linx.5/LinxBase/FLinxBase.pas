unit FLinxBase;

interface

uses SysUtils, WinTypes, WinProcs, Messages, Classes,
     Graphics, Controls, Forms, Dialogs, StdCtrls, Grids,
     Menus, Buttons, ExtCtrls, DB, DBTables, ComCtrls,
     FAsciiServer,
     LinxIni,FHelpManager,ZetaRegistryCliente;

type
  eBatchOp = (boUnknown, boAsistencia, boAlarma, boSincronizar, boImportar, boEmpleados, boFlash, boScript, boReboot, boProgramar);
  Executer = procedure( const SelectedTerminal: Integer ) of object;
  FileSender = procedure( const SelectedTerminal: Integer; sFileName: String ) of object;
  TAsciiData = class( TObject )
  private
    { Private declarations }
    FIsOpen: Boolean;
    FFileName: String;
    FTextFile: TextFile;
    function GetEof: Boolean;
  protected
    { Private Declarations }
  public
    { Public Declarations }
    destructor Destroy; override;
    property IsOpen: Boolean read FIsOpen;
    property FileName: String read FFileName write FFileName;
    property EndOfFile: Boolean read GetEof;
    function Read: String;
    procedure Close;
    procedure DeleteFile;
    procedure Open; virtual;
    procedure OpenNew;
    procedure OpenReadOnly;
    procedure WriteText( const sText: String );
  end;
  TRelojDat = class( TAsciiData )
  private
    { Private declarations }
  public
    { Public Declarations }
    constructor Create;
    procedure AttachASCII(const sFileName: String);
  end;
  TBitacora = class( TAsciiData )
  private
    { Private declarations }
    function GetTimeStamp: String;
  public
    { Public Declarations }
    constructor Create;
    procedure Open; override;
    procedure OpenSpecial( const sMessage: String );
    procedure WriteError( const sText, sError: String );
    procedure WriteException( const sText: String; Error: Exception );
    procedure WriteTimeStamp( const sText: String );
  end;
  TLinxBase = class(TForm)
    MainMenu: TMainMenu;
    Archivo: TMenuItem;
    ArchivoRecolectar: TMenuItem;
    ArchivoSalir: TMenuItem;
    Ayuda: TMenuItem;
    AyudaContenido: TMenuItem;
    AyudaUsandoAyuda: TMenuItem;
    N1: TMenuItem;
    AyudaAcercaDe: TMenuItem;
    PanelBotones: TPanel;
    Sincronizar: TBitBtn;
    Version: TBitBtn;
    Programa: TBitBtn;
    Alarma: TBitBtn;
    Terminales: TMenuItem;
    TerminalesSincronizar: TMenuItem;
    TerminalesSincronizarTodas: TMenuItem;
    TerminalesVersion: TMenuItem;
    N3: TMenuItem;
    TerminalesPrograma: TMenuItem;
    TerminalesAlarma: TMenuItem;
    ArchivoTerminar: TMenuItem;
    N4: TMenuItem;
    TerminalesResetear: TMenuItem;
    TerminalesReboot: TMenuItem;
    PanelInferior: TPanel;
    LecturasGB: TGroupBox;
    Lecturas: TLabel;
    UltimaLecturaGB: TGroupBox;
    UltimaLectura: TLabel;
    UnitList: TListBox;
    TerminalesEmpleados: TMenuItem;
    OpenDialog: TOpenDialog;
    ProgressBar: TProgressBar;
    PanelControl: TPanel;
    Recolectar: TBitBtn;
    Terminar: TBitBtn;
    Relojito: TLabel;
    StatusBar: TStatusBar;
    Configuracion: TMenuItem;
    ConfiguracionLista: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    ConfiguracionAlarmas: TMenuItem;
    ConfiguracionFuncionalidad: TMenuItem;
    N2: TMenuItem;
    ConfiguracionServidor: TMenuItem;
    N8: TMenuItem;
    ArchivoImportar: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure ArchivoClick(Sender: TObject);
    procedure ArchivoRecolectarClick(Sender: TObject);
    procedure ArchivoTerminarClick(Sender: TObject);
    procedure ArchivoImportarClick(Sender: TObject);
    procedure ArchivoSalirClick(Sender: TObject);
    procedure TerminalesClick(Sender: TObject);
    procedure TerminalesTodasClick(Sender: TObject);
    procedure ConfiguracionClick(Sender: TObject);
    procedure ConfiguracionAlarmasClick(Sender: TObject);
    procedure ConfiguracionServidorClick(Sender: TObject);
    procedure ConfiguracionFuncionalidadClick(Sender: TObject);
    procedure AyudaContenidoClick(Sender: TObject);
    procedure AyudaUsandoAyudaClick(Sender: TObject);
    procedure AyudaAcercaDeClick(Sender: TObject);
  private
    { Private declarations }
    FCursor: TCursor;
    FAsciiServer: TAsciiServer;
    FRecolectando: Boolean;
    FDataRx: TDateTime;
    FBitacora: TBitacora;
    FBatchMode: Boolean;
    FEsDemo: Boolean;
    FRelojDat: TRelojDat;
    FOffLineBuffer: String;
    function GetAllSelected: Boolean;
    procedure ShowLecturas( const lVisible: Boolean );
    procedure IniciaRecoleccion( const iIntervaloTime: Integer );
    procedure Procesar(const sArchivo: String);
    procedure ProcesarBatch(const sArchivo: String);
  protected
    { Protected declarations }
    FLecturas: Integer;
    FOldHelpEvent: THelpEvent;
    procedure StartPoll;
    procedure EndPoll;
    function HTMLHelpHook( Command: Word; Data: Longint; var CallHelp: Boolean ): Boolean;
    property DataRX: TDateTime read FDataRX;
    property EsDemo: Boolean read FEsDemo write FEsDemo;
    function BatchModeEnabled: Boolean; virtual;
    function DatosEnviados: String; virtual; abstract;
    function GetFileName( var sFileName: String ): Boolean;
    function InitAllServers: Boolean; virtual;
    function Read: Boolean; virtual; abstract;
    function RegistroLeido: String; virtual; abstract;
    function RegistroNormal: Boolean; virtual; abstract;
    function SecondsElapsed( const dStart, dEnd: TDateTime ): Integer;
    function Terminal: Integer; virtual; abstract;
    function VerifyAllServers: Boolean; virtual; abstract;
    function PuedeRecolectar: Boolean; virtual;
    procedure AutoSincronizarTodas( const dValue: TDate ); virtual; abstract;
    procedure BatchModeSet;
    procedure BatchModeReset;
    procedure CicloLecturas;
    procedure IncrementaLecturas; virtual;
    procedure DownAllServers; virtual;
    procedure EnableControls( const lRecolectando: Boolean ); virtual;
    procedure EnablePoll( const lEnabled: Boolean ); virtual;
    procedure GaugeSetSize( const sMsg: String; iValue: Integer );
    procedure GaugeAdvance( const sMsg: String; iValue: Integer );
    procedure GaugeFinish( const sMsg: String; iValue: Integer );
    procedure ProcesarLectura( var sData: String ); virtual;
    procedure InitIniValues;
    procedure LogClose;
    procedure LogOpen; overload;
    procedure LogOpen( const sMessage: String ); overload;
    procedure SendAlarm( const sFileName: String ); virtual; abstract;
    procedure SendEmpleados( const sFileName: String ); virtual; abstract;
    procedure SendFlash( const sFileName: String ); virtual; abstract;
    procedure SendScript( const sFileName: String ); virtual; abstract;
    procedure SendPrograma( const sFileName, sFileName2, sFileName3 : String ); virtual; abstract;
    procedure SendRebootCmd; virtual; abstract;
    procedure SetCursorWait;
    procedure SetCursorNormal;
    procedure StatusMessage( const sMessage: String );
    procedure MakeNodeList; virtual; abstract;
    procedure ManejaBitacora( const sMensaje: String );
    procedure ManejaMensaje( const sMensaje: String );
    procedure ManejaError( const sMensaje: String; Error: Exception );
    procedure ManejaExcepcion( Sender: TObject; E: Exception );
    procedure EscribeAllData( const sData: String ); virtual;
    function ChecaTopeLecturas: Boolean;
  public
    { Public declarations }
    property BatchMode: Boolean read FBatchMode;
    property Bitacora: TBitacora read FBitacora;
    property AsciiServer: TAsciiServer read FAsciiServer;
    property AllTerminals: Boolean read GetAllSelected;
    property Recolectando: Boolean read FRecolectando;
    procedure CicloBatch( const eOp: eBatchOp; const sParametro: String ); overload;
    procedure CicloBatch( const eOp: eBatchOp; const sParametro, sParametro2, sParametro3: String ); overload;
    procedure MakeUnitList; virtual; abstract;
    procedure DisplayHint( Sender: TObject );
    procedure PostMessage( const sMessage: String; iTerminal: Integer );
    procedure ProcesaParametro( const sParametro: String ); overload; virtual;
    procedure ProcesaParametro( const sParametro, sArchivo: String ); overload; virtual;
    function ValidaConexionHTTP( var sMensajeErrorHTTP: String ): Boolean;
  end;

var
  LinxBase: TLinxBase;

implementation

uses ZetaDialogo,
     ZetaCommonTools,
     ZetaCommonClasses,
     DPoll,
     FImportar,
     FAutoClasses,
     FLinxTools,
     FAlarmas,
     FFuncionalidad,
     FPollAcercaDe;

{$R *.DFM}

const
{$ifdef PRUEBAS}
     TOPE_DEMO = 10;
{$else}
     TOPE_DEMO = 25;
{$endif}
     //Modificar tambien en :
     //function TL7Server.DataDownload( Terminal: TTerminal ): Boolean;
     //K_TOPE_DEMO = 25;
     
function GetLecturas( const iValue: Integer ): String;
const
     K_FORMAT_LECTURAS = '%-9.0n';
begin
     Result := Trim( Format( K_FORMAT_LECTURAS, [ iValue / 1 ] ) );
end;

{ ************ TAsciiData *********** }

destructor TAsciiData.Destroy;
begin
     if IsOpen then
        Close;
end;

procedure TAsciiData.Open;
begin
     try
        AssignFile( FTextFile, FileName );
        if FileExists( FileName ) then
           Append( FTextFile )
        else
            Rewrite( FTextFile );
        FIsOpen := True;
     except
           on Error: Exception do
           begin
                FIsOpen := False;
           end;
     end;
end;

procedure TAsciiData.OpenNew;
begin
     try
        if FileExists( FileName ) then
           SysUtils.DeleteFile( FileName );
        AssignFile( FTextFile, FileName ); { Siempre crea uno nuevo }
        Rewrite( FTextFile );
        FIsOpen := True;
     except
           on Error: Exception do
           begin
                FIsOpen := False;
           end;
     end;
end;

procedure TAsciiData.OpenReadOnly;
begin
     try
        if FileExists( FileName ) then
        begin
             AssignFile( FTextFile, FileName );
             Reset( FTextFile );
             FIsOpen := True;
        end
        else
            FIsOpen := False;
     except
           on Error: Exception do
           begin
                FIsOpen := False;
           end;
     end;
end;

procedure TAsciiData.DeleteFile;
begin
     if not IsOpen and FileExists( FFileName ) then
     begin
          SysUtils.DeleteFile( FFileName );
     end;
end;

procedure TAsciiData.WriteText( const sText: String );
begin
     Writeln( FTextFile, sText );
end;

function TAsciiData.GetEof: Boolean;
begin
     Result := Eof( FTextFile );
end;

function TAsciiData.Read: String;
begin
     Readln( FTextFile, Result );
end;

procedure TAsciiData.Close;
begin
     if FIsOpen then
        CloseFile( FTextFile );
     FIsOpen := False;
end;

{ ************ TRelojDat *********** }

constructor TRelojDat.Create;
begin
     FileName := FLinxTools.SetFileNameDefaultPath( 'RELOJ.DAT' );
end;

procedure TRelojDat.AttachASCII( const sFileName: String );
var
   FAttachment: TASCIIData;
begin
     if ( sFileName <> '' ) and FileExists( sFileName ) then
     begin
          FAttachment := TASCIIData.Create;
          with FAttachment do
          begin
               try
                  FileName := sFileName;
                  OpenReadOnly;
                  if IsOpen then
                  begin
                       while not EndOfFile do
                       begin
                            Self.WriteText( Read );
                       end;
                       Close;
                       DeleteFile;
                  end;
               finally
                      Free;
               end;
          end;
     end;
end;

{ ************ TBitacora *********** }

constructor TBitacora.Create;
begin
     FFileName := SetFileNameDefaultPath( 'BITACORA.TXT' );
end;

function TBitacora.GetTimeStamp: String;
begin
     Result := FormatDateTime( 'ddd dd/mmm/yyyy hh:nn:ss AM/PM', Now );
end;

procedure TBitacora.Open;
begin
     inherited Open;
     WriteTimeStamp( '*** Recolecci�n Empez� %s ***' );
end;

procedure TBitacora.OpenSpecial( const sMessage: String );
begin
     inherited Open;
     WriteTimeStamp( '*** ' + sMessage + ' Empez� %s ***' );
end;
procedure TBitacora.WriteError( const sText, sError: String );
var
   pValor, pStart: PChar;
   sValor: String;
begin
     WriteTimeStamp( 'Fecha y Hora: %s Evento: ' + sText );
     pValor := Pointer( sError );
     if ( pValor <> nil ) then
     begin
          while ( pValor^ <> #0 ) do
          begin
               pStart := pValor;
               while not ( pValor^ in [ #0, #10, #13 ] ) do
               begin
                    Inc( pValor );
               end;
               SetString( sValor, pStart, ( pValor - pStart ) );
               if ( sValor <> '' ) then
                  WriteText( sValor );
               if ( pValor^ = #13 ) then
                  Inc( pValor );
               if ( pValor^ = #10 ) then
                  Inc( pValor );
          end;
     end;
end;

procedure TBitacora.WriteException( const sText: String; Error: Exception );
begin
     WriteError( sText, Error.Message );
end;

procedure TBitacora.WriteTimeStamp( const sText: String );
begin
     WriteText( Format( sText, [ GetTimeStamp ] ) );
end;

{$define DUMMY_SERVER}
{$undef DUMMY_SERVER}

{ ********* TLinxBase ******* }

procedure TLinxBase.FormCreate(Sender: TObject);

const
     K_OFFLINE_BUFFER = 'BUFFER.DAT';
begin
     FAutoClasses.InitAuto;
     LinxIni.IniValuesInit;
     with Application do
     begin
          OnHint := DisplayHint;
          {
          OnException := ManejaExcepcion;
          }
          UpdateFormatSettings := FALSE;
          FOldHelpEvent := OnHelp;
          OnHelp := HTMLHelpHook;
     end;
     Environment;
     Lecturas.Caption := '';
     UltimaLectura.Caption := '';
     Relojito.Caption := '';
     FAsciiServer := TAsciiServer.Create;
     FBitacora := TBitacora.Create;
     FEsDemo := False;
     dmPoll := TdmPoll.Create( Self );
     with dmPoll do
     begin
          OnMessage := ManejaMensaje;
          OnError := ManejaError;
          OnStatus := StatusMessage;
     end;
     FRelojDat := TRelojDat.Create;
     FOffLineBuffer := FLinxTools.SetFileNameDefaultPath( K_OFFLINE_BUFFER );
     with Autorizacion do
     begin
          try
             Cargar( dmPoll.GetAuto );
          except
                on Error: Exception do
                begin
                     if ( ParamCount > 0  ) then
                     begin
                          LogOpen( 'Error al conectar servidor' );
                          Bitacora.WriteException( 'Error', Error );
                          LogClose;
                     end
                     else
                         Application.HandleException( Error );
                end;
          end;
          Self.EsDemo := EsDemo or not OkModulo( okL5Poll, False );
     end;
end;

procedure TLinxBase.FormShow(Sender: TObject);
begin
     InitIniValues;
     EnableControls( False );
     EnablePoll( True );
end;

procedure TLinxBase.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
     if Recolectando then
     begin
          zWarning( '� Recolectando Datos !', 'Antes de Salir Hay Que Terminar La Recolecci�n De Datos', 0, mbOK );
          CanClose := False;
     end
     else
         CanClose := zConfirm( '� Atenci�n !', '� Realmente Desea Salir?', 0, mbNo );
end;

procedure TLinxBase.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Action := caFree;
end;

function TLinxBase.HTMLHelpHook(Command: Word; Data: Integer; var CallHelp: Boolean): Boolean;
begin
     Result := FHelpManager.HtmlHelpHook( Application.HelpFile, Command, Data, CallHelp );
end;

procedure TLinxBase.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FRelojDat );
     Application.OnHelp := FOldHelpEvent;
     FAutoClasses.ClearAuto;
     FreeAndNil( dmPoll );
     FBitacora.Free;
     AsciiServer.Free;
     LinxIni.IniValuesClear;
end;

procedure TLinxBase.InitIniValues;
begin
end;

function TLinxBase.BatchModeEnabled: Boolean;
begin
     Result := True;
end;

procedure TLinxBase.BatchModeReset;
begin
     FBatchMode := False;
end;

procedure TLinxBase.BatchModeSet;
begin
     FBatchMode := True;
end;

function TLinxBase.SecondsElapsed( const dStart, dEnd: TDateTime ): Integer;
var
   iHour, iMin, iSegs, iMiliSegs: Word;
begin
     DecodeTime( ( dEnd - dStart ), iHour, iMin, iSegs, iMiliSegs );
     Result := ( 60 * ( ( 60 * iHour ) + iMin ) ) + iSegs;
end;

procedure TLinxBase.ManejaExcepcion( Sender: TObject; E: Exception );
begin
     ManejaError( 'Error', E );
end;

procedure TLinxBase.ManejaError( const sMensaje: String; Error: Exception );
begin
     if BatchMode then
        Bitacora.WriteException( sMensaje, Error )
     else
         ZetaDialogo.zExcepcion( Caption, sMensaje, Error, 0 );
end;

procedure TLinxBase.ManejaMensaje( const sMensaje: String );
begin
     if BatchMode then
        Bitacora.WriteText( sMensaje )
     else
         ZetaDialogo.zError( Caption, sMensaje, 0 );
end;

procedure TLinxBase.ManejaBitacora( const sMensaje: String );
begin
     Bitacora.WriteText( sMensaje )
end;

procedure TLinxBase.DisplayHint(Sender: TObject);
begin
     StatusMessage( GetLongHint( Application.Hint ) );
end;

procedure TLinxBase.GaugeSetSize( const sMsg: String; iValue: Integer );
begin
     ShowLecturas( False );
     with ProgressBar do
     begin
          Visible := True;
          Position := 0;
          Min := 0;
          Max := iValue;
          Refresh;
     end;
end;

procedure TLinxBase.GaugeAdvance( const sMsg: String; iValue: Integer );
begin
     ProgressBar.StepBy( iValue );
end;

procedure TLinxBase.GaugeFinish( const sMsg: String; iValue: Integer );
begin
     with ProgressBar do
     begin
          Position := Max;
          Visible := False;
     end;
     ShowLecturas( True );
end;

procedure TLinxBase.IncrementaLecturas;
begin
     FLecturas := FLecturas + 1;
     Lecturas.Caption := GetLecturas( FLecturas );
     UltimaLectura.Caption := DateTimeToStr( Now );
end;

procedure TLinxBase.ShowLecturas( const lVisible: Boolean );
begin
     Lecturas.Visible := lVisible;
     UltimaLectura.Visible := lVisible;
     Relojito.Visible := lVisible;
end;

procedure TLinxBase.SetCursorWait;
begin
     if not BatchMode then
     begin
          FCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
     end;
end;

procedure TLinxBase.SetCursorNormal;
begin
     if not BatchMode then
     begin
          Screen.Cursor := FCursor;
     end;
end;

procedure TLinxBase.EnablePoll( const lEnabled: Boolean );
begin
     ArchivoRecolectar.Enabled := lEnabled;
     Recolectar.Enabled := lEnabled;
end;

procedure TLinxBase.EnableControls( const lRecolectando: Boolean );
begin
     Terminales.Enabled := lRecolectando;
     { Botones }
     Terminar.Enabled := lRecolectando;
     Sincronizar.Enabled := lRecolectando;
     Version.Enabled := lRecolectando;
     Programa.Enabled := lRecolectando;
     Alarma.Enabled := lRecolectando;
     UnitList.Enabled := lRecolectando;
     Relojito.Enabled := lRecolectando;
end;

function TLinxBase.GetAllSelected: Boolean;
begin
     with UnitList do
     begin
          Result := ( Items.Count > 1 ) and ( Items.Count = SelCount );
     end;
end;

procedure TLinxBase.StatusMessage( const sMessage: String );
begin
     if not BatchMode then
     begin
          StatusBar.SimpleText := sMessage;
          Application.ProcessMessages;
     end;
end;

procedure TLinxBase.PostMessage( const sMessage: String; iTerminal: Integer );
var
   i: Integer;
   lSelected: Boolean;

function FormatUnitMessage( sOldMsg, sNewMsg: String ): String;
begin
     Result := Copy( Trim( Copy( sOldMsg, 1, 23 ) ) + StringOfChar( ' ', 23 ), 1, 23 ) + '|' + sNewMsg;
end;

begin
     iTerminal := iTerminal - 1;
     with UnitList do
     begin
          Items.BeginUpdate;
          if ( iTerminal < 0 ) then
          begin
               for i := 0 to ( Items.Count - 1 ) do
               begin
                    lSelected := Selected[ i ];
                    Items.Strings[ i ] := FormatUnitMessage( Items.Strings[ i ], sMessage );
                    Selected[ i ] := lSelected;
               end;
          end
          else
          begin
               if ( iTerminal < Items.Count ) then
               begin
                    lSelected := Selected[ iTerminal ];
                    Items.Strings[ iTerminal ] := FormatUnitMessage( Items.Strings[ iTerminal ], sMessage );
                    Selected[ iTerminal ] := lSelected;
               end
               else
                   Items.Add( FormatUnitMessage( 'Terminal Desconocida', sMessage ) );
          end;
          Items.EndUpdate;
     end;
end;

procedure TLinxBase.StartPoll;
begin
     try
        SetCursorWait;
        try
           if InitAllServers then
           begin
                FRecolectando := True;
                try
                   VerifyAllServers;
                except
                      on Error: Exception do
                      begin
                           StatusMessage( Error.Message );
                      end;
                end;
           end;
        except
              on Error: Exception do
              begin
                   ManejaError( 'Error Al Inicializar Servidor de Terminales Linx', Error );
              end;
        end;
     finally
            SetCursorNormal;
     end;
end;

function TLinxBase.ChecaTopeLecturas: Boolean;
begin
     if FEsDemo then
        Result := ( FLecturas <= TOPE_DEMO )
     else
         Result := True;
end;

procedure TLinxBase.CicloLecturas;
var
   sData: String;
   dStart: TDateTime;
   sMensajeErrorHTTP: String;
begin
     FRecolectando := False;
     EnableControls( True );
     if not ValidaConexionHTTP( sMensajeErrorHTTP ) then
     begin
          try
             EnablePoll( False );
             LogOpen;
             StartPoll;
             StatusMessage( 'Iniciando Recolecci�n' );
             if Recolectando then
             begin
                  if FEsDemo then
                     ZetaDialogo.zInformation( 'Versi�n DEMO', Format( 'Se Recolectar�n Hasta %d Checadas Unicamente', [ TOPE_DEMO ] ), 0 );
                  FLecturas := -1;
                  IncrementaLecturas;
                  ShowLecturas( True );
                  while Recolectando and ChecaTopeLecturas do
                  begin
                       dStart := Now;
                       while ChecaTopeLecturas and Read do
                       begin
                            FDataRX := Now;
                            sData := RegistroLeido;
                            EscribeAllData( DatosEnviados );
                            if RegistroNormal then
                            begin
                                 ProcesarLectura( sData );
                                 IncrementaLecturas;
                            end;
                            PostMessage( sData, Terminal );
                            Application.ProcessMessages;
                       end;
                       MakeNodeList;
                       Relojito.Caption := TimeToStr( Now );
                       Application.ProcessMessages;
                       IniValues.ListaAlarmas.CheckInterval( dStart, SendAlarm );
                  end;
             end;
             StatusMessage( 'Recolecci�n Ha Terminado' );
             EndPoll;
             LogClose;
          finally
                 FRecolectando := False;
                 EnableControls( False );
          end;
     end
     else
     begin
          FRecolectando := False;
          EnableControls( False );
          ZetaDialogo.zInformation( Application.Title, sMensajeErrorHTTP, 0 );
     end;
end;

procedure TLinxBase.EscribeAllData( const sData: String );
begin
     AsciiServer.WriteAllData( sData );
end;

procedure TLinxBase.IniciaRecoleccion( const iIntervaloTime: Integer );
var
   dStart: TDateTime;
   sData: String;
begin
     dStart := Now; { Inicia Recolecci�n }
     while ( SecondsElapsed( dStart, Now ) <= iIntervaloTime ) and ChecaTopeLecturas do
     begin
          while ChecaTopeLecturas and Read do
          begin
               sData := RegistroLeido;
               EscribeAllData( DatosEnviados );
               if RegistroNormal then
               begin
                    ProcesarLectura( sData );
                    IncrementaLecturas;
               end;
               dStart := Now;
          end;
     end;
end;

procedure TLinxBase.CicloBatch( const eOp: eBatchOp; const sParametro: String );
begin
    CicloBatch( eOp, sParametro, VACIO, VACIO );
end;

procedure TLinxBase.CicloBatch( const eOp: eBatchOp; const sParametro, sParametro2, sParametro3: String );
const
     K_SET_TIME = 'Terminales Sincronizadas A %s';
     K_SHOW_TIME = 'dd/mmm/yyyy hh:nn:ss AM/PM';
var
   iIntervalo: Integer;
   sData: String;
   dStart: TDateTime;
   sMensajeErrorHTTP: String;
begin
     BatchModeSet;
     try
        InitIniValues;
        iIntervalo := IniValues.Intervalo;
        try
           LogOpen;
           if not ValidaConexionHTTP( sMensajeErrorHTTP ) then
           begin
                if( PuedeRecolectar )then
                begin
                     StartPoll;
                     if Recolectando then
                     begin
                          FLecturas := -1;
                          if FEsDemo then
                             Bitacora.WriteText( Format( 'Versi�n DEMO: Se Recolectar�n Hasta %d Checadas Unicamente', [ TOPE_DEMO ] ) );
                          IncrementaLecturas;
                          //Comienza a Recolectar checadas
                          IniciaRecoleccion( iIntervalo );
                          { Sincronizar }
                          if ( eOp = boSincronizar ) then
                          begin
                               if not FEsDemo then
                               begin
                                    if ZetaCommonTools.StrLleno( sParametro ) then
                                    begin
                                         sData := sParametro;
                                         if ZetaCommonTools.TimeIsValid( 24, sData ) then { Par�metro de Hora es v�lido: Sincronizar }
                                         begin
                                              dStart := Trunc( Now ) + EncodeTime( StrToIntDef( Copy( sData, 1, 2 ), 0 ), StrToIntDef( Copy( sData, 3, 2 ), 0 ), 0, 0 );
                                              AutoSincronizarTodas( dStart );
                                              Bitacora.WriteText( Format( K_SET_TIME, [ FormatDateTime( K_SHOW_TIME, dStart ) ] ) );
                                         end;
                                    end
                                    else
                                    begin { Usar Hora del Sistema }
                                         AutoSincronizarTodas( Now );
                                         Bitacora.WriteText( Format( K_SET_TIME, [ FormatDateTime( K_SHOW_TIME, Now ) ] ) );
                                    end;
                               end;
                          end
                          else
                          begin
                               //MA:29/ABR/2003: Antes se llamaba de este punto el IniciaRecoleccion por eso se perdian checadas.
                               if not FEsDemo then
                               begin
                                    case eOp of
                                         boAlarma: SendAlarm( sParametro ); { Enviar ALARMA.VAL }
     {
                                         //ER: La validaci�n se hace dentro del metodo SendAlarm()
                                         begin
                                              if ZetaCommonTools.StrLleno( sParametro ) and FileExists( sParametro ) then
                                              begin
                                                   SendAlarm( sParametro ); // Enviar ALARMA.VAL
                                              end;
                                         end;
     }
                                         boEmpleados: SendEmpleados( sParametro ); { Enviar EMPLEADO.VAL }
                                         boFlash: SendFlash( sParametro ); { Enviar archivo a FLASH }
                                         boScript: SendScript( sParametro ); { Enviar archivo script }
                                         boReboot: SendRebootCmd; { Enviar comando de reboot }
                                         boProgramar : SendPrograma( sParametro, sParametro2, sParametro3 );
                                    end;
                               end;
                               if ( FLecturas <= 0 ) then
                                  ExitCode := 1; { No Hubo Lecturas }
                          end;
                     end;
                end
                else
                    Bitacora.WriteText( 'Para realizar cualquier operaci�n es necesario tener una lista de Terminales' );
                EndPoll;
           end
           else
           begin
                Bitacora.WriteText( sMensajeErrorHTTP );
           end;
        except
              on Error: Exception do
              begin
                   ManejaError( 'Error Al Recolectar En Modo Batch', Error );
                   ExitCode := 2; { Se Encontr� un Error }
              end;
        end;

        LogClose;
     finally
            BatchModeReset;
     end;
end;

procedure TLinxBase.EndPoll;
begin
     SetCursorWait;
     DownAllServers;
     SetCursorNormal;
end;

{ ******* M�todos Sobrecargables ********* }

procedure TLinxBase.LogOpen;
begin
     Bitacora.Open;
end;

procedure TLinxBase.LogOpen( const sMessage: String );
begin
     Bitacora.OpenSpecial( sMessage );
end;

procedure TLinxBase.LogClose;
begin
     Bitacora.Close;
end;

function TLinxBase.InitAllServers: Boolean;
begin
     AsciiServer.Init;
     with FRelojDat do
     begin
          OpenNew;
          Result := IsOpen;
     end;
end;

procedure TLinxBase.ProcesarLectura( var sData: String );
begin
     AsciiServer.Write( sData );
     FRelojDat.WriteText( sData );
end;

procedure TLinxBase.DownAllServers;
begin
     Bitacora.WriteTimeStamp( '*** Recolecci�n Termin� %s Con ' + GetLecturas( FLecturas ) + ' Lecturas ***' );
     AsciiServer.CloseAll;
     with FRelojDat do
     begin
          AttachASCII( IniValues.ArchivoAdicional );
          if IniValues.EnviarChecadas then
          begin
               {$ifndef LINX7}
               AttachASCII( FOffLineBuffer );
               {$endif}
               Close;
               if BatchMode then
                  ProcesarBatch( FileName )
               else
                   Procesar( FileName );
          end
          else
              Close;
     end;
end;

procedure TLinxBase.ProcesarBatch( const sArchivo: String );
var
   lOk: Boolean;
   iRecords: Integer;
begin
     with Bitacora do
     begin
          WriteTimeStamp( 'Env�o de Checadas Empez� ( %s )' );
          lOk := dmPoll.DoPoll( sArchivo, FOffLineBuffer, iRecords );
          WriteTimeStamp( Format( 'Checadas Fueron Enviadas', [ iRecords ] ) + ' ( %s )' );
     end;
     if not lOk then
     begin
          with dmPoll.Log do
          begin
               if Active and not IsEmpty then
               begin
                    with Bitacora do
                    begin
                         WriteText( '------- Errores Al Enviar Checadas -------' );
                         while not Eof do
                         begin
                              WriteError( FieldByName( 'POLL_DATA' ).AsString, FieldByName( 'POLL_ERR' ).AsString );
                              Next;
                         end;
                         WriteText( '--- Fin de Errores Al Enviar Checadas ----' );
                    end;
               end;
               Active := False;
          end;
     end;
end;

procedure TLinxBase.Procesar( const sArchivo: String );
var
   iRecords: Integer;
   lLoop, lSalirTress: Boolean;
   sMensajeError: String;
begin
     {if dmCliente.ValidarConexionHTTP( lLoop, lSalirTress, sMensajeError, BatchMode ) then
     begin}
          StatusMessage( 'Enviando Checadas' );
          if dmPoll.DoPoll( sArchivo, FOffLineBuffer, iRecords ) then
             StatusMessage( Format( '%d Checadas Enviadas', [ iRecords ] ) )
          else
          begin
               StatusMessage( 'Problemas Al Enviar Checadas' );
               dmPoll.ShowLog;
          end;
     {end;
     else
     begin
          if BatchMode then
             Bitacora.WriteText( sMensajeError );
     end;}
end;

procedure TLinxBase.ProcesaParametro( const sParametro: String );
begin
     ProcesaParametro( sParametro, '' );
end;

function TLinxBase.ValidaConexionHTTP( var sMensajeErrorHTTP: String ): Boolean;
begin
     Result := ( ( ClientRegistry.TipoConexion = conxHTTP ) and ( not Autorizacion.OkModulo( okTressHTTP, False ) ) and ( not FEsDemo ) );
     if Result then
        sMensajeErrorHTTP := 'El m�dulo de ' + Autorizacion.GetModuloStr( okTressHTTP ) + ' no est� autorizado.' + CR_LF + 'Consulte a Grupo Tress Internacional o a su distribuidor autorizado.';
end;

procedure TLinxBase.ProcesaParametro( const sParametro, sArchivo: String );
const
     P_ASISTENCIA = '/X';
     P_ASISTENCIA_ALARMA = '/XA';
     P_ASISTENCIA_SINCRONIZAR = '/XS';
     P_IMPORTAR = '/I';
     P_EMPLEADOS = '/XE';
     P_FLASH = '/FLASH';
     P_SCRIPT = '/SCRIPT';
     P_REBOOT = '/REBOOT';
var
   iRecords: Integer;
   eOp: eBatchOp;
   sMensajeErrorHTTP: String;
begin
     if BatchModeEnabled then
     begin
          if ( sParametro = P_ASISTENCIA ) then
             eOp := boAsistencia
          else
              if ( sParametro = P_ASISTENCIA_ALARMA ) then
                 eOp := boAlarma
              else
                  if ( sParametro = P_ASISTENCIA_SINCRONIZAR ) then
                     eOp := boSincronizar
                  else
                      if ( sParametro = P_IMPORTAR ) then
                         eOp := boImportar
                      else
                          if ( sParametro = P_EMPLEADOS ) then
                             eOp := boEmpleados
                          else
                            if ( sParametro = P_FLASH ) then
                               eOp := boFlash
                            else
                             if ( sParametro = P_SCRIPT ) then
                                eOp := boScript
                             else
                              if ( sParametro = P_REBOOT ) then
                                 eOp := boReboot
                              else
                                   eOp := boUnknown;
          case eOp of
               boAsistencia: CicloBatch( boAsistencia, '' );
               boAlarma: CicloBatch( boAlarma, sArchivo );
               boSincronizar: CicloBatch( boSincronizar, sArchivo );
               boImportar:
               begin
                    BatchModeSet;
                    try
                       LogOpen( 'Procesamiento de Archivo' );
                       try
                          if not ValidaConexionHTTP( sMensajeErrorHTTP ) then
                          begin
                               if FileExists( sArchivo ) then
                               begin
                                    if dmPoll.EnviarChecadas( sArchivo, iRecords ) then
                                       ManejaMensaje( Format( 'El Archivo %s Fue Procesado Con %d Checadas', [ sArchivo, iRecords ] ) )
                                    else
                                        ManejaMensaje( Format( 'El Archivo %s No Fue Procesado', [ sArchivo ] ) );
                               end
                               else
                                   ManejaMensaje( Format( 'El Archivo %s No Existe', [ sArchivo ] ) );
                          end
                          else
                          begin
                               ManejaMensaje( sMensajeErrorHTTP );
                          end;
                       finally
                              LogClose;
                       end;
                    finally
                           BatchModeReset;
                    end;
               end;
               boEmpleados: CicloBatch( boEmpleados, sArchivo );
               boFlash: CicloBatch( boFlash, sArchivo );
               boScript: CicloBatch( boScript, sArchivo );
               boReboot: CicloBatch( boReboot, '' );
          else
              ZetaDialogo.zError( '� Error En Par�metro !',
                                  'Se Especific� el Par�metro ' + sParametro + CR_LF +
                                  CR_LF +
                                  'Los Par�metros V�lidos Son: ' + CR_LF +
                                  Format( '%s : Operaci�n Batch de Asistencia', [ P_ASISTENCIA ] ) + CR_LF +
                                  Format( '%s <ALARMA.VAL> : Operaci�n Batch de Asistencia y Enviar <ALARMA.VAL>', [ P_ASISTENCIA_ALARMA ] ) + CR_LF +
                                  Format( '%s : Operaci�n Batch de Asistencia y Sincronizar Relojes a Hora del Sistema', [ P_ASISTENCIA_SINCRONIZAR ] ) + CR_LF +
                                  Format( '%s <HORA> : Operaci�n Batch de Asistencia y Sincronizar Relojes a <HORA>', [ P_ASISTENCIA_SINCRONIZAR ] ) + CR_LF +
                                  Format( '%s <ARCHIVO> : Procesar Archivo de Asistencia', [ P_IMPORTAR ] ) + CR_LF +
                                  Format( '%s <ARCHIVO> : Operaci�n Batch de Asistencia y Enviar <EMPLEADO.VAL> � EMPLEADO.VAL Default>', [ P_EMPLEADOS ] ) + CR_LF +
                                  Format( '%s <ARCHIVO> : Enviar Archivo a Memoria Flash', [ P_FLASH ] ) + CR_LF +
                                  Format( '%s <ARCHIVO> : Enviar Archivo Script a Memoria No Volatil', [ P_SCRIPT ] ) + CR_LF +
                                  Format( '%s : Reiniciar Terminal', [ P_REBOOT ] ), 0 );
          end;
     end
     else
         ZetaDialogo.zError( '� Modo Inv�lido !', 'No Se Puede Operar En Modo Batch', 0 );
end;

{ ******** Preguntar por Archivos ************ }

function TLinxBase.GetFileName( var sFileName: String ): Boolean;
var
   sExtension: String;
begin
     sExtension := ExtractFileExt( sFileName );
     with OpenDialog do
     begin
          FileName := sFileName;
          DefaultExt := '';
          Filter := 'Todos Los Archivos(*.*)|*.*';
          if ( sExtension <> '' ) then
          begin
               DefaultExt := sExtension;
               Filter := 'Archivos *' + sExtension + '|*' + sExtension + '|' + Filter;
          end;
          FilterIndex := 0;
          if Execute then
          begin
               Result := True;
               sFileName := FileName;
          end
          else
              Result := False;
     end;
end;

{ ******** Eventos del Men� ********* }

procedure TLinxBase.ArchivoClick(Sender: TObject);
begin
     ArchivoTerminar.Enabled := Recolectando;
end;

procedure TLinxBase.ArchivoRecolectarClick(Sender: TObject);
begin
     CicloLecturas;
end;

procedure TLinxBase.ArchivoTerminarClick(Sender: TObject);
begin
     FRecolectando := False;
end;

procedure TLinxBase.ArchivoImportarClick(Sender: TObject);
var
   oCursor: TCursor;
   iRecords: Integer;
   sMensajeErrorHTTP: String;
begin
     if not ValidaConexionHTTP( sMensajeErrorHTTP ) then
     begin
          with TImportarTarjetas.Create( Self ) do
          begin
               try
                  ShowModal;
                  if ( ModalResult = mrOK ) then
                  begin
                       oCursor := Screen.Cursor;
                       Screen.Cursor := crHourglass;
                       try
                          with dmPoll do
                          begin
                               if EnviarChecadas( ImportFile, ProcesarTarjetas, iRecords ) then
                                  ZetaDialogo.zInformation( '� Archivo Fu� Importado !', Format( 'Se Procesaron %d Checadas', [ iRecords ] ), 0 )
                               else
                                   ShowLog;
                          end;
                       finally
                              Screen.Cursor := oCursor;
                       end;
                  end;
               finally
                      Free;
               end;
          end;
     end
     else
     begin
          ZetaDialogo.zInformation( Application.Title, sMensajeErrorHTTP, 0 );
     end;
end;

procedure TLinxBase.ArchivoSalirClick(Sender: TObject);
begin
     Close;
end;

procedure TLinxBase.TerminalesClick(Sender: TObject);
begin
     TerminalesSincronizar.Enabled := Recolectando;
     TerminalesSincronizarTodas.Enabled := Recolectando;
     TerminalesVersion.Enabled := Recolectando;
     TerminalesPrograma.Enabled := Recolectando;
     TerminalesAlarma.Enabled := Recolectando;
end;

procedure TLinxBase.TerminalesTodasClick(Sender: TObject);
var
   i: Integer;
begin
     with UnitList do
     begin
          try
             Items.BeginUpdate;
             for i := 0 to ( Items.Count - 1 ) do
                 ItemIndex := i;
          finally
                 Items.EndUpdate;
          end;
     end;
end;

procedure TLinxBase.ConfiguracionClick(Sender: TObject);
begin
     ConfiguracionServidor.Enabled := ( not Recolectando ) and dmPoll.ValidaRegistryWrite;
     ConfiguracionFuncionalidad.Enabled := not Recolectando;
     ConfiguracionAlarmas.Enabled := not Recolectando;
end;

procedure TLinxBase.ConfiguracionServidorClick(Sender: TObject);
begin
     dmPoll.ConfigurarServidor;
end;

procedure TLinxBase.ConfiguracionFuncionalidadClick(Sender: TObject);
begin
     with TFuncionalidad.Create( Self ) do
     begin
          try
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TLinxBase.ConfiguracionAlarmasClick(Sender: TObject);
begin
     with TCapturarAlarmas.Create( Self ) do
     begin
          try
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TLinxBase.AyudaContenidoClick(Sender: TObject);
begin
     Application.HelpCommand( HELP_CONTENTS, 0 );
end;

procedure TLinxBase.AyudaUsandoAyudaClick(Sender: TObject);
begin
     Application.HelpCommand( HELP_HELPONHELP, 0 );
end;

procedure TLinxBase.AyudaAcercaDeClick(Sender: TObject);
begin
     with TPollAcercaDe.Create( Self ) do
     begin
          try
             Producto := Application.Title;
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

function TLinxBase.PuedeRecolectar: Boolean;
begin
     Result := True;
end;

end.
