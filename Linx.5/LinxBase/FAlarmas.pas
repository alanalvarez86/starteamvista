unit FAlarmas;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Mask, Buttons, ExtCtrls, CheckLst,
     LinxIni,
     LinxIniBase,
     ZBaseDlgModal,
     ZetaHora;

type
  TCapturarAlarmas = class(TZetaDlgModal)
    PanelControles: TPanel;
    HoraLBL: TLabel;
    ArchivoLBL: TLabel;
    Archivo: TEdit;
    PanelLista: TPanel;
    Alarmas: TListBox;
    Hora: TZetaHora;
    AlarmaSeek: TSpeedButton;
    Dias: TCheckListBox;
    DiasLBL: TLabel;
    OpenDialog: TOpenDialog;
    Agregar: TBitBtn;
    Borrar: TBitBtn;
    Modificar: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure AgregarClick(Sender: TObject);
    procedure BorrarClick(Sender: TObject);
    procedure ModificarClick(Sender: TObject);
    procedure AlarmasClick(Sender: TObject);
    procedure AlarmaSeekClick(Sender: TObject);
    procedure HayCambios(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
    FRefreshing: Boolean;
    FAlarma: TAlarma;
    FLista: TStringList;
    procedure SeleccionaPrimerAlarma;
    procedure SetAlarmaControls;
    procedure ShowAlarmaInfo;
  public
    { Public declarations }
  end;

var
  CapturarAlarmas: TCapturarAlarmas;

implementation

uses ZetaDialogo;

{$R *.DFM}

procedure TCapturarAlarmas.FormCreate(Sender: TObject);
begin
     inherited;
     FRefreshing := False;
     FAlarma := TAlarma.Create( '' );
     FLista := TStringList.Create;
end;

procedure TCapturarAlarmas.FormShow(Sender: TObject);
var
   i: Integer;
begin
     inherited;
     with LinxIni.IniValues do
     begin
          ListaAlarmas.Cargar( FLista );
     end;
     with Alarmas.Items do
     begin
          BeginUpdate;
          try
             Clear;
             for i := 0 to ( FLista.Count - 1 ) do
             begin
                  FAlarma.Data := FLista.Strings[ i ];
                  Add( FAlarma.Archivo );
             end;
          finally
                 EndUpdate;
          end;
     end;
     SeleccionaPrimerAlarma;
     ShowAlarmaInfo;
     SetAlarmaControls;
     if ( FLista.Count = 0 ) then
        ActiveControl := Agregar;
end;

procedure TCapturarAlarmas.FormDestroy(Sender: TObject);
begin
     FLista.Free;
     FAlarma.Free;
     inherited;
end;

procedure TCapturarAlarmas.SetAlarmaControls;
begin
     Agregar.Enabled := True;
     Borrar.Enabled := ( FLista.Count > 0 );
     Modificar.Enabled := Borrar.Enabled;
end;

procedure TCapturarAlarmas.SeleccionaPrimerAlarma;
begin
     with Alarmas do
     begin
          if ( Items.Count > 0 ) then
             ItemIndex := 0;
     end;
end;

procedure TCapturarAlarmas.ShowAlarmaInfo;
var
   i, j: Integer;
begin
     with Alarmas do
     begin
          i := ItemIndex;
          if ( i >= 0 ) then
          begin
               FRefreshing := True;
               FAlarma.Data := FLista.Strings[ i ];
               with FAlarma do
               begin
                    Self.Archivo.Text := Archivo;
                    Self.Hora.Valor := Hora;
                    with Self.Dias do
                    begin
                         for j := 0 to ( Items.Count - 1 ) do
                         begin
                              Checked[ j ] := GetDay( j );
                         end;
                    end;
               end;
               FRefreshing := False;
               Self.Archivo.Enabled := True;
               Self.ArchivoLBL.Enabled := True;
               Self.AlarmaSeek.Enabled := True;
               Self.Hora.Enabled := True;
               Self.HoraLBL.Enabled := True;
               Self.Dias.Enabled := True;
               Self.DiasLBL.Enabled := True;
          end
          else
          begin
               Self.Archivo.Enabled := False;
               Self.ArchivoLBL.Enabled := False;
               Self.AlarmaSeek.Enabled := False;
               Self.Hora.Enabled := False;
               Self.HoraLBL.Enabled := False;
               Self.Dias.Enabled := False;
               Self.DiasLBL.Enabled := False;
          end;
     end;
end;

procedure TCapturarAlarmas.HayCambios(Sender: TObject);
var
   i: Integer;
begin
     if not FRefreshing then
     begin
          with FAlarma do
          begin
               Archivo := Self.Archivo.Text;
               Hora := Self.Hora.Valor;
               with Self.Dias do
               begin
                    for i := 0 to ( Items.Count - 1 ) do
                    begin
                         SetDay( i, Checked[ i ] );
                    end;
               end;
               with Alarmas do
               begin
                    FLista.Strings[ ItemIndex ] := Data;
                    Items.Strings[ ItemIndex ] := Archivo;
               end;
          end;
     end;
end;

procedure TCapturarAlarmas.AlarmasClick(Sender: TObject);
begin
     inherited;
     ShowAlarmaInfo;
end;

procedure TCapturarAlarmas.AlarmaSeekClick(Sender: TObject);
begin
     inherited;
     with OpenDialog do
     begin
          with Archivo do
          begin
               FileName := Text;
               if Execute then
                  Text := FileName;
          end;
     end;
end;

procedure TCapturarAlarmas.AgregarClick(Sender: TObject);
begin
     inherited;
     with FAlarma do
     begin
          Data := '';
          FLista.Add( Data );
     end;
     with Alarmas do
     begin
          with Items do
          begin
               Add( FAlarma.Archivo );
               ItemIndex := ( Count - 1 );
          end;
     end;
     SetAlarmaControls;
     Modificar.Click;
end;

procedure TCapturarAlarmas.BorrarClick(Sender: TObject);
var
   i: Integer;
begin
     inherited;
     with Alarmas do
     begin
          i := ItemIndex;
          if ( i >= 0 ) then
          begin
               Items.Delete( i );
               with FLista do
               begin
                    Delete( i );
                    if ( i >= Count ) then
                       i := ( i - 1 );
               end;
               ItemIndex := i;
               ShowAlarmaInfo;
               SetAlarmaControls;
          end;
     end;
end;

procedure TCapturarAlarmas.ModificarClick(Sender: TObject);
begin
     inherited;
     ShowAlarmaInfo;
     with Archivo do
     begin
          if CanFocus and Visible then
             SetFocus;
     end;
end;

procedure TCapturarAlarmas.OKClick(Sender: TObject);
begin
     inherited;
     with LinxIni.IniValues do
     begin
          ListaAlarmas.Descargar( FLista );
     end;
end;

end.
