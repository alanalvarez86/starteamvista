unit FViewLog;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     DBCtrls, StdCtrls, Buttons, ExtCtrls, Grids, Db;

type
  TViewLog = class(TForm)
    PanelInferior: TPanel;
    Salir: TBitBtn;
    Exportar: TBitBtn;
    SaveDialog: TSaveDialog;
    Bitacora: TMemo;
    procedure FormShow(Sender: TObject);
    procedure ExportarClick(Sender: TObject);
  private
    { Private declarations }
    FLogDataset: TDataset;
  public
    { Public declarations }
    property LogDataset: TDataset read FLogDataset write FLogDataset;
  end;

var
  ViewLog: TViewLog;

implementation

{$R *.DFM}

procedure TViewLog.FormShow(Sender: TObject);
var
   pValor, pStart: PChar;
   sError, sValor: String;
begin
     with Bitacora.Lines do
     begin
          BeginUpdate;
          try
             Clear;
             with LogDataset do
             begin
                  First;
                  while not Eof do
                  begin
                       Add( FieldByName( 'POLL_DATA' ).AsString + ' : ' );
                       sError := FieldByName( 'POLL_ERR' ).AsString;
                       pValor := Pointer( sError );
                       if ( pValor <> nil ) then
                       begin
                            while ( pValor^ <> #0 ) do
                            begin
                                 pStart := pValor;
                                 while not ( pValor^ in [ #0, #10, #13 ] ) do
                                 begin
                                      Inc( pValor );
                                 end;
                                 SetString( sValor, pStart, ( pValor - pStart ) );
                                 if ( sValor <> '' ) then
                                    Add( '    ' + sValor );
                                 if ( pValor^ = #13 ) then
                                    Inc( pValor );
                                 if ( pValor^ = #10 ) then
                                    Inc( pValor );
                            end;
                       end;
                       Next;
                  end;
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TViewLog.ExportarClick(Sender: TObject);
begin
     with SaveDialog do
     begin
          if Execute then
             Bitacora.Lines.SaveToFile( FileName );
     end;
end;

end.
