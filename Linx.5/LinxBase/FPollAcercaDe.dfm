inherited PollAcercaDe: TPollAcercaDe
  Left = 245
  Caption = 'Acerca De...'
  ClientHeight = 344
  ClientWidth = 369
  PixelsPerInch = 96
  TextHeight = 16
  inherited PageControl: TPageControl
    Width = 369
    Height = 311
    inherited TabVersion: TTabSheet
      inherited PanelDatos: TPanel
        Width = 361
        Height = 283
        inherited VersionBuild: TLabel
          Left = 65
          Width = 225
        end
        inherited Copyright: TLabel
          Left = 52
          Top = 167
          Width = 251
        end
        inherited Comments: TLabel
          Left = 80
          Top = 185
          Width = 193
          Caption = 'Derechos Reservados 1996-2011'
        end
        inherited ProductName: TLabel
          Left = 120
          Width = 113
          Caption = 'L5Poll'
        end
        inherited Logo: TImage
          Left = 161
          Top = 111
          Picture.Data = {
            055449636F6E0000010001002020100000000000E80200001600000028000000
            2000000040000000010004000000000080020000000000000000000000000000
            0000000000000000000080000080000000808000800000008000800080800000
            C0C0C000808080000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000
            FFFFFF00000000000000000000000000000000000CCCCCCCCCCCCCCCCCCCCCCC
            CCCCCCC00CCCCCCCCCCCCCCCCCCCCCCCCCCCCCC00CCCCCCCCCCCCCCCCCCCCCCC
            CCCCCCC00CCCCCCCCCCCCCCCCCCCCCCCCCCCCCC00CCCCCCCCCCCC00000000CCC
            CCCCCCC00CCCCCCCCC0009B9FFFF0000CCCCCCC00CCCCCCCC09B9B9BFFFFF000
            0CCCCCC00CCCCCCCC0B9B9B9FF55FF000CCCCCC00CCC0000009B9FFFFF55FF50
            0CCCCCC00CCC000000B9FFFFFF55FF550CCCCCC00CCC00CCC09FFF555555F555
            0CCCCCC00CCC0CCCC0BFFF55555555550CCCCCC00CCC0CCCC00FFF5555555555
            0CCCCCC00CCC0CCCC000FFFF555555550CCCCCC00CCC0CCCC0000FFF55555555
            0CCCCCC00CCC0CCCC0000055555555550CCCCCC00CCC0CCCC000000555555555
            0CCCCCC00CCC00CCC0000000555555550CCCCCC00CCC00000000000005555555
            0CCCCCC00CCC000000000000005555550CCCCCC00CCCCCCCC000000000000555
            0CCCCCC00CCCCCCCC0000000000000050CCCCCC00CCCCCCCC000000000000000
            0CCCCCC00CCCCCCCCCC00F000000000CCCCCCCC00CCCCCCCCCCCC00000000CCC
            CCCCCCC00CCCCCCCCCCCFCCCCFCCCFCCCCCCCCC00CCCCCCCCCCCCCCCCCFCCCCC
            CCCCCCC00CCCCCCCCCCFCCCCCCCCFCCCCCCCCCC00CCCCCCCCCCCFCCCCFCCCFCC
            CCCCCCC00CCCCCCCCCCCFCCCCFCCCFCCCCCCCCC0000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            00000000}
        end
        inherited LblBuild: TLabel
          Left = 15
        end
        object Titulo: TLabel
          Left = 51
          Top = 76
          Width = 252
          Height = 13
          Alignment = taCenter
          AutoSize = False
          Caption = 'Titulo'
        end
      end
    end
    inherited TabRecursos: TTabSheet
      inherited DiscoGB: TGroupBox
        Width = 361
        Height = 164
      end
      inherited MemoriaGB: TGroupBox
        Width = 361
      end
    end
    inherited AutoData: TTabSheet
      inherited PanelAuto: TPanel
        Width = 361
        inherited Empleados: TLabel
          Width = 149
        end
      end
      inherited PanelModulos: TPanel
        Width = 361
        Height = 186
        inherited ModulosGB: TGroupBox
          Width = 167
          Height = 186
          inherited Modulos: TCheckListBox
            Width = 163
            Height = 169
            Enabled = True
          end
        end
        inherited Caducidad: TGroupBox
          Left = 167
          Width = 194
          Height = 186
          inherited Prestamos: TCheckListBox
            Width = 190
            Height = 169
            Enabled = True
          end
        end
      end
    end
  end
  inherited PanelInferior: TPanel
    Top = 311
    Width = 369
    inherited OK: TBitBtn
      Left = 288
    end
  end
end
