unit FPollAcercaDe;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ExtCtrls, StdCtrls, Buttons, ComCtrls, CheckLst,
     FAcercaDe;

type
  TPollAcercaDe = class(TAcercaDe)
    Titulo: TLabel;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure SetProducto(const Value: String);
  public
    { Public declarations }
    property Producto: String write SetProducto;
  end;

var
  PollAcercaDe: TPollAcercaDe;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses;

{$R *.DFM}

procedure TPollAcercaDe.FormShow(Sender: TObject);
begin
     inherited;
     Titulo.Caption := Application.MainForm.Caption;
end;

procedure TPollAcercaDe.SetProducto(const Value: String);
begin
     ProductName.Caption := Value;
end;

end.
