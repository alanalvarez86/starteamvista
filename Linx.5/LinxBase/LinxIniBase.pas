unit LinxIniBase;

interface

uses Classes, IniFiles, SysUtils, Forms, Dialogs;

const
     INI_L5POLL = 'L5Poll';
     INI_ARCHIVOS = 'Archivos';
     IP_ADDRESS_LENGTH = 20;
     K_SI = 'SI';
     K_NO = 'NO';
     K_DEFAULT_TXT = 'DEFAULT';
     K_DEFAULT_NUM = -1;
     aSiNo: array[ False..True ] of pChar = ( K_NO, K_SI );

type
  TBit = 0..7;
  TBinValue = Byte;
  TUnitID = String[ 2 ];
  TIdentificaID = String [ 4 ];
  TIPAddress = String[ IP_ADDRESS_LENGTH ];
  TSendAlarm = procedure( const sFileName: String ) of object;
  TAlarma = class( TObject )
  private
    { Private declarations }
    FArchivo: String;
    FHora: String;
    FDias: TBinValue;
    FHourMax: TDateTime;
    FMomento: TDateTime;
    function GetData: String;
    function GetEnDomingo: Boolean;
    function GetEnJueves: Boolean;
    function GetEnLunes: Boolean;
    function GetEnMartes: Boolean;
    function GetEnMiercoles: Boolean;
    function GetEnSabado: Boolean;
    function GetEnViernes: Boolean;
    procedure SetData(const Value: String);
    procedure SetEnDomingo(const Value: Boolean);
    procedure SetEnJueves(const Value: Boolean);
    procedure SetEnLunes(const Value: Boolean);
    procedure SetEnMartes(const Value: Boolean);
    procedure SetEnMiercoles(const Value: Boolean);
    procedure SetEnSabado(const Value: Boolean);
    procedure SetEnViernes(const Value: Boolean);
    procedure SetHora( const Value: String );
  public
    { Public declarations }
    constructor Create( const sData: String );
    property Archivo: String read FArchivo write FArchivo;
    property Hora: String read FHora write SetHora;
    property Data: String read GetData write SetData;
    property Dias: TBinValue read FDias;
    property EnDomingo: Boolean read GetEnDomingo write SetEnDomingo;
    property EnLunes: Boolean read GetEnLunes write SetEnLunes;
    property EnMartes: Boolean read GetEnMartes write SetEnMartes;
    property EnMiercoles: Boolean read GetEnMiercoles write SetEnMiercoles;
    property EnJueves: Boolean read GetEnJueves write SetEnJueves;
    property EnViernes: Boolean read GetEnViernes write SetEnViernes;
    property EnSabado: Boolean read GetEnSabado write SetEnSabado;
    function CheckInterval( const dStart, dEnd: TDateTime ): Boolean;
    function GetDay(const iDayOfWeek: Byte): Boolean;
    procedure SetDay(const iDayOfWeek: Byte; const lValue: Boolean);
  end;
  TAlarmList = class( TObject )
  private
    { Private declarations }
    FItems: TList;
    function GetAlarma(Index: Integer): TAlarma;
    function GetAlarmasFileName: String;
    function Agrega(const sData: String): TAlarma;
    procedure Delete(const Index: Integer);
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Alarma[ Index: Integer ]: TAlarma read GetAlarma;
    procedure Cargar( Lista: TStrings );
    procedure Clear;
    procedure Descargar( Lista: TStrings );
    procedure Init;
    procedure Write;
    procedure CheckInterval( const dInit: TDateTime; Metodo: TSendAlarm );
  end;
  TIniValues = class( TObject )
  private
    { Private declarations }
    FIniFile: TIniFile;
    FAlarmList: TAlarmList;
    function GetAlarma: String;
    function GetEmpleados: String;
    function GetIntervalo: LongInt;
    function GetPrograma: String;
    function GetDirPausa: String;
    function GetDirReinicio: String;
    function GetTimeOut: integer;
    {}
    function GetArchivoAdicional: String;
    function GetCampo: String;
    function GetEmpresa: String;
    function GetEnviarChecadas: Boolean;
    function GetProcesarTarjetas: Boolean;
    function GetTransformarNumero: Boolean;
    {}
    procedure PutAlarma( const Valor: String );
    procedure PutEmpleados( const Valor: String );
    procedure PutIntervalo( const Valor: LongInt );
    procedure PutPrograma( const Valor: String );
    procedure PutDirPausa( const Valor: String );
    procedure PutDirReinicio( const Valor: String );
    procedure PutTimeOut( const Valor: Integer );
    {}
    procedure SetArchivoAdicional(const Value: String);
    procedure SetEnviarChecadas(const Value: Boolean);
    procedure SetProcesarTarjetas(const Value: Boolean);
    procedure SetCampo(const Value: String);
    procedure SetEmpresa(const Value: String);
    procedure SetTransformarNumero(const Value: Boolean);
  protected
    { Protected declarations }
    property IniFile: TIniFile read FIniFile;
    function ReadBoolean( const sSection, sIdentifier: String; const lDefault: Boolean ): Boolean;
    procedure WriteBoolean( const sSection, sIdentifier: String; const lValue: Boolean );
  public
    { Public declarations }
    constructor Create( const sIniFileName: String );
    destructor Destroy; override;
    property ListaAlarmas: TAlarmList read FAlarmList;
    property Intervalo: LongInt read GetIntervalo write PutIntervalo;
    property Programa: String read GetPrograma write PutPrograma;
    property Alarma: String read GetAlarma write PutAlarma;
    property Empleados: String read GetEmpleados write PutEmpleados;
    property DirPausa: String read GetDirPausa write PutDirPausa;
    property DirReinicio: String read GetDirReinicio write PutDirReinicio;
    property TimeOut: integer read GetTimeOut write PutTimeOut;

    {}
    property ArchivoAdicional: String read GetArchivoAdicional write SetArchivoAdicional;
    property Campo: String read GetCampo write SetCampo;
    property Empresa: String read GetEmpresa write SetEmpresa;
    property EnviarChecadas: Boolean read GetEnviarChecadas write SetEnviarChecadas;
    property ProcesarTarjetas: Boolean read GetProcesarTarjetas write SetProcesarTarjetas;
    property TransformarNumero: Boolean read GetTransformarNumero write SetTransformarNumero;
    {}
    function GetAddressLength: Integer;
  end;

function StringValue( const Value: String ): String;
function IntegerValue( const Value: String ): Integer;
function ValidPath( const sPath: String ): String;
function strToken( var sValue: String; const cSeparator: Char ): String;
function strPadChL( const sValue: String; const cValue: Char; const iLen: Integer): String;
function strPadChR( const sValue: String; const cValue: Char; const iLen: Integer): String;
function strTrimChL( const sValue: String; const cValue: Char ): String;
function strTrimChR( const sValue: String; const cValue: Char): String;
function GetAlarmData( const iDays: TBinValue; const sHora, sArchivo: String ): String;
function GetIPSubAddress( var sData: String ): String;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses,
     FLinxTools;

const
     Archivos_App = 'Programa';
     Archivos_Data = 'Alarma';
     Archivos_Empleados = 'Empleados';
     Archivos_DirPausa = 'DirPausa';
     Archivos_DirReinicio = 'DirReinicio';
     Archivos_TimeOut = 'TimeOut';

     L5Poll_Archivo_Adicional = 'Archivo Adicional';
     L5Poll_Enviar_Checadas = 'Enviar Checadas';
     L5Poll_Procesar_Tarjetas = 'Procesar Tarjetas';
     L5Poll_Intervalo = 'Intervalo';
     INI_TRANSFORMACION = 'TRANSFORMACION';
     L5Poll_TRANSFORMACION_CAMPO = 'Campo COLABORA';
     L5Poll_TRANSFORMACION_Empresa = 'Empresa';
     L5Poll_TRANSFORMAR_NUMERO = 'Transformar Numero';
     L5Poll_PROXIMIDAD = 'Proximidad';

{ ****** Funciones de Uso General ********* }

function ValidValue( Value: String; var iValue: Integer ): Boolean;
var
   iCode: Integer;
begin
     Val( Value, iValue, iCode );
     Result := ( iCode = 0 ) and ( iValue > 0 );
end;

function StringValue( const Value: String ): String;
var
   iValue: Integer;
begin
     if ValidValue( Value, iValue ) then
        Result := Value
     else
         Result := K_DEFAULT_TXT;
end;

function IntegerValue( const Value: String ): Integer;
var
   iValue: Integer;
begin
     if ValidValue( Value, iValue ) then
        Result := iValue
     else
         Result := K_DEFAULT_NUM;
end;

function ValidPath( const sPath: String ): String;
begin
     Result := sPath;
     if ( Copy( Result, Length( Result ), 1 ) <> '\' ) then
        Result := Result + '\';
end;

function strToken( var sValue: String; const cSeparator: Char ): String;
var
   i: Word;
begin
     i := Pos( cSeparator, sValue );
     if ( i <> 0 ) then
     begin
          Result := System.Copy( sValue, 1, ( i - 1 ) );
          System.Delete( sValue, 1, i );
     end
     else
     begin
          Result := sValue;
          sValue := '';
     end;
end;

function strPadChL( const sValue: String; const cValue: Char; const iLen: Integer): String;
begin
     Result := sValue;
     while ( Length( Result ) < iLen ) do
     begin
          Result := cValue + Result;
     end;
end;

function strPadChR( const sValue: String; const cValue: Char; const iLen: Integer): String;
begin
     Result := sValue;
     while ( Length( Result ) < iLen ) do
     begin
          Result := Result + cValue;
     end;
end;

function strTrimChL( const sValue: String; const cValue: Char ): String;
begin
     Result := sValue;
     while ( Length( Result ) > 0 ) and ( Result[ 1 ] = cValue ) do
     begin
          Delete( Result, 1, 1 );
     end;
end;

function strTrimChR( const sValue: String; const cValue: Char): String;
begin
     Result := sValue;
     while ( Length( Result ) > 0 ) and ( Result[ Length( Result ) ] = cValue ) do
     begin
          Delete( Result, Length( Result ), 1 );
     end;
end;

function StrResiduo( const sData: String; const iPos: Integer ): String;
begin
     Result := Copy( sData, ( iPos + 1 ), ( Length( sData ) - iPos ) );
end;

function GetIPSubAddress( var sData: String ): String;
var
   iPos: Integer;
begin
     iPos := Pos( '.', sData );
     if ( iPos > 0 ) then
     begin
          Result := StrPadChL( Copy( sData, 1, ( iPos - 1 ) ), '0', 3 );
          sData := StrResiduo( sData, iPos );
     end
     else
     begin
          iPos := Pos( ' ', sData );
          if ( iPos > 0 ) then
          begin
               Result := StrPadChL( Copy( sData, 1, ( iPos - 1 ) ), '0', 3 );
               sData := Trim( StrResiduo( sData, iPos ) );
          end
          else
              Result := '';
     end;
end;

function BitSet( const Value: TBinValue; const WhatBit: TBit ): Boolean;
begin
     Result:= ( ( Value and ( 1 shl WhatBit ) ) <> 0 );
end;

function BitOn( const Value: TBinValue; const WhatBit: TBit ): TBinValue;
begin
     Result := Value or ( 1 shl WhatBit );
end;

function BitOff( const Value: TBinValue; const WhatBit: TBit ): TBinValue;
begin
     Result := Value and ( ( 1 shl WhatBit ) xor $FF );
end;

function GetAlarmData( const iDays: TBinValue; const sHora, sArchivo: String ): String;
begin
     Result := Format( '%3.3d %s %s', [ iDays, sHora, sArchivo ] );
end;

{ ********** TAlarma ********* }

constructor TAlarma.Create(const sData: String );
begin
     SetData( sData );
     FHourMax := EncodeTime( 23, 59, 59, 999 );
end;

function TAlarma.GetData: String;
begin
     Result := GetAlarmData( FDias, FHora, FArchivo );
end;

procedure TAlarma.SetData( const Value: String );
begin
     if ( Value = '' ) then
     begin
          FDias := 0;
          SetHora( '0000' );
          FArchivo := 'Alarma.val';
     end
     else
     begin
          FDias := StrToIntDef( Copy( Value, 1, 3 ), 0 );
          SetHora( Copy( Value, 5, 4 ) );
          FArchivo := Copy( Value, 10, ( Length( Value ) - 9 ) );
     end;
end;

function TAlarma.GetDay( const iDayOfWeek: Byte ): Boolean;
begin
     Result := BitSet( FDias, iDayOfWeek );
end;

procedure TAlarma.SetDay( const iDayOfWeek: Byte; const lValue: Boolean );
begin
     if lValue then
        FDias := BitOn( FDias, iDayOfWeek )
     else
         FDias := BitOff( FDias, iDayOfWeek );
end;

procedure TAlarma.SetHora( const Value: String );
begin
     FHora := Value;
     try
        FMomento := EncodeTime( StrToIntDef( Copy( Value, 1, 2 ), 0 ), StrToIntDef( Copy( Value, 3, 2 ), 0 ), 0, 0 );
     except
           on Error: Exception do
           begin
                FMomento := 0;
                FHora := '';
           end;
     end;
end;

function TAlarma.GetEnDomingo: Boolean;
begin
     Result := GetDay( 1 );
end;

function TAlarma.GetEnLunes: Boolean;
begin
     Result := GetDay( 2 );
end;

function TAlarma.GetEnMartes: Boolean;
begin
     Result := GetDay( 3 );
end;

function TAlarma.GetEnMiercoles: Boolean;
begin
     Result := GetDay( 4 );
end;

function TAlarma.GetEnJueves: Boolean;
begin
     Result := GetDay( 5 );
end;

function TAlarma.GetEnViernes: Boolean;
begin
     Result := GetDay( 6 );
end;

function TAlarma.GetEnSabado: Boolean;
begin
     Result := GetDay( 7 );
end;

procedure TAlarma.SetEnDomingo(const Value: Boolean);
begin
     SetDay( 1, Value );
end;

procedure TAlarma.SetEnLunes(const Value: Boolean);
begin
     SetDay( 2, Value );
end;

procedure TAlarma.SetEnMartes(const Value: Boolean);
begin
     SetDay( 3, Value );
end;

procedure TAlarma.SetEnMiercoles(const Value: Boolean);
begin
     SetDay( 4, Value );
end;

procedure TAlarma.SetEnJueves(const Value: Boolean);
begin
     SetDay( 5, Value );
end;

procedure TAlarma.SetEnViernes(const Value: Boolean);
begin
     SetDay( 6, Value );
end;

procedure TAlarma.SetEnSabado(const Value: Boolean);
begin
     SetDay( 7, Value );
end;

function TAlarma.CheckInterval( const dStart, dEnd: TDateTime ): Boolean;
var
   iDayStart, iDayEnd: Word;

function GetDecimals( const dValue: TDateTime ): TDateTime;
begin
     Result := dValue - Trunc( dValue );
end;

function CheckHour( const dMin, dMax: TDateTime ): Boolean;
begin
     Result := ( FMomento >= GetDecimals( dMin ) ) and ( FMomento <= GetDecimals( dMax ) );
end;

begin
     iDayStart := DayOfWeek( dStart );
     iDayEnd := DayOfWeek( dEnd );
     if ( iDayStart = iDayEnd ) then
     begin
          if GetDay( iDayStart ) then
             Result := CheckHour( dStart, dEnd )
          else
              Result := False;
     end
     else
     begin
          if GetDay( iDayStart ) then
             Result := CheckHour( dStart, FHourMax )
          else
              if GetDay( iDayEnd ) then
                 Result := CheckHour( 0, dEnd )
              else
                  Result := False;
     end;
end;

{ ***** TAlarmList ***** }

constructor TAlarmList.Create;
begin
     FItems := TList.Create;
     inherited Create;
end;

destructor TAlarmList.Destroy;
begin
     Clear;
     FreeAndNil( FItems );
     inherited Destroy;
end;

function TAlarmList.GetAlarma( Index: Integer ): TAlarma;
begin
     Result := TAlarma( FItems.Items[ Index ] );
end;

procedure TAlarmList.Clear;
var
   i: Integer;
begin
     for i := ( FItems.Count - 1 ) downto 0 do
     begin
          Delete( i );
     end;
end;

function TAlarmList.Agrega( const sData: String ): TAlarma;
begin
     Result := TAlarma.Create( sData );
     FItems.Add( Result );
end;

procedure TAlarmList.Delete( const Index: Integer );
begin
     Alarma[ Index ].Free;
     FItems.Delete( Index );
end;

function TAlarmList.GetAlarmasFileName: String;
const
     ALARMA_FILE_NAME = 'L5ALARMA.INI';
begin
     Result := FLinxTools.SetFileNameDefaultPath( ALARMA_FILE_NAME );
end;

procedure TAlarmList.Init;
var
   UnitFile: TextFile;
   sData: String;
begin
     if FileExists( GetAlarmasFileName ) then
     begin
          AssignFile( UnitFile, GetAlarmasFileName );
          Reset( UnitFile );
          while not Eof( UnitFile ) do
          begin
               Readln( UnitFile, sData );
               Agrega( sData );
          end;
          CloseFile( UnitFile );
     end;
end;

procedure TAlarmList.Write;
var
   i: Integer;
   UnitFile: TextFile;
begin
     AssignFile( UnitFile, GetAlarmasFileName );
     Rewrite( UnitFile );
     for i := 0 to ( FItems.Count - 1 ) do
     begin
          Writeln( UnitFile, Alarma[ i ].Data );
     end;
     CloseFile( UnitFile );
end;

procedure TAlarmList.Cargar(Lista: TStrings);
var
   i: Integer;
begin
     with Lista do
     begin
          BeginUpdate;
          try
             Clear;
             for i := 0 to ( FItems.Count - 1 ) do
             begin
                  Add( Alarma[ i ].Data );
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TAlarmList.Descargar(Lista: TStrings);
var
   i: Integer;
begin
     Clear;
     with Lista do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               Agrega( Strings[ i ] );
          end;
     end;
     Write;
end;

procedure TAlarmList.CheckInterval( const dInit: TDateTime; Metodo: TSendAlarm );
var
   i: Integer;
begin
     with FItems do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               with Alarma[ i ] do
               begin
                    if CheckInterval( dInit, Now ) then
                    begin
                         Metodo( Archivo );
                    end;
               end;
          end;
     end;
end;

{ ***** TIniValues ******** }

constructor TIniValues.Create( const sIniFileName: String );
begin
     FIniFile := TIniFile.Create( SetFileNameDefaultPath( sIniFileName ) );
     FAlarmList := TAlarmList.Create;
     FAlarmList.Init;
end;

destructor TIniValues.Destroy;
begin
     FAlarmList.Free;
     FIniFile.Free;
end;

function TIniValues.GetAddressLength: Integer;
begin
     Result := IP_ADDRESS_LENGTH;
end;

function TIniValues.ReadBoolean( const sSection, sIdentifier: String; const lDefault: Boolean ): Boolean;
begin
     Result := ( FIniFile.ReadString( sSection, sIdentifier, aSiNo[ lDefault ] ) = K_SI );
end;

procedure TIniValues.WriteBoolean( const sSection, sIdentifier: String; const lValue: Boolean );
begin
     FIniFile.WriteString( sSection, sIdentifier, aSiNo[ lValue ] );
end;

{ *** M�todos Read / Write de las propiedades *** }

function TIniValues.GetIntervalo: LongInt;
begin
     Result := FIniFile.ReadInteger( INI_L5POLL, L5Poll_Intervalo, 30 );
end;

procedure TIniValues.PutIntervalo( const Valor: LongInt );
begin
     FIniFile.WriteInteger( INI_L5POLL, L5Poll_Intervalo, Valor );
end;

function TIniValues.GetPrograma: String;
begin
     Result := FIniFile.ReadString( INI_ARCHIVOS, Archivos_App, '' );
end;

procedure TIniValues.PutPrograma( const Valor: String );
begin
     FIniFile.WriteString( INI_ARCHIVOS, Archivos_App, Valor );
end;

function TIniValues.GetAlarma: String;
begin
     Result := FIniFile.ReadString( INI_ARCHIVOS, Archivos_Data, '' );
end;

procedure TIniValues.PutAlarma( const Valor: String );
begin
     FIniFile.WriteString( INI_ARCHIVOS, Archivos_Data, Valor );
end;

function TIniValues.GetEmpleados: String;
begin
     Result := FIniFile.ReadString( INI_ARCHIVOS, Archivos_Empleados, '' );
end;

procedure TIniValues.PutEmpleados( const Valor: String );
begin
     FIniFile.WriteString( INI_ARCHIVOS, Archivos_Empleados, Valor );
end;

function TIniValues.GetDirPausa: String;
begin
     Result := FIniFile.ReadString( INI_ARCHIVOS, Archivos_DirPausa, '' );
end;

procedure TIniValues.PutDirPausa( const Valor: String );
begin
     FIniFile.WriteString( INI_ARCHIVOS, Archivos_DirPausa, Valor );
end;

function TIniValues.GetDirReinicio: String;
begin
     Result := FIniFile.ReadString( INI_ARCHIVOS, Archivos_DirReinicio, '' );
end;

procedure TIniValues.PutDirReinicio( const Valor: String );
begin
     FIniFile.WriteString( INI_ARCHIVOS, Archivos_DirReinicio, Valor );
end;

function TIniValues.GetTimeOut: integer;
begin
     Result := FIniFile.ReadInteger( INI_ARCHIVOS, Archivos_TimeOut, 120 );
end;

procedure TIniValues.PutTimeOut( const Valor: integer );
begin
     FIniFile.WriteInteger( INI_ARCHIVOS, Archivos_TimeOut, Valor );
end;

function TIniValues.GetArchivoAdicional: String;
begin
     Result := IniFile.ReadString( INI_ARCHIVOS, L5Poll_ARCHIVO_ADICIONAL, '' );
end;

function TIniValues.GetCampo: String;
begin
     Result := IniFile.ReadString( INI_TRANSFORMACION, L5Poll_TRANSFORMACION_CAMPO, 'CB_G_TEX_4' );
end;

function TIniValues.GetEmpresa: String;
begin
     Result := IniFile.ReadString( INI_TRANSFORMACION, L5Poll_TRANSFORMACION_EMPRESA, '' );
end;

function TIniValues.GetEnviarChecadas: Boolean;
begin
     Result := ReadBoolean( INI_L5POLL, L5Poll_Enviar_Checadas, True );
end;

function TIniValues.GetProcesarTarjetas: Boolean;
begin
     Result := ReadBoolean( INI_L5POLL, L5Poll_Procesar_Tarjetas, True );
end;

function TIniValues.GetTransformarNumero: Boolean;
begin
     Result := ReadBoolean( INI_TRANSFORMACION, L5Poll_TRANSFORMAR_NUMERO, False );
end;

procedure TIniValues.SetArchivoAdicional(const Value: String);
begin
     IniFile.WriteString( INI_ARCHIVOS, L5Poll_ARCHIVO_ADICIONAL, Value );
end;

procedure TIniValues.SetCampo(const Value: String);
begin
     IniFile.WriteString( INI_TRANSFORMACION, L5Poll_TRANSFORMACION_CAMPO, Value );
end;

procedure TIniValues.SetEmpresa(const Value: String);
begin
     IniFile.WriteString( INI_TRANSFORMACION, L5Poll_TRANSFORMACION_EMPRESA, Value );
end;

procedure TIniValues.SetEnviarChecadas(const Value: Boolean);
begin
     WriteBoolean( INI_L5POLL, L5Poll_Enviar_Checadas, Value );
end;

procedure TIniValues.SetProcesarTarjetas(const Value: Boolean);
begin
     WriteBoolean( INI_L5POLL, L5Poll_Procesar_Tarjetas, Value );
end;

procedure TIniValues.SetTransformarNumero(const Value: Boolean);
begin
     WriteBoolean( INI_TRANSFORMACION, L5Poll_TRANSFORMAR_NUMERO, Value );
end;

end.
