unit FImportar;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, ComCtrls;

type
  TImportarTarjetas = class(TForm)
    OpenDialog: TOpenDialog;
    ArchivoLBL: TLabel;
    Archivo: TEdit;
    BuscarArchivo: TSpeedButton;
    Importar: TBitBtn;
    Salir: TBitBtn;
    Procesar: TCheckBox;
    procedure BuscarArchivoClick(Sender: TObject);
    procedure ImportarClick(Sender: TObject);
  private
    { Private declarations }
    function GetImportFile: String;
    function GetProcesarTarjetas: Boolean;
  public
    { Public declarations }
    property ImportFile: String read GetImportFile;
    property ProcesarTarjetas: Boolean read GetProcesarTarjetas;
  end;

var
  ImportarTarjetas: TImportarTarjetas;

implementation

uses ZetaDialogo;

{$R *.DFM}

procedure TImportarTarjetas.BuscarArchivoClick(Sender: TObject);
begin
     with OpenDialog do
     begin
          FileName := ExtractFileName( Archivo.Text );
          InitialDir := ExtractFilePath( Archivo.Text );
          if Execute then
             Archivo.Text := FileName;
     end;
end;

function TImportarTarjetas.GetImportFile: String;
begin
     Result := Archivo.Text;
end;

function TImportarTarjetas.GetProcesarTarjetas: Boolean;
begin
     Result := Procesar.Checked;
end;

procedure TImportarTarjetas.ImportarClick(Sender: TObject);
begin
     if FileExists( ImportFile ) then
        ModalResult := mrOK
     else
     begin
          ZetaDialogo.zError( '� Archivo No Existe !', 'El Archivo Para Importar No Existe', 0 );
          ActiveControl := Archivo;
     end;
end;

end.
