unit FAcercaDe;

interface

uses Windows, WinTypes, WinProcs, Classes, Graphics, Forms, Controls, StdCtrls,
     Buttons, ExtCtrls, ComCtrls, SysUtils, CheckLst;

type
  TAcercaDe = class(TForm)
    PageControl: TPageControl;
    TabVersion: TTabSheet;
    TabRecursos: TTabSheet;
    DiscoGB: TGroupBox;
    DiscoTotalLBL: TLabel;
    DiscoDisponible: TLabel;
    DiscoTotal: TLabel;
    DiscoDisponibleLBL: TLabel;
    DiscoOcupadoLBL: TLabel;
    DiscoOcupado: TLabel;
    MemoriaGB: TGroupBox;
    MemoriaRAMLBL: TLabel;
    MemoriaRAM: TLabel;
    RecursosLibresLBL: TLabel;
    RecursosLibres: TLabel;
    MemoriaRAMDisponible: TLabel;
    MemoriaRAMDisponibleLBL: TLabel;
    MemoriaVirtualLBL: TLabel;
    MemoriaVirtual: TLabel;
    MemoriaVirtualDisponible: TLabel;
    MemoriaVirtualDisponibleLBL: TLabel;
    PanelDatos: TPanel;
    VersionBuild: TLabel;
    Copyright: TLabel;
    Comments: TLabel;
    ProductName: TLabel;
    Logo: TImage;
    AutoData: TTabSheet;
    PanelInferior: TPanel;
    OK: TBitBtn;
    PanelAuto: TPanel;
    SerialNumberLBL: TLabel;
    EmpresaLBL: TLabel;
    EmpleadosLBL: TLabel;
    UsuariosLBL: TLabel;
    Vencimiento: TLabel;
    Usuarios: TLabel;
    Empresa: TLabel;
    Empleados: TLabel;
    SerialNumber: TLabel;
    VencimientoLBL: TLabel;
    Version: TLabel;
    VersionLBL: TLabel;
    EsKit: TCheckBox;
    PanelModulos: TPanel;
    ModulosGB: TGroupBox;
    Modulos: TCheckListBox;
    Caducidad: TGroupBox;
    Prestamos: TCheckListBox;
    LblBuild: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Plataforma: TLabel;
    lblPlataforma: TLabel;
    lblBaseDatos: TLabel;
    Label4: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure LlenaPageAutorizacion;
  public
    { Public declarations }
  end;
{$ifdef VER130}
  ZetaPchar = Pchar;
{$else}
  ZetaPChar = string;
{$endif}

var
  AcercaDe: TAcercaDe;

implementation

uses ZetaCommonClasses,
     FAutoClasses,
     ZetaWinAPITools;

{$R *.DFM}

{$ifdef ANTES}
function BytesAMegas( const Bytes: Integer ): Real;
begin
     Result := Bytes / ( 1024 * 1024 );
end;
{$endif}

function GetApplicationVersionNumber: String;
type
    VersionInfo = record
      MajorVersion: Word;
      MinorVersion: Word;
      Release: Word;
      Build: Word;
    end;

function HighWord( Value: DWord ): Word;
begin
     Result := ( ( Value and $FFFF0000 ) shr $10 );
end;

function LowWord( Value: DWord ): Word;
begin
     Result := ( Value and $0000FFFF );
end;

function GetVersionInfo: VersionInfo;
var
   Size, Value: DWord;
   pFileName: PChar;
   sFileName: String;
   Data, Info: Pointer;
   iLen: UINT;
begin
     sFileName := Application.ExeName;
     pFileName := StrAlloc( Length( sFileName ) + 1 );
     try
        pFileName := StrPCopy( pFileName, sFileName );
        Size := Windows.GetFileVersionInfoSize( pFileName, Value );
        if ( Size > 0 ) then
        begin
             GetMem( Data, Size );
             try
                if Windows.GetFileVersionInfo( pFileName, Value, Size, Data ) then
                begin
                     if Windows.VerQueryValue( Data, '\', Info, iLen ) then
                     begin
                          with Result, PVSFixedFileInfo( Info )^ do
                          begin
	                       MajorVersion := HighWord( dwFileVersionMS );
                               MinorVersion := LowWord( dwFileVersionMS );
                               Release := HighWord( dwFileVersionLS );
                               Build := LowWord( dwFileVersionLS );
                          end;
                     end;
                end;
             finally
                    FreeMem( Data );
             end;
        end;
     finally
            StrDispose( pFileName );
     end;
end;

begin
     with GetVersionInfo do
     begin
          Result := 'Build ' +
                    IntToStr( MajorVersion ) +
                    '.' +
                    IntToStr( MinorVersion ) +
                    '.' +
                    IntToStr( Release ) +
                    '.' +
                    IntToStr( Build );
     end;
end;

function GetApplicationVersionInfo( const sItem: String ): String;
const
     K_LEE_FILE_INFO = '\\StringFileInfo\\040904E4\\%s';
var
   Size, Value: DWord;
   pFileName: PChar;
   sFileName: String;
   Data, Info: Pointer;
   iLen: UINT;
begin
     Result := VACIO;
     sFileName := Application.ExeName;
     pFileName := StrAlloc( Length( sFileName ) + 1 );
     try
        pFileName := StrPCopy( pFileName, sFileName );
        Size := Windows.GetFileVersionInfoSize( pFileName, Value );
        if ( Size > 0 ) then
        begin
             GetMem( Data, Size );
             try
                if Windows.GetFileVersionInfo( pFileName, Value, Size, Data ) then
                begin
                     if Windows.VerQueryValue( Data, PChar( Format( K_LEE_FILE_INFO, [ sItem ] ) ), Info, iLen ) then
                        Result := PChar( Info );
                end;
             finally
                    FreeMem( Data );
             end;
        end;
     finally
            StrDispose( pFileName );
     end;
end;

function GetApplicationProductVersion: String;
begin
     Result := 'Versi�n ' + GetApplicationVersionInfo( 'ProductVersion' );
end;

{ ******* TAcercaDe ********* }

procedure TAcercaDe.FormCreate(Sender: TObject);
var
   rSize, rAvailable: Real;
{$ifdef ANTES}
   MemoryStatus: TMemoryStatus;
{$else}
   MemoryStatus :TMemoryStatusEx;
{$endif}
begin
     Logo.Picture.Icon := Application.Icon;
     Caption := 'Acerca de ' + Application.Title;
     VersionBuild.Caption := GetApplicationProductVersion;
     LblBuild.Caption := GetApplicationVersionNumber;
{$ifdef ANTES}
     MemoryStatus.dwLength := SizeOf( MemoryStatus );
     GlobalMemoryStatus( MemoryStatus );
     with MemoryStatus do
     begin
          RecursosLibres.Caption := Format( '%.0n', [( 100 - dwMemoryLoad ) / 1 ] ) + '%';
          MemoriaRAM.Caption := Format( '%.2n MegaBytes', [ BytesAMegas( dwTotalPhys ) ] );
          MemoriaRAMDisponible.Caption := Format( '%.2n MegaBytes', [ BytesAMegas( dwAvailPhys ) ] );
          MemoriaVirtual.Caption := Format( '%.2n MegaBytes', [ BytesAMegas( dwTotalVirtual ) ] );
          MemoriaVirtualDisponible.Caption := Format( '%.2n MegaBytes', [ BytesAMegas( dwAvailVirtual ) ] );
     end;
{$else}
     FillChar(MemoryStatus, SizeOf(MemoryStatus), 0);
     MemoryStatus.dwLength := SizeOf(MemoryStatus);
     // check return code for errors
     Win32Check(GlobalMemoryStatusEx(MemoryStatus));

     with MemoryStatus do
     begin
          RecursosLibres.Caption := Format( '%.0n', [( 100 - dwMemoryLoad ) / 1 ] ) + '%';
          MemoriaRAM.Caption := Format( '%.2n MegaBytes', [ BytesAMegas( ullTotalPhys ) ] );
          MemoriaRAMDisponible.Caption := Format( '%.2n MegaBytes', [ BytesAMegas( ullAvailPhys ) ] );
          MemoriaVirtual.Caption := Format( '%.2n MegaBytes', [ BytesAMegas( ullTotalVirtual ) ] );
          MemoriaVirtualDisponible.Caption := Format( '%.2n MegaBytes', [ BytesAMegas( ullAvailVirtual ) ] );
     end;
{$endif}
     rSize := BytesAMegas( DiskSize( 0 ) );
     rAvailable := BytesAMegas( DiskFree( 0 ) );
     DiscoTotal.Caption := Format( '%.2n MegaBytes', [ rSize ] );
     DiscoDisponible.Caption := Format( '%.2n MegaBytes', [ rAvailable ] );
     DiscoOcupado.Caption := Format( '%.2n', [ 100 * ( 1 - rAvailable / rSize ) ] ) + '%';
     LlenaPageAutorizacion;
end;

procedure TAcercaDe.LlenaPageAutorizacion;
begin
     with Autorizacion do
     begin
          if EsDemo then
          begin
               if HaySentinel then
               begin
                    Self.SerialNumber.Caption := IntToStr( NumeroSerie );
                    Self.Empresa.Caption := 'DEMO';
               end
               else
               begin
                    Self.SerialNumber.Caption := 'NO SENTINEL';
                    Self.Empresa.Caption := 'DEMO';
               end;
          end
          else
          begin
               Self.SerialNumber.Caption := IntToStr( NumeroSerie );
               Self.Empresa.Caption := IntToStr( Empresa );
          end;
          Self.Version.Caption := Version;
          Self.Empleados.Caption := GetEmpleadosStr;
          Self.Vencimiento.Caption := GetVencimientoStr;
          Self.Caducidad.Caption := ' Pr�stamos: ' + GetCaducidadStr + ' ';
          Self.Usuarios.Caption := IntToStr( Usuarios );
          Self.EsKit.Checked := EsKit;
          lblPlataforma.Caption := GetPlataformaStr;
          lblBaseDatos.Caption  := GetSQLEngineStr; 
          GetModulos( Self.Modulos );
          GetPrestamos( Self.Prestamos );
     end;
end;

procedure TAcercaDe.FormShow(Sender: TObject);
begin
     with PageControl do
     begin
          ActivePage := Pages[ 0 ];
     end;
end;

end.

