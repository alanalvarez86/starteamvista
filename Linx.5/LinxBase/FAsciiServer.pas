unit FAsciiServer;

interface

uses
  Classes, SysUtils;

type
  TAsciiServer = Class(TObject)
  private
      { Private declarations }
    FUsed: Boolean;
    FFileSize: LongInt;
    FLongBuffer: TextFile;
    FSmallBuffer: TextFile;
    FDataBuffer: TextFile;
    function GetEof: Boolean;
    function GetFullFileName(const sValue: String): String;
    function GetTimeStamp: String;
  public
      { Public declarations }
    constructor Create;
//    property Bytes: LongInt read FFileSize;
    property EndOfFile: Boolean read GetEof;
    property Used: Boolean read FUsed;
    function Read: String;
    procedure Init;
    procedure InitBuffersOnly;
    procedure Close;
    procedure CloseBuffersOnly;
    procedure CloseAll;
    procedure Open(const sArchivo: String);
    procedure Write(const sData: String);
    procedure WriteAllData(const sData: String);
  end;

implementation

uses
  FLinxTools, ZetaCommonTools;

const
  ALL_DATA_BUFFER    = 'LECTURAS.DAT';
  BIG_ASCII_BUFFER   = 'MIENT.MIE';
  SMALL_ASCII_BUFFER = 'MIENT.DAT';
  ASCII_BUFFER_1     = 'MIENT.001';
  ASCII_BUFFER_2     = 'MIENT.002';
  ASCII_BUFFER_3     = 'MIENT.003';

  {$I+}         { Input/Output-Checking Directive }

procedure SwapFiles(const sSource, sResult: String);
begin
  if SysUtils.FileExists(sResult) then
    SysUtils.DeleteFile(sResult);
  SysUtils.RenameFile(sSource, sResult);
end;

{ *********** TASCIIServer ********** }

constructor TAsciiServer.Create;
begin
  FUsed := False;
end;

function TAsciiServer.GetFullFileName(const sValue: String): String;
begin
  Result := FLinxTools.SetFileNameDefaultPath(sValue);
end;

function TAsciiServer.GetTimeStamp: String;
begin
  Result := '********** ' + FormatDateTime('ddd dd/mmm/yyyy hh:nn:ss AM/PM', Now) + ' **********';
end;

procedure TAsciiServer.InitBuffersOnly;
begin
  try
    SwapFiles(GetFullFileName(ASCII_BUFFER_2), GetFullFileName(ASCII_BUFFER_3));
    SwapFiles(GetFullFileName(ASCII_BUFFER_1), GetFullFileName(ASCII_BUFFER_2));
    SwapFiles(GetFullFileName(SMALL_ASCII_BUFFER), GetFullFileName(ASCII_BUFFER_1));
    AssignFile(FLongBuffer, GetFullFileName(BIG_ASCII_BUFFER));

    if SysUtils.FileExists(GetFullFileName(BIG_ASCII_BUFFER)) then
      Append(FLongBuffer)
    else
      Rewrite(FLongBuffer);

    AssignFile(FSmallBuffer, GetFullFileName(SMALL_ASCII_BUFFER));
    Rewrite(FSmallBuffer);
    Writeln(FLongBuffer, GetTimeStamp);
    FUsed := True;
  except
  end;
end;

procedure TAsciiServer.Init;
begin
  InitBuffersOnly;
  if FUsed then begin
    AssignFile(FDataBuffer, GetFullFileName(ALL_DATA_BUFFER));
    if FileExists(GetFullFileName(ALL_DATA_BUFFER)) then
      Append(FDataBuffer)
    else
      Rewrite(FDataBuffer);
    Writeln(FDataBuffer, GetTimeStamp);
  end;
end;

procedure TAsciiServer.CloseAll;
begin
  if Used then begin
    CloseBuffersOnly;
    CloseFile(FDataBuffer);
    FUsed := False;
  end;
end;

procedure TAsciiServer.CloseBuffersOnly;
begin
  if Used then begin
    CloseFile(FLongBuffer);
    CloseFile(FSmallBuffer);
    FUsed := False;
  end;
end;

procedure TAsciiServer.Close;
begin
  if Used then begin
    CloseFile(FLongBuffer);
    FUsed := False;
  end;
end;

procedure TAsciiServer.Open(const sArchivo: String);
var
  SearchRec: TSearchRec;
begin
  if FileExists(sArchivo) AND (FindFirst(sArchivo, faAnyFile, SearchRec) = 0) then begin
    AssignFile(FLongBuffer, sArchivo);
    Reset(FLongBuffer);
    FFileSize := SearchRec.Size;
    FUsed := True;
  end;
end;

function TAsciiServer.GetEof: Boolean;
begin
  Result := Eof(FLongBuffer);
end;

function TAsciiServer.Read: String;
begin
  Readln(FLongBuffer, Result);
end;

procedure TAsciiServer.Write(const sData: String);
begin
  if Used then begin
    Writeln(FLongBuffer, sData);
    Writeln(FSmallBuffer, sData);
  end;
end;

procedure TAsciiServer.WriteAllData(const sData: String);
begin
  if Used then begin
    Writeln(FDataBuffer, FormatDateTime('dd/mmm/yy hh:nn:ss AM/PM', Now) + ' ~ ' + sData);
  end;
end;

end.
