inherited Funcionalidad: TFuncionalidad
  Caption = 'Funcionalidad'
  ClientHeight = 199
  ClientWidth = 424
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ArchivoLBL: TLabel [0]
    Left = 28
    Top = 52
    Width = 115
    Height = 13
    Alignment = taRightJustify
    Caption = '&Archivo ASCII Adicional:'
    FocusControl = Archivo
  end
  object ArchivoSeek: TSpeedButton [1]
    Left = 395
    Top = 48
    Width = 23
    Height = 22
    Hint = 'Buscar Archivo ASCII Por Procesar Despu�s De Recolectar'
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000130B0000130B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
      300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
      330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
      333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
      339977FF777777773377000BFB03333333337773FF733333333F333000333333
      3300333777333333337733333333333333003333333333333377333333333333
      333333333333333333FF33333333333330003333333333333777333333333333
      3000333333333333377733333333333333333333333333333333}
    NumGlyphs = 2
    ParentShowHint = False
    ShowHint = True
    OnClick = ArchivoSeekClick
  end
  object EmpresaLBL: TLabel [2]
    Left = 99
    Top = 112
    Width = 44
    Height = 13
    Caption = '&Empresa:'
    FocusControl = Empresa
  end
  object CampoLBL: TLabel [3]
    Left = 31
    Top = 134
    Width = 112
    Height = 13
    Hint = 'Campo de COLABORA que Contiene el N�mero Impreso en Gafete'
    Caption = '&Campo de COLABORA:'
    FocusControl = Campo
  end
  inherited PanelBotones: TPanel
    Top = 170
    Width = 424
    Height = 29
    BevelOuter = bvNone
    TabOrder = 6
    inherited OK: TBitBtn
      Left = 259
      Top = 1
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 344
      Top = 1
    end
  end
  object ProcesarTarjetas: TCheckBox
    Left = 55
    Top = 27
    Width = 104
    Height = 17
    Hint = 'Procesar Tarjetas Despu�s de Recolectar'
    Alignment = taLeftJustify
    Caption = 'Procesar &Tarjetas:'
    Checked = True
    ParentShowHint = False
    ShowHint = True
    State = cbChecked
    TabOrder = 1
  end
  object EnviarChecadas: TCheckBox
    Left = 57
    Top = 6
    Width = 102
    Height = 17
    Hint = 'Enviar Checadas Despu�s de Recolectar'
    Alignment = taLeftJustify
    Caption = 'En&viar Checadas:'
    Checked = True
    ParentShowHint = False
    ShowHint = True
    State = cbChecked
    TabOrder = 0
  end
  object Archivo: TEdit
    Left = 146
    Top = 48
    Width = 245
    Height = 21
    TabOrder = 2
  end
  object TransformarNumero: TCheckBox
    Left = 42
    Top = 88
    Width = 117
    Height = 17
    Hint = 'Transformar N�mero de Empleado Seg�n Campo en COLABORA'
    Alignment = taLeftJustify
    Caption = 'Transformar &N�mero:'
    Checked = True
    ParentShowHint = False
    ShowHint = True
    State = cbChecked
    TabOrder = 3
    OnClick = TransformarNumeroClick
  end
  object Empresa: TEdit
    Left = 146
    Top = 108
    Width = 245
    Height = 21
    TabOrder = 4
  end
  object Campo: TEdit
    Left = 146
    Top = 130
    Width = 245
    Height = 21
    TabOrder = 5
  end
  object OpenDialog: TOpenDialog
    DefaultExt = '.dat'
    Filter = 'Datos ( *.dat)|*.dat|Textos ( *.txt )|*.txt|Todos ( *.* )|*.*'
    Options = [ofEnableSizing]
    Title = 'Especifique El Archivo ASCII Adicional'
    Left = 238
    Top = 16
  end
end
